/**
 * @file
 * A JavaScript file for the theme.
 *
 */

(function($) {
	$(document).ready(function(){
		$('#nav-shelf-action').click(function() {
      var cur_width = $( window ).width();
			if($("body").hasClass('nav-shelf-expand')) {
				$('.side-nav-inner').perfectScrollbar('destroy');
				$(".nav-shelf-expand").animate({
						left: "0",
					}, 200);
				$("#site-header").animate({
						left: "0",
					}, 200);
				$("#main-nav").animate({
					left: "-240",
				}, 200, function() {
					$("body").removeClass('nav-shelf-expand');
				});
				$('.nav-shelf-social').removeClass('social-border');
        $("body").css('cursor','');
        
			} else {
				$('.side-nav-inner').perfectScrollbar();
				$("body").addClass('nav-shelf-expand');

				$(".nav-shelf-expand").animate({
						left: "240",
					}, 200);
				$("#site-header").animate({
						left: "280",
					}, 200);
				$("#main-nav").animate({
					left: "0",
				}, 200);
				$('.nav-shelf-social').addClass('social-border');
        if(cur_width <= 1040) {
          $("body").css('cursor','pointer');
        }
			}
		});
		
		$('body').on('click swipe touchstart',function(e) {
			var tarId = $("#main-nav");
			if (e.target.id != tarId.attr('id') && !tarId.has(e.target).length && e.target.id != 'nav-shelf-action') {
				if($("body").hasClass('nav-shelf-expand')) {
          $('.side-nav-inner').perfectScrollbar('destroy');
          var cur_width = $( window ).width();
          if(cur_width >= 648) {
            $(".nav-shelf-expand").animate({
                left: "0",
              }, 200);
            $("#site-header").animate({
                left: "0",
              }, 200);
            $("#main-nav").animate({
              left: "-240",
            }, 200, function() {
              $("body").removeClass('nav-shelf-expand');
            });
          } else {
            $("#main-nav").animate({
              left: "-90%",
            }, 200, function() {
              $("body").removeClass('nav-shelf-expand');
            });
          }
          $('.nav-shelf-social').removeClass('social-border');
				} 
        $("body").css('cursor','');
			}
		});
		
		$(window).resize(function() {
			if($('body').hasClass('nav-shelf-expand')) {
				$('.side-nav-inner').perfectScrollbar('update');
			}
      var cur_width = $( window ).width();
      if(cur_width >= 648) {
        $('#main-nav').css({'left':'-240px','width':'280px'});
      } else {
         $('#main-nav').css({'left':'-90%','width':'90%'});
      }
      if($("body").hasClass('nav-shelf-expand')) {
        $('.side-nav-inner').perfectScrollbar('destroy');
        $("body").css('left', '0');
        $(".nav-shelf-expand").css('left', '0');
        $("#site-header").css('left', '0');
        $("body").removeClass('nav-shelf-expand');
        $('.nav-shelf-social').removeClass('social-border');
        $("body").css('cursor','');
      } 
		});
    
    $('#nav-shelf-action-mobile').click(function() {
      var cur_width = $( window ).width();
			if($("body").hasClass('nav-shelf-expand')) {
				$('.side-nav-inner').perfectScrollbar('destroy');
				$("#main-nav").animate({
					left: "-90%",
				}, 200, function() {
					$("body").removeClass('nav-shelf-expand');
				});
				$('.nav-shelf-social').removeClass('social-border');
        $("body").css('cursor','');
			} else {
				$('.side-nav-inner').perfectScrollbar();
				$("body").addClass('nav-shelf-expand');
				$("#main-nav").animate({
					left: "0",
				}, 200);
				$('.nav-shelf-social').addClass('social-border');
        if(cur_width <= 1040) {
          $("body").css('cursor','pointer');
        }
			}
		});
    
		/* Site Search */
		$('.header-nav ul').append('<li class="last"><a id="tw-search" href="#">Search</a><span class="site-search-img"></span></li>');
		
		$('a#tw-search').click(function(e){
			e.preventDefault();
			if($('#tw-site-search').css('display') == "none") {
				$('#tw-site-search').css('display','block');
				$('#page-wrapper').css('position','absolute');
				if(!Modernizr.input.placeholder){
					Placeholders.enable();
				}
				$("#page-wrapper").animate({
						top: "80",
					}, 200);
				$('.site-search-input').focus();
				
			} else {
				$('#tw-site-search').css('display','none');
				$('#page-wrapper').css('position','');
				$("#page-wrapper").animate({
						top: "0",
					}, 200);
				$('.site-search-input').val('');
				$('.site-search-input').removeClass('search-text');
			}
		});
		
    $('.site-search-img').click(function(e){
			if($('#tw-site-search').css('display') == "none") {
				$('#tw-site-search').css('display','block');
				$('#page-wrapper').css('position','absolute');
				if(!Modernizr.input.placeholder){
					Placeholders.enable();
				}
				$("#page-wrapper").animate({
						top: "80",
					}, 200);
				$('.site-search-input').focus();
				
			} else {
				$('#tw-site-search').css('display','none');
				$('#page-wrapper').css('position','');
				$("#page-wrapper").animate({
						top: "0",
					}, 200);
				$('.site-search-input').val('');
				$('.site-search-input').removeClass('search-text');
			}
		});
    
    $('.site-search-input').keydown(function(event){
      var keyword = $(this).val();
      if (!Modernizr.input.placeholder) {
        // for ie9
        if ($(this).attr('placeholder') == keyword) {
          keyword = '';
        }
      }
      if (event.keyCode == 13 && keyword != "") {
        $('#site-search-formid').attr('action', "/search/node/"+keyword);
        this.form.submit();
        return false;
      }
      else if (event.keyCode == 13 && keyword == "") {
        event.preventDefault();
        $('.site-search-input').focus();
      }
    });

    $('.site-search-input').keyup(function(e) {
      var keyword = $(this).val();
      if (keyword != "") {
        $(this).addClass('search-text');
      }
      else {
        $(this).removeClass('search-text');
      }
      if (!Modernizr.input.placeholder) {
        // for ie9
        if (keyword == "") {
          $(this).val($(this).attr('placeholder'));
        }
        if ($(this).attr('placeholder') != $(this).val()) {
          $(this).addClass('search-text');
        }
        else {
          $(this).removeClass('search-text');
        }
      }
    });
		
		$('.site-search-button').click(function(e){
			e.preventDefault();
				$('#tw-site-search').css('display','none'); 
				$('#page-wrapper').css('position','');
				$('.site-search-input').removeClass('search-text');
			$("#page-wrapper").animate({
						top: "0",
			}, 200);
			$('.site-search-input').val('');
			$('.site-search-input').removeClass('search-text');
		});
    
    /*Careers header menu*/
      var careers_menu_target = function(){
        $(document).on("click", function(e){
          $('#careers-header > div > ul > li').each(function(){
            if(this != e.target) {
              $(this).children('ul').css('display','none');
            }
          });
        });
      }();
    
    $('#careers-header > div > ul > li').each(function() {
      if($(this).children("ul").length) {
          $(this).addClass('career-menu-arrow');
       }
    });  
    
    $('#careers-header ul li').click(function () {
      
      if($(this).children('ul').css('display') == 'block') {
        $(this).children('ul').css('display','none');
      } else {
        $(this).children('ul').css('display','block');
      }
    });
    
    /*Press Release*/
    $('.pr-filters .form-select').each(function () {
       
       var selectHtml = $(this).parent('div').html();
       var selectVal = $(this).children("option:selected").text(); 
       $(this).parent('div').html(selectHtml + '<span class="pr-select-span">'+selectVal+'</span>');
    });
    
    $('.pr-filters .form-select').change(function() {
      var selectVal = $(this).children("option:selected").text();
      $(this).parent('div').children('.pr-select-span').html(selectVal);
    });
    
    $('.pr-filters .form-select').hover(function() {
      $(this).parent('div').children('.pr-select-span').css('border','1px solid #d2d2d2');
      //$(this).parent('div').children('.pr-select-span').addClass('change-arrow');
    }, function() {
      $(this).parent('div').children('.pr-select-span').css('border','1px solid #ebebeb');
       //$(this).parent('div').children('.pr-select-span').removeClass('change-arrow');
    });
    
    $('.pr-filters #edit-combine').keydown(function(event) {
      var keyword = $(this).val();
      if ((event.keyCode == 13) && (keyword != "")) {
        $('#views-exposed-form-press-release-page-page').submit();
      }
    });
    
    $('.pr-filter-clear').click(function() {
      $('#edit-combine').val('');
      return false;
    });
    
    $('.search-result').hover(function() {
      $(this).addClass('search-each-result');
    }, function() {
      $(this).removeClass('search-each-result');
    });
    
    // Adding auto complete attribute to search form.
    if($('#search-form input[type="text"]').length) {
      $('#search-form input[type="text"]').attr('autocomplete','off');
    }
    
    $('#advanced-search-expand').click(function() {
      $('#careers-advanced-search').addClass('ad-search-active');
      //$('.inputs-third.second').css('display','block');
     // $('.inputs-third.third .inputs-wrap').css('display','block');
      //$('#advanced-search-expand').hide();
      //$('#advanced-search-hide').show();
      return false;
    });
    
    $('#advanced-search-hide').click(function() {
      $('#careers-advanced-search').removeClass('ad-search-active');
      //$('.inputs-third.second').css('display','none');
      //$('.inputs-third.third .inputs-wrap').css('display','none');
      //$('#advanced-search-hide').hide();
      //$('#advanced-search-expand').show();
      return false;
    });
	//All links with class="open-blank" will open in new window.
    $('a.open-blank').each(function() {
      $(this).attr( {
        target: '_blank',
      });
    });
       
	});

	Modernizr.load({
		'test': Modernizr.input.placeholder,
    'nope': ['/' + tw_theme_path + '/lib/placeholder/placeholders.js'],
		'complete': function() {
				if(!Modernizr.input.placeholder){Placeholders.enable();}
		}
	});

})(jQuery);

