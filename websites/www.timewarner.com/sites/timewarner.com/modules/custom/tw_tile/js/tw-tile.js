/*
 * main javascript library for tiles
 */

// initial parameters
var tileCount          = 0;
var tilePage           = 0;
var tileCategoryIDs    = "";
var tileDivisionIDs    = "";
var tileJSONAjaxURI    = "/ajax-json-tiles";
var tileList           = "#tile-list";
var tileLoadMoreButton = "#load-more-button";
var requestPath        = "";

function RenderTile(tileNumber, item, hideTileOnInit) {
  var liClass        = "";
  var tileClass      = "";
  var tileDataColor  = "";
  var tileDataImage  = "";
  var tileDataLogo   = "";
  var tileDataSkin   = item.jwpSkin;
  var tileDataTitle  = "";
  var tileDataVideo  = item.video;
  var tileEditLink   = "";
  var tileImage      = "";
  var tileInfo       = "";
  var tileInfoIcon   = "";
  var tileJWPSkin    = "";
  var tileLogo       = "";
  var tileMarkup     = tileTemplates['tileTemplate'];
  var tilePlayIcon   = "";
  var tilePlayVideo  = "";
  var tileStyle      = "";
  var tileSubTitle   = "";
  var tileTextStyle  = "";
  var tileTitle      = "";

  // figure out 2 or 3 per row
  if ((tileCategoryIDs == "") && (tileDivisionIDs == "")) {
    if (((tileNumber % 5) >= 1) && ((tileNumber %5) <= 3)) {
      liClass = "tile-three-per-row";
    }
    else {
      liClass = "tile-two-per-row";
    }
  }
  else {
    liClass = "tile-three-per-row";
  }

  // add additional class if it has slides
  if (item.slides == "1") {
    tileClass = "tile-with-slides-wrapper";
  }

  // extract data from json to be used in html markup
  if (item.backgroundColor.length > 0) {
    tileStyle += "background-color:#" + item.backgroundColor + ";";
    tileDataColor = item.backgroundColor;
  }
  if (item.borderColor.length > 0) {
    tileStyle += "border-bottom-color:#" + item.borderColor + ";";
    tileTextStyle = "color:#" + item.borderColor + ";";
  }

  if (item.nodeEdit == 1) {
    var pathName = window.location.pathname;
    if (pathName == "/") {
      pathName = "";
    }
    tileEditLink = "<a href='/node/" + item.nid + "/edit?destination=" + pathName + "'>edit</a>";
  }

  if (item.heroImage.length > 0) {
    tileDataImage = item.heroImage;
  }
  else {
    tileDataImage = item.image;
  }

  if (item.imageTag.length > 0) {
    tileImage = item.imageTag;
    if (item.video.length == 0) {
      if (item.infoURL.length > 0) {
        if (item.infoURLTarget.length > 0) {
          tileImage = "<a target='" + item.infoURLTarget + "' href='" + item.infoURL + "'>" + tileImage + "</a>";
        }
        else {
          tileImage = "<a href='" + item.infoURL + "'>" + tileImage + "</a>";
        }
      }
    }
  }

  if (item.infoText.length > 0) {
    if (item.infoIcon.length > 0) {
      tileInfoIcon = "<img src='" + item.infoIcon + "' />";
    }

    if (item.infoURL.length > 0) {
      if (item.infoURLTarget.length > 0) {
        tileInfo = "<a target='" + item.infoURLTarget + "' href='" + item.infoURL + "' style='" + tileTextStyle + "'>" + item.infoText + " " + tileInfoIcon + "</a>";
      }
      else {
        tileInfo = "<a href='" + item.infoURL + "' style='" + tileTextStyle + "'>" + item.infoText + " " + tileInfoIcon + "</a>";
      }
    }
    else {
      tileInfo = item.infoText + " " + tileInfoIcon;
    }
  }
  else {
    tileMarkup = tileMarkup.replace('<div class="tile-info" style="#TILE-TEXT-STYLE#">#TILE-INFO#</div>', "");
  }

  if (item.altLogo.length > 0) {
    tileDataLogo = item.altLogo;
  }

  if (item.jwpSkin.length > 0) {
    tileJWPSkin = item.jwpSkin;
  }

  if (item.logo.length > 0) {
    tileLogo = "<img src='" + item.logo + "' />";
  }

  if (item.video.length > 0) {
    tilePlayVideo = "Watch Video ";
    if (item.playIcon.length > 0) {
      tilePlayIcon = "<img src='" + item.playIcon + "' />";
      tilePlayVideo += " <img src='" + item.playIcon + "' />";
    }
  }
  else {
    tileMarkup = tileMarkup.replace('<div class="tile-play-video" data-color="#TILE-DATA-COLOR#" data-image="#TILE-DATA-IMAGE#" data-logo="" data-logo-url="" data-skin="#TILE-DATA-SKIN#" data-title="#TILE-DATA-TITLE#" data-title-url="#TILE-DATA-TITLE-URL#" data-video="#TILE-DATA-VIDEO#" style="#TILE-TEXT-STYLE#">#TILE-PLAY-VIDEO#</div>', "");
    tileMarkup = tileMarkup.replace('<span class="tile-play-icon" data-color="#TILE-DATA-COLOR#" data-image="#TILE-DATA-IMAGE#" data-logo="" data-logo-url="" data-skin="#TILE-DATA-SKIN#" data-title="#TILE-DATA-TITLE#" data-title-url="#TILE-DATA-TITLE-URL#" data-video="#TILE-DATA-VIDEO#">#TILE-PLAY-ICON#</span>', "");
  }

  if (item.subTitle.length > 0) {
    if (item.titleURL.length > 0) {
      if (item.titleURLTarget.length > 0) {
        tileSubTitle = "<a target='" + item.titleURLTarget + "' href='" + item.titleURL + "'>" + item.subTitle + "</a>";
      }
      else {
        tileSubTitle = "<a href='" + item.titleURL + "'>" + item.subTitle + "</a>";
      }
    }
    else {
      tileSubTitle = item.subTitle;
    }
  }

  if (item.title.length > 0) {
    tileDataTitle = item.title;

    if (item.titleURL.length > 0) {
      if (item.titleURLTarget.length > 0) {
        tileTitle = "<a target='" + item.titleURLTarget + "' href='" + item.titleURL + "'>" + item.title + "</a>";
      }
      else {
        tileTitle = "<a href='" + item.titleURL + "'>" + item.title + "</a>";
      }
    }
    else {
      tileTitle = item.title;
    }
  }

  if (item.hideTitle.length > 0) {
    if (item.hideTitle == 1) {
      tileMarkup = tileMarkup.replace('<div class="tile-title">#TILE-TITLE#</div>', "");
      tileTitle = ""; // just in case
    }
  }

  // search and replace placeholders for data in html markup
  tileMarkup = tileMarkup.replace("#TILE-CLASS#", tileClass);
  tileMarkup = tileMarkup.replace(/#TILE-DATA-COLOR#/g, tileDataColor);
  tileMarkup = tileMarkup.replace(/#TILE-DATA-IMAGE#/g, tileDataImage);
  tileMarkup = tileMarkup.replace(/#TILE-DATA-LOGO#/g, tileDataLogo);
  tileMarkup = tileMarkup.replace(/#TILE-DATA-SKIN#/g, tileDataSkin);
  tileMarkup = tileMarkup.replace(/#TILE-DATA-TITLE#/g, tileDataTitle);
  tileMarkup = tileMarkup.replace(/#TILE-DATA-VIDEO#/g, tileDataVideo);
  tileMarkup = tileMarkup.replace("#TILE-EDIT-LINK#", tileEditLink);
  tileMarkup = tileMarkup.replace(/#TILE-ID#/g, item.nid);
  tileMarkup = tileMarkup.replace("#TILE-IMAGE#", tileImage);
  tileMarkup = tileMarkup.replace("#TILE-INFO#", tileInfo);
  tileMarkup = tileMarkup.replace("#TILE-JWP-SKIN#", tileJWPSkin);
  tileMarkup = tileMarkup.replace("#TILE-LOGO#", tileLogo);
  tileMarkup = tileMarkup.replace("#TILE-PLAY-ICON#", tilePlayIcon);
  tileMarkup = tileMarkup.replace("#TILE-PLAY-VIDEO#", tilePlayVideo);
  tileMarkup = tileMarkup.replace("#TILE-STYLE#", tileStyle);
  tileMarkup = tileMarkup.replace("#TILE-SUBTITLE#", tileSubTitle);
  tileMarkup = tileMarkup.replace(/#TILE-TEXT-STYLE#/g, tileTextStyle);
  tileMarkup = tileMarkup.replace("#TILE-TITLE#", tileTitle);

  tileCount++;
  if (hideTileOnInit) {
    tileMarkup = "<li id='tile-num-" + tileCount + "' class='" + liClass + "' style='display:none;'>" + tileMarkup + "</li>";
  }
  else {
    tileMarkup = "<li id='tile-num-" + tileCount + "' class='" + liClass + "'>" + tileMarkup + "</li>";
  }

  jQuery(tileList).append(tileMarkup);

  // hide image container if there is no image
  if (item.image.length == 0) {
    jQuery("#tile-image-id-" + item.nid).hide();
  }

  // hide slide container if there is no slide
  if (item.slides == "1") {
    jQuery("#tile-slides-id-" + item.nid).detach().appendTo("#tile-slides-id-" + item.nid + "-wrapper");
  }
  else {
    jQuery("#tile-slides-id-" + item.nid + "-wrapper").hide();
  }
}
function imagePreload() 
{
	//jQuery("p.hidden").remove();
	var ntilePage = tilePage + 1;
	var ajaxParameters = {page: ntilePage, categories: tileCategoryIDs, divisions: tileDivisionIDs, path: requestPath};
 
	jQuery.getJSON('/ajax-json-tiles', ajaxParameters, function(jsonData) {
		jQuery(jsonData).each(function(index, jsonItem) {
		var te= '<p class="hidden" style="display:none">'+jsonItem.imageTag+'</p>';
	    jQuery('body').append(te);
		jQuery(".hidden").show().hide().remove();	
		});
	});
	
}
function GetTilesByAjax(checkLoadMore) {
  var ajaxParameters = { page: tilePage, categories: tileCategoryIDs, divisions: tileDivisionIDs, path: requestPath };

  jQuery.getJSON(tileJSONAjaxURI, ajaxParameters, function(jsonData) {
    if (tilePage == 0) {
      // detach all slides
      jQuery(".tile-slides").each(function() {
        jQuery(this).detach().appendTo("#tile-slides");
      });
      jQuery(tileList).empty();
    }

    if (jsonData.length == 0) {
      jQuery(tileLoadMoreButton).hide();
    }
    else {
      var tileNumber     = 0;
      var jsonDataLength = jsonData.length;

      jQuery(jsonData).each(function(index, jsonItem) {
        tileNumber++;
        RenderTile(tileNumber, jsonItem, true);
        jQuery("#tile-num-" + tileCount).show();
      });

      jQuery(tileLoadMoreButton + " a").removeClass("loading");

      // check conditions to hide load more button
      if (checkLoadMore) {
        if (tilePage == 0) {
          if ((tileCategoryIDs == "") && (tileDivisionIDs == "")) {
            // unfiltered first page
            if (jsonData.length < 13) {
              jQuery(tileLoadMoreButton).hide();
            }
            else {
			  imagePreload();
              jQuery(tileLoadMoreButton).show();
            }
          }
          else {
            // filtered first page
            if (jsonData.length < 12) {
              jQuery(tileLoadMoreButton).hide();
            }
            else {
			  imagePreload();
              jQuery(tileLoadMoreButton).show();
            }
          }
        }
        else {
          if ((tileCategoryIDs == "") && (tileDivisionIDs == "")) {
            // unfiltered pages
            if (jsonData.length < 10) {
              jQuery(tileLoadMoreButton).hide();
            }
            else {
			  imagePreload();
              jQuery(tileLoadMoreButton).show();
            }
          }
          else {
            // filtered pages
            if (jsonData.length < 12) {
              jQuery(tileLoadMoreButton).hide();
            }
            else {
			  imagePreload();
              jQuery(tileLoadMoreButton).show();
            }
          }
        }
      }
    }
  });
}


function SetupFirstPageTiles() {
  var tileNumber = 0;

  // detach all slides
  jQuery(".tile-slides").each(function() {
    jQuery(this).detach().appendTo("#tile-slides");
  });
  jQuery(tileList).empty();

  jQuery(tileFirstPage).each(function(index, jsonItem) {
    tileNumber++;
    RenderTile(tileNumber, jsonItem, true);
    jQuery("#tile-num-" + tileNumber).show();
  });

  // check to hide load more button or not
  if (tileFirstPage.length < 13) {
    jQuery(tileLoadMoreButton).hide();
  }
  else {
    imagePreload();
    jQuery(tileLoadMoreButton).show();
  }

  // just in case
  jQuery(tileLoadMoreButton + " a").removeClass("loading");
}
