var groupresponseData;
var searchProductResponse=null;
var addProductGroupSearchResponse=null;
var softwareContractsViewLevels=null;

(function() {
	 var InsightSearch = window.InsightSearch = {
	            'staticContentUrl':'/insightweb/getStaticContent',
	'messageUrl':'/insightweb/getCmsContent',
	'addToCartFromCompanyStandardsFlag':false,
	'globalCSData':'',            
	 relatedLinkLabelCache : null,
	 relatedLinkMapCache : null,
	 ipsSearchCurrentContract : null,
	 numberOfSingleSelection : 0, 
	 globalSearchLabels : null,
	 globalSearchMetaTagLabels : null,
	 csGroupFlag : false,
	 returnToSearchResultsURL : null,
	 csData : null,
	 groupName : null,
	 categoryId : null,
	 setId : null,
	 setType : null,
	 groupId : null,
	 cmtStandards:null,
	 cmtCustomerNumber:null,
	 shared : null,
	 controller : null,
	 microSiteMfrId : null,
	 isFromCs : false,
	 isShowThisPage : false,             productInfoResponse : null,
	 cartFromAddToCart : null,
	 warrantyAddedFromProductInfo : null,
	 oemAddedToCartFromPopup : new Array(),
	 ippAddedToCartFromPopup : new Array(),
	 oemWarrantiesInPopup : new Array(),
	 parentProductQuantity : null,
	 icsMap : null,
	 checkedProducts : [],
	 shoppingCartlabels : null,
	 lastReportedUsage : null,
	 reportUsageMonth :"",
	 reportUsagePeriod :"",
	 SPLAProgramID : false,
	 CITRIXPRogramID : false,
	 allMonthsCurrent:true,
	 loadSearchLabels : function(){            	
		if(InsightSearch.globalSearchLabels!=null){
			return InsightSearch.globalSearchLabels;
		}else{
			InsightSearch.globalSearchLabels = InsightCommon.getMessageContent(InsightSearch.messageUrl, Insight.locale, "http://www.insight.com/messages/search/searchLabels.txt");    	
			InsightSearch.globalSearchLabels = eval('(' + InsightSearch.globalSearchLabels + ')');  	
			searchLabels = InsightSearch.globalSearchLabels;
			return  InsightSearch.globalSearchLabels;
		}
	},
	displayResults : function(data){
		if(data.nugsSearchParameters!=null && data.nugsSearchParameters.searchCategory!=null && data.nugsSearchParameters.searchCategory.categoryLabel!=null && data.nugsSearchParameters.searchCategory.categoryLabel!=""){
			document.title=data.nugsSearchParameters.searchCategory.categoryLabel;
		}
		InsightCommon.showLoading();
		// reset csid map
		InsightSearch.icsMap = null;
		var isLoggedIn=$('#loggedInFlag').val();
		/* Contract Data*/
	
		if(data.clpview == true){   
			$("#bodyContent").html("");
		      var catId = data.nugsRecognizedCategoryKeywords.categoryId;
		      //Get CLP template for this product from CMS
		      InsightCommon.getServiceResponseAsync("/insightweb/getCategoryUrl?category="+catId, null, "GET", function(clpUrl) {
		    	  InsightCommon.getMessageContentAsync(InsightSearch.messageUrl, "", clpUrl.url, function(clpHtml) {
		    		  var srchLabels = InsightSearch.loadSearchLabels();
		                  data = $.extend(data, srchLabels);
		                  showCLP(data, clpHtml);
		    	  });
		      });
		}else{                 
	    	  var srchLabels = InsightSearch.loadSearchLabels();
	    	  if(InsightSearch.cmtStandards!=null && data.cmtLocale!=null ){
	    		  Insight.locale = data.cmtLocale;
	    	  }
	    	    /* we end the advanced search here */
	          if(data.nugsProducts != null  && data.nugsHitCount==1 && data.brandData == null){
	             if(data.nugsProducts[0].discontinuedStatus){
	            	 $("#advanceSearch").html("");
	                 $("#bodyContent").html("");
	                 advanceSearch="advSearch";
	                 advanceSearchSubmitData = data;
	                 postAdvanceData(advanceSearch);
	             }else{
		              $.each(data.nugsProducts, function(j, prod){
		            	  InsightCommon.isFromSearch= true;
		            	  getProductInfo(prod.materialId, 'mainPPP', '','',true,InsightSearch.isFromCs,InsightSearch.groupName,InsightSearch.categoryId,InsightSearch.setId,InsightSearch.groupId,InsightSearch.shared,InsightSearch.controller);
		              });
	             }
	            }else
	            	if (data.nugsProducts != null && data.nugsHitCount>0 && data.brandData == null){
		                  narrowData = data;
		                  totalRecords=data.nugsHitCount;
		                  var IBOResults = null;
		                  if(data!=null && narrowData.inventoryBlowOut!=null && narrowData.inventoryBlowOut)
		                	  inventoryBlowOut = true;
			                  $.when( 
			                  		InsightCommon.getContentAsync(InsightSearch.staticContentUr, '', "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/searchContentTemplate.html"), 
			                  		InsightCommon.getContentAsync(InsightSearch.staticContentUr, '', "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/relatedLinksMapList.txt"),
			                  		InsightCommon.getMessageContentAsync("/insightweb/getCmsContent", Insight.locale, "http://www.insight.com/messages/search/relatedLinksLabels.txt")
			                  ).then(function(searchTemplate,relatedLinksMap,relatedLinksLabels ){
		                             $.template( "searchTemplate", searchTemplate[0]);
		                             if(InsightSearch.relatedLinkLabelCache==null){
		                            	InsightSearch.relatedLinkLabelCache = relatedLinksLabels[0];
		                             }
		                             if(InsightSearch.relatedLinkMapCache ==null){
		                            	 InsightSearch.relatedLinkMapCache = relatedLinksMap[0];
		                             }
		                             relatedLinksLabels = JSON.parse(InsightSearch.relatedLinkLabelCache);
		                             relatedLinksMap = JSON.parse(InsightSearch.relatedLinkMapCache);                                        
					                 var researchLoggedIn=  $('#loggedInFlag').val();;
					                 data.researchResultLog=researchLoggedIn;
					                 var categoryItem =null;
					                 if(data.nugsSearchParameters.searchCategory!=null)
					                 categoryItem = data.nugsSearchParameters.searchCategory.categoryId;
		                             if(data!=null && narrowData.inventoryBlowOut!=null && narrowData.inventoryBlowOut)
		                           	  	inventoryBlowOut = true;
					                  	$("#bodyContent").html("");
					                  	var markup = data;
						                $.each(markup.nugsProducts, function(index, value) {                            	 
							                value["buttonDescription"]=value["description"].replaceAll("\"","\\\""); 
							                value["buttonDescription"]=value["buttonDescription"].replaceAll("\'","\\\'");
					                    });
					                  var loggedInFlag = $('#loggedInFlag').val();
					                  var loginFlag=false;
					                  if(loggedInFlag){
					                       if(loggedInFlag==true || loggedInFlag=="true")
					                             loginFlag=true;
					                  }
					      	          markup = $.extend(markup,{'showApproveditem':Insight.showApproveditem});
					                  markup = $.extend(markup,{"IBOResults":IBOResults},{"relatedLinksLabels":relatedLinksLabels.labels},{"relatedLinksMap": relatedLinksMap},{"currentSearchContract":InsightSearch.ipsSearchCurrentContract},{"userPermissions":Insight.userPermissions},{'similarMtrId':similarMaterialId},{'loginFlag':loginFlag},{'savedProductsList':savedProducts}, {'labels':srchLabels.labels}, {'imgServer':imageServerURL}, {'category': categoryItem},{'IBdata':inventoryBlowOut},{'labels':srchLabels.labels});
					                  InsightCommon.renderTemplate( "searchTemplate", markup, "#bodyContent");
		                              $.each(markup.relatedLinksMap, function(i, item){
		                            	 if(i == markup.category ){
		                            		 $(".relatedbox").css("display","block");
		                            	 }
		                              });
					                  if(isLoggedIn=="false"){
					            	   $("#researchRequestSearchDiv").css('display','none');
					            	  }
					                  $("#shownRecords").val(data.shown);
					                  currentPageNum=data.currentPage;
					                  createPagination(data.currentPage,data.shown);
					                  var narowdata = null;
					                  if(data.manufacturerSummary!=null)
					                        narowdata = data.manufacturerSummary.fieldSummaryItems;
					                  if(inventoryBlowOut)
					                        $("#centerDiv").attr("id","inventoryBlowOutDiv");
					                  /* start : sort by options building */
					                  if( ($("#nugsSortBySelectTop option").size()<=0) && ($("#nugsSortBySelectBottom option").size()<=0) ){
					                        $.each(data.nugsSearchParameters.sortList, function(index, value) {
					                              if(nugsSortBy && nugsSortBy==value){
					                                    $("#nugsSortBySelectTop").append('<option value="'+value+'"  selected>'+srchLabels.labels[value]+'</option>');
					                                    $("#nugsSortBySelectBottom").append('<option value="'+value+'"  selected>'+srchLabels.labels[value]+'</option>');
					                              }else if( !nugsSortBy && data.nugsSearchParameters.sort == value){
					                                    $("#nugsSortBySelectTop").append('<option value="'+value+'"  selected>'+srchLabels.labels[value]+'</option>');
					                                    $("#nugsSortBySelectBottom").append('<option value="'+value+'"  selected>'+srchLabels.labels[value]+'</option>');
					                              }else{
					                                    $("#nugsSortBySelectTop").append('<option value='+value+'>'+srchLabels.labels[value]+'</option>');
					                                    $("#nugsSortBySelectBottom").append('<option value='+value+'>'+srchLabels.labels[value]+'</option>');
					                              }if(value=="BestMatch" && data.showBestMatch==false){
					                                    $("#nugsSortBySelectTop option[value='BestMatch']").remove();
					                                    $("#nugsSortBySelectBottom option[value='BestMatch']").remove();
					                              }
					                        });
					                        if((data.showBestMatch==true) && (nugsSortBy == "BestMatch" || nugsSortBy == null)){
					                              $("#nugsSortBySelectTop").val(srchLabels.labels['BestMatch']);
					                              $("#nugsSortBySelectBottom").val(srchLabels.labels['BestMatch']);
					                        }
					                  }
					                  /* end : sort by options building */
					
					                  InsightCommon.hideLoading();
					                 /* Show mainContent Div */
					                  $("#searchContent").show();
					
					                  /* Reconstruct the Saved Product Compare Box */
					                  reconstructSPCBox();
					
					                  /* Adjusting the height */
					                  var centerDivHeight = $("#centerDiv").height();
					                  if(inventoryBlowOut)
					                        centerDivHeight = $("#inventoryBlowOutDiv").height();
					                  var login=productCenterLogin();
					                  if(login==false){
					                        $("#productCenterId").css('display','none');
					                  }
					                  var isNonUsCommunitiesFlag = false;
					                  $.each(data.nugsProducts, function(index, value){
					                	  if(value!=null && value.contractDisplayInfo!=null){                            		
					                		  isNonUsCommunitiesFlag = true; 
					                		  if(value.contractDisplayInfo.contractTitle=='CALLFORPRICELABEL'){
					                			  var contractId = "orderButton_"+value.materialId;
					                 			  contractDiv = InsightSearch.getContractDivId(contractId);			
					                 			  $('#'+contractDiv).css("display","none");
					                		  }    
					                	  }
					                  });
					
					                  InsightSearch.contractData = data;
					                  if(data.priceCacheData!=='undefined' && data.priceCacheData!=null && data.priceCacheData.productData!=='undefined' && data.priceCacheData.productData!=null && data.priceCacheData.productData.length>0 
					                		&&  !isNonUsCommunitiesFlag && InsightSearch.ipsSearchCurrentContract!='undefined' && InsightSearch.ipsSearchCurrentContract!=='openMarket'){ 
					              		var contractPricingDiv;
					              		var searchData = data;
					      				var url = '/insightweb/getCachedContractPrices/'+InsightCommon.getCurrentTimestamp();
					      				var priceCacheDataToExtend =  '"priceCacheData" : '+ JSON.stringify(data.priceCacheData);
					      				var materialIdToExtend = '"materialId" : ""';
					      				var morePricesFlag = '"morePricesFlag" : '+ JSON.stringify({"morePricesFlag":false});
					      				var jsonObj= '{'+priceCacheDataToExtend+','+ materialIdToExtend +','+morePricesFlag+ '}';
					      				var reqType = 'POST';
					      				var contractPricingData =  null ; 
					      				InsightCommon.getServiceResponseAsync(url,jsonObj,"POST",function(data){ 				
					      					contractPricingData = data;
											InsightSearch.getContractPrice(contractPricingData,searchData,srchLabels);
					      				});	                  				
					      			  }
				              });
		           }else if(data.body != null){
	                  $("#bodyContent").html("");
	                  var microSiteTemplate = InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/microSiteTemplate.html");
	                  $.template( "microSiteTemplate", microSiteTemplate);
	                  InsightCommon.renderTemplate( "microSiteTemplate", " ", "#bodyContent" );
	                  $("#microSiteTemplateDiv").html(data.body);
	              }else if(data.brandData != null){
	                  renderShopByBrand(data);
	              }else if(advanceSearch!=null && advanceSearch=='advSearch'){                        	
	                  getAdvanceSearchTemplateContent(data);                              
	              }else if(advanceSearch==null){
	                  $("#advanceSearch").html("");
	                  $("#bodyContent").html("");
	                  advanceSearch="advSearch";
	                  advanceSearchSubmitData = data;
	                  postAdvanceData(advanceSearch);
	              }
		  }
	      /* Scroll to the Top of the Page */
	      window.scrollTo(0, 0);
	      $(".search-input").val('');
	},
	
	getPriceCacheData : function(data){
		var url = '/insightweb/getCachedContractPrices/'+InsightCommon.getCurrentTimestamp();
		var priceCacheDataToExtend =  '"priceCacheData" : '+ JSON.stringify(data.priceCacheData);
		var materialIdToExtend = '"materialId" : ""';
		var jsonObj= '{'+priceCacheDataToExtend+','+ materialIdToExtend + '}';
		var reqType = 'POST';
		var contractPricingData = InsightCommon.getServiceResponse(url,jsonObj,reqType);
		return contractPricingData;
	},            
	
	getContractDivId: function(contractId){
		return contractId.replaceAll("#", "\\#").replaceAll("/","\\/").replaceAll(".","\\.").replaceAll("=","\\="); 
	},
	getContractPrice : function(contractPricingData,data,srchLabels){
		if(contractPricingData!=null && contractPricingData.length>0 && contractPricingData[0]!=null ){                   					
				$.each(contractPricingData,function(i,val){
					if(val!=null){
						var contractId = "contractHeader_"+val.materialId;
						contractDiv = InsightSearch.getContractDivId(contractId); 
						contractPricingDiv=getContractPricingDiv(val, null,srchLabels,contractId);
						$('#'+contractDiv).html("");
						$('#'+contractDiv).html(contractPricingDiv); 
					}
				});
		}else{
			$.each(data.nugsProducts,function(i,val){
				var contractId = "contractHeader_"+val.materialId;
				contractDiv = InsightSearch.getContractDivId(contractId); 
				contractPricingDiv=getContractPricingDiv(null, val.materialId,srchLabels,contractId);
				$('#'+contractDiv).html("");
				$('#'+contractDiv).html(contractPricingDiv); 
			});
		}
		if(InsightSearch.ipsSearchCurrentContract!='All'){
			$('#centerDiv').css("width","700px");	
		}
	},
	
	nonUSCommunityContractPrice : function(contractPricingData,srchLabels){
		var contractPricingDiv ='';
		if(contractPricingData!=null  ){    						
				var contractId = "contractHeader_"+contractPricingData.materialId;
				contractDiv = InsightSearch.getContractDivId(contractId); 
				contractPricingDiv=getNonUsCommunityContractPricingDiv(contractPricingData,srchLabels,contractId);    						  					
				$('#'+contractDiv).html("");
				$('#'+contractDiv).html(contractPricingDiv); 
		}    		    	
		return contractPricingDiv;
	},
	
	addToCart : function(selectedMeterialId){
		  var sDate = new Date();
		  sDate = (sDate.getMonth()+1)+"/"+sDate.getDate()+"/"+sDate.getFullYear();
	      var shopCategoryURL = "/insightweb/transaction/addtocart";
	      var jsonObj = '{"clientBrowserDate":"'+sDate+'","materialID" : "'+selectedMeterialId+'","quantity" : 1}';
	      var data = InsightCommon.getServiceResponse(shopCategoryURL,jsonObj,"POST");
	},
	
	purchasePopUpPermission: function(){
		var testPermission = Insight.userPermissions;        	
	 	if(testPermission!=null && $.inArray('enable_purchase_popup',testPermission)==-1){	
	 		return false;
	 	}else if(testPermission==null){
	 		return true;
	 	}
	 	return true;
	},
	postData : function(){
	    InsightCommon.showLoading();
	 	var sessionSearchURL = "";
	    var shown = $("#shownRecords").val();
	    productSet = productSetMenu;
		if(paginationFlag){
		      shown= 10;
		      getPage=1;
		}
       /* ***Reset advanceSearch depending on searchTxt*** */
       if(searchTxt!=null && searchTxt!="")
            advanceSearch = null;
      
       /* For software license search, set advanced search to null */
       if(fromLicense!=null && fromLicense==true){
    	  advanceSearch = null;
       }
       if(narrowData!=null && narrowData.inventoryBlowOut){
          inventoryBlowOut = true;
       }
       if(inventoryBlowOut) {
    	  searchTxt ='';
    	  shown = 250;
       }else if(shown == null)
            shown = 10;
	
	      /* ***Check for advanceSearch value*** */
       if(advanceSearch!=null && advanceSearch=='advSearch'){
	        if(advSearchSubmit!=null){
	              advanceSearch = null;
	        }
	        var advSearchURLAddress = '';
	        /* ***Create jsonObj for Advance Search and make Advance Search Service call*** */
	        var url =  '/insightweb/getAdvanceSearch';//$('#url').val();
	        var jsonObj = '{"defaultPlant": "10",  "useBreadcrumb": false,"page": "1","shown":"10","pageNumber": 0,"pageSize": 0,';
	        if(mainCatagorySelected!=null ){
	        	jsonObj = jsonObj+'"selectedMainCategory":"'+mainCatagorySelected+'","mainCategory" : ['+mainCategory+'],';
	        	advSearchURLAddress= advSearchURLAddress+"#selectedMainCategory="+mainCatagorySelected;
	        }
	        if(subCategorySelected!=null ){
	        	 jsonObj =   jsonObj+'"category":"'+subCategorySelected+'",';
	        	 advSearchURLAddress= advSearchURLAddress+"#category="+subCategorySelected;
	        }
	        if(advSearchSubmit!=null){
	        	jsonObj =   jsonObj+'"submitSearch":"'+advSearchSubmit+'",';
	        	advSearchURLAddress= advSearchURLAddress+"#submitSearch="+advSearchSubmit;
	        }
	        if(manufactureIdSelected!=null && manufactureIdSelected.length > 0) {
	        	manufactureIdSelected = $.map($("#manufacturerId"), function(elt, i) { return $(elt).val();});
	        	if(manufactureIdSelected.length <= 0){
	           		 manufactureIdSelected = $.map($("#manufacturer"), function(elt, i) { return $(elt).val();});
	           	}
	        	jsonObj =   jsonObj+'"manufacturerId":"'+manufactureIdSelected+'",';
	        	advSearchURLAddress= advSearchURLAddress+"#manufacturerId="+manufactureIdSelected;
	        }
	        if(languageSelected!=null ){
	          	 jsonObj =   jsonObj+'"languageList":"'+languageSelected+'",';
	          	 advSearchURLAddress= advSearchURLAddress+"#languageList="+languageSelected;
	        }
	        if(platformSelected!=null ){
	             jsonObj =   jsonObj+'"platform":["'+platformSelected+'"],';
	             advSearchURLAddress= advSearchURLAddress+"#platform="+platformSelected;
	        }
	        if(productTypeSelected!=null ){
	             jsonObj =   jsonObj+'"productType":["'+productTypeSelected+'"],';
	             advSearchURLAddress= advSearchURLAddress+"#productType="+productTypeSelected;
	        }
	        if(priceRangeLow!=null || priceRangeHigh!=null){
	        	jsonObj =   jsonObj+'"priceRangeLow":"'+priceRangeLow+'","priceRangeHigh":"'+priceRangeHigh+'",';
	        	advSearchURLAddress= advSearchURLAddress+"#priceRangeLow="+priceRangeLow+"#priceRangeHigh="+priceRangeHigh;
	        }
	        if(advanceSearchTxt!=null){
	        	jsonObj =   jsonObj+'"searchText":["'+advanceSearchTxt+'"],';
	        	advanceSearchTxt = encodeURIComponent(advanceSearchTxt);
	        	advSearchURLAddress= advSearchURLAddress+"#searchText="+advanceSearchTxt;
	        }else
	            jsonObj =   jsonObj+'"searchText":[""],';
	
	        if(productSet!=null){
	        	jsonObj =   jsonObj+'"productSet":["'+productSet+'"],';
	        	advSearchURLAddress= advSearchURLAddress+"#productSet="+productSet;
	        }
	        if(approvedItemsOnly ){
	        	jsonObj = jsonObj+'"onlyApprovedItems" : "Y",';
	        	advSearchURLAddress= advSearchURLAddress+"#onlyApprovedItems=Y";
	        }
	        if (agreementProductsFromLeftNavFlag) {
	          	 jsonObj = jsonObj+'"onlyAgreementProducts" : "Y",';
	          	 sessionSearchURL = sessionSearchURL+"#onlyAgreementProducts=Y";
	        }
	        if(advSearchSubmit!=null) {
	          document.location.href="http://www.insight.com/insightweb/search#advSearch"+advSearchURLAddress;
	        }else{
		        jsonObj =   jsonObj+'"defaultSort": false,"businessUnit":"2","noCLP": false,"salesOrg": "2400", "rebateSearch": false, "imageURL" : "http://imagesqa01.insight.com/"}';
		        if (url != "" && jsonObj != "" ){
		              var data = InsightCommon.getServiceResponse(url,jsonObj,"POST");
		              InsightSearch.displayResults(data);	                        
		        }else
		        	alert(searchErrorMessages.labels.PleaseenterURLANDJSON);
	        }
       }else{
	            /* ***Create jsonObj for normal search and make the service call*** */
	            var url =  '/insightweb/getProduct';//$('#url').val();                    
	            var jsonObj = '{"defaultPlant": "10","useBreadcrumb": false,"page": '+getPage+',"pageNumber": 0,"pageSize": 0,';
	            sessionSearchURL = sessionSearchURL+"#page="+getPage;
	            if(!removeApprovedItemsFlag){
	            	if (narrowData!=null && narrowData.nugsSearchParameters.onlyApprovedItems){
	                  jsonObj = jsonObj+'"onlyApprovedItems" : "Y",';
	                  sessionSearchURL = sessionSearchURL+"#onlyApprovedItems=Y";
	            	}else  if (approvedItemsFromLeftNavFlag){
	 					 jsonObj = jsonObj+'"onlyApprovedItems" : "Y",';
	 					 sessionSearchURL = sessionSearchURL+"#onlyApprovedItems=Y";
	            	}
      			}
                if(!removeAgreementProductsFlag){
                	 if (narrowData!=null && narrowData.nugsSearchParameters.onlysoftwareProducts){
                		 jsonObj = jsonObj+'"onlyAgreementProducts" : "Y",';
                    	 sessionSearchURL = sessionSearchURL+"#onlyAgreementProducts=Y";
                	 }else if (agreementProductsFromLeftNavFlag) {
                    	 jsonObj = jsonObj+'"onlyAgreementProducts" : "Y",';
                    	 sessionSearchURL = sessionSearchURL+"#onlyAgreementProducts=Y";
                     }
                }
                if(narrowData!=null ){
		            if(narrowData.nugsSearchParameters.fullSearchUrl!=null){
		                  var str = narrowData.nugsSearchParameters.fullSearchUrl;
		                  if(str.search("productSet")>0 && productSetMenu==null){
		                     productSet=narrowData.nugsSearchParameters.searchCategory.categoryId;
		                     searchTxt ="";
			                 if(removeBreadCrumb==null && removeCategory==productSet)
			                       productSet = '';
		                  }
		             }
	            }
	        	if( !removeRebateFlag){
	        		if(narrowData!=null && narrowData.nugsSearchParameters.rebateSearch){
		        		 jsonObj =   jsonObj+'"ccnRebate":"CCN-Rebate","rebateSearch": true,';
		        		 sessionSearchURL = sessionSearchURL+"#ccnRebate=CCN-Rebate#rebateSearch=true";
	        		}
	        	}
	            if(productSet!=null && productSet!=''){
	                  jsonObj =   jsonObj+'"productSet":["'+productSet+'"],';
	                  narrowCategory = null;
	                  sessionSearchURL = sessionSearchURL+"#productSet="+productSet;
	            }else if(narrowData!=null && narrowData.nugsSearchParameters!=null && narrowData.nugsSearchParameters.searchCategory!=null){
	                  narrowCategory = narrowData.nugsSearchParameters.searchCategory.categoryId;
	            }
	            if(inventoryBlowOut && narrowCategory!=null && narrowCategory.search('C-')==0){
	               narrowCategory = narrowCategory;
	            }
	            var narrowFields = "";
	            var narrowFlag = false;
	            if(narrowData!=null && narrowData.nugsSearchParameters!=null && narrowData.nugsSearchParameters.fieldList.length>0){
	                  if((fieldId!=null && fieldValue!=null))
	                        narrowFlag = true;
	                  $.each(narrowData.nugsSearchParameters.fieldList, function(index, value){
	                	  if (value.fieldId != "A-XPM-ProgramID") {
		                      var narrowField = '"'+value.fieldId+'~'+value.fieldValues[0].fieldValue.replaceAll('"', '\\"')+'"';
		                      if((removeBreadCrumb!=null && removeBreadCrumb!=narrowField) || narrowFlag || fieldBread!=null ){
		                            narrowFields =narrowFields+narrowField+',';
		                            if(fieldBread !=null && fieldBread==narrowField )
		                                  return false;
		                      }
	                	  }
	                  });
	                  if(narrowFields !=null && narrowFields !="")
	                        narrowFields = narrowFields.substring(0,narrowFields.length-1);
	            }
	            if(fieldId!=null && fieldValue!=null && narrowFields!=""){
	                  jsonObj =jsonObj+'"field":['+narrowFields+',"'+fieldId+'~'+fieldValue.replaceAll('"', '\\"')+'"],';
	                  var fieldDataEncode = encodeURIComponent(narrowFields+',"'+fieldId+'~'+fieldValue.replaceAll('"', '\\"')+'"');
	                  sessionSearchURL = sessionSearchURL+"#field="+fieldDataEncode;
	            }else if(fieldId!=null && fieldValue!=null){
	                  jsonObj =jsonObj+'"field":["'+fieldId+'~'+fieldValue.replaceAll('"', '\\"')+'"],';
	                  var fieldDataEncode = encodeURIComponent('"'+fieldId+'~'+fieldValue.replaceAll('"', '\\"')+'"');
	                  sessionSearchURL = sessionSearchURL+"#field="+fieldDataEncode;
	            }else
	                  if(removeBreadCrumb!=null || fieldBread!=null){
	                	  jsonObj = jsonObj+'"removeBreadCrumb" : "Y",';
	                	  jsonObj =jsonObj+'"field":['+narrowFields+'],';
	                      var fieldDataEncode = encodeURIComponent(narrowFields);
	                      sessionSearchURL = sessionSearchURL+"#field="+fieldDataEncode;
	                      sessionSearchURL = sessionSearchURL+"#removeBreadCrumb="+encodeURIComponent(removeBreadCrumb);
	                  }
	                  else
	                        if(firstBreadCrumb==null && narrowData!=null && narrowData.nugsSearchParameters!=null && narrowData.nugsSearchParameters.fieldList.length>0) {
	                        	$.each(narrowData.nugsSearchParameters.fieldList, function(index, value){
									var narrowField = "";
	                        		if (value.fieldId != "A-XPM-ProgramID"){
	                        			narrowField = '"'+value.fieldId+'~'+value.fieldValues[0].fieldValue.replaceAll('"', '\\"')+'"';
	                        		} 									
		                            if (narrowField!=null && narrowField!==""){
		                            	narrowFields =narrowFields+narrowField+',';
		                            }
	                            });
	                            if(narrowFields !=null && narrowFields !="")
	                                  narrowFields = narrowFields.substring(0,narrowFields.length-1);
	                            jsonObj =jsonObj+'"field":['+narrowFields+'],';
	                            var fieldDataEncode = encodeURIComponent(narrowFields);
	                            sessionSearchURL = sessionSearchURL+"#field="+fieldDataEncode;
	                        }
	           
				            if((fieldId!=null && fieldId=='InStockOnly') || inventoryBlowOut || (narrowData!=null && narrowData.nugsSearchParameters.inStockFilterType!=null)){
				                if(removeBreadCrumb!=null && removeBreadCrumb=="InStockOnly"){
				                	jsonObj =jsonObj+'"inStockFilterType": "BothInStockAndOutOfStock",';
				                	sessionSearchURL = sessionSearchURL+"#inStockFilterType=BothInStockAndOutOfStock";
				                }else{
					               jsonObj =jsonObj+'"inStockFilterType": "InStockOnly",';
					               sessionSearchURL = sessionSearchURL+"#inStockFilterType=InStockOnly";
				                }
				            }else if(narrowData!=null && narrowData.nugsSearchParameters.inStockFilterType==null){
				            	   jsonObj =jsonObj+'"inStockFilterType": "BothInStockAndOutOfStock",';
				                   sessionSearchURL = sessionSearchURL+"#inStockFilterType=BothInStockAndOutOfStock";
				            }
	
					       if(removeText!=null ){
					            if(narrowData.nugsSearchParameters.searchText!=null){
					                  searchTxt = "";
					                  var searchTxts = "";
					                  $.each(narrowData.nugsSearchParameters.searchText, function(index, value){
					                        if(value.toLowerCase() !=removeText.toLowerCase() && removeText!=null){
						                         searchTxts='"'+value+'"';
						                         searchTxt =searchTxt+searchTxts+',';
					                        }
					                        searchTxts ="";
					                  });
					                  searchTxt = searchTxt.substring(0,searchTxt.length-1);
					            }else{
					               searchTxt = "";
					            }
					        }else{
					            if(narrowData!=null && narrowData.nugsSearchParameters.searchText!=null){
					                  searchTxt = "";
					                  var searchTxts = "";
					                  $.each(narrowData.nugsSearchParameters.searchText, function(index, value){
					                         searchTxts='"'+value+'"';
					                         searchTxt =searchTxt+searchTxts+',';
					                         searchTxts ="";
					                  });
					                  searchTxt = searchTxt.substring(0,searchTxt.length-1);
					                  if(additionalSearchText!=null &&  additionalSearchText!=""){
					                          searchTxt=searchTxt+',"'+additionalSearchText+'"';
					                  }
					            }else if(additionalSearchText!=null &&   additionalSearchText!=""){
					                 searchTxt = '"'+additionalSearchText+'"';
					            }
					        }
	
				            if(removeCategory!=null && removeCategory!=''){
				            	narrowCategory = null;
				            }
				            if(relatedCategory!=null )
				                  narrowCategory = relatedCategory;
				            if(narrowData!=null){
					            jsonObj =jsonObj+'"noCLP": "true",';
					            sessionSearchURL = sessionSearchURL+"#noCLP=true";
				            }else if(Insight.ipsFlag) {
				            	jsonObj =jsonObj+'"noCLP": "true",';
				            	sessionSearchURL = sessionSearchURL+"#noCLP=true";
				            }else{
				            	jsonObj =jsonObj+'"noCLP": "false",';
				           	 	sessionSearchURL = sessionSearchURL+"#noCLP=false";
				            }
				            if(narrowCategory!=null){
				                jsonObj =jsonObj+'"category":"'+narrowCategory+'",';
				                sessionSearchURL = sessionSearchURL+"#category="+narrowCategory;
				            }
				            if(inventoryBlowOut){
				                jsonObj =jsonObj+'"inventoryBlowOut":'+inventoryBlowOut+',';
				                sessionSearchURL = sessionSearchURL+"#inventoryBlowOut="+inventoryBlowOut;
				            }
				            if(similarMaterialId!=null){
				                jsonObj =jsonObj+'"similarMaterialId":"'+similarMaterialId+'",';
				                similarMaterialId = encodeURIComponent(similarMaterialId);
				                sessionSearchURL = sessionSearchURL+"#similarMaterialId="+similarMaterialId;
				            }
				            if(fromLicense!=null){
				                jsonObj =jsonObj+'"fromLicense":'+fromLicense+',';
				                sessionSearchURL = sessionSearchURL+"#fromLicense="+fromLicense;
				            }
				            if(licenseContractIds!=null){
				            	jsonObj =jsonObj+'"licenseContractIds":['+licenseContractIds+'],';
				                sessionSearchURL = sessionSearchURL+"#licenseContractIds="+encodeURIComponent(licenseContractIds);
				            }
				            if(licenseCountflag!=null){
				                jsonObj =jsonObj+'"licenseCountflag":'+licenseCountflag+',';
				                sessionSearchURL = sessionSearchURL+"#licenseCountflag="+licenseCountflag;
				            }
				            if(programIds!=null){
				                jsonObj =jsonObj+'"programIds":['+programIds+'],';
				                sessionSearchURL = sessionSearchURL+"#programIds="+encodeURIComponent(programIds);
				            }
				            if(priceRangeLow!=null){
				                jsonObj =jsonObj+'"priceRangeLow":"'+priceRangeLow+'",';
				                priceRangeLow = encodeURIComponent(priceRangeLow);
				                sessionSearchURL = sessionSearchURL+"#priceRangeLow="+priceRangeLow;
				            }
				            if(priceRangeHigh!=null){
				                jsonObj =jsonObj+'"priceRangeHigh":"'+priceRangeHigh+'",';
				                priceRangeHigh = encodeURIComponent(priceRangeHigh);
				                sessionSearchURL = sessionSearchURL+"#priceRangeHigh="+priceRangeHigh;
				            }                       
				            if(fromcs){
				            	jsonObj =jsonObj+'"fromcs":"'+fromcs+'",';
						     	sessionSearchURL = sessionSearchURL+"#fromcs="+fromcs;
				            }
				            if(groupId){
				            	jsonObj =jsonObj+'"groupId":"'+groupId+'",';
						     	sessionSearchURL = sessionSearchURL+"#groupId="+groupId;
				            }
				            if(setId){
				            	jsonObj =jsonObj+'"setId":"'+setId+'",';
						     	sessionSearchURL = sessionSearchURL+"#setId="+setId;
				            }                        
				            if(setType){
				            	jsonObj =jsonObj+'"setType":"'+setType+'",';
						     	sessionSearchURL = sessionSearchURL+"#setType="+setType;
				            }
				            if(controller){
				            	jsonObj =jsonObj+'"controller":"'+controller+'",';
						     	sessionSearchURL = sessionSearchURL+"#controller="+controller;
				            }
				            if(InsightSearch.cmtStandards!=null && InsightSearch.cmtStandards=='true'){
				            	jsonObj =jsonObj+'"cmtStandards":true,';
						     	sessionSearchURL = sessionSearchURL+"#cmtStandards=true";
						     	if(InsightSearch.cmtCustomerNumber!=null){
							     	jsonObj =jsonObj+'"cmtCustomerNumber":"'+InsightSearch.cmtCustomerNumber+'",';
							     	sessionSearchURL = sessionSearchURL+"#cmtCustomerNumber="+InsightSearch.cmtCustomerNumber;
						     	}
				            }
				            if(groupName){
				            	jsonObj =jsonObj+'"groupName":"'+groupName+'",';
						     	sessionSearchURL = sessionSearchURL+"#groupName="+encodeURIComponent(groupName);
				            }
				            if(shared){
				            	jsonObj =jsonObj+'"shared":"'+shared+'",';
						     	sessionSearchURL = sessionSearchURL+"#shared="+shared;
				            }                        
				            if(categoryId){
				            	jsonObj =jsonObj+'"categoryId":"'+categoryId+'",';
						     	sessionSearchURL = sessionSearchURL+"#categoryId="+categoryId;
				            }
				            if(shown!=null && shownScriptFlag && narrowData!=null && narrowData.userShownFlag ){
				            	jsonObj =jsonObj+'"shown": '+shown+',"shownFlag":false,';
				            	sessionSearchURL = sessionSearchURL+"#shown="+shown+"#shownFlag=false";
				            }else{
				            	jsonObj =jsonObj+'"shown": '+shown+',"shownFlag":true,';
				            	sessionSearchURL = sessionSearchURL+"#shown="+shown+"#shownFlag=true";
				            }
				            if(nugsSortBy){
				            	sessionSearchURL = sessionSearchURL+"#nugsSortBySelect="+nugsSortBy;
				                jsonObj =jsonObj+'"nugsSortBySelect":"'+nugsSortBy+'","searchText": [';
				            }else{
				                jsonObj =jsonObj+'"searchText": [';
				            }
				            if(searchTxt!=undefined){
				            	searchTxt = encodeURIComponent(searchTxt);
				            }else{
				            	searchTxt = "";
				            }
				            sessionSearchURL = sessionSearchURL+"#searchText="+searchTxt;
				            jsonObj =jsonObj+searchTxt+'],"defaultSort": false,"businessUnit":"2","fieldList": [],"salesOrg": "2400","imageURL" : "http://imagesqa01.insight.com/"}';
				            $(".search-input").val('');
				            var oldReturnToSearchResultsURL =  document.location.href; 
				            document.location.href = 'http://www.insight.com/insightweb/search#searchResults'+sessionSearchURL;
				            InsightSearch.returnToSearchResultsURL =  oldReturnToSearchResultsURL; 
       }
	},
	 
	advanceSearchResultsByAddress : function(selectedMainCategory,submitSearch,category,manufacturerId,languageSelected,productTypeSelected,platformSelected,searchText,shown,page,priceRangeLow,priceRangeHigh,productSet,onlyApprovedItems){
		 var  advSearchSessionJsonObj = '{"defaultPlant": "10","useBreadcrumb": false,"page":'+page+',"pageNumber": 0,"pageSize": 0,';
		 if(mainCatagorySelected!=null )
	     	advSearchSessionJsonObj = advSearchSessionJsonObj+'"selectedMainCategory":"'+mainCatagorySelected+'","mainCategory" : ['+mainCategory+'],';
	     if(subCategorySelected!=null )
	    	 advSearchSessionJsonObj =   advSearchSessionJsonObj+'"category":"'+subCategorySelected+'",';
	     if(submitSearch!=null)
	    	 advSearchSessionJsonObj =   advSearchSessionJsonObj+'"submitSearch":"'+advSearchSubmit+'",';
	     if(manufactureIdSelected!=null && manufactureIdSelected.length > 0){
	    	 manufactureIdSelected = $.map($("#manufacturerId"), function(elt, i) { return $(elt).val();});
	    	 if(manufactureIdSelected.length <= 0){
	    		 manufactureIdSelected = $.map($("#manufacturer"), function(elt, i) { return $(elt).val();});
	    	 }
	    	 advSearchSessionJsonObj =   advSearchSessionJsonObj+'"manufacturerId":"'+manufactureIdSelected+'",';
	     }else if(manufacturerId!=null) {
	    	 advSearchSessionJsonObj =   advSearchSessionJsonObj+'"manufacturerId":"'+manufacturerId+'",';
	     }
	     if(languageSelected!=null )
	      	advSearchSessionJsonObj =   advSearchSessionJsonObj+'"languageList":"'+languageSelected+'",';
	     if(platformSelected!=null )
	      	advSearchSessionJsonObj =   advSearchSessionJsonObj+'"platform":"'+platformSelected+'",';
	     if(productTypeSelected!=null )
	        advSearchSessionJsonObj =   advSearchSessionJsonObj+'"productType":"'+productTypeSelected+'",';
	     if(priceRangeLow!=null || priceRangeHigh!=null)
	    	 advSearchSessionJsonObj =   advSearchSessionJsonObj+'"priceRangeLow":"'+priceRangeLow+'","priceRangeHigh":"'+priceRangeHigh+'",';
	     if(advanceSearchTxt!=null)
	    	 advSearchSessionJsonObj =   advSearchSessionJsonObj+'"searchText":["'+advanceSearchTxt+'"],';
	     else
	    	 advSearchSessionJsonObj =   advSearchSessionJsonObj+'"searchText":[""],';
	     if(productSet!=null)
	    	 advSearchSessionJsonObj =   advSearchSessionJsonObj+'"productSet":["'+productSet+'"],';
	     if(approvedItemsOnly )
	    	 advSearchSessionJsonObj = advSearchSessionJsonObj+'"onlyApprovedItems" : "Y",';
	     advSearchSessionJsonObj =advSearchSessionJsonObj+'"shown":"10"}';
	     var advSearchResultsByAddressURL =  '/insightweb/getAdvanceSearch';
	 	 if(advSearchResultsByAddressURL != "" && advSearchSessionJsonObj != "" ){
	           var data = InsightCommon.getServiceResponse(advSearchResultsByAddressURL,advSearchSessionJsonObj,"POST");
	           if(typeof data!=='undefined' && data!=null){
	       		 InsightCommon.getRequestJsonObject = advSearchSessionJsonObj;
	       	   }
	           InsightSearch.displayResults(data);
	     }else
	          alert(searchErrorMessages.labels.PleaseenterURLANDJSON);
	          removeObjectSession();
	 },
	 
	 searchResultsByAddress : function(field,noCLP,category,shownFlag,searchText,shown,page,nugsSortBySelect,ccnRebate,rebateSearch,productSet,onlyApprovedItems,inventoryBlowOut,similarMaterialId,fromLicense,licenseContractIds,licenseCountflag,programIds,priceRangeLow,priceRangeHigh,fromcs,groupId,setId,setType,categoryId,controller,groupName,shared,removeBreadCrumb,inStockFilterType,cmtStandards,cmtCustomerNumber, onlyAgreementProducts){
		InsightSearch.isFromCs = fromcs;
		InsightSearch.groupName = groupName;
		InsightSearch.categoryId = categoryId;
		InsightSearch.setId = setId;
		InsightSearch.setType = setType;
		InsightSearch.groupId =groupId;
		InsightSearch.shared =  shared;
		InsightSearch.controller = controller;
		InsightSearch.cmtStandards = cmtStandards;
		InsightSearch.cmtCustomerNumber = cmtCustomerNumber;
		var searchResultsByAddressJson = '{"defaultPlant": "10","useBreadcrumb": false,"page":'+page+',"pageNumber": 0,"pageSize": 0,';
		if(InsightSearch.cmtStandards!=undefined && InsightSearch.cmtStandards=="true"){
			searchResultsByAddressJson =searchResultsByAddressJson+'"cmtStandards":true,';
			if(cmtCustomerNumber!=null){
			  searchResultsByAddressJson =searchResultsByAddressJson+'"cmtCustomerNumber":"'+cmtCustomerNumber+'",';
			}
		}
		if(nugsSortBySelect!=null)
			searchResultsByAddressJson =searchResultsByAddressJson+'"nugsSortBySelect":"'+nugsSortBySelect+'",';
		if(similarMaterialId!=null)
			searchResultsByAddressJson =searchResultsByAddressJson+'"similarMaterialId":"'+similarMaterialId+'",';
		if(fromLicense!=null)
			searchResultsByAddressJson = searchResultsByAddressJson+'"fromLicense" : '+fromLicense+',';
		if(licenseContractIds!=null)
			searchResultsByAddressJson =searchResultsByAddressJson+'"licenseContractIds":['+decodeURIComponent(licenseContractIds)+'],';
		if(licenseCountflag!=null)
			searchResultsByAddressJson = searchResultsByAddressJson+'"licenseCountflag" : '+licenseCountflag+',';
		if(programIds!=null )
			searchResultsByAddressJson =searchResultsByAddressJson+'"programIds":['+decodeURIComponent(programIds)+'],';                 
		if(priceRangeHigh!=null)
			searchResultsByAddressJson =searchResultsByAddressJson+'"priceRangeHigh":"'+priceRangeHigh+'",';
		if(priceRangeLow!=null)
			searchResultsByAddressJson =searchResultsByAddressJson+'"priceRangeLow":"'+priceRangeLow+'",';
		if(fromcs!=null)
			searchResultsByAddressJson =searchResultsByAddressJson+'"fromcs":"'+fromcs+'",';
		if(groupId!=null)
			searchResultsByAddressJson =searchResultsByAddressJson+'"groupId":"'+groupId+'",';
		if(setId!=null)
			searchResultsByAddressJson =searchResultsByAddressJson+'"setId":"'+setId+'",';
		if(categoryId!=null)
			searchResultsByAddressJson =searchResultsByAddressJson+'"categoryId":"'+categoryId+'",';            	
		if(controller!=null)
			searchResultsByAddressJson =searchResultsByAddressJson+'"controller":"'+controller+'",';
		if(groupName!=null)
			searchResultsByAddressJson =searchResultsByAddressJson+'"groupName":"'+encodeURIComponent(groupName)+'",';
		if(shared!=null)
			searchResultsByAddressJson =searchResultsByAddressJson+'"shared":"'+shared+'",';         	
		if(inventoryBlowOut!=null)
			searchResultsByAddressJson = searchResultsByAddressJson+'"inventoryBlowOut" : '+inventoryBlowOut+',';
		if(onlyApprovedItems!=null)
			searchResultsByAddressJson = searchResultsByAddressJson+'"onlyApprovedItems" : "Y",';
		if(onlyAgreementProducts!=null)
			searchResultsByAddressJson = searchResultsByAddressJson+'"onlyAgreementProducts" : "Y",';
		if(ccnRebate!=null)
			searchResultsByAddressJson =   searchResultsByAddressJson+'"ccnRebate":"CCN-Rebate","rebateSearch": true,';
		if(productSet!=null)
			searchResultsByAddressJson =   searchResultsByAddressJson+'"productSet":["'+productSet+'"],';
		if(field!=null)
		searchResultsByAddressJson =searchResultsByAddressJson+'"field":['+field+'],';
		if(category!=null && category.length>0)
		searchResultsByAddressJson =searchResultsByAddressJson+'"category":"'+category+'",';
		if(inStockFilterType!=null)
			searchResultsByAddressJson =searchResultsByAddressJson+'"inStockFilterType": "'+inStockFilterType+'",';
		var lastSearchURL = document.location.href;
		lastSearchURL = lastSearchURL.substring(lastSearchURL.indexOf('insightweb'),lastSearchURL.length);
		if (lastSearchURL != null) {
			searchResultsByAddressJson =searchResultsByAddressJson+'"returnSearchURL": "'+lastSearchURL+'",';
		}
		if(searchText.length>0){
			searchText = searchText.substring(searchText.length-(searchText.length-1),searchText.length-1);
			searchText= encodeURIComponent(searchText);
			searchText = '"'+searchText+'"';
		}
		searchResultsByAddressJson =searchResultsByAddressJson+'"noCLP":"'+noCLP+'","shown": '+shown+',"shownFlag":'+shownFlag+',"searchText": ['+searchText+'],"defaultSort": false,"salesOrg": "2400","imageURL" : "http://imagesqa01.insight.com/"}';
		var searchResultsByAddressURL =  '/insightweb/getProduct';
		if (searchResultsByAddressURL != "" && searchResultsByAddressJson != "" ){
	           var data = InsightCommon.getServiceResponse(searchResultsByAddressURL,searchResultsByAddressJson,"POST");
	           if(typeof data!=='undefined' && data!=null){
	      		 InsightCommon.getRequestJsonObject = searchResultsByAddressJson;
	      	   }
	           InsightSearch.displayResults(data);
	    }else
	         alert(searchErrorMessages.labels.PleaseenterURLANDJSON);
	         removeObjectSession();
	},
	/* function to map domain URL to the B2BUser's Cancel-Exit URL in the header */
	cancelExitURL : function(){
		var requestObj = {'cancel':true,'extrinsic':InsightB2BUserAttributes.extrinsicName,'token':InsightB2BUserAttributes.token};
		requestObj = JSON.stringify(requestObj);
		var cartCXMLServiceUrl = '/insightweb/b2b/cxml/transferCart';
		var cartOCIServiceUrl = '/insightweb/b2b/oci/transferOCICart';
		var cartOAGServiceUrl = '/insightweb/b2b/oag/transferOagCart';
		var data = {};
		if(InsightB2BUserAttributes.eProcType=="CX"){
			 data = InsightCommon.getServiceResponse(cartCXMLServiceUrl, requestObj, 'POST');
			 var form = document.createElement("form");
			 form.setAttribute("method", "POST");
			 form.setAttribute("action", data.setupToken.browserFormPost);
			 form.setAttribute("target", data.target);
	         var hiddenField = document.createElement("input");
	         hiddenField.setAttribute("type", "hidden");
	         hiddenField.setAttribute("name", "cxml-urlencoded");
	         hiddenField.setAttribute("value", data.formPostHtml);
	         form.appendChild(hiddenField);
	         document.body.appendChild(form);
	         form.submit();
		}else if(InsightB2BUserAttributes.eProcType=="EB"){
			data = InsightCommon.getServiceResponse(cartOCIServiceUrl, requestObj, 'POST');
			document.location.href = data.setupToken.browserFormPost;
		}else if(InsightB2BUserAttributes.eProcType=="OA"){
			data = InsightCommon.getServiceResponse(cartOAGServiceUrl, requestObj, 'POST');
			document.location.href = data.setupToken.browserFormPost;
		}else{
			if (window.console && window.console.log){
				window.console.log("Unexpected eProcType "+InsightB2BUserAttributes.eProcType+" in InsightSearch.cancelExitURL()");
			}else{
				alert("Unexpected eProcType "+InsightB2BUserAttributes.eProcType+" in InsightSearch.cancelExitURL()");
	        }
	    }
	}
}})();

var searchTxt="";
var groupId = null;
var setId = null;
var setType = null;
var categoryId = null;
var controller = null;
var groupName = null;
var shared = null;
var fromcs =false;
var additionalSearchText = "";
var totalRecords=0;
var nugsSortBy = null;
var userBreadCrumb= false;
var advanceSearch=null;
var mainCatagorySelected = null;
var subCategorySelected = null;
var mainCategory = null;
var advSearchSubmit = null;
var manufactureIdSelected = [];
var removeBreadCrumb = null;
var languageSelected = null;
var productTypeSelected = null;
var platformSelected = null;
var priceRangeLow = null;
var priceRangeHigh = null;
var fromLicense = null;
var licenseContractIds = null;
var licenseCountflag = null;
var programIds = null;
var advanceSearchTxt=null;
var advanceSearchSubmitData=null;
var productSet = null;
var firstTimeLoad=false;
var pageFrom=1;
var pageTo=10;
var getPage=1;
var pager;
var sortByForBreadCrumb=false;
var bundleFlag=false;
var bundleGrpName = null;
//new
var previousResultsPage;
var nextResultsPage;
var numOfPages;
var firstPage;
var prevPage;
var nextPage;
var lastPage;
var linkNumber;
var currentPageNum;
var narrowData = null;
//narrowDownResults
var fieldId=null;
var fieldValue=null;
var narrowCategory = null;
var removeBreadCrumb = null;
var fieldBread = null;
var firstBreadCrumb = null;
var inventoryBlowOut = false;
var noKeywordFlag = false;
var searchLabels=InsightSearch.globalSearchLabels;
var globalSearchMetaTagLabels = InsightSearch.globalSearchMetaTagLabels;
var searchErrorMessages=null;
var paginationFlag = false;
var similarMaterialId = null;
var imageServerURL = '';
var productSetMenu = null;
var relatedCategory = null;
var removeRebateFlag = false;
var rebateSearchFlag = false;
var removeApprovedItemsFlag = false;
var removeAgreementProductsFlag = false;
var agreementProductsFromLeftNavFlag = false;
/* saved product compare box variables */
var savedProducts = [];
var shownScriptFlag = false;
var approvedItemsFromLeftNavFlag = false;
/* product compare page add mfr part number */
var fromAddMfrPartNumFlag = false;
/* invalid material ids in product compare */
var invalidMaterialIds = [];
/* checkbox for approved items only */
var approvedItemsOnly = false;
var jsonCompareProductsEmail="";
/* advance search user preference data*/
var userSoftSearchData = null;
var sDate = new Date();
sDate = (sDate.getMonth()+1)+"/"+sDate.getDate()+"/"+sDate.getFullYear();
function GetLPMenuData(url){
      var lpId = url.substring(url.indexOf('=')+1,url.length);
      GetLPData(lpId);
}
function GetLPData(lpId){
      var url="/insightweb/getLPData/";
      var jsonObj= '{"lpId":"'+lpId+'"}';
      if (url != "" && jsonObj != "" ){
          var data = InsightCommon.getServiceResponse(url,jsonObj,"POST");
          InsightSearch.displayResults(data);
      }else
          alert(searchErrorMessages.labels.PleaseenterURLANDJSON);
}
function narrowDownResults(fieldIdInput, fieldValueInput,catId,IBdata){
      fieldId  = fieldIdInput;
      fieldValue  = fieldValueInput;
      fromcs = $("#fromcs").val();
	  if(fromcs == 'true')
		fromcs = true;
	  else
		fromcs = false;
	  if(fromcs){
	     	searchTxt = $("#searchCSText").val();
		  	groupId = $("#groupId").val();
		  	setId = $("#setId").val();
		  	categoryId = catId;
		  	controller = $("#controller").val();
		  	groupName = $("#groupName").val();
		  	shared = $("#shared").val();
		  	approvedItemsOnly = false;
		  	InsightSearch.cmtStandards=$("#cmtStandards").val();
		  	InsightSearch.cmtCustomerNumber=$("#cmtCustomerNumber").val();
	  }  
      priceRangeLow = $('#priceRangeLow').val();	            		   
	  priceRangeHigh = $('#priceRangeHigh').val();
	  if(fieldIdInput!=null && fieldIdInput == 'A-XPM-ProgramID'){
		  $('#programIds').val(fieldValueInput);		 
	  }
	  softwareLicense();
	  if(catId==''){
	      narrowCategory =null;
	      if(IBdata=='true')
	      inventoryBlowOut= true;
      }else
    	  narrowCategory = catId;
      /* pagintion Flag*/
      paginationFlag = true;
      InsightSearch.postData();
}
function narrowUpResults(narrrowField,catId){
      if(narrrowField!=null){
    	  narrrowField =narrrowField.replaceAll('"','\\"'); 
          fieldBread  = '"'+narrrowField+'"';
      }else
            firstBreadCrumb = "";
      narrowCategory = categoryId;
      fromcs = $("#fromcs").val();
	  if(fromcs == 'true')
		fromcs = true;
	  else
		fromcs = false;
      if(narrrowField!=null && narrrowField.indexOf('A-XPM-ProgramID') !=-1){
    	  removeCategory = categoryId;
	  }
	  softwareLicense();
	  if(fromcs){
	     	searchTxt = $("#searchCSText").val();
		  	groupId = $("#groupId").val();
		  	setId = $("#setId").val();
		  	categoryId = catId;
		  	controller = $("#controller").val();
		  	groupName = $("#groupName").val();
		  	shared = $("#shared").val();
		  	InsightSearch.cmtStandards=$("#cmtStandards").val();
		  	InsightSearch.cmtCustomerNumber=$("#cmtCustomerNumber").val();
		  	approvedItemsOnly = false;
	  }  
      priceRangeLow = $('#priceRangeLow').val();	            		   
	  priceRangeHigh = $('#priceRangeHigh').val();
	  /* pagintion Flag*/
      paginationFlag = true;
      InsightSearch.postData();
}
function showLoading(){
      $("#loading").show();   
}
function hideLoading(){
      $("#loading").hide();
}
function postAdvanceData(search1){
      advanceSearch = ""+search1;
      searchTxt = "";
      $("#searchText").val("");
      removeBreadCrumb = null;
      narrowCategory = null;
      mainCatagorySelected = null;
      subCategorySelected = null;
      mainCategory = null;
      advSearchSubmit = null;
      manufactureIdSelected = [];
      languageSelected = null;
      platformSelected = null;
      productTypeSelected = null;
      priceRangeLow = null;
      priceRangeHigh = null;
      advanceSearchTxt=null;
      narrowData = null;
      fromcs = $("#fromcs").val();
      if(fromcs == 'true')
      		fromcs = true;
      else
      		fromcs = false;
      if(fromcs){
         	searchTxt = $("#searchCSText").val();
    	  	groupId = $("#groupId").val();
    	  	setId = $("#setId").val();
    	  	categoryId = $("#categoryId").val();
    	  	controller = $("#controller").val();
    	  	groupName = $("#groupName").val();
    	  	shared = $("#shared").val();
    	  	InsightSearch.cmtStandards=$("#cmtStandards").val();
    	  	InsightSearch.cmtCustomerNumber=$("#cmtCustomerNumber").val();
    	  	approvedItemsOnly = false;
      }
      InsightSearch.postData();
}
function onShownChange(){
      getPage=1;
      shownScriptFlag = true;
      fromcs = $("#fromcs").val();
      if(fromcs == 'true')
    	  fromcs = true;
      else
    	  fromcs = false;
      softwareLicense();
      if(fromcs){
       	searchTxt = $("#searchCSText").val();
  	  	groupId = $("#groupId").val();
  	  	setId = $("#setId").val();
  	  	categoryId = $("#categoryId").val();
  	  	controller = $("#controller").val();
  	  	groupName = $("#groupName").val();
  	  	shared = $("#shared").val();
  	    InsightSearch.cmtStandards=$("#cmtStandards").val();
  	  	InsightSearch.cmtCustomerNumber=$("#cmtCustomerNumber").val();
  	  	approvedItemsOnly = false;
   	  }        
      priceRangeLow = $('#priceRangeLow').val();	            		   
	  priceRangeHigh = $('#priceRangeHigh').val();
	  InsightSearch.postData();
}
function softwareLicense(){
	fromLicense = $('#fromLicense').val();
	if(fromLicense == 'true')
		fromLicense = true;
	else
		fromLicense = false;
    licenseContractIdArray = $('#licenseContractIds').val();
    if(Insight.isLoggedin && licenseContractIdArray!=""){  
	  	var licenseContractIdList = licenseContractIdArray.split(",");
	  	licenseContractIds = new Array();
	  	for(var l=0;l<licenseContractIdList.length;l++){
	  		licenseContractIds.push('"'+licenseContractIdList[l]+'"');
	  	}
    }
    licenseCountflag = $('#licenseCountflag').val();
	  var lContractIds = $('#programIds').val();
	  if(Insight.isLoggedin && $.trim(lContractIds).length > 0){
		  var licContractIds = lContractIds.split(",");
		  programIds = new Array();
		  for(var l=0;l<licContractIds.length;l++){
		    	programIds.push('"'+licContractIds[l]+'"');
		  }	
	  }
}
function postDataBycategory(productSet1){
	if((productSet1 != "top_standardWarranties") && (productSet1 != "top_warrantiesGroup")){ 
		productSetMenu = ""+productSet1;
		if(productSetMenu!=null)  {
            advanceSearch = null;
            postSearchByCriteria(false,false,false,null);
        }
	}else{
		productSet1 = "C-ZF";
		advanceSearch = productSet1;
        postSearchByCriteria(false,false,false,productSet1);		
	}
}
/* ***getNextPrevPage takes us to next page or previous page*** */
function getNextPrevPage(prevNext){
	fromcs = $("#fromcs").val();
	if(fromcs == 'true')
		fromcs = true;
	else
		fromcs = false;
	if(fromcs){
	 	searchTxt = $("#searchCSText").val();
	  	groupId = $("#groupId").val();
	  	setId = $("#setId").val();
	  	categoryId = $("#categoryId").val();
	  	controller = $("#controller").val();
	  	groupName = $("#groupName").val();
	  	shared = $("#shared").val();
	  	InsightSearch.cmtStandards=$("#cmtStandards").val();
	    InsightSearch.cmtCustomerNumber=$("#cmtCustomerNumber").val();
	  	approvedItemsOnly = false;
	}  
	softwareLicense();
	priceRangeLow = $('#priceRangeLow').val();	            		   
	priceRangeHigh = $('#priceRangeHigh').val();
	if(prevNext=="Prev"){
		getPage=currentPageNum-1; InsightSearch.postData();
	}else{
		getPage=currentPageNum+1; InsightSearch.postData();
	}
}

/* ***Create Pagination in paging Div*** */
function createPagination(currPage,nowShowing){
      numOfPages= Math.ceil(totalRecords / nowShowing);
      if(numOfPages == 1){
            $("#shown").hide();
            $("#paging").hide();
            return false;
      }
      $("#shown").show();
      var pagerHtml;
      // show 'previous results' link
      if (currPage - 1 > 0){
            previousResultsPage=currPage-1;
            $('#prevRecords').html("&lt;prev "+$("#shownRecords").val()+" results");
            $('#prevRecords').mousedown(function() {InsightCommon.showLoading();});
      }else
            $('#prevRecords').html("");
      // show 'next results' link
      if (currPage < numOfPages){
            nextResultsPage=currPage+1;
            $('#nextRecords').html("next "+$("#shownRecords").val()+" results&gt;");
            $('#nextRecords').mousedown(function() {InsightCommon.showLoading();});
      }else
            $('#nextRecords').html("");
      // show '|< link'
      if(currPage > 1){
            firstPage=1;
            pagerHtml='<strong><a onmousedown="InsightCommon.showLoading();" href="javascript:firstPagefn();" > |&lt;</a></strong>&nbsp;';
      }else{
            //change style to disabled mode
            pagerHtml='<strong style=\"color:#cdcdcd;\">|&lt;</strong>&nbsp;';
      }
      //show '<< link' that takes the user back 1 set of links
      if (currPage > 10){
            prevPage=currPage - 10;
            pagerHtml += '<strong><a onmousedown="InsightCommon.showLoading();" href="javascript:prevPagefn();"> &lt;&lt; </a></strong>&nbsp;';
      }else{
            //change style to disabled mode
            pagerHtml += '<strong style=\"color:#cdcdcd;\"> &lt;&lt; </strong>&nbsp;';
      }
      //The value of the page number link that is the leftmost
      linkNumber =  currPage - 5;
      while(numOfPages - linkNumber < 10){
            linkNumber--;
      }
      if(linkNumber < 1) {
            linkNumber = 1;
      }
      if (linkNumber < 1) {
            linkNumber = 1;
      }
      if (currPage < 1) {
            currPage = 1;
      }
      for (var count=1;linkNumber <= numOfPages && count < 12; linkNumber++,count++) {
            if(linkNumber==currPage)
                  pagerHtml += '&nbsp;'+ linkNumber + '&nbsp;';
            else
                  pagerHtml += '&nbsp;<strong><a id="pg' + linkNumber + '" href="javascript:showThisPage(' + linkNumber + ');" onmousedown="InsightCommon.showLoading();">' + linkNumber + '</a> &nbsp;</strong>';
      }
      // show '>> link'
      if (linkNumber + 4 <= numOfPages) {
            nextPage=linkNumber + 4;
            pagerHtml += '&nbsp;<strong><a onmousedown="InsightCommon.showLoading();" href="javascript:nextPagefn();"> &gt;&gt; </a></strong>';
      }
      else
            pagerHtml += '&nbsp;<strong style=\"color:#cdcdcd;\"> &gt;&gt; </strong>';
      // show '>| link'
      if (currPage < numOfPages) {
            lastPage=numOfPages;
            pagerHtml += '&nbsp;<strong><a onmousedown="InsightCommon.showLoading();" href="javascript:lastPagefn();" > &gt;|</a></strong>';
      }
      else
            pagerHtml += '&nbsp;<strong style=\"color:#cdcdcd;\">&gt;|</strong>';
      $('#paging').html(pagerHtml);

}
/* ***End of paging*** */
/* ***Helper functions for paging*** */

function firstPagefn(){
      getPage=firstPage;
      fromcs = $("#fromcs").val();
      if(fromcs == 'true')
      	fromcs = true;
      else
      	fromcs = false;
      if(fromcs){
      	searchTxt = $("#searchCSText").val();
      	groupId = $("#groupId").val();
      	setId = $("#setId").val();
      	categoryId = $("#categoryId").val();
      	controller = $("#controller").val();
      	groupName = $("#groupName").val();
      	shared = $("#shared").val();
      	InsightSearch.cmtStandards=$("#cmtStandards").val();
      	InsightSearch.cmtCustomerNumber=$("#cmtCustomerNumber").val();
      	approvedItemsOnly = false;
      }
      InsightSearch.postData();
}
function prevPagefn(){
      getPage=prevPage;
      fromcs = $("#fromcs").val();
      if(fromcs == 'true')
      	fromcs = true;
      else
      	fromcs = false;
      if(fromcs){
      	searchTxt = $("#searchCSText").val();
      	groupId = $("#groupId").val();
      	setId = $("#setId").val();
      	categoryId = $("#categoryId").val();
      	controller = $("#controller").val();
      	groupName = $("#groupName").val();
      	shared = $("#shared").val();
      	InsightSearch.cmtStandards=$("#cmtStandards").val();
      	InsightSearch.cmtCustomerNumber=$("#cmtCustomerNumber").val();
      	approvedItemsOnly = false;
      }
      InsightSearch.postData();
}
function nextPagefn(){
      getPage=nextPage;
      fromcs = $("#fromcs").val();
      if(fromcs == 'true')
      	fromcs = true;
      else
      	fromcs = false;
      if(fromcs){
      	searchTxt = $("#searchCSText").val();
      	groupId = $("#groupId").val();
      	setId = $("#setId").val();
      	categoryId = $("#categoryId").val();
      	controller = $("#controller").val();
      	groupName = $("#groupName").val();
      	shared = $("#shared").val();
      	InsightSearch.cmtStandards=$("#cmtStandards").val();
      	InsightSearch.cmtCustomerNumber=$("#cmtCustomerNumber").val();
      	approvedItemsOnly = false;
      }
      InsightSearch.postData();
}
function lastPagefn(){
      getPage=lastPage;
      fromcs = $("#fromcs").val();
      if(fromcs == 'true')
      	fromcs = true;
      else
      	fromcs = false;
      if(fromcs){
      	searchTxt = $("#searchCSText").val();
      	groupId = $("#groupId").val();
      	setId = $("#setId").val();
      	categoryId = $("#categoryId").val();
      	controller = $("#controller").val();
      	groupName = $("#groupName").val();
      	shared = $("#shared").val();
      	InsightSearch.cmtStandards=$("#cmtStandards").val();
      	InsightSearch.cmtCustomerNumber=$("#cmtCustomerNumber").val();
      	approvedItemsOnly = false;
      }
      InsightSearch.postData();
}
function showThisPage(pageNum){
    fromcs = $("#fromcs").val();
    if(fromcs == 'true')
    	fromcs = true;
    else
    	fromcs = false;
    if(fromcs){
    	searchTxt = $("#searchCSText").val();
        groupId = $("#groupId").val();
        setId = $("#setId").val();
        categoryId = $("#categoryId").val();
        controller = $("#controller").val();
        groupName = $("#groupName").val();
        shared = $("#shared").val();
        InsightSearch.cmtStandards=$("#cmtStandards").val();
        InsightSearch.cmtCustomerNumber=$("#cmtCustomerNumber").val();
        approvedItemsOnly = false;
    }
    getPage=pageNum;
    softwareLicense();
    InsightSearch.postData();
}
function nextRecords(){
      pager.getNextPrevPage("Next");
}
function prevRecords(){
      pager.getNextPrevPage("Prev");
}
/* ***End of helper functions for paging*** */

function isEmpty(str) {
      return (!str || 0 === str.length);
}
function searchResults(e, input){
	var intKey = (window.Event) ? e.which : e.keyCode;
    if (intKey == 13 || e.keyCode ==13) { //enter key
    	if(input  == null) {
    		searchTxt = $("#searchText").val();
    	}else {
    		searchTxt = $(input).val();
    	}
    	var usrSrch = searchTxt;
        imageServerURL = $("#imgServer").val();
        if($('#approvedItemsOnly').is(':checked'))
        approvedItemsOnly = true;
        searchTxt = '"'+searchTxt+'"';
        narrowData = null;
        productSet = null;
        nugsSortBy = null;
        /* pagintion Flag*/
        paginationFlag = true;
    	fromcs = $("#fromcs").val();
		if(fromcs == 'true')
			fromcs = true;
		else
			fromcs = false;
	    if(fromcs){
	       	searchTxt = $("#searchCSText").val();
	  	  	groupId = $("#groupId").val();
	  	  	setId = $("#setId").val();
	  	  	categoryId = $("#categoryId").val();
	  	  	controller = $("#controller").val();
	  	  	groupName = $("#groupName").val();
	  	  	shared = $("#shared").val();
	  	  	InsightSearch.cmtStandards=$("#cmtStandards").val();
	  	  	InsightSearch.cmtCustomerNumber=$("#cmtCustomerNumber").val();
	  	  	approvedItemsOnly = false;
	   	}
        /*
		 * this is for the feature to go to advance search page when no
		 * keyword is given
		 */
		if (searchTxt == "" || searchTxt == null || searchTxt == " " || searchTxt == '""'){
			advanceSearchSubmitData = null;
			noKeywordFlag = true;
			searchTxt = null;
			advanceSearch = "advSearch";
			postAdvanceData(advanceSearch);
		}else{
		    InsightCommon.showLoading();
			// check if the search string has any landing page in CMS
		
		    var jsonReqS=	'{"text":'+searchTxt+',"locale":"'+Insight.locale+'","url":"n"}';
			var data = InsightCommon.getServiceResponse("/insightweb/cloud/keyword",jsonReqS,"POST");
			if(data!=null && data.url!=null){
				window.location=data.url;
				return;
			}
			var testPermission = Insight.userPermissions;
			if ((Insight.isLoggedin && ( $.inArray('shop_by_brand',testPermission) != -1) )  || (!Insight.isLoggedin)){
				var r = checkforCMSPage(usrSrch);
			    if (r!=null && r!="null"){		                  
			       window.location = Insight.envConfigs.cmsServer + r;
				}else{
					if (approvedItemsOnly)
						postSearchByCriteria(false, false, true, null);
					else
						InsightSearch.postData();
				}
			}else{
        		if(approvedItemsOnly)
		    		postSearchByCriteria(false,false,true,null);
		    	else
		    		InsightSearch.postData();
			}
		}
		return false;
    }
    return true;
}
$(document).ready(function(){
	InsightSearch.loadSearchLabels();
	if(typeof InsightSavedProducts!='undefined') {
    	savedProducts = InsightSavedProducts.savedProducts;
	}	
	if(document.location.pathname=='/insightweb/search'){
	    InsightCommon.showLoading();
	    var searchLabels = InsightSearch.loadSearchLabels();
		var showSug= $('#showSuggest').val();
	    var errmsgs = InsightCommon.getMessageContent(InsightSearch.messageUrl, Insight.locale, "http://www.insight.com/messages/search/errorMessages.txt");
	    searchErrorMessages = eval('(' + errmsgs + ')');
	    $('#searchBtn').click(function(){
	    	    searchTxt = $("#searchText").val();
	            var usrSrch = searchTxt;
	            imageServerURL = $("#imgServer").val();
	            if($('#approvedItemsOnly').is(':checked'))
	                approvedItemsOnly = true;
	            searchTxt = '"'+searchTxt+'"';
	            narrowData = null;
	            productSet = null;
	            nugsSortBy = null;
	            /* pagintion Flag*/
	            paginationFlag = true;
	    		fromcs = $("#fromcs").val();
    			if(fromcs == 'true')
    				fromcs = true;
    			else
    				fromcs = false;
    			if(fromcs){
    				searchTxt = $("#searchCSText").val();
    				groupId = $("#groupId").val();
    				setId = $("#setId").val();
    				categoryId = $("#categoryId").val();
    				controller = $("#controller").val();
    				groupName = $("#groupName").val();
    				shared = $("#shared").val();
    				InsightSearch.cmtStandards=$("#cmtStandards").val();
    				InsightSearch.cmtCustomerNumber=$("#cmtCustomerNumber").val();
    				approvedItemsOnly = false;
    			}
	            /* this is for the feature to go to advance search page when no keyword is given */
	            if(searchTxt=="" || searchTxt==null || searchTxt==" " || searchTxt=='""'){
	                  advanceSearchSubmitData = null;
	                  noKeywordFlag = true;
	                  searchTxt = null;
	                  advanceSearch="advSearch";
	                  postAdvanceData(advanceSearch);
	            }else{
	            	// check if the search string has any landing page in CMS
	            	var jsonReqS=	'{"text":'+searchTxt+',"locale":"'+Insight.locale+'","url":"n"}';
	    			var data = InsightCommon.getServiceResponse("/insightweb/cloud/keyword",jsonReqS,"POST");
	    			if(data!=null && data.url!=null){
	    				window.location=data.url;
	    				return;
		    		}
	    			var testPermission = Insight.userPermissions;
				 	if ((Insight.isLoggedin && ( $.inArray('shop_by_brand',testPermission) != -1) )  || (!Insight.isLoggedin)) {	
		    			var r =  checkforCMSPage(usrSrch);
		    		    if (r!=null && r!="null"){		    		                  
		    		    	window.location = Insight.envConfigs.cmsServer + r;
		    			}else{
			            	if(approvedItemsOnly)
					    		postSearchByCriteria(false,false,true,null);
					    	else
					            InsightSearch.postData();
			    		}
					 }else{
				 		if(approvedItemsOnly)
	   			    		postSearchByCriteria(false,false,true,null);
	   			    	else
	   			    		InsightSearch.postData();
					 }
	            }
	
	      });
	     $('#searchBtn-footer').click(function() {
	    	 searchTxt = $("#searchText-footer").val();
	    	 var usrSrch = searchTxt;
	    	 imageServerURL = $("#imgServer").val();
	         if($('#approvedItemsOnly').is(':checked'))
	             approvedItemsOnly = true;
	         searchTxt = '"'+searchTxt+'"';
	         narrowData = null;
	         productSet = null;
	         nugsSortBy = null;
	         /* pagintion Flag*/
	         paginationFlag = true;
	         console.log(searchTxt);
	         /* this is for the feature to go to advance search page when no keyword is given */
	         fromcs = $("#fromcs").val();
	         if(fromcs == 'true')
	         	fromcs = true;
	         else
	         	fromcs = false;
	         if(fromcs){
		         	searchTxt = $("#searchCSText").val();
		         	groupId = $("#groupId").val();
		         	setId = $("#setId").val();
		         	categoryId = $("#categoryId").val();
		         	controller = $("#controller").val();
		         	groupName = $("#groupName").val();
		         	shared = $("#shared").val();
		         	InsightSearch.cmtStandards=$("#cmtStandards").val();
		         	InsightSearch.cmtCustomerNumber=$("#cmtCustomerNumber").val();
		         	approvedItemsOnly = false;
	         }
	         if(searchTxt=="" || searchTxt==null || searchTxt==" " || searchTxt=='""'){
	               advanceSearchSubmitData = null;
	               noKeywordFlag = true;
	               searchTxt = null;
	               advanceSearch="advSearch";
	               postAdvanceData(advanceSearch);
	         } else{	        	 
	        	 var jsonReqS=	'{"text":'+searchTxt+',"locale":"'+Insight.locale+'","url":"n"}';
	        	 var data = InsightCommon.getServiceResponse("/insightweb/cloud/keyword",jsonReqS,"POST");
 	 			 if(data!=null && data.url!=null){
 	 				window.location=data.url;
 	 				return;
 	 			 } 				
	 			 var r =  checkforCMSPage(usrSrch);
	 	 	     if (r!=null && r!="null") {	 	 	                  
	 	 	       window.location = Insight.envConfigs.cmsServer + r;
	 			 }else{
		         	if(approvedItemsOnly)
				    	postSearchByCriteria(false,false,true,null);
				    else
				        InsightSearch.postData();
	 			}
	         }
	     });
	     var redirectFunction = $("#redirectFunction").val();
	     if(redirectFunction != null){
	    	  if(redirectFunction == "demoCompanyStandards")
	    		  renderDemoCompanyStandards();
	    	  else if(redirectFunction == "userPreferences")
	    		  userPreferencePage();
	    	  else if(redirectFunction == "configurators")
	    		  getConfiguratorsPage();
	    	  else if(redirectFunction == "productCenter")
	    		  productCenter();
	    	  else if(redirectFunction == "productCompare")
	    		  viewProductCompareList();
	    	  else if(redirectFunction == "productSet"){
	    		  var category = $("#category").val();
	    		  postDataBycategory(category);	  
	    	  }
	     }
	}else {
    	 $('#searchBtn').click(function() {
	    	 searchTxt = $("#searchText").val();
	    	 var jsonReqS=	'{"text":"'+searchTxt+'","locale":"'+Insight.locale+'","url":"n"}';
	    	 var data = InsightCommon.getServiceResponse("/insightweb/cloud/keyword",jsonReqS,"POST");
			 if(data!=null && data.url!=null){
				window.location=data.url;
				return;
			 }				
	    	 imageServerURL = $("#imgServer").val();
	         if($('#approvedItemsOnly').is(':checked'))
	             approvedItemsOnly = true;
	         	searchSubmit(searchTxt,approvedItemsOnly) ;
    	 });
    	 $('#searchBtn-footer').click(function() {
        	 searchTxt = $("#searchText-footer").val();
        	 var jsonReqS=	'{"text":"'+searchTxt+'","locale":"'+Insight.locale+'","url":"n"}';
        	 var data = InsightCommon.getServiceResponse("/insightweb/cloud/keyword",jsonReqS,"POST");
   			 if(data!=null && data.url!=null){
 	 				window.location=data.url;
 	 				return;
 	 		 } 		
        	 imageServerURL = $("#imgServer").val();
             if($('#approvedItemsOnly').is(':checked'))
                 approvedItemsOnly = true;
             searchSubmit(searchTxt,approvedItemsOnly,'') ;
        });
    }
});

function CSSubmitSearch(csetid,groupid){
	  searchTxt = $("#searchCSText").val();
	  groupId = $("#groupId").val();
	  categoryId = $("#categoryId").val();
	  setId = $("#setId").val();
	  controller = $("#controller").val();
	  groupName = $("#groupName").val();
	  shared = $("#shared").val();
	  InsightSearch.cmtStandards=$("#cmtStandards").val();
	  InsightSearch.cmtCustomerNumber=$("#cmtCustomerNumber").val();
	  approvedItemsOnly = false;
	  fromcs=true;
	  var setType = $("#setType").val();
	  var usrSrch = searchTxt;
      var grname = encodeURIComponent(groupName);
      narrowData = null;
      productSet = null;
      nugsSortBy = null;
      /* pagintion Flag*/
      paginationFlag = true;
      /* this is for the feature to go to advance search page when no keyword is given */
      if(searchTxt=="" || searchTxt==null || searchTxt==" " || searchTxt=='""'){
            advanceSearchSubmitData = null;
            noKeywordFlag = true;
            searchTxt = null;
            advanceSearch="advSearch";
            postAdvanceData(advanceSearch);
      }else{
      	// check if the search string has any landing page in CMS
      		if(InsightNavigation.isEadminUser){
      			$.ajax({
      		         url: '/insightweb/popUpURL',
      		         type: "POST",
      		         global: false,
      		         async: false,
      		         crossDomain: true,
      		         success: function(data) {
      		        	 var miniWindowleft = ($(window).width - 760)/2;
      		        	 var miniWindowheight = ($(window).height - 600)/2;
      		        	 var url =null;      		        	 
      		        	 url= encodeURI('search?redirectTo=cs#searchTxt=' + encodeURIComponent(searchTxt)+ '#groupId='+ groupId +'#setId='+setId+'#setType='+setType+ '#categoryId=' +categoryId+  
	 								'#controller='+controller+'#groupName='+grname+'#shared='+shared+'#approvedItemsOnly=false#fromcs=true#cmtStandards=true#cmtCustomerNumber='+webGroupId);
      		        	 var minipppWindow = window.open(url, '', 'width=790px, height=600px, left='+miniWindowleft+',top='+miniWindowheight+' ,scrollbars=yes',true);
      		        	 minipppWindow.focus();
      		         },
      		         error: function(data){}
      		   });
     		}else{
      				searchTxt = '"'+searchTxt+'"';
		            InsightSearch.postData();
      		}
  	  } 
}

function searchCSSubmit(sText,grpId,sId,sType,catId,contr,grpName,share,isApproved,fromc,isCMTStandards,cmtCustomerNo){
	var showSug= $('#showSuggest').val();
    searchTxt = sText;
    imageServerURL = $("#imgServer").val();
    if(isApproved=="true")
        approvedItemsOnly =true;
    searchTxt = '"'+searchTxt+'"';
    narrowData = null;
    productSet = null;
    nugsSortBy = null;
    /* pagintion Flag*/
    paginationFlag = true;
    fromcs = fromc;
    groupId = grpId;
  	setId = sId;
  	setType = sType;
  	categoryId = catId;
  	controller = contr;
  	groupName = grpName;
  	shared = share;
  	if(isCMTStandards!=undefined){
  		InsightSearch.cmtStandards = isCMTStandards;
  	} 
  	InsightSearch.cmtCustomerNumber=cmtCustomerNo; 	
    /* this is for the feature to go to advance search page when no keyword is given */
    if(searchTxt=="" || searchTxt==null || searchTxt==" " || searchTxt=='""'){
          advanceSearchSubmitData = null;
          noKeywordFlag = true;
          searchTxt = null;
          postAdvanceData(advanceSearch);
    }else{
    	InsightSearch.postData();
	}
}

function searchSubmit(sText,isApproved){
	var showSug= $('#showSuggest').val();
    searchTxt = sText;
    imageServerURL = $("#imgServer").val();
     if(isApproved=="true")
        approvedItemsOnly =true;
    searchTxt = '"'+searchTxt+'"';
    narrowData = null;
    productSet = null;
    nugsSortBy = null;
    /* pagintion Flag*/
    paginationFlag = true;
    /* this is for the feature to go to advance search page when no keyword is given */
    fromcs = $("#fromcs").val();
    if(fromcs == 'true')
    	fromcs = true;
    else
    	fromcs = false;
    if(fromcs){
    	searchTxt = $("#searchCSText").val();
    	groupId = $("#groupId").val();
    	setId = $("#setId").val();
    	categoryId = $("#categoryId").val();
    	controller = $("#controller").val();
    	groupName = $("#groupName").val();
    	shared = $("#shared").val();
    	InsightSearch.cmtStandards=$("#cmtStandards").val();
    	InsightSearch.cmtCustomerNumber=$("#cmtCustomerNumber").val();
    	approvedItemsOnly = false;
    }
    if(searchTxt=="" || searchTxt==null || searchTxt==" " || searchTxt=='""'){
      advanceSearchSubmitData = null;
      noKeywordFlag = true;
      searchTxt = null;
      advanceSearch="advSearch";
      postAdvanceData(advanceSearch);
    }else{
    	usrSrch = sText;
    	var testPermission = Insight.userPermissions;        	
    	if ((Insight.isLoggedin && ( $.inArray('shop_by_brand',testPermission) != -1) )  || (!Insight.isLoggedin)){
    		var r = checkforCMSPage(usrSrch);
	        if (r!=null && r!="null"){
	             window.location = Insight.envConfigs.cmsServer + r;
			}else{
	        	if(approvedItemsOnly)
		    		postSearchByCriteria(false,false,true,null);
		    	else
		    		InsightSearch.postData();
			}
        }else{
    		if(approvedItemsOnly)
	    		postSearchByCriteria(false,false,true,null);
	    	else
	    		InsightSearch.postData();
        }
    }

   var redirectFunction = $("#redirectFunction").val();
   if(redirectFunction != null){
	  if(redirectFunction == "demoCompanyStandards")
		  renderDemoCompanyStandards();
	  else if(redirectFunction == "userPreferences")
		  userPreferencePage();
	  else if(redirectFunction == "configurators")
		  getConfiguratorsPage();
	  else if(redirectFunction == "productCenter")
		  productCenter();
	  else if(redirectFunction == "productCompare")
	  	  viewProductCompareList();
	  else if(redirectFunction == "productSet") {
		  var category = $("#category").val();    		  
		  postDataBycategory(category);	    		  
	  }
  }
}

 function narrowKeywordSearch(e){
	  var intKey = (window.Event) ? e.which : e.keyCode;
      if (intKey == 13 || e==='' || e.keyCode==13){
         additionalSearchText = $('#narrowSearchInput').val();
         $("#narrowSearchInput").replaceWith('<input name="additionalSearchText" id="narrowSearchInput" type="text" value="" onkeypress="return narrowKeywordSearch(event);" />');
         fromcs = $("#fromcs").val();
         if(fromcs == 'true')
        	fromcs = true;
         else
        	fromcs = false;
          if(fromcs){
        	 searchTxt = $("#searchCSText").val();
        	 groupId = $("#groupId").val();
        	 setId = $("#setId").val();
        	 categoryId = $("#categoryId").val();
        	 controller = $("#controller").val();
        	 groupName = $("#groupName").val();
        	 shared = $("#shared").val();
        	 InsightSearch.cmtStandards=$("#cmtStandards").val();
        	 InsightSearch.cmtCustomerNumber=$("#cmtCustomerNumber").val();
        	 approvedItemsOnly = false;
          }
          InsightSearch.postData();
          return false;
        }
	     return true;
}
 
var removeText = null;
var removeCategory = null;
function breadCrumbRemove(removeBread,removeCat, removeTxt){
	  priceRangeLow = $('#priceRangeLow').val();	            		   
	  priceRangeHigh = $('#priceRangeHigh').val();	
	  fromcs = $("#fromcs").val();
	  var removeBreadStr = null;
	  if(fromcs == 'true')
		  fromcs = true;
	  else
		  fromcs = false;
	  if(fromcs){
	     	searchTxt = $("#searchCSText").val();
		  	groupId = $("#groupId").val();
		  	setId = $("#setId").val();
		  	categoryId = $("#categoryId").val();
		  	controller = $("#controller").val();
		  	groupName = $("#groupName").val();
		  	shared = $("#shared").val();
		  	InsightSearch.cmtStandards=$("#cmtStandards").val();
		  	InsightSearch.cmtCustomerNumber=$("#cmtCustomerNumber").val();
		  	approvedItemsOnly = false;
	   }  
		fromLicense = $('#fromLicense').val();
		licenseContractIdArray = $('#licenseContractIds').val();
		
		if(Insight.isLoggedin && licenseContractIdArray!=""){   
		  var licenseContractIdList = licenseContractIdArray.split(",");
		  licenseContractIds = new Array();
		  for(var l=0;l<licenseContractIdList.length;l++){
			  licenseContractIds.push('"'+licenseContractIdList[l]+'"');
		    }	
		}
		if(removeBread!=null && removeBread.search("~")!=-1){
			removeBread = removeBread.replaceAll('"','\\"');
			removeBreadStr = removeBread.split("~");
		}			
		licenseCountflag = $('#licenseCountflag').val();
		if(fromLicense == 'true')
			fromLicense = true;
		else
			fromLicense = false;
	    if(fromLicense){
			  var lContractIds = $('#programIds').val();			  
			  if(lContractIds!=null && lContractIds.length>0){
				  var licContractIds = lContractIds.split(",");
				  programIds = new Array();
				  for(var l=0;l<licContractIds.length;l++){
				    	programIds.push('"'+licContractIds[l]+'"');
				  }	
		      }
		 }else if ( licenseContractIdArray!=null && licenseContractIdArray.length>0 
				  && (removeBread == null || (removeBreadStr != null && removeBreadStr[0]!="A-XPM-ProgramID")) ){
			  var lContractIds = $('#programIds').val();			  
			  if(lContractIds!=null && lContractIds.length>0){
				  var licContractIds = lContractIds.split(",");
				  programIds = new Array();
				  for(var l=0;l<licContractIds.length;l++){
				    	programIds.push('"'+licContractIds[l]+'"');
				  }	
		      }  
			  
		 }
		 if(removeBread!=null && removeBread.search("~")!=-1){
			  removeBread = removeBread.replaceAll('"','\\"');
			  var breadStr = removeBread.split("~");
			  if(breadStr[0]=="A-XPM-ListPrice"){
				  var price = breadStr[1].split("x");
				  var lowRange = price[0].substr(0,price[0].indexOf("."));
				  var highRange = price[1].substr(0,price[1].indexOf("."));
			  	  if(lowRange==priceRangeLow && highRange==priceRangeHigh){
					  priceRangeLow = null;
					  priceRangeHigh = null;
				  }
			  }
		  }
    
	    if(removeBread!=null && removeBread=='InStockOnly')
	    	removeBreadCrumb = removeBread;
	    else if(removeBread!=null)
	    	removeBreadCrumb = '"'+removeBread+'"';
	    if(removeTxt!='')
	    	removeText = removeTxt;
	    if(removeCat!='')
	    	removeCategory= removeCat;
      /* pagintion Flag*/
    paginationFlag = true;
    InsightSearch.postData();
}
/* ***end of bread crumbs helper function*** */

function inventaryBlowOut(){
	narrowData=null;
	advanceSearch= null;
	inventoryBlowOut = true;
	postSearchByCriteria(true,false,false,null);
}

function innventaryUpResults(narrrowField){
		if(narrrowField!=null)
            fieldBread  = '"'+narrrowField+'"';
		else
            firstBreadCrumb = "";
      narrowData = null;
      inventoryBlowOut = true;

        /* pagintion Flag*/
      paginationFlag = true;
      fromcs = $("#fromcs").val();
      if(fromcs == 'true')
      	fromcs = true;
      else
      	fromcs = false;
      if(fromcs){
      	searchTxt = $("#searchCSText").val();
      	groupId = $("#groupId").val();
      	setId = $("#setId").val();
      	categoryId = $("#categoryId").val();
      	controller = $("#controller").val();
      	groupName = $("#groupName").val();
      	shared = $("#shared").val();
      	InsightSearch.cmtStandards=$("#cmtStandards").val();
      	InsightSearch.cmtCustomerNumber=$("#cmtCustomerNumber").val();
      	approvedItemsOnly = false;
      }
      InsightSearch.postData();
}

function relatedURLString(discontinuedMode,category,inStockMode,addSearchText,fieldId,fieldValue){
      relatedCategory = category;
      additionalSearchText=addSearchText;
      fieldId = fieldId;
      fieldValue =fieldValue;
      advanceSearch = null;
      fromcs = $("#fromcs").val();
      if(fromcs == 'true')
      	fromcs = true;
      else
      	fromcs = false;
      if(fromcs){
      	searchTxt = $("#searchCSText").val();
      	groupId = $("#groupId").val();
      	setId = $("#setId").val();
      	categoryId = $("#categoryId").val();
      	controller = $("#controller").val();
      	groupName = $("#groupName").val();
      	shared = $("#shared").val();
      	InsightSearch.cmtStandards=$("#cmtStandards").val();
      	InsightSearch.cmtCustomerNumber=$("#cmtCustomerNumber").val();
      	approvedItemsOnly = false;
      }
      InsightSearch.postData();
}
/* ***End of postData()*** */

function removeObjectSession(){
      fieldId = null;
      fieldValue = null;
      removeBreadCrumb = null;
      fieldBread = null;
      narrowCategory = null;
      inventoryBlowOut = null;
      removeText= null;
      firstBreadCrumb = null;
      searchTxt = "";
      fromcs = false;
      groupId = null;
      setId = null;
      categoryId = null;
      controller = null;
      groupName = null;
      shared = null;
      paginationFlag = false;
      similarMaterialId=null;
      additionalSearchText = null;
      productSetMenu =null;
      relatedCategory=null;
      rebateSearchFlag = false;
	  removeRebateFlag = false;
	  removeApprovedItemsFlag=false;
	  removeAgreementProductsFlag=false;
	  approvedItemsOnly=false;
	  removeCategory= null;
	  approvedItemsFromLeftNavFlag = false;
	  agreementProductsFromLeftNavFlag = false;
	  priceRangeHigh = null;
	  priceRangeLow = null;
	  fromLicense = null;
	  licenseContractIds = null;
	  licenseCountflag = null;
	  programIds = null;
}

/* ***Executed upon ajax error*** */
function searchFailed(data){
      $("#shown").show();
}


function toggleSection(toggleThis, changeThis){
      var expandImageURL = "https://iq01.insight.com/ccms_img/ico_expandCollapse_expand.png";
      var collapseImageURL = "https://i04.insight.com/ccms_img/ico_expandCollapse_collapse.png";
      $(toggleThis).toggle('fast');
      var imageURL = $(changeThis).attr('src');
      if(imageURL == expandImageURL)
            $(changeThis).attr('src', collapseImageURL);
      else
            $(changeThis).attr('src', expandImageURL);
}

function companyStandardsToggle(toggleThis, changeThis){
    var expandImageURL = "ui-icon ui-icon-triangle-1-e left";
    var collapseImageURL="ui-icon ui-icon-triangle-1-s left";
    $(toggleThis).toggle();
    var imageURL = $(changeThis).attr('class');
    if(imageURL == expandImageURL){
  	  $(changeThis).attr('class','ui-icon ui-icon-triangle-1-s left');
  	}else{
  	  $(changeThis).attr('class','ui-icon ui-icon-triangle-1-e left');
  	}
}

function toggleMoreLessChoices(clickedLink, toggleThis){
      if( $(clickedLink).text() == "More Choices..." )
            $(clickedLink).text("Fewer Choices");
      else
            $(clickedLink).text("More Choices...");
      $(toggleThis).toggle('fast');
}

function onSortBy(sel){
	  var sortSelected = $(sel).find("option:selected");
      fromcs = $("#fromcs").val();
      if(fromcs == 'true')
    	  fromcs = true;
      else
    	  fromcs = false;
      softwareLicense();
      if(fromcs){
	       	searchTxt = $("#searchCSText").val();
	  	  	groupId = $("#groupId").val();
	  	  	setId = $("#setId").val();
	  	  	categoryId = $("#categoryId").val();
	  	  	controller = $("#controller").val();
	  	  	groupName = $("#groupName").val();
	  	  	shared = $("#shared").val();
	  	    InsightSearch.cmtStandards=$("#cmtStandards").val();
	  	    InsightSearch.cmtCustomerNumber=$("#cmtCustomerNumber").val();
	  	  	approvedItemsOnly = false;
   	  }  
      priceRangeLow = $('#priceRangeLow').val();	            		   
	  priceRangeHigh = $('#priceRangeHigh').val();
	  if(sortSelected.val() != null){
            nugsSortBy = sortSelected.val();
            sortByForBreadCrumb = true;
      }
      getPage=1;
      InsightSearch.postData();
}

function getTemplate(tmplStr){
      if(tmplStr == "searchLeftNavTemplate")
            return InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/searchLeftNavTemplate.html");

      else if(tmplStr == "searchResultsTemplate")
            return InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/searchResultsTemplate.html");

      else if(tmplStr == "breadCrumbTemplate")
            return InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/breadCrumbTemplate.html");

      else if(tmplStr == "pagingTemplate")
            return InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/pagingTemplate.html");

      else if(tmplStr == "productRecordTemplate")
            return InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/productRecordTemplate.html");

      else if(tmplStr == "savedProductCompareBoxTemplate")
            return InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/savedProductCompareBoxTemplate.html");

      else if(tmplStr == "softwareProgramResultsTempl"){
    	  return InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/softwareProgramResultsTempl.html");
    	  
      }else if(tmplStr == "loadTopmostRecommendedProducts"){
        	  return InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/loadTopmostRecommendedProducts.html");
      }else
            alert("Invalid Template:: "+tmplStr);
}

function compareSimilar(materialId,reloadFields){
      var url="/insightweb/compareSimilar";
      var shown = $('#shownRecords').val();
      var jsonObj = '{"showSuggestedSearch": false,"locale": "en_US","searchOpenMarket": false,"showApprovedItemOnlyLink": false,"showProductCompare": false,"showProductCenterLink": false,';
      if(approvedItemsOnly)
                  jsonObj = jsonObj+'"onlyApprovedItems" : "Y",';
      if(reloadFields!=null && reloadFields!='')
               jsonObj = jsonObj+'"field":['+reloadFields+'],';
      jsonObj=jsonObj+'"fromClp": false,"salesOffice": "2001","salesOrg": "2400","defaultPlant": "10","onlyOpenMarket": false,';
      jsonObj=jsonObj+'"pageSize": 0,"pageNumber": 0,"fieldList": [],"sort": "BestSellers","rebateSearch": false,"defaultSort": true,"useBreadcrumb": false,"similarMaterialId": "'+materialId+'","secure": false,"displayOpenMarket": false}';
      if (url != "" && jsonObj != "" ){
    	  var data = InsightCommon.getServiceResponse(url,jsonObj,"POST");
          displayCompareSimilarResults(data);
      }else
            alert(searchErrorMessages.labels.PleaseenterURLANDJSON);
}

var sortByCP = "";
var sortByCPFieid= "";
var checkedProdStringCP ="";
function sortByCompareProducts (a,b){
	sortByCP=a;
	sortByCPFieid =b;
	fromAddMfrPartNumFlag = true;
	compareProducts();
}

function displayCompareSimilarResults(data){
      $( "#bodyContent" ).empty();
      $("#bodyContent").css('background-color', '#ffffff');
      var newData=null;
      var newExtSummary=null;
      var totalExtSpecs=0;
      var newFieldSummary=null;
      var styleWidth=915;
      var productLength=0;
      if(data.products.length>0){    	  
    	  productLength =data.products.length;
      }
      if(data.extendedSpecSummary){
            newExtSummary=new Array();
            $.each(data.extendedSpecSummary, function(index, value) {
                  newExtSummary.push({Type: "Label", Value: value.label});
                  $.each(value.detailLabels, function(detIndex, detValue) {
                        var tempDetArray=new Array();
                        var oddrw=false;
                        if(detIndex%2!=0)
                              oddrw=true;
                        tempDetArray.push(detValue);
                        $.each(data.products, function(prodIndex,prodValue){
                              tempDetArray.push(prodValue.extendedSpecValues[totalExtSpecs]);
                        });
                        newExtSummary.push({Type:"Data", Value: tempDetArray,OddRow:oddrw});
                        totalExtSpecs++;
                  });

            });
      }
      // field summary : construct data
      var newFieldSummary=null;
      if(data.fieldSummary){
            newFieldSummary=new Array();
            $.each(data.fieldSummary, function(index, value){
                  var tempFieldSummary=new Array();
                  var prodSumm=new Array();
                  $.each(data.products, function(prodInd,prodVal){
                        if(prodInd==0)    data.similarMaterialId = prodVal.materialId;
                        prodSumm.push(value.prodFieldValueMap[prodVal.manufacturerPartNumber]);
                  });
                  var reloadSearchfieldValue=value.fieldId+'~'+value.fieldInternalValue;
                  if(value.usedForCompare)
                        newFieldSummary.push({showCheckBox:true, fieldIdValue:reloadSearchfieldValue,label:value.label, values: prodSumm});
                  else
                        newFieldSummary.push({label:value.label, fieldIdValue:reloadSearchfieldValue, values: prodSumm});
            });
      }
      newData='{"fieldSummary":'+data.fieldSummary+'"products":'+data.products+'"extendedSpecSummary":'+data.extendedSpecSummary+'"newExtSpecSummary":'+newExtSummary+'"newFieldSummary":'+newFieldSummary+'}';
      data.newExtSpecSummary=newExtSummary;
      data.newFieldSummary=newFieldSummary;
      data.hitCount = productLength;
      data.colcount = productLength+2;
      data.tableWidth=100/(productLength+1);
      var comptemplate = InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/compareSimilarTemplate.html");
      $.template( "compSimTemplate", comptemplate);
      data = $.extend(data, {'labels':searchLabels.labels});
      InsightCommon.renderTemplate( "compSimTemplate", data, "#bodyContent" );
}

function compareProducts(){
      var checkedProducts = [];
      var checkedProdString="[";
      var savedProductsLength = savedProducts.length;
      var checkedProductsLength = 0;
      if(!fromAddMfrPartNumFlag){
    	  $('#results :checkbox:checked').each(function(i){
            checkedProductsLength = checkedProductsLength +1;
    	  });
      }
      var finalProductLength = savedProductsLength + checkedProductsLength;
      if(fromAddMfrPartNumFlag==false && (finalProductLength < 2 || finalProductLength >10)){
          alert(searchLabels.labels.errorMinMax);
          return;
      }
      if(!fromAddMfrPartNumFlag){
    	  $('#results :checkbox:checked').each(function(i){
            checkedProducts[i] = $(this).val();
            saveOneToCompareList(checkedProducts[i]);
            checkedProductsLength = checkedProductsLength +1;
    	  });
      }
      fromAddMfrPartNumFlag = false;
      $.each(checkedProducts,function(index,value){
            checkedProdString=checkedProdString+'"'+value+'"';
            if(index+1==checkedProducts.length)
                  checkedProdString=checkedProdString+']';
            else
                  checkedProdString=checkedProdString+',';
      });
      /* *** appending the saved products to compare manufacturer ids to the checkedProdString *** */
      var savedProdString = '';
      if(savedProducts.length != 0){
            $.each(savedProducts, function(index, value){
                  savedProdString = savedProdString +'"'+ value+'"';
                  if(index < savedProducts.length-1)
                        savedProdString = savedProdString+',';
            });
            savedProdString = ','+savedProdString+']';
            checkedProdString = checkedProdString.replace("]", " ");
            checkedProdString = checkedProdString.concat(savedProdString);
            if(checkedProdString.indexOf("[,")>=0){
                  checkedProdString = checkedProdString.replace("[,", "[");
            }
      }else if(checkedProdStringCP!="")
    	  checkedProdString = checkedProdStringCP;
      checkedProdStringCP = checkedProdString;
      /* ***** end of appending saved products to checkedproducts ********* */
      var url="/insightweb/compareProducts";
      var jsonObj='{"showSuggestedSearch":false,"locale":"en_US","searchOpenMarket":false,"showApprovedItemOnlyLink":false,"showProductCompare":false,"showProductCenterLink":false,"catalogData":{"customCatalogId":null,"overrideCatalogId":null,"approvedItemsCatalogId":null,"purchasableCatalogSet":null,"filterManufacturerCatalogId":null},"fromClp":false,"salesOffice":"2001","salesOrg":"2400","defaultPlant":"10","onlyOpenMarket":false,"pageSize":0,"pageNumber":0,"fieldList":[],"sort":"BestSellers","rebateSearch":false,"defaultSort":true,"useBreadcrumb":false,"materialIds":'+checkedProdString+',"secure":false,"displayOpenMarket":false';
      if(sortByCP!="" && sortByCPFieid!=""){
          jsonObj = jsonObj+',"nugsSortBy":"'+sortByCPFieid+'","nugsSortBySelect":"'+sortByCP+'"';
      }
      jsonObj = jsonObj+'}';
      if (url != "" && jsonObj != "" ){
    	  var data = InsightCommon.getServiceResponse(url,jsonObj,"POST");
          if(data.savedProducts)
        	  	data.savedProducts = savedProducts;
            displayCompareProductResults(data,'');
            return;
      }else{
    	  alert(searchErrorMessages.labels.PleaseenterURLANDJSON);
    	  return;
      }
}
/* *** add Mfr Part Number in Compare Products page *** */
function addMfrPartNum(){
      var manPartNumber = $('#mfrPartNumText').val();
      manPartNumber = $.trim(manPartNumber);
      if(manPartNumber.length <= 0){
            alert(searchLabels.labels.errorNoPartNumber);
            return;
      }
      var presence = $.inArray(manPartNumber, savedProducts);
      if(presence != -1){
    	  $('#compareListErrorDiv').html(searchLabels.labels.errorItemAlreadyExists);
          $('#compareListErrorDivWrapper').show();
          $('#mfrPartNumText').val("");
          return;
      }

      /* call the service and get response */
  	  var	data = getCompareProductsResponse('["'+manPartNumber+'"]');

  	  /* check response for invalid material ids */
  	  var tempMaterialIdsArray = [];
  	  tempMaterialIdsArray[0] = manPartNumber;
  	  var okFlag = checkIfValidProducts(data, tempMaterialIdsArray);

  	  /* display error messages incase of invalid material ids */
  	  if(!okFlag && invalidMaterialIds && invalidMaterialIds.length > 0){
	  		var errorStr = searchLabels.labels.errorInvalidProduct;
	  		errorStr = errorStr + makeMaterialIdString(invalidMaterialIds);
	  		$('#compareListErrorDiv').html(errorStr);
	  		$('#compareListErrorDivWrapper').show();
	  		$('#mfrPartNumText').val("");
	  		invalidMaterialIds = [];
	  		return;
  	  }
      saveOneToCompareList(manPartNumber);
      $('#mfrPartNumText').val("");
      fromAddMfrPartNumFlag = true;
      compareProducts();
}

function showCLP(data, landingData){
      $("#bodyContent").html("");
      narrowData = data;
      var markup = data;
      markup = $.extend(markup,{'IBdata':false});
      markup = $.extend(markup,{'showApproveditem':Insight.showApproveditem});
      var twoColTemplate= InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/searchTwoColumnTemplate.html");
      $.template( "twoColTemplate", twoColTemplate);
      InsightCommon.renderTemplate( "twoColTemplate", "", "#bodyContent" );
      var leftTemplate1= InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/searchLeftNavTemplate.html");
      $.template( "searchNarrowTemplate", leftTemplate1);
      InsightCommon.renderTemplate("searchNarrowTemplate", markup, "#leftCol");
      $("#contentCol").html(landingData);   
      InsightCommon.hideLoading();
}

function renderShopByBrand(data){
      var bsDisplay = false;
      var apDisplay = false;
      if(data.brandData.displayBestSellers == true){
            $.each(data.brandData.lineListingData, function(i, item){
                  $.each(data.nugsProducts, function(j, prod){
                        if(item.itemNumber == prod.materialId){
                              bsDisplay = true;
                              return false;
                        }
                  });
            });
      }
      if(data.brandData.displayAdditionalProducts == true){
            $.each(data.brandData.bestSellersData, function(i, item){
                  $.each(data.nugsProducts, function(j, prod){
                        if(item.itemNumber == prod.materialId){
                              apDisplay = true;
                              return false;
                        }
                  });
            });
      }

      var side = false;
      if(data.nugsProducts){
            $.each(data.brandData.featureTwoData, function(i, item){
                  $.each(data.nugsProducts, function(j, prod){
                        if(item.itemNumber==prod.materialId){
                              item.itemCount = side;
                              if(side)
                                    side = false;
                              else
                                    side = true;
                        }
                  });
            });
      }
      data.brandData.displayBestSellers = bsDisplay;
      data.brandData.displayAdditionalProducts = apDisplay;
      data = $.extend(data, {'labels':searchLabels.labels});
      $("#bodyContent").html("");
      var shopByBrandTemplate= InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/shopByBrandTemplate.html");
      $.template("shopByBrandTemplate", shopByBrandTemplate);
      InsightCommon.renderTemplate("shopByBrandTemplate", data, "#bodyContent");
}

function getAdvanceSearchTemplateContent(data){
	InsightNavigation.getProductsData(data,true);
}

function getAdvanceSearchMenuData(searchData,advanceMenuData){
	var advancetemplate = InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/advanceSearchTemplate.html");
    $.template( "advanceSearchTempl", advancetemplate);
    mainCategory = "";
    if (searchData.mainCategoryList != undefined && searchData.mainCategoryList != null && searchData.mainCategoryList.length > 0) {
        $.each(searchData.mainCategoryList, function(index, value){
          mainCat = '"'+value.categoryId+'~'+value.categoryItemLabel+'"';
          mainCategory =mainCategory+mainCat+',';
        });
    }
    if(mainCategory !=null && mainCategory !="")
          mainCategory = mainCategory.substring(0,mainCategory.length-1);
    var advmarkup = searchData;   
    var noResults = false;
    var nugsParameters = null;
    if(advanceSearchSubmitData!=null && advanceSearchSubmitData.nugsHitCount==0 )
    noResults = true;
    if(advanceSearchSubmitData!=null && advanceSearchSubmitData.nugsSearchParameters!=null)
    nugsParameters = advanceSearchSubmitData.nugsSearchParameters;
    $("#advanceSearch").html("");
    $("#bodyContent").html("");    
    var menuData = advanceMenuData;
    advmarkup.menuData =menuData ;
    if(advmarkup.categorySelectedItems!=undefined && advmarkup.categorySelectedItems!=null && advmarkup.categorySelectedItems.parentSelectedCategory=='C-G'){
    	userSoftSearchData = userSoftwarePreferences();  	  
    }
    advmarkup = $.extend(advmarkup,{'noAdvResults':advanceSearchSubmitData},{'userSoftSearchData':userSoftSearchData});
    advmarkup = $.extend(advmarkup, {'labels':searchLabels.labels});
    InsightCommon.renderTemplate("advanceSearchTempl", advmarkup, "#bodyContent");
    if(menuData==null){
    	 $("#advMenuHeader").hide();
    }
    $("#bodyContent").show();
    if(noKeywordFlag){
          $("#errorMessageDiv").show();
          noKeywordFlag = false;
    }
    if(advmarkup!=null && advmarkup.selectedManufacturerIdsFromAdvanceSearch!=null){
    	$.each(advmarkup.selectedManufacturerIdsFromAdvanceSearch,function(index,value){
    		$("#manufacturerId option[id="+value+"]").attr('selected', 'selected');    
    		$("#manufacturer option[id="+value+"]").attr('selected', 'selected');  
    		
      });
    }
    advanceSearchSubmitData = null;
}

/*json request for product center*/
var jsonfirst='{"locale": "en_US",'+
            '"fromClp": false,'+
            '"salesOffice": "2001",'+
            '"salesOrg": "2400",'+
            '"defaultPlant": "10",'+
            '"onlyOpenMarket": false,'+
            '"pageSize": 0,'+
            '"pageNumber": 0,'+
            '"fieldList": [],'+
            '"sort": "BestSellers",'+
            '"rebateSearch": false,'+
            '"defaultSort": true,'+
            '"useBreadcrumb": false,'+
            '"materialIds": [';

var jsonlast='],'+
            '"secure": false,'+
            '"displayOpenMarket": false}';


function callmaterialid(){
      var checkedProducts = [];
      var checkedProdString="";
      $(':checkbox:checked').each(function(i){
            checkedProducts[i] = $(this).val();

      });
      $.each(checkedProducts,function(index,value){
            checkedProdString=checkedProdString+'"'+value+'"';
            if(index+1==checkedProducts.length)
                  checkedProdString=checkedProdString+'';
            else
                  checkedProdString=checkedProdString+',';
      });
   return checkedProdString;
}

/* a method to get all values of the checkboxes in the current page */
function getCheckedItems(){
      var checkedProducts = [];
      $(':checkbox:checked').each(function(i){
            var item = $(this).val();
            checkedProducts.push(item);
      });
      return checkedProducts;
}

function productCenterLogin(){
      var prodString= $('#loggedInFlag').val();
      if(prodString=="true"){
            return true;
      }else{
            return false;
      }
}

function productCenter(){
      var checkedProducts = [];
      var checkedProdString="";
      $(':checkbox:checked').each(function(i){
            checkedProducts[i] = $(this).val();

      });
      $.each(checkedProducts,function(index,value){
            checkedProdString=checkedProdString+'"'+value+'"';
            if(index+1==checkedProducts.length)
                  checkedProdString=checkedProdString+'';
            else
                  checkedProdString=checkedProdString+',';
      });
      if(checkedProducts.length > 0){
            var url="/insightweb/productCenter/insertProducts";
            var jsonObj=jsonfirst+checkedProdString+jsonlast;
            var data = InsightCommon.getServiceResponse(url,jsonObj,"POST");
            displayProductCenter();
      }else{
    	  displayProductCenter();
    	  $('#outerErrorMessageDiv').css('display','block');
 	  }
}

function productCenterTutorial(){
	 $("#bodyContent").html("");
     var prodcenterTutorial = InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/productCenterTutorial.html");
     $.template("prodcenterTutorial",prodcenterTutorial);
     markup = {'labels':searchLabels.labels};
     InsightCommon.renderTemplate("prodcenterTutorial",markup, "#bodyContent");
}

function redirectToProductCenter(){
		if((location.pathname + location.hash) == '/insightweb/search#addProductCenter'){
			window.location.reload();
		} else {
			window.location = 'http://www.insight.com/insightweb/search#addProductCenter'; // redirect current window(main)
		}
}

function displayProductCenter(){
      var url="/insightweb/productCenter/"+new Date().getTime();
      var jsonObj = '{'+
     '"locale": "en_US",'+
    '"fromClp": false,'+
    '"salesOffice": "2001",'+
    '"salesOrg": "2400",'+
    '"defaultPlant": "10",'+
    '"onlyOpenMarket": false,'+
    '"pageSize": 0,'+
    '"pageNumber": 0,'+
    '"fieldList": [],'+
    '"sort": "BestSellers",'+
    '"rebateSearch": false,'+
    '"defaultSort": true,'+
    '"useBreadcrumb": false,'+
    '"secure": false,'+
    '"displayOpenMarket": false'+
'}';
    var data = InsightCommon.getServiceResponse(url,jsonObj,"GET");
    if(data==undefined || data.products == null || data.products.length == 0){	
    	window.location.href = Insight.envConfigs.cmsServer + "/shopping-tools/product-center.html";
    }else{
    	renderProdCenter(data);
    }
}

function renderProdCenter(data){
      $("#bodyContent").html("");
      var prodcenter1 = InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/productCenterTemp.html");
      $.template("prodcenter1",prodcenter1);
      var data = $.extend(data, {savedProductsList:savedProducts});
      data = $.extend(data, {labels:searchLabels.labels}, {imgServer:imageServerURL});
      InsightCommon.renderTemplate("prodcenter1",data, "#bodyContent");
      reconstructSPCBox();
}

var pdoption = '';
function getCombo1(sel){
	pdoption = sel.options[sel.selectedIndex].value;
}

function GOProductCenter(){
    if(pdoption=="selectoptions" || pdoption=="checkitems" || pdoption=="add" || pdoption=="remove" || pdoption==''){
        var fields = $("input[name='product_chk']").serializeArray();
        if ((pdoption=="selectoptions" || pdoption=='') && fields.length == 0){
        	alert(searchErrorMessages.labels.PleaseReviewthefollowing+'  \n'+  searchErrorMessages.labels.Pleasecheckatleastoneproduct  +'\n'+  searchErrorMessages.labels.Pleaseselectanoption);
        }else if(fields.length == 0){
            alert(searchErrorMessages.labels.PleaseReviewthefollowing+'  \n'+  searchErrorMessages.labels.Pleasecheckatleastoneproduct);
        }else if(pdoption=="selectoptions"|| pdoption==''){
            alert(searchErrorMessages.labels.PleaseReviewthefollowing+'  \n'+  searchErrorMessages.labels.Pleaseselectanoption);
        }else if(pdoption=="add"){
            saveToCompareList('#cp-response');
        }else if(pdoption=="checkitems"){
        	var checkedProdString = callmaterialid();
        	productCenterAddtoCart(checkedProdString,null,'PPC',null,null,null,null,null,null);
        }else if(pdoption=="remove"){
            var checkedProdString = callmaterialid();
            var url="/insightweb/productCenter/removeProducts";
            var jsonObj=jsonfirst+checkedProdString+jsonlast;
            var data = InsightCommon.getServiceResponse(url,jsonObj,"POST");
            displayProductCenter();
        }else{
           alert("Invalid Option in GoProductCenter()");
        }
    }else if(pdoption == "checkToCompStd"){
        var checkedItems = getCheckedItems();
        addProdCenterToCompanyStandards(checkedItems);
    }else if(pdoption=="excel"){
          var url="/insightweb/productCenter/export";
          clearAllCheckBoxes();
          window.open(url);
    }
}

function gotoMultipleSkus(){
      $("#bodyContent").html("");
      var list = InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/MultipleAddProductListTemplate.html");
      $.template("list",list);
      InsightCommon.renderTemplate("list", {'labels':searchLabels.labels}, "#bodyContent");
}

function addbutton(){
      var productIds = $("#text1").val();
      productIds = jQuery.trim(productIds);
      var url="/insightweb/productCenter/insertProducts";
      var jsonObj = jsonfirst+'"'+productIds+'"'+jsonlast;
      var data = InsightCommon.getServiceResponse(url,jsonObj,"POST");
      if(data.length <1){
          displayProductCenter();
      }else{
       	  $('#invalidProductErrorMessageDiv').show();
       	  $('#text1').val('');
      }
}

function productCenterList(){
      var text1 = $("#tx1");
      var text2 = $("#tx2");
      var text3 = $("#tx3");
      var text4 = $("#tx4");
      var text5 = $("#tx5");
      var text6 = $("#tx6");
      var text7 = $("#tx7");
      var text8 = $("#tx8");
      var text9 = $("#tx9");
      var text10 = $("#tx10");
      if((text1.value=="") && (text2.value=="") && (text3.value=="") && (text4.value=="")&&(text5.value=="") && (text6.value=="") && (text7.value=="") && (text8.value=="")&& (text9.value=="") && (text10.value=="")){
            alert(searchErrorMessages.labels.PleaseReviewthefollowing+' \n'+ searchErrorMessages.labels.PleaseenteMfrpartnumber);
            valid = false;
      }else{
            var prodString='';
            for(var i=1; i<11; i++){
	             var textboxVal = $("#tx"+i+"").val();
	             textboxVal = jQuery.trim(textboxVal);
                 if(textboxVal || 0 !== textboxVal.length){
                       if(i>1){
                            prodString=prodString+',';
                       }
                       prodString = prodString+'"'+textboxVal+'"';
                 }
            }
            var url="/insightweb/productCenter/insertProducts";
            var jsonObj = jsonfirst+prodString+jsonlast;
            var data = InsightCommon.getServiceResponse(url,jsonObj,"POST");
            if(data.length <1){
                displayProductCenter();
            }else{
            	displayProductCenter();
             	$('#invalidProductErrorMessageDiv').show();
            }
      }
}

function didYouMeanSearch(dymSearchText){
      narrowData = null;
      productSet = null;
      nugsSortBy = null;
      searchTxt='"'+dymSearchText+'"';
      fromcs = $("#fromcs").val();
      if(fromcs == 'true')
      	fromcs = true;
      else
      	fromcs = false;
      if(fromcs){
	      	searchTxt = $("#searchCSText").val();
	      	groupId = $("#groupId").val();
	      	setId = $("#setId").val();
	      	categoryId = $("#categoryId").val();
	      	controller = $("#controller").val();
	      	groupName = $("#groupName").val();
	      	shared = $("#shared").val();
	      	InsightSearch.cmtStandards=$("#cmtStandards").val();
	      	InsightSearch.cmtCustomerNumber=$("#cmtCustomerNumber").val();
	      	approvedItemsOnly = false;
      }
      InsightSearch.postData();
      /*Coremetrics Tag*/
      cmCreatePageviewTag("Search Did You Mean","SEARCH_DID_YOU_MEAN",null,"161465");
}

/* To save products to product compare box */
function saveToCompareList(checkboxDiv){
      /* if saved list is already filled up and we are trying to add more items */
      if(savedProducts.length > 0 && savedProducts.length+1 > 10){
            $(checkboxDiv+' :checkbox:checked').each(function() {
                  this.checked = false;
            });
            alert(searchLabels.labels.errorMinMax);
            return;
      }

      /* getting all the checked products */
      var checkedProducts = [];
      $(checkboxDiv+' :checkbox:checked').each(function() {
            checkedProducts.push($(this).val());
            this.checked = false;
      });

      /* if no products are selected */
      if(checkedProducts.length == 0){
            alert(searchLabels.labels.errorAtleastOne);
            return;
      }

      /* if more than maximum number of products are being added */
      if(checkedProducts.length+savedProducts.length > 10){
            alert(searchLabels.labels.errorMinMax);
            return;
      }

      /* see if the product already exists, if not save to list, else do nothing */
      var alreadyExists = false;
      $.each(checkedProducts, function(i, check){
            alreadyExists = false;
            $.each(savedProducts, function(j, sel){
                  if(check==sel){
                        alreadyExists = true;
                        return;
                  }
            });
            if(!alreadyExists){
                  $('#spcBoxSelect').append('<option value="'+check+'">'+check+'</option>');
                  savedProducts.push(check);
                  $('#spcBoxNumberSaved').html(savedProducts.length);
            }
      });
      updateSavedProductsInSession();
      if(savedProducts.length > 0)
            $('#savedProductCompareBox').show();
}

/* save one item to compare list */
function saveOneToCompareList(manPartNumber){
      /* if more than maximum number of products are being added */
      if(savedProducts.length+1 > 10)
            return;

      var alreadyExists = false;
      $.each(savedProducts, function(i, sel) {
            if(manPartNumber==sel){
                  alreadyExists = true;
                  return;
            }
      });
      if(!alreadyExists){
            savedProducts.push(manPartNumber);
            updateSavedProductsInSession();
      }
}

function removeCP(toDeleteMeterialId){
		savedProducts = $.grep(savedProducts, function(value) {
			return value != toDeleteMeterialId;
		});
		fromAddMfrPartNumFlag = true;
		updateSavedProductsInSession();
		compareProducts();
}
/* to delete one from the saved list */
function deleteOneFromCompareList(){
      var toDelete = "";
      if($('#spcBoxSelect').val())
            toDelete = $('#spcBoxSelect').val();
      if(toDelete != "" || toDelete != null){
            /* remove from array */
            savedProducts = $.grep(savedProducts, function(value) {
                  return value != toDelete;
            });
            updateSavedProductsInSession();
            /* remove from drop down box */
            $('#spcBoxSelect option[value="'+toDelete+'"]').remove();
      }
      /* rewrite the number of saved products */
      $('#spcBoxNumberSaved').html(savedProducts.length);
      if(savedProducts.length == 0)
            $('#savedProductCompareBox').hide();
}

/* to empty the saved list */
function deleteAllFromCompareList(){
      savedProducts = [];
      updateSavedProductsInSession();
      $('#spcBoxSelect').html("");
      $('#spcBoxNumberSaved').html(savedProducts.length);
      $('#savedProductCompareBox').hide();
}

/* to compare the products in the list */
function spcHashCompareProducts(){
	document.location.hash = '#cmpSavedProducts';
}

function spcCompareProducts(){	
      if(savedProducts.length < 2)
            alert(searchLabels.labels.errorMinMax);
      else{
            var savedProdString = '';
            if(savedProducts.length != 0){
                  $.each(savedProducts, function(index, value) {
                        savedProdString = savedProdString +'"'+ value +'"';
                        if(index < savedProducts.length-1)
                              savedProdString = savedProdString+',';
                  });
                  savedProdString = '['+savedProdString+']';
            }
            var url="/insightweb/compareProducts";
            var jsonObj='{"showSuggestedSearch":false,"filterMaterialId":true,"locale":"en_US","searchOpenMarket":false,"showApprovedItemOnlyLink":false,"showProductCompare":false,"showProductCenterLink":false,"catalogData":{"customCatalogId":null,"overrideCatalogId":null,"approvedItemsCatalogId":null,"purchasableCatalogSet":null,"filterManufacturerCatalogId":null},"fromClp":false,"salesOffice":"2001","salesOrg":"2400","defaultPlant":"10","onlyOpenMarket":false,"pageSize":0,"pageNumber":0,"fieldList":[],"sort":"BestSellers","rebateSearch":false,"defaultSort":true,"useBreadcrumb":false,"materialIds":'+savedProdString+',"secure":false,"displayOpenMarket":false';
            if(sortByCP!="" && sortByCPFieid!=""){
                  jsonObj = jsonObj+',"nugsSortBy":"'+sortByCPFieid+'","nugsSortBySelect":"'+sortByCP+'"';
            }
            jsonObj = jsonObj+'}';
            if (url != "" && jsonObj != "" ){
            	var data = InsightCommon.getServiceResponse(url,jsonObj,"POST");
                displayCompareProductResults(data,'');
            }else
                  alert(searchErrorMessages.labels.PleaseenterURLANDJSON);
      }
      return;
}

function spcComparePrintProducts(saveProductList,type,byodUser,emailInfo){
	  savedProducts = saveProductList;
      if(savedProducts.length < 2)
            alert(searchLabels.labels.errorMinMax);
      else{
            var savedProdString = '';
            if(byodUser==undefined || byodUser == ''){
              byodUser = false;
            } 
            if(savedProducts.length != 0){
                  $.each(savedProducts, function(index, value) {
                        savedProdString = savedProdString +'"'+ value +'"';
                        if(index < savedProducts.length-1)
                              savedProdString = savedProdString+',';
                  });
                  savedProdString = '['+savedProdString+']';
            }
            var url="/insightweb/compareProducts";
            var jsonObj='{"showSuggestedSearch":false,"filterMaterialId":true,"byodEndUser":'+byodUser+',"locale":"en_US","searchOpenMarket":false,"showApprovedItemOnlyLink":false,"showProductCompare":false,"showProductCenterLink":false,"catalogData":{"customCatalogId":null,"overrideCatalogId":null,"approvedItemsCatalogId":null,"purchasableCatalogSet":null,"filterManufacturerCatalogId":null},"fromClp":false,"salesOffice":"2001","salesOrg":"2400","defaultPlant":"10","onlyOpenMarket":false,"pageSize":0,"pageNumber":0,"fieldList":[],"sort":"BestSellers","rebateSearch":false,"defaultSort":true,"useBreadcrumb":false,"materialIds":'+savedProdString+',"secure":false,"displayOpenMarket":false';
            if(sortByCP!="" && sortByCPFieid!=""){
                  jsonObj = jsonObj+',"nugsSortBy":"'+sortByCPFieid+'","nugsSortBySelect":"'+sortByCP+'"';
            }
            jsonObj = jsonObj+'}';
            if (url != "" && jsonObj != "" ){     var data = InsightCommon.getServiceResponse(url,jsonObj,"POST");
            	displayPrintCompareProductResults(data,type,emailInfo);
            }else
                  alert(searchErrorMessages.labels.PleaseenterURLANDJSON);
      }
      return;
}

function reconstructSPCBox(){
      if(savedProducts.length > 0){
            $.each(savedProducts, function(index, value) {
                  $('#spcBoxSelect').append('<option value="'+value+'">'+value+'</option>');
            });
            $('#spcBoxNumberSaved').html(savedProducts.length);
            $('#savedProductCompareBox').show();
      }
}

function upChangeButtonColor(){
      $("#btnUpdate").attr("src","https://if01.insight.com/ccms_img/btn_update_orn_91x25.png");
}

function getMicrosite(usrSrch, manufacturerId, brandId, microsite){
	var r = checkforCMSPage(usrSrch);
	fromcs = $("#fromcs").val();
	if(fromcs == 'true')
		fromcs = true;
	else
		fromcs = false;
	if(fromcs){
		searchTxt = $("#searchCSText").val();
		groupId = $("#groupId").val();
		setId = $("#setId").val();
		categoryId = $("#categoryId").val();
		controller = $("#controller").val();
		groupName = $("#groupName").val();
		shared = $("#shared").val();
		InsightSearch.cmtStandards=$("#cmtStandards").val();
		InsightSearch.cmtCustomerNumber=$("#cmtCustomerNumber").val();
		approvedItemsOnly = false;
	}
    if (r!=null && r!="null") {
       window.location = Insight.envConfigs.cmsServer + r;
	}else{
		InsightSearch.microSiteMfrId = manufacturerId;
   		postSearchByCriteria(false,false,approvedItemsOnly,null);
	}
}

function getInternalMicrosite(manufacturerCode, manufacturerId, brandId, microsite){
	var url = "/insightweb/getBrand";
	var jsonObj = '{"manufactureCode":"'+manufacturerCode+'",';
	if(approvedItemsOnly)
	            jsonObj = jsonObj+'"onlyApprovedItems" : "Y",';
	if(brandId == "0" || brandId == 0 || brandId == null)
	      jsonObj = jsonObj+'"manufacturerId":'+manufacturerId+',';
	else
	      jsonObj = jsonObj+'"brandId":'+brandId+',';
	jsonObj = jsonObj+'"microsite":'+microsite+',';
	jsonObj = jsonObj+'"businessUnit": 2,"salesOrg": "2400","defaultPlant": "10"';
	jsonObj = jsonObj+'}';
	var data = InsightCommon.getServiceResponse(url,jsonObj,"POST");
	renderMicrosite(data);
}

function renderMicrosite(data){
      $("#bodyContent").html("");
      if(data.brandData != null){
            renderShopByBrand(data);
            return;
      }
      if(data.body != null && data.body != ""){
            var shopBrandsTemplate = InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/shopBrandsTemplate.html");
            $.template("shopBrandsTemplate", shopBrandsTemplate);
	        data =  {'labels':searchLabels.labels};

            InsightCommon.renderTemplate("shopBrandsTemplate", data, "#bodyContent");
            $("#shopByBrandPageHeading").hide();
            $("#alphabetsPagingDiv").hide();
            $("#brandsLogoDiv").hide();
            $("#brandLinksDiv").hide();
            $("#micrositeByLetterDiv").html(data.body);
            return;
      }
}

function getBrandByLetter(letter){
      letter = letter.toLowerCase();
      var bblURL = '/insightweb/getBrandByLetter/'+letter+'/en_US'; //<mark
      $.ajax({
            url: bblURL,
            type: "GET",
            global: false,
            async: false,
            crossDomain: true,
            success: function(data) {renderBrandByLetter(data);},
            error: function(data){alert("failed");}
      });
}

function renderBrandByLetter(data){
      /* if brandid==0 take manufacturerid */
      var tempData = $.parseJSON(data);
      var shopBrandsTemplate = InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/shopBrandsTemplate.html");
      $.template("shopBrandsTemplate", shopBrandsTemplate);
      var letters = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
      $.each(tempData, function(index, value){
            if( (index+1)%5 == 0 )
                  value=$.extend(value,{"newLineFlag":true});
            else
                  value=$.extend(value,{"newLineFlag":false});
            tempData[index]=value;
      });
      data = tempData;
      var linkColumnBrands = computeLinkColumnBrands(data);
      var allData = $.extend({'allBrands':data}, {'alphabet':letters}, {'linkColBrands':linkColumnBrands}, {'labels':searchLabels.labels});
      $("#bodyContent").html("");
      InsightCommon.renderTemplate("shopBrandsTemplate", allData, "#bodyContent");
}

function onErrorHideThis(element){
      $(element).hide();
}

function computeLinkColumnBrands(data){
      var listCount = data.length;
      var eachCol = Math.floor(listCount/3);
      var colOneElements = [];
      var colTwoElements = [];
      var colThreeElements = [];
      $.each(data, function(index, value){
            if(colOneElements.length < eachCol)
                  colOneElements.push(value);
            else if(colTwoElements.length < eachCol)
                  colTwoElements.push(value);
            else
                  colThreeElements.push(value);
      });
      var linkColumnBrands = {colOne:colOneElements, colTwo:colTwoElements, colThree:colThreeElements};
      return linkColumnBrands;
}

/* Shop by Category functions */
function getShopByCategoryPage(){
	 var url="/insightweb/getLPData";
     var jsonObj= '{"lpId":"'+10905+'"}';
     if (url != "" && jsonObj != "" ){
         var data = InsightCommon.getServiceResponse(url,jsonObj,"POST");
         renderShopByCategoryPage(data);
     }else
         alert(searchErrorMessages.labels.PleaseenterURLANDJSON);
}

function renderShopByCategoryPage(data){
      $("#bodyContent").html("");
      var shopBrandsTemplate = InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/shopBrandsTemplate.html");
      $.template("shopBrandsTemplate", shopBrandsTemplate);
      data =  {'labels':searchLabels.labels};
      InsightCommon.renderTemplate("shopBrandsTemplate", data, "#bodyContent");
      $("#shopByBrandPageHeading").hide();
      $("#alphabetsPagingDiv").hide();
      $("#brandsLogoDiv").hide();
      $("#allBrandsDiv").hide();
      $("#brandLinksDiv").hide();
      $("#micrositeByLetterDiv").html(data.body);
}

/* Product Compare List functions */
function viewProductCompareList(){
      $("#bodyContent").html("");
      var productCompareListTemplate = InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/productCompareListTemplate.html");
      $.template("productCompareListTemplate", productCompareListTemplate);
      searchLabels = InsightSearch.loadSearchLabels();
      var allData = $.extend({'labels':searchLabels.labels}, {'imgServer':imageServerURL});
      InsightCommon.renderTemplate("productCompareListTemplate", allData, "#bodyContent");
      if(invalidMaterialIds && invalidMaterialIds.length > 0){
            var errorStr = searchLabels.labels.errorInvalidProduct+'[';
            $.each(invalidMaterialIds, function(index, value){
                  if(index==invalidMaterialIds.length-1)
                        errorStr = errorStr+value+']';
                  else
                        errorStr = errorStr+value+', ';
                  savedProducts = $.grep(savedProducts, function(val) {
                        return val != value;
                  });
            });
            $('#compareListErrorDiv').html(errorStr);
            $('#compareListErrorDivWrapper').show();
            invalidMaterialIds = [];
      }
      if(savedProducts.length > 0){
            var len = savedProducts.length;
            var count = 0;
            $.each(savedProducts, function(index, value) {
                  var val = index+1;
                  $("#tx"+val).val(value);
            });
      }
}

function productCompareList(){
      $('#compareListErrorDivWrapper').hide();
      var serviceCallFlag = true;
      var temp1 = $("#tx1").val();
      var temp2 = $("#tx2").val();
      temp1 = $.trim(temp1);
      temp2 = $.trim(temp2);
      if(temp1 == "" || temp1 == null || temp2 == "" || temp2 == null){
            alert(searchLabels.labels.errorMinMax);
            return;
      }
      var newSavedProducts = [];
      $('input[id*="tx"]').each(function() {
            var temp = $(this).val();
            temp = $.trim(temp);
            if(temp.length > 0 && temp != null && temp != ""){
                  if($.inArray(temp, newSavedProducts) <= -1)
                        newSavedProducts.push(temp);
                  else{
                        serviceCallFlag = false;
                        viewProductCompareList();
                        $('#compareListErrorDiv').html(searchLabels.labels.errorItemAlreadyExists);
                        $('#compareListErrorDivWrapper').show();
                        return true;
                  }
            }
      });
      if(!serviceCallFlag)
            return;

      if(newSavedProducts.length < 2){
            alert(searchLabels.labels.errorMinMax);
            return;
      }
      invalidMaterialIds = [];
      var newSavedProductsString = makeMaterialIdString(newSavedProducts);
      var data = getCompareProductsResponse(newSavedProductsString);
      var okFlag = checkIfValidProducts(data, newSavedProducts);

      if(okFlag){
	      savedProducts = newSavedProducts;
	      updateSavedProductsInSession();
	      var savedProdString = '';
	      if(savedProducts.length != 0){
	            $.each(savedProducts, function(index, value) {
	                  savedProdString = savedProdString +'"'+ value +'"';
	                  if(index < savedProducts.length-1)
	                        savedProdString = savedProdString+',';
	            });
	            savedProdString = '['+savedProdString+']';
	      }
	      var url="/insightweb/compareProducts";
	      var jsonObj='{"showSuggestedSearch":false,"filterMaterialId":true,"locale":"en_US","searchOpenMarket":false,"showApprovedItemOnlyLink":false,"showProductCompare":false,"showProductCenterLink":false,"catalogData":{"customCatalogId":null,"overrideCatalogId":null,"approvedItemsCatalogId":null,"purchasableCatalogSet":null,"filterManufacturerCatalogId":null},"fromClp":false,"salesOffice":"2001","salesOrg":"2400","defaultPlant":"10","onlyOpenMarket":false,"pageSize":0,"pageNumber":0,"fieldList":[],"sort":"BestSellers","rebateSearch":false,"defaultSort":true,"useBreadcrumb":false,"materialIds":'+savedProdString+',"secure":false,"displayOpenMarket":false';
	      if(sortByCP!="" && sortByCPFieid!=""){
	          jsonObj = jsonObj+',"nugsSortBy":"'+sortByCPFieid+'","nugsSortBySelect":"'+sortByCP+'"';
	      }
	      jsonObj = jsonObj+'}';
	      if (url != "" && jsonObj != "" ){
	    	  var data = InsightCommon.getServiceResponse(url,jsonObj,"POST");
	          displayCompareProductResults(data,'');
	      }else
	            alert(searchErrorMessages.labels.PleaseenterURLANDJSON);
      }else{
    	  viewProductCompareList();
    	  invalidMaterialIds = [];
          return;
	  }
}

function shopByBrandSearch(){
      var sbbSearchTxt = $('#sb_quickSearch').val();
      sbbSearchTxt = $.trim(sbbSearchTxt);
      narrowData = null;
      productSet = null;
      nugsSortBy = null;
      searchTxt='"'+sbbSearchTxt+'"';
      fromcs = $("#fromcs").val();
      if(fromcs == 'true')
      	fromcs = true;
      else
      	fromcs = false;
      if(fromcs){
      	searchTxt = $("#searchCSText").val();
      	groupId = $("#groupId").val();
      	setId = $("#setId").val();
      	categoryId = $("#categoryId").val();
      	controller = $("#controller").val();
      	groupName = $("#groupName").val();
      	shared = $("#shared").val();
      	InsightSearch.cmtStandards=$("#cmtStandards").val();
      	InsightSearch.cmtCustomerNumber=$("#cmtCustomerNumber").val();
      	approvedItemsOnly = false;
      }
      InsightSearch.postData();
}

function addToCart(selectedMeterialId){
	 var shopCategoryURL = "/insightweb/transaction/addtocart";
	 var jsonObj = '{"clientBrowserDate":"'+sDate+'","materialID" : "'+selectedMeterialId+'","quantity" : 1}';
	 var data = InsightCommon.getServiceResponse(shopCategoryURL,jsonObj,"POST");
}

function similarProductSearch(mtrId){
	similarMaterialId = mtrId;
	fromcs = $("#fromcs").val();
	if(fromcs == 'true')
		fromcs = true;
	else
		fromcs = false;
	if(fromcs){
		searchTxt = $("#searchCSText").val();
		groupId = $("#groupId").val();
		setId = $("#setId").val();
		categoryId = $("#categoryId").val();
		controller = $("#controller").val();
		groupName = $("#groupName").val();
		shared = $("#shared").val();
		InsightSearch.cmtStandards=$("#cmtStandards").val();
		InsightSearch.cmtCustomerNumber=$("#cmtCustomerNumber").val();
		approvedItemsOnly = false;
	}
	InsightSearch.postData();
}


/* function to get company standards data */
function getCompanyStandards(){
      var url="/insightweb/account/companyStandard";
      var jsonObj = '{"userEdit":false}';
      var companyStandardsData = InsightCommon.getServiceResponse(url, jsonObj, "POST");
      return companyStandardsData;
}

function getManagedCompanyStandards(){
	  var jsonObj = "";
      var url="/insightweb/manage/companyStandard";
      if(InsightNavigation.isEadminUser){ jsonObj = '{"userEdit":false}';}
      else{
    	  jsonObj = '{"userEdit":true}';  
      }
      var companyStandardsData = InsightCommon.getServiceResponse(url, jsonObj, "POST");
      return companyStandardsData;
}

/* add checked items in product center to company standards */
function addProdCenterToCompanyStandards(checkedItems){
	if(checkedItems.length <= 0){
		alert(searchErrorMessages.labels.PleaseReviewthefollowing +'\n'+ searchErrorMessages.labels.Pleasecheckatleastoneproduct);
		return;
	}
	var addToCompanyStandardsPopUpTemplate = InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/addToCompanyStandardsPopUp.html");
	$.template("addToCompanyStandardsPopUpTemplate", addToCompanyStandardsPopUpTemplate);
	var jsonObj = '{"user":{}}';
	var setUrl = "/insightweb/account/companyStandardSets";
	var responseData = InsightCommon.getServiceResponse(setUrl, jsonObj, "POST");
	InsightSearch.csData = responseData;
	companyStandardsData = $.extend({'companyStandards':responseData}, {'labels':searchLabels.labels});
	$( "#popUpDiv" ).remove();
    $('body').append('<div id="popUpDiv"></div>'); 
    $("#popUpDiv").attr("title", searchLabels.labels.addToCompanyStandardsPopUpHeading);
	$("#popUpDiv").html("");
	InsightCommon.renderTemplate("addToCompanyStandardsPopUpTemplate", companyStandardsData, "#popUpDiv");
	var buttons = {};
	buttons[ companyStandardsData.labels.cancel ] = function() {$(this).dialog("close");};
	buttons[ companyStandardsData.labels.add ] = function() {csPopUpAddBtn();;};
	$("#popUpDiv").dialog({
							buttons: buttons,
							height: 500,
							width: 400,
							resizable: true,
							draggable: true
	});
}

function csPopUpAddBtn(){
	var materialIds = [];
	var divId = "";
	if($("#results").length != 0)
		divId = "#results";
	else if($("#cp-response").length != 0)
		divId = "#cp-response";
	else
		alert("Invalid Div Id in csPopUpAddBtn()");
	$(divId+" input:checked").each(function(index, item){
		materialIds[index] = $(this).val();
	});
	var materialIdStr = '[';
	$.each(materialIds, function(index, value){
		if(index == materialIds.length-1)
			materialIdStr = materialIdStr + '"'+value+'"]';
		else
			materialIdStr = materialIdStr + '"'+value+'",';
	});
	var productSets = [];
	var selectedSets = [];
	$("#popUpDialogBody input:checked").each(function (index, item){
		productSets[index] = $(this).val();
		selectedSets[index] = $(this).attr('id');		
	});
	if(productSets.length==0){
		alert(searchErrorMessages.labels.PleaseselectProductSet);
		return;
	}
	clearAllCheckBoxes();
	var requestObj = '{"materialSet":{';
	$.each(productSets, function(index, prodSet){
		requestObj = requestObj+'"'+prodSet+'":'+materialIdStr;
		if(index < productSets.length-1)
			requestObj = requestObj+',';
	});
	// get set types of selected sets
	var Collection = function() {
		this.catGroupMap = {};
		this.add = function(cat, group) {
			var addgroups = [];
			if(this.catGroupMap[cat]!=undefined){  
				  if($.inArray(group, this.group(cat) )== -1 ){
					   this.group(cat).push(group);   
					   this.catGroupMap[cat]=this.group(cat);
				  } 
				  return undefined;
			};
			addgroups.push(group);
			this.catGroupMap[cat] = addgroups;
			return;
		};
		this.group = function(cat) {
			return this.catGroupMap[cat];
		};

	};

    var controller = false; 
    var csCategorygroupUnsharedMap=new Collection(); 
	requestObj = requestObj+'},"setType":{';
	if(InsightSearch.csData != null && InsightSearch.csData.length > 0){		
		$.each(selectedSets, function(index0,selectedSet){
			$.each(InsightSearch.csData, function(index1,category){
				if(selectedSet.split('_')[0] == category.id){
					if(category.csgroups!=null && category.csgroups.length > 0){
						$.each(category.csgroups, function(index2,group){
							if(selectedSet.split('_')[1] == group.groupId){
								if(group.cssets !=null && group.cssets.length > 0){
									$.each(group.cssets, function(index3,set){
										if(selectedSet.split('_')[2] == set.id){
											requestObj = requestObj+'"'+set.id+'":'+set.setType; 
											if(index0 < selectedSets.length-1)
												requestObj = requestObj+',';
											if(selectedSet.split('_')[3] == "false"){
												controller = true;
												csCategorygroupUnsharedMap.add(category.id,group.groupId);
											}
										}
									});									
								}
							}
						});
					}
				}
			});
		});
	}
	var catGrpUnsharedMap = JSON.stringify(csCategorygroupUnsharedMap.catGroupMap);
	requestObj = requestObj +'},"catGroupUnsharedMap":'+catGrpUnsharedMap+'}';	
    var unlinkFlag = false;
	if(controller){		 
		unlinkFlag = isUnlinkSharedGroup()? true: false;
		 if(unlinkFlag == false){
			 $("#popUpDiv").dialog("close");
			 $( "#popUpDiv" ).remove();    
		 }		
	 }	
	if(unlinkFlag || controller == false  ){		
	      var responseObj = InsightCommon.getServiceResponse("/insightweb/account/companyStandard/insertProduct", requestObj, "POST");
	      var successNotificationTemplate = InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/companyStandardsSuccessPopUpTemplate.html");
	      $.template("successNotificationTemplate", successNotificationTemplate);
	      $("#notificationTimeOutDiv").html("");
	      var jsonObjComp = $.extend({'labels':searchLabels.labels}, {'materialIds':materialIds},{'responseObj':responseObj});
	      InsightCommon.renderTemplate("successNotificationTemplate", jsonObjComp, "#notificationTimeOutDiv");
	      if(responseObj && responseObj.status && responseObj.statusMessage==''){     
		         $("#popUpDiv").dialog("close");
		         $( "#popUpDiv" ).remove();      
		         $("#nonESD").css('display','block');
		         window.scrollTo(0, 0);
		         $("#notificationTimeOutDiv").css({'position':'absolute', 'top':'20%', 'left':'70%','width':'auto'});
		         $("#notificationTimeOutDiv").show();
		         setTimeout(function() {
		        	 $("#notificationTimeOutDiv").hide();
		         }, 2000); // <-- time in milliseconds
	      }else if(responseObj && responseObj.status && responseObj.statusMessage!=''){
		    	  $("#popUpDiv").dialog("close");
		    	  $( "#popUpDiv" ).remove();
		          $("#DuplicateMessages").css('display','block');
		          window.scrollTo(0, 0);
		          $("#notificationTimeOutDiv").css({'position':'absolute', 'top':'20%', 'left':'70%','width':'auto'});
		          $("#notificationTimeOutDiv").show();
		          setTimeout(function() {
		        	  $("#notificationTimeOutDiv").hide();
		          }, 2000); 
	      }else{
	         $("#popUpDiv").dialog("close");
	         $( "#popUpDiv" ).remove();
	      }
	}
}

function searchToCompanyStandards(){
      var checkedItems = getCheckedItems();
      addProdCenterToCompanyStandards(checkedItems);
}

function AddToCartDialog(image,selectedMeterialId,price,qty, vsppProduct){
	if (vsppProduct != null) {
			if (vsppProduct){
			    var rootURL = InsightCommon.getRootURL();
			    window.location = rootURL + "/insightweb/vsppcalculator";
			    return;
			}
	 }
	
	//check for "Enable Purchasing Popup" permission and show pop up if enabled
	if(Insight.userPermissions!=null){
			var testPermission = Insight.userPermissions;        	
			 	if($.inArray('enable_purchase_popup',testPermission)==-1){	
			 		 var contractId = null;			 		
			 		 if((Insight.selectedContractId!=null  && Insight.selectedContractId!='undefined' && Insight.selectedContractId.length>0)){
			 			contractId =Insight.selectedContractId;
			 		 }
			 		 var url = "/insightweb/transaction/addtocart";
			 		 if(!isNumeric(qty)){
			            	qty =1;
			         }
			 		 var loc = document.location.href;
					 var previousSearchURL = '';
					 loc = loc.substring(loc.indexOf('insightweb'),loc.length);
					 previousSearchURL =  '/'+loc; 
					 var parentMtrId = selectedMeterialId.replaceAll("#", "\\#").replaceAll("/","\\/").replaceAll(".","\\."); 
					 var contractCartId = $('#'+parentMtrId).val();	
					 if(contractCartId !=null && contractCartId!=''){
						 contractId = contractCartId;
					 }	
					 var softwareContractId = $('#softwareContractId_'+parentMtrId).val();
					 var programId = $('#programId_'+parentMtrId).val();
			         if(softwareContractId==null || softwareContractId==undefined || softwareContractId=='' || programId==null && programId==undefined || programId==''){
			        	 softwareContractId = '';
			        	 programId = '';
			         }
			         if(contractId!=null){
						 var jsonObj = '[{"clientBrowserDate":"'+sDate+'","selectedSoftwareContractId":"","programId":"'+programId+'","contractID":"'+contractId+'","previousSearchURL":"'+previousSearchURL+'","materialID" : "'+selectedMeterialId+'","quantity" :'+qty+'}]';
					 }else  var jsonObj = '[{"clientBrowserDate":"'+sDate+'","selectedSoftwareContractId":"'+softwareContractId+'","programId":"'+programId+'","previousSearchURL":"'+previousSearchURL+'","materialID" : "'+selectedMeterialId+'","quantity" :'+qty+'}]';
		             var data = InsightCommon.getServiceResponse(url,jsonObj,"POST");	 	 
			 		 window.location.href="http://www.insight.com/insightweb/viewCart";
			 		 return true;
				}
	}
    $("#addToCartDialog").dialog({ width: 600, resizable: false, modal:true,position:['top',150], overlay: { background: "#F2F2F2" }}).find("button").click(function () {
            $(this).closest(".ui-dialog-content").dialog("close");
    });
    insightLightBox.styleCustomDialog("#addToCartDialog");
    getAddToCartDialog(image,selectedMeterialId,price,qty);
}

function AddToCartDialogForCrossSell(image,selectedMeterialId,price,qty, vsppProduct,contractID){
	$("#addToCartDialog").dialog({ width: 600, resizable: false, modal:true,position:['top',150], overlay: { background: "#F2F2F2" }}).find("button").click(function () {
        $(this).closest(".ui-dialog-content").dialog("close");            
    });	
    insightLightBox.styleCustomDialog("#addToCartDialog");
    getAddToCartDialog(image,selectedMeterialId,price,qty,contractID);
}

var parentMaterialIdKey=null;
function getAddToCartDialog(image,selectedMeterialId,price,qty,contractIDFromCart){
	$("#addToCartDialog").html($('#loading').html());             
 	var url = '/insightweb/getProductInfo';
 	var softwareContractId = $('#softwareContractId_'+selectedMeterialId).val();
	var programId = $('#programId_'+selectedMeterialId).val();
    if(softwareContractId==undefined || !IsNumeric(softwareContractId) || programId==undefined ||  programId==null ){
    	 softwareContractId = '';
    	 programId = '';
    }
 	var contractId = null;     
 	var parentMtrId = selectedMeterialId.replaceAll("#", "\\#").replaceAll("/","\\/").replaceAll(".","\\."); 
	var contractCartId = $('#'+parentMtrId).val();	
	if(contractCartId !=null && contractCartId!=''){
		 contractId = contractCartId;
	} 
	var loc = document.location.href;
	if(loc.indexOf('viewCart')!=-1){
		 if(contractId==null && contractIDFromCart != null){
			 contractId = contractIDFromCart;  
		 }
	}
	var jsonRequestObj = populateProductInfoJSONObject(selectedMeterialId,contractId,false,softwareContractId);
	/*duplicationg product info call for loading accessories*/
	var productInfoReq = JSON.parse(jsonRequestObj);
	productInfoReq.loadAccessories = true;
	productInfoReq.cartFlag = false; 
	productInfoReq.contractId = contractId; 
	/*this line is added to allow more prices in ips case need to verify the condition
	 * it can always be null. will this handle open market case 
	 */
	productInfoReq = JSON.stringify(productInfoReq);
	var serviceResponseObj;
	var jsonObj ='';
	var jsonResponseObj = null;
	var data  = null;
	InsightCommon.getServiceResponseAsync(url,jsonRequestObj,"POST",function(ResponseObj){ 	
		jsonResponseObj = ResponseObj;
		/*function is modified to populate add to cart popup on call back of getProductInfo call
		 * so that no need to check for cart data and productInfoData two times which will also get used 
		 * */       		
		/*adding code to reuse the same add to cart from view cart line level cross sell and up sell
    	 * adding to condition to bypass adding line item again to cart when add to cart is opened from view cart page*/
    	 if(loc.indexOf('viewCart')!=-1){
    		/*this mean we are on view cart page */ 
    		 InsightCommon.getServiceResponseAsync("/insightweb/transaction/getcart","","GET",function(getCartResponse){        				
    				if(getCartResponse!=null && getCartResponse.contracts!=null){				
    					 $("#addToCartDialog").html("");
    		     		 getAddtoCartItemsSimpleDialogInfo(getCartResponse,jsonResponseObj,qty,productInfoReq);
    		            /* parentMaterialIdKey=data.materialIdKeyList[0]; */ 
    		     		 /*parentMaterialIdKey since this is global variable we will be populating this from view cart page from 
    		     		  * trans shopping cart .js from the function loadIppWarrenties
    		     		  * */
    				}
					/* get csid of all ipp warranty pdcts */
			    	InsightSearch.icsMap = null;
			    	var icsObj = new Array();
			    	if(jsonResponseObj!=null && jsonResponseObj.warrantyPdcts!=null){
			    		$.each(jsonResponseObj.warrantyPdcts,function(index,rowData){
			    			if(rowData.csid!=null && rowData.csid!=""){
			    				var matId = rowData.materialId;
			    				var csid = rowData.csid;
			    				icsObj[matId] = csid;
			    			}
			    		});
			    		InsightSearch.icsMap = icsObj;
			    	}
    		 });
    	 }else{
    		 var url = "/insightweb/transaction/addtocart";
	         if(!isNumeric(qty)){
	         	qty =1;
	         }			 
			var previousSearchURL = '';
			loc = loc.substring(loc.indexOf('insightweb'),loc.length);
			if(loc.indexOf('searchResults')!=-1){
			  previousSearchURL =  '/'+loc; 
			}
			if(contractId!=null){
				jsonObj = '[{"clientBrowserDate":"'+sDate+'","selectedSoftwareContractId":"","programId":"'+programId+'","contractID":"'+contractId+'","previousSearchURL":"'+previousSearchURL+'","materialID" : "'+selectedMeterialId+'","quantity" :'+qty+'}]';
			}else
				jsonObj = '[{"clientBrowserDate":"'+sDate+'","selectedSoftwareContractId":"'+softwareContractId+'","programId":"'+programId+'","previousSearchURL":"'+previousSearchURL+'","materialID" : "'+selectedMeterialId+'","quantity" :'+qty+'}]';
			InsightCommon.getServiceResponseAsync(url,jsonObj,"POST",function(dataObj){					
				data = dataObj;
				if(data!=null && jsonResponseObj!=null){
		     		   $("#addToCartDialog").html("");
		     		   getAddtoCartItemsSimpleDialogInfo(data,jsonResponseObj,qty,productInfoReq);
		                parentMaterialIdKey=data.materialIdKeyList[0];   
		     	}
			});
    	 }
	});
}

var selectedIPPwarranty = null;
function warrantiesAdded(ParentMeterialId,selectedMeterialId,itemCount){
     var url = '/insightweb/getProductInfo';
     var contractId = null;
 	 if(InsightSearch.ipsSearchCurrentContract!=null && InsightSearch.ipsSearchCurrentContract!='undefined' && InsightSearch.ipsSearchCurrentContract!=='All' && 
  			InsightSearch.ipsSearchCurrentContract!=='openMarket'){
  			contractId=InsightSearch.ipsSearchCurrentContract;
  		
  	 }
  	var parentMtrId = ParentMeterialId.replaceAll("#", "\\#").replaceAll("/","\\/").replaceAll(".","\\."); 
	var contractCartId = $('#'+parentMtrId).val();	
	if(contractCartId !=null && contractCartId!=''){
		 contractId = contractCartId;
	}
	if($('input:radio[name=contractPrice]:checked').val() !=undefined ){
		 contractId = $('input:radio[name=contractPrice]:checked').val();
	} 
 	var jsonObj  ='';
	var jsonRequestObj = populateProductInfoJSONObject(ParentMeterialId,contractId,true);
	var serviceResponseObj;
   	ParentMeterialId=parentMaterialIdKey;
   	var jsonResponseObj = null;
   	var data  = null;
	InsightCommon.getServiceResponseAsync(url,jsonRequestObj,"POST",function(ResponseObj){ 	
		jsonResponseObj = ResponseObj;
		if(data!=null && jsonResponseObj!=null){
     		   $("#addToCartDialog").html("");
     		  getAddtoCartDialogInfo(data,jsonResponseObj,itemCount);
     	   }
	});
   	selectedIPPwarranty =selectedMeterialId;
    var url = "/insightweb/transaction/addIPPtocart";
    if(contractId!=null){
        	jsonObj = '{"contractID":"'+contractId+'","warranty" : {"parentMaterialId" :  "'+ParentMeterialId+'","ippMaterialId": "'+selectedMeterialId+'","quantity": '+itemCount+'}}';
    }else jsonObj = '{"warranty" : {"parentMaterialId" :  "'+ParentMeterialId+'","ippMaterialId": "'+selectedMeterialId+'","quantity": '+itemCount+'}}';
		InsightCommon.getServiceResponseAsync(url,jsonObj,"POST",function(dataObj){					
			data = dataObj;
			if(data!=null && jsonResponseObj!=null){
	     		  $("#addToCartDialog").html("");
                  getAddtoCartDialogInfo(data,jsonResponseObj,itemCount);
             }
				
		});
}

function getAddtoCartDialogInfo(data,jsonResponseObj,itemCount){
    var markup = data;
	if(data.cart!=null){
		markup=data.cart;
	}
	var itemCount = itemCount;
	$( "#addToCartDialog" ).empty();
	/* get csid if ipp warranty is selected*/
	var csid=null;
	if(InsightSearch.icsMap!=null){
		if(selectedIPPwarranty in InsightSearch.icsMap){
			  $.each(jsonResponseObj.warrantyPdcts,function(index,value){
				  if(selectedIPPwarranty == value.materialId ){
					  csid = value.csid;
				  }
			  });
		 }  
	 }
	  markup = $.extend(markup,{"cartProductInfo":jsonResponseObj},{"selectedIPPwarranty":selectedIPPwarranty},{"itemCount":itemCount},{"trackingIdIpp":csid},{"trackingUrl":data.icsCartUrl});
	  var  addToCartDialogData = null;
	  if($.template["addToCartDialogData"] == undefined) {
		 InsightCommon.getContentAsync(InsightSearch.staticContentUrl,null,"http://www.insight.com/insightweb/assets/en_US/www/javascript/search/addToCartTemplate.html", function(data) {
			 renderAddToCartDialog(data,markup);
		 });
	  }else {
		  addToCartDialogData = $.template["addToCartDialogData"];
		  renderAddToCartDialog(addToCartDialogData,markup);
	  }     
}

function renderAddToCartDialog(addToCartDialogData,markup) {
      $.template( "addToCartDialogData",addToCartDialogData);
      markup = $.extend(markup, searchLabels);
      // adjusting currency 
      markup.totalCost = InsightCommon.formatCurrency(markup.totalCost);
      InsightCommon.renderTemplate( "addToCartDialogData",markup, "#addToCartDialog");
      /* set tracking url if ipp warranty is selected*/
      if(markup.trackingIdIpp!=null && markup.trackingIdIpp!=""){
	    	var ts = InsightCommon.getCurrentTimestamp();
	    	var src = "http://"+markup.trackingUrl+"ts="+ts+"&csid="+markup.trackingIdIpp+"&sid="+Insight.cacheSessionId; 
	    	$("#icsTrackingInAddToCartTemplate").attr("src",src);
      }
      $("#mainHeaderCartTotalPrice").html(" - "+markup.currency+" "+markup.totalCost);
      Insight.cartTotalPrice=markup.totalCost;
      selectedIPPwarranty = null;
}

/* rebates page */
function getAllRebates(){
	narrowData = null;
    postSearchByCriteria(false,true,false,null);
}

function removeRebateSearch(){
	 removeRebateFlag = true;
	 fromcs = $("#fromcs").val();
	 if(fromcs == 'true')
	 	fromcs = true;
	 else
	 	fromcs = false;
	 if(fromcs){
	 	searchTxt = $("#searchCSText").val();
	 	groupId = $("#groupId").val();
	 	setId = $("#setId").val();
	 	categoryId = $("#categoryId").val();
	 	controller = $("#controller").val();
	 	groupName = $("#groupName").val();
	 	shared = $("#shared").val();
	 	InsightSearch.cmtStandards=$("#cmtStandards").val();
	 	InsightSearch.cmtCustomerNumber=$("#cmtCustomerNumber").val();
	 	approvedItemsOnly = false;
	 }
	 InsightSearch.postData();
}

function removeOnlyApprovedItems(){
	removeApprovedItemsFlag = true;
	fromcs = $("#fromcs").val();
	if(fromcs == 'true')
		fromcs = true;
	else
		fromcs = false;
	if(fromcs){
		searchTxt = $("#searchCSText").val();
		groupId = $("#groupId").val();
		setId = $("#setId").val();
		categoryId = $("#categoryId").val();
		controller = $("#controller").val();
		groupName = $("#groupName").val();
		shared = $("#shared").val();
		InsightSearch.cmtStandards=$("#cmtStandards").val();
		InsightSearch.cmtCustomerNumber=$("#cmtCustomerNumber").val();
		approvedItemsOnly = false;
	}
	InsightSearch.postData();
}

function removeOnlyAgeementProductsOnly(){
	removeAgreementProductsFlag = true;
	fromcs = $("#fromcs").val();
	if(fromcs == 'true')
		fromcs = true;
	else
		fromcs = false;
	if(fromcs){
		searchTxt = $("#searchCSText").val();
		groupId = $("#groupId").val();
		setId = $("#setId").val();
		categoryId = $("#categoryId").val();
		controller = $("#controller").val();
		groupName = $("#groupName").val();
		shared = $("#shared").val();		
	}
	softwareLicense();
	InsightSearch.postData();
}

function approvedItemsOnlyLeftNav(){
	approvedItemsFromLeftNavFlag = true;
	fromcs = $("#fromcs").val();
	if(fromcs == 'true')
		fromcs = true;
	else
		fromcs = false;
	if(fromcs){
		searchTxt = $("#searchCSText").val();
		groupId = $("#groupId").val();
		setId = $("#setId").val();
		categoryId = $("#categoryId").val();
		controller = $("#controller").val();
		groupName = $("#groupName").val();
		shared = $("#shared").val();
		InsightSearch.cmtStandards=$("#cmtStandards").val();
		InsightSearch.cmtCustomerNumber=$("#cmtCustomerNumber").val();
		approvedItemsOnly = false;
	}
	InsightSearch.postData();
}

function agreementProductsOlny(){
	agreementProductsFromLeftNavFlag = true;
	fromcs = $("#fromcs").val();
	if(fromcs == 'true')
		fromcs = true;
	else
		fromcs = false;
	if(fromcs){
		searchTxt = $("#searchCSText").val();
		groupId = $("#groupId").val();
		setId = $("#setId").val();
		categoryId = $("#categoryId").val();
		controller = $("#controller").val();
		groupName = $("#groupName").val();
		shared = $("#shared").val();		
	}
	softwareLicense();
	InsightSearch.postData();
}

/* shopping-tools configurators page */
function getConfiguratorsPage(){
      $("#bodyContent").html("");
      var url = "/insightweb/configurator";
      var configuratorsData = getJSON(url);
      var configuratorDataArray = [];
      $.each(configuratorsData, function(index, value){
            configuratorDataArray.push({configuratorName:index, configuratorValue:value});
      });
      configuratorsData = $.extend({configuratorArray:configuratorDataArray}, {'labels':searchLabels.labels});
      var configuratorTemplate = InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/configuratorTemplate.html");
      $.template("configuratorTemplate",configuratorTemplate);
      InsightCommon.renderTemplate("configuratorTemplate", configuratorsData, "#bodyContent");
      $('a[href*="http://www.insight.com/pages/landingpage.web?id="]').each(function() {
            var href = $(this).attr("href");
            var rewrittenURL = "javascript:GetLPData('";
            var start = href.indexOf("=")+1;
            var end = href.length;
            var lpid = href.substring(start, end);
            rewrittenURL = rewrittenURL+lpid+"');";
            $(this).attr("href", rewrittenURL);
      });
}

function  normalResults(sMeterialId){
    var reloadFields = $("input[name=keySpecs]:checked").map(
    function () {
    	return '"'+this.value+'"';
    }).get().join(",");
    compareSimilar(sMeterialId,reloadFields);
}


function renderDemoCompanyStandards(){
	if(Insight.isLoggedin){
	  $("#bodyContent").html('');
      var data = getCompanyStandards();   
      var groupData = data;
      searchLabels = InsightSearch.loadSearchLabels();
      var labels = searchLabels.labels;
      var demoCompanyStandardsTemplate = InsightCommon.getContent(InsightSearch.staticContentUrl, " ", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/demoCompanyStandardsTemplate.html");
      $.template("demoCompanyStandardsTemplate", demoCompanyStandardsTemplate);
      data = $.extend({'data':data}, {'labels':labels});
      InsightCommon.renderTemplate("demoCompanyStandardsTemplate", data ,"#bodyContent");
      var testing=window.location.href;
      var address=[];
      address=testing.split("#")
      var NewUrl=address[0];
	  var groupID=decodeURIComponent((new RegExp('[?|&]' + "groupId" + '=' + '([^&;]+?)(&|#|;|$)').exec(NewUrl)||[,""])[1].replace(/\+/g, '%20'));
	  var oldCategoryId=decodeURIComponent((new RegExp('[?|&]' + "oldCategoryId" + '=' + '([^&;]+?)(&|#|;|$)').exec(NewUrl)||[,""])[1].replace(/\+/g, '%20'));
	  $('#'+oldCategoryId).click();
      if(groupID!=null && oldCategoryId!=null){
		  $.each(groupData, function(index, value){ 
			    $.each(value.csgroups, function(groupIndex, groupValue){
		               if(groupValue.groupId==groupID && oldCategoryId==value.id ){
		                      displayProductGroup(value.id, encodeURIComponent(escape(value.name)), groupValue.groupId, encodeURIComponent(escape(groupValue.groupName)),groupValue.bundleTypes, groupValue.contractId);
		                      return true; 
		                }
		              
		        });
		  });
	   }
	}else{
		  var loginURL=InsightCommon.getRootURL()+"/insightweb/login";
	      window.location.replace(loginURL);
	}
}

function displayProductGroup(categoryId, categoryName, groupId, groupName,bundleType,groupContractId){
	$('#csEmptyCartMessageError').css("display","none");
    InsightCommon.showLoading();
    var url="/insightweb/manage/getCSGroupDetails/"+groupId;	    
    categoryName = decodeURIComponent(unescape(categoryName));	
    groupName =  decodeURIComponent(unescape(groupName));	
	var jsonObj = '{"categoryId":'+categoryId+',"groupContractId":"'+groupContractId+'", "setId":'+groupId+'}';
	var setUrl = "/insightweb/account/companyStandard/groupSet";
	var responseData = InsightCommon.getServiceResponse(setUrl, jsonObj, "POST");
	var viewPricePermission = false;
	InsightSearch.numberOfSingleSelection = 0;
	var productList=[];
	$.each(responseData, function(index, value){
		var displayProductSetFlag = false;
		if(value.psitem!=null){
			$.each(value.psitem, function(itemIndex, itemValue){
				if(itemValue.product != null){
					displayProductSetFlag = true;
					productList=productList.concat(itemValue.product);
				}
			});
		}
		if(displayProductSetFlag)
			value=$.extend(value, {'displayFlag':displayProductSetFlag});
		if(value.setType == 2){
			InsightSearch.numberOfSingleSelection++;			
		}
	});
	searchProductResponse=productList;
	var testPermission = Insight.userPermissions;  
 	if($.inArray('price',testPermission)!= -1){
 		  viewPricePermission = true;
 	}
	var productSetsTemplate = InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/productSetsTemplate.html");
	$.template("productSetsTemplate", productSetsTemplate);
	$("#csRightSectionParent").html("");
	var templateData = $.extend({'productSets':responseData}, {'labels':searchLabels.labels}, {'categoryId':categoryId}, {'groupId':groupId}, {'categoryName':categoryName}, {'groupName':groupName}, {'isB2BUser':InsightSavedProducts.isB2BUser},{"bundleType":bundleType},{"viewPricePermission":viewPricePermission});	
	InsightCommon.renderTemplate("productSetsTemplate", templateData, "#csRightSectionParent");
	// hide 'Add To Order' button if all parts are discontinued
	if(responseData!=null && responseData.length > 0){
		var discontinuedItems = 0;
		var totalItems = 0;
		$.each(responseData, function(i,data){
			if(data.psitem!=null && data.psitem.length > 0){
				totalItems = totalItems + data.psitem.length;
				$.each(data.psitem, function(index,item){
					if(item.discontinued == true){
						discontinuedItems++;
					}
				});
			}
		});
		if(discontinuedItems == totalItems){
			$("#csProductGroupAddToOrder").hide();
		} 
	}
	$("#csContractId").val(groupContractId);
	InsightSearch.csGroupFlag = true;
	$.each(responseData, function(index, value){
		if(value.psitem!=null){
				$.each(value.psitem, function(itemIndex, itemValue){
							if(itemValue.selected== true){
								$("#selectedProduct"+itemValue.setId+itemIndex).attr('checked', true);
							 }
				});
	 	}
	});
  	InsightCommon.hideLoading();
}
	

function selectedProductChange(catId,index){
	$('#csProductGroupTableid'+catId+' :radio').each(function(i) {
		if (($(this).attr('id') != "selectedProduct"+catId+index))
		$(this).attr('checked', false);
	});
}

function userPreferencePage(){
	 var url="/insightweb/getSoftwarePreferences";
     return  userPreferenceTemplate(InsightCommon.getServiceResponse(url,"","POST"));
}

function userSoftwarePreferences(){
      var url="/insightweb/getSoftwarePreferences";
      return  InsightCommon.getServiceResponse(url,"","POST");
}

function userPreferenceTemplate(data){
      var url="/insightweb/getStaticContent";
      $("#bodyContent").html("");
      var userPref =InsightCommon.getContent(url,"","http://www.insight.com/insightweb/assets/en_US/www/javascript/search/userPreferenceTemplate.html");
      $.template("userPref",userPref);
      InsightCommon.renderTemplate("userPref",data ,"#bodyContent");
	  $("input:checkbox[@name=checkin1]").change(function(){
    	  $("#btnUpdate").attr("src","https://if01.insight.com/ccms_img/btn_update_orn_91x25.png");
      });
      $("input:checkbox[@name=checkin2]").change(function(){
    	  $("#btnUpdate").attr("src","https://if01.insight.com/ccms_img/btn_update_orn_91x25.png");
      });
      $("input:checkbox[@name=checkin3]").change(function(){
    	  $("#btnUpdate").attr("src","https://if01.insight.com/ccms_img/btn_update_orn_91x25.png");
      });
}

function userPrefUpdateButton(){
      var upStockValue;
      var upDidYouMeanValue;
      var upSearchValue;
      var prodList = $.map($('#platform2 option'), function(elt, i) { return '"'+$(elt).val()+'"';});
      var prodTypeList = $.map($('#productType2 option'), function(elt, i) { return '"'+$(elt).val()+'"';});
      var lanList = $.map($('#language2 option'), function(elt, i) { return '"'+$(elt).val()+'"';});
      var manuList = $.map($('#manufacturer2 option'), function(elt, i) { return '"'+$(elt).val()+'"';});
      upStockValue=getCheckboxValue('upInStock');
	  upDidYouMeanValue=getCheckboxValue('upDidYouMean');
	  upSearchValue=getCheckboxValue('upSearch');
      var upSelected=$("#upNumberCount").val();
      var url="account/updatePreferences";
      var jsonObj = '{'+
                        '"displayDidYouMean":'+upDidYouMeanValue+','+
                        '"productsPerPage":'+upSelected+','+
                        '"displayInStockOnly":'+upStockValue+','+
                        '"displayKeyword":'+upSearchValue+  ','+
                        '"manuList":['+manuList+'],'+
                        '"prodList":['+prodList+'],'+
                        '"prodTypeList":['+prodTypeList+'],'+
                        '"lanList":['+lanList+']'+
                        '}';
      $.ajax({
            url: url,
            type: "POST",
            data: (jsonObj),
            global: false,
            dataType: "json",
            async: false,
            contentType: "application/json",
            crossDomain: true,
            headers: {"Accept":"application/json"},
            success: function(data) {userPreferencePage();},
            error: function(data){searchFailed();}
      });
      /* Scroll to the Top of the Page */
	     window.scrollTo(0, 0);
}

function getCheckboxValue(checkboxId){
	if ($('#'+checkboxId).is(':checked')){
		return true;
	}else{
		return false;
	}
}

function postSearchByCriteria(inventoryBOFlag,rebateFlag,approvedItemsFlag,narrowCat){
	if(!InsightSearch.isShowThisPage){
		paginationFlag = true;
	}
	var shown = $("#shownRecords").val();
	productSet = productSetMenu;
	if(paginationFlag){
	     shown= 10;
	     getPage=1;
	}
    if(inventoryBOFlag) {
	    searchTxt ='';
	    shown = 250;
    }else if(shown == null)
	    shown = 10;
	var lastSearchURL = document.location.href;
	lastSearchURL = lastSearchURL.substring(lastSearchURL.indexOf('insightweb'),lastSearchURL.length);	
    var url =  '/insightweb/getProduct';//$('#url').val();
    var jsonObj = '{"page": '+getPage+',"pageNumber": 0,"pageSize": 0,';
    if(narrowCat!=null && narrowCat!="" )
        jsonObj =jsonObj+'"category":"'+narrowCat+'",';
    if(approvedItemsFlag)
        jsonObj = jsonObj+'"onlyApprovedItems" : "Y",';
    if (lastSearchURL != null) {
    	jsonObj =jsonObj+'"returnSearchURL": "'+lastSearchURL+'",';
  	}
    if(productSet!=null && productSet!=''){
          jsonObj =   jsonObj+'"productSet":["'+productSet+'"],';
    }
  	if(rebateFlag)
  		 jsonObj =   jsonObj+'"ccnRebate":"CCN-Rebate","rebateSearch": true,';
    if(inventoryBOFlag)
            jsonObj =jsonObj+'"inventoryBlowOut":'+inventoryBOFlag+',';
    if(InsightSearch.microSiteMfrId!=undefined && InsightSearch.microSiteMfrId!=null ){
          jsonObj =jsonObj+'"manufacturerId":"'+InsightSearch.microSiteMfrId+'",';
          InsightSearch.microSiteMfrId = null;
    }
    if(Insight.ipsFlag){
    	jsonObj =jsonObj+'"noCLP":"true",';
    }
    if(shown!=null)
   	  jsonObj =jsonObj+'"shown": '+shown+',"shownFlag":true,';
    if(nugsSortBy)
          jsonObj =jsonObj+'"nugsSortBySelect":"'+nugsSortBy+'","searchText": [';
    else
          jsonObj =jsonObj+'"searchText": [';

    jsonObj =jsonObj+searchTxt+'],"defaultSort": false,"businessUnit":"2","fieldList": [],"salesOrg": "2400","imageURL" : "http://imagesqa01.insight.com/"}';
    if (url != "" && jsonObj != "" ){
        var data = InsightCommon.getServiceResponse(url,jsonObj,"POST");
        if(typeof data!=='undefined' && data!=null){
   		 	InsightCommon.getRequestJsonObject = jsonObj;
   	 	}
        InsightSearch.displayResults(data);
        $("#approvedItemsOnly").attr("checked",false);
    }else
      alert(searchErrorMessages.labels.PleaseenterURLANDJSON);
      advanceSearch = null;
      removeObjectSession();
}

function showPppToolTipRhs1(targetDiv, sourceDiv) {
	var pos = $(sourceDiv).offset();
	$("#" + targetDiv).css({
	
	});
	$("#" + targetDiv).show();
}

function hidePppToolTip(targetDiv, sourceDiv) {
	$("#" + targetDiv).hide();
}

function replaceHash(str){
    var n=str.replace("#","_");
    return n;
}

function getNonUsCommunityContractPricingDiv(contractPricingData,srchLabels,contractDiv ){
	var contractPricingDiv = null;
	var uniqueMaterialId = replaceHash(contractDiv);
	if(contractPricingData!=null && contractPricingData.contractDisplayInfo!=null ){
		if(contractPricingData.contractDisplayInfo.contractTitle!='CALLFORPRICELABEL'){
			var contractPrice = contractPricingData.contractDisplayInfo.contractPrice;
			if(!contractPricingData.contractDisplayInfo.morePricesAvailable){
				contractPricingDiv = '<div style="width:180px;margin-left:15px;"><span style="font-size: 11px;">'+ contractPricingData.contractDisplayInfo.contractTitle+'</span>';
				contractPricingDiv = contractPricingDiv +'<span class="prices updatedPrices"><span>'+contractPricingData.contractDisplayInfo.currency+'</span>';			
				contractPricingDiv = contractPricingDiv + '<span class="price">'+ InsightCommon.formatCurrency(contractPrice)+'</span></span>';
				var contractId = "orderButton_"+contractPricingData.materialId;
				contractDiv = InsightSearch.getContractDivId(contractId);	
				 $('#centerDiv').css("width","700px");
              	 $('.productBullets').css("width","150px");	
				 $('#'+contractDiv).css("display","block");
			} 
			contractPricingDiv = contractPricingDiv+'<input type=hidden  id="'+contractPricingData.materialId+'"  value="'+contractPricingData.contractDisplayInfo.contractId+'"></div>';
		}else if (contractPricingData.contractDisplayInfo.contractTitle=='CALLFORPRICELABEL'){
			contractPricingDiv =  '<span class="callForPrice"><a  href="javascript:contactUsRequestOnclick(\''+contractPricingData.materialId+'\',\''+contractPricingData.description+'\');">'+searchLabels.labels.contactUsForPricePopUpHeading+'</a></span>';
			var contractId = "orderButton_"+contractPricingData.materialId;
			contractDiv = InsightSearch.getContractDivId(contractId);			
			$('#'+contractDiv).css("display","none");	
		}
	}else{
		contractPricingDiv = '<div id="loader" name="loader">'+searchLabels.labels.loadingPrice+'</div>';		
	}
	return contractPricingDiv;
}

function getContractPricingDiv(contractPricingData, nugsProduct,srchLabels,contractDiv ){
	var contractPricingDiv = '';
	var uniqueMaterialId = replaceHash(contractDiv);
	if(contractPricingData.contractDisplayInfo!=null){
		var contractTitleFull=contractPricingData.contractDisplayInfo.contractToolTip;
		if(contractPricingData!=null && contractPricingData.contractDisplayInfo!=null ){
			if(contractPricingData.contractDisplayInfo.contractTitle.indexOf('OPENMARKETPRICE')!=-1){
				contractPricingData.contractDisplayInfo.contractTitle = srchLabels.labels.openMarketPrice.toUpperCase();
				contractTitleFull =srchLabels.labels.openMarketPrice.toUpperCase();
			}else if(contractPricingData.contractDisplayInfo.contractTitle.indexOf('YOURPRICE')!=-1){
				contractPricingData.contractDisplayInfo.contractTitle = srchLabels.labels.yourPrice.toUpperCase();
				contractTitleFull =srchLabels.labels.yourPrice.toUpperCase();
			}
			if(contractPricingData.contractDisplayInfo.contractId!=null){
				var contractPrice = contractPricingData.contractDisplayInfo.contractPrice;
				if(!contractPricingData.contractDisplayInfo.morePricesAvailable){
					contractPricingDiv = '<div style="width:180px;margin-left:15px;"><span style="font-size: 11px;">'+ contractPricingData.contractDisplayInfo.contractTitle+'</span>';
					contractPricingDiv = contractPricingDiv +'<span class="prices updatedPrices"><span>'+contractPricingData.contractDisplayInfo.currency+'</span>';			
					contractPricingDiv = contractPricingDiv + '<span class="price">'+ InsightCommon.formatCurrency(contractPrice)+'</span></span>';
					contractPricingDiv = contractPricingDiv+'<input type=hidden  id="'+contractPricingData.materialId+'"  value="'+contractPricingData.contractDisplayInfo.contractId+'"></div>';
					var contractId = "orderButton_"+contractPricingData.materialId;
					contractDiv = InsightSearch.getContractDivId(contractId);
					$('#'+contractDiv).css("display","block");	
				}else{
					var contractDisplayInfo = contractPricingData.contractDisplayInfo;
					if(contractDisplayInfo.contractPriceLabel!=null)
						contractPricingDiv = '<span class="prices">'+contractDisplayInfo.contractPriceLabel+'</span>';
					else
						contractPricingDiv = '<span class="prices">'+searchLabels.labels.lowestPrice+'</span>';
					if(contractDisplayInfo.contractPrice!=null)
						contractPricingDiv = contractPricingDiv + '<span class="prices"><span class="price">'+ InsightCommon.formatCurrency(contractPrice)+'</span></span>';
						contractPricingDiv = contractPricingDiv + '<span  class="prices"  onmouseout="InsightCommon.closeTooltip(this);" onmouseover="InsightCommon.showTooltip(\'<center>'+contractTitleFull+'</center>\', this);">'+ contractPricingData.contractDisplayInfo.contractTitle + '</span>';
					if(contractDisplayInfo.morePricesAvailable='true'){
						contractPricingDiv = contractPricingDiv +'<a class="prices" href="javascript:viewContractPricing(\''+contractPricingData.materialId+'\');">'+searchLabels.labels.morePricesAvailable+'</a>';
					}				
					contractPricingDiv = contractPricingDiv+'<input type=hidden  id="'+contractPricingData.materialId+'"  value="'+contractPricingData.contractDisplayInfo.contractId+'">';
					var contractId = "orderButton_"+contractPricingData.materialId;
					contractDiv = InsightSearch.getContractDivId(contractId);
					$('#'+contractDiv).css("display","block");	
				}
			}else if (contractPricingData.contractDisplayInfo.contractPriceLabel=='CALLFORPRICELABEL'){
				contractPricingDiv =  '<span class="callForPrice"><a  href="javascript:contactUsRequestOnclick(\''+contractPricingData.materialId+'\',\''+contractPricingData.description+'\');">'+searchLabels.labels.contactUsForPricePopUpHeading+'</a></span>';
				var contractId = "orderButton_"+contractPricingData.materialId;
				contractDiv = InsightSearch.getContractDivId(contractId);			
				$('#'+contractDiv).css("display","none");	
			}
		}else{
			contractPricingDiv = '<div id="loader" name="loader">'+searchLabels.labels.loadingLowestPrice+'</div>';
			contractPricingDiv = contractPricingDiv + '<br><br>';
			contractPricingDiv = contractPricingDiv + '<a id="showAllPrices" onclick="return showAllPrices(\''+nugsProduct+'\');" href=" ">'+searchLabels.labels.showAllPrices+'</a>';
			contractPricingDiv = contractPricingDiv + '<br><br>';
			contractPricingDiv = contractPricingDiv + '<div id="contractPricing" style="display:none;">'+searchLabels.labels.noContractPricing+'</div>';
		}
	}
	return contractPricingDiv;
}

//popUp is an optional variable
function cpPrintCompare(popUp){
	   var savedProdString = '';
	   if(savedProducts.length != 0){
            $.each(savedProducts, function(index, value) {
                  savedProdString = savedProdString +'"'+ value+'"';
                  if(index < savedProducts.length-1){
                        savedProdString = savedProdString+',';
                  }
            });
            savedProdString = '['+savedProdString+']';
        }
      var byodUser = false;
      if(popUp == 'byod')
   	  byodUser = true;
      $.ajax({
			  url: "/insightweb/compare_print/",
			  type: "POST",
			  data: '{"materialIds":'+savedProdString+',"byodEndUser":'+byodUser+'}',
			  global: false,
			  dataType: "json",
			  async: false,
			  contentType: "application/json",
			  crossDomain: true,
			  headers: {"Accept":"application/json"},
			  success: function(data) {
				  if(popUp != null){
					  window.open("/insightweb/compare_printable?eighthundredNumber="+$("#eighthundredNumber").val(),"",'height=600,width=600,scrollbars=yes,resizable=yes');
				  }
				  else{
					  window.open("/insightweb/compare_printable?eighthundredNumber="+$("#eighthundredNumber").val());
				  }
			  }
	});
}

function similarPrintPage(mtrId){
	srchLabels = InsightSearch.loadSearchLabels();
	$.ajax({
		  url: "/insightweb/compare_print",
		  type: "POST",
		  data: '{"similarMaterialId":"'+mtrId+'"}',
		  global: false,
		  dataType: "json",
		  async: false,
		  contentType: "application/json",
		  crossDomain: true,
		  headers: {"Accept":"application/json"},
		  success: function(data) {window.open("http://www.insight.com/insightweb/compare_printable");}
	});
}

function compareSimilarPrint(materialId,reloadFields){
    var url="/insightweb/compareSimilar";
    var shown = $('#shownRecords').val();
    var jsonObj = '{"showSuggestedSearch": false,"locale": "en_US","searchOpenMarket": false,"showApprovedItemOnlyLink": false,"showProductCompare": false,"showProductCenterLink": false,';
    if(approvedItemsOnly)
                jsonObj = jsonObj+'"onlyApprovedItems" : "Y",';
    if(reloadFields!=null && reloadFields!='')
             jsonObj = jsonObj+'"field":['+reloadFields+'],';
    jsonObj=jsonObj+'"fromClp": false,"salesOffice": "2001","salesOrg": "2400","defaultPlant": "10","onlyOpenMarket": false,';
    jsonObj=jsonObj+'"pageSize": 0,"pageNumber": 0,"fieldList": [],"sort": "BestSellers","rebateSearch": false,"defaultSort": true,"useBreadcrumb": false,"similarMaterialId": "'+materialId+'","secure": false,"displayOpenMarket": false}';
    if (url != "" && jsonObj != "" ){
    	var data = InsightCommon.getServiceResponse(url,jsonObj,"POST");
    	displayPrintCompareSimilarResults(data);
    }else
          alert(searchErrorMessages.labels.PleaseenterURLANDJSON);
}

function displayPrintCompareSimilarResults(data){
    $( "#bodyContent" ).empty();
    $("#bodyContent").css('background-color', '#ffffff');
    var newData=null;
    var newExtSummary=null;
    var totalExtSpecs=0;
    var newFieldSummary=null;
    var styleWidth=915;
    var productLength=0;
    if(data.products.length>0){    	  
  	  productLength =data.products.length;
    }
    if(data.extendedSpecSummary){
          newExtSummary=new Array();
          $.each(data.extendedSpecSummary, function(index, value) {
                newExtSummary.push({Type: "Label", Value: value.label});
                $.each(value.detailLabels, function(detIndex, detValue) {
                      var tempDetArray=new Array();
                      var oddrw=false;
                      if(detIndex%2!=0)
                            oddrw=true;
                      tempDetArray.push(detValue);
                      $.each(data.products, function(prodIndex,prodValue){
                            tempDetArray.push(prodValue.extendedSpecValues[totalExtSpecs]);
                            productLength =data.products.length;
                      });
                      newExtSummary.push({Type:"Data", Value: tempDetArray,OddRow:oddrw});
                      totalExtSpecs++;
                });
          });
    }
    // field summary : construct data
    var newFieldSummary=null;
    if(data.fieldSummary){
          newFieldSummary=new Array();
          $.each(data.fieldSummary, function(index, value){
                var tempFieldSummary=new Array();
                var prodSumm=new Array();
                $.each(data.products, function(prodInd,prodVal){
                      if(prodInd==0)    data.similarMaterialId = prodVal.materialId;
                      prodSumm.push(value.prodFieldValueMap[prodVal.manufacturerPartNumber]);
                });
                var reloadSearchfieldValue=value.fieldId+'~'+value.fieldInternalValue;
                if(value.usedForCompare)
                      newFieldSummary.push({showCheckBox:true, fieldIdValue:reloadSearchfieldValue,label:value.label, values: prodSumm});
                else
                      newFieldSummary.push({label:value.label, fieldIdValue:reloadSearchfieldValue, values: prodSumm});
          });
    }
    newData='{"fieldSummary":'+data.fieldSummary+'"products":'+data.products+'"extendedSpecSummary":'+data.extendedSpecSummary+'"newExtSpecSummary":'+newExtSummary+'"newFieldSummary":'+newFieldSummary+'}';
    data.newExtSpecSummary=newExtSummary;
    data.newFieldSummary=newFieldSummary;
    data.hitCount = productLength;
    data.colcount = productLength+2;
    data.tableWidth=100/(productLength+1);
    var comptemplate = InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/compareSimilarTemplate.html");
    $.template( "compSimTemplate", comptemplate);
    searchLabels = InsightSearch.loadSearchLabels();
    data = $.extend(data, {'labels':searchLabels.labels});    
    InsightCommon.renderTemplate( "compSimTemplate", data, "#compareProductsDiv");
}

function displayPrintCompareProductResults (data,type,emailInfo){
    if(data.invalidMaterialIds && data.invalidMaterialIds.length>0){
          invalidMaterialIds = data.invalidMaterialIds;
          viewProductCompareList();
          return;
    }
    $( "#bodyContent" ).empty();
    $("#bodyContent").css('background-color', '#ffffff');
    var newData=null;
    var newExtSummary=null;
    var totalExtSpecs=0;
    var newFieldSummary=null;
    var styleWidth=600;
    var productLength=0;
    var comptemplate = '';
    if(data.products.length>0){    	  
  	  productLength =data.products.length;
    }
    if(data.extendedSpecSummary){
          newExtSummary=new Array();
          $.each(data.extendedSpecSummary, function(index, value) {
                newExtSummary.push({Type: "Label", Value: value.label});
                $.each(value.detailLabels, function(detIndex, detValue){
                      var tempDetArray=new Array();
                      var oddrw=false;
                      if(detIndex%2!=0)
                            oddrw=true;
                      tempDetArray.push(detValue);
                      $.each(data.products, function(prodIndex,prodValue){
                            tempDetArray.push(prodValue.extendedSpecValues[totalExtSpecs]);
                            productLength =data.products.length;
                      });
                      newExtSummary.push({Type:"Data", Value: tempDetArray,OddRow:oddrw});
                      totalExtSpecs++;
                });
          });
    }
    // field summary : construct data
    var newFieldSummary=null;
    if(data.fieldSummary){
          newFieldSummary=new Array();
          $.each(data.fieldSummary, function(index, value){
                var tempFieldSummary=new Array();
                var prodSumm=new Array();
                $.each(data.products, function(prodInd,prodVal){
                      prodSumm.push(value.prodFieldValueMap[prodVal.manufacturerPartNumber]);
                });
          });
    }
    newData='{"fieldSummary":'+data.fieldSummary+'"products":'+data.products+'"extendedSpecSummary":'+data.extendedSpecSummary+'"newExtSpecSummary":'+newExtSummary+'"newFieldSummary":'+newFieldSummary+'}';
    styleWidth1 = (styleWidth-((productLength+1)*10)-(productLength+1))/(productLength+1);
    styleWidth2 = (styleWidth-((productLength+1)*10))/(productLength+1);
    data.newExtSpecSummary=newExtSummary;
    data.newFieldSummary=newFieldSummary;
    data.styleWidth1=styleWidth1;
    data.styleWidth2=styleWidth2;
    data.mainStyleWidth=styleWidth-10;
    data.tableWidth=100/(productLength+1);
    data.colcount = productLength+1;
    if(type=='email'){
	   data.emailFlag=true;
	   data.printFlag = false;
	   data.emailContent = jsonCompareProductsEmail;
    }else{
	   data.emailFlag=false;
       data.printFlag = true;
   }
   if(emailInfo == ''){
	   comptemplate = InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/compareProductsTmpl.html");//search/compareProductsTmpl.html productInfoEmailTemplate
   }else{
	   comptemplate = InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/productInfoEmailTemplate.html");//search/compareProductsTmpl.html productInfoEmailTemplate
   }
    $.template( "sfsdfdsf", comptemplate);
    data = $.extend(data, {'labels':searchLabels.labels},{'emailInfo':emailInfo});
    if(type=='email')
    	 InsightCommon.renderTemplate( "sfsdfdsf", data, "#bodyContent");
    else
    	InsightCommon.renderTemplate( "sfsdfdsf", data, "#compareProductsDiv");
}

function displayCompareProductResults (data,CmpType){
      if(data.invalidMaterialIds && data.invalidMaterialIds.length>0){
            invalidMaterialIds = data.invalidMaterialIds;
            viewProductCompareList();
            return;
      }
      $( "#bodyContent" ).empty();
      $("#bodyContent").css('background-color', '#ffffff');
      var newData=null;
      var newExtSummary=null;
      var totalExtSpecs=0;
      var newFieldSummary=null;
      var styleWidth=915;
      var productLength=0;
      if(data.products.length>0){    	  
    	  productLength =data.products.length;
      }
      if(data.extendedSpecSummary){
            newExtSummary=new Array();
            $.each(data.extendedSpecSummary, function(index, value){
                  newExtSummary.push({Type: "Label", Value: value.label});
                  $.each(value.detailLabels, function(detIndex, detValue){
                        var tempDetArray=new Array();
                        var oddrw=false;
                        if(detIndex%2!=0)
                              oddrw=true;
                        tempDetArray.push(detValue);
                        $.each(data.products, function(prodIndex,prodValue){
                              tempDetArray.push(prodValue.extendedSpecValues[totalExtSpecs]);
                              productLength =data.products.length;
                        });
                        newExtSummary.push({Type:"Data", Value: tempDetArray,OddRow:oddrw});
                        totalExtSpecs++;
                  });
            });
      }
      // field summary : construct data
      var newFieldSummary=null;
      if(data.fieldSummary){
            newFieldSummary=new Array();
            $.each(data.fieldSummary, function(index, value){
                  var tempFieldSummary=new Array();
                  var prodSumm=new Array();
                  $.each(data.products, function(prodInd,prodVal){
                        prodSumm.push(value.prodFieldValueMap[prodVal.manufacturerPartNumber]);
                  });
            });
      }
      newData='{"fieldSummary":'+data.fieldSummary+'"products":'+data.products+'"extendedSpecSummary":'+data.extendedSpecSummary+'"newExtSpecSummary":'+newExtSummary+'"newFieldSummary":'+newFieldSummary+'}';
      styleWidth1 = (styleWidth-((productLength+1)*10)-(productLength+1))/(productLength+1);
      styleWidth2 = (styleWidth-((productLength+1)*10))/(productLength+1);
      data.newExtSpecSummary=newExtSummary;
      data.newFieldSummary=newFieldSummary;
      data.styleWidth1=styleWidth1;
      data.styleWidth2=styleWidth2;
      data.mainStyleWidth=styleWidth-10;
      data.tableWidth=100/(productLength+1);
      data.colcount = productLength+1;
      data.printFlag = false;
      data.emailFlag = false;
      data.jsonCompareProductsEmail =null;
      if(sortByCP!="" && sortByCP=='des')
    	  data.sortByCP = 'asc';
      else if(sortByCP!="" && sortByCP=='asc')
    	  data.sortByCP = 'des';
      else
    	  data.sortByCP = 'des';
	  var comptemplate = InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/compareProductsTmpl.html");
	  $.template( "sfsdfdsf", comptemplate);
	  searchLabels = InsightSearch.loadSearchLabels();
      data = $.extend(data, {'labels':searchLabels.labels});
	  InsightCommon.renderTemplate( "sfsdfdsf", data, "#bodyContent");
	  return;
}

function compareProductsEmail(){
      $( "#bodyContent" ).empty();
      var emailTempColleague = InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/transaction/sendtocolleaguetemplate.html");
      $.template( "emailTempColleague", emailTempColleague);
      var data =  {"fromPage":"productcompare"};
      var srchLabels = InsightCommon.getMessageContent(InsightSearch.messageUrl, Insight.locale, "http://www.insight.com/messages/transaction/ShoppingCart/shoppingcartlabels.txt");
      searchLabels = eval('(' + srchLabels + ')');
      data = $.extend(data, {'labels':searchLabels.labels});
      InsightCommon.renderTemplate( "emailTempColleague",data, "#bodyContent");
}

function sendCompareProductsEmail(fromPage){
    var jsonTemplate= InsightCommon.getContent(InsightSearch.staticContentUrl,"", "http://www.insight.com/search/compareProductEmailJsonObject.txt");
    jsonCompareProductsEmail = eval('(' + jsonTemplate + ')');
    jsonCompareProductsEmail.fromName=$("#yournameinput").val();
    jsonCompareProductsEmail.from=$("#youremailinput").val();
    jsonCompareProductsEmail.to=$("#recipientemailinput").val();
    jsonCompareProductsEmail.subject=$("#yournameinput").val() + ' has emailed you this message from Insight';
    var mailComment = $("#yourcommentsinput").val();
    $( "#bodyContent" ).empty();
    var emailInfo = { 
			"fromName" : jsonCompareProductsEmail.fromName,
			"mailComment" : mailComment,
			"fromPage" : fromPage,
			"from" : jsonCompareProductsEmail.from
		}; 
    spcComparePrintProducts(savedProducts,'email','',emailInfo);
    $('.hideForEMail').hide();
    jsonCompareProductsEmail.content = $("#ProductInfoEmailDiv").html();
    var url ="/insightweb/report/sendEmail";
    var  jsonStr = JSON.stringify(jsonCompareProductsEmail); jsonStr=jsonStr+'"}';
    var data = InsightCommon.getServiceResponse(url,jsonStr,"POST");
    alert(data.response);
    $('.hideForEMail').show();
}

function contactUsRequestOnclick(materialId,description){
	data = $.extend({'materialId':materialId},{'description':description});
	var contactUsConfiguratorTemplate = InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/contactPricingPopUpTemplate.html");
	$("#contactUsPopUpDivTemplate").attr("title", searchLabels.labels.contactUsForPricePopUpHeading);
	$("#contactUsPopUpDivTemplate").html("");
    $.template("contactUsConfiguratorTemplate",contactUsConfiguratorTemplate);
    data = $.extend(data, {'labels':searchLabels.labels});
    InsightCommon.renderTemplate("contactUsConfiguratorTemplate", data, "#contactUsPopUpDivTemplate");
    $("#contactUsPopUpDivTemplate").dialog({ width: 'auto',height:'auto',resizable: true, position: 'relative', draggable: true});
}

function contactUsSendButton(){
	 var contactUsName=$("#contactUsNameTextBox").val();
     var contactUsEmail=$("#contactUsEmailTextBox").val();
     var contactUsAdditional=$("#contactUsAddEmailTextBox").val();
     var contactUsCountry=$("#contactUsCountryTextBox").val();
     var contactUsQuantity=$("#contactUsQuantityTextBox").val();
     var contactUsPartNum=$("#contactUsPartTextBox").val();
     var contactUsDescption=$("#contactUsDescriptionTextBox").val();
     $( "#contactUsPopUpDivEmailTemplate" ).empty();
     var emailTempContactPricing = InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/contactPricingEmailTemplate.html");
     $.template( "emailTempContactPricing", emailTempContactPricing);
     InsightCommon.renderTemplate( "emailTempContactPricing", {'labels':searchLabels.labels}, "#contactUsPopUpDivEmailTemplate");
     $('#ContactPriceMail').css('display','none');
     $('#ContactPriceName').text(contactUsName);
     $('#ContactPriceEmailName').text(contactUsEmail);
     $('#ContactPriceAddName').text(contactUsAdditional);
     $('#ContactPriceCountry').text(contactUsCountry);
     $('#ContactPriceQuantity').text(contactUsQuantity);
     $('#ContactPricePartNum').text(contactUsPartNum);
     $('#ContactPriceDescription').text(contactUsDescption);
     if((contactUsName!="")&&(contactUsEmail!="")&&(contactUsPartNum!="")){
           if(validatEmail(contactUsEmail)){
			     var url ="/insightweb/report/sendEmail";
			     var jsonTemplate= InsightCommon.getContent(InsightSearch.staticContentUrl,"", "http://www.insight.com/search/compareProductEmailJsonObject.txt");
			     jsonObj = eval('(' + jsonTemplate + ')');
			     var jsonStr = '';
			     jsonObj.content = jQuery("#ContactPriceMail").html();
			     var  emailData = JSON.stringify(jsonObj); jsonStr=jsonStr+'"}';
			     var data = InsightCommon.getServiceResponse(url,emailData,"POST");
            }
           $("#contactUsPopUpDivTemplate").dialog("close");
     }else{
           var str="";
           if((contactUsName=="")){
                 var str1 =' Name,';
                 str=str+str1;
           }
           if((contactUsEmail=="")){
                 var str2 =' Email,';
                 str=str+str2;
           }
           if((contactUsPartNum=="")){
                 var str3 =' Part Number,';
                 str=str+str3;
           }
           alert('Please enter '+str);
     }
}

function contactUsCancelButton(){
    $("#contactUsPopUpDivTemplate").dialog("close");
}

function approvedItems(){
	window.open("http://www.insight.com/insightweb/approvedItems","mywindow","menubar=1,resizable=1,width=625,height=750");
}

function productdetail(materialId,softwareContractId,core){	
	/*core is an optional value*/
	InsightCommon.showLoading();
	var returnToSearchResultsURL =  document.location.href;   
	var groupName = $('#groupName').val();
	var categoryId = $('#categoryId').val();
	var setId = $('#setId').val();
	var groupId =$('#groupId').val();
	var shared =  $('#shared').val();
	var controller = $('#controller').val();
	var isFromCs = $('#fromcs').val();
	if (core != null){
		InsightSearch.returnToSearchResultsURL = returnToSearchResultsURL.substring(returnToSearchResultsURL.indexOf('/insightweb'),returnToSearchResultsURL.length);   
		setTimeout('document.location.href="/insightweb/search#!ProdInfoMtrId='+encodeURIComponent(materialId)+'~licenseContractIds='+softwareContractId+'~fromSearch?'+core+'"',600);
	}else{
	  InsightSearch.returnToSearchResultsURL = returnToSearchResultsURL.substring(returnToSearchResultsURL.indexOf('/insightweb'),returnToSearchResultsURL.length);   
	  setTimeout('document.location.href="/insightweb/search#!ProdInfoMtrId='+encodeURIComponent(materialId)+'~licenseContractIds='+softwareContractId+'~fromSearch?isFromCs='+isFromCs+'&groupName='+groupName+'&categoryId='+categoryId+'&setId='+setId+'&groupId='+groupId+'&shared='+shared+'&controller='+controller+'"',600);
	}
}

function setReturnToSearchResultsUrl(){
	InsightCommon.showLoading();
	var returnToSearchResultsURL =  document.location.href; 
	InsightSearch.returnToSearchResultsURL = returnToSearchResultsURL.substring(returnToSearchResultsURL.indexOf('/insightweb'),returnToSearchResultsURL.length);
}

function approvedItemsCloseWindow(){
	window.close();
}

function prdInfoMfrSearchResults(category,mfrId,isJSON){
	var lastSearchUrl = $("#lastSearchURL").val();
	var searchType = $("#returnSearchType").val();
	if (!InsightCommon.isFromSearch && (((searchType == "IBO") && isJSON ) || ((searchType == "SEARCH" ) && isJSON) ) ) {
		document.location.href= InsightCommon.getRootURL()+"/"+lastSearchUrl;
	}else if(InsightCommon.isFromSearch && InsightSearch.returnToSearchResultsURL!=undefined && InsightSearch.returnToSearchResultsURL!=null && isJSON){
		 document.location.href = InsightSearch.returnToSearchResultsURL;
	}else{
		document.location.href = 'http://www.insight.com/insightweb/search#searchResults#page=1#field=%22A-MARA-MFRNR~'+mfrId+'%22#removeBreadCrumb=null#inStockFilterType=BothInStockAndOutOfStock#noCLP=true#category='+category+'#searchText=';
	}
}

function inventoryBlowOutResults(){
	 var url =  '/insightweb/getInventoryBlowOut';
	 var data = InsightCommon.getServiceResponse(url,'',"GET");
	 return data;
}

function isNumeric(input) {	
	 input = parseFloat(input.toString().replace(/,/g,"."));
	 return !isNaN(parseFloat(input)) && isFinite(input);
}

function productResearchRequest(){
	var data = "";
    data = $.extend(data, {'labels':searchLabels.labels});
    var ipsContractDropData = InsightCommon.getServiceResponse("/insightweb/endUser/getContracts/"+InsightCommon.getCurrentTimestamp(),null,"GET");   
    if(ipsContractDropData!=null && ipsContractDropData.contracts.length > 0)
    	data = $.extend(data, {'contracts':ipsContractDropData.contracts},{"contractsAvailable":true});
    else 
    	data = $.extend(data, {"contractsAvailable":false});
   var configuratorTemplate = InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/productResearchRequestPopUp.html");
   $("#productResearchRequestTemplate").attr("title", searchLabels.labels.researchRequestPopUpHeading);
   $("#productResearchRequestTemplate").html("");
   $.template("configuratorTemplate",configuratorTemplate);
   InsightCommon.renderTemplate("configuratorTemplate", data, "#productResearchRequestTemplate");
   if(ipsContractDropData!=null && ipsContractDropData.contracts.length > 0) {
	   $("#productResearchRequestTemplate").dialog({ width: '650',height:'auto',resizable: true, position: 'relative', draggable: true});
   }else{
	   $("#productResearchRequestTemplate").dialog({ width: '340',height:'auto',resizable: true, position: 'relative', draggable: true});
   }
}

function validatEmail(prodResearchEmail){
   var emailPattern=/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
   if(prodResearchEmail.match(emailPattern)){
         return true;
   }else{
         alert("Enter a valid Email Address");
         return false;
   }
}

function ProdResearchSendButton(){
   var prodResearchName=$("#prodResearchNameTextBox").val();
   var prodResearchEmail=$("#prodResearchEmailTextBox").val();
   var prodResearchAdditional=$("#productAddEmailTextBox").val();
   var prodResearchCountry=$("#prodResearchCountryTextBox").val();
   var prodResearchQuantity=$("#prodResearchQuantityTextBox").val();
   var prodResearchPartNum=$("#prodResearchPartTextBox").val();
   var prodResearchMfr=$("#prodResearchMfrTextBox").val();
   var prodResearchVersion=$("#prodResearchVersionTextBox").val();
   var prodResearchDescrption=$("#prodResearchDescriptionTextBox").val();
   var prodResearchContract=$("#productAddContractTextBox").val();
   var supportEmail = null;
   var mailResponse = null;
   var isManintenanceFlag = "";
   var contractsAvailable = false;
   var manintenanceFlag = $('input:radio[name=Config]:checked').val();
   if (manintenanceFlag == 'Yes') {
	   isManintenanceFlag = searchLabels.labels.yes;
   }else{
	   isManintenanceFlag = searchLabels.labels.no;
   }
   if(prodResearchContract != null){
	   contractsAvailable = true;
   }
   $( "#productResearchRequestEmailTemplate" ).empty();
   var emailTempProductResearch = InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/ProductResearchEmail.html");
   $.template( "emailTempProductResearch", emailTempProductResearch);
   var data = InsightCommon.getServiceResponse("/insightweb/transaction/getUserInformation",null,"GET",false);
   data = $.extend(data, {'labels':searchLabels.labels, "isManintenance":isManintenanceFlag, "contractsAvailable":contractsAvailable });
   InsightCommon.renderTemplate( "emailTempProductResearch",data, "#productResearchRequestEmailTemplate");
   $('#researchRequestMail').css('display','none');
   $('#productResearchEmailName').text(prodResearchName);
   $('#productResearchEmail1').text(prodResearchEmail);
   $('#productResearchAddEmail').text(prodResearchAdditional);
   $('#productResearchEmailCountry').text(prodResearchCountry);
   $('#productResearchEmailQuantity').text(prodResearchQuantity);
   $('#productResearchEmailPartNum').text(prodResearchPartNum);
   $('#productResearchEmailVersion').text(prodResearchVersion);
   $('#productResearchEmailManufacturer').text(prodResearchMfr);
   $('#productResearchEmailDescription').text(prodResearchDescrption);
   $('#productResearchWebGroup').text(data.UserInformation.webGroupId);
   $('#productResearchSoldTo').text(data.UserInformation.soldto);
   $('#productResearchContract').text(prodResearchContract);
   $( "#productResearchDistributionEmailTemplate" ).empty();
   var productResearchDistributionEmailTemplate = InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/ProductRequestDistributionEmail.html");
   $.template( "productResearchDistributionEmailTemplate", productResearchDistributionEmailTemplate);
   data = $.extend(data, {'labels':searchLabels.labels, "isManintenance":isManintenanceFlag, "contractsAvailable":contractsAvailable});
   InsightCommon.renderTemplate( "productResearchDistributionEmailTemplate",data, "#productResearchDistributionEmailTemplate");
   $('#productResearchDistrbutionName').text(prodResearchName);
   $('#ProductResearchDistrbutionEmail').text(prodResearchEmail);
   $('#ProductResearchDistrbutionAddEmail').text(prodResearchAdditional);
   $('#productResearchDistrbutionContract').text(prodResearchContract);
   $('#ProductResearchDistrbutionCountry').text(prodResearchCountry);
   $('#ProductResearchDistrbutionWebGroup').text(data.UserInformation.webGroupId);
   $('#ProductResearchDistrbutionSoldTo').text(data.UserInformation.soldto);
   $('#ProductResearchDistrbutionPartNum').text(prodResearchPartNum);
   $('#ProductResearchDistrbutionManufacturer').text(prodResearchMfr);
   $('#productResearchDistributionVersion').text(prodResearchVersion);
   $('#ProductResearchDistrbutionQuantity').text(prodResearchQuantity);
   $('#ProductResearchDistrbutionDescription').text(prodResearchDescrption);
   var clientData = InsightCommon.getServiceResponse("/insightweb/endUser/clientSupport/"+InsightCommon.getCurrentTimestamp(),null,"GET",false);
   $.each(clientData, function(key, item){
	   if (item.supportTopicName == "Product Information Request"){
		   supportEmail = item.supportTopicEmail; 
	   }
   });
   	
   if((prodResearchName!="")&&(prodResearchEmail!="")&&(prodResearchPartNum!="")&&(prodResearchDescrption!="")){
         if(validatEmail(prodResearchEmail)){
        	 var url ="/insightweb/report/sendEmail";
        	 var jsonTemplate= InsightCommon.getContent(InsightSearch.staticContentUrl,"", "http://www.insight.com/search/compareProductEmailJsonObject.txt");
             jsonObj = eval('(' + jsonTemplate + ')');
             var jsonStr = '';
             jsonObj.content = jQuery("#productResearchRequestEmailTemplate").html();
             jsonObj.to = $('#productResearchEmail1').text();
             jsonObj.subject = searchLabels.labels.productResearchSubject+" "+prodResearchPartNum;
             var  emailData = JSON.stringify(jsonObj); jsonStr=jsonStr+'"}';
             var data = InsightCommon.getServiceResponse(url,emailData,"POST");
         }
         if (validatEmail(supportEmail)){
        	 var url ="/insightweb/report/sendEmail";
        	 var jsonTemplate= InsightCommon.getContent(InsightSearch.staticContentUrl,"", "http://www.insight.com/search/compareProductEmailJsonObject.txt");
             jsonObj = eval('(' + jsonTemplate + ')');
             var jsonStr = '';
             jsonObj.content = jQuery("#productResearchDistributionEmailTemplate").html();
             jsonObj.to = supportEmail;
             jsonObj.subject = searchLabels.labels.productResearchSubject+" "+prodResearchPartNum;
             var  emailData = JSON.stringify(jsonObj); jsonStr=jsonStr+'"}';
             mailResponse = InsightCommon.getServiceResponse(url,emailData,"POST");
         }
         if(mailResponse.response != ''){
        	 var button = '<div class="prodOkButton"><button id="prodCloseDialog" type="button"><span>Ok</span></button></div>';
        	 $("#productResearchRequestTemplate").html(searchLabels.labels.productResearchMessage +' \n'+button);
        	 $("#prodCloseDialog").click(function(){
        		 $("#productResearchRequestTemplate").dialog("close");
        	 });
         }
    }else{
         var str="";
         if((prodResearchName=="")){
               var str1 =' Name,';
               str=str+str1;
         }
         if((prodResearchEmail=="")){
               var str2 =' Email,';
               str=str+str2;
         }
         if((prodResearchPartNum=="")){
               var str3 =' Part Number,';
               str=str+str3;
         }
         if((prodResearchDescrption=="")){
               var str4 =' Product Description.';
               str=str+str4;
         }
         alert('Please enter '+str);
   }
   
}


function ProdResearchCancelButton(){
   $("#productResearchRequestTemplate").dialog("close");
   $("#productResearchRequestTemplate").html('');
}

function addToCartDialogClose(){
	$("#addToCartDialog").empty();
	$("#addToCartDialog").dialog("close");	 	
}

//bind mousedown on entire document
$(document).mousedown(function(e){
	var clicked=$(e.target); // get the element clicked   
	if(clicked.is('#addToCartDialog') || clicked.parents().is('#addToCartDialog') || clicked.is('.ui-dialog-titlebar ')  ) {
	// clicked happend within the dialog
		if($("div#commonContractDropDown").length!=0){
			$('div#commonContractDropDown').remove(); // to remove the contract dropdown 
		}	
	}else if(clicked.is('#commonContractDropDown') || clicked.parents().is('#commonContractDropDown')){
		// clicked inside contract dropdown 
	}else {
	// outside click
		if($("div#commonContractDropDown").length!=0){
			$('div#commonContractDropDown').remove(); // to remove the contract dropdown 
		}
	}
});


function clearAllCheckBoxes(){
	$("input:checked").each(function(index, value){
		$(this).attr('checked', false);
	});
}

function bubbleUp(state,hover,obj , topval , leftVal){
	var theBubble = $(hover);
	var lft = $(obj).offset().left;
	var tp = $(obj).offset().top;
	var theWidth = (theBubble.width())/2-46;
	var top = tp-topval+10;
	var left = lft+leftVal;
	theBubble.css("position", "absolute");
	theBubble.css("top", top+"px");
	theBubble.css("left", left+"px");
	// toggle function to disable/enable bubble tip.
	var display = ((theBubble.css("display") == 'block') ? 'none' : 'block');
	theBubble.css("display", display);
}

function getCompareProductsResponse(materialIdString){
	var url="/insightweb/compareProducts";
	var jsonObj='{"showSuggestedSearch":false,"locale":"en_US","searchOpenMarket":false,"showApprovedItemOnlyLink":false,"showProductCompare":false,"showProductCenterLink":false,"catalogData":{"customCatalogId":null,"overrideCatalogId":null,"approvedItemsCatalogId":null,"purchasableCatalogSet":null,"filterManufacturerCatalogId":null},"fromClp":false,"salesOffice":"2001","salesOrg":"2400","defaultPlant":"10","onlyOpenMarket":false,"pageSize":0,"pageNumber":0,"fieldList":[],"sort":"BestSellers","rebateSearch":false,"defaultSort":true,"useBreadcrumb":false,"materialIds":'+materialIdString+',"secure":false,"displayOpenMarket":false';
	if(sortByCP!="" && sortByCPFieid!=""){
		jsonObj = jsonObj+',"nugsSortBy":"'+sortByCPFieid+'","nugsSortBySelect":"'+sortByCP+'"';
	}
	jsonObj = jsonObj+'}';
	var data = InsightCommon.getServiceResponse(url,jsonObj,"POST");
	return data;
}

function checkIfValidProducts(data, materialIdsArray){
	/* populate invalidMaterialIdsArray  */
	if(data.invalidMaterialIds && data.invalidMaterialIds.length > 0)
		invalidMaterialIds = data.invalidMaterialIds;
	else{
		/* more materials supplied than valid products means there are invalid products */
		if(materialIdsArray.length > data.validMaterialIds.length){
			for(var i=0; i<materialIdsArray.length; i++){
				var tempIndex = $.inArray(materialIdsArray[i], data.validMaterialIds);
				if(tempIndex == -1)
					invalidMaterialIds.push(materialIdsArray[i]);
			}
		}
	}
	if(invalidMaterialIds.length > 0)
		return false;
	else
		return true;
}

function makeMaterialIdString(materialIdArray){
	/* return string of materialIds in [] */
	var materialIdString='[';
	if(materialIdArray && materialIdArray.length > 0){
		$.each(materialIdArray,function(index,value){
			materialIdString=materialIdString+'"'+value+'"';
			if(index+1==materialIdArray.length)
				materialIdString=materialIdString+']';
			else
				materialIdString=materialIdString+',';
		});
	}else{
		materialIdString=materialIdString+']';
	}
	return materialIdString;
}

function updateSavedProductsInSession(){
	var savedProductsString = makeMaterialIdString(savedProducts);
	InsightSavedProducts.savedProducts = savedProducts;
	var url = '/insightweb/sendSavedProducts';
	var data = InsightCommon.getServiceResponse(url, savedProductsString, "POST");
}

function IPPwarantiesAddFromProductInfo(warrantyMtrId, parentMtrId, productsResponse,quantity,type){
	var contractId = null;	  
	var serviceResponseObj;
  	var contractMtrId = parentMtrId.replaceAll("#", "\\#").replaceAll("/","\\/").replaceAll(".","\\."); 
  	contractMtrId = $('#'+contractMtrId).val();	
	if(contractMtrId !=null && contractMtrId!=''){
		 contractId = contractMtrId;
	}	
  	var url = "/insightweb/transaction/addtocart";
    if(contractId!=null){    	 
      	jsonObj = '[{"clientBrowserDate":"'+sDate+'","contractID":"'+contractId+'","previousSrearchURL":"","materialID" : "'+parentMtrId+'","quantity" :'+quantity+',"warranty" : {"parentMaterialId" : "'+parentMtrId+'","ippMaterialId": "'+warrantyMtrId+'"}}]';
    }else jsonObj = '[{"clientBrowserDate":"'+sDate+'","previousSrearchURL":"","materialID" : "'+parentMtrId+'","quantity" :'+quantity+',"warranty" : {"parentMaterialId" : "'+parentMtrId+'","ippMaterialId": "'+warrantyMtrId+'"}}]';
    InsightCommon.getServiceResponseAsync(url,jsonObj,"POST",function(data){		
                    	if(type=='NOPOP'){
                    		document.location.href="http://www.insight.com/insightweb/viewCart";
                    	}else if(productsResponse!=null){
                    		$("#addToCartDialog").html("");
                    		getAddtoCartItemsDialogInfo(data,productsResponse,quantity);
                    	}
			
    });
}

function addTocartFromProductInfo(selectedMeterialId,targetWindow){	 
	 var quantity = 1;
	 var flag = false;
	 if($("#contractM_prod_qty").val() != undefined){
		  quantity =  $('#contractM_prod_qty').val();
	 }else{
		   quantity =  $('#main_prod_qty').val();	 
		   if($('#main_prod_qty').length==0){
				 quantity = Insight.miniPPPQty;
		   }
		 var parentMtrId = selectedMeterialId;
		 var warrantyId =  $('#main_prod_warranty_material_id').val();
		 if($('#main_prod_warranty_material_id').length==0){
			 warrantyId = Insight.miniPPPWarr;
		 }
		 if(targetWindow == 'miniPPP'){
			 warrantyId = '';
		 }
		 if(warrantyId!='' ){			
		   selectedMeterialId = '"'+selectedMeterialId+'","'+warrantyId+'"';
		   InsightSearch.warrantyAddedFromProductInfo = warrantyId;
		   var jsonRequestObj1 = '{"showSuggestedSearch":false,"locale":"en_US","searchOpenMarket":false,"showApprovedItemOnlyLink":false,"showProductCompare":false,"showProductCenterLink":false,"catalogData":{"customCatalogId":null,"overrideCatalogId":null,"approvedItemsCatalogId":null,"purchasableCatalogSet":null,"filterManufacturerCatalogId":null},"fromClp":false,"salesOffice":"2001","salesOrg":"2400","defaultPlant":"10","onlyOpenMarket":false,"pageSize":0,"pageNumber":0,"fieldList":[],"sort":"BestSellers","rebateSearch":false,"defaultSort":true,"useBreadcrumb":false,"materialIds":['+selectedMeterialId+'],"secure":false,"displayOpenMarket":false}';
		   var url="/insightweb/compareProducts";		
		   var productsResponse = null;
		   var data = null;
		   // removing compare products call & reusing product info response
		   productsResponse = InsightSearch.productInfoResponse;
		   $("#addToCartDialog").html($("#loading").html());
		   $("#addToCartDialog").dialog({ width: 600, resizable: false, modal:true, position:['top',150],overlay: { background: "#F2F2F2" },
			        close: function (event, ui) {
			            if (event.originalEvent && $(event.originalEvent.target).closest(".ui-dialog-titlebar-close").length){}
			        }
		   }).find("button").click(function (){
			        $(this).closest(".ui-dialog-content").dialog("close");
		   });
		   insightLightBox.styleCustomDialog("#addToCartDialog");
	 	  	/* *
	 	  	 * added code for synchronize calls 
	 	  	 */		
	     	var contractId = null;	  
		  	var contractMtrId = parentMtrId.replaceAll("#", "\\#").replaceAll("/","\\/").replaceAll(".","\\."); 
		  	contractMtrId = $('#'+contractMtrId).val();	
			if(contractMtrId !=null && contractMtrId!=''){
				 contractId = contractMtrId;
			}	
		  	var url = "/insightweb/transaction/addtocart";
		    var selectedWarrantyType = $("#selectedWarrantyTypeID").val();
		    if(contractId!=null){    	 
		    	  if(selectedWarrantyType =='IPP'){
		    		  jsonObj = '[{"clientBrowserDate":"'+sDate+'","contractID":"'+contractId+'","previousSrearchURL":"","materialID" : "'+parentMtrId+'","quantity" :'+quantity+',"warranty" : {"parentMaterialId" : "'+parentMtrId+'","ippMaterialId": "'+warrantyId+'"}}]';
		    	  }else if(selectedWarrantyType =="OEM"){
		    		  jsonObj = '[{"clientBrowserDate":"'+sDate+'","contractID":"'+contractId+'","materialID":"'+parentMtrId+'","quantity" :'+quantity+',"warrantyDetail":{"parentMaterialId":"'+parentMtrId+'","warrMaterialId":"'+warrantyId+'","visited":false}}]';
		    	  }
		    }else{
		    	  if(selectedWarrantyType =='IPP'){
		    		  jsonObj = '[{"clientBrowserDate":"'+sDate+'","previousSrearchURL":"","materialID" : "'+parentMtrId+'","quantity" :'+quantity+',"warranty" : {"parentMaterialId" : "'+parentMtrId+'","ippMaterialId": "'+warrantyId+'"}}]';
		    	  }
		    	  else if(selectedWarrantyType =="OEM"){
		    		  jsonObj = '[{"clientBrowserDate":"'+sDate+'","materialID":"'+parentMtrId+'","quantity" :'+quantity+',"warrantyDetail":{"parentMaterialId":"'+parentMtrId+'","warrMaterialId":"'+warrantyId+'","visited":false}}]';
		    	  }
		    	  
		    } 
		   /* accessories request object */
	       var softwareContractId = $('#softwareContractId_'+parentMtrId).val();
	       var jsonRequestObj = populateProductInfoJSONObject(parentMtrId,contractId,false,softwareContractId);
		   var productInfoReq = JSON.parse(jsonRequestObj);
		   productInfoReq.loadAccessories = true;
		   productInfoReq.cartFlag = false; 
		   productInfoReq.contractId = null; 
		   productInfoReq = JSON.stringify(productInfoReq);
	       InsightCommon.getServiceResponseAsync(url,jsonObj,"POST",function(dataObj){	
		       data = dataObj;
			   if(productsResponse!=null){
				  $("#addToCartDialog").html("");
			      getAddtoCartItemsSimpleDialogInfo(data,productsResponse,quantity,productInfoReq);
			      parentMaterialIdKey=data.materialIdKeyList[0];
		       }
		   });
		   flag = true;
		 }
	}
	if(flag == false){
		   selectedMeterialId = '"'+selectedMeterialId+'"';
		   productCenterAddtoCart(selectedMeterialId,quantity,'PPC',null,null,null,null,null,null);
	 }
}

function addTocartFromNoPop(selectedMeterialId){
	var quantity = 1;
	var flag = false;
	if($("#contractM_prod_qty").val() != undefined){
	  quantity =  $('#contractM_prod_qty').val();
	}else {
	   quantity =  $('#main_prod_qty').val();
	   var parentMtrId = selectedMeterialId;
	   var warrantyId =  $('#main_prod_warranty_material_id').val();
	   var selectedWarrantyType = $("#selectedWarrantyTypeID").val();
	   if(selectedWarrantyType =="IPP"){
		   if(warrantyId!='' ){			
			    selectedMeterialId = '"'+selectedMeterialId+'","'+warrantyId+'"';
			    var jsonRequestObj1 = '{"showSuggestedSearch":false,"locale":"en_US","searchOpenMarket":false,"showApprovedItemOnlyLink":false,"showProductCompare":false,"showProductCenterLink":false,"catalogData":{"customCatalogId":null,"overrideCatalogId":null,"approvedItemsCatalogId":null,"purchasableCatalogSet":null,"filterManufacturerCatalogId":null},"fromClp":false,"salesOffice":"2001","salesOrg":"2400","defaultPlant":"10","onlyOpenMarket":false,"pageSize":0,"pageNumber":0,"fieldList":[],"sort":"BestSellers","rebateSearch":false,"defaultSort":true,"useBreadcrumb":false,"materialIds":['+selectedMeterialId+'],"secure":false,"displayOpenMarket":false}';
			    var url="/insightweb/compareProducts";		
				var productsResponse = getJSONResponse(url, jsonRequestObj1);			
		 	  	$("#addToCartDialog").html($("#loading").html());
				IPPwarantiesAddFromProductInfo(warrantyId, parentMtrId, productsResponse,quantity,'NOPOP');
				flag = true;
			 }
		}else if(selectedWarrantyType =="OEM"){
			 if(warrantyId!='' ){
				 var contractId = null;	
				 var addproductWithOEMReq ="";
				 var contractMtrId = parentMtrId.replaceAll("#", "\\#").replaceAll("/","\\/").replaceAll(".","\\."); 
			  	 contractMtrId = $('#'+contractMtrId).val();	
				 if(contractMtrId !=null && contractMtrId!=''){
					 contractId = contractMtrId;
				 }
				 if(contractId!=null){
					 addproductWithOEMReq = '[{"clientBrowserDate":"'+sDate+'","contractID":"'+contractId+'","materialID":"'+parentMtrId+'","quantity" :'+quantity+',"warrantyDetail":{"parentMaterialId":"'+parentMtrId+'","warrMaterialId":"'+warrantyId+'","visited":false}}]';
				 }else{
					 addproductWithOEMReq = '[{"clientBrowserDate":"'+sDate+'","materialID":"'+parentMtrId+'","quantity" :'+quantity+',"warrantyDetail":{"parentMaterialId":"'+parentMtrId+'","warrMaterialId":"'+warrantyId+'","visited":false}}]';
				 }
				 InsightCommon.getServiceResponseAsync("/insightweb/transaction/addtocart",addproductWithOEMReq,"POST",function(data){		
						if(data !=null){
			        		document.location.href="http://www.insight.com/insightweb/viewCart";
			        	}
				 });
				 flag = true;
			 }
	     }
	}
	if(flag == false){
		   selectedMeterialId = '"'+selectedMeterialId+'"';
		   productCenterAddtoCart(selectedMeterialId,quantity,'NOPOP',null,null,null,null,null,null);
	 }
}
var productsTotalQuantity = 0;
var bundleCartURL = "/insightweb/transaction/addtocart";
function productCenterAddtoCart(materialIds,quantity,fromPage,bundleType,bundleConfig,bundleName){
	var arrPC = [];
    var arrPC = materialIds.split(',');   
    var contractId = null;
	if(bundleType!=null){
	 	 contractId = $('#csContractId').val();
	 	 if(contractId==null || contractId==''){
	 		contractId = '';
	 	 }
    }
	if(arrPC.length==1){	
		if($('#contractPricingTemplatePopUpDiv input:radio[name=contractPrice]:checked').val() !=undefined ){
			contractId = $('#contractPricingTemplatePopUpDiv input:radio[name=contractPrice]:checked').val();
		} 	
		var url = '/insightweb/getProductInfo';		
    	var jsonRequestObj2 = populateProductInfoJSONObject(delquote(arrPC[0]),contractId,false);
    	/*duplicationg product info call for loading accessories*/
    	var productInfoReq = JSON.parse(jsonRequestObj2);
    	productInfoReq.loadAccessories = true;
    	productInfoReq.cartFlag = false; 
    	productInfoReq.contractId = contractId; 
    	/*this line is added to allow more prices in ips case need to verify the condition
    	 * it can always be null. will this handle open market case 
    	 */
    	productInfoReq = JSON.stringify(productInfoReq);
    	var serviceResponseObj;
    	var jsonResponseObj2;
    	var productsResponse = null;			
    	var data  = null;
    	if(InsightSearch.purchasePopUpPermission()){
        	InsightCommon.getServiceResponseAsync(url,jsonRequestObj2,"POST",function(ResponseObj){ 	
        		productsResponse = ResponseObj;
        		if(data!=null && productsResponse!=null){						
	           	  	 if(fromPage=='PPP'){               		 
	           	  		if(InsightSearch.purchasePopUpPermission()){
	               	    	getAddtoCartItemsSimpleDialogInfo(data,productsResponse,productsTotalQuantity,productInfoReq); 
	           	  		}
	           	  	 }else if(InsightSearch.purchasePopUpPermission()){
	           	    	getAddtoCartItemsSimpleDialogInfo(data,productsResponse,productsTotalQuantity,productInfoReq);   
	           	     } 
		     	}
        	});
		}
    	if(fromPage=='PPC' || fromPage=='bundle' ||fromPage=='PPP' || fromPage=='NOPOP'){
			if(InsightSearch.purchasePopUpPermission()){
		    	$("#addToCartDialog").dialog({ width: 600, resizable: false, modal:true,position:['top',150], overlay: { background: "#F2F2F2" }});
		    	insightLightBox.styleCustomDialog("#addToCartDialog");
		    }
			var jsonObj=getAddToCartPPCDialog(arrPC[0],productsResponse,quantity,fromPage,bundleType,bundleConfig,bundleName);
			var url = bundleCartURL;
		    InsightCommon.getServiceResponseAsync(url,jsonObj,"POST",function(dataObj){					
				data = dataObj;
				if(InsightB2BUserAttributes.isB2BUser==true){
        	  		document.location.href="http://www.insight.com/insightweb/viewCart";
        	  	}else if((fromPage=='PPP' && !InsightSearch.purchasePopUpPermission()) || fromPage=='NOPOP'){ 
        	  		document.location.href="http://www.insight.com/insightweb/viewCart";
        	  	}else if(!InsightSearch.purchasePopUpPermission()) {
        	  		document.location.href="http://www.insight.com/insightweb/viewCart";
          	  	} else         	   
				if(data!=null && productsResponse!=null){
                	 getAddtoCartItemsSimpleDialogInfo(data,productsResponse,productsTotalQuantity,productInfoReq);       	  		               	  		
                }
			});
    	}
	}else if (arrPC.length>1){
		   var jsonRequestObj1='';
		    if(fromPage=='bundle'){
		    	 jsonRequestObj1 = '{"csProductFlag":'+true+',"showSuggestedSearch":false,"locale":"en_US","searchOpenMarket":false,"showApprovedItemOnlyLink":false,"showProductCompare":false,"contractId":"'+contractId+'","showProductCenterLink":false,"catalogData":{"customCatalogId":null,"overrideCatalogId":null,"approvedItemsCatalogId":null,"purchasableCatalogSet":null,"filterManufacturerCatalogId":null},"fromClp":false,"salesOffice":"2001","salesOrg":"2400","defaultPlant":"10","onlyOpenMarket":false,"pageSize":0,"pageNumber":0,"fieldList":[],"sort":"BestSellers","rebateSearch":false,"defaultSort":true,"useBreadcrumb":false,"materialIds":['+materialIds+'],"secure":false,"displayOpenMarket":false}';	
		    } else{
			 jsonRequestObj1 = '{"showSuggestedSearch":false,"locale":"en_US","searchOpenMarket":false,"showApprovedItemOnlyLink":false,"showProductCompare":false,"contractId":"'+contractId+'","showProductCenterLink":false,"catalogData":{"customCatalogId":null,"overrideCatalogId":null,"approvedItemsCatalogId":null,"purchasableCatalogSet":null,"filterManufacturerCatalogId":null},"fromClp":false,"salesOffice":"2001","salesOrg":"2400","defaultPlant":"10","onlyOpenMarket":false,"pageSize":0,"pageNumber":0,"fieldList":[],"sort":"BestSellers","rebateSearch":false,"defaultSort":true,"useBreadcrumb":false,"materialIds":['+materialIds+'],"secure":false,"displayOpenMarket":false}';
		    }
			var url="/insightweb/compareProducts";
			var productsResponse = null;
        	var data  = null;
			if(InsightSearch.purchasePopUpPermission()){
	        	InsightCommon.getServiceResponseAsync(url,jsonRequestObj1,"POST",function(ResponseObj){ 	
	        		productsResponse = ResponseObj;
	        		if(data!=null && productsResponse!=null){						
	               	  	 if(fromPage=='PPP'){
	               	  		 getAddtoCartItemsSimpleDialogInfo(data,productsResponse,productsTotalQuantity);               	    
	               	  	 }
	        		}
	        	});
			}
			if(fromPage=='PPC' || fromPage=='bundle'||fromPage=='PPP' ){		
			      if(InsightSearch.purchasePopUpPermission()){
				    	$("#addToCartDialog").dialog({ width: 600, resizable: false, modal:true,position:['top',150], overlay: { background: "#F2F2F2" }});
				    	insightLightBox.styleCustomDialog("#addToCartDialog");
			      }
				  var jsonObj=getAddToCartPPCDialog(materialIds,productsResponse,quantity,fromPage,bundleType,bundleConfig,bundleName);
				  var url = bundleCartURL;
			      InsightCommon.getServiceResponseAsync(url,jsonObj,"POST",function(dataObj){					
					data = dataObj;
					if(InsightB2BUserAttributes.isB2BUser==true){
            	  		document.location.href="http://www.insight.com/insightweb/viewCart";
            	  	}else if((fromPage=='PPP' && !InsightSearch.purchasePopUpPermission()) || fromPage=='NOPOP'){ 
        	  			document.location.href="http://www.insight.com/insightweb/viewCart";            	  	
              	  	}else if(!InsightSearch.purchasePopUpPermission()) {
              	  	 document.location.href="http://www.insight.com/insightweb/viewCart";
              	  	}else         	   
					if(data!=null && productsResponse!=null){						               	  	
						getAddtoCartItemsSimpleDialogInfo(data,productsResponse,productsTotalQuantity);                	    
			     	}
				});
			}
	}
}

function delquote(str){return (str=str.replace(/["']{1}/gi,""));}

function AddToCartPPCDialog(selectedMeterialId,jsonResponse,quantity,fromPage,bundleType,bundleConfig,bundleName){
	if(InsightB2BUserAttributes.fromCompanyStandards==true && InsightB2BUserAttributes.isB2BUser==true){

	}else{  
		if(InsightSearch.purchasePopUpPermission()){
	    	$("#addToCartDialog").dialog({ width: 600, resizable: false, modal:true, position:['top',150], overlay: { background: "#F2F2F2" }});
	    	insightLightBox.styleCustomDialog("#addToCartDialog");
	    }
	}
  getAddToCartPPCDialog(selectedMeterialId,jsonResponse,quantity,fromPage,bundleType,bundleConfig,bundleName);
}

function getAddToCartPPCDialog(selectedMeterialIds,jsonResponse,quantity,fromPage,bundleType,bundleConfig,bundleName){
		 $("#addToCartDialog").html($('#loading').html());             
		 var loc = document.location.href;
		 var previousSrearchURL = '';
		 loc = loc.substring(loc.indexOf('insightweb'),loc.length);
		 if(loc.indexOf('searchResults')!=-1){
			 previousSrearchURL =  '/'+loc;
		 }
         var jsonObj = '';
         var url = "/insightweb/transaction/addtocart";
         var qty=1;
         var productQuantity = 0;
         var arrPC = [];
         var arrPC = selectedMeterialIds.split(',');
         var arrQty =[];
         var arrBundle =[];
         var quickCartCSQuantity = 1;
         var bundleNameArray =[];            
         var mtrbundleIds = null;
         var mtrbundleIdtype1 = '';
         var mtrbundleIdtype2 = '';  
         var mtrbundleIdtype3 ='';
         var mtrPPCIds = null;
         var contractID ='' ; 
         var configured ='';
         var isConfigured = false;
         var isConfig = false;
         var type1 =[];
    	 var type2 =[];
    	 var type3 =[];
    	 var imageCartBundleContractId = null;
         if(bundleConfig!=null)
        	 arrBundle =  bundleConfig.split(',');
         if(quantity!=null && (bundleType==null || (bundleType!=null && bundleType[1] != "cartImage")))
           arrQty = quantity.split(',');
            if(bundleName!=null){
	        	 bundleFlag = true;
	        	 bundleNameArray =  bundleName.split(','); 
            	 if(bundleNameArray!=null && bundleNameArray.length==1){
            		 bundleNameArray[0]= bundleName;
            		}
             }
            if(bundleType!=null && bundleType[0]!=null){      
               if(bundleType[1] == "button"){
            	   $("input.typeOne").each(function() {
	            	   var mtrId = '';
	            	   var qty ='';
	        		   if($(this).is(':checked')){
		        			 mtrId = $(this).val();
		        			 qty = $('#changeQty_'+this.id).val();
		        			 if(qty===''){
		        				 qty=1;
		        			 }     
		        			 isConfigured = $.trim($('input[name="configured1_'+mtrId+'"]').val());               			 
		        			 if(isConfigured=='undefined' || isConfigured ==''){
		        				 isConfigured=false;
		        			 }
		        			 var obj = {"materialId":mtrId,"qty":qty,"configured":isConfigured};
		        			 type1.push(obj);
	        		   }
            	   });
	        	 $("input.typeTwo").each(function() {
	        		 var mtrId = '';
	        		 var qty ='';
	        		 if($(this).is(':checked')){
	        			 mtrId = $(this).val();
	        			 qty = $('#changeQty_'+this.id).val();
	        			 if(qty===''){
	        				 qty=1;
	        			 }
	        			 isConfigured = $.trim($('input[name="configured2_'+mtrId+'"]').val());               			 
	        			 if(isConfigured=='undefined' || isConfigured ==''){
	        				 isConfigured=false;
	        			 }
	        			 var obj = {"materialId":mtrId,"qty":qty,"configured":isConfigured};
	        			 type2.push(obj);
	        		  }
	        	 });
	        	 $("input.typeThree").each(function() {
	        		 var mtrId = '';
	        		 var qty ='';
	        		 if($(this).is(':checked')){
	        			 mtrId = $(this).val();
	        			 qty = $('#changeQty_'+this.id).val();
	        			 if(qty===''){
	        				 qty=1;
	        			 }            
	        			 isConfigured = $.trim($('input[name="configured3_'+mtrId+'"]').val());               			 
	        			 if(isConfigured=='undefined' || isConfigured ==''){
	        				 isConfigured=false;
	        			 }
	        			 var obj = {"materialId":mtrId,"qty":qty,"configured":isConfigured};
	        			 type3.push(obj);
	        		  }
	        	 });
               }else if(bundleType[1] == "cartImage"){
            	   $.each(groupresponseData, function(index, value){
        					var k =value.psitem; 
        					if(k!=null){
              	   				for(var i=0; i<k.length;i++){
            	   					if(value.psitem[i].selected && value.psitem[i].product!=null){
	           	                   		 var mtrId = value.psitem[i].materialId;
	        	                		 var qty = value.psitem[i].quantity;
	        	                		 var configured = value.psitem[i].configured;
	        	                		 if(quantity!=null && quantity!=''){
	        	                			 quickCartCSQuantity = parseInt(quantity); 
	        	                		 }
	        	            			 if(qty===''){
	        	            				 qty=1;
	        	            			 }
	        	            			 if(configured=='undefined' || configured ==''){
	        	            				 configured=false;
	        	            			 }
	        	            			 var obj = {"materialId":mtrId,"qty":qty,"configured":configured};
	        	            			 if(isNumeric(qty)){
	            			   				if(value.setType == 1)
	            			   					type1.push(obj);
	            			   				else if(value.setType == 2)
	            	            				type2.push(obj);
	            	            			else if(value.setType == 3)
	            	            				type3.push(obj);
	            			   			}
            	   					}
              	   				}
          	   				}
        				
            	   });
        		imageCartBundleContractId = $('#'+groupresponseData.groupId).val();
        	  }
        	 url = "/insightweb/transaction/addtocartbundle";
        	 var loc = document.location.href;
			 var previousSearchURL = '';
			 loc = loc.substring(loc.indexOf('insightweb'),loc.length);    			
			 previousSearchURL =  '/'+loc; 
			 var contractid = $('#csContractId').val();
        	 if(imageCartBundleContractId != null){
        		 contractid =imageCartBundleContractId;
        	 }
        	 if(contractid!=null && contractid!=''){
        		 contractid='"'+contractid+'"';
        	 } else {
        		 contractid =null;
        	 }
        	 if(type1!=null && type1.length>0){
        		 $.each(type1, function(index, value) {
        			 mtrbundleIdtype1 = mtrbundleIdtype1 + '{"previousSearchURL":"'+previousSearchURL+'","materialID" :"'+value.materialId+'","configured":'+value.configured+',"quantity" :'+value.qty+'}'; 
        			 mtrbundleIdtype1 = mtrbundleIdtype1+',';
        		 });            		 
        		 mtrbundleIdtype1 = mtrbundleIdtype1.substring(0,mtrbundleIdtype1.length-1);
        	 }
        	 if(type2!=null && type2.length>0){
        		 $.each(type2, function(index, value) {             			 
        			 mtrbundleIdtype2 =mtrbundleIdtype2 + '{"previousSearchURL":"'+previousSearchURL+'","materialID" :"'+value.materialId+'","configured":'+value.configured+',"quantity" :'+value.qty+'}'; 
        			 mtrbundleIdtype2 = mtrbundleIdtype2+',';	
        		 });            		 
        		 mtrbundleIdtype2 = mtrbundleIdtype2.substring(0,mtrbundleIdtype2.length-1);
        	 }
        	 if(type3!=null && type3.length>0){            		 
        		 $.each(type3, function(index, value) {            			 
        			 if(bundleType[0]=='COMPANY_STANDARD'){
          				productQuantity = parseInt(productQuantity)+parseInt(value.qty);
          			   }
        			 mtrbundleIdtype3 = mtrbundleIdtype3+'{"previousSearchURL":"'+previousSearchURL+'","materialID" :"'+value.materialId+'","configured":'+value.configured+',"quantity" :'+value.qty+'}'; 
        			 mtrbundleIdtype3 = mtrbundleIdtype3+',';	
        		 });            		
        		 mtrbundleIdtype3 = mtrbundleIdtype3.substring(0,mtrbundleIdtype3.length-1);            		 
        	 }
        	 if(bundleType[0]!='COMPANY_STANDARD'){
        		 productQuantity = parseInt(productQuantity)+parseInt(1);
        		 }
        	 
        	 if(mtrbundleIdtype1!='' && mtrbundleIdtype2!=''){
        		 mtrbundleIds =  mtrbundleIdtype1+','+mtrbundleIdtype2;
        	 } else if(mtrbundleIdtype1!=''){
        		 mtrbundleIds = mtrbundleIdtype1;
        	 } else if(mtrbundleIdtype2!=''){
        		 mtrbundleIds = mtrbundleIdtype2;
        	 }
        	 if(mtrbundleIdtype3!=null && mtrbundleIdtype3!=''){
        		 mtrPPCIds = mtrbundleIdtype3;
        	 }
        	 if(bundleType[0]!=null && bundleType[0]=='COMPANY_STANDARD'){
        		 if(mtrPPCIds!=null && mtrbundleIds!=null){
        			 if(contractid!=null){
        				 jsonObj = '{"clientBrowserDate":"'+sDate+'","productsQuantity":'+quickCartCSQuantity+',"addOnsQuantity":'+quickCartCSQuantity+',"bundleType":"'+bundleType[0]+'","name":"'+bundleNameArray[0]+'","contractID":'+contractid+',"products" :['+mtrbundleIds+'],"addOns" :['+mtrPPCIds+']}';	 
        			 }else
        				 jsonObj = '{"clientBrowserDate":"'+sDate+'","productsQuantity":'+quickCartCSQuantity+',"addOnsQuantity":'+quickCartCSQuantity+',"bundleType":"'+bundleType[0]+'","name":"'+bundleNameArray[0]+'","products" :['+mtrbundleIds+'],"addOns" :['+mtrPPCIds+']}';
        		 }else if (mtrPPCIds!=null){
        			 if(contractid){
        				 jsonObj = '{"clientBrowserDate":"'+sDate+'","addOnsQuantity":'+quickCartCSQuantity+',"contractID":'+contractid+',"addOns" :['+mtrPPCIds+']}'; 
        			 }else 
        				 jsonObj = '{"clientBrowserDate":"'+sDate+'","addOnsQuantity":'+quickCartCSQuantity+',"addOns" :['+mtrPPCIds+']}'; 
        		 }else if (mtrbundleIds!=null){
        			 if(contractid!=null){
        				 jsonObj = '{"clientBrowserDate":"'+sDate+'","productsQuantity":'+quickCartCSQuantity+',"bundleType":"'+bundleType[0]+'","name":"'+bundleNameArray[0]+'","contractID":'+contractid+',"products" :['+mtrbundleIds+']}'; 
        			 } else 
        				 jsonObj = '{"clientBrowserDate":"'+sDate+'","productsQuantity":'+quickCartCSQuantity+',"bundleType":"'+bundleType[0]+'","name":"'+bundleNameArray[0]+'","products" :['+mtrbundleIds+']}';
      			 }
        	}else{
    				 if(mtrPPCIds!=null && mtrbundleIds!=null){
    					 if(contractid!=null){
    						 jsonObj = '{"clientBrowserDate":"'+sDate+'","productsQuantity":'+quickCartCSQuantity+',"bundleType":"'+bundleType[0]+'","name":"'+bundleNameArray[0]+'","contractID":'+contractid+',"products" :['+mtrbundleIds+','+mtrPPCIds+']}';
    					 }
    					 else
    						 jsonObj = '{"clientBrowserDate":"'+sDate+'","productsQuantity":'+quickCartCSQuantity+',"bundleType":"'+bundleType[0]+'","name":"'+bundleNameArray[0]+'","products" :['+mtrbundleIds+','+mtrPPCIds+']}';
    				 }else if (mtrPPCIds!=null){
    					 if(contractid!=null){
    						 jsonObj = '{"clientBrowserDate":"'+sDate+'","productsQuantity":'+quickCartCSQuantity+',"bundleType":"'+bundleType[0]+'","name":"'+bundleNameArray[0]+'","contractID":'+contractid+',"products" :['+mtrPPCIds+']}';
    					 }else 
    						 jsonObj = '{"clientBrowserDate":"'+sDate+'","productsQuantity":'+quickCartCSQuantity+',"bundleType":"'+bundleType[0]+'","name":"'+bundleNameArray[0]+'","products" :['+mtrPPCIds+']}'; 				 
    			  
    				 }else if (mtrbundleIds!=null){
    					 if(contractid!=null){
    						 jsonObj = '{"clientBrowserDate":"'+sDate+'","productsQuantity":'+quickCartCSQuantity+',"bundleType":"'+bundleType[0]+'","name":"'+bundleNameArray[0]+'","contractID":'+contractid+',"products" :['+mtrbundleIds+']}';
    					 }
    					 else{
    						 jsonObj = '{"clientBrowserDate":"'+sDate+'","productsQuantity":'+quickCartCSQuantity+',"bundleType":"'+bundleType[0]+'","name":"'+bundleNameArray[0]+'","products" :['+mtrbundleIds+']}';
    					 }
    				 } 
    			 }  	
        	}else{
        		var contractId = null;            			 		
		 		if((Insight.selectedContractId!=null  && Insight.selectedContractId!='undefined' && Insight.selectedContractId.length>0)){
		 			contractId =Insight.selectedContractId;
		 		}			 		
				if(arrPC.length==1 ){ 
		     		var softwareMaterialId = arrPC[0].replaceAll('"','');
		     		var softwareContractId = $('#softwareContractId_'+softwareMaterialId).val();
					var programId = $('#programId_'+softwareMaterialId).val();
			        if(softwareContractId==undefined || !IsNumeric(softwareContractId) || programId==undefined ||  programId==null ){
			        	 softwareContractId = '';
			        	 programId = '';
			        }
		     		var contractMtrId = arrPC[0].replaceAll('"','').replaceAll("#", "\\#").replaceAll("/","\\/").replaceAll(".","\\."); 
	     		    contractMtrId= "#"+contractMtrId;
	       	        contractMtrId = $(contractMtrId).val();	
		     	    if( contractMtrId !=null && contractMtrId!=''){
		     		 contractId = contractMtrId;
		     	    }	
		     	    if($('input:radio[name=contractPrice]:checked').val() !=undefined ){
		     	    	contractId = $('input:radio[name=contractPrice]:checked').val();
		     	    }
		     		if(arrQty.length>0)
		     			qty = arrQty[0];
		     		productQuantity =  parseInt(productQuantity)+parseInt(qty);
		     		if(contractId!=null){
		     			jsonObj ='[{"clientBrowserDate":"'+sDate+'","selectedSoftwareContractId":"","programId":"'+programId+'","contractID":"'+contractId+'","previousSrearchURL":"'+previousSrearchURL+'","materialID" : '+arrPC[0]+',"quantity" :'+qty+'}]';	
		     		}else
		     			jsonObj = '[{"clientBrowserDate":"'+sDate+'","selectedSoftwareContractId":"'+softwareContractId+'","programId":"'+programId+'","previousSrearchURL":"'+previousSrearchURL+'","materialID" : '+arrPC[0]+',"quantity" :'+qty+'}]';
		     	}else if(arrPC.length>1){
		     		jsonObj = '[';
		     		var mtrPPCIds = '';
		     		for(var i=0;i<arrPC.length;i++){ 
		     			var contractMtrId = arrPC[i].replaceAll('"','').replaceAll("#", "\\#").replaceAll("/","\\/").replaceAll(".","\\."); 
		    		    contractMtrId= "#"+contractMtrId;
		      	        contractMtrId = $(contractMtrId).val();	
		    	        if(contractMtrId !=null && contractMtrId!=''){
		    		         contractId = contractMtrId;
		    	        }
		     			if(arrQty.length>0)
		         			qty = arrQty[i];
		     			productQuantity =  parseInt(productQuantity)+parseInt(qty);
		     			if(contractId!=null){
		     				mtrPPCIds = mtrPPCIds+','+'{"clientBrowserDate":"'+sDate+'","contractID":"'+contractId+'","previousSrearchURL":"'+previousSrearchURL+'","materialID" : '+arrPC[i]+',"quantity" :'+qty+'}';
		     			}else{
		     				mtrPPCIds = mtrPPCIds+','+'{"clientBrowserDate":"'+sDate+'","previousSrearchURL":"'+previousSrearchURL+'","materialID" : '+arrPC[i]+',"quantity" :'+qty+'}';
		     			}
		     		}
		     		mtrPPCIds = mtrPPCIds.substring(1);
		     		jsonObj = jsonObj+mtrPPCIds+']';
		         }
		    }
         bundleCartURL = url;
         productsTotalQuantity = productQuantity;
         return jsonObj;
}


function getAddtoCartItemsDialogInfo(data,jsonResponseObj,productQuantity){
    var markup = data;
	if(data.cart!=null){
		markup=data.cart;
		parentMaterialIdKey=data.materialIdKeyList[0];
	}
	var itemCount = productQuantity;
    var cartItemsProductInfo = null;
    var singleCartProductInfo =null;
	if(jsonResponseObj!=null && jsonResponseObj.products!=null && jsonResponseObj.products.length>0 ){
		cartItemsProductInfo = jsonResponseObj.products;
	}
	if(jsonResponseObj!=null && jsonResponseObj.webProduct!=null)
		singleCartProductInfo = jsonResponseObj;
	var addToCartDialogData = null; 
	/* get csid for all ics products added to cart*/
	var icsObj = null;
	if(InsightSearch.icsMap!=null){
		icsObj = InsightSearch.icsMap;
	}
	var icsMatId = new Array();
	var csid=null;
	var trackingExists = false;
	if(icsObj!=null){
		if(cartItemsProductInfo!=null && cartItemsProductInfo!=""){
			$.each(cartItemsProductInfo,function(index,value){
				if(value.materialId in icsObj){
					icsMatId.push(value.materialId);
				}
			});
		}else if(singleCartProductInfo!=null && singleCartProductInfo!=""){
			if(singleCartProductInfo.webProduct.materialId in icsObj){
				icsMatId.push(singleCartProductInfo.webProduct.materialId);
			}
		}
	}
	if(icsMatId!=null){ 
		if(icsMatId.length > 0){
			csid = "";
			trackingExists = true;
			for(var i=0;i<icsMatId.length;i++){
				var matId = icsMatId[i];
				if(i == (icsMatId.length-1)){
					csid = csid+icsObj[matId];
				}else{
					csid = csid+icsObj[matId]+",";
				}
			}
		}
	}
	markup = $.extend(markup,{"cartItemsProductInfo":cartItemsProductInfo},{"singleCartProductInfo":singleCartProductInfo},{"selectedIPPwarranty":selectedIPPwarranty},{"itemCount":itemCount},{"bundleGrpName":bundleGrpName},{"bundleFlag":bundleFlag},{"trackingExists":trackingExists},{"trackingId":csid},{"trackingUrl":data.icsCartUrl});
    markup = $.extend(markup, {'labels':searchLabels.labels});
    if($.template["addItemsToCartDialogData"] == undefined) {
		 InsightCommon.getContentAsync(InsightSearch.staticContentUrl,null,"http://www.insight.com/insightweb/assets/en_US/www/javascript/search/addItemsToCartTempl.html", function(data) {
			 renderAddItemsToCartDialog(data,markup);
		 });
	}else{
		  addToCartDialogData = $.template["addItemsToCartDialogData"];
		  renderAddItemsToCartDialog(addToCartDialogData,markup);
	}
}

function getAddtoCartItemsSimpleDialogInfo(data,jsonResponseObj,productQuantity,accessoriesReq){ 
	var markup = data;
	if(data.cart!=null){
		markup=data.cart;
		parentMaterialIdKey=data.materialIdKeyList[0]; /* in case of from view cart this value gets populated from transshoppingCart.js from loadIppWarrenties */
	}
	InsightSearch.parentProductQuantity = productQuantity;
	var itemCount = productQuantity;
    var cartItemsProductInfo = null;
    var singleCartProductInfo =null;
    var warrantyTabText = null;
    var prodLabels = InsightSearch.loadSearchLabels();
    InsightSearch.cartFromAddToCart = markup;
	if(jsonResponseObj!=null && jsonResponseObj.products!=null && jsonResponseObj.products.length>0 ){
		cartItemsProductInfo = jsonResponseObj.products;
	}
	if(jsonResponseObj!=null && jsonResponseObj.webProduct!=null  ){
		/* truncate IPP warranty description */
		if(jsonResponseObj.warrantyPdcts!=null){
			$.each(jsonResponseObj.warrantyPdcts, function(index,data){
				var description = data.description.split(" ",5);
				data.description = description.toString().replaceAll(',',' ');
			});
		}
		singleCartProductInfo = jsonResponseObj;
		/* Tech specs in warranty tab */
		var productComesWithLabel = prodLabels.labels.thisProductComesWith;
		if(jsonResponseObj.webProduct.extendedSpecsMap!=null){
			$.each(jsonResponseObj.webProduct.extendedSpecsMap, function(colKey,extendedSpec){
				if(colKey == 'Manufacturer Warranty'){ 
					$.each(extendedSpec.details, function(rowKey,detail){
						if(detail.label == 'Service & Support Details'){
							warrantyTabText = productComesWithLabel+" "+detail.value;
							return false;
						}else if (detail.label == 'Service & Support'){
							warrantyTabText = productComesWithLabel+" "+detail.value;
						}
					});
				}
			});
		}
	}
	var addToCartSimpleDialogData = null; 
	var extendedData = { 
			getPrice: function(webProduct,source){
								return getPriceFromWebProduct(webProduct,source,prodLabels.labels);
							}		
	};
	/* check if cart already contains warranty for the item added to cart*/
	var ippWarrantyInCart = null;
	var oemWarrantyInCart = null;
	var oemWarranties = new Array();
	if(singleCartProductInfo!=null && singleCartProductInfo.oemWarranties!=null){
		$.each(singleCartProductInfo.oemWarranties,function(index,warranty){
			oemWarranties.push(warranty.materialId);
		});
		for(var i=0;i<oemWarranties.length;i++){
			InsightSearch.oemWarrantiesInPopup.push(oemWarranties[i]);
		}
	}else{
		InsightSearch.oemWarrantiesInPopup.length=0;
	}
	/* identify oem warranty in cart */
	if(oemWarranties!=undefined){
		if(InsightSearch.warrantyAddedFromProductInfo!=null){
			oemWarrantyInCart = InsightSearch.warrantyAddedFromProductInfo;
			InsightSearch.warrantyAddedFromProductInfo = null;
		}
	}
	
	/* identify ipp warranty in cart */
	if(singleCartProductInfo!=null && markup.contracts!=null){
		$.each(markup.contracts,function(index1,contract){
			if(contract.cartItems!=null){
				$.each(contract.cartItems,function(index2,lineItem){
					/*this condition will make sure we are looking for proper line item*/
					if(parentMaterialIdKey!=null && parentMaterialIdKey!=undefined){
						if(parentMaterialIdKey == lineItem.materialIDKey){
							if(singleCartProductInfo.webProduct.materialId == lineItem.materialID){
								if(lineItem.selectedwarrantyDetails!=null){
									ippWarrantyInCart = lineItem.selectedwarrantyDetails.ippMaterialId;
								}
								if(lineItem.selectedProductWarranty!=null){
									oemWarrantyInCart = lineItem.selectedProductWarranty.warrMaterialId;
								}
							}
						}
					}	
				});
			}
		});
	}
	
	/* reset added warranties */
	InsightSearch.oemAddedToCartFromPopup.length = 0;
	InsightSearch.ippAddedToCartFromPopup.length = 0;
	var existingWarrInCart = "";
	if(ippWarrantyInCart!=null){
		existingWarrInCart = ippWarrantyInCart;
	}else if(oemWarrantyInCart!= null){
		existingWarrInCart = oemWarrantyInCart;
	}else{
		existingWarrInCart = null;
	} 
	markup = $.extend(markup,{"cartItemsProductInfo":cartItemsProductInfo},{"singleCartProductInfo":singleCartProductInfo},{"ippWarrantyInCart":ippWarrantyInCart},{"oemWarrantyInCart":oemWarrantyInCart},{"itemCount":itemCount},{"warrantyTabText":warrantyTabText},{"existingWarrInCart":existingWarrInCart},{"bundleGrpName":bundleGrpName},{"bundleFlag":bundleFlag});
    markup = $.extend(markup, {'labels':searchLabels.labels});
    markup = $.extend(markup,extendedData);
    if($.template["addToCartSimpleDialogData"] == undefined) {
		 InsightCommon.getContentAsync(InsightSearch.staticContentUrl,null,"http://www.insight.com/insightweb/assets/en_US/www/javascript/search/addItemsToCartSimpleTemplate.html", function(data) {
			if(accessoriesReq!=null){
				renderAddItemsToCartSimpleDialog(data,markup,accessoriesReq);
				if(!(jsonResponseObj.warrantyPdcts || jsonResponseObj.oemWarranties)){
					if(jsonResponseObj.hasAccessories){
					  // this mean we have no warrenties but we have accessories , so we need to populate accessories by default
						$(".accessoriesOnClick").click();// this click is defined in renderAddItemsToCartSimpleDialog function.
						$(".accessoriesPopUpTab").addClass("ui-tabs-selected ui-state-active"); // to make the tab active 
					}	
				}
			}else{
				renderAddItemsToCartSimpleDialog(data,markup);
			}
		 });
	  }else {
		  addToCartSimpleDialogData = $.template["addToCartSimpleDialogData"];
		  if(accessoriesReq!=null){
			  renderAddItemsToCartSimpleDialog(addToCartSimpleDialogData,markup,accessoriesReq);
			}else{
				 renderAddItemsToCartSimpleDialog(addToCartSimpleDialogData,markup);
			}
	  }
}

function renderAddItemsToCartSimpleDialog(addToCartSimpleDialogData,markup,accessoriesReq){
   $.template("addToCartSimpleDialogData",addToCartSimpleDialogData);
   $("#addToCartDialog").html("");
   InsightCommon.renderTemplate( "addToCartSimpleDialogData",markup, "#addToCartDialog");
   $("#mainHeaderCartTotalPrice").html(" - "+markup.currency +" "+InsightCommon.formatCurrency(markup.totalCost));
   Insight.cartTotalPrice=markup.totalCost;
   /* remove Your Price: label from oem price for logged in user */
   if(markup.singleCartProductInfo!=null && markup.singleCartProductInfo.oemWarranties!=null){
	   $.each(markup.singleCartProductInfo.oemWarranties, function(index,data){
			  var oemPrice = $("#oem_price_display_"+data.materialId+"").text();
			  if(oemPrice.indexOf(':')!= -1){
				  var oemDisplayPrice = oemPrice.substr(13);
				  $("#oem_price_display_"+data.materialId+"").text(oemDisplayPrice);
			  }else{
				  $(".oem_price .yourPrice").removeClass('yourPrice');
				  $(".oem_price .currency").removeClass('currency');
			  }
		  });
	   //remove oem contract description for IPS 
	   $(".oem_price div").hide();
   }
   if(markup.singleCartProductInfo!=null && markup.singleCartProductInfo.warrantyPdcts!=null){
	   $(".ipp_price_display .yourPrice").removeClass('yourPrice');
	   $(".ipp_price_display .currency").removeClass('currency');
	   //remove ipp contract description for IPS
	   $(".ippWarrantyDetails .ipp_price div").hide();
   }
  
  /* pre-select warranty present in cart */
   if(markup.ippWarrantyInCart!=null){
	  $("#warrantyRadio_"+markup.ippWarrantyInCart+"").attr("checked",true);
	  $("#warrantyButtons .itemSelectedBtn,#warrantyButtons .removeWarrBtn").css("display","inline");
	  $("#warrantyButtons .addSelToItemBtn").css("display","none");
	  InsightSearch.ippAddedToCartFromPopup.push(markup.ippWarrantyInCart);
   }
   if(markup.oemWarrantyInCart!=null){
	  $("#warrantyRadio_"+markup.oemWarrantyInCart+"").attr("checked",true);
	  $("#warrantyButtons .itemSelectedBtn, #warrantyButtons .removeWarrBtn").css("display","inline");
	  $("#warrantyButtons .addSelToItemBtn").css("display","none");
	  InsightSearch.oemAddedToCartFromPopup.push(markup.oemWarrantyInCart);
   }
   
   /*functionality for accessories tab on click */
	if(markup.singleCartProductInfo!=null && markup.singleCartProductInfo.topMostRecProducts && markup.singleCartProductInfo.topMostRecProducts.length>0){
	   $(".accessoriesOnClick").click(function() {
		   if($(".accessoriesContainer").length == 0){
			   $("#accessoriesContent").empty().html($('#loading').html()); // to show loading wheel while loading data  
			   InsightCommon.getContentAsync("/insightweb/getStaticContent",null,"http://www.insight.com/insightweb/assets/en_US/www/javascript/search/accessoriesInAddToCartPopup.html",function(accessoriesInPopupTmplRes){
				   $.template( "accessoriesInPopupTmpl", accessoriesInPopupTmplRes);
				   $("#accessoriesContent").empty();
				   var ResponseObjRes = markup.singleCartProductInfo;
				   ResponseObjRes= $.extend(ResponseObjRes, {'labels':markup.labels},{getPrice: function(webProduct,source){			    			
		    			return getPriceFromWebProduct(webProduct,source,markup.labels);
		   	 		}});
				   InsightCommon.renderTemplate("accessoriesInPopupTmpl", ResponseObjRes, "#accessoriesContent");
				   adjustHeights(".accessoryProductID",".accessoriesDesc");
				   $(".viewAllAccessoriesLink").css("display","inline");
			   });	   
		   }else{
			   $(".viewAllAccessoriesLink").css("display","inline");
		   }
	   });
	   $(".servicesOnClick,.warrantyOnClick ").click(function() {
		   $(".viewAllAccessoriesLink").css("display","none");
	   });
   }
   
   /* highlight accessories tab if warranty is already added*/
   if(markup.ippWarrantyInCart!=null || markup.oemWarrantyInCart!=null){
	   if(markup.singleCartProductInfo!=null){
		   if(markup.singleCartProductInfo.topMostRecProducts){
			   $(".warrantiesPopUpTab").removeClass("ui-tabs-selected ui-state-active"); 
			   $(".accessoriesOnClick").click();
			   $(".accessoriesPopUpTab").addClass("ui-tabs-selected ui-state-active");  
		   }
		   else{
			   $(".warrantiesPopUpTab").addClass("ui-tabs-selected ui-state-active"); 
			   $(".warrantiesPopUpTab").click();
		   }
	   }
   }
   $(".warrantyOnClick").click(function() {
	   adjustScrollingHeight(".icsWarrScrollItem","#icsWarrScrollContainer .touchcarousel");
   });
	 adjustScrollingHeight(".icsWarrScrollItem","#icsWarrScrollContainer .touchcarousel");
}

function adjustScrollingHeight(item, itemContainer){
	/* to adjust height of warranties and accessories in pop-up*/
	   /*adding code to adjust height of scroll widget dynamically*/
	   var DivHeights=[];
	   $(item).each(function () {
			 DivHeights.push($(this).height());
	   });
	   if(Math.max.apply(Math, DivHeights)!=0){
			$(itemContainer).height(Math.max.apply(Math, DivHeights)+35); 
		}	 
		/*END adding code to adjust height of scroll widget dynamically*/
}
/*
 * Adjust heights of all descriptions in recently viewed and recommanded
 */
function adjustHeights(parentDiv, childDiv){
       var childDivHeights=[];
       $(parentDiv).find(childDiv).each(function () {
                childDivHeights.push($(this).height());
       });
       var childMaxDiv= Math.max.apply(Math, childDivHeights);
       if(childMaxDiv!=0){
	       $(parentDiv).find(childDiv).each(function () {
	             $(this).height(childMaxDiv);
	       });
       }
}

function addAccessoryProduct(materialID,ContractID){
	cmCreateConversionEventTag("Accessory Added","1",materialID,"5");
	if($('#accessoriesContent a[name="'+materialID+'_AddToCartAnchor"]').attr("clickFlag")=="true"){
		/*the if condition is created to disable onclick functionality based on clickFlag 
		 * we are implementing this instead of removing onclick and adding it back. reason is the add to 
		 * cart button needs ontract id and which is not same all the time if user is IPS
		 * */
		 $('#accessoriesContent [name="'+materialID+'_AddToCartAnchor"]').css("display","none");
		 $('#accessoriesContent span[name="'+materialID+'_LoadingImg"]').css("display","block");
		 var jsonObj = '[{"clientBrowserDate":"'+sDate+'","quantity" :1,"contractID":"'+ContractID+'","materialID":"'+materialID+'"}]';
		 InsightCommon.getServiceResponse("/insightweb/transaction/addtocart",jsonObj, "POST","false",function(addCartResponse){
			 if(addCartResponse.InsightError!=null){
				 $('#accessoriesContent span[name="'+materialID+'_LoadingImg"]').css("display","none");
				 $('#accessoriesContent a[name="'+materialID+'_AddToCartAnchor"]').removeClass('ltgrey').addClass('orange').css("display","block");
				 $('#accessoriesContent span[name="'+materialID+'_AddToCartButton"]').css("display","block");
				 $('#accessoriesContent a[name="'+materialID+'_RemoveCartAnchor"]').css("display","none");
			 }else{
				  $('#accessoriesContent span[name="'+materialID+'_LoadingImg"]').css("display","none");
				  $('#accessoriesContent a[name="'+materialID+'_AddToCartAnchor"]').css("display","none").attr("clickFlag","false");
				  $('#accessoriesContent span[name="'+materialID+'_AddToCartButton"]').css("display","none");
				  $('#accessoriesContent a[name="'+materialID+'_RemoveCartAnchor"]').css("display","block");
				  if(addCartResponse.cart!= null){					  
					  $(".addToCartPopupSubTotalID").html('<span class="addToCartPopupSubTotalID">'+InsightCommon.formatCurrency(addCartResponse.cart.subTotal)+'</span>');
					  $("#mainHeaderCartTotalPrice").html(" - "+addCartResponse.cart.currency +" "+InsightCommon.formatCurrency(addCartResponse.cart.totalCost));
				  }	
			  }
		 });  
	}else if($('#accessoriesContent a[name="'+materialID+'_AddToCartAnchor"]').attr("clickFlag")=="false"){
		/* need to determine what to do -- for now do nothing */
	}
}


function removeAccessoryProduct(materialID,contractId){
	if($('#accessoriesContent a[name="'+materialID+'_RemoveCartAnchor"]').attr("clickFlag")=="true"){
		 $('#accessoriesContent a[name="'+materialID+'_RemoveCartAnchor"]').css("display","none");
		 $('#accessoriesContent span[name="'+materialID+'_LoadingImg"]').css("display","block");
		 if(contractId == undefined){
			contractId = "";
		 }
		 InsightCommon.getServiceResponseAsync("/insightweb/transaction/getcart","","GET",function(getCartResponse){
			var materialIDKey = null;
			if(getCartResponse!=null && getCartResponse.contracts!=null){				
				$.each(getCartResponse.contracts,function(index,contract){
					if(index == contractId){
						if(contract.cartItems!=null){
							$.each(contract.cartItems,function(itemIndex,lineItem){
								if(materialID == lineItem.materialID){
									materialIDKey = lineItem.materialIDKey;
								}
							});
						}	
					}
				});
				if(materialIDKey!=null){
					var deleteAccessoryReq = '{"materialID" : "'+materialIDKey+'", "contractID" : "'+contractId+'"}';
					InsightCommon.getServiceResponse("/insightweb/transaction/deletefromcart",deleteAccessoryReq, "POST","false",function(deleteCartResponse){
						if(deleteCartResponse!=null){
							 InsightSearch.cartFromAddToCart = deleteCartResponse;
							 if(deleteCartResponse.totalCost!=null || deleteCartResponse.totalCost==0){
								 $("#cartSubTotal").html('<span class="addToCartPopupSubTotalID">'+InsightCommon.formatCurrency(deleteCartResponse.subTotal)+'</span>');
								 $("#mainHeaderCartTotalPrice").html(" - "+deleteCartResponse.currency +" "+InsightCommon.formatCurrency(deleteCartResponse.totalCost));
								 $('#accessoriesContent span[name="'+materialID+'_LoadingImg"]').css("display","none");
								 $('#accessoriesContent a[name="'+materialID+'_AddToCartAnchor"]').removeClass('ltgrey').addClass('orange').css("display","block").attr("clickFlag","true");
								 $('#accessoriesContent span[name="'+materialID+'_AddToCartButton"]').css("display","block");
								 $('#accessoriesContent a[name="'+materialID+'_RemoveCartAnchor"]').css("display","none");
								 
							 }
						 }
					});
				}
			}
		});
	}
}

function renderAddItemsToCartDialog(addToCartDialogData,markup){
      $.template( "addItemsToCartDialogData",addToCartDialogData);
	  $("#addToCartDialog").html("");
      InsightCommon.renderTemplate( "addItemsToCartDialogData",markup, "#addToCartDialog");
      /* set ics tracking url */
      if(markup.trackingExists){
		   var ts = InsightCommon.getCurrentTimestamp();
		   var src = "http://"+markup.trackingUrl+"ts="+ts+"&csid="+markup.trackingId+"&sid="+Insight.cacheSessionId;
		   $("#icsTrackingInAddItemsToCartTempl").attr("src",src);
      }
      $("#mainHeaderCartTotalPrice").html(" - "+markup.currency +" "+InsightCommon.formatCurrency(markup.totalCost));
      Insight.cartTotalPrice=markup.totalCost;
      selectedIPPwarranty = null;
}

function getAllItemsIncart(){
	var fields =$(":input[name^='accessories_qty_']").serializeArray();
	var accessaryMeterialIds =  $.map(fields, function(field, i){
	      if(IsNumeric(field.value))return ('"'+field.name.split('accessories_qty_')[1]+'"');
	    }).join(",");
	var accessaryQty =  $.map(fields, function(field, i){
	      if(IsNumeric(field.value))return (field.value);
	    }).join(",");
	productCenterAddtoCart(accessaryMeterialIds,accessaryQty,"PPP",null);
	$(":input[name^='accessories_qty_']").val('');
}

function IsNumeric(strString){
	//  check for valid numeric strings
	var strValidChars = "0123456789.-";
	var strChar;
	var blnResult = true;
	if (strString.length == 0) return false;
	//  test strString consists of valid characters listed above
	for (var i = 0; i < strString.length && blnResult == true; i++){
	   strChar = strString.charAt(i);
	   if (strValidChars.indexOf(strChar) == -1){
	      blnResult = false;
	   }
	}
	return blnResult;
}

/* function to export company standards data */
function exportCompanyStandards(){
      var url="/insightweb/export/companyStandard";
      window.open(url); ;
}

function advanceSearchLink(){
	if(location.hash=="#advSearch"){
		 postAdvanceData('advSearch');
	}else
	document.location.href="http://www.insight.com/insightweb/search#advSearch";
}

function softwareProgramsReq(){
	 var jsonObj = '{"salesOrg":"2400"}';
	 licenseJsonServiceRequest(jsonObj);
}

function splaReportUsage(obj) {
	var value = $("#reportUsage").val();
	if(value == '0'){
		recreateSPLACartData();
	}else{
		reportZeroUsage();
	}		
}

function recreateUsageCartData(usageName,usageValue ){
	var json = {"data":{"name" : "", "value" : ""}};
	if(usageValue!=undefined){
		json.data.name = usageName;
		json.data.value = usageValue;
	}
	json = JSON.stringify(json);
	var updatedCart = InsightCommon.getServiceResponse("/insightweb/transaction/recreateUsageCartData", json,"POST",false);
	var redirectUrlForViewCart = InsightCommon.getRootURL() + "/insightweb/viewCart";
	document.location.href = redirectUrlForViewCart;
	return;
}

function reportZeroUsage(usageName,usageValue ){
	if(usageName == 'usageType'){
		if($("#reportType").length>0){
			usageValue = $("#reportType").val();
		}
	}
	var json = {"data":{"name" : "", "value" : ""}};
	json.data.isRequestFromContractPage = true;
	if(usageValue!=undefined){
		json.data.name = usageName;
		json.data.value = usageValue;
	}
	var jsonString = JSON.stringify(json);
    var updatedCart	= InsightCommon.getServiceResponse("/insightweb/transaction/reportZeroUsage", jsonString,"POST",false);
	var redirectUrlForViewCart = InsightCommon.getRootURL() + "/insightweb/viewCart";
	document.location.href = redirectUrlForViewCart;
	return;	
}

function addToCompanyStandards(){    
    var companyStandards = InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/managestandards/companyStandardsProductsTemplate.html");
    $.template("companyStandards",companyStandards);
    InsightSearch.globalCSData = getManagedCompanyStandards();
    data={'data':InsightSearch.globalCSData};
    try {
    	 if(soldToManagementResults!=null )    
    	    	soltToData = soldToManagementResults;
    }catch (e) {
    	soltToData = null;
	}
    if(soltToData!=null)
    data = $.extend(data, {'labels':searchLabels.labels},{"eadminUser":InsightNavigation.isEadminUser},{"soltToData":soltToData});
    else 
    data = $.extend(data, {'labels':searchLabels.labels},{"eadminUser":InsightNavigation.isEadminUser});
    if(InsightNavigation.isEadminUser){
		 $("#pageContent,#createNewUserDIV").empty();
		InsightCommon.renderTemplate("companyStandards",data, "#pageContent");
	}else{
		$('#bodyContent').html('');
		InsightCommon.renderTemplate("companyStandards",data, "#bodyContent");
	}
}

function manageStandardsAddToSet(setId, groupId,addToSetSuccess,addToSetDuplicate,dupProdString,invalidParts){
	var companyStandards = InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/managestandards/companyStandardsProductsTemplate.html");
    $.template("companyStandards",companyStandards);
    InsightSearch.globalCSData = getManagedCompanyStandards();
    data={'data':InsightSearch.globalCSData};
    try {
    	 if(soldToManagementResults!=null )    
    	    	soltToData = soldToManagementResults;
    }catch (e) {
    	soltToData = null;
	}
    if(soltToData!=null)
    data = $.extend(data, {'labels':searchLabels.labels},{"eadminUser":InsightNavigation.isEadminUser},{"soltToData":soltToData});
    else 
    data = $.extend(data, {'labels':searchLabels.labels},{"eadminUser":InsightNavigation.isEadminUser});
    if(InsightNavigation.isEadminUser){
		$("#pageContent,#createNewUserDIV").empty();
		InsightCommon.renderTemplate("companyStandards",data, "#pageContent");
		$('#commentTag').html("");
	}else{
		$('#bodyContent').html('');
		InsightCommon.renderTemplate("companyStandards",data, "#bodyContent");
		$('#commentTag').html("");
	}
    editConfigButton(setId,groupId);
    if(addToSetSuccess=="true"){
    	 $('#commentTag').append("<h2 class='successUpdated'>"+searchLabels.labels.compStdConfigSetUpdated+"</h2>");
    }
    if(addToSetDuplicate=="true"){
    	 var tempString = decodeURIComponent(dupProdString);
    	 var updatedStr = tempString.replaceAll(",",", ");
    	 $('#commentTag').append("<h2 class='alertError'>"+ updatedStr+" " + data.labels.duplicateProductComment +"</h2>");
    }
    if(invalidParts!=null && invalidParts!="" && invalidParts!="null"){
    	var invalidPartIds =  decodeURIComponent(invalidParts);
    	var updatedStr = invalidPartIds.replaceAll(",",", ");
    	$('#commentTag').append("<h2 class='alertError'>"+ data.labels.compStdInvalidMaterialID + " "+updatedStr+"</h2>");
    }
}

/*remove crumbs for narrow licecnse products */
function narrowLicenseProducts(fieldid,fieldValue,breadCrumb){
	var contractIds = '"0040000088","0040000089","0040000090","0040000091","0040000114","0040000117","0040000119","0040000120","0040000125","0040000128","0045000002","0045000011","0045000012","0045000013","0045000015"';
	var jsonObj = '{"salesOrg":"2400","contractIds":['+contractIds+'],';
	if(breadCrumb!=null)
		jsonObj =jsonObj+'"field":["'+breadCrumb+'","'+fieldid+'~'+fieldValue+'"]';
	else		
	jsonObj =jsonObj+'"field":["'+fieldid+'~'+fieldValue+'"]';
    jsonObj = jsonObj + '}';
    licenseJsonServiceRequest(jsonObj);
}

/*remove crumbs for license products */
function licenseBreadCrumbDisplay(breadcrumbDisplay,breadCrumbStr){
	var contractIds = '"0040000088","0040000089","0040000090","0040000091","0040000114","0040000117","0040000119","0040000120","0040000125","0040000128","0045000002","0045000011","0045000012","0045000013","0045000015"';
	var jsonObj = '{"salesOrg":"2400","contractIds":['+contractIds+'],';
	if(breadcrumbDisplay!=null && breadCrumbStr.length>0){
		breadCrumbStr = breadCrumbStr.substring(0,breadCrumbStr.search(breadcrumbDisplay)+ breadcrumbDisplay.length);	
		jsonObj =jsonObj+'"searchString":"'+breadCrumbStr+'"';
	}
	else		
	jsonObj =jsonObj+'"searchString":"'+breadcrumbDisplay+'"';
    jsonObj = jsonObj + '}';
    licenseJsonServiceRequest(jsonObj);
}

/*remove crumbs for license products */
function licenseBreadCrumbRemove(breadcrumbRemove,breadCrumbStr){
	var contractIds = '"0040000088","0040000089","0040000090","0040000091","0040000114","0040000117","0040000119","0040000120","0040000125","0040000128","0045000002","0045000011","0045000012","0045000013","0045000015"';
	var jsonObj = '{"salesOrg":"2400","contractIds":['+contractIds+'],';
	if(breadcrumbRemove!=null && breadCrumbStr.length>0){
		breadCrumbStr = breadCrumbStr.replace(breadcrumbRemove,",");
		jsonObj =jsonObj+'"searchString":"'+breadCrumbStr+'"';
	}	
    jsonObj = jsonObj + '}';
    licenseJsonServiceRequest(jsonObj);
}

/*display selected  license products */
function displayCheckedLicenseProducts(checkboxDiv){
	/* getting all the checked products */
	InsightSearch.checkedProducts = [];
	var programIds = '';
	var licenseContractIds = '';
    $(checkboxDiv+' :checkbox:checked').each(function() {
    	if($(this).val() != "slpCheckBox")
    		InsightSearch.checkedProducts.push('"'+$(this).val()+'"');
          this.checked = false;
    });

    /* if no products are selected */	
    if(InsightSearch.checkedProducts.length == 0) {
	   return;
    }else{
		 InsightCommon.showLoading();
	}
    for(var i=0; i<InsightSearch.checkedProducts.length; i++) {
    	var programContract = InsightSearch.checkedProducts[i];
    	programIds = programContract.substring(0, programContract.indexOf('-'))+'",'+programIds;
    	licenseContractIds = '"'+programContract.substring(programContract.indexOf('-')+1,programContract.length )+','+licenseContractIds;
    }
    programIds = programIds.substring(0,programIds.length -1 );
    licenseContractIds = licenseContractIds.substring(0,licenseContractIds.length -1 );
    document.location.hash = 'searchResults#page=1#field=#inStockFilterType=BothInStockAndOutOfStock#noCLP=true#fromLicense=true#licenseContractIds='+encodeURIComponent(licenseContractIds)+'#licenseCountflag=false#programIds='+encodeURIComponent(programIds)+'#priceRangeLow=#priceRangeHigh=#shown=10#shownFlag=true#searchText=';
}

/*display all license products */
function displayAllLicenseProducts(checkboxDiv){   
	 $(checkboxDiv+' :checkbox').each(function() {
         this.checked = true;
	 });
	 checkedProducts = [];
	 $(checkboxDiv+' :checkbox:checked').each(function() {
        checkedProducts.push('"'+$(this).val()+'"');
        this.checked = false;
	 });
	 document.location.hash = '#softwareLicense';
}

function displaylicenseProcessingforLicenseTab(programId){
	 var checkedProducts = [];
	 checkedProducts.push('"'+programId+'"');
	 displaylicenseProcessing(checkedProducts);
}

function displaySPLAProcessing(checkedProducts){
	InsightCommon.showLoading();
	var lastSearchURL = document.location.href;
	lastSearchURL = lastSearchURL.substring(lastSearchURL.indexOf('insightweb'),lastSearchURL.length);
	var searchResultsByAddressJson = '{"page": 1,"pageNumber": 0,"pageSize": 0, "returnSearchURL": "'+lastSearchURL+'", "programContractIds":['+checkedProducts+'],"shown": 10,"shownFlag":true,"searchText": [],"defaultSort": false,"fromLicense":true,"businessUnit":"2"}';
	var searchResultsByAddressURL =  '/insightweb/getProduct';
	if (searchResultsByAddressURL != "" && searchResultsByAddressJson != "" ){
          var data = InsightCommon.getServiceResponse(searchResultsByAddressURL,searchResultsByAddressJson,"POST");
          if(typeof data!=='undefined' && data!=null){
     		 InsightCommon.getRequestJsonObject = searchResultsByAddressJson;
     	  }
          InsightSearch.displayResults(data);
    }else
        alert(searchErrorMessages.labels.PleaseenterURLANDJSON);
    InsightCommon.hideLoading();
    removeObjectSession(); 
}


function displaylicenseProcessing(checkedProducts){
	InsightCommon.showLoading();
	var lastSearchURL = document.location.href;
	lastSearchURL = lastSearchURL.substring(lastSearchURL.indexOf('insightweb'),lastSearchURL.length);
	var searchResultsByAddressJson = '{"page": 1,"pageNumber": 0,"pageSize": 0, "returnSearchURL": "'+lastSearchURL+'", "programContractIds":['+InsightSearch.checkedProducts+'],"shown": 10,"shownFlag":true,"searchText": [],"defaultSort": false,"fromLicense":true,"businessUnit":"2"}';
	var searchResultsByAddressURL =  '/insightweb/getProduct';
	if (searchResultsByAddressURL != "" && searchResultsByAddressJson != "" ){
          var data = InsightCommon.getServiceResponse(searchResultsByAddressURL,searchResultsByAddressJson,"POST");
          if(typeof data!=='undefined' && data!=null){
     		 InsightCommon.getRequestJsonObject = searchResultsByAddressJson;
     	  }
          InsightSearch.displayResults(data);
    }else
        alert(searchErrorMessages.labels.PleaseenterURLANDJSON);
    InsightCommon.hideLoading();
    removeObjectSession(); 
}


/*json req and resp for  license products */
function licenseJsonServiceRequest(jsonObj){
	InsightSearch.checkedProducts = [];
	InsightSearch.shoppingCartlabels = null;
	InsightSearch.lastReportedUsage = null;
	var setUrl = "/insightweb/getSoftwarePrograms";
	var responseData = InsightCommon.getServiceResponse(setUrl, jsonObj, "POST");
	var licenseTemp = InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/licenseTwoColumnTemp.html");
	$.template("licenseTemp",licenseTemp);
	var licenseLabels = InsightCommon.getMessageContent(InsightSearch.messageUrl, Insight.locale, "http://www.insight.com/messages/search/license1Labels.txt");
	licnseLabels = eval('(' + licenseLabels + ')');
	responseData = $.extend(responseData.softwareContractResponse, {"splContracts":responseData.splContracts},{"ipsContracts":responseData.ipsContracts},{"labels":licnseLabels.labels},{"hitCount":responseData.hitCount},{"breadCrumbStr":responseData.breadCrumbStr});
	var UsageResponseData = InsightCommon.getServiceResponse("/insightweb/getUsageForSoldTo", jsonObj, "GET");
	responseData = $.extend(responseData, {"usageMap":UsageResponseData});
	softwareContractsViewLevels = responseData.softwareContracts;
	if(responseData.softwareContracts!=null && responseData.softwareContracts.length>0){
		$('#bodyContent').html('');
		InsightCommon.renderTemplate("licenseTemp",responseData, "#bodyContent");		
		var licenseLeftNav= InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/licenseLeftNav.html");
		$.template( "licenseLeftNav", licenseLeftNav);
		InsightCommon.renderTemplate("licenseLeftNav",responseData, "#licenseLeftCol1");
		var licenseCenter= InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/licenseCenterResults.html");
		$.template( "licenseCenter", licenseCenter);
		InsightCommon.renderTemplate("licenseCenter",responseData, "#licensecontentCol1");
		$("#viewProductsForSelected").on("click", function(){
    		setTimeout('displayCheckedLicenseProducts(\"#results\");',600);
    	});
		
		// APAC format for 'Effective Date' & 'Expiration Date'
		if(responseData.splContracts!=null){
			$.each(responseData.softwareContracts, function(index1,contract){
				$.each(responseData.splContracts, function(index2,splContract){
					if(contract.sapContractId == splContract.contractID){
						var serviceDate = null;
						var dateTimeArray = null;
						var splitDate = null;
						var splitTime = null;
						var tmp_dt = null;
						var apacDate = null;
						
						/* Effective Date & Time */
						if(splContract.abbreviation!=null && splContract.abbreviation!=""){
							 serviceDate = splContract.abbreviation;
							 dateTimeArray = serviceDate.split(" ");
							 splitDate = dateTimeArray[1]+"-"+dateTimeArray[2]+"-"+dateTimeArray[5];
							 tmp_dt =  $.datepicker.parseDate('M-dd-yy', splitDate);
							 apacDate = $.datepicker.formatDate($.datepicker.content.dateFormat, tmp_dt);
							 splitTime = dateTimeArray[3]+'(GMT - 6)';
							 $("#"+contract.sapContractId+"_effectiveDate").text(apacDate);
							 $("#"+contract.sapContractId+"_effectiveTime").text(splitTime);
						}
						/* Expiration Date & Time */
						if(splContract.longDescription!=null && splContract.longDescription!=""){
							 serviceDate = splContract.longDescription;
							 dateTimeArray = serviceDate.split(" ");
							 splitDate = dateTimeArray[1]+"-"+dateTimeArray[2]+"-"+dateTimeArray[5];
							 tmp_dt =  $.datepicker.parseDate('M-dd-yy', splitDate);
							 apacDate = $.datepicker.formatDate($.datepicker.content.dateFormat, tmp_dt);
							 splitTime = dateTimeArray[3]+'(GMT - 5)';
							 $("#"+contract.sapContractId+"_expirationDate").text(apacDate);
							 $("#"+contract.sapContractId+"_expirationTime").text(splitTime);
						}
					}
				});
			});
	   }
   	   var testPermission = Insight.userPermissions;  
       if($.inArray('enable_browse_sw_contract',testPermission)== -1){
    		$("#SoftwareDisplayedProgrames").css("display","none");
    		$("#SoftwareSelectedProgrames").css("display","none");
       }	
	   $("#searchContent").show();
	}else{	
		window.location.replace("http://www.insight.com/insightweb/welcome");
	}
}

function checkforCMSPage(usrSrch){
	var hasCMSPage = false;
	usrSrch = usrSrch.replace(".",'');
	if(usrSrch.indexOf(",")!= -1){
		var tempStr = usrSrch.split(",");
		usrSrch = tempStr[0];
	}
	var sbbURL = "/insightweb/checkCMSForBrand?search="
			+ encodeURIComponent(usrSrch.toLowerCase());
	$.ajax({
		url : sbbURL,
		type : "GET",
		global : false,
		async : false,
		crossDomain : true,
		success : function(data) {
			hasCMSPage = data;
		},
		error : function(data) {
		}
	});
	return hasCMSPage;
}

String.prototype.replaceAll = function(token, newToken, ignoreCase) {
    var str, i = -1, _token;
    if((str = this.toString()) && typeof token === "string") {
        _token = ignoreCase === true? token.toLowerCase() : undefined;
        while((i = (
            _token !== undefined? 
                str.toLowerCase().indexOf(
                            _token, 
                            i >= 0? i + newToken.length : 0
                ) : str.indexOf(
                            token,
                            i >= 0? i + newToken.length : 0
                )
        )) !== -1 ) {
            str = str.substring(0, i)
                    .concat(newToken)
                    .concat(str.substring(i + token.length));
        }
    }
return str;
};

function addTosetCSChecked(checkboxDiv){
	var grpName = $('#groupName').val();
	var catId = $('#categoryId').val();
	catId = parseInt(catId);
	if(isNaN(catId)){
		catId=0;
	}
	var setId = $('#setId').val();
	var grpId = $('#groupId').val();
	grpId = parseInt(grpId);
	if(isNaN(grpId)){
		grpId = 0;
	}
	var shared = $('#shared').val();
	if(shared=='true'){
		shared=true;
	}else{
		shared=false;
	}
	var controller = $('#controller').val();
	if(controller=='true'){
		controller=true;
	}else{
		controller=false;
	}
	var qty = 1;
	var matList = [];
	$(checkboxDiv+' :checkbox:checked').each(function(){
		matList.push($(this).val());
	  this.checked = false;	  
	});
	var unlinkFlag = false;
	if(!controller){
		unlinkFlag = isUnlinkSharedGroup()? true: false;
	}
	if(unlinkFlag || controller  ){	
		var requestObj = {'categoryId':catId, 'controller':controller,"setId":$('#setId').val(),"setType":InsightSearch.setType};
		requestObj = $.extend(requestObj, {'groupName':grpName, 'groupId':grpId});//, 'allSetsHaveDefaultItemsSelected':allSetsHaveDefaultItemsSelected});
		requestObj = $.extend(requestObj, {'shared':shared, 'qty':qty, 'matList':matList});
		requestObj = JSON.stringify(requestObj);
		var serviceURL = "/insightweb/manage/addItemsToSet/"+controller;
		var responseObj = InsightCommon.getServiceResponse(serviceURL, requestObj, 'POST');	
		setId = responseObj.items.setId;	
		grpId = responseObj.items.groupId;
		var addToSetSuccess = false;
		var addToSetDuplicate = false;
		var dupProdString = null;
		var invalidParts = null;
		if(responseObj.items.addedMaterialIds!=null && responseObj.items.addedMaterialIds!=""){
			addToSetSuccess = true;
		}
		if(responseObj.items.duplicateMaterialIds!=null && responseObj.items.duplicateMaterialIds!=""){
			addToSetDuplicate = true;
			dupProdString = responseObj.items.duplicateMaterialIds;
		}
		if(responseObj.items.invalidMaterialIds!=null && responseObj.items.invalidMaterialIds!=""){
			invalidParts = responseObj.items.invalidMaterialIds;
		}
		if(InsightNavigation.isEadminUser && window.opener != null){		
			window.opener.editConfigButton(setId,grpId);
			var commentTagDiv = window.opener.$("#commentTag");
			if(responseObj.items.addedMaterialIds!="" && responseObj.items.addedMaterialIds!=null){
					$(commentTagDiv).html("<h2 class='successUpdated'>"+searchLabels.labels.compStdConfigSetUpdated+"</h2>");
			}
			if(responseObj.items.duplicateMaterialIds!="" && responseObj.items.duplicateMaterialIds!=null){
				var dupProd = dupProdString.toString();
				$(commentTagDiv).append("<h2 class='alertError'>"+ dupProd.replaceAll(',',', ')+" " + searchLabels.labels.duplicateProductComment +"</h2>");
			}
			if(responseObj.items.invalidMaterialIds!="" && responseObj.items.invalidMaterialIds!=null){
				var invalidString = invalidParts.toString();
	    		$(commentTagDiv).append("<h2 class='alertError'>"+ searchLabels.labels.compStdInvalidMaterialID + " "+invalidString.replaceAll(',',', ')+"</h2>");
			}
			window.close();
			window.opener.focus();
	}else{		
		document.location.href="http://www.insight.com/insightweb/search#manageStandardsAddToSet#setId="+setId+"#groupId="+grpId+"#addToSetSuccess="+addToSetSuccess+"#addToSetDuplicate="+addToSetDuplicate+"#dupProdString="+encodeURIComponent(dupProdString)+"#invalidParts="+encodeURIComponent(invalidParts);
		}	
	}
}

function addTosetCSSelected(selectedMtrId){
	var grpName = $('#groupName').val();
	var catId = $('#categoryId').val();
	catId = parseInt(catId);
	if(isNaN(catId)){
		catId=0;
	}
	var stId = $('#setId').val();
	var grpId = $('#groupId').val();
	grpId = parseInt(grpId);
	if(isNaN(grpId)){
		grpId = 0;
	}
	var shared = $('#shared').val();
	if(shared=='true'){
		shared=true;
	}else{
		shared=false;
	}
	var controller = $('#controller').val();
	if(controller=='true'){
		controller=true;
	}else{
		controller=false;
	}
	var qty = 1;
	var matList = [];
	if(selectedMtrId!=null && selectedMtrId.length > 0){
	    var item = selectedMtrId;
	    matList.push(item);
	}      
	var unlinkFlag = false;
	if(!controller){
		unlinkFlag = isUnlinkSharedGroup()? true: false;
	}
	if(unlinkFlag || controller  ){		
		var requestObj = {'categoryId':catId, 'controller':controller,"setId":$('#setId').val(),"setType":InsightSearch.setType};
		requestObj = $.extend(requestObj, {'groupName':grpName, 'groupId':grpId});//, 'allSetsHaveDefaultItemsSelected':allSetsHaveDefaultItemsSelected});
		requestObj = $.extend(requestObj, {'shared':shared, 'qty':qty, 'matList':matList});
		requestObj = JSON.stringify(requestObj);
		var serviceURL = "/insightweb/manage/addItemsToSet/"+controller;
		var responseObj = InsightCommon.getServiceResponse(serviceURL, requestObj, 'POST');
		stId = responseObj.items.setId;	
		grpId = responseObj.items.groupId;
		var addToSetSuccess = false;
		var addToSetDuplicate = false;
		var dupProdString = null;
		var invalidParts = null;
		if(responseObj.items.addedMaterialIds!=null && responseObj.items.addedMaterialIds!=""){
			addToSetSuccess = true;
			window.opener.focus();
			window.opener.focus();
		}
		if(responseObj.items.duplicateMaterialIds!=null && responseObj.items.duplicateMaterialIds!=""){
			addToSetDuplicate = true;
			dupProdString = responseObj.items.duplicateMaterialIds;
		}
		if(responseObj.items.invalidMaterialIds!=null && responseObj.items.invalidMaterialIds!=""){
			invalidParts = responseObj.items.invalidMaterialIds;
		}
		if(InsightNavigation.isEadminUser && window.opener != null){
			window.opener.editConfigButton(stId,grpId);
			var commentTagDiv = window.opener.$("#commentTag");
			if(responseObj.items.addedMaterialIds!="" && responseObj.items.addedMaterialIds!=null){
				window.opener.updateSuccessComment();
			}else{
				if(responseObj.items.duplicateMaterialIds!="" && responseObj.items.duplicateMaterialIds!=null){
					$(commentTagDiv).append("<h2 class='alertError'>"+ dupProdString+" " + searchLabels.labels.duplicateProductComment +"</h2>");
				}
				if(responseObj.items.invalidMaterialIds!="" && responseObj.items.invalidMaterialIds!=null){
					var invalidString = invalidParts.toString();
					$(commentTagDiv).append("<h2 class='alertError'>"+ searchLabels.labels.compStdInvalidMaterialID + " "+invalidString+"</h2>");
				}
			}
			window.close();
			window.opener.focus();
		}else{
			document.location.href="http://www.insight.com/insightweb/search#manageStandardsAddToSet#setId="+stId+"#groupId="+grpId+"#addToSetSuccess="+addToSetSuccess+"#addToSetDuplicate="+addToSetDuplicate+"#dupProdString="+encodeURIComponent(dupProdString)+"#invalidParts="+encodeURIComponent(invalidParts);
		}
	}
}

function getUrlVarsWithHash(){    
	var vars = [], hash;    
	var hashes = window.location.href.slice(window.location.href.indexOf('/') + 1).split('#');    
	for(var i = 0; i < hashes.length; i++){        
		hash = hashes[i].split('=');        
		vars.push(hash[0]);        
		vars[hash[0]] = hash[1];    
	}    
	return vars;
}

function formatCurrencyForLeftNav(priceRange){		
	priceRange = priceRange.replaceAll("$", "");	
	if(priceRange.indexOf('-')>=0){		
		return InsightCommon.formatCurrency(priceRange.substring(0,priceRange.indexOf('-')))+" - "+InsightCommon.formatCurrency(priceRange.substring(priceRange.indexOf('-')+1,priceRange.length));	
	}else if(priceRange.indexOf('<')>=0){		
		return priceRange.substring(0,1)+' '+InsightCommon.formatCurrency(priceRange.substring(1,priceRange.length));	
	}
	return priceRange;
}

function getProgramIdValues(fieldValues){		
	var programIds = "";
	$.each(fieldValues, function(index, value) {
		programIds = value.fieldValue +","+programIds;
	});
	return programIds.substring(0, programIds.length - 1);
}

function getProgramIdValueLables(fieldValues){		
	var programLables = "";
	$.each(fieldValues, function(index, value) {
		programLables = value.fieldValueLabel +" OR "+programLables;
	});
	return programLables.substring(0, programLables.length - 4);
}

function encodeTemplate(name){
	return encodeURIComponent(escape(name));
}

// add company standards items to cart
function addCompanyStandardItemsTocart(bundleType,check,groupName,catId,contractId,groupId){
	bundleGrpName = decodeURIComponent(unescape(groupName));
	InsightB2BUserAttributes.fromCompanyStandards = true;
	var typeOfBundle = new Array;
	typeOfBundle = [bundleType,check];
	var grpDetails = [catId,contractId,groupId];
	if(check == "button"){
		var fields =$(":input[name^='selectedProduct']").serializeArray();	
		var qtyfields =$(":input[name^='selectedquantity']").serializeArray();
		if(fields!=null && fields.length==0){
			$('#csEmptyCartMessageError').css("display","block");
			$('#csEmptyCartMessageError').html("");
			$('#csEmptyCartMessageError').html(searchErrorMessages.labels.YouMustSelectLeastOneProductYourCart);
			window.scrollTo(0,0);
		}else{
			 var numberOfSingleSelectionSelected = 0;
			 $("input.typeTwo").each(function() {
				 if($(this).is(':checked')){
		 			 numberOfSingleSelectionSelected++;  
		 		 }
			 });
			 var vsppProductFlag=false;
			 if((numberOfSingleSelectionSelected == InsightSearch.numberOfSingleSelection ) ){	
				 // added for VSPP Calcuator from CompanyStandards
				 $.each(fields, function(index, field){
						if($("#vsppPrdoductFlag_"+field.value).val()=='true'){
							vsppProductFlag=true;
							return false;
						}	
				 });
				 if(vsppProductFlag){
					 	var rootURL = InsightCommon.getRootURL();							
					    window.location = rootURL + "/insightweb/vsppcalculator";				
					    return;
				 }else{
					 var accessaryMeterialIds =  $.map(fields, function(field, i){
					 return ('"'+field.value+'"');
					 }).join(",");
					 var bundleName = $.map(fields, function(field, i){
						 var bundleStr = field.name.split('selectedProduct_')[2];
						 bundleStr = bundleStr.substring(0,bundleStr.length-1);
						 return (bundleStr);
					}).join(",");
					var bundleConfig = $.map(fields, function(field, i){
						var bundleStr = field.name.split('selectedProduct_')[3];
					    bundleStr = bundleStr.substring(0,bundleStr.length-1);
					    return (bundleStr);
					}).join(",");
					var accessaryQty =  $.map(qtyfields, function(field, i){
						var id= jQuery.map(fields, function(n, i){  return n.value; });
						if($.inArray(field.name.split('selectedquantity_')[1],id)!=-1){
							if(IsNumeric(field.value))return (field.value);}
						}).join(",");
						bundleName=bundleName.replaceAll('"', '\\"');
						productCenterAddtoCart_CS(accessaryMeterialIds,accessaryQty,"bundle",typeOfBundle,bundleConfig,bundleName);		
			 	  }
			 }else {	
			    $('#csEmptyCartMessageError').css("display","block");
				$('#csEmptyCartMessageError').html(searchErrorMessages.labels.YouMustSelectAtLeastFromSinglSelectionCart);
				window.scrollTo(0,0);			  
			 }
		}
	}else if(check == "cartImage"){
		var accessaryMeterialIds = "";
		var accessaryQty = 0;
		var bundleConfig = "";
		var bundleName = bundleGrpName; 
		InsightCommon.showLoading();
		bundleName=bundleName.replaceAll('"', '\\"');
		accessaryQty = accessaryQty + $("#qty_"+groupId).val();
		accessaryQty = parseFloat(accessaryQty);
		if(isNaN(accessaryQty)){
		     return true;
        }else if(parseInt(accessaryQty) < 1){
			   return true;
		}
		var jsonObj = '{"categoryId":'+catId+',"groupContractId":"'+contractId+'", "setId":'+groupId+'}';
		var setUrl = "/insightweb/account/companyStandard/groupSet";
		groupresponseData = InsightCommon.getServiceResponse(setUrl, jsonObj, "POST");
		groupresponseData.groupId = groupId;
		var productList=[];
		// added for VSPP Calcuator from CompanyStandards
		var vsppProductFlag=false;
		$.each(groupresponseData, function(index, value){
			if(value.psitem!=null){
				$.each(value.psitem, function(itemIndex, itemValue){
			   		if(itemValue.selected && itemValue.product!=null && itemValue.product.vsppProduct.toString()=='true'){
							vsppProductFlag=true;
							return false;		
			   		}					 													
				});
		   	}
		});
		if(vsppProductFlag){
			 	var rootURL = InsightCommon.getRootURL();							
			    window.location = rootURL + "/insightweb/vsppcalculator";				
			    return;
		}else{
			$.each(groupresponseData, function(index, value){
				if(value.psitem!=null){
					$.each(value.psitem, function(itemIndex, itemValue){
				   		if(itemValue.selected && itemValue.product!=null){
				   			  var qty = $("#qty_"+groupId).val();
				   			  accessaryMeterialIds = accessaryMeterialIds +'"'+ itemValue.materialId +'"'+ ",";
		   	   				  bundleConfig= bundleConfig + itemValue.configured + ",";
		   	   				  productList=productList.concat(itemValue.product);
				   		}
					});
			   	}
			});
			if(productList!=null && productList.length>0){
				addProductGroupSearchResponse=productList;
				accessaryMeterialIds = accessaryMeterialIds.substring(0, accessaryMeterialIds.length-1);
				bundleConfig = bundleConfig.substring(0, bundleConfig.length-1);
				InsightCommon.hideLoading();
				productCenterAddtoCart_CS(accessaryMeterialIds,accessaryQty,"bundle",typeOfBundle,bundleConfig,bundleName);	
			}else{
				$('#csEmptyCartMessageError').css("display","block");
				$('#csEmptyCartMessageError').html("");
				$('#csEmptyCartMessageError').html(searchErrorMessages.labels.YouMustSelectLeastOneProductYourCart);
				window.scrollTo(0,0);
				InsightCommon.hideLoading();
			}
		}
	}
}

function productCenterAddtoCart_CS(materialIds,quantity,fromPage,bundleType,bundleConfig,bundleName){
	var arrPC = [];
    var arrPC = materialIds.split(',');   
    var contractId = null;
	if(bundleType!=null){
	 	 contractId = $('#csContractId').val();
	 	 if(contractId==null || contractId==''){
	 		contractId = '';
	 	 }
    }
	var groupSetSearchResponse=null;
	if(bundleType!=null && bundleType.length>1){
		if(bundleType[1] == "button"){
			groupSetSearchResponse=searchProductResponse;
		}else if(bundleType[1] == "cartImage"){
			groupSetSearchResponse=addProductGroupSearchResponse;
		}
	}else{
		groupSetSearchResponse=groupresponseData;
	}
	if(arrPC.length==1){	
		    if($('#contractPricingTemplatePopUpDiv input:radio[name=contractPrice]:checked').val() !=undefined ){
			  contractId = $('#contractPricingTemplatePopUpDiv input:radio[name=contractPrice]:checked').val();
			} 	
	    	var productsResponse=null;
	    	var data = null;
	    	if(InsightSearch.purchasePopUpPermission()){
	    		productsResponse=setSelectedProductInfo(arrPC,groupSetSearchResponse, bundleType, "webProduct");
	    	}
	    	if(InsightSearch.purchasePopUpPermission()){
		    	$("#addToCartDialog").dialog({ width: 600, resizable: false,modal:true,position:['top',150], overlay: { background: "#F2F2F2" }});
		    	insightLightBox.styleCustomDialog("#addToCartDialog");
		    }
			var jsonObj=getAddToCartPPCDialog(arrPC[0],productsResponse,quantity,fromPage,bundleType,bundleConfig,bundleName);
  	        var url = bundleCartURL;
		    InsightCommon.getServiceResponseAsync(url,jsonObj,"POST",function(dataObj){					
				data = dataObj;
				if(InsightB2BUserAttributes.isB2BUser==true){
        	  		document.location.href="http://www.insight.com/insightweb/viewCart";
        	  	}else if((fromPage=='PPP' && !InsightSearch.purchasePopUpPermission()) || fromPage=='NOPOP'){ 
        	  		document.location.href="http://www.insight.com/insightweb/viewCart";
        	  	}else if(!InsightSearch.purchasePopUpPermission()){
        	  		document.location.href="http://www.insight.com/insightweb/viewCart";
          	  	}else        	   
          	  		if(data!=null && productsResponse!=null){
          	  			getAddtoCartItemsSimpleDialogInfo(data,productsResponse,productsTotalQuantity);       	  		               	  		
            	}
			});
 	}else if(arrPC.length>1){		  
			var productsResponse = null;
			if(InsightSearch.purchasePopUpPermission()){
	    		productsResponse=setSelectedProductInfo(arrPC,groupSetSearchResponse, bundleType,"products");				
			}
			if(InsightSearch.purchasePopUpPermission()){
		    	$("#addToCartDialog").dialog({ width: 600, resizable: false,modal:true,position:['top',150],overlay: { background: "#F2F2F2" }});
		    	insightLightBox.styleCustomDialog("#addToCartDialog");
			}
			var jsonObj=getAddToCartPPCDialog(materialIds,productsResponse,quantity,fromPage,bundleType,bundleConfig,bundleName);
			var url = bundleCartURL;
		    InsightCommon.getServiceResponseAsync(url,jsonObj,"POST",function(dataObj){					
				data = dataObj;
				if(InsightB2BUserAttributes.isB2BUser==true){
        	  		document.location.href="http://www.insight.com/insightweb/viewCart";
        	  	}else if((fromPage=='PPP' && !InsightSearch.purchasePopUpPermission()) || fromPage=='NOPOP'){ 
        	  			document.location.href="http://www.insight.com/insightweb/viewCart";
        	  	}else if(!InsightSearch.purchasePopUpPermission()) {
          	  	 document.location.href="http://www.insight.com/insightweb/viewCart";
          	  	}else         	   
          	  		if(data!=null && productsResponse!=null){
          	  			getAddtoCartItemsSimpleDialogInfo(data,productsResponse,productsTotalQuantity);       	  		               	  		
 		     	    }
			});
	}
}

function getSelectedProductInfo(selectedProductList, productGroupResponse){
	var productList=[];
	for(var i=0; i<selectedProductList.length;i++){
		var materialID=selectedProductList[i];
		materialID=materialID.replaceAll('"', '');
		if(materialID!=null){
			$.each(productGroupResponse, function(productIndex, productItem){
				if(productItem.materialId !=null && productItem.materialId==materialID){
					productList=productList.concat(productItem);
					return false;
				}
			});
		 }
	 }
	return productList;
}


function setSelectedProductInfo(arrPC,searchProductResponse, bundleType, attribute){
	var productList=[];
	var productsResponse=null;
	// if from button, get the selected items product info.
	if(bundleType!=null && bundleType.length>1){
		if(bundleType[1]=='button'){
			productList= getSelectedProductInfo(arrPC,searchProductResponse);
			if(productList!=null && productList.length>0){
				productsResponse=new Object();
				if(productList.length==1){
					productsResponse[attribute]=productList[0];
				}else{
					productsResponse[attribute]=productList;
				}
			}
		}else if(bundleType[1]=='cartImage'){
			//if from cartImage, use the product info from searchProductResponse directly.
			if(searchProductResponse!=null && searchProductResponse.length>0){
				productsResponse=new Object();
				if(searchProductResponse.length==1){
					productsResponse[attribute]=searchProductResponse[0];
				}else{
					productsResponse[attribute]=searchProductResponse;
				}
			}
		}
	}
	return productsResponse;
}

function countChars(textbox, counter, max) {
	  var count = max - $("."+textbox).val().length;
	  $("#"+counter).html("");
	  if(count>=20)
		  $("#"+counter).html(count);
	  else
		 $("#"+counter).html("<span style=\"color: red;\">" +count+ "</span>");
}

function getAjaxLoader(){
	var str1 = Insight.envConfigs.assetsurl;
	var str2 = "404.html"/*tpa=http://www.insight.com/ccms_img/ajax-loader.gif*/;
	var src = str1 + str2;
	return src;
}

function addOEMWarrantyToCartFromPopup(warrantyId,warrantyType,parentMtrId,contractId){
	/*show loading image*/
	var imgSrc = getAjaxLoader();
	$("#cartSubtotalCurrency").hide();
	$("#cartSubTotal").html('<img alt="" width="20px" height="20px" class="ajaxWaitImage" src='+imgSrc+'/>');
	if(contractId == undefined || contractId == ''){
		contractId = "";
	} 
	var cart = InsightCommon.getServiceResponse("/insightweb/transaction/getcart", "","GET",false);
	if(parentMaterialIdKey!=null && parentMaterialIdKey!=undefined){
		var addOEMRequestObj ={"materialID":"","warrantyDetail":{"parentMaterialId":"","parentMaterialIDKey":"","warrMaterialId":"","quantity":0,"contractID":"" }};
		addOEMRequestObj.materialID = parentMtrId;
		addOEMRequestObj.warrantyDetail.parentMaterialId = parentMtrId;
		addOEMRequestObj.warrantyDetail.parentMaterialIDKey = parentMaterialIdKey;
		addOEMRequestObj.warrantyDetail.warrMaterialId = warrantyId;
		if(contractId!=""){			
			addOEMRequestObj.warrantyDetail.contractID = contractId;	  		
	    }
		$.each(cart.contracts,function(contractIDIndex,contract){
			if(contract.cartItems!=null){
				$.each(contract.cartItems,function(itemIndex,CItem){
					if(CItem !=null){
						if(CItem.materialIDKey == parentMaterialIdKey){
							addOEMRequestObj.warrantyDetail.quantity = CItem.quantity;
						}
					}
				});
			}			
		});	
	}
	InsightCommon.getServiceResponseAsync("/insightweb/transaction/addOEMtocart",JSON.stringify(addOEMRequestObj),"POST",function(addOEMtoCartRes){
		 InsightSearch.cartFromAddToCart = addOEMtoCartRes;
		 /* update cart total in pop up & header */
		 if(addOEMtoCartRes!=null){
			 if(addOEMtoCartRes.subTotal!=null || addOEMtoCartRes.subTotal==0){
				 $("#cartSubtotalCurrency").show();
				 $("#cartSubTotal").html(InsightCommon.formatCurrency(addOEMtoCartRes.subTotal));
				 $("#mainHeaderCartTotalPrice").html(" - "+addOEMtoCartRes.currency +" "+InsightCommon.formatCurrency(addOEMtoCartRes.totalCost));
			 }
		 }
	});
}

function addOrDeleteIPPWarrantyInCartPopup(warrantyId,warrantyType,parentMtrId,contractId,deleteFlag){
	/* show loading image */
	var imgSrc = getAjaxLoader();
	$("#cartSubtotalCurrency").hide();
	$("#cartSubTotal").html('<img alt="" width="20px" height="20px" class="ajaxWaitImage" src='+imgSrc+'/>');
	if(contractId == undefined || contractId == ''){
		contractId = null;
	} 
	/* delete recently added OEM */
	if(InsightSearch.oemAddedToCartFromPopup!=undefined && InsightSearch.oemAddedToCartFromPopup.length>0){
		var length = InsightSearch.oemAddedToCartFromPopup.length;
		var oemWarranty =  InsightSearch.oemAddedToCartFromPopup[length-1];
		deleteOEMWarrantyFromCartPopup(oemWarranty,parentMtrId,contractId);
		InsightSearch.oemAddedToCartFromPopup.length = 0;
	}
	var materialIDKey = null;
	/* get parent materialIDKey from cart */
	var jsonObj = '';
	var cart = InsightCommon.getServiceResponse("/insightweb/transaction/getcart", jsonObj,"GET",false);
	if(deleteFlag == false){
		if(parentMaterialIdKey!=null && parentMaterialIdKey!=undefined){
			materialIDKey = parentMaterialIdKey;
		}else{
			if(cart!=null && cart.contracts!=null){
				$.each(cart.contracts,function(index1,contract){
					if(contract.cartItems!=null){
						$.each(contract.cartItems,function(index2,item){
							if(parentMtrId == item.materialID){
								materialIDKey = item.materialIDKey;
							}
						});
					}
				});
			}
		}
	}
	if(deleteFlag == true){
		if(cart!=null && cart.contracts!=null){
			$.each(cart.contracts,function(index1,contract){
				if(contract.cartItems!=null){
					$.each(contract.cartItems,function(index2,item){
						if(parentMtrId == item.materialID){
							if(item.selectedwarrantyDetails!=null && item.selectedwarrantyDetails.ippMaterialId == warrantyId)
								materialIDKey = item.materialIDKey;
						}
					});
				}
			});
		}
		warrantyId = '';
	}
	var jsonRequestObj = null;
	var responseData = null;
	var quantity = 1;
	if(InsightSearch.parentProductQuantity != null){
		quantity = InsightSearch.parentProductQuantity;
	}
	var url = "/insightweb/transaction/addIPPtocart";
	if(contractId!=null){ 
  		jsonRequestObj = '{"warranty" : {"parentMaterialId" :  "'+materialIDKey+'","ippMaterialId": "'+warrantyId+'","quantity": '+quantity+',"contractID": '+contractId+'}}';
    }else{
    	jsonRequestObj = '{"warranty" : {"parentMaterialId" :  "'+materialIDKey+'","ippMaterialId": "'+warrantyId+'","quantity": '+quantity+'}}';
    }
	InsightCommon.getServiceResponseAsync(url,jsonRequestObj,"POST",function(addToCartResponse){	
		 responseData = addToCartResponse;
		 InsightSearch.cartFromAddToCart = responseData;
		 /* update cart total in pop up & header */
		 if(responseData!=null){
			 if(responseData.subTotal!=null || responseData.subTotal==0){
				 $("#cartSubtotalCurrency").show();
				 $("#cartSubTotal").html(InsightCommon.formatCurrency(responseData.subTotal));
				 $("#mainHeaderCartTotalPrice").html(" - "+responseData.currency +" "+InsightCommon.formatCurrency(responseData.totalCost));
			 }
		 }
	 });
}

function deleteOEMWarrantyFromCartPopup(warrantyId,parentMtrId,contractId){
	/* show loading image*/
	var imgSrc = getAjaxLoader();
	$("#cartSubtotalCurrency").hide();
	$("#cartSubTotal").html('<img alt="" width="20px" height="20px" class="ajaxWaitImage" src='+imgSrc+'/>');
	if(contractId == undefined){
		contractId = "";
	}
	/* get materialIDKey from cart*/
	var jsonObj = '';
	var cart = InsightCommon.getServiceResponse("/insightweb/transaction/getcart", jsonObj,"GET",false);
	var OEMmaterialIDKey = null;
	if(parentMaterialIdKey!=null && parentMaterialIdKey!=undefined){
		$.each(cart.contracts,function(contractIDIndex,contract){
			if(contract.cartItems!=null){
				$.each(contract.cartItems,function(itemIndex,CItem){
					if(CItem !=null){
						if(CItem.materialIDKey == parentMaterialIdKey){
							if(CItem.selectedProductWarranty!=null){
								OEMmaterialIDKey = CItem.selectedProductWarranty.warrMaterialIdKey;
							}
						}
					}
				});
			}			
		});	
	}
	if(OEMmaterialIDKey!=null && OEMmaterialIDKey!=""){
		var deleteOEmRequestObj = '{"materialID" : "'+OEMmaterialIDKey+'", "contractID" : "'+contractId+'"}';
		var responseData = InsightCommon.getServiceResponse("/insightweb/transaction/deletefromcart", deleteOEmRequestObj,"POST");
		/* update cart total in pop up & header */
		 if(responseData!=null){
			 InsightSearch.cartFromAddToCart = responseData;
			 if(responseData.subTotal!=null || responseData.subTotal==0){
				 $("#cartSubtotalCurrency").show();
				 $("#cartSubTotal").html(InsightCommon.formatCurrency(responseData.subTotal));
				 $("#mainHeaderCartTotalPrice").html(" - "+responseData.currency +" "+InsightCommon.formatCurrency(responseData.totalCost));
			 }
		 }
	}
}

function viewContractPricingOptions(element,materialID){
	var getContractPricesReq  = {"morePricesFlag" : {"morePricesFlag":true},"materialId" : ""};
	getContractPricesReq.materialId = materialID;		
	$.when( InsightCommon.getServiceResponseAsync('/insightweb/getCachedContractPrices/'+InsightCommon.getCurrentTimestamp(),JSON.stringify(getContractPricesReq),"POST"),
    		($.template["contractPricingDropBoxTmpl"] == undefined) ? InsightCommon.getContentAsync("/insightweb/getStaticContent",null,"http://www.insight.com/insightweb/assets/en_US/www/javascript/search/contractPricingDropBox.html") : $.template["contractPricingDropBoxTmpl"]		    		        		
        	).then(function(contractPricesRes, contractPricingDropBoxTemplateRes){
        		$.template("contractPricingDropBoxTmpl", contractPricingDropBoxTemplateRes[0]);
        		var searchLabels = InsightSearch.loadSearchLabels();
        		if(contractPricesRes[0]!=null){
        			var contractResult = contractPricesRes[0];
        			showDropDown(element,"left");/* to open flying dropdown for contract prices - empty div*/
        			var data = $.extend({'webProduct':contractResult[0]}, {'labels':searchLabels.labels});
        			InsightCommon.renderTemplate("contractPricingDropBoxTmpl", data, "div#commonContractDropDown");
		   	    }
    	})
    	.fail(function(){
    	      alert("requests failed");
    	});
}

function updateContractDropDown(trigger,contractTitle, ContractID,materialID){	
	closeDropDown();	
	var clicked=$(trigger.target);
	$('a[name="'+materialID+'_AddToCartAnchor"]').attr("onclick","javascript:addAccessoryProduct('"+materialID+"','"+ContractID+"');"); // only for popup
}

function showDropDown(element,pos,wid){
	var left = $(element).offset().left;
    var top = $(element).offset().top;
    if(pos != null && pos == 'left'){
    	left = left-120;    	
	}else {
		//DropDown on the right side of element
		left = left+ $(element).width(); 
	}    
    top = top+ $(element).height();
    $('body').append('<div class="commonContractDropDown" id="commonContractDropDown">&nbsp;</div>');
    //set custom width
	if(wid != null){
		$('div#commonContractDropDown').css("width",wid);
	}
	$('#commonContractDropDown').show();
	$('#commonContractDropDown').css("top", top+ "px");
	$('#commonContractDropDown').css("left",left+ "px");
}

function closeDropDown(){
	$('div#commonContractDropDown').remove();	
}

function viewMoreAccessories(materialId){
	$("#addToCartDialog").empty();
	$("#addToCartDialog").dialog("close");	 
	var moreAccessoriesURL = InsightCommon.getRootURL()+"/insightweb/search#!ProdInfoMtrId="+materialId+"~fromSearch~moreRecommended";
	window.location.replace(moreAccessoriesURL);	
}

function formatDate(obj){
	return $.datepicker.formatDate('dd-mm-yy', new Date(obj)); 
}

function formatSplaDate(format,obj){
	return $.datepicker.formatDate(format, new Date(obj)); 
}

/**
 * Selection/Deselection of the parent checkbox checks/unchecks the child checkboxes on the page (Ex: software contracts page - My software License Agreements).
 */
function selectAllCheckBox(){
    $('.selectedId').prop('checked', $('#selectall').is(":checked"));
    viewProdForSelProg();
}

/**
 * Selection/Deselection of the child checkbox, checks/unchecks the parent checkbox based on the select all/none scenario.
 */
function resetSelectAllCheckBox(){
    if ($(".selectedId").length == $(".selectedId:checked").length){
        $("#selectall").attr("checked", "checked");
    }else{
        $("#selectall").removeAttr("checked");
    }
    viewProdForSelProg();
}

/**
 * Enables the button if atleast one of the checkbox is selected
 */
function viewProdForSelProg(){
	if($(".selectedId:checked").length > 0){
		$("#viewProductsForSelected").addClass("grey").removeClass("ltgrey").css('opacity','1').attr('title','View Products For Selected Agreement');	 
    }else{
    	$("#viewProductsForSelected").addClass("ltgrey").removeClass("grey").css('opacity','0.8').attr('title','Disabled');;
    }
}

/**
 * Brings a pop onclick of 'View Levels' on software contracts page. 
 * @param obj - is used to get the indexValue of the levels in softwareContractsViewLevels json data.
 */
function viewLevels(obj) {
	$( "#licenseCenterRight" ).remove();
    $('body').append('<div id="licenseCenterRight"></div>'); 
	var indexValue = parseInt($(obj).attr("num"));
	var poolDescriptions="";
	 $.each(softwareContractsViewLevels[indexValue].poolDescriptions, function(index,value){
		 poolDescriptions=poolDescriptions+""+value+"<br>";
	 });
	 $("#licenseCenterRight").html("");
	 $("#licenseCenterRight").html(poolDescriptions);
	 $("#licenseCenterRight").dialog({
					title: softwareContractsViewLevels[indexValue].programName,
					height: 200,
					width: 400,
					resizable: true,
					draggable: true
	});
}

/**
 * Takes to the products page for the selected program
 * @param obj - get the id ("A000000022-0040002360") to search the products of the selected program
 */
function viewProducts(obj) {
	InsightSearch.checkedProducts = [];
	InsightSearch.checkedProducts.push('"'+$(obj).attr("id")+'"');
	var programIds = '';
	var licenseContractIds = '';
	for(var i=0; i<InsightSearch.checkedProducts.length; i++) {
	   	var programContract = InsightSearch.checkedProducts[i];
	   	programIds = programContract.substring(0, programContract.indexOf('-'))+'",'+programIds;
	   	licenseContractIds = '"'+programContract.substring(programContract.indexOf('-')+1,programContract.length )+','+licenseContractIds;
	}
	programIds = programIds.substring(0,programIds.length -1 );
	licenseContractIds = licenseContractIds.substring(0,licenseContractIds.length -1 );	
	document.location.hash = 'searchResults#page=1#field=#inStockFilterType=BothInStockAndOutOfStock#noCLP=true#fromLicense=true#licenseContractIds='+encodeURIComponent(licenseContractIds)+'#licenseCountflag=false#programIds='+encodeURIComponent(programIds)+'#priceRangeLow=#priceRangeHigh=#shown=10#shownFlag=true#searchText=';
}

function getLastXMonth(x) {
    var date = new Date();
    var newDate = new Date(date.getFullYear(), date.getMonth(), 1);
    newDate.setMonth((newDate.getMonth()) + x);
    return newDate;
}

/*
 * this method is used to get usgae report of webgroup and show it in Software License Agreements page for all products
 */
function getUsageForWebgroup(contractEffectiveDate,usage,programID){
	InsightSearch.reportUsageMonth = "";
	InsightSearch.reportUsagePeriod = "";
    var options = "";
    var dueMonthsFlag = false;
    var reportingMonths = new Array();
    var sDate = null; 
	var effDateMonth = null;
	var effDateYear = null;
	var j= 0;
	var date = new Date();
	if(programID === "A000000023" || programID ==="A000000024"){
		InsightSearch.SPLAProgramID = true;
	}else if(programID === "A000000006"){
		InsightSearch.CITRIXPRogramID = true;
	}
	if(InsightSearch.shoppingCartlabels == null) {
		InsightSearch.shoppingCartlabels = InsightCommon.getMessageContent(messageUrl, Insight.locale, "/messages/transaction/ShoppingCart/shoppingcartlabels.txt/jcr:content/jcr:data.txt");
	}
	Templatelabels = JSON.parse(InsightSearch.shoppingCartlabels);
    InsightSearch.lastReportedUsage =	usage;
	if (contractEffectiveDate != null &&  contractEffectiveDate != ""){
		sDate = new Date(contractEffectiveDate);
  		if (sDate != null){
  			effDateMonth = (sDate.getMonth() + 1);
  			effDateYear = sDate.getFullYear();
  		}	
  	}
    prevMonthDate  = getLastXMonth(-1);
    for (var i = 6; i >=1; i--){
    	var minusMonth = (i * -1);
        var prevDate = getLastXMonth(minusMonth);
        var usageFound = false;
        var lastMonthDueFlag = false;        
        if (usage.length == 0){
        	lastMonthDueFlag = true;
        }    
        $.each(usage, function(key, val) {
        	var parts = val.split('-');
        	var d = new Date(parts[0], parts[1]-1, parts[2]);
        	if ((d.getMonth() == prevDate.getMonth()) && (d.getYear() == prevDate.getYear())) {
        		usageFound = true;
				if(programID === "A000000023" || programID ==="A000000024") {
        			if((Insight.webGroupPermissions!=null) && (($.inArray("allow_unlimited_spla_ordering", Insight.webGroupPermissions) > -1)) 
            				&& ((prevMonthDate.getMonth() == prevDate.getMonth()) && (prevMonthDate.getYear() == prevDate.getYear()))){
    					usageFound = false;
    					lastMonthDueFlag = false;
                    } else {
                    	lastMonthDueFlag = true;
                    }
        		}
        		if(programID === "A000000006") {
        			if((Insight.webGroupPermissions!=null) && (($.inArray("allow_unlimited_citrix_ordering", Insight.webGroupPermissions) > -1)) 
            				&& ((prevMonthDate.getMonth() == prevDate.getMonth()) && (prevMonthDate.getYear() == prevDate.getYear()))){
    					usageFound = false;
    					lastMonthDueFlag = false;
                    }else{
                    	lastMonthDueFlag = true;
                    }
        		}
			}else{
				lastMonthDueFlag = true;
			}
        });

        var addOptions = false;
        if (!usageFound) {
        	var monthStr = "";
        	addOptions = false;
        	switch(prevDate.getMonth()) {
        	case 0:
        		monthStr = Templatelabels.labels.Jan;
        		break;
        	case 1:
        		monthStr = Templatelabels.labels.Feb;
        		break;
        	case 2:
        		monthStr = Templatelabels.labels.Mar;
        		break;
        	case 3:
        		monthStr = Templatelabels.labels.Apr;
        		break;
        	case 4:
        		monthStr = Templatelabels.labels.May;
        		break;
        	case 5:
        		monthStr = Templatelabels.labels.Jun;
        		break;
        	case 6:
        		monthStr = Templatelabels.labels.Jul;
        		break;
        	case 7:
        		monthStr = Templatelabels.labels.Aug;
        		break;
        	case 8:
        		monthStr = Templatelabels.labels.Sep;
        		break;
        	case 9:
        		monthStr = Templatelabels.labels.Oct;
        		break;
        	case 10:
        		monthStr = Templatelabels.labels.Nov;
        		break;
        	case 11:
        		monthStr = Templatelabels.labels.Dec;
        		break;
        	}
        	if (effDateMonth != null && effDateYear != null){
            	if (prevDate.getFullYear() > effDateYear ){
            		addOptions = true;
            	}else if (prevDate.getFullYear() == effDateYear){
            		if ((prevDate.getMonth() + 1) >= effDateMonth ){
                		addOptions = true;
                	}
            	}
        	}else{
        		addOptions = true;
        	}
        	if (addOptions){
	    		reportingMonths[j] = monthStr+ ' ' + prevDate.getFullYear();
	    		if (i>1){
	    			options = options+', '+monthStr;
	        	}
				if (i==1 && date.getDate()>10 && lastMonthDueFlag){
	    			options = options+', '+monthStr;
	    		}	
	    		j = j+1;
        	}	
        }
    }    		
	InsightSearch.reportUsageMonth = reportingMonths[0];			
	InsightSearch.reportUsagePeriod = options.substring(1, options.length);

	if(InsightSearch.CITRIXPRogramID && (InsightSearch.reportUsageMonth == null || InsightSearch.reportUsageMonth =="")) {
		InsightSearch.reportUsageMonth = Templatelabels.labels.AllReportingPeriodsCurrent;
		InsightSearch.CITRIXPRogramID = false;
	}else if(InsightSearch.SPLAProgramID && (InsightSearch.reportUsageMonth == null || InsightSearch.reportUsageMonth =="")){
		InsightSearch.reportUsageMonth = Templatelabels.labels.AllReportingPeriodsCurrent;
		InsightSearch.SPLAProgramID = false;
	}
	if (InsightSearch.reportUsagePeriod != null && InsightSearch.reportUsagePeriod != "") {
		dueMonthsFlag = true;
	} else {
		dueMonthsFlag = false;
	}
	return dueMonthsFlag;
}


function selectDropDown(){
	$('.selectDropDown').toggleClass('moreNarrowOptionsDiv');
}

function hideSelectDropDown(){
	$('.selectDropDown').addClass('moreNarrowOptionsDiv');
}

function getByodContent(){
	return;
}

function setByodImages(){
	return;
}

function exportReportUsage(){
    var url="/insightweb/export/reportUsage";
    window.open(url);
}