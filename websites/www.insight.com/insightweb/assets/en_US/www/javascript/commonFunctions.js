var showLoadingTimer;

(function() { var InsightCommon = window.InsightCommon =
{
	/* 
	 * To get JQUERY templates from default en_US Asynchronous
	 * @param url: service url to get content
	 * @param language: locale to use for locale specific content
	 * @param path: path to content file within assets directory
	 * @param callback:  function to execute after ajax call
	 */
	getContentAsync : function (url, language, path, callback)
    {       
		//disable cache locally for development
	 	var cacheFlag = true;
	 	/*if(window.location.href.indexOf("localhost") >= 0)
	 	{
	 		cacheFlag = false;	 		
	 	}*/
		
		
	 	if (path != null) {
	 		if(language !=null  && language.length == 5 && language != Insight.envConfigs.defaultLocale)
	 		{
	 			url = Insight.envConfigs.assetsurl.substring(0,Insight.envConfigs.assetsurl.length-5)+language+"/www/" + path;
	 		}
	 		else {
	 			url = Insight.envConfigs.assetsurl.substring(0,Insight.envConfigs.assetsurl.length-5)+Insight.envConfigs.defaultLocale+"/www/" + path + "?cache=" + Insight.cacheSessionId;
	 		}
	 	
	 		var xhr = $.ajax({ url:url, success:function(data){
					if( data == 'sessionTimeOut'){
						document.location.href = 'http://www.insight.com/insightweb/login';
					}else{
						if(callback != null)
						{
							callback(data);
						}
						else {
							return data;
						}
					}
				},
				//error: function(jqXHR, textStatus, errorThrown) {alert(errorThrown);},
				async:true, cache:cacheFlag });
			if(callback == null)
	        	return xhr;
	 	}
	 	else {
	 		alert("no path specified!");	 	
	 	}
    },        
    /* 
     * To get content or labels from CMS Asynchronous
     * @param url: service url to get message content
     * @param language:  not used
     * @param path: path for content file within CMS
     * @param callback:  function to execute after ajax call
     */
    getMessageContentAsync : function (url, language, path,callback)
    {         
    	//disable cache locally for development
	 	var cacheFlag = true;
	 	/*if(window.location.href.indexOf("localhost") >= 0)
	 	{
	 		cacheFlag = false;	 		
	 	}*/
        
        if (path != null)
        {
        	//remove /jcr:content/jcr:data.txt if included in url
            if(path.indexOf("/jcr:content/jcr:data.txt") > 0) 
            {
            	path = path.replace("/jcr:content/jcr:data.txt","");
            }
            if((window.location.href.indexOf("localhost") >= 0) || (window.location.href.indexOf("-preview") >= 0)){
            	url = url + "?path=" + Insight.envConfigs.cmsServer.substring(Insight.envConfigs.cmsServer.length-6,Insight.envConfigs.cmsServer.length)  + path + "&cache=" + Insight.cacheSessionId;
            }
            else{
            	url = Insight.envConfigs.cmsServer.substring(Insight.envConfigs.cmsServer.length-6,Insight.envConfigs.cmsServer.length)  + path;
            }
	        var xhr = $.ajax({ url:url, 
	        	success:function(data){
	        		if( data == 'sessionTimeOut'){
						document.location.href = 'http://www.insight.com/insightweb/login';
					}else{
						if(callback != null)
						{
							callback(data);
						}
						else {
							return data;
						}
					}        	
	        	},
	        	//error: function(jqXHR, textStatus, errorThrown) {alert(errorThrown);}, 
	        	async:true,  cache:cacheFlag});
	        
	        if(callback == null)
	        	return xhr;
        }
        else {
	 		alert("no path specified!");	 	
	 	}
    },    
    /* 
     * Single function for GET and POST ajax calls Asynchronous
     * @param url: service url
     * @param jsonObj:  request JSON object
     * @param reqType:  type of request
     * @param callback:  function to execute after ajax call     
     * @param timeout: set timeout interval
     * @param error:  custom error handling function when ajax fails
     */
    getServiceResponseAsync : function (url,jsonObj,reqType,callback,timeOut,error)
    { 	                 
		{
			var ajaxData = null;
			var errorFunct = null;
			if(error != null)
			{
				errorFunct = error;				
			}
			
			if(url != "" && reqType != "")
			{				
				ajaxData = {
						  url: url,
						  type: reqType,
						  cache:false,
						  global: false,
						  dataType: "json",
						  async: true,
						  contentType: "application/json; charset=UTF-8",
						  crossDomain: true,
						  headers: {"Accept":"application/json; charset=UTF-8"},
						  error: function(jqXHR, textStatus, errorThrown) {
							  if(errorFunct != null) {
								  errorFunct(jqXHR, textStatus, errorThrown);
							  }
						  },
						  success: function(data) {
							  if( data == 'sessionTimeOut'){
									document.location.href = 'http://www.insight.com/insightweb/login';
								}else{
									if(callback != null)
									{
										callback(data);
									}
									else {
										return data;
									}
								}  
						  }
				};
				//handle POST data
				if(reqType=="POST") {
					ajaxData.data = (jsonObj);
				}	
				//set time out
				if(timeOut != null) {
					ajaxData.timeout = timeOut;
				}
				var xhr = $.ajax(ajaxData);
				
				if(callback == null)
					return xhr;
			}
			else
				alert("Please enter a URL AND JSON string to post.");
		}
		
		return ajaxData;
    },
		/* 
		 * To get JQUERY templates from default en_US
		 */
	getContent : function (url, language, path)
    {         
		//disable cache locally for development
	 	var cacheFlag = true;
	 	/*if(window.location.href.indexOf("localhost") >= 0)
	 	{
	 		cacheFlag = false;	 		
	 	}*/
		
		var strReturn;
	 	
	 	if (path != null) {
	 		if(language !=null  && language.length == 5 && language != Insight.envConfigs.defaultLocale)
	 		{
	 			url = Insight.envConfigs.assetsurl.substring(0,Insight.envConfigs.assetsurl.length-5)+language+"/www/" + path;
	 		}
	 		else {
	 			url = Insight.envConfigs.assetsurl.substring(0,Insight.envConfigs.assetsurl.length-5)+Insight.envConfigs.defaultLocale+"/www/"  + path + "?cache=" + Insight.cacheSessionId;
	 		}
	 	}	 		
	 	
		$.ajax({ url:url, success:function(data){
				if( data == 'sessionTimeOut'){
					document.location.href = 'http://www.insight.com/insightweb/login';
				}else{
					strReturn = data;
				}
			},
			//error: function(jqXHR, textStatus, errorThrown) {alert(errorThrown);},
			async:false, cache:cacheFlag });
		return strReturn;
    },       
    /* 
     * To get content or labels from CMS 
     */
    getMessageContent : function (url, language, path)
    {         
    	//disable cache locally for development
	 	var cacheFlag = true;
	 	/*if(window.location.href.indexOf("localhost") >= 0)
	 	{
	 		cacheFlag = false;	 		
	 	}*/
    	
        var strReturn;
        
        if (path != null)
        {
        	//remove /jcr:content/jcr:data.txt if included in url
            if(path.indexOf("/jcr:content/jcr:data.txt") > 0) 
            {
            	path = path.replace("/jcr:content/jcr:data.txt","");
            }
            if((window.location.href.indexOf("localhost") >= 0) || (window.location.href.indexOf("-preview") >= 0)){
            	url = url + "?path=" + Insight.envConfigs.cmsServer.substring(Insight.envConfigs.cmsServer.length-6,Insight.envConfigs.cmsServer.length)  + path + "&cache=" + Insight.cacheSessionId;
            }else{
            	url = Insight.envConfigs.cmsServer.substring(Insight.envConfigs.cmsServer.length-6,Insight.envConfigs.cmsServer.length)  + path;
            }        	
        }        
        $.ajax({ url:url, 
        	success:function(data){strReturn = data;},
        	//error: function(jqXHR, textStatus, errorThrown) {alert(errorThrown);}, 
        	async:false,  cache:cacheFlag});

        return strReturn;
    },    
    /* 
     * Single function for GET and POST ajax calls 
     * INPUT: url = Service URL
     * 		  jsonObj = "" or null in case of GET call
     * 					JSON request object as String for POST call
     * 		  reqType = "GET" or "POST"
     * OUTPUT: strReturn = JSON Response Object
     *
     */
    getServiceResponse : function (url,jsonObj,reqType,cacheFlag,callback)
    { 	                   
		{
			
			var strReturn = null;
			if(url != "" && reqType != "")
			{
				if(reqType=="POST")
				{
					$.ajax({
						  url: url,
						  type: "POST",
						  data: (jsonObj),
						  cache:false,
						  global: false,
						  dataType: "json",
						  async: false,
						  contentType: "application/json; charset=UTF-8",
						  crossDomain: true,
						  headers: {"Accept":"application/json; charset=UTF-8"},
						 // error: function(jqXHR, textStatus, errorThrown) {alert(errorThrown);},
						  success: function(data) {
							  if( data == 'sessionTimeOut'){
									document.location.href = 'http://www.insight.com/insightweb/login';
								}else{
									if(callback != null)
									{
										callback(data);
									}
									else {
										strReturn = data;
									}
								}  
							  }						  
					});
				}
				else if(reqType=="GET")
				{
						$.ajax({
							url: url,
							type: "GET",
							dataType: "json",
							cache:false,
							async: false,
							contentType: "application/json; charset=UTF-8",
							crossDomain: true,
							headers: {"Accept":"application/json; charset=UTF-8"},
							//error: function(jqXHR, textStatus, errorThrown) {alert(errorThrown);},
							success: function(data) {
								if( data == 'sessionTimeOut'){
									document.location.href = 'http://www.insight.com/insightweb/login';
								}else{
									if(callback != null)
									{
										callback(data);
									}
									else {
										strReturn = data;
									}
								}
							}							
						});
				}				
				else
				{
					alert("getServiceResponse :: Invalid Request Type.");		
				}
				if (strReturn)
				{
					 
					return strReturn;
				}
			
			}
			else
				alert("Please enter a URL AND JSON string to post.");
		}
    },
    
    isAPAC : function (orgId) {
		var flag = false;
		if (orgId == '6100' || orgId == '6200' || orgId == '6300' || orgId == '6500' || orgId  == '6600') {
			flag = true;
		} else {
			flag = false;
		}
		return flag;
	},
    
    /* 
     * Custom function for passing all the params needed for making an ajax call 
     
     *
     */
    getCustomServiceResponse : function (url,reqType,jsonObj,globalFlag,dataType,asyncFlag,contentType,crossDomainFlag,headersObj,callbackfn)
    { 	                   
    	if(url!=null && reqType!=null && jsonObj!=null && globalFlag!=null && dataType!=null && asyncFlag!=null && contentType!=null && crossDomainFlag!=null && headersObj!=null && callbackfn!=null)
		$.ajax({
			  url: url,
			  cache:false,
			  type: reqType,
			  data: (jsonObj),
			  global: globalFlag,
			  dataType: dataType,
			  async: asyncFlag,
			  contentType: contentType,
			  crossDomain: crossDomainFlag,
			  headers: headersObj,
			  //error: function(jqXHR, textStatus, errorThrown) {alert(errorThrown);},
			  success: function(data) {callbackfn(data);}			  
		});
    	else
    		{
    		alert("Please check all parameters.");    		
    		}
    },
    getCustomServiceResponseTimeOut : function (url,reqType,jsonObj,globalFlag,dataType,asyncFlag,contentType,crossDomainFlag,headersObj,timeOut,callbackfn)
    { 	                   
    	if(url!=null && reqType!=null && globalFlag!=null && dataType!=null && asyncFlag!=null && contentType!=null && crossDomainFlag!=null && headersObj!=null && callbackfn!=null && timeOut!=null)
		$.ajax({
			  url: url,
			  cache:false,
			  type: reqType,
			  data: (jsonObj),
			  global: globalFlag,
			  dataType: dataType,
			  async: asyncFlag,
			  timeout:timeOut,
			  contentType: contentType,
			  crossDomain: crossDomainFlag,
			  headers: headersObj,
			  success: function(data) {callbackfn(data);},
		      error: function (data, status)
		                      { if(status == "timeout")
		                          { alert("timeout"); }
		                      else {
		                    	  
		                    	  alert("Request is failed! Please try again later.");
		                      }
		                      }
		});
    	else
    		{
    		alert("Please check all parameters.");    		
    		}
    },
    /* To get the domain URL */
    getRootURL : function ()
    {         
    	var rootURL = "";
    	var baseURL = location.href;

    	if (location.protocol == "http:")
    	{
    		rootURL = baseURL.substring(0, baseURL.indexOf('/', 7));
    	}
    	else
    	{
    		rootURL = baseURL.substring(0, baseURL.indexOf('/', 8));
    	}

    	return rootURL;
    },
    /*
     * Function to Get Current Time.
     * */
    getCurrentTimestamp : function ()
    {         
    	var tstmp = new Date();
    	return tstmp.getTime();
    },
    /*
     * Function to Get convert map to array on top level.
     * */
    /*convertToArray : function (mapData)
    {         
    	var mapArr = [];
    	if(mapData!= null && mapData != undefined && mapData !=""){
    		$.each(mapData, function(index,value){
    			mapArr.push(value);
        	});	
    	} 
    	return mapArr;
    },*/
    getRequestJsonObject : '',
    isFromSearch: false,
    /* 
     * Wrapper function for $.tmpl to render template with data into the specified selector 
     * INPUT: templateName = Name of the template ( $.template("templateName", templateStr); )
     * 		  templateData = Data to be rendered into the template.
     * 		  divId = Selector ID where the template has to be rendered.
     */
    renderTemplate: function(templateName, templateData, divId) {
    	if(templateData != null)
    	{
    		if(Insight.sessionManager) {
    			clearInterval(Insight.sessionManager);
	    		InsightCommon.startSessionManager();
	    	}
    		//note: there is no need to extend global variables anymore like here.  Just define global variables and jquery templates will have access to them
    		templateData = $.extend(templateData, {assetsurl:Insight.envConfigs.assetsurl},{assetsurllocale:Insight.envConfigs.assetsurllocale},{fileServer:Insight.envConfigs.fileServer},{cmsServer:Insight.envConfigs.cmsServer});
            templateData = $.extend(templateData, { dataArrayIndex: function(item, arrayObj) { return getArrayIndex(item, arrayObj); } });
            templateData = $.extend(templateData, { dataArrayCount: function(arrayObj) { return getArrayCount(arrayObj); } });
            templateData = $.extend(templateData, { checkPropertyInArray: function(property, value, arrayObj) { return checkPropertyInArray(property, value, arrayObj); } });
            $.tmpl(templateName, templateData).appendTo(divId);           
            
            //fix IE9 table issue with cell spaces
            if($.browser.msie && $.browser.version == '9.0')
            {            	            	
            	var IE9TableIssueFix = new RegExp('\/td>[ \t\r\n\v\f]*<td', 'g');
            	
            	$(divId).find("tr").each(function(i) {     
            		$(this).html($(this).html().replace(IE9TableIssueFix, '\/td><td'));
            	});
            }
    	}
    },
   formatTelNumber: function  (phoneField)
    {
	   //format phone number based on locales
	   /*if(Insight.locale.indexOf("US")){
		   ...
	   }*/
	   //default to US format for now
	   if(Insight.apac != true) {     	//do format for for APAC
	   $(phoneField).mask("999-999-9999");
   	   }	   
    },
    
    formatTelNumberWithExtension:function(phoneField){
    	if(Insight.apac != true) {     	//do format for for APAC
    	 $(phoneField).mask("(999)999-9999? x99999");
    	}
    },
    
    validateEmail:function (email){
    	// contributed by Scott Gonzalez: _http://projects.scottsplayground.com/email_address_validation/
    				return /^([^\s@?=!/]+@[^\s@?=!/]+\.[^\s@]+)$/i.test(email);
    },
    
    validateZip:function (zipcode){
    	if (Insight.locale == null || zipcode == null || zipcode == '' || Insight.locale == '')
    	{
    		return false;
    	}
    	else if (Insight.locale == 'en_CA' && zipcode.match(/^[a-z][0-9][a-z]\s*?[0-9][a-z][0-9]$/i))
    	{
    		return true;
    	}
    	else if (Insight.locale == 'en_US' && zipcode.match(/^(\d{5})(-\d{4})?$/))
    	{
    		return true;
    	}
    	else if(Insight.locale == 'fr_CA' && zipcode.match(/^[a-z][0-9][a-z]\s*?[0-9][a-z][0-9]$/i)){
    		return true;
    	}
    	else if(Insight.apac == true) {     	//do not validate zip code for APAC
    		return true;
    	}
    	else {
    		return false;
    	}    	
    },
    
    formatZip:function (zipcode){
    	if (zipcode != null && Insight.locale == 'en_CA' && zipcode.length == 6)
    	{
    		return zipcode.substr(0, 3) + " " + zipcode.substr(3);
    	}
    	else{
    		return zipcode;
    	}
    },
    
    validatePassword:function(passwordVal,div) {
    	var A = $(passwordVal).val();
		var G = new Array();
		var typePwd = new Array();
		/*
		 * G[0]="<font class='loginMessage' size='1'><b>Invalid Password</b></font>";
		 * G[1]="<font class='loginMessage' size='1'><b>Mediocre</b></font>";
		 * G[2]="<font class='loginMessage' size='1'><b>Strong</b></font>";
		 * G[3]="<font class='loginMessage' size='1'><b>Strongest</b></font>";
		 * G[4]="<font class='loginMessage' size='1'><b>Begin Typing</b></font>";
		 */

		G[0] = "<table width='100%'><tr><td><table cellpadding=0 cellspacing=2><tr><td style='height:4px' width=120 bgcolor=tan></td></tr></table></td><td> &nbsp;&nbsp;<b>Invalid Password</b></td></tr></table>";
		G[1] = "<table width='100%'><tr><td><table cellpadding=0 cellspacing=2><tr><td style='height:4px' width=48 bgcolor=#99ff33></td><td height=4 width=72 bgcolor=tan></td></tr></table></td><td> &nbsp;&nbsp;<b>Mediocre</b></td></tr></table>";
		G[2] = "<table width='100%'><tr><td><table cellpadding=0 cellspacing=2><tr><td style='height:4px' width=80 bgcolor=#00cc33></td><td height=4 width=40 bgcolor=tan></td></tr></table></td><td> &nbsp;&nbsp;<b>Strong</b></td></tr></table>";
		G[3] = "<table width='100%'><tr><td><table cellpadding=0 cellspacing=2><tr><td style='height:4px' width=120 bgcolor=#009900></td></tr></table></td><td> &nbsp;&nbsp;<b>Strongest</b></td></tr></table>";
		G[4] = "<table width='100%'><tr><td><table><tr><td style='height:4px' width=120 bgcolor=tan></td></tr></table></td><td> &nbsp;&nbsp;<b>Begin Typing</b></td></tr></table>";

		typePwd[0]="Invalid Password";
		typePwd[1]="Mediocre";
		typePwd[2]="Strong";
		typePwd[3]="Strongest";
		typePwd[4]="Begin Typing";
		var E = 0;
		var C = 0;
		var I = 0;
		var F = 0;
		var B = false;
		var D = false;
		var H = 0;
		var pwdValue;
		if (A.length == 0 || !A.length)
			C = -1;
		else {
			if (A.length < 8 || A.length > 16)
				H = 1;
			else {
				 if(A.match(/[^a-zA-Z0-9\\$\\@\\!\\#\\%\\.\\_\\-]/))
				//if (A.match(/[^a-z0-9\\$\\@\\!\\#\\%\\.\\_\\-]/))
					C = -2;

				else {
					if (A.match(/[a-z]/)) {
						C = (C + 1);
						B = true;
					}
					
					 if(A.match(/[A-Z]/)){ C=(C+1);B=true; }
					 
					if (A.match(/\d+/)) {
						C = (C + 1);
						D = true;
					}
					if (A.match(/.[!,@,#,$,%,.,_,-]/)) {
						C = (C + 1);
						D = true;
					}
					C = (C + 1);
					B = true;
				}
			}
		}
		if (C == -1) {
			I = G[4];
			pwdValue = typePwd[4]
		} else {
			if (C == -2 || (!B) || (!D)) {
				I = G[0];
				pwdValue=typePwd[0];
			} else {
				if (C < 2 || H == 1) {
					I = G[0];
					pwdValue= typePwd[0];
				} else {
					if (C >= 2 && C < 3) {
						I = G[1];
						pwdValue = typePwd[1];
					} else {
						if (C == 3) {
							I = G[2];
							pwdValue=typePwd[2]
						} else {
							I = G[3];
							pwdValue=typePwd[3];
						}
					}
				}
			}
		}

		document.getElementById(div).innerHTML = (I);
		$("#"+div).attr('name',pwdValue);
		// $("#pwdStrengthMsg").text(I);
		// alert(I);
    },
    
    inactivateLinks:function(){
    	
    	$('#footerContent a').each(function() {
    	$(this).click(function(){
    		InsightCommon.inactiveAllLinks(this);
    	});
    	    $(this).attr('name',this);
    	    $(this).removeAttr('href');
    	   });
    	$('#webGroupIds a').each(function() {
    	    $(this).click(function(){
        		InsightCommon.inactiveAllLinks(this);
        	});
    	    $(this).attr('name',this);
    	    $(this).removeAttr('href');
    	   });
    	$('#outerHeaderContentDiv a').each(function() {
    	    $(this).click(function(){
        		InsightCommon.inactiveAllLinks(this);
        	});
    	    $(this).attr('name',this);
    	    $(this).removeAttr('href');
    	   });
/*    	$('#outerHeaderContentDiv span a').each(function() {
    	    $(this).click(function(){
        		InsightCommon.inactiveAllLinks(this);
        	});
    	    $(this).attr('name',this);
    	    $(this).removeAttr('href');
    	   });*/
    	/*
    	 * remove the public sector link
    	 */
    	$("#searchBtn-footer").attr('onclick','');
    	$('#megaanchor1').off('mouseenter');
    	$('#megaanchor2').off('mouseenter');
    	$('#megaanchor3').off('mouseenter');
    	$('#megaanchor-locales').off('mouseenter');
    	$('#megaanchor7').off('mouseenter');
    	$("#megaanchor6").off('mouseenter');
    	$("#megaanchor5").off('mouseenter');
    	$("#megaanchor4").off('mouseenter');
    	$("#searchText-footer").removeAttr('onkeypress');
    	$("#megaanchor7").hide();
    	
    	
    	/* for few specific links remove the href */
    	var anchorLinks = new Array();
    	anchorLinks =['#insightPublicSectorCloud','#helpLinkId a','#headerOrderTracking a','megaanchor7'];
    	
    	$.each(anchorLinks, function(i,v){
    		var ipsuserHref = $(v).attr('href');	
    		$(v).attr('onclick',"InsightCommon.inactiveAllLinks('"+ipsuserHref +"')");
    		$(v).attr('name',ipsuserHref);
    		$(v).attr('href',"#");
    	});
    	
    	// inactivate the product description link
    	$(".materialDesc").each(function(){
    		var text = $(this).html();
    		$(this).parent().html(text);
    	});
    },
    
	inactiveAllLinks:function(currentHref)
	{
		//alert(currentHref);
		//var currentHrefValue= $("#"+currentHref).attr("href")
		//alert(currentHref.attr("name"));
		var confirmBoxReturnValue = confirm("Leaving the checkout process will remove InsightCloud products from your cart");
		if(confirmBoxReturnValue){
			/* 
			 * reload the page or soemthing like that!
			 */
			var clearfromcart = InsightCommon.getServiceResponse('/insightweb/transaction/clearcart', '',"GET",false);
			document.location = currentHref;
		}
		else{
			return;
		}

	},
    
    /* *** function to return an array of variable and associated value *** */
    /*
     * e.g. URL=http://www.example.com/?me=myValue&name2=SomeOtherValue
     * 
     * To get a value of first parameter you would do this:
     * var first = getUrlVars()["me"];
     * 
     * To get the second parameter you would do this: 
     * second = getUrlVars()["name2"];
     * 
     * This function is taken from http://jquery-howto.blogspot.com/2009/09/get-url-parameters-values-with-jquery.html
     * */
    getUrlVars : function()
    {    
    	var vars = [], hash;    
    	var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');    
    	for(var i = 0; i < hashes.length; i++)    
    	{        
    		hash = hashes[i].split('=');        
    		vars.push(hash[0]);        
    		vars[hash[0]] = hash[1];    
    	}    
    	return vars;
    },
    formatCurrency: function(num, locale, displayUnit)
    {    	
    	var currentRegion;
    	var mask;
    	if(locale != null) {
    		 if(locale=="en_CN"){
                 currentRegion = "zh-CN";
         }else{
    		currentRegion = locale.replace('_','-');
         }
    	}
    	else {
            if(Insight.locale=="en_CN"){
                    currentRegion = "zh-CN";
            }else{
            currentRegion = Insight.locale.replace('_','-');
            }
    	}
    	var spanWrapper = $("<span></span>");
    	spanWrapper.html(num);
    	spanWrapper.formatCurrency({region:currentRegion});
    	mask = spanWrapper.html();
    	spanWrapper.remove();
    	
    	if(displayUnit != null  && displayUnit == true)
    	{
    		if(locale.indexOf("US") >= 0) 
    		{
    			mask = "<strong>USD</strong> " + mask;
    		}
    		else if(locale.indexOf("CA") >= 0)
    		{
    			mask = "<strong>CAD</strong> " + mask;
    		}
    		
    		else if(locale.indexOf("GB") >= 0) 
    		{
    			mask = "<strong>GBP</strong> " + mask;
    			
    		}
    		else if(locale.indexOf("ES") >= 0) 
    		{
    			mask = "<strong>ESP</strong> " + mask;
    			
    		}
    		else if(locale.indexOf("FR") >= 0) 
    		{
    			mask = "<strong>EUR</strong> " + mask;
    			
    		}
    		else if(locale.indexOf("NZ") >= 0) 
    		{
    			mask = "<strong>NZD</strong> " + mask;
    			
    		}
    		else if(locale.indexOf("AU") >= 0) 
    		{
    			mask = "<strong>AUD</strong> " + mask;
    			
    		}
    		else if(locale.indexOf("HK") >= 0) 
    		{
    			mask = "<strong>HKD</strong> " + mask;
    			
    		}
    		else if(locale.indexOf("CN") >= 0) 
    		{
    			mask = "<strong>CNY</strong> " + mask;  
    			
    		}
    		else if(locale.indexOf("SG") >= 0) 
    		{
    			mask = "<strong>SGD</strong> " + mask;
    			
    		}
    		/**.. more locales here ..**/
    	}
    	
    	return mask;       	 
    },
    
    getDateFormatLabelsByLocale:function(){
    	var locale = Insight.locale;
  	  
 	    var dateFormat = "MM-dd-yyyy";
 	
 		if(locale.indexOf("US") >= 0) 
 		{
 			dateFormat = "MM-dd-yyyy";
 		}
 		else if(locale.indexOf("CA") >= 0)
		{
 			dateFormat = "dd-MM-yyyy";
		}
 		else if(locale.indexOf("CN") >= 0) 
		{
 			dateFormat = "yyyy-MM-dd";
			
		}else {
			dateFormat = "dd-MMM-yyyy";
		}    	
 		return dateFormat;
    },
    
    getListOfDateFormats:function(labels){
    	var object = {
    		    US : {
    		        name : 'MM-dd-yyyy',
    		        value : labels.usaDateFormat
    		    },
    		    CA : {
    		        name : 'dd-MM-yyyy',
    		        value : labels.canadaDateFormat
    		    },
    		    CN : {
    		        name : 'yyyy-MM-dd',
    		        value : labels.chinaDateFormat
    		    },
    		    NA : {
    		        name : 'dd-MMM-yyyy',
    		        value : labels.internationalDateFormat
    		    }
    		};    	
    	return object;
    },
    
    
    getCurrencyCode: function()
    {
    	  var locale = Insight.locale;
    	  
    	   var mask = "USD";
    	
    		if(locale.indexOf("US") >= 0) 
    		{
    			mask = "USD";
    		}
    		else if(locale.indexOf("CA") >= 0)
    		{
    			mask = "CAD";
    		}
    		
    		else if(locale.indexOf("GB") >= 0) 
    		{
    			mask = "GBP";
    			
    		}
    		else if(locale.indexOf("ES") >= 0) 
    		{
    			mask = "ESP";
    			
    		}
    		else if(locale.indexOf("FR") >= 0) 
    		{
    			mask = "EUR";
    			
    		}
    		else if(locale.indexOf("NZ") >= 0) 
    		{
    			mask = "NZD";
    			
    		}
    		else if(locale.indexOf("AU") >= 0) 
    		{
    			mask = "AUD";
    			
    		}
    		else if(locale.indexOf("HK") >= 0) 
    		{
    			mask = "HKD";
    			
    		}
    		else if(locale.indexOf("CN") >= 0) 
    		{
    			mask = "CNY";
    			
    		}
    		else if(locale.indexOf("SG") >= 0) 
    		{
    			mask = "SGD";
    			
    		}
    		return mask;
    		/**.. more locales here ..**/
    	
    	
    },
    
    bundlePrice: function(bundleObj)
    {    	
    	var bundleTotal = 0.00;
    	
    	$.each(bundleObj.lineItems, function(arrayKey, arrayItem) {
    		bundleTotal = bundleTotal + arrayItem.price;  		
 
        });   	

    	return InsightCommon.formatCurrency(bundleTotal);       	 
    },
    
    
    formatDateByLocale: function(serviceDate, changeMonthLabel){
		var formatedDate = serviceDate;
   		var from=formatedDate.indexOf("-")+1;
		var to =formatedDate.lastIndexOf("-");
		formatedDate = InsightCommon.replaceSubStringBetweenIndex(from, to, changeMonthLabel, formatedDate);
    	return formatedDate;
    },
    
    getMonthFromFormatedDate: function (serviceDate){
    	
		formatedDate = serviceDate;
		var from=formatedDate.indexOf("-")+1;
		var to =formatedDate.lastIndexOf("-");
		var source = formatedDate.substring(from,to);
    	return source;
    },
    
    replaceSubStringBetweenIndex: function(start, end, what, formatedDate) {
        return formatedDate.substring(0, start) + what + formatedDate.substring(end);
    },
    
    bundleTotalPrice: function(bundleObj)
    {    	
    	var bundleTotal = 0.00;
    	
    	$.each(bundleObj.lineItems, function(arrayKey, arrayItem) {
    		bundleTotal = bundleTotal + arrayItem.price;  		
 
        });   	
    	bundleTotal = bundleObj.quantity * bundleTotal;
    	return InsightCommon.formatCurrency(bundleTotal);       	 
    },
    
    showTooltip: function(data,e,pos,wid){

    	var left = $(e).offset().left;
        var top = $(e).offset().top;
        var imgClass = "tailRight";
        
        //tooltip on the left side of element
        if(pos != null && pos == 'left') {
        	left = left-220; 
        	imgClass = "tailLeft";
    	}
        //tooltip on the right side of element
    	else {
    		left = left+ $(e).width(); 
    	}
        
        top = top + $(e).height();
        
    	$('body').append('<div class="commonTooltip" id="tooltip"><div class="'+imgClass+'"></div>'+data +'</div>');		
    	
    	//set custom width
    	if(wid != null) {
    		$('div#tooltip').css("width",wid);
    	}
    			
    	$('#tooltip').show();
    	$('#tooltip').css("top", top+ "px");
    	$('#tooltip').css("left",left+ "px");
    	
    },
    closeTooltip: function(e){
    	$('div#tooltip').remove();	
    },

    showLoading:function(){
		$("#loading").dialog({modal:false, width: '250',height:'100',resizable: false, position: 'relative', draggable:false, autoOpen:false});
		$("#loading").show();
		$(".ui-dialog").css({zIndex:'99999999'});	
		$("#loading").dialog("open");
	    $(".ui-dialog-titlebar").hide();
	    clearTimeout(showLoadingTimer);
	    showLoadingTimer = setTimeout("InsightCommon.hideLoading();",5000);
	},

	hideLoading:function(){
	   setTimeout( function() {
		 $("#loading").dialog("close");
		 $("#loading").dialog("destroy");
		 $(".ui-dialog-titlebar").show();
       }, 200 );
	},
	
	showLoadingWithoutTimer:function(){
    	$("#loading").dialog({modal:false, width: '250',height:'100',resizable: false, position: 'relative', draggable:false, autoOpen:false});
		$("#loading").show();
		$(".ui-dialog").css({zIndex:'99999999'});	
		$("#loading").dialog("open");
		$(".ui-dialog-titlebar").hide();
	},

	hideLoadingWithoutTimer:function(){
		 $("#loading").dialog("close");
		 $("#loading").dialog("destroy");
		 $(".ui-dialog-titlebar").show();
	},
	
	getCookie:function(c_name){
		var i,x,y;
			var domCookie;
			var valList;
	
				if($.cookie('cr_name')!=null){
					domCookie = $.cookie('cr_name');
					valList = domCookie.split(";");
					
					for (i=0;i<valList.length;i++)
					  {
					  x=valList[i].substr(0,valList[i].indexOf("="));
					  y=valList[i].substr(valList[i].indexOf("=")+1);
					  x=x.replace(/^\s+|\s+$/g,"");
					  if (x==c_name)
					    {
						  return unescape(y);
					    }
					  }
				}
		
	},
	selectEmailValidator:function(className){
		var ele = className;
		$(ele).each(function() {
			if(!InsightCommon.validateEmail($(this).val())){
				$(this).addClass("errorField");
			}
		});
		
	},	
	selectDropDownWideIEFix:function(className){
	
if($.browser.msie && parseInt($.browser.version, 10) < 9){
            
            var element = className;
            
            $(element).each(function() {
            	//$(this).data("maxwidth",$(this).css("width").replaceAll("px",""));
            	$(this).data("maxwidth",$(this).outerWidth());
            	
            });


            $(element).mousedown(function() {
                        	
                                              
                  $("body").append("<div class=selectclone style=position:absolute;visibility:hidden></div>");
                                    
                  $(this).clone().css("width","auto").appendTo(".selectclone");
                   
                  if($(".selectclone select").width() <= $(this).data("maxwidth")){                	  
                	  $(this).width($(this).data("maxwidth"));

                  }else{

                	  $(this).width($(".selectclone select").width());

                  }

                  $(".selectclone").remove();


            
            });
            
            $(element).change(function() {
                  $(this).width($(this).data("maxwidth"));
            });
     
            $(element).blur(function() {
                  $(this).width($(this).data("maxwidth"));
            });                  

		}
	},
	
	poll:function(condition,action,loop,interval){
	/* POLLING FUNCTION */
	
		if(!interval){
			interval = 100;
		}
		$.doTimeout(eval(interval), function(){
			if (eval(condition)) {
				eval(action);
				return false;
			}
			eval(loop);
			return true;
		});

	/* example usage
	InsightCommon.poll(
		'$("#someinput").val() == 1',
		'console.log("Condition met! Polling now stopped.")',
		//'console.log("Polling..."+(new Date()).getTime()+"")',
		''
	);
	*/
	/* POLLING FUNCTION */	
	},
	getDomainURL: function(){         
        var domainURL = location.protocol + '//' + location.host;        
        if(domainURL.indexOf("loginas-")!=-1){
        domainURL= domainURL.replace("loginas-","");  
        } else if(domainURL.indexOf("loginas.")!=-1){
        domainURL= domainURL.replace("loginas.","www.");
        }         
        return domainURL;  
    },
	
	getURLParameter:function(name,url) {

	    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(url)||[,""])[1].replace(/\+/g, '%20'))||null;

	},
	formatUsageDate:function(month) {
 
	/*
     * Please do not remove this function. It is needed to display shopping cart template properly on all pages
     * */		
		if(InsightNavigation.globalLabels!="" && InsightNavigation.globalLabels!=undefined && InsightNavigation.globalLabels!=null){
			var month_names = new Array(InsightNavigation.globalLabels.monthNamesJan,
				InsightNavigation.globalLabels.monthNamesFeb,
				InsightNavigation.globalLabels.monthNamesMar,
				InsightNavigation.globalLabels.monthNamesApr,
				InsightNavigation.globalLabels.monthNamesMay,
				InsightNavigation.globalLabels.monthNamesJun,
				InsightNavigation.globalLabels.monthNamesJul,
				InsightNavigation.globalLabels.monthNamesAug,
				InsightNavigation.globalLabels.monthNamesSep,
				InsightNavigation.globalLabels.monthNamesOct,
				InsightNavigation.globalLabels.monthNamesNov,
				InsightNavigation.globalLabels.monthNamesDec
			);
		} else {
			var globalLabelsObj = JSON.parse(InsightCommon.getMessageContent ("/insightweb/getCmsContent",Insight.locale,"http://www.insight.com/messages/header/globalLabels.txt"));
			
			var month_names = new Array(globalLabelsObj.labels.monthNamesJan,
				globalLabelsObj.labels.monthNamesFeb,
				globalLabelsObj.labels.monthNamesMar,
				globalLabelsObj.labels.monthNamesApr,
				globalLabelsObj.labels.monthNamesMay,
				globalLabelsObj.labels.monthNamesJun,
				globalLabelsObj.labels.monthNamesJul,
				globalLabelsObj.labels.monthNamesAug,
				globalLabelsObj.labels.monthNamesSep,
				globalLabelsObj.labels.monthNamesOct,
				globalLabelsObj.labels.monthNamesNov,
				globalLabelsObj.labels.monthNamesDec);
			
	}	
		return month_names[month];
	},
	
	startSessionManager: function () {		
		Insight.sessionManager = setInterval(function(){document.location.href = 'http://www.insight.com/insightweb/j_spring_security_logout';}, Insight.sessionInterval);

	},
	
	/*Get lables from CMS */
	getGlobalLabels : function()
	{
		$.when((Insight.globalLabels == null) ? InsightCommon.getMessageContent (InsightNavigation.messageUrl ,Insight.locale,"http://www.insight.com/messages/header/globalLabels.txt") : Insight.globalLabels)
		.then(function(globalLabelsRes){
			if(Insight.globalLabels == null || Insight.globalLabels == "" || Insight.globalLabels == undefined){
				Insight.globalLabels = globalLabelsRes;	
				Insight.globalLabels = JSON.parse(Insight.globalLabels);
				Insight.globalLabels = Insight.globalLabels.labels;
			}
		});
	},
	
	/* Add Date format for APAC*/
	datePickerInit : function(){
		var addDateFormat=null;
		if(Insight.apac){
			if(Insight.locale=='zh_CN' || Insight.locale=='en_CN'){
				addDateFormat={
    					dateFormat : 'yy-mm-dd',
    					altFormat: 'mm/dd/yy',
    					altField: "#altDate"
    			};
			}else
			{
				addDateFormat={
    					dateFormat : 'dd-M-yy',
    					altFormat: 'mm/dd/yy',
    					altField: "#altDate"
    			};
			}	
			
		}else
		{	
			addDateFormat={
					dateFormat : 'dd-M-yy',
					altFormat: 'mm/dd/yy',
					altField: "#altDate"
			};
		}
		/* initialisation for the jQuery UI date picker plugin to get content from CMS */
		$.datepicker.content = {
    			closeText: Insight.globalLabels.closeText,
    			prevText: Insight.globalLabels.prevText,
    			nextText: Insight.globalLabels.nextText,
    			currentText: Insight.globalLabels.currentText,
    			monthNames: [Insight.globalLabels.monthNamesJan,Insight.globalLabels.monthNamesFeb,Insight.globalLabels.monthNamesMar,Insight.globalLabels.monthNamesApr,Insight.globalLabels.monthNamesMay,Insight.globalLabels.monthNamesJun,
    			             Insight.globalLabels.monthNamesJul,Insight.globalLabels.monthNamesAug,Insight.globalLabels.monthNamesSep,Insight.globalLabels.monthNamesOct,Insight.globalLabels.monthNamesNov,Insight.globalLabels.monthNamesDec],
    			monthNamesShort: [Insight.globalLabels.monthNamesJanShort,Insight.globalLabels.monthNamesFebShort,Insight.globalLabels.monthNamesMarShort,Insight.globalLabels.monthNamesAprShort,Insight.globalLabels.monthNamesMayShort,Insight.globalLabels.monthNamesJunShort,
    	    			             Insight.globalLabels.monthNamesJulShort,Insight.globalLabels.monthNamesAugShort,Insight.globalLabels.monthNamesSepShort,Insight.globalLabels.monthNamesOctShort,Insight.globalLabels.monthNamesNovShort,Insight.globalLabels.monthNamesDecShort],
    			dayNames: [Insight.globalLabels.dayNamesSun, Insight.globalLabels.dayNamesMon, Insight.globalLabels.dayNamesTue, Insight.globalLabels.dayNamesWed, Insight.globalLabels.dayNamesThu, Insight.globalLabels.dayNamesFri, Insight.globalLabels.dayNamesSat],
    			dayNamesShort: [Insight.globalLabels.dayNamesSunShort, Insight.globalLabels.dayNamesMonShort, Insight.globalLabels.dayNamesTueShort, Insight.globalLabels.dayNamesWedShort, Insight.globalLabels.dayNamesThuShort, Insight.globalLabels.dayNamesFriShort, Insight.globalLabels.dayNamesSatShort],
    			dayNamesMin: [Insight.globalLabels.dayNamesSunMin, Insight.globalLabels.dayNamesMonMin, Insight.globalLabels.dayNamesTueMin, Insight.globalLabels.dayNamesWedMin, Insight.globalLabels.dayNamesThuMin, Insight.globalLabels.dayNamesFriMin, Insight.globalLabels.dayNamesSatMin]
    		};
    		
    		$.extend($.datepicker.content,addDateFormat);
    		$.datepicker.setDefaults($.datepicker.content);
	},
	
	// function to cancel events on parent elements
	cancelEventPropagation : function(){
		if($.browser.msie){
				window.event.cancelBubble = true;
				window.event.returnValue = false;
		}
	},
	
	// prevents white spaces in text input
	preventWhiteSpace : function(event){
		var e = event || window.event;
		// replace existing white spaces (if any) in the default text
		var str = $(event.target).val();
		str = str.replace(/\s/g,'');
		$(event.target).val(str);
		
		var key = e.keyCode!=undefined ? e.keyCode : e.which;  
		if(key == 32)
			e.preventDefault();
		},
	
	// Function to format the Deploy Date
	formatDeployDate : function(cartobject,bundledate){
	    $.each(cartobject.contracts,function(index,contract){
			 $.each(contract.cartItems, function(keyItem, cartItem) {
				 if(cartItem.bundle){
			 			$.each(cartItem.lineItems, function(lineItemKey, lineItemVal) {
				   			//if(lineItemVal.showProrationDeployDateLink){
				   				var lineLevelContractStartDate = $("."+bundledate+"-"+contract.contractID+"-"+lineItemVal.materialIDKey).text();
				   				if(lineLevelContractStartDate!=null && lineLevelContractStartDate!=""){
				   					lineLevelContractStartDate = new Date(parseInt(lineLevelContractStartDate));
					   				lineLevelContractStartDate = $.datepicker.formatDate('dd-M-yy',lineLevelContractStartDate);
							 		$("."+bundledate+"-"+contract.contractID+"-"+lineItemVal.materialIDKey).text(lineLevelContractStartDate);
				   				}
						 	// }
		 				});
			 		}else{
			 			var contractStartDate = $(".currentDeployDate-"+contract.contractID+"-"+cartItem.materialIDKey).text();
			 			if(contractStartDate!=null && contractStartDate!=""){
			 				contractStartDate = new Date(parseInt(contractStartDate));
							contractStartDate = $.datepicker.formatDate('dd-M-yy',contractStartDate);
							$(".currentDeployDate-"+contract.contractID+"-"+cartItem.materialIDKey).text(contractStartDate);
			 			}
			 		}
		 	 });
		});
	},
	
	flagForBundleWithSameContractId : function(data){
		 var showAuthHelpTextForBundle = false;
		/*CR242 - Flag to set for showing helpText for AUTHORIZATION in Edit Line Level Page */
		 $.each(data.contracts, function(key, val) {
	        $.each(val.cartItems, function(keyItem, valItem) {
	        	if(valItem.bundle){
		            if(valItem.sourceContractIdsinBundleAreSame){
		            	 $.each(valItem.lineItems, function(lineKey, lineVal) {
		            		 if(lineVal.proratable && lineVal.prorationType == 'ADOBE VIP'){
		            			 showAuthHelpTextForBundle = true;
		            			 return false;
		            		 }
		            	});
		            }
	        	}
	        	
	        });
		  });
		 return showAuthHelpTextForBundle;
		/* END */  
	}
		
	};
	
	
})();


(function($){var a={},c="doTimeout",d=Array.prototype.slice;$[c]=function(){return b.apply(window,[0].concat(d.call(arguments)))};$.fn[c]=function(){var f=d.call(arguments),e=b.apply(this,[c+f[0]].concat(f));return typeof f[0]==="number"||typeof f[1]==="number"?this:e};function b(l){var m=this,h,k={},g=l?$.fn:$,n=arguments,i=4,f=n[1],j=n[2],p=n[3];if(typeof f!=="string"){i--;f=l=0;j=n[1];p=n[2]}if(l){h=m.eq(0);h.data(l,k=h.data(l)||{})}else{if(f){k=a[f]||(a[f]={})}}k.id&&clearTimeout(k.id);delete k.id;function e(){if(l){h.removeData(l)}else{if(f){delete a[f]}}}function o(){k.id=setTimeout(function(){k.fn()},j)}if(p){k.fn=function(q){if(typeof p==="string"){p=g[p]}p.apply(m,d.call(n,i))===true&&!q?o():e()};o()}else{if(k.fn){j===undefined?e():k.fn(j===false);return true}else{e()}}}})(jQuery);




/* REVERTED BACK TO THIS OLD FUNCTION */
$(function(){ 
	  InsightCommon.selectDropDownWideIEFix(".selectWide");
});


function checkPropertyInArray(property, value, arrayObj) {
    var foundMatch = false;

    $.each(arrayObj, function(arrayKey, arrayItem) {
        if (arrayItem[property] == value) {
            foundMatch = true;
            
            return false;
        }
    });

    return foundMatch;
}


function getArrayIndex(item, arrayObj) {
    var i = 0;
    var index = -1;

    $.each(arrayObj, function(arrayKey, arrayItem) {
        if (item == arrayKey) {
            index = i;
            return false;
        }
        i++;
    });

    return index;
}

function getArrayCount(arrayObj) {
    var i = 0;

    $.each(arrayObj, function(arrayKey, arrayItem) {
        i++;                
    });

    return i;
}


function getJSON(path)
{
 	var url = path;
 	var strReturn;

 	$.ajax({
    		  url: url,
    		  type: "GET",
			  dataType: "json",
      		  async: false,
	          contentType: "application/json",
	          crossDomain: true,
			  headers: {"Accept":"application/json"},
	          success: function(data) { strReturn = data; }
	});

	return strReturn;
}


function getData(path)
{
 	var url = path;
 	var strReturn;

	$.ajax({ url:url, success:function(data){strReturn = data;}, async:false });
	return strReturn;
}


function getContent(url, language, path)
{
	return InsightCommon.getContent(url, language, path);
}



function getRootURL()
{
	var rootURL = "";
	var baseURL = location.href;

	if (location.protocol == "http:")
	{
		rootURL = baseURL.substring(0, baseURL.indexOf('/', 7));
	}
	else
	{
		rootURL = baseURL.substring(0, baseURL.indexOf('/', 8));
	}

	return rootURL;
}

/* *** functions from common.js *** */

/* This function is called to validate the email address */
/* The function accepts email address and returns boolean value */
/*function validateEmail(email) {   
	 var emailPattern=/^[a-zA-Z0-9.'_-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/; 
	 if(email.match(emailPattern)) return true;
	 else 
	    return false;
} */





/*-------------------------------------------------------------------- 
 * JQuery Plugin: "EqualHeights" & "EqualWidths" 
--------------------------------------------------------------------*/

$.fn.equalHeights = function(px) {
	$(this).each(function(){
		var currentTallest = 0;
		$(this).children().each(function(i){
			if ($(this).height() > currentTallest) { currentTallest = $(this).height(); }
		});
		if (!px || !Number.prototype.pxToEm) currentTallest = currentTallest.pxToEm(); //use ems unless px is specified
		// for ie6, set height since min-height isn't supported
		if ($.browser.msie && $.browser.version == 6.0) { $(this).children().css({'height': currentTallest}); }
		$(this).children().css({'min-height': currentTallest}); 
	});
	return this;
};

// just in case you need it...
$.fn.equalWidths = function(px) {
	$(this).each(function(){
		var currentWidest = 0;
		$(this).children().each(function(i){
				if($(this).width() > currentWidest) { currentWidest = $(this).width(); }
		});
		if(!px || !Number.prototype.pxToEm) currentWidest = currentWidest.pxToEm(); //use ems unless px is specified
		// for ie6, set width since min-width isn't supported
		if ($.browser.msie && $.browser.version == 6.0) { $(this).children().css({'width': currentWidest}); }
		$(this).children().css({'min-width': currentWidest}); 
	});
	return this;
};


/*-------------------------------------------------------------------- 
 * javascript method: "pxToEm"
 --------------------------------------------------------------------*/

Number.prototype.pxToEm = String.prototype.pxToEm = function(settings){
	//set defaults
	settings = jQuery.extend({
		scope: 'body',
		reverse: false
	}, settings);
	
	var pxVal = (this == '') ? 0 : parseFloat(this);
	var scopeVal;
	var getWindowWidth = function(){
		var de = document.documentElement;
		return self.innerWidth || (de && de.clientWidth) || document.body.clientWidth;
	};	
	
	/* When a percentage-based font-size is set on the body, IE returns that percent of the window width as the font-size. 
		For example, if the body font-size is 62.5% and the window width is 1000px, IE will return 625px as the font-size. 	
		When this happens, we calculate the correct body font-size (%) and multiply it by 16 (the standard browser font size) 
		to get an accurate em value. */
				
	if (settings.scope == 'body' && $.browser.msie && (parseFloat($('body').css('font-size')) / getWindowWidth()).toFixed(1) > 0.0) {
		var calcFontSize = function(){		
			return (parseFloat($('body').css('font-size'))/getWindowWidth()).toFixed(3) * 16;
		};
		scopeVal = calcFontSize();
	}
	else { scopeVal = parseFloat(jQuery(settings.scope).css("font-size")); };
			
	var result = (settings.reverse == true) ? (pxVal * scopeVal).toFixed(2) + 'px' : (pxVal / scopeVal).toFixed(2) + 'em';
	return result;
};

/* *** end of functions from common.js *** */


function isLoggedIn(){
	if(Insight.isLoggedin){
		return true;
	}else{
		return false;
	}
}

function manageStandards(){
	document.location.href="http://www.insight.com/insightweb/search#manageStandards";
}

function quoteSafe(str,quote){
	if(str){
		if(quote == "'"){
			str = str.split("'").join("\\'");
		}else{
			str = str.split('"').join('\\"');
		}
		return str;
	}
}

function isProratedMaterial(softwareLicenseType ) {
	var isProratedMaterial = false;
	if (softwareLicenseType != null &&  (softwareLicenseType == "LT006" || softwareLicenseType == "LT008" || 
		softwareLicenseType == "LT012" || softwareLicenseType == "LT016")) {
		isProratedMaterial= true;
	} else {		
		 isProratedMaterial= false;
	}
	return isProratedMaterial;
}

function checkIfShipAddressIsInCountriesWithNoZipCodeList(shipCountry) {
	
	shipCountry = $.trim(shipCountry);
	
	if(shipCountry == "HK" || shipCountry == "hk" || shipCountry == "Hong Kong") {
		return true;
	} else {
		return false;
	}
}

//add in Object key enumeration function for IE7,8
Object.keys = Object.keys || function(o) { 
    var result = []; 
    for(var name in o) { 
        if (o.hasOwnProperty(name)) 
          result.push(name); 
    } 
    return result; 
};

function loadLabels(taxLabelList, gstLabelValue, pstLabelValue, taxLabelValue) {
	var taxLabel = "";
	if (taxLabelList != null && taxLabelList.length > 0) {
		var taxLabelKey = taxLabelList[0];	
		if (taxLabelKey == "GST") {
			taxLabel = gstLabelValue;
		} else if (taxLabelKey == "PST") {
			taxLabel = pstLabelValue;
		}
		else if (taxLabelKey == "TAX") {
			taxLabel = taxLabelValue;
		} else {
			taxLabel = taxLabelValue;
		}
	} else if (Insight.apac) {
		taxLabel = gstLabelValue;
	}
	return taxLabel;
}


function determinePostalCodeRequiredByCountryCodeSelected(countryList, countrySelected){
	 
    var shippingBillingPostalCodeZipValidationRequired = false; 
    
    $.each(countryList, function(val, text) {
    	
           if (text.countryCode == countrySelected) {
        	   
                 isZipCode = text.mandatoryZipCode;                      
                 if(isZipCode) {
                	 
                	 shippingBillingPostalCodeZipValidationRequired = true;
                 }
           }
    });
    
    return shippingBillingPostalCodeZipValidationRequired;
}


function determineProvinceRequiredByCountryCodeSelected(countryList, countrySelected){	        
	var shippingBillingStateValidationRequired = false;
    
    $.each(countryList, function(val, text) {
    	
           if (text.countryCode == countrySelected) {
        	   
                 isRegion= text.mandatoryState;                      
                 if(isRegion) {
                	 
                	 shippingBillingStateValidationRequired = true;
                 }
           }
    });
    
    return shippingBillingStateValidationRequired;
}
/*Avoid `console` errors in browsers that lack a console.*/
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

         /*Only stub undefined methods.*/
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());