/* author yvadugu
 * Custon jQuery dialog to simulate lightbox 
 * need to include insightLightBox.css to work properly
 */

(function(){
	var insightLightBox = window.insightLightBox = {
		
			styleCustomDialog : function(element) {			
				$(element).closest(".ui-dialog").wrapAll('<div class="insightCustomDialog"></div>');	
				if($(".insightCustomDialog .insightLightBoxClose").length==0){
					$(".insightCustomDialog .ui-dialog").prepend('<span class="insightLightBoxClose frameworkIcons Deep_Close_Icon"></span>');
					$(".insightLightBoxClose").click(function() {
				      $(element).dialog("close").dialog("destroy");
				    });				
				}
			}
	};
})();
