var jsonResponseObj="";
var jsonRecommendedResponseObj = "";
var messageUrl="/insightweb/getCmsContent";
var minipppWindow;
var globalProductInfoResponse;
var globalCompanyStandards;
var stockData;
var productSrchlabels =  null;
var isIpp =  false;
var isCSMiniPPP = false;
var productInfoPopUpDiv = null;
var topmostCSidValues = "";
var recommendedCSidValues = "";
var icsDomainUrl = "";
var icsPviewUrl = "";
var icsViewUrl = "";
var inventoryBlowout = "";

function getProductInfo(materialId, targetWindow, sourceWindow, isIppProduct,isFromCartRequest, isFromCS,groupName,categoryId,setId,groupId,shared,controller) {
	InsightSearch.isFromCs = isFromCS;
	InsightSearch.groupName = groupName;
	InsightSearch.categoryId = categoryId;
	InsightSearch.setId = setId;
	InsightSearch.groupId =groupId;
	InsightSearch.shared =  shared;
	InsightSearch.controller = controller;
	InsightSearch.productInfoResponse = null;
	var url = '/insightweb/getProductInfo';//$('#url').val();
	var templateName = "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/ProductInfoTemplate.html";
	var contractId = null;	
	if (sourceWindow == 'search') {
		InsightCommon.isFromSearch = false;
	} else if (sourceWindow == 'IB'){
		inventoryBlowout = "IB";
	}
	var jsonRequestObj = populateProductInfoJSONObject(decodeURIComponent(materialId),contractId,false,null, isFromCS);
	var serviceResponseObj;
	if(Insight.locale == 'en_CA' || Insight.locale == 'fr_CA'){
		//if( (Insight.webGroupPermissions!= null && $.inArray("display_sap_quantity", Insight.webGroupPermissions)>-1) || !Insight.isLoggedin) {
				var stockUrl="/insightweb/getAvalialabilityByWareHouse";
				var stockJsonRequestObj = '{"similarMaterialId":"'+decodeURIComponent(materialId)+'"}';
				stockData = InsightCommon.getServiceResponse(stockUrl,stockJsonRequestObj,"POST");
		//}
		}
	InsightCommon.getServiceResponseAsync(url,jsonRequestObj,"POST",function(data){ 				
		globalProductInfoResponse = data;		
		InsightSearch.productInfoResponse = data;
		jsonResponseObj = data;
		if(isIppProduct=='YES'){
			jsonResponseObj = $.extend(jsonResponseObj,{"isIpp": true});
		}
		if(Insight.locale == 'en_CA' || Insight.locale =='fr_CA'){
		jsonResponseObj = $.extend(jsonResponseObj,{"StockInfo": stockData});
		}
		jsonRecommendedResponseObj = jsonResponseObj;
		if(data.webProduct==undefined){
			document.location.href = "http://www.insight.com/insightweb/search#advSearch";
		}else {
			//InsightCommon.getRequestJsonObject = advSearchSessionJsonObj;
			populateResponseJson(jsonResponseObj, templateName, targetWindow,isIppProduct);
			if(InsightCommon.enableWarrantyTab != undefined && InsightCommon.enableWarrantyTab == true){
				moreWarranties();/*this flag is to open product preview page with warranty tab enabled after rendering ppp page async, if directly accessed using URL @yvadugu */			
		}
		if(InsightCommon.enableMoreRecommended != undefined && InsightCommon.enableMoreRecommended == true){
			showAllRecommendedProducts();/*this flag is to open product preview page with See More Recommendations if directly accessed using URL @yvadugu */	
			InsightCommon.enableMoreRecommended = false;
		}
		// For CR 3033 sell points code addition. sending material ID as sku for sell points
		 sp_sku.push(data.webProduct.manufacturerPartNumber);
		}
	});		
}
/*$(window).bind("load", function() {
	
	});*/
   
/**
 * returnToProductInfo - to go back to product detail page from recommended page 
 */
function returnToProductInfo(materialId){
	$("#showRecommendedPdctsContainer" ).css("display","none").empty();
	
	if($("#bodyContent").html()!=null && $("#bodyContent").html()!=""){
		$("#bodyContent" ).css("display","block");
	}else{
		$("#bodyContent" ).css("display","block").empty();
		getProductInfo(materialId,'mainPPP','');	
	}
}


function getPopupProductInfo(materialId, targetWindow, sourceWindow, isIppProduct,contractId) {
	var url = '/insightweb/getProductInfo';//$('#url').val();
	var templateName = "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/ProductInfoTemplate.html";
	if(contractId==null && contractId=='' && contractId=='null'){
		contractId = null;	
	} 		
	var jsonRequestObj = populateProductInfoJSONObject(decodeURIComponent(materialId),contractId,false);
	var serviceResponseObj;
	
//	if (jsonRequestObj != null)
//		jsonResponseObj=InsightCommon.getServiceResponseAsync(url,jsonRequestObj,"POST");	
	if(Insight.locale == 'en_CA' || Insight.locale == 'fr_CA'){
	var stockUrl="/insightweb/getAvalialabilityByWareHouse";
	var stockJsonRequestObj = '{"similarMaterialId":"'+materialId+'"}';
	stockData = InsightCommon.getServiceResponse(stockUrl,stockJsonRequestObj,"POST");
	}
	
	InsightCommon.getServiceResponseAsync(url,jsonRequestObj,"POST",function(data){ 				
		globalProductInfoResponse = data;
		jsonResponseObj = data;
		if(isIppProduct=='YES'){
			jsonResponseObj = $.extend(jsonResponseObj,{"isIpp": true});
		}
		if(Insight.locale == 'en_CA' || Insight.locale == 'fr_CA'){
		jsonResponseObj = $.extend(jsonResponseObj,{"StockInfo": stockData});
		}
		jsonRecommendedResponseObj = jsonResponseObj;
		populateResponseJson(jsonResponseObj, templateName, targetWindow,isIppProduct);		
	});		

	

	
	
}


function showStockCurrency(currentTag){
	
	if((Insight.locale == 'en_CA' || Insight.locale == 'fr_CA') && stockData != null){
		//if( (Insight.webGroupPermissions!= null && $.inArray("display_sap_quantity", Insight.webGroupPermissions)>-1) || !Insight.isLoggedin) {
			var stockpopuptitle = productSrchlabels.labels.stockpopuptitle;
			if(stockData.length){
				var data = stockData;
				var s='';
				s += '<table width=200px class="tableMain">';
				s += '<tr><td><h3 style="color:#000000; font-weight:500;">'+productSrchlabels.labels.location+'</h3></td><td><h3 style="color:#000000; font-weight:500; halign:center;">'+productSrchlabels.labels.stock+'</h3></td></tr>';
						$("#showStockCurrencyElementID").empty();
				for(var i=0; i<data.length-1; i++)
					s += '<tr><td><label style="align:right;">'+data[i].location+'</label></td><td><label style="halign:center;">#'+data[i].stock+'</label></td></tr>';
				
				s += '<tr><td><label style="align:right;">total</label></td><td><label style="halign:center;">#'+data[i].total+'</label></td></tr>';
				s += '</table>';
				$.template("StockPopupTmpl",s);
				InsightCommon.renderTemplate("StockPopupTmpl",'',"#showStockCurrencyElementID"); 
						$("#showStockCurrencyElementID").dialog({ width: '400px',height:'auto',resizable: true, position: 'relative', draggable: true, title: stockpopuptitle });
			}
		//}
		}
}



function loadAccessoriesInfo(materialId) {
	var url = '/insightweb/getProductInfo';//$('#url').val();
	var templateName = "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/loadAccessoriesPage.html";
	var contractId = null;	
	if(contractId!=null){
		contractId = '"'+contractId+'"';
	}
	var jsonRequestObj = '{"defaultPlant": "10","loadAccessories":true,"cartFlag":'+false+',"salesOrg": "2400","similarMaterialId" : "'+decodeURIComponent(materialId)+'", "locale":"en_us", "contractId":'+contractId+','
			+ '"user"  :  {}}';	
	var serviceResponseObj;
	if (jsonRequestObj != null){
		InsightCommon.showLoading();
		InsightCommon.getServiceResponseAsync(url,jsonRequestObj,"POST",function(productsResponse){ 				
			
			populateResponseJson(productsResponse, templateName, "accessoriesWindow",'');	
		});		
	}
	
}

function loadRecommendedProducts(materialId) {
	var url = '/insightweb/getProductInfo';//$('#url').val();
	var templateName = "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/loadTopmostRecommendedProducts.html";
	var contractId = null;	
	if(contractId!=null){
		contractId = '"'+contractId+'"';
	}
	var jsonRequestObj = '{"defaultPlant": "10","loadRecommendedProducts":true,"cartFlag":'+false+',"salesOrg": "2400","similarMaterialId" : "'+decodeURIComponent(materialId)+'", "locale":"en_us", "contractId":'+contractId+','
			+ '"user"  :  {}}';	
	var serviceResponseObj;
//	if (jsonRequestObj != null)
//		jsonRecommendedResponseObj=InsightCommon.getServiceResponse(url,jsonRequestObj,"POST");			
	
	InsightCommon.getServiceResponseAsync(url,jsonRequestObj,"POST",function(productsResponse){ 
		jsonRecommendedResponseObj = productsResponse;
		populateResponseJson(jsonRecommendedResponseObj, templateName, "topRecPrdsWindow",'');	
	});	
	
}
function populateProductInfoJSONObject(materialId,contractId,isFromCartRequest,productInfoSoftwareContractId, isFromCS) {
	var licenseContractIds = null;
	var loc = document.location.href;         
    if(loc.indexOf('search')>0  ){
           loc = loc.substring(loc.indexOf('search')+'search'.length, loc.length);           
           var partsArray = loc.split('#');
           var partsArray1 = "";
           var partsArray2 = "";
           if (partsArray != null && partsArray.length > 1) {
                  partsArray1 = partsArray[1].split('=');
                  try
                    {
                        if(partsArray[2]!=null && partsArray[2].split('=') != undefined){
                               partsArray2 = partsArray[2].split('=');
                        }
                    }
                  catch(err)
                    {
                        partsArray2 = null;
                    }                  
                  
                  if (partsArray1[0] == 'ProdInfoMtrId') {
                        itemnum =decodeURIComponent(partsArray1[1].split('='));                           
                  } if (partsArray[1]!=null && partsArray[1].split('=')[1]!=null && partsArray[1].split('=')[1].split('~')[1] == 'licenseContractIds'){
                	  licenseContractIds =partsArray[1].split('~')[1].split('=')[1];
                  } else 
                  if(partsArray2!=null && partsArray2[0] == 'licenseContractIds'){      
                	  licenseContractIds =partsArray2[1].substring(0,partsArray2[1].indexOf('~'));
                  } else {
                	  licenseContractIds = null; 
                  }
           }
           
    }
		
	if(contractId!=null){
		contractId = '"'+contractId+'"';
	}
	if(productInfoSoftwareContractId!=null &&  IsNumeric(productInfoSoftwareContractId)){
		licenseContractIds = '"'+productInfoSoftwareContractId+'"';
	} else if(licenseContractIds!=null && IsNumeric(licenseContractIds)){
		licenseContractIds = '"'+licenseContractIds+'"';
	} else
	if(licenseContractIds==null){
		licenseContractIds = "";
	}
	if(isFromCS==null || isFromCS == "" || isFromCS == undefined) {
		isFromCS=false;
	}
	
	var jsonObj = '{"defaultPlant": "10","loadAccessories":false,"loadRecommendedProducts":true,"cartFlag":'+isFromCartRequest+',"fromcs":'+isFromCS+',"salesOrg": "2400","similarMaterialId" : "'+materialId+'", "softwareContractIds":['+licenseContractIds+'],"locale":"en_us", "contractId":'+contractId+','
			+ '"user"  :  {}}';
	return jsonObj;

}

function populatePrices(data){	
var leasePrice=null;
if(data.webProduct.prices){
	$.each(data.webProduct.prices, function(index,price){
		if(price.priceLabel=="LEASEPRICELABEL"){
			leasePrice = price.price;
		}
	});
}
	
	
return leasePrice;	
}

function getPrices(webProduct){
	var pricesObj=new Object();
     pricesObj.isInventoryPrice = false;  
	 if(webProduct.prices){   
		$.each(webProduct.prices, function(index,price){
			if(price.priceLabel=="LISTPRICELABEL"){
				pricesObj.listPrice = price.price;
			}else if(price.priceLabel=="OPENMARKETPRICELABEL"){
				pricesObj.openmarketprice = price.price;
			}else if(price.priceLabel=="YOURPRICELABEL"){
				pricesObj.yourPrice = price.price;
			}else if( price.priceLabel== "COIPRICELABEL" || price.priceLabel=="CSIPRICELABEL"){
					pricesObj.yourPrice = price.inventoryPrice;		
					pricesObj.isInventoryPrice = true;
			}else if(price.priceLabel=="CALLFORPRICELABEL" ){
				pricesObj.yourPrice = price.price;
				pricesObj.callforprice =  price.priceLabel;
			}else if(price.priceLabel==null){
				if(typeof pricesObj.listPrice!=='undefined' || pricesObj.listPrice==null){
					pricesObj.listPrice = price.price; 
				}else{
					//????? It should n't be there what would be the problem? :-)
				}
			}
		});
	 }	
	
     
	return pricesObj;
}

function setPrices(webProduct){
	var pricesObj=new Object();	
	if(webProduct.contractDisplayInfo!=null && webProduct.contractDisplayInfo.contractTitle=='CALLFORPRICELABEL'){		
		pricesObj.callForPrice = "EXIST";		
    }else{
	    if(webProduct.prices){	
			$.each(webProduct.prices, function(index,price){	
				if(webProduct!=null && webProduct.contractDisplayInfo!=null && webProduct.contractDisplayInfo.contractPriceLabel=="CALLFORPRICELABEL"){
					pricesObj.callForPrice = "EXIST";
				} else 
				if(price.priceLabel=="CALLFORPRICELABEL"){
					if (!(typeof webProduct.contractDisplayInfo !== "undefined" && webProduct.contractDisplayInfo != null)){
						pricesObj.callForPrice = "EXIST";
					}
				}else if(price.priceLabel=="LISTPRICELABEL"){
					pricesObj.listPrice = price.price;
				}else if(price.priceLabel=="OPENMARKETPRICELABEL"){
					pricesObj.openmarketprice = price.price;
				}
				else if(price.priceLabel=="YOURPRICELABEL"){
					pricesObj.yourPrice = price.price;
				}else if(price.priceLabel=="LEASEPRICELABEL"){
					pricesObj.leasePrice = price.price;
				}
				else if(price.priceLabel==null){
					if(typeof pricesObj.listPrice!=='undefined' || pricesObj.listPrice==null){
						pricesObj.listPrice = price.price; 
					}else{
						//????? It should n't be there what would be the problem? :-)
					}
				}
			});
		}
    }
	webProduct = $.extend(webProduct,pricesObj);
	
}

function replaceHash(str)
{
    var n=str.replace("#","_");
    
    return n;
}

function getPriceFromWebProduct(webProduct, source,productSrchlabels) {	
	var priceDiv = '';	
	var productInfoLabels = productSrchlabels;
		if (typeof webProduct !== "undefined" && webProduct != null) {
			if (typeof webProduct.contractDisplayInfo !== "undefined"&& webProduct.contractDisplayInfo != null) { // contract price exist
				var contractPrice = webProduct.contractDisplayInfo.contractPrice;
				var contractPriceLabel = 'USD'; // webProduct.contractDisplayInfo.contractPriceLabel;
				var contractTitle = webProduct.contractDisplayInfo.contractTitle;
				var contractLongTitle = webProduct.contractDisplayInfo.contractLongTitle;
				var contractTitleFull=webProduct.contractDisplayInfo.contractLongTitle;
				var uniqueMaterialId = replaceHash(webProduct.materialId);
				    uniqueMaterialId =  uniqueMaterialId.replaceAll("/","-");
				var contractId = webProduct.contractDisplayInfo.contractId;
				if(contractId!=null && contractId.length==0){
					contractId = "";
				}
				
				if (source == 'RECOMMENDEDPRODS' ||source == 'RECENTLYVIEWED' || source == 'WARRANTIES' || source == 'WARRANTIESTAB'|| source == 'ACCESSORIESTAB' || source=='OEMWARRANTIES' || source =='COMPSTDS') {
					if (webProduct.contractDisplayInfo.morePricesAvailable) {					
						if(webProduct.contractDisplayInfo.contractTitle!=null && (webProduct.contractDisplayInfo.contractTitle=='OPENMARKETPRICELABEL' || webProduct.contractDisplayInfo.contractTitle=='YOURPRICELABEL')){
							if(webProduct.contractDisplayInfo.contractTitle=='OPENMARKETPRICELABEL'){
								priceDiv = priceDiv + '<span class="priceLabelClass">'+productInfoLabels.openmarketPrice+'</span><br>';	
								priceDiv = priceDiv + '<span class="currency">'+webProduct.contractDisplayInfo.currency+'&nbsp;</span>';
								priceDiv = priceDiv + '<span id="price_'+webProduct.materialId+'" class="yourPrice">'+InsightCommon.formatCurrency(contractPrice)+'</span>';	
							} else if(webProduct.contractDisplayInfo.contractTitle=='YOURPRICELABEL'){
								priceDiv = priceDiv + productInfoLabels.yourprice+' &nbsp;<br>';	
								priceDiv = priceDiv + '<span class="currency">'+webProduct.contractDisplayInfo.currency+'&nbsp;</span>';
								priceDiv = priceDiv + '<span id="price_'+webProduct.materialId+'" class="yourPrice">'+InsightCommon.formatCurrency(contractPrice)+'</span>';								
							}	
						
						} else {
					if(source == 'RECOMMENDEDPRODS' ||source == 'RECENTLYVIEWED'){
						priceDiv = priceDiv + '<span class="textBold" style="color:#95A6B1;">'+productInfoLabels.lowestPrice+'</span>';
						priceDiv = priceDiv + '<br>';
					}
					priceDiv = priceDiv + '<span class="currency">'+webProduct.contractDisplayInfo.currency+'&nbsp;</span>';
					if(source == 'WARRANTIES'){
					priceDiv = priceDiv + '<span id="price_'+webProduct.materialId+'" class="yourPriceSmall">'+InsightCommon.formatCurrency(contractPrice)+'</span>';
					}else{
						priceDiv = priceDiv + '<span id="price_'+webProduct.materialId+'" class="yourPrice">'+InsightCommon.formatCurrency(contractPrice)+'</span>';	
					}
					priceDiv = priceDiv + '<br>';
					if(!(source == 'WARRANTIES')){
						
					priceDiv = priceDiv + '<div  style="margin-top:2px;" class="float ppptools"  onmouseout="InsightCommon.closeTooltip(this);" onmouseover="InsightCommon.showTooltip(\'<center>'+contractTitleFull+'</center>\', this);">'+ contractTitle + '</div>';
					//priceDiv = priceDiv + '<br>';
					}
					if(source == 'RECOMMENDEDPRODS'||source == 'RECENTLYVIEWED' ||source == 'ACCESSORIESTAB'){
						if (webProduct.contractDisplayInfo.morePricesAvailable) {
							priceDiv = priceDiv
									+ '<div class="left"><a href="javascript:viewContractPricing(\''+webProduct.materialId+'\');">'+productInfoLabels.morePricesAvailable+'</a></div>';//<span onclick="javascript:viewContractPricingOptions(this,\''+webProduct.materialId+'\');" class="frameworkIcons Deep_Close_Icon"></span>';
							// priceDiv=priceDiv+'<div id="cpl41786UU"
							// style="display:none;">';
							//priceDiv = priceDiv+'<div class="left"><select><option value="1">one</option></select></div>'; 
						}
					}
				}
			} else if(webProduct.contractDisplayInfo.contractTitle!=null && (webProduct.contractDisplayInfo.contractTitle=='OPENMARKETPRICELABEL' || webProduct.contractDisplayInfo.contractTitle=='YOURPRICELABEL')){
						if(webProduct.contractDisplayInfo.contractTitle=='OPENMARKETPRICELABEL'){
							priceDiv = priceDiv + '<span class="priceLabelClass">'+productInfoLabels.openmarketPrice.toUpperCase()+'</span><br>';	
							priceDiv = priceDiv + '<span class="currency">'+webProduct.contractDisplayInfo.currency+'&nbsp;</span>';
							priceDiv = priceDiv + '<span id="price_'+webProduct.materialId+'" class="yourPrice">'+InsightCommon.formatCurrency(contractPrice)+'</span>';	
						} else if(webProduct.contractDisplayInfo.contractTitle=='YOURPRICELABEL'){
							priceDiv = priceDiv + productInfoLabels.yourPrice.toUpperCase()+' &nbsp;<br>';	
							priceDiv = priceDiv + '<span class="currency">'+webProduct.contractDisplayInfo.currency+'&nbsp;</span>';
							priceDiv = priceDiv + '<span id="price_'+webProduct.materialId+'" class="yourPrice">'+InsightCommon.formatCurrency(contractPrice)+'</span>';								
						}	
						priceDiv = priceDiv + '<input type=hidden  id="'+webProduct.materialId+'"  value="'+contractId+'">'; 
						
					} else {
						if(source =='COMPSTDS'){
							priceDiv = priceDiv + '<div >'+contractLongTitle+ '</div>';
							priceDiv = priceDiv + '<span id="price_'+webProduct.materialId+'" class="yourPriceSmall">'+InsightCommon.formatCurrency(contractPrice)+'</span>';
						}
						else { priceDiv = priceDiv + '<span class="currency">'+webProduct.contractDisplayInfo.currency+'&nbsp;</span>';
						if(source == 'WARRANTIES'){
						priceDiv = priceDiv + '<span id="price_'+webProduct.materialId+'" class="yourPriceSmall">'+InsightCommon.formatCurrency(contractPrice)+'</span>';
						}else{
							priceDiv = priceDiv + '<span id="price_'+webProduct.materialId+'" class="yourPrice">'+InsightCommon.formatCurrency(contractPrice)+'</span>';	
						}
						priceDiv = priceDiv + '<br>';
						if(!(source == 'WARRANTIES')){
							priceDiv  = priceDiv + '<div  style="margin-top:2px;" class="float ppptools"  onmouseout="InsightCommon.closeTooltip(this);" onmouseover="InsightCommon.showTooltip(\'<center>'+contractTitleFull.toUpperCase()+'</center>\', this);">'+ contractTitle + '</div>';
							priceDiv = priceDiv + '<br>';
						}			
						
					}					
				    }
					priceDiv = priceDiv + '<input type=hidden  id="'+webProduct.materialId+'"  value="'+contractId+'">'; 
					} else {
					if (webProduct.contractDisplayInfo.morePricesAvailable) {			
						// priceDiv=priceDiv+'<div class="custPriceClass">';
						if(webProduct.contractDisplayInfo.contractTitle=='OPENMARKETPRICELABEL'){
							contractTitle =productInfoLabels.OPENMARKETPRICE;
							contractLongTitle =productInfoLabels.OPENMARKETPRICE;
						}else if(webProduct.contractDisplayInfo.contractTitle=='YOURPRICELABEL'){
							contractTitle =productInfoLabels.yourPrice.toUpperCase();
							contractLongTitle =contractTitle;	
							contractTitleFull = contractTitle;
						}
						priceDiv = priceDiv + '<div>';
						priceDiv = priceDiv	+ '<div style="width:100%;text-align:left;float:left; position: relative">';
						priceDiv = priceDiv	+ '<span class="textBold" style="color:#95A6B1;">'+productInfoLabels.lowestContractPrice+'</span>';
						priceDiv = priceDiv + '</div>';						
						priceDiv = priceDiv	+ '<div style="width:90%;padding-left:20px;text-align:left;float:left;">';
						priceDiv = priceDiv + '<span class="currencyLarge">'+ webProduct.contractDisplayInfo.currency + ' </span>&nbsp;';
						priceDiv = priceDiv + '<span id="price_'+webProduct.materialId+'" class="yourPriceLarge">' + InsightCommon.formatCurrency(contractPrice) + '</span>';
						priceDiv = priceDiv + '<br>';
						priceDiv = priceDiv + '<div  class="float ppptools"  onmouseout="InsightCommon.closeTooltip(this);" onmouseover="InsightCommon.showTooltip(\'<center>'+contractTitleFull+'</center>\', this);">'+ contractTitle + '</div>';
						
						// CR 565 added code
						if(source =='MainPP' && webProduct.contractDisplayInfo.indexPrice!=null && webProduct.contractDisplayInfo.discountPrice!=null ){						
							priceDiv = priceDiv	+ '<div style="width:100%;text-align:left;float:left; position: relative">';
							priceDiv = priceDiv	+ '<span>'+productInfoLabels.insightStandardPrice+'</span>';
							priceDiv = priceDiv	+ '<span>'+InsightCommon.formatCurrency(webProduct.contractDisplayInfo.indexPrice)+ '</span>';
							priceDiv = priceDiv + '</div>';
							priceDiv = priceDiv	+ '<div style="width:100%;text-align:left;float:left; position: relative">';
							priceDiv = priceDiv	+ '<span>'+productInfoLabels.pppDiscount+'</span>';
							priceDiv = priceDiv	+ '<span>&nbsp;'+webProduct.contractDisplayInfo.discountPrice+'%</span>';
							priceDiv = priceDiv + '</div>';
							}
						// end CR 565
						
						if (webProduct.contractDisplayInfo.morePricesAvailable) {
							priceDiv = priceDiv
									+ '<br/><a href="javascript:viewContractPricing(\''+webProduct.materialId+'\');">'+productInfoLabels.morePricesAvailable+'</a>';
						}
						priceDiv = priceDiv + '</div>';
						
						priceDiv = priceDiv + '</div>'; 
					}
					else {						
						if(webProduct.contractDisplayInfo.contractTitle!=null && (webProduct.contractDisplayInfo.contractTitle=='OPENMARKETPRICELABEL' || webProduct.contractDisplayInfo.contractTitle=='YOURPRICELABEL')){
							if(webProduct.contractDisplayInfo.contractTitle=='OPENMARKETPRICELABEL'){
								priceDiv = priceDiv + '<span class="priceLabelClass">'+productInfoLabels.openmarketPrice+'</span><br>';	
							} else if(webProduct.contractDisplayInfo.contractTitle=='YOURPRICELABEL'){
								priceDiv = priceDiv + productInfoLabels.yourPrice+'<br>';	
							}							
							priceDiv = priceDiv + '<span class="currency">'+webProduct.contractDisplayInfo.currency+'&nbsp;</span>';
							priceDiv = priceDiv + '<span id="price_'+webProduct.materialId+'" class="yourPrice">'+InsightCommon.formatCurrency(contractPrice)+'</span>';	
						} else {
						priceDiv = priceDiv + '<div>';
						priceDiv = priceDiv	+ '<div style="width:90%;padding-left:20px;text-align:left;float:left;">';
						priceDiv = priceDiv + '<span class="currencyLarge">'+ webProduct.contractDisplayInfo.currency + ' </span>';
						priceDiv = priceDiv + '<span id="price_'+webProduct.materialId+'" class="yourPriceLarge">' + InsightCommon.formatCurrency(contractPrice) + '</span>';
						priceDiv = priceDiv + '<br>';
						priceDiv = priceDiv + '<div  class="float ppptools">'+ contractLongTitle + '</div>';
						// CR 565 added code
						if(source =='MainPP' && webProduct.contractDisplayInfo.indexPrice!=null && webProduct.contractDisplayInfo.discountPrice!=null ){						
							priceDiv = priceDiv	+ '<div style="width:100%;text-align:left;float:left; position: relative">';
							priceDiv = priceDiv	+ '<span>'+productInfoLabels.insightStandardPrice+'</span>';
							priceDiv = priceDiv	+ '<span>'+InsightCommon.formatCurrency(webProduct.contractDisplayInfo.indexPrice)+ '</span>';
							priceDiv = priceDiv + '</div>';
							priceDiv = priceDiv	+ '<div style="width:100%;text-align:left;float:left; position: relative">';
							priceDiv = priceDiv	+ '<span>'+productInfoLabels.pppDiscount+'</span>';
							priceDiv = priceDiv	+ '<span>&nbsp;'+webProduct.contractDisplayInfo.discountPrice+'%</span>';
							priceDiv = priceDiv + '</div>';
							}
						// end CR 565
						//priceDiv = priceDiv + '<div class="bubble_rec bubble_recInline" id="pppRecProductsDesc1_'+source+'_'+uniqueMaterialId+'" ><table cellpadding="0" cellspacing="0" style="table-layout: fixed; word-wrap: break-word; width: 100px"><tbody><tr><td class="topLeftCorner topLeftCornerInline" width="10px"></td>	<td class="topSidePointerLeft topLeftCornerInline" ></td> <td class="topRightCorner topLeftCornerInline" width="10"></td> </tr>	<tr><td class="leftSide" bgcolor="#FFFFFF">&nbsp;</td><td bgcolor="#FFFFFF"><div>Contract : <br>'+ contractLongTitle + '</div></td><td class="rightSide" bgcolor="#FFFFFF">&nbsp;</td></tr><tr><td class="bottomLeftCorner bottomLeftInline"	width="10px"></td>	<td class="bottomSide bottomLeftInline" ></td>	<td class="bottomRightCorner bottomLeftInline"	width="10px"></td>	</tr></tbody></table></div>';
						priceDiv = priceDiv + '</div>';						
						priceDiv = priceDiv + '</div>'; 
						}
					}
				
					priceDiv = priceDiv + '<input type=hidden  id="'+webProduct.materialId+'"  value="'+webProduct.contractDisplayInfo.contractId+'">'; 
				}
				
			} else {//no contract price.  populate leastPrice from the prices object.
				var productPrices = getPrices(webProduct);
				var productPrice = null;
				if (source == 'RECOMMENDEDPRODS' ||source == 'RECENTLYVIEWED' ||  source == 'WARRANTIESTAB' || source == 'WARRANTIES') {
					if(typeof productPrices.yourPrice!== 'undefined' && productPrices.yourPrice!=null){
						productPrices.listPrice = productPrices.yourPrice;
						productPrices.yourPrice = null;
					}
				}
				if(typeof productPrices.yourPrice!== 'undefined' && productPrices.yourPrice!=null){
					productPrice = productPrices.yourPrice;
				}else if(productPrices.openmarketprice!=null){
					productPrice = productPrices.openmarketprice;
				} else{
					productPrice = productPrices.listPrice;
				}
				var callforprice = productPrices.callforprice;
				//priceDiv = '<span class="currency">USD&nbsp;</span> <span class="yourPrice"></span>';
				if(source=='MainPP'){
					if(typeof productPrices.yourPrice!== 'undefined' && productPrices.yourPrice!=null && typeof productPrices.listPrice!== 'undefined' && productPrices.listPrice!=null){
						priceDiv = priceDiv + '<span class="priceLabelClass">'+productInfoLabels.listPrice+'</span>';
						priceDiv = priceDiv + '<span class="priceValueClass">';
						priceDiv = priceDiv + '<span class="currencyLarge">'+webProduct.prices[0].currency+'</span>&nbsp;';
						priceDiv = priceDiv + '<span  class="listprice">'+productPrices.listPrice+'</span>';
						priceDiv = priceDiv + '</span>';
						priceDiv = priceDiv + '<span class="priceLabelClass">'+productInfoLabels.yourPrice+' &nbsp;&nbsp;&nbsp;</span>';

						priceDiv = priceDiv + '<span class="priceValueClass">';
						priceDiv = priceDiv + '<span class="currencyLarge">'+webProduct.prices[0].currency+'</span>&nbsp;';
						priceDiv = priceDiv + '<span id="price_'+webProduct.materialId+'" class="yourPriceLarge">'+InsightCommon.formatCurrency(productPrices.yourPrice)+'</span>';
						priceDiv = priceDiv + '</span>';
					} else {
						if(webProduct.prices){	
							if(webProduct.prices[0].priceLabel!=null && webProduct.prices[0].priceLabel=='OPENMARKETPRICELABEL'){
								priceDiv = priceDiv + '<span class="priceLabelClass">'+productInfoLabels.openmarketPrice+'</span><br>';						
							}else if(webProduct.prices[0].priceLabel!=null && webProduct.prices[0].priceLabel=='YOURPRICELABEL')
								priceDiv = priceDiv + '<span class="priceLabelClass">'+productInfoLabels.yourPrice+' &nbsp;&nbsp;&nbsp;</span>';
							else if(webProduct.prices[0].priceLabel!=null && webProduct.prices[0].priceLabel== "COIPRICELABEL"){
								priceDiv = priceDiv + '<span class="priceLabelClass">'+productInfoLabels.coiprice+'&nbsp;&nbsp;&nbsp;</span><br>';
							}else if (webProduct.prices[0].priceLabel!=null && webProduct.prices[0].priceLabel=="CSIPRICELABEL"){
								 priceDiv = priceDiv + '<span class="priceLabelClass">'+productInfoLabels.csiprice+'&nbsp;&nbsp;&nbsp;</span><br>';
							}else if (webProduct.prices[0].priceLabel!=null && webProduct.prices[0].priceLabel=="CALLFORPRICELABEL" && (((webProduct.usageProduct && webProduct.usageType=='SPLA' )|| webProduct.vsppProduct))){
								 priceDiv = priceDiv + '<span class="priceLabelClass">'+productInfoLabels.yourprice+'&nbsp;&nbsp;&nbsp;</span><br>';
							}else
							priceDiv = priceDiv + '<span class="priceLabelClass">'+productInfoLabels.listPrice+'</span>';					
							
							priceDiv = priceDiv + '<span class="priceValueClass">';
							priceDiv = priceDiv + '<span class="currencyLarge">'+webProduct.prices[0].currency+'</span>&nbsp;';
							priceDiv = priceDiv + '<span id="price_'+webProduct.materialId+'" class="yourPriceLarge">'+InsightCommon.formatCurrency(productPrice)+'</span>';
							priceDiv = priceDiv + '</span>';
						}
				}
				}
 				else {
				if (source == 'RECOMMENDEDPRODS' ||source == 'RECENTLYVIEWED' || source == 'OEMWARRANTIES' || source == 'ACCESSORIESTAB' ||  source == 'COMPSTDS' || source == 'WARRANTIESTAB'  || source == 'WARRANTIES') {					
					if(productPrices.isInventoryPrice){
						if(webProduct.prices[0].priceLabel!=null && webProduct.prices[0].priceLabel== "COIPRICELABEL"){
							priceDiv = priceDiv +productInfoLabels.coiprice+'&nbsp;&nbsp;&nbsp;';
						}else if (webProduct.prices[0].priceLabel!=null && webProduct.prices[0].priceLabel=="CSIPRICELABEL"){
							 priceDiv = priceDiv +productInfoLabels.csiprice+'&nbsp;&nbsp;&nbsp;';
						}	 
							     priceDiv = priceDiv	+ '<span class="currency">'+webProduct.prices[0].currency+'&nbsp;</span>';
								priceDiv = priceDiv + '<span id="price_'+ webProduct.materialId	+ '" class="yourPrice">'+ InsightCommon.formatCurrency(productPrice) + '</span>';
							 
					}else{					
						if (typeof productPrices.yourPrice !== 'undefined' && productPrices.yourPrice != null) {
					
						if (typeof productPrices.listPrice != 'undefined' && productPrices.listPrice != null) {
							
							if (source == 'ACCESSORIESTAB') {
								priceDiv = priceDiv + productInfoLabels.listPrice+'';
								priceDiv = priceDiv	+ '<span class="currency">'+webProduct.prices[0].currency+'</span>';
								priceDiv = priceDiv + '<span id="price_list_'+ webProduct.materialId + '" class="listPrice">'+ InsightCommon.formatCurrency(productPrices.listPrice) + '</span><br>';

								priceDiv = priceDiv + productInfoLabels.yourPrice+' &nbsp;';

								priceDiv = priceDiv	+ '<span class="currency">'+webProduct.prices[0].currency+'&nbsp;</span>';
								priceDiv = priceDiv + '<span id="price_'+ webProduct.materialId	+ '" class="yourPrice">'+ InsightCommon.formatCurrency(productPrices.yourPrice) + '</span>';
							} else {
								priceDiv = priceDiv + productInfoLabels.listPrice+'<br>';
								priceDiv = priceDiv	+ '<span class="currency">'+webProduct.prices[0].currency+'</span>';
								priceDiv = priceDiv + '<span id="price_list_'+ webProduct.materialId + '" class="listPrice">'+ InsightCommon.formatCurrency(productPrices.listPrice) + '</span><br>';


								priceDiv = priceDiv + productInfoLabels.yourPrice+' &nbsp;<br>';

								priceDiv = priceDiv	+ '<span class="currency">'+webProduct.prices[0].currency+'&nbsp;</span>';
								priceDiv = priceDiv + '<span id="price_'+ webProduct.materialId	+ '" class="yourPrice">'+ InsightCommon.formatCurrency(productPrices.yourPrice) + '</span>';
							}							
							
						} else if(callforprice!=null && callforprice=='CALLFORPRICELABEL' && source == 'COMPSTDS'){
						        if(webProduct.materialId == 'NONE')
						        	{

						        	priceDiv = priceDiv + '&nbsp;&nbsp;'+productInfoLabels.yourPrice+'</br>';

									priceDiv = priceDiv	+ '<span class="currency">&nbsp;&nbsp;'+webProduct.prices[0].currency+'&nbsp;</span>';
									priceDiv = priceDiv + '<span id="price_'+ webProduct.materialId	+ '" class="yourPrice">'+ InsightCommon.formatCurrency(productPrices.yourPrice) + '</span>';
						        	}
						        else{
						    	priceDiv = priceDiv + '<span>'+productInfoLabels.callforprice+'</span>';
						        }
						}else {
							if(source == 'COMPSTDS'){
								priceDiv = priceDiv + '&nbsp;&nbsp;'+productInfoLabels.yourprice+'<br>';
								priceDiv = priceDiv	+ '<span class="currency">&nbsp;&nbsp;'+webProduct.prices[0].currency+'&nbsp;</span>';
							}
							else{
								priceDiv = priceDiv + productInfoLabels.yourprice+' &nbsp;';

								priceDiv = priceDiv	+ '<span class="currency">'+webProduct.prices[0].currency+'&nbsp;</span>';
							}
							priceDiv = priceDiv + '<span id="price_'+ webProduct.materialId	+ '" class="yourPrice">'+ InsightCommon.formatCurrency(productPrices.yourPrice) + '</span>';
						}
					}else{
						if(webProduct.prices){
							if(webProduct.prices[0].priceLabel!=null && webProduct.prices[0].priceLabel=='OPENMARKETPRICELABEL'){
								priceDiv = priceDiv + productInfoLabels.openmarketPrice;
								priceDiv = priceDiv + '<br>';
								priceDiv = priceDiv	+ '<span class="currency">'+webProduct.prices[0].currency+'&nbsp;</span>';
								priceDiv = priceDiv + '<span id="price_'+ webProduct.materialId	+ '" class="yourPrice">'+ InsightCommon.formatCurrency(productPrice) + '</span>';
							}
							 else{
							priceDiv = priceDiv	+ '<span class="currency">'+webProduct.prices[0].currency+'&nbsp;</span>';
							priceDiv = priceDiv + '<span id="price_'+ webProduct.materialId	+ '" class="yourPrice">'+ InsightCommon.formatCurrency(productPrice) + '</span>';
							}
						}
					}
				} 
 				}else {
 					if(productPrices.isInventoryPrice){
						if(webProduct.prices[0].priceLabel!=null && webProduct.prices[0].priceLabel== "COIPRICELABEL"){
							priceDiv = priceDiv + productInfoLabels.coiprice+'<br>';
						}else if (webProduct.prices[0].priceLabel!=null && webProduct.prices[0].priceLabel=="CSIPRICELABEL"){
							 priceDiv = priceDiv +productInfoLabels.csiprice+'<br>';
						}	 
							     priceDiv = priceDiv	+ '<span class="currency">'+webProduct.prices[0].currency+'&nbsp;</span>';
								priceDiv = priceDiv + '<span id="price_'+ webProduct.materialId	+ '" class="yourPrice">'+ InsightCommon.formatCurrency(productPrice) + '</span>';
							 
					}  else {
						if(webProduct.prices){	
		 					if(webProduct.prices[0].priceLabel!=null){
								if(source=='CP' && webProduct.prices[0].priceLabel=='OPENMARKETPRICELABEL'){
									priceDiv = priceDiv + productInfoLabels.openmarketPrice+'<br>';
								} else if(webProduct.prices[0].priceLabel=='YOURPRICELABEL'){
									priceDiv = priceDiv + productInfoLabels.yourprice;
									priceDiv = priceDiv + '<br>';
								}
							}
							priceDiv = priceDiv	+ '<span class="currency">'+webProduct.prices[0].currency+'&nbsp;</span>';
						}
					if(source == 'WARRANTIES'){
						priceDiv = priceDiv + '<span id="price_'+webProduct.materialId+'" class="yourPriceSmall">'+InsightCommon.formatCurrency(productPrice)+'</span>';
					}else
						priceDiv = priceDiv + '<span id="price_'+ webProduct.materialId + '" class="yourPrice">' + InsightCommon.formatCurrency(productPrice) + '</span>';
					}
				}
			}
			}
		}
	
	return priceDiv;
}

function populateResponseJson(data, templateName, targetWindow,isIPP) {
	 productSrchlabels = InsightSearch.loadSearchLabels();

	try {
		var productInfoTemplate = InsightCommon.getContent(InsightSearch.staticContentUrl,'',templateName);
		$.template("productInfoTemplate1", productInfoTemplate);
	} catch (e) {
		alert(e);
	}
	if(InsightSearch.isFromCs == 'true')
		InsightSearch.isFromCs = true;
	else
		InsightSearch.isFromCs = false;
	var extendedData = {tmplLeasePrice:populatePrices(data), isNotSame:function(i,accessory,accessories)
									{
										if(i==0)
											return true
										else
											if(accessories[eval(i-1)].categoryId!=accessory.categoryId)
												return true;
											else
												return false;
									}
						, getPrice: function(webProduct,source){
							return getPriceFromWebProduct(webProduct,source,productSrchlabels.labels);
						}
						, isShowReturn : function(){
							return getShowReturn();
						},
						isFromCs : InsightSearch.isFromCs
						};
	var divId = "#bodyContent";
	if(targetWindow=='accessoriesWindow'){
		divId = "#loadAccessoriesData";		
		var windowsSpecs = getWindowConfigSpecs("mainPPP",isIPP);
		var targetWindowExteder = {"targetWindow" : "mainPPP"};
		extendedData = $.extend(extendedData, windowsSpecs);
		extendedData = $.extend(extendedData, targetWindowExteder);
	}else{
	var windowsSpecs = getWindowConfigSpecs(targetWindow,isIPP);
	var targetWindowExteder = {"targetWindow" : targetWindow};
	extendedData = $.extend(extendedData, windowsSpecs);
	extendedData = $.extend(extendedData, targetWindowExteder);
		$(divId).empty();
		$(divId).html("");
		$(divId).css("background-color:#FFFFFF");
     }
		
	/* get csid of all ics pdcts */
	InsightSearch.icsMap = null;
	var icsObj = new Array();
	// oem warranties
	if(data.oemWarranties!=null){
		$.each(data.oemWarranties,function(index,rowData){
			if(rowData.csid!=null && rowData.csid!=""){
				var matId = rowData.materialId;
				var csid = rowData.csid;
				icsObj[matId] = csid;
			}
		});
		
	}
	// ipp warranties
	if(data.warrantyPdcts!=null){
		$.each(data.warrantyPdcts,function(index,rowData){
			if(rowData.csid!=null && rowData.csid!=""){
				var matId = rowData.materialId;
				var csid = rowData.csid;
				icsObj[matId] = csid;
			}
		});
	}
	// recommended pdcts
	if(data.recommendedProducts!=null){
		$.each(data.recommendedProducts,function(index,rowData){
			if(rowData.csid!=null && rowData.csid!=""){
				var matId = rowData.materialId;
				var csid = rowData.csid;
				icsObj[matId] = csid;
			}
		});
	}
	if(data.oemWarranties!=null || data.warrantyPdcts!=null || data.recommendedProducts!=null){
		InsightSearch.icsMap = icsObj;
	}
	
	var searchLabels = InsightSearch.loadSearchLabels();

         data = $.extend(data, {'labels':searchLabels.labels});
         data = $.extend(data,extendedData);
		InsightCommon.renderTemplate("productInfoTemplate1", data, divId);
		InsightCommon.hideLoading();
		/*ippTouchCarousel();*/
				
		
		/* Append Tech Specs details to Warranties Tab */
		var productComesWithLabel = productSrchlabels.labels.thisProductComesWith;
		if(data.webProduct.extendedSpecsMap){
			$.each(data.webProduct.extendedSpecsMap, function(colKey,extendedSpec){
				if(colKey == 'Manufacturer Warranty'){ 
					$.each(extendedSpec.details, function(rowKey,warrantyDetails){
						if(warrantyDetails.label == 'Service & Support Details'){
							$("#warrantyTabText").html(productComesWithLabel+" "+warrantyDetails.value);
							return false;
						}
						else if (warrantyDetails.label == 'Service & Support'){
							$("#warrantyTabText").html(productComesWithLabel+" "+warrantyDetails.value);
						}
						else
							$("#warrantyTabText").remove();
					});
					}
			});
		}else{
			$("#warrantyTabText").remove();
		}
		icsDomainUrl = jsonResponseObj.icsDomainUrl;
		icsPviewUrl = jsonResponseObj.icsPviewUrl;
		icsViewUrl = jsonResponseObj.icsViewUrl;
		icsClickUrl = jsonResponseObj.icsClickUrl;
		$.each(data.topMostRecProducts, function(key, topmost) {
			recommendedCSidValues = topmost.csid+","+recommendedCSidValues;
			if (key< 4 && topmost.discontinuedStatus == false ) {
				topmostCSidValues = topmost.csid+","+topmostCSidValues;
			}			
		});
		recommendedCSidValues = recommendedCSidValues.substring(0,recommendedCSidValues.length-1);
		topmostCSidValues = topmostCSidValues.substring(0,topmostCSidValues.length-1);
		
		$('#icsclicktracking').attr('src',"https://"+icsDomainUrl+icsClickUrl+"ts="+InsightCommon.getCurrentTimestamp()+"&csid="+topmostCSidValues+"&sid="+Insight.cacheSessionId);
		
		$('#icstracking').attr('src',"https://"+icsDomainUrl+icsViewUrl+"ts="+InsightCommon.getCurrentTimestamp()+"&csid="+topmostCSidValues+"&sid="+Insight.cacheSessionId);
		
		$('#producticstracking').attr('src',"https://"+icsDomainUrl+icsPviewUrl+"ts="+InsightCommon.getCurrentTimestamp()+
				"&sellerId=4&contextName=Featured&categoryId="+data.webProduct.categoryId+"&productId="+data.webProduct.materialId+"&sid="+Insight.cacheSessionId);
		
		$('meta[name=description]').attr('content', data.webProduct.description);	
		
		if(targetWindow =='printPPP'){		
		  $(".tabClass").css('display','block');
		  $(".pppMainQuantityAdd").css('display','none');
		  $("#productTabButtonContainer").css('display','none');
		  $("#printHeader").css('display','block');	  
		  $('#printHeader').equalHeights();
		}
		

        var zIndexNumber = 1000;
        $(".rhsProductsDiv").each(function() {
               $(this).css('zIndex', zIndexNumber);
               zIndexNumber -= 10;
       });
}
function ippTouchCarousel(){
	var sliderInstance = $("#ippWarrScrollDiv").data("touchCarousel");
	if(sliderInstance == null){
		var ippTouchCarouselConfig = {					
				pagingNav: true,
				snapToItems: true,
				itemsPerMove: 3,				
				scrollToLast: false,
				loopItems: false,
				scrollbar: false
		    };
		$("#ippWarrScrollDiv").touchCarousel(ippTouchCarouselConfig);
	}	
}
function oemTouchCarousel(){
	var sliderInstance = $("#oemWarrScrollDiv").data("touchCarousel");
	if(sliderInstance == null){
		var oemTouchCarouselConfig = {					
				pagingNav: true,
				snapToItems: true,
				itemsPerMove: 3,				
				scrollToLast: false,
				loopItems: false,
				scrollbar: false
		    };
		$("#oemWarrScrollDiv").touchCarousel(oemTouchCarouselConfig);
	}
}
function icsTouchCarousel(){
	var icsTouchCarouselConfig = {					
			pagingNav: true,
			snapToItems: true,
			itemsPerMove: 3,				
			scrollToLast: false,
			loopItems: false,
			scrollbar: false
	    };
	$("#icsWarrScrollDiv").touchCarousel(icsTouchCarouselConfig);
}
function getJSONResponse(url, jsonRequestObj) {
	var serviceResponse;
	if (url != "" && jsonRequestObj != "") {
		serviceResponse =InsightCommon.getServiceResponse(url,jsonRequestObj,"POST");							
	}
	return serviceResponse; 
}


/**/
function getWindowConfigSpecs(targetWindow,isIPP){
	var windowConfigSpecs = null;
	if(targetWindow=='mainPPP'){
		windowConfigSpecs=
		{showProductNav : true,
		showProductBar : true,
		showProductImage : true,
		showAddCart : true,
		showDiscontinuedProduct : false,
		showTabs : true,
		showProductTitle:true,
		/*Individual product Tools*/
		showProductTools:true,
		priceEstimate : true,
		productCenter : true,
		companyStandard : true,
		removeTabSec: false,
		print : true,
		sendMail  : true,
		/*Individual Product Tools*/
		insightPrefferdManf:true,
		showEmailLink:true,
		showPrintLink:true,
		showCompareSimilarLink:true,
		showSearchSimillarProducts:true,
		showProductCenterLink:true,
		showPurchasingTools:true,
		MEMORY_SELECTOR_ATTRIBUTE:true,
		showCheckCompatibilityLink:true,
		showBrandShowcase:true,
		RECOMMENDED_PRODUCTS_REQUEST_ATTRIBUTE:true,
		showMoreRecommendedProducts:true,
		showRecentlyViewedProducts:true,
		ACCESSORIES_REQUEST_ATTRIBUTE:true,
		REBATES_REQUEST_ATTRIBUTE:true,
		WARRANTY_REQUEST_ATTRIBUTE:true,
		showRhs:true,
		showProductTour:true,
		ALLOW_MAIN_PRODUCT_ADD_ATTR:true,
		ALLOW_ACCESSORIES_ADD_ATTR:true,
		ALLOW_OEM_WARRANTIES_ADD_ATTR:true,
		ALLOW_RECOMMENDED_PRODUCTS_ADD_ATTR:true,
		showPriceEstimate:true,
		showRelatedProducts:true,
		showBullets:true,
		showLogo:true,
		showAllContractPrices:false,
		showMorePricesLink:true,
		showLease:true,
		PRODUCT_REVIEW_REQUEST_ATTRIBUTE:true,
		showAlascoreTooltip:true,
		showCnetContentCast:true,
		showCooInOverview:false,
		termlink : false,
		showInsightLogo : false,
		//added for BYOD
		cSMiniPPP:false
		};
	} 
	else if (targetWindow=='recomPPP') {
		windowConfigSpecs=
		{showProductNav : true,
		showProductBar : true,
		showProductImage : true,
		showAddCart : true,
		showDiscontinuedProduct : false,
		showTabs : true,
		showProductTitle:true,
		/*Individual product Tools*/
		showProductTools:true,
		removeTabSec: false,
		priceEstimate : false,
		productCenter : true,
		companyStandard : true,
		print : false,
		sendMail  : false,
		/*Individual Product Tools*/
		insightPrefferdManf:true,
		showEmailLink:false,
		showPrintLink:false,
		showCompareSimilarLink:true,
		showSearchSimillarProducts:true,
		showProductCenterLink:true,
		showPurchasingTools:true,
		MEMORY_SELECTOR_ATTRIBUTE:true,
		showCheckCompatibilityLink:true,
		showBrandShowcase:true,
		showRelatedProducts:false,
		RECOMMENDED_PRODUCTS_REQUEST_ATTRIBUTE:true,
		showMoreRecommendedProducts:true,
		showRecentlyViewedProducts:true,
		ACCESSORIES_REQUEST_ATTRIBUTE:true,
		REBATES_REQUEST_ATTRIBUTE:true,
		WARRANTY_REQUEST_ATTRIBUTE:true,
		showRhs: false,
		showProductTour:true,
		ALLOW_MAIN_PRODUCT_ADD_ATTR:true,
		ALLOW_ACCESSORIES_ADD_ATTR:true,
		ALLOW_OEM_WARRANTIES_ADD_ATTR:true,
		ALLOW_RECOMMENDED_PRODUCTS_ADD_ATTR:true,
		showPriceEstimate:true,
		showBullets:true,
		showLogo:true,
		showAllContractPrices:false,
		showMorePricesLink:true,
		showLease:true,
		PRODUCT_REVIEW_REQUEST_ATTRIBUTE:true,
		showAlascoreTooltip:true,
		showCnetContentCast:true,
		showCooInOverview:false,
		termlink : false,
		showInsightLogo : true,
		//added for BYOD
		cSMiniPPP:false
		};
	}
	else if(targetWindow=='miniPPP'){	
			//minippp
		var showCartOnMiniPPP = true;
		if(isCSMiniPPP){
			showCartOnMiniPPP = false;
		}
		if(isIPP == 'YES'){
		windowConfigSpecs=		
			{showProductNav: false,
			showProductBar: true,
			showProductImage: true,
			showAddCart: true,
			showDiscontinuedProduct: false,
			showTabs: true,
			showProductTitle: true,
			miniPPP: true,
			removeTabSec: false,
			/*ProductTools, enable due the feature */
			showProductTools: true,
			priceEstimate : false,
			productCenter : true,
			companyStandard : false,
			print : false,
			sendMail  : false,
			/*ProductTools, enable due the feature */
			insightPrefferdManf: true,
			showEmailLink: false,
			showPrintLink: false,
			showCompareSimilarLink: false,
			showSearchSimillarProducts: false,
			showProductCenterLink: false,
			showPurchasingTools: false,
			showRelatedProducts:false,
			MEMORY_SELECTOR_ATTRIBUTE: false,
			showCheckCompatibilityLink: false,
			showBrandShowcase: false,
			RECOMMENDED_PRODUCTS_REQUEST_ATTRIBUTE: false,
			showMoreRecommendedProducts: false,
			showRecentlyViewedProducts: false,
			ACCESSORIES_REQUEST_ATTRIBUTE: false,
			REBATES_REQUEST_ATTRIBUTE: false,
			WARRANTY_REQUEST_ATTRIBUTE: false,
			showRhs: false,
			showProductTour: false,
			ALLOW_MAIN_PRODUCT_ADD_ATTR: true,
			ALLOW_ACCESSORIES_ADD_ATTR: false,
			ALLOW_OEM_WARRANTIES_ADD_ATTR: false,
			ALLOW_RECOMMENDED_PRODUCTS_ADD_ATTR: false,
			showPriceEstimate: false,
			showBullets: false,
			showLogo: false,
			showAllContractPrices: true,
			showMorePricesLink: true,
			showLease: true,
			PRODUCT_REVIEW_REQUEST_ATTRIBUTE: false,
			showAlascoreTooltip: false,
			showCnetContentCast: false,
			showCooInOverview: false,
			termlink : true,
			showInsightLogo : true,
			//added for BYOD
			cSMiniPPP:false
			};
		}else{
			windowConfigSpecs=		
			{showProductNav: false,
			showProductBar: true,
			showProductImage: true,
			showAddCart: showCartOnMiniPPP,
			showDiscontinuedProduct: false,
			showTabs: true,
			showProductTitle: true,
			miniPPP: true,
			removeTabSec: false,
			/*ProductTools, enable due the feature */
			showProductTools: true,
			priceEstimate : false,
			productCenter : true,
			companyStandard : false,
			print : false,
			sendMail  : false,
			/*ProductTools, enable due the feature */
			insightPrefferdManf: true,
			showEmailLink: false,
			showPrintLink: false,
			showCompareSimilarLink: false,
			showSearchSimillarProducts: false,
			showProductCenterLink: false,
			showPurchasingTools: false,
			showRelatedProducts:false,
			MEMORY_SELECTOR_ATTRIBUTE: false,
			showCheckCompatibilityLink: false,
			showBrandShowcase: false,
			RECOMMENDED_PRODUCTS_REQUEST_ATTRIBUTE: false,
			showMoreRecommendedProducts: false,
			showRecentlyViewedProducts: false,
			ACCESSORIES_REQUEST_ATTRIBUTE: false,
			REBATES_REQUEST_ATTRIBUTE: false,
			WARRANTY_REQUEST_ATTRIBUTE: false,
			showRhs: false,
			showProductTour: false,
			ALLOW_MAIN_PRODUCT_ADD_ATTR: true,
			ALLOW_ACCESSORIES_ADD_ATTR: false,
			ALLOW_OEM_WARRANTIES_ADD_ATTR: false,
			ALLOW_RECOMMENDED_PRODUCTS_ADD_ATTR: false,
			showPriceEstimate: false,
			showBullets: false,
			showLogo: false,
			showAllContractPrices: true,
			showMorePricesLink: true,
			showLease: true,
			PRODUCT_REVIEW_REQUEST_ATTRIBUTE: false,
			showAlascoreTooltip: false,
			showCnetContentCast: false,
			showCooInOverview: false,
			termlink : false,
			showInsightLogo : true,
			//added for BYOD
			cSMiniPPP:false
			};
		}
	}
	else if(targetWindow=='miniPPPTest'){	
		//minippp
	var showCartOnMiniPPP = true;
	if(isCSMiniPPP){
		showCartOnMiniPPP = false;
	}
	if(isIPP == 'YES'){
	windowConfigSpecs=		
		{showProductNav: true,
		showProductBar: true,
		showProductImage: true,
		showAddCart: false,
		showDiscontinuedProduct: false,
		showTabs: true,
		showProductTitle: true,
		miniPPP: false,
		removeTabSec: false,
		/*ProductTools, enable due the feature */
		showProductTools: true,
		priceEstimate : false,
		productCenter : true,
		companyStandard : false,
		print : false,
		sendMail  : false,
		/*ProductTools, enable due the feature */
		insightPrefferdManf: true,
		showEmailLink: false,
		showPrintLink: false,
		showCompareSimilarLink: false,
		showSearchSimillarProducts: false,
		showProductCenterLink: false,
		showPurchasingTools: false,
		showRelatedProducts:false,
		MEMORY_SELECTOR_ATTRIBUTE: false,
		showCheckCompatibilityLink: false,
		showBrandShowcase: false,
		RECOMMENDED_PRODUCTS_REQUEST_ATTRIBUTE: false,
		showMoreRecommendedProducts: false,
		showRecentlyViewedProducts: false,
		ACCESSORIES_REQUEST_ATTRIBUTE: false,
		REBATES_REQUEST_ATTRIBUTE: false,
		WARRANTY_REQUEST_ATTRIBUTE: false,
		showRhs: false,
		showProductTour: false,
		ALLOW_MAIN_PRODUCT_ADD_ATTR: true,
		ALLOW_ACCESSORIES_ADD_ATTR: false,
		ALLOW_OEM_WARRANTIES_ADD_ATTR: false,
		ALLOW_RECOMMENDED_PRODUCTS_ADD_ATTR: false,
		showPriceEstimate: false,
		showBullets: false,
		showLogo: false,
		showAllContractPrices: true,
		showMorePricesLink: true,
		showLease: true,
		PRODUCT_REVIEW_REQUEST_ATTRIBUTE: false,
		showAlascoreTooltip: false,
		showCnetContentCast: false,
		showCooInOverview: false,
		termlink : true,
		showInsightLogo : true,
		//added for BYOD
		cSMiniPPP:true
		};
	}else{
		windowConfigSpecs=		
		{showProductNav: true,
		showProductBar: true,
		showProductImage: true,
		showAddCart: showCartOnMiniPPP,
		showDiscontinuedProduct: false,
		showTabs: true,
		showProductTitle: true,
		miniPPP: false,
		removeTabSec: false,
		/*ProductTools, enable due the feature */
		showProductTools: true,
		priceEstimate : false,
		productCenter : false,
		companyStandard : false,
		print : false,
		sendMail  : false,
		/*ProductTools, enable due the feature */
		insightPrefferdManf: true,
		showEmailLink: false,
		showPrintLink: false,
		showCompareSimilarLink: false,
		showSearchSimillarProducts: false,
		showProductCenterLink: false,
		showPurchasingTools: false,
		showRelatedProducts:false,
		MEMORY_SELECTOR_ATTRIBUTE: false,
		showCheckCompatibilityLink: false,
		showBrandShowcase: false,
		RECOMMENDED_PRODUCTS_REQUEST_ATTRIBUTE: false,
		showMoreRecommendedProducts: false,
		showRecentlyViewedProducts: false,
		ACCESSORIES_REQUEST_ATTRIBUTE: false,
		REBATES_REQUEST_ATTRIBUTE: false,
		WARRANTY_REQUEST_ATTRIBUTE: false,
		showRhs: false,
		showProductTour: false,
		ALLOW_MAIN_PRODUCT_ADD_ATTR: true,
		ALLOW_ACCESSORIES_ADD_ATTR: false,
		ALLOW_OEM_WARRANTIES_ADD_ATTR: false,
		ALLOW_RECOMMENDED_PRODUCTS_ADD_ATTR: false,
		showPriceEstimate: false,
		showBullets: false,
		showLogo: false,
		showAllContractPrices: true,
		showMorePricesLink: true,
		showLease: true,
		PRODUCT_REVIEW_REQUEST_ATTRIBUTE: false,
		showAlascoreTooltip: false,
		showCnetContentCast: false,
		showCooInOverview: false,
		termlink : false,
		showInsightLogo : true,
		//added for BYOD
		cSMiniPPP:true
		};
	}
}
	else if(targetWindow='printPPP'){
		windowConfigSpecs=
			{showProductNav: false,
			showProductBar: true,
			showProductImage: true,
			showAddCart: false,
			showDiscontinuedProduct: false,
			showTabs: false,
			showProductTitle: true,
			removeTabSec: true,
			showProductTools: false,
			priceEstimate : false,
			productCenter : false,
			companyStandard : false,
			print : false,
			sendMail  : false,
			
			insightPrefferdManf: true,
			showEmailLink: false,
			showPrintLink: false,
			showCompareSimilarLink: false,
			showSearchSimillarProducts: false,
			showRelatedProducts:false,
			showProductCenterLink: false,
			showPurchasingTools: false,
			MEMORY_SELECTOR_ATTRIBUTE: false,
			showCheckCompatibilityLink: false,
			showBrandShowcase: false,
			RECOMMENDED_PRODUCTS_REQUEST_ATTRIBUTE: false,
			showMoreRecommendedProducts: false,
			showRecentlyViewedProducts: false,
			ACCESSORIES_REQUEST_ATTRIBUTE: true,
			REBATES_REQUEST_ATTRIBUTE: true,
			WARRANTY_REQUEST_ATTRIBUTE: true,
			showRhs: false,
			showProductTour: false,
			ALLOW_MAIN_PRODUCT_ADD_ATTR: false,
			ALLOW_ACCESSORIES_ADD_ATTR: false,
			ALLOW_OEM_WARRANTIES_ADD_ATTR: false,
			ALLOW_RECOMMENDED_PRODUCTS_ADD_ATTR: false,
			showPriceEstimate: false,
			showBullets: true,
			showLogo: true,
			showAllContractPrices: false,
			showMorePricesLink: false,
			showLease: true,
			PRODUCT_REVIEW_REQUEST_ATTRIBUTE: false,
			showAlascoreTooltip: false,
			showCnetContentCast: false,
			showCooInOverview: false,
			termlink : false,
			showInsightLogo : false,
			//added for BYOD
			cSMiniPPP:false
			};
	}		
	else if(windowConfigSpecs='emailPPP'){
		windowConfigSpecs=
			{showProductNav: false,
			showProductBar: true,
			showProductImage: true,
			showAddCart: true,
			showDiscontinuedProduct: false,
			showTabs: true,
			showProductTitle: true,
			/*product tools*/
			showProductTools: false,
			priceEstimate : false,
			productCenter : false,
			companyStandard : false,
			removeTabSec: false,
			print : false,
			sendMail  : false,
			/*product tools */
			insightPrefferdManf: true,
			showEmailLink: false,
			showPrintLink: false,
			showCompareSimilarLink: false,
			showSearchSimillarProducts: false,
			showRelatedProducts:false,
			showProductCenterLink: false,
			showPurchasingTools: false,
			MEMORY_SELECTOR_ATTRIBUTE: false,
			showCheckCompatibilityLink: false,
			showBrandShowcase: false,
			RECOMMENDED_PRODUCTS_REQUEST_ATTRIBUTE: false,
			showMoreRecommendedProducts: false,
			showRecentlyViewedProducts: false,
			ACCESSORIES_REQUEST_ATTRIBUTE: false,
			REBATES_REQUEST_ATTRIBUTE: false,
			WARRANTY_REQUEST_ATTRIBUTE: true,
			showRhs: false,
			showProductTour: false,
			ALLOW_MAIN_PRODUCT_ADD_ATTR: false,
			ALLOW_ACCESSORIES_ADD_ATTR: false,
			ALLOW_OEM_WARRANTIES_ADD_ATTR: false,
			ALLOW_RECOMMENDED_PRODUCTS_ADD_ATTR: false,
			showPriceEstimate: false,
			showBullets: true,
			showLogo: true,
			showAllContractPrices: false,
			showMorePricesLink: false,
			showLease: true,
			PRODUCT_REVIEW_REQUEST_ATTRIBUTE: false,
			showAlascoreTooltip: false,
			showCnetContentCast: false,
			showCooInOverview: true,
			termlink : false,
			showInsightLogo : false,
			//added for BYOD
			cSMiniPPP:false
			};
	}
	return windowConfigSpecs;
}
/**/

function showPppToolTipRhs(targetDiv, sourceDiv) {

	var pos = $(sourceDiv).offset();
	/*$("#" + targetDiv).css({
		"top" : +(pos.top + 10) + "px",
		"left" : +pos.left + "px"
	});*/
	targetDiv = InsightSearch.getContractDivId(targetDiv);	
	$("#" + targetDiv).css({
		"top" : "10px",
		"left" : "8px"
		
	});
	$("#" + targetDiv).show();
}

function showPppToolTipRhs1(targetDiv, sourceDiv) {

	var pos = $(sourceDiv).offset();
	/*$("#" + targetDiv).css({
		"top" : +(pos.top + 10) + "px",
		"left" : +pos.left + "px"
	});*/
	
	targetDiv = InsightSearch.getContractDivId(targetDiv);	
	$("#" + targetDiv).css({
		
	});
	$("#" + targetDiv).show();
}

function hidePppToolTipRhs(targetDiv, sourceDiv, toppos, leftpos) {
	targetDiv = InsightSearch.getContractDivId(targetDiv);	
	$("#" + targetDiv).hide();
}

function showPppToolTip(targetDiv, sourceDiv, toppos, leftpos) {
	targetDiv = InsightSearch.getContractDivId(targetDiv);	
	var pos = $(sourceDiv).offset();
	$("#" + targetDiv).css({
		"top" : (pos.top-toppos), //65
		"left" : (pos.left-leftpos) //40
	});
	$("#" + targetDiv).show();
}

function hidePppToolTip(targetDiv, sourceDiv) {
	targetDiv = InsightSearch.getContractDivId(targetDiv); 	
	$("#" + targetDiv).hide();
}


function addToProductCenterProductInfo(materialId, description)
{
	var jsonfirst='{"locale": "en_US",'+            
    '"fromClp": false,'+
    '"salesOffice": "2001",'+
    '"salesOrg": "2400",'+
    '"defaultPlant": "10",'+
    '"onlyOpenMarket": false,'+
    '"pageSize": 0,'+
    '"pageNumber": 0,'+
    '"fieldList": [],'+
    '"sort": "BestSellers",'+
    '"rebateSearch": false,'+
    '"defaultSort": true,'+
    '"useBreadcrumb": false,'+
    '"materialIds": [';
    
	var jsonlast='],'+
	    '"secure": false,'+
	    '"displayOpenMarket": false}';
	var jsonObj = jsonfirst+'"'+materialId+'"'+jsonlast;
	
	var url="/insightweb/productCenter/insertProducts";
	
	var data = InsightCommon.getServiceResponse(url,jsonObj,"POST");
	if(data.length===0)
	{
		var searchLabels = InsightSearch.loadSearchLabels();

		var successObj = $.extend( {'labels':searchLabels.labels}, {'materialId':materialId}, {'description':description});
		var prodCenterSuccessMsgPopUpTemplate = InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/prodCenterSuccessMsgPopUpTemplate.html");
		$.template("prodCenterSuccessMsgPopUpTemplate", prodCenterSuccessMsgPopUpTemplate);
		$("#productInfoPopUpDiv").html("");
		InsightCommon.renderTemplate("prodCenterSuccessMsgPopUpTemplate", successObj, "#productInfoPopUpDiv");
		
		showSuccessMessage("#productInfoPopUpDiv");
		
	}
}

function showSuccessMessage(popUpDivId)
{
	$(popUpDivId).css({"position":"absolute", "top":"39%", "left":"54%", "z-index":"1"});
	$(popUpDivId).show();
	setTimeout(function() {$(popUpDivId).hide();  
	}, 5000); // <-- time in milliseconds
}

function closeSuccessMessage(popUpDivId)
{
	$(popUpDivId).hide();
}

function addToSetInProductInfo(materialId){
	var grpName = InsightSearch.groupName; 
	var catId = InsightSearch.categoryId;
	catId = parseInt(catId);
	if(isNaN(catId))
	{
		catId=0;
	}
	var stId = InsightSearch.setId;
	var grpId = InsightSearch.groupId;
	grpId = parseInt(grpId);
	if(isNaN(grpId))
	{
		grpId = 0;
	}
	var shared = InsightSearch.shared;
	if(shared=='true')
	{
		shared=true;
	}
	else
	{
		shared=false;
	}
	var controller = InsightSearch.controller;
	if(controller=='true')
	{
		controller=true;
	}
	else
	{
		controller=false;
	}
	var qty = 1;
	var matList = [];
	if(materialId!=null && materialId.length > 0){
	    var item = materialId;
	    matList.push(item);
	}   
	var requestObj = {'categoryId':catId, 'controller':controller,'setId':stId};
	requestObj = $.extend(requestObj, {'groupName':grpName, 'groupId':grpId});
	requestObj = $.extend(requestObj, {'shared':shared, 'qty':qty, 'matList':matList});
	
	requestObj = JSON.stringify(requestObj);
	
	var serviceURL = "/insightweb/manage/addItemsToSet";
	
	var responseObj = InsightCommon.getServiceResponse(serviceURL, requestObj, 'POST');
	
	var addToSetSuccess = false;
	var addToSetDuplicate = false;
	var dupProdString = null;
	var invalidParts = null;
	
	if(responseObj.items.addedMaterialIds!=null && responseObj.items.addedMaterialIds!=""){
		addToSetSuccess = true;
	}
	if(responseObj.items.duplicateMaterialIds!=null && responseObj.items.duplicateMaterialIds!=""){
		addToSetDuplicate = true;
		dupProdString = responseObj.items.duplicateMaterialIds;
	}
	if(responseObj.items.invalidMaterialIds!=null && responseObj.items.invalidMaterialIds!=""){
		invalidParts = responseObj.items.invalidMaterialIds;
	}
	
	if(InsightNavigation.isEadminUser && window.opener != null){
	window.opener.editConfigButton(stId,grpId);
	if(responseObj.items.addedMaterialIds!=""){
		window.opener.updateSuccessComment();}
	if(responseObj.items.duplicateMaterialIds!=""){
		var commentTagDiv = window.opener.$("#commentTag");
		$(commentTagDiv).append("<h2 class='alertError'>"+ dupProdString+" " + searchLabels.labels.duplicateProductComment +"</h2>");
	}
	if(responseObj.items.invalidMaterialIds!=""){
		var commentTagDiv = window.opener.$("#commentTag");
		var invalidPart = invalidParts.toString();
		 $(commentTagDiv).append("<h2 class='alertError'>"+ searchLabels.labels.compStdInvalidMaterialID + " "+invalidPart+"</h2>");
	}
	window.close();
	window.opener.focus();
}
else{
		document.location.href="http://www.insight.com/insightweb/search#manageStandardsAddToSet#setId="+stId+"#groupId="+grpId+"#addToSetSuccess="+addToSetSuccess+"#addToSetDuplicate="+addToSetDuplicate+"#dupProdString="+encodeURIComponent(dupProdString)+"#invalidParts="+encodeURIComponent(invalidParts);
	}
}

function addToCompanyStandardsProductInfo(materialId)
{
	var jsonObj = '{"user":{}}';
	var setUrl = "/insightweb/account/companyStandardSets";
	var responseData = InsightCommon.getServiceResponse(setUrl, jsonObj, "POST");
	globalCompanyStandards = responseData;
	var companyStandardsData = $.extend({'companyStandards':responseData}, {'labels':searchLabels.labels});
	var addToCompanyStandardsPopUpTemplate = InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/addToCompanyStandardsPopUp.html");
	$.template("addToCompanyStandardsPopUpTemplate", addToCompanyStandardsPopUpTemplate);
	$( "#productInfoPopUpDiv" ).remove();
    $('body').append('<div id="productInfoPopUpDiv"></div>'); 
	
	
	$("#productInfoPopUpDiv").attr("title", searchLabels.labels.addToCompanyStandardsPopUpHeading);
	$("#productInfoPopUpDiv").html("");
	InsightCommon.renderTemplate("addToCompanyStandardsPopUpTemplate", companyStandardsData, "#productInfoPopUpDiv");
	
	$("#productInfoPopUpDiv").dialog({ 
		buttons: {
			"Cancel":function(){ $(this).dialog("close");},
			"Add":function(){productInfoAddToCompanyStandardsBtn(materialId);}
			},
			height: 500,
			width: 400,
			resizable: true,
			draggable: false
		});
	
	
}

function productInfoAddToCompanyStandardsBtn(materialId){
	var productSets = [];
	var selectedSets = [];
	$("#popUpDialogBody input:checked").each(function (index, item){
		productSets[index] = $(this).val();
		selectedSets[index] = $(this).attr('id');
	});
	
	
	
	
	if(productSets.length!=0)
	{
        var ESDProduct ="";
        var nonESDProduct="";
       
      		  $.each(globalCompanyStandards, function(index,value)
      			{
      			  $.each(value.csgroups, function(subIndex,subValue)
      			   {
      				  $.each(subValue.cssets, function(cssetsIndex,cssetsValue)
      					{
      					  for(var i=0;i<productSets.length;i++)
      					  	if(cssetsValue.id == productSets[i])
      					  		if(cssetsValue.setType == "1" || cssetsValue.setType =="2")
      					  			{
      					  			  if(globalProductInfoResponse.webProduct.taxMaterialGroup == "310")
      					  			  ESDProduct = ESDProduct+ productSets[i]+" ";
      					  			else
          					  			nonESDProduct = nonESDProduct+ productSets[i]+" ";
      					  			}
      					  		else
      					  			nonESDProduct = nonESDProduct+ productSets[i]+" ";
      					  			
      	        		});
      			   });
      		   });  
      		
		
	if(nonESDProduct)
	{
	var requestObj = '{"materialSet":{';
	$.each(productSets, function(index, prodSet){
		
		requestObj = requestObj+'"'+prodSet+'":["'+materialId+'"]';
		if(index < productSets.length-1)
			requestObj = requestObj+',';
	});
	
	// get set types of selected sets
	requestObj = requestObj+'},"setType":{';
	if(globalCompanyStandards != null && globalCompanyStandards.length > 0){
		$.each(selectedSets, function(index0,selectedSet){
			$.each(globalCompanyStandards, function(index1,category){
				if(selectedSet.split('_')[0] == category.id){
					if(category.csgroups!=null && category.csgroups.length > 0){
						$.each(category.csgroups, function(index2,group){
							if(selectedSet.split('_')[1] == group.groupId){
								if(group.cssets !=null && group.cssets.length > 0){
									$.each(group.cssets, function(index3,set){
										if(selectedSet.split('_')[2] == set.id){
											requestObj = requestObj+'"'+set.id+'":'+set.setType; 
											if(index0 < selectedSets.length-1)
												requestObj = requestObj+',';
										}
									});
								}
							}
						});
					}
				}
			});
		});
	}
		
	requestObj = requestObj+'}}';
	
      var responseObj = InsightCommon.getServiceResponse("/insightweb/account/companyStandard/insertProduct", requestObj, "POST");
	}
 		  
        		  
          var companyStandardsSuccessPopUpTemplate = InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/companyStandardsSuccessPopUpTemplate.html");
		  $.template("companyStandardsSuccessPopUpTemplate", companyStandardsSuccessPopUpTemplate);
    	  var jsonObj = $.extend({'labels':searchLabels.labels}, {'materialIds':materialId}, {'productInfo':true},{'globalProductInfoResponse':globalProductInfoResponse},{'responseObj':responseObj});
    	  $("#PDNotificationTimeOutDiv").html("");
    	  InsightCommon.renderTemplate("companyStandardsSuccessPopUpTemplate", jsonObj, "#PDNotificationTimeOutDiv");
    	 
          if(responseObj && responseObj.status && responseObj.statusMessage=='')
          {     
          $("#productInfoPopUpDiv").dialog("close");
          $( "#productInfoPopUpDiv" ).remove();      
          $("#nonESD").css('display','block');
          window.scrollTo(0, 0);
           $("#PDNotificationTimeOutDiv").css({'position':'absolute', 'top':'23%', 'left':'57%','width':'auto',  'z-index':'999'});
           $("#PDNotificationTimeOutDiv").show();
          setTimeout(function() {$("#PDNotificationTimeOutDiv").hide();
           }, 2000); // <-- time in milliseconds
          }
       else if(responseObj && responseObj.status && responseObj.statusMessage!=''){
     	  $("#productInfoPopUpDiv").dialog("close");
     	  $( "#productInfoPopUpDiv" ).remove();
           $("#DuplicateMessages").css('display','block');
           window.scrollTo(0, 0);
            $("#PDNotificationTimeOutDiv").css({'position':'absolute', 'top':'23%', 'left':'57%','width':'auto', 'z-index':'999'});
            $("#PDNotificationTimeOutDiv").show();
            
           setTimeout(function() {$("#PDNotificationTimeOutDiv").hide();
            }, 2000); 
       }      
       else{
          $("#productInfoPopUpDiv").dialog("close");
          $( "#productInfoPopUpDiv" ).remove();
       }
         
	}
	
	
}
function showAllRecommendedProducts(){	
	$("#bodyContent" ).css("display","none");
	$("#showRecommendedPdctsContainer").empty();
	var recommendedProductsTemplate = InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/recommemdedProductsTemplate.html");
    $.template( "recommendedProductsTemplate", recommendedProductsTemplate);
    
    var searchLabels = InsightSearch.loadSearchLabels();

     markup = $.extend(jsonRecommendedResponseObj, {'labels':searchLabels.labels});
    InsightCommon.renderTemplate( "recommendedProductsTemplate",markup, "#showRecommendedPdctsContainer"); 
    $('#icstracking').attr('src',"https://"+icsDomainUrl+icsViewUrl+"ts="+InsightCommon.getCurrentTimestamp()+"&csid=" +recommendedCSidValues+"&sid="+Insight.cacheSessionId);       
}

function removeDuplicateProd(recentProds, val) {
	var arr = recentProds.split("~");
	var newArr=null;
	var prodCount = 0; 
    for(var i=0; i<arr.length; i++) {
        if(arr[i] == val) {
            arr.splice(i, 1);
            //break;
        }else{
        	if(i!=arr.length-1){
        		if(prodCount<4){
	        		if(newArr!=null){
	        			newArr = newArr+"~"+arr[i];
	        			prodCount++;
	        		}else{
	        			newArr = arr[i];
	        			prodCount++;
	        		}
        		}else{
        			break;
        		}
        	}
        }
    }
    return newArr;
}

function populateRecentlyViewedProducts(recentProds,currentprod){
	var productSrchlabels = InsightSearch.loadSearchLabels();	
	//var materialIds = removeDuplicateProd(recentProds,currentprod);
	var materialIds = recentProds; 
	var materialIdsArray = recentProds.split("~");

	if (materialIdsArray.length > 4) {
		materialIds = materialIds.split("~").slice(0,4).toString().replace(/,/g, "~");//remove last elements of array
		materialIdsArray = materialIdsArray.slice(0,4); //remove last elements of array
	}
	
	
	var jsonRequestObj = null;
	if (materialIdsArray.length > 1) {
		jsonRequestObj = materialIds.replace(/~/g, "\",\"");
		jsonRequestObj = '"' + jsonRequestObj + '"';
		jsonRequestObj = '{"showSuggestedSearch":false,"locale":"en_US","searchOpenMarket":false,"showApprovedItemOnlyLink":false,"showProductCompare":false,"showProductCenterLink":false,"catalogData":{"customCatalogId":null,"overrideCatalogId":null,"approvedItemsCatalogId":null,"purchasableCatalogSet":null,"filterManufacturerCatalogId":null},"fromClp":false,"salesOffice":"2001","salesOrg":"2400","defaultPlant":"10","onlyOpenMarket":false,"pageSize":0,"pageNumber":0,"fieldList":[],"sort":"BestSellers","rebateSearch":false,"defaultSort":true,"useBreadcrumb":false,"materialIds":['
				+ jsonRequestObj
				+ '],"secure":false,"displayOpenMarket":false}';
	} else {
		if (materialIdsArray.length == 1) {
			jsonRequestObj = '{"showSuggestedSearch":false,"locale":"en_US","searchOpenMarket":false,"showApprovedItemOnlyLink":false,"showProductCompare":false,"showProductCenterLink":false,"catalogData":{"customCatalogId":null,"overrideCatalogId":null,"approvedItemsCatalogId":null,"purchasableCatalogSet":null,"filterManufacturerCatalogId":null},"fromClp":false,"salesOffice":"2001","salesOrg":"2400","defaultPlant":"10","onlyOpenMarket":false,"pageSize":0,"pageNumber":0,"fieldList":[],"sort":"BestSellers","rebateSearch":false,"defaultSort":true,"useBreadcrumb":false,"materialIds":["'
					+ materialIdsArray[0]
					+ '"],"secure":false,"displayOpenMarket":false}';
		}
	}
	var url="/insightweb/compareProducts";
	InsightCommon.getServiceResponseAsync(url,jsonRequestObj,"POST",function(productsResponse){ 
		var extendedData = {getPrice: function(webProduct,source){
			return getPriceFromWebProduct(webProduct,source,productSrchlabels.labels);
	 		}
		};
			productsResponse = $.extend(productsResponse,extendedData);
			var searchLabels = InsightSearch.loadSearchLabels();
			productsResponse = $.extend(productsResponse, {'labels':searchLabels.labels});
			var recentlyViewedProductsTemplate = InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/recentlyViewedProductsTemplate.html");
			$.template( "recentlyViewedProductsTemplate", recentlyViewedProductsTemplate);
			InsightCommon.renderTemplate( "recentlyViewedProductsTemplate",productsResponse, "#recentlyViewed");
	});
}

/***Cookie Functions**/
function addNRemoveCookies(recentProds, currentProd){
	if(currentProd!=""){
	var currentProductIndex = recentProds.indexOf(currentProd);
	if(currentProductIndex>=0){
		
		var prefixCookies = recentProds.substring(0,currentProductIndex-1);
		var suffixCookies = recentProds.substring(currentProductIndex+currentProd.length+1); 
		recentProds = '';
		if(prefixCookies.length>0){
			recentProds = prefixCookies;
		}
		if(suffixCookies.length>0){
			if(recentProds.length>0){
				recentProds = recentProds + '~' + suffixCookies;
			}else{
				recentProds = suffixCookies;
			}
		}
	}
	if(recentProds.length>0){
		recentProds = currentProd + '~' + recentProds;
	}else{
		recentProds = currentProd;
	}
	}
	
	setCookie("recentlyViewedProducts",recentProds,"365");
}
function getCookie(c_name)
{
var i,x,y;
var domCookie;
var valList;

	if($.cookie('cr_name')!=null){
		domCookie = $.cookie('cr_name');
		valList = domCookie.split(";");
		
		for (i=0;i<valList.length;i++)
		  {
		  x=valList[i].substr(0,valList[i].indexOf("="));
		  y=valList[i].substr(valList[i].indexOf("=")+1);
		  x=x.replace(/^\s+|\s+$/g,"");
		  if (x==c_name)
		    {
			  return unescape(y);
		    }
		  }
	}
}

function setCookie(c_name,value,exdays)
{
	var exdate=new Date();
	exdate.setDate(exdate.getDate() + exdays);
	var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
	var cr_value = c_name + "=" + c_value;
	$.cookie('cr_name',cr_value, { expires: Insight.envConfigs.defaultLocaleExpire, path: "/", domain: ".insight.com"});
}

/*******Cookied Function **************/
/********popup*************************/
function openMiniPPP(materialId, targetWindow, sourceWindow, ippProduct) {
	var materialId1=materialId;
	var miniWindowleft = ($(window).width - 760)/2;
	var miniWindowheight = ($(window).height - 600)/2;
	var url =null;
	var isIPP='NO';
	if(typeof ippProduct!=='undefined' && ippProduct=='ippProduct')
		isIPP ='YES';
	url= encodeURI(InsightCommon.getRootURL()+"/insightweb/"+'productinfo?materialId='+encodeURIComponent(materialId)+'&targetWindow='+targetWindow+'&ipp='+isIPP+"&isPopUp=1&fromCS=false");
	minipppWindow = window.open(url, '', 'width=790px, height=600px, left='+miniWindowleft+',top='+miniWindowheight+' ,scrollbars=yes',true);
}

function openPrintMiniPPP(materialId, targetWindow, sourceWindow, ippProduct) {
	
	var miniWindowleft = ($(window).width - 760)/2;
	var miniWindowheight = ($(window).height - 600)/2;
	var url =null
	var isIPP='NO';
	if(typeof ippProduct!=='undefined' && ippProduct=='ippProduct')
		isIPP ='YES';

	url= encodeURI('productinfo?materialId='+encodeURIComponent(materialId)+'&targetWindow='+targetWindow+'&ipp='+isIPP+'&fromCS=false');
	var loginURL = InsightCommon.getRootURL() + "/insightweb/"+url;
	window.open(loginURL);
}

function loadPPP(materialId){
	productID = encodeURIComponent(materialId);
	window.opener.location = 'http://www.insight.com/insightweb/search#!ProdInfoMtrId='+productID+'~fromSearch';
	window.close();
	window.opener.focus();
	
}
/********popup*************************/
function productInfoEmail(){
  $( "#bodyContent" ).empty();
  var emailTempColleague = InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/transaction/sendtocolleaguetemplate.html");
  $.template( "emailTempColleague", emailTempColleague);
  var data =  {"fromPage":"productInfo"};
  var srchLabels = InsightCommon.getMessageContent(messageUrl, Insight.locale, "/messages/transaction/ShoppingCart/shoppingcartlabels.txt/jcr:content/jcr:data.txt");
  searchLabels = eval('(' + srchLabels + ')');
  data = $.extend(data, {'labels':searchLabels.labels});
  InsightCommon.renderTemplate( "emailTempColleague", data, "#bodyContent"); 	
}

function validateSendToCollegue(){
	 var yourNameInput=$('#yournameinput').val();
	 var yourEmailInput=$('#youremailinput').val();
	 var yourRecipientEmailInput=$('#recipientemailinput').val();
	 
	 var errors=[];
	 var i=0;
	 
	 if(yourNameInput=="")
	 {
	 errors[i]="1.Please enter your name.";
	 i++;
	 }
	 if(yourEmailInput=="")
	 {
	 errors[i]="2. Please ensure your Email address is formatted correctly.";
	 i++;
	 }
	 if(yourRecipientEmailInput=="")
	 {
	 errors[i]="3. Please ensure recipient's Email address is formatted correctly.";
	 i++;
	 }
	 return errors;
	
}

function sendProductInfoEmail(fromPage){
	 var errors=validateSendToCollegue();
	 var errorMessage="";
	 if(errors.length>0)
	 {
		$.each(errors, function(index,value)
		{
			errorMessage=errorMessage+value+"\n";
		});
		alert(errorMessage);
	 }
	 else{
		var jsonTemplate= InsightCommon.getContent(InsightSearch.staticContentUrl,"", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/compareProductEmailJsonObject.txt");
		jsonRequestObj = eval('(' + jsonTemplate + ')');
		jsonRequestObj.from=$("#youremailinput").val();
		jsonRequestObj.to=$("#recipientemailinput").val();
		jsonRequestObj.subject= $("#yournameinput").val() + ' has emailed you this message from Insight';
		var emailInfo = { 
							"fromName" : $("#yournameinput").val(),
							"from" : $("#youremailinput").val(),
							"mailto"	 : $("#recipientemailinput").val(),
							"mailComment" : $("#yourcommentsinput").val(),
							"fromPage" : fromPage
						 }; 
		var productInfoTemplateEmailJsonObj = $.extend(jsonResponseObj, {'emailInfo':emailInfo});
		$("#bodyContent" ).empty();
	    var ProductInfoTemplate1 = InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/productInfoEmailTemplate.html");
	    $.template( "ProductInfoTemplate_Email", ProductInfoTemplate1);
	    InsightCommon.renderTemplate( "ProductInfoTemplate_Email",productInfoTemplateEmailJsonObj, "#bodyContent");
	   // $('#ProductInfoEmailDiv').hide();
	    $('.hideForEMail').hide();
	    jsonRequestObj.content = $("#ProductInfoEmailDiv").html();
		 var url ="/insightweb/report/sendEmail";    
	     var  jsonStr = JSON.stringify(jsonRequestObj); jsonStr=jsonStr+'"}';     
	     var data = InsightCommon.getServiceResponse(url,jsonStr,"POST");
	     alert(data.response);
	     $('#ProductInfoEmailDiv,.hideForEMail').show();
	    
		}
	}

function clearform(){
	$('#yournameinput').val("");
	$('#youremailinput').val("");
	$('#recipientemailinput').val("");
	$('#yourcommentsinput').val("");
}

function showPriceEstimateDiv(materialId){   

	openEstimateDiv();
	var data=null;
    showPriceEstimateDialog(materialId,data);         
   
}

function openEstimateDiv(){   

	$("#showPriceEstimateDialog").html("");
    $("#showPriceEstimateDialog").dialog({ width: 470, resizable: false, overlay: { background: "#F2F2F2" },      
        close: function (event, ui) {
            if (event.originalEvent && $(event.originalEvent.target).closest(".ui-dialog-titlebar-close").length) {

            }
        }
    }).find("button").click(function () {
        $(this).closest(".ui-dialog-content").dialog("close");
    });        
	$("#showPriceEstimateDialog").html($("#loading").html());
}

function showPriceEstimateDialog(materialId,data)
{          
	var url = '/insightweb/getProductInfo';//$('#url').val();   
	var contractId = null;	
	var jsonRequestObj = populateProductInfoJSONObject(materialId,contractId,true);
	var serviceResponseObj;
	if (jsonRequestObj != null)
		jsonResponseObj = getJSONResponse(url, jsonRequestObj);	
	getShowPriceEstimateDialog(data,jsonResponseObj);
	    
}


function getShowPriceEstimateDialog(data1,jsonResponseObj)
{
	
     var markup = jsonResponseObj;   
     markup = $.extend(markup,{"shippingData":data1},{'imgServer':imageServerURL});
     var searchLabels = InsightSearch.loadSearchLabels();
     markup = $.extend(markup, {'labels':searchLabels.labels});
     var  showPriceEstimateDialogData = InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/priceEstimator.html");                   
     $.template( "showPriceEstimateDialogData",showPriceEstimateDialogData); 
     var price;
     if(markup.webProduct.prices[0].price.indexOf("$") === 0){
    	 price= parseFloat(markup.webProduct.prices[0].price.substr(1).replace(/\,/g,''));
     }
     else{
    	 price= parseFloat(markup.webProduct.prices[0].price.replace(/\,/g,''));
     }
     
     var totalCost=null;
      if(data1!=null)
      {
    	  
    	  if(Insight.locale == 'en_US') {
    		  		totalCost = price + data1.cost;
    	  } else if (Insight.locale == 'en_CA' || Insight.locale == 'fr_CA' || Insight.apac) {
    		  if(data1.fees != null && data1.fees != "" && data1.fees != undefined && data1.fees.freightCost != null && data1.fees.freightCost !="" && data1.fees.freightCost !=undefined) {
    			  	totalCost = price + data1.fees.freightCost;
    		  } else {
    			  	totalCost = price + data1.cost;
    		  }
    	  }
    	  
    	  
    	  
    	  if(data1.fees != null && data1.fees.taxes != null)
    	  {
    	  	if(Insight.locale == 'en_US' || Insight.apac)
    	  	{
    	  		totalCost = totalCost+data1.fees.taxes.tax;	
    	  	}
    	  	else if (Insight.locale == 'en_CA' || Insight.locale == 'fr_CA')
    	  	{
    	  		totalCost = totalCost + data1.fees.taxes.gstHstTax + data1.fees.taxes.pstTax + data1.fees.ewr;
    	  	}
    	   
    	  }
      }
      markup = $.extend(markup, {'totalCost':totalCost});
     
  	$("#showPriceEstimateDialog").html("");
      InsightCommon.renderTemplate( "showPriceEstimateDialogData",markup, "#showPriceEstimateDialog");  
     
}

function estimateShippingTax(meterialId){
	var zip = $('#pppPriceEstZipCode').val(); 
	var estimateDialog = $("#showPriceEstimateDialog");
	if (estimateDialog){
		$("#showPriceEstimateDialog").dialog("close");	
		openEstimateDiv();
	}
	if(InsightCommon.validateZip(zip))
	{
		zip = InsightCommon.formatZip(zip);
		$('#totalPriceEstimatePanel').hide();
		$('#bd').css('display','block');
		var url = '/insightweb/transaction/getFreightForProduct';        	
		var jsonObj = '{"materialID": "'+meterialId+'","zipCode" : "'+zip+'"}';
		jsonObj = getJSONResponse(url, jsonObj);	
/*		if(Insight.userPermissions!=null)
		{
*/			url='/insightweb/transaction/taxEstimate';
			var jsonObjTaxReq = '{"materialID": "'+meterialId+'","zipCode" : "'+zip+'"}';
			jsonObjTaxReq = getJSONResponse(url, jsonObjTaxReq);			
			jsonObj = $.extend(jsonObj,{"fees":jsonObjTaxReq});			
//		}
	
		showPriceEstimateDialog(meterialId,jsonObj);
	}
	else
	{
		alert('Please enter a valid zip code : '+zip);
		$('#pppPriceEstZipCode').val("");
		showPriceEstimateDialog(meterialId,null);
	}
	
}


/*****/

function showSellRequirementDiv(materialId){   

	$("#showPriceEstimateDialog").html("");
	$("#showPriceEstimateDialog").dialog({ title:"Sell Requirements" , width: 470, resizable: false, overlay: { background: "#F2F2F2" },   
        close: function (event, ui) {
            if (event.originalEvent && $(event.originalEvent.target).closest(".ui-dialog-titlebar-close").length) {
            }
        }
    }).find("button").click(function () {
        $(this).closest(".ui-dialog-content").dialog("close");
    });
    var data = null;	
    showSellRequirementDialog(materialId,data);         
}

function showSellRequirementDialog(materialId,data)
{          
           
	var url = '/insightweb/getProductInfo';//$('#url').val();
	var contractId = null;
	
	var jsonRequestObj = populateProductInfoJSONObject(materialId,contractId,true);
	var serviceResponseObj;
	if (jsonRequestObj != null)
		jsonResponseObj = getJSONResponse(url, jsonRequestObj);	
	getShowSellRequirementDialog(data,jsonResponseObj);
	    
}


function getShowSellRequirementDialog(data1,jsonResponseObj)
{
      
	 var markup = jsonResponseObj;   
	 markup = $.extend(markup,{"shippingData":data1},{'imgServer':imageServerURL});
	 var searchLabels = InsightSearch.loadSearchLabels();
     markup = $.extend(markup, {'labels':searchLabels.labels});
     var  showPriceEstimateDialogData = InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/sellRequirements.html");                   
     $.template( "showPriceEstimateDialogData",showPriceEstimateDialogData);     
     InsightCommon.renderTemplate( "showPriceEstimateDialogData",markup, "#showPriceEstimateDialog");  
     
}

/*****/


function viewContractPricing(materialId){	
	var contractId = null;
	var searchLabels = InsightSearch.loadSearchLabels();
	var url = '/insightweb/getCachedContractPrices/'+InsightCommon.getCurrentTimestamp();
		
		var materialIdToExtend = '"materialId" : "'+materialId+'"';
		var morePricesFlag = '"morePricesFlag" : '+JSON.stringify({"morePricesFlag":true});
		var jsonObj= '{'+morePricesFlag+','+ materialIdToExtend + '}';
		var reqType = 'POST';
		var jsonResponseObj =  InsightCommon.getServiceResponse(url,jsonObj,reqType);

	var contractPricingPopUpTemplate = InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/search/contractPricingPopUpTemplate.html");
	$.template("contractPricingPopUpTemplate", contractPricingPopUpTemplate);
	$("#productInfoPopUpDiv").attr("title", searchLabels.labels.contractPricingPopUpHeading);
	$("#productInfoPopUpDiv").html("");
	var data = $.extend({'webProduct':jsonResponseObj[0]}, {'labels':searchLabels.labels});
	InsightCommon.renderTemplate("contractPricingPopUpTemplate", data, "#productInfoPopUpDiv");
	
	productInfoPopUpDiv = $("#productInfoPopUpDiv").dialog({ 
		height: "auto",
		width: "500px",
		resizable: true,
		draggable: true
	});
}

/****************************/
function getShowReturn(){
	if ($("#returnSearchType").val() == "IBO") {
		return 'IB';
	} else if ($("#returnSearchType").val() == "SEARCH") {
		return 'search';
	} else {
		return 'search'
	}
	
}
/****************************/

function showCompatibilityPopup(){
	$("#compatibilityDialog").html($("#compatibilityDiv").html());
	$("#compatibilityDialog").dialog({ 
		title : "Compatible Products",
		height: 400,
		width: 530,
		resizable: false,
		draggable: true
	});
}

function addtoOrderMorePrices(meterialId){	
		$(productInfoPopUpDiv).dialog("destroy");
		if(Insight.userPermissions){			 
			if($.inArray('enable_purchase_popup',Insight.userPermissions)==-1 && $.inArray('inventory_blowout',Insight.userPermissions)==-1)
				addTocartFromNoPop(meterialId);					
			else{
				addTocartFromProductInfo(meterialId);
			}
		}	
		else
			addTocartFromProductInfo(meterialId);		
 
}
/** method took from product info template to accomidate add to cart functionality from view cart page
 * replaced to take material id from template to pass in 
 */
function submitPPPSubmission(tab,materialId,vsppProduct,targetWindow){

	if (vsppProduct != null) {
		if (vsppProduct)
		{
		    var rootURL = InsightCommon.getRootURL();

		    window.location = rootURL + "/insightweb/vsppcalculator";

		    return;
		}
	}

	var childmaterialID=materialId;
	if(tab=='main'){
		var quantity = $("#main_prod_qty").val();
		if(quantity < 1){
			alert(labels.PPPEnterQuantityGreaterThanZero);
			return false;
		}
	}	
	else if(tab=='accessories'){ 
	var fields = $("div.pppCategoryProductCell input[name^='accessories_qty_']").serializeArray();
	var oemQty =  $.map(fields, function(field, i){
	      if(IsNumeric(field.value))return (field.value);
	    }).join(",");
		var oemQtyArr =[];
		if(oemQty.length!==0)
		oemQtyArr = oemQty.split(',');
		if(oemQtyArr.length==0){
			alert(labels.EnterQuantityGreaterThanZero);
			return false;
		}
		if(oemQtyArr.length>0){
			for(var i=0;i<oemQtyArr.length;i++){
				if(oemQtyArr[i]<1){
					alert(labels.EnterQuantityGreaterThanZero);
			          return false;
				}
			}
		}
	}
	else if(tab=='oem'){	
	var fields = $("div.oemWarrantyProductQuantityCell input[name^='accessories_qty_']").serializeArray();
	var oemQty =  $.map(fields, function(field, i){
	      if(IsNumeric(field.value))return (field.value);
	    }).join(",");
		var oemQtyArr =[];
		if(oemQty.length!==0)
		oemQtyArr = oemQty.split(',');
		if(oemQtyArr.length==0){
			alert(labels.EnterQuantityGreaterThanZero);
			return false;
		}
		if(oemQtyArr.length>0){
			for(var i=0;i<oemQtyArr.length;i++){
				if(oemQtyArr[i]<1){
					alert(labels.EnterQuantityGreaterThanZero);
			          return false;
				}
			}
		}
		}
		if(tab=='main'){
			if(Insight.userPermissions){
				 $("#productInfoPopUpDiv").dialog("close");
				if($.inArray('enable_purchase_popup',Insight.userPermissions)==-1 && $.inArray('inventory_blowout',Insight.userPermissions)==-1)
					addTocartFromNoPop(materialId);					
				else{
					if(childmaterialID!=undefined && childmaterialID!='' && childmaterialID!=null){
							addTocartFromProductInfo(childmaterialID,targetWindow);
						}
					else{
							addTocartFromProductInfo(materialId,targetWindow);
						}
				}
			}	
			else{
					if(childmaterialID!=undefined && childmaterialID!='' && childmaterialID!=null){
						addTocartFromProductInfo(childmaterialID,targetWindow);
					}
					else{
						addTocartFromProductInfo(materialId,targetWindow);
					}	
				}
		}
		else
			getAllItemsIncart();			
}

function sameWarrContract(webProduct, warrItem ) {	
	if (webProduct != null && webProduct.contractDisplayInfo != null && warrItem.contractDisplayInfo.contractId != null) {
		if(webProduct.contractDisplayInfo.contractId != warrItem.contractDisplayInfo.contractId) {
			return false;
		} else {
			return true;
		}			
	} else {
		return true;
	}
}

function sameWarrentyContracts(webProduct, warrantyPdcts) {
	var isSameContractFlag = false;
	if (webProduct != null && webProduct.contractDisplayInfo != null && warrantyPdcts != null) {
		 $.each(warrantyPdcts, function(j, contract){
			 if (contract != null && contract.contractDisplayInfo != null && contract.contractDisplayInfo.contractId != null) {
				if (webProduct.contractDisplayInfo.contractId == contract.contractDisplayInfo.contractId) {
					 isSameContractFlag = true;
				 } else {
					 isSameContractFlag = false;
				 }
			 } else {
				 isSameContractFlag= true;
			 }			 
		 });	
	} else {		
		isSameContractFlag= true;
	}
	return isSameContractFlag;
}
