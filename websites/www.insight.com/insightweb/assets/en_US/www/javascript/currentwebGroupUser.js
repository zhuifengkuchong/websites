 var webGroupCustomerIdData=null;
 var navAccountsData=null;
 var cartItemsLength=0;
 var currentWebGroupUserLabels;
 var messageUrl="/insightweb/getCmsContent";
 
 function webGroupCurrentSendData(webGroupData,accountsData)
 {
    //cartItemsLength=Insight.cartItemsLength;
	//cartItemsLength=3;
	webGroupCustomerIdData = webGroupData;
	navAccountsData=accountsData;
	displayCurrentWebGroupDropdown();
	displayCurrentAccountDropDown();
	
 }
 
 function displayCurrentWebGroupDropdown()
 {
	 $('#currentWebGroupDropdownDiv').css('display','block');
	 if(document.getElementById("currentWebGroupAnchor") != null)
		{
			jkmegamenu.definemenu("currentWebGroupAnchor", "currentWebGroupAnchorMenu", "mouseover");	
			loadCurrentWebGroupDropdown();
		}	 	
 }
 
 
function  displayCurrentAccountDropDown()
{
	
	$('#currentAccountDropDownDiv').css('display','block');
	if(document.getElementById("currentAccountAnchor") != null)
	{
		jkmegamenu.definemenu("currentAccountAnchor", "currentAccountAnchorMenu", "mouseover");
		loadCurrentAccountName();
	}
		

}


function loadCurrentWebGroupDropdown()
{
	 if($('#currentWebGroupName').length==0)
	  {
		 var currWebGrp = InsightCommon.getMessageContent(messageUrl, Insight.locale, "/messages/accountMgmt/webGroupMgmt/currentWebGroupuser.txt/jcr:content/jcr:data.txt");
		 //var currWebGrp=InsightCommon.getContent(InsightSearch.staticContentUrl,"","http://www.insight.com/insightweb/assets/en_US/www/javascript/accountMgmt/webGroupMgmt/currentWebGroupuser.txt");
		 currentWebGroupUserLabels = eval('(' + currWebGrp + ')');
	    var webGroupAccountDropDown = InsightCommon.getContent(InsightSearch.staticContentUrl, '',"http://www.insight.com/insightweb/assets/en_US/www/javascript/accountMgmt/webGroupMgmt/currentAccountDropDown.html");
	    $.template("webGroupAccountDropDown", webGroupAccountDropDown);
	    $("#currentWebGroupMenuDiv").empty();
	    var labelObj = $.extend({},webGroupCustomerIdData,currentWebGroupUserLabels);
	    InsightCommon.renderTemplate("webGroupAccountDropDown",labelObj, "#currentWebGroupMenuDiv"); 
	  }  
}

/*Function to display current webgroup details in a popup template*/
function loadCurrentWebGroupPopupTemplate()
{
	 if($('#currentWebGroupName').length==0)
	  {
		 var currWebGrp = InsightCommon.getMessageContent(messageUrl, Insight.locale, "/messages/accountMgmt/webGroupMgmt/currentWebGroupuser.txt/jcr:content/jcr:data.txt");
		 //var currWebGrp=InsightCommon.getContent(InsightSearch.staticContentUrl,"","http://www.insight.com/insightweb/assets/en_US/www/javascript/accountMgmt/webGroupMgmt/currentWebGroupuser.txt");
		 currentWebGroupUserLabels = eval('(' + currWebGrp + ')');
	    var webGroupAccountDropDown = InsightCommon.getContent(InsightSearch.staticContentUrl, '',"http://www.insight.com/insightweb/assets/en_US/www/javascript/accountMgmt/webGroupMgmt/currentAccountDropDown.html");
	    $.template("webGroupAccountDropDown", webGroupAccountDropDown);
	    $("#currentWebGroupMenuDiv").empty();
	    var labelObj = $.extend({},webGroupCustomerIdData,currentWebGroupUserLabels);
	    InsightCommon.renderTemplate("webGroupAccountDropDown",labelObj, "#currentWebGroupMenuDiv"); 
	    $("#currentWebGroupMenuDiv").dialog();
	    $('div.ui-dialog').css({'width':'350px'});
	  }  
}
function loadCurrentAccountName()
{
	if($('#currentAccountName').length==0)
	  {
		var webGroupAccountCurrentUserName = InsightCommon.getContent(InsightSearch.staticContentUrl, '',"http://www.insight.com/insightweb/assets/en_US/www/javascript/accountMgmt/webGroupMgmt/currentUserNameDropDown.html");
	    $.template("webGroupAccountCurrentUserName", webGroupAccountCurrentUserName);
	    $("#currentAccountMenuDiv").empty();
	   // var currWebGrp=InsightCommon.getContent(InsightSearch.staticContentUrl,"","http://www.insight.com/insightweb/assets/en_US/www/javascript/accountMgmt/webGroupMgmt/currentWebGroupuser.txt");
	    var currWebGrp = InsightCommon.getMessageContent(messageUrl, Insight.locale, "/messages/accountMgmt/webGroupMgmt/currentWebGroupuser.txt/jcr:content/jcr:data.txt");
	    currentWebGroupUserLabels = eval('(' + currWebGrp + ')');
	    var labelsObj = $.extend({},navAccountsData,currentWebGroupUserLabels);
	    InsightCommon.renderTemplate("webGroupAccountCurrentUserName",labelsObj, "#currentAccountMenuDiv"); 
	  }
}
function currentWebGroupDropdown(){
	
    $('#webCurrentLoginDropdown').css('display','block');
    $('#currentUserNameDropDown').css('display','none');
}

function currentWebGroup(){
	$('#webCurrentLoginDropdown').css('display','none');
}

function ShowCurrentUserName(){
	
    $('#currentUserNameDropDown').css('display','block');
    $('#webCurrentLoginDropdown').css('display','none');
}

function currentUserNameWebGroup(){
	 $('#currentUserNameDropDown').css('display','none');
}
function changeWebGroup(webGroupId)
{
	var webGroupAccountCurrentUserName = InsightCommon.getContent(InsightSearch.staticContentUrl, '',"http://www.insight.com/insightweb/assets/en_US/www/javascript/accountMgmt/webGroupMgmt/currentUserNameDropDown.html");
    $.template("webGroupAccountCurrentUserName", webGroupAccountCurrentUserName);
    InsightNavigation.loadCartDialog();
	 cartItemsLength=Insight.cartItemsLength;
	 if(cartItemsLength==0)
		 {
	      var data=InsightCommon.getServiceResponse("/insightweb/endUser/changeWebGroup/"+webGroupId+"/"+getCurrentTimestamp(),"","GET");
	      //alert(data);
          if(data!=null)
    	   {
    	    var redirectURL=InsightCommon.getRootURL()+"/insightweb/"+data.redirectUrl;
    	    if(data.changeDomain){    	    	
    	    	window.location.replace(data.redirectUrl);
    	    	
    	    }else{    	    	
    	    	window.location.replace(redirectURL);
    	    	var currentUrl = window.location.href;
    	    	if(currentUrl==redirectURL){
    	    		window.location.reload(true);
    	    		}
    	    	}
    	   }
		 }
	 else
		 {
		  var diagTxt=currentWebGroupUserLabels.labels.WarningIteminyourcartwilremoved;
			
			$( '<div></div>')
			.html(diagTxt)
		    .dialog({
		          resizable: false,
		          modal: true,
		          title : "",
		          width:"661px",
		          hieght:"150px",
		          buttons: {
		                "Cancel": function() {
		                      $( this ).dialog( "close" );
		                },
		                "OK ": function() {
		                      $( this ).dialog( "close" );
		                      var data=InsightCommon.getServiceResponse("/insightweb/endUser/changeWebGroup/"+webGroupId+"/"+getCurrentTimestamp(),"","GET");
		            	      //alert(data);
		                      if(data!=null)
		                	   {
		                	    var redirectURL=InsightCommon.getRootURL()+"/insightweb/"+data.redirectUrl;
		                	    if(data.changeDomain){
		                	    	window.location.replace(data.redirectUrl);
		                	    }else{
		            	        window.location.replace(redirectURL);
		                	    }
		                	   }
		                }
		          }
		    });
			$(".ui-dialog-titlebar").hide(); 
		 }
}
function changeSoldTo(soldtoNumber)
{
	var data=InsightCommon.getServiceResponse("/insightweb/endUser/changeSoldTo/"+soldtoNumber+"/"+getCurrentTimestamp(),"","GET");
	if(data!=null)
	{
	 
	  var redirectURL=InsightCommon.getRootURL()+"/insightweb/"+data.redirectUrl;
      window.location.replace(redirectURL);
	}
}

function loadCartItemsPopup(webGroupId)
{
	

}

function getCurrentTimestamp() 
{
	tstmp = new Date();
	return tstmp.getTime();
}

 function goToCurrentAccountTab(){
	 var currentAccountURL = InsightCommon.getRootURL()
		+ "/insightweb/accountTools#currentAccount";
	 window.location.replace(currentAccountURL);
	 
	 
 }