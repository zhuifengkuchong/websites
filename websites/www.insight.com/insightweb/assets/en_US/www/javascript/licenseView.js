/*
 * @yvadugu
 */
(function() { var InsightLicenseView = window.InsightLicenseView =
{
		globalLicenseViewData:null,
		licenseLabelsObj:"",
		loadLicenseViewPage : function ()
	    {   
			$('#bodyContent').empty();
			 //var srchLabels = InsightCommon.getMessageContent(InsightSearch.messageUrl, Insight.locale, "/messages/search/searchLabels.txt/jcr:content/jcr:data.txt");
		     //searchLabels = eval('(' + srchLabels + ')');
		       
		     var licenseLabels = InsightCommon.getMessageContent(InsightSearch.messageUrl, Insight.locale, "/messages/accountMgmt/webGroupMgmt/licenseViewLabels.txt/jcr:content/jcr:data.txt");
		     licenseLabelsObj = eval('(' + licenseLabels + ')');
		     		     
		     
		     
		       var shopCategoryURL = "/insightweb/getLicensePageDetails/"+tstmp.getTime();      
		       var data = InsightCommon.getServiceResponse(shopCategoryURL,'',"GET");   
		       globalLicenseViewData = data;
		       var template1 = InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/LicenseViewAdminTemplates/licenseViewTemplate.html");
		       $.template( "licenseViewTemplate", template1);
		       
		       data = $.extend(data.response.licenseMaster,licenseLabelsObj);
		       InsightCommon.renderTemplate( "licenseViewTemplate", data, "#bodyContent");	
		       
	    },
	    contentChangeByLienceView : function(licenseName)
	    {
	    	$('#licenseViewContent').empty();
	    	$('#licenseViewTitle').empty();
	             $.each(globalLicenseViewData.response.licenseMaster.licenseViews, function(index, value) {
	            	 if((value.name == licenseName)){
	            	 if( value.customCotent!=null && value.customCotent!="")
	            	 {	            		 
	            		
	            		 $('#licenseViewContent').html(value.customCotent);
	            	 }

	            		 $('#licenseViewTitle').html('<h3><strong>'+value.name+'</strong></h3>');
	            		 $("#licenseViewContentStatic").html("");
	            		 var template1 = InsightCommon.getContent(InsightSearch.staticContentUrl, "", value.licenseTemplate.templateContent);
	            		 $.template( "templateTmpl", template1);
	            		 var tempData = $.extend(licenseLabelsObj,globalLicenseViewData);
	            		 InsightCommon.renderTemplate( "templateTmpl", tempData, "#licenseViewContentStatic");	            			      
	            	 }
	       });	
	    }
}
})();
