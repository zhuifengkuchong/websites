/** Functions for global navigation elements **/
(function() { var InsightNavigation = window.InsightNavigation =
{
	/* This is the URL we have to use to get jQuery templates */		
	contentURL : "/insightweb/getStaticContent",	
	/* This is the URL we have to use to get content from CMS */
	messageUrl : "/insightweb/getCmsContent",
	isIPSUserSearch : false,
	ipsMenuLabelsObj:"",
	cookiesLabelsObj:"",
	isEadminUser:false,
	isNonLoggedinContractId:null,
	ipsContractDetails:null,
	showIPPWarranties:true,
	enableCrossSell:true,
	searchPageTitle : "default_value",
	globalLabels : null,
	accountToolsData : null,
    init : function ()
    {
        var testPermission = null; 		
    	if(isLoggedIn()){	
    		InsightNavigation.loadUserPermissions();
    		testPermission = Insight.userPermissions;  
    		if(InsightNavigation.isEadminUser){
    			InsightNavigation.loadUserInternalPermissions();
    		}
    		InsightNavigation.showIPPWarranties = ($.inArray("enable_insight_protection_plan", Insight.userPermissions)>-1);
    		InsightNavigation.enableCrossSell = ($.inArray("enable_crosssell", Insight.userPermissions)>-1);
    	}else{
    		 Insight.userPermissions=null;
    		 InsightNavigation.showIPPWarranties = true;
    		 InsightNavigation.enableCrossSell = true;
    	}
        
        //Shops permission
    	if($.inArray('shop_by_brand',testPermission)!= -1 || testPermission==null){
    	      $("#megaanchor13").mouseover(function() {InsightNavigation.getShopByBrandPage();}); 
    		  $('#ssb-top-link').show();
        }	
    	else {    		
    		$('#ssb-top-link').hide();
    	}
		
    	if(testPermission!=null && $.inArray('search',testPermission)== -1 ){
			$('#searchDiv').hide();
		}
		if(document.getElementById('solutionsMenuId')!= null) {			
				InsightNavigation.getSolutionsMenu();
    	}
		if(document.getElementById('servicesMenuId')!= null) {
				InsightNavigation.getServicesMenu();
		}
		if(document.getElementById('whyInsightId')!= null) {
			InsightNavigation.getWhyInsightMenu();
		}
		if(document.getElementById('resources')!= null) {
			InsightNavigation.getResourcesMenu();
		}
	
		if(document.getElementById('megaanchor-locales')!=null) {
			  if(!Insight.isLoggedin || ($.inArray("country_select", Insight.userPermissions)> -1)|| InsightGLMPages.loggedInFlag)
			 jkmegamenu.definemenu("megaanchor-locales", "megamenu-locales", "mouseover");
		 }
		
		//Deals permission
		// CR-184 When an IPS users logs in and chooses a specific contract, they shouldn?t have access to the Top Tech Buys page
		 if( ($.inArray('inventory_blowout',testPermission)!= -1 || $.inArray('weekly_deals',testPermission)!= -1 || testPermission==null)
				 && (Insight.selectedContractId == null  || Insight.selectedContractId == 'undefined' || Insight.selectedContractId.length == 0) )
		 {
			 InsightNavigation.getDealsMenu();
			 $('#ttb-top-link').show();
    	 }
		 else{
			 $('#ttb-top-link').hide();
		 }
			 
		 
			 
		//Products Menu
		if(document.getElementById('productsMenuId')!=null) {
			jkmegamenu.definemenu("megaanchor13", "megamenu13", "mouseover");
		}
		
		//Account Tools
		if(document.getElementById("megaanchor5") != null)
		{
			jkmegamenu.definemenu("megaanchor5", "megamenu5", "mouseover");			
		} 
		
		//Header Cart
		if(document.getElementById("megaanchor7") != null)
		{
			jkmegamenu.definemenu("megaanchor7", "megamenu7", "mouseover");	
		}
		
		//Header login
		if(document.getElementById("megaanchor6") != null)
		{			
			jkmegamenu.definemenu("megaanchor6", "megamenu6", "click");						
		}
			
		$("#products-tabs").tabs();	
		InsightNavigation.loadLocaleMenu();		
		InsightNavigation.loadLoginDialog();	
		InsightNavigation.loadCartDialog();
		InsightNavigation.getProductsData(); 
		/* not required if you are not logged in*/
		if(isLoggedIn()){
			InsightNavigation.loadUserInformation();
			InsightNavigation.loadAccountTools();			
			InsightNavigation.loadRole();        
			InsightNavigation.hideCloudMenuUnderSolution();
		}
	
		if(testPermission!=null && $.inArray('inventory_blowout',testPermission)== -1  && $.inArray('weekly_deals',testPermission)== -1 && $.inArray('shop_by_brand',testPermission)== -1   ){
			   $('#productMenuTabs').hide();
    	     } 
    	     
    	     
    	     
		//InsightNavigation.loadcurrentWebGroupId();
		
		$('#megaanchor7').mouseover(function() {
			InsightNavigation.loadCartDialog();
		});		
    },
    
	attachCloudLink: function(){
		var quickaddjsonObj = "";
		var cloudOrder = InsightCommon.getServiceResponse("/insightweb/cloud/getManageCloudLink", quickaddjsonObj,"POST");
		if(cloudOrder!=null && cloudOrder.url!=null)
		{
		   window.location.replace(cloudOrder.url);
		}
    },
    hideCloudMenuUnderSolution:function(){
    	if(($.inArray("user_requires_approval", Insight.userPermissions)> -1) || Insight.userInformation.UserType =="Shared" || InsightGLMPages.loggedInFlag){
    		$("#solutions-menu .cloud").css('display','none');
    	}
    	
    },
    getProductsData : function(searchData,advFlag)
    {        
    	var advmarkup = searchData;
    	var productMenuPermission = Insight.userPermissions;
    	
    	if(!InsightNavigation.isEadminUser && (!Insight.isLoggedin || (productMenuPermission!=null  && ($.inArray("product_menu", Insight.userPermissions)> -1)))){
    	var menuURL = "/insightweb/getProductMenu/All";
    	if(InsightNavigation.isNonLoggedinContractId!=null && InsightNavigation.isNonLoggedinContractId!=''){
    		menuURL = "/insightweb/getProductMenu/"+InsightNavigation.isNonLoggedinContractId;
    	}
	    	
	    	$.when(InsightCommon.getServiceResponseAsync(menuURL,null,"GET"),
	    			($.template["productMenuTemplate"] == undefined) ?  InsightCommon.getContentAsync(InsightNavigation.contentURL,'' , "http://www.insight.com/insightweb/assets/en_US/www/javascript/header/productMenuTemplate.html") : $.template["productMenuTemplate"])
	    	.then(function(menuData, templateData){
	    		InsightNavigation.loadProductsMenu("productsMenu", menuData[0].TopMenu, templateData[0]);
	    		//if search data exists, load advanced search page
            if(searchData!= null) {
            	getAdvanceSearchMenuData(searchData,menuData[0].TopMenu);
            }
    	});
    } else if(advFlag){
    	getAdvanceSearchMenuData(searchData,null);
    }
    },
    loadContractTab : function ()
    {   
    
					   
    	InsightNavigation.isIPSUserSearch=true;
    	
    	ipsCurrentContract = InsightCommon.getServiceResponse("/insightweb/endUser/currentContract/"+InsightCommon.getCurrentTimestamp(),null,"GET");    	
    	$("#webCurrentContractDiv").css("display","block");
    	var ipsMenuLabels =InsightCommon.getMessageContent(messageUrl, Insight.locale,"http://www.insight.com/messages/accountMgmt/ipsHome/ipsMenuLabels.txt");

    	ipsMenuLabelsObj = eval('(' + ipsMenuLabels + ')');
    	if(ipsCurrentContract!=null && ipsCurrentContract!=undefined &&
    			ipsCurrentContract.currentContract!=null && ipsCurrentContract.currentContract!=null&&
    			ipsCurrentContract.currentContract=="All")
    		{
    		$("#ipsMenuContractType").html(Insight.globalLabels.BrowseAll);
    		}
    	else if(ipsCurrentContract.currentContract=="openMarket"){
    		$("#ipsMenuContractType").html(Insight.globalLabels.OpenMarket);
    	}
    	else
    		{
    			if(ipsCurrentContract != undefined && ipsCurrentContract != null &&
    					ipsCurrentContract.currentContract != undefined && ipsCurrentContract.currentContract != null)
    			{
    				$("#ipsMenuContractType").html(ipsCurrentContract.currentContract);
    			}
    		}
    	
    	//ips Contract menu//
		if(document.getElementById("ipsContractAnchor") != null)
		{
			jkmegamenu.definemenu("ipsContractAnchor", "ipsContractAnchorMenu", "mouseover");			
		}
		
		$.template( "ipsContractDropMenuTemplate", InsightCommon.getContent(InsightNavigation.contentURL,"","http://www.insight.com/insightweb/assets/en_US/www/javascript/accountMgmt/ipsHome/ipsContractDropMenuTemplate.html"));		
		var ipsMenuLabels =InsightCommon.getMessageContent(messageUrl, Insight.locale,"http://www.insight.com/messages/accountMgmt/ipsHome/ipsMenuLabels.txt");					
    	ipsMenuLabelsObj = eval('(' + ipsMenuLabels + ')');
    	
		$('#ipsContractAnchor').mouseover(function() {
			InsightNavigation.getIpsContractMenu();
		});	
   
    }, 
    loadCookiePopUp: function ()
    {
    	$("#setEmailCookie").html("");
    	$("#setEmailCookie").dialog({
    		title: "Welcome to Insight Beta!",
    		resizable: false,
    		overlay: { background: "white" },
    		width: '800px',
    		height:'300',
    		resizable: false   
    	});
    	var setCookie=InsightCommon.getContent(InsightNavigation.contentURL,"","http://www.insight.com/insightweb/assets/en_US/www/javascript/login/setEmailAddressInCookie.html");
    	$.template("setEmailCookie",setCookie);
    	var cookiespopuplabel = InsightCommon.getMessageContent(messageUrl,Insight.locale,"http://www.insight.com/messages/header/setEmailAddressInCookieLabels.txt");
    	cookiesLabelsObj = eval('(' + cookiespopuplabel +')');
    	InsightCommon.renderTemplate("setEmailCookie",cookiesLabelsObj,"#setEmailCookie");
    },
    setEmailInCookie:function ()
    {
    	var emailId =$("#emailId").val();
    	var cookieObj = '{"emailId":"'+emailId+'"}';
    	InsightCommon.getServiceResponse("/insightweb/setEmailInCookie",cookieObj,"POST",false);
    	$("#setEmailCookie").dialog("close");
    },
    loadIPSContractDetail : function (contractId,contractType)
    {   
    	var ipsContractDetailData = "";
    	if(contractType=="All")
		{  
    		ipsContractDetailData = InsightCommon.getServiceResponse("/insightweb/endUser/getContractByContractId?contractType="+contractType,null,"GET",false);    		
    		$("#ipsMenuCurrentContractName").html(Insight.globalLabels.BrowseAll);
    		window.location.replace("http://www.insight.com/insightweb/welcome"); 
		}
    	else if(contractType=="openMarket")
		{
    		ipsContractDetailData = InsightCommon.getServiceResponse("/insightweb/endUser/getContractByContractId?contractType="+contractType,null,"GET",false);    		
    		$("#ipsMenuCurrentContractName").html(Insight.globalLabels.OpenMarket);
    		window.location.replace("http://www.insight.com/insightweb/welcome");
		}
    	else
		{ 
    		
    		ipsContractDetailData = InsightCommon.getServiceResponse("/insightweb/endUser/getContractByContractId?contractId="+parseInt(contractId)+"&contractType="+contractType,null,"GET",false); 
    		window.location.replace("http://www.insight.com/insightweb/contractDetail");
    		  		
		}
    	if(ipsContractDetailData)
		{ 
    	
			if(contractType=="openMarket"){
				$("#ipsMenuCurrentContractName").html(Insight.globalLabels.OpenMarket);
				$("#ipsMenuContractType").html(Insight.globalLabels.OpenMarket);
			}
			if(contractType=="All"){
				$("#ipsMenuCurrentContractName").html(Insight.globalLabels.BrowseAll);
				$("#ipsMenuContractType").html(Insight.globalLabels.BrowseAll);
			}
		}
		
	},
    getIpsContractMenu : function ()
    {       	
    	if ($("#ipsContractDropMenuContainer").length == 0) 
    	{
    		//$.template( "ipsContractDropMenuTemplate", InsightCommon.getContent(InsightNavigation.contentURL,"","http://www.insight.com/insightweb/assets/en_US/www/javascript/accountMgmt/ipsHome/ipsContractDropMenuTemplate.html"));
    		var ipsContractDropData = InsightCommon.getServiceResponse("/insightweb/endUser/getContracts/"+InsightCommon.getCurrentTimestamp(),null,"GET");
    		var ipsMenuLabels =InsightCommon.getMessageContent(messageUrl, Insight.locale,"http://www.insight.com/messages/accountMgmt/ipsHome/ipsMenuLabels.txt");    				
        	ipsMenuLabelsObj = eval('(' + ipsMenuLabels + ')');
    		ipsContractDropData = $.extend({"ipsContract":ipsContractDropData},ipsMenuLabelsObj);
    		$("#ipsContractMenuDiv").empty();        	      	
            InsightCommon.renderTemplate("ipsContractDropMenuTemplate",ipsContractDropData,"#ipsContractMenuDiv");
    	}    	
    },
    getIPSContractInfo : function (seg,reg)
    {   
    	window.location.href = ($.address.baseURL().indexOf('localhost') >= 0 ? '/insightweb/' : '/')+'#contracts?segment='+seg+'&region='+reg;
    },    
    
    getIPSContractsDropdown : function(seg,reg)
    {
    	$("#bodyContent").empty();    	
    	var ipsMenuLabels =InsightCommon.getMessageContent(messageUrl, Insight.locale,"http://www.insight.com/messages/accountMgmt/ipsHome/ipsMenuLabels.txt");
		
    	InsightNavigation.ipsMenuLabelsObj = eval('(' + ipsMenuLabels + ')');
    	var ipsContractData = InsightCommon.getServiceResponse("/insightweb/endUser/getContractsBySegmentRegion/"+seg+"/"+reg,null,"GET");
    	$.template( "ipsContractTemplate", InsightCommon.getContent(InsightNavigation.contentURL,"","http://www.insight.com/insightweb/assets/en_US/www/javascript/accountMgmt/ipsHome/ipsContractTemplate.html"));
    	ipsContractData = $.extend(ipsContractData,InsightNavigation.ipsMenuLabelsObj,{"isLoggedIn":Insight.isLoggedin});
        InsightCommon.renderTemplate("ipsContractTemplate",ipsContractData,"#bodyContent");
        $.template( "ipsContractDetailTemplate", InsightCommon.getContent(InsightNavigation.contentURL,"","http://www.insight.com/insightweb/assets/en_US/www/javascript/accountMgmt/ipsHome/ipsContractDetailTemplate.html"));

        if(ipsContractData.noOfContracts==1)
        	{
        	  $("#ipsContractDetailDiv").empty();        	      	
        	  /* APAC date format for contract start/end dates */
        		if(ipsContractData.contractDetail!=null ){
        		var startDate = ipsContractData.contractDetail.startDate;
        		if(startDate!=null && startDate!="" && startDate!='undefined' ){
        			var tmp_dt = $.datepicker.parseDate('mm/dd/yy', startDate);
        			var apacDate = $.datepicker.formatDate($.datepicker.content.dateFormat, tmp_dt);
        			ipsContractData.contractDetail.startDate = apacDate;
        		}
        		
        		var endDate = ipsContractData.contractDetail.currentEndDate;
        		if(endDate!=null && endDate!="" && endDate!='undefined' ){
        			tmp_dt = $.datepicker.parseDate('mm/dd/yy', endDate);
        			apacDate = $.datepicker.formatDate($.datepicker.content.dateFormat, tmp_dt);
        			ipsContractData.contractDetail.currentEndDate = apacDate;
        		}
        	  }
              InsightCommon.renderTemplate("ipsContractDetailTemplate",ipsContractData,"#ipsContractDetailDiv");
        	}
        
        	$("#ipsContractSelectID").change(function() { 
         		InsightNavigation.isNonLoggedinContractId = parseInt($('#ipsContractSelectID').val());
        		InsightNavigation.getProductsData();
        		InsightNavigation.ipsContractDetails = InsightCommon.getServiceResponse("/insightweb/endUser/getContractByContractId?contractId="+parseInt($('#ipsContractSelectID').val())+"&contractType=contractType",null,"GET",false);
        		InsightNavigation.ipsContractDetails = $.extend({"contractDetail":InsightNavigation.ipsContractDetails},InsightNavigation.ipsMenuLabelsObj);
               	window.location.href = (($.address.baseURL().indexOf('localhost') >= 0 ? '/insightweb/' : '/')+'#contract?id='+$('#ipsContractSelectID').val()+'&title='+InsightNavigation.ipsContractDetails.contractDetail.contract.longDescription+'&extNum='+InsightNavigation.ipsContractDetails.contractDetail.contract.extContractNumber);
            });
    	
    },
    
    
    displayContractDetail : function(contractId)
    {
		if (InsightNavigation.ipsContractDetails == null) {
			InsightNavigation.getIPSContractsDropdown('S','TX');
			$("#ipsContractSelectDiv").empty(); 
     		InsightNavigation.isNonLoggedinContractId = parseInt($('#ipsContractSelectID').val());
    		InsightNavigation.getProductsData();
    		InsightNavigation.ipsContractDetails = InsightCommon.getServiceResponse("/insightweb/endUser/getContractByContractId?contractId="+contractId+"&contractType=contractType",null,"GET",false);
    		InsightNavigation.ipsContractDetails = $.extend({"contractDetail":InsightNavigation.ipsContractDetails},InsightNavigation.ipsMenuLabelsObj);			
		} else { $("#ipsContractSelectDiv").empty(); } 
		/* APAC date format for contract start/end dates */
		if(InsightNavigation.ipsContractDetails.contractDetail!=null ){
		var startDate = InsightNavigation.ipsContractDetails.contractDetail.startDate;
		if(startDate!=null && startDate!="" && startDate!='undefined' ){
			var tmp_dt = $.datepicker.parseDate('mm/dd/yy', startDate);
			var apacDate = $.datepicker.formatDate($.datepicker.content.dateFormat, tmp_dt);
			InsightNavigation.ipsContractDetails.contractDetail.startDate = apacDate;
		}

		var endDate = InsightNavigation.ipsContractDetails.contractDetail.currentEndDate;
		if(endDate!=null && endDate!="" && endDate!='undefined' ){
			tmp_dt = $.datepicker.parseDate('mm/dd/yy', endDate);
			apacDate = $.datepicker.formatDate($.datepicker.content.dateFormat, tmp_dt);
			InsightNavigation.ipsContractDetails.contractDetail.currentEndDate = apacDate;
		}
	  }
		InsightCommon.renderTemplate("ipsContractDetailTemplate",InsightNavigation.ipsContractDetails,"#ipsContractSelectDiv");
		InsightNavigation.ipsContractDetails = null;
    },
    
    loadProductsMenu : function (container,menuData, templateData)
    { 			
        resourceCenterPermission = false;
        
        if(Insight.userPermissions!=null){    
        	var testPermission = Insight.userPermissions;        	
        	 if($.inArray('enable_resource_centers',testPermission)==-1)
        		 resourceCenterPermission = true;	
        }
        menuData = $.extend(menuData,{"resourceCenterPermission":resourceCenterPermission});
	        
        $('#'+container).html("<br>"+$("#loading").html());
	        
        // cache product menu template
        $.template( "productMenuTemplate", templateData);
        	$('#'+container).html("");
     	InsightCommon.renderTemplate("productMenuTemplate", menuData, "#" + container);
     	
        var hlabel = InsightCommon.getMessageContent (InsightNavigation.messageUrl ,Insight.locale,"http://www.insight.com/messages/header/headerJSP.txt");
        var hlabelObj = eval('(' + hlabel + ')');
        $("#brandsLink").text(hlabelObj.labels.brandsTab);
     	$("#dealsLink").text(hlabelObj.labels.dealsTab);
     	// brands & deals permission
     	if(testPermission!=null && $.inArray('inventory_blowout',testPermission)== -1  && $.inArray('weekly_deals',testPermission)== -1){
			$('.deals').hide();
        }
     	if (testPermission!=null && $.inArray('shop_by_brand',testPermission)== -1 ) {
     		$('.brands').hide();
     	}
        
     	//lazy load brands & deals 
     	$("#brandsLink").mouseover(function(){
     		InsightNavigation.getBrandsMenu();
        });
     	$("#dealsLink").mouseover(function(){
     		InsightNavigation.getDealsMenu();
     	});
        
     	InsightNavigation.loadMarketingContent(menuData);
         
    },    

    /* CMS content in Products Menu */
    loadMarketingContent : function(menuData){
    	$.each(menuData, function(colIndex,colValue){
    		$.each(colValue.children, function(rowIndex,rowValue){
    			var category = rowValue.productSetAlias;
    			if(rowValue.label=="Warranties"){
    				category = rowValue.alias;
    			}
    			var cmsUrl = "/menus/products/"+category+".html";
    			InsightCommon.getMessageContentAsync(InsightNavigation.messageUrl, Insight.locale, cmsUrl, function(cmsData) { 
    				var marketingStyle = $(cmsData).find('.prod_content').attr('type');
    				var label = rowValue.label;
    				$('#'+label).addClass(marketingStyle);
    			    $("#"+category).html(cmsData);	
    		    });
    		});
    	});
    },

    loadSearchBox : function ()
    {
    	// make suggestions visible
    	   	
    	if(Insight.showSuggests)  {
    		$(".search-input").each(function(){$(this).autocomplete({
				minLength:1,
				open: function(){
			        $(this).autocomplete('widget').css('z-index', 100);
			        return false;
			    },
	    		source: '/insightweb/suggest',
	    		select: function(event, ui) {
	    			if($(this).attr('id') != 'advancedSearch_searchText'){
	    				if($(this).attr('id')=='searchText-footer'){
	    					$("#searchText-footer").val(ui.item.label);
	    					$(this).autocomplete("close");
	    					$("#searchBtn-footer").click();
	    				}
	    				else{
	    					$("#searchText").val(ui.item.label);
	    					$(this).autocomplete("close");
	    					$("#searchBtn").click();
	    				}
	    				return false;
	    			}
	    		}
	    	}).data("autocomplete")._renderItem = function (ul, item) {
				  var term = this.term.split(' ').join('|');
	    		  var t = item.label.replace(term,"<b>"+term+"</b>");
	    		  return $( "<li></li>" )
	    		     .data( "item.autocomplete", item )
	    		     .append( "<a style='overflow-x:hidden; white-space: nowrap;'>" + t + "</a>" )
	    		     .appendTo( ul );
    		};}); 
    	}					 
    },
    parseData : function (data){
    	
    	var divData = [];
    	$.each(data.suggested.phrase, function(i, data) {
    		divData.push(data.value);
    	});
    	return divData;
    },
    showSuggest : function(data,id){
    	
    	$( "#"+id ).autocomplete({
    		source: InsightNavigation.parseData(data),
    		select: function(event, ui) { if(id!='advancedSearch_searchText'){ $("#"+id).val(ui.item.label);$('#searchBtn').click();} }
    	}).data("autocomplete")._renderItem = function (ul, item) {
    		  var term = this.term.split(' ').join('|');
    		  var t = item.label.replace(term,"<b>"+term+"</b>");
    		  return $( "<li></li>" )
    		     .data( "item.autocomplete", item )
    		     .append( "<a style='overflow-x:hidden; white-space: nowrap;'>" + t + "</a>" )
    		     .appendTo( ul );
    	};
    },
	    	
    /**
     * Loads the brands tab on the products menu
     */
    getShopByBrandPage : function() {
    	//lazy load brands menu
    	if($('#fragment-2').html() == "")
    	{    	
	    	$('#fragment-2').html("<br>"+$("#loading").html());
	    	
	    	InsightCommon.getMessageContentAsync(InsightNavigation.messageUrl ,Insight.locale,"http://www.insight.com/messages/accountMgmt/ipsHome/ipsMenuLabels.txt", function(data) {		
		    	InsightNavigation.renderShopByBrandPage();    		      
	    	});
    	}
    },
    
    /* Solution Menu */
    getSolutionsMenu : function(){
		  
    	jkmegamenu.definemenu("megaanchor1", "solutions-menu", "mouseover");
    	var menuUrl = "http://www.insight.com/menus/solutions.html";
    	//get IPS menu
    	if(Insight.ipsFlag != null && Insight.ipsFlag == true) {
    		menuUrl = "http://www.insight.com/ips/menus/solutions.html";
    	}
    	InsightCommon.getMessageContentAsync(InsightNavigation.messageUrl, Insight.locale, menuUrl, function(data) {    		
    		var solutionsHref = $(data).find(".megaanchor-arrow").attr('href');
    		if(solutionsHref!=undefined && solutionsHref!=""){
    			$("#megaanchor1").attr("href",solutionsHref);
    		}
    		
    		if($(data).find(".megamenu").html() == null) {
	    	$('#solutions-menu').html(data);
    		}
    		else {
	    		$('#solutions-menu').html($(data).find(".megamenu").html());
    			InsightNavigation.initSlideOutMenu($('#solutions-menu'));
    		}
    	});
    },
    
    /* Services Menu */
    getServicesMenu : function(){
    	jkmegamenu.definemenu("megaanchor2", "services-menu", "mouseover");
    	var menuUrl = "http://www.insight.com/menus/services.html";
    	//get IPS menu
    	if(Insight.ipsFlag != null && Insight.ipsFlag == true) {
    		menuUrl = "http://www.insight.com/ips/menus/services.html";
    	}
    	InsightCommon.getMessageContentAsync(InsightNavigation.messageUrl, Insight.locale, menuUrl, function(data) {
    		
    		var servicesHref = $(data).find(".megaanchor-arrow").attr('href');
    		if(servicesHref!=undefined && servicesHref!=""){
    			$("#megaanchor2").attr("href",servicesHref);
    		}
    		
    		if($(data).find(".megamenu").html() == null) {
	    	$('#services-menu').html(data);	    	
    		}
    		else {
    			$('#services-menu').html($(data).find(".megamenu").html());	    
    			InsightNavigation.initSlideOutMenu($('#services-menu'));
    		}  	
    	});
    },
    
    
    /* Why Insight Menu */
    getWhyInsightMenu : function(){
    	jkmegamenu.definemenu("megaanchor4", "why-insight-menu", "mouseover");
    	InsightCommon.getMessageContentAsync(InsightNavigation.messageUrl, Insight.locale, "http://www.insight.com/menus/why-insight.html", function(data) {
    		
    			var whyInsightHref = $(data).find(".megaanchor-arrow").attr('href');
    		    if(whyInsightHref!=undefined && whyInsightHref!=""){
    		    	$("#megaanchor4").attr("href",whyInsightHref);
    		   	}
    		
    		if($(data).find(".megamenu").html() == null) {
	    	$('#why-insight-menu').html(data);	    	
    		} 
    		else {
		    	$('#why-insight-menu').html($(data).find(".megamenu").html());	
		    	InsightNavigation.initSlideOutMenu($('#why-insight-menu'));    	
		    }
    	});
    },
    
        
    /* Resources Menu */
    getResourcesMenu : function(){
    	jkmegamenu.definemenu("megaanchor8", "resources-menu", "mouseover");
    	InsightCommon.getMessageContentAsync(InsightNavigation.messageUrl, Insight.locale, "http://www.insight.com/menus/resources.html", function(data) {    		
    		var resourcesHref = $(data).find(".megaanchor-arrow").attr('href');
    		if(resourcesHref!=undefined && resourcesHref!=""){
    			$("#megaanchor8").attr("href",resourcesHref);    		
    		}
    		if($(data).find(".megamenu").html() == null) {
	    	$('#resources-menu').html(data);	    	
    		} 
    		else {
		    	$('#resources-menu').html($(data).find(".megamenu").html());	
		    	InsightNavigation.initSlideOutMenu($('#resources-menu'));    	
		    }
    	});
    },
    
    
    /* code for FlyoutMenu */
    initSlideOutMenu: function(container) {

      var h = container.height() - 20;
      var w = container.width();
      var hidden = false;
      container.find("ul li").hover(function() {
        hidden = false;
        container.find("ul li .primary").hide();
        $(this).css('background-color', '#FFFFFF');
        $('div:first', this).show();
        var url = $('div:first', this).attr('url');
        if (!(url == undefined || url == null || url == "")) {
          var url1 = url.substring(url.indexOf("/menus"), url.length);
          var id = $('div:first', this).attr('id');
          if ($('#' + id).is(':empty')) {
            InsightNavigation.getFlyoutMenu(id, url1);
          }
        }
      }, function() {
        hidden = true;
        $(this).css('background-color', '');

        var that = this;

        setTimeout(
            function() {
              if(hidden)
                $('div:first', that).hide();
            }, 2000);
      });

      container.find("ul.productsList div").css("min-height", h);
      container.find("ul li:has(div)").hover(function() {
        $(this).addClass("hover");
        var dropDownItem = $(this).find("a.dropDownItem");
        if ($(dropDownItem).attr("href") != "")
          $(dropDownItem).addClass("hoverText");
        else
          $(dropDownItem).addClass("hoverTextNone");

        $('div:first', this).hover(function() {
          $(this).siblings().css('color', '#CC0000');
          $(this).siblings().css('font-weight', 'bold');
        }, function() {
          $(this).siblings().css('color', '');
          $(this).siblings().css('font-weight', '');
        });

      }, function() {
        $(this).removeClass("hover");
        $(this).css('background-color', '');
        var dropDownItem = $(this).find("a.dropDownItem");
        if ($(dropDownItem).attr("href") != "")
          $(dropDownItem).removeClass("hoverText");
        else
          $(dropDownItem).removeClass("hoverTextNone");
      });

    },
    
    /* Flyout For Sub Menu Item */   
    getFlyoutMenu : function(temp,menuUrl){
    	$('#'+temp).html("<br>"+$("#loading").html());
    	InsightCommon.getMessageContentAsync(InsightNavigation.messageUrl, Insight.locale, menuUrl, function(data) {     		
    		$("#"+temp).html($(data).find(".htmlcomponent").html());
    	});
    },   
    
    /* Brands Menu */
    getBrandsMenu : function(){
    	
    	var testPermission = Insight.userPermissions;
    	if($.inArray('shop_by_brand',testPermission)!= -1 || testPermission==null){
    		if( $("#brandsContent").html() == "") {
    			$('#brandsContent').html("<br>"+$("#loading").html());
    			var brandsUrl = "http://www.insight.com/brands/menu1.html";
       	 		InsightCommon.getMessageContentAsync(InsightNavigation.messageUrl, Insight.locale, brandsUrl, function(cmsData) { 
       	 			$("#brandsContent").html(cmsData);	
       	 		});
        	}
    		else {
    			$("#brandsContent").show();
        	}
        	 }
    	else
    		$("#brandsContent").hide();
    },
    
    /* Deals Menu */
    getDealsMenu : function(){   	
		 if( $("#dealsContent").html() == "") {
			 $('#dealsContent').html("<br>"+$("#loading").html());
			 var dealsUrl = "http://www.insight.com/menus/deals.html";
			 InsightCommon.getMessageContentAsync(InsightNavigation.messageUrl, Insight.locale, dealsUrl, function(cmsData) { 
				 $("#dealsContent").html(cmsData);	
				 
				 var testPermission = Insight.userPermissions;
	    		 if (testPermission!=null && $.inArray('inventory_blowout',testPermission)== -1) {
						$('#deals-ibo').hide();
				}
				if (testPermission!=null && $.inArray('weekly_deals',testPermission)== -1) {					
					$('#deals-ttb').hide();
				}				 
    	});
        	}
		 else {
			 $("#dealsContent").show();
        	}
    },
    renderShopByBrandPage : function () {
    	InsightCommon.getMessageContentAsync(InsightNavigation.messageUrl, Insight.locale, "http://www.insight.com/brands/menu.html", function(data) {    	
    		$('#fragment-2').html(data);
    	});
        
    },	  
        
	  loadLoginDialog : function ()
	  {
		  //alert("in login");
		  var headerLoginLabels = InsightCommon.getMessageContent(InsightNavigation.messageUrl, Insight.locale, "http://www.insight.com/messages/login/headerLoginLabels.txt");			
		  headerLoginLabels = eval('(' + headerLoginLabels + ')');
		  var data = InsightCommon.getContent(InsightNavigation.contentURL, '', "http://www.insight.com/insightweb/assets/en_US/www/javascript/login/headerLogin.html");
		  $.template( "loginTemplate", data);
		  InsightCommon.renderTemplate( "loginTemplate",headerLoginLabels,"#loginFormHeader");
		  //bind mousedown on entire document
		  $(document).mousedown(function(e) {
				var clicked=$(e.target); // get the element clicked
				if(!clicked.is('#loginFormHeader') && !clicked.parents().is('#loginFormHeader')) {
					// outside click
					$('#header-cancel').click();
				}
		  });			
	  },
	  validateLoginSmallWindow:function (){

		  var headerLoginLabels = InsightCommon.getMessageContent(InsightNavigation.messageUrl, Insight.locale, "http://www.insight.com/messages/login/headerLoginLabels.txt");			
			headerLoginLabels = eval('(' + headerLoginLabels + ')');
			var errorStrin="";
			if($("#headerLoginForm").find("#j_username").val()=="" && $("#headerLoginForm").find("#j_password").val()=="" ){
				alert(headerLoginLabels.labels.usernameAndPassword);
				InsightCommon.hideLoading();
				return;
			}
			 if($("#headerLoginForm").find("#j_username").val()==""){
				alert(headerLoginLabels.labels.enterUserName);
				InsightCommon.hideLoading();
				 return;
			}
			
		     if($("#headerLoginForm").find("#j_password").val()==""){
				alert(headerLoginLabels.labels.enterPassword);
				InsightCommon.hideLoading();
				return;
			}
             var loc = document.domain;
               if(loc.indexOf("localhost")==-1){
               if(loc.indexOf("https:")==-1){
                   if(loc.indexOf("http:")!=-1){
                	  loc = loc.replace("http:", "https:");
                     }
                   else{
           	          loc="https://"+loc; 
                     }
                }
            	   loc = loc+"/insightweb/j_spring_security_check"; 
               }
               else{
            	   loc = "/insightweb/j_spring_security_check"; 
               }
              
             $('#headerLoginForm').attr('action',loc).submit();
			
		},
		loadCartDialog : function()
		{
		  //make a server call to get the cart information
		  InsightCommon.getServiceResponseAsync("/insightweb/transaction/getcart",null,"GET",function(data) {
			  var cartItemCount=0;
			  var newCartInfo=data;
			  if(!jQuery.isEmptyObject(data.contracts)){
				  $.each(data.contracts, function(key, val) {
				        $.each(val.cartItems, function(keyItem, valItem) {
				        	if(valItem.description.length>22)
				        		newCartInfo.contracts[key].cartItems[keyItem].description=
				        		valItem.description.replace(valItem.description.substr(22,valItem.description.length),"...");
				        	cartItemCount=cartItemCount+1;
				        });
				  });
			  }
			  Insight.cartItemsLength=cartItemCount;

					//Initialize Header Cart
					if(document.getElementById("megaanchor7") != null)
				    {
							jkmegamenu.definemenu("megaanchor7", "megamenu7", "mouseover");	
					}

				if(cartItemCount>0)
				{
				  Insight.cartTotalPrice=newCartInfo.totalCost;				 
				  Insight.cloudCart = newCartInfo.cloudCart;
				//  alert(Insight.cloudCart);
				 
				  newCartInfo.totalCost = InsightCommon.formatCurrency(newCartInfo.subTotal);
			
				  $("#mainHeaderCartTotalPrice").html(" - "+newCartInfo.currency +" "+newCartInfo.totalCost);
				  var cartTmpl =  InsightCommon.getContentAsync(InsightNavigation.contentURL, '', "http://www.insight.com/insightweb/assets/en_US/www/javascript/transaction/ShoppingCart/headerCart.html", function(data) {
				  
					  $.template( "cartTemplate", data);
					
					  //get labels
					  var templateLabelsStr = InsightCommon.getMessageContentAsync(InsightNavigation.messageUrl, Insight.locale, "http://www.insight.com/messages/transaction/ShoppingCart/shoppingcartlabels.txt", function(data) {
						  var templateLabels = eval('(' + data + ')');
						  
						  newCartInfo=$.extend(newCartInfo, templateLabels);
						  $("#viewCartHeader").html("");
						  
						InsightCommon.renderTemplate( "cartTemplate",newCartInfo,"#viewCartHeader");
						
					  });
				  });
			  }
				else
				{
				var cartTmpl =  InsightCommon.getContentAsync(InsightNavigation.contentURL, '', "http://www.insight.com/insightweb/assets/en_US/www/javascript/transaction/ShoppingCart/headerCartEmpty.html", function(data) {

					$.template( "cartTemplate", data);

					//get labels
					var templateLabelsStr = InsightCommon.getMessageContentAsync(InsightNavigation.messageUrl, Insight.locale, "http://www.insight.com/messages/transaction/ShoppingCart/shoppingcartlabels.txt", function(data) {
						var templateLabels = eval('(' + data + ')');

						newCartInfo=$.extend(newCartInfo, templateLabels);
					$("#viewCartHeader").html("");

					InsightCommon.renderTemplate( "cartTemplate",newCartInfo,"#viewCartHeader");
						});
					});
			  }
		  });
	  },
	  loadUserPermissions: function(){
		  
		   var response=InsightCommon.getServiceResponse("/insightweb/transaction/getPermissions",null,"POST");
		  var test ='';		 
		  if(response.Permissions!=""){
			  Insight.userPermissions= response.Permissions;			 
		  }
		  else
			  Insight.userPermissions=null;
		  
	  },
	  loadAccountTools: function()
	  {
			 
			var accountToolsLab = InsightCommon.getMessageContent(InsightNavigation.messageUrl, Insight.locale, "http://www.insight.com/messages/accountMgmt/webGroupMgmt/welcomePageLabels.txt");
			accountToolsLab = eval('(' + accountToolsLab + ')');
		    var softwareProgramAvail = false;	
		    
		  $("#welcomeContent").empty(); 
		  var serviceURL = "/insightweb/getAccountToolsMenuDetails"; 
		  InsightNavigation.accountToolsData = InsightCommon.getServiceResponse(serviceURL, null, 'GET',false);
		  InsightNavigation.accountToolsData = $.extend(InsightNavigation.accountToolsData,{"labels":accountToolsLab.labels});
		  var welcomeTempl = InsightCommon.getContent(InsightNavigation.contentURL, '', "http://www.insight.com/insightweb/assets/en_US/www/javascript/login/accountTools.html");
		  $.template("welcomeTemplate", welcomeTempl); 
		  InsightCommon.renderTemplate("welcomeTemplate",InsightNavigation.accountToolsData,"#welcomeContent");

	  },

	  loadRole: function()
	  {
		   //$("#webCurrentContractDiv").css("display","block");
			if(document.getElementById("roleAnchor") != null)
			{
				jkmegamenu.definemenu("roleAnchor", "roleAnchorMenu", "mouseover");			
			}			
			$.template( "roleDropDownTemplate", InsightCommon.getContent(InsightNavigation.contentURL,"","http://www.insight.com/insightweb/assets/en_US/www/javascript/accountMgmt/webGroupMgmt/roleDropDown.html"));
			$('#roleAnchor').mouseover(function() {
				InsightNavigation.loadRolesDropDown();
			});	
			
	  },
	  loadRolesDropDown : function ()
	    {       	
	    	if ($("#roleDropDownMainDiv").length == 0) 
	    	{
	    		//$.template( "roleDropDownTemplate", InsightCommon.getContent(InsightNavigation.contentURL,"","http://www.insight.com/insightweb/assets/en_US/www/javascript/accountMgmt/webGroupMgmt/roleDropDown.html"));
	    		$("#roleMenuDiv").empty();
	    		var ipsMenuLabels =InsightCommon.getMessageContent(messageUrl, Insight.locale,"http://www.insight.com/messages/accountMgmt/ipsHome/ipsMenuLabels.txt");
				
	        	ipsMenuLabelsObj = eval('(' + ipsMenuLabels + ')');
	            InsightCommon.renderTemplate("roleDropDownTemplate",ipsMenuLabelsObj,"#roleMenuDiv");
	    	}    	
	    },
	  loadcurrentWebGroupId: function(){
		  
		  
		   var tstmp = new Date();
		   var currentTimestamp= tstmp.getTime();	
		   var webGrpPermissions=[];
		   var webGroupData=InsightCommon.getServiceResponse("/insightweb/endUser/getwebGroupDetails/"+currentTimestamp,"","GET");
		   var accountsData=InsightCommon.getServiceResponse("/insightweb/endUser/getCurrentAccountDropDownDetails/"+currentTimestamp,"","GET");
		   if(webGroupData.webGroupPermissions!=null)
			  {
			   $.each(webGroupData.webGroupPermissions , function(index,value){
				   webGrpPermissions[index]=value.name;
			   });
			   Insight.webGroupPermissions=webGrpPermissions;
			  }
		   else
			   {
			    Insight.webGroupPermissions=[];
			   }
		  
		   var wgAccountLabels = InsightCommon.getMessageContent (InsightNavigation.messageUrl ,Insight.locale,"http://www.insight.com/messages/accountMgmt/ipsHome/ipsMenuLabels.txt");
	
	       var WGAccountAccountDetails = eval('(' + wgAccountLabels + ')');
		   Insight.soldTosCount=accountsData.soldtoList.soldToList.length;
		   var data=null;
		   data=$.extend(data,{"webGroupData":webGroupData});
		   data=$.extend(data,{"soldTosData":accountsData});
		   data=$.extend(data,{"labels":WGAccountAccountDetails.labels});
		   
		   $("#webGroupIds" ).empty();
		   var currentAccount = InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/accountMgmt/webGroupMgmt/webGroupCurrentAccountTemplate.html");
		   $.template( "currentAccount", currentAccount);
		   InsightCommon.renderTemplate("currentAccount",data,"#webGroupIds");	
		   webGroupCurrentSendData(webGroupData,accountsData);
		   
	  },
	  loadUserInformation:function(){
		  
		  var response=InsightCommon.getServiceResponse("/insightweb/transaction/getUserInformation",null,"GET",false);
		  Insight.userInformation= response.UserInformation;	  
	  },loadUserInternalPermissions: function(){
		  
		 var response=InsightCommon.getServiceResponse("/insightweb/transaction/getInternalPermissions",null,"POST");
		  if(response.Permissions!=""){
			  Insight.userInternalPermissions= response.Permissions;			 
		  }
		  else
			  Insight.userInternalPermissions=null;
	  },
	  loadLocaleMenu : function ()
	  {		   
		  var urlDomain = InsightCommon.getRootURL();		  
		  
		  var callbackFunct = function(data) {
			  	var localesLabels = eval('(' + data + ')');							
				//update selected locale display	
				$('#currentLocale').html(eval('localesLabels.labels.'+Insight.locale+".text"));
	
				
				if(!(InsightGLMPages.loggedInFlag))
				{
					InsightCommon.getContentAsync(InsightNavigation.contentURL, '', "http://www.insight.com/insightweb/assets/en_US/www/javascript/header/localesTemplate.html", function(data) {
						$.template( "localesTemplate", data);
						InsightCommon.renderTemplate( "localesTemplate",localesLabels,"#localesHeader"); 	
					});
				}	
		  	};
		  
		  
		  	//get the locale menu based on the environment
		  	if(urlDomain.indexOf("localhost") > 0)
		  	{
		  		InsightCommon.getMessageContentAsync (InsightNavigation.messageUrl ,Insight.locale,"http://www.insight.com/messages/header/localesLabels.txt",function(data) {
		  			callbackFunct(data);		  			
		  		});
		  	}
		  	else	  	
		  	{
		  		InsightCommon.getMessageContentAsync (InsightNavigation.messageUrl ,Insight.locale,Insight.envConfigs.localesMenu,function(data) {
		  			callbackFunct(data);		  			
		  		});
		  		
		  	}			  	
		  	
	  },
	  changeLocale : function (localeVal, localeLang)
	  {		
		  if(Insight.userInformation!=null && Insight.userInformation.loginAsFlag && 
				  (Insight.locale=='fr_CA'||Insight.locale=='en_CA' || Insight.locale == 'en_CN' || Insight.locale=='zh_CN') 
				  && (localeLang=='fr_CA'||localeLang == 'en_CA' || localeLang == 'en_CN' || localeLang == 'zh_CN'))
		  {			
		  			
				  //update cookie with new locale and reload the welcomepage.
				  $.cookie('insight_locale', localeLang, { expires: Insight.envConfigs.defaultLocaleExpire, path: "/"});			 				  			  
				  window.location.reload();
			  
			  
		  }else{
		  //if external site, redirect to it
		  if(localeVal.indexOf("http") != -1 && localeLang == '') {
			  //redirect to external regional site for unsupported locales
			  window.location = localeVal;			  
		  }	
		  //if not a insightweb url but a cms content url
		  else if(localeVal.indexOf("http") != -1 && window.location.href.indexOf("insightweb") == -1) {
			  //different country domain, redirect to homepage of the new locale			  
			  if(localeVal.indexOf("//"+document.domain) == -1) {
			  window.location = localeVal;			  
		  }	
			  //same domain, just switching language, update CMS url with the right locale path
			  else {
				  var currentUrl = window.location.href.replace("/content/insight","");				  
				  var elems = currentUrl.split("/");
				  if(elems.length > 5 && elems[3] != null && elems[3].length == 2 && elems[4] != null && elems[4].length == 2) {
					  elems[4] = localeLang.substring(0,2);
					  window.location = elems.join("/");
				  }
				  else {
					  window.location = localeVal;
				  }  
			  }			  
		  }	
 		  //if a insightweb url, but different country domain, redirect to homepage of the new locale
		  else if(localeVal.indexOf("http") != -1 && localeVal.indexOf("//"+document.domain) == -1) {
			  //redirect to external regional site for unsupported locales
			  window.location = localeVal;			  
		  }	
		  //if localhost or insightweb url of the same domain, reload page with new language
		  else
		  {		  			  			 
			  //update cookie with new locale
			  $.cookie('insight_locale', localeLang, { expires: Insight.envConfigs.defaultLocaleExpire, path: "/"});
			 				  			  
			  window.location.reload();
		  }
		}
	  },
	  changeToInternalRep: function()
	  {	  
		//alert(Insight.cartItemsLength);
        if(Insight.cartItemsLength==0)
			 {
	        	var searchURL=InsightCommon.getRootURL()+"/insightweb/endUser/changeInternalUserRole?roleType=InternalRole";
	  	        window.location.replace(searchURL);
			 }
		 else
			 {
			  var diagTxt=Insight.globalLabels.WarningtemsinCartwillremoved;
				
				$( '<div></div>')
				.html(diagTxt)
			    .dialog({
			          resizable: false,
			          modal: true,
			          title : "",
			          width:"661px",
			          hieght:"150px",
			          buttons: {
			                "Cancel": function() {
			                      $( this ).dialog( "close" );
			                },
			                "ok ": function() {
			                      $( this ).dialog( "close" );
			                      var searchURL=InsightCommon.getRootURL()+"/insightweb/endUser/changeInternalUserRole?roleType=ClientAdmin";
			      	  	           window.location.replace(searchURL);
			                }
			          }
			    });
				$(".ui-dialog-titlebar").hide(); 
			 }
	  },
	  clearDate: function (input,inst) {
			//add in clear date button to calendar
	      	setTimeout(function() {
	          var buttonPane = $( input )
	            .datepicker( "widget" )
	            .find( ".ui-datepicker-buttonpane" );
	          
	          buttonPane.find('button').hide();
	          
	         var buttonWrapper =  $( "<div>", {
	                                 
	            }).appendTo( buttonPane );
	          buttonWrapper.addClass("buttons");
	          
	          $( "<a>", {
	            text: Insight.globalLabels.clearButton,
	            style: "float:right;padding:5px;",
	            href: "javascript:void(0);",
	            click: function() {
	              $.datepicker._clearDate( input );
	            }
	          }).appendTo( buttonWrapper ).addClass("grey");
	        }, 1 );
	},
    checkCartProducts : function(cloudurl) {
	    var isCartEmpty = InsightCommon.getServiceResponse('/insightweb/transaction/checkCloudCartProducts', '',"GET",false);
	    if (isCartEmpty.message == "FALSE"){
	           var confirmBoxReturnValue = confirm(Insight.globalLabels.cloudCartCheck);
	           if(confirmBoxReturnValue == true){
	                  var clearfromcart = InsightCommon.getServiceResponse('/insightweb/transaction/clearcart', '',"GET",false);
	                  window.location.href = cloudurl;
	                  return true;
	           }else{
	                  return false;
	}
	    }else{
	           window.location.href = cloudurl;                                                
	           return true;
	    }
	  
    },
	loadCreateAccountPage : function(){
		 if(Insight.apac){
			 var loc = Insight.envConfigs.cmsServer+"/support/create_account.html";
			 window.location.replace(loc);
		 }else{
		 var loc = document.domain;
		  if(loc.indexOf("localhost")==-1){
			   if(loc.indexOf("https:")==-1){
		          if(loc.indexOf("http:")!=-1){
		       	  loc = loc.replace("http:", "https:");
		            }
		          else{
		  	          loc="https://"+loc; 
		            }
		       }
			   loc = loc+"/insightweb/endUser/createAccount"; 
		  }
		  else{
			   loc = "/insightweb/endUser/createAccount"; 
		  }
		window.location.replace(loc);
		 }
	},

    

    /* Product menu restructure */
    subcategories: function() {
      var hidden = false;
      
      $("#megamenu13 ul.productsList > li").hover(
	      function() {
          hidden = false;
          $("#megamenu13 ul.productsList li .flyOut").hide();
        
          $(this).addClass("hover");
          $(this).find(".dropDownItem").addClass("hoverText");
          $('div:first',this).css('visibility', 'visible');
          $('.flyOut', this).show();
        },
        function() {
          hidden = true;
          $(this).find(".dropDownItem").removeClass("hoverText");
          $(this).removeClass("hover");

          var that = this;

          setTimeout(function() {
            if(hidden)
              $(that).find(".flyOut").hide();
          }, 2000);
        });

      $("div.flyOut").css("min-height", 340);
    }
  };
})();
/** Functions for global Support Popup Form **/
(function() { var InsightSupport = window.InsightSupport =
{       insightHelpLabelsObj:"",
		loadHelpDialog : function ()
	    {   $("#insightHelpSectionDialog").empty(); 	    	
			$("#insightHelpSectionDialog").dialog({ width: '550px',height:'auto',resizable: false, position: 'relative', draggable: true,     title: 'Insight Support' });
			$("#insightHelpSectionDialog").dialog('open');
			var insightHelpLabels = InsightCommon.getMessageContent (InsightNavigation.messageUrl ,Insight.locale,"http://www.insight.com/messages/header/insightHelpLabels.txt");			
	    	insightHelpLabelsObj = eval('(' + insightHelpLabels + ')');
	    	$.template( "InsightHelpFormTemplate", InsightCommon.getContent(InsightNavigation.contentURL, '', "http://www.insight.com/insightweb/assets/en_US/www/javascript/header/insightHelpSupportFormTmpl.html"));
	    	var insightHelpSelectData = InsightCommon.getServiceResponse("/insightweb/endUser/clientSupport/"+InsightCommon.getCurrentTimestamp(),null,"GET");
	    	insightHelpSelectData = $.extend({"SelectTopic":insightHelpSelectData},insightHelpLabelsObj);
	    	//JSON.stringify(insightHelpSelectData);
	        InsightCommon.renderTemplate("InsightHelpFormTemplate",insightHelpSelectData,"#insightHelpSectionDialog");        
	    },
	    loadHelpDialogClose : function ()
	    { 
			$("#insightHelpSectionDialog").dialog('close');
	    },
	    loadHelpDialogSubmit : function ()
	    { 
			if(InsightSupport.validateHelpForm())
				{
			    	var emailJSONStr='{"firstName":"","lastName":"", "company":"","phoneNumber":"","email":"","supportTopic":"","supportTopicEmail":"","contactVia":"","comments":""}';
					emailJSONObj = eval('(' + emailJSONStr + ')');
					emailJSONObj.firstName = $("#insightHelpFormFirstName").val();
					emailJSONObj.lastName = $("#insightHelpFormLastName").val();
					emailJSONObj.company = $("#insightHelpFormCompany").val();
					emailJSONObj.phoneNumber = reformatTelNo($("#insightHelpFormPhone").val());
					emailJSONObj.email = $("#insightHelpFormEmail").val();
					if($("#insightHelpFormSelectBoxID").val()==99)
						{
						emailJSONObj.supportTopic = "None Selected";
						emailJSONObj.supportTopicEmail = "http://www.insight.com/insightweb/assets/en_US/www/javascript/webprocessing@insight.com";
						}
					else
						{
						emailJSONObj.supportTopic = $('#insightHelpFormSelectBoxID option:selected').text();    
						emailJSONObj.supportTopicEmail = $("#insightHelpFormSelectBoxID").val();
						}					
					emailJSONObj.contactVia = $('input[name=insightFormContactvia]').filter(':checked').val(); 
					emailJSONObj.comments = $("#insightHelpFormComments").val();
					emailJSONStr = JSON.stringify(emailJSONObj);
				InsightCommon.getServiceResponse("/insightweb/endUser/sendWebSupportEmail",emailJSONStr,"POST");
				InsightSupport.loadHelpDialogClose();
				}			
	    },
	    validateHelpForm : function ()
	    { 
	    	var fName = $("#insightHelpFormFirstName").val();
	    	var lName = $("#insightHelpFormLastName").val();
	    	var Company = $("#insightHelpFormCompany").val();
	    	var email = $("#insightHelpFormEmail").val();
	    	var phone = $("#insightHelpFormPhone").val();
	    	var validateErr="";
	    	if(!InsightSupport.validateHelpFormText(fName))
	    		{
	    		validateErr="First Name Required.\n";
	    		}
	    	if(!InsightSupport.validateHelpFormText(lName))
    		{
    		 validateErr=validateErr+"Last Name Required.\n";
    		}
	    	if(!InsightSupport.validateHelpFormText(Company))
    		{
    		 validateErr=validateErr+"Company Name Required.\n";
    		}
	    	
	    	if(email)
	    		{
	    		if(!InsightCommon.validateEmail(email))
	    			{
	    			validateErr=validateErr+"Valid Email Address Required.\n";
	    			}
	    		}
	    	else
	    		{
	    		    validateErr=validateErr+"Email Address Required.\n";
	    		}
	    	if(phone==""||phone==null)
	    		{
	    		   validateErr=validateErr+"Phone Number Required.\n";
	    		}
	    	
	    	if(validateErr!="")
	    		{
	    		 alert(validateErr);
	    		}
	    	else
	    		{
	    		return true;
	    		}
	    	
	    },
	    validateHelpFormText : function (text)
	    { 
			if (text==""||text==null)
				return false;
			else if(text)
				{
				var pattern = /^[A-Za-z0-9 ]{3,20}$/;
				if(text.match(pattern))
					{
					  return true;
					}
				else
					return false;
				}
				
	    }
}
})();

$(document).ready(function()
{
	 $.address.change(function(e) {

	     if(e.parameterNames != null && e.parameterNames.length > 1) {
	    	 
	        if (e.parameterNames[1] == "region") {
	            InsightNavigation.getIPSContractsDropdown(e.parameters.segment, e.parameters.region);                
	        }
	        else if (e.parameterNames[0] == "id") {
	            InsightNavigation.displayContractDetail(e.parameters.id);
	        }
	     }
	 });
	 if (InsightNavigation.accountToolsData != null ) {
	 if (InsightNavigation.accountToolsData.vipTools.softwareContracts == "Software Contracts") {		 
		 $("#mySoftwareLA a").css("display","block");
	 }
	 }
	 
});
