/**
 * 
 *
 */
function submitme()
{
alert('hai');
document.forms[0].submit();
}
function reformatTelNo(telNo)
{
	if(telNo!="")
		{
		telNo = telNo.replace('-', '');
		telNo = telNo.replace('(', '');
		telNo = telNo.replace(')', '');
		telNo = telNo.replace(' ', '');
		return telNo;
		}
	else
		return null;
}
/*
FormatTelNo method takes in the current element of the form and formats the phone number
in "(123 456-7890" format. This method should be fired on every key entry
(using onKeyUp method) in the current element of the form. If any key other than 0 to 9
is entered, it erases that entry rightaway. Maxlength and size of the current
element of the form should be 13.
Eg. <netui:textBox size="13" maxlength="13" onKeyUp="JavaScript:formatTelNo (this);" onBlur="JavaScript:checkTelNo (this);" onKeyDown="JavaScript:formatTelNo (this);"/>
*/
function formatTelNo (telNo)
{
// If it's blank, save yourself some trouble by doing nothing.
if (telNo.value == "") return;
var phone = new String (telNo.value);
phone = phone.substring(0,14);
/*
"." means any character. If you try to use "(" and ")", the regular expression becomes
complicated sice both are reserve characters and escaping them sometimes fails. So just
use "." for any character and replace it later.
*/
if (phone.match (".[0-9]{3}.[0-9]{3}-[0-9]{4}") == null)
{
/*
Following "if" is for user making any changes to the formatted tel. no. If you don't put this
"if" condition, the user can not correct a digit by first deleting it and then entering a
correct one, since this will fire two "onkeyup" events : first one on deleting a
character and second one on entering the correct one. The first "onkeyup" event will fire this
function which will reformatt the tel no before the user gets a chace to correct the digit. This
will surely confuse the user. The "if" condition below eliminates that.
*/
if (phone.match (".[0-9]{2}.[0-9]{3}-[0-9]{4}|" + ".[0-9].[0-9]{3}-[0-9]{4}|" +
".[0-9]{3}.[0-9]{2}-[0-9]{4}|" + ".[0-9]{3}.[0-9]-[0-9]{4}") == null)
{
/*
You will reach here only if the user is still typing the number or if he/she has
messed up already formatted number.
*/
var phoneNumeric = phoneChar = "", i;
// Loop thru what user has entered.
for (i=0;i<phone.length;i++)
{
// Go thru what user has entered one character at a time.
phoneChar = phone.substr (i,1);
// If that character is not a number or is a White space, ignore it. Only if it is a digit,
// concatinate it with a number string.
if (!isNaN (phoneChar) && (phoneChar != " ")) phoneNumeric = phoneNumeric + phoneChar;
}
phone = "";
// At this point, you have picked up only digits from what user has entered. Loop thru it.
for (i=0;i<phoneNumeric.length;i++)
{
// If it's the first digit, throw in "(" before that.
if (i == 0) phone = phone + "(";
// If you are on the 4th digit, put ") " before that.
if (i == 3) phone = phone + ") ";
// If you are on the 7th digit, insert "-" before that.
if (i == 6) phone = phone + "-";
// Add the digit to the phone charatcer string you are building.
phone = phone + phoneNumeric.substr (i,1)
}
}
}
else
{
// This means the tel no is in proper format. Make sure by replacing the 0th, 4th and 8th character.
phone = "(" + phone.substring (1,4) + ") " + phone.substring (5,8) + "-" + phone.substring(9,13);
}
// So far you are working internally. Refresh the screen with the re-formatted value.
if (phone != telNo.value) telNo.value = phone;
}
/*
CheckTelNo method takes in current element of the form as input. This method should be
fired as the user attempts to leave the current element in the form (by using onBlur method).
It checks to see if the format of the phone is "(123) 456-7890".
Eg. <netui:textBox size="13" maxlength="13" onBlur="JavaScript:checkTelNo (this);" onKeyUp="JavaScript:formatTelNo (this);" onKeyDown="JavaScript:formatTelNo (this);"/>
*/
function checkTelNo (telNo)
{
if (telNo.value == "") return;
if (telNo.value.match (".[0-9]{3}.[0-9]{3}-[0-9]{4}") == null)
{
if (telNo.value.match ("[0-9]{10}") != null)
formatTelNo (telNo)
}
}
//Enter-listener
//if the user presses ENTER KEY submit the Form
//Added by Guruprasad Thorvey on 03/23/05 to fix issue #21632
function checkEnterForFindListing(e){
var characterCode;
if(e && e.which){
e = e;
characterCode = e.which ;
}
else{
e = event;
characterCode = e.keyCode;
}
if(characterCode == 13){ //13 = the code for pressing ENTER
document.forms[getNetuiTagName("findListingForm")].submit();
return false;
}
else{
return true ;
}
}
function validatePwd(){
var invalidVal ="!@#$%^&*()-_=+\|[]{};:/?.>< ";
var isInValidPwd = false;
var obj = document.getElementById("pwd");
var elementValue ="";
var capErr = document.getElementById("captchaRes-error");
if(capErr!=null){
capErr.style.display = 'none';
}
if(obj!=null){
elementValue = obj.value;
for (var i=0; i<elementValue.length; i++) {
indVal = "" + elementValue.substring(i, i+1);
if(!(isStringValid(invalidVal,indVal)))
{
isInValidPwd = true;
break;
}
}
}
if(isInValidPwd){
if(document.getElementById("invalidPwdChar")!=null) {
document.getElementById("invalidPwdChar").style.display = 'block';
}
}else
document.getElementById("invalidPwdChar").style.display = 'none';
if(isInValidPwd)
return false;
else
return true;
}
function isStringValid(invalidchars,str)
{
for(var i=0;i<str.length;i++){
if(invalidchars.lastIndexOf(str.charAt(i)) != -1){
return false;
}
}
return true;
}
function findbusiness(link){
var ph = document.getElementById("phNum");
var sgsrc = $('#sgsrc').attr('value');
if(ph!=null){
if(sgsrc == 'SGL')
link.href = "http://www.insight.com/spportal/spportalFlow.do?_flowId=microsite-flow&sgsrc=SGl&phNum="+ph.value;
else
link.href = "http://www.insight.com/spportal/spportalFlow.do?_flowId=microsite-flow&phNum="+ph.value;
window.location = link.href;
return true;
}else
return false;
}