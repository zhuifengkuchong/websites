var productReturnRequestLabelsObj=null;
var messageUrl="/insightweb/getCmsContent";

function loadProductReturnRequestForm()
 {
	  var productReturnRequestLabels = InsightCommon.getMessageContent(messageUrl, Insight.locale, "/messages/accountMgmt/webGroupMgmt/productReturnRequestFormLabels.txt/jcr:content/jcr:data.txt");
	// var productReturnRequestLabels = InsightCommon.getContent(InsightNavigation.contentURL, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/accountMgmt/webGroupMgmt/productReturnRequestFormLabels.txt");					
	  productReturnRequestLabelsObj = eval('(' + productReturnRequestLabels + ')');	
		
	   $("#bodyContent").empty();
	   var updateAddressBookTempl = InsightCommon.getContent(InsightSearch.staticContentUrl, "", "http://www.insight.com/insightweb/assets/en_US/www/javascript/accountMgmt/webGroupMgmt/productReturnRequestForm.html");
	   $.template( "updateAddressBookTemplate", updateAddressBookTempl);
	   InsightCommon.renderTemplate( "updateAddressBookTemplate",productReturnRequestLabelsObj,"#bodyContent"); 
	   $("#returnErrors").empty().css("display","none");
 }
function validateProductReturnRequestForm()
{
	var errorMessages=[];
	var i=0;
	if($("#productReturnRequestContactName").val()=="")
		{
		  errorMessages[i]=productReturnRequestLabelsObj.labels.enterContactName;
		  i++;
		}
	if($("#productReturnRequestCompanyName").val()=="")
	    {
		  errorMessages[i]=productReturnRequestLabelsObj.labels.enterCompanyName;
		  i++;
	    }
	if($("#productReturnRequestAccountNumber").val()=="")
		{
		  errorMessages[i]=productReturnRequestLabelsObj.labels.enterAccountNumber;
		  i++;
		}
	if($("#productReturnRequestPhone").val()=="")
		{
		  errorMessages[i]=productReturnRequestLabelsObj.labels.enterPhoneNumber;
		  i++;
		}
	if($("#productReturnRequestFax").val()=="")
		{
		  errorMessages[i]=productReturnRequestLabelsObj.labels.enterFaxNumber;
		  i++;
		}
	if($("#productReturnRequestEmail").val()=="")
		{
		  errorMessages[i]=productReturnRequestLabelsObj.labels.pleaseEnterEmail;
		  i++;
		}
	if($("#productReturnRequestEmail").val()!="" && (!InsightCommon.validateEmail($.trim($("#productReturnRequestEmail").val()))))
	{
	  errorMessages[i]=productReturnRequestLabelsObj.labels.enterValidEmail;
	  i++;
	}
	if($("#productReturnRequestOrderNumber").val()=="")
		{
		  errorMessages[i]=productReturnRequestLabelsObj.labels.enterOrderNumber;
		  i++;
		}
	if($("#productReturnRequestItemNumber").val()=="")
		{
		  errorMessages[i]=productReturnRequestLabelsObj.labels.enterItemNumber;
		  i++;
		}
	if($("#productReturnRequestProductDesc").val()=="")
		{
		  errorMessages[i]=productReturnRequestLabelsObj.labels.enterProductDescription;
		  i++;
		}
	if($("#productReturnRequestSerialNumber").val()=="")
		{
		  errorMessages[i]=productReturnRequestLabelsObj.labels.enterSerialNUmber;
		  i++;
		}
	if($("#productReturnRequestQuantity").val()=="")
		{
		  errorMessages[i]=productReturnRequestLabelsObj.labels.pleaseEnterQuantity;
		  i++;
		}
	if($("#productReturnRequestReason").val()=="")
		{
		  errorMessages[i]=productReturnRequestLabelsObj.labels.enterRequestReason;
		  i++;
		}
	
	if(!$('input[name=productCondition]').is(':checked')){
		errorMessages[i]=productReturnRequestLabelsObj.labels.pleaseSelectProductCondition;
		  i++;
	}
	return errorMessages;
	
}
function submitProductReturnRequestForm()
{
	
	$("#returnErrors").empty().css("display","none");
	var errorMessages=validateProductReturnRequestForm();
	var errorMessage="";
	if(errorMessages.length>0)
		{
		   $.each(errorMessages, function(index,value){
			   errorMessage=errorMessage+(index+1)+"."+value + "<br>";
		   }); 
		   
		   $("#returnErrors").html(errorMessage).css("display","block");
		}
	else
	{
		var submitProductReturnRequestFormReqObj = {
			    "contactName": "",
			    "companyName": "",
			    "accountNumber": "",
			    "phone": "",
			    "fax": "",
			    "email": "",
			    "orderNumber": "",
			    "itemNumber": "",
			    "productDesc": "",
			    "serialNumber": "",
			    "quantity": "",
			    "reason": "",
			    "productCondition": "",
			    "contentsComplete": "yes"
			};
		submitProductReturnRequestFormReqObj.contactName = $.trim($("#productReturnRequestContactName").val());
		submitProductReturnRequestFormReqObj.companyName = $.trim($("#productReturnRequestCompanyName").val());
		submitProductReturnRequestFormReqObj.accountNumber = $.trim($("#productReturnRequestAccountNumber").val());
		submitProductReturnRequestFormReqObj.phone = $.trim(($("#productReturnRequestPhone").val()));
		submitProductReturnRequestFormReqObj.fax = $.trim($("#productReturnRequestFax").val());
		submitProductReturnRequestFormReqObj.email = $.trim($("#productReturnRequestEmail").val());
		submitProductReturnRequestFormReqObj.orderNumber = $.trim($("#productReturnRequestOrderNumber").val());
		submitProductReturnRequestFormReqObj.itemNumber = $.trim($("#productReturnRequestItemNumber").val());
		submitProductReturnRequestFormReqObj.productDesc = $.trim($("#productReturnRequestProductDesc").val());
		submitProductReturnRequestFormReqObj.quantity = $.trim($("#productReturnRequestQuantity").val());
		submitProductReturnRequestFormReqObj.serialNumber = $.trim($("#productReturnRequestSerialNumber").val());
		submitProductReturnRequestFormReqObj.reason = $.trim($("#productReturnRequestReason").val());
		submitProductReturnRequestFormReqObj.productCondition = $('input[name=productCondition]').filter(':checked').val();
		if(submitProductReturnRequestFormReqObj.productCondition =='open'){
			submitProductReturnRequestFormReqObj.contentsComplete = $('input[name=productConditionContents]').filter(':checked').val();
		}
		
		submitProductReturnRequestFormReqObj = JSON.stringify(submitProductReturnRequestFormReqObj);
		
		
		if (Insight.apac) {
			InsightCommon.getServiceResponse("/insightweb/endUser/senRmaRequestEmail/true",submitProductReturnRequestFormReqObj,"POST");			
		}	else {
			InsightCommon.getServiceResponse("/insightweb/endUser/senRmaRequestEmail/false",submitProductReturnRequestFormReqObj,"POST");
		}
		 window.location.reload(); 
	
}
}














