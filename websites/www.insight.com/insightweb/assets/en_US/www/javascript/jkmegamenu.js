/* jQuery Mega Menu v1.02
* Last updated: June 29th, 2009. This notice must stay intact for usage 
* Author: JavaScript Kit at http://www.javascriptkit.com/
* Visit http://www.javascriptkit.com/script/script2/jScale/ for full source code
*/

//jQuery.noConflict();
var jkmegamenu={
effectduration: 200, //duration of animation, in milliseconds
delaytimer: 1000, //delay after mouseout before menu should be hidden, in milliseconds

//No need to edit beyond here
megamenulabels: [],
maxTabHeight: 0,
megamenus: [], //array to contain each block menu instances
zIndexVal: 1000, //starting z-index value for drop down menu
$shimobj: null,

addshim:function($){
	$(document.body).append('<IFRAME id="outlineiframeshim" src="'+(location.protocol=="https:"? 'about:blank' : 'about:blank')+'" style="display:none; left:0; top:0; z-index:999; overflow:hidden; position:absolute; filter:progid:DXImageTransform.Microsoft.Alpha(style=0,opacity=0)" frameBorder="0" scrolling="no"></IFRAME>')
	this.$shimobj=$("#outlineiframeshim")
},

alignmenu:function($, e, megamenu_pos){
	var megamenu=this.megamenus[megamenu_pos]
	var $anchor=megamenu.$anchorobj
	var $menu=megamenu.$menuobj
	var menuleft=megamenu.$anchorobj.position().left
	if (megamenu.$anchorobj.selector == '#megaanchor3'){
		//reposition products menu
		menuleft = menuleft - 180;
		if (document.getElementById('megaanchor1') != null){
			if (document.getElementById('megaanchor1').style.display != 'none')
			{
				menuleft = menuleft - 100;
			}
		}
		if (document.getElementById('megaanchor2') != null){
			if (document.getElementById('megaanchor2').style.display != 'none')
			{
				menuleft = menuleft - 100;	
			}
		}

	}
	if (megamenu.$anchorobj.selector == '#megaanchor5'){
		menuleft = menuleft-85;
	}
	if (megamenu.$anchorobj.selector == '#megaanchor6'){
		menuleft = menuleft-95;
	}	
	if (megamenu.$anchorobj.selector == '#ipsContractAnchor'){
		menuleft = menuleft-400;
	}
	var menutop = 0;
	megamenu.offsety=megamenu.$anchorobj.position().top;
	if (megamenu.$anchorobj.selector == '#megaanchor1' ||
		megamenu.$anchorobj.selector == '#megaanchor2' ||
		megamenu.$anchorobj.selector == '#megaanchor13'||
		megamenu.$anchorobj.selector == '#megaanchor4' ||
		megamenu.$anchorobj.selector == '#megaanchor8'){
	 	if(/Safari/i.test(navigator.userAgent) || /Firefox/i.test(navigator.userAgent)){
			menutop=megamenu.offsety+megamenu.anchorheight  + 3;//get y coord of menu
		}
		else{
			menutop=megamenu.offsety+megamenu.anchorheight  + 4;//get y coord of menu
		}
	}
	else{
		menutop=megamenu.offsety+megamenu.anchorheight + 2
	}
	
	if($.browser.msie){
		userAgent = $.browser.version
		userAgent = userAgent.substring(0,userAgent.indexOf('.'));
		version = userAgent
		if (version == 7){
		  menutop=megamenu.offsety+13+1;//get y coord of menu
		  if (megamenu.$anchorobj.selector == '#megaanchor1' ||
		  		megamenu.$anchorobj.selector == '#megaanchor2' ||
				megamenu.$anchorobj.selector == '#megaanchor3' ||
				megamenu.$anchorobj.selector == '#megaanchor13' ||
				megamenu.$anchorobj.selector == '#megaanchor4' ||
				megamenu.$anchorobj.selector == '#megaanchor8'){
			menutop=megamenu.offsety+megamenu.anchorheight  + 2;//get y coord of menu
		  }
		}
		
	}
	
	if (megamenu.$xcorrection != undefined){
		menuleft = menuleft - megamenu.$xcorrection
	}
	if (megamenu.$ycorrection != undefined){
		menutop = menutop - megamenu.$ycorrection
	}
	$menu.css({left:menuleft+"px", top:menutop+"px"});
	this.$shimobj.css({width:megamenu.actualwidth+"px", height:megamenu.actualheight+"px", left:menuleft+"px", top:menutop+"px", display:"block"})
},


hideAllMenusExcept:function(menu_index){
    for(var i = 0; i < this.megamenus.length; i++) {
        if(i != menu_index) {
            this.hidemenu(0, i);
        }
    }
},


showmenu:function(e, megamenu_pos){	
	var megamenu=this.megamenus[megamenu_pos]	
	var $menu=megamenu.$menuobj
	var borderBottom = "5px solid " + $menu.css("border-left-color")
	megamenu.$anchorobj.css("border-bottom", borderBottom);
	//$('ul div.primary').css("display","none");
	var $menuinner=megamenu.$menuinner
	if ($menu.css("display")=="none"){
		this.alignmenu(jQuery, e, megamenu_pos)
		$menu.css("z-index", ++this.zIndexVal);

        this.hideAllMenusExcept(megamenu_pos);

		$menu.slideDown(this.effectduration, function(){
			$menuinner.css('visibility', 'visible')

			$('a.menuButton:eq(0)').width(309);
			$('a.menuButton:eq(1)').width(310);
			$('a.menuButton:eq(2)').width(309);
			
			if (megamenu.$anchorobj.selector == '#megaanchor3'){
				var maxHeight = Math.max(jQuery("#fragment-1").height(), jQuery("#fragment-2").height(), jQuery ("#fragment-3").height());
				$("#megamenu3").height(maxHeight);
				$("#fragment-1").height(maxHeight);
				$("#fragment-2").height(maxHeight);
				$("#fragment-3").height(maxHeight);
			}
			
			if(megamenu.$anchorobj.selector == '#accountToolsSpan'){
				var maxHeight = Math.max(jQuery("#accountToolsColumn1").height(), jQuery("#accountToolsColumn2").height(), 
				$("#accountToolsColumn3").height(), jQuery ("#accountToolsColumn4").height(),
				$("#accountToolsColumn5").height())
				$("#accountToolsColumn1").height(maxHeight)
				$("#accountToolsColumn2").height(maxHeight)
				$("#accountToolsColumn3").height(maxHeight)
				$("#accountToolsColumn4").height(maxHeight)
				$("#accountToolsColumn5").height(maxHeight)
			}
		})
	}
	return false
},

hidemenu:function(e, megamenu_pos){
	var megamenu=this.megamenus[megamenu_pos]
	var $menu=megamenu.$menuobj
	var $menuinner=megamenu.$menuinner
	$menuinner.css('visibility', 'hidden')
	this.$shimobj.css({display:"none", left:0, top:0})
	$menu.hide()
	megamenu.$anchorobj.css("border-bottom", "0px"); 
},

definemenu:function(anchorid, menuid, revealtype, xcorrection, ycorrection){
	//alert("Width="+$('#'+menuid).width());
	var alreadyExists = false;
	for (var i=0; i < this.megamenulabels.length; i ++){
	   var labels = this.megamenulabels[i]
	   if (anchorid == labels[0]){
	    alreadyExists = true;
	   }
	}
	if (alreadyExists == false){
	  this.megamenulabels.push([anchorid, menuid, revealtype, xcorrection, ycorrection])
	}
},

render:function($){
	for (var i=0, labels=this.megamenulabels[i]; i<this.megamenulabels.length; i++, labels=this.megamenulabels[i]){
	    var alreadyExists = false;
	    var index = 0;
	    for (var j=0; j < this.megamenus.length; j ++){
	           var spanLabel = "#" + labels[0]
	    	   if (spanLabel == this.megamenus[j].$anchorobj.selector){
	    	   	alreadyExists = true;
	    	   	index = j;
	    	   	
	    	   }
	     }
		if (alreadyExists){
			this.megamenus[index].$anchorobj = $("#"+labels[0])
			this.megamenus[index].$menuobj = $("#"+labels[1])
			this.megamenus[index].$menuinner = $("#"+labels[1]).children('ul:first-child')
			this.megamenus[index].revealtype = labels[2]
			this.megamenus[index].hidetimer = null
		}
		else{
			this.megamenus.push({$anchorobj:$("#"+labels[0]), $menuobj:$("#"+labels[1]), $menuinner:$("#"+labels[1]).children('ul:first-child'), revealtype:labels[2], hidetimer:null});
		}
		var megamenu=this.megamenus[i]	
		megamenu.$anchorobj.add(megamenu.$menuobj).attr("_megamenupos", i+"pos");
		megamenu.actualwidth=megamenu.$menuobj.outerWidth();
		megamenu.actualheight=megamenu.$menuobj.outerHeight();
		megamenu.offsetx=megamenu.$anchorobj.position().left;
		megamenu.offsety=megamenu.$anchorobj.position().top;
		megamenu.$xcorrection = labels[3]
		megamenu.$ycorrection = labels[4]
		megamenu.anchorwidth=megamenu.$anchorobj.outerWidth()
		megamenu.anchorheight=megamenu.$anchorobj.outerHeight()
		$(document.body).append(megamenu.$menuobj)
		megamenu.$menuobj.css("z-index", ++this.zIndexVal).hide()
		megamenu.$menuinner.css("visibility", "hidden")
		megamenu.$anchorobj.bind(megamenu.revealtype=="click"? "click" : "mouseenter", function(e){
			var menuinfo=jkmegamenu.megamenus[parseInt(this.getAttribute("_megamenupos"))]
			clearTimeout(menuinfo.hidetimer);
			return jkmegamenu.showmenu(e, parseInt(this.getAttribute("_megamenupos")))
		})
		megamenu.$anchorobj.bind("mouseleave", function(e){
			var menuinfo=jkmegamenu.megamenus[parseInt(this.getAttribute("_megamenupos"))]	
			if (e.relatedTarget!=menuinfo.$menuobj.get(0) && $(e.relatedTarget).parents("#"+menuinfo.$menuobj.get(0).id).length==0 && this.getAttribute("id") != "megaanchor6"){ //check that mouse hasn't moved into menu object
				menuinfo.hidetimer=setTimeout(function(){ 
					jkmegamenu.hidemenu(e, parseInt(menuinfo.$menuobj.get(0).getAttribute("_megamenupos")))
				}, jkmegamenu.delaytimer)				
			}
			//customization to handle header login form
			else if(this.getAttribute("id") == "megaanchor6") {
				$(window).resize(function() {
					jkmegamenu.hidemenu(e, parseInt(menuinfo.$menuobj.get(0).getAttribute("_megamenupos")));
				});
				$('#header-cancel').click(function() {				
					jkmegamenu.hidemenu(e, parseInt(menuinfo.$menuobj.get(0).getAttribute("_megamenupos")));
				});
			}
		})
		megamenu.$menuobj.bind("mouseenter", function(e){
			var menuinfo=jkmegamenu.megamenus[parseInt(this.getAttribute("_megamenupos"))]
			clearTimeout(menuinfo.hidetimer) 
		})
		megamenu.$menuobj.bind("mouseleave", function(e){			
			var menuinfo=jkmegamenu.megamenus[parseInt(this.getAttribute("_megamenupos"))]	
			//customization to handle header login form			
			if(this.getAttribute("id") != "megamenu6")
			{
				menuinfo.hidetimer=setTimeout(function(){
					jkmegamenu.hidemenu(e, parseInt(menuinfo.$menuobj.get(0).getAttribute("_megamenupos")))
				}, jkmegamenu.delaytimer)
			}
		})
	} //end for loop
	if(/Safari/i.test(navigator.userAgent)){ //if Safari
		$(window).bind("resize load", function(){
			for (var i=0; i<jkmegamenu.megamenus.length; i++){
				var megamenu=jkmegamenu.megamenus[i]
				var $anchorisimg=(megamenu.$anchorobj.children().length==1 && megamenu.$anchorobj.children().eq(0).is('img'))? megamenu.$anchorobj.children().eq(0) : null
				if ($anchorisimg){ //if anchor is an image link, get offsets and dimensions of image itself, instead of parent A
					megamenu.offsetx=$anchorisimg.offset().left
					megamenu.offsety=$anchorisimg.offset().top
					megamenu.anchorwidth=$anchorisimg.width()
					megamenu.anchorheight=$anchorisimg.height()
				}
			}
		})
	}
	else{
		$(window).bind("resize", function(){
			for (var i=0; i<jkmegamenu.megamenus.length; i++){
				var megamenu=jkmegamenu.megamenus[i]	
				megamenu.offsetx=megamenu.$anchorobj.offset().left
				megamenu.offsety=megamenu.$anchorobj.offset().top
			}
		})
	}
	jkmegamenu.addshim($)
}

}

$(document).ready(function($){
	jkmegamenu.render($)
});