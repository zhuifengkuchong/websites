<script id = "race83b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race83={};
	myVars.races.race83.varName="RegExp[2651].lastIndex";
	myVars.races.race83.varType="@varType@";
	myVars.races.race83.repairType = "@RepairType";
	myVars.races.race83.event1={};
	myVars.races.race83.event2={};
	myVars.races.race83.event1.id = "Lu_Id_li_9";
	myVars.races.race83.event1.type = "onmouseout";
	myVars.races.race83.event1.loc = "Lu_Id_li_9_LOC";
	myVars.races.race83.event1.isRead = "False";
	myVars.races.race83.event1.eventType = "@event1EventType@";
	myVars.races.race83.event2.id = "Lu_Id_li_8";
	myVars.races.race83.event2.type = "onmouseout";
	myVars.races.race83.event2.loc = "Lu_Id_li_8_LOC";
	myVars.races.race83.event2.isRead = "False";
	myVars.races.race83.event2.eventType = "@event2EventType@";
	myVars.races.race83.event1.executed= false;// true to disable, false to enable
	myVars.races.race83.event2.executed= false;// true to disable, false to enable
</script>

