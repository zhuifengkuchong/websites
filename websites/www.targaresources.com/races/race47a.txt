<script id = "race47a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race47={};
	myVars.races.race47.varName="RegExp[2651].lastIndex";
	myVars.races.race47.varType="@varType@";
	myVars.races.race47.repairType = "@RepairType";
	myVars.races.race47.event1={};
	myVars.races.race47.event2={};
	myVars.races.race47.event1.id = "Lu_Id_li_12";
	myVars.races.race47.event1.type = "onmouseover";
	myVars.races.race47.event1.loc = "Lu_Id_li_12_LOC";
	myVars.races.race47.event1.isRead = "False";
	myVars.races.race47.event1.eventType = "@event1EventType@";
	myVars.races.race47.event2.id = "Lu_Id_li_13";
	myVars.races.race47.event2.type = "onmouseover";
	myVars.races.race47.event2.loc = "Lu_Id_li_13_LOC";
	myVars.races.race47.event2.isRead = "False";
	myVars.races.race47.event2.eventType = "@event2EventType@";
	myVars.races.race47.event1.executed= false;// true to disable, false to enable
	myVars.races.race47.event2.executed= false;// true to disable, false to enable
</script>

