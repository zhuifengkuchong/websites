<script id = "race62a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race62={};
	myVars.races.race62.varName="Object[2565].a:eq(0)";
	myVars.races.race62.varType="@varType@";
	myVars.races.race62.repairType = "@RepairType";
	myVars.races.race62.event1={};
	myVars.races.race62.event2={};
	myVars.races.race62.event1.id = "Lu_Id_li_5";
	myVars.races.race62.event1.type = "onmouseover";
	myVars.races.race62.event1.loc = "Lu_Id_li_5_LOC";
	myVars.races.race62.event1.isRead = "False";
	myVars.races.race62.event1.eventType = "@event1EventType@";
	myVars.races.race62.event2.id = "Lu_Id_li_21";
	myVars.races.race62.event2.type = "onmouseover";
	myVars.races.race62.event2.loc = "Lu_Id_li_21_LOC";
	myVars.races.race62.event2.isRead = "True";
	myVars.races.race62.event2.eventType = "@event2EventType@";
	myVars.races.race62.event1.executed= false;// true to disable, false to enable
	myVars.races.race62.event2.executed= false;// true to disable, false to enable
</script>

