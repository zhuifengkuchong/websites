<script id = "race95a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race95={};
	myVars.races.race95.varName="RegExp[2651].lastIndex";
	myVars.races.race95.varType="@varType@";
	myVars.races.race95.repairType = "@RepairType";
	myVars.races.race95.event1={};
	myVars.races.race95.event2={};
	myVars.races.race95.event1.id = "Lu_Id_li_13";
	myVars.races.race95.event1.type = "onmouseout";
	myVars.races.race95.event1.loc = "Lu_Id_li_13_LOC";
	myVars.races.race95.event1.isRead = "False";
	myVars.races.race95.event1.eventType = "@event1EventType@";
	myVars.races.race95.event2.id = "Lu_Id_li_14";
	myVars.races.race95.event2.type = "onmouseout";
	myVars.races.race95.event2.loc = "Lu_Id_li_14_LOC";
	myVars.races.race95.event2.isRead = "False";
	myVars.races.race95.event2.eventType = "@event2EventType@";
	myVars.races.race95.event1.executed= false;// true to disable, false to enable
	myVars.races.race95.event2.executed= false;// true to disable, false to enable
</script>

