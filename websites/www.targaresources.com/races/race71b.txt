<script id = "race71b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race71={};
	myVars.races.race71.varName="RegExp[2651].lastIndex";
	myVars.races.race71.varType="@varType@";
	myVars.races.race71.repairType = "@RepairType";
	myVars.races.race71.event1={};
	myVars.races.race71.event2={};
	myVars.races.race71.event1.id = "Lu_Id_li_26";
	myVars.races.race71.event1.type = "onmouseover";
	myVars.races.race71.event1.loc = "Lu_Id_li_26_LOC";
	myVars.races.race71.event1.isRead = "False";
	myVars.races.race71.event1.eventType = "@event1EventType@";
	myVars.races.race71.event2.id = "Lu_Id_li_25";
	myVars.races.race71.event2.type = "onmouseover";
	myVars.races.race71.event2.loc = "Lu_Id_li_25_LOC";
	myVars.races.race71.event2.isRead = "False";
	myVars.races.race71.event2.eventType = "@event2EventType@";
	myVars.races.race71.event1.executed= false;// true to disable, false to enable
	myVars.races.race71.event2.executed= false;// true to disable, false to enable
</script>

