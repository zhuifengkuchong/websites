<script id = "race105a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race105={};
	myVars.races.race105.varName="Object[2565].ul:eq(0)";
	myVars.races.race105.varType="@varType@";
	myVars.races.race105.repairType = "@RepairType";
	myVars.races.race105.event1={};
	myVars.races.race105.event2={};
	myVars.races.race105.event1.id = "Lu_Id_li_5";
	myVars.races.race105.event1.type = "onmouseover";
	myVars.races.race105.event1.loc = "Lu_Id_li_5_LOC";
	myVars.races.race105.event1.isRead = "False";
	myVars.races.race105.event1.eventType = "@event1EventType@";
	myVars.races.race105.event2.id = "Lu_Id_li_19";
	myVars.races.race105.event2.type = "onmouseout";
	myVars.races.race105.event2.loc = "Lu_Id_li_19_LOC";
	myVars.races.race105.event2.isRead = "True";
	myVars.races.race105.event2.eventType = "@event2EventType@";
	myVars.races.race105.event1.executed= false;// true to disable, false to enable
	myVars.races.race105.event2.executed= false;// true to disable, false to enable
</script>

