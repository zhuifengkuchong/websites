<script id = "race67b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race67={};
	myVars.races.race67.varName="RegExp[2651].lastIndex";
	myVars.races.race67.varType="@varType@";
	myVars.races.race67.repairType = "@RepairType";
	myVars.races.race67.event1={};
	myVars.races.race67.event2={};
	myVars.races.race67.event1.id = "Lu_Id_li_22";
	myVars.races.race67.event1.type = "onmouseover";
	myVars.races.race67.event1.loc = "Lu_Id_li_22_LOC";
	myVars.races.race67.event1.isRead = "False";
	myVars.races.race67.event1.eventType = "@event1EventType@";
	myVars.races.race67.event2.id = "Lu_Id_li_21";
	myVars.races.race67.event2.type = "onmouseover";
	myVars.races.race67.event2.loc = "Lu_Id_li_21_LOC";
	myVars.races.race67.event2.isRead = "False";
	myVars.races.race67.event2.eventType = "@event2EventType@";
	myVars.races.race67.event1.executed= false;// true to disable, false to enable
	myVars.races.race67.event2.executed= false;// true to disable, false to enable
</script>

