<script id = "race63a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race63={};
	myVars.races.race63.varName="RegExp[2651].lastIndex";
	myVars.races.race63.varType="@varType@";
	myVars.races.race63.repairType = "@RepairType";
	myVars.races.race63.event1={};
	myVars.races.race63.event2={};
	myVars.races.race63.event1.id = "Lu_Id_li_20";
	myVars.races.race63.event1.type = "onmouseover";
	myVars.races.race63.event1.loc = "Lu_Id_li_20_LOC";
	myVars.races.race63.event1.isRead = "False";
	myVars.races.race63.event1.eventType = "@event1EventType@";
	myVars.races.race63.event2.id = "Lu_Id_li_21";
	myVars.races.race63.event2.type = "onmouseover";
	myVars.races.race63.event2.loc = "Lu_Id_li_21_LOC";
	myVars.races.race63.event2.isRead = "False";
	myVars.races.race63.event2.eventType = "@event2EventType@";
	myVars.races.race63.event1.executed= false;// true to disable, false to enable
	myVars.races.race63.event2.executed= false;// true to disable, false to enable
</script>

