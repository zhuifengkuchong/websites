<script id = "race112b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race112={};
	myVars.races.race112.varName="Object[2565].ul:eq(0)";
	myVars.races.race112.varType="@varType@";
	myVars.races.race112.repairType = "@RepairType";
	myVars.races.race112.event1={};
	myVars.races.race112.event2={};
	myVars.races.race112.event1.id = "Lu_Id_li_22";
	myVars.races.race112.event1.type = "onmouseout";
	myVars.races.race112.event1.loc = "Lu_Id_li_22_LOC";
	myVars.races.race112.event1.isRead = "True";
	myVars.races.race112.event1.eventType = "@event1EventType@";
	myVars.races.race112.event2.id = "Lu_Id_li_5";
	myVars.races.race112.event2.type = "onmouseover";
	myVars.races.race112.event2.loc = "Lu_Id_li_5_LOC";
	myVars.races.race112.event2.isRead = "False";
	myVars.races.race112.event2.eventType = "@event2EventType@";
	myVars.races.race112.event1.executed= false;// true to disable, false to enable
	myVars.races.race112.event2.executed= false;// true to disable, false to enable
</script>

