

// Place any jQuery/helper plugins in here.

/*! Backstretch - v2.0.3 - 2012-11-30
* http://srobbin.com/jquery-plugins/backstretch/
* Copyright (c) 2012 Scott Robbin; Licensed MIT */
(function (e, t, n) { "use strict"; e.fn.backstretch = function (r, s) { return (r === n || r.length === 0) && e.error("No images were supplied for Backstretch"), e(t).scrollTop() === 0 && t.scrollTo(0, 0), this.each(function () { var t = e(this), n = t.data("backstretch"); n && (s = e.extend(n.options, s), n.destroy(!0)), n = new i(this, r, s), t.data("backstretch", n) }) }, e.backstretch = function (t, n) { return e("body").backstretch(t, n).data("backstretch") }, e.expr[":"].backstretch = function (t) { return e(t).data("backstretch") !== n }, e.fn.backstretch.defaults = { centeredX: !0, centeredY: !0, duration: 5e3, fade: 0 }; var r = { wrap: { left: 0, top: 0, overflow: "hidden", margin: 0, padding: 0, height: "100%", width: "100%", zIndex: -999999 }, img: { position: "absolute", display: "none", margin: 0, padding: 0, border: "none", width: "auto", height: "auto", maxWidth: "none", zIndex: -999999} }, i = function (n, i, o) { this.options = e.extend({}, e.fn.backstretch.defaults, o || {}), this.images = e.isArray(i) ? i : [i], e.each(this.images, function () { e("<img />")[0].src = this }), this.isBody = n === document.body, this.$container = e(n), this.$wrap = e('<div class="backstretch"></div>').css(r.wrap).appendTo(this.$container), this.$root = this.isBody ? s ? e(t) : e(document) : this.$container; if (!this.isBody) { var u = this.$container.css("position"), a = this.$container.css("zIndex"); this.$container.css({ position: u === "static" ? "relative" : u, zIndex: a === "auto" ? 0 : a, background: "none" }), this.$wrap.css({ zIndex: -999998 }) } this.$wrap.css({ position: this.isBody && s ? "fixed" : "absolute" }), this.index = 0, this.show(this.index), e(t).on("resize.backstretch", e.proxy(this.resize, this)).on("orientationchange.backstretch", e.proxy(function () { this.isBody && t.pageYOffset === 0 && (t.scrollTo(0, 1), this.resize()) }, this)) }; i.prototype = { resize: function () { try { var e = { left: 0, top: 0 }, n = this.isBody ? this.$root.width() : this.$root.innerWidth(), r = n, i = this.isBody ? t.innerHeight ? t.innerHeight : this.$root.height() : this.$root.innerHeight(), s = r / this.$img.data("ratio"), o; s >= i ? (o = (s - i) / 2, this.options.centeredY && (e.top = "-" + o + "px")) : (s = i, r = s * this.$img.data("ratio"), o = (r - n) / 2, this.options.centeredX && (e.left = "-" + o + "px")), this.$wrap.css({ width: n, height: i }).find("img:not(.deleteable)").css({ width: r, height: s }).css(e) } catch (u) { } return this }, show: function (t) { if (Math.abs(t) > this.images.length - 1) return; this.index = t; var n = this, i = n.$wrap.find("img").addClass("deleteable"), s = e.Event("http://www.targaresources.com/scripts/backstretch.show", { relatedTarget: n.$container[0] }); return clearInterval(n.interval), n.$img = e("<img />").css(r.img).bind("load", function (t) { var r = this.width || e(t.target).width(), o = this.height || e(t.target).height(); e(this).data("ratio", r / o), e(this).fadeIn(n.options.speed || n.options.fade, function () { i.remove(), n.paused || n.cycle(), n.$container.trigger(s, n) }), n.resize() }).appendTo(n.$wrap), n.$img.attr("src", n.images[t]), n }, next: function () { return this.show(this.index < this.images.length - 1 ? this.index + 1 : 0) }, prev: function () { return this.show(this.index === 0 ? this.images.length - 1 : this.index - 1) }, pause: function () { return this.paused = !0, this }, resume: function () { return this.paused = !1, this.next(), this }, cycle: function () { return this.images.length > 1 && (clearInterval(this.interval), this.interval = setInterval(e.proxy(function () { this.paused || this.next() }, this), this.options.duration)), this }, destroy: function (n) { e(t).off("resize.backstretch orientationchange.backstretch"), clearInterval(this.interval), n || this.$wrap.remove(), this.$container.removeData("backstretch") } }; var s = function () { var e = navigator.userAgent, n = navigator.platform, r = e.match(/AppleWebKit\/([0-9]+)/), i = !!r && r[1], s = e.match(/Fennec\/([0-9]+)/), o = !!s && s[1], u = e.match(/Opera Mobi\/([0-9]+)/), a = !!u && u[1], f = e.match(/MSIE ([0-9]+)/), l = !!f && f[1]; return !((n.indexOf("iPhone") > -1 || n.indexOf("iPad") > -1 || n.indexOf("iPod") > -1) && i && i < 534 || t.operamini && {}.toString.call(t.operamini) === "[object OperaMini]" || u && a < 7458 || e.indexOf("Android") > -1 && i && i < 533 || o && o < 6 || "palmGetResource" in t && i && i < 534 || e.indexOf("MeeGo") > -1 && e.indexOf("NokiaBrowser/8.5.0") > -1 || l && l <= 6) } () })(jQuery, window);



$('document').ready(function () {

    var $operationsModule = $('#operations_module');
    var $operationsLink = $('#operations_link');
    var $newsModule = $('#news_module');
    var $newsLink = $('#news_link');
    var $presentationsModule = $('#presentations_module');
    var $presentationsLink = $('#presentations_link');


    $operationsModule.css({ 'left': $operationsLink.offset().left });
    $newsModule.css({ 'left': $newsLink.offset().left });
    $presentationsModule.css({ 'left': $presentationsLink.offset().left });

    if ($(window).height() <= 600) {
        $('#footer').css({ 'position': 'relative' });
        $('.footer_module').hide();
    } else {
        $('#footer').css({ 'position': 'fixed' });
        if (homeFooter) {
            $('.footer_module').show();
        }
    }

    $('.footer_links li').on('click', function (event) {
        var currentLiId = $(this).attr('id').split('_');
        var $currentModule = $("#" + currentLiId[0] + "_module");
        $currentModule.show(200);
    });

    $('.footer_module .module_close .close_button').on('click', function () {
        $(this).parents('.footer_module').hide();
    });

    //    $('.footer_module').mouseleave(function (event) {
    //            $('.footer_module').hide();
    //    });

    $('.footer_toggle').click(function () {
        $('#footer_content').hide();
        $('#footer_content_closed').show();
        $('#footer').css({ 'background': 'transparent' });
    });
    $('.footer_toggle_closed').click(function () {
        $('#footer_content_closed').hide();
        $('#footer_content').show();
        $('#footer').css({ 'background': 'transparent url\(\'../Themes/Default/Content/Images/f_bk_nav.png\'/*tpa=http://www.targaresources.com/Themes/Default/Content/Images/f_bk_nav.png*/\) 0 0 repeat' });
    });

});

$(window).resize(function () {

    var $operationsModule = $('#operations_module');
    var $operationsLink = $('#operations_link');
    var $newsModule = $('#news_module');
    var $newsLink = $('#news_link');
    var $presentationsModule = $('#presentations_module');
    var $presentationsLink = $('#presentations_link');


    $operationsModule.css({ 'left': $operationsLink.offset().left });
    $newsModule.css({ 'left': $newsLink.offset().left });
    $presentationsModule.css({ 'left': $presentationsLink.offset().left });
    if ($(window).height() <= 600) {
        $('#footer').css({ 'position': 'relative' });
        $('.footer_module').hide();
    } else {
        $('#footer').css({ 'position': 'fixed' });
        if (homeFooter) {
            $('.footer_module').show();
        }
    }
});


var Browser = {
  Version: function() {
    var version = 999; // we assume a sane browser
    if (navigator.appVersion.indexOf("MSIE") != -1)
      // bah, IE again, lets downgrade version number
      version = parseFloat(navigator.appVersion.split("MSIE")[1]);
    return version;
  }
}

if (Browser.Version() >= 9) {
// >= ie9

/*********************
//* jQuery Multi Level CSS Menu #2- By Dynamic Drive: http://www.dynamicdrive.com/
*********************/
//Specify full URL to down and right arrow images (23 is padding-right to add to top level LIs with drop downs):
var arrowimages={down:['downarrowclass', 'Unknown_83_filename'/*tpa=http://www.targaresources.com/scripts/images/down.gif*/, 23], right:['rightarrowclass', 'Unknown_83_filename'/*tpa=http://www.targaresources.com/scripts/images/right.gif*/]}
var jqueryslidemenu = {
    animateduration: { over: 200, out: 200 }, //duration of slide in/ out animation, in milliseconds
    buildmenu: function (menuid, arrowsvar) {
        jQuery(document).ready(function ($) {
            var $mainmenu = $("#" + menuid + " > ul")
            var $headers = $mainmenu.find("li");
            $headers.each(function (i) {
                var $curobj = $(this)
                var $subul = $(this).find('ul:eq(0)')
                this._dimensions = { w: this.offsetWidth, h: this.offsetHeight, subulw: $subul.outerWidth(), subulh: $subul.outerHeight() }
                this.istopheader = $curobj.parents("ul").length == 1 ? true : false
                $subul.css({ top: this.istopheader ? this._dimensions.h + "px" : 0 })
                $curobj.hover(
                function (e) {
                    if (this.istopheader) {
                        $(this).children('a:eq(0)').css('background', 'url(/Themes/Default/Content/Images/bk_nav_hover.png) 0 0 repeat-x');
                    }
                    var $targetul = $(this).children("ul:eq(0)")
                    this._offsets = { left: $(this).offset().left, top: $(this).offset().top }
                    var menuleft = this.istopheader ? 0 : this._dimensions.w
                    menuleft = (this._offsets.left + menuleft + this._dimensions.subulw > $(window).width()) ? (this.istopheader ? -this._dimensions.subulw + this._dimensions.w : -this._dimensions.w) : menuleft;

                    var targetName = $targetul.parent().find('a').html();
                    var subWidth = ""
                    if (targetName == 'Investors') {
                        subWidth = 305;
                    } else if (targetName == 'Contact') {
                        subWidth = 250;
                    } else {
                        subWidth = 185;
                    };

                    $targetul.css({ width: subWidth }).show(jqueryslidemenu.animateduration.over)
                },
                function (e) {
                    if (this.istopheader) {
                        $(this).children('a:eq(0)').css('background', 'none');
                    }
                    var $targetul = $(this).children("ul:eq(0)")
                    $targetul.hide(jqueryslidemenu.animateduration.out)
                }
            ) //end hover

            }) //end $headers.each()
            $mainmenu.find("ul").css({ display: 'none', visibility: 'visible' })
        }) //end document.ready
    }
}
//build menu with ID="myslidemenu" on page:
jqueryslidemenu.buildmenu("nav_main_container", arrowimages)


}else{
// < ie9

$('document').ready(function () {
    var $mainmenu = $("#nav_main_container > ul")
    var $headers = $mainmenu.find("li");
    $headers.each(function (i) {
        var $curobj = $(this);
        var $subul = $(this).find('ul:eq(0)');
        $subul.hide();
        if (Browser.Version() == 8) {
            $subul.css({ top: 17 });
        } else {
            $subul.css({ top: 18 });
        }



        $curobj.hover(
                    function (e) {
                        var $targetul = $(this).children("ul:eq(0)");

                        var targetName = $targetul.parent().find('a').html();
                        var subWidth = ""
                        if (targetName == 'Investors') {
                            subWidth = 305;
                        } else if (targetName == 'Contact') {
                            subWidth = 250;
                        } else {
                            subWidth = 185;
                        };

                        $targetul.css({ width: subWidth }).show(200);
                    },
                    function (e) {
                        var $targetul = $(this).children("ul:eq(0)");
                        $targetul.hide(100);
                    }
                ) //end hover            

    });

});

        }






/*********************
//* jQuery irXML JS implementation
*********************/
        $('document').ready(function () {

            if (typeof irxmlstockquote != "undefined") {
                $.each(irxmlstockquote, function (i, tickerLine) {

                    var ir_exch = tickerLine.exchange, ir_ticker = tickerLine.ticker, ir_price = "$" + tickerLine.lastprice.toFixed(2), ir_change = (Math.round(tickerLine.change * 100) / 100).toFixed(2), ir_pChange = (Math.round(tickerLine.pchange * 100) / 100).toFixed(2) + "%", $cLine = $('.stockTickerLine').eq(i), changeIcon = "";

                    if (ir_change > 0) {
                        changeIcon = "<img src='../Themes/Default/Content/Images/stockuparrow.gif'/*tpa=http://www.targaresources.com/Themes/Default/Content/Images/stockuparrow.gif*/ />"
                    } else {
                        changeIcon = "<img src='../Themes/Default/Content/Images/stockdownarrow.gif'/*tpa=http://www.targaresources.com/Themes/Default/Content/Images/stockdownarrow.gif*/ />"
                    }


                    $cLine.children('.stockTickerLineTitle').html(ir_exch + ': ' + ir_ticker);
                    $cLine.children('.stockTickerLineLastprice').html(ir_price);
                    $cLine.children('.stockTickerLineChange').html(changeIcon + ir_change);
                    $cLine.children('.stockTickerLinePchange').html(ir_pChange);

                });
            }
            if (typeof irxmlnewsreleases != "undefined") {

                    var prDate = irxmlnewsreleases[0].releasedate.getDate();
                    var monthNames = ["JAN", "FEB", "MARCH", "APRIL", "MAY", "JUNE", "JULY", "AUG", "SEP", "OCT", "NOV", "DEC"];
                    var prMonth = irxmlnewsreleases[0].releasedate.getMonth(); //Months are zero based
                    var prYear = irxmlnewsreleases[0].releasedate.getFullYear();

                    var prDateStr = monthNames[prMonth] + " " + prDate + " " + prYear
                    var prTitle = irxmlnewsreleases[0].title

                    $('.module_content_prDate').html(prDateStr);
                    $('.module_content_prTitle').html(prTitle);


            }

            if (typeof irxmlevents != "undefined") {
                    var prDate = irxmlevents[0].startdate.getDate();
                    var monthNames = ["JAN", "FEB", "MARCH", "APRIL", "MAY", "JUNE", "JULY", "AUG", "SEP", "OCT", "NOV", "DEC"];
                    var prMonth = irxmlevents[0].startdate.getMonth(); //Months are zero based
                    var prYear = irxmlevents[0].startdate.getFullYear();

                    var prDateStr = monthNames[prMonth] + " " + prDate + " " + prYear
                    var prTitle = irxmlevents[0].event

                    $('.module_content_eventDate').html(prDateStr);
                    $('.module_content_eventTitle').html(prTitle);
            }

        });



        $.fn.toggleClick = function () {
            var methods = arguments, // store the passed arguments for future reference
            count = methods.length; // cache the number of methods 

            //use return this to maintain jQuery chainability
            return this.each(function (i, item) {
                // for each element you bind to
                var index = 0; // create a local counter for that element
                $(item).click(function () { // bind a click handler to that element
                    return methods[index++ % count].apply(this, arguments); // that when called will apply the 'index'th method to that element
                    // the index % count means that we constrain our iterator between 0 and (count-1)
                });
            });
        };