var siteMaintShowMessage = false;

var siteMaintMessageText = "On Saturday, May 30 from 2 to 10 p.m., Central time, Insurance Account Values and Transactions may not be available due to system maintenance."


var siteMaintHTMLString = '<div id="sitemaint-pop" class="sitemaint-pop">';
	siteMaintHTMLString += '<div class="sitemaint-pop-header">';
		siteMaintHTMLString += '<span class="cta circle circle-5 right b-close"><span class="icon-close"></span></span>';
		siteMaintHTMLString += '<h2 class="subheading-2"><span class="icon-alert"></span> Site Maintenance</h2>';
	siteMaintHTMLString += '</div>';
	siteMaintHTMLString += '<hr/>';
	siteMaintHTMLString += '<div class="sitemaint-pop-content">';
	siteMaintHTMLString +=  siteMaintMessageText;
	siteMaintHTMLString += '</div>';
siteMaintHTMLString += '</div>';

if (siteMaintShowMessage) {
	jQuery(document).ready(function($) {
		$('.header-inner').append(siteMaintHTMLString);
	});
}