//Macromedia Functions

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function SetStatusbar(msg){
	window.status=msg;
	return true;
}


// Custom functions

function vendorPfd() {
var winleft = (screen.width - 450) / 2;
var winUp = (screen.height - 650) / 2;
Win = window.open('http://www.mgmresorts.com/company/vendor-download.aspx','vendor','toolbar=yes,scrollbars=yes,resizable=yes,status=yes,width=450,height=450,top='+winUp+',left='+winleft);
if (parseInt(navigator.appVersion) >= 4) { Win.focus(); }
}

function privacyWin() {
var winleft = (screen.width - 450) / 2;
var winUp = (screen.height - 450) / 2;
Win = window.open('http://www.mgmresorts.com/privacy.htm','privacy','toolbar=yes,scrollbars=yes,resizable=yes,status=yes,width=450,height=450,top='+winUp+',left='+winleft);
if (parseInt(navigator.appVersion) >= 4) { Win.focus(); }
}

//terms of use page
function termsWin() {
var winleft = (screen.width - 450) / 2;
var winUp = (screen.height - 450) / 2;
Win = window.open('http://www.mgmresorts.com/termsofuse.htm','privacy','toolbar=yes,scrollbars=yes,resizable=yes,status=yes,width=450,height=450,top='+winUp+',left='+winleft);
if (parseInt(navigator.appVersion) >= 4) { Win.focus(); }
}


// Open guestbook / MPE
function guestBook(email){
var winleft = (screen.width - 850) / 2;
var winUp = (screen.height - 500) / 2;
var URL = "https://secure02.mgm-mirage.com/Guestbook/Guestbook.aspx?pid=938"
if (email != undefined){
	URL += "&email=" + email
}
winProp = 'width=850,height=500,left='+winleft+',top='+winUp+',scrollbars=yes,resizable=yes,menubar=yes,toolbar=yes,status=yes';
Win = window.open(URL,'guestbook',winProp);
if (parseInt(navigator.appVersion) >= 4) { Win.focus(); }
}


function roomWin(){
var winleft = (screen.width - 800) / 2;
var winUp = (screen.height - 500) / 2;
var URL = "https://reservations.mgmmirage.com/bookingengine.aspx?pid=938&ad=10/1/2009"
winProp = 'width=800,height=500,left='+winleft+',top='+winUp+',scrollbars=yes,resizable=yes,menubar=yes,toolbar=yes,status=yes';
Win = window.open(URL,'guestbook',winProp);
if (parseInt(navigator.appVersion) >= 4) { Win.focus(); }
}


// Book a Room
function datePickWin(property,ad,nights) { 
//if (theURL == '') { theURL = 'https://reservations.mgmmirage.com/bookingengine.aspx?pid=938'; }
var theURL = 'https://reservations.mgmmirage.com/bookingengine.aspx';
var pid;

switch(property)
{
case 'ARIA':
  pid = '930';
  break;
case 'Bellagio':
  pid = '190';
  break;
case 'Vdara':
  pid = '938';
  break;
case 'MGM Grand':
  pid = '001';
  break;
case 'SKYLOFTS':
  pid = '002';
  break;
case 'The Signature':
  pid = '005';
  break;
case 'Mandalay Bay':
  pid = 'MBH';
  break;
case 'THEhotel':
  pid = '276';
  break;
case 'Mirage':
  pid = '160';
  break;
case 'Monte Carlo':
  pid = 'MCH';
  break;
case 'New York-New York':
  pid = '010';
  break;  
case 'Luxor':
  pid = 'LUX';
  break;
case 'Excalibur':
  pid = 'EXC';
  break;
case 'Circus Circus':
  pid = 'CCC';
  break;
case 'Railroad Pass':
  pid = 'RRP';
  break;case 'Beau Rivage':
  pid = '180';
  break;
case 'MGM Grand Detroit':
  pid = '016';
  break;
case 'Circus Circus Reno':
  pid = 'CCR';
  break;
case 'Gold Strike Tunica':
  pid = 'GST';
  break;
case 'Gold Strike Jean':
  pid = 'GSJ';
  break;  
case 'MGM Macau':
  pid = 'Macau'
  break;
case 'MGM Grand Sanya':
  pid = 'Sanya'
  break;
case 'Delano Las Vegas':
  pid = '277'
  break;
default:
  theURL="https://reservations.mgmmirage.com/pageNotFound.aspx";
  pid = '';
}

if (pid!=''){ theURL = theURL + "?pid=" + pid;}
if (ad!=''){ theURL = theURL + "&ad=" + ad;}
if (nights!=''){theURL = theURL + "&nights=" + nights;}
if (pid=='Macau'){ theURL = "http://www.mgmmacau.com/room_booking"}
if (pid=='Sanya'){ theURL = "http://www.mgmgrandsanya.com/reservation/"}
var winleft = (screen.width - 795) / 2;
var winUp = (screen.height - 460) / 2;
winProp = 'width=795,height=460,left='+winleft+',top='+winUp+',scrollbars=yes,resizable=yes,menubar=yes,toolbar=yes,status=yes';
Win = window.open(theURL,'resWin',winProp);
if (parseInt(navigator.appVersion) >= 4) { Win.focus(); }
}

// Center Dinner Window
function DinnerWin(Restaurant,Hotel) { 
var winleft = (screen.width - 750) / 2;
var winUp = (screen.height - 525) / 2;
winProp = 'width=780,height=525,left='+winleft+',top='+winUp+',resizable=yes,menubar=no,toolbar=yes,status=yes,scrollbars=yes';
window.open('https://reservations.mgmmirage.com/bookingengine.aspx?pid='+Hotel+'&host=dining&code='+Restaurant,'dinner',winProp);
}

// -- Nights

function showProperties(element,hide){
document.getElementById(element).style.display="block";
//document.getElementById(element).onmouseout.display="none";
}

function hide(element,hide){
document.getElementById(element).style.display="none";
}


// Puts focus on textbox
function enterDir(objEvent, flag){
    if(keyCheck(objEvent) == 13){
        //alert("Enter")
        switch(flag)
        {
            case "guestbook":
	            guestBook(document.mpeForm.email_signup.value);
	            break;            
        }
        //document.mpeForm.submit();
    }
    else
    {
        return false;
    }
}

// Air/Hotel Packages
function AirHotel() {
var winleft = (screen.width - 795) / 2;
var winUp = (screen.height - 460) / 2;
Win = window.open('http://www.mgmresorts.com/mmvTemp/','reg','toolbar=yes,scrollbars=yes,resizable=yes,status=yes,width=795,height=460,top='+winUp+',left='+winleft);
if (parseInt(navigator.appVersion) >= 4) { Win.focus(); }
}

function popWin(theURL, winName, popW, popH) { 
var winleft = (screen.width - popW) / 2;
var winUp = (screen.height - popH) / 2;
winProp = 'width='+popW+',height='+popH+',left='+winleft+',top='+winUp+',resizable=yes,status=yes,scrollbars=yes';
Win = window.open(theURL,winName,winProp);
if (parseInt(navigator.appVersion) >= 4) { Win.focus(); }
}


// ---- Book Gui ----
// -- Calendar
window.addEvent('domready', function() { 
	myCal5 = new Calendar({ date1: 'n/j/Y'}, { classes: ['bookgui'], direction: 1, tweak: {x: 3, y: -3} });
});

// -- Nights
function changeNumberNights(intNights, field, show, hide){

	var myTextBox;
	myTextBox = document.getElementById(field);
	myTextBox.value = intNights;
	document.getElementById(hide).style.display = "none";
	if (show != ''){document.getElementById(show).style.display= "block";};
	
}

function showNights(element,hide){
document.getElementById(element).style.display="block";
//if (hide != ''){document.getElementById(hide).style.display="none";};
}
//


	
	






