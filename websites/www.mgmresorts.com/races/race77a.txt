<script id = "race77a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race77={};
	myVars.races.race77.varName="Window[26].qm_la";
	myVars.races.race77.varType="@varType@";
	myVars.races.race77.repairType = "@RepairType";
	myVars.races.race77.event1={};
	myVars.races.race77.event2={};
	myVars.races.race77.event1.id = "Lu_Id_a_79";
	myVars.races.race77.event1.type = "onmouseover";
	myVars.races.race77.event1.loc = "Lu_Id_a_79_LOC";
	myVars.races.race77.event1.isRead = "False";
	myVars.races.race77.event1.eventType = "@event1EventType@";
	myVars.races.race77.event2.id = "Lu_Id_a_80";
	myVars.races.race77.event2.type = "onmouseover";
	myVars.races.race77.event2.loc = "Lu_Id_a_80_LOC";
	myVars.races.race77.event2.isRead = "True";
	myVars.races.race77.event2.eventType = "@event2EventType@";
	myVars.races.race77.event1.executed= false;// true to disable, false to enable
	myVars.races.race77.event2.executed= false;// true to disable, false to enable
</script>

