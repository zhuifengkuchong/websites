<script id = "race2b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race2={};
	myVars.races.race2.varName="date1__onfocus";
	myVars.races.race2.varType="@varType@";
	myVars.races.race2.repairType = "@RepairType";
	myVars.races.race2.event1={};
	myVars.races.race2.event2={};
	myVars.races.race2.event1.id = "date1";
	myVars.races.race2.event1.type = "onfocus";
	myVars.races.race2.event1.loc = "date1_LOC";
	myVars.races.race2.event1.isRead = "True";
	myVars.races.race2.event1.eventType = "@event1EventType@";
	myVars.races.race2.event2.id = "Lu_DOM";
	myVars.races.race2.event2.type = "onDOMContentLoaded";
	myVars.races.race2.event2.loc = "Lu_DOM_LOC";
	myVars.races.race2.event2.isRead = "False";
	myVars.races.race2.event2.eventType = "@event2EventType@";
	myVars.races.race2.event1.executed= false;// true to disable, false to enable
	myVars.races.race2.event2.executed= false;// true to disable, false to enable
</script>

