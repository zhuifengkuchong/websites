<script id = "race82a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race82={};
	myVars.races.race82.varName="Window[26].qm_la";
	myVars.races.race82.varType="@varType@";
	myVars.races.race82.repairType = "@RepairType";
	myVars.races.race82.event1={};
	myVars.races.race82.event2={};
	myVars.races.race82.event1.id = "Lu_Id_a_83";
	myVars.races.race82.event1.type = "onmouseover";
	myVars.races.race82.event1.loc = "Lu_Id_a_83_LOC";
	myVars.races.race82.event1.isRead = "False";
	myVars.races.race82.event1.eventType = "@event1EventType@";
	myVars.races.race82.event2.id = "Lu_Id_a_84";
	myVars.races.race82.event2.type = "onmouseover";
	myVars.races.race82.event2.loc = "Lu_Id_a_84_LOC";
	myVars.races.race82.event2.isRead = "True";
	myVars.races.race82.event2.eventType = "@event2EventType@";
	myVars.races.race82.event1.executed= false;// true to disable, false to enable
	myVars.races.race82.event2.executed= false;// true to disable, false to enable
</script>

