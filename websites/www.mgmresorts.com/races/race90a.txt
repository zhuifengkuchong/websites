<script id = "race90a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race90={};
	myVars.races.race90.varName="Window[26].qm_la";
	myVars.races.race90.varType="@varType@";
	myVars.races.race90.repairType = "@RepairType";
	myVars.races.race90.event1={};
	myVars.races.race90.event2={};
	myVars.races.race90.event1.id = "Lu_Id_a_91";
	myVars.races.race90.event1.type = "onmouseover";
	myVars.races.race90.event1.loc = "Lu_Id_a_91_LOC";
	myVars.races.race90.event1.isRead = "False";
	myVars.races.race90.event1.eventType = "@event1EventType@";
	myVars.races.race90.event2.id = "Lu_Id_a_92";
	myVars.races.race90.event2.type = "onmouseover";
	myVars.races.race90.event2.loc = "Lu_Id_a_92_LOC";
	myVars.races.race90.event2.isRead = "True";
	myVars.races.race90.event2.eventType = "@event2EventType@";
	myVars.races.race90.event1.executed= false;// true to disable, false to enable
	myVars.races.race90.event2.executed= false;// true to disable, false to enable
</script>

