<script id = "race69a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race69={};
	myVars.races.race69.varName="Window[26].qm_la";
	myVars.races.race69.varType="@varType@";
	myVars.races.race69.repairType = "@RepairType";
	myVars.races.race69.event1={};
	myVars.races.race69.event2={};
	myVars.races.race69.event1.id = "Lu_Id_a_71";
	myVars.races.race69.event1.type = "onmouseover";
	myVars.races.race69.event1.loc = "Lu_Id_a_71_LOC";
	myVars.races.race69.event1.isRead = "False";
	myVars.races.race69.event1.eventType = "@event1EventType@";
	myVars.races.race69.event2.id = "Lu_Id_a_72";
	myVars.races.race69.event2.type = "onmouseover";
	myVars.races.race69.event2.loc = "Lu_Id_a_72_LOC";
	myVars.races.race69.event2.isRead = "True";
	myVars.races.race69.event2.eventType = "@event2EventType@";
	myVars.races.race69.event1.executed= false;// true to disable, false to enable
	myVars.races.race69.event2.executed= false;// true to disable, false to enable
</script>

