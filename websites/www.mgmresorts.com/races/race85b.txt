<script id = "race85b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race85={};
	myVars.races.race85.varName="Window[26].qm_la";
	myVars.races.race85.varType="@varType@";
	myVars.races.race85.repairType = "@RepairType";
	myVars.races.race85.event1={};
	myVars.races.race85.event2={};
	myVars.races.race85.event1.id = "Lu_Id_a_87";
	myVars.races.race85.event1.type = "onmouseover";
	myVars.races.race85.event1.loc = "Lu_Id_a_87_LOC";
	myVars.races.race85.event1.isRead = "True";
	myVars.races.race85.event1.eventType = "@event1EventType@";
	myVars.races.race85.event2.id = "Lu_Id_a_86";
	myVars.races.race85.event2.type = "onmouseover";
	myVars.races.race85.event2.loc = "Lu_Id_a_86_LOC";
	myVars.races.race85.event2.isRead = "False";
	myVars.races.race85.event2.eventType = "@event2EventType@";
	myVars.races.race85.event1.executed= false;// true to disable, false to enable
	myVars.races.race85.event2.executed= false;// true to disable, false to enable
</script>

