<script id = "race23a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race23={};
	myVars.races.race23.varName="Lu_DOM__onmouseover";
	myVars.races.race23.varType="@varType@";
	myVars.races.race23.repairType = "@RepairType";
	myVars.races.race23.event1={};
	myVars.races.race23.event2={};
	myVars.races.race23.event1.id = "Lu_Id_script_10";
	myVars.races.race23.event1.type = "Lu_Id_script_10__parsed";
	myVars.races.race23.event1.loc = "Lu_Id_script_10_LOC";
	myVars.races.race23.event1.isRead = "False";
	myVars.races.race23.event1.eventType = "@event1EventType@";
	myVars.races.race23.event2.id = "Lu_Id_a_21";
	myVars.races.race23.event2.type = "onmouseover";
	myVars.races.race23.event2.loc = "Lu_Id_a_21_LOC";
	myVars.races.race23.event2.isRead = "True";
	myVars.races.race23.event2.eventType = "@event2EventType@";
	myVars.races.race23.event1.executed= false;// true to disable, false to enable
	myVars.races.race23.event2.executed= false;// true to disable, false to enable
</script>

