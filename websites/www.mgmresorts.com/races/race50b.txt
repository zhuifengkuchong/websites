<script id = "race50b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race50={};
	myVars.races.race50.varName="Window[26].qm_la";
	myVars.races.race50.varType="@varType@";
	myVars.races.race50.repairType = "@RepairType";
	myVars.races.race50.event1={};
	myVars.races.race50.event2={};
	myVars.races.race50.event1.id = "Lu_Id_a_17";
	myVars.races.race50.event1.type = "onmouseover";
	myVars.races.race50.event1.loc = "Lu_Id_a_17_LOC";
	myVars.races.race50.event1.isRead = "True";
	myVars.races.race50.event1.eventType = "@event1EventType@";
	myVars.races.race50.event2.id = "Lu_Id_a_16";
	myVars.races.race50.event2.type = "onmouseover";
	myVars.races.race50.event2.loc = "Lu_Id_a_16_LOC";
	myVars.races.race50.event2.isRead = "False";
	myVars.races.race50.event2.eventType = "@event2EventType@";
	myVars.races.race50.event1.executed= false;// true to disable, false to enable
	myVars.races.race50.event2.executed= false;// true to disable, false to enable
</script>

