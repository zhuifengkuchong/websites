<script id = "race104b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race104={};
	myVars.races.race104.varName="Window[26].qm_la";
	myVars.races.race104.varType="@varType@";
	myVars.races.race104.repairType = "@RepairType";
	myVars.races.race104.event1={};
	myVars.races.race104.event2={};
	myVars.races.race104.event1.id = "Lu_Id_a_104";
	myVars.races.race104.event1.type = "onmouseover";
	myVars.races.race104.event1.loc = "Lu_Id_a_104_LOC";
	myVars.races.race104.event1.isRead = "True";
	myVars.races.race104.event1.eventType = "@event1EventType@";
	myVars.races.race104.event2.id = "Lu_Id_a_103";
	myVars.races.race104.event2.type = "onmouseover";
	myVars.races.race104.event2.loc = "Lu_Id_a_103_LOC";
	myVars.races.race104.event2.isRead = "False";
	myVars.races.race104.event2.eventType = "@event2EventType@";
	myVars.races.race104.event1.executed= false;// true to disable, false to enable
	myVars.races.race104.event2.executed= false;// true to disable, false to enable
</script>

