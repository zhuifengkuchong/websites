<script id = "race15b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race15={};
	myVars.races.race15.varName="Object[646].uuid";
	myVars.races.race15.varType="@varType@";
	myVars.races.race15.repairType = "@RepairType";
	myVars.races.race15.event1={};
	myVars.races.race15.event2={};
	myVars.races.race15.event1.id = "search_data";
	myVars.races.race15.event1.type = "onkeypress";
	myVars.races.race15.event1.loc = "search_data_LOC";
	myVars.races.race15.event1.isRead = "True";
	myVars.races.race15.event1.eventType = "@event1EventType@";
	myVars.races.race15.event2.id = "Lu_Id_button_1";
	myVars.races.race15.event2.type = "onkeypress";
	myVars.races.race15.event2.loc = "Lu_Id_button_1_LOC";
	myVars.races.race15.event2.isRead = "False";
	myVars.races.race15.event2.eventType = "@event2EventType@";
	myVars.races.race15.event1.executed= false;// true to disable, false to enable
	myVars.races.race15.event2.executed= false;// true to disable, false to enable
</script>

