/*global requirejs, require*/

requirejs.config({
    baseUrl: '/_assets/js/',

	paths: {
		// application specific
	    'jquery': 'vendor/jquery',
        'jquery-ui': 'vendor/jquery-ui',
		//'slider' : 'vendor/slider',
		'drags'  : 'vendor/drags',

		// dependencies
		'hammer' : 'vendor/hammer',
		'browserdetect' : 'http://www.level3.com/_assets/js/vendor/browserdetect.min',
		'slider' : 'http://www.level3.com/_assets/js/slider.min',
		'global' : 'http://www.level3.com/_assets/js/global.min',
		'cycle'  : 'http://www.level3.com/_assets/js/vendor/jquery.cycle2.min',
		'carousel'  : 'vendor/jquery.cycle2.carousel',
		'jwplayer': 'vendor/jwplayer',
		'datatables': 'http://www.level3.com/_assets/js/vendor/jquery.dataTables.min',
		'collab_table': 'http://www.level3.com/_assets/js/collab_table.min',
        'colorbox': 'vendor/jquery.colorbox-min',
        'form': 'http://www.level3.com/_assets/js/form.min',
        'hidden_fields': 'hiddenFields',
        'partner_finder': 'partner_finder',
		

		// backbone (if needed)
		'lodash' : 'vendor/lodash',
		'backbone' : '/_assets/js/vendor/backbone',

		// IE specific
		'respond' : 'vendor/respond',
		'selectivizr' : 'vendor/selectivizr',
		'pie' : 'vendor/ie',
		'msie' : 'http://www.level3.com/_assets/js/ie.min',
		'modernizr' : 'vendor/modernizr',

		// pages
		'home' : 'http://www.level3.com/_assets/js/home.min',
		'product': 'http://www.level3.com/_assets/js/product.min',
		'product-landing': 'http://www.level3.com/_assets/js/product-landing.min',
		'scroll_navigation' : 'http://www.level3.com/_assets/js/scroll_navigation.min',
		'generic' : 'http://www.level3.com/_assets/js/generic.min',
		'goals' : 'http://www.level3.com/_assets/js/goals.min',
		'goals_detail' : 'http://www.level3.com/_assets/js/goals_detail.min',
		'resources' : 'http://www.level3.com/_assets/js/resources.min',
		'mapped_reporting' : 'mapped_reporting',
		'management' : 'http://www.level3.com/_assets/js/management.min',
		'find_participants' : 'http://www.level3.com/_assets/js/find_participants.min',
		'data_entry' : 'http://www.level3.com/_assets/js/data_entry.min',
		'resources_modal' : 'http://www.level3.com/_assets/js/resources_modal.min',
		'tabs' : 'tabs',
		'resource_tray': 'http://www.level3.com/_assets/js/resource_tray.min',
		'search_results': 'http://www.level3.com/_assets/js/search_results.min'
	},
	shim: {
		// application specific
		/*
		slider: {
			deps: ['jquery', 'hammer', 'browserdetect']
		},
		*/
		// IE specific
		respond: {
			deps: ['jquery']
		},
		selectivizr: {
			deps: ['jquery', 'respond']
		},
		pie: {
			deps: ['jquery']
		},
		datatables: {
		    deps: ['global']
		},
		msie: {
			deps: ['jquery', 'selectivizr', 'ie']
		},
		
		// pages
		home: {
			deps: ['jquery', 'global', 'carousel', 'cycle', 'slider', 'jwplayer', 'form', 'hidden_fields']
		},
		product: {
			deps: ['jquery', 'global', 'filter', 'carousel', 'cycle', 'slider', 'jwplayer']
		},
		scroll_navigation: {
			deps: ['jquery', 'global', 'filter', 'carousel', 'cycle', 'slider']
		},
		generic: {
			deps: ['jquery', 'global', 'filter', 'carousel', 'cycle', 'slider', 'jwplayer']
		},
		solution: {
			deps: ['jquery', 'global', 'filter', 'carousel', 'cycle', 'slider', 'jwplayer']
		},
		solutions: {
			deps: ['jquery', 'global']
		},
		microsite: {
			deps: ['jquery', 'jquery-ui', 'global', 'filter', 'tabs', 'jwplayer']
		},
		global_reach: {
			deps: ['jquery', 'global']
		},
		goals: {
			deps: ['jquery']
		},
		resources: {
			deps: ['jquery', 'global']
		},
		mapped_reporting: {
			deps: ['jquery']
		},
		management: {
			deps: ['jquery', 'global']
		},
		find_participants: {
			deps: ['jquery']
		},
		jwplayer : {
			exports: 'jwplayer'
		},
		modernizr : {
			exports: 'Modernizr'
		},
		cycle: {
			deps: ['jquery']
		},
		carousel: {
			deps: ['cycle']
		},
		global: {
			deps: [
				'jquery',
				'config',
				'jwplayer',
				'cycle',
				'carousel',
				'slider',
				'filter',
				'modernizr',
                'colorbox',
				'tabs']
		},
		contact: {
		    deps: ['jquery', 'global', 'form', 'hidden_fields', 'tabs']
		}
	}
});

var sliders = {},
	videos = [],
	search_active = false,
	goals = [];
	window.is_init = false;
	

require(['jquery', 'browserdetect'], function($, browserdetect) {
	"use strict";

	/////////     GLOBAL FUNCTIONS     //////////

	switch($('body').attr('class').split(' ')[0]) {
		case 'home' :
			require(['home']);
		break;
	
		case 'product' :
			require(['product']);
		break;
	
		case 'product-landing' :
			require(['scroll_navigation', 'product_landing']);
		break;

	    case 'partner-finder':
	        require(['partner_finder']);
	        break;

		case 'collab' :
			require(['global','scroll_navigation','datatables','collab_table']);
		break;
		case 'generic' :
			require(['generic']);
		break;
	
		case 'solution' :
			require(['solution']);
		break;
	
		case 'solutions' :
			require(['solutions']);
		break;

		case 'goals' :
			require(['goals']);
		break;

		case 'goals_detail' :
			require(['goals_detail']);
		break;

		case 'resources' :
			require(['resources','resource_tray']);
		break;

		case 'contact' :
			require(['contact']);
		break;
		case 'microsite' :
		    require(['microsite','partner_finder']);
		break;

		case 'global-reach' :
			require(['global_reach']);
		break;

		case 'mapped_reporting' :
			require(['data_slider']);
		break;
		case 'management' :
			require(['management']);
		break;

		case 'find_participants' :
			require(['find_participants']);
		break;

		case 'data_entry-1' :
			//require(['data_entry']);
			require(['data_slider']);
		break;

		case 'data_entry-2' :
			//require(['data_entry']);
			require(['data_slider']);
		break;

		case 'data_entry-3' :
			//require(['data_entry']);
			require(['data_slider']);
		break;

		case 'search-results' :
		    require(['search_results', 'resources', 'resource_tray']);
		break;
	}

	if ($('body').attr('class') !== 'home') {
		require(['resources_modal']);
	}

	function init_objects () {}

	function on_resize () {}

	function release_hover(e) {
		if (search_active === true) {
			if (e.target !== $('li.search').eq(0)) {
				$('body').click();
				search_active = false;
			}
		} else {
			if (e.target !== $('li.search').eq(0)) {
				search_active = true;
			}
		}
	}

	function init_listeners () {
		$(window).resize(on_resize);
		$('body').bind('touchend', release_hover);
	}

	function init_ie () {
		if (browserdetect.browser === 'Explorer' && browserdetect.version === 8) {
			require(['msie'], function () {
				$('body').css({
					'display': 'block',
					'opacity': 0
				}).animate({
					'opacity': 1
				});
				init_objects();
				init_listeners();
				on_resize();
			});

		} else {
			$('body').css({
				'display': 'block',
				'opacity': 0
			}).animate({
				'opacity': 1
			});
			init_objects();
			init_listeners();
			on_resize();
		}
	}

	init_ie();
});

define('config', function() {
  return {
    'jwplayer_key': 'mZjlSdZESNW6m5qtbT7yAI4j1zmJuTUCFF+0pKp0Pd0=',
	'jwplayer_defaults' : {
		image: null,
		flashplayer: 'Unknown_83_filename'/*tpa=http://www.level3.com/_assets/flash/jwplayer.flash.swf*/,
		html5player: 'vendor/jwplayer.html5.js'/*tpa=http://www.level3.com/_assets/js/vendor/jwplayer.html5.js*/,
		skin: null
	}
  };
});

// helper function for console logging
window.log = function () {
	log.history = log.history || [];
	log.history.push(arguments);
	if ('console' in self && 'log' in console) {
		console.log(Array.prototype.slice.call(arguments));
	}
};

window.arrayRemove = function(ary, from, to) {
  var rest = ary.slice((to || from) + 1 || ary.length);
  ary.length = from < 0 ? ary.length + from : from;
  return ary.push.apply(ary, rest);
};

// polling to reduce the number of calls for rapidly firing events
window.poll = (function(){
	var timer = 0;
	return function(callback, ms){
		clearTimeout(timer);
		timer = setTimeout(callback, ms);
	};
	
})();