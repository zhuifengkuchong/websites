//********************************************************************
//*-------------------------------------------------------------------
//* Licensed Materials - Property of IBM
//*
//* WebSphere Commerce
//*
//* (c) Copyright International Business Machines Corporation. 2003
//*     All rights reserved.
//*
//* US Government Users Restricted Rights - Use, duplication or
//* disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
//*
//*-------------------------------------------------------------------
//*

//////////////////////////////////////////////////////////
// Checks whether a string contains a double byte character
// target = the string to be checked
//
// Return true if target contains a double byte char; false otherwise
//////////////////////////////////////////////////////////
function containsDoubleByte (target) {
     var str = new String(target);
     var oneByteMax = 0x007F;

     for (var i=0; i < str.length; i++){
        chr = str.charCodeAt(i);
        if (chr > oneByteMax) {return true;}
     }
     return false;
}

//////////////////////////////////////////////////////////
// A simple function to validate an email address
// It does not allow double byte characters
// strEmail = the email address string to be validated
//
// Return true if the email address is valid; false otherwise
//////////////////////////////////////////////////////////
function isValidEmail(strEmail){
	// check if email contains dbcs chars
	if (containsDoubleByte(strEmail)){
		return false;
	}
	
	if(strEmail.length == 0) {
		return true;
	} else if (strEmail.length < 5) {
             return false;
       	}else{
           	if (strEmail.indexOf(" ") > 0){
                      	return false;
               	}else{
                  	if (strEmail.indexOf("@") < 1) {
                            	return false;
                     	}else{
                           	if (strEmail.lastIndexOf(".") < (strEmail.indexOf("@") + 2)){
                                     	return false;
                                }else{
                                        if (strEmail.lastIndexOf(".") >= strEmail.length-2){
                                        	return false;
                                        }
                              	}
                       	}
              	}
       	}
      	return true;
}



//////////////////////////////////////////////////////////
// This function will count the number of bytes
// represented in a UTF-8 string
//
// arg1 = the UTF-16 string
// arg2 = the maximum number of bytes allowed in your input field
// Return false is this input string is larger then arg2
// Otherwise return true...
//////////////////////////////////////////////////////////
function isValidUTF8length(UTF16String, maxlength) {
    if (utf8StringByteLength(UTF16String) > maxlength) return false;
    else return true;
}

//////////////////////////////////////////////////////////
// This function will count the number of bytes
// represented in a UTF-8 string
//
// arg1 = the UTF-16 string you want a byte count of...
// Return the integer number of bytes represented in a UTF-8 string
//////////////////////////////////////////////////////////
function utf8StringByteLength(UTF16String) {
  if (UTF16String === null) return 0;
  var str = String(UTF16String);
  var oneByteMax = 0x007F;
  var twoByteMax = 0x07FF;
  var byteSize = str.length;

  for (i = 0; i < str.length; i++) {
    chr = str.charCodeAt(i);
    if (chr > oneByteMax) byteSize = byteSize + 1;
    if (chr > twoByteMax) byteSize = byteSize + 1;
  }  
  return byteSize;
}

// clear field value function: removes the default value onfocus, and adds back if nothing entered 
function fieldClear(obj) {
	if(obj.Val) {
		if (obj.value == '') { 
			obj.value = obj.Val;
			obj.Val = null;
			obj.first = null;
		} 
		else {
			obj.Val = null;
		}
	} else if (!obj.first) { 
		obj.Val = obj.value;
		obj.value = ''; 
		obj.first = 'true';
	} 
}

function trimMe(field) {
	var value = field.value ;
	value = value.replace(/^\s+|\s+$/g,"");
	field.value = value;
}

// Formatting the phonenumber xxx-xxx-xxxx
function formatPhone( field ){
	var v = field.value ;
	if( (v.length < 10) || (v.length > 11) ) return( field.value );
	else{
		//if( v.length == 11 ) v = v.substr(1);
		v = v.substr(0,3) + "-" + v.substr(3,3) + "-" + v.substr(6,4);
	}
	field.value = v;
}

// function to convert the field to lower case
function convertToLowerCase(field){
	var value = field.value;
	value = value.toLowerCase();
	field.value = value;
}

/*
// duplicate methods in Commerce.js
var busy = false;
function Add2ShopCart(form){ 		
    alert("Unknown_83_filename"/*tpa=http://www.tractorsupply.com/wcsstore/CVB2BDirectStorefrontAssetStore/javascript/Add2ShopCart - Util.js*/);
  		if (!busy) {
              busy = true;
              form.action="OrderItemAdd";
              //form.catEntryId.value = catEntryId;
              //form.quantity.value = catEntryQuantity;
              form.URL.value='OrderCalculate?updatePrices=1&calculationUsageId=-1&dummaryparam=1&URL=OrderItemDisplay';
              form.submit();
       }
}
// This javascript function is used by the 'Add to Wish List' button to set appropriate values before the form is submitted
// Quantity is hard-coded to value = 1, since adding to wish list does not require a valid quantity.
function Add2WishList(form) {
    alert("Unknown_83_filename"/*tpa=http://www.tractorsupply.com/wcsstore/CVB2BDirectStorefrontAssetStore/javascript/Add2WishList - Util.js*/);
	var inputsArr = form.getElementsByTagName("input");
	var i, inputID;

	for(i in inputsArr)
	{
		inputID = inputsArr[i].id;
		if ( inputID != null && inputID.indexOf("quantity") != -1 ) { inputsArr[i].value = 1; }
	}
	
       if (!busy) {
              busy = true;
              form.action="InterestItemAdd";
             // form.catEntryId.value = catEntryId;
              form.URL.value='InterestItemDisplay';
              form.submit();
       }
}
*/

// This javascript display the value of the 'keyName' stored in 'cookieName'. If value is null then display "0".
// This script is used to display Items for cookie CVMINICART & balancePoints for CVREWRDPOINTS cookies
function displayIntegerValue(cookieName, keyName)
{
	//alert("IN displayCartItems method");
	var str = "0";
	str = getUserCookieValue(cookieName, keyName);
	if(str == null)
		str = "0";
	document.write(str);
}

// This javascript display the value of the 'keyName' stored in 'cookieName'. If value is null then display "http://www.tractorsupply.com/wcsstore/CVB2BDirectStorefrontAssetStore/javascript/0.00". 
// If value is integer then it appends the .00 after the integer value.
// This script is used to display Items amount total for cookie CVMINICART & balanceAmount for CVREWRDPOINTS cookies
function displayAmountValue(cookieName, keyName)
{
	//alert("IN displayCartItems method");
	var str = "http://www.tractorsupply.com/wcsstore/CVB2BDirectStorefrontAssetStore/javascript/0.00";
	str = getUserCookieValue(cookieName, keyName);
	if (str != null)
	{
		var iDotIndex = str.indexOf(".");
		if (str.length - iDotIndex > 3)
			str = str.substring(0, iDotIndex + 3);
		else if (iDotIndex != -1 && str.length - iDotIndex == 2)
			str = str + "0";
		else if (iDotIndex == -1)
			str = str + ".00";
	}
	else
	{
		str = "http://www.tractorsupply.com/wcsstore/CVB2BDirectStorefrontAssetStore/javascript/0.00";
	}
	
	document.write(str);
}

function getUserCookieValue(cookieName, keyName) {
 //alert("in getUserCookieValue");
 var cookieValue = getCookie(cookieName);
 //alert("cookieValue="+cookieValue);
  if ( cookieValue == null )
 	return null;
 var  nameDelimiter = "@" ;
 var pairDelimiter = "~~~" ;
 var matchPattern = keyName + pairDelimiter + '(.*?)(' + nameDelimiter +')';	 	
 //alert("match partern = "+matchPattern);
 var results = cookieValue.match ( matchPattern);
 //alert("resutls="+results);
  if ( results )
    return ( unescape ( results[1] ) );
  else
    return null;
}

function getCookie ( cookieName ) {
  var results = document.cookie.match ( cookieName + '=(.*?)(;|$)' );
  if ( results )
    return ( unescape ( results[1] ) );
  else
    return null;
}

// Checks whether inputStr has a valid value.
function isEmpty(inputStr) {
	if (inputStr == null) {
		return true;
	} else {    
		return !inputStr.match(/[^\s]/);
	}
}

// Utility method to list the event listeners on a given object.
//
// Example: $('#applyGCBtn').listHandlers('click', console.info);
//
// The above example will output to the firebug console if any listeners exist.
$.fn.listHandlers = function(events, outputFunction) {
    return this.each(function(i){
        var elem = this,
            dEvents = $(this).data('events');
        if (!dEvents) {return;}
        $.each(dEvents, function(name, handler){
            if((new RegExp('^(' + (events === '*' ? '.+' : events.replace(',','|').replace(/^on/i,'')) + ')$' ,'i')).test(name)) {
               $.each(handler, function(i,handler){
                   outputFunction(elem, '\n' + i + ': [' + name + '] : ' + handler );
               });
           }
        });
    });
};

/*! http://mths.be/placeholder v2.0.7 by @mathias */
;(function(f,h,$){var a='placeholder' in h.createElement('input'),d='placeholder' in h.createElement('textarea'),i=$.fn,c=$.valHooks,k,j;if(a&&d){j=i.placeholder=function(){return this};j.input=j.textarea=true}else{j=i.placeholder=function(){var l=this;l.filter((a?'textarea':':input')+'[placeholder]').not('.placeholder').bind({'focus.placeholder':b,'blur.placeholder':e}).data('placeholder-enabled',true).trigger('blur.placeholder');return l};j.input=a;j.textarea=d;k={get:function(m){var l=$(m);return l.data('placeholder-enabled')&&l.hasClass('placeholder')?'':m.value},set:function(m,n){var l=$(m);if(!l.data('placeholder-enabled')){return m.value=n}if(n==''){m.value=n;if(m!=h.activeElement){e.call(m)}}else{if(l.hasClass('placeholder')){b.call(m,true,n)||(m.value=n)}else{m.value=n}}return l}};a||(c.input=k);d||(c.textarea=k);$(function(){$(h).delegate('form','submit.placeholder',function(){var l=$('.placeholder',this).each(b);setTimeout(function(){l.each(e)},10)})});$(f).bind('beforeunload.placeholder',function(){$('.placeholder').each(function(){this.value=''})})}function g(m){var l={},n=/^jQuery\d+$/;$.each(m.attributes,function(p,o){if(o.specified&&!n.test(o.name)){l[o.name]=o.value}});return l}function b(m,n){var l=this,o=$(l);if(l.value==o.attr('placeholder')&&o.hasClass('placeholder')){if(o.data('placeholder-password')){o=o.hide().next().show().attr('id',o.removeAttr('id').data('placeholder-id'));if(m===true){return o[0].value=n}o.focus()}else{l.value='';o.removeClass('placeholder');l==h.activeElement&&l.select()}}}function e(){var q,l=this,p=$(l),m=p,o=this.id;if(l.value==''){if(l.type=='password'){if(!p.data('placeholder-textinput')){try{q=p.clone().attr({type:'text'})}catch(n){q=$('<input>').attr($.extend(g(this),{type:'text'}))}q.removeAttr('name').data({'placeholder-password':true,'placeholder-id':o}).bind('focus.placeholder',b);p.data({'placeholder-textinput':q,'placeholder-id':o}).before(q)}p=p.removeAttr('id').hide().prev().attr('id',o).show()}p.addClass('placeholder');p[0].value=p.attr('placeholder')}else{p.removeClass('placeholder')}}}(this,document,jQuery));
$().ready(function(){
	/* place holder fix */
	$('dd[rel]').each(function(){
	    $('input', this).attr('placeholder', $(this).attr('rel'));
	});
	$('input, textarea').placeholder();
});