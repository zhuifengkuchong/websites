//-----------------------------------------------------------------
// Licensed Materials - Property of IBM
//
// WebSphere Commerce
//
// (C) Copyright IBM Corp. 2010 All Rights Reserved.
//
// US Government Users Restricted Rights - Use, duplication or
// disclosure restricted by GSA ADP Schedule Contract with
// IBM Corp.
//-----------------------------------------------------------------
//---------------------------------------------------------------------
//  The sample contained herein is provided to you "AS IS".
// 
//  It is furnished by IBM as a simple example and has not been thoroughly
//  tested under all conditions.  IBM, therefore, cannot guarantee its
//  reliability, serviceability or functionality.
// 
//  This sample may include the names of individuals, companies, brands
//  and products in order to illustrate concepts as completely as possible.
//  All of these names are fictitious and any similarity to the names and
//  addresses used by actual persons or business enterprises is entirely
//  coincidental.
//--------------------------------------------------------------------- 

/** 
 * @fileOverview This file provides the render context used to display
 * the recommendations from Coremetrics Intelligent Offer. It also
 * contains the zone population function called with the results to
 * display.
 */
 /*
dojo.require("wc.render.common");
dojo.require("wc.render.RefreshController");
dojo.require("wc.render.Context");
dojo.require("wc.widget.RefreshArea");
*/
/** 
 * @class The IntelligentOfferJS class defines the render context used to display
 * the recommendations from Coremetrics Intelligent Offer.
 */
/*IntelligentOfferJS = {*/
	/* The common parameters used in service calls. */
	/*	langId: "-1",
		storeId: "",
		catalogId: "",
	*/
	/**
	 * This function initializes common parameters used in all service calls.
	 * @param {int} langId The language id to use.
	 * @param {int} storeId The store id to use.
	 * @param {int} catalogId The catalog id to use.
	 */
		/*setCommonParameters:function(langId,storeId,catalogId){
			this.langId = langId;
			this.storeId = storeId;
			this.catalogId = catalogId;
		},
		*/
	/**
	 *  This function declares a context and a refresh area controller that has the given context ID and controller ID.
	 *	@param {string} controllerId The ID of the controller that is going to be declared.
	 *	@param {string} contextId The ID of the context that is going to be declared.
	 */
		/*
	declareRefreshController: function(controllerId, contextId){
		 
		wc.render.declareContext(contextId,{partNumbers: "", zoneId: "", espotTitle: ""},"")
		 
		//console.debug("entering IntelligentOfferJS.declareRefreshController with contextId = " + contextId + " and controller id = " + controllerId);
		if(wc.render.getRefreshControllerById(controllerId)){
			//console.debug("controller with id = " + controllerId + " already exists. No declaration will be done");
			return;
		}
		wc.render.declareRefreshController({
			id: controllerId, 
			renderContext: wc.render.getContextById(contextId),
			url: "",
	    formId: ""
	    */
     /** 
      * Refreshes the Intelligent Offer Recommendations e-Marketing Spot area.
      * This function is called when a render context changed event is detected. 
      * @param {string} message The model changed event message
      * @param {object} widget The registered refresh area
      */
	    	/*
	   ,modelChangedHandler: function(message, widget) {
          var renderContext = this.renderContext;
			}
		*/
     /** 
      * Refreshes the Intelligent Offer Recommendations e-Marketing Spot area.
      * This function is called when a render context changed event is detected. 
      * @param {string} message The model changed event message
      * @param {object} widget The registered refresh area
      */
		/*
     ,renderContextChangedHandler: function(message, widget) {
          var renderContext = this.renderContext;

          widget.refresh(renderContext.properties);
			}
    */
     /** 
      * Post handling for the Intelligent Offer Recommendations e-Marketing Spot area.
      * This function is called after a successful refresh. 
      * @param {object} widget The registered refresh area
      */ 
     /*
     ,postRefreshHandler: function(widget) {
	    		var renderContext = this.renderContext;	   
	    		cursor_clear();
	    		// need to process cm_cr tags
	    		cX("onload");
     	}
		});
	}
};	
*/
var displayZoneId ='';
var modalIOZoneId ='';
var modalZoneCall = false;
var ioCallBusy = false;
var respData;
/**
 *  This function is the zone population function called by Coremetrics Intelligent Offer.
 *  It creates a comma separated list of the partnumbers to display, and then updates
 *  the refresh area that will display the recommendations.
 */
function io_rec_zp(a_product_ids,zone,symbolic,target_id,category,rec_attributes,target_attributes,target_header_txt) {
    /*
	console.warn('in io_rec_zp update');
    console.warn('zone ' + zone);
    console.warn('symbolic ' + symbolic);
    console.warn('target_id ' + target_id);
    console.warn('category ' + category);
    console.warn('rec_attributes ' + rec_attributes);
    console.warn('target_attributes ' + target_attributes);
    console.warn('target_header_txt ' + target_header_txt);
    */
    if (symbolic !== '_NR_') {
        var n_recs = a_product_ids.length;
        var rec_part_numbers;
        for (var ii=0; ii < n_recs; ii++) {
            if (ii == 0) {
                rec_part_numbers = a_product_ids[ii];
            } else {
                rec_part_numbers = rec_part_numbers + ',' + a_product_ids[ii];
            }
            //console.warn('product: ' + a_product_ids[ii]);
        }
        //console.warn('partNumbers: ' + rec_part_numbers);
        //alert('partNumbers: ' + rec_part_numbers);
        //Make Ajax call to AjaxIntelligentOfferDisplayView
        var zoneId = zone.replace(/\s+/g,'');
        //for testing purpose only-- REMOVE after UNIT TESTING
        displayZoneId = zoneId;
        if (modalZoneCall) {
        	displayIOProducts(modalIOZoneId, rec_part_numbers, target_header_txt);
        	modalZoneCall = false;
        } else {
        	displayIOProducts(zoneId, rec_part_numbers, target_header_txt);
        }
        

    }
}

function invokeCMRecRequest(zoneId, targetPartNumber, targetCategoryId, targetRandomize, targetSearchTerm) {
	//cmRecRequest ('<c:out value="${marketingSpotData.uniqueID}" />','<c:out value="${param.partNumber}"/>','<c:out value="${param.parentCategoryId}"/>','<c:out value="${param.randomize}"/>','<c:out value="${param.searchTerm}"/>');
	cmRecRequest(zoneId, targetPartNumber, targetCategoryId, targetRandomize, targetSearchTerm);
	cmDisplayRecs();
}

function displayIOProducts(zoneId, partNumbers, eSpotTitle) {
    // alert('populate ESpot Content-Ajax Call');
    //alert('zoneId' + zoneId);
    var objFormId = zoneId+'_form';
    //alert(objFormId);

    $("#"+objFormId+" :input[name='partNumbers']").val(partNumbers);
    $("#"+objFormId+" :input[name='eSpotTitle']").val(eSpotTitle);
    
    //alert($("#"+objFormId+" :input[name='partNumbers']").val());
    //alert($("#"+objFormId+" :input[name='eSpotTitle']").val());
    
    if (!ioCallBusy) {
    	ioCallBusy = true;
        var ajaxESOptions = {
                success:  showIOESData,  // post-submit callback
                error:    showIOESError,
                url:      'AjaxIntelligentOfferDisplayView',
                type:     'POST',
                async:    true,
                cache:    false
            };
        $("#"+objFormId).ajaxSubmit(ajaxESOptions);
        //$.ajax(ajaxESOptions); 
        ioCallBusy = false;
        //alert('after success Ajax Call - zoneId is ' + zoneId );
        //populateContent(zoneId);
    }    
}	

function showIOESData(resp, status)  {
  if ("success" == status) {
        //alert('SVN IE success');
        //alert(resp);
        respData = resp;
        //alert('displayZoneId-' + displayZoneId);
        populateContent(displayZoneId);
        
  }
}
/*
function displayIOProducts(zoneId, partNumbers, eSpotTitle) {
    // alert('populate ESpot Content-Ajax Call');
    var ajaxESOptions = {
            success:  showIOESData,  // post-submit callback
            error:    showIOESError,
            url:      '${WC_IntelligentOfferESpot_url_variable}',
            type:     'POST',
            data:      "partNumbers="+partNumbers+"&eSpotTitle="+eSpotTitle,
            async:    false,
            timeout:  30000,
            cache:    false
        };

    $.ajax(ajaxESOptions);       
}*/


function showIOESError(responseText, statusText)  { 
	//console.warn( "ES encountered error responseText " + responseText + " statusText " + statusText );
}

function populateContent(zoneId) {
    //alert('populating response data');
    
    var objFormId = zoneId+'_form';
    //alert(objFormId);

    var emsName = $("#"+objFormId+" :input[name='emsName']").val();
    var enableCarousel = $("#"+objFormId+" :input[name='enableCarousel']").val();
    var displayMode = $("#"+objFormId+" :input[name='mode']").val();  
    //alert('emsName' + emsName);
    //alert('enableCarousel' + enableCarousel);
    //alert('displayMode' + displayMode);
    
    if (emsName == 'HomePageIntelligentOffer') {
    	//alert(respData);
    	$("#homepage-IO-spot").empty().html(respData);
		  $('.slider2').bxSlider({
		    slideWidth: 199,
		    minSlides: 5,
		    maxSlides: 5,
		    slideMargin: 0,
		    pager: false,
		    controls: enableCarousel,
		    mode: displayMode,
		    infiniteLoop:false,
		    hideControlOnEnd:false,
		    adaptiveHeight:false
		  });
		  $('.slide').css('height','400px');
		  $('.slide').css('border-right','1px dashed #DACFC6');
		  $('.slide').css('margin-right','1px');
    } else if (emsName == 'CategoryPageIntelligentOffer') {
    	$("#category-IO-spot").empty().html(respData);
        $('.slider2').bxSlider({
          slideWidth: 189,
          minSlides: 4,
          maxSlides: 4,
          slideMargin: 0,
          pager: false,
          controls: enableCarousel,
          mode: displayMode,
          infiniteLoop:false,
          hideControlOnEnd:false,
          adaptiveHeight:false
        });
        $('.slide').css('height',' 	');
        $('.slide').css('border-right','1px dashed #DACFC6');	
    	$('.slide').css('margin-right','1px');		    
    } else if (emsName == 'ProductPageIntelligentOffer' || emsName == 'InStoreProductPageIntelligentOffer') {
    	$("#product-IO-spot").empty().html(respData);
		$('.slider2').bxSlider({
		  slideWidth: 175,
		  minSlides: 3,
		  maxSlides: 3,
		  slideMargin: 0,
		  pager: false,
		  controls: enableCarousel,
		  mode: displayMode,
		  infiniteLoop:false,
		  hideControlOnEnd:false,
		  adaptiveHeight:true
		});

    } else if (emsName == 'ShoppingCartPageIntelligentOffer') {
        $("#cart-IO-spot").empty().html(respData);
        $('.slider2').bxSlider({
          slideWidth: 199,
          minSlides: 5,
          maxSlides: 5,
          slideMargin: 0,
          pager: false,
          controls: enableCarousel,
          mode: displayMode,
          infiniteLoop:false,
          hideControlOnEnd:false,
          adaptiveHeight:false
        });
        $('.slide').css('height','400px');
        $('.slide').css('border-right','1px dashed #DACFC6');
        $('.slide').css('margin-right','1px');
    } else if (emsName == 'QuickShopIntelligentOffer') {
        $("#quickshopmodal-IO-spot").empty().html(respData);
        $("div#quickshopmodal-IO-spot.slide").css('float','left');
    }		    
}  		

function getVirtualCategoryFromURL() {
	var queryString = window.location.search;
   	var queryParams = queryString.split("?")[1];
   	if (queryParams != undefined) {
   	   	queryParams = queryParams.split("&");
	   	for (var i = 0; i < queryParams.length; i++) {
	   	    var paramArr = queryParams[i].split("=");
	   	    var name = paramArr[0];
	   	    var value = paramArr[1];
	   	    if (name == "cm_vc") {
	   	   	    return value;
	   	    }
	   	}
   	}	
}

function getVirtualCategoryFromResponse(response, defaultCategoryId) {
	var virtualCategory = response.cm_vc;
    if (virtualCategory == undefined) {
        //categoryId = '${tscProductVB.parentCategoryIds[0]}';
    	virtualCategory = defaultCategoryId;
    }
    return virtualCategory;
}



function quickShopModel(form){
    //$("#quickShop-modal_"+productId).modal();
    $.modal.close();
    var ajaxQSOptions = {
            success:  showQSData,  // post-submit callback
            error:    showQSError,
            url:      'QuickShopModalView',
            type:     'POST',
            async:    false,
            timeout:  30000,
            cache:    false
        };

        $(form).ajaxSubmit(ajaxQSOptions);         
}

function showQSData(resp, status)  {
    
    if ("success" == status) {
        //alert('SVN IE success');
        //alert(resp);
        $('#quickShop-modal').empty().html(resp);
        $('#quickShop-modal').modal();
        
        
        $('#simplemodal-container').fadeIn("slow");
        $('#simplemodal-overlay').fadeIn("slow");
        //Added part of ECP-6906 to use AJAX Inv call for Quick Shop
        checkOnlineInventory(productID);
        displayReset();
        resetSelections();
        initializePriceAndAttributeElements();
                   
           		
  }
}

function showQSError(responseText, statusText)  { 
    alert ( "QS encountered error responseText " + responseText + " statusText " + statusText );
  }
// Added part of ECP-6906
function checkOnlineInventory(productId){
    var url1 = "CheckOnlineStockView?productId="+productId;
    var ajaxCIOptions = {
            success:  showCIData,  // post-submit callback
            error:    showCIError,
            url:      url1,
            type:     'POST',
            async:    false,
            timeout:  30000,
            cache:    false
        };
    $.ajax(ajaxCIOptions); 
    // return false to prevent normal browser submit and page navigation 
    return false;   
}
function showCIData(resp, status)  {
    if ("success" == status) {
        resp = resp.replace("/*", "");
        resp = resp.replace("*/", "");
        resp = jQuery.trim(resp);         
        var obj = eval("(" + resp + ")");
        var itemsInventory = obj.itemsInventory;
        for (var i=0; i < itemsInventory.length; i++) {
            updateSKUInventoryvalues(itemsInventory[i].itemId, itemsInventory[i].inventory);
        }
        updateProductDynamicElementsFromDefiningAttributes();
    }
}
function showCIError(responseText, statusText)  { 
 //   alert ( "Check Inventory encountered error responseText " + responseText + " statusText " + statusText );
  }

//ECP-7107 used to update mapp product value at the time of adding item to cart
function updateMappPr(form){
    if(window != undefined ){
        var sppzaMap = window['sppzaMap_' + $(form).find('input[name="productId"]').val()];
        if(sppzaMap != undefined ){
           var mappPr = sppzaMap["MappProduct"];
           if(mappPr != undefined) {
        	   $(form).find('input[name="mappPr"]').val(mappPr);
           }
        }
    }
}