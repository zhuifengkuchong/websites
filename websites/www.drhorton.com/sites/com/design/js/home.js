$(document).ready(function () {

    //jQuery.preLoadImages("../images/billboard1.jpg", "../images/billboard2.jpg", "../images/billboard3.jpg", "../images/billboard4.jpg");

    $('#slider1').bxSlider({
        preloadImages: 'visible',
        mode: 'fade',
        auto: true,
        autoHover: true,
        captions: true,
        pause: g_HomeSlideInterval,
        touchEnabled: false
    });

    $('#slider2').bxSlider({
        minSlides: 4,
        maxSlides: 4,
        slideWidth: 180,
        slideMargin: 48,
        touchEnabled: false
    });

    // Feature requirement (SSD 12.03.15) need to hide the prev and next if less then 5
    if ($('#featurecount').html() < 5) {
        $('#homeSlider .bx-wrapper .bx-prev').hide();
        $('#homeSlider .bx-wrapper .bx-next').hide();
    } else {
        $('#homeSlider .bx-wrapper .bx-prev').show();
        $('#homeSlider .bx-wrapper .bx-next').show();
    }


});
