$(document).ready(function () {
    //jQuery.preLoadImages("Unknown_83_filename"/*tpa=http://www.drhorton.com/sites/com/design/js/images/billboard1.jpg*/, "Unknown_83_filename"/*tpa=http://www.drhorton.com/sites/com/design/js/images/billboard2.jpg*/, "Unknown_83_filename"/*tpa=http://www.drhorton.com/sites/com/design/js/images/billboard3.jpg*/, "Unknown_83_filename"/*tpa=http://www.drhorton.com/sites/com/design/js/images/billboard4.jpg*/,
    //"Unknown_83_filename"/*tpa=http://www.drhorton.com/sites/com/design/js/images/subboard1.jpg*/, "Unknown_83_filename"/*tpa=http://www.drhorton.com/sites/com/design/js/images/subboard2.jpg*/, "Unknown_83_filename"/*tpa=http://www.drhorton.com/sites/com/design/js/images/subboard3.jpg*/, "Unknown_83_filename"/*tpa=http://www.drhorton.com/sites/com/design/js/images/floor_plan_diagram_md.jpg*/,
    //"Unknown_83_filename"/*tpa=http://www.drhorton.com/sites/com/design/js/images/floor_plan_diagram.jpg*/, "Unknown_83_filename"/*tpa=http://www.drhorton.com/sites/com/design/js/images/floor_plan_diagram2.jpg*/, "Unknown_83_filename"/*tpa=http://www.drhorton.com/sites/com/design/js/images/floor_plan_diagram3.jpg*/,
    //"Unknown_83_filename"/*tpa=http://www.drhorton.com/sites/com/design/js/images/floor_plan_diagram_sm1.jpg*/, "Unknown_83_filename"/*tpa=http://www.drhorton.com/sites/com/design/js/images/floor_plan_diagram_sm2.jpg*/, "Unknown_83_filename"/*tpa=http://www.drhorton.com/sites/com/design/js/images/floor_plan_diagram_sm3.jpg*/);

    isIpad = navigator.userAgent.match(/iPad/i) != null;

    if (isIpad) {
        $(".view").fancybox({
            'type': 'iframe',
            'autoScale': false,
            'scrolling': 'no',
            'width': 740,
            'height': 550

        });
        $(".modal").fancybox({
            'type': 'iframe',
            'autoScale': false,
            'scrolling': 'no',
            'width': 840,
            //'height'			: '90%',
            'height': 780
        });
    }
    //else {
    //    $(".view").fancybox({
    //        'type': 'iframe',
    //        'autoScale': false,
    //        'width': "70%",
    //        'height': "99%",
    //        'scrolling': 'yes'
    //    });
    //}

    //$(".viewmap").fancybox({
    //    'type': 'iframe',
    //    'autoScale': false,
    //    'scrolling': 'no',
    //    'width': 820,
    //    'height': 480
    //});
    /*
	$(".view").fancybox({
		'type'				: 'iframe',
		'autoScale'			: false,
		//'width'				: "70%",
		'width'				: 880,
		//'height'			: "99%",
		'height'			: 550,
		'scrolling'			: 'yes'
	});
	
	$(".modal").fancybox({
		'type'				: 'iframe',
		'autoScale'			: false,
		'width'				: 740,
		//'height'			: '90%',
		'height'			: 780,
		'scrolling'			: 'yes'
	});
	*/
    $(".wherewebuild").fancybox({
        'type': 'iframe',
        'width': 800,
        //'width'				: '75%',
        'height': 450,
        //'height'			: '75%',
        'autoScale': false,
        'transitionIn': 'none',
        'transitionOut': 'none'
    });

    $(".map").fancybox({
        'type': 'iframe',
        'width': 740,
        //'width'				: '75%',
        'height': 575,
        //'height'			: '75%',
        'autoScale': false,
        'transitionIn': 'none',
        'transitionOut': 'none'
    });

    $("#tabs").tabs();
    // fix the classes
    $(".tabs-bottom .ui-tabs-nav, .tabs-bottom .ui-tabs-nav > *")
	.removeClass("ui-corner-all ui-corner-top")
	.addClass("ui-corner-bottom");
    // move the nav to the bottom
    $(".tabs-bottom .ui-tabs-nav").appendTo(".tabs-bottom");

    $("#alpha").tabs();

    $("#tabsTop").tabs({ show: { effect: "fade", duration: 800 } });

    $('#subSlider').bxSlider({
        preloadImages: 'all',
        pager: false,
        mode: 'fade',
        auto: true,
        autoHover: true,
        captions: false,
        pause: window.g_SlideInterval,
        touchEnabled: false
    });

    $('#modalSlider').bxSlider({
        preloadImages: 'all',
        mode: 'fade',
        captions: true,
        pagerCustom: '#bx-pager',
        touchEnabled: false
    });

    $('#plansSlider').bxSlider({
        minSlides: 3,
        maxSlides: 3,
        slideWidth: 194,
        slideMargin: 15,
        pager: false,
        touchEnabled: false
    });

    $('#quickSlider').bxSlider({
        minSlides: 3,
        maxSlides: 3,
        slideWidth: 194,
        slideMargin: 15,
        pager: false,
        touchEnabled: false
    });
    var exteriorSlider = jQuery('#exteriorSlider').bxSlider({
        preloadImages: 'all',
        pager: false,
        mode: 'fade',
        auto: true,
        autoHover: true,
        captions: false,
        pause: window.g_SlideInterval,
        touchEnabled: false
    });

    var photosSlider = jQuery('#photosSlider').bxSlider({
        preloadImages: 'all',
        pager: false,
        mode: 'fade',
        auto: true,
        autoHover: true,
        captions: false,
        pause: window.g_SlideInterval,
        touchEnabled: false
    });

    var amenitiesSlider = jQuery('#amenitiesSlider').bxSlider({
        preloadImages: 'all',
        pager: false,
        mode: 'fade',
        auto: true,
        autoHover: true,
        captions: false,
        pause: window.g_SlideInterval,
        touchEnabled: false
    });

    var areaInfoSlider = jQuery('#areaInfoSlider').bxSlider({
        preloadImages: 'all',
        pager: false,
        mode: 'fade',
        auto: true,
        autoHover: true,
        captions: false,
        pause: window.g_SlideInterval,
        touchEnabled: false
    });

    var customSlider = jQuery('#customSlider').bxSlider({
        preloadImages: 'all',
        pager: false,
        mode: 'fade',
        auto: true,
        autoHover: true,
        captions: false,
        pause: window.g_SlideInterval,
        touchEnabled: false
    });

    var customSliderArray = [];
    var customSliderFields = jQuery('ul[id^="customSlider"]');
    for (var customSliderField = 0; customSliderField < customSliderFields.length; customSliderField++) {

        var tempCustomSlider = jQuery('#' + customSliderFields[customSliderField].id).bxSlider({
            preloadImages: 'all',
            pager: false,
            mode: 'fade',
            auto: true,
            autoHover: true,
            captions: false,
            pause: window.g_SlideInterval,
            touchEnabled: false
        });
        customSliderArray.push(tempCustomSlider);

    }

    var featuresSlider = jQuery('#featuresSlider').bxSlider({
        preloadImages: 'all',
        pager: false,
        mode: 'fade',
        auto: true,
        autoHover: true,
        captions: false,
        pause: window.g_SlideInterval,
        touchEnabled: false
    });
    var videoSlider = jQuery('#videoSlider').bxSlider({
        preloadImages: 'all',
        pager: false,
        mode: 'fade',
        auto: true,
        autoHover: true,
        captions: false,
        pause: window.g_SlideInterval,
        touchEnabled: false
    });

    var floorPlanSlider = jQuery('#floorPlanSlider').bxSlider({
        preloadImages: 'all',
        pager: false,
        mode: 'fade',
        auto: true,
        autoHover: true,
        captions: false,
        pause: window.g_SlideInterval,
        touchEnabled: false
    });
    var schoolSlider = jQuery('#schoolSlider').bxSlider({
        preloadImages: 'all',
        pager: false,
        mode: 'fade',
        auto: true,
        autoHover: true,
        captions: false,
        pause: window.g_SlideInterval,
        touchEnabled: false
    });
    var overviewSlider = jQuery('#overviewSlider').bxSlider({

        preloadImages: 'all',
        pager: false,
        mode: 'fade',
        auto: true,
        autoHover: true,
        captions: false,
        pause: window.g_SlideInterval,
        touchEnabled: false
    });
    jQuery('.reload-slider').click(function (e) {
        e.preventDefault();

        if (typeof overviewSlider != 'undefined') {
            overviewSlider.reloadSlider();
        }

        if (typeof schoolSlider != 'undefined') {
            schoolSlider.reloadSlider();
        }

        if (typeof floorPlanSlider != 'undefined') {
            floorPlanSlider.reloadSlider();
        }

        if (typeof videoSlider != 'undefined') {
            videoSlider.reloadSlider();
        }

        if (typeof featuresSlider != 'undefined') {
            featuresSlider.reloadSlider();
        }

        for (var customSliderArrayCount = 0; customSliderArrayCount < customSliderArray.length; customSliderArrayCount++) {
            var customSliderCurrentItem = customSliderArray[customSliderArrayCount];

            if (typeof customSliderCurrentItem != 'undefined' && customSliderCurrentItem != null) {
                customSliderCurrentItem.reloadSlider();
            }
        }

        if (typeof customSlider != 'undefined') {
            customSlider.reloadSlider();
        }

        if (typeof areaInfoSlider != 'undefined') {
            areaInfoSlider.reloadSlider();
        }

        if (typeof amenitiesSlider != 'undefined') {
            amenitiesSlider.reloadSlider();
        }

        if (typeof photosSlider != 'undefined') {
            photosSlider.reloadSlider();
        }
        if (typeof exteriorSlider != 'undefined') {
            exteriorSlider.reloadSlider();
        }

    });

    $('#sideSlider').bxSlider({
        preloadImages: 'all',
        pager: false,
        mode: 'fade',
        auto: false,
        autoHover: true,
        captions: false,
        pause: window.g_SlideInterval,
        touchEnabled: false
    });

    filterpromotions();

    /*** QMI Functions ***/
    $("#qmiTabs").tabs();
    //$('.quickMove').collapser({
    //    target: 'siblings',
    //    targetOnly: 'div',
    //    effect: 'slide',
    //    changeText: 0
    //}, function () {
    //    $('.qmiPanel').slideUp("fast");
    //    return false;
    //}, function beforeCallback() {
    //    qmiSlide1.reloadSlider();
    //    qmiSlide2.reloadSlider();
    //    qmiSlide3.reloadSlider();
    //    qmiSlide4.reloadSlider();
    //});
    /*
	var qmiSlide1 = $('.sliderQMI').bxSlider({
        minSlides: 3,
        maxSlides: 3,
        slideWidth: 194,
        slideMargin: 15,
		pager: false,
        touchEnabled: false
    });
	*/
    //var qmiSlide1 = $('#qmiSlider1').bxSlider({
    //    minSlides: 3,
    //    maxSlides: 3,
    //    slideWidth: 194,
    //    slideMargin: 15,
    //    pager: false,
    //    touchEnabled: false
    //});
    //var qmiSlide2 = $('#qmiSlider2').bxSlider({
    //    minSlides: 3,
    //    maxSlides: 3,
    //    slideWidth: 194,
    //    slideMargin: 15,
    //    pager: false,
    //    touchEnabled: false
    //});
    //var qmiSlide3 = $('#qmiSlider3').bxSlider({
    //    minSlides: 3,
    //    maxSlides: 3,
    //    slideWidth: 194,
    //    slideMargin: 15,
    //    pager: false,
    //    touchEnabled: false
    //});
    //var qmiSlide4 = $('#qmiSlider4').bxSlider({
    //    minSlides: 3,
    //    maxSlides: 3,
    //    slideWidth: 194,
    //    slideMargin: 15,
    //    pager: false,
    //    touchEnabled: false
    //});

    /*
	$('.reload').click(function(k){
	  k.preventDefault();
	  qmiSlide1.reloadSlider();
	  qmiSlide2.reloadSlider();
	  qmiSlide3.reloadSlider();
	  qmiSlide4.reloadSlider();
	});
	*/
    $('.collapse').click(function () {
        $('.qmiPanel').slideUp("fast");
        return false;
    });
    /*** END QMI Functions ***/

    $("#button").button();
    $(".resetBtn").button();
    $(".goBtn").button();

    $("input[type=radio]").picker();

    $("#price-range").slider({
        range: true,
        min: 0,
        max: 500,
        values: [100, 300],
        slide: function (event, ui) {
            $("#amount").val("$" + ui.values[0] + "k" + " - $" + ui.values[1] + "k");
        }
    });
    $("#amount").val("$" + $("#price-range").slider("values", 0) + "k" + " - $" + $("#price-range").slider("values", 1) + "k"
);

    $("#square-feet").slider({
        range: true,
        min: 0,
        max: 10000,
        values: [1500, 3500],
        slide: function (event, ui) {
            $("#feet").val(ui.values[0] + " - " + ui.values[1]);
        }
    });
    $("#feet").val($("#square-feet").slider("values", 0) + " - " + $("#square-feet").slider("values", 1)
);


    $('#tweetScroll').jScrollPane();


    $('#tweetScroll').jScrollPane(
		{ autoReinitialise: true }
	);

    $(".selectMenu").selectBoxIt({ theme: "jqueryui" }).data("selectBoxIt");



    /***** RFI Dialog *****/
    //var $loading = $('<img src="Unknown_83_filename"/*tpa=http://www.drhorton.com/sites/com/design/js/images/loader.gif*/ alt="loading">');
    /*
	$('#request').each(function() {
		var $dialog = $('<div></div>')
			//.append($loading.clone());
		var $link = $(this).one('click', function() {
			$dialog
				.load($link.attr('href'))
				.dialog({
					//title: $link.attr('title'),
					width:442,
					height:432,
					modal: true,
					position: "right",
					dialogClass: "rfiDialog"
				});
			$link.click(function() {
				$dialog.dialog('open');
				return false;
			});
			return false;
		});
	});
	*/
    loadnationalpromotionbanners();
    updateStateLinkHashes();
});

function loadnationalpromotionbanners() {
    try {
        jQuery.ajax({
            url: '/sites/com/navigation/HeaderPromotionsBanner.aspx?showallflag=' + window._showAllNationalPromotionBanners,
            type: "GET",
            dataType: "HTML",
            success: function (data) {
                try {
                    // this needs to come before the .empty call
                    var pauseDuration = getPromotionHeaderPauseDuration(data);

                    jQuery('#nationalPromotionsBannerSlider').empty();
                    jQuery('#nationalPromotionsBannerSlider').append(data);

                    $('#nationalPromoBannerSlider').bxSlider({
                        preloadImages: 'all',
                        pager: false,
                        mode: 'fade',
                        auto: (jQuery("#nationalPromotionsBannerSlider ul li").length > 1) ? true : false,
                        autoHover: true,
                        captions: false,
                        pause: pauseDuration,
                        touchEnabled: false,
                        controls: false
                    });
                } catch (j) {
                    console.error(j);
                } 
               

            }
        });
    } catch (e) {
        console.WriteLine(e);
    } 
   
}


var _defaultPromotionSlider;
function filterpromotions() {
    try {
        var hash = '';
        var windowHashKeyValuePairs = getHashKeyValuePairs(location.hash);

        if (window._defaultPromotionSlider == undefined || window._defaultPromotionSlider == null) {
            window._defaultPromotionSlider = document.getElementById('promosideSlider').outerHTML;
        }

        document.getElementById('calloutPromoSlider').innerHTML = window._defaultPromotionSlider;
        $('.promotionsHeader').show();
        document.getElementById('calloutPromoSlider').style.display = '';
        if (windowHashKeyValuePairs.length > 0) {

            for (var k = 0; k < windowHashKeyValuePairs.length; k++) {
                var keyValue = windowHashKeyValuePairs[k];

                if (hash.toLowerCase().indexOf(keyValue.Name) == -1) {

                    var facet = keyValue.Name.substring(keyValue.Name.indexOf('_') + 1);
                    facet = facet.substring(facet.indexOf('_') + 1);

                    var promotionContainer = document.getElementById('calloutPromoSlider').getElementsByTagName('ul')[0];

                    var promoSlides = promotionContainer.getElementsByTagName('li');

                    var slidesToRemove = new Array();

                    for (var i = 0; i < promoSlides.length; i++) {
                        var promoSlide = promoSlides[i];
                        var promoData = $(promoSlide).find('.callout');
                        var attribute = $(promoData).attr('drh-' + facet);
                        if (attribute != undefined && attribute != null) {
                            var hashValues = keyValue.Value.toLowerCase().split('|');
                            var attributeValues = attribute.toLowerCase().split('|');
                            for (var h = 0; h < hashValues.length; h++) {
                                var filter = true;
                                var hashValue = hashValues[h];
                                if (hashValue.length > 0) {
                                    for (var a = 0; a < attributeValues.length; a++) {
                                        var attributeValue = attributeValues[a];
                                        if (attributeValue.length > 0) {
                                            if (hashValue == attributeValue) {
                                                filter = false;
                                                break;
                                            }
                                        } else {
                                            filter = false;
                                        }
                                    }
                                    if (filter) {
                                        slidesToRemove.push(promoSlide);
                                    }
                                }
                            }
                        }
                    }

                    for (var e = 0; e < slidesToRemove.length; e++) {
                        promotionContainer.removeChild(slidesToRemove[e]);
                    }


                    if (windowHashKeyValuePairs.length > 1) {
                        hash += '&';
                    }
                    hash += windowHashKeyValuePairs[k].Name.toLowerCase();
                    if (windowHashKeyValuePairs[k].Value.length > 0) {
                        hash += '=' + windowHashKeyValuePairs[k].Value.toLowerCase();
                    }
                }
            }

            promoSlides = promotionContainer.getElementsByTagName('li');
            if (promoSlides.length > 0) {
                loadpromotions();
            } else {
                $('.promotionsHeader').hide();
                document.getElementById('calloutPromoSlider').style.display = 'none';
            }
        } else {
            loadpromotions();

        }
    } catch (e) {
        console.log(e);
    }

}

function loadpromotions() {

    $('#promosideSlider').bxSlider({
        preloadImages: 'all',
        pager: ($("#calloutPromoSlider ul li").length > 1) ? true : false,
        mode: 'fade',
        auto: ($("#calloutPromoSlider ul li").length > 1) ? true : false,
        autoHover: true,
        captions: false,
        pause: getPromotionCalloutPauseDuration(),
        touchEnabled: false,
        controls: false
    });
}

function OpenLink(url, name) {
    name = name.replace(/ /g, ""); //remove spaces from window name
    name = name.replace(/-/g, ""); //remove hypens from window name
    window.open(url, name, "left=30,top=30,width=600,height=575,toolbar=0,resizable=yes");
}
function OpenLinkWindow(url, name, extra) {

    name = name.replace(/ /g, ""); //remove spaces from window name
    name = name.replace(/-/g, ""); //remove hypens from window name
    window.open(url, name, extra);
}

// Promotion
function getPromotionCalloutPauseDuration() {
    var valueField = $('#promotionCalloutPauseDuration');
    var duration;
    if (valueField.length > 0) {
        duration = valueField.val();
    } else {
        duration = window.g_SlideInterval;
    }
    return tryParseInt(duration, window.g_SlideInterval);
}

function getPromotionHeaderPauseDuration(bannerHtml) {

    duration = window.g_SlideInterval;

    if (bannerHtml != null) {

        var valueField = ($(document.createElement("div")).append(bannerHtml)).find('#promotionHeaderPauseDuration')
        var duration;
        if (valueField.length > 0) {
            duration = valueField.val();
        } else {
            duration = window.g_SlideInterval;
        }
    }

    return tryParseInt(duration, window.g_SlideInterval);
}

function tryParseInt(str, defaultValue) { var retValue = defaultValue; if (str != null) { if (str.length > 0) { if (!isNaN(str)) { retValue = parseInt(str); } } } return retValue; }
//-----------------------------------------------------------------------------------------------------------
// Search related functions below
//-----------------------------------------------------------------------------------------------------------

function loadCommunityList(newurl) {
    var callbackUrl = location.hash.replace('#', '');
    if (newurl != undefined) {
        callbackUrl = newurl.replace('#', '');
    }

    var sortSelect = window.GetSortOptionCookieValue();
    if (sortSelect != undefined && sortSelect != null) {
        callbackUrl += '&st=' + sortSelect;
    }

    jQuery.ajax({
        url: '?' + callbackUrl + '&ajaxdata=true',
        type: "GET",
        dataType: "HTML",
        success: function (data) {
            jQuery('#communiltyList').empty();
            jQuery('#communiltyList').append(data);
            updateStateLinkHashes();
        },
    });
}

function loadpushpinmap() {
    jQuery.ajax({
        url: '?' + location.hash.replace('#', '') + '&pushpinmap=true',
        type: "GET",
        dataType: "HTML",
        success: function (data) {
            jQuery('#mapouter').empty();
            jQuery('#mapouter').append(data);

        },
    });
}

function loadnoresultsfound() {
    //jQuery.ajax({
    //    url: '?' + location.hash.replace('#', '') + '&nosearchresultscheck=true',
    //    type: "GET",
    //    dataType: "HTML",
    //    success: function (data) {
    //        jQuery('#noresultsfound').empty();
    //        jQuery('#noresultsfound').append(data);

    //    },
    //});
}

function loadfacets(newurl) {
    var callbackUrl = location.hash.replace('#', '');
    if (newurl != undefined) {
        callbackUrl = newurl.replace('#', '');
    }
    jQuery.ajax({
        url: '?' + callbackUrl + '&searchfacets=true',
        type: "GET",
        dataType: "HTML",
        success: function (data) {
            jQuery('#narrowSearch').empty();
            jQuery('#narrowSearch').append(data);
        },
    });
}


function updateStateLinkHashes() {
    jQuery('html').find('a.statelink').each(function () {
        var searching = window.IsSearchInHash();

        var hash = '';

        var windowHashKeyValuePairs = getHashKeyValuePairs(location.hash);
        var thisHashKeyValuePairs = getHashKeyValuePairs(this.hash);
        var newHashKeyValuePairs = new Array();

        for (var i = 0; i < windowHashKeyValuePairs.length; i++) {
            if (windowHashKeyValuePairs[i].Name.toString().toLowerCase().indexOf('cities') == -1 && windowHashKeyValuePairs[i].Name.toString().toLowerCase().indexOf('schooldistricts') == -1 &&
                 windowHashKeyValuePairs[i].Name.toLowerCase().indexOf('openrfi') == -1) {

                var found = false;
                for (var j = 0; j < thisHashKeyValuePairs.length; j++) {
                    if (thisHashKeyValuePairs[j].Name.toLowerCase() == windowHashKeyValuePairs[i].Name.toLowerCase()) {
                        newHashKeyValuePairs.push(new NameValuePair(windowHashKeyValuePairs[i].Name.toLowerCase(), windowHashKeyValuePairs[i].Value.toLowerCase()));
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    if (!isCommunityTab(windowHashKeyValuePairs[i].Name.toLowerCase()) && !isHomeFloorPlanOrQmiTab(windowHashKeyValuePairs[i].Name.toLowerCase())) {
                        newHashKeyValuePairs.push(new NameValuePair(windowHashKeyValuePairs[i].Name.toLowerCase(), windowHashKeyValuePairs[i].Value.toLowerCase()));
                    }
                }
            }
        }
        for (var l = 0; l < thisHashKeyValuePairs.length; l++) {
            var foundIt = false;
            for (var w = 0; w < newHashKeyValuePairs.length; w++) {
                if (thisHashKeyValuePairs[l].Name.toLowerCase() == newHashKeyValuePairs[w].Name.toLowerCase()) {
                    foundIt = true;
                    break;
                }
            }
            if ((!foundIt && searching) || (thisHashKeyValuePairs[l].Name.toLowerCase().indexOf('f_') == -1 && thisHashKeyValuePairs[l].Name.toLowerCase().indexOf('fc_') == -1 && thisHashKeyValuePairs[l].Name.toLowerCase().indexOf('collapsed') == -1) && thisHashKeyValuePairs[l].Name.toLowerCase().indexOf('searching') == -1) {
                newHashKeyValuePairs.splice(0, 0, new NameValuePair(thisHashKeyValuePairs[l].Name.toLowerCase(), thisHashKeyValuePairs[l].Value.toLowerCase()));
            }
        }


        for (var k = 0; k < newHashKeyValuePairs.length; k++) {

            if (hash.toLowerCase().indexOf(newHashKeyValuePairs[k].Name) == -1) {
                if (newHashKeyValuePairs.length > 1) {
                    hash += '&';
                }
                hash += newHashKeyValuePairs[k].Name.toLowerCase();
                if (newHashKeyValuePairs[k].Value.length > 0) {
                    hash += '=' + newHashKeyValuePairs[k].Value.toLowerCase();
                }
            }
        }

        if (hash.length > 0) {
            this.href = decodeURI(this.href).replace(decodeURI(this.hash), '') + '#' + hash;
        } else {
            this.href = decodeURI(this.href).replace(decodeURI(this.hash), '');
        }


        if (this.href.indexOf('##') > -1) {
            // clean up hash if need be
            this.href = this.href.replace('##', '#');
        }
    });
}


function getUpdatedUrlHash(include) {

    var updatedhash = '';
    var communityTabsTab = GetCommunityTabsTab();
    var homeFloorPlanOrQmiTab = GetHomeFloorPlanOrQMITab();
    var zipCode = getZipCode();

    if (communityTabsTab.length > 0) {
        // add community tab state to hash (i.e. overview, amenities, areainfo, schools, video) 
        updatedhash += '&' + communityTabsTab;
    }

    if (homeFloorPlanOrQmiTab.length > 0) {
        // add floor plan or qmi tab state to hash (i.e. home-floor-plans, quick-move-ins) 
        updatedhash += '&' + homeFloorPlanOrQmiTab;
    }

    // build array of existing filters and order
    var keyValuePairs = window.getHashKeyValuePairs(window.location.hash);
    var facetTitleFilterOrderArray = new Array(); // list of facet titles
    var filter = 'f_';

    // build hash of existing facet category filters and order
    for (var n = 0; n < keyValuePairs.length; n++) {
        if (keyValuePairs[n].Name.indexOf(filter) == 0) {
            // get facet category
            var facetTitle = keyValuePairs[n].Name.replace(filter, '');
            facetTitle = facetTitle.substring(facetTitle.indexOf('_') + 1);
            var facetValue = keyValuePairs[n].Value;
            var nameValuePair = new NameValuePair(facetTitle, facetValue);
            facetTitleFilterOrderArray.push(nameValuePair);
        }
    }

    var searching = false;
    jQuery('#narrowSearch').find('.checkboxes').each(function () {
        if (jQuery(this).is(':checked')) {
            searching = true;
            var isfound = false;
            var facetTitleCheckboxName = jQuery(this).attr('name').toLowerCase();
            var facetTitleCheckboxValue = jQuery(this).attr('value').toLowerCase();
            for (var u = 0; u < facetTitleFilterOrderArray.length; u++) {
                if (facetTitleFilterOrderArray[u].Name.toLowerCase().indexOf(facetTitleCheckboxName) > -1) {
                    if (facetTitleFilterOrderArray[u].Value.toLowerCase().indexOf('|' + facetTitleCheckboxValue + '|') == -1) {
                        facetTitleFilterOrderArray[u].Value += facetTitleCheckboxValue + '|';
                    }
                    isfound = true;
                    break;
                }
            }
            if (!isfound) {
                var tempNameValuePair = new NameValuePair(facetTitleCheckboxName, '|' + facetTitleCheckboxValue + '|');
                facetTitleFilterOrderArray.push(tempNameValuePair);
            }
        } else {
            // have to remove value
            var facetTitleCheckboxNameRemove = jQuery(this).attr('name').toLowerCase();
            var facetTitleCheckboxValueRemove = jQuery(this).attr('value').toLowerCase();
            for (var r = 0; r < facetTitleFilterOrderArray.length; r++) {
                if (facetTitleFilterOrderArray[r].Name.toLowerCase().indexOf(facetTitleCheckboxNameRemove) > -1 &&
                    facetTitleFilterOrderArray[r].Value.toLowerCase().indexOf('|' + facetTitleCheckboxValueRemove + '|') > -1) {
                    facetTitleFilterOrderArray[r].Value = facetTitleFilterOrderArray[r].Value.replace(facetTitleCheckboxValueRemove + '|', '');
                    break;
                }
            }
        }
    });

    var facetCount = 0;
    for (var h = 0; h < facetTitleFilterOrderArray.length; h++) {
        if (facetTitleFilterOrderArray[h].Value.length > 1) {

            var checkboxName = facetTitleFilterOrderArray[h].Name;
            var checkboxValueList = facetTitleFilterOrderArray[h].Value.split('|');
            var exists = false;

            jQuery('#narrowSearch').find('.checkboxes').each(function () {
                for (var k = 0; k < checkboxValueList.length; k++) {
                    var facetTitleCheckboxName = jQuery(this).attr('name').toLowerCase();
                    var facetTitleCheckboxValue = jQuery(this).attr('value').toLowerCase();
                    if (facetTitleCheckboxName.toLowerCase() == checkboxName.toLowerCase() && facetTitleCheckboxValue.toLowerCase() == checkboxValueList[k].toLowerCase()) {
                        exists = true;
                    }
                }
            });

            if (exists) {

                updatedhash += '&' + filter + facetCount + '_' + facetTitleFilterOrderArray[h].Name + '=' + facetTitleFilterOrderArray[h].Value;
                facetCount++;
            }
        }
    }

    if (searching || include) {

        //loop through facet titles, adding state to hash (i.e. expand or collapse facets)
        var temptitles = "&collapsed=|";
        jQuery('#narrowSearch').find('.section').each(function () {
            var toggler = jQuery(this).children('.toggler').first();
            var title = toggler.html();
            var classname = toggler.attr('class');
            if (title != undefined && classname.indexOf(' collapsed') == -1) {
                temptitles += title + '|';
            }
        });
        if (temptitles != '&collapsed=|') {
            updatedhash += temptitles;
        }

        //add show more options to hash (i.e. more options or less options)
        var value = '';
        jQuery('#narrowSearch').find('.facetcount').each(function () {
            var defaultValue = jQuery(this).attr('default');
            var currentValue = jQuery(this).val();
            if (defaultValue != currentValue && currentValue > defaultValue) {
                value += '&' + jQuery(this).attr('id') + "=" + jQuery(this).val();
            }

        });
        updatedhash += value;
    }

    if (zipCode.length > 0) {
        // add zip code
        updatedhash += '&zip=' + zipCode;
    }
    if (searching) {
        updatedhash += '&searching=true';
    }


    // clean up hash if need be
    updatedhash = (updatedhash.indexOf('#') > -1 ? '' : '#') + updatedhash;

    // clean up hash if need be
    if (updatedhash == '#') {
        updatedhash = '';
    }
    return updatedhash.toLowerCase();
}



function getZipCode() {
    var zip = '';
    if (window.zipcode != undefined && window.zipcode != null && window.zipcode.length > 0) {
        zip = window.zipcode;
    }
    return zip;
}

function GetHomeFloorPlanOrQMITab() {
    var returnValue = '';
    try {
        var selectedqmiTab = $('#qmiTabs .ui-state-active a')[0].getAttribute('href');
        returnValue = selectedqmiTab.replace('#', '');
    } catch (e) {
    }
    return returnValue;
}

function isHomeFloorPlanOrQmiTab(val) {
    var found = false;
    try {
        for (var i = 0; i < window.homeFloorPlanAndQMIArray.length; i++) {
            var homeFloorPlanOrQmi = window.homeFloorPlanAndQMIArray[i];
            if (val.toLowerCase() == homeFloorPlanOrQmi.toLowerCase()) {
                found = true;
            }
        }
    } catch (e) {

    }
    return found;
}

function setHomeFloorPlanOrQMITab() {
    var found = false;
    try {
        for (var i = 0; i < window.homeFloorPlanAndQMIArray.length; i++) {
            var homeFloorPlanOrQmi = window.homeFloorPlanAndQMIArray[i];
            var keyValuePairs = window.getHashKeyValuePairs(window.location.hash);
            for (var j = 0; j < keyValuePairs.length; j++) {
                if (keyValuePairs[j].Name.toLowerCase() == homeFloorPlanOrQmi.toLowerCase()) {
                    $('#qmiTabs').tabs('option', 'active', i);
                    found = true;
                    i = window.homeFloorPlanAndQMIArray.length;
                    break;
                }
            }
        }
        if (!found) {
            $('#qmiTabs').tabs('option', 'active', 0);
        }

    } catch (e) {

    }
}

function GetCommunityTabsTab() {
    var returnValue = '';
    try {
        var selectedcommunityTab = $('#tabs .ui-state-active a')[0].getAttribute('href');
        returnValue = selectedcommunityTab.replace('#', '');
    } catch (e) {
    }
    return returnValue;
}

function isCommunityTab(val) {
    var found = false;
    try {
        for (var i = 0; i < window.communityTabArray.length; i++) {
            var communityTab = window.communityTabArray[i];
            if (val.toLowerCase() == communityTab.toLowerCase()) {
                found = true;
            }
        }
    } catch (e) {

    }
    return found;
}


function setCommunityTab() {
    var found = false;
    try {
        for (var i = 0; i < window.communityTabArray.length; i++) {
            var communityTab = window.communityTabArray[i];
            var keyValuePairs = window.getHashKeyValuePairs(window.location.hash);
            for (var j = 0; j < keyValuePairs.length; j++) {
                if (keyValuePairs[j].Name.toLowerCase() == communityTab.toLowerCase()) {
                    $('#tabs').tabs('option', 'active', i);
                    found = true;
                    i = window.communityTabArray.length;
                    break;
                }
            }
        }
        if (!found) {
            $('#tabs').tabs('option', 'active', 0);
        }
    } catch (e) {

    }
}

function IsSearchInHtml() {
    var atLeastOneFacetChecked = false;
    jQuery('#narrowSearch').find('.checkboxes').each(function () {
        if (jQuery(this).is(':checked')) {
            atLeastOneFacetChecked = true;
        }
    });
    return atLeastOneFacetChecked;
}

function IsSearchInHash() {
    // simple check - if hash has an underscore we are in search mode, might need to be enhanced
    var isSearchInHash = window.location.hash.toString().toLowerCase().indexOf('&searching=true') > -1;
    return isSearchInHash;
}

function IsSortInHash() {
    // check for sort hash ("querystring") name
    var isSortInHash = window.location.hash.toString().indexOf('&st=') > -1;
    return isSortInHash;
}


function getHashKeyValuePairs(hash) {
    // split the hash, delimiter is ampersand (&)
    var hashArray = hash.toString().replace('#', '').split('&');
    var hashHash = new Array();
    for (var i = 0; i < hashArray.length; i++) {
        var tempHashKeyValue = hashArray[i];
        if (tempHashKeyValue.length > 0) {
            var key;
            var value;
            if (tempHashKeyValue.indexOf('=') > -1) {
                key = tempHashKeyValue.substring(0, tempHashKeyValue.indexOf('='));
                value = decodeURI(tempHashKeyValue.substring(tempHashKeyValue.indexOf('=') + 1));
                hashHash.push(new NameValuePair(key, value));
            } else {
                key = tempHashKeyValue;
                value = '';
                hashHash.push(new NameValuePair(key, value));
            }
        }
    }
    return hashHash;
}

function NameValuePair(n, v) {
    this.Name = n;
    this.Value = v;
}

function panMap(map, infobox, location) {
    var buffer = 25;
    var infoboxOffset = infobox.getOffset();
    var infoboxAnchor = infobox.getAnchor();
    var infoboxLocation = map.tryLocationToPixel(location, window.Microsoft.Maps.PixelReference.control);
    var dy = infoboxLocation.y - buffer - infoboxAnchor.y;
    if (dy < buffer) {
        dy *= -1;
        dy += buffer;
    } else {
        dy = 0;
    }
    var dx = infoboxLocation.x + infoboxOffset.x - infoboxAnchor.x;
    if (dx < buffer) {
        dx *= -1;
        dx += buffer;
    } else {
        dx = map.getWidth() - infoboxLocation.x + infoboxAnchor.x - infobox.getWidth();
        if (dx > buffer) {
            dx = 0;
        } else {
            dx -= buffer;
        }
    }
    if (dx != 0 || dy != 0) {
        map.setView({ centerOffset: new window.Microsoft.Maps.Point(dx, dy), center: map.getCenter() });
    }
}


function MM_swapImgRestore() { //v3.0
    var i, x, a = document.MM_sr; for (i = 0; a && i < a.length && (x = a[i]) && x.oSrc; i++) x.src = x.oSrc;
}
function MM_preloadImages() { //v3.0
    var d = document; if (d.images) {
        if (!d.MM_p) d.MM_p = new Array();
        var i, j = d.MM_p.length, a = MM_preloadImages.arguments; for (i = 0; i < a.length; i++)
            if (a[i].indexOf("#") != 0) { d.MM_p[j] = new Image; d.MM_p[j++].src = a[i]; }
    }
}

function MM_findObj(n, d) { //v4.01
    var p, i, x; if (!d) d = document; if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
        d = parent.frames[n.substring(p + 1)].document; n = n.substring(0, p);
    }
    if (!(x = d[n]) && d.all) x = d.all[n]; for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
    for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document);
    if (!x && d.getElementById) x = d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
    var i, j = 0, x, a = MM_swapImage.arguments; document.MM_sr = new Array; for (i = 0; i < (a.length - 2) ; i += 3)
        if ((x = MM_findObj(a[i])) != null) { document.MM_sr[j++] = x; if (!x.oSrc) x.oSrc = x.src; x.src = a[i + 2]; }
}






