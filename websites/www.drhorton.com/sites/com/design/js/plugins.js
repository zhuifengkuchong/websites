// Avoid `console` errors in browsers that lack a console.
(function () {
    var method;
    var noop = function () { };
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

////////////////////////////////////////////////
// Place any jQuery/helper plugins in here.
////////////////////////////////////////////////

/* Preload Images */


(function ($) {
    var cache = [];
    // Arguments are image paths relative to the current page.
    $.preLoadImages = function () {
        var args_len = arguments.length;
        for (var i = args_len; i--;) {
            var cacheImage = document.createElement('img');
            cacheImage.src = arguments[i];
            cache.push(cacheImage);
        }
    }
})(jQuery);



/*
 * FancyBox - jQuery Plugin
 * Simple and fancy lightbox alternative
 *
 * Examples and documentation at: http://fancybox.net
 * 
 * Copyright (c) 2008 - 2010 Janis Skarnelis
 * That said, it is hardly a one-person project. Many people have submitted bugs, code, and offered their advice freely. Their support is greatly appreciated.
 * 
 * Version: 1.3.4 (11/11/2010)
 * Requires: jQuery v1.3+
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 */

; (function (b) {
    var m, t, u, f, D, j, E, n, z, A, q = 0, e = {}, o = [], p = 0, d = {}, l = [], G = null, v = new Image, J = /\.(jpg|gif|png|bmp|jpeg)(.*)?$/i, W = /[^\.]\.(swf)\s*$/i, K, L = 1, y = 0, s = "", r, i, h = false, B = b.extend(b("<div/>")[0], { prop: 0 }), M = b.browser.msie && b.browser.version < 7 && !window.XMLHttpRequest, N = function () { t.hide(); v.onerror = v.onload = null; G && G.abort(); m.empty() }, O = function () {
        if (false === e.onError(o, q, e)) { t.hide(); h = false } else {
            e.titleShow = false; e.width = "auto"; e.height = "auto"; m.html('<p id="fancybox-error">The requested content cannot be loaded.<br />Please try again later.</p>');
            F()
        }
    }, I = function () {
        var a = o[q], c, g, k, C, P, w; N(); e = b.extend({}, b.fn.fancybox.defaults, typeof b(a).data("fancybox") == "undefined" ? e : b(a).data("fancybox")); w = e.onStart(o, q, e); if (w === false) h = false; else {
            if (typeof w == "object") e = b.extend(e, w); k = e.title || (a.nodeName ? b(a).attr("title") : a.title) || ""; if (a.nodeName && !e.orig) e.orig = b(a).children("img:first").length ? b(a).children("img:first") : b(a); if (k === "" && e.orig && e.titleFromAlt) k = e.orig.attr("alt"); c = e.href || (a.nodeName ? b(a).attr("href") : a.href) || null; if (/^(?:javascript)/i.test(c) ||
            c == "#") c = null; if (e.type) { g = e.type; if (!c) c = e.content } else if (e.content) g = "html"; else if (c) g = c.match(J) ? "image" : c.match(W) ? "swf" : b(a).hasClass("iframe") ? "iframe" : c.indexOf("#") === 0 ? "inline" : "ajax"; if (g) {
                if (g == "inline") { a = c.substr(c.indexOf("#")); g = b(a).length > 0 ? "inline" : "ajax" } e.type = g; e.href = c; e.title = k; if (e.autoDimensions) if (e.type == "html" || e.type == "inline" || e.type == "ajax") { e.width = "auto"; e.height = "auto" } else e.autoDimensions = false; if (e.modal) {
                    e.overlayShow = true; e.hideOnOverlayClick = false; e.hideOnContentClick =
                    false; e.enableEscapeButton = false; e.showCloseButton = false
                } e.padding = parseInt(e.padding, 10); e.margin = parseInt(e.margin, 10); m.css("padding", e.padding + e.margin); b(".fancybox-inline-tmp").unbind("fancybox-cancel").bind("fancybox-change", function () { b(this).replaceWith(j.children()) }); switch (g) {
                    case "html": m.html(e.content); F(); break; case "inline": if (b(a).parent().is("#fancybox-content") === true) { h = false; break } b('<div class="fancybox-inline-tmp" />').hide().insertBefore(b(a)).bind("fancybox-cleanup", function () { b(this).replaceWith(j.children()) }).bind("fancybox-cancel",
                    function () { b(this).replaceWith(m.children()) }); b(a).appendTo(m); F(); break; case "image": h = false; b.fancybox.showActivity(); v = new Image; v.onerror = function () { O() }; v.onload = function () { h = true; v.onerror = v.onload = null; e.width = v.width; e.height = v.height; b("<img />").attr({ id: "fancybox-img", src: v.src, alt: e.title }).appendTo(m); Q() }; v.src = c; break; case "swf": e.scrolling = "no"; C = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="' + e.width + '" height="' + e.height + '"><param name="movie" value="' + c +
                    '"></param>'; P = ""; b.each(e.swf, function (x, H) { C += '<param name="' + x + '" value="' + H + '"></param>'; P += " " + x + '="' + H + '"' }); C += '<embed src="' + c + '" type="application/x-shockwave-flash" width="' + e.width + '" height="' + e.height + '"' + P + "></embed></object>"; m.html(C); F(); break; case "ajax": h = false; b.fancybox.showActivity(); e.ajax.win = e.ajax.success; G = b.ajax(b.extend({}, e.ajax, {
                        url: c, data: e.ajax.data || {}, error: function (x) { x.status > 0 && O() }, success: function (x, H, R) {
                            if ((typeof R == "object" ? R : G).status == 200) {
                                if (typeof e.ajax.win ==
                                "function") { w = e.ajax.win(c, x, H, R); if (w === false) { t.hide(); return } else if (typeof w == "string" || typeof w == "object") x = w } m.html(x); F()
                            }
                        }
                    })); break; case "iframe": Q()
                }
            } else O()
        }
    }, F = function () {
        var a = e.width, c = e.height; a = a.toString().indexOf("%") > -1 ? parseInt((b(window).width() - e.margin * 2) * parseFloat(a) / 100, 10) + "px" : a == "auto" ? "auto" : a + "px"; c = c.toString().indexOf("%") > -1 ? parseInt((b(window).height() - e.margin * 2) * parseFloat(c) / 100, 10) + "px" : c == "auto" ? "auto" : c + "px"; m.wrapInner('<div style="width:' + a + ";height:" + c +
        ";overflow: " + (e.scrolling == "auto" ? "auto" : e.scrolling == "yes" ? "scroll" : "hidden") + ';position:relative;"></div>'); e.width = m.width(); e.height = m.height(); Q()
    }, Q = function () {
        var a, c; t.hide(); if (f.is(":visible") && false === d.onCleanup(l, p, d)) { b.event.trigger("fancybox-cancel"); h = false } else {
            h = true; b(j.add(u)).unbind(); b(window).unbind("http://www.drhorton.com/sites/com/design/js/resize.fb scroll.fb"); b(document).unbind("http://www.drhorton.com/sites/com/design/js/keydown.fb"); f.is(":visible") && d.titlePosition !== "outside" && f.css("height", f.height()); l = o; p = q; d = e; if (d.overlayShow) {
                u.css({
                    "background-color": d.overlayColor,
                    opacity: d.overlayOpacity, cursor: d.hideOnOverlayClick ? "pointer" : "auto", height: b(document).height()
                }); if (!u.is(":visible")) { M && b("select:not(#fancybox-tmp select)").filter(function () { return this.style.visibility !== "hidden" }).css({ visibility: "hidden" }).one("fancybox-cleanup", function () { this.style.visibility = "inherit" }); u.show() }
            } else u.hide(); i = X(); s = d.title || ""; y = 0; n.empty().removeAttr("style").removeClass(); if (d.titleShow !== false) {
                if (b.isFunction(d.titleFormat)) a = d.titleFormat(s, l, p, d); else a = s && s.length ?
                d.titlePosition == "float" ? '<table id="fancybox-title-float-wrap" cellpadding="0" cellspacing="0"><tr><td id="fancybox-title-float-left"></td><td id="fancybox-title-float-main">' + s + '</td><td id="fancybox-title-float-right"></td></tr></table>' : '<div id="fancybox-title-' + d.titlePosition + '">' + s + "</div>" : false; s = a; if (!(!s || s === "")) {
                    n.addClass("fancybox-title-" + d.titlePosition).html(s).appendTo("body").show(); switch (d.titlePosition) {
                        case "inside": n.css({ width: i.width - d.padding * 2, marginLeft: d.padding, marginRight: d.padding });
                            y = n.outerHeight(true); n.appendTo(D); i.height += y; break; case "over": n.css({ marginLeft: d.padding, width: i.width - d.padding * 2, bottom: d.padding }).appendTo(D); break; case "float": n.css("left", parseInt((n.width() - i.width - 40) / 2, 10) * -1).appendTo(f); break; default: n.css({ width: i.width - d.padding * 2, paddingLeft: d.padding, paddingRight: d.padding }).appendTo(f)
                    }
                }
            } n.hide(); if (f.is(":visible")) {
                b(E.add(z).add(A)).hide(); a = f.position(); r = { top: a.top, left: a.left, width: f.width(), height: f.height() }; c = r.width == i.width && r.height ==
                i.height; j.fadeTo(d.changeFade, 0.3, function () { var g = function () { j.html(m.contents()).fadeTo(d.changeFade, 1, S) }; b.event.trigger("fancybox-change"); j.empty().removeAttr("filter").css({ "border-width": d.padding, width: i.width - d.padding * 2, height: e.autoDimensions ? "auto" : i.height - y - d.padding * 2 }); if (c) g(); else { B.prop = 0; b(B).animate({ prop: 1 }, { duration: d.changeSpeed, easing: d.easingChange, step: T, complete: g }) } })
            } else {
                f.removeAttr("style"); j.css("border-width", d.padding); if (d.transitionIn == "elastic") {
                    r = V(); j.html(m.contents());
                    f.show(); if (d.opacity) i.opacity = 0; B.prop = 0; b(B).animate({ prop: 1 }, { duration: d.speedIn, easing: d.easingIn, step: T, complete: S })
                } else { d.titlePosition == "inside" && y > 0 && n.show(); j.css({ width: i.width - d.padding * 2, height: e.autoDimensions ? "auto" : i.height - y - d.padding * 2 }).html(m.contents()); f.css(i).fadeIn(d.transitionIn == "none" ? 0 : d.speedIn, S) }
            }
        }
    }, Y = function () {
        if (d.enableEscapeButton || d.enableKeyboardNav) b(document).bind("http://www.drhorton.com/sites/com/design/js/keydown.fb", function (a) {
            if (a.keyCode == 27 && d.enableEscapeButton) { a.preventDefault(); b.fancybox.close() } else if ((a.keyCode ==
            37 || a.keyCode == 39) && d.enableKeyboardNav && a.target.tagName !== "INPUT" && a.target.tagName !== "TEXTAREA" && a.target.tagName !== "SELECT") { a.preventDefault(); b.fancybox[a.keyCode == 37 ? "prev" : "next"]() }
        }); if (d.showNavArrows) { if (d.cyclic && l.length > 1 || p !== 0) z.show(); if (d.cyclic && l.length > 1 || p != l.length - 1) A.show() } else { z.hide(); A.hide() }
    }, S = function () {
        if (!b.support.opacity) { j.get(0).style.removeAttribute("filter"); f.get(0).style.removeAttribute("filter") } e.autoDimensions && j.css("height", "auto"); f.css("height", "auto");
        s && s.length && n.show(); d.showCloseButton && E.show(); Y(); d.hideOnContentClick && j.bind("click", b.fancybox.close); d.hideOnOverlayClick && u.bind("click", b.fancybox.close); b(window).bind("http://www.drhorton.com/sites/com/design/js/resize.fb", b.fancybox.resize); d.centerOnScroll && b(window).bind("http://www.drhorton.com/sites/com/design/js/scroll.fb", b.fancybox.center); if (d.type == "iframe") b('<iframe id="fancybox-frame" name="fancybox-frame' + (new Date).getTime() + '" frameborder="0" hspace="0" ' + (b.browser.msie ? 'allowtransparency="true""' : "") + ' scrolling="' + e.scrolling + '" src="' + d.href + '"></iframe>').appendTo(j);
        f.show(); h = false; b.fancybox.center(); d.onComplete(l, p, d); var a, c; if (l.length - 1 > p) { a = l[p + 1].href; if (typeof a !== "undefined" && a.match(J)) { c = new Image; c.src = a } } if (p > 0) { a = l[p - 1].href; if (typeof a !== "undefined" && a.match(J)) { c = new Image; c.src = a } }
    }, T = function (a) {
        var c = { width: parseInt(r.width + (i.width - r.width) * a, 10), height: parseInt(r.height + (i.height - r.height) * a, 10), top: parseInt(r.top + (i.top - r.top) * a, 10), left: parseInt(r.left + (i.left - r.left) * a, 10) }; if (typeof i.opacity !== "undefined") c.opacity = a < 0.5 ? 0.5 : a; f.css(c);
        j.css({ width: c.width - d.padding * 2, height: c.height - y * a - d.padding * 2 })
    }, U = function () { return [b(window).width() - d.margin * 2, b(window).height() - d.margin * 2, b(document).scrollLeft() + d.margin, b(document).scrollTop() + d.margin] }, X = function () {
        var a = U(), c = {}, g = d.autoScale, k = d.padding * 2; c.width = d.width.toString().indexOf("%") > -1 ? parseInt(a[0] * parseFloat(d.width) / 100, 10) : d.width + k; c.height = d.height.toString().indexOf("%") > -1 ? parseInt(a[1] * parseFloat(d.height) / 100, 10) : d.height + k; if (g && (c.width > a[0] || c.height > a[1])) if (e.type ==
        "image" || e.type == "swf") { g = d.width / d.height; if (c.width > a[0]) { c.width = a[0]; c.height = parseInt((c.width - k) / g + k, 10) } if (c.height > a[1]) { c.height = a[1]; c.width = parseInt((c.height - k) * g + k, 10) } } else { c.width = Math.min(c.width, a[0]); c.height = Math.min(c.height, a[1]) } c.top = parseInt(Math.max(a[3] - 20, a[3] + (a[1] - c.height - 40) * 0.5), 10); c.left = parseInt(Math.max(a[2] - 20, a[2] + (a[0] - c.width - 40) * 0.5), 10); return c
    }, V = function () {
        var a = e.orig ? b(e.orig) : false, c = {}; if (a && a.length) {
            c = a.offset(); c.top += parseInt(a.css("paddingTop"),
            10) || 0; c.left += parseInt(a.css("paddingLeft"), 10) || 0; c.top += parseInt(a.css("border-top-width"), 10) || 0; c.left += parseInt(a.css("border-left-width"), 10) || 0; c.width = a.width(); c.height = a.height(); c = { width: c.width + d.padding * 2, height: c.height + d.padding * 2, top: c.top - d.padding - 20, left: c.left - d.padding - 20 }
        } else { a = U(); c = { width: d.padding * 2, height: d.padding * 2, top: parseInt(a[3] + a[1] * 0.5, 10), left: parseInt(a[2] + a[0] * 0.5, 10) } } return c
    }, Z = function () { if (t.is(":visible")) { b("div", t).css("top", L * -40 + "px"); L = (L + 1) % 12 } else clearInterval(K) };
    b.fn.fancybox = function (a) { if (!b(this).length) return this; b(this).data("fancybox", b.extend({}, a, b.metadata ? b(this).metadata() : {})).unbind("http://www.drhorton.com/sites/com/design/js/click.fb").bind("http://www.drhorton.com/sites/com/design/js/click.fb", function (c) { c.preventDefault(); if (!h) { h = true; b(this).blur(); o = []; q = 0; c = b(this).attr("rel") || ""; if (!c || c == "" || c === "nofollow") o.push(this); else { o = b("a[rel=" + c + "], area[rel=" + c + "]"); q = o.index(this) } I() } }); return this }; b.fancybox = function (a, c) {
        var g; if (!h) {
            h = true; g = typeof c !== "undefined" ? c : {}; o = []; q = parseInt(g.index, 10) || 0; if (b.isArray(a)) {
                for (var k =
                0, C = a.length; k < C; k++) if (typeof a[k] == "object") b(a[k]).data("fancybox", b.extend({}, g, a[k])); else a[k] = b({}).data("fancybox", b.extend({ content: a[k] }, g)); o = jQuery.merge(o, a)
            } else { if (typeof a == "object") b(a).data("fancybox", b.extend({}, g, a)); else a = b({}).data("fancybox", b.extend({ content: a }, g)); o.push(a) } if (q > o.length || q < 0) q = 0; I()
        }
    }; b.fancybox.showActivity = function () { clearInterval(K); t.show(); K = setInterval(Z, 66) }; b.fancybox.hideActivity = function () { t.hide() }; b.fancybox.next = function () {
        return b.fancybox.pos(p +
        1)
    }; b.fancybox.prev = function () { return b.fancybox.pos(p - 1) }; b.fancybox.pos = function (a) { if (!h) { a = parseInt(a); o = l; if (a > -1 && a < l.length) { q = a; I() } else if (d.cyclic && l.length > 1) { q = a >= l.length ? 0 : l.length - 1; I() } } }; b.fancybox.cancel = function () { if (!h) { h = true; b.event.trigger("fancybox-cancel"); N(); e.onCancel(o, q, e); h = false } }; b.fancybox.close = function () {
        function a() { u.fadeOut("fast"); n.empty().hide(); f.hide(); b.event.trigger("fancybox-cleanup"); j.empty(); d.onClosed(l, p, d); l = e = []; p = q = 0; d = e = {}; h = false } if (!(h || f.is(":hidden"))) {
            h =
            true; if (d && false === d.onCleanup(l, p, d)) h = false; else {
                N(); b(E.add(z).add(A)).hide(); b(j.add(u)).unbind(); b(window).unbind("http://www.drhorton.com/sites/com/design/js/resize.fb scroll.fb"); b(document).unbind("http://www.drhorton.com/sites/com/design/js/keydown.fb"); j.find("iframe").attr("src", M && /^https/i.test(window.location.href || "") ? "javascript:void(false)" : "about:blank"); d.titlePosition !== "inside" && n.empty(); f.stop(); if (d.transitionOut == "elastic") {
                    r = V(); var c = f.position(); i = { top: c.top, left: c.left, width: f.width(), height: f.height() }; if (d.opacity) i.opacity = 1; n.empty().hide(); B.prop = 1;
                    b(B).animate({ prop: 0 }, { duration: d.speedOut, easing: d.easingOut, step: T, complete: a })
                } else f.fadeOut(d.transitionOut == "none" ? 0 : d.speedOut, a)
            }
        }
    }; b.fancybox.resize = function () { u.is(":visible") && u.css("height", b(document).height()); b.fancybox.center(true) }; b.fancybox.center = function (a) {
        var c, g; if (!h) {
            g = a === true ? 1 : 0; c = U(); !g && (f.width() > c[0] || f.height() > c[1]) || f.stop().animate({
                top: parseInt(Math.max(c[3] - 20, c[3] + (c[1] - j.height() - 40) * 0.5 - d.padding)), left: parseInt(Math.max(c[2] - 20, c[2] + (c[0] - j.width() - 40) * 0.5 -
                d.padding))
            }, typeof a == "number" ? a : 200)
        }
    }; b.fancybox.init = function () {
        if (!b("#fancybox-wrap").length) {
            b("body").append(m = b('<div id="fancybox-tmp"></div>'), t = b('<div id="fancybox-loading"><div></div></div>'), u = b('<div id="fancybox-overlay"></div>'), f = b('<div id="fancybox-wrap"></div>')); D = b('<div id="fancybox-outer"></div>').append('<div class="fancybox-bg" id="fancybox-bg-n"></div><div class="fancybox-bg" id="fancybox-bg-ne"></div><div class="fancybox-bg" id="fancybox-bg-e"></div><div class="fancybox-bg" id="fancybox-bg-se"></div><div class="fancybox-bg" id="fancybox-bg-s"></div><div class="fancybox-bg" id="fancybox-bg-sw"></div><div class="fancybox-bg" id="fancybox-bg-w"></div><div class="fancybox-bg" id="fancybox-bg-nw"></div>').appendTo(f);
            D.append(j = b('<div id="fancybox-content"></div>'), E = b('<a id="fancybox-close"></a>'), n = b('<div id="fancybox-title"></div>'), z = b('<a href="javascript:;" id="fancybox-left"><span class="fancy-ico" id="fancybox-left-ico"></span></a>'), A = b('<a href="javascript:;" id="fancybox-right"><span class="fancy-ico" id="fancybox-right-ico"></span></a>')); E.click(b.fancybox.close); t.click(b.fancybox.cancel); z.click(function (a) { a.preventDefault(); b.fancybox.prev() }); A.click(function (a) { a.preventDefault(); b.fancybox.next() });
            b.fn.mousewheel && f.bind("http://www.drhorton.com/sites/com/design/js/mousewheel.fb", function (a, c) { if (h) a.preventDefault(); else if (b(a.target).get(0).clientHeight == 0 || b(a.target).get(0).scrollHeight === b(a.target).get(0).clientHeight) { a.preventDefault(); b.fancybox[c > 0 ? "prev" : "next"]() } }); b.support.opacity || f.addClass("fancybox-ie"); if (M) { t.addClass("fancybox-ie6"); f.addClass("fancybox-ie6"); b('<iframe id="fancybox-hide-sel-frame" src="' + (/^https/i.test(window.location.href || "") ? "javascript:void(false)" : "about:blank") + '" scrolling="no" border="0" frameborder="0" tabindex="-1"></iframe>').prependTo(D) }
        }
    };
    b.fn.fancybox.defaults = {
        padding: 10, margin: 40, opacity: false, modal: false, cyclic: false, scrolling: "auto", width: 560, height: 340, autoScale: true, autoDimensions: true, centerOnScroll: false, ajax: {}, swf: { wmode: "transparent" }, hideOnOverlayClick: true, hideOnContentClick: false, overlayShow: true, overlayOpacity: 0.7, overlayColor: "#777", titleShow: true, titlePosition: "float", titleFormat: null, titleFromAlt: false, transitionIn: "fade", transitionOut: "fade", speedIn: 300, speedOut: 300, changeSpeed: 300, changeFade: "fast", easingIn: "swing",
        easingOut: "swing", showCloseButton: true, showNavArrows: true, enableEscapeButton: true, enableKeyboardNav: true, onStart: function () { }, onCancel: function () { }, onComplete: function () { }, onCleanup: function () { }, onClosed: function () { }, onError: function () { }
    }; b(document).ready(function () { b.fancybox.init() })
})(jQuery);

/*
**	Anderson Ferminiano
**	contato@andersonferminiano.com -- feel free to contact me for bugs or new implementations.
**	jQuery ScrollPagination
**	28th/March/2011
**	http://andersonferminiano.com/jqueryscrollpagination/
**	You may use this script for free, but keep my credits.
**	Thank you.
*/

(function ($) {


    $.fn.scrollPagination = function (options) {

        var opts = $.extend($.fn.scrollPagination.defaults, options);
        var target = opts.scrollTarget;
        if (target == null) {
            target = obj;
        }
        opts.scrollTarget = target;

        return this.each(function () {
            $.fn.scrollPagination.init($(this), opts);
        });

    };

    $.fn.stopScrollPagination = function () {
        return this.each(function () {
            $(this).attr('scrollPagination', 'disabled');
        });

    };

    $.fn.scrollPagination.loadContent = function (obj, opts) {
        var target = opts.scrollTarget;
        var mayLoadContent = $(target).scrollTop() + opts.heightOffset >= $(document).height() - $(target).height();
        if (mayLoadContent) {
            if (opts.beforeLoad != null) {
                opts.beforeLoad();
            }
            $(obj).children().attr('rel', 'loaded');
            $.ajax({
                //type: 'POST',
                type: 'GET',
                url: opts.contentPage,
                data: opts.contentData,
                success: function (data) {
                    $(obj).append(data);
                    var objectsRendered = $(obj).children('[rel!=loaded]');

                    if (opts.afterLoad != null) {
                        opts.afterLoad(objectsRendered);
                    }
                },
                dataType: 'html'
            });
        }

    };

    $.fn.scrollPagination.init = function (obj, opts) {
        var target = opts.scrollTarget;
        $(obj).attr('scrollPagination', 'enabled');

        $(target).scroll(function (event) {
            if ($(obj).attr('scrollPagination') == 'enabled') {
                $.fn.scrollPagination.loadContent(obj, opts);
            }
            else {
                event.stopPropagation();
            }
        });

        $.fn.scrollPagination.loadContent(obj, opts);

    };

    $.fn.scrollPagination.defaults = {
        'contentPage': null,
        'contentData': {},
        'beforeLoad': null,
        'afterLoad': null,
        'scrollTarget': null,
        'heightOffset': 0
    };
})(jQuery);




/*
 * jQuery Easing v1.3 - http://gsgd.co.uk/sandbox/jquery/easing/
 *
 * Uses the built in easing capabilities added In jQuery 1.1
 * to offer multiple easing options
 *
 * TERMS OF USE - jQuery Easing
 * 
 * Open source under the BSD License. 
 * 
 * Copyright Â© 2008 George McGinley Smith
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 
 * Redistributions of source code must retain the above copyright notice, this list of 
 * conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list 
 * of conditions and the following disclaimer in the documentation and/or other materials 
 * provided with the distribution.
 * 
 * Neither the name of the author nor the names of contributors may be used to endorse 
 * or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED 
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE. 
 *
*/

// t: current time, b: begInnIng value, c: change In value, d: duration

jQuery.easing['jswing'] = jQuery.easing['swing'];

jQuery.extend(jQuery.easing,
{
    def: 'easeOutQuad',
    swing: function (x, t, b, c, d) {
        //(jQuery.easing.default);
        return jQuery.easing[jQuery.easing.def](x, t, b, c, d);
    },
    easeInQuad: function (x, t, b, c, d) {
        return c * (t /= d) * t + b;
    },
    easeOutQuad: function (x, t, b, c, d) {
        return -c * (t /= d) * (t - 2) + b;
    },
    easeInOutQuad: function (x, t, b, c, d) {
        if ((t /= d / 2) < 1) return c / 2 * t * t + b;
        return -c / 2 * ((--t) * (t - 2) - 1) + b;
    },
    easeInCubic: function (x, t, b, c, d) {
        return c * (t /= d) * t * t + b;
    },
    easeOutCubic: function (x, t, b, c, d) {
        return c * ((t = t / d - 1) * t * t + 1) + b;
    },
    easeInOutCubic: function (x, t, b, c, d) {
        if ((t /= d / 2) < 1) return c / 2 * t * t * t + b;
        return c / 2 * ((t -= 2) * t * t + 2) + b;
    },
    easeInQuart: function (x, t, b, c, d) {
        return c * (t /= d) * t * t * t + b;
    },
    easeOutQuart: function (x, t, b, c, d) {
        return -c * ((t = t / d - 1) * t * t * t - 1) + b;
    },
    easeInOutQuart: function (x, t, b, c, d) {
        if ((t /= d / 2) < 1) return c / 2 * t * t * t * t + b;
        return -c / 2 * ((t -= 2) * t * t * t - 2) + b;
    },
    easeInQuint: function (x, t, b, c, d) {
        return c * (t /= d) * t * t * t * t + b;
    },
    easeOutQuint: function (x, t, b, c, d) {
        return c * ((t = t / d - 1) * t * t * t * t + 1) + b;
    },
    easeInOutQuint: function (x, t, b, c, d) {
        if ((t /= d / 2) < 1) return c / 2 * t * t * t * t * t + b;
        return c / 2 * ((t -= 2) * t * t * t * t + 2) + b;
    },
    easeInSine: function (x, t, b, c, d) {
        return -c * Math.cos(t / d * (Math.PI / 2)) + c + b;
    },
    easeOutSine: function (x, t, b, c, d) {
        return c * Math.sin(t / d * (Math.PI / 2)) + b;
    },
    easeInOutSine: function (x, t, b, c, d) {
        return -c / 2 * (Math.cos(Math.PI * t / d) - 1) + b;
    },
    easeInExpo: function (x, t, b, c, d) {
        return (t == 0) ? b : c * Math.pow(2, 10 * (t / d - 1)) + b;
    },
    easeOutExpo: function (x, t, b, c, d) {
        return (t == d) ? b + c : c * (-Math.pow(2, -10 * t / d) + 1) + b;
    },
    easeInOutExpo: function (x, t, b, c, d) {
        if (t == 0) return b;
        if (t == d) return b + c;
        if ((t /= d / 2) < 1) return c / 2 * Math.pow(2, 10 * (t - 1)) + b;
        return c / 2 * (-Math.pow(2, -10 * --t) + 2) + b;
    },
    easeInCirc: function (x, t, b, c, d) {
        return -c * (Math.sqrt(1 - (t /= d) * t) - 1) + b;
    },
    easeOutCirc: function (x, t, b, c, d) {
        return c * Math.sqrt(1 - (t = t / d - 1) * t) + b;
    },
    easeInOutCirc: function (x, t, b, c, d) {
        if ((t /= d / 2) < 1) return -c / 2 * (Math.sqrt(1 - t * t) - 1) + b;
        return c / 2 * (Math.sqrt(1 - (t -= 2) * t) + 1) + b;
    },
    easeInElastic: function (x, t, b, c, d) {
        var s = 1.70158; var p = 0; var a = c;
        if (t == 0) return b; if ((t /= d) == 1) return b + c; if (!p) p = d * .3;
        if (a < Math.abs(c)) { a = c; var s = p / 4; }
        else var s = p / (2 * Math.PI) * Math.asin(c / a);
        return -(a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p)) + b;
    },
    easeOutElastic: function (x, t, b, c, d) {
        var s = 1.70158; var p = 0; var a = c;
        if (t == 0) return b; if ((t /= d) == 1) return b + c; if (!p) p = d * .3;
        if (a < Math.abs(c)) { a = c; var s = p / 4; }
        else var s = p / (2 * Math.PI) * Math.asin(c / a);
        return a * Math.pow(2, -10 * t) * Math.sin((t * d - s) * (2 * Math.PI) / p) + c + b;
    },
    easeInOutElastic: function (x, t, b, c, d) {
        var s = 1.70158; var p = 0; var a = c;
        if (t == 0) return b; if ((t /= d / 2) == 2) return b + c; if (!p) p = d * (.3 * 1.5);
        if (a < Math.abs(c)) { a = c; var s = p / 4; }
        else var s = p / (2 * Math.PI) * Math.asin(c / a);
        if (t < 1) return -.5 * (a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p)) + b;
        return a * Math.pow(2, -10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p) * .5 + c + b;
    },
    easeInBack: function (x, t, b, c, d, s) {
        if (s == undefined) s = 1.70158;
        return c * (t /= d) * t * ((s + 1) * t - s) + b;
    },
    easeOutBack: function (x, t, b, c, d, s) {
        if (s == undefined) s = 1.70158;
        return c * ((t = t / d - 1) * t * ((s + 1) * t + s) + 1) + b;
    },
    easeInOutBack: function (x, t, b, c, d, s) {
        if (s == undefined) s = 1.70158;
        if ((t /= d / 2) < 1) return c / 2 * (t * t * (((s *= (1.525)) + 1) * t - s)) + b;
        return c / 2 * ((t -= 2) * t * (((s *= (1.525)) + 1) * t + s) + 2) + b;
    },
    easeInBounce: function (x, t, b, c, d) {
        return c - jQuery.easing.easeOutBounce(x, d - t, 0, c, d) + b;
    },
    easeOutBounce: function (x, t, b, c, d) {
        if ((t /= d) < (1 / 2.75)) {
            return c * (7.5625 * t * t) + b;
        } else if (t < (2 / 2.75)) {
            return c * (7.5625 * (t -= (1.5 / 2.75)) * t + .75) + b;
        } else if (t < (2.5 / 2.75)) {
            return c * (7.5625 * (t -= (2.25 / 2.75)) * t + .9375) + b;
        } else {
            return c * (7.5625 * (t -= (2.625 / 2.75)) * t + .984375) + b;
        }
    },
    easeInOutBounce: function (x, t, b, c, d) {
        if (t < d / 2) return jQuery.easing.easeInBounce(x, t * 2, 0, c, d) * .5 + b;
        return jQuery.easing.easeOutBounce(x, t * 2 - d, 0, c, d) * .5 + c * .5 + b;
    }
});

/*
 *
 * TERMS OF USE - EASING EQUATIONS
 * 
 * Open source under the BSD License. 
 * 
 * Copyright Â© 2001 Robert Penner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 
 * Redistributions of source code must retain the above copyright notice, this list of 
 * conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list 
 * of conditions and the following disclaimer in the documentation and/or other materials 
 * provided with the distribution.
 * 
 * Neither the name of the author nor the names of contributors may be used to endorse 
 * or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED 
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE. 
 *
 */

(function (e) { "use strict"; e(window.jQuery, window, document) })(function (e, t, n, r) { "use strict"; e.widget("selectBox.selectBoxIt", { VERSION: "2.2.0", options: { showEffect: "none", showEffectOptions: {}, showEffectSpeed: "medium", hideEffect: "none", hideEffectOptions: {}, hideEffectSpeed: "medium", showFirstOption: !0, defaultText: "", defaultIcon: "", downArrowIcon: "", theme: "bootstrap", keydownOpen: !0, isMobile: function () { var e = navigator.userAgent || navigator.vendor || t.opera; return /iPhone|iPod|iPad|Android|BlackBerry|Opera Mini|IEMobile/.test(e) }, nostyle: !1, "native": !1 }, _create: function () { var t = this; return t.originalElem = t.element[0], t.selectBox = t.element, t.selectItems = t.element.find("option"), t.firstSelectItem = t.element.find("option").slice(0, 1), t.currentFocus = 0, t.blur = !0, t.documentHeight = e(n).height(), t.textArray = [], t.currentIndex = 0, t.flipped = !1, t._createDiv()._createUnorderedList()._replaceSelectBox()._eventHandlers(), t.originalElem.disabled && t.disable && t.disable(), t._ariaAccessibility && t._ariaAccessibility(), t.options.theme === "bootstrap" ? t._twitterbootstrap() : this.options.theme === "jqueryui" ? t._jqueryui() : this.options.theme === "jquerymobile" ? t._jquerymobile() : t._addClasses(), t._mobile && t._mobile(), t.options.native && this._applyNativeSelect(), t.selectBox.trigger("create"), t }, _createDiv: function () { var t = this; return t.divText = e("<span/>", { id: (t.originalElem.id || "") && t.originalElem.id + "SelectBoxItText", "class": "selectboxit-text", unselectable: "on", text: t.firstSelectItem.text() }).attr("data-val", t.originalElem.value), t.divImage = e("<i/>", { id: (t.originalElem.id || "") && t.originalElem.id + "SelectBoxItDefaultIcon", "class": "selectboxit-default-icon", unselectable: "on" }), t.div = e("<span/>", { id: (t.originalElem.id || "") && t.originalElem.id + "SelectBoxIt", "class": "selectboxit " + (t.selectBox.attr("class") || ""), style: t.selectBox.attr("style"), name: t.originalElem.name, tabindex: t.selectBox.attr("tabindex") || "0", unselectable: "on" }).append(t.divImage).append(t.divText), t.divContainer = e("<span/>", { id: (t.originalElem.id || "") && t.originalElem.id + "SelectBoxItContainer", "class": "selectboxit-container" }).append(t.div), t }, _createUnorderedList: function () { var t = this, n, r = "", i = "", s, o = "", u = e("<ul/>", { id: (t.originalElem.id || "") && t.originalElem.id + "SelectBoxItOptions", "class": "selectboxit-options", tabindex: -1 }); return t.options.showFirstOption || (t.selectItems = t.selectBox.find("option").slice(1)), t.selectItems.each(function (u) { n = e(this).prop("disabled"), s = e(this).data("icon") || "", e(this).parent().is("optgroup") ? (r = "selectboxit-optgroup-option", e(this).index() === 0 ? i = '<div class="selectboxit-optgroup-header" data-disabled="true">' + e(this).parent().first().attr("label") + "</div>" : i = "") : r = "", o += i + '<li id="' + u + '" data-val="' + t.htmlEscape(this.value) + '" data-disabled="' + n + '" class="' + r + " selectboxit-option" + (e(this).attr("class") || "") + '" style="' + (e(this).attr("style") || "") + '"><a class="selectboxit-option-anchor"><i class="selectboxit-option-icon ' + s + '"></i>' + t.htmlEscape(e(this).text()) + "</a></li>", t.textArray[u] = e(this).text(), this.selected && (t.divText.text(e(this).text()), t.currentFocus = u) }), t.options.defaultText && t.divText.text(t.options.defaultText), t.selectBox.data("text") && (t.divText.text(t.selectBox.data("text")), t.options.defaultText = t.selectBox.data("text")), u.append(o), t.list = u, t.divContainer.append(t.list), t.listItems = t.list.find("li"), t.listItems.first().addClass("selectboxit-option-first"), t.listItems.last().addClass("selectboxit-option-last"), t.list.find("li[data-disabled='true']").not(".optgroupHeader").addClass("ui-state-disabled"), t.currentFocus === 0 && !t.options.showFirstOption && t.listItems.eq(0).hasClass("ui-state-disabled") && (t.currentFocus = +t.listItems.not(".ui-state-disabled").first().attr("id")), t.divImage.addClass(t.selectBox.data("icon") || t.options.defaultIcon || t.listItems.eq(t.currentFocus).find("i").attr("class")), t }, _replaceSelectBox: function () { var t = this; t.selectBox.css("display", "none").after(t.divContainer); var n = t.div.height(); return t.downArrow = e("<i/>", { id: (t.originalElem.id || "") && t.originalElem.id + "SelectBoxItArrow", "class": "selectboxit-arrow", unselectable: "on" }), t.downArrowContainer = e("<span/>", { id: (t.originalElem.id || "") && t.originalElem.id + "SelectBoxItArrowContainer", "class": "selectboxit-arrow-container", unselectable: "on" }).append(t.downArrow), t.div.append(this.options.nostyle ? t.downArrow : t.downArrowContainer), t.options.nostyle || (t.downArrowContainer.css({ height: n + "px" }), t.divText.css({ "line-height": t.div.css("height"), "max-width": t.div.outerWidth() - (t.downArrowContainer.outerWidth() + t.divImage.outerWidth()) }), t.divImage.css({ "margin-top": n / 4 })), t }, _scrollToView: function (e) { var t = this, n = t.list.scrollTop(), r = t.listItems.eq(t.currentFocus).height(), i = t.listItems.eq(t.currentFocus).position().top, s = t.list.height(); return e === "search" ? s - i < r ? t.list.scrollTop(n + (i - (s - r))) : i < -1 && t.list.scrollTop(i - r) : e === "up" ? i < -1 && t.list.scrollTop(n - Math.abs(t.listItems.eq(t.currentFocus).position().top)) : e === "down" && s - i < r && t.list.scrollTop(n + (Math.abs(t.listItems.eq(t.currentFocus).position().top) - s + r)), t }, _callbackSupport: function (t) { var n = this; return e.isFunction(t) && t.call(n, n.div), n }, open: function (e) { var t = this; t._dynamicPositioning && t._dynamicPositioning(); if (!this.list.is(":visible")) { t.selectBox.trigger("open"); switch (t.options.showEffect) { case "none": t.list.show(), t._scrollToView("search"); break; case "show": t.list.show(t.options.showEffectSpeed, function () { t._scrollToView("search") }); break; case "slideDown": t.list.slideDown(t.options.showEffectSpeed, function () { t._scrollToView("search") }); break; case "fadeIn": t.list.fadeIn(t.options.showEffectSpeed), t._scrollToView("search"); break; default: t.list.show(t.options.showEffect, t.options.showEffectOptions, t.options.showEffectSpeed, function () { t._scrollToView("search") }) } } return t._callbackSupport(e), t }, close: function (e) { var t = this; if (t.list.is(":visible")) { t.selectBox.trigger("close"); switch (t.options.hideEffect) { case "none": t.list.hide(), t._scrollToView("search"); break; case "hide": t.list.hide(t.options.hideEffectSpeed); break; case "slideUp": t.list.slideUp(t.options.hideEffectSpeed); break; case "fadeOut": t.list.fadeOut(t.options.hideEffectSpeed); break; default: t.list.hide(t.options.hideEffect, t.options.hideEffectOptions, t.options.hideEffectSpeed, function () { t._scrollToView("search") }) } } return t._callbackSupport(e), t }, _eventHandlers: function () { var t = this, n = 38, r = 40, i = 13, s = 8, o = 9, u = 32, a = 27; return this.div.bind({ "click.selectBoxIt": function () { t.div.focus(), t.originalElem.disabled || (t.selectBox.trigger("click"), t.list.is(":visible") ? t.close() : t.open()) }, "mousedown.selectBoxIt": function () { e(this).data("mdown", !0) }, "blur.selectBoxIt": function () { t.blur && (t.selectBox.trigger("blur").trigger("focusout"), t.list.is(":visible") && t.close()) }, "focus.selectBoxIt": function () { var n = e(this).data("mdown"); e(this).removeData("mdown"), n || t.selectBox.trigger("tab-focus"), t.list.is(":visible") || t.selectBox.trigger("focus").trigger("focusin") }, "keydown.selectBoxIt": function (e) { var u = e.keyCode; switch (u) { case r: e.preventDefault(), t.moveDown && (t.options.keydownOpen ? t.list.is(":visible") ? t.moveDown() : t.open() : t.moveDown()), t.options.keydownOpen && t.open(); break; case n: e.preventDefault(), t.moveUp && (t.options.keydownOpen ? t.list.is(":visible") ? t.moveUp() : t.open() : t.moveUp()), t.options.keydownOpen && t.open(); break; case i: e.preventDefault(), t.list.is(":visible") && t.close(), t._checkDefaultText(), t.selectBox.trigger("enter"); break; case o: t.selectBox.trigger("tab-blur"); break; case s: e.preventDefault(), t.selectBox.trigger("backspace"); break; case a: t.close(); break; default: } }, "keypress.selectBoxIt": function (e) { var n = e.charCode || e.keyCode, r = String.fromCharCode(n); n === u && e.preventDefault(), t.search && t.search(r, !0, "") }, "mouseenter.selectBoxIt": function () { t.selectBox.trigger("mouseenter") }, "mouseleave.selectBoxIt": function () { t.selectBox.trigger("mouseleave") } }), t.list.bind({ "mouseover.selectBoxIt": function () { t.blur = !1 }, "mouseout.selectBoxIt": function () { t.blur = !0 }, "focusin.selectBoxIt": function () { t.div.focus() } }).delegate("li", "click.selectBoxIt", function () { e(this).data("disabled") || (t.selectBox.val(e(this).attr("data-val")), t.currentFocus = +this.id, t.close(), t.originalElem.value !== t.divText.attr("data-val") && t.selectBox.trigger("change", !0), t._checkDefaultText(), t.selectBox.trigger("option-click")) }).delegate("li", "focus.selectBoxIt", function () { e(this).data("disabled") || (t.originalElem.value = e(this).attr("data-val"), t.originalElem.value !== t.divText.attr("data-val") && t.selectBox.trigger("change", !0)) }), t.selectBox.bind({ "change.selectBoxIt, internal-change.selectBoxIt": function (e, n) { if (!n) { var r = t.list.find('li[data-val="' + t.originalElem.value + '"]'); r.length && (t.listItems.eq(t.currentFocus).removeClass(t.focusClass), t.currentFocus = +r.attr("id")) } t.divText.text(t.listItems.eq(t.currentFocus).find("a").text()).attr("data-val", t.originalElem.value), t.listItems.eq(t.currentFocus).find("i").attr("class") && t.divImage.attr("class", t.listItems.eq(t.currentFocus).find("i").attr("class")).addClass("selectboxit-default-icon"), t.selectBox.trigger("changed") }, "disable.selectBoxIt": function () { t.div.addClass("ui-state-disabled") }, "enable.selectBoxIt": function () { t.div.removeClass("ui-state-disabled") } }), t }, _addClasses: function (t) { var n = this, r = t.focusClasses || "selectboxit-focus", i = t.hoverClasses || "selectboxit-hover", s = t.buttonClasses || "selectboxit-btn", o = t.listClasses || "selectboxit-dropdown"; return n.focusClass = r, n.downArrow.addClass(n.selectBox.data("downarrow") || n.options.downArrowIcon || t.arrowClasses), n.divContainer.addClass(t.containerClasses), n.div.addClass(s), n.list.addClass(o), n.listItems.bind({ "focus.selectBoxIt": function () { e(this).addClass(r) }, "blur.selectBoxIt": function () { e(this).removeClass(r) } }), n.selectBox.bind({ "open.selectBoxIt": function () { n.div.removeClass(i).add(n.listItems.eq(n.currentFocus)).addClass(r) }, "blur.selectBoxIt": function () { n.div.removeClass(r) }, "mouseenter.selectBoxIt": function () { n.div.addClass(i) }, "mouseleave.selectBoxIt": function () { n.div.removeClass(i) } }), n.listItems.bind({ "mouseenter.selectBoxIt": function () { n.listItems.removeClass(r), e(this).addClass(i) }, "mouseleave.selectBoxIt": function () { e(this).removeClass(i) } }), e(".selectboxit-option-icon").not(".selectboxit-default-icon").css("margin-top", n.downArrowContainer.height() / 4), n }, _jqueryui: function () { var e = this; return e._addClasses({ focusClasses: "ui-state-focus", hoverClasses: "ui-state-hover", arrowClasses: "ui-icon ui-icon-triangle-1-s", buttonClasses: "ui-widget ui-state-default", listClasses: "ui-widget ui-widget-content", containerClasses: "" }), e }, _twitterbootstrap: function () { var e = this; return e._addClasses({ focusClasses: "active", hoverClasses: "", arrowClasses: "caret", buttonClasses: "btn", listClasses: "dropdown-menu", containerClasses: "" }), e }, _jquerymobile: function () { var e = this; return e._addClasses({ focusClasses: "ui-btn-active-c ui-btn-down-c", hoverClasses: "ui-btn-hover-c", arrowClasses: "ui-icon ui-icon-arrow-d ui-icon-shadow", buttonClasses: "ui-btn ui-btn-icon-right ui-btn-corner-all ui-shadow ui-btn-up-c", listClasses: "ui-btn ui-btn-icon-right ui-btn-corner-all ui-shadow ui-btn-up-c", containerClasses: "" }), e }, destroy: function (t) { var n = this; return n._destroySelectBoxIt(), e.Widget.prototype.destroy.call(n), n._callbackSupport(t), n }, _destroySelectBoxIt: function () { var e = this; return e.div.unbind(".selectBoxIt").undelegate(".selectBoxIt"), e.divContainer.remove(), e.selectBox.trigger("destroy").show(), e }, refresh: function (e) { var t = this; return t._destroySelectBoxIt()._create()._callbackSupport(e), t.selectBox.trigger("refresh"), t }, _applyNativeSelect: function () { var e = this, t; e.divContainer.css({ position: "static" }), e.selectBox.css({ display: "block", width: e.div.outerWidth(), height: e.div.outerHeight(), opacity: "0", position: "absolute", top: e.div.offset().top, bottom: e.div.offset().bottom, left: e.div.offset().left, right: e.div.offset().right, cursor: "pointer" }).bind({ changed: function () { t = e.selectBox.find("option").filter(":selected"), e.divText.text(t.text()), e.list.find('li[data-val="' + t.val() + '"]').find("i").attr("class") && e.divImage.attr("class", e.list.find('li[data-val="' + t.val() + '"]').find("i").attr("class")).addClass("selectboxit-default-icon") } }) }, htmlEscape: function (e) { return String(e).replace(/&/g, "&amp;").replace(/"/g, "&quot;").replace(/'/g, "&#39;").replace(/</g, "&lt;").replace(/>/g, "&gt;") }, _checkDefaultText: function () { var e = this; e.options.defaultText && e.divText.text() === e.options.defaultText && e.divText.text(e.listItems.eq(e.currentFocus).text()).trigger("internal-change", !0) }, selectOption: function (e, t) { var n = this; return typeof e == "number" ? n.selectBox.val(n.selectBox.find("option").eq(e).val()).change() : typeof e == "string" && n.selectBox.val(e).change(), n._callbackSupport(t), n } }) });
$(function () { $.selectBox.selectBoxIt.prototype._ariaAccessibility = function () { var e = this; return e.div.attr({ role: "combobox", "aria-autocomplete": "list", "aria-expanded": "false", "aria-owns": e.list.attr("id"), "aria-activedescendant": e.listItems.eq(e.currentFocus).attr("id"), "aria-label": $("label[for='" + e.originalElem.id + "']").text() || "", "aria-live": "assertive" }).bind({ "disable.selectBoxIt": function () { e.div.attr("aria-disabled", "true") }, "enable.selectBoxIt": function () { e.div.attr("aria-disabled", "false") } }), e.list.attr({ role: "listbox", "aria-hidden": "true" }), e.listItems.attr({ role: "option" }), e.selectBox.bind({ "change.selectBoxIt": function () { e.divText.attr("aria-label", e.originalElem.value) }, "open.selectBoxIt": function () { e.list.attr("aria-hidden", "false"), e.div.attr("aria-expanded", "true") }, "close.selectBoxIt": function () { e.list.attr("aria-hidden", "true"), e.div.attr("aria-expanded", "false") } }), e } });
$(function () { $.selectBox.selectBoxIt.prototype.disable = function (e) { var t = this; if (!t.options.disabled) return t.close(), t.selectBox.trigger("disable").attr("disabled", "disabled"), t.div.removeAttr("tabindex").addClass("selectboxit-disabled"), $.Widget.prototype.disable.call(t), t._callbackSupport(e), t }, $.selectBox.selectBoxIt.prototype._isDisabled = function (e) { var t = this; return t.originalElem.disabled && t.disable(), t } });
$(function () { $.selectBox.selectBoxIt.prototype._dynamicPositioning = function () { var e = this, t = e.div.offset().top, n = e.list.data("max-height") || e.list.outerHeight(), r = e.div.outerHeight(), i = $(window).height(), s = $(window).scrollTop(), o = t + r + n <= i + s, u = !o; e.list.data("max-height") || e.list.data("max-height", e.list.outerHeight()), e.selectBox.css("display", "none"); if (!u) e.list.css("max-height", e.list.data("max-height")), e.list.css("top", "auto"); else if (e.div.offset().top - s >= n) e.list.css("max-height", e.list.data("max-height")), e.list.css("top", e.div.position().top - e.list.outerHeight()); else { var a = Math.abs(t + r + n - (i + s)), f = Math.abs(e.div.offset().top - s - n); a < f ? (e.list.css("max-height", e.list.data("max-height") - a - r / 2), e.list.css("top", "auto")) : (e.list.css("max-height", e.list.data("max-height") - f - r / 2), e.list.css("top", e.div.position().top - e.list.outerHeight())) } return e } });
$(function () { $.selectBox.selectBoxIt.prototype.enable = function (e) { var t = this; return t.options.disabled && (t.selectBox.trigger("enable").removeAttr("disabled"), t.div.attr("tabindex", 0).removeClass("selectboxit-disabled"), $.Widget.prototype.enable.call(t), t._callbackSupport(e)), t } });
$(function () { $.selectBox.selectBoxIt.prototype.moveDown = function (e) { var t = this; t.currentFocus += 1; var n = t.listItems.eq(t.currentFocus).data("disabled"), r = t.listItems.eq(t.currentFocus).nextAll("li").not("[data-disabled='true']").first().length; if (t.currentFocus === t.listItems.length) t.currentFocus -= 1; else { if (n && r) { t.listItems.eq(t.currentFocus - 1).blur(), t.moveDown(); return } n && !r ? t.currentFocus -= 1 : (t.listItems.eq(t.currentFocus - 1).blur().end().eq(t.currentFocus).focus(), t._scrollToView("down"), t.selectBox.trigger("moveDown")) } return t._callbackSupport(e), t }, $.selectBox.selectBoxIt.prototype.moveUp = function (e) { var t = this; t.currentFocus -= 1; var n = t.listItems.eq(t.currentFocus).data("disabled"), r = t.listItems.eq(t.currentFocus).prevAll("li").not("[data-disabled='true']").first().length; if (t.currentFocus === -1) t.currentFocus += 1; else { if (n && r) { t.listItems.eq(t.currentFocus + 1).blur(), t.moveUp(); return } n && !r ? t.currentFocus += 1 : (t.listItems.eq(this.currentFocus + 1).blur().end().eq(t.currentFocus).focus(), t._scrollToView("up"), t.selectBox.trigger("moveUp")) } return t._callbackSupport(e), t } });
$(function () { $.selectBox.selectBoxIt.prototype._setCurrentSearchOption = function (e) { var t = this; return (e !== 0 || !!t.options.showFirstOption) && t.listItems.eq(e).data("disabled") !== !0 && (t.divText.text(t.textArray[e]), t.listItems.eq(t.currentFocus).blur(), t.currentIndex = e, t.currentFocus = e, t.listItems.eq(t.currentFocus).focus(), t._scrollToView("search"), t.selectBox.trigger("search")), t }, $.selectBox.selectBoxIt.prototype._searchAlgorithm = function (e, t) { var n = this, r = !1, i, s, o; for (i = e, o = n.textArray.length; i < o; i += 1) { for (s = 0; s < o; s += 1) n.textArray[s].search(t) !== -1 && (r = !0, s = o); r || (n.currentText = n.currentText.charAt(n.currentText.length - 1).replace(/[|()\[{.+*?$\\]/g, "\\$0"), t = new RegExp(n.currentText, "gi")); if (n.currentText.length < 3) { t = new RegExp(n.currentText.charAt(0), "gi"); if (n.textArray[i].charAt(0).search(t) !== -1) return n._setCurrentSearchOption(i), n.currentIndex += 1, !1 } else if (n.textArray[i].search(t) !== -1) return n._setCurrentSearchOption(i), !1; if (n.textArray[i].toLowerCase() === n.currentText.toLowerCase()) return n._setCurrentSearchOption(i), n.currentText = "", !1 } return !0 }, $.selectBox.selectBoxIt.prototype.search = function (e, t, n) { var r = this; t ? r.currentText += e.replace(/[|()\[{.+*?$\\]/g, "\\$0") : r.currentText = e.replace(/[|()\[{.+*?$\\]/g, "\\$0"); var i = new RegExp(r.currentText, "gi"), s = r._searchAlgorithm(r.currentIndex, i); return s && r._searchAlgorithm(0, i), r._callbackSupport(n), r } });
$(function () { $.selectBox.selectBoxIt.prototype._mobile = function (e) { return this.options.isMobile() && this._applyNativeSelect(), this } });
$(function () { $.selectBox.selectBoxIt.prototype.setOption = function (e, t, n) { var r = this; return e === "showFirstOption" && !t ? r.listItems.eq(0).hide() : e === "showFirstOption" && t ? r.listItems.eq(0).show() : e === "defaultIcon" && t ? r.divImage.attr("class", t + " selectboxit-arrow") : e === "downArrowIcon" && t ? r.downArrow.attr("class", t + " selectboxit-arrow") : e === "defaultText" && r.divText.text(t), $.Widget.prototype._setOption.apply(r, arguments), r._callbackSupport(n), r } });
$(function () { $.selectBox.selectBoxIt.prototype.setOptions = function (e, t) { var n = this; return $.Widget.prototype._setOptions.apply(n, arguments), n.options.showFirstOption ? n.listItems.eq(0).show() : n.listItems.eq(0).hide(), n.options.defaultIcon && n.divImage.attr("class", n.options.defaultIcon + " selectboxit-arrow"), n.options.downArrowIcon && n.downArrow.attr("class", n.options.downArrowIcon + " selectboxit-arrow"), n.options.defaultText && n.divText.text(n.options.defaultText), n._callbackSupport(t), n } });
$(function () { $.selectBox.selectBoxIt.prototype.wait = function (e, t) { var n = this, r = this.returnTimeout(e); return r.then(function () { n._callbackSupport(t) }), n }, $.selectBox.selectBoxIt.prototype.returnTimeout = function (e) { return $.Deferred(function (t) { setTimeout(t.resolve, e) }) } });



/*
 * Picker Plugin [Formstone Library]
 * @author Ben Plum
 * @version 0.2.8
 *
 * Copyright © 2013 Ben Plum <mr@benplum.com>
 * Released under the MIT License <http://www.opensource.org/licenses/mit-license.php>
 */

if (jQuery) (function ($) {

    // Default Options
    var options = {
        customClass: ""
    };

    // Public Methods
    var pub = {

        // Set Defaults
        defaults: function (opts) {
            options = $.extend(options, opts || {});
            return $(this);
        },

        // Disable field
        disable: function () {
            return $(this).each(function (i) {
                var $input = $(this),
					$picker = $input.parent(".picker");

                $input.prop("disabled", true);
                $picker.addClass("disabled");
            });
        },

        // Enable field
        enable: function () {
            return $(this).each(function (i) {
                var $input = $(this),
					$picker = $input.parent(".picker");

                $input.prop("disabled", false);
                $picker.removeClass("disabled");
            });
        },

        // Destroy picker
        destroy: function () {
            return $(this).each(function (i) {
                var $input = $(this),
					$picker = $input.parent(".picker"),
					$handle = $picker.find(".picker-handle"),
					$label = $("label[for=" + $input.attr("id") + "]");

                // Restore DOM / Unbind click events
                $picker.off(".picker");
                $handle.remove();
                $input.off(".picker")
					  .removeClass("picker-element")
					  .unwrap();
                $label.removeClass("picker-label");
            });
        }
    };

    // Private Methods

    // Initialize
    function _init(opts) {
        opts = opts || {};

        // Define settings
        var settings = $.extend({}, options, opts);

        // Apply to each element
        return $(this).each(function (i) {
            var $input = $(this);

            if (!$input.data("picker")) {
                var $label = $("label[for=" + $input.attr("id") + "]"),
					$merged = $($.merge($.merge([], $input), $label)),
					type = $input.attr("type"),
					typeClass = "picker-" + (type == "radio" ? "radio" : "checkbox"),
					group = $input.attr("name");

                // Modify DOM
                $merged.wrapAll('<div class="picker ' + typeClass + ' ' + settings.customClass + '" />');

                $input.addClass("picker-element")
					  .after('<div class="picker-handle"><div class="picker-flag" /></div>');
                $label.addClass("picker-label");

                // Store plugin data
                var $picker = $input.parent(".picker");
                var $handle = $picker.find(".picker-handle");

                // Check checked
                if ($input.is(":checked")) {
                    $picker.addClass("checked");
                }

                // Check disabled
                if ($input.is(":disabled")) {
                    $picker.addClass("disabled");
                }

                var data = $.extend({
                    $picker: $picker,
                    $input: $input,
                    $handle: $handle,
                    $label: $label,
                    group: group,
                    isRadio: (type == "radio"),
                    isCheckbox: (type == "checkbox")
                }, settings);

                // Bind click events
                $input.on("focus.picker", data, _onFocus)
					  .on("blur.picker", data, _onBlur)
					  .on("change.picker", data, _onChange)
					  .on("deselect.picker", data, _onDeselect);

                $picker.on("click.picker", ".picker-handle", data, _onClick)
					   .data("picker", data);
            }
        });
    }

    // Handle click
    function _onClick(e) {
        e.preventDefault();
        e.stopPropagation();

        var data = e.data;

        if (!data.$input.is(":disabled")) {
            // Change events fire before we change the val
            if (data.$input.is(":checked")) {
                if (!data.isRadio) {
                    _onDeselect(e);
                }
            } else {
                _onSelect(e);
            }
        }
    }

    // Handle change
    function _onChange(e, internal) {
        if (!internal) {
            var data = e.data;

            if (!data.$input.is(":disabled")) {
                // Change events fire after val has changed
                if (data.$input.is(":checked")) {
                    _onSelect(e, true);
                } else {
                    _onDeselect(e, true);
                }
            }
        }
    }

    // Handle select
    function _onSelect(e, internal) {
        var data = e.data;

        if (typeof data.group !== "undefined" && data.isRadio) {
            $('input[name="' + data.group + '"]').not(data.$input).trigger("deselect");
        }

        data.$input.prop("checked", true);
        data.$picker.addClass("checked");

        if (!internal) {
            data.$input.trigger("change", [true]);
        }
    }

    // Handle deselect
    function _onDeselect(e, internal) {
        var data = e.data;

        data.$input.prop("checked", false);
        data.$picker.removeClass("checked");

        if (!internal) {
            data.$input.trigger("change", [true]);
        }
    }

    // Handle focus
    function _onFocus(e) {
        e.data.$picker.addClass("focus");
    }

    // Handle blur
    function _onBlur(e) {
        e.data.$picker.removeClass("focus");
    }

    // Define Plugin
    $.fn.picker = function (method) {
        if (pub[method]) {
            return pub[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return _init.apply(this, arguments);
        }
        return this;
    };
})(jQuery);



/**
 * BxSlider v4.1 - Fully loaded, responsive content slider
 * http://bxslider.com
 *
 * Copyright 2012, Steven Wanderski - http://stevenwanderski.com - http://bxcreative.com
 * Written while drinking Belgian ales and listening to jazz
 *
 * Released under the WTFPL license - http://sam.zoy.org/wtfpl/
 */

; (function ($) {

    var plugin = {};

    var defaults = {

        // GENERAL
        mode: 'horizontal',
        slideSelector: '',
        infiniteLoop: true,
        hideControlOnEnd: false,
        speed: 500,
        easing: null,
        slideMargin: 0,
        startSlide: 0,
        randomStart: false,
        captions: false,
        ticker: false,
        tickerHover: false,
        adaptiveHeight: false,
        adaptiveHeightSpeed: 500,
        video: false,
        useCSS: true,
        preloadImages: 'visible',

        // TOUCH
        touchEnabled: true,
        swipeThreshold: 50,
        oneToOneTouch: true,
        preventDefaultSwipeX: true,
        preventDefaultSwipeY: false,

        // PAGER
        pager: true,
        pagerType: 'full',
        pagerShortSeparator: ' / ',
        pagerSelector: null,
        buildPager: null,
        pagerCustom: null,

        // CONTROLS
        controls: true,
        nextText: 'Next',
        prevText: 'Prev',
        nextSelector: null,
        prevSelector: null,
        autoControls: false,
        startText: 'Start',
        stopText: 'Stop',
        autoControlsCombine: false,
        autoControlsSelector: null,

        // AUTO
        auto: false,
        pause: 4000,
        autoStart: true,
        autoDirection: 'next',
        autoHover: false,
        autoDelay: 0,

        // CAROUSEL
        minSlides: 1,
        maxSlides: 1,
        moveSlides: 0,
        slideWidth: 0,

        // CALLBACKS
        onSliderLoad: function () { },
        onSlideBefore: function () { },
        onSlideAfter: function () { },
        onSlideNext: function () { },
        onSlidePrev: function () { }
    }

    $.fn.bxSlider = function (options) {

        if (this.length == 0) return;

        // support mutltiple elements
        if (this.length > 1) {
            this.each(function () { $(this).bxSlider(options) });
            return this;
        }

        // create a namespace to be used throughout the plugin
        var slider = {};

        // set a reference to our slider element
        var el = this;
        plugin.el = this;

        /**
		 * Makes slideshow responsive
		 */
        // first get the original window dimens (thanks alot IE)
        var windowWidth = $(window).width();
        var windowHeight = $(window).height();



        /**
		 * ===================================================================================
		 * = PRIVATE FUNCTIONS
		 * ===================================================================================
		 */

        /**
		 * Initializes namespace settings to be used throughout plugin
		 */
        var init = function () {
            // merge user-supplied options with the defaults
            slider.settings = $.extend({}, defaults, options);
            // parse slideWidth setting
            slider.settings.slideWidth = parseInt(slider.settings.slideWidth);
            // store the original children
            slider.children = el.children(slider.settings.slideSelector);
            // check if actual number of slides is less than minSlides / maxSlides
            if (slider.children.length < slider.settings.minSlides) slider.settings.minSlides = slider.children.length;
            if (slider.children.length < slider.settings.maxSlides) slider.settings.maxSlides = slider.children.length;
            // if random start, set the startSlide setting to random number
            if (slider.settings.randomStart) slider.settings.startSlide = Math.floor(Math.random() * slider.children.length);
            // store active slide information
            slider.active = { index: slider.settings.startSlide }
            // store if the slider is in carousel mode (displaying / moving multiple slides)
            slider.carousel = slider.settings.minSlides > 1 || slider.settings.maxSlides > 1;
            // if carousel, force preloadImages = 'all'
            if (slider.carousel) slider.settings.preloadImages = 'all';
            // calculate the min / max width thresholds based on min / max number of slides
            // used to setup and update carousel slides dimensions
            slider.minThreshold = (slider.settings.minSlides * slider.settings.slideWidth) + ((slider.settings.minSlides - 1) * slider.settings.slideMargin);
            slider.maxThreshold = (slider.settings.maxSlides * slider.settings.slideWidth) + ((slider.settings.maxSlides - 1) * slider.settings.slideMargin);
            // store the current state of the slider (if currently animating, working is true)
            slider.working = false;
            // initialize the controls object
            slider.controls = {};
            // initialize an auto interval
            slider.interval = null;
            // determine which property to use for transitions
            slider.animProp = slider.settings.mode == 'vertical' ? 'top' : 'left';
            // determine if hardware acceleration can be used
            slider.usingCSS = slider.settings.useCSS && slider.settings.mode != 'fade' && (function () {
                // create our test div element
                var div = document.createElement('div');
                // css transition properties
                var props = ['WebkitPerspective', 'MozPerspective', 'OPerspective', 'msPerspective'];
                // test for each property
                for (var i in props) {
                    if (div.style[props[i]] !== undefined) {
                        slider.cssPrefix = props[i].replace('Perspective', '').toLowerCase();
                        slider.animProp = '-' + slider.cssPrefix + '-transform';
                        return true;
                    }
                }
                return false;
            }());
            // if vertical mode always make maxSlides and minSlides equal
            if (slider.settings.mode == 'vertical') slider.settings.maxSlides = slider.settings.minSlides;
            // perform all DOM / CSS modifications
            setup();
        }

        /**
		 * Performs all DOM and CSS modifications
		 */
        var setup = function () {
            // wrap el in a wrapper
            el.wrap('<div class="bx-wrapper"><div class="bx-viewport"></div></div>');
            // store a namspace reference to .bx-viewport
            slider.viewport = el.parent();
            // add a loading div to display while images are loading
            slider.loader = $('<div class="bx-loading" />');
            slider.viewport.prepend(slider.loader);
            // set el to a massive width, to hold any needed slides
            // also strip any margin and padding from el
            el.css({
                width: slider.settings.mode == 'horizontal' ? slider.children.length * 215 + '%' : 'auto',
                position: 'relative'
            });
            // if using CSS, add the easing property
            if (slider.usingCSS && slider.settings.easing) {
                el.css('-' + slider.cssPrefix + '-transition-timing-function', slider.settings.easing);
                // if not using CSS and no easing value was supplied, use the default JS animation easing (swing)
            } else if (!slider.settings.easing) {
                slider.settings.easing = 'swing';
            }
            var slidesShowing = getNumberSlidesShowing();
            // make modifications to the viewport (.bx-viewport)
            slider.viewport.css({
                width: '100%',
                overflow: 'hidden',
                position: 'relative'
            });
            slider.viewport.parent().css({
                maxWidth: getViewportMaxWidth()
            });
            // apply css to all slider children
            slider.children.css({
                'float': slider.settings.mode == 'horizontal' ? 'left' : 'none',
                listStyle: 'none',
                position: 'relative'
            });
            // apply the calculated width after the float is applied to prevent scrollbar interference
            slider.children.width(getSlideWidth());
            // if slideMargin is supplied, add the css
            if (slider.settings.mode == 'horizontal' && slider.settings.slideMargin > 0) slider.children.css('marginRight', slider.settings.slideMargin);
            if (slider.settings.mode == 'vertical' && slider.settings.slideMargin > 0) slider.children.css('marginBottom', slider.settings.slideMargin);
            // if "fade" mode, add positioning and z-index CSS
            if (slider.settings.mode == 'fade') {
                slider.children.css({
                    position: 'absolute',
                    zIndex: 0,
                    display: 'none'
                });
                // prepare the z-index on the showing element
                slider.children.eq(slider.settings.startSlide).css({ zIndex: 50, display: 'block' });
            }
            // create an element to contain all slider controls (pager, start / stop, etc)
            slider.controls.el = $('<div class="bx-controls" />');
            // if captions are requested, add them
            if (slider.settings.captions) appendCaptions();
            // if infinite loop, prepare additional slides
            if (slider.settings.infiniteLoop && slider.settings.mode != 'fade' && !slider.settings.ticker) {
                var slice = slider.settings.mode == 'vertical' ? slider.settings.minSlides : slider.settings.maxSlides;
                var sliceAppend = slider.children.slice(0, slice).clone().addClass('bx-clone');
                var slicePrepend = slider.children.slice(-slice).clone().addClass('bx-clone');
                el.append(sliceAppend).prepend(slicePrepend);
            }
            // check if startSlide is last slide
            slider.active.last = slider.settings.startSlide == getPagerQty() - 1;
            // if video is true, set up the fitVids plugin
            if (slider.settings.video) el.fitVids();
            // set the default preload selector (visible)
            var preloadSelector = slider.children.eq(slider.settings.startSlide);
            if (slider.settings.preloadImages == "all") preloadSelector = el.children();
            // only check for control addition if not in "ticker" mode
            if (!slider.settings.ticker) {
                // if pager is requested, add it
                if (slider.settings.pager) appendPager();
                // if controls are requested, add them
                if (slider.settings.controls) appendControls();
                // if auto is true, and auto controls are requested, add them
                if (slider.settings.auto && slider.settings.autoControls) appendControlsAuto();
                // if any control option is requested, add the controls wrapper
                if (slider.viewport.find(".pagerNav").length) {
                    if (slider.settings.controls || slider.settings.autoControls || slider.settings.pager) slider.viewport.find('.pagerNav').html(slider.controls.el);
                    if (slider.viewport.find(".pagerNav").find("Div").length == 0) {
                        slider.viewport.find(".pagerNav").hide();
                    }
                } else {
                    if (slider.settings.controls || slider.settings.autoControls || slider.settings.pager) slider.viewport.after(slider.controls.el);
                }
                
            }
            // preload all images, then perform final DOM / CSS modifications that depend on images being loaded
            preloadSelector.imagesLoaded(start);
        }

        /**
		 * Start the slider
		 */
        var start = function () {
            // remove the loading DOM element
            slider.loader.remove();
            // set the left / top position of "el"
            setSlidePosition();
            // if "vertical" mode, always use adaptiveHeight to prevent odd behavior
            if (slider.settings.mode == 'vertical') slider.settings.adaptiveHeight = true;
            // set the viewport height
            slider.viewport.height(getViewportHeight());
            // make sure everything is positioned just right (same as a window resize)
            el.redrawSlider();
            // onSliderLoad callback
            slider.settings.onSliderLoad(slider.active.index);
            // slider has been fully initialized
            slider.initialized = true;
            // bind the resize call to the window
            $(window).bind('resize', resizeWindow);
            // if auto is true, start the show
            if (slider.settings.auto && slider.settings.autoStart) initAuto();
            // if ticker is true, start the ticker
            if (slider.settings.ticker) initTicker();
            // if pager is requested, make the appropriate pager link active
            if (slider.settings.pager) updatePagerActive(slider.settings.startSlide);
            // check for any updates to the controls (like hideControlOnEnd updates)
            if (slider.settings.controls) updateDirectionControls();
            // if touchEnabled is true, setup the touch events
            if (slider.settings.touchEnabled && !slider.settings.ticker) initTouch();
        }

        /**
		 * Returns the calculated height of the viewport, used to determine either adaptiveHeight or the maxHeight value
		 */
        var getViewportHeight = function () {
            var height = 0;
            // first determine which children (slides) should be used in our height calculation
            var children = $();
            // if mode is not "vertical" and adaptiveHeight is false, include all children
            if (slider.settings.mode != 'vertical' && !slider.settings.adaptiveHeight) {
                children = slider.children;
            } else {
                // if not carousel, return the single active child
                if (!slider.carousel) {
                    children = slider.children.eq(slider.active.index);
                    // if carousel, return a slice of children
                } else {
                    // get the individual slide index
                    var currentIndex = slider.settings.moveSlides == 1 ? slider.active.index : slider.active.index * getMoveBy();
                    // add the current slide to the children
                    children = slider.children.eq(currentIndex);
                    // cycle through the remaining "showing" slides
                    for (i = 1; i <= slider.settings.maxSlides - 1; i++) {
                        // if looped back to the start
                        if (currentIndex + i >= slider.children.length) {
                            children = children.add(slider.children.eq(i - 1));
                        } else {
                            children = children.add(slider.children.eq(currentIndex + i));
                        }
                    }
                }
            }
            // if "vertical" mode, calculate the sum of the heights of the children
            if (slider.settings.mode == 'vertical') {
                children.each(function (index) {
                    height += $(this).outerHeight();
                });
                // add user-supplied margins
                if (slider.settings.slideMargin > 0) {
                    height += slider.settings.slideMargin * (slider.settings.minSlides - 1);
                }
                // if not "vertical" mode, calculate the max height of the children
            } else {
                height = Math.max.apply(Math, children.map(function () {
                    return $(this).outerHeight(false);
                }).get());
            }

  
            return height;
        }

        /**
		 * Returns the calculated width to be used for the outer wrapper / viewport
		 */
        var getViewportMaxWidth = function () {
            var width = '100%';
            if (slider.settings.slideWidth > 0) {
                if (slider.settings.mode == 'horizontal') {
                    width = (slider.settings.maxSlides * slider.settings.slideWidth) + ((slider.settings.maxSlides - 1) * slider.settings.slideMargin);
                } else {
                    width = slider.settings.slideWidth;
                }
            }
            return width;
        }

        /**
		 * Returns the calculated width to be applied to each slide
		 */
        var getSlideWidth = function () {
            // start with any user-supplied slide width
            var newElWidth = slider.settings.slideWidth;
            // get the current viewport width
            var wrapWidth = slider.viewport.width();
            // if slide width was not supplied, or is larger than the viewport use the viewport width
            if (slider.settings.slideWidth == 0 ||
				(slider.settings.slideWidth > wrapWidth && !slider.carousel) ||
				slider.settings.mode == 'vertical') {
                newElWidth = wrapWidth;
                // if carousel, use the thresholds to determine the width
            } else if (slider.settings.maxSlides > 1 && slider.settings.mode == 'horizontal') {
                if (wrapWidth > slider.maxThreshold) {
                    // newElWidth = (wrapWidth - (slider.settings.slideMargin * (slider.settings.maxSlides - 1))) / slider.settings.maxSlides;
                } else if (wrapWidth < slider.minThreshold) {
                    newElWidth = (wrapWidth - (slider.settings.slideMargin * (slider.settings.minSlides - 1))) / slider.settings.minSlides;
                }
            }
            return newElWidth;
        }

        /**
		 * Returns the number of slides currently visible in the viewport (includes partially visible slides)
		 */
        var getNumberSlidesShowing = function () {
            var slidesShowing = 1;
            if (slider.settings.mode == 'horizontal' && slider.settings.slideWidth > 0) {
                // if viewport is smaller than minThreshold, return minSlides
                if (slider.viewport.width() < slider.minThreshold) {
                    slidesShowing = slider.settings.minSlides;
                    // if viewport is larger than minThreshold, return maxSlides
                } else if (slider.viewport.width() > slider.maxThreshold) {
                    slidesShowing = slider.settings.maxSlides;
                    // if viewport is between min / max thresholds, divide viewport width by first child width
                } else {
                    var childWidth = slider.children.first().width();
                    slidesShowing = Math.floor(slider.viewport.width() / childWidth);
                }
                // if "vertical" mode, slides showing will always be minSlides
            } else if (slider.settings.mode == 'vertical') {
                slidesShowing = slider.settings.minSlides;
            }
            return slidesShowing;
        }

        /**
		 * Returns the number of pages (one full viewport of slides is one "page")
		 */
        var getPagerQty = function () {
            var pagerQty = 0;
            // if moveSlides is specified by the user
            if (slider.settings.moveSlides > 0) {
                if (slider.settings.infiniteLoop) {
                    pagerQty = slider.children.length / getMoveBy();
                } else {
                    // use a while loop to determine pages
                    var breakPoint = 0;
                    var counter = 0
                    // when breakpoint goes above children length, counter is the number of pages
                    while (breakPoint < slider.children.length) {
                        ++pagerQty;
                        breakPoint = counter + getNumberSlidesShowing();
                        counter += slider.settings.moveSlides <= getNumberSlidesShowing() ? slider.settings.moveSlides : getNumberSlidesShowing();
                    }
                }
                // if moveSlides is 0 (auto) divide children length by sides showing, then round up
            } else {
                pagerQty = Math.ceil(slider.children.length / getNumberSlidesShowing());
            }
            return pagerQty;
        }

        /**
		 * Returns the number of indivual slides by which to shift the slider
		 */
        var getMoveBy = function () {
            // if moveSlides was set by the user and moveSlides is less than number of slides showing
            if (slider.settings.moveSlides > 0 && slider.settings.moveSlides <= getNumberSlidesShowing()) {
                return slider.settings.moveSlides;
            }
            // if moveSlides is 0 (auto)
            return getNumberSlidesShowing();
        }

        /**
		 * Sets the slider's (el) left or top position
		 */
        var setSlidePosition = function () {
            // if last slide, not infinite loop, and number of children is larger than specified maxSlides
            if (slider.children.length > slider.settings.maxSlides && slider.active.last && !slider.settings.infiniteLoop) {
                if (slider.settings.mode == 'horizontal') {
                    // get the last child's position
                    var lastChild = slider.children.last();
                    var position = lastChild.position();
                    // set the left position
                    setPositionProperty(-(position.left - (slider.viewport.width() - lastChild.width())), 'reset', 0);
                } else if (slider.settings.mode == 'vertical') {
                    // get the last showing index's position
                    var lastShowingIndex = slider.children.length - slider.settings.minSlides;
                    var position = slider.children.eq(lastShowingIndex).position();
                    // set the top position
                    setPositionProperty(-position.top, 'reset', 0);
                }
                // if not last slide
            } else {
                // get the position of the first showing slide
                var position = slider.children.eq(slider.active.index * getMoveBy()).position();
                // check for last slide
                if (slider.active.index == getPagerQty() - 1) slider.active.last = true;
                // set the repective position
                if (position != undefined) {
                    if (slider.settings.mode == 'horizontal') setPositionProperty(-position.left, 'reset', 0);
                    else if (slider.settings.mode == 'vertical') setPositionProperty(-position.top, 'reset', 0);
                }
            }
        }

        /**
		 * Sets the el's animating property position (which in turn will sometimes animate el).
		 * If using CSS, sets the transform property. If not using CSS, sets the top / left property.
		 *
		 * @param value (int) 
		 *  - the animating property's value
		 *
		 * @param type (string) 'slider', 'reset', 'ticker'
		 *  - the type of instance for which the function is being
		 *
		 * @param duration (int) 
		 *  - the amount of time (in ms) the transition should occupy
		 *
		 * @param params (array) optional
		 *  - an optional parameter containing any variables that need to be passed in
		 */
        var setPositionProperty = function (value, type, duration, params) {
            // use CSS transform
            if (slider.usingCSS) {
                // determine the translate3d value
                var propValue = slider.settings.mode == 'vertical' ? 'translate3d(0, ' + value + 'px, 0)' : 'translate3d(' + value + 'px, 0, 0)';
                // add the CSS transition-duration
                el.css('-' + slider.cssPrefix + '-transition-duration', duration / 1000 + 's');
                if (type == 'slide') {
                    // set the property value
                    el.css(slider.animProp, propValue);
                    // bind a callback method - executes when CSS transition completes
                    el.bind('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd', function () {
                        // unbind the callback
                        el.unbind('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd');
                        updateAfterSlideTransition();
                    });
                } else if (type == 'reset') {
                    el.css(slider.animProp, propValue);
                } else if (type == 'ticker') {
                    // make the transition use 'linear'
                    el.css('-' + slider.cssPrefix + '-transition-timing-function', 'linear');
                    el.css(slider.animProp, propValue);
                    // bind a callback method - executes when CSS transition completes
                    el.bind('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd', function () {
                        // unbind the callback
                        el.unbind('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd');
                        // reset the position
                        setPositionProperty(params['resetValue'], 'reset', 0);
                        // start the loop again
                        tickerLoop();
                    });
                }
                // use JS animate
            } else {
                var animateObj = {};
                animateObj[slider.animProp] = value;
                if (type == 'slide') {
                    el.animate(animateObj, duration, slider.settings.easing, function () {
                        updateAfterSlideTransition();
                    });
                } else if (type == 'reset') {
                    el.css(slider.animProp, value)
                } else if (type == 'ticker') {
                    el.animate(animateObj, speed, 'linear', function () {
                        setPositionProperty(params['resetValue'], 'reset', 0);
                        // run the recursive loop after animation
                        tickerLoop();
                    });
                }
            }
        }

        /**
		 * Populates the pager with proper amount of pages
		 */
        var populatePager = function () {
            var pagerHtml = '';
            pagerQty = getPagerQty();
            // loop through each pager item
            for (var i = 0; i < pagerQty; i++) {
                var linkContent = '';
                // if a buildPager function is supplied, use it to get pager link value, else use index + 1
                if (slider.settings.buildPager && $.isFunction(slider.settings.buildPager)) {
                    linkContent = slider.settings.buildPager(i);
                    slider.pagerEl.addClass('bx-custom-pager');
                } else {
                    linkContent = i + 1;
                    slider.pagerEl.addClass('bx-default-pager');
                }
                // var linkContent = slider.settings.buildPager && $.isFunction(slider.settings.buildPager) ? slider.settings.buildPager(i) : i + 1;
                // add the markup to the string
                pagerHtml += '<div class="bx-pager-item"><a href="" data-slide-index="' + i + '" class="bx-pager-link">' + linkContent + '</a></div>';
            };
            // populate the pager element with pager links
            slider.pagerEl.html(pagerHtml);
        }

        /**
		 * Appends the pager to the controls element
		 */
        var appendPager = function () {
            if (!slider.settings.pagerCustom) {
                // create the pager DOM element
                slider.pagerEl = $('<div class="bx-pager" />');
                // if a pager selector was supplied, populate it with the pager
                if (slider.settings.pagerSelector) {
                    $(slider.settings.pagerSelector).html(slider.pagerEl);
                    // if no pager selector was supplied, add it after the wrapper
                } else {
                    slider.controls.el.addClass('bx-has-pager').append(slider.pagerEl);
                }
                // populate the pager
                populatePager();
            } else {
                slider.pagerEl = $(slider.settings.pagerCustom);
            }
            // assign the pager click binding
            slider.pagerEl.delegate('a', 'click', clickPagerBind);
        }

        /**
		 * Appends prev / next controls to the controls element
		 */
        var appendControls = function () {
            slider.controls.next = $('<a class="bx-next" href="">' + slider.settings.nextText + '</a>');
            slider.controls.prev = $('<a class="bx-prev" href="">' + slider.settings.prevText + '</a>');
            // bind click actions to the controls
            slider.controls.next.bind('click', clickNextBind);
            slider.controls.prev.bind('click', clickPrevBind);
            // if nextSlector was supplied, populate it
            if (slider.settings.nextSelector) {
                $(slider.settings.nextSelector).append(slider.controls.next);
            }
            // if prevSlector was supplied, populate it
            if (slider.settings.prevSelector) {
                $(slider.settings.prevSelector).append(slider.controls.prev);
            }
            // if no custom selectors were supplied
            if (!slider.settings.nextSelector && !slider.settings.prevSelector) {
                // add the controls to the DOM
                slider.controls.directionEl = $('<div class="bx-controls-direction" />');
                // add the control elements to the directionEl
                slider.controls.directionEl.append(slider.controls.prev).append(slider.controls.next);
                // slider.viewport.append(slider.controls.directionEl);
                slider.controls.el.addClass('bx-has-controls-direction').append(slider.controls.directionEl);
            }
        }

        /**
		 * Appends start / stop auto controls to the controls element
		 */
        var appendControlsAuto = function () {
            slider.controls.start = $('<div class="bx-controls-auto-item"><a class="bx-start" href="">' + slider.settings.startText + '</a></div>');
            slider.controls.stop = $('<div class="bx-controls-auto-item"><a class="bx-stop" href="">' + slider.settings.stopText + '</a></div>');
            // add the controls to the DOM
            slider.controls.autoEl = $('<div class="bx-controls-auto" />');
            // bind click actions to the controls
            slider.controls.autoEl.delegate('.bx-start', 'click', clickStartBind);
            slider.controls.autoEl.delegate('.bx-stop', 'click', clickStopBind);
            // if autoControlsCombine, insert only the "start" control
            if (slider.settings.autoControlsCombine) {
                slider.controls.autoEl.append(slider.controls.start);
                // if autoControlsCombine is false, insert both controls
            } else {
                slider.controls.autoEl.append(slider.controls.start).append(slider.controls.stop);
            }
            // if auto controls selector was supplied, populate it with the controls
            if (slider.settings.autoControlsSelector) {
                $(slider.settings.autoControlsSelector).html(slider.controls.autoEl);
                // if auto controls selector was not supplied, add it after the wrapper
            } else {
                slider.controls.el.addClass('bx-has-controls-auto').append(slider.controls.autoEl);
            }
            // update the auto controls
            updateAutoControls(slider.settings.autoStart ? 'stop' : 'start');
        }

        /**
		 * Appends image captions to the DOM
		 */
        /*
       var appendCaptions = function(){
           // cycle through each child
           slider.children.each(function(index){
               // get the image title attribute
               var title = $(this).find('img:first').attr('title');
               // append the caption
               if (title != undefined) $(this).append('<div class="bx-caption"><span>' + title + '</span></div>');
           });
       }
       */
        var appendCaptions = function () {
            // cycle through each child
            slider.children.each(function (index) {
                // get the image title attribute
                var title = $(this).find('img:first').attr('title');
                // !! changed
                //if (title != undefined) $(this).append('<div class="bx-caption"><span>' + title + '</span></div>');
                // get the image alt attribute !! Added
                var subtitle = $(this).find("img:first").attr("alt");
                // append the caption
                if (title != undefined) $(this).append('<div class="bx-caption"><h2>' + title + '</h2><h3>' + subtitle + '</h3></div>');
            });
        }

        /**
		 * Click next binding
		 *
		 * @param e (event) 
		 *  - DOM event object
		 */
        var clickNextBind = function (e) {
            // if auto show is running, stop it
            if (slider.settings.auto) el.stopAuto();
            el.goToNextSlide();
            e.preventDefault();
        }

        /**
		 * Click prev binding
		 *
		 * @param e (event) 
		 *  - DOM event object
		 */
        var clickPrevBind = function (e) {
            // if auto show is running, stop it
            if (slider.settings.auto) el.stopAuto();
            el.goToPrevSlide();
            e.preventDefault();
        }

        /**
		 * Click start binding
		 *
		 * @param e (event) 
		 *  - DOM event object
		 */
        var clickStartBind = function (e) {
            el.startAuto();
            e.preventDefault();
        }

        /**
		 * Click stop binding
		 *
		 * @param e (event) 
		 *  - DOM event object
		 */
        var clickStopBind = function (e) {
            el.stopAuto();
            e.preventDefault();
        }

        /**
		 * Click pager binding
		 *
		 * @param e (event) 
		 *  - DOM event object
		 */
        var clickPagerBind = function (e) {
            // if auto show is running, stop it
            if (slider.settings.auto) el.stopAuto();
            var pagerLink = $(e.currentTarget);
            var pagerIndex = parseInt(pagerLink.attr('data-slide-index'));
            // if clicked pager link is not active, continue with the goToSlide call
            if (pagerIndex != slider.active.index) el.goToSlide(pagerIndex);
            e.preventDefault();
        }

        /**
		 * Updates the pager links with an active class
		 *
		 * @param slideIndex (int) 
		 *  - index of slide to make active
		 */
        var updatePagerActive = function (slideIndex) {
            // if "short" pager type
            if (slider.settings.pagerType == 'short') {
                slider.pagerEl.html((slideIndex + 1) + slider.settings.pagerShortSeparator + slider.children.length);
                return;
            }
            // remove all pager active classes
            slider.pagerEl.find('a').removeClass('active');
            slider.pagerEl.find('.bx-pager-item').removeClass('active1');

            // apply the active class for all pagers
            slider.pagerEl.each(function(i, el) {
                $(el).find('a').eq(slideIndex).addClass('active');
                $(el).find('.bx-pager-item').eq(slideIndex).addClass('active1');
            });
            if (slider.viewport.find(".pagerNav").length) {
                slider.controls.el.detach();
                slider.viewport.find('.pagerNav').eq(slideIndex).html(slider.controls.el);
                if (slider.viewport.find(".pagerNav").find("Div").length == 0) {
                    slider.viewport.find(".pagerNav").hide();
                }
            }
        }

        /**
		 * Performs needed actions after a slide transition
		 */
        var updateAfterSlideTransition = function () {
            // if infinte loop is true
            if (slider.settings.infiniteLoop) {
                var position = '';
                // first slide
                if (slider.active.index == 0) {
                    // set the new position
                    position = slider.children.eq(0).position();
                    // carousel, last slide
                } else if (slider.active.index == getPagerQty() - 1 && slider.carousel) {
                    position = slider.children.eq((getPagerQty() - 1) * getMoveBy()).position();
                    // last slide
                } else if (slider.active.index == slider.children.length - 1) {
                    position = slider.children.eq(slider.children.length - 1).position();
                }
                if (slider.settings.mode == 'horizontal') { setPositionProperty(-position.left, 'reset', 0);; }
                else if (slider.settings.mode == 'vertical') { setPositionProperty(-position.top, 'reset', 0);; }
            }
            // declare that the transition is complete
            slider.working = false;
            // onSlideAfter callback
            slider.settings.onSlideAfter(slider.children.eq(slider.active.index), slider.oldIndex, slider.active.index);
        }

        /**
		 * Updates the auto controls state (either active, or combined switch)
		 *
		 * @param state (string) "start", "stop"
		 *  - the new state of the auto show
		 */
        var updateAutoControls = function (state) {
            // if autoControlsCombine is true, replace the current control with the new state 
            if (slider.settings.autoControlsCombine) {
                slider.controls.autoEl.html(slider.controls[state]);
                // if autoControlsCombine is false, apply the "active" class to the appropriate control 
            } else {
                slider.controls.autoEl.find('a').removeClass('active');
                slider.controls.autoEl.find('a:not(.bx-' + state + ')').addClass('active');
            }
        }

        /**
		 * Updates the direction controls (checks if either should be hidden)
		 */
        var updateDirectionControls = function () {
            // if infiniteLoop is false and hideControlOnEnd is true
            if (!slider.settings.infiniteLoop && slider.settings.hideControlOnEnd) {
                // if first slide
                if (slider.active.index == 0) {
                    slider.controls.prev.addClass('disabled');
                    slider.controls.next.removeClass('disabled');
                    // if last slide
                } else if (slider.active.index == getPagerQty() - 1) {
                    slider.controls.next.addClass('disabled');
                    slider.controls.prev.removeClass('disabled');
                    // if any slide in the middle
                } else {
                    slider.controls.prev.removeClass('disabled');
                    slider.controls.next.removeClass('disabled');
                }
                // if slider has only one page, disable controls
            } else if (getPagerQty() == 1) {
                slider.controls.prev.addClass('disabled');
                slider.controls.next.addClass('disabled');
            }
        }

        /**
		 * Initialzes the auto process
		 */
        var initAuto = function () {
            // if autoDelay was supplied, launch the auto show using a setTimeout() call
            if (slider.settings.autoDelay > 0) {
                var timeout = setTimeout(el.startAuto, slider.settings.autoDelay);
                // if autoDelay was not supplied, start the auto show normally
            } else {
                el.startAuto();
            }
            // if autoHover is requested
            if (slider.settings.autoHover) {
                // on el hover
                el.hover(function () {
                    // if the auto show is currently playing (has an active interval)
                    if (slider.interval) {
                        // stop the auto show and pass true agument which will prevent control update
                        el.stopAuto(true);
                        // create a new autoPaused value which will be used by the relative "mouseout" event
                        slider.autoPaused = true;
                    }
                }, function () {
                    // if the autoPaused value was created be the prior "mouseover" event
                    if (slider.autoPaused) {
                        // start the auto show and pass true agument which will prevent control update
                        el.startAuto(true);
                        // reset the autoPaused value
                        slider.autoPaused = null;
                    }
                });
            }
        }

        /**
		 * Initialzes the ticker process
		 */
        var initTicker = function () {
            var startPosition = 0;
            // if autoDirection is "next", append a clone of the entire slider
            if (slider.settings.autoDirection == 'next') {
                el.append(slider.children.clone().addClass('bx-clone'));
                // if autoDirection is "prev", prepend a clone of the entire slider, and set the left position
            } else {
                el.prepend(slider.children.clone().addClass('bx-clone'));
                var position = slider.children.first().position();
                startPosition = slider.settings.mode == 'horizontal' ? -position.left : -position.top;
            }
            setPositionProperty(startPosition, 'reset', 0);
            // do not allow controls in ticker mode
            slider.settings.pager = false;
            slider.settings.controls = false;
            slider.settings.autoControls = false;
            // if autoHover is requested
            if (slider.settings.tickerHover && !slider.usingCSS) {
                // on el hover
                slider.viewport.hover(function () {
                    el.stop();
                }, function () {
                    // calculate the total width of children (used to calculate the speed ratio)
                    var totalDimens = 0;
                    slider.children.each(function (index) {
                        totalDimens += slider.settings.mode == 'horizontal' ? $(this).outerWidth(true) : $(this).outerHeight(true);
                    });
                    // calculate the speed ratio (used to determine the new speed to finish the paused animation)
                    var ratio = slider.settings.speed / totalDimens;
                    // determine which property to use
                    var property = slider.settings.mode == 'horizontal' ? 'left' : 'top';
                    // calculate the new speed
                    var newSpeed = ratio * (totalDimens - (Math.abs(parseInt(el.css(property)))));
                    tickerLoop(newSpeed);
                });
            }
            // start the ticker loop
            tickerLoop();
        }

        /**
		 * Runs a continuous loop, news ticker-style
		 */
        var tickerLoop = function (resumeSpeed) {
            speed = resumeSpeed ? resumeSpeed : slider.settings.speed;
            var position = { left: 0, top: 0 };
            var reset = { left: 0, top: 0 };
            // if "next" animate left position to last child, then reset left to 0
            if (slider.settings.autoDirection == 'next') {
                position = el.find('.bx-clone').first().position();
                // if "prev" animate left position to 0, then reset left to first non-clone child
            } else {
                reset = slider.children.first().position();
            }
            var animateProperty = slider.settings.mode == 'horizontal' ? -position.left : -position.top;
            var resetValue = slider.settings.mode == 'horizontal' ? -reset.left : -reset.top;
            var params = { resetValue: resetValue };
            setPositionProperty(animateProperty, 'ticker', speed, params);
        }

        /**
		 * Initializes touch events
		 */
        var initTouch = function () {
            // initialize object to contain all touch values
            slider.touch = {
                start: { x: 0, y: 0 },
                end: { x: 0, y: 0 }
            }
            slider.viewport.bind('touchstart', onTouchStart);
        }

        /**
		 * Event handler for "touchstart"
		 *
		 * @param e (event) 
		 *  - DOM event object
		 */
        var onTouchStart = function (e) {
            if (slider.working) {
                e.preventDefault();
            } else {
                // record the original position when touch starts
                slider.touch.originalPos = el.position();
                var orig = e.originalEvent;
                // record the starting touch x, y coordinates
                slider.touch.start.x = orig.changedTouches[0].pageX;
                slider.touch.start.y = orig.changedTouches[0].pageY;
                // bind a "touchmove" event to the viewport
                slider.viewport.bind('touchmove', onTouchMove);
                // bind a "touchend" event to the viewport
                slider.viewport.bind('touchend', onTouchEnd);
            }
        }

        /**
		 * Event handler for "touchmove"
		 *
		 * @param e (event) 
		 *  - DOM event object
		 */
        var onTouchMove = function (e) {
            var orig = e.originalEvent;
            // if scrolling on y axis, do not prevent default
            var xMovement = Math.abs(orig.changedTouches[0].pageX - slider.touch.start.x);
            var yMovement = Math.abs(orig.changedTouches[0].pageY - slider.touch.start.y);
            // x axis swipe
            if ((xMovement * 3) > yMovement && slider.settings.preventDefaultSwipeX) {
                e.preventDefault();
                // y axis swipe
            } else if ((yMovement * 3) > xMovement && slider.settings.preventDefaultSwipeY) {
                e.preventDefault();
            }
            if (slider.settings.mode != 'fade' && slider.settings.oneToOneTouch) {
                var value = 0;
                // if horizontal, drag along x axis
                if (slider.settings.mode == 'horizontal') {
                    var change = orig.changedTouches[0].pageX - slider.touch.start.x;
                    value = slider.touch.originalPos.left + change;
                    // if vertical, drag along y axis
                } else {
                    var change = orig.changedTouches[0].pageY - slider.touch.start.y;
                    value = slider.touch.originalPos.top + change;
                }
                setPositionProperty(value, 'reset', 0);
            }
        }

        /**
		 * Event handler for "touchend"
		 *
		 * @param e (event) 
		 *  - DOM event object
		 */
        var onTouchEnd = function (e) {
            slider.viewport.unbind('touchmove', onTouchMove);
            var orig = e.originalEvent;
            var value = 0;
            // record end x, y positions
            slider.touch.end.x = orig.changedTouches[0].pageX;
            slider.touch.end.y = orig.changedTouches[0].pageY;
            // if fade mode, check if absolute x distance clears the threshold
            if (slider.settings.mode == 'fade') {
                var distance = Math.abs(slider.touch.start.x - slider.touch.end.x);
                if (distance >= slider.settings.swipeThreshold) {
                    slider.touch.start.x > slider.touch.end.x ? el.goToNextSlide() : el.goToPrevSlide();
                    el.stopAuto();
                }
                // not fade mode
            } else {
                var distance = 0;
                // calculate distance and el's animate property
                if (slider.settings.mode == 'horizontal') {
                    distance = slider.touch.end.x - slider.touch.start.x;
                    value = slider.touch.originalPos.left;
                } else {
                    distance = slider.touch.end.y - slider.touch.start.y;
                    value = slider.touch.originalPos.top;
                }
                // if not infinite loop and first / last slide, do not attempt a slide transition
                if (!slider.settings.infiniteLoop && ((slider.active.index == 0 && distance > 0) || (slider.active.last && distance < 0))) {
                    setPositionProperty(value, 'reset', 200);
                } else {
                    // check if distance clears threshold
                    if (Math.abs(distance) >= slider.settings.swipeThreshold) {
                        distance < 0 ? el.goToNextSlide() : el.goToPrevSlide();
                        el.stopAuto();
                    } else {
                        // el.animate(property, 200);
                        setPositionProperty(value, 'reset', 200);
                    }
                }
            }
            slider.viewport.unbind('touchend', onTouchEnd);
        }

        /**
		 * Window resize event callback
		 */
        var resizeWindow = function (e) {
            // get the new window dimens (again, thank you IE)
            var windowWidthNew = $(window).width();
            var windowHeightNew = $(window).height();
            // make sure that it is a true window resize
            // *we must check this because our dinosaur friend IE fires a window resize event when certain DOM elements
            // are resized. Can you just die already?*
            if (windowWidth != windowWidthNew || windowHeight != windowHeightNew) {
                // set the new window dimens
                windowWidth = windowWidthNew;
                windowHeight = windowHeightNew;
                // update all dynamic elements
                el.redrawSlider();
            }
        }

        /**
		 * ===================================================================================
		 * = PUBLIC FUNCTIONS
		 * ===================================================================================
		 */

        /**
		 * Performs slide transition to the specified slide
		 *
		 * @param slideIndex (int) 
		 *  - the destination slide's index (zero-based)
		 *
		 * @param direction (string) 
		 *  - INTERNAL USE ONLY - the direction of travel ("prev" / "next")
		 */
        el.goToSlide = function (slideIndex, direction) {
            // if plugin is currently in motion, ignore request
            if (slider.working || slider.active.index == slideIndex) return;
            // declare that plugin is in motion
            slider.working = true;
            // store the old index
            slider.oldIndex = slider.active.index;
            // if slideIndex is less than zero, set active index to last child (this happens during infinite loop)
            if (slideIndex < 0) {
                slider.active.index = getPagerQty() - 1;
                // if slideIndex is greater than children length, set active index to 0 (this happens during infinite loop)
            } else if (slideIndex >= getPagerQty()) {
                slider.active.index = 0;
                // set active index to requested slide
            } else {
                slider.active.index = slideIndex;
            }
            // onSlideBefore, onSlideNext, onSlidePrev callbacks
            slider.settings.onSlideBefore(slider.children.eq(slider.active.index), slider.oldIndex, slider.active.index);
            if (direction == 'next') {
                slider.settings.onSlideNext(slider.children.eq(slider.active.index), slider.oldIndex, slider.active.index);
            } else if (direction == 'prev') {
                slider.settings.onSlidePrev(slider.children.eq(slider.active.index), slider.oldIndex, slider.active.index);
            }
            // check if last slide
            slider.active.last = slider.active.index >= getPagerQty() - 1;
            // update the pager with active class
            if (slider.settings.pager) updatePagerActive(slider.active.index);
            // // check for direction control update
            if (slider.settings.controls) updateDirectionControls();
            // if slider is set to mode: "fade"
            if (slider.settings.mode == 'fade') {
                // if adaptiveHeight is true and next height is different from current height, animate to the new height
                if (slider.settings.adaptiveHeight && slider.viewport.height() != getViewportHeight()) {
                    slider.viewport.animate({ height: getViewportHeight() }, slider.settings.adaptiveHeightSpeed);
                }
                // fade out the visible child and reset its z-index value
                slider.children.filter(':visible').fadeOut(slider.settings.speed).css({ zIndex: 0 });
                // fade in the newly requested slide
                slider.children.eq(slider.active.index).css('zIndex', 51).fadeIn(slider.settings.speed, function () {
                    $(this).css('zIndex', 50);
                    updateAfterSlideTransition();
                });
                // slider mode is not "fade"
            } else {
                // if adaptiveHeight is true and next height is different from current height, animate to the new height
                if (slider.settings.adaptiveHeight && slider.viewport.height() != getViewportHeight()) {
                    slider.viewport.animate({ height: getViewportHeight() }, slider.settings.adaptiveHeightSpeed);
                }
                var moveBy = 0;
                var position = { left: 0, top: 0 };
                // if carousel and not infinite loop
                if (!slider.settings.infiniteLoop && slider.carousel && slider.active.last) {
                    if (slider.settings.mode == 'horizontal') {
                        // get the last child position
                        var lastChild = slider.children.eq(slider.children.length - 1);
                        position = lastChild.position();
                        // calculate the position of the last slide
                        moveBy = slider.viewport.width() - lastChild.width();
                    } else {
                        // get last showing index position
                        var lastShowingIndex = slider.children.length - slider.settings.minSlides;
                        position = slider.children.eq(lastShowingIndex).position();
                    }
                    // horizontal carousel, going previous while on first slide (infiniteLoop mode)
                } else if (slider.carousel && slider.active.last && direction == 'prev') {
                    // get the last child position
                    var eq = slider.settings.moveSlides == 1 ? slider.settings.maxSlides - getMoveBy() : ((getPagerQty() - 1) * getMoveBy()) - (slider.children.length - slider.settings.maxSlides);
                    var lastChild = el.children('.bx-clone').eq(eq);
                    position = lastChild.position();
                    // if infinite loop and "Next" is clicked on the last slide
                } else if (direction == 'next' && slider.active.index == 0) {
                    // get the last clone position
                    position = el.find('.bx-clone').eq(slider.settings.maxSlides).position();
                    slider.active.last = false;
                    // normal non-zero requests
                } else if (slideIndex >= 0) {
                    var requestEl = slideIndex * getMoveBy();
                    position = slider.children.eq(requestEl).position();
                }
                // plugin values to be animated
                var value = slider.settings.mode == 'horizontal' ? -(position.left - moveBy) : -position.top;
                setPositionProperty(value, 'slide', slider.settings.speed);
            }
        }

        /**
		 * Transitions to the next slide in the show
		 */
        el.goToNextSlide = function () {
            // if infiniteLoop is false and last page is showing, disregard call
            if (!slider.settings.infiniteLoop && slider.active.last) return;
            var pagerIndex = parseInt(slider.active.index) + 1;
            el.goToSlide(pagerIndex, 'next');
        }

        /**
		 * Transitions to the prev slide in the show
		 */
        el.goToPrevSlide = function () {
            // if infiniteLoop is false and last page is showing, disregard call
            if (!slider.settings.infiniteLoop && slider.active.index == 0) return;
            var pagerIndex = parseInt(slider.active.index) - 1;
            el.goToSlide(pagerIndex, 'prev');
        }

        /**
		 * Starts the auto show
		 *
		 * @param preventControlUpdate (boolean) 
		 *  - if true, auto controls state will not be updated
		 */
        el.startAuto = function (preventControlUpdate) {
            // if an interval already exists, disregard call
            if (slider.interval) return;
            // create an interval
            slider.interval = setInterval(function () {
                slider.settings.autoDirection == 'next' ? el.goToNextSlide() : el.goToPrevSlide();
            }, slider.settings.pause);
            // if auto controls are displayed and preventControlUpdate is not true
            if (slider.settings.autoControls && preventControlUpdate != true) updateAutoControls('stop');
        }

        /**
		 * Stops the auto show
		 *
		 * @param preventControlUpdate (boolean) 
		 *  - if true, auto controls state will not be updated
		 */
        el.stopAuto = function (preventControlUpdate) {
            // if no interval exists, disregard call
            if (!slider.interval) return;
            // clear the interval
            clearInterval(slider.interval);
            slider.interval = null;
            // if auto controls are displayed and preventControlUpdate is not true
            if (slider.settings.autoControls && preventControlUpdate != true) updateAutoControls('start');
        }

        /**
		 * Returns current slide index (zero-based)
		 */
        el.getCurrentSlide = function () {
            return slider.active.index;
        }

        /**
		 * Returns number of slides in show
		 */
        el.getSlideCount = function () {
            return slider.children.length;
        }

        /**
		 * Update all dynamic slider elements
		 */
        el.redrawSlider = function () {
            // resize all children in ratio to new screen size
            slider.children.add(el.find('.bx-clone')).width(getSlideWidth());
            // adjust the height
            slider.viewport.css('height', getViewportHeight());
            // update the slide position
            if (!slider.settings.ticker) setSlidePosition();
            // if active.last was true before the screen resize, we want
            // to keep it last no matter what screen size we end on
            if (slider.active.last) slider.active.index = getPagerQty() - 1;
            // if the active index (page) no longer exists due to the resize, simply set the index as last
            if (slider.active.index >= getPagerQty()) slider.active.last = true;
            // if a pager is being displayed and a custom pager is not being used, update it
            if (slider.settings.pager && !slider.settings.pagerCustom) {
                populatePager();
                updatePagerActive(slider.active.index);
            }
        }

        /**
		 * Destroy the current instance of the slider (revert everything back to original state)
		 */
        el.destroySlider = function () {
            // don't do anything if slider has already been destroyed
            if (!slider.initialized) return;
            slider.initialized = false;
            $('.bx-clone', this).remove();
            slider.children.removeAttr('style');
            this.removeAttr('style').unwrap().unwrap();
            if (slider.controls.el) slider.controls.el.remove();
            if (slider.controls.next) slider.controls.next.remove();
            if (slider.controls.prev) slider.controls.prev.remove();
            if (slider.pagerEl) slider.pagerEl.remove();
            $('.bx-caption', this).remove();
            if (slider.controls.autoEl) slider.controls.autoEl.remove();
            clearInterval(slider.interval);
            $(window).unbind('resize', resizeWindow);
        }

        /**
		 * Reload the slider (revert all DOM changes, and re-initialize)
		 */
        el.reloadSlider = function (settings) {
            if (settings != undefined) options = settings;
            el.destroySlider();
            init();
        }

        init();

        // returns the current jQuery object
        return this;
    }

})(jQuery);

/*!
 * jQuery imagesLoaded plugin v2.1.0
 * http://github.com/desandro/imagesloaded
 *
 * MIT License. by Paul Irish et al.
 */

/*jshint curly: true, eqeqeq: true, noempty: true, strict: true, undef: true, browser: true */
/*global jQuery: false */

(function (c, n) {
    var l = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw=="; c.fn.imagesLoaded = function (f) {
        function m() { var b = c(i), a = c(h); d && (h.length ? d.reject(e, b, a) : d.resolve(e)); c.isFunction(f) && f.call(g, e, b, a) } function j(b, a) { b.src === l || -1 !== c.inArray(b, k) || (k.push(b), a ? h.push(b) : i.push(b), c.data(b, "imagesLoaded", { isBroken: a, src: b.src }), o && d.notifyWith(c(b), [a, e, c(i), c(h)]), e.length === k.length && (setTimeout(m), e.unbind(".imagesLoaded"))) } var g = this, d = c.isFunction(c.Deferred) ? c.Deferred() :
        0, o = c.isFunction(d.notify), e = g.find("img").add(g.filter("img")), k = [], i = [], h = []; c.isPlainObject(f) && c.each(f, function (b, a) { if ("callback" === b) f = a; else if (d) d[b](a) }); e.length ? e.bind("load.imagesLoaded error.imagesLoaded", function (b) { j(b.target, "error" === b.type) }).each(function (b, a) { var d = a.src, e = c.data(a, "imagesLoaded"); if (e && e.src === d) j(a, e.isBroken); else if (a.complete && a.naturalWidth !== n) j(a, 0 === a.naturalWidth || 0 === a.naturalHeight); else if (a.readyState || a.complete) a.src = l, a.src = d }) : m(); return d ? d.promise(g) :
        g
    }
})(jQuery);






/*! Copyright (c) 2011 Brandon Aaron (http://brandonaaron.net)
 * Licensed under the MIT License (LICENSE.txt).
 *
 * Thanks to: http://adomas.org/javascript-mouse-wheel/ for some pointers.
 * Thanks to: Mathias Bank(http://www.mathias-bank.de) for a scope bug fix.
 * Thanks to: Seamus Leahy for adding deltaX and deltaY
 *
 * Version: 3.0.6
 * 
 * Requires: 1.2.2+
 */

(function ($) {

    var types = ['DOMMouseScroll', 'mousewheel'];

    if ($.event.fixHooks) {
        for (var i = types.length; i;) {
            $.event.fixHooks[types[--i]] = $.event.mouseHooks;
        }
    }

    $.event.special.mousewheel = {
        setup: function () {
            if (this.addEventListener) {
                for (var i = types.length; i;) {
                    this.addEventListener(types[--i], handler, false);
                }
            } else {
                this.onmousewheel = handler;
            }
        },

        teardown: function () {
            if (this.removeEventListener) {
                for (var i = types.length; i;) {
                    this.removeEventListener(types[--i], handler, false);
                }
            } else {
                this.onmousewheel = null;
            }
        }
    };

    $.fn.extend({
        mousewheel: function (fn) {
            return fn ? this.bind("mousewheel", fn) : this.trigger("mousewheel");
        },

        unmousewheel: function (fn) {
            return this.unbind("mousewheel", fn);
        }
    });


    function handler(event) {
        var orgEvent = event || window.event, args = [].slice.call(arguments, 1), delta = 0, returnValue = true, deltaX = 0, deltaY = 0;
        event = $.event.fix(orgEvent);
        event.type = "mousewheel";

        // Old school scrollwheel delta
        if (orgEvent.wheelDelta) { delta = orgEvent.wheelDelta / 120; }
        if (orgEvent.detail) { delta = -orgEvent.detail / 3; }

        // New school multidimensional scroll (touchpads) deltas
        deltaY = delta;

        // Gecko
        if (orgEvent.axis !== undefined && orgEvent.axis === orgEvent.HORIZONTAL_AXIS) {
            deltaY = 0;
            deltaX = -1 * delta;
        }

        // Webkit
        if (orgEvent.wheelDeltaY !== undefined) { deltaY = orgEvent.wheelDeltaY / 120; }
        if (orgEvent.wheelDeltaX !== undefined) { deltaX = -1 * orgEvent.wheelDeltaX / 120; }

        // Add event and delta to the front of the arguments
        args.unshift(event, delta, deltaX, deltaY);

        return ($.event.dispatch || $.event.handle).apply(this, args);
    }

})(jQuery);


/*
 * jScrollPane - v2.0.0beta12 - 2012-09-27
 * http://jscrollpane.kelvinluck.com/
 *
 * Copyright (c) 2010 Kelvin Luck
 * Dual licensed under the MIT or GPL licenses.
 */
(function (b, a, c) {
    b.fn.jScrollPane = function (e) {
        function d(D, O) {
            var ay, Q = this, Y, aj, v, al, T, Z, y, q, az, aE, au, i, I, h, j, aa, U, ap, X, t, A, aq, af, am, G, l, at, ax, x, av, aH, f, L, ai = true, P = true, aG = false, k = false, ao = D.clone(false, false).empty(), ac = b.fn.mwheelIntent ? "http://www.drhorton.com/sites/com/design/js/mwheelIntent.jsp" : "http://www.drhorton.com/sites/com/design/js/mousewheel.jsp"; aH = D.css("paddingTop") + " " + D.css("paddingRight") + " " + D.css("paddingBottom") + " " + D.css("paddingLeft"); f = (parseInt(D.css("paddingLeft"), 10) || 0) + (parseInt(D.css("paddingRight"), 10) || 0); function ar(aQ) { var aL, aN, aM, aJ, aI, aP, aO = false, aK = false; ay = aQ; if (Y === c) { aI = D.scrollTop(); aP = D.scrollLeft(); D.css({ overflow: "hidden", padding: 0 }); aj = D.innerWidth() + f; v = D.innerHeight(); D.width(aj); Y = b('<div class="jspPane" />').css("padding", aH).append(D.children()); al = b('<div class="jspContainer" />').css({ width: aj + "px", height: v + "px" }).append(Y).appendTo(D) } else { D.css("width", ""); aO = ay.stickToBottom && K(); aK = ay.stickToRight && B(); aJ = D.innerWidth() + f != aj || D.outerHeight() != v; if (aJ) { aj = D.innerWidth() + f; v = D.innerHeight(); al.css({ width: aj + "px", height: v + "px" }) } if (!aJ && L == T && Y.outerHeight() == Z) { D.width(aj); return } L = T; Y.css("width", ""); D.width(aj); al.find(">.jspVerticalBar,>.jspHorizontalBar").remove().end() } Y.css("overflow", "auto"); if (aQ.contentWidth) { T = aQ.contentWidth } else { T = Y[0].scrollWidth } Z = Y[0].scrollHeight; Y.css("overflow", ""); y = T / aj; q = Z / v; az = q > 1; aE = y > 1; if (!(aE || az)) { D.removeClass("jspScrollable"); Y.css({ top: 0, width: al.width() - f }); n(); E(); R(); w() } else { D.addClass("jspScrollable"); aL = ay.maintainPosition && (I || aa); if (aL) { aN = aC(); aM = aA() } aF(); z(); F(); if (aL) { N(aK ? (T - aj) : aN, false); M(aO ? (Z - v) : aM, false) } J(); ag(); an(); if (ay.enableKeyboardNavigation) { S() } if (ay.clickOnTrack) { p() } C(); if (ay.hijackInternalLinks) { m() } } if (ay.autoReinitialise && !av) { av = setInterval(function () { ar(ay) }, ay.autoReinitialiseDelay) } else { if (!ay.autoReinitialise && av) { clearInterval(av) } } aI && D.scrollTop(0) && M(aI, false); aP && D.scrollLeft(0) && N(aP, false); D.trigger("jsp-initialised", [aE || az]) } function aF() { if (az) { al.append(b('<div class="jspVerticalBar" />').append(b('<div class="jspCap jspCapTop" />'), b('<div class="jspTrack" />').append(b('<div class="jspDrag" />').append(b('<div class="jspDragTop" />'), b('<div class="jspDragBottom" />'))), b('<div class="jspCap jspCapBottom" />'))); U = al.find(">.jspVerticalBar"); ap = U.find(">.jspTrack"); au = ap.find(">.jspDrag"); if (ay.showArrows) { aq = b('<a class="jspArrow jspArrowUp" />').bind("http://www.drhorton.com/sites/com/design/js/mousedown.jsp", aD(0, -1)).bind("http://www.drhorton.com/sites/com/design/js/click.jsp", aB); af = b('<a class="jspArrow jspArrowDown" />').bind("http://www.drhorton.com/sites/com/design/js/mousedown.jsp", aD(0, 1)).bind("http://www.drhorton.com/sites/com/design/js/click.jsp", aB); if (ay.arrowScrollOnHover) { aq.bind("http://www.drhorton.com/sites/com/design/js/mouseover.jsp", aD(0, -1, aq)); af.bind("http://www.drhorton.com/sites/com/design/js/mouseover.jsp", aD(0, 1, af)) } ak(ap, ay.verticalArrowPositions, aq, af) } t = v; al.find(">.jspVerticalBar>.jspCap:visible,>.jspVerticalBar>.jspArrow").each(function () { t -= b(this).outerHeight() }); au.hover(function () { au.addClass("jspHover") }, function () { au.removeClass("jspHover") }).bind("http://www.drhorton.com/sites/com/design/js/mousedown.jsp", function (aI) { b("html").bind("http://www.drhorton.com/sites/com/design/js/dragstart.jsp selectstart.jsp", aB); au.addClass("jspActive"); var s = aI.pageY - au.position().top; b("html").bind("http://www.drhorton.com/sites/com/design/js/mousemove.jsp", function (aJ) { V(aJ.pageY - s, false) }).bind("http://www.drhorton.com/sites/com/design/js/mouseup.jsp mouseleave.jsp", aw); return false }); o() } } function o() { ap.height(t + "px"); I = 0; X = ay.verticalGutter + ap.outerWidth(); Y.width(aj - X - f); try { if (U.position().left === 0) { Y.css("margin-left", X + "px") } } catch (s) { } } function z() {
                if (aE) {
                    al.append(b('<div class="jspHorizontalBar" />').append(b('<div class="jspCap jspCapLeft" />'), b('<div class="jspTrack" />').append(b('<div class="jspDrag" />').append(b('<div class="jspDragLeft" />'), b('<div class="jspDragRight" />'))), b('<div class="jspCap jspCapRight" />'))); am = al.find(">.jspHorizontalBar"); G = am.find(">.jspTrack"); h = G.find(">.jspDrag"); if (ay.showArrows) {
                        ax = b('<a class="jspArrow jspArrowLeft" />').bind("http://www.drhorton.com/sites/com/design/js/mousedown.jsp", aD(-1, 0)).bind("http://www.drhorton.com/sites/com/design/js/click.jsp", aB); x = b('<a class="jspArrow jspArrowRight" />').bind("http://www.drhorton.com/sites/com/design/js/mousedown.jsp", aD(1, 0)).bind("http://www.drhorton.com/sites/com/design/js/click.jsp", aB);
                        if (ay.arrowScrollOnHover) { ax.bind("http://www.drhorton.com/sites/com/design/js/mouseover.jsp", aD(-1, 0, ax)); x.bind("http://www.drhorton.com/sites/com/design/js/mouseover.jsp", aD(1, 0, x)) } ak(G, ay.horizontalArrowPositions, ax, x)
                    } h.hover(function () { h.addClass("jspHover") }, function () { h.removeClass("jspHover") }).bind("http://www.drhorton.com/sites/com/design/js/mousedown.jsp", function (aI) { b("html").bind("http://www.drhorton.com/sites/com/design/js/dragstart.jsp selectstart.jsp", aB); h.addClass("jspActive"); var s = aI.pageX - h.position().left; b("html").bind("http://www.drhorton.com/sites/com/design/js/mousemove.jsp", function (aJ) { W(aJ.pageX - s, false) }).bind("http://www.drhorton.com/sites/com/design/js/mouseup.jsp mouseleave.jsp", aw); return false }); l = al.innerWidth(); ah()
                }
            } function ah() { al.find(">.jspHorizontalBar>.jspCap:visible,>.jspHorizontalBar>.jspArrow").each(function () { l -= b(this).outerWidth() }); G.width(l + "px"); aa = 0 } function F() { if (aE && az) { var aI = G.outerHeight(), s = ap.outerWidth(); t -= aI; b(am).find(">.jspCap:visible,>.jspArrow").each(function () { l += b(this).outerWidth() }); l -= s; v -= s; aj -= aI; G.parent().append(b('<div class="jspCorner" />').css("width", aI + "px")); o(); ah() } if (aE) { Y.width((al.outerWidth() - f) + "px") } Z = Y.outerHeight(); q = Z / v; if (aE) { at = Math.ceil(1 / y * l); if (at > ay.horizontalDragMaxWidth) { at = ay.horizontalDragMaxWidth } else { if (at < ay.horizontalDragMinWidth) { at = ay.horizontalDragMinWidth } } h.width(at + "px"); j = l - at; ae(aa) } if (az) { A = Math.ceil(1 / q * t); if (A > ay.verticalDragMaxHeight) { A = ay.verticalDragMaxHeight } else { if (A < ay.verticalDragMinHeight) { A = ay.verticalDragMinHeight } } au.height(A + "px"); i = t - A; ad(I) } } function ak(aJ, aL, aI, s) { var aN = "before", aK = "after", aM; if (aL == "os") { aL = /Mac/.test(navigator.platform) ? "after" : "split" } if (aL == aN) { aK = aL } else { if (aL == aK) { aN = aL; aM = aI; aI = s; s = aM } } aJ[aN](aI)[aK](s) } function aD(aI, s, aJ) { return function () { H(aI, s, this, aJ); this.blur(); return false } } function H(aL, aK, aO, aN) { aO = b(aO).addClass("jspActive"); var aM, aJ, aI = true, s = function () { if (aL !== 0) { Q.scrollByX(aL * ay.arrowButtonSpeed) } if (aK !== 0) { Q.scrollByY(aK * ay.arrowButtonSpeed) } aJ = setTimeout(s, aI ? ay.initialDelay : ay.arrowRepeatFreq); aI = false }; s(); aM = aN ? "http://www.drhorton.com/sites/com/design/js/mouseout.jsp" : "http://www.drhorton.com/sites/com/design/js/mouseup.jsp"; aN = aN || b("html"); aN.bind(aM, function () { aO.removeClass("jspActive"); aJ && clearTimeout(aJ); aJ = null; aN.unbind(aM) }) } function p() { w(); if (az) { ap.bind("http://www.drhorton.com/sites/com/design/js/mousedown.jsp", function (aN) { if (aN.originalTarget === c || aN.originalTarget == aN.currentTarget) { var aL = b(this), aO = aL.offset(), aM = aN.pageY - aO.top - I, aJ, aI = true, s = function () { var aR = aL.offset(), aS = aN.pageY - aR.top - A / 2, aP = v * ay.scrollPagePercent, aQ = i * aP / (Z - v); if (aM < 0) { if (I - aQ > aS) { Q.scrollByY(-aP) } else { V(aS) } } else { if (aM > 0) { if (I + aQ < aS) { Q.scrollByY(aP) } else { V(aS) } } else { aK(); return } } aJ = setTimeout(s, aI ? ay.initialDelay : ay.trackClickRepeatFreq); aI = false }, aK = function () { aJ && clearTimeout(aJ); aJ = null; b(document).unbind("http://www.drhorton.com/sites/com/design/js/mouseup.jsp", aK) }; s(); b(document).bind("http://www.drhorton.com/sites/com/design/js/mouseup.jsp", aK); return false } }) } if (aE) { G.bind("http://www.drhorton.com/sites/com/design/js/mousedown.jsp", function (aN) { if (aN.originalTarget === c || aN.originalTarget == aN.currentTarget) { var aL = b(this), aO = aL.offset(), aM = aN.pageX - aO.left - aa, aJ, aI = true, s = function () { var aR = aL.offset(), aS = aN.pageX - aR.left - at / 2, aP = aj * ay.scrollPagePercent, aQ = j * aP / (T - aj); if (aM < 0) { if (aa - aQ > aS) { Q.scrollByX(-aP) } else { W(aS) } } else { if (aM > 0) { if (aa + aQ < aS) { Q.scrollByX(aP) } else { W(aS) } } else { aK(); return } } aJ = setTimeout(s, aI ? ay.initialDelay : ay.trackClickRepeatFreq); aI = false }, aK = function () { aJ && clearTimeout(aJ); aJ = null; b(document).unbind("http://www.drhorton.com/sites/com/design/js/mouseup.jsp", aK) }; s(); b(document).bind("http://www.drhorton.com/sites/com/design/js/mouseup.jsp", aK); return false } }) } } function w() { if (G) { G.unbind("http://www.drhorton.com/sites/com/design/js/mousedown.jsp") } if (ap) { ap.unbind("http://www.drhorton.com/sites/com/design/js/mousedown.jsp") } } function aw() { b("html").unbind("http://www.drhorton.com/sites/com/design/js/dragstart.jsp selectstart.jsp mousemove.jsp mouseup.jsp mouseleave.jsp"); if (au) { au.removeClass("jspActive") } if (h) { h.removeClass("jspActive") } } function V(s, aI) { if (!az) { return } if (s < 0) { s = 0 } else { if (s > i) { s = i } } if (aI === c) { aI = ay.animateScroll } if (aI) { Q.animate(au, "top", s, ad) } else { au.css("top", s); ad(s) } } function ad(aI) { if (aI === c) { aI = au.position().top } al.scrollTop(0); I = aI; var aL = I === 0, aJ = I == i, aK = aI / i, s = -aK * (Z - v); if (ai != aL || aG != aJ) { ai = aL; aG = aJ; D.trigger("jsp-arrow-change", [ai, aG, P, k]) } u(aL, aJ); Y.css("top", s); D.trigger("jsp-scroll-y", [-s, aL, aJ]).trigger("scroll") } function W(aI, s) {
                if (!aE) { return } if (aI < 0) { aI = 0 } else { if (aI > j) { aI = j } } if (s === c) { s = ay.animateScroll } if (s) {
                    Q.animate(h, "left", aI, ae)
                } else { h.css("left", aI); ae(aI) }
            } function ae(aI) { if (aI === c) { aI = h.position().left } al.scrollTop(0); aa = aI; var aL = aa === 0, aK = aa == j, aJ = aI / j, s = -aJ * (T - aj); if (P != aL || k != aK) { P = aL; k = aK; D.trigger("jsp-arrow-change", [ai, aG, P, k]) } r(aL, aK); Y.css("left", s); D.trigger("jsp-scroll-x", [-s, aL, aK]).trigger("scroll") } function u(aI, s) { if (ay.showArrows) { aq[aI ? "addClass" : "removeClass"]("jspDisabled"); af[s ? "addClass" : "removeClass"]("jspDisabled") } } function r(aI, s) { if (ay.showArrows) { ax[aI ? "addClass" : "removeClass"]("jspDisabled"); x[s ? "addClass" : "removeClass"]("jspDisabled") } } function M(s, aI) { var aJ = s / (Z - v); V(aJ * i, aI) } function N(aI, s) { var aJ = aI / (T - aj); W(aJ * j, s) } function ab(aV, aQ, aJ) { var aN, aK, aL, s = 0, aU = 0, aI, aP, aO, aS, aR, aT; try { aN = b(aV) } catch (aM) { return } aK = aN.outerHeight(); aL = aN.outerWidth(); al.scrollTop(0); al.scrollLeft(0); while (!aN.is(".jspPane")) { s += aN.position().top; aU += aN.position().left; aN = aN.offsetParent(); if (/^body|html$/i.test(aN[0].nodeName)) { return } } aI = aA(); aO = aI + v; if (s < aI || aQ) { aR = s - ay.verticalGutter } else { if (s + aK > aO) { aR = s - v + aK + ay.verticalGutter } } if (aR) { M(aR, aJ) } aP = aC(); aS = aP + aj; if (aU < aP || aQ) { aT = aU - ay.horizontalGutter } else { if (aU + aL > aS) { aT = aU - aj + aL + ay.horizontalGutter } } if (aT) { N(aT, aJ) } } function aC() { return -Y.position().left } function aA() { return -Y.position().top } function K() { var s = Z - v; return (s > 20) && (s - aA() < 10) } function B() { var s = T - aj; return (s > 20) && (s - aC() < 10) } function ag() { al.unbind(ac).bind(ac, function (aL, aM, aK, aI) { var aJ = aa, s = I; Q.scrollBy(aK * ay.mouseWheelSpeed, -aI * ay.mouseWheelSpeed, false); return aJ == aa && s == I }) } function n() { al.unbind(ac) } function aB() { return false } function J() { Y.find(":input,a").unbind("http://www.drhorton.com/sites/com/design/js/focus.jsp").bind("http://www.drhorton.com/sites/com/design/js/focus.jsp", function (s) { ab(s.target, false) }) } function E() { Y.find(":input,a").unbind("http://www.drhorton.com/sites/com/design/js/focus.jsp") } function S() { var s, aI, aK = []; aE && aK.push(am[0]); az && aK.push(U[0]); Y.focus(function () { D.focus() }); D.attr("tabindex", 0).unbind("http://www.drhorton.com/sites/com/design/js/keydown.jsp keypress.jsp").bind("http://www.drhorton.com/sites/com/design/js/keydown.jsp", function (aN) { if (aN.target !== this && !(aK.length && b(aN.target).closest(aK).length)) { return } var aM = aa, aL = I; switch (aN.keyCode) { case 40: case 38: case 34: case 32: case 33: case 39: case 37: s = aN.keyCode; aJ(); break; case 35: M(Z - v); s = null; break; case 36: M(0); s = null; break } aI = aN.keyCode == s && aM != aa || aL != I; return !aI }).bind("http://www.drhorton.com/sites/com/design/js/keypress.jsp", function (aL) { if (aL.keyCode == s) { aJ() } return !aI }); if (ay.hideFocus) { D.css("outline", "none"); if ("hideFocus" in al[0]) { D.attr("hideFocus", true) } } else { D.css("outline", ""); if ("hideFocus" in al[0]) { D.attr("hideFocus", false) } } function aJ() { var aM = aa, aL = I; switch (s) { case 40: Q.scrollByY(ay.keyboardSpeed, false); break; case 38: Q.scrollByY(-ay.keyboardSpeed, false); break; case 34: case 32: Q.scrollByY(v * ay.scrollPagePercent, false); break; case 33: Q.scrollByY(-v * ay.scrollPagePercent, false); break; case 39: Q.scrollByX(ay.keyboardSpeed, false); break; case 37: Q.scrollByX(-ay.keyboardSpeed, false); break } aI = aM != aa || aL != I; return aI } } function R() { D.attr("tabindex", "-1").removeAttr("tabindex").unbind("http://www.drhorton.com/sites/com/design/js/keydown.jsp keypress.jsp") } function C() { if (location.hash && location.hash.length > 1) { var aK, aI, aJ = escape(location.hash.substr(1)); try { aK = b("#" + aJ + ', a[name="' + aJ + '"]') } catch (s) { return } if (aK.length && Y.find(aJ)) { if (al.scrollTop() === 0) { aI = setInterval(function () { if (al.scrollTop() > 0) { ab(aK, true); b(document).scrollTop(al.position().top); clearInterval(aI) } }, 50) } else { ab(aK, true); b(document).scrollTop(al.position().top) } } } } function m() {
                if (b(document.body).data("jspHijack")) { return } b(document.body).data("jspHijack", true); b(document.body).delegate("a[href*=#]", "click", function (s) {
                    var aI = this.href.substr(0, this.href.indexOf("#")), aK = location.href, aO, aP, aJ, aM, aL, aN; if (location.href.indexOf("#") !== -1) { aK = location.href.substr(0, location.href.indexOf("#")) } if (aI !== aK) { return } aO = escape(this.href.substr(this.href.indexOf("#") + 1)); aP; try { aP = b("#" + aO + ', a[name="' + aO + '"]') } catch (aQ) { return } if (!aP.length) { return } aJ = aP.closest(".jspScrollable"); aM = aJ.data("jsp"); aM.scrollToElement(aP, true); if (aJ[0].scrollIntoView) { aL = b(a).scrollTop(); aN = aP.offset().top; if (aN < aL || aN > aL + b(a).height()) { aJ[0].scrollIntoView() } } s.preventDefault()
                })
            } function an() { var aJ, aI, aL, aK, aM, s = false; al.unbind("touchstart.jsp touchmove.jsp touchend.jsp click.jsp-touchclick").bind("http://www.drhorton.com/sites/com/design/js/touchstart.jsp", function (aN) { var aO = aN.originalEvent.touches[0]; aJ = aC(); aI = aA(); aL = aO.pageX; aK = aO.pageY; aM = false; s = true }).bind("http://www.drhorton.com/sites/com/design/js/touchmove.jsp", function (aQ) { if (!s) { return } var aP = aQ.originalEvent.touches[0], aO = aa, aN = I; Q.scrollTo(aJ + aL - aP.pageX, aI + aK - aP.pageY); aM = aM || Math.abs(aL - aP.pageX) > 5 || Math.abs(aK - aP.pageY) > 5; return aO == aa && aN == I }).bind("http://www.drhorton.com/sites/com/design/js/touchend.jsp", function (aN) { s = false }).bind("click.jsp-touchclick", function (aN) { if (aM) { aM = false; return false } }) } function g() { var s = aA(), aI = aC(); D.removeClass("jspScrollable").unbind(".jsp"); D.replaceWith(ao.append(Y.children())); ao.scrollTop(s); ao.scrollLeft(aI); if (av) { clearInterval(av) } } b.extend(Q, { reinitialise: function (aI) { aI = b.extend({}, ay, aI); ar(aI) }, scrollToElement: function (aJ, aI, s) { ab(aJ, aI, s) }, scrollTo: function (aJ, s, aI) { N(aJ, aI); M(s, aI) }, scrollToX: function (aI, s) { N(aI, s) }, scrollToY: function (s, aI) { M(s, aI) }, scrollToPercentX: function (aI, s) { N(aI * (T - aj), s) }, scrollToPercentY: function (aI, s) { M(aI * (Z - v), s) }, scrollBy: function (aI, s, aJ) { Q.scrollByX(aI, aJ); Q.scrollByY(s, aJ) }, scrollByX: function (s, aJ) { var aI = aC() + Math[s < 0 ? "floor" : "ceil"](s), aK = aI / (T - aj); W(aK * j, aJ) }, scrollByY: function (s, aJ) { var aI = aA() + Math[s < 0 ? "floor" : "ceil"](s), aK = aI / (Z - v); V(aK * i, aJ) }, positionDragX: function (s, aI) { W(s, aI) }, positionDragY: function (aI, s) { V(aI, s) }, animate: function (aI, aL, s, aK) { var aJ = {}; aJ[aL] = s; aI.animate(aJ, { duration: ay.animateDuration, easing: ay.animateEase, queue: false, step: aK }) }, getContentPositionX: function () { return aC() }, getContentPositionY: function () { return aA() }, getContentWidth: function () { return T }, getContentHeight: function () { return Z }, getPercentScrolledX: function () { return aC() / (T - aj) }, getPercentScrolledY: function () { return aA() / (Z - v) }, getIsScrollableH: function () { return aE }, getIsScrollableV: function () { return az }, getContentPane: function () { return Y }, scrollToBottom: function (s) { V(i, s) }, hijackInternalLinks: b.noop, destroy: function () { g() } }); ar(O)
        } e = b.extend({}, b.fn.jScrollPane.defaults, e); b.each(["mouseWheelSpeed", "arrowButtonSpeed", "trackClickSpeed", "keyboardSpeed"], function () { e[this] = e[this] || e.speed }); return this.each(function () { var f = b(this), g = f.data("jsp"); if (g) { g.reinitialise(e) } else { b("script", f).filter('[type="text/javascript"],:not([type])').remove(); g = new d(f, e); f.data("jsp", g) } })
    }; b.fn.jScrollPane.defaults = { showArrows: false, maintainPosition: true, stickToBottom: false, stickToRight: false, clickOnTrack: true, autoReinitialise: false, autoReinitialiseDelay: 500, verticalDragMinHeight: 0, verticalDragMaxHeight: 99999, horizontalDragMinWidth: 0, horizontalDragMaxWidth: 99999, contentWidth: c, animateScroll: false, animateDuration: 300, animateEase: "linear", hijackInternalLinks: false, verticalGutter: 4, horizontalGutter: 4, mouseWheelSpeed: 0, arrowButtonSpeed: 0, arrowRepeatFreq: 50, arrowScrollOnHover: false, trackClickSpeed: 0, trackClickRepeatFreq: 70, verticalArrowPositions: "split", horizontalArrowPositions: "split", enableKeyboardNavigation: true, hideFocus: false, keyboardSpeed: 0, initialDelay: 300, speed: 30, scrollPagePercent: 0.8 }
})(jQuery, this);


/* 
 * jQuery - Collapser - Plugin v1.0
 * http://www.aakashweb.com/
 * Copyright 2010, Aakash Chakravarthy
 * Released under the MIT License.
 */

(function ($) {
    $.fn.collapser = function (options, beforeCallback, afterCallback) {
        var defaults = { target: 'next', targetOnly: null, effect: 'slide', changeText: true, expandHtml: 'Expand', collapseHtml: 'Collapse', expandClass: '', collapseClass: '' }; var options = $.extend(defaults, options); var expHtml, collHtml, effectShow, effectHide; if (options.effect == 'slide') { effectShow = 'slideDown'; effectHide = 'slideUp'; } else { effectShow = 'fadeIn'; effectHide = 'fadeOut'; } if (options.changeText == true) { expHtml = options.expandHtml; collHtml = options.collapseHtml; } function callBeforeCallback(obj) { if (beforeCallback !== undefined) { beforeCallback.apply(obj); } } function callAfterCallback(obj) { if (afterCallback !== undefined) { afterCallback.apply(obj); } } function hideElement(obj, method) { callBeforeCallback(obj); if (method == 1) { obj[options.target](options.targetOnly)[effectHide](); obj.html(expHtml); obj.removeClass(options.collapseClass); obj.addClass(options.expandClass); } else { $(document).find(options.target)[effectHide](); obj.html(expHtml); obj.removeClass(options.collapseClass); obj.addClass(options.expandClass); } callAfterCallback(obj); } function showElement(obj, method) {
            callBeforeCallback(obj)
            if (method == 1) { obj[options.target](options.targetOnly)[effectShow](); obj.html(collHtml); obj.removeClass(options.expandClass); obj.addClass(options.collapseClass); } else { $(document).find(options.target)[effectShow](); obj.html(collHtml); obj.removeClass(options.expandClass); obj.addClass(options.collapseClass); } callAfterCallback(obj);
        } function toggleElement(obj, method) { if (method == 1) { if (obj[options.target](options.targetOnly).is(':visible')) { hideElement(obj, 1); } else { showElement(obj, 1); } } else { if ($(document).find(options.target).is(':visible')) { hideElement(obj, 2); } else { showElement(obj, 2); } } } return this.each(function () { if ($.fn[options.target] && $(this)[options.target]()) { $(this).toggle(function () { toggleElement($(this), 1); }, function () { toggleElement($(this), 1); }); } else { $(this).toggle(function () { toggleElement($(this), 2); }, function () { toggleElement($(this), 2); }); } if ($.fn[options.target] && $(this)[options.target]()) { if ($(this)[options.target]().is(':hidden')) { $(this).html(expHtml); $(this).removeClass(options.collapseClass); $(this).addClass(options.expandClass); } else { $(this).html(collHtml); $(this).removeClass(options.expandClass); $(this).addClass(options.collapseClass); } } else { if ($(document).find(options.target).is(':hidden')) { $(this).html(expHtml); } else { $(this).html(collHtml); } } });
    };
})(jQuery);