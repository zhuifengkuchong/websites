var results = new Array();

// JavaScript Document
$(function () {
   
    $(".autosearch").autocomplete({
        source: function (request, response) {
            var pricemin = $('#min').val();
            var pricemax = $('#max').val();
            
          

            $.ajax({
                url: "/Sites/Com/Coveo/CoveoAutoSuggestHandler.ashx?pricemin=" + pricemin + "&pricemax=" + pricemax ,
                dataType: "jsonp",
                data: {
                    featureClass: "P",
                    style: "full",
                    maxRows: 12,
                    name_startsWith: request.term
                },
                success: function (data) {
                    response(
                        $.map(data.results, function (item) {
				            return {
				                label: item.value,
				                value: item.value
				            };
                        }),
                        $.map(data.results, function (item) {
                            var result = new NameValueTypeAlts(item.name, item.value, item.type, item.alt);
                            results.push(result);                            
                        })

                    );

                },
            });
        },
        minLength: 1,
        html: true,
        //select: function (event, ui) {
        //    var selection = ui.item.value;
        //    for (var i = 0; i < results.length; i++) {
        //        var result = results[i];
        //        if (selection.toLowerCase() == result.Value.toLowerCase()) {
        //            jQuery('#resulttype').val(result.Type);
        //            jQuery('#resultname').val(result.Name);
        //            break;
        //        }
                
        //    }
        //},
        open: function () {
            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
        },
        close: function () {
            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        }
    });
});


function handlesearch() {
    if (event.which == 13 || event.keyCode == 13) {
        setsearch();
        document.getElementById('searchBtn').click();
    } else {

        document.getElementById('resultname').value = '';
        document.getElementById('resulttype').value = '';
    }
}

function setsearch() {
    var selection = jQuery('#searchtextbox').val();
    for (var i = 0; i < results.length; i++) {
        var result = results[i];
        if (selection.toLowerCase() == result.Value.toLowerCase() || selection.toLowerCase() == result.Alt.toLowerCase()) {
            jQuery('#resulttype').val(result.Type);
            jQuery('#resultname').val(result.Name);
            break;
        }

    }
}


/*
 * jQuery UI Autocomplete HTML Extension
 *
 * Copyright 2010, Scott Gonz�lez (http://scottgonzalez.com)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 *
 * http://github.com/scottgonzalez/jquery-ui-extensions
 */
//(function ($) {

//    var proto = $.ui.autocomplete.prototype,
//        initSource = proto._initSource;

//    function filter(array, term) {
//        var matcher = new RegExp($.ui.autocomplete.escapeRegex(term), "i");
//        return $.grep(array, function (value) {
//            return matcher.test($("<div>").html(value.label || value.value || value).text());
//        });
//    }

//    $.extend(proto, {
//        _initSource: function () {
//            if (this.options.html && $.isArray(this.options.source)) {
//                this.source = function (request, response) {
//                    response(filter(this.options.source, request.term));
//                };
//            } else {
//                initSource.call(this);
//            }
//        },

//        _renderItem: function (ul, item) {
//            return $("<li></li>")
//                .data("item.autocomplete", item)
//                .append($("<span></span>")[this.options.html ? "html" : "text"](item.label))
//                .appendTo(ul);
//        }
//    });

//})(jQuery);



function NameValueTypeAlts(n, v, t,a) {
    this.Name = n;
    this.Value = v;
    this.Type = t;
    this.Alt = a;
}

