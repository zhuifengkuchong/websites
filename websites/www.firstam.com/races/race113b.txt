<script id = "race113b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race113={};
	myVars.races.race113.varName="Lu_Id_area_34__onmouseout";
	myVars.races.race113.varType="@varType@";
	myVars.races.race113.repairType = "@RepairType";
	myVars.races.race113.event1={};
	myVars.races.race113.event2={};
	myVars.races.race113.event1.id = "Lu_Id_area_34";
	myVars.races.race113.event1.type = "onmouseout";
	myVars.races.race113.event1.loc = "Lu_Id_area_34_LOC";
	myVars.races.race113.event1.isRead = "True";
	myVars.races.race113.event1.eventType = "@event1EventType@";
	myVars.races.race113.event2.id = "Lu_DOM";
	myVars.races.race113.event2.type = "onDOMContentLoaded";
	myVars.races.race113.event2.loc = "Lu_DOM_LOC";
	myVars.races.race113.event2.isRead = "False";
	myVars.races.race113.event2.eventType = "@event2EventType@";
	myVars.races.race113.event1.executed= false;// true to disable, false to enable
	myVars.races.race113.event2.executed= false;// true to disable, false to enable
</script>

