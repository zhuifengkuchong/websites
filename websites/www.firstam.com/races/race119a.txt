<script id = "race119a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race119={};
	myVars.races.race119.varName="Lu_Id_area_40__onmouseout";
	myVars.races.race119.varType="@varType@";
	myVars.races.race119.repairType = "@RepairType";
	myVars.races.race119.event1={};
	myVars.races.race119.event2={};
	myVars.races.race119.event1.id = "Lu_DOM";
	myVars.races.race119.event1.type = "onDOMContentLoaded";
	myVars.races.race119.event1.loc = "Lu_DOM_LOC";
	myVars.races.race119.event1.isRead = "False";
	myVars.races.race119.event1.eventType = "@event1EventType@";
	myVars.races.race119.event2.id = "Lu_Id_area_40";
	myVars.races.race119.event2.type = "onmouseout";
	myVars.races.race119.event2.loc = "Lu_Id_area_40_LOC";
	myVars.races.race119.event2.isRead = "True";
	myVars.races.race119.event2.eventType = "@event2EventType@";
	myVars.races.race119.event1.executed= false;// true to disable, false to enable
	myVars.races.race119.event2.executed= false;// true to disable, false to enable
</script>

