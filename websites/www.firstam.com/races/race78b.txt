<script id = "race78b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race78={};
	myVars.races.race78.varName="btnContact__onmouseout";
	myVars.races.race78.varType="@varType@";
	myVars.races.race78.repairType = "@RepairType";
	myVars.races.race78.event1={};
	myVars.races.race78.event2={};
	myVars.races.race78.event1.id = "btnContact";
	myVars.races.race78.event1.type = "onmouseout";
	myVars.races.race78.event1.loc = "btnContact_LOC";
	myVars.races.race78.event1.isRead = "True";
	myVars.races.race78.event1.eventType = "@event1EventType@";
	myVars.races.race78.event2.id = "Lu_DOM";
	myVars.races.race78.event2.type = "onDOMContentLoaded";
	myVars.races.race78.event2.loc = "Lu_DOM_LOC";
	myVars.races.race78.event2.isRead = "False";
	myVars.races.race78.event2.eventType = "@event2EventType@";
	myVars.races.race78.event1.executed= false;// true to disable, false to enable
	myVars.races.race78.event2.executed= false;// true to disable, false to enable
</script>

