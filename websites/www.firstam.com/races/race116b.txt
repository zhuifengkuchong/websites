<script id = "race116b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race116={};
	myVars.races.race116.varName="Lu_Id_area_37__onmouseout";
	myVars.races.race116.varType="@varType@";
	myVars.races.race116.repairType = "@RepairType";
	myVars.races.race116.event1={};
	myVars.races.race116.event2={};
	myVars.races.race116.event1.id = "Lu_Id_area_37";
	myVars.races.race116.event1.type = "onmouseout";
	myVars.races.race116.event1.loc = "Lu_Id_area_37_LOC";
	myVars.races.race116.event1.isRead = "True";
	myVars.races.race116.event1.eventType = "@event1EventType@";
	myVars.races.race116.event2.id = "Lu_DOM";
	myVars.races.race116.event2.type = "onDOMContentLoaded";
	myVars.races.race116.event2.loc = "Lu_DOM_LOC";
	myVars.races.race116.event2.isRead = "False";
	myVars.races.race116.event2.eventType = "@event2EventType@";
	myVars.races.race116.event1.executed= false;// true to disable, false to enable
	myVars.races.race116.event2.executed= false;// true to disable, false to enable
</script>

