<script id = "race192b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race192={};
	myVars.races.race192.varName="Object[1432].triggered";
	myVars.races.race192.varType="@varType@";
	myVars.races.race192.repairType = "@RepairType";
	myVars.races.race192.event1={};
	myVars.races.race192.event2={};
	myVars.races.race192.event1.id = "Lu_Id_a_20";
	myVars.races.race192.event1.type = "onmouseup";
	myVars.races.race192.event1.loc = "Lu_Id_a_20_LOC";
	myVars.races.race192.event1.isRead = "False";
	myVars.races.race192.event1.eventType = "@event1EventType@";
	myVars.races.race192.event2.id = "Lu_Id_li_38";
	myVars.races.race192.event2.type = "onmouseover";
	myVars.races.race192.event2.loc = "Lu_Id_li_38_LOC";
	myVars.races.race192.event2.isRead = "True";
	myVars.races.race192.event2.eventType = "@event2EventType@";
	myVars.races.race192.event1.executed= false;// true to disable, false to enable
	myVars.races.race192.event2.executed= false;// true to disable, false to enable
</script>

