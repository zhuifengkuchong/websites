<script id = "race158b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race158={};
	myVars.races.race158.varName="Object[1432].triggered";
	myVars.races.race158.varType="@varType@";
	myVars.races.race158.repairType = "@RepairType";
	myVars.races.race158.event1={};
	myVars.races.race158.event2={};
	myVars.races.race158.event1.id = "Lu_Id_a_20";
	myVars.races.race158.event1.type = "onmouseup";
	myVars.races.race158.event1.loc = "Lu_Id_a_20_LOC";
	myVars.races.race158.event1.isRead = "False";
	myVars.races.race158.event1.eventType = "@event1EventType@";
	myVars.races.race158.event2.id = "Lu_Id_li_14";
	myVars.races.race158.event2.type = "onmouseout";
	myVars.races.race158.event2.loc = "Lu_Id_li_14_LOC";
	myVars.races.race158.event2.isRead = "True";
	myVars.races.race158.event2.eventType = "@event2EventType@";
	myVars.races.race158.event1.executed= false;// true to disable, false to enable
	myVars.races.race158.event2.executed= false;// true to disable, false to enable
</script>

