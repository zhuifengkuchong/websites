<script id = "race54a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race54={};
	myVars.races.race54.varName="Lu_DOM__onmouseout";
	myVars.races.race54.varType="@varType@";
	myVars.races.race54.repairType = "@RepairType";
	myVars.races.race54.event1={};
	myVars.races.race54.event2={};
	myVars.races.race54.event1.id = "Lu_window";
	myVars.races.race54.event1.type = "onload";
	myVars.races.race54.event1.loc = "Lu_window_LOC";
	myVars.races.race54.event1.isRead = "False";
	myVars.races.race54.event1.eventType = "@event1EventType@";
	myVars.races.race54.event2.id = "paddle-left";
	myVars.races.race54.event2.type = "onmouseout";
	myVars.races.race54.event2.loc = "paddle-left_LOC";
	myVars.races.race54.event2.isRead = "True";
	myVars.races.race54.event2.eventType = "@event2EventType@";
	myVars.races.race54.event1.executed= false;// true to disable, false to enable
	myVars.races.race54.event2.executed= false;// true to disable, false to enable
</script>

