<script id = "race217a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race217={};
	myVars.races.race217.varName="Object[1432].triggered";
	myVars.races.race217.varType="@varType@";
	myVars.races.race217.repairType = "@RepairType";
	myVars.races.race217.event1={};
	myVars.races.race217.event2={};
	myVars.races.race217.event1.id = "Lu_Id_a_21";
	myVars.races.race217.event1.type = "onmouseup";
	myVars.races.race217.event1.loc = "Lu_Id_a_21_LOC";
	myVars.races.race217.event1.isRead = "False";
	myVars.races.race217.event1.eventType = "@event1EventType@";
	myVars.races.race217.event2.id = "Lu_Id_a_22";
	myVars.races.race217.event2.type = "onmouseup";
	myVars.races.race217.event2.loc = "Lu_Id_a_22_LOC";
	myVars.races.race217.event2.isRead = "True";
	myVars.races.race217.event2.eventType = "@event2EventType@";
	myVars.races.race217.event1.executed= false;// true to disable, false to enable
	myVars.races.race217.event2.executed= false;// true to disable, false to enable
</script>

