<script id = "race219b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race219={};
	myVars.races.race219.varName="Object[1432].triggered";
	myVars.races.race219.varType="@varType@";
	myVars.races.race219.repairType = "@RepairType";
	myVars.races.race219.event1={};
	myVars.races.race219.event2={};
	myVars.races.race219.event1.id = "Lu_Id_a_23";
	myVars.races.race219.event1.type = "onmouseup";
	myVars.races.race219.event1.loc = "Lu_Id_a_23_LOC";
	myVars.races.race219.event1.isRead = "True";
	myVars.races.race219.event1.eventType = "@event1EventType@";
	myVars.races.race219.event2.id = "Lu_Id_a_22";
	myVars.races.race219.event2.type = "onmouseup";
	myVars.races.race219.event2.loc = "Lu_Id_a_22_LOC";
	myVars.races.race219.event2.isRead = "False";
	myVars.races.race219.event2.eventType = "@event2EventType@";
	myVars.races.race219.event1.executed= false;// true to disable, false to enable
	myVars.races.race219.event2.executed= false;// true to disable, false to enable
</script>

