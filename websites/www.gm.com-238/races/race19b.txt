<script id = "race19b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race19={};
	myVars.races.race19.varName="Lu_DOM__onmouseover";
	myVars.races.race19.varType="@varType@";
	myVars.races.race19.repairType = "@RepairType";
	myVars.races.race19.event1={};
	myVars.races.race19.event2={};
	myVars.races.race19.event1.id = "Lu_Id_li_32";
	myVars.races.race19.event1.type = "onmouseover";
	myVars.races.race19.event1.loc = "Lu_Id_li_32_LOC";
	myVars.races.race19.event1.isRead = "True";
	myVars.races.race19.event1.eventType = "@event1EventType@";
	myVars.races.race19.event2.id = "Lu_window";
	myVars.races.race19.event2.type = "onload";
	myVars.races.race19.event2.loc = "Lu_window_LOC";
	myVars.races.race19.event2.isRead = "False";
	myVars.races.race19.event2.eventType = "@event2EventType@";
	myVars.races.race19.event1.executed= false;// true to disable, false to enable
	myVars.races.race19.event2.executed= false;// true to disable, false to enable
</script>

