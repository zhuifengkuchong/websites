<script id = "race155a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race155={};
	myVars.races.race155.varName="Object[1432].triggered";
	myVars.races.race155.varType="@varType@";
	myVars.races.race155.repairType = "@RepairType";
	myVars.races.race155.event1={};
	myVars.races.race155.event2={};
	myVars.races.race155.event1.id = "Lu_Id_li_17";
	myVars.races.race155.event1.type = "onmouseout";
	myVars.races.race155.event1.loc = "Lu_Id_li_17_LOC";
	myVars.races.race155.event1.isRead = "True";
	myVars.races.race155.event1.eventType = "@event1EventType@";
	myVars.races.race155.event2.id = "Lu_Id_a_20";
	myVars.races.race155.event2.type = "onmouseup";
	myVars.races.race155.event2.loc = "Lu_Id_a_20_LOC";
	myVars.races.race155.event2.isRead = "False";
	myVars.races.race155.event2.eventType = "@event2EventType@";
	myVars.races.race155.event1.executed= false;// true to disable, false to enable
	myVars.races.race155.event2.executed= false;// true to disable, false to enable
</script>

