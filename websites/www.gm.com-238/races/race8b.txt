<script id = "race8b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race8={};
	myVars.races.race8.varName="Lu_DOM__onmouseover";
	myVars.races.race8.varType="@varType@";
	myVars.races.race8.repairType = "@RepairType";
	myVars.races.race8.event1={};
	myVars.races.race8.event2={};
	myVars.races.race8.event1.id = "paddle-right";
	myVars.races.race8.event1.type = "onmouseover";
	myVars.races.race8.event1.loc = "paddle-right_LOC";
	myVars.races.race8.event1.isRead = "True";
	myVars.races.race8.event1.eventType = "@event1EventType@";
	myVars.races.race8.event2.id = "Lu_window";
	myVars.races.race8.event2.type = "onload";
	myVars.races.race8.event2.loc = "Lu_window_LOC";
	myVars.races.race8.event2.isRead = "False";
	myVars.races.race8.event2.eventType = "@event2EventType@";
	myVars.races.race8.event1.executed= false;// true to disable, false to enable
	myVars.races.race8.event2.executed= false;// true to disable, false to enable
</script>

