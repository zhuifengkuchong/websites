<script id = "race163b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race163={};
	myVars.races.race163.varName="Object[1432].triggered";
	myVars.races.race163.varType="@varType@";
	myVars.races.race163.repairType = "@RepairType";
	myVars.races.race163.event1={};
	myVars.races.race163.event2={};
	myVars.races.race163.event1.id = "Lu_Id_a_20";
	myVars.races.race163.event1.type = "onmouseup";
	myVars.races.race163.event1.loc = "Lu_Id_a_20_LOC";
	myVars.races.race163.event1.isRead = "False";
	myVars.races.race163.event1.eventType = "@event1EventType@";
	myVars.races.race163.event2.id = "Lu_Id_li_36";
	myVars.races.race163.event2.type = "onmouseout";
	myVars.races.race163.event2.loc = "Lu_Id_li_36_LOC";
	myVars.races.race163.event2.isRead = "True";
	myVars.races.race163.event2.eventType = "@event2EventType@";
	myVars.races.race163.event1.executed= false;// true to disable, false to enable
	myVars.races.race163.event2.executed= false;// true to disable, false to enable
</script>

