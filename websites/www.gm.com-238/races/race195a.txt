<script id = "race195a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race195={};
	myVars.races.race195.varName="Object[1432].triggered";
	myVars.races.race195.varType="@varType@";
	myVars.races.race195.repairType = "@RepairType";
	myVars.races.race195.event1={};
	myVars.races.race195.event2={};
	myVars.races.race195.event1.id = "Lu_Id_li_35";
	myVars.races.race195.event1.type = "onmouseover";
	myVars.races.race195.event1.loc = "Lu_Id_li_35_LOC";
	myVars.races.race195.event1.isRead = "True";
	myVars.races.race195.event1.eventType = "@event1EventType@";
	myVars.races.race195.event2.id = "Lu_Id_a_20";
	myVars.races.race195.event2.type = "onmouseup";
	myVars.races.race195.event2.loc = "Lu_Id_a_20_LOC";
	myVars.races.race195.event2.isRead = "False";
	myVars.races.race195.event2.eventType = "@event2EventType@";
	myVars.races.race195.event1.executed= false;// true to disable, false to enable
	myVars.races.race195.event2.executed= false;// true to disable, false to enable
</script>

