<script id = "race201a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race201={};
	myVars.races.race201.varName="Object[1432].triggered";
	myVars.races.race201.varType="@varType@";
	myVars.races.race201.repairType = "@RepairType";
	myVars.races.race201.event1={};
	myVars.races.race201.event2={};
	myVars.races.race201.event1.id = "Lu_Id_li_29";
	myVars.races.race201.event1.type = "onmouseover";
	myVars.races.race201.event1.loc = "Lu_Id_li_29_LOC";
	myVars.races.race201.event1.isRead = "True";
	myVars.races.race201.event1.eventType = "@event1EventType@";
	myVars.races.race201.event2.id = "Lu_Id_a_20";
	myVars.races.race201.event2.type = "onmouseup";
	myVars.races.race201.event2.loc = "Lu_Id_a_20_LOC";
	myVars.races.race201.event2.isRead = "False";
	myVars.races.race201.event2.eventType = "@event2EventType@";
	myVars.races.race201.event1.executed= false;// true to disable, false to enable
	myVars.races.race201.event2.executed= false;// true to disable, false to enable
</script>

