<script id = "race233a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race233={};
	myVars.races.race233.varName="Object[1432].triggered";
	myVars.races.race233.varType="@varType@";
	myVars.races.race233.repairType = "@RepairType";
	myVars.races.race233.event1={};
	myVars.races.race233.event2={};
	myVars.races.race233.event1.id = "Lu_Id_a_29";
	myVars.races.race233.event1.type = "onmouseup";
	myVars.races.race233.event1.loc = "Lu_Id_a_29_LOC";
	myVars.races.race233.event1.isRead = "False";
	myVars.races.race233.event1.eventType = "@event1EventType@";
	myVars.races.race233.event2.id = "paddle-left";
	myVars.races.race233.event2.type = "onmousedown";
	myVars.races.race233.event2.loc = "paddle-left_LOC";
	myVars.races.race233.event2.isRead = "True";
	myVars.races.race233.event2.eventType = "@event2EventType@";
	myVars.races.race233.event1.executed= false;// true to disable, false to enable
	myVars.races.race233.event2.executed= false;// true to disable, false to enable
</script>

