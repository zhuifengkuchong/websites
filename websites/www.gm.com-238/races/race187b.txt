<script id = "race187b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race187={};
	myVars.races.race187.varName="Object[1432].triggered";
	myVars.races.race187.varType="@varType@";
	myVars.races.race187.repairType = "@RepairType";
	myVars.races.race187.event1={};
	myVars.races.race187.event2={};
	myVars.races.race187.event1.id = "Lu_Id_a_20";
	myVars.races.race187.event1.type = "onmouseup";
	myVars.races.race187.event1.loc = "Lu_Id_a_20_LOC";
	myVars.races.race187.event1.isRead = "False";
	myVars.races.race187.event1.eventType = "@event1EventType@";
	myVars.races.race187.event2.id = "Lu_Id_li_16";
	myVars.races.race187.event2.type = "onmouseover";
	myVars.races.race187.event2.loc = "Lu_Id_li_16_LOC";
	myVars.races.race187.event2.isRead = "True";
	myVars.races.race187.event2.eventType = "@event2EventType@";
	myVars.races.race187.event1.executed= false;// true to disable, false to enable
	myVars.races.race187.event2.executed= false;// true to disable, false to enable
</script>

