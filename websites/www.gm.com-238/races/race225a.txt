<script id = "race225a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race225={};
	myVars.races.race225.varName="Object[1432].triggered";
	myVars.races.race225.varType="@varType@";
	myVars.races.race225.repairType = "@RepairType";
	myVars.races.race225.event1={};
	myVars.races.race225.event2={};
	myVars.races.race225.event1.id = "Lu_Id_a_25";
	myVars.races.race225.event1.type = "onmouseup";
	myVars.races.race225.event1.loc = "Lu_Id_a_25_LOC";
	myVars.races.race225.event1.isRead = "False";
	myVars.races.race225.event1.eventType = "@event1EventType@";
	myVars.races.race225.event2.id = "Lu_Id_a_26";
	myVars.races.race225.event2.type = "onmouseup";
	myVars.races.race225.event2.loc = "Lu_Id_a_26_LOC";
	myVars.races.race225.event2.isRead = "True";
	myVars.races.race225.event2.eventType = "@event2EventType@";
	myVars.races.race225.event1.executed= false;// true to disable, false to enable
	myVars.races.race225.event2.executed= false;// true to disable, false to enable
</script>

