<script id = "race218a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race218={};
	myVars.races.race218.varName="Function[32001].elem";
	myVars.races.race218.varType="@varType@";
	myVars.races.race218.repairType = "@RepairType";
	myVars.races.race218.event1={};
	myVars.races.race218.event2={};
	myVars.races.race218.event1.id = "Lu_window";
	myVars.races.race218.event1.type = "onload";
	myVars.races.race218.event1.loc = "Lu_window_LOC";
	myVars.races.race218.event1.isRead = "False";
	myVars.races.race218.event1.eventType = "@event1EventType@";
	myVars.races.race218.event2.id = "Lu_Id_a_22";
	myVars.races.race218.event2.type = "onmouseup";
	myVars.races.race218.event2.loc = "Lu_Id_a_22_LOC";
	myVars.races.race218.event2.isRead = "True";
	myVars.races.race218.event2.eventType = "@event2EventType@";
	myVars.races.race218.event1.executed= false;// true to disable, false to enable
	myVars.races.race218.event2.executed= false;// true to disable, false to enable
</script>

