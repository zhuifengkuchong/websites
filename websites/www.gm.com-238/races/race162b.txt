<script id = "race162b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race162={};
	myVars.races.race162.varName="Object[1432].triggered";
	myVars.races.race162.varType="@varType@";
	myVars.races.race162.repairType = "@RepairType";
	myVars.races.race162.event1={};
	myVars.races.race162.event2={};
	myVars.races.race162.event1.id = "Lu_Id_a_20";
	myVars.races.race162.event1.type = "onmouseup";
	myVars.races.race162.event1.loc = "Lu_Id_a_20_LOC";
	myVars.races.race162.event1.isRead = "False";
	myVars.races.race162.event1.eventType = "@event1EventType@";
	myVars.races.race162.event2.id = "Lu_Id_li_37";
	myVars.races.race162.event2.type = "onmouseout";
	myVars.races.race162.event2.loc = "Lu_Id_li_37_LOC";
	myVars.races.race162.event2.isRead = "True";
	myVars.races.race162.event2.eventType = "@event2EventType@";
	myVars.races.race162.event1.executed= false;// true to disable, false to enable
	myVars.races.race162.event2.executed= false;// true to disable, false to enable
</script>

