<script id = "race61b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race61={};
	myVars.races.race61.varName="Lu_DOM__onmouseout";
	myVars.races.race61.varType="@varType@";
	myVars.races.race61.repairType = "@RepairType";
	myVars.races.race61.event1={};
	myVars.races.race61.event2={};
	myVars.races.race61.event1.id = "Lu_Id_li_27";
	myVars.races.race61.event1.type = "onmouseout";
	myVars.races.race61.event1.loc = "Lu_Id_li_27_LOC";
	myVars.races.race61.event1.isRead = "True";
	myVars.races.race61.event1.eventType = "@event1EventType@";
	myVars.races.race61.event2.id = "Lu_window";
	myVars.races.race61.event2.type = "onload";
	myVars.races.race61.event2.loc = "Lu_window_LOC";
	myVars.races.race61.event2.isRead = "False";
	myVars.races.race61.event2.eventType = "@event2EventType@";
	myVars.races.race61.event1.executed= false;// true to disable, false to enable
	myVars.races.race61.event2.executed= false;// true to disable, false to enable
</script>

