<script id = "race202a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race202={};
	myVars.races.race202.varName="Object[1432].triggered";
	myVars.races.race202.varType="@varType@";
	myVars.races.race202.repairType = "@RepairType";
	myVars.races.race202.event1={};
	myVars.races.race202.event2={};
	myVars.races.race202.event1.id = "Lu_Id_li_28";
	myVars.races.race202.event1.type = "onmouseover";
	myVars.races.race202.event1.loc = "Lu_Id_li_28_LOC";
	myVars.races.race202.event1.isRead = "True";
	myVars.races.race202.event1.eventType = "@event1EventType@";
	myVars.races.race202.event2.id = "Lu_Id_a_20";
	myVars.races.race202.event2.type = "onmouseup";
	myVars.races.race202.event2.loc = "Lu_Id_a_20_LOC";
	myVars.races.race202.event2.isRead = "False";
	myVars.races.race202.event2.eventType = "@event2EventType@";
	myVars.races.race202.event1.executed= false;// true to disable, false to enable
	myVars.races.race202.event2.executed= false;// true to disable, false to enable
</script>

