<script id = "race56a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race56={};
	myVars.races.race56.varName="Lu_DOM__onmouseout";
	myVars.races.race56.varType="@varType@";
	myVars.races.race56.repairType = "@RepairType";
	myVars.races.race56.event1={};
	myVars.races.race56.event2={};
	myVars.races.race56.event1.id = "Lu_window";
	myVars.races.race56.event1.type = "onload";
	myVars.races.race56.event1.loc = "Lu_window_LOC";
	myVars.races.race56.event1.isRead = "False";
	myVars.races.race56.event1.eventType = "@event1EventType@";
	myVars.races.race56.event2.id = "Lu_Id_a_35";
	myVars.races.race56.event2.type = "onmouseout";
	myVars.races.race56.event2.loc = "Lu_Id_a_35_LOC";
	myVars.races.race56.event2.isRead = "True";
	myVars.races.race56.event2.eventType = "@event2EventType@";
	myVars.races.race56.event1.executed= false;// true to disable, false to enable
	myVars.races.race56.event2.executed= false;// true to disable, false to enable
</script>

