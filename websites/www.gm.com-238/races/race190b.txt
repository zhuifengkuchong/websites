<script id = "race190b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race190={};
	myVars.races.race190.varName="Object[1432].triggered";
	myVars.races.race190.varType="@varType@";
	myVars.races.race190.repairType = "@RepairType";
	myVars.races.race190.event1={};
	myVars.races.race190.event2={};
	myVars.races.race190.event1.id = "Lu_Id_a_20";
	myVars.races.race190.event1.type = "onmouseup";
	myVars.races.race190.event1.loc = "Lu_Id_a_20_LOC";
	myVars.races.race190.event1.isRead = "False";
	myVars.races.race190.event1.eventType = "@event1EventType@";
	myVars.races.race190.event2.id = "company";
	myVars.races.race190.event2.type = "onmouseover";
	myVars.races.race190.event2.loc = "company_LOC";
	myVars.races.race190.event2.isRead = "True";
	myVars.races.race190.event2.eventType = "@event2EventType@";
	myVars.races.race190.event1.executed= false;// true to disable, false to enable
	myVars.races.race190.event2.executed= false;// true to disable, false to enable
</script>

