<script id = "race198b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race198={};
	myVars.races.race198.varName="Object[1432].triggered";
	myVars.races.race198.varType="@varType@";
	myVars.races.race198.repairType = "@RepairType";
	myVars.races.race198.event1={};
	myVars.races.race198.event2={};
	myVars.races.race198.event1.id = "Lu_Id_a_20";
	myVars.races.race198.event1.type = "onmouseup";
	myVars.races.race198.event1.loc = "Lu_Id_a_20_LOC";
	myVars.races.race198.event1.isRead = "False";
	myVars.races.race198.event1.eventType = "@event1EventType@";
	myVars.races.race198.event2.id = "Lu_Id_li_32";
	myVars.races.race198.event2.type = "onmouseover";
	myVars.races.race198.event2.loc = "Lu_Id_li_32_LOC";
	myVars.races.race198.event2.isRead = "True";
	myVars.races.race198.event2.eventType = "@event2EventType@";
	myVars.races.race198.event1.executed= false;// true to disable, false to enable
	myVars.races.race198.event2.executed= false;// true to disable, false to enable
</script>

