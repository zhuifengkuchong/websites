/* brand logo lockup (like investors home bottom) */
/**
 * @author jason.campbell
**/
(function($){
			
	$.fn.extend({

		brandLockup: function( options ) {
			
			var defaults = {
				bgColor			:		'#0C0F14',
				hoverColor	:			'#1c1f23',
				borders			:		true,
				itemWidth		:		calculateItemWidth()
			}

			var options =  $.extend( defaults, options );
	
			$.globals={
			};
			
			return this.each( function(index) {
				$this=$(this);
				var itemParent=$this.children('ul');
				var itemAnchors=itemParent.find('a');
				var logoLength=itemParent.find('li').length;
				itemAnchors.css('height','98px');
	
				itemAnchors.mouseup(function(){$(this).blur();});
			
				$bgColor=options.bgColor;
				$hoverColor=options.hoverColor;
				
				itemParent.find('li').each(function(index){
					
					if(!options.borders){
						bg=$bgColor;
						borderRight = '1px solid #848688';
					}else if(index==0){
						bg='url(/etc/designs/gmcom/images/gInvestors/brandLockUp-left.png) no-repeat '+$bgColor+''; borderRight = '1px solid #848688';
					}else if(index==(logoLength-1)){
						bg='url(/etc/designs/gmcom/images/gInvestors/brandLockUp-right.png) 100%  0 no-repeat '+$bgColor+'';borderRight = '';
					}else
					{
						bg='url(/etc/designs/gmcom/images/gInvestors/brandLockUp-center.png) 0 0 no-repeat '+$bgColor+'';borderRight = '1px solid #848688';
					}
					
					$(this).css({
						width:options.itemWidth,
						height:'98px',
						background:bg,
						borderRight:borderRight
					});

					$(this).css('text-align', 'center');

					$this.css({width:(options.itemWidth+1)*(index+1)});
					
					if(!$('html').hasClass('ipad')){
						$(this).hover(function(){
							$(this).css('backgroundColor', $hoverColor);
						},
						function(){
							$(this).css('backgroundColor',$bgColor);
						});
					}
					else{
						$(this).css('backgroundColor',$bgColor);					
					}
				});
				
			});
		}
	});
})(jQuery);

function calculateItemWidth(){
	var widthContainer = $('.parsys.lowerpar').width();
	if($('body').hasClass('mastheadhomepage'))
		widthContainer = $('.home-section').width();
	
	var logoLength = $('.brandLogoLockUp_container').children('ul').find('li').length;

	if(widthContainer > 980)
		widthContainer = 980;
	
	if(global_index > -1)
		logoLength = $('.container_'+global_index).children('ul').find('li').length;
	
	// 1px to border-right
	widthItem = (widthContainer / logoLength);
	return widthItem-1;
}


var global_index = -1;
$(document).ready(function(){
	var count = $('.brandLogoLockUp_container').length;
	if (count > 1){	
		global_index = 0;
		$('body.contentpagetwocolumn .brandLogoLockUp_container').each(function( index ) {
			global_index = index;
			var newitem=$(this).addClass('container_'+index);			
			$('body#investors .subsection_image_link_container:nth-child(2)').brandLockup({});
			$(newitem).brandLockup();
			
		});
	}
	else{
		$('body#investors .subsection_image_link_container:nth-child(2)').brandLockup({});	
		$('body.contentpagetwocolumn .brandLogoLockUp_container').brandLockup({});
		$('body.mastheadhomepage .home-section .brandLogoLockUp_container').brandLockup({});
	}
})