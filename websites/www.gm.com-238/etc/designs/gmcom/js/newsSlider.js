var minHeight = 0;
$('document').ready(function(){
	var count = 0;
	var inc = 0;
	var brandFeedArr = [];
	
	var max_characters = 185;
	var text = '';
	var read_more = '';
	var maxHeight = 0; 
	 
	var heightItem = 0;
	$('.gmFeed li').each(function( index ) {
		
		text = $(this).find('p').text();
		read_more = $(this).find('p a');
		
		if (text.length > max_characters){
			small_text = text.substr(0, max_characters);
			new_text = small_text.concat('...');
			$(this).find('p').text(new_text).append(read_more);
		}		
		
		// get height of items for min height to middle-section
		heightItem = $(this).height();
		if(heightItem > maxHeight)
			maxHeight = heightItem;		
	});
	minHeight = maxHeight;
	
	//places all feeds into a 2d array
	brandFeedArr.push($('.gmFeed li'));
	$('.brandFeed ul li').remove();
	
	//places brand feeds inside of contentTwo into an array and removes unseen nodes
	var brandFeeds = $('.brandFeed').toArray();
	$('.feedWrapper li:first').nextAll().remove();
	$('.brandFeed ul').append( $( brandFeedArr[inc][count] ).clone() );
	//Add cufon here
	if( typeof( Cufon ) == 'function' && Cufon.replace ){
		//Cufon.refresh();
		$('div.slideHolder .gotham-bold-dynamic:visible').each(function() {
			if ( !$(this).attr('cufid') ) {
				Cufon.replace(this, {
					fontFamily: 'gotham-bold',
					hover: true
				});
			}
		});
	}	
	
	// adds a corresponding #href for each feed
	for(var i = 1; i <= $(brandFeedArr[inc]).length; i++){
		$('.contentSlideNo').append('<a id="slideNo'+i+'" href="#">' + i + '</a>');
	}
	$('.contentSlideNo a:first').addClass('slideNoActive');
	
	var id;
	// controls numbers links for feed content slides
	$('.contentSlideNo a').live('click',function(e){
		if( $( this ).is( '.slideNoActive' )) return false;
	
		id = $(this).attr("id").toString().split("slideNo")[1];
		if(id=='1'){count=0;}
		if(id=='2'){count=1;}
		if(id=='3'){count=2;}
		if(id=='4'){count=3;}
		if(id=='5'){count=4;}
		
		$(brandFeedArr[inc][count]).clone().appendTo($('.slideHolder ul'));
		//Add cufon here
		if( typeof( Cufon ) == 'function' && Cufon.replace ){
			//Cufon.refresh();
			$('div.slideHolder .gotham-bold-dynamic:visible').each(function() {
				if ( !$(this).attr('cufid') ) {
					Cufon.replace(this, {
				fontFamily: 'gotham-bold',
				hover: true
			});
		}
			});
		}
		$('a[class="slideNoActive"]').removeClass('slideNoActive');
		$(this).addClass('slideNoActive');
		if($('.slideHolder ul li').length >= 2){
			$('.slideHolder ul li:last').css('opacity','0');
			$('.slideHolder ul li:first').animate({opacity:0}, 500 );
			$('.slideHolder ul li:last').animate({opacity:1}, 1 );
			console.log($('.slideHolder ul li:first').width());
			$('.slideHolder ul').animate({marginLeft:'-=' + $('.slideHolder ul li:first').width() + 'px'}, 500, function(){
				$(this).animate({marginLeft:'+=' + $('.slideHolder ul li:last').width() + 'px'}, 0, function(){
					$('.slideHolder ul li:first').remove();
				});
			});
		}
		return false;
	});
	
	
	
});