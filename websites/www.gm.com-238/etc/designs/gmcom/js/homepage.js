function addBackgroundImage(icon){
	var img = icon.children('img');
	var img_src = $(img).attr('src');
	icon.css('background', 'url(' + img_src + ') no-repeat');
	$(img).css('display','none');
}

function getMarginTopForCTA(isItemOne){
	var nameitem = 'mh_item';
	if(isItemOne)
		nameitem = 'mh_item1';
	
	//if there is not a paragraph
	if($('.'+nameitem+' div.text.section').length == 0)
		if(isItemOne){
			if($('html').hasClass('ie'))
				return 16;
			else
				return 23;
		}
		else
			return 18;
	else
		return 20;
}

var minHeight = 0;
var defaultHeight = 0;
$(document).ready(function(){	
	
	// The image in img tag is copied in the css attribute "background" for each Social Icon
	$('.mastheadhomepage .social a').each(function( index ) {
		addBackgroundImage($(this));
	});		
	
	//height for pop up of disclaimer qhen data-height is empty
	defaultHeight = $('div.parbase.disclaimer .rollOverDetails .tipText div').height() + 80;
	if($('html').hasClass('ie'))
		defaultHeight = $('div.parbase.disclaimer .rollOverDetails .tipText div').height() + 120;
	
	//margin top for CTAs on masthead
	var marginTop_CTA = 15;
	var isItemOne = false;
	$('div#rotatingMasthead li').each(function(){
		
		if($(this).find('div.mh_item').hasClass('mh_item1'))
			isItemOne = true;
		else
			isItemOne = false;			
		
		marginTop_CTA = getMarginTopForCTA(isItemOne);
		
		if($(this).find('div.mh_content_wrapper').hasClass('mhCtaVertical')){			
			$(this).find('div.callToAction').first().css('marginTop',marginTop_CTA+'px');
			$(this).find('div.callToAction .actionContainer').first().css('marginTop','0');
		}
		else if($(this).find('div.mh_content_wrapper').hasClass('mhCtaHorizontal')){
			$(this).find('div.callToAction').css('marginTop',marginTop_CTA+'px');			
			$(this).find('div.callToAction .actionContainer').css('marginTop','0');
		}		
	});
	
	$(window).bind('orientationchange', function (e) {
		$('#lower-section .brandLogoLockUp_container').brandLockup();	    
	});	
});

$(window).load(function(){	
	// initialize Disclaimer
	$('body.mastheadhomepage').slideupDisclaimer({
		width	:	701,
		defaultHeight : defaultHeight
	});
	
	// set min-height of column left to middle-section
	// minHeight got from newsSlider.js
	$('#middle-section .column-left').css('minHeight',minHeight);
	
	//set height to middle-section
	var heightCenter = $('#middle-section .column-center').height();
	var heightSection = heightCenter;
	
	if($('body').hasClass('ipad'))
		heightSection = heightCenter-24;
	if (minHeight > heightCenter)
		heightSection = minHeight;
	
	
		
	$('#middle-section').css('height',heightSection);
	
	//changed height of cufon gotham-bold
	$('.mastheadhomepage .mh_item1 h1.shoppingMastheadHeadline.gotham-bold cufon').css('height', '22px');
	$('.mastheadhomepage .mh_item1 h1.shoppingMastheadHeadline.gotham-bold cufon canvas').css('top', '-6px');		
	
	//lower-section margin for center Social Icons
	var widthSection = $('#lower-section').width();
	var widthIcons = $('#lower-section .content.social').width();
	var marginLeft = (widthSection - widthIcons)/2;
	$('#lower-section .content.social').css({marginLeft: marginLeft});
		
	//Update margin-top of disclaimer rollover if there are two disclaimer (spacing must be 10px)
	var nDisclaimer = $('#disclaimer').children('div.parbase').length;	
	if(nDisclaimer > 1){
		$('.mastheadhomepage #content .parbase.disclaimer #disclosureLinks').css('marginTop','0');
	}
	
	if($('.mh_item1 .title_diff_sizes h1').last().hasClass('gotham-bold'))
		$('.mh_item1 .section.text p').css('marginTop','15px');
});