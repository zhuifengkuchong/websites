<!-- //
(function () {
    var bizrate, Base64;
    var cdnPath = (("https:" == document.location.protocol) ? 'https://images.bizrate.com/s2static/us/eval/f99d3184' : 'http://images.bizrate.com/s2static/us/eval/f99d3184');
	var logoPath= (("https:" == document.location.protocol) ? '' : '');

    bizrate = {

        config: {
            modalWidth: 28,
            modalHeight: 41,
            popupWidth: 20,
            popupHeight: 20
        },

        params: {
            modalX: null,
            modalY: null,
            modalZIndex: 1000000,
            preview: null,
            passin: null,
            cartzip: null,
            cartdata: null,
            orderId: null,
            cartTotal: null,
            billingZipCode: null,
            productsPurchased: null
        },
        
        windowTracking: {
            isSurveyOpen: false,
            isCloseTouchpointSent: false,
            pitchType: 0
        },

        tplVars: {
        	popupTitle:'Tell Us What You Think!',
        	pitchType: 3,
            altImageText: '',
            surveyUrl: 'https://survey.bizrate.com/jfe/form/SV_3HNWcWCdYrgsYFT?br=14337804764424335929202030301003609&sessionId=920331443410638476&rid=1433781411310202955&mid=21744&merchantName=Coach.com&flow=9&rf_code=sur&testIds=S12275_JFE_SAVECONTINUE,jfe.test.enabled&browserAndVersion=unknown&browserType=unknown&osAndVersion=Windows&C102003=1&C1p=2&Q_JFE=1&ff_flow=10',
            inviteImageUrl: 'https://images.bizrate.com/eval/survey/invite_template/custom/21744_1.jpg',
            merchantLogoUrl: ((logoPath === '') ? '' : (logoPath + 21744 +'.gif')),
            buttonImageUrl:'',
            inviteWidth: '330',
            inviteHeight: '330',
            transparentImageUrl: cdnPath + '/images/transparent.gif',
            inviteUrl: 'https://eval.bizrate.com/surveyinvite?id=21744&surveyType=pos&browserAndVersion=unknown&browserType=unknown&osAndVersion=Windows',
            popupUrl: 'https://eval.bizrate.com/popupinvite?id=21744&surveyType=pos&browserAndVersion=unknown&browserType=unknown&osAndVersion=Windows',
            touchpointURL:'https://eval.bizrate.com/inviteLog?id=21744&product_id=1&flow=9&respondentId=1433781411310202955&testIds=S12275_JFE_SAVECONTINUE,jfe.test.enabled',
            closeWindowText: 'Close [X]'
        },

		escapeString:function (mString) {
			return mString.replace(/'/g, "&#39;").replace(/"/g, "&quot;");
		},

        loadScript: function (url, callback) {
            var sc;
            sc = document.createElement('script');
            sc.type = 'text/javascript';
            if (callback !== undefined) {
                if (sc.readyState) {
                    sc.onreadystatechange = function() {
                        if (sc.readyState == 'loaded' ||
                              sc.readyState == 'complete') {
                            sc.onreadystatechange = null;
                            callback();
                        }
                    };
                }
                else {
                    sc.onload = function() {
                        callback();
                    };
                }
            }
            sc.src = url;
            document.getElementsByTagName("head")[0].appendChild(sc);
            return true;
        },

        setParams: function () {
	    	var safeMerchantName = this.escapeString("Coach.com");
            if (window.passin_x !== undefined &&
                    window.passin_y !== undefined) {
                bizrate.params.modalX = window.parseInt(window.passin_x, 10);
                bizrate.params.modalY = window.parseInt(window.passin_y, 10);
            }
            if (window.z_index !== undefined &&
                  window.z_index > bizrate.params.modalZIndex) {
                bizrate.params.modalZIndex = window.z_index;
            }
            if (window.preview !== undefined && window.preview !== null) {
                bizrate.params.preview = window.preview;
            }
            if (window.passin !== undefined && window.passin !== null) {
                bizrate.params.passin = window.passin;
            }
            if (window.br_cartdata !== undefined &&
                window.br_cartdata !== null) {
                // Unless there is binary data here, these should be URL-encoded instead of Base64-encoded
                bizrate.params.cartdata = Base64.encode(window.br_cartdata);
            }
            if (window.br_cartzip !== undefined &&
                  window.br_cartzip !== null) {
                bizrate.params.cartzip = window.br_cartzip;
            }
            if (window.orderId !== undefined && window.orderId !== null) {
                bizrate.params.orderId = window.orderId;
            }
            if (window.cartTotal !== undefined && window.cartTotal !== null) {
                bizrate.params.cartTotal = window.cartTotal;
            }
            if (window.billingZipCode !== undefined && window.billingZipCode !== null) {
                bizrate.params.billingZipCode = window.billingZipCode;
            }
            if (window.productsPurchased !== undefined && window.productsPurchased !== null) {
                bizrate.params.productsPurchased = Base64.encode(window.productsPurchased);
            }

            return true;
        },

        getFormat: function () {
            var ua, ref, format;
            ua = navigator.userAgent.toLowerCase();
            ref = document.referrer.toLowerCase();
            if (bizrate.tplVars.pitchType !== 0) {
                if (ua.match(/googlebot/i) || ref.match(/[.]google[.]/)) {
                    format = 3;
                }
                else if (ua.match(/chrome/i)) {
                    format = 3;
                }
                else {
                    format = bizrate.tplVars.pitchType;
                }
            }
            else {
                format = bizrate.tplVars.pitchType;
            }
            return format;
        },

        init: function () {
            var format;
            bizrate.setParams();
            format = bizrate.getFormat();
            bizrate.windowTracking.pitchType = format;
            if (format === 0) {
                bizrate.displayInviteInline();
            } else {
                var ready = function (f) {document.body ? f() : setTimeout(function () { ready(f); }, 100);};
                ready(function () {
                    bizrate.start(format);
                });
            }

            return true;
        },

        start: function (format) {
            bizrate.windowTracking.isSurveyOpen = true;
            
            var ret;
            // Modal Invite
            if (format === 3 || format === 99) {
                ret = bizrate.displayInviteModal(format);
            }
            // Popup Invite
            else if (format !== 0) {
                ret = bizrate.displayInvitePopup();
                if (!ret) {
                    bizrate.start(99);
                }
            }
            return ret;
        },

        popupOptions: function (options) {
            var o;
            o = bizrate.mapHash(options, function (value, key) {
                var o, s;
                if (value === true) {
                    o = 'yes';
                }
                else if (value === false) {
                    o = 'no';
                }
                else if (!isNaN(value)) {
                    o = window.parseInt(value, 10).toString(10);
                }
                else {
                    o = value;
                }
                s = [key, o].join('=');
                return s;
            }).join(',');
            return o;
        },

        mapHash: function (arr, callback) {
            var i, out;
            out = [];
            for (i in arr) {
                if (arr.hasOwnProperty(i)) {
                    out.push(callback(arr[i], i));
                }
            }
            return out;
        },

        getObjectSize: function (obj) {
            var size, key;
            size = 0;
            for (key in obj) {
                if (obj.hasOwnProperty(key)) {
                    size++;
                }
            }
            return size;
        },

        buildURLParams: function (url, params, pString) {
            if (bizrate.getObjectSize(params) > 0) {
                url += (url.indexOf('?') === -1) ? '?' : '&';
                url += bizrate.mapHash(params, function (value, key) {
                    return [key, value].join('=');
                }).join('&');
            }
            return url;
        },

        buildRawParams: function (pString) {
            var params, i, _i, kv, out;
            out = {};
            params = pString.split(/[&\?]+/g);
            for (i = 0, _i = params.length; i < _i; i++) {
                if (params[i].length > 0) {
                    kv = params[i].split('=');
                    if (kv.length > 1) {
                        out[kv[0]] = kv[1];
                    }
                    else {
                        out[kv[0]] = '';
                    }
                }
            }
            return out;
        },

        extend: function (o) {
            var a, i, _i, args;
            if (arguments.length > 1) {
                args = Array.prototype.slice.call(arguments).slice(1);
                for (i = 0, _i = args.length; i < _i; i++) {
                    for (a in args[i]) {
                        if (args[i].hasOwnProperty(a)) {
                            o[a] = args[i][a];
                        }
                    }
                }
            }
            return o;
        },

        cssDefs: function () {
            var o, a, d;
            a = Array.prototype.slice.call(arguments, 0);
            a.push(this.cssBoxSize());
            o = $.extend.apply(true, a);
            return o;
        },

        cssBoxSize: function () {
            return {
                boxSizing: 'border-box',
                MozBoxSizing: 'border-box',
                MsBoxSizing: 'border-box',
                WebkitBoxSizing: 'border-box'
            };
        },

        cssBoxShadow: function (val) {
            return {
                boxShadow: val,
                MozBoxShadow: val,
                WebkitBoxShadow: val,
                MsBoxShadow: val
            };
        },


        extendedURLParams: function (extras) {
            var o, s;
            o = {};
            if (bizrate.params.cartTotal === null && bizrate.params.billingZipCode === null
                && bizrate.params.cartzip !== null) {
                bizrate.extend(o, bizrate.buildRawParams(bizrate.params.cartzip));
            }
            if (bizrate.params.cartTotal !== null) {
                o['Q113198'] = bizrate.params.cartTotal;
            }
            if (bizrate.params.billingZipCode !== null) {
                o['Q113199'] = bizrate.params.billingZipCode;
            }
            if (bizrate.params.orderId === null && bizrate.params.passin !== null) {
                bizrate.extend(o,
                               bizrate.buildRawParams(bizrate.params.passin));
            }
            if (bizrate.params.orderId !== null) {
                o['Q104626'] = bizrate.params.orderId;
            }
            if (bizrate.params.preview !== null) {
                o['preview'] = bizrate.params.preview;
            }
            if (bizrate.params.productsPurchased === null && bizrate.params.cartdata !== null) {
                o['Q113195'] = bizrate.params.cartdata;
            }
            if (bizrate.params.productsPurchased !== null && bizrate.params.productsPurchased.length <= 1750) {
                o['Q113201'] = bizrate.params.productsPurchased;
            }
            if (!isNaN(bizrate.tplVars.pitchType)) {
                o['pitch_type'] = bizrate.tplVars.pitchType;
            }
            if (extras !== undefined) {
                bizrate.extend(o, extras);
            }
            return o;
        },

        coordsCenterException: function () {
            return (bizrate.params.modalX === 500 &&
                    bizrate.params.modalY === 500);
        },
        getPopupCoords: function (w, h) {
            var o, win, sx, sy, wx, wy,
                magicPopupTitleHeight, magicWindowTitleHeight;
            magicPopupTitleHeight = 50;
            magicWindowTitleHeight = 100;
            o = {};
            win = window;
            var e = window, a = 'inner';
			if ( !( 'innerWidth' in window ) )
			{
				a = 'client';
				e = document.documentElement || document.body;
			}
			var winWidth = e[ a+'Width' ];
			var winHeight = e[ a+'Height' ];
            // Exception to center 500x500 passin
            if (bizrate.params.modalX !== null &&
                    bizrate.params.modalY !== null &&
                    !bizrate.coordsCenterException()) {
                o.x = bizrate.params.modalX;
                o.y = bizrate.params.modalY;
            }
            else {
                // X Coords
                if (window.screenLeft !== undefined) {
                    sx = window.screenLeft;
                }
                else if (window.screenX !== undefined) {
                    sx = window.screenX;
                }
                else {
                    sx = 0;
                }
                wx = Math.floor(winWidth / 2) - Math.floor(w / 2);
                o.x = sx + wx;
                // Y Coords
                if (window.screenTop !== undefined) {
                    sy = window.screenTop;
                }
                else if (window.screenY !== undefined) {
                    sy = window.screenY;
                }
                else {
                    sy = 0;
                }
                wy = ((Math.floor(winHeight / 2) - Math.floor(h / 2)) -
                      magicPopupTitleHeight);
                o.y = (sy + wy + magicWindowTitleHeight);
            }
            return o;
        },

        getModalCoords: function (w, h, isPopup) {
            var o, win;
            o = {};

            if (bizrate.params.modalX !== null &&
                    !bizrate.coordsCenterException()) {
                o.x = bizrate.params.modalX;
            }
            else {
                o.x = (Math.floor(bizrate.f_clientWidth() / 2) - Math.floor(w / 2));
            }
            if (bizrate.params.modalY !== null &&
                    !bizrate.coordsCenterException()) {
                o.y = bizrate.params.modalY;
            }
            else {
                o.y = (Math.floor(bizrate.f_clientHeight() / 2) - Math.floor(h / 2));
            }
            return o;
        },

        f_clientWidth: function () {
            return bizrate.f_filterResults (
                window.innerWidth ? window.innerWidth : 0,
                document.documentElement ? document.documentElement.clientWidth : 0,
                document.body ? document.body.clientWidth : 0
            );
        },

        f_clientHeight: function() {
            return bizrate.f_filterResults (
                window.innerHeight ? window.innerHeight : 0,
                document.documentElement ? document.documentElement.clientHeight : 0,
                document.body ? document.body.clientHeight : 0
            );
        },

        f_filterResults: function (n_win, n_docel, n_body) {
	        var n_result = n_win ? n_win : 0;
	        var n_result2 = n_docel ? n_docel : 0;
	        if(n_result2 > n_result && n_result <= 0) {
	            n_result = n_result2;
	        }

	        n_result2 = n_body ? n_body : 0;
	        if(n_result2 > n_result && n_result <= 0) {
	            n_result = n_result2;
	        }

	        return n_result;
        },

        displayInvitePopup: function () {
            var u, popOps, win, winOp, o, u, posi, imgWidth, imgHeight;
            sendPitchType = 1;
            o = bizrate.extendedURLParams({
	                pitch_type: sendPitchType
		    });
            u = bizrate.buildURLParams(bizrate.tplVars.popupUrl, o);
            imgWidth = parseInt(bizrate.tplVars.inviteWidth) + parseInt(bizrate.config.popupWidth);
            imgHeight = parseInt(bizrate.tplVars.inviteHeight) + parseInt(bizrate.config.popupHeight);
            posi = bizrate.getPopupCoords(imgWidth, imgHeight);
            popOps =
                bizrate.popupOptions({
                    directories: 1,
                    height: imgHeight,
                    width: imgWidth,
                    screenX: posi.x,
                    screenY: posi.y,
                    left: posi.x,
                    top: posi.y,
                    resizable: 0
                });
            win = window.open(u, 'br_invite', popOps);
            winOp = (win && win.closed !== undefined && !win.closed);
            return winOp;
        },

		inviteInlineDom: function (o,u,imgWidth,imgHeight) {
			d = document.createElement('div');
            d.style.cursor = "pointer";
            d.style.position = "relative";
            d.style.textAlign = "left";
            d.style.width = imgWidth + "px";
            d.style.height = imgHeight + "px";
            d.style.top = "0px";
            d.style.left = "0px";

            anc = document.createElement('a');
            anc.setAttribute('id','inlineBrDialog');
            anc.setAttribute("target", "_blank");
            anc.style.display = "block";
            anc.style.width = bizrate.tplVars.inviteWidth + "px";
            anc.style.height = bizrate.tplVars.inviteHeight + "px";
            anc.style.textDecoration = "none";

            /*if('' != '') {
                im = document.createElement('img');
                im.setAttribute("src", bizrate.tplVars.inviteImageUrl);
                im.setAttribute("usemap", "#br_image_map");
                im.setAttribute("alt", bizrate.tplVars.altImageText);
                im.style.cursor = "pointer";
                im.style.top = "0px";
                im.style.left = "0px";
                im.style.border = "0px none";
                im.style.display = "block";
                im.style.position = "absolute";
                im.style.width = bizrate.tplVars.inviteWidth + "px";
                im.style.height = bizrate.tplVars.inviteHeight + "px";

                inviteMap = document.createElement('map');
                inviteMap.setAttribute("name", "br_image_map");
                area1 = document.createElement('area');
                area1.setAttribute("coords", "");
                area1.setAttribute("shape", "rect");
                area1.setAttribute("href", "");
                area1.setAttribute("target", "_blank");
                area2 = document.createElement('area');
                area2.setAttribute("coords", "");
                area2.setAttribute("shape", "rect");
                area2.setAttribute("href", u);
                area2.setAttribute("target", "_blank");
                inviteMap.appendChild(area1);
                inviteMap.appendChild(area2);
                anc.appendChild(im);
                anc.appendChild(inviteMap);
            } else {*/
            anc.setAttribute("href", u);
            im = document.createElement('img');
            im.setAttribute("src", bizrate.tplVars.inviteImageUrl);
            im.setAttribute("alt", bizrate.tplVars.altImageText);
            im.style.cursor = "pointer";
            im.style.top = "0px";
            im.style.left = "0px";
            im.style.border = "0px none";
            im.style.display = "block";
            im.style.position = "absolute";
            im.style.width = bizrate.tplVars.inviteWidth + "px";
            im.style.height = bizrate.tplVars.inviteHeight + "px";

            if (logoPath) {
                merchantLogo = document.createElement("img");
                merchantLogo.setAttribute("class","brmerchantLogo");
                merchantLogo.setAttribute("id","brmerchantLogo");
                merchantLogo.setAttribute("src", bizrate.tplVars.merchantLogoUrl);
                bizrate.setStyle(merchantLogo, {});
                merchantLogo.onerror = function() {
                    merchantLogo.style.display = "none";
                };
            }

            buttonImage = document.createElement("img");
            buttonImage.setAttribute("src", bizrate.tplVars.buttonImageUrl);
            bizrate.setStyle(buttonImage, {});


            merchantWords = document.createElement("div");
            merchantWords.setAttribute("class", "brshrink_fit");
            merchantWords.setAttribute("id", "brshrink_fit");
            bizrate.setStyle(merchantWords, {});
            merchantWords.style.whiteSpace = "nowrap";

            merchantWords2 = document.createElement("div");
            bizrate.setStyle(merchantWords2, {});

            merchantWords.innerHTML = '';
            merchantWords2.innerHTML = '';

            anc.appendChild(im);
            anc.appendChild(merchantWords);
            anc.appendChild(merchantWords2);
            if (logoPath) {
                anc.appendChild(merchantLogo);
            }
            if(bizrate.tplVars.buttonImageUrl) {
                anc.appendChild(buttonImage);
            }
            //}

            d.appendChild(anc);

            //if(typeof merchantWords != 'undefined') {
            //    bizrate.fill_with_text(merchantWords);
            //}

			return d;
		},


        displayInviteInline: function () {

            var u, d, im, al, o, sendPitchType;
            sendPitchType = 0;
            o = bizrate.extendedURLParams({
                pitch_type: sendPitchType
            });
            u = bizrate.buildURLParams(bizrate.tplVars.surveyUrl, o);
            im = bizrate.tplVars.inviteImageUrl;
            al = bizrate.tplVars.altImageText;
            d = bizrate.inviteInlineDom(o,u,parseInt(bizrate.tplVars.inviteWidth),parseInt(bizrate.tplVars.inviteHeight));
            document.write(d.outerHTML);

            bizrate.fill_with_text(document.getElementById('brshrink_fit'));
            window.onload=function(){
                var image = document.getElementById('brmerchantLogo');
                if (image != null && ((typeof image.naturalWidth != "undefined" && image.naturalWidth == 0) || image.readyState == 'uninitialized' )) {
                    image.style.display = "none";
                }

                document.getElementById('inlineBrDialog').onclick = function() {
                    bizrate.logInvite(sendPitchType, 1600300);
                    return true;
                }

            };

            bizrate.logInvite(sendPitchType);
            return true;

        },

        logInvite: function (sendPitchType) {
            var confirmUrl;
            confirmUrl = bizrate.buildURLParams(bizrate.tplVars.touchpointURL, {
                pitchType: sendPitchType
            });
            bizrate.loadScript(confirmUrl);
            return true;
        },

        logInvite: function (sendPitchType, sendTouchpoint) {
            var confirmUrl;
            confirmUrl = bizrate.buildURLParams(bizrate.tplVars.touchpointURL, {
                pitchType: sendPitchType,
                touchpoint: sendTouchpoint
            });
            bizrate.loadScript(confirmUrl);
            return true;
        },

        displayInviteModal: function (sendPitchType) {

            var im, d, imur, anc, o, u,
                posi, imt, coordWidth, coordHeight, imgWidth, imgHeight;
            o = bizrate.extendedURLParams({
                pitch_type: sendPitchType
            });
            u = bizrate.buildURLParams(bizrate.tplVars.surveyUrl, o);

            coordWidth = parseInt(bizrate.tplVars.inviteWidth) + parseInt(bizrate.config.modalWidth);
            coordHeight = parseInt(bizrate.tplVars.inviteHeight) + parseInt(bizrate.config.modalHeight);
            imgWidth = parseInt(bizrate.tplVars.inviteWidth) + 26;
            imgHeight = parseInt(bizrate.tplVars.inviteHeight) + 20;
            posi = bizrate.getModalCoords(coordWidth, coordHeight);

            d = document.createElement('div');
            d.setAttribute('id','brDialog');
            d.style.width = imgWidth + "px";
            d.style.backgroundColor = "#FFF";
            d.style.cursor = "pointer";
            d.style.position = "relative";
            d.style.textAlign = "left";
            d.style.whiteSpace = "normal";
            d.style.boxSizing = "border-box";
            d.style.MozBoxSizing = "border-box";
            d.style.MsBoxSizing = "border-box";
            d.style.WebkitBoxSizing = "border-box";

            anc = document.createElement('a');
            anc.style.display = "block";
            anc.style.width = imgWidth + "px";
            anc.style.height = imgHeight + "px";
            anc.style.textDecoration = "none";
            anc.style.boxSizing = "border-box";
            anc.style.MozBoxSizing = "border-box";
            anc.style.MsBoxSizing = "border-box";
            anc.style.WebkitBoxSizing = "border-box";

            /*if('' != '') {
                imt = document.createElement('img');
                imt.setAttribute("src", bizrate.tplVars.transparentImageUrl);
                imt.setAttribute("usemap", "#br_image_map");
                imt.style.cursor = "pointer";
                imt.style.top = "0px";
                imt.style.left = "0px";
                imt.style.border = "0px none";
                imt.style.display = "block";
                imt.style.position = "absolute";
                imt.style.width = imgWidth + "px";
                imt.style.height = imgHeight + "px";
                imt.style.boxSizing = "border-box";
                imt.style.MozBoxSizing = "border-box";
                imt.style.MsBoxSizing = "border-box";
                imt.style.WebkitBoxSizing = "border-box";


                im = document.createElement('img');
                im.setAttribute("src", bizrate.tplVars.inviteImageUrl);
                im.setAttribute("usemap", "#br_image_map");
                im.setAttribute("alt", bizrate.tplVars.altImageText);
                im.style.boxSizing = "border-box";
                im.style.MozBoxSizing = "border-box";
                im.style.MsBoxSizing = "border-box";
                im.style.WebkitBoxSizing = "border-box";
                im.style.cursor = "pointer";
                im.style.top = "10px";
                im.style.left = "1px";
                im.style.border = "0px none";
                im.style.display = "block";
                im.style.position = "absolute";
                im.style.width = bizrate.tplVars.inviteWidth + "px";
                im.style.height = bizrate.tplVars.inviteHeight + "px";

                inviteMap = document.createElement('map');
                inviteMap.setAttribute("name", "br_image_map");
                area1 = document.createElement('area');
                area1.setAttribute("coords", "");
                area1.setAttribute("shape", "rect");
                area1.setAttribute("href", "");
                area1.setAttribute("target", "_blank");
                area2 = document.createElement('area');
                area2.setAttribute("coords", "");
                area2.setAttribute("shape", "rect");
                area2.setAttribute("href", u);
                area2.setAttribute("target", "_blank");
                inviteMap.appendChild(area1);
                inviteMap.appendChild(area2);
                anc.appendChild(im);
                anc.appendChild(imt);
                anc.appendChild(inviteMap);

                area2.onclick = function() {
                    d.brDialog('destroy');
                    return true;
                };

            } else {*/
            anc.setAttribute("href", u);
            anc.setAttribute("target", "_blank");
            imt = document.createElement('img');
            imt.setAttribute("src", bizrate.tplVars.transparentImageUrl);
            imt.style.position = "absolute";
            imt.style.top = "0px";
            imt.style.left = "0px";
            imt.style.cursor = "pointer";
            imt.style.border = "0px none";
            imt.style.display = "block";
            imt.style.width = imgWidth + "px";
            imt.style.height = imgHeight + "px";
            imt.style.boxSizing = "border-box";
            imt.style.MozBoxSizing = "border-box";
            imt.style.MsBoxSizing = "border-box";
            imt.style.WebkitBoxSizing = "border-box";

            dimg = document.createElement('div');
            dimg.style.position = "absolute";
            dimg.style.cursor = "pointer";
            dimg.style.top = "10px";
            dimg.style.left = "13px";
            dimg.style.border = "0px none";
            dimg.style.display = "block";
            dimg.style.boxSizing = "border-box";
            dimg.style.MozBoxSizing = "border-box";
            dimg.style.MsBoxSizing = "border-box";
            dimg.style.WebkitBoxSizing = "border-box";

            im = document.createElement('img');
            im.setAttribute("src", bizrate.tplVars.inviteImageUrl);
            im.setAttribute("alt", bizrate.tplVars.altImageText);
            im.style.cursor = "pointer";
            im.style.top = "10px";
            im.style.left = "13px";
            im.style.border = "0px none";
            im.style.display = "block";
            im.style.position = "absolute";
            im.style.width = bizrate.tplVars.inviteWidth + "px";
            im.style.height = bizrate.tplVars.inviteHeight + "px";
            im.style.boxSizing = "border-box";
            im.style.MozBoxSizing = "border-box";
            im.style.MsBoxSizing = "border-box";
            im.style.WebkitBoxSizing = "border-box";
            if (logoPath) {
                merchantLogo = document.createElement("img");
                merchantLogo.setAttribute("class","brmerchantLogo");
                merchantLogo.setAttribute("id","brmerchantLogo");
                merchantLogo.setAttribute("src", bizrate.tplVars.merchantLogoUrl);
                bizrate.setStyle(merchantLogo, {});
                merchantLogo.onerror = function() {
                    merchantLogo.style.display = "none";
                };
            }

            buttonImage = document.createElement("img");
            buttonImage.setAttribute("src", bizrate.tplVars.buttonImageUrl);
            bizrate.setStyle(buttonImage, {});


            merchantWords = document.createElement("div");
            merchantWords.setAttribute("class", "brshrink_fit");
            merchantWords.setAttribute("id", "brshrink_fit");
            bizrate.setStyle(merchantWords, {});
            merchantWords.style.whiteSpace = "nowrap";

            merchantWords2 = document.createElement("div");
            bizrate.setStyle(merchantWords2, {});

            merchantWords.innerHTML = '';
            merchantWords2.innerHTML = '';

            anc.appendChild(imt);
            anc.appendChild(im);
            dimg.appendChild(merchantWords);
            dimg.appendChild(merchantWords2);
            if (logoPath) {
                anc.appendChild(merchantLogo);
            }
            if(bizrate.tplVars.buttonImageUrl) {
                anc.appendChild(buttonImage);
            }
            anc.appendChild(dimg);
            //}
            anc.onclick = function() {
                bizrate.logInvite(sendPitchType, 1600300);
                bizrate.destroyDialog(document.getElementById('brdialog-win'));
                return true;
            };
            d.appendChild(anc);

            bizrate.createBrDialog(d, {
                titleText: bizrate.tplVars.altImageText,
                closeText: bizrate.tplVars.closeWindowText,
                x: posi['x'],
                y: posi['y'],
                z: bizrate.params.modalZIndex,
                width: parseInt(bizrate.tplVars.inviteWidth) + parseInt(bizrate.config.modalWidth)
            }, sendPitchType);

            bizrate.fill_with_text(document.getElementById('brdialog-title'));

            if(typeof merchantWords != 'undefined') {
                bizrate.fill_with_text(merchantWords);
            }

            bizrate.logInvite(sendPitchType);

            return true;
        },

        setStyle: function ( objId, propertyObject ) {
            for (var property in propertyObject) {
                if(property == 'float') {
                    objId.style['cssFloat'] = propertyObject[property];
                    objId.style['styleFloat'] = propertyObject[property];
                    objId.style[property] = propertyObject[property];
                } else {
                    objId.style[property] = propertyObject[property];
                }

            }
        },

        createBrDialog: function (dialog, options, sendPitchType) {
            var settings = {
                titleText: '',
                closeText: 'Close [X]',
                x: 0,
                y: 0,
                z: 99999,
                width: 300
            };
            if (options) {
                settings = options;
            }
            var brdWin, brdHead, brdTitle, brdBody, brdClose;


            brdWin = document.createElement("div");
            brdWin.setAttribute("class", "brdialog-win");
            brdWin.setAttribute("id", "brdialog-win");
            bizrate.setStyle(brdWin, {zIndex: settings.z.toString(10),width: settings.width.toString(10) + 'px',position: 'absolute',left: settings.x.toString(10) + 'px',top: settings.y.toString(10) + 'px',border: '1px solid #ccc'});
            brdWin.style.boxSizing = "border-box";
            brdWin.style.MozBoxSizing = "border-box";
            brdWin.style.MsBoxSizing = "border-box";
            brdWin.style.WebkitBoxSizing = "border-box";


            brdHead = document.createElement("div");
            brdHead.setAttribute("class", "brdialog-head");
            bizrate.setStyle(brdHead, {backgroundColor: '#000080',height: '19px',cursor: 'default',lineHeight: '19px',color: '#FFF',fontSize: '13px',paddingLeft: '10px',paddingRight: '10px',fontFamily: 'Arial, Helvetica, sans-serif'});
            brdHead.style.boxSizing = "border-box";
            brdHead.style.MozBoxSizing = "border-box";
            brdHead.style.MsBoxSizing = "border-box";
            brdHead.style.WebkitBoxSizing = "border-box";

            brdHead.onmousedown = function(event) {
                if (!event) event = window.event;
                (event.preventDefault) ? event.preventDefault() : event.returnValue = false;
                bizrate.startMoving(event);

            }

            brdHead.onmouseup = function() {
                bizrate.stopMoving();
            }

            brdTitle = document.createElement("div");
            brdTitle.setAttribute("class", "brdialog-title");
            brdTitle.setAttribute("id", "brdialog-title");
            brdTitle.innerHTML = settings.titleText;
            bizrate.setStyle(brdTitle, {whiteSpace:'nowrap', width: '280px', float: 'left',height: '100%',fontWeight: 'bold',fontFamily: 'Arial, Helvetica, sans-serif',fontSize: '13px',color: '#FFF',lineHeight: '19px',textAlign:'left'});
            brdTitle.style.boxSizing = "border-box";
            brdTitle.style.MozBoxSizing = "border-box";
            brdTitle.style.MsBoxSizing = "border-box";
            brdTitle.style.WebkitBoxSizing = "border-box";

            brdClose = document.createElement("div");
            brdClose.setAttribute("class", "brdialog-close");
            brdClose.innerHTML = settings.closeText;
            bizrate.setStyle(brdClose, {cursor: 'pointer',float: 'right',height: '100%',fontSize: '9px',color: '#FFF',fontFamily: 'Arial, Helvetica, sans-serif',textDecoration: 'underline',lineHeight: '19px'});
            brdClose.style.boxSizing = "border-box";
            brdClose.style.MozBoxSizing = "border-box";
            brdClose.style.MsBoxSizing = "border-box";
            brdClose.style.WebkitBoxSizing = "border-box";

            brdClose.onclick = function () {
                bizrate.logInvite(sendPitchType, 1600200);
                bizrate.destroyDialog(document.getElementById('brdialog-win'));
                return false;
            };

            brdBody = document.createElement("div");
            brdBody.setAttribute("class", "brdialog-body");
            bizrate.setStyle(brdBody, {backgroundColor: '#FFF',cursor: 'pointer, height:155px'});
            brdBody.style.boxSizing = "border-box";
            brdBody.style.MozBoxSizing = "border-box";
            brdBody.style.MsBoxSizing = "border-box";
            brdBody.style.WebkitBoxSizing = "border-box";


            brdHead.appendChild(brdClose);
            brdHead.appendChild(brdTitle);
            brdBody.appendChild(dialog);
            brdWin.appendChild(brdHead);
            brdWin.appendChild(brdBody);


            var bObj = document.getElementsByTagName('body').item(0);
            bObj.appendChild(brdWin);
            return true;
        },

        destroyDialog: function (dialog) {
            bizrate.windowTracking.isSurveyOpen = false;
            dialog.parentNode.removeChild(dialog);
            return true;
        },

        startMoving: function(evt){
           evt = evt || window.event;
           var posX = evt.clientX;
           posY = evt.clientY;

           var a = document.getElementById('brdialog-win');

           divTop = a.style.top;
           divLeft = a.style.left;
           divTop = divTop.replace('px','');
           divLeft = divLeft.replace('px','');
           var diffX = posX - divLeft;
           diffY = posY - divTop;
           document.onmousemove = function(evt){
               evt = evt || window.event;
               var posX = evt.clientX;
               posY = evt.clientY;
               aX = posX - diffX;
               aY = posY - diffY;
               if (aY > 0 && aX > 0 && aX < 1200 && aY < 900) {
                   bizrate.move(aX,aY);
               } else {
                   bizrate.stopMoving();
               }
           }

       },

        stopMoving: function(){
            document.onmousemove = function(){};
        },

        move: function(xpos,ypos){

           var a = document.getElementById('brdialog-win');
           a.style.left = xpos + 'px';
           a.style.top = ypos + 'px';

       },

       fill_with_text: function (container) {
            var fontSize = parseFloat(container.style.fontSize);
            container.style.overflow = "auto";
            var changes = 0;
            var blnSuccess = true;

            while (container.scrollWidth > container.clientWidth) {
                fontSize--;
                container.style.fontSize = fontSize + "px";
                changes++;
                if (changes > 500) {
                    //failsafe..
                    blnSuccess = false;
                    break;
                }
            }
            container.style.overflow = "visible";
       }

   }

    /**
    *
    *  Base64 encode / decode
    *  http://www.webtoolkit.info/
    *
    **/
    var Base64 = {
        // private property
        _keyStr :
            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
        // public method for encoding
        encode : function (input) {
            var output = "";
            var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
            var i = 0;
            input = Base64._utf8_encode(input);
            while (i < input.length) {
                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);
                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;
                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }
                output = output +
                this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
                this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);
            }
            return output;
        },

        // public method for decoding
        decode : function (input) {
            var output = "";
            var chr1, chr2, chr3;
            var enc1, enc2, enc3, enc4;
            var i = 0;
            input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
            while (i < input.length) {
                enc1 = this._keyStr.indexOf(input.charAt(i++));
                enc2 = this._keyStr.indexOf(input.charAt(i++));
                enc3 = this._keyStr.indexOf(input.charAt(i++));
                enc4 = this._keyStr.indexOf(input.charAt(i++));
                chr1 = (enc1 << 2) | (enc2 >> 4);
                chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                chr3 = ((enc3 & 3) << 6) | enc4;
                output = output + String.fromCharCode(chr1);
                if (enc3 != 64) {
                    output = output + String.fromCharCode(chr2);
                }
                if (enc4 != 64) {
                    output = output + String.fromCharCode(chr3);
                }
            }
            output = Base64._utf8_decode(output);
            return output;
        },

        // private method for UTF-8 encoding
        _utf8_encode : function (string) {
            string = string.replace(/\r\n/g,"\n");
            var utftext = "";
            for (var n = 0; n < string.length; n++) {
                var c = string.charCodeAt(n);
                if (c < 128) {
                    utftext += String.fromCharCode(c);
                }
                else if((c > 127) && (c < 2048)) {
                    utftext += String.fromCharCode((c >> 6) | 192);
                    utftext += String.fromCharCode((c & 63) | 128);
                }
                else {
                    utftext += String.fromCharCode((c >> 12) | 224);
                    utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                    utftext += String.fromCharCode((c & 63) | 128);
                }
            }
            return utftext;
        },

        // private method for UTF-8 decoding
        _utf8_decode : function (utftext) {
            var string = "";
            var i = 0;
            var c = c1 = c2 = 0;
            while ( i < utftext.length ) {
                c = utftext.charCodeAt(i);
                if (c < 128) {
                    string += String.fromCharCode(c);
                    i++;
                }
                else if((c > 191) && (c < 224)) {
                    c2 = utftext.charCodeAt(i+1);
                    string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                    i += 2;
                }
                else {
                    c2 = utftext.charCodeAt(i+1);
                    c3 = utftext.charCodeAt(i+2);
                    string += String.fromCharCode(
                        ((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                    i += 3;
                }
            }
            return string;
        }
    };
    
    window.addEventListener('beforeunload', function() {
       if(bizrate.windowTracking.isSurveyOpen && !bizrate.windowTracking.isCloseTouchpointSent) {
          console.log('before unload');
          bizrate.windowTracking.isCloseTouchpointSent = true;
          bizrate.logInvite((bizrate.windowTracking.pitchType ? bizrate.windowTracking.pitchType : 1), 1600420);
       }
    });
    
    window.addEventListener('unload', function() { //fallback
       if(bizrate.windowTracking.isSurveyOpen && !bizrate.windowTracking.isCloseTouchpointSent) {
          console.log('unload');
          bizrate.windowTracking.isCloseTouchpointSent = true;
          bizrate.logInvite((bizrate.windowTracking.pitchType ? bizrate.windowTracking.pitchType : 1), 1600420);
       }
    });

    bizrate.init();
    return true;
})();
//-->
