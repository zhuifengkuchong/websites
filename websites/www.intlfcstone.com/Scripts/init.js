(function(w){
	
	// force commodities closed on mobile
	if (isMobileDisplay() && (navigator.userAgent.match(/iPad/i) == null)) {
		$('.active', $('.commodities-container').parent()).removeClass('active');
	}

	menusInit();

	if ($('.js-move-capabilities').length && isMobileDisplay()) {
		$('div.other-list[role=navigation]').removeClass('other-list').addClass('accordion').appendTo('.l-main').find('h4').addClass('toggle').next().addClass('accordion-body');
	}

	accordion('.accordion', '.toggle', '.accordion-body');
	accordion('.accordion', '.toggle', '.accordion-body', '#js_filterOfficePartial');

	tablist('.tabs-stacked');

	makeFollowBtn('.link-to-top');

	modals();

	block_link();

	matchHeights_separatorBlks(); // separator blocks

	fixExternalLinks();
	
	// TOMBSTONES in ARTICLES //
	if ( !isMobileDisplay() ) {// only in desktop
		$('article > .block-tombstones').insertAfter('article header');
	}
	
	// HOMEPAGE ONLY /////////////////////
	// set up sliders
	var $sliders = $('.slider');
	for (var i = $sliders.length; 0 < i--;) {
		
		var $this = $sliders.eq(i);
		if ($this.children().length > 1) {
			var $left = $('<a class="carousel-prev" href="#">&lsaquo;</a>').on('click', function (e) {
				e.preventDefault();
				var $this = $(this).parent().find('.active');
				$this.trigger('swiperight');
			}).insertBefore($this),
				$right = $('<a class="carousel-next" href="#">&rsaquo;</a>').on('click', function (e) {
					e.preventDefault();
					var $this = $(this).parent().find('.active');
					$this.trigger('swipeleft');
				}).insertBefore($this),
				$navigation = $('<div class="carousel-pagination"><ol/></div>').insertAfter($this);

			$this.find('> li').each(function (i) {
				var $thisSlide = $(this);
				$('<li><a href="#"/></li>').appendTo($navigation.find('ol')).find('a').on('click', function (e) {
					e.preventDefault();
					$thisSlide.trigger('slideTo');
				});
			});

			$navigation.find('li').eq(0).addClass('active');
			$this.on('nSwipeSlider.swipeDone', function (e, jQueryDOMObject, i) {
				$(this).parent().find('.carousel-pagination li').removeClass('active');
				$(this).parent().find('.carousel-pagination li').eq(i - 1).addClass('active');
			});

			$this.nSwipeSlider({ nonTransSpeed: 400, autoSlide: ($this.hasClass('autoSlide')) ? 5000 : false });
		}
	}
})(this);



//
function matchHeights_separatorBlks()
{
	//if ( !isIE7() ) return false;

	var outer   = '.table-cell',
		inner   = '.address',
		adjustMe = $('.icon-form').parent(inner),
		matchMe  = $(outer).eq(0).find(inner);

	//setTimeout(function () { verticalAlign($('.team-list .row')); }, 1000);
	
	//matchChildToParentHeight(matchMe, adjustMe); //
}

// Match height of children and center vertically
function verticalAlign($row) {
	for (var y = $row.length; 0 < y--;) { // for each row...
		var $kiddies = $row.eq(y).children(),
			tallest = 0;

		// I know, one extra loop but we need to get each child and see which is tallest,
		// we can't just check the wrapper in the case of elements wrapping down on two lines!
		for (var x = $kiddies.length; 0 < x--;) { 
			var h = $kiddies.eq(x).height();
			if (h > tallest) tallest = h;
		}
		for (var z = $kiddies.length; 0 <= z--;) {
			var diff = tallest - $kiddies.eq(z).height();
			$kiddies.eq(z).css({
				paddingTop: diff / 2,
				paddingBottom: diff / 2
			});
		}
	}
	// for extra safety, make every sixth clear left...
	$row.find('.team:nth-child(6n)').css({ clear: 'left' });
}

window.onload = function () { // for some reason we need this on load and not doc ready...
	verticalAlign($('.team-list .row'));
};
$(window).on('resize', function () { verticalAlign($('.team-list .row')); });
// Match child to parent height
function matchChildToParentHeight(matchMe, adjustMe)
{
	var match	= matchMe.height(),
		adjust	= adjustMe.height();
	var diff	= match - adjust;

	if (adjust !== match) {
		adjustMe.css({
			paddingTop: diff/2,
			paddingBottom: diff/2
		});
	}
}


function menusInit() 
{
	menuToggle('.primary-nav');

	commodities_toggle();
	
	// Search button (desktop / mobile)
	nav_toggle('.nav-toggle-search');

	// Mobile navigation
	mobileMenu();
}

function isIE7() {
	return $('.ie7').length;
}
function isMobileDisplay() {
	return $('#mobileCheck:visible').length;
}

// 
function modals(opts) 
{
	opts = $.extend(
	{
		triggerClass : '.trigger-modal',
		cancelClass : '.btn-cancel',
		addCloseBtn : true,
		rate : 150
	}, opts);

	var $t = $(opts.triggerClass);
	if(!$t.length) return;
	
	var $win = $(window),
		$bg = $('<div class="modal-bg">').appendTo('body');

	var closeModal = function (e) // clickOutside //
	{
		if (e.currentTarget === e.target) { // not bubbling up // bg, no modal
			e.preventDefault();
			// console.log(e.target, e.currentTarget, e);
			$bg.fadeOut(opts.rate) // bg
				.find('> *').hide(); // all modals, too //
		}
	};

	$t.each(function (e, $targ) 
	{
		// console.log('https://www.intlfcstone.com/Scripts/this.hash', this.hash);
		$targ = $(this.hash).appendTo($bg);

		$targ.find(opts.cancelClass).add($bg).on('click', closeModal);

		if (opts.addCloseBtn) { // prepend an X btn to the modal //
			$('<a href="#" class="btn btn-close" title="close">X</a>')
				.prependTo($targ).on('click', closeModal);
		}

		$(this).on('click', function (e) 
		{
			e.preventDefault();
			$bg.fadeIn(opts.rate);
			$targ.css({left:0,top:0,display:'block',visibility:'hidden'}); // show it but still hide it, to be able to calculate correct width and height...
			var targTop  = ($win.height() - $targ.outerHeight()) / 2,
				targLeft = ($win.width() - $targ.outerWidth()) / 2;
			$targ.hide();
			$targ.css({ display: 'none', visibility: 'visible' }); // reset the styling...
			$targ.css({
				left: targLeft,
				top : targTop
			}).fadeIn(opts.rate);
		});
	});
}

function block_link() 
{
	if (typeof(clickable) === "function")
	$('[data-link]').removeAttr('data-link')
		.clickable('a:last');
	// .clickable in plugins.js //
}

//
function makeFollowBtn( linkContainer, buff ) 
{
	var	$par = $(linkContainer);
	if(!$par.length) return;

	buff = buff || 40;
	var	parO = $par.offset().top,
		parH = $par.innerHeight(),
		$btn = $par.find('a'),
		btnH = $btn.outerHeight(),
		$win = $(window);

	var follow = function() 
	{
		var winPos = $win.scrollTop() + $win.height(),
			btnPos = Math.round( parO + btnH + buff ),
			btnBtm = Math.round( parH - btnH ),
			btnWin = (winPos - btnPos);
		//console.log( 'winH', $win.height(), '/ winPos:', winPos );

		if (winPos > btnPos) 
		{
			var t = (btnWin < btnBtm) ?	btnWin : btnBtm;

			$btn.css({ top: t });
			// console.log( btnWin, '/ parH:', parH, '/ btnBtm:', btnBtm );
		} else 
		{
			$btn.css({ top: 0 });
		}
	};

	$win.scroll(follow).resize(follow);
}

//
function accordion (list, link, body, parentElement) 
{
    list = list || '.accordion-list';
	link = link || 'a.toggle';
	body = body || '.accordion-body'; //'ul, ol';
	if (! $(list).length ) return false;

	var $link = $(link, list),
		 clss = 'active',
		 rate = 150;

	if ( $(list).hasClass('accordion-large') ) 
	{
		rate = 300;
	}

	if (isMobileDisplay()) {
		$(list).removeClass(clss);
	}

	if (!isIE7()) {
		$(body, list).hide();
		$(body, list+'.'+clss).show();
	}

	var clickEventFunction = function (e) {
	    e.preventDefault();
	    var $this = $(this),
            $parent = $this.parent(),
            $sibs = $parent.siblings('li,' + list);

	    if (!isIE7()) {
	        var openAccordionItems = $sibs.find(body + ':visible');
	        if ($parent.hasClass('accordion-large') && openAccordionItems.length && $parent.index() > $parent.parent().find('.' + clss).index()) {
	            $('html,body').animate({ scrollTop: $(window).scrollTop() - openAccordionItems.height() }, rate);
	        }
	        $sibs.find(body + ':visible').slideUp(rate);
	        $this.next(body).slideToggle(rate);
	    }

	    $sibs.removeClass(clss);
	    $parent.toggleClass(clss);

	    if (isIE7())
	    {
	    	var $allSpans = $('a.toggle.h4 span');
	    	if ($allSpans.length) {
	    		$allSpans.innerHtml = '+';
	    	}
	    	if ($parent.hasClass(clss) && $this.hasClass('h4')) {
	    		var $span = $this.find('span');
	    		if ($span.length) {
	    			$span.innerHtml = '-';
	    		}
	    	}
	    }

	    // console.log( 'parent active:', $parent.hasClass(clss) );
	    $link.not(this).removeClass('selected');

	    var addClass = $parent.hasClass(clss);

	    $this.not('.toggle').toggleClass('selected', addClass);

	    if (addClass && $parent.hasClass('accordion-large')) {
	        setTimeout(function () {
	            $('html,body').animate({ scrollTop: $parent.offset().top }, rate);
	        }, rate);
	    }
	};

	if (!parentElement) {
		$link.on('click', clickEventFunction);
	}

	else {
        //binding to parent link instead of link because binding is lost on ajax calls on the contact page filter

		var $parentElement = $(parentElement);

	    $parentElement.find(link, list).off('click');

	    $parentElement.on('click', link, clickEventFunction);
	}

	if (isMobileDisplay()) {
		$(body).find('ul').show();
	}
}

// megamenu /////////////////////////////////
function menuToggle (list) 
{
	if (isMobileDisplay()) return;
	list += ' > ul';
	var $list = $(list),
		$link = $(list + ' > li > a'),
		 sub  = '.submenu',
		$sub  = $(sub),
		 sLnk = '.sub-caps a',
		 cap  = '.capability-sub',
		 clss = 'active',
		 rate = 200,
		 out;
	//console.log('menu toggle:', list);

	$link.on('click', function (e) 
	{
		e.preventDefault();
		var subM = $(this).next(sub),
			href = bestLink(this, true);
			href = $('.sub-1 ' + cap, subM)[0];

		// console.log( this, 'subM', subM[0] ); // if subM[0] == null, do search link func!
		
		// nav_toggle('.nav-toggle-search', null, true); // force close search

		$sub.not(subM).css('zIndex',8).slideUp(rate)
			.add($link).add(cap).add(sLnk).removeClass(clss);
		// console.log( 'subM', subM[0], subM.context, subM );

		$(subM).css('zIndex',9).slideDown(rate).add(href).add(this).addClass(clss);
		$(this).addClass(clss).parents('nav').addClass('inactive');
	});

	( ! isMobileDisplay() ) // only in desktop
		remoteToggle(sub, sLnk, cap); // subcaps in submenu //

	closePanel ($list, $(sub), $link, rate, clss);
}

var out;
function closePanel ($container, $target, $link, rate, clss) 
{
	rate = rate || 200;
	clss = clss || 'active';

	// Events ------------------------------------------
	var close = function () 
	{
		$target.slideUp(rate, function () {
			$(this).removeAttr('style')
				.add($link).removeClass(clss);
		});
	};

	var timeout = function () 
	{
		out = setTimeout(function(){
			close();
			clearTimeout(out);
		}, 400);
	};

	var clickOutside = function (e) 
	{
		if ((e.target !== $link) && (e.target !== $container)) {
			e.preventDefault();
			// console.log(e.target, e.currentTarget, $link, e);
			close();
		}
	};

	// Event Binding -----------------------------------
	$container
	.on('mouseover', function () {
		clearTimeout(out);
	})
	.on('mouseout', timeout);
	
	// $(window).on('click', clickOutside);
}

function remoteToggle (list, link, setTarg) 
{
	var $list = $(list),
		$link = $(link, list),
		 clss = 'active';
	// console.log('toggle:', list);

	$link.on('click', function (e) 
	{
		// console.log('click:', this);
		e.preventDefault();

		var href = bestLink(this, true);
		// console.log('href:', href);

		$link.add(setTarg).removeClass(clss);
		$(this).add(href).addClass(clss);
	});
}

//
function nav_toggle(click, target, force) 
{
	var clss	= 'active',
		rate	= 200,
		$target = $(target),
		$click	= $(click);

	if ( $click.length ) {
		$target = $( $click[0].hash );
	}

	//console.log(target, $target);
	var clickFunc = function (e) 
	{
		console.log('force', force);
		e.preventDefault();
		
		clearTimeout(out);

		$target//.slideToggle(rate)
			.add(this).toggleClass(clss)
			.find('input').eq(0).focus();

		// don't auto-close because it makes it too hard to use //
		// if (target !== false) closePanel ($target.parent(), $target, $click);
		// console.log('parent', $target.parent());
	};

	if (force) {
		clickFunc(null);
		return;
	}

	$click.on('click', clickFunc);
}

function mobileMenu() {
	if (Math.max(document.documentElement.clientWidth, window.innerWidth || 0) > '1024') return;
	var $burger = $('.icon-menu'),
		$nav = $('#megamenu'),
		$items = $nav.find('> ul > li > a'),
		$subnavs = $nav.find('.submenu'),
		$capItems = $items.next().find('.sub-caps > ul > li > a'),
		$allCaps = $nav.find('.capability-sub');

	// when coming to the page, parse all items and...
	for (var i = $items.length; 0 < i--;) {
		var $item = $items.eq(i);

		var itemClass = $item.hasClass('currentPage') ? ' currentPage' : '';

		// ...create headlines on subs and...
		$item.next().find('.wrapper').prepend('<a class="header' + itemClass + '" href="' + $item.attr('href') + '">' + $item.text() + ' Overview</a>');

		$item.removeClass('currentPage');

		// ...move the capabilites sub pages into a subnav and add a back button to them...
		var $caps = $item.next().find('.sub-caps a');
		for (var x = $caps.length; 0 < x--;) {
			var cap = $caps.eq(x).attr('data-href'), $cap
			cap = cap.substring(0, cap.indexOf(','));
			$cap = $(cap, $item.next()).insertAfter($caps.eq(x)).prepend('<div class="btn-mobmenu-prevSub"><i class="icon-arrow-mobmenu-prev"></i></div>');
			$cap.find('strong.h6 a').text($cap.find('strong.h6 a').text() + ' Overview');
		}
	}

	$burger.on('click', function (e) {
		e.preventDefault();
		if (!$burger.hasClass('active')) {
		    $burger.addClass('active');
			$nav.show(2,function () {
			    var $currentPageItem = $nav.find('.currentPage');
			    if ($currentPageItem.length) {
			    	$nav.addClass('notransition active');
			    	$currentPageItem.parents('.submenu').prev().click();
			    	$currentPageItem.parents('.capability-sub').prev().click();
			    	$nav.removeClass('notransition');
			    } else {
			    	$nav.addClass('active');
			    }
			    
			});
		} else {
			$burger.removeClass('active');
			$nav.removeClass('active subActive capActive');
			$subnavs.removeClass('active');
			setTimeout(function () { $nav.hide(); $subnavs.hide(); $allCaps.hide(); }, 300);
		}
	});

	$items.on('click', function (e) {
		e.preventDefault();
		var $this = $(this);
		$nav.addClass('subActive');
		$this.next().show(2, function () {
			$this.next().addClass('active');
		});
	});

	$capItems.on('click', function (e) {
		e.preventDefault();
		var $this = $(this);
		$nav.addClass('capActive');
		$this.next().show(2, function () {
			$this.next().addClass('active');
		});
	});

	$('.btn-mobmenu-prev').on('click', function (e) {
		e.preventDefault();
		$nav.removeClass('subActive');
		$subnavs.removeClass('active');
		setTimeout(function () { $subnavs.hide(); }, 300);
	});

	$('.btn-mobmenu-prevSub').on('click', function (e) {
		e.preventDefault();
		$nav.removeClass('capActive');
		$allCaps.removeClass('active');
		setTimeout(function () { $allCaps.hide(); }, 300);
	});
}

////////////////////////////////////////////////////////

function tablist (list, link, body) 
{
	var $list = list ? $(list) : $('.tab-list'),
		$link = link ? $(link) : $('[role="tablist"] a'),
		 body = body || '.tab-body', // others like this //
		 clss = 'active';
	
	//console.log(list);
	var setTarg;

	$link.on('click', function (e) 
	{
		e.preventDefault();

		if (! setTarg ) {
			var targ = $(e.target.hash)[0];
			// console.log('https://www.intlfcstone.com/Scripts/e.target.hash', e.target.hash);
			// console.log( targ, targ.className );
			setTarg = targ.className;
			body = '.' + setTarg;
		}
		//console.log( hash, targ, targ.className );
		//
		$link.add(body).removeClass(clss);
		$(this).add(this.hash).addClass(clss);
		// console.log('https://www.intlfcstone.com/Scripts/this.hash', this.hash);
	});
}

//storage() obsolete because cookies are now used. Cookies are used bacuase commodities navigation rendering is now moved rto server side. -ZG 6/2/2014
//function storage() {
//	return (typeof(Storage) !== "undefined") && (typeof(sessionStorage) !== "undefined");
//	//console.log( sessionStorage );
//}

function commodities_toggle(link) 
{
	link = link || '.nav-toggle-commodities';
	
	var clss = 'active',
		name = "commoditiesOpen",
	   $link = $(link),
		targ = $link[0].hash || $link.data('href');
		//open = true; // default open

	//Storage() now obsolete because cookies are used now. -ZG 6/2/2014
	//else {
	//	if (storage()) open = sessionStorage.getItem(name);// alert( 'open '+ open );
	//}
	// console.log( open, (open === "true"), (open == null) );

	//This is now set directly in the view -ZG 6/2/2014
	//if (open === "true" || open === null) {
	//	$link.add(targ).addClass(clss); // add class active
	//}

	var clickFunc = function (e, isActive) 
	{
		// var targ = this.hash || $(this).data('href');
		//console.log( 'targ', targ );

		e.preventDefault();

		isActive = !$(targ).hasClass(clss);

		if (!isActive) $('.commodities-wrapper').slideUp(250);

		$(targ).add(this).toggleClass(clss, isActive);

		if (isActive) $('.commodities-wrapper').slideDown(250);

		document.cookie = name + "=" + isActive + ";path=/";

		//if (storage()) {
		//    sessionStorage.setItem(name, isActive);
			
		//    //console.log('cookie', name + "=" + isActive);
		//}
	};

	// Event Binding //-------------------------------//
	$link.on('click', clickFunc);
}
//*/
//

function toggle_thru_objs(click) 
{
	var $cliks = $(click),
		$targs = $();
	$cliks.each(function (i, target)
	{
		target = this.hash; //console.log('target', target);
		$targs = $targs.add(target); //console.log('$targs', $targs);

		$(this).on('click', function (e) {
			e.preventDefault();
			$cliks.not(this).add( $targs.not(target) ).removeClass('active');
			$(this).add(target).toggleClass('active');
		});
	});
}

//
function bestLink(targ, ignoreHref) 
{
	var dataHref = $(targ).data('href'),
		href	 = dataHref || targ.hash;

	if (ignoreHref === null || ignoreHref === false) {
		href = href || targ.href;
		//console.log('ignoreHref', ignoreHref);
	}
	// console.log('data-href:', dataHref );
	// console.log('targ hash:', targ.hash );
	// console.log('targ href:', targ.href );
	// console.log('href:', href );
	return href;
}

//
function allHashLinks(func) {
	$('a[href*=#]').not('a[href=#]').each(function () {
		//if (!this.hash) return;
		console.log('/', this.hash);
		if (typeof func === 'function') func(this);
	});
}

var wzpHelper = wzpHelper || {};
wzpHelper = (function ($, w, d) {
	var mobileView = function () { return !!($(window).width() <= 598); }, // we actually only care about viewport size
		origView;

	function setHtmlTagClass() {
		if (mobileView() !== origView) {
			$('html').removeClass('mobile desktop').addClass((mobileView()) ? 'mobile' : 'desktop');
			origView = mobileView();
			$(d).trigger('mobileViewChange'); // trigger deviceChange on document level, so listeners can act accordingly
		}
	}

	setHtmlTagClass();

	$(w).on('resize', function () {
		setHtmlTagClass(); // no need to throttle, it's a fast check.
	});
})(jQuery, window, document);

// PLUGINS ///////////////////////
/*
$.fn.toggle = function (target) {
	return this.each(function () {
		var $this = $(this);
	});
};
*/



// RESPONSIVE FIX

// button for scrolling back to top - on phone only
$(window).on('scroll', function () {         
    var y_scroll_pos = window.pageYOffset;  // when scrolled past a certain point
    var scroll_pos_test = 400;

    if (y_scroll_pos > scroll_pos_test) { // do something when scrolled past that point
        $("#goToTop").fadeIn(500);
    }

    else {
        $("#goToTop").fadeOut(150);
    }
});

// the actual button click action
$("#goToTop").on("click", function () {
    $("body").animate({ scrollTop: 0 }, 500);
});

function fixExternalLinks() {
    // all external links should open in new window
    setTimeout(function () {
        $('a').each(function () {
            var linkHostName = this.hostname.toLowerCase();
            if (linkHostName != '' && linkHostName !== window.location.hostname.toLowerCase()) {
                $(this).attr('target', '_blank');
            }
        });
    }, 400);
}