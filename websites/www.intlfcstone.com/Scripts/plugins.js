/* Placeholders.js v3.0.2 */
(function(t){"use strict";function e(t,e,r){return t.addEventListener?t.addEventListener(e,r,!1):t.attachEvent?t.attachEvent("on"+e,r):void 0}function r(t,e){var r,n;for(r=0,n=t.length;n>r;r++)if(t[r]===e)return!0;return!1}function n(t,e){var r;t.createTextRange?(r=t.createTextRange(),r.move("character",e),r.select()):t.selectionStart&&(t.focus(),t.setSelectionRange(e,e))}function a(t,e){try{return t.type=e,!0}catch(r){return!1}}t.Placeholders={Utils:{addEventListener:e,inArray:r,moveCaret:n,changeType:a}}})(this),function(t){"use strict";function e(){}function r(){try{return document.activeElement}catch(t){}}function n(t,e){var r,n,a=!!e&&t.value!==e,u=t.value===t.getAttribute(V);return(a||u)&&"true"===t.getAttribute(P)?(t.removeAttribute(P),t.value=t.value.replace(t.getAttribute(V),""),t.className=t.className.replace(R,""),n=t.getAttribute(z),parseInt(n,10)>=0&&(t.setAttribute("maxLength",n),t.removeAttribute(z)),r=t.getAttribute(D),r&&(t.type=r),!0):!1}function a(t){var e,r,n=t.getAttribute(V);return""===t.value&&n?(t.setAttribute(P,"true"),t.value=n,t.className+=" "+I,r=t.getAttribute(z),r||(t.setAttribute(z,t.maxLength),t.removeAttribute("maxLength")),e=t.getAttribute(D),e?t.type="text":"password"===t.type&&K.changeType(t,"text")&&t.setAttribute(D,"password"),!0):!1}function u(t,e){var r,n,a,u,i,l,o;if(t&&t.getAttribute(V))e(t);else for(a=t?t.getElementsByTagName("input"):f,u=t?t.getElementsByTagName("textarea"):h,r=a?a.length:0,n=u?u.length:0,o=0,l=r+n;l>o;o++)i=r>o?a[o]:u[o-r],e(i)}function i(t){u(t,n)}function l(t){u(t,a)}function o(t){return function(){b&&t.value===t.getAttribute(V)&&"true"===t.getAttribute(P)?K.moveCaret(t,0):n(t)}}function c(t){return function(){a(t)}}function s(t){return function(e){return A=t.value,"true"===t.getAttribute(P)&&A===t.getAttribute(V)&&K.inArray(C,e.keyCode)?(e.preventDefault&&e.preventDefault(),!1):void 0}}function d(t){return function(){n(t,A),""===t.value&&(t.blur(),K.moveCaret(t,0))}}function v(t){return function(){t===r()&&t.value===t.getAttribute(V)&&"true"===t.getAttribute(P)&&K.moveCaret(t,0)}}function g(t){return function(){i(t)}}function p(t){t.form&&(T=t.form,"string"==typeof T&&(T=document.getElementById(T)),T.getAttribute(U)||(K.addEventListener(T,"submit",g(T)),T.setAttribute(U,"true"))),K.addEventListener(t,"focus",o(t)),K.addEventListener(t,"blur",c(t)),b&&(K.addEventListener(t,"keydown",s(t)),K.addEventListener(t,"keyup",d(t)),K.addEventListener(t,"click",v(t))),t.setAttribute(j,"true"),t.setAttribute(V,x),(b||t!==r())&&a(t)}var f,h,b,m,A,y,E,x,L,T,S,N,w,B=["text","search","url","tel","email","password","number","textarea"],C=[27,33,34,35,36,37,38,39,40,8,46],k="#ccc",I="placeholdersjs",R=RegExp("(?:^|\\s)"+I+"(?!\\S)"),V="data-placeholder-value",P="data-placeholder-active",D="data-placeholder-type",U="data-placeholder-submit",j="data-placeholder-bound",q="data-placeholder-focus",Q="data-placeholder-live",z="data-placeholder-maxlength",F=document.createElement("input"),G=document.getElementsByTagName("head")[0],H=document.documentElement,J=t.Placeholders,K=J.Utils;if(J.nativeSupport=void 0!==F.placeholder,!J.nativeSupport){for(f=document.getElementsByTagName("input"),h=document.getElementsByTagName("textarea"),b="false"===H.getAttribute(q),m="false"!==H.getAttribute(Q),y=document.createElement("style"),y.type="text/css",E=document.createTextNode("."+I+" { color:"+k+"; }"),y.styleSheet?y.styleSheet.cssText=E.nodeValue:y.appendChild(E),G.insertBefore(y,G.firstChild),w=0,N=f.length+h.length;N>w;w++)S=f.length>w?f[w]:h[w-f.length],x=S.attributes.placeholder,x&&(x=x.nodeValue,x&&K.inArray(B,S.type)&&p(S));L=setInterval(function(){for(w=0,N=f.length+h.length;N>w;w++)S=f.length>w?f[w]:h[w-f.length],x=S.attributes.placeholder,x?(x=x.nodeValue,x&&K.inArray(B,S.type)&&(S.getAttribute(j)||p(S),(x!==S.getAttribute(V)||"password"===S.type&&!S.getAttribute(D))&&("password"===S.type&&!S.getAttribute(D)&&K.changeType(S,"text")&&S.setAttribute(D,"password"),S.value===S.getAttribute(V)&&(S.value=x),S.setAttribute(V,x)))):S.getAttribute(P)&&(n(S),S.removeAttribute(V));m||clearInterval(L)},100)}K.addEventListener(t,"beforeunload",function(){J.disable()}),J.disable=J.nativeSupport?e:i,J.enable=J.nativeSupport?e:l}(this),function(t){"use strict";var e=t.fn.val,r=t.fn.prop;Placeholders.nativeSupport||(t.fn.val=function(t){var r=e.apply(this,arguments),n=this.eq(0).data("placeholder-value");return void 0===t&&this.eq(0).data("placeholder-active")&&r===n?"":r},t.fn.prop=function(t,e){return void 0===e&&this.eq(0).data("placeholder-active")&&"value"===t?"":r.apply(this,arguments)})}(jQuery);

/*! placeholder text: http://mths.be/placeholder v2.0.7 by @mathias */
(function (f, h, $) { var a = 'placeholder' in h.createElement('input'), d = 'placeholder' in h.createElement('textarea'), i = $.fn, c = $.valHooks, k, j; if (a && d) { j = i.placeholder = function () { return this }; j.input = j.textarea = true } else { j = i.placeholder = function () { var l = this; l.filter((a ? 'textarea' : ':input') + '[placeholder]').not('.placeholder').bind({ 'focus.placeholder': b, 'blur.placeholder': e }).data('placeholder-enabled', true).trigger('blur.placeholder'); return l }; j.input = a; j.textarea = d; k = { get: function (m) { var l = $(m); return l.data('placeholder-enabled') && l.hasClass('placeholder') ? '' : m.value }, set: function (m, n) { var l = $(m); if (!l.data('placeholder-enabled')) { return m.value = n } if (n == '') { m.value = n; if (m != h.activeElement) { e.call(m) } } else { if (l.hasClass('placeholder')) { b.call(m, true, n) || (m.value = n) } else { m.value = n } } return l } }; a || (c.input = k); d || (c.textarea = k); $(function () { $(h).delegate('form', 'submit.placeholder', function () { var l = $('.placeholder', this).each(b); setTimeout(function () { l.each(e) }, 10) }) }); $(f).bind('beforeunload.placeholder', function () { $('.placeholder').each(function () { this.value = '' }) }) } function g(m) { var l = {}, n = /^jQuery\d+$/; $.each(m.attributes, function (p, o) { if (o.specified && !n.test(o.name)) { l[o.name] = o.value } }); return l } function b(m, n) { var l = this, o = $(l); if (l.value == o.attr('placeholder') && o.hasClass('placeholder')) { if (o.data('placeholder-password')) { o = o.hide().next().show().attr('id', o.removeAttr('id').data('placeholder-id')); if (m === true) { return o[0].value = n } o.focus() } else { l.value = ''; o.removeClass('placeholder'); l == h.activeElement && l.select() } } } function e() { var q, l = this, p = $(l), m = p, o = this.id; if (l.value == '') { if (l.type == 'password') { if (!p.data('placeholder-textinput')) { try { q = p.clone().attr({ type: 'text' }) } catch (n) { q = $('<input>').attr($.extend(g(this), { type: 'text' })) } q.removeAttr('name').data({ 'placeholder-password': true, 'placeholder-id': o }).bind('focus.placeholder', b); p.data({ 'placeholder-textinput': q, 'placeholder-id': o }).before(q) } p = p.removeAttr('id').hide().prev().attr('id', o).show() } p.addClass('placeholder'); p[0].value = p.attr('placeholder') } else { p.removeClass('placeholder') } } }(this, document, jQuery));

// CONSOLE FALLBACK // Avoid errors in browsers that lack a console
(function(){var a,noop=function(){},methods=['assert','clear','count','debug','dir','dirxml','error','exception','group','groupCollapsed','groupEnd','info','log','markTimeline','profile','profileEnd','table','time','timeEnd','timeStamp','trace','warn'],length=methods.length,console=(window.console=window.console||{});while(length--){a=methods[length];if(!console[a]){console[a]=noop}}}());

// jquery.pseudo.js // http://jquery.lukelutman.com/plugins/pseudo/
(function($){
	var patterns = {
		text: /^['"]?(.+?)["']?$/,
		url: /^url\(["']?(.+?)['"]?\)$/
	};
	function clean(content) {
		if(content && content.length) {
			var text = content.match(patterns.text)[1],
				url = text.match(patterns.url);
			return url ? '<img src="' + url[1] + '" />' : text;
		}
	}
	function inject(prop, elem, content) {
		if(prop != 'after') prop = 'before';
		if(content = clean(elem.currentStyle[prop])) {
			$(elem)[prop == 'before' ? 'prepend' : 'append'](
				$(document.createElement('span')).addClass(prop).html(content)
			);
		}
	}
	$.pseudo = function(elem) {
		inject('before', elem);
		inject('after', elem);
		elem.runtimeStyle.behavior = null;
	};
	if(document.createStyleSheet) {
		var o = document.createStyleSheet(null, 0);
		o.addRule('.dummy','display: static;');
		o.cssText = 'html, head, head *, body, *.before, *.after, *.before *, *.after * { behavior: none; } * { behavior: expression($.pseudo(this)); }';
	}
})(jQuery);

// TODO: Hear with Allison why this is in here?
$ = jQuery;

// Actually working imagesLoaded
$.fn.imagesLoaded = function (callback) { var elems = this.filter('img'), len = elems.length, blank = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw=="; elems.bind('load.imgloaded', function () { if (--len <= 0 && this.src !== blank) { elems.unbind('load.imgloaded'); callback.call(elems, this); } }).each(function () { if (this.complete || this.complete === undefined) { var src = this.src; this.src = blank; this.src = src; } }); return this; };

/*jQuery Easing v1.3: for animation easing (used on timeline) - http://gsgd.co.uk/sandbox/jquery/easing/ Uses the built in easing capabilities added In jQuery 1.1 to offer multiple easing options*/
jQuery.easing['jswing'] = jQuery.easing['swing']; jQuery.extend(jQuery.easing, { def: 'easeOutQuad', swing: function (x, t, b, c, d) { return jQuery.easing[jQuery.easing.def](x, t, b, c, d) }, easeInQuad: function (x, t, b, c, d) { return c * (t /= d) * t + b }, easeOutQuad: function (x, t, b, c, d) { return -c * (t /= d) * (t - 2) + b }, easeInOutQuad: function (x, t, b, c, d) { if ((t /= d / 2) < 1) return c / 2 * t * t + b; return -c / 2 * ((--t) * (t - 2) - 1) + b }, easeInCubic: function (x, t, b, c, d) { return c * (t /= d) * t * t + b }, easeOutCubic: function (x, t, b, c, d) { return c * ((t = t / d - 1) * t * t + 1) + b }, easeInOutCubic: function (x, t, b, c, d) { if ((t /= d / 2) < 1) return c / 2 * t * t * t + b; return c / 2 * ((t -= 2) * t * t + 2) + b }, easeInQuart: function (x, t, b, c, d) { return c * (t /= d) * t * t * t + b }, easeOutQuart: function (x, t, b, c, d) { return -c * ((t = t / d - 1) * t * t * t - 1) + b }, easeInOutQuart: function (x, t, b, c, d) { if ((t /= d / 2) < 1) return c / 2 * t * t * t * t + b; return -c / 2 * ((t -= 2) * t * t * t - 2) + b }, easeInQuint: function (x, t, b, c, d) { return c * (t /= d) * t * t * t * t + b }, easeOutQuint: function (x, t, b, c, d) { return c * ((t = t / d - 1) * t * t * t * t + 1) + b }, easeInOutQuint: function (x, t, b, c, d) { if ((t /= d / 2) < 1) return c / 2 * t * t * t * t * t + b; return c / 2 * ((t -= 2) * t * t * t * t + 2) + b }, easeInSine: function (x, t, b, c, d) { return -c * Math.cos(t / d * (Math.PI / 2)) + c + b }, easeOutSine: function (x, t, b, c, d) { return c * Math.sin(t / d * (Math.PI / 2)) + b }, easeInOutSine: function (x, t, b, c, d) { return -c / 2 * (Math.cos(Math.PI * t / d) - 1) + b }, easeInExpo: function (x, t, b, c, d) { return (t == 0) ? b : c * Math.pow(2, 10 * (t / d - 1)) + b }, easeOutExpo: function (x, t, b, c, d) { return (t == d) ? b + c : c * (-Math.pow(2, -10 * t / d) + 1) + b }, easeInOutExpo: function (x, t, b, c, d) { if (t == 0) return b; if (t == d) return b + c; if ((t /= d / 2) < 1) return c / 2 * Math.pow(2, 10 * (t - 1)) + b; return c / 2 * (-Math.pow(2, -10 * --t) + 2) + b }, easeInCirc: function (x, t, b, c, d) { return -c * (Math.sqrt(1 - (t /= d) * t) - 1) + b }, easeOutCirc: function (x, t, b, c, d) { return c * Math.sqrt(1 - (t = t / d - 1) * t) + b }, easeInOutCirc: function (x, t, b, c, d) { if ((t /= d / 2) < 1) return -c / 2 * (Math.sqrt(1 - t * t) - 1) + b; return c / 2 * (Math.sqrt(1 - (t -= 2) * t) + 1) + b }, easeInElastic: function (x, t, b, c, d) { var s = 1.70158; var p = 0; var a = c; if (t == 0) return b; if ((t /= d) == 1) return b + c; if (!p) p = d * .3; if (a < Math.abs(c)) { a = c; var s = p / 4 } else var s = p / (2 * Math.PI) * Math.asin(c / a); return -(a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p)) + b }, easeOutElastic: function (x, t, b, c, d) { var s = 1.70158; var p = 0; var a = c; if (t == 0) return b; if ((t /= d) == 1) return b + c; if (!p) p = d * .3; if (a < Math.abs(c)) { a = c; var s = p / 4 } else var s = p / (2 * Math.PI) * Math.asin(c / a); return a * Math.pow(2, -10 * t) * Math.sin((t * d - s) * (2 * Math.PI) / p) + c + b }, easeInOutElastic: function (x, t, b, c, d) { var s = 1.70158; var p = 0; var a = c; if (t == 0) return b; if ((t /= d / 2) == 2) return b + c; if (!p) p = d * (.3 * 1.5); if (a < Math.abs(c)) { a = c; var s = p / 4 } else var s = p / (2 * Math.PI) * Math.asin(c / a); if (t < 1) return -.5 * (a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p)) + b; return a * Math.pow(2, -10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p) * .5 + c + b }, easeInBack: function (x, t, b, c, d, s) { if (s == undefined) s = 1.70158; return c * (t /= d) * t * ((s + 1) * t - s) + b }, easeOutBack: function (x, t, b, c, d, s) { if (s == undefined) s = 1.70158; return c * ((t = t / d - 1) * t * ((s + 1) * t + s) + 1) + b }, easeInOutBack: function (x, t, b, c, d, s) { if (s == undefined) s = 1.70158; if ((t /= d / 2) < 1) return c / 2 * (t * t * (((s *= (1.525)) + 1) * t - s)) + b; return c / 2 * ((t -= 2) * t * (((s *= (1.525)) + 1) * t + s) + 2) + b }, easeInBounce: function (x, t, b, c, d) { return c - jQuery.easing.easeOutBounce(x, d - t, 0, c, d) + b }, easeOutBounce: function (x, t, b, c, d) { if ((t /= d) < (1 / 2.75)) { return c * (7.5625 * t * t) + b } else if (t < (2 / 2.75)) { return c * (7.5625 * (t -= (1.5 / 2.75)) * t + .75) + b } else if (t < (2.5 / 2.75)) { return c * (7.5625 * (t -= (2.25 / 2.75)) * t + .9375) + b } else { return c * (7.5625 * (t -= (2.625 / 2.75)) * t + .984375) + b } }, easeInOutBounce: function (x, t, b, c, d) { if (t < d / 2) return jQuery.easing.easeInBounce(x, t * 2, 0, c, d) * .5 + b; return jQuery.easing.easeOutBounce(x, t * 2 - d, 0, c, d) * .5 + c * .5 + b } });

//Lightweight scrollto plugin
$.fn.scrollView=function(){return this.each(function(){$('html,body').animate({scrollTop:$(this).offset().top},700,"easeOutExpo");});}

// nSwipeSlider
; (function ($) {
	$.fn.nSwipeSlider = function (userOptions) {
		// do not load plugins twice
		//console.warn("Being called from " + arguments.callee.caller.toString());
		if (typeof (jQuery.event.special.movestart) == 'undefined') (function (a) { if (typeof define === "function" && define.amd) { define(["jquery"], a) } else { a(jQuery) } })(function (s, j) { var c = 6, e = s.event.add, v = s.event.remove, y = function (R, Q, S) { s.event.trigger(Q, S, R) }, C = (function () { return (window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || function (R, Q) { return window.setTimeout(function () { R() }, 25) }) })(), g = { textarea: true, input: true, select: true, button: true, video: true, embed: true, object: true }, i = { move: "mousemove", cancel: "mouseup dragstart", end: "mouseup" }, d = { move: "touchmove", cancel: "touchend", end: "touchend" }; function n(S) { var U = S, T = false, Q = false; function R(V) { if (T) { U(); C(R); Q = true; T = false } else { Q = false } } this.kick = function (V) { T = true; if (!Q) { R() } }; this.end = function (W) { var V = U; if (!W) { return } if (!Q) { W() } else { U = T ? function () { V(); W() } : W; T = true } } } function K() { return true } function P() { return false } function B(Q) { Q.preventDefault() } function N(Q) { if (g[Q.target.tagName.toLowerCase()]) { return } Q.preventDefault() } function F(Q) { return (Q.which === 1 && !Q.ctrlKey && !Q.altKey) } function L(S, T) { var R, Q; if (S.identifiedTouch) { return S.identifiedTouch(T) } R = -1; Q = S.length; while (++R < Q) { if (S[R].identifier === T) { return S[R] } } } function l(R, Q) { var S = L(R.changedTouches, Q.identifier); if (!S) { return } if (S.pageX === Q.pageX && S.pageY === Q.pageY) { return } return S } function o(R) { var Q; if (!F(R)) { return } Q = { target: R.target, startX: R.pageX, startY: R.pageY, timeStamp: (typeof (R.timeStamp) == "undefined") ? new Date().getTime() : R.timeStamp }; e(document, i.move, u, Q); e(document, i.cancel, x, Q) } function u(R) { var Q = R.data; r(R, Q, R, z) } function x(Q) { z() } function z() { v(document, i.move, u); v(document, i.cancel, z) } function O(R) { var S, Q; if (g[R.target.tagName.toLowerCase()]) { return } S = R.changedTouches[0]; Q = { target: S.target, startX: S.pageX, startY: S.pageY, timeStamp: new Date().getTime(), identifier: S.identifier }; e(document, d.move + "." + S.identifier, p, Q); e(document, d.cancel + "." + S.identifier, D, Q) } function p(R) { var Q = R.data, S = l(R, Q); if (!S) { return } r(R, Q, S, M) } function D(R) { var Q = R.data, S = L(R.changedTouches, Q.identifier); if (!S) { return } M(Q.identifier) } function M(Q) { v(document, "." + Q, p); v(document, "." + Q, D) } function r(U, T, V, S) { var R = V.pageX - T.startX, Q = V.pageY - T.startY; if ((R * R) + (Q * Q) < (c * c)) { return } q(U, T, V, R, Q, S) } function H() { this._handled = K; return false } function J(Q) { Q._handled() } function q(U, Y, S, X, V, W) { var R = Y.target, T, Q; T = U.targetTouches; Q = ((typeof (U.timeStamp) == "undefined") ? new Date().getTime() : U.timeStamp) - Y.timeStamp; Y.type = "movestart"; Y.distX = X; Y.distY = V; Y.deltaX = X; Y.deltaY = V; Y.pageX = S.pageX; Y.pageY = S.pageY; Y.velocityX = X / Q; Y.velocityY = V / Q; Y.targetTouches = T; Y.finger = T ? T.length : 1; Y._handled = H; Y._preventTouchmoveDefault = function () { U.preventDefault() }; y(Y.target, Y); W(Y.identifier) } function G(R) { var Q = R.data.event, S = R.data.timer; E(Q, R, ((typeof (R.timeStamp) == "undefined") ? new Date().getTime() : R.timeStamp), S) } function b(R) { var Q = R.data.event, S = R.data.timer; a(); w(Q, S, function () { setTimeout(function () { v(Q.target, "click", P) }, 0) }) } function a(Q) { v(document, i.move, G); v(document, i.end, b) } function A(R) { var Q = R.data.event, T = R.data.timer, S = l(R, Q); if (!S) { return } R.preventDefault(); Q.targetTouches = R.targetTouches; E(Q, S, ((typeof (R.timeStamp) == "undefined") ? new Date().getTime() : R.timeStamp), T) } function h(R) { var Q = R.data.event, T = R.data.timer, S = L(R.changedTouches, Q.identifier); if (!S) { return } k(Q); w(Q, T) } function k(Q) { v(document, "." + Q.identifier, A); v(document, "." + Q.identifier, h) } function E(R, U, Q, T) { var S = Q - R.timeStamp; R.type = "move"; R.distX = U.pageX - R.startX; R.distY = U.pageY - R.startY; R.deltaX = U.pageX - R.pageX; R.deltaY = U.pageY - R.pageY; R.velocityX = 0.3 * R.velocityX + 0.7 * R.deltaX / S; R.velocityY = 0.3 * R.velocityY + 0.7 * R.deltaY / S; R.pageX = U.pageX; R.pageY = U.pageY; T.kick() } function w(R, S, Q) { S.end(function () { R.type = "moveend"; y(R.target, R); return Q && Q() }) } function f(S, R, Q) { e(this, "https://www.intlfcstone.com/Scripts/movestart.move", J); return true } function t(Q) { v(this, "dragstart drag", B); v(this, "mousedown touchstart", N); v(this, "movestart", J); return true } function I(Q) { if (Q.namespace === "move" || Q.namespace === "moveend") { return } e(this, "dragstart." + Q.guid + " drag." + Q.guid, B, j, Q.selector); e(this, "mousedown." + Q.guid, N, j, Q.selector) } function m(Q) { if (Q.namespace === "move" || Q.namespace === "moveend") { return } v(this, "dragstart." + Q.guid + " drag." + Q.guid); v(this, "mousedown." + Q.guid) } s.event.special.movestart = { setup: f, teardown: t, add: I, remove: m, _default: function (S) { var Q, R; if (!S._handled()) { return } Q = { target: S.target, startX: S.startX, startY: S.startY, pageX: S.pageX, pageY: S.pageY, distX: S.distX, distY: S.distY, deltaX: S.deltaX, deltaY: S.deltaY, velocityX: S.velocityX, velocityY: S.velocityY, timeStamp: S.timeStamp, identifier: S.identifier, targetTouches: S.targetTouches, finger: S.finger }; R = { event: Q, timer: new n(function (T) { y(S.target, Q) }) }; if (S.identifier === j) { e(S.target, "click", P); e(document, i.move, G, R); e(document, i.end, b, R) } else { S._preventTouchmoveDefault(); e(document, d.move + "." + S.identifier, A, R); e(document, d.end + "." + S.identifier, h, R) } } }; s.event.special.move = { setup: function () { e(this, "https://www.intlfcstone.com/Scripts/movestart.move", s.noop) }, teardown: function () { v(this, "https://www.intlfcstone.com/Scripts/movestart.move", s.noop) } }; s.event.special.moveend = { setup: function () { e(this, "movestart.moveend", s.noop) }, teardown: function () { v(this, "movestart.moveend", s.noop) } }; e(document, "https://www.intlfcstone.com/Scripts/mousedown.move", o); e(document, "https://www.intlfcstone.com/Scripts/touchstart.move", O); if (typeof Array.prototype.indexOf === "function") { (function (S, T) { var R = ["changedTouches", "targetTouches"], Q = R.length; while (Q--) { if (S.event.props.indexOf(R[Q]) === -1) { S.event.props.push(R[Q]) } } })(s) } });
		if (typeof (jQuery.event.special.swipe) == 'undefined') (function (a) { if (typeof define === "function" && define.amd) { define(["jquery"], a) } else { a(jQuery) } })(function (g, h) { var f = g.event.add, c = g.event.remove, d = function (j, i, k) { g.event.trigger(i, k, j) }, e = { threshold: 0.4, sensitivity: 6 }; function b(l) { var i, j, k; i = l.target.offsetWidth; j = l.target.offsetHeight; k = { distX: l.distX, distY: l.distY, velocityX: l.velocityX, velocityY: l.velocityY, finger: l.finger }; if (l.distX > l.distY) { if (l.distX > -l.distY) { if (l.distX / i > e.threshold || l.velocityX * l.distX / i * e.sensitivity > 1) { k.type = "swiperight"; d(l.currentTarget, k) } } else { if (-l.distY / j > e.threshold || l.velocityY * l.distY / i * e.sensitivity > 1) { k.type = "swipeup"; d(l.currentTarget, k) } } } else { if (l.distX > -l.distY) { if (l.distY / j > e.threshold || l.velocityY * l.distY / i * e.sensitivity > 1) { k.type = "swipedown"; d(l.currentTarget, k) } } else { if (-l.distX / i > e.threshold || l.velocityX * l.distX / i * e.sensitivity > 1) { k.type = "swipeleft"; d(l.currentTarget, k) } } } } function a(i) { var j = g.data(i, "event_swipe"); if (!j) { j = { count: 0 }; g.data(i, "event_swipe", j) } return j } g.event.special.swipe = g.event.special.swipeleft = g.event.special.swiperight = g.event.special.swipeup = g.event.special.swipedown = { setup: function (k, j, i) { var k = a(this); if (k.count++ > 0) { return } f(this, "moveend", b); return true }, teardown: function () { var i = a(this); if (--i.count > 0) { return } c(this, "moveend", b); return true }, settings: e } });

		var defaults = { wrapAround: true, nonTransSpeed: 200, autoSlide: 0, destroy: false },
			options = $.extend(defaults, userOptions),
			that = this,
			hasTrans = (function (a, b) { a = (new Image).style; b = 'ransition'; return 't' + b in a || 'webkitT' + b in a || 'MozT' + b in a || 'OT' + b in a })(),
			transEnd = (function () { var t, el = document.createElement('fakeelement'), transitions = { 'transition': 'transitionend', 'OTransition': 'otransitionend', 'MSTransition': 'msTransitionEnd', 'MozTransition': 'transitionend', 'WebkitTransition': 'webkitTransitionEnd' }; for (t in transitions) { if (el.style[t] !== undefined) { return transitions[t]; } } })();

		$.fn.nSwipeSlider.destroy = function () {
			if (that.data("nSwipeSlider").wrapped === true) { // did we have wrap around? if so, remove extra elements we added
				that.children().eq(that.children().length - 1).remove();
				that.children().eq(0).remove();
			}
			that.off('movestart, move, moveend, swipeleft, swiperight, slideTo');
			that.removeData('nSwipeSlider');
		}

		return this.each(function () {
			var $this = $(this),
				alreadyApplied = typeof ($this.data('nSwipeSlider')) != 'undefined'
			if (alreadyApplied) return;
			$this.data('nSwipeSlider', { loaded: true, wrapped: options.wrapAround });
			if (options.wrapAround) {
				$this.children().eq(0).clone(true).appendTo($this).removeClass('active');
				$this.children().eq($this.children().length - 2).clone(true).prependTo($this);

				if (!!$this.children().eq(0).find('.videoPlayerWrapper').attr('id')) {
					$this.children().eq(0).find('.videoPlayerWrapper').attr('id', 'copied_' + $this.children().eq(0).find('.videoPlayerWrapper').attr('id'));
					$this.children().eq(0).find('.videoLink').attr('data-videoid', $this.children().eq(0).find('.videoPlayerWrapper').attr('id'));
				}
			}

			var $wrap = $this,
				$items = $wrap.children(),
				active = $items.filter('.active'),
				i = $items.index(active),
				width = $wrap.width(),
				autoSlideTimer = null;

			$items.on('movestart', function (e) {
				$wrap.addClass('notransition').children().removeClass('notransition');
				width = $wrap.width(); // update width in case of resize

				if ((e.distX > e.distY && e.distX < -e.distY) ||
				    (e.distX < e.distY && e.distX > -e.distY)) {
					e.preventDefault();
					return;
				}
				$wrap.trigger('nSwipeSlider.swipeStart', [$(this), i]);
			}).on('move', function (e) {
				var left = 100 * e.distX / width;

				if (e.distX < 0) {
					if ($items[i + 1]) {
						$items[i].style.left = left + '%';
						$items[i + 1].style.left = (left + 100) + '%';
					}
					else {
						$items[i].style.left = left / 4 + '%';
					}
				}
				if (e.distX > 0) {
					if ($items[i - 1]) {
						$items[i].style.left = left + '%';
						$items[i - 1].style.left = (left - 100) + '%';
					}
					else {
						$items[i].style.left = left / 5 + '%';
					}
				}
			}).on('moveend', function (e) {
				if (hasTrans) {
					$wrap.removeClass('notransition').children().removeClass('notransition');
					resetStyleTag();
				} else {
					$items.eq(i).animate({ left: 0 }, options.nonTransSpeed, function () { $wrap.removeClass('notransition'); resetStyleTag(); });
					$items.eq(i).prev('li').animate({ left: '-100%' }, options.nonTransSpeed);
					$items.eq(i).next('li').animate({ left: '100%' }, options.nonTransSpeed);
				}
			}).on('swipeleft', function (e) {
				if (i === $items.length - 1) { return; }
				$items.eq(i + 1).trigger('slideTo');
			}).on('swiperight', function (e) {
				if (i === 0) { return; }
				$items.eq(i - 1).trigger('slideTo');
			}).on('slideTo', function (e) {
				if (hasTrans) {

					$items.addClass('notransition').removeClass('previous').eq(i).removeClass('active notransition').addClass('previous');
					$(e.target).removeClass('notransition').addClass('active');
					e.target.addEventListener(transEnd, triggerDoneEvent, true);

					i = $items.index(e.target);
				} else {
					$items.stop(true);
					var direction = (i > $items.index(e.target)) ? '100%' : '-100%';
					$items.eq(i).animate({ left: direction }, options.nonTransSpeed, function () {
						$(this).removeClass('active');
						$(e.target).addClass('active');
						i = $items.index(e.target);
						if (options.wrapAround) {
							changeCurrent();
						}

						setTimeout(resetStyleTag, 20);
					});
					$(e.target).animate({ left: 0 }, options.nonTransSpeed, function () {

						$wrap.trigger('nSwipeSlider.swipeDone', [$(this), i]);
					});
				}
			});

			function changeCurrent() {

				if (i === 0) {
					$wrap.addClass('notransition');
					$items.eq(i).removeClass('active').end().eq($items.length - 2).addClass('active');
					i = $items.length - 2;
					setTimeout(function () { $wrap.removeClass('notransition'); }, options.nonTransSpeed);
				} else if (i === ($items.length - 1)) {
					$wrap.addClass('notransition');
					$items.eq($items.length - 1).removeClass('active').end().eq(1).addClass('active');
					$items.eq($items.length - 1)[0].style.left = '';
					i = 1;
					setTimeout(function () { $wrap.removeClass('notransition'); }, options.nonTransSpeed);
				}
			}

			if (options.wrapAround && hasTrans) {
				$items.eq(0)[0].addEventListener(transEnd, changeCurrent, true);
				$items.eq($items.length - 1)[0].addEventListener(transEnd, changeCurrent, true);
			}

			// Autoscroll needs wrapAround as well...
			if (options.wrapAround && !!options.autoSlide) { // set up the done event and trigger it for the first auto slide
				$wrap.on('nSwipeSlider.swipeDone', function () {
					autoSlide();
				}).trigger('nSwipeSlider.swipeDone');
				
			}

			function autoSlide() {
				clearTimeout(autoSlideTimer); // clear eventual timer we might have and...
				autoSlideTimer = -1;
				autoSlideTimer = setTimeout(function () { // ...set up a new timer on each done event
					$items.filter('.active').trigger('swipeleft');
				}, options.autoSlide);
			}

			function resetStyleTag() {
				$items.each(function () { $(this)[0].style.left = ''; });
			}

			function triggerDoneEvent(e) {
				$wrap.trigger('nSwipeSlider.swipeDone', [$(e.target), i]);
				e.target.removeEventListener(transEnd, triggerDoneEvent, true);
			}



		});
	};
})(jQuery);

//
function autoCenter(selector) 
{
	var dSel = '[data-autocenter]',
		 obj = selector ? dSel +', '+ selector : dSel,
		$obj = $(obj+':visible'),
		// $obj = selector ? $(dSel +', '+ selector) : $(dSel),
		reset = {
			maxWidth: '67%',
			// margin: 'auto',
			// left	: 'auto',
			// right: 'auto',
			height	: 'auto',
			bottom	: 'auto'
		};
	//
	// console.log($obj);
	return $obj.each(function (i, obj) 
	{
		var $obj = $(this).css(reset), // don't string these //
			$par = $obj.parent(),
			// w = ($par.outerWidth() - $obj.outerWidth()) / 2,
			h = ($par.outerHeight() - $obj.outerHeight()) / 2;
		// console.log( obj, $par.outerHeight(), $obj.outerHeight(), h);

		$obj.css({
			// left : w,
			top  : h
		});
	});
}

//
//var carouselCount = 0;
//// requires jquery.carouFredSel.js -> added at bottom of this script //
//var makeCarousel = function (selector, options) {
//	if (!$(selector).length) return false;

//	options = $.extend({
//	//	auto		: false,
//	//	width		: 'auto',
//		pagination	: true,
//		height		: 'auto',
//		direction	: 'left',
//		scroll		: {
//			'duration' : 800
//		},
//		mousewheel: false, // don't interfere w/ page scroll
//		swipe: { //	scrolled by user interaction
//			onMouse: true,
//			onTouch: true
//		}
//	}, options);
	
//	carouselCount++;
//	// console.log('carouselCount', carouselCount);
//	// console.log(selector, $(selector).length);

//	$(selector).each(function (i) 
//	{
//		i += carouselCount;
//		var carousel = $(this).find('ul'),
//			addObj = "";

//		if (options.pagination === true) 
//		{
//			addObj				= '<div id="paginate' + i + '" class="carousel-pagination" />';
//			options.pagination	= '#paginate' + i ;
//		}
//		// else {
//		addObj += '<a id="prev' + i + '" class="carousel-prev arrow-left" href="#">&lsaquo;</a>',
//		addObj += '<a id="next' + i + '" class="carousel-next arrow-right" href="#">&rsaquo;</a>';
//		options.prev = '#prev' + i;
//		options.next = '#next' + i;
//		// }

//		carousel.after(addObj); // adds controls //
//		// alert('makeCarousel');

//		//if (typeof(autoCenter) == 'function')
//		//	autoCenter('figcaption, .figcaption');
		
//		//if (jQuery.fn.imagesLoaded === 'function') {
//		carousel.find('img').imagesLoaded(function () {
//			carousel.carouFredSel(options);
////			alert('images loaded!');
//		});
//		//} //else {
//		//	carousel.carouFredSel(options);
//			//alert('function "imagesLoaded" does not exist');
//		//}
//	});
//};


//
$.fn.selectLabel = function ( label, selectLabel ) {
	return this.each(function (i, input, opts) 
	{
		opts = selectLabel ? { 'disabled': 'disabled', 'selected': 'selected' } : {};
		opts.text = label;
		$('<option>', opts).prependTo(input);
	});
};


// non-interactive sliders //////////////////////
$.fn.slider = function (opts) 
{
	if ( !this.length ) return this;

	opts = $.extend({
		fadeSpeed	: 4500,
		interval	: 6600,
		slideUp		: false,
		crossFade	: false,
		slideWrapper: 'li'
	}, opts);

	var sliderUL = this.selector,
		slide	 = sliderUL + " > " + opts.slideWrapper,
		j = 0,
		func,

	cLog = function () { // for testing purposes // console.log shortcut //
		return false;
		console.log( sliderUL, ++j );
	},

	crossFade = function () {
		$(slide).first()
			.fadeOut( opts.fadeSpeed )
			.next()
			.fadeIn( opts.fadeSpeed, cLog )
			.end()//.css('top', 0)
		.appendTo( sliderUL );
	},
	
	slideLeft = function () {
		$(sliderUL).animate({ 'margin-left': '-100%' }, opts.fadeSpeed, cLog );
	},

	slideUp = function () {
		$(slide).first()
			.slideUp( opts.fadeSpeed )
			.next()
			.slideDown( opts.fadeSpeed, cLog )
			.end()
		.appendTo( sliderUL );
	},

	fadeOutFadeIn = function () {
		var frst = $(slide).first()
		.fadeOut( opts.fadeSpeed, function() {
			frst.next()
				.fadeIn( opts.fadeSpeed, cLog )
				.end()
			.appendTo( sliderUL );
		});
	};
	// CALLBACK FUNCTION //
	if (opts.crossFade)
		func = crossFade;
	else 
	if (opts.slideLeft)
		func = slideLeft;
	else 
	if (opts.slideUp)
		func = slideUp;
	else 
	if (opts.fadeSpeed < 1500) 
		func = fadeOutFadeIn;
	else
		func = crossFade;
	//
	return this.each(function(i, obj) // returns 1ce per UL container
	{	//console.log(obj);
		if ( $(this).hasClass('init') ) return;

		$(slide).hide()//.addClass('slide')
			.first().fadeIn('fast');

		if ( $(slide).length > 1 )
		{

			//console.log( '[' + sliderUL + '] slideUp?', opts.slideUp, '[' + func.name + ']' );
			//
			setInterval( func, opts.interval );
		}

		$(this).addClass('init');
	});
};

// Adds :childof //
$.expr[':'].childof = function(obj, index, meta, stack){
	return $(obj).parent().is(meta[3]);
};

/*!
* jQuery Clickable v1.0b
* http://pixeltango.com
*
* Copyright 2010, PixelTango
* Dual licensed under the MIT or GPL Version 2 licenses.
*
* Date: Fri Aug 20 08:10:07 2010 +0100
*/
$.fn.clickable = function (targetSelector, settings) {
	settings = jQuery.extend({
		hoverClass: 'hover',
		changeCursor: true
	}, settings);

	return this.each(function () {
		var $e = $(this);

		// Find target anchor return if not found --------------------
		var $a = targetSelector != null ?
			$e.find(targetSelector) :
			$e.find('a:first');

		if ($a == null || !$a.length) return;

		// Save targetUrl for later reference ------------------------
		var targetUrl = $a[0].href; // $a.attr('href'); // $a[0].href

		// Events ----------------------------------------------------
		var onMouseEnter = function (e) 
		{
			$e.addClass(settings.hoverClass);
			$a.addClass(settings.hoverClass);

			// Remove browser's default outline
			if (e.type != 'focus') $a.focus().css('outline', 'none');

			// Update status bar (older browsers only)
			window.status = targetUrl;

			// Change cursor
			if (settings.changeCursor) $e.css('cursor', 'pointer');
		};

		var onMouseLeave = function (e) 
		{
			$e.removeClass(settings.hoverClass);
			$a.removeClass(settings.hoverClass);

			if (e.type != 'blur') $a.blur().css('outline', 'inherit');

			window.status = '';

			if (settings.changeCursor) $e.css('cursor', 'auto');
		};

		// Event binding ---------------------------------------------
		$e.click(function () {
			// Open the targetUrl in existing or new window
			var t;
			if (t = $a.attr('target'))
				window.open(targetUrl, t);
			else
				window.location.href = targetUrl;

			// if ($a.attr('target') == '_blank')
			// 	window.open(targetUrl, '_blank');
			// else
			// 	window.location.href = targetUrl;
		})
		.hover(onMouseEnter, onMouseLeave);

		$a.focus(onMouseEnter)
		  .blur(onMouseLeave);
	});
};


// Place any jQuery/helper plugins in here ////////////////

mobileDevice = { // mobile detection package //

	isMobile: false,

	mobSize : {
		min : 320, // min-width breakpoint
		ave : 480, // average-width breakpoint
		max : 650 // iPhone 5 height // landscape
	},
	
	mobUserAgent : function () {
		return /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent);
	},

	init: function () 
	{
		this.isMobile = this.isMobileSize();
		
		this.iosDeviceOrientation();

		$(window).resize( this.doOnWindowResize );
	},

	isMinMobileSize: function(){
		return ( $(window).width() <= this.mobSize.min );
	},
	isMobileSize: function(){
		return ( $(window).width() <= this.mobSize.max );
	},

	// this function can be changed externally //
	initMobile : function() 
	{
		//console.log('FUNCTION: initMobile');
		//document.location.reload(true); //don't actually reload
	},
	// this function can be changed externally //
	initDesktop : function() 
	{
		//console.log('FUNCTION: initDesktop');
		//document.location.reload(true); //don't actually reload
	},

	// this function can be changed externally //
	doOnWindowResize : function( e ) 
	{
		var t = mobileDevice, // because e
			wasMobile = t.isMobile,
			isMobSize = t.isMobileSize();

		//console.log('wasMobile?', wasMobile, '/ isMobSize?', isMobSize); // FIRST TIME

		if (isMobSize && !wasMobile) 
		{
			console.log('// change from Desktop to Mobile');

			t.isMobile = true;
			t.initMobile();

		} else
		if (!isMobSize && wasMobile) 
		{
			console.log('// change from Mobile to Desktop');

			t.isMobile = false;
			t.initDesktop();
		}
	},

	iosDeviceOrientation: function()
	{
		if (navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i)) {
			var vmeta = document.querySelector('meta[name="viewport"]');
			if (vmeta) {
				vmeta.content = 'width=device-width, minimum-scale=1.0, maximum-scale=1.0, initial-scale=1.0';
				document.body.addEventListener('gesturestart', function () {
					vmeta.content = 'width=device-width, minimum-scale=0.25, maximum-scale=1.5';
				}, false);
			}
			return true;
		}
		return false;
	}

};
