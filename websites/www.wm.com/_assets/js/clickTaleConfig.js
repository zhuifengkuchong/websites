document.write(unescape("%3Cscript%20src='"+
 (document.location.protocol=='https:'?
  'https://clicktale.pantherssl.com/':
  'http://s.clicktale.net/')+
 "WRb6.js'%20type='text/javascript'%3E%3C/script%3E"));

function escapeRegex(text) {
    if (!arguments.callee.sRE) {
        var specials = [
            '/', '.', '*', '+', '?', '|',
            '(', ')', '[', ']', '{', '}', '\\'
        ];
        arguments.callee.sRE = new RegExp(
            '(\\' + specials.join('|\\') + ')', 'g'
        );
    }
    return text.replace(arguments.callee.sRE, '\\$1');
}

function urlPathIs(urls) {
    /// <summary>Check if the current url matches any of the given urls passed</summary>
    /// <param name="urls" type="Array[String]|String">A url or an array of urls to test against</param>
    /// <returns type="Boolean">True if the the current location matches any of the passed urls, false otherwise</returns>
    if(arguments.length > 1) {
        urls = Array.prototype.slice.call(arguments, 0);
    }
    if(typeof urls == "string") {
        urls = [urls];
    }
    var escapedUrls = [],
        regexp;
    for(var i = 0; i < urls.length; i++) {
        escapedUrls.push(escapeRegex(urls[i]));
    }
    regexp = new RegExp("^(?:"+escapedUrls.join("|")+")/?(?:\\?|$|#)","i");
    return regexp.test(window.location.pathname);
}


function urlPathContains(urls) {
    /// <summary>Check if the current url contains any of the given urls passed</summary>
    /// <param name="urls" type="Array[String]|String">A url or an array of urls to test against</param>
    /// <returns type="Boolean">True if the the current location contains any of the passed strings, false otherwise</returns>
    if(arguments.length > 1) {
        urls = Array.prototype.slice.call(arguments, 0);
    }
    if(typeof urls == "string") {
        urls = urls.split(",");
    }
    
	var thisUrl = window.location.pathname;
	var isMatch = false
	for (var i=0;i<urls.length;i++)
	{
		if (thisUrl.indexOf(urls[i])>-1)
			isMatch = true
	}

	return isMatch;
}

//Check to see if the current URL has any partial match

var ctAccount = 41464; //test account
if(document.location.host == "http://www.wm.com/_assets/js/www.wm.com" ||
	document.location.host == "http://www.wm.com/_assets/js/wm.com")
	ctAccount = 41604; //production account

//set default recording Ratio
var recordingRatio = 0.16;

//Change recordingRatio based on full or partial match
if (urlPathContains("/customer-service/contact-us,/facility.,/products-and-services/request-service/,/pay-my-bill/")) //snippets, single string (all in one pair of double quotes), comma separated
{
	recordingRatio = 1;
}
else if (urlPathIs("http://www.wm.com/placeholder.html")) //full hrefs, each one in double quotes, comma separated
	recordingRatio = 1;


var ClickTaleSSL=1;
