(function(){

    var FSR;

    // Do we support AMD?
    var supports_amd =
        typeof(window.define) === 'function' && window.define.amd &&
            (!window.FSR || window.FSR.supportsAMD);

    if(!supports_amd)
        FSR = window.FSR;
    else
        FSR = {};

    FSR.surveydefs = [{
    name: 'phone_web',
	platform: 'phone',
    invite: {
        when: 'onentry',
		dialogs: [[{
            reverseButtons: false,
            headline: "We welcome your feedback!",
            blurb: "You have been selected to participate in a brief customer satisfaction survey to let us know how we can improve your experience.",
            attribution: "Conducted by ForeSee.",
            declineButton: "No, thanks",
            acceptButton: "Yes, I'll help",
            error: "Error",
            warnLaunch: "this will launch a new window"
        }]]
    },
    pop: {
        when: 'now'
    },
    criteria: {
        sp: 25,
        lf: 3
    },
    include: {
        urls: ['.']
    }
},{
    name: 'bizrate',
    platform: 'desktop',
    alt: [{
        sp: 100,
        script: '../../../../../../../../../eval.bizrate.com/js/pos_21744.js'/*tpa=http://eval.bizrate.com/js/pos_21744.js*/
    }],
    invite: {
        when: 'onentry'
    },
    pin: 1,
    pop: {
        when: 'now'
    },
    criteria: {
        sp: 10,
        lf: 1
    },
    include: {
        urls: ['/OrderOKView', '/order-summary']
    }
}, {
    name: 'browse',
	platform: 'desktop',
    invite: {
        when: 'onentry'
    },
    pop: {
        when: 'later'
    },
    criteria: {
        sp: 35,
        lf: 3
    },
    include: {
        urls: ['/shop/']
    }
}];
FSR.properties = {
    repeatdays: 90,
    
    repeatoverride: false,
    
    altcookie: {},
    
    language: {
        locale: 'en'
    },
    
    exclude: {},
    
    zIndexPopup: 10000,
    
    ignoreWindowTopCheck: false,
    
    ipexclude: 'fsr$ip',
    
    mobileHeartbeat: {
        delay: 60, /*mobile on exit heartbeat delay seconds*/
        max: 3600 /*mobile on exit heartbeat max run time seconds*/
    },
    
    invite: {
    
        // For no site logo, comment this line:
        siteLogo: "sitelogo.gif"/*tpa=http://static.coach.com/aapz_prd/on/demandware.static/Sites-Coach_US-Site/Library-Sites-globalLibrary/default/v1433766638119/foresee/sitelogo.gif*/,
        
        //alt text fore site logo img
        siteLogoAlt: "",
        
        /* Desktop */
        dialogs: [[{
            reverseButtons: false,
            headline: "We welcome your feedback!",
            blurb: "Thank you for visiting Coach.com. You have been selected to participate in a brief customer satisfaction survey to let us know how we can improve your experience.",
            noticeAboutSurvey: "The survey is designed to assess your entire experience, please look for it at the <u>conclusion</u> of your visit.",
            attribution: "This survey is conducted by an independent company, ForeSee, on behalf of the site you are visiting.",
            closeInviteButtonText: "Click to close.",
            declineButton: "No, thanks",
            acceptButton: "Yes, I'll give feedback",
            error: "Error",
            warnLaunch: "this will launch a new window"
        
        }]],
        
        exclude: {
            urls: ['/payment', '/cart', '/account', '/confirmednewpassword', '/account-edit', '/wishlist', '/paymentInstruments-list', '/address-list', '/favorite-store', '/orders', '/staticpage', '/home', '/homepage'],
            referrers: [],
            userAgents: [],
            browsers: [],
            cookies: [{
                name: 'fsrInvite',
                value: 'false'
            }],
            variables: [{
                name: 'Needle.targetedSeen',
                value: 'true'
            }]
            // [name (content), http-equiv (content), itemprop (content),  charset] possible attributes for meta tag element http://devdocs.io/html/meta
            //metas:[{"name":{"key":"value", "content":"value"}}, {"http-equiv":{"key":"value", "content":"value"}}, {"itemprop":{"key":"value", "content":"value"}}, {"charset":{"key":"value"}}]
        
        },
        include: {
            local: ['.']
        },
        
        delay: 0,
        timeout: 0,
        
        hideOnClick: false,
        
        hideCloseButton: false,
        
        css: 'foresee-dhtml.css'/*tpa=http://static.coach.com/aapz_prd/on/demandware.static/Sites-Coach_US-Site/Library-Sites-globalLibrary/default/v1433766638119/foresee/foresee-dhtml.css*/,
        
        hide: [],
        
        hideFlash: false,
        
        type: 'dhtml',
        /* desktop */
        // url: 'http://static.coach.com/aapz_prd/on/demandware.static/Sites-Coach_US-Site/Library-Sites-globalLibrary/default/v1433766638119/foresee/invite.html'
        /* mobile */
        url: 'http://static.coach.com/aapz_prd/on/demandware.static/Sites-Coach_US-Site/Library-Sites-globalLibrary/default/v1433766638119/foresee/invite-mobile.html',
        back: 'url'
    
        //SurveyMutex: 'SurveyMutex'
    },
    
    tracker: {
        width: '690',
        height: '415',
        timeout: 3,
        adjust: true,
        alert: {
            enabled: true,
            message: 'The survey is now available.'
        },
        url: 'http://static.coach.com/aapz_prd/on/demandware.static/Sites-Coach_US-Site/Library-Sites-globalLibrary/default/v1433766638119/foresee/tracker.html'
    },
    
    survey: {
        width: 690,
        height: 600
    },
    
    qualifier: {
        footer: '<div id=\"fsrcontainer\"><div style=\"float:left;width:80%;font-size:8pt;text-align:left;line-height:12px;\">This survey is conducted by an independent company ForeSee,<br>on behalf of the site you are visiting.</div><div style=\"float:right;font-size:8pt;\"><a target="_blank" title="Validate TRUSTe privacy certification" href="http://privacy-policy.truste.com/click-with-confidence/ctv/en/www.foreseeresults.com/seal_m"><img border=\"0\" src=\"{%baseHref%}truste.png\" alt=\"Validate TRUSTe Privacy Certification\"></a></div></div>',
        width: '690',
        height: '500',
        bgcolor: '#333',
        opacity: 0.7,
        x: 'center',
        y: 'center',
        delay: 0,
        buttons: {
            accept: 'Continue'
        },
        hideOnClick: false,
        css: 'foresee-dhtml.css'/*tpa=http://static.coach.com/aapz_prd/on/demandware.static/Sites-Coach_US-Site/Library-Sites-globalLibrary/default/v1433766638119/foresee/foresee-dhtml.css*/,
        url: 'http://static.coach.com/aapz_prd/on/demandware.static/Sites-Coach_US-Site/Library-Sites-globalLibrary/default/v1433766638119/foresee/qualifying.html'
    },
    
    cancel: {
        url: 'http://static.coach.com/aapz_prd/on/demandware.static/Sites-Coach_US-Site/Library-Sites-globalLibrary/default/v1433766638119/foresee/cancel.html',
        width: '690',
        height: '400'
    },
    
    pop: {
        what: 'survey',
        after: 'leaving-site',
        pu: false,
        tracker: true
    },
    
    meta: {
        referrer: true,
        terms: true,
        ref_url: true,
        url: true,
        url_params: false,
        user_agent: false,
        entry: false,
        entry_params: false,
        viewport_size: false,
        document_size: false,
        scroll_from_top: false,
        invite_URL: false
    },
    
    events: {
        enabled: true,
        id: true,
        codes: {
            purchase: 800,
            items: 801,
            dollars: 802,
            followup: 803,
            information: 804,
            content: 805
        },
        pd: 7,
        custom: {
            purchase: {
                enabled: true,
                repeat: false,
                source: 'url',
                patterns: ['/order-summary']
            },
            items: {
                enabled: true,
                repeat: false,
                source: 'variable',
                name: 'http://static.coach.com/aapz_prd/on/demandware.static/Sites-Coach_US-Site/Library-Sites-globalLibrary/default/v1433766638119/foresee/dataLayer.qty'
            },
            dollars: {
                enabled: true,
                repeat: false,
                source: 'variable',
                name: 'http://static.coach.com/aapz_prd/on/demandware.static/Sites-Coach_US-Site/Library-Sites-globalLibrary/default/v1433766638119/foresee/dataLayer.cost'
            }
        }
    },
    
    previous: false,
    
    analytics: {
        google_local: false,
        google_remote: false
    },
    
    cpps: {},
    
    mode: 'first-party'
};


    if(supports_amd)
        define(function(){ return FSR; })
})();
