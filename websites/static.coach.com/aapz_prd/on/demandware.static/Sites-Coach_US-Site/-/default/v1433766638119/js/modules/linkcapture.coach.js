/*!
 * linkcapture.coach.js
 * This file contains the code for capturing JS links before JS load.
 *
 * @project   Coach eCommerce
 * @date      2014-06-30
 * @author    Shantnu Aggarwal
 * @licensor  Coach Inc.
 *
 */
(function() {
    // Figure out what to hook clicks on
    var container =
        document.body ||
        document.documentElement ||
        document;

    // Hook clicks that reach the bottom level
    hookClicks();
    function hookClicks() {
      if (container.attachEvent) {
        container.attachEvent("onclick", handleClick);
      }
      else if (document.addEventListener) {
        container.addEventListener("click", handleClick, false);
      }
    }

    // Set up an unhook function for jQuery to call
    window.unhookClicks = unhookClicks;
    function unhookClicks() {
      if (container.attachEvent) {
        container.detachEvent("onclick", handleClick);
      }
      else if (document.addEventListener) {
        container.removeEventListener("click", handleClick, false);
      }
    }

    // Handle clicks
    function handleClick(event) {
      var target;
	//debugger;
      // Handle Microsoft vs. W3C event passing style
      event = event || window.event;

      // Get the target (again handling Microsoft vs. W3C style)
      target = event.target || event.srcElement;
     
      if (target &&
          (target.tagName.toUpperCase() === "BUTTON" || target.getAttribute("class").indexOf('click-handler') > '-1' || target.hasAttribute('data-toggle') ) ) {
        // It's a link we want to prevent for the moment
        // Remember the element that was clicked if there
        // isn't already one
        if (!window.pendingLink) {
          window.pendingLink = target;
        }

        // Prevent it from being processed
        if (event.preventDefault) { // If W3C method...
          event.preventDefault();
        }

        // This should work if preventDefault doesnt
        return false;
      }
    }
  })();