<script id = "race4a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race4={};
	myVars.races.race4.varName="Object[24983].name";
	myVars.races.race4.varType="@varType@";
	myVars.races.race4.repairType = "@RepairType";
	myVars.races.race4.event1={};
	myVars.races.race4.event2={};
	myVars.races.race4.event1.id = "Lu_DOM";
	myVars.races.race4.event1.type = "onDOMContentLoaded";
	myVars.races.race4.event1.loc = "Lu_DOM_LOC";
	myVars.races.race4.event1.isRead = "False";
	myVars.races.race4.event1.eventType = "@event1EventType@";
	myVars.races.race4.event2.id = "flightSearchForm.adultPassengerCount.flightHotel";
	myVars.races.race4.event2.type = "onchange";
	myVars.races.race4.event2.loc = "flightSearchForm.adultPassengerCount.flightHotel_LOC";
	myVars.races.race4.event2.isRead = "True";
	myVars.races.race4.event2.eventType = "@event2EventType@";
	myVars.races.race4.event1.executed= false;// true to disable, false to enable
	myVars.races.race4.event2.executed= false;// true to disable, false to enable
</script>

