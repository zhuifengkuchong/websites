(function( $ )
{
	$.fn.expandedSliderControl = function() {
		

/**************************************************************************
* ESTABLISH ROUTE SCOPE
*/
 	var obj = this;
 

/**************************************************************************
* DEFINE SLIDER MODE - slider mode controls whether or not the olympics
content is shown in the slider.
*/
	var landingMode = false; 
	


	
/**************************************************************************
* DEFINE GLOBAL VARIABLES
*/
    
    //initial settings
    var numSlides=14;
    //var numSlides           =       obj.find('li.expanded-slide').size();  //get total number of slides
    var panelWidth          =       obj.find('li.expanded-slide').width();  //get the panel width of each item
	var panelPadding 		= 		parseInt(obj.find('li.expanded-slide').css("padding-left"));
    var animDistance        =       panelWidth + panelPadding;
    var totalWidth          =       animDistance * numSlides;
    var slidesVisible       =       1;
    
    //animationBooleans
    var isAnimating         =       false;
    
    //set index
    var currentIndex        =       0;
    
    
    var panelOpen           =       false;
    var closeIntent         =       false;

	
    
    
    

    
 
 /**************************************************************************
* DEFINE MAIN FUNCTIONS
*/
	function searchFixHide() {
		//return;
		for(i = 0; i< numSlides; i++) {
			if(i != currentIndex) {
				obj.find('li.expanded-slide').eq(i).css({visibility : 'hidden'});
			}
		}
	}
	
	function searchFixShow() {

		obj.find('li.expanded-slide').css({visibility : 'visible'});
	}
	

    /*on initialization checks whether or not to display arrows
    and sets their current opacity*/
    function setArrows() {
        if(numSlides <= slidesVisible) {
             $('a.expand-arrow-rt').remove();
             $('a.expand-arrow-lt').remove();
        } else {
            checkArrows();
        }
    }
    
    
    //utility function to delegate arrow display on animation
    function checkArrows() {

        if(currentIndex == (numSlides - slidesVisible)) {
        
            $('a.expand-arrow-lt').removeClass('deact');
            $('a.expand-arrow-rt').addClass('deact');
            
        } else if(currentIndex == 0) {
       
            $('a.expand-arrow-lt').addClass('deact');
            $('a.expand-arrow-rt').removeClass('deact');
            
        } else {
           
            $('a.expand-arrow-lt').removeClass('deact');
            $('a.expand-arrow-rt').removeClass('deact');
       
        }
    }
    
    function SlideToSlide(newIdx) {
		obj.find('a.back-link').show();
		searchFixShow();
        slideCallbackExtension.slideOut(currentIndex); 
        slideCallbackExtension.slideTo(newIdx, true);
        if(isAnimating == false) {
            isAnimating = true;
            calcAnimDistance = newIdx * animDistance;
            obj.find('ul.expanded-slides').animate({left: -calcAnimDistance}, 1000, 'easeInOutQuart', function(){
                isAnimating = false;
                currentIndex = newIdx;
				setDeepLink(currentIndex);
                checkArrows();
                slideCallbackExtension.slideComplete(newIdx);
				searchFixHide();
             });
        }
    }
    
    $.fn.expandedSliderControl.globalSlideTo = function(newIdx, story) {
        if(!(story)) {
        	story=''
        }else{
        	story = '-' + story;
        }

        if(story !='-back-link'){
        	if(newIdx == 0){
        		getMetrics('200-14');
        	}else if(newIdx == 1){
        		getMetrics('200');
        	}else{
       			getMetrics('200-' + (newIdx-1) + story);
        	}		
        }else{
        	getMetrics('200-21');
        }
        SlideToSlide(newIdx);
    }
    
    //main function that slides panel left or right
    function SlideController(direction) {
    
    if((currentIndex <=13) && (currentIndex >=0)){
    	getMetrics(direction + '-' + currentIndex);
    }
        if(isAnimating == false) {
            if((direction === 'rt') && (currentIndex  < (numSlides - slidesVisible))) {
				searchFixShow();
                slideCallbackExtension.slideOut(currentIndex); 
                slideCallbackExtension.slideTo(currentIndex + 1);
                isAnimating = true;
                obj.find('ul.expanded-slides').animate({left: '-=' + animDistance}, 1000, 'easeInOutQuart',
                    function() {
                        isAnimating = false;
                        currentIndex ++;
						setDeepLink(currentIndex);
                        checkArrows();
                        slideCallbackExtension.slideComplete(currentIndex);
						if(currentIndex == 1) {
							obj.find('a.back-link').show();
						}
						if(!isIE()) {
							searchFixHide();
						}
                        //ajaxController.ajaxCheck(currentIndex);
                });
            
            } else if((direction === 'lt') && (currentIndex > 0)) {
				searchFixShow();
                slideCallbackExtension.slideOut(currentIndex); 
                slideCallbackExtension.slideTo(currentIndex - 1);
                isAnimating = true;
                obj.find('ul.expanded-slides').animate({left: '+=' + animDistance}, 1000, 'easeInOutQuart', 
                    function() {
                        isAnimating = false;
                        currentIndex --;
						setDeepLink(currentIndex);
                        slideCallbackExtension.slideComplete(currentIndex);
                        checkArrows();
						if(currentIndex == 1) {
							obj.find('a.back-link').show();
						}
						if(!isIE()) {
							searchFixHide();
						}
                        //ajaxController.ajaxCheck(currentIndex);
                });
            }
        }
        
    }
    
    function expandView(index, showBack) {
	
		if(index == 1) {
			obj.find('a.back-link').show();
		}
	
		if(showBack != undefined) {
			this.showBackButton = showBack;
		}
		
		if(landingMode == true) {
			obj.find('a.close-expanded-carousel').hide();
			obj.find('a.back-link').show();
			numSlides = (obj.find('li.expanded-slide').size())-6;
			
		} else {
			obj.find('a.close-expanded-carousel').show();
		}
		
		
		//if((index==6) && (window.location.hash !='#!200-5')){
		//	index= 14;
		//}else if((index == 14) && (window.location.hash !='#!olympics')){
		//	index= 6;
		//}
		
		//setDeepLink(index);
		socialMediaShelf.hideTab();
        slideVideoController.resetAll();
        closeIntent = false;
        obj.find('ul.expanded-slides').css({left:(-animDistance * index)});
		
        
        $('ul.slide-control').hide();
        if(!isIE()) {
            $('#progress-informed').stop(true, true).fadeTo(700, 0.15, function(){
                $(this).css({left: -700});
            });
            $('#progress-informed').css({left: -700});
            obj.fadeTo(700, 1 , function() {
                slideCallbackExtension.slideComplete(index);
                panelOpen = true;
				//searchFixHide();
            });
        } else {
			obj.show();
			obj.find('ul.expanded-slides li').css({visibility:'visible'})
            $('#progress-informed').css({left: -700});
            $('#progress-informed').hide();
            slideCallbackExtension.slideComplete(index);
            panelOpen = true;
			//searchFixHide();
        }
        
        currentIndex = index;
        checkArrows(); 

		if(!isIE()) {
			searchFixHide();
		}
           
    }
    
    
    
    $.fn.expandedSliderControl.expandLargeSlider = function(index) {
        expandView(index);
    }
    
    function closeView() {
		landingMode = false;
		numSlides = obj.find('li.expanded-slide').size();
		obj.find('a.back-link').hide();
		removeHash();
        slideVideoController.hideVideos();
        socialMediaShelf.showTab();
        panelOpen = false;
		//obj.find('a.back-link').hide();
        slideCallbackExtension.slideOut(currentIndex);
        if(!isIE()) {
            $('ul.slide-control').fadeTo(100, 1);
            $('#progress-informed').css({left: 48});  
            $('#progress-informed').stop(true, true).fadeTo(100, 1);
            obj.fadeTo(700, 0, function() {
                obj.hide();
				searchFixShow();
                slideVideoController.showVideos();
                
            });
            
            
            
        } else {
            panelOpen = false;
            obj.hide();
            $('#progress-informed').css({left: 48});
            $('#progress-informed').show();
            $('ul.slide-control').show();
            slideVideoController.showVideos();
            
            
        }
       
    }
    


 
/**************************************************************************
* BIND EVENT LISTENERS 
*/	
 
 /*bind listeners for controller arrows*/
    obj.find('a.expand-arrow-rt').click(function(e) {
        //e.preventDefault();
        //SlideController('rt');
    });
    obj.find('a.expand-arrow-lt').click(function(e) {
        //e.preventDefault();    
        //SlideController('lt');
    });
    
/*bind close event to close slider button*/
    obj.find('a.close-expanded-carousel').click(function(e) {
        e.preventDefault();
       // closeView();
        window.location ='http://www.citigroup.com/citi/index.htm';
    });
    
/*bind close intent listeneres for closing pop up */

$('body, html').click(function(e) {
    //if(panelOpen === true){
    //	window.location ='http://www.citigroup.com/citi/index_new_overlays.htm';
    //}else{
    //	return
    //}	
    
    if((closeIntent === true) && (panelOpen === true)) {
        
        //closeView();
        //console.log(e.target.id);
      if(! /citibankClients|institutionalClients|news|investor|about|oo_launch_prompt|oo_no_thanks|oo_close_prompt|oo_invitation_prompt|oo_container|oo_company_logo|prompt_buttons|oo_never_show|oo_ol_brand|oo_invitation_overlay/i.test(e.target.id) ) {
      //if($(e.target).parent() != 'li.submenu' !=-1) {
              window.location ='http://www.citigroup.com/citi/index.htm';
       }         
    } else {
        return;
    }
}) 

obj.mouseenter(function() {
    closeIntent = false;
})  

obj.mouseleave(function() {
    closeIntent = true;
}) 

/**************************************************************************
* DEEP LINKING FUNCTIONALITY 
*/


/*var deepLinks = new Array();

deepLinks['200'] = 0;
deepLinks['Olympics'] = 13;

function setDeepLink(index) {
	
	var setIndex = index;
	var story;
	
	if(index < deepLinks['Olympics']) {
		
		story = "200";
		setIndex = setIndex - deepLinks['200'];
		
	} else {
		
		story = "Olympics";
		setIndex = setIndex - deepLinks['Olympics'];
		
	}
	
	window.location.hash = '!' + story + '-' + index + brandInfo.utmc;
}*/


var deepLinks = [

		{
			"name" : "200",
			"id" : 1,
			"startIndex" : 0,
			"endIndex" : 0
		},		{
			"name" : "featured",
			"id" : 2,
			"startIndex" : 1,
			"endIndex" : 13
		}


		
		//{
		//	"name" : "olympics",
		//	"id" : 3,
		//	"startIndex" : 14,
		//	"endIndex" : 19
		//}
		
];

function constructHash(index) {
	var storyIndex;
	var storyName;
	var targetIndex = index;
	var length = deepLinks.length;

	brand.checkLanguage(index);
	
	for(i = 0; i < length; i++) {
		
		if(index <= deepLinks[i].endIndex) {
			storyIndex = targetIndex - deepLinks[i].startIndex;
			storyName = deepLinks[i].name;
			
			if(storyIndex === 0) {
				window.location.hash = '!' + storyName +  brandInfo.utmc;
			} else {
				window.location.hash = '!' + storyName + '-' + storyIndex + brandInfo.utmc;
			}
			
			return;
		}	
	}	
}

function getHash() {
	
	var baseIndex;
	var totalDistance;
	var hash = window.location.hash.substring(1).toLowerCase();
	if(hash.indexOf("?") !== -1) {
		var thash = hash.split('?');
		hash = thash[0];
		brandInfo.utmc = '?' + thash[1];
	}
	var removeExclamation = hash.split('!');
	var myHash = removeExclamation[1];
	
	
	
	var splitHash;
	var storyType;
	var story;
	
	
	if(myHash.indexOf("-") !== -1) {
 		splitHash = removeExclamation[1].split('-');
		storyType = splitHash[0];
		story = parseInt(splitHash[1]);

	} else {
		storyType = myHash;
		story = 0;
	}
	

	var length = deepLinks.length;
	
	for(i = 0; i < length; i++) {
		
		
		if(storyType == deepLinks[i].name) {
			
			if(story > deepLinks[i].endIndex) {
				story = 0;
			}
			
			baseIndex = parseInt(deepLinks[i].startIndex);

			totalDistance = baseIndex + story;
			
			
			//we can assume we are at the 200 years city slide
			if(totalDistance == 1)  {
				//expand slider with landing page mode
				//COMMMENT THIS BACK IN BEFORE LAUNCH!
				landingMode = true;
				obj.find('a.back-link').show();
			}
			//if(totalDistance >13){
			//	window.open('http://everystep.citi.com/','_blank');
			//}else{	
				expandView(totalDistance);
			//}			
			return;
		}
	}	 

}



function setDeepLink(index) {
	constructHash(index);
}



function removeHash() {
	window.location.hash = '';
}



 
    
/**************************************************************************
* INITIALIZE PAGE SETTINGS
*/

	setArrows(); //check if arrows are needed and set there opacity
	
	var checkHash = window.location.hash;
	if(checkHash.indexOf("#!") !== -1) {
		getHash();
	}
	
  
  	/*set total width of the slider*/
  	obj.find('ul.expanded-slides').css({width : totalWidth});
  
  
  
	
	
 

	
};
})( jQuery );// JavaScript Document