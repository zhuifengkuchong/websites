/**************************************************************************
 * JAVASCRIPT PAGE FOR SMART DETECTION
 */

/**************************************************************************
 * Detect if Ipad or Iphone
 */
 
var agent=navigator.userAgent.toLowerCase();
var isIDevice = (agent.indexOf('iphone')!=-1 || agent.indexOf('ipad')!=-1);

/* USE
if (isIDevice ) {
	Code Specific for Ipad or Iphone       
} 

*/

function isIpad() {
	return (navigator.userAgent.match(/iPad/i) != null);
}

function isIphone() {
	return (navigator.userAgent.match(/iPhone/i) != null);
}

/**************************************************************************
 * Detection for Android Devices
 */
 
 function isMobile() {
	return isIpad()==true || isIphone()==true || isAndroid()==true;
}

/**************************************************************************
 * Detection for Mobile Devices
 */
 
 function isMobile() {
	return isIpad()==true || isIphone()==true || isAndroid()==true;
}

/**************************************************************************
 * Detection for Internet Explorer Version
 */
 
 function isIE() {
	return isIE6() || isIE7() || isIE8();
	
}

/* USE
if (isIE()) {
	Code Specific for all IE Browsers      
} 

*/

//SETS FUNCTION TO DETECT IE6
function isIE6() {
	return (navigator.appVersion.indexOf("MSIE 6.") >= 0);
}

//SETS FUNCTION TO DETECT IE7
function isIE7() {
	return (navigator.appVersion.indexOf("MSIE 7.") >= 0);
}

//SETS FUNCTION TO DETECT IE8
function isIE8() {
	return (navigator.appVersion.indexOf("MSIE 8.") >= 0);
}