/**************************************************************************
* PUBLIC UTILITY CLASS DEDICATED TO MANAGING THE AJAX DATA ON LANDING PAG
*/

landingModel = {
	
	ajaxPaths : [{}],
	
	urlLoaded : [{}],
	
	getPath : function(pathIndex) {
	    
	   path = this.ajaxPaths[pathIndex];
	   
	   return path;
	    
	}
    
};

