/**************************************************************************
* PUBLIC UTILITY CLASS TO MANAGE CALLBACK REQUESTS FOR EXPANDED SLIDER - 
use to render custom JS events for custom slides. Both homepage sliders send
message to this Class to allow for customized javascript to be fired.

*/



slideCallbackExtension = {
    
    currentIndex : {},

	showBackButton : true,
    
    prevIndex : {},
    
    
	//slide has finished animating and landed on sent index
	slideComplete : function(index) {
	    
	     var eventObj = {
    	        target:this,
    	        index:index
    	    }
            
            this.dispatchEvent("slide_complete", eventObj);
	    
	    
        if(index == 1) {
            
            //alert('startAnim');
            $.fn.timelineControl.slideIn();
            
           
            
        }
        
        if(this.prevIndex == 0) {
            
            //alert('resetAnimation');
        }
	    

	},

	//slide has begun sliding to sent index
	slideTo : function(index, showBack) {
	    
	    /* var eventObj = {
    	        target:this,
    	        index:index
    	    }*/
            
           /* this.dispatchEvent("slide_to", eventObj);*/

			/*if(showBack != undefined) {
				this.showBackButton = showBack;
			}*/
    
	   
	   
	   
	   // console.log('has begun sliding to index ' + index);
	  /* if(this.showBackButton === true) {

	       $('ul.expanded-slides li.expanded-slide').eq(index).find('a.back-link').show();
	   } else {
	       $('ul.expanded-slides li.expanded-slide').eq(index).find('a.back-link').hide();
	    
	   }*/
	   
	  /* var eventObj = {
	       target:this,
	       index:index
	   }*/
	   
	  // this.dispatchEvent("slide_to", eventObj);

	},

	//slide is moving from sent index which is now previous index.
	slideOut : function(index) {

	    //console.log('slide is moving from sent index ' + index);
	    this.prevIndex = index;
	    
	    if(index == 1) {
	        $.fn.timelineControl.slideOut();
	    }

	}

};

slideCallbackExtension = EventDispatcher.implementEventDispatcher(slideCallbackExtension);
