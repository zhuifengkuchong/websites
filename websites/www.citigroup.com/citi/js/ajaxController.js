/**************************************************************************
* PUBLIC UTILITY CLASS TO MANAGE AJAX REQUESTS BETWEEN SLIDERS
*/

ajaxController = {
	
	
	lazyLoad : function(index) {
	    
	    if(landingModel.urlLoaded[index] == false) { //check if URL has been loaded
	        
            path = landingModel.getPath(index); // get appropriate path for ajax url from the model
            
            landingModel.urlLoaded[index] = true;

            $('ul.expanded-slides li.expanded-slide').eq(index).load(path);

            } else {

            return;

        }
	   
	},
	
	ajaxCheck :function(index) {
	    
	    this.lazyLoad(index);
	    
	    if(index > 0) {
	        
	        this.lazyLoad(index - 1);
	        
	    }
	    
	    if(index !== landingModel.ajaxPaths.length) {
	        
	        this.lazyLoad(index + 1);
	        
	    }
	    
	}
    
};

