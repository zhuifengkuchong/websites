(function( $ )
{
	$.fn.timelineControl = function() {
		

/**************************************************************************
* ESTABLISH ROUTE SCOPE
*/
 	var obj = this;
 	
	
/**************************************************************************
* DEFINE GLOBAL VARIABLES
*/


/**************************************************************************
* DEFINE MAIN FUNCTIONS
*/

    function slideInAnim() {
        var fadeDelay = 0;
        $('ul.chip-holder li').each(function() {
           $(this).delay(fadeDelay).fadeTo(250, 1);
            fadeDelay += 30;
        });
    }
    
    function slideOutAnim() {
        var fadeDelay = 0;
        
        $('ul.chip-holder li').each(function() {
           $(this).delay(fadeDelay).fadeTo(250, 0);
            fadeDelay += 30;
        });
    }
    
    $.fn.timelineControl.slideIn = function() {
            slideInAnim();
    }
    
    $.fn.timelineControl.slideOut = function() {
            slideOutAnim();
    }

/**************************************************************************
* BIND LISTENERS
*/	

   $('ul.chip-holder li').mouseenter(function() {
       $(this).find('.slide-two').show();
       $(this).find('.slide-two').stop(true, true).animate({height:'82px', width: '148px'}, 180, 'easeOutQuad');
       
   });
   
   $('ul.chip-holder li').mouseleave(function() {
         $(this).find('.slide-two').show();
         $(this).find('.slide-two').stop(true, true).animate({height:'0px', width: '0px'}, 180, 'easeOutQuad',
            function() {
                //$(this).find('img.arrow').css({bottom:'-30px'});
                //$(this).find('img.arrow').show();
                $(this).hide();
         });
      });
	
 

	
};
})( jQuery );// JavaScript Document