;
(function($, nmcom, undefined) {
    'use strict';

    nmcom.Services = nmcom.Services || {};
    nmcom.AdobeTracker = window._satellite !== undefined ? window._satellite : undefined;

    //#region Http Service
    nmcom.Services.Http = (function() {
        var post = function(url, header, data) {
            /// <summary>Perform a generic AJAX POST</summary>
            /// <param name="url" type="Object"></param>
            /// <param name="header" type="Object"></param>
            /// <param name="data" type="Object"></param>
            return $.ajax({
                type: 'POST',
                dataType: 'json',
                headers: header,
                url: url,
                contentType: 'application/json; charset=utf-8',
                data: data
            });
        };
        var get = function(url, header, data) {
            /// <summary>Perform a generic AJAX GET</summary>
            /// <param name="url" type="Object"></param>
            /// <param name="header" type="Object"></param>
            /// <param name="data" type="Object"></param>
            return $.ajax({
                type: 'GET',
                dataType: 'json',
                headers: header,
                url: url,
                contentType: 'application/json; charset=utf-8',
                data: data
            });
        };
        var html = function(url) {
            /// <summary>Get an html response</summary>
            /// <param name="url" type="Object"></param>
            return $.ajax({
                type: 'GET',
                url: url,
                contentType: 'html'
            });
        };
        var script = function(url) {
            /// <summary>Perform a AJAX GET for javascript</summary>
            /// <param name="url" type="Object"></param>
            return $.getScript(url);
        };
        return {
            POST: post,
            GET: get,
            HTML: html,
            SCRIPT: script
        };
    })();
    //#endregion

    $.extend(nmcom, {
        settings: {
            fullScreenWidth: 1000,
            tabletWidth: 768,
            menuOpenDelay: 100, //milliseconds
            menuCloseTimeout: false,
            menuCloseDelay: 0, //milliseconds
            isCorsSupported: true,
            tabScrollTime: 1000,
            tabCookieName: "default-tab-name"
        },

        /*
         * This function initializes the nmcom application.  <strong>Only call after DOM is ready</strong>
         */
        init: function() {

            this.ellipsis();
            this.setupTemplating();
            this.setupEvents();
            this.initGlobalLegacyPlaceholders();
            this.setupAjaxErrorHandler();
            this.isCorsSupported();
            nmcom.leadTracking.configLeadTrackingCookie();
            nmcom.leadTracking.configCampaignTrackingCookie();
            this.setupFRDetails();
            this.globalAlert();
            this.setTriggerGoals();
        },

        isCorsSupported: function() {
            if ('withCredentials' in $.ajaxSettings.xhr()) {
                this.settings.isCorsSupported = true;
            } else {
                this.settings.isCorsSupported = false;
            }
        },

        setupTemplating: function() {

            // this changes the default delimiters for Underscore templates from greater/less
            // than symbols to curly braces to be more in line with mustache-style (handlebars) templating
            // http://underscorejs.org/#template
            _.templateSettings = {
                interpolate: /\{\{(.+?)\}\}/g, // print value: {{ value_name }}
                evaluate: /\{%([\s\S]+?)%\}/g, // excute code: {% code_to_execute %}
                escape: /\{%-([\s\S]+?)%\}/g // escape HTML: {%- <script> %} prints &lt;script&gt;
            };

        },

        setupAjaxErrorHandler: function() {
            $(document).ajaxError(function(event, jqXhr, settings, exception) {
                var error = {
                    scriptName: settings.url,
                    message: 'Ajax Exception: ' + exception + ' | HTTP Status: ' + jqXhr.status,
                    originUrl: window.location.href,
                    browser: window.navigator.userAgent,
                    lineNumber: 0,
                    level: 3
                };
                nmcom.errorHandler.logError(error);
            });
        },

        /*
         * Initialize the jQuery events for the entire NMCOM application
         */
        setupEvents: function() {

            this.hideNav();
            this.initTertiaryNav();
            this.columnSplit();
            this.toggleSubNav();
            this.advancedSearch();
            this.tabs();
            this.itemsWidget();
            this.setupForms();
            this.disclosure();
            this.launchSimpleSlider();
            this.addCursorPositionFunction();
            this.toggleLoginInfo();
            this.highlightCurrentTab();
            this.accordion();
            this.printButton();
            this.hideFilters();
            this.contactModal();
        },

        /* Manages main nav toggle */
        hideNav: function() {
            $('.header-container nav .nav-toggle').click(function() {
                $('.header-container nav').toggleClass('closed');
            });
        },

        toggleSubNav: function() {

            var self = this;

            $('.main > li button, .secondary > li button').click(function() {

                var openTarget = !$(this).closest('li').hasClass('active');
                $('.nav-menus li').removeClass('active');
                if (openTarget) {
                    $(this).closest('li').toggleClass('active');
                }

            });

            $('nav .title a').click(function(e) {
                self.openNavItem(e);
            });

            $('nav li > .title').hover(function(e) {
                var windowWidth = $(window).width();
                if (windowWidth >= self.settings.fullScreenWidth) {
                    clearTimeout(self.settings.menuOpenTimeout);
                    self.settings.menuOpenTimeout = setTimeout(function() {
                        self.openNavItem(e);
                    }, self.settings.menuOpenDelay);
                }

            }, function(e) {
                var windowWidth = $(window).width();
                if (windowWidth >= self.settings.fullScreenWidth) {
                    clearTimeout(self.settings.menuOpenTimeout);
                    self.closeNavItem(e);
                }
            });

            $('.nav-menus>ul>li, .sub-menu').mouseleave(function() {
                var windowWidth = $(window).width();
                if (windowWidth >= self.settings.fullScreenWidth) {
                    setTimeout(function() {
                        $('.nav-menus li').removeClass('active');
                    }, 100);
                }
            });

            $(document).on('click', function(e) {
                self.closeNavItem(e);
            });

        },

        openNavItem: function(e) {
            if ($(e.target).closest('li').hasClass('active') && $(e.target).is('[href]')) {
                window.location = $(e.target).attr('href');
                return;
            }

            e.preventDefault();
            var $selection = $(e.target).closest('li');
            $('ul.main li').not($selection).removeClass('active');
            $(e.target).closest('li').toggleClass('active');
            nmcom.columnSplit();
        },

        closeNavItem: function(e) {
            if ($(e.target).closest('.nav-menus').length || (e.target.type === 'button' && e.target.className === 'nav-toggle')) {
                return;
            }

            $('.nav-menus li').removeClass('active');
        },
        /* Ends main nav toggle */

        /* Manages tertiary nav */
        initTertiaryNav: function() {
            $('.tertiary-menu-trigger').click(function() {
                $('.tertiary-nav').addClass('active');
            });
            $('.close-menu').click(function() {
                $('.tertiary-nav').removeClass('active');
            });
            $('.tertiary-nav .more>a').on('click', function(e) {
                e.preventDefault();
            });
        },
        /* Ends tertiary nav */

        /* Advanced Search */
        advancedSearch: function() {
            if ($('.advanced-search')) {
                $('.advanced-search .content').hide();
                $('.advanced-search .title').click(function() {
                    $(this).toggleClass('active');
                    $('.advanced-search .content').slideToggle();
                });
            }
        },
        /* Ends Advanced Search */

        columnSplit: function() {
            $('ul.column').each(function() {
                var $thisUl = $(this);
                var $totalHeight = $($thisUl).innerHeight();
                var $currentHeight = 0;
                var $height = 0;
                $($thisUl).children('li').each(function() {
                    $height = $(this).outerHeight();
                    $currentHeight = $currentHeight + $height;
                    if ($currentHeight > $totalHeight) {
                        var $boundry = $(this);
                        $('<ul>').insertAfter($boundry.parent()).append($boundry.nextAll().andSelf());
                        $currentHeight = 0;
                    }

                });
            });
        },

        /* default form behaviors */
        setupForms: function() {
            $('form #upload-link').on('click', function(e) {
                e.preventDefault();
                $('#upload-file:hidden').trigger('click');
            });
        },

        /* Print button */
        printButton: function() {
            $('.print').on('click', function() {
                window.print();
            });
        },

        /* Items Widget */
        itemsWidget: function() {
            var self = this;
            if ($('.items-widget')) {
                $('.items-widget .headline').click(function() {
                    var windowWidth = $(window).width();
                    $(this).parent('li').siblings().addClass('hide');
                    if (windowWidth < self.settings.tabletWidth) {
                        $(this).parent('li').toggleClass('hide');
                    } else {
                        $(this).parent('li').removeClass('hide');
                    }
                });
            }
        },

        /* tabs */
        highlightCurrentTab: function() {

            var currentPageUrl = window.location.href;
            var urlParts = currentPageUrl.split('/');
            var currentPage = urlParts[urlParts.length - 1];

            if (currentPage !== '') {
                var currentTab = $('.title a[href$="' + currentPage + '"]');
                if (currentTab.length && !currentTab.hasClass('active')) {
                    currentTab.addClass('active');
                }
            }

        },
        tabs: function() {
            if ($('.tabbed-content')) {
                var self = this;
                var scrollTo = false;

                var defaultTabName = nmcom.getCookie(self.settings.tabCookieName);
                var queryStringTabName = nmcom.getQueryStringParam("tab");

                //if query string tab is present
                if (queryStringTabName.length) {
                    defaultTabName = queryStringTabName;
                    nmcom.setCookie(self.settings.tabCookieName, defaultTabName, 0);
                    scrollTo = true;
                } else if (defaultTabName === null || defaultTabName === '') {
                    defaultTabName = nmcom.getQueryStringParam("tab");
                    nmcom.setCookie(self.settings.tabCookieName, defaultTabName, 0);
                    scrollTo = true;
                } else {
                    defaultTabName = '';
                }

                var tabHeader = $('.tabbed-content .tabs li[rel="' + defaultTabName + '"]');
                var tabBody = $('.tabbed-content .tab[rel="' + defaultTabName + '"]');

                $('.tabbed-content').each(function() {
                    $(this).find('.tab').addClass('hide');

                    if (defaultTabName.length && $(this).find(tabHeader).length && $(this).find(tabBody).length) {
                        tabHeader.addClass('active');
                        tabBody.removeClass('hide');
                    } else {
                        $(this).find('.tabs li.default').addClass('active');
                        $(this).find('.tab.default').removeClass('hide');
                    }
                });

                if (scrollTo && defaultTabName.length && tabHeader.length && tabBody.length) {
                    setTimeout(function() {
                        $('html, body').animate({
                            scrollTop: tabHeader.offset().top - 20
                        }, self.settings.tabScrollTime);
                    }, self.settings.tabScrollTime);
                }

                $('ul.tabs li').click(function() {
                    var $label = $(this).attr('rel');
                    nmcom.setCookie(self.settings.tabCookieName, $label, 0);
                    $(this).siblings('li').removeClass('active');
                    $(this).addClass('active');
                    $('.tab').removeClass('hidden');
                    $('.tab[rel="' + $label + '"]').siblings().addClass('hide');
                    $('.tab[rel="' + $label + '"]').removeClass('hide');
                });
                $('.tab h3').click(function() {
                    var $label = $(this).parent('.tab').attr('rel');
                    nmcom.setCookie(self.settings.tabCookieName, $label, 0);
                    $(this).parent('.tab').siblings().addClass('hidden');
                    $(this).parent('.tab').toggleClass('hide');
                    $(this).parent('.tab').removeClass('hidden');
                    $('.tabs li[rel="' + $label + '"]').siblings().removeClass('active');
                    $('.tabs li[rel="' + $label + '"]').toggleClass('active');
                });
            }
        },
        /* Ends Tabs */

        disclosure: function() {
            if ($('.disclosure')) {
                var $hideText = $('#close-disclosure').text();
                var $showText = $('#open-disclosure').text();
                $('.disclosure').addClass('interactive');
                if (!$('.disclosure').hasClass('open')) {
                    $('.disclosure button').html($showText);
                    $('.disclosure .content').hide();
                }
                $('.disclosure button').click(function() {
                    $('.disclosure').toggleClass('open');
                    $('.disclosure .content').slideToggle();
                    if ($(this).parent('.disclosure').hasClass('open')) {
                        $(this).html($hideText);
                    } else {
                        $(this).html($showText);
                    }
                });
            }
        },

        /* function to create a carousel based on the provided configuration */
        carousel: function(config) {
            if (config.containerId === undefined) {
                return;
            }

            var container = $('#' + config.containerId);

            // no container, or container with less than two slides: hide the nav and don't apply the slider
            if (!container.length || container.children().length < 2) {
                var n = $('#' + config.navId);
                if (n.length) {
                    n.hide();
                }
                return;
            }

            var slickOptions = {
                autoplay: config.AutoPlay,
                autoplaySpeed: config.AutoPlaySpeed,
                speed: config.Speed,
                slidesToShow: config.SlidesToShow,
                slidesToScroll: config.SlidesToScroll
            };

            if (config.Infinite !== undefined) {
                slickOptions.infinite = config.Infinite;
            }

            if (config.Responsive !== undefined) {
                slickOptions.responsive = config.Responsive;
            }

            var nav;
            if (config.navId !== undefined) {
                nav = $('#' + config.navId);
                if (nav.length) {
                    slickOptions.asNavFor = nav;
                }
            } else {
                slickOptions.dots = config.ShowNavDots;
            }

            container.slick(slickOptions);

            // if there are .ellipsis items, we need to make sure the plugin runs when they are shown.
            if (container.find('.ellipsis').length) {
                container.on('beforeChange', function () {
                    var ellipsisContainer = container.find('.ellipsis');
                    ellipsisContainer.trigger("destroy");
                    window.nmcom.ellipsis(ellipsisContainer);
                });
            }

            if (config.navId !== undefined && nav !== undefined && nav.length) {
                var navSlickOptions = {
                    slidesToShow: config.NavItemsToShow,
                    slidesToScroll: config.NavItemsToScroll,
                    asNavFor: container,
                    centerMode: false,
                    focusOnSelect: true
                };

                nav.slick(navSlickOptions);

                container.on('beforeChange', function(e, slick, currentSlide, nextSlide) {
                    nav.find('.active').removeClass('active');
                    nav.find('[data-slick-index="' + nextSlide + '"]').addClass('active');
                }.bind(nav));
            }
        },

        launchSimpleSlider: function() {
            var slider = $('.slider .slides');
            if (slider.length > 0) {
                slider.slick({
                    infinite: true,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    responsive: [
                        {
                            breakpoint: this.settings.tabletWidth,
                            settings: {
                                slidesToShow: 1
                            }
                        }
                    ]
                });
            }
        },

        setCookie: function(name, value, days) {
            var expires;
            if (days) {
                var date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                expires = '; expires=' + date.toGMTString();
            } else {
                expires = '';
            }
            document.cookie = name + '=' + value + expires + '; path=/';
        },

        getCookie: function(name) {
            var nameEq = name + '=';
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) === ' ') c = c.substring(1, c.length);
                if (c.indexOf(nameEq) === 0) {
                    return c.substring(nameEq.length, c.length);
                }
            }
            return null;
        },

        getQueryStringParam: function(varname) {

            var args = new Object({});
            var urlString = document.location.search.substring(1);
            var pairs = urlString.split('&');
            for (var i = 0; i < pairs.length; i++) {
                var pos = pairs[i].indexOf('=');
                if (pos === -1) {
                    continue;
                }
                var argname = pairs[i].substring(0, pos);
                var value = pairs[i].substring(pos + 1);
                args[argname] = unescape(value);
            }

            var x = unescape(args[varname]);
            return x === 'undefined' ? '' : x;
        },

        adobeDtmDirectCall: function(directCallRule) {
            if (nmcom.AdobeTracker !== undefined && nmcom.AdobeTracker.track !== undefined) {
                try {
                    nmcom.AdobeTracker.track(directCallRule);
                } catch (e) {
                    var errorMsg = {
                        scriptName: 'nmcom.js'/*tpa=https://www.northwesternmutual.com/NMCOM/assets/js/nmcom.js*/,
                        message: '_satellite: Failed to call .track() method with this rule: ' + directCallRule,
                        originUrl: window.location.href,
                        browser: window.navigator.userAgent,
                        lineNumber: 479,
                        level: 2
                    };

                    nmcom.errorHandler.logError(errorMsg);
                }
            }
        },

        isWin8TouchDevice: function() {
            /*
            gets user agent and checks to see if it's a Windows device (Trident) and if
            the device is touch enabled
            */

            var userAgent = navigator.userAgent;
            var pattern = /(.*Trident.*Touch.*|.*Touch.*Trident.*)/i;

            if (pattern.test(userAgent)) {
                return true;
            }

            return false;
        },

        /**
         * For old browsers (IE9), add placeholder functionality to inputs where we don't display a field label.
         * @param {array} $inputs - a jQuery array of input elements
         */
        setupLegacyPlaceholders: function($inputs) {

            $inputs.each(function() {

                var $input = $(this);
                var placeholderText = $input.attr('placeholder');

                // set placeholder on page load
                if ($input.val().trim() === '') {
                    $input.val(placeholderText);
                    $input.addClass('placeholder');
                }

                // upon clicking into the field set the cursor to the beginning
                $input.on('click', function() {
                    if ($input.val() === '' || $input.val() === placeholderText) {
                        $input.setCursorPosition(0);
                    }
                });

                // remove placeholder text goes upon typing
                // add placeholder text back when empty
                $input.on('keyup blur', function(e) {
                    var $trimmedInput = $input.val().trim();
                    if ($trimmedInput === '' || $trimmedInput === placeholderText) {
                        $input.val(placeholderText);
                        $input.addClass('placeholder');
                        if (e.type === 'keyup') {
                            $input.setCursorPosition(0);
                        }
                    } else if ($input.val().indexOf(placeholderText) >= 1) {
                        var userEnteredText = nmcom.replaceString(placeholderText, '', $input.val());
                        $input.val(userEnteredText);
                        $input.removeClass('placeholder');
                    } else {
                        $input.removeClass('placeholder');
                    }
                });

                // override the .val() function to return an empty string when the value is the same as the placeholder
                var originalVal = $.fn.val;
                $.fn.val = function(value) {
                    if (arguments.length >= 1) {
                        // setter invoked, do processing
                        return originalVal.call(this, value);
                    }

                    //getter invoked do processing
                    if ($input.is('[placeholder]') && originalVal.call(this) === $input.attr('placeholder')) {
                        return '';
                    }

                    return originalVal.call(this);
                };
            });

        },

        toggleLoginInfo: function() {
            $('.login-learn-more').on('click', function(e) {
                e.preventDefault();
                var $moreInfoLink = $(e.currentTarget);
                var $moreInfoContainer = $moreInfoLink.closest('.login-tile').find('.login-more-info');
                $moreInfoLink.toggleClass('expanded');
                $moreInfoContainer.slideToggle().toggleClass('expanded');
                var $moreInfoIcon = $moreInfoLink.next();
                if ($moreInfoContainer.hasClass('expanded')) {
                    $moreInfoLink.html('Close');
                    $moreInfoIcon.removeClass().addClass('close');
                    $moreInfoIcon.html('x');
                } else {
                    $moreInfoLink.html('Learn more');
                    $moreInfoIcon.removeClass();
                    $moreInfoIcon.html('&#9660;');
                }
            });
        },

        //Hide Filters
        hideFilters: function() {
            $('.hide-filters').on('click', function(e) {
                e.preventDefault();
                var refineSearch = document.getElementById('refineSearchContainer');
                var displaySetting = refineSearch.style.display;

                if (displaySetting === 'block') {
                    refineSearch.style.display = 'none';
                    $('.hide-filters').text("Show Filters");
                    $('.hide-filters').removeClass('hidden');
                } else {
                    refineSearch.style.display = 'block';
                    $('.hide-filters').text("Hide Filters");
                    $('.hide-filters').addClass('hidden');
                }
            });
        },

        // Contact modal
        contactModal: function() {
            var contactButton = $('.JScontactMeClick');
            var modalWrapper = $('body');

            contactButton.click(function() {
                nmcom.formatModalContactForm();
                modalWrapper.toggleClass('visible');
                $('header nav').addClass('closed');
            });
        },

        formatModalContactForm: function() {
            var $modalForm = $('#modal-wrapper form');

            //populate h2 with FR name
            $modalForm.find('h2').text('Contact ' + $('.footer-container .advisor-list .name').text());

            //hide fields that are not needed
            $('input[name="address_1"]').parents('.form-group').hide();
            $('input[name="city"]').parents('.form-item').hide();
            $('select[name="state"]').parents('.form-item').hide();
            $('input[name="policyowner"]').parents('.form-group').hide();

            //set agentNumber so that lead is assigned to FR
            $('input[name="AgentNumber"]').val(nmcom.getCookie("frDetailCard"));

            //set campaign name
            $('input[name="campName"]').val("NM.com FR Form");


        },

        // Simple accordion (FAQs, search facet groups)
        accordion: function() {
            var $accordionPanels = $('.accordion > dd');
            $accordionPanels.not('.open').hide();
            var $definitions = $('.accordion > dt');
            $('.accordion > dt').not('.headline-link').on('click', function (e) {
                e.preventDefault();
                var $definition = $(e.currentTarget);
                var $target = $definition.next();
                if ($target.hasClass('open')) {
                    $target.removeClass('open').slideUp();
                    $definition.removeClass('open');
                } else {
                    $accordionPanels.removeClass('open').slideUp();
                    $definitions.removeClass('open');
                    $definition.addClass('open');
                    $target.addClass('open').slideDown();
                }
                return false;
            });
        },

        addCursorPositionFunction: function() {
            $.fn.setCursorPosition = function(pos) {
                this.each(function(index, elem) {
                    if (elem.setSelectionRange) {
                        elem.setSelectionRange(pos, pos);
                    } else if (elem.createTextRange) {
                        var range = elem.createTextRange();
                        range.collapse(true);
                        range.moveEnd('character', pos);
                        range.moveStart('character', pos);
                        range.select();
                    }
                });
                return this;
            };
        },

        replaceString: function(oldString, newString, fullString) {
            return fullString.split(oldString).join(newString);
        },

        initGlobalLegacyPlaceholders: function() {

            // add placeholders for fields without labels in older browsers (IE9)
            // here it's for global search
            if (!Modernizr.input.placeholder) {
                nmcom.setupLegacyPlaceholders($('.search-placeholder'));
            }

        },

        setupFRDetails: function() {
            var cardBool = nmcom.getQueryStringParam("card");
            var agentNum = nmcom.getQueryStringParam("agentNumber");
            agentNum = (agentNum === "" || agentNum === null) ? nmcom.getQueryStringParam("agentnumber") : agentNum;

            if (cardBool && agentNum.length) {
                //nmcom.setCookie("frDetailCard", agentNum, 0);
                nmcom.setCookie("frDetailCard", agentNum, 10);
                //nmcom.setCookie("hideFindFPLink", "true", 0);
            }
        },

        isCookieValid: function(trackingCookie) {

            if (trackingCookie === undefined || trackingCookie === null || trackingCookie === "") {
                return false;
            }

            for (var prop in trackingCookie) {
                if (trackingCookie.hasOwnProperty(prop)) {
                    if (trackingCookie[prop] === "") {
                        return false;
                    }
                }
            }

            return true;
        },

        HideGlobalAlertBox: function() {
            $("#globalAlertBox").slideUp("slow");
            nmcom.setCookie("globalAlertBox", "hidden", 0);
        },

        globalAlert: function() {
            $("#globalAlertBox .global-alert-box-content a").click(function() {
                nmcom.HideGlobalAlertBox();
            });
        },

        setTriggerGoals: function() {
            $("a[class^='goal-'],a[class*=' goal-']").click(function() {
                nmcom.Services.Http.POST(
                    '/nmcomMvc/Components/TriggerGoal',
                    {},
                    JSON.stringify({ classValue: $(this).attr('class') })
                );
            });
        },

        ellipsis: function (elem) {
            var ellipsisContainer = elem === undefined ? $('.ellipsis') : elem;
            ellipsisContainer.dotdotdot({
                watch: true
            });
        }
    });

})(window.jQuery, window.nmcom || (window.nmcom = {}));

jQuery(function() {
    window.nmcom.init();
});