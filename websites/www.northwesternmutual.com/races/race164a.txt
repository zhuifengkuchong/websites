<script id = "race164a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race164={};
	myVars.races.race164.varName="Object[712].uuid";
	myVars.races.race164.varType="@varType@";
	myVars.races.race164.repairType = "@RepairType";
	myVars.races.race164.event1={};
	myVars.races.race164.event2={};
	myVars.races.race164.event1.id = "curCustNo";
	myVars.races.race164.event1.type = "onchange";
	myVars.races.race164.event1.loc = "curCustNo_LOC";
	myVars.races.race164.event1.isRead = "False";
	myVars.races.race164.event1.eventType = "@event1EventType@";
	myVars.races.race164.event2.id = "curCustYes";
	myVars.races.race164.event2.type = "onchange";
	myVars.races.race164.event2.loc = "curCustYes_LOC";
	myVars.races.race164.event2.isRead = "True";
	myVars.races.race164.event2.eventType = "@event2EventType@";
	myVars.races.race164.event1.executed= false;// true to disable, false to enable
	myVars.races.race164.event2.executed= false;// true to disable, false to enable
</script>

