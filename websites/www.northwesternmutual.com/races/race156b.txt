<script id = "race156b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race156={};
	myVars.races.race156.varName="Object[712].uuid";
	myVars.races.race156.varType="@varType@";
	myVars.races.race156.repairType = "@RepairType";
	myVars.races.race156.event1={};
	myVars.races.race156.event2={};
	myVars.races.race156.event1.id = "Lu_Id_select_1";
	myVars.races.race156.event1.type = "onchange";
	myVars.races.race156.event1.loc = "Lu_Id_select_1_LOC";
	myVars.races.race156.event1.isRead = "True";
	myVars.races.race156.event1.eventType = "@event1EventType@";
	myVars.races.race156.event2.id = "txtAreaMore";
	myVars.races.race156.event2.type = "onchange";
	myVars.races.race156.event2.loc = "txtAreaMore_LOC";
	myVars.races.race156.event2.isRead = "False";
	myVars.races.race156.event2.eventType = "@event2EventType@";
	myVars.races.race156.event1.executed= false;// true to disable, false to enable
	myVars.races.race156.event2.executed= false;// true to disable, false to enable
</script>

