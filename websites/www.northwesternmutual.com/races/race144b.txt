<script id = "race144b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race144={};
	myVars.races.race144.varName="Object[712].uuid";
	myVars.races.race144.varType="@varType@";
	myVars.races.race144.repairType = "@RepairType";
	myVars.races.race144.event1={};
	myVars.races.race144.event2={};
	myVars.races.race144.event1.id = "q";
	myVars.races.race144.event1.type = "onkeypress";
	myVars.races.race144.event1.loc = "q_LOC";
	myVars.races.race144.event1.isRead = "True";
	myVars.races.race144.event1.eventType = "@event1EventType@";
	myVars.races.race144.event2.id = "headerSearchButton";
	myVars.races.race144.event2.type = "onkeypress";
	myVars.races.race144.event2.loc = "headerSearchButton_LOC";
	myVars.races.race144.event2.isRead = "False";
	myVars.races.race144.event2.eventType = "@event2EventType@";
	myVars.races.race144.event1.executed= false;// true to disable, false to enable
	myVars.races.race144.event2.executed= false;// true to disable, false to enable
</script>

