<script id = "race148b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race148={};
	myVars.races.race148.varName="Object[712].uuid";
	myVars.races.race148.varType="@varType@";
	myVars.races.race148.repairType = "@RepairType";
	myVars.races.race148.event1={};
	myVars.races.race148.event2={};
	myVars.races.race148.event1.id = "Lu_Id_button_7";
	myVars.races.race148.event1.type = "onchange";
	myVars.races.race148.event1.loc = "Lu_Id_button_7_LOC";
	myVars.races.race148.event1.isRead = "True";
	myVars.races.race148.event1.eventType = "@event1EventType@";
	myVars.races.race148.event2.id = "Lu_Id_button_8";
	myVars.races.race148.event2.type = "onchange";
	myVars.races.race148.event2.loc = "Lu_Id_button_8_LOC";
	myVars.races.race148.event2.isRead = "False";
	myVars.races.race148.event2.eventType = "@event2EventType@";
	myVars.races.race148.event1.executed= false;// true to disable, false to enable
	myVars.races.race148.event2.executed= false;// true to disable, false to enable
</script>

