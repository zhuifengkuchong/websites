<script id = "race136b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race136={};
	myVars.races.race136.varName="Object[712].uuid";
	myVars.races.race136.varType="@varType@";
	myVars.races.race136.repairType = "@RepairType";
	myVars.races.race136.event1={};
	myVars.races.race136.event2={};
	myVars.races.race136.event1.id = "Lu_Id_input_7";
	myVars.races.race136.event1.type = "onkeypress";
	myVars.races.race136.event1.loc = "Lu_Id_input_7_LOC";
	myVars.races.race136.event1.isRead = "True";
	myVars.races.race136.event1.eventType = "@event1EventType@";
	myVars.races.race136.event2.id = "curCustYes";
	myVars.races.race136.event2.type = "onkeypress";
	myVars.races.race136.event2.loc = "curCustYes_LOC";
	myVars.races.race136.event2.isRead = "False";
	myVars.races.race136.event2.eventType = "@event2EventType@";
	myVars.races.race136.event1.executed= false;// true to disable, false to enable
	myVars.races.race136.event2.executed= false;// true to disable, false to enable
</script>

