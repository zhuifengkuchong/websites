<script id = "race193b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race193={};
	myVars.races.race193.varName="Object[712].uuid";
	myVars.races.race193.varType="@varType@";
	myVars.races.race193.repairType = "@RepairType";
	myVars.races.race193.event1={};
	myVars.races.race193.event2={};
	myVars.races.race193.event1.id = "curCustYes";
	myVars.races.race193.event1.type = "onfocus";
	myVars.races.race193.event1.loc = "curCustYes_LOC";
	myVars.races.race193.event1.isRead = "True";
	myVars.races.race193.event1.eventType = "@event1EventType@";
	myVars.races.race193.event2.id = "curCustNo";
	myVars.races.race193.event2.type = "onfocus";
	myVars.races.race193.event2.loc = "curCustNo_LOC";
	myVars.races.race193.event2.isRead = "False";
	myVars.races.race193.event2.eventType = "@event2EventType@";
	myVars.races.race193.event1.executed= false;// true to disable, false to enable
	myVars.races.race193.event2.executed= false;// true to disable, false to enable
</script>

