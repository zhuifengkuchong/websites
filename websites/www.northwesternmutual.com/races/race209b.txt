<script id = "race209b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race209={};
	myVars.races.race209.varName="Object[712].uuid";
	myVars.races.race209.varType="@varType@";
	myVars.races.race209.repairType = "@RepairType";
	myVars.races.race209.event1={};
	myVars.races.race209.event2={};
	myVars.races.race209.event1.id = "Lu_Id_button_4";
	myVars.races.race209.event1.type = "onblur";
	myVars.races.race209.event1.loc = "Lu_Id_button_4_LOC";
	myVars.races.race209.event1.isRead = "True";
	myVars.races.race209.event1.eventType = "@event1EventType@";
	myVars.races.race209.event2.id = "Lu_Id_button_5";
	myVars.races.race209.event2.type = "onblur";
	myVars.races.race209.event2.loc = "Lu_Id_button_5_LOC";
	myVars.races.race209.event2.isRead = "False";
	myVars.races.race209.event2.eventType = "@event2EventType@";
	myVars.races.race209.event1.executed= false;// true to disable, false to enable
	myVars.races.race209.event2.executed= false;// true to disable, false to enable
</script>

