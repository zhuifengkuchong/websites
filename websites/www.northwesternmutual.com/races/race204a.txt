<script id = "race204a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race204={};
	myVars.races.race204.varName="Object[712].uuid";
	myVars.races.race204.varType="@varType@";
	myVars.races.race204.repairType = "@RepairType";
	myVars.races.race204.event1={};
	myVars.races.race204.event2={};
	myVars.races.race204.event1.id = "Lu_Id_button_10";
	myVars.races.race204.event1.type = "onblur";
	myVars.races.race204.event1.loc = "Lu_Id_button_10_LOC";
	myVars.races.race204.event1.isRead = "False";
	myVars.races.race204.event1.eventType = "@event1EventType@";
	myVars.races.race204.event2.id = "Lu_Id_button_9";
	myVars.races.race204.event2.type = "onblur";
	myVars.races.race204.event2.loc = "Lu_Id_button_9_LOC";
	myVars.races.race204.event2.isRead = "True";
	myVars.races.race204.event2.eventType = "@event2EventType@";
	myVars.races.race204.event1.executed= false;// true to disable, false to enable
	myVars.races.race204.event2.executed= false;// true to disable, false to enable
</script>

