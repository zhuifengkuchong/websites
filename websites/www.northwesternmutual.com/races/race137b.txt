<script id = "race137b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race137={};
	myVars.races.race137.varName="Object[712].uuid";
	myVars.races.race137.varType="@varType@";
	myVars.races.race137.repairType = "@RepairType";
	myVars.races.race137.event1={};
	myVars.races.race137.event2={};
	myVars.races.race137.event1.id = "Lu_Id_input_6";
	myVars.races.race137.event1.type = "onkeypress";
	myVars.races.race137.event1.loc = "Lu_Id_input_6_LOC";
	myVars.races.race137.event1.isRead = "True";
	myVars.races.race137.event1.eventType = "@event1EventType@";
	myVars.races.race137.event2.id = "Lu_Id_input_7";
	myVars.races.race137.event2.type = "onkeypress";
	myVars.races.race137.event2.loc = "Lu_Id_input_7_LOC";
	myVars.races.race137.event2.isRead = "False";
	myVars.races.race137.event2.eventType = "@event2EventType@";
	myVars.races.race137.event1.executed= false;// true to disable, false to enable
	myVars.races.race137.event2.executed= false;// true to disable, false to enable
</script>

