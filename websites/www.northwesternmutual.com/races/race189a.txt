<script id = "race189a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race189={};
	myVars.races.race189.varName="Object[712].uuid";
	myVars.races.race189.varType="@varType@";
	myVars.races.race189.repairType = "@RepairType";
	myVars.races.race189.event1={};
	myVars.races.race189.event2={};
	myVars.races.race189.event1.id = "Lu_Id_input_11";
	myVars.races.race189.event1.type = "onfocus";
	myVars.races.race189.event1.loc = "Lu_Id_input_11_LOC";
	myVars.races.race189.event1.isRead = "False";
	myVars.races.race189.event1.eventType = "@event1EventType@";
	myVars.races.race189.event2.id = "Lu_Id_input_10";
	myVars.races.race189.event2.type = "onfocus";
	myVars.races.race189.event2.loc = "Lu_Id_input_10_LOC";
	myVars.races.race189.event2.isRead = "True";
	myVars.races.race189.event2.eventType = "@event2EventType@";
	myVars.races.race189.event1.executed= false;// true to disable, false to enable
	myVars.races.race189.event2.executed= false;// true to disable, false to enable
</script>

