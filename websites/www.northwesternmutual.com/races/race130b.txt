<script id = "race130b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race130={};
	myVars.races.race130.varName="Object[712].uuid";
	myVars.races.race130.varType="@varType@";
	myVars.races.race130.repairType = "@RepairType";
	myVars.races.race130.event1={};
	myVars.races.race130.event2={};
	myVars.races.race130.event1.id = "Lu_Id_input_11";
	myVars.races.race130.event1.type = "onkeypress";
	myVars.races.race130.event1.loc = "Lu_Id_input_11_LOC";
	myVars.races.race130.event1.isRead = "True";
	myVars.races.race130.event1.eventType = "@event1EventType@";
	myVars.races.race130.event2.id = "Lu_Id_input_12";
	myVars.races.race130.event2.type = "onkeypress";
	myVars.races.race130.event2.loc = "Lu_Id_input_12_LOC";
	myVars.races.race130.event2.isRead = "False";
	myVars.races.race130.event2.eventType = "@event2EventType@";
	myVars.races.race130.event1.executed= false;// true to disable, false to enable
	myVars.races.race130.event2.executed= false;// true to disable, false to enable
</script>

