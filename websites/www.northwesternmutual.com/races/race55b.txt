<script id = "race55b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race55={};
	myVars.races.race55.varName="Lu_Id_input_2__onchange";
	myVars.races.race55.varType="@varType@";
	myVars.races.race55.repairType = "@RepairType";
	myVars.races.race55.event1={};
	myVars.races.race55.event2={};
	myVars.races.race55.event1.id = "Lu_Id_input_2";
	myVars.races.race55.event1.type = "onchange";
	myVars.races.race55.event1.loc = "Lu_Id_input_2_LOC";
	myVars.races.race55.event1.isRead = "True";
	myVars.races.race55.event1.eventType = "@event1EventType@";
	myVars.races.race55.event2.id = "Lu_DOM";
	myVars.races.race55.event2.type = "onDOMContentLoaded";
	myVars.races.race55.event2.loc = "Lu_DOM_LOC";
	myVars.races.race55.event2.isRead = "False";
	myVars.races.race55.event2.eventType = "@event2EventType@";
	myVars.races.race55.event1.executed= false;// true to disable, false to enable
	myVars.races.race55.event2.executed= false;// true to disable, false to enable
</script>

