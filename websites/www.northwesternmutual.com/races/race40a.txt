<script id = "race40a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race40={};
	myVars.races.race40.varName="txtAreaMore__onchange";
	myVars.races.race40.varType="@varType@";
	myVars.races.race40.repairType = "@RepairType";
	myVars.races.race40.event1={};
	myVars.races.race40.event2={};
	myVars.races.race40.event1.id = "Lu_DOM";
	myVars.races.race40.event1.type = "onDOMContentLoaded";
	myVars.races.race40.event1.loc = "Lu_DOM_LOC";
	myVars.races.race40.event1.isRead = "False";
	myVars.races.race40.event1.eventType = "@event1EventType@";
	myVars.races.race40.event2.id = "txtAreaMore";
	myVars.races.race40.event2.type = "onchange";
	myVars.races.race40.event2.loc = "txtAreaMore_LOC";
	myVars.races.race40.event2.isRead = "True";
	myVars.races.race40.event2.eventType = "@event2EventType@";
	myVars.races.race40.event1.executed= false;// true to disable, false to enable
	myVars.races.race40.event2.executed= false;// true to disable, false to enable
</script>

