<script id = "race226b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race226={};
	myVars.races.race226.varName="Object[712].uuid";
	myVars.races.race226.varType="@varType@";
	myVars.races.race226.repairType = "@RepairType";
	myVars.races.race226.event1={};
	myVars.races.race226.event2={};
	myVars.races.race226.event1.id = "Lu_Id_input_4";
	myVars.races.race226.event1.type = "onblur";
	myVars.races.race226.event1.loc = "Lu_Id_input_4_LOC";
	myVars.races.race226.event1.isRead = "True";
	myVars.races.race226.event1.eventType = "@event1EventType@";
	myVars.races.race226.event2.id = "Lu_Id_input_5";
	myVars.races.race226.event2.type = "onblur";
	myVars.races.race226.event2.loc = "Lu_Id_input_5_LOC";
	myVars.races.race226.event2.isRead = "False";
	myVars.races.race226.event2.eventType = "@event2EventType@";
	myVars.races.race226.event1.executed= false;// true to disable, false to enable
	myVars.races.race226.event2.executed= false;// true to disable, false to enable
</script>

