<script id = "race106a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race106={};
	myVars.races.race106.varName="curCustNo__onblur";
	myVars.races.race106.varType="@varType@";
	myVars.races.race106.repairType = "@RepairType";
	myVars.races.race106.event1={};
	myVars.races.race106.event2={};
	myVars.races.race106.event1.id = "Lu_DOM";
	myVars.races.race106.event1.type = "onDOMContentLoaded";
	myVars.races.race106.event1.loc = "Lu_DOM_LOC";
	myVars.races.race106.event1.isRead = "False";
	myVars.races.race106.event1.eventType = "@event1EventType@";
	myVars.races.race106.event2.id = "curCustNo";
	myVars.races.race106.event2.type = "onblur";
	myVars.races.race106.event2.loc = "curCustNo_LOC";
	myVars.races.race106.event2.isRead = "True";
	myVars.races.race106.event2.eventType = "@event2EventType@";
	myVars.races.race106.event1.executed= false;// true to disable, false to enable
	myVars.races.race106.event2.executed= false;// true to disable, false to enable
</script>

