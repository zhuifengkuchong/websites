<script id = "race50b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race50={};
	myVars.races.race50.varName="Object[1238].uuid";
	myVars.races.race50.event1={};
	myVars.races.race50.event2={};
	myVars.races.race50.event1.id = "signup-email";
	myVars.races.race50.event1.type = "onfocus";
	myVars.races.race50.event1.loc = "signup-email_LOC";
	myVars.races.race50.event2.id = "signup-confirm-email";
	myVars.races.race50.event2.type = "onfocus";
	myVars.races.race50.event2.loc = "signup-confirm-email_LOC";
	myVars.races.race50.event1.executed= false;// true to disable, false to enable
	myVars.races.race50.event2.executed= false;// true to disable, false to enable
</script>

