<script id = "race25b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race25={};
	myVars.races.race25.varName="signup-email__onfocus";
	myVars.races.race25.event1={};
	myVars.races.race25.event2={};
	myVars.races.race25.event1.id = "signup-email";
	myVars.races.race25.event1.type = "onfocus";
	myVars.races.race25.event1.loc = "signup-email_LOC";
	myVars.races.race25.event2.id = "Lu_DOM";
	myVars.races.race25.event2.type = "onDOMContentLoaded";
	myVars.races.race25.event2.loc = "Lu_DOM_LOC";
	myVars.races.race25.event1.executed= false;// true to disable, false to enable
	myVars.races.race25.event2.executed= false;// true to disable, false to enable
</script>

