<script id = "race16a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race16={};
	myVars.races.race16.varName="signup-email__onchange";
	myVars.races.race16.event1={};
	myVars.races.race16.event2={};
	myVars.races.race16.event1.id = "Lu_DOM";
	myVars.races.race16.event1.type = "onDOMContentLoaded";
	myVars.races.race16.event1.loc = "Lu_DOM_LOC";
	myVars.races.race16.event2.id = "signup-email";
	myVars.races.race16.event2.type = "onchange";
	myVars.races.race16.event2.loc = "signup-email_LOC";
	myVars.races.race16.event1.executed= false;// true to disable, false to enable
	myVars.races.race16.event2.executed= false;// true to disable, false to enable
</script>

