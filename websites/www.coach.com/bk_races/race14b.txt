<script id = "race14b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race14={};
	myVars.races.race14.varName="mobile-nav__onchange";
	myVars.races.race14.event1={};
	myVars.races.race14.event2={};
	myVars.races.race14.event1.id = "mobile-nav";
	myVars.races.race14.event1.type = "onchange";
	myVars.races.race14.event1.loc = "mobile-nav_LOC";
	myVars.races.race14.event2.id = "Lu_DOM";
	myVars.races.race14.event2.type = "onDOMContentLoaded";
	myVars.races.race14.event2.loc = "Lu_DOM_LOC";
	myVars.races.race14.event1.executed= false;// true to disable, false to enable
	myVars.races.race14.event2.executed= false;// true to disable, false to enable
</script>

