<script id = "race18b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race18={};
	myVars.races.race18.varName="search-box__onchange";
	myVars.races.race18.event1={};
	myVars.races.race18.event2={};
	myVars.races.race18.event1.id = "search-box";
	myVars.races.race18.event1.type = "onchange";
	myVars.races.race18.event1.loc = "search-box_LOC";
	myVars.races.race18.event2.id = "Lu_DOM";
	myVars.races.race18.event2.type = "onDOMContentLoaded";
	myVars.races.race18.event2.loc = "Lu_DOM_LOC";
	myVars.races.race18.event1.executed= false;// true to disable, false to enable
	myVars.races.race18.event2.executed= false;// true to disable, false to enable
</script>

