<script id = "race9b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race9={};
	myVars.races.race9.varName="search-box__onkeypress";
	myVars.races.race9.event1={};
	myVars.races.race9.event2={};
	myVars.races.race9.event1.id = "search-box";
	myVars.races.race9.event1.type = "onkeypress";
	myVars.races.race9.event1.loc = "search-box_LOC";
	myVars.races.race9.event2.id = "Lu_DOM";
	myVars.races.race9.event2.type = "onDOMContentLoaded";
	myVars.races.race9.event2.loc = "Lu_DOM_LOC";
	myVars.races.race9.event1.executed= false;// true to disable, false to enable
	myVars.races.race9.event2.executed= false;// true to disable, false to enable
</script>

