<script id = "race6b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race6={};
	myVars.races.race6.varName="signup-confirm-email__onkeypress";
	myVars.races.race6.event1={};
	myVars.races.race6.event2={};
	myVars.races.race6.event1.id = "signup-confirm-email";
	myVars.races.race6.event1.type = "onkeypress";
	myVars.races.race6.event1.loc = "signup-confirm-email_LOC";
	myVars.races.race6.event2.id = "Lu_DOM";
	myVars.races.race6.event2.type = "onDOMContentLoaded";
	myVars.races.race6.event2.loc = "Lu_DOM_LOC";
	myVars.races.race6.event1.executed= false;// true to disable, false to enable
	myVars.races.race6.event2.executed= false;// true to disable, false to enable
</script>

