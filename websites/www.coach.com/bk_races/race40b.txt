<script id = "race40b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race40={};
	myVars.races.race40.varName="Object[1238].uuid";
	myVars.races.race40.event1={};
	myVars.races.race40.event2={};
	myVars.races.race40.event1.id = "signup-confirm-email";
	myVars.races.race40.event1.type = "onchange";
	myVars.races.race40.event1.loc = "signup-confirm-email_LOC";
	myVars.races.race40.event2.id = "mobile-nav";
	myVars.races.race40.event2.type = "onchange";
	myVars.races.race40.event2.loc = "mobile-nav_LOC";
	myVars.races.race40.event1.executed= false;// true to disable, false to enable
	myVars.races.race40.event2.executed= false;// true to disable, false to enable
</script>

