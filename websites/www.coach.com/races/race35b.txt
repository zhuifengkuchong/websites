<script id = "race35b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race35={};
	myVars.races.race35.varName="Object[1238].uuid";
	myVars.races.race35.varType="@varType@";
	myVars.races.race35.repairType = "@RepairType";
	myVars.races.race35.event1={};
	myVars.races.race35.event2={};
	myVars.races.race35.event1.id = "mobile-nav";
	myVars.races.race35.event1.type = "onkeypress";
	myVars.races.race35.event1.loc = "mobile-nav_LOC";
	myVars.races.race35.event1.isRead = "True";
	myVars.races.race35.event1.eventType = "@event1EventType@";
	myVars.races.race35.event2.id = "Lu_Id_button_1";
	myVars.races.race35.event2.type = "onkeypress";
	myVars.races.race35.event2.loc = "Lu_Id_button_1_LOC";
	myVars.races.race35.event2.isRead = "False";
	myVars.races.race35.event2.eventType = "@event2EventType@";
	myVars.races.race35.event1.executed= false;// true to disable, false to enable
	myVars.races.race35.event2.executed= false;// true to disable, false to enable
</script>

