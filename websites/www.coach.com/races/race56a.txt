<script id = "race56a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race56={};
	myVars.races.race56.varName="Object[1238].uuid";
	myVars.races.race56.varType="@varType@";
	myVars.races.race56.repairType = "@RepairType";
	myVars.races.race56.event1={};
	myVars.races.race56.event2={};
	myVars.races.race56.event1.id = "search-box";
	myVars.races.race56.event1.type = "onfocus";
	myVars.races.race56.event1.loc = "search-box_LOC";
	myVars.races.race56.event1.isRead = "False";
	myVars.races.race56.event1.eventType = "@event1EventType@";
	myVars.races.race56.event2.id = "Lu_Id_button_3";
	myVars.races.race56.event2.type = "onblur";
	myVars.races.race56.event2.loc = "Lu_Id_button_3_LOC";
	myVars.races.race56.event2.isRead = "True";
	myVars.races.race56.event2.eventType = "@event2EventType@";
	myVars.races.race56.event1.executed= false;// true to disable, false to enable
	myVars.races.race56.event2.executed= false;// true to disable, false to enable
</script>

