FSR.surveydefs = [{
    name: 'browse',
    section: 'apply',
    repeatdays: 90,
    invite: {
        when: 'onentry',
        exclude: {
            urls: ['apply.usbank.com/apply/', 'USB/CMSContent/pdf/DashBoard', 'Enrollment', 'https://www.usbank.com/foresee/USB/CMSContent/pdf/DashBoard/eSIGN_Consent_Agreement.pdf', 'https://www.usbank.com/foresee/USB/CMSContent/pdf/DashBoard/USBank_Terms_and_Conditions.pdf', 'https://www.usbank.com/foresee/USB/CMSContent/pdf/Dashboard/BillPayTermsAndConditions_USB.pdf', 'internetBanking/RequestRouter', 'customerdashboard', 'CustomerDashboard', 'DepositPointCentral', 'login']
        }
    },
    pop: {
        when: 'later',
        what: 'qualifier'
    },
    pin: 1,
    criteria: {
        sp: 50,
        lf: 2
    },
    include: {
        urls: ['https://www.usbank.com/foresee/apply.usbank.com']
    }
}, {
    name: 'browse',
    section: 'wealth',
    repeatdays: [90, 45],
    invite: {
        when: 'onentry',
        siteLogo: "sitelogo_wealth.gif"/*tpa=https://www.usbank.com/foresee/sitelogo_wealth.gif*/,
        /* Desktop */
        dialogs: [[{
            reverseButtons: false,
            headline: "We'd like your feedback.",
            blurb: "Thank you for visiting U.S. Bank / U.S. Bancorp Investments. You've been randomly chosen to take part in a brief survey to let us know what we're doing well and where we can improve.",
            noticeAboutSurvey: "Please take a few minutes to share your opinions, which are essential in helping us provide the best online experience possible.",
            attribution: "This survey is conducted by an independent company, ForeSee.",
            closeInviteButtonText: "Click to close.",
            declineButton: "No, thanks",
            acceptButton: "Yes, I'll give feedback",
            error: "Error",
            warnLaunch: "this will launch a new window"
        }]],
        exclude: {
            urls: ['usbank(?!(.)*CustomerDashboard/Index(.)*)'] //exclude all pages with 'usbank' in URL but not when 'CustomerDashboard' is present
        }
    },
    pop: {
        when: 'later',
        what: 'qualifier'
    },
    pin: 1,
    criteria: {
        sp: 30,
        lf: 1
    },
    include: {
        variables: [{
            name: 'ForeSee.UserType',
            value: 'Wealth'
        }]
    },
    tracker: {
        url: 'https://www.usbank.com/foresee/tracker_wealth.html'
    },
    qualifier: {
        url: 'https://www.usbank.com/foresee/qualifying_wealth.html'
    }
}, {
    name: 'browse',
    section: 'secure',
    repeatdays: 90,
    invite: {
        when: 'onentry',
        exclude: {
            urls: ['apply.usbank.com/apply/', 'USB/CMSContent/pdf/DashBoard', 'Enrollment', 'https://www.usbank.com/foresee/USB/CMSContent/pdf/DashBoard/eSIGN_Consent_Agreement.pdf', 'https://www.usbank.com/foresee/USB/CMSContent/pdf/DashBoard/USBank_Terms_and_Conditions.pdf', 'https://www.usbank.com/foresee/USB/CMSContent/pdf/Dashboard/BillPayTermsAndConditions_USB.pdf', 'internetBanking/RequestRouter', 'customerdashboard', 'CustomerDashboard', 'DepositPointCentral', 'login']
        }
    },
    pop: {
        when: 'later',
        what: 'qualifier'
    },
    pin: 1,
    criteria: {
        sp: 1,
        lf: 2
    },
    include: {
        variables: [{
            name: 'UsbMasterRIBConvertedUser.strPilotParticipantId',
            value: '.'
        }]
    }
}, {
    name: 'browse',
    repeatdays: 90,
    invite: {
        when: 'onentry',
        exclude: {
            urls: ['apply.usbank.com/apply/', 'USB/CMSContent/pdf/DashBoard', 'Enrollment', 'https://www.usbank.com/foresee/USB/CMSContent/pdf/DashBoard/eSIGN_Consent_Agreement.pdf', 'https://www.usbank.com/foresee/USB/CMSContent/pdf/DashBoard/USBank_Terms_and_Conditions.pdf', 'https://www.usbank.com/foresee/USB/CMSContent/pdf/Dashboard/BillPayTermsAndConditions_USB.pdf', 'internetBanking/RequestRouter', 'customerdashboard', 'CustomerDashboard', 'DepositPointCentral', 'login']
        }
    },
    pop: {
        when: 'later',
        what: 'qualifier'
    },
    criteria: {
        sp: 50,
        lf: 3
    },
    include: {
        urls: ['.']
    }
}];
FSR.properties = {
    //repeatdays: 90,
    
    repeatoverride: false,
    
    altcookie: {},
    
    language: {
        locale: 'en'
    },
    
    exclude: {
        variables: [{
            name: 'testAndTargetID',
            value: ['USBank_Global_OLB_Header_Send_Receive_Money']
        }]
    },
    
    zIndexPopup: 10000,
    
    ignoreWindowTopCheck: false,
    
    ipexclude: 'fsr$ip',
    
    mobileHeartbeat: {
        delay: 60, /*mobile on exit heartbeat delay seconds*/
        max: 3600 /*mobile on exit heartbeat max run time seconds*/
    },
    
    invite: {
    
        // For no site logo, comment this line:
        siteLogo: "sitelogo.gif"/*tpa=https://www.usbank.com/foresee/sitelogo.gif*/,
        
        //alt text fore site logo img
        siteLogoAlt: "",
        
        /* Desktop */
        dialogs: [[{
            reverseButtons: false,
            headline: "We'd like your feedback.",
            blurb: "Thanks for visiting our site. We've partnered with ForeSee - an industry leader in customer surveys - to get your input. You've been randomly chosen to participate in a satisfaction survey to let us know how we can improve your experience on usbank.com.",
            noticeAboutSurvey: "Please note: <b>This survey is designed to measure your</b> entire <b>site experience and will appear at the end of your visit.</b>",
            attribution: "This survey is conducted by an independent company, ForeSee.",
            closeInviteButtonText: "Click to close.",
            declineButton: "No, thanks",
            acceptButton: "Yes, I'll give feedback",
            error: "Error",
            warnLaunch: "this will launch a new window"
        
        
        }]],
        
        exclude: {
            urls: [/*'apply.usbank.com/apply/', 'USB/CMSContent/pdf/DashBoard', 'Enrollment', 'https://www.usbank.com/foresee/USB/CMSContent/pdf/DashBoard/eSIGN_Consent_Agreement.pdf', 'https://www.usbank.com/foresee/USB/CMSContent/pdf/DashBoard/USBank_Terms_and_Conditions.pdf', 'https://www.usbank.com/foresee/USB/CMSContent/pdf/Dashboard/BillPayTermsAndConditions_USB.pdf', 'internetBanking/RequestRouter', 'customerdashboard', 'CustomerDashboard', 'DepositPointCentral', 'login'*/],
            referrers: [],
            userAgents: [],
            browsers: [],
            cookies: [],
            variables: []
        },
        include: {
            local: ['.']
        },
        
        delay: 0,
        timeout: 0,
        
        hideOnClick: false,
        
        hideCloseButton: false,
        
        css: 'foresee-dhtml.css'/*tpa=https://www.usbank.com/foresee/foresee-dhtml.css*/,
        
        hide: [],
        
        hideFlash: false,
        
        type: 'dhtml',
        /* desktop */
        // url: 'https://www.usbank.com/foresee/invite.html'
        /* mobile */
        url: 'https://www.usbank.com/foresee/invite-mobile.html',
        back: 'url'
    
        //SurveyMutex: 'SurveyMutex'
    },
    
    tracker: {
        width: '690',
        height: '590',
        timeout: 3,
        adjust: true,
        alert: {
            enabled: true,
            message: 'The survey is now available.'
        },
        url: 'https://www.usbank.com/foresee/tracker.html'
    },
    
    survey: {
        width: 690,
        height: 600
    },
    
    qualifier: {
        footer: '<div id=\"fsrcontainer\"><div style=\"float:left;width:80%;font-size:8pt;text-align:left;line-height:12px;\">This survey is conducted by an independent company ForeSee,<br>on behalf of the site you are visiting.</div><div style=\"float:right;font-size:8pt;\"><a target="_blank" title="Validate TRUSTe privacy certification" href="https://privacy-policy.truste.com/click-with-confidence/ctv/en/www.foreseeresults.com/seal_m"><img border=\"0\" src=\"{%baseHref%}truste.png\" alt=\"Validate TRUSTe Privacy Certification\"></a></div></div>',
        width: '690',
        height: '630',
        bgcolor: '#333',
        opacity: 0.7,
        x: 'center',
        y: 'center',
        delay: 0,
        buttons: {
            accept: 'Continue'
        },
        hideOnClick: false,
        css: 'foresee-dhtml.css'/*tpa=https://www.usbank.com/foresee/foresee-dhtml.css*/,
        url: 'https://www.usbank.com/foresee/qualifying.html'
    },
    
    cancel: {
        url: 'https://www.usbank.com/foresee/cancel.html',
        width: '690',
        height: '400'
    },
    
    pop: {
        what: 'survey',
        after: 'leaving-site',
        pu: false,
        tracker: true
    },
    
    meta: {
        referrer: true,
        terms: true,
        ref_url: true,
        url: true,
        url_params: false,
        user_agent: false,
        entry: false,
        entry_params: false
    },
    
    events: {
        enabled: true,
        id: true,
        codes: {
            purchase: 800,
            items: 801,
            dollars: 802,
            followup: 803,
            information: 804,
            content: 805
        },
        pd: 7,
        custom: {
            purchase: {
                enabled: true,
                repeat: false,
                source: 'url',
                patterns: ['https://www.usbank.com/apply/confirmation.html', 'https://www.usbank.com/apply/resultDeposit.html']
            }
        
        }
    },
    
    previous: false,
    
    analytics: {
        google_local: false,
        google_remote: false
    },
    
    cpps: {
        CustomerTenure: {
            source: 'variable',
            name: 'UsbMasterRIBConvertedUser.strPilotParticipantId'
        },
        LPID: {
            source: 'cookie',
            name: 'riblpid'
        },
        Product: {
            source: 'function',
            value: function(){
                var out = "";
                for(var i=0; typeof reportingData != 'undefined' && reportingData != null && i<reportingData.products.length; i++){
                    if(i>0) out += ",";
                    out += reportingData.products[0].productName;
                }
                return out;
            }
        },
        Status: {
            source: 'cookie',
            name: 'riblpid',
            init: 'false',
            value: 'true'
        }
    
    },
    
    mode: 'hybrid'
};
