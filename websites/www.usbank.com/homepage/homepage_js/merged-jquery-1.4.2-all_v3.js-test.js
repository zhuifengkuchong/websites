/*!
 * jQuery JavaScript Library v1.4.2
 * http://jquery.com/
 *
 * Copyright 2010, John Resig
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * Includes Sizzle.js
 * http://sizzlejs.com/
 * Copyright 2010, The Dojo Foundation
 * Released under the MIT, BSD, and GPL Licenses.
 *
 * Date: Sat Feb 13 22:33:48 2010 -0500
 */
(function(window,undefined){var jQuery=function(selector,context){return new jQuery.fn.init(selector,context)},_jQuery=window.jQuery,_$=window.$,document=window.document,rootjQuery,quickExpr=/^[^<]*(<[\w\W]+>)[^>]*$|^#([\w-]+)$/,isSimple=/^.[^:#\[\.,]*$/,rnotwhite=/\S/,rtrim=/^(\s|\u00A0)+|(\s|\u00A0)+$/g,rsingleTag=/^<(\w+)\s*\/?>(?:<\/\1>)?$/,userAgent=navigator.userAgent,browserMatch,readyBound=false,readyList=[],DOMContentLoaded,toString=Object.prototype.toString,hasOwnProperty=Object.prototype.hasOwnProperty,push=Array.prototype.push,slice=Array.prototype.slice,indexOf=Array.prototype.indexOf;jQuery.fn=jQuery.prototype={init:function(selector,context){var match,elem,ret,doc;if(!selector){return this}if(selector.nodeType){this.context=this[0]=selector;this.length=1;return this}if(selector==="body"&&!context){this.context=document;this[0]=document.body;this.selector="body";this.length=1;return this}if(typeof selector==="string"){match=quickExpr.exec(selector);if(match&&(match[1]||!context)){if(match[1]){doc=(context?context.ownerDocument||context:document);ret=rsingleTag.exec(selector);if(ret){if(jQuery.isPlainObject(context)){selector=[document.createElement(ret[1])];jQuery.fn.attr.call(selector,context,true)}else{selector=[doc.createElement(ret[1])]}}else{ret=buildFragment([match[1]],[doc]);selector=(ret.cacheable?ret.fragment.cloneNode(true):ret.fragment).childNodes}return jQuery.merge(this,selector)}else{elem=document.getElementById(match[2]);if(elem){if(elem.id!==match[2]){return rootjQuery.find(selector)}this.length=1;this[0]=elem}this.context=document;this.selector=selector;return this}}else if(!context&&/^\w+$/.test(selector)){this.selector=selector;this.context=document;selector=document.getElementsByTagName(selector);return jQuery.merge(this,selector)}else if(!context||context.jquery){return(context||rootjQuery).find(selector)}else{return jQuery(context).find(selector)}}else if(jQuery.isFunction(selector)){return rootjQuery.ready(selector)}if(selector.selector!==undefined){this.selector=selector.selector;this.context=selector.context}return jQuery.makeArray(selector,this)},selector:"",jquery:"1.4.2",length:0,size:function(){return this.length},toArray:function(){return slice.call(this,0)},get:function(num){return num==null?this.toArray():(num<0?this.slice(num)[0]:this[num])},pushStack:function(elems,name,selector){var ret=jQuery();if(jQuery.isArray(elems)){push.apply(ret,elems)}else{jQuery.merge(ret,elems)}ret.prevObject=this;ret.context=this.context;if(name==="find"){ret.selector=this.selector+(this.selector?" ":"")+selector}else if(name){ret.selector=this.selector+"."+name+"("+selector+")"}return ret},each:function(callback,args){return jQuery.each(this,callback,args)},ready:function(fn){jQuery.bindReady();if(jQuery.isReady){fn.call(document,jQuery)}else if(readyList){readyList.push(fn)}return this},eq:function(i){return i===-1?this.slice(i):this.slice(i,+i+1)},first:function(){return this.eq(0)},last:function(){return this.eq(-1)},slice:function(){return this.pushStack(slice.apply(this,arguments),"slice",slice.call(arguments).join(","))},map:function(callback){return this.pushStack(jQuery.map(this,function(elem,i){return callback.call(elem,i,elem)}))},end:function(){return this.prevObject||jQuery(null)},push:push,sort:[].sort,splice:[].splice};jQuery.fn.init.prototype=jQuery.fn;jQuery.extend=jQuery.fn.extend=function(){var target=arguments[0]||{},i=1,length=arguments.length,deep=false,options,name,src,copy;if(typeof target==="boolean"){deep=target;target=arguments[1]||{};i=2}if(typeof target!=="object"&&!jQuery.isFunction(target)){target={}}if(length===i){target=this;--i}for(;i<length;i++){if((options=arguments[i])!=null){for(name in options){src=target[name];copy=options[name];if(target===copy){continue}if(deep&&copy&&(jQuery.isPlainObject(copy)||jQuery.isArray(copy))){var clone=src&&(jQuery.isPlainObject(src)||jQuery.isArray(src))?src:jQuery.isArray(copy)?[]:{};target[name]=jQuery.extend(deep,clone,copy)}else if(copy!==undefined){target[name]=copy}}}}return target};jQuery.extend({noConflict:function(deep){window.$=_$;if(deep){window.jQuery=_jQuery}return jQuery},isReady:false,ready:function(){if(!jQuery.isReady){if(!document.body){return setTimeout(jQuery.ready,13)}jQuery.isReady=true;if(readyList){var fn,i=0;while((fn=readyList[i++])){fn.call(document,jQuery)}readyList=null}if(jQuery.fn.triggerHandler){jQuery(document).triggerHandler("ready")}}},bindReady:function(){if(readyBound){return}readyBound=true;if(document.readyState==="complete"){return jQuery.ready()}if(document.addEventListener){document.addEventListener("DOMContentLoaded",DOMContentLoaded,false);window.addEventListener("load",jQuery.ready,false)}else if(document.attachEvent){document.attachEvent("onreadystatechange",DOMContentLoaded);window.attachEvent("onload",jQuery.ready);var toplevel=false;try{toplevel=window.frameElement==null}catch(e){}if(document.documentElement.doScroll&&toplevel){doScrollCheck()}}},isFunction:function(obj){return toString.call(obj)==="[object Function]"},isArray:function(obj){return toString.call(obj)==="[object Array]"},isPlainObject:function(obj){if(!obj||toString.call(obj)!=="[object Object]"||obj.nodeType||obj.setInterval){return false}if(obj.constructor&&!hasOwnProperty.call(obj,"constructor")&&!hasOwnProperty.call(obj.constructor.prototype,"isPrototypeOf")){return false}var key;for(key in obj){}return key===undefined||hasOwnProperty.call(obj,key)},isEmptyObject:function(obj){for(var name in obj){return false}return true},error:function(msg){throw msg;},parseJSON:function(data){if(typeof data!=="string"||!data){return null}data=jQuery.trim(data);if(/^[\],:{}\s]*$/.test(data.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,"@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,"]").replace(/(?:^|:|,)(?:\s*\[)+/g,""))){return window.JSON&&window.JSON.parse?window.JSON.parse(data):(new Function("return "+data))()}else{jQuery.error("Invalid JSON: "+data)}},noop:function(){},globalEval:function(data){if(data&&rnotwhite.test(data)){var head=document.getElementsByTagName("head")[0]||document.documentElement,script=document.createElement("script");script.type="text/javascript";if(jQuery.support.scriptEval){script.appendChild(document.createTextNode(data))}else{script.text=data}head.insertBefore(script,head.firstChild);head.removeChild(script)}},nodeName:function(elem,name){return elem.nodeName&&elem.nodeName.toUpperCase()===name.toUpperCase()},each:function(object,callback,args){var name,i=0,length=object.length,isObj=length===undefined||jQuery.isFunction(object);if(args){if(isObj){for(name in object){if(callback.apply(object[name],args)===false){break}}}else{for(;i<length;){if(callback.apply(object[i++],args)===false){break}}}}else{if(isObj){for(name in object){if(callback.call(object[name],name,object[name])===false){break}}}else{for(var value=object[0];i<length&&callback.call(value,i,value)!==false;value=object[++i]){}}}return object},trim:function(text){return(text||"").replace(rtrim,"")},makeArray:function(array,results){var ret=results||[];if(array!=null){if(array.length==null||typeof array==="string"||jQuery.isFunction(array)||(typeof array!=="function"&&array.setInterval)){push.call(ret,array)}else{jQuery.merge(ret,array)}}return ret},inArray:function(elem,array){if(array.indexOf){return array.indexOf(elem)}for(var i=0,length=array.length;i<length;i++){if(array[i]===elem){return i}}return-1},merge:function(first,second){var i=first.length,j=0;if(typeof second.length==="number"){for(var l=second.length;j<l;j++){first[i++]=second[j]}}else{while(second[j]!==undefined){first[i++]=second[j++]}}first.length=i;return first},grep:function(elems,callback,inv){var ret=[];for(var i=0,length=elems.length;i<length;i++){if(!inv!==!callback(elems[i],i)){ret.push(elems[i])}}return ret},map:function(elems,callback,arg){var ret=[],value;for(var i=0,length=elems.length;i<length;i++){value=callback(elems[i],i,arg);if(value!=null){ret[ret.length]=value}}return ret.concat.apply([],ret)},guid:1,proxy:function(fn,proxy,thisObject){if(arguments.length===2){if(typeof proxy==="string"){thisObject=fn;fn=thisObject[proxy];proxy=undefined}else if(proxy&&!jQuery.isFunction(proxy)){thisObject=proxy;proxy=undefined}}if(!proxy&&fn){proxy=function(){return fn.apply(thisObject||this,arguments)}}if(fn){proxy.guid=fn.guid=fn.guid||proxy.guid||jQuery.guid++}return proxy},uaMatch:function(ua){ua=ua.toLowerCase();var match=/(webkit)[ \/]([\w.]+)/.exec(ua)||/(opera)(?:.*version)?[ \/]([\w.]+)/.exec(ua)||/(msie) ([\w.]+)/.exec(ua)||!/compatible/.test(ua)&&/(mozilla)(?:.*? rv:([\w.]+))?/.exec(ua)||[];return{browser:match[1]||"",version:match[2]||"0"}},browser:{}});browserMatch=jQuery.uaMatch(userAgent);if(browserMatch.browser){jQuery.browser[browserMatch.browser]=true;jQuery.browser.version=browserMatch.version}if(jQuery.browser.webkit){jQuery.browser.safari=true}if(indexOf){jQuery.inArray=function(elem,array){return indexOf.call(array,elem)}}rootjQuery=jQuery(document);if(document.addEventListener){DOMContentLoaded=function(){document.removeEventListener("DOMContentLoaded",DOMContentLoaded,false);jQuery.ready()}}else if(document.attachEvent){DOMContentLoaded=function(){if(document.readyState==="complete"){document.detachEvent("onreadystatechange",DOMContentLoaded);jQuery.ready()}}}function doScrollCheck(){if(jQuery.isReady){return}try{document.documentElement.doScroll("left")}catch(error){setTimeout(doScrollCheck,1);return}jQuery.ready()}function evalScript(i,elem){if(elem.src){jQuery.ajax({url:elem.src,async:false,dataType:"script"})}else{jQuery.globalEval(elem.text||elem.textContent||elem.innerHTML||"")}if(elem.parentNode){elem.parentNode.removeChild(elem)}}function access(elems,key,value,exec,fn,pass){var length=elems.length;if(typeof key==="object"){for(var k in key){access(elems,k,key[k],exec,fn,value)}return elems}if(value!==undefined){exec=!pass&&exec&&jQuery.isFunction(value);for(var i=0;i<length;i++){fn(elems[i],key,exec?value.call(elems[i],i,fn(elems[i],key)):value,pass)}return elems}return length?fn(elems[0],key):undefined}function now(){return(new Date).getTime()}(function(){jQuery.support={};var root=document.documentElement,script=document.createElement("script"),div=document.createElement("div"),id="script"+now();div.style.display="none";div.innerHTML="   <link/><table></table><a href='/a' style='color:red;float:left;opacity:.55;'>a</a><input type='checkbox'/>";var all=div.getElementsByTagName("*"),a=div.getElementsByTagName("a")[0];if(!all||!all.length||!a){return}jQuery.support={leadingWhitespace:div.firstChild.nodeType===3,tbody:!div.getElementsByTagName("tbody").length,htmlSerialize:!!div.getElementsByTagName("link").length,style:/red/.test(a.getAttribute("style")),hrefNormalized:a.getAttribute("href")==="/a",opacity:/^0.55$/.test(a.style.opacity),cssFloat:!!a.style.cssFloat,checkOn:div.getElementsByTagName("input")[0].value==="on",optSelected:document.createElement("select").appendChild(document.createElement("option")).selected,parentNode:div.removeChild(div.appendChild(document.createElement("div"))).parentNode===null,deleteExpando:true,checkClone:false,scriptEval:false,noCloneEvent:true,boxModel:null};script.type="text/javascript";try{script.appendChild(document.createTextNode("window."+id+"=1;"))}catch(e){}root.insertBefore(script,root.firstChild);if(window[id]){jQuery.support.scriptEval=true;delete window[id]}try{delete script.test}catch(e){jQuery.support.deleteExpando=false}root.removeChild(script);if(div.attachEvent&&div.fireEvent){div.attachEvent("onclick",function click(){jQuery.support.noCloneEvent=false;div.detachEvent("onclick",click)});div.cloneNode(true).fireEvent("onclick")}div=document.createElement("div");div.innerHTML="<input type='radio' name='radiotest' checked='checked'/>";var fragment=document.createDocumentFragment();fragment.appendChild(div.firstChild);jQuery.support.checkClone=fragment.cloneNode(true).cloneNode(true).lastChild.checked;jQuery(function(){var div=document.createElement("div");div.style.width=div.style.paddingLeft="1px";document.body.appendChild(div);jQuery.boxModel=jQuery.support.boxModel=div.offsetWidth===2;document.body.removeChild(div).style.display='none';div=null});var eventSupported=function(eventName){var el=document.createElement("div");eventName="on"+eventName;var isSupported=(eventName in el);if(!isSupported){el.setAttribute(eventName,"return;");isSupported=typeof el[eventName]==="function"}el=null;return isSupported};jQuery.support.submitBubbles=eventSupported("submit");jQuery.support.changeBubbles=eventSupported("change");root=script=div=all=a=null})();jQuery.props={"for":"htmlFor","class":"className",readonly:"readOnly",maxlength:"maxLength",cellspacing:"cellSpacing",rowspan:"rowSpan",colspan:"colSpan",tabindex:"tabIndex",usemap:"useMap",frameborder:"frameBorder"};var expando="jQuery"+now(),uuid=0,windowData={};jQuery.extend({cache:{},expando:expando,noData:{"embed":true,"object":true,"applet":true},data:function(elem,name,data){if(elem.nodeName&&jQuery.noData[elem.nodeName.toLowerCase()]){return}elem=elem==window?windowData:elem;var id=elem[expando],cache=jQuery.cache,thisCache;if(!id&&typeof name==="string"&&data===undefined){return null}if(!id){id=++uuid}if(typeof name==="object"){elem[expando]=id;thisCache=cache[id]=jQuery.extend(true,{},name)}else if(!cache[id]){elem[expando]=id;cache[id]={}}thisCache=cache[id];if(data!==undefined){thisCache[name]=data}return typeof name==="string"?thisCache[name]:thisCache},removeData:function(elem,name){if(elem.nodeName&&jQuery.noData[elem.nodeName.toLowerCase()]){return}elem=elem==window?windowData:elem;var id=elem[expando],cache=jQuery.cache,thisCache=cache[id];if(name){if(thisCache){delete thisCache[name];if(jQuery.isEmptyObject(thisCache)){jQuery.removeData(elem)}}}else{if(jQuery.support.deleteExpando){delete elem[jQuery.expando]}else if(elem.removeAttribute){elem.removeAttribute(jQuery.expando)}delete cache[id]}}});jQuery.fn.extend({data:function(key,value){if(typeof key==="undefined"&&this.length){return jQuery.data(this[0])}else if(typeof key==="object"){return this.each(function(){jQuery.data(this,key)})}var parts=key.split(".");parts[1]=parts[1]?"."+parts[1]:"";if(value===undefined){var data=this.triggerHandler("getData"+parts[1]+"!",[parts[0]]);if(data===undefined&&this.length){data=jQuery.data(this[0],key)}return data===undefined&&parts[1]?this.data(parts[0]):data}else{return this.trigger("setData"+parts[1]+"!",[parts[0],value]).each(function(){jQuery.data(this,key,value)})}},removeData:function(key){return this.each(function(){jQuery.removeData(this,key)})}});jQuery.extend({queue:function(elem,type,data){if(!elem){return}type=(type||"fx")+"queue";var q=jQuery.data(elem,type);if(!data){return q||[]}if(!q||jQuery.isArray(data)){q=jQuery.data(elem,type,jQuery.makeArray(data))}else{q.push(data)}return q},dequeue:function(elem,type){type=type||"fx";var queue=jQuery.queue(elem,type),fn=queue.shift();if(fn==="inprogress"){fn=queue.shift()}if(fn){if(type==="fx"){queue.unshift("inprogress")}fn.call(elem,function(){jQuery.dequeue(elem,type)})}}});jQuery.fn.extend({queue:function(type,data){if(typeof type!=="string"){data=type;type="fx"}if(data===undefined){return jQuery.queue(this[0],type)}return this.each(function(i,elem){var queue=jQuery.queue(this,type,data);if(type==="fx"&&queue[0]!=="inprogress"){jQuery.dequeue(this,type)}})},dequeue:function(type){return this.each(function(){jQuery.dequeue(this,type)})},delay:function(time,type){time=jQuery.fx?jQuery.fx.speeds[time]||time:time;type=type||"fx";return this.queue(type,function(){var elem=this;setTimeout(function(){jQuery.dequeue(elem,type)},time)})},clearQueue:function(type){return this.queue(type||"fx",[])}});var rclass=/[\n\t]/g,rspace=/\s+/,rreturn=/\r/g,rspecialurl=/href|src|style/,rtype=/(button|input)/i,rfocusable=/(button|input|object|select|textarea)/i,rclickable=/^(a|area)$/i,rradiocheck=/radio|checkbox/;jQuery.fn.extend({attr:function(name,value){return access(this,name,value,true,jQuery.attr)},removeAttr:function(name,fn){return this.each(function(){jQuery.attr(this,name,"");if(this.nodeType===1){this.removeAttribute(name)}})},addClass:function(value){if(jQuery.isFunction(value)){return this.each(function(i){var self=jQuery(this);self.addClass(value.call(this,i,self.attr("class")))})}if(value&&typeof value==="string"){var classNames=(value||"").split(rspace);for(var i=0,l=this.length;i<l;i++){var elem=this[i];if(elem.nodeType===1){if(!elem.className){elem.className=value}else{var className=" "+elem.className+" ",setClass=elem.className;for(var c=0,cl=classNames.length;c<cl;c++){if(className.indexOf(" "+classNames[c]+" ")<0){setClass+=" "+classNames[c]}}elem.className=jQuery.trim(setClass)}}}}return this},removeClass:function(value){if(jQuery.isFunction(value)){return this.each(function(i){var self=jQuery(this);self.removeClass(value.call(this,i,self.attr("class")))})}if((value&&typeof value==="string")||value===undefined){var classNames=(value||"").split(rspace);for(var i=0,l=this.length;i<l;i++){var elem=this[i];if(elem.nodeType===1&&elem.className){if(value){var className=(" "+elem.className+" ").replace(rclass," ");for(var c=0,cl=classNames.length;c<cl;c++){className=className.replace(" "+classNames[c]+" "," ")}elem.className=jQuery.trim(className)}else{elem.className=""}}}}return this},toggleClass:function(value,stateVal){var type=typeof value,isBool=typeof stateVal==="boolean";if(jQuery.isFunction(value)){return this.each(function(i){var self=jQuery(this);self.toggleClass(value.call(this,i,self.attr("class"),stateVal),stateVal)})}return this.each(function(){if(type==="string"){var className,i=0,self=jQuery(this),state=stateVal,classNames=value.split(rspace);while((className=classNames[i++])){state=isBool?state:!self.hasClass(className);self[state?"addClass":"removeClass"](className)}}else if(type==="undefined"||type==="boolean"){if(this.className){jQuery.data(this,"__className__",this.className)}this.className=this.className||value===false?"":jQuery.data(this,"__className__")||""}})},hasClass:function(selector){var className=" "+selector+" ";for(var i=0,l=this.length;i<l;i++){if((" "+this[i].className+" ").replace(rclass," ").indexOf(className)>-1){return true}}return false},val:function(value){if(value===undefined){var elem=this[0];if(elem){if(jQuery.nodeName(elem,"option")){return(elem.attributes.value||{}).specified?elem.value:elem.text}if(jQuery.nodeName(elem,"select")){var index=elem.selectedIndex,values=[],options=elem.options,one=elem.type==="select-one";if(index<0){return null}for(var i=one?index:0,max=one?index+1:options.length;i<max;i++){var option=options[i];if(option.selected){value=jQuery(option).val();if(one){return value}values.push(value)}}return values}if(rradiocheck.test(elem.type)&&!jQuery.support.checkOn){return elem.getAttribute("value")===null?"on":elem.value}return(elem.value||"").replace(rreturn,"")}return undefined}var isFunction=jQuery.isFunction(value);return this.each(function(i){var self=jQuery(this),val=value;if(this.nodeType!==1){return}if(isFunction){val=value.call(this,i,self.val())}if(typeof val==="number"){val+=""}if(jQuery.isArray(val)&&rradiocheck.test(this.type)){this.checked=jQuery.inArray(self.val(),val)>=0}else if(jQuery.nodeName(this,"select")){var values=jQuery.makeArray(val);jQuery("option",this).each(function(){this.selected=jQuery.inArray(jQuery(this).val(),values)>=0});if(!values.length){this.selectedIndex=-1}}else{this.value=val}})}});jQuery.extend({attrFn:{val:true,css:true,html:true,text:true,data:true,width:true,height:true,offset:true},attr:function(elem,name,value,pass){if(!elem||elem.nodeType===3||elem.nodeType===8){return undefined}if(pass&&name in jQuery.attrFn){return jQuery(elem)[name](value)}var notxml=elem.nodeType!==1||!jQuery.isXMLDoc(elem),set=value!==undefined;name=notxml&&jQuery.props[name]||name;if(elem.nodeType===1){var special=rspecialurl.test(name);if(name==="selected"&&!jQuery.support.optSelected){var parent=elem.parentNode;if(parent){parent.selectedIndex;if(parent.parentNode){parent.parentNode.selectedIndex}}}if(name in elem&&notxml&&!special){if(set){if(name==="type"&&rtype.test(elem.nodeName)&&elem.parentNode){jQuery.error("type property can't be changed")}elem[name]=value}if(jQuery.nodeName(elem,"form")&&elem.getAttributeNode(name)){return elem.getAttributeNode(name).nodeValue}if(name==="tabIndex"){var attributeNode=elem.getAttributeNode("tabIndex");return attributeNode&&attributeNode.specified?attributeNode.value:rfocusable.test(elem.nodeName)||rclickable.test(elem.nodeName)&&elem.href?0:undefined}return elem[name]}if(!jQuery.support.style&&notxml&&name==="style"){if(set){elem.style.cssText=""+value}return elem.style.cssText}if(set){elem.setAttribute(name,""+value)}var attr=!jQuery.support.hrefNormalized&&notxml&&special?elem.getAttribute(name,2):elem.getAttribute(name);return attr===null?undefined:attr}return jQuery.style(elem,name,value)}});var rnamespaces=/\.(.*)$/,fcleanup=function(nm){return nm.replace(/[^\w\s\.\|`]/g,function(ch){return"\\"+ch})};jQuery.event={add:function(elem,types,handler,data){if(elem.nodeType===3||elem.nodeType===8){return}if(elem.setInterval&&(elem!==window&&!elem.frameElement)){elem=window}var handleObjIn,handleObj;if(handler.handler){handleObjIn=handler;handler=handleObjIn.handler}if(!handler.guid){handler.guid=jQuery.guid++}var elemData=jQuery.data(elem);if(!elemData){return}var events=elemData.events=elemData.events||{},eventHandle=elemData.handle,eventHandle;if(!eventHandle){elemData.handle=eventHandle=function(){return typeof jQuery!=="undefined"&&!jQuery.event.triggered?jQuery.event.handle.apply(eventHandle.elem,arguments):undefined}}eventHandle.elem=elem;types=types.split(" ");var type,i=0,namespaces;while((type=types[i++])){handleObj=handleObjIn?jQuery.extend({},handleObjIn):{handler:handler,data:data};if(type.indexOf(".")>-1){namespaces=type.split(".");type=namespaces.shift();handleObj.namespace=namespaces.slice(0).sort().join(".")}else{namespaces=[];handleObj.namespace=""}handleObj.type=type;handleObj.guid=handler.guid;var handlers=events[type],special=jQuery.event.special[type]||{};if(!handlers){handlers=events[type]=[];if(!special.setup||special.setup.call(elem,data,namespaces,eventHandle)===false){if(elem.addEventListener){elem.addEventListener(type,eventHandle,false)}else if(elem.attachEvent){elem.attachEvent("on"+type,eventHandle)}}}if(special.add){special.add.call(elem,handleObj);if(!handleObj.handler.guid){handleObj.handler.guid=handler.guid}}handlers.push(handleObj);jQuery.event.global[type]=true}elem=null},global:{},remove:function(elem,types,handler,pos){if(elem.nodeType===3||elem.nodeType===8){return}var ret,type,fn,i=0,all,namespaces,namespace,special,eventType,handleObj,origType,elemData=jQuery.data(elem),events=elemData&&elemData.events;if(!elemData||!events){return}if(types&&types.type){handler=types.handler;types=types.type}if(!types||typeof types==="string"&&types.charAt(0)==="."){types=types||"";for(type in events){jQuery.event.remove(elem,type+types)}return}types=types.split(" ");while((type=types[i++])){origType=type;handleObj=null;all=type.indexOf(".")<0;namespaces=[];if(!all){namespaces=type.split(".");type=namespaces.shift();namespace=new RegExp("(^|\\.)"+jQuery.map(namespaces.slice(0).sort(),fcleanup).join("\\.(?:.*\\.)?")+"(\\.|$)")}eventType=events[type];if(!eventType){continue}if(!handler){for(var j=0;j<eventType.length;j++){handleObj=eventType[j];if(all||namespace.test(handleObj.namespace)){jQuery.event.remove(elem,origType,handleObj.handler,j);eventType.splice(j--,1)}}continue}special=jQuery.event.special[type]||{};for(var j=pos||0;j<eventType.length;j++){handleObj=eventType[j];if(handler.guid===handleObj.guid){if(all||namespace.test(handleObj.namespace)){if(pos==null){eventType.splice(j--,1)}if(special.remove){special.remove.call(elem,handleObj)}}if(pos!=null){break}}}if(eventType.length===0||pos!=null&&eventType.length===1){if(!special.teardown||special.teardown.call(elem,namespaces)===false){removeEvent(elem,type,elemData.handle)}ret=null;delete events[type]}}if(jQuery.isEmptyObject(events)){var handle=elemData.handle;if(handle){handle.elem=null}delete elemData.events;delete elemData.handle;if(jQuery.isEmptyObject(elemData)){jQuery.removeData(elem)}}},trigger:function(event,data,elem){var type=event.type||event,bubbling=arguments[3];if(!bubbling){event=typeof event==="object"?event[expando]?event:jQuery.extend(jQuery.Event(type),event):jQuery.Event(type);if(type.indexOf("!")>=0){event.type=type=type.slice(0,-1);event.exclusive=true}if(!elem){event.stopPropagation();if(jQuery.event.global[type]){jQuery.each(jQuery.cache,function(){if(this.events&&this.events[type]){jQuery.event.trigger(event,data,this.handle.elem)}})}}if(!elem||elem.nodeType===3||elem.nodeType===8){return undefined}event.result=undefined;event.target=elem;data=jQuery.makeArray(data);data.unshift(event)}event.currentTarget=elem;var handle=jQuery.data(elem,"handle");if(handle){handle.apply(elem,data)}var parent=elem.parentNode||elem.ownerDocument;try{if(!(elem&&elem.nodeName&&jQuery.noData[elem.nodeName.toLowerCase()])){if(elem["on"+type]&&elem["on"+type].apply(elem,data)===false){event.result=false}}}catch(e){}if(!event.isPropagationStopped()&&parent){jQuery.event.trigger(event,data,parent,true)}else if(!event.isDefaultPrevented()){var target=event.target,old,isClick=jQuery.nodeName(target,"a")&&type==="click",special=jQuery.event.special[type]||{};if((!special._default||special._default.call(elem,event)===false)&&!isClick&&!(target&&target.nodeName&&jQuery.noData[target.nodeName.toLowerCase()])){try{if(target[type]){old=target["on"+type];if(old){target["on"+type]=null}jQuery.event.triggered=true;target[type]()}}catch(e){}if(old){target["on"+type]=old}jQuery.event.triggered=false}}},handle:function(event){var all,handlers,namespaces,namespace,events;event=arguments[0]=jQuery.event.fix(event||window.event);event.currentTarget=this;all=event.type.indexOf(".")<0&&!event.exclusive;if(!all){namespaces=event.type.split(".");event.type=namespaces.shift();namespace=new RegExp("(^|\\.)"+namespaces.slice(0).sort().join("\\.(?:.*\\.)?")+"(\\.|$)")}var events=jQuery.data(this,"events")||{},handlers=events[event.type];if(events&&handlers){handlers=handlers.slice(0);for(var j=0,l=handlers.length;j<l;j++){var handleObj=handlers[j];if(all||namespace.test(handleObj.namespace)){event.handler=handleObj.handler;event.data=handleObj.data;event.handleObj=handleObj;var ret=handleObj.handler.apply(this,arguments);if(ret!==undefined){event.result=ret;if(ret===false){event.preventDefault();event.stopPropagation()}}if(event.isImmediatePropagationStopped()){break}}}}return event.result},props:"altKey attrChange attrName bubbles button cancelable charCode clientX clientY ctrlKey currentTarget data detail eventPhase fromElement handler keyCode layerX layerY metaKey newValue offsetX offsetY originalTarget pageX pageY prevValue relatedNode relatedTarget screenX screenY shiftKey srcElement target toElement view wheelDelta which".split(" "),fix:function(event){if(event[expando]){return event}var originalEvent=event;event=jQuery.Event(originalEvent);for(var i=this.props.length,prop;i;){prop=this.props[--i];event[prop]=originalEvent[prop]}if(!event.target){event.target=event.srcElement||document}if(event.target.nodeType===3){event.target=event.target.parentNode}if(!event.relatedTarget&&event.fromElement){event.relatedTarget=event.fromElement===event.target?event.toElement:event.fromElement}if(event.pageX==null&&event.clientX!=null){var doc=document.documentElement,body=document.body;event.pageX=event.clientX+(doc&&doc.scrollLeft||body&&body.scrollLeft||0)-(doc&&doc.clientLeft||body&&body.clientLeft||0);event.pageY=event.clientY+(doc&&doc.scrollTop||body&&body.scrollTop||0)-(doc&&doc.clientTop||body&&body.clientTop||0)}if(!event.which&&((event.charCode||event.charCode===0)?event.charCode:event.keyCode)){event.which=event.charCode||event.keyCode}if(!event.metaKey&&event.ctrlKey){event.metaKey=event.ctrlKey}if(!event.which&&event.button!==undefined){event.which=(event.button&1?1:(event.button&2?3:(event.button&4?2:0)))}return event},guid:1E8,proxy:jQuery.proxy,special:{ready:{setup:jQuery.bindReady,teardown:jQuery.noop},live:{add:function(handleObj){jQuery.event.add(this,handleObj.origType,jQuery.extend({},handleObj,{handler:liveHandler}))},remove:function(handleObj){var remove=true,type=handleObj.origType.replace(rnamespaces,"");jQuery.each(jQuery.data(this,"events").live||[],function(){if(type===this.origType.replace(rnamespaces,"")){remove=false;return false}});if(remove){jQuery.event.remove(this,handleObj.origType,liveHandler)}}},beforeunload:{setup:function(data,namespaces,eventHandle){if(this.setInterval){this.onbeforeunload=eventHandle}return false},teardown:function(namespaces,eventHandle){if(this.onbeforeunload===eventHandle){this.onbeforeunload=null}}}}};var removeEvent=document.removeEventListener?function(elem,type,handle){elem.removeEventListener(type,handle,false)}:function(elem,type,handle){elem.detachEvent("on"+type,handle)};jQuery.Event=function(src){if(!this.preventDefault){return new jQuery.Event(src)}if(src&&src.type){this.originalEvent=src;this.type=src.type}else{this.type=src}this.timeStamp=now();this[expando]=true};function returnFalse(){return false}function returnTrue(){return true}jQuery.Event.prototype={preventDefault:function(){this.isDefaultPrevented=returnTrue;var e=this.originalEvent;if(!e){return}if(e.preventDefault){e.preventDefault()}e.returnValue=false},stopPropagation:function(){this.isPropagationStopped=returnTrue;var e=this.originalEvent;if(!e){return}if(e.stopPropagation){e.stopPropagation()}e.cancelBubble=true},stopImmediatePropagation:function(){this.isImmediatePropagationStopped=returnTrue;this.stopPropagation()},isDefaultPrevented:returnFalse,isPropagationStopped:returnFalse,isImmediatePropagationStopped:returnFalse};var withinElement=function(event){var parent=event.relatedTarget;try{while(parent&&parent!==this){parent=parent.parentNode}if(parent!==this){event.type=event.data;jQuery.event.handle.apply(this,arguments)}}catch(e){}},delegate=function(event){event.type=event.data;jQuery.event.handle.apply(this,arguments)};jQuery.each({mouseenter:"mouseover",mouseleave:"mouseout"},function(orig,fix){jQuery.event.special[orig]={setup:function(data){jQuery.event.add(this,fix,data&&data.selector?delegate:withinElement,orig)},teardown:function(data){jQuery.event.remove(this,fix,data&&data.selector?delegate:withinElement)}}});if(!jQuery.support.submitBubbles){jQuery.event.special.submit={setup:function(data,namespaces){if(this.nodeName.toLowerCase()!=="form"){jQuery.event.add(this,"click.specialSubmit",function(e){var elem=e.target,type=elem.type;if((type==="submit"||type==="image")&&jQuery(elem).closest("form").length){return trigger("submit",this,arguments)}});jQuery.event.add(this,"keypress.specialSubmit",function(e){var elem=e.target,type=elem.type;if((type==="text"||type==="password")&&jQuery(elem).closest("form").length&&e.keyCode===13){return trigger("submit",this,arguments)}})}else{return false}},teardown:function(namespaces){jQuery.event.remove(this,".specialSubmit")}}}if(!jQuery.support.changeBubbles){var formElems=/textarea|input|select/i,changeFilters,getVal=function(elem){var type=elem.type,val=elem.value;if(type==="radio"||type==="checkbox"){val=elem.checked}else if(type==="select-multiple"){val=elem.selectedIndex>-1?jQuery.map(elem.options,function(elem){return elem.selected}).join("-"):""}else if(elem.nodeName.toLowerCase()==="select"){val=elem.selectedIndex}return val},testChange=function testChange(e){var elem=e.target,data,val;if(!formElems.test(elem.nodeName)||elem.readOnly){return}data=jQuery.data(elem,"_change_data");val=getVal(elem);if(e.type!=="focusout"||elem.type!=="radio"){jQuery.data(elem,"_change_data",val)}if(data===undefined||val===data){return}if(data!=null||val){e.type="change";return jQuery.event.trigger(e,arguments[1],elem)}};jQuery.event.special.change={filters:{focusout:testChange,click:function(e){var elem=e.target,type=elem.type;if(type==="radio"||type==="checkbox"||elem.nodeName.toLowerCase()==="select"){return testChange.call(this,e)}},keydown:function(e){var elem=e.target,type=elem.type;if((e.keyCode===13&&elem.nodeName.toLowerCase()!=="textarea")||(e.keyCode===32&&(type==="checkbox"||type==="radio"))||type==="select-multiple"){return testChange.call(this,e)}},beforeactivate:function(e){var elem=e.target;jQuery.data(elem,"_change_data",getVal(elem))}},setup:function(data,namespaces){if(this.type==="file"){return false}for(var type in changeFilters){jQuery.event.add(this,type+".specialChange",changeFilters[type])}return formElems.test(this.nodeName)},teardown:function(namespaces){jQuery.event.remove(this,".specialChange");return formElems.test(this.nodeName)}};changeFilters=jQuery.event.special.change.filters}function trigger(type,elem,args){args[0].type=type;return jQuery.event.handle.apply(elem,args)}if(document.addEventListener){jQuery.each({focus:"focusin",blur:"focusout"},function(orig,fix){jQuery.event.special[fix]={setup:function(){this.addEventListener(orig,handler,true)},teardown:function(){this.removeEventListener(orig,handler,true)}};function handler(e){e=jQuery.event.fix(e);e.type=fix;return jQuery.event.handle.call(this,e)}})}jQuery.each(["bind","one"],function(i,name){jQuery.fn[name]=function(type,data,fn){if(typeof type==="object"){for(var key in type){this[name](key,data,type[key],fn)}return this}if(jQuery.isFunction(data)){fn=data;data=undefined}var handler=name==="one"?jQuery.proxy(fn,function(event){jQuery(this).unbind(event,handler);return fn.apply(this,arguments)}):fn;if(type==="unload"&&name!=="one"){this.one(type,data,fn)}else{for(var i=0,l=this.length;i<l;i++){jQuery.event.add(this[i],type,handler,data)}}return this}});jQuery.fn.extend({unbind:function(type,fn){if(typeof type==="object"&&!type.preventDefault){for(var key in type){this.unbind(key,type[key])}}else{for(var i=0,l=this.length;i<l;i++){jQuery.event.remove(this[i],type,fn)}}return this},delegate:function(selector,types,data,fn){return this.live(types,data,fn,selector)},undelegate:function(selector,types,fn){if(arguments.length===0){return this.unbind("live")}else{return this.die(types,null,fn,selector)}},trigger:function(type,data){return this.each(function(){jQuery.event.trigger(type,data,this)})},triggerHandler:function(type,data){if(this[0]){var event=jQuery.Event(type);event.preventDefault();event.stopPropagation();jQuery.event.trigger(event,data,this[0]);return event.result}},toggle:function(fn){var args=arguments,i=1;while(i<args.length){jQuery.proxy(fn,args[i++])}return this.click(jQuery.proxy(fn,function(event){var lastToggle=(jQuery.data(this,"lastToggle"+fn.guid)||0)%i;jQuery.data(this,"lastToggle"+fn.guid,lastToggle+1);event.preventDefault();return args[lastToggle].apply(this,arguments)||false}))},hover:function(fnOver,fnOut){return this.mouseenter(fnOver).mouseleave(fnOut||fnOver)}});var liveMap={focus:"focusin",blur:"focusout",mouseenter:"mouseover",mouseleave:"mouseout"};jQuery.each(["live","die"],function(i,name){jQuery.fn[name]=function(types,data,fn,origSelector){var type,i=0,match,namespaces,preType,selector=origSelector||this.selector,context=origSelector?this:jQuery(this.context);if(jQuery.isFunction(data)){fn=data;data=undefined}types=(types||"").split(" ");while((type=types[i++])!=null){match=rnamespaces.exec(type);namespaces="";if(match){namespaces=match[0];type=type.replace(rnamespaces,"")}if(type==="hover"){types.push("mouseenter"+namespaces,"mouseleave"+namespaces);continue}preType=type;if(type==="focus"||type==="blur"){types.push(liveMap[type]+namespaces);type=type+namespaces}else{type=(liveMap[type]||type)+namespaces}if(name==="live"){context.each(function(){jQuery.event.add(this,liveConvert(type,selector),{data:data,selector:selector,handler:fn,origType:type,origHandler:fn,preType:preType})})}else{context.unbind(liveConvert(type,selector),fn)}}return this}});function liveHandler(event){var stop,elems=[],selectors=[],args=arguments,related,match,handleObj,elem,j,i,l,data,events=jQuery.data(this,"events");if(event.liveFired===this||!events||!events.live||event.button&&event.type==="click"){return}event.liveFired=this;var live=events.live.slice(0);for(j=0;j<live.length;j++){handleObj=live[j];if(handleObj.origType.replace(rnamespaces,"")===event.type){selectors.push(handleObj.selector)}else{live.splice(j--,1)}}match=jQuery(event.target).closest(selectors,event.currentTarget);for(i=0,l=match.length;i<l;i++){for(j=0;j<live.length;j++){handleObj=live[j];if(match[i].selector===handleObj.selector){elem=match[i].elem;related=null;if(handleObj.preType==="mouseenter"||handleObj.preType==="mouseleave"){related=jQuery(event.relatedTarget).closest(handleObj.selector)[0]}if(!related||related!==elem){elems.push({elem:elem,handleObj:handleObj})}}}}for(i=0,l=elems.length;i<l;i++){match=elems[i];event.currentTarget=match.elem;event.data=match.handleObj.data;event.handleObj=match.handleObj;if(match.handleObj.origHandler.apply(match.elem,args)===false){stop=false;break}}return stop}function liveConvert(type,selector){return"live."+(type&&type!=="*"?type+".":"")+selector.replace(/\./g,"`").replace(/ /g,"&")}jQuery.each(("blur focus focusin focusout load resize scroll unload click dblclick "+"mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave "+"change select submit keydown keypress keyup error").split(" "),function(i,name){jQuery.fn[name]=function(fn){return fn?this.bind(name,fn):this.trigger(name)};if(jQuery.attrFn){jQuery.attrFn[name]=true}});if(window.attachEvent&&!window.addEventListener){window.attachEvent("onunload",function(){for(var id in jQuery.cache){if(jQuery.cache[id].handle){try{jQuery.event.remove(jQuery.cache[id].handle.elem)}catch(e){}}}})}(function(){var chunker=/((?:\((?:\([^()]+\)|[^()]+)+\)|\[(?:\[[^[\]]*\]|['"][^'"]*['"]|[^[\]'"]+)+\]|\\.|[^ >+~,(\[\\]+)+|[>+~])(\s*,\s*)?((?:.|\r|\n)*)/g,done=0,toString=Object.prototype.toString,hasDuplicate=false,baseHasDuplicate=true;[0,0].sort(function(){baseHasDuplicate=false;return 0});var Sizzle=function(selector,context,results,seed){results=results||[];var origContext=context=context||document;if(context.nodeType!==1&&context.nodeType!==9){return[]}if(!selector||typeof selector!=="string"){return results}var parts=[],m,set,checkSet,extra,prune=true,contextXML=isXML(context),soFar=selector;while((chunker.exec(""),m=chunker.exec(soFar))!==null){soFar=m[3];parts.push(m[1]);if(m[2]){extra=m[3];break}}if(parts.length>1&&origPOS.exec(selector)){if(parts.length===2&&Expr.relative[parts[0]]){set=posProcess(parts[0]+parts[1],context)}else{set=Expr.relative[parts[0]]?[context]:Sizzle(parts.shift(),context);while(parts.length){selector=parts.shift();if(Expr.relative[selector]){selector+=parts.shift()}set=posProcess(selector,set)}}}else{if(!seed&&parts.length>1&&context.nodeType===9&&!contextXML&&Expr.match.ID.test(parts[0])&&!Expr.match.ID.test(parts[parts.length-1])){var ret=Sizzle.find(parts.shift(),context,contextXML);context=ret.expr?Sizzle.filter(ret.expr,ret.set)[0]:ret.set[0]}if(context){var ret=seed?{expr:parts.pop(),set:makeArray(seed)}:Sizzle.find(parts.pop(),parts.length===1&&(parts[0]==="~"||parts[0]==="+")&&context.parentNode?context.parentNode:context,contextXML);set=ret.expr?Sizzle.filter(ret.expr,ret.set):ret.set;if(parts.length>0){checkSet=makeArray(set)}else{prune=false}while(parts.length){var cur=parts.pop(),pop=cur;if(!Expr.relative[cur]){cur=""}else{pop=parts.pop()}if(pop==null){pop=context}Expr.relative[cur](checkSet,pop,contextXML)}}else{checkSet=parts=[]}}if(!checkSet){checkSet=set}if(!checkSet){Sizzle.error(cur||selector)}if(toString.call(checkSet)==="[object Array]"){if(!prune){results.push.apply(results,checkSet)}else if(context&&context.nodeType===1){for(var i=0;checkSet[i]!=null;i++){if(checkSet[i]&&(checkSet[i]===true||checkSet[i].nodeType===1&&contains(context,checkSet[i]))){results.push(set[i])}}}else{for(var i=0;checkSet[i]!=null;i++){if(checkSet[i]&&checkSet[i].nodeType===1){results.push(set[i])}}}}else{makeArray(checkSet,results)}if(extra){Sizzle(extra,origContext,results,seed);Sizzle.uniqueSort(results)}return results};Sizzle.uniqueSort=function(results){if(sortOrder){hasDuplicate=baseHasDuplicate;results.sort(sortOrder);if(hasDuplicate){for(var i=1;i<results.length;i++){if(results[i]===results[i-1]){results.splice(i--,1)}}}}return results};Sizzle.matches=function(expr,set){return Sizzle(expr,null,null,set)};Sizzle.find=function(expr,context,isXML){var set,match;if(!expr){return[]}for(var i=0,l=Expr.order.length;i<l;i++){var type=Expr.order[i],match;if((match=Expr.leftMatch[type].exec(expr))){var left=match[1];match.splice(1,1);if(left.substr(left.length-1)!=="\\"){match[1]=(match[1]||"").replace(/\\/g,"");set=Expr.find[type](match,context,isXML);if(set!=null){expr=expr.replace(Expr.match[type],"");break}}}}if(!set){set=context.getElementsByTagName("*")}return{set:set,expr:expr}};Sizzle.filter=function(expr,set,inplace,not){var old=expr,result=[],curLoop=set,match,anyFound,isXMLFilter=set&&set[0]&&isXML(set[0]);while(expr&&set.length){for(var type in Expr.filter){if((match=Expr.leftMatch[type].exec(expr))!=null&&match[2]){var filter=Expr.filter[type],found,item,left=match[1];anyFound=false;match.splice(1,1);if(left.substr(left.length-1)==="\\"){continue}if(curLoop===result){result=[]}if(Expr.preFilter[type]){match=Expr.preFilter[type](match,curLoop,inplace,result,not,isXMLFilter);if(!match){anyFound=found=true}else if(match===true){continue}}if(match){for(var i=0;(item=curLoop[i])!=null;i++){if(item){found=filter(item,match,i,curLoop);var pass=not^!!found;if(inplace&&found!=null){if(pass){anyFound=true}else{curLoop[i]=false}}else if(pass){result.push(item);anyFound=true}}}}if(found!==undefined){if(!inplace){curLoop=result}expr=expr.replace(Expr.match[type],"");if(!anyFound){return[]}break}}}if(expr===old){if(anyFound==null){Sizzle.error(expr)}else{break}}old=expr}return curLoop};Sizzle.error=function(msg){throw"Syntax error, unrecognized expression: "+msg;};var Expr=Sizzle.selectors={order:["ID","NAME","TAG"],match:{ID:/#((?:[\w\u00c0-\uFFFF-]|\\.)+)/,CLASS:/\.((?:[\w\u00c0-\uFFFF-]|\\.)+)/,NAME:/\[name=['"]*((?:[\w\u00c0-\uFFFF-]|\\.)+)['"]*\]/,ATTR:/\[\s*((?:[\w\u00c0-\uFFFF-]|\\.)+)\s*(?:(\S?=)\s*(['"]*)(.*?)\3|)\s*\]/,TAG:/^((?:[\w\u00c0-\uFFFF\*-]|\\.)+)/,CHILD:/:(only|nth|last|first)-child(?:\((even|odd|[\dn+-]*)\))?/,POS:/:(nth|eq|gt|lt|first|last|even|odd)(?:\((\d*)\))?(?=[^-]|$)/,PSEUDO:/:((?:[\w\u00c0-\uFFFF-]|\\.)+)(?:\((['"]?)((?:\([^\)]+\)|[^\(\)]*)+)\2\))?/},leftMatch:{},attrMap:{"class":"className","for":"htmlFor"},attrHandle:{href:function(elem){return elem.getAttribute("href")}},relative:{"+":function(checkSet,part){var isPartStr=typeof part==="string",isTag=isPartStr&&!/\W/.test(part),isPartStrNotTag=isPartStr&&!isTag;if(isTag){part=part.toLowerCase()}for(var i=0,l=checkSet.length,elem;i<l;i++){if((elem=checkSet[i])){while((elem=elem.previousSibling)&&elem.nodeType!==1){}checkSet[i]=isPartStrNotTag||elem&&elem.nodeName.toLowerCase()===part?elem||false:elem===part}}if(isPartStrNotTag){Sizzle.filter(part,checkSet,true)}},">":function(checkSet,part){var isPartStr=typeof part==="string";if(isPartStr&&!/\W/.test(part)){part=part.toLowerCase();for(var i=0,l=checkSet.length;i<l;i++){var elem=checkSet[i];if(elem){var parent=elem.parentNode;checkSet[i]=parent.nodeName.toLowerCase()===part?parent:false}}}else{for(var i=0,l=checkSet.length;i<l;i++){var elem=checkSet[i];if(elem){checkSet[i]=isPartStr?elem.parentNode:elem.parentNode===part}}if(isPartStr){Sizzle.filter(part,checkSet,true)}}},"":function(checkSet,part,isXML){var doneName=done++,checkFn=dirCheck;if(typeof part==="string"&&!/\W/.test(part)){var nodeCheck=part=part.toLowerCase();checkFn=dirNodeCheck}checkFn("parentNode",part,doneName,checkSet,nodeCheck,isXML)},"~":function(checkSet,part,isXML){var doneName=done++,checkFn=dirCheck;if(typeof part==="string"&&!/\W/.test(part)){var nodeCheck=part=part.toLowerCase();checkFn=dirNodeCheck}checkFn("previousSibling",part,doneName,checkSet,nodeCheck,isXML)}},find:{ID:function(match,context,isXML){if(typeof context.getElementById!=="undefined"&&!isXML){var m=context.getElementById(match[1]);return m?[m]:[]}},NAME:function(match,context){if(typeof context.getElementsByName!=="undefined"){var ret=[],results=context.getElementsByName(match[1]);for(var i=0,l=results.length;i<l;i++){if(results[i].getAttribute("name")===match[1]){ret.push(results[i])}}return ret.length===0?null:ret}},TAG:function(match,context){return context.getElementsByTagName(match[1])}},preFilter:{CLASS:function(match,curLoop,inplace,result,not,isXML){match=" "+match[1].replace(/\\/g,"")+" ";if(isXML){return match}for(var i=0,elem;(elem=curLoop[i])!=null;i++){if(elem){if(not^(elem.className&&(" "+elem.className+" ").replace(/[\t\n]/g," ").indexOf(match)>=0)){if(!inplace){result.push(elem)}}else if(inplace){curLoop[i]=false}}}return false},ID:function(match){return match[1].replace(/\\/g,"")},TAG:function(match,curLoop){return match[1].toLowerCase()},CHILD:function(match){if(match[1]==="nth"){var test=/(-?)(\d*)n((?:\+|-)?\d*)/.exec(match[2]==="even"&&"2n"||match[2]==="odd"&&"2n+1"||!/\D/.test(match[2])&&"0n+"+match[2]||match[2]);match[2]=(test[1]+(test[2]||1))-0;match[3]=test[3]-0}match[0]=done++;return match},ATTR:function(match,curLoop,inplace,result,not,isXML){var name=match[1].replace(/\\/g,"");if(!isXML&&Expr.attrMap[name]){match[1]=Expr.attrMap[name]}if(match[2]==="~="){match[4]=" "+match[4]+" "}return match},PSEUDO:function(match,curLoop,inplace,result,not){if(match[1]==="not"){if((chunker.exec(match[3])||"").length>1||/^\w/.test(match[3])){match[3]=Sizzle(match[3],null,null,curLoop)}else{var ret=Sizzle.filter(match[3],curLoop,inplace,true^not);if(!inplace){result.push.apply(result,ret)}return false}}else if(Expr.match.POS.test(match[0])||Expr.match.CHILD.test(match[0])){return true}return match},POS:function(match){match.unshift(true);return match}},filters:{enabled:function(elem){return elem.disabled===false&&elem.type!=="hidden"},disabled:function(elem){return elem.disabled===true},checked:function(elem){return elem.checked===true},selected:function(elem){elem.parentNode.selectedIndex;return elem.selected===true},parent:function(elem){return!!elem.firstChild},empty:function(elem){return!elem.firstChild},has:function(elem,i,match){return!!Sizzle(match[3],elem).length},header:function(elem){return/h\d/i.test(elem.nodeName)},text:function(elem){return"text"===elem.type},radio:function(elem){return"radio"===elem.type},checkbox:function(elem){return"checkbox"===elem.type},file:function(elem){return"file"===elem.type},password:function(elem){return"password"===elem.type},submit:function(elem){return"submit"===elem.type},image:function(elem){return"image"===elem.type},reset:function(elem){return"reset"===elem.type},button:function(elem){return"button"===elem.type||elem.nodeName.toLowerCase()==="button"},input:function(elem){return/input|select|textarea|button/i.test(elem.nodeName)}},setFilters:{first:function(elem,i){return i===0},last:function(elem,i,match,array){return i===array.length-1},even:function(elem,i){return i%2===0},odd:function(elem,i){return i%2===1},lt:function(elem,i,match){return i<match[3]-0},gt:function(elem,i,match){return i>match[3]-0},nth:function(elem,i,match){return match[3]-0===i},eq:function(elem,i,match){return match[3]-0===i}},filter:{PSEUDO:function(elem,match,i,array){var name=match[1],filter=Expr.filters[name];if(filter){return filter(elem,i,match,array)}else if(name==="contains"){return(elem.textContent||elem.innerText||getText([elem])||"").indexOf(match[3])>=0}else if(name==="not"){var not=match[3];for(var i=0,l=not.length;i<l;i++){if(not[i]===elem){return false}}return true}else{Sizzle.error("Syntax error, unrecognized expression: "+name)}},CHILD:function(elem,match){var type=match[1],node=elem;switch(type){case'only':case'first':while((node=node.previousSibling)){if(node.nodeType===1){return false}}if(type==="first"){return true}node=elem;case'last':while((node=node.nextSibling)){if(node.nodeType===1){return false}}return true;case'nth':var first=match[2],last=match[3];if(first===1&&last===0){return true}var doneName=match[0],parent=elem.parentNode;if(parent&&(parent.sizcache!==doneName||!elem.nodeIndex)){var count=0;for(node=parent.firstChild;node;node=node.nextSibling){if(node.nodeType===1){node.nodeIndex=++count}}parent.sizcache=doneName}var diff=elem.nodeIndex-last;if(first===0){return diff===0}else{return(diff%first===0&&diff/first>=0)}}},ID:function(elem,match){return elem.nodeType===1&&elem.getAttribute("id")===match},TAG:function(elem,match){return(match==="*"&&elem.nodeType===1)||elem.nodeName.toLowerCase()===match},CLASS:function(elem,match){return(" "+(elem.className||elem.getAttribute("class"))+" ").indexOf(match)>-1},ATTR:function(elem,match){var name=match[1],result=Expr.attrHandle[name]?Expr.attrHandle[name](elem):elem[name]!=null?elem[name]:elem.getAttribute(name),value=result+"",type=match[2],check=match[4];return result==null?type==="!=":type==="="?value===check:type==="*="?value.indexOf(check)>=0:type==="~="?(" "+value+" ").indexOf(check)>=0:!check?value&&result!==false:type==="!="?value!==check:type==="^="?value.indexOf(check)===0:type==="$="?value.substr(value.length-check.length)===check:type==="|="?value===check||value.substr(0,check.length+1)===check+"-":false},POS:function(elem,match,i,array){var name=match[2],filter=Expr.setFilters[name];if(filter){return filter(elem,i,match,array)}}}};var origPOS=Expr.match.POS;for(var type in Expr.match){Expr.match[type]=new RegExp(Expr.match[type].source+/(?![^\[]*\])(?![^\(]*\))/.source);Expr.leftMatch[type]=new RegExp(/(^(?:.|\r|\n)*?)/.source+Expr.match[type].source.replace(/\\(\d+)/g,function(all,num){return"\\"+(num-0+1)}))}var makeArray=function(array,results){array=Array.prototype.slice.call(array,0);if(results){results.push.apply(results,array);return results}return array};try{Array.prototype.slice.call(document.documentElement.childNodes,0)[0].nodeType}catch(e){makeArray=function(array,results){var ret=results||[];if(toString.call(array)==="[object Array]"){Array.prototype.push.apply(ret,array)}else{if(typeof array.length==="number"){for(var i=0,l=array.length;i<l;i++){ret.push(array[i])}}else{for(var i=0;array[i];i++){ret.push(array[i])}}}return ret}}var sortOrder;if(document.documentElement.compareDocumentPosition){sortOrder=function(a,b){if(!a.compareDocumentPosition||!b.compareDocumentPosition){if(a==b){hasDuplicate=true}return a.compareDocumentPosition?-1:1}var ret=a.compareDocumentPosition(b)&4?-1:a===b?0:1;if(ret===0){hasDuplicate=true}return ret}}else if("sourceIndex"in document.documentElement){sortOrder=function(a,b){if(!a.sourceIndex||!b.sourceIndex){if(a==b){hasDuplicate=true}return a.sourceIndex?-1:1}var ret=a.sourceIndex-b.sourceIndex;if(ret===0){hasDuplicate=true}return ret}}else if(document.createRange){sortOrder=function(a,b){if(!a.ownerDocument||!b.ownerDocument){if(a==b){hasDuplicate=true}return a.ownerDocument?-1:1}var aRange=a.ownerDocument.createRange(),bRange=b.ownerDocument.createRange();aRange.setStart(a,0);aRange.setEnd(a,0);bRange.setStart(b,0);bRange.setEnd(b,0);var ret=aRange.compareBoundaryPoints(Range.START_TO_END,bRange);if(ret===0){hasDuplicate=true}return ret}}function getText(elems){var ret="",elem;for(var i=0;elems[i];i++){elem=elems[i];if(elem.nodeType===3||elem.nodeType===4){ret+=elem.nodeValue}else if(elem.nodeType!==8){ret+=getText(elem.childNodes)}}return ret}(function(){var form=document.createElement("div"),id="script"+(new Date).getTime();form.innerHTML="<a name='"+id+"'/>";var root=document.documentElement;root.insertBefore(form,root.firstChild);if(document.getElementById(id)){Expr.find.ID=function(match,context,isXML){if(typeof context.getElementById!=="undefined"&&!isXML){var m=context.getElementById(match[1]);return m?m.id===match[1]||typeof m.getAttributeNode!=="undefined"&&m.getAttributeNode("id").nodeValue===match[1]?[m]:undefined:[]}};Expr.filter.ID=function(elem,match){var node=typeof elem.getAttributeNode!=="undefined"&&elem.getAttributeNode("id");return elem.nodeType===1&&node&&node.nodeValue===match}}root.removeChild(form);root=form=null})();(function(){var div=document.createElement("div");div.appendChild(document.createComment(""));if(div.getElementsByTagName("*").length>0){Expr.find.TAG=function(match,context){var results=context.getElementsByTagName(match[1]);if(match[1]==="*"){var tmp=[];for(var i=0;results[i];i++){if(results[i].nodeType===1){tmp.push(results[i])}}results=tmp}return results}}div.innerHTML="<a href='#'></a>";if(div.firstChild&&typeof div.firstChild.getAttribute!=="undefined"&&div.firstChild.getAttribute("href")!=="#"){Expr.attrHandle.href=function(elem){return elem.getAttribute("href",2)}}div=null})();if(document.querySelectorAll){(function(){var oldSizzle=Sizzle,div=document.createElement("div");div.innerHTML="<p class='TEST'></p>";if(div.querySelectorAll&&div.querySelectorAll(".TEST").length===0){return}Sizzle=function(query,context,extra,seed){context=context||document;if(!seed&&context.nodeType===9&&!isXML(context)){try{return makeArray(context.querySelectorAll(query),extra)}catch(e){}}return oldSizzle(query,context,extra,seed)};for(var prop in oldSizzle){Sizzle[prop]=oldSizzle[prop]}div=null})()}(function(){var div=document.createElement("div");div.innerHTML="<div class='test e'></div><div class='test'></div>";if(!div.getElementsByClassName||div.getElementsByClassName("e").length===0){return}div.lastChild.className="e";if(div.getElementsByClassName("e").length===1){return}Expr.order.splice(1,0,"CLASS");Expr.find.CLASS=function(match,context,isXML){if(typeof context.getElementsByClassName!=="undefined"&&!isXML){return context.getElementsByClassName(match[1])}};div=null})();function dirNodeCheck(dir,cur,doneName,checkSet,nodeCheck,isXML){for(var i=0,l=checkSet.length;i<l;i++){var elem=checkSet[i];if(elem){elem=elem[dir];var match=false;while(elem){if(elem.sizcache===doneName){match=checkSet[elem.sizset];break}if(elem.nodeType===1&&!isXML){elem.sizcache=doneName;elem.sizset=i}if(elem.nodeName.toLowerCase()===cur){match=elem;break}elem=elem[dir]}checkSet[i]=match}}}function dirCheck(dir,cur,doneName,checkSet,nodeCheck,isXML){for(var i=0,l=checkSet.length;i<l;i++){var elem=checkSet[i];if(elem){elem=elem[dir];var match=false;while(elem){if(elem.sizcache===doneName){match=checkSet[elem.sizset];break}if(elem.nodeType===1){if(!isXML){elem.sizcache=doneName;elem.sizset=i}if(typeof cur!=="string"){if(elem===cur){match=true;break}}else if(Sizzle.filter(cur,[elem]).length>0){match=elem;break}}elem=elem[dir]}checkSet[i]=match}}}var contains=document.compareDocumentPosition?function(a,b){return!!(a.compareDocumentPosition(b)&16)}:function(a,b){return a!==b&&(a.contains?a.contains(b):true)};var isXML=function(elem){var documentElement=(elem?elem.ownerDocument||elem:0).documentElement;return documentElement?documentElement.nodeName!=="HTML":false};var posProcess=function(selector,context){var tmpSet=[],later="",match,root=context.nodeType?[context]:context;while((match=Expr.match.PSEUDO.exec(selector))){later+=match[0];selector=selector.replace(Expr.match.PSEUDO,"")}selector=Expr.relative[selector]?selector+"*":selector;for(var i=0,l=root.length;i<l;i++){Sizzle(selector,root[i],tmpSet)}return Sizzle.filter(later,tmpSet)};jQuery.find=Sizzle;jQuery.expr=Sizzle.selectors;jQuery.expr[":"]=jQuery.expr.filters;jQuery.unique=Sizzle.uniqueSort;jQuery.text=getText;jQuery.isXMLDoc=isXML;jQuery.contains=contains;return;window.Sizzle=Sizzle})();var runtil=/Until$/,rparentsprev=/^(?:parents|prevUntil|prevAll)/,rmultiselector=/,/,slice=Array.prototype.slice;var winnow=function(elements,qualifier,keep){if(jQuery.isFunction(qualifier)){return jQuery.grep(elements,function(elem,i){return!!qualifier.call(elem,i,elem)===keep})}else if(qualifier.nodeType){return jQuery.grep(elements,function(elem,i){return(elem===qualifier)===keep})}else if(typeof qualifier==="string"){var filtered=jQuery.grep(elements,function(elem){return elem.nodeType===1});if(isSimple.test(qualifier)){return jQuery.filter(qualifier,filtered,!keep)}else{qualifier=jQuery.filter(qualifier,filtered)}}return jQuery.grep(elements,function(elem,i){return(jQuery.inArray(elem,qualifier)>=0)===keep})};jQuery.fn.extend({find:function(selector){var ret=this.pushStack("","find",selector),length=0;for(var i=0,l=this.length;i<l;i++){length=ret.length;jQuery.find(selector,this[i],ret);if(i>0){for(var n=length;n<ret.length;n++){for(var r=0;r<length;r++){if(ret[r]===ret[n]){ret.splice(n--,1);break}}}}}return ret},has:function(target){var targets=jQuery(target);return this.filter(function(){for(var i=0,l=targets.length;i<l;i++){if(jQuery.contains(this,targets[i])){return true}}})},not:function(selector){return this.pushStack(winnow(this,selector,false),"not",selector)},filter:function(selector){return this.pushStack(winnow(this,selector,true),"filter",selector)},is:function(selector){return!!selector&&jQuery.filter(selector,this).length>0},closest:function(selectors,context){if(jQuery.isArray(selectors)){var ret=[],cur=this[0],match,matches={},selector;if(cur&&selectors.length){for(var i=0,l=selectors.length;i<l;i++){selector=selectors[i];if(!matches[selector]){matches[selector]=jQuery.expr.match.POS.test(selector)?jQuery(selector,context||this.context):selector}}while(cur&&cur.ownerDocument&&cur!==context){for(selector in matches){match=matches[selector];if(match.jquery?match.index(cur)>-1:jQuery(cur).is(match)){ret.push({selector:selector,elem:cur});delete matches[selector]}}cur=cur.parentNode}}return ret}var pos=jQuery.expr.match.POS.test(selectors)?jQuery(selectors,context||this.context):null;return this.map(function(i,cur){while(cur&&cur.ownerDocument&&cur!==context){if(pos?pos.index(cur)>-1:jQuery(cur).is(selectors)){return cur}cur=cur.parentNode}return null})},index:function(elem){if(!elem||typeof elem==="string"){return jQuery.inArray(this[0],elem?jQuery(elem):this.parent().children())}return jQuery.inArray(elem.jquery?elem[0]:elem,this)},add:function(selector,context){var set=typeof selector==="string"?jQuery(selector,context||this.context):jQuery.makeArray(selector),all=jQuery.merge(this.get(),set);return this.pushStack(isDisconnected(set[0])||isDisconnected(all[0])?all:jQuery.unique(all))},andSelf:function(){return this.add(this.prevObject)}});function isDisconnected(node){return!node||!node.parentNode||node.parentNode.nodeType===11}jQuery.each({parent:function(elem){var parent=elem.parentNode;return parent&&parent.nodeType!==11?parent:null},parents:function(elem){return jQuery.dir(elem,"parentNode")},parentsUntil:function(elem,i,until){return jQuery.dir(elem,"parentNode",until)},next:function(elem){return jQuery.nth(elem,2,"nextSibling")},prev:function(elem){return jQuery.nth(elem,2,"previousSibling")},nextAll:function(elem){return jQuery.dir(elem,"nextSibling")},prevAll:function(elem){return jQuery.dir(elem,"previousSibling")},nextUntil:function(elem,i,until){return jQuery.dir(elem,"nextSibling",until)},prevUntil:function(elem,i,until){return jQuery.dir(elem,"previousSibling",until)},siblings:function(elem){return jQuery.sibling(elem.parentNode.firstChild,elem)},children:function(elem){return jQuery.sibling(elem.firstChild)},contents:function(elem){return jQuery.nodeName(elem,"iframe")?elem.contentDocument||elem.contentWindow.document:jQuery.makeArray(elem.childNodes)}},function(name,fn){jQuery.fn[name]=function(until,selector){var ret=jQuery.map(this,fn,until);if(!runtil.test(name)){selector=until}if(selector&&typeof selector==="string"){ret=jQuery.filter(selector,ret)}ret=this.length>1?jQuery.unique(ret):ret;if((this.length>1||rmultiselector.test(selector))&&rparentsprev.test(name)){ret=ret.reverse()}return this.pushStack(ret,name,slice.call(arguments).join(","))}});jQuery.extend({filter:function(expr,elems,not){if(not){expr=":not("+expr+")"}return jQuery.find.matches(expr,elems)},dir:function(elem,dir,until){var matched=[],cur=elem[dir];while(cur&&cur.nodeType!==9&&(until===undefined||cur.nodeType!==1||!jQuery(cur).is(until))){if(cur.nodeType===1){matched.push(cur)}cur=cur[dir]}return matched},nth:function(cur,result,dir,elem){result=result||1;var num=0;for(;cur;cur=cur[dir]){if(cur.nodeType===1&&++num===result){break}}return cur},sibling:function(n,elem){var r=[];for(;n;n=n.nextSibling){if(n.nodeType===1&&n!==elem){r.push(n)}}return r}});var rinlinejQuery=/ jQuery\d+="(?:\d+|null)"/g,rleadingWhitespace=/^\s+/,rxhtmlTag=/(<([\w:]+)[^>]*?)\/>/g,rselfClosing=/^(?:area|br|col|embed|hr|img|input|link|meta|param)$/i,rtagName=/<([\w:]+)/,rtbody=/<tbody/i,rhtml=/<|&#?\w+;/,rnocache=/<script|<object|<embed|<option|<style/i,rchecked=/checked\s*(?:[^=]|=\s*.checked.)/i,fcloseTag=function(all,front,tag){return rselfClosing.test(tag)?all:front+"></"+tag+">"},wrapMap={option:[1,"<select multiple='multiple'>","</select>"],legend:[1,"<fieldset>","</fieldset>"],thead:[1,"<table>","</table>"],tr:[2,"<table><tbody>","</tbody></table>"],td:[3,"<table><tbody><tr>","</tr></tbody></table>"],col:[2,"<table><tbody></tbody><colgroup>","</colgroup></table>"],area:[1,"<map>","</map>"],_default:[0,"",""]};wrapMap.optgroup=wrapMap.option;wrapMap.tbody=wrapMap.tfoot=wrapMap.colgroup=wrapMap.caption=wrapMap.thead;wrapMap.th=wrapMap.td;if(!jQuery.support.htmlSerialize){wrapMap._default=[1,"div<div>","</div>"]}jQuery.fn.extend({text:function(text){if(jQuery.isFunction(text)){return this.each(function(i){var self=jQuery(this);self.text(text.call(this,i,self.text()))})}if(typeof text!=="object"&&text!==undefined){return this.empty().append((this[0]&&this[0].ownerDocument||document).createTextNode(text))}return jQuery.text(this)},wrapAll:function(html){if(jQuery.isFunction(html)){return this.each(function(i){jQuery(this).wrapAll(html.call(this,i))})}if(this[0]){var wrap=jQuery(html,this[0].ownerDocument).eq(0).clone(true);if(this[0].parentNode){wrap.insertBefore(this[0])}wrap.map(function(){var elem=this;while(elem.firstChild&&elem.firstChild.nodeType===1){elem=elem.firstChild}return elem}).append(this)}return this},wrapInner:function(html){if(jQuery.isFunction(html)){return this.each(function(i){jQuery(this).wrapInner(html.call(this,i))})}return this.each(function(){var self=jQuery(this),contents=self.contents();if(contents.length){contents.wrapAll(html)}else{self.append(html)}})},wrap:function(html){return this.each(function(){jQuery(this).wrapAll(html)})},unwrap:function(){return this.parent().each(function(){if(!jQuery.nodeName(this,"body")){jQuery(this).replaceWith(this.childNodes)}}).end()},append:function(){return this.domManip(arguments,true,function(elem){if(this.nodeType===1){this.appendChild(elem)}})},prepend:function(){return this.domManip(arguments,true,function(elem){if(this.nodeType===1){this.insertBefore(elem,this.firstChild)}})},before:function(){if(this[0]&&this[0].parentNode){return this.domManip(arguments,false,function(elem){this.parentNode.insertBefore(elem,this)})}else if(arguments.length){var set=jQuery(arguments[0]);set.push.apply(set,this.toArray());return this.pushStack(set,"before",arguments)}},after:function(){if(this[0]&&this[0].parentNode){return this.domManip(arguments,false,function(elem){this.parentNode.insertBefore(elem,this.nextSibling)})}else if(arguments.length){var set=this.pushStack(this,"after",arguments);set.push.apply(set,jQuery(arguments[0]).toArray());return set}},remove:function(selector,keepData){for(var i=0,elem;(elem=this[i])!=null;i++){if(!selector||jQuery.filter(selector,[elem]).length){if(!keepData&&elem.nodeType===1){jQuery.cleanData(elem.getElementsByTagName("*"));jQuery.cleanData([elem])}if(elem.parentNode){elem.parentNode.removeChild(elem)}}}return this},empty:function(){for(var i=0,elem;(elem=this[i])!=null;i++){if(elem.nodeType===1){jQuery.cleanData(elem.getElementsByTagName("*"))}while(elem.firstChild){elem.removeChild(elem.firstChild)}}return this},clone:function(events){var ret=this.map(function(){if(!jQuery.support.noCloneEvent&&!jQuery.isXMLDoc(this)){var html=this.outerHTML,ownerDocument=this.ownerDocument;if(!html){var div=ownerDocument.createElement("div");div.appendChild(this.cloneNode(true));html=div.innerHTML}return jQuery.clean([html.replace(rinlinejQuery,"").replace(/=([^="'>\s]+\/)>/g,'="$1">').replace(rleadingWhitespace,"")],ownerDocument)[0]}else{return this.cloneNode(true)}});if(events===true){cloneCopyEvent(this,ret);cloneCopyEvent(this.find("*"),ret.find("*"))}return ret},html:function(value){if(value===undefined){return this[0]&&this[0].nodeType===1?this[0].innerHTML.replace(rinlinejQuery,""):null}else if(typeof value==="string"&&!rnocache.test(value)&&(jQuery.support.leadingWhitespace||!rleadingWhitespace.test(value))&&!wrapMap[(rtagName.exec(value)||["",""])[1].toLowerCase()]){value=value.replace(rxhtmlTag,fcloseTag);try{for(var i=0,l=this.length;i<l;i++){if(this[i].nodeType===1){jQuery.cleanData(this[i].getElementsByTagName("*"));this[i].innerHTML=value}}}catch(e){this.empty().append(value)}}else if(jQuery.isFunction(value)){this.each(function(i){var self=jQuery(this),old=self.html();self.empty().append(function(){return value.call(this,i,old)})})}else{this.empty().append(value)}return this},replaceWith:function(value){if(this[0]&&this[0].parentNode){if(jQuery.isFunction(value)){return this.each(function(i){var self=jQuery(this),old=self.html();self.replaceWith(value.call(this,i,old))})}if(typeof value!=="string"){value=jQuery(value).detach()}return this.each(function(){var next=this.nextSibling,parent=this.parentNode;jQuery(this).remove();if(next){jQuery(next).before(value)}else{jQuery(parent).append(value)}})}else{return this.pushStack(jQuery(jQuery.isFunction(value)?value():value),"replaceWith",value)}},detach:function(selector){return this.remove(selector,true)},domManip:function(args,table,callback){var results,first,value=args[0],scripts=[],fragment,parent;if(!jQuery.support.checkClone&&arguments.length===3&&typeof value==="string"&&rchecked.test(value)){return this.each(function(){jQuery(this).domManip(args,table,callback,true)})}if(jQuery.isFunction(value)){return this.each(function(i){var self=jQuery(this);args[0]=value.call(this,i,table?self.html():undefined);self.domManip(args,table,callback)})}if(this[0]){parent=value&&value.parentNode;if(jQuery.support.parentNode&&parent&&parent.nodeType===11&&parent.childNodes.length===this.length){results={fragment:parent}}else{results=buildFragment(args,this,scripts)}fragment=results.fragment;if(fragment.childNodes.length===1){first=fragment=fragment.firstChild}else{first=fragment.firstChild}if(first){table=table&&jQuery.nodeName(first,"tr");for(var i=0,l=this.length;i<l;i++){callback.call(table?root(this[i],first):this[i],i>0||results.cacheable||this.length>1?fragment.cloneNode(true):fragment)}}if(scripts.length){jQuery.each(scripts,evalScript)}}return this;function root(elem,cur){return jQuery.nodeName(elem,"table")?(elem.getElementsByTagName("tbody")[0]||elem.appendChild(elem.ownerDocument.createElement("tbody"))):elem}}});function cloneCopyEvent(orig,ret){var i=0;ret.each(function(){if(this.nodeName!==(orig[i]&&orig[i].nodeName)){return}var oldData=jQuery.data(orig[i++]),curData=jQuery.data(this,oldData),events=oldData&&oldData.events;if(events){delete curData.handle;curData.events={};for(var type in events){for(var handler in events[type]){jQuery.event.add(this,type,events[type][handler],events[type][handler].data)}}}})}function buildFragment(args,nodes,scripts){var fragment,cacheable,cacheresults,doc=(nodes&&nodes[0]?nodes[0].ownerDocument||nodes[0]:document);if(args.length===1&&typeof args[0]==="string"&&args[0].length<512&&doc===document&&!rnocache.test(args[0])&&(jQuery.support.checkClone||!rchecked.test(args[0]))){cacheable=true;cacheresults=jQuery.fragments[args[0]];if(cacheresults){if(cacheresults!==1){fragment=cacheresults}}}if(!fragment){fragment=doc.createDocumentFragment();jQuery.clean(args,doc,fragment,scripts)}if(cacheable){jQuery.fragments[args[0]]=cacheresults?fragment:1}return{fragment:fragment,cacheable:cacheable}}jQuery.fragments={};jQuery.each({appendTo:"append",prependTo:"prepend",insertBefore:"before",insertAfter:"after",replaceAll:"replaceWith"},function(name,original){jQuery.fn[name]=function(selector){var ret=[],insert=jQuery(selector),parent=this.length===1&&this[0].parentNode;if(parent&&parent.nodeType===11&&parent.childNodes.length===1&&insert.length===1){insert[original](this[0]);return this}else{for(var i=0,l=insert.length;i<l;i++){var elems=(i>0?this.clone(true):this).get();jQuery.fn[original].apply(jQuery(insert[i]),elems);ret=ret.concat(elems)}return this.pushStack(ret,name,insert.selector)}}});jQuery.extend({clean:function(elems,context,fragment,scripts){context=context||document;if(typeof context.createElement==="undefined"){context=context.ownerDocument||context[0]&&context[0].ownerDocument||document}var ret=[];for(var i=0,elem;(elem=elems[i])!=null;i++){if(typeof elem==="number"){elem+=""}if(!elem){continue}if(typeof elem==="string"&&!rhtml.test(elem)){elem=context.createTextNode(elem)}else if(typeof elem==="string"){elem=elem.replace(rxhtmlTag,fcloseTag);var tag=(rtagName.exec(elem)||["",""])[1].toLowerCase(),wrap=wrapMap[tag]||wrapMap._default,depth=wrap[0],div=context.createElement("div");div.innerHTML=wrap[1]+elem+wrap[2];while(depth--){div=div.lastChild}if(!jQuery.support.tbody){var hasBody=rtbody.test(elem),tbody=tag==="table"&&!hasBody?div.firstChild&&div.firstChild.childNodes:wrap[1]==="<table>"&&!hasBody?div.childNodes:[];for(var j=tbody.length-1;j>=0;--j){if(jQuery.nodeName(tbody[j],"tbody")&&!tbody[j].childNodes.length){tbody[j].parentNode.removeChild(tbody[j])}}}if(!jQuery.support.leadingWhitespace&&rleadingWhitespace.test(elem)){div.insertBefore(context.createTextNode(rleadingWhitespace.exec(elem)[0]),div.firstChild)}elem=div.childNodes}if(elem.nodeType){ret.push(elem)}else{ret=jQuery.merge(ret,elem)}}if(fragment){for(var i=0;ret[i];i++){if(scripts&&jQuery.nodeName(ret[i],"script")&&(!ret[i].type||ret[i].type.toLowerCase()==="text/javascript")){scripts.push(ret[i].parentNode?ret[i].parentNode.removeChild(ret[i]):ret[i])}else{if(ret[i].nodeType===1){ret.splice.apply(ret,[i+1,0].concat(jQuery.makeArray(ret[i].getElementsByTagName("script"))))}fragment.appendChild(ret[i])}}}return ret},cleanData:function(elems){var data,id,cache=jQuery.cache,special=jQuery.event.special,deleteExpando=jQuery.support.deleteExpando;for(var i=0,elem;(elem=elems[i])!=null;i++){id=elem[jQuery.expando];if(id){data=cache[id];if(data.events){for(var type in data.events){if(special[type]){jQuery.event.remove(elem,type)}else{removeEvent(elem,type,data.handle)}}}if(deleteExpando){delete elem[jQuery.expando]}else if(elem.removeAttribute){elem.removeAttribute(jQuery.expando)}delete cache[id]}}}});var rexclude=/z-?index|font-?weight|opacity|zoom|line-?height/i,ralpha=/alpha\([^)]*\)/,ropacity=/opacity=([^)]*)/,rfloat=/float/i,rdashAlpha=/-([a-z])/ig,rupper=/([A-Z])/g,rnumpx=/^-?\d+(?:px)?$/i,rnum=/^-?\d/,cssShow={position:"absolute",visibility:"hidden",display:"block"},cssWidth=["Left","Right"],cssHeight=["Top","Bottom"],getComputedStyle=document.defaultView&&document.defaultView.getComputedStyle,styleFloat=jQuery.support.cssFloat?"cssFloat":"styleFloat",fcamelCase=function(all,letter){return letter.toUpperCase()};jQuery.fn.css=function(name,value){return access(this,name,value,true,function(elem,name,value){if(value===undefined){return jQuery.curCSS(elem,name)}if(typeof value==="number"&&!rexclude.test(name)){value+="px"}jQuery.style(elem,name,value)})};jQuery.extend({style:function(elem,name,value){if(!elem||elem.nodeType===3||elem.nodeType===8){return undefined}if((name==="width"||name==="height")&&parseFloat(value)<0){value=undefined}var style=elem.style||elem,set=value!==undefined;if(!jQuery.support.opacity&&name==="opacity"){if(set){style.zoom=1;var opacity=parseInt(value,10)+""==="NaN"?"":"alpha(opacity="+value*100+")";var filter=style.filter||jQuery.curCSS(elem,"filter")||"";style.filter=ralpha.test(filter)?filter.replace(ralpha,opacity):opacity}return style.filter&&style.filter.indexOf("opacity=")>=0?(parseFloat(ropacity.exec(style.filter)[1])/100)+"":""}if(rfloat.test(name)){name=styleFloat}name=name.replace(rdashAlpha,fcamelCase);if(set){style[name]=value}return style[name]},css:function(elem,name,force,extra){if(name==="width"||name==="height"){var val,props=cssShow,which=name==="width"?cssWidth:cssHeight;function getWH(){val=name==="width"?elem.offsetWidth:elem.offsetHeight;if(extra==="border"){return}jQuery.each(which,function(){if(!extra){val-=parseFloat(jQuery.curCSS(elem,"padding"+this,true))||0}if(extra==="margin"){val+=parseFloat(jQuery.curCSS(elem,"margin"+this,true))||0}else{val-=parseFloat(jQuery.curCSS(elem,"border"+this+"Width",true))||0}})}if(elem.offsetWidth!==0){getWH()}else{jQuery.swap(elem,props,getWH)}return Math.max(0,Math.round(val))}return jQuery.curCSS(elem,name,force)},curCSS:function(elem,name,force){var ret,style=elem.style,filter;if(!jQuery.support.opacity&&name==="opacity"&&elem.currentStyle){ret=ropacity.test(elem.currentStyle.filter||"")?(parseFloat(RegExp.$1)/100)+"":"";return ret===""?"1":ret}if(rfloat.test(name)){name=styleFloat}if(!force&&style&&style[name]){ret=style[name]}else if(getComputedStyle){if(rfloat.test(name)){name="float"}name=name.replace(rupper,"-$1").toLowerCase();var defaultView=elem.ownerDocument.defaultView;if(!defaultView){return null}var computedStyle=defaultView.getComputedStyle(elem,null);if(computedStyle){ret=computedStyle.getPropertyValue(name)}if(name==="opacity"&&ret===""){ret="1"}}else if(elem.currentStyle){var camelCase=name.replace(rdashAlpha,fcamelCase);ret=elem.currentStyle[name]||elem.currentStyle[camelCase];if(!rnumpx.test(ret)&&rnum.test(ret)){var left=style.left,rsLeft=elem.runtimeStyle.left;elem.runtimeStyle.left=elem.currentStyle.left;style.left=camelCase==="fontSize"?"1em":(ret||0);ret=style.pixelLeft+"px";style.left=left;elem.runtimeStyle.left=rsLeft}}return ret},swap:function(elem,options,callback){var old={};for(var name in options){old[name]=elem.style[name];elem.style[name]=options[name]}callback.call(elem);for(var name in options){elem.style[name]=old[name]}}});if(jQuery.expr&&jQuery.expr.filters){jQuery.expr.filters.hidden=function(elem){var width=elem.offsetWidth,height=elem.offsetHeight,skip=elem.nodeName.toLowerCase()==="tr";return width===0&&height===0&&!skip?true:width>0&&height>0&&!skip?false:jQuery.curCSS(elem,"display")==="none"};jQuery.expr.filters.visible=function(elem){return!jQuery.expr.filters.hidden(elem)}}var jsc=now(),rscript=/<script(.|\s)*?\/script>/gi,rselectTextarea=/select|textarea/i,rinput=/color|date|datetime|email|hidden|month|number|password|range|search|tel|text|time|url|week/i,jsre=/=\?(&|$)/,rquery=/\?/,rts=/(\?|&)_=.*?(&|$)/,rurl=/^(\w+:)?\/\/([^\/?#]+)/,r20=/%20/g,_load=jQuery.fn.load;jQuery.fn.extend({load:function(url,params,callback){if(typeof url!=="string"){return _load.call(this,url)}else if(!this.length){return this}var off=url.indexOf(" ");if(off>=0){var selector=url.slice(off,url.length);url=url.slice(0,off)}var type="GET";if(params){if(jQuery.isFunction(params)){callback=params;params=null}else if(typeof params==="object"){params=jQuery.param(params,jQuery.ajaxSettings.traditional);type="POST"}}var self=this;jQuery.ajax({url:url,type:type,dataType:"html",data:params,complete:function(res,status){if(status==="success"||status==="notmodified"){self.html(selector?jQuery("<div />").append(res.responseText.replace(rscript,"")).find(selector):res.responseText)}if(callback){self.each(callback,[res.responseText,status,res])}}});return this},serialize:function(){return jQuery.param(this.serializeArray())},serializeArray:function(){return this.map(function(){return this.elements?jQuery.makeArray(this.elements):this}).filter(function(){return this.name&&!this.disabled&&(this.checked||rselectTextarea.test(this.nodeName)||rinput.test(this.type))}).map(function(i,elem){var val=jQuery(this).val();return val==null?null:jQuery.isArray(val)?jQuery.map(val,function(val,i){return{name:elem.name,value:val}}):{name:elem.name,value:val}}).get()}});jQuery.each("ajaxStart ajaxStop ajaxComplete ajaxError ajaxSuccess ajaxSend".split(" "),function(i,o){jQuery.fn[o]=function(f){return this.bind(o,f)}});jQuery.extend({get:function(url,data,callback,type){if(jQuery.isFunction(data)){type=type||callback;callback=data;data=null}return jQuery.ajax({type:"GET",url:url,data:data,success:callback,dataType:type})},getScript:function(url,callback){return jQuery.get(url,null,callback,"script")},getJSON:function(url,data,callback){return jQuery.get(url,data,callback,"json")},post:function(url,data,callback,type){if(jQuery.isFunction(data)){type=type||callback;callback=data;data={}}return jQuery.ajax({type:"POST",url:url,data:data,success:callback,dataType:type})},ajaxSetup:function(settings){jQuery.extend(jQuery.ajaxSettings,settings)},ajaxSettings:{url:location.href,global:true,type:"GET",contentType:"application/x-www-form-urlencoded",processData:true,async:true,xhr:window.XMLHttpRequest&&(window.location.protocol!=="file:"||!window.ActiveXObject)?function(){return new window.XMLHttpRequest()}:function(){try{return new window.ActiveXObject("Microsoft.XMLHTTP")}catch(e){}},accepts:{xml:"application/xml, text/xml",html:"text/html",script:"text/javascript, application/javascript",json:"application/json, text/javascript",text:"text/plain",_default:"*/*"}},lastModified:{},etag:{},ajax:function(origSettings){var s=jQuery.extend(true,{},jQuery.ajaxSettings,origSettings);var jsonp,status,data,callbackContext=origSettings&&origSettings.context||s,type=s.type.toUpperCase();if(s.data&&s.processData&&typeof s.data!=="string"){s.data=jQuery.param(s.data,s.traditional)}if(s.dataType==="jsonp"){if(type==="GET"){if(!jsre.test(s.url)){s.url+=(rquery.test(s.url)?"&":"?")+(s.jsonp||"callback")+"=?"}}else if(!s.data||!jsre.test(s.data)){s.data=(s.data?s.data+"&":"")+(s.jsonp||"callback")+"=?"}s.dataType="json"}if(s.dataType==="json"&&(s.data&&jsre.test(s.data)||jsre.test(s.url))){jsonp=s.jsonpCallback||("jsonp"+jsc++);if(s.data){s.data=(s.data+"").replace(jsre,"="+jsonp+"$1")}s.url=s.url.replace(jsre,"="+jsonp+"$1");s.dataType="script";window[jsonp]=window[jsonp]||function(tmp){data=tmp;success();complete();window[jsonp]=undefined;try{delete window[jsonp]}catch(e){}if(head){head.removeChild(script)}}}if(s.dataType==="script"&&s.cache===null){s.cache=false}if(s.cache===false&&type==="GET"){var ts=now();var ret=s.url.replace(rts,"$1_="+ts+"$2");s.url=ret+((ret===s.url)?(rquery.test(s.url)?"&":"?")+"_="+ts:"")}if(s.data&&type==="GET"){s.url+=(rquery.test(s.url)?"&":"?")+s.data}if(s.global&&!jQuery.active++){jQuery.event.trigger("ajaxStart")}var parts=rurl.exec(s.url),remote=parts&&(parts[1]&&parts[1]!==location.protocol||parts[2]!==location.host);if(s.dataType==="script"&&type==="GET"&&remote){var head=document.getElementsByTagName("head")[0]||document.documentElement;var script=document.createElement("script");script.src=s.url;if(s.scriptCharset){script.charset=s.scriptCharset}if(!jsonp){var done=false;script.onload=script.onreadystatechange=function(){if(!done&&(!this.readyState||this.readyState==="loaded"||this.readyState==="complete")){done=true;success();complete();script.onload=script.onreadystatechange=null;if(head&&script.parentNode){head.removeChild(script)}}}}head.insertBefore(script,head.firstChild);return undefined}var requestDone=false;var xhr=s.xhr();if(!xhr){return}if(s.username){xhr.open(type,s.url,s.async,s.username,s.password)}else{xhr.open(type,s.url,s.async)}try{if(s.data||origSettings&&origSettings.contentType){xhr.setRequestHeader("Content-Type",s.contentType)}if(s.ifModified){if(jQuery.lastModified[s.url]){xhr.setRequestHeader("If-Modified-Since",jQuery.lastModified[s.url])}if(jQuery.etag[s.url]){xhr.setRequestHeader("If-None-Match",jQuery.etag[s.url])}}if(!remote){xhr.setRequestHeader("X-Requested-With","XMLHttpRequest")}xhr.setRequestHeader("Accept",s.dataType&&s.accepts[s.dataType]?s.accepts[s.dataType]+", */*":s.accepts._default)}catch(e){}if(s.beforeSend&&s.beforeSend.call(callbackContext,xhr,s)===false){if(s.global&&!--jQuery.active){jQuery.event.trigger("ajaxStop")}xhr.abort();return false}if(s.global){trigger("ajaxSend",[xhr,s])}var onreadystatechange=xhr.onreadystatechange=function(isTimeout){if(!xhr||xhr.readyState===0||isTimeout==="abort"){if(!requestDone){complete()}requestDone=true;if(xhr){xhr.onreadystatechange=jQuery.noop}}else if(!requestDone&&xhr&&(xhr.readyState===4||isTimeout==="timeout")){requestDone=true;xhr.onreadystatechange=jQuery.noop;status=isTimeout==="timeout"?"timeout":!jQuery.httpSuccess(xhr)?"error":s.ifModified&&jQuery.httpNotModified(xhr,s.url)?"notmodified":"success";var errMsg;if(status==="success"){try{data=jQuery.httpData(xhr,s.dataType,s)}catch(err){status="parsererror";errMsg=err}}if(status==="success"||status==="notmodified"){if(!jsonp){success()}}else{jQuery.handleError(s,xhr,status,errMsg)}complete();if(isTimeout==="timeout"){xhr.abort()}if(s.async){xhr=null}}};try{var oldAbort=xhr.abort;xhr.abort=function(){if(xhr){oldAbort.call(xhr)}onreadystatechange("abort")}}catch(e){}if(s.async&&s.timeout>0){setTimeout(function(){if(xhr&&!requestDone){onreadystatechange("timeout")}},s.timeout)}try{xhr.send(type==="POST"||type==="PUT"||type==="DELETE"?s.data:null)}catch(e){jQuery.handleError(s,xhr,null,e);complete()}if(!s.async){onreadystatechange()}function success(){if(s.success){s.success.call(callbackContext,data,status,xhr)}if(s.global){trigger("ajaxSuccess",[xhr,s])}}function complete(){if(s.complete){s.complete.call(callbackContext,xhr,status)}if(s.global){trigger("ajaxComplete",[xhr,s])}if(s.global&&!--jQuery.active){jQuery.event.trigger("ajaxStop")}}function trigger(type,args){(s.context?jQuery(s.context):jQuery.event).trigger(type,args)}return xhr},handleError:function(s,xhr,status,e){if(s.error){s.error.call(s.context||s,xhr,status,e)}if(s.global){(s.context?jQuery(s.context):jQuery.event).trigger("ajaxError",[xhr,s,e])}},active:0,httpSuccess:function(xhr){try{return!xhr.status&&location.protocol==="file:"||(xhr.status>=200&&xhr.status<300)||xhr.status===304||xhr.status===1223||xhr.status===0}catch(e){}return false},httpNotModified:function(xhr,url){var lastModified=xhr.getResponseHeader("Last-Modified"),etag=xhr.getResponseHeader("Etag");if(lastModified){jQuery.lastModified[url]=lastModified}if(etag){jQuery.etag[url]=etag}return xhr.status===304||xhr.status===0},httpData:function(xhr,type,s){var ct=xhr.getResponseHeader("content-type")||"",xml=type==="xml"||!type&&ct.indexOf("xml")>=0,data=xml?xhr.responseXML:xhr.responseText;if(xml&&data.documentElement.nodeName==="parsererror"){jQuery.error("parsererror")}if(s&&s.dataFilter){data=s.dataFilter(data,type)}if(typeof data==="string"){if(type==="json"||!type&&ct.indexOf("json")>=0){data=jQuery.parseJSON(data)}else if(type==="script"||!type&&ct.indexOf("javascript")>=0){jQuery.globalEval(data)}}return data},param:function(a,traditional){var s=[];if(traditional===undefined){traditional=jQuery.ajaxSettings.traditional}if(jQuery.isArray(a)||a.jquery){jQuery.each(a,function(){add(this.name,this.value)})}else{for(var prefix in a){buildParams(prefix,a[prefix])}}return s.join("&").replace(r20,"+");function buildParams(prefix,obj){if(jQuery.isArray(obj)){jQuery.each(obj,function(i,v){if(traditional||/\[\]$/.test(prefix)){add(prefix,v)}else{buildParams(prefix+"["+(typeof v==="object"||jQuery.isArray(v)?i:"")+"]",v)}})}else if(!traditional&&obj!=null&&typeof obj==="object"){jQuery.each(obj,function(k,v){buildParams(prefix+"["+k+"]",v)})}else{add(prefix,obj)}}function add(key,value){value=jQuery.isFunction(value)?value():value;s[s.length]=encodeURIComponent(key)+"="+encodeURIComponent(value)}}});var elemdisplay={},rfxtypes=/toggle|show|hide/,rfxnum=/^([+-]=)?([\d+-.]+)(.*)$/,timerId,fxAttrs=[["height","marginTop","marginBottom","paddingTop","paddingBottom"],["width","marginLeft","marginRight","paddingLeft","paddingRight"],["opacity"]];jQuery.fn.extend({show:function(speed,callback){if(speed||speed===0){return this.animate(genFx("show",3),speed,callback)}else{for(var i=0,l=this.length;i<l;i++){var old=jQuery.data(this[i],"olddisplay");this[i].style.display=old||"";if(jQuery.css(this[i],"display")==="none"){var nodeName=this[i].nodeName,display;if(elemdisplay[nodeName]){display=elemdisplay[nodeName]}else{var elem=jQuery("<"+nodeName+" />").appendTo("body");display=elem.css("display");if(display==="none"){display="block"}elem.remove();elemdisplay[nodeName]=display}jQuery.data(this[i],"olddisplay",display)}}for(var j=0,k=this.length;j<k;j++){this[j].style.display=jQuery.data(this[j],"olddisplay")||""}return this}},hide:function(speed,callback){if(speed||speed===0){return this.animate(genFx("hide",3),speed,callback)}else{for(var i=0,l=this.length;i<l;i++){var old=jQuery.data(this[i],"olddisplay");if(!old&&old!=="none"){jQuery.data(this[i],"olddisplay",jQuery.css(this[i],"display"))}}for(var j=0,k=this.length;j<k;j++){this[j].style.display="none"}return this}},_toggle:jQuery.fn.toggle,toggle:function(fn,fn2){var bool=typeof fn==="boolean";if(jQuery.isFunction(fn)&&jQuery.isFunction(fn2)){this._toggle.apply(this,arguments)}else if(fn==null||bool){this.each(function(){var state=bool?fn:jQuery(this).is(":hidden");jQuery(this)[state?"show":"hide"]()})}else{this.animate(genFx("toggle",3),fn,fn2)}return this},fadeTo:function(speed,to,callback){return this.filter(":hidden").css("opacity",0).show().end().animate({opacity:to},speed,callback)},animate:function(prop,speed,easing,callback){var optall=jQuery.speed(speed,easing,callback);if(jQuery.isEmptyObject(prop)){return this.each(optall.complete)}return this[optall.queue===false?"each":"queue"](function(){var opt=jQuery.extend({},optall),p,hidden=this.nodeType===1&&jQuery(this).is(":hidden"),self=this;for(p in prop){var name=p.replace(rdashAlpha,fcamelCase);if(p!==name){prop[name]=prop[p];delete prop[p];p=name}if(prop[p]==="hide"&&hidden||prop[p]==="show"&&!hidden){return opt.complete.call(this)}if((p==="height"||p==="width")&&this.style){opt.display=jQuery.css(this,"display");opt.overflow=this.style.overflow}if(jQuery.isArray(prop[p])){(opt.specialEasing=opt.specialEasing||{})[p]=prop[p][1];prop[p]=prop[p][0]}}if(opt.overflow!=null){this.style.overflow="hidden"}opt.curAnim=jQuery.extend({},prop);jQuery.each(prop,function(name,val){var e=new jQuery.fx(self,opt,name);if(rfxtypes.test(val)){e[val==="toggle"?hidden?"show":"hide":val](prop)}else{var parts=rfxnum.exec(val),start=e.cur(true)||0;if(parts){var end=parseFloat(parts[2]),unit=parts[3]||"px";if(unit!=="px"){self.style[name]=(end||1)+unit;start=((end||1)/e.cur(true))*start;self.style[name]=start+unit}if(parts[1]){end=((parts[1]==="-="?-1:1)*end)+start}e.custom(start,end,unit)}else{e.custom(start,val,"")}}});return true})},stop:function(clearQueue,gotoEnd){var timers=jQuery.timers;if(clearQueue){this.queue([])}this.each(function(){for(var i=timers.length-1;i>=0;i--){if(timers[i].elem===this){if(gotoEnd){timers[i](true)}timers.splice(i,1)}}});if(!gotoEnd){this.dequeue()}return this}});jQuery.each({slideDown:genFx("show",1),slideUp:genFx("hide",1),slideToggle:genFx("toggle",1),fadeIn:{opacity:"show"},fadeOut:{opacity:"hide"}},function(name,props){jQuery.fn[name]=function(speed,callback){return this.animate(props,speed,callback)}});jQuery.extend({speed:function(speed,easing,fn){var opt=speed&&typeof speed==="object"?speed:{complete:fn||!fn&&easing||jQuery.isFunction(speed)&&speed,duration:speed,easing:fn&&easing||easing&&!jQuery.isFunction(easing)&&easing};opt.duration=jQuery.fx.off?0:typeof opt.duration==="number"?opt.duration:jQuery.fx.speeds[opt.duration]||jQuery.fx.speeds._default;opt.old=opt.complete;opt.complete=function(){if(opt.queue!==false){jQuery(this).dequeue()}if(jQuery.isFunction(opt.old)){opt.old.call(this)}};return opt},easing:{linear:function(p,n,firstNum,diff){return firstNum+diff*p},swing:function(p,n,firstNum,diff){return((-Math.cos(p*Math.PI)/2)+0.5)*diff+firstNum}},timers:[],fx:function(elem,options,prop){this.options=options;this.elem=elem;this.prop=prop;if(!options.orig){options.orig={}}}});jQuery.fx.prototype={update:function(){if(this.options.step){this.options.step.call(this.elem,this.now,this)}(jQuery.fx.step[this.prop]||jQuery.fx.step._default)(this);if((this.prop==="height"||this.prop==="width")&&this.elem.style){this.elem.style.display="block"}},cur:function(force){if(this.elem[this.prop]!=null&&(!this.elem.style||this.elem.style[this.prop]==null)){return this.elem[this.prop]}var r=parseFloat(jQuery.css(this.elem,this.prop,force));return r&&r>-10000?r:parseFloat(jQuery.curCSS(this.elem,this.prop))||0},custom:function(from,to,unit){this.startTime=now();this.start=from;this.end=to;this.unit=unit||this.unit||"px";this.now=this.start;this.pos=this.state=0;var self=this;function t(gotoEnd){return self.step(gotoEnd)}t.elem=this.elem;if(t()&&jQuery.timers.push(t)&&!timerId){timerId=setInterval(jQuery.fx.tick,13)}},show:function(){this.options.orig[this.prop]=jQuery.style(this.elem,this.prop);this.options.show=true;this.custom(this.prop==="width"||this.prop==="height"?1:0,this.cur());jQuery(this.elem).show()},hide:function(){this.options.orig[this.prop]=jQuery.style(this.elem,this.prop);this.options.hide=true;this.custom(this.cur(),0)},step:function(gotoEnd){var t=now(),done=true;if(gotoEnd||t>=this.options.duration+this.startTime){this.now=this.end;this.pos=this.state=1;this.update();this.options.curAnim[this.prop]=true;for(var i in this.options.curAnim){if(this.options.curAnim[i]!==true){done=false}}if(done){if(this.options.display!=null){this.elem.style.overflow=this.options.overflow;var old=jQuery.data(this.elem,"olddisplay");this.elem.style.display=old?old:this.options.display;if(jQuery.css(this.elem,"display")==="none"){this.elem.style.display="block"}}if(this.options.hide){jQuery(this.elem).hide()}if(this.options.hide||this.options.show){for(var p in this.options.curAnim){jQuery.style(this.elem,p,this.options.orig[p])}}this.options.complete.call(this.elem)}return false}else{var n=t-this.startTime;this.state=n/this.options.duration;var specialEasing=this.options.specialEasing&&this.options.specialEasing[this.prop];var defaultEasing=this.options.easing||(jQuery.easing.swing?"swing":"linear");this.pos=jQuery.easing[specialEasing||defaultEasing](this.state,n,0,1,this.options.duration);this.now=this.start+((this.end-this.start)*this.pos);this.update()}return true}};jQuery.extend(jQuery.fx,{tick:function(){var timers=jQuery.timers;for(var i=0;i<timers.length;i++){if(!timers[i]()){timers.splice(i--,1)}}if(!timers.length){jQuery.fx.stop()}},stop:function(){clearInterval(timerId);timerId=null},speeds:{slow:600,fast:200,_default:400},step:{opacity:function(fx){jQuery.style(fx.elem,"opacity",fx.now)},_default:function(fx){if(fx.elem.style&&fx.elem.style[fx.prop]!=null){fx.elem.style[fx.prop]=(fx.prop==="width"||fx.prop==="height"?Math.max(0,fx.now):fx.now)+fx.unit}else{fx.elem[fx.prop]=fx.now}}}});if(jQuery.expr&&jQuery.expr.filters){jQuery.expr.filters.animated=function(elem){return jQuery.grep(jQuery.timers,function(fn){return elem===fn.elem}).length}}function genFx(type,num){var obj={};jQuery.each(fxAttrs.concat.apply([],fxAttrs.slice(0,num)),function(){obj[this]=type});return obj}if("getBoundingClientRect"in document.documentElement){jQuery.fn.offset=function(options){var elem=this[0];if(options){return this.each(function(i){jQuery.offset.setOffset(this,options,i)})}if(!elem||!elem.ownerDocument){return null}if(elem===elem.ownerDocument.body){return jQuery.offset.bodyOffset(elem)}var box=elem.getBoundingClientRect(),doc=elem.ownerDocument,body=doc.body,docElem=doc.documentElement,clientTop=docElem.clientTop||body.clientTop||0,clientLeft=docElem.clientLeft||body.clientLeft||0,top=box.top+(self.pageYOffset||jQuery.support.boxModel&&docElem.scrollTop||body.scrollTop)-clientTop,left=box.left+(self.pageXOffset||jQuery.support.boxModel&&docElem.scrollLeft||body.scrollLeft)-clientLeft;return{top:top,left:left}}}else{jQuery.fn.offset=function(options){var elem=this[0];if(options){return this.each(function(i){jQuery.offset.setOffset(this,options,i)})}if(!elem||!elem.ownerDocument){return null}if(elem===elem.ownerDocument.body){return jQuery.offset.bodyOffset(elem)}jQuery.offset.initialize();var offsetParent=elem.offsetParent,prevOffsetParent=elem,doc=elem.ownerDocument,computedStyle,docElem=doc.documentElement,body=doc.body,defaultView=doc.defaultView,prevComputedStyle=defaultView?defaultView.getComputedStyle(elem,null):elem.currentStyle,top=elem.offsetTop,left=elem.offsetLeft;while((elem=elem.parentNode)&&elem!==body&&elem!==docElem){if(jQuery.offset.supportsFixedPosition&&prevComputedStyle.position==="fixed"){break}computedStyle=defaultView?defaultView.getComputedStyle(elem,null):elem.currentStyle;top-=elem.scrollTop;left-=elem.scrollLeft;if(elem===offsetParent){top+=elem.offsetTop;left+=elem.offsetLeft;if(jQuery.offset.doesNotAddBorder&&!(jQuery.offset.doesAddBorderForTableAndCells&&/^t(able|d|h)$/i.test(elem.nodeName))){top+=parseFloat(computedStyle.borderTopWidth)||0;left+=parseFloat(computedStyle.borderLeftWidth)||0}prevOffsetParent=offsetParent,offsetParent=elem.offsetParent}if(jQuery.offset.subtractsBorderForOverflowNotVisible&&computedStyle.overflow!=="visible"){top+=parseFloat(computedStyle.borderTopWidth)||0;left+=parseFloat(computedStyle.borderLeftWidth)||0}prevComputedStyle=computedStyle}if(prevComputedStyle.position==="relative"||prevComputedStyle.position==="static"){top+=body.offsetTop;left+=body.offsetLeft}if(jQuery.offset.supportsFixedPosition&&prevComputedStyle.position==="fixed"){top+=Math.max(docElem.scrollTop,body.scrollTop);left+=Math.max(docElem.scrollLeft,body.scrollLeft)}return{top:top,left:left}}}jQuery.offset={initialize:function(){var body=document.body,container=document.createElement("div"),innerDiv,checkDiv,table,td,bodyMarginTop=parseFloat(jQuery.curCSS(body,"marginTop",true))||0,html="<div style='position:absolute;top:0;left:0;margin:0;border:5px solid #000;padding:0;width:1px;height:1px;'><div></div></div><table style='position:absolute;top:0;left:0;margin:0;border:5px solid #000;padding:0;width:1px;height:1px;' cellpadding='0' cellspacing='0'><tr><td></td></tr></table>";jQuery.extend(container.style,{position:"absolute",top:0,left:0,margin:0,border:0,width:"1px",height:"1px",visibility:"hidden"});container.innerHTML=html;body.insertBefore(container,body.firstChild);innerDiv=container.firstChild;checkDiv=innerDiv.firstChild;td=innerDiv.nextSibling.firstChild.firstChild;this.doesNotAddBorder=(checkDiv.offsetTop!==5);this.doesAddBorderForTableAndCells=(td.offsetTop===5);checkDiv.style.position="fixed",checkDiv.style.top="20px";this.supportsFixedPosition=(checkDiv.offsetTop===20||checkDiv.offsetTop===15);checkDiv.style.position=checkDiv.style.top="";innerDiv.style.overflow="hidden",innerDiv.style.position="relative";this.subtractsBorderForOverflowNotVisible=(checkDiv.offsetTop===-5);this.doesNotIncludeMarginInBodyOffset=(body.offsetTop!==bodyMarginTop);body.removeChild(container);body=container=innerDiv=checkDiv=table=td=null;jQuery.offset.initialize=jQuery.noop},bodyOffset:function(body){var top=body.offsetTop,left=body.offsetLeft;jQuery.offset.initialize();if(jQuery.offset.doesNotIncludeMarginInBodyOffset){top+=parseFloat(jQuery.curCSS(body,"marginTop",true))||0;left+=parseFloat(jQuery.curCSS(body,"marginLeft",true))||0}return{top:top,left:left}},setOffset:function(elem,options,i){if(/static/.test(jQuery.curCSS(elem,"position"))){elem.style.position="relative"}var curElem=jQuery(elem),curOffset=curElem.offset(),curTop=parseInt(jQuery.curCSS(elem,"top",true),10)||0,curLeft=parseInt(jQuery.curCSS(elem,"left",true),10)||0;if(jQuery.isFunction(options)){options=options.call(elem,i,curOffset)}var props={top:(options.top-curOffset.top)+curTop,left:(options.left-curOffset.left)+curLeft};if("using"in options){options.using.call(elem,props)}else{curElem.css(props)}}};jQuery.fn.extend({position:function(){if(!this[0]){return null}var elem=this[0],offsetParent=this.offsetParent(),offset=this.offset(),parentOffset=/^body|html$/i.test(offsetParent[0].nodeName)?{top:0,left:0}:offsetParent.offset();offset.top-=parseFloat(jQuery.curCSS(elem,"marginTop",true))||0;offset.left-=parseFloat(jQuery.curCSS(elem,"marginLeft",true))||0;parentOffset.top+=parseFloat(jQuery.curCSS(offsetParent[0],"borderTopWidth",true))||0;parentOffset.left+=parseFloat(jQuery.curCSS(offsetParent[0],"borderLeftWidth",true))||0;return{top:offset.top-parentOffset.top,left:offset.left-parentOffset.left}},offsetParent:function(){return this.map(function(){var offsetParent=this.offsetParent||document.body;while(offsetParent&&(!/^body|html$/i.test(offsetParent.nodeName)&&jQuery.css(offsetParent,"position")==="static")){offsetParent=offsetParent.offsetParent}return offsetParent})}});jQuery.each(["Left","Top"],function(i,name){var method="scroll"+name;jQuery.fn[method]=function(val){var elem=this[0],win;if(!elem){return null}if(val!==undefined){return this.each(function(){win=getWindow(this);if(win){win.scrollTo(!i?val:jQuery(win).scrollLeft(),i?val:jQuery(win).scrollTop())}else{this[method]=val}})}else{win=getWindow(elem);return win?("pageXOffset"in win)?win[i?"pageYOffset":"pageXOffset"]:jQuery.support.boxModel&&win.document.documentElement[method]||win.document.body[method]:elem[method]}}});function getWindow(elem){return("scrollTo"in elem&&elem.document)?elem:elem.nodeType===9?elem.defaultView||elem.parentWindow:false}jQuery.each(["Height","Width"],function(i,name){var type=name.toLowerCase();jQuery.fn["inner"+name]=function(){return this[0]?jQuery.css(this[0],type,false,"padding"):null};jQuery.fn["outer"+name]=function(margin){return this[0]?jQuery.css(this[0],type,false,margin?"margin":"border"):null};jQuery.fn[type]=function(size){var elem=this[0];if(!elem){return size==null?null:this}if(jQuery.isFunction(size)){return this.each(function(i){var self=jQuery(this);self[type](size.call(this,i,self[type]()))})}return("scrollTo"in elem&&elem.document)?elem.document.compatMode==="CSS1Compat"&&elem.document.documentElement["client"+name]||elem.document.body["client"+name]:(elem.nodeType===9)?Math.max(elem.documentElement["client"+name],elem.body["scroll"+name],elem.documentElement["scroll"+name],elem.body["offset"+name],elem.documentElement["offset"+name]):size===undefined?jQuery.css(elem,type):this.css(type,typeof size==="string"?size:size+"px")}});window.jQuery=window.$=jQuery})(window);

/*!
 * jQuery UI 1.8.6
 *
 * Copyright 2010, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI
 */
(function(c,j){function k(a){return!c(a).parents().andSelf().filter(function(){return c.curCSS(this,"visibility")==="hidden"||c.expr.filters.hidden(this)}).length}c.ui=c.ui||{};if(!c.ui.version){c.extend(c.ui,{version:"1.8.6",keyCode:{ALT:18,BACKSPACE:8,CAPS_LOCK:20,COMMA:188,COMMAND:91,COMMAND_LEFT:91,COMMAND_RIGHT:93,CONTROL:17,DELETE:46,DOWN:40,END:35,ENTER:13,ESCAPE:27,HOME:36,INSERT:45,LEFT:37,MENU:93,NUMPAD_ADD:107,NUMPAD_DECIMAL:110,NUMPAD_DIVIDE:111,NUMPAD_ENTER:108,NUMPAD_MULTIPLY:106,
NUMPAD_SUBTRACT:109,PAGE_DOWN:34,PAGE_UP:33,PERIOD:190,RIGHT:39,SHIFT:16,SPACE:32,TAB:9,UP:38,WINDOWS:91}});c.fn.extend({_focus:c.fn.focus,focus:function(a,b){return typeof a==="number"?this.each(function(){var d=this;setTimeout(function(){c(d).focus();b&&b.call(d)},a)}):this._focus.apply(this,arguments)},scrollParent:function(){var a;a=c.browser.msie&&/(static|relative)/.test(this.css("position"))||/absolute/.test(this.css("position"))?this.parents().filter(function(){return/(relative|absolute|fixed)/.test(c.curCSS(this,
"position",1))&&/(auto|scroll)/.test(c.curCSS(this,"overflow",1)+c.curCSS(this,"overflow-y",1)+c.curCSS(this,"overflow-x",1))}).eq(0):this.parents().filter(function(){return/(auto|scroll)/.test(c.curCSS(this,"overflow",1)+c.curCSS(this,"overflow-y",1)+c.curCSS(this,"overflow-x",1))}).eq(0);return/fixed/.test(this.css("position"))||!a.length?c(document):a},zIndex:function(a){if(a!==j)return this.css("zIndex",a);if(this.length){a=c(this[0]);for(var b;a.length&&a[0]!==document;){b=a.css("position");
if(b==="absolute"||b==="relative"||b==="fixed"){b=parseInt(a.css("zIndex"),10);if(!isNaN(b)&&b!==0)return b}a=a.parent()}}return 0},disableSelection:function(){return this.bind((c.support.selectstart?"selectstart":"mousedown")+".ui-disableSelection",function(a){a.preventDefault()})},enableSelection:function(){return this.unbind(".ui-disableSelection")}});c.each(["Width","Height"],function(a,b){function d(f,g,l,m){c.each(e,function(){g-=parseFloat(c.curCSS(f,"padding"+this,true))||0;if(l)g-=parseFloat(c.curCSS(f,
"border"+this+"Width",true))||0;if(m)g-=parseFloat(c.curCSS(f,"margin"+this,true))||0});return g}var e=b==="Width"?["Left","Right"]:["Top","Bottom"],h=b.toLowerCase(),i={innerWidth:c.fn.innerWidth,innerHeight:c.fn.innerHeight,outerWidth:c.fn.outerWidth,outerHeight:c.fn.outerHeight};c.fn["inner"+b]=function(f){if(f===j)return i["inner"+b].call(this);return this.each(function(){c(this).css(h,d(this,f)+"px")})};c.fn["outer"+b]=function(f,g){if(typeof f!=="number")return i["outer"+b].call(this,f);return this.each(function(){c(this).css(h,
d(this,f,true,g)+"px")})}});c.extend(c.expr[":"],{data:function(a,b,d){return!!c.data(a,d[3])},focusable:function(a){var b=a.nodeName.toLowerCase(),d=c.attr(a,"tabindex");if("area"===b){b=a.parentNode;d=b.name;if(!a.href||!d||b.nodeName.toLowerCase()!=="map")return false;a=c("img[usemap=#"+d+"]")[0];return!!a&&k(a)}return(/input|select|textarea|button|object/.test(b)?!a.disabled:"a"==b?a.href||!isNaN(d):!isNaN(d))&&k(a)},tabbable:function(a){var b=c.attr(a,"tabindex");return(isNaN(b)||b>=0)&&c(a).is(":focusable")}});
c(function(){var a=document.body,b=a.appendChild(b=document.createElement("div"));c.extend(b.style,{minHeight:"100px",height:"auto",padding:0,borderWidth:0});c.support.minHeight=b.offsetHeight===100;c.support.selectstart="onselectstart"in b;a.removeChild(b).style.display="none"});c.extend(c.ui,{plugin:{add:function(a,b,d){a=c.ui[a].prototype;for(var e in d){a.plugins[e]=a.plugins[e]||[];a.plugins[e].push([b,d[e]])}},call:function(a,b,d){if((b=a.plugins[b])&&a.element[0].parentNode)for(var e=0;e<b.length;e++)a.options[b[e][0]]&&
b[e][1].apply(a.element,d)}},contains:function(a,b){return document.compareDocumentPosition?a.compareDocumentPosition(b)&16:a!==b&&a.contains(b)},hasScroll:function(a,b){if(c(a).css("overflow")==="hidden")return false;b=b&&b==="left"?"scrollLeft":"scrollTop";var d=false;if(a[b]>0)return true;a[b]=1;d=a[b]>0;a[b]=0;return d},isOverAxis:function(a,b,d){return a>b&&a<b+d},isOver:function(a,b,d,e,h,i){return c.ui.isOverAxis(a,d,h)&&c.ui.isOverAxis(b,e,i)}})}})(jQuery);
;/*!
 * jQuery UI Widget 1.8.6
 *
 * Copyright 2010, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI/Widget
 */
(function(b,j){if(b.cleanData){var k=b.cleanData;b.cleanData=function(a){for(var c=0,d;(d=a[c])!=null;c++)b(d).triggerHandler("remove");k(a)}}else{var l=b.fn.remove;b.fn.remove=function(a,c){return this.each(function(){if(!c)if(!a||b.filter(a,[this]).length)b("*",this).add([this]).each(function(){b(this).triggerHandler("remove")});return l.call(b(this),a,c)})}}b.widget=function(a,c,d){var e=a.split(".")[0],f;a=a.split(".")[1];f=e+"-"+a;if(!d){d=c;c=b.Widget}b.expr[":"][f]=function(h){return!!b.data(h,
a)};b[e]=b[e]||{};b[e][a]=function(h,g){arguments.length&&this._createWidget(h,g)};c=new c;c.options=b.extend(true,{},c.options);b[e][a].prototype=b.extend(true,c,{namespace:e,widgetName:a,widgetEventPrefix:b[e][a].prototype.widgetEventPrefix||a,widgetBaseClass:f},d);b.widget.bridge(a,b[e][a])};b.widget.bridge=function(a,c){b.fn[a]=function(d){var e=typeof d==="string",f=Array.prototype.slice.call(arguments,1),h=this;d=!e&&f.length?b.extend.apply(null,[true,d].concat(f)):d;if(e&&d.charAt(0)==="_")return h;
e?this.each(function(){var g=b.data(this,a),i=g&&b.isFunction(g[d])?g[d].apply(g,f):g;if(i!==g&&i!==j){h=i;return false}}):this.each(function(){var g=b.data(this,a);g?g.option(d||{})._init():b.data(this,a,new c(d,this))});return h}};b.Widget=function(a,c){arguments.length&&this._createWidget(a,c)};b.Widget.prototype={widgetName:"widget",widgetEventPrefix:"",options:{disabled:false},_createWidget:function(a,c){b.data(c,this.widgetName,this);this.element=b(c);this.options=b.extend(true,{},this.options,
this._getCreateOptions(),a);var d=this;this.element.bind("remove."+this.widgetName,function(){d.destroy()});this._create();this._trigger("create");this._init()},_getCreateOptions:function(){return b.metadata&&b.metadata.get(this.element[0])[this.widgetName]},_create:function(){},_init:function(){},destroy:function(){this.element.unbind("."+this.widgetName).removeData(this.widgetName);this.widget().unbind("."+this.widgetName).removeAttr("aria-disabled").removeClass(this.widgetBaseClass+"-disabled ui-state-disabled")},
widget:function(){return this.element},option:function(a,c){var d=a;if(arguments.length===0)return b.extend({},this.options);if(typeof a==="string"){if(c===j)return this.options[a];d={};d[a]=c}this._setOptions(d);return this},_setOptions:function(a){var c=this;b.each(a,function(d,e){c._setOption(d,e)});return this},_setOption:function(a,c){this.options[a]=c;if(a==="disabled")this.widget()[c?"addClass":"removeClass"](this.widgetBaseClass+"-disabled ui-state-disabled").attr("aria-disabled",c);return this},
enable:function(){return this._setOption("disabled",false)},disable:function(){return this._setOption("disabled",true)},_trigger:function(a,c,d){var e=this.options[a];c=b.Event(c);c.type=(a===this.widgetEventPrefix?a:this.widgetEventPrefix+a).toLowerCase();d=d||{};if(c.originalEvent){a=b.event.props.length;for(var f;a;){f=b.event.props[--a];c[f]=c.originalEvent[f]}}this.element.trigger(c,d);return!(b.isFunction(e)&&e.call(this.element[0],c,d)===false||c.isDefaultPrevented())}}})(jQuery);
;/*
 * jQuery UI Position 1.8.6
 *
 * Copyright 2010, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI/Position
 */
(function(c){c.ui=c.ui||{};var n=/left|center|right/,o=/top|center|bottom/,t=c.fn.position,u=c.fn.offset;c.fn.position=function(b){if(!b||!b.of)return t.apply(this,arguments);b=c.extend({},b);var a=c(b.of),d=a[0],g=(b.collision||"flip").split(" "),e=b.offset?b.offset.split(" "):[0,0],h,k,j;if(d.nodeType===9){h=a.width();k=a.height();j={top:0,left:0}}else if(d.setTimeout){h=a.width();k=a.height();j={top:a.scrollTop(),left:a.scrollLeft()}}else if(d.preventDefault){b.at="left top";h=k=0;j={top:b.of.pageY,
left:b.of.pageX}}else{h=a.outerWidth();k=a.outerHeight();j=a.offset()}c.each(["my","at"],function(){var f=(b[this]||"").split(" ");if(f.length===1)f=n.test(f[0])?f.concat(["center"]):o.test(f[0])?["center"].concat(f):["center","center"];f[0]=n.test(f[0])?f[0]:"center";f[1]=o.test(f[1])?f[1]:"center";b[this]=f});if(g.length===1)g[1]=g[0];e[0]=parseInt(e[0],10)||0;if(e.length===1)e[1]=e[0];e[1]=parseInt(e[1],10)||0;if(b.at[0]==="right")j.left+=h;else if(b.at[0]==="center")j.left+=h/2;if(b.at[1]==="bottom")j.top+=
k;else if(b.at[1]==="center")j.top+=k/2;j.left+=e[0];j.top+=e[1];return this.each(function(){var f=c(this),l=f.outerWidth(),m=f.outerHeight(),p=parseInt(c.curCSS(this,"marginLeft",true))||0,q=parseInt(c.curCSS(this,"marginTop",true))||0,v=l+p+parseInt(c.curCSS(this,"marginRight",true))||0,w=m+q+parseInt(c.curCSS(this,"marginBottom",true))||0,i=c.extend({},j),r;if(b.my[0]==="right")i.left-=l;else if(b.my[0]==="center")i.left-=l/2;if(b.my[1]==="bottom")i.top-=m;else if(b.my[1]==="center")i.top-=m/2;
i.left=parseInt(i.left);i.top=parseInt(i.top);r={left:i.left-p,top:i.top-q};c.each(["left","top"],function(s,x){c.ui.position[g[s]]&&c.ui.position[g[s]][x](i,{targetWidth:h,targetHeight:k,elemWidth:l,elemHeight:m,collisionPosition:r,collisionWidth:v,collisionHeight:w,offset:e,my:b.my,at:b.at})});c.fn.bgiframe&&f.bgiframe();f.offset(c.extend(i,{using:b.using}))})};c.ui.position={fit:{left:function(b,a){var d=c(window);d=a.collisionPosition.left+a.collisionWidth-d.width()-d.scrollLeft();b.left=d>0?
b.left-d:Math.max(b.left-a.collisionPosition.left,b.left)},top:function(b,a){var d=c(window);d=a.collisionPosition.top+a.collisionHeight-d.height()-d.scrollTop();b.top=d>0?b.top-d:Math.max(b.top-a.collisionPosition.top,b.top)}},flip:{left:function(b,a){if(a.at[0]!=="center"){var d=c(window);d=a.collisionPosition.left+a.collisionWidth-d.width()-d.scrollLeft();var g=a.my[0]==="left"?-a.elemWidth:a.my[0]==="right"?a.elemWidth:0,e=a.at[0]==="left"?a.targetWidth:-a.targetWidth,h=-2*a.offset[0];b.left+=
a.collisionPosition.left<0?g+e+h:d>0?g+e+h:0}},top:function(b,a){if(a.at[1]!=="center"){var d=c(window);d=a.collisionPosition.top+a.collisionHeight-d.height()-d.scrollTop();var g=a.my[1]==="top"?-a.elemHeight:a.my[1]==="bottom"?a.elemHeight:0,e=a.at[1]==="top"?a.targetHeight:-a.targetHeight,h=-2*a.offset[1];b.top+=a.collisionPosition.top<0?g+e+h:d>0?g+e+h:0}}}};if(!c.offset.setOffset){c.offset.setOffset=function(b,a){if(/static/.test(c.curCSS(b,"position")))b.style.position="relative";var d=c(b),
g=d.offset(),e=parseInt(c.curCSS(b,"top",true),10)||0,h=parseInt(c.curCSS(b,"left",true),10)||0;g={top:a.top-g.top+e,left:a.left-g.left+h};"using"in a?a.using.call(b,g):d.css(g)};c.fn.offset=function(b){var a=this[0];if(!a||!a.ownerDocument)return null;if(b)return this.each(function(){c.offset.setOffset(this,b)});return u.call(this)}}})(jQuery);
;/*
 * jQuery UI Dialog 1.8.6
 *
 * Copyright 2010, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI/Dialog
 *
 * Depends:
 *	jquery.ui.core.js
 *	jquery.ui.widget.js
 *  jquery.ui.button.js
 *	jquery.ui.draggable.js
 *	jquery.ui.mouse.js
 *	jquery.ui.position.js
 *	jquery.ui.resizable.js
 */
(function(c,j){var k={buttons:true,height:true,maxHeight:true,maxWidth:true,minHeight:true,minWidth:true,width:true},l={maxHeight:true,maxWidth:true,minHeight:true,minWidth:true};c.widget("ui.dialog",{options:{autoOpen:true,buttons:{},closeOnEscape:true,closeText:"close",dialogClass:"",draggable:true,hide:null,height:"auto",maxHeight:false,maxWidth:false,minHeight:150,minWidth:150,modal:false,position:{my:"center",at:"center",of:window,collision:"fit",using:function(a){var b=c(this).css(a).offset().top;
b<0&&c(this).css("top",a.top-b)}},resizable:true,show:null,stack:true,title:"",width:300,zIndex:1E3},_create:function(){this.originalTitle=this.element.attr("title");if(typeof this.originalTitle!=="string")this.originalTitle="";this.options.title=this.options.title||this.originalTitle;var a=this,b=a.options,d=b.title||"&#160;",e=c.ui.dialog.getTitleId(a.element),g=(a.uiDialog=c("<div></div>")).appendTo(document.body).hide().addClass("ui-dialog ui-widget ui-widget-content ui-corner-all "+b.dialogClass).css({zIndex:b.zIndex}).attr("tabIndex",
-1).css("outline",0).keydown(function(i){if(b.closeOnEscape&&i.keyCode&&i.keyCode===c.ui.keyCode.ESCAPE){a.close(i);i.preventDefault()}}).attr({role:"dialog","aria-labelledby":e}).mousedown(function(i){a.moveToTop(false,i)});a.element.show().removeAttr("title").addClass("ui-dialog-content ui-widget-content").appendTo(g);var f=(a.uiDialogTitlebar=c("<div></div>")).addClass("ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix").prependTo(g),h=c('<a href="#"></a>').addClass("ui-dialog-titlebar-close ui-corner-all").attr("role",
"button").hover(function(){h.addClass("ui-state-hover")},function(){h.removeClass("ui-state-hover")}).focus(function(){h.addClass("ui-state-focus")}).blur(function(){h.removeClass("ui-state-focus")}).click(function(i){a.close(i);return false}).appendTo(f);(a.uiDialogTitlebarCloseText=c("<span></span>")).addClass("ui-icon ui-icon-closethick").text(b.closeText).appendTo(h);c("<span></span>").addClass("ui-dialog-title").attr("id",e).html(d).prependTo(f);if(c.isFunction(b.beforeclose)&&!c.isFunction(b.beforeClose))b.beforeClose=
b.beforeclose;f.find("*").add(f).disableSelection();b.draggable&&c.fn.draggable&&a._makeDraggable();b.resizable&&c.fn.resizable&&a._makeResizable();a._createButtons(b.buttons);a._isOpen=false;c.fn.bgiframe&&g.bgiframe()},_init:function(){this.options.autoOpen&&this.open()},destroy:function(){var a=this;a.overlay&&a.overlay.destroy();a.uiDialog.hide();a.element.unbind(".dialog").removeData("dialog").removeClass("ui-dialog-content ui-widget-content").hide().appendTo("body");a.uiDialog.remove();a.originalTitle&&
a.element.attr("title",a.originalTitle);return a},widget:function(){return this.uiDialog},close:function(a){var b=this,d;if(false!==b._trigger("beforeClose",a)){b.overlay&&b.overlay.destroy();b.uiDialog.unbind("keypress.ui-dialog");b._isOpen=false;if(b.options.hide)b.uiDialog.hide(b.options.hide,function(){b._trigger("close",a)});else{b.uiDialog.hide();b._trigger("close",a)}c.ui.dialog.overlay.resize();if(b.options.modal){d=0;c(".ui-dialog").each(function(){if(this!==b.uiDialog[0])d=Math.max(d,c(this).css("z-index"))});
c.ui.dialog.maxZ=d}return b}},isOpen:function(){return this._isOpen},moveToTop:function(a,b){var d=this,e=d.options;if(e.modal&&!a||!e.stack&&!e.modal)return d._trigger("focus",b);if(e.zIndex>c.ui.dialog.maxZ)c.ui.dialog.maxZ=e.zIndex;if(d.overlay){c.ui.dialog.maxZ+=1;d.overlay.$el.css("z-index",c.ui.dialog.overlay.maxZ=c.ui.dialog.maxZ)}a={scrollTop:d.element.attr("scrollTop"),scrollLeft:d.element.attr("scrollLeft")};c.ui.dialog.maxZ+=1;d.uiDialog.css("z-index",c.ui.dialog.maxZ);d.element.attr(a);
d._trigger("focus",b);return d},open:function(){if(!this._isOpen){var a=this,b=a.options,d=a.uiDialog;a.overlay=b.modal?new c.ui.dialog.overlay(a):null;a._size();a._position(b.position);d.show(b.show);a.moveToTop(true);b.modal&&d.bind("keypress.ui-dialog",function(e){if(e.keyCode===c.ui.keyCode.TAB){var g=c(":tabbable",this),f=g.filter(":first");g=g.filter(":last");if(e.target===g[0]&&!e.shiftKey){f.focus(1);return false}else if(e.target===f[0]&&e.shiftKey){g.focus(1);return false}}});c(a.element.find(":tabbable").get().concat(d.find(".ui-dialog-buttonpane :tabbable").get().concat(d.get()))).eq(0).focus();
a._isOpen=true;a._trigger("open");return a}},_createButtons:function(a){var b=this,d=false,e=c("<div></div>").addClass("ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"),g=c("<div></div>").addClass("ui-dialog-buttonset").appendTo(e);b.uiDialog.find(".ui-dialog-buttonpane").remove();typeof a==="object"&&a!==null&&c.each(a,function(){return!(d=true)});if(d){c.each(a,function(f,h){h=c.isFunction(h)?{click:h,text:f}:h;f=c('<button type="button"></button>').attr(h,true).unbind("click").click(function(){h.click.apply(b.element[0],
arguments)}).appendTo(g);c.fn.button&&f.button()});e.appendTo(b.uiDialog)}},_makeDraggable:function(){function a(f){return{position:f.position,offset:f.offset}}var b=this,d=b.options,e=c(document),g;b.uiDialog.draggable({cancel:".ui-dialog-content, .ui-dialog-titlebar-close",handle:".ui-dialog-titlebar",containment:"document",start:function(f,h){g=d.height==="auto"?"auto":c(this).height();c(this).height(c(this).height()).addClass("ui-dialog-dragging");b._trigger("dragStart",f,a(h))},drag:function(f,
h){b._trigger("drag",f,a(h))},stop:function(f,h){d.position=[h.position.left-e.scrollLeft(),h.position.top-e.scrollTop()];c(this).removeClass("ui-dialog-dragging").height(g);b._trigger("dragStop",f,a(h));c.ui.dialog.overlay.resize()}})},_makeResizable:function(a){function b(f){return{originalPosition:f.originalPosition,originalSize:f.originalSize,position:f.position,size:f.size}}a=a===j?this.options.resizable:a;var d=this,e=d.options,g=d.uiDialog.css("position");a=typeof a==="string"?a:"n,e,s,w,se,sw,ne,nw";
d.uiDialog.resizable({cancel:".ui-dialog-content",containment:"document",alsoResize:d.element,maxWidth:e.maxWidth,maxHeight:e.maxHeight,minWidth:e.minWidth,minHeight:d._minHeight(),handles:a,start:function(f,h){c(this).addClass("ui-dialog-resizing");d._trigger("resizeStart",f,b(h))},resize:function(f,h){d._trigger("resize",f,b(h))},stop:function(f,h){c(this).removeClass("ui-dialog-resizing");e.height=c(this).height();e.width=c(this).width();d._trigger("resizeStop",f,b(h));c.ui.dialog.overlay.resize()}}).css("position",
g).find(".ui-resizable-se").addClass("ui-icon ui-icon-grip-diagonal-se")},_minHeight:function(){var a=this.options;return a.height==="auto"?a.minHeight:Math.min(a.minHeight,a.height)},_position:function(a){var b=[],d=[0,0],e;if(a){if(typeof a==="string"||typeof a==="object"&&"0"in a){b=a.split?a.split(" "):[a[0],a[1]];if(b.length===1)b[1]=b[0];c.each(["left","top"],function(g,f){if(+b[g]===b[g]){d[g]=b[g];b[g]=f}});a={my:b.join(" "),at:b.join(" "),offset:d.join(" ")}}a=c.extend({},c.ui.dialog.prototype.options.position,
a)}else a=c.ui.dialog.prototype.options.position;(e=this.uiDialog.is(":visible"))||this.uiDialog.show();this.uiDialog.css({top:0,left:0}).position(a);e||this.uiDialog.hide()},_setOptions:function(a){var b=this,d={},e=false;c.each(a,function(g,f){b._setOption(g,f);if(g in k)e=true;if(g in l)d[g]=f});e&&this._size();this.uiDialog.is(":data(resizable)")&&this.uiDialog.resizable("option",d)},_setOption:function(a,b){var d=this,e=d.uiDialog;switch(a){case "beforeclose":a="beforeClose";break;case "buttons":d._createButtons(b);
break;case "closeText":d.uiDialogTitlebarCloseText.text(""+b);break;case "dialogClass":e.removeClass(d.options.dialogClass).addClass("ui-dialog ui-widget ui-widget-content ui-corner-all "+b);break;case "disabled":b?e.addClass("ui-dialog-disabled"):e.removeClass("ui-dialog-disabled");break;case "draggable":var g=e.is(":data(draggable)");g&&!b&&e.draggable("destroy");!g&&b&&d._makeDraggable();break;case "position":d._position(b);break;case "resizable":(g=e.is(":data(resizable)"))&&!b&&e.resizable("destroy");
g&&typeof b==="string"&&e.resizable("option","handles",b);!g&&b!==false&&d._makeResizable(b);break;case "title":c(".ui-dialog-title",d.uiDialogTitlebar).html(""+(b||"&#160;"));break}c.Widget.prototype._setOption.apply(d,arguments)},_size:function(){var a=this.options,b,d;this.element.show().css({width:"auto",minHeight:0,height:0});if(a.minWidth>a.width)a.width=a.minWidth;b=this.uiDialog.css({height:"auto",width:a.width}).height();d=Math.max(0,a.minHeight-b);if(a.height==="auto")if(c.support.minHeight)this.element.css({minHeight:d,
height:"auto"});else{this.uiDialog.show();a=this.element.css("height","auto").height();this.uiDialog.hide();this.element.height(Math.max(a,d))}else this.element.height(Math.max(a.height-b,0));this.uiDialog.is(":data(resizable)")&&this.uiDialog.resizable("option","minHeight",this._minHeight())}});c.extend(c.ui.dialog,{version:"1.8.6",uuid:0,maxZ:0,getTitleId:function(a){a=a.attr("id");if(!a){this.uuid+=1;a=this.uuid}return"ui-dialog-title-"+a},overlay:function(a){this.$el=c.ui.dialog.overlay.create(a)}});
c.extend(c.ui.dialog.overlay,{instances:[],oldInstances:[],maxZ:0,events:c.map("focus,mousedown,mouseup,keydown,keypress,click".split(","),function(a){return a+".dialog-overlay"}).join(" "),create:function(a){if(this.instances.length===0){setTimeout(function(){c.ui.dialog.overlay.instances.length&&c(document).bind(c.ui.dialog.overlay.events,function(d){if(c(d.target).zIndex()<c.ui.dialog.overlay.maxZ)return false})},1);c(document).bind("keydown.dialog-overlay",function(d){if(a.options.closeOnEscape&&
d.keyCode&&d.keyCode===c.ui.keyCode.ESCAPE){a.close(d);d.preventDefault()}});c(window).bind("resize.dialog-overlay",c.ui.dialog.overlay.resize)}var b=(this.oldInstances.pop()||c("<div></div>").addClass("ui-widget-overlay")).appendTo(document.body).css({width:this.width(),height:this.height()});c.fn.bgiframe&&b.bgiframe();this.instances.push(b);return b},destroy:function(a){this.oldInstances.push(this.instances.splice(c.inArray(a,this.instances),1)[0]);this.instances.length===0&&c([document,window]).unbind(".dialog-overlay");
a.remove();var b=0;c.each(this.instances,function(){b=Math.max(b,this.css("z-index"))});this.maxZ=b},height:function(){var a,b;if(c.browser.msie&&c.browser.version<7){a=Math.max(document.documentElement.scrollHeight,document.body.scrollHeight);b=Math.max(document.documentElement.offsetHeight,document.body.offsetHeight);return a<b?c(window).height()+"px":a+"px"}else return c(document).height()+"px"},width:function(){var a,b;if(c.browser.msie&&c.browser.version<7){a=Math.max(document.documentElement.scrollWidth,
document.body.scrollWidth);b=Math.max(document.documentElement.offsetWidth,document.body.offsetWidth);return a<b?c(window).width()+"px":a+"px"}else return c(document).width()+"px"},resize:function(){var a=c([]);c.each(c.ui.dialog.overlay.instances,function(){a=a.add(this)});a.css({width:0,height:0}).css({width:c.ui.dialog.overlay.width(),height:c.ui.dialog.overlay.height()})}});c.extend(c.ui.dialog.overlay.prototype,{destroy:function(){c.ui.dialog.overlay.destroy(this.$el)}})})(jQuery);
;/*
 * jQuery UI Tabs 1.8.6
 *
 * Copyright 2010, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI/Tabs
 *
 * Depends:
 *	jquery.ui.core.js
 *	jquery.ui.widget.js
 */
(function(d,p){function u(){return++v}function w(){return++x}var v=0,x=0;d.widget("https://www.usbank.com/homepage/homepage_js/ui.tabs",{options:{add:null,ajaxOptions:null,cache:false,cookie:null,collapsible:false,disable:null,disabled:[],enable:null,event:"click",fx:null,idPrefix:"ui-tabs-",load:null,panelTemplate:"<div></div>",remove:null,select:null,show:null,spinner:"<em>Loading&#8230;</em>",tabTemplate:"<li><a href='#{href}'><span>#{label}</span></a></li>"},_create:function(){this._tabify(true)},_setOption:function(b,e){if(b=="selected")this.options.collapsible&&
e==this.options.selected||this.select(e);else{this.options[b]=e;this._tabify()}},_tabId:function(b){return b.title&&b.title.replace(/\s/g,"_").replace(/[^\w\u00c0-\uFFFF-]/g,"")||this.options.idPrefix+u()},_sanitizeSelector:function(b){return b.replace(/:/g,"\\:")},_cookie:function(){var b=this.cookie||(this.cookie=this.options.cookie.name||"ui-tabs-"+w());return d.cookie.apply(null,[b].concat(d.makeArray(arguments)))},_ui:function(b,e){return{tab:b,panel:e,index:this.anchors.index(b)}},_cleanup:function(){this.lis.filter(".ui-state-processing").removeClass("ui-state-processing").find("span:data(label.tabs)").each(function(){var b=
d(this);b.html(b.data("https://www.usbank.com/homepage/homepage_js/label.tabs")).removeData("https://www.usbank.com/homepage/homepage_js/label.tabs")})},_tabify:function(b){function e(g,f){g.css("display","");!d.support.opacity&&f.opacity&&g[0].style.removeAttribute("filter")}var a=this,c=this.options,h=/^#.+/;this.list=this.element.find("ol,ul").eq(0);this.lis=d(" > li:has(a[href])",this.list);this.anchors=this.lis.map(function(){return d("a",this)[0]});this.panels=d([]);this.anchors.each(function(g,f){var i=d(f).attr("href"),l=i.split("#")[0],q;if(l&&(l===location.toString().split("#")[0]||
(q=d("base")[0])&&l===q.href)){i=f.hash;f.href=i}if(h.test(i))a.panels=a.panels.add(a._sanitizeSelector(i));else if(i&&i!=="#"){d.data(f,"https://www.usbank.com/homepage/homepage_js/href.tabs",i);d.data(f,"https://www.usbank.com/homepage/homepage_js/load.tabs",i.replace(/#.*$/,""));i=a._tabId(f);f.href="#"+i;f=d("#"+i);if(!f.length){f=d(c.panelTemplate).attr("id",i).addClass("ui-tabs-panel ui-widget-content ui-corner-bottom").insertAfter(a.panels[g-1]||a.list);f.data("https://www.usbank.com/homepage/homepage_js/destroy.tabs",true)}a.panels=a.panels.add(f)}else c.disabled.push(g)});if(b){this.element.addClass("ui-tabs ui-widget ui-widget-content ui-corner-all");
this.list.addClass("ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all");this.lis.addClass("ui-state-default ui-corner-top");this.panels.addClass("ui-tabs-panel ui-widget-content ui-corner-bottom");if(c.selected===p){location.hash&&this.anchors.each(function(g,f){if(f.hash==location.hash){c.selected=g;return false}});if(typeof c.selected!=="number"&&c.cookie)c.selected=parseInt(a._cookie(),10);if(typeof c.selected!=="number"&&this.lis.filter(".ui-tabs-selected").length)c.selected=
this.lis.index(this.lis.filter(".ui-tabs-selected"));c.selected=c.selected||(this.lis.length?0:-1)}else if(c.selected===null)c.selected=-1;c.selected=c.selected>=0&&this.anchors[c.selected]||c.selected<0?c.selected:0;c.disabled=d.unique(c.disabled.concat(d.map(this.lis.filter(".ui-state-disabled"),function(g){return a.lis.index(g)}))).sort();d.inArray(c.selected,c.disabled)!=-1&&c.disabled.splice(d.inArray(c.selected,c.disabled),1);this.panels.addClass("ui-tabs-hide");this.lis.removeClass("ui-tabs-selected ui-state-active");
if(c.selected>=0&&this.anchors.length){d(a._sanitizeSelector(a.anchors[c.selected].hash)).removeClass("ui-tabs-hide");this.lis.eq(c.selected).addClass("ui-tabs-selected ui-state-active");a.element.queue("tabs",function(){a._trigger("show",null,a._ui(a.anchors[c.selected],d(a._sanitizeSelector(a.anchors[c.selected].hash))))});this.load(c.selected)}d(window).bind("unload",function(){a.lis.add(a.anchors).unbind(".tabs");a.lis=a.anchors=a.panels=null})}else c.selected=this.lis.index(this.lis.filter(".ui-tabs-selected"));
this.element[c.collapsible?"addClass":"removeClass"]("ui-tabs-collapsible");c.cookie&&this._cookie(c.selected,c.cookie);b=0;for(var j;j=this.lis[b];b++)d(j)[d.inArray(b,c.disabled)!=-1&&!d(j).hasClass("ui-tabs-selected")?"addClass":"removeClass"]("ui-state-disabled");c.cache===false&&this.anchors.removeData("https://www.usbank.com/homepage/homepage_js/cache.tabs");this.lis.add(this.anchors).unbind(".tabs");if(c.event!=="mouseover"){var k=function(g,f){f.is(":not(.ui-state-disabled)")&&f.addClass("ui-state-"+g)},n=function(g,f){f.removeClass("ui-state-"+
g)};this.lis.bind("https://www.usbank.com/homepage/homepage_js/mouseover.tabs",function(){k("hover",d(this))});this.lis.bind("https://www.usbank.com/homepage/homepage_js/mouseout.tabs",function(){n("hover",d(this))});this.anchors.bind("https://www.usbank.com/homepage/homepage_js/focus.tabs",function(){k("focus",d(this).closest("li"))});this.anchors.bind("https://www.usbank.com/homepage/homepage_js/blur.tabs",function(){n("focus",d(this).closest("li"))})}var m,o;if(c.fx)if(d.isArray(c.fx)){m=c.fx[0];o=c.fx[1]}else m=o=c.fx;var r=o?function(g,f){d(g).closest("li").addClass("ui-tabs-selected ui-state-active");f.hide().removeClass("ui-tabs-hide").animate(o,o.duration||"normal",
function(){e(f,o);a._trigger("show",null,a._ui(g,f[0]))})}:function(g,f){d(g).closest("li").addClass("ui-tabs-selected ui-state-active");f.removeClass("ui-tabs-hide");a._trigger("show",null,a._ui(g,f[0]))},s=m?function(g,f){f.animate(m,m.duration||"normal",function(){a.lis.removeClass("ui-tabs-selected ui-state-active");f.addClass("ui-tabs-hide");e(f,m);a.element.dequeue("tabs")})}:function(g,f){a.lis.removeClass("ui-tabs-selected ui-state-active");f.addClass("ui-tabs-hide");a.element.dequeue("tabs")};
this.anchors.bind(c.event+".tabs",function(){var g=this,f=d(g).closest("li"),i=a.panels.filter(":not(.ui-tabs-hide)"),l=d(a._sanitizeSelector(g.hash));if(f.hasClass("ui-tabs-selected")&&!c.collapsible||f.hasClass("ui-state-disabled")||f.hasClass("ui-state-processing")||a.panels.filter(":animated").length||a._trigger("select",null,a._ui(this,l[0]))===false){this.blur();return false}c.selected=a.anchors.index(this);a.abort();if(c.collapsible)if(f.hasClass("ui-tabs-selected")){c.selected=-1;c.cookie&&
a._cookie(c.selected,c.cookie);a.element.queue("tabs",function(){s(g,i)}).dequeue("tabs");this.blur();return false}else if(!i.length){c.cookie&&a._cookie(c.selected,c.cookie);a.element.queue("tabs",function(){r(g,l)});a.load(a.anchors.index(this));this.blur();return false}c.cookie&&a._cookie(c.selected,c.cookie);if(l.length){i.length&&a.element.queue("tabs",function(){s(g,i)});a.element.queue("tabs",function(){r(g,l)});a.load(a.anchors.index(this))}else throw"jQuery UI Tabs: Mismatching fragment identifier.";
d.browser.msie&&this.blur()});this.anchors.bind("https://www.usbank.com/homepage/homepage_js/click.tabs",function(){return false})},_getIndex:function(b){if(typeof b=="string")b=this.anchors.index(this.anchors.filter("[href$="+b+"]"));return b},destroy:function(){var b=this.options;this.abort();this.element.unbind(".tabs").removeClass("ui-tabs ui-widget ui-widget-content ui-corner-all ui-tabs-collapsible").removeData("tabs");this.list.removeClass("ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all");this.anchors.each(function(){var e=
d.data(this,"https://www.usbank.com/homepage/homepage_js/href.tabs");if(e)this.href=e;var a=d(this).unbind(".tabs");d.each(["href","load","cache"],function(c,h){a.removeData(h+".tabs")})});this.lis.unbind(".tabs").add(this.panels).each(function(){d.data(this,"https://www.usbank.com/homepage/homepage_js/destroy.tabs")?d(this).remove():d(this).removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active ui-state-hover ui-state-focus ui-state-disabled ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide")});b.cookie&&this._cookie(null,b.cookie);return this},add:function(b,
e,a){if(a===p)a=this.anchors.length;var c=this,h=this.options;e=d(h.tabTemplate.replace(/#\{href\}/g,b).replace(/#\{label\}/g,e));b=!b.indexOf("#")?b.replace("#",""):this._tabId(d("a",e)[0]);e.addClass("ui-state-default ui-corner-top").data("https://www.usbank.com/homepage/homepage_js/destroy.tabs",true);var j=d("#"+b);j.length||(j=d(h.panelTemplate).attr("id",b).data("https://www.usbank.com/homepage/homepage_js/destroy.tabs",true));j.addClass("ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide");if(a>=this.lis.length){e.appendTo(this.list);j.appendTo(this.list[0].parentNode)}else{e.insertBefore(this.lis[a]);
j.insertBefore(this.panels[a])}h.disabled=d.map(h.disabled,function(k){return k>=a?++k:k});this._tabify();if(this.anchors.length==1){h.selected=0;e.addClass("ui-tabs-selected ui-state-active");j.removeClass("ui-tabs-hide");this.element.queue("tabs",function(){c._trigger("show",null,c._ui(c.anchors[0],c.panels[0]))});this.load(0)}this._trigger("add",null,this._ui(this.anchors[a],this.panels[a]));return this},remove:function(b){b=this._getIndex(b);var e=this.options,a=this.lis.eq(b).remove(),c=this.panels.eq(b).remove();
if(a.hasClass("ui-tabs-selected")&&this.anchors.length>1)this.select(b+(b+1<this.anchors.length?1:-1));e.disabled=d.map(d.grep(e.disabled,function(h){return h!=b}),function(h){return h>=b?--h:h});this._tabify();this._trigger("remove",null,this._ui(a.find("a")[0],c[0]));return this},enable:function(b){b=this._getIndex(b);var e=this.options;if(d.inArray(b,e.disabled)!=-1){this.lis.eq(b).removeClass("ui-state-disabled");e.disabled=d.grep(e.disabled,function(a){return a!=b});this._trigger("enable",null,
this._ui(this.anchors[b],this.panels[b]));return this}},disable:function(b){b=this._getIndex(b);var e=this.options;if(b!=e.selected){this.lis.eq(b).addClass("ui-state-disabled");e.disabled.push(b);e.disabled.sort();this._trigger("disable",null,this._ui(this.anchors[b],this.panels[b]))}return this},select:function(b){b=this._getIndex(b);if(b==-1)if(this.options.collapsible&&this.options.selected!=-1)b=this.options.selected;else return this;this.anchors.eq(b).trigger(this.options.event+".tabs");return this},
load:function(b){b=this._getIndex(b);var e=this,a=this.options,c=this.anchors.eq(b)[0],h=d.data(c,"https://www.usbank.com/homepage/homepage_js/load.tabs");this.abort();if(!h||this.element.queue("tabs").length!==0&&d.data(c,"https://www.usbank.com/homepage/homepage_js/cache.tabs"))this.element.dequeue("tabs");else{this.lis.eq(b).addClass("ui-state-processing");if(a.spinner){var j=d("span",c);j.data("https://www.usbank.com/homepage/homepage_js/label.tabs",j.html()).html(a.spinner)}this.xhr=d.ajax(d.extend({},a.ajaxOptions,{url:h,success:function(k,n){d(e._sanitizeSelector(c.hash)).html(k);e._cleanup();a.cache&&d.data(c,"https://www.usbank.com/homepage/homepage_js/cache.tabs",
true);e._trigger("load",null,e._ui(e.anchors[b],e.panels[b]));try{a.ajaxOptions.success(k,n)}catch(m){}},error:function(k,n){e._cleanup();e._trigger("load",null,e._ui(e.anchors[b],e.panels[b]));try{a.ajaxOptions.error(k,n,b,c)}catch(m){}}}));e.element.dequeue("tabs");return this}},abort:function(){this.element.queue([]);this.panels.stop(false,true);this.element.queue("tabs",this.element.queue("tabs").splice(-2,2));if(this.xhr){this.xhr.abort();delete this.xhr}this._cleanup();return this},url:function(b,
e){this.anchors.eq(b).removeData("https://www.usbank.com/homepage/homepage_js/cache.tabs").data("https://www.usbank.com/homepage/homepage_js/load.tabs",e);return this},length:function(){return this.anchors.length}});d.extend(d.ui.tabs,{version:"1.8.6"});d.extend(d.ui.tabs.prototype,{rotation:null,rotate:function(b,e){var a=this,c=this.options,h=a._rotate||(a._rotate=function(j){clearTimeout(a.rotation);a.rotation=setTimeout(function(){var k=c.selected;a.select(++k<a.anchors.length?k:0)},b);j&&j.stopPropagation()});e=a._unrotate||(a._unrotate=!e?function(j){j.clientX&&a.rotate(null)}:
function(){t=c.selected;h()});if(b){this.element.bind("tabsshow",h);this.anchors.bind(c.event+".tabs",e);h()}else{clearTimeout(a.rotation);this.element.unbind("tabsshow",h);this.anchors.unbind(c.event+".tabs",e);delete this._rotate;delete this._unrotate}return this}})})(jQuery);
;

 /*
 * jQuery UI selectmenu
 *
 * Copyright (c) 2009 AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT (MIT-LICENSE.txt)
 * and GPL (GPL-LICENSE.txt) licenses.
 *
 * http://docs.jquery.com/UI
 */

(function($){$.widget("ui.selectmenu",{widgetEventPrefix:"selectmenu",options:{transferClasses:true,style:'popup',width:null,menuWidth:null,handleWidth:26,maxHeight:null,icons:null,format:null,errorClass:'ui-state-error ui-selectmenu-error'},_init:function(){var self=this,o=this.options;this.ids=[this.element.attr('id')+'-'+'button',this.element.attr('id')+'-'+'menu'];this._safemouseup=true;this.newelement=$('<a class="'+this.widgetBaseClass+' ui-widget ui-state-default ui-corner-all" id="'+this.ids[0]+'" role="button" href="#" aria-haspopup="true" aria-owns="'+this.ids[1]+'"></a>').insertAfter(this.element);var tabindex=this.element.attr('tabindex');if(tabindex){this.newelement.attr('tabindex',tabindex)}this.newelement.data('selectelement',this.element);this.selectmenuIcon=$('<span class="'+this.widgetBaseClass+'-icon ui-icon"></span>').prependTo(this.newelement).addClass((o.style=="popup")?'ui-icon-triangle-2-n-s':'ui-icon-triangle-1-s');$('label[for='+this.element.attr('id')+']').attr('for',this.ids[0]).bind('click',function(){self.newelement[0].focus();return false});this.newelement.bind('mousedown',function(event){self._toggle(event);if(o.style=="popup"){self._safemouseup=false;setTimeout(function(){self._safemouseup=true},300)}return false}).bind('click',function(){return false}).keydown(function(event){var ret=true;switch(event.keyCode){case $.ui.keyCode.ENTER:ret=true;break;case $.ui.keyCode.SPACE:ret=false;self._toggle(event);break;case $.ui.keyCode.UP:case $.ui.keyCode.LEFT:ret=false;self._moveSelection(-1);break;case $.ui.keyCode.DOWN:case $.ui.keyCode.RIGHT:ret=false;self._moveSelection(1);break;case $.ui.keyCode.TAB:ret=true;break;default:ret=false;self._typeAhead(event.keyCode,'mouseup');break}return ret}).bind('mouseover focus',function(){$(this).addClass(self.widgetBaseClass+'-focus ui-state-hover')}).bind('mouseout blur',function(){$(this).removeClass(self.widgetBaseClass+'-focus ui-state-hover')});$(document).mousedown(function(event){self.close(event)});this.element.click(function(){this._refreshValue()}).focus(function(){this.newelement[0].focus()});var cornerClass=(o.style=="dropdown")?" ui-corner-bottom":" ui-corner-all";this.list=$('<ul class="'+self.widgetBaseClass+'-menu ui-widget ui-widget-content'+cornerClass+'" aria-hidden="true" role="listbox" aria-labelledby="'+this.ids[0]+'" id="'+this.ids[1]+'"></ul>').appendTo('body');var selectOptionData=[];this.element.find('option').each(function(){selectOptionData.push({value:$(this).attr('value'),text:self._formatText(jQuery(this).text()),selected:$(this).attr('selected'),classes:$(this).attr('class'),parentOptGroup:$(this).parent('optgroup').attr('label')})});var activeClass=(self.options.style=="popup")?" ui-state-active":"";for(var i in selectOptionData){var thisLi=$('<li role="presentation"><a href="#" tabindex="-1" role="option" aria-selected="false">'+selectOptionData[i].text+'</a></li>').data('index',i).addClass(selectOptionData[i].classes).data('optionClasses',selectOptionData[i].classes||'').mouseup(function(event){if(self._safemouseup){var changed=$(this).data('index')!=self._selectedIndex();self.value($(this).data('index'));self.select(event);if(changed){self.change(event)}self.close(event,true)}return false}).click(function(){return false}).bind('mouseover focus',function(){self._selectedOptionLi().addClass(activeClass);self._focusedOptionLi().removeClass(self.widgetBaseClass+'-item-focus ui-state-hover');$(this).removeClass('ui-state-active').addClass(self.widgetBaseClass+'-item-focus ui-state-hover')}).bind('mouseout blur',function(){if($(this).is(self._selectedOptionLi())){$(this).addClass(activeClass)}$(this).removeClass(self.widgetBaseClass+'-item-focus ui-state-hover')});if(selectOptionData[i].parentOptGroup){var optGroupName=self.widgetBaseClass+'-group-'+selectOptionData[i].parentOptGroup;if(this.list.find('li.'+optGroupName).size()){this.list.find('li.'+optGroupName+':last ul').append(thisLi)}else{$('<li role="presentation" class="'+self.widgetBaseClass+'-group '+optGroupName+'"><span class="'+self.widgetBaseClass+'-group-label">'+selectOptionData[i].parentOptGroup+'</span><ul></ul></li>').appendTo(this.list).find('ul').append(thisLi)}}else{thisLi.appendTo(this.list)}this.list.bind('mousedown mouseup',function(){return false});if(o.icons){for(var j in o.icons){if(thisLi.is(o.icons[j].find)){thisLi.data('optionClasses',selectOptionData[i].classes+' '+self.widgetBaseClass+'-hasIcon').addClass(self.widgetBaseClass+'-hasIcon');var iconClass=o.icons[j].icon||"";thisLi.find('a:eq(0)').prepend('<span class="'+self.widgetBaseClass+'-item-icon ui-icon '+iconClass+'"></span>')}}}}this.list.find('li:last').addClass("ui-corner-bottom");if(o.style=='popup'){this.list.find('li:first').addClass("ui-corner-top")}if(o.transferClasses){var transferClasses=this.element.attr('class')||'';this.newelement.add(this.list).addClass(transferClasses)}var selectWidth=this.element.width();this.newelement.width((o.width)?o.width:selectWidth);if(o.style=='dropdown'){this.list.width((o.menuWidth)?o.menuWidth:((o.width)?o.width:selectWidth))}else{this.list.width((o.menuWidth)?o.menuWidth:((o.width)?o.width-o.handleWidth:selectWidth-o.handleWidth))}if(o.maxHeight&&o.maxHeight<this.list.height()){this.list.height(o.maxHeight)}this._optionLis=this.list.find('li:not(.'+self.widgetBaseClass+'-group)');this.list.keydown(function(event){var ret=true;switch(event.keyCode){case $.ui.keyCode.UP:case $.ui.keyCode.LEFT:ret=false;self._moveFocus(-1);break;case $.ui.keyCode.DOWN:case $.ui.keyCode.RIGHT:ret=false;self._moveFocus(1);break;case $.ui.keyCode.HOME:ret=false;self._moveFocus(':first');break;case $.ui.keyCode.PAGE_UP:ret=false;self._scrollPage('up');break;case $.ui.keyCode.PAGE_DOWN:ret=false;self._scrollPage('down');break;case $.ui.keyCode.END:ret=false;self._moveFocus(':last');break;case $.ui.keyCode.ENTER:case $.ui.keyCode.SPACE:ret=false;self.close(event,true);$(event.target).parents('li:eq(0)').trigger('mouseup');break;case $.ui.keyCode.TAB:ret=true;self.close(event,true);break;case $.ui.keyCode.ESCAPE:ret=false;self.close(event,true);break;default:ret=false;self._typeAhead(event.keyCode,'focus');break}return ret});if(o.style=='dropdown'){this.newelement.addClass(self.widgetBaseClass+"-dropdown");this.list.addClass(self.widgetBaseClass+"-menu-dropdown")}else{this.newelement.addClass(self.widgetBaseClass+"-popup");this.list.addClass(self.widgetBaseClass+"-menu-popup")}this.newelement.prepend('<span class="'+self.widgetBaseClass+'-status">'+selectOptionData[this._selectedIndex()].text+'</span>');this.element.hide();if(this.element.attr('disabled')==true){this.disable()}this.value(this._selectedIndex())},destroy:function(){this.element.removeData(this.widgetName).removeClass(this.widgetBaseClass+'-disabled'+' '+this.namespace+'-state-disabled').removeAttr('aria-disabled');$('label[for='+this.newelement.attr('id')+']').attr('for',this.element.attr('id')).unbind('click');this.newelement.remove();this.list.remove();this.element.show()},_typeAhead:function(code,eventType){var self=this;if(!self._prevChar){self._prevChar=['',0]}var C=String.fromCharCode(code);c=C.toLowerCase();var focusFound=false;function focusOpt(elem,ind){focusFound=true;$(elem).trigger(eventType);self._prevChar[1]=ind};this.list.find('li a').each(function(i){if(!focusFound){var thisText=$(this).text();if(thisText.indexOf(C)==0||thisText.indexOf(c)==0){if(self._prevChar[0]==C){if(self._prevChar[1]<i){focusOpt(this,i)}}else{focusOpt(this,i)}}}});this._prevChar[0]=C},_uiHash:function(){return{value:this.value()}},open:function(event){var self=this;var disabledStatus=this.newelement.attr("aria-disabled");if(disabledStatus!='true'){this._refreshPosition();this._closeOthers(event);this.newelement.addClass('ui-state-active');this.list.appendTo('body').addClass(self.widgetBaseClass+'-open').attr('aria-hidden',false).find('li:not(.'+self.widgetBaseClass+'-group):eq('+this._selectedIndex()+') a')[0].focus();if(this.options.style=="dropdown"){this.newelement.removeClass('ui-corner-all').addClass('ui-corner-top')}this._refreshPosition();this._trigger("open",event,this._uiHash())}},close:function(event,retainFocus){if(this.newelement.is('.ui-state-active')){this.newelement.removeClass('ui-state-active');this.list.attr('aria-hidden',true).removeClass(this.widgetBaseClass+'-open');if(this.options.style=="dropdown"){this.newelement.removeClass('ui-corner-top').addClass('ui-corner-all')}if(retainFocus){this.newelement[0].focus()}this._trigger("close",event,this._uiHash())}},change:function(event){this.element.trigger('change');this._trigger("change",event,this._uiHash())},select:function(event){this._trigger("select",event,this._uiHash())},_closeOthers:function(event){$('.'+this.widgetBaseClass+'.ui-state-active').not(this.newelement).each(function(){$(this).data('selectelement').selectmenu('close',event)});$('.'+this.widgetBaseClass+'.ui-state-hover').trigger('mouseout')},_toggle:function(event,retainFocus){if(this.list.is('.'+this.widgetBaseClass+'-open')){this.close(event,retainFocus)}else{this.open(event)}},_formatText:function(text){return this.options.format?this.options.format(text):text},_selectedIndex:function(){return this.element[0].selectedIndex},_selectedOptionLi:function(){return this._optionLis.eq(this._selectedIndex())},_focusedOptionLi:function(){return this.list.find('.'+this.widgetBaseClass+'-item-focus')},_moveSelection:function(amt){var currIndex=parseInt(this._selectedOptionLi().data('index'),10);var newIndex=currIndex+amt;return this._optionLis.eq(newIndex).trigger('mouseup')},_moveFocus:function(amt){if(!isNaN(amt)){var currIndex=parseInt(this._focusedOptionLi().data('index'),10);var newIndex=currIndex+amt}else{var newIndex=parseInt(this._optionLis.filter(amt).data('index'),10)}if(newIndex<0){newIndex=0}if(newIndex>this._optionLis.size()-1){newIndex=this._optionLis.size()-1}var activeID=this.widgetBaseClass+'-item-'+Math.round(Math.random()*1000);this._focusedOptionLi().find('a:eq(0)').attr('id','');this._optionLis.eq(newIndex).find('a:eq(0)').attr('id',activeID)[0].focus();this.list.attr('aria-activedescendant',activeID)},_scrollPage:function(direction){var numPerPage=Math.floor(this.list.outerHeight()/this.list.find('li:first').outerHeight());numPerPage=(direction=='up')?-numPerPage:numPerPage;this._moveFocus(numPerPage)},_setData:function(key,value){this.options[key]=value;if(key=='disabled'){this.close();this.element.add(this.newelement).add(this.list)[value?'addClass':'removeClass'](this.widgetBaseClass+'-disabled'+' '+this.namespace+'-state-disabled').attr("aria-disabled",value)}},value:function(newValue){if(arguments.length){this.element[0].selectedIndex=newValue;this._refreshValue();this._refreshPosition()}return this.element[0].selectedIndex},_refreshValue:function(){var activeClass=(this.options.style=="popup")?" ui-state-active":"";var activeID=this.widgetBaseClass+'-item-'+Math.round(Math.random()*1000);this.list.find('.'+this.widgetBaseClass+'-item-selected').removeClass(this.widgetBaseClass+"-item-selected"+activeClass).find('a').attr('aria-selected','false').attr('id','');this._selectedOptionLi().addClass(this.widgetBaseClass+"-item-selected"+activeClass).find('a').attr('aria-selected','true').attr('id',activeID);var currentOptionClasses=this.newelement.data('optionClasses')?this.newelement.data('optionClasses'):"";var newOptionClasses=this._selectedOptionLi().data('optionClasses')?this._selectedOptionLi().data('optionClasses'):"";this.newelement.removeClass(currentOptionClasses).data('optionClasses',newOptionClasses).addClass(newOptionClasses).find('.'+this.widgetBaseClass+'-status').html(this._selectedOptionLi().find('a:eq(0)').html());this.list.attr('aria-activedescendant',activeID)},_refreshPosition:function(){this.list.css('left',this.newelement.offset().left);var menuTop=this.newelement.offset().top;var scrolledAmt=this.list[0].scrollTop;this.list.find('li:lt('+this._selectedIndex()+')').each(function(){scrolledAmt-=$(this).outerHeight()});if(this.newelement.is('.'+this.widgetBaseClass+'-popup')){menuTop+=scrolledAmt;this.list.css('top',menuTop)}else{menuTop+=this.newelement.height();this.list.css('top',menuTop)}}});$.extend($.ui.selectmenu,{getter:"value",version:"@VERSION",eventPrefix:"selectmenu",defaults:{transferClasses:true,style:'popup',width:null,menuWidth:null,handleWidth:26,maxHeight:null,icons:null,format:null}})})(jQuery);

/*
 * In-Field Label jQuery Plugin
 * http://fuelyourcoding.com/scripts/infield.html
 *
 * Copyright (c) 2009 Doug Neiner
 * Dual licensed under the MIT and GPL licenses.
 * Uses the same license as jQuery, see:
 * http://docs.jquery.com/License
 *
 * @version 0.1
 */
(function($){$.InFieldLabels=function(label,field,options){var base=this;base.$label=$(label);base.label=label;base.$field=$(field);base.field=field;base.$label.data("InFieldLabels",base);base.showing=true;base.init=function(){base.options=$.extend({},$.InFieldLabels.defaultOptions,options);if(base.$field.val()!=""){base.$label.hide();base.showing=false};base.$field.focus(function(){base.fadeOnFocus()}).blur(function(){base.checkForEmpty(true)}).bind('keydown.infieldlabel',function(e){base.hideOnChange(e)}).change(function(e){base.checkForEmpty()}).bind('onPropertyChange',function(){base.checkForEmpty()})};base.fadeOnFocus=function(){if(base.showing){base.setOpacity(base.options.fadeOpacity)}};base.setOpacity=function(opacity){base.$label.stop().animate({opacity:opacity},base.options.fadeDuration);base.showing=(opacity>0.0)};base.checkForEmpty=function(blur){if(base.$field.val()==""){base.prepForShow();base.setOpacity(blur?1.0:base.options.fadeOpacity)}else{base.setOpacity(0.0)}};base.prepForShow=function(e){if(!base.showing){base.$label.css({opacity:0.0}).show();base.$field.bind('keydown.infieldlabel',function(e){base.hideOnChange(e)})}};base.hideOnChange=function(e){if((e.keyCode==16)||(e.keyCode==9))return;if(base.showing){base.$label.hide();base.showing=false};base.$field.unbind('keydown.infieldlabel')};base.init()};$.InFieldLabels.defaultOptions={fadeOpacity:0.5,fadeDuration:300};$.fn.inFieldLabels=function(options){return this.each(function(){var for_attr=$(this).attr('for');if(!for_attr)return;var $field=$("input#"+for_attr+"[type='text'],"+"input#"+for_attr+"[type='password'],"+"textarea#"+for_attr);if($field.length==0)return;(new $.InFieldLabels(this,$field[0],options))})}})(jQuery);

/**
* hoverIntent r5 // 2007.03.27 // jQuery 1.1.2+
* <http://cherne.net/brian/resources/jquery.hoverIntent.html>
* 
* @param  f  onMouseOver function || An object with configuration options
* @param  g  onMouseOut function  || Nothing (use configuration options object)
* @author    Brian Cherne <brian@cherne.net>
*/
(function($){$.fn.hoverIntent=function(f,g){var cfg={sensitivity:7,interval:100,timeout:0};cfg=$.extend(cfg,g?{over:f,out:g}:f);var cX,cY,pX,pY;var track=function(ev){cX=ev.pageX;cY=ev.pageY;};var compare=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);if((Math.abs(pX-cX)+Math.abs(pY-cY))<cfg.sensitivity){$(ob).unbind("mousemove",track);ob.hoverIntent_s=1;return cfg.over.apply(ob,[ev]);}else{pX=cX;pY=cY;ob.hoverIntent_t=setTimeout(function(){compare(ev,ob);},cfg.interval);}};var delay=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);ob.hoverIntent_s=0;return cfg.out.apply(ob,[ev]);};var handleHover=function(e){var p=(e.type=="mouseover"?e.fromElement:e.toElement)||e.relatedTarget;while(p&&p!=this){try{p=p.parentNode;}catch(e){p=this;}}if(p==this){return false;}var ev=jQuery.extend({},e);var ob=this;if(ob.hoverIntent_t){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);}if(e.type=="mouseover"){pX=ev.pageX;pY=ev.pageY;$(ob).bind("mousemove",track);if(ob.hoverIntent_s!=1){ob.hoverIntent_t=setTimeout(function(){compare(ev,ob);},cfg.interval);}}else{$(ob).unbind("mousemove",track);if(ob.hoverIntent_s==1){ob.hoverIntent_t=setTimeout(function(){delay(ev,ob);},cfg.timeout);}}};return this.mouseover(handleHover).mouseout(handleHover);};})(jQuery);

/*!
 * Swift Carousel, Version 1.0
 * Licensed under GPL version 3
 * http://www.gnu.org/licenses/gpl.txt

 */
(function($){$.fn.featureCarousel=function(options){options=$.extend({},$.fn.featureCarousel.defaults,options||{});return $(this).each(function(){var pluginData={currentCenterNum:options.startingFeature,containerWidth:400,containerHeight:335,largeFeatureWidth:60,largeFeatureHeight:0,smallFeatureWidth:0,smallFeatureHeight:0,totalFeatureCount:$(this).children("div").length,currentlyMoving:false,featuresContainer:$(this),featuresArray:[],containerIDTag:"#"+$(this).attr("id"),timeoutVar:null,rotationsRemaining:0,itemsToAnimate:3,borderWidth:0};preload(function(){setupFeatureDimensions();setupCarousel();setupFeaturePositions();setupBlips();initiateMove(true,1)});function preload(callback){if(options.preload==true){var $imageElements=pluginData.featuresContainer.find("img");var loadedImages=0;var totalImages=$imageElements.length;$imageElements.each(function(){$(this).load(function(){loadedImages++;if(loadedImages==totalImages){callback()}});if(this.complete){$(this).trigger('load')}})}else{callback()}}function getContainer(featureNum){return pluginData.featuresArray[featureNum-1]}function getBySetPos(position){$.each(pluginData.featuresArray,function(){if($(this).data().setPosition==position)return $(this)})}function getPreviousNum(num){if((num-1)==0){return pluginData.totalFeatureCount}else{return num-1}}function getNextNum(num){if((num+1)>pluginData.totalFeatureCount){return 1}else{return num+1}}function setupFeatureDimensions(){pluginData.containerWidth=pluginData.featuresContainer.width();pluginData.containerHeight=pluginData.featuresContainer.height();var $firstFeatureImage=$(pluginData.containerIDTag).find("div img:first");if(options.largeFeatureWidth>1)pluginData.largeFeatureWidth=options.largeFeatureWidth;else if(options.largeFeatureWidth>0&&options.largeFeatureWidth<1)pluginData.largeFeatureWidth=$firstFeatureImage.width()*options.largeFeatureWidth;else pluginData.largeFeatureWidth=$firstFeatureImage.outerWidth();if(options.largeFeatureHeight>1)pluginData.largeFeatureHeight=options.largeFeatureHeight;else if(options.largeFeatureHeight>0&&options.largeFeatureHeight<1)pluginData.largeFeatureHeight=$firstFeatureImage.height()*options.largeFeatureHeight;else pluginData.largeFeatureHeight=$firstFeatureImage.outerHeight();if(options.smallFeatureWidth>1)pluginData.smallFeatureWidth=options.smallFeatureWidth;else if(options.smallFeatureWidth>0&&options.smallFeatureWidth<1)pluginData.smallFeatureWidth=$firstFeatureImage.width()*options.smallFeatureWidth;else pluginData.smallFeatureWidth=$firstFeatureImage.outerWidth()/2;if(options.smallFeatureHeight>1)pluginData.smallFeatureHeight=options.smallFeatureHeight;else if(options.smallFeatureHeight>0&&options.smallFeatureHeight<1)pluginData.smallFeatureHeight=$firstFeatureImage.height()*options.smallFeatureHeight;else pluginData.smallFeatureHeight=$firstFeatureImage.outerHeight()/2}function setupCarousel(){if(options.displayCutoff>0&&options.displayCutoff<pluginData.totalFeatureCount){pluginData.totalFeatureCount=options.displayCutoff}pluginData.featuresContainer.children("div").each(function(index){if(index<pluginData.totalFeatureCount){pluginData.featuresArray[index]=$(this)}});if(pluginData.featuresContainer.children("div").first().css("borderLeftWidth")!="medium"){pluginData.borderWidth=parseInt(pluginData.featuresContainer.children("div").first().css("borderLeftWidth"))*2}pluginData.featuresContainer.children("div").each(function(){$(this).css({'left':(pluginData.containerWidth/2)-(pluginData.smallFeatureWidth/2)-(pluginData.borderWidth/2),'width':pluginData.smallFeatureWidth,'height':pluginData.smallFeatureHeight,'top':options.smallFeatureOffset+options.topPadding,'opacity':0})}).find("img:first").css({'width':pluginData.smallFeatureWidth});if(pluginData.totalFeatureCount<4){pluginData.itemsToAnimate=pluginData.totalFeatureCount}else{pluginData.itemsToAnimate=4}pluginData.featuresContainer.find("div > div").hide()}function setupFeaturePositions(){$.each(pluginData.featuresArray,function(i){$(this).data('setPosition',i+1)});var oneBeforeStarting=getPreviousNum(options.startingFeature);pluginData.currentCenterNum=oneBeforeStarting;var $centerFeature=getContainer(oneBeforeStarting);$centerFeature.data('position',1);var $prevFeatures=$centerFeature.prevAll();$prevFeatures.each(function(i){$(this).data('position',(pluginData.totalFeatureCount-i))});var $nextFeatures=$centerFeature.nextAll();$nextFeatures.each(function(i){if($(this).data('setPosition')!=undefined){$(this).data('position',(i+2))}});if(options.counterStyle==3){$.each(pluginData.featuresArray,function(){var pos=getPreviousNum($(this).data('position'));var $numberTag=$("<span></span>");$numberTag.addClass("numberTag");$numberTag.html("("+pos+" of "+pluginData.totalFeatureCount+")");$(this).find('div p').prepend($numberTag)})}}function setupBlips(){if(options.counterStyle==1||options.counterStyle==2){var $rightb=$("<div class=span-2 last nextButton><a href='javascript:void(0);'><img src='../images/car_next.gif'/*tpa=https://www.usbank.com/homepage/images/car_next.gif*/ alt='Next Carousal' title='Next Carousal' /></a><p class='prepend-top-20px'><img src='../images/shadow_carobtn.jpg'/*tpa=https://www.usbank.com/homepage/images/shadow_carobtn.jpg*/ alt='carousal button shadow' title='carousal button shadow'/></p></div>");$rightb.addClass("nextButtonContainer rightButton");var $leftb=$("<div class=span-2 last prevButton><a href='javascript:void(0);'><img src=images/car_prev.gif alt=Previous Carousal title=Previous Carousal /></a><p class=prepend-top-20px><img src=/homepage/images/shadow_carobtn.jpg alt=carousal button shadow title=carousal button shadow/></p></div>");$leftb.addClass("previousButtonContainer leftButton");for(var i=0;i<pluginData.totalFeatureCount;i++){var counter;if(options.counterStyle==2)counter="";else counter=i+1}$(pluginData.containerIDTag).append($rightb);$rightb.show();$(pluginData.containerIDTag).append($leftb);$leftb.show()}}function changeBlip(oldCenter,newCenter){var $blipsContainer=pluginData.featuresContainer.find(".nextButtonContainer");var $oldCenter=$blipsContainer.find("#blip_"+oldCenter);var $newCenter=$blipsContainer.find("#blip_"+newCenter);$oldCenter.removeClass("blipSelected");$newCenter.addClass("blipSelected")}function autoPlay(){if(pluginData.timeoutVar!=null){pluginData.timeoutVar=clearTimeout(pluginData.timeoutVar)}if(options.autoPlay!=0){var autoTime=(Math.abs(options.autoPlay)<options.carouselSpeed)?options.carouselSpeed:Math.abs(options.autoPlay);pluginData.timeoutVar=setTimeout(function(){if(options.autoPlay>0)initiateMove(true,1);else if(options.autoPlay<0)initiateMove(false,1)},autoTime)}}function rotatePositions(direction){$.each(pluginData.featuresArray,function(){var newPos;if(direction==false){newPos=getNextNum($(this).data().position)}else{newPos=getPreviousNum($(this).data().position)}$(this).data('position',newPos)})}function animateFeature($feature,direction){var new_width,new_height,new_top,new_left,new_zindex,new_padding,new_fade;var oldPosition=$feature.data('position');var newPosition;if(direction==true)newPosition=getPreviousNum(oldPosition);else newPosition=getNextNum(oldPosition);if(newPosition==1){new_width=245;new_height=pluginData.largeFeatureHeight;new_top=options.topPadding;new_zindex=$feature.css("z-index");new_left=100;new_fade=1.0;$feature.removeClass('secondaryRight');$feature.removeClass('secondaryLeft');$feature.find(".description").removeClass('prependAppend-5px');$feature.find(".description").addClass('prependAppend-15px');$feature.addClass('primary')}else{new_width=115;new_height=pluginData.smallFeatureHeight;new_top=options.smallFeatureOffset+options.topPadding;new_zindex=1;new_fade=0.9;if(newPosition==pluginData.totalFeatureCount){new_left=20;$feature.removeClass('secondaryRight');$feature.removeClass('primary');$feature.find(".description").removeClass('prependAppend-15px');$feature.find(".description").addClass('prependAppend-5px');$feature.addClass('secondaryLeft')}else if(newPosition==2){new_left=308;$feature.removeClass('secondaryLeft');$feature.removeClass('primary');$feature.find(".description").removeClass('prependAppend-15px');$feature.find(".description").addClass('prependAppend-5px');$feature.addClass('secondaryRight')}else{new_left=(pluginData.containerWidth/2)-(pluginData.smallFeatureWidth/2)-(pluginData.borderWidth/2);new_fade=0}}if(newPosition!=1){$feature.find("div").hide()}$feature.animate({width:new_width,height:new_height,top:new_top,left:new_left,opacity:new_fade},options.carouselSpeed,options.animationEasing,function(){if(newPosition==1){$feature.find("div").fadeTo("fast",0.85)}pluginData.rotationsRemaining=pluginData.rotationsRemaining-1;$feature.css("z-index",new_zindex);if(options.counterStyle==1||options.counterStyle==2){if(newPosition==1){var newCenterItemNum=pluginData.featuresContainer.children("div").index($feature)+1;var oldCenterItemNum;if(direction==false)oldCenterItemNum=getNextNum(newCenterItemNum);else oldCenterItemNum=getPreviousNum(newCenterItemNum);changeBlip(oldCenterItemNum,newCenterItemNum)}}var divide=pluginData.rotationsRemaining/pluginData.itemsToAnimate;if(divide%1==0){pluginData.currentlyMoving=false;rotatePositions(direction);if(pluginData.rotationsRemaining>0)move(direction)}autoPlay()}).find("img:first").animate({width:new_width,height:new_height},options.carouselSpeed,options.animationEasing).end()}function move(direction){pluginData.currentlyMoving=true;var $newCenter,$newLeft,$newRight,$newHidden;if(direction==true){$newCenter=getContainer(getNextNum(pluginData.currentCenterNum));$newLeft=getContainer(pluginData.currentCenterNum);$newRight=getContainer(getNextNum(getNextNum(pluginData.currentCenterNum)));$newHidden=getContainer(getPreviousNum(pluginData.currentCenterNum));pluginData.currentCenterNum=getNextNum(pluginData.currentCenterNum)}else{$newCenter=getContainer(getPreviousNum(pluginData.currentCenterNum));$newLeft=getContainer(getPreviousNum(getPreviousNum(pluginData.currentCenterNum)));$newRight=getContainer(pluginData.currentCenterNum);$newHidden=getContainer(getNextNum(pluginData.currentCenterNum));pluginData.currentCenterNum=getPreviousNum(pluginData.currentCenterNum)}if(direction){$newLeft.css("z-index",3)}else{$newRight.css("z-index",3)}$newCenter.css("z-index",4);animateFeature($newLeft,direction);animateFeature($newCenter,direction);animateFeature($newRight,direction);if(pluginData.totalFeatureCount>3){animateFeature($newHidden,direction)}}function initiateMove(direction,rotations){if(pluginData.currentlyMoving==false){var queue=rotations*pluginData.itemsToAnimate;pluginData.rotationsRemaining=queue;move(direction)}}function findShortestDistance(from,to){var goingToLeft=1,goingToRight=1,tracker;tracker=from;while((tracker=getPreviousNum(tracker))!=to){goingToLeft++}tracker=from;while((tracker=getNextNum(tracker))!=to){goingToRight++}return(goingToLeft<goingToRight)?goingToLeft*-1:goingToRight}$(".leftButton").click(function(){initiateMove(false,1)});$(".rightButton").click(function(){initiateMove(true,1)});pluginData.featuresContainer.children("div").click(function(){var position=$(this).data('position');if(position==2){initiateMove(true,1)}else if(position==pluginData.totalFeatureCount){initiateMove(false,1)}}).mouseover(function(){if(pluginData.currentlyMoving==false){var position=$(this).data('position');if(position==2||position==pluginData.totalFeatureCount){$(this).css("opacity",0.8)}}}).mouseout(function(){if(pluginData.currentlyMoving==false){var position=$(this).data('position');if(position==2||position==pluginData.totalFeatureCount){$(this).css("opacity",0.9)}}});$("a",pluginData.containerIDTag).live("click",function(event){var $parents=$(this).parentsUntil(pluginData.containerIDTag);$parents.each(function(){var position=$(this).data('position');if(position!=undefined){if(position!=1){if(position==pluginData.totalFeatureCount){initiateMove(false,1)}else if(position==2){initiateMove(true,1)}event.preventDefault();return false}}})});$(".blip").live("click",function(){var goTo=$(this).attr("id").substring(5);var whereIsIt=pluginData.featuresContainer.children("div").eq(goTo-1).data('position');var currentlyAt=pluginData.currentCenterNum;if(goTo!=currentlyAt){var shortest=findShortestDistance(1,whereIsIt);if(shortest<0){initiateMove(false,(shortest*-1))}else{initiateMove(true,shortest)}}})})};$.fn.featureCarousel.defaults={largeFeatureWidth:0,largeFeatureHeight:0,smallFeatureWidth:.5,smallFeatureHeight:.5,topPadding:10,sidePadding:20,smallFeatureOffset:40,startingFeature:1,carouselSpeed:1300,autoPlay:0,counterStyle:1,preload:true,displayCutoff:0,animationEasing:'swing'}})(jQuery);
/*!
 * Swift Carousel, Version 1.0
 * Licensed under GPL version 3
 * http://www.gnu.org/licenses/gpl.txt

 */
(function($){$.fn.featureCarousel=function(options){options=$.extend({},$.fn.featureCarousel.defaults,options||{});return $(this).each(function(){var pluginData={currentCenterNum:options.startingFeature,containerWidth:400,containerHeight:335,largeFeatureWidth:60,largeFeatureHeight:0,smallFeatureWidth:0,smallFeatureHeight:0,totalFeatureCount:$(this).children("div").length,currentlyMoving:false,featuresContainer:$(this),featuresArray:[],containerIDTag:"#"+$(this).attr("id"),timeoutVar:null,rotationsRemaining:0,itemsToAnimate:3,borderWidth:0};preload(function(){setupFeatureDimensions();setupCarousel();setupFeaturePositions();setupBlips();initiateMove(true,1)});function preload(callback){if(options.preload==true){var $imageElements=pluginData.featuresContainer.find("img");var loadedImages=0;var totalImages=$imageElements.length;$imageElements.each(function(){$(this).load(function(){loadedImages++;if(loadedImages==totalImages){callback()}});if(this.complete){$(this).trigger('load')}})}else{callback()}}function getContainer(featureNum){return pluginData.featuresArray[featureNum-1]}function getBySetPos(position){$.each(pluginData.featuresArray,function(){if($(this).data().setPosition==position)return $(this)})}function getPreviousNum(num){if((num-1)==0){return pluginData.totalFeatureCount}else{return num-1}}function getNextNum(num){if((num+1)>pluginData.totalFeatureCount){return 1}else{return num+1}}function setupFeatureDimensions(){pluginData.containerWidth=pluginData.featuresContainer.width();pluginData.containerHeight=pluginData.featuresContainer.height();var $firstFeatureImage=$(pluginData.containerIDTag).find("div img:first");if(options.largeFeatureWidth>1)pluginData.largeFeatureWidth=options.largeFeatureWidth;else if(options.largeFeatureWidth>0&&options.largeFeatureWidth<1)pluginData.largeFeatureWidth=$firstFeatureImage.width()*options.largeFeatureWidth;else pluginData.largeFeatureWidth=$firstFeatureImage.outerWidth();if(options.largeFeatureHeight>1)pluginData.largeFeatureHeight=options.largeFeatureHeight;else if(options.largeFeatureHeight>0&&options.largeFeatureHeight<1)pluginData.largeFeatureHeight=$firstFeatureImage.height()*options.largeFeatureHeight;else pluginData.largeFeatureHeight=$firstFeatureImage.outerHeight();if(options.smallFeatureWidth>1)pluginData.smallFeatureWidth=options.smallFeatureWidth;else if(options.smallFeatureWidth>0&&options.smallFeatureWidth<1)pluginData.smallFeatureWidth=$firstFeatureImage.width()*options.smallFeatureWidth;else pluginData.smallFeatureWidth=$firstFeatureImage.outerWidth()/2;if(options.smallFeatureHeight>1)pluginData.smallFeatureHeight=options.smallFeatureHeight;else if(options.smallFeatureHeight>0&&options.smallFeatureHeight<1)pluginData.smallFeatureHeight=$firstFeatureImage.height()*options.smallFeatureHeight;else pluginData.smallFeatureHeight=$firstFeatureImage.outerHeight()/2}function setupCarousel(){if(options.displayCutoff>0&&options.displayCutoff<pluginData.totalFeatureCount){pluginData.totalFeatureCount=options.displayCutoff}pluginData.featuresContainer.children("div").each(function(index){if(index<pluginData.totalFeatureCount){pluginData.featuresArray[index]=$(this)}});if(pluginData.featuresContainer.children("div").first().css("borderLeftWidth")!="medium"){pluginData.borderWidth=parseInt(pluginData.featuresContainer.children("div").first().css("borderLeftWidth"))*2}pluginData.featuresContainer.children("div").each(function(){$(this).css({'left':(pluginData.containerWidth/2)-(pluginData.smallFeatureWidth/2)-(pluginData.borderWidth/2),'width':pluginData.smallFeatureWidth,'height':pluginData.smallFeatureHeight,'top':options.smallFeatureOffset+options.topPadding,'opacity':0})}).find("img:first").css({'width':pluginData.smallFeatureWidth});if(pluginData.totalFeatureCount<4){pluginData.itemsToAnimate=pluginData.totalFeatureCount}else{pluginData.itemsToAnimate=4}pluginData.featuresContainer.find("div > div").hide()}function setupFeaturePositions(){$.each(pluginData.featuresArray,function(i){$(this).data('setPosition',i+1)});var oneBeforeStarting=getPreviousNum(options.startingFeature);pluginData.currentCenterNum=oneBeforeStarting;var $centerFeature=getContainer(oneBeforeStarting);$centerFeature.data('position',1);var $prevFeatures=$centerFeature.prevAll();$prevFeatures.each(function(i){$(this).data('position',(pluginData.totalFeatureCount-i))});var $nextFeatures=$centerFeature.nextAll();$nextFeatures.each(function(i){if($(this).data('setPosition')!=undefined){$(this).data('position',(i+2))}});if(options.counterStyle==3){$.each(pluginData.featuresArray,function(){var pos=getPreviousNum($(this).data('position'));var $numberTag=$("<span></span>");$numberTag.addClass("numberTag");$numberTag.html("("+pos+" of "+pluginData.totalFeatureCount+")");$(this).find('div p').prepend($numberTag)})}}function setupBlips(){if(options.counterStyle==1||options.counterStyle==2){var $rightb=$("<div class=span-2 last nextButton><a href='javascript:void(0);'><img src=/homepage/images/car_next.gif alt=Next Carousal title=Next Carousal /></a><p class=prepend-top-20px><img src=/homepage/images/shadow_carobtn.jpg alt=carousal button shadow title=carousal button shadow/></p></div>");$rightb.addClass("nextButtonContainer rightButton");var $leftb=$("<div class=span-2 last prevButton><a href='javascript:void(0);'><img src=images/car_prev.gif alt=Previous Carousal title=Previous Carousal /></a><p class=prepend-top-20px><img src=/homepage/images/shadow_carobtn.jpg alt=carousal button shadow title=carousal button shadow/></p></div>");$leftb.addClass("previousButtonContainer leftButton");for(var i=0;i<pluginData.totalFeatureCount;i++){var counter;if(options.counterStyle==2)counter="";else counter=i+1}$(pluginData.containerIDTag).append($rightb);$rightb.show();$(pluginData.containerIDTag).append($leftb);$leftb.show()}}function changeBlip(oldCenter,newCenter){var $blipsContainer=pluginData.featuresContainer.find(".nextButtonContainer");var $oldCenter=$blipsContainer.find("#blip_"+oldCenter);var $newCenter=$blipsContainer.find("#blip_"+newCenter);$oldCenter.removeClass("blipSelected");$newCenter.addClass("blipSelected")}function autoPlay(){if(pluginData.timeoutVar!=null){pluginData.timeoutVar=clearTimeout(pluginData.timeoutVar)}if(options.autoPlay!=0){var autoTime=(Math.abs(options.autoPlay)<options.carouselSpeed)?options.carouselSpeed:Math.abs(options.autoPlay);pluginData.timeoutVar=setTimeout(function(){if(options.autoPlay>0)initiateMove(true,1);else if(options.autoPlay<0)initiateMove(false,1)},autoTime)}}function rotatePositions(direction){$.each(pluginData.featuresArray,function(){var newPos;if(direction==false){newPos=getNextNum($(this).data().position)}else{newPos=getPreviousNum($(this).data().position)}$(this).data('position',newPos)})}function animateFeature($feature,direction){var new_width,new_height,new_top,new_left,new_zindex,new_padding,new_fade;var oldPosition=$feature.data('position');var newPosition;if(direction==true)newPosition=getPreviousNum(oldPosition);else newPosition=getNextNum(oldPosition);if(newPosition==1){new_width=245;new_height=pluginData.largeFeatureHeight;new_top=options.topPadding;new_zindex=$feature.css("z-index");new_left=100;new_fade=1.0;$feature.removeClass('secondaryRight');$feature.removeClass('secondaryLeft');$feature.find(".description").removeClass('prependAppend-5px');$feature.find(".description").addClass('prependAppend-15px');$feature.addClass('primary')}else{new_width=115;new_height=pluginData.smallFeatureHeight;new_top=options.smallFeatureOffset+options.topPadding;new_zindex=1;new_fade=0.9;if(newPosition==pluginData.totalFeatureCount){new_left=20;$feature.removeClass('secondaryRight');$feature.removeClass('primary');$feature.find(".description").removeClass('prependAppend-15px');$feature.find(".description").addClass('prependAppend-5px');$feature.addClass('secondaryLeft')}else if(newPosition==2){new_left=308;$feature.removeClass('secondaryLeft');$feature.removeClass('primary');$feature.find(".description").removeClass('prependAppend-15px');$feature.find(".description").addClass('prependAppend-5px');$feature.addClass('secondaryRight')}else{new_left=(pluginData.containerWidth/2)-(pluginData.smallFeatureWidth/2)-(pluginData.borderWidth/2);new_fade=0}}if(newPosition!=1){$feature.find("div").hide()}$feature.animate({width:new_width,height:new_height,top:new_top,left:new_left,opacity:new_fade},options.carouselSpeed,options.animationEasing,function(){if(newPosition==1){$feature.find("div").fadeTo("fast",0.85)}pluginData.rotationsRemaining=pluginData.rotationsRemaining-1;$feature.css("z-index",new_zindex);if(options.counterStyle==1||options.counterStyle==2){if(newPosition==1){var newCenterItemNum=pluginData.featuresContainer.children("div").index($feature)+1;var oldCenterItemNum;if(direction==false)oldCenterItemNum=getNextNum(newCenterItemNum);else oldCenterItemNum=getPreviousNum(newCenterItemNum);changeBlip(oldCenterItemNum,newCenterItemNum)}}var divide=pluginData.rotationsRemaining/pluginData.itemsToAnimate;if(divide%1==0){pluginData.currentlyMoving=false;rotatePositions(direction);if(pluginData.rotationsRemaining>0)move(direction)}autoPlay()}).find("img:first").animate({width:new_width,height:new_height},options.carouselSpeed,options.animationEasing).end()}function move(direction){pluginData.currentlyMoving=true;var $newCenter,$newLeft,$newRight,$newHidden;if(direction==true){$newCenter=getContainer(getNextNum(pluginData.currentCenterNum));$newLeft=getContainer(pluginData.currentCenterNum);$newRight=getContainer(getNextNum(getNextNum(pluginData.currentCenterNum)));$newHidden=getContainer(getPreviousNum(pluginData.currentCenterNum));pluginData.currentCenterNum=getNextNum(pluginData.currentCenterNum)}else{$newCenter=getContainer(getPreviousNum(pluginData.currentCenterNum));$newLeft=getContainer(getPreviousNum(getPreviousNum(pluginData.currentCenterNum)));$newRight=getContainer(pluginData.currentCenterNum);$newHidden=getContainer(getNextNum(pluginData.currentCenterNum));pluginData.currentCenterNum=getPreviousNum(pluginData.currentCenterNum)}if(direction){$newLeft.css("z-index",3)}else{$newRight.css("z-index",3)}$newCenter.css("z-index",4);animateFeature($newLeft,direction);animateFeature($newCenter,direction);animateFeature($newRight,direction);if(pluginData.totalFeatureCount>3){animateFeature($newHidden,direction)}}function initiateMove(direction,rotations){if(pluginData.currentlyMoving==false){var queue=rotations*pluginData.itemsToAnimate;pluginData.rotationsRemaining=queue;move(direction)}}function findShortestDistance(from,to){var goingToLeft=1,goingToRight=1,tracker;tracker=from;while((tracker=getPreviousNum(tracker))!=to){goingToLeft++}tracker=from;while((tracker=getNextNum(tracker))!=to){goingToRight++}return(goingToLeft<goingToRight)?goingToLeft*-1:goingToRight}$(".leftButton").click(function(){initiateMove(false,1)});$(".rightButton").click(function(){initiateMove(true,1)});pluginData.featuresContainer.children("div").click(function(){var position=$(this).data('position');if(position==2){initiateMove(true,1)}else if(position==pluginData.totalFeatureCount){initiateMove(false,1)}}).mouseover(function(){if(pluginData.currentlyMoving==false){var position=$(this).data('position');if(position==2||position==pluginData.totalFeatureCount){$(this).css("opacity",0.8)}}}).mouseout(function(){if(pluginData.currentlyMoving==false){var position=$(this).data('position');if(position==2||position==pluginData.totalFeatureCount){$(this).css("opacity",0.9)}}});$("a",pluginData.containerIDTag).live("click",function(event){var $parents=$(this).parentsUntil(pluginData.containerIDTag);$parents.each(function(){var position=$(this).data('position');if(position!=undefined){if(position!=1){if(position==pluginData.totalFeatureCount){initiateMove(false,1)}else if(position==2){initiateMove(true,1)}event.preventDefault();return false}}})});$(".blip").live("click",function(){var goTo=$(this).attr("id").substring(5);var whereIsIt=pluginData.featuresContainer.children("div").eq(goTo-1).data('position');var currentlyAt=pluginData.currentCenterNum;if(goTo!=currentlyAt){var shortest=findShortestDistance(1,whereIsIt);if(shortest<0){initiateMove(false,(shortest*-1))}else{initiateMove(true,shortest)}}})})};$.fn.featureCarousel.defaults={largeFeatureWidth:0,largeFeatureHeight:0,smallFeatureWidth:.5,smallFeatureHeight:.5,topPadding:10,sidePadding:20,smallFeatureOffset:40,startingFeature:1,carouselSpeed:1300,autoPlay:0,counterStyle:1,preload:true,displayCutoff:0,animationEasing:'swing'}})(jQuery);
jQuery.cookie=function(name,value,options){if(typeof value!='undefined'){options=options||{};if(value===null){value='';options.expires=-1}var expires='';if(options.expires&&(typeof options.expires=='number'||options.expires.toUTCString)){var date;if(typeof options.expires=='number'){date=new Date();date.setTime(date.getTime()+(options.expires*24*60*60*1000))}else{date=options.expires}expires='; expires='+date.toUTCString()}var path=options.path?'; path='+(options.path):'';var domain=options.domain?'; domain='+(options.domain):'';var secure=options.secure?'; secure':'';document.cookie=[name,'=',encodeURIComponent(value),expires,path,domain,secure].join('')}else{var cookieValue=null;if(document.cookie&&document.cookie!=''){var cookies=document.cookie.split(';');for(var i=0;i<cookies.length;i++){var cookie=jQuery.trim(cookies[i]);if(cookie.substring(0,name.length+1)==(name+'=')){cookieValue=decodeURIComponent(cookie.substring(name.length+1));break}}}return cookieValue}};

/*!
 * jQuery UI 1.8.2
 *
 * Copyright (c) 2010 AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT (MIT-LICENSE.txt)
 * and GPL (GPL-LICENSE.txt) licenses.
 *
 * http://docs.jquery.com/UI
 */
(function(c){c.ui=c.ui||{};if(!c.ui.version){c.extend(c.ui,{version:"1.8.2",plugin:{add:function(a,b,d){a=c.ui[a].prototype;for(var e in d){a.plugins[e]=a.plugins[e]||[];a.plugins[e].push([b,d[e]])}},call:function(a,b,d){if((b=a.plugins[b])&&a.element[0].parentNode)for(var e=0;e<b.length;e++)a.options[b[e][0]]&&b[e][1].apply(a.element,d)}},contains:function(a,b){return document.compareDocumentPosition?a.compareDocumentPosition(b)&16:a!==b&&a.contains(b)},hasScroll:function(a,b){if(c(a).css("overflow")==
"hidden")return false;b=b&&b=="left"?"scrollLeft":"scrollTop";var d=false;if(a[b]>0)return true;a[b]=1;d=a[b]>0;a[b]=0;return d},isOverAxis:function(a,b,d){return a>b&&a<b+d},isOver:function(a,b,d,e,f,g){return c.ui.isOverAxis(a,d,f)&&c.ui.isOverAxis(b,e,g)},keyCode:{ALT:18,BACKSPACE:8,CAPS_LOCK:20,COMMA:188,COMMAND:91,COMMAND_LEFT:91,COMMAND_RIGHT:93,CONTROL:17,DELETE:46,DOWN:40,END:35,ENTER:13,ESCAPE:27,HOME:36,INSERT:45,LEFT:37,MENU:93,NUMPAD_ADD:107,NUMPAD_DECIMAL:110,NUMPAD_DIVIDE:111,NUMPAD_ENTER:108,
NUMPAD_MULTIPLY:106,NUMPAD_SUBTRACT:109,PAGE_DOWN:34,PAGE_UP:33,PERIOD:190,RIGHT:39,SHIFT:16,SPACE:32,TAB:9,UP:38,WINDOWS:91}});c.fn.extend({_focus:c.fn.focus,focus:function(a,b){return typeof a==="number"?this.each(function(){var d=this;setTimeout(function(){c(d).focus();b&&b.call(d)},a)}):this._focus.apply(this,arguments)},enableSelection:function(){return this.attr("unselectable","off").css("MozUserSelect","")},disableSelection:function(){return this.attr("unselectable","on").css("MozUserSelect",
"none")},scrollParent:function(){var a;a=c.browser.msie&&/(static|relative)/.test(this.css("position"))||/absolute/.test(this.css("position"))?this.parents().filter(function(){return/(relative|absolute|fixed)/.test(c.curCSS(this,"position",1))&&/(auto|scroll)/.test(c.curCSS(this,"overflow",1)+c.curCSS(this,"overflow-y",1)+c.curCSS(this,"overflow-x",1))}).eq(0):this.parents().filter(function(){return/(auto|scroll)/.test(c.curCSS(this,"overflow",1)+c.curCSS(this,"overflow-y",1)+c.curCSS(this,"overflow-x",
1))}).eq(0);return/fixed/.test(this.css("position"))||!a.length?c(document):a},zIndex:function(a){if(a!==undefined)return this.css("zIndex",a);if(this.length){a=c(this[0]);for(var b;a.length&&a[0]!==document;){b=a.css("position");if(b=="absolute"||b=="relative"||b=="fixed"){b=parseInt(a.css("zIndex"));if(!isNaN(b)&&b!=0)return b}a=a.parent()}}return 0}});c.extend(c.expr[":"],{data:function(a,b,d){return!!c.data(a,d[3])},focusable:function(a){var b=a.nodeName.toLowerCase(),d=c.attr(a,"tabindex");return(/input|select|textarea|button|object/.test(b)?
!a.disabled:"a"==b||"area"==b?a.href||!isNaN(d):!isNaN(d))&&!c(a)["area"==b?"parents":"closest"](":hidden").length},tabbable:function(a){var b=c.attr(a,"tabindex");return(isNaN(b)||b>=0)&&c(a).is(":focusable")}})}})(jQuery);
;/*!
 * jQuery UI Widget 1.8.2
 *
 * Copyright (c) 2010 AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT (MIT-LICENSE.txt)
 * and GPL (GPL-LICENSE.txt) licenses.
 *
 * http://docs.jquery.com/UI/Widget
 */
(function(b){var j=b.fn.remove;b.fn.remove=function(a,c){return this.each(function(){if(!c)if(!a||b.filter(a,[this]).length)b("*",this).add(this).each(function(){b(this).triggerHandler("remove")});return j.call(b(this),a,c)})};b.widget=function(a,c,d){var e=a.split(".")[0],f;a=a.split(".")[1];f=e+"-"+a;if(!d){d=c;c=b.Widget}b.expr[":"][f]=function(h){return!!b.data(h,a)};b[e]=b[e]||{};b[e][a]=function(h,g){arguments.length&&this._createWidget(h,g)};c=new c;c.options=b.extend({},c.options);b[e][a].prototype=
b.extend(true,c,{namespace:e,widgetName:a,widgetEventPrefix:b[e][a].prototype.widgetEventPrefix||a,widgetBaseClass:f},d);b.widget.bridge(a,b[e][a])};b.widget.bridge=function(a,c){b.fn[a]=function(d){var e=typeof d==="string",f=Array.prototype.slice.call(arguments,1),h=this;d=!e&&f.length?b.extend.apply(null,[true,d].concat(f)):d;if(e&&d.substring(0,1)==="_")return h;e?this.each(function(){var g=b.data(this,a),i=g&&b.isFunction(g[d])?g[d].apply(g,f):g;if(i!==g&&i!==undefined){h=i;return false}}):this.each(function(){var g=
b.data(this,a);if(g){d&&g.option(d);g._init()}else b.data(this,a,new c(d,this))});return h}};b.Widget=function(a,c){arguments.length&&this._createWidget(a,c)};b.Widget.prototype={widgetName:"widget",widgetEventPrefix:"",options:{disabled:false},_createWidget:function(a,c){this.element=b(c).data(this.widgetName,this);this.options=b.extend(true,{},this.options,b.metadata&&b.metadata.get(c)[this.widgetName],a);var d=this;this.element.bind("remove."+this.widgetName,function(){d.destroy()});this._create();
this._init()},_create:function(){},_init:function(){},destroy:function(){this.element.unbind("."+this.widgetName).removeData(this.widgetName);this.widget().unbind("."+this.widgetName).removeAttr("aria-disabled").removeClass(this.widgetBaseClass+"-disabled ui-state-disabled")},widget:function(){return this.element},option:function(a,c){var d=a,e=this;if(arguments.length===0)return b.extend({},e.options);if(typeof a==="string"){if(c===undefined)return this.options[a];d={};d[a]=c}b.each(d,function(f,
h){e._setOption(f,h)});return e},_setOption:function(a,c){this.options[a]=c;if(a==="disabled")this.widget()[c?"addClass":"removeClass"](this.widgetBaseClass+"-disabled ui-state-disabled").attr("aria-disabled",c);return this},enable:function(){return this._setOption("disabled",false)},disable:function(){return this._setOption("disabled",true)},_trigger:function(a,c,d){var e=this.options[a];c=b.Event(c);c.type=(a===this.widgetEventPrefix?a:this.widgetEventPrefix+a).toLowerCase();d=d||{};if(c.originalEvent){a=
b.event.props.length;for(var f;a;){f=b.event.props[--a];c[f]=c.originalEvent[f]}}this.element.trigger(c,d);return!(b.isFunction(e)&&e.call(this.element[0],c,d)===false||c.isDefaultPrevented())}}})(jQuery);
;/*!
 * jQuery UI Mouse 1.8.2
 *
 * Copyright (c) 2010 AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT (MIT-LICENSE.txt)
 * and GPL (GPL-LICENSE.txt) licenses.
 *
 * http://docs.jquery.com/UI/Mouse
 *
 * Depends:
 *	jquery.ui.widget.js
 */
(function(c){c.widget("ui.mouse",{options:{cancel:":input,option",distance:1,delay:0},_mouseInit:function(){var a=this;this.element.bind("mousedown."+this.widgetName,function(b){return a._mouseDown(b)}).bind("click."+this.widgetName,function(b){if(a._preventClickEvent){a._preventClickEvent=false;b.stopImmediatePropagation();return false}});this.started=false},_mouseDestroy:function(){this.element.unbind("."+this.widgetName)},_mouseDown:function(a){a.originalEvent=a.originalEvent||{};if(!a.originalEvent.mouseHandled){this._mouseStarted&&
this._mouseUp(a);this._mouseDownEvent=a;var b=this,e=a.which==1,f=typeof this.options.cancel=="string"?c(a.target).parents().add(a.target).filter(this.options.cancel).length:false;if(!e||f||!this._mouseCapture(a))return true;this.mouseDelayMet=!this.options.delay;if(!this.mouseDelayMet)this._mouseDelayTimer=setTimeout(function(){b.mouseDelayMet=true},this.options.delay);if(this._mouseDistanceMet(a)&&this._mouseDelayMet(a)){this._mouseStarted=this._mouseStart(a)!==false;if(!this._mouseStarted){a.preventDefault();
return true}}this._mouseMoveDelegate=function(d){return b._mouseMove(d)};this._mouseUpDelegate=function(d){return b._mouseUp(d)};c(document).bind("mousemove."+this.widgetName,this._mouseMoveDelegate).bind("mouseup."+this.widgetName,this._mouseUpDelegate);c.browser.safari||a.preventDefault();return a.originalEvent.mouseHandled=true}},_mouseMove:function(a){if(c.browser.msie&&!a.button)return this._mouseUp(a);if(this._mouseStarted){this._mouseDrag(a);return a.preventDefault()}if(this._mouseDistanceMet(a)&&
this._mouseDelayMet(a))(this._mouseStarted=this._mouseStart(this._mouseDownEvent,a)!==false)?this._mouseDrag(a):this._mouseUp(a);return!this._mouseStarted},_mouseUp:function(a){c(document).unbind("mousemove."+this.widgetName,this._mouseMoveDelegate).unbind("mouseup."+this.widgetName,this._mouseUpDelegate);if(this._mouseStarted){this._mouseStarted=false;this._preventClickEvent=a.target==this._mouseDownEvent.target;this._mouseStop(a)}return false},_mouseDistanceMet:function(a){return Math.max(Math.abs(this._mouseDownEvent.pageX-
a.pageX),Math.abs(this._mouseDownEvent.pageY-a.pageY))>=this.options.distance},_mouseDelayMet:function(){return this.mouseDelayMet},_mouseStart:function(){},_mouseDrag:function(){},_mouseStop:function(){},_mouseCapture:function(){return true}})})(jQuery);
;/*
 * jQuery UI Position 1.8.2
 *
 * Copyright (c) 2010 AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT (MIT-LICENSE.txt)
 * and GPL (GPL-LICENSE.txt) licenses.
 *
 * http://docs.jquery.com/UI/Position
 */
(function(c){c.ui=c.ui||{};var m=/left|center|right/,n=/top|center|bottom/,p=c.fn.position,q=c.fn.offset;c.fn.position=function(a){if(!a||!a.of)return p.apply(this,arguments);a=c.extend({},a);var b=c(a.of),d=(a.collision||"flip").split(" "),e=a.offset?a.offset.split(" "):[0,0],g,h,i;if(a.of.nodeType===9){g=b.width();h=b.height();i={top:0,left:0}}else if(a.of.scrollTo&&a.of.document){g=b.width();h=b.height();i={top:b.scrollTop(),left:b.scrollLeft()}}else if(a.of.preventDefault){a.at="left top";g=h=
0;i={top:a.of.pageY,left:a.of.pageX}}else{g=b.outerWidth();h=b.outerHeight();i=b.offset()}c.each(["my","at"],function(){var f=(a[this]||"").split(" ");if(f.length===1)f=m.test(f[0])?f.concat(["center"]):n.test(f[0])?["center"].concat(f):["center","center"];f[0]=m.test(f[0])?f[0]:"center";f[1]=n.test(f[1])?f[1]:"center";a[this]=f});if(d.length===1)d[1]=d[0];e[0]=parseInt(e[0],10)||0;if(e.length===1)e[1]=e[0];e[1]=parseInt(e[1],10)||0;if(a.at[0]==="right")i.left+=g;else if(a.at[0]==="center")i.left+=
g/2;if(a.at[1]==="bottom")i.top+=h;else if(a.at[1]==="center")i.top+=h/2;i.left+=e[0];i.top+=e[1];return this.each(function(){var f=c(this),k=f.outerWidth(),l=f.outerHeight(),j=c.extend({},i);if(a.my[0]==="right")j.left-=k;else if(a.my[0]==="center")j.left-=k/2;if(a.my[1]==="bottom")j.top-=l;else if(a.my[1]==="center")j.top-=l/2;j.left=parseInt(j.left);j.top=parseInt(j.top);c.each(["left","top"],function(o,r){c.ui.position[d[o]]&&c.ui.position[d[o]][r](j,{targetWidth:g,targetHeight:h,elemWidth:k,
elemHeight:l,offset:e,my:a.my,at:a.at})});c.fn.bgiframe&&f.bgiframe();f.offset(c.extend(j,{using:a.using}))})};c.ui.position={fit:{left:function(a,b){var d=c(window);b=a.left+b.elemWidth-d.width()-d.scrollLeft();a.left=b>0?a.left-b:Math.max(0,a.left)},top:function(a,b){var d=c(window);b=a.top+b.elemHeight-d.height()-d.scrollTop();a.top=b>0?a.top-b:Math.max(0,a.top)}},flip:{left:function(a,b){if(b.at[0]!=="center"){var d=c(window);d=a.left+b.elemWidth-d.width()-d.scrollLeft();var e=b.my[0]==="left"?
-b.elemWidth:b.my[0]==="right"?b.elemWidth:0,g=-2*b.offset[0];a.left+=a.left<0?e+b.targetWidth+g:d>0?e-b.targetWidth+g:0}},top:function(a,b){if(b.at[1]!=="center"){var d=c(window);d=a.top+b.elemHeight-d.height()-d.scrollTop();var e=b.my[1]==="top"?-b.elemHeight:b.my[1]==="bottom"?b.elemHeight:0,g=b.at[1]==="top"?b.targetHeight:-b.targetHeight,h=-2*b.offset[1];a.top+=a.top<0?e+b.targetHeight+h:d>0?e+g+h:0}}}};if(!c.offset.setOffset){c.offset.setOffset=function(a,b){if(/static/.test(c.curCSS(a,"position")))a.style.position=
"relative";var d=c(a),e=d.offset(),g=parseInt(c.curCSS(a,"top",true),10)||0,h=parseInt(c.curCSS(a,"left",true),10)||0;e={top:b.top-e.top+g,left:b.left-e.left+h};"using"in b?b.using.call(a,e):d.css(e)};c.fn.offset=function(a){var b=this[0];if(!b||!b.ownerDocument)return null;if(a)return this.each(function(){c.offset.setOffset(this,a)});return q.call(this)}}})(jQuery);
;/*
 * jQuery UI Draggable 1.8.2
 *
 * Copyright (c) 2010 AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT (MIT-LICENSE.txt)
 * and GPL (GPL-LICENSE.txt) licenses.
 *
 * http://docs.jquery.com/UI/Draggables
 *
 * Depends:
 *	jquery.ui.core.js
 *	jquery.ui.mouse.js
 *	jquery.ui.widget.js
 */
(function(d){d.widget("ui.draggable",d.ui.mouse,{widgetEventPrefix:"drag",options:{addClasses:true,appendTo:"parent",axis:false,connectToSortable:false,containment:false,cursor:"auto",cursorAt:false,grid:false,handle:false,helper:"original",iframeFix:false,opacity:false,refreshPositions:false,revert:false,revertDuration:500,scope:"default",scroll:true,scrollSensitivity:20,scrollSpeed:20,snap:false,snapMode:"both",snapTolerance:20,stack:false,zIndex:false},_create:function(){if(this.options.helper==
"original"&&!/^(?:r|a|f)/.test(this.element.css("position")))this.element[0].style.position="relative";this.options.addClasses&&this.element.addClass("ui-draggable");this.options.disabled&&this.element.addClass("ui-draggable-disabled");this._mouseInit()},destroy:function(){if(this.element.data("draggable")){this.element.removeData("draggable").unbind(".draggable").removeClass("ui-draggable ui-draggable-dragging ui-draggable-disabled");this._mouseDestroy();return this}},_mouseCapture:function(a){var b=
this.options;if(this.helper||b.disabled||d(a.target).is(".ui-resizable-handle"))return false;this.handle=this._getHandle(a);if(!this.handle)return false;return true},_mouseStart:function(a){var b=this.options;this.helper=this._createHelper(a);this._cacheHelperProportions();if(d.ui.ddmanager)d.ui.ddmanager.current=this;this._cacheMargins();this.cssPosition=this.helper.css("position");this.scrollParent=this.helper.scrollParent();this.offset=this.positionAbs=this.element.offset();this.offset={top:this.offset.top-
this.margins.top,left:this.offset.left-this.margins.left};d.extend(this.offset,{click:{left:a.pageX-this.offset.left,top:a.pageY-this.offset.top},parent:this._getParentOffset(),relative:this._getRelativeOffset()});this.originalPosition=this.position=this._generatePosition(a);this.originalPageX=a.pageX;this.originalPageY=a.pageY;b.cursorAt&&this._adjustOffsetFromHelper(b.cursorAt);b.containment&&this._setContainment();if(this._trigger("start",a)===false){this._clear();return false}this._cacheHelperProportions();
d.ui.ddmanager&&!b.dropBehaviour&&d.ui.ddmanager.prepareOffsets(this,a);this.helper.addClass("ui-draggable-dragging");this._mouseDrag(a,true);return true},_mouseDrag:function(a,b){this.position=this._generatePosition(a);this.positionAbs=this._convertPositionTo("absolute");if(!b){b=this._uiHash();if(this._trigger("drag",a,b)===false){this._mouseUp({});return false}this.position=b.position}if(!this.options.axis||this.options.axis!="y")this.helper[0].style.left=this.position.left+"px";if(!this.options.axis||
this.options.axis!="x")this.helper[0].style.top=this.position.top+"px";d.ui.ddmanager&&d.ui.ddmanager.drag(this,a);return false},_mouseStop:function(a){var b=false;if(d.ui.ddmanager&&!this.options.dropBehaviour)b=d.ui.ddmanager.drop(this,a);if(this.dropped){b=this.dropped;this.dropped=false}if(!this.element[0]||!this.element[0].parentNode)return false;if(this.options.revert=="invalid"&&!b||this.options.revert=="valid"&&b||this.options.revert===true||d.isFunction(this.options.revert)&&this.options.revert.call(this.element,
b)){var c=this;d(this.helper).animate(this.originalPosition,parseInt(this.options.revertDuration,10),function(){c._trigger("stop",a)!==false&&c._clear()})}else this._trigger("stop",a)!==false&&this._clear();return false},cancel:function(){this.helper.is(".ui-draggable-dragging")?this._mouseUp({}):this._clear();return this},_getHandle:function(a){var b=!this.options.handle||!d(this.options.handle,this.element).length?true:false;d(this.options.handle,this.element).find("*").andSelf().each(function(){if(this==
a.target)b=true});return b},_createHelper:function(a){var b=this.options;a=d.isFunction(b.helper)?d(b.helper.apply(this.element[0],[a])):b.helper=="clone"?this.element.clone():this.element;a.parents("body").length||a.appendTo(b.appendTo=="parent"?this.element[0].parentNode:b.appendTo);a[0]!=this.element[0]&&!/(fixed|absolute)/.test(a.css("position"))&&a.css("position","absolute");return a},_adjustOffsetFromHelper:function(a){if(typeof a=="string")a=a.split(" ");if(d.isArray(a))a={left:+a[0],top:+a[1]||
0};if("left"in a)this.offset.click.left=a.left+this.margins.left;if("right"in a)this.offset.click.left=this.helperProportions.width-a.right+this.margins.left;if("top"in a)this.offset.click.top=a.top+this.margins.top;if("bottom"in a)this.offset.click.top=this.helperProportions.height-a.bottom+this.margins.top},_getParentOffset:function(){this.offsetParent=this.helper.offsetParent();var a=this.offsetParent.offset();if(this.cssPosition=="absolute"&&this.scrollParent[0]!=document&&d.ui.contains(this.scrollParent[0],
this.offsetParent[0])){a.left+=this.scrollParent.scrollLeft();a.top+=this.scrollParent.scrollTop()}if(this.offsetParent[0]==document.body||this.offsetParent[0].tagName&&this.offsetParent[0].tagName.toLowerCase()=="html"&&d.browser.msie)a={top:0,left:0};return{top:a.top+(parseInt(this.offsetParent.css("borderTopWidth"),10)||0),left:a.left+(parseInt(this.offsetParent.css("borderLeftWidth"),10)||0)}},_getRelativeOffset:function(){if(this.cssPosition=="relative"){var a=this.element.position();return{top:a.top-
(parseInt(this.helper.css("top"),10)||0)+this.scrollParent.scrollTop(),left:a.left-(parseInt(this.helper.css("left"),10)||0)+this.scrollParent.scrollLeft()}}else return{top:0,left:0}},_cacheMargins:function(){this.margins={left:parseInt(this.element.css("marginLeft"),10)||0,top:parseInt(this.element.css("marginTop"),10)||0}},_cacheHelperProportions:function(){this.helperProportions={width:this.helper.outerWidth(),height:this.helper.outerHeight()}},_setContainment:function(){var a=this.options;if(a.containment==
"parent")a.containment=this.helper[0].parentNode;if(a.containment=="document"||a.containment=="window")this.containment=[0-this.offset.relative.left-this.offset.parent.left,0-this.offset.relative.top-this.offset.parent.top,d(a.containment=="document"?document:window).width()-this.helperProportions.width-this.margins.left,(d(a.containment=="document"?document:window).height()||document.body.parentNode.scrollHeight)-this.helperProportions.height-this.margins.top];if(!/^(document|window|parent)$/.test(a.containment)&&
a.containment.constructor!=Array){var b=d(a.containment)[0];if(b){a=d(a.containment).offset();var c=d(b).css("overflow")!="hidden";this.containment=[a.left+(parseInt(d(b).css("borderLeftWidth"),10)||0)+(parseInt(d(b).css("paddingLeft"),10)||0)-this.margins.left,a.top+(parseInt(d(b).css("borderTopWidth"),10)||0)+(parseInt(d(b).css("paddingTop"),10)||0)-this.margins.top,a.left+(c?Math.max(b.scrollWidth,b.offsetWidth):b.offsetWidth)-(parseInt(d(b).css("borderLeftWidth"),10)||0)-(parseInt(d(b).css("paddingRight"),
10)||0)-this.helperProportions.width-this.margins.left,a.top+(c?Math.max(b.scrollHeight,b.offsetHeight):b.offsetHeight)-(parseInt(d(b).css("borderTopWidth"),10)||0)-(parseInt(d(b).css("paddingBottom"),10)||0)-this.helperProportions.height-this.margins.top]}}else if(a.containment.constructor==Array)this.containment=a.containment},_convertPositionTo:function(a,b){if(!b)b=this.position;a=a=="absolute"?1:-1;var c=this.cssPosition=="absolute"&&!(this.scrollParent[0]!=document&&d.ui.contains(this.scrollParent[0],
this.offsetParent[0]))?this.offsetParent:this.scrollParent,f=/(html|body)/i.test(c[0].tagName);return{top:b.top+this.offset.relative.top*a+this.offset.parent.top*a-(d.browser.safari&&d.browser.version<526&&this.cssPosition=="fixed"?0:(this.cssPosition=="fixed"?-this.scrollParent.scrollTop():f?0:c.scrollTop())*a),left:b.left+this.offset.relative.left*a+this.offset.parent.left*a-(d.browser.safari&&d.browser.version<526&&this.cssPosition=="fixed"?0:(this.cssPosition=="fixed"?-this.scrollParent.scrollLeft():
f?0:c.scrollLeft())*a)}},_generatePosition:function(a){var b=this.options,c=this.cssPosition=="absolute"&&!(this.scrollParent[0]!=document&&d.ui.contains(this.scrollParent[0],this.offsetParent[0]))?this.offsetParent:this.scrollParent,f=/(html|body)/i.test(c[0].tagName),e=a.pageX,g=a.pageY;if(this.originalPosition){if(this.containment){if(a.pageX-this.offset.click.left<this.containment[0])e=this.containment[0]+this.offset.click.left;if(a.pageY-this.offset.click.top<this.containment[1])g=this.containment[1]+
this.offset.click.top;if(a.pageX-this.offset.click.left>this.containment[2])e=this.containment[2]+this.offset.click.left;if(a.pageY-this.offset.click.top>this.containment[3])g=this.containment[3]+this.offset.click.top}if(b.grid){g=this.originalPageY+Math.round((g-this.originalPageY)/b.grid[1])*b.grid[1];g=this.containment?!(g-this.offset.click.top<this.containment[1]||g-this.offset.click.top>this.containment[3])?g:!(g-this.offset.click.top<this.containment[1])?g-b.grid[1]:g+b.grid[1]:g;e=this.originalPageX+
Math.round((e-this.originalPageX)/b.grid[0])*b.grid[0];e=this.containment?!(e-this.offset.click.left<this.containment[0]||e-this.offset.click.left>this.containment[2])?e:!(e-this.offset.click.left<this.containment[0])?e-b.grid[0]:e+b.grid[0]:e}}return{top:g-this.offset.click.top-this.offset.relative.top-this.offset.parent.top+(d.browser.safari&&d.browser.version<526&&this.cssPosition=="fixed"?0:this.cssPosition=="fixed"?-this.scrollParent.scrollTop():f?0:c.scrollTop()),left:e-this.offset.click.left-
this.offset.relative.left-this.offset.parent.left+(d.browser.safari&&d.browser.version<526&&this.cssPosition=="fixed"?0:this.cssPosition=="fixed"?-this.scrollParent.scrollLeft():f?0:c.scrollLeft())}},_clear:function(){this.helper.removeClass("ui-draggable-dragging");this.helper[0]!=this.element[0]&&!this.cancelHelperRemoval&&this.helper.remove();this.helper=null;this.cancelHelperRemoval=false},_trigger:function(a,b,c){c=c||this._uiHash();d.ui.plugin.call(this,a,[b,c]);if(a=="drag")this.positionAbs=
this._convertPositionTo("absolute");return d.Widget.prototype._trigger.call(this,a,b,c)},plugins:{},_uiHash:function(){return{helper:this.helper,position:this.position,originalPosition:this.originalPosition,offset:this.positionAbs}}});d.extend(d.ui.draggable,{version:"1.8.2"});d.ui.plugin.add("draggable","connectToSortable",{start:function(a,b){var c=d(this).data("draggable"),f=c.options,e=d.extend({},b,{item:c.element});c.sortables=[];d(f.connectToSortable).each(function(){var g=d.data(this,"sortable");
if(g&&!g.options.disabled){c.sortables.push({instance:g,shouldRevert:g.options.revert});g._refreshItems();g._trigger("activate",a,e)}})},stop:function(a,b){var c=d(this).data("draggable"),f=d.extend({},b,{item:c.element});d.each(c.sortables,function(){if(this.instance.isOver){this.instance.isOver=0;c.cancelHelperRemoval=true;this.instance.cancelHelperRemoval=false;if(this.shouldRevert)this.instance.options.revert=true;this.instance._mouseStop(a);this.instance.options.helper=this.instance.options._helper;
c.options.helper=="original"&&this.instance.currentItem.css({top:"auto",left:"auto"})}else{this.instance.cancelHelperRemoval=false;this.instance._trigger("deactivate",a,f)}})},drag:function(a,b){var c=d(this).data("draggable"),f=this;d.each(c.sortables,function(){this.instance.positionAbs=c.positionAbs;this.instance.helperProportions=c.helperProportions;this.instance.offset.click=c.offset.click;if(this.instance._intersectsWith(this.instance.containerCache)){if(!this.instance.isOver){this.instance.isOver=
1;this.instance.currentItem=d(f).clone().appendTo(this.instance.element).data("sortable-item",true);this.instance.options._helper=this.instance.options.helper;this.instance.options.helper=function(){return b.helper[0]};a.target=this.instance.currentItem[0];this.instance._mouseCapture(a,true);this.instance._mouseStart(a,true,true);this.instance.offset.click.top=c.offset.click.top;this.instance.offset.click.left=c.offset.click.left;this.instance.offset.parent.left-=c.offset.parent.left-this.instance.offset.parent.left;
this.instance.offset.parent.top-=c.offset.parent.top-this.instance.offset.parent.top;c._trigger("toSortable",a);c.dropped=this.instance.element;c.currentItem=c.element;this.instance.fromOutside=c}this.instance.currentItem&&this.instance._mouseDrag(a)}else if(this.instance.isOver){this.instance.isOver=0;this.instance.cancelHelperRemoval=true;this.instance.options.revert=false;this.instance._trigger("out",a,this.instance._uiHash(this.instance));this.instance._mouseStop(a,true);this.instance.options.helper=
this.instance.options._helper;this.instance.currentItem.remove();this.instance.placeholder&&this.instance.placeholder.remove();c._trigger("fromSortable",a);c.dropped=false}})}});d.ui.plugin.add("draggable","cursor",{start:function(){var a=d("body"),b=d(this).data("draggable").options;if(a.css("cursor"))b._cursor=a.css("cursor");a.css("cursor",b.cursor)},stop:function(){var a=d(this).data("draggable").options;a._cursor&&d("body").css("cursor",a._cursor)}});d.ui.plugin.add("draggable","iframeFix",{start:function(){var a=
d(this).data("draggable").options;d(a.iframeFix===true?"iframe":a.iframeFix).each(function(){d('<div class="ui-draggable-iframeFix" style="background: #fff;"></div>').css({width:this.offsetWidth+"px",height:this.offsetHeight+"px",position:"absolute",opacity:"https://www.usbank.com/homepage/homepage_js/0.001",zIndex:1E3}).css(d(this).offset()).appendTo("body")})},stop:function(){d("div.ui-draggable-iframeFix").each(function(){this.parentNode.removeChild(this)})}});d.ui.plugin.add("draggable","opacity",{start:function(a,b){a=d(b.helper);b=d(this).data("draggable").options;
if(a.css("opacity"))b._opacity=a.css("opacity");a.css("opacity",b.opacity)},stop:function(a,b){a=d(this).data("draggable").options;a._opacity&&d(b.helper).css("opacity",a._opacity)}});d.ui.plugin.add("draggable","scroll",{start:function(){var a=d(this).data("draggable");if(a.scrollParent[0]!=document&&a.scrollParent[0].tagName!="HTML")a.overflowOffset=a.scrollParent.offset()},drag:function(a){var b=d(this).data("draggable"),c=b.options,f=false;if(b.scrollParent[0]!=document&&b.scrollParent[0].tagName!=
"HTML"){if(!c.axis||c.axis!="x")if(b.overflowOffset.top+b.scrollParent[0].offsetHeight-a.pageY<c.scrollSensitivity)b.scrollParent[0].scrollTop=f=b.scrollParent[0].scrollTop+c.scrollSpeed;else if(a.pageY-b.overflowOffset.top<c.scrollSensitivity)b.scrollParent[0].scrollTop=f=b.scrollParent[0].scrollTop-c.scrollSpeed;if(!c.axis||c.axis!="y")if(b.overflowOffset.left+b.scrollParent[0].offsetWidth-a.pageX<c.scrollSensitivity)b.scrollParent[0].scrollLeft=f=b.scrollParent[0].scrollLeft+c.scrollSpeed;else if(a.pageX-
b.overflowOffset.left<c.scrollSensitivity)b.scrollParent[0].scrollLeft=f=b.scrollParent[0].scrollLeft-c.scrollSpeed}else{if(!c.axis||c.axis!="x")if(a.pageY-d(document).scrollTop()<c.scrollSensitivity)f=d(document).scrollTop(d(document).scrollTop()-c.scrollSpeed);else if(d(window).height()-(a.pageY-d(document).scrollTop())<c.scrollSensitivity)f=d(document).scrollTop(d(document).scrollTop()+c.scrollSpeed);if(!c.axis||c.axis!="y")if(a.pageX-d(document).scrollLeft()<c.scrollSensitivity)f=d(document).scrollLeft(d(document).scrollLeft()-
c.scrollSpeed);else if(d(window).width()-(a.pageX-d(document).scrollLeft())<c.scrollSensitivity)f=d(document).scrollLeft(d(document).scrollLeft()+c.scrollSpeed)}f!==false&&d.ui.ddmanager&&!c.dropBehaviour&&d.ui.ddmanager.prepareOffsets(b,a)}});d.ui.plugin.add("draggable","snap",{start:function(){var a=d(this).data("draggable"),b=a.options;a.snapElements=[];d(b.snap.constructor!=String?b.snap.items||":data(draggable)":b.snap).each(function(){var c=d(this),f=c.offset();this!=a.element[0]&&a.snapElements.push({item:this,
width:c.outerWidth(),height:c.outerHeight(),top:f.top,left:f.left})})},drag:function(a,b){for(var c=d(this).data("draggable"),f=c.options,e=f.snapTolerance,g=b.offset.left,n=g+c.helperProportions.width,m=b.offset.top,o=m+c.helperProportions.height,h=c.snapElements.length-1;h>=0;h--){var i=c.snapElements[h].left,k=i+c.snapElements[h].width,j=c.snapElements[h].top,l=j+c.snapElements[h].height;if(i-e<g&&g<k+e&&j-e<m&&m<l+e||i-e<g&&g<k+e&&j-e<o&&o<l+e||i-e<n&&n<k+e&&j-e<m&&m<l+e||i-e<n&&n<k+e&&j-e<o&&
o<l+e){if(f.snapMode!="inner"){var p=Math.abs(j-o)<=e,q=Math.abs(l-m)<=e,r=Math.abs(i-n)<=e,s=Math.abs(k-g)<=e;if(p)b.position.top=c._convertPositionTo("relative",{top:j-c.helperProportions.height,left:0}).top-c.margins.top;if(q)b.position.top=c._convertPositionTo("relative",{top:l,left:0}).top-c.margins.top;if(r)b.position.left=c._convertPositionTo("relative",{top:0,left:i-c.helperProportions.width}).left-c.margins.left;if(s)b.position.left=c._convertPositionTo("relative",{top:0,left:k}).left-c.margins.left}var t=
p||q||r||s;if(f.snapMode!="outer"){p=Math.abs(j-m)<=e;q=Math.abs(l-o)<=e;r=Math.abs(i-g)<=e;s=Math.abs(k-n)<=e;if(p)b.position.top=c._convertPositionTo("relative",{top:j,left:0}).top-c.margins.top;if(q)b.position.top=c._convertPositionTo("relative",{top:l-c.helperProportions.height,left:0}).top-c.margins.top;if(r)b.position.left=c._convertPositionTo("relative",{top:0,left:i}).left-c.margins.left;if(s)b.position.left=c._convertPositionTo("relative",{top:0,left:k-c.helperProportions.width}).left-c.margins.left}if(!c.snapElements[h].snapping&&
(p||q||r||s||t))c.options.snap.snap&&c.options.snap.snap.call(c.element,a,d.extend(c._uiHash(),{snapItem:c.snapElements[h].item}));c.snapElements[h].snapping=p||q||r||s||t}else{c.snapElements[h].snapping&&c.options.snap.release&&c.options.snap.release.call(c.element,a,d.extend(c._uiHash(),{snapItem:c.snapElements[h].item}));c.snapElements[h].snapping=false}}}});d.ui.plugin.add("draggable","stack",{start:function(){var a=d(this).data("draggable").options;a=d.makeArray(d(a.stack)).sort(function(c,f){return(parseInt(d(c).css("zIndex"),
10)||0)-(parseInt(d(f).css("zIndex"),10)||0)});if(a.length){var b=parseInt(a[0].style.zIndex)||0;d(a).each(function(c){this.style.zIndex=b+c});this[0].style.zIndex=b+a.length}}});d.ui.plugin.add("draggable","zIndex",{start:function(a,b){a=d(b.helper);b=d(this).data("draggable").options;if(a.css("zIndex"))b._zIndex=a.css("zIndex");a.css("zIndex",b.zIndex)},stop:function(a,b){a=d(this).data("draggable").options;a._zIndex&&d(b.helper).css("zIndex",a._zIndex)}})})(jQuery);
;/*
 * jQuery UI Droppable 1.8.2
 *
 * Copyright (c) 2010 AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT (MIT-LICENSE.txt)
 * and GPL (GPL-LICENSE.txt) licenses.
 *
 * http://docs.jquery.com/UI/Droppables
 *
 * Depends:
 *	jquery.ui.core.js
 *	jquery.ui.widget.js
 *	jquery.ui.mouse.js
 *	jquery.ui.draggable.js
 */
(function(d){d.widget("ui.droppable",{widgetEventPrefix:"drop",options:{accept:"*",activeClass:false,addClasses:true,greedy:false,hoverClass:false,scope:"default",tolerance:"intersect"},_create:function(){var a=this.options,b=a.accept;this.isover=0;this.isout=1;this.accept=d.isFunction(b)?b:function(c){return c.is(b)};this.proportions={width:this.element[0].offsetWidth,height:this.element[0].offsetHeight};d.ui.ddmanager.droppables[a.scope]=d.ui.ddmanager.droppables[a.scope]||[];d.ui.ddmanager.droppables[a.scope].push(this);
a.addClasses&&this.element.addClass("ui-droppable")},destroy:function(){for(var a=d.ui.ddmanager.droppables[this.options.scope],b=0;b<a.length;b++)a[b]==this&&a.splice(b,1);this.element.removeClass("ui-droppable ui-droppable-disabled").removeData("droppable").unbind(".droppable");return this},_setOption:function(a,b){if(a=="accept")this.accept=d.isFunction(b)?b:function(c){return c.is(b)};d.Widget.prototype._setOption.apply(this,arguments)},_activate:function(a){var b=d.ui.ddmanager.current;this.options.activeClass&&
this.element.addClass(this.options.activeClass);b&&this._trigger("activate",a,this.ui(b))},_deactivate:function(a){var b=d.ui.ddmanager.current;this.options.activeClass&&this.element.removeClass(this.options.activeClass);b&&this._trigger("deactivate",a,this.ui(b))},_over:function(a){var b=d.ui.ddmanager.current;if(!(!b||(b.currentItem||b.element)[0]==this.element[0]))if(this.accept.call(this.element[0],b.currentItem||b.element)){this.options.hoverClass&&this.element.addClass(this.options.hoverClass);
this._trigger("over",a,this.ui(b))}},_out:function(a){var b=d.ui.ddmanager.current;if(!(!b||(b.currentItem||b.element)[0]==this.element[0]))if(this.accept.call(this.element[0],b.currentItem||b.element)){this.options.hoverClass&&this.element.removeClass(this.options.hoverClass);this._trigger("out",a,this.ui(b))}},_drop:function(a,b){var c=b||d.ui.ddmanager.current;if(!c||(c.currentItem||c.element)[0]==this.element[0])return false;var e=false;this.element.find(":data(droppable)").not(".ui-draggable-dragging").each(function(){var g=
d.data(this,"droppable");if(g.options.greedy&&!g.options.disabled&&g.options.scope==c.options.scope&&g.accept.call(g.element[0],c.currentItem||c.element)&&d.ui.intersect(c,d.extend(g,{offset:g.element.offset()}),g.options.tolerance)){e=true;return false}});if(e)return false;if(this.accept.call(this.element[0],c.currentItem||c.element)){this.options.activeClass&&this.element.removeClass(this.options.activeClass);this.options.hoverClass&&this.element.removeClass(this.options.hoverClass);this._trigger("drop",
a,this.ui(c));return this.element}return false},ui:function(a){return{draggable:a.currentItem||a.element,helper:a.helper,position:a.position,offset:a.positionAbs}}});d.extend(d.ui.droppable,{version:"1.8.2"});d.ui.intersect=function(a,b,c){if(!b.offset)return false;var e=(a.positionAbs||a.position.absolute).left,g=e+a.helperProportions.width,f=(a.positionAbs||a.position.absolute).top,h=f+a.helperProportions.height,i=b.offset.left,k=i+b.proportions.width,j=b.offset.top,l=j+b.proportions.height;
switch(c){case "fit":return i<e&&g<k&&j<f&&h<l;case "intersect":return i<e+a.helperProportions.width/2&&g-a.helperProportions.width/2<k&&j<f+a.helperProportions.height/2&&h-a.helperProportions.height/2<l;case "pointer":return d.ui.isOver((a.positionAbs||a.position.absolute).top+(a.clickOffset||a.offset.click).top,(a.positionAbs||a.position.absolute).left+(a.clickOffset||a.offset.click).left,j,i,b.proportions.height,b.proportions.width);case "touch":return(f>=j&&f<=l||h>=j&&h<=l||f<j&&h>l)&&(e>=i&&
e<=k||g>=i&&g<=k||e<i&&g>k);default:return false}};d.ui.ddmanager={current:null,droppables:{"default":[]},prepareOffsets:function(a,b){var c=d.ui.ddmanager.droppables[a.options.scope]||[],e=b?b.type:null,g=(a.currentItem||a.element).find(":data(droppable)").andSelf(),f=0;a:for(;f<c.length;f++)if(!(c[f].options.disabled||a&&!c[f].accept.call(c[f].element[0],a.currentItem||a.element))){for(var h=0;h<g.length;h++)if(g[h]==c[f].element[0]){c[f].proportions.height=0;continue a}c[f].visible=c[f].element.css("display")!=
"none";if(c[f].visible){c[f].offset=c[f].element.offset();c[f].proportions={width:c[f].element[0].offsetWidth,height:c[f].element[0].offsetHeight};e=="mousedown"&&c[f]._activate.call(c[f],b)}}},drop:function(a,b){var c=false;d.each(d.ui.ddmanager.droppables[a.options.scope]||[],function(){if(this.options){if(!this.options.disabled&&this.visible&&d.ui.intersect(a,this,this.options.tolerance))c=c||this._drop.call(this,b);if(!this.options.disabled&&this.visible&&this.accept.call(this.element[0],a.currentItem||
a.element)){this.isout=1;this.isover=0;this._deactivate.call(this,b)}}});return c},drag:function(a,b){a.options.refreshPositions&&d.ui.ddmanager.prepareOffsets(a,b);d.each(d.ui.ddmanager.droppables[a.options.scope]||[],function(){if(!(this.options.disabled||this.greedyChild||!this.visible)){var c=d.ui.intersect(a,this,this.options.tolerance);if(c=!c&&this.isover==1?"isout":c&&this.isover==0?"isover":null){var e;if(this.options.greedy){var g=this.element.parents(":data(droppable):eq(0)");if(g.length){e=
d.data(g[0],"droppable");e.greedyChild=c=="isover"?1:0}}if(e&&c=="isover"){e.isover=0;e.isout=1;e._out.call(e,b)}this[c]=1;this[c=="isout"?"isover":"isout"]=0;this[c=="isover"?"_over":"_out"].call(this,b);if(e&&c=="isout"){e.isout=0;e.isover=1;e._over.call(e,b)}}}})}}})(jQuery);
;/*
 * jQuery UI Resizable 1.8.2
 *
 * Copyright (c) 2010 AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT (MIT-LICENSE.txt)
 * and GPL (GPL-LICENSE.txt) licenses.
 *
 * http://docs.jquery.com/UI/Resizables
 *
 * Depends:
 *	jquery.ui.core.js
 *	jquery.ui.mouse.js
 *	jquery.ui.widget.js
 */
(function(d){d.widget("ui.resizable",d.ui.mouse,{widgetEventPrefix:"resize",options:{alsoResize:false,animate:false,animateDuration:"slow",animateEasing:"swing",aspectRatio:false,autoHide:false,containment:false,ghost:false,grid:false,handles:"e,s,se",helper:false,maxHeight:null,maxWidth:null,minHeight:10,minWidth:10,zIndex:1E3},_create:function(){var b=this,a=this.options;this.element.addClass("ui-resizable");d.extend(this,{_aspectRatio:!!a.aspectRatio,aspectRatio:a.aspectRatio,originalElement:this.element,
_proportionallyResizeElements:[],_helper:a.helper||a.ghost||a.animate?a.helper||"ui-resizable-helper":null});if(this.element[0].nodeName.match(/canvas|textarea|input|select|button|img/i)){/relative/.test(this.element.css("position"))&&d.browser.opera&&this.element.css({position:"relative",top:"auto",left:"auto"});this.element.wrap(d('<div class="ui-wrapper" style="overflow: hidden;"></div>').css({position:this.element.css("position"),width:this.element.outerWidth(),height:this.element.outerHeight(),
top:this.element.css("top"),left:this.element.css("left")}));this.element=this.element.parent().data("resizable",this.element.data("resizable"));this.elementIsWrapper=true;this.element.css({marginLeft:this.originalElement.css("marginLeft"),marginTop:this.originalElement.css("marginTop"),marginRight:this.originalElement.css("marginRight"),marginBottom:this.originalElement.css("marginBottom")});this.originalElement.css({marginLeft:0,marginTop:0,marginRight:0,marginBottom:0});this.originalResizeStyle=
this.originalElement.css("resize");this.originalElement.css("resize","none");this._proportionallyResizeElements.push(this.originalElement.css({position:"static",zoom:1,display:"block"}));this.originalElement.css({margin:this.originalElement.css("margin")});this._proportionallyResize()}this.handles=a.handles||(!d(".ui-resizable-handle",this.element).length?"e,s,se":{n:".ui-resizable-n",e:".ui-resizable-e",s:".ui-resizable-s",w:".ui-resizable-w",se:".ui-resizable-se",sw:".ui-resizable-sw",ne:".ui-resizable-ne",
nw:".ui-resizable-nw"});if(this.handles.constructor==String){if(this.handles=="all")this.handles="n,e,s,w,se,sw,ne,nw";var c=this.handles.split(",");this.handles={};for(var e=0;e<c.length;e++){var g=d.trim(c[e]),f=d('<div class="ui-resizable-handle '+("ui-resizable-"+g)+'"></div>');/sw|se|ne|nw/.test(g)&&f.css({zIndex:++a.zIndex});"se"==g&&f.addClass("ui-icon ui-icon-gripsmall-diagonal-se");this.handles[g]=".ui-resizable-"+g;this.element.append(f)}}this._renderAxis=function(h){h=h||this.element;for(var i in this.handles){if(this.handles[i].constructor==
String)this.handles[i]=d(this.handles[i],this.element).show();if(this.elementIsWrapper&&this.originalElement[0].nodeName.match(/textarea|input|select|button/i)){var j=d(this.handles[i],this.element),l=0;l=/sw|ne|nw|se|n|s/.test(i)?j.outerHeight():j.outerWidth();j=["padding",/ne|nw|n/.test(i)?"Top":/se|sw|s/.test(i)?"Bottom":/^e$/.test(i)?"Right":"Left"].join("");h.css(j,l);this._proportionallyResize()}d(this.handles[i])}};this._renderAxis(this.element);this._handles=d(".ui-resizable-handle",this.element).disableSelection();
this._handles.mouseover(function(){if(!b.resizing){if(this.className)var h=this.className.match(/ui-resizable-(se|sw|ne|nw|n|e|s|w)/i);b.axis=h&&h[1]?h[1]:"se"}});if(a.autoHide){this._handles.hide();d(this.element).addClass("ui-resizable-autohide").hover(function(){d(this).removeClass("ui-resizable-autohide");b._handles.show()},function(){if(!b.resizing){d(this).addClass("ui-resizable-autohide");b._handles.hide()}})}this._mouseInit()},destroy:function(){this._mouseDestroy();var b=function(c){d(c).removeClass("ui-resizable ui-resizable-disabled ui-resizable-resizing").removeData("resizable").unbind(".resizable").find(".ui-resizable-handle").remove()};
if(this.elementIsWrapper){b(this.element);var a=this.element;a.after(this.originalElement.css({position:a.css("position"),width:a.outerWidth(),height:a.outerHeight(),top:a.css("top"),left:a.css("left")})).remove()}this.originalElement.css("resize",this.originalResizeStyle);b(this.originalElement);return this},_mouseCapture:function(b){var a=false;for(var c in this.handles)if(d(this.handles[c])[0]==b.target)a=true;return!this.options.disabled&&a},_mouseStart:function(b){var a=this.options,c=this.element.position(),
e=this.element;this.resizing=true;this.documentScroll={top:d(document).scrollTop(),left:d(document).scrollLeft()};if(e.is(".ui-draggable")||/absolute/.test(e.css("position")))e.css({position:"absolute",top:c.top,left:c.left});d.browser.opera&&/relative/.test(e.css("position"))&&e.css({position:"relative",top:"auto",left:"auto"});this._renderProxy();c=m(this.helper.css("left"));var g=m(this.helper.css("top"));if(a.containment){c+=d(a.containment).scrollLeft()||0;g+=d(a.containment).scrollTop()||0}this.offset=
this.helper.offset();this.position={left:c,top:g};this.size=this._helper?{width:e.outerWidth(),height:e.outerHeight()}:{width:e.width(),height:e.height()};this.originalSize=this._helper?{width:e.outerWidth(),height:e.outerHeight()}:{width:e.width(),height:e.height()};this.originalPosition={left:c,top:g};this.sizeDiff={width:e.outerWidth()-e.width(),height:e.outerHeight()-e.height()};this.originalMousePosition={left:b.pageX,top:b.pageY};this.aspectRatio=typeof a.aspectRatio=="number"?a.aspectRatio:
this.originalSize.width/this.originalSize.height||1;a=d(".ui-resizable-"+this.axis).css("cursor");d("body").css("cursor",a=="auto"?this.axis+"-resize":a);e.addClass("ui-resizable-resizing");this._propagate("start",b);return true},_mouseDrag:function(b){var a=this.helper,c=this.originalMousePosition,e=this._change[this.axis];if(!e)return false;c=e.apply(this,[b,b.pageX-c.left||0,b.pageY-c.top||0]);if(this._aspectRatio||b.shiftKey)c=this._updateRatio(c,b);c=this._respectSize(c,b);this._propagate("resize",
b);a.css({top:this.position.top+"px",left:this.position.left+"px",width:this.size.width+"px",height:this.size.height+"px"});!this._helper&&this._proportionallyResizeElements.length&&this._proportionallyResize();this._updateCache(c);this._trigger("resize",b,this.ui());return false},_mouseStop:function(b){this.resizing=false;var a=this.options,c=this;if(this._helper){var e=this._proportionallyResizeElements,g=e.length&&/textarea/i.test(e[0].nodeName);e=g&&d.ui.hasScroll(e[0],"left")?0:c.sizeDiff.height;
g={width:c.size.width-(g?0:c.sizeDiff.width),height:c.size.height-e};e=parseInt(c.element.css("left"),10)+(c.position.left-c.originalPosition.left)||null;var f=parseInt(c.element.css("top"),10)+(c.position.top-c.originalPosition.top)||null;a.animate||this.element.css(d.extend(g,{top:f,left:e}));c.helper.height(c.size.height);c.helper.width(c.size.width);this._helper&&!a.animate&&this._proportionallyResize()}d("body").css("cursor","auto");this.element.removeClass("ui-resizable-resizing");this._propagate("stop",
b);this._helper&&this.helper.remove();return false},_updateCache:function(b){this.offset=this.helper.offset();if(k(b.left))this.position.left=b.left;if(k(b.top))this.position.top=b.top;if(k(b.height))this.size.height=b.height;if(k(b.width))this.size.width=b.width},_updateRatio:function(b){var a=this.position,c=this.size,e=this.axis;if(b.height)b.width=c.height*this.aspectRatio;else if(b.width)b.height=c.width/this.aspectRatio;if(e=="sw"){b.left=a.left+(c.width-b.width);b.top=null}if(e=="nw"){b.top=
a.top+(c.height-b.height);b.left=a.left+(c.width-b.width)}return b},_respectSize:function(b){var a=this.options,c=this.axis,e=k(b.width)&&a.maxWidth&&a.maxWidth<b.width,g=k(b.height)&&a.maxHeight&&a.maxHeight<b.height,f=k(b.width)&&a.minWidth&&a.minWidth>b.width,h=k(b.height)&&a.minHeight&&a.minHeight>b.height;if(f)b.width=a.minWidth;if(h)b.height=a.minHeight;if(e)b.width=a.maxWidth;if(g)b.height=a.maxHeight;var i=this.originalPosition.left+this.originalSize.width,j=this.position.top+this.size.height,
l=/sw|nw|w/.test(c);c=/nw|ne|n/.test(c);if(f&&l)b.left=i-a.minWidth;if(e&&l)b.left=i-a.maxWidth;if(h&&c)b.top=j-a.minHeight;if(g&&c)b.top=j-a.maxHeight;if((a=!b.width&&!b.height)&&!b.left&&b.top)b.top=null;else if(a&&!b.top&&b.left)b.left=null;return b},_proportionallyResize:function(){if(this._proportionallyResizeElements.length)for(var b=this.helper||this.element,a=0;a<this._proportionallyResizeElements.length;a++){var c=this._proportionallyResizeElements[a];if(!this.borderDif){var e=[c.css("borderTopWidth"),
c.css("borderRightWidth"),c.css("borderBottomWidth"),c.css("borderLeftWidth")],g=[c.css("paddingTop"),c.css("paddingRight"),c.css("paddingBottom"),c.css("paddingLeft")];this.borderDif=d.map(e,function(f,h){f=parseInt(f,10)||0;h=parseInt(g[h],10)||0;return f+h})}d.browser.msie&&(d(b).is(":hidden")||d(b).parents(":hidden").length)||c.css({height:b.height()-this.borderDif[0]-this.borderDif[2]||0,width:b.width()-this.borderDif[1]-this.borderDif[3]||0})}},_renderProxy:function(){var b=this.options;this.elementOffset=
this.element.offset();if(this._helper){this.helper=this.helper||d('<div style="overflow:hidden;"></div>');var a=d.browser.msie&&d.browser.version<7,c=a?1:0;a=a?2:-1;this.helper.addClass(this._helper).css({width:this.element.outerWidth()+a,height:this.element.outerHeight()+a,position:"absolute",left:this.elementOffset.left-c+"px",top:this.elementOffset.top-c+"px",zIndex:++b.zIndex});this.helper.appendTo("body").disableSelection()}else this.helper=this.element},_change:{e:function(b,a){return{width:this.originalSize.width+
a}},w:function(b,a){return{left:this.originalPosition.left+a,width:this.originalSize.width-a}},n:function(b,a,c){return{top:this.originalPosition.top+c,height:this.originalSize.height-c}},s:function(b,a,c){return{height:this.originalSize.height+c}},se:function(b,a,c){return d.extend(this._change.s.apply(this,arguments),this._change.e.apply(this,[b,a,c]))},sw:function(b,a,c){return d.extend(this._change.s.apply(this,arguments),this._change.w.apply(this,[b,a,c]))},ne:function(b,a,c){return d.extend(this._change.n.apply(this,
arguments),this._change.e.apply(this,[b,a,c]))},nw:function(b,a,c){return d.extend(this._change.n.apply(this,arguments),this._change.w.apply(this,[b,a,c]))}},_propagate:function(b,a){d.ui.plugin.call(this,b,[a,this.ui()]);b!="resize"&&this._trigger(b,a,this.ui())},plugins:{},ui:function(){return{originalElement:this.originalElement,element:this.element,helper:this.helper,position:this.position,size:this.size,originalSize:this.originalSize,originalPosition:this.originalPosition}}});d.extend(d.ui.resizable,
{version:"1.8.2"});d.ui.plugin.add("resizable","alsoResize",{start:function(){var b=d(this).data("resizable").options,a=function(c){d(c).each(function(){d(this).data("resizable-alsoresize",{width:parseInt(d(this).width(),10),height:parseInt(d(this).height(),10),left:parseInt(d(this).css("left"),10),top:parseInt(d(this).css("top"),10)})})};if(typeof b.alsoResize=="object"&&!b.alsoResize.parentNode)if(b.alsoResize.length){b.alsoResize=b.alsoResize[0];a(b.alsoResize)}else d.each(b.alsoResize,function(c){a(c)});
else a(b.alsoResize)},resize:function(){var b=d(this).data("resizable"),a=b.options,c=b.originalSize,e=b.originalPosition,g={height:b.size.height-c.height||0,width:b.size.width-c.width||0,top:b.position.top-e.top||0,left:b.position.left-e.left||0},f=function(h,i){d(h).each(function(){var j=d(this),l=d(this).data("resizable-alsoresize"),p={};d.each((i&&i.length?i:["width","height","top","left"])||["width","height","top","left"],function(n,o){if((n=(l[o]||0)+(g[o]||0))&&n>=0)p[o]=n||null});if(/relative/.test(j.css("position"))&&
d.browser.opera){b._revertToRelativePosition=true;j.css({position:"absolute",top:"auto",left:"auto"})}j.css(p)})};typeof a.alsoResize=="object"&&!a.alsoResize.nodeType?d.each(a.alsoResize,function(h,i){f(h,i)}):f(a.alsoResize)},stop:function(){var b=d(this).data("resizable");if(b._revertToRelativePosition&&d.browser.opera){b._revertToRelativePosition=false;el.css({position:"relative"})}d(this).removeData("resizable-alsoresize-start")}});d.ui.plugin.add("resizable","animate",{stop:function(b){var a=
d(this).data("resizable"),c=a.options,e=a._proportionallyResizeElements,g=e.length&&/textarea/i.test(e[0].nodeName),f=g&&d.ui.hasScroll(e[0],"left")?0:a.sizeDiff.height;g={width:a.size.width-(g?0:a.sizeDiff.width),height:a.size.height-f};f=parseInt(a.element.css("left"),10)+(a.position.left-a.originalPosition.left)||null;var h=parseInt(a.element.css("top"),10)+(a.position.top-a.originalPosition.top)||null;a.element.animate(d.extend(g,h&&f?{top:h,left:f}:{}),{duration:c.animateDuration,easing:c.animateEasing,
step:function(){var i={width:parseInt(a.element.css("width"),10),height:parseInt(a.element.css("height"),10),top:parseInt(a.element.css("top"),10),left:parseInt(a.element.css("left"),10)};e&&e.length&&d(e[0]).css({width:i.width,height:i.height});a._updateCache(i);a._propagate("resize",b)}})}});d.ui.plugin.add("resizable","containment",{start:function(){var b=d(this).data("resizable"),a=b.element,c=b.options.containment;if(a=c instanceof d?c.get(0):/parent/.test(c)?a.parent().get(0):c){b.containerElement=
d(a);if(/document/.test(c)||c==document){b.containerOffset={left:0,top:0};b.containerPosition={left:0,top:0};b.parentData={element:d(document),left:0,top:0,width:d(document).width(),height:d(document).height()||document.body.parentNode.scrollHeight}}else{var e=d(a),g=[];d(["Top","Right","Left","Bottom"]).each(function(i,j){g[i]=m(e.css("padding"+j))});b.containerOffset=e.offset();b.containerPosition=e.position();b.containerSize={height:e.innerHeight()-g[3],width:e.innerWidth()-g[1]};c=b.containerOffset;
var f=b.containerSize.height,h=b.containerSize.width;h=d.ui.hasScroll(a,"left")?a.scrollWidth:h;f=d.ui.hasScroll(a)?a.scrollHeight:f;b.parentData={element:a,left:c.left,top:c.top,width:h,height:f}}}},resize:function(b){var a=d(this).data("resizable"),c=a.options,e=a.containerOffset,g=a.position;b=a._aspectRatio||b.shiftKey;var f={top:0,left:0},h=a.containerElement;if(h[0]!=document&&/static/.test(h.css("position")))f=e;if(g.left<(a._helper?e.left:0)){a.size.width+=a._helper?a.position.left-e.left:
a.position.left-f.left;if(b)a.size.height=a.size.width/c.aspectRatio;a.position.left=c.helper?e.left:0}if(g.top<(a._helper?e.top:0)){a.size.height+=a._helper?a.position.top-e.top:a.position.top;if(b)a.size.width=a.size.height*c.aspectRatio;a.position.top=a._helper?e.top:0}a.offset.left=a.parentData.left+a.position.left;a.offset.top=a.parentData.top+a.position.top;c=Math.abs((a._helper?a.offset.left-f.left:a.offset.left-f.left)+a.sizeDiff.width);e=Math.abs((a._helper?a.offset.top-f.top:a.offset.top-
e.top)+a.sizeDiff.height);g=a.containerElement.get(0)==a.element.parent().get(0);f=/relative|absolute/.test(a.containerElement.css("position"));if(g&&f)c-=a.parentData.left;if(c+a.size.width>=a.parentData.width){a.size.width=a.parentData.width-c;if(b)a.size.height=a.size.width/a.aspectRatio}if(e+a.size.height>=a.parentData.height){a.size.height=a.parentData.height-e;if(b)a.size.width=a.size.height*a.aspectRatio}},stop:function(){var b=d(this).data("resizable"),a=b.options,c=b.containerOffset,e=b.containerPosition,
g=b.containerElement,f=d(b.helper),h=f.offset(),i=f.outerWidth()-b.sizeDiff.width;f=f.outerHeight()-b.sizeDiff.height;b._helper&&!a.animate&&/relative/.test(g.css("position"))&&d(this).css({left:h.left-e.left-c.left,width:i,height:f});b._helper&&!a.animate&&/static/.test(g.css("position"))&&d(this).css({left:h.left-e.left-c.left,width:i,height:f})}});d.ui.plugin.add("resizable","ghost",{start:function(){var b=d(this).data("resizable"),a=b.options,c=b.size;b.ghost=b.originalElement.clone();b.ghost.css({opacity:0.25,
display:"block",position:"relative",height:c.height,width:c.width,margin:0,left:0,top:0}).addClass("ui-resizable-ghost").addClass(typeof a.ghost=="string"?a.ghost:"");b.ghost.appendTo(b.helper)},resize:function(){var b=d(this).data("resizable");b.ghost&&b.ghost.css({position:"relative",height:b.size.height,width:b.size.width})},stop:function(){var b=d(this).data("resizable");b.ghost&&b.helper&&b.helper.get(0).removeChild(b.ghost.get(0))}});d.ui.plugin.add("resizable","grid",{resize:function(){var b=
d(this).data("resizable"),a=b.options,c=b.size,e=b.originalSize,g=b.originalPosition,f=b.axis;a.grid=typeof a.grid=="number"?[a.grid,a.grid]:a.grid;var h=Math.round((c.width-e.width)/(a.grid[0]||1))*(a.grid[0]||1);a=Math.round((c.height-e.height)/(a.grid[1]||1))*(a.grid[1]||1);if(/^(se|s|e)$/.test(f)){b.size.width=e.width+h;b.size.height=e.height+a}else if(/^(ne)$/.test(f)){b.size.width=e.width+h;b.size.height=e.height+a;b.position.top=g.top-a}else{if(/^(sw)$/.test(f)){b.size.width=e.width+h;b.size.height=
e.height+a}else{b.size.width=e.width+h;b.size.height=e.height+a;b.position.top=g.top-a}b.position.left=g.left-h}}});var m=function(b){return parseInt(b,10)||0},k=function(b){return!isNaN(parseInt(b,10))}})(jQuery);
;
/*
 * jQuery UI Selectable 1.8.2
 *
 * Copyright (c) 2010 AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT (MIT-LICENSE.txt)
 * and GPL (GPL-LICENSE.txt) licenses.
 *
 * http://docs.jquery.com/UI/Selectables
 *
 * Depends:
 *	jquery.ui.core.js
 *	jquery.ui.mouse.js
 *	jquery.ui.widget.js
 */
(function($) {

$.widget("ui.selectable", $.ui.mouse, {
	options: {
		appendTo: 'body',
		autoRefresh: true,
		distance: 0,
		filter: '*',
		tolerance: 'touch'
	},
	_create: function() {
		var self = this;

		this.element.addClass("ui-selectable");

		this.dragged = false;

		// cache selectee children based on filter
		var selectees;
		this.refresh = function() {
			selectees = $(self.options.filter, self.element[0]);
			selectees.each(function() {
				var $this = $(this);
				var pos = $this.offset();
				$.data(this, "selectable-item", {
					element: this,
					$element: $this,
					left: pos.left,
					top: pos.top,
					right: pos.left + $this.outerWidth(),
					bottom: pos.top + $this.outerHeight(),
					startselected: false,
					selected: $this.hasClass('ui-selected'),
					selecting: $this.hasClass('ui-selecting'),
					unselecting: $this.hasClass('ui-unselecting')
				});
			});
		};
		this.refresh();

		this.selectees = selectees.addClass("ui-selectee");

		this._mouseInit();

		this.helper = $("<div class='ui-selectable-helper'></div>");
	},

	destroy: function() {
		this.selectees
			.removeClass("ui-selectee")
			.removeData("selectable-item");
		this.element
			.removeClass("ui-selectable ui-selectable-disabled")
			.removeData("selectable")
			.unbind(".selectable");
		this._mouseDestroy();

		return this;
	},

	_mouseStart: function(event) {
		var self = this;

		this.opos = [event.pageX, event.pageY];

		if (this.options.disabled)
			return;

		var options = this.options;

		this.selectees = $(options.filter, this.element[0]);

		this._trigger("start", event);

		$(options.appendTo).append(this.helper);
		// position helper (lasso)
		this.helper.css({
			"z-index": 100,
			"position": "absolute",
			"left": event.clientX,
			"top": event.clientY,
			"width": 0,
			"height": 0
		});

		if (options.autoRefresh) {
			this.refresh();
		}

		this.selectees.filter('.ui-selected').each(function() {
			var selectee = $.data(this, "selectable-item");
			selectee.startselected = true;
			if (!event.metaKey) {
				selectee.$element.removeClass('ui-selected');
				selectee.selected = false;
				selectee.$element.addClass('ui-unselecting');
				selectee.unselecting = true;
				// selectable UNSELECTING callback
				self._trigger("unselecting", event, {
					unselecting: selectee.element
				});
			}
		});

		$(event.target).parents().andSelf().each(function() {
			var selectee = $.data(this, "selectable-item");
			if (selectee) {
				var doSelect = !event.metaKey || !selectee.$element.hasClass('ui-selected');
				selectee.$element
					.removeClass(doSelect ? "ui-unselecting" : "ui-selected")
					.addClass(doSelect ? "ui-selecting" : "ui-unselecting");
				selectee.unselecting = !doSelect;
				selectee.selecting = doSelect;
				selectee.selected = doSelect;
				// selectable (UN)SELECTING callback
				if (doSelect) {
					self._trigger("selecting", event, {
						selecting: selectee.element
					});
				} else {
					self._trigger("unselecting", event, {
						unselecting: selectee.element
					});
				}
				return false;
			}
		});

	},

	_mouseDrag: function(event) {
		var self = this;
		this.dragged = true;

		if (this.options.disabled)
			return;

		var options = this.options;

		var x1 = this.opos[0], y1 = this.opos[1], x2 = event.pageX, y2 = event.pageY;
		if (x1 > x2) { var tmp = x2; x2 = x1; x1 = tmp; }
		if (y1 > y2) { var tmp = y2; y2 = y1; y1 = tmp; }
		this.helper.css({left: x1, top: y1, width: x2-x1, height: y2-y1});

		this.selectees.each(function() {
			var selectee = $.data(this, "selectable-item");
			//prevent helper from being selected if appendTo: selectable
			if (!selectee || selectee.element == self.element[0])
				return;
			var hit = false;
			if (options.tolerance == 'touch') {
				hit = ( !(selectee.left > x2 || selectee.right < x1 || selectee.top > y2 || selectee.bottom < y1) );
			} else if (options.tolerance == 'fit') {
				hit = (selectee.left > x1 && selectee.right < x2 && selectee.top > y1 && selectee.bottom < y2);
			}

			if (hit) {
				// SELECT
				if (selectee.selected) {
					selectee.$element.removeClass('ui-selected');
					selectee.selected = false;
				}
				if (selectee.unselecting) {
					selectee.$element.removeClass('ui-unselecting');
					selectee.unselecting = false;
				}
				if (!selectee.selecting) {
					selectee.$element.addClass('ui-selecting');
					selectee.selecting = true;
					// selectable SELECTING callback
					self._trigger("selecting", event, {
						selecting: selectee.element
					});
				}
			} else {
				// UNSELECT
				if (selectee.selecting) {
					if (event.metaKey && selectee.startselected) {
						selectee.$element.removeClass('ui-selecting');
						selectee.selecting = false;
						selectee.$element.addClass('ui-selected');
						selectee.selected = true;
					} else {
						selectee.$element.removeClass('ui-selecting');
						selectee.selecting = false;
						if (selectee.startselected) {
							selectee.$element.addClass('ui-unselecting');
							selectee.unselecting = true;
						}
						// selectable UNSELECTING callback
						self._trigger("unselecting", event, {
							unselecting: selectee.element
						});
					}
				}
				if (selectee.selected) {
					if (!event.metaKey && !selectee.startselected) {
						selectee.$element.removeClass('ui-selected');
						selectee.selected = false;

						selectee.$element.addClass('ui-unselecting');
						selectee.unselecting = true;
						// selectable UNSELECTING callback
						self._trigger("unselecting", event, {
							unselecting: selectee.element
						});
					}
				}
			}
		});

		return false;
	},

	_mouseStop: function(event) {
		var self = this;

		this.dragged = false;

		var options = this.options;

		$('.ui-unselecting', this.element[0]).each(function() {
			var selectee = $.data(this, "selectable-item");
			selectee.$element.removeClass('ui-unselecting');
			selectee.unselecting = false;
			selectee.startselected = false;
			self._trigger("unselected", event, {
				unselected: selectee.element
			});
		});
		$('.ui-selecting', this.element[0]).each(function() {
			var selectee = $.data(this, "selectable-item");
			selectee.$element.removeClass('ui-selecting').addClass('ui-selected');
			selectee.selecting = false;
			selectee.selected = true;
			selectee.startselected = true;
			self._trigger("selected", event, {
				selected: selectee.element
			});
		});
		this._trigger("stop", event);

		this.helper.remove();

		return false;
	}

});

$.extend($.ui.selectable, {
	version: "1.8.2"
});

})(jQuery);
(function(e){e.widget("ui.selectable",e.ui.mouse,{options:{appendTo:"body",autoRefresh:true,distance:0,filter:"*",tolerance:"touch"},_create:function(){var c=this;this.element.addClass("ui-selectable");this.dragged=false;var f;this.refresh=function(){f=e(c.options.filter,c.element[0]);f.each(function(){var d=e(this),b=d.offset();e.data(this,"selectable-item",{element:this,$element:d,left:b.left,top:b.top,right:b.left+d.outerWidth(),bottom:b.top+d.outerHeight(),startselected:false,selected:d.hasClass("ui-selected"),
selecting:d.hasClass("ui-selecting"),unselecting:d.hasClass("ui-unselecting")})})};this.refresh();this.selectees=f.addClass("ui-selectee");this._mouseInit();this.helper=e("<div class='ui-selectable-helper'></div>")},destroy:function(){this.selectees.removeClass("ui-selectee").removeData("selectable-item");this.element.removeClass("ui-selectable ui-selectable-disabled").removeData("selectable").unbind(".selectable");this._mouseDestroy();return this},_mouseStart:function(c){var f=this;this.opos=[c.pageX,
c.pageY];if(!this.options.disabled){var d=this.options;this.selectees=e(d.filter,this.element[0]);this._trigger("start",c);e(d.appendTo).append(this.helper);this.helper.css({"z-index":100,position:"absolute",left:c.clientX,top:c.clientY,width:0,height:0});d.autoRefresh&&this.refresh();this.selectees.filter(".ui-selected").each(function(){var b=e.data(this,"selectable-item");b.startselected=true;if(!c.metaKey){b.$element.removeClass("ui-selected");b.selected=false;b.$element.addClass("ui-unselecting");
b.unselecting=true;f._trigger("unselecting",c,{unselecting:b.element})}});e(c.target).parents().andSelf().each(function(){var b=e.data(this,"selectable-item");if(b){var g=!c.metaKey||!b.$element.hasClass("ui-selected");b.$element.removeClass(g?"ui-unselecting":"ui-selected").addClass(g?"ui-selecting":"ui-unselecting");b.unselecting=!g;b.selecting=g;(b.selected=g)?f._trigger("selecting",c,{selecting:b.element}):f._trigger("unselecting",c,{unselecting:b.element});return false}})}},_mouseDrag:function(c){var f=
this;this.dragged=true;if(!this.options.disabled){var d=this.options,b=this.opos[0],g=this.opos[1],h=c.pageX,i=c.pageY;if(b>h){var j=h;h=b;b=j}if(g>i){j=i;i=g;g=j}this.helper.css({left:b,top:g,width:h-b,height:i-g});this.selectees.each(function(){var a=e.data(this,"selectable-item");if(!(!a||a.element==f.element[0])){var k=false;if(d.tolerance=="touch")k=!(a.left>h||a.right<b||a.top>i||a.bottom<g);else if(d.tolerance=="fit")k=a.left>b&&a.right<h&&a.top>g&&a.bottom<i;if(k){if(a.selected){a.$element.removeClass("ui-selected");
a.selected=false}if(a.unselecting){a.$element.removeClass("ui-unselecting");a.unselecting=false}if(!a.selecting){a.$element.addClass("ui-selecting");a.selecting=true;f._trigger("selecting",c,{selecting:a.element})}}else{if(a.selecting)if(c.metaKey&&a.startselected){a.$element.removeClass("ui-selecting");a.selecting=false;a.$element.addClass("ui-selected");a.selected=true}else{a.$element.removeClass("ui-selecting");a.selecting=false;if(a.startselected){a.$element.addClass("ui-unselecting");a.unselecting=
true}f._trigger("unselecting",c,{unselecting:a.element})}if(a.selected)if(!c.metaKey&&!a.startselected){a.$element.removeClass("ui-selected");a.selected=false;a.$element.addClass("ui-unselecting");a.unselecting=true;f._trigger("unselecting",c,{unselecting:a.element})}}}});return false}},_mouseStop:function(c){var f=this;this.dragged=false;e(".ui-unselecting",this.element[0]).each(function(){var d=e.data(this,"selectable-item");d.$element.removeClass("ui-unselecting");d.unselecting=false;d.startselected=
false;f._trigger("unselected",c,{unselected:d.element})});e(".ui-selecting",this.element[0]).each(function(){var d=e.data(this,"selectable-item");d.$element.removeClass("ui-selecting").addClass("ui-selected");d.selecting=false;d.selected=true;d.startselected=true;f._trigger("selected",c,{selected:d.element})});this._trigger("stop",c);this.helper.remove();return false}});e.extend(e.ui.selectable,{version:"1.8.2"})})(jQuery);
;/*
 * jQuery UI Sortable 1.8.2
 *
 * Copyright (c) 2010 AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT (MIT-LICENSE.txt)
 * and GPL (GPL-LICENSE.txt) licenses.
 *
 * http://docs.jquery.com/UI/Sortables
 *
 * Depends:
 *	jquery.ui.core.js
 *	jquery.ui.mouse.js
 *	jquery.ui.widget.js
 */
(function(d){d.widget("ui.sortable",d.ui.mouse,{widgetEventPrefix:"sort",options:{appendTo:"parent",axis:false,connectWith:false,containment:false,cursor:"auto",cursorAt:false,dropOnEmpty:true,forcePlaceholderSize:false,forceHelperSize:false,grid:false,handle:false,helper:"original",items:"> *",opacity:false,placeholder:false,revert:false,scroll:true,scrollSensitivity:20,scrollSpeed:20,scope:"default",tolerance:"intersect",zIndex:1E3},_create:function(){this.containerCache={};this.element.addClass("ui-sortable");
this.refresh();this.floating=this.items.length?/left|right/.test(this.items[0].item.css("float")):false;this.offset=this.element.offset();this._mouseInit()},destroy:function(){this.element.removeClass("ui-sortable ui-sortable-disabled").removeData("sortable").unbind(".sortable");this._mouseDestroy();for(var a=this.items.length-1;a>=0;a--)this.items[a].item.removeData("sortable-item");return this},_setOption:function(a,b){if(a==="disabled"){this.options[a]=b;this.widget()[b?"addClass":"removeClass"]("ui-sortable-disabled")}else d.Widget.prototype._setOption.apply(this,
arguments)},_mouseCapture:function(a,b){if(this.reverting)return false;if(this.options.disabled||this.options.type=="static")return false;this._refreshItems(a);var c=null,e=this;d(a.target).parents().each(function(){if(d.data(this,"sortable-item")==e){c=d(this);return false}});if(d.data(a.target,"sortable-item")==e)c=d(a.target);if(!c)return false;if(this.options.handle&&!b){var f=false;d(this.options.handle,c).find("*").andSelf().each(function(){if(this==a.target)f=true});if(!f)return false}this.currentItem=
c;this._removeCurrentsFromItems();return true},_mouseStart:function(a,b,c){b=this.options;var e=this;this.currentContainer=this;this.refreshPositions();this.helper=this._createHelper(a);this._cacheHelperProportions();this._cacheMargins();this.scrollParent=this.helper.scrollParent();this.offset=this.currentItem.offset();this.offset={top:this.offset.top-this.margins.top,left:this.offset.left-this.margins.left};this.helper.css("position","absolute");this.cssPosition=this.helper.css("position");d.extend(this.offset,
{click:{left:a.pageX-this.offset.left,top:a.pageY-this.offset.top},parent:this._getParentOffset(),relative:this._getRelativeOffset()});this.originalPosition=this._generatePosition(a);this.originalPageX=a.pageX;this.originalPageY=a.pageY;b.cursorAt&&this._adjustOffsetFromHelper(b.cursorAt);this.domPosition={prev:this.currentItem.prev()[0],parent:this.currentItem.parent()[0]};this.helper[0]!=this.currentItem[0]&&this.currentItem.hide();this._createPlaceholder();b.containment&&this._setContainment();
if(b.cursor){if(d("body").css("cursor"))this._storedCursor=d("body").css("cursor");d("body").css("cursor",b.cursor)}if(b.opacity){if(this.helper.css("opacity"))this._storedOpacity=this.helper.css("opacity");this.helper.css("opacity",b.opacity)}if(b.zIndex){if(this.helper.css("zIndex"))this._storedZIndex=this.helper.css("zIndex");this.helper.css("zIndex",b.zIndex)}if(this.scrollParent[0]!=document&&this.scrollParent[0].tagName!="HTML")this.overflowOffset=this.scrollParent.offset();this._trigger("start",
a,this._uiHash());this._preserveHelperProportions||this._cacheHelperProportions();if(!c)for(c=this.containers.length-1;c>=0;c--)this.containers[c]._trigger("activate",a,e._uiHash(this));if(d.ui.ddmanager)d.ui.ddmanager.current=this;d.ui.ddmanager&&!b.dropBehaviour&&d.ui.ddmanager.prepareOffsets(this,a);this.dragging=true;this.helper.addClass("ui-sortable-helper");this._mouseDrag(a);return true},_mouseDrag:function(a){this.position=this._generatePosition(a);this.positionAbs=this._convertPositionTo("absolute");
if(!this.lastPositionAbs)this.lastPositionAbs=this.positionAbs;if(this.options.scroll){var b=this.options,c=false;if(this.scrollParent[0]!=document&&this.scrollParent[0].tagName!="HTML"){if(this.overflowOffset.top+this.scrollParent[0].offsetHeight-a.pageY<b.scrollSensitivity)this.scrollParent[0].scrollTop=c=this.scrollParent[0].scrollTop+b.scrollSpeed;else if(a.pageY-this.overflowOffset.top<b.scrollSensitivity)this.scrollParent[0].scrollTop=c=this.scrollParent[0].scrollTop-b.scrollSpeed;if(this.overflowOffset.left+
this.scrollParent[0].offsetWidth-a.pageX<b.scrollSensitivity)this.scrollParent[0].scrollLeft=c=this.scrollParent[0].scrollLeft+b.scrollSpeed;else if(a.pageX-this.overflowOffset.left<b.scrollSensitivity)this.scrollParent[0].scrollLeft=c=this.scrollParent[0].scrollLeft-b.scrollSpeed}else{if(a.pageY-d(document).scrollTop()<b.scrollSensitivity)c=d(document).scrollTop(d(document).scrollTop()-b.scrollSpeed);else if(d(window).height()-(a.pageY-d(document).scrollTop())<b.scrollSensitivity)c=d(document).scrollTop(d(document).scrollTop()+
b.scrollSpeed);if(a.pageX-d(document).scrollLeft()<b.scrollSensitivity)c=d(document).scrollLeft(d(document).scrollLeft()-b.scrollSpeed);else if(d(window).width()-(a.pageX-d(document).scrollLeft())<b.scrollSensitivity)c=d(document).scrollLeft(d(document).scrollLeft()+b.scrollSpeed)}c!==false&&d.ui.ddmanager&&!b.dropBehaviour&&d.ui.ddmanager.prepareOffsets(this,a)}this.positionAbs=this._convertPositionTo("absolute");if(!this.options.axis||this.options.axis!="y")this.helper[0].style.left=this.position.left+
"px";if(!this.options.axis||this.options.axis!="x")this.helper[0].style.top=this.position.top+"px";for(b=this.items.length-1;b>=0;b--){c=this.items[b];var e=c.item[0],f=this._intersectsWithPointer(c);if(f)if(e!=this.currentItem[0]&&this.placeholder[f==1?"next":"prev"]()[0]!=e&&!d.ui.contains(this.placeholder[0],e)&&(this.options.type=="semi-dynamic"?!d.ui.contains(this.element[0],e):true)){this.direction=f==1?"down":"up";if(this.options.tolerance=="pointer"||this._intersectsWithSides(c))this._rearrange(a,
c);else break;this._trigger("change",a,this._uiHash());break}}this._contactContainers(a);d.ui.ddmanager&&d.ui.ddmanager.drag(this,a);this._trigger("sort",a,this._uiHash());this.lastPositionAbs=this.positionAbs;return false},_mouseStop:function(a,b){if(a){d.ui.ddmanager&&!this.options.dropBehaviour&&d.ui.ddmanager.drop(this,a);if(this.options.revert){var c=this;b=c.placeholder.offset();c.reverting=true;d(this.helper).animate({left:b.left-this.offset.parent.left-c.margins.left+(this.offsetParent[0]==
document.body?0:this.offsetParent[0].scrollLeft),top:b.top-this.offset.parent.top-c.margins.top+(this.offsetParent[0]==document.body?0:this.offsetParent[0].scrollTop)},parseInt(this.options.revert,10)||500,function(){c._clear(a)})}else this._clear(a,b);return false}},cancel:function(){var a=this;if(this.dragging){this._mouseUp();this.options.helper=="original"?this.currentItem.css(this._storedCSS).removeClass("ui-sortable-helper"):this.currentItem.show();for(var b=this.containers.length-1;b>=0;b--){this.containers[b]._trigger("deactivate",
null,a._uiHash(this));if(this.containers[b].containerCache.over){this.containers[b]._trigger("out",null,a._uiHash(this));this.containers[b].containerCache.over=0}}}this.placeholder[0].parentNode&&this.placeholder[0].parentNode.removeChild(this.placeholder[0]);this.options.helper!="original"&&this.helper&&this.helper[0].parentNode&&this.helper.remove();d.extend(this,{helper:null,dragging:false,reverting:false,_noFinalSort:null});this.domPosition.prev?d(this.domPosition.prev).after(this.currentItem):
d(this.domPosition.parent).prepend(this.currentItem);return this},serialize:function(a){var b=this._getItemsAsjQuery(a&&a.connected),c=[];a=a||{};d(b).each(function(){var e=(d(a.item||this).attr(a.attribute||"id")||"").match(a.expression||/(.+)[-=_](.+)/);if(e)c.push((a.key||e[1]+"[]")+"="+(a.key&&a.expression?e[1]:e[2]))});return c.join("&")},toArray:function(a){var b=this._getItemsAsjQuery(a&&a.connected),c=[];a=a||{};b.each(function(){c.push(d(a.item||this).attr(a.attribute||"id")||"")});return c},
_intersectsWith:function(a){var b=this.positionAbs.left,c=b+this.helperProportions.width,e=this.positionAbs.top,f=e+this.helperProportions.height,g=a.left,h=g+a.width,i=a.top,k=i+a.height,j=this.offset.click.top,l=this.offset.click.left;j=e+j>i&&e+j<k&&b+l>g&&b+l<h;return this.options.tolerance=="pointer"||this.options.forcePointerForContainers||this.options.tolerance!="pointer"&&this.helperProportions[this.floating?"width":"height"]>a[this.floating?"width":"height"]?j:g<b+this.helperProportions.width/
2&&c-this.helperProportions.width/2<h&&i<e+this.helperProportions.height/2&&f-this.helperProportions.height/2<k},_intersectsWithPointer:function(a){var b=d.ui.isOverAxis(this.positionAbs.top+this.offset.click.top,a.top,a.height);a=d.ui.isOverAxis(this.positionAbs.left+this.offset.click.left,a.left,a.width);b=b&&a;a=this._getDragVerticalDirection();var c=this._getDragHorizontalDirection();if(!b)return false;return this.floating?c&&c=="right"||a=="down"?2:1:a&&(a=="down"?2:1)},_intersectsWithSides:function(a){var b=
d.ui.isOverAxis(this.positionAbs.top+this.offset.click.top,a.top+a.height/2,a.height);a=d.ui.isOverAxis(this.positionAbs.left+this.offset.click.left,a.left+a.width/2,a.width);var c=this._getDragVerticalDirection(),e=this._getDragHorizontalDirection();return this.floating&&e?e=="right"&&a||e=="left"&&!a:c&&(c=="down"&&b||c=="up"&&!b)},_getDragVerticalDirection:function(){var a=this.positionAbs.top-this.lastPositionAbs.top;return a!=0&&(a>0?"down":"up")},_getDragHorizontalDirection:function(){var a=
this.positionAbs.left-this.lastPositionAbs.left;return a!=0&&(a>0?"right":"left")},refresh:function(a){this._refreshItems(a);this.refreshPositions();return this},_connectWith:function(){var a=this.options;return a.connectWith.constructor==String?[a.connectWith]:a.connectWith},_getItemsAsjQuery:function(a){var b=[],c=[],e=this._connectWith();if(e&&a)for(a=e.length-1;a>=0;a--)for(var f=d(e[a]),g=f.length-1;g>=0;g--){var h=d.data(f[g],"sortable");if(h&&h!=this&&!h.options.disabled)c.push([d.isFunction(h.options.items)?
h.options.items.call(h.element):d(h.options.items,h.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"),h])}c.push([d.isFunction(this.options.items)?this.options.items.call(this.element,null,{options:this.options,item:this.currentItem}):d(this.options.items,this.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"),this]);for(a=c.length-1;a>=0;a--)c[a][0].each(function(){b.push(this)});return d(b)},_removeCurrentsFromItems:function(){for(var a=this.currentItem.find(":data(sortable-item)"),
b=0;b<this.items.length;b++)for(var c=0;c<a.length;c++)a[c]==this.items[b].item[0]&&this.items.splice(b,1)},_refreshItems:function(a){this.items=[];this.containers=[this];var b=this.items,c=[[d.isFunction(this.options.items)?this.options.items.call(this.element[0],a,{item:this.currentItem}):d(this.options.items,this.element),this]],e=this._connectWith();if(e)for(var f=e.length-1;f>=0;f--)for(var g=d(e[f]),h=g.length-1;h>=0;h--){var i=d.data(g[h],"sortable");if(i&&i!=this&&!i.options.disabled){c.push([d.isFunction(i.options.items)?
i.options.items.call(i.element[0],a,{item:this.currentItem}):d(i.options.items,i.element),i]);this.containers.push(i)}}for(f=c.length-1;f>=0;f--){a=c[f][1];e=c[f][0];h=0;for(g=e.length;h<g;h++){i=d(e[h]);i.data("sortable-item",a);b.push({item:i,instance:a,width:0,height:0,left:0,top:0})}}},refreshPositions:function(a){if(this.offsetParent&&this.helper)this.offset.parent=this._getParentOffset();for(var b=this.items.length-1;b>=0;b--){var c=this.items[b],e=this.options.toleranceElement?d(this.options.toleranceElement,
c.item):c.item;if(!a){c.width=e.outerWidth();c.height=e.outerHeight()}e=e.offset();c.left=e.left;c.top=e.top}if(this.options.custom&&this.options.custom.refreshContainers)this.options.custom.refreshContainers.call(this);else for(b=this.containers.length-1;b>=0;b--){e=this.containers[b].element.offset();this.containers[b].containerCache.left=e.left;this.containers[b].containerCache.top=e.top;this.containers[b].containerCache.width=this.containers[b].element.outerWidth();this.containers[b].containerCache.height=
this.containers[b].element.outerHeight()}return this},_createPlaceholder:function(a){var b=a||this,c=b.options;if(!c.placeholder||c.placeholder.constructor==String){var e=c.placeholder;c.placeholder={element:function(){var f=d(document.createElement(b.currentItem[0].nodeName)).addClass(e||b.currentItem[0].className+" ui-sortable-placeholder").removeClass("ui-sortable-helper")[0];if(!e)f.style.visibility="hidden";return f},update:function(f,g){if(!(e&&!c.forcePlaceholderSize)){g.height()||g.height(b.currentItem.innerHeight()-
parseInt(b.currentItem.css("paddingTop")||0,10)-parseInt(b.currentItem.css("paddingBottom")||0,10));g.width()||g.width(b.currentItem.innerWidth()-parseInt(b.currentItem.css("paddingLeft")||0,10)-parseInt(b.currentItem.css("paddingRight")||0,10))}}}}b.placeholder=d(c.placeholder.element.call(b.element,b.currentItem));b.currentItem.after(b.placeholder);c.placeholder.update(b,b.placeholder)},_contactContainers:function(a){for(var b=null,c=null,e=this.containers.length-1;e>=0;e--)if(!d.ui.contains(this.currentItem[0],
this.containers[e].element[0]))if(this._intersectsWith(this.containers[e].containerCache)){if(!(b&&d.ui.contains(this.containers[e].element[0],b.element[0]))){b=this.containers[e];c=e}}else if(this.containers[e].containerCache.over){this.containers[e]._trigger("out",a,this._uiHash(this));this.containers[e].containerCache.over=0}if(b)if(this.containers.length===1){this.containers[c]._trigger("over",a,this._uiHash(this));this.containers[c].containerCache.over=1}else if(this.currentContainer!=this.containers[c]){b=
1E4;e=null;for(var f=this.positionAbs[this.containers[c].floating?"left":"top"],g=this.items.length-1;g>=0;g--)if(d.ui.contains(this.containers[c].element[0],this.items[g].item[0])){var h=this.items[g][this.containers[c].floating?"left":"top"];if(Math.abs(h-f)<b){b=Math.abs(h-f);e=this.items[g]}}if(e||this.options.dropOnEmpty){this.currentContainer=this.containers[c];e?this._rearrange(a,e,null,true):this._rearrange(a,null,this.containers[c].element,true);this._trigger("change",a,this._uiHash());this.containers[c]._trigger("change",
a,this._uiHash(this));this.options.placeholder.update(this.currentContainer,this.placeholder);this.containers[c]._trigger("over",a,this._uiHash(this));this.containers[c].containerCache.over=1}}},_createHelper:function(a){var b=this.options;a=d.isFunction(b.helper)?d(b.helper.apply(this.element[0],[a,this.currentItem])):b.helper=="clone"?this.currentItem.clone():this.currentItem;a.parents("body").length||d(b.appendTo!="parent"?b.appendTo:this.currentItem[0].parentNode)[0].appendChild(a[0]);if(a[0]==
this.currentItem[0])this._storedCSS={width:this.currentItem[0].style.width,height:this.currentItem[0].style.height,position:this.currentItem.css("position"),top:this.currentItem.css("top"),left:this.currentItem.css("left")};if(a[0].style.width==""||b.forceHelperSize)a.width(this.currentItem.width());if(a[0].style.height==""||b.forceHelperSize)a.height(this.currentItem.height());return a},_adjustOffsetFromHelper:function(a){if(typeof a=="string")a=a.split(" ");if(d.isArray(a))a={left:+a[0],top:+a[1]||
0};if("left"in a)this.offset.click.left=a.left+this.margins.left;if("right"in a)this.offset.click.left=this.helperProportions.width-a.right+this.margins.left;if("top"in a)this.offset.click.top=a.top+this.margins.top;if("bottom"in a)this.offset.click.top=this.helperProportions.height-a.bottom+this.margins.top},_getParentOffset:function(){this.offsetParent=this.helper.offsetParent();var a=this.offsetParent.offset();if(this.cssPosition=="absolute"&&this.scrollParent[0]!=document&&d.ui.contains(this.scrollParent[0],
this.offsetParent[0])){a.left+=this.scrollParent.scrollLeft();a.top+=this.scrollParent.scrollTop()}if(this.offsetParent[0]==document.body||this.offsetParent[0].tagName&&this.offsetParent[0].tagName.toLowerCase()=="html"&&d.browser.msie)a={top:0,left:0};return{top:a.top+(parseInt(this.offsetParent.css("borderTopWidth"),10)||0),left:a.left+(parseInt(this.offsetParent.css("borderLeftWidth"),10)||0)}},_getRelativeOffset:function(){if(this.cssPosition=="relative"){var a=this.currentItem.position();return{top:a.top-
(parseInt(this.helper.css("top"),10)||0)+this.scrollParent.scrollTop(),left:a.left-(parseInt(this.helper.css("left"),10)||0)+this.scrollParent.scrollLeft()}}else return{top:0,left:0}},_cacheMargins:function(){this.margins={left:parseInt(this.currentItem.css("marginLeft"),10)||0,top:parseInt(this.currentItem.css("marginTop"),10)||0}},_cacheHelperProportions:function(){this.helperProportions={width:this.helper.outerWidth(),height:this.helper.outerHeight()}},_setContainment:function(){var a=this.options;
if(a.containment=="parent")a.containment=this.helper[0].parentNode;if(a.containment=="document"||a.containment=="window")this.containment=[0-this.offset.relative.left-this.offset.parent.left,0-this.offset.relative.top-this.offset.parent.top,d(a.containment=="document"?document:window).width()-this.helperProportions.width-this.margins.left,(d(a.containment=="document"?document:window).height()||document.body.parentNode.scrollHeight)-this.helperProportions.height-this.margins.top];if(!/^(document|window|parent)$/.test(a.containment)){var b=
d(a.containment)[0];a=d(a.containment).offset();var c=d(b).css("overflow")!="hidden";this.containment=[a.left+(parseInt(d(b).css("borderLeftWidth"),10)||0)+(parseInt(d(b).css("paddingLeft"),10)||0)-this.margins.left,a.top+(parseInt(d(b).css("borderTopWidth"),10)||0)+(parseInt(d(b).css("paddingTop"),10)||0)-this.margins.top,a.left+(c?Math.max(b.scrollWidth,b.offsetWidth):b.offsetWidth)-(parseInt(d(b).css("borderLeftWidth"),10)||0)-(parseInt(d(b).css("paddingRight"),10)||0)-this.helperProportions.width-
this.margins.left,a.top+(c?Math.max(b.scrollHeight,b.offsetHeight):b.offsetHeight)-(parseInt(d(b).css("borderTopWidth"),10)||0)-(parseInt(d(b).css("paddingBottom"),10)||0)-this.helperProportions.height-this.margins.top]}},_convertPositionTo:function(a,b){if(!b)b=this.position;a=a=="absolute"?1:-1;var c=this.cssPosition=="absolute"&&!(this.scrollParent[0]!=document&&d.ui.contains(this.scrollParent[0],this.offsetParent[0]))?this.offsetParent:this.scrollParent,e=/(html|body)/i.test(c[0].tagName);return{top:b.top+
this.offset.relative.top*a+this.offset.parent.top*a-(d.browser.safari&&this.cssPosition=="fixed"?0:(this.cssPosition=="fixed"?-this.scrollParent.scrollTop():e?0:c.scrollTop())*a),left:b.left+this.offset.relative.left*a+this.offset.parent.left*a-(d.browser.safari&&this.cssPosition=="fixed"?0:(this.cssPosition=="fixed"?-this.scrollParent.scrollLeft():e?0:c.scrollLeft())*a)}},_generatePosition:function(a){var b=this.options,c=this.cssPosition=="absolute"&&!(this.scrollParent[0]!=document&&d.ui.contains(this.scrollParent[0],
this.offsetParent[0]))?this.offsetParent:this.scrollParent,e=/(html|body)/i.test(c[0].tagName);if(this.cssPosition=="relative"&&!(this.scrollParent[0]!=document&&this.scrollParent[0]!=this.offsetParent[0]))this.offset.relative=this._getRelativeOffset();var f=a.pageX,g=a.pageY;if(this.originalPosition){if(this.containment){if(a.pageX-this.offset.click.left<this.containment[0])f=this.containment[0]+this.offset.click.left;if(a.pageY-this.offset.click.top<this.containment[1])g=this.containment[1]+this.offset.click.top;
if(a.pageX-this.offset.click.left>this.containment[2])f=this.containment[2]+this.offset.click.left;if(a.pageY-this.offset.click.top>this.containment[3])g=this.containment[3]+this.offset.click.top}if(b.grid){g=this.originalPageY+Math.round((g-this.originalPageY)/b.grid[1])*b.grid[1];g=this.containment?!(g-this.offset.click.top<this.containment[1]||g-this.offset.click.top>this.containment[3])?g:!(g-this.offset.click.top<this.containment[1])?g-b.grid[1]:g+b.grid[1]:g;f=this.originalPageX+Math.round((f-
this.originalPageX)/b.grid[0])*b.grid[0];f=this.containment?!(f-this.offset.click.left<this.containment[0]||f-this.offset.click.left>this.containment[2])?f:!(f-this.offset.click.left<this.containment[0])?f-b.grid[0]:f+b.grid[0]:f}}return{top:g-this.offset.click.top-this.offset.relative.top-this.offset.parent.top+(d.browser.safari&&this.cssPosition=="fixed"?0:this.cssPosition=="fixed"?-this.scrollParent.scrollTop():e?0:c.scrollTop()),left:f-this.offset.click.left-this.offset.relative.left-this.offset.parent.left+
(d.browser.safari&&this.cssPosition=="fixed"?0:this.cssPosition=="fixed"?-this.scrollParent.scrollLeft():e?0:c.scrollLeft())}},_rearrange:function(a,b,c,e){c?c[0].appendChild(this.placeholder[0]):b.item[0].parentNode.insertBefore(this.placeholder[0],this.direction=="down"?b.item[0]:b.item[0].nextSibling);this.counter=this.counter?++this.counter:1;var f=this,g=this.counter;window.setTimeout(function(){g==f.counter&&f.refreshPositions(!e)},0)},_clear:function(a,b){this.reverting=false;var c=[];!this._noFinalSort&&
this.currentItem[0].parentNode&&this.placeholder.before(this.currentItem);this._noFinalSort=null;if(this.helper[0]==this.currentItem[0]){for(var e in this._storedCSS)if(this._storedCSS[e]=="auto"||this._storedCSS[e]=="static")this._storedCSS[e]="";this.currentItem.css(this._storedCSS).removeClass("ui-sortable-helper")}else this.currentItem.show();this.fromOutside&&!b&&c.push(function(f){this._trigger("receive",f,this._uiHash(this.fromOutside))});if((this.fromOutside||this.domPosition.prev!=this.currentItem.prev().not(".ui-sortable-helper")[0]||
this.domPosition.parent!=this.currentItem.parent()[0])&&!b)c.push(function(f){this._trigger("update",f,this._uiHash())});if(!d.ui.contains(this.element[0],this.currentItem[0])){b||c.push(function(f){this._trigger("remove",f,this._uiHash())});for(e=this.containers.length-1;e>=0;e--)if(d.ui.contains(this.containers[e].element[0],this.currentItem[0])&&!b){c.push(function(f){return function(g){f._trigger("receive",g,this._uiHash(this))}}.call(this,this.containers[e]));c.push(function(f){return function(g){f._trigger("update",
g,this._uiHash(this))}}.call(this,this.containers[e]))}}for(e=this.containers.length-1;e>=0;e--){b||c.push(function(f){return function(g){f._trigger("deactivate",g,this._uiHash(this))}}.call(this,this.containers[e]));if(this.containers[e].containerCache.over){c.push(function(f){return function(g){f._trigger("out",g,this._uiHash(this))}}.call(this,this.containers[e]));this.containers[e].containerCache.over=0}}this._storedCursor&&d("body").css("cursor",this._storedCursor);this._storedOpacity&&this.helper.css("opacity",
this._storedOpacity);if(this._storedZIndex)this.helper.css("zIndex",this._storedZIndex=="auto"?"":this._storedZIndex);this.dragging=false;if(this.cancelHelperRemoval){if(!b){this._trigger("beforeStop",a,this._uiHash());for(e=0;e<c.length;e++)c[e].call(this,a);this._trigger("stop",a,this._uiHash())}return false}b||this._trigger("beforeStop",a,this._uiHash());this.placeholder[0].parentNode.removeChild(this.placeholder[0]);this.helper[0]!=this.currentItem[0]&&this.helper.remove();this.helper=null;if(!b){for(e=
0;e<c.length;e++)c[e].call(this,a);this._trigger("stop",a,this._uiHash())}this.fromOutside=false;return true},_trigger:function(){d.Widget.prototype._trigger.apply(this,arguments)===false&&this.cancel()},_uiHash:function(a){var b=a||this;return{helper:b.helper,placeholder:b.placeholder||d([]),position:b.position,originalPosition:b.originalPosition,offset:b.positionAbs,item:b.currentItem,sender:a?a.element:null}}});d.extend(d.ui.sortable,{version:"1.8.2"})})(jQuery);
;/*
 * jQuery UI Accordion 1.8.2
 *
 * Copyright (c) 2010 AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT (MIT-LICENSE.txt)
 * and GPL (GPL-LICENSE.txt) licenses.
 *
 * http://docs.jquery.com/UI/Accordion
 *
 * Depends:
 *	jquery.ui.core.js
 *	jquery.ui.widget.js
 */
(function(c){c.widget("ui.accordion",{options:{active:0,animated:"slide",autoHeight:true,clearStyle:false,collapsible:false,event:"click",fillSpace:false,header:"> li > :first-child,> :not(li):even",icons:{header:"ui-icon-triangle-1-e",headerSelected:"ui-icon-triangle-1-s"},navigation:false,navigationFilter:function(){return this.href.toLowerCase()==location.href.toLowerCase()}},_create:function(){var a=this.options,b=this;this.running=0;this.element.addClass("ui-accordion ui-widget ui-helper-reset");
this.element.children("li").addClass("ui-accordion-li-fix");this.headers=this.element.find(a.header).addClass("ui-accordion-header ui-helper-reset ui-state-default ui-corner-all").bind("mouseenter.accordion",function(){c(this).addClass("ui-state-hover")}).bind("mouseleave.accordion",function(){c(this).removeClass("ui-state-hover")}).bind("focus.accordion",function(){c(this).addClass("ui-state-focus")}).bind("blur.accordion",function(){c(this).removeClass("ui-state-focus")});this.headers.next().addClass("ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom");
if(a.navigation){var d=this.element.find("a").filter(a.navigationFilter);if(d.length){var f=d.closest(".ui-accordion-header");this.active=f.length?f:d.closest(".ui-accordion-content").prev()}}this.active=this._findActive(this.active||a.active).toggleClass("ui-state-default").toggleClass("ui-state-active").toggleClass("ui-corner-all").toggleClass("ui-corner-top");this.active.next().addClass("ui-accordion-content-active");this._createIcons();this.resize();this.element.attr("role","tablist");this.headers.attr("role",
"tab").bind("keydown",function(g){return b._keydown(g)}).next().attr("role","tabpanel");this.headers.not(this.active||"").attr("aria-expanded","false").attr("tabIndex","-1").next().hide();this.active.length?this.active.attr("aria-expanded","true").attr("tabIndex","0"):this.headers.eq(0).attr("tabIndex","0");c.browser.safari||this.headers.find("a").attr("tabIndex","-1");a.event&&this.headers.bind(a.event+".accordion",function(g){b._clickHandler.call(b,g,this);g.preventDefault()})},_createIcons:function(){var a=
this.options;if(a.icons){c("<span/>").addClass("ui-icon "+a.icons.header).prependTo(this.headers);this.active.find(".ui-icon").toggleClass(a.icons.header).toggleClass(a.icons.headerSelected);this.element.addClass("ui-accordion-icons")}},_destroyIcons:function(){this.headers.children(".ui-icon").remove();this.element.removeClass("ui-accordion-icons")},destroy:function(){var a=this.options;this.element.removeClass("ui-accordion ui-widget ui-helper-reset").removeAttr("role").unbind(".accordion").removeData("accordion");
this.headers.unbind(".accordion").removeClass("ui-accordion-header ui-helper-reset ui-state-default ui-corner-all ui-state-active ui-corner-top").removeAttr("role").removeAttr("aria-expanded").removeAttr("tabIndex");this.headers.find("a").removeAttr("tabIndex");this._destroyIcons();var b=this.headers.next().css("display","").removeAttr("role").removeClass("ui-helper-reset ui-widget-content ui-corner-bottom ui-accordion-content ui-accordion-content-active");if(a.autoHeight||a.fillHeight)b.css("height",
"");return this},_setOption:function(a,b){c.Widget.prototype._setOption.apply(this,arguments);a=="active"&&this.activate(b);if(a=="icons"){this._destroyIcons();b&&this._createIcons()}},_keydown:function(a){var b=c.ui.keyCode;if(!(this.options.disabled||a.altKey||a.ctrlKey)){var d=this.headers.length,f=this.headers.index(a.target),g=false;switch(a.keyCode){case b.RIGHT:case b.DOWN:g=this.headers[(f+1)%d];break;case b.LEFT:case b.UP:g=this.headers[(f-1+d)%d];break;case b.SPACE:case b.ENTER:this._clickHandler({target:a.target},
a.target);a.preventDefault()}if(g){c(a.target).attr("tabIndex","-1");c(g).attr("tabIndex","0");g.focus();return false}return true}},resize:function(){var a=this.options,b;if(a.fillSpace){if(c.browser.msie){var d=this.element.parent().css("overflow");this.element.parent().css("overflow","hidden")}b=this.element.parent().height();c.browser.msie&&this.element.parent().css("overflow",d);this.headers.each(function(){b-=c(this).outerHeight(true)});this.headers.next().each(function(){c(this).height(Math.max(0,
b-c(this).innerHeight()+c(this).height()))}).css("overflow","auto")}else if(a.autoHeight){b=0;this.headers.next().each(function(){b=Math.max(b,c(this).height())}).height(b)}return this},activate:function(a){this.options.active=a;a=this._findActive(a)[0];this._clickHandler({target:a},a);return this},_findActive:function(a){return a?typeof a=="number"?this.headers.filter(":eq("+a+")"):this.headers.not(this.headers.not(a)):a===false?c([]):this.headers.filter(":eq(0)")},_clickHandler:function(a,b){var d=
this.options;if(!d.disabled)if(a.target){a=c(a.currentTarget||b);b=a[0]==this.active[0];d.active=d.collapsible&&b?false:c(".ui-accordion-header",this.element).index(a);if(!(this.running||!d.collapsible&&b)){this.active.removeClass("ui-state-active ui-corner-top").addClass("ui-state-default ui-corner-all").find(".ui-icon").removeClass(d.icons.headerSelected).addClass(d.icons.header);if(!b){a.removeClass("ui-state-default ui-corner-all").addClass("ui-state-active ui-corner-top").find(".ui-icon").removeClass(d.icons.header).addClass(d.icons.headerSelected);
a.next().addClass("ui-accordion-content-active")}e=a.next();f=this.active.next();g={options:d,newHeader:b&&d.collapsible?c([]):a,oldHeader:this.active,newContent:b&&d.collapsible?c([]):e,oldContent:f};d=this.headers.index(this.active[0])>this.headers.index(a[0]);this.active=b?c([]):a;this._toggle(e,f,g,b,d)}}else if(d.collapsible){this.active.removeClass("ui-state-active ui-corner-top").addClass("ui-state-default ui-corner-all").find(".ui-icon").removeClass(d.icons.headerSelected).addClass(d.icons.header);
this.active.next().addClass("ui-accordion-content-active");var f=this.active.next(),g={options:d,newHeader:c([]),oldHeader:d.active,newContent:c([]),oldContent:f},e=this.active=c([]);this._toggle(e,f,g)}},_toggle:function(a,b,d,f,g){var e=this.options,k=this;this.toShow=a;this.toHide=b;this.data=d;var i=function(){if(k)return k._completed.apply(k,arguments)};this._trigger("changestart",null,this.data);this.running=b.size()===0?a.size():b.size();if(e.animated){d={};d=e.collapsible&&f?{toShow:c([]),
toHide:b,complete:i,down:g,autoHeight:e.autoHeight||e.fillSpace}:{toShow:a,toHide:b,complete:i,down:g,autoHeight:e.autoHeight||e.fillSpace};if(!e.proxied)e.proxied=e.animated;if(!e.proxiedDuration)e.proxiedDuration=e.duration;e.animated=c.isFunction(e.proxied)?e.proxied(d):e.proxied;e.duration=c.isFunction(e.proxiedDuration)?e.proxiedDuration(d):e.proxiedDuration;f=c.ui.accordion.animations;var h=e.duration,j=e.animated;if(j&&!f[j]&&!c.easing[j])j="slide";f[j]||(f[j]=function(l){this.slide(l,{easing:j,
duration:h||700})});f[j](d)}else{if(e.collapsible&&f)a.toggle();else{b.hide();a.show()}i(true)}b.prev().attr("aria-expanded","false").attr("tabIndex","-1").blur();a.prev().attr("aria-expanded","true").attr("tabIndex","0").focus()},_completed:function(a){var b=this.options;this.running=a?0:--this.running;if(!this.running){b.clearStyle&&this.toShow.add(this.toHide).css({height:"",overflow:""});this.toHide.removeClass("ui-accordion-content-active");this._trigger("change",null,this.data)}}});c.extend(c.ui.accordion,
{version:"1.8.2",animations:{slide:function(a,b){a=c.extend({easing:"swing",duration:300},a,b);if(a.toHide.size())if(a.toShow.size()){var d=a.toShow.css("overflow"),f=0,g={},e={},k;b=a.toShow;k=b[0].style.width;b.width(parseInt(b.parent().width(),10)-parseInt(b.css("paddingLeft"),10)-parseInt(b.css("paddingRight"),10)-(parseInt(b.css("borderLeftWidth"),10)||0)-(parseInt(b.css("borderRightWidth"),10)||0));c.each(["height","paddingTop","paddingBottom"],function(i,h){e[h]="hide";i=(""+c.css(a.toShow[0],
h)).match(/^([\d+-.]+)(.*)$/);g[h]={value:i[1],unit:i[2]||"px"}});a.toShow.css({height:0,overflow:"hidden"}).show();a.toHide.filter(":hidden").each(a.complete).end().filter(":visible").animate(e,{step:function(i,h){if(h.prop=="height")f=h.end-h.start===0?0:(h.now-h.start)/(h.end-h.start);a.toShow[0].style[h.prop]=f*g[h.prop].value+g[h.prop].unit},duration:a.duration,easing:a.easing,complete:function(){a.autoHeight||a.toShow.css("height","");a.toShow.css("width",k);a.toShow.css({overflow:d});a.complete()}})}else a.toHide.animate({height:"hide"},
a);else a.toShow.animate({height:"show"},a)},bounceslide:function(a){this.slide(a,{easing:a.down?"easeOutBounce":"swing",duration:a.down?1E3:200})}}})})(jQuery);
;/*
 * jQuery UI Autocomplete 1.8.2
 *
 * Copyright (c) 2010 AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT (MIT-LICENSE.txt)
 * and GPL (GPL-LICENSE.txt) licenses.
 *
 * http://docs.jquery.com/UI/Autocomplete
 *
 * Depends:
 *	jquery.ui.core.js
 *	jquery.ui.widget.js
 *	jquery.ui.position.js
 */
(function(e){e.widget("ui.autocomplete",{options:{minLength:1,delay:300},_create:function(){var a=this,c=this.element[0].ownerDocument;this.element.addClass("ui-autocomplete-input").attr("autocomplete","off").attr({role:"textbox","aria-autocomplete":"list","aria-haspopup":"true"}).bind("keydown.autocomplete",function(d){var b=e.ui.keyCode;switch(d.keyCode){case b.PAGE_UP:a._move("previousPage",d);break;case b.PAGE_DOWN:a._move("nextPage",d);break;case b.UP:a._move("previous",d);d.preventDefault();
break;case b.DOWN:a._move("next",d);d.preventDefault();break;case b.ENTER:case b.NUMPAD_ENTER:a.menu.active&&d.preventDefault();case b.TAB:if(!a.menu.active)return;a.menu.select(d);break;case b.ESCAPE:a.element.val(a.term);a.close(d);break;case b.LEFT:case b.RIGHT:case b.SHIFT:case b.CONTROL:case b.ALT:case b.COMMAND:case b.COMMAND_RIGHT:case b.INSERT:case b.CAPS_LOCK:case b.END:case b.HOME:break;default:clearTimeout(a.searching);a.searching=setTimeout(function(){a.search(null,d)},a.options.delay);
break}}).bind("focus.autocomplete",function(){a.selectedItem=null;a.previous=a.element.val()}).bind("blur.autocomplete",function(d){clearTimeout(a.searching);a.closing=setTimeout(function(){a.close(d);a._change(d)},150)});this._initSource();this.response=function(){return a._response.apply(a,arguments)};this.menu=e("<ul></ul>").addClass("ui-autocomplete").appendTo("body",c).mousedown(function(){setTimeout(function(){clearTimeout(a.closing)},13)}).menu({focus:function(d,b){b=b.item.data("item.autocomplete");
false!==a._trigger("focus",null,{item:b})&&/^key/.test(d.originalEvent.type)&&a.element.val(b.value)},selected:function(d,b){b=b.item.data("item.autocomplete");false!==a._trigger("select",d,{item:b})&&a.element.val(b.value);a.close(d);d=a.previous;if(a.element[0]!==c.activeElement){a.element.focus();a.previous=d}a.selectedItem=b},blur:function(){a.menu.element.is(":visible")&&a.element.val(a.term)}}).zIndex(this.element.zIndex()+1).css({top:0,left:0}).hide().data("menu");e.fn.bgiframe&&this.menu.element.bgiframe()},
destroy:function(){this.element.removeClass("ui-autocomplete-input").removeAttr("autocomplete").removeAttr("role").removeAttr("aria-autocomplete").removeAttr("aria-haspopup");this.menu.element.remove();e.Widget.prototype.destroy.call(this)},_setOption:function(a){e.Widget.prototype._setOption.apply(this,arguments);a==="source"&&this._initSource()},_initSource:function(){var a,c;if(e.isArray(this.options.source)){a=this.options.source;this.source=function(d,b){b(e.ui.autocomplete.filter(a,d.term))}}else if(typeof this.options.source===
"string"){c=this.options.source;this.source=function(d,b){e.getJSON(c,d,b)}}else this.source=this.options.source},search:function(a,c){a=a!=null?a:this.element.val();if(a.length<this.options.minLength)return this.close(c);clearTimeout(this.closing);if(this._trigger("search")!==false)return this._search(a)},_search:function(a){this.term=this.element.addClass("ui-autocomplete-loading").val();this.source({term:a},this.response)},_response:function(a){if(a.length){a=this._normalize(a);this._suggest(a);
this._trigger("open")}else this.close();this.element.removeClass("ui-autocomplete-loading")},close:function(a){clearTimeout(this.closing);if(this.menu.element.is(":visible")){this._trigger("close",a);this.menu.element.hide();this.menu.deactivate()}},_change:function(a){this.previous!==this.element.val()&&this._trigger("change",a,{item:this.selectedItem})},_normalize:function(a){if(a.length&&a[0].label&&a[0].value)return a;return e.map(a,function(c){if(typeof c==="string")return{label:c,value:c};return e.extend({label:c.label||
c.value,value:c.value||c.label},c)})},_suggest:function(a){var c=this.menu.element.empty().zIndex(this.element.zIndex()+1),d;this._renderMenu(c,a);this.menu.deactivate();this.menu.refresh();this.menu.element.show().position({my:"left top",at:"left bottom",of:this.element,collision:"none"});a=c.width("").width();d=this.element.width();c.width(Math.max(a,d))},_renderMenu:function(a,c){var d=this;e.each(c,function(b,f){d._renderItem(a,f)})},_renderItem:function(a,c){return e("<li></li>").data("item.autocomplete",
c).append("<a>"+c.label+"</a>").appendTo(a)},_move:function(a,c){if(this.menu.element.is(":visible"))if(this.menu.first()&&/^previous/.test(a)||this.menu.last()&&/^next/.test(a)){this.element.val(this.term);this.menu.deactivate()}else this.menu[a](c);else this.search(null,c)},widget:function(){return this.menu.element}});e.extend(e.ui.autocomplete,{escapeRegex:function(a){return a.replace(/([\^\$\(\)\[\]\{\}\*\.\+\?\|\\])/gi,"\\$1")},filter:function(a,c){var d=new RegExp(e.ui.autocomplete.escapeRegex(c),
"i");return e.grep(a,function(b){return d.test(b.label||b.value||b)})}})})(jQuery);
(function(e){e.widget("https://www.usbank.com/homepage/homepage_js/ui.menu",{_create:function(){var a=this;this.element.addClass("ui-menu ui-widget ui-widget-content ui-corner-all").attr({role:"listbox","aria-activedescendant":"ui-active-menuitem"}).click(function(c){if(e(c.target).closest(".ui-menu-item a").length){c.preventDefault();a.select(c)}});this.refresh()},refresh:function(){var a=this;this.element.children("li:not(.ui-menu-item):has(a)").addClass("ui-menu-item").attr("role","menuitem").children("a").addClass("ui-corner-all").attr("tabindex",
-1).mouseenter(function(c){a.activate(c,e(this).parent())}).mouseleave(function(){a.deactivate()})},activate:function(a,c){this.deactivate();if(this.hasScroll()){var d=c.offset().top-this.element.offset().top,b=this.element.attr("scrollTop"),f=this.element.height();if(d<0)this.element.attr("scrollTop",b+d);else d>f&&this.element.attr("scrollTop",b+d-f+c.height())}this.active=c.eq(0).children("a").addClass("ui-state-hover").attr("id","ui-active-menuitem").end();this._trigger("focus",a,{item:c})},deactivate:function(){if(this.active){this.active.children("a").removeClass("ui-state-hover").removeAttr("id");
this._trigger("blur");this.active=null}},next:function(a){this.move("next",".ui-menu-item:first",a)},previous:function(a){this.move("prev",".ui-menu-item:last",a)},first:function(){return this.active&&!this.active.prev().length},last:function(){return this.active&&!this.active.next().length},move:function(a,c,d){if(this.active){a=this.active[a+"All"](".ui-menu-item").eq(0);a.length?this.activate(d,a):this.activate(d,this.element.children(c))}else this.activate(d,this.element.children(c))},nextPage:function(a){if(this.hasScroll())if(!this.active||
this.last())this.activate(a,this.element.children(":first"));else{var c=this.active.offset().top,d=this.element.height(),b=this.element.children("li").filter(function(){var f=e(this).offset().top-c-d+e(this).height();return f<10&&f>-10});b.length||(b=this.element.children(":last"));this.activate(a,b)}else this.activate(a,this.element.children(!this.active||this.last()?":first":":last"))},previousPage:function(a){if(this.hasScroll())if(!this.active||this.first())this.activate(a,this.element.children(":last"));
else{var c=this.active.offset().top,d=this.element.height();result=this.element.children("li").filter(function(){var b=e(this).offset().top-c+d-e(this).height();return b<10&&b>-10});result.length||(result=this.element.children(":first"));this.activate(a,result)}else this.activate(a,this.element.children(!this.active||this.first()?":last":":first"))},hasScroll:function(){return this.element.height()<this.element.attr("scrollHeight")},select:function(a){this._trigger("selected",a,{item:this.active})}})})(jQuery);
;/*
 * jQuery UI Button 1.8.2
 *
 * Copyright (c) 2010 AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT (MIT-LICENSE.txt)
 * and GPL (GPL-LICENSE.txt) licenses.
 *
 * http://docs.jquery.com/UI/Button
 *
 * Depends:
 *	jquery.ui.core.js
 *	jquery.ui.widget.js
 */
(function(a){var g,i=function(b){a(":ui-button",b.target.form).each(function(){var c=a(this).data("button");setTimeout(function(){c.refresh()},1)})},h=function(b){var c=b.name,d=b.form,e=a([]);if(c)e=d?a(d).find("[name='"+c+"']"):a("[name='"+c+"']",b.ownerDocument).filter(function(){return!this.form});return e};a.widget("ui.button",{options:{text:true,label:null,icons:{primary:null,secondary:null}},_create:function(){this.element.closest("form").unbind("reset.button").bind("reset.button",i);this._determineButtonType();
this.hasTitle=!!this.buttonElement.attr("title");var b=this,c=this.options,d=this.type==="checkbox"||this.type==="radio",e="ui-state-hover"+(!d?" ui-state-active":"");if(c.label===null)c.label=this.buttonElement.html();if(this.element.is(":disabled"))c.disabled=true;this.buttonElement.addClass("ui-button ui-widget ui-state-default ui-corner-all").attr("role","button").bind("mouseenter.button",function(){if(!c.disabled){a(this).addClass("ui-state-hover");this===g&&a(this).addClass("ui-state-active")}}).bind("mouseleave.button",
function(){c.disabled||a(this).removeClass(e)}).bind("focus.button",function(){a(this).addClass("ui-state-focus")}).bind("blur.button",function(){a(this).removeClass("ui-state-focus")});d&&this.element.bind("change.button",function(){b.refresh()});if(this.type==="checkbox")this.buttonElement.bind("click.button",function(){if(c.disabled)return false;a(this).toggleClass("ui-state-active");b.buttonElement.attr("aria-pressed",b.element[0].checked)});else if(this.type==="radio")this.buttonElement.bind("click.button",
function(){if(c.disabled)return false;a(this).addClass("ui-state-active");b.buttonElement.attr("aria-pressed",true);var f=b.element[0];h(f).not(f).map(function(){return a(this).button("widget")[0]}).removeClass("ui-state-active").attr("aria-pressed",false)});else{this.buttonElement.bind("mousedown.button",function(){if(c.disabled)return false;a(this).addClass("ui-state-active");g=this;a(document).one("mouseup",function(){g=null})}).bind("mouseup.button",function(){if(c.disabled)return false;a(this).removeClass("ui-state-active")}).bind("keydown.button",
function(f){if(c.disabled)return false;if(f.keyCode==a.ui.keyCode.SPACE||f.keyCode==a.ui.keyCode.ENTER)a(this).addClass("ui-state-active")}).bind("keyup.button",function(){a(this).removeClass("ui-state-active")});this.buttonElement.is("a")&&this.buttonElement.keyup(function(f){f.keyCode===a.ui.keyCode.SPACE&&a(this).click()})}this._setOption("disabled",c.disabled)},_determineButtonType:function(){this.type=this.element.is(":checkbox")?"checkbox":this.element.is(":radio")?"radio":this.element.is("input")?
"input":"button";if(this.type==="checkbox"||this.type==="radio"){this.buttonElement=this.element.parents().last().find("[for="+this.element.attr("id")+"]");this.element.addClass("ui-helper-hidden-accessible");var b=this.element.is(":checked");b&&this.buttonElement.addClass("ui-state-active");this.buttonElement.attr("aria-pressed",b)}else this.buttonElement=this.element},widget:function(){return this.buttonElement},destroy:function(){this.element.removeClass("ui-helper-hidden-accessible");this.buttonElement.removeClass("ui-button ui-widget ui-state-default ui-corner-all ui-state-hover ui-state-active  ui-button-icons-only ui-button-icon-only ui-button-text-icons ui-button-text-icon ui-button-text-only").removeAttr("role").removeAttr("aria-pressed").html(this.buttonElement.find(".ui-button-text").html());
this.hasTitle||this.buttonElement.removeAttr("title");a.Widget.prototype.destroy.call(this)},_setOption:function(b,c){a.Widget.prototype._setOption.apply(this,arguments);if(b==="disabled")c?this.element.attr("disabled",true):this.element.removeAttr("disabled");this._resetButton()},refresh:function(){var b=this.element.is(":disabled");b!==this.options.disabled&&this._setOption("disabled",b);if(this.type==="radio")h(this.element[0]).each(function(){a(this).is(":checked")?a(this).button("widget").addClass("ui-state-active").attr("aria-pressed",
true):a(this).button("widget").removeClass("ui-state-active").attr("aria-pressed",false)});else if(this.type==="checkbox")this.element.is(":checked")?this.buttonElement.addClass("ui-state-active").attr("aria-pressed",true):this.buttonElement.removeClass("ui-state-active").attr("aria-pressed",false)},_resetButton:function(){if(this.type==="input")this.options.label&&this.element.val(this.options.label);else{var b=this.buttonElement.removeClass("ui-button-icons-only ui-button-icon-only ui-button-text-icons ui-button-text-icon ui-button-text-only"),
c=a("<span></span>").addClass("ui-button-text").html(this.options.label).appendTo(b.empty()).text(),d=this.options.icons,e=d.primary&&d.secondary;if(d.primary||d.secondary){b.addClass("ui-button-text-icon"+(e?"s":""));d.primary&&b.prepend("<span class='ui-button-icon-primary ui-icon "+d.primary+"'></span>");d.secondary&&b.append("<span class='ui-button-icon-secondary ui-icon "+d.secondary+"'></span>");if(!this.options.text){b.addClass(e?"ui-button-icons-only":"ui-button-icon-only").removeClass("ui-button-text-icons ui-button-text-icon");
this.hasTitle||b.attr("title",c)}}else b.addClass("ui-button-text-only")}}});a.widget("ui.buttonset",{_create:function(){this.element.addClass("ui-buttonset");this._init()},_init:function(){this.refresh()},_setOption:function(b,c){b==="disabled"&&this.buttons.button("option",b,c);a.Widget.prototype._setOption.apply(this,arguments)},refresh:function(){this.buttons=this.element.find(":button, :submit, :reset, :checkbox, :radio, a, :data(button)").filter(":ui-button").button("refresh").end().not(":ui-button").button().end().map(function(){return a(this).button("widget")[0]}).removeClass("ui-corner-all ui-corner-left ui-corner-right").filter(":first").addClass("ui-corner-left").end().filter(":last").addClass("ui-corner-right").end().end()},
destroy:function(){this.element.removeClass("ui-buttonset");this.buttons.map(function(){return a(this).button("widget")[0]}).removeClass("ui-corner-left ui-corner-right").end().button("destroy");a.Widget.prototype.destroy.call(this)}})})(jQuery);
;/*
 * jQuery UI Dialog 1.8.2
 *
 * Copyright (c) 2010 AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT (MIT-LICENSE.txt)
 * and GPL (GPL-LICENSE.txt) licenses.
 *
 * http://docs.jquery.com/UI/Dialog
 *
 * Depends:
 *	jquery.ui.core.js
 *	jquery.ui.widget.js
 *  jquery.ui.button.js
 *	jquery.ui.draggable.js
 *	jquery.ui.mouse.js
 *	jquery.ui.position.js
 *	jquery.ui.resizable.js
 */
(function(c){c.widget("ui.dialog",{options:{autoOpen:true,buttons:{},closeOnEscape:true,closeText:"close",dialogClass:"",draggable:true,hide:null,height:"auto",maxHeight:false,maxWidth:false,minHeight:150,minWidth:150,modal:false,position:"center",resizable:true,show:null,stack:true,title:"",width:300,zIndex:1E3},_create:function(){this.originalTitle=this.element.attr("title");var a=this,b=a.options,d=b.title||a.originalTitle||"&#160;",e=c.ui.dialog.getTitleId(a.element),g=(a.uiDialog=c("<div></div>")).appendTo(document.body).hide().addClass("ui-dialog ui-widget ui-widget-content ui-corner-all "+
b.dialogClass).css({zIndex:b.zIndex}).attr("tabIndex",-1).css("outline",0).keydown(function(i){if(b.closeOnEscape&&i.keyCode&&i.keyCode===c.ui.keyCode.ESCAPE){a.close(i);i.preventDefault()}}).attr({role:"dialog","aria-labelledby":e}).mousedown(function(i){a.moveToTop(false,i)});a.element.show().removeAttr("title").addClass("ui-dialog-content ui-widget-content").appendTo(g);var f=(a.uiDialogTitlebar=c("<div></div>")).addClass("ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix").prependTo(g),
h=c('<a href="#"></a>').addClass("ui-dialog-titlebar-close ui-corner-all").attr("role","button").hover(function(){h.addClass("ui-state-hover")},function(){h.removeClass("ui-state-hover")}).focus(function(){h.addClass("ui-state-focus")}).blur(function(){h.removeClass("ui-state-focus")}).click(function(i){a.close(i);return false}).appendTo(f);(a.uiDialogTitlebarCloseText=c("<span></span>")).addClass("ui-icon ui-icon-closethick").text(b.closeText).appendTo(h);c("<span></span>").addClass("ui-dialog-title").attr("id",
e).html(d).prependTo(f);if(c.isFunction(b.beforeclose)&&!c.isFunction(b.beforeClose))b.beforeClose=b.beforeclose;f.find("*").add(f).disableSelection();b.draggable&&c.fn.draggable&&a._makeDraggable();b.resizable&&c.fn.resizable&&a._makeResizable();a._createButtons(b.buttons);a._isOpen=false;c.fn.bgiframe&&g.bgiframe()},_init:function(){this.options.autoOpen&&this.open()},destroy:function(){var a=this;a.overlay&&a.overlay.destroy();a.uiDialog.hide();a.element.unbind(".dialog").removeData("dialog").removeClass("ui-dialog-content ui-widget-content").hide().appendTo("body");
a.uiDialog.remove();a.originalTitle&&a.element.attr("title",a.originalTitle);return a},widget:function(){return this.uiDialog},close:function(a){var b=this,d;if(false!==b._trigger("beforeClose",a)){b.overlay&&b.overlay.destroy();b.uiDialog.unbind("keypress.ui-dialog");b._isOpen=false;if(b.options.hide)b.uiDialog.hide(b.options.hide,function(){b._trigger("close",a)});else{b.uiDialog.hide();b._trigger("close",a)}c.ui.dialog.overlay.resize();if(b.options.modal){d=0;c(".ui-dialog").each(function(){if(this!==
b.uiDialog[0])d=Math.max(d,c(this).css("z-index"))});c.ui.dialog.maxZ=d}return b}},isOpen:function(){return this._isOpen},moveToTop:function(a,b){var d=this,e=d.options;if(e.modal&&!a||!e.stack&&!e.modal)return d._trigger("focus",b);if(e.zIndex>c.ui.dialog.maxZ)c.ui.dialog.maxZ=e.zIndex;if(d.overlay){c.ui.dialog.maxZ+=1;d.overlay.$el.css("z-index",c.ui.dialog.overlay.maxZ=c.ui.dialog.maxZ)}a={scrollTop:d.element.attr("scrollTop"),scrollLeft:d.element.attr("scrollLeft")};c.ui.dialog.maxZ+=1;d.uiDialog.css("z-index",
c.ui.dialog.maxZ);d.element.attr(a);d._trigger("focus",b);return d},open:function(){if(!this._isOpen){var a=this,b=a.options,d=a.uiDialog;a.overlay=b.modal?new c.ui.dialog.overlay(a):null;d.next().length&&d.appendTo("body");a._size();a._position(b.position);d.show(b.show);a.moveToTop(true);b.modal&&d.bind("keypress.ui-dialog",function(e){if(e.keyCode===c.ui.keyCode.TAB){var g=c(":tabbable",this),f=g.filter(":first");g=g.filter(":last");if(e.target===g[0]&&!e.shiftKey){f.focus(1);return false}else if(e.target===
f[0]&&e.shiftKey){g.focus(1);return false}}});c([]).add(d.find(".ui-dialog-content :tabbable:first")).add(d.find(".ui-dialog-buttonpane :tabbable:first")).add(d).filter(":first").focus();a._trigger("open");a._isOpen=true;return a}},_createButtons:function(a){var b=this,d=false,e=c("<div></div>").addClass("ui-dialog-buttonpane ui-widget-content ui-helper-clearfix");b.uiDialog.find(".ui-dialog-buttonpane").remove();typeof a==="object"&&a!==null&&c.each(a,function(){return!(d=true)});if(d){c.each(a,
function(g,f){g=c('<button type="button"></button>').text(g).click(function(){f.apply(b.element[0],arguments)}).appendTo(e);c.fn.button&&g.button()});e.appendTo(b.uiDialog)}},_makeDraggable:function(){function a(f){return{position:f.position,offset:f.offset}}var b=this,d=b.options,e=c(document),g;b.uiDialog.draggable({cancel:".ui-dialog-content, .ui-dialog-titlebar-close",handle:".ui-dialog-titlebar",containment:"document",start:function(f,h){g=d.height==="auto"?"auto":c(this).height();c(this).height(c(this).height()).addClass("ui-dialog-dragging");
b._trigger("dragStart",f,a(h))},drag:function(f,h){b._trigger("drag",f,a(h))},stop:function(f,h){d.position=[h.position.left-e.scrollLeft(),h.position.top-e.scrollTop()];c(this).removeClass("ui-dialog-dragging").height(g);b._trigger("dragStop",f,a(h));c.ui.dialog.overlay.resize()}})},_makeResizable:function(a){function b(f){return{originalPosition:f.originalPosition,originalSize:f.originalSize,position:f.position,size:f.size}}a=a===undefined?this.options.resizable:a;var d=this,e=d.options,g=d.uiDialog.css("position");
a=typeof a==="string"?a:"n,e,s,w,se,sw,ne,nw";d.uiDialog.resizable({cancel:".ui-dialog-content",containment:"document",alsoResize:d.element,maxWidth:e.maxWidth,maxHeight:e.maxHeight,minWidth:e.minWidth,minHeight:d._minHeight(),handles:a,start:function(f,h){c(this).addClass("ui-dialog-resizing");d._trigger("resizeStart",f,b(h))},resize:function(f,h){d._trigger("resize",f,b(h))},stop:function(f,h){c(this).removeClass("ui-dialog-resizing");e.height=c(this).height();e.width=c(this).width();d._trigger("resizeStop",
f,b(h));c.ui.dialog.overlay.resize()}}).css("position",g).find(".ui-resizable-se").addClass("ui-icon ui-icon-grip-diagonal-se")},_minHeight:function(){var a=this.options;return a.height==="auto"?a.minHeight:Math.min(a.minHeight,a.height)},_position:function(a){var b=[],d=[0,0];a=a||c.ui.dialog.prototype.options.position;if(typeof a==="string"||typeof a==="object"&&"0"in a){b=a.split?a.split(" "):[a[0],a[1]];if(b.length===1)b[1]=b[0];c.each(["left","top"],function(e,g){if(+b[e]===b[e]){d[e]=b[e];b[e]=
g}})}else if(typeof a==="object"){if("left"in a){b[0]="left";d[0]=a.left}else if("right"in a){b[0]="right";d[0]=-a.right}if("top"in a){b[1]="top";d[1]=a.top}else if("bottom"in a){b[1]="bottom";d[1]=-a.bottom}}(a=this.uiDialog.is(":visible"))||this.uiDialog.show();this.uiDialog.css({top:0,left:0}).position({my:b.join(" "),at:b.join(" "),offset:d.join(" "),of:window,collision:"fit",using:function(e){var g=c(this).css(e).offset().top;g<0&&c(this).css("top",e.top-g)}});a||this.uiDialog.hide()},_setOption:function(a,
b){var d=this,e=d.uiDialog,g=e.is(":data(resizable)"),f=false;switch(a){case "beforeclose":a="beforeClose";break;case "buttons":d._createButtons(b);break;case "closeText":d.uiDialogTitlebarCloseText.text(""+b);break;case "dialogClass":e.removeClass(d.options.dialogClass).addClass("ui-dialog ui-widget ui-widget-content ui-corner-all "+b);break;case "disabled":b?e.addClass("ui-dialog-disabled"):e.removeClass("ui-dialog-disabled");break;case "draggable":b?d._makeDraggable():e.draggable("destroy");break;
case "height":f=true;break;case "maxHeight":g&&e.resizable("option","maxHeight",b);f=true;break;case "maxWidth":g&&e.resizable("option","maxWidth",b);f=true;break;case "minHeight":g&&e.resizable("option","minHeight",b);f=true;break;case "minWidth":g&&e.resizable("option","minWidth",b);f=true;break;case "position":d._position(b);break;case "resizable":g&&!b&&e.resizable("destroy");g&&typeof b==="string"&&e.resizable("option","handles",b);!g&&b!==false&&d._makeResizable(b);break;case "title":c(".ui-dialog-title",
d.uiDialogTitlebar).html(""+(b||"&#160;"));break;case "width":f=true;break}c.Widget.prototype._setOption.apply(d,arguments);f&&d._size()},_size:function(){var a=this.options,b;this.element.css({width:"auto",minHeight:0,height:0});b=this.uiDialog.css({height:"auto",width:a.width}).height();this.element.css(a.height==="auto"?{minHeight:Math.max(a.minHeight-b,0),height:"auto"}:{minHeight:0,height:Math.max(a.height-b,0)}).show();this.uiDialog.is(":data(resizable)")&&this.uiDialog.resizable("option","minHeight",
this._minHeight())}});c.extend(c.ui.dialog,{version:"1.8.2",uuid:0,maxZ:0,getTitleId:function(a){a=a.attr("id");if(!a){this.uuid+=1;a=this.uuid}return"ui-dialog-title-"+a},overlay:function(a){this.$el=c.ui.dialog.overlay.create(a)}});c.extend(c.ui.dialog.overlay,{instances:[],oldInstances:[],maxZ:0,events:c.map("focus,mousedown,mouseup,keydown,keypress,click".split(","),function(a){return a+".dialog-overlay"}).join(" "),create:function(a){if(this.instances.length===0){setTimeout(function(){c.ui.dialog.overlay.instances.length&&
c(document).bind(c.ui.dialog.overlay.events,function(d){return c(d.target).zIndex()>=c.ui.dialog.overlay.maxZ})},1);c(document).bind("keydown.dialog-overlay",function(d){if(a.options.closeOnEscape&&d.keyCode&&d.keyCode===c.ui.keyCode.ESCAPE){a.close(d);d.preventDefault()}});c(window).bind("resize.dialog-overlay",c.ui.dialog.overlay.resize)}var b=(this.oldInstances.pop()||c("<div></div>").addClass("ui-widget-overlay")).appendTo(document.body).css({width:this.width(),height:this.height()});c.fn.bgiframe&&
b.bgiframe();this.instances.push(b);return b},destroy:function(a){this.oldInstances.push(this.instances.splice(c.inArray(a,this.instances),1)[0]);this.instances.length===0&&c([document,window]).unbind(".dialog-overlay");a.remove();var b=0;c.each(this.instances,function(){b=Math.max(b,this.css("z-index"))});this.maxZ=b},height:function(){var a,b;if(c.browser.msie&&c.browser.version<7){a=Math.max(document.documentElement.scrollHeight,document.body.scrollHeight);b=Math.max(document.documentElement.offsetHeight,
document.body.offsetHeight);return a<b?c(window).height()+"px":a+"px"}else return c(document).height()+"px"},width:function(){var a,b;if(c.browser.msie&&c.browser.version<7){a=Math.max(document.documentElement.scrollWidth,document.body.scrollWidth);b=Math.max(document.documentElement.offsetWidth,document.body.offsetWidth);return a<b?c(window).width()+"px":a+"px"}else return c(document).width()+"px"},resize:function(){var a=c([]);c.each(c.ui.dialog.overlay.instances,function(){a=a.add(this)});a.css({width:0,
height:0}).css({width:c.ui.dialog.overlay.width(),height:c.ui.dialog.overlay.height()})}});c.extend(c.ui.dialog.overlay.prototype,{destroy:function(){c.ui.dialog.overlay.destroy(this.$el)}})})(jQuery);
;


/*
 * jQuery UI Tabs 1.8.2
 *
 * Copyright (c) 2010 AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT (MIT-LICENSE.txt)
 * and GPL (GPL-LICENSE.txt) licenses.
 *
 * http://docs.jquery.com/UI/Tabs
 *
 * Depends:
 *	jquery.ui.core.js
 *	jquery.ui.widget.js
 */
(function(d){function s(){return++u}function v(){return++w}var u=0,w=0;d.widget("https://www.usbank.com/homepage/homepage_js/ui.tabs",{options:{add:null,ajaxOptions:null,cache:false,cookie:null,collapsible:false,disable:null,disabled:[],enable:null,event:"click",fx:null,idPrefix:"ui-tabs-",load:null,panelTemplate:"<div></div>",remove:null,select:null,show:null,spinner:"<em>Loading&#8230;</em>",tabTemplate:'<li><a href="#{href}"><span>#{label}</span></a></li>'},_create:function(){this._tabify(true)},_setOption:function(c,e){if(c=="selected")this.options.collapsible&&
e==this.options.selected||this.select(e);else{this.options[c]=e;this._tabify()}},_tabId:function(c){return c.title&&c.title.replace(/\s/g,"_").replace(/[^A-Za-z0-9\-_:\.]/g,"")||this.options.idPrefix+s()},_sanitizeSelector:function(c){return c.replace(/:/g,"\\:")},_cookie:function(){var c=this.cookie||(this.cookie=this.options.cookie.name||"ui-tabs-"+v());return d.cookie.apply(null,[c].concat(d.makeArray(arguments)))},_ui:function(c,e){return{tab:c,panel:e,index:this.anchors.index(c)}},_cleanup:function(){this.lis.filter(".ui-state-processing").removeClass("ui-state-processing").find("span:data(label.tabs)").each(function(){var c=
d(this);c.html(c.data("https://www.usbank.com/homepage/homepage_js/label.tabs")).removeData("https://www.usbank.com/homepage/homepage_js/label.tabs")})},_tabify:function(c){function e(g,f){g.css({display:""});!d.support.opacity&&f.opacity&&g[0].style.removeAttribute("filter")}this.list=this.element.find("ol,ul").eq(0);this.lis=d("li:has(a[href])",this.list);this.anchors=this.lis.map(function(){return d("a",this)[0]});this.panels=d([]);var a=this,b=this.options,h=/^#.+/;this.anchors.each(function(g,f){var j=d(f).attr("href"),l=j.split("#")[0],p;if(l&&(l===location.toString().split("#")[0]||
(p=d("base")[0])&&l===p.href)){j=f.hash;f.href=j}if(h.test(j))a.panels=a.panels.add(a._sanitizeSelector(j));else if(j!="#"){d.data(f,"https://www.usbank.com/homepage/homepage_js/href.tabs",j);d.data(f,"https://www.usbank.com/homepage/homepage_js/load.tabs",j.replace(/#.*$/,""));j=a._tabId(f);f.href="#"+j;f=d("#"+j);if(!f.length){f=d(b.panelTemplate).attr("id",j).addClass("ui-tabs-panel ui-widget-content ui-corner-bottom").insertAfter(a.panels[g-1]||a.list);f.data("https://www.usbank.com/homepage/homepage_js/destroy.tabs",true)}a.panels=a.panels.add(f)}else b.disabled.push(g)});if(c){this.element.addClass("ui-tabs ui-widget ui-widget-content ui-corner-all");
this.list.addClass("ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all");this.lis.addClass("ui-state-default ui-corner-top");this.panels.addClass("ui-tabs-panel ui-widget-content ui-corner-bottom");if(b.selected===undefined){location.hash&&this.anchors.each(function(g,f){if(f.hash==location.hash){b.selected=g;return false}});if(typeof b.selected!="number"&&b.cookie)b.selected=parseInt(a._cookie(),10);if(typeof b.selected!="number"&&this.lis.filter(".ui-tabs-selected").length)b.selected=
this.lis.index(this.lis.filter(".ui-tabs-selected"));b.selected=b.selected||(this.lis.length?0:-1)}else if(b.selected===null)b.selected=-1;b.selected=b.selected>=0&&this.anchors[b.selected]||b.selected<0?b.selected:0;b.disabled=d.unique(b.disabled.concat(d.map(this.lis.filter(".ui-state-disabled"),function(g){return a.lis.index(g)}))).sort();d.inArray(b.selected,b.disabled)!=-1&&b.disabled.splice(d.inArray(b.selected,b.disabled),1);this.panels.addClass("ui-tabs-hide");this.lis.removeClass("ui-tabs-selected ui-state-active");
if(b.selected>=0&&this.anchors.length){this.panels.eq(b.selected).removeClass("ui-tabs-hide");this.lis.eq(b.selected).addClass("ui-tabs-selected ui-state-active");a.element.queue("tabs",function(){a._trigger("show",null,a._ui(a.anchors[b.selected],a.panels[b.selected]))});this.load(b.selected)}d(window).bind("unload",function(){a.lis.add(a.anchors).unbind(".tabs");a.lis=a.anchors=a.panels=null})}else b.selected=this.lis.index(this.lis.filter(".ui-tabs-selected"));this.element[b.collapsible?"addClass":
"removeClass"]("ui-tabs-collapsible");b.cookie&&this._cookie(b.selected,b.cookie);c=0;for(var i;i=this.lis[c];c++)d(i)[d.inArray(c,b.disabled)!=-1&&!d(i).hasClass("ui-tabs-selected")?"addClass":"removeClass"]("ui-state-disabled");b.cache===false&&this.anchors.removeData("https://www.usbank.com/homepage/homepage_js/cache.tabs");this.lis.add(this.anchors).unbind(".tabs");if(b.event!="mouseover"){var k=function(g,f){f.is(":not(.ui-state-disabled)")&&f.addClass("ui-state-"+g)},n=function(g,f){f.removeClass("ui-state-"+g)};this.lis.bind("https://www.usbank.com/homepage/homepage_js/mouseover.tabs",
function(){k("hover",d(this))});this.lis.bind("https://www.usbank.com/homepage/homepage_js/mouseout.tabs",function(){n("hover",d(this))});this.anchors.bind("https://www.usbank.com/homepage/homepage_js/focus.tabs",function(){k("focus",d(this).closest("li"))});this.anchors.bind("https://www.usbank.com/homepage/homepage_js/blur.tabs",function(){n("focus",d(this).closest("li"))})}var m,o;if(b.fx)if(d.isArray(b.fx)){m=b.fx[0];o=b.fx[1]}else m=o=b.fx;var q=o?function(g,f){d(g).closest("li").addClass("ui-tabs-selected ui-state-active");f.hide().removeClass("ui-tabs-hide").animate(o,o.duration||"normal",function(){e(f,o);a._trigger("show",
null,a._ui(g,f[0]))})}:function(g,f){d(g).closest("li").addClass("ui-tabs-selected ui-state-active");f.removeClass("ui-tabs-hide");a._trigger("show",null,a._ui(g,f[0]))},r=m?function(g,f){f.animate(m,m.duration||"normal",function(){a.lis.removeClass("ui-tabs-selected ui-state-active");f.addClass("ui-tabs-hide");e(f,m);a.element.dequeue("tabs")})}:function(g,f){a.lis.removeClass("ui-tabs-selected ui-state-active");f.addClass("ui-tabs-hide");a.element.dequeue("tabs")};this.anchors.bind(b.event+".tabs",
function(){var g=this,f=d(this).closest("li"),j=a.panels.filter(":not(.ui-tabs-hide)"),l=d(a._sanitizeSelector(this.hash));if(f.hasClass("ui-tabs-selected")&&!b.collapsible||f.hasClass("ui-state-disabled")||f.hasClass("ui-state-processing")||a._trigger("select",null,a._ui(this,l[0]))===false){this.blur();return false}b.selected=a.anchors.index(this);a.abort();if(b.collapsible)if(f.hasClass("ui-tabs-selected")){b.selected=-1;b.cookie&&a._cookie(b.selected,b.cookie);a.element.queue("tabs",function(){r(g,
j)}).dequeue("tabs");this.blur();return false}else if(!j.length){b.cookie&&a._cookie(b.selected,b.cookie);a.element.queue("tabs",function(){q(g,l)});a.load(a.anchors.index(this));this.blur();return false}b.cookie&&a._cookie(b.selected,b.cookie);if(l.length){j.length&&a.element.queue("tabs",function(){r(g,j)});a.element.queue("tabs",function(){q(g,l)});a.load(a.anchors.index(this))}else throw"jQuery UI Tabs: Mismatching fragment identifier.";d.browser.msie&&this.blur()});this.anchors.bind("https://www.usbank.com/homepage/homepage_js/click.tabs",
function(){return false})},destroy:function(){var c=this.options;this.abort();this.element.unbind(".tabs").removeClass("ui-tabs ui-widget ui-widget-content ui-corner-all ui-tabs-collapsible").removeData("tabs");this.list.removeClass("ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all");this.anchors.each(function(){var e=d.data(this,"https://www.usbank.com/homepage/homepage_js/href.tabs");if(e)this.href=e;var a=d(this).unbind(".tabs");d.each(["href","load","cache"],function(b,h){a.removeData(h+".tabs")})});this.lis.unbind(".tabs").add(this.panels).each(function(){d.data(this,
"https://www.usbank.com/homepage/homepage_js/destroy.tabs")?d(this).remove():d(this).removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active ui-state-hover ui-state-focus ui-state-disabled ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide")});c.cookie&&this._cookie(null,c.cookie);return this},add:function(c,e,a){if(a===undefined)a=this.anchors.length;var b=this,h=this.options;e=d(h.tabTemplate.replace(/#\{href\}/g,c).replace(/#\{label\}/g,e));c=!c.indexOf("#")?c.replace("#",""):this._tabId(d("a",e)[0]);e.addClass("ui-state-default ui-corner-top").data("https://www.usbank.com/homepage/homepage_js/destroy.tabs",
true);var i=d("#"+c);i.length||(i=d(h.panelTemplate).attr("id",c).data("https://www.usbank.com/homepage/homepage_js/destroy.tabs",true));i.addClass("ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide");if(a>=this.lis.length){e.appendTo(this.list);i.appendTo(this.list[0].parentNode)}else{e.insertBefore(this.lis[a]);i.insertBefore(this.panels[a])}h.disabled=d.map(h.disabled,function(k){return k>=a?++k:k});this._tabify();if(this.anchors.length==1){h.selected=0;e.addClass("ui-tabs-selected ui-state-active");i.removeClass("ui-tabs-hide");
this.element.queue("tabs",function(){b._trigger("show",null,b._ui(b.anchors[0],b.panels[0]))});this.load(0)}this._trigger("add",null,this._ui(this.anchors[a],this.panels[a]));return this},remove:function(c){var e=this.options,a=this.lis.eq(c).remove(),b=this.panels.eq(c).remove();if(a.hasClass("ui-tabs-selected")&&this.anchors.length>1)this.select(c+(c+1<this.anchors.length?1:-1));e.disabled=d.map(d.grep(e.disabled,function(h){return h!=c}),function(h){return h>=c?--h:h});this._tabify();this._trigger("remove",
null,this._ui(a.find("a")[0],b[0]));return this},enable:function(c){var e=this.options;if(d.inArray(c,e.disabled)!=-1){this.lis.eq(c).removeClass("ui-state-disabled");e.disabled=d.grep(e.disabled,function(a){return a!=c});this._trigger("enable",null,this._ui(this.anchors[c],this.panels[c]));return this}},disable:function(c){var e=this.options;if(c!=e.selected){this.lis.eq(c).addClass("ui-state-disabled");e.disabled.push(c);e.disabled.sort();this._trigger("disable",null,this._ui(this.anchors[c],this.panels[c]))}return this},
select:function(c){if(typeof c=="string")c=this.anchors.index(this.anchors.filter("[href$="+c+"]"));else if(c===null)c=-1;if(c==-1&&this.options.collapsible)c=this.options.selected;this.anchors.eq(c).trigger(this.options.event+".tabs");return this},load:function(c){var e=this,a=this.options,b=this.anchors.eq(c)[0],h=d.data(b,"https://www.usbank.com/homepage/homepage_js/load.tabs");this.abort();if(!h||this.element.queue("tabs").length!==0&&d.data(b,"https://www.usbank.com/homepage/homepage_js/cache.tabs"))this.element.dequeue("tabs");else{this.lis.eq(c).addClass("ui-state-processing");
if(a.spinner){var i=d("span",b);i.data("https://www.usbank.com/homepage/homepage_js/label.tabs",i.html()).html(a.spinner)}this.xhr=d.ajax(d.extend({},a.ajaxOptions,{url:h,success:function(k,n){d(e._sanitizeSelector(b.hash)).html(k);e._cleanup();a.cache&&d.data(b,"https://www.usbank.com/homepage/homepage_js/cache.tabs",true);e._trigger("load",null,e._ui(e.anchors[c],e.panels[c]));try{a.ajaxOptions.success(k,n)}catch(m){}},error:function(k,n){e._cleanup();e._trigger("load",null,e._ui(e.anchors[c],e.panels[c]));try{a.ajaxOptions.error(k,n,c,b)}catch(m){}}}));e.element.dequeue("tabs");return this}},
abort:function(){this.element.queue([]);this.panels.stop(false,true);this.element.queue("tabs",this.element.queue("tabs").splice(-2,2));if(this.xhr){this.xhr.abort();delete this.xhr}this._cleanup();return this},url:function(c,e){this.anchors.eq(c).removeData("https://www.usbank.com/homepage/homepage_js/cache.tabs").data("https://www.usbank.com/homepage/homepage_js/load.tabs",e);return this},length:function(){return this.anchors.length}});d.extend(d.ui.tabs,{version:"1.8.2"});d.extend(d.ui.tabs.prototype,{rotation:null,rotate:function(c,e){var a=this,b=this.options,h=a._rotate||(a._rotate=
function(i){clearTimeout(a.rotation);a.rotation=setTimeout(function(){var k=b.selected;a.select(++k<a.anchors.length?k:0)},c);i&&i.stopPropagation()});e=a._unrotate||(a._unrotate=!e?function(i){i.clientX&&a.rotate(null)}:function(){t=b.selected;h()});if(c){this.element.bind("tabsshow",h);this.anchors.bind(b.event+".tabs",e);h()}else{clearTimeout(a.rotation);this.element.unbind("tabsshow",h);this.anchors.unbind(b.event+".tabs",e);delete this._rotate;delete this._unrotate}return this}})})(jQuery);
;

/*
 * jQuery UI Tabs 1.8.13
 *
 * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI/Tabs
 *
 * Depends:
 *	jquery.ui.core.js
 *	jquery.ui.widget.js
 */
(function($,undefined){var tabId=0,listId=0;function getNextTabId(){return++tabId}function getNextListId(){return++listId}$.widget("https://www.usbank.com/homepage/homepage_js/ui.tabs",{options:{add:null,ajaxOptions:null,cache:false,cookie:null,collapsible:false,disable:null,disabled:[],enable:null,event:"click",fx:null,idPrefix:"ui-tabs-",load:null,panelTemplate:"<div></div>",remove:null,select:null,show:null,spinner:"<em>Loading&#8230;</em>",tabTemplate:"<li><a href='#{href}'><span>#{label}</span></a></li>"},_create:function(){this._tabify(true)},_setOption:function(key,value){if(key=="selected"){if(this.options.collapsible&&value==this.options.selected){return}this.select(value)}else{this.options[key]=value;this._tabify()}},_tabId:function(a){return a.title&&a.title.replace(/\s/g,"_").replace(/[^\w\u00c0-\uFFFF-]/g,"")||this.options.idPrefix+getNextTabId()},_sanitizeSelector:function(hash){return hash.replace(/:/g,"\\:")},_cookie:function(){var cookie=this.cookie||(this.cookie=this.options.cookie.name||"ui-tabs-"+getNextListId());return $.cookie.apply(null,[cookie].concat($.makeArray(arguments)))},_ui:function(tab,panel){return{tab:tab,panel:panel,index:this.anchors.index(tab)}},_cleanup:function(){this.lis.filter(".ui-state-processing").removeClass("ui-state-processing").find("span:data(label.tabs)").each(function(){var el=$(this);el.html(el.data("https://www.usbank.com/homepage/homepage_js/label.tabs")).removeData("https://www.usbank.com/homepage/homepage_js/label.tabs")})},_tabify:function(init){var self=this,o=this.options,fragmentId=/^#.+/;this.list=this.element.find("ol,ul").eq(0);this.lis=$(" > li:has(a[href])",this.list);this.anchors=this.lis.map(function(){return $("a",this)[0]});this.panels=$([]);this.anchors.each(function(i,a){var href=$(a).attr("href");var hrefBase=href.split("#")[0],baseEl;if(hrefBase&&(hrefBase===location.toString().split("#")[0]||(baseEl=$("base")[0])&&hrefBase===baseEl.href)){href=a.hash;a.href=href}if(fragmentId.test(href)){self.panels=self.panels.add(self.element.find(self._sanitizeSelector(href)))}else if(href&&href!=="#"){$.data(a,"https://www.usbank.com/homepage/homepage_js/href.tabs",href);$.data(a,"https://www.usbank.com/homepage/homepage_js/load.tabs",href.replace(/#.*$/,""));var id=self._tabId(a);a.href="#"+id;var $panel=self.element.find("#"+id);if(!$panel.length){$panel=$(o.panelTemplate).attr("id",id).addClass("ui-tabs-panel ui-tabs-widget-content ui-corner-bottom").insertAfter(self.panels[i-1]||self.list);$panel.data("https://www.usbank.com/homepage/homepage_js/destroy.tabs",true)}self.panels=self.panels.add($panel)}else{o.disabled.push(i)}});if(init){this.element.addClass("ui-tabs ui-widget ui-tabs-widget-content ui-corner-all");this.list.addClass("ui-tabs-nav ui-tabs-helper-reset ui-tabs-helper-clearfix ui-tabs-widget-header ui-corner-all");this.lis.addClass("ui-tabs-state-default ui-corner-top");this.panels.addClass("ui-tabs-panel ui-tabs-widget-content ui-corner-bottom");if(o.selected===undefined){if(location.hash){this.anchors.each(function(i,a){if(a.hash==location.hash){o.selected=i;return false}})}if(typeof o.selected!=="number"&&o.cookie){o.selected=parseInt(self._cookie(),10)}if(typeof o.selected!=="number"&&this.lis.filter(".ui-tabs-selected").length){o.selected=this.lis.index(this.lis.filter(".ui-tabs-selected"))}o.selected=o.selected||(this.lis.length?0:-1)}else if(o.selected===null){o.selected=-1}o.selected=((o.selected>=0&&this.anchors[o.selected])||o.selected<0)?o.selected:0;o.disabled=$.unique(o.disabled.concat($.map(this.lis.filter(".ui-state-disabled"),function(n,i){return self.lis.index(n)}))).sort();if($.inArray(o.selected,o.disabled)!=-1){o.disabled.splice($.inArray(o.selected,o.disabled),1)}this.panels.addClass("ui-tabs-hide");this.lis.removeClass("ui-tabs-selected ui-tabs-state-active aaa");if(o.selected>=0&&this.anchors.length){self.element.find(self._sanitizeSelector(self.anchors[o.selected].hash)).removeClass("ui-tabs-hide");this.lis.eq(o.selected).addClass("ui-tabs-selected ui-tabs-state-active ui-tabs-state-login-active");self.element.queue("tabs",function(){self._trigger("show",null,self._ui(self.anchors[o.selected],self.element.find(self._sanitizeSelector(self.anchors[o.selected].hash))[0]))});this.load(o.selected)}$(window).bind("unload",function(){self.lis.add(self.anchors).unbind(".tabs");self.lis=self.anchors=self.panels=null})}else{o.selected=this.lis.index(this.lis.filter(".ui-tabs-selected"))}this.element[o.collapsible?"addClass":"removeClass"]("ui-tabs-collapsible");if(o.cookie){this._cookie(o.selected,o.cookie)}for(var i=0,li;(li=this.lis[i]);i++){$(li)[$.inArray(i,o.disabled)!=-1&&!$(li).hasClass("ui-tabs-selected")?"addClass":"removeClass"]("ui-state-disabled")}if(o.cache===false){this.anchors.removeData("https://www.usbank.com/homepage/homepage_js/cache.tabs")}this.lis.add(this.anchors).unbind(".tabs");if(o.event!=="mouseover"){var addState=function(state,el){if(el.is(":not(.ui-state-disabled)")){}};var removeState=function(state,el){el.removeClass("ui-state-"+state)};this.lis.bind("https://www.usbank.com/homepage/homepage_js/mouseover.tabs",function(){addState("hover",$(this))});this.lis.bind("https://www.usbank.com/homepage/homepage_js/mouseout.tabs",function(){removeState("hover",$(this))});this.anchors.bind("https://www.usbank.com/homepage/homepage_js/focus.tabs",function(){addState("focus",$(this).closest("li"))});this.anchors.bind("https://www.usbank.com/homepage/homepage_js/blur.tabs",function(){removeState("focus",$(this).closest("li"))})}var hideFx,showFx;if(o.fx){if($.isArray(o.fx)){hideFx=o.fx[0];showFx=o.fx[1]}else{hideFx=showFx=o.fx}}function resetStyle($el,fx){$el.css("display","");if(!$.support.opacity&&fx.opacity){$el[0].style.removeAttribute("filter")}}var showTab=showFx?function(clicked,$show){$(clicked).closest("li").addClass("ui-tabs-selected ui-tabs-state-active ccc");$show.hide().removeClass("ui-tabs-hide").animate(showFx,showFx.duration||"normal",function(){resetStyle($show,showFx);self._trigger("show",null,self._ui(clicked,$show[0]))})}:function(clicked,$show){if($(clicked).closest("li").hasClass("login")){$(clicked).closest("li").removeClass("ui-tabs-state-login-inactive").addClass("ui-tabs-selected ui-tabs-state-active ui-tabs-state-login-active").siblings().addClass("ui-tabs-selected ui-tabs-state-active ui-tabs-state-location-inactive").removeClass("ui-tabs-selected ui-tabs-state-active ui-tabs-state-location-active")}else{$(clicked).closest("li").removeClass("ui-tabs-state-location-inactive").addClass("ui-tabs-selected ui-tabs-state-active ui-tabs-state-location-active").siblings().addClass("ui-tabs-selected ui-tabs-state-active ui-tabs-state-login-inactive").removeClass("ui-tabs-selected ui-tabs-state-active ui-tabs-state-login-active")}$show.removeClass("ui-tabs-hide");/*$('#userID').focus(); ADA*/self._trigger("show",null,self._ui(clicked,$show[0]))};var hideTab=hideFx?function(clicked,$hide){$hide.animate(hideFx,hideFx.duration||"normal",function(){self.lis.removeClass("ui-tabs-selected ui-tabs-state-active eee");$hide.addClass("ui-tabs-hide");resetStyle($hide,hideFx);self.element.dequeue("tabs")})}:function(clicked,$hide,$show){self.lis.removeClass("ui-tabs-selected ui-tabs-state-active");$hide.addClass("ui-tabs-hide");self.element.dequeue("tabs")};this.anchors.bind(o.event+".tabs",function(){var el=this,$li=$(el).closest("li"),$hide=self.panels.filter(":not(.ui-tabs-hide)"),$show=self.element.find(self._sanitizeSelector(el.hash));if(($li.hasClass("ui-tabs-selected")&&!o.collapsible)||$li.hasClass("ui-state-disabled")||$li.hasClass("ui-state-processing")||self.panels.filter(":animated").length||self._trigger("select",null,self._ui(this,$show[0]))===false){this.blur();return false}o.selected=self.anchors.index(this);self.abort();if(o.collapsible){if($li.hasClass("ui-tabs-selected")){o.selected=-1;if(o.cookie){self._cookie(o.selected,o.cookie)}self.element.queue("tabs",function(){hideTab(el,$hide)}).dequeue("tabs");this.blur();return false}else if(!$hide.length){if(o.cookie){self._cookie(o.selected,o.cookie)}self.element.queue("tabs",function(){showTab(el,$show)});self.load(self.anchors.index(this));this.blur();return false}}if(o.cookie){self._cookie(o.selected,o.cookie)}if($show.length){if($hide.length){self.element.queue("tabs",function(){hideTab(el,$hide)})}self.element.queue("tabs",function(){showTab(el,$show)});self.load(self.anchors.index(this))}else{throw"jQuery UI Tabs: Mismatching fragment identifier.";}if($.browser.msie){this.blur()}});this.anchors.bind("https://www.usbank.com/homepage/homepage_js/click.tabs",function(){return false})},_getIndex:function(index){if(typeof index=="string"){index=this.anchors.index(this.anchors.filter("[href$="+index+"]"))}return index},destroy:function(){var o=this.options;this.abort();this.element.unbind(".tabs").removeClass("ui-tabs ui-widget ui-tabs-widget-content ui-corner-all ui-tabs-collapsible").removeData("tabs");this.list.removeClass("ui-tabs-nav ui-tabs-helper-reset ui-tabs-helper-clearfix ui-tabs-widget-header ui-corner-all");this.anchors.each(function(){var href=$.data(this,"https://www.usbank.com/homepage/homepage_js/href.tabs");if(href){this.href=href}var $this=$(this).unbind(".tabs");$.each(["href","load","cache"],function(i,prefix){$this.removeData(prefix+".tabs")})});this.lis.unbind(".tabs").add(this.panels).each(function(){if($.data(this,"https://www.usbank.com/homepage/homepage_js/destroy.tabs")){$(this).remove()}else{$(this).removeClass(["ui-tabs-state-default","ui-corner-top","ui-tabs-selected","ui-tabs-state-active","ui-state-hover","ui-state-focus","ui-state-disabled","ui-tabs-panel","ui-tabs-widget-content","ui-corner-bottom","ui-tabs-hide"].join(" "))}});if(o.cookie){this._cookie(null,o.cookie)}return this},add:function(url,label,index){if(index===undefined){index=this.anchors.length}var self=this,o=this.options,$li=$(o.tabTemplate.replace(/#\{href\}/g,url).replace(/#\{label\}/g,label)),id=!url.indexOf("#")?url.replace("#",""):this._tabId($("a",$li)[0]);$li.addClass("ui-tabs-state-default ui-corner-top").data("https://www.usbank.com/homepage/homepage_js/destroy.tabs",true);var $panel=self.element.find("#"+id);if(!$panel.length){$panel=$(o.panelTemplate).attr("id",id).data("https://www.usbank.com/homepage/homepage_js/destroy.tabs",true)}$panel.addClass("ui-tabs-panel ui-tabs-widget-content ui-corner-bottom ui-tabs-hide");if(index>=this.lis.length){$li.appendTo(this.list);$panel.appendTo(this.list[0].parentNode)}else{$li.insertBefore(this.lis[index]);$panel.insertBefore(this.panels[index])}o.disabled=$.map(o.disabled,function(n,i){return n>=index?++n:n});this._tabify();if(this.anchors.length==1){o.selected=0;$li.addClass("ui-tabs-selected ui-state-active");$panel.removeClass("ui-tabs-hide");this.element.queue("tabs",function(){self._trigger("show",null,self._ui(self.anchors[0],self.panels[0]))});this.load(0)}this._trigger("add",null,this._ui(this.anchors[index],this.panels[index]));return this},remove:function(index){index=this._getIndex(index);var o=this.options,$li=this.lis.eq(index).remove(),$panel=this.panels.eq(index).remove();if($li.hasClass("ui-tabs-selected")&&this.anchors.length>1){this.select(index+(index+1<this.anchors.length?1:-1))}o.disabled=$.map($.grep(o.disabled,function(n,i){return n!=index}),function(n,i){return n>=index?--n:n});this._tabify();this._trigger("remove",null,this._ui($li.find("a")[0],$panel[0]));return this},enable:function(index){index=this._getIndex(index);var o=this.options;if($.inArray(index,o.disabled)==-1){return}this.lis.eq(index).removeClass("ui-state-disabled");o.disabled=$.grep(o.disabled,function(n,i){return n!=index});this._trigger("enable",null,this._ui(this.anchors[index],this.panels[index]));return this},disable:function(index){index=this._getIndex(index);var self=this,o=this.options;if(index!=o.selected){this.lis.eq(index).addClass("ui-state-disabled");o.disabled.push(index);o.disabled.sort();this._trigger("disable",null,this._ui(this.anchors[index],this.panels[index]))}return this},select:function(index){index=this._getIndex(index);if(index==-1){if(this.options.collapsible&&this.options.selected!=-1){index=this.options.selected}else{return this}}this.anchors.eq(index).trigger(this.options.event+".tabs");return this},load:function(index){index=this._getIndex(index);var self=this,o=this.options,a=this.anchors.eq(index)[0],url=$.data(a,"https://www.usbank.com/homepage/homepage_js/load.tabs");this.abort();if(!url||this.element.queue("tabs").length!==0&&$.data(a,"https://www.usbank.com/homepage/homepage_js/cache.tabs")){this.element.dequeue("tabs");return}this.lis.eq(index).addClass("ui-state-processing");if(o.spinner){var span=$("span",a);span.data("https://www.usbank.com/homepage/homepage_js/label.tabs",span.html()).html(o.spinner)}this.xhr=$.ajax($.extend({},o.ajaxOptions,{url:url,success:function(r,s){self.element.find(self._sanitizeSelector(a.hash)).html(r);self._cleanup();if(o.cache){$.data(a,"https://www.usbank.com/homepage/homepage_js/cache.tabs",true)}self._trigger("load",null,self._ui(self.anchors[index],self.panels[index]));try{o.ajaxOptions.success(r,s)}catch(e){}},error:function(xhr,s,e){self._cleanup();self._trigger("load",null,self._ui(self.anchors[index],self.panels[index]));try{o.ajaxOptions.error(xhr,s,index,a)}catch(e){}}}));self.element.dequeue("tabs");return this},abort:function(){this.element.queue([]);this.panels.stop(false,true);this.element.queue("tabs",this.element.queue("tabs").splice(-2,2));if(this.xhr){this.xhr.abort();delete this.xhr}this._cleanup();return this},url:function(index,url){this.anchors.eq(index).removeData("https://www.usbank.com/homepage/homepage_js/cache.tabs").data("https://www.usbank.com/homepage/homepage_js/load.tabs",url);return this},length:function(){return this.anchors.length}});$.extend($.ui.tabs,{version:"https://www.usbank.com/homepage/homepage_js/1.8.13"});$.extend($.ui.tabs.prototype,{rotation:null,rotate:function(ms,continuing){var self=this,o=this.options;var rotate=self._rotate||(self._rotate=function(e){clearTimeout(self.rotation);self.rotation=setTimeout(function(){var t=o.selected;self.select(++t<self.anchors.length?t:0)},ms);if(e){e.stopPropagation()}});var stop=self._unrotate||(self._unrotate=!continuing?function(e){if(e.clientX){self.rotate(null)}}:function(e){t=o.selected;rotate()});if(ms){this.element.bind("tabsshow",rotate);this.anchors.bind(o.event+".tabs",stop);rotate()}else{clearTimeout(self.rotation);this.element.unbind("tabsshow",rotate);this.anchors.unbind(o.event+".tabs",stop);delete this._rotate;delete this._unrotate}return this}})})(jQuery);


/// <reference path="../../../lib/jquery-1.2.6.js" />
/*
	Masked Input plugin for jQuery
	Copyright (c) 2007-2009 Josh Bush (digitalbush.com)
	Licensed under the MIT license (http://digitalbush.com/projects/masked-input-plugin/#license) 
	Version: 1.2.2 (03/09/2009 22:39:06)
*/
(function($){var pasteEventName=($.browser.msie?'paste':'input')+".mask";var iPhone=(window.orientation!=undefined);$.mask={definitions:{'9':"[0-9]",'a':"[A-Za-z]",'*':"[A-Za-z0-9]"}};$.fn.extend({caret:function(begin,end){if(this.length==0)return;if(typeof begin=='number'){end=(typeof end=='number')?end:begin;return this.each(function(){if(this.setSelectionRange){this.focus();this.setSelectionRange(begin,end)}else if(this.createTextRange){var range=this.createTextRange();range.collapse(true);range.moveEnd('character',end);range.moveStart('character',begin);range.select()}})}else{if(this[0].setSelectionRange){begin=this[0].selectionStart;end=this[0].selectionEnd}else if(document.selection&&document.selection.createRange){var range=document.selection.createRange();begin=0-range.duplicate().moveStart('character',-100000);end=begin+range.text.length}return{begin:begin,end:end}}},unmask:function(){return this.trigger("unmask")},mask:function(mask,settings){if(!mask&&this.length>0){var input=$(this[0]);var tests=input.data("tests");return $.map(input.data("buffer"),function(c,i){return tests[i]?c:null}).join('')}settings=$.extend({placeholder:"_",completed:null},settings);var defs=$.mask.definitions;var tests=[];var partialPosition=mask.length;var firstNonMaskPos=null;var len=mask.length;$.each(mask.split(""),function(i,c){if(c=='?'){len--;partialPosition=i}else if(defs[c]){tests.push(new RegExp(defs[c]));if(firstNonMaskPos==null)firstNonMaskPos=tests.length-1}else{tests.push(null)}});return this.each(function(){var input=$(this);var buffer=$.map(mask.split(""),function(c,i){if(c!='?')return defs[c]?settings.placeholder:c});var ignore=false;var focusText=input.val();input.data("buffer",buffer).data("tests",tests);function seekNext(pos){while(++pos<=len&&!tests[pos]);return pos};function shiftL(pos){while(!tests[pos]&&--pos>=0);for(var i=pos;i<len;i++){if(tests[i]){buffer[i]=settings.placeholder;var j=seekNext(i);if(j<len&&tests[i].test(buffer[j])){buffer[i]=buffer[j]}else break}}writeBuffer();input.caret(Math.max(firstNonMaskPos,pos))};function shiftR(pos){for(var i=pos,c=settings.placeholder;i<len;i++){if(tests[i]){var j=seekNext(i);var t=buffer[i];buffer[i]=c;if(j<len&&tests[j].test(t))c=t;else break}}};function keydownEvent(e){var pos=$(this).caret();var k=e.keyCode;ignore=(k<16||(k>16&&k<32)||(k>32&&k<41));if((pos.begin-pos.end)!=0&&(!ignore||k==8||k==46))clearBuffer(pos.begin,pos.end);if(k==8||k==46||(iPhone&&k==127)){shiftL(pos.begin+(k==46?0:-1));return false}else if(k==27){input.val(focusText);input.caret(0,checkVal());return false}};function keypressEvent(e){if(ignore){ignore=false;return(e.keyCode==8)?false:null}e=e||window.event;var k=e.charCode||e.keyCode||e.which;var pos=$(this).caret();if(e.ctrlKey||e.altKey||e.metaKey){return true}else if((k>=32&&k<=125)||k>186){var p=seekNext(pos.begin-1);if(p<len){var c=String.fromCharCode(k);if(tests[p].test(c)){shiftR(p);buffer[p]=c;writeBuffer();var next=seekNext(p);$(this).caret(next);if(settings.completed&&next==len)settings.completed.call(input)}}}return false};function clearBuffer(start,end){for(var i=start;i<end&&i<len;i++){if(tests[i])buffer[i]=settings.placeholder}};function writeBuffer(){return input.val(buffer.join('')).val()};function checkVal(allow){var test=input.val();var lastMatch=-1;for(var i=0,pos=0;i<len;i++){if(tests[i]){buffer[i]=settings.placeholder;while(pos++<test.length){var c=test.charAt(pos-1);if(tests[i].test(c)){buffer[i]=c;lastMatch=i;break}}if(pos>test.length)break}else if(buffer[i]==test[pos]&&i!=partialPosition){pos++;lastMatch=i}}if(!allow&&lastMatch+1<partialPosition){}else if(allow||lastMatch+1>=partialPosition){writeBuffer();if(!allow)input.val(input.val().substring(0,lastMatch+1))}return(partialPosition?i:firstNonMaskPos)};if(!input.attr("readonly"))input.one("unmask",function(){input.unbind(".mask").removeData("buffer").removeData("tests")}).bind("https://www.usbank.com/homepage/homepage_js/focus.mask",function(){focusText=input.val();var pos=checkVal();writeBuffer();setTimeout(function(){if(pos==mask.length)input.caret(0,pos);else input.caret(pos)},0)}).bind("https://www.usbank.com/homepage/homepage_js/blur.mask",function(){checkVal();if(input.val()!=focusText)input.change()}).bind("https://www.usbank.com/homepage/homepage_js/keydown.mask",keydownEvent).bind("https://www.usbank.com/homepage/homepage_js/keypress.mask",keypressEvent).bind(pasteEventName,function(){setTimeout(function(){input.caret(checkVal(true))},0)});checkVal(true)})}})})(jQuery);


/* below codes merged on 30-Jan-2012 */

/* comp_2_hp_carousel.js code starts here */

/* set the domain names for the Apply and tools application */
var domainURL = window.location.hostname;
var apply_pattern_search='/apply/';  
var tools_pattern_search='/ICWeb';
var applyURL=getApplyURL();
var toolsURL=getToolsURL();
var application_pattern_search='https://www.usbank.com/homepage/homepage_js/applications.usbank.com';
var fastapp_pattern_search='https://www.usbank.com/homepage/homepage_js/fastapp.usbank.com';
var login_url_search = 'https://www.usbank.com/homepage/homepage_js/www4.usbank.com';
/* Tools PKG URL pattern */
var tools_pkg_url_search='https://www.usbank.com/ICWeb/productComparison.html?productCode=PKG';



var Mwidth=950;

function modalPromoNew(){
$.ajax({
		url: 'https://www.usbank.com/htmlFragments/OLB_Lightbox.html', 
		async: true, 
		dataType: "html", 
		success: function(html) 			
		{			
			$('#actualContent').html(html);
			$('.modalHeader > ul > li.forZip').addClass('visibilityHidden');
			$('#printWindow').addClass('visibilityHidden');
			$('#hidezip').hide();
			$("#notification").hide();
			var winH = $(document).height();
			var winW = $(window).width();			
			$('.mask').css({'width' :winW, 'height' :winH});							
			$('.mask').show();	
			showModal("modal");	
			modalCloseFlag = true;
			modalFunction();
			$('html, body').animate({scrollTop: $('.ui-dialog').offset().top-50}, 1);
			$('.ui-resizable-handle').hide();
		}});			
};

// Below funtion creates the Mega Menu
// required plugin: jquery.hoverIntent.js
function createMegaMenu(){
	// cache the selectors
	var $menuBar = $('.horizanMenu'); //used only to get other variable only in this section
	var $megaMenuTriggers = $menuBar.find('a.megaMenu-trigger');
	var $megaMenus = $menuBar.find('div.megaMenu'); 
	var $megaMenuTriggersUL = $megaMenuTriggers.closest('ul'); 
	var $megaMenuTriggersLIs = $megaMenuTriggersUL.children('li');
	var $megaMenuTriggersLIsMinusSiloName = $megaMenuTriggersLIs.filter('li:not(li:first)'); 	
	//var $currentFocusedLink; now this variable defined below by abhishek
	// give each Trigger a unique ID so that we can navigate through key board
	$megaMenuTriggersLIsMinusSiloName.each(function(i){	$(this).attr("id","megaTrigger-"+i); });
	// adding a class if the browser needs to use an IMG for the drop shadow
	if (!(typeof document.body.style.MozBoxShadow === 'string' || typeof document.body.style.WebkitBoxShadow === 'string' || typeof document.body.style.BoxShadow === 'string')) {
		// this is browser that doesn't support CSS box shadows, so we will use images
		$megaMenus.each(function(){
			if($(this).is('.megaMenu-wide')){
				$(this).addClass('megaMenu-imageShadowWide')
			}else{
				$(this).addClass('megaMenu-imageShadow')
			};
		});
	};
	// configuration forhoverIntent
	var megaConfig = {
         interval: 250,
         sensitivity: 8,
         over: function(){addMega($(this))},
         timeout: 100,
         out: function(){removeMega($(this))}
    };
// attach the hover and focus behaviors	
	var blurTimer; // focusing on each sub menu cancels this. Therefore, if it's not canceled, we'll assume we're no longer in the menu
    var $currentFocusedLink;
	$megaMenuTriggersLIsMinusSiloName.hoverIntent(megaConfig)
	.find('a:first') // tab links
			.each(function(){
				var $thisTab = $(this).parent('li');
				$(this)
					.focus(function(event){
						$currentFocusedLink = $(this);
						addMega($thisTab);
						clearTimeout (blurTimer);
					})
					
			}).end()
		.find('div.megaMenu a')	// secondary links
			.focus(function(){
				$currentFocusedLink = $(this);
				clearTimeout (blurTimer);
			})								 
	// enable arrow key navigation
	$('.horizanMenu ul li.nav-main-tab').keydown(function (eh){	
		var keyPressed = eh.keyCode*1;
		if ((keyPressed >= 37)&&(keyPressed <= 40)){
			eh.preventDefault();
			var $thisLI = $currentFocusedLink.closest('li');
			var $nextMainTab = $currentFocusedLink.closest('li.nav-main-tab').next('li.nav-main-tab:first');
			var $prevMainTab = $currentFocusedLink.closest('li.nav-main-tab').prev('li.nav-main-tab:first');
			var $outBeforeFocus = $('.horizanMenu').find('ul.nav-main li:first').find('a:first'); // home link
			var $outAfterFocus = $('.horizanMenu').find('#search-searchterm'); // search field
			switch(keyPressed)
				{
				case 37:
					// left
					if ($prevMainTab.length > 0){
						$prevMainTab.find('a:first').focus()
					}else{
						// this was the first tab, so focus on home link and close the tab as we leave
						$outBeforeFocus.focus();
						blurTimer = setTimeout (function(){
								removeMega($currentFocusedLink.closest('li.nav-main-tab'))
						}, 500)
					};
					break;
				case 38:
					//up
					if( ($thisLI.is('.nav-main-tab')) && ($prevMainTab.length == 0) ){
						// this was the first tab main tab, so focus on home link and close the tab as we leave
						$outBeforeFocus.focus();
						blurTimer = setTimeout (function(){
								removeMega($currentFocusedLink.closest('li.nav-main-tab'))
						}, 500)
					}else{
						if ($thisLI.prev('li').length) {
							$thisLI.prev('li').find('a').focus()
						}else{
							$thisLI.closest('li.nav-main-tab').find('a:first').focus()
						};	
					};
					break;
				case 39:
					// right
					if ($nextMainTab.length > 0){
						$nextMainTab.find('a:first').focus()
					}else{
						// this was the last tab, so jump to search
						$outAfterFocus.focus()
						blurTimer = setTimeout (function(){
								removeMega($currentFocusedLink.closest('li.nav-main-tab'))
						}, 500)
					};
					break;
				case 40:
					// down
					if($thisLI.is('.nav-main-tab')){
						$thisLI.find('li:first a').focus()
					}else{
						if ($thisLI.next('li').length) {
							$thisLI.next('li:first').find('a').focus()
						}else{
							if ($nextMainTab.length > 0){
								$nextMainTab.find('a:first').focus()
							}else{
								// this was the last tab, so jump to search
								$outAfterFocus.focus()
								blurTimer = setTimeout (function(){
										removeMega($currentFocusedLink.closest('li.nav-main-tab'))
								}, 500)
							};
						};
					};
				  break;
				};
		};
	});
};
// show the mega on hover
function addMega($theLI){
	var $navbar = $('div.horizanMenu');
	var triggerPosition = $theLI.find('a.megaMenu-trigger').position(); //  To set the y position 
	var navbarHeight = $navbar.height();
	var navbarWidth = $navbar.width();
	var xpos = triggerPosition.left;
	// A menu might be open from keyboard tab focus. remove it if it occurs 
	var $openMenus = $theLI.parent("ul").find('li.megaMenu-trigger-hover');
	$openMenus.each(function(){
			if ($(this).attr("id") !=  $theLI.attr("id")){ 
				removeMega($(this))
			};
		});
// handle the megamenu to open from right to left if it exceeds the lenght of main menubar
	var ypos = navbarHeight;
	$theLI.addClass('megaMenu-trigger-hover megaMenu-trigger-hover-visualStyle').each(function(){
			if (!(typeof document.body.style.MozBoxShadow === 'string')){
				// this isn't FF 3.5, therefore the main nav tab needs an image for the gradient
				$(this).addClass('megaMenu-trigger-hover-imageGradient');
			};
		}).addClass('roundedTop').find('div.megaMenu').each(function(){
				/* handle menu and open it from right to left if it exceeds the page */
				if (xpos > (navbarWidth-$(this).width())){
					xpos = (xpos-$(this).width()) + $theLI.width();
				}; 		   
				$(this).css({
					top: ypos,
					left: xpos
				}).css("z-index","9999")  
				if(!($.browser.msie)){
					$(this)
						.css('opacity', 1)
				};					
			}).end().prev('li').addClass('megaMenu-trigger-hover-visualStyle-leftSibling');
};
// ends here --handle the megamenu to open from right to left 
// remove the mega on mouseout
function removeMega($theLI){
	$theLI.removeClass('megaMenu-trigger-hover-visualStyle megaMenu-trigger-hover-imageGradient')
		.find('a.megaMenu-trigger').removeClass('roundedTop').end()
		.find('div.megaMenu').each(function(){
				if(!($.browser.msie)){
					$(this).animate({ 
							opacity: 0
							}, 0, function(){
								$(this).parent('li').removeClass('megaMenu-trigger-hover')
							})
				}else{
					$(this).parent('li').removeClass('megaMenu-trigger-hover')
				};	
			}).end().prev('li').removeClass('megaMenu-trigger-hover-visualStyle-leftSibling');
};
// end ---------------------------------------------------------------------------------------------------------------- 
$(document).ready(function(){ 
	var contentVersionJS = 1;
	createMegaMenu();
	function mouseOver() {
		setTimeout(function() {
			$(".personalMenu").show().css("z-index","10000");
			$("#personalDropDown").unbind("mouseover");
		}, 500);
	}
	function mouseOut() {
		setTimeout(function() {
			$(".personalMenu").hide();
			$("#personalDropDown").mouseover(mouseOver);
		}, 500);
	}
	$("#personalDropDown").addClass("posRelative").hover(mouseOver, mouseOut);
	/*
		case "Personal" : $.cookie('USBHOMEPAGEPREF', 'Pers', {path : '/', expires:100});
			break;
		case "Small Business" : $.cookie('USBHOMEPAGEPREF', 'Bus', {path : '/', expires:100});
			break;
		case "Commercial &amp; Government" : $.cookie('USBHOMEPAGEPREF', 'Coml', {path : '/', expires:100});
			break;
	*/
	
	$(".megaMenu-secondary li").hover(function() {
		$(this).find("div:last-child").addClass("visibilityVisible").removeClass("visibilityHidden");
	}, function() {
		$(this).find("div:last-child").removeClass("visibilityVisible").addClass("visibilityHidden");
	});
	if($.browser.mozilla)
	{
		if(parseFloat($.browser.version) == 2)
		{
			$('.shadowEffects15px').addClass('shadowEffects15apx').removeClass('shadowEffects15px');
		}
	}
	$("#loginOption").selectmenu("destroy");
	$('#userID').val('');
	
	$('#ZIP').val('');
	$("#loginOption").attr("selectedIndex",0);
	$("#loginOption").selectmenu({menuWidth: 190, maxHeight: 250}).change(function () {
        optionSelected = $(this).val();
		if(optionSelected != "https://test1.www.usbank.com/internetBanking/RequestRouter?src=Homepage"){
			$('#userID').removeClass("highlightBlueBorder highlightRedBorder invalid");
			$("#notification").hide();
			 $(".bottomLinks").hide();
			 $(".showGo").show();
			 $("#personalSection").hide();
		}else{
			$(".showGo").hide();
			$(".bottomLinks").show();
			$("#personalSection").show();
		}   		
		$("#loginForm").attr("action",optionSelected);  
    });
	$("#loginForm").submit(function(){
		if($(this).attr("action")!='https://test1.www.usbank.com/internetBanking/RequestRouter?src=Homepage' && $(this).attr("action")!='https://onlinebanking.usbank.com/Auth/Login/Login')/*PID 3213*/
		{
			window.location = $(this).attr("action");
			return false;
		}else
		{
			$('#userID').val($.trim($('#userID').val()));
			if($('#userID').val().length==0)
			{
				$('#userID').addClass("highlightBlueBorder highlightRedBorder invalid");
				var formErrorMessage = "<div class='usbSpriteIcon showErrorIcon floatLeft'>&nbsp;</div>&nbsp;Please enter a Personal ID";
				showNotification(formErrorMessage,$(".notification_message"),$('#userID'),$("#notification"),$(".formErrorArrow"),8,3,"redPopup","slow","show");
				/*$('#userID').focus(); ADA*/
				return false;
			}
			if($('#hEstateSelection option:selected').text()=="Online Investing"){
						$('#userType').val('USBI');
					}else{
							$('#userType').val('');
					}
				$('#reqcrda').val($('#userID').val());
				$('#requestCmdId').val("VALIDATEID");
		}
		return true;
	});
	$("#userID").unbind().bind("click focus keyup",function(e){
		if(e.type=="focus" || e.type=="click") {
			if($(this).val()=="") {
				if($("label[for=userID]").hasClass("infieldLabel")){
					if($.browser.msie && $.browser.version == "6.0") {
						$("label[for=userID]").css({"color":"#ccc","filter":""});
					} else {
						$("label[for=userID]").css({"opacity":".5","filter":"alpha(opacity=50)"});
						//$("label[for=userID]").css({"color":"#ccc","filter":""});
					}
				}
			}
			if($(this).val().length>0&&$(this).hasClass('invalid')) {
				$('#userID').removeClass("highlightBlueBorder highlightRedBorder invalid");
				$("#notification").hide();
			}
		}
		if(e.type=="keyup") {
			if($(this).val()!="") {
				$("label[for=userID]").hide();
			} else {
				$("label[for=userID]").show().css({"color":"#999","filter":""});
			}
		}
		
	});
	$("#userID").bind("blur",function(e){
		if($(this).hasClass('invalid'))
		{
			$('#userID').removeClass("highlightBlueBorder highlightRedBorder invalid");
			$("#notification").hide();
		}
		/*if($.browser.msie && $.browser.version == "6.0") {
			$("label[for=userID]").css({"color":"", "filter":""});
		}*/
	});
	$('#NONCE').val(GetNonces());
	// PID_2940: Change made attribute to MachineAttribute
	//$('#MACHINEATTR').val(GetClientParameters());
	$('#MachineAttribute').val(GetClientParameters());
	$("#loginOption-button,#filter1-button,#loginOption-menu").css('cssText',"width:185px !important");
	//$("#loginOption-button").css("width","185px");
	$("#loginOption-menu").css("z-index","9999");	
	if(domainURL.match(/it/gi)||domainURL.match(/dev/gi)||domainURL.match(/uat/gi)){
	$.each($('a'),function(index){ 		
			if(this.href!=null){
			var current_url= this.href; 
			
			var new_url='';
			if(current_url.match(apply_pattern_search))
				{
					
					new_url=applyURL+current_url.substr(current_url.indexOf(".com/")+4);
					$(this).attr('href',new_url); 			
				}
			else if(current_url.match(tools_pattern_search))
				{
					new_url=toolsURL+current_url.substr(current_url.indexOf(".com/")+4);
					/* Start of URL redirection of Tools PKG comparison chart to dotcom chart */
					if(current_url.indexOf(tools_pkg_url_search)!=-1){
						//new_url="https://"+domainURL+current_url.substr(current_url.indexOf(".com/")+4).replace('ICWeb','ICWeb'); 
					}
					/* End of URL redirection of Tools PKG comparison chart to dotcom chart */
					$(this).attr('href',new_url);
				}		
			else if(current_url.match(application_pattern_search)){
				new_url="https://uat-applications.usbank.com"+current_url.substr(current_url.indexOf(".com/")+4);
				$(this).attr('href',new_url);
				}
				else if(current_url.match(fastapp_pattern_search)){
					new_url="https://uat-fastapp.usbank.com"+current_url.substr(current_url.indexOf(".com/")+4);
					$(this).attr('href',new_url);
				}else if(current_url.match(login_url_search)){
					new_url="https://test1.www.usbank.com"+current_url.substr(current_url.indexOf(".com/")+4);
					$(this).attr('href',new_url);
				}else if(current_url.match("https://www.usbank.com")){
					new_url="https://"+domainURL+current_url.substr(current_url.indexOf(".com/")+4);
					$(this).attr('href',new_url);
				}
				else if(current_url.match("https://www.usbank.com/homepage/homepage_js/loansandlines.usbank.com")){
					new_url="https://uat-loansandlines.usbank.com"+current_url.substr(current_url.indexOf(".com/")+4);
					$(this).attr('href',new_url);
				}
			}});
	}
	
/*$('#userID').focus(); ADA*/
});
function ie7ZindexIssue() {
	var zIndexNo=1000;
	$('div').each(function() {
		$(this).css('zIndex',zIndexNo);
		zIndexNo-=5;
	});
};

var containerObj = $(".container a, .container input");
var currentBrowser;
var maskObj;
$().ready(function(){
	maskObj = $(".mask");
	var browserCompatibleContent = $("#browserCompatibleContent");
	var browserCompatible = $("#browserCompatible");
	if(getCookie("browser")!="1" && browserSupport()) {
		var cnt = 0;
		var browsers = ["Internet Explorer","Mozilla Firefox","Google Chrome","Safari"];
		$(".browserText").html(currentBrowser);
		$.each(browsers,function(){
			if(currentBrowser != browsers[cnt]){
				$("#alterNateBrowser"+cnt).html(browsers[cnt]);
				cnt++;
			}
			if($("#browserCompatibleContent")){
				masking($("#browserCompatibleContent"));
			}
		});
	}else
	{
		ie6Issue();
	}
	if(browserSupport() && getCookie("compatibleMessage")!="1") {
		$("#browserCompatible").show("slow");
	}
	
	$("#closeBrowserWindow, #closeBrowserImage, #cancelBrowserButton").bind("click", function() {
		setCookie("browser","1");
		maskObj.hide();
		browserCompatibleContent.hide();
		containerObj.removeAttr("tabindex");		
		ie6Issue();
	});

	$("#closeBrowserCompatible, #closeBrowserCompatibleImage").bind("click", function() {
		setCookie("compatibleMessage","1");
		browserCompatible.hide();			
	});	
	loadAndRotateFeatures(1500,8000);
		//createMarketingCarousel($("#secondaryMessage"));
		roundCornersInIE();
	if($.browser.msie){
		if(parseFloat($.browser.version) == 7){
			ie7ZindexIssue();
		}
	}
});
function ie6Issue()
{
	if($.browser.msie){
		if(parseFloat($.browser.version) == 6){
			ie7ZindexIssue();
		}
	}
}
function getWidth(){
	var width1,width2;
	if($.browser.msie && $.browser.version<7){
		width1 = Math.max(document.documentElement.scrollWidth,document.body.scrollWidth);
		width2 = Math.max(document.documentElement.offsetWidth,document.body.offsetWidth);
		return width1 < width2 ? $(window).width() : width1;
	}else{
		return Math.max($('body').width(),$('.container').width());
	}
}

function masking(targetId) {
	if(maskObj.length > 0){
		var windowWidth,windowWidth1,windowHeight;
		windowHeight = $(document).height();		
		windowWidth  = getWidth();
		
		if(window.innerWidth){
			windowWidth1 = window.innerWidth;
		}else{
			windowWidth1 = document.documentElement.clientWidth;
		}
		maskObj.width(windowWidth).height(windowHeight).show();
		var halfWidth = (windowWidth1) / 2;
		var leftX = halfWidth - ($(".span-18").width() / 2);
		targetId.css("left",leftX+"px").show();			
		$(".container a, .container input").attr("tabindex","-1");
		$('html, body').animate({scrollTop: targetId.offset().top-50}, 1000);
	}
}

function browserSupport(){
	var userAgent = navigator.userAgent.toString().toLowerCase();
	if($.browser.msie){
		if(parseFloat($.browser.version) < 7){
			currentBrowser = "Internet Explorer";
		}else{
			return false;			
		}
	}else if($.browser.mozilla){
		if(parseFloat($.browser.version) < 1.9){
			currentBrowser = "Mozilla Firefox";
		}else{
			return false;			
		}
	}else if((userAgent.indexOf('safari') != -1) && !(userAgent.indexOf('chrome') != -1) && parseFloat(navigator.appVersion) < 5){
		currentBrowser = "Safari";
	}else if(navigator.userAgent.toLowerCase().indexOf("chrome") && (parseFloat(navigator.appVersion) < 3)){
		currentBrowser = "Google Chrome";
	}else{
		return false;
	}
	return true;
}

function getCookie(c_name) {
	if (document.cookie.length>0) {
		c_start=document.cookie.indexOf(c_name + "=");
		if (c_start!=-1) {
			c_start=c_start + c_name.length+1;
			c_end=document.cookie.indexOf(";",c_start);
			if (c_end==-1) c_end=document.cookie.length;
			return unescape(document.cookie.substring(c_start,c_end));
		}
	}
	return "";
}

function setCookie(c_name,value,expiredays) {
	var exdate=new Date();
	exdate.setDate(exdate.getDate()+expiredays);
	document.cookie=c_name+ "=" +escape(value)+
	((expiredays==null) ? "" : ";expires="+exdate.toUTCString())+";path=/";
	if(getCookie(c_name)=="") {
		browserCompatible.hide();
		cookieDisabled.show("slow");
		$('html, body').animate({scrollTop: cookieDisabled.offset().top}, 1000);
		setTimeout(function() {
			url = hardFallUrl.val();
			window.location = url;
		}, 5000);
	}
}

buttonHeight = $("#onlineButton").height();
onlineHeight = (buttonHeight/2) - 6;
$("#onlineLabel").css("padding-top",onlineHeight+"px");

/* Added URL re-direction-----*/
function getToolsURL()
{
	return "";
}
function getApplyURL()
{
	if(domainURL.match(/dev2/gi)!=null)
	{
		return "http://dev1-apply.us.bank-dns.com/";
	}
	if(domainURL.match(/it2/gi)!=null)
	{
		return "http://it1-apply.us.bank-dns.com/";
	}
	if(domainURL.match(/it3/gi)!=null)
	{
		return "http://it3-apply.us.bank-dns.com/";
	}
	if(domainURL.match(/uat2/gi)!=null)
	{
		return "https://uat-apply.usbank.com";	
	}
	if(domainURL.match(/uat3/gi)!=null)
	{
		return "https://uat1-apply.usbank.com";	
	}
	if(domainURL.match(/uat4/gi)!=null)
	{
		return "https://uat2-apply.usbank.com";	
	}
	return "https://apply.usbank.com";
}

$('.SelectOptionMenu').click(function() {
	cloaseOverlay();
	divWidth =  $(this).width();
	var id = $(this).attr('id').replace("OverlayShow",'');
	leftPos = $(this).offset().left;
	Ttop = $(this).offset().top;
	Ttop=Ttop+25;
	if(leftPos > divWidth) {
	  	if($.browser.mozilla) {
			Tleft = leftPos-202;
			}
		else 
		 Tleft = leftPos-196;
	} 
	else {
		Tleft = $(this).offset().left;
	}
	$("#OverlayHide"+id).show().css({"top":Ttop,"left":Tleft,"position":"absolute","z-index":"9999"});
});
$('.closeOverlay').click(function(){
	cloaseOverlay();
});
function cloaseOverlay()
{
	$('.SelectOptionMenu').each(function(){
		var id = $(this).attr('id').replace("OverlayShow",'');
		$("#OverlayHide"+id).hide();
	});
}
$(window).resize(function() {	
	$('.SelectOptionMenu').each(function(){
		var id = $(this).attr('id').replace("OverlayShow",'');
		var divWidth =  $(this).width();
		var leftPos = $(this).offset().left;
		if(leftPos > divWidth) {
		
		if($.browser.mozilla) {
		Tleft = leftPos-202;
		}
		else 
			leftPos = leftPos-196;
		

		}


		else {
			leftPos = $(this).offset().left;
		}
		$("#OverlayHide"+id).css("left", leftPos);
	});
});

var ua = $.browser;
  if ( (ua.mozilla && (parseFloat(ua.version.slice(0,3)) >= 2.0 || ua.version.slice(0,3) == "1.9")) || (ua.safari)) {
    $('.shadowEffects15px').addClass('shadowEffects15apx').removeClass('shadowEffects15px');
  }
 $("label").inFieldLabels();	 
 /**Fucntions for hidden values intialization**/
 var NonceData='';
 var COOKIE_NAME="EASNonce";
 var SWFFile="../../homepage_images/AIDAuth.swf"/*tpa=https://www.usbank.com/homepage_images/AIDAuth.swf*/
function GetClientParameters()
{
	var colorDepth=window.screen.colorDepth;
	var width=window.screen.width;
	var height=window.screen.height;
	var availWidth=window.screen.availWidth;
	var availHeight=window.screen.availHeight;
	var platform=navigator.platform;
	var userAgent=navigator.userAgent;
	var java="No";
	if(navigator.javaEnabled()==1)
	{
		java="Yes";
	}	clientParms="colorDepth="+colorDepth+"|width="+width+"|height="+height+"|availWidth="+availWidth+"|availHeight="+availHeight+"|platform="+platform+"|javaEnabled="+java+"|userAgent="+ParseUserAgentString(userAgent);return clientParms;
}
function FlashGetNonceCallback(nonceData)
{
	NonceData=nonceData;
}
function GetNonces()
{
var returnVal;
if(NonceData!=null&&NonceData!="")
{returnVal=NonceData;}
else
{
if(navigator.cookieEnabled)
	{
		if(document.cookie.length>0)
		{
			var start=document.cookie.indexOf(COOKIE_NAME+"=");
			if(start!=-1)
			{	
				start=start+COOKIE_NAME.length+1;
				var end=document.cookie.indexOf(";",start);
				if(end==-1)
				{end=document.cookie.length;}
				returnVal=document.cookie.substring(start,end);
			}
		}
	}
}
if(returnVal==null)
{returnVal="NoNonce";}
return returnVal;
}
function ParseUserAgentString(userAgent)
{
	var returnVal=userAgent;
	if(returnVal.search("Firefox")>-1)
	{var revPos=returnVal.search("rv:");
		if(revPos>-1)
		{returnVal=returnVal.substring(0,revPos+6);}
	}
return returnVal;}

/**Fucntion to show notification error**/
function showNotification(formHelpMessage,notificationMessageObj,el,notificationObj,errorArrowObj,top,left,popuptype,fadeIn,status){
	if(el.attr("class")=="helpOptional") {
		$("body").bind("mousedown",function(e) { notificationObj.hide();
		$("body").unbind("mousedown"); });
	}
	if(popuptype=="redPopup") {
		notificationObj.removeClass("greenPopup").addClass("redPopup");	
	} else if(popuptype=="greenPopup") {
		notificationObj.removeClass("redPopup").addClass("greenPopup");
	}
	notificationMessageObj.html(formHelpMessage);
	Ttop = (el.offset().top) - (notificationObj.height() + (top+2));
	Tleft = el.offset().left - left;
	notificationObj.css({"position":"absolute","left":Tleft,"top":Ttop,"zIndex":10000});
	
	if(status == "show") {
		notificationObj.show();
	} else if(status == "hide") {
		notificationObj.hide();
	}
	errorArrowObj.css("left","20px").show();
}



      
 function getDomain(str) {        
 /*      
 This function will extract the domain from a URL.        
 For example, "http://mtg.usbank.com/somepage.cfm" will yield "https://www.usbank.com/homepage/homepage_js/mtg.usbank.com".      
 */      
    if (str == null || str.length == 0) {     
        return "";      
    }     
      
    var startAt = str.indexOf("//") + 2;      
    var endAt = str.indexOf("/", startAt);      
         
    if (endAt == -1) { /*no trailing slash */     
        endAt = str.length;      
    }     
      
    var domain = str.substring(startAt,endAt);     
    return domain;     
 }      
/**Spanish implementation* 
$('.headerBorder > .last_nav > a').click(function(){
MP.switchLanguage('es');
});*/
/**Cookie implemetation**/
$(".personalMenu ul li").bind("click",function() {
	var hrefLoc;
	hrefLoc = $(this).find('a:eq(1)').attr('href');
	var textA = $(this).find('a:eq(1)').html();
	switch(textA)
	{
		case "Personal" : $.cookie('USBHOMEPAGEPREF', 'Pers', {path : '/', expires:100});
			break;
		case "Small Business" : $.cookie('USBHOMEPAGEPREF', 'Bus', {path : '/', expires:100});
			break;
		case "Commercial &amp; Government" : $.cookie('USBHOMEPAGEPREF', 'Coml', {path : '/', expires:100});
			break;
	}	
	window.location = hrefLoc;
});
$('.personalMenu > ul > li > div > a.mmdiv').click(function(){
	var textA = $(this).html();
	switch(textA)
	{
		case "Personal" : $.cookie('USBHOMEPAGEPREF', 'Pers', {path : '/', expires:100});
			break;
		case "Small Business" : $.cookie('USBHOMEPAGEPREF', 'Bus', {path : '/', expires:100});
			break;
		case "Commercial &amp; Government" : $.cookie('USBHOMEPAGEPREF', 'Coml', {path : '/', expires:100});
			break;
	}	
});

/**IM299146 defect fix code **/

$("#topNavigation ul li").bind("click",function() {
	var textA = $(this).text();
	//alert(":" +textA+ ":");
	textA = $.trim(textA);
	//alert(":" +textA+ ":");
	switch(textA)
	{		
		case "Personal" : $.cookie('USBHOMEPAGEPREF', 'Pers', {path : '/', expires:100});
			//alert("set1");
			break;
		case "Small Business" : $.cookie('USBHOMEPAGEPREF', 'Bus', {path : '/', expires:100});
			//alert("set2");
			break;
		case "Commercial &amp; Government" : $.cookie('USBHOMEPAGEPREF', 'Coml', {path : '/', expires:100});
			//alert("set3");
			break;
	}	
});

/*$("#topNavigation ul li").bind("click",function() {
	alert("1");
	var hrefLoc;
	hrefLoc = $(this).find('a:eq(1)').attr('href');
	var textA = $(this).find('a:eq(1)').html();
	alert(textA);
	alert(hrefLoc);
	alert($(this).find('a:eq(1)').attr('title'));
	alert($(this).text());
	alert($(this).html());
	switch(textA)
	{		
		case "Personal" : $.cookie('USBHOMEPAGEPREF', 'Pers', {path : '/', expires:100});
			break;
		case "Small Business" : $.cookie('USBHOMEPAGEPREF', 'Bus', {path : '/', expires:100});
			break;
		case "Commercial &amp; Government" : $.cookie('USBHOMEPAGEPREF', 'Coml', {path : '/', expires:100});
			break;
	}	
	window.location = hrefLoc;
});
$('#topNavigation > ul > li > div > a.mmdiv').click(function(){
	alert("2");
	var textA = $(this).html();
	alert(textA);
	alert(hrefLoc);
	alert($(this).find('a:eq(1)').attr('title'));
	alert($(this).text());
	alert($(this).html());
	switch(textA)
	{
		case "Personal" : $.cookie('USBHOMEPAGEPREF', 'Pers', {path : '/', expires:100});
			break;
		case "Small Business" : $.cookie('USBHOMEPAGEPREF', 'Bus', {path : '/', expires:100});
			break;
		case "Commercial &amp; Government" : $.cookie('USBHOMEPAGEPREF', 'Coml', {path : '/', expires:100});
			break;
	}	
});*/
/**IM299146 defect fix code end**/


//Avoid  framing
if (self != top) {
	var domain = getDomain(document.referrer);
	var okDomains = /usbank|localhost|usbnet/;
	var matchDomain = domain.search(okDomains);
	
	// If within TeamSite return else proceed
	var isCMS = getCookie("IWAUTH");
	if(isCMS == "" || isCMS == null) {
	if (matchDomain == -1) {
		/* Not a usbank domain so don't allow framing */
		logFrameAttempt(document.referrer,document.location);
		var jQuote = "'";
		if (document.images) {
			var msg= document.referrer + ' is attemping to display a U.S. Bank Web page within a window.\n\n' +
								'U.S. Bank does not allow "framing" of our Web pages.\n\n' +
								'Selecting "OK" will  replace this Web site' + jQuote + 's page with the requested page from the U.S. Bank Web site.';
			alert(msg);
			top.location.replace(document.location);
	/*		var answer = confirm (msg);
			if (answer)
					top.location.replace(document.location);
			else
					top.location.replace('https://www.usbank.com/cgi_w/cfm/notAuthorized.cfm'); */
		} else {
			var msg='This Web site is attemping to display a U.S. Bank Web page within a window.\n\n' +
								'U.S. Bank does not allow "framing" of our Web pages.\n\n' +
								'Selecting "OK" will  replace this Web site' + jQuote + 's page with the requested page from the U.S. Bank Web site.';
			alert(msg);
			top.location.href = document.location;
		}
	}
   }
}

/*
function alertContents() {
  if (http_request.readyState == 4) {
	 if (http_request.status == 200) {
		//alert(http_request.responseText);
		result = http_request.responseText;
		document.getElementById('myspan').innerHTML = result;            
	 } else {
		alert('There was a problem with the request.');
	 }
  }
}
*/
function logFrameAttempt(framingPage,ourPage) {

	var http_request = false;

	if (window.XMLHttpRequest) { // Mozilla, Safari,...
		http_request = new XMLHttpRequest();
		if (http_request.overrideMimeType) {
			// set type accordingly to anticipated content type
			//http_request.overrideMimeType('text/xml');
			http_request.overrideMimeType('text/html');
			}
		} else if (window.ActiveXObject) { // IE
			try {
			http_request = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try {
				   http_request = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e) {}
			}
		}
	
	if (!http_request) {
		/*alert('Cannot create XMLHTTP instance');*/
		return false;
		}
      
 	var framingPageURL=encodeURIComponent(framingPage);
	var ourPageURL=encodeURIComponent(ourPage);
	var parameters="framingPage="+framingPageURL+"&ourPage="+ourPageURL;

	/*http_request.onreadystatechange = alertContents;*/
	http_request.open('POST', "https://www.usbank.com/logFrameAttempt.cfm", true);
	http_request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	http_request.setRequestHeader("Content-length", parameters.length);
	http_request.setRequestHeader("Connection", "close");
	http_request.send(parameters);
}

function getDomain(str) {   
/*
This function will extract the domain from a URL.  
For example, "http://mtg.usbank.com/somepage.cfm" will yield "https://www.usbank.com/homepage/homepage_js/mtg.usbank.com".
*/
	if (str == null || str.length == 0) {
		return ""; 
	}

	var startAt = str.indexOf("//") + 2; 
	var endAt = str.indexOf("/", startAt); 
	
	if (endAt == -1) { /*no trailing slash */
		endAt = str.length; 
	}

	var domain = str.substring(startAt,endAt);
	return domain;
}

function showModal(id){	
	$("#"+id).dialog({
			autoOpen: false,
			width : Mwidth+30+"px",
			position :'center',
			close : modalCloseFun
		});
	$('#'+id).dialog("open");	
	modalResize();
}
/**Function to resize the modal on window resize**/
function modalResize()
{
   
	var windowWidth1;
	if(window.innerWidth){
		windowWidth1 = window.innerWidth;		
	}else{
		windowWidth1 = document.documentElement.clientWidth;
	}	
	var left = (windowWidth1-(Mwidth+30))/2; 	
	$(".ui-dialog").css({"left":left+"px","top":"25px","position":"absolute"});
}

function modalFunction()
{	
   
	$("#closeWindow, #closeImage").bind("click", function() {
		modalCloseFun();
	});
}

function modalCloseFun()
{
     $('.visibilityVisible').removeClass('visibilityVisible').addClass('visibilityHidden');
	//$(".ui-dialog").hide();	
	$('#modal').dialog("destroy");
	$('.mask').hide();		
	$("#notification").hide();
	$("#userID").focus();
	}
 $("<div id='HPModal'></div>").prependTo("#footer");
 
/* comp_2_hp_carousel.js code ends here */

/* validate.js code starts here */
var domainURL = window.location.hostname;
var notificationObj = $("#notification");
var errorArrowObj = $(".formErrorArrow");
var notificationMessageObj = $(".notification_message");
var formErrorContent = $(".formErrorContent");
var containerObj = $(".container a, .container input");
var Ttop = Tleft = 0;
var message = "is a required field.";
var ready = 1;
/* LeavingOurSite specific variables */
var tpW=screen.availWidth;
var tpH=screen.availHeight;
var tpPopupW=500;
var tpPopupH=420;
var tpPopupLeftCorner=(tpW-tpPopupW)/2;
var tpPopupTopCorner=(tpH-tpPopupH)/2;
	
$().ready(function(){
//var 	formErrorMessage = "<div class='usbSpriteIcon showErrorIcon floatLeft'></div><div class='width130px padLeft-3px floatLeft last'><i>"+el.attr('title')+"</i> is a required field.</div>";						
	//var formErrorMessage = "<div class='usbSpriteIcon showErrorIcon floatLeft'>&nbsp;</div><div class='width130px padLeft-3px floatLeft last'>This is a required field.</div>";
	$("#branch").attr('checked', true);
	$("#atm").attr('checked', true); 
	/*Tab function*/
	$("#tabs").tabs({
	event: "click"	});
	$("input[type=text]").val("");
	var ready=1;
	notificationObj.bind("click",function(){
		$(this).hide();		
	});
	$('#userID').val('');
	$('#zipcode').remove();
	$('#ZIP').val('');
	$("#phoneNum").mask("(999)-999-9999").css("cssText", "color:gray !important;");
	$("#likeToBorrow, #currentMortBal, #estHomeVal").mask("9999999999", {placeholder:""});
	//$('#phoneNum')	
	$("input[type=select]").val("");
	$("#hEstateSelection,#openAcc8").attr("selectedIndex",0);
	$("#hEstateSelection-button,#openAcc8-button").remove();
	loadSelectMenu();
	//selectMenuFocus();
	$("#hEstateSelection-button").mouseout(function() {
		$(this).removeClass("ui-state-hover");
	});	
	$("#loginForm").attr("action",checkURL($("#loginForm").attr("action")));  
	$("#hEstateSelection").change(function() {		
		if($(this).val() == "https://onlinebanking.usbank.com/Auth/Login/Login") {/*PID 3213*/
			$("#onlineBankingSection, #userIDSection, #loginButton").show();
			$("#goButton").hide();
			if($('#hEstateSelection option:selected').text()=="Online Investing"){
						$('#userType').val('USBI');
					}else{
							$('#userType').val('');
					}
		} else {
			$("#goButton").show();
			$("#userID").val("").removeClass("validated highlightRedBorder highlightBlueBorder blueSelect invalid").addClass("validate");
			$("label[for=userID]").show();
			$("#onlineBankingSection, #userIDSection, #loginButton").hide();
		}
		$("#loginForm").attr("action",checkURL($(this).val()));  
		changeInDropDownLocation();
	});	
	$("#hEstateSelection-menu").css("z-index","999999");
	$(".openAccountButton").click(function(e) {
		overlayPlugin($(this),e, $("#OverlayHide3"));
	});
	$(".closeOverlay").click(function() {
		$("#OverlayHide3").hide();
	});
	$(".bankProducts").click(function(e) {
		overlayPlugin($(this),e, $("#bankProductsOverlay"));
	});
	$(".closeOverlayBankProducts").click(function() {
		$("#bankProductsOverlay").hide();
	}); 
	$(".megaMenu-trigger").bind("mouseover", function(){
		$("#userID").blur();
	});
});


/* Function for Speedbump functionality */
function leavingOurSite(tpBL,tpID,tpNW,tpPath){
	if('undefined'==typeof tpNW){tpNW=''}
	if('undefined'==typeof tpPath){tpPath=''}
	var sThirdPartyURL="/ThirdParty/goOffsiteConfirm.cfm?BL="+tpBL+"&ID="+tpID+"&NW="+tpNW+"&tpPath="+tpPath;
	var goOffSite=window.open(sThirdPartyURL,"Leaving","directories=no,location=no,menubar=no,resizable=no,status=no,toolbar=no,scrollbars=no,width="+tpPopupW+",height="+tpPopupH+",top="+tpPopupTopCorner+",left="+tpPopupLeftCorner);
	goOffSite.focus()
}

function overlayPlugin(el, event, target) {
	var leftPx = el.offset().left-200;
	var topPx = el.offset().top +34;
	var targetWidth = target.width();
	var totWidth = leftPx + targetWidth;
	if(totWidth > $("body").width()) {
		var leftWidth = (leftPx + el.width()) - targetWidth;
		target.css({"left":leftWidth+"px", "top":topPx+"px"}).show();
	} else {
		target.css({"left":leftPx+"px", "top":topPx+"px"}).show();
	}
}
$('#userID').live('keypress',function(event){		
	var key;
	var isCtrl = false;
	var keychar;
	var reg;		
	if(window.event) {
		key = event.keyCode;
		isCtrl = window.event.ctrlKey
	}
	else if(event.which) {
		key = event.which;
		isCtrl = event.ctrlKey;
	}	
	if (isNaN(key)) return true;
	//if (key<48 || (key>57 && key <65) || (key>90 && key<97) || key>122) return false;
	
	keychar = String.fromCharCode(key);
	//if((keychar==/^[a-zA-Z0-9]$/) || (key==8 || isCtrl)) return true;
	// check for backspace or delete, or if Ctrl was pressed
	if (key == 8 || key == 13 || isCtrl)
	{
		return true;
	}

	reg = /[a-zA-Z0-9]/;
	//var isFirstN = allowNegative ? keychar == '-' && obj.value.indexOf('-') == -1 : false;
	//var isFirstD = allowDecimal ? keychar == '.' && obj.value.indexOf('.') == -1 : false;
	
	return reg.test(keychar);	
	
});
$("#phoneNum").bind('keypress',function(){
$(this).css("cssText", "color:#000 !important;");
});

$(".infoIcon").bind("click",function(){
	var el = currentElemObj = $(this);
	formHelpMessage = $(this).siblings('input[type=text]').attr("title");
	formErrorContent.show();
	errorArrowObj.show();
	showNotification(formHelpMessage,notificationMessageObj,el,notificationObj,errorArrowObj,8,14,"greenPopup","slow","show");
	$("body").bind("mousedown",function(e) {
		notificationObj.hide();
		$(el).unbind("mousedown");
	});
});

$(".helpOptional").bind("blur",function(){
		notificationObj.hide();
});
function selectMenu(ids,mWidth,mHeight,changeNeeded,changeId){
	if(changeNeeded){
			$(ids).selectmenu({menuWidth: mWidth, maxHeight: mHeight}).change(function() {
			var prefix = "";
			if($(this).attr('id').indexOf("ja") != -1) prefix = "ja";
			if($(this).val() != ""){
				notificationObj.hide();
				$(".ui-selectmenu").unbind("focus",selectFocus);			
				var selectButton = $(this).attr('id')+"-button";
				$("#"+selectButton).removeClass("highlightRedBorder ui-selectmenu-error invalid");
			}
			$(".ui-selectmenu-menu").css({"left":"0px","top":"0px"});
		});
	}else{
		$(ids).selectmenu({menuWidth: mWidth,maxHeight: mHeight});
	}	
}

var selectMenuId;
$("#hESubmit").bind("click", function(){
	var cnt = 0;
	var firstFocus = null;
	$.each($("#homeEquityLeadFrom input[type=text]:not(#hESubmit), #homeEquityLeadFrom select"), function() {
		if(!$(this).hasClass("selectInput")) {
			if($(this).hasClass("textInputBox")) {
				if(!$($(this)).hasClass("validated")){					
					if($($(this)).hasClass("validate")){
						if($($(this)).hasClass("dollarInput")){
							$($(this)).addClass("highlightRedBorder invalidAmount");
						} else {
							$($(this)).addClass("highlightRedBorder invalid");
						}
						if(cnt == 0){
							firstFocus = $(this);
						}
						cnt++;
					}
				}
			} else {
				$(this).addClass("highlightRedBorder invalidDollar");
				if(cnt==0) {
					firstFocus = $(this);
				}
				cnt++;
			}
		} else {
			if(!$($(this)).hasClass("validated")){			
				if($($(this)).hasClass("validate")){
					if($(this).val()=="") {
						$("#"+$(this).attr("id")+"-button").addClass("highlightRedBorder ui-selectmenu-error");
						if(cnt == 0){ 
							firstFocus = $($("#"+$(this).attr("id")+"-button"));
						}
						cnt++;
					} else { 
						$("#"+$(this).attr("id")+"-button").removeClass("validate highlightRedBorder ui-selectmenu-error").addClass("validated");
					}
				}
			}
		}
		if (($(this).hasClass("validated")) && ($(this).hasClass("dollar"))) {
			$(this).removeClass('invalidDollar highlightRedBorder');	
		}
		if (($(this).hasClass("validated")) && ($(this).hasClass("validateNumber"))) {
			if ($(this).hasClass("dollarInput")) {
				$(this).removeClass('invalidDollar invalid highlightRedBorder');	
			} else {
				$(this).removeClass('validateNumber invalidDollar invalid highlightRedBorder');	
			}
			
		}
	});
	
	//animate if its not null still fields are there for entering the values
	if(cnt != 0) {
		if(firstFocus != null){
			//alert(firstFocus.html());
			$('html, body').animate({scrollTop: firstFocus.offset().top-65}, 1000,function(){
				if(firstFocus.hasClass("ui-selectmenu")) {	
					selectMenuFocus();
				}	
				firstFocus.focus();
			});
		}
	}
	if (cnt == 0) {		
		$("#homeEquityLeadFrom").addClass('hide'); 
		$("#HEThankUForm").addClass('show');
	}
	
});

registerGroupHover();

function registerGroupHover(){
	$(".groupValidate, .ui-selectmenu").bind("mouseout blur",function(e){
		e.stopPropagation();
		if(!$(this).hasClass("groupValidated")){
			notificationObj.hide();	
		}
});

$(".groupValidate, .ui-selectmenu").bind("mouseover",function(e){
	e.stopPropagation();			
		el = $(this);
		if(el.hasClass("ui-selectmenu") && el.hasClass("highlightRedBorder")){
			if($(this).hasClass("validate")){
				formErrorMessage = "<div class='usbSpriteIcon showErrorIcon floatLeft'>&nbsp;</div><div class='width130px padLeft-3px floatLeft last'>&nbsp;&nbsp;<i>"+el.attr('title')+"</i> is a required field.</div>";showNotification(formErrorMessage,notificationMessageObj,el,notificationObj,errorArrowObj,8,3,'redPopup','slow','show');
				showNotification(formErrorMessage,notificationMessageObj,el,notificationObj,errorArrowObj,8,3,"redPopup","slow","show"); 
			}			
		}else{
			if(!el.hasClass("groupValidated") && el.hasClass("highlightRedBorder")){
				formErrorMessage = "<div class='usbSpriteIcon showErrorIcon floatLeft'>&nbsp;</div><div class='width130px padLeft-3px floatLeft last'>&nbsp;&nbsp;Please select an option.</div>";showNotification(formErrorMessage,notificationMessageObj,el,notificationObj,errorArrowObj,8,3,'redPopup','slow','show');
			}
		}
	});
}

/*
 * Function to register click,focus & blur events on select dropdown.
 * @return void
 */

function selectMenuFocus(){
	$(".ui-selectmenu").bind("click mouseout",function(){
		currentElemObj = $(this);
		$(this).focus();
	});
	$(".ui-selectmenu").bind("focus",selectFocus);
	$(".ui-selectmenu").bind("blur",function(){
		$(".ui-selectmenu").bind("focus",selectFocus);
		notificationObj.hide();
		var el = $(this);
		if(el.prev().val() == "") {
			if(el.hasClass("validated")){ 
			
				el.removeClass("validate validated highlightBlueBorder highlightRedBorder ui-selectmenu-error");
				el.addClass("validate highlightBlueBorder highlightRedBorder ui-selectmenu-error");
				el.prev().addClass("validate");
				showReqStarDropdown();
			}else if(el.hasClass("validate")){
			   if(ready){
			       ready=0;
				  }
				  else{
				      el.removeClass("highlightBlueBorder").addClass("highlightRedBorder ui-selectmenu-error");
					  }
			}else{   
				el.removeClass("highlightBlueBorder optionalField");
			}
		}else{
			if(el.hasClass("validate")){ 
				el.removeClass("validate highlightBlueBorder highlightRedBorder ui-selectmenu-error").addClass("validated");
				el.prev().removeClass("validate");
			}
		}
      		
	});
}


function loadSelectMenu(){
	/*selectMenu("#filter1",176,250,1,"hEstateSelection-button");
	$("#hEstateSelection").selectmenu({style: "dropdown"});
	$("#hEstateSelection-button, #filter1-button").css({"width":"178px"}).attr("title","");*/
	
	selectMenu("#hEstateSelection",176,250,0,"hEstateSelection-button");
	//$("#hEstateSelection-button").css({"width":"178px"}).attr("title","Suffix");
	//$('#hEstateSelection-menu').css('cssText',"width:188px !important; z-index: 10000;");
	$("#hEstateSelection-button").css('cssText',"width:178px !important");
	
	if($("#openAcc8").is("SELECT")) {
		$("#openAcc8").selectmenu({style: "dropdown"});
		$("#openAcc8-button").css({"width":"207px"});
		$("#openAcc8-menu").css("height","210px");
	}	
	changeInDropDownLocation();
}

function changeInDropDownLocation() {
	if($.browser.msie || $.browser.mozilla) {
		$("#hEstateSelection-menu").css('cssText','top:'+($("#hEstateSelection-button").offset().top + ($("#hEstateSelection-button").outerHeight() - 0))+'px !important; width:'+($("#hEstateSelection-button").outerWidth()-2)+'px !important; z-index: 10000;');
		$('#userID').css('cssText','z-index:10000;');
		//$("#hEstateSelection-menu").css({"top":($("#hEstateSelection-button").offset().top + ($("#hEstateSelection-button").outerHeight() - 0))+"px","width":($("#hEstateSelection-button").outerWidth()-2)+"px"});
		if($("#openAcc8").is("SELECT")) {
			$("#openAcc8-menu").css({"top":($("#openAcc8-button").offset().top + ($("#openAcc8-button").outerHeight() - 0))+"px","width":($("#openAcc8-button").outerWidth()-2)});
		}
	} else {
		$("#hEstateSelection-menu").css({"top":($("#hEstateSelection-button").offset().top + ($("#hEstateSelection-button").outerHeight() - 5))+"px","width":($("#hEstateSelection-button").outerWidth()-2)});
		if($("#openAcc8").is("SELECT")) {
			$("#openAcc8-menu").css({"top":($("#openAcc8-button").offset().top + ($("#openAcc8-button").outerHeight() - 5))+"px","width":($("#openAcc8-button").outerWidth()-2)});
		}
	}
}

//$("#hEstateSelection").bind('mouserover',function(){ loadSelectMenu()};

function selectFocus(){
	currentElemObj = $(this);
	errorArrowObj.css("left","20px");
	if($(this).hasClass("validated")) {
		$(this).removeClass("highlightRedBorder ui-selectmenu-error");
	} else if($(this).hasClass("validate")) {
		if(!$(this).hasClass("highlightRedBorder")){
			$(this).addClass("highlightBlueBorder blueSelect1");
		}else{
			var el = $(this);
			var formErrorMessage = "<div class='usbSpriteIcon showErrorIcon floatLeft'>&nbsp;</div>&nbsp;<i>"+el.attr('title')+"</i> is a required field.";
			el.addClass("highlightRedBorder ui-selectmenu-error");
			if ($(el).is('.ui-selectmenu-error')) {
				showNotification(formErrorMessage,notificationMessageObj,el,notificationObj,errorArrowObj,8,3,"redPopup","slow","show"); 
			}
		}
	}
}
function checkURL(current_url)
{
	var new_url;
	if(domainURL.match(/it/gi)||domainURL.match(/dev/gi)||domainURL.match(/uat/gi)){
	if(current_url.match(apply_pattern_search))
	{
		
		new_url=applyURL+current_url.substr(current_url.indexOf(".com/")+4);
		return new_url;
	}
	else if(current_url.match(tools_pattern_search))
	{
		new_url=toolsURL+current_url.substr(current_url.indexOf(".com/")+4);
		return new_url;
	}		
	else if(current_url.match(application_pattern_search)){
	new_url="https://uat-applications.usbank.com"+current_url.substr(current_url.indexOf(".com/")+4);
	return new_url;
	}
	else if(current_url.match(fastapp_pattern_search)){
		new_url="https://uat-fastapp.usbank.com"+current_url.substr(current_url.indexOf(".com/")+4);
		return new_url;
	}else if(current_url.match(login_url_search)){
		new_url="https://test1.www.usbank.com"+current_url.substr(current_url.indexOf(".com/")+4);
		return new_url;
	}else if(current_url.match("../../index.html"/*tpa=http://www.usbank.com/*/)){
		new_url="http://"+domainURL+current_url.substr(current_url.indexOf(".com/")+4);
		return new_url;
	}
	else if(current_url.match("https://www.usbank.com/homepage/homepage_js/loansandlines.usbank.com")){
		new_url="https://uat-loansandlines.usbank.com"+current_url.substr(current_url.indexOf(".com/")+4);
		return new_url;
	}else
	{
		return current_url;
	}
	}else
	{
		return current_url;
	}
}

$("#openAcc8Go").bind('click',function(){
	if($("#openAcc8").val()!='Select Account or Loan Type'){
		window.location=checkURL($("#openAcc8").val());
	}
});

$("#ad_one,#ad_two,#ad_three,#ad_four").bind('click',function(){
	window.location=$(this).find('a').attr('href');
});
/* validate.js code ends here */

/* homeCarousel.js code starts here */
//var jsFeatureArray = ['../../homepage_images/PER-1231-START-NewYears.jpg'/*tpa=https://www.usbank.com/homepage_images/PER-1231-START-NewYears.jpg*/,'../../homepage_images/PER-0618-HomeMortgageLowRates.jpg'/*tpa=https://www.usbank.com/homepage_images/PER-0618-HomeMortgageLowRates.jpg*/,'../../homepage_images/PER-0128-Mobiile-PayBills.jpg'/*tpa=https://www.usbank.com/homepage_images/PER-0128-Mobiile-PayBills.jpg*/,'../../homepage_images/PER-1231-Auto-375.jpg'/*tpa=https://www.usbank.com/homepage_images/PER-1231-Auto-375.jpg*/]; 
function loadAndRotateFeatures(fade, display) {
    if ('undefined' == typeof jsFeatureArray) {
        jsFeatureArray = '';
    }
    var features = jsFeatureArray;
    if (features == "") {
        return false;
    }
    var multipleFeatures = true;
    if (features.length < 2) {
        multipleFeatures = false;
    };

    var $homeFeatureDiv = $('div#homeFeature');
    var $IEfadeFix = $homeFeatureDiv.find('#IEfadeFix');
    var $homeFeatureList = $homeFeatureDiv.find('#homeFeatureList');
    var $currentListItem;
    var $currentListItemImg;
    var $currentListItemLink;
    var $currentListItemNav;
    var oldImgSrc;
    var newImgSrc;
    var timeout = null;
    var paused = false;
    $homeFeatureList.mouseover(function () {
        $homeFeatureList.find('a').addClass("featureLink-hovered")
    })
    $homeFeatureList.mouseout(function () {
        $homeFeatureList.find('a').removeClass("featureLink-hovered")
    })
    var isIE7or8 = false;
    if ($('body').is('#IE7') || $('body').is('#IE8')) {
        isIE7or8 = true;
    }
    if (isIE7or8) {
        $IEfadeFix.click(function () {
            window.location = $currentListItemLink.attr("href");
        }).mouseover(function () {
            $homeFeatureList.find('a').addClass("featureLink-hovered")
        }).mouseout(function () {
            $homeFeatureList.find('a').removeClass("featureLink-hovered")
        })
    };
    if (multipleFeatures) {
        var s = "<ul class='nav-horizontal hide' id='homeFeature-navigation'><li id='homeFeature-navigation-playButton'><span class='usbicon usbicon-bluePause'></span></li>"
        for (i = 0; i < features.length; i++) {
            s = s + "<li id='listItemNav" + i + "'>" + (i + 1) + "</li>";
        }
        s = s + "</ul>";
        $homeFeatureDiv.append(s);
        var $homeFeatureNavLIs = $homeFeatureDiv.find('ul#homeFeature-navigation li');
        var $playPauseButton = $homeFeatureNavLIs.filter(':first').find('span.usbicon');
        $homeFeatureNavLIs.not(':first').each(function (i) {
            $(this).data('linkNumber', i)
        }).click(function () {
            paused = false;
            clearTimeout(timeout);
            var linkNumber = $(this).data('linkNumber');
            navReset(linkNumber, 'slideLink');
            if ($playPauseButton.is('.usbicon-bluePlay')) {
                $playPauseButton.addClass('usbicon-bluePause').removeClass('usbicon-bluePlay');
            }
        }).end().hover(function () {
            $(this).addClass('hover')
        }, function () {
            $(this).removeClass('hover')
        });
        $playPauseButton.click(function () {
            if ($(this).is('.usbicon-bluePause')) {
                paused = true;
                if (isIE7or8) {
                    $IEfadeFix.stop().css('opacity', 0);
                } else {
                    $currentListItem.stop().css('opacity', 1);
                }
                $(this).removeClass('usbicon-bluePause').addClass('usbicon-bluePlay');
            } else {
                paused = false;
                $(this).addClass('usbicon-bluePause').removeClass('usbicon-bluePlay');
                navReset(slideNumber, 'playButton');
            }
            $(this).closest('li').removeClass('hover');
        });
    };
   	
	if(typeof slideNumberX == "undefined")
 {
  var slideNumber = Math.floor(Math.random() * features.length);
 }
  else
 {
  slideNumber = slideNumberX;
  }

    function navReset(linkNumber, sourceClick) {
        slideNumber = linkNumber;
        if (slideNumber >= features.length) {
            slideNumber = 0
        }
        loopCount++;
        if (isIE7or8) {
            $IEfadeFix.stop().fadeIn(250, function () {
                $currentListItem.css('display', 'none');
                if (sourceClick == 'playButton') {
                    slideNumber++;
                    if (slideNumber >= features.length) {
                        slideNumber = 0
                    };
                };
                $(this).css('opacity', 1);
                timeout = setTimeout(function () {
                    loadAndFade(slideNumber, loopCount)
                }, 100);
            });
        } else {
            $currentListItem.stop().fadeOut(250, function () {
                if (sourceClick == 'playButton') {
                    slideNumber++;
                    if (slideNumber >= features.length) {
                        slideNumber = 0
                    };
                };
                $(this).css('opacity', 1);
                timeout = setTimeout(function () {
                    loadAndFade(slideNumber, loopCount)
                }, 100);
            });
        }
    }
    var loopCount = 0;

    function loadAndFade(slideNumber, currentLoopCount) {
        if (loopCount == currentLoopCount) {
            $currentListItem = $homeFeatureDiv.find('#listItem' + slideNumber);
            $currentListItemImg = $homeFeatureDiv.find('#listItem' + slideNumber + ' > img');
            $currentListItemLink = $homeFeatureDiv.find('#listItem' + slideNumber + ' a');
			
            $currentListItemNav = $homeFeatureDiv.find('#listItemNav' + slideNumber)
            if (!($currentListItem.is('.loaded'))) {}
            oldImgSrc = $currentListItemImg.attr("src");
            newImgSrc = features[slideNumber];
            $currentListItem.click(function () {
                window.location = $currentListItemLink.attr("href");
            });
            if (oldImgSrc != newImgSrc) {
                $currentListItemImg.attr('src', newImgSrc).addClass('loaded').each(function () {
                    if (this.complete || (this.readyState == 'complete' && e.type == 'readystatechange')) {
                        timedFadeIn(currentLoopCount);
                    } else {
                        $(this).load(function () {
                            timedFadeIn(currentLoopCount);
                        });
                    };
                });
            } else {
                timedFadeIn(currentLoopCount);
            };
        }
    };

    function timedFadeIn(currentLoopCount) {
        if ((loopCount == currentLoopCount) && (paused == false)) {
            if (multipleFeatures) {
                updateNavLink($currentListItemNav);
            }
            if (isIE7or8) {
                $currentListItem.css('display', 'block');
                $IEfadeFix.fadeOut(fade, function () {
                    if (multipleFeatures) {
                        timeout = setTimeout(function () {
                            timedFadeOut(currentLoopCount)
                        }, display);
                    }
                });
            } else {
                $currentListItem.fadeIn(fade, function () {
                    if (multipleFeatures) {
                        timeout = setTimeout(function () {
                            timedFadeOut(currentLoopCount)
                        }, display);
                    }
                });
            }
        }
    };

    function updateNavLink($navLink) {
        $homeFeatureNavLIs.removeClass('current');
        $navLink.addClass('current');
    }

    function timedFadeOut(currentLoopCount) {
        if ((loopCount == currentLoopCount) && (paused == false)) {
            if (isIE7or8) {
                $IEfadeFix.fadeIn(fade, function () {
                    $currentListItem.css('display', 'none');
                    incrementSlideAndRestart(currentLoopCount)
                });
            } else {
                $currentListItem.fadeOut(fade, function () {
                    incrementSlideAndRestart(currentLoopCount)
                });
            }
        }
    };

    function incrementSlideAndRestart(currentLoopCount) {
        slideNumber++;
        if (slideNumber >= features.length) {
            slideNumber = 0
        };
        timeout = setTimeout(function () {
            loadAndFade(slideNumber, currentLoopCount)
        }, 100);
    }
    loadAndFade(slideNumber, loopCount);
};
(function ($) {
    $.fn.jCarouselLite = function (o) {
        o = $.extend({
            btnPrev: null,
            btnNext: null,
            btnGo: null,
            mouseWheel: false,
            auto: null,
            speed: 200,
            easing: null,
            vertical: false,
            circular: true,
            visible: 3,
            start: 0,
            scroll: 1,
            beforeStart: null,
            afterEnd: null
        }, o || {});
        return this.each(function () {
            var b = false,
                animCss = o.vertical ? "top" : "left",
                sizeCss = o.vertical ? "height" : "width";
            var c = $(this),
                ul = $("ul", c),
                tLi = $("li", ul),
                tl = tLi.size(),
                v = o.visible;
            if (o.circular) {
                ul.prepend(tLi.slice(tl - v - 1 + 1).clone()).append(tLi.slice(0, v).clone());
                o.start += v
            }
            var f = $("li", ul),
                itemLength = f.size(),
                curr = o.start;
            c.css("visibility", "visible");
            f.css({
                overflow: "hidden",
                float: o.vertical ? "none" : "left"
            });
            ul.css({
                margin: "0",
                padding: "0",
                position: "relative",
                "list-style-type": "none",
                "z-index": "1"
            });
            c.css({
                overflow: "hidden",
                position: "relative",
                "z-index": "2",
                left: "0px"
            });
            var g = o.vertical ? height(f) : width(f);
            var h = g * itemLength;
            var j = g * v;
            f.css({
                width: f.width(),
                height: f.height()
            });
            ul.css(sizeCss, h + "px").css(animCss, -(curr * g));
            c.css(sizeCss, j + "px");
            if (o.btnPrev) $(o.btnPrev).click(function () {
                return go(curr - o.scroll)
            });
            if (o.btnNext) $(o.btnNext).click(function () {
                return go(curr + o.scroll)
            });
            if (o.btnGo) $.each(o.btnGo, function (i, a) {
                $(a).click(function () {
                    return go(o.circular ? o.visible + i : i)
                })
            });
            if (o.mouseWheel && c.mousewheel) c.mousewheel(function (e, d) {
                return d > 0 ? go(curr - o.scroll) : go(curr + o.scroll)
            });
            if (o.auto) setInterval(function () {
                go(curr + o.scroll)
            }, o.auto + o.speed);

            function vis() {
                return f.slice(curr).slice(0, v)
            };

            function go(a) {
                if (!b) {
                    if (o.beforeStart) o.beforeStart.call(this, vis());
                    if (o.circular) {
                        if (a <= o.start - v - 1) {
                            ul.css(animCss, -((itemLength - (v * 2)) * g) + "px");
                            curr = a == o.start - v - 1 ? itemLength - (v * 2) - 1 : itemLength - (v * 2) - o.scroll
                        } else if (a >= itemLength - v + 1) {
                            ul.css(animCss, -((v) * g) + "px");
                            curr = a == itemLength - v + 1 ? v + 1 : v + o.scroll
                        } else curr = a
                    } else {
                        if (a < 0 || a > itemLength - v) return;
                        else curr = a
                    }
                    b = true;
                    ul.animate(animCss == "left" ? {
                        left: -(curr * g)
                    } : {
                        top: -(curr * g)
                    }, o.speed, o.easing, function () {
                        if (o.afterEnd) o.afterEnd.call(this, vis());
                        b = false
                    });
                    if (!o.circular) {
                        $(o.btnPrev + "," + o.btnNext).removeClass("disabled");
                        $((curr - o.scroll < 0 && o.btnPrev) || (curr + o.scroll > itemLength - v && o.btnNext) || []).addClass("disabled")
                    }
                }
                return false
            }
        })
    };

    function css(a, b) {
        return parseInt($.css(a[0], b)) || 0
    };

    function width(a) {
        return a[0].offsetWidth + css(a, 'marginLeft') + css(a, 'marginRight')
    };

    function height(a) {
        return a[0].offsetHeight + css(a, 'marginTop') + css(a, 'marginBottom')
    }
})(jQuery);;
function createMarketingCarousel($theCarousel) {
    $theCarousel.jCarouselLite({
        btnNext: ".next",
        btnPrev: ".prev",
        auto: null,
        speed: 1000,
        circular: true,
        scroll: 1
    });
    $theCarousel.find(".prev").hover(function () {
        $(this).addClass("usbicon-carouselleft-hover")
    }).mouseout(function () {
        $(this).removeClass("usbicon-carouselleft-hover")
    });
    $theCarousel.find(".next").hover(function () {
        $(this).addClass("usbicon-carouselright-hover")
    }).mouseout(function () {
        $(this).removeClass("usbicon-carouselright-hover")
    });
};
function roundCornersInIE() {
    $('div.ieRounded.span-border').removeClass('ieRounded').wrap('<div class="ieRounded ieRoundedBorderedWrapper"></div>')
    if ($.browser.msie) {
        if ((parseInt(jQuery.browser.version)) <= 6) {
            $('div.ieRounded').each(function () {
                if ($(this).height() % 2 != 0) {
                    $(this).css('height', ($(this).height() + 1) + 'px')
                }
            })
        }
    }
    $('div.ieRounded').append("<div></div> \
     <div class='ieCorner ieCorner-topLeft'></div> \
     <div class='ieCorner ieCorner-topRight'></div> \
     <div class='ieCorner ieCorner-bottomLeft'></div> \
     <div class='ieCorner ieCorner-bottomRight'></div> \
     ")
    $('div.ieRoundedTop').append("<div></div> \
     <div class='ieCorner ieCorner-topLeft'></div> \
     <div class='ieCorner ieCorner-topRight'></div> \
     ");
    $('div.ieRoundedBottom').append("<div></div> \
     <div class='ieCorner ieCorner-bottomLeft'></div> \
     <div class='ieCorner ieCorner-bottomRight'></div> \
     ");
};
/* homeCarousel.js code ends here */

/*index-oof.js starts here */
/**Out of foot print area**/
//var OFPA = new Array("AK","CT","DC","FL","GA","HI","LA","MA","MD","MS","NC","NH","NJ","NM","NY","RI","ME","VT");
// Modified on 04-Oct-2011.
//var OFPA = new Array("AK","CT","DC","FL","GA","HI","LA","MA","MD","MS","MI","NC","NH","NJ","NY","RI","ME","VT","PA","OK","WV","AL");
// Modified on 18-Oct-2011.
var OFPA = new Array("AK","CT","DC","FL","GA","HI","LA","MA","MD","MS","NC","NH","NJ","NY","RI","ME","VT");
/**Out of lending area**/
var OLA = new Array("DE","TX","SC");
var clickedButton;
var applyURL;
var state;
var zipInvalid;
var message = "Please enter a valid 5-digit ZIP code.";
var zipHtml = "";
var prevHtml="";


$(document).ready(function(){
//ZIP Code Light box HTML Fragment Retrieval
$(".mask").css("cssText","z-index:9000");
$.ajax({
		url: 'https://www.usbank.com/htmlFragments/collateral-zip-variation.html', 
		async: true, 
		dataType: "html", 
		success: function(html) 			
		{			
			zipHtml = html;
			$('.modalHeader > ul > li.forZip').addClass('visibilityHidden');
			$('#printWindow').remove();
		}});


});

/*Modal Window or Light Box Functionality*/
function showModal(id){
$("#"+id).dialog({
autoOpen: false,
width : Mwidth+30+"px",
position :'center',
close : modalCloseFun
});
$('#'+id).dialog("open");
resizeModal();
} 

function modalCloseFun()
{
$('#actualContent').html(prevHtml);
$('.visibilityVisible').removeClass('visibilityVisible').addClass('visibilityHidden');
if(!$('.svcError').hasClass("hide")){
      $('.svcError').addClass("hide")
}
$("#collateralZip").unbind();
$("#submitZip").unbind();
$('#modal').dialog("destroy");
$('.mask').hide();
notificationObj.hide();
} 

function resizeModal()
{
var windowWidth1;
if(window.innerWidth){
windowWidth1 = window.innerWidth;
}else{
windowWidth1 = document.documentElement.clientWidth;
}
var left = (windowWidth1-(Mwidth+30))/2;
$(".ui-dialog").css({"left":left+"px","top":"25px","position":"absolute"});
} 

function modalFunction()
{
$("#closeWindow, #closeImage").bind("click", function() {
modalCloseFun();
});
} 

/*Opening Light Box on Click of any Apply Now Link on the page*/
$("a").live("click",function(e){
    if($(this).attr("href").indexOf("https://www.usbank.com/apply/apply.html?PRODUCT_CODE=HEQ")!=-1){
    /*Omniture Code 16-September-2011
	if($(this).attr("href")==undefined){
	   s.tl(this,'o','usb:tool:home equity calculator:rates and payments calculator:apply');
	 }else{
	   s.tl(this,'o','usb:tool:home equity calculator:index:apply');
	 }*/
    e.preventDefault();
	prevHtml = $('#actualContent').html();
	$('#actualContent').html(zipHtml);
	$("#collateralZip").val("");
	$("#collateralZip").prev().show();
	$('.modalHeader > ul > li.forZip').addClass('visibilityHidden');
	$('#printWindow').remove();
	$("#submitZip").attr("action","javascript:void(0);");
	Mwidth = 510; //Setting width of LightBox
	var tmpLoanAmount;
	var tmpProdCode;
	applyURL = $(this).attr("href");
    clickedButton = $(this);
	notificationObj.hide();
	var winH = $(document).height();
	var winW = $(window).width();			
	$('.mask').css({'width' :winW, 'height' :winH});							
	$('.mask').show();	
	showModal("modal");		
	modalFunction();
	$('html, body').animate({scrollTop: $('.ui-dialog').offset().top-50}, 1);		
	ZipFlowFunction();
	ZipValidation();
	if($("#collateralZip").val()!=""){
		$("#collateralZip").focus();
	}
    }

});

/*Allowing Only Numbers */
function ZipFlowFunction()
 {
	$('#collateralZip').bind('keypress',function(event){		
			var key;
			var isCtrl = false;
			var keychar;
			var reg;		
			if(window.event) {
				key = event.keyCode;
				isCtrl = window.event.ctrlKey
			}
			else if(event.which) {
				key = event.which;
				isCtrl = event.ctrlKey;
			}	
			if (isNaN(key)) return true;
			keychar = String.fromCharCode(key);
			if (key == 8 || key==13 || isCtrl)
			{
				return true;
			}
			reg = /\d/;
			return reg.test(keychar);
		});
	
 }
 
/* Zip Validation */
function ZipValidation(){
 $("#collateralZip").bind("click focus",function(e){
            
			var el = currentElemObj = $(this);
			
			var idVal = el.attr("id");
			if(el.hasClass("specialInput") && !el.hasClass("validate")){
				el.addClass("highlightBlueBorder");
			}else{
				if($("label[for="+idVal+"]").hasClass("infieldLabel")){
					$(this).prev().css({"opacity":".5","filter":"alpha(opacity=50)"});
				}
				var t = el.attr("id");
				if(el.hasClass("validate")){
					$(".formErrorContent").removeClass("hide");				
					if(!el.hasClass("highlightRedBorder")){
						el.addClass("highlightBlueBorder");
					}else{
						el.addClass("highlightRedBorder invalid");		
						if ($(el).is('.invalid')) {
							formErrorMessage = "<div class='usbSpriteIcon showErrorIcon floatLeft'>&nbsp;</div>&nbsp;"+message;
							showNotification(formErrorMessage,notificationMessageObj,el,notificationObj,errorArrowObj,8,3,"redPopup","slow","show");
						}
					}
				}else{			
					el.addClass("highlightBlueBorder");
				}
			}
	});
$("#collateralZip").bind("keyup change",function(e){
    if(($.trim($(this).val()).length != 5)){
	   $(this).removeClass("validated").addClass("validate");
	}
	var idVal = $(this).attr("id"); 
    if(($.trim($(this).val()).length > 0)){
	      if(e.keyCode!=13){
          notificationObj.hide();
		  }
          if($("label[for="+idVal+"]").hasClass("infieldLabel")){
          $(this).prev().hide();
        }	
    }else{

          if($("label[for="+idVal+"]").hasClass("infieldLabel")){
            $(this).prev().show();
        }
      } 
	
});
	
$("#collateralZip").bind("blur",function(e){
			if(e.keyCode!=13){
			notificationObj.hide();
			}
			var el = $(this);
			var idVal = el.attr("id");
			if(el.hasClass("specialInput") && !el.hasClass("validate")){
				el.removeClass("highlightBlueBorder");
			}else if(el.hasClass("optional")){	
				el.removeClass("highlightBlueBorder optionalField");
				notificationObj.removeClass("greenPopup");			
				notificationObj.css("display","none");
			}else{
				if($("label[for="+idVal+"]").hasClass("infieldLabel")){
					$("label[for="+idVal+"]").css({"opacity":"1","filter":"alpha(opacity=100)"});
				}
				
				if(el.hasClass("validated")){
					el.removeClass("highlightBlueBorder highlightRedBorder invalid");
				}else if(el.hasClass("validate") && $.trim(el.val()).length<5){
					el.removeClass("highlightBlueBorder").addClass("highlightRedBorder invalid");
				}else if($.trim(el.val()).length==5){
				    el.removeClass("highlightBlueBorder highlightRedBorder invalid").addClass("validated");
				}
				else{			
					el.removeClass("highlightBlueBorder optionalField");
				}
			}
	});

	$("#submitZip").bind("submit",function(){
       formSubmit();
 });
}
 
function formSubmit(){
		   el = $("#collateralZip");
		if($.trim(el.val()).length==5 && el.val()!=0 && !isNaN(el.val())){
				//Calling Zip Code Mapping Service to get the State Code for the Zip code entered.
				$.jsonp({
				url: "https://www.usbank.com/ICZipcodeRestServiceWeb/ZipcodeMapping.html", 
				data : {"ZipCode":$.trim(el.val()),"o":"json","callback":"callback"},
				async: false,	
				callback: "callback",
				success: function(json) {
					gZipCode = el.val();			
					zipInvalid = true;
					$('.svcError').addClass('hide');
					if(json.ZipCodeMappingVO!="undefined"&&json.ZipCodeMappingVO!=null)
					{
						$("#hidezip").hide();
						state=json.ZipCodeMappingVO.stateCode;	
						zipInvalid = false;
						if(checkOutOfFootPrint(state) || checkOutOfLending(state)){
								//$("#submitZip").attr("action","https://www.usbank.com/home-equity/start-process.html?oof=y").submit();
						    window.location.href="https://www.usbank.com/home-equity/start-process.html?oof=y";
						}else{
						    //$("#submitZip").attr("action",applyURL+'&COLLATERAL_ZIP_CODE='+el.val()).submit();
						    window.location.href=applyURL+"&COLLATERAL_ZIP_CODE="+el.val();
						}
					}else if(json.Error!="undefined"&&json.Error!=null)
					{
						if($.trim(json.Error.code)=="EA006")
						{
							el.removeClass("highlightBlueBorder").addClass("highlightRedBorder invalid validate");
							formErrorMessage = "<div class='usbSpriteIcon showErrorIcon floatLeft'>&nbsp;</div>&nbsp;"+message;
							showNotification(formErrorMessage,notificationMessageObj,el,notificationObj,errorArrowObj,8,3,"redPopup","slow","show");
						}else
						{
							$('.svcError').html(json.Error.description);
							$('.svcError').removeClass('hide');
						}
					}
				},
				error : function(err){
					$('.svcError').html('We\'re sorry; our system is experiencing technical difficulties.  Please return and try again later.');			
					$('.svcError').removeClass('hide');
				}
			});
				
				
		}	else
	    {
		   validateZip(el);
	    }
	}

	
 
 function validateZip(el)
 {
	var regExp = /[0-9]/;
		if($.trim(el.val()) == ""||$.trim(el.val())==0||$.trim(el.val()).length<5||!regExp.test($.trim(el.val()))){
			//message = "is a required field.";
			el.addClass("highlightBlueBorder highlightRedBorder invalid");
			formErrorMessage = "<div class='usbSpriteIcon showErrorIcon floatLeft'>&nbsp;</div>&nbsp;"+message;
			showNotification(formErrorMessage,notificationMessageObj,el,notificationObj,errorArrowObj,8,3,"redPopup","slow","show");
		return false;
		}
 }

 
 /***Function for checking out of footprint area***/
function checkOutOfFootPrint(st)
{
	for(i=0;i<OFPA.length;i++)
	{	
		if(OFPA[i]==st)
		{
			return true;
		}
	}
	return false;
}
/***Function for checking out of lending area***/
function checkOutOfLending(st)
{
	for(i=0;i<OLA.length;i++)
	{	
		if(OLA[i]==st)
		{
			return true;
		}
	}
	return false;
}

/* index-oof.js*/

/*jsonp.js starts here*/

// jquery.jsonp 2.1.4 (c)2010 Julian Aubourg | MIT License
// http://code.google.com/p/jquery-jsonp/
(function(e,b){function d(){}function t(C){c=[C]}function m(C){f.insertBefore(C,f.firstChild)}function l(E,C,D){return E&&E.apply(C.context||C,D)}function k(C){return/\?/.test(C)?"&":"?"}var n="async",s="charset",q="",A="error",r="_jqjsp",w="on",o=w+"click",p=w+A,a=w+"load",i=w+"readystatechange",z="removeChild",g="<script/>",v="success",y="timeout",x=e.browser,f=e("head")[0]||document.documentElement,u={},j=0,c,h={callback:r,url:location.href};function B(C){C=e.extend({},h,C);var Q=C.complete,E=C.dataFilter,M=C.callbackParameter,R=C.callback,G=C.cache,J=C.pageCache,I=C.charset,D=C.url,L=C.data,P=C.timeout,O,K=0,H=d;C.abort=function(){!K++&&H()};if(l(C.beforeSend,C,[C])===false||K){return C}D=D||q;L=L?((typeof L)=="string"?L:e.param(L,C.traditional)):q;D+=L?(k(D)+L):q;M&&(D+=k(D)+encodeURIComponent(M)+"=?");!G&&!J&&(D+=k(D)+"_"+(new Date()).getTime()+"=");D=D.replace(/=\?(&|$)/,"="+R+"$1");function N(S){!K++&&b(function(){H();J&&(u[D]={s:[S]});E&&(S=E.apply(C,[S]));l(C.success,C,[S,v]);l(Q,C,[C,v])},0)}function F(S){!K++&&b(function(){H();J&&S!=y&&(u[D]=S);l(C.error,C,[C,S]);l(Q,C,[C,S])},0)}J&&(O=u[D])?(O.s?N(O.s[0]):F(O)):b(function(T,S,U){if(!K){U=P>0&&b(function(){F(y)},P);H=function(){U&&clearTimeout(U);T[i]=T[o]=T[a]=T[p]=null;f[z](T);S&&f[z](S)};window[R]=t;T=e(g)[0];T.id=r+j++;if(I){T[s]=I}function V(W){(T[o]||d)();W=c;c=undefined;W?N(W[0]):F(A)}if(x.msie){T.event=o;T.htmlFor=T.id;T[i]=function(){/loaded|complete/.test(T.readyState)&&V()}}else{T[p]=T[a]=V;x.opera?((S=e(g)[0]).text="jQuery('#"+T.id+"')[0]."+p+"()"):T[n]=n}T.src=D;m(T);S&&m(S)}},0);return C}B.setup=function(C){e.extend(h,C)};e.jsonp=B})(jQuery,setTimeout);


/*jsonp.js ends here*/


/* New top navigation code : PID 246 */

// Below funtion creates the Mega Menu
// required plugin: jquery.hoverIntent.js
$(document).ready(function(){ 
createMenu();	
});
function createMenu(){
	// cache the selectors
	var $menuBar = $('.ssi_horizanMenu'); //used only to get other variable only in this section
	var $megaMenuTriggers = $menuBar.find('a.ssi_megaMenu-trigger');
	var $megaMenus = $menuBar.find('div.ssi_megaMenu'); 
	var $megaMenuTriggersUL = $megaMenuTriggers.closest('ul'); 
	var $megaMenuTriggersLIs = $megaMenuTriggersUL.children('li');
	var $megaMenuTriggersLIsMinusSiloName = $megaMenuTriggersLIs.filter('li:not(li:first)'); 	
	//var $currentFocusedLink; now this variable defined below by abhishek
	// give each Trigger a unique ID so that we can navigate through key board
	$megaMenuTriggersLIsMinusSiloName.each(function(i){	$(this).attr("id","megaTrigger-"+i); });
	// adding a class if the browser needs to use an IMG for the drop shadow
	if (!(typeof document.body.style.MozBoxShadow === 'string' || typeof document.body.style.WebkitBoxShadow ===  'string' || typeof document.body.style.BoxShadow === 'string')) {
	
		// this is browser that doesn't support CSS box shadows, so we will use images
		$megaMenus.each(function(){
		
			if($(this).is('.ssi_megaMenu-wide')){
				$(this).addClass('ssi_megaMenu-imageShadowWide')
			}else{
				$(this).addClass('ssi_megaMenu-imageShadow')
			};
		});
	};
	// configuration forhoverIntent
	var megaConfig = {
         interval: 250,
         sensitivity: 8,
         over: function(){addMegaMenu($(this))},
         timeout: 100,
         out: function(){removeMegaMenu($(this))}
    };
// attach the hover and focus behaviors	
	var blurTimer; // focusing on each sub menu cancels this. Therefore, if it's not canceled, we'll assume we're  no longer in the menu
    var $currentFocusedLink;
	$megaMenuTriggersLIsMinusSiloName.hoverIntent(megaConfig)
	.find('a:first') // tab links
			.each(function(){
				var $thisTab = $(this).parent('li');
				$(this)
					.focus(function(event){
						$currentFocusedLink = $(this);
						addMegaMenu($thisTab);
						clearTimeout (blurTimer);
					})
					
			}).end()
		.find('div.ssi_megaMenu a')	// secondary links
			.focus(function(){
				$currentFocusedLink = $(this);
				clearTimeout (blurTimer);
			})								 
	// enable arrow key navigation
	$('.horizanMenu ul li.nav-main-tab').keydown(function (eh){	
		var keyPressed = eh.keyCode*1;
		if ((keyPressed >= 37)&&(keyPressed <= 40)){
			eh.preventDefault();
			var $thisLI = $currentFocusedLink.closest('li');
			var $nextMainTab =  $currentFocusedLink.closest('li.ssi_nav-main-tab').next('li.ssi_nav-main-tab:first');
			var $prevMainTab =  $currentFocusedLink.closest('li.ssi_nav-main-tab').prev('li.ssi_nav-main-tab:first');
			var $outBeforeFocus = $('.ssi_horizanMenu').find('ul.ssi_nav-main li:first').find('a:first'); // home  link
			var $outAfterFocus = $('.ssi_horizanMenu').find('#ssi_search-searchterm'); // search field
			switch(keyPressed)
				{
				case 37:
					// left
					if ($prevMainTab.length > 0){
						$prevMainTab.find('a:first').focus()
					}else{
						// this was the first tab, so focus on home link and close the tab as  we leave
						$outBeforeFocus.focus();
						blurTimer = setTimeout (function(){
								 removeMegaMenu($currentFocusedLink.closest('li.ssi_nav-main-tab'))
						}, 500)
					};
					break;
				case 38:
					//up
					if( ($thisLI.is('.ssi_nav-main-tab')) && ($prevMainTab.length == 0) ){
						// this was the first tab main tab, so focus on home link and close the  tab as we leave
						$outBeforeFocus.focus();
						blurTimer = setTimeout (function(){
								 removeMegaMenu($currentFocusedLink.closest('li.ssi_nav-main-tab'))
						}, 500)
					}else{
						if ($thisLI.prev('li').length) {
							$thisLI.prev('li').find('a').focus()
						}else{
							$thisLI.closest('li.ssi_nav-main-tab').find('a:first').focus()
						};	
					};
					break;
				case 39:
					// right
					if ($nextMainTab.length > 0){
						$nextMainTab.find('a:first').focus()
					}else{
						// this was the last tab, so jump to search
						$outAfterFocus.focus()
						blurTimer = setTimeout (function(){
								 removeMegaMenu($currentFocusedLink.closest('li.ssi_nav-main-tab'))
						}, 500)
					};
					break;
				case 40:
					// down
					if($thisLI.is('.ssi_nav-main-tab')){
						$thisLI.find('li:first a').focus()
					}else{
						if ($thisLI.next('li').length) {
							$thisLI.next('li:first').find('a').focus()
						}else{
							if ($nextMainTab.length > 0){
								$nextMainTab.find('a:first').focus()
							}else{
								// this was the last tab, so jump to search
								$outAfterFocus.focus()
								blurTimer = setTimeout (function(){
										 removeMegaMenu($currentFocusedLink.closest('li.ssi_nav-main-tab'))
								}, 500)
							};
						};
					};
				  break;
				};
		};
	});
};
// show the mega on hover
function addMegaMenu($theLI){
	var $navbar = $('div.ssi_horizanMenu');
	var triggerPosition = $theLI.find('a.ssi_megaMenu-trigger').position(); //  To set the y position 
	var navbarHeight = $navbar.height();
	var navbarWidth = $navbar.width();
	var xpos = triggerPosition.left;
	// A menu might be open from keyboard tab focus. remove it if it occurs 
	var $openMenus = $theLI.parent("ul").find('li.ssi_megaMenu-trigger-hover');
	$openMenus.each(function(){
			if ($(this).attr("id") !=  $theLI.attr("id")){ 
				removeMegaMenu($(this))
			};
		});
// handle the megamenu to open from right to left if it exceeds the lenght of main menubar
	var ypos = navbarHeight;
	$theLI.addClass('ssi_megaMenu-trigger-hover ssi_megaMenu-trigger-hover-visualStyle').each(function(){
			if (!(typeof document.body.style.MozBoxShadow === 'string')){
				// this isn't FF 3.5, therefore the main nav tab needs an image for the gradient
				$(this).addClass('ssi_megaMenu-trigger-hover-imageGradient');
			};
		}).addClass('roundedTop').find('div.ssi_megaMenu').each(function(){
				/* handle menu and open it from right to left if it exceeds the page */
				if (xpos > (navbarWidth-$(this).width())){
					xpos = (xpos-$(this).width()) + $theLI.width();
				}; 		   
				$(this).css({
					top: ypos+5,
					left: xpos
				}).css("z-index","9999")  
				if(!($.browser.msie)){
					$(this)
						.css('opacity', 1)
				};					
			}).end().prev('li').addClass('ssi_megaMenu-trigger-hover-visualStyle-leftSibling');
};
// ends here --handle the megamenu to open from right to left 
// remove the mega on mouseout
function removeMegaMenu($theLI){
	$theLI.removeClass('ssi_megaMenu-trigger-hover-visualStyle ssi_megaMenu-trigger-hover-imageGradient')
		.find('a.ssi_megaMenu-trigger').removeClass('roundedTop').end()
		.find('div.ssi_megaMenu').each(function(){
				if(!($.browser.msie)){
					$(this).animate({ 
							opacity: 0
							}, 0, function(){
								 $(this).parent('li').removeClass('ssi_megaMenu-trigger-hover')
							})
				}else{
					$(this).parent('li').removeClass('ssi_megaMenu-trigger-hover')
				};	
			}).end().prev('li').removeClass('ssi_megaMenu-trigger-hover-visualStyle-leftSibling');
};
/*adding shadow image in the MegaMenu*/
$('div.ssi_innerWrapper').children('ul').each(function(){
			    if($(this).next().html()==null)
				{
				    $(this).parent().attr('style','width:195px !important');
					$(this).parent().parent().attr('style','background:url("../../images/comp_2_bgnd_mega_shadow_157.png"/*tpa=https://www.usbank.com/images/comp_2_bgnd_mega_shadow_157.png*/) no-repeat scroll left bottom transparent');
				}
			});
/*adding shadow image in the MegaMenu*/
// end ---------------------------------------------------------------------------------------------------------------- 

function ie7ZindexIssue() {
	var zIndexNo=1000;
	$('div').each(function() {
		$(this).css('zIndex',zIndexNo);
		zIndexNo-=5;
	});
}/*;e"+id).css("left", leftPos);
	});
});*/
/* New top navigation code : PID 246  : End*/
/* 3213: start*/
$(document).ready(function(){

var domainURL = window.location.hostname;
var uat5Reg = RegExp('uat5[.]www[.]usbank[.]com')
if(uat5Reg.test(domainURL)){
	$.each($('a'),function(index){ 		
				if(this.href=="https://onlinebanking.usbank.com/Auth/Login"){
						this.href ="https://uat1-onlinebanking.usbank.com/Auth/Login";
				}
	});
}

$('.olbInvestmentLogin').click(function(e){
e.preventDefault();
$('<fo'+'rm method="post" action="'+$(this).attr('href')+'" />')
.append($('<input type="hidden" name="userType" value="USBI">'))
.appendTo($(document.body))
.submit();
});


});
/* 3213: end*/ 
