/* Start of MotionPoint translation functions.*/
var domainURLmp = window.location.hostname;

var MP = {

<!-- mp_trans_disable_start --> 
  Version: 'https://www.usbank.com/homepage_js/components/1.0.22',
  Domains: {'es':'https://www.usbank.com/homepage_js/components/espanol.usbank.com'},	
  SrcLang: 'en',
<!-- mp_trans_disable_end -->
  UrlLang: 'mp_js_current_lang',
  SrcUrl: unescape('mp_js_orgin_url'),
<!-- mp_trans_disable_start --> 	
  init: function(){
    if (MP.UrlLang.indexOf('p_js_')==1) {
      MP.SrcUrl=window.top.document.location.href;
      MP.UrlLang=MP.SrcLang;
  }
},

switchLanguage: function(lang){
	/* Only check when going from English to Spanish*/
	if(domainURLmp!='https://www.usbank.com/homepage_js/components/espanol.usbank.com'){
	if (lang=='es') {
		if ( !checkEspanolAlertCookie( 'ESPANOLALERTED' ) ) {
			displayEspanolDialog();
		}
	}
	}

  if(lang!=MP.SrcLang){
    var script=document.createElement('SCRIPT');
    script.src=location.protocol+'//'+MP.Domains[lang]+'/'+MP.SrcLang+lang+'/?1023749632;'+encodeURIComponent(MP.SrcUrl);
	document.body.appendChild(script);
  } else if(lang==MP.SrcLang && MP.UrlLang!=MP.SrcLang){
    var script=document.createElement('SCRIPT');
    script.src=location.protocol+'//'+MP.Domains[MP.UrlLang]+'/'+MP.SrcLang+MP.UrlLang+'/?1023749634;'+encodeURIComponent(location.href);
	document.body.appendChild(script);
  }
  return false;
},
/*switchToLang: function(url) {
    var mplink=document.createElement('A');
	if (mplink.click) { // using location.href will cause IE6 to not report referrer
		mplink.href=url;
        window.top.document.body.appendChild(mplink);
        mplink.click();
	} else {
	    window.top.location.href=url; 
	}
}*/
switchToLang: function(url) {
  window.top.location.href=url; 
}
<!-- mp_trans_disable_end -->	
};
/* End of MotionPoint translation functions.*/
      
      
 function checkEspanolAlertCookie(check_name) {      
 /* This function will determine if this user has seen the Espanol "In progress" pop up message.*/        
         
    // first we'll split this cookie up into name/value pairs     
    // note: document.cookie only returns name=value, not the other components     
    var a_all_cookies = document.cookie.split( ';' );     
    var a_temp_cookie = '';     
    var cookie_name = '';     
    var cookie_value = '';     
    var b_cookie_found = false; // set boolean t/f default f     
      
    for ( i = 0; i < a_all_cookies.length; i++ )     
    {     
        // now we'll split apart each name=value pair     
        a_temp_cookie = a_all_cookies[i].split( '=' );     
      
        // and trim left/right whitespace while we're at it     
        cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g, '');     
      
        // if the extracted name matches passed check_name     
        if ( cookie_name == check_name )    {     
            b_cookie_found = true;     
            // we need to handle case where cookie has no value but exists (no = sign, that is):     
            if ( a_temp_cookie.length > 1 ) {     
                cookie_value = unescape( a_temp_cookie[1].replace(/^\s+|\s+$/g, '') );     
            }     
            // note that in cases where cookie is initialized but no value, null is returned     
            return cookie_value;     
            break;     
        }     
        a_temp_cookie = null;     
        cookie_name = '';     
    }     
    if ( !b_cookie_found ) {     
        return null;     
    }     
 }      
      
 function setEspanolAlertCookie( name, value, expires, path, domain, secure ) {      
    // set time, it's in milliseconds     
    var today = new Date();     
    today.setTime( today.getTime() );     
         
    /*     
    if the expires variable is set, make the correct expires time, the current script below will set     
    it for x number of days, to make it for hours, delete * 24, for minutes, delete * 60 * 24     
    */     
    if ( expires )  {     
        expires = expires * 1000 * 60 * 60 * 24;     
        }     
      
    var expires_date = new Date( today.getTime() + (expires) );     
         
    document.cookie = name + "=" +escape( value ) +     
    ( ( expires ) ? ";expires=" + expires_date.toGMTString() : "" ) +     
    ( ( path ) ? ";path=" + path : "" ) +     
    ( ( domain ) ? ";domain=" + domain : "" ) +     
    ( ( secure ) ? ";secure" : "" );     
    }     
      
 function displayEspanolDialog() {      
      
    var alertPage = 'https://www.usbank.com/espanolAlert.cfm';     
    var domain = getDomain(document.referrer);     
      
    if (domain == 'https://www.usbank.com/homepage_js/components/mtg.usbank.com') {     
        var alertPage = 'http://www.usbank.com' + alertPage;     
    } else if (domain == 'https://www.usbank.com/homepage_js/components/uat2.mtg.usbank.com') {     
        var alertPage = 'http://uat2.www.usbank.com' + alertPage;     
    }     
      
    /* Determine the users resolution so we can center the pop-up window. */     
         
    /* get the users screen resolution */     
    espW = screen.availWidth;     
    espH = screen.availHeight;     
         
    /* set the default popup window size */     
    var espPopupW = 500;     
    var espPopupH = 500;     
         
    /* set the upper left hand corner of the popup window */     
    var espPopupLeftCorner = (espW-espPopupW)/2;     
    var espPopupTopCorner = (espH-espPopupH)/2;      
      
    var espAlertWindow=window.open(alertPage,"espAlert","directories=no,location=no,menubar=no,resizable=yes,status=no,toolbar=no,scrollbars=no,width=" + espPopupW + ",height=" + espPopupH + ",top=" + espPopupTopCorner + ",left=" + espPopupLeftCorner);     
    espAlertWindow.focus();     
    }     
 // -->      
