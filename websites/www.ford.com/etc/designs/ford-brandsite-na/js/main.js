Number.prototype.mod = function(n) { return ((this%n)+n)%n; }

'use strict';

// fix modulus

/**
 * @name fordCarousel
 *
 * @requires _
 * @requires jQuery
 * @requires jquery.hammer
 *
 * @description
 * Ford's carousel jQuery plugin
 */
(function fordCarouselInit($) {

	var methodMap = {
			left: 'next',
			right: 'previous'
		},
		isInitialized = false,
		instances = [];

	function initFordCarousel() {
		if (isInitialized) {
			return;
		}

		isInitialized = true;

		// Listen for the window's resize (250ms throttle) and call
		$(window)
			.on('unload', function onWindowUnload() {
				$(this).off('resize.ford-carousel');
				// For each carousel instance, call destroy()
				_.invoke(instances, 'destroy', true);
				instances = null;
			})
			.on('resize.ford-carousel', _.throttle(function onWindowResize(event) {
				_.invoke(instances, 'onWindowResize');
			}, 250));
	}

	/**
	 * @constructor
	 * @name FordCarousel
	 *
	 * @param {jQuery.element} $elm - The carousel element
	 *
	 * @returns {FordCarousel instance} The newly created instance
	 *
	 * @description
	 * Handles caoursel interations and responsiveness
	 */
	function FordCarousel($elm) {
		this.animationTime  = 500,
		this.$elm           = $elm,
		this.$ul            = null,
		this.$li            = null,
		this.$pagination    = null,
		this.index          = 0,
		this.page           = 0, //legacy
		this.count          = 1,
		this.isAnimating    = !1,
		this.isUpdating     = !1,
		this.slides         = [],
		this.imageLoadCount = 0,
		this.animating      = false,
		this.currentLi,
		this.previousLi,
		this.nextLi,
		instances.push(this),
		this.init();
	}

	/**
	 * @name init
	 * @method
	 * @memberof FordCarousel
	 *
	 * @param {object} data - The data from the component service
	 *
	 * @return {void}
	 */
	FordCarousel.prototype.init = function instanceInit() {
		//console.log('http://www.ford.com/etc/designs/ford-brandsite-na/js/FordCarousel.prototype.init');
		var self = this, onImageEvent;

		this.$ul = this.$elm.find('> ul');
		this.$li = this.$ul.find('> li');
		this.$pagination = this.$elm.find('.pagination');

		_.each(this.$li, function(li, i) {
			var clone = $(li).clone();
			if(i == 0) {
				self.loadImageForSlide(clone, 'unwait');
			} else {
				self.loadImageForSlide(clone, 'showControls');
			}
			self.slides.push(clone);
			self.$li.remove();
		});


		this.count = this.slides.length;

		this.update();

		this.$ul.append(this.previousSlide());
		this.$ul.append(this.currentSlide());
		this.$ul.append(this.nextSlide());

		if (this.count > 1) {

			initFordCarousel();

			this.$elm
				.on('click', '.pagination .page', function onPaginationClick(event) {
					var $btn = $(this);

					//event.stopImmediatePropagation();

					if (!$btn.hasClass('current')) {
						self.goToSlide( Number($btn.text() - 1) );
					}
				})
				// Initialize controls
				.on('click', '.controls .previous,.controls .next', function onControlsClick(event) {
					var $btn = $(this);

					//event.stopImmediatePropagation();

					if ($btn.hasClass('next')) {
						self.next();
					}
					else if ($btn.hasClass('previous')) {
						self.previous();
					}
				})
				.on('swipe', function onSwipe(event) {
					var method = methodMap[event.gesture.direction];
					if (method) {
						event.gesture.preventDefault();
						self[method]();
					}
				})
            	.on('carousel.force-update', function(event){
					self.update();
					if(typeof(event.action) !== 'undefined' && event.action) {
						if(event.action === 'unwait' && self.$elm.hasClass('primary')) {
							$(window).trigger('unwait');
						} else if(event.action === 'showControls' && this.imageLoadCount == this.count) {
							//console.log("removing billboardWait");
							self.$elm.find('.billboardWait').removeClass('billboardWait');
						}
					}
				});

				try {
                    var hammertime = new Hammer(this.$elm[0]);
                    hammertime.on('panleft', function(ev) {
                        self.next();
                    });
                    hammertime.on('panright', function(ev){
                        self.previous();
                    });
                }
                catch(exception){
                    //console.log("Something went wrong");
                }

			setTimeout(function() { self.update(); }, 250);
		}
		//console.log('FordCarousel.prototype.init complete');
	};

	FordCarousel.prototype.previousSlide = function previousSlide() {

		return this.unstyleSlide( this.slides[ (this.index - 1).mod(this.count) ] );
	}

	FordCarousel.prototype.currentSlide = function currentSlide() {
		return this.unstyleSlide( this.slides[ this.index ] );
	}

	FordCarousel.prototype.nextSlide = function nextSlide() {
		return this.unstyleSlide( this.slides[ (this.index + 1).mod(this.count) ] );
	}

	FordCarousel.prototype.slideAt = function slideAt(i) {
		return this.unstyleSlide( this.slides[i] );
	}

	FordCarousel.prototype.unstyleSlide = function unstyleSlide($slide) {
		$slide.removeAttr('style');
		return $slide;
	}

	/**
	 * @name loadImageForSlide
	 * @method
	 * @memberof FordCarousel
	 *
 	 * @param  {$el} $li - The $slide to load the image for
	 *
	 * @returns {void}
	 *
	 * @description
	 * Loads the image for the $slide
	 */
	FordCarousel.prototype.loadImageForSlide = function loadImageForSlide($li, action) {
		var $img = $li.find('.picture img');

		if($img.attr('src') == null || $img.attr('src').length == 0) {
			$img[0].onload = function(e) {
				//console.log("carousel image loaded");
				this.loadImageCount++
                $(e.target).trigger({ type: 'carousel.force-update', action: action });
			}
			if(window.devicePixelRatio >= 1.2 ? true : false) {
				//console.log('carousel loading ' + $img.attr('data-high-retina'));
				$img.attr('src', $img.attr('data-high-retina'));
			} else {
				//console.log('carsouel loading ' + $img.attr('data-high-retina'));
				$img.attr('src', $img.attr('data-high'));
			}
		}
	}

	/**
	 * @name getLeft
	 * @method
	 * @memberof FordCarousel
	 *
	 * @returns {number} The left of the ul element
	 *
	 * @description
	 * Gets the left of the ul element, based on the current page and the width of the pages
	 */
	FordCarousel.prototype.getLeft = function getLeft() {
		var showIdx = 2;
        return (showIdx > 1)? ('-' + (this.$ul.find('li').filter(':first').find('img').width() * (showIdx - 1)) + 'px') : 0;
	};

	/**
	 * @name getHeight
	 * @method
	 * @memberof FordCarousel
	 *
	 * @returns {number} The height of the ul element
	 *
	 * @description
	 * Gets the height of the ul element, based on the maximum height of the <li> elements.
	 */
	FordCarousel.prototype.getHeight = function getHeight() {
		var height = 0;

		_.each(this.$ul.children('li'), function (li, i) {
			height = Math.max(height, $(li).find('img').height());
		});
		return height;
	};

	/**
	 * @name next
	 * @method
	 * @memberof FordCarousel
	 *
	 * @returns {void}
	 *
	 * @description
	 * Goes to the next page. If at the end, cycles to the beginning
	 */
	FordCarousel.prototype.next = function next(slideCount) {
		if(this.animating) { return; }

		var jump = false;
		this.animating = true;

		if(typeof(slideCount) === 'undefined') {
			slideCount = 1;
		} else {
			jump = true;
		}

		var self        = this,
			destination = (this.index + slideCount).mod(this.count),
			easing      = 'swing';

 		if(jump) {
			self.$ul.find('li').filter(':last').remove();
			self.$ul.append(self.slideAt(destination).clone());
		}

		this.index = (this.index + slideCount).mod(this.count);

		this.$ul.find('li').filter(':first').animate(
			{ width: 0 },
			//time,
			this.animationTime,
		    easing,
			function() {
				self.$ul.find('li').filter(':first').remove();
				self.$ul.append(self.nextSlide().clone());
				if(jump) {
					self.$ul.find('li').filter(':first').remove();
					self.$ul.prepend(self.previousSlide().clone());
				}
		  		self.animating = false;
				// 		if(typeof(recurseCount) !== 'undefined' && recurseCount > 0) {
				// 			self.next(--recurseCount, time)
				// 		} else {
				// 			self.updatePagination();
				// 		}
				self.updatePagination();
  			}
  		);
	};

	/**
	 * @name previous
	 * @method
	 * @memberof FordCarousel
	 *
	 * @returns {void}
	 *
	 * @description
	 * Goes to the previous page. If at the beginning, cycles to the end
	 */
	// FordCarousel.prototype.previous = function previous(recurseCount, time) {
	FordCarousel.prototype.previous = function previous(slideCount) {
		if(this.animating) { return; }

		var jump = false;
		this.animating = true;

		if(typeof(slideCount) === 'undefined') {
			slideCount = 1;
		} else {
			jump = true;
		}

		var $slide,
			self        = this,
			destination = (this.index - slideCount).mod(this.count),
			easing      = 'swing';

		if(jump) {
			self.$ul.find('li').filter(':first').remove();
			self.$ul.prepend(self.slideAt(destination).clone());
		}

		this.index = (this.index - slideCount).mod(this.count);

		$slide = this.previousSlide().css({ width: 0})

		this.$ul.prepend($slide.clone());

		this.$ul.find('li').filter(':first').animate(
			{ width: this.$ul.find('li').filter(':last').width() },
			//time,
			self.animationTime,
			easing,
			function() {
				self.$ul.find('li').filter(':last').remove();
				self.$ul.find('li').filter(':first').removeAttr('style');
				self.animating = false;
				if(jump) {
					self.$ul.find('li').filter(':last').remove();
					self.$ul.append(self.nextSlide().clone());
				}
				self.updatePagination();
			}
		);
	};

	FordCarousel.prototype.goToSlide = function goToSlide(index) {

		if(this.index === index) { return; }

		var numSlides = Math.abs(index - this.index),
			method    = index - this.index > 0 ? this.next : this.previous;

		method.call(this, numSlides);
	}

	FordCarousel.prototype.updatePagination = function updatePagination() {
		this.$pagination.find('.current').removeClass('current');
		this.$pagination.find('li:eq(' + (this.index) + ') .page').addClass('current');
	}

	/**
	 * @name onWindowResize
	 * @method
	 * @memberof FordCarousel
	 *
	 * @returns {void}
	 *
	 * @description
	 * Handles responsive resize
	 */
	FordCarousel.prototype.onWindowResize = function onWindowResize() {
		if(!this.animating) {
			this.update();
		}
	};

	/**
	 * @name update
	 * @method
	 * @memberof FordCarousel
	 *
	 * @returns {void}
	 *
	 * @description
	 * Updates the carousel's left / height.
	 */
	FordCarousel.prototype.update = function update() {
		//console.info('FordCarousel.update() left: ', this.getLeft());
		this.$ul.css({
			left: this.getLeft(),
			height: this.getHeight()
		});
	};

	/**
	 * @name destroy
	 * @method
	 * @memberof FordCarousel
	 *
	 * @returns {void}
	 *
	 * @description
	 * Unbinds event listners and destroys the elements
	 */
	FordCarousel.prototype.destroy = function destroy(dontRemoveInstance) {
		if (!dontRemoveInstance) {
			instances = _.reject(instances, this);
		}

		this.$ul = null;
		this.$li = null;
		this.$pagination = null;

		this.$elm
			.removeData('ford-carousel')
			.empty();
		this.$elm = null;
	};

	// Extend jQuery with fordCarousel
	$.fn.fordCarousel = function fordCarousel() {
		return this.each(function eachElm() {
			var $elm = $(this);
			$elm.data('ford-carousel', new FordCarousel($elm));
		});
	};

})(jQuery);

$(window).load(function() {
$('#getupdates-flip').on('click', '#submit-button', function(){ _satellite.track("get-updates-submit"); });
	$(window).on('unwait', function(e) { $('.wait').removeClass('wait'); });

	var imgSelector     = '.slide .picture img',
		$images         = $('.tiles .tile .content a img'),
		$showrooms      = $('section.Showroom'),
		$promotions     = $('section.Promotions'),
		$smartNextMoves = $('section.SmartNextSteps');

	if($('.ford-carousel').length > 0) {
		$('.ford-carousel').fordCarousel();
	} else if($('.Billboard').length > 0) {
		$images.push($('.Billboard .picture img')[0]);
	}


	if($showrooms.length > 0) {
		_.forEach($showrooms, function(el, i){
			_.forEach($(el).find(imgSelector), function(img, ii) {
				$images.push(img);
			});
		});
	}

	_.each($promotions, function(el, i){
		$images.push($(el).find(imgSelector)[0]);
	});

	_.each($smartNextMoves, function(el, i) {
		$images.push($(el).find(imgSelector)[0]);
	});

    var x = $images.length;

	_.forEach($images, function(img, n){
		var $img = $(img);
		if($img.attr('src') == null || $img.attr('src').length == 0) {
			img.onload = function(e) {
				//console.log("image loaded");
			}

            if(window.devicePixelRatio == "undefined")  {
				//console.log('loading ' + $img.attr('data-high'));
				$img.attr('src', $img.attr('data-high'));
			}else if(window.devicePixelRatio >= 1.2 ? true : false) {
				//console.log(' loading ' + $img.attr('data-high-retina'));
				$img.attr('src', $img.attr('data-high-retina'));
			} else {
				//console.log('loading ' + $img.attr('data-high-retina'));
				$img.attr('src', $img.attr('data-high'));
			}
		}
	});
});

(function (window) {
    var last = +new Date();
    var delay = 100; // default delay

    // Manage event queue
    var stack = [];

    function callback() {
        var now = +new Date();
        if (now - last > delay) {
            for (var i = 0; i < stack.length; i++) {
                stack[i]();
            }
            last = now;
        }
    }

    // Public interface
    var onDomChange = function (fn, newdelay) {
        if (newdelay) delay = newdelay;
        stack.push(fn);
    };

    // Naive approach for compatibility
    function naive() {

        var last = document.getElementsByTagName('*');
        var lastlen = last.length;
        var timer = setTimeout(function check() {

            // get current state of the document
            var current = document.getElementsByTagName('*');
            var len = current.length;

            // if the length is different
            // it's fairly obvious
            if (len != lastlen) {
                // just make sure the loop finishes early
                last = [];
            }

            // go check every element in order
            for (var i = 0; i < len; i++) {
                if (current[i] !== last[i]) {
                    callback();
                    last = current;
                    lastlen = len;
                    break;
                }
            }

            // over, and over, and over again
            setTimeout(check, delay);

        }, delay);
    }

    //
    //  Check for mutation events support
    //

    var support = {};

    var el = document.documentElement;
    var remain = 3;

    // callback for the tests
    function decide() {
        if (support.DOMNodeInserted) {
            window.addEventListener("DOMContentLoaded", function () {
                if (support.DOMSubtreeModified) { // for FF 3+, Chrome
                    el.addEventListener('DOMSubtreeModified', callback, false);
                } else { // for FF 2, Safari, Opera 9.6+
                    el.addEventListener('DOMNodeInserted', callback, false);
                    el.addEventListener('DOMNodeRemoved', callback, false);
                }
            }, false);
        } else if (document.onpropertychange) { // for IE 5.5+
            document.onpropertychange = callback;
        } else { // fallback
            naive();
        }
    }

    // checks a particular event
    function test(event) {
        el.addEventListener(event, function fn() {
            support[event] = true;
            el.removeEventListener(event, fn, false);
            if (--remain === 0) decide();
        }, false);
    }

    // attach test events
    if (window.addEventListener) {
        test('DOMSubtreeModified');
        test('DOMNodeInserted');
        test('DOMNodeRemoved');
    } else {
        decide();
    }

    // do the dummy test
    var dummy = document.createElement("div");
    el.appendChild(dummy);
    el.removeChild(dummy);

    // expose
    window.onDomChange = onDomChange;
})(window);
