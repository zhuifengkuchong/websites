(function($) {

	var AnalyticsService = function () {
	};
	
	AnalyticsService.prototype = {
	    track: function (action) {
	        if (typeof window._satellite !== 'undefined') {
	            window._satellite.track(action);
	        } else {
	            console.info('track', action);
	        }
	    }
	}
	
	$(document).ready(function () {
	
	    var as = new AnalyticsService();
	
	    // Billboard links to external websites
	    $('.Billboard a[href*=fmccmp]').on('click', function () {
	        as.track('hp-billboard-exit');
	    });
	    // Billboard left and right arrows
	    $('.Billboard span.icon-chevron-thin-left, span.icon-chevron-thin-right').on('click', function () {
	        as.track('hp-billboard');
	    });
	    // Billboard pagination icons
	    $('.Billboard .pagination span.page').on('click', function () {
	        as.track('hp-billboard');
	    });
	    // Showroom links to external websites
	    $('.Showroom a[href*=fmccmp]').on('click', function () {
	        as.track('hp-showroom-exit');
	    });
	    // Compare Vehicles expand button
	    $('.CompareVehicles span.icon-plus-hollow').on('click', function () {
	        as.track('hp-compare-vehicles');
	    });
	    // Promotions links to external websites
	    $('.Promotions a[href*=fmccmp]').on('click', function () {
	        as.track('hp-promotions-exit');
	    });
	    // Ford Brand Gallery links to external websites
	    $('.FordBrandGallery a[href*=fmccmp]').on('click', function () {
	        as.track('hp-ford-brand-gallery-exit');
	    });
	    // Smart Next Steps links to external websites
	    $('.SmartNextSteps a[href*=fmccmp]').on('click', function () {
	        as.track('hp-next-steps-exit');
	    });
	    // Find a Dealer Mini directions link
	    $('.FindDealerMini li.directions a').on('click', function () {
	        as.track('hp-find-dealer-exit-direction');
	    });
	    // Find a Dealer Mini dealer website links
	    $('.FindDealerMini .name').on('click', function () {
	        as.track('hp-find-dealer-exit-dealer-website');
	    });
	
	});

})(jQuery);

/* EOF */