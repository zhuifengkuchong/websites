var itemTimer;

jQuery(function($) {
    // Your code using failsafe $ alias here...

    $(".enterZip").live("click", function() {
        $(this).parent().siblings(".shipData").toggle();
        return false;
    });

    $("ul.sf-menu").supersubs({ 
            minWidth:    10,   // minimum width of sub-menus in em units 
            maxWidth:    30,   // maximum width of sub-menus in em units 
            extraWidth:  1     // extra width can ensure lines don't sometimes turn over 
        }).superfish({
        animation: { height: 'show' },
        speed: 300,
        autoArrows: false,
        dropShadows: false
    }).find("ul").bgIframe();

    $(".itemRow").equalHeights(true);
    $(".modCarousel .items").equalHeights(true);
    $(".columnRow").equalHeights(true);

    $(".itemWrapper ul.swatchList li a").live("mouseover", function() {
        $(this).parents(".swatchList").find("a").removeClass("active");
        $(this).addClass("active");
        var newImg = $(this).find(".swatchAlt").html();
        $(this).parents(".itemWrapper").find(".itemImage").attr({ src: newImg });
    });

    $('.altViews li a').live("click", function() {
        //the new image
        var newimg = $(this).attr("href");

        //update active status
        var altViews = $(".altViews li")
        altViews.each(function(i) {
            var children = altViews.eq(i).children("a");
            children.each(function(j) {
            if (newimg.replace('/large/', '').replace('/regular/', '') == children.eq(j).attr("href").replace('/large/', '').replace('/regular/', '')) {
                    children.eq(j).addClass("active");
                } else {
                    children.eq(j).removeClass("active");
                }
            });
        });

        //find the regular image
        $(".productImage img.mainImage").attr({ src: newimg.replace('/large/', '/regular/') });

        //find the large image
        $(".imageViewer .mainImage img").attr({ src: newimg.replace('/regular/', '/large/') });

        return false;
    });

    //Banner slide show
    //$("#homeSSW").cycle({
       // fx: 'fade',
        //speed: 300,
        //timeout: 5000,
       // pager: '.nav'
    //});
    $(".sswControls").css({ 'background-color': '#fff', 'opacity': '0.9' });

    $(".truncate").truncate({
        max_length: 400,
        more: "read more",
        less: "read less"
    });

    // dialog windows 
    $(".uiModal").dialog({
        autoOpen: false,
        width: "720px",
        bgiframe: true,
        modal: true,
        resizable: false,
        live: false,
        buttons: {
            Close: function() {
                $(this).dialog('close');
            }
        }
    });

    $(".uiDialog").dialog({
        autoOpen: false,
        width: "720px",
        bgiframe: true,
        modal: false,
        resizable: false,
        buttons: {
            Close: function() {
                $(this).dialog('close');
            }
        }
    });

    $(".pageTools .recent").live("click", function() {
        $(".recentDialog").dialog("open");
        return false;
    });

    $(".productImage .btnEnlarge").live("click", function() {
        $(".enlargeView").dialog("open");
        return false;
    });
    // END dialog windows 

    $(".tabModule .tabsRow").tabs();
    $(".tabModuleRss .tabsRow").tabs();
    $(".tabModuleRso .tabsRow").tabs();
    $(".cartSupport .supportTabs").tabs();

    // carousels 
    $(".modCarousel4 .scrollable").scrollable({
        size: 4,
        items: ".items",
        clickable: false
    });


    $(".modCarousel5 .scrollable").scrollable({
        size: 5,
        items: ".items",
        clickable: false
    });

    $(".scrollable").each(function() {
        var mcHeight = $(".itemWrapper:first", this).height();
        $(this).css({ 'height': mcHeight });
    });
    // END carousels 

    $(".swatchList a").cluetip({
        cluetipClass: 'swatch',
        width: '178px',
        positionBy: 'fixed',
        topOffset: '-122px',
        leftOffset: '-115px',
        local: true,
        dropShadow: false,
        showTitle: false
    });

    // Item Added to Cart Slider
    $("#item-dialog").mouseover(function() { clearTimeout(itemTimer); });
    $("#item-dialog").mouseout(function() { itemTimer = setTimeout(function() { hideItemSlider(); }, 1500); });
    // END Item Added
 

  $(".tooltip").tooltip({ 
	    effect: 'slide', 
       	    position: 'bottom right',
  	    relative:  	true
  }).dynamic( { 
        // customized configuration on bottom edge 
        bottom: { 
            direction: 'down',  
            bounce: true 
        },
        right: { 
            direction: 'right',  
            bounce: true 
        } 
    });
    
    
     $(".tooltipadmin").tooltip({ 
            position: 'bottom right',
      	    relative:  	true,
      	    track: true
      }).dynamic( { 
            // customized configuration on bottom edge 
            bottom: { 
                direction:'down',  
                bounce: true
            },
            right: { 
                direction: 'right',  
                bounce: true 
            } 
    });
    
  
    $(".mod_landing_gal li").matchHeights();
    
});
function showItemSlider() {
    clearTimeout(itemTimer);
    $("#item-dialog").slideDown(300);
    itemTimer = setTimeout(function() { hideItemSlider(); }, 5000);
}
function hideItemSlider() {
    $("#item-dialog").slideUp(150);
}

  $(window).load(function() {

            // CYCLE //

            var cCarouselObj;
            var jCounter;
            var jCurrentSlide = 0;
            var jSlideCount = 0;

            function onAfter(curr, next, opts) {
                jSlideCount = opts.slideCount;
                jCounter = 'Image ' + (opts.currSlide + 1) + ' of ' + jSlideCount;
                $('#jCounter').html(jCounter);
                jCurrentSlide = opts.currSlide;
                $('#navCycle').trigger('click');
            }

            function onBefore(curr, next, opts) {
                //		$('#navCycle').trigger('click');
            }

            function jAnchorBuilder(idx, slide) {               
//                var new_src = slide.src.substring(0, slide.src.length - 4);
//                new_src += "_thumb";
//                new_src += slide.src.substring(slide.src.length - 4, slide.src.length);
                return ('<li><img src="' + InnerSlideShowThumbImagesArray[idx] + '" width="90" height="46" style="cursor:pointer;" /></li>');
            }

            $('#slideshow').cycle({
                fx: 'fade',
                timeout: 0,
                speed: 300,
                delay: 1000,
                timeout: 2000,
                pause: 1,
                prev: '#prev',
                next: '#next',
                pager: '#navCycle',
                display_back_and_forward: true,
                before: onBefore,
                after: onAfter,
                pagerAnchorBuilder: jAnchorBuilder

            });

            // CAROUSEL //

            function mycarousel_initCallback(carousel) {

                cCarouselObj = carousel;

                $('#prev').bind('click', function() {
                    jCurrentSlide--;
                    //			if (jCurrentSlide<0) jCurrentSlide=jSlideCount-2;
                    //			cCarouselObj.scroll(jCurrentSlide);
                    //			cCarouselObj.prev();
                    $('#slideshow').cycle('pause');
                    return false;
                });

                $('#next').bind('click', function() {
                    jCurrentSlide++;
                    //			if (jCurrentSlide>jSlideCount-2) jCurrentSlide=0;
                    //			cCarouselObj.scroll(jCurrentSlide);
                    //			cCarouselObj.next();
                    $('#slideshow').cycle('pause');
                    return false;
                });

                // Disable autoscrolling if the user clicks the prev or next button.
                cCarouselObj.buttonNext.bind('click', function() {
                    cCarouselObj.startAuto(0);
                    $('#slideshow').cycle('pause');
                });

                cCarouselObj.buttonPrev.bind('click', function() {
                    cCarouselObj.startAuto(0);
                    $('#slideshow').cycle('pause');
                });

                // Pause autoscrolling if the user moves with the cursor over the clip.
                //		carousel.clip.hover(function() {
                //			cCarouselObj.stopAuto();
                //			$('#slideshow').cycle('pause');
                //			}, function() {
                //				cCarouselObj.startAuto();
                //		});

                carousel.clip.hover(function() {
                    cCarouselObj.startAuto(0);
                    $('#slideshow').cycle('pause');
                });

                $('#navCycle').bind('click', function() {
                    jCurrentSlide++;
                    //			if (jCurrentSlide>jSlideCount-2) jCurrentSlide=0;
                    cCarouselObj.scroll(jCurrentSlide - 2);
                    return false;
                });
            };

            $('#navCycle').jcarousel({
                scroll: 1,
                visible: 7,
                wrap: 'both',
                animation: 300,
                buttonPrevHTML: '<div id="prevJCarousel">&nbsp;</div>',
                buttonNextHTML: '<div id="nextJCarousel">&nbsp;</div>',
                buttonPrevEvent: 'click',
                buttonNextEvent: 'click',
                initCallback: mycarousel_initCallback
            });           

        });
		
        $(window).load(function() {
            // CYCLE //			
			  $('#sGallery').css('visibility', 'visible');
        });

      
$(document).ready(function () {

$('#leftnav a').each(function() {if ($(this).hasClass('active')) {$(this).next().show();$(this).parents(this).show();}});


$('#leftnav > li > a').click(function(){
     $('#leftnav li ul').slideUp();

       if ($(this).next().is(":visible")){
           $(this).next().slideUp('fast', function() {});
	       $(this).removeClass('active');
       } else {
	       $(this).next().slideToggle('fast', function() {});
	       $(this).addClass('active');
       }

  });



  $('#leftnav > li > ul > li > a').click(function(){

//      $('#leftnav li ul li ul').slideUp();

       if ($(this).next().is(":visible")){
           $(this).next().slideUp('fast', function() {});
	       $(this).removeClass('active');
       } else {
	       $(this).next().slideToggle('fast', function() {});
	       $(this).addClass('active');
       }

  });


$('#sitemapnav span').each(function() {if ($(this).hasClass('active')) {$(this).next().next().next().show();$(this).parents(this).show();}});


$('#sitemapnav > li > span').click(function(){
     $(this).find('ul:first').slideUp();

       if ($(this).next().next().next().is(":visible")){
           $(this).next().next().next().slideUp('fast', function() {});
	       $(this).removeClass('active');
       } else {
	       $(this).next().next().next().slideToggle('fast', function() {});
	       $(this).addClass('active');
       }

  });

$('#sitemapnav > li > ul > li > ul > li > span').click(function(){

     $(this).find('ul:first').slideUp();

       if ($(this).next().next().next().is(":visible")){
           $(this).next().next().next().slideUp('fast', function() {});
	       $(this).removeClass('active');
       } else {
	       $(this).next().next().next().slideToggle('fast', function() {});
	       $(this).addClass('active');
       }

  });

});

$(document).ready(function() {
//When you click on a link with class of poplight and the href starts with a # 
$('a.poplight[href^=#]').click(function() {
    var popID = $(this).attr('rel'); //Get Popup Name
    var popURL = $(this).attr('href'); //Get Popup href to define size
    
    //Pull Query & Variables from href URL
    var query= popURL.split('?');
    var dim= query[1].split('&');
    var popWidth = dim[0].split('=')[1]; //Gets the first query string value

    //Fade in the Popup and add close button
    $('#' + popID).fadeIn().css({ 'width': Number( popWidth ) }).prepend('<a href="#" class="close"><img src="../images/close_pop.gif"/*tpa=http://www.tenneco.com/cms/images/close_pop.gif*/ class="btn_close" title="Close Window" alt="Close" /></a>');

    //Define margin for center alignment (vertical   horizontal) - we add 80px to the height/width to accomodate for the padding  and border width defined in the css
    var popMargTop = ($('#' + popID).height() + 80) / 2;
    var popMargLeft = ($('#' + popID).width() + 80) / 2;

    //Apply Margin to Popup
    $('#' + popID).css({
        'margin-top' : -popMargTop,
        'margin-left' : -popMargLeft
    });

    //Fade in Background
    $('body').append('<div id="fade"></div>'); //Add the fade layer to bottom of the body tag.
    $('#fade').css({'filter' : 'alpha(opacity=40)'}).fadeIn(); //Fade in the fade layer - .css({'filter' : 'alpha(opacity=80)'}) is used to fix the IE Bug on fading transparencies 

    return false;
});

//Close Popups and Fade Layer
$('a.close, #fade').live('click', function() { //When clicking on the close or fade layer...
    $('#fade , .popup_block').fadeOut(function() {
        $('#fade, a.close').remove();  //fade them both out
    });
    return false;
});
});


jQuery(document).ready(function($) {

	$('a[rel*=facebox]').facebox({
		loadingImage : '../images/layout/loading.gif'/*tpa=http://www.tenneco.com/cms/images/layout/loading.gif*/,
		closeImage   : '../images/layout/closelabel.png.gif'/*tpa=http://www.tenneco.com/cms/images/layout/closelabel.png*/
	})

	// Initialize scrollable for multimedia page
	$(".mMP_ThumbWrap").scrollable({
		items: ".mMP_items",
		next: ".mMP_next",
		prev: ".mMP_prev",
		easing: "swing"
	});

})



// LOCATION MAP FUNCTIONS - JQUERY SHOW/HIDE STUFF

$(document).ready(function(){

	  $(".northaA").click(function(){
		$(".northaTxt").show();
		$(".southaTxt, .europeTxt, .africaTxt, .asiaTxt, .australiaTxt").hide();
	  });

	  $(".southaA").click(function(){
		$(".southaTxt").show();
		$(".northaTxt, .europeTxt, .africaTxt, .asiaTxt, .australiaTxt").hide();
	  });
	  
	  $(".europeA").click(function(){
		$(".europeTxt").show();
		$(".northaTxt, .southaTxt, .africaTxt, .asiaTxt, .australiaTxt").hide();
	  });
	  
	  $(".africaA").click(function(){
		$(".africaTxt").show();
		$(".northaTxt, .southaTxt, .europeTxt, .asiaTxt, .australiaTxt").hide();
	  });
	 
	  $(".asiaA").click(function(){
		$(".asiaTxt").show();
		$(".northaTxt, .southaTxt, .europeTxt, .africaTxt, .australiaTxt").hide();
	  });
	  
	  $(".australiaA").click(function(){
		$(".australiaTxt").show();
		$(".northaTxt, .southaTxt, .europeTxt, .africaTxt, .asiaTxt").hide();
	  });

});