// /assets/js/page-specific/homepage/embeddedGeoContentController.js
// Set up URL test parameters
var genericCRIS1_param = location.search.indexOf('geoContent=cris1_States');
var genericCRIS2_param = location.search.indexOf('geoContent=cris2_States');
var genericENS1_param = location.search.indexOf('geoContent=ens1_States');
var genericENS2_param = location.search.indexOf('geoContent=ens2_States');
var prism_paramA = location.search.indexOf('geoContent=prismOfferENS1'); //ENS prismOfferA
var prism_paramB = location.search.indexOf('geoContent=prismOfferENS2'); //ENS prismOfferB
var prism_paramC = location.search.indexOf('geoContent=prismOfferCRIS1'); //CRIS prismOfferA
var geo_showDefault = location.search.indexOf('geoContent=showDefaultBanners');
var geoStatus = 'enable';

var ttSeePrismTVURL = ctl_url + '/prismtv/';

if (
	(genericCRIS1_param != -1) || 
	(genericCRIS2_param != -1) || 
	(genericENS1_param != -1) ||
	(genericENS2_param != -1) ||
	(prism_paramA != -1) ||
	(prism_paramB != -1) ||
	(prism_paramC != -1) ||
	(geo_showDefault != -1))
{
	geoStatus = 'disable';
}

//Update body content other than banners for Prism
function geoRouter_PrismBodyContent() {
	$('div.btnLeftPrism-selectDTV').click(function() {
		qa.trackAction('ctl|corp|hp|tv|toggle|directv'); //This sends DTV clickTrack when DTV button is clicked while Prism content displays
		setPlacardTvDtvContent_onToggle();
	});
	$('div.btnRightDTV-selectPrism').click(function() {
		qa.trackAction('ctl|corp|hp|tv|toggle|prism'); //This sends DPrism clickTrack when Prism TV button is clicked while DTV content displays
		setPlacardTvPrismContent_onToggle();
	});

	if (
		(prism_paramA != -1) ||
		(prism_paramB != -1) ||
		(prism_paramC != -1) ||
		(((ttGeoLoc.prismDMAGroup == 'prismENS1') || (ttGeoLoc.prismDMAGroup == 'prismENS2') || (ttGeoLoc.prismDMAGroup == 'prismCRIS1')) && (geoStatus != 'disable'))
	)
	{
		// Update TV body elements to Prism TV content
		document.getElementById('placardBundle').className += " prismTV"; // Display Prism in scrolling section Bundles element
		setPlacardTvPrismContent_onLoad();
		appendPrismFooterCopyright();
		updatePrismExtLinkFooter();
	}
	else {
		setPlacardTvDtvContent_onLoad();
	}
}

function setPlacardTvPrismContent_onLoad() {
	setPlacardTvPrismContent_classicView();
	//$().ready(function(){
	//	update_placardTVSWFPlayer();
	//});
}

function setPlacardTvPrismContent_onToggle() {
	$('div.btnRightDTV-selectPrism').css('cursor','wait');
	setPlacardTvPrismContent_toggleView();
	// update_placardTVSWFPlayer();
	$('div.btnRightDTV-selectPrism').css('cursor','pointer');
}


function setPlacardTvPrismContent_classicView() {
	document.getElementById('placardTV').className += " prismTV"; // Display Prism in scrolling section TV element
	$('div#prismVideo').show();
	$('div#tvContainer img#dtvImage').hide();
	$('div.prismContent div#prism-toggle-view-heading-wrapper').hide();
	$('div.prismContent div#prism-classic-view-heading-wrapper').show();
	$('div#btnGetPrismTV-wrapper div.prism-classic-view').show();
	$('div#btnGetPrismTV-wrapper div.prism-Toggle-view').hide();
	$('div#placardTV div#prismVideo-disclaimer').hide();
	$('div#tvContainer div#prismVideo').css('margin-top','0px');
}

function setPlacardTvPrismContent_toggleView() {
	document.getElementById('placardTV').className += " prismTV"; // Display Prism in scrolling section TV element
	$('div.prismContent div#prism-toggle-view-heading-wrapper').show();
	$('div.prismContent div#prism-classic-view-heading-wrapper').hide();
	$('div#btnGetPrismTV-wrapper div.prism-classic-view').hide();
	$('div#btnGetPrismTV-wrapper div.prism-Toggle-view').show();
	$('div#prismVideo').show();
	$('div#tvContainer img#dtvImage').hide();
	$('div#placardTV div#prismVideo-disclaimer').show();
}

function setPlacardTvDtvContent_onLoad() {
	setPlacardTvDtvContent_common();
}

function setPlacardTvDtvContent_onToggle() {
	setPlacardTvDtvContent_common();
	// update_placardTVSWFPlayer();
}

function setPlacardTvDtvContent_common() {
	$('div#placardTV').removeClass('prismTV'); // Display DTV in scrolling section TV element
	$('div#prismVideo').hide();
	$('div#tvContainer img#dtvImage').show();
	$('div#placardTV div#prismVideo-disclaimer').hide();
}

function appendPrismFooterCopyright() {
	//Add Prism footer copyright declarations after CenturyLink copyright declaration
	var geoPrismFooterAddons = '';
	geoPrismFooterAddons += '<div class="footer">';
	geoPrismFooterAddons += 'The Heat: &copy; 2014 Twentieth Century Fox Film Corporation. All rights reserved. Now available on Prism On Demand. Prism On Demand charges apply.';
	geoPrismFooterAddons += '</div>';

	$().ready(function(){
		$('#tpl_footer').after(geoPrismFooterAddons);
	});
}

function update_placardTVSWFPlayer() {
	//Body Copy TV tab swap
	var swfObject = new SWFObject('../../../../static/Common/Includes/Lib/player.swf'/*tpa=http://www.centurylink.com/static/Common/Includes/Lib/player.swf*/, 'PrismPlayer', '520', '320', '9', '#000000');
	swfObject.addParam('allowfullscreen', 'true');
	swfObject.addParam('allowscriptaccess', 'always');
	swfObject.addParam('wmode', 'opaque');
	swfObject.addVariable('image', '../../../images/page-components/prism_staticimage.png'/*tpa=http://www.centurylink.com/assets/images/page-components/prism_staticimage.png*/);
	swfObject.addVariable('provider', 'rtmp');
	swfObject.addVariable('streamer', 'rtmp://cp108058.edgefcs.net/ondemand/');
	swfObject.addVariable('file', 'http://www.centurylink.com/assets/js/page-specific/homepage/flash/sptv-demos-2014/sptv-demo-overview.mp4');
	//swfObject.write('tvContainer');
	swfObject.write('prismVideo');
}

function updatePrismExtLinkFooter() {
	$().ready(function(){
		document.getElementById('tpl_ext-footer-tv-services').href=ttSeePrismTVURL;
		document.getElementById('tpl_ext-footer-tv-services').setAttribute('clicktrack','ctl|corp|hp|quick_links|info3|link|prismtv');
	});
}



/*
 * Begin controller for wide banner block;  this is now the default, and code for previous banner block is removed.
 */


function geoRouter_banner0()
{
	if (ttGeoLoc.custGroup == "ENS")
	{
		$('#hp-banner-june').remove();
	}
}

/*
 * Begin controller for banner position 1 (id="hp-banner-hsi")
 */

function geoRouter_banner1_B()
{
	//evaluate test parameter conditions
	if (geoStatus == 'disable')
	{
		if (genericCRIS1_param != -1 || prism_paramC != -1)
		{
			runtimeBanner1_genericCRIS_1();
		}
		else if (genericCRIS2_param != -1)
		{
			runtimeBanner1_genericCRIS_2();
		}
		else if ((genericENS1_param != -1) || (genericENS2_param != -1))
		{
			runtimeBanner1_genericENS();
		}
	}
	//evaluate live geo-targeting conditions
	else if(geoStatus == 'enable') {
		if (ttGeoLoc.targetStatesSubgroup == "crisGroup1") {runtimeBanner1_genericCRIS_1();}
		else if (ttGeoLoc.targetStatesSubgroup == "crisGroup2") {runtimeBanner1_genericCRIS_2();}
		else if (ttGeoLoc.custGroup == "ENS") {runtimeBanner1_genericENS();}
	}
}

function runtimeBanner1_genericCRIS_1()
{
	//Image setup- id=homepageBanner1_img
	var init_genericCRISBanner1_img_src = '../../../images/banners/tgt/959x313_homepage_banner1_cris1-generic.png'/*tpa=http://www.centurylink.com/assets/images/banners/tgt/959x313_homepage_banner1_cris1-generic.png*/;

	//Learn more setup - id=homepageBanner1_area1
	var init_genericCRISBanner1_area1_evar = 'ctl|corp|hp|main_promo|position1|cris|button';
	
	//img

	$("#homepageBanner1_img-B").attr("src", init_genericCRISBanner1_img_src);
	
	//area1
	$("#homepageBanner1_area1-B").attr("clicktrack", init_genericCRISBanner1_area1_evar);
	
	//area2
	document.getElementById('homepageBanner1_area2-B').href = 'http://www.centurylink.com/common/offer_details/pos1cris1.html';
}

function runtimeBanner1_genericCRIS_2()
{
	//Learn more setup - id=homepageBanner1_area1

	//img
	$("#homepageBanner1_img-B").attr(
		{
			"src": '../../../images/banners/tgt/959x313_homepage_banner1_cris2-generic.png'/*tpa=http://www.centurylink.com/assets/images/banners/tgt/959x313_homepage_banner1_cris2-generic.png*/
		}
	);

	//image map area1
	$("#homepageBanner1_area1-B").attr(
		{
			"clicktrack": 'ctl|corp|hp|main_promo|position1|cris|button'
		}
	);

	//image map area2
	document.getElementById('homepageBanner1_area2-B').href = 'http://www.centurylink.com/common/offer_details/pos1cris2.html';
}


function runtimeBanner1_genericENS() {
	//Image setup- id=homepageBanner1_img
	var init_genericENSBanner1_img_src = '../../../images/banners/tgt/959x313_homepage_banner1_ens.png'/*tpa=http://www.centurylink.com/assets/images/banners/tgt/959x313_homepage_banner1_ens.png*/;

	//Learn more setup - id=homepageBanner1_area1 - learn more
	var init_genericENSBanner1_area1_evar = 'ctl|corp|hp|main_promo|position1|ens|button';

	//Disclaimer setup - id=homepageBanner1_area2 - disclaimer

	//img
	$("#homepageBanner1_img-B").attr("src", init_genericENSBanner1_img_src);
	
	//area1
	$("#homepageBanner1_area1-B").attr("clicktrack", init_genericENSBanner1_area1_evar);

	//area2
}

// End banner1 controller #############################################################################################






/*
 *#############################################################################################
 * Begin controller for banner position 2 (id="hp-banner-bundle")
 *#############################################################################################
 */

function geoRouter_banner2_B() {
	//evaluate test parameter conditions
	if (geoStatus == 'disable') {
		if (prism_paramA != -1) {runtimeBanner2_prismOfferA();}
		else if (prism_paramB != -1) {runtimeBanner2_prismOfferA();}
		else if (prism_paramC != -1) {runtimeBanner2_prismOfferC();}
		else if (genericCRIS1_param != -1) {runtimeBanner2_genericCRIS_1();}
		else if (genericCRIS2_param != -1) {runtimeBanner2_genericCRIS_2();}
		else if ((genericENS1_param != -1) || (genericENS2_param != -1)) {runtimeBanner2_genericENS();}
	}
	//evaluate live geo-targeting conditions
	else if(geoStatus == 'enable') {
		if (ttGeoLoc.prismDMAGroup == 'prismENS1') {runtimeBanner2_prismOfferA();}
		else if (ttGeoLoc.prismDMAGroup == 'prismENS2') {runtimeBanner2_prismOfferA();}
		else if (ttGeoLoc.prismDMAGroup == 'prismCRIS1') {runtimeBanner2_prismOfferC();}
		else if (ttGeoLoc.targetStatesSubgroup == 'crisGroup1') {runtimeBanner2_genericCRIS_1();}
		else if (ttGeoLoc.targetStatesSubgroup == 'crisGroup2') {runtimeBanner2_genericCRIS_2();}
		else if (ttGeoLoc.custGroup == "ENS") {runtimeBanner2_genericENS();}
	}
}

function runtimeBanner2_genericCRIS_1() {
	//Image setup- id=homepageBanner2_img

	//Learn more setup - id=homepageBanner2_area1 - learn more
	var init_genericCRISBanner2_area1_evar = 'ctl|corp|hp|main_promo|position2|cris|button';

	//Disclaimer setup - id=homepageBanner2_area2 - disclaimer

	//img
	
	//area1
	$("#homepageBanner2_area1-B").attr("clicktrack", init_genericCRISBanner2_area1_evar);
	
	//area2
	//document.getElementById('homepageBanner2_area2-B').href = 'http://www.centurylink.com/common/offer_details/pos2default.html';

}


function runtimeBanner2_genericCRIS_2() {
	//Image setup- id=homepageBanner2_img

	//Learn more setup - id=homepageBanner2_area1 - learn more
	var init_genericCRISBanner2_area1_evar = 'ctl|corp|hp|main_promo|position2|cris|button';

	//Disclaimer setup - id=homepageBanner2_area2 - disclaimer

	//img
	
	//area1
	$("#homepageBanner2_area1-B").attr("clicktrack", init_genericCRISBanner2_area1_evar);
	
	//area2
	//document.getElementById('homepageBanner2_area2-B').href = 'http://www.centurylink.com/common/offer_details/pos2default.html';
}


function runtimeBanner2_genericENS() {
	//Image setup- id=homepageBanner2_img

	//Learn more setup - id=homepageBanner2_area1 - learn more
	var init_genericENSBanner2_area1_evar = 'ctl|corp|hp|main_promo|position2|ens|button';

	//Disclaimer setup - id=homepageBanner2_area2 - disclaimer

	//img
	
	//area1
	$("#homepageBanner2_area1-B").attr("clicktrack", init_genericENSBanner2_area1_evar);

	//area2
}

function runtimeBanner2_prismOfferA() {
	//Image setup- id=homepageBanner2_img
	var init_prismBanner2_A_img_src = '../../../images/banners/tgt/959x313_homepage_banner2_prism_base.png'/*tpa=http://www.centurylink.com/assets/images/banners/tgt/959x313_homepage_banner2_prism_base.png*/;
	var init_prismBanner2_A_img_alt = 'Get a $100 Prepaid Card when you bundle CenturyLink&reg; Prism&trade; TV with a qualifying CenturyLink service.';

		//Learn more setup - id=homepageBanner2_area1
	var init_prismBanner2_A_area1_href = ttSeePrismTVURL;
	var init_prismBanner2_A_area1_evar = 'ctl|corp|hp|main_promo|position2|ens|prismbutton';
	var init_prismBanner2_A_area1_coords = '65,235,222,272';

	//Disclaimer setup - id=homepageBanner2_area2
	var init_prismBanner2_A_area2_coords = '851,294,920,308';
	var init_prismBanner2_A_area2_href = 'http://www.centurylink.com/assets/js/page-specific/homepage/common/offer_details/pos2defaultprism.html';

	//img
	$("#homepageBanner2_img-B").attr("src", init_prismBanner2_A_img_src).attr("alt", init_prismBanner2_A_img_alt);
	
	//area1
	$("#homepageBanner2_area1-B").attr(
		{
			"clicktrack": init_prismBanner2_A_area1_evar,
			"href": init_prismBanner2_A_area1_href,
			"coords": init_prismBanner2_A_area1_coords
		}
	);
	
	//area2
	$("#homepageBanner2_area2-B").attr("coords",init_prismBanner2_A_area2_coords).attr("href",init_prismBanner2_A_area2_href);
}

function runtimeBanner2_prismOfferC() {
	//Image setup- id=homepageBanner2_img
	var init_prismBanner2_C_img_src = '../../../images/banners/tgt/959x313_homepage_banner2_prism_base.png'/*tpa=http://www.centurylink.com/assets/images/banners/tgt/959x313_homepage_banner2_prism_base.png*/;
	var init_prismBanner2_C_img_alt = 'Get a $100 Prepaid Card when you bundle CenturyLink&reg; Prism&trade; TV with a qualifying CenturyLink service.';

	//Learn more setup - id=homepageBanner2_area1
	var init_prismBanner2_C_area1_href = ttSeePrismTVURL;
	var init_prismBanner2_C_area1_evar = 'ctl|corp|hp|main_promo|position2|cris|prismbutton';
	var init_prismBanner2_C_area1_coords = '65,235,222,272';

	//Disclaimer setup - id=homepageBanner2_area2
	var init_prismBanner2_C_area2_coords = '851,294,920,308';
	var init_prismBanner2_C_area2_href = 'http://www.centurylink.com/assets/js/page-specific/homepage/common/offer_details/pos2defaultprism.html';

	//img
	$("#homepageBanner2_img-B").attr("src", init_prismBanner2_C_img_src).attr("alt", init_prismBanner2_C_img_alt);
	
	//area1
	$("#homepageBanner2_area1-B").attr(
		{
			"clicktrack": init_prismBanner2_C_area1_evar,
			"href": init_prismBanner2_C_area1_href,
			"coords": init_prismBanner2_C_area1_coords
		}
	);
	
	//area2
	$("#homepageBanner2_area2-B").attr("coords", init_prismBanner2_C_area2_coords).attr("href", init_prismBanner2_C_area2_href);
}



// End banner2 controller #############################################################################################





/*
 * Begin controller for banner position 3 (id="hp-banner-internetonly")
 */

function geoRouter_banner3_B()
{
	// evaluate test parameter conditions
	if (geoStatus == 'disable')
	{
		if (genericCRIS1_param != -1 || genericCRIS2_param != -1 || prism_paramC != -1)
		{
			runtimeBanner3_genericCRIS();
		}
		else if (genericENS1_param != -1 || genericENS2_param != -1)
		{
			runtimeBanner3_genericENS();
		}
	}
	// evaluate live geo-targeting conditions
	else if (geoStatus == 'enable')
	{
		if (ttGeoLoc.custGroup == "CRIS")
		{
			runtimeBanner3_genericCRIS();
		}
		else if (ttGeoLoc.custGroup == "ENS")
		{
			runtimeBanner3_genericENS();
		}
	}
}

function runtimeBanner3_genericCRIS()
{
	//Image setup- id=homepageBanner3_img
	var init_genericCRISBanner3_img_src = '../../../images/banners/tgt/959x313_homepage_banner3_cris-generic.png'/*tpa=http://www.centurylink.com/assets/images/banners/tgt/959x313_homepage_banner3_cris-generic.png*/;
	var init_genericCRISBanner3_img_alt = 'CenturyLink High-Speed Internet. No Phone Line Required. $29.95 a month ' +
		'for 12 months. Speeds up to 40 Mbps. Autopay enrollment required.';

	//Learn more setup - id=homepageBanner3_area1 - learn more
	var init_genericCRISBanner3_area1_evar = 'ctl|corp|hp|main_promo|position3|cris|button';
	var init_genericCRISBanner3_area1_coords = '250,128,366,161';

	//Disclaimer setup - id=homepageBanner3_area2 - disclaimer
	var init_genericCRISBanner3_area2_coords = '866,283,952,302';

	//img
	$("#homepageBanner3_img-B").attr("src", init_genericCRISBanner3_img_src).attr("alt", init_genericCRISBanner3_img_alt);
	
	//area1
	$("#homepageBanner3_area1-B").attr("clicktrack", init_genericCRISBanner3_area1_evar).attr("coords", init_genericCRISBanner3_area1_coords);
	
	//area2
	$("#homepageBanner3_area2-B").attr("coords", init_genericCRISBanner3_area2_coords);
}

function runtimeBanner3_genericENS()
{
	//Image setup- id=homepageBanner3_img
	var init_genericENSBanner3_img_src = '../../../images/banners/tgt/959x313_homepage_banner3_ens-generic.png'/*tpa=http://www.centurylink.com/assets/images/banners/tgt/959x313_homepage_banner3_ens-generic.png*/;
	var init_genericENSBanner3_img_alt = 'CenturyLink High-Speed Internet. No Phone Line Required. $34.95 a month for 12 months. Speeds up to 10 Mbps. Autopay enrollment required.';

	//Learn more setup - id=homepageBanner3_area1 - learn more
	var init_genericENSBanner3_area1_evar = 'ctl|corp|hp|main_promo|position3|ens|button';
	var init_genericENSBanner3_area1_coords = '251,126,367,160';

	//Disclaimer setup - id=homepageBanner3_area2 - disclaimer
	var init_genericENSBanner3_area2_coords = '874,282,951,303';


	//img
	$("#homepageBanner3_img-B").attr("src", init_genericENSBanner3_img_src).attr("alt", init_genericENSBanner3_img_alt);
	
	//area1
	$("#homepageBanner3_area1-B").attr("clicktrack", init_genericENSBanner3_area1_evar).attr("coords", init_genericENSBanner3_area1_coords);
	
	//area2
	$("#homepageBanner3_area2-B").attr("coords", init_genericENSBanner3_area2_coords);

	
	
}

// End banner3 controller #############################################################################################


/*
 * Begin controller for banner position 4 (id="hp-banner-tv")
 */

function geoRouter_banner4_B() {
	//evaluate test parameter conditions
	if (geoStatus == 'disable') {
		if (prism_paramA != -1) {runtimeBanner4_prismOfferA();}
		else if (prism_paramB != -1) {runtimeBanner4_prismOfferA();}
		else if (prism_paramC != -1) {runtimeBanner4_prismOfferC();}
		else if (genericCRIS1_param != -1) {runtimeBanner4_genericCRIS();}
		else if (genericCRIS2_param != -1) {runtimeBanner4_genericCRIS();}
		else if (genericENS1_param != -1) {runtimeBanner4_ens_TvOfferA();}
		else if (genericENS2_param != -1) {runtimeBanner4_ens_TvOfferB();}
	}
	//evaluate live geo-targeting conditions
	else if(geoStatus == 'enable') {
		if (ttGeoLoc.prismDMAGroup == 'prismENS1') {runtimeBanner4_prismOfferA();}
		else if (ttGeoLoc.prismDMAGroup == 'prismENS2') {runtimeBanner4_prismOfferA();} 
		else if (ttGeoLoc.prismDMAGroup == 'prismCRIS1') {runtimeBanner4_prismOfferC();}
		else if (ttGeoLoc.targetStatesSubgroup == 'crisGroup1')  { runtimeBanner4_genericCRIS();}
		else if (ttGeoLoc.targetStatesSubgroup == 'crisGroup2')  {runtimeBanner4_genericCRIS(); }
		else if (ttGeoLoc.targetStatesSubgroup == 'ensGroup1')  {runtimeBanner4_ens_TvOfferA();}
		else if (ttGeoLoc.targetStatesSubgroup == 'ensGroup2')  {runtimeBanner4_ens_TvOfferB();}
	}
}

function runtimeBanner4_genericCRIS() {
	//Image setup- id=homepageBanner4_img

	//Learn more setup - id=homepageBanner4_area1 - learn more
	var init_genericCRISBanner4_area1_evar = 'ctl|corp|hp|main_promo|position4|cris|button';

	//Disclaimer setup - id=homepageBanner4_area2 - disclaimer

	//area1
	$("#homepageBanner4_area1-B").attr("clicktrack", init_genericCRISBanner4_area1_evar);

	//area2
}

function runtimeBanner4_ens_TvOfferA() {
	//Image setup- id=homepageBanner4_img
	var init_ens1_banner4_A_img_src = '../../../images/banners/tgt/959x313_homepage_banner4_ens_offerA.png'/*tpa=http://www.centurylink.com/assets/images/banners/tgt/959x313_homepage_banner4_ens_offerA.png*/;
	var init_ens1_banner4_A_img_alt = 'Online only! Get a $150 Prepaid Card when you order online. Internet + Phone ' +
		'+ TV for $104.95 a month for 12 months when you bundle.';

	//Learn more setup - id=homepageBanner4_area1
	var init_ens1_banner4_A_area1_href = 'http://www.centurylink.com/home/specialoffers/';
	var init_ens1_banner4_A_area1_evar = 'ctl|corp|hp|main_promo|position4|ens|button';
	var init_ens1_banner4_A_area1_coords = '63,222,173,254';

	//img
	$("#homepageBanner4_img-B").attr(
		{
			"src": init_ens1_banner4_A_img_src,
			"alt": init_ens1_banner4_A_img_alt
		}
	);
	
	//area1
	$("#homepageBanner4_area1-B").attr(
		{
			"href": init_ens1_banner4_A_area1_href,
			"clicktrack": init_ens1_banner4_A_area1_evar,
			"coords": init_ens1_banner4_A_area1_coords
		}
	);

	// area2 disclaimer
	$("#homepageBanner4_area2-B").attr('coords', '890,283,958,294');
}

function runtimeBanner4_ens_TvOfferB() {
	//Image setup- id=homepageBanner4_img
	var init_ens1_banner4_A_img_src = '../../../images/banners/tgt/959x313_homepage_banner4_ens_offerB.png'/*tpa=http://www.centurylink.com/assets/images/banners/tgt/959x313_homepage_banner4_ens_offerB.png*/;
	var init_ens1_banner4_A_img_alt = 'Online only! Get a $150 Prepaid Card when you order online. Internet + Phone ' +
		'+ TV for $94.95 a month for 12 months when you bundle.';

	//img
	$("#homepageBanner4_img-B").attr(
		{
			"src": init_ens1_banner4_A_img_src,
			"alt": init_ens1_banner4_A_img_alt
		}
	);

	// Learn more setup - id=homepageBanner4_area1
	$("#homepageBanner4_area1-B").attr(
		{
			"href": '/home/specialoffers/',
			"clicktrack": 'ctl|corp|hp|main_promo|position4|ens|button',
			"coords": '63,222,173,254'
		}
	);
	
	// area2 disclaimer
	$("#homepageBanner4_area2-B").attr('coords', '890,283,958,294');
}

function runtimeBanner4_prismOfferA() {
	//Image setup- id=homepageBanner4_img
	var init_prismBanner4_A_img_src = '../../../images/banners/tgt/959x313_homepage_banner4_prism_offerA.png'/*tpa=http://www.centurylink.com/assets/images/banners/tgt/959x313_homepage_banner4_prism_offerA.png*/;
	var init_prismBanner4_A_img_alt = 'Online only! Get a $150 Prepaid Card when you order CenturyLink&reg; ' +
		'Prism&trade; TV + Internet + Phone. $89.95 with Complete&trade; Lite when you bundle.';

	//Learn more setup - id=homepageBanner4_area1
	var init_prismBanner4_A_area1_href = ttSeePrismTVURL + '#prism-tv-plans-and-prices.html';
	var init_prismBanner4_A_area1_evar = 'ctl|corp|hp|main_promo|position4|ens|prismbutton';
	var init_prismBanner4_A_area1_coords = '58,225,168,257';

	//img
	$("#homepageBanner4_img-B").attr(
		{
			"src": init_prismBanner4_A_img_src,
			"alt": init_prismBanner4_A_img_alt
		}
	);
	
	//area1
	$("#homepageBanner4_area1-B").attr(
		{
			"href": init_prismBanner4_A_area1_href,
			"clicktrack": init_prismBanner4_A_area1_evar,
			"coords": init_prismBanner4_A_area1_coords
		}
	);
	
	//area2
	$("#homepageBanner4_area2-B").remove();
	$('#homepageBanner4_area2-prismOffer').show();
}

function runtimeBanner4_prismOfferC() {
	//Image setup- id=homepageBanner4_img
	var init_prismBanner4_C_img_src = '../../../images/banners/tgt/959x313_homepage_banner4_prism_offerA.png'/*tpa=http://www.centurylink.com/assets/images/banners/tgt/959x313_homepage_banner4_prism_offerA.png*/;
	var init_prismBanner4_C_img_alt = 'Online only! Get a $150 Prepaid Card when you order CenturyLink&reg;' +
		' Prism&trade; TV + Internet + Phone. $89.95 with Complete&trade; Lite when you bundle.';

	//Learn more setup - id=homepageBanner4_area1
	var init_prismBanner4_C_area1_href = ttSeePrismTVURL + '#prism-tv-plans-and-prices.html';
	var init_prismBanner4_C_area1_evar = 'ctl|corp|hp|main_promo|position4|cris|prismbutton';
	var init_prismBanner4_C_area1_coords = '58,225,168,257';

	//img
	$("#homepageBanner4_img-B").attr(
		{
			"src": init_prismBanner4_C_img_src,
			"alt": init_prismBanner4_C_img_alt
		}
	);
	
	//area1
	$("#homepageBanner4_area1-B").attr(
		{
			"href": init_prismBanner4_C_area1_href,
			"clicktrack": init_prismBanner4_C_area1_evar,
			"coords": init_prismBanner4_C_area1_coords
		}
	);
	
	//area2
	$("#homepageBanner4_area2-B").remove();
	$('#homepageBanner4_area2-prismOffer').show();
}


// End banner4 controller #############################################################################################
