(function ($) {

  /**
   * The recommended way for producing HTML markup through JavaScript is to write
   * theming functions. These are similiar to the theming functions that you might
   * know from 'phptemplate' (the default PHP templating engine used by most
   * Drupal themes including Omega). JavaScript theme functions accept arguments
   * and can be overriden by sub-themes.
   *
   * In most cases, there is no good reason to NOT wrap your markup producing
   * JavaScript in a theme function.
   */
  Drupal.theme.prototype.wyndhamExampleButton = function (path, title) {
    // Create an anchor element with jQuery.
    return $('<a href="' + path + '" title="' + title + '">' + title + '</a>');
  };

  /**
   * Behaviors are Drupal's way of applying JavaScript to a page. The advantage
   * of behaviors over simIn short, the advantage of Behaviors over a simple
   * document.ready() lies in how it interacts with content loaded through Ajax.
   * Opposed to the 'document.ready()' event which is only fired once when the
   * page is initially loaded, behaviors get re-executed whenever something is
   * added to the page through Ajax.
   *
   * You can attach as many behaviors as you wish. In fact, instead of overloading
   * a single behavior with multiple, completely unrelated tasks you should create
   * a separate behavior for every separate task.
   *
   * In most cases, there is no good reason to NOT wrap your JavaScript code in a
   * behavior.
   *
   * @param context
   *   The context for which the behavior is being executed. This is either the
   *   full page or a piece of HTML that was just added through Ajax.
   * @param settings
   *   An array of settings (added through drupal_add_js()). Instead of accessing
   *   Drupal.settings directly you should use this because of potential
   *   modifications made by the Ajax callback that also produced 'context'.
   */
  Drupal.behaviors.wyndhamExampleBehavior = {
    attach: function (context, settings) {

      // By using the 'context' variable we make sure that our code only runs on
      // the relevant HTML. Furthermore, by using jQuery.once() we make sure that
      // we don't run the same piece of code for an HTML snippet that we already
      // processed previously. By using .once('foo') all processed elements will
      // get tagged with a 'foo-processed' class, causing all future invocations
      // of this behavior to ignore them.
        

    //popover with close box for mobile
    var isVisible = false;
    var clickedAway = false;

    $('.pophover').popover({
	    container: '.gridly',
	    placement: 'auto top',
        html: true,
        trigger: 'hover'
    }).hover(function(e) {
        $('.popover-title').children('.close').remove();
	    $('.popover-title').append('<button type="button" class="close">&times;</button>');
        
        $('.close').click(function(e){
            $('.pophover').popover('hide');
        });
        e.preventDefault();
    });
  

   // Disable link click not scroll top
    $("a[href='#']").not('#booking-toggle').click(function() {
        return false
    });
        // SLIDERS


        $(".front #slides").slidesjs({
            width: 1141,
            height: 350,
            play: {
                active: false,
                interval: 6000,
                auto: true,
                swap: true,
                pauseOnHover: false,
                restartDelay: 2500
            },
            navigation: {
                active: false
            },
            effect: {
                slide: {
                    speed: 1000
                }
            }

        });

        //slider for internal pages
        $(".not-front #slides").slidesjs({
            width: 1141,
            height: 275 ,
            play: {
                active: false,
                interval: 6000,
                auto: true,
                swap: true,
                pauseOnHover: false,
                restartDelay: 2500,
                preloadImage: '../../../../default/files/loading.gif'/*tpa=http://www.wyndhamworldwide.com/sites/default/files/loading.gif*/,
                preload: true
            },
            navigation: {
                active: false
            },
            effect: {
                slide: {
                    speed: 1000
                }
            }

        });

        //awards slider
        $("#awards").slidesjs({
            width: 254,
            height: 105,
            play: {
                active: false,
                interval: 6000,
                auto: true,
                swap: true,
                pauseOnHover: false,
                restartDelay: 2500,
                preloadImage: '../../../../default/files/loading.gif'/*tpa=http://www.wyndhamworldwide.com/sites/default/files/loading.gif*/,
                preload: true
            },
            navigation: {
                active: true
            },
            pagination: {
                active: false
            },
            effect: {
                slide: {
                    speed: 1000
                }
            }

        });

        //Innovation slider
        $(".view-innovation-slider #slides").slidesjs({
            autoWidth: true,
            autoHeight: true,
            play: {
                active: false,
                interval: 6000,
                auto: true,
                swap: true,
                pauseOnHover: false,
                restartDelay: 2500,
                preloadImage: '../../../../default/files/loading.gif'/*tpa=http://www.wyndhamworldwide.com/sites/default/files/loading.gif*/,
                preload: true
            },
            navigation: {
                active: true
            },
            pagination: {
                active: false
            },
            effect: {
                slide: {
                    speed: 1000
                }
            }

        });

		// BRAND BAR SLIDER STUFF
		
		function switchTabs() {
		    var tabs = $('.business_units ul > li'),
		        active = tabs.filter('.active'),
		        next = active.next('li'),
		        toClick = next.length ? next.find('a') : tabs.eq(0).find('a');
		
		    toClick.trigger('click');
		}
		
		var tabCarousel = setInterval(switchTabs, 8000);
		
		$('.business_units').hover(function() {
			clearInterval(tabCarousel);
		}, function() {
			tabCarousel = setInterval(switchTabs, 8000);
		});
        //Press releases anchors
        if($('.view-press-releases').length > 0) {
            var hash = location.hash;
            $('#pr-corp ul.pagination li a').each(function(){
                var href = $(this).attr('href') + '#pr-corp';
                $(this).attr('href', href);
            })
            $('#pr-whg ul.pagination li a').each(function(){
                var href = $(this).attr('href') + '#pr-whg';
                $(this).attr('href', href);
            })
            $('#pr-wer ul.pagination li a').each(function(){
                var href = $(this).attr('href') + '#pr-wer';
                $(this).attr('href', href);
            })
            $('#pr-wvo ul.pagination li a').each(function(){
                var href = $(this).attr('href') + '#pr-wvo';
                $(this).attr('href', href);
            })
            if(hash == '#pr-corp') {
                $('.nav-tabs a').eq(1).tab('show');
                $("html, body").animate({ scrollTop: 0 }, "slow");
            }
            if(hash == '#pr-whg') {
                $('.nav-tabs a').eq(2).tab('show');
                $("html, body").animate({ scrollTop: 0 }, "slow");
            }
            if(hash == '#pr-wer') {
                $('.nav-tabs a').eq(3).tab('show');
                $("html, body").animate({ scrollTop: 0 }, "slow");
            }
            if(hash == '#pr-wvo') {
                $('.nav-tabs a').eq(4).tab('show');
                $("html, body").animate({ scrollTop: 0 }, "slow");
            }
           if(location.search != '') {
                var ids = '.tab-content';

                $(ids).hide();
                $('.nav-tabs').removeClass('invisible');
                $('.loading').show().fadeOut(1200);
                setTimeout(function(){
                    $(ids).removeClass('invisible');
                    $(ids).fadeIn(1000);


                },2000);

            }
        }
        //contact us switching tabs
        $('.btn-group .dropdown-menu a').click(function(){
            if($(this).hasClass('wer')) {
                $('.nav-tabs a').eq(2).tab('show');
                $('.contact-block').hide();
            }
            else if($(this).hasClass('wyndww')) {
                $('.contact-block').show();
                $('#edit-submitted-category').focus();
                $('#pr-all .dropdown-toggle').dropdown('toggle');
            }
            else if($(this).hasClass('wvo')) {
                $('.nav-tabs a').eq(3).tab('show');
                $('.contact-block').hide();
            }
            else {
                $('.nav-tabs a').eq(1).tab('show');
                $('.contact-block').hide();
            }
        })

        $('.nav-tabs').attr('href', '');
        
        //tabs
		/*if($("#tabs").length > 0) {
		$("#tabs").tabs();
		}*/
		/*if($("#accordion").length > 0) {
		$( "#accordion" ).accordion({
		            heightStyle: "fill"
		        });
		}*/
		/*if($(".accordion").length > 0) {
		        $(".accordion").accordion({ heightStyle: "content", collapsible: true });
		}*/

        //adding classes to category menu
        //$('#block-wyndham-category-menu ul li a.active').parent().addClass("active-trail").parent('ul').parent().addClass('active-container').parent("ul").parent().addClass('active-container').parent("ul").parent().addClass('active-container');
        $('#block-wyndham-category-menu ul li a.active').parent().addClass("active-trail").parent('ul').parent().addClass('active-container');
        $(".fourth:has('.active-container')").css('display','block');
        $(".fourth .active-container ul:has('.active-trail')").css('display','block');
        $('#block-wyndham-media-menu ul li a.active').parent().addClass("active-trail").parent().addClass('active-container');
        $('#block-wyndham-investors-menu ul li a.active').parent().addClass("active-trail").parent().addClass('active-container');
        $("ul:has('.active-container')").css('display','block');

        //Adding class for menu headers li wihtout triangle
       // $($("li:has(h3)")).addClass('noarrow');
       

	   // BRAND PAGE GRID AND FILTER - Uses the Isotope plugin: isotope.metafizzy.co/index.html


// it could be that the CSS is giving these things a width and it is messing with the plugin ( or we could just do this: //github.com/zonear/isotope-perfectmasonry - JT

/*
// ISOTOPE FOR OUR BRANDS PAGE
var $container = $('.page-taxonomy-term-2 .gridly')
$container.isotope({
  resizable: false, // disable normal resizing
  masonry: { columnWidth: $container.width() / 4 }
});
// update columnWidth on window resize
$(window).smartresize(function(){
  $container.isotope({
    masonry: { columnWidth: $container.width() / 4 }
  });
});

// ISOTOPE FOR WER PAGE
var $container1 = $('.page-taxonomy-term-37 .gridly')
$container1.isotope({
  resizable: false, // disable normal resizing
  masonry: { columnWidth: $container1.width() / 3 }
});
// update columnWidth on window resize
$(window).smartresize(function(){
  $container1.isotope({
    masonry: { columnWidth: $container1.width() / 3 }
  });
});

// ISOTOPE FOR WVO PAGE
var $container2 = $('.page-taxonomy-term-36 .gridly')
$container2.isotope({
  resizable: false, // disable normal resizing
  masonry: { columnWidth: $container2.width() / 4 }
});
// update columnWidth on window resize
$(window).smartresize(function(){
  $container2.isotope({
    masonry: { columnWidth: $container2.width() / 4 }
  });
});

// ISOTOPE FOR WHG PAGE
var $container3 = $('.page-taxonomy-term-34 .gridly')
$container3.isotope({
  itemSelector : '.brand',
  resizable: false, // disable normal resizing
  masonry: { columnWidth: $container3.width() / 3 }
});
// update columnWidth on window resize
$(window).smartresize(function(){
  $container3.isotope({
    masonry: { columnWidth: $container3.width() / 3 }
  });
});
*/

var $container = $('.gridly')

  $('#filters a').click(function(){
    $(this).parent().addClass('active');
    $(this).parent().siblings().removeClass('active');
    var selector = $(this).attr('data-filter');
    $container.isotope({ filter: selector });
    return false;
  });


  //MOBILE NAV STUFF

  $(".navbar").navobile({
    cta: "#show-navobile",
    changeDOM: true
  });

  $('.navobile-mobile-only .om-link').each(function(index, el) {
    if ($(this).parent('.om-leaf').children('.om-maximenu-content').length > 0) {
      $(this).after('<span class="subnav_collapse"><i class="fa fa-chevron-down"></i></span>');
    }
  });
  $('.navobile-desktop-only .om-link').each(function(index, el) {
    if ($(this).parent('.om-leaf').children('.om-maximenu-content').length > 0) {
      $(this).append('<span class="subnav_collapse"><i class="fa fa-caret-down"></i></span>');
    }
  });
  $('#show-navobile').click(function(){
	  $('.home_link').fadeToggle();
  });
	
  $('.subnav_collapse').click(function() {
    var target_subnav = $(this).parent('.om-leaf').find('.om-maximenu-content');
    target_subnav.toggle('fast');
    $('.om-maximenu-content').not(target_subnav).slideUp('fast');
    
  });

  $(window).scroll( function (){
  	if (!$('.navobile-mobile-only').hasClass('navobile-navigation-visible')) {
	  if ($(window).scrollTop() > 100) {
		  $('.home_link').fadeIn();
	  } else {
		  $('.home_link').fadeOut();
	  }
	}
  });

  $('#show-navobile').click(function(){
    $('.globalnav-inner').fadeToggle();
    $('#show-globalnav').fadeToggle();
    if ($('#booking-slide').is(":visible")) {
      $('#booking-slide').collapse('hide');
    }
  });

  //GLOBAL NAV
  var clonednav = $('.globalnav').clone();
  clonednav.appendTo($('.mobile_navbar'));
  clonednav.removeClass('hidden-xs');
  clonednav.hide();
  $('#show-globalnav').click(function() {
    clonednav.fadeToggle('fast');
    if ($('#booking-slide').is(":visible")) {
    $('#booking-slide').collapse('hide');
    }
  });

  $('.bannerww').insertBefore($('.business_units'));


  $('.sidebar_toggle').click(function() {
    $('.main-content').prev('aside').slideToggle('fast');
  });
  $(window).resize(function() {
    if ($(window).width() > 767 && $('.main-content').prev('aside').is(':hidden')) {
      $('.main-content').prev('aside').attr('style', 'overflow: hidden;');
    }
  });
  //Sidebar Nav

//  $('.aside .menu li').each(function(index, el) {
//    if ($(this).children('ul').children().length > 0) {
//      $(this).find('a:first').after('<a href="#" class="label label-default">sub-menu</a>');
//    }
//  });
//
//  $('.aside .menu .label').click(function() {
//    $(this).closest('li').children('ul').not('.content').slideToggle('fast');
//
//    return false;
//  });

  // GOOGLE ANALYTICS STUFF

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../../../../../www.google-analytics.com/analytics.js'/*tpa=http://www.google-analytics.com/analytics.js*/,'ga');

  ga('create', 'UA-37779543-1', 'auto');
  ga('send', 'pageview');

  $('.menu .bookit').on('click', function() {
    ga('send', 'event', 'button', 'click', 'book-your-vacation');
    console.log();
  });
  
  $('.bookhotel .btn').on('click', function() {
    ga('send', 'event', 'button', 'click', 'book-a-hotel');
  });
  
  $('.bookrentals .btn').on('click', function() {
    ga('send', 'event', 'button', 'click', 'book-vacation-rentals');
  });
  
  $('.bookresort .btn').on('click', function() {
    ga('send', 'event', 'button', 'click', 'book-a-resort');
  });
// End Google Analytics
  

// See more at: http://www.snackoclock.net/2012/08/simple-box-sizing-border-box-fallback-for-ie/#sthash.jltMOWFe.dpuf 
// requires mondernizr to detect boxsizing
$(function(){
    if( !($('html').hasClass('boxsizing')) ){
        $('.boxSized, .boxSized *').each(function(){
            var fullW = $(this).outerWidth(),
                actualW = $(this).width(),
                wDiff = fullW - actualW,
                newW = actualW - wDiff;
 
            $(this).css('width',newW);
        });
    }
});

        //Selectbox Emulator
        $('.show-contacts').click(function(){
            $('#contact-listing').toggle();
        })
        //Changing tab by clicking in selectbox
        $('#contact-listing li a').click(function(){
            $('.nav-tabs li a').eq(1).click();
        })
        // Fisheye menu
        var fisheyemenu = {
            startSize : 50,
            endSize : 75,
            imgType : ".gif",
            init : function () {
                var animElements = document.getElementById("fisheye_menu").getElementsByTagName("img");
                var titleElements = document.getElementById("fisheye_menu").getElementsByTagName("span");
                for(var j=0; j<titleElements.length; j++) {
                    titleElements[j].style.display = 'none';
                }
                for(var i=0; i<animElements.length; i++) {
                    var y = animElements[i];
                    y.style.width = fisheyemenu.startSize+'px';
                    y.style.height = fisheyemenu.startSize+'px';
                    fisheyemenu.imgSmall(y);
                    animElements[i].onmouseover = changeSize;
                    animElements[i].onmouseout = restoreSize;
                }
                function changeSize() {
                    fisheyemenu.imgLarge(this);
                    var x = this.parentNode.getElementsByTagName("span");
                    x[0].style.display = 'block';
                    if (!this.currentWidth) this.currentWidth = fisheyemenu.startSize;
                    fisheyemenu.resizeAnimation(this,this.currentWidth,fisheyemenu.endSize,15,10,0.333);
                }
                function restoreSize() {
                    var x = this.parentNode.getElementsByTagName("span");
                    x[0].style.display = 'none';
                    if (!this.currentWidth) return;
                    fisheyemenu.resizeAnimation(this,this.currentWidth,fisheyemenu.startSize,15,10,0.5);
                    fisheyemenu.imgSmall(this);
                }
            },
            resizeAnimation : function (elem,startWidth,endWidth,steps,intervals,powr) {
                if (elem.widthChangeMemInt) window.clearInterval(elem.widthChangeMemInt);
                var actStep = 0;
                elem.widthChangeMemInt = window.setInterval(
                    function() {
                        elem.currentWidth = fisheyemenu.easeInOut(startWidth,endWidth,steps,actStep,powr);
                        elem.style.width = elem.currentWidth+"px";
                        elem.style.height = elem.currentWidth+"px";
                        actStep++;
                        if (actStep > steps) window.clearInterval(elem.widthChangeMemInt);
                    }
                    ,intervals)
            },
            easeInOut : function (minValue,maxValue,totalSteps,actualStep,powr) {
                //Generic Animation Step Value Generator By www.hesido.com
                var delta = maxValue - minValue;
                var stepp = minValue+(Math.pow(((1 / totalSteps)*actualStep),powr)*delta);
                return Math.ceil(stepp)
            },
            imgSmall : function (obj) {
                imgSrc = obj.getAttribute("src");
                var typePos = imgSrc.indexOf(fisheyemenu.imgType, 0);
                var imgName = imgSrc.substr(0, typePos);
                obj.setAttribute("src", imgName+"_small"+fisheyemenu.imgType);
            },
            imgLarge : function (obj) {
                imgSrc = obj.getAttribute("src");
                var typePos = imgSrc.indexOf("_small", 0);
                var imgName = imgSrc.substr(0, typePos);
                obj.setAttribute("src", imgName+fisheyemenu.imgType);
            }
        }
        if($('#fisheye_menu').length > 0) {
            fisheyemenu.init();
        }

        // Fisheye menu ends
        //conditional form show or hide button and return to home page link
        $('#webform-component-is-this-request-for input').eq('0').click(function(){
            $('.return-home').hide()
            $('#wwebform-client-form-1373 #edit-submit').show();
        })

        $('#webform-component-is-this-request-for input').eq('4').click(function(){
            $('.return-home').hide();
            $('#webform-client-form-1373 #edit-submit').show();
        })

        $('#webform-component-is-this-request-for input').eq('1').click(function(){
            $('.return-home').show()
            $('#webform-client-form-1373 #edit-submit').hide();
        })

        $('#webform-component-is-this-request-for input').eq('2').click(function(){
            $('.return-home').show()
            $('#webform-client-form-1373 #edit-submit').hide();})

        $('#webform-component-is-this-request-for input').eq('3').click(function(){
            $('.return-home').show()
            $('#webform-client-form-1373 #edit-submit').hide();})

        $('#edit-submitted-is-this-for-one-of-our-signature-charities input').click(function(){
            $('#webform-client-form-1373 #edit-submit').show();
        })
        
    // RESPONSIVE VIDEO CODE - FITVIDS.JS 
    
    //$(".main-content").fitVids();


	// SOCIAL MEDIA ROOM TABS
	
	$('.social-toggle button').click(function() {
		var target = $(this).attr("data-toggle");
		$('.'+target).addClass('active');
		$('.'+target).siblings().removeClass('active');
	});
    if($("#block-views-social-media-room-block").length > 0) {
        var hash = location.hash;
        hash = hash.replace('Tab', '');
        if(hash.length > 0) {
            $('.media-accordion a[href$="' + hash + '"]').click();
        }
    }


    }

  
  };
//Mobile share JS
function twittermobileshare() {

 var appstoreFail = "http://twitter.com/";

    //Check is device is isMobile
    var isMobile = (navigator.userAgent.match(/(Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini)/g) ? true : false );
    var appUrlScheme = "twitter://";

    if (isMobile)
    {
        //If the app is not installed the script will wait for 2sec and redirect to web.
        var loadedAt = +new Date;
        setTimeout(
                   function(){
                       if (+new Date - loadedAt < 2000){
                   window.location = appstoreFail;
                       }
                   }
                   ,25);
        //Try launching the app using URL schemes
        window.open(appUrlScheme,"_self");
    } else {
        //Launch the website
        window.location = weblink;
    }
};
function facebookmobileshare() {

 var appstoreFail = "http://facebook.com/profile";

    //Check is device is isMobile
    var isMobile = (navigator.userAgent.match(/(Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini)/g) ? true : false );
    var appUrlScheme = "fb://";

    if (isMobile)
    {
        //If the app is not installed the script will wait for 2sec and redirect to web.
        var loadedAt = +new Date;
        setTimeout(
                   function(){
                       if (+new Date - loadedAt < 2000){
                   window.location = appstoreFail;
                       }
                   }
                   ,25);
        //Try launching the app using URL schemes
        window.open(appUrlScheme,"_self");
    } else {
        //Launch the website
        window.location = weblink;
    }
};

})(jQuery);



