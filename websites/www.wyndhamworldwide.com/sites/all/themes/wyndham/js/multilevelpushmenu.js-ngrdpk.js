/*
Copyright (c) 2013, Made By Made Ltd
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the "Made By Made Ltd" nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MADE BY MADE LTD BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

(function(){(function(a,b){b.navobile=function(d,f){var e,c;e=this;e.$el=b(d);e.el=d;e.$el.data("navobile",e);e.attach=function(){var g;e.$el.data({open:false});e.$content.data({drag:false});e.bindTap(e.$cta,e.$nav,e.$content,(b("html").hasClass("touch")?"touchend":"click"));if(typeof Hammer==="function"&&(e.options.bindSwipe||e.options.bindDrag)){g=Hammer(e.$content,e.options.hammerOptions);if(e.options.bindSwipe){e.bindSwipe(e.$nav,e.$content)}if(e.options.bindDrag){return e.bindDrag(e.$nav,e.$content)}}};e.bindTap=function(i,g,h,j){return i.on(j,function(k){k.preventDefault();k.stopPropagation();if(!e.isMobile()){return false}if(g.data("open")){e.slideContentIn(g,h)}else{e.slideContentOut(g,h)}return false})};e.bindSwipe=function(g,h){h.on("swipeleft",function(i){if(!e.isMobile()){return false}if(h.data("drag")){e.removeInlineStyles(g,h);h.data("drag",false)}e.slideContentIn(g,h);i.gesture.preventDefault();return i.stopPropagation()});return h.on("swiperight",function(i){if(!e.isMobile()){return false}if(h.data("drag")){e.removeInlineStyles(g,h);h.data("drag",false)}e.slideContentOut(g,h);i.gesture.preventDefault();return i.stopPropagation()})};e.bindDrag=function(g,h){return h.on("dragstart drag dragend release",function(i){var k,j;if(!e.isMobile()){return false}if(i.type==="release"){e.removeInlineStyles(g,h);return false}if(i.direction==="left"){if(!h.hasClass("navobile-content-hidden")){return false}else{e.slideContentIn(g,h)}}if(i.direction==="right"){if(i.type==="dragend"){if(i.distance>60){e.slideContentOut(g,h)}else{e.slideContentIn(g,h)}e.removeInlineStyles(g,h);return false}if(i.type==="dragstart"){h.data("drag",true)}k=i.position.x;j=Math.ceil(e.calculateTranslate(k));if(j>80||j<0){return false}if(b("html").hasClass("csstransforms3d")){return h.css("transform","translate3d("+j+"%, 0, 0)")}else{if(b("html").hasClass("csstransforms")){return h.css("transform","translateX("+j+"%)")}}}})};e.animateLeft=function(i,g,h){if(!b("html").hasClass("csstransforms3d")&&!b("html").hasClass("csstransforms")){h.animate({left:i},100,e.options.easing)}else{if(i==="0%"){h.removeClass("navobile-content-hidden")}else{h.addClass("navobile-content-hidden")}}if(i==="0%"){g.removeClass("navobile-navigation-visible")}else{g.addClass("navobile-navigation-visible")}return e.removeInlineStyles(g,h)};e.slideContentIn=function(g,h){g.data("open",false);return e.animateLeft("0%",g,h)};e.slideContentOut=function(g,h){g.data("open",true);return e.animateLeft("80%",g,h)};e.calculateTranslate=function(g){return(g/b(document).width())*100};e.removeInlineStyles=function(g,h){return h.css("transform","")};e.isMobile=function(){return b("#navobile-device-pixel").width()>0};c={init:function(h){var g;if(b("body").hasClass("navobile-bound")){return}e.options=b.extend({},b.navobile.settings,h);e.$cta=b(e.options.cta);e.$content=b(e.options.content);e.$nav=e.options.changeDOM?e.$el.clone():e.$el;e.$content.addClass("navobile-content");if(b("#navobile-device-pixel").length===0){b("body").append('<div id="navobile-device-pixel" />')}b("body").addClass("navobile-bound");if(e.options.changeDOM){e.$el.addClass("navobile-desktop-only");e.$nav.addClass("navobile-mobile-only");g=e.$nav.attr("id");e.$nav.attr("id","navobile-"+g);e.$content.before(e.$nav)}e.$nav.addClass("navobile-navigation");return e.attach()}};if(c[f]){return c[f].apply(this,Array.prototype.slice.call(argument,1))}else{if(typeof f==="object"||!f){return c.init(f)}else{return b.error("Method "+f+" does not exist on jQuery.navobile")}}};b.navobile.settings={cta:"#show-navigation",content:"#content",easing:"linear",changeDOM:false,bindSwipe:false,bindDrag:false,hammerOptions:{}};return b.fn.navobile=function(c){return this.each(function(){return new b.navobile(this,c)})}})(window,jQuery)}).call(this);