// EFM Client Side Poll API
// Copyright 2014 Verint Systems Inc. All Rights Reserved Worldwide.
// Version: 8.0.0
function VoviciPoll(pollId) { this.PollId = pollId; }
VoviciPoll.prototype.RenderPoll = function(isPreview) {
	if (!isPreview && this.GetCookie()) {
		document.getElementById('efm-poll-' + this.PollId + '-results').style.display = 'block';
	} else {
		document.getElementById('efm-poll-' + this.PollId + '-question').style.display = 'block';
	}
};
VoviciPoll.prototype.SubmitVote = function() {
	var results = '';
	var index = 1;
	for (;;) {
		var element = document.getElementById(this.PollId + '_Q1_' + index);
		if (!element) element = document.getElementById('Q1_' + index);
		if (!element) break;
		results += ((element.checked) || element.getAttribute('checked') == 'checked') ? '1|' : '0|';
		index++;
	}
	if (results.indexOf('1') != -1) {
		this.SetCookie();
		var url = document.getElementById('efm-poll-' + this.PollId + '-action').value;
		var headID = document.getElementsByTagName('head')[0];
		var newScript = document.createElement('script');
		newScript.type = 'text/javascript';
		newScript.src = url + '?p=' + this.PollId + '&v=' + results;
		headID.appendChild(newScript);
	} else {
		alert('Please select an option before voting.');
	}
};
VoviciPoll.prototype.Rate = function(id) {
	document.getElementById(id).checked = true;
	var pos = id.lastIndexOf('_');
	var baseId = id.substr(0, pos + 1);
	var rating = id.substr(pos+1);
	var i = 1;
	for (;;) {
		var element = document.getElementById(baseId + i + '_rating');
		if (!element) break;
		element.className = (i <= rating) ? 'efm-poll-rating-selected' : '';
		i++;
	}
};
VoviciPoll.prototype.ViewResults = function(newContents) {
	if (newContents) {
		var pollDiv = document.getElementById('efm-poll-' + this.PollId);
		pollDiv.outerHTML = newContents;
	} else {
		var questionDiv = document.getElementById('efm-poll-' + this.PollId + '-question');
		if (questionDiv) questionDiv.style.display = 'none';
	}
	document.getElementById('efm-poll-' + this.PollId + '-results').style.display = 'block';
};
VoviciPoll.prototype.SetCookie = function() {
	var expires = new Date();
	expires.setTime(expires.getTime() + (90*24*60*60*1000));
	var cookieString = 'PollStatus_' + this.PollId + '=';
	cookieString += '; expires=' + expires.toGMTString();
	document.cookie = cookieString
};
VoviciPoll.prototype.GetCookie = function() {
	var cookieString = document.cookie;
	var cookies = cookieString.split('; ');
	for (var i = 0; i < cookies.length; i++){
		var nameValuePair = cookies[i].split('=');
		if (nameValuePair[0] == 'PollStatus_' + this.PollId) return true;
	}
	return false;
};
var poll04BD76CC486C93C6 = new VoviciPoll('04BD76CC486C93C6');document.write('<style\ type=\"text/css\">#efm-poll-04BD76CC486C93C6\ {\ display:\ block;\ width:\ 166px;\ padding:\ 6px;\ border:1px\ none\ #808080;\ \ background:\ #FFFFFF;\ color:\ #000000;\ font-family:\ Arial;\ font-size:\ 11px;\ font-weight:\ normal;\ }#efm-poll-04BD76CC486C93C6\ *\ {\ font-family:\ inherit;\ font-size:\ inherit;\ color:\ inherit;\ }#efm-poll-04BD76CC486C93C6\ .efm-poll-chooseone,\ #efm-poll-04BD76CC486C93C6\ .efm-poll-choosemany,\ #efm-poll-2E113F80575343FC\ .efm-poll-rating\ {\ margin:\ 20px\ 0px;\ }#efm-poll-04BD76CC486C93C6\ .efm-poll-vote,\ #efm-poll-04BD76CC486C93C6\ .efm-poll-viewresults\ {\ text-align:\ center;\ clear:both;\ }#efm-poll-04BD76CC486C93C6\ .efm-poll-freqbar\ {\ display:\ block;\ background:#3399FF;\ height:\ 12px;\ !important;\ }.perseus-link\ {font-size:\ x-small;}</style>');document.write('<div\ id=\"efm-poll-04BD76CC486C93C6\"><div\ class=\"efm-poll-heading\">In\ terms\ of\ digital\ services,\ my\ institution\ is:</div><div\ id=\"efm-poll-04BD76CC486C93C6-question\"\ class=\"efm-poll-question\"\ \ style=\"display:\ none;\"><div\ class=\"efm-poll-chooseone\"><input\ type=\"radio\"\ name=\"Q1\"\ value=\"1\"\ id=\"04BD76CC486C93C6_Q1_1\"\ /><label\ for=\"04BD76CC486C93C6_Q1_1\">Far\ ahead</label><br\ /><input\ type=\"radio\"\ name=\"Q1\"\ value=\"2\"\ id=\"04BD76CC486C93C6_Q1_2\"\ /><label\ for=\"04BD76CC486C93C6_Q1_2\">Ahead</label><br\ /><input\ type=\"radio\"\ name=\"Q1\"\ value=\"3\"\ id=\"04BD76CC486C93C6_Q1_3\"\ /><label\ for=\"04BD76CC486C93C6_Q1_3\">On\ par</label><br\ /><input\ type=\"radio\"\ name=\"Q1\"\ value=\"4\"\ id=\"04BD76CC486C93C6_Q1_4\"\ /><label\ for=\"04BD76CC486C93C6_Q1_4\">Behind</label><br\ /></div><div\ class=\"efm-poll-vote\"><input\ type=\"button\"\ name=\"vote\"\ value=\"Submit\"\ onclick=\"poll04BD76CC486C93C6.SubmitVote();\"></div><div\ class=\"efm-poll-viewresults\"><a\ href=\"javascript:poll04BD76CC486C93C6.ViewResults();\">View\ Results</a></div></div><input\ type=\"hidden\"\ id=\"efm-poll-04BD76CC486C93C6-action\"\ value=\"https://www.yourvoice2us.com/poll.ashx\"\ /><div\ id=\"efm-poll-04BD76CC486C93C6-results\"\ class=\"efm-poll-results\"\ \ style=\"display:\ none;\"><table\ cellspacing=\"0\"\ cellpadding=\"5\"><tbody><tr><td\ colspan=\"2\">Far\ ahead</td></tr><tr><td\ width=\"100%\"><div\ class=\"efm-poll-freqbar\"\ style=\"width:47%;\">&nbsp;</div></td><td\ align=\"right\"\ nowrap=\"nowrap\">97</td></tr><tr><td\ colspan=\"2\">Ahead</td></tr><tr><td\ width=\"100%\"><div\ class=\"efm-poll-freqbar\"\ style=\"width:40%;\">&nbsp;</div></td><td\ align=\"right\"\ nowrap=\"nowrap\">84</td></tr><tr><td\ colspan=\"2\">On\ par</td></tr><tr><td\ width=\"100%\"><div\ class=\"efm-poll-freqbar\"\ style=\"width:55%;\">&nbsp;</div></td><td\ align=\"right\"\ nowrap=\"nowrap\">114</td></tr><tr><td\ colspan=\"2\">Behind</td></tr><tr><td\ width=\"100%\"><div\ class=\"efm-poll-freqbar\"\ style=\"width:100%;\">&nbsp;</div></td><td\ align=\"right\"\ nowrap=\"nowrap\">208</td></tr><tr><td\ colspan=\"2\">Total\ Votes:\ 503</td></tr></tbody></table></div></div>');poll04BD76CC486C93C6.RenderPoll(false);