var resourcePath;
var q;
var searchBoxTitle;
var noResultMessage;
var searchDefault;
var didYouMean;
var gsaUrl;
var num;
var paginationQ;
var index;
 var lastelement;
 var autoCompleteUrl;
function onLoad()
{
searchSubmit(true);
}
function pMethod(OnlyPageName,id){
    var idfromhref=id;
     var st= document.getElementById(idfromhref).href;
        var pq =st.split('/');
         
        var rm= st.replace(pq[pq.length-1],OnlyPageName);
       location.href= rm;
    }
function facetSearch(currentCat,entireSite)
{
	if(currentCat.value != entireSite && currentCat.checked)
      {
          var facetCatValue = document.searchFacetForm.sitesearch;
          facetCatValue[0].checked = false;
      }
      if(currentCat.value == entireSite && currentCat.checked)
      {
          clearCategories(entireSite);
      }
      searchSubmit(false);
}
function clearCategories(entireSite)
{
    var checkBoxes = document.searchFacetForm.sitesearch;
    for(i=0; i<document.searchFacetForm.sitesearch.length; i++)
    {
        if(checkBoxes[i].value != entireSite)
        {
            checkBoxes[i].checked = false;  
        }
        else
        {
            checkBoxes[i].checked = true;
        }
    } 
}
function clearCatSubmitSearch(entireSite)
{
    clearCategories(entireSite);
    searchSubmit(false);
}
function populatesearch(searchDefault)
{
      if(document.searchbox.q.value== searchDefault || document.searchbox.q.value == '')
      {
          document.searchbox.q.value = '';
      }
}
function setSearchBox(searchDefault)
{
    if(document.searchbox.q.value == searchDefault || document.searchbox.q.value == '')
    {
        document.searchbox.q.value = searchDefault;
    }
}
function searchSubmit(status){
if(!status)
{
q=document.searchbox.q.value;
document.getElementById("start").value = 0;
document.getElementById("index").value = 1;
paginationQ = q;
}
else
{
q = paginationQ;
document.searchbox.q.value = q;
}
var testQ = "";
if(document.searchbox.q.value != null && document.searchbox.q.value != undefined)
{
         testQ = $.trim(document.searchbox.q.value);
}
if(document.searchbox.q.value == searchDefault || document.searchbox.q.value == '' || testQ == '' ){
      return;
      }
  var searchString = escape(q.replace("™","&trade;"));
	var facetCatValue = document.searchFacetForm.sitesearch;
  var site = "";
	for( i = 0; i<facetCatValue.length ; i++)
  {        
      if(facetCatValue[i].checked)
      {
          if(site == "" || site == "undefined")
          {
            site =  facetCatValue[i].value;
          }
          else
          {
            site = site + "|" + facetCatValue[i].value;
          }
      }
  }
if(site == '')
  {
      var resultsDiv = document.getElementById('searchResults');
      if ( resultsDiv.hasChildNodes() )
      {
          while ( resultsDiv.childNodes.length >= 1 )
          {
              resultsDiv.removeChild( resultsDiv.firstChild );       
          } 
      }
      if(facetCatValue[0] != null && facetCatValue[0] != "" && facetCatValue[0] != undefined)
      {
          clearCatSubmitSearch(facetCatValue[0].value);        
      }      
  }
  else
  {
      callGsa(searchString,site);
  }
}
function callGsa(searchString,site)
    {
        var start = document.getElementById("start").value;
        var controler = gsaUrl+"?q="+searchString+"&site="+site+"&qType=siteSearch"+"&start="+start+"&num="+noOfResultsPage;
        $.ajax({ url:controler ,
            dataType: 'jsonp', 
            crossDomain: 'true',
            beforeSend: function() {
                 $('#ajaxLoading').show();
            },
            error: function(request,error) {
            },
            success: function(gsaresponse){  
            },
            complete: function() {
                $('#ajaxLoading').hide();
            }
            });
    }
    function submitDidYouMean()
    {       
        document.searchbox.q.value = q;
        searchSubmit(false);
    }
function getAlert(jsonObject){
	var vurl = document.getElementById("topSearchRedirect").href; 
        var lc = vurl.split("/")[3];  
        var jsonObj = jQuery.parseJSON(jsonObject);
        var resultsDiv = document.getElementById('searchResults');
		var resultTitle = document.getElementById('searchResulttitle');
		if ( resultTitle.hasChildNodes() )
        {
          while ( resultTitle.childNodes.length >= 1 )
          {
            resultTitle.removeChild( resultTitle.firstChild );
            document.getElementById("pages").innerHTML = "";
            document.getElementById("pages1").innerHTML = "";       
          } 
        }
		if ( resultsDiv.hasChildNodes() )
        {
            while ( resultsDiv.childNodes.length >= 1 )
            {
                resultsDiv.removeChild( resultsDiv.firstChild );
                document.getElementById("pages").innerHTML = "";
                document.getElementById("pages1").innerHTML = "";       
            } 
        }
		var elementUL = document.createElement('ul');
		var p = jsonObj.gsautput;    
        var counter = 0;
        var didYouMeanMessage = "";
        num = "";
        var title = "";
        var description = "";
        var imagePath = "";
        var fnl ='';    
        for (var key in p) {
            if (p.hasOwnProperty(key)) {
                 title = p[key].gsaTitle;
                  description = p[key].gsadescription;
                   if(description ==null || description ==undefined ){
                     description ="";
                     }
                     if(p[key].url != null){
						var nme =  p[key].url;
						var ltn= nme.substring(nme.lastIndexOf("/"));
                          //nme = nme.substring(0,nme .lastIndexOf("/"));
                          //nme = nme .substring(nme.lastIndexOf("/"));
                          //fnl = "/"+lc+nme+ltn;
						  fnl = "/"+lc+ltn;
					} 
				  if(key != "noOfResults")
                 {
                     var elementLI = document.createElement('li');
                         if(counter%2 != 0){
                             elementLI.className="evenList";
                         }
                        var htmlcode = "";
                        htmlcode= '<h3><a href="'+fnl+'">'+title+'</a></h3>'+description;  
                        elementLI.innerHTML = htmlcode; 
                         lastelement = elementLI;
                         elementUL.appendChild(elementLI); 
						counter = counter + 1; 
                 }
              }
              if (p.hasOwnProperty(key) && key == 'noOfResults') {
                 num = String(p[key]);
                 }
              if(num != "")
              writePages();
            }
			if(p.noResults)
            {
                counter = 0;
            }
			var titleText = searchBoxTitle+" "+q;
			var ElementTitleText = document.createTextNode(titleText);
           resultTitle.appendChild(ElementTitleText);
    if(counter != 0)
    {
        resultsDiv.appendChild(elementUL);
    }
    else
    {
        resultsDiv.innerHTML = noResultMessage;
    }
     }

   