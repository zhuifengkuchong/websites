
(function($){

 /* Mac specific class added
    ----------------------------------------------------*/
    (function(){
        if(navigator.userAgent.indexOf('Mac') > 0) {
             $('body').addClass('mac_os');
         } 
    })();

    
    /* Modal Overlay for Connect With Kellogg
    ----------------------------------------------------*/
     (function(){
        $('a.connectKModal').live('click',function(){
            $('#connectWithKellogg').load($(this).attr("href"));
            $("#connectWithKellogg, #greyout").show();
            $('html, body').animate({scrollTop:0}, 'fast');
            return false;
        });
    
        $('.closeModal, #greyout').live('click', function() {
         $('.divModalDialog , #greyout').hide();
         return false;
        });
    })();

    /* Custom jQuery extensions
    ----------------------------------------------------*/
    $.fn.extend({
        equalizeHeights: function(){
            this.css("height","");
            var maxHeight = this.map(function(i,e) {
                return $(e).height()+70;
            }).get();
            return this.height( Math.max.apply(this, maxHeight) );
        }
    });
    
    
    

        /* Setup the flexslider Carousels
    ----------------------------------------------------*/
        $(document).ready(function() {
            /* Hero Slider */
             var speed=parseInt($('#rotationperiod').text());
            $('.hero-main .flexslider').flexslider({
                animation: 'slide',
                animationDuration: 750,
                slideshow: true,
                slideshowSpeed: speed,
                touchSwipe: true,
                start: pagination1,
                after: pagination
                });
                function pagination1(){
                        $('.flex-control-nav li a').wrapInner('<span/>');
                                $('.hero-main .next, .hero-main .prev, .flex-control-nav li a').click(function(){
                                                $(this).data('clicked', true);
                                });
                                if($('.hero-main .next').data('clicked') || $('.hero-main .prev').data('clicked') || $('.flex-control-nav li a').data('clicked')) {
                                                //do nothing
                                } else {
                                                var cycleTimer,
                                                                autoCount = 0,
                                                                slideSpeed = this.slideshowSpeed / 1000,
                                                                controlItemSize = 10,
                                                                activeNav = $('.hero-main .flex-control-nav li a.active span');
                                                                function startCycle() {
                                                                                cycleTimer = setInterval(function(){
                                                                                                                                                autoCount = ++autoCount;
                                                                                                                                                var newHeight = controlItemSize - (autoCount * (controlItemSize / slideSpeed));
                                                                                                                                                $(activeNav).css('height',newHeight);
                                                                                                                                                if (newHeight == 0){
                                                                                                                                                                $(activeNav).removeAttr('style');
                                                                                                                                                                clearInterval(cycleTimer);
                                                                                                                                                }
                                                                                                                                }, 1000);
                                                                }
                                                startCycle();
                                }
                }
                function pagination(){
                                $('.hero-main .next, .hero-main .prev, .flex-control-nav li a').click(function(){
                                                $(this).data('clicked', true);
                                });
                                if($('.hero-main .next').data('clicked') || $('.hero-main .prev').data('clicked') || $('.flex-control-nav li a').data('clicked')) {
                                                //do nothing
                                } else {
                                                var cycleTimer,
                                                                autoCount = 0,
                                                                slideSpeed = this.slideshowSpeed / 1000,
                                                                controlItemSize = 10,
                                                                activeNav = $('.hero-main .flex-control-nav li a.active span');
                                                                function startCycle() {
                                                                                cycleTimer = setInterval(function(){
                                                                                                                                                autoCount = ++autoCount;
                                                                                                                                                var newHeight = controlItemSize - (autoCount * (controlItemSize / slideSpeed));
                                                                                                                                                $(activeNav).css('height',newHeight);
                                                                                                                                                if (newHeight == 0){
                                                                                                                                                                $(activeNav).removeAttr('style');
                                                                                                                                                                clearInterval(cycleTimer);
                                                                                                                                                }
                                                                                                                                }, 1000);
                                                                }
                                                startCycle();
                                }
                }
            /* Brand Slider */
            var brandspeed=parseInt($('#brandrotationperiod').text());
            $('.brand-main .flexslider').flexslider({
                animation: 'slide',
                animationDuration: 750,
                slideshow: true,
                slideshowSpeed: brandspeed,
                touchSwipe: true,
                start:function(){
                    var li = $('.brand-main .slides li'),
                        lis = li.length,
                        lastLi = $('.brand-main .slides li').eq(lis - 2).find('span').length,
                        img1 = $('.brand-main .slides li:nth-child(2) span:nth-child(1)').html(),
                        img2 = $('.brand-main .slides li:nth-child(2) span:nth-child(2)').html(),
                        clonedLi = $('.brand-main .slides li:first-child'),
                        lastEmtyLi = $('.brand-main .slides li').eq(lis - 2);
                    if(lis > 1 && lastLi == 1){
                        $(clonedLi).append('<span>'+ img1 +'</span><span>'+ img2 +'</span>');
                        $(lastEmtyLi).append('<span>'+ img1 +'</span><span>'+ img2 +'</span>');
                    }else if(lis > 1 && lastLi == 2){
                        $(clonedLi).append('<span>'+ img1 +'</span>');
                        $(lastEmtyLi).append('<span>'+ img1 +'</span>');
                    }
                }
            });
        });

    


    /* Text Size Increase/Decrease
    ----------------------------------------------------*/
    (function(){
        
        /* Helper Functions */
        function createCookie(name,value,days) {
            if (days) {
              var date = new Date();
              date.setTime(date.getTime()+(days*24*60*60*1000));
              var expires = "; expires="+date.toGMTString();
            }
            else expires = "";
            document.cookie = name+"="+value+expires+"; path=/";
            var peopleHTML = $(".peopleDesc .person");
            peopleHTML.equalizeHeights();
        };

        readCookie = function(name) {
           var nameEQ = name + "=";
           var ca = document.cookie.split(';');
           for(var i=0;i < ca.length;i++) {
             var c = ca[i];
             while (c.charAt(0)==' ') c = c.substring(1,c.length);
             if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
           }
           return null;

        };

        function eraseCookie(name){
            createCookie(name,"",-1);

        };


        $('.nav-textsizer .default').click(function(){
            $(this).parent().attr('rel', '1'); 
            $('body').css('font-size','.8em');
            createCookie("rel", '1' , '14');
            createCookie("FontSize", ".8em", '14');
        });

        function setDefaultFont(){
           var default_rel= 1;
            var currentfont = readCookie("FontSize");
            if(readCookie("rel")!=null){
            default_rel= readCookie("rel");
            }
            $('body').css('font-size',currentfont);
            $('.nav-textsizer').attr('rel',default_rel);  

        }

        $('.nav-textsizer .minus').unbind("click").click(function(){

          var getCurrent = parseFloat($(this).parent().attr('rel'));
          if(getCurrent === 2){ 

              getCurrent--; 
              $(this).parent().attr('rel',getCurrent); 
              $('body').css('font-size','.8em'); 
              createCookie("rel", getCurrent , '14');
              createCookie("FontSize", ".8em", '14');
          }
          else if(getCurrent === 3){ 
              getCurrent--; 
              $(this).parent().attr('rel',getCurrent); 
              $('body').css('font-size','.95em');
              createCookie("rel", getCurrent , '14');
              createCookie("FontSize", ".95em", '14');
              }
              else {}

          return false;
        });

        $('.nav-textsizer .plus').unbind("click").click(function(){
          var getCurrent = parseFloat($(this).parent().attr('rel'));
          if(getCurrent === 1){ 
                  getCurrent++; 
            $(this).parent().attr('rel',getCurrent); 
                  $('body').css('font-size','.95em');  
                  createCookie("rel", getCurrent , '14');
                  createCookie("FontSize", ".95em", '14');

          }
              else if(getCurrent === 2){ 
              getCurrent++; 
                  $(this).parent().attr('rel',getCurrent); 
                  $('body').css('font-size','http://www.kelloggcompany.com/etc/designs/kelloggs/kelloggcompany/js/1.1em');  

                  createCookie("rel", getCurrent , '14');
                  createCookie("FontSize", "http://www.kelloggcompany.com/etc/designs/kelloggs/kelloggcompany/js/1.1em", '14');

              }
              else {}

          return false;
        }); 

        setDefaultFont();
                $.mobile.ajaxEnabled = false;

    })();

    

        
}(jQuery));








