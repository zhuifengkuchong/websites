(function($, undefined){
    //visit locale function
    $.visitLocale = function(e){
        var selector = $(".visit_locale a");        
        $(selector).live("click", function(){
            $('div.availability').hide();
            $(".locale_popup").slideToggle(function(){
                $('.lt-ie8 .main.brandfolio').toggleClass('ie7main');
            });
            return false;
        }); 
    }
    
/*  $.megaDraopdown = function(e){
        var myLink = $('.nav-main a').attr("href"); 
        $('.nav-main a').click(function(){
        if($(".megaDropDown").is(":hidden")){
            $('.megaDropDown').slideToggle();
            var page = $(this).attr('href').split(/\?/)[1];
            var pageURL = 'http://www.kelloggcompany.com/etc/designs/kelloggs/kelloggcompany/js/_includes/global/megadropdown.html';
                $.ajax({
                    type: 'POST',
                    url: pageURL,
                    data: page,
                    success: function(content) {
                       $('.megaDropDown').html(content);  // replace
                    }
                });        
            return false; // to stop link   
        
        } else{
                //var myLink = $(this).attr("href");
                window.location.replace(myLink);
        }
        
        });
    
    }*/
    
    
    $.megaDraopdown = function(e){
            
        $('.nav-main a').live('click',function(){           
        var currObj = $(this), 
            isDropDownOpen = $(this).attr('dropdown'),
            myLink = $(currObj).attr("href");   
        if(isDropDownOpen == "false"){
            $('.megaDropDown').slideUp();
            var page = $(this).attr('href').split(/\?/)[1];
            var pageURL = 'http://www.kelloggcompany.com/etc/designs/kelloggs/kelloggcompany/js/_includes/global/megadropdown.html';
                $.ajax({
                    type: 'POST',
                    url: pageURL,
                    data: page,
                    success: function(content) {                    
                       $('.megaDropDown').html(content);  // replace
                       $('.megaDropDown').slideDown();
                       //$('.nav-main a').attr('dropdown','false');
                       $(currObj).attr('dropdown', 'true');
                    }
                });        
            
            return false;
        
        } else{
                window.location.replace(myLink);            
        }
        
        });
        
        $('.megaDropDown a.close').live('click', function(){
            $('.megaDropDown').slideUp();
            //$('.nav-main a').attr('dropdown','false');
        });
    
    }
    
    $.availability = function(e){
        var myavailabilty = $('.brandLevels a.availability');
        $(myavailabilty).live('click', function(){
            $('div.availability').hide();
            $(this).parent().parent().find('div.availability').slideDown(); 
                var page = $(this).attr('href').split(/\?/)[1];
                $.ajax({
                    type: 'POST',
                    url: 'http://www.kelloggcompany.com/etc/designs/kelloggs/kelloggcompany/js/_includes/content/availability.html',
                    data: page,
                    success: function(content) {
                       $('div.availability').html(content);  // replace
                    }
                });   
                return false;
        });
        
        $(".availabilityTitle a").live('click', function(){
            $('div.availability').slideUp();
            return false;
        });
        
    
    }
    
    $.additionalResouces = function(e){
        var additionalVar = $('div.additionalResources div.list > a');
        $(additionalVar).live('click', function(){
            /*$(additionalVar).parent().find('p').slideUp();
            $(additionalVar).parent().find('a').removeClass('open');*/          
            $(this).toggleClass('open');
            $(this).parent().find('p').slideToggle();
            return false;
        });
        
    
    }
    
    $.searchSuggestion = function(e){
        var myInput = $('#searchBox'),
            searchLbl = $('label.searchlbl');
        
        $(myInput).keypress(function(){
            $('#searchSuggestion').show();
        });
        
        $(searchLbl).live('click', function(){
            $(myInput).focus();
            $(this).hide();
        });
        
        $(myInput).focus(function(){
            $(searchLbl).hide('');
        });
        $(myInput).focusout(function(event){
            $(searchLbl).hide();
            if($(myInput).val() == ""){
                $(searchLbl).show();
            } 
        });
        
        
        
        
        $('#searchSuggestion a.close').live('click', function(){
            $('#searchSuggestion').hide();
        });
        
    }
    
    $.healthterms = function(e){
            var mailInput = $('#emailId'),
            emailLbl = $('label.emailLbl');
            
        $('#termsCheckbox').live('click', function(){
            $('#termsButton').toggleClass('disabled');
            if($('input#termsCheckbox:checked')){
                $('#termsButton').removeAttr('disabled');
            } 
            $('#termsButton.disabled').attr('disabled','disabled'); 
        });
         $(emailLbl).live('click', function(){
            $(mailInput).focus();
            $(this).hide();
        });
        
        $(mailInput).focus(function(){
            $(emailLbl).hide('');
        });
        $(mailInput).focusout(function(event){
            $(emailLbl).hide();
            if($(mailInput).val() == ""){
               // alert(1);
                $(emailLbl).show();
            } 
        });
        
    }
    
    $.healthThank = function(){
        $('#termsButton').live('click',function(){
            //$('#formview').hide();
            //$('#thanksView').show();
        });
    }
    
            // Custom dropdown change...
            $('.cSelectbox select').bind('change',function () {
                $(this).parent().find('span').html($(this).find('option:selected').text());
            });

    $.visitLocale();
    //$.megaDraopdown();
    //$.availability();
    $.additionalResouces();
    $.searchSuggestion();
    $.healthterms();
    $.healthThank();
    $(".availabilityTitle a").live('click', function(){
        $('div.availability').slideUp();
        return false;
    });
    
    $('.megaDropDown a.close').live('click', function(){
                var megali = $('.nav-main li');
                    megali.removeClass('hover');

                $('.megaDropDown').slideUp();
                //$('.nav-main a').attr('dropdown','false');
            });
    
})(jQuery);

function availabilityDrop(path,divid,aDiv){
	$('div.locale_popup').hide();
    $('#'+aDiv).live('click', function(){
        $('.lt-ie8 .main').removeClass('ie7main');
        $('div.availability').hide();
        $('#'+divid).slideDown(); 
            var page = $(this).attr('href').split(/\?/)[1];
            $.ajax({
                type: 'get',
                url: path,
                data: page,
                success: function(content) {
                document.getElementById(divid).innerHTML = content;
                  // $('div.availability').html(content);  // replace
                }
            });   
            return false;
    });
    
    
}

function megaDropdown(path,id){
    $('#'+id).live('click', function(){          
        var currObj = $(this), 
            isDropDownOpen = $(this).attr('dropdown'),
            myLink = $(currObj).attr("href");   
            var megali = $('.nav-main li');
        if(isDropDownOpen == "false"){
                        megali.removeClass('hover');
                        $(this).closest('li').addClass('hover');
            $('.megaDropDown').slideUp();
            var page = $(this).attr('href').split(/\?/)[1];
            //var pageURL = 'http://www.kelloggcompany.com/content/NorthAmerica/kelloggcompanyus/en_US/megamenu.html';
                $.ajax({
                    type: 'GET',
                    url: path,
                    data: page,
                    success: function(content) {                    
                       //$('.megaDropDown').html(content);  // replace
                    document.getElementById("DropDown").innerHTML = content;
                       $('.megaDropDown').slideDown();
                       //$('.nav-main a').attr('dropdown','false');
                       $(currObj).attr('dropdown', 'true');
                    }
                });        
            
            return false;
        
        } else{
                window.location.replace(myLink);            
        }       
    });
    
}
    
    
    