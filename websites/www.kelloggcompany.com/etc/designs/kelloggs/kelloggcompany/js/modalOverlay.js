/*================================================================================================  
    GLOBAL VARIABLES
    ================================================================================================
  vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv*/

var isIE;
var isIE6;
var isIE7;
var isIE8;
var isIE8CompatibilityMode; 
var isLessThanIE8;

//close button for modal
var buttonMarkup;
if(isIE6){
    buttonMarkup = '<div class="modalheader"><a href="#prevpoint" class="btn_modalclose btn_modalclose_IE6"></a></div>'
}else{
    buttonMarkup = '<div class="modalheader"><a href="#prevpoint" class="btn_modalclose"></a></div>'
}


/*================================================================================================  
    MAIN: $(document).ready()
  ================================================================================================
  vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv*/
  
$(document).ready(function(){
    //initialize
    isIE = $.browser.msie;
    isIE6 = isIE && $.browser.version=='6.0';
    isIE7 = isIE && $.browser.version=='7.0';
    isIE8 = isIE && $.browser.version=='8.0'; 
    isIE8CompatibilityMode = isIE8 && document.documentMode && document.documentMode == 7;
    isLessThanIE8 = isIE && (isIE7 || isIE6);
    
    if(isLessThanIE8 && !isIE8CompatibilityMode){
        $('#modalContainer .modal_content .btn_modalclose').css({'top':'14px', 'right':'12px'});
    }
});

/*==== CENTER ELEMENT VERTICALLY ====*/ 
$.fn.centerItVertical = function () {
    this.css('position','fixed');
    this.css('top', (($(window).height() - this.outerHeight()) / 2)  + 'px');
    return this;
}

/*==== CENTER ELEMENT HORIZONTALLY ====*/   
$.fn.centerItHorizontal = function () {
    this.css('position','fixed');
    this.css('left', (($(window).width() - this.outerWidth()) / 2) + 'px');
    return this;
} 

$('.quickread').live('click',function(){
           // $(this).parent().parent().parent().prepend('<a name="prevpoint"/>');
            $('body').append(getModalMarkup('modalHTML','width:530px;'));
            $("#modalHTML").html($('#overlaySection'+$(this).attr('index')).html());
            openModal(500, false, 20);
            $('#modalContainer').addClass('modalSlim');
            $("html, body").animate({scrollTop: '0px'}, 100);
            return false;
    });
    
/*================================================================================================  
    FUNCTIONS
  ================================================================================================
  vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv*/

/*======================================================================================
    getModalMarkup()
---------------------------------------------------------------------------------------- 
    targetId                    -   the id of the div into which you will inject the content
    modalContainerStyle -   optional argument to style the modalContainer
---------------------------------------------------------------------------------------- 
    Return markup to create a modal overlay
----------------------------------------------------------------------------------------*/
function getModalMarkup(targetId, modalContainerStyle) {
    return '<div id="blackout"></div><div id="modalContainer" style="'+modalContainerStyle+'"><div class="modal_content">'+buttonMarkup+'<div id="'+targetId+'"></div></div></div>';
}//endfun getModalMarkup()

/*======================================================================================
    openModal()
----------------------------------------------------------------------------------------
    fadeIn  -   integer value to set the fade in time for both the background and content
    centerVertical - boolean, set to true if you want content auto centered horizontally
    top -   integer value to set distance of the content from the top of the window ,
                ignored if centerVertical is true
---------------------------------------------------------------------------------------- 
    Open the modal overlay
----------------------------------------------------------------------------------------*/
function openModal(fadeIn, centerVertical, top){
    var blackoutHeight;
    $('#blackout').css({'width':$(window).width(),'height':$(document).height()}).fadeIn(fadeIn).fadeTo(100,0.8, function(){
        $('#modalContainer').css({'height':$('#modalContainer').outerHeight(true), 'width':$('#modalContainer').outerWidth(true)}).centerItHorizontal();
        if(centerVertical){
            $('#modalContainer').centerItVertical();
        }else{
            $('#modalContainer').css({'top':top+'px', 'left':$(window).width()/2-$('#modalContainer').outerWidth(true)/2,'position':'absolute'});
        }
        $('#modalContainer').fadeIn(fadeIn);
        
        blackoutHeight = $('#blackout').outerHeight();
        if( blackoutHeight-(top) <= $('#modalContainer').outerHeight(true)){
            $('#blackout').css('height',blackoutHeight+(top)+'px');
         }
    }); 
    $('#blackout, div.modal_content .btn_modalclose, div.modal_content .btn_modalclose_IE6').bind('click', function(){ closeModal('#modalContainer'); return true;  });
    if(isIE6){ 
        $('html').scrollTop(0);
    } else {
        $('html','body').animate({scrollTop:0},'fast');
    }
}//endfun openModal()

/*======================================================================================
    closeModal()
---------------------------------------------------------------------------------------- 
    Close the modal overlay
----------------------------------------------------------------------------------------*/
function closeModal(){
    $('#blackout').remove(); 
    $('#flash_fallback_1').hide();
    $('#modalContainer').removeClass('modalSlim');
    $('#modalContainer').remove(); 
    return false;
}//endfun closeModal()


/*================================================================================================  
    CUSTOM JQUERY METHODS
  ================================================================================================
  vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv*/



