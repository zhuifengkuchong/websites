var xhr=null;
function httpReq() {
    if (window.XMLHttpRequest)
      {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xhr=new XMLHttpRequest();
      }
    else
      {
        // code for IE6, IE5
        xhr=new ActiveXObject("Microsoft.XMLHTTP");
      }
}

var Browser = {
          Version: function() {
            var version = 999; // we assume a some other browser
            if (navigator.appVersion.indexOf("MSIE") != -1)
              version = parseFloat(navigator.appVersion.split("MSIE")[1]);
            return version;
          }
}

function criticalMessage()
{
    httpReq();
    // set the protocol type to create full site path
    siteURLArray=(location.href).split("/");
    siteURL=siteURLArray[0]+"//"+siteURLArray[2]+"/"+siteURLArray[3];
    if (!xhr) 
    {
        alert ("XMLHttp failed to instantiate");
        return false;
    }                                       
    xhr.onreadystatechange = function() {                    
        if ((xhr.readyState==4) && (xhr.status==200))
        {                   
            // JSON object of CRX values
            var str=xhr.responseText;                        
            var i;
            var msg="";
            // check for defined alert messages 
            if(str!='{"success":false}'){               
                var json=JSON.parse(str);
                for (i=0;i<json.hits.length;i++)
                {      
                    
                    if(document.getElementById('error_wp')!=undefined && document.getElementById('error_wp')!=null){
                        // check this condition
                        var ser=json.hits[i].detMsgURL;    
                        // check the URL whether its a day based application or non-day based application       
                        var alertImgURL=json.hits[i].alertImgURL;                           
                        var destURL=json.hits[i].detMsgURL;
                        var shortMsg=json.hits[i].shortMsg;                                                                         
                        msg+='<div id="pf_CCErrorWrap"><p><img id="pf_CCImageStyle" src="'+alertImgURL+'"  alt="Alert Image" />&nbsp;<a id="pf_CCError_msg" href=\''+destURL+'\' target=\'_blank\'> '+shortMsg+'</a></div>';                
                        document.getElementById('error_wp').style.display = "block";
                    }
                }            
                if(document.getElementById('error_wp')!=undefined && document.getElementById('error_wp').innerHTML!=null) {
                    document.getElementById('error_wp').innerHTML=msg; 
                     //newton page align
                    if(msg!=""){
                       if(Browser.Version()<=7){
                          document.getElementById("page").style.paddingTop='16px';
                       }else{
                           document.getElementById("page").style.marginTop='12px';    
                       } 
                }
            }
          
         }   
		   return 1; 
        }     
    };   
    var URL="http://www.kelloggcompany.com/bin/ccquerybuilder.json";
    var params="propertyName=sitepath&propertyValue=" + siteURL;    
    xhr.open('POST',URL, true);     
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send(params);                                  
} 
