(function(){

var Stockticker = function(){
	var self = this,
	
		tradeDate,
		tradePrice,
		tradeChange,
		tradePercentageChange,
		
		getMonthDisplayName = function(numberOfMonth) {
			
			//var monthMap = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
			
			var monthMap = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
			
			return monthMap[numberOfMonth];
		},
		
		getTradeDateDisplay = function(dateFromApi) {
			var fullDate = new Date(dateFromApi);
			var displayDate = new Date();
			var month = getMonthDisplayName(fullDate.getMonth());
			var day = fullDate.getDate();
			var year = fullDate.getFullYear();
			var time = fullDate.toLocaleTimeString().replace(/:\d+ /, ' ');
			return month + ' ' + day + ', ' + year + ' ' + time;
		},
		
		getStockTickerMarkup = function(stockPrice, stockChange, stockPercentageChange, stockTradeDate) {
			var stockTickerMarkup = '<link rel="stylesheet" type="text/css" href="../../../../../www.kelloggs.com/content/dam/newton/css/stockticker_kco.css"/*tpa=http://www.kelloggs.com/content/dam/newton/css/stockticker_kco.css*/ /><div id="kelloggcompany_stockticker"><p>Kellogg Co. (K-NYSE) : $' + tradePrice + '<span>' + tradeChange + '(' + tradePercentageChange + '%)</span></p><p>' + getTradeDateDisplay(tradeDate) + ' CT</p></div><p>Quotes delayed at least 20 minutes</p>';
			$('.stocktrader').empty().append(stockTickerMarkup);
			if (tradeChange < 0) {
				$('#kelloggcompany_stockticker span').addClass('arrowDown');	
			}
		},

		fetchCurrentStock = function() {
			var stockUrl = 'http://investor.kelloggs.com/feed/StockQuote.svc/GetStockQuoteList?apiKey=DB0328D0FED14D978DEB3D4C3AE3A75B&symbol=K&exchange=NYSE&callback=?';
			$.getJSON(stockUrl, function() {
				$('.stocktrader').empty();
			}).done( function (data) {
				tradePrice = data.GetStockQuoteListResult[0].TradePrice;
				tradeDate = data.GetStockQuoteListResult[0].TradeDate;
				tradeChange = data.GetStockQuoteListResult[0].Change;
				tradePercentageChange = data.GetStockQuoteListResult[0].PercChange;
				getStockTickerMarkup();
			});
		};
		
	this.init = function() {
		fetchCurrentStock();
	};
};


var kStockticker = new Stockticker();

kStockticker.init();

})();