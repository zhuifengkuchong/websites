/**
* hoverIntent r5 // 2007.03.27 // jQuery 1.1.2+
* <http://cherne.net/brian/resources/jquery.hoverIntent.html>
* 
* @param  f  onMouseOver function || An object with configuration options
* @param  g  onMouseOut function  || Nothing (use configuration options object)
* @author    Brian Cherne <brian@cherne.net>
*/
(function($){$.fn.hoverIntent=function(f,g){var cfg={sensitivity:7,interval:100,timeout:0};cfg=$.extend(cfg,g?{over:f,out:g}:f);var cX,cY,pX,pY;var track=function(ev){cX=ev.pageX;cY=ev.pageY;};var compare=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);if((Math.abs(pX-cX)+Math.abs(pY-cY))<cfg.sensitivity){$(ob).unbind("mousemove",track);ob.hoverIntent_s=1;return cfg.over.apply(ob,[ev]);}else{pX=cX;pY=cY;ob.hoverIntent_t=setTimeout(function(){compare(ev,ob);},cfg.interval);}};var delay=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);ob.hoverIntent_s=0;return cfg.out.apply(ob,[ev]);};var handleHover=function(e){var p=(e.type=="mouseover"?e.fromElement:e.toElement)||e.relatedTarget;while(p&&p!=this){try{p=p.parentNode;}catch(e){p=this;}}if(p==this){return false;}var ev=jQuery.extend({},e);var ob=this;if(ob.hoverIntent_t){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);}if(e.type=="mouseover"){pX=ev.pageX;pY=ev.pageY;$(ob).bind("mousemove",track);if(ob.hoverIntent_s!=1){ob.hoverIntent_t=setTimeout(function(){compare(ev,ob);},cfg.interval);}}else{$(ob).unbind("mousemove",track);if(ob.hoverIntent_s==1){ob.hoverIntent_t=setTimeout(function(){delay(ev,ob);},cfg.timeout);}}};return this.mouseover(handleHover).mouseout(handleHover);};})(jQuery);

 $(document).ready(function() {
	

	function megaHoverOver(){
		$(this).find("a").first().addClass('hoverthis');
		$(this).find("ul").show();
			
			var offset1 = $('#navpos').offset();
			var offset2 = $(this).offset();
						
			var t1 = offset1.left + $('#navpos').outerWidth();
			var t2 = offset2.left + $(this).find("ul").width() + 30;
			var left = (t1 - t2);
			
			if (t1 < t2) {
			$('ul#megamenu li ul').css( {'left': left});
			}
			
		// End
		
		
	}
	
	function megaHoverOut(){ 
	$(this).find("a").first().removeClass('hoverthis');
	  $("ul#megamenu li ul").css( {'left': 0});
	  $("ul#megamenu li ul").hide();
	
	 
	}


	var config = {    
		 sensitivity: 2, // number = sensitivity threshold (must be 1 or higher)    
		 interval: 100, // number = milliseconds for onMouseOver polling interval    
		 over: megaHoverOver, // function = onMouseOver callback (REQUIRED)    
		 timeout: 100, // number = milliseconds delay before onMouseOut    
		 out: megaHoverOut // function = onMouseOut callback (REQUIRED)    
	};
	
	// Hide the sub first
	
	$("ul#megamenu li ul").hide();
	
	// If nested ul exists, float suv li's 
	
	
	$("ul#megamenu li ul li:has(ul)").css( {"float": "left"});
	
	$("ul#megamenu li ul li:has(ul)").find("a:first").css( {"color": "#000","font-weight": "bold"});

	
	// Set up the hover
	
	$("ul#megamenu li").not("ul#megamenu li ul li").hoverIntent(config);
	



});