<script id = "race25a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race25={};
	myVars.races.race25.varName="Function[18928].guid";
	myVars.races.race25.varType="@varType@";
	myVars.races.race25.repairType = "@RepairType";
	myVars.races.race25.event1={};
	myVars.races.race25.event2={};
	myVars.races.race25.event1.id = "nav_about_us";
	myVars.races.race25.event1.type = "onmouseover";
	myVars.races.race25.event1.loc = "nav_about_us_LOC";
	myVars.races.race25.event1.isRead = "False";
	myVars.races.race25.event1.eventType = "@event1EventType@";
	myVars.races.race25.event2.id = "nav_corporate_responsibility";
	myVars.races.race25.event2.type = "onmouseover";
	myVars.races.race25.event2.loc = "nav_corporate_responsibility_LOC";
	myVars.races.race25.event2.isRead = "True";
	myVars.races.race25.event2.eventType = "@event2EventType@";
	myVars.races.race25.event1.executed= false;// true to disable, false to enable
	myVars.races.race25.event2.executed= false;// true to disable, false to enable
</script>

