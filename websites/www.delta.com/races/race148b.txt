<script id = "race148b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race148={};
	myVars.races.race148.varName="Object[45615].focus";
	myVars.races.race148.varType="@varType@";
	myVars.races.race148.repairType = "@RepairType";
	myVars.races.race148.event1={};
	myVars.races.race148.event2={};
	myVars.races.race148.event1.id = "passwordType";
	myVars.races.race148.event1.type = "onfocus";
	myVars.races.race148.event1.loc = "passwordType_LOC";
	myVars.races.race148.event1.isRead = "True";
	myVars.races.race148.event1.eventType = "@event1EventType@";
	myVars.races.race148.event2.id = "searchTextHeader";
	myVars.races.race148.event2.type = "onfocus";
	myVars.races.race148.event2.loc = "searchTextHeader_LOC";
	myVars.races.race148.event2.isRead = "False";
	myVars.races.race148.event2.eventType = "@event2EventType@";
	myVars.races.race148.event1.executed= false;// true to disable, false to enable
	myVars.races.race148.event2.executed= false;// true to disable, false to enable
</script>

