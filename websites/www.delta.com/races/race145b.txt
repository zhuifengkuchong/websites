<script id = "race145b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race145={};
	myVars.races.race145.varName="Object[45615].focus";
	myVars.races.race145.varType="@varType@";
	myVars.races.race145.repairType = "@RepairType";
	myVars.races.race145.event1={};
	myVars.races.race145.event2={};
	myVars.races.race145.event1.id = "Lu_Id_button_1";
	myVars.races.race145.event1.type = "onfocus";
	myVars.races.race145.event1.loc = "Lu_Id_button_1_LOC";
	myVars.races.race145.event1.isRead = "True";
	myVars.races.race145.event1.eventType = "@event1EventType@";
	myVars.races.race145.event2.id = "searchTextHeader";
	myVars.races.race145.event2.type = "onfocus";
	myVars.races.race145.event2.loc = "searchTextHeader_LOC";
	myVars.races.race145.event2.isRead = "False";
	myVars.races.race145.event2.eventType = "@event2EventType@";
	myVars.races.race145.event1.executed= false;// true to disable, false to enable
	myVars.races.race145.event2.executed= false;// true to disable, false to enable
</script>

