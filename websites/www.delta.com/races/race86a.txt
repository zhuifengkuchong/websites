<script id = "race86a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race86={};
	myVars.races.race86.varName="awards_displayPreferredOnly__onfocus";
	myVars.races.race86.varType="@varType@";
	myVars.races.race86.repairType = "@RepairType";
	myVars.races.race86.event1={};
	myVars.races.race86.event2={};
	myVars.races.race86.event1.id = "Lu_DOM";
	myVars.races.race86.event1.type = "onDOMContentLoaded";
	myVars.races.race86.event1.loc = "Lu_DOM_LOC";
	myVars.races.race86.event1.isRead = "False";
	myVars.races.race86.event1.eventType = "@event1EventType@";
	myVars.races.race86.event2.id = "awards_displayPreferredOnly";
	myVars.races.race86.event2.type = "onfocus";
	myVars.races.race86.event2.loc = "awards_displayPreferredOnly_LOC";
	myVars.races.race86.event2.isRead = "True";
	myVars.races.race86.event2.eventType = "@event2EventType@";
	myVars.races.race86.event1.executed= false;// true to disable, false to enable
	myVars.races.race86.event2.executed= false;// true to disable, false to enable
</script>

