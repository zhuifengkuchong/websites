<script id = "race206b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race206={};
	myVars.races.race206.varName="Object[45615].focus";
	myVars.races.race206.varType="@varType@";
	myVars.races.race206.repairType = "@RepairType";
	myVars.races.race206.event1={};
	myVars.races.race206.event2={};
	myVars.races.race206.event1.id = "deltaOnly";
	myVars.races.race206.event1.type = "onfocus";
	myVars.races.race206.event1.loc = "deltaOnly_LOC";
	myVars.races.race206.event1.isRead = "True";
	myVars.races.race206.event1.eventType = "@event1EventType@";
	myVars.races.race206.event2.id = "searchTextHeader";
	myVars.races.race206.event2.type = "onfocus";
	myVars.races.race206.event2.loc = "searchTextHeader_LOC";
	myVars.races.race206.event2.isRead = "False";
	myVars.races.race206.event2.eventType = "@event2EventType@";
	myVars.races.race206.event1.executed= false;// true to disable, false to enable
	myVars.races.race206.event2.executed= false;// true to disable, false to enable
</script>

