<script id = "race90b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race90={};
	myVars.races.race90.varName="hiddenFieldsId__onfocus";
	myVars.races.race90.varType="@varType@";
	myVars.races.race90.repairType = "@RepairType";
	myVars.races.race90.event1={};
	myVars.races.race90.event2={};
	myVars.races.race90.event1.id = "hiddenFieldsId";
	myVars.races.race90.event1.type = "onfocus";
	myVars.races.race90.event1.loc = "hiddenFieldsId_LOC";
	myVars.races.race90.event1.isRead = "True";
	myVars.races.race90.event1.eventType = "@event1EventType@";
	myVars.races.race90.event2.id = "Lu_DOM";
	myVars.races.race90.event2.type = "onDOMContentLoaded";
	myVars.races.race90.event2.loc = "Lu_DOM_LOC";
	myVars.races.race90.event2.isRead = "False";
	myVars.races.race90.event2.eventType = "@event2EventType@";
	myVars.races.race90.event1.executed= false;// true to disable, false to enable
	myVars.races.race90.event2.executed= false;// true to disable, false to enable
</script>

