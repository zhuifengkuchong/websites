<script id = "race186a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race186={};
	myVars.races.race186.varName="Object[45615].focus";
	myVars.races.race186.varType="@varType@";
	myVars.races.race186.repairType = "@RepairType";
	myVars.races.race186.event1={};
	myVars.races.race186.event2={};
	myVars.races.race186.event1.id = "searchTextHeader";
	myVars.races.race186.event1.type = "onfocus";
	myVars.races.race186.event1.loc = "searchTextHeader_LOC";
	myVars.races.race186.event1.isRead = "False";
	myVars.races.race186.event1.eventType = "@event1EventType@";
	myVars.races.race186.event2.id = "cardNo";
	myVars.races.race186.event2.type = "onfocus";
	myVars.races.race186.event2.loc = "cardNo_LOC";
	myVars.races.race186.event2.isRead = "True";
	myVars.races.race186.event2.eventType = "@event2EventType@";
	myVars.races.race186.event1.executed= false;// true to disable, false to enable
	myVars.races.race186.event2.executed= false;// true to disable, false to enable
</script>

