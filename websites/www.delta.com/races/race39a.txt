<script id = "race39a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race39={};
	myVars.races.race39.varName="Lu_Id_input_14__onfocus";
	myVars.races.race39.varType="@varType@";
	myVars.races.race39.repairType = "@RepairType";
	myVars.races.race39.event1={};
	myVars.races.race39.event2={};
	myVars.races.race39.event1.id = "Lu_DOM";
	myVars.races.race39.event1.type = "onDOMContentLoaded";
	myVars.races.race39.event1.loc = "Lu_DOM_LOC";
	myVars.races.race39.event1.isRead = "False";
	myVars.races.race39.event1.eventType = "@event1EventType@";
	myVars.races.race39.event2.id = "Lu_Id_input_14";
	myVars.races.race39.event2.type = "onfocus";
	myVars.races.race39.event2.loc = "Lu_Id_input_14_LOC";
	myVars.races.race39.event2.isRead = "True";
	myVars.races.race39.event2.eventType = "@event2EventType@";
	myVars.races.race39.event1.executed= false;// true to disable, false to enable
	myVars.races.race39.event2.executed= false;// true to disable, false to enable
</script>

