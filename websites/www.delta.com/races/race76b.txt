<script id = "race76b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race76={};
	myVars.races.race76.varName="departureTime__onfocus";
	myVars.races.race76.varType="@varType@";
	myVars.races.race76.repairType = "@RepairType";
	myVars.races.race76.event1={};
	myVars.races.race76.event2={};
	myVars.races.race76.event1.id = "departureTime";
	myVars.races.race76.event1.type = "onfocus";
	myVars.races.race76.event1.loc = "departureTime_LOC";
	myVars.races.race76.event1.isRead = "True";
	myVars.races.race76.event1.eventType = "@event1EventType@";
	myVars.races.race76.event2.id = "Lu_DOM";
	myVars.races.race76.event2.type = "onDOMContentLoaded";
	myVars.races.race76.event2.loc = "Lu_DOM_LOC";
	myVars.races.race76.event2.isRead = "False";
	myVars.races.race76.event2.eventType = "@event2EventType@";
	myVars.races.race76.event1.executed= false;// true to disable, false to enable
	myVars.races.race76.event2.executed= false;// true to disable, false to enable
</script>

