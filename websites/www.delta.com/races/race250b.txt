<script id = "race250b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race250={};
	myVars.races.race250.varName="Object[45615].focus";
	myVars.races.race250.varType="@varType@";
	myVars.races.race250.repairType = "@RepairType";
	myVars.races.race250.event1={};
	myVars.races.race250.event2={};
	myVars.races.race250.event1.id = "__checkbox_pol_widget_isRequestUpgrade";
	myVars.races.race250.event1.type = "onfocus";
	myVars.races.race250.event1.loc = "__checkbox_pol_widget_isRequestUpgrade_LOC";
	myVars.races.race250.event1.isRead = "True";
	myVars.races.race250.event1.eventType = "@event1EventType@";
	myVars.races.race250.event2.id = "searchTextHeader";
	myVars.races.race250.event2.type = "onfocus";
	myVars.races.race250.event2.loc = "searchTextHeader_LOC";
	myVars.races.race250.event2.isRead = "False";
	myVars.races.race250.event2.eventType = "@event2EventType@";
	myVars.races.race250.event1.executed= false;// true to disable, false to enable
	myVars.races.race250.event2.executed= false;// true to disable, false to enable
</script>

