<script id = "race118a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race118={};
	myVars.races.race118.varName="hotelWidgetSearchType__onfocus";
	myVars.races.race118.varType="@varType@";
	myVars.races.race118.repairType = "@RepairType";
	myVars.races.race118.event1={};
	myVars.races.race118.event2={};
	myVars.races.race118.event1.id = "Lu_DOM";
	myVars.races.race118.event1.type = "onDOMContentLoaded";
	myVars.races.race118.event1.loc = "Lu_DOM_LOC";
	myVars.races.race118.event1.isRead = "False";
	myVars.races.race118.event1.eventType = "@event1EventType@";
	myVars.races.race118.event2.id = "hotelWidgetSearchType";
	myVars.races.race118.event2.type = "onfocus";
	myVars.races.race118.event2.loc = "hotelWidgetSearchType_LOC";
	myVars.races.race118.event2.isRead = "True";
	myVars.races.race118.event2.eventType = "@event2EventType@";
	myVars.races.race118.event1.executed= false;// true to disable, false to enable
	myVars.races.race118.event2.executed= false;// true to disable, false to enable
</script>

