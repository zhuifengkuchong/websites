<script id = "race187a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race187={};
	myVars.races.race187.varName="Object[45615].focus";
	myVars.races.race187.varType="@varType@";
	myVars.races.race187.repairType = "@RepairType";
	myVars.races.race187.event1={};
	myVars.races.race187.event2={};
	myVars.races.race187.event1.id = "searchTextHeader";
	myVars.races.race187.event1.type = "onfocus";
	myVars.races.race187.event1.loc = "searchTextHeader_LOC";
	myVars.races.race187.event1.isRead = "False";
	myVars.races.race187.event1.eventType = "@event1EventType@";
	myVars.races.race187.event2.id = "creditCardButton";
	myVars.races.race187.event2.type = "onfocus";
	myVars.races.race187.event2.loc = "creditCardButton_LOC";
	myVars.races.race187.event2.isRead = "True";
	myVars.races.race187.event2.eventType = "@event2EventType@";
	myVars.races.race187.event1.executed= false;// true to disable, false to enable
	myVars.races.race187.event2.executed= false;// true to disable, false to enable
</script>

