<script id = "race91b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race91={};
	myVars.races.race91.varName="awards_iamtravelling__onfocus";
	myVars.races.race91.varType="@varType@";
	myVars.races.race91.repairType = "@RepairType";
	myVars.races.race91.event1={};
	myVars.races.race91.event2={};
	myVars.races.race91.event1.id = "awards_iamtravelling";
	myVars.races.race91.event1.type = "onfocus";
	myVars.races.race91.event1.loc = "awards_iamtravelling_LOC";
	myVars.races.race91.event1.isRead = "True";
	myVars.races.race91.event1.eventType = "@event1EventType@";
	myVars.races.race91.event2.id = "Lu_DOM";
	myVars.races.race91.event2.type = "onDOMContentLoaded";
	myVars.races.race91.event2.loc = "Lu_DOM_LOC";
	myVars.races.race91.event2.isRead = "False";
	myVars.races.race91.event2.eventType = "@event2EventType@";
	myVars.races.race91.event1.executed= false;// true to disable, false to enable
	myVars.races.race91.event2.executed= false;// true to disable, false to enable
</script>

