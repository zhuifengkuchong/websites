<script id = "race80b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race80={};
	myVars.races.race80.varName="fareBundle__onfocus";
	myVars.races.race80.varType="@varType@";
	myVars.races.race80.repairType = "@RepairType";
	myVars.races.race80.event1={};
	myVars.races.race80.event2={};
	myVars.races.race80.event1.id = "fareBundle";
	myVars.races.race80.event1.type = "onfocus";
	myVars.races.race80.event1.loc = "fareBundle_LOC";
	myVars.races.race80.event1.isRead = "True";
	myVars.races.race80.event1.eventType = "@event1EventType@";
	myVars.races.race80.event2.id = "Lu_DOM";
	myVars.races.race80.event2.type = "onDOMContentLoaded";
	myVars.races.race80.event2.loc = "Lu_DOM_LOC";
	myVars.races.race80.event2.isRead = "False";
	myVars.races.race80.event2.eventType = "@event2EventType@";
	myVars.races.race80.event1.executed= false;// true to disable, false to enable
	myVars.races.race80.event2.executed= false;// true to disable, false to enable
</script>

