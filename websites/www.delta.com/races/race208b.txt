<script id = "race208b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race208={};
	myVars.races.race208.varName="Object[45615].focus";
	myVars.races.race208.varType="@varType@";
	myVars.races.race208.repairType = "@RepairType";
	myVars.races.race208.event1={};
	myVars.races.race208.event2={};
	myVars.races.race208.event1.id = "departureTime";
	myVars.races.race208.event1.type = "onfocus";
	myVars.races.race208.event1.loc = "departureTime_LOC";
	myVars.races.race208.event1.isRead = "True";
	myVars.races.race208.event1.eventType = "@event1EventType@";
	myVars.races.race208.event2.id = "searchTextHeader";
	myVars.races.race208.event2.type = "onfocus";
	myVars.races.race208.event2.loc = "searchTextHeader_LOC";
	myVars.races.race208.event2.isRead = "False";
	myVars.races.race208.event2.eventType = "@event2EventType@";
	myVars.races.race208.event1.executed= false;// true to disable, false to enable
	myVars.races.race208.event2.executed= false;// true to disable, false to enable
</script>

