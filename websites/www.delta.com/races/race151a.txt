<script id = "race151a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race151={};
	myVars.races.race151.varName="Object[45615].focus";
	myVars.races.race151.varType="@varType@";
	myVars.races.race151.repairType = "@RepairType";
	myVars.races.race151.event1={};
	myVars.races.race151.event2={};
	myVars.races.race151.event1.id = "searchTextHeader";
	myVars.races.race151.event1.type = "onfocus";
	myVars.races.race151.event1.loc = "searchTextHeader_LOC";
	myVars.races.race151.event1.isRead = "False";
	myVars.races.race151.event1.eventType = "@event1EventType@";
	myVars.races.race151.event2.id = "Lu_Id_input_5";
	myVars.races.race151.event2.type = "onfocus";
	myVars.races.race151.event2.loc = "Lu_Id_input_5_LOC";
	myVars.races.race151.event2.isRead = "True";
	myVars.races.race151.event2.eventType = "@event2EventType@";
	myVars.races.race151.event1.executed= false;// true to disable, false to enable
	myVars.races.race151.event2.executed= false;// true to disable, false to enable
</script>

