<script id = "race139b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race139={};
	myVars.races.race139.varName="Object[45615].keypress";
	myVars.races.race139.varType="@varType@";
	myVars.races.race139.repairType = "@RepairType";
	myVars.races.race139.event1={};
	myVars.races.race139.event2={};
	myVars.races.race139.event1.id = "escape-search-origin_bok";
	myVars.races.race139.event1.type = "onkeypress";
	myVars.races.race139.event1.loc = "escape-search-origin_bok_LOC";
	myVars.races.race139.event1.isRead = "True";
	myVars.races.race139.event1.eventType = "@event1EventType@";
	myVars.races.race139.event2.id = "nav-widget-mydelta";
	myVars.races.race139.event2.type = "onkeypress";
	myVars.races.race139.event2.loc = "nav-widget-mydelta_LOC";
	myVars.races.race139.event2.isRead = "False";
	myVars.races.race139.event2.eventType = "@event2EventType@";
	myVars.races.race139.event1.executed= false;// true to disable, false to enable
	myVars.races.race139.event2.executed= false;// true to disable, false to enable
</script>

