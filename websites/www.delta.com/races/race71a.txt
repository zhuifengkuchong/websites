<script id = "race71a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race71={};
	myVars.races.race71.varName="searchType__onfocus";
	myVars.races.race71.varType="@varType@";
	myVars.races.race71.repairType = "@RepairType";
	myVars.races.race71.event1={};
	myVars.races.race71.event2={};
	myVars.races.race71.event1.id = "Lu_DOM";
	myVars.races.race71.event1.type = "onDOMContentLoaded";
	myVars.races.race71.event1.loc = "Lu_DOM_LOC";
	myVars.races.race71.event1.isRead = "False";
	myVars.races.race71.event1.eventType = "@event1EventType@";
	myVars.races.race71.event2.id = "searchType";
	myVars.races.race71.event2.type = "onfocus";
	myVars.races.race71.event2.loc = "searchType_LOC";
	myVars.races.race71.event2.isRead = "True";
	myVars.races.race71.event2.eventType = "@event2EventType@";
	myVars.races.race71.event1.executed= false;// true to disable, false to enable
	myVars.races.race71.event2.executed= false;// true to disable, false to enable
</script>

