<script id = "race185b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race185={};
	myVars.races.race185.varName="Object[45615].focus";
	myVars.races.race185.varType="@varType@";
	myVars.races.race185.repairType = "@RepairType";
	myVars.races.race185.event1={};
	myVars.races.race185.event2={};
	myVars.races.race185.event1.id = "ccLName";
	myVars.races.race185.event1.type = "onfocus";
	myVars.races.race185.event1.loc = "ccLName_LOC";
	myVars.races.race185.event1.isRead = "True";
	myVars.races.race185.event1.eventType = "@event1EventType@";
	myVars.races.race185.event2.id = "searchTextHeader";
	myVars.races.race185.event2.type = "onfocus";
	myVars.races.race185.event2.loc = "searchTextHeader_LOC";
	myVars.races.race185.event2.isRead = "False";
	myVars.races.race185.event2.eventType = "@event2EventType@";
	myVars.races.race185.event1.executed= false;// true to disable, false to enable
	myVars.races.race185.event2.executed= false;// true to disable, false to enable
</script>

