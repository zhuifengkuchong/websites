<script id = "race126b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race126={};
	myVars.races.race126.varName="Object[45615].keydown";
	myVars.races.race126.varType="@varType@";
	myVars.races.race126.repairType = "@RepairType";
	myVars.races.race126.event1={};
	myVars.races.race126.event2={};
	myVars.races.race126.event1.id = "pwd";
	myVars.races.race126.event1.type = "onkeydown";
	myVars.races.race126.event1.loc = "pwd_LOC";
	myVars.races.race126.event1.isRead = "True";
	myVars.races.race126.event1.eventType = "@event1EventType@";
	myVars.races.race126.event2.id = "escape-mapit-from";
	myVars.races.race126.event2.type = "onkeydown";
	myVars.races.race126.event2.loc = "escape-mapit-from_LOC";
	myVars.races.race126.event2.isRead = "False";
	myVars.races.race126.event2.eventType = "@event2EventType@";
	myVars.races.race126.event1.executed= false;// true to disable, false to enable
	myVars.races.race126.event2.executed= false;// true to disable, false to enable
</script>

