<script id = "race12b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race12={};
	myVars.races.race12.varName="escape-search-origin_bok__onkeypress";
	myVars.races.race12.varType="@varType@";
	myVars.races.race12.repairType = "@RepairType";
	myVars.races.race12.event1={};
	myVars.races.race12.event2={};
	myVars.races.race12.event1.id = "escape-search-origin_bok";
	myVars.races.race12.event1.type = "onkeypress";
	myVars.races.race12.event1.loc = "escape-search-origin_bok_LOC";
	myVars.races.race12.event1.isRead = "True";
	myVars.races.race12.event1.eventType = "@event1EventType@";
	myVars.races.race12.event2.id = "Lu_Id_script_37";
	myVars.races.race12.event2.type = "Lu_Id_script_37__parsed";
	myVars.races.race12.event2.loc = "Lu_Id_script_37_LOC";
	myVars.races.race12.event2.isRead = "False";
	myVars.races.race12.event2.eventType = "@event2EventType@";
	myVars.races.race12.event1.executed= false;// true to disable, false to enable
	myVars.races.race12.event2.executed= false;// true to disable, false to enable
</script>

