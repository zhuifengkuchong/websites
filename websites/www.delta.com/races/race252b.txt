<script id = "race252b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race252={};
	myVars.races.race252.varName="Object[45615].focus";
	myVars.races.race252.varType="@varType@";
	myVars.races.race252.repairType = "@RepairType";
	myVars.races.race252.event1={};
	myVars.races.race252.event2={};
	myVars.races.race252.event1.id = "hotelWidgetSearchType";
	myVars.races.race252.event1.type = "onfocus";
	myVars.races.race252.event1.loc = "hotelWidgetSearchType_LOC";
	myVars.races.race252.event1.isRead = "True";
	myVars.races.race252.event1.eventType = "@event1EventType@";
	myVars.races.race252.event2.id = "searchTextHeader";
	myVars.races.race252.event2.type = "onfocus";
	myVars.races.race252.event2.loc = "searchTextHeader_LOC";
	myVars.races.race252.event2.isRead = "False";
	myVars.races.race252.event2.eventType = "@event2EventType@";
	myVars.races.race252.event1.executed= false;// true to disable, false to enable
	myVars.races.race252.event2.executed= false;// true to disable, false to enable
</script>

