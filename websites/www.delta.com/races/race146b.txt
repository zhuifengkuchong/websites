<script id = "race146b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race146={};
	myVars.races.race146.varName="Object[45615].focus";
	myVars.races.race146.varType="@varType@";
	myVars.races.race146.repairType = "@RepairType";
	myVars.races.race146.event1={};
	myVars.races.race146.event2={};
	myVars.races.race146.event1.id = "Lu_Id_input_2";
	myVars.races.race146.event1.type = "onfocus";
	myVars.races.race146.event1.loc = "Lu_Id_input_2_LOC";
	myVars.races.race146.event1.isRead = "True";
	myVars.races.race146.event1.eventType = "@event1EventType@";
	myVars.races.race146.event2.id = "searchTextHeader";
	myVars.races.race146.event2.type = "onfocus";
	myVars.races.race146.event2.loc = "searchTextHeader_LOC";
	myVars.races.race146.event2.isRead = "False";
	myVars.races.race146.event2.eventType = "@event2EventType@";
	myVars.races.race146.event1.executed= false;// true to disable, false to enable
	myVars.races.race146.event2.executed= false;// true to disable, false to enable
</script>

