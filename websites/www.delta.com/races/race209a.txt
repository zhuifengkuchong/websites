<script id = "race209a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race209={};
	myVars.races.race209.varName="Object[45615].focus";
	myVars.races.race209.varType="@varType@";
	myVars.races.race209.repairType = "@RepairType";
	myVars.races.race209.event1={};
	myVars.races.race209.event2={};
	myVars.races.race209.event1.id = "searchTextHeader";
	myVars.races.race209.event1.type = "onfocus";
	myVars.races.race209.event1.loc = "searchTextHeader_LOC";
	myVars.races.race209.event1.isRead = "False";
	myVars.races.race209.event1.eventType = "@event1EventType@";
	myVars.races.race209.event2.id = "returnTime";
	myVars.races.race209.event2.type = "onfocus";
	myVars.races.race209.event2.loc = "returnTime_LOC";
	myVars.races.race209.event2.isRead = "True";
	myVars.races.race209.event2.eventType = "@event2EventType@";
	myVars.races.race209.event1.executed= false;// true to disable, false to enable
	myVars.races.race209.event2.executed= false;// true to disable, false to enable
</script>

