<script id = "race68a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race68={};
	myVars.races.race68.varName="invalidCCNumber__onfocus";
	myVars.races.race68.varType="@varType@";
	myVars.races.race68.repairType = "@RepairType";
	myVars.races.race68.event1={};
	myVars.races.race68.event2={};
	myVars.races.race68.event1.id = "Lu_DOM";
	myVars.races.race68.event1.type = "onDOMContentLoaded";
	myVars.races.race68.event1.loc = "Lu_DOM_LOC";
	myVars.races.race68.event1.isRead = "False";
	myVars.races.race68.event1.eventType = "@event1EventType@";
	myVars.races.race68.event2.id = "invalidCCNumber";
	myVars.races.race68.event2.type = "onfocus";
	myVars.races.race68.event2.loc = "invalidCCNumber_LOC";
	myVars.races.race68.event2.isRead = "True";
	myVars.races.race68.event2.eventType = "@event2EventType@";
	myVars.races.race68.event1.executed= false;// true to disable, false to enable
	myVars.races.race68.event2.executed= false;// true to disable, false to enable
</script>

