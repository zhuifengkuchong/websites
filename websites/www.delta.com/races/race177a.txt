<script id = "race177a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race177={};
	myVars.races.race177.varName="Object[45615].focus";
	myVars.races.race177.varType="@varType@";
	myVars.races.race177.repairType = "@RepairType";
	myVars.races.race177.event1={};
	myVars.races.race177.event2={};
	myVars.races.race177.event1.id = "searchTextHeader";
	myVars.races.race177.event1.type = "onfocus";
	myVars.races.race177.event1.loc = "searchTextHeader_LOC";
	myVars.races.race177.event1.isRead = "False";
	myVars.races.race177.event1.eventType = "@event1EventType@";
	myVars.races.race177.event2.id = "tab";
	myVars.races.race177.event2.type = "onfocus";
	myVars.races.race177.event2.loc = "tab_LOC";
	myVars.races.race177.event2.isRead = "True";
	myVars.races.race177.event2.eventType = "@event2EventType@";
	myVars.races.race177.event1.executed= false;// true to disable, false to enable
	myVars.races.race177.event2.executed= false;// true to disable, false to enable
</script>

