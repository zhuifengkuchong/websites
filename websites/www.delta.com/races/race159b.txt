<script id = "race159b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race159={};
	myVars.races.race159.varName="Object[45615].focus";
	myVars.races.race159.varType="@varType@";
	myVars.races.race159.repairType = "@RepairType";
	myVars.races.race159.event1={};
	myVars.races.race159.event2={};
	myVars.races.race159.event1.id = "pwd";
	myVars.races.race159.event1.type = "onfocus";
	myVars.races.race159.event1.loc = "pwd_LOC";
	myVars.races.race159.event1.isRead = "True";
	myVars.races.race159.event1.eventType = "@event1EventType@";
	myVars.races.race159.event2.id = "searchTextHeader";
	myVars.races.race159.event2.type = "onfocus";
	myVars.races.race159.event2.loc = "searchTextHeader_LOC";
	myVars.races.race159.event2.isRead = "False";
	myVars.races.race159.event2.eventType = "@event2EventType@";
	myVars.races.race159.event1.executed= false;// true to disable, false to enable
	myVars.races.race159.event2.executed= false;// true to disable, false to enable
</script>

