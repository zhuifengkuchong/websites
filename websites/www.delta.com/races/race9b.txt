<script id = "race9b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race9={};
	myVars.races.race9.varName="nav-widget-escape__onkeypress";
	myVars.races.race9.varType="@varType@";
	myVars.races.race9.repairType = "@RepairType";
	myVars.races.race9.event1={};
	myVars.races.race9.event2={};
	myVars.races.race9.event1.id = "nav-widget-escape";
	myVars.races.race9.event1.type = "onkeypress";
	myVars.races.race9.event1.loc = "nav-widget-escape_LOC";
	myVars.races.race9.event1.isRead = "True";
	myVars.races.race9.event1.eventType = "@event1EventType@";
	myVars.races.race9.event2.id = "Lu_Id_script_37";
	myVars.races.race9.event2.type = "Lu_Id_script_37__parsed";
	myVars.races.race9.event2.loc = "Lu_Id_script_37_LOC";
	myVars.races.race9.event2.isRead = "False";
	myVars.races.race9.event2.eventType = "@event2EventType@";
	myVars.races.race9.event1.executed= false;// true to disable, false to enable
	myVars.races.race9.event2.executed= false;// true to disable, false to enable
</script>

