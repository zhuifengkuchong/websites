<script id = "race129a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race129={};
	myVars.races.race129.varName="Object[45615].keyup";
	myVars.races.race129.varType="@varType@";
	myVars.races.race129.repairType = "@RepairType";
	myVars.races.race129.event1={};
	myVars.races.race129.event2={};
	myVars.races.race129.event1.id = "pwd";
	myVars.races.race129.event1.type = "onkeyup";
	myVars.races.race129.event1.loc = "pwd_LOC";
	myVars.races.race129.event1.isRead = "False";
	myVars.races.race129.event1.eventType = "@event1EventType@";
	myVars.races.race129.event2.id = "departureDate";
	myVars.races.race129.event2.type = "onkeyup";
	myVars.races.race129.event2.loc = "departureDate_LOC";
	myVars.races.race129.event2.isRead = "True";
	myVars.races.race129.event2.eventType = "@event2EventType@";
	myVars.races.race129.event1.executed= false;// true to disable, false to enable
	myVars.races.race129.event2.executed= false;// true to disable, false to enable
</script>

