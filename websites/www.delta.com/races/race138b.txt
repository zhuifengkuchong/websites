<script id = "race138b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race138={};
	myVars.races.race138.varName="Function[44064]..airporterror:visible ";
	myVars.races.race138.varType="@varType@";
	myVars.races.race138.repairType = "@RepairType";
	myVars.races.race138.event1={};
	myVars.races.race138.event2={};
	myVars.races.race138.event1.id = "escape-search-destination";
	myVars.races.race138.event1.type = "onkeypress";
	myVars.races.race138.event1.loc = "escape-search-destination_LOC";
	myVars.races.race138.event1.isRead = "True";
	myVars.races.race138.event1.eventType = "@event1EventType@";
	myVars.races.race138.event2.id = "escape-mapit-from";
	myVars.races.race138.event2.type = "onkeydown";
	myVars.races.race138.event2.loc = "escape-mapit-from_LOC";
	myVars.races.race138.event2.isRead = "False";
	myVars.races.race138.event2.eventType = "@event2EventType@";
	myVars.races.race138.event1.executed= false;// true to disable, false to enable
	myVars.races.race138.event2.executed= false;// true to disable, false to enable
</script>

