<script id = "race231a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race231={};
	myVars.races.race231.varName="Object[45615].focus";
	myVars.races.race231.varType="@varType@";
	myVars.races.race231.repairType = "@RepairType";
	myVars.races.race231.event1={};
	myVars.races.race231.event2={};
	myVars.races.race231.event1.id = "searchTextHeader";
	myVars.races.race231.event1.type = "onfocus";
	myVars.races.race231.event1.loc = "searchTextHeader_LOC";
	myVars.races.race231.event1.isRead = "False";
	myVars.races.race231.event1.eventType = "@event1EventType@";
	myVars.races.race231.event2.id = "departureDate";
	myVars.races.race231.event2.type = "onfocus";
	myVars.races.race231.event2.loc = "departureDate_LOC";
	myVars.races.race231.event2.isRead = "True";
	myVars.races.race231.event2.eventType = "@event2EventType@";
	myVars.races.race231.event1.executed= false;// true to disable, false to enable
	myVars.races.race231.event2.executed= false;// true to disable, false to enable
</script>

