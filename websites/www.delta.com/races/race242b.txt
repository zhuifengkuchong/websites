<script id = "race242b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race242={};
	myVars.races.race242.varName="Object[45615].focus";
	myVars.races.race242.varType="@varType@";
	myVars.races.race242.repairType = "@RepairType";
	myVars.races.race242.event1={};
	myVars.races.race242.event2={};
	myVars.races.race242.event1.id = "cabinFareClass";
	myVars.races.race242.event1.type = "onfocus";
	myVars.races.race242.event1.loc = "cabinFareClass_LOC";
	myVars.races.race242.event1.isRead = "True";
	myVars.races.race242.event1.eventType = "@event1EventType@";
	myVars.races.race242.event2.id = "searchTextHeader";
	myVars.races.race242.event2.type = "onfocus";
	myVars.races.race242.event2.loc = "searchTextHeader_LOC";
	myVars.races.race242.event2.isRead = "False";
	myVars.races.race242.event2.eventType = "@event2EventType@";
	myVars.races.race242.event1.executed= false;// true to disable, false to enable
	myVars.races.race242.event2.executed= false;// true to disable, false to enable
</script>

