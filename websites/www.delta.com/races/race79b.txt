<script id = "race79b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race79={};
	myVars.races.race79.varName="dispatchMethod__onfocus";
	myVars.races.race79.varType="@varType@";
	myVars.races.race79.repairType = "@RepairType";
	myVars.races.race79.event1={};
	myVars.races.race79.event2={};
	myVars.races.race79.event1.id = "dispatchMethod";
	myVars.races.race79.event1.type = "onfocus";
	myVars.races.race79.event1.loc = "dispatchMethod_LOC";
	myVars.races.race79.event1.isRead = "True";
	myVars.races.race79.event1.eventType = "@event1EventType@";
	myVars.races.race79.event2.id = "Lu_DOM";
	myVars.races.race79.event2.type = "onDOMContentLoaded";
	myVars.races.race79.event2.loc = "Lu_DOM_LOC";
	myVars.races.race79.event2.isRead = "False";
	myVars.races.race79.event2.eventType = "@event2EventType@";
	myVars.races.race79.event1.executed= false;// true to disable, false to enable
	myVars.races.race79.event2.executed= false;// true to disable, false to enable
</script>

