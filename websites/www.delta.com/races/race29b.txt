<script id = "race29b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race29={};
	myVars.races.race29.varName="usernm__onfocus";
	myVars.races.race29.varType="@varType@";
	myVars.races.race29.repairType = "@RepairType";
	myVars.races.race29.event1={};
	myVars.races.race29.event2={};
	myVars.races.race29.event1.id = "usernm";
	myVars.races.race29.event1.type = "onfocus";
	myVars.races.race29.event1.loc = "usernm_LOC";
	myVars.races.race29.event1.isRead = "True";
	myVars.races.race29.event1.eventType = "@event1EventType@";
	myVars.races.race29.event2.id = "Lu_DOM";
	myVars.races.race29.event2.type = "onDOMContentLoaded";
	myVars.races.race29.event2.loc = "Lu_DOM_LOC";
	myVars.races.race29.event2.isRead = "False";
	myVars.races.race29.event2.eventType = "@event2EventType@";
	myVars.races.race29.event1.executed= false;// true to disable, false to enable
	myVars.races.race29.event2.executed= false;// true to disable, false to enable
</script>

