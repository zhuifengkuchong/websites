<script id = "race40b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race40={};
	myVars.races.race40.varName="BAUParams2__onfocus";
	myVars.races.race40.varType="@varType@";
	myVars.races.race40.repairType = "@RepairType";
	myVars.races.race40.event1={};
	myVars.races.race40.event2={};
	myVars.races.race40.event1.id = "BAUParams2";
	myVars.races.race40.event1.type = "onfocus";
	myVars.races.race40.event1.loc = "BAUParams2_LOC";
	myVars.races.race40.event1.isRead = "True";
	myVars.races.race40.event1.eventType = "@event1EventType@";
	myVars.races.race40.event2.id = "Lu_DOM";
	myVars.races.race40.event2.type = "onDOMContentLoaded";
	myVars.races.race40.event2.loc = "Lu_DOM_LOC";
	myVars.races.race40.event2.isRead = "False";
	myVars.races.race40.event2.eventType = "@event2EventType@";
	myVars.races.race40.event1.executed= false;// true to disable, false to enable
	myVars.races.race40.event2.executed= false;// true to disable, false to enable
</script>

