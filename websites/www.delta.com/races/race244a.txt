<script id = "race244a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race244={};
	myVars.races.race244.varName="Object[45615].focus";
	myVars.races.race244.varType="@varType@";
	myVars.races.race244.repairType = "@RepairType";
	myVars.races.race244.event1={};
	myVars.races.race244.event2={};
	myVars.races.race244.event1.id = "searchTextHeader";
	myVars.races.race244.event1.type = "onfocus";
	myVars.races.race244.event1.loc = "searchTextHeader_LOC";
	myVars.races.race244.event1.isRead = "False";
	myVars.races.race244.event1.eventType = "@event1EventType@";
	myVars.races.race244.event2.id = "milesBtn";
	myVars.races.race244.event2.type = "onfocus";
	myVars.races.race244.event2.loc = "milesBtn_LOC";
	myVars.races.race244.event2.isRead = "True";
	myVars.races.race244.event2.eventType = "@event2EventType@";
	myVars.races.race244.event1.executed= false;// true to disable, false to enable
	myVars.races.race244.event2.executed= false;// true to disable, false to enable
</script>

