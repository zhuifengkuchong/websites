<script id = "race180a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race180={};
	myVars.races.race180.varName="Object[45615].focus";
	myVars.races.race180.varType="@varType@";
	myVars.races.race180.repairType = "@RepairType";
	myVars.races.race180.event1={};
	myVars.races.race180.event2={};
	myVars.races.race180.event1.id = "searchTextHeader";
	myVars.races.race180.event1.type = "onfocus";
	myVars.races.race180.event1.loc = "searchTextHeader_LOC";
	myVars.races.race180.event1.isRead = "False";
	myVars.races.race180.event1.eventType = "@event1EventType@";
	myVars.races.race180.event2.id = "confNo";
	myVars.races.race180.event2.type = "onfocus";
	myVars.races.race180.event2.loc = "confNo_LOC";
	myVars.races.race180.event2.isRead = "True";
	myVars.races.race180.event2.eventType = "@event2EventType@";
	myVars.races.race180.event1.executed= false;// true to disable, false to enable
	myVars.races.race180.event2.executed= false;// true to disable, false to enable
</script>

