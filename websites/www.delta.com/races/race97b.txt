<script id = "race97b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race97={};
	myVars.races.race97.varName="originCity__onfocus";
	myVars.races.race97.varType="@varType@";
	myVars.races.race97.repairType = "@RepairType";
	myVars.races.race97.event1={};
	myVars.races.race97.event2={};
	myVars.races.race97.event1.id = "originCity";
	myVars.races.race97.event1.type = "onfocus";
	myVars.races.race97.event1.loc = "originCity_LOC";
	myVars.races.race97.event1.isRead = "True";
	myVars.races.race97.event1.eventType = "@event1EventType@";
	myVars.races.race97.event2.id = "Lu_DOM";
	myVars.races.race97.event2.type = "onDOMContentLoaded";
	myVars.races.race97.event2.loc = "Lu_DOM_LOC";
	myVars.races.race97.event2.isRead = "False";
	myVars.races.race97.event2.eventType = "@event2EventType@";
	myVars.races.race97.event1.executed= false;// true to disable, false to enable
	myVars.races.race97.event2.executed= false;// true to disable, false to enable
</script>

