<script id = "race150a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race150={};
	myVars.races.race150.varName="Object[45615].focus";
	myVars.races.race150.varType="@varType@";
	myVars.races.race150.repairType = "@RepairType";
	myVars.races.race150.event1={};
	myVars.races.race150.event2={};
	myVars.races.race150.event1.id = "searchTextHeader";
	myVars.races.race150.event1.type = "onfocus";
	myVars.races.race150.event1.loc = "searchTextHeader_LOC";
	myVars.races.race150.event1.isRead = "False";
	myVars.races.race150.event1.eventType = "@event1EventType@";
	myVars.races.race150.event2.id = "Lu_Id_input_4";
	myVars.races.race150.event2.type = "onfocus";
	myVars.races.race150.event2.loc = "Lu_Id_input_4_LOC";
	myVars.races.race150.event2.isRead = "True";
	myVars.races.race150.event2.eventType = "@event2EventType@";
	myVars.races.race150.event1.executed= false;// true to disable, false to enable
	myVars.races.race150.event2.executed= false;// true to disable, false to enable
</script>

