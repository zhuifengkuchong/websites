<script id = "race81b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race81={};
	myVars.races.race81.varName="flexMainRTRTravelDate__onfocus";
	myVars.races.race81.varType="@varType@";
	myVars.races.race81.repairType = "@RepairType";
	myVars.races.race81.event1={};
	myVars.races.race81.event2={};
	myVars.races.race81.event1.id = "flexMainRTRTravelDate";
	myVars.races.race81.event1.type = "onfocus";
	myVars.races.race81.event1.loc = "flexMainRTRTravelDate_LOC";
	myVars.races.race81.event1.isRead = "True";
	myVars.races.race81.event1.eventType = "@event1EventType@";
	myVars.races.race81.event2.id = "Lu_DOM";
	myVars.races.race81.event2.type = "onDOMContentLoaded";
	myVars.races.race81.event2.loc = "Lu_DOM_LOC";
	myVars.races.race81.event2.isRead = "False";
	myVars.races.race81.event2.eventType = "@event2EventType@";
	myVars.races.race81.event1.executed= false;// true to disable, false to enable
	myVars.races.race81.event2.executed= false;// true to disable, false to enable
</script>

