<script id = "race207b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race207={};
	myVars.races.race207.varName="Object[45615].focus";
	myVars.races.race207.varType="@varType@";
	myVars.races.race207.repairType = "@RepairType";
	myVars.races.race207.event1={};
	myVars.races.race207.event2={};
	myVars.races.race207.event1.id = "dl";
	myVars.races.race207.event1.type = "onfocus";
	myVars.races.race207.event1.loc = "dl_LOC";
	myVars.races.race207.event1.isRead = "True";
	myVars.races.race207.event1.eventType = "@event1EventType@";
	myVars.races.race207.event2.id = "searchTextHeader";
	myVars.races.race207.event2.type = "onfocus";
	myVars.races.race207.event2.loc = "searchTextHeader_LOC";
	myVars.races.race207.event2.isRead = "False";
	myVars.races.race207.event2.eventType = "@event2EventType@";
	myVars.races.race207.event1.executed= false;// true to disable, false to enable
	myVars.races.race207.event2.executed= false;// true to disable, false to enable
</script>

