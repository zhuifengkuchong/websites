<script id = "race116b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race116={};
	myVars.races.race116.varName="__checkbox_pol_widget_isRequestUpgrade__onfocus";
	myVars.races.race116.varType="@varType@";
	myVars.races.race116.repairType = "@RepairType";
	myVars.races.race116.event1={};
	myVars.races.race116.event2={};
	myVars.races.race116.event1.id = "__checkbox_pol_widget_isRequestUpgrade";
	myVars.races.race116.event1.type = "onfocus";
	myVars.races.race116.event1.loc = "__checkbox_pol_widget_isRequestUpgrade_LOC";
	myVars.races.race116.event1.isRead = "True";
	myVars.races.race116.event1.eventType = "@event1EventType@";
	myVars.races.race116.event2.id = "Lu_DOM";
	myVars.races.race116.event2.type = "onDOMContentLoaded";
	myVars.races.race116.event2.loc = "Lu_DOM_LOC";
	myVars.races.race116.event2.isRead = "False";
	myVars.races.race116.event2.eventType = "@event2EventType@";
	myVars.races.race116.event1.executed= false;// true to disable, false to enable
	myVars.races.race116.event2.executed= false;// true to disable, false to enable
</script>

