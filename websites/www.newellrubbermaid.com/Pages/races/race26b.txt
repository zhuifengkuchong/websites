<script id = "race26b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race26={};
	myVars.races.race26.varName="Window[26].closetimer";
	myVars.races.race26.varType="@varType@";
	myVars.races.race26.repairType = "@RepairType";
	myVars.races.race26.event1={};
	myVars.races.race26.event2={};
	myVars.races.race26.event1.id = "ctl00_mainContentPlaceHolder_dlstBrandLogo_ct1_divRollover";
	myVars.races.race26.event1.type = "onmouseout";
	myVars.races.race26.event1.loc = "ctl00_mainContentPlaceHolder_dlstBrandLogo_ct1_divRollover_LOC";
	myVars.races.race26.event1.isRead = "False";
	myVars.races.race26.event1.eventType = "@event1EventType@";
	myVars.races.race26.event2.id = "ctl00_mainContentPlaceHolder_dlstBrandLogo_ct7_divRollover";
	myVars.races.race26.event2.type = "onmouseover";
	myVars.races.race26.event2.loc = "ctl00_mainContentPlaceHolder_dlstBrandLogo_ct7_divRollover_LOC";
	myVars.races.race26.event2.isRead = "True";
	myVars.races.race26.event2.eventType = "@event2EventType@";
	myVars.races.race26.event1.executed= false;// true to disable, false to enable
	myVars.races.race26.event2.executed= false;// true to disable, false to enable
</script>

