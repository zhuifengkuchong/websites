<script id = "race40a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race40={};
	myVars.races.race40.varName="Window[26].mclosetime";
	myVars.races.race40.varType="@varType@";
	myVars.races.race40.repairType = "@RepairType";
	myVars.races.race40.event1={};
	myVars.races.race40.event2={};
	myVars.races.race40.event1.id = "Lu_Id_script_25";
	myVars.races.race40.event1.type = "Lu_Id_script_25__parsed";
	myVars.races.race40.event1.loc = "Lu_Id_script_25_LOC";
	myVars.races.race40.event1.isRead = "False";
	myVars.races.race40.event1.eventType = "@event1EventType@";
	myVars.races.race40.event2.id = "ctl00_mainContentPlaceHolder_dlstBrandLogo_ct2_arollover";
	myVars.races.race40.event2.type = "onmouseout";
	myVars.races.race40.event2.loc = "ctl00_mainContentPlaceHolder_dlstBrandLogo_ct2_arollover_LOC";
	myVars.races.race40.event2.isRead = "True";
	myVars.races.race40.event2.eventType = "@event2EventType@";
	myVars.races.race40.event1.executed= false;// true to disable, false to enable
	myVars.races.race40.event2.executed= false;// true to disable, false to enable
</script>

