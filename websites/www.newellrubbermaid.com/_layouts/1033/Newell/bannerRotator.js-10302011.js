(function ($) {
    $.fn.extend({
        bannerRotator: function (options) {
            var defaults = {
                width: "800px",
                height: "800px",
                transitionStyle: 'Fade',
                transitionDelay: 2000,
                transitionSpeed: 2000,
                fadeWithSlide: true,
                loop: true,
                autoStart: true
            }
            var options = defaults = $.extend(defaults, options);
            var inner = this;
            var mainTimer = null;
            var currentBannerIndex = 0;
            var outIndex = 0;
            var inIndex = 1;
            var play = true;

            this._TransitionOut = function (index, style, speed, callbackFunc) {
                //actionsLog.push('transitioning out: ' + index + ' options.width: ' + options.width + ' style: ' + style);
                outIndex = index;
                var banner = this.get_Banner(index);
                var toolbar = this.get_BannerToolbar(index);

                //when transitioning out, we'd like to hide the banner altogether in case content overhangs the edge and blocks links, etc... on the next banner.
                var decor = this.get_Decor();
                var callback = function () {
                    if (banner.length > 0) banner[0].style.visibility = 'hidden';
                    if (jQuery.browser.msie && decor.length > 0) decor.get(0).style.removeAttribute('filter');
                    if (callbackFunc != undefined) {
                        callbackFunc();
                    }
                }


                //banner.css({ "left": 0, "top": 0 });
                if (jQuery.browser.msie) decor.css("opacity", 1);
                switch (style) {
                    case 'Fade': banner.fadeOut(speed, callback); break;
                    case 'SlideLeft': banner.animate({ left: -options.width, opacity: (options.fadeWithSlide ? 0 : 1) }, speed, callback); break;
                    case 'SlideRight': banner.animate({ left: options.width, opacity: (options.fadeWithSlide ? 0 : 1) }, speed, callback); break;
                    case 'SlideUp': banner.animate({ top: -options.height, opacity: (options.fadeWithSlide ? 0 : 1) }, speed, callback); break;
                    case 'SlideDown': banner.animate({ top: options.height, opacity: (options.fadeWithSlide ? 0 : 1) }, speed, callback); break;
                }
                var alphaFix = false; // ($.browser.msie && $.browser.version <= 8);
                if (alphaFix) toolbar.css("background-color", "white");
                toolbar.fadeOut(0, function () { if (alphaFix) toolbar.css("background-color", "transparent"); });



                try {
                    $f('*').each(function () { this.pause() }); //pause any flowplayer instances
                } catch (E) {
                    //flowplayer instances may not even be on this page!
                }

                try {
                    $('video').each(function () { this.pause() }); //pause any video instances
                } catch (E) {
                    //flowplayer instances may not even be on this page!
                }

                if ($.browser.mozilla) {
                    try {
                        $('.fpVideoLink').each(function () {
                            if (this.reloadVid) {
                                this.onclick = this.reloadVid;
                                this.innerHTML = '';
                            }

                        }); //pause any video instances
                    } catch (E) {
                        //flowplayer instances may not even be on this page!
                    }
                }
            }

            this._TransitionIn = function (index, style, speed, callback) {
                //actionsLog.push('transitioning in: ' + index + ' options.width: ' + options.width + ' style: ' + style);
                inIndex = index;
                var obj = this;
                var banner = this.get_Banner(index);
                var toolbar = this.get_BannerToolbar(index);

                //make sure it's showing before we transition in
                if (banner.length > 0) banner[0].style.display = 'block';
                var decor = this.get_Decor();
                if (jQuery.browser.msie) decor.css("opacity", 1);
                switch (style) {
                    case 'Fade':
                        banner.css({ "visibility": "visible" });
                        banner.fadeIn(speed, function () {
                            if (jQuery.browser.msie && decor.length > 0) decor.get(0).style.removeAttribute('filter');
                            if (jQuery.browser.msie) $(this).get(0).style.removeAttribute('filter');
                            if (callback != undefined)
                                callback();
                        });
                        break;
                    case 'SlideLeft':
                        banner.css({ "left": options.width + "px", "top": "0px", "visibility": "visible", opacity: (options.fadeWithSlide ? 0 : 1) });
                        //banner[0].style.left = options.width + 'px';
                        //banner[0].style.display = 'inline';
                        //banner[0].style.opacity = 0;
                        banner.animate({ left: 0, opacity: 1 }, speed, function () {
                            if (jQuery.browser.msie && decor.length > 0) decor.get(0).style.removeAttribute('filter');
                            if (jQuery.browser.msie) $(this).get(0).style.removeAttribute('filter');
                            if (callback != undefined)
                                callback();
                        });
                        break;
                    case 'SlideRight':
                        banner.css({ "left": -(options.width) + 'px', "visibility": "visible", opacity: (options.fadeWithSlide ? 0 : 1) });
                        banner.animate({ left: 0, opacity: 1 }, speed, function () {
                            if (jQuery.browser.msie && decor.length > 0) decor.get(0).style.removeAttribute('filter');
                            if (jQuery.browser.msie) $(this).get(0).style.removeAttribute('filter');
                            if (callback != undefined)
                                callback();
                        });
                        break;
                    case 'SlideUp':
                        banner.css({ "top": options.height + 'px', "visibility": "visible", opacity: (options.fadeWithSlide ? 0 : 1) });
                        banner.animate({ top: 0, opacity: 1 }, speed, function () {
                            if (jQuery.browser.msie && decor.length > 0) decor.get(0).style.removeAttribute('filter');
                            if (jQuery.browser.msie) $(this).get(0).style.removeAttribute('filter');
                            if (callback != undefined)
                                callback();
                        });
                        break;
                    case 'SlideDown':
                        banner.css({ "top": -options.height + 'px', "visibility": "visible", opacity: (options.fadeWithSlide ? 0 : 1) });
                        banner.animate({ top: 0, opacity: 1 }, speed, function () {
                            if (jQuery.browser.msie && decor.length > 0) decor.get(0).style.removeAttribute('filter');
                            if (jQuery.browser.msie) $(this).get(0).style.removeAttribute('filter');
                            if (callback != undefined)
                                callback();
                        });
                        break;
                }
                var alphaFix = false; // ($.browser.msie && $.browser.version <= 8);
                if (alphaFix) toolbar.css("background-color", "white");

                toolbar.fadeIn(0, function () { if (alphaFix) toolbar.css("background-color", "transparent"); });
            }

            this._Play = function () {


                // If a Stop was initiatied, stop
                if (play == false) return;

                // Get the next banner index
                var nextBannerIndex = Number(currentBannerIndex) + 1;
                if (nextBannerIndex >= this.get_BannerCount()) {
                    if (options.loop == false) {
                        this.Stop();
                        return;
                    }
                    nextBannerIndex = 0;
                }


                // Transition out the current banner
                this._TransitionOut(currentBannerIndex, options.transitionStyle, options.transitionSpeed);

                // Increment the banner index
                currentBannerIndex = Number(nextBannerIndex);

                // Transition in the new banner
                this._TransitionIn(currentBannerIndex, options.transitionStyle, options.transitionSpeed, function () {
                    mainTimer = window.setTimeout(function () { inner._Play(); }, options.transitionDelay);
                });

                //alert("current banner left: " + this.get_Banner(nextBannerIndex).css("left"));


            }

            this._GetPreviousBannerIndex = function () {
                var index = Number(currentBannerIndex) - 1;
                if (index < 0) index = this.get_BannerCount() - 1;
                return index;
            }

            this._GetNextBannerIndex = function () {
                var index = Number(currentBannerIndex) + 1;
                if (index >= this.get_BannerCount()) index = 0;
                return index;
            }

            this.ShowBanner = function (index) {

                // Stop the main timer
                this.Stop();

                // If the current banner index is the index requested, do nothing
                if (Number(currentBannerIndex) == Number(index)) return;

                // Figure out how to transition to the selected banner. If doing fades, just fade to it. If sliding, reverse the slide if necessary to make it seem like the banners are on a continuous loop.
                var style = options.transitionStyle;
                if (index < currentBannerIndex) {
                    switch (options.transitionStyle) {
                        case "SlideLeft": style = "SlideRight"; break;
                        case "SlideRight": style = "SlideLeft"; break;
                        case "SlideUp": style = "SlideDown"; break;
                        case "SlideDown": style = "SlideUp"; break;
                    }
                }

                // Transition out the current banner
                this._TransitionOut(currentBannerIndex, style, 200);

                // Update the currentBannerIndex
                currentBannerIndex = Number(index);

                // Transition in the requested banner
                this._TransitionIn(currentBannerIndex, style, 200);

            }

            this.get_Decor = function () {
                return $(this).find(".newell_banner_decor");
            }

            this.get_Banner = function (index) {
                return $(this).find(".newell_banner[BannerIndex=" + index + "]");
            }

            this.get_BannerToolbar = function (index) {
                return $(this).find(".newell_banner_toolbar[BannerIndex=" + index + "]");
            }

            this.get_BannerCount = function () {
                return $(this).find(".newell_banner[BannerIndex]").length;
            }

            this.Play = function () {
                this.Stop();
                play = true;
                mainTimer = window.setTimeout(function () { inner._Play(); }, options.transitionDelay);
            }

            this.Stop = function () {
                play = false;

                if (mainTimer != null) window.clearTimeout(mainTimer);

                // Accelerate the outgoing banner
                var outBanner = this.get_Banner(outIndex);
                var outBannerTB = this.get_BannerToolbar(outIndex);
                if (outBanner != null) outBanner.stop(true, true);
                if (outBannerTB != null) outBannerTB.stop(true, true);

                // Stop the current incoming banner
                var inBanner = this.get_Banner(inIndex);
                var inBannerTB = this.get_BannerToolbar(inIndex);
                if (inBanner != null) inBanner.stop(true, true);
                if (inBannerTB != null) inBannerTB.stop(true, true);
            }

            return this.each(function () {
                var obj = $(this);

                //obj.find(".newell_banner_outer").css({ "width": options.width, "height": options.height });

                // Set the z-index values on the banners and toolbars in order.
                var bannerCount = inner.get_BannerCount();
                obj.find(".newell_banner").each(function () {
                    var banner = $(this);

                    // Order the z-index of the banners
                    var bannerIndex = banner.attr("BannerIndex");
                    banner.css({ "z-index": bannerIndex });

                    // Order the banner buttons, but make sure they show on top of any overlays
                    var bannerButton = obj.find(".newell_banner_toolbar[BannerIndex=" + bannerIndex + "]");
                    bannerButton.css("z-index", bannerIndex + bannerCount + 99);

                    // Fix for extra margin at top of P elements in Firefox
                    if ($.browser.mozilla) {
                        banner.find("div").find("p").css("margin-top", "0px");
                    }

                });

                // Link the banner toolbar buttons so they are clickable
                obj.find(".newell_banner_toolbar_buttonoff, .newell_banner_toolbar_buttonon").each(function () {
                    $(this).css("cursor", "pointer");
                    $(this).click(function () {
                        inner.ShowBanner($(this).attr("BannerIndex"));
                    });
                });

                // Link the banner previous and next buttons so they are clickable
                obj.find(".newell_banner_toolbar_left").each(function () {
                    $(this).css("cursor", "pointer");
                    $(this).click(function () {
                        inner.ShowBanner(inner._GetPreviousBannerIndex());
                    });
                });
                obj.find(".newell_banner_toolbar_right").each(function () {
                    $(this).css("cursor", "pointer");
                    $(this).click(function () {
                        inner.ShowBanner(inner._GetNextBannerIndex());
                    });
                });

                // If any banners exist, start the animation
                var bannerCount = inner.get_BannerCount();
                if (bannerCount > 1 && options.autoStart) {
                    inner.Play();
                }
                else if (bannerCount == 0) {
                    obj.html("<div>Edit the properties of this web part to select banners to display.</div>");
                }

            });

        }
    });
})(jQuery);

function NRBR_ActivateVideo(ID, mp4Url, flvUrl, w, h, fpKey) {

    var newFunc = function () {
        if (flashembed.getVersion()[0] > 0) {

        flowplayer('aVid_' + ID,
		    { src: 'flowplayer/flowplayer.commercial-3.2.7.swf'/*tpa=http://www.newellrubbermaid.com/_layouts/1033/Newell/flowplayer/flowplayer.commercial-3.2.7.swf*/, wmode: 'opaque' },
		    {
		        key: fpKey,

		        playlist: [
			    {
			        autoPlay: true,
			        scaling: 'orig',
			        url: flashembed.isSupported([9, 115]) ? mp4Url : flvUrl,
			        bufferLength: 3,
			        buffering: true,
			        controls: { time: false}//,
			        //provider: 'pstream'


			    }
			    ]//,
		        // streaming plugins are configured normally under the plugins node
		        // plugins: {

		        // pstream: { url: 'flowplayer/flowplayer.pseudostreaming-3.2.7.swf'/*tpa=http://www.newellrubbermaid.com/_layouts/1033/Newell/flowplayer/flowplayer.pseudostreaming-3.2.7.swf*/, rangeRequests: true }


		        //}
		    }
	    );

            //need to stop anchor from responding to additional clicks once loaded.
            $('#aVid_' + ID).attr("onclick", "return false;");

        } else {

            var msg = '<div style="text-align:center; padding-top:20px;">Adobe Flash or HTML5 Video support is Required to view this video.  Please upgrade your browser or install Flash.<br><br><a href="' + mp4Url + '">Download Video</a></div>';

            document.getElementById('aVidContainer_' + ID).innerHTML = '<video id="aVidVideo_' + ID + '" width="' + w + '" height="' + h + '" controls="controls" autoplay="autoplay"><source src="' + mp4Url + '" type="video/mp4" />' + msg + '</video>'; //video tag goes here....
            try {
                document.getElementById('aVidVideo_' + ID).play();
            } catch (err) { }
        }

    }

    newFunc();

    if (flashembed.getVersion()[0] > 0){
        $('#aVid_' + ID).attr("class", "fpVideoLink");
        $('#aVid_' + ID)[0].reloadVid = newFunc;
    }

    return false;

}
