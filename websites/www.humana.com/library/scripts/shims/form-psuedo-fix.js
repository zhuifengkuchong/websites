$(document).ready(function () {

    $('input.input-text[type="text"]., input[type=email],input[type=url],input[type=tel],input[type=password],input[type=date],textarea').focus(function () {
        $(this).addClass("focus");
    });
    $('input[type="text"], input[type=email],input[type=url],input[type=tel],input[type=password],input[type=date],textarea').blur(function () {
        $(this).removeClass("focus");
    });

});