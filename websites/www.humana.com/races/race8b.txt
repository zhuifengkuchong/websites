<script id = "race8b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race8={};
	myVars.races.race8.varName="Function[14335].timeout";
	myVars.races.race8.varType="@varType@";
	myVars.races.race8.repairType = "@RepairType";
	myVars.races.race8.event1={};
	myVars.races.race8.event2={};
	myVars.races.race8.event1.id = "Lu_Id_h1_3";
	myVars.races.race8.event1.type = "onfocus";
	myVars.races.race8.event1.loc = "Lu_Id_h1_3_LOC";
	myVars.races.race8.event1.isRead = "True";
	myVars.races.race8.event1.eventType = "@event1EventType@";
	myVars.races.race8.event2.id = "Lu_Id_h1_2";
	myVars.races.race8.event2.type = "onfocus";
	myVars.races.race8.event2.loc = "Lu_Id_h1_2_LOC";
	myVars.races.race8.event2.isRead = "False";
	myVars.races.race8.event2.eventType = "@event2EventType@";
	myVars.races.race8.event1.executed= false;// true to disable, false to enable
	myVars.races.race8.event2.executed= false;// true to disable, false to enable
</script>

