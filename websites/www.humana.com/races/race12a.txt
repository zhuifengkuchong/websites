<script id = "race12a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race12={};
	myVars.races.race12.varName="Function[14335].timeout";
	myVars.races.race12.varType="@varType@";
	myVars.races.race12.repairType = "@RepairType";
	myVars.races.race12.event1={};
	myVars.races.race12.event2={};
	myVars.races.race12.event1.id = "Lu_Id_h1_6";
	myVars.races.race12.event1.type = "onfocus";
	myVars.races.race12.event1.loc = "Lu_Id_h1_6_LOC";
	myVars.races.race12.event1.isRead = "False";
	myVars.races.race12.event1.eventType = "@event1EventType@";
	myVars.races.race12.event2.id = "Lu_Id_h3_1";
	myVars.races.race12.event2.type = "onfocus";
	myVars.races.race12.event2.loc = "Lu_Id_h3_1_LOC";
	myVars.races.race12.event2.isRead = "True";
	myVars.races.race12.event2.eventType = "@event2EventType@";
	myVars.races.race12.event1.executed= false;// true to disable, false to enable
	myVars.races.race12.event2.executed= false;// true to disable, false to enable
</script>

