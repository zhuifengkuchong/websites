/*
OnlineOpinion v5.7.3
Released: 6/4/2013. Compiled 06/04/2013 08:37:25 AM -0500
Branch: master 122a760d8e979af7090004b5d3cb086d5b0896be
Components: Full
The following code is Copyright 1998-2013 Opinionlab, Inc.  All rights reserved. Unauthorized use is prohibited. This product and other products of OpinionLab, Inc. are protected by U.S. Patent No. 6606581, 6421724, 6785717 B1 and other patents pending. http://www.opinionlab
*/

/* global window, OOo */

/* [+] Tab configuration */
(function (w, o) {
    'use strict';

    /* Shim Date.now() for older browsers */
    if (!Date.now) {
        Date.now = function() { return new Date().getTime(); };
    }

    var OpinionLabInit = function () {
        var tStamp = Date.now();

        o.oo_feedback = new o.Ocode({
            customVariables: {
                coreID6: o.readCookie('CoreID6'),
                timestamp: tStamp
            }
        });
    };

    o.addEventListener(w, 'load', OpinionLabInit, false);

})(window, OOo);