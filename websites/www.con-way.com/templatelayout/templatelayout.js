
var showMasTplMenu  = '';
var closeMasTplFAQ 	= '';

//demo	
var internetdemo_cookie_name = "conwayInternetDemoCookie";
var internetdemo_cookie_info;
function putInternetDemoCookie() {
	var index;
	if(document.cookie != document.cookie){
		index = document.cookie.indexOf(internetdemo_cookie_name);
	}else { 
		index = -1;
	}
	if (index == -1){
		document.cookie=internetdemo_cookie_name+"="+"Yes"+"; path=/";
	}
	document.forms["testDemoCookieForm"].submit();
}
function clearInternetDemoCookie() {
	var index;
	if(document.cookie != document.cookie){
		index = document.cookie.indexOf(internetdemo_cookie_name);
	}else { 
		index = -1;
	}
	if (index == -1){
		document.cookie=internetdemo_cookie_name+"="+"Yes"+"; path=/;expires=15/02/2003 00:00:00";
	}
		document.forms["testDemoCookieForm"].submit();
}
function getInternetDemoCookieName() {
	var index;
	var namestart;
	var nameend;
	if(document.cookie){
		index = document.cookie.indexOf(internetdemo_cookie_name);
		if (index != -1){
			namestart = (document.cookie.indexOf("=", index) + 1);
			nameend = document.cookie.indexOf(";", index);
			if (nameend == -1) {
				nameend = document.cookie.length;
			}
			internetdemo_cookie_info = document.cookie.substring(namestart, nameend);
			return internetdemo_cookie_info;
		}
	}
}
	var internetdemo_cookie_info_display=getInternetDemoCookieName();
	if(internetdemo_cookie_info_display == "Yes"){
		//internetdemo_cookie_info_display = "Yes";
	}else{
		internetdemo_cookie_info_display = "No";
	}
	
function masTplDemoMode() {
		var masTplDemoText = "<font size=+1><b>Demo Mode</b></font>: safe to try out any application. <a href=/Tools/demo/demo_help.html>Demo help</a>";
		var masTplDemoColor = "#f3de47";
		var masTplDemoImage = "../private/images/backgrounds/demo.gif"/*tpa=http://www.con-way.com/private/images/backgrounds/demo.gif*/;
		var masTplDemoElemTest = "masTplDemoDisplay";	
		masTplDemoElemTest = "masTplBreadcrumbsTop";
		
		if((document.getElementById)&& (document.getElementById(masTplDemoElemTest)!=null)) {
	  		// Get a reference to the element
			var masTplDemoElement = document.getElementById(masTplDemoElemTest);
			// Check the element's style object and background property are available
		 	if ((masTplDemoElement.style)&& (masTplDemoElement.style.backgroundColor!=null)) { 
					var masTplDemoBreadcrumbText = "<a href=\"http://www.con-way.com/en/tools_pricing\">Tools &amp; Pricing</a> &gt; <a href=\"http://www.con-way.com/en/tools_pricing/freight\">Freight</a> &gt; " + document.title;			
					document.getElementById(masTplDemoElemTest).innerHTML = masTplDemoBreadcrumbText + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " + masTplDemoText;
				document.getElementById(masTplDemoElemTest).style.backgroundImage = "url('" + masTplDemoImage + "')";
					document.getElementById(masTplDemoElemTest).style.backgroundColor = masTplDemoColor;					
  			}else {	
				// This CSS property is not assigned or is not supported
				return;
  			}
		}else {
		  return;
		}
}

function showMasTplSpecialInfo(masTplMarketingFrameName,masTplMarketingLayerName,masTplMarketingFormElement, masTplMarketingFormName,masTplMarketingApplId,masTplMarketingAuthInd,masTplMarketingMsgId) {
		if (document.all.item(masTplMarketingLayerName).style.visibility == 'visible') {
			document.all.item(masTplMarketingLayerName).style.visibility = 'hidden';
		}
		else {
			document.all.item(masTplMarketingLayerName).style.visibility = 'visible';
			var masTplMarketingLayerPage = "http://www.con-way.com/webapp/marketing_app/unreg/MarketingInfoLayerUnreg.jsp";
			if(masTplMarketingAuthInd == 'Y'){
				masTplMarketingLayerPage = "http://www.con-way.com/webapp/marketing_app/reg/MarketingInfoLayerReg.jsp";
			}
			masTplSpecialInfoIFrame.location.replace(masTplMarketingLayerPage + "?frameName=" 
										+ masTplMarketingFrameName 
										+ "&layerName=" 
										+ masTplMarketingLayerName 
										+ "&formElement=" 
										+ masTplMarketingFormElement 
										+ "&formName=" 
										+ masTplMarketingFormName
										+ "&displayType=inline"
										+ "&applId="
										+ masTplMarketingApplId
										+ "&msgId="
										+ masTplMarketingMsgId
										);
		}
}

function highlightMasTplMenu(masterTemplate_ApplicationId, masterTemplate_ApplicationFAQNbr, masterTemplate_Language){

	if(masterTemplate_Language == 'es_MX'){		
		initMenuSpanish();
		//closeMasTplFAQ = 'x Cierre';
	} else {
		// English is default
	}

	//determineMasTplSettings(masterTemplate_ApplicationId,masterTemplate_ApplicationFAQNbr);
	
}

function writeMasTplCopyright(masterTemplate_copyright){
/* Optional copyright */
	document.getElementById("masTplCopyright").innerHTML = masterTemplate_copyright;	
}

function writeMasTplBreadcrumbs(masterTemplate_ApplicationId,masterTemplate_ApplicationFAQNbr,masterTemplate_FAQ_URL,masterTemplate_FAQ_Link,masterTemplate_FAQ_Header,masterTemplate_FAQ_Desc){
/* Optional breadcrumbs */
	var masterTemplate_Breadcrumbs_Careers = "<a href=\"../en.htm\"/*tpa=http://www.con-way.com/*/>Home</a> &gt; <a href=\"http://www.con-way.com/en/careers\">Careers</a> &gt;  ";	
	var masterTemplate_Breadcrumbs_About = "<a href=\"../en.htm\"/*tpa=http://www.con-way.com/*/>Home</a> &gt; <a href=\"http://www.con-way.com/en/about_con_way\">About Con-way</a> &gt;  ";
	var masterTemplate_Breadcrumbs_Tools_Freight = 	"<a href=\"../en.htm\"/*tpa=http://www.con-way.com/*/>Home</a>  &gt; <a href=\"http://www.con-way.com/en/tools_pricing\">Tools &amp; Pricing</a> &gt; <a href=\"http://www.con-way.com/en/tools_pricing/freight\">Freight</a> &gt; ";
	//"<a href='../en.htm'/*tpa=http://www.con-way.com/*/>Home</a>  &gt; <a href='http://www.con-way.com/en/tools_pricing'>Tools &amp; Pricing</a> &gt; <a href='http://www.con-way.com/en/tools_pricing/freight'>Freight</a> &gt; <a href='http://www.con-way.com/en/tools_pricing/freight/fr_tools'>Tools</a> &gt; ";
	//note: do not put more than 3 links in the breadcrumb because it causes alignment problems when using the BACK button on some pages
	
	
	var masterTemplate_Breadcrumbs_start = masterTemplate_Breadcrumbs_Tools_Freight;
	if(masterTemplate_ApplicationFAQNbr != null && masterTemplate_ApplicationFAQNbr != '' && masterTemplate_ApplicationFAQNbr != '0'){
		if(masterTemplate_ApplicationId != null && masterTemplate_ApplicationId == "careers"){
			masterTemplate_Breadcrumbs_start = masterTemplate_Breadcrumbs_Careers;
		}else if(masterTemplate_ApplicationId != null && masterTemplate_ApplicationId == "about"){
			masterTemplate_Breadcrumbs_start = masterTemplate_Breadcrumbs_About;
		}
		
		document.getElementById("masTplBreadcrumbsTop").innerHTML = masterTemplate_Breadcrumbs_start + document.title;
		document.getElementById("masTplBreadcrumbsBottom").innerHTML = masterTemplate_Breadcrumbs_start + document.title;
	}
}

function writeMasTplFAQ(masterTemplate_ApplicationId,masterTemplate_ApplicationFAQNbr,masterTemplate_FAQ_URL,masterTemplate_FAQ_Link,masterTemplate_FAQ_Header,masterTemplate_FAQ_Desc){
/* Optional FAQ */
	/*
	if(masterTemplate_ApplicationFAQNbr != null && masterTemplate_ApplicationFAQNbr != '' && masterTemplate_ApplicationFAQNbr != '0'){
		document.getElementById("masTplFAQ").innerHTML = '<h2>' + masterTemplate_FAQ_Header + '</h2>'
			+ '<p>' + masterTemplate_FAQ_Desc + '</p>';
	}
	document.getElementById("masTplFAQ_close1").innerHTML = closeMasTplFAQ;
	document.getElementById("masTplFAQ_close2").innerHTML = closeMasTplFAQ;
	*/
	
	writeMasTplBreadcrumbs(masterTemplate_ApplicationId,masterTemplate_ApplicationFAQNbr,masterTemplate_FAQ_URL,masterTemplate_FAQ_Link,masterTemplate_FAQ_Header,masterTemplate_FAQ_Desc);
	
	if(internetdemo_cookie_info_display == "Yes"){
		//change style
		masTplDemoMode();
	}
	
}

function writeMasTplFAQLink(masterTemplate_ApplicationId,masterTemplate_ApplicationFAQNbr,masterTemplate_FAQ_URL,masterTemplate_FAQ_Link,masterTemplate_FAQ_Header,masterTemplate_FAQ_Desc){
/* Optional FAQ Link */
	/*
	if(masterTemplate_ApplicationFAQNbr != null && masterTemplate_ApplicationFAQNbr != '' && masterTemplate_ApplicationFAQNbr != '0'){
		document.getElementById("masTplFAQLink").innerHTML = masterTemplate_FAQ_Link;
		document.getElementById("masTplFAQUrl").innerHTML = masterTemplate_FAQ_URL;
	}
	*/
}
function newwindowFAQ(){ 
	/*
	var masTplFAQUrlStr = '';
	if(document.all){
		 masTplFAQUrlStr = document.getElementById("masTplFAQUrl").innerText;
	}else{
		 masTplFAQUrlStr = document.getElementById("masTplFAQUrl").textContent;	
	}
	var awindow = window.open(masTplFAQUrlStr,'FAQWin','width=300,height=200,resizable=yes,scrollbars=yes,status=yes');
	awindow.focus(); 
	*/
} 

function initMasTplFAQLink(){
//alert("testing init FAQ link, check before doDimensions");
	//doDimensions();	
//alert("testing after doDimensions");
	//Effect.Appear('help_link', { duration: .5 });
}

function addLoadEvent(func) {
  var oldonload = window.onload;
  if (typeof window.onload != 'function') {
    window.onload = func;
  } else {
    window.onload = function() {
      if (oldonload) {
        oldonload();
      }
      func();
    }
  }
}

function checkMasTplSearchProNumber(){  
   //check for numbers to go to freight tracking or continue with search
    var masTplSearchItem = document.masTplSearchForm.q.value;
    var i;
	var continueSearch = "no";
	var someNumberFound = "no";
	var masTplProItem = "";
    for (i = 0; i < masTplSearchItem.length; i++){   
        var c = masTplSearchItem.charAt(i);
		if(c != "-" && c != " "){ ///skip hyphen and space
    	    if (c < "0" || c > "9"){
				continueSearch = "yes";
				break;
			}else{
				someNumberFound = "yes";
				masTplProItem = masTplProItem + c;
			}
		}
    }
	if(continueSearch == "no" && someNumberFound == "yes"){
		location.href="https://www.con-way.com/webapp/manifestrpts_p_app/shipmentTracking.do?PRO=" + masTplProItem; //send to Tracking
		return false;
	}else{
	    return true;
	}
}


//addLoadEvent(initMasTplFAQLink);



