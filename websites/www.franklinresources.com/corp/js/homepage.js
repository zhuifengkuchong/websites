function getCountryNav(url){
	//console.log('location=Homepage&content=countrySelectNav&subcontent=-&tab=-&drawer=-&drawertab=-&ev=LinkClicked&ev_value=' + url);
	ntptEventTag('location=Homepage&content=countrySelectNav&subcontent=-&tab=-&drawer=-&drawertab=-&ev=LinkClicked&ev_value=' + url);
	
	
}

function trackBillboard(url){
	//console.log('location=Homepage&content=Billboard&subcontent=-&tab=-&drawer=-&drawertab=-&ev=LinkClicked&ev_value=' + url);
	ntptEventTag('location=Homepage&content=Billboard&subcontent=-&tab=-&drawer=-&drawertab=-&ev=LinkClicked&ev_value=' + url);
	window.location = url;
}


jQuery(document).ready(function() {
	jQuery("body").bind("click", function(e) {
		var target = jQuery(e.target).closest('a');
		if (target.length > 0) { //if this is a link
			//check download || regular link
			var h = jQuery(e.target).attr('href');
			var evType = "LinkClicked";
			var dlType = null;
			if ( h != null && h.match(/(\.pdf|\.doc|\.ppt|\.doc)/gi) ) {
				evType = "download";
				if ( h.match("stream_pdf") ) { //if streaming pdf, strip out app portion for url
				  h = h.replace(/(.*)relativeDocPath=/gi,"").replace(/'\);$/gi, "");
				} else if ( h.match(/^javascript:newPop/i) ) { //if in newPop, strip out
				  h = h.replace(/javascript:newPop\('/gi,"").replace(/'\);$/gi, "");
				}

				var l = h.lastIndexOf('.');
				dlType = h.substr(l + 1).toLowerCase().replace(/'\);$/gi, "");
			}

			if (target.parents("#gp-main").length > 0) {
				if (dlType != null ) {
					ntptEventTag('location=Homepage&content=Modules&subcontent=-&tab=-&drawer=-&drawertab=-&ev=' + evType + '&ev_value=' + escape(h) + '&filetype='+dlType);
					//console.log('location=Homepage&content=Modules&subcontent=-&tab=-&drawer=-&drawertab=-&ev=' + evType + '&ev_value=' + escape(h) + '&filetype='+dlType);
				} else {
					ntptEventTag('location=Homepage&content=Modules&subcontent=-&tab=-&drawer=-&drawertab=-&ev=' + evType + '&ev_value=' + escape(h));
					//console.log('location=Homepage&content=Modules&subcontent=-&tab=-&drawer=-&drawertab=-&ev=' + evType + '&ev_value=' + escape(h));
				}
			}
		}
	});

	//countries
		jQuery('#countries li a').each(function(index){
            jQuery(this).click( function(){
                var url = jQuery(this).attr('href');
				getCountryNav(url);
            } );
       
    });
	
});