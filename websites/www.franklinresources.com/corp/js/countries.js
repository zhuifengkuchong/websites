jQuery(document).ready(function(){
                jQuery('#countries h3.expand')
                    .hover(function () {
                        jQuery(this).toggleClass('cHover');
                    }, function() {
                        if(jQuery(this).hasClass('selected') != true) {
                            jQuery(this).toggleClass('cHover');
                        }
                    })
                    .click(
                    function() {
                        if(jQuery(this).hasClass('selected') != true) {
                            jQuery('.selected').next('ul').slideUp('fast');
                            jQuery('.selected').removeClass('selected');
                            jQuery('.cHover').removeClass('cHover');
                            jQuery(this).addClass('selected').next('ul').slideDown('fast');
                        } else {
			   
                            jQuery('.selected').next('ul').slideUp('fast');
                            jQuery('.selected').removeClass('selected');
                            jQuery(this).addClass('cHover');
 			                         
                        }
                    return false;
                    })
                    .next().hide();
                
                jQuery('#countries h3.otherSite').hover(function () {
                        jQuery(this).toggleClass('cHover');
                    }, function() {
                        jQuery(this).toggleClass('cHover');
                    }
                )
                
            });