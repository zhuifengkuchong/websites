<script id = "race87a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race87={};
	myVars.races.race87.varName="Object[3781].triggered";
	myVars.races.race87.varType="@varType@";
	myVars.races.race87.repairType = "@RepairType";
	myVars.races.race87.event1={};
	myVars.races.race87.event2={};
	myVars.races.race87.event1.id = "Lu_Id_li_16";
	myVars.races.race87.event1.type = "onmouseout";
	myVars.races.race87.event1.loc = "Lu_Id_li_16_LOC";
	myVars.races.race87.event1.isRead = "True";
	myVars.races.race87.event1.eventType = "@event1EventType@";
	myVars.races.race87.event2.id = "Lu_Id_div_31";
	myVars.races.race87.event2.type = "onmouseout";
	myVars.races.race87.event2.loc = "Lu_Id_div_31_LOC";
	myVars.races.race87.event2.isRead = "False";
	myVars.races.race87.event2.eventType = "@event2EventType@";
	myVars.races.race87.event1.executed= false;// true to disable, false to enable
	myVars.races.race87.event2.executed= false;// true to disable, false to enable
</script>

