<script id = "race37a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race37={};
	myVars.races.race37.varName="Lu_Id_li_14__onmouseout";
	myVars.races.race37.varType="@varType@";
	myVars.races.race37.repairType = "@RepairType";
	myVars.races.race37.event1={};
	myVars.races.race37.event2={};
	myVars.races.race37.event1.id = "Lu_DOM";
	myVars.races.race37.event1.type = "onDOMContentLoaded";
	myVars.races.race37.event1.loc = "Lu_DOM_LOC";
	myVars.races.race37.event1.isRead = "False";
	myVars.races.race37.event1.eventType = "@event1EventType@";
	myVars.races.race37.event2.id = "Lu_Id_li_14";
	myVars.races.race37.event2.type = "onmouseout";
	myVars.races.race37.event2.loc = "Lu_Id_li_14_LOC";
	myVars.races.race37.event2.isRead = "True";
	myVars.races.race37.event2.eventType = "@event2EventType@";
	myVars.races.race37.event1.executed= false;// true to disable, false to enable
	myVars.races.race37.event2.executed= false;// true to disable, false to enable
</script>

