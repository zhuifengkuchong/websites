<script id = "race48b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race48={};
	myVars.races.race48.varName="Object[3781].triggered";
	myVars.races.race48.varType="@varType@";
	myVars.races.race48.repairType = "@RepairType";
	myVars.races.race48.event1={};
	myVars.races.race48.event2={};
	myVars.races.race48.event1.id = "Lu_Id_a_14";
	myVars.races.race48.event1.type = "onmouseover";
	myVars.races.race48.event1.loc = "Lu_Id_a_14_LOC";
	myVars.races.race48.event1.isRead = "False";
	myVars.races.race48.event1.eventType = "@event1EventType@";
	myVars.races.race48.event2.id = "Lu_Id_a_11";
	myVars.races.race48.event2.type = "onmouseover";
	myVars.races.race48.event2.loc = "Lu_Id_a_11_LOC";
	myVars.races.race48.event2.isRead = "True";
	myVars.races.race48.event2.eventType = "@event2EventType@";
	myVars.races.race48.event1.executed= false;// true to disable, false to enable
	myVars.races.race48.event2.executed= false;// true to disable, false to enable
</script>

