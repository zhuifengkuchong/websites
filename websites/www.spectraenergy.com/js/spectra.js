// exit out of external frameset
if (parent.location != this.location) {
  parent.location.href = this.location.href
  }

//add analytics tracking and other features to links
function reconfigureLinks() {
	var myLinks = document.getElementsByTagName("a");
	for(var i = 0; i < myLinks.length; i++) {
		var a = myLinks[i];
		var href = a.getAttribute("href");
		if (href!=null) {
			var exten = href.substring(href.lastIndexOf('.'), href.length);
			// document links
			if (exten == ".pdf" || exten == ".doc" || exten == ".xls" || exten == ".ppt") {
				a.target = "_blank";
				a.onclick = function(){pageTracker._trackEvent('Documents', this.href.replace('http://'+document.domain,''), document.location.pathname);window.open(this.href,'document','resizable,location');return false;};
			}
			// external site links
			if ((href.indexOf("http") > -1 && href.indexOf("http://www.spectraenergy.com/js/spectraenergy.com") < 0) || href.indexOf("http://www.spectraenergy.com/js/link.spectraenergy.com") > -1) {
				a.title = "This link will take you to another Web site.";
				//a.className += " externallink"
				a.onclick = function(){pageTracker._trackEvent('Outbound', this.href.substring(this.href.indexOf('://')+3, this.href.indexOf('/',8)), document.location.pathname);};
			}
			// multimedia links
			if (exten == ".swf" || exten == ".mp3" || exten == ".wma" || exten == ".wmv" || a.href.indexOf("/content/includes/Videos/") > -1 || a.className == "videoPlayer") {
				a.onclick = function(){pageTracker._trackEvent('Multimedia', this.href.replace('http://'+document.domain,''), document.location.pathname);};
				a.title = "Watch the Video";
			}
		}
	}
}

var timeOuts = new Array();

jQuery.noConflict();

var cNav = function (navto) {
document.location = navto;
};

hasCategory = function (category, match) {
    return category && jQuery.inArray(match, category) > -1;
};

conditionallyAddVideo = function (entry, id, link, category) {
    count = jQuery("#" + id + ">div").length;
    if (count < 3) {
        jQuery("#" + id).append("<div><div class=\"moreVideo\" id=\"vid" + count + "\"><a rel=\"#fullSizeImage\" class=\"videoPlayer\" href=\"http://www.youtube.com/v/" + entry.id + "\"><img src=\"/content/includes/dbthumb/bethumb.aspx?img=" + entry["thumbnails"]["default"]["url"] + "&w=216&h=162\"></a><h3><a rel=\"#fullSizeImage\" class=\"videoPlayer\" href=\"" + entry.id + "\" style=\"display:block; clear:both;\">" + entry.title + "</a></h3><p class=\"desc\">" + entry.description + "<br/><b>Duration:</b> " + (entry.duration / 60).toFixed(2) + " minutes</p></div></div>");
    }
    if (count == 3) {
        jQuery("#" + id).append("<div class=\"clearleft\"><p><a target=\"_blank\" href=\"http://www.youtube.com/user/SpectraEnergy#g/c/" + link + "\">More " + category + " Videos &raquo;</a></p></div>");
    }
};

var openYouTubePopup = function (val,w,h) {
    //jQuery("#putcontenthere").html("<div id=\"youtubevideo\"><strong>This content requires the Adobe Flash Player.</strong><br /><br /> <a href=\"http://www.adobe.com/go/getflashplayer\"><img src=\"../../wwwimages.adobe.com/www.adobe.com/images/shared/download_buttons/get_flash_player.gif\"/*tpa=http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif*/ border=\"0\" alt=\"Get Adobe Flash player\" /></a></div><script type=\"text/javascript\">flashembed('#youtubevideo', {src: \"" + val + "\",allowfullscreen: false,width:480,height:385});</script>");
    //tb_show('', 'TB_inline?height=470&width=580&inlineId=video');
    jQuery("#putcontenthere").html("<div id=\"youtubevideo\"><strong>This content requires the Adobe Flash Player.</strong><br /><br /> <a href=\"http://www.adobe.com/go/getflashplayer\"><img src=\"../../wwwimages.adobe.com/www.adobe.com/images/shared/download_buttons/get_flash_player.gif\"/*tpa=http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif*/ border=\"0\" alt=\"Get Adobe Flash player\" /></a></div><script type=\"text/javascript\">flashembed('#youtubevideo', {src: \"" + val + "&rel=0&autoplay=1\",allowfullscreen: true,width:" + w + ",height:" + h + "});</script>");
	h = h+100;
	w = w+100;
	showsize = "TB_inline?height=" + h + "&width=" + w + "&inlineId=video";
    tb_show('', showsize);
};

String.prototype.parseURL = function () {
    return this.replace(/[A-Za-z]+:\/\/[A-Za-z0-9-_]+\.[A-Za-z0-9-_:%&\?\/.=]+/, function (url) {
        return url.link(url);
    });
};

String.prototype.parseDate = function () {
    var v = this.split(' '); var dte = new Date(Date.parse(v[1] + " " + v[2] + ", " + v[5] + " " + v[3] + " UTC"));
    return (dte.getMonth() + 1) + '/' + dte.getDate() + '/' + dte.getFullYear();
};

var yt = function (data) {
    jQuery.each(data, function (i, item) {
		if (i<2) {
			jQuery("<div class='ytvid'><a class='videoPlayer' title='Watch the Video' href='http://www.youtube.com/v/" + item.id + "'><div class='ytmb' style=\"background:url('" + item["thumbnails"]["default"]["url"] + "') top center no-repeat;\"><span>" + (item.duration / 60).toFixed(2).replace('.', ':') + "</span></div><span>" + item.title + "</span></a></div>").appendTo("#youtubefeed");
		}
    });
};

var noyt = function () {
			jQuery("<p>Video feed is temporarily unavailable. Please visit our <a href=\"http://www.youtube.com/user/SpectraEnergy\" title=\"Visit Our YouTube Channel\">our YouTube page</a>.</p>").appendTo("#youtubefeed");
};

var fb = function (data) {
    jQuery.each(data.data, function (i, item) {
        jQuery("<div/>").html("<div class='stat'>" + item.message + "</div>").appendTo("#facebookfeed");
    });
};

var tw = function (data) {
    jQuery.each(data, function (i, item) {
		if (i<2) {
			jQuery("<div/>").html("<div class='twit'>" + item.text.parseURL() + "<span>" + item.created_at.parseDate() + " via " + item.source + "</span></div>").appendTo("#twitterfeed");
		}
    });
};

getVideos = function () {
	if (myData.iid15) {
		var entries = myData.iid15.result || [];
		for (var i = 0; i < entries.length; i++) {
			entry = entries[i];
			if (hasCategory(entry.tags, "Featured")) {
				jQuery("#featuredVid").html("<object data=\"http://www.youtube.com/v/" + entry.id + "?fs=1&amp;hl=en_US&amp;rel=0&amp;hd=1\" height=\"315\" type=\"application/x-shockwave-flash\" width=\"516\"><param name=\"allowFullScreen\" value=\"true\"><param name=\"allowscriptaccess\" value=\"always\"><param name=\"src\" value=\"http://www.youtube.com/v/" + entry.id + "?fs=1&amp;hl=en_US&amp;rel=0&amp;hd=1\"><param name=\"allowfullscreen\" value=\"true\"><param name=\"wmode\" value=\"transparent\"></object>");
				jQuery("#featuredDisc").html("<h3>" + entry.title + "</h3><p>" + entry.description + "</p>");
			}
			
			if (hasCategory(entry.tags, "Community Involvement")) {
				conditionallyAddVideo(entry, "community", "FC299078F88E2F80", "Community Involvement");
			}
			if (hasCategory(entry.tags, "Recruiting")) {
				conditionallyAddVideo(entry, "recruiting", "718E777764E53CA0", "Recruiting");
			}
			if (hasCategory(entry.tags, "Operations Construction Safety")) {
				conditionallyAddVideo(entry, "operations", "76795615884EF823", "Operations Construction Safety");
			}
			if (hasCategory(entry.tags, "Commitment to Landowners")) {
				conditionallyAddVideo(entry, "commitment", "3C816EAB8880DEBA", "Commitment to Landowners");
			}
			if (hasCategory(entry.tags, "Benefits of Natural Gas")) {
				conditionallyAddVideo(entry, "benefits", "9B787ACA560ABAD3", "Benefits of Natural Gas");
			}
		
		}
	} else {
		jQuery("#featuredVid").html("<object width=\"516\" height=\"315\" type=\"application/x-shockwave-flash\" data=\"http://www.youtube.com/v/-UCaziwXMjE?version=3&f=videos&app=youtube_gdata\"><param value=\"true\" name=\"allowFullScreen\"><param value=\"always\" name=\"allowscriptaccess\"><param value=\"http://www.youtube.com/v/-UCaziwXMjE?version=3&f=videos&app=youtube_gdata\" name=\"src\"><param value=\"true\" name=\"allowfullscreen\"><param value=\"transparent\" name=\"wmode\"></object>");
		jQuery("#featuredDisc").html("<h3>Energy for Life Part Three</h3><p>We all have a stake in our energy future. Check out our third Energy for Life video and learn how natural gas is making a positive impact in our lives.</p>");
		jQuery("#YTGrid").html("<hr class=\"clearleft\"><h2 class=\"clearleft\">More Spectra Energy Videos</h2><div><p class=\"desc\">Spectra Energy has many beneficial videos on <a title=\"Visit Our YouTube Channel\" href=\"http://www.youtube.com/user/SpectraEnergy\">our YouTube page</a>. Learn more about our company and industry.</p><p>&nbsp;</p></div>")
	}
	
    jQuery("a.videoPlayer").bind("click", function (event) {
        event.preventDefault();
        //openYouTubePopup(this.getAttribute("href", 2));
		swfPath = this.getAttribute("href", 2);
		if (swfPath.indexOf('http://www.spectraenergy.com/js/www.youtube.com')>-1) {
			w = 640;
			h = 360;
		} else {
			w = Number(swfPath.substring(swfPath.indexOf('w=')+2,swfPath.indexOf('w=')+5));
			h = Number(swfPath.substring(swfPath.indexOf('h=')+2,swfPath.indexOf('h=')+5));
		}
		openYouTubePopup(swfPath,w,h);
		pageTracker._trackEvent('Multimedia', this.href.replace('http://'+document.domain,''), document.location.pathname);
    });
	
	// add querystring to URL to auto-play a video (i.e. ?play=FBN_Nov2011)
	// get querystring as an array split on "&"
//	var querystring = location.search.replace( '?', '' ).split( '&' );
	// declare object
//	var queryObj = {};
	// loop through each name-value pair and populate object
//	for ( var i=0; i<querystring.length; i++ ) {
		  // get name and value
//		  var name = querystring[i].split('=')[0];
//		  var value = querystring[i].split('=')[1];
//		  // populate object
//		  queryObj[name] = value;
//	}
//	jQuery("a[href*='" + queryObj["play"] + "']").click();
	
};

myCallBack = function (data) {
    myData = data;
	getVideos();
	if (data.tweets) {tw(data.tweets.result);}
	if (data.iid15) {yt(data.iid15.result);} else {noyt()}
};

gup = function( name ) {
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( window.location.href );
  if( results == null )
    return "";
  else
    return results[1];
};

hideYears = function () {
jQuery("#yearSelect").siblings().each( function() {
me = jQuery(this);
if(me.attr('ID').substring(0,4) == 'year') {
me.hide();
}
});
};


jQuery(function () {

    if (jQuery("#homeNews>a").outerHeight() > 20) {
        jQuery("#homeNews").css('padding-top', '5px');
    }

    jQuery.fn.spectraNav = function (params) {
        this.each(function () {
            jQuery(this).bind("mouseover", function () {
                switch (params.type) {
                    case 'primary':
                        //stopTimeOut();
                        //secondaryOff();
                        jQuery(this).children('a').css("color", params.color);
                        jQuery(this).css("background-image", params.bg);
                        //jQuery(this).children('div').show();
                        break;
                    case 'dropdown':
                        //stopTimeOut();
                        break;
                    default:
                        //stopTimeOut();
                }
            });
            jQuery(this).bind("mouseout", function () {
                switch (params.type) {
                    case 'primary':
                        //timeOuts["mouseOut"] = setTimeout("secondaryOff()", 500);
                        break;
                    default:
                        //timeOuts["mouseOut"] = setTimeout("secondaryOff()", 500);
                }
                secondaryOff();
            });
            jQuery(this).bind('click', function () {
                window.location.href = jQuery(this).children('a').attr('href');
            });
            jQuery(this).find("*").bind("mouseover", function () {
                //stopTimeOut();
            });
        });
    };

    jQuery("#primaryNav ul li.operations").spectraNav({ type: 'primary', color: '#53a8c8', bg: 'url(http://www.spectraenergy.com/images/menuArrowYellow.png)' });
    jQuery("#primaryNav ul li.naturalGas").spectraNav({ type: 'primary', color: '#0071bc', bg: 'url(http://www.spectraenergy.com/images/menuArrowBlue.png)' });
    jQuery("#primaryNav ul li.operations").spectraNav({ type: 'primary', color: '#a2722c', bg: 'url(http://www.spectraenergy.com/images/menuArrowOrange.png)' });
    jQuery("#primaryNav ul li.investors").spectraNav({ type: 'primary', color: '#53a8c8', bg: 'url(http://www.spectraenergy.com/images/menuArrowYellow.png)' });
    jQuery("#primaryNav ul li.responsibility").spectraNav({ type: 'primary', color: '#34b233', bg: 'url(http://www.spectraenergy.com/images/menuArrowGreen.png)' });
    jQuery("#primaryNav ul li.news").spectraNav({ type: 'primary', color: '#57068c', bg: 'url(http://www.spectraenergy.com/images/menuArrowPurple.png)' });
    jQuery("#primaryNav ul li.careers").spectraNav({ type: 'primary', color: '#e00034', bg: 'url(http://www.spectraenergy.com/images/menuArrowRed.png)' });
    jQuery("#primaryNav ul li.aboutUs").spectraNav({ type: 'primary', color: '#512b16', bg: 'url(http://www.spectraenergy.com/images/menuArrowBrown.png)' });

    //jQuery("#facebook").click(function () { window.open('http://www.facebook.com/spectraenergycorp'); });
    //jQuery("#twitter").click(function () { window.open('http://www.twitter.com/spectraenergy'); });
    //jQuery("#youtube").click(function () { window.open('http://www.youtube.com/user/SpectraEnergy'); });

    jQuery("#contacts").bind('change', function () {
        if (jQuery(this).val() == 'ug') {
            jQuery("#number").html('');
            window.open('http://www.uniongas.com/contact-us/emergency');
        } else {
            jQuery("#number").html((jQuery(this).val()));
        }
    });

    jQuery(".prettyTable tr:even").css("background-color", "#ccc").hover(function() {
            $(this).css('background-color', '#ffc');
        },
        function() {
            $(this).css('background-color', '#ccc');
        });
    jQuery(".prettyTable tr:odd").css("background-color", "#fff").hover(function() {
            $(this).css('background-color', '#ffc');
        },
        function() {
            $(this).css('background-color', '#fff');
        });
    jQuery("#inlineContent img.cutline,#inlineContent img.cutlineRight,#inlineContent img.cutlineLeft").each(function () {
        my = jQuery(this);
		if($.browser.msie){setTop=my.height()+6}else{setTop=my.height()+6};
        my.before('<div class="cutlineBox" style="left:0;width:' + (my.width() - 5) + 'px;margin-left:' + (my.offset().left + 5) + 'px;margin-top:' + setTop + 'px;"><p style="margin-top:0;">' + my.attr("alt") + '</p></div>');
		my.attr("title","");
        my.css('padding-bottom', my.prev().innerHeight() + 8 + 'px'); 
    });

    //var thedate = ;
    jQuery("#theyear").html(new Date().getFullYear());

    /*stopTimeOut = function () {
    for (key in timeOuts) {
    clearTimeout(timeOuts[key]);
    }
    }*/

    secondaryOff = function () {
        jQuery("#primaryNav>ul>li>a").css("color", "#757678");
        //jQuery("#primaryNav>ul>li>div").hide();
        jQuery("#primaryNav>ul>li").css("background-image", "url(http://www.spectraenergy.com/images/menuArrowGrey.png)");
    }


	// Home Page "Carousel" Images 
    var posArray = [-268, -180, -330, -20];
    jQuery.tools.tabs.addEffect("horizontal", function (f, e) {
        this.getCurrentPane().toggleClass('active').animate({
            width: 108,
            height: 290,
            marginTop: 29,
            backgroundPosition: posArray[this.getCurrentPane().index()] + 'px 0px'
        }, function () {
            //d(this).hide()
        });
        this.getCurrentPane().find("div.handle").css('display', 'block');
        this.getCurrentPane().find("div.bpane").css('display', 'none');
        this.getPanes().eq(f).toggleClass('active').animate({
            width: 640,
            height: 345,
            marginTop: 0,
            backgroundPosition: '0px 0px'
        }, function () {
            e.call()
        });
        this.getPanes().eq(f).find("div.handle").css('display', 'none');
        this.getPanes().eq(f).find("div.bpane").css('display', 'block');
    });

    jQuery("#accordion").tabs("#accordion div", {
        tabs: 'div.handle',
        effect: 'horizontal'
    });

    jQuery(".inlineAccordionContent a").each(function () {
        my = jQuery(this);
        my.addClass("maillink").attr("title", "Send an email to " + my.text()); 
    });


    jQuery('#searchGo').bind("click", function (e) {
        if (window.document.search.searchstring.value != '' && window.document.search.searchstring.value != 'Search this site...') { window.document.search.submit() } else { window.document.search.searchstring.value = ''; window.document.search.searchstring.focus(); };
    });
    jQuery("#search2").bind("focus", function (e) {
        if (jQuery("#search2").val() == 'Search this site...') jQuery("#search2").val('');
    });
    jQuery('#printUtility').bind("click", function (e) {
        window.print();
    });
	
	jQuery("#yearSelect").change( function() {hideYears();jQuery("#year"+jQuery("#yearSelect option:selected").val()).show();});

	hideYears();

	if ((x=gup("year")) != '') {jQuery("#year"+x).show();jQuery("#yearSelect option[value="+x+"]").attr("selected", true);} else {jQuery("#yearSelect").next().show();}
	
	jQuery("a.tooltip-link").tooltip({position:"bottom center",effect:"fade",opacity:0.9});
	
	jQuery("a.videoPlayer").bind("click", function (event) {
        event.preventDefault();
		swfPath = this.getAttribute("href", 2);
		if (swfPath.indexOf('http://www.spectraenergy.com/js/www.youtube.com')>-1) {
			w = 640;
			h = 360;
		} else {
			w = Number(swfPath.substring(swfPath.indexOf('w=')+2,swfPath.indexOf('w=')+5));
			h = Number(swfPath.substring(swfPath.indexOf('h=')+2,swfPath.indexOf('h=')+5));
		}
		openYouTubePopup(swfPath,w,h);
		pageTracker._trackEvent('Multimedia', this.href.replace('http://'+document.domain,''), document.location.pathname);
    });

	
	//enlarge map for project pages
	if (jQuery(".openMap").length) {
		openImg = jQuery(".openMap").attr('href');
		jQuery('#inlineContent').append('<img class="printOnly rawImage" id="enlargeImg" src="' + openImg + '" />');
		jQuery('.openMap').removeAttr('onclick').attr('title', 'Click to view larger version.');
	}
	jQuery(".openMap").click(function(e) {
		e.preventDefault(); 
		var width = jQuery("#enlargeImg").outerWidth();
		var height = jQuery("#enlargeImg").outerHeight();
		width = width+20;
		height = height+60;
		jQuery("#putcontenthere").empty().append(jQuery("#enlargeImg").clone().css({display: "block"}));
		showsize = "TB_inline?height=" + height + "&width=" + width + "&inlineId=video";
		tb_show('', showsize);
	});

	//case study popups for Sustainability
	jQuery(".footerChunk :last-child").css("margin-bottom","0");
	jQuery("a[href^='#CaseStudy']").click(function(e) {
		e.preventDefault(); 
		var divID = jQuery(this).attr('href');
		var divChild = jQuery(divID).children(':first-child').clone();
		jQuery(divChild).appendTo('body').css({display: "block", width: "592px", marginBottom: "0", overflow: "auto"});
		if((jQuery(divChild).outerHeight()+140) > jQuery(window).height()){
			jQuery(divChild).css({height: jQuery(window).height()-140+"px"});
		}
		var width = jQuery(divChild).outerWidth();
		var height = jQuery(divChild).outerHeight();
		width = width+20;
		height = height+56;
		jQuery("#putcontenthere").html(divChild).prepend("<h3 class='callout'>Case Study</h3>").css("text-align","left").prev().addClass("caseStudyClose");
		showsize = "TB_inline?height=" + height + "&width=" + width + "&inlineId=video";
		tb_show('', showsize);
	});
	
	//custom heading styles for Sustainability
//	if (location.pathname.indexOf("/Sustainability/Economic/")>-1) {
//		copyHead = jQuery("#inlineContent h2:first").clone();
//		jQuery("#inlineContent h2:first").addClass("printOnly");
//		jQuery("#inlineContent").prepend(copyHead);
//		jQuery("#inlineContent h2:first").wrap('<div class="EconomicHead shadow" />')
//	}
//	if (location.pathname.indexOf("/Sustainability/Environment/")>-1) {
//		copyHead = jQuery("#inlineContent h2:first").clone();
//		jQuery("#inlineContent h2:first").addClass("printOnly");
//		jQuery("#inlineContent").prepend(copyHead);
//		jQuery("#inlineContent h2:first").wrap('<div class="EnvironmentHead shadow" />')
//	}
//	if (location.pathname.indexOf("/Sustainability/Social/")>-1) {
//		copyHead = jQuery("#inlineContent h2:first").clone();
//		jQuery("#inlineContent h2:first").addClass("printOnly");
//		jQuery("#inlineContent").prepend(copyHead);
//		jQuery("#inlineContent h2:first").wrap('<div class="SocialHead shadow" />')
//	}
	//custom heading styles for Sustainability
	if (location.pathname.indexOf("/Sustainability/Economic/")>-1) {
		jQuery("body").addClass("Economic");
	}
	if (location.pathname.indexOf("/Sustainability/Environmental/")>-1) {
		jQuery("body").addClass("Environment");
	}
	if (location.pathname.indexOf("/Sustainability/Social/")>-1) {
		jQuery("body").addClass("Social");
	}

	reconfigureLinks();
	
});

/*
jQuery-SelectBox
  
Traditional select elements are very difficult to style by themselves, 
but they are also very usable and feature rich. This plugin attempts to 
recreate all selectbox functionality and appearance while adding 
animation and stylability.
  
This product includes software developed 
by RevSystems, Inc (http://www.revsystems.com/) and its contributors
  
Please see the accompanying LICENSE.txt for licensing information.
*/
(function (a) { jQuery.fn.borderWidth = function () { return a(this).outerWidth() - a(this).innerWidth() }; jQuery.fn.marginWidth = function () { return a(this).outerWidth(true) - a(this).outerWidth() }; jQuery.fn.paddingWidth = function () { return a(this).innerWidth() - a(this).width() }; jQuery.fn.extraWidth = function () { return a(this).outerWidth(true) - a(this).width() }; jQuery.fn.offsetFrom = function (b) { return { left: a(this).offset().left - b.offset().left, top: a(this).offset().top - b.offset().top} }; jQuery.fn.maxWidth = function () { var b = 0; a(this).each(function () { if (a(this).width() > b) { b = a(this).width() } }); return b }; jQuery.fn.sb = function (b) { if (a.browser.msie && a.browser.version < 7) { return a(this) } b = a.extend({ acTimeout: 800, animDuration: 300, ddCtx: "body", fixedWidth: true, maxHeight: false, maxWidth: false, noScrollThreshold: 100, placement: "before", selectboxClass: "selectbox", arrowMarkup: "<span class='arrow_btn'><span class='interior'><span class='arrow'></span></span></span>", optionFormat: function (c, d) { return a(this).text() }, optgroupFormat: function (c) { return "<span class='label'>" + a(this).attr("label") + "</span>" } }, b); a(this).each(function () { var q = a(this); var F = null; var k = null; var A = null; var g = null; function p() { F = a("<div class='" + b.selectboxClass + " " + q.attr("class") + "'></div>"); a("body").append(F); k = a("<a href='#' class='display " + q.attr("class") + "'><span class='value'>" + q.val() + "</span> <span class='text'>" + b.optionFormat.call(q.find("option:selected")[0], 0, 0) + "</span>" + b.arrowMarkup + "</a>"); F.append(k); A = a("<ul class='items " + q.attr("class") + "'></ul>"); F.append(A); if (q.children("optgroup").size() > 0) { q.children("optgroup").each(function (H) { var I = a(this); var J = a("<li class='optgroup'>" + b.optgroupFormat.call(I[0], H + 1) + "</li>"); var K = a("<ul class='items'></ul>"); J.append(K); A.append(J); I.children("option").each(function (L) { K.append("<li class='" + (a(this).attr("selected") ? "selected" : "") + " " + (a(this).attr("disabled") ? "disabled" : "") + "'><a href='#'><span class='value'>" + a(this).attr("value") + "</span><span class='text'>" + b.optionFormat.call(this, H + 1, L + 1) + "</span></a></li>") }) }) } q.children("option").each(function (H) { A.append("<li class='" + (a(this).attr("selected") ? "selected" : "") + " " + (a(this).attr("disabled") ? "disabled" : "") + "'><a href='#'><span class='value'>" + a(this).attr("value") + "</span><span class='text'>" + b.optionFormat.call(this, 0, H + 1) + "</span></a></li>") }); g = A.find("li").not(".optgroup"); A.children(":first").addClass("first"); A.children(":last").addClass("last"); q.hide(); if (b.fixedWidth) { var G = F.find(".text").maxWidth() + k.extraWidth() + 1; F.width(b.maxWidth ? Math.min(b.maxWidth, G) : G); if (a.browser.msie && a.browser.version <= 7) { g.find("a").each(function () { a(this).css("width", "100%").width(a(this).width() - a(this).paddingWidth() - a(this).borderWidth()) }) } } else { if (b.maxWidth && F.width() > b.maxWidth) { F.width(b.maxWidth) } } if (b.placement == "before") { q.before(F) } else { if (b.placement == "after") { q.after(F) } } A.hide(); if (!q.is(":disabled")) { k.click(D).focus(c).blur(E).hover(C, t); g.not(".disabled").find("a").click(h); g.filter(".disabled").find("a").click(function () { return false }); g.not(".disabled").hover(C, t); A.find(".optgroup").hover(C, t) } else { F.addClass("disabled"); k.click(function (H) { H.preventDefault() }) } F.bind("close", v); F.bind("destroy", d); q.bind("reload", function () { d(); p() }); q.focus(z) } function z() { k.focus(); return false } function x() { d(); p() } function d() { F.unbind().find("*").unbind(); F.remove(); q.unbind("reload", x).unbind("focus", z).show() } function r() { e(); a(document).unbind("click", r) } function e() { a("." + b.selectboxClass).trigger("close") } function y() { a("." + b.selectboxClass).not(F[0]).trigger("close") } function v() { g.removeClass("hover"); a(document).unbind("keyup", o); a(document).unbind("keydown", i); A.fadeOut(b.animDuration, function () { F.removeClass("open"); F.append(A) }) } function m() { var G = null; if (b.ddCtx == "self") { G = F } else { if (a.isFunction(b.ddCtx)) { G = a(b.ddCtx.call(q[0])) } else { G = a(b.ddCtx) } } return G } function s() { var I = m(); e(); F.addClass("open"); var G = u(); I.append(A); function H() { A.scrollTop(g.filter(".selected").offsetFrom(A).top - A.height() / 2 + g.filter(".selected").outerHeight(true) / 2) } if (a.browser.msie && a.browser.version < 8) { a("." + b.selectboxClass + " .display").hide().show() } if (G == "up") { A.fadeIn(b.animDuration, H) } else { if (G == "down") { A.slideDown(b.animDuration, H) } else { A.fadeIn(b.animDuration, H) } } a(document).unbind("keyup", o).keyup(o); a(document).unbind("keydown", i).keydown(i); a(document).click(r) } function u() { var M = m(); var G = 0; var K = 0; var H = ""; A.removeClass("above"); A.css({ display: "block", maxHeight: "none", position: "relative", visibility: "hidden" }); if (b.fixedWidth) { A.width(k.outerWidth() - A.extraWidth() + 1) } var J = a(window).scrollTop() + a(window).height() - k.offset().top - k.outerHeight(); var L = k.offset().top - a(window).scrollTop(); var I = k.offsetFrom(M).top + k.outerHeight(); if (A.outerHeight() <= J) { G = b.maxHeight ? b.maxHeight : J; K = I; H = "down" } else { if (A.outerHeight() <= L) { G = b.maxHeight ? b.maxHeight : L; K = k.offsetFrom(M).top - Math.min(G, A.outerHeight()); H = "up" } else { if (J > b.noScrollThreshold && J > L) { G = b.maxHeight ? b.maxHeight : J; K = I; H = "down" } else { if (L > b.noScrollThreshold) { G = b.maxHeight ? b.maxHeight : L; K = k.offsetFrom(M).top - Math.min(G, A.outerHeight()) } else { G = b.maxHeight ? b.maxHeight : "none"; K = I; H = "down" } } } } A.css({ display: "none", left: k.offsetFrom(M).left + (M[0].tagName.toLowerCase() == "body" ? parseInt(a("body").css("margin-left")) : 0), maxHeight: G, position: "absolute", top: K + (M[0].tagName.toLowerCase() == "body" ? parseInt(a("body").css("margin-top")) : 0), visibility: "visible" }); if (H == "up") { A.addClass("above") } return H } function D(G) { var H = a(this).closest("." + b.selectboxClass); if (H.is(".open")) { v(); k.focus() } else { k.focus(); s() } return false } function n() { var G = a(this); k.find(".value").html(G.find(".value").html()); k.find(".text").html(G.find(".text").html()); k.find(".text").attr("title", G.find(".text").html()); A.find("li").removeClass("selected"); G.closest("li").addClass("selected"); q.val(k.find(".value").html()).change() } function h(G) { n.call(this); r(); k.focus(); return false } var B = ""; var j = null; function f() { B = "" } function w(J) { var K = ""; var I = g.not(".disabled"); for (var H = 0; H < I.size(); H++) { var G = I.eq(H).find(".text").text(); K += G + " "; if (G.toLowerCase().match("^" + J.toLowerCase()) == J.toLowerCase()) { return I.eq(H) } } return null } function l(H) { var G = w(H); if (G != null) { n.call(G[0]); return true } return false } function i(G) { if (G.which == 38 || G.which == 40 || G.which == 8 || G.which == 32) { G.preventDefault() } } function o(H) { if (H.altKey || H.ctrlKey) { return false } var G = g.filter(".selected"); switch (H.which) { case 38: if (G.size() > 0) { if (g.not(".disabled").filter(":first")[0] != G[0]) { n.call(g.not(".disabled").eq(g.not(".disabled").index(G) - 1)[0]) } } break; case 40: if (G.size() > 0) { if (g.not(".disabled").filter(":last")[0] != G[0]) { n.call(g.not(".disabled").eq(g.not(".disabled").index(G) + 1)[0]) } } else { if (g.size() > 1) { n.call(g.eq(0)[0]) } } break; default: B += String.fromCharCode(H.keyCode); if (!l(B)) { clearTimeout(j); f() } else { clearTimeout(j); j = setTimeout(f, b.acTimeout) } break } return false } function c() { y(); F.addClass("focused"); if (!F.is(".open")) { a(document).unbind("keyup", o).keyup(o); a(document).unbind("keydown", i).keydown(i) } } function E() { F.removeClass("focused"); a(document).unbind("keyup", o); a(document).unbind("keydown", i) } function C() { a(this).addClass("hover") } function t() { a(this).removeClass("hover") } p() }) } })(jQuery);