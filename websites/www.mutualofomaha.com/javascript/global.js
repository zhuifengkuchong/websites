/*** Begin Global JavaScript ***/

/** disallow using MOO in iFrames */
if (top.location!= self.location) {
    top.location = self.location.href;
}

if (location.host.indexOf("moonew") >= 0) {
    $.google_api_key = 'ABQIAAAAb44hYBVEhSfjpytulK9cCxR34GjjYYP7tGQrc7XAPa1_DtaZeRTQlCOTQTLZwewJ0iJwK1utlFLssQ';
} else if (location.host.indexOf("itg") >= 0) {
    $.google_api_key = 'ABQIAAAAb44hYBVEhSfjpytulK9cCxQjJ8N-PxAm5eBGW4jYdUgKkFfQcxSiXZ7DtRnmmFjPhTC40-A0EcMfmA';
} else if (location.host.indexOf("cat") >= 0) {
    $.google_api_key = 'ABQIAAAAb44hYBVEhSfjpytulK9cCxRxm5I2hTitulAWAMftnM0ETNqJxhTUbko9Y3LIkyqM2ce8ZUJ88B48qA';
} else if (location.host.indexOf("www") >= 0) {
    $.google_api_key = "ABQIAAAAb44hYBVEhSfjpytulK9cCxRp0dZbVIj7yEqAqhSnq1fddhGOLRTKlZR5HUzIcmIKfM7UdgeoI78WEA";
} else {
    $.google_api_key = "ABQIAAAAb44hYBVEhSfjpytulK9cCxRHGTEq5IG99ASK7EDtJaY-qQ9wgBQm5vK4tWUVOxRbYVRbqr9dZopPRw";
}

// Google Search submit
function submitsearch(form) {
    if (document.seek.q.value=="") {
        alert('Please enter an item to search');
        document.seek.q.focus();
    } else {
        document.seek.submit();
    }
}

/**
 * Universal Analytics Event Call
 */
var analytics = {
    track: function(obj) {
        var category = obj.category || null,
            action   = obj.action || null,
            label    = obj.label || null,
            value    = obj.value || null,
            nonint   = obj.nonint || null;

        if (nonint !== null) {
            dataLayer.push({
              'event': 'trackEvent',
              'eventCategory': category,
              'eventAction': action,
              'eventLabel': label,
              'eventValue': value,
              'eventNonInt': nonint
            });
        } else if (value !== null) {
            dataLayer.push({
              'event': 'trackEvent',
              'eventCategory': category,
              'eventAction': action,
              'eventLabel': label,
              'eventValue': value
            });
        } else if (label !== null) {
            dataLayer.push({
              'event': 'trackEvent',
              'eventCategory': category,
              'eventAction': action,
              'eventLabel': label
            });
            // ga('send', 'event', category, action, label);
        } else {
            dataLayer.push({
              'event': 'trackEvent',
              'eventCategory': category,
              'eventAction': action
            });
        }
    },
    pageview: function(obj) {
        var path = obj.path || location.pathname;

        if('undefined' !== typeof ga && path !== null) {
            dataLayer.push({
              'event': 'trackVirtualPageview',
              'virtualPageview': path
            });
        } else {
            // console.log({path:path});
        }
    }
};

function printPage() {
    analytics.pageview({path: '/util/print/?page='+location.pathname});
    window.print();
}

function toggle(id) {
    $('#'+id).toggle();
}

// function display PDF tag
$.fn.trackPDF = function() {
    var anchors = $('a',$(this));

    for(i=0; i<anchors.length; i++) {
        var url = anchors.eq(i).attr('href');
        isPDF = /(\.pdf)$/;
        if(isPDF.test(url)) {
            var parentEls = false;
            parentEls = anchors.eq(i).parents()
                          .map(function () {
                                if (this.tagName.toLowerCase() == "div" && $(this).hasClass("subnav")) {
                                    return true;//this.tagName;
                                }
                              })
                          .get().join(", ");
                          //alert(parentEls);

            var str_output = '&nbsp;<small>(PDF)<\/small>';
            if (parentEls) {
                anchors.eq(i).html(anchors.eq(i).text() + str_output);
            } else {
                anchors.eq(i).after(str_output);
            }
        }
    }

    anchors.click(function() {
        var url = $(this).attr('href');
        isPDF = /(\.pdf)$/;
        if(isPDF.test(url)) {
            analytics.pageview({path: url});
        }
    });
}

//declare vars
var ctaname = "#cta_scroll",
    menuYloc = null;

/*function callback(hash)
{
    // search URL for faq and select question # after hash
    if (hash != '' && hash.indexOf("question") >= 0) {
        //get the index
        faqSelect = hash.substr(8);
        faqSelect = parseInt(faqSelect);

        $('.answer > ul > li').hide();
        $('.answer > ul > li:nth-child(' + faqSelect + ')').show();
        $('.question h4 a').removeClass('on');
        $('.question li:nth-child(' + faqSelect + ') a').addClass('on');

    } else {
        $('.answer > ul > li').hide();
        $('.answer > ul > li:nth-child(1)').show();
        $('.question h4 a').removeClass('on');
        $('.question li:nth-child(1) a').addClass('on');
    }
}*/

// Validate Buy Online Link
function validateBuyOnline() {

    if ($("#select-product").val().length > 0) {
        $("#buy-online").attr("action",$("#select-product").val());
        $("#buy-online").submit();
    } else {
        $("#select-product").focus();
        $("#submit-buy-online").attr("disabled",false);
        $("#submit-buy-online").parent("td").removeClass("disabled");
        //$("#modal-go").parent("td").removeClass("disabled");
    }

}

// Validate Banking Link
function validateBankingProducts() {
    if ($("#banking-products").val().length > 0) {
        $("#banking-form").attr("action",$("#banking-products").val()).submit();
    } else {
        $("#banking-products").focus();
        $("#submit-banking-products").attr("disabled",false).parent("td").removeClass("disabled");
    }
}

// begin document load
$(document).ready(function() {

    $('a.provider').bind('click', function() {
        $.cookie("usertype", "provider");
    });

    $('a.retirementServices').bind('click', function() {
        $.cookie("usertype", "retirementServices");
    });

    $('a.salesProfessional').bind('click', function() {
        $.cookie("usertype", "salesProfessional");
    });


    // if JavaScript Enabled
    $('#content .utilities').removeClass('displayNone');
    $('#find-links').removeClass('displayNone');
    $('.google-search').append('<a href="javascript:submitsearch(0)"><span>SEARCH</span></a>');

    // Content Util Print btn
    $('#print_util').click(function() { printPage(); });

    // Adjust Photo/Caption div to the width of the photo
    $('div.photo').css({ width: $('div.photo img').attr("width")});

    // remove borders from radios and checkboxes
    $('input:radio').addClass('no-border');
    $('input:checkbox').addClass('no-border');

    // FAQs
    $("a[rel='faq']").click(function() {
        var q = $("a[rel='faq']").index($(this));
        // add 1 to q instead of starting w/ 0
        q = q + 1;

        $('.answer > ul > li').hide();
        $('.answer > ul > li:nth-child(' + q + ')').show();
        $('.question h4 a').removeClass('on');
        $(this).addClass('on');

        return false;
    });

    if (window.location.hash != '' && window.location.hash.indexOf("question") >= 0) {
        //get the index
        faqSelect = window.location.hash.substr(8);
        faqSelect = parseInt(faqSelect);
        $('.answer > ul > li').hide();
        $('.answer > ul > li:nth-child(' + faqSelect + ')').show();
        $('.question h4 a').removeClass('on');
        $('.question li:nth-child(' + faqSelect + ') a').addClass('on');
    } else {
        $('.answer > ul > li').hide();
        $('.answer > ul > li:nth-child(1)').show();
        $('.question h4 a').removeClass('on');
        $('.question li:nth-child(1) a').addClass('on');
    }

    // Tabs
    $('#box-about').tabs();
    $('#forms_service').tabs();
    $('#banner.careers').tabs();
    $('.content-title').tabs({
        select: function(event, ui) {
            window.location.hash = ui.tab.hash;
            var url = location.pathname + ui.tab.hash;
            analytics.pageview({path: url});
        }
    });
    $('.content-tab').tabs();

    if (window.location.hash != '' && window.location.hash.indexOf("section") >= 0) {
        //get the index
        $(".content-title").tabs('select',window.location.hash);

        setTimeout(function() {
            if (location.hash) {
                window.scrollTo(0, 0);
            }
        }, 1);
    } else if (window.location.hash == '') {
        $(".content-title").tabs('select',0);
    }

    if (window.location.hash != '' && window.location.hash.indexOf("section") < 0) {
        //get the index
        $(".content-tab").tabs('select',window.location.hash);
    } else {
        $(".content-tab").tabs('select',0);
    }

    // Whole Life DTC
    $('a.buy-online-link').bind('click',function() {
        $('.content-title').tabs('select','#buy-online');
        $('#dtc-title-phone, #dtc-title-or, .life-dtc-disclaimer').removeClass('display-none');
        if (url_string.indexOf("/whole-life-insurance/") >= 0 && (url_string.indexOf("http://www.mutualofomaha.com/whole-life-insurance/guaranteed.php") >= 0 || url_string.indexOf("http://www.mutualofomaha.com/whole-life-insurance/childrens.php") >= 0)) {
            $('#dtc-title-btn').removeClass('display-none');
        } else {
            $('#dtc-title-or').addClass('display-none');
        }

        $.cookie("lifedtc", "dtc=true", { path: '/', expires: 3 });
        $('#agent-title-btn, #agent-title-or, #agent-title-phone, .life-alg-disclaimer').addClass('display-none');
        return false;
    });

    $('a.licensed-agent-link').bind('click',function() {
        $('.content-title').tabs('select','#agency');
        $('#dtc-title-btn, #dtc-title-or, #dtc-title-phone, .life-dtc-disclaimer').addClass('display-none');
        $('#agent-title-btn, #agent-title-or, #agent-title-phone, .life-alg-disclaimer').removeClass('display-none');
        $.cookie("lifedtc", "dtc=false", { path: '/', expires: 3 });
        return false;
    });

    var url_string = String(window.location);

    if ($.cookie("lifedtc")) {
        var dtc_cookie = $.cookie("lifedtc");
    } else {
        var dtc_cookie = "";
    }


    if (dtc_cookie.indexOf("dtc=true") >= 0 && url_string.indexOf("/whole-life-insurance/") >= 0 && url_string.indexOf("http://www.mutualofomaha.com/whole-life-insurance/express.php") == -1) { // select buy online tab
        $('.content-title').tabs('select','#buy-online');
    } else if (dtc_cookie.indexOf("dtc=false") >= 0 && url_string.indexOf("/whole-life-insurance/") >= 0 && url_string.indexOf("http://www.mutualofomaha.com/whole-life-insurance/express.php") == -1) {
        $('.content-title').tabs('select','#agency');
    }

    if ((url_string.indexOf("/whole-life-insurance/") >= 0 && url_string.indexOf("http://www.mutualofomaha.com/whole-life-insurance/express.php") == -1 && $('#agency').hasClass('ui-tabs-hide')) || url_string.indexOf("http://www.mutualofomaha.com/whole-life-insurance/childrens.php") >= 0) {
        $('#dtc-title-phone, .life-dtc-disclaimer').removeClass('display-none');
        if (url_string.indexOf("/whole-life-insurance/") >= 0 && (url_string.indexOf("http://www.mutualofomaha.com/whole-life-insurance/guaranteed.php") >= 0 || url_string.indexOf("http://www.mutualofomaha.com/whole-life-insurance/childrens.php") >= 0)) {
            $('#dtc-title-btn, #dtc-title-or').removeClass('display-none');
        }

        $('#agent-title-btn, #agent-title-or, #agent-title-phone, .life-alg-disclaimer').addClass('display-none');
    } else if (url_string.indexOf("/whole-life-insurance/") >= 0 && url_string.indexOf("http://www.mutualofomaha.com/whole-life-insurance/express.php") == -1 && $('#buy-online').hasClass('ui-tabs-hide')) {
        $('#dtc-title-btn, #dtc-title-or, #dtc-title-phone, .life-dtc-disclaimer').addClass('display-none');
        $('#agent-title-btn, #agent-title-or, #agent-title-phone, .life-alg-disclaimer').removeClass('display-none');
    }

    if (url_string.indexOf("/whole-life-insurance/") >= 0 && url_string.indexOf("http://www.mutualofomaha.com/whole-life-insurance/express.php") == -1 && url_string.indexOf("view=agent") != -1) {
        $('.content-title').tabs('select','#agency');
        //show correct cta in blue bar
        $('#dtc-title-btn, #dtc-title-or, #dtc-title-phone, .life-dtc-disclaimer').addClass('display-none');
        $('#agent-title-btn, #agent-title-or, #agent-title-phone, .life-alg-disclaimer').removeClass('display-none');
        //alert("executed?");
    }

    /**
     * Click functions for /cancer-insurance
     * @author Andy Hughes
     * @date 09/02/14
     **/

    // Cancer Heart Attack and Stroke Insurance
    $('a.cancer-dtc-link').bind('click',function() {
        $('.content-title').tabs('select','#paid-upon-diagnosis');
        $('#dtc-title-btn, #dtc-title-or, #dtc-title-phone, .life-dtc-disclaimer').addClass('display-none');
        $('#agent-title-btn, #agent-title-or, #agent-title-phone, .life-alg-disclaimer').removeClass('display-none');
        $.cookie("lifedtc", "dtc=true", { path: '/', expires: 3 });
        // analytics
        analytics.track({category:'/cancer-insurance',action:'Tab Click',label:'Paid Upon Diagnosis (DTC)'});
        return false;
    });

    $('a.cancer-agent-link').bind('click',function() {
        $('.content-title').tabs('select','#treatment-coverage');
        $('#dtc-title-phone, .life-dtc-disclaimer').removeClass('display-none');
        $('#agent-title-btn, #agent-title-or, #agent-title-phone, .life-alg-disclaimer').addClass('display-none');
        $.cookie("lifedtc", "dtc=false", { path: '/', expires: 3 });
        // analytics
        analytics.track({category:'/cancer-insurance',action:'Tab Click',label:'Treatment Coverage (Agency)'});
        return false;
    });

    //call pdf tag function
    $("#content-wrapper").trackPDF();
    //call mobile tracking
    $("a.mobile-number").click(function() {
        var url = location.pathname + "?clicktocall=1&number=" + $(this).attr("href").replace("tel:","");
        analytics.pageview({path: url});
    });

    // Clear table borders in content section
    $("#content .table-border th:first-child").addClass("border-left-none");
    $("#content .table-border th:last-child").addClass("border-right-none");
    $("#content .table-border th").addClass("border-top-none");

    $("#content .table-border tr:first td").addClass("border-top-none");

    $("#content .table-border tr td:first-child").addClass("border-left-none");
    $("#content .table-border tr td:last-child").addClass("border-right-none");
    $("#content .table-border tr:last td").addClass("border-btm-none");

    //Add alternating rows to table
    var rows = $("#content .zebra-cols tbody tr");
    rows.each(function() {
        $('td:nth-child(2n)', $(this)).addClass("dft");
        $('td:nth-child(2n+3)', $(this)).addClass("alt");
    });

    var lifeText = "life insurance",
        medSuppText = "medicare supplement insurance",
        diText = "disability insurance";
        ltcText = "long-term care insurance";

    // Build search term array and echo into search input
    var searchTerms = new Array("401k","agents and brokers","agent login","broker login","about mutual of omaha", "company ratings","company history","foundation","mutual of omaha foundation","aha moment","annuities","group annuities","bank","careers","banking careers","careers in banking","careers in sales","careers in internships","internships","investments","current job openings","sales careers", "critical illness insurance","customer access","customer service","customer service and forms","forms","forms for individual policyholders","forms for plan members","forms for employees","forms for plan administrators","forms for employers","life insurance", "life insurance whole", "life insurance quote", "life insurance universal", "locate an agent", "locate a sales office", medSuppText, medSuppText+" glossary", medSuppText+" quote", medSuppText+" plans", "medigap benefits", "medicare select", medSuppText+" when to buy", "disability insurance", "long-term care insurance", "long-term disability insurance", "quote for disability insurance", "quote for life insurance", "quote for "+medSuppText, "quote for long-term care insurance",ltcText+" see why people buy",ltcText+" costs", "term life insurance","universal life insurance","life insurance term","whole life insurance","usa swimming sponsorship","wild kingdom","special risk","K-12","catastrophic","how much life insurance do i need","how much disability insurance do i need", "life insurance one family's story", "life insurance learn the basics","disability insurance one family's story",diText+" glossary",diText+" quote",diText+" product overview","ratings","claims","claim forms","cancer insurance","part a medicare","part b medicare","part c medicare","part d medicare","medicare eligibility","medicare basics","medicare supplement basics","medicare out of pocket costs","medicare supplement selecting a policy","short-term disability insurance","group dental insurance","retirement services","institutional investments","eap","employee assistance program","group benefits","children's life insurance","create account","account access for customers","account access for sales professionals","account access for plan administrators","broker commission statements","commission statements","online commission statements","group insurance","provider","provider access","health care","eligibility","benefits");

    $("#q").bind("focus blur", function(ev){
        switch(ev.type) {
            case 'focus':
                if ($(this).val().toUpperCase() == "SEARCH") { $(this).val(""); }
            break;
            case 'blur':
                if ($(this).val().toUpperCase() == "SEARCH" || $(this).val() == "") { $(this).val("Search"); }
            break;
        }
    });

    // echo autocomplete into input
    $("#q").autocomplete(searchTerms.sort(), {
        width: 350,
        minChars: 1,
        max: 10,
        autoFill: false,
        mustMatch: false,
        matchContains: false,
        scrollHeight: 220
    }).result(function(event, data, formatted) {
        //$(this).parents().find('form:first').submit();
    });
    /*.result(function(event, item, formatted) {
        $(this).parents().find('form:first').submit();
    });*/

    $("#search form").submit(function() {
        $("#search-btn").attr("disabled",false);
        if ($("#q").val().length > 0 && $("#q").val().toUpperCase() != "SEARCH") {
            if (validateAlphaNum($("#q").val())) {
                $("#search-btn").attr("disabled",false);
                return true;
            } else {
                $("#q").focus();
                $("#search-btn").attr("disabled",false);
                return false;
            }
        } else {
            $("#q").focus();
            $("#search-btn").attr("disabled",false);
            return false;
        }
    });

    // qTips
    /* generic site-wide qtip style */
    $.fn.qtip.styles.qtip = { // Last part is the name of the style (qtip)
        background: 'none',
        fontSize: 12,
        color: '#444444',
        border: 0
    }

    /* Email Friend Utilities Overlay */
    $.fn.qtip.styles.email_style = { // Last part is the name of the style (email_style)
        width: 249,
        height: 232,
        padding: 5,
        background: 'none',
        border: {
            width: 0,
            radius: 0
        }
    }

    $('#email_util').qtip({
        content: {
            url: 'http://www.mutualofomaha.com/includes/utilities/email.php'
        },
        style: 'email_style',
        position: {
            target: $('#email_util'),
            corner: {
                tooltip: 'topMiddle',
                target: 'topMiddle'
            },
            adjust: { x: 8, y: -14 }
        },
        show: {
            when: 'click',
            delay: 0,
            effect: { length: 0 },
            solo: true
            },
        hide: { when: 'unfocus',fixed: true, delay: 1000, effect: { length: 0 } },
        api: {
           onShow: function() {
                $('#yourEmail').select();
                analytics.pageview({path: '/util/email/show'});
           },
            onHide: function() {
                analytics.pageview({path: '/util/email/hide'});
           }
        }
    });

    // Share Page Utilities Overlay
    $.fn.qtip.styles.share_style = { // Last part is the name of the style
        width: 136,
        height: 232,
        padding: 5,
        background: 'none',
        border: {
            width: 0,
            radius: 0
        }
    }

    $.page_title = document.title;

    if (window.location.href.indexOf("http://www.mutualofomaha.com/javascript/quote.php") > -1 && window.location.href.indexOf("#") > -1) {
        $.page_url = window.location.href.substr(0, window.location.href.indexOf('#'));

    } else {
        $.page_url = window.location.href;
    }

    $.page_title_encoded = encodeURIComponent($.page_title);
    $.page_url_encoded = encodeURIComponent($.page_url);

    $('#share_util').qtip({
        content: {
            url: 'http://www.mutualofomaha.com/includes/utilities/share.php'
        },
        style: 'share_style',
        position: {
            target: $('#share_util'),
            corner: {
                tooltip: 'topMiddle',
                target: 'topLeft'
            },
            adjust: { x: 5, y: -14 }
        },
        show: {
            when: 'click',
            delay: 0,
            effect: { length: 0 },
            solo: true
            },
        hide: { when: 'unfocus',fixed: true, delay: 1000, effect: { length: 0 } },
        api: {
           onShow: function() {
                analytics.pageview({path: '/util/share/show'});
           },
            onHide: function() {
                analytics.pageview({path: '/util/share/hide'});
           }
        }
    });

    // Find links
    $("#utilities .links ul li a").bind('click', function(ev) {
        return false;
    });

    // Find Overlay Links
    $.fn.qtip.styles.find_style = { // Last part is the name of the style
        width: 300,
        height: 145,
        padding: 5,
        background: 'none',
        border: {
            width: 0,
            radius: 0
        }
    }

    // Setup a content,focus array
   var contents = [
      'http://www.mutualofomaha.com/includes/find/forms.php',
      'http://www.mutualofomaha.com/includes/find/agents.php',
      'http://www.mutualofomaha.com/includes/find/quotes.php',
      'http://www.mutualofomaha.com/includes/find/careers.php'
   ];

   var focus_on = [
      '#coverage0',
      '#agent-lookup',
      '#atm-zip',
      '#sales-career-zip'
   ];

   var find_on = [
      'a[rel="find-modal"]:eq(0)',
      'a[rel="find-modal"]:eq(2)',
      'a[rel="find-modal"]:eq(2)',
      'a[rel="find-modal"]:eq(2)'
   ];

    // Set hide bg var
    var hideBG = true;

    $('a[rel="find-modal"]').bind('mousedown', function(inputChange) {
        switch(inputChange.type) {
            case 'mousedown':
                if ($('#find-bg').css('display') == 'block') {
                    hideBG = false;
                } else {
                    hideBG = true;
                }
            break;
        }
    });

   // Create a common qTip configuration
   var config = {
      content: {
      },
      style: 'find_style',
      position: {
        target: $('#find-links'),
        corner: {
            tooltip: 'topLeft',
            target: 'bottomLeft'
        },
        adjust: { x: 15, y: 15 }
      },
      show: {
        when: 'click', // Show it on click
        solo: true // And hide all other tooltips
      },
    hide: { when: { event: 'unfocus' } },
      api: {
         beforeRender: function() {
            $('#loading').show();
         },
         beforeShow: function()
         {
            // Fade in the modal "blanket" using the defined show speed
            if ($('#find-bg').css('display') != 'block') {
                $('#find-bg').show();
            }
            hideBG = true;
            $('#content .utilities ul').css({'z-index' : '4000'});
         },
         onShow: function() {
         },
         beforeHide: function() {
         },
         onHide: function()
         {
            if(hideBG) {
                $('#find-bg').hide();
                $('#content .utilities ul').css({'z-index' : '7000'});
            }

            hideBG = true;
         }
      }
   };

   $('a[rel="find-modal"]').each(function(i) // Notice the i variable
   {
        // Create a new 'config' object and add the right content from our content array
        var options = $.extend(true, {}, config, { content: { url: contents[i] }, api: { onShow: function() { $(focus_on[i]).select(); + '/n' + $('a[rel="find-modal"]:eq(' + i + ')').addClass('on'); $('#loading').hide(); },  beforeHide: function() { /* $(focus_on[i]).focus(); + '/n' + */ $('a[rel="find-modal"]:eq(' + i + ')').removeClass('on'); } } });
        // Create the tooltip
        $(this).qtip(options);
   });

   // Create the modal backdrop on document load so all modal tooltips can use it
   $('<div id="find-bg"><div id="loading">')
      .css({
         zIndex: 5000  // Make sure the zIndex is below 6000 to keep it below tooltips!
      })
      .appendTo('#utilities') // Append to the utility div
      .hide(); // Hide it initially

    // Show Hide Edit State in the Title Utilities Section
    $("#text-state-wrapper").bind("click", function (inputChange) {
        switch(inputChange.type) {
            case 'click':
                $(this).hide();
                $("#edit-state-wrapper").show();
                $("#content-state-select").focus();
            break;
        }
    });

    $("#content-state-select").bind("select blur", function (inputChange) {
        switch(inputChange.type) {
            case 'select':
                $("#edit-state-wrapper").hide();
                $("#text-state-wrapper").show();
            break;
            case 'blur':
                $("#edit-state-wrapper").hide();
                $("#text-state-wrapper").show();
            break;
        }
    });

    // Accordions (homepage & sidebar)
    $(".sidebar-home .accordion, .content-copy .accordion").accordion({ header: 'h2', autoHeight: true, active: 0, collapsible: false });

    //Sidebar toggle for all three-column pages.
    $.fn.blindToggle = function(speed, easing, callback) {
        var h = this.height() + parseInt(this.css('paddingTop')) + parseInt(this.css('paddingBottom'));
        return this.animate({marginTop: parseInt(this.css('marginTop')) < 0 ? 0 : -h}, speed, easing, callback);
    };

    var $mask = $('.sidebar .toggle h3').next().wrap('<div class="mask"></div>');
    $('.sidebar .toggle h3').click(function() {
        $mask.blindToggle('slow');
    });

    /*
    $('#link').click(function() {
        $("#sitemap_products_nav a").trigger('click');
    });
    */

    // SITEMAP NAVIGATION
    $(".sitemap_title a").bind('click', function(){

        if ($('#sitemap').css('display') == 'block') {
        $("#sitemap").removeClass('displayNone').slideUp("slow");
            $(".sitemap_arrow").addClass('displayNone');
        } else {
        $('#sitemap').slideDown('slow');
            $('.sitemap_arrow').removeClass('displayNone');
            if ($('#sitemap_products_links').hasClass("displayNone")){
                $('#sitemap_tools_links').fadeTo(350,0).addClass('displayNone').css('display','none');
                $('#sitemap_about_links').fadeTo(350,0).addClass('displayNone').css('display','none');
                $(".sitemap_arrow").animate({marginLeft:'125px'},'fast');
                $('#sitemap_products_links').removeClass('displayNone').fadeTo('slow',1);
            }
        }

        $('#sitemap_products_nav a').removeClass('clicked');
        $('#sitemap_tools_nav a').removeClass('clicked');
        $('#sitemap_about_nav a').removeClass('clicked');
        $(this).addClass('clicked');
    });

    $("#sitemap_products_nav a").bind('click', function(){
        $(".sitemap_arrow").animate({marginLeft:'125px'},'fast');

        $('#sitemap_tools_links').fadeTo(350,0).addClass('displayNone').css('display','none');
        $('#sitemap_about_links').fadeTo(350,0).addClass('displayNone').css('display','none');


        if ($('#sitemap_products_links').hasClass("displayNone")){
            $('#sitemap_products_links').removeClass('displayNone').fadeTo('slow',1);
        }

        if ($('#sitemap').css('display') == 'block' && $('#sitemap_products_nav a').hasClass('clicked')) {
            $("#sitemap").removeClass('displayNone').slideUp("slow");
            $(".sitemap_arrow").addClass('displayNone');
        } else {
            $('#sitemap').slideDown('slow');
            $('.sitemap_arrow').removeClass('displayNone');
        }

        $('.sitemap_title a').removeClass('clicked');
        $('#sitemap_tools_nav a').removeClass('clicked');
        $('#sitemap_about_nav a').removeClass('clicked');
        $(this).addClass('clicked');
    });

    $("#sitemap_tools_nav a").bind('click', function(){
        $(".sitemap_arrow").animate({marginLeft:'240px'},'fast');

        $('#sitemap_products_links').fadeTo(350,0).addClass('displayNone').css('display','none');
        $('#sitemap_about_links').fadeTo(350,0).addClass('displayNone').css('display','none');

        if ($('#sitemap').css('display') == 'block' && $('#sitemap_tools_nav a').hasClass('clicked')) {
            $("#sitemap").removeClass('displayNone').slideUp("slow");
            $(".sitemap_arrow").addClass('displayNone');
        } else {
            $('#sitemap').slideDown('slow');
            $('.sitemap_arrow').removeClass('displayNone');
        }

        $('.sitemap_title a').removeClass('clicked');
        $('#sitemap_products_nav a').removeClass('clicked');
        $('#sitemap_about_nav a').removeClass('clicked');
        $(this).addClass('clicked');

        if ($('#sitemap_tools_links').hasClass("displayNone")){
            $('#sitemap_tools_links').removeClass('displayNone').fadeTo('slow',1);
        }
    });

    $("#sitemap_about_nav a").bind('click', function(){
        $(".sitemap_arrow").animate({marginLeft:'360px'},'fast');

        $('#sitemap_products_links').fadeTo(350,0).addClass('displayNone').css('display','none');
        $('#sitemap_tools_links').fadeTo(350,0).addClass('displayNone').css('display','none');

        if ($('#sitemap').css('display') == 'block' && $('#sitemap_about_nav a').hasClass('clicked')) {
            $("#sitemap").removeClass('displayNone').slideUp("slow");
            $(".sitemap_arrow").addClass('displayNone');
        } else {
            $('#sitemap').slideDown('slow');
            $('.sitemap_arrow').removeClass('displayNone');
        }

        $('.sitemap_title a').removeClass('clicked');
        $('#sitemap_products_nav a').removeClass('clicked');
        $('#sitemap_tools_nav a').removeClass('clicked');
        $(this).addClass('clicked');

        if ($('#sitemap_about_links').hasClass("displayNone")){
            $('#sitemap_about_links').removeClass('displayNone').fadeTo(350,1);
        }

    });

    // Find Forms
    $("#coverage0").trigger("click");

    // Set template input according to coverage radio
    $("#coverage0,#coverage1").bind("click", function (ev) {
        if ($("#coverage0").attr("checked")) {
            $("#template").attr('value','ifs');
        } else {
            $("#template").attr('value','gbs');
        }
    });

    // validation function and submit
    function validateState() {

        if ($("#content-state-select").val().length > 0) {
            if (validateAlpha($("#content-state-select").val())) {
                analytics.track({category:'Change State',action:$("#content-state-select").val()});
                $("#content-state").submit();
            } else {
                $("#content-state-select").focus();
            }
        } else {
            $("#content-state-select").focus();
        }

    }

    // find form submit
    $('#content-state-select').bind('change', function(ev) {
        switch(ev.type) {
            case 'change':

                // call validation
                validateState();
                return false;

            break;
        }
    });

    // toggle disclosure info on investment page
    $("#idi-btn").click(function(){
        $("#inv-idi").toggle();
    });

    $("#bcd-btn").click(function(){
        $("#inv-bcd").toggle();
    });

    // Buy Online Selector
    $('#submit-buy-online').bind('click', function(ev) {
        switch(ev.type) {
            case 'click':
                //$(this).parent("td").addClass("disabled");
                $(this).attr("disabled",true);

                // call validation
                validateBuyOnline();
                return false;

            break;
        }
    });

    $("#buy-online").submit(function() {
        $("#submit-buy-online").attr("disabled",true);
    });

    // Buy Online Selector
    $('#submit-banking-products').bind('click', function(ev) {
        switch(ev.type) {
            case 'click':
                //$(this).parent("td").addClass("disabled");
                $(this).attr("disabled",true);

                // call validation
                validateBankingProducts();
                return false;

            break;
        }
    });

    $("#banking-form").submit(function() {
        $("#submit-banking-products").attr("disabled",true);
    });


    ///// Fade //////

    // Apply fade to each child element of each "fade-sequence" element
    $.fn.addFade = function(time) {
        (time) ? timeVar = time : timeVar = 5000;
        var fadeElement = $("blockquote", $(this));
        var total = fadeElement.length;
        var cur = 0;
        fadeElement.hide();
        if (total > 1 && !$(this).hasClass('ui-tabs-hide')) {
            fadeSequence(0, fadeElement.length, $(this));
        }
        function fadeSequence(cur, total, obj) {
            if (obj.hasClass('ui-tabs-hide')) {
                return false;
            }
            if(total > 1) {
                /*
                // Because IE doesn't support alpha fades properly:
                if ($.browser.msie) {
                    // alert(cur);
                    $(fadeElement[cur]).css('display', 'block');
                    $(fadeElement[cur]).animate({height:$(this).height()}, timeVar, function () {
                        $(fadeElement[cur]).css('display', 'none');
                    });
                    $(this).delay(timeVar, function() {
                        cur++;
                        if(cur == (total)) {
                            cur = 0;
                        }
                        this.cur = cur;
                        this.total = total;
                        fadeSequence(this.cur, this.total, obj);
                    });
                } else {
                    $(fadeElement[cur]).fadeIn('fast').animate({height:$(this).height()}, timeVar).fadeOut('fast');
                    $(this).delay(timeVar+500, function() {
                        cur++;
                        if(cur == (total)) {
                            cur = 0;
                        }
                        this.cur = cur;
                        this.total = total;
                        fadeSequence(this.cur, this.total, obj);
                    });
                }
                */
                // $(fadeElement[cur]).fadeIn('fast').animate({opacity:1.0}, timeVar).fadeOut('fast');
                $(fadeElement[cur]).fadeIn('fast').animate({height:$(this).height()}, timeVar).fadeOut('fast');
                $(this).delay(timeVar+500, function() {
                    cur++;
                    if(cur == (total)) {
                        cur = 0;
                    }
                    this.cur = cur;
                    this.total = total;
                    fadeSequence(this.cur, this.total, obj);
                });
            } else {
                $(fadeSequence[cur]).fadeIn('fast');
            }
        }
    }
    /*var fadeElement = $("blockquote", "#all");
    var total = fadeElement.length;
    var cur = 0;
    if(total > 0) {
        fadeSequence(0, fadeElement.length);
    }*/

    $("#tab-internship").click(function() {
        $('#internships').addFade(8000);
    });

    $("#tab-all").click(function() {
        $('#all').addFade(8000);
    });

    $('#all').addFade(8000);


    /*
     * stickyfloat - jQuery plugin for verticaly floating anything in a constrained area
     *
     * Example: jQuery('#menu').stickyfloat({duration: 400});
     * parameters:
     *      duration    - the duration of the animation
     *      startOffset - the amount of scroll offset after it the animations kicks in
     *      offsetY     - the offset from the top when the object is animated
     *      lockBottom  - 'true' by default, set to false if you don't want your floating box to stop at parent's bottom
     * $Version: 05.16.2009 r1
     * Copyright (c) 2009 Yair Even-Or
     * vsync.design@gmail.com
     */

    $.fn.stickyfloat = function(options, lockBottom) {
        var $obj                = this;
        var parentPaddingTop    = parseInt($obj.parent().css('padding-top'));
        var parentWidth         = parseInt($obj.parent().width()) / 2;
        var bodyWidth           = parseInt($(document).width()) / 2;
        var startOffset         = $obj.parent().offset().top;
        var opts                = $.extend({ startOffset: startOffset, offsetY: parentPaddingTop, duration: 200, lockBottom:true }, options);

        if(opts.lockBottom){
            var bottomPos = $obj.parent().height() - $obj.height() + parentPaddingTop; //get the maximum scrollTop value
            if( bottomPos < 0 )
                bottomPos = 0;
        }

        $(window).bind('resize scroll', function() { // $(window).scroll(function () {
            $obj.stop(); // stop all calculations on scroll event

            var pastStartOffset         = $(document).scrollTop() > opts.startOffset;   // check if the window was scrolled down more than the start offset declared.
            var objFartherThanTopPos    = $obj.offset().top > startOffset;  // check if the object is at it's top position (starting point)
            var objBiggerThanWindow     = $obj.outerHeight() < $(window).height();  // if the window size is smaller than the Obj size, then do not animate.
            var pageWiderThanWindow     = $obj.parent().outerWidth() < $(window).width();   // if the window size is smaller than the page size, then do not animate.

            // if window scrolled down more than startOffset OR obj position is greater than
            // the top position possible (+ offsetY) AND window size must be bigger than Obj size
            if( (pastStartOffset || objFartherThanTopPos) && objBiggerThanWindow && pageWiderThanWindow){
                var newpos = ($(document).scrollTop() -startOffset + opts.offsetY );
                $obj.stop().css({
                    position: 'fixed',
                    left: '50%',
                    top: 0,
                    marginLeft: -parentWidth
                });

                if ( newpos > bottomPos )
                {
                    $obj.stop().css({
                        position: 'absolute',
                        left: '50%',
                        top: bottomPos,
                        marginLeft: -parentWidth
                    });
                }

                //$obj.animate({ top: newpos }, opts.duration );

            } else {
                $obj.stop().css({
                    position: 'absolute',
                    left: '50%',
                    top: 0,
                    marginLeft: -parentWidth
                });
            }
        });
    };

    if ($('#content .subnav').length && !jQuery.browser.msie){
        $('#content .subnav').stickyfloat({ duration: 0 });
    } else if ($('#content .subnav').length && parseInt(jQuery.browser.version) != 6) {
        $('#content .subnav').stickyfloat({ duration: 0 });
    }

    /**
     * SEARCH FOR AGENT AND ADD '1'
     *
     */
    $.fn.textWalk = function( fn ) {
        this.contents().each( jwalk );

        function jwalk() {
            var nn = this.nodeName.toLowerCase();
            if( nn === '#text' ) {
                fn.call( this );
            } else if( this.nodeType === 1 && this.childNodes && this.childNodes[0] && nn !== 'script' && nn !== 'textarea' ) {
                $(this).contents().each( jwalk );
            }
        }
        return this;
    };

    var indProducts_array = new Array("/medicare-supplement-insurance/", "/life-insurance/", "/long-term-care-insurance/", "/disability-insurance/", "/annuities/", "/critical-illness-insurance/", "/investments/", "http://www.mutualofomaha.com/templates/two-col.php", "/small-business/");

    var is_indProduct = false;
    /**
     * Get company from cookie
     * @author Jeff Yates
     * @date 04/12/2012
     *
     * Doesn't work in FF
     *
     *  if($.cookie('settings').length > 0)
     *  {
     *      var company = $.cookie('settings').match(/company=[\w]+/gi);
     *
     *      if(company)
     *          company = company[0].replace(/company=/gi,'').toUpperCase();
     *  }
     */

    $.each(indProducts_array, function(i)
    {

        if(indProducts_array[i] === '/medicare-supplement-insurance/' && window.location.href.match(/find\-agent/gi)) //&& company === 'UMS')
            var addAgentState = /^(Alabama|Arizona|Arkansas|Indiana|Maine|Montana|New Jersey|New Hampshire|New Mexico|Oregon|South Dakota|Texas|Washington|Wyoming|Connecticut|Georgia|Iowa|Kentucky|Maryland|Michigan|Mississippi|Nebraska|North Dakota|Ohio|Oklahoma|Oregon|Pennsylvania|South Carolina|Texas|West Virginia)$/gi;
        else if(indProducts_array[i] === '/medicare-supplement-insurance/')
            var addAgentState = /^(Connecticut|Louisiana|Georgia|Iowa|Kentucky|Maryland|Michigan|Mississippi|Nebraska|North Dakota|Ohio|Oklahoma|Oregon|Pennsylvania|South Carolina|Texas|West Virginia)$/gi;
        else if( indProducts_array[i] === '/disability-insurance/' && window.location.href.match( /one\-customers\-story/gi ) )
            var addAgentState = /^(Washington)$/gi;
        else
            var addAgentState = /^(Washington)$/gi; // example states /New York|Ohio/gi -- | = OR

        if (window.location.href.match(indProducts_array[i]))
        {
            // add ¹ to agent(s)
            is_indProduct = true;

            // if medsupp look at state and only add if WA except
            if ( ( window.location.href.match(/\/medicare-supplement-insurance\//gi) || window.location.href.match(/\/long-term-care-insurance\//gi) ) && $('#title-bar .change-state').length > 0 && !$('#title-bar .change-state').html().match(addAgentState))
                is_indProduct = false;

            return false;
        }
    });

    /*if (window.location.href.indexOf('http://www.mutualofomaha.com/medicare-supplement-insurance/find-agent.php') === -1) { }*/
    $('.subnav, #content .content-copy, #content .sidebar, #content .content-title, #next-steps, #find-links').textWalk(function() {
        if (is_indProduct)
        {
            this.data = this.data.replace(/\bagent[s]?(?!¹)\b/gi, function (m, n) { return m+String.fromCharCode(0185); });
        }
    });

    if(window.location.href.match(/\/small-business\//gi)) {
        $('#disclaimers').textWalk(function() {
            this.data = this.data.replace(/\bagent[s]?(?!¹)\b/gi, function (m, n) { return m+String.fromCharCode(0185); });
        });
    }

    /**
     * ACTIONS FOR SITE DATA FOR ITG AND CAT
     *
     */
    $('#close-site-data').click(function() {
        $('#site-data-wrapper').fadeOut('slow');
        $.get("http://www.mutualofomaha.com/includes/utilities/site-data-settings.php", { display: "close" } );
        return false;
    });

    $('#collapse-site-data').click(function() {
        var collapseHeight = '12';
        var openHeight = '117';
        if ($("#site-data").height() <= collapseHeight)
        {
            //$('#js-data .show-data').html($("#site-data").height());
            $(this).removeClass('selected');
            //Animate the left column to this height
            $("#site-data").animate({
                height: openHeight
            }, 500);

            $.get("http://www.mutualofomaha.com/includes/utilities/site-data-settings.php", { display: "show" } );
        }
        else
        {
            //$('#js-data .show-data').html($("#site-data").height());
            $(this).addClass('selected');
            //Animate the left column to this height
            $("#site-data").animate({
                height: collapseHeight
            }, 500);

            $.get("http://www.mutualofomaha.com/includes/utilities/site-data-settings.php", { display: "collapse" } );
        }
        return false;
    });

    $('#top-site-data').click(function() {
        $('#site-data').addClass('top').removeClass('btm');
        $('#site-data .window-ctl').addClass('top');
        $.get("http://www.mutualofomaha.com/includes/utilities/site-data-settings.php", { position: "top" } );
        return false;
    });

    $('#btm-site-data').click(function() {
        $('#site-data').addClass('btm').removeClass('top');
        $('#site-data .window-ctl').removeClass('top');
        $.get("http://www.mutualofomaha.com/includes/utilities/site-data-settings.php", { position: "btm" } );
        return false;
    });
});

function copyright()// automatically updates the copyright
{
    var year="";
    mydate = new Date();
    myyear= mydate.getFullYear();
    year = myyear;
    var year = ("&nbsp; &copy; " + year);
    document.write(year);
}

