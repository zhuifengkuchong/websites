
/*** Begin Modal JS ***/
var extLinks = new Array();
		extLinks["dentabenefits"] = 'http://www.mutualofomaha.com/javascript/dentabenefits.com';
		extLinks["ucci"] = 'http://www.mutualofomaha.com/javascript/ucci.com';
		extLinks["dentemax"] = 'http://www.mutualofomaha.com/javascript/dentemax.com';

$(document).ready(function()
{
	
	// set vars
	$.allowCancel = false;
	
	// notice that you can pass an element as the target 
	//  in addition to a string selector.
	var v = $('#modal div.jqmdMSG');

	$('#modal').jqm({
		trigger: false,
		ajax: 'http://www.mutualofomaha.com/includes/modals/modal.php', /* Extract ajax URL from the 'href' attribute of triggering element */
		target: v,
		modal: true, /* FORCE FOCUS */
		onHide: function(k) { 
			k.o.remove(); // remove overlay
			k.w.fadeOut(0); // hide window
		},
		overlay: 80
	});
	
	// Work around for IE's lack of :focus CSS selector
	if($.browser.msie)
	$('input')
		.focus(function(){$(this).addClass('iefocus');})
			.blur(function(){$(this).removeClass('iefocus');});
			
	// shows modal if link is clicked w/ rel = external
	$('a[rel="external"]').bind('click',function(){
		// add close to cancel button						
		$.allowCancel = true;
		$.site_name = $(this).text();
		$.site_link = $(this).attr('href');
		
		$('#modal').removeClass('ratings-pos');
		
		/*if ($(this).attr('name') != "") {
			$.type_description = "You are now leaving mutualofomaha.com, and entering " + extLinks[$(this).attr('name')] + ". Mutual of Omaha does not control the content or services provided by this third-party Web site. Mutual of Omaha expressly disclaims any responsibility for the content, information, products or services provided by or advertised on this third-party site.";
		} else {
			$.type_description = "You are now leaving MutualOfOmaha.com.";
		}*/
		$.type_description = "You are now leaving MutualOfOmaha.com.";
		
		$('#modal').jqm({ ajax: 'http://www.mutualofomaha.com/includes/modals/ext-link-modal.php', overlay:60 });
		$('#modal').jqmShow();
		
		return false;
	});
	
	// shows modal if link is clicked w/ rel = external
	$('a[rel="ratings"]').bind('click',function(){
		// add close to cancel button						
		$.allowCancel = true;
		$('#modal').addClass('ratings-pos');
		$('#modal').jqm({ ajax: 'http://www.mutualofomaha.com/includes/modals/ratings-modal.php', overlay:60 });
		$('#modal').jqmShow();
		
		return false;
	});
	
	// shows modal if link is clicked w/ rel = external
	$('a[rel="buy-online"]').bind('click',function(){
		// add close to cancel button						
		$.allowCancel = true;
		$('#modal').jqm({ ajax: 'http://www.mutualofomaha.com/includes/modals/buy-online-modal.php', overlay:60 });
		$('#modal').jqmShow();
		
		return false;
	});
	
	// shows modal if link is clicked w/ rel = ad
	$('a[rel="ad"]').bind('click',function(){
		// add close to cancel button						
		$.allowCancel = true;
		$('#modal').jqm({ ajax: 'http://www.mutualofomaha.com/includes/modals/ad-modal.php', overlay:60 });
		$('#modal').jqmShow();
		
		return false;
	});
	
	// shows modal if link is clicked w/ rel = provider
	$('a[rel="provider"]').live('click',function(){
		// add close to cancel button						
		$.allowCancel = true;
		$('#modal').jqm({ ajax: 'http://www.mutualofomaha.com/includes/modals/provider-login-terms-modal.php', overlay:60 });
		$('#modal').jqmShow();
		
		return false;
	});
	
	// shows modal if link is clicked w/ rel = external
	$('img.marketing').bind('click',function(){
		// add close to cancel button						
		$.allowCancel = true;
		var image = $(this).attr('name');
		$('#modal-di-choice').jqm({ ajax: '/includes/modals/di-choice-marketing.php?image=' + image, overlay:60 });
		$('#modal-di-choice').jqmShow();
		
		return false;
	});
	
	$(".life-liam-video").click(function(event) { 
		event.preventDefault(); 
		$('#video-modal').jqm({ ajax: 'http://www.mutualofomaha.com/includes/modals/life-liam-modal.php', overlay:0, 
			onHide:function() {
				$('#video-modal').empty();
				$( "#life-liam-modal" ).remove(); 
			}
		});
		$('#video-modal').jqmShow();
	}); 
	
});
