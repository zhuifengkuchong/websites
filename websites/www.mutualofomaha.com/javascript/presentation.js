
$.fn.scrollWin = function(){
	$('html, body').animate({
		scrollTop: $(this).offset().top
	}, 1000);
}

// begin document load
$(document).ready(function() {


	/* presentation tabs */
	var $tabs = $('#presentation-wrapper').tabs();
	var selected = $tabs.data('http://www.mutualofomaha.com/javascript/selected.tabs');
	var totalSize = $(".ui-tabs-panel").size() - 1;

	if (selected == totalSize) {
		$('#presentation-wrapper .next-frame').addClass('disabled');
	}
	if (selected == 0) {
		$('#presentation-wrapper .back-frame').addClass('disabled');
	}

	$('#presentation-nav a').bind('click', function() {
		var selected = $tabs.data('http://www.mutualofomaha.com/javascript/selected.tabs');
		var totalSize = $(".ui-tabs-panel").size() - 1;
		if (selected >= totalSize) {
			$('#presentation-wrapper .next-frame').addClass('disabled');
		} else {
			$('#presentation-wrapper .next-frame').removeClass('disabled');
		}

		if (selected <= 0) {
			$('#presentation-wrapper .back-frame').addClass('disabled');
		} else {
			$('#presentation-wrapper .back-frame').removeClass('disabled');
		}

		//GA Tracking
		analytics.pageview({path: location.pathname + $(this).attr("href")});


		if ($('#content .subnav').length && !jQuery.browser.msie){
			$('#content .subnav').stickyfloat({ duration: 0 });
		} else if ($('#content .subnav').length && parseInt(jQuery.browser.version) != 6) {
			$('#content .subnav').stickyfloat({ duration: 0 });
		}

		$('#content').scrollWin();
		return false;
	});

	$('#presentation-wrapper .next-frame').click(function() {
		var selected = $tabs.data('http://www.mutualofomaha.com/javascript/selected.tabs');
		var nextTab = selected + 1;
		var tab_href = "#"+$(".ui-tabs-panel:eq(" + nextTab + ")").attr('id');
		var totalSize = $(".ui-tabs-panel").size() - 1;

		$('#presentation-wrapper .back-frame').removeClass('disabled');

		if (selected + 1 >= totalSize) {
			$(this).addClass('disabled');
		} else {
			$(this).removeClass('disabled');
		}

		//alert(tab_href);

		if (selected != totalSize) {
			//$.history.load(tab_href.replace(/^.*#/, ''));
			$tabs.tabs('select', selected + 1);
		}

		if ($('#content .subnav').length && !jQuery.browser.msie){
			$('#content .subnav').stickyfloat({ duration: 0 });
		} else if ($('#content .subnav').length && parseInt(jQuery.browser.version) != 6) {
			$('#content .subnav').stickyfloat({ duration: 0 });
		}

		$('#content').scrollWin();
		return false;
	});

	$('#presentation-wrapper .back-frame').click(function() {
		var selected = $tabs.data('http://www.mutualofomaha.com/javascript/selected.tabs');
		var prevTab = selected - 1;
		var tab_href = "#"+$(".ui-tabs-panel:eq(" + prevTab + ")").attr('id');
		var totalSize = $(".ui-tabs-panel").size() - 1;

		$('#presentation-wrapper .next-frame').removeClass('disabled');

		if (selected - 1 <= 0) {
			$(this).addClass('disabled');
		} else {
			$(this).removeClass('disabled');
		}

		if (selected != 0) {
			//$.history.load(tab_href.replace(/^.*#/, ''));
			$tabs.tabs('select', selected - 1);
		}

		if ($('#content .subnav').length && !jQuery.browser.msie){
			$('#content .subnav').stickyfloat({ duration: 0 });
		} else if ($('#content .subnav').length && parseInt(jQuery.browser.version) != 6) {
			$('#content .subnav').stickyfloat({ duration: 0 });
		}

		$('#content').scrollWin();
		return false;
	});

	if (window.location.hash != '' && window.location.hash.indexOf("section") < 0) {
		//get the index
		var $tabs = $("#presentation-wrapper").tabs('select',window.location.hash);
		var selected = $tabs.data('http://www.mutualofomaha.com/javascript/selected.tabs');
		var totalSize = $(".ui-tabs-panel").size() - 1;

		if (selected >= totalSize) {
			$('#presentation-wrapper .next-frame').addClass('disabled');
		} else {
			$('#presentation-wrapper .next-frame').removeClass('disabled');
		}

		if (selected <= 0) {
			$('#presentation-wrapper .back-frame').addClass('disabled');
		} else {
			$('#presentation-wrapper .back-frame').removeClass('disabled');
		}
	} else if (window.location.hash == '') {
		var $tabs = $("#presentation-wrapper").tabs('select',0);
		var selected = $tabs.data('http://www.mutualofomaha.com/javascript/selected.tabs');
		var totalSize = $(".ui-tabs-panel").size() - 1;

		if (selected >= totalSize) {
			$('#presentation-wrapper .next-frame').addClass('disabled');
		} else {
			$('#presentation-wrapper .next-frame').removeClass('disabled');
		}

		if (selected <= 0) {
			$('#presentation-wrapper .back-frame').addClass('disabled');
		} else {
			$('#presentation-wrapper .back-frame').removeClass('disabled');
		}
	}


	// presentation TOGGLE show & hide
	$("a.toggle-link").each(function() {
		$(this).click(function(e) {
			e.preventDefault();
			if ($(this).hasClass('open')) {
				$(this).removeClass('open');
			} else {
				$(this).addClass('open');
			}
		 	$(this).next('div').slideToggle('slow');
        	return false;
		});
    });
});