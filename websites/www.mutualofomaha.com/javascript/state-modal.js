
/*** Begin State Modal JS ***/

$(document).ready(function()
{

	// set vars

	$.allowCancel = $.hide_submit = false;
	$.cookieSettings = $.cookie('settings');

	$.products_array = new Array("/bank/", "/long-term-care-insurance/", "http://www.mutualofomaha.com/life-insurance/plan/quote.php", "/medicare-supplement-insurance/", "http://www.mutualofomaha.com/term-life-insurance/express.php", "http://www.mutualofomaha.com/templates/two-col.php", "/annuities/plan-details/", "/disability-insurance/", "/life-insurance/learn/universal-life-insurance/", "/life-insurance/legacy/learn/universal-life-insurance/", "/livingpromise/", "/cancer-insurance/");
	// /life-insurance/learn/universal-life-insurance/
	$.products = $.is_medsupp = $.is_ltc = $.is_life = $.is_di = $.is_annuities_details = $.is_life_details = $.is_life_ad = $.is_chs = false;
	$.product_name = "";
	//$.product_link_array = new Array("http://www.longtermanswers.com/", "http://www.mutualofomaha.com/long-term-answers/not-available.php");

	$.each($.products_array, function(i) // Notice the i variable
	{
		if (window.location.href.indexOf($.products_array[i]) > -1) {
			$.products = true;
			if ($.products_array[i] == "/long-term-care-insurance/") {
				$.is_ltc = true;
				$.product_name = 'Long Term Care Insurance';
			} else if ($.products_array[i] == "/medicare-supplement-insurance/" || $.products_array[i] == "http://www.mutualofomaha.com/templates/two-col.php") {
				// state modal will not show if directory is referenced below (set $.products to false)
				if (window.location.href.indexOf("/medicare-supplement-insurance/ssc/") !== -1) {
					$.products = false;
				}
				$.is_medsupp = true;
				$.product_name = 'Medicare Supplement Insurance';
			} else if ($.products_array[i] == "/annuities/plan-details/") {
				$.is_annuities_details = true;
				$.product_name = 'Plan Details';
			//} else if ($.products_array[i] == "/life-insurance/learn/universal-life-insurance/") {
				//$.is_life_details = true;
				//$.product_name = 'Universal Life Insurance';
			} else if ($.products_array[i] == "/disability-insurance/") {
				if (window.location.href.indexOf("/benefits-info/") !== -1 || window.location.href.indexOf("/disability-income-choice-portfolio/") !== -1) {
					$.products = false;
				}
				$.is_di = true;
				$.product_name = 'Disability Insurance';

			} else if ($.products_array[i] == "/life-insurance/learn/universal-life-insurance/") {
				if (window.location.href.indexOf("http://www.mutualofomaha.com/life-insurance/learn/universal-life-insurance/accumul-plus.php") !== -1) {
					$.products = false;
				}
				$.is_life = true;
				$.product_name = 'Life Insurance';
			} else if ($.products_array[i] == "http://www.mutualofomaha.com/life-insurance/plan/quote.php") {
				$.is_life = true;
				$.product_name = 'Life Insurance';
			} else if(window.location.href.indexOf('/printable/bank/') !== -1) {
				$.products = false;
			}
			else if ($.products_array[i] == "/livingpromise/"){
				$.product_name = "product details"
			} else if ($.products_array[i] == "/cancer-insurance/") {
				$.is_chs = true;
				$.product_name = 'Cancer, Heart Attack and Stroke Insurance';
			}
			/* else if ($.products_array[i] == "/life-insurance/learn/accidental-death/") {
				if (window.location.href.indexOf("http://www.mutualofomaha.com/life-insurance/learn/universal-life-insurance/accumul-plus.php") !== -1 || window.location.href.indexOf("http://www.mutualofomaha.com/life-insurance/learn/universal-life-insurance/estate-planning.php") !== -1) {
					$.products = false;
				}
				$.is_life = $.is_life_ad = true;
				$.product_name = 'Accidental Death Insurance';
			}*/
			return false;
		}
	});

	// notice that you can pass an element as the target
	//  in addition to a string selector.
	var t = $('#state-modal div.jqmdMSG');

	$('#state-modal').jqm({
		trigger: false,
		ajax: '/includes/modals/state-modal.php?surl=' + window.location.href, /* Extract ajax URL from the 'href' attribute of triggering element */
		target: t,
		modal: true, /* FORCE FOCUS */
		onHide: function(h) {
			t.html('Please Wait...');  // Clear Content HTML on Hide.
			h.o.remove(); // remove overlay
			h.w.fadeOut(0); // hide window
		},
		overlay: 80
	});

	// Work around for IE's lack of :focus CSS selector
	if($.browser.msie)
	$('input')
		.focus(function(){$(this).addClass('iefocus');})
			.blur(function(){$(this).removeClass('iefocus');});



	// shows modal if link is clicked w/ rel = productstate
	$('a[rel="productstate"]').bind('click',function(){
		if (!$.cookieSettings || $.cookieSettings.indexOf("state=") < 0) {
			// add close to cancel button
			$.allowCancel = true;
			$.product_name = $(this).text();

			if ($.product_name.toLowerCase().indexOf("insurance") < 0) {
				$.product_name = $.product_name + " Insurance";
			}

			$.type_description = "View product policies available where you live.";//"View <strong>" + $.product_name + "</strong> product policies available where you live.";
			$.product_link = $(this).attr('href');
			$('#state-modal').jqm({ ajax: '/includes/modals/state-modal.php?surl=' + $.product_link, overlay:60 });
			$('#state-modal').jqmShow();

			return false;
		}
	});

	// shows modal if link is clicked w/ rel = details
	$('a[rel="details"]').bind('click',function(){
		if (!$.cookieSettings || $.cookieSettings.indexOf("state=") < 0) {
			// add close to cancel button
			$.allowCancel = true;
			$.product_name = $(this).text();
			$.type_description = "View <strong>" + $.product_name + "</strong> available where you live.";
			$.product_link = $(this).attr('href');
			$('#state-modal').jqm({ ajax: '/includes/modals/state-modal.php?surl=' + $.product_link, overlay:60 });
			$('#state-modal').jqmShow();

			return false;
		}
	});

	// shows modal if direct link to page
	if ((!$.cookieSettings || $.cookieSettings.indexOf("state=") < 0) && $.products == true) {
		$.product_link = window.location.href;
		if ($.is_annuities_details == true) {
			$.type_description = "View <strong>" + $.product_name + "</strong> available where you live.";
		} else {
			$.type_description = "View <strong>" + $.product_name + "</strong> product policies available where you live.";
		}

		$.type_description = "View <strong>" + $.product_name + "</strong> available where you live.";
		$('#state-modal').jqm({ ajax: '/includes/modals/state-modal.php?surl=' + $.product_link, overlay:80 });
		$('#state-modal').jqmShow();
	}

	/*
	$('a[rel="productstate"]').bind('click',function(){
		$('body').qtip(
		{
			content: {
				url: '/includes/state-modal.php?url='+ window.location.href
			},
			position: {
				target: $(document.body), // Position it via the document body...
				corner: 'center' // ...at the center of the viewport
			},
			show: {
				when: false,
				delay: 0,
				effect: { length: 0 },
				ready: true,
				solo: true // And hide all other tooltips
			},
			hide: false,
			style: 'state_modal_style',
			api: {
				beforeShow: function()
				{
					$('#utilities .links').css({'z-index' : '4000'});
					$('#content .content-title .utilities ul').css({'z-index' : '4000'});
					// Fade in the modal "blanket" using the defined show speed
					$('#state-blanket').fadeIn(this.options.show.effect.length);
				},
				beforeHide: function()
				{
					$('#utilities .links').css({'z-index' : '7000'});
					$('#content .content-title .utilities ul').css({'z-index' : '7000'});
					// Fade out the modal "blanket" using the defined hide speed
					$('#state-blanket').fadeOut(this.options.hide.effect.length);
				}
			}
		});

		// Create the modal backdrop on document load so all modal tooltips can use it
		$('<div id="state-blanket">')
		.css({
			position: 'absolute',
			top: $(document).scrollTop(), // Use document scrollTop so it's on-screen even if the window is scrolled
			left: 0,
			height: $(document).height(), // Span the full document height...
			width: '100%', // ...and full width

			opacity: 0.5, // Make it slightly transparent
			backgroundColor: 'black',
			zIndex: 5000  // Make sure the zIndex is below 6000 to keep it below tooltips!
		})
		.appendTo(document.body) // Append to the document body
		.show(); // Hide it initially

		$.product_link = $(this).attr('href');

		return false;
	});
	*/

});
