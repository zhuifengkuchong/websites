// JavaScript Document

// Start Form Validation
function validateAlpha(strValue) {
	var objRegExp  = /(^[a-zA-Z][a-zA-Z',.\-\\(\) @]+$)/;
	return objRegExp.test(strValue);
}
function validateAlphaNum(strValue) {
	var objRegExp = /(^[a-zA-Z0-9'\-\.,#:&\!\?\(\)\s\/]+$)/;
	return objRegExp.test(strValue);
}

function validateAlphaNumNone(strValue) {
	var objRegExp = /(^[a-zA-Z0-9'\-\.,\$%"@\(\)! ]+$)|(^\s*$)/;
	return objRegExp.test(strValue);
}

function validateMsg(strValue) {
	var objRegExp = /(^[a-zA-Z0-9'\-\.,;\$%\/"@\(\)?!\n\r\&: ]+$)/;
	return objRegExp.test(strValue);
}

function validateTiny(strValue) {
	//var objRegExp = /(^[a-zA-Z0-9'\-\.,\$%<>=_\/"@\(\)!\w\n\r ]+$)/;
	var objRegExp = /(^[\s\S]+$)/;
	return objRegExp.test(strValue);
}

function validateNum( strValue ) {
	var objRegExp = /(^-?\d\d*\.\d*$)|(^-?\d\d*$)|(^-?\.\d\d*$)/;
	return objRegExp.test(strValue);
}

function validateDigit2(strValue) {
	var objRegExp = /^(\d{2})$/;
	return objRegExp.test(strValue);
}

function validateDigit3(strValue) {
	var objRegExp = /^(\d{3})$/;
	return objRegExp.test(strValue);
}

function validateDigit4(strValue) {
	var objRegExp = /^(\d{4})$/;
	return objRegExp.test(strValue);
}

function validateDigit5(strValue) {
	var objRegExp = /^(\d{5})$/;
	return objRegExp.test(strValue);
}

function validateDigit7(strValue) {
	var objRegExp = /^(\d{7})$/;
	return objRegExp.test(strValue);
}

function validateMonth(strValue) {
var objRegExp = /(^[0-9]{2})$/;
return objRegExp.test(strValue);
}
function validateDate(strValue) {
var objRegExp = /(^[0-9]{2})$/;
return objRegExp.test(strValue);
}
/*function validateYear(strValue) {
var objRegExp = /(^[0-9]{4})$/;
return objRegExp.test(strValue);
}*/
function validateFullDate(strValue){
var objRegExp = /(^[0-9]{2}\/[0-9]{2}\/[0-9]{4})$/;
return objRegExp.test(strValue);
}
function validate2Max(strValue){
var objRegExp = /(^[0-9]{1,2})$/;
return objRegExp.test(strValue);
}

function validate4Max(strValue){
var objRegExp = /(^[0-9]{1,4})$/;
return objRegExp.test(strValue);
}

function validateYear(strValue){
var objRegExp = /(^(19|20)[0-9]{2})$/;
return objRegExp.test(strValue);
}

function validateSSN(strValue){
var objRegExp = /(^[0-9]{3}(-)?[0-9]{2}(-)?[0-9]{4})$/;
return objRegExp.test(strValue);
}

function validateTime(strValue){ 
var objRegExp = /(^[0-9]{1,2}:[0-9]{2})$/; 
return objRegExp.test(strValue); 
}

function validateTime12hr(strValue){ 
var objRegExp = /(^[0-9]{1,2}:[0-9]{2}\s[AM|am]|[PM|pm])$/; 
return objRegExp.test(strValue); 
}

function validateDatePart1(strValue) { 
	var objRegExp = /^((0[1-9])|(10|11|12))$/; /*(\d{2})*/
	return objRegExp.test(strValue);
}

function validateDatePart2(strValue) {
	var objRegExp = /^(0[1-9]|(10|11|12))[- \/.](0[1-9]|(1|2)[0-9]|3(0|1))$/;/*(\d{2})\/(\d{2})*/
	return objRegExp.test(strValue);
}

function validateDatePart3(strValue) {
	var objRegExp = /^(0[1-9]|(10|11|12))[-\/.](0[1-9]|(1|2)[0-9]|3(0|1))[- \/.](19|20)?\d\d$/;/*(\d{2})\/(\d{2})\/(\d{4})*/
	return objRegExp.test(strValue);
}
function validateFullDatePart3(strValue) {
	var objRegExp = /^(0[1-9]|(10|11|12))[-\/.](0[1-9]|(1|2)[0-9]|3(0|1))[- \/.](19|20)\d\d$/;/*(\d{2})\/(\d{2})\/(\d{4})*/
	return objRegExp.test(strValue);
}

function validateDateYMD(strValue) { 
	var objRegExp = /^(19|20)\d\d[-](0[1-9]|10|11|12)[-](0[1-9]|\d\d)$/;
	return objRegExp.test(strValue);
}

function validateDateYMDPart1(strValue) {
	var objRegExp =  /^((19|20)\d\d)$/;
	return objRegExp.test(strValue);
}

function validateDateYMDPart2(strValue) {
	var objRegExp = /^((19|20)\d\d)[- \/.](0[1-9]|1[012])$/;
	return objRegExp.test(strValue);
}

function validateDateYMDPart3(strValue) { 
	var objRegExp = /^((19|20)\d\d)[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$/;
	return objRegExp.test(strValue);
}

function validatePhonePart1(strValue) {
	var objRegExp = /^(\d{3})$/;
	return objRegExp.test(strValue);
}

function validatePhonePart2(strValue) {
	var objRegExp = /^(\d{3})(-|\.)(\d{3})$/;
	return objRegExp.test(strValue);
}

function validatePhonePart3(strValue) {
	var objRegExp = /^(\d{3})(-|\.)(\d{3})(-|\.)(\d{4})$/;
	return objRegExp.test(strValue);
}

function validatePhone( strValue ) {
var objRegExp  = /(^\d{10,11}$)|(\([0-9]{3}\)\s{0,1}[0-9]{3}-[0-9]{4}$)|(\(\d{3}\)\s{0,1}\d{7}$)|(^(\d-){0,1}\d{3}(-|\.)\d{3}(-|\.)\d{4}$)/;
return objRegExp.test(strValue);
}
function validatePhoneParens( strValue ) {
var objRegExp  = /(\([0-9]{3}\)\s{0,1}[0-9]{3}-[0-9]{4}$)|(\(\d{3}\)\s{0,1}\d{7}$)/;
return objRegExp.test(strValue);
}
function validatePhoneString( strValue ) {
var objRegExp  = /(^\d{10,11}$)/;
return objRegExp.test(strValue);
}
function validatePhoneNumber(strValue) { 
var objRegExp = /^[0-9]{4}$/;
return objRegExp.test(strValue);
}

function validateNeededPhone( strValue ) {
var objRegExp  = /(^\d{3}-\d{3}-\d{4}$)/;
return objRegExp.test(strValue);
}

function validatePhoneACPrefix(strValue) {
var objRegExp = /(^[0-9]{3})$/;
return objRegExp.test(strValue);
}

function validateEmail(strValue) {
	var objRegExp  = /^[a-z0-9&]([a-z0-9_\-\.'&]*)@([a-z0-9_\-\.]*)(\.[a-z]{2,6}(\.[a-z]{2}){0,2})$/i;
	return objRegExp.test(strValue);
}

function validateMultiEmail(strValue) {
	var objRegExp  = /^((([a-z0-9&]([a-z0-9_\-\.&]*)@([a-z0-9_\-\.]*)(\.[a-z]{2,6}(\.[a-z]{2}){0,2}))(\s*[\,]\s*)){0,5})([a-z0-9&]([a-z0-9_\-\.&]*)@([a-z0-9_\-\.]*)(\.[a-z]{2,6}(\.[a-z]{2}){0,2}))$/i;
	return objRegExp.test(strValue);
}

function validateState(strValue) {
var objRegExp = /^(AL|al|Al|AK|ak|Ak|AZ|az|Az|AR|ar|Ar|CA|ca|Ca|CO|co|Co|CT|ct|Ct|DC|dc|Dc|DE|de|De|FL|fl|Fl|GA|ga|Ga|HI|hi|Hi|ID|id|Id|IL|il|Il|IN|in|In|IA|ia|Ia|KS|ks|Ks|KY|ky|Ky|LA|la|La|ME|me|Me|MD|md|Md|MA|ma|Ma|MI|mi|Mi|MN|mn|Mn|MS|ms|Ms|MO|mo|Mo|MT|mt|Mt|NE|ne|Ne|NV|nv|Nv|NH|nh|Nh|NJ|nj|Nj|NM|nm|Nm|NY|ny|Ny|NC|nc|Nc|ND|nd|Nd|OH|oh|Oh|OK|ok|Ok|OR|or|Or|PA|pa|Pa|PR|pr|Pr|RI|ri|Ri|SC|sc|Sc|SD|sd|Sd|TN|tn|Tn|TX|tx|Tx|UT|ut|Ut|VT|vt|Vt|VA|va|Va|VI|vi|Vi|WA|wa|Wa|WV|wv|Wv|WI|wi|Wi|WY|wy|Wy)$/i; 
return objRegExp.test(strValue);
}

function validateUrl(strValue) {
	var objRegExp  = /^(([http|https]+):)(\/{2})([0-9.\-A-Za-z]+)\.([0-9.\-A-Za-z]+)\.([0-9.\-A-Za-z]+)(?::(\d+))?(?:\/([^?#]*))?(?:\?([^#]*))?(?:#(.*))?$/;
	return objRegExp.test(strValue);
}

function validateZip( strValue ) {
	var objRegExp  = (/(^\d{5}$)|(^\d{9}$)|(^\d{5}-\d{4}$)/);
	return objRegExp.test(strValue);
}

function validateFile( strValue ) {
	var objRegExp  = /(^.*(\.csv|\.txt))$/;
	return objRegExp.test(strValue);
}

function validateNameOrZip(strValue) { 
	var objRegExp = /(^[a-zA-Z][a-zA-Z',.\-\ @]+)|(^\d{5}$)|(^\d{9}$)|(^\d{5}-\d{4}$)/
	return objRegExp.test(strValue);
}
// End Form Validation