
/*** Begin Validation Scripts ***/

// Start Form Validation

function validateAlpha(strValue) {
	var objRegExp  = /(^[a-zA-Z',.\-\ ]+$)/;
	return objRegExp.test(strValue);
}
function validateAlphaNum(strValue) {
	var objRegExp = /(^[a-zA-Z0-9'\-\.,#\/ &]+$)/;
	return objRegExp.test(strValue);
}
function validateAlphaNumNone(strValue) {
	var objRegExp = /(^[a-zA-Z0-9'\-\.,\$%"@\(\)! ]+$)|(^\s*$)/;
	return objRegExp.test(strValue);
}

function validateMsg(strValue) {
	var objRegExp = /(^[a-zA-Z0-9'\-\.,\$%\/"@\(\)?!\n\r ]+$)/;
	return objRegExp.test(strValue);
}

function validateTiny(strValue) {
	//var objRegExp = /(^[a-zA-Z0-9'\-\.,\$%<>=_\/"@\(\)!\w\n\r ]+$)/;
	var objRegExp = /(^[\s\S]+$)/;
	return objRegExp.test(strValue);
}

function validateMonth(strValue) {
	var objRegExp = /(^[0-9]{2})$/;
	return objRegExp.test(strValue);
}
function validateDate(strValue) {
	var objRegExp = /(^[0-9]{2})$/;
	return objRegExp.test(strValue);
}
function validateYear(strValue) {
	var objRegExp = /(^[0-9]{4})$/;
	return objRegExp.test(strValue);
}

function validateSSN(strValue){
	var objRegExp = /(^[0-9]{3}(-)?[0-9]{2}(-)?[0-9]{4})$/;
	return objRegExp.test(strValue);
}

function validateTime(strValue){
var objRegExp = /(^[0-9]{1,2}:[0-9]{2})$/;
return objRegExp.test(strValue);
}

function validateTime12hr(strValue){
var objRegExp = /(^[0-9]{1,2}:[0-9]{2}\s[AM|am]|[PM|pm])$/;
return objRegExp.test(strValue);
}

function validatePhone( strValue ) {
var objRegExp  = /(^\d{10,11}$)|(\([0-9]{3}\)\s{0,1}[0-9]{3}-[0-9]{4}$)|(\(\d{3}\)\s{0,1}\d{7}$)|(^(\d-){0,1}\d{3}-\d{3}-\d{4}$)/;
return objRegExp.test(strValue);
}
function validatePhoneParens( strValue ) {
var objRegExp  = /(\([0-9]{3}\)\s{0,1}[0-9]{3}-[0-9]{4}$)|(\(\d{3}\)\s{0,1}\d{7}$)/;
return objRegExp.test(strValue);
}
function validatePhoneString( strValue ) {
var objRegExp  = /(^\d{10,11}$)/;
return objRegExp.test(strValue);
}
function validatePhoneNumber(strValue) {
var objRegExp = /^[0-9]{4}$/;
return objRegExp.test(strValue);
}

function validateNeededPhone( strValue ) {
var objRegExp  = /(^\d{3}-\d{3}-\d{4}$)/;
return objRegExp.test(strValue);
}

function validatePhoneACPrefix(strValue) {
var objRegExp = /(^[0-9]{3})$/;
return objRegExp.test(strValue);
}

function validateMultiEmail(strValue) {
	var objRegExp  = /^((([a-z0-9]([a-z0-9_\-\.]*)@([a-z0-9_\-\.]*)(\.[a-z]{2,4}(\.[a-z]{2}){0,2}))(\s*[\,]\s*)){0,5})([a-z0-9]([a-z0-9_\-\.]*)@([a-z0-9_\-\.]*)(\.[a-z]{2,4}(\.[a-z]{2}){0,2}))$/i;
	return objRegExp.test(strValue);
}

function validateState(strValue) {
var objRegExp = /^(AL|al|Al|AK|ak|Ak|AZ|az|Az|AR|ar|Ar|CA|ca|Ca|CO|co|Co|CT|ct|Ct|DC|dc|Dc|DE|de|De|FL|fl|Fl|GA|ga|Ga|HI|hi|Hi|ID|id|Id|IL|il|Il|IN|in|In|IA|ia|Ia|KS|ks|Ks|KY|ky|Ky|LA|la|La|ME|me|Me|MD|md|Md|MA|ma|Ma|MI|mi|Mi|MN|mn|Mn|MS|ms|Ms|MO|mo|Mo|MT|mt|Mt|NE|ne|Ne|NV|nv|Nv|NH|nh|Nh|NJ|nj|Nj|NM|nm|Nm|NY|ny|Ny|NC|nc|Nc|ND|nd|Nd|OH|oh|Oh|OK|ok|Ok|OR|or|Or|PA|pa|Pa|PR|pr|Pr|RI|ri|Ri|SC|sc|Sc|SD|sd|Sd|TN|tn|Tn|TX|tx|Tx|UT|ut|Ut|VT|vt|Vt|VA|va|Va|VI|vi|Vi|WA|wa|Wa|WV|wv|Wv|WI|wi|Wi|WY|wy|Wy)$/i;
return objRegExp.test(strValue);
}

function validateFile( strValue ) {
	var objRegExp  = /(^.*(\.csv|\.txt))$/;
	return objRegExp.test(strValue);
}

function validateFullDate(strValue){
	var objRegExp = /(^[0-9]{2}\/[0-9]{2}\/[0-9]{4})$/;
	return objRegExp.test(strValue);
}
function validateNum(strValue) {
	var objRegExp  =  /(^-?\d\d*\.\d*$)|(^-?\d\d*$)|(^-?\.\d\d*$)/;
	return objRegExp.test(strValue);
}
function validateEmail(strValue) {
	var objRegExp  = /^[a-z0-9]([a-z0-9_\-\.]*)@([a-z0-9_\-\.]*)(\.[a-z]{2,4}(\.[a-z]{2}){0,2})$/i;
	return objRegExp.test(strValue);
}
function validateZip(strValue) {
	var objRegExp  = (/(^\d{5,9}$)|(^\d{5}-\d{4}$)/);
	return objRegExp.test(strValue);
}
function validateAreaCode(strValue) {
	var objRegExp  = (/(^\d{3}$)/);
	return objRegExp.test(strValue);
}
function validate4DigitPhone(strValue) {
	var objRegExp  = (/(^\d{4}$)/);
	return objRegExp.test(strValue);
}
function validate2DigitNum(strValue) {
	var objRegExp  = (/(^\d{2}$)/);
	return objRegExp.test(strValue);
}
function validate4DigitNum(strValue) {
	var objRegExp  = (/(^\d{4}$)/);
	return objRegExp.test(strValue);
}

//declare vars
var protocol = location.protocol; // ie http: or https:
var addState = false;
var submit_form = false;
var reqFieldsConnect;

function createErrorTip(field, text) {
	field.qtip({
		content: '<div class="qtipError">' + text + '</div><div class="qtipError-btm"></div>',
		style: 'formErrorTip',
		position: {
			corner: {
				tooltip: 'bottomMiddle',
				target: 'topMiddle'
			}
		},
		show: { when: { event: 'focus' } },
		hide: { when: { event: 'blur' },
				effect: function() { $(this).qtip("destroy"); }
		}
	});

}

function getStateName(	_input, submit_val) {
	var zip = zip_input.val();
	var get_google_address = 'http://maps.google.com/maps/geo?q=' + zip + '&output=xml&key=' + $.google_api_key;
	var thru_proxy = location.protocol + '//' + location.host + '/web-services/proxy.php?url=' + encodeURIComponent(get_google_address);
	//alert(thru_proxy);
	$.ajax({
		type: "GET",
		url: thru_proxy,
		dataType: "xml",
		success: function(data){
			$(data).find('AddressDetails').each(function(){
				$("#agt_find_state_accordion").val($(this).find('AdministrativeAreaName').text());
				$("#agt_find_city_accordion").val($(this).find('LocalityName').text());

				//alert($(this).find('LocalityName').text());
				if (submit_val == true) {
					$("#urlpost_accordion").val('/divisionweb/SearchbyCity');
					$("#agent-accordion .error").addClass('displayNone');
					//$("#findAgentfind").submit();
				} else {
					$("#urlpost_accordion").val('/divisionweb/SearchbyCity');
					//return true;//$(this).text();
				}
			});
		},
		error: function(){
			alert('There was an error accessing the google api.');
			return false;
		},
        async: false
	});
}

function getStateNameFind(zip_input, submit_val) {
	var zip = zip_input.val();

	var get_google_address = 'http://maps.google.com/maps/geo?q=' + zip + '&output=xml&key=' + $.google_api_key;
	var thru_proxy = location.protocol + '//' + location.host + '/web-services/proxy.php?url=' + encodeURIComponent(get_google_address);
	//alert(thru_proxy);
	$.ajax({
		type: "GET",
		url: thru_proxy,
		dataType: "xml",
		success: function(data){
			$(data).find('AddressDetails').each(function(){
				$("#agt_find_state").val($(this).find('AdministrativeAreaName').text());
				$("#agt_find_city").val($(this).find('LocalityName').text());
				//alert($(this).find('LocalityName').text());
				if (submit_val == true) {
					$("#urlpost_find").val('/divisionweb/SearchbyCity');
					$("#findAgentfind .error").addClass('displayNone');
					//$("#findAgentfind").submit();
				} else {
					$("#urlpost_find").val('/divisionweb/SearchbyCity');
					//return true;//$(this).text();
				}
			});
		},
		error: function(){
			alert('There was an error accessing the google api.');
			return false;
		},
        async: false
	});
}

function compareStateZip(zip_input, state_input, new_state) {
	var zip = $(zip_input).val();
	var state_lookup = location.protocol + '//' + location.host + '/web-services/stateLookup.php?zipcode=' + zip + '&format=xml';
	var trimState = $.trim($(state_input).val());
	$.success = false;

	// set to error then remove if zip found

	$.ajax({
		type: "GET",
		url: state_lookup,
		dataType: "xml",
		success: function(data){
			$(data).find('state').each(function(){
				var stateAbbrev = $(this).find('abbrev').text();
				var stateName = $(this).find('name').text();

				if (trimState.length > 0) {
					if (trimState.toUpperCase() == stateAbbrev) {
						$.success = true;
					} else {
						$.success = false;
					}
				} else {
					$.success = true;
					if (new_state) {
						$('#State, #State_m').val(stateAbbrev);
						$('#state-label').css('display','block');
						$('.show-state').find('p').html(stateName);
					} else {
						$('#State option[value="' + stateAbbrev + '"]').attr('selected', 'selected');
					}
				}
			});
			if ($.success) {
				if ($(zip_input).hasClass('error') ||$(zip_input).hasClass('empty')) {
					$(zip_input).qtip("destroy");
				}
				denoteFieldAsValid('Zip');
				return true;
			} else {
				$(zip_input).addClass('error');

				createErrorTip($(zip_input),'Invalid ZIP');
				/*
				$(zip_input).qtip({
					content: '<div class="qtipError">Invalid ZIP</div><div class="qtipError-btm"></div>',
					style: { name: 'formErrorTip', width:150 },
					position: {
						corner: {
							tooltip: 'bottomMiddle',
							target: 'topMiddle'
						}
					},
					show: { when: { event: 'focus' } },
					hide: { when: { event: 'blur' },
							effect: function() { $(this).qtip("destroy"); }
					}
				});
				*/
			}

		},
        async: false

	});
}

function checkZipState(inputField,secondaryField,clearState) {
	var trimValue = $.trim($(inputField).val());

	if (trimValue.length > 0 && $(inputField).val().toUpperCase() != "ZIP") {
		if (validateZip($(inputField).val())) {

			$(inputField).ajaxStart(function() {
				$(secondaryField).parent('td').addClass('disabled');
			});

			$(inputField).ajaxStop(function() {
				$(secondaryField).parent('td').removeClass('disabled');
			});

			compareStateZip(inputField,secondaryField,clearState);

			if ($.success) {
				$(inputField).removeClass('error');
				denoteFieldAsValid('State');
			}

			return $.success;
		} else {
			$(inputField).addClass('error');

			createErrorTip($(inputField),'Invalid ZIP');

			/*
			$(inputField).qtip({
				content: '<div class="qtipError">Invalid ZIP</div><div class="qtipError-btm"></div>',
				style: { name: 'formErrorTip', width:150 },
				position: {
					corner: {
						tooltip: 'bottomMiddle',
						target: 'topMiddle'
					}
				},
				show: { when: { event: 'focus' } },
				hide: { when: { event: 'blur' },
          				effect: function() { $(this).qtip("destroy"); }
				}
			});
			*/

			return false;
		}
	} else if ($(inputField).hasClass('required')) {
		$(inputField).addClass('error').addClass('empty');

		createErrorTip($(inputField),'ZIP is required');

		/*
		$(inputField).qtip({
			content: '<div class="qtipError">ZIP is required</div><div class="qtipError-btm"></div>',
			style: { name: 'formErrorTip', width:150 },
			position: {
				corner: {
					tooltip: 'bottomMiddle',
					target: 'topMiddle'
				}
			},
			show: { when: { event: 'focus' } },
			hide: { when: { event: 'blur' },
					effect: function() { $(this).qtip("destroy"); }
			}
		});
		*/

		return false;
	} else {
		if ($(inputField).hasClass('error') ||$(inputField).hasClass('empty')) {
			$(inputField).qtip("destroy");
		}
		$(inputField).removeClass('error').removeClass('empty');
		return true;
	}
}

function checkName(inputField) {
	var trimValue = jQuery.trim($(inputField).val());

	if (trimValue.length > 0 && $(inputField).val().toUpperCase() != "FIRST NAME" && $(inputField).val().toUpperCase() != "LAST NAME") {
		if (validateAlpha($(inputField).val())) {
			$(inputField).removeClass('error').removeClass('empty');
			denoteFieldAsValid($(inputField).attr('name'));
			return true;
		} else {
			$(inputField).addClass('error');

			createErrorTip($(inputField),'Invalid Name');

			/*
			$(inputField).qtip({
				content: '<div class="qtipError">Invalid Name</div><div class="qtipError-btm"></div>',
				style: 'formErrorTip',
				position: {
					corner: {
						tooltip: 'bottomMiddle',
						target: 'topMiddle'
					}
				},
				show: { when: { event: 'focus' } },
				hide: { when: { event: 'blur' },
          				effect: function() { $(this).qtip("destroy"); }
				}
			});
			*/

			return false;
		}
	} else if ($(inputField).hasClass('required')) {

		$(inputField).addClass('error').addClass('empty');

		createErrorTip($(inputField),'Name is required');

		/*
		$(inputField).qtip({
			content: '<div class="qtipError">Name is required</div><div class="qtipError-btm"></div>',
			style: 'formErrorTip',
			position: {
				corner: {
					tooltip: 'bottomMiddle',
					target: 'topMiddle'
				}
			},
			show: { when: { event: 'focus' } },
			hide: { when: { event: 'blur' },
					effect: function() { $(this).qtip("destroy"); }
			}
		});
		*/

		return false;
	} else {
		$(inputField).removeClass('error').removeClass('empty');
		return true;
	}

}

function checkPolicyNumber(inputField) {
	var trimValue = jQuery.trim($(inputField).val());

	if (trimValue.length > 0 && $(inputField).val().toUpperCase() != "FIRST NAME" && $(inputField).val().toUpperCase() != "LAST NAME") {
		if (validateAlphaNum($(inputField).val())) {
			$(inputField).removeClass('error').removeClass('empty');
			return true;
		} else {
			$(inputField).addClass('error');
			return false;
		}
	} else if ($(inputField).hasClass('required')) {
		$(inputField).addClass('error').addClass('empty');
		return false;
	} else {
		$(inputField).removeClass('error').removeClass('empty');
		return true;
	}

}

function checkClaimNumber(inputField) {
	var trimValue = jQuery.trim($(inputField).val());

	if (trimValue.length > 0 && $(inputField).val().toUpperCase() != "FIRST NAME" && $(inputField).val().toUpperCase() != "LAST NAME") {
		if (validateNum($(inputField).val())) {
			$(inputField).removeClass('error').removeClass('empty');
			return true;
		} else {
			$(inputField).addClass('error');
			return false;
		}
	} else if ($(inputField).hasClass('required')) {
		$(inputField).addClass('error').addClass('empty');
		return false;
	} else {
		$(inputField).removeClass('error').removeClass('empty');
		return true;
	}

}

function checkAddress(inputField) {
	var trimValue = jQuery.trim($(inputField).val());

	if (trimValue.length > 0 && $(inputField).val().toUpperCase() != "ADDRESS") {
		if (validateAlphaNum($(inputField).val())) {
			$(inputField).removeClass('error').removeClass('empty');
			denoteFieldAsValid($(inputField).attr('name'));
			return true;
		} else {
			$(inputField).addClass('error');

			createErrorTip($(inputField),'Invalid Address');

			/*
			$(inputField).qtip({
				content: '<div class="qtipError">Invalid Address</div><div class="qtipError-btm"></div>',
				style: 'formErrorTip',
				position: {
					corner: {
						tooltip: 'bottomMiddle',
						target: 'topMiddle'
					}
				},
				show: { when: { event: 'focus' } },
				hide: { when: { event: 'blur' },
          				effect: function() { $(this).qtip("destroy"); }
				}
			});
			*/

			return false;
		}
	} else if ($(inputField).hasClass('required')) {
		$(inputField).addClass('error').addClass('empty');

		createErrorTip($(inputField),'Address is required');

		/*
		$(inputField).qtip({
			content: '<div class="qtipError">Address is required</div><div class="qtipError-btm"></div>',
			style: 'formErrorTip',
			position: {
				corner: {
					tooltip: 'bottomMiddle',
					target: 'topMiddle'
				}
			},
			show: { when: { event: 'focus' } },
			hide: { when: { event: 'blur' },
					effect: function() { $(this).qtip("destroy"); }
			}
		});
		*/

		return false;
	} else {
		$(inputField).removeClass('error').removeClass('empty');
		return true;
	}

}

function checkCity(inputField) {
	var trimValue = jQuery.trim($(inputField).val());

	if (trimValue.length > 0 && $(inputField).val().toUpperCase() != "CITY") {
		if (validateAlpha($(inputField).val())) {
			$(inputField).removeClass('error').removeClass('empty');
			denoteFieldAsValid($(inputField).attr('name'));
			return true;
		} else {
			$(inputField).addClass('error');

			createErrorTip($(inputField),'Invalid City');

			/*
			$(inputField).qtip({
				content: '<div class="qtipError">Invalid City</div><div class="qtipError-btm"></div>',
				style: 'formErrorTip',
				position: {
					corner: {
						tooltip: 'bottomMiddle',
						target: 'topMiddle'
					}
				},
				show: { when: { event: 'focus' } },
				hide: { when: { event: 'blur' },
          				effect: function() { $(this).qtip("destroy"); }
				}
			});
			*/

			return false;
		}
	} else if ($(inputField).hasClass('required')) {
		$(inputField).addClass('error').addClass('empty');

		createErrorTip($(inputField),'City is required');

		/*
		$(inputField).qtip({
			content: '<div class="qtipError">City is required</div><div class="qtipError-btm"></div>',
			style: 'formErrorTip',
			position: {
				corner: {
					tooltip: 'bottomMiddle',
					target: 'topMiddle'
				}
			},
			show: { when: { event: 'focus' } },
			hide: { when: { event: 'blur' },
					effect: function() { $(this).qtip("destroy"); }
			}
		});
		*/

		return false;
	} else {
		$(inputField).removeClass('error').removeClass('empty');
		return true;
	}

}

function checkEmail(inputField) {
	var trimValue = jQuery.trim($(inputField).val());

	if (trimValue.length > 0 && $(inputField).val().toUpperCase() != "E-MAIL") {

		if (validateEmail($(inputField).val())) {
			$(inputField).removeClass('error').removeClass('empty');
			denoteFieldAsValid($(inputField).attr('name'));
			return true;
		} else {
			$(inputField).addClass('error');

			createErrorTip($(inputField),'Invalid E-mail');

			/*
			$(inputField).qtip({
				content: '<div class="qtipError">Invalid E-mail</div><div class="qtipError-btm"></div>',
				style: 'formErrorTip',
				position: {
					corner: {
						tooltip: 'bottomMiddle',
						target: 'topMiddle'
					}
				},
				show: { when: { event: 'focus' } },
				hide: { when: { event: 'blur' },
          				effect: function() { $(this).qtip("destroy"); }
				}
			});
			*/

			return false;
		}
	} else if ($(inputField).hasClass('required')) {
		$(inputField).addClass('error');

		createErrorTip($(inputField),'E-mail is Required');

		/*
		$(inputField).qtip({
			content: '<div class="qtipError">E-mail is Required</div><div class="qtipError-btm"></div>',
			style: 'formErrorTip',
			position: {
				corner: {
					tooltip: 'bottomMiddle',
					target: 'topMiddle'
				}
			},
			show: { when: { event: 'focus' } },
			hide: { when: { event: 'blur' },
					effect: function() { $(this).qtip("destroy"); }
			}
		});
		*/

		return false;
	} else {
		$(inputField).removeClass('error').removeClass('empty');
		return true;
	}

}

function checkPhone(inputField) {
	var trimValue = jQuery.trim($(inputField).val());

	if (trimValue.length > 0) {
		if (validateAreaCode($(inputField).val()) && $(inputField).attr('maxlength') == 3) {
			$(inputField).removeClass('error').removeClass('empty');
			denoteFieldAsValid($(inputField).attr('name'));
			return true;
		} else if (validate4DigitPhone($(inputField).val()) && $(inputField).attr('maxlength') == 4) {
			$(inputField).removeClass('error').removeClass('empty');
			$('#Phone').val($('#PhoneArea').val() + '-' + $('#PhonePrefix').val() + '-' + $('#PhoneNum').val());
			denoteFieldAsValid($(inputField).attr('name'));
			return true;
		} else {
			$(inputField).addClass('error');

			return false;
		}
	} else if ($(inputField).hasClass('required')) {
			$(inputField).addClass('error');
		return false;
	} else {
		$(inputField).removeClass('error').removeClass('empty');
		return true;
	}

}

function checkDOB(inputField) {
	var trimValue = jQuery.trim($(inputField).val());
	if (trimValue.length > 0) {
		if (validate2DigitNum($(inputField).val()) && $(inputField).attr('maxlength') == 2) {
			$(inputField).removeClass('error').removeClass('empty');
			return true;
		} else if (validate4DigitNum($(inputField).val()) && $(inputField).attr('maxlength') == 4) {
			$(inputField).removeClass('error').removeClass('empty');
			return true;
		} else {
			$(inputField).addClass('error');
			return false;
		}
	} else if ($(inputField).hasClass('required')) {
		$(inputField).addClass('error').addClass('empty');
		return false;
	} else {
		$(inputField).removeClass('error').removeClass('empty');
		return true;
	}

}

function checkComments(inputField) {
	var trimValue = jQuery.trim($(inputField).val());
	if (trimValue.length > 0 && trimValue.length <= 254) {
		//if (validateAlphaNum($(inputField).val())) {
			$(inputField).removeClass('error').removeClass('empty');
			return true;
		/*} else {
			$(inputField).addClass('error');
			return false;
		}*/
	} else if ($(inputField).hasClass('required')) {
		$(inputField).addClass('error').addClass('empty');
		return false;
	} else {
		$(inputField).removeClass('error').removeClass('empty');
		return true;
	}

}

// Careers
// validation function and submit
function validateCareersZip(zip_input) {
	if ($("#sales-career-zip").val().length > 0 && $("#sales-career-zip").val().toUpperCase() != "ZIP") {
		if (validateZip($("#sales-career-zip").val())) {
			$("#career_find_state").val(getStateName(zip_input));
			$("#sales-career .error").addClass('displayNone');
			$("$sales-career").submit();
		} else {
			if (submit_form == true) {
				$("#sales-career-zip").focus();
			}
			$("#sales-career .error").html('ZIP Code is not valid');
			$("#sales-career .error").removeClass('displayNone');
			$("#search-careers").attr("disabled",false);
		}
	} else {
		if (submit_form == true) {
			$("#sales-career-zip").focus();
		}
		$("#sales-career .error").html('ZIP Code is required');
		$("#sales-career .error").removeClass('displayNone');
		$("#search-careers").attr("disabled",false);
	}

}

var calcHours;
var calcMinutes;
var calcAMPM;
function calcTimeForLyris(){
	calcHours = document.getElementById('Time_Hour_S').value;
	calcMinutes = document.getElementById('Time_Minute_S').value;
	calcAMPM = document.getElementById('Time_AMPM_S').value;
	document.getElementById('time_contact_S').value = calcHours + ":" + calcMinutes + " " + calcAMPM;
}

function denoteFieldAsValid(_nameOrId) {

	if (location.pathname.indexOf("find-agent") != -1 || location.pathname.indexOf("findagent") != -1 || location.pathname.indexOf("not-available") != -1) {
		for (var i=0; i<reqFieldsConnect.length; i++){
			if (reqFieldsConnect[i][0] == _nameOrId) {
				if (reqFieldsConnect[i][1]) {
					i=reqFieldsConnect.length;
				} else {
					reqFieldsConnect[i][1] = true;
					//track as field completed
					//need to derive product name and add below
					var product = "";
					if (location.pathname.indexOf("/findagent/") != -1) {
						//generic form, check for checkboxes
						if ($("#Financial_Planning").attr("checked")) { product = "Financial Planning"; }
						if ($("#Retirement_Planning").attr("checked")) { product = "Retirement Planning"; }
						if ($("#Small_Business_Planning").attr("checked")) { product = "Small Business Planning"; }
						if ($("#Estate_Planning").attr("checked")) { product = "Estate Planning"; }
						if ($("#Mutual_Funds").attr("checked")) { product = "Mutual Funds"; }
						if ($("#Annuities").attr("checked")) { product = "Annuities"; }
						if ($("#Critical_Illness").attr("checked")) { product = "Critical Illness"; }
						if ($("#Long_Term_Care").attr("checked")) { product = "Long-Term Care"; }
						if ($("#Disability").attr("checked")) { product = "Disability"; }
						if ($("#Life").attr("checked")) { product = "Life"; }
						if ($("#Medicare_Supplement").attr("checked")) { product = "Medicare Supplement"; }
						if (product == "") { product = "None Selected"; }
					} else {
						if (location.pathname.indexOf("/medicare-supplement-insurance/") != -1) {
							product = "Medicare Supplement";
						} else if (location.pathname.indexOf("/life-insurance/") != -1) {
							product = "Life";
						} else if (location.pathname.indexOf("/disability-insurance/") != -1) {
							product = "Disability";
						} else if (location.pathname.indexOf("/long-term-care-insurance/") != -1) {
							product = "Long-Term Care";
						} else if (location.pathname.indexOf("/critical-illness-insurance/") != -1) {
							product = "Critical Illness";
						} else if (location.pathname.indexOf("/annuities/") != -1) {
							product = "Annuities";
						} else {
							product = "Other";
						}
					}

                    analytics.track({category:'Connect Form',action:_nameOrId});
                    analytics.track({category:'Connect Form',action:product,label:_nameOrId});

				}
			}
		}
	} else {
		return false;
	}

}
var origMcVal;
$(document).ready(function() {
	origMcVal = $("#Media_Code").val();

	if (location.pathname.indexOf("find-agent") != -1 || location.pathname.indexOf("findagent") != -1 || location.pathname.indexOf("not-available") != -1) {
		reqFieldsConnect = new Array();

		$('.required').each(function(index) {
    		//alert(index + ': ' + $(this).attr('name'));
			//begin with required form elements marked as 'incomplete'
			reqFieldsConnect.push(new Array($(this).attr('name'),false));
  		});
	}

	$.fn.qtip.styles.formErrorTip = { // Last part is the name of the style
		background: 'none',
		width: 200,
		fontSize: 12,
		color: '#444444',
		textAlign: 'center',
		border: 0
	}

	$(".sidebar #FirstName:not([required]), #lifeBalanceForm #FirstName:not([required])").bind("click focus blur", function (inputChange) {
		switch(inputChange.type) {
			case 'focus':
				$(this).addClass('focus');
			break;
			case 'blur':
				if ($(this).val().toUpperCase() == "FIRST NAME" || $(this).val() == "") {
					$(this).val("First Name");
				}
				$(this).removeClass('focus');
				checkName(this);
			break;
			case 'click':
				if ($(this).val().toUpperCase() == "FIRST NAME") { 					$(this).focus();
				$(this).select();
				}
			break;
		}
	});

	$(".sidebar #LastName:not([required]), #lifeBalanceForm #LastName:not([required])").bind("click focus blur", function (inputChange) {
		switch(inputChange.type) {
			case 'focus':
				$(this).addClass('focus');
			break;
			case 'blur':
				if ($(this).val().toUpperCase() == "LAST NAME" || $(this).val() == "") {
					$(this).val("Last Name");
				}
				$(this).removeClass('focus');
				checkName(this);
			break;
			case 'click':
				if ($(this).val().toUpperCase() == "LAST NAME") { 					$(this).focus();
				$(this).select();
				}
			break;
		}
	});

	$(".sidebar #Address:not([required])").bind("click focus blur", function (inputChange) {
		switch(inputChange.type) {
			case 'focus':
				$(this).addClass('focus');
			break;
			case 'blur':
				if ($(this).val().toUpperCase() == "ADDRESS" || $(this).val() == "") {
					$(this).val("Address");
				}
				$(this).removeClass('focus');
				checkAddress(this);
			break;
			case 'click':
				if ($(this).val().toUpperCase() == "ADDRESS") { 					$(this).focus();
				$(this).select();
				}
			break;
		}
	});

	$(".sidebar #City:not([required])").bind("click focus blur", function (inputChange) {
		switch(inputChange.type) {
			case 'focus':
				$(this).addClass('focus');
			break;
			case 'blur':
				if ($(this).val().toUpperCase() == "CITY" || $(this).val() == "") {
					$(this).val("City");
				}
				$(this).removeClass('focus');
				checkCity(this);
			break;
			case 'click':
				if ($(this).val().toUpperCase() == "CITY") { 					$(this).focus();
				$(this).select();
				}
			break;
		}
	});

	$(".content-copy #FirstName:not([required])").bind("focus blur", function (inputChange) {
		switch(inputChange.type) {
			case 'focus':
				$(this).addClass('focus');

			break;
			case 'blur':
				$(this).removeClass('focus');
				checkName(this);
				/*
				if (checkName(this)) {
					$('#FirstName.error').qtip('destroy');
					$('#FirstName.error').qtip('hide');
				} else {
					if ($(this).hasClass('empty')) {
						var errmsg = 'First Name is required.';
					} else {
						var errmsg = 'Please enter a valid Name.';
					}

					$('#FirstName.error').qtip({
						content: errmsg,
						style: 'displayError',
						position: {
							corner: {
								tooltip: 'bottomMiddle',
								target: 'topMiddle'
							}
						},
						show: {
							when: 'focus',
							solo: false
							},
						hide: { when: 'unfocus, blur' }
					});
				}
				*/
			break;
		}
	});

	$(".content-copy #LastName:not([required])").bind("focus blur", function (inputChange) {
		switch(inputChange.type) {
			case 'focus':
				$(this).addClass('focus');
			break;
			case 'blur':
				$(this).removeClass('focus');
				checkName(this);
			break;
		}
	});

	$(".content-copy #policy_number:not([required])").bind("focus blur", function (inputChange) {
		switch(inputChange.type) {
			case 'focus':
				$(this).addClass('focus');
			break;
			case 'blur':
				$(this).removeClass('focus');
				checkPolicyNumber(this);
			break;
		}
	});

	$(".content-copy #claim_number:not([required])").bind("focus blur", function (inputChange) {
		switch(inputChange.type) {
			case 'focus':
				$(this).addClass('focus');
			break;
			case 'blur':
				$(this).removeClass('focus');
				checkClaimNumber(this);
			break;
		}
	});

	$(".content-copy #certNum:not([required])").bind("focus blur", function (inputChange) {
		switch(inputChange.type) {
			case 'focus':
				$(this).addClass('focus');
			break;
			case 'blur':
				$(this).removeClass('focus');
				checkClaimNumber(this);
			break;
		}
	});

	$(".content-copy #contractNum:not([required])").bind("focus blur", function (inputChange) {
		switch(inputChange.type) {
			case 'focus':
				$(this).addClass('focus');
			break;
			case 'blur':
				$(this).removeClass('focus');
				checkClaimNumber(this);
			break;
		}
	});

	$("#dobMonth:not([required]), #dobDay:not([required]), #dobYear:not([required])").bind("focus blur", function (inputChange) {
		switch(inputChange.type) {
			case 'focus':
				$(this).addClass('focus');
				checkDOB($(this).prev(':input'));
				$(this).prev(':input').removeClass('focus');
			break;
			case 'blur':
				$(this).removeClass('focus');
				checkDOB(this);
			break;
		}
	});

	/*$('#dobMonth').autotab({ target: 'dobDay', format: 'numeric', nospace:true });
	$('#dobDay').autotab({ target: 'dobYear', format: 'numeric', previous: 'dobMonth', nospace:true });
	$('#dobYear').autotab({ format: 'numeric', previous: 'dobDay', nospace:true });*/

	$(".content-copy #Address:not([required])").bind("focus blur", function (inputChange) {
		switch(inputChange.type) {
			case 'focus':
				$(this).addClass('focus');
			break;
			case 'blur':
				$(this).removeClass('focus');
				checkAddress(this);
			break;
		}
	});

	$(".content-copy #Address2:not([required])").bind("focus blur", function (inputChange) {
		switch(inputChange.type) {
			case 'focus':
				$(this).addClass('focus');
			break;
			case 'blur':
				$(this).removeClass('focus');
				checkAddress(this);
			break;
		}
	});

	$(".content-copy #City:not([required])").bind("focus blur", function (inputChange) {
		switch(inputChange.type) {
			case 'focus':
				$(this).addClass('focus');
			break;
			case 'blur':
				$(this).removeClass('focus');
				checkCity(this);
			break;
		}
	});

	$("#State:not([required]), #State_m:not([required])").bind("change focus blur", function (inputChange) {
		switch(inputChange.type) {
			case 'focus':
				$(this).addClass('focus');
			break;
			case 'change':
				$(this).addClass('focus');

				if ($('#Zip').val().toUpperCase() != "ZIP" && $('#Zip').val().length > 0) {
					checkZipState($('#Zip'), this, false);
				}
			break;
			case 'blur':
				$(this).removeClass('focus');
				if ($('#Zip').val().toUpperCase() != "ZIP" && $('#Zip').val().length > 0) {
					checkZipState($('#Zip'), this, false);
				}
			break;
		}
	});

	$(".sidebar #Zip:not([required])").bind("click focus blur keyup", function (inputChange) {
		switch(inputChange.type) {
			case 'focus':
				$(this).addClass('focus');
			break;
			case 'blur':
				if ($(this).hasClass('show-state')) {
					addState = true;
					$('#State').val('');
					$('.show-state p').html('');
				}

				if ($(this).val().toUpperCase() == "ZIP" || $(this).val() == "") {
					$(this).val("ZIP");
				}
				$(this).removeClass('focus');
				checkZipState(this, $('#State'),addState);
			break;
			case 'keyup':
				if ($(this).val().length >= 5) {
					if ($(this).hasClass("show-state")) {
						addState = true;
						$('#State').val('');
						$('.show-state p').html('');
					}
					checkZipState(this, $('#State'), addState);
					$(this).focus();
				}
			break;
			case 'click':
				if ($(this).val().toUpperCase() == "ZIP") {
					$(this).focus();
					$(this).select();
				}
			break;
		}
	});

	$(".content-copy #Zip:not([required]), .content-copy #zipcode_m:not([required])").bind("focus blur keyup", function (inputChange) {
		switch(inputChange.type) {
			case 'focus':
				$(this).addClass('focus');
			break;
			case 'blur':
				if ($(this).val().length < 5) {
					if ($(this).hasClass("show-state")) {
						addState = true;
						$('#State, #State_m').val('');
						$('.show-state p').html('');
					}
					$(this).removeClass('focus');
					checkZipState(this, $('#State'), addState);
				}
			break;
			case 'keyup':
				if ($(this).val().length >= 5) {
					if ($(this).hasClass("show-state")) {
						addState = true;
						$('#State').val('');
						$('.show-state p').html('');
					}
					checkZipState(this, $('#State'), addState);
					$(this).focus();
				}
			break;
		}
	});

	$("#PhoneArea:not([required]), #PhonePrefix:not([required]), #PhoneNum:not([required])").bind("focus blur", function (inputChange) {
		switch(inputChange.type) {
			case 'focus':
				$(this).addClass('focus');
				checkPhone($(this).prev(':input'));
				$(this).prev(':input').removeClass('focus');
			break;
			case 'blur':
				$(this).removeClass('focus');
				checkPhone(this);
			break;
		}
	});

	/*$('#PhoneArea').autotab({ target: 'PhonePrefix', format: 'numeric', nospace:true });
	$('#PhonePrefix').autotab({ target: 'PhoneNum', format: 'numeric', previous: 'PhoneArea', nospace:true });
	$('#PhoneNum').autotab({ format: 'numeric', previous: 'PhonePrefix', nospace:true });*/

	$(".sidebar #email:not([required]), #lifeBalanceForm #email:not([required])").bind("click focus blur", function (inputChange) {
		switch(inputChange.type) {
			case 'focus':
				$(this).addClass('focus');
			break;
			case 'blur':
				if ($(this).val().toUpperCase() == "E-MAIL" || $(this).val() == "") {
					$(this).val("E-mail");
				}
				$(this).removeClass('focus');
				checkEmail(this);
			break;
			case 'click':
				if ($(this).val().toUpperCase() == "E-MAIL") { 					$(this).focus();
				$(this).select();
				}
			break;
		}
	});

	$(".content-copy #email:not([required]), .content-copy #email_m:not([required])").bind("focus blur", function (inputChange) {
		switch(inputChange.type) {
			case 'focus':
				$(this).addClass('focus');
			break;
			case 'blur':
				$(this).removeClass('focus');
				checkEmail(this);
			break;
		}
	});

	$("#Contact_Type0:not([required])").bind("click", function (inputChange) {
		switch(inputChange.type) {
			case 'click':
				$("#best_time").show();
			break;
		}
	});

	$("#Contact_Type1:not([required])").bind("click", function (inputChange) {
		switch(inputChange.type) {
			case 'click':
				$("#best_time").hide();
			break;
		}
	});

	$("#Comments:not([required])").bind("focus blur", function (inputChange) {
		switch(inputChange.type) {
			case 'focus':
				$(this).addClass('focus');
			break;
			case 'blur':
				$(this).removeClass('focus');
				checkComments(this);
			break;
		}
	});

	$("#areaselect:not([required]), #gender_m:not([required]), #usesTobacco_m:not([required])").bind("focus blur change", function (inputChange) {
		switch(inputChange.type) {
			case 'focus':
				$(this).addClass('focus');
			break;
			case 'blur':
				$(this).removeClass('focus');
				if ($(this).hasClass('required')) {
					if (validateAlphaNum($(this).val())) {
						$(this).removeClass('error');
					} else {
						$(this).addClass('error');
					}
				}
			break;
			case 'change':
				if ($(this).hasClass('required')) {
					if (validateAlphaNum($(this).val())) {
						$(this).removeClass('error');
					} else {
						$(this).addClass('error');
					}
				}
			break;
		}
	});

	$("#Reset_button").bind("click", function (inputChange) {
		switch(inputChange.type) {
			case 'click':
				$(this).parents('form') // For each element, pick the ancestor that's a form tag.
					.find(':input') // Find all the input elements under those.
					.each(function(i) {
						$(this).removeClass('error').removeClass('empty'); // Remove error and empty tags
					});
			break;
		}
	});

	// Individual Submit Question
	// Connect with an Agent
	$('#indCSform_submit').bind('click', function(ev) {
		switch(ev.type) {
			case 'click':
				var submit_fields = new Array();
				var form_fields = new Array();
				var val_submit = false;

				$(this).attr("disabled",true);
				$(this).parent("td").addClass("disabled");

				// call validation
				if (checkName($('#FirstName'))) {
					submit_fields.push(true);
				} else {
					form_fields.push($('#FirstName'));
					submit_fields.push(false);
				}

				if (checkName($('#LastName'))) {
					submit_fields.push(true);
				} else {
					form_fields.push($('#LastName'));
					submit_fields.push(false);
				}

				if (checkPolicyNumber($('#policy_number'))) {
					submit_fields.push(true);
				} else {
					form_fields.push($('#policy_number'));
					submit_fields.push(false);
				}

				if (checkClaimNumber($('#claim_number'))) {
					submit_fields.push(true);
				} else {
					form_fields.push($('#claim_number'));
					submit_fields.push(false);
				}

				if (checkDOB($('#dobMonth'))) {
					submit_fields.push(true);
				} else {
					form_fields.push($('#dobMonth'));
					submit_fields.push(false);
				}

				if (checkDOB($('#dobDay'))) {
					submit_fields.push(true);
				} else {
					form_fields.push($('#dobDay'));
					submit_fields.push(false);
				}

				if (checkDOB($('#dobYear'))) {
					submit_fields.push(true);
				} else {
					form_fields.push($('#dobYear'));
					submit_fields.push(false);
				}

				if (checkAddress($('#Address'))) {
					submit_fields.push(true);
				} else {
					form_fields.push($('#Address'));
					submit_fields.push(false);
				}

				if (checkCity($('#City'))) {
					submit_fields.push(true);
				} else {
					form_fields.push($('#City'));
					submit_fields.push(false);
				}

				if (checkZipState($('#Zip'), $('#State'))) {
					submit_fields.push(true);
				} else {
					form_fields.push($('#Zip'));
					submit_fields.push(false);
				}

				if (checkPhone($('#PhoneArea'))) {
					submit_fields.push(true);
				} else {
					form_fields.push($('#PhoneArea'));
					submit_fields.push(false);
				}

				if (checkPhone($('#PhonePrefix'))) {
					submit_fields.push(true);
				} else {
					form_fields.push($('#PhonePrefix'));
					submit_fields.push(false);
				}

				if (checkPhone($('#PhoneNum'))) {
					submit_fields.push(true);
				} else {
					form_fields.push($('#PhoneNum'));
					submit_fields.push(false);
				}

				if (checkEmail($('#email'))) {
					submit_fields.push(true);
				} else {
					form_fields.push($('#email'));
					submit_fields.push(false);
				}

				if($('#employment2:checked').length) {
					 /*
					 * checking for current customer. overrides other media codes
					 * add by Michael E. on 12/12/2011
					 */
					if($('#CurrentCustomerY:checked').length) {
						$('#Media_Code').val('YCE**');
					} else {
						$('#Media_Code').val('CAZ**');
					}

				}
				if($('#employment1:checked').length) {
					 /*
					 * checking for current customer. overrides other media codes
					 * add by Michael E. on 12/6/2011
					 */
					if($('#CurrentCustomerY:checked').length) {
						$('#Media_Code').val('YCE**');
					} else {
						$('#Media_Code').val(origMcVal);
					}
				}

				if (checkComments($('#Comments'))) {
					submit_fields.push(true);
				} else {
					form_fields.push($('#Comments'));
					submit_fields.push(false);
				}

				$.each(submit_fields, function(key, val) {
					if (val == false) {
						val_submit = false;
						return false;
					} else {
						val_submit = true;
					}
				});

				if (val_submit == false) {
					window.location = '#' + form_fields[0].attr('id');
					form_fields[0].focus();
					$(this).parent("td").removeClass("disabled");
					$(this).attr("disabled",false);
				} else if (val_submit == true) {
					//GA Tracking
					var pathname = location.pathname.split("/");
					if (pathname[1] != "") {
						analytics.pageview({path: "/"+pathname[1]+"/find-agent.php"});
					}
					analytics.pageview({path: "/find-agent/"});

					if($('#employment2:checked').length) {
        				analytics.track({category:'Unqualified Leads - Disability',action:'Source',label:origMcVal});
					}
					$("#contactForm").submit();
				}

				return false;

			break;
		}
	});

	$("#contactForm").submit(function() {
		$("#indCSform_submit").attr("disabled",true);
	});


	// MedSupp Quoter
	$('#submit_msqform, #msqform .input-hidden').bind('click', function(ev) {
		switch(ev.type) {
			case 'click':
				var submit_fields = new Array();
				var form_fields = new Array();
				var val_submit = false;

				$(this).attr("disabled",true);
				$(this).parent("td").addClass("disabled");

				if ($('.content-copy #gender0').is(':checked') || $('.content-copy #gender1').is(':checked') || $('.content-copy #gender0').length > 0) {
					submit_fields.push(true);
					$('td.gender').removeClass("error");
				} else {
					$('td.gender').addClass("error");
					form_fields.push($('.content-copy #gender0'));
					submit_fields.push(false);
				}

				if ($('.content-copy #tobacco0').is(':checked') || $('.content-copy #tobacco1').is(':checked') || $('.content-copy #tobacco0').length > 0) {
					submit_fields.push(true);
					$('td.tobacco').removeClass("error");
				} else {
					$('td.tobacco').addClass("error");
					form_fields.push($('.content-copy #tobacco0'));
					submit_fields.push(false);
				}

				if ($('#Zip').hasClass('show-state')) {
					addState = true;
					$('#State').val('');
					$('.show-state p').html('');
				}

				// call validation
				if (checkZipState($('#Zip'), $('#State'),addState)) {
					submit_fields.push(true);
				} else {
					form_fields.push($('#Zip'));
					submit_fields.push(false);
				}

				$.each(submit_fields, function(key, val) {
					if (val == false) {
						val_submit = false;
						return false;
					} else {
						val_submit = true;
					}
				});

				if (val_submit == false) {
					form_fields[0].focus();
					$(this).parent("td").removeClass("disabled");
					$(this).attr("disabled",false);
				} else if (val_submit == true) {
					$("#msqform").submit();
				}

				return false;

			break;
		}
	});

	$("#msqform").submit(function() {
		$("#submit_msqform").attr("disabled",true);
	});


	// Annuities Update Quoter
	$('#submit_update_annuities').bind('click', function(ev) {
		switch(ev.type) {
			case 'click':
				var submit_fields = new Array();
				var form_fields = new Array();
				var val_submit = false;

				$(this).attr("disabled",true);
				$(this).parent("td").addClass("disabled");

				// call validation
				if (checkName($('#FirstName'))) {
					submit_fields.push(true);
				} else {
					form_fields.push($('#FirstName'));
					submit_fields.push(false);
				}

				if (checkName($('#LastName'))) {
					submit_fields.push(true);
				} else {
					form_fields.push($('#LastName'));
					submit_fields.push(false);
				}

				if (checkClaimNumber($('#certNum'))) {
					submit_fields.push(true);
				} else {
					form_fields.push($('#certNum'));
					submit_fields.push(false);
				}

				if (checkClaimNumber($('#contractNum'))) {
					submit_fields.push(true);
				} else {
					form_fields.push($('#contractNum'));
					submit_fields.push(false);
				}

				if (checkAddress($('#Address'))) {
					submit_fields.push(true);
				} else {
					form_fields.push($('#Address'));
					submit_fields.push(false);
				}

				if (checkAddress($('#Address2'))) {
					submit_fields.push(true);
				} else {
					form_fields.push($('#Address2'));
					submit_fields.push(false);
				}

				if (checkCity($('#City'))) {
					submit_fields.push(true);
				} else {
					form_fields.push($('#City'));
					submit_fields.push(false);
				}

				if (checkZipState($('#Zip'), $('#State'))) {
					submit_fields.push(true);
				} else {
					form_fields.push($('#Zip'));
					submit_fields.push(false);
				}

				if (checkPhone($('#PhoneArea'))) {
					submit_fields.push(true);
				} else {
					form_fields.push($('#PhoneArea'));
					submit_fields.push(false);
				}

				if (checkPhone($('#PhonePrefix'))) {
					submit_fields.push(true);
				} else {
					form_fields.push($('#PhonePrefix'));
					submit_fields.push(false);
				}

				if (checkPhone($('#PhoneNum'))) {
					submit_fields.push(true);
				} else {
					form_fields.push($('#PhoneNum'));
					submit_fields.push(false);
				}

				if (checkEmail($('#email'))) {
					submit_fields.push(true);
				} else {
					form_fields.push($('#email'));
					submit_fields.push(false);
				}

				if (checkComments($('#Comments'))) {
					submit_fields.push(true);
				} else {
					form_fields.push($('#Comments'));
					submit_fields.push(false);
				}

				$.each(submit_fields, function(key, val) {
					if (val == false) {
						val_submit = false;
						return false;
					} else {
						val_submit = true;
					}
				});

				if (val_submit == false) {
					form_fields[0].focus();
					$(this).parent("td").removeClass("disabled");
					$(this).attr("disabled",false);
				} else if (val_submit == true) {
					$("#annuitiesUpdate").submit();
				}

				return false;

			break;
		}
	});

	$("#annuitiesUpdate").submit(function() {
		$("#submit_update_annuities").attr("disabled",true);
	});

	// MedSupp Quoter
	$('#submit_lifebalance').bind('click', function(ev) {
		switch(ev.type) {
			case 'click':
				var submit_fields = new Array();
				var form_fields = new Array();
				var val_submit = false;

				$(this).attr("disabled",true);
				$(this).parent("td").addClass("disabled");

				// call validation
				if (checkName($('#FirstName'))) {
					submit_fields.push(true);
				} else {
					form_fields.push($('#FirstName'));
					submit_fields.push(false);
				}

				if (checkName($('#LastName'))) {
					submit_fields.push(true);
				} else {
					form_fields.push($('#LastName'));
					submit_fields.push(false);
				}

				if (checkEmail($('#email'))) {
					submit_fields.push(true);
				} else {
					form_fields.push($('#email'));
					submit_fields.push(false);
				}

				$.each(submit_fields, function(key, val) {
					if (val == false) {
						val_submit = false;
						return false;
					} else {
						val_submit = true;
					}
				});

				if (val_submit == false) {
					form_fields[0].focus();
					$(this).parent("td").removeClass("disabled");
					$(this).attr("disabled",false);
				} else if (val_submit == true) {
					var dataString = $("#lifeBalanceForm").serialize();

					$.ajax({
						type: "POST",
						url: "http://www.mutualofomaha.com/includes/lyris/lyris_insert.php",
						data: dataString,
						success: function(data){
							if (data == "true") {
								$('#submit_lifebalance').attr("disabled",false).parent("td").removeClass("disabled");
								$('.success').html('Email Sent').fadeIn(400).delay(2000).fadeOut(400);
								$('#FirstName').val("First Name");
								$('#LastName').val("Last Name");
								$('#email').val("E-mail");
								$('#FirstName').select().addClass('focus');
								$('#FirstName, #LastName, #email').removeClass('error');
							} else {
								$('#submit_lifebalance').attr("disabled",false).parent("td").removeClass("disabled");
								$('.success').html('Email Error').addClass('error').fadeIn(400).delay(2000).fadeOut(400);
							}
						},
						error: function(){
							$('#submit_lifebalance').attr("disabled",false).parent("td").removeClass("disabled");
							$('.success').html('Email Error').addClass('error').fadeIn(400).delay(2000).fadeOut(400);
							//alert('There was an error accessing Lyris.');
							return false;
						},
						async: false
					});

					// $("#lifeBalanceForm").submit();
				}

				return false;

			break;
		}
	});

	$("#lifeBalanceForm").submit(function() {
		$("#submit_lifebalance").attr("disabled",true);
	});

	// Sales Careers Search
	$("#sales-career-zip").bind("focus blur", function (inputChange) {
		switch(inputChange.type) {
			case 'focus':
				if ($(this).val().toUpperCase() == "ZIP") { $(this).select(); }
				$(this).addClass('focus');
			break;
			case 'blur':
				if ($(this).val().toUpperCase() == "ZIP" || $(this).val() == "") {
					$(this).val("ZIP");
				}
				$(this).removeClass('focus');
			break;
		}
	});

	// Search Sales Careers submit
	$('#search-careers').bind('click', function(ev) {
		switch(ev.type) {
			case 'click':
				submit_form = true;
				// call validation
				validateCareersZip($('#sales-career-zip'));
				return false;
			break;
		}
	});

	$("#sales-career").submit(function() {
		$("#search-careers").attr("disabled",true);
	});

	$("#indAccess").submit(function() {
		var text = $('input:text, input:password', $(this));
		var pass = true;

		text.each(function(){
			if(!validateAlphaNum($(this).val())) {
				$(this).addClass('error');
				pass = false;
			} else {
				$(this).removeClass('error');
			}
		});
		return pass;
	});

	// Agent Lookup FIND
	// Agent
	$("#agent-lookup").bind("focus select blur change click", function (inputChange) {
		switch(inputChange.type) {
			case 'focus':
				$(this).addClass('focus');
			break;
			case 'select':
				$(this).addClass('focus');
			break;
			case 'blur':
				if ($(this).val().toUpperCase() == "ZIP OR LAST NAME" || $(this).val() == "") {
					$(this).val("ZIP or Last Name");
					$(this).removeClass('focus');
				}
			break;
			case 'change':
				$(this).removeClass('focus');
			break;
			case 'click':
				$(this).select();
			break;
		}
	});

	// find agent submit
	$('#agent-go').bind('click', function(ev) {
		switch(ev.type) {
			case 'click':
				$("#findAgentfind").submit();
			break;
		}
	});

	/*$("#findAgentfind").submit(function() {
		if ($("#agent-lookup").val().length > 0 && $("#agent-lookup").val().toUpperCase() != "ZIP OR LAST NAME") {
			if (validateAlpha($("#agent-lookup").val())) {
				$("#lastname").val($("#agent-lookup").val());
				$("#urlpost_find").val('/divisionweb/SearchAgent');
				$("#findAgentfind .error").addClass('displayNone');
			} else if ($("#agent-lookup").val().length == 5 && validateNum($("#agent-lookup").val())) {
				$("#agt_find_state").val(getStateNameFind($("#agent-lookup"), true));
			} else {
				$("#agent-lookup").select();
				$("#agent-go").attr("disabled",false);
				$("#findAgentfind .error").html('ZIP Code Or Last Name is not valid');
				$("#findAgentfind .error").removeClass('displayNone');
				return false;
			}
		} else {
			$("#agent-lookup").select();
			$("#agent-go").attr("disabled",false);
			$("#findAgentfind .error").html('ZIP Code Or Last Name is required');
			$("#findAgentfind .error").removeClass('displayNone');
			return false
		}
	});	*/

	// Quote FIND submit
	$('#quote-find').submit(function() {
		var urlSuffix = "plan/quote.php?src=find&page="+location.pathname;
		switch ($('#quote-find-product').val() ) {
			case 'life':
				$(this).attr('action', 'http://' + location.host + '/life-insurance/' + urlSuffix);
				break;

			case 'di':
				$(this).attr('action', 'http://' + location.host + '/disability-insurance/' + urlSuffix);
				break;

			case 'medsupp':
				$(this).attr('action', 'http://' + location.host + '/medicare-supplement-insurance/' + urlSuffix);
				break;

			case 'ltc':
				$(this).attr('action', 'http://' + location.host + '/long-term-care-insurance/' + urlSuffix);
				break;

			default:
				break;
		}
	});

	$('#quote-find-submit').bind('click', function(ev) {
		switch(ev.type) {
			case 'click':
				var submit_fields = new Array();
				var form_fields = new Array();
				var val_submit = false;

				$(this).attr("disabled",true);
				$(this).parent("td").addClass("disabled");

				// call validation
				if ($('#quote-find-product').val() !== "" && validateAlpha($('#quote-find-product').val())) {
					submit_fields.push(true);
				} else {
					form_fields.push($('#quote-find-product'));
					submit_fields.push(false);
				}

				if ($('#quote-find-state').val() !== "" && validateAlpha($('#quote-find-state').val())) {
					submit_fields.push(true);
				} else {
					form_fields.push($('#quote-find-state'));
					submit_fields.push(false);
				}

				$.each(submit_fields, function(key, val) {
					if (val == false) {
						val_submit = false;
						return false;
					} else {
						val_submit = true;
					}
				});

				if (val_submit == false) {
					form_fields[0].focus();
					$(this).parent("td").removeClass("disabled");
					$(this).attr("disabled",false);
				} else if (val_submit == true) {
					$("#quote-find").submit();
				}

				return false;
			break;
		}
	});

	$("#quote-find").submit(function() {
		$("#quote-find-submit").attr("disabled",true);
	});


	$('#submit_email_form').bind('click', function(ev) {
		switch(ev.type) {
			case 'click':
				var submit_fields = new Array();
				var form_fields = new Array();
				var val_submit = false;

				$(this).attr("disabled",true);
				$(this).parent("td").addClass("disabled");

				// call validation
				$(this).parents('form') // For each element, pick the ancestor that's a form tag.
					.find(':input') // Find all the input elements under those.
						.each(function() {
							if ($(this).hasClass('name') || $(this).attr('id') == 'FirstName' || $(this).attr('id') == 'LastName') {
								if (checkName($(this))) {
									submit_fields.push(true);
								} else {
									form_fields.push($(this));
									submit_fields.push(false);
								}
							} else if ($(this).attr('id') == 'email') {
								if (checkEmail($(this))) {
									submit_fields.push(true);
								} else {
									form_fields.push($(this));
									submit_fields.push(false);
								}
							} else if ($(this).attr('id') == 'Address') {
								if (checkAddress($(this))) {
									submit_fields.push(true);
								} else {
									form_fields.push($(this));
									submit_fields.push(false);
								}
							} else if ($(this).attr('id') == 'City') {
								if (checkCity($(this))) {
									submit_fields.push(true);
								} else {
									form_fields.push($(this));
									submit_fields.push(false);
								}
							} else if ($(this).attr('id') == 'Zip') {
								if ($(this).hasClass('show-state')) { // determine if state will be displayed
									addState = true; // addState is false by default
									$('#State').val('');
									$('.show-state p').html('');
								}

								if (checkZipState($(this), $('#State'), addState)) {
									submit_fields.push(true);
								} else {
									form_fields.push($(this));
									submit_fields.push(false);
								}
							} else if ($(this).hasClass('phone3') || $(this).hasClass('phone4')) {
								if (checkPhone($(this))) {
									submit_fields.push(true);
								} else {
									form_fields.push($(this));
									submit_fields.push(false);
								}
							} else if ($(this).attr('id') == 'Comments') {
								if (checkComments($(this))) {
									submit_fields.push(true);
								} else {
									form_fields.push($(this));
									submit_fields.push(false);
								}
							}
				});

				$(this).parents('form') // For each element, pick the ancestor that's a form tag.
					.find('select') // Find all the input elements under those.
						.each(function() {
							if ($(this).hasClass('required')) {
								if (validateAlphaNum($(this).val())) {
									$(this).removeClass('error');
									submit_fields.push(true);
								} else {
									$(this).addClass('error');
									form_fields.push($(this));
									submit_fields.push(false);
								}
							}
				});

				$.each(submit_fields, function(key, val) {
					if (val == false) {
						val_submit = false;
						return false;
					} else {
						val_submit = true;
					}
				});

				if (val_submit == false) {
					form_fields[0].focus();
					$(this).parent("td").removeClass("disabled");
					$(this).attr("disabled",false);
				} else if (val_submit == true) {
					$("#email_form").submit();
				}

				return false;
			break;
		}
	});

	$("#email_form").submit(function() {
		$("#submit_email_form").attr("disabled",true);
	});

});