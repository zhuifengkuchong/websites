
/*** Begin Flash Detection & Insert Utility ***/
// Detect Client Browser type
var isIE  = (navigator.appVersion.indexOf("MSIE") != -1) ? true : false;
var isWin = (navigator.appVersion.toLowerCase().indexOf("win") != -1) ? true : false;
var isOpera = (navigator.userAgent.indexOf("Opera") != -1) ? true : false;
jsVersion = 1.1;

/*************************************************************************/
if(isIE && isWin) {
	document.write("<" + "SCR" + "IPT LANGUAGE=VBScript> \n");
    document.write("  Function VBGetSwfVer(i) \n");
    document.write("    on error resume next \n");
    document.write("    Dim swControl, swVersion \n");
    document.write("    swVersion = 0 \n");
    document.write("    set swControl = CreateObject(\"ShockwaveFlash.ShockwaveFlash.\" + CStr(i)) \n");
    document.write("    if (IsObject(swControl)) then \n");
    document.write("      swVersion = swControl.GetVariable(\"$version\") \n");
	document.write("      break  \n");
    document.write("    end if \n");
    document.write("    VBGetSwfVer = swVersion \n");
    document.write("End Function \n");
	document.write("// -->  \n");
	document.write("<" + "/SCR" + "IPT> \n");
}
/************************************************************************/

// JavaScript helper required to detect Flash Player PlugIn version information
function JSGetSwfVer(i) {
	// NS/Opera version >= 3 check for Flash plugin in plugin array
	if (navigator.plugins != null && navigator.plugins.length > 0) {
		if (navigator.plugins["Shockwave Flash 2.0"] || navigator.plugins["Shockwave Flash"]) {
			var swVer2 = navigator.plugins["Shockwave Flash 2.0"] ? " 2.0" : "";
      		var flashDescription = navigator.plugins["Shockwave Flash" + swVer2].description;
			descArray = flashDescription.split(" ");
			tempArrayMajor = descArray[2].split(".");
			versionMajor = tempArrayMajor[0];
			versionMinor = tempArrayMajor[1];
			if ( descArray[3] != "" ) {
				tempArrayMinor = descArray[3].split("r");
			} else {
				tempArrayMinor = descArray[4].split("r");
			}
      		versionRevision = tempArrayMinor[1] > 0 ? tempArrayMinor[1] : 0;
            flashVer = versionMajor + "." + versionMinor + "." + versionRevision;
      	} else {
			flashVer = -1;
		}
	}
	// MSN/WebTV 2.6 supports Flash 4
	else if (navigator.userAgent.toLowerCase().indexOf("webtv/2.6") != -1) flashVer = 4;
	// WebTV 2.5 supports Flash 3
	else if (navigator.userAgent.toLowerCase().indexOf("webtv/2.5") != -1) flashVer = 3;
	// older WebTV supports Flash 2
	else if (navigator.userAgent.toLowerCase().indexOf("webtv") != -1) flashVer = 2;
	// Can't detect in all other cases
	else {
		flashVer = -1;
	}
	return flashVer;
}

// If called with no parameters this function returns a floating point value 
// which should be the version of the Flash Player or 0.0 
// ex: Flash Player 7r14 returns 7.14
// If called with reqMajorVer, reqMinorVer, reqRevision returns true if that version or greater is available
function DetectFlashVer(reqMajorVer, reqMinorVer, reqRevision) {
 	reqVer = parseFloat(reqMajorVer + "." + reqRevision);
   	// loop backwards through the versions until we find the newest version	
	for (i=25;i>0;i--) {	
		if (isIE && isWin && !isOpera) {
			versionStr = VBGetSwfVer(i);
		} else {
			versionStr = JSGetSwfVer(i);		
		}
		if (versionStr == -1 ) { 
			return false;
		} else if (versionStr != 0) {
			if(isIE && isWin && !isOpera) {
				tempArray         = versionStr.split(" ");
				tempString        = tempArray[1];
				versionArray      = tempString .split(",");				
			} else {
				versionArray      = versionStr.split(".");
			}
			versionMajor      = versionArray[0];
			versionMinor      = versionArray[1];
			versionRevision   = versionArray[2];
			
			versionString     = versionMajor + "." + versionRevision;   // 7.0r24 == 7.24
			versionNum        = parseFloat(versionString);
        	// is the major.revision >= requested major.revision AND the minor version >= requested minor
			if ( (versionMajor > reqMajorVer) && (versionNum >= reqVer) ) {
				return true;
			} else {
				return ((versionNum >= reqVer && versionMinor >= reqMinorVer) ? true : false );	
			}
		}
	}	
	return (reqVer ? false : 0.0);
}

function insertFlashObject(mUrl,mID,mWidth,mHeight,mMenu,mBGColor,fpVersion,altImg) {	//parameters accepted are movie url (mUrl), movie id (mID), movie width (mWidth), movie height (mHeight), movie menu (mMenu), movie background color (mBGColor), Flash Player required (fpVersion), alternate image (altImg)
	var requiredMajorVersion = fpVersion;
	var requiredMinorVersion = 0;
	var requiredRevision = 0;
	var hasRequiredFlashPlayerVersion = DetectFlashVer(requiredMajorVersion, requiredMinorVersion, requiredRevision);
	var oeTags = "";
	
	if (hasRequiredFlashPlayerVersion) {
		oeTags += "<object classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" codebase=\"http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab\" width=\"" + mWidth + "\" height=\"" + mHeight + "\" id=\"" + mID + "\">";
		oeTags += "<param name=\"wmode\" value=\"transparent\" />";
		oeTags += "<param name=\"movie\" value=\"" + mUrl + "\" />";
		oeTags += "<param name=\"quality\" value=\"high\" />";
		oeTags += "<param name=\"menu\" value=\"" + mMenu + "\" />";
		oeTags += "<param name=\"bgcolor\" value=\"#" + mBGColor + "\" />";
		oeTags += "<embed src=\"" + mUrl + "\" quality=\"high\" menu=\"" + mMenu + "\" bgcolor=\"#" + mBGColor + "\" pluginspage=\"http://www.macromedia.com/go/getflashplayer/\" type=\"application/x-shockwave-flash\" width=\"" + mWidth + "\"  height=\"" + mHeight + "\"></embed>";
		oeTags += "</object>";
		
		document.write(oeTags);
	} else { //doesn't have required version of Flash Player
		//Display alternative content
		document.write("<div id=\"noflash_image\"><a href=\"http://www.macromedia.com/go/getflashplayer\" target=\"_blank\"><img src=\"" + altImg + "\" width=\"" + mWidth + "\" height=\"" + mHeight + "\" border=\"0\" alt=\"\" class=\"noflash\" /></a></div>");
		document.write("<p class=\"noflash\" style=\"padding-left:8px;\"><strong>Note:</strong><br /><span class=\"error\">Flash Player " + fpVersion + " or above is required to view this content.</span>  <a href=\"http://www.macromedia.com/go/getflashplayer\" target=\"_blank\">Download Flash Player</a>.</p>");
	}
}
/*** End Flash Detection & Insert Utility ***/

function insertString(_stringData) {
	document.write(_stringData);
}