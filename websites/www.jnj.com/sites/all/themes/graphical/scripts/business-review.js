(function($) {

  $(function() {
    if ($('.business-review-style li').length != 0) {
      $('.business-review-style li').each(function(){
        var link = $(this).find('http://www.jnj.com/sites//all//themes//graphical//scripts//a.pdf, a.img').attr('href');

        var wrapperDiv = $('<div/>');
        wrapperDiv.addClass('business-review-wrapper');
        wrapperDiv.append($(this).find(' > *'));

        var wrapperLink = $('<a/>');
        wrapperLink.attr('target', '_blank');
        wrapperLink.attr('href', link);
        wrapperLink.append(wrapperDiv);

        $(this).prepend(wrapperLink);
      });
    }

    if ($('.pharma-businessreview').length != 0) {
      $('.pharma-businessreview div.elements').each(function(){
        h_element = $(this).height();	
        h_parent = $(this).parent().height();
        m_top = (h_parent - h_element) / 2;
        $(this).css({
          'margin-top' : m_top + 'px' 	
        })
      });
    }
  });
})(jQuery);