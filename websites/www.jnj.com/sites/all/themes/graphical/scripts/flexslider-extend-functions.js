/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function destroyFlexslider(flexslider) {
  jQuery(flexslider).each(function(){
    var el = jQuery(this);
    var elClean = el.clone();

    elClean.find('.flex-viewport').children().unwrap();
    elClean
      .removeClass('flexslider')
      .find('.clone, .flex-direction-nav, .flex-control-nav, .direction-nav')
        .remove()
        .end()
      .find('*').removeAttr('style').removeClass (function (index, css) {
        return (css.match (/\bflex\S+/g) || []).join(' ');
      });

    elClean.insertBefore(el);
    el.remove();
  });
}
