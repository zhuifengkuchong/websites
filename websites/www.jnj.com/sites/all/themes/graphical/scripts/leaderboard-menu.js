/*
 *  Setting variables to script of leaderboard menu.
 */
(function($) {
  //Set a global variable to check when the device is BlackBerry
  var isBlackberry = navigator.userAgent.match(/BlackBerry/i);
  var leaderboardMenuSelector = 'div.region-leaderboard .leaderboard-menu';

  $(function () {
    addMediaQueries();
  });

  function addMediaQueries() {
    MQ.addQuery({
      context: ['smartphone_portrait', 'smartphone_landscape'],
      call_in_each_context: false,
      callback: function() {
        //Set the resize callback just for smartphone.
        rotateBehavior();
        createFlexslider();
      }
    });

    //By default do not apply the flexslider for tablets and desktop.
    MQ.addQuery({
      context: ['tablet_portrait', 'tablet_landscape', 'standard'],
      call_in_each_context: false,
      callback: function() {
        //Remove the elements from flexslider.
        removeFlexslider(leaderboardMenuSelector);
      }
    });
  }

  function createFlexslider() {
    //First of all, remove the elements and let's start from a clean structure.
    removeFlexslider($(leaderboardMenuSelector));

    //Add flexslider class, required by the plugin.
    $(leaderboardMenuSelector).addClass('flexslider');
    $(leaderboardMenuSelector).find('div div ul').addClass('slides');


    //Apply flexslider
    $(leaderboardMenuSelector).flexslider({
      animation: "slide",
      animationLoop: false,
      controlNav: false,
      slideshow: false,
      itemWidth: 80,
      minItems: 3,
      touch: true,
      maxItems: 3,
      after: function(slider) {
        $(leaderboardMenuSelector).find(".prev, .next").show();
        if (slider.currentSlide == 0) {
          $(leaderboardMenuSelector).find(".prev").hide();
        }
        else if (slider.currentSlide == slider.last) {
          $(leaderboardMenuSelector).find(".next").hide();
        }
      }
    });

    add_prevnext();
  }

  //Function used to clear all the elements used by the flexslider.
  function removeFlexslider(selector) {
    destroyFlexslider(selector);

    remove_prevnext();
  }

  //Add custom buttons.
  function add_prevnext() {
    if (Drupal.settings && Drupal.settings.basePath && Drupal.settings.jnj && Drupal.settings.jnj.themePath) {
      var bhtml = '<ul class="direction-nav">';
      bhtml += '<li><a href="#" class="prev">&nbsp;</a></li>';
      bhtml += '<li><a href="#" class="next">&nbsp;</a></li>';
      bhtml += '</ul>';

      var slider = $(leaderboardMenuSelector);

      //Remove default flexslider navigation.
      slider.find(".flex-direction-nav").remove();
      slider.append(bhtml);

      //Hide button prev on load {
      slider.find(".prev").css('display', 'none');

      //Simulate navigation behavior.
      slider.find(".prev").click(function () {
        if($(".next").not('visible')){
          slider.flexslider("previous");
        }
        return false;
      });

      //Simulate navigation behavior.
      slider.find(".next").click(function () {
        if($(".prev").not('visible')){
          slider.flexslider("next");
        }
        return false;
      });
    }
  }

  //Remove custom buttons.
  function remove_prevnext() {
    $(leaderboardMenuSelector).find('.direction-nav').remove();
  }

  function rotateBehavior() {
    if(!isBlackberry) {
      $(window).resize(function() {
        setTimeout(function() {
          //Once we set a timout, this function may happens when user finished resizing his browser from smartphone layout to tablets/desktop.
          //So we do not create the slider when he is not in smartphones
          if (MQ.context == 'smartphone_portrait' || MQ.context == 'smartphone_landscape') {
            createFlexslider();
          }
        }, 200);
      });
    }
  }

})(jQuery);