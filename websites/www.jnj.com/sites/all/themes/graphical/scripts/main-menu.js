(function($){
  $(function() {
    // Mobile link
    $(".open-main-menu a").click(function(event) {
      event.preventDefault();
      if ($(this).hasClass('open')) {
        closeMainMenu();
      }
      else {
        openMainMenu();
      }
    });

    // Tablet click outside
    $('*').bind('click',function(){
      if ($.inArray(MQ.context, ['tablet_portrait', 'tablet_landscape']) != -1 && Modernizr.touch) {
        if ($(this).closest('.main-menu').length == 0) {
          if ($('.main-menu ul ul:visible').length > 0) {
            animateCloseMainMenu();
          }
        }
      }
    });

    $('.main-menu').click(function(event){
      event.stopPropagation();
    });

    // Media Queries
    MQ.addQuery({
      context: ['smartphone_portrait', 'smartphone_landscape', 'tablet_portrait', 'tablet_landscape', 'standard'],
      call_in_each_context: false,
      callback: function() {
        // Reset Behavior
        resetBehaviors();

        // CloseAllMenus
        closeAllSubMainMenu();
      }
    });

    MQ.addQuery({
      context: ['smartphone_portrait', 'smartphone_landscape'],
      call_in_each_context: false,
      callback: function() {
        // Main menu
        closeMainMenu();

        $('.main-menu li.expanded li a').show();

        // Sub-menu
        $('.main-menu li.expanded > a').bind('click', function(event) {
          event.preventDefault();
          toggleSubMainMenu($(this));
        });
      }
    });

    MQ.addQuery({
      context: ['tablet_portrait', 'tablet_landscape', 'standard'],
      call_in_each_context: false,
      callback: function() {
        // Main menu
        openMainMenu();
        if (Modernizr.touch) {
          if ($('.main-menu li.expanded > span.static-menu-item').length == 0) {
            $.each($('.main-menu li.expanded > a'), function(index, value) {
              var classes = "static-menu-item";
              if ($(this).hasClass('active-trail')) {
                classes += " active-trail";
              }
              $(this).parent().prepend('<span style="display:block" class="'+ classes +'">' + $(this).text());
              $(this).css('display', 'none');
            });
          }

          $('.main-menu li.expanded li a').show();

          // Sub-menu
          $('.main-menu li.expanded > a').bind('click', function(event) {
            event.preventDefault();
          });

          $('.main-menu li.expanded').bind('click', function() {
            if ($(" > a", $(this)).hasClass("nav-hover")) {
              $(" > span.static-menu-item", $(this)).removeClass("active-trail");
              animateCloseMainMenu();
            }
            else {
              $(".main-menu li.expanded > span.static-menu-item").removeClass("active-trail");
              $(" > span.static-menu-item", $(this)).addClass("active-trail");
              animateOpenMainMenu($(this));
            }
          });
        } else {
          $('.main-menu li.expanded').each(function(){
            $('li a:first', $(this)).hide();
          });

          // Sub-menu
          $('.main-menu li.expanded').bind('mouseover', function() {
            if (!$(" > a", $(this)).hasClass("nav-hover")) {
              animateOpenMainMenu($(this));
            }
          });

          $('.main-menu li.expanded').bind('mouseleave', function() {
            animateCloseMainMenu();
          });
        }
      }
    });
  });

  /**
  *  Setting variables to script of main menu use.
  */
  var animateTimer = null;

  function animateOpenMainMenu(submenu) {
    $(".main-menu li ul.menu").slideUp(101);
    headerDefaultHeight = $("#header").height();

    clearAnimateTimer();
    animateTimer = setTimeout(function() {

      height = $(".main-menu li ul.menu").height() + $(".main-menu ul").height() + $(".main-menu").height();
      $("#header").animate({height:height}, 400, 'linear');
      $(submenu).children("ul").slideDown(500);

      $(".main-menu li.expanded > a").removeClass("nav-hover");
      $(submenu).children("a").addClass("nav-hover");
    }, 200);
  }

  function animateCloseMainMenu() {
    $(".main-menu li ul.menu").slideUp(101);

    clearAnimateTimer();

    animateTimer=setTimeout(function(){
      $("#header").animate({height:101}, 400, 'linear');

      $(".main-menu li.expanded > a").removeClass("nav-hover");

      $('.main-menu .active-trail').removeClass('active-trail');
      $('.main-menu .active').parents('li').addClass('active-trail');
      $('.main-menu li.active-trail').children('a, span').addClass('active-trail');
    }, 300);
  }

  function openMainMenu() {
    $(".main-menu").show();
    $(".open-main-menu a").addClass('open');
    $('.main-menu li.expanded').removeClass('open');
    $('.main-menu li.expanded ul').hide();
  }

  function closeMainMenu() {
    $(".main-menu").hide();
    $(".open-main-menu a").removeClass('open');
    $('.main-menu li.expanded').removeClass('open');
    $('.main-menu li.expanded ul').hide();
  }

  function toggleSubMainMenu(obj) {
    obj.parent('li.expanded').toggleClass('open');
    obj.next('ul').toggle();
  }

  function closeAllSubMainMenu() {
    $('.main-menu li.expanded').removeClass('open');
    $('.main-menu li.expanded > ul').hide();

    $('#header').css('height', '');
  }

  function clearAnimateTimer() {
    clearTimeout(animateTimer);
    animateTimer = null;
  }

  function resetBehaviors(){
    clearAnimateTimer();
    $('.main-menu li.expanded').unbind('mouseover');
    $('.main-menu li.expanded').unbind('mouseleave');
    $('.main-menu li.expanded').unbind('click');
    $('.main-menu li.expanded > a').unbind('click');
  }
})(jQuery);