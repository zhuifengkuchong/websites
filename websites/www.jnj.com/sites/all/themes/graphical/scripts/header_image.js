/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

(function($){
  var fill_header = false;
  $(function() {
    //We add the header image unless the device is a smartphone.
    MQ.addQuery({
      context: ['tablet_portrait', 'tablet_landscape', 'standard'],
      call_in_each_context: false,
      callback: function() {
        if (Drupal.settings.jnj && Drupal.settings.jnj.node && Drupal.settings.jnj.node.header_image && (fill_header == false)) {
          fill_header = true;
          jQuery('#content-column').css('background-image', 'url("' + Drupal.settings.jnj.node.header_image + '")');
          jQuery('#content-column').addClass('header-image');
        }
        else if (fill_header == false) {
          $('body').addClass('no-header-image');
        }
      }
    });
  });
})(jQuery);