(function($) {
  
  //Rebuild gallery if using back/forward cache on Mobile Safari.
  $(window).bind("pageshow", function(event) {
    if (event.originalEvent.persisted) {
      createAllGallery(true);
    }
  });
  
  var startAt = {};

  $(function() {

    // Media Queries
    MQ.addQuery({
      context: ['smartphone', 'smartphone_landscape'],
      call_in_each_context: false,
      callback: function() {
        createAllGallery(true);
      }
    });

    MQ.addQuery({
      context: ['tablet_portrait', 'tablet_landscape', 'standard'],
      call_in_each_context: false,
      callback: function() {
        createAllGallery(false);
      }
    });
  });

  function createAllGallery(mobile) {
    createGallery('div.view-display-id-whats_new', mobile, 21);
    createGallery('div.view-display-id-our_pillars', mobile, 21);
    createGallery('div.view-display-id-our_result_data', mobile, 21);
    createGallery('div.recently-updated-reports-stories', mobile, 21);
    createGallery('div.featured-stories', mobile, 13);
  }

  function createGallery(selector, mobile, margin) {
    if ($(selector).length == 0) {
      return;
    }
    
    $(selector + ' .slider li.empty').remove();
    destroyFlexslider(selector + ' .slider');

    var wrapper = $(selector);
    var pages = 0;
    var slider;

    if (mobile == true) {
      initGallery(wrapper, 2);
      slider = $('.slider', wrapper);
      /* MOBILE VERSION */
      slider.flexslider({
        animation: "slide",
        animationLoop: true,
        pauseOnAction: false,
        pauseOnHover: true,
        slideshow: false,
        directionNav: !Modernizr.touch,
        controlNav: false,
        move: 2,
        minItems: 2,
        maxItems: 2,
        itemWidth: 115,
        itemMargin: margin,
        startAt: startAt[selector],
        after: function(slider) {
          var pages = Math.ceil(slider.count/slider.move);
          var hasPagination = (pages == 1 ? false : true);
          startAt[selector] = $('.slider').data('flexslider').currentSlide;

          if (hasPagination) {
            $('li.flex-nav-pager', slider).html(slider.currentSlide+1 + Drupal.t(' of ') + pages);
          }
        }
      });
    } else{
      initGallery(wrapper, 3);
      slider = $('.slider', wrapper);
      /* DESKTOP VERSION */
      slider.flexslider({
        animation: "slide",
        animationLoop: true,
        pauseOnAction: false,
        pauseOnHover: true,
        slideshow: false,
        directionNav: true,
        controlNav: false,
        move: 3,
        minItems: 3,
        maxItems: 3,
        itemWidth: 200,
        itemMargin: margin,
        startAt: startAt[selector],
        after: function(slider) {
          var pages = Math.ceil(slider.count/slider.move);
          var hasPagination = (pages == 1 ? false : true);
          startAt[selector] = $('.slider').data('flexslider').currentSlide;

          if (hasPagination) {
            $('li.flex-nav-pager', slider).html(slider.currentSlide+1 + Drupal.t(' of ') + pages);
          }
        }
      });
    }
    if (Modernizr.touch) {
      slider.addClass('touch');
    } else {
      slider.removeClass('touch');
    }

    startAt[selector] = $('.slider').data('flexslider').currentSlide;

    var sliderData = slider.data('flexslider');
    var sliderPages = Math.ceil(sliderData.count/sliderData.move);
    var hasPagination = (sliderPages == 1 ? false : true);

    if (hasPagination) {
      $('ul.flex-direction-nav li:first', slider).after('<li class="flex-nav-pager">' + (sliderData.currentSlide+1) + Drupal.t(' of ') + sliderPages + '</li>');
    }

    /*fix transition from mobile to desktop where current page on mobile is higher than desktop count.*/
    if (sliderData.currentSlide >= sliderPages) {
      sliderData.flexslider(sliderPages-1);
    }
    /* fix end */
  }

  function initGallery(wrapper, items) {
    var mainList = wrapper.find('div.view-content > .item-list');
    mainList.addClass('slider');

    var count = mainList.find('ul.slides li').length;
    var totalPages = Math.ceil(count/items);
    for (var i = count; i < totalPages*items; i++) {
      mainList.find('ul.slides').append('<li class="views-row slide empty"></li>');
    }
  }

})(jQuery);