(function($){
  $(function() {
    disableRightClick(".no-copy-image img");
    disableDragging(".no-copy-image img");

//    var hmtl5 = "<input list='browsers' />" +
//    "<datalist id='browsers'>" +
//      "<option value='Chrome'></option>" +
//      "<option value='Firefox'></option>" +
//      "<option value='Internet Explorer'></option>" +
//      "<option value='Opera'></option>" +
//      "<option value='Safari'></option>" +
//    "</datalist>";
//  
//    $("#block-block-7 form").append(hmtl5);
  });

  function disableRightClick( selector ) {
    $(selector).bind("contextmenu",function(e){
      e.preventDefault();
    });
  }

  function disableDragging( selector ) {
    $(selector).bind('dragstart',function(e){
      e.preventDefault();
    });
  }

  
})(jQuery);