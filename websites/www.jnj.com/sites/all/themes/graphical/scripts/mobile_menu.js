(function($) {
  $(function() {
    $('.mobile-menu-block .menu-name-main-menu').prepend('<span class="arrow">');

    $('.region-mobile-nav .mobile-nav').click(function() {
      $('.region-mobile-nav .mobile-menu-block').toggle();
    });

    $('*').bind('click', function() {
      if (mobile && $(this).closest('.region-mobile-nav').length == 0) {
        if ($('.region-mobile-nav .mobile-menu-block:visible').length > 0) {
          $('.region-mobile-nav .mobile-menu-block').hide();
        }
      }
    });

    $('.mobile-nav').click(function(event){
      event.stopPropagation();
    });

    // Media Queries
    MQ.addQuery({
      context: ['smartphone', 'smartphone_landscape'],
      call_in_each_context: false,
      callback: function() {
        mobile = true;
        $('.region-mobile-nav .mobile-menu-block').hide();
      }
    });

    MQ.addQuery({
      context: ['tablet_portrait', 'tablet_landscape', 'standard'],
      call_in_each_context: false,
      callback: function() {
        mobile = false;
      }
    });
  });
})(jQuery);