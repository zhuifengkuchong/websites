(function($){
  $(function() {
    // Media Queries
    MQ.addQuery({
      context: ['smartphone', 'smartphone_landscape'],
      call_in_each_context: false,
      callback: function() {
        moveSearchToHeader();
      }
    });

    MQ.addQuery({
      context: ['tablet_portrait', 'tablet_landscape', 'standard'],
      call_in_each_context: false,
      callback: function() {
        moveSearchToLeaderboard();
        $('.search-block').css('display', '');
      }
    });

    $(".open-main-menu a").click(function(event) {
      event.preventDefault();
      $('.search-block').toggle();
    });
  });

  function moveSearchToLeaderboard() {
    $('.region-leaderboard .region-inner').append($('.search-block'));
  }

  function moveSearchToHeader() {
    if ($('.search-page').length == 0) {
      $('.region-header .region-inner .main-menu').before($('.search-block'));
    }
  }
})(jQuery);
