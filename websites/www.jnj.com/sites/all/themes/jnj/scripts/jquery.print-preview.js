/*!
 * jQuery Print Previw Plugin v1.0.1
 *
 * Copyright 2011, Tim Connell
 * Licensed under the GPL Version 2 license
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Date: Wed Jan 25 00:00:00 2012 -000
 */
(function($) {

	// Initialization
	$.fn.printPreview = function(options) {
		this.each(function() {
			$(this).bind('click', function(e) {
			    e.preventDefault();
			    if (!$('#print-modal').length) {
			        $.printPreview.loadPrintPreview(options);
			    }
			});
		});
		return this;
	};

    // Private functions
    var mask, size, print_modal, print_controls, options, printFrameName, times = 0;
    $.printPreview = {
        loadPrintPreview: function(opt) {
            printFrameName = 'print-frame-' + times;
            options = opt || {};

            // Declare DOM objects
            print_modal = $('<div id="print-modal"></div>');
            print_controls = $('<div id="print-modal-controls">' +
                                    '<a href="#" class="print" title="Print page">Print page</a>' +
                                    '<a href="#" class="close" title="Close print preview">Close</a>').hide();
            var print_frame = $('<iframe id="print-modal-content" scrolling="no" border="0" frameborder="0" name="' + printFrameName + '" />');

            // Raise print preview window from the dead, zooooooombies
            print_modal
                .hide()
                .append(print_controls)
                .append(print_frame)
                .appendTo('body');

            // The frame lives
            var print_frame_ref = window.frames[printFrameName].document;

            // Include Scripts
            var scripts = '';
            if (typeof options.addScripts !== 'undefined') {
                $.each(options.addScripts, function() {
                    scripts += '<script src="' + this + '"></script>';
                })
            }

            print_frame_ref.open();
            print_frame_ref.write('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">' +
                '<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">' +
                '<head>' +  scripts + '<title>' + document.title + '</title></head>' +
                '<body class="' + $('body').attr('class') + '"></body>' +
                '</html>');
            print_frame_ref.close();

            // Grab contents and apply stylesheet
            var $iframe_head = $([]),
                $iframe_body = $('body > *:not(#print-modal):not(script)').clone();
            if (typeof options.cloneStyles === 'undefined' || options.cloneStyles) {
                $iframe_head = $('head *[media*=print], head *[media=all]').clone();
            }


            // Extend CSS
            if (typeof options.extendedCss !== 'undefined') {
                $.each(options.extendedCss, function() {
                    $iframe_head = $iframe_head.add($('<link type="text/css" rel="stylesheet" href="' + this + '" >'));
                })
            }

            $.each($iframe_head, function() {
                $(this).attr('media', 'all');
            });

            //First it was set a setTimeout just for Internet Explorer, but after we include some scripts, we needed to put the timeout for every browser.
//            if ($.browser.msie) {
                setTimeout(function() {
                    $.printPreview.insertModalIframeContent($iframe_head, $iframe_body, print_frame, print_frame_ref);
                }, 200);
//            }
//            else {
//                $.printPreview.insertModalIframeContent($iframe_head, $iframe_body, print_frame, print_frame_ref);
//            }
        },

        insertModalIframeContent: function($iframe_head, $iframe_body, print_frame, print_frame_ref) {
            // Insert contents into iframe
            $iframe_head.each(function() {
                $('head', print_frame_ref).append(this.outerHTML || $(this));
            });

            var clonedBody = $iframe_body.clone();
            // Perform some change before appending the body into the iframe
            if (typeof options.onBeforeCreate === 'function') {
                options.onBeforeCreate(clonedBody);
            }

            clonedBody.each(function() {
                $('body', print_frame_ref).append(this.outerHTML || $(this));
            });

            // Disable all links
            $('a', print_frame_ref).bind('click.printPreview', function(e) {
                e.preventDefault();
            });

            // Introduce print styles
            $('head').append('<style type="text/css">' +
                '@media print {' +
                    '/* -- Print Preview --*/' +
                    '#print-modal-mask,' +
                    '#print-modal {' +
                        'display: none !important;' +
                    '}' +
                '}' +
                '</style>'
            );

            // Load mask
            $.printPreview.loadMask();

            // Disable scrolling
            $('body').css({overflowY: 'hidden', height: '100%'});
            $('img', print_frame_ref).load(function() {
                print_frame.height($('body', print_frame.contents())[0].scrollHeight);
            });

            // Perform some change inside iframe
            if (typeof options.onLoad === 'function') {
                options.onLoad(print_frame_ref);
            }

            // Position modal
            starting_position = $(window).height() + $(window).scrollTop();
            var css = {
                    top:         starting_position,
                    height:      '100%',
                    overflowY:   'auto',
                    zIndex:      10000,
                    display:     'block'
                }
            print_modal
                .css(css)
                .animate({ top: $(window).scrollTop() }, 400, 'linear', function() {
                    print_controls.fadeIn('slow').focus();
                });
            print_frame.height( $('body', print_frame.contents())[0].scrollHeight );

            // Bind closure
            $('a', print_controls).bind('click', function(e) {
                e.preventDefault();
                if ($(this).hasClass('print')) {
                    window.frames[printFrameName].focus();
                    window.frames[printFrameName].print();
                }
                else {
                    $.printPreview.destroyPrintPreview();
                }
            });
        },

        destroyPrintPreview: function() {
            print_controls.fadeOut(100);
            print_modal.animate({ top: $(window).scrollTop() - $(window).height(), opacity: 1}, 400, 'linear', function(){
    	        print_modal.remove();
    	        $('body').css({overflowY: 'auto', height: 'auto'});
                times++;
    	    });
    	    mask.fadeOut('slow', function()  {
    			mask.remove();
    		});

    		$(document).unbind("http://www.jnj.com/sites//all//themes//jnj//scripts//keydown.printPreview.mask");
    		mask.unbind("http://www.jnj.com/sites//all//themes//jnj//scripts//click.printPreview.mask");
    		$(window).unbind("http://www.jnj.com/sites//all//themes//jnj//scripts//resize.printPreview.mask");

            // Perform some action after destroying modal
            if (typeof options.onDestroy === 'function') {
                options.onDestroy();
            }
	    },

    	/* -- Mask Functions --*/
	    loadMask: function() {
	        size = $.printPreview.sizeUpMask();
            mask = $('<div id="print-modal-mask" />').appendTo($('body'));
    	    mask.css({
    			position:           'absolute',
    			top:                0,
    			left:               0,
    			width:              size[0],
    			height:             size[1],
    			display:            'none',
    			opacity:            0,
    			zIndex:             9999,
    			backgroundColor:    '#000'
    		});

    		mask.css({display: 'block'}).fadeTo('400', 0.75);

            $(window).bind("http://www.jnj.com/sites//all//themes//jnj//scripts//resize..printPreview.mask", function() {
				$.printPreview.updateMaskSize();
			});

			mask.bind("http://www.jnj.com/sites//all//themes//jnj//scripts//click.printPreview.mask", function(e)  {
				$.printPreview.destroyPrintPreview();
			});

			$(document).bind("http://www.jnj.com/sites//all//themes//jnj//scripts//keydown.printPreview.mask", function(e) {
			    if (e.keyCode == 27) {  $.printPreview.destroyPrintPreview(); }
			});
        },

        sizeUpMask: function() {
            if ($.browser.msie) {
            	// if there are no scrollbars then use window.height
            	var d = $(document).height(), w = $(window).height();
            	return [
            		window.innerWidth || 						// ie7+
            		document.documentElement.clientWidth || 	// ie6
            		document.body.clientWidth, 					// ie6 quirks mode
            		d - w < 20 ? w : d
            	];
            } else { return [$(document).width(), $(document).height()]; }
        },

        updateMaskSize: function() {
    		var size = $.printPreview.sizeUpMask();
    		mask.css({width: size[0], height: size[1]});
        }
    }
})(jQuery);