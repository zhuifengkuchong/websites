/**
*  Setting variables to script of main menu use.
*/
(function($){
  $(function() {

    if (!Drupal.settings.textonly) {
      fixActiveTrail();
      createShowHideLink();
      configureTooltip();
      mediaQueries();
    }
  });

  function fixActiveTrail() {
    var activeItem = $('.sidebar-menu a.active');
    var fix = !activeItem.hasClass('active-trail');

    if (fix) {
      var elements = activeItem.parents('.sidebar-menu li, .sidebar-menu a');
      elements.addClass('active-trail');
    }
  }

  function createShowHideLink() {
    var menu_bar_label = "";
    if (Drupal.settings.jnj_translate && Drupal.settings.jnj_translate.sidebar_menu) {
      menu_bar_label = Drupal.settings.jnj_translate.sidebar_menu;
    }

    // Add link show-hide
    $('.sidebar-menu ul:first').after('<a href="javascript:void(0);" class="show-hide tablet">' + menu_bar_label + '</a>');

    // Configure sidebar behavior
    $('.sidebar-menu a.show-hide').click(function() {
      if ($('.sidebar-menu ul.menu:first').css('margin-left') == '0px') {
        closeSidebarMenu();
      }
      else {
        openSidebarMenu();
      }
    });
  }

  function configureTooltip() {
    // Add class for submenu tooltip
    $('.sidebar-menu-tooltip ul:first > li').each(function(){
      if ($('> ul',$(this)).length > 0) {
        $(this).addClass('has-submenu');
      }
    });

    // Configure Tooltip
    $('.sidebar-menu-tooltip ul ul').hide();

    $('.sidebar-menu-tooltip ul li').mouseover(function(){
      showSidebarToolTip($(this));
    });

    $('.sidebar-menu-tooltip ul li').mouseout(function(){
      hideSidebarToolTip($(this));
    });
  }

  function mediaQueries() {
    // Media Queries
    MQ.addQuery({
      context: ['tablet_portrait', 'tablet_landscape'],
      call_in_each_context: false,
      callback: function() {
        // reset sidebar menu
        openSidebarMenu();
      }
    });

    MQ.addQuery({
      context: ['standard'],
      call_in_each_context: false,
      callback: function() {
        // reset sidebar menu
        openSidebarMenu();
        // reset sidebar menu tooltip
        hideAllSidebarToolTip();
      }
    });
  }

  function openSidebarMenu() {
    $('.sidebar-menu a.show-hide').removeClass('open');
    $('.sidebar-menu ul.menu:first').css('margin-left', '');
  }

  function closeSidebarMenu() {
    $('.sidebar-menu a.show-hide').addClass('open');
    $('.sidebar-menu ul.menu:first').css('margin-left', $('.sidebar-menu ul.menu:first').width() + "px");
  }

  function showSidebarToolTip(sidebarItem) {
    sidebarItem.find(' > ul').addClass('desktop');
  }

  function hideSidebarToolTip(sidebarItem) {
    sidebarItem.find(' > ul').removeClass('desktop');
  }

  function hideAllSidebarToolTip() {
    $('.sidebar-menu-tooltip ul ul').hide();
  }
})(jQuery);