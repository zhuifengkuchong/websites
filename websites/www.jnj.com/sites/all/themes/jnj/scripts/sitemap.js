(function($){
  $(function() {
    $('.sitemap-block ul li a').each(function (){
      if ($(this).hasClass('sitemap-hidden-item')) {
        var elementParent = $(this).parent();
        $(this).remove();
        if (elementParent.children().size() <= 0) {
          elementParent.remove();
        }
      }else if ($(this).hasClass('sitemap-hidden-group')) {
        $(this).parent().remove();
      }else if ($(this).hasClass('sitemap-hidden-child')) {
        $(this).next('ul').remove();
      }
      if ($(this).hasClass('mobile')) {
        $(this).closest('li').addClass('mobile');
      }
    });
    if ($('body').hasClass('responsibility-sitemap')) {
      $('.sitemap-block ul li.menu-depth-4').each(function (){
        $('> ul', this).makeacolumnlists({cols:3,equalHeight:false,startN:1});
      });
    }else {
      $('.sitemap-block ul li.menu-depth-2').each(function (){
        $('> ul', this).makeacolumnlists({cols:3,equalHeight:false,startN:1});
      });
    }
  });

  $.fn.makeacolumnlists = function(settings){
          settings = $.extend({
                  cols: 2,				// set number of columns
                  equalHeight: false, 	// can be false, 'ul', 'ol', 'li'
                  startN: 1				// first number on your ordered list
          }, settings);

          if($('> li', this)) {
                  this.each(function(y) {
                          var y= $('.li_container').size(),
                          height = 0, 
                          maxHeight = 0,
                                  t = $(this),
                                  classN = t.attr('class'),
                                  listsize = $('> li', this).size(),
                                  percol = Math.ceil(listsize/settings.cols),
                                  contW = t.width(),
                                  bl = ( isNaN(parseInt(t.css('borderLeftWidth'),10)) ? 0 : parseInt(t.css('borderLeftWidth'),10) ),
                                  br = ( isNaN(parseInt(t.css('borderRightWidth'),10)) ? 0 : parseInt(t.css('borderRightWidth'),10) ),
                                  pl = parseInt(t.css('paddingLeft'),10),
                                  pr = parseInt(t.css('paddingRight'),10),
                                  ml = parseInt(t.css('marginLeft'),10),
                                  mr = parseInt(t.css('marginRight'),10)
                          var colnum=1,
                                  percol2=percol;
                          $(this).addClass('li_cont1').wrap('<div id="li_container' + (++y) + '" class="li_container"></div>');
                          for (var i=0; i<=listsize; i++) {
                                  if(i>=percol2) { percol2+=percol; colnum++; }
                                  var eq = $('> li:eq('+i+')',this);
                                  eq.addClass('li_col'+ colnum);
                                  if($(this).is('ol')){eq.attr('value', ''+(i+settings.startN))+'';}
                          }
                          for (colnum=2; colnum<=settings.cols; colnum++) {
                                  if($(this).is('ol')) {
                                          $('li.li_col'+ colnum, this).appendTo('#li_container' + y).wrapAll('<ol class="li_cont'+colnum +' ' + classN + '" ></ol>');
                                  } else {
                                          $('li.li_col'+ colnum, this).appendTo('#li_container' + y).wrapAll('<ul class="li_cont'+colnum +' ' + classN + '" ></ul>');
                                  }
                          }
                          if (settings.equalHeight=='li') {
                                  for (colnum=1; colnum<=settings.cols; colnum++) {
                                      $('#li_container'+ y +' li').each(function() {
                                          var e = $(this);
                                          var border_top = ( isNaN(parseInt(e.css('borderTopWidth'),10)) ? 0 : parseInt(e.css('borderTopWidth'),10) );
                                          var border_bottom = ( isNaN(parseInt(e.css('borderBottomWidth'),10)) ? 0 : parseInt(e.css('borderBottomWidth'),10) );
                                          height = e.height() + parseInt(e.css('paddingTop'), 10) + parseInt(e.css('paddingBottom'), 10) + border_top + border_bottom;
                                          maxHeight = (height > maxHeight) ? height : maxHeight;
                                      });
                                  }
                                  for (colnum=1; colnum<=settings.cols; colnum++) {
                                          var eh = $('#li_container'+ y +' li');
                                  var border_top = ( isNaN(parseInt(eh.css('borderTopWidth'),10)) ? 0 : parseInt(eh.css('borderTopWidth'),10) );
                                  var border_bottom = ( isNaN(parseInt(eh.css('borderBottomWidth'),10)) ? 0 : parseInt(eh.css('borderBottomWidth'),10) );
                                          mh = maxHeight - (parseInt(eh.css('paddingTop'), 10) + parseInt(eh.css('paddingBottom'), 10) + border_top + border_bottom );
                                  eh.height(mh);
                                  }
                          } else 
                          if (settings.equalHeight=='ul' || settings.equalHeight=='ol') {
                                  for (colnum=1; colnum<=settings.cols; colnum++) {
                                      $('#li_container'+ y +' .li_cont'+colnum).each(function() {
                                          var e = $(this);
                                          var border_top = ( isNaN(parseInt(e.css('borderTopWidth'),10)) ? 0 : parseInt(e.css('borderTopWidth'),10) );
                                          var border_bottom = ( isNaN(parseInt(e.css('borderBottomWidth'),10)) ? 0 : parseInt(e.css('borderBottomWidth'),10) );
                                          height = e.height() + parseInt(e.css('paddingTop'), 10) + parseInt(e.css('paddingBottom'), 10) + border_top + border_bottom;
                                          maxHeight = (height > maxHeight) ? height : maxHeight;
                                      });
                                  }
                                  for (colnum=1; colnum<=settings.cols; colnum++) {
                                    var eh = $('#li_container'+ y +' .li_cont'+colnum);
                                    var border_top = ( isNaN(parseInt(eh.css('borderTopWidth'),10)) ? 0 : parseInt(eh.css('borderTopWidth'),10) );
                                    var border_bottom = ( isNaN(parseInt(eh.css('borderBottomWidth'),10)) ? 0 : parseInt(eh.css('borderBottomWidth'),10) );
                                            mh = maxHeight - (parseInt(eh.css('paddingTop'), 10) + parseInt(eh.css('paddingBottom'), 10) + border_top + border_bottom );
                                    eh.height(mh);
                                  }
                          }
                      $('#li_container' + y).append('<div style="clear:both; overflow:hidden; height:0px;"></div>');
                  });
          }
  }

})(jQuery);