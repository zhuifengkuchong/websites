(function($) {

  $(function() {
    if (typeof Drupal.settings.jnj === 'undefined') {
      return;
    }

    var originPath    = window.location.protocol + '//' + window.location.host + Drupal.settings.basePath,
        fullJnJThemePath = originPath + Drupal.settings.jnj.jnjThemePath,
        fullThemePath = originPath + Drupal.settings.jnj.themePath;

    var addScripts = [
          fullJnJThemePath + '/scripts/modernizr.js',
          fullJnJThemePath + '/scripts/browser_detect.js'
        ],
        addStyles  = [
          fullThemePath + '/css/tout.css',
          fullThemePath + '/css/global.base.css',
          fullThemePath + '/css/content.css',
          fullJnJThemePath + '/css/jnj.global.styles.css',
          fullThemePath + '/css/global.styles.css',
          fullJnJThemePath + '/css/print-friendly.css'
        ];

    if (typeof Drupal.settings.jnj.printerFriendlyStyles !== 'undefined') {
      $.each(Drupal.settings.jnj.printerFriendlyStyles, function() {
        addStyles.push(originPath + this);
      });
    }

    $('a.printer').not('.disabled').printPreview({
      cloneStyles: false,
      addScripts: addScripts,
      extendedCss : addStyles,
      onBeforeCreate: function(body) {
        // remove map scripts from body before appending it
        $(body).find('#map_canvas').parent().find('script').remove();
      },
      onLoad: function(print_frame_ref) {
        var columnsHeight;

        //Just for devices we hide all the page and set the overlay fits in the entire page.
        if (Modernizr.touch || $.inArray(MQ.context, ['smartphone_portrait', 'smartphone_landscape']) != -1){
          columnsHeight = $('#page', print_frame_ref).height();
          $("div#page").hide();
          $("body").addClass("printer-friendly");
        }

        $(print_frame_ref).find('#block-views-header-and-footer-logo img').attr({
          src: [originPath, Drupal.settings.jnj.themePath, 'Unknown_83_filename'/*tpa=http://www.jnj.com/images/jnj-logo.jpg*/].join('/')
        });

        if ($.browser.msie && $.browser.version <= 8 && typeof window.html5 !== 'undefined') {
          window.html5.shivPrint(print_frame_ref);
        }

        // Fix Modal size
        // give time to all browsers be able to properly calculate the height (firefox issue)
        setTimeout(function() {
          columnsHeight = columnsHeight ? columnsHeight : $('#page', print_frame_ref).height();
          $('#print-modal-content').height(columnsHeight);
        }, 500);

        //Disable Form Itens
        $(print_frame_ref).find('input, textarea, select').attr('disabled','disabled');
      },
      onDestroy: function() {
        //Just for devices we hide all the page and set the overlay fits in the entire page.
        $("div#page").show();
        $("body").removeClass("printer-friendly");
        $(window).trigger('resize');
      }
    });

  });

})(jQuery);



