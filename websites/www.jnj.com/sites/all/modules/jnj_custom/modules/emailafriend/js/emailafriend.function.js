(function($) {
    $(document).ready(function() {
        //Change toolbox menu link if 'mailto' option is chosen
        if (Drupal.settings.emailafriend.emailafriend_type == "0") {
            jQuery('a#emailafriend_menu').attr('href', 'mailto:?subject=' + Drupal.settings.emailafriend.emailafriend_email_subject + '&body=' + document.URL);
        }
    });
})(jQuery);

function openEmailAFriendForm(email_page, shared_page, method) {
    var queryString = '?qp=' + shared_page;
    var windowOptions = 'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=420,height=495';

    if (method == "new") {
        queryString += '&method=new';
        window.open(email_page + queryString, 'page', windowOptions);
    }
    else {
        queryString += '&method=redirect';
        window.location.href = email_page + queryString;
    }
}

function emailafriend_addthis() {
    //Set corresponding E-mail to a friend link inside AddThis box, depending on the option chosen on the administration page
    if (Drupal.settings.emailafriend.emailafriend_type == "0") {
        jQuery('div#at_hover').prepend('<a href="mailto:?subject=' + Drupal.settings.emailafriend.emailafriend_email_subject + '&body=' + document.URL + '" class="email at_item at_col0"><span class="mail-icon">'
                + Drupal.t(Drupal.settings.emailafriend.emailafriend_link_title)
                + '</span></a>');
    } else {
        jQuery('div#at_hover').prepend('<a href="http://www.jnj.com/emailafriend" class="email at_item at_col0"><span class="mail-icon">'
                + Drupal.t(Drupal.settings.emailafriend.emailafriend_link_title)
                + '</span></a>');
        jQuery('div#at_hover a.email').click(function(event) {
            var email_page = jQuery(this).attr('href');
            event.preventDefault();

            if (jQuery.inArray(MQ.context, ['smartphone_portrait', 'smartphone_landscape']) != -1) {
                openEmailAFriendForm(email_page, document.location.href, "redirect");
            }
            else {
                openEmailAFriendForm(email_page, document.location.href, "new");
            }
        });
    }
}