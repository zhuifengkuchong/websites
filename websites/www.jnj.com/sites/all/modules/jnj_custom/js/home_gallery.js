(function($) {

  var Gallery = {};
  var firstTime = true;
  var startAt = 0;

  $(function() {

    //Remove toolbox functionality
    $('.toolbox-block li a').not('.share, .read-speaker').addClass('disabled');
    $('.toolbox-block li a').not('.share, .read-speaker').click(function(event){
      event.preventDefault();
      event.stopPropagation();
    });

    // Media Queries
    MQ.addQuery({
      context: ['smartphone', 'smartphone_landscape'],
      call_in_each_context: false,
      callback: function() {
        destroyFlexslider('#slider');
        createHomeGallery('mobile');
        firstTime = false;
      }
    });

    MQ.addQuery({
      context: ['tablet_portrait', 'tablet_landscape', 'standard'],
      call_in_each_context: false,
      callback: function() {
        destroyFlexslider('#slider');
        $('#control-buttons').remove();
        createHomeGallery('desktop');
        firstTime = false;
      }
    });
    addHomeGATrackEvents();
  });

  function createHomeGallery(context) {
    initGallery();
    insertImage(context);
    applyBehaviors(context);
  }

  function applyBehaviors(context) {
    var slider = $('#slider');

    if (context == 'mobile') {
      slider.flexslider({
        animation: "slide",
        controlNav: false,
        slideshow: false,
        touch: true,
        animationLoop: true,
        startAt: startAt,
        after : function () {
          changeActive();
        }
      });

      var sliderData = slider.data('flexslider');
      var list = $('<ol></ol>').attr({
        'class' : 'control-nav control-paging mobile',
        'id'    : 'control-buttons'
      });

      var listItems = '';
      for (var i = 0 ; i < sliderData.slides.length; i++) {
        if (i == startAt) {
          listItems += '<li class="active"><a>'+ parseInt(i + 1) +'</a></li>';
        } else {
          listItems += '<li><a>'+ parseInt(i + 1) +'</a></li>';
        }
      }

      list.append(listItems).find('li').click(function(e) {
        e.preventDefault();
        e.stopPropagation();
        slider.flexslider($(this).index());
      });

      slider.find('.flex-direction-nav').remove();
      slider.append(list);
    }
    else {
      slider.flexslider({
        animation: "fade",
        animationLoop: true,
        pauseOnAction: false,
        pauseOnHover: true,
        slideshow: true,
        controlNav: true,
        manualControls: '.thumbnails ul li',
        startAt: startAt,
        start: function(slider) {
          triggerControlsOnHover();
          forceNavControlsDefaultBehavior();

          // Store current item index where mouse is over with a class
          Gallery.thumbsList.find('li').hover(function() {
            $(this).addClass('mouseover');
          }, function() {
            $(this).removeClass('mouseover');
          });
        },
        after: function(slider) {
          // Save current slide for reconstruct slider after readspeaker destroy flexslider
          startAt = $('#slider').data('flexslider').currentSlide;

          // FlexSlider prevents any new animation if the slider is already animating
          // To prevent the active slider from being different from where the mouse is over
          // Recheck which item the mouse is currently on and trigger a change if necessary
          // after the animation is done
          // This verification is applied only to devices without touch support
          if (!Modernizr.touch) {
            var  currentActiveItem = Gallery.thumbsList.find('li.flex-active').index(),
                currentMouseOverItemIndex = Gallery.thumbsList.find('li.mouseover').index();

            if (currentMouseOverItemIndex != -1 && currentActiveItem != currentMouseOverItemIndex) {
              $('#slider').flexslider(currentMouseOverItemIndex);
            }
          }
        }
      });
    }

    Gallery.slider = slider.data('flexslider');
  }

  function changeActive() {
    var wrapper = $('.view-home-gallery');
    var sliderData = $('#slider').data('flexslider');
    var list = wrapper.find('ol li');
    list.removeClass('active');
    list.eq(sliderData.currentSlide).addClass('active');
  }

  // FlexSlider prevents the default behavior of the controlNav
  // So the clicks on the links doesn't work
  // Force the behavior again
  function forceNavControlsDefaultBehavior() {
    Gallery.thumbsList.find('a').bind('touchend click', function() {
      if ($(this).parents('li.flex-active').length != 0) {
        window.location.href = $(this).attr('href');
      }
    });
  }

  function triggerControlsOnHover() {
    Gallery.thumbsList.find('li').mouseover(function() {
      $('#slider').flexslider($(this).index());
    });
  }

  function initGallery() {
    var wrapper = $('#block-views-home-gallery-block div.view-home-gallery');

    Gallery = {
      wrapper    : wrapper
    };

    Gallery.mainList   = Gallery.wrapper.find('div.view-content > .item-list');
    Gallery.thumbsList = Gallery.wrapper.find('.thumbnails');

    if (firstTime) {
      structureGallery();
      firstTime = false;
    }

    Gallery.mainList.attr('id', 'slider');
    Gallery.mainList.find('ul:first').addClass('slides');

  }

  function structureGallery() {

    var featuredStoriesText = Drupal.t('Featured Stories');
    if (typeof Drupal.settings.jnj.translations.gallery_title != 'undefined') {
      featuredStoriesText = Drupal.settings.jnj.translations.gallery_title;
    }
    var featuredStoriesTitleElement = $('<h3>' + featuredStoriesText + '</h3>');

    Gallery.thumbsList.prepend(featuredStoriesTitleElement);
  }

  function insertImage(context) {
    if ($('.view-home-gallery .image-home-gallery-' + context).length == 0) {
      $.each(Drupal.settings.jnj.home_gallery, function(index, value){
        var img = $('<img />');
        img.addClass('image-home-gallery');
        img.addClass('image-home-gallery-' + context);

        if (context == 'desktop' || !value.mobileImage) {
          img.addClass('no-mobile');
          img.attr('src', value.desktopImage);
          $('.view-home-gallery #slider li:eq(' + index + ') .views-field-title').parent(':first').before(img);
        }
        else {
          img.addClass('mobile');
          img.attr('src', value.mobileImage);
          var wrap = $('<div class="wrap-image-gallery"></div>');
          wrap.append(img);
          $('.view-home-gallery #slider li:eq(' + index + ') .views-field-title :first').after(wrap);
        }
      });
    }
  }
  function addHomeGATrackEvents () {
    var count = 1;
    
    //Click events for Title + Learn more links (desktop, tablet and mobile)
    $("div.view-home-gallery.view-display-id-block ul.slides li.views-row").each(function () {
      var matches = ((" " + $(this).attr('class') + " ").match(/\sviews-row-(\d+)\s/))        
      if (matches) {
        $(this).find('a').click(function () {
          GATrackWithURLAsLabel($(this), 'click-story' + matches[1]);
        });
      }
    });
    
    //Click events for image thumbnails (desktop/tablet only)
    $("div.view-home-gallery.view-display-id-thumbnails ul.slides li").each(function () {
      var number = count;
      $(this).click(function(e) {
          GATrackWithURLAsLabel($(this).find('a'), 'click-story' + number);
      });
      count++;
    });
  }

  function GATrackWithURLAsLabel(href_element, action) {
    var lastString = '';
    var arrURL = $(href_element).attr('href');
    if (arrURL.charAt(0) === '/') {
      lastString = arrURL;
    }
    else {
      if (arrURL.indexOf('http://www.jnj.com/sites//all//modules//jnj_custom//js//jnj.com') > -1) {
        arrURL = arrURL.replace('../../../../../index.htm'/*tpa=http://www.jnj.com/*/);
      }
      if (arrURL.indexOf('http://www.jnj.com/sites//all//modules//jnj_custom//js//acquia-sites.com') > -1) {
        arrURL = arrURL.replace('http://jandjprodstg.prod.acquia-sites.com/');
      }
      lastString = arrURL;
    }

    _gaq.push(['_trackEvent', 'homepage-articles', action, lastString]);
  }
})(jQuery);