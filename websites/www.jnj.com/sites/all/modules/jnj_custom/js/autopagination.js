/*
 * Control load more
 *
 */
(function($) {

  $(function() {
  	changeImageLink();

    Drupal.behaviors.removeRowBottonLine = {
      attach: function(context, settings) {
        var viewList = context.find('.view-content');
        $('#content .views-row-last:first').removeClass('views-row-last');
        $(viewList.find('li')).appendTo(viewList.find('.item-list ul'));
	  		changeImageLink();
      }
    }
  });

  function isTextOnly() {
  	return readCookie('text_only') == 1;
  }

  function readCookie(name) {
  	var nameEQ = name + "=";
  	var ca = document.cookie.split(';');

  	for(var i=0;i < ca.length;i++) {
      var c = ca[i];

      while (c.charAt(0)==' ')
      	c = c.substring(1,c.length);

      if (c.indexOf(nameEQ) == 0)
      	return c.substring(nameEQ.length,c.length);
  	}
  	return null;
	}

	function changeImageLink() {
		$('.pager-next a').click(function () {
			$('div.throbber').addClass('hide-image');
      if (
        Drupal.settings &&
        Drupal.settings.jnj &&
        Drupal.settings.jnj.translations &&
        Drupal.settings.jnj.translations.load_more &&
        Drupal.settings.jnj.translations.load_more.loading) {
        $('.pager-next a').html(Drupal.settings.jnj.translations.load_more.loading);
      }
		});
	}

})(jQuery);