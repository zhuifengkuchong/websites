(function($){
  $(function(){
    if ($('http://www.jnj.com/sites//all//modules//jnj_custom//js//a.rss').length > 0 && typeof(Drupal.settings.jnj.rss_feed_type) != 'Undefined') {
      rss_type = Drupal.settings.jnj.rss_feed_type;
      href = $('http://www.jnj.com/sites//all//modules//jnj_custom//js//a.rss').attr('href');
      if (href.indexOf('?') < 0) {
        $('http://www.jnj.com/sites//all//modules//jnj_custom//js//a.rss').attr('href', $('http://www.jnj.com/sites//all//modules//jnj_custom//js//a.rss').attr('href') + '?feedUrl=' + rss_type);
      } else {
        if (href.indexOf('feedUrl') < 0) {
          $('http://www.jnj.com/sites//all//modules//jnj_custom//js//a.rss').attr('href', $('http://www.jnj.com/sites//all//modules//jnj_custom//js//a.rss').attr('href') + '&feedUrl=' + rss_type);
        } else {
          regex = /feedUrl=[^&]*/i;
          result = href.replace(regex, 'feedUrl='+rss_type);
          $('http://www.jnj.com/sites//all//modules//jnj_custom//js//a.rss').attr('href', result);
        }
      }
    }
  });
})(jQuery);