jQuery(document).ready(function() {
  if (typeof(MQ) != "undefined") {
    // Media Queries
    MQ.addQuery({
      context: ['smartphone', 'smartphone_landscape'],
      call_in_each_context: false,
      callback: function() {
        if (jQuery('#overlay-background').length != 0) {
          mobileCallOverlayBehavior();
        } else {
          attachOverlayBehaviors();
        }
      }
    });

    MQ.addQuery({
      context: ['tablet_portrait', 'tablet_landscape', 'standard'],
      call_in_each_context: false,
      callback: function() {
        attachOverlayBehaviors();
        mobileCloseOverlayBehavior();
      }
    });
  }
});

function callOverlay(overlayElement, settings) {
  if (overlayElement.length != 0) {
    settings = jQuery.extend({onClose:mobileCloseOverlayBehaviorResize}, settings);
    new jQuery.CustomOverlay(overlayElement, settings);
    if (jQuery.inArray(MQ.context, ['smartphone_portrait', 'smartphone_landscape']) != -1){
      mobileCallOverlayBehavior();
    }
    overlayElement.center();
  }
}

function mobileCallOverlayBehavior() {
  jQuery('#columns').hide();
  jQuery('html, body').scrollTop(0);
}

function mobileCloseOverlayBehavior(){
  jQuery('#columns').css({'display':''});
}

function mobileCloseOverlayBehaviorResize(){
  mobileCloseOverlayBehavior();
  if (jQuery('.flex-viewport .slides').length > 0) {
    //trigger resize event for flexslider after closing the overlay.
    jQuery(window).trigger("resize");
  }
}

function attachOverlayBehaviors() {
  // Put Close Labels on Overlay
  if (Drupal.settings && Drupal.settings.jnj && Drupal.settings.jnj.overlay) {
    if (Drupal.settings.jnj.overlay.close_overlay) {
      jQuery('.overlay-close.no-mobile').html(Drupal.settings.jnj.overlay.close_overlay);
    }
    if (Drupal.settings.jnj.overlay.close_overlay_mobile) {
      jQuery('.overlay-close.mobile').html(Drupal.settings.jnj.overlay.close_overlay_mobile);
    }
  }

  jQuery(".overlay-content").click(function(event) {
    event.stopPropagation();
  });

  jQuery('#overlay-background, .overlay-close').live('click', function() {
    mobileCloseOverlayBehavior();
  });
}