var resp = '';
(function($){
  $(function() {
     var maxpg = Math.ceil(Drupal.settings.jnj.total_results / Drupal.settings.jnj.results_perpage);

      $('.google-appliance-results').infinitescroll({
        navSelector  : "ul.pager",
        nextSelector : "ul.pager>li.pager-next>a",
        itemSelector : "ol.google-appliance-results>li",
        maxPage      : maxpg,
        path         : function (num) {
          var nextpage = $('ul.pager>li.pager-next>a:last').attr('href');
          nextpage = nextpage.replace('page=1','page='+num);
          return nextpage;
        },
        state : {
            currPage : 0
        },
        loading : {
            finished: undefined,
            finishedMsg: null,
            img: "",
            msgText: '<ul class="loading"><li>'+ Drupal.settings.jnj_translate.loading + '</li></ul>',
            selector: null,
            speed: 'fast',
            start: undefined
        }
      },
      function (newElements) {
        $('#parcial_total').text($('ol.google-appliance-results>li').length);

        if($('ol.google-appliance-results>li').length < Drupal.settings.jnj.total_results) {
          $('.google-appliance-results:last').after(addLoadMoreButton());
        }
      }); 

      $('.google-appliance-results').infinitescroll('pause');
      $('#block-jnj-custom-jnj-custom-gsa-search ul.pager').hide();

      if($('ol.google-appliance-results>li').length < Drupal.settings.jnj.total_results) {
        $('.google-appliance-results:last').after(addLoadMoreButton());
      }

    // Combobox filter
    cbFilterRedirect('select#collections.custom_filter_combos');
  });

  function addLoadMoreButton() {
    return createButton(Drupal.settings.jnj_translate.load_more,true);
  }

  function createButton(title, hasLink) {
    var di = $('<div></div>').attr({'class' : 'item-list'});
    var ul = $('<ul></ul>').attr({
      'class' : 'pager'
    });
    
    var li = $('<li></li>').attr({
      'class': 'pager-next'
    });

    var link = $('<a>' + title + '</a>').attr({'href' : 'javascript:void(0)'});
    if(hasLink) {
      li.click(function () {
        $('.google-appliance-results').infinitescroll('retrieve');
      });
    }

    li.append(link);
    ul.append(li);
    di.append(ul);
    return di;
  }

  // Redirect combobox clicks (mobile)
  function cbFilterRedirect(selector) {
    $(selector).change(function () {
      window.location.href = window.location.origin + '/' + $(selector).val();
      console.log(window.location.origin + '/' + $(selector).val());
    });
  }

})(jQuery);