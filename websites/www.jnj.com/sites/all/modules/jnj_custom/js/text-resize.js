(function($){
  $(function() {
    $('a.text-resize').not('.disabled').click(function(event){
      event.preventDefault();
      target = $('div#columns');
      if (target.hasClass('x-large')) {
        target.removeClass('x-large');
      } else if (target.hasClass('large')) {
        target.addClass('x-large');
        target.removeClass('large');
      } else {
        target.addClass('large');
      }
      return false;
    });
    
    //Add tracking events to utility buttons
    addUtilGATrackEvents();
    addPDFGATrackEvents();
  });
  
  function addUtilGATrackEvents() {
    //Listen button
    $('div#readspeaker_button1 a').click(function(e) {
      utilityBtnTrack('listen');
    });
    
    //Text resize
    $('a.text-resize').click(function(e) {
      utilityBtnTrack('text-size');
    });
    
    //E-mail to a friend
    $('.toolbox-block a.email').click(function(e) {
      utilityBtnTrack('email-to-friend');
    });
    
    //Printer-friendly version
    $('.toolbox-block a.printer').click(function(e) {
      utilityBtnTrack('printer-friendly');
    });
    
    //RSS
    $('.toolbox-block a.rss').click(function(e) {
      utilityBtnTrack('RSS');
    });
    
    //Share
    $('.toolbox-block a.share-btn').click(function(e) {
      utilityBtnTrack('Share');
    });
  }
  
  function addPDFGATrackEvents() {
    $('a').click(function(e) {
      var isDownload = new RegExp("\\.(pdf)$", "i");
      if (isDownload.test(this.href)) {
        pdfDownloadTrack(this); 
      }
    });
  }
  
  function utilityBtnTrack(label) {
    _gaq.push(['_trackEvent', 'utilities', 'click', label]);
  }
  
  function pdfDownloadTrack(element) {
    var currentURI = window.location.href.toString().split(window.location.host)[1];
    var locationName = handleCustomSectionName(currentURI.split('/'));
    
    if (element.href.toString().indexOf('http://www.jnj.com/sites//all//modules//jnj_custom//js//jnj.com') > -1 ||
          element.href.toString().indexOf('http://www.jnj.com/sites//all//modules//jnj_custom//js//acquia-sites.com') > -1) {
      var pdfHref = element.href.toString().split('/');
      var pdfName = pdfHref[pdfHref.length - 1];
    }
    else {
      var pdfName = element.href.toString();
    }

    _gaq.push(["_trackEvent", "Downloads", locationName, pdfName]);
  }
  
  function handleCustomSectionName(sections) {
    var locationName;
    switch (sections[1]) {
      case 'healthcare-products':
        locationName = 'our-products';
        break;
      case 'caring':
        locationName = 'our-caring';
        break;
      case 'our-giving':
        locationName = 'our-caring/our-giving';
        break;
      case 'about-jnj':
        locationName = 'our-company';
        break;
      default:
        locationName = sections[1];
        break;
    }
    
    if (sections[2] != null && sections[1] != 'our-giving') {
      locationName = locationName + '/' + sections[2];
    }
    
    return locationName;
  }
})(jQuery);