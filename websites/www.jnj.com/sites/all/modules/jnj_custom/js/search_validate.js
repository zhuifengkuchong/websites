(function($){
  $(function() {
    // check for blank/spaces before search submit
    $('.search-google-appliance-search-form,#google-appliance-block-form,#block-jnj-custom-jnj-custom-gs-desktop-top').submit(function (e) {
      var resp = $(e,'input[id^="edit-search-keys"]');
      var key = $(resp[0].currentTarget).find('input[id^="edit-search-keys"]').val();
      key = $.trim(key);

      var msg = Drupal.settings.jnj_translate.empty_keyword_message.split('\\n');

      // force \n on message to break line on alert
      if( typeof msg === 'string' ) {
        msg = [ msg ];
      }

      var newstring = "";
      for (i=0; i< msg.length; i++) {
        if (i>0) {
          newstring += '\n';
        };
        newstring += msg[i];
      }

      if (key == '') {
        alert(newstring);
        return false;
      };
    });
  });
})(jQuery);