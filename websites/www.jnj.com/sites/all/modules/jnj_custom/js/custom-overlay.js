var overlay;

(function($){

  $.fn.center = function (action) {
    if ($.inArray(MQ.context, ['smartphone_portrait', 'smartphone_landscape']) != -1) {
      return false;
    }

    var spacing = 0;

    $(this).animate({
      top: (($(window).height() - this.outerHeight()) / 2) + "px"
    },300,function(){
    });

    return this;
  }

  $.CustomOverlays = [];


  $.CustomOverlay = function(co,options){
    this.overlay = co;
    this.overlayBackground = $('#overlay-background');
    this.index = $.CustomOverlays.length;
    $.CustomOverlays.push(this);

    this.settings = $.extend({
      confirmCallback : false,
      cancelCallback : false
    },options);

    this.scrollDelay = 200;
    this.interval = false;
    this.counter = 0;

    this.lastWindowWidth = -1;

    this.resizeTimer = null;
    overlay = this;

    this.createOverlay();

  }

  $.CustomOverlay.prototype = {
    createOverlay : function() {
      //if (page.isTablet) this.scrollDelay = 0;
//      if (IsTablets()) this.scrollDelay = 0;
      if (this.overlayBackground.length == 0) {
        this.overlayBackground = $('<div id="overlay-background">&nbsp;</div>');
        this.overlayBackground.append($('<div id="overlay-div"></div>'));
        this.overlayBackground.click(overlay.close);
//        this.overlayBackground.css({'height':$('body').height()+'px'});
         //$('#page').before(this.overlayBackground);
        $('#footer').before(this.overlayBackground);
        // /*
        // if ($.browser.msie && parseInt($.browser.version) <= 7) {
          // //this.overlay.before(this.overlayBackground);

          // this.overlay.insertBefore($('#page'));
        // } else {
          // $('#main').append(this.overlayBackground);
        // }
        // */
      }
      $this = this;

      this.overlay.find('.overlay-confirm').click(function(e){
        overlay.confirm();
        e.preventDefault();
        return false;
      });
      this.overlay.find('.overlay-cancel').click(function(e){
        overlay.cancel();
        e.preventDefault();
        return false;
      });
      this.overlay.find('.overlay-close').click(function(e){
        overlay.close();
        e.preventDefault();
        return false;
      });
      this.open();

      this.overlay.resize(function() {
        overlay.center();
      });

      $(window).resize(function(){
        overlay.doResize();
      });


      overlay.center();

      if (typeof(this.settings.onCreate) == 'function') {
        this.settings.onCreate();
      }

    },
    doScroll : function() {
      clearTimeout(this.resizeTimer);
      this.resizeTimer = setTimeout(function(){
        overlay.scrollComplete()
      }, overlay.scrollDelay);
    },
    doResize : function() {
      clearTimeout(this.resizeTimer);
      this.resizeTimer = setTimeout(function(){
        overlay.resizeComplete();
      }, overlay.scrollDelay);
    },
    resizeComplete : function() {
      if (this.lastWindowWidth != $('body').width()) {
        this.lastWindowWidth = $('body').width();
        overlay.center('update');
      }
      else {
        overlay.center();
      }
    },
    scrollComplete : function() {
      overlay.center();
    },
    center : function(action) {
      overlay.overlay.center(action);
    },
    open : function() {
      this.lastWindowWidth = -1;

      // $('body').addClass('overlay-state');
      var placeHolder = $('<div id="overlay-placeholder"></div>');
      placeHolder.insertBefore(this.overlay);

      //$('#page').append(this.overlay);

      //Preload Images
      $('img', this.overlay).each(function(){
        var preloadImg = $(this).attr('src');
        $("<img>").attr("src", preloadImg);
      });

      this.overlay.appendTo(this.overlayBackground);
      this.overlay.addClass('active');
      this.overlayBackground.addClass('active');
      this.overlay.hide();
      this.overlay.fadeIn(600);
      if (typeof(overlay.settings.onOpen) == 'function') overlay.settings.onOpen();
    },
    confirm : function() {
      this.lastWindowWidth = -1;

      if (typeof overlay.settings.confirmCallback == 'function'){
        overlay.settings.confirmCallback();
        overlay.close();
        return false;
      }
      overlay.close();
    },
    cancel : function() {
      this.lastWindowWidth = -1;

      if (typeof overlay.settings.cancelCallback == 'function'){
        overlay.settings.cancelCallback();
        return false;
      }
      overlay.close();
    },
    close : function() {
      this.lastWindowWidth = -1;

      // $('body').removeClass('overlay-state');
      // document.body.onresize = page.doResize;
      var placeHolder = $('#overlay-placeholder');
      overlay.overlay.insertAfter(placeHolder);
      placeHolder.remove();
      overlay.overlay.removeClass('active');
      $('#overlay-background').remove();
      if (typeof(overlay.settings.onClose) == 'function') {
        overlay.settings.onClose();
      }
      return false;
    }
  }

})(jQuery);