addthis_config = {
  'ui_language' : 'en',
  'ui_click' : true,
  'ui_hide_embed' : true,
  'services_compact' : "facebook, linkedin, twitter, google",
  'services_compact_org' : "facebook, linkedin, twitter, google"
};
build = false;
(function($){
  $(function(){
    if (typeof(addthis) == 'undefined') {
      $('.share-block').remove();
    } else {
      if (Drupal.settings.jnj.show_share == '1') {
        $('.share-block').show();
      }
      if (addthis.addEventListener) {
        addthis.addEventListener('http://www.jnj.com/sites//all//modules//jnj_custom//js//addthis.menu.open', handleAddThis);
      }
      // Tilte link addthis
      if (Drupal.settings.addthis_title_link) {
        $('.block-addthis a').attr('title', Drupal.settings.addthis_title_link);
      }

      changeAddThisBehavior('.toolbox-desktop a.share-btn');
      if ($('.share-block a').length > 0) {
        if (!Modernizr.touch) {
          jQuery('.share-block .share-btn').closest('a').removeAttr('onmouseover');
          jQuery('.share-block .share-btn').closest('a').removeAttr('onclick');
          $('.share-block a').click(open_addthis_share);
        }
      }
      
      $('body').bind('click', function(e) {
        $('#at_hover a#at15sptx').click();
        setTimeout(function(){
          $('.atm').hide();
          $('#at20mc div').hide();
        }, 500);
      });
      
    }
  });

  function open_addthis_share(event){
    event.stopPropagation();
    var r = addthis_open(this, '', '[URL]', '[TITLE]');
    setTimeout(function(){
      $('.atm').show();
      $('#at20mc div').show();
    }, 500);
    return r;
  }

  function close_addthis_share(event) {
    event.stopPropagation();
    var r = addthis_close();
    setTimeout(function(){
      $('.atm').hide();
      $('#at20mc div').hide();
    }, 500);
    return r;
  }

  function changeAddThisBehavior(selector) {
    if (navigator.userAgent.match(/BlackBerry/i)) {
      $(selector).click(function(){
        window.location.href = 'http://www.addthis.com/bookmark.php?url='+window.location.href;
      });
    } else if (navigator.userAgent.match(/Windows Phone/i)) {
      $(selector).attr('href', 'http://www.addthis.com/bookmark.php?url='+window.location.href);
      $(selector).attr('target', '_blank');
    } else {
      $(selector).attr('href', '#');
      $(selector).click(open_addthis_share);
    }
  }

  function handleAddThis(){
    if (!build) {
      
      if ($('.share-block a').length > 0 && $('a.share-btn').length == 0) {
        share_window = $('#at15s').parent();
        $('.share-block div.block-content').append(share_window);
      } else if ($('a.share-btn').length > 0){
        share_window = $('#at15s').parent();
        $('a.share-btn').parent().append(share_window);
        $('a.share-btn').parent().find('#at15sptx').click(close_addthis_share);
      }

      if ($('a#atic_settings').length > 0) {
        $('a#atic_settings').remove();
      }
      
      if ($('a .at_bold').length > 0) {
        $('a .at_bold').addClass('at_b');
        $('a .at_bold').removeClass('at_bold');
      }


      if (Drupal.settings.addthis_append != '') {
        $('div#at_hover').append('<p>' + Drupal.settings.addthis_append + '</p>');
      }
      $('#at_hover a').click(function(){
        $('#at_hover a#at15sptx').click();
      });
      if (typeof(emailafriend_addthis) === "function") { 
        emailafriend_addthis();
      }
      $('#at20mc #at16pcc').remove();
      $('#at20mc #at16lb').remove();

      //addthis uses own eventlistener implementation, wich doesnt work for ie7-8, so we use a flag.
      build = true;
      if(addthis.removeEventListener) {
        addthis.removeEventListener('http://www.jnj.com/sites//all//modules//jnj_custom//js//addthis.menu.open', handleAddThis, true);
      }
    }
  }

})(jQuery);