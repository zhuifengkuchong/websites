(function($) {
  var started_mobile = false;
  $(function() {
    if ($('.view-display-id-care_inspires_care_thumb').length > 0) {
      // Media Queries
      MQ.addQuery({
        context: ['smartphone', 'smartphone_landscape'],
        call_in_each_context: false,
        callback: function() {
          started_mobile = true;
          mobileDeviceGallery();
          $('#control-buttons').removeClass('hide');
          if (!$('#overlay-background').length != 0) {
            attachVideoBehaviors();
          }
        }
      });

      MQ.addQuery({
        context: ['tablet_portrait', 'tablet_landscape', 'standard'],
        call_in_each_context: false,
        callback: function() {
          removeMobileGallery();
          $('#control-buttons').addClass('hide');
          attachVideoBehaviors();
        }
      });
    }
  });

  function attachVideoBehaviors() {
    // Call Overlay
    $('.view-display-id-care_inspires_care_thumb .views-row').click(function(){
      // Get Overlay class views-row-\d
      var overlayClass = $(this).attr('class').replace(/(.)*(views-row-)(\d)(.)*/, "$2$3");

      // Set video player id
      var videoPlayerId = 'video-player-on-overlay-' + overlayClass;

      // Create Overlay
      var onClose = function() {
        mobileCloseOverlayBehaviorResize();
        jwplayer(videoPlayerId).stop();
      };
      var settings = {onClose: onClose};
      callOverlay($('.view-display-id-care_inspires_care_video .' + overlayClass), settings);

      // Confire Video Player Container
      $('#overlay-background .overlay-content .video-player').attr('id', videoPlayerId);

      // Video Player
      var settings = new Object();
      settings.file = $('#overlay-background .overlay-content .video_url').text();
      settings.stretching = 'fill';
      if (navigator.userAgent.match(/BlackBerry/i) || navigator.userAgent.match(/Windows Phone/i)) {
        settings.modes = [
          {type: 'download'}
        ];
      }
      else if (Drupal.settings && Drupal.settings.jnj && Drupal.settings.jnj.care_inspires_care && Drupal.settings.jnj.care_inspires_care.player) {
        settings.flashplayer = Drupal.settings.jnj.care_inspires_care.player;
      }

      jwplayer(videoPlayerId).setup(settings);
      $('#' + videoPlayerId + '_wrapper').addClass('video-player-on-overlay');
    });
  }

  function mobileDeviceGallery() {
    var wrapper = $('div.view-display-id-care_inspires_care_thumb .view-content');

    wrapper.attr({'id' : 'cic_slider'});
    wrapper.find('div.views-row').wrap( '<li></li>' );
    wrapper.wrapInner('<ul class="slides"></ul>');

    if (wrapper.find('ul.slides').length > 1) {
      setTimeout( function () {
        $('div.view-display-id-care_inspires_care_thumb ul.slides:first').remove();
        $('div.view-display-id-care_inspires_care_thumb .control-nav:first').remove();
      }, 15);
    }

    $('#cic_slider').flexslider({
      animation: "slide",
      controlNav: false,
      slideshow: false,
      touch: true,
      animationLoop: true,
      after : function () {
        changeActive();
      }
    });

    var sliderSelector = '#cic_slider';
    var slider = $(sliderSelector).data('flexslider');
    var list = $('<ol></ol>').attr({
      'class' : 'control-nav control-paging',
      'id'    : 'control-buttons'
    });

    var listItems = '';
    for (var i = 0 ; i < slider.slides.length; i++) {
      if (i == 0) {
        listItems += '<li class="active"><a>'+ parseInt(i + 1) +'</a></li>';
      } else {
        listItems += '<li><a>'+ parseInt(i + 1) +'</a></li>';
      }
    }

    list.append(listItems).find('li').click(function(e) {
      e.preventDefault();
      e.stopPropagation();
      $(sliderSelector).flexslider($(this).index());
    });

    $(sliderSelector).find('.flex-direction-nav').remove();
    wrapper.append(list);
  }

  function changeActive() {
    var wrapper = $('div.view-display-id-care_inspires_care_thumb');
    var sliderData = $('#cic_slider').data('flexslider');
    var list = wrapper.find('ol li');
    list.removeClass('active');
    list.eq(sliderData.currentSlide).addClass('active');
  }

  function removeMobileGallery() {
    if (started_mobile) {
      var wrapper = $('div.view-display-id-care_inspires_care_thumb');
      wrapper.find('ul.slides li').contents().unwrap();
      wrapper.find('ul.slides').contents().unwrap();
      if(wrapper.find('div.views-row.views-row-first').length > 1) {
        wrapper.find('div.views-row.views-row-first:last').remove();
      }
      if(wrapper.find('div.views-row.views-row-last').length > 1) {
        wrapper.find('div.views-row.views-row-last:first').remove();
      }
      wrapper.find('.view-content ol.control-nav').remove();
      destroyFlexslider('#cic_slider');
      wrapper.find('.view-content').removeAttr('id');
      started_mobile = false;
    }
  }

})(jQuery);
