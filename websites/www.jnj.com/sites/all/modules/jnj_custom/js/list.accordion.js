var finished = true;

jQuery(function(){
  initAccordion();
});

function initAccordion(context) {
  configureAccordion(context);

  var accordionContext = jQuery('.accordion', context);
  if (!context) {
    accordionContext = accordionContext.not('.ignore-automatic-accordion');
  }

  accordionContext.each(function(){
    jQuery(this).find('.accordion-close:first, .accordion-body:first').hide();
  });

  jQuery('.accordion-group').each(function(index, value){
    var accordionGroup = jQuery(this);

    jQuery(this).find('.accordion-expand-all, .accordion-collapse-all').each(function(){
      jQuery(this).parents('a').click(function(event){
        event.preventDefault();
      });
    });

    jQuery(this).find('.accordion-expand-all').click(function(event) {
      if(!jQuery(this).hasClass('accordion-ignore-prevent-click')) {
        event.preventDefault();
      }
      openAllAccordion(accordionGroup);
    });

    jQuery(this).find('.accordion-collapse-all').click(function(event) {
      if(!jQuery(this).hasClass('accordion-ignore-prevent-click')) {
        event.preventDefault();
      }
      closeAllAccordion(accordionGroup);
    });
  });
}

function configureAccordion(context) {
  // Readspeaker fix
  jQuery('.accordion').addClass('rs_preserve');
  jQuery('.rs_skip .accordion').removeClass('rs_preserve');
  jQuery('.accordion-close').addClass('rs_skip');
  jQuery('.rs_skip .accordion-close').removeClass('rs_skip');

  var accordionContext = jQuery('.accordion', context);
  if (!context) {
    accordionContext = accordionContext.not('.ignore-automatic-accordion');
  }

  accordionContext.find('.accordion-open:first, .accordion-close:first, .accordion-toggle:first, .accordion-open-close:first').each(function(){
    jQuery(this).parents('a').click(function(event){
      event.preventDefault();
    });
  });

  accordionContext.find('.accordion-toggle:first').click(function(event) {
    if(!jQuery(this).hasClass('accordion-ignore-prevent-click')) {
      event.preventDefault();
    }
    closeOthersAccordion(jQuery(this));
  });

  accordionContext.find('.accordion-open:first, .accordion-close:first, .accordion-toggle:first, .accordion-open-close:first').click(function(event){
    if(!jQuery(this).hasClass('accordion-ignore-prevent-click')) {
      event.preventDefault();
    }
    toggleAccordion(jQuery(this));
  });

  if (Drupal.settings.textonly) {
    jQuery('.accordion-toggle', context).unbind('click');
  }
}

function destroyAccordion(context) {
  resetAccordionBehaviors(context);
  jQuery('.accordion-close, .accordion-body', context).show();
}

function resetAccordionBehaviors(context){
  jQuery('.accordion-open, .accordion-close, .accordion-toggle, .accordion-open-close', context).unbind('click');
}

function reConfigureAccordion(context){
  resetAccordionBehaviors(context);
  configureAccordion(context);

  jQuery('.accordion', context).each(function(){
    jQuery(this).find('.accordion-close:first, .accordion-body:first').hide();
  });
}

function toggleAccordion(objAccordion) {
  if (finished || Modernizr.touch) {
    finished = false;

    if (objAccordion.closest('.accordion').find('.accordion-body:first:visible').length == 0) {
      objAccordion.closest('.accordion').addClass('open');
    }
    else {
      objAccordion.closest('.accordion').removeClass('open');
    }

    objAccordion.closest('.accordion').find('.accordion-open:first').toggle();
    objAccordion.closest('.accordion').find('.accordion-close:first').toggle();
    if (jQuery.inArray(MQ.context, ['smartphone_portrait', 'smartphone_landscape']) != -1 || !Modernizr.touch) {
      objAccordion.closest('.accordion').find('.accordion-body:first').slideToggle({'complete':finishToggle});
    }
    else {
      objAccordion.closest('.accordion').find('.accordion-body:first').toggle();
    }

  }
}

function closeOthersAccordion(objAccordion) {
  jQuery('.accordion').not(objAccordion.closest('.accordion')).find('.accordion-open').show();
  jQuery('.accordion').not(objAccordion.closest('.accordion')).find('.accordion-close').hide();
  jQuery('.accordion').not(objAccordion.closest('.accordion')).find('.accordion-body').slideUp({'complete':finishToggle});
  jQuery('.accordion').not(objAccordion.closest('.accordion')).removeClass('open');
}

function finishToggle() {
  finished = true;
  //jQuery('body').scrollTop(0); to scroll up
}

function openAllAccordion(accordionGroup) {
  jQuery('.accordion', accordionGroup).addClass('open');
  jQuery('.accordion', accordionGroup).find('.accordion-open').hide();
  jQuery('.accordion', accordionGroup).find('.accordion-close').show();
  if (jQuery.inArray(MQ.context, ['smartphone_portrait', 'smartphone_landscape']) != -1 || !Modernizr.touch) {
    jQuery('.accordion', accordionGroup).find('.accordion-body:first').slideDown({'complete':finishToggle});
  }
  else {
    jQuery('.accordion', accordionGroup).find('.accordion-body:first').show();
  }
}

function closeAllAccordion(accordionGroup) {
  jQuery('.accordion', accordionGroup).find('.accordion-open').show();
  jQuery('.accordion', accordionGroup).find('.accordion-close').hide();
  jQuery('.accordion', accordionGroup).find('.accordion-body').slideUp({'complete':finishToggle});
  jQuery('.accordion', accordionGroup).removeClass('open');
}
