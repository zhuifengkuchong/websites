/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

(function($) {
  $(function() {
    
    $.each($('img.help-img'), function(k, v){
      var img = "/sites/default/files/help/" + $(this).attr('id') + ".png";
      $(this).after('<img style="display:none" class="preview" src="'+ img +'" alt="preview image admin area" />');
    });

    $('img.help-img').click(function() {
      var preview = $(this).next();
      var height = preview.height();
      var width = preview.width();
      preview.modal({
        opacity:40,
        overlayCss:{backgroundColor:"#fff"},
	containerCss:{
          height:height, 
          width:width
	},
        overlayClose:true
      });
    });
    
    $("#block-views-related-news-block .field-name-field-custom-title").click(function() {
      var pai = $(this).parents('li');
      var path = $('.news-link', pai).html();
      window.location = path;
    });
    
  });
})(jQuery);

