<script id = "race3a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race3={};
	myVars.races.race3.varName="edit-search-keys--2__onblur";
	myVars.races.race3.varType="@varType@";
	myVars.races.race3.repairType = "@RepairType";
	myVars.races.race3.event1={};
	myVars.races.race3.event2={};
	myVars.races.race3.event1.id = "Lu_DOM";
	myVars.races.race3.event1.type = "onDOMContentLoaded";
	myVars.races.race3.event1.loc = "Lu_DOM_LOC";
	myVars.races.race3.event1.isRead = "False";
	myVars.races.race3.event1.eventType = "@event1EventType@";
	myVars.races.race3.event2.id = "edit-search-keys--2";
	myVars.races.race3.event2.type = "onblur";
	myVars.races.race3.event2.loc = "edit-search-keys--2_LOC";
	myVars.races.race3.event2.isRead = "True";
	myVars.races.race3.event2.eventType = "@event2EventType@";
	myVars.races.race3.event1.executed= false;// true to disable, false to enable
	myVars.races.race3.event2.executed= false;// true to disable, false to enable
</script>

