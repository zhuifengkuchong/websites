//#region Oxy Videos
var oxy = window.oxy || {};

oxy.videoOnThisSite = function (id, Title, Text, ytId) {
    this.videoId = id;
    this.videoTitle = Title;
    this.videoText = Text;
    this.ytId = ytId;
};
oxy.videoOnThisSite.prototype = {
    get_YTVideoLink: function () { return '//www.youtube.com/embed/' + this.ytId + '?modestbranding=1';}

};

oxy.videoSonThisSite = function (videoArray) {
    this.videos = [];
    this.videos = videoArray;
}
oxy.videoSonThisSite.prototype = {
    addItemToArray: function (id, Title, Text, ytId) {
        this.videos.push(new oxy.videoOnThisSite(id, Title, Text, ytId));
    },

    get_IndexOfSelectedItemShowPlayer: function (curVideoId) {
        if (oxy.inSwipeAction) {
            oxy.inSwipeAction = false;
            return;
        }

        var foundIndex = -1;
        var i;
        var curArrayItemId;

        for (i = 0; i < this.videos.length; i++) {
            curArrayItemId = this.videos[i].videoId;
            if (curArrayItemId == curVideoId) {
                foundIndex = i;
                break;
            };
        };
        if (foundIndex != -1) {
            this.showPlayer(this.videos[foundIndex].ytId, this.videos[foundIndex].videoTitle, this.videos[foundIndex].videoText);
        }
    },

    showPlayer: function (ytEmbedCode, videoTitle, videoText, ensureAutoPlay) {

        neudesic.writeToConsole("Before string tests: " + ytEmbedCode);

        videoTitle = videoTitle.replace(/&#39;/g, "'"); //Convert the quote character code back to a quote for display in the modal box
        videoText = videoText.replace(/&#39;/g, "'"); //Convert the quote character code back to a quote for display in the modal box
        videoText = videoText.replace(/&#92;/g, "\\"); //Convert the \ character code back to a \ for display in the modal box
        videoText = videoText.replace(/&#13;/g, "  "); //Convert the newline character code 2 spaces for display in the modal box

        if (ytEmbedCode.search(/rel\=/i) === (-1)) {
            ytEmbedCode = ytEmbedCode.replace("?", "?rel=0&");
        }

        if (ytEmbedCode.search(/modestbranding/) === (-1)) {
            ytEmbedCode = ytEmbedCode.replace("?", "?modestbranding=1&");
        }
        if (ytEmbedCode.search(/nocookie/i) === (-1)) {
            ytEmbedCode = ytEmbedCode.replace(/youtube/i, "youtube-nocookie");
        }
        if (ytEmbedCode.search(/allowfullscreen/i) === (-1)) {
            ytEmbedCode = ytEmbedCode.replace(/><\/iframe>/i, " allowfullscreen></iframe>");
        }

        if (ensureAutoPlay &&  (ytEmbedCode.search(/autoplay/) === (-1)) ) {
            ytEmbedCode = ytEmbedCode.replace("?", "?autoplay=1&");
        }

        neudesic.writeToConsole("After  string tests: " + ytEmbedCode);
        
        $('#oxyVideoPlayer .modal-body').html(ytEmbedCode); //Load the embed code directly into the div
        $('#oxyModalVideoTitle').text(videoTitle);
        $('#oxyModalVideoText').text(videoText);

        $('#oxyVideoPlayer').modal('show');

    },

    stopPlayer: function (){
        $('#oxyVideoPlayer .modal-body').html(""); //Remove the iframe to stop the player.

    }

};

oxy.videos = [];
var siteVideos = new oxy.videoSonThisSite(oxy.videos);


//#endregion

