
//#region Oxy name space
var oxy = oxy || {};

oxy.pageLayoutInterface = function () {
    addCarouselItems = function () {
        var newCarouselItem;
        var newCarouselItemTemplate = '<div class="item">ANCHORSTART<img src="IMAGE" alt="ALT"  />ANCHOREND</div>';
        var newCarouselIndicatorTemplate = '<li title="CAPTION" data-target="#heroCarousel" data-slide-to="SLIDETONUM"></li>';
        var curIndicatorNum = 0;
        var rendition = "";
        var siteRelativeURL = neudesic.siteRelativeURL();

        function setRendition() {
            if (oxy.curGrid == 0) {
                rendition = "?RenditionID=7";
            }
            if (oxy.curGrid == 1) {
                rendition = "?RenditionID=6";
            }
        }

        function bldAndAddItem(image, alt, caption,  url) {
            var newCarouselIndicator = newCarouselIndicatorTemplate.replace('SLIDETONUM', curIndicatorNum++);
            newCarouselIndicator = newCarouselIndicator.replace('CAPTION', (caption || "No Caption"));
            var newItem = newCarouselItemTemplate.replace('IMAGE', image); 
            var anchorStart = '';
            var anchorEnd = '';

            if (typeof (url) != undefined && url != '' && url != null) {
                anchorStart = '<a href=' + url + '>';
                anchorEnd = '</a>';
            }

            newItem = newItem.replace('ALT', (alt || "No Alt Text"));
            newItem = newItem.replace('CAPTION', (caption || "No Caption"));
            newItem = newItem.replace('ANCHORSTART', anchorStart);
            newItem = newItem.replace('ANCHOREND', anchorEnd);

            neudesic.writeToConsole("New Hero Carousel Item: ");
            neudesic.writeToConsole(newItem);

            $('#heroCarousel-inner').append(newItem);
            $('#heroCarousel-indicators').append(newCarouselIndicator);

        }

        setRendition();
        neudesic.writeToConsole("In AddCarouselItems. Rendition: " + rendition);

        $('#heroCarousel-inner div').remove();
        $('#heroCarousel-indicators li').remove();

        var RestUrl = "_api/web/lists/getbytitle('Carousel')/items/?$select=Sequence,NameOrTitle,ServerUrl,Carousel_x0020_Item_x0020_Link,Carousel_x0020_Item_x0020_Caption&$orderby=Sequence&$filter=Sequence gt 0";
        function getListItems_AddToCarousel() {;
            var writeRESTResultsToConsole = function (item) {
                neudesic.writeToConsole("Carousel Item:");
                neudesic.writeToConsole("Sequence: " + item.Sequence);
                neudesic.writeToConsole("Name: " + item.NameOrTitle);
                neudesic.writeToConsole("Url: " + item.ServerUrl);
                neudesic.writeToConsole("Link: " + item.Carousel_x0020_Item_x0020_Link);
                neudesic.writeToConsole("Caption: " + item.Carousel_x0020_Item_x0020_Caption);
            };
            $.ajax({
                url: siteRelativeURL + RestUrl,
                method: "GET",
                headers: { "Accept": "application/json; odata=verbose" },
                success: function (data) {
                    neudesic.writeToConsole(_spPageContextInfo.webServerRelativeUrl);
                    $.each(data.d.results, function (index, item) {
                        if (typeof console != "undefined") { writeRESTResultsToConsole(item) };
                        bldAndAddItem(item.ServerUrl + rendition, item.Carousel_x0020_Item_x0020_Caption, item.Carousel_x0020_Item_x0020_Caption, item.Carousel_x0020_Item_x0020_Link);

                        if (index === 0) { //Display the 1st image as soon as available.
                            $('#heroCarousel-inner :first-child').addClass('active');
                            $('#heroCarousel-indicators :first-child').addClass('active');
                        }

                    });

                },
                error: function (data) {
                    neudesic.writeToConsole(("Failed to load Hero Carousel items for this query: " + _spPageContextInfo.webServerRelativeUrl + RestUrl), "error");
                }
            });
        }

        getListItems_AddToCarousel();

    };

    gridChange = function () {
        addCarouselItems();
        $("#heroCarousel").carousel();

        if (oxy.curGrid == 1) {
            $('#videolist').addClass('list-inline');
        }
        else {
            $('#videolist').removeClass('list-inline');
        }

        $('#headlineAndHeroCarousel div').on('dragstart', function (event) { event.preventDefault(); });

    };

    return {
        manageGridChange: gridChange
    }
}();


//#endregion


