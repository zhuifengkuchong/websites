//#region Neudesic name space
var neudesic = neudesic || {};

neudesic.writeToConsole = function (writeThis, writeHow) {
    if (typeof console != "undefined") {
        var consoleMode = writeHow || "info";
        if ("info|warn|error".indexOf(consoleMode) === -1)
        { consoleMode = "info" }; // Not found so assign to Info

        console[consoleMode](writeThis);
        return (true);
    }
    return (false);
};
neudesic.handleWinPhoneIE = function () {
    if (!neudesic.writeToConsole(navigator.userAgent)) {
        //alert(navigator.userAgent);
    }
    if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
        var msViewportStyle = document.createElement("style");
        msViewportStyle.appendChild(
            document.createTextNode(
                "@-ms-viewport{width:auto!important}"
            )
        );
        document.getElementsByTagName("head")[0].
            appendChild(msViewportStyle);
    }
}

neudesic.checkURLForString = function (stringToFind) {
    var urlString = window.location.href.toLowerCase();
    return (urlString.indexOf(stringToFind.toLowerCase()) > -1);
};

neudesic.stringMultiReplace = function (str) {
    for (i = 1; i < arguments.length; i++) {
        str = str.replace('{' + (i - 1) + '}', arguments[i]);
    }
    return str;
};

neudesic.siteRelativeURL = function () {
    var siteRelativeUrl = _spPageContextInfo.siteServerRelativeUrl;
    if (siteRelativeUrl != '/') { siteRelativeUrl += '/' };

    return siteRelativeUrl;
};

neudesic.timer = function (msgPrefix, msgType, timerOn) {
    var startTime = Date.now();

    function getElapsedTime(descr) {
        if (timerOn) {
            neudesic.writeToConsole(msgPrefix + ": " + (Date.now() - startTime) + " " + descr, msgType);
        }
    }
    return getElapsedTime;
}

//#endregion

//#region Oxy name space
var oxy = oxy || {};

oxy.rptPageLoadTime = neudesic.timer("PAGE LOAD TIME", "info", true); //msgType = info, warn, error


oxy.onHomeTab = false;
oxy.curGrid = 99; //Force grid change on load
oxy.devices = new Array("phone", "tablet", "desktop"); // These align with the Oxy.curGrid var
oxy.inDesignMode = '';
oxy.eventReminderOnPage = false;

oxy.inSwipeAction = false;

oxy.gridBreakPts = {
    phone: 240,
    tablet: 740,
    desktop: 992
};

//Handle non-existent interface
oxy.pageLayoutInterface = oxy.pageLayoutInterface || function () {
    nonExistentInterface = function () {
        neudesic.writeToConsole("No Page Layout Interface set for this page");
    };

    return {
        manageGridChange: nonExistentInterface
    }
}();

oxy.setCurGridManageChange = function () {
    var innerwidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    neudesic.writeToConsole('New Window.innerWidth: ' + innerwidth);
    /* Fade logo on Home Page
	*/
    if (oxy.onHomeTab) {
        $('.siteLogo_Oxy img').toggle(false); //New
    }

    var cur_curGrid = oxy.curGrid; //Do a test to handle Init

    oxy.curGrid = 2; //Desktop
    if (innerwidth <= 992) {
        oxy.curGrid = 1; //Tablet
    }
    if (innerwidth <= 740) {
        oxy.curGrid = 0; //Phone
    }
    if (cur_curGrid != oxy.curGrid) {
        if (oxy.pageLayoutInterface && oxy.pageLayoutInterface.manageGridChange) {
            oxy.pageLayoutInterface.manageGridChange();
        }
        /* If the main Carousel function exists AND the page has a carousel AND the manageGridChange exists in the Carousel function, change the grid */
        if (oxy.jCarouselPerPageControl && oxy.jCarouselPerPageControl.pageHasCarousel && oxy.jCarouselPerPageControl.manageGridChange) {
            oxy.jCarouselPerPageControl.manageGridChange();
        }
    }

    /* Fade logo on Home Page
	*/
    if (oxy.onHomeTab) {
        $('.siteLogo_Oxy img').toggle(1500); //New
    }

};

oxy.setTabActive = function () {
    //This function is for debugging in a non-SharePoint environment only.
    neudesic.writeToConsole("In click event handler");
    $('#topNavList li').removeClass('active');
    $(this).toggleClass("active");
};

//#region Stock Info Region

//The following 2 functions make similar Ajax calls against the same SiteCollectionDocuments/StockInfo.xml file. 
//If that file changes, both must be updated. The functions are used in different places and have different output.
oxy.stockInfoFile = "http://www.oxy.com/_catalogs/masterpage/oxy/js/SiteCollectionDocuments/StockInfo.xml";
oxy.dividendRate = 2.16;

oxy.bldStockTicker = function () {
    var siteRelativeURL = neudesic.siteRelativeURL();
    var stkTickerUrl = (siteRelativeURL === '/') ? (siteRelativeURL + oxy.stockInfoFile) : (siteRelativeURL + '/' + oxy.stockInfoFile);
    neudesic.writeToConsole(stkTickerUrl);

    //    var siteRelativeURL = ''; //TODO: Retrieve site URL from system.
    var dateTime, price, diff, priceN, diffN;
    try {
        $.ajax({
            type: "GET",
            url: stkTickerUrl,
            dataType: "xml",
            success: function (xml) {
                $(xml).find('Stock_Quote').each(function () {
                    neudesic.writeToConsole($(this).attr('Ticker'));
                    if (($(this).attr('Ticker')).toLowerCase() == 'oxy') {
                        dateTime = $(this).find('Date').text();
                        price = $(this).find('Trade').text();
                        diff = $(this).find('Change').text();

                        if (dateTime == '') throw 'Date is blank';
                        if (isNaN(price)) throw 'Price (<Trade>) is not a number';
                        if (isNaN(diff)) throw 'Change (<Change>) is not a number';
                        priceN = (price * 1);
                        diffN = (diff * 1);
                    }
                });

                var dir = (diffN > 0)
                    ? '<span class="glyphicon glyphicon-arrow-up"> </span>'
                    : '<span class="glyphicon glyphicon-arrow-down"> </span>';
                if (diffN == 0) { dir = ''; }; //No arrow or color if no change.

                var initialPrice = priceN - diffN;
                var percentDiff = (initialPrice == 0) ? '' : (diffN / initialPrice * 100).toFixed(2);

                var formatBrowser = '<div><span>as of {0} on NYSE </span> <strong>OXY {1}</strong> {2} {3} ({4}%)</div>';

                var formatPhoneBodyLastHR = (oxy.eventReminderOnPage) ? '' : '<hr />'; //Hide the bottom <hr> when the Event Reminder is on the page.
                var formatPhoneBody = '<div><hr /><p>as of {0} on NYSE </p> <strong>OXY {1}</strong> {2} {3} ({4}%)'
                + formatPhoneBodyLastHR
                + '</div>';

                var formatPhoneFooter = '<div><p>as of {0} on NYSE </p> <strong>OXY {1}</strong> {2} {3} ({4}%)</div>';

                var stkTickerSection = neudesic.stringMultiReplace(formatBrowser, dateTime, price, dir, diff, percentDiff);
                var stkTickerSectionPhoneBody = neudesic.stringMultiReplace(formatPhoneBody, dateTime, price, dir, diff, percentDiff);
                var stkTickerSectionPhoneFooter = neudesic.stringMultiReplace(formatPhoneFooter, dateTime, price, dir, diff, percentDiff);

                neudesic.writeToConsole(formatBrowser);
                neudesic.writeToConsole(formatPhoneBody);
                neudesic.writeToConsole(stkTickerSection);
                neudesic.writeToConsole(stkTickerSectionPhoneBody);

                $('#stockTickerBrowser').append(stkTickerSection);
                $('#stockTickerPhoneBody').append(stkTickerSectionPhoneBody);
                $('#stockTickerPhoneFooter').append(stkTickerSectionPhoneFooter);
            }
        });
    }
    catch (exc) {
        neudesic.writeToConsole('Error with Stock Quote: ' + exc.message, 'error');
    };

    neudesic.writeToConsole('After Stock Ticker xml success call');
};

oxy.bldStockTicker_Full = function (containingDivID) {

    var bldRowHTML = function (descr, value, valDivClass) {
        valDivClass = valDivClass || "";

        return (neudesic.stringMultiReplace("<li><div class='col-xs-5 col-sm-5 col-md-6'>{0}</div><div class='col-xs-7 col-sm-7 col-md-6 oxyIRPageStockPriceValCol {2}'>{1}</div></li>", descr, value, valDivClass));
    };

    var siteRelativeURL = neudesic.siteRelativeURL();
    var stkTickerUrl = (siteRelativeURL === '/') ? (siteRelativeURL + oxy.stockInfoFile) : (siteRelativeURL + '/' + oxy.stockInfoFile);
    neudesic.writeToConsole(stkTickerUrl);

    var stockPriceStructure = $('#' + containingDivID + ' ul');
    var rows = [];
    var curRow = 0;
    $(stockPriceStructure).empty();

    var dateTime, price, diff, priceN, diffN;
    var Volume, FiftyTwoWeekHigh, FiftyTwoWeekLow;
    var dir, initialPrice, percentDiff;
    var changeDivClass;

    try {
        $.ajax({
            type: "GET",
            url: stkTickerUrl,
            dataType: "xml",
            success: function (xml) {
                $(xml).find('Stock_Quote').each(function () {
                    neudesic.writeToConsole($(this).attr('Ticker'));
                    if (($(this).attr('Ticker')).toLowerCase() == 'oxy') {
                        dateTime = $(this).find('Date').text();
                        price = $(this).find('Trade').text();
                        diff = $(this).find('Change').text();
                        Volume = $(this).find('Volume').text();
                        FiftyTwoWeekHigh = $(this).find('FiftyTwoWeekHigh').text();
                        FiftyTwoWeekLow = $(this).find('FiftyTwoWeekLow').text();

                        if (dateTime == '') throw 'Date is blank';
                        if (isNaN(price)) throw 'Price (<Trade>) is not a number';
                        if (isNaN(diff)) throw 'Change (<Change>) is not a number';
                        priceN = (price * 1);
                        diffN = (diff * 1);
                    }
                });

                neudesic.writeToConsole('After Stock Ticker xml success call');

                if (diffN == 0) {
                    dir = '';
                }; //No arrow or color if no change.

                if (diffN > 0) {
                    dir = '<span class="glyphicon glyphicon-arrow-up"> </span>';
                    changeDivClass = "oxyStockUp";
                }

                if (diffN < 0) {
                    dir = '<span class="glyphicon glyphicon-arrow-down"> </span>';
                    changeDivClass = "oxyStockDown";
                }

                initialPrice = priceN - diffN;
                percentDiff = (initialPrice == 0) ? '' : (diffN / initialPrice * 100).toFixed(2);

                //rows[curRow++] = bldRowHTML("Stock Price: <b>Oxy</b>", ""); 
                rows[curRow++] = bldRowHTML('Price', price);
                rows[curRow++] = bldRowHTML('Change', dir + diffN + ' (' + percentDiff + '%)', changeDivClass);
                rows[curRow++] = bldRowHTML('Volume', Volume);
                rows[curRow++] = bldRowHTML('52 Week High', FiftyTwoWeekHigh);
                rows[curRow++] = bldRowHTML('52 Week Low', FiftyTwoWeekLow);
                rows[curRow++] = bldRowHTML('Dividend Rate', oxy.dividendRate);

                for (curRow = 0; curRow < rows.length; curRow++) {
                    neudesic.writeToConsole(rows[curRow]);
                    $(stockPriceStructure).append(rows[curRow]);
                }


            }
        });
    }

    catch (exc) {
        neudesic.writeToConsole('Error with Stock Quote: ' + exc.message, 'error');
    };

};

//#endregion 

oxy.setTab = function () {
    oxy.onHomeTab = true; //Default to true if no tabs detected.
    $('#topNavList li').removeClass('active');

    var tabNum = -1;
    $('#topNavList li').each(function (index) {
        //neudesic.writeToConsole(index + ' ' + (thisthis.children('a').text()));
        neudesic.writeToConsole(index + ' ' + ($(this).children('a').attr('href')));
        if (neudesic.checkURLForString($(this).children('a').attr('href'))) {
            tabNum = index;
        }
    });
    if (tabNum != -1) {
        $('#topNavList li:eq(' + tabNum + ')').addClass('active');
        oxy.onHomeTab = false; //On a non-home tab
    }

};

oxy.loadTopNav = function () {
    oxy.rptPageLoadTime("loadTopNav Start");
    var parentUrl = "";

    var curUrl = _spPageContextInfo.webServerRelativeUrl;
    var endOfParentUrl = curUrl.lastIndexOf("/");
    if (endOfParentUrl != -1) {
        parentUrl = curUrl.substring(0, endOfParentUrl) + "/"; //Need to append the / here to handle the top level site.
    }
    else {
        neudesic.writeToConsole(("Failed to determine Parent Url for Site Title. Current URL:  " + curUrl), "error");
    }

    var processRestSuccess = function (data) {
        var glyph = "arrow-up";
        if (parentUrl === "/") {
            glyph = "home";
        }
        var phoneParentLink = $("#OxyPhoneNavParentLink a");
        $(phoneParentLink).attr("href", parentUrl);
        $(phoneParentLink).html("<span class='glyphicon glyphicon-" + glyph + "'>&nbsp;</span><em>" + data.d.Title + "</em>");
    }

    var RestUrl = parentUrl + "_api/web?$select=Title";
    $.ajax({
        url: RestUrl,
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {
            processRestSuccess(data); //Use a function for the processing to facilitate debugging.
            oxy.rptPageLoadTime(("loadTopNav - Loaded the Site Title: Rest call: " + RestUrl));
        },
        error: function (data) {
            neudesic.writeToConsole(("Failed to load Site Title with this Rest call: " + RestUrl), "error");
        }
    });

};

//#region jCarousel
oxy.jCarouselPerPageControl = oxy.jCarouselPerPageControl || {
    /*Note that this code is not used for phones as of 11-March-2014. 
        The video content is displayed on phones using a Bootstrap Carousel. That is created in the web part.
        The code for phones was left in this function to facilitate vertical listings on a phone if that's desired later at some point.
     */

    pageHasCarousel: true,

	maxVidsToShow: {
	    phone: 1, /*Set to 1 to support vertical display*/
	    tablet: 3,
	    desktop: 4,
	},
	
    divWidths: {
        desktop: 820,
        tablet: 580,
        phone: 200
    },
    
    centeringOffset: {
    	 desktop1: '100px'
    	,desktop2: '200px'
    	,desktop3: '300px'
    	,tablet1: '80px'
    	,tablet2: '190px'
   	},

    classCarousel_phone: 'carouselPhone',
    classCarousel_tablet: 'carouselTablet',
    classCarousel_desktop: 'carouselDesktop',

    setWidthBasedOnCountsPerGrid: function (curWidth) {
        var width;
        var divWidth;
        var curDevice = oxy.devices[oxy.curGrid];
        var vidsToShow = this.maxVidsToShow[curDevice];

		if(curDevice != 'phone' && oxy.vidCount < this.maxVidsToShow[curDevice]){
			var centerVideos = true; //Set this to true to cause videos to be centered when less than the max. Set to false to cause the videos to take the entire section.
			if (centerVideos){
				var offsetLeftMarginKey = curDevice + (this.maxVidsToShow[curDevice]-oxy.vidCount); 
				oxy.vidList.css('margin-left', this.centeringOffset[offsetLeftMarginKey]);
			}
			else{
				vidsToShow = oxy.vidCount;
			}
		}
		else{
			oxy.vidList.css('margin-left', '0');
		}
		
        divWidth = this.divWidths[curDevice];
        width = (divWidth / vidsToShow);

        oxy.jcarouselDiv.css('width', divWidth + 'px');
        oxy.jcarouselWrapperDiv.css('width', divWidth + 'px');
        oxy.jcarouselDiv.jcarousel('items').css('width', width + 'px');

    },
    
    showHideNavControls: function(){
	    var curDevice = oxy.devices[oxy.curGrid];
		if(oxy.vidCount <= this.maxVidsToShow[curDevice]){
			$('.jcarousel-pagination').hide();
			$('.videoCarouselButton').hide();
			$('#phoneVideoCarousel .carousel-indicators').hide();
		}
		else{
			$('.jcarousel-pagination').show();
			$('.videoCarouselButton').show();
			$('#phoneVideoCarousel .carousel-indicators').show();
		}
    },

    init: function () {
        oxy.jcarouselDiv = $('.jcarousel');
        if (oxy.jcarouselDiv.length === 0) {
            this.pageHasCarousel = false;
            return;
        }
        oxy.vidList = $('#videolist'); oxy.vidCount=oxy.vidList.children().length;
        oxy.jcarouselWrapper = $('.wrapper');
        oxy.jcarouselWrapperDiv = $('.jcarousel-wrapper');
        oxy.jcarouselPaginationBar = $('.jcarousel-pagination');
        oxy.jcarouselPrev = $('.jcarousel-control-prev');
        oxy.jcarouselNext = $('.jcarousel-control-next');
        oxy.jcarouselPrevControl = $('.jcarousel-control-prev span');
        oxy.jcarouselNextControl = $('.jcarousel-control-next span');

        oxy.jcarouselAutoScroll = false;

        oxy.jcarouselDiv.jcarousel({
            wrap: 'circular'
        });

        if (oxy.jcarouselAutoScroll) {
            oxy.jcarouselDiv.jcarouselAutoscroll(
				{
				    interval: 3000,
				    target: '+=1',
				    autostart: true
				}
				);
        }

        $('.jcarousel-control-prev')
		.jcarouselControl({
		    target: '-=1'
		});

        $('.jcarousel-control-next')
			.jcarouselControl({
			    target: '+=1'
			});

        $('.jcarousel-pagination')
			.on('jcarouselpagination:active', 'a', function () {
			    $(this).addClass('active');
			})
			.on('jcarouselpagination:inactive', 'a', function () {
			    $(this).removeClass('active');
			})
			.on('click', function (e) {
			    e.preventDefault();
			})
			.jcarouselPagination({
			    perPage: 1,
			    item: function (page) {
			        return '<a href="#' + page + '">' + page + '</a>';
			    }
			});
    },

    manageGridChange: function () {
		this.showHideNavControls(); // Do this regardless of viewport
        if (oxy.curGrid === 0) return; /*As of 11-March-2014 the phone video carousel uses Bootstrap and not jcarousel. Code left to enable changing if needed.*/
        var curDevice = oxy.devices[oxy.curGrid];
        var curDeviceClass = this['classCarousel_' + curDevice];

        this.setWidthBasedOnCountsPerGrid();

        oxy.jcarouselDiv.jcarousel('scroll', 0); //Scroll to the 1st item to 'reload' the current display.

        var classesToRemove = this.classCarousel_phone + ' ' + this.classCarousel_tablet + ' ' + this.classCarousel_desktop;
        oxy.jcarouselDiv.removeClass(classesToRemove);
        oxy.jcarouselWrapperDiv.removeClass(classesToRemove);
        oxy.jcarouselDiv.jcarousel('items').removeClass(classesToRemove);
        oxy.jcarouselPaginationBar.removeClass(classesToRemove);
        oxy.jcarouselPrev.removeClass(classesToRemove);
        oxy.jcarouselNext.removeClass(classesToRemove);

        oxy.jcarouselDiv.addClass(curDeviceClass);
        oxy.jcarouselWrapperDiv.addClass(curDeviceClass);
        oxy.jcarouselDiv.jcarousel('items').addClass(curDeviceClass);

        oxy.jcarouselPaginationBar.addClass(curDeviceClass);
        oxy.jcarouselPrev.addClass(curDeviceClass);
        oxy.jcarouselNext.addClass(curDeviceClass);


        oxy.jcarouselDiv.jcarousel({
            vertical: false // (oxy.curGrid === 0) /*Turn vertical on for phones and off for all else*/
        });

    }

};
//#endregion

oxy.clearUnusedCallouts = function () {
    if (oxy.inDesignMode != "1") {
        var emptySectionsThisBlock = 0, sectionsPerBlock = 3;
        var emptyBlockCount = 0, numberOfBlocks = 4;
        var firstCallout = true;
        var currentCallout;
        jQuery(".callouts .calloutTitle div").each(function () {
        	emptySectionsThisBlock = 0;
        	var titleDiv = $(this);
        	
        	var imgDiv = $(titleDiv).parent().next('div.responsiveImg');
        	var imgCount = $(imgDiv).find('img').length;
        	
        	var HTMLDiv = $(imgDiv).next('.calloutHTML');
        	var HTMLCount = $(HTMLDiv).find('.ms-rtestate-field').children().length;
        	
            if ($(titleDiv ).text().trim() == "") {
                $(titleDiv ).attr("class", "calloutTitleEmpty");
                currentCallout = titleDiv ;
                emptySectionsThisBlock++;
                $(titleDiv).hide(); //No title. Hide the entire
                if (firstCallout){
                	$(titleDiv).parent().hide(); // No hr here. Hide the entire section.
                }
            }

            if (imgCount == 0){
            	$(imgDiv).hide();
            	emptySectionsThisBlock++;
            }
            
            if (HTMLCount == 0){
            	$(HTMLDiv).hide();
            	emptySectionsThisBlock++;
            }
            
            if (emptySectionsThisBlock == sectionsPerBlock){
                $(titleDiv).parent().hide(); //Hide the entire title div if nothing in this section. This hides the <hr>.
                emptyBlockCount++;
            }
            firstCallout = false;
        });
    
        /*
        */
        if (emptyBlockCount == numberOfBlocks ) {
            jQuery(currentCallout).closest("Table").hide();
        }
        $(".callouts .responsiveImg").filter(function () {
            return $(this).find("img").length == 0;
        }).hide();

    }
};

oxy.loadMegaMenus = function () {
    var source = neudesic.siteRelativeURL() + "pages/mega-menus.aspx";

    $("#menuOB").load(source + " #contentOB");
    $("#menuSR").load(source + " #contentSR");
    $("#menuIR").load(source + " #contentIR");
    $("#menuCareers").load(source + " #contentCareers");
    $("#menuNews").load(source + " #contentNews");
    $("#menuAboutUs").load(source + " #contentAboutUs");

};

var hoverConfig = {
    sensitivity: 2, // number = sensitivity threshold (must be 1 or higher)
    interval: 200, // number = milliseconds for onMouseOver polling interval
    over: megaHoverOver, // function = onMouseOver callback (REQUIRED)
    timeout: 500, // number = milliseconds delay before onMouseOut
    out: megaHoverOut // function = onMouseOut callback (REQUIRED)
};
var navBackColor;
var navColor;

//On Hover Over
function megaHoverOver() {
    jQuery(this).find(".sub").stop().fadeTo(100, .98).show(); //Find sub and fade it in 
    jQuery(this).find("a").attr("style", "background-color:#00529b;color:#e8eaef");

}

//On Hover Out
function megaHoverOut() {
    var aTag = jQuery(this).find("a");
    if (!aTag.parent().hasClass("active")) {
        aTag.attr("style", "background-color:transparent;color:#00529b");
    };

    jQuery(this).find(".sub").stop().fadeTo(100, 0, function () { //Fade to 0 opactiy

        jQuery(this).hide();  //after fading, hide it

    });
}

oxy.formatTopSrchBox = function () {
    /*Replace the SP search icon with a glyph*/
    var srchImg = $('.ms-srch-sb-searchImg');
    $.each(srchImg, function (index, value) {
        $(value).replaceWith("<span class='oxySrchIcon glyphicon glyphicon-search'></span>");

    });
    //Hide the header search box if in the search page.
    if (neudesic.checkURLForString("http://www.oxy.com/_catalogs/masterpage/oxy/js/search.aspx")) {
        $('.HeaderRow1 .searchbox').remove();
        $('#searchButton').remove();

    }
}

oxy.attachPhoneSearchButtonEventHandlers = function () {
    var aElement = $('#searchButton a');
    var spanElement = $('#searchButton a span')
    aElement.on('click', function () {
        $(spanElement).toggleClass('glyphicon-chevron-up');
        $(spanElement).toggleClass('glyphicon-search');
        $(aElement).toggleClass('oxyBlue1Background');
        $(aElement).toggleClass('oxyOrangeBackground');

    });
}

oxy.attachSwipeEventsToCarousels = function () {
    $("#heroCarousel, #phoneVideoCarousel").swiperight(function () {
        oxy.inSwipeAction = true;
        $(this).carousel('prev');
        $(this).carousel('pause');
    });
    $("#heroCarousel, #phoneVideoCarousel").swipeleft(function () {
        oxy.inSwipeAction = true;
        $(this).carousel('next');
        $(this).carousel('pause');
    });

    $("#videoWebPart").swipeleft(function () {
        oxy.inSwipeAction = true;
        $('.jcarousel').jcarousel('scroll', '+=1');

    });

    $("#videoWebPart").swiperight(function () {
        oxy.inSwipeAction = true;
        $('.jcarousel').jcarousel('scroll', '-=1');
    });

    $('#heroCarousel-inner').click(function () {
        if (oxy.inSwipeAction) {
            oxy.inSwipeAction = false; //Set this back to false. 
            return false; //Don't act on the <a href=...> click.
        }
    });

}

oxy.attachMiscEvents = function () {
    /*Add Shift-Ctrl-Click on the TopNav bar to show-hide the ribbon. Modified in Artifacts.*/
    $('#topnav').mouseup(function (e) {
        var isShiftCtrlPressed = (e.ctrlKey && e.shiftKey);
        if (isShiftCtrlPressed) {
            var curState = $('#ms-designer-ribbon').css('display');
            if (curState === null || curState !== 'none') {
                $('#ms-designer-ribbon').css('display', 'none');
            }
            else {
                $('#ms-designer-ribbon').css('display', 'block');
            }
        }
    });
}

oxy.attachVideoLinkActions = function () {
    //The siteVideos object and associated functions used below are defined in OxyPrePageLoad.js

    $('.oxyShowVideoLink').on('click', function () {
        //oxy.showVideoPlayer($(this).attr('oxyvideoId'), siteVideos);
        siteVideos.get_IndexOfSelectedItemShowPlayer($(this).attr('oxyvideoid'));
    });

    // Can use on('hide.bs.modal') also. That will stop the video immediately instead of 'fading out'
    $('#oxyVideoPlayer').on('hidden.bs.modal', function () {
        siteVideos.stopPlayer();
    });

    $('a.oxyDirectVideoLink').click(function () {
        var siteRelativeURL = neudesic.siteRelativeURL();

        var href = $(this).attr('href');

        var splits = href.split("/");
        var videoID = splits[splits.length - 1];
        var ensureAutoPlay = $(this).hasClass('AutoPlayVideo');

        var RestUrl = "_api/web/lists/getbytitle('Oxy Video Repository')/items(" + videoID + ")/?$select=VideoSetDescription,NameOrTitle,VideoSetEmbedCode";

        $.ajax({
            url: siteRelativeURL + RestUrl,
            method: "GET",
            headers: { "Accept": "application/json; odata=verbose" },
            success: function (data) {
                siteVideos.showPlayer(data.d.VideoSetEmbedCode, data.d.NameOrTitle, data.d.VideoSetDescription, ensureAutoPlay);
            },
            error: function (data) {
                neudesic.writeToConsole(("Failed to load Direct Video Link for this query: " + _spPageContextInfo.webServerRelativeUrl + RestUrl), "error");
            }
        });
        return false; //Cancel the href action
    });
}

oxy.attachStoryLinkActions = function () {
    $('a.oxyDirectStoryLink').click(function () {

        var RestUrl = buildRestURL($(this).attr('href'));

        $.ajax({
            url: RestUrl,
            method: "GET",
            headers: { "Accept": "application/json; odata=verbose" },
            success: function (data) {
                processRestSuccess(data); //Use a function for the processing to facilitate debugging.
            },
            error: function (data) {
                neudesic.writeToConsole(("Failed to load Direct Video Link for this query: " + _spPageContextInfo.webServerRelativeUrl + RestUrl), "error");
            }
        });
        return false; //Cancel the href action
    });

    var buildRestURL = function (seed) {
        var splits = seed.split("/");
        var storyID = splits[splits.length - 1];
        return neudesic.siteRelativeURL() + "_api/web/lists/getbytitle('Oxy Story Repository')/items(" + storyID + ")/?$select=Article,Title,Image";
    }

    var processRestSuccess = function (data) {
        var storyTitle = data.d.Title.replace(/&#39;/g, "'"); //Convert the quote character code back to a quote for display in the modal box

        var storyText = data.d.Article.replace(/&#39;/g, "'"); //Convert the quote character code back to a quote for display in the modal box
        storyText = storyText.replace(/&#92;/g, "\\"); //Convert the \ character code back to a \ for display in the modal box
        storyText = storyText.replace(/&#13;/g, "  "); //Convert the newline character code 2 spaces for display in the modal box

        var storyImg = data.d.Image.Url;

        $('#oxyModalStoryTitle').text(storyTitle);
        $('#oxyModalStoryText').html(storyText);
        $('#oxyModalStoryText>div').prepend('<img id="oxyModalStoryImg" style="float: right;" src="' + storyImg + '">');

        $('#oxyStoryPresentation').modal('show');
    }
}


oxy.miscPageFunctions = function(){
	var pageTitle = $('#pageTitle h1').text();
	$('#pageTitle h1').text(pageTitle.trim())
}


//Used on Page Layouts with Tabs
function autoSelectTab() {
    if (jQuery('#bodyCol #tabs').length != 0) {
        jQuery('#bodyCol #tabs ul li a').removeClass('selected');
        jQuery('#bodyCol #tabs ul li a').each(function () {
            if (neudesic.checkURLForString(jQuery(this).attr('href'))) {
                jQuery(this).addClass('selected');
            }

        });
    };
};

jQuery(document).ready(function () {

    oxy.inDesignMode = document.forms[MSOWebPartPageFormName].MSOLayout_InDesignMode.value;

    // oxy.loadTopNav();
    jQuery("http://www.oxy.com/_catalogs/masterpage/oxy/js/ul#topNavList li .sub").css({ 'opacity': '0' }); //Fade sub nav to 0 opacity on default
    jQuery("ul#topNavList li").hoverIntent(hoverConfig); //Trigger Hover intent with custom configurations

    oxy.loadMegaMenus();
    neudesic.handleWinPhoneIE();
    autoSelectTab();
    neudesic.writeToConsole("Starting up");

    oxy.jCarouselPerPageControl.init();

    oxy.setTab(); oxy.rptPageLoadTime("setTab");
    oxy.setCurGridManageChange(); //Note that this (currently 29-Jan) loads and displays the Home Tab carousel

    oxy.bldStockTicker(); oxy.rptPageLoadTime("bldStockTicker");

    $(window).resize(function () {
        /* Fade logo on Home Page
		*/
        if (oxy.onHomeTab) {
            $('.siteLogo_Oxy img').toggle(false); //New
        }

        if (this.resizeTO) clearTimeout(this.resizeTO);
        this.resizeTO = setTimeout(function () {
            $(this).trigger('resizeEnd');
        }, 500);
    });

    $(window).bind('resizeEnd', function () {
        oxy.setCurGridManageChange(); oxy.rptPageLoadTime("setCurGridManageChange");
    });

    oxy.clearUnusedCallouts(); oxy.rptPageLoadTime("clearUnusedCallouts");

    oxy.formatTopSrchBox(); oxy.rptPageLoadTime("formatTopSrchBox")

    oxy.attachPhoneSearchButtonEventHandlers(); oxy.rptPageLoadTime("attachPhoneSearchButtonEventHandlers");
    oxy.attachSwipeEventsToCarousels(); oxy.rptPageLoadTime("attachSwipeEventsToCarousels");
    oxy.attachMiscEvents(); oxy.rptPageLoadTime("attachMiscEvents");
    oxy.attachVideoLinkActions(); oxy.rptPageLoadTime("attachVideoLinkActions");
    oxy.attachStoryLinkActions(); oxy.rptPageLoadTime("attachStoryLinkActions");
    oxy.loadTopNav(); oxy.rptPageLoadTime("loadTopNav");
	
	oxy.miscPageFunctions();
	
    oxy.rptPageLoadTime("Page Loaded");

});
