var oxy = oxy || {};

oxy.displayNewsArticles = function () {
    //#region Configuration variables
    var year = 2015; // Current year. Previous years are calculated based on this. Use this statement to get the current year: year=(new Date()).getFullYear();
    var articleSite = "/news";
    var articleLibrary = "articles";

    //#endregion

    //#region Function variables
    var newItem = "";
    var maxSummaryChars = 270; //Default setting. For Home page.
    var addHRThisSection = true; //Override below as needed
    var divIDIndexer = 0;
    var RestUrl = "";
    //#endregion 

    //#region Date variables
    var dayNames = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
    var mthNames = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
    //#endregion

    //#region Filter variables
    // The following variables support filtering in the web part. The default is no filter.
    // Typically the filter will be based on the current year.
    //EXAMPLE to get the current year: filterStr = "&$filter=OxyNewsArticleYear eq " + year;
    var maxArticlesToList = 3;
    var filterStr = "";
    //#endregion

    //#region HTML String variables
    var newsArticleHTML = "";
    var newsArticleHTML_Accordion = "\
                    <div class='oxyNewsFeedDate'>{pubdate}</div>\
                    <div class='accordion-group'> \
                        <div class='accordion-heading'> \
                            <h2 class='oxyNewsFeedTitle'> \
                                <a href='#oxyNewsDetailDivNum{divIDIndexer}' data-parent='#oxyNewsArticlesContainer' data-toggle='collapse' class='accordion-toggle'>\
                                    {title} \
                                </a> \
                            </h2>\
                        </div> \
                        <div class='accordion-body collapse' id='oxyNewsDetailDivNum{divIDIndexer}'> \
                            <div class='description accordion-inner'>\
                                <article class='oxyNewsFeedBody'>{summary}</article>\
                                <div class='oxyReadMoreLink'><a href='http://www.oxy.com/News/Pages/Article.aspx?Article={id}'>Read More</a></div>\
                            </div>\
                        </div> \
                    </div> \
                ";
    //#endregion

    function bldRestURL() {
        RestUrl = articleSite + "/_api/web/lists/getbytitle('" + articleLibrary + "')/items/?";
        RestUrl += "$select=LinkFilenameNoMenu,OxyNewsArticleTitle,OxyNewsArticlePubDate,OxyNewsArticleSummary,OxyNewsArticleCreatedDate,OxyNewsArticleYear";
        RestUrl += "&$orderby=OxyNewsArticleCreatedDate desc";
        RestUrl += filterStr;
        RestUrl += "&$top=" + maxArticlesToList;
    }

    // This function sets parameters for the web part specific to the page. Modify the settings per page and add to new pages here.
    function startMeUp() {
    	var urlString = window.location.href.toLowerCase();

        //#region Investor Page settings
        if ( (/\/investors/i).test(urlString) ) {
            maxArticlesToList = 4;
            maxSummaryChars = 1000; //Show more text on the Investor page because it collapses.

            $('#oxyNewsArticlesContainer').css('marginTop', '-25px');
            $('#oxyNewsArticlesContainer').addClass('accordion'); //Top level DIV needs to have this class

            $('#oxyNewsArticlesContainer').append("<h3>&nbsp;&nbsp;Latest News</h3>"); //Append header for the IR web part

            newsArticleHTML = newsArticleHTML_Accordion;
        }
            //#endregion 

            //#region News Site Home Page settings
        else if ( (/\/news\/|news$/i).test(urlString) ) {
            maxArticlesToList = 999;
            maxSummaryChars = 1000; //Show more text on the Accordion page because it collapses.

            $('#oxyNewsArticlesContainer').css('marginTop', '15px');
            $('#oxyNewsArticlesContainer').addClass('accordion'); //Top level DIV needs to have this class

            newsArticleHTML = newsArticleHTML_Accordion;

            $('#oxyNewsArticleMoreNewsButton').remove(); //Don't need the link on the News page

            var yrPage = "";
            var yrPageRE = /pages\/news(.)\.aspx/i;
            var yearDelta = 0;

            yrPage = (yrPageRE.exec(window.location.href)); //Execute the Reg Expression defined above to get the year delta
            if (yrPage) {
                yearDelta = yrPage[1]; //If the reg exp test returned anything, (i.e. the page is NEWSn) then get it for the year delta
            }
            filterStr = "&$filter=OxyNewsArticleYear eq " + (year - yearDelta);
        }
            //#endregion 

            //#region Home Page settings
        else {
            maxArticlesToList = 3;
            addHRThisSection = true; //For example purposes. The default is true.
            $('#oxyNewsArticlesContainer').css('marginTop', '-25px');

            newsArticleHTML = "\
                    <h2 class='oxyNewsFeedTitle'>{title}</h2>\
                    <div class='description'>\
                        <div class='oxyNewsFeedDate'>{pubdate}</div>\
                        <article class='oxyNewsFeedBody'>{summary}</article>\
                        <div><a href='http://www.oxy.com/News/Pages/Article.aspx?Article={id}'>Read More</a></div>\
                    </div>\
                ";
        }
        //#endregion

        bldRestURL();
    }

    function bldAndAddItem(title, pubDate, summary, id, createdDate, addHR) {
        var crtDate = new Date(createdDate);
        var pubDateStr = dayNames[crtDate.getDay()] + ", " + mthNames[crtDate.getMonth()] + " " + crtDate.getDate() + ", " + crtDate.getFullYear();
        summary = summary.replace("read more", ""); //alert(summary);
        newItem = newsArticleHTML;
        newItem = newItem.replace("{title}", title);
        newItem = newItem.replace("{pubdate}", pubDateStr);
        newItem = newItem.replace("{summary}", summary);
        //newItem = newItem.replace("{summary}", $(summary).text().substring(0, maxSummaryChars) + "...");
        //newItem = newItem.replace("{summary}", summary.substring(0, maxSummaryChars) + "...");
        newItem = newItem.replace("{id}", id);
        newItem = newItem.replace(/{divIDIndexer}/g, divIDIndexer++); //Unique DIV id if present in the HTML string

        neudesic.writeToConsole("News Item: ");
        neudesic.writeToConsole(newItem);

        $('#oxyNewsArticlesContainer').append(newItem);
        if (addHR) {
            $('#oxyNewsArticlesContainer').append("<hr>");
        }

    }


    function getListItems_AddToWebPart() {;
        var addHR = true;
        var writeRESTResultsToConsole = function (item) {
            neudesic.writeToConsole("News Item");
            neudesic.writeToConsole("File Name: " + item.LinkFilenameNoMenu);
            neudesic.writeToConsole("Title: " + item.OxyNewsArticleTitle);
            neudesic.writeToConsole("PubDate: " + item.OxyNewsArticlePubDate);
            neudesic.writeToConsole("Summary: " + item.OxyNewsArticleSummary);
            neudesic.writeToConsole("Year: " + item.OxyNewsArticleYear);
        };
        neudesic.writeToConsole("RestURL: " + RestUrl);
        $.ajax({
            url: RestUrl,
            method: "GET",
            headers: { "Accept": "application/json; odata=verbose" },
            success: function (data) {
                neudesic.writeToConsole(data.d.results.length);
                if (data.d.results.length === 0) {
                    $('#oxyNewsArticlesContainer').text("No News articles were found for the current year.");
                }
                $.each(data.d.results, function (index, item) {
                    if (typeof console != "undefined") { writeRESTResultsToConsole(item) };
                    addHR = (addHRThisSection && index < (data.d.results.length - 1));
                    bldAndAddItem(item.OxyNewsArticleTitle, item.OxyNewsArticlePubDate, item.OxyNewsArticleSummary, item.LinkFilenameNoMenu, item.OxyNewsArticleCreatedDate, addHR);

                });

            },
            error: function (data) {
                neudesic.writeToConsole(("Failed to load Hero Carousel items for this query: " + _spPageContextInfo.webServerRelativeUrl + RestUrl), "error");
            }
        });
    }

    startMeUp();
    getListItems_AddToWebPart();


}();
