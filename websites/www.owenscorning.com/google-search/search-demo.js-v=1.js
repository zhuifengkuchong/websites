$(document).ready(function() { 
    function isIE () {
  		var myNav = navigator.userAgent.toLowerCase();
  		return (myNav.indexOf('msie') != -1) ? parseInt(myNav.split('msie')[1]) : false;
	}
	var hash = location.hash.substr(1);

	//take off the training wheels - 4-6-15 RWB
	//if (hash == null || hash != 'newsearch'){
		//do nothing

	//}

	var gsaURL = 'http://d.oc-search.com/';
	//if(location.hostname == 'http://www.owenscorning.com/google-search/www.owenscorning.com') {
  		gsaURL = 'http://oc-search.com/';
	//}

	if (isIE() == 8) {
		$("#html5SearchButton").prop("onclick", null);
		$("#mobileSearchButton").prop("onclick", null);

	    $("#html5SearchButton").on('click', function(e) {
	    	var searchText = $('#ctl00_ctl00_masterHeader_searchTextbox').val();
	                
	        if (searchText === "")
	            return false;

	        var isValid = checkForSplCharacters(searchText);
	        if (isValid) {
	           	var path = gsaURL;
	           	path += '/search?q=';
	            path += searchText;
	            path += '&client=oc&output=xml_no_dtd&proxystylesheet=oc';
	            window.open(path, "_self");
	            return false;
	        } else {
	            alert("Search Text Contains Invalid Characters.");
	            return false;
	       	}
    	});
	}

	else {
    	$("head").append('<link rel="stylesheet" href="Unknown_83_filename"/*tpa=http://www.owenscorning.com/google-search/google-search/style-search.css*/ type="text/css" />');

	    var proconnect = document.querySelectorAll('.additional-meta-nav');
	    proconnect[0].parentNode.removeChild(proconnect[0]);
	    var menu = document.querySelectorAll('.meta-list');
	    menu[0].parentNode.removeChild(menu[0]);
	    $("#html5SearchButton").remove();
	    $("#ctl00_ctl00_mobileDataContentPlaceHolder_mobileContentPlaceHolder_homePageGlobalNavigationMobileMenu_mobileSearchPanel").remove();

	    $("#ctl00_ctl00_masterHeader_searchTextbox").attr("placeholder", "What can we help you find today?");
	    $("#ctl00_ctl00_masterHeader_searchTextbox").attr("autocomplete", "off");
	   	$("#ctl00_ctl00_masterHeader_searchTextbox").attr("autocorrect", "off");
	  	$("#ctl00_ctl00_masterHeader_searchTextbox").attr("autocapitalize", "off");
	    var buttonTxt = '<button id="html5SearchButton"><img src="Unknown_83_filename"/*tpa=http://www.owenscorning.com/google-search/google-search/img/search-icon-md.png*/ id="search-icon" /></button>';
	    $(buttonTxt).insertAfter("#ctl00_ctl00_masterHeader_searchTextbox");
	    var menuTxt = '<ul class="l-inline hide-for-med meta-list"><li><a href="http://www.owenscorning.com/contact-us/" title="Contact Us">Contact Us</a></li><li><a href="http://jobs.owenscorningcareers.com/" title="Careers" target="_blank">Careers</a></li><li><a href="http://sustainability.owenscorning.com/home.aspx" title="Sustainability" class="is-green" target="_blank">Sustainability</a></li></ul>';
	    $(menuTxt).insertBefore(".standard-meta-nav");
	    var listTxt = '<div id="list"></div>';
	    $(listTxt).insertAfter(".standard-meta-nav");
	    var mobileSearchTxt = '<div id="ctl00_ctl00_mobileDataContentPlaceHolder_mobileContentPlaceHolder_homePageGlobalNavigationMobileMenu_mobileSearchPanel" class="mobile-search"><input name="ctl00$ctl00$mobileDataContentPlaceHolder$mobileContentPlaceHolder$homePageGlobalNavigationMobileMenu$mobileSearchTextbox" type="text" id="ctl00_ctl00_mobileDataContentPlaceHolder_mobileContentPlaceHolder_homePageGlobalNavigationMobileMenu_mobileSearchTextbox" placeholder="http://www.owenscorning.com/google-search/search owenscorning.com" autocomplete="off" autocorrect="off" autocapitalize="off" style="margin: 0px auto 0px auto; padding: 4px 0 0 5px; width: 85%;" /><button id="mobileSearchButton"><i class="icon-search" /></button></div>';
	    $(mobileSearchTxt).insertBefore(".mobile-primary-nav");
	    var proconnectTxt = '<div class="additional-meta-nav hide-for-med proconnect"><ul class="l-inline right"><li><a href="http://www.owenscorning.com/ProConnect.html" title="Proconnect" class="proconnect">Login to<br>ProConnect<sup>TM</sup> Resource Center<i class="icon-double-angle-right"></i></a></li></ul></div>';
	    $(proconnectTxt).insertBefore("ul.meta-list");	

	    $('http://www.owenscorning.com/google-search/div.icon').click(function() {
	        $('input#ctl00_ctl00_masterHeader_searchTextbox').focus();
	    });      

	    function search() {
	        var query_value = $('input#ctl00_ctl00_masterHeader_searchTextbox').val();
	        var query_plus = query_value.replace(" ", "+");
	        var dir = gsaURL;
	        dir += "/suggest?token=";
	        dir += query_plus;
	        dir += "&max_matches=10&use_similar=0&client=oc";
	        if (query_plus !== '') {
	            $.ajax({
	                url: dir,
	                type: 'GET',
	                crossDomain: true,
	                dataType: 'jsonp',
	                success: function(jsonp) { 
	                    var num_rslt = 0;
	                    var tag = '<ul id="results">';
	                    $.each(jsonp, function(index) {
	                        if (num_rslt > 4) {
	                            return false;
	                        }
	                        var plus_str = jsonp[index].replace(" ", "+");
	                        tag += '<li onclick="javascript:window.open(';
	                        tag += "'";
	                        tag += gsaURL;
	                        tag += '/search?q=';
	                        tag += plus_str;
	                        tag += '&client=oc&output=xml_no_dtd&proxystylesheet=oc';
	                        tag += "', '_self'";
	                        tag += ');">';
	                        tag += '<img src="Unknown_83_filename"/*tpa=http://www.owenscorning.com/google-search/google-search/img/search-icon-md.png*/ id="find-icon" />';
	                        tag += jsonp[index];
	                        tag += '</li>';
	                        num_rslt++;
	                    });
	                    tag += '</ul>';
	                    $('#list').html(''); 
	                    if (num_rslt != 0) {
	                    	$(tag).appendTo('#list');
	                    	$("ul#results").fadeIn();
	                    	$('h4#results-text').fadeIn(); 
	                    	$('#list').show(); 
	                    	items = $("ul#results li");
	                    	currentItem = -1;
	                    }   
	                }
	            });
	        }
	    }

	    $("#html5SearchButton").on('click', function(e) {
	        var searchText = $('#ctl00_ctl00_masterHeader_searchTextbox').val();
	                
	        if (searchText === "")
	            return false;

	        if ($("ul#results li").hasClass("selected")) {
	        	$(".selected").click();
	        	return false;
	        }

	        var isValid = checkForSplCharacters(searchText);
	        if (isValid) {
	           	var path = gsaURL;
	           	path += '/search?q=';
	            path += searchText;
	            path += '&client=oc&output=xml_no_dtd&proxystylesheet=oc';
	            window.open(path, "_self");
	            return false;
	        } else {
	            alert("Search Text Contains Invalid Characters.");
	            return false;
	       	 }
	    });

	    $("#mobileSearchButton").on('click', function(e) {
	        var searchText = $('#ctl00_ctl00_mobileDataContentPlaceHolder_mobileContentPlaceHolder_homePageGlobalNavigationMobileMenu_mobileSearchTextbox').val();
	                
	        if (searchText === "")
	            return false;

	        var isValid = checkForSplCharacters(searchText);
	        if (isValid) {
	            var path = gsaURL;
	            path += '/search?q=';
	            path += searchText;
	            path += '&client=oc&output=xml_no_dtd&proxystylesheet=oc';
	            window.open(path, "_self");
	            return false;
	        } else {
	            alert("Search Text Contains Invalid Characters.");
	            return false;
	        }
	    });

	   	$("#ctl00_ctl00_mobileDataContentPlaceHolder_mobileContentPlaceHolder_homePageGlobalNavigationMobileMenu_mobileSearchTextbox").on('keyup', function(e) {
	        var searchText = $('#ctl00_ctl00_mobileDataContentPlaceHolder_mobileContentPlaceHolder_homePageGlobalNavigationMobileMenu_mobileSearchTextbox').val();
	                
	        if (searchText === "")
	            return false;

	        if (e.which == 13)
	        {
		        var isValid = checkForSplCharacters(searchText);
		        if (isValid) {
		            var path = gsaURL;
		            path += '/search?q=';
		            path += searchText;
		            path += '&client=oc&output=xml_no_dtd&proxystylesheet=oc';
		            window.open(path, "_self");
		            return false;
		        } else {
		            alert("Search Text Contains Invalid Characters.");
		            return false;
		        }
		    }
	    });

	    $("div#list").on('mouseover', "li", function(e) {
	    	$("ul#results li").removeClass("selected");
	    	currentItem = $(e.target).index();
	    	$(e.target).addClass("selected");
	    });

	    $("div#list").on('mouseout', "li", function(e) {
	    	$("ul#results li").removeClass("selected");
	    	currentItem = -1;
	    });

	    function checkForSplCharacters(keyEntered) {
	        var regex = new RegExp("^[^<>%$#%@*=?,|]*$");
	        if (!regex.test(keyEntered))
	            return false;
	        else
	            return true;
	    }

	    $("input#ctl00_ctl00_masterHeader_searchTextbox").on('keyup', function(e) {
	        // Set Timeout
	        clearTimeout($.data(this, 'timer'));

	        // get keycode of current keypress event
	    	var code = (e.keyCode || e.which);

	    	// do nothing if it's an arrow key
	   		if(code == 37 || code == 38 || code == 39 || code == 40) {
	        	return;
	   		}

	   		if(code == 27) {
	        	$("ul#results").fadeOut();
	            $('#list').hide();
	            $('ul#results').html('');
	            $("ul#results li").removeClass("selected");
	            return;
	   		}

	        // Set Search String
	        var search_string = $(this).val();
	        var string_len = search_string.length;

	        // Do Search
	        if (string_len < 2) {
	            $("ul#results").fadeOut();
	            $('h4#results-text').fadeOut();
	            $('#list').hide();
	            $('ul#results').html('');
	            $('ul#results').hide();
	        } else {
	        	if (string_len > 35) {
	        		return;
	        	}
	            $('#list').hide();
	            $('#list').html('');
	            $('ul#results').html('');
	            $(this).data('timer', setTimeout(search, 400));
	        }
	    });

	    $('body, html').on('click', function(e) {
	        if(!$(e.target).is($("ul#results"))) {
	        	$("ul#results").fadeOut();
	            $('#list').hide();
	            $('ul#results').html('');
	            $("ul#results li").removeClass("selected");
	            $("#ctl00_ctl00_masterHeader_searchTextbox").val('');
	            $("#ctl00_ctl00_masterHeader_searchTextbox").attr("placeholder", "What can we help you find today?");
	        }
	    });

	    $(window).resize(function() {
	        $("ul#results").fadeOut();
	        $('#list').hide();
	        $('ul#results').html('');
	    });

		var currentItem = -1;
		/* Inside the event handler */
		$(document).keydown(function(event) {
			var items = $("ul#results li");

			if (event.keyCode === 40) {
				// Move to the next element
		        currentItem++;
		        // Return to first item
		        if (currentItem >= items.length-1) 
		        	currentItem = -1;
		    } else if (event.keyCode === 38){
		        // Move to the previous element
		        currentItem--;
		        // Move to last item
		        if(currentItem < -1) 
		        	currentItem = items.length - 2;
		    }
		    else
		    	return;

		    // Remove selected class from all elements
		    items.removeClass("selected");
		    // Add selected class to current item
		   	items.eq(currentItem).addClass("selected");
		});
	}
});
