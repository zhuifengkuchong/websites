$(document).ready(function(){
	
	var form = $("#newsletterForm");
	var formLink = $("#shortFormButton");
	var errorField = $(".errorMessage p");
	
	var fieldA = $("#fieldA");
	var fieldB = $("#fieldB");
	var fieldC = $("#fieldC");
	var fieldD = $("#fieldD");
	
	var captchaInput = $(".form_captcha_input").children('input');
	var captchaLabel = $(".section.captcha").children().children().children().children('label');
	
	var newsletteritems = $('#newsletterForm .form.checks').children().children().children('tr').length;
	var newsServlet = $('#newsletterForm').attr('action');
	var fieldArray = [fieldA, fieldB, fieldC, fieldD];
	var checkboxArray = new Array();
	var requiredArray = new Array();
	
	var errorMSG = $("#errorMessage").val();
	var customError = $("#customError").val();
	
	$(".modalContent.newsLetter.thankyou").hide();
		
	//POPULATE ARRAY OF REQUIRED INPUT FIELDS
	/*
	jQuery.each(fieldArray, function(i, val) {
		if(val.hasClass("required")){
			requiredArray.push(val);
			val.parent().parent().children().children().append('<span class="errorShow"> *</span>');
		}	
    });*/
	
	jQuery.each(fieldArray, function(i, val) {
        
        if($(val).hasClass("required")){
        	requiredArray.push($(val));
            var currentLabel = $(val).parent().parent().children().children('label');
            $(currentLabel).append('<span class="errorShow"> *</span>');
        }
        
	});

	

	
	$('#req').append('<span class="errorShow"> *</span>');
	
	
	//POPULATE ARRAY OF CHECKBOXES
	$('#newsletterForm .form.checks').children().children().children('tr').each(function(index) {
		checkboxArray.push($(this).children('.option').children('input').attr('id'));
	});

	formLink.click(function() {
 	
		$(".modalContent.newsLetter").show();
		$(".modalContent.newsLetter.thankyou").hide();
		
       errorField.text("");
	   errorField.hide();
	   $(captchaLabel).removeClass('errorShow');
	   
		//$("#newsletterForm .thankYou").hide();
		if(!checkA() || !checkB() || !checkC() || !checkD()|| !checkCheckbox() || !checkCaptcha()) {// 
		
			errorField.show();
			if(!errorMSG){
				errorField.text("Please complete fields marked as required");
			}
			else{
				errorField.text(errorMSG);
			}
		}
		else{
			processForm();
		}
    });
	
	function processForm(){
		
		var newsData = $("#newsletterForm").serialize();
		//console.log("NEWS DATA:"+newsData);
		
		$.ajax({
			type: "POST",
			url: newsServlet,
			success: function(){
				//console.log("Success");
				_gaq.push(['_trackEvent', 'Newsletter Signup', 'Submit', 'Success']);
				errorField.text("");
				$(".modalContent.newsLetter").hide();
				$(".modalContent.newsLetter.thankyou").show();
			  },
			error: function(jqXHR, exception) {
				if (jqXHR.status === 0) {
					//console.log('Not connect.\n Verify Network.');
				} else if (jqXHR.status == 404) {
					//console.log('Requested page not found. [404]');
				} else if (jqXHR.status == 500) {
					
					errorField.show();
					$(captchaLabel).addClass('errorShow');
					
					if(!customError){
						errorField.text("Invalid Captcha");
					}
					// Take error from 
					else{
						errorField.text(customError);
					}
				} else if (exception === 'parsererror') {
					//console.log('Requested JSON parse failed.');
				} else if (exception === 'timeout') {
					//console.log('Time out error.');
				} else if (exception === 'abort') {
					//console.log('Ajax request aborted.');
				} else {
					//console.log('Uncaught Error.\n' + jqXHR.responseText);
				}
			},
			data: newsData
			}).done(function( msg ) {
			//console.log("Servlet Message:"+msg);
			//console.log("Data2:"+newsData);
		});
		
	}
	
	/*
	Before 11/8 integration
	function processForm(){
		//sterling.breckenridge@acquitygroup.com
		var newsData = $("#newsletterForm").serialize();
		console.log("NEWS DATA:"+newsData);
		$.ajax({
			type: "POST",
			url: newsServlet,
			data: newsData
			}).done(function( msg ) {
			//console.log("Servlet Response:"+msg);
			$(".modalContent.newsLetter.form").hide();
			$(".modalContent.newsLetter.thankyou").show();
		});
		
	}
	*/
	function checkA(){
		
		if(!fieldA.hasClass("required")){
			fieldA.parent().parent().removeClass('errorShow');	
			return true;
		}
		else if(fieldA.hasClass("required") && fieldA.val()!=""){
			fieldA.parent().parent().removeClass('errorShow');
			return true;
		}
		else{
			fieldA.parent().parent().addClass('errorShow');
			return false;	
		}
	}
	
	function checkB(){
		
		if(!fieldB.hasClass("required")){
			fieldB.parent().parent().removeClass('errorShow');
			return true;
		}
		else if(fieldB.hasClass("required") && fieldB.val()!=""){
			fieldB.parent().parent().removeClass('errorShow');
			return true;
		}
		else{
			fieldB.parent().parent().addClass('errorShow');
			return false;	
		}
	}
	
	function checkC(){

		var emailStr = fieldC.val();

		if(!fieldC.hasClass("required")){
			fieldC.parent().parent().removeClass('errorShow');
			return true;
		}
		else if(fieldC.hasClass("required") && emailStr.indexOf('@')!=-1 && emailStr.indexOf('.')!=-1){
			fieldC.parent().parent().removeClass('errorShow');
			return true;
		}
		else{
			fieldC.parent().parent().addClass('errorShow');
			return false;	
		}
	}
	
	function checkD(){
		
		if(!fieldD.hasClass("required")){
			fieldD.parent().parent().removeClass('errorShow');
			return true;
		}
		else if(fieldD.hasClass("required") && fieldD.val()!=""){
			fieldD.parent().parent().removeClass('errorShow');
			return true;
		}
		else{
			fieldD.parent().parent().addClass('errorShow');
			return false;	
		}
	}
	
	// VALIDATE AT LEAST ONE CHECKBOX WAS SELECTED
	function checkCheckbox(){
		var pass=0;
		jQuery.each(checkboxArray, function(j, checkVal) {
			
			var temp = "#"+checkVal;
			if($(temp).attr("checked")=="checked"){
				pass+=1;
			}	
    	});
		
		if(pass>0){
			$('.form.checks .newsOption').removeClass('errorShow');
			return true;
		}
		else{
			$('.form.checks .newsOption').addClass('errorShow');
			return false;
		}
	}
	
	function checkCaptcha(){
		
		if(captchaInput.val()!=""){
			$(captchaLabel).removeClass('errorShow');
			return true;
		}
		else{
			$(captchaLabel).addClass('errorShow');
			return false;
		}
		
	}
	
});