var tic_PageGlobals =
{
    internal_globals: new Object(),

    Add:
        function(varName, varValue) 
        {
            this.internal_globals[varName] = varValue ;
        },
    Get:
        function(varName)
        {
            return this.internal_globals[varName] ;
        }        
} ;

var tic_FormUtilities = 
{
    HideField:
        function(ctl, includeFieldset)
        {
            while(ctl && (!ctl.className || ctl.className.indexOf("fieldwrapper")!=0))
                ctl = ctl.parentNode ;
                
            if (ctl)
                ctl.style.display = "none" ;                         
                
            if (includeFieldset && ctl.parentNode && ctl.parentNode.tagName == "FIELDSET")
                ctl.parentNode.style.display = "none" ;
        },
    ShowField:
        function(ctl, includeFieldset)
        {
            while(ctl && (!ctl.className || ctl.className.indexOf("fieldwrapper")!=0))
                ctl = ctl.parentNode ;
                
            if (ctl)
                ctl.style.display = "block" ;                         

            if (includeFieldset && ctl.parentNode && ctl.parentNode.tagName == "FIELDSET")
                ctl.parentNode.style.display = "block" ;
        }                
} ;

function SpecialFilePickerCallbackForCss(ctlName, shortFileName, longFileName)
{
    var useShortName = false ;
    if (shortFileName.toLowerCase().lastIndexOf(".css") == shortFileName.length - 4)
        useShortName = true ;

    tic_Utilities.ChangeClientStyle(longFileName) ;
    Wkst_SetAndCheckControlValue(ctlName, (useShortName?shortFileName:longFileName)) ;    
}

var tic_Utilities =
{
    AddEventListener:
        function(sourceObject, eventName, listener)
        {
            if (!sourceObject)
                return;
            if (tic_Utilities.IsIE())
                sourceObject.attachEvent('on' + eventName, listener);
            else
                sourceObject.addEventListener(eventName, listener, false);
        },
    RemoveEventListener:
        function(sourceObject, eventName, listener)
        {
            if (!sourceObject)
                return;
            if (tic_Utilities.IsIE())
                sourceObject.detachEvent('on' + eventName, listener);
            else
                sourceObject.removeEventListener(eventName, listener, false);
        },
    GetBaseID:
        function(ctlName)
        {
            return ctlName.substring(0, ctlName.lastIndexOf("_")) ;
        },
    GetTargetCtl:
        function(evt)
        {
            if (!evt)
                evt = window.event ;

            if (evt.originalTarget)
                return evt.originalTarget;
            else if (evt.srcElement)
                return evt.srcElement;
                
            return null ;                
        },

    GetQueryStringArg:
        function()
        {
            var hashValue = null ;
            if (location.hash)
                hashValue = location.hash ;
            else if (location.search)
                hashValue = location.search ;

            return hashValue ;            
        },

    IsFirefox:
        function()
        {            
            var htmlTags = document.getElementsByTagName("HTML") ; 
            if (htmlTags && htmlTags.length  > 0)
                return tic_Utilities.HasStyle(htmlTags[0], "Firefox") ;
        },

    DisableButtons:
        function()
        {
            // WARNING: USE WITH CAUTION ON LARGE SCREENS
            var buttons = document.getElementsByTagName("INPUT") ;
            for (var ii=0 ; ii<buttons.length ; ++ii)
                if (buttons[ii].type == "button" || buttons[ii].type == "submit")
                    buttons[ii].disabled = true ;
        },
    StyleArray:
        function(ctl)
        {
            if (!ctl || !ctl.className)
                return new Array() ;
                
            return ctl.className.split(" ") ;
        },
    HasStyle:
        function (ctl, style) 
        {
            var styles = tic_Utilities.StyleArray(ctl) ;
            if (!styles || !styles.length)
                return false ;
                
            for (var ii=0 ; ii<styles.length ; ++ii)
                if (styles[ii] == style)
                    return true ; 
                    
            return false ;                                    
        },
    RemoveStyle:
        function (ctl, style) 
        {
            var styles = tic_Utilities.StyleArray(ctl) ;
            if (!styles || !styles.length)
                return false ;
            
            ctl.className = "" ;    
            var retVal = false ;
            for (var ii=0 ; ii<styles.length ; ++ii)
            {
                if (styles[ii] == style)
                    retVal = true ; 
                else
                    ctl.className += ((ctl.className?" ":"") + styles[ii]) ;
            }        
            return retVal ;                                    
        },   
    AddStyle:
        function (ctl, style)
        {
            if (!ctl)
                return ; 
            if (tic_Utilities.HasStyle(ctl, style))
                return ;                
                
            if (!ctl.className)
                ctl.className = style ;
            else
                ctl.className += " " + style ;                
        },    
    CancelBubble:
        function(evt)
        {
            if (!evt)
                evt = window.event ;
                
            if (!evt)
                return ;                
                
            evt.cancelBubble = true ;   
            evt.returnValue = false ;    

            if (evt.stopPropagation)
                evt.stopPropagation() ;     
            if (evt.preventDefault)
                evt.preventDefault() ;

            return false ;                                     
        },              

    LoadScriptFile:
        function(fileName)
        {
            var newElt = document.createElement("script") ; 
            newElt.src = fileName ;
            newElt.type = "text/javascript" ; 
            document.getElementsByTagName("head")[0].appendChild(newElt) ;
        },

    GetWindowHeight: 
        function(minHeight)
        {
            windowHeight = window.innerHeight ; // Mozilla
            
            if (!windowHeight && document.documentElement && document.documentElement.clientHeight)
                windowHeight = document.documentElement.clientHeight ; // IE

            if (!windowHeight && document.body && document.body.clientHeight)
                windowHeight = document.body.clientHeight ; // IE
                
            if (!windowHeight || windowHeight < minHeight)
                return minHeight ;
                        
            return windowHeight ;
        },
    LeftTrim:
        function(strVal)
        {
            return strVal?strVal.replace(/^\s+/, ''):"" ;
        },        
    RightTrim:
        function(strVal)
        {
            return strVal?strVal.replace(/\s+$/, ''):"" ;
        },
    Trim:
        function(strVal)
        {
            return strVal?strVal.replace(/^\s+|\s+$/g, ''):"";
        },  

    PackageXml:
        function(tagName, data, includeCData)
        {
            var retVal = tic_Utilities.MakeXmlStartTag(tagName, includeCData) ;
            retVal += data ;
            retVal += tic_Utilities.MakeXmlEndTag(tagName, includeCData) ;
            
            return retVal ;
        },
    XmlWithAttributes:
        function(tagName)
        {
            var retVal = "<" + tagName ;
            for (var ii=1 ; ii<arguments.length ; ii+=2)
                retVal += " " + arguments[ii] + "='" + EscapeSingleQuotes(arguments[ii+1]) + "'" ;
                
            return retVal + "/>" ;                            
        },        
    MakeXmlStartTag:
        function(tagName, includeCData)
        {
            if (!tagName || tagName.length == 0)
                return "" ;

            return "<" + tagName + ">" + (includeCData?"<![CDATA[":"");                 
        },
    MakeXmlEndTag:
        function(tagName, includeCData)
        {
            if (!tagName || tagName.length == 0)
                return '' 
            
            return (includeCData?"]]>":"") + "</" + tagName + ">" ;                 
        },
    ShowHideById:
        function(ctlID, doShow)
        {
            var ctl = document.getElementById(ctlID) ; 
            if (ctl)
                ctl.style.display = (doShow?"block":"none") ;
        },
    ToggleShowHide:
        function(ctlID)
        {
            var ctl = document.getElementById(ctlID) ; 
            if (ctl)
                ctl.style.display = (ctl.style.display=="none"?"block":"none") ;
        },        
    ForceVisible:
        function(id)
        {
            var item = document.getElementById(id) ;
            if (!item)
                return ; 
            item.style.display = "block" ;
            item.style.visibility = "visible" ;
        },      
                    
    ChangeClientStyle:    
        function ChangeClientStyle(newStyleSheet, loadCompleteFunction)
        {
            if (!newStyleSheet || newStyleSheet.lastIndexOf(".css") != (newStyleSheet.length - 4))
                return ;

            var clientStyle = document.getElementById("clientCSS") ;
            if (clientStyle)
                clientStyle.parentNode.removeChild(clientStyle) ;

            var newStyle = document.createElement("link") ;
            newStyle.id = "clientCSS" ; 
            newStyle.setAttribute("href", newStyleSheet, 0);		
            newStyle.setAttribute("type", "text/css");
            newStyle.setAttribute("rel", "stylesheet", 0);	

            var oHead = document.getElementsByTagName("head")[0];	                
            oHead.appendChild(newStyle);

            if (loadCompleteFunction && newStyle.addEventListener)
            {
                newStyle.addEventListener("onload", loadCompleteFunction, true) ;
            }                
            else if (loadCompleteFunction)
            {            
                newStyle.onload = loadCompleteFunction ;
            }
        },
    ToggleContextHelp:
        function (regionName)
        {
            var area = document.getElementById(regionName) ;
            if (area)
                area.style.display = ((area.style.display=="block")?"none":"block") ;
        }                
} ; 

function StringBuilder()
{
    this.strings = new Array(""); // puts a string as the first array element so that + is strcat
    for (var ii=0 ; ii<arguments.length ; ++ii)
        if (arguments[ii])
            this.strings.push(arguments[ii]) ;
}

StringBuilder.prototype.Append = function()
{
    for (var ii=0 ; ii<arguments.length ; ++ii)
        if (arguments[ii])
            this.strings.push(arguments[ii]) ;
}

StringBuilder.prototype.ReturnAndEmpty = function()
{
    var retString = this.strings.join("") ;
    this.strings = null ;
    return retString ;
}

StringBuilder.prototype.IsEmpty = function()
{
    return this.strings.length == 1 ;
}