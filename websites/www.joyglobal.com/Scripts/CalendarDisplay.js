
function Cal_PackageQueryStringData(calID)
{
    var calTable = document.getElementById(calID);
    var blockID = calTable.getAttribute("blockID");

    var params = "&DateRange=" + calTable.getAttribute("dateRange");
    var selectedDate = new Date(calTable.getAttribute("selectedDate"));
    params += "&Date="+ String(selectedDate.getMonth() + 1) + "/" + String(selectedDate.getDate()) + "/" + String(selectedDate.getFullYear());
    params += "&Keyword=" + document.getElementById("keywordFilter" + blockID).value ;
    params += "&Classifications=" + Cal_FormatClassifications(calTable, blockID);
    if (document.getElementById("Calendar" + blockID + "_EventID"))
        params += (params.length ? "&EventID=" : "EventID=") + document.getElementById("Calendar" + blockID + "_EventID").value;

    return params ;
}

function CalendarBlock_RemoveExistingCalendarParams(params)
{
    if (!params)
        return "";

    var retVal = "";
    var paramArray = params.split("&");
    for (var ii = 0; ii < paramArray.length; ii++)
        if (paramArray[ii].indexOf("DateRange") == 0 ||
            paramArray[ii].indexOf("Date") == 0 ||
            paramArray[ii].indexOf("Keyword") == 0 ||
            paramArray[ii].indexOf("Classifications") == 0 ||
            paramArray[ii].indexOf("EventID") == 0)
            ;
        else
            retVal += (retVal.length ? "&" : "") + paramArray[ii];

    return retVal;
}

function Cal_TableClick(ctl, evt)
{
    var targetCell = tic_Utilities.GetTargetCtl(evt) ;
    if (tic_Utilities.HasStyle(targetCell, "otherMonth") || targetCell.tagName != "TD" || targetCell.parentNode.tagName != "TR" || targetCell.parentNode.parentNode.tagName != "TBODY")
        return ;
        
    var selectedDate = new Date(ctl.getAttribute("currentMonth")) ;
    selectedDate.setDate(selectedDate.getDate() + Number(targetCell.innerHTML) - 1) ;

    ctl.setAttribute("selectedDate", selectedDate) ; 
    Cal_ResetSelection(ctl) ; 
}

function Cal_ResetSelection(calTable)
{
    var firstDay=-1, lastDay=-1, selectedDay=-1 ;
    var currentMonth = new Date(calTable.getAttribute("currentMonth")) ;
    var selectedDate = new Date(calTable.getAttribute("selectedDate")) ;
    if (currentMonth.getMonth() == selectedDate.getMonth() &&
        currentMonth.getFullYear() == selectedDate.getFullYear())
    {
        selectedDay = selectedDate.getDate() ;
        switch (calTable.getAttribute("dateRange"))
        {
            default:
            case "day":
                firstDay = lastDay = selectedDay ;
                break ;
            case "month":
            case "year":
                firstDay = 1 ;
                lastDay = 31 ;
                break ;
            case "week": 
                firstDay = selectedDay - selectedDate.getDay() ;
                lastDay  = selectedDay + 6 - selectedDate.getDay() ;
                break ;
        } 
    }   

    var dateRows = Cal_GetDateRows(calTable) ;
    var searchRow = 0, searchCol = 0 ;
    for (searchCol=0 ; searchCol<7 ; ++searchCol)
        if (dateRows[searchRow][searchCol].className != "otherMonth")
            break ;
                
    for (var rows=searchRow; rows<6 ; ++rows)
        for (var cols=(rows?0:searchCol); cols<7 ; ++cols)
        {
            var cell = dateRows[rows][cols] ;
            if (cell.className == "otherMonth" && calTable.getAttribute("dateRange")!="year")
                break ;
                
            if (cell.className == "otherMonth" && calTable.getAttribute("dateRange")=="year")
            {
                cell.className = "otherMonth selected" ;
                continue ;
            }
            
            var cellDate = Number(cell.innerHTML) ;                
            cell.className = "" ;
            if (cellDate >= firstDay && cellDate <= lastDay)
                cell.className = "selected" ;
            if (cellDate == selectedDay)
                cell.className += " selectedDate" ;
        }     
        
   Cal_ReloadAjaxData(calTable.id) ;                       
}

function Cal_SwitchRange(anchor, calID, range)
{
    var calTable = document.getElementById(calID) ;
    var currRange = calTable.getAttribute("dateRange") ;
    if (currRange == range)
        return ;        
        
    var container = anchor.parentNode ;
    while (container && container.tagName != "DIV")
        container = container.parentNode ;
    if (container.className != 'calendarNav')
        return ;        
        
    var anchors = container.getElementsByTagName("SPAN") ;        
    for (var ii=0 ; ii<anchors.length ; ii++)
        anchors[ii].className = "" ;        
    anchor.className = "selected" ;             
    
    calTable.setAttribute("dateRange", range) ;
    
    if (currRange == "year")
    {
        var calDate = new Date(calTable.getAttribute("currentMonth")) ;
        Cal_ResetCalendar(calTable, calDate) ;
    }
    else
        Cal_ResetSelection(calTable) ;
}

function Cal_InitCalendar(calID)
{
    var calTable = document.getElementById(calID) ;
    
    var today = new Date(), firstOfMonth = new Date() ;
    
    firstOfMonth.setDate(1) ;
    var lastOfMonth = Cal_FindLastOfMonth(new Date()) ;

    Cal_ResetCalendar(calTable, firstOfMonth) ;
}

function Cal_FindLastOfMonth(dayInMonth)
{
    var maxDaysToEnd = 31 - dayInMonth.getDate() ;
    
    var lastOfMonth = new Date() ;
    lastOfMonth.setDate(dayInMonth.getDate() + maxDaysToEnd) ; 
    while (lastOfMonth.getDate() < 10)
        lastOfMonth.setDate(lastOfMonth.getDate() - 1) ;
        
    return lastOfMonth ;            
}

function Cal_ResetCalendar(calTable, firstOfMonth) 
{    
    var becauseMacsDontPlayFair = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"] ;
    Cal_GetHeaderCell(calTable).innerHTML = becauseMacsDontPlayFair[firstOfMonth.getMonth()] + " " + firstOfMonth.getFullYear() ;

    var dateRows = Cal_GetDateRows(calTable) ;
    var dateToFill = new Date(firstOfMonth.toString()) ;
    dateToFill.setDate(firstOfMonth.getDate() - (firstOfMonth.getDay()==0?7:firstOfMonth.getDay())) ;
    
    for (var rows=0 ; rows<6 ; ++rows)
        for (var cols=0 ; cols<7 ; ++cols, dateToFill.setDate(dateToFill.getDate() + 1))
        {
            var cell = dateRows[rows][cols] ;
            cell.innerHTML = dateToFill.getDate() ; 
            cell.className = (dateToFill.getMonth() != firstOfMonth.getMonth())?"otherMonth":"" ;
        }
        
    calTable.setAttribute("currentMonth", firstOfMonth.toString()) ;          
    Cal_ResetSelection(calTable) ;
}

function Cal_JumpSelection(selectedDate, newFirstOfMonth)
{
    var newDate = new Date(newFirstOfMonth) ;
    newDate.setDate(selectedDate.getDate()) ;
    
    while (newDate.getMonth() != newFirstOfMonth.getMonth())
        newDate.setDate(newDate.getDate() - 1) ;

    return newDate ;
}

function Cal_NextMonth(calID)
{
    var calTable = document.getElementById(calID) ;
    if (!calTable)
        return ;
    
    var calDate = new Date(calTable.getAttribute("currentMonth")) ;
    calDate.setMonth(calDate.getMonth() + 1) ;

    var selectedDate = new Date(calTable.getAttribute("selectedDate")) ;
    calTable.setAttribute("selectedDate", Cal_JumpSelection(selectedDate, calDate).toString()) ;
    
    Cal_ResetCalendar(calTable, calDate) ;    
}

function Cal_PrevMonth(calID)
{
    var calTable = document.getElementById(calID) ;
    if (!calTable)
        return ;
    var calDate = new Date(calTable.getAttribute("currentMonth")) ;
    calDate.setMonth(calDate.getMonth() - 1) ;    

    var selectedDate = new Date(calTable.getAttribute("selectedDate")) ;
    calTable.setAttribute("selectedDate", Cal_JumpSelection(selectedDate, calDate).toString()) ;
    
    Cal_ResetCalendar(calTable, calDate) ;
}

function Cal_GetDateRows(calTable)
{
    var allRows = calTable.getElementsByTagName("TR") ;     
    var retArray = new Array(6) ;
    for (var ii=0 ; ii<6 ; ++ii)
        retArray[ii] = allRows[ii+2].getElementsByTagName("TD") ;
        
    return retArray ;        
}

function Cal_GetHeaderCell(calTable) 
{
    return document.getElementById('calTitle' + calTable.getAttribute("blockId")) ; 
}

function Cal_PrevRange(calID)
{
    var calTable = document.getElementById(calID) ;
    if (!calTable)
        return ;

    var selectedDate = new Date(calTable.getAttribute("selectedDate")) ;
    switch (calTable.getAttribute("dateRange"))
    {
        default:
        case 'day':
            selectedDate.setDate(selectedDate.getDate() - 1) ;
            break ;
        case 'week':
            selectedDate.setDate(selectedDate.getDate() - 7) ;
            break ;
        case 'month':
        case 'year':
            return Cal_PrevMonth(calID) ;
            break ;
    }
    
    var firstOfMonth = new Date(selectedDate.toString()) ;
    firstOfMonth.setDate(1) ;
    calTable.setAttribute("selectedDate", selectedDate) ;
    Cal_ResetCalendar(calTable, firstOfMonth) ;
}

function Cal_NextRange(calID)
{
    var calTable = document.getElementById(calID) ;
    if (!calTable)
        return ;

    var selectedDate = new Date(calTable.getAttribute("selectedDate")) ;
    switch (calTable.getAttribute("dateRange"))
    {
        default:
        case 'day':
            selectedDate.setDate(selectedDate.getDate() + 1) ;
            break ;
        case 'week':
            selectedDate.setDate(selectedDate.getDate() + 7) ;
            break ;
        case 'month':
        case 'year':
            return Cal_NextMonth(calID) ;
            break ;
    }
    
    var firstOfMonth = new Date(selectedDate.toString()) ;
    firstOfMonth.setDate(1) ;
    calTable.setAttribute("selectedDate", selectedDate) ;
    Cal_ResetCalendar(calTable, firstOfMonth) ;
}

function Cal_Return(calID)
{
    Cal_ReloadAjaxData(calID) ;                           
}

function Cal_ClassificationCheck(calID, allID)
{
    FilterBlock_UncheckAll(allID) ;
    Cal_ReloadAjaxData(calID) ;                           
}

function Cal_ClassificationCheckAll(allCtl, calID, blockID, classID)
{
    FilterBlock_CheckAll(allCtl, blockID, classID)
    Cal_ReloadAjaxData(calID) ;                           
}

function Cal_KeywordFilter(calID)
{
    Cal_ReloadAjaxData(calID) ;    
}

function Cal_FormatClassifications(calTable, blockID)
{
    var blockPrefix = FilterBlock_MakePrefix(blockID) ; 
    var allInputs = document.getElementsByTagName("INPUT") ;

    var classArgs = "";    
    for (var ii=0 ; ii<allInputs.length ; ++ii)
    {
        var myId = allInputs[ii].id ;
    
        if (allInputs[ii].type == "checkbox" &&   // may be a filter checkbox
            allInputs[ii].checked &&              // it is checked
            myId.indexOf(blockPrefix) == 0)       // and it is mine, all mine
            classArgs += myId + "!" ;
    }
    
    return  classArgs ;   
}

function Cal_ReloadAjaxData(calID)
{
    PrintPage_StoreInfoForPrintPage("CalendarBlock", calID)

    var calTable = document.getElementById(calID) ;
    if (calTable.id == "DP_PopupCalendar")
        return ;
    
    var blockID = calTable.getAttribute("blockID") ;
    
    var selectedDate = new Date(calTable.getAttribute("selectedDate")) ;    
    
    var ajaxArgs = new Array() ;
    ajaxArgs[ajaxArgs.length] = calTable.getAttribute("docID") ;
    ajaxArgs[ajaxArgs.length] = blockID ; 
    ajaxArgs[ajaxArgs.length] = String(selectedDate.getMonth()+1) + "/" + String(selectedDate.getDate()) + "/" + String(selectedDate.getFullYear()) ; 
    ajaxArgs[ajaxArgs.length] = calTable.getAttribute("dateRange") ;
    if (document.getElementById("keywordFilter" + blockID))
        ajaxArgs[ajaxArgs.length] = document.getElementById("keywordFilter" + blockID).value ;
    else        
        ajaxArgs[ajaxArgs.length] = "" ;
    ajaxArgs[ajaxArgs.length] = Cal_FormatClassifications(calTable, blockID) ;

    document.getElementById("CalendarResults_" + blockID).className += " waiting" ;
    TitanDisplayServiceWrapper.MakeWebServiceCall("Calendar_Main" + blockID, NorthwoodsSoftwareDevelopment.Cms.WebServices.CalendarAjax.GetResultsData, ajaxArgs, Cal_AjaxComplete, [blockID], true) ;
}

function Cal_ViewCalItem(calID, itemNumber)
{
    PrintPage_StoreInfoForPrintPage("CalendarBlock", calID)

    var calTable = document.getElementById(calID) ;
    var blockID = calTable.getAttribute("blockID") ;

    var ajaxArgs = new Array() ;
    ajaxArgs[ajaxArgs.length] = calTable.getAttribute("docID") ;
    ajaxArgs[ajaxArgs.length] = blockID ; 
    ajaxArgs[ajaxArgs.length] = itemNumber ; 
    
    document.getElementById("CalendarResults_" + blockID).className += " waiting" ;
    TitanDisplayServiceWrapper.MakeWebServiceCall("Calendar_Main" + blockID, NorthwoodsSoftwareDevelopment.Cms.WebServices.CalendarAjax.GetEventItem, ajaxArgs, Cal_AjaxComplete, [blockID], true) ;    
}

function Cal_AjaxComplete(blockID, responseAsJSON, responseAsXml, responseAsText)
{
    var resultsArea = document.getElementById("CalendarResults_" + blockID) ;
    if (!resultsArea)
        return window.alert("Lost results") ;
    resultsArea.className = "CalendarResults" ;                

    resultsArea.innerHTML = responseAsJSON ;
}

function GetCursorPosition(evt) 
{    
    evt = evt || window.event;    
    var cursor = {x:0, y:0};    
    if (evt.pageX || evt.pageY) 
    {        
        cursor.x = evt.pageX;        
        cursor.y = evt.pageY;    
    }     
    else 
    {        
        cursor.x = evt.clientX +             
                    (document.documentElement.scrollLeft || document.body.scrollLeft) -             
                     document.documentElement.clientLeft;        
        cursor.y = evt.clientY +             
                    (document.documentElement.scrollTop || document.body.scrollTop) -             
                    document.documentElement.clientTop;    
    }    
    return cursor;
}         
function DP_ShowPopup(evt)
{
    var popup = document.getElementById("DP_PopupCalendarContainer") ;
    var pos = GetCursorPosition(evt)  ;
    
    popup.style.position = "absolute" ;
    popup.style.left = pos.x + "px" ;               
    popup.style.top  = pos.y + "px" ;
    popup.style.display = "block"  ;                    
}       
function DP_HidePopup()
{
    document.getElementById("DP_PopupCalendarContainer").style.display="none" ;
}
function DP_MouseOut(evt)
{
    var target = tic_Utilities.GetTargetCtl(evt) ;
    if (target != document.getElementById("DP_PopupCalendarContainer"))
        return ;
        
    DP_HidePopup() ;                                
}
function DP_EnableCalendar(inputName, evt)
{
    var calTable = document.getElementById('DP_PopupCalendar') ;
    if (calTable.getAttribute("doInit") == "1")                    
        Cal_InitCalendar('DP_PopupCalendar') ;
        
    calTable.setAttribute("openFor", inputName) ;
        
    var date = new Date() ;                
    var ctl = document.getElementById(inputName) ;
    if (SFEditFieldIsDate(inputName))
        date = SFEditParseDate(inputName) ;
        
    calTable.setAttribute("selectedDate", date) ;                

    var setDate = new Date(date) ;
    setDate.setDate(1) ;        
    Cal_ResetCalendar(calTable, setDate) ;
    
    DP_ShowPopup(evt) ;
}
function DP_TableClick(ctl, evt)
{
    var calTable = document.getElementById("DP_PopupCalendar") ;
    if (!calTable) 
        return ;
        
    var inputCtl = document.getElementById(calTable.getAttribute("openFor")) ;
    if (!inputCtl)
        return ;                

    var target = tic_Utilities.GetTargetCtl(evt) ;
    if (target.tagName == "TD" && target.className == "")
    {
        var selectedDate = new Date(ctl.getAttribute("currentMonth")) ;
        selectedDate.setDate(selectedDate.getDate() + Number(target.innerHTML) - 1) ;
    
        inputCtl.value = String(selectedDate.getMonth() + 1) + "/" + selectedDate.getDate() + "/" + selectedDate.getFullYear() ;
        
        DP_HidePopup() ;                
    }
}
