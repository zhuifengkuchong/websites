function PrintPage_PrintPage(printPageString)
{
    var params = PrintPage_FormatParams() ;
    window.open(window.location.pathname + printPageString + params) ;
}

function PrintPage_Email(subjectLine)
{
    var params = PrintPage_FormatParams() ;
    window.open("mailto:?subject=" + subjectLine + "&body=" + window.location.protocol + "//" + window.location.host + window.location.pathname + "?" + encodeURIComponent(params));
}

function PrintPage_FormatParams()
{
    var params = window.location.search;
    if (params.indexOf("?") == 0)
        params = params.substring(1, params.length);

    var blocks = window.location.titanBlocks;
    var filterParams = "", calendarParams = "";
    if (blocks != null)
    {
        for (var ii = 0; ii < blocks.length; ++ii)
        {
            if (blocks[ii].blockType != "FilterBlock")
                continue ;

            params = FilterBlock_RemoveExistingFilterBlockParams(params);
            filterParams = FilterBlock_PackageQueryStringData(blocks[ii].blockID, -1, true);
            break ; // One call handles all filter blocks on the page                           
        }
        
        for (var ii = 0; ii < blocks.length; ++ii)
        {
            if (blocks[ii].blockType != "CalendarBlock")                        
                continue ;

            params = CalendarBlock_RemoveExistingCalendarParams(params);
            calendarParams = Cal_PackageQueryStringData(blocks[ii].blockID);
            break; // As of 11-2008 we only support one calendar on the page for PrintPage            
        }
    }
    
    return params + filterParams + calendarParams;
}

function FilterBlock_RemoveExistingFilterBlockParams(params)
{
    if (!params)
        return "";

    var retVal = "";
    var paramArray = params.split("&");
    for (var ii = 0; ii < paramArray.length; ii++)
        if (paramArray[ii].indexOf("FB_Values") == 0 ||
            paramArray[ii].match(/^F\d*_/))
                ;
        else
            retVal += (retVal.length?"&":"") + paramArray[ii] ;

    return retVal;
}

function PrintPage_StoreInfoForPrintPage(blockType, blockID)
{
    var blockInfos = window.location.titanBlocks || [] ;
    blockInfos[blockInfos.length] = {blockType:blockType, blockID:blockID} ;
    window.location.titanBlocks = blockInfos ;
}

function FilterBlock_PackageQueryStringData(blockID, pageID, printPageEvent)
{
    var blockPrefix = FilterBlock_MakePrefix(blockID) ; 
    
    /* 
        If we are ajaxEnabled, we're paging so use the current checkbox information,
        If we are not ajaxEnabled and the new page num is not the current page num
            then we're paging and we should be using the original checkbox state
     */
    var useCheck ;
    if (FilterBlock_AjaxEnabled(blockPrefix) || printPageEvent)
        useCheck = true ; // Need current state of checkboxes and pageID is valid
    else if (pageID == -1)
    { // We have a submit action and we will return to page 1 with the current check
      // state
        useCheck = true ;
        pageID = 1 ;
    }
    else
        useCheck = false ; // Paging so grab the old state.
    
    var allInputs = document.getElementsByTagName("INPUT") ;
    
    var additionalArgs = new StringBuilder() ;
    var newQueryString = new StringBuilder() ;
    
    if (pageID > 0)
        additionalArgs.Append(blockPrefix + "PageNum=", pageID) ;
    for (var ii=0 ; ii<allInputs.length ; ++ii)
    {
        var myId = allInputs[ii].id ;
    
        if (allInputs[ii].type == "hidden" &&             // it might be a page number
            myId.indexOf(blockPrefix) != 0 &&             // it is not ours so we might want to keep it
            myId.indexOf("_PageNum") == (myId.length - 8))// yes, it is a page number
            additionalArgs.Append((additionalArgs.IsEmpty()?"":"&"), myId, "=", allInputs[ii].value) ;
        else if (allInputs[ii].type == "text" &&
                 myId.indexOf(blockPrefix) == 0 &&
                 useCheck)
            additionalArgs.Append((additionalArgs.IsEmpty() ? "" : "&"), myId, "=", allInputs[ii].value);
        else if (allInputs[ii].type == "text" &&
                 myId.indexOf(blockPrefix) == 0 &&
                 !useCheck)
            additionalArgs.Append((additionalArgs.IsEmpty()?"":"&"), myId, "=", allInputs[ii].defaultValue) ;
        else if (allInputs[ii].type == "text" &&      // input control
                 myId.indexOf(blockPrefix) != 0 &&    // not mine
                 (/^F\d*_/).test(myId) &&             // but matches our pattern
                 printPageEvent)
            additionalArgs.Append((additionalArgs.IsEmpty() ? "" : "&"), myId, "=", allInputs[ii].value);
        else if (allInputs[ii].type == "checkbox" &&   // may be a filter checkbox
                 useCheck && allInputs[ii].checked &&  // it is checked
                 myId.indexOf(blockPrefix) == 0)       // and it is mine, all mine
            newQueryString.Append(myId, "!") ;
        else if (allInputs[ii].type == "checkbox" &&          // may be a filter checkbox
                 !useCheck && allInputs[ii].defaultChecked && // it was checked
                 myId.indexOf(blockPrefix) == 0)              // and it is mine, all mine
            newQueryString.Append(myId, "!") ;
        else if (allInputs[ii].type == "checkbox" &&   // may be a filter checkbox  
                 allInputs[ii].checked &&              // it was checked
                 myId.indexOf(blockPrefix) == -1 &&    // it isn't mine
                 (/^F\d*_C\d*_A\d*$/).test(myId) &&    // but it is definitely a filter checkbox and it is ajax enabled
                 FilterBlock_AjaxEnabled(myId.substring(0, myId.indexOf("_") + 1)))
            newQueryString.Append(myId, "!");
        else if (allInputs[ii].type == "checkbox" &&   // may be a filter checkbox  
                 allInputs[ii].defaultChecked &&       // it was checked when we entered
                 myId.indexOf(blockPrefix) == -1 &&    // it isn't mine
                 (/^F\d*_C\d*_A\d*$/).test(myId) &&    // but it is definitely a filter checkbox and it isn't ajax, so use its default state
                 !FilterBlock_AjaxEnabled(myId.substring(0, myId.indexOf("_") + 1)))      
            newQueryString.Append(myId, "!");
        else if (printPageEvent &&                     // we're processing a PrintPage
                 allInputs[ii].type == "checkbox" &&   // it is a checkbox
                 allInputs[ii].checked &&              // it is checked
                 myId.indexOf(blockPrefix == -1) &&    // it isn't mine
                 (/^F\d*_C\d*_A\d*$/).test(myId))      // but it is definitely a filter checkbox           
            newQueryString.Append(myId, "!") ;
        else if (printPageEvent &&                              // we're processing a PrintPage
                 allInputs[ii].type == "hidden" &&             // it might be a page number
                 myId.indexOf(blockPrefix) == 0 &&             // it is mine
                 myId.indexOf("_PageNum") == (myId.length - 8))// and, yes, it is the page number
            additionalArgs.Append((additionalArgs.IsEmpty() ? "" : "&"), myId, "=", allInputs[ii].value);
    }

    return "&FB_Values=" + newQueryString.ReturnAndEmpty() + (additionalArgs.IsEmpty()?"":"&") + additionalArgs.ReturnAndEmpty() ;
}

function FilterBlock_MakePrefix(blockID)
{
    return "F" + blockID + "_" ;
}

function FilterBlock_Pager(blockID, pageID)
{
}

function FilterBlock_Submit(blockID, pageID)
{
    var params = window.location.search;
    if (params.indexOf("?") == 0)
        params = params.substring(1, params.length);

    params = FilterBlock_RemoveExistingFilterBlockParams(params);

    var blocks = window.location.titanBlocks;
    var calendarParams = "";
    if (blocks != null)
    {
        for (var ii = 0; ii < blocks.length; ++ii)
        {
            if (blocks[ii].blockType != "CalendarBlock")
                continue;

            params = CalendarBlock_RemoveExistingCalendarParams(params);
            calendarParams = Cal_PackageQueryStringData(blocks[ii].blockID);
            break; // As of 11-2008 we only support one calendar on the page for PrintPage            
        }
    }

    location.search = params + FilterBlock_PackageQueryStringData(blockID, pageID, false) + calendarParams;
}

function FilterBlock_FormatClassifications(blockPrefix)
{
    var allInputs = document.getElementsByTagName("INPUT") ;
    
    var checkBoxes = new StringBuilder() ;
    for (var ii=0 ; ii<allInputs.length ; ++ii)
    {
        var myId = allInputs[ii].id ;
    
        if (allInputs[ii].type == "checkbox" &&   // may be a filter checkbox
            allInputs[ii].checked &&              // it is checked
            myId.indexOf(blockPrefix) == 0)       // and it is mine, all mine
            checkBoxes.Append(myId, "!") ;
    }
    
    return checkBoxes.ReturnAndEmpty() ;
}

function FilterBlock_CheckAll(ctl, blockID, classID)
{
    var blockPrefix = FilterBlock_MakePrefix(blockID) + "C" + classID + "_" ; 
    var allInputs = document.getElementsByTagName("INPUT") ;
    for (var ii=0 ; ii<allInputs.length ; ++ii)    
    {
        if (allInputs[ii].type == "checkbox" &&
            allInputs[ii].id.indexOf(blockPrefix) == 0)
            allInputs[ii].checked = ctl.checked ;            
    }    
}

function FilterBlock_KeywordFilter(blockID)
{
    FilterBlock_ClassificationCheck(blockID) ;
}

function FilterBlock_AjaxEnabled(blockPrefix)
{
    return (document.getElementById(blockPrefix + "ajaxEnabled") && document.getElementById(blockPrefix + "ajaxEnabled").value == 1) ;
}

function FilterBlock_ClassificationCheck(blockID)
{
    PrintPage_StoreInfoForPrintPage("FilterBlock", blockID);

    var blockPrefix = FilterBlock_MakePrefix(blockID) ; 
    if (!FilterBlock_AjaxEnabled(blockPrefix))
        return ;

    var ajaxArgs = new Array() ;
    ajaxArgs[ajaxArgs.length] = document.getElementById(blockPrefix + "DocID").value ;
    ajaxArgs[ajaxArgs.length] = blockID ;
    if (document.getElementById(blockPrefix + "keywordFilter"))
        ajaxArgs[ajaxArgs.length] = document.getElementById(blockPrefix + "keywordFilter").value ;
    else
        ajaxArgs[ajaxArgs.length] = "" ;
    ajaxArgs[ajaxArgs.length] = FilterBlock_FormatClassifications(blockPrefix) ;
    
    document.getElementById(blockPrefix + "ResultsDiv").className += " waiting" ;
    TitanDisplayServiceWrapper.MakeWebServiceCall("FilterBlock_" + blockID, NorthwoodsSoftwareDevelopment.Cms.WebServices.AggregationV2Ajax.GetResultsData, ajaxArgs, FilterBlock_AjaxComplete, [blockID], true) ;
}

function FilterBlock_AjaxComplete(blockID, results, context, methodName)
{
    var blockPrefix = FilterBlock_MakePrefix(blockID) ; 
    var resultsArea = document.getElementById(blockPrefix + "ResultsDiv") ;
    if (!resultsArea)
        return window.alert("Lost results") ;
        
    resultsArea.className = "AggregationResults" ;
        
    resultsArea.innerHTML = results ;        
} 

function FilterBlock_UncheckAll(allID)
{
    var ctl = document.getElementById(allID) ; 
    if (ctl)
        ctl.checked = false ;
}

function DisplaySideUpload_Expand(dataArea, link)
{
    if (document.getElementById(dataArea).style.display == "block")
    {
        document.getElementById(dataArea).style.display = "none" ;
        document.getElementById(link).innerHTML = "Upload File" ;
    }
    else
    {
        document.getElementById(dataArea).style.display = "block" ;
        document.getElementById(link).innerHTML = "Hide Upload Data" ;
    }
}

function DisplaySideUpload_Upload(btnCtl, baseID)
{
    btnCtl.disabled = true ;
    window.setTimeout("document.forms[0].submit()", 0) ;
}

var TitanDisplayServiceWrapper = 
{
    gCallbackQueue:
        new Array(),
        
        
    WebServiceCallComplete:
        function(results, context, methodName)
        {
            var callInfo = TitanDisplayServiceWrapper.gCallbackQueue.shift() ;
            if (!callInfo)
                return ; // can't happen, but....

            var matchesExist = false ;
            for (var ii=0 ; ii<TitanDisplayServiceWrapper.gCallbackQueue.length ; ++ii)
                if (TitanDisplayServiceWrapper.gCallbackQueue[ii].name == callInfo.name)
                {
                    matchesExist = true ; 
                    break ;
                }
                
            if (!matchesExist)   
            {
                var args = callInfo.postOpArgs ;
                args[args.length] = results ;
                args[args.length] = context ;
                args[args.length] = methodName ;
                callInfo.postOp.apply(window, args) ;
            }                
                
            if (TitanDisplayServiceWrapper.gCallbackQueue.length)
                TitanDisplayServiceWrapper.ActualAsyncCall() ;                                           
        },      
    WebServiceErrorFunction:
        function(myArgs)
        {            
            window.alert("An error was encountered processing your request") ;
            // Useful for debugging client-side: window.alert("Error: " + myArgs._message) ;
        },          
    
    MakeWebServiceCall:
        function(name, webServiceFunction, arrayOfArgs, postOpFunction, postOpArguments, canSkip)
        {
            arrayOfArgs[arrayOfArgs.length] = TitanDisplayServiceWrapper.WebServiceCallComplete ;
            arrayOfArgs[arrayOfArgs.length] = TitanDisplayServiceWrapper.WebServiceErrorFunction ;

            var newQueueItem = {name: name, callName: webServiceFunction, args: arrayOfArgs, postOp: postOpFunction, postOpArgs: postOpArguments} ;
            if (canSkip)
            {   // look for a queued up copy of this one and replace it
                for (var ii=1/*skip the active item*/; ii<TitanDisplayServiceWrapper.gCallbackQueue.length ; ++ii)
                    if (TitanDisplayServiceWrapper.gCallbackQueue[ii].name == name)
                    {
                        TitanDisplayServiceWrapper.gCallbackQueue[ii] = newQueueItem ;
                        return ; // We're done....the queue has an active item
                    }
            }                            
            
            TitanDisplayServiceWrapper.gCallbackQueue.push(newQueueItem) ;
            if (TitanDisplayServiceWrapper.gCallbackQueue.length == 1)
                TitanDisplayServiceWrapper.ActualAsyncCall() ;                        
        },    
        
    ActualAsyncCall:
        function()
        {
            if (TitanDisplayServiceWrapper.gCallbackQueue.length == 0)
                return ; // can't happen, but...
                
            var callInfo = TitanDisplayServiceWrapper.gCallbackQueue[0] ;
            callInfo.callName.apply(window, callInfo.args) ;
        }                          
} ;