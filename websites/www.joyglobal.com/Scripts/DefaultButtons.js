function SearchSubmit(baseURL, searchCtl)
{
    if (baseURL.indexOf("?") > 0)
        location.href = baseURL + "&Search_Keywords=" + escape(searchCtl.value) ; 
    else        
        location.href = baseURL + "?Search_Keywords=" + escape(searchCtl.value) ; 
}

function DefaultButton(anEvent, btn)
{ 
	if (document.all)
	{ 
		if (event.keyCode == 13)
		{ 
			event.returnValue=false; 
			event.cancel = true; 
			btn.click(); 
		} 
	}
	else if (document.getElementById)
	{ 
		if (anEvent.which == 13)
		{ 
			anEvent.returnValue=false; 
			anEvent.preventDefault();
			if (btn.click)
			    btn.click(anEvent); 
			else if (btn.onclick)
			    btn.onclick(anEvent) ; 			    
		}
	}
	else if(document.layers)
	{ 
		if(event.which == 13)
		{ 
			event.returnValue=false; 
			event.cancel = true; 
			btn.click(); 
		}
	}
}

function ShowPanel(onPanel, offPanel, suffix)
{
    document.getElementById(onPanel).style.display = "block" ;
    document.getElementById(offPanel).style.display = "none" ;
    
    if (!suffix)
        return ;
    
    for (var ii=0 ; ii<Page_Validators.length ; ++ii)
        if (Page_Validators[ii].id.indexOf(suffix) == (Page_Validators[ii].id.length - suffix.length))
            ValidatorEnable(Page_Validators[ii], true) ;
        else            
            ValidatorEnable(Page_Validators[ii], false) ;
}

function ValidateLogin(panel, changeMessageID)
{
    var messageControl = document.getElementById(changeMessageID) ;
    if (!messageControl)
        return ;
        
    messageControl.innerText = "" ;
    messageControl.style.display = "none" ;
    switch (panel)
    {
        case "changePasswordPanel":
            if (!document.getElementById("userNameEP").value ||
                !document.getElementById("oldPassword").value ||
                !document.getElementById("newPassword1").value ||
                !document.getElementById("newPassword2").value)
                messageControl.innerText = "All fields must be supplied" ;               
            else if (document.getElementById("newPassword1").value !=
                     document.getElementById("newPassword2").value)
                messageControl.innerText = "New passwords do not match" ;
            else if (document.getElementById("newPassword1").value ==
                     document.getElementById("oldPassword").value)
                messageControl.innerText = "New passwords matches old password" ;
            break ;
        case "loginOnlyDiv":
            if (!document.getElementById("userName").value ||
                !document.getElementById("userPassword").value)
                messageControl.innerText = "All fields must be supplied" ;               
            break ;
        case "loginAndChangeDiv":
            if (!document.getElementById("userNameCP").value ||
                !document.getElementById("oldPasswordCP").value ||
                !document.getElementById("newPassword1CP").value ||
                !document.getElementById("newPassword2CP").value)
                messageControl.innerText = "All fields must be supplied" ;               
            else if (document.getElementById("newPassword1CP").value !=
                     document.getElementById("newPassword2CP").value)
                messageControl.innerText = "New passwords do not match" ;
            else if (document.getElementById("newPassword1CP").value ==
                     document.getElementById("oldPasswordCP").value)
                messageControl.innerText = "New passwords matches old password" ;
            break ;
    }
    
    if (!messageControl.innerText)
    {
        document.getElementById("submitType").value = panel ;
        document.forms[0].submit() ;
    }        
    else
        messageControl.style.display="block" ;        
}


