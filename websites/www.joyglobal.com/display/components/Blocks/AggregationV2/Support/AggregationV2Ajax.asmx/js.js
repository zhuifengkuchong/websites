Type.registerNamespace('NorthwoodsSoftwareDevelopment.Cms.WebServices');
NorthwoodsSoftwareDevelopment.Cms.WebServices.AggregationV2Ajax=function() {
NorthwoodsSoftwareDevelopment.Cms.WebServices.AggregationV2Ajax.initializeBase(this);
this._timeout = 0;
this._userContext = null;
this._succeeded = null;
this._failed = null;
}
NorthwoodsSoftwareDevelopment.Cms.WebServices.AggregationV2Ajax.prototype={
_get_path:function() {
 var p = this.get_path();
 if (p) return p;
 else return NorthwoodsSoftwareDevelopment.Cms.WebServices.AggregationV2Ajax._staticInstance.get_path();},
GetResultsData:function(docID,blockID,keyword,classifications,succeededCallback, failedCallback, userContext) {
return this._invoke(this._get_path(), 'GetResultsData',false,{docID:docID,blockID:blockID,keyword:keyword,classifications:classifications},succeededCallback,failedCallback,userContext); }}
NorthwoodsSoftwareDevelopment.Cms.WebServices.AggregationV2Ajax.registerClass('NorthwoodsSoftwareDevelopment.Cms.WebServices.AggregationV2Ajax',Sys.Net.WebServiceProxy);
NorthwoodsSoftwareDevelopment.Cms.WebServices.AggregationV2Ajax._staticInstance = new NorthwoodsSoftwareDevelopment.Cms.WebServices.AggregationV2Ajax();
NorthwoodsSoftwareDevelopment.Cms.WebServices.AggregationV2Ajax.set_path = function(value) { NorthwoodsSoftwareDevelopment.Cms.WebServices.AggregationV2Ajax._staticInstance.set_path(value); }
NorthwoodsSoftwareDevelopment.Cms.WebServices.AggregationV2Ajax.get_path = function() { return NorthwoodsSoftwareDevelopment.Cms.WebServices.AggregationV2Ajax._staticInstance.get_path(); }
NorthwoodsSoftwareDevelopment.Cms.WebServices.AggregationV2Ajax.set_timeout = function(value) { NorthwoodsSoftwareDevelopment.Cms.WebServices.AggregationV2Ajax._staticInstance.set_timeout(value); }
NorthwoodsSoftwareDevelopment.Cms.WebServices.AggregationV2Ajax.get_timeout = function() { return NorthwoodsSoftwareDevelopment.Cms.WebServices.AggregationV2Ajax._staticInstance.get_timeout(); }
NorthwoodsSoftwareDevelopment.Cms.WebServices.AggregationV2Ajax.set_defaultUserContext = function(value) { NorthwoodsSoftwareDevelopment.Cms.WebServices.AggregationV2Ajax._staticInstance.set_defaultUserContext(value); }
NorthwoodsSoftwareDevelopment.Cms.WebServices.AggregationV2Ajax.get_defaultUserContext = function() { return NorthwoodsSoftwareDevelopment.Cms.WebServices.AggregationV2Ajax._staticInstance.get_defaultUserContext(); }
NorthwoodsSoftwareDevelopment.Cms.WebServices.AggregationV2Ajax.set_defaultSucceededCallback = function(value) { NorthwoodsSoftwareDevelopment.Cms.WebServices.AggregationV2Ajax._staticInstance.set_defaultSucceededCallback(value); }
NorthwoodsSoftwareDevelopment.Cms.WebServices.AggregationV2Ajax.get_defaultSucceededCallback = function() { return NorthwoodsSoftwareDevelopment.Cms.WebServices.AggregationV2Ajax._staticInstance.get_defaultSucceededCallback(); }
NorthwoodsSoftwareDevelopment.Cms.WebServices.AggregationV2Ajax.set_defaultFailedCallback = function(value) { NorthwoodsSoftwareDevelopment.Cms.WebServices.AggregationV2Ajax._staticInstance.set_defaultFailedCallback(value); }
NorthwoodsSoftwareDevelopment.Cms.WebServices.AggregationV2Ajax.get_defaultFailedCallback = function() { return NorthwoodsSoftwareDevelopment.Cms.WebServices.AggregationV2Ajax._staticInstance.get_defaultFailedCallback(); }
NorthwoodsSoftwareDevelopment.Cms.WebServices.AggregationV2Ajax.set_path("http://www.joyglobal.com/display/components/Blocks/AggregationV2/Support/AggregationV2Ajax.asmx");
NorthwoodsSoftwareDevelopment.Cms.WebServices.AggregationV2Ajax.GetResultsData= function(docID,blockID,keyword,classifications,onSuccess,onFailed,userContext) {NorthwoodsSoftwareDevelopment.Cms.WebServices.AggregationV2Ajax._staticInstance.GetResultsData(docID,blockID,keyword,classifications,onSuccess,onFailed,userContext); }
