
dojo.addOnLoad(function() {
	//Create Array of File Extensions and an Array of their corresponding lengeths.
	var fileExtArray = [".pdf", ".docx", ".doc", ".swf", ".xls", ".xlsx", ".ppt"];
	index = 0;
	//Find All Links In The Page
	dojo.query("a").forEach(function(theA){
    	//Get The Link
		var theLink = dojo.attr(theA,"href");
		//alert("Link: " + theLink + " A: " + theA);
		//Compare the Link with the file extensions
		var found = false;			
		dojo.forEach(fileExtArray, function(v, i){
			//If think link contains the ext, tag it
			if(theLink.lastIndexOf(v)!== -1){		
				found = true;
			}
		});
		if (found == true){
			//alert("theA " + theA.href + " theLink: " + theLink);
			dojo.connect(theA, "onclick", function(){
				//Report this click as a page view.
				//alert("theA " + theA.href + " theLink: " + theLink);
				ntptEventTag('lc=' + encodeURIComponent(theA.href));
			return true;
			});
		}
		return true;			
	});
});  

