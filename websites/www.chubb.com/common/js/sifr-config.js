//<[CDATA[
var metabold = {
    src: 'Unknown_83_filename'/*tpa=http://www.chubb.com/common/js/common/js/meta_bold.swf*/
  };

//sIFR.debugMode = true;
sIFR.delayCSS  = true;
sIFR.forceWidth = true;
sIFR.fixWrap = true;
sIFR.activate(metabold);

sIFR.replace(metabold, {
	selector: 'span.blockHeaderText'
	,css: [
	  '.sIFR-root { font-size:19px; color:#FFFFFF; padding:0px; margin:0px; line-height:17px; width:170px; display:block; leading:-8; }'
	]
	,wmode: 'transparent'
});

sIFR.replace(metabold, {
	selector: 'h1.headerRed'
	,css: [
	  '.sIFR-root { font-size:21px; color:#5B110E; padding:0px; margin:0px; line-height:23px; width:auto; display:block; }'
	]
	,wmode: 'transparent'
});

sIFR.replace(metabold, {
	selector: 'h1.headerBlue'
	,css: [
	  '.sIFR-root { font-size:21px; color:#074B89; padding:0px; margin:0px; line-height:23px; width:auto; display:block; }'
	]
	,wmode: 'transparent'
});


sIFR.replace(metabold, {
	selector: 'h3.businessTopic'
	,css: [
	  '.sIFR-root { font-size:13px; color:#792A0D; padding:0px; margin:0px; line-height:12px; width:auto; display:block; leading:-5; }'
	  ,'a { text-decoration: none; }'
      ,'a:link { color: #792A0D; }'
	  ,'a:visited { color: #792A0D; }'
      ,'a:hover { color: #9B5F4A; }'
	]
	,wmode: 'transparent'
});

sIFR.replace(metabold, {
	selector: 'h3.personalTopic'
	,css: [
	  '.sIFR-root { font-size:13px; color:#113454; padding:0px; margin:0px; line-height:12px; width:auto; display:block; leading:-5; }'
	  ,'a { text-decoration: none; }'
      ,'a:link { color: #113454; }'
	  ,'a:visited { color: #113454; }'
      ,'a:hover { color: #4D677F; }'
	]
	,wmode: 'transparent'
});


sIFR.replace(metabold, {
	selector: 'span.regularHeading'
	,css: [
	  '.sIFR-root { font-size:14px; color:#000000; padding:0px; margin:0px; line-height:12px; width:auto; display:block; }'
	]
	,wmode: 'transparent'
});


sIFR.replace(metabold, {
	selector: 'http://www.chubb.com/common/js/span.date'
	,css: [
	  '.sIFR-root { font-size:11px; color:#666666; padding:0px; margin:0px; line-height:10px; width:auto; display:block; leading:-10; text-align:right; }'
	]
	,wmode: 'transparent'
});

sIFR.replace(metabold, {
	selector: 'span.local'
	,css: [
	  '.sIFR-root { font-size:11px; color:#666666; padding:0px; margin:0px; line-height:10px; width:auto; display:block; leading:-10; text-align:right; }'
	]
	,wmode: 'transparent'
});

sIFR.replace(metabold, {
	selector: 'div.sidebarHeading'
	,css: [
	  '.sIFR-root { font-size:14px; color:#FFFFFF; padding:0px; margin:0px; line-height:12px; width:170px; leading:-3; display:block; }'
	]
	,wmode: 'transparent'
});

  
//sIFR.debug.ratios({ src: 'Unknown_83_filename'/*tpa=http://www.chubb.com/media/sifr/meta_bold.swf*/, selector: 'h1.headerRed' });
//]]>