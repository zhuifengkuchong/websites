	
// =============================================================== //
// Translate the follwong strings.  Nothing else needs translations //
// =============================================================== //


    var txtRoutersOverview = "WD Wireless Routers Overview";
	var txtExternalOverview = "External Storage Overview";
	var txtBSSverview = "Business Storage Solutions";
	var txtNetworkOverview = "Personal Cloud";
	var txtInternalOverview = "Internal Storage Overview";
	var txtEnterpriseOverview = "Enterprise Drives Overview";
	var txtMobileOverview = "Mobile Drives Overview";
	var txtCEOverview = "AV Hard Drives Overview";
	var txtMyBookOverview = "Desktop Overview";
	var txtMyPassportOverview = "Portable Overview";
	var txtHEOverview = "Home Entertainment Overview";
	var txtWDTVOverview = "WD TV Overview";
	var txtInternalOverview = "Internal Hard Drive Overview";
	var txtSSDOverview = "Solid State Storage Overview";
	var txtExtDesktop = "External Desktop Hard Drives Overview";
	var txtExtDesktopMac = "External Desktop Hard Drives for Mac Overview";
	var txtExtPortable = "External Portable Hard Drives Overview";
	var txtExtPortableMac = "External Portable Hard Drives for Mac Overview";
	var txtHEDVR = "DVR Expanders Overview";
	var txtHEmediaplayers = "Media Players Overview";
	var txtHEmulitmediadrives = "Multimedia Drives Overview";
	var txtHEportablemediadrives = "Portable Media Drives Overview";
	var txtIntav = "AV Hard Drives Overview";
	var txtIntdesktop = "Internal Desktop Hard Drives Overview";
	var txtIntNas = "NAS Hard Drives";
	var txtIntenterprise = "Enterprise Hard Drives Overview";
	var txtIntmobile = "Internal Mobile Hard Drives Overview";
	var txtSSDapplications = "Applications";
	var txtSSDdesktopmobile= "Desktop and Mobile Storage Overview";
	var txtSSDembedded= "Embedded Storage Overview";
	var txtSSDtechnology = "Technology";
	var txtSSDWhitepapers = "Whitepapers";	
	var txtSSDSales = "Sales";
	var txtProducts = "Products";
	var txtWDTVLiveUpdates = "WD TV Live Updates";
	var txtWDHome = "WD Home";
	var txtPressRoom = "Press Room";
	var txtPressRoomCorp = "Corporate/Financial Releases";
	var txtPressRoomFin = "Financial Press Releases";
	var txtPressRoomProd = "WD Product Releases";
	var txtPressRoomRelease = "Press Release";
	var txtContactWD = "Contact WD";
	var txtCompany = "Company";
	var txtEmployment = "Careers";
	var txtCreativeMasters = "Creative Masters";
	// =============================================================== //
	// END of translation strings                                      //
	// =============================================================== //	
		
	var strBody = "";
	
	
function showPath()
{	
	var path = new String();
	var title, tokens, classname;
	var strBegin, strBody, strEnd, strText;
	var depth = 0;
	
	// get the document's path and title
	title = document.title;
	path = location.pathname;
	if (title.length > 100) {
	    title = title.substring(0, 99) + "...";
	}

	//Start breadcrumb build
	strBody = "<a href=\"../en/index.htm\"/*tpa=http://www.wdc.com/*/>"+txtWDHome+"</a> ";
	
	delim = "/";
	stop = 0;
	token = "";
	tokenlink = "/";

	  while (stop != -1)
	  {

	      if (token == txtProducts)
			{
			    //strBody = strBody  + " / " + token ;
			    strBody = strBody + getPathDescription();
			}
		else if (token != "")
			{
				strBody = strBody + " / <a href=\"" + tokenlink + "\">" + token + "</a>"; 
			}
	  	if (++depth > 4) break;	  	
	  	path = path.substring(stop+1, path.length);
	  	stop = path.indexOf(delim);
	  	token = path.substring(0, stop);
	  	tokenlink = tokenlink + token + "/";
	  	token = getDescription(token);
	  	 
	  } 
       
	strBody += " / "  + title;
	  document.write (strBody);
	}

	function getPathDescription() {
	    var daPath = "";
	    var daURL = "";
	    if (typeof (bc_variable) !== 'undefined') {
	        switch (bc_variable) {
	            case "wdmscHomeEntertainment_MediaPlayers":
	                daPath = txtHEmediaplayers;
	                daURL = "homeentertainment/mediaplayers/";
	                break;
	            case "wdmscExternal_Desktop":
	                daPath = txtExtDesktop;
	                daURL = "external/desktop/";
	                break;
	            case "wdmscExternal_Desktop_Mac":
	                daPath = txtExtDesktopMac;
	                daURL = "external/desktopformac/";
	                break;
	            case "wdmscExternal_Portable":
	                daPath = txtExtPortable;
	                daURL = "external/portable/";
	                break;
	            case "wdmscExternal_Portable_Mac":
	                daPath = txtExtPortableMac;
	                daURL = "external/portableformac/";
	                break;
	            default:
	        }
	    }
	    if (typeof (bc_segment) !== 'undefined') {
	        if (daPath == "") {
	            switch (bc_segment) {
	                case "wdmsBSS":
	                    daPath = txtBSSverview;
	                    daURL = "business/";
	                    break;
	                case "wdmsNAS":
	                    daPath = txtIntNas;
	                    daURL = "http://www.wdc.com/scripts/products.aspx?id=810";
	                    break;
	                case "wdmsDesktop":
	                    daPath = txtIntdesktop;
	                    daURL = "internal/desktop/";
	                    break;

	                case "wdmsSolidStateEmbedded":
	                    daPath = txtSSDOverview;
	                    daURL = "solidstate/";
	                    break;
	                case "wdmsMobile":
	                    daPath = txtIntmobile;
	                    daURL = "internal/mobile/";
	                    break;
	                case "wdmsEnterprise":
	                    daPath = txtEnterpriseOverview;
	                    daURL = "internal/enterprise/";
	                    break;
	                case "wdmsCEDrives":
	                    daPath = txtCEOverview;
	                    daURL = "internal/av/";
	                    break;
	                case "wdmsRouters":
	                    daPath = txtRoutersOverview;
	                    daURL = "network/routers/";
	                    break;
	                    
	                case "wdmsNetwork":
	                    daPath = txtNetworkOverview;
	                    daURL = "network/networkstorage/";
	                    break;
	                default: 
	            }
	        }
	         }
	         if (daPath != "") {
	             daPath = " / <a href='" + daURL + "'>" + daPath + "</a>";
	         }

	         return daPath;
	}
function getDescription(token)
{
	switch(token)
	{
		case "wdtvliveupdates":
		    token = txtWDTVLiveUpdates;
		    break;
		case "pressroom":
		    token = txtPressRoom;
		    break;
		case "corporate":
		    token = txtPressRoomCorp;
		    break;
		case "financials":
		    token = txtPressRoomFin;
		    break;
		case "releases":
		    token = txtPressRoomRelease;
		    break;
		case "productnews":
		    token = txtPressRoomProd;
		    break;
		case "contact":
			token = txtContactWD;
		    break;		
		case "creativemasters":
		    token = txtCreativeMasters;
		    break;
		case "wdsmartware":
			token = "WD SmartWare";
		    break;
		case "products":
		    token = txtProducts;
		    break;
		case "company":
		    token = txtCompany;
		    break;
		case "employment":
		    token = txtEmployment; 
		    break;
		case "networkstorageX":
		    token = txtNetworkOverview;
		    break;		
		case "solidstate":
		    token = txtSSDOverview;
		    break;	
		case "applications":
			token = txtSSDapplications;				
		    break;
		case "desktopandmobile":
			token = txtSSDdesktopmobile;				
		    break;
		case "sales":
			token = txtSSDSales;				
		break;
		case "technology":
			token = txtSSDtechnology;				
		break;
		case "whitepapers":
			token = txtSSDWhitepapers;				
		break;	
        default:
            token = "";			
			
	}

	return token;
}