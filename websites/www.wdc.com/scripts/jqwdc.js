
var strMore;
var strLess;
var isFF = (navigator.userAgent.toLowerCase()).indexOf("firefox") != -1;

$(document).ready(function() {
    //=================================================================


    if ($('#nav-menu').length) {
        $('#nav-menu .parent').each(function() {
            $('div.submenu-wrapper', this).css({ 'display': 'block', visibility: 'hidden', height: 'auto', left: -9999, top: -9999 });
            if ($('div.submenu-padding', this).outerHeight() >= $('div.side-bar', this).outerHeight()) {
                var _height = $('div.submenu-padding', this).outerHeight();
                $('div.side-bar', this).height(_height).addClass('fill-height');
            } else {
                var _padding = parseInt($('div.submenu-padding', this).css('padding-top').replace('px', '')) + parseInt($('div.submenu-padding', this).css('padding-bottom').replace('px', '')),
                    _height = $('div.side-bar', this).outerHeight();
                $('div.submenu-padding', this).height(_height - _padding);
            }
            $(this).data('height', _height);
            $('div.submenu-wrapper', this).removeAttr('style');
            if (!isFF || (isFF && navigator.buildID && (Math.abs(navigator.buildID.slice(0, 6)) >= 201406)) || (isFF && !navigator.buildID)) {
                var pos = $(this).position().left;
                $('div.submenu-wrapper', this).css({ left: -(pos) });
            }
        });

        function hovInt_over() {
            $('span.up-arrow', this).css({ 'display': 'block' });
            var _height = $(this).data('height');
            $('div.submenu-wrapper', this).css('display', 'block').animate({ height: _height, opacity: 1 }, 300, 'swing');
            if (Modernizr.touch) {
                $('div.submenu-wrapper', this).removeClass("modrnHide");
                $(this).removeClass("bkgdPatch");
                $('div.submenu-wrapper', this).addClass("modrnShow");
            }
        }
        function hovInt_out() {
            $('span.up-arrow', this).css({ 'display': 'none' });
            $('div.submenu-wrapper', this).animate({ height: 0, opacity: 0 }, 300, 'swing', function() {
                $(this).css('display', 'none');
            });
            if (Modernizr.touch) {
                $('div.submenu-wrapper').removeClass("modrnShow");
                $('div.submenu-wrapper').addClass("modrnHide");
                $(this).addClass("bkgdPatch");
            }
        }
        $('#nav-menu .parent').hoverIntent({
            over: hovInt_over,
            timeout: 0,
            out: hovInt_out
        });

        $('#nav-menu .parent').click(function() {
            if (Modernizr.touch) {
                var elem = this;
                var elemChild = this.children[1];
                var elemArrow = (this.children[0]).children[0];

                if (elemChild.classList.contains("modrnShow")) {
                    $(elemChild).removeClass("modrnShow");
                    $('#nav-menu .parent').each(function() {
                        $('div.submenu-wrapper').addClass("modrnHide");
                    });
                    $(elemArrow).css("display", "none");
                    $(elem).addClass("bkgdPatch");
                    return;
                }
                if (elemChild.classList.contains("modrnHide")) {
                    $(elemChild).removeClass("modrnHide");
                    $(elemChild).addClass("modrnShow");
                    $(elemArrow).css("display", "block");
                    $(elem).removeClass("bkgdPatch")
                }
            }
        });

    }
    if ($('#home-grid .box').length) {
        $('.layer .bkg', $('#home-grid .box')).css({ opacity: 0 });
        $('.layer .over', $('#home-grid .box')).css({ opacity: 0 });
        $('.content p', $('#home-grid .box')).slideUp();
        $('.content .link', $('#home-grid .box')).hide();
        $('#home-grid .box').hoverIntent({
            over: function() {
                $('.layer .bkg', this).css({ opacity: 1 });
                $('.layer .over', this).css({ opacity: 1 });
                $('.content p', this).slideDown(150);
                $('.content .link', this).show();
            },
            out: function() {
                $('.layer .bkg', this).css({ opacity: 0 });
                $('.layer .over', this).css({ opacity: 0 });
                $('.content p', this).slideUp();
                $('.content .link', this).hide();
            }
        });
    }
    if ($('#bxslider').length) {
        $("#bxslider").bxSlider({
            responsive: true,
            autoHover: true,
            auto: true
        });
        /*
        $(".bx-wrapper").bind('mouseenter', function(){
        $('.bx-wrapper .bx-controls-direction a').fadeIn();
        }).bind('mouseleave', function(){
        $('.bx-wrapper .bx-controls-direction a').hide();
        })
        */
    }

    //Init New Functionality
    if ($('#product-detail').length)
        init_product_detail();

    //** Search functionality begins **
    $('.btnSearch').click(function(e) {
        e.preventDefault();
        // get the search value, which is the previous textbox with class .sq
        var query = $.trim($(this).prev('.sq').val());
        SearchSite(query);
    });

    $('.sq').keypress(function(e) {
        //var query;
        if (e.which == 13) {
            e.preventDefault();
            var query = $.trim($(this).val());
            if (query != 'undefined' && query != '') {
                SearchSite(query);
            }
            //            $('.sq').each(function() {
            //                query = $.trim($(this).val());
            //                if (query != 'undefined' && query != '') {
            //                    SearchSite(query);
            //                    return false;  // break out of loop
            //                }
            //            });
        }
    });

});

//=================================================================================================
//END of document onready
//=================================================================================================

//** Search function methods **

// Get the query string key value.
function getQuerystring(key, default_) {
    if (default_ == null) default_ = "";
    key = key.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + key + "=([^&#]*)");
    var qs = regex.exec(window.location.href);
    if (qs == null)
        return default_;
    else
        return qs[1];
}

function URLEncode(clearString) {
    var output = '';
    var x = 0;
    clearString = clearString.toString();
    var regex = /(^[a-zA-Z0-9_.]*)/;
    while (x < clearString.length) {
        var match = regex.exec(clearString.substr(x));
        if (match != null && match.length > 1 && match[1] != '') {
            output += match[1];
            x += match[1].length;
        }
        else {
            if (clearString[x] == ' ')
                output += '+';
            else {
                var charCode = clearString.charCodeAt(x);
                var hexVal = charCode.toString(16);
                output += '%' + (hexVal.length < 2 ? '0' : '') + hexVal.toUpperCase();
            }
            x++;
        }
    }
    return output;
}

function SearchSite(query) {
    // get search query
    //    var query = $.trim($('.sq').val());

    // first check if "sl" key is in query string; if it exists, then we are on the /wdsearch page;
    // so continue to use the same search language.  If there is "none" then we are not on the search page;
    // so get language folder name to pass to the "sl" query string key.

    var lang = getQuerystring('sl', 'none');
    if (lang == 'none') {
        lang = location.pathname.split('/')[1];
    }
    if (query) {
        window.location = 'http:///' + location.hostname + '/wdsearch/?sq=' + URLEncode(query) + '&sl=' + lang;
    }
}
//** End Search function methods **

//Generic JQ scroll to for anchored links
function scrollToId(id) {
    window.scrollTo(0, $("#" + id).offset().top);
}
//========================================================
// show today's date in the header part
//========================================================
function showDate(plaintext) {
    // CB Global Edge - changed this to call GetDateString for localized date string
    var time = new Date();
    strToday = GetDateString(time);


    if (plaintext == 1) {
        document.write(strToday);
    }
    else {
        document.write("<span class=date>" + strToday + "</span>");
    }

}
function GetDateString(time) {
    // CB Global Edge - added this function for international date formatting
    var intYear = time.getFullYear();
    var intMonth = time.getMonth();
    var intDate = time.getDate();
    var strToday = "%M %d, %Y"; // date formatting

    switch (intMonth) {
        case 0:
            strMonth = "January";
            break;
        case 1:
            strMonth = "February";
            break;
        case 2:
            strMonth = "March";
            break;
        case 3:
            strMonth = "April";
            break;
        case 4:
            strMonth = "May";
            break;
        case 5:
            strMonth = "June";
            break;
        case 6:
            strMonth = "July";
            break;
        case 7:
            strMonth = "August";
            break;
        case 8:
            strMonth = "September";
            break;
        case 9:
            strMonth = "October";
            break;
        case 10:
            strMonth = "November";
            break;
        case 11:
            strMonth = "December";
            break;
    }

    re = /%M/;
    strToday = strToday.replace(re, strMonth);
    re = /%d/;
    strToday = strToday.replace(re, intDate);
    re = /%Y/;
    strToday = strToday.replace(re, intYear);
    return strToday;
}

//===============================================================================
var nw
function openAnyWindow(url, name, w, h) {
    if (!nw || nw.closed) {
        nw = window.open(url, name, 'scrollbars=yes,resizable=yes,status=no,location=no,menubar=yes,width=' + w + ',height=' + h);
        if (!nw.opener) {
            nw.opener
        }
        //nw.document.close()
    }
    else {
        nw.close()
        nw = window.open(url, name, 'scrollbars=yes,resizable=yes,status=no,location=no,menubar=yes,width=' + w + ',height=' + h);
        nw.focus()
    }
}
//========================================================
// New Functionality
//========================================================
var init_product_detail = function() {
    var $_tab_container = $('.tab-container'),
        $_tabs = $('http://www.wdc.com/scripts/ul.cabinet li.tab', $_tab_container),
        _first_load = true;
    if ($_tabs.length) {
        $_tabs.unbind().click(function() {
            var $_this = $(this),
                $_container = $_this.parents('.tab-container'),
                _ref = $_this.attr('ref'),
                _parent_content = false;
            $_this.siblings('http://www.wdc.com/scripts/li.tab').removeClass('selected');
            $_this.addClass('selected');
            if ($_container.hasClass('parent')) {
                var $_child_tabs = $_container.next('.tab-container.child');
                if ($_child_tabs.length) {
                    var $_child_filter = $_child_tabs.find('div.tab-table').stop().hide().filter('[ref="' + _ref + '"]');
                    if ($_child_filter.length) {
                        $_child_filter.fadeIn(300).find('http://www.wdc.com/scripts/li.tab').eq(0).click();
                        $_container.removeClass('no-child');
                    } else {
                        _parent_content = true;
                        $_container.addClass('no-child');
                    }
                }
            }
            if (!$_container.hasClass('parent') || _parent_content) {
                var $_content = (_parent_content) ? $_child_tabs.next('.content-container') : $_container.next('.content-container');
                if ($_content.length)
                    $_content.find('div.tab-content').stop().hide().filter('[ref="' + _ref + '"]').fadeIn(300);
            }
            if (!_first_load && history && history.pushState) {
                if (!$_container.hasClass('child'))
                    history.pushState({}, '', '#' + _ref);
                else if ($_this.parents('.tab-table').length && $_this.parents('.tab-table').is('[ref]'))
                    history.pushState({}, '', '#' + $_this.parents('.tab-table').attr('ref') + '&' + _ref);
            }
        });
        if (_first_load)
            $_tabs.eq(0).click();
        _first_load = false;
    }
    var _handle_hash = function() {
        var _anchorjump = null;
        var _anchor = null;
        var _source = ($(this).hasClass('changetabs')) ? $(this).attr('href') : location.hash,
            _source = _source.replace("#t", "#T"),
                _deeplinks = _source.substring(1).split('&', 2),
                _deeplinks_length = _deeplinks.length,
                $_parent = $_tab_container.first(),
                $_child = $_tab_container.filter('.child');
        for (i = 0; i < _deeplinks_length; ++i) {
            var $_selected_tab = {};
            _anchor = _deeplinks[i].substring(0).split('#', 2);

            if (i == 0 && $_parent.length) {
                $_selected_tab = $_parent.find('li.tab[ref="' + _deeplinks[i] + '"]');

            } else if (i > 0 && $_child.length) {
                $_selected_tab = $_child.find('div.tab-table[ref="' + _deeplinks[0] + '"]').find('li.tab[ref="' + _anchor[0] + '"]');

            }
            if ($_selected_tab.length)
                $_selected_tab.click();
            _anchorjump = _anchor[1];
        }
        if (_anchorjump != null) {
            document.getElementById(_anchorjump).scrollIntoView(true);
            _anchorjump = null;
        }

    }
    $('.changetabs').unbind().click(_handle_hash);
    _handle_hash();

};