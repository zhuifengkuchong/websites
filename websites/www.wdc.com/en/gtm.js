
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" xmlns="http://www.w3.org/1999/xhtml" > <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" xmlns="http://www.w3.org/1999/xhtml" > <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" xmlns="http://www.w3.org/1999/xhtml" > <![endif]-->
<!--[if gt IE 8]><!--> 
<html> <!--<![endif]-->

<head>
    <title>The page is no longer available.</title>

    
    
    <meta property="og:image" content="/global/images/logos/wdlogo395.jpg"/>
    <link href="/css50/normalize.min.css" rel="stylesheet" type="text/css" />
    <link href="/css50/reset.css" rel="stylesheet" type="text/css" />
    <link href="/css50/skin.css" rel="stylesheet" type="text/css" />
    <link href="/css50/colorbox.css" rel="stylesheet" type="text/css" />
    <link href="/css50/jquery.bxslider.css" rel="stylesheet" type="text/css" />
    <link href="/css50/menu.css" rel="stylesheet" type="text/css" />
    <link href="/css50/wdc50.css" rel="stylesheet" type="text/css" />
    <link href="/css50/wdc50TXT.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/scripts/modernizr-2.6.2.min.js"></script>
    <script type="text/javascript" src="/scripts/jquery-1.11.1.min.js"></script>
 
  

   
<script type="text/javascript">var _gaq = _gaq || [];_gaq.push(['_setAccount', 'UA-20073-2']);_gaq.push (['_gat._anonymizeIp']); _gaq.push(['_trackPageview']);_gaq.push(['wdwdc._setDomainName', 'wdc.com']);_gaq.push(['wdwdc._setAllowLinker', true]);_gaq.push (['wdwdc._gat._anonymizeIp']);_gaq.push(['wdwdc._trackPageview']);(function() {var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);})();</script> 
</head>
<body>
<!-- Google Tag Manager --><noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-PPXM4Q" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-PPXM4Q');</script><!-- End Google Tag Manager --> 
<a name="_top" id="_top"></a>
    <form name="aspnetForm" method="post" action="default.aspx?404%3bhttp%3a%2f%2fwww.wdc.com%3a80%2fen%2fgtm.js" id="aspnetForm">
<div>
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwULLTExMTMxOTA0MjJkZLE17xkRPxDx1qCBqc60DSDtwejS" />
</div>


<script src="../en/scripts/breadcrumb.js" type="text/javascript"></script>
<div>

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="EB9BA371" />
</div>

    <div id="ctl00_primaryNav"><header id="header_v5">
  <div class="header-wrapper">
    <div id="header_top_v5">
      <div id="search_v5">
        <div class="header_links"><a style="color:#06C;" href="/en/company/">About WD</a><span class="verbarheader_padding">|</span><a style="color:#06C;" href="/en/buy">Where to Buy</a><span class="verbarheader_padding">|</span><a style="color:#06C;" href="/en/company/pressroom/corporate/">Corporate News</a><span class="verbarheader_padding">|</span><a style="color:#06C;" href="/en/partners/">Partners</a><span class="verbarheader_padding">|</span><a style="color:#06C;" href="company/employment/">Careers</a><span class="verbarheader_padding">|</span><a style="color:#06C;" href="/en/company/pressroom/socialmedia/">WD Social</a><span class="verbarheader_padding">|</span><a style="color:#06C;" href="http://community.wd.com/">WD Community</a><span class="verbarheader_padding">|</span><a style="color:#06C;" href="/en/language">Language</a> &nbsp;&nbsp; </div>
        <input name="sc" type="hidden" value=""/>
        <input name="sl" type="hidden" value=""/>
        <!--<input name="sq" type="text" style="height: 16px;" onclick="this.value='';" onfocus="this.select()" onblur="this.value!=this.value?'Search':this.value;" value="Search"/>-->
        <input class="sq" id="sq" name="sq" type="text" style="height: 16px;" onfocus="this.select()" placeholder="Search"/>
        &nbsp;
        <input id="btnSearch" class="btn btnSearch" type="submit" value="" alt="Go"/>
      </div>
    </div>
    <a id="top-logo" href="/"><img src="/global/images/topnav50/nav_logo116x75.png"/></a>
    <nav id="top-menu-wrapper">
      <ul id="nav-menu" class="qmmc">
        <li id="top-nav-offset"><img src="/global/images/topnav/spacer.png"/></li>
        <li class="parent"><a runat="server" href="javascript:void(0)">External Storage<span class="up-arrow">&nbsp;</span></a>
          <div class="submenu-wrapper show-side-bar">
            <div class="submenu-padding">
              <div class="submenu-table">
                <ul class="submenu">
                  <li class="group-menu">
                    <h1>For PC</h1>
                    <h3><a href="/en/products/external/desktop" title="Desktop Drives">Desktop Drives</a></h3>
                    <ul>
                      <li><a href="/en/products/products.aspx?id=1260" title="My Book Duo">My Book Duo</a></li>
                      <li><a href="/en/products/products.aspx?id=870" title="My Book">My Book</a></li> 
                      <li><a href="/en/products/products.aspx?id=260" title="WD Elements Desktop">WD Elements Desktop</a></li>
                      <li class="actions"><a runat="server" class="button thin" href="/en/products/external/desktop">Compare All</a>
                        <div class="clear"/>
                      </li>
                    </ul>
                    <h3><a href="/en/products/external/portable" title="Portable Drives">Portable Drives</a></h3>
                    <ul>
                      <li><a href="/en/products/products.aspx?id=1490" title="My Passport Ultra">My Passport Ultra</a></li>
                      <li><a href="/en/products/products.aspx?id=1290" title="My Passport Ultra Metal Edition">My Passport Ultra Metal Edition</a></li>
                      <li><a href="/en/products/products.aspx?id=470" title="WD Elements Portable">WD Elements Portable</a></li>
                      <li class="actions"><a runat="server" class="button thin" href="/en/products/external/portable">Compare All</a>
                        <div class="clear"/>
                      </li>
                    </ul>
                  </li>
                  <li class="group-menu">
                    <h1>For Mac</h1>
                    <h3><a href="/en/products/external/desktopformac/" title="Desktop Drives for Mac">Desktop Drives for Mac</a></h3>
                    <ul>
                      <li><a href="/en/products/products.aspx?id=250" title="My Book for Mac">My Book for Mac</a></li>
                      <li><a href="/en/products/products.aspx?id=200" title="My Book Studio">My Book Studio</a></li>
                      <li><a href="/en/products/products.aspx?id=630" title="My Book Thunderbolt Duo">My Book Thunderbolt Duo</a></li>
                      <li class="actions"><a runat="server" class="button thin" href="/en/products/external/desktopformac/">Compare All</a>
                        <div class="clear"/>
                      </li>
                    </ul>
                    <h3><a href="/en/products/external/portableformac/" title="Portable Drives for Mac">Portable Drives for Mac</a></h3>
                    <ul>
                      <li><a href="/en/products/products.aspx?id=1040" title="My Passport Air">My Passport Air</a></li>
                      <li><a href="/en/products/products.aspx?id=1500" title="My Passport for Mac">My Passport for Mac</a></li>
                      <li><a href="/en/products/products.aspx?id=420" title="My Passport Studio">My Passport Studio</a></li>
                      <li><a href="/en/products/products.aspx?id=1240" title="My Passport Pro">My Passport Pro</a></li>
                      <li class="actions"><a runat="server" class="button thin" href="/en/products/external/portableformac/">Compare All</a>
                        <div class="clear"/>
                      </li>
                    </ul>
                  </li>
                  <li class="group-menu">
                    <h1>For Both</h1>
                    <h3><a href="/en/products/personalcloud/consumer/" title="Consumer Series">Consumer Series</a></h3>
                    <ul>
                      <li><a href="/en/products/products.aspx?id=1140" title="My Cloud">My Cloud</a></li>
                      <li><a href="/en/products/products.aspx?id=1200" title="My Cloud Mirror">My Cloud Mirror</a></li>
                      <li class="actions"><a runat="server" class="button thin" href="/en/products/personalcloud/consumer/">Compare All</a>
                        <div class="clear"/>
                      </li>
                    </ul>
                    <h3><a href="/en/products/personalcloud/expert/" title="Expert Series">Expert Series</a></h3>
                    <ul>
                      <li><a href="/en/products/products.aspx?id=1180" title="My Cloud EX2">My Cloud EX2</a></li>
                      <li><a href="/en/products/products.aspx?id=1170" title="My Cloud EX4">My Cloud EX4</a></li>
                      <li class="actions"><a runat="server" class="button thin" href="/en/products/personalcloud/expert/">Compare All</a>
                        <div class="clear"/>
                      </li>
                    </ul>
                    <h3><a href="/en/products/products.aspx?id=1330" title="Wireless Storage">Wireless Storage</a></h3>
                    <ul>
                      <li><a href="/en/products/products.aspx?id=1330" title="My Passport Wireless">My Passport Wireless</a></li>
                    </ul>
                    <h3><a href="/en/products/products.aspx?id=1480" title="Wireless Storage">Gaming Storage</a></h3>
                    <ul>
                      <li><a href="/en/products/products.aspx?id=1480" title="My Passport X">My Passport X</a></li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
            <div class="side-bar">
              <div class="top-half">
                <div class="bar-padding">
                  <h2>Support</h2>
                  <ul>
                    <li><a href="http://mysupport.wdc.com?wdc_lang=en" title="WD Support Portal">WD Support Portal</a></li>
                    <li><a href="http://support.wdc.com/warranty/index.asp?wdc_lang=en" title="Warranty & RMA Services">Warranty & RMA Services</a></li>
                    <li><a href="http://support.wdc.com/product/kb.asp?wdc_lang=en" title="FAQ/Knowledge base">FAQ/Knowledge base</a></li>
                  </ul>
                </div>
              </div>
              <div class="bottom-half">
                <div class="bar-padding">
                  <div class="actions"><a  class="button thin" href="/en/products/catalog/?segment=2">Product List</a></div>
                </div>
                <div class="bar-padding">
                  <div><a  class="button thin" href="/backitup"><img src="/global/images/logos/Backitup_.png" border="0" alt="Back it Up"/></a></div>
                </div>
              </div>
            </div>
            <div class="clear"/>
          </div>
        </li>
        <li class="parent"><a href="javascript:void(0)" title="Internal Storage">Internal Storage<span class="up-arrow">&nbsp;</span></a>
          <div class="submenu-wrapper show-side-bar">
            <div class="submenu-padding">
              <div class="submenu-table">
                <ul class="submenu">
                  <li class="group-menu">
                    <h3><a href="/en/products/internal/desktop/" title="Desktop">Desktop</a></h3>
                    <ul>
                      <li><a href="/en/products/products.aspx?id=770" title="WD Blue">WD Blue</a></li>
                      <li><a href="/en/products/products.aspx?id=780" title="WD Green">WD Green</a></li>
                      <li><a href="/en/products/products.aspx?id=760" title="WD Black">WD Black</a></li>
                      <li class="actions"><a class="button thin" href="/en/products/internal/desktop/">Compare All</a>
                        <div class="clear"/>
                      </li>
                    </ul>
                    <h3><a href="/en/products/internal/mobile/" title="Mobile">Mobile</a></h3>
                    <ul>
                      <li><a href="/en/products/products.aspx?id=800" title="WD Blue">WD Blue</a></li>
                      <li><a href="/en/products/products.aspx?id=830" title="WD Green">WD Green</a></li>
                      <li><a href="/en/products/products.aspx?id=790" title="WD Black">WD Black</a></li>
                      <li><a href="/en/products/products.aspx?id=1190" title="WD Black²">WD Black²</a></li>
                      <li class="actions"><a class="button thin" href="/en/products/internal/mobile/">Compare All</a>
                        <div class="clear"/>
                      </li>
                    </ul>
                  </li>
                  <li class="group-menu">
                    <h3><a href="/en/products/internal/nas/" title="NAS">NAS</a></h3>
                    <ul>
                      <li><a href="/en/products/products.aspx?id=810" title="WD Red">WD Red</a></li>
                      <li><a href="/en/products/products.aspx?id=1280" title="WD Red Pro">WD Red Pro</a></li>
                      <li><a href="/en/products/products.aspx?id=580" title="WD Re">WD Re</a></li>
                      <li class="actions"><a class="button thin" href="/en/products/internal/nas/">Compare All</a>
                        <div class="clear"/>
                      </li>
                    </ul>
                    <h3><a href="/en/products/datacenter/" title="Datacenter">Datacenter</a></h3>
                    <ul>
                      <li><a href="/en/products/products.aspx?id=580" title="WD Re">WD Re</a></li>
                      <li><a href="/en/products/products.aspx?id=1390" title="WD Re">WD Re+</a></li>
                      <li><a href="/en/products/products.aspx?id=1050" title="WD Se">WD Se</a></li>
                      <li><a href="/en/products/products.aspx?id=1340" title="WD Ae">WD Ae</a></li>
                      <li class="actions"><a class="button thin" href="/en/products/internal/enterprise/">Compare All</a>
                        <div class="clear"/>
                      </li>
                    </ul>
                  </li>
                  <li class="group-menu">
                    <h3><a href="/en/products/surveillance" title="Surveillance">Surveillance</a></h3>
                    <ul>
                      <li><a href="/en/products/products.aspx?id=1210" title="WD Purple">WD Purple</a></li>
                      <li><a href="/en/products/products.aspx?id=1540" title="WD Se">WD Purple NV</a></li>
                      <li><a href="/en/products/products.aspx?id=580" title="WD Re">WD Re</a></li>
                      <li class="actions"><a class="button thin" href="/en/products/surveillance">Compare All</a>
                        <div class="clear"/>
                      </li>
                    </ul>
                    <h3><a href="/en/products/products.aspx?id=170" title="CE/AV">CE/AV</a></h3>
                    <ul>
                      <li><a href="/en/products/products.aspx?id=170" title="WD AV">WD AV</a></li>
                    </ul>
                    <h3><a href="/en/products/products.aspx?id=20" title="Workstation">Workstation</a></h3>
                    <ul>
                      <li><a href="/en/products/products.aspx?id=20" title="WD VelociRaptor">WD VelociRaptor</a></li>
                    </ul>
                  </li>
                  <li class="group-menu">
                    <h3><a href="/en/products/internal/retailkits/" title="Internal Drive Kits">Internal Drive Kits</a></h3>
                    <ul>
                      <li><a href="/en/products/products.aspx?id=1070" title="Laptop Everyday">Laptop Everyday</a></li>
                      <li><a href="/en/products/products.aspx?id=1080" title="Desktop Everyday">Desktop Everyday</a></li>
                      <li><a href="/en/products/products.aspx?id=1320" title="Laptop Performance">Laptop Performance</a></li>
                      <li><a href="/en/products/products.aspx?id=1090" title="Desktop Performance">Desktop Performance</a></li>
                      <li><a href="/en/products/products.aspx?id=1060" title="WD NAS">WD NAS</a></li>
                      <li><a href="/en/products/products.aspx?id=1310" title="WD Surveillance">WD Surveillance</a></li>
                      <li class="actions"><a class="button thin" href="/en/products/internal/retailkits/">Compare All</a>
                        <div class="clear"/>
                      </li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
            <div class="side-bar">
              <div class="top-half">
                <div class="bar-padding">
                  <h2>Support</h2>
                  <ul>
                    <li><a href="http://mysupport.wdc.com?wdc_lang=en" title="WD Support Portal">WD Support Portal</a></li>
                    <li><a href="http://support.wdc.com/warranty/index.asp?wdc_lang=en" title="Warranty & RMA Services">Warranty & RMA Services</a></li>
                    <li><a href="http://support.wdc.com/product/kb.asp?wdc_lang=en" title="FAQ/Knowledge base">FAQ/Knowledge base</a></li>
                  </ul>
                </div>
              </div>
              <div class="bottom-half">
                <div class="bar-padding">
                  <div class="actions"><a  class="button thin" href="/en/products/catalog">Product List</a></div>
                </div>
                <div class="bar-padding">
                   <div><a  class="button thin" href="/backitup"><img src="/global/images/logos/Backitup_.png" border="0" alt="Back it Up"/></a></div>
                </div>
              </div>
            </div>
            <div class="clear"/>
          </div>
        </li>
        <li class="parent"><a href="javascript:void(0)" title="Personal Cloud">Personal Cloud<span class="up-arrow">&nbsp;</span></a>
          <div class="submenu-wrapper show-side-bar">
            <div class="submenu-padding">
              <div class="submenu-table">
                <ul class="submenu">
                  <li class="group-menu">
                    <h3><a href="/en/products/personalcloud/consumer/" title="Consumer Series">Consumer Series</a></h3>
                    <ul>
                      <li><a href="/en/products/products.aspx?id=1140" title="My Cloud">My Cloud</a></li>
                      <li><a href="/en/products/products.aspx?id=1200" title="My Cloud Mirror">My Cloud Mirror</a></li>
                      <li class="actions"><a runat="server" class="button thin" href="/en/products/personalcloud/consumer/">Compare All</a>
                        <div class="clear"/>
                      </li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
            <div class="side-bar">
              <div class="top-half">
                <div class="bar-padding">
                  <h2>Support</h2>
                  <ul>
                    <li><a href="http://mysupport.wdc.com?wdc_lang=en" title="WD Support Portal">WD Support Portal</a></li>
                    <li><a href="http://support.wdc.com/warranty/index.asp?wdc_lang=en" title="Warranty & RMA Services">Warranty & RMA Services</a></li>
                    <li><a href="http://support.wdc.com/product/kb.asp?wdc_lang=en" title="FAQ/Knowledge base">FAQ/Knowledge base</a></li>
                  </ul>
                </div>
              </div>
              <div class="bottom-half">
                <div class="bar-padding">
                  <div class="actions"><a  class="button thin" href="/en/products/catalog/default.aspx?segment=4#jump5">Product List</a></div>
                </div>
                <div class="bar-padding">
                  <div><a  class="button thin" href="/backitup"><img src="/global/images/logos/Backitup_.png" border="0" alt="Back it Up"/></a></div>
                </div>
              </div>
            </div>
            <div class="clear"/>
          </div>
        </li>
                <li class="parent"><a href="javascript:void(0)" title="Personal Cloud">Network Attached Storage<span class="up-arrow">&nbsp;</span></a>
          <div class="submenu-wrapper show-side-bar">
            <div class="submenu-padding">
              <div class="submenu-table">
                <ul class="submenu">
                  <li class="group-menu">
                    <h3><a href="/en/products/nas/expert/" title="Consumer Series">My Cloud Expert Series</a></h3>
                    <ul>
                      <li><a href="/en/products/products.aspx?id=1180" title="My Cloud EX2">My Cloud EX2</a></li>
                      <li><a href="/en/products/products.aspx?id=1170" title="My Cloud EX4">My Cloud EX4</a></li>
                      <li><a href="/en/products/products.aspx?id=1470" title="My Cloud EX2100">My Cloud EX2100</a></li>
                      <li><a href="/en/products/products.aspx?id=1460" title="My Cloud EX4100">My Cloud EX4100</a></li>
                      
                      <li class="actions"><a runat="server" class="button thin" href="/en/products/nas/expert/">Compare All</a>
                        <div class="clear"/>
                      </li>
                    </ul>
                     <h3><a href="/en/products/nas/business/" title="Consumer Series">My Cloud Business Series</a></h3>
                    <ul>
                      <li><a href="/en/products/products.aspx?id=1440" title="My Cloud DL100">My Cloud DL2100</a></li>
                      <li><a href="/en/products/products.aspx?id=1450" title="My Cloud DL4100">My Cloud DL4100</a></li>                   
                      <li class="actions"><a runat="server" class="button thin" href="/en/products/nas/business/">Compare All</a>
                        <div class="clear"/>
                      </li>
                    </ul>
                  </li>
                  <li class="group-menu">
                    <h3><a href="/en/products/internal/nas/" title="Expert Series">NAS Hard Drives</a></h3>
                    <ul>
                      <li><a href="/en/products/products.aspx?id=810" title="WD Red">WD Red</a></li>
                      <li><a href="/en/products/products.aspx?id=1280" title="WD Red Pro">WD Red Pro</a></li>
                      <li><a href="/en/products/products.aspx?id=580" title="WD Re">WD Re</a></li>
                      <li class="actions"><a runat="server" class="button thin" href="/en/products/internal/nas">Compare All</a>
                        <div class="clear"/>
                      </li>
                    </ul>
                    <!-- <h3><a href="/en/products/nas/solutions/" title="Expert Series">NAS Solutions</a></h3>-->
                    
                  </li>
                </ul>
              </div>
            </div>
            <div class="side-bar">
              <div class="top-half">
                <div class="bar-padding">
                  <h2>Support</h2>
                  <ul>
                    <li><a href="http://mysupport.wdc.com?wdc_lang=en" title="WD Support Portal">WD Support Portal</a></li>
                    <li><a href="http://support.wdc.com/warranty/index.asp?wdc_lang=en" title="Warranty & RMA Services">Warranty & RMA Services</a></li>
                    <li><a href="http://support.wdc.com/product/kb.asp?wdc_lang=en" title="FAQ/Knowledge base">FAQ/Knowledge base</a></li>
                  </ul>
                </div>
              </div>
              <div class="bottom-half">
                <div class="bar-padding">
                  <div class="actions"><a  class="button thin" href="/en/products/catalog/default.aspx?segment=4#jump5">Product List</a></div>
                </div>
                <div class="bar-padding">
                  <div><a  class="button thin" href="/backitup"><img src="/global/images/logos/Backitup_.png" border="0" alt="Back it Up"/></a></div>
                </div>
              </div>
            </div>
            <div class="clear"/>
          </div>
        </li>
        <li class="parent"><a href="javascript:void(0)" title="Home Entertainment">Home Entertainment<span class="up-arrow">&nbsp;</span></a>
          <div class="submenu-wrapper show-side-bar">
            <div class="submenu-padding">
              <div class="submenu-table">
                <ul class="submenu">
                  <li class="group-menu">
                    <h3><a href="/en/products/products.aspx?id=1270" title="WD TV Media Players">Media Players</a></h3>
                    <ul>
                      <li><a href="/en/products/products.aspx?id=1270" title="WD TV">WD TV</a></li>
                    </ul>
                  </li>
                  <li class="group-menu">
                    <h3><a href="/en/products/products.aspx?id=360" title="DVR Expander">DVR Expander</a></h3>
                    <ul>
                      <li><a href="/en/products/products.aspx?id=360" title="My Book AV">My Book AV</a></li>
                    </ul>
                    <h3><a href="/en/products/products.aspx?id=400" title="Multimedia Drives">Multimedia Drives</a></h3>
                    <ul>
                      <li><a href="/en/products/products.aspx?id=400" title="WD Elements Play">WD Elements Play</a></li>
                    </ul>
                  </li>

                  <li class="group-menu">
                    <h3><a href="/en/products/tvstorage" title="TV Storage">TV Storage</a></h3>
                    <ul>
                      <li><a href="/en/products/products.aspx?id=1010" title="My Book AV-TV">My Book AV-TV</a></li>
                      <li><a href="/en/products/products.aspx?id=1250" title="My Passport AV-TV">My Passport AV-TV</a></li>
                      <li class="actions"><a runat="server" class="button thin" href="/en/products/tvstorage">Compare All</a>
                        <div class="clear"/>
                      </li>
                    </ul>
                  </li>
                  <li class="group-menu">
                    <h3><a href="/en/products/products.aspx?id=1480" title="TV Storage">Gaming Storage</a></h3>
                    <ul>
                      <li><a href="/en/products/products.aspx?id=1480" title="My Passport X">My Passport X</a>
                        <div class="clear"/>
                      </li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
            <div class="side-bar">
              <div class="top-half">
                <div class="bar-padding">
                  <h2>Support</h2>
                  <ul>
                    <li><a href="http://mysupport.wdc.com?wdc_lang=en" title="WD Support Portal">WD Support Portal</a></li>
                    <li><a href="http://support.wdc.com/warranty/index.asp?wdc_lang=en" title="Warranty & RMA Services">Warranty & RMA Services</a></li>
                    <li><a href="http://support.wdc.com/product/kb.asp?wdc_lang=en" title="FAQ/Knowledge base">FAQ/Knowledge base</a></li>
                  </ul>
                </div>
              </div>
              <div class="bottom-half">
                <div class="bar-padding">
                  <div class="actions"><a  class="button thin" href="/en/products/catalog/?segment=3">Product List</a></div>
                </div>
                <div class="bar-padding">
                   <div><a  class="button thin" href="/backitup"><img src="/global/images/logos/Backitup_.png" border="0" alt="Back it Up"/></a></div>
                </div>
              </div>
            </div>
            <div class="clear"/>
          </div>
        </li>
        <li class="parent"><a href="javascript:void(0)" title="Business Solutions">Business Solutions<span class="up-arrow">&nbsp;</span></a>
          <div class="submenu-wrapper show-side-bar">
            <div class="submenu-padding">
              <div class="submenu-table">
                <ul class="submenu">
                  <li class="group-menu">
                    <h2><a href="/en/products/nas/business/" title="Network Attached Storage">Network Attached Storage</a></h2>
                      <h3><a href="/en/products/nas/business/" title="My Cloud Business Series">My Cloud Business Series</a></h3>
                    <ul>
                      <li><a href="/en/products/products.aspx?id=1440" title="My Cloud DL2100">My Cloud DL2100</a></li>
                      <li><a href="/en/products/products.aspx?id=1450" title="My Cloud DL4100">My Cloud DL4100</a></li>
                      <li class="actions"><a class="button thin" href="/en/products/nas/business/">Compare All</a>
                        <div class="clear"/>
                      </li>
                    </ul>
                    <h3><a href="/en/products/business/networkstorage/" title="Windows Storage Servers">Windows Storage Servers</a></h3>
                    <ul>
                      <li><a href="/en/products/products.aspx?id=610" title="WD Sentinel DX4000">WD Sentinel DX4000</a></li>
                      <li><a href="/en/products/products.aspx?id=1030" title="WD Sentinel RX4100">WD Sentinel RX4100</a></li>
                      <li><a href="/en/products/products.aspx?id=1380">WD Sentinel DX4200</a></li>
                      <li class="actions"><a class="button thin" href="/en/products/business/networkstorage/">Compare All</a>
                        <div class="clear"/>
                      </li>
                    </ul>
                    <h3><a href="/en/products/business/Windows-Servers/" title="Windows Servers">Windows Servers</a></h3>
                    <ul>
                      <li><a href="/en/products/products.aspx?id=1150" title="WD Sentinel DS5100">WD Sentinel DS5100</a></li>
                      <li><a href="/en/products/products.aspx?id=1160" title="WD Sentinel DS6100">WD Sentinel DS6100</a></li>
                      <li class="actions"><a class="button thin" href="/en/products/business/Windows-Servers/">Compare All</a>
                        <div class="clear"/>
                      </li>
                    </ul>
                  </li>
                  <li class="group-menu">
                    <h3><a href="http://www.arkeia.com/" title="Network Backup Solutions">Network Backup Solutions</a></h3>
                    <ul>
                      <li><a href="http://www.arkeia.com" title="Visit WD Arkeia">Visit WD Arkeia</a></li>
                    </ul>
                    <h3><a href="http://support.wdc.com/product/wdguardian.asp" title="WD Guardian Services">WD Guardian Services</a></h3>
                    <ul>
                      <li><a href="http://support.wdc.com/product/wdguardian.asp" title="Guardian Service Plans">Guardian Service Plans</a></li>
                    </ul>
                    <h3><a href="/en/products/products.aspx?id=930" title="Windows To Go Storage">Windows To Go Storage</a></h3>
                    <ul>
                      <li><a href="/en/products/products.aspx?id=930" title="My Passport Enterprise">My Passport Enterprise</a></li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
            <div class="side-bar">
              <div class="top-half">
                <div class="bar-padding">
                  <h2>Support</h2>
                  <ul>
                    <li><a href="http://mysupport.wdc.com?wdc_lang=en" title="WD Support Portal">WD Support Portal</a></li>
                    <li><a href="http://support.wdc.com/warranty/index.asp?wdc_lang=en" title="Warranty & RMA Services">Warranty & RMA Services</a></li>
                    <li><a href="http://support.wdc.com/product/kb.asp?wdc_lang=en" title="FAQ/Knowledge base">FAQ/Knowledge base</a></li>
                  </ul>
                </div>
              </div>
              <div class="bottom-half">
                <div class="bar-padding">
                  <div class="actions"><a  class="button thin" href="/en/products/catalog/?segment=36">Product List</a></div>
                </div>
                <div class="bar-padding">
                   <div><a  class="button thin" href="/backitup"><img src="/global/images/logos/Backitup_.png" border="0" alt="Back it Up"/></a></div>
                </div>
              </div>
            </div>
            <div class="clear"/>
          </div>
        </li>
        <li class="parent"><a href="http://support.wdc.com?wdc_lang=en" title="Support">Support<span class="up-arrow">&nbsp;</span></a>
          <div class="submenu-wrapper">
            <div class="submenu-padding">
              <div class="submenu-table">
                <ul class="submenu">
                  <li class="group-menu"> 
                    <h3><a href="http://support.wdc.com/?wdc_lang=en" title="Get Support">Get Support</a></h3>
                    <ul>
                      <li><a href="http://mysupport.wdc.com?wdc_lang=en" title="WD Support Portal">WD Support Portal</a></li>
                      <li><a href="http://support.wdc.com/warranty/index.asp?wdc_lang=en" title="Warranty & RMA Services">Warranty & RMA Services</a></li>
                      <li><a href="http://support.wdc.com/warranty/policy.asp?wdc_lang=en" title="Warranty Policy">Warranty Policy</a></li>
                      <li><a href="http://support.wdc.com/recovery/index.asp?wdc_lang=en" title="Data Recovery">Data Recovery</a></li>
                      <li><a href="http://support.wdc.com/product/kb.asp?wdc_lang=en" title="FAQ/Knowledge base">FAQ/Knowledge base</a></li>
                    </ul>
                  </li>
                  <li class="group-menu">
                    <h3>&nbsp; </h3>
                    <ul>
                      <li><a href="http://support.wdc.com/product/download.asp?wdc_lang=en" title="Downloads">Downloads</a></li>
                      <li><a href="http://support.wdc.com/product/install.asp?wdc_lang=en" title="Installation">Installation</a></li>
                      <li><a href="http://support.wdc.com/contact/index.asp?wdc_lang=en" title="Phone & Email Support">Phone & Email Support</a></li>
                      <li><a href="/en/library/" title="Document Library">Document Library</a></li>
                      <li><a href="/en/products/legacy/index.asp" title="Legacy Products">Legacy Products</a></li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
            <div class="clear"/>
          </div>
        </li>
        <li class="parent dark"><a href="http://store.westerndigital.com/promo/85417500">SHOP NOW</a></li>
        <li class="clear"/>
      </ul>
    </nav>
  </div>
</header></div>
     <div id="breadcrumb_v5"><span class="breadcrumbtext_v5"><script type="text/javascript" language="javascript">showPath()</script></span></div> 
    <div id="maincontainer"><!-- main container -->

        
<table cellpadding="0" cellspacing="0" width="100%" border="0" >
<tr>

	
	<td  align="left" valign="top" class="contentpadding">
		<table cellpadding="20" cellspacing="0" width="100%" border="0">

		<tr>
			<td>
				<b>Sorry, there is no WesternDigital.com web page matching your request</b>. <br/>
				It is possible you typed the address incorrectly, or that the page 
				no longer exists. As an option, you may visit any of the pages below 
				for information about Western Digital services and products. 
				<p></p>
				Please visit:
				<ul>
					<li><a href="/">Western Digital Homepage
					</a> 
					<li><a href="http://support.wdc.com/">Service and Support Homepage</a> 
					<li><a href="http://wdc.custhelp.com/">Support Knowledge Base</a> 
					<li><a href="/en/library/">Document Info Library</a>
					<li><a href="/en/products/catalog">Products Information</a> 
					<li><a href="http://support.wdc.com/contact/">Technical Contact Information</a> 
					<li><a href="/en/company/contact.asp">Corporate Contact Information</a>
				</ul>
				Our apologies for this inconvenience.<br />
				<div id="ctl00_ContentPlaceHolder_dastring"></div>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<div id="ctl00_ContentPlaceHolder_errorContents"></div>


    </div> <!-- end main container -->
    <div id="ctl00_footerNav">    <div id="wdsurvey-dialog" style="display:none;text-align:left;font-family:Lucida Grande,Lucida Sans Unicode,Arial;"><img src="/Global/images/logos/wdlogo100x54.jpg" width="100" height="54" style="padding-bottom:8px;" />
      <div style="font-family:Lucida Grande,Lucida Sans Unicode,Arial;">Your opinion is important.<br />
        Would you be willing to take our survey?</div>
    </div>
    <div id="footer-wrapper">
      <footer id="footer_v5">
        <div class="contentpadding">
          <section class="cell">
            <h3 class="title_bold10g">Company Information</h3>
            <div><a href="/en/company/employment/">Careers</a><br />
              <a href="http://investor.wdc.com/">Investor Relations</a><br />
              <a href="/en/company/communityrelations/">Corporate Philanthropy</a><br />
              <a href="/en/company/contact/">Contact WD</a><br />
              <a href="/en/company/branding/trademarks.aspx">Trademarks</a><br />
               <a href="/en/company/corporateinfo/copyrightpolicy.aspx">Copyright Policy</a><br />
            </div>
          </section>
          <section class="cell">
            <h3 class="title_bold10g">&nbsp;</h3>
            <div><a href="http://wdc.com/ventito">WD Ventito</a><br />
            <a href="/en/company/pressroom/socialmedia/">WD Social</a><br />
              <a href="/en/partners/">Partners</a><br />
              <a href="/en/company/pressroom/corporate/">Corporate News</a><br />
              <a href="/en/company/corporateinfo/privacy.aspx">Privacy Policy</a><br />
              <a href="/en/company/corporateinfo/privacy.aspx#Interest-Based-Ads">Interest-Based Ads</a><br />
              <a href="/en/company/corporateinfo/privacy.aspx#California">Your California Privacy Rights</a><br />
              
            </div>
          </section>
          <section class="cell">
            <h3 class="title_bold10g">Top Support Links</h3>
            <div><a href="http://mysupport.wdc.com?wdc_lang=en">WD Support Portal</a><br />
              <a href="http://support.wdc.com/product/kb.asp?wdc_lang=en">FAQ/Knowledge base</a><br />
              <a href="http://support.wdc.com/product/download.asp">Downloads</a><br />
              <a href="http://support.wdc.com/warranty/index.asp?wdc_lang=en">Warranty & RMA Services</a><br />
              <a href="http://support.wdc.com/product/kb.asp?wdc_lang=en">Technical Help & Training</a><br />
              <a href="http://community.wd.com/">WD Community</a><br />
              <a href="/en/products/legacy/">Legacy Products</a><br />
            </div>
          </section>
          <section class="cell">
            <h3 class="title_bold10g">Useful Site Links</h3>
            <div><a href="/en/products/reviews/">Reviews</a><br />
              <a href="/en/products/resources/drivecompatibility/">Drive Compatibility Guide</a><br />
              <a href="/en/sitemap/">Site Map</a><br />
              <a href="http://www.wd.com/mobile">Mobile Site</a><br />
              <a href="/en/company/corporateinfo/termsofuse.aspx">Terms of Use</a><br />
              <a href="/backitup"><img src="/global/images/logos/Backitup_65x45.png" width="65" border="0" alt="Back it Up"/></a><br />
              <div id='teconsent'><script type="text/javascript" src='//consent-st.truste.com/get?name=notice.js&domain=westerndigital.com&c=teconsent'></script></div>
            </div>
          </section>
        </div>
      </footer>
      <div class="legal center"><a href="http://www.ea-1.net/global_login.html"><img src="/global/images/footer/footerlogo.png" width="27" height="15" border="0" alt="" align="absmiddle" /></a>Copyright &copy;  2001 – 2015 Western Digital Technologies, Inc. All rights reserved</div>
    </div>
        </div> 

    </form>
    <!-- ================================================================ -->
<!-- ================= JS section  ================== -->
<!-- ================================================================ -->

    
    <script type="text/javascript" src="/scripts/compatibility.js"></script>
    <script type="text/javascript" src="/scripts/jquery.colorbox-min.js"></script>
    <script type="text/javascript" src="/scripts/swfobject.js"></script>
	<script type="text/javascript" src="/scripts/tooltip.jquery.js"></script>  
    <script type="text/javascript" src="/scripts/jquery.bxslider.min.js" ></script>
    <script type="text/javascript" src="/scripts/jquery.hoverIntent.minified.js"></script>
    <script type="text/javascript" src="/scripts/breadcrumb.js"></script>
    <script type="text/javascript" src="/scripts/jqwdc.js"></script>
      <script type="text/javascript" src="/scripts/expand.js"></script>
        <script type="text/javascript" src="/scripts/coin-slider.min.js"></script>

    <script type="text/javascript" charset="utf-8">
        $(document).ready(function() {
            //colorbox overlay scripts
            $("a[rel='elastic1']").colorbox();
            $("a[rel='fade1']").colorbox({ transition: "fade" });
            $("a[rel='ntfixed']").colorbox({ transition: "none", width: "75%", height: "75%" });
            $("a[rel='slideshw']").colorbox({ slideshow: true });
            $(".htmlflash").colorbox();
            $(".htmloverlay").colorbox({ iframe: true, innerWidth: 425, innerHeight: 344 });
            $(".iframealert").colorbox({ width: "700", height: "250", iframe: true });
            $(".iframeyoutube").colorbox({ width: "525", height: "450", iframe: true });
            $(".iframevideo").colorbox({ width: "800", height: "550", iframe: true });
            $(".iframevideoPL").colorbox({ width: "800", height: "735", iframe: true });
            $(".iframealert").colorbox({ width: "710", height: "375", iframe: true });
            $(".iframe1").colorbox({ iframe: true, innerWidth: 425, innerHeight: 344 });
            $(".iframe2").colorbox({ width: "80%", height: "80%", iframe: true });
            $(".iframeCatalog").colorbox({ width: "750", height: "700", iframe: true });
            $(".wtbitem").colorbox({ width: "900", height: "700", iframe: true });

            $(".inline1").colorbox({ width: "50%", inline: true, href: "#inline_elastic" });  
        });
    </script>
<!-- ================================================================ -->
<!-- ================= END JS section  ================== -->
<!-- ================================================================ -->
</body>
</html>
