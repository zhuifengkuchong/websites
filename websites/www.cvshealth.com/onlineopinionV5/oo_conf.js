/*
OnlineOpinion v5.8.1
Released: 02/27/2014. Compiled 03/12/2014 01:40:21 PM -0500
Branch: master Feb
Components: Full
UMD: disabled
The following code is Copyright 1998-2014 Opinionlab, Inc.  All rights reserved. Unauthorized use is prohibited. This product and other products of OpinionLab, Inc. are protected by U.S. Patent No. 6606581, 6421724, 6785717 B1 and other patents pending. http://www.opinionlab.com
*/
/*global OOo*/

/* Inline configuration */
window.oo_feedback = new OOo.Ocode({
/* pass variable into custom variable parameter Name corresponds to header on reports. Replace customVar with the name of the variable to be passed */
//    customVariables: {
//        Name1: (customVar !== undefined) ? customVar : '',
//        Name2: (customVar2 !== undefined) ? customVar2 : '',
//        Name2: (customVar3 !== undefined) ? customVar3 : ''
//        }
});