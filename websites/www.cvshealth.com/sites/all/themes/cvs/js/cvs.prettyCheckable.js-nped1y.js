
/**
 * Re-attach prettycheckable.
 */
(function($) {
  Drupal.behaviors.prettyCheckable = {
    attach: function(context, settings) {
      /* ===================================
        prettyCheckable()
        =================================== */
      /* Add data-label attribute to input[type="checkbox"] */
      var arr = $('input[type="checkbox"] + label');
      arr.each(function(){
        var dataLabel = $(this).text();
        $(this).addClass('helper');
        $(this).prev().attr('data-label', dataLabel);
      });
      // init
      $('input[type="checkbox"]', context).prettyCheckable();
      $('.has-pretty-child', context).addClass('clearfix');
    }
  };
})(jQuery);