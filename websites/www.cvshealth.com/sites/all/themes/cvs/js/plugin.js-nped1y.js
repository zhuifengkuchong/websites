(function ($) {

  /**
   * Plugin List:
   *
   * - columnGenerator
   * - arrangeObjects
   * 
   */

  /**
   * TODO: Chris, can you document this?
   */
  $.fn.columnGenerator = function(cols) {
    var $list=$(this).children('ul');
    var $container=$list.parent();
    var $li=$list.children('li');
    var totItems=$li.length;
    var totPerCol = Math.ceil(totItems/cols);
    for (i=0; i<=totItems; i+=totPerCol) {
    var slice = $li.slice(i,i+totPerCol).clone();
      slice.appendTo($container).wrapAll('<ul class="grid_'+ Math.floor(4/cols)+'" />');
    }
    $list.remove();
  };
  
  /* ========================
    Split Menu 
	======================== */
  $.fn.arrangeObjects = function(wrapWith, maxCols) {
  
    this.each(function() {
      if ($(this).parent(wrapWith).length) $(this).unwrap();
    });
  
    this.parent().each(function () {
      
      var $subnodes       = $(this).children();
  
      // true will cause counter increment
      // false will cause counter decrement
      var inc     = true;
      var cols    = [];
  
      for (var i = 0; i < maxCols; i++) {
        cols.push($('<ul></ul>'));
        cols[i].appendTo($(this));    
      }
  
      i = 0;
      $subnodes.each(function () {
        // logic for left and right boundry
        if (i < 0 || i === maxCols) {
          inc = !inc;
          // this will cause node to be added once again to the same column
          inc ? i++ : i--;
        }
  
        cols[i].append($(this));
  
        inc ? i++ : i--;
        
      });
    });
  };
  
})(jQuery);