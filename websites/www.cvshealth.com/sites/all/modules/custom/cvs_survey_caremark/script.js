(function ($) {
/**
 * Survey Modal JavaScript
 *
 * Depends on jQuery 1.7.1+ (load jQuery before this script)
 * Replace $.on with $.delegate if need to backport
 *
 * Usage:
 * Call with surveyModal.init();
 *
 * Called automatically on load (bottom of this script!)
 * Reads window.surveyDisabled variable
 */

////////////////////////////////////////////////////////////////////////////////
var surveyModal = (function () {
  var m = this; // self reference

  /**
   * default options - don't edit.
   * you should copy these to the BOTTON of this script and override them when
   * calling init()
   */
  this.options = {
    debug:       false, // debug to console at certain steps?
    ab:          false, // show to 50% of all users, overrides percentage
    percentage:  25,    // show to x% of all users, e.g. 25% would be 1/4 users
    mobileUrl:   'http://google.com/' // URL to pop open when click "yes"
  };
  /**
   * set ROOT DOMAIN cookie so lightbox never pops up again
   * set when user clicks yes or no
   */
  this.done = function (e) {
    var expires = new Date();
    expires.setTime(expires.getTime() + (3600 * 1000 * 24 * 365 * 10)); // 10 years
    document.cookie = 'isSurveyDoneCM=1;expires=' + expires.toGMTString() + ';path=/'; // unexpiring root cookie
    m.hide();
  };

  /**
   * read cookie determining if lightbox should popup
   * read before opening popup on init
   * @return bool true if isSurveyDone cookie value is 1
   */
  this.isSurveyDone = function () {
    var cookieValue = document.cookie.match('(^|;) ?isSurveyDoneCM=([^;]*)(;|$)');
    return (cookieValue && cookieValue.length >= 2 && cookieValue[2] === "1") ? true : false;
  };

  /**
   * Update Popup
   */
  this.show = function () {
    m.debug();
    $('#survey_modal').show();
    // Add correct mobileUrl 
    $('#survey_modal-yes').attr('href', m.options.mobileUrl);

    $("#survey_modal-yes").unbind("click").bind("click", function(e) {
      e.preventDefault();
      window.open($(this).attr("href"), "survey", "resizable=1,scrollbars=yes,width=770,height=830").blur();
      window.focus();
      m.done();
    });
  };

  /**
   * completely remove popup from DOM
   */
  this.hide = function (e) {
    $('#survey_modal').remove();
    m.debug();
  };

  // show mobile detection status and lightbox cookie
  this.debug = function () {
    if (!m.options.debug) {
      return;
    }
    console.log('isSurveyDone? ' + m.isSurveyDone());
  };

  // debugging method to unset cookie
  this.undo = function (e) {
    var expires = new Date();
    expires.setTime(expires.getTime() + (3600 * 1000 * 24 * 365 * 10)); // 10 years
    document.cookie = 'isSurveyDoneCM=0;expires=' + expires.toGMTString() + ';path=/'; // unexpiring root cookie
    m.debug();
  };

  /**
   * @param options object
   */
  this.init = function () {
    if (arguments.length) {
      m.options = $.extend(m.options, arguments[0]);
    }
    if (m.options.debug) { // show options
      console.log(m.options, 'cookied: ' + this.isSurveyDone());
    }

    // set cookie after click yes or no, so popup only appears once ever
    $('body').bind('click', '#survey_modal-yes, #survey_modal-no', m.done);

    // determine sample group, show if in 50% or percentage
    var isInSampleGroup = true;
    var pool = 2;

    if (m.options.ab) {
      isInSampleGroup = Math.floor(Math.random() * pool);
    }
    else if (m.options.percentage) {
      m.options.percentage = parseInt(m.options.percentage, 10);
      //roll a 100 sided die, accept numbers less than % 
      isInSampleGroup = Math.floor(Math.random() * 100) <= m.options.percentage;
    }

    if (isInSampleGroup) {
      // cookied?
      if (!m.isSurveyDone()) {
        m.show();
      }
      else if (m.options.debug){
        console.log('cookied, normally wouldn\'t show but debug mode is on');
        m.show();
      }
    }
    else {
      if (m.options.debug) {
        console.log('Not in sample group');
      }
    }
  };

  // expose methods
  return this;

}).call({});


////////////////////////////////////////////////////////////////////////////////
// Execution
////////////////////////////////////////////////////////////////////////////////
jQuery(document).ready(function ($) {
  surveyModal.init({
    // override default options here
    // debug: true,
    percentage: 100,
    mobileUrl:   'http://surveyanalytics.com/t/ADVheZNx07'
  });
});
 
}(jQuery));