<script id = "race10a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race10={};
	myVars.races.race10.varName="Object[32049].searching";
	myVars.races.race10.varType="@varType@";
	myVars.races.race10.repairType = "@RepairType";
	myVars.races.race10.event1={};
	myVars.races.race10.event2={};
	myVars.races.race10.event1.id = "edit-search-block-form--2";
	myVars.races.race10.event1.type = "onkeydown";
	myVars.races.race10.event1.loc = "edit-search-block-form--2_LOC";
	myVars.races.race10.event1.isRead = "False";
	myVars.races.race10.event1.eventType = "@event1EventType@";
	myVars.races.race10.event2.id = "edit-search-block-form--2";
	myVars.races.race10.event2.type = "onblur";
	myVars.races.race10.event2.loc = "edit-search-block-form--2_LOC";
	myVars.races.race10.event2.isRead = "True";
	myVars.races.race10.event2.eventType = "@event2EventType@";
	myVars.races.race10.event1.executed= false;// true to disable, false to enable
	myVars.races.race10.event2.executed= false;// true to disable, false to enable
</script>

