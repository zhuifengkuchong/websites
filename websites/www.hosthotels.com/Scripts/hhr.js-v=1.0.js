$(function () {

    var loc_href = window.location.pathname;
    $('#navv a').each(function () {
        if (loc_href == $(this).attr('href')) {
            $(this).addClass('active');
        }
    });

    window.onhashchange = locationHashChanged;

    $(document).prop('title', 'Host Hotels & Resorts, Inc. - ' + getPageName(document.URL));

    if (window.location.pathname=='http://www.hosthotels.com/ourProperties.asp')
	{
		parsePropertyXml();
		loadBrandImgs();
	}


    $('#banner-fade').bjqs({
        height: 220,
        width: 320,
        // animation values
        animtype: 'fade', // accepts 'fade' or 'slide'
        animduration: 3000, // how fast the animation are
        animspeed: 4000, // the delay between each slide
        automatic: true,  // automatic
        // control and marker configuration
        showcontrols: false, // show next and prev controls
        centercontrols: true, // center controls verically
        nexttext: 'Next', // Text for 'next' button (can use HTML)
        prevtext: 'Prev', // Text for 'previous' button (can use HTML)
        showmarkers: false, // Show individual slide markers
        centermarkers: true, // Center markers horizontally
        // interaction values
        keyboardnav: true, // enable keyboard navigation
        hoverpause: true, // pause the slider on hover
        // presentational options
        usecaptions: true, // show captions for images using the image title tag
        randomstart: false, // start slider at random slide
        responsive: true // enable responsive capabilities (beta)
    });

    //  find external link Add 'external' CSS class to all external links
    $.expr[':'].external = function (obj) {
        return !obj.href.match(/^mailto\:/)
                && (obj.hostname != location.hostname) && (obj.hostname != 'http://www.hosthotels.com/Scripts/ir.hosthotels.com') && (obj.hostname != 'http://www.hosthotels.com/Scripts/hr.hosthotels.com') && (obj.hostname != 'http://www.hosthotels.com/Scripts/phx.corporate-ir.net');
    };
    
    $('a:external').addClass('external');

    $('.external').click(function () {
        externalLink($(this));
    });

    //if jump to the nested tab from other pages
    if (window.location.pathname=='http://www.hosthotels.com/ourCompany.asp'){
		handleTabLinks();
	}
	else {
        if (location.hash.length>0)
        {
            $('ul.nav a[href="' + location.hash + '"]').tab('show');
        }
    }
    
    //links within the same page
    $(document).on('click.bs.tab.data-api', '[data-toggle="tab"], [data-toggle="pill"]', function (e) {
        e.preventDefault()
        $('ul.nav li a[href="' + $(this).attr('href') + '"]').tab('show');

        $(document.body).on("click", "a[data-toggle=open-tab]", function (event) {
            var scrollmem = $('body').scrollTop();
            $('html,body').scrollTop(scrollmem);
            location.hash = this.getAttribute("href");
            //alert(location.hash);
            location.reload(true);
        });
        //alert(location.hash);
    })

    $('.carousel').carousel()

});


function handleTabLinks() {
    if (window.location.hash == '') {
        window.location.hash = window.location.hash + '#_';
    }
    var hash = window.location.hash.split('#')[1];
    var prefix = '_';
    var hpieces = hash.split('/');
    for (var i = 0; i < hpieces.length; i++) {
        var domelid = hpieces[i].replace(prefix, '');
        var domitem = $('a[href=#' + domelid + '][data-toggle=tab]');
        if (domitem.length > 0) {
            domitem.tab('show');
        }
    }
    $('a[data-toggle=tab]').on('shown', function (e) {
        if ($(this).hasClass('nested')) {

            var nested = window.location.hash.split('/');
            window.location.hash = nested[0] + '/' + e.target.hash.split('#')[1];
        } else {
            window.location.hash = e.target.hash.replace('#', '#' + prefix);
        }
        
    });
}

$(document.body).on("click", "a[data-toggle=open-tab]", function (event) {
    var scrollmem = $('body').scrollTop();
    $('html,body').scrollTop(scrollmem);
    location.hash = this.getAttribute("href");

    if (!History.emulated.pushState) {
        history.pushState(null, null, '#'+location.hash.split('#')[2]);
    }
    else {
        location.hash = '#' + location.hash;
    }
   
    //location.reload(true);
});

function locationHashChanged() {
    
    if (location.hash.indexOf('#_') !== -1) {
        if (!History.emulated.pushState) {
            history.pushState(null, null, '#'+location.hash.split('#')[2]);
			location.reload(true);
        }
        else {
            location.hash = this.getAttribute("href");
			location.reload(true);
        }
    }
    //alert(location.hash);
    location.reload(true);
}

function externalLink(alink) {
    var link = $(alink).attr('data-href');
    
    var index = link.lastIndexOf('/') + 1;
    var filenameWithExtension = link.substr(index);
    var filetype = filenameWithExtension.split(".")[1];

    if (filetype != 'pdf') {
        if (link.indexOf('www') == -1)
        {
            link = link.replace('https://', '//www.');
        }
        else {
            link = link.replace('https://', '//');
        }
    }
    
    var $text = $('<div></div>');
    $text.append('By clicking on this link you are leaving the Host Hotels & Resorts® website and accessing a third party website.  The link is provided for your convenience only.  Host Hotels & Resorts® exercises no control over the organization, content, views, accuracy, copyright or trademark compliance, or legality of the material contained in the third party website, and Host accepts no responsibility for such website or for any loss or damage that may arise from your use of it.  If you decide to access any third party website linked to the Hotels & Resorts® website, you do so entirely at your own risk and subject to the terms and conditions of use for such website. <br /><br />');
    $text.append('The content of the site you enter from our site does not reflect the opinions, standards, or policy of Host Hotels & Resorts®.');

    var dialog = new BootstrapDialog({
        title: 'Disclaimer for External Links',
        message: $text,
        buttons: [{
            icon: 'glyphicon glyphicon-new-window',
            label: 'Acknowledge & Continue',
            cssClass: 'btn-warning',
            action: function (dialogRef) {
                dialogRef.enableButtons(false);
                dialogRef.setClosable(false);

                dialogRef.close();
                window.open(link);

            }
            //}, {
            //    label: 'Close',
            //    action: function (dialogRef) {
            //        dialogRef.close();
            //    }
        }]

    });
    dialog.open();
}

function collapsibleLink(alink) {
    var id = $(alink).attr('id');
    $('#' + id).collapse('toggle');

    var t = $(alink).children('span:first').attr('class');
    if (t.indexOf("right") < 0) {
        $(alink).children("span:first").removeClass("fa-caret-down");
        $(alink).children('span:first').addClass("fa-caret-right");
    }
    else {
        $(alink).children("span:first").removeClass("fa-caret-right");
        $(alink).children('span:first').addClass("fa-caret-down");
    }
}

function parsePropertyXml() {
    var s;
    var items = [];
    var temp = [];
    var brands = new Array();
    var countries = new Array();
    var states = new Array();
   
   
    $.get('http://www.hosthotels.com/Scripts/xml/hmProperties.xml',{}, function (xml) {
        
        $("property", xml).each(function () {
            
            items.push({
                name: $(this).find('title').text(),
                brand: $(this).find('brand').text(),
                url: $(this).find('aLink').text(),
                country: $(this).find('country').text(),
                state: $(this).find('state').text()
            });
            
            brands.push($(this).find('brand').text());
            if ($(this).find('country').text() != 'US') {
                countries.push($(this).find('country').text());
            }
            else
            {
                states.push($(this).find('state').text());
            }

            
        });
        brands = unique(brands).sort();
        countries = unique(countries).sort();
        states = unique(states).sort();

        temp = items.sort(sorter('name'));
        for (i = 0; i < temp.length; i++) {
            s = "<tr><td>";
            s += "<a onclick='externalLink(this)' class='external' data-href='" + temp[i].url + "'>" + temp[i].name + "</a>";
            s += "</td></tr>";
            $(s).appendTo($('#hotelName'));
        }
        
        $.each(states, function (index, state) {
            var st = state.replace(/[^A-Z0-9]+/ig, '');
            s = "<tr><td>";
            s += "<a onclick='collapsibleLink(this)' class='collapsibleLink' data-toggle='collapse' href='#" + st + "' aria-expanded='false' aria-controls='" + st + "'><span class='fa fa-caret-right'> " + state + "</span></a>";
            s += "<div class='collapse' id='" + st + "'>";
            s += "<table class='table hhrPropertyTable' style='margin-top:10px'>";

            temp = jQuery.grep(items, function (a) {
                 return a.state == state;
            });
            temp.sort(sorter('name'));
            
            for (i = 0; i < temp.length; i++) {
                s += "<tr><td>";
                s += "<a onclick='externalLink(this)' class='external' data-href='" + temp[i].url + "'>" + temp[i].name + "</a>";
                s += "</td></tr>";
            }

            s += "</table>";
            s += "</div>";
            s += "</td></tr>";
            $(s).appendTo($('#state'));
        });

        $.each(countries, function (index, countrie) {
            var co = countrie.replace(/[^A-Z0-9]+/ig, '');
            s = "<tr><td>";
            s += "<a onclick='collapsibleLink(this)' class='collapsibleLink' data-toggle='collapse' href='#" + co + "' aria-expanded='false' aria-controls='" + co + "'><span class='fa fa-caret-right'> " + countrie + "</span></a>";
            s += "<div class='collapse' id='" + co + "'>";
            s += "<table class='table hhrPropertyTable' style='margin-top:10px'>";

            temp = jQuery.grep(items, function (a) {
                return a.country == countrie;
            });
            temp.sort(sorter('name'));

            for (i = 0; i < temp.length; i++) {
                s += "<tr><td>";
                s += "<a onclick='externalLink(this)' class='external' data-href='" + temp[i].url + "'>" + temp[i].name + "</a>";
                s += "</td></tr>";
            }

            s += "</table>";
            s += "</div>";
            s += "</td></tr>";
            $(s).appendTo($('#country'));
        });

        //console.log(brands);
        $.each(brands, function (index, brand) {
            var b = brand.replace(/[^A-Z0-9]+/ig, '');
            s = "<tr><td>";
            s += "<a onclick='collapsibleLink(this)' class='collapsibleLink' data-toggle='collapse' href='#" + b + "' aria-expanded='false' aria-controls='" + b + "'><span class='fa fa-caret-right'> " + brand + "</span></a>";
            s += "<div class='collapse' id='" + b + "'>";
            s += "<table class='table hhrPropertyTable' style='margin-top:10px'>";

            temp = jQuery.grep(items, function (a) {
                return a.brand == brand;
            });
            temp.sort(sorter('name'));
            
            for (i = 0; i < temp.length; i++) {
                s += "<tr><td>";
                s += "<a onclick='externalLink(this)' class='external' data-href='" + temp[i].url + "'>" + temp[i].name + "</a>";
                s += "</td></tr>";
            }

            s += "</table>";
            s += "</div>";
            s += "</td></tr>";
            $(s).appendTo($('#brandName'));
        });
       
    });
    
}

function loadBrandImgs() {

    var dir = "pictures/brands/";
    var fileextension = ".gif";
    var html = "";
    $.ajax({
        //This will retrieve the contents of the folder if the folder is configured as 'browsable'
        url: dir,
        success: function (data) {
            //Lsit all png file names in the page
            $(data).find("a:contains(" + fileextension + ")").each(function () {
                var url = this.href;
                var filename = url.substring(url.lastIndexOf('/') + 1);
                var images = "<img src='" + dir + filename + "'></img>";
                //html += "<img src=" + dir + '/' + filename + "></img>" + " width=125px;>";
                if (filename != "Unknown_83_filename"/*tpa=http://www.hosthotels.com/Scripts/TheStarwoodBrands.gif*/) {
                    images += "<br/><br/>";
                }
                $("#brandImgs").append($(images));
            });
        }
    });
   
}

function sorter(propName) {
    return function (a, b) {
        // The following won't work for strings
        // return a[propName] - b[propName];
        var aVal = a[propName].toUpperCase(), bVal = b[propName].toUpperCase();
        return ((aVal < bVal) ? -1 : ((aVal > bVal) ? 1 : 0));
    };
}
function unique(array) {
    return $.grep(array, function (el, index) {
        return index == $.inArray(el, array);
    });
}

function getPageName(url) {
    var index = url.lastIndexOf("/") + 1;
    var filenameWithExtension = url.substr(index);
    var filename = filenameWithExtension.split(".")[0]; 
    return filename;                                    
}

