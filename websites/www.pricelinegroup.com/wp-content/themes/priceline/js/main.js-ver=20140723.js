var mobile_pl;
var tablet_pl;
var screen_pl;
var screen_large_pl;

$(document).ready(function() {

    setDevice();
            
    //MAIN NAVIGATION
    $(".menu-button").click(function(){
        $(this).parents('#header-nav').toggleClass('nav-active');
    });
    
    $(".menu-item-has-children > a").click(function(){
        $('#menu-main-menu .sub-menu').hide();
        $(".sub-menu",$(this).parent()).show();
        return false;
    })
    // LOGO HOVER ANIMATION
    $("#header h1 > div").hover(function(){
        $(this).parents("#header").addClass("hover");
    },function(){
        $(this).parents("#header").removeClass("hover");
    })
    // LOGO animation on homepage
    setTimeout(function(){
        $("body.home #header h1 > div, body.home #header-nav .bg .logo").fadeIn();
    },3000);
    
  // NEWS more/less
  $('.news-col .more').on('click', function(e) {
    $(this).parent().find('.more-text').slideDown();
    $(this).hide();
    return false;
  });
  $('.news-col .less').on('click', function(e) {
     $(this).parent().slideUp(function(){
       $(this).parents('article').find('.more').show();
     });
     return false;
  });

  // TABS
  $('.tabs-nav.active').superSimpleTabs({selected: 1, callback: responsiveAdjust });
  // TIMELINE
    set_date_pop($(".timeline .date-on .date-pop"));
    $('.timeline .date').on('mouseover',function(){
        $('.timeline .date').removeClass('date-on');
        set_date_pop($(".date-pop",$(this)));
        $(this).addClass('date-on');
    });
  // MEDIA TABS onclick
  $(".page-media-room header h1 a > span").text($(".page-media-room .tabs-nav li > a[href="+document.location.hash+"]").attr('data-title'));
  $(".page-media-room .tabs-nav li > a").click(function(){
      $(".page-media-room header h1 a > span").text($(this).attr('data-title'));
  })


  // MAP overlay trigger

  $('a[href="#brands-map"]').fancybox({
        type: 'inline',
        showCloseButton: true,
        onStart: function() {  
                // $('#brands-map').css({width: '100%'}); 
                var mW = $('#brands-map').width();
                var newH = mW / 1.54 + 190;
                if($(window).width() > 767) newH = $(window).height() * 0.8;
                
                $('#brands-map').css({height: newH}).show(); 
                
        } ,
        onComplete: function(a) {  prepairMaps($(a).attr('data-destination-map')); $('#fancybox-outer, #fancybox-content > div').css({overflow : 'hidden'}); },
        onCleanup: function() { $('#brands-map').hide(); }
    });
  

    twitter_slider();
    responsiveAdjust();
    
    $(window).resize(function() {
        setDevice();
        responsiveAdjust();
        $.fancybox.close();
    });
    
    

    $('a#category-filter').fancybox({
        type: 'inline',
        showCloseButton: false,
        autoScale: true,
        onStart: function() { $('#media-tab-nav').show(); },
        onClosed : function() { $('#media-tab-nav').hide(); $('#category-filter span').text($('.tab-link.selected').text()); }
    });

    $('#media-tab-nav .tab-link').click(function() {
           $.fancybox.close() ;
    });
    
    $('.share-span').click(function(){
        $('#share-wrapper').toggleClass('show-share-icons');
    });
    
});

function setDevice(){
    $('body').addClass('desktop-display');
    if(window.innerWidth <= 767 || /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        $('body').removeClass('desktop-display');
        mobile_pl = true;
        screen_large_pl = screen_pl = tablet_pl = false;
    } else if(window.innerWidth <= 1024){
        tablet_pl = true;
        screen_large_pl = screen_pl = mobile_pl = false;
    }else if (window.innerWidth <= 1530){
        screen_pl = true;
        screen_large_pl = tablet_pl = mobile_pl = false;
    }else{
        screen_large_pl = true;
        mobile_pl = tablet_pl = screen_pl = false;
    }
}


function twitter_slider(){
    setTimeout(function(){
        jQuery("#happening-now .social #tweets-slider .slider-container").animate({top:-100},400,function(){
            var windowW = $(window).width();
            
            var tNum = 3;
            if(windowW < 980) tNum = 1;
            else if(windowW < 1200) tNum = 2;
            
            var tweets = $("#tweets-slider .slider-container article:lt(" + tNum + ")");
            $(this).append(tweets);
            $(this).css({top:0});
        })
        twitter_slider();
    },15000)
}

function facebook_share(url) {
    var left_padding = Math.ceil($('body').width()/2 - 250);
    window.open('https://www.facebook.com/sharer/sharer.php?u='+url, 'Share_this_link','width=500,height=200,location=no,left='+left_padding);
    return false;
}

function twitter_share(url, text) {
    text = text=="undefined" ? '' : text
    url = encodeURIComponent(url);
    text = encodeURIComponent(text);
    var left_padding = Math.ceil($('body').width()/2 - 250);
    window.open('https://twitter.com/share?text='+text+'&url='+url, 'Share_this_link', 'width=500,height=300,location=no,left='+left_padding);
    return false;
}



function set_date_pop(el_date_pop){
    if (el_date_pop.length > 0){
        var wrap = $(".wrap.history");
        if (el_date_pop.offset().left < wrap.offset().left+10)
            el_date_pop.css({left: parseInt(el_date_pop.css("left"))+(wrap.offset().left-el_date_pop.offset().left+10)});
        if (el_date_pop.offset().left+el_date_pop.outerWidth(true) >= wrap.offset().left+wrap.outerWidth()-10)
            el_date_pop.css({left: parseInt(el_date_pop.css("left"))-(el_date_pop.offset().left+el_date_pop.outerWidth(true)-(wrap.offset().left+wrap.outerWidth()-10))});
    }
}

function getPostsPage(target, post_type, category, page, s) {
    $(target).fadeOut();
    $.post('http://www.pricelinegroup.com/wp-admin/admin-ajax.php', {action: 'get_posts_page', page: page, category: category, post_type: post_type, s : s}, function(data) {
        $(data).find('.nav').css({height: 0});        
        $(target).replaceWith(data);
        responsiveAdjust();
    });
}


// =============================
// Responsive elements hights
// =============================
function responsiveAdjust() {
    var mt = parseInt($('.glamour-slider, .brand-pic').css('margin-top'));
    $('#wrapper').css({paddingBottom: $('#footer').height() + mt});
    var windowW = $(window).width();
    
    if(windowW > 767)  { 
        setMaxHeight($('.about-nav'), 'article');
        
        $('.news-group, #board-of-directors').each(function(idx,ng){
            setMaxHeightColumns($(ng).find('.news-col'), 'article');
        });
        
        $('.media.active .articles').each(function(idx,ng){
            setMaxHeightColumns($(ng).find('.articles-col, .articles-col-2'), 'article');
        });
        $('#media-tab-nav').show();
    } else { 
        $('#media-tab-nav').hide();
        mobileFooterBorders();
        mobileHiostryPopupsPosition();
    }
    
    $('.brand-data article').equalizeHeights();
}

function setMaxHeight(parent, child) {
    $(parent).each(function(idx, pItem) {
        var max = 0;
        $(pItem).find(child).css({minHeight : '0px' });
        $(pItem).find(child).each(function(idx, item) { 
                if(max == 0 || $(item).outerHeight() > max) { 
                    max = $(item).outerHeight();
                }
            });
        
        $(pItem).find(child).each(function(idx, item) { 
            $(item).css({minHeight: max + "px"});
        }); 
       
    });
    
}

function setMaxHeightColumns(columns, child) {
    for(var i=0; i < columns.first().find(child).length; i++) {
        var max = 0;
        $(columns).each(function(idx, pItem) {
            $(pItem).find(child).eq(i).css({minHeight : '0px' });
            if(max == 0 || $(pItem).find(child).eq(i).outerHeight() > max) { 
                max = $(pItem).find(child).eq(i).outerHeight();
            }
        });   
        
        $(columns).each(function(idx, pItem) {
            $(pItem).find(child).eq(i).css({minHeight: max + "px"});
        });
    }
}

function mobileHiostryPopupsPosition() {
    $('.timeline .year .date-pop').each(function(){
        var yTop = $(this).offset().top;
        var tTop = $('.timeline').offset().top;
        
        var diff = tTop - yTop;
        
        if(diff > 0) { 
            var newTop = "top:" +   parseInt(diff -146) + "px !important";
            $(this).attr("style", newTop);
        } else { 
            var yH = $(this).outerHeight();
            var tH = $('.timeline').outerHeight();
            
            var bDiff = yTop + yH -  tTop - tH;
            if( bDiff > 0 ) { 
                var newTop = "top:" + (-134 - parseInt(bDiff)) + "px !important";
                $(this).attr("style", newTop);
            }
        }
    });
}

function mobileFooterBorders(){
    $('#footer nav li').each(function(){
        if($(this).offset().left == $('#footer nav li').first().offset().left)  $(this).addClass('no-border');
        else $(this).removeClass('no-border');
    });

}


$.fn.equalizeHeights = function() {
    var maxHeight = this.map(function( i, e ) {
        $(e).css({height: 'auto'});
        var hTmp = $( e ).outerHeight();
        return hTmp;
    }).get();

    return this.height( Math.max.apply( this, maxHeight ) );
}

function animateWhenScrolledTo(data, chartdiv, type) {

    if (undefined != chartsDrawn[chartdiv]) return false;            

    var hT = $('#' + chartdiv).offset().top,
            hH = $('#' + chartdiv).outerHeight(),
            wH = $(window).height(),
            wS = $(window).scrollTop();
    if (wS > (hT + hH - wH)) {
        if(type === 'line') {
            drawLineGraph(data, chartdiv);                
        } else { 
            drawChart(data, chartdiv);
        }
        chartsDrawn[chartdiv] = true;
    }
}


function drawChart(data, wrapperId) {
                            AmCharts.makeChart(wrapperId,
                                    {
                                    "type": "serial",
                                    "pathToImages": "http://cdn.amcharts.com/lib/3/images/",
                                    "categoryField": "category",
                                    "columnSpacing": 10,
                                    "columnWidth": 0.86,                                    
                                    "startDuration": 0.75,                                    
                                    "fontSize": 12,
                                    "handDrawScatter": 0,
                                    "handDrawThickness": 0,
                                        "categoryAxis": {
                                            "gridPosition": "start",
                                            "color": "#000000",
                                            "fontSize": 12,
                                            "gridAlpha": 0,
                                            "gridColor": "#E7E7E7",
                                            "gridCount": 0,
                                            "gridThickness": 0,
                                            "minHorizontalGap": 69,
                                            "minorGridAlpha": 0,
                                            "minVerticalGap": 19,
                                            "titleFontSize": 0
                                        },
                                        "trendLines": [],
                                        "graphs": [
                                            {
                                                "colorField": "color",
                                                "fillAlphas": 1,
                                                "id": "AmGraph-1",
                                                "lineColorField": "color",
                                                "title": "graph 1",
                                                "type": "column",
                                                "valueField": "column-1"
                                            }
                                        ],
                                        "guides": [],
                                        "valueAxes": [
                                            {
                                                "gridType": "circles",
                                                "id": "ValueAxis-1",
                                                "axisThickness": 0,
                                                "fontSize": 3,
                                                "gridThickness": 0,
                                                "labelRotation": -7.2,
                                                "labelsEnabled": false,
                                                "tickLength": 0,
                                                "title": "",
                                                "titleFontSize": 0
                                            }
                                        ],
                                        "allLabels": [],
                                        "balloon": {
                                            "borderColor": "#000000",
                                            "borderThickness": 0,
                                            "color": "#FFFFFF",
                                            "fillAlpha": 1,
                                            "fillColor": "#000000",
                                            "fontSize": 14,
                                            "offsetX": 0,
                                            "offsetY": 10
                                        },
                                        "titles": [
                                            {
                                                "id": "Title-1",
                                                "size": 15,
                                                "text": ""
                                            }
                                        ],
                                        "dataProvider": data
                                    }
                            );
    
    
}

function drawLineGraph(data, wrapperId) {
			AmCharts.makeChart(wrapperId,
			{
                    "type": "serial",
                    "pathToImages": "http://cdn.amcharts.com/lib/3/images/",
                    "categoryField": "category",
                    "columnSpacing": 15,
                    "columnWidth": 1,
                    "autoMarginOffset": 12,
                    "marginBottom": 13,
                    "plotAreaBorderColor": "#FF0000",
                    "plotAreaFillColors": "#FF0000",
                    "startDuration": 1,
                    "fontSize": 12,
                    "handDrawScatter": 0,
                    "handDrawThickness": 0,
                    "theme": "light",
                    "categoryAxis": {
                        "axisAlpha": 1,
                        "axisThickness": 7,
                        "gridAlpha": 1,
                        "gridColor": "#F4F4F4",
                        "gridThickness": 2,
                        "minorGridAlpha": 1,
                        "tickLength": 0
                    },
                    "trendLines": [],
                    "graphs": [
                        {
                            "balloonColor": "#A2B0C0",
                            "balloonText": "",
                            "bullet": "round",
                            "bulletBorderThickness": 0,
                            "bulletSize": 3,
                            "color": "A2B0C0",
                            "connect": false,
                            "fontSize": 20,
                            "gapPeriod": 5,
                            "id": "AmGraph-1",
                            "lineColor": "#A2B0C0",
                            "lineThickness": 4,
                            "precision": 0,
                            "title": "graph 1",
                            "type": "smoothedLine",
                            "valueField": "column-1"
                        }
                    ],
                    "guides": [],
                    "valueAxes": [
                        {
                            "id": "ValueAxis-1",
                            "minMaxMultiplier": 0,
                            "radarCategoriesEnabled": false,
                            "synchronizeWith": "ValueAxis-1",
                            "axisAlpha": 0,
                            "axisThickness": 0,
                            "fontSize": 0,
                            "gridAlpha": 1,
                            "gridColor": "#F4F4F4",
                            "gridCount": 7,
                            "gridThickness": 2,
                            "minorGridAlpha": 0,
                            "tickLength": 0,
                            "title": "",
                            "titleBold": false
                        }
                    ],
                    "allLabels": [],
                    "balloon": {
                        "borderColor": "#FF0000",
                        "borderThickness": 0,
                        "color": "#FFFFFF",
                        "fillAlpha": 0,
                        "fillColor": "#FF0000",
                        "fontSize": 0,
                        "horizontalPadding": 12,
                        "pointerWidth": 7,
                        "shadowAlpha": 0,
                        "verticalPadding": 12
                    },
                    "titles": [],
                    "dataProvider": data
                }
			);
}

function unescapeArray(arr) {
    $(arr).each(function(idx, item) { 
        var d = document.createElement("div");
        d.innerHTML = item.category;        
        item.category = $(d).text();              
    });
    return arr;
    
}
