/* Codeev cdvSlider v1.0.0 */
/* CHANGE LOG */
/*
    1. Create pagination functionality
*/


(function($){
    
    $.fn.cdvSlider = function(options){
	
	var _this = this;
	var args = $.extend({
	    slideClass : "cdv-slide",
	    animationTime : 1500,
	    animationEasing: "swing",
	    interval: 6000,
	    auto: true,
	    startSlideIndex : 1,
	    delayEffect: false,
	    delayContainers: "",
	    nextClass: "cdv-slide-next",
	    prevClass: "cdv-slide-prev",
	    pagination: {
	        pagContClass: "cdvSlider-pagination"
	    }
	},options);	

	var curr, next,prev,slideTimeout,slidesCount=0,container,isAnimate,isOneEl=false;
    if($("."+args.slideClass,_this).length <= 1) isOneEl = true;
	/* SET STYLES */
	function set_styles(){
	    _this.css({position:"relative",overflow:"hidden"});	    
	    container = _this.wrapInner("<div class='cdvSlider-container' style='position:relative' />").children().first().css({width:"200%"});	    
	    _this.find("."+args.slideClass).each(function(i,el){		
		    $(el).css({position:"relative",float:"left",width:"50%"}).attr("data-slide-index",i+1);
		    (i+1 != args.startSlideIndex) ? $(el).hide() : $(el).show();
		    slidesCount++;
	    });	    
	    if(args.delayEffect){
		    $(_this).find(args.delayContainers).css({position:"relative"});
	    }
	}	
	
	/* pagination functions */
	function setPagination(){
	    var pagContainer = _this.find("."+args.pagination.pagContClass).length ? $("."+args.pagination.pagContClass) : $("<div class='"+args.pagination.pagContClass+"'></div>").appendTo(container.parent());
	    for(var i=1; i <= slidesCount; i++)
	        $("<a href='#' data-slide-index='"+i+"'>"+i+"</a>").appendTo(pagContainer);
	    pagContainer.find("a[data-slide-index=1]").addClass("current");
	    pagContainer.find("a").click(function(){
	        if(!isAnimate && parseInt($(this).attr("data-slide-index")) != curr){
    	        next = parseInt($(this).attr("data-slide-index"));
    	        nextSlide();
	        }
	    })
	}
	
    function setCurrentPaginationEl(el_index){
        var pagContainer = _this.find("."+args.pagination.pagContClass);
        pagContainer.find("a").removeClass("current");
        pagContainer.find("a[data-slide-index="+el_index+"]").addClass("current");
        
    }
	function nextSlide(){
	    if(!isAnimate){
            clearTimeout(slideTimeout);
    	    isAnimate = true;
	        if (args.pagination)
	            setCurrentPaginationEl(next);
    	    var curr_el = $("."+args.slideClass+"[data-slide-index="+curr+"]");
    	    var next_el = $("."+args.slideClass+"[data-slide-index="+next+"]").show();
    	    curr_el.after(next_el);
    	    next_el.find(args.delayContainers).css({left:0});
    	    container.css({left:0});
    	    
    	    if (args.delayEffect){
    		container.delay(args.animationTime/2);
    		delayAnimation(curr_el);
    		setTimeout(function(){
    		    delayAnimation(next_el,true);
    		},args.animationTime/2);
    	    }
    
    	    container.animate({left:-$(_this).width()},args.animationTime/2,args.animationEasing,function(){
        		curr_el.hide();
        		if(args.delayEffect) curr_el.find(args.delayContainers).css({left:0});
        		if(args.auto) autoAnimate();
        		container.css({left:0});
        		setNewSlideVal();
        		isAnimate = false;
    	    });
	    }
	}

	function prevSlide(){
	    if(!isAnimate){
	        clearTimeout(slideTimeout);
    	    isAnimate = true;
    	    if(args.pagination)
    	        setCurrentPaginationEl(prev);
    	    var curr_el = $("."+args.slideClass+"[data-slide-index="+curr+"]");
    	    var prev_el = $("."+args.slideClass+"[data-slide-index="+prev+"]").show();
    	    curr_el.before(prev_el);
    	    prev_el.find(args.delayContainers).css({left:0});
    	    container.css({left:-$(_this).width()});
    	    
    	    if(args.delayEffect){
    		container.delay(args.animationTime/2);
    		delayAnimation(curr_el,false,true);
    		setTimeout(function(){
    		    delayAnimation(prev_el,true,true);
    		},args.animationTime/2);
    	    }	    
    
    	    container.animate({left:0},args.animationTime/2,args.animationEasing,function(){
    		curr_el.hide();
    		if(args.delayEffect) curr_el.find(args.delayContainers).css({left:0});
    		if(args.auto) autoAnimate();
    		container.css({left:0});
    		setNewSlideVal(true);
    		isAnimate = false;
    	    });	    
	    }
	}
	
	function setNewSlideVal(is_prev){
	    if(is_prev)
	        curr = prev;
	    else
	        curr = next;
		prev = (curr == 1) ? slidesCount : curr-1;
	    next = (curr == slidesCount) ? 1 : curr+1;
	}

	function delayAnimation(slide,is_slide_in,is_prev){
	    var delaySlides = $(slide).find(args.delayContainers);
	    var delayTime = args.animationTime/2/delaySlides.length;
	    delaySlides.each(function(i,el){
		if(is_prev && is_slide_in){
		    $(el).css({left:-$(_this).width()}).delay(i*delayTime).animate({left:0},args.animationTime/2,args.animationEasing);
		}else if(is_prev && !is_slide_in){
		    $(el).css({left:0}).delay(i*delayTime).animate({left:$(_this).width()},args.animationTime/2,args.animationEasing);
		}else if(!is_prev && is_slide_in){
		    $(el).css({left:$(_this).width()}).delay(i*delayTime).animate({left:0},args.animationTime/2,args.animationEasing);
		}else{
		    $(el).css({left:0}).delay(i*delayTime).animate({left:-$(_this).width()},args.animationTime/2,args.animationEasing);
		}
	    });
	}

	function stop(){
	    clearTimeout(slideTimeout);
	}

	function autoAnimate(){
	    clearTimeout(slideTimeout);
	    slideTimeout = setTimeout(function(){
		nextSlide();
	    },args.interval);
	}

    
	set_styles();
	if(!isOneEl) {
    	if(args.pagination)
    	    setPagination();
    	curr = args.startSlideIndex;
    	next = (curr == slidesCount) ? 1 : curr+1;
    	prev = (curr == 1) ? slidesCount : curr-1;
    	if(args.auto) autoAnimate();
    	if(!args.delayEffect) args.animationTime = args.animationTime*2;
    
        $("."+args.nextClass).on("swipeleft",function(e){
            console.log("swipe left");
            prevSlide();
            return false;
        })
    
        $(_this).on("swipeleft",function(e){
            nextSlide();
            return false;
        });
        $(_this).on("swiperight",function(e){
            prevSlide();
            return false;
        });
    
    	$("."+args.nextClass).click(function(){
    		nextSlide();
    	    return false;
    	});
    
    	$("."+args.prevClass).click(function(){
    		prevSlide();
    	    return false;
    	});
    
    	$(_this).find("."+args.slideClass).mouseover(function(){
    	    clearTimeout(slideTimeout);
    	})
    
    	$(_this).find("."+args.slideClass).mouseout(function(){
    	    if(args.auto) autoAnimate();
    	})
	} else {
	    $("."+args.nextClass+",."+args.prevClass).addClass("cdv-slide-block");
	}

	return this;
    };


}(jQuery));