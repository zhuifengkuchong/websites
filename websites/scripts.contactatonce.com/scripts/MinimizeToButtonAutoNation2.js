var _imAction;

var objAutoNationDrop = {};

function PostDropinCustomFunction() {
    setTimeout(function(){MinimizeCaoPopin()},10000);
}

function imActionWithQuestion(imAction)
{
	
	var urlEncodedQuestion = escape(document.getElementById('textbox').value);
	
	var imActionWithInput = imAction.replace(/',''/g, "&InitialMessage="+urlEncodedQuestion+"',''");
	
	var urlAction = imActionWithInput.substring(imActionWithInput.indexOf("http"), imActionWithInput.indexOf("\'", imActionWithInput.indexOf("http")));
	
	var nullStr=''; 
	
	window.open(urlAction, '', 'resizable=yes,toolbar=no,menubar=no,location=no,scrollbars=no,status=no,height=400,width=600');
}

var caoMinPopin = {};
caoMinPopin.buttonColor = 'transparent';
caoMinPopin.originalWidth = 321;
caoMinPopin.originalHeight = 177;

function MinimizeCaoPopin() {

	var popInModeValue = GetCookie('caoPopInMode');
	if (popInModeValue == 'min') return false;
	
	minimizejs_eraseCookie('caoPopInMode');
	minimizejs_createCookie('caoPopInMode', 'min', 5);
	
	caoQuery('#iCoder_POP1141877261').animate({left: '+='+caoMinPopin.originalWidth, width: '0px', height: '177px'}, 700, 'swing', function() {

		caoMinPopin.imAction = caoQuery('#CaoIMAction').attr('onclick');
		caoMinPopin.chatButton = '<a style="z-index:10001; position:absolute; top:29px; left:2px;" href="#" onclick="'+caoMinPopin.imAction+'"><img src="../../cdn.contactatonce.com/dropin/AutoNationSidePanelChatButton2.png"/*tpa=http://cdn.contactatonce.com/dropin/AutoNationSidePanelChatButton2.png*/ style="border:0;" /></a>';	
		caoMinPopin.maxButton = '<a style="z-index:10002; position:absolute; top:153px; left:1px;" href="javascript:MaximizeCaoPopin();"><img src="../../cdn.contactatonce.com/dropin/AutoNationSidePanelExpandButton2.png"/*tpa=http://cdn.contactatonce.com/dropin/AutoNationSidePanelExpandButton2.png*/ style="border:0;" /></a>';		
		
		caoQuery('#iCoder_POP1141877261').append('<div id="caoPopinNewBg" style="z-index:10000; background-color:'+caoMinPopin.buttonColor+'; color:'+caoMinPopin.buttonColor+'; background-image:url(\'../../cdn.contactatonce.com/dropin/AutoNationSidePanel2.png\'/*tpa=http://cdn.contactatonce.com/dropin/AutoNationSidePanel2.png*/); position:absolute; top:0px; left:0px; width:31px; height:177px;">'+ caoMinPopin.chatButton + caoMinPopin.maxButton +'</div>');
		
		caoQuery('#iCoder_POP1141877261').animate({left: '-=31px', width: '31px'}, 200, 'swing', function() {
		
		});
	});

}



function MaximizeCaoPopin() {

	minimizejs_eraseCookie('caoPopInMode');
	minimizejs_createCookie('caoPopInMode', 'max');
	
	var topOfDiv = caoQuery('#iCoder_POP1141877261').offset().top;
	var bottomOfVisibleWindow = caoQuery(window).height();
	
	caoMinPopin.bottomOffset = bottomOfVisibleWindow - topOfDiv;
	
	if (caoMinPopin.bottomOffset <= caoMinPopin.originalHeight) {
		caoMinPopin.heightAdd = caoMinPopin.originalHeight - caoMinPopin.bottomOffset + 3;

		caoQuery('#iCoder_POP1141877261').animate({left: '+=31px', width: '0px' }, 200, 'swing', function() {
			caoQuery('#caoPopinNewBg').remove();
			caoQuery('#iCoder_POP1141877261').animate({left: '-='+(caoMinPopin.originalWidth-1), top:'-='+caoMinPopin.heightAdd, width: caoMinPopin.originalWidth+'px', height: caoMinPopin.originalHeight+'px'}, 700, 'swing', function() {
				
			});
		});
	} else {
		caoQuery('#iCoder_POP1141877261').animate({left: '+=31px', width: '0px'}, 200, 'swing', function() {
			caoQuery('#caoPopinNewBg').remove();
			caoQuery('#iCoder_POP1141877261').animate({left: '-='+(caoMinPopin.originalWidth-1), width: caoMinPopin.originalWidth+'px', height: caoMinPopin.originalHeight+'px'}, 700, 'swing', function() {
			
			});
		});
	}
}


function minimizejs_createCookie(name,value,minutes) {
	if (minutes) {
		var date = new Date();
		date.setTime(date.getTime()+(minutes*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function minimizejs_eraseCookie(name) {
	minimizejs_createCookie(name,"",-1);
}