(function(window) {
    /* Do not change the names of these variables. */
    /* ------------------------------------------- */
var basePath = '//ja-jp.ecolab.com/mpel/';
    var documentDomain = ''; /*-- $documentDomain --*/
var lightboxUrl = 'http://ja-jp.ecolab.com/mpel/chooser.html';
var bodyclass = parent.document.body.className; /* MPB */
    /*-------------------------------------------- */
    
    var rootDomain = (function(host) {
        var m = host.match('[^.]+\.(([^.]{2,3}\.)?[^.]{2}|[^.]{3,})$');
        if (m == null) return host;
        return m[0];
    })(window.document.location.host);
    
    documentDomain = documentDomain != '' ? documentDomain : rootDomain;
    if (documentDomain != '') {
        document.domain = documentDomain;
    }
    
    if (typeof window.MpEasyLink != 'undefined') {
        window.MpEasyLink = MpEasyLink;
    }
    
    var lightbox = (function() {
        var exports = {};
        var events = {};
        exports.events = events;

        exports.pop = function(url) {
            var overlay = document.createElement('div');
            overlay.id = 'lightbox_overlay';
            overlay.className = 'black_overlay';
			parent.document.body.className='target '+bodyclass; /* MPB */
            document.body.appendChild(overlay);
        
            var iframe = document.createElement('iframe');
            iframe.src = url;
            iframe.id = 'lightbox_pop';
            iframe.className = 'white_content';
            iframe.scrolling = 'no';
            iframe.frameBorder = '0';
            iframe.style.height = '750px';
            iframe.style.width = '900px';
            var onload = function() {
                iframe.style.display = 'block';
				iframe.setAttribute('allowTransparency', 'true');
                overlay.onclick = function() {
                    if (typeof events.evade != 'undefined') {
                        events.evade();
                    }
                };
            };
            if (iframe.addEventListener) {
                iframe.addEventListener('load', onload, false);
            }
            else if (iframe.attachEvent) {
                iframe.attachEvent('onload', onload);
            }

            overlay.style.display = 'block';
            document.body.appendChild(iframe);
        };
        
        exports.remove = function() {
            var iframe = document.getElementById('lightbox_pop');
            if (iframe != null) {
                iframe.style.display = 'none';
                iframe.parentNode.removeChild(iframe);
            }
            var overlay = document.getElementById('lightbox_overlay');
            if (overlay != null) {
                overlay.style.display = 'none';
                overlay.parentNode.removeChild(overlay);
            }
			parent.document.body.className=bodyclass;/* MPB */
        };
        
        return exports;
    })();
    
    window.lightbox = {
        events: lightbox.events,
        remove: lightbox.remove
    };
    
    /* analytics */
    window.MpA = (function() {
        var MpA = {
          init: function () {
            if (typeof _MpA == "undefined") {
              return;
            }
            var str = function(s) {
              if (typeof s == "undefined") return '';
              return s;
            };
            var params = {
              'hn':str(_MpA.hn),
              'rsite':str(_MpA.rsite),
              'blang':str(_MpA.blang),
              'dlang':str(_MpA.dlang),
              'dcountry':str(_MpA.dcountry),
              'dcurrency':str(_MpA.dcurrency),
              'r':str(_MpA.r)
            };
            var px = function(type, lang, country, currency) {
              var qs = function() {
                var s = '';
                for (var k in params) {
                  s += k + '=';
                  s += escape(str(params[k]));
                  s += '&';
                }
                return s;
              };
              var img = new Image;
              img.src = _MpA.px + '?' + qs(params)
                        + 'type=' + str(type)
                        + '&slang=' + str(lang)
                        + '&scountry=' + str(country)
                        + '&scurrency=' + str(currency);
            };
            this.pop = function() {
              px('pop');
            };
            this.confirm = function(lang, country, currency) {
              px('easylink', lang, country, currency);
            };
            this.choose = function(lang, country, currency) {
              px('chooser', lang, country, currency);
            };
          }
        };
        MpA.init();
        return MpA;
    })();
    
    (function() {
        var execReady = false;
        var onReady = function() {
            if (execReady == true) return;
            execReady = true;
            var css = document.createElement('link');
            css.rel = 'stylesheet';
            css.href = basePath+'lightbox.css';
            css.type = 'text/css';
            css.media = 'screen';
            document.body.appendChild(css);
            lightbox.pop(lightboxUrl);
        };
        if (document.readyState === "loaded" || document.readyState === "complete") {
            setTimeout(onReady, 1);
        }
        else if (document.addEventListener) {
            document.addEventListener('DOMContentLoaded', onReady, false);
            window.addEventListener('load', onReady, false);
        }
        else if (document.attachEvent) {
            document.attachEvent('onreadystatechange', onReady);
            window.attachEvent('onload', onReady);
        }
    })();
})(window);
