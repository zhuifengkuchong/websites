var loadAuthImage;

/*START: $() in this context is jQuery()*/
(function(w, $){ 


/*START: logged in check */
	loadAuthImage = function()
	{
		var tstDmn = testDomain();
		var deferred = $.Deferred();
		var img = new Image(); // Create new img element 
		$(img).on('error', function() { 
			deferred.resolve(false); 
		} );
		$(img).on('load', function() {
			deferred.resolve('true'); 
		} );
		
		var date = new Date();
		var datetime = date.getTime();
		
		img.src = 'https://service' + tstDmn + '.thrivent.com/content/security/check.gif?r=' + datetime; // Set source path
		return deferred.promise();
	}

	$.when(loadAuthImage()).done(function(value) {
		var tstDmn = testDomain();		
		var isAuth = value;
		if (isAuth) {

			$('#registerLink').text('Business Accounts Log In');			
			$('#registerLink').attr('href', '#');		
			$('#registerLink').addClass('busAcctLogInLnk');
			
			$('#loginLink').hide();
			$('#logoutNavItem').show();
			
			$('#loginLinkMobile').text('Log Out');
			$('#registerLinkMobile').text('Business Accounts Log In');
			$('#registerLinkMobile').attr('href', '#');										
			$('#registerLinkMobile').addClass('busAcctLogInLnk');

			$('#logoutLink').attr('href', 'http://www.lutheransonline.com/servlet/lo_ProcServ/dbpage=rur&ruraction=logout&REMOTE_DEST=https://service.thrivent.com/apps/logout/logout.do');
			$('#loginLinkMobile').attr('href', 'http://www.lutheransonline.com/servlet/lo_ProcServ/dbpage=rur&ruraction=logout&REMOTE_DEST=https://service.thrivent.com/apps/logout/logout.do');			
			if (tstDmn != "") {
				$('#logoutLink').attr('href', 'https://service' + tstDmn + '.thrivent.com/apps/logout/logout.do');
				$('#loginLinkMobile').attr('href', 'https://service' + tstDmn + '.thrivent.com/apps/logout/logout.do');
			}			
				 
		} else {

			$('#loginLink').show();
			$('#logoutNavItem').hide()

			$('#registerLink').text('Register');	
						
			$('#loginLinkMobile').text('Log In');
			$('#registerLinkMobile').text('Register');						
			
			$('#loginLink').attr('href', 'https://myservice' + tstDmn + '.thrivent.com/portal/mythrivent');
			$('#registerLink').attr('href', 'https://service' + tstDmn + '.thrivent.com/apps/MbrEnrollment/registerAccessInfoInit.do');				
			$('#loginLinkMobile').attr('href', 'https://myservice' + tstDmn + '.thrivent.com/portal/mythrivent');				
			$('#registerLinkMobile').attr('href', 'https://service' + tstDmn + '.thrivent.com/apps/MbrEnrollment/registerAccessInfoInit.do');														
			
		}
	});
/*END: logged in check*/

/*START: navigation*/
	//Navigation toggle
	$('.nav-link').click(function(e) {
		e.preventDefault();
		$(this).toggleClass('active');
		$('.nav-primary').toggleClass('active');
	});
	
	//Show navigation dropdown on click of plus icon, hide on minus
	$(document).on('click', '.sizeLarge .nav-item:not(.active) .nav-head-withdrop a', function(e) {
		e.preventDefault();
		e.stopPropagation();
		var $this = $(this);
		$this.parents('.nav-item').addClass('active').siblings('.nav-item').removeClass('active').each(function() {
			$(this).find('.icon-minus').toggleClass('icon-plus icon-minus');
		});
		$this.children('span').toggleClass('icon-plus icon-minus');
	});
	
	$(document).on('click', '.sizeMedium .nav-head-withdrop a, .sizeSmall .nav-head-withdrop a', function(e) {
		e.preventDefault();
		e.stopPropagation();
		var $this = $(this);
		$this.parents('.nav-item').toggleClass('active');
		$this.children('span').toggleClass('icon-plus icon-minus');
	});
	
	$(document).on('click', '.sizeLarge', function() {
		$('.nav-primary .active').removeClass('active');	
	});
	
	$(window).on('resize', function(e) {
		if( Modernizr.mq('only all and (min-width: 750px)') ) {
			$('html').addClass('sizeLarge');
			$('html').removeClass('sizeMedium');
			$('html').removeClass('sizeSmall');
			$('.nav-primary .nav-item').removeClass('active').find('.icon-minus').toggleClass('icon-plus icon-minus');
		}
		else if ( Modernizr.mq('only all and (min-width: 592px)') ) {
			$('html').addClass('sizeMedium');
			$('html').removeClass('sizeLarge');
			$('html').removeClass('sizeSmall');
		}
		else {
			$('html').addClass('sizeSmall');
			$('html').removeClass('sizeMedium');
			$('html').removeClass('sizeLarge');
		}
	});
/*END: navigation*/
	
/*START: addThis defer script load until share button clicked */
	var aTLoaded = false;
	$(".addthis-link").click(function(){
		if (!aTLoaded) {
		  $.getScript("//s7.addthis.com/js/300/addthis_widget.js#async=1", function() {
			  aTLoaded = true;
		  });
		}
	});
/*END: addThis defer script load until share button clicked */
	
/*START: expanding/collapsing modules */
	//expand and collapse module
	$('.expand .e-head').on('click', function(e) {
		e.preventDefault();
		var $this = $(this);
		$this.parents('.expand').toggleClass('is-expanded');
		$this.find('.is-shown, .is-hidden').toggleClass('is-shown is-hidden');
		
		$(window).trigger('resize');
	});
	
	
	$('.expand-maj .e-link, .expand-min .e-link').on('click', function(e) {
		e.preventDefault();
		var $this = $(this);
		$this.parent().toggleClass('is-expanded');
		$this.find('.is-shown, .is-hidden').toggleClass('is-shown is-hidden');
		
		$(window).trigger('resize');
	});
	
	$(window).on('resize', function(e) {
		
		$('.expand-tabs').each(function(index, element) {
			var $this = $(element);
			var $eItem; 
			
			//only do this at the 'large' breakpoint and up
			if( Modernizr.mq('only all and (min-width: 750px)') ) {
				//if there's more than one 'tab' that is expanded, then collapse all and expand the first one
				if ($this.find('> .is-expanded').length > 1) { 
					$this.find('> .is-expanded').removeClass('is-expanded');
					$this.find('> .e-item:first-child').addClass('is-expanded');
					$eItem = $this.find('> .is-expanded');
				} 
				//if there is exactly one 'tab' expanded then let it be
				else if ($this.find('> .is-expanded').length == 1) {
					$eItem = $this.find('> .is-expanded');
				}
				//catch all
				else {
					$this.find('> .e-item:first-child').addClass('is-expanded');
					$eItem = $this.find('> .is-expanded');
				}
				//get the full height of the tabs so absolute positioning of tab content can still push content after it further down the page instead of overlaying it.
				var navHeight = $this.height();
				var extraHeight = $eItem.children('.e-extra').height();
				var fullHeight = navHeight + extraHeight + 18;
				//set the height of a parent div for the whole tabs
				$this.parent().css('min-height', fullHeight.toString() + 'px');
				
			}
		});
		//trigger the height calculation basically on load
	}).trigger('resize');
	
	$('.expand-tabs > .e-item > .e-link').on('click', function(e) {
		e.preventDefault();
		var $eItem = $(this).parent();
		
		if($(window).width() >= 750 ) {
			$eItem.siblings('.is-expanded').removeClass('is-expanded');
			$eItem.addClass('is-expanded');
			//height calculation will occur
			$(window).trigger('resize');
		}
		else {
			$eItem.toggleClass('is-expanded');
		}
		
	});
	
	$('.faqs dt').on('click', function() {
		$(this).toggleClass('is-expanded');
	});
/*END: expanding/collapsing modules */


	$(document).ready(function(){
		
	/* START: Nav link character on Android Chrome */
	if ( $('.nav-link-char').width() < 30 ) { 
				$('.nav-link-char').empty().prepend('&#8801;');
	}
	/* END: Nav link character on Android Chrome */
		
	/* START: Contextual Help        */		
		/* 
		 * Start Bug-fix.  
		 *
		 * Contextual-help icons (usually "?" icons, but any icon generally)
		 * don't register a "touch" ("click") on Android 4.1.1 tablets.  It 
		 * could be on phones too, but those render in the collapsed view.
		 * To resolve, need to add a blank space, and then group the icon 
		 * with the blank space so it doesn't wrap.  Otherwise, the clickable
		 * part of the link stays on the line above the text and the icon
		 * is useless.
		 */		 
		$('a.ctxhlpOpen').prepend('&nbsp;');
		$('a.ctxhlpOpen').wrap("<span class='group' />");
		$('.is-vishidden ~ .icon-desktop').before('&nbsp;');
		/* 
		 * End Bug-fix 
		 */
		 
		$('.ctxhlpOpen').on('click', function(e) {
			e.preventDefault();
			var contextualHelpId = $(this).attr('href');
			$(contextualHelpId).bPopup({
					modal: false					
			});
		});
	/* END:   Contextual Help        */	
	
	/* START: Business Login         */		
		$(document).on('click', '.busAcctLogInLnk', function(e) {
			e.preventDefault();		
			$('.buslogin-pop').bPopup();
		});
	/* END:   Business Login         */	
	
	/* START: Site Maintenance Link  */
		if(!($.trim($('.sitemaint-pop-content').html())=='')){
			$('.sitemaint').addClass('is-shown');
			$('.nav-item.nav-item-sitemaint').show();
			$(document).on('click', '.sitemaint-link', function(e) {
				e.preventDefault();
				$('.sitemaint-pop').bPopup();		
			});	
		}
	
	/* END:   Site Maintenance Link  */
	
	/*START: Breadcrumbs*/
		function breadCrumbCompress() {
			
			var $bc = $('.breadcrumbs');
			var $bcItems = $bc.find('li');
			var bcTotalString = '';
			
			$bcItems.each(function() {
				bcTotalString += $(this).text();
			});
			
			if (bcTotalString.length > 75) {
				$bcItems.not(':first-child, :last-child').addClass('bc-compress');
			}
		}

		if ( $('.breadcrumbs li').length > 2) {
			breadCrumbCompress();
		}
	/*END: Breadcrumbs*/

	});
	
/* START: Contact Bar Pop Up  */
	var contactBarPopOpened = false;
	$('#contactBarLink').click(function(e) {
		e.preventDefault();	
		$this = $(this);
		$this.find('[class*="icon-"]').toggleClass('icon-plus icon-minus');
		$this.find('.circle').toggleClass('circle-2 circle-4');
		$('#contactBarPop').toggleClass('is-hidden');
		
		if(!contactBarPopOpened){
			contactBarPopOpened=true;
			ntptEventTag('ev=findFRBanner.opened');
		}
	});
	
	$('#contactBarPopClose').click(function(e) {
		$('#contactBarLink').find('[class*="icon-"]').toggleClass('icon-plus icon-minus');
		$('#contactBarLink').find('.circle').toggleClass('circle-2 circle-4');
		$('#contactBarPop').addClass('is-hidden');
	});
/* END: Contact Bar Pop Up  */


/* START: fire event tag for items clicked that have .addEvTag */
	$(document).on('click', '.addEvTag', function(e) {
		var $this = $(this);
		if ($this.is('[data-addEvTag]')){
			// use the attribute's value
			var tagValue = $this.attr('data-addEvTag');
			ntptEventTag('ev=' + escape( tagValue ));
		}else{
			if ($this.attr('target') == '_blank') {
				//fire event tag with the value of the href
				ntptEventTag('ev=' + escape( this.href ));
			}
			else {
				ntptLinkTag(this, 'ev=' + encodeURIComponent(this.href));
			}
		}	
	});
/* END: fire event tag for items clicked that have .addEvTag */

/* START: Leaving Thrivent script */
		$(document).on('click', '.leaving-link', function(event) {
			event.preventDefault();
			var leaveText = "You are now leaving the Thrivent Financial website.";
			
			var leaveType = $(this).attr("data-leave-type");
			if(typeof leaveType !== typeof undefined && leaveType > ""){
				if (leaveType == "tcom-to-cu") {
					leaveText = "<p>You are now leaving the Thrivent Financial website. Deposit and lending services are offered by Thrivent Federal Credit Union, a member-owned not-for-profit financial cooperative that is federally insured by the National Credit Union Administration and doing business in accordance with the Federal Fair Lending Laws. Must qualify for membership. Insurance, securities, investment advisory and trust and investment management accounts and services offered by Thrivent Financial, the marketing name for Thrivent Financial for Lutherans, or its affiliates are not deposits or obligations of Thrivent Federal Credit Union, are not guaranteed by Thrivent Federal Credit Union or any bank, are not insured by the NCUA, FDIC or any other federal government agency, and involve investment risk, including possible loss of the principal amount invested.</p>";
				} else if (leaveType == "tcom-to-mbr") {
					leaveText = "<p><strong>Membership</strong><br>Thank you for your interest in Thrivent Financial membership.</p><p>By clicking the continue button below, you will leave the Thrivent website and be sent to the Vanco website to complete the process.</p>";
				} else if (leaveType == "trust-to-tcom") {
					leaveText = "<p>You are now leaving the Thrivent Trust Company portion of the Thrivent Financial website. Trust and investment management accounts and services are offered through Thrivent Trust Company.</p>";
				}
			}
			
			$("#leave-text").html(leaveText);
			
			var destination = $(this).attr("data-destination");
			if(typeof destination !== typeof undefined && destination > ""){
				$("#leaving-to-destination").val(destination);
				$('#leavePop.modal-pop').bPopup();						
			}else{
				$("#leaving-to-destination").val(destination);
			}			
		});
/* END: Leaving Thrivent script */

/* BEGIN: Adding fields for reference code */
	$().ready(function() {
		var $hiddenInputURL = $('<input/>', {
			type : 'hidden',
			id : "ReferenceCodeURL",
			name : "ReferenceCodeURL",
			value : document.URL
		});
		$hiddenInputURL.appendTo($("#ReferenceCode").parent());
		var $hiddenInputReferrer = $('<input/>', {
			type : 'hidden',
			id : "ReferenceCodeReferrer",
			name : "ReferenceCodeReferrer",
			value : document.referrer
		});
		$hiddenInputReferrer.appendTo($("#ReferenceCode").parent());
	});
/* END:   Adding fields for reference code   */

})(this, jQuery);
/*END $() in the context is jQuery()*/ 



var frflag = 0;
function callzipsearch() {
	var tstDmn = testDomain();
	if (frflag != 1) {
		frzip = document.getElementById('locatefrzip').value;
		
		document.getElementById('locatefriframe').innerHTML = "<iframe scrolling=\"no\" noresize=\"noresize\" frameborder=\"0\" height=\"35\" width=\"260\" src=\"https://service" + tstDmn + ".thrivent.com/content/_assets/images/ajax-loader.gif\">";

		document.getElementById('locatefriframe').innerHTML = "<iframe scrolling=\"no\" noresize=\"noresize\" frameborder=\"0\" height=\"35\" width=\"260\" src=\"https://service" + tstDmn + ".thrivent.com/content/includes/locate/zipsearch.php?zip=" + frzip + "\">";		
		}
	frflag = 1;
}

/* Begin: Set Select Events */
function setSelectEvents(controlSelector)
{
	var theSelect = document.getElementById(controlSelector);
	
	theSelect.onfocus = selectOnFocus;
	theSelect.onchange = selectOnChange;
	theSelect.onkeydown = selectOnKeyDown;
	theSelect.onclick = selectOnClick;
	
	return true;
}

function selectOnChange(theElement)
{
	var theSelect;
	
	if (theElement && theElement.value){
		theSelect = theElement;
	} else {
		theSelect = this;
	}
	
	if (!theSelect.changed){
		return false;
	} else {	
		if(typeof whatToDoWhenSelectValueChanges == 'function'){
			whatToDoWhenSelectValueChanges(theSelect);
		}else{
			//console.log("Error:  Could not find whatToDoWhenSelectValueChanges() function.");
		}	
		return true;
	}

}

function selectOnClick()
{
	this.changed = true;
}

function selectOnFocus()
{
	this.initValue = this.value;
	
	return true;
}

function selectOnKeyDown(q)
{
	var theEvent;
	var keyCodeTab = "9";
	var keyCodeEnter = "13";
	
	if (q)
	{
		theEvent = q;
	}
	else
	{
		theEvent = event;
	}

	if ((theEvent.keyCode == keyCodeEnter || theEvent.keyCode == keyCodeTab) && this.value != this.initValue)
	{
		this.changed = true;
		selectOnChange(this);
	}
	else
	{
		this.changed = false;
	}
	
	return true;
}
/* End: Set Select Events */