<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<meta HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<title>Contact Us</title>
<link id="cssBrand" rel="stylesheet" type="text/css">
<script language="javascript" src="ContactUs.js"></script>
<script language="javascript" id="externalJS"></script>
<script src="http://econsumeraffairs.com/resources/js/wilke.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#frmEntry').attr('action', 'ProcessContactUs.asp'  + window.location.search);
	});
</script>
</head>
<body onload="bdyPage_OnLoad()" style="text-align: center;">
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-BN9P"

height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-BN9P');</script>

<!-- End Google Tag Manager -->
<div id="testdiv">

</div>
<iframe id="fmeResponse" name="fmeResponse" width="0" height="0" style="display:none"></iframe>
<form id="frmEntry" name="frmEntry" method="POST">
  <input type="hidden" name="F1" id="F1"></input>
  <input type="hidden" name="F2" id="F2"></input>
  <input type="hidden" name="F3" id="F3"></input>
  <input type="hidden" name="F4" id="F4"></input>
  <input type="hidden" name="F5" id="F5"></input>
  <input type="hidden" name="F6" id="F6"></input>
  <input type="hidden" name="F7" id="F7"></input>
  <input type="hidden" name="F8" id="F8"></input>
  <input type="hidden" name="F9" id="F9"></input>
  <input type="hidden" name="valid_form" id="valid_form" value="true"></input>

  <h1 class="pagetitle">Consumer Relations Feedback Form</h1>
  <p id=para1><center id=center1>
    Please use the form below to submit your question.<br>
    <span class="required" id=required>* denotes required fields</span>
  </center></p>
  <div style="margin: 0 auto !important;">
  <table style="margin: 0 auto !important; text-align: left;" cellpadding="5" id='table1'> 
    <tr>
      <td align="right" width="35%"><label for="imp_salutation">Salutation</label></td>
      <td>
        <input type="radio" id="imp_salutation_mr" name="imp_salutation" value="Mr."> Mr.&nbsp;&nbsp;
        <input type="radio" id="imp_salutation_mrs" name="imp_salutation" value="Mrs."> Mrs.&nbsp;&nbsp;
        <input type="radio" id="imp_salutation_ms" name="imp_salutation" value="Ms."> Ms.&nbsp;&nbsp;
      </td>
    </tr>
    <tr>
      <td align="right" width="35%"><label for="imp_first_name">First Name <span class="required">*</span></label></td>
      <td><input id="imp_first_name" name="imp_first_name" size="15" maxlength="15"></td>
    </tr>
    <tr>
      <td align="right"><label for="imp_last_name">Last Name <span class="required">*</span></label></td>
      <td><input id="imp_last_name" name="imp_last_name" size="30" maxlength="40"></td>
    </tr>
    <tr>
      <td align="right"><label for="imp_address_1">Address</label></td>
      <td><input id="imp_address_1" name="imp_address_1" size="40" maxlength="40"></td>
    </tr>
    <tr>
      <td align="right"><label for="imp_address_2">Address</label></td>
      <td><input id="imp_address_2" name="imp_address_2" size="40" maxlength="40"></td>
    </tr>
    <tr>
      <td align="right"><label for="imp_city">City</label></td>
      <td><input id="imp_city" name="imp_city" size="32" maxlength="40"></td>
    </tr>
    <tr>
      <td align="right"><label for="imp_state">State or Province</label></td>
      <td>
        <select id="imp_state" name="imp_state">
          <option value="">Please Select</option>
          <option value="AL">Alabama (AL)</option>
          <option value="AK">Alaska (AK)</option>
          <option value="AB">Alberta (AB)</option>
          <option value="AZ">Arizona (AZ)</option>
          <option value="AR">Arkansas (AR)</option>
          <option value="BC">British Columbia (BC)</option>
          <option value="CA">California (CA)</option>
          <option value="CO">Colorado (CO)</option>
          <option value="CT">Connecticut (CT)</option>
          <option value="DE">Delaware (DE)</option>
          <option value="DC">District of Columbia (DC)</option>
          <option value="FL">Florida (FL)</option>
          <option value="GA">Georgia (GA)</option>
          <option value="GU">Guam (GU)</option>
          <option value="HI">Hawaii (HI)</option>
          <option value="ID">Idaho (ID)</option>
          <option value="IL">Illinois (IL)</option>
          <option value="IN">Indiana (IN)</option>
          <option value="IA">Iowa (IA)</option>
          <option value="KS">Kansas (KS)</option>
          <option value="KY">Kentucky (KY)</option>
          <option value="LA">Louisiana (LA)</option>
          <option value="ME">Maine (ME)</option>
          <option value="MB">Manitoba (MB)</option>
          <option value="MD">Maryland (MD)</option>
          <option value="MA">Massachusetts (MA)</option>
          <option value="MI">Michigan (MI)</option>
          <option value="MN">Minnesota (MN)</option>
          <option value="MS">Mississippi (MS)</option>
          <option value="MO">Missouri (MO)</option>
          <option value="MT">Montana (MT)</option>
          <option value="NE">Nebraska (NE)</option>
          <option value="NV">Nevada (NV)</option>
          <option value="NL">Newfoundland & Labrador (NL)</option>
          <option value="NB">New Brunswick (NB)</option>
          <option value="NH">New Hampshire (NH)</option>
          <option value="NJ">New Jersey (NJ)</option>
          <option value="NM">New Mexico (NM)</option>
          <option value="NY">New York (NY)</option>
          <option value="NT">Northwest Territories (NT)</option>
          <option value="NC">North Carolina(NC)</option>
          <option value="ND">North Dakota (ND)</option>
          <option value="NS">Nova Scotia (NS)</option>
          <option value="NU">Nunavut (NU)</option>
          <option value="OH">Ohio (OH)</option>
          <option value="OK">Oklahoma (OK)</option>
          <option value="ON">Ontario (ON)</option>
          <option value="OR">Oregon (OR)</option>
          <option value="PA">Pennsylvania (PA)</option>
          <option value="PE">Prince Edward Island (PE)</option>
          <option value="PR">Puerto Rico (PR)</option>
          <option value="QC">Quebec (QC)</option>
          <option value="RI">Rhode Island (RI)</option>
          <option value="SK">Saskatchewan (SK)</option>
          <option value="SC">South Carolina (SC)</option>
          <option value="SD">South Dakota (SD)</option>
          <option value="TN">Tennessee (TN)</option>
          <option value="TX">Texas (TX)</option>
          <option value="UT">Utah (UT)</option>
          <option value="VT">Vermont (VT)</option>
          <option value="VA">Virginia (VA)</option>
          <option value="WA">Washington (WA)</option>
          <option value="WV">West Virginia (WV)</option>
          <option value="WI">Wisconsin (WI)</option>
          <option value="WY">Wyoming (WY)</option>
          <option value="YT">Yukon (YT)</option>
		  <option value="AA">Armed Forces of the Americas (AA)</option>
		  <option value="AE">Armed Forces of Europe (AE)</option>
		  <option value="AP">Armed Forces of the Pacific (AP)</option>
        </select>
      </td>
    </tr>
    <tr>
      <td align="right"><label for="imp_zip">ZIP/Postal Code <span class="required">*</span></label></td>
      <td><input id="imp_zip" name="imp_zip" size="10" maxlength="10"></td>
    </tr>
    <tr>
      <td align="right"><label for="imp_country">Country <span class="required">*</span></label></td>
      <td>
		<input type="text" id="imp_country" name="imp_country" />
      </td>
    </tr>
    <tr>
      <td align="right"><label for="imp_phone_area_code">Phone number<span class="required">*</span></label></td>
      <td>
        ( <input id="imp_phone_area_code" name="imp_phone_area_code" size="3" maxlength="3"> )
          <input id="imp_phone_exchange" name="imp_phone_exchange" size="3" maxlength="3">
        - <input id="imp_phone_last_four" name="imp_phone_last_four" size="4" maxlength="4">
      </td>
    </tr>
    <tr>
      <td align="right"><label for="imp_email">E-mail address <span class="required">*</span></label></td>
      <td><input id="imp_email" name="imp_email" size="40" maxlength="255"></td>
    </tr>
    <tr>
      <td align="right"><label for="imp_verify_email">Verify E-mail <span class="required">*</span></label></td>
      <td><input id="imp_verify_email" name="imp_verify_email" size="40" maxlength="255"></td>
    </tr>
    <tr>
        <td align="right" valign="top"><strong id=strong1>IMPORTANT:</strong></td>
        <td>
        	<div style="width: 400px;">If you are using spam blocker software, please be sure to add consumer.relations@dpsg.com to your safe list to ensure that you will receive the reply to your inquiry promptly.</div>
        </td>
    </tr>
    <tr>
    	<td colspan="2" style="text-align: center;">
	    	<div style="width: 600px;margin: 0 auto;">
		    	<strong>Are you emailing about a contest, sweepstakes or other promotion? If so, click <a id="promoLink" href="#" onclick="submitPromo()">here</a>.</strong>
	    	</div>
    	</td>
    </tr>
    <tr>
      <td align="right"><label for="imp_birth_month">Birth Date <span class="required">*</span></label></td>
      <td>
        <select id="imp_birth_month" name="imp_birth_month">
          <option value="">Month</option>
          <option value="01">01</option>
          <option value="02">02</option>
          <option value="03">03</option>
          <option value="04">04</option>
          <option value="05">05</option>
          <option value="06">06</option>
          <option value="07">07</option>
          <option value="08">08</option>
          <option value="09">09</option>
          <option value="10">10</option>
          <option value="11">11</option>
          <option value="12">12</option>
        </select>
        <select id="imp_birth_year" name="imp_birth_year">
          <option value="">Year</option>
          <option value="2015">2015</option>
          <option value="2014">2014</option>
		  <option value="2013">2013</option>
          <option value="2012">2012</option>
          <option value="2011">2011</option>
		  <option value="2010">2010</option>
          <option value="2009">2009</option>
          <option value="2008">2008</option>
          <option value="2007">2007</option>
          <option value="2006">2006</option>
          <option value="2005">2005</option>
          <option value="2004">2004</option>
          <option value="2003">2003</option>
          <option value="2002">2002</option>
          <option value="2001">2001</option>
          <option value="2000">2000</option>
          <option value="1999">1999</option>
          <option value="1998">1998</option>
          <option value="1997">1997</option>
          <option value="1996">1996</option>
          <option value="1995">1995</option>
          <option value="1994">1994</option>
          <option value="1993">1993</option>
          <option value="1992">1992</option>
          <option value="1991">1991</option>
          <option value="1990">1990</option>
          <option value="1989">1989</option>
          <option value="1988">1988</option>
          <option value="1987">1987</option>
          <option value="1986">1986</option>
          <option value="1985">1985</option>
          <option value="1984">1984</option>
          <option value="1983">1983</option>
          <option value="1982">1982</option>
          <option value="1981">1981</option>
          <option value="1980">1980</option>
          <option value="1979">1979</option>
          <option value="1978">1978</option>
          <option value="1977">1977</option>
          <option value="1976">1976</option>
          <option value="1975">1975</option>
          <option value="1974">1974</option>
          <option value="1973">1973</option>
          <option value="1972">1972</option>
          <option value="1971">1971</option>
          <option value="1970">1970</option>
          <option value="1969">1969</option>
          <option value="1968">1968</option>
          <option value="1967">1967</option>
          <option value="1966">1966</option>
          <option value="1965">1965</option>
          <option value="1964">1964</option>
          <option value="1963">1963</option>
          <option value="1962">1962</option>
          <option value="1961">1961</option>
          <option value="1960">1960</option>
          <option value="1959">1959</option>
          <option value="1958">1958</option>
          <option value="1957">1957</option>
          <option value="1956">1956</option>
          <option value="1955">1955</option>
          <option value="1954">1954</option>
          <option value="1953">1953</option>
          <option value="1952">1952</option>
          <option value="1951">1951</option>
          <option value="1950">1950</option>
          <option value="1949">1949</option>
          <option value="1948">1948</option>
          <option value="1947">1947</option>
          <option value="1946">1946</option>
          <option value="1945">1945</option>
          <option value="1944">1944</option>
          <option value="1943">1943</option>
          <option value="1942">1942</option>
          <option value="1941">1941</option>
          <option value="1940">1940</option>
          <option value="1939">1939</option>
          <option value="1938">1938</option>
          <option value="1937">1937</option>
          <option value="1936">1936</option>
          <option value="1935">1935</option>
          <option value="1934">1934</option>
          <option value="1933">1933</option>
          <option value="1932">1932</option>
          <option value="1931">1931</option>
          <option value="1930">1930</option>
          <option value="1929">1929</option>
          <option value="1928">1928</option>
          <option value="1927">1927</option>
          <option value="1926">1926</option>
          <option value="1925">1925</option>
          <option value="1924">1924</option>
          <option value="1923">1923</option>
          <option value="1922">1922</option>
          <option value="1921">1921</option>
          <option value="1920">1920</option>
          <option value="1919">1919</option>
          <option value="1918">1918</option>
          <option value="1917">1917</option>
          <option value="1916">1916</option>
          <option value="1915">1915</option>
          <option value="1914">1914</option>
          <option value="1913">1913</option>
          <option value="1912">1912</option>
          <option value="1911">1911</option>
          <option value="1910">1910</option>
          <option value="1909">1909</option>
          <option value="1908">1908</option>
          <option value="1907">1907</option>
          <option value="1906">1906</option>
          <option value="1905">1905</option>
          <option value="1904">1904</option>
          <option value="1903">1903</option>
          <option value="1902">1902</option>
          <option value="1901">1901</option>
          <option value="1900">1900</option>
          <option value="1899">1899</option>
        </select>
      </td>
    </tr>
    <tr>
      <td align="right"><label for="imp_flavor">Flavor</label></td>
      <td><select id="imp_flavor" name="imp_flavor"></select></td>
    </tr>
    <tr>
      <td align="right">
        <label for="imp_production_code">Production Code</label><br>
        <small>(From the neck of the bottle, bottom of the<br />can or wrapping of the package)</small>
      </td>
      <td><input id="imp_production_code" name="imp_production_code" size="40" maxlength="40"></td>
    </tr>
    <tr>
      <td align="right"><label for="imp_comments">Comments <span class="required">*</span></label></td>
      <td><textarea wrap="physical" id="imp_comments" name="imp_comments" rows="5" cols="45"></textarea></td>
    </tr>
    <tr>
      <td align="right" valign="top"><input type="checkbox" name="imp_opt_in" value="Y"></td>
      <td valign="top">If you live in the U.S. and would like to receive news and updates, check this box</td>
    </tr>
    <tr>
      <td colspan="2" align="center">
        <strong id=strong2>If your request is related to the quality of one of our products, please complete the following:</strong>       
      </td>
    </tr>
    <tr>
      <td align="right">
        <label for="imp_upc">UPC</label><br>
        <small>(The bar code number)</small>
      </td>
      <td><input id="imp_upc" name="imp_upc" size="13 " maxlength="13"></td>
    </tr>
    <tr>
      <td align="right"><label for="imp_store_purchased">Store Purchased</label></td>
      <td><input id="imp_store_purchased" name="imp_store_purchased" size="40 " maxlength="40"></td>
    </tr>
    <tr>
      <td align="right"><label for="imp_store_address">Store Address</label></td>
      <td><input id="imp_store_address" name="imp_store_address" size="40" maxlength="40"></td>
    </tr>
    <tr>
      <td align="right"><label for="imp_store_city">Store City</label></td>
      <td><input id="imp_store_city" name="imp_store_city" size="40" maxlength="40"></td>
    </tr>
    <tr>
      <td align="right"><label for="imp_store_state">Store State</label></td>
      <td>
        <select id="imp_store_state" name="imp_store_state">
          <option value="">Please Select</option>
          <option value="AL">Alabama (AL)</option>
          <option value="AK">Alaska (AK)</option>
          <option value="AB">Alberta (AB)</option>
          <option value="AZ">Arizona (AZ)</option>
          <option value="AR">Arkansas (AR)</option>
          <option value="BC">British Columbia (BC)</option>
          <option value="CA">California (CA)</option>
          <option value="CO">Colorado (CO)</option>
          <option value="CT">Connecticut (CT)</option>
          <option value="DE">Delaware (DE)</option>
          <option value="DC">District of Columbia (DC)</option>
          <option value="FL">Florida (FL)</option>
          <option value="GA">Georgia (GA)</option>
          <option value="GU">Guam (GU)</option>
          <option value="HI">Hawaii (HI)</option>
          <option value="ID">Idaho (ID)</option>
          <option value="IL">Illinois (IL)</option>
          <option value="IN">Indiana (IN)</option>
          <option value="IA">Iowa (IA)</option>
          <option value="KS">Kansas (KS)</option>
          <option value="KY">Kentucky (KY)</option>
          <option value="LA">Louisiana (LA)</option>
          <option value="ME">Maine (ME)</option>
          <option value="MB">Manitoba (MB)</option>
          <option value="MD">Maryland (MD)</option>
          <option value="MA">Massachusetts (MA)</option>
          <option value="MI">Michigan (MI)</option>
          <option value="MN">Minnesota (MN)</option>
          <option value="MS">Mississippi (MS)</option>
          <option value="MO">Missouri (MO)</option>
          <option value="MT">Montana (MT)</option>
          <option value="NE">Nebraska (NE)</option>
          <option value="NV">Nevada (NV)</option>
          <option value="NL">Newfoundland & Labrador (NL)</option>
          <option value="NB">New Brunswick (NB)</option>
          <option value="NH">New Hampshire (NH)</option>
          <option value="NJ">New Jersey (NJ)</option>
          <option value="NM">New Mexico (NM)</option>
          <option value="NY">New York (NY)</option>
          <option value="NT">Northwest Territories (NT)</option>
          <option value="NC">North Carolina(NC)</option>
          <option value="ND">North Dakota (ND)</option>
          <option value="NS">Nova Scotia (NS)</option>
          <option value="NU">Nunavut (NU)</option>
          <option value="OH">Ohio (OH)</option>
          <option value="OK">Oklahoma (OK)</option>
          <option value="ON">Ontario (ON)</option>
          <option value="OR">Oregon (OR)</option>
          <option value="PA">Pennsylvania (PA)</option>
          <option value="PE">Prince Edward Island (PE)</option>
          <option value="PR">Puerto Rico (PR)</option>
          <option value="QC">Quebec (QC)</option>
          <option value="RI">Rhode Island (RI)</option>
          <option value="SK">Saskatchewan (SK)</option>
          <option value="SC">South Carolina (SC)</option>
          <option value="SD">South Dakota (SD)</option>
          <option value="TN">Tennessee (TN)</option>
          <option value="TX">Texas (TX)</option>
          <option value="UT">Utah (UT)</option>
          <option value="VT">Vermont (VT)</option>
          <option value="VA">Virginia (VA)</option>
          <option value="WA">Washington (WA)</option>
          <option value="WV">West Virginia (WV)</option>
          <option value="WI">Wisconsin (WI)</option>
          <option value="WY">Wyoming (WY)</option>
          <option value="YT">Yukon (YT)</option>
        </select>
      </td>
    </tr>
    <tr>
      <td align="right"><label for="imp_store_country">Store Country</label></td>
      <td>
        <select id="imp_store_country" name="imp_store_country">
          <option value="">Please Select</option>
          <option value="USA">USA</option>
          <option value="Canada">Canada</option>
          <option value="Australia">Australia</option>
          <option value="Mexico">Mexico</option>
          <option value="Belgium">Belgium</option>
          <option value="UK">UK</option>
          <option value="Brazil">Brazil</option>
          <option value="France">France</option>
          <option value="Chile">Chile</option>
          <option value="Finland">Finland</option>
          <option value="Germany">Germany</option>
          <option value="Hungary">Hungary</option>
          <option value="Ireland">Ireland</option>
          <option value="Italy">Italy</option>
          <option value="Japan">Japan</option>
          <option value="Netherlands">Netherlands</option>
          <option value="New Zealand">New Zealand</option>
          <option value="Norway">Norway</option>
          <option value="Portugal">Portugal</option>
          <option value="South Africa">South Africa</option>
          <option value="Spain">Spain</option>
          <option value="Sweden">Sweden</option>
          <option value="Switzerland">Switzerland</option>
          <option value="Bulgaria">Bulgaria</option>
          <option value="Costa Rica">Costa Rica</option>
          <option value="Scotland">Scotland</option>
        </select>
      </td>
    </tr>
    <tr>
      <td align="right"><label for="imp_purchased_month">Date Purchased</label></td>
      <td>
        <select id="imp_purchased_month" name="imp_purchased_month">
          <option value="">Month</option>
          <option value="01">01</option>
          <option value="02">02</option>
          <option value="03">03</option>
          <option value="04">04</option>
          <option value="05">05</option>
          <option value="06">06</option>
          <option value="07">07</option>
          <option value="08">08</option>
          <option value="09">09</option>
          <option value="10">10</option>
          <option value="11">11</option>
          <option value="12">12</option>
        </select>
        <select id="imp_purchased_day" name="imp_purchased_day">
          <option value="">Day</option>
          <option value="01">01</option>
          <option value="02">02</option>
          <option value="03">03</option>
          <option value="04">04</option>
          <option value="05">05</option>
          <option value="06">06</option>
          <option value="07">07</option>
          <option value="08">08</option>
          <option value="09">09</option>
          <option value="10">10</option>
          <option value="11">11</option>
          <option value="12">12</option>
          <option value="13">13</option>
          <option value="14">14</option>
          <option value="15">15</option>
          <option value="16">16</option>
          <option value="17">17</option>
          <option value="18">18</option>
          <option value="19">19</option>
          <option value="20">20</option>
          <option value="21">21</option>
          <option value="22">22</option>
          <option value="23">23</option>
          <option value="24">24</option>
          <option value="25">25</option>
          <option value="26">26</option>
          <option value="27">27</option>
          <option value="28">28</option>
          <option value="29">29</option>
          <option value="30">30</option>
          <option value="31">31</option>
        </select>
        <select id="imp_purchased_year" name="imp_purchased_year">
          <option value="">Year</option>
          <option value="2015">2015</option>
          <option value="2014">2014</option>
		  <option value="2013">2013</option>
          <option value="2012">2012</option>
          <option value="2011">2011</option>
          <option value="2010">2010</option>
		  <option value="2009">2009</option>
          <option value="2008">2008</option>
          <option value="2007">2007</option>
          <option value="2006">2006</option>
          <option value="2005">2005</option>
          <option value="2004">2004</option>
          <option value="2003">2003</option>
          <option value="2002">2002</option>
          <option value="2001">2001</option>
          <option value="2000">2000</option>
          <option value="1999">1999</option>
          <option value="1998">1998</option>
          <option value="1997">1997</option>
          <option value="1996">1996</option>
          <option value="1995">1995</option>
          <option value="1994">1994</option>
          <option value="1993">1993</option>
          <option value="1992">1992</option>
          <option value="1991">1991</option>
          <option value="1990">1991</option>
        </select>
      </td>
    </tr>
  </table></div>
  <p align="center"><b>If you require immediate assistance, please call 1-800-696-5891.</b></p>
  <div id='submitButton' align='center'>
    <table>
      <tr><td><br><button id="btnSubmit" type="button" onclick="frmEntry_OnSubmit()">Submit</button></td></tr>
    </table>
  </div>
</form>
</body>
</html>
