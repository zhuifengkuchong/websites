function loadXMLDoc(dname)
{
if (window.XMLHttpRequest)
  {
  xhttp=new XMLHttpRequest();
  }
else
  {
  xhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xhttp.open("GET",dname,false);
xhttp.send();
return xhttp.responseXML;
}

function getEmailIds(xmlname)
{
	xmlDoc=loadXMLDoc(xmlname);
	x=xmlDoc.getElementsByTagName("SendTo")[0];
	if(x.childNodes[0].nodeValue == "config")
	{
	  clist=xmlDoc.getElementsByTagName("SendToAddresses")[0];
	  for(i=0;i<clist.childNodes.length;i++)
	  {
		if(typeof(clist.childNodes[i].attributes) != "undefined")
		{
			custType = clist.childNodes[i].attributes.getNamedItem("CustomerType").value;
			prodcat = clist.childNodes[i].attributes.getNamedItem("ProdCategory").value;
			itval = clist.childNodes[i].childNodes[0].nodeValue;
			selectedcusttype = $('.ContactUsCustomerType').find('.scfDropList').val();
			selectedprodcate = $('.ContactUsProdCategory').find('.scfDropList').val();
			if(selectedcusttype == custType && selectedprodcate == prodcat)
			{
				$('.ContactUsToAddress').find('.scfSingleLineTextBox').val(itval);
				break;
			}
		}
	  }
	}
	else
	{
		$('.ContactUsToAddress').find('.scfSingleLineTextBox').val(x.childNodes[0].nodeValue);
	}
}