function _getPGHeight(){var A=0,B=document;if(typeof(window.innerWidth)=="number"){A=window.innerHeight;}else{if(B.documentElement&&(B.documentElement.clientWidth||B.documentElement.clientHeight)){A=B.documentElement.clientHeight;
}else{if(B.body&&(B.body.clientWidth||B.body.clientHeight)){A=B.body.clientHeight;
}}}return A;}
function _getPGTopOffset(){var A=0;var B=document;if(typeof(window.pageYOffset)=="number"){A=window.pageYOffset;}else{if(B.body&&(B.body.scrollLeft||B.body.scrollTop)){A=B.body.scrollTop;
}else{if(B.documentElement&&(B.documentElement.scrollLeft||B.documentElement.scrollTop)){A=B.documentElement.scrollTop;
}}}return A;}
var _ph = _getPGHeight();
var _pt = _getPGTopOffset();
var _sr_header_height = 142;
var _vertMarg = (_pt + ((_ph - (_sr_header_height)) / 2));//4;
var _hdHeight = 60;// + _vertMarg;
var duration = 1000;  /* 1000 millisecond fade = 1 sec */
var steps = 20;       /* number of opacity intervals   */
var delay = 5000;     /* 5 sec delay before fading out */
var Q_style = 'style="font-family:Arial;font-size:11px;color:#000000;font-weight:bold;vertical-align:text-top;text-align:left;"';
var T_HD_style = 'style="font-family:Arial;font-size:10px;color:#FFFFFF;text-align:center"';
var T_HD_styleZH = 'style="font-family:Arial;font-size:11px;color:#FFFFFF;text-align:center"';
var starta;
function imposeMaxLength(obj, maxlen){ if(obj.value.length>maxlen) {obj.value=obj.value.substring(0,maxlen); return false;}} 

function checkOther(field, txtBox, spnField){
	txtBox = document.getElementById('tb'+txtBox);
	ofield = document.getElementById(field.name+'_'+spnField);
	if(txtBox.value!=""){
		if(confirm("Because you have typed text in the box, the additional \nselection you chose will be deleted. Is this OK?")){ 
			ofield.checked=true;
			chars_remaining(txtBox, spnField);
		}else{
			txtBox.value="";
			field.checked=true;
		}
	}
} 

function checkBoxOther(field, txtBox){
	if(!field.checked){document.getElementById(txtBox).value="";}
} 

function setStartTime(){
	today=new Date();
	startsek=today.getSeconds();
	startmin=today.getMinutes();
	starttim=today.getHours();
	starta=(startsek) + 60 * (startmin) + 3600 * (starttim); 
	//document.getElementById("startTime").value=starta;
}

function chars_remaining(field, num_of_chars, max_Length){
	txt = field.value;
	if(!max_Length) max_Length = field.maxLength; /*document.getElementById('txt1').maxLength;*/
	chars_Total = max_Length - txt.length; 
	num_of_chars.innerHTML = chars_Total;
}

function fadeIn(){
  for (i = 0; i <= 1; i += (1 / steps)) {
    setTimeout("setOpacity(" + i + ")", i * duration);
  }
}

function fadeOut() {
  for (i = 0; i <= 1; i += (1 / steps)) {
    setTimeout("setOpacity(" + (1 - i) + ")", i * duration);
  }
}

/* set the opacity of the element (between 0.0 and 1.0) */
function setOpacity(level) {
	setElement(level, document.getElementById('_SR_qInvite_layer'));
	//setElement(level, document.getElementById('_qInvite'));
}
function setElement(level, element){
	element.style.opacity = level;
  element.style.mozOpacity = level;
  element.style.KhtmlOpacity = level;
	element.style.filter = "alpha(opacity=" + (level * 100) + ");";
}

function write_expandLog(pid, params){
	params = typeof(params) != 'undefined' ? params : false;
	//CUSTOM CODE - View log
	var pa_loc = "location=" + window.location.toString();
	var pa_ref = "referrer=" + document.referrer + "&";
	var pa_args = pa_ref + pa_loc; //pa_loc.substring(pa_loc.indexOf("?")+1,pa_loc.length);
	var url = "http://web.survey-poll.com/tc/CreateLog.aspx?log=comscore/view/"+pid+"-expand.log&"  + pa_args;
	if(params){ url+= "&" + params; }
	var i = new Image();
	i.src = url;
	//END CUSTOM CODE	
}
	
function expand_qInvite(flag){
	
	doc = document;
	if(flag==1){
		doc.getElementById('expand_layer').style.display="none";	
		setElement(0, doc.getElementById('_SR_qInvite_layer'));

		doc.getElementById('_SR_qInvite_layer').style.display="";	
		doc.getElementById('_SR_qInvite_layer').style.position = "absolute";
	  doc.getElementById('_SR_qInvite_layer').style.left = '0';
		doc.getElementById('_SR_qInvite_layer').style.top = _hdHeight + 'px';
		doc.getElementById('SR_DIV_GATE').style.backgroundColor = "#000000";
		_vertMarg = 4;
		qI_flag = true;
		COMSCORE.SiteRecruit.Invitation.BaseInvitation.prototype.resize_DivGate();	
		bgEffect(document.getElementsByTagName("div")["_SR_qInvite"].offsetWidth,getTotalDivHeight());
		setTimeout("fadeIn()", 3000);
		
		//write_expandLog(); //Write the 'expand' log
	}
}

function getTotalDivHeight(){
	var h = document.getElementsByTagName("div")["_SR_qInvite"].offsetHeight;
	for(i=1; i<=9; i++){
		str = "sr_q" + i;
		if(document.getElementsByTagName("div")[str]){ h += document.getElementsByTagName("div")[str].offsetHeight;}
	}
	//h -= document.getElementsByTagName("div")["_SR_footer"].offsetHeight;
	//h-= 20;
	//h+= document.getElementById("_SR_footer").style.offsetHeight;
	//h=600;
	return h;
}

function bgEffect(w,h){
	x=(w/2), y= _hdHeight;//(h/2);
	//x=0, y= _hdHeight;
	document.getElementById('_SR_BG').style.display="";
	document.getElementById('_SR_BG').style.position="absolute";
	document.getElementById('_SR_BG').style.height=40 + "px";
	document.getElementById('_SR_BG').style.top=_hdHeight + 'px';//y + "px";

  //setTimeout("setXpos(" + w + "," + x + ")", 600);
  setXpos(w,x);
  setTimeout("setYpos(" + h + "," + y + ")", 500);
}
function setXpos(w, x){
	//w+=1;
	for (i = 0; i <= w; i++) {
    setTimeout("_w(" + i + "," + x + ")", i+duration);
    x--;
  }
}

function setYpos(h, y){
	for (i = _hdHeight; i <= h; i++) {
    setTimeout("_h(" + i + "," + y + ")", i+duration);
    y--;
  }
}

function _w(w, x){
	var _layer = document.getElementById('_SR_BG');
  _layer.style.width = w + "px";
  if(x >= 0){
    	_layer.style.left= x + "px";    	
  }
}
function _h(h, y){
	var _layer = document.getElementById('_SR_BG');
  _layer.style.height = h + "px";
  if(y >= _hdHeight){
    	_layer.style.top = y + "px";
  }
}

function checkRadioNBox(checkRadio){
	if(checkRadio){
		for(var i=6; i<=14; i++){
			if(document.getElementById('sn'+i).checked){
				document.getElementById('sn'+i).checked=false;
			}
		}	
	}else if(!checkRadio){
		document.getElementById('sn5').checked=false;
	}
}

function validateForm(startSN,endSN,numOfTxt,forceFlag){
	forceFlag = (forceFlag)? true: false;

	var a =COMSCORE.SiteRecruit.Builder.addSurveyParam;
	var today=new Date();endsek=today.getSeconds();var endmin=today.getMinutes();var endtim=today.getHours();
	enda=(endsek) + 60 * (endmin) + 3600 * (endtim); 
	//starta = document.getElementById('startTime').value; 
	totalTime=enda-starta; 
	var values="", flag = true;
	for(i=startSN;i<=endSN;i++){ 
		var _sn = 'sn'+i;
		var _array = document.getElementsByName(_sn);
		if(_array!= null){
			for(j=0; j<_array.length; j++){
				if(_array[j].checked){
					//alert("passing -> " + _sn + "=" + _array[j].value);
					a(_sn, _array[j].value);
					values += _sn + "=" + _array[j].value +"; ";
					break;	
				}
			}
		}
		if(document.getElementById(_sn) != null && document.getElementById(_sn).nodeName =="SELECT"){
				a(_sn,document.getElementById(_sn).value);
				values += _sn + "=" +document.getElementById(_sn).value+"; ";
		}
	} 
	for(i=1;i<=numOfTxt;i++){
		var _tb = 'tb'+i;
		if(document.getElementById(_tb) && document.getElementById(_tb)!=null){
			a(_tb, document.getElementById(_tb).value);
			values += _tb + "=" +document.getElementById(_tb).value+"; ";
		}
	}
	a('totalTime', totalTime); a('startTime', starta); a('endTime', enda);
	//alert(values + "\n"+enda+"(EndTime) - "+starta+"(StartTime) = "+ totalTime+"(TotalTime)");
	if(!flag){
		//alert("Please fill out the questions before submitting.");
	}
	return flag;	
}

var optArrayEN = ["Company\'s culture values diverse perspectives", "Managers motivate employees to succeed","Leaders have a vision for the future","Employees are excited about the work they do", "Work environment is team-oriented and collaborative","Company is an exciting place to work","I can grow my career faster at the company than at other companies","Offers advancement based on personal achievement","Jobs at the company offer a lot of variety in day-to-day work","Work at the company is interesting and engaging","Company offers me a variety of different career opportunities","Company is ethical","Company is first to market with products and technologies","Company makes substantial improvements to products and technologies","Company's work benefits society","Company enables employees to have a healthy work/life balance","Company has jobs available in locations where I want to live","Offers leading financial compensation","Offers leading benefits and perks","Company offers job security"];


function loadQ7_8(sn,l){
	var optArray = new Array();
	if(typeof l == "undefined") l="EN";
	optArray = eval('optArray'+l);
	var str='';
	
	for(i=0; i<optArray.length; i++){
		str += '<TR><TD width="4">&nbsp;</TD><TD style="font-family:Arial; font-size:10px; color:#000000;text-align:left;height:14;width:120px;"><DIV style="width:120px;">'+optArray[i]+'</DIV></TD>';
		for(j=5; j>= 1; j--){
			str += '<TD style="text-align:center;width:60px;"><INPUT TYPE="radio" NAME="sn'+sn+'" ID="sn'+sn+'_'+j+'" VALUE="'+j+'"></TD>';
		}
		str += '<TD style="text-align:center;border-left:1px solid #DADADA;width:50px;"><INPUT TYPE="radio" NAME="sn'+sn+'" ID="sn'+sn+'_6" VALUE="99"></TD></TR>';
		if(i<(optArray.length-1)){str += '<TR><TD colspan="8" style="height:1px;background-color:#DADADA;"></TD></TR>';}
		sn++;
	}
	str+='<TR><TD colspan="8" style="height:3px;"></TD></TR></TABLE></TD></TR></TABLE>				</div>';
	return str;
}
var optArrayEN_q4_5 = ["Culture and work atmosphere", "Employee benefits","Organizational structure","Office locations", "General types of positions ","Jobs that matched my skill sets","Interview or resume tips","Online application"];


function loadQ4_5(sn,w1,w2,l){
	var optArray = new Array();
	if(typeof l == "undefined") l="EN";
	optArray = eval('optArray'+l+'_q4_5');
	var str='';
	
	for(i=0; i<optArray.length; i++){
		str += '<TR><TD width="4">&nbsp;</TD><TD width="'+w1+'" style="font-family:Arial; font-size:10px; color:#000000;text-align:left;">'+optArray[i]+' </TD>';
		for(j=1; j<= 3; j++){
			str += '<TD width="'+w2+'" align="center"><INPUT TYPE="radio" NAME="sn'+sn+'" ID="sn'+sn+'_'+j+'" VALUE="'+j+'"></TD>';
		}
		if(i<(optArray.length-1)){str += '<TR><TD colspan="5" style="height:1px;background-color:#DADADA;"></TD></TR>';}
		sn++;
	}
	return str;
}

function loadAgeOpt(sel){
	if(typeof sel == "undefined") sel="Please Select";
	//optArray = eval('optArray'+l);
	var str='<option value="0" selected="selected">--'+sel+'--</option><option value="17"><18</option>';
	for(i=18; i<=65; i++){
		str+='<option value="'+i+'" >'+i+'</option>';
	}
	str+='<option value="66" >65+</option>';
	return str;
}

function loadQHeader(qNum, qText){
	var str='<div id="sr_q'+qNum+'" name="sr_q'+qNum+'" style="margin:14px 0px 10px 10px;"><TABLE width="640" cellpadding="0" cellspacing="0" border="0"> <TR> 	<TD width="15" '+Q_style+'>'+qNum+'. </TD> <TD '+Q_style+'><B>'+qText+'</B> </TD> </TR>	<TR> 	<TD width="15">&nbsp;</TD>';
	return str;
}
setStartTime();