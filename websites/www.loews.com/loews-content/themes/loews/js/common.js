// JavaScript Document
		$(function(){		
				$.slidenav({
					animspeed: 'medium',
					hideonout: 'true'
				});
				$('.sliderCont').show();
				$('.subnav ul:nth-child(1)').addClass('sm01');
				$('.subnav ul:nth-child(2)').addClass('sm02');
				$('.subnav ul:nth-child(3)').addClass('sm03');
				$('.subnav ul:nth-child(4)').addClass('sm04');
				$('.subnav ul:nth-child(5)').addClass('sm05');
				$('.inactive-box').removeClass('inactive-box');
				$('.current_page_item').parent().show();
				var str = $('.subnav .current_page_item').parent().attr('class'); 
				if(str) {
					var left_margine = '0px';
					if(str.indexOf('sm02')  != -1) left_margine = '145px';
					if(str.indexOf('sm03') != -1) left_margine = '211px';
					if(str.indexOf('sm04') != -1) left_margine = '171px';
					if(str.indexOf('sm05') != -1) left_margine = '506px';
					$('.slidenav-box').attr('style', 'margin-left:'+left_margine);
				}
				
				if($.browser.msie && parseInt($.browser.version, 10) == 7) {
				   $('input[placeholder]').each(function(){  
						
						var input = $(this);        
						
						$(input).val(input.attr('placeholder'));
								
						$(input).focus(function(){
							 if (input.val() == input.attr('placeholder')) {
								 input.val('');
							 }
						});
						
						$(input).blur(function(){
							if (input.val() == '' || input.val() == input.attr('placeholder')) {
								input.val(input.attr('placeholder'));
							}
						});
					});
				};
			}); 		
			var fired = 0;



