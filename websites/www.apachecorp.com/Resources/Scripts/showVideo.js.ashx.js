$(document).ready(function(){
	$(".embededVideoPlayer").each(function(i, vpdiv) {
		//$(vpdiv).html("<div id='video_" + i + "'>Loading video player ...</div>");
		var src="/LMSContentConstructionKit/item-json.aspx?Get=Video&Where=Code&Is=" + $(vpdiv).html();
		$.getJSON(src,
        function(data){
			$(vpdiv).html("<div id='video_" + i + "'></div>");
			
			if (data.Video != undefined) {
				var flv;
				if (data.Video.FilePath != null) {
					flv = data.Video.FilePath;
				} else {
					flv = data.Video.File.url
				}
				src="../Player/SkinOverPlayStopSeekFullVol.swf"/*tpa=http://www.apachecorp.com/Resources/Player/SkinOverPlayStopSeekFullVol.swf*/;
				var src1 = src;
				if (data.Video.PreviewImage.value != undefined) {
					src=data.Video.PreviewImage;
				} else {
					src="";
				}
				var src2 = src;
				var flashvars = {
					flvPath : flv,
					skinPath: src1,
					preloadImage: src2
				};
				var params = {
					menu: "false"
				};
				src="../Player/simple_media_player.swf"/*tpa=http://www.apachecorp.com/Resources/Player/simple_media_player.swf*/;
				
				$('#video_'+i).flash(
					{
						src: src,
						width: 320,
						height: 240,
						flashvars: flashvars
					},
					{ expressInstall: true }
				);
			} else {
				$(vpdiv).html("<div id='video_" + i + "'><h2>Error! Video not found!</h2></div>");
			}
        });
	});
	
	$("#dialog").dialog({
			autoOpen: false,
			bgiframe: true,
			width: 680,
			height: 540,
			modal: true,
			stack: true,
			zIndex: 3999 
		});
	
});

function showVideo(videoCode) {
	$("#dialog").dialog("option", "title", "");
	$("#dialog").dialog("open").html("<div id='videoPopup'>Loading video player ...</div>");
	var src="/LMSContentConstructionKit/item-json.aspx?Get=Video&Where=Code&Is=" + videoCode;
	$.getJSON(src,
	function(data){
		if (data.Video != undefined) {
			var flv;
			if (data.Video.FilePath != null) {
				flv = data.Video.FilePath;
			} else {
				flv = data.Video.File.url
			}
			src="../Player/SkinOverPlayStopSeekFullVol.swf"/*tpa=http://www.apachecorp.com/Resources/Player/SkinOverPlayStopSeekFullVol.swf*/;
			var flashvars = {
				flvPath : flv,
				skinPath: src
			};
			var params = {
				menu: "false"
			};
			src="../Player/simple_media_player_large.swf"/*tpa=http://www.apachecorp.com/Resources/Player/simple_media_player_large.swf*/;
			$("#dialog #videoPopup").html('');
			$('#videoPopup').flash(
				{
					src: src,
					width: 640,
					height: 480,
					flashvars: flashvars
				},
				{ version: 9 },
				{ expressInstall: true }
			);
			$("#dialog").dialog("option", "title", data.Video.Title);
		} else {
			$("#dialog").dialog("option", "title", "Error! Video not found!");
			$("#dialog #videoPopup").html('<h2>Error! Video not found!</h2>');
		}
	});
}