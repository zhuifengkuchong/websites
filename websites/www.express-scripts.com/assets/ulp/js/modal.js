// last modified 3.18.2010 by p00719

// browser, version and OS detection

function getBrowser(){
	return BrowserDetect.browser;
} // end getBrowser

function getVersion(){
	return BrowserDetect.version;
} // end getVersion

function getOS(){
	return BrowserDetect.OS;
} // end getOS

var BrowserDetect = {
	init: function () {
		this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
		this.version = this.searchVersion(navigator.userAgent)
			|| this.searchVersion(navigator.appVersion)
			|| "an unknown version";
		this.OS = this.searchString(this.dataOS) || "an unknown OS";
	},
	searchString: function (data) {
		for (var i=0;i<data.length;i++)	{
			var dataString = data[i].string;
			var dataProp = data[i].prop;
			this.versionSearchString = data[i].versionSearch || data[i].identity;
			if (dataString) {
				if (dataString.indexOf(data[i].subString) != -1)
					return data[i].identity;
			}
			else if (dataProp)
				return data[i].identity;
		}
	},
	searchVersion: function (dataString) {
		var index = dataString.indexOf(this.versionSearchString);
		if (index == -1) return;
		return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
	},
	dataBrowser: [
		{
			string: navigator.userAgent,
			subString: "Chrome",
			identity: "Chrome"
		},
		{ 	string: navigator.userAgent,
			subString: "OmniWeb",
			versionSearch: "OmniWeb/",
			identity: "OmniWeb"
		},
		{
			string: navigator.vendor,
			subString: "Apple",
			identity: "Safari"
		},
		{
			prop: window.opera,
			identity: "Opera"
		},
		{
			string: navigator.vendor,
			subString: "iCab",
			identity: "iCab"
		},
		{
			string: navigator.vendor,
			subString: "KDE",
			identity: "Konqueror"
		},
		{
			string: navigator.userAgent,
			subString: "Firefox",
			identity: "Firefox"
		},
		{
			string: navigator.vendor,
			subString: "Camino",
			identity: "Camino"
		},
		{		// for newer Netscapes (6+)
			string: navigator.userAgent,
			subString: "Netscape",
			identity: "Netscape"
		},
		{
			string: navigator.userAgent,
			subString: "MSIE",
			identity: "Explorer",
			versionSearch: "MSIE"
		},
		{
			string: navigator.userAgent,
			subString: "Gecko",
			identity: "Mozilla",
			versionSearch: "rv"
		},
		{ 		// for older Netscapes (4-)
			string: navigator.userAgent,
			subString: "Mozilla",
			identity: "Netscape",
			versionSearch: "Mozilla"
		}
	],
	dataOS : [
		{
			string: navigator.platform,
			subString: "Win",
			identity: "Windows"
		},
		{
			string: navigator.platform,
			subString: "Mac",
			identity: "Mac"
		},
		{
			string: navigator.platform,
			subString: "Linux",
			identity: "Linux"
		}
	]

};

BrowserDetect.init();


// lightbox/modal message box

var myWidth = 0, myHeight = 0;

function windowSize(){

	if(typeof(window.innerWidth)=='number'){
		//Non-IE
		myWidth = window.innerWidth;
		myHeight = window.innerHeight;
	} else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)){
		//IE 6+ in 'standards compliant mode'
		myWidth = document.documentElement.clientWidth;
		myHeight = document.documentElement.clientHeight;
	} else if (document.body && (document.body.clientWidth || document.body.clientHeight)){
		//IE 4 compatible
		myWidth = document.body.clientWidth;
		myHeight = document.body.clientHeight;
    } // end if

} // end windowSize


var scrOfX = 0, scrOfY = 0;

function getScrollXY(){

	if(typeof(window.pageYOffset)=='number') {
		//Netscape compliant
		scrOfY = window.pageYOffset;
		scrOfX = window.pageXOffset;
	} else if (document.body && (document.body.scrollLeft || document.body.scrollTop)){
		//DOM compliant
		scrOfY = document.body.scrollTop;
		scrOfX = document.body.scrollLeft;
	} else if (document.documentElement && (document.documentElement.scrollLeft || document.documentElement.scrollTop)){
		//IE6 standards compliant mode
		scrOfY = document.documentElement.scrollTop;
		scrOfX = document.documentElement.scrollLeft;
    } else {
        scrOfX = 0;
        scrOfY = 0;
	} // end if

} // end getScrollXY


function showModal(strId,intW){
    toggleModal(document.getElementById(strId).innerHTML,intW);
} // end showModal


function showModalJs(strMsg,intW){
    toggleModal(strMsg,intW);
} // end showModalJs


var objModal, objMsg, intLightboxWidth, intLightboxHeight;

var blnModalMsgOn = false;

function closeModal(){
    toggleModal('',0);
} // end closeModal


function toggleModal(strMsg,intW){

    intLightboxWidth = intW;

    objMsg = document.getElementById("msg");
    objMsg.innerHTML = strMsg;
	objMsg.style.width = intLightboxWidth + "px";

	objTransparency = document.getElementById('transparency');
	objModal = document.getElementById('modalbox');

	if (objModal.style.display=='none'){

        blnModalMsgOn = true;        

		// must reveal the object before getting it's height
        objModal.style.display='';
		objTransparency.style.display='';

        if(document.implementation.hasFeature("CSS", "2.0")) {
            intLightboxHeight = document.defaultView.getComputedStyle(objMsg,null).getPropertyCSSValue("height").getFloatValue(5);
        } else if(objMsg.clientHeight!=null) {
            intLightboxHeight = objMsg.offsetHeight;
        } // end if

        centerModalWindow();

        toggleHtml('hidden');

	} else {

        blnModalMsgOn = false;

		objModal.style.display='none';
		objTransparency.style.display='none';
        
        toggleHtml('visible');

	} // end if

} // end toggleModal



/* hides/shows HTML tags listed in the array with class='modal' to prevent showing through lightbox */
/* strModalAttribute parameter should be a string "hidden" to hide element or "visible" to show */

function toggleHtml(strModalAttribute) {

    var strHtmlElements = new Array('object','embed','span','div');

    if(getBrowser()=="Explorer") strHtmlElements.push("select");

    for(var intElementNumber=0; intElementNumber<strHtmlElements.length; intElementNumber++){

        var objHtmlElement = document.getElementsByTagName(strHtmlElements[intElementNumber]);

        for (i=0; i<objHtmlElement.length; i++) {
             if(objHtmlElement[i].className=="modal") objHtmlElement[i].style.visibility=strModalAttribute;
        } // end for
    
    } // end for

} // end toggleHtml


function centerModalWindow(){

	// adjust transparency background to fill width and height of browser
    
    var height;
	var width;

	if (document.documentElement.clientHeight <= document.body.clientHeight){

		//For IE

        with(document.body){
        
            //Calculates if Body Content is taller than Browser display area
            if (scrollHeight > clientHeight){
                height = scrollHeight;
            } else {
                height = clientHeight;
            } // end if

            width = clientWidth;

        } // end with
			
	} else {
		//For FF
		height = document.documentElement.clientHeight;
		width= document.documentElement.clientWidth;
	} // end if
				
	var yy = height + "px";
	var zz = width + "px";

    with(document.getElementById("transparency").style){
        width = zz;
        height = yy;
    } // end with

    // center the modal window

	getScrollXY();
	windowSize();

    with(objModal.style){
        left = (scrOfX + (myWidth - intLightboxWidth)/2) + "px";
        top = (scrOfY + (myHeight - intLightboxHeight)/2) + "px";
    } // end with

} // end centerModalWindow


function focusTo(strButtonId){
	document.getElementById(strButtonId).focus();
} // end focusTo


// adjusts modal message to center on browser window resize
window.onresize = function(){
    if(blnModalMsgOn) centerModalWindow();
} // end window.onresize = function

// original style
//var strStyle = "<style type='text/css' media='all'>";
//strStyle = strStyle + "div#transparency {position: absolute; left: 0px; top: 0px; width: 50px; height: 50px; z-index: 10; background-image: url(http://www.medco.com/art/site/elements/bgblock.png);}";
//strStyle = strStyle + "div#modalbox {position: absolute; z-index: 11; background-color: #fff; border-width: 3px 4px 3px 4px; border-color: #fff; border-style: solid;}";
//strStyle = strStyle + "div#msg {border: #003399 solid 4px; margin: 0px; padding: 8px; vertical-align: top;}";
//strStyle = strStyle + "div.modal_source {display: none;}";
//strStyle = strStyle + "</style>";
//document.write(strStyle);

// new style
var strStyle = "<style type='text/css' media='all'>";
strStyle = strStyle + "div#transparency {position: absolute; left: 0px; top: 0px; width: 50px; height: 50px; z-index: 500; background-image: url(/assets/ulp/art/site/elements/bgblock.png);}";
strStyle = strStyle + "div#modalbox {position: absolute; z-index: 501; background-color: #fff; border: 1px solid #999;}";
strStyle = strStyle + "div#msg {border: 3px solid #fff; margin: 0px; padding: 0; vertical-align: top;}";
strStyle = strStyle + "div.modal_source {display: none;}";
strStyle = strStyle + "</style>";
document.write(strStyle);

var strModal = "<div id='transparency' style='display: none; filter: progid:DXImageTransform.Microsoft.Alpha(opacity=50);'></div>";
strModal = strModal + "<div id='modalbox' style='display: none;'><div id='msg'></div></div>";
document.write(strModal);


