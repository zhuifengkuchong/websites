$(document).ready( function() {
  pageTracker = {_trackPageview: function(){}};
  var pageURL = window.location.pathname;
  var linkText;

  $('a').click(function (e) {
      if ($(this).text() != ""){
            linkText = $(this).text();
      }
      else if (($(this).text() === "") && ($(this).children("img") != "") && ($(this).children("img").attr("alt") != "")) {
            linkText = $(this).children("img").attr("alt") + "- Image";
      }
      else if (($(this).text() === "") && ($(this).children("img") != "") && ($(this).children("img").attr("alt") === "")) {
            linkText = $(this).children("img").attr("src").split("/").pop() + "- Image";
      }
      _gaq.push(['_trackEvent', pageURL, 'click', linkText]);
      if (!($(this).attr('target') == '_blank' || $(this).attr('target') == '_parent' || $(this).attr('href').match("^#"))) {
            e.preventDefault();
            setTimeout('document.location = "' + $(this).attr('href') + '"', 150);
      }
  });
  $('form').submit(function () {
	if ($(this).children("input[type='submit']").length) {
		submitButton = $(this).children("input[type='submit']");
		if (submitButton.attr("value") != ""){
			  linkText = submitButton.attr("value") + " - Button";
		}
		else if ((submitButton.attr("value") === "") && (submitButton.prop("id") != "")) {
			  linkText = submitButton.prop("id") + " - Button";
		}
		else if ((submitButton.attr("value") === "") && (submitButton.prop("class") != "")) {
			  linkText = submitButton.prop("class") + " - Button";
		}
		else {
			  linkText = "Form Submitted - Button";  
		}
	}
	else if ($(this).children("button").length) {
		submitButton = $(this).children("button");
		linkText = submitButton.text() + " - Button";
	}
	_gaq.push(['_trackEvent', pageURL, 'click', linkText]);
	});
});