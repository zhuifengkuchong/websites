history.navigationMode = 'compatible';
$(document).ready(function() {

    function addMenu() {
        $(this).addClass("over");
        $($(this).find("ul")).fadeIn("fast");
    }

    function removeMenu() {
        $(this).removeClass("over");
        $($(this).find("ul")).fadeOut("fast");
    }

    $.event.special.hover.delay = 120;
    $.event.special.hover.speed = 150;

    $("ul.nav li").hover(addMenu, removeMenu);
    /* $("ul.nav ul").bgiframe(); */

    $('input[type="text"]').addClass('text');
    $('input[type="password"]').addClass('text');

    $('.btnGreen').hover(function() { $(this).addClass('btnGreenHover') }, function() { $(this).removeClass('btnGreenHover'); });
    $('.btnYellow').hover(function() { $(this).addClass('btnYellowHover') }, function() { $(this).removeClass('btnYellowHover'); });
    $('.btnRed').hover(function() { $(this).addClass('btnRedHover') }, function() { $(this).removeClass('btnRedHover'); });

    // set the document domain to the parent domain
    try {
        var patt = /(\w+)(.\w+)?$/;
        var parentDomain = patt.exec(document.domain)[0];
        document.domain = parentDomain;
    }
    catch (e) { }

    $('.autoHeightiFrame').iframeAutoHeight({ heightOffset: 10 });
    $('.autoHeightiFrameSideBar').iframeAutoHeight({ heightOffset: 10 });


    /* Set styles for TR iframe items, this section handles load events */
    $('#feature iframe').load(function() {
        $(this.contentDocument).find('body').addClass('extraFeatures');
    });

    $('body.home #feature iframe').load(function() {
        $(this.contentDocument).find('body').removeClass('extraFeatures');
        $(this.contentDocument).find('#ccbnRCol').css({ 'background-color': '#ffffff' });
    });


    /* Set styles for TR iframe items, this section handles when browsers load iFrame from cache.  Load event doesn't fire */
    var delayediFrameCSS = function() {

        var obj = $('#feature iframe');
        if (obj == null) {
            setTimeout(delayediFrameCSS, 500);
            return;
        }
        try {
            if (obj[0].contentDocument != null) {
                var bodyHome = $('http://www.oneok.com/js/body.home');
                if (bodyHome[0] == null)
                    $(obj[0].contentDocument).find('body').addClass('extraFeatures');
                else {
                    if ($(bodyHome).find('#feature iframe')[0].contentDocument != null) {
                        $($(bodyHome).find('#feature iframe')[0].contentDocument).find('#ccbnRCol').css({ 'background-color': '#ffffff' });
                        $($(bodyHome).find('#feature iframe')[0].contentDocument).find('body').removeClass('extraFeatures');
                    }
                }
            }
            else {
                setTimeout(delayediFrameCSS, 500);
                return;
            }
        }
        catch (e) {
            /* Getting Access Denied, use lower timeout since this means we at least have an iFrame loaded */
            setTimeout(delayediFrameCSS, 50);
            return;
        }
    };
    var obj = $('#feature iframe');
    if (obj == null) {

        delayediFrameCSS();
    }
    else {
        setTimeout(delayediFrameCSS, 200);
    }
    /* End of TR iFrame implementation */

    /* PLACEHOLDERS */
    activatePlaceholders();
    $("form").submit(function() {
        $("input").each(function(i, element2) {
            var input = $(element2);
            if (input.val() == input.attr("alt")) {
                input.val('');
            }
        });
    });

    /* TEXT BOX EXPAND*/
    $('.boxExpand').html("<div class='close'>Close [X]</div>" + $('.boxExpand').html());
    $('.boxExpand').addClass('boxExpandClosed');
    $('.boxExpand').click(function() { $(this).children('.header').toggle(); $(this).children('.close').toggle(); $(this).children('.message').slideToggle('slow'); });
	
	
	//brand window stuff
 	if ($('#brand-window').length) {
		$('#brand-window').append('<div id="brand-buttons"></div>');
		$('#brand-window .brand-image').each(function(index, element) {
			var button = '<a onclick="brandWindowRotatorLoop('+index+')"></a>';
			$('#brand-buttons').append(button);
        });
		
		$('#brand-window a:first-child').addClass('active');
		$('.windows .brand-image:first-child').clone().appendTo('.windows');
		$('.windows .brand-image:first-child').addClass('active');
		
		startTimer();
	}
});

function activatePlaceholders() {
    var detect = navigator.userAgent.toLowerCase();
    if (detect.indexOf("safari") > 0) return false;
    var inputs = document.getElementsByTagName("input");
    for (var i = 0; i < inputs.length; i++) {
        if (inputs[i].getAttribute("type") == "text") {
            if (inputs[i].getAttribute("alt") && inputs[i].getAttribute("alt").length > 0 && ((inputs[i].getAttribute("value") == null) || (inputs[i].getAttribute("value") == ""))) {
                inputs[i].value = inputs[i].getAttribute("alt");
                inputs[i].style.color = "#808080";
                inputs[i].onclick = function() {
                    if (this.value == this.getAttribute("alt")) {
                        this.value = "";
                    }
                    return false;
                }
                inputs[i].onkeyup = function() {
                    if ((this.value != this.getAttribute("alt")) && (this.value != "")) {
                        this.style.color = "#000000";
                    }
                    else {
                        this.style.color = "#808080";
                    }
                }
                inputs[i].onblur = function() {
                    if (this.value.length < 1) {
                        this.value = this.getAttribute("alt");
                    }
                }
            }
        }
    }
}


//browser properties
var Browser = {
    Version: function() {
        var version = 999;
        if (navigator.appVersion.indexOf("MSIE") != -1) {
            version = parseFloat(navigator.appVersion.split("MSIE")[1]); return version;
        }
    },
    Name: navigator.appName,
    isIE: function() {
        if (navigator.appVersion.indexOf("MSIE") != -1) {
            return true;
        }
        return false;
    }
};


//enable the button and restore the original text value for browsers other than IE
function EnableOnUnload(btn) {
    if (!Browser.isIE()) {
        window.onunload = function() {
            ResetToDefault(btn);
        };
    }
}

function disableBtn(btnID) {
    var btn = document.getElementById(btnID);
    btn.disabled = true;

    if (typeof (Page_ClientValidate) == 'function') {
        if (!Page_IsValid) {
            ResetToDefault(btn); //break;
            return false;
        }
    }

    EnableOnUnload(btn);
    return true;
}

function ResetToDefault(btn) {
    btn.disabled = false;
}




function brandWindowRotatorLoop(direction) { 
	
	var restart = false;
	var count = $(".windows").children(".brand-image").length;
	var active = $('.windows .active').prevAll().length;
	//var active = $('.windows .active').index();
	
	if (direction == '+') {
		if (active < count) {
			var next = active + 1;
		} else {
			var next = 0;
		}
	} else {
		var next = direction;
		restart = true;
		window.clearInterval(window.timer);
	}
	
	//console.log('count =  ' + count + '  active = ' + active + '  next = ' + next + '  direction = ' + direction);
	
	if (active != next) {
		if (active > next) {
			$(".windows").find(".brand-image").eq(next).addClass('next-active').css('display','block');
			$('.windows .brand-image.active').fadeOut('slow', function () {
				$(".brand-image.active").removeClass("active").css("display","none");
				$(".brand-image.next-active").addClass('active').removeClass('next-active');
				
				$('#brand-buttons a.active').removeClass('active');
				$('#brand-buttons').find('a').eq(next).addClass('active');
			});
		} else { 
			$(".windows").find(".brand-image").eq(next).addClass("next-active").fadeIn('slow', function () { 
				$(".brand-image.active").removeClass("active").css("display","none");
				$(".brand-image.next-active").addClass('active').removeClass('next-active');
				
				$('#brand-buttons a.active').removeClass('active');
				$('#brand-buttons').find('a').eq(next).addClass('active');
				
				if (next + 1 == count) {
					$('#brand-buttons').find('a').eq(0).addClass('active');
					$('.windows .brand-image:first-child').addClass('active').css('display','block');
					$('.windows .brand-image:last-child').removeClass('active').css('display','none');
				}	
			});
		}
		
		
		if (restart) {
			startTimer();	
		}
	}
}

//Timer
function startTimer() { 
    window.timer = window.setInterval(function() {
           brandWindowRotatorLoop('+');
    }, 8000);	
}

