var _gaq = _gaq || [];
_gaq.push(['global._setAccount', 'UA-28302736-1']); /* Global Roll-up */
_gaq.push(['global._setSiteSpeedSampleRate', 100]); // #protip
_gaq.push(['global._setDomainName', 'http://video.medtronic.com/js/libs/google-analytics/ga-core-code-medtronic-plc/medtronic.com']); /* Your Site Domain */
_gaq.push(['global._setAllowLinker', true]); /* Add setAllowLinker to allow tracking cross domains */
_gaq.push(['global._trackPageview']);

_gaq.push(['site._setAccount', 'UA-26704824-1']); /* Your Site Domain tracking code */
_gaq.push(['site._setSiteSpeedSampleRate', 100]); // #protip                                                                                                                                                                                              
_gaq.push(['site._setDomainName', 'http://video.medtronic.com/js/libs/google-analytics/ga-core-code-medtronic-plc/medtronic.com']); /* Your Site Domain */
_gaq.push(['site._setAllowLinker', true]); /* Add setAllowLinker to allow tracking cross domains */
_gaq.push(['site._trackPageview']);

(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0]; 
g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js'; 
s.parentNode.insertBefore(g,s)}(document,'script'));
