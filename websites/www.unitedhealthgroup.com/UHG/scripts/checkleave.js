(function (a) {
    function b(b, c) {
        if (b.timeOut > 0) {
            clearTimeout(c)
        }
        a("#" + b.overlayId + ", #" + b.messageHolderId).fadeOut(800,
function () {
    a("#" + b.overlayId + ", #" + b.messageHolderId).remove()
});
        a(document).unbind("keyup")
    }
    a.fn.leaveNotice =
function (c) {
    var d = { siteName: window.location.href, exitMessage: "<p><strong>You have requested a website outside of {SITENAME}.</strong></p><p>Thank you for visiting.</p>", preLinkMessage: "<div class='setoff'><p>You will be directed to:<br/>{URL}</p></div>", linkString: "", timeOut: 4e3, overlayId: "ln-blackout", messageBoxId: "ln-messageBox", messageHolderId: "ln-messageHolder", displayUrlLength: 50, overlayAlpha: .85
    };
    var e = a.extend(d, c); return this.each(
    function () {
        el = a(this);
        var c = el.attr("href");
        var d = e.displayUrlLength;
        if (c.length >= d) {
            var f = "..."
        }
        else {
            var f = ""
        }
        var g = c.substr(0, d) + f;
        var h = el.attr("title");
        if (h === undefined || h == "") {
            var i = g
        }
        else {
            var i = h
        }
        el.click(function () {
            a("body").append('<div id="' + e.overlayId + '"></div>');
            a("body").append('<div id="' + e.messageHolderId + '"><div id="' + e.messageBoxId + '"></div></div>');

            if (e.overlayAlpha !== false) {
                a("#" + e.overlayId).css("opacity", e.overlayAlpha).hide().fadeIn('slow')
            }


            preFilteredContent = e.exitMessage + e.preLinkMessage; msgContent = preFilteredContent.replace(/\{URL\}/g, '<a href="' + c + '" title="' + c + '"' + e.linkString + ">" + i + "</a>");
            msgContent = msgContent.replace(/\{SITENAME\}/g, e.siteName);

            if (e.timeOut > 0) {
                msgContent += '<p id="ln-cancelMessage"><a href="#close" id="ln-cancelLink">Cancel</a></p>'
            }
            else {
                msgContent += '<span id="ln-agreeMessage"><a href="#close" id="ln-agreeLink">Proceed</a></span> &nbsp; <span id="ln-cancelMessage"><a href="#close" id="ln-cancelLink">Cancel</a></span>'
            }

            a("#" + e.messageBoxId).append(msgContent);

            if (e.timeOut > 0) {
                leaveIn = setTimeout(
            function () {
                a("#ln-cancelMessage").html("<em>Loading...</em>");
                window.open(c)
                //window.location.href = c
            }, e.timeOut)
            }
            else {
                leaveIn = false
            }
            a("#ln-cancelLink").click(
            function () {
                b(e, leaveIn);
                return false
            });

            a("#ln-agreeLink").click(
            function () {
                window.open(c)
                b(e, leaveIn);
                //window.location.href = c;

            });

            a(document).bind("keyup",
            function (a) {
                if (a.which == 27) {
                    b(e, leaveIn)
                }
            });
            a(window).unload(
            function () {
                b(e, leaveIn)
            });
            return false
        })
    })
}
})(jQuery)