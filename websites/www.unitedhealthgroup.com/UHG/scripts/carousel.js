//Quan Truong - Corporate Applications

jQuery(document).ready(function () {

  
    //options( 1 - ON , 0 - OFF)   
    var auto_slide = 0;
    var hover_pause = 1;
    var key_slide = 0;

    //speed of auto slide(   
    var auto_slide_seconds = 5000;

    jQuery('#carousel_ul li:first').before(jQuery('#carousel_ul li:last'));

    //check if auto sliding is enabled   
//    if (auto_slide == 1) {
//        //var timer = setInterval('slide("right")', auto_slide_seconds);
//        //jQuery('#hidden_auto_slide_seconds').val(auto_slide_seconds);
//    }

    //check if hover pause is enabled   
//    if (hover_pause == 1) {
//        //when hovered over the list   
//        jQuery('#carousel_ul').hover(function () {
//            //stop the interval   
//            clearInterval(timer)
//        }, function () {
//            //and when mouseout start it again   
//            timer = setInterval('slide("right")', auto_slide_seconds);
//        });

//    }


//    if (key_slide == 1) {
//        //binding keypress function   
//        jQuery(document).bind('keypress', function (e) {
//            //keyCode for left arrow is 37 and for right it's 39 '   
//            if (e.keyCode == 37) {
//                //initialize the slide to left function   
//                slide('left');
//            } else if (e.keyCode == 39) {
//                //initialize the slide to right function   
//                slide('right');
//            }
//        });
//    }



}); 
//End Doc Ready


//slide function
function slide(where) {
    //get the item width   
    var item_width = jQuery('#carousel_ul li').outerWidth() + 10;
    if (where == 'left') {
        var left_indent = parseInt(jQuery('#carousel_ul').css('left')) + item_width;
    } else {
        var left_indent = parseInt(jQuery('#carousel_ul').css('left')) - item_width;
    }

    jQuery('#carousel_ul:not(:animated)').animate({ 'left': left_indent }, 500, function () {
        if (where == 'left') {
            jQuery('#carousel_ul li:first').before(jQuery('#carousel_ul li:last'));
        } else {
            jQuery('#carousel_ul li:last').after(jQuery('#carousel_ul li:first'));
        }
        //...and then just get back the default left indent   
        jQuery('#carousel_ul').css({ 'left': '-210px' });
    });

}
