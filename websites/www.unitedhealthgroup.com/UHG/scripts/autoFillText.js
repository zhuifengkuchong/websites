jQuery(document).ready(function () {

	//Auto Add Text if box is empty
	var initialText = jQuery(".autoText").attr('value');
	
	jQuery(".autoText").blur(function(){
								 
		if(jQuery(this).attr("value") == ""){
			jQuery(this).attr("value", initialText)
		};
	});
	jQuery(".autoText").focus(function(){
								 
		if(jQuery(this).attr("value") == initialText){
			jQuery(this).attr("value", "")
		};
	});
	

});