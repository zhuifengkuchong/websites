// Developer - Jonathan Larson Corporate Communications
// Last Modifed: 08/11/11
// Support for IE 8.

mainRotator = function (id, xmlURL) {
    this.ref = id;
    this.divID = id;
    window[this.ref] = this;
    this.ready = false;

    jQuery.noConflict();
    jQuery.ajax({
        url: xmlURL,
        context: this,
        success: this.setup,
        dataType: "xml"

    });

}

mainRotator.prototype = {
    setup: function (data) {
        var id = this.divID;
        this.current = 0; // current rotation frame
        this.last = 0; // previous rotation frame
        jQuery.noConflict();
        this.mainDiv = jQuery("#" + id); // containing div


        var xmlLinks = data.getElementsByTagName("link");
        var xmlFrames = data.getElementsByTagName("slide");
        var xmlSettings = data.getElementsByTagName("settings")[0];

        for (var l = 0; l < xmlLinks.length; l++) {
            var url = xmlLinks[l].getElementsByTagName("url")[0];
            var title = xmlLinks[l].getElementsByTagName("title")[0];
            this.mainDiv.find(".sideBoxLinksList").append("<li><a href=\"" + url.childNodes[0].nodeValue + "\">" + title.childNodes[0].nodeValue + "</a></li>");
        }

        for (var f = xmlFrames.length - 1; f >= 0; f--) {
            var content = xmlFrames[f].getElementsByTagName("content")[0];
            this.mainDiv.prepend("<div class=\"frames\">" + content.childNodes[0].nodeValue + "</div>");
        }


        this.auto = !!xmlSettings.getElementsByTagName("autoRotate")[0].childNodes[0].nodeValue; // rotate automatically?
        this.speed = parseInt(xmlSettings.getElementsByTagName("slidedelay")[0].childNodes[0].nodeValue); // auto-rotaton speed in milliseconds
        this.tranSpeed = parseInt(xmlSettings.getElementsByTagName("transitionSpeed")[0].childNodes[0].nodeValue); // speed of transitions in milliseconds

        this.mainDiv.find(".sideBoxTitle").html(xmlSettings.getElementsByTagName("sideNavHeader")[0].childNodes[0].nodeValue);

        this.frames = this.mainDiv.children(".frames"); // frames to rotate
        this.total = this.frames.length; // total frames to rotate
        this.timer = 0;
        this.hovered = false;

        this.frames.each(function (idx, elem) {
            if (idx > 0) {
                jQuery.noConflict();
                jQuery(this).css('display', 'none');
            } else {
                jQuery.noConflict();
                jQuery(this).css('display', '');
            }
            var visibleIdx = idx + 1;
            jQuery.noConflict();
            jQuery("#" + id + " .selector .right").before("<div class=\"center\">" + visibleIdx + "</div>");
        });

        this.updateSelector();
        jQuery.noConflict();
        var selectorWidth = (this.total * 20) + 10;
        var selectorCenter = (jQuery("#" + id).width() / 2) - (selectorWidth / 2);

        jQuery("#" + id + " .selector").css("width", selectorWidth + "px");
        //$("#" + id + " .selector").css("left", selectorCenter + "px");
        if (jQuery.browser.msie && jQuery.browser.version < 7) {
            jQuery.noConflict();
            jQuery(document).pngFix();
        }
        jQuery("#" + id + " .selector .center").click(function () { window["mainRotator" + jQuery(this).parent().parent().attr("id")].selectorClick(this); });
        jQuery("#" + id + " .selector .center").hover(
			function () {
			    window["mainRotator" + jQuery(this).parent().parent().attr("id")].halt();
			    window["mainRotator" + jQuery(this).parent().parent().attr("id")].hovered = true;
			    //$(this).addClass("centerHover");
			    jQuery.noConflict();
			    jQuery(this).css("background-image", "url(/images/SlideSelect-CenterHover.png)");
			    if (jQuery.browser.msie && jQuery.browser.version < 7) {
			        jQuery.noConflict();
			        jQuery(document).pngFix();
			    }
			},
			function () {
			    jQuery.noConflict();
			    window["mainRotator" + jQuery(this).parent().parent().attr("id")].play();
			    window["mainRotator" + jQuery(this).parent().parent().attr("id")].hovered = false;
			    //$(this).removeClass("centerHover");
			    if (window["mainRotator" + jQuery(this).parent().parent().attr("id")].current != parseInt(jQuery(this).html()) - 1) {
			        jQuery.noConflict();
			        jQuery(this).css("background-image", "url(/images/SlideSelect-Center.png)");
			        if (jQuery.browser.msie && jQuery.browser.version < 7) {
			            jQuery.noConflict();
			            jQuery(document).pngFix();
			        }
			    }
			}
		);

        this.ready = true;
        this.mainDiv.find(".loading").hide();
    },

    isReady: function () {
        return this.ready;
    },

    init: function () {
        if (this.isReady()) {
            if (this.auto) {
                this.play();
            }
        } else {
            setTimeout("window['" + this.ref + "'].init()", 500);
        }
    },

    play: function () {
        clearTimeout(this.timer);
        this.timer = setTimeout("window['" + this.ref + "'].next()", this.speed);
    },

    halt: function () {
        clearTimeout(this.timer);
    },

    next: function () {
        this.last = this.current;
        this.current++;
        if (this.current > this.total - 1) {
            this.current = 0;
        }
        this.transition();
    },

    prev: function () {
        this.last = this.current;
        this.current--;
        if (this.current < 0) {
            this.current = this.length - 1;
        }
        this.transition();
    },

    updateSelector: function () {
        this.mainDiv.find(".selector .center").each(function (idx) {
            jQuery.noConflict();
            if (window["mainRotator" + jQuery(this).parent().parent().attr("id")].current != parseInt(jQuery(this).html()) - 1) {
                jQuery(this).css("background-image", "url(/images/SlideSelect-Center.png)");
            } else {
                jQuery(this).css("background-image", "url(/images/SlideSelect-CenterHover.png)");
            }
            if (jQuery.browser.msie && jQuery.browser.version < 7) {
                jQuery.noConflict();
                jQuery(document).pngFix();
            }
        });
    },

    transition: function () {
        this.updateSelector();
        if (this.last != this.current) {
            //alert(this.last);
            this.frames.slice(this.last, this.last + 1).fadeOut(this.tranSpeed, function () { /*window["mainRotator" + $(this).parent().attr("id")].transitionStep()*/ });
            this.frames.slice(this.current, this.current + 1).fadeIn(this.tranSpeed, function () { window["mainRotator" + jQuery(this).parent().attr("id")].transitionComplete() });
        } else {
            if (this.auto && !this.hovered) {
                this.play();
            }
        }
    },

    swap: function () {
        this.updateSelector();
        if (this.last != this.current) {
            //alert(this.last);
            this.frames.slice(this.last, this.last + 1).hide();
            this.frames.slice(this.current, this.current + 1).show();
        }
    },

    transitionComplete: function () {
        if (this.auto && !this.hovered) {
            this.play();
        }
    },

    goTo: function (idx, transition) {
        this.halt();
        this.last = this.current;
        this.current = idx;
        if (transition) {
            this.transition();
        } else {
            this.swap();
        }
    },

    selectorClick: function (element) {
        jQuery.noConflict();
        var idx = parseInt(jQuery(element).html()) - 1;
        this.goTo(idx, true);
    },

    scrollUp: function (element) {
        var idx = this.current + 1;
        if (idx > this.total - 1) {
            idx = 0;
        }
        this.goTo(idx, false);
    },

    scrollDown: function (element) {
        var idx = this.current - 1;
        if (idx < 0) {
            idx = this.length - 1;
        }
        this.goTo(idx, false);
    }

}