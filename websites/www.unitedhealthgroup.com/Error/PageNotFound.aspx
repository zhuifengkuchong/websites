
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<title>UnitedHealth Group - Page Not Found</title>
 
 


<meta http-equiv="Content-Language" content="en-us" />
<meta http-equiv="imagetoolbar" content="no" />
<meta name="MSSmartTagsPreventParsing" content="true" />
<meta name="author" content="UnitedHealth Group" />
<!--Bing Meta-->
<meta name="msvalidate.01" content="BC7A281322341735C3C0A5327450098D" />
<!--Meta Keywords News Type Tags-->
 
<!--Meta Keywords Business Tags-->
 
<!--Meta Keywords DateRange -->
 
<!--Meta Keywords Topics Tag -->



<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-8497980-1', 'auto');
  ga('send', 'pageview');

</script>

<link rel="stylesheet" type="text/css" href="/UHG/styles/UHGMaster.css" />
<link rel="stylesheet" type="text/css" href="/UHG/styles/leaveNotice.css" />
<script type="text/javascript" src="/UHG/timeline/js/jquery/jquery-1.7.1.min.js" aysnc></script>

<script src="/UHG/scripts/autoFillText.js" type="text/javascript" aysnc></script>
<script type="text/javascript" src="/UHG/scripts/carousel.js" aysnc></script>
<link href="/UHG/styles/carousel.css" rel="stylesheet" type="text/css" />
<link href="/UHG/video/lightbox.css" rel="stylesheet" type="text/css" />
<style type="text/css">
/* START TO DO ADD this to the /UHG/styles/Print.css, it doesn't work here */

.printarticle .contentright {
float:right;
margin-left:30px;
margin-bottom:12px;
}

.printarticle .caption { 
font-size:85%; color: #7C878E; 
}

/* START TO DO ADD this to the timeline/css/styles.css */
#timeline .scrollbar .track .marks .mark {
background: url('/~/media/UHG/Images/Timeline/scrollbar_mark.ashx') no-repeat scroll 0px 0px transparent;
cursor: pointer;
height: 21px;
width: 13px;
}
/* START TO DO ADD this to the carousel CSS file */
        
#carousel_ul {
line-height:1.4em;
}

/* START TO DO ADD this to the master CSS file */

/* ------------- for buttons ------------- */

a#ctl05_content1_0_RadCaptcha1_CaptchaLinkButton.rcRefreshImage {
background: url('http://www.unitedhealthgroup.com/~/media/UHG/Images/Buttons/captcha-refresh.ashx');
}
.SendBtn, .SendBtn:link, input#ctl05_content1_0_SendEmail.SendBtn, #SearchSubmit a.button-box, #SearchSubmit a.button-box:link, a#SearchSubmit.button-box {
background:#0077c8;
background-color:#0077c8;
color: rgb(255, 255, 255);
text-decoration: none;
border: 1px solid rgb(225, 225, 225);
text-align: center;
padding: 3px 10px 4px;
margin: 0px 0px 5px;
display: inline-block;
white-space: nowrap;
font-weight: bold;
border-radius: 0px 0px 0px 0px;
width: 148px;
}
.SendBtn, input#ctl05_content1_0_SendEmail.SendBtn {
background: url('http://www.unitedhealthgroup.com/~/media/UHG/Images/Buttons/send.ashx');
}
.SendBtn:hover, input#ctl05_content1_0_SendEmail.SendBtn:hover, #SearchSubmit a.button-box:hover, a#SearchSubmit.button-box:hover {
background: #78be20;
background-color: #78be20;
color: rgb(255, 255, 255);
border-radius 8px 8px 8px 8px;
text-decoration: none;
}



/* ------------- for code snippet for users on page ------------- */
.mainbody pre {
white-space: pre-line;
word-wrap: break-word;
}

.center {
margin-left:auto;
margin-right:auto;
width:70%;
}

.spotlight {
width: 86%;
}

.spotlightitem .spotlightimg {
float: left;
margin: 0.2em 1.5em 1.7em 0;
width: 36%;
}
.spotlightdescription {
width: 60%; 
display:inline-block;
}

@media (max-width: 767px) {
.spotlightitem .spotlightimg {
margin: 0.2em 0.2em 1.7em 0;
float:none;
width:auto;
}

.spotlightdescription {
width: auto; 
display:inline-block;
}
}

.borderround {
-webkit-border-radius: 6px;
-moz-border-radius: 6px;
border-radius: 6px;
}

.opaquebox {
opacity:1.0;
filter:alpha(opacity=100); /* For IE8 and earlier */
}

.opaquebox:hover, .opaquebox:active {
opacity:0.4;
filter:alpha(opacity=40); /* For IE8 and earlier */
}

.borderbox {
border: 1px solid #5b6770;
-webkit-box-shadow: 0px 3px 2px #CCC;
-moz-box-shadow: 0px 3px 2px #CCC;
box-shadow: 0px 3px 2px #CCC;
}

.borderimage {
border: 1px solid #e2e7ed;
-webkit-box-shadow: 0px 3px 2px #DDDDDD;
-moz-box-shadow: 0px 3px 2px #DDDDDD;
box-shadow: 0px 3px 2px #DDDDDD;
-webkit-border-radius: 6px;
-moz-border-radius: 6px;
border-radius: 6px;
}
.borderimage:hover, .borderimage:active {
border: 1px solid #CCC;
}

/* shadow on bottom */
.shadow-effect-1 { 
box-shadow: 0px 8px 6px -6px #c1c6c8; 
-moz-box-shadow: 0 8px 6px -6px #c1c6c8;
-webkit-box-shadow: 0 8px 6px -6px #c1c6c8
}

/* shadow on top */
.shadow-effect-10 { 
box-shadow: 0px -6px 6px -6px #C1C6C8; 
-moz-box-shadow: 0px -6px 6px -6px #C1C6C8;
-webkit-box-shadow: 0px -6px 6px -6px #C1C6C8
}

.gsamainbody h1 {
font-family: FrutigerLTPro-Bold, Arial, Helvetica, sans-serif;
font-weight:normal;
letter-spacing:0em;
}
.gsamainbody h2, .gsamainbody h3, .gsamainbody h4, .gsamainbody h5, .gsamainbody h6 {
font-family: FrutigerLTPro-Roman, Arial, Helvetica, sans-serif;
font-weight:normal;
letter-spacing:0em;
}

/* jump to list styles*/
ul.sidenav_list {
                list-style:none;
                margin:0px;
                padding:0px;
                }
ul.sidenav_list li {
                padding: 8px 10px 8px 14px;
                line-height:1.2em;           
                margin:0px;
                list-style:none outside none;
                font-weight:normal;
                }
.jumpto li {
border-bottom: 1px solid rgb(216, 216, 216);
}

hr { border-top:0; border-bottom: 2px dotted #e1e1e1; color: #fff; height: 1px; border-right: none; border-left:none; width:100%; }

/* END TO DO ADD this to the master CSS file */



/* keep this one line for info-blocks for the icon that is a graphics file, not a icon */

.info-blocks .icon-info-blocks { float:left; min-width:30px; margin-top: 3px; text-align: center; }


/* REMOVE these styles upon next launch */

.tag-box-v6 { border: 1px solid #eee }

.margin-bottom-30 { margin-bottom:30px; }

/*tag box*/
.tag-box { padding:20px; margin-bottom:30px; }

.tag-box-v2 { 
border-width:1px 1px 1px 2px;
border-style:solid;
border-color:rgb(238, 238, 238) rgb(238, 238, 238) rgb(238, 238, 238) rgb(114, 192, 44);
-moz-border-top-colors:none;
-moz-border-right-colors:none;
-moz-border-bottom-colors:none;
-moz-border-left-colors:none;
border-image:none;
border-left: 2px solid rgb(120, 190, 32); }

.hero { border-left-color:rgb(114, 192, 44); }

.shadow-wrapper { z-index:1; position:relative; }

.box-shadow { background: none repeat scroll 0% 0% rgb(255, 255, 255); position: relative; }


/* fourth column */

.team .col-sm-3, .team .col-md-3 {  width: 20%; float:left; position: relative; min-height: 1px; padding-left: 15px; padding-right: 15px;  } 

/* third column */
.team .col-sm-4, .team .col-md-4, .sorting-grid .col-md-4 {  width: 27%; float:left; position: relative; min-height: 1px; padding-left: 15px; padding-right: 15px;  } 

/* two-thirds column */
.team .col-sm-8, .team .col-md-8 {  width: 62%; float:left; position: relative; min-height: 1px; padding-left: 15px; padding-right: 15px;  } 

/* half column */

.col-sm-6, .col-md-6 { width: 45%; float:left; position: relative; min-height: 1px; padding-left: 15px; padding-right: 15px; }

.sorting-grid .col-sm-6, .sorting-grid .col-md-6 { width: 40%; float:left; position: relative; min-height: 1px; padding-left: 15px; padding-right: 15px; }

/* bigger than half column */
.col-sm-7, .col-md-7 { width: 56%; float:left; position: relative; min-height: 1px; padding-left: 15px; padding-right: 15px; }

.col-sm-5, .col-md-5 { width: 34%; float:left; position: relative; min-height: 1px; padding-left: 15px; padding-right: 15px; }


/* info-blocks */

.info-blocks { margin-bottom: 15px; }
.info-blocks .info-blocks-in { padding: 0 10px; overflow: hidden; }


.sidebox { width:244px; }

.clearfix { clear: both; font-size:1px; }
.img-responsive { display: block; max-width: 100%; height: auto; vertical-align: middle;} 
.rounded { border-radius: 4px 4px 4px 4px !important; }
.rounded-2x { border-radius: 10px 10px 10px 10px !important; }

.inner-team {
padding:14px 0 10px 0;
margin:0 20px 0 20px;
border-bottom: #b1b1b1 2px dotted; 
}

.sorting-block .sorting-grid .mix { margin-bottom: 30px; }
.sorting-block .col-md-3 { width:25%; display: inline-block; position:relative; padding-left: 15px; padding-right: 15px; vertical-align:top; }
.sorting-block li { list-style: none outside none; }
.sorting-block span.sorting-cover { padding:8px; display:block; margin-top: 1px; background: #e3e3e3; border-right:1px solid #e1e1e1; border-left:1px solid #e1e1e1; border-bottom:1px solid #e1e1e1; min-height:36px; }
.sorting-block img { border:0; }

/* Home Page rotator */
#mainRotator .selector .centerimg {
padding-top: 6px;
}
#mainRotator .selector .shownum {
font-weight: bolder;
}

/* Left Navigation */
.topblue {
min-height: 30px;
padding-left:14px;
padding-right:4px;
}

/* New Blue and Green Buttons */

.btn-u { color: #ffffff; white-space: nowrap; border: 0px none; font-size: 1em; cursor: pointer; font-weight: 400; padding: 2px 13px; position: relative; display: inline-block; text-decoration: none; text-shadow: 0 -1px 0 rgba(0,0,0,0.4); }
.btn-u:hover { color: #ffffff; text-decoration: none; transition: all 0.3s ease-in-out 0s; }

.btn-u a, .btn-u a:link, .btn-u a:hover, .btn-u a:active, .btn-u a:visited { color: #ffffff; text-decoration: none; text-shadow 0 -1px 0 rgba(0,0,0,0.4); }

.btn-u.btn-u-blue { background:none repeat scroll 0% 0% #0077C8; }
.btn-u.btn-u-blue:hover, .btn-u.btn-u-blue:focus, .btn-u.btn-u-blue:active, .btn-u.btn-u-blue.active { background:none repeat scroll 0% 0% rgb(41, 128, 185); }

.btn-u.btn-u-green { background:none repeat scroll 0% 0% #78BE20; }
.btn-u.btn-u-green:hover, .btn-u.btn-u-green:focus, .btn-u.btn-u-green:active, .btn-u.btn-u-green.active { background:none repeat scroll 0% 0% rgb(95, 182, 17); }

.btn-u.btn-u-dark { background:none repeat scroll 0% 0% #5B6770; }
.btn-u.btn-u-dark:hover, .btn-u.btn-u-dark:focus, .btn-u.btn-u-dark:active, .btn-u.btn-u-green.dark { background:none repeat scroll 0% 0% rgb(51, 51, 51); }

h2 { line-height:1.5em }

/* END REMOVE these styles upon next launch */

/* KEEP this tweak upon next launch */

/* Large Text */
.headerlight {
  font:normal 4em/1.2em FrutigerLTPro-Light,'Open Sans','Trebuchet MS','Helvetica Neue',Helvetica,Arial,clean,sans-serif
}

.headerroman {
  font:normal 4em/1.2em FrutigerLTPro-Roman,'Open Sans','Trebuchet MS','Helvetica Neue',Helvetica,Arial,clean,sans-serif
}

.headerbold {
  font:normal 4em/1.2em FrutigerLTPro-Bold,'Open Sans','Trebuchet MS','Helvetica Neue',Helvetica,Arial,clean,sans-serif
}

/* Big Story Box */
.bigstory {
width:340px;
border:thin solid #dedede;
padding:15px;
background:#fff;
word-wrap:break-word;
font-size:93%;
}

/* Topics Next Page Panels */
a.nextpanel:hover { text-decoration: none; }
a.nextpanel .panel-u { color:#fff; background-color: #00629b; }
a.nextpanel .panel-u:hover { background-color: #00a9ce; }
a.nextpanel .panel-u:hover h4, a.nextpanel .panel-u p { text-decoration: none; color:#fff; }
a.nextpanel .panel-blue { color:#fff; background-color: #0077c8; } 
a.nextpanel .panel-blue:hover { background-color: #00a9ce; }
a.nextpanel .panel-blue:hover h4, a.nextpanel .panel-blue p { text-decoration: none; color:#fff; }
a.nextpanel .panel-dark-blue { color:#fff; background-color: #003c71; }
a.nextpanel .panel-dark-blue:hover { background-color: #0077c8; }
a.nextpanel .panel-dark-blue:hover h4, a.nextpanel .panel-dark-blue p { text-decoration: none; color:#fff; }
a.nextpanel .panel-green { color:#fff; background-color: #007041; }
a.nextpanel .panel-green:hover { background-color: #44883e; }
a.nextpanel .panel-green:hover h4, a.nextpanel .panel-green p { text-decoration: none; color:#fff; }

/* Article -- do we want to keep */
.articleheadline { font-family: Arial, Helvetica, clean, sans-serif !important; font-weight: bold; }

/*keep this color as our green left line tag box by adopting the bottom line into new css*/
.tag-box-v2 { 
border-left: 2px solid rgb(120, 190, 32); }

.hero { border-left-color:rgb(114, 192, 44); }

.inner-team .team-bio {
text-align: left;
padding:0 30px 0 30px;
}

.btn-u { font-size: 1em; padding: 2px 13px; text-shadow: 0 -1px 0 rgba(0,0,0,0.4); }
.btn-u a, .btn-u a:link, .btn-u a:hover, .btn-u a:active, .btn-u a:visited { color: #ffffff; text-decoration: none; text-shadow 0 -1px 0 rgba(0,0,0,0.4); }

.thumbnail-style h3 {margin: 6px 0 8px 0;}
.ltgraybox .thumbnail-style { background:#f7f7f7; }

/* END KEEP this tweak upon next launch */



/* START CSS Document for keeping in Sitecore */

.InteriorPageHeader {
height: 1.7em;
background: transparent url('http://www.unitedhealthgroup.com/UHG/Images/PageHeader_Underline.gif') repeat-x bottom;
}

/* CSS for some input fields like search */
.input-container {
    background: url(/UHG/images/bg-search.gif) no-repeat;
    width:197px;
    height:30px;
}
.input-container input{
    height:27px;
    height:27px\9; /* IE8 and below */
    color: rgb(97, 97, 97);
    line-height:23px;/*results in nice text vertical alignment*/
    line-height:15px\9;/*results in nice text vertical alignment*/
    border:none;
    background:transparent;
    padding:0 8px;/*don't start input text directly from the edge*/
    width:148px;/*full width - grey on the side - 2*10px padding*/
    font-size: 75%; 
}

/* horizontal rules */

hr { border-top:0; border-bottom: 2px dotted #e1e6e8; color: #fff; height: 2px; border-right: none; border-left:none; width:100%; }
hr.darkrule { border-top: medium none; border-left: medium none; border-bottom: #5b6770 2px dotted; border-right: medium none; }
hr.lightrule { border-top: medium none; border-left: medium none; border-bottom: #f1f6f8 2px dotted; border-right: medium none; }

ul.list-icons li {
padding:0 0 1em 0;
list-style: none;
clear:both;
} 
/* put this class on an icon img tag */
.iconlink {
float:left;
margin:0.1em 0.5em 0 0;
}

/* mdash ndash */
.mdash, .ndash {
font-family: calibri, Arial, 'Helvetica Neue', Helvetica, clean, sans-serif
}

</style>
<script type="text/javascript" src="/UHG/scripts/checkleave.js" aysnc></script>
<script type="text/javascript" src="/UHG/scripts/checkleave-es.js" aysnc></script>
<script type="text/javascript" src="/UHG/scripts/checkleave-br.js" aysnc></script>
<script type="text/javascript" src="/UHG/scripts/fls.js" aysnc></script>
<script async type="text/javascript" src="http://cdn.gigya.com/js/socialize.js?apiKey=3_Fwl9sw6wlNWuHvDwcqz0UKooUBnJFTrVh1kxaLqqxJ8da2Dyw8aV35sCeH4Cp-q-">
{
lang: "en"  
}
</script>

<script type="text/javascript">
    function printWindow() {
        var url = document.URL.replace('aspx', 'aspx?p=1&')
        window.open(url);
    }


//    function toggleFB() {
//        jQuery.noConflict();
//        jQuery("#GoogleForm").hide();
//        jQuery("#emailPanel").hide();
//        jQuery("#fbForm").slideToggle(100);

//    }

//    function toggleGoogle() {
//        jQuery.noConflict();
//        jQuery("#fbForm").hide();
//        jQuery("#emailPanel").hide();
//        jQuery("#GoogleForm").slideToggle(100);
//    }

//    function toggleEmail() {
//        jQuery.noConflict();
//        jQuery("#fbForm").hide();
//        jQuery("#GoogleForm").hide();
//        jQuery("#emailPanel").slideToggle(100);
//    }

    //Contact Us Page Toggle
    function showHideDetailContact(id) {
        for (x = 1; x <= 4; x++) {
            thisItem = 'content' + x;
            thisArrow = 'arrow' + x;
            if (x == id) {
                if (document.getElementById(thisItem).style.display == 'block') {
                    document.getElementById(thisItem).style.display = 'none';
                    document.images[thisArrow].src = '/UHG/Images/Icons/arrow_blue_new_r.gif';
                }
                else {
                    document.getElementById(thisItem).style.display = 'block';
                    document.images[thisArrow].src = '/UHG/Images/Icons/arrow_blue_new_dwn.gif';
                }
            }
            else {
                document.getElementById(thisItem).style.display = 'none';
                document.images[thisArrow].src = '/UHG/Images/Icons/arrow_blue_new_r.gif';
            }
        }
    }



    jQuery.noConflict();

    jQuery(document).ready(function () {

        //language panel
//        jQuery(".btn-slide").click(function () {
//            jQuery("#panel").slideToggle("fast");
//            jQuery(this).toggleClass("active");
//            return false;
        //        });

        var paneldown = false;
        jQuery(".btn-slide").click(function () {
            if (paneldown == false) {
                jQuery("#panel").slideDown("fast");
                jQuery(this).toggleClass("active");
                paneldown = true;
                return false;
            } else {
                jQuery("#panel").slideUp("fast");
                jQuery(this).toggleClass("active");
                paneldown = false;
                return false;
            }
        });

        //Retract Social Media and Toggles
        jQuery(document).mouseup(function (e) {
//            var EMLcontainer = jQuery("#emailPanel");
//            var FBcontainer = jQuery("#fbForm");
//            var Gcontainer = jQuery("#GoogleForm");
            var Langcontainer = jQuery("#panel");

//            if (!EMLcontainer.is(e.target) // if the target not the container and
//            && EMLcontainer.has(e.target).length === 0) // a descendent in container
//            {
//                //EMLcontainer.hide();
//            }

//            if (!FBcontainer.is(e.target) 
//            && FBcontainer.has(e.target).length === 0) 
//            {
//                FBcontainer.hide();
//            }

//            if (!Gcontainer.is(e.target) 
//            && Gcontainer.has(e.target).length === 0) 
//            {
//                Gcontainer.hide();
//            }

            if (!Langcontainer.is(e.target) 
            && Langcontainer.has(e.target).length === 0) 
            {
                jQuery("#panel").slideUp("fast");
            }

        });


        jQuery('#masterContainer').click(function () {
            if (paneldown == true) {
                jQuery("#panel").slideUp("fast");
                jQuery(".btn-slide").removeClass("active");
                paneldown = false;
                return false;
            }
        });


        //Disclaimer & Forward Looking Statement 
        jQuery('a[rel=external]').leaveNotice();
        jQuery('a[rel=external]').flsNotice();

        //Slide Box Animation
        jQuery(".boxfunction").click(function () {
            var index = jQuery(this).attr('id');
            jQuery(".boxed").hide();
            jQuery(".boxshow").hide();
            jQuery("#box" + index).animate({ height: 'toggle' }, 700);
            jQuery(".boxfunction").css('color', '#0077C8');
            jQuery(this).css('color', '#78BE20');
            return false;
        });


        //Navigation rollovers
        jQuery("img.rollover").hover(
        function () { this.src = this.src.replace("_off", "_on"); },
        function () { this.src = this.src.replace("_on", "_off"); }
        );



        //Get/Set Page Title for Email
//        var pageTitle = jQuery(this).attr('title');
//        jQuery('#emailPageTitle').html(pageTitle);

        //Slide toggle left Navigation
        jQuery("#button1").click(function () {
            var clicked = jQuery(this).next();
            jQuery(clicked).slideToggle(300);

        });

        jQuery("#button2").click(function () {
            var clicked = jQuery(this).next();
            jQuery(clicked).slideToggle(300);

        });

        jQuery("#button3").click(function () {
            var clicked = jQuery(this).next();
            jQuery(clicked).slideToggle(300);

        });

        jQuery("#button4").click(function () {
            var clicked = jQuery(this).next();
            jQuery(clicked).slideToggle(300);

        });

        jQuery("#button5").click(function () {
            var clicked = jQuery(this).next();
            jQuery(clicked).slideToggle(300);

        });

        jQuery("#button6").click(function () {
            var clicked = jQuery(this).next();
            jQuery(clicked).slideToggle(300);

        });

        jQuery("#button7").click(function () {
            var clicked = jQuery(this).next();
            jQuery(clicked).slideToggle(300);

        });

        jQuery("#button8").click(function () {
            var clicked = jQuery(this).next();
            jQuery(clicked).slideToggle(300);

        });

        jQuery("#ShowProfile").click(function () {
            jQuery("#ProfileBlock").show()
            jQuery("#ProfileBlock").animate({
                width: "600px",
                height: "580px",
                top: "200px",
                left: "420px",
                opacity: 1
            }, 400);

            return false;
        });

        jQuery("#HideProfile").click(function () {
            jQuery("#ProfileBlock").hide(500).animate({
                width: "0px",
                height: "0px",
                top: "200px",
                left: "420px",
                opacity: 1
            }, 500);
            return false;
        });

        jQuery("#rss").change(function () {
            //alert(jQuery(this).val());

            if (jQuery(this).val() == "xml") {
                jQuery(".col2").show();
                jQuery(".col3").hide();
                jQuery(".col4").hide();
                jQuery(".col5").hide();
            }

            if (jQuery(this).val() == "google") {
                jQuery(".col2").hide();
                jQuery(".col3").show();
                jQuery(".col4").hide();
                jQuery(".col5").hide();
            }

            if (jQuery(this).val() == "yahoo") {
                jQuery(".col2").hide();
                jQuery(".col3").hide();
                jQuery(".col4").show();
                jQuery(".col5").hide();
            }
            if (jQuery(this).val() == "blog") {
                jQuery(".col2").hide();
                jQuery(".col3").hide();
                jQuery(".col4").hide();
                jQuery(".col5").show();
            }
        });

        //Light Box Code

        jQuery('.bringthefader').click(function () {
            jQuery('#Overlay').fadeIn(700);
        });
        
        jQuery('#CloseVideo').click(function () {
            jQuery('#VideoBox').html("&nbsp;");
            jQuery('#Overlay').hide();
        });

        jQuery("#Overlay").css("height", jQuery(document).height());

    });

    function toggleCookie() {

            if (jQuery(this).is(':checked')) {
                jQuery(this).attr('checked', false);
                jQuery("a.LanguageLinks").each(function () {
                    var href = jQuery(this).attr('href');
                    href = href.replace("&ac=yes", ""); 
                    jQuery(this).attr("href", href);
                });
            } else {
                jQuery(this).attr('checked', true);
                jQuery("a.LanguageLinks").each(function () {
                    var _href = jQuery(this).attr("href");
                    jQuery(this).attr("href", _href + '&ac=yes');
                });
            }


        }



</script> 
 
<script type="text/javascript">
// Sitecore JavaScript 
jQuery(document).ready(function () {


jQuery("#button9").click(function () {
var clicked = jQuery(this).next();
jQuery(clicked).slideToggle(300);
});

jQuery("#button10").click(function () {
var clicked = jQuery(this).next();
jQuery(clicked).slideToggle(300);
});

jQuery("#button11").click(function () {
var clicked = jQuery(this).next();
jQuery(clicked).slideToggle(300);
});

//set font size based on the value saved in cookies
if ( size= GetCookie('FontSize') ) {
  document.body.style.fontSize = size  + "px";
  SetUnderline('size' + size );
}

//siteName: link to site
//exitMessage: Main Message when exiting the site
//preLinkMessage: Message before link to exit the site
//linkString: additional attributes to the link - ex. target='_blank' or css classes
//timeout: seconds to automatically redirect or set to 0 for no timer
//Example of link: <p><a href="http://www.google.com" class="disclaimer">Example Link</a></p>
//leaving site disclaimer - use a href with class named disclaimer

  jQuery('a.englishonly-es').leaveNotice_es({
            siteName: "careers.unitedhealthgroup.com",
            exitMessage: "<p>Ahora está entrando a una página de este sitio de internet que solo está disponible en inglés.</p>",
            preLinkMessage: "<div class='setoff'><p>Haga clic en “continuar” para ingresar a la página en inglés.<br/></p></div>",
            linkString: "target='_blank'",
            timeOut: 0
        });

  jQuery('a.englishonly-br').leaveNotice_br({
            siteName: "careers.unitedhealthgroup.com",
            exitMessage: "<p>Você está entrando em uma página que só está disponível em inglês.</p>",
            preLinkMessage: "<div class='setoff'><p>Clique “continuar” para acessar a página em inglês.<br/></p></div>",            
            linkString: "target='_blank'",
            timeOut: 0
        });


  jQuery('a.NavMenuItemEnglishOnly-es').leaveNotice_es({
            siteName: "careers.unitedhealthgroup.com",
            exitMessage: "<p>Ahora está entrando a una página de este sitio de internet que solo está disponible en inglés.</p>",
            preLinkMessage: "<div class='setoff'><p>Haga clic en “continuar” para ingresar a la página en inglés.<br/></p></div>",
            linkString: "target='_blank'",
            timeOut: 0
        });

  jQuery('a.NavMenuItemEnglishOnly-br').leaveNotice_br({
            siteName: "careers.unitedhealthgroup.com",
            exitMessage: "<p>Você está entrando em uma página que só está disponível em inglês.</p>",
            preLinkMessage: "<div class='setoff'><p>Clique “continuar” para acessar a página em inglês.<br/></p></div>",
            linkString: "target='_blank'",
            timeOut: 0
        });




  jQuery('a.disclaimer').leaveNotice({
            siteName: "www.unitedhealthgroup.com",
            exitMessage: "<p>You are now leaving our site. Links to third party sites do not imply endorsement of the sites by UnitedHealth Group Incorporated. In no event will UnitedHealth Group Incorporated be liable to any party for any damages arising out of or in connection with any use of any hyper-linked website.</p>" +
            "<p>Opinions expressed in any blog entry and in any corresponding comments are the personal opinions of the original authors, and do not necessarily reflect the views of UnitedHealth Group Incorporated.</p>",
            preLinkMessage: "<div class='setoff'><p>Click 'proceed' to leave {SITENAME}<br/></p></div>",
            linkString: "target='_blank'",
            timeOut: 0
        });


        jQuery('a.fls').flsNotice({
            siteName: "Presentations",
            exitMessage: "<p><strong>Forward-Looking Statements</strong></p><div style=\"height:150px; overflow-y:scroll; padding:12px; text-align:left; \"><p class=\"xx-small\">The statements, estimates, projections, guidance or outlook  contained in this document include &ldquo;forward-looking&rdquo; statements within the  meaning of the Private Securities Litigation Reform Act of 1995 (PSLRA). These statements  are intended to take advantage of the &ldquo;safe harbor&rdquo; provisions of the PSLRA.  Generally the words &ldquo;believe,&rdquo; &ldquo;expect,&rdquo; &ldquo;intend,&rdquo; &ldquo;estimate,&rdquo; &ldquo;anticipate,&rdquo;  &ldquo;forecast,&rdquo; &ldquo;plan,&rdquo; &ldquo;project,&rdquo; &ldquo;should&rdquo; and similar expressions identify  forward-looking statements, which generally are not historical in nature. These  statements may contain information about financial prospects, economic  conditions and trends and involve risks and uncertainties. We caution that  actual results could differ materially from those that management expects,  depending on the outcome of certain factors.<br /><br />Some factors that could cause actual results to differ  materially from results discussed or implied in the forward-looking statements  include: our ability to effectively estimate, price for and manage our medical  costs, including the impact of any new coverage requirements; new laws or  regulations, or changes in existing laws or regulations, or their enforcement  or application, including increases in medical, administrative, technology or  other costs or decreases in enrollment resulting from U.S., Brazilian and other  jurisdictions regulations affecting the health care industry; assessments for  insolvent payers under state guaranty fund laws; our ability to achieve  improvement in CMS star ratings and other quality scores that impact revenue;  reductions in revenue or delays to cash flows received under Medicare, Medicaid  and TRICARE programs, including sequestration and the effects of a prolonged  U.S. government shutdown or debt ceiling constraints; changes in Medicare,  including changes in risk adjustment data validation audits, payment adjustment  methodology or the CMS star ratings program; our participation in federal and  state health insurance exchanges which entail uncertainties associated with mix  and volume of business; cyber-attacks or other privacy or data security  incidents; failure to comply with privacy and data security regulations;  regulatory and other risks and uncertainties of the pharmacy benefits  management industry; competitive pressures, which could affect our ability to  maintain or increase our market share; challenges to our public sector contract  awards; our ability to execute contracts on competitive terms with physicians,  hospitals and other service providers; failure to achieve targeted operating  cost productivity improvements, including savings resulting from technology  enhancement and administrative modernization; increases in costs and other  liabilities associated with increased litigation, government investigations,  audits or reviews; failure to manage successfully our strategic alliances or  complete or receive anticipated benefits of acquisitions and other strategic  transactions, including our pending acquisition of Catamaran; fluctuations in  foreign currency exchange rates on our reported shareholders equity and results  of operations; downgrades in our credit ratings; adverse economic conditions,  including decreases in enrollment resulting from increases in the unemployment  rate and commercial attrition; the performance of our investment portfolio;  impairment of the value of our goodwill and intangible assets in connection  with dispositions or if estimated future results do not adequately support  goodwill and intangible assets recorded for our existing businesses or the  businesses that we acquire; increases in health care costs resulting from  large-scale medical emergencies; failure to maintain effective and efficient  information systems or if our technology products do not operate as intended;  and our ability to obtain sufficient funds from our regulated subsidiaries or  the debt or capital markets to fund our obligations, to maintain our debt to  total capital ratio at targeted levels, to maintain our quarterly dividend  payment cycle or to continue repurchasing shares of our common stock.<br /><br />This list of important factors is not intended  to be exhaustive. We discuss certain of these matters more fully, as well as  certain risk factors that may affect our business operations, financial  condition and results of operations, in our filings with the Securities and  Exchange Commission, including our annual reports on Form 10-K, quarterly  reports on Form 10-Q and current reports on Form 8-K. Any or all  forward-looking statements we make may turn out to be wrong, and can be affected  by inaccurate assumptions we might make or by known or unknown risks and  uncertainties. By their nature, forward-looking statements are not guarantees  of future performance or results and are subject to risks, uncertainties and  assumptions that are difficult to predict or quantify. Actual future results  may vary materially from expectations expressed or implied in this document or  any of our prior communications. You should not place undue reliance on  forward-looking statements, which speak only as of the date they are made. We  do not undertake to update or revise any forward-looking statements, except as  required by applicable securities laws.<br /></p></div><br />",
            preLinkMessage: "<div class='setoff'><p>Click 'Agree' to proceed to {SITENAME}<br/></p></div>",
            linkString: "target='_blank'",
            timeOut: 0
        });

        jQuery('a.flsConference').flsNotice({
            siteName: "Presentations",
            exitMessage: "<p><strong>Conference Forward-Looking Statements</strong></p><div style=\"height:150px; overflow-y:scroll; padding:12px; text-align:left; \"><p class=\"xx-small\">The statements, estimates, projections, guidance or outlook contained in these investor conference materials include &ldquo;forward-looking&rdquo; statements within the meaning of the Private Securities Litigation Reform Act of 1995 (PSLRA). These statements are intended to take advantage of the &ldquo;safe harbor&rdquo; provisions of the PSLRA. Generally the words &ldquo;believe,&rdquo; &ldquo;expect,&rdquo; &ldquo;intend,&rdquo; &ldquo;estimate,&rdquo; &ldquo;anticipate,&rdquo; &ldquo;forecast,&rdquo; &ldquo;plan,&rdquo; &ldquo;project,&rdquo; &ldquo;should&rdquo; and similar expressions identify forward-looking statements, which generally are not historical in nature. These statements may contain information about financial prospects, economic conditions and trends and involve risks and uncertainties. We caution that actual results could differ materially from those that management expects, depending on the outcome of certain factors.<br /><br />Some factors that could cause results to differ materially from results discussed or implied in the forward-looking statements include: our ability to effectively estimate, price for and manage our medical costs, including the impact of any new coverage requirements; the potential impact that new laws or regulations, or changes in existing laws or regulations, or their enforcement or application could have on our results of operations, financial position and cash flows, including as a result of increases in medical, administrative, technology or other costs or decreases in enrollment resulting from U.S., Brazilian and other jurisdictions' regulations affecting the health care industry; the impact of any potential assessments for insolvent payers under state guaranty fund laws; the impact of the Patient Protection and Affordable Care Act, which could materially and adversely affect our results of operations, financial position and cash flows through reduced revenues, increased costs, new taxes and expanded liability, or require changes to the ways in which we conduct business or put us at risk for loss of business; potential reductions in revenue or delays to cash flows received under Medicare, Medicaid and TRICARE programs, including sequestration and potential effects of a prolonged U.S. government shutdown or debt ceiling constraints; uncertainties regarding changes in Medicare, including potential changes in risk adjustment data validation audit and payment adjustment methodology; failure to comply with privacy and data security regulations; regulatory and other risks and uncertainties associated with the pharmacy benefits management industry; competitive pressures, which could affect our ability to maintain or increase our market share; the impact of challenges to our public sector contract awards; our ability to execute contracts on competitive terms with physicians, hospitals and other service professionals; increases in costs and other liabilities associated with increased litigation, government investigations, audits or reviews; failure to manage successfully our strategic alliances or complete or receive anticipated benefits of acquisitions and other strategic transactions, including the Amil acquisition; the impact of fluctuations in foreign currency exchange rates on our reported shareholders' equity and results of operations; potential downgrades in our credit ratings; our ability to attract, retain and provide support to a network of independent producers (i.e., brokers and agents) and consultants; the potential impact of adverse economic conditions on our revenues (including decreases in enrollment resulting from increases in the unemployment rate and commercial attrition) and results of operations; the performance of our investment portfolio; possible impairment of the value of our goodwill and intangible assets in connection with dispositions or if estimated future results do not adequately support goodwill and intangible assets recorded for our existing businesses or the businesses that we acquire; increases in health care costs resulting from large-scale medical emergencies; failure to maintain effective and efficient information systems or if our technology products otherwise do not operate as intended; misappropriation of our proprietary technology; failure to protect against cyber-attacks or other privacy or data security incidents; our ability to obtain sufficient funds from our regulated subsidiaries or the debt or capital markets to fund our obligations, to maintain our debt to total capital ratio at targeted levels, to maintain our quarterly dividend payment cycle or to continue repurchasing shares of our common stock; and failure to achieve targeted operating cost productivity improvements, including savings resulting from technology enhancement and administrative modernization.<br /><br />This list of important factors is not intended to be exhaustive. We discuss certain of these matters more fully, as well as certain risk factors that may affect our business operations, financial condition and results of operations, in our other periodic and current filings with the Securities and Exchange Commission, including our annual reports on Form 10-K, quarterly reports on Form 10-Q and current reports on Form 8-K. Any or all forward-looking statements we make may turn out to be wrong, and can be affected by inaccurate assumptions we might make or by known or unknown risks and uncertainties. By their nature, forward-looking statements are not guarantees of future performance or results and are subject to risks, uncertainties and assumptions that are difficult to predict or quantify. Actual future results may vary materially from expectations expressed or implied in this presentation or any of our prior communications. You should not place undue reliance on forward-looking statements, which speak only as of the date they are made. We do not undertake to update or revise any forward-looking statements, except as required by applicable securities laws.</p></div><br />",
            preLinkMessage: "<div class='setoff'><p>Click 'Agree' to proceed to the {SITENAME}<br/></p></div>",
            linkString: "target='_blank'",
            timeOut: 0
        });




        jQuery('a.fls2').flsNotice({
            siteName: "Presentations",
            exitMessage: "<p class=\"xx-small\">Statements that UnitedHealth Group publishes, including  those on this website, may contain statements, estimates, projections, guidance  or outlook that constitute &ldquo;forward-looking&rdquo; within the meaning of the Private  Securities Litigation Reform Act of 1995 (PSLRA). These statements are intended  to take advantage of the &ldquo;safe harbor&rdquo; provisions of the PSLRA. Generally the  words &ldquo;believe,&rdquo; &ldquo;expect,&rdquo; &ldquo;intend,&rdquo; &ldquo;estimate,&rdquo; &ldquo;anticipate,&rdquo; &ldquo;forecast,&rdquo;  &ldquo;plan,&rdquo; &ldquo;project,&rdquo; &ldquo;should&rdquo; and similar expressions identify forward-looking  statements, which generally are not historical in nature. These statements may  contain information about financial prospects, economic conditions and trends  and involve risks and uncertainties. We caution that actual results could  differ materially from those that management expects, depending on the outcome  of certain factors.<br /><br />A list and description of some of these risks  and uncertainties can be found in our reports filed with the Securities and  Exchange Commission from time to time, including the cautionary statements in our  annual reports on Form 10-K, quarterly reports on Form 10-Q and current reports  on Form 8-K. Any or all forward-looking statements we make may turn out to be  wrong, and can be affected by inaccurate assumptions we might make or by known  or unknown risks and uncertainties. By their nature, forward-looking statements  are not guarantees of future performance or results and are subject to risks,  uncertainties and assumptions that are difficult to predict or quantify. Actual  future results may vary materially from expectations expressed or implied in  this document or any of our prior communications. You should not place undue  reliance on forward-looking statements, which speak only as of the date they  are made. We do not undertake to update or revise any forward-looking  statements, except as required by applicable securities laws.</p>",
            preLinkMessage: "<div class='setoff'><p>Click 'Agree' to proceed to {SITENAME}<br/></p></div>",
            linkString: "target='_blank'",
            timeOut: 0
        });



//Enter Video ID and Location here.
//Example entry: 
//jQuery('#CHANGE_THIS_TO_UNIQUE_VIDEO_ID').click(function () {
//        jQuery('#VideoBox').html('<iframe width="501" height="286" 
//        src="Video/CHANGE_THIS_TO_UNIQUE_VIDEO_PAGE.aspx"
//        frameborder="0" align="middle" allowfullscreen scrolling="no"></iframe>');
//        });


    jQuery('#2013InnovationReportYouth').click(function () {
        jQuery('#VideoBox').html('<iframe width="501" height="286" src="/Video/2013InnovationReportYouth.aspx" frameborder="0" align="middle" allowfullscreen scrolling="no"></iframe>');
    });

    jQuery('#2013InnovationReportBigData').click(function () {
        jQuery('#VideoBox').html('<iframe width="501" height="286" src="/Video/2013InnovationReportBigData.aspx" frameborder="0" align="middle" allowfullscreen scrolling="no"></iframe>');
    });

    jQuery('#2013InnovationReportDiabetes').click(function () {
        jQuery('#VideoBox').html('<iframe width="501" height="286" src="/Video/2013InnovationReportDiabetes.aspx" frameborder="0" align="middle" allowfullscreen scrolling="no"></iframe>');
    });

    jQuery('#2013InnovationReportHealthNeeds').click(function () {
        jQuery('#VideoBox').html('<iframe width="501" height="286" src="/Video/2013InnovationReportHealthNeeds.aspx" frameborder="0" align="middle" allowfullscreen scrolling="no"></iframe>');
    });

    jQuery('#2013SRReportMakingADifference').click(function () {
        jQuery('#VideoBox').html('<iframe width="501" height="286" src="/Video/2013SRReportMakingADifference.aspx" frameborder="0" align="middle" allowfullscreen scrolling="no"></iframe>');
    });

    jQuery('#VideoLinkTest').click(function () {
        jQuery('#VideoBox').html('<iframe width="501" height="286" src="../Video/TestVideo.aspx" frameborder="0" align="middle" allowfullscreen scrolling="no"></iframe>');
    });

    jQuery('#VideoLinkTest2').click(function () {
        jQuery('#VideoBox').html('<iframe width="501" height="286" src="../Video/TestVideo2.aspx" frameborder="0" align="middle" allowfullscreen scrolling="no"></iframe>');
    });

    jQuery('#VideoLinkTestNews').click(function () {
        jQuery('#VideoBox').html('<iframe width="501" height="286" src="../../../Video/NewsVideo.aspx" frameborder="0" align="middle" allowfullscreen scrolling="no"></iframe>');
    });

    jQuery('#TermsBox').click(function () {
        jQuery('#VideoBox').html('<iframe width="630" height="400" src="http://www.unitedhealthgroup.com/PurchaseOrder/Default.aspx?sc_lang=en" frameborder="0" align="middle" allowfullscreen scrolling="auto"></iframe>');
    });



//Do not edit or remove:
     jQuery("#Overlay").css("height", jQuery(document).height());
});


function resizeText(id, multiplier) {
  if (document.body.style.fontSize == "") {
    document.body.style.fontSize = "13px";
  }
  //document.body.style.fontSize = multiplier + "em";
  document.body.style.fontSize = multiplier + "px";
  SetUnderline(id);
  //set cookie - never expired and for all sub domains
  document.cookie = "FontSize="+ multiplier+";path=/;expires=Fri, 31 Dec 9999 20:47:11 GMT;domain=.unitedhealthgroup.com";
}

function GetCookie (w){
	cName = "";
	pCOOKIES = new Array();
	pCOOKIES = document.cookie.split('; ');
	for(bb = 0; bb < pCOOKIES.length; bb++){
		NmeVal  = new Array();
		NmeVal  = pCOOKIES[bb].split('=');
		if(NmeVal[0] == w){
			cName = unescape(NmeVal[1]);
		}
	}
	return cName;
}

function SetUnderline(id){
  ClearUnderline('size9');
  ClearUnderline('size11');
  ClearUnderline('size13');
  ClearUnderline('size15');
  ClearUnderline('size17');
  ClearUnderline('size21');
  document.getElementById(id).style.textDecoration="underline";
}

function ClearUnderline(id) {
  if (document.getElementById(id)){
    document.getElementById(id).style.textDecoration="none";
  }
}



</script>
</head>


<!--Begin Header and Social Media-->
<style>

#SocialMediaContainer
{
margin-top:-20px;
margin-left:518px;
width:135px;
height:17px;
}
#SocialMediaContainer ul
{
    margin: 0;
    padding: 0;
    list-style-type: none;

}

#SocialMediaContainer ul li { display: inline; }

#SocialMediaContainer ul li a
{
    text-decoration: none;
    padding: 0px;
}

#UHGlogo {  clear:both; margin-top:0px; margin-left:23px; z-index:6666; height:36px; }
#searchC {  margin-top:2px; width: 250px; margin-left:4px;}



      
.printgigyacustom
{
    width:7%;
    float:left;

}
        
#componentDiv
{
    float:left;
    position:relative;
    z-index: 500;
    padding-bottom:20px;
}

.ToggleText
{
    position:relative;
    bottom: 0.2em;
    left:-0.3em;
}


</style>

<div id="headerContainer">
<div id="CountrySelector" align="right"> 
        <div id="panel">
         
         
            <a class="LanguageLinks" style="padding:6px;color:#0077C8;text-decoration: none;" href="http://es.unitedhealthgroup.com/Error/PageNotFound.aspx?sc_lang=es-MX">Español</a>
         
            <a class="LanguageLinks" style="padding:6px;color:#0077C8;text-decoration: none;" href="http://br.unitedhealthgroup.com/Error/PageNotFound.aspx?sc_lang=pt-BR">Português (Brasil)</a>
         
            <a class="LanguageLinks" style="padding:6px;color:#0077C8;text-decoration: none;" href="http://www.unitedhealthgroup.com/Error/PageNotFound.aspx?sc_lang=en"><b>English</b></a>
        
            
         
        </div>
    
    <p id="ctl01_LanguageSelectControl" class="slide"><a href="#" class="btn-slide"><span class="ToggleText">Language</span></a></p>

</div>



<!--googleoff: index-->


<div id="logo"><a href="/default.aspx"><IMG title="UnitedHeath Group®" alt="UnitedHealth Group®" src="/UHG/images/logo.gif"></a></DIV> 
<div id="searchC"><form id="mainform2" method="get" name="GSASEARCHFORM" action="/GSASearchResults.aspx">
<div class="input-container">
    <input type="text" value="search" onfocus="if (this.value=='search') this.value = ''" value="search" name="q" />
</div><INPUT class="btn" align="top" src="/UHG/images/b-search.gif" type="image">
</form></div>


<DIV class="header-bottom"><img alt="" src="/UHG/images/header_bottom.gif" width="900" height="10"></DIV>
<DIV class="header-bottom"><img alt="" src="/UHG/images/gray_spacer.gif" width="900" height="5"></DIV>

<!--googleon: index-->

</div>




<body>

<form method="post" action="PageNotFound.aspx" id="mainform">
<input type="hidden" name="RadScriptManager10_TSM" id="RadScriptManager10_TSM" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwULLTE0NTY0ODk3NDUPZBYCAgIPZBYCZg9kFgICGQ8WAh4HVmlzaWJsZWhkZLqpgkz9ucb/KWuCbSOW1bhy8A18" />


<script src="/Telerik.Web.UI.WebResource.axd?_TSM_HiddenField_=RadScriptManager10_TSM&amp;compress=1&amp;_TSM_CombinedScripts_=%3b%3bSystem.Web.Extensions%2c+Version%3d4.0.0.0%2c+Culture%3dneutral%2c+PublicKeyToken%3d31bf3856ad364e35%3aen%3a33363de7-7c08-435e-ab35-682b2ed2c688%3aea597d4b" type="text/javascript"></script>   

<!--Begin Main Page Container-->  
<div id="masterContainer">
<!--Begin Main Graphic-->  
<span id="HeaderGraphic"> 

</span>
<!--End Main Graphic-->

<!-- Begin Main Nav -->
<!--googleoff: index-->
<div id="masterNav">     
    <!----><div><a href="/Default.aspx" target="" class="NavMenuItem">Home</a><a href="/About/Default.aspx" target="" class="NavMenuItem">About</a><a href="/Businesses/Default.aspx" target="" class="NavMenuItem">Businesses</a><a href="/SocialResponsibility/Default.aspx" target="" class="NavMenuItem">Social Responsibility</a><a href="/Investors/Default.aspx" target="" class="NavMenuItem">Investors</a><a href="/Newsroom/Newsroom.aspx" target="" class="NavMenuItem">Newsroom</a><a href="/Modernization/Default.aspx" target="" class="NavMenuItem">Health Care Modernization</a><a href="http://careers.unitedhealthgroup.com/" target="_blank" class="NavMenuItem">Careers</a></div><!----> 
</div>
<!--googleon: index-->

<div id="masterContent">
 
<!--Begin Main Content-->
	<div id="main_content_onecolumn">
        <div class="mainbody">
         <div class="contentright">
            
        </div>
  
        
     
        <h1 class="colorlightcrimson">Page Not Found</h1>
<p>The page you are looking for could not be found. The page might have been removed, had its name changed, or is temporarily unavailable.</p>
<p>Please select from them menu above to find the page you were looking for. You may also visit the <a href="/Sitemap.aspx">Site Map</a>.</p>
<p>Are you looking for UnitedHealth Group news? <a href="/Newsroom/Newsroom.aspx">Click here</a>.&nbsp;<br />
Are you looking for UnitedHealthcare? <a href="http://www.uhc.com/">Click here</a>.<br />
Are you looking for United Health Foundation? <a href="http://www.unitedhealthfoundation.org/">Click here</a>.<br />
<br />
<br />
</p>

 
        
 
        
        </div>  
    </div>
<!--End Main Content-->




</div>
<!--End Main Content-->
<!-- End Main Nav -->

<!--Begin Footer-->

<!--Begin Footer-->
<!--googleoff: index-->
<div id="masterFooter" bgcolor="#E8E8E7">
<table width="900" border="0" cellspacing="0" cellpadding="0">
    <tbody>
        <tr height="28" bgcolor="#003c71">
            <td style="width: 29px;"><img width="29" height="1" style="border: 0px solid;" alt=" " src="/UHG/images/spacer.gif" /></td>
            <td style="text-align: left; color: #fff; vertical-align: middle;"><span class="NavMenuItem">Helping People Live Healthier Lives&trade;</span></td>
            <td style="text-align: right;">
            <div style="padding-bottom: 4px; padding-left: 4px; padding-right: 4px; padding-top: 4px;"><a href="#" title="Adjust Text Size" id="size9" style="color: #78be20; font-size: 9px;" onclick="resizeText('size9', 9);">A</a> <a href="#" title="Adjust Text Size" id="size11" style="color: #78be20; font-size: 11px;" onclick="resizeText('size11',11)">A</a> <a href="#" title="Adjust Text Size" id="size13" style="color: #78be20; font-size: 13px;" onclick="resizeText('size13',13)">A</a> <a href="#" title="Adjust Text Size" id="size15" style="color: #78be20; font-size: 15px;" onclick="resizeText('size15',15)">A</a> <a href="#" title="Adjust Text Size" id="size17" style="color: #78be20; font-size: 17px;" onclick="resizeText('size17',17)">A</a> <a href="#" title="Adjust Text Size" id="size21" style="color: #78be20; font-size: 21px;" onclick="resizeText('size21',21)">A</a> </div>
            </td>
            <td style="width: 29px;"><img width="29" height="28" style="border: 0px solid;" alt=" " src="/UHG/images/spacer.gif" /></td>
        </tr>
    </tbody>
</table>
<table width="900" border="0" cellspacing="0" cellpadding="0">
    <tbody>
        <tr>
            <td style="text-align: left; width: 29px;">&nbsp;</td>
            <td align="left">&nbsp;</td>
            <td>&nbsp;</td>
            <td class="alignright">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: left; width: 29px;">&nbsp;</td>
            <td align="left" colspan="3">
            <table align="left" style="width: 100%;" cellspacing="0" cellpadding="20">
                <tbody>
                    <tr>
                        <td style="text-align: left; width: 25%; vertical-align: top;">
                        <ul class="footerlist">
                            <li><a href="/About/Default.aspx">About</a>&nbsp; </li>
                            <li><a href="/Businesses/Default.aspx">Businesses</a>&nbsp; </li>
                            <li><a href="/SocialResponsibility/Default.aspx">Social Responsibility</a>&nbsp; </li>
                            <li><a href="/Investors/Default.aspx">Investors</a>&nbsp; </li>
                            <li><a href="/Newsroom/Newsroom.aspx">Newsroom</a>&nbsp; </li>
                            <li><a href="/Modernization/Default.aspx">Health Care Modernization</a>&nbsp; </li>
                            <li><a href="http://careers.unitedhealthgroup.com/">Careers</a>
                            <div></div>
                            </li>
                        </ul>
                        </td>
                        <td style="text-align: left; width: 31%; vertical-align: top;">
                        <ul class="footerlist">
                            <li><a href="/Programs/Default.aspx">Innovation</a>&nbsp;&nbsp;&nbsp; </li>
                            <li><a href="/Topics/Default.aspx">Let's Get to a Better Future, Faster</a>&nbsp;&nbsp;&nbsp; </li>
                            <li><a href="/GlobalHealth/Default.aspx">Global Health</a>&nbsp; </li>
                            <li><a href="/Ventures/Default.aspx">Ventures</a>&nbsp; </li>
                            <li><a href="/Diabetes/Default.aspx">Diabetes Prevention &amp; Control</a>&nbsp; </li>
                            <li><a href="http://www.unitedhealthfoundation.org/">United Health Foundation</a>&nbsp; </li>
                            <li><a href="/Diversity/Default.aspx">Diversity &amp; Inclusion</a>&nbsp; </li>
                            <li><a href="/NursingAdvancement/Default.aspx">Nursing Advancement</a>&nbsp; </li>
                        </ul>
                        </td>
                        <td style="text-align: left; width: 17%; vertical-align: top;">
                        <ul class="footerlist">
                            <li><a href="http://es.unitedhealthgroup.com/Default.aspx?sc_lang=es-MX">Spanish</a>&nbsp; </li>
                            <li><a href="http://br.unitedhealthgroup.com/?sc_lang=pt-BR">Portuguese</a>&nbsp; </li>
                            <li><a href="/ContactUs.aspx">Contact Us</a>&nbsp; </li>
                            <li><a href="/Privacy.aspx">Privacy Policy</a>&nbsp; </li>
                            <li><a href="/TermsOfUse.aspx">Terms Of Use</a>&nbsp; </li>
                            <li><a href="/Sitemap.aspx">Site Map</a>&nbsp; </li>
                        </ul>
                        </td>
                        <td style="text-align: left; width: 27%; vertical-align: top;">
                        <ul class="footerlist">
                            <li><a href="/Newsroom/SocialMedia.aspx">Connect</a>&nbsp; </li>
                            <li style="line-height: 1.8em;"><a href="/Newsroom/RSS.aspx"><img style="float: left; margin-right: 3px;" alt="RSS" src="/~/media/UHG/Images/Icons/SmRSSbw18.ashx" />&nbsp;RSS Feeds</a>&nbsp;<br style="clear: both;" /></li>

<li style="line-height: 1.8em;"><a href="https://twitter.com/UnitedHealthGrp"  target="_blank"><img width="18" height="18" style="float: left; margin-right: 3px;" alt="Twitter" src="/~/media/UHG/Images/Icons/SmTwitterbw18.ashx" originalAttribute="src" originalPath="/~/media/UHG/Images/Icons/SmTwitterbw18.ashx" />&nbsp;Follow @UnitedHealthGrp</a>&nbsp;<br style="clear: both;" />
                            </li>

                            <!-- ALTERNATE
                                                        <li style="line-height: 1.8em;"><a href="http://www.linkedin.com/companies/1720" originalAttribute="href" originalPath="http://www.linkedin.com/companies/1720" target="_blank"><img width="18" height="18" style="float: left; margin-right: 3px;" alt="LinkedIn" src="/~/media/UHG/Images/Icons/SmLinkedInbw18.ashx" originalAttribute="src" originalPath="/~/media/UHG/Images/Icons/SmLinkedInbw18.ashx" />&nbsp;Follow Us on LinkedIn</a> <br style="clear: both;" />
                            </li>
                            <li style="line-height: 1.8em;"><a href="http://www.facebook.com/uhgcareers" originalAttribute="href" originalPath="http://www.facebook.com/uhgcareers" target="_blank"><img width="18" height="18" style="float: left; margin-right: 3px;" alt="Facebook" src="/~/media/UHG/Images/Icons/SmFacebookbw18.ashx" originalAttribute="src" originalPath="/~/media/UHG/Images/Icons/SmFacebookbw18.ashx" />&nbsp;Visit Us on Facebook</a>&nbsp;<br style="clear: both;" />
                            </li>
                            <li style="line-height: 1.8em;"><a href="http://www.youtube.com/user/unitedhealthgroup" originalAttribute="href" originalPath="http://www.youtube.com/user/unitedhealthgroup" target="_blank"><img width="18" height="18" style="float: left; margin-right: 3px;" alt="YouTube" src="/~/media/UHG/Images/Icons/SmYouTubebw18.ashx" originalAttribute="src" originalPath="/~/media/UHG/Images/Icons/SmYouTubebw18.ashx" />&nbsp;See Our YouTube Channel</a> <br style="clear: both;" />
                            </li>END ALTERNATE -->
                            <li><a href="/Newsroom/SocialMedia.aspx">more...</a>&nbsp; </li>
                        </ul>
                        </td>
                    </tr>
                </tbody>
            </table>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: left; width: 29px;"><img width="29" height="1" style="border: 0px solid;" alt=" " src="/UHG/images/spacer.gif" /></td>
            <td align="left">
            <p class="xx-small"><span class="colorgray">&copy; UnitedHealth Group. All rights reserved.</span> </p>
            </td>
            <td>&nbsp;</td>
            <td>
            <p class="alignright">&nbsp;</p>
            </td>
            <td><img width="10" height="1" style="border: 0px solid;" alt=" " src="/UHG/images/spacer.gif" /></td>
        </tr>
        <tr height="32">
            <td colspan="5"><img width="1" height="32" style="border: 0px solid;" alt=" " src="/UHG/images/spacer.gif" /></td>
        </tr>
    </tbody>
</table>
</div>
<!--googleon: index--><!-- END masterFooter -->

<!--End Footer-->
<!--End Footer-->

</div>
<!--End Main Page Container-->  

</form></body>
</html>
