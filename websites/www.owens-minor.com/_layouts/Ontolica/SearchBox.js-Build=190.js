/* Ontolica Search Box (OSB) related javascript methods */

function OSB_Submit()
{ 
  var referrerElem = document.getElementById('OSB_Referrer');
  if (referrerElem == null) return;  
  var scopeElem = document.getElementById('OSB_Scopes');
  if (scopeElem == null) return;
  var contextScopeTypeElem = document.getElementById('OSB_ContextScopeType');
  var contextScopeNameElem = document.getElementById('OSB_ContextScopeName');
  
  var scopeParams = scopeElem.value.split(';');
  var scopeType = scopeParams[0];
  var targetUrl = scopeParams[1];
  var scopeName = scopeParams[2];
  var contextUrl = scopeParams[3];
  
  var keywordsElem = document.getElementById('OSB_Keywords');
  if (keywordsElem == null) return;

  var k = keywordsElem.value;    
  var requestUrl = targetUrl;
  requestUrl += '?k=' + encodeURIComponent(k);
  requestUrl += '&s=' + scopeName;
  requestUrl += '&start1=0';
  
  if (contextScopeTypeElem != null)
  {
    requestUrl += '&ct=' + contextScopeTypeElem.value;
  }

  if (contextScopeNameElem != null)
  {
    requestUrl += '&cs=' + contextScopeNameElem.value;
  }
  
  if (contextUrl != '')
  {
    requestUrl += '&cu=' + contextUrl;
  }

  requestUrl += '&ref=' + referrerElem.value;    
  requestUrl += '&ret=' + encodeURIComponent(window.location.href);
  
  window.location.href = requestUrl;
}

function OSB_NavigateTo(target)
{ 
  var referrerElem = document.getElementById('OSB_Referrer');
  if (referrerElem == null) return;  
  var scopeElem = document.getElementById('OSB_Scopes');
  if (scopeElem == null) return;
  var contextScopeTypeElem = document.getElementById('OSB_ContextScopeType');
  var contextScopeNameElem = document.getElementById('OSB_ContextScopeName');
  
  var scopeParams = scopeElem.value.split(';');
  var scopeType = scopeParams[0];
  var scopeName = scopeParams[2];
  var contextUrl = scopeParams[3];
  
  var requestUrl = target;
  requestUrl += '?k=';
  requestUrl += '&s=' + scopeName;
  requestUrl += '&start1=0';
  
  if (contextScopeTypeElem != null)
  {
    requestUrl += '&ct=' + contextScopeTypeElem.value;
  }

  if (contextScopeNameElem != null)
  {
    requestUrl += '&cs=' + contextScopeNameElem.value;
  }
  
  if (contextUrl != '')
  {
    requestUrl += '&cu=' + contextUrl;
  }

  requestUrl += '&ref=' + referrerElem.value;    
  requestUrl += '&ret=' + encodeURIComponent(window.location.href);
  
  window.location.href = requestUrl;
}

function OSB_HandleEnter(event1) 
{ 
  var kCode = String.fromCharCode(event1.keyCode);
  if (kCode == '\n' || kCode == '\r')
  {   
    OSB_Submit();
    return false;
  }
  return true;
}
