var QLC=0;
var currentResultRow = null;

/**************************************************************/
/* Ontolica Search Tab (OST) related javascript methods       */
/**************************************************************/

function OST_SetTab(target)
{
  if (target != null && target.length > 0)
  {
    var queryUrl = new QueryUrl(window.location.href);
    queryUrl.SetTarget(target);     
    queryUrl.SetParameter('start1', null);
    queryUrl.SetParameter('w', null);
    queryUrl.SetParameter('f', null);    
    queryUrl.SetParameter('s', null);    
    queryUrl.SetParameter('sort', null);
    queryUrl.SetParameter('k', OSD_GetAllKeywords('OSD'));    
    window.location.href = queryUrl.ToString();
  }
}

/**************************************************************/
/* Ontolica Search Summary (OSS) related javascript methods   */
/**************************************************************/

function OSS_CreateSearchRSSFeed(target)
{
  var k=OSD_GetExpressionString('OSD');
  var queryUrl = new QueryUrl(window.location.href);
  queryUrl.SetTarget(target);     
  queryUrl.SetParameter('k', k);
  queryUrl.SetParameter('w', null);
  queryUrl.SetParameter('f', null);    
  queryUrl.SetParameter('ct', null);    
  queryUrl.SetParameter('ref', null);    
  queryUrl.SetParameter('ret', null);    
  
  /* RSS does not accept Ontolica context scopes. */
  /* Remove if the selected scope is a context scope. */
  var s = queryUrl.GetParameter('s');
  var cs = queryUrl.GetParameter('cs');
  if (cs != null && s != null && cs == s)
  {
    queryUrl.SetParameter('s', null);
    
    var cu = queryUrl.GetParameter('cu');
    if (cu != null)
    {
      queryUrl.SetParameter('u', cu);
    }    
  }
  queryUrl.SetParameter('cs', null);
  queryUrl.SetParameter('cu', null);
    
  window.location.href = queryUrl.ToString();
}

function OSS_SetSuggestion(suggestion)
{
  var elem = document.getElementById('OSS_SS');
  if (elem != null)
  {
    suggestion = elem.value;
  }

  var expr = new QueryExpression();
  expr.AddAllKeywords(suggestion);
  OSD_GetExpression('OSD', expr, true);
  var k=expr.GetExpressionString();
  var queryUrl = new QueryUrl(window.location.href);
  queryUrl.SetParameter('k', k);
  window.location.href = queryUrl.ToString();
}

function OSS_SetStartRow(startRow)
{
  var queryUrl = new QueryUrl(window.location.href);
  queryUrl.SetParameter('start1', startRow);     
  window.location.href = queryUrl.ToString();
}

function OSS_SetSort(sortId)
{
  var queryUrl = new QueryUrl(window.location.href);
  queryUrl.SetParameter('sort', sortId);
  window.location.href = queryUrl.ToString();
}

function OSS_SetGrouping(elem)
{
  var queryUrl = new QueryUrl(window.location.href);
  queryUrl.SetParameter('group', elem.value);
  window.location.href = queryUrl.ToString();
}

function OSS_AddRefinement(property, value, displayValue, propertyOperator)
{
  if (propertyOperator == null)
    property += '=';
  else
    property += propertyOperator;
   
  var queryUrl = new QueryUrl(window.location.href);
  queryUrl.SetParameter('start1', null);
  var f = queryUrl.GetParameter('f');
  if (f == null)
  {
    f = property + value;
  } else
  {
    index1 = f.indexOf(property);
    if (index1 >= 0)
    { 
      index2 = f.indexOf(' ', index1 + property.length);
      if (index2 > 0)
      {
        ff = f.substr(0, index1);
        ff += property;
        ff += value;
        ff += f.substr(index2);
        f = ff;
      } else      
      {
        f = f.substr(0, index1);
        f += property;
        f += value;
      }
    } else
    {
      f += ' ' + property + value;
    }
  }
  
  queryUrl.SetParameter('f', f);
  
  if (property.toLowerCase().indexOf('site') == 0)
  {
    queryUrl.SetParameter('st', displayValue);
  }
  
  window.location.href = queryUrl.ToString();
}

function OSS_AddRefinement2(suggestionId)
{
  var tdElem = document.getElementById(suggestionId);
  
  var mapping = tdElem.getAttribute('ddmapping');
  var value = tdElem.getAttribute('ddvalue');
  var display = tdElem.getAttribute('dddisplay');;
  var oper = tdElem.getAttribute('ddoperator');;
  
  OSS_AddRefinement(mapping, value, display, oper);
}

function OSS_RemoveRefinement(expr)
{
  var queryUrl = new QueryUrl(window.location.href);
  var f = queryUrl.GetParameter('f');
  if (f == null) return;
      
  var index1 = f.indexOf(expr);
  if (index1 >= 0)
  { 
    ff = f.substr(0, index1);
    ff += f.substr(index1+expr.length);
    
    /* Trim */
    while (ff.charAt(0) == ' ') ff = ff.substr(1);
    while (ff.charAt(ff.length-1) == ' ') ff = ff.substr(0, ff.length-1);

    queryUrl.SetParameter('f', ff);
    
    if (expr.toLowerCase().indexOf('site') == 0)
    {
      queryUrl.SetParameter('st', null);
    }
    
    window.location.href = queryUrl.ToString();    
  }
}

function OSS_RemoveRefinement2(refinementId)
{
  var tdElem = document.getElementById(refinementId);
  var expr = tdElem.getAttribute('ddexpr');
  OSS_RemoveRefinement(expr);
}

function OSS_RemoveAllRefinements()
{
  var queryUrl = new QueryUrl(window.location.href);
  queryUrl.SetParameter('f', null);
  window.location.href = queryUrl.ToString();
}

function OSS_SaveSearch()
{
  var args = new Array();
  var queryUrl = new QueryUrl(window.location.href);
  args[0] =  'Search: ' + queryUrl.GetParameter('k');
  args[1] = window.location.href;
  args[2] = (document.getElementById('ServerTemplate')) ? document.getElementById('ServerTemplate').value : '';
  var features = 'resizable=no,status=no,scrollbars=yes,menubar=no,directories=no,location=no,width=750,height=475';
  if (browseris.ie55up)
    features = 'resizable: no; status: no; scroll: yes; help: no; center: yes; dialogWidth: 750px; dialogHeight: 475px;';
  commonShowModalDialog('http://www.owens-minor.com/_layouts/QuickLinksDialog.aspx', features, null, args);
}

/*************************************************************/
/* Ontolica Search Dialog (OSD) related javascript methods   */
/*************************************************************/

function OSD_Submit(dlgname)
{   
  var target = OSD_GetTarget(dlgname);  
  var k = OSD_GetExpressionString(dlgname);  
  var queryUrl = new QueryUrl(window.location.href);
  queryUrl.SetTarget(target);
  queryUrl.SetParameter('k', k);
  queryUrl.SetParameter('start1', null);

  var s = OSD_GetSearchScopes(dlgname);
  if (s != '')
  {
    queryUrl.SetParameter('s', s);
  }
    
  window.location.href = queryUrl.ToString();
}

function OSD_SetQuickFilter(dlgname, query)
{   
  var elements = document.forms[0].elements;
  var name = dlgname + '_QF';
  for (elemIdx=0; elemIdx<elements.length; elemIdx++)
  {
    var elem = elements[elemIdx];
    if (elem.name == name)
    {
      elem.value = query;
      OSD_Submit(dlgname);
      break;
    }
  }
}

function OSD_ChangeQuickFilter(dlgname, query)
{
  var k = OSD_GetExpressionString(dlgname);  
  if (k == null) return;
  if (k == "") return;
  if (k.substr(1) == query) return;
  
  OSD_Submit(dlgname);
}

function OSD_Clear(dlgname)
{   
  var elements = document.forms[0].elements;
  for (elemIdx=0; elemIdx<elements.length; elemIdx++)
  {
    var elem = elements[elemIdx];
    var name = elem.name;
    if (name == null || name.indexOf(dlgname) == -1) continue;  
    elem.value = '';      
  }
}

function OSD_HandleEnter(event1, dlgname) 
{ 
  var kCode = String.fromCharCode(event1.keyCode);
  if(kCode == "\n" || kCode == "\r")
  {   
    OSD_Submit(dlgname);
    return false;
  }
  return true;
}

function OSD_NavigateTo(dlgname, target)
{
  var k = OSD_GetExpressionString(dlgname);
  var queryUrl = new QueryUrl(window.location.href);
  queryUrl.SetTarget(target);
  queryUrl.SetParameter('k', k);
     
  window.location.href = queryUrl.ToString();
}

function OSD_GetSearchScopes(dlgname)
{
  var scopesStr = '';
  
  var elements = document.forms[0].elements;
  for (elemIdx=0; elemIdx<elements.length; elemIdx++)
  {
    var elem = elements[elemIdx];
    var name = elem.name;
    if (name == null || name.indexOf(dlgname) == -1) continue;
    if (name.indexOf('_SearchScope') != dlgname.length) continue;
    
    if (elem.tagName == 'SELECT')
    {
      var option = elem.options[elem.selectedIndex];
      if (option.value != null && option.value.length > 0)
        scopesStr += option.value;
      else
        scopesStr += option.text;
    } else if (elem.tagName == 'INPUT')
    {
      if (scopesStr.length > 0) scopesStr += ',';
      scopesStr += elem.text;
    }
  }
  
  return scopesStr;
}

function OSD_SetAllKeywords(dlgname, keyword)
{
  var elements = document.forms[0].elements;
  for (elemIdx=0; elemIdx<elements.length; elemIdx++)
  {
    var elem = elements[elemIdx];
    var name = elem.name;
    if (name == null || name.indexOf(dlgname) == -1) continue;
    if (elem.value == '') continue;
    var dlgnameEndIdx = dlgname.length;
    
    if (name.indexOf('_AllKeywords') == dlgnameEndIdx)
    {
      elem.value = keyword;
      break;
    }
  }
}

function OSD_GetAllKeywords(dlgname)
{
  var formElementName = dlgname + '_AllKeywords';
  var elements = document.forms[0].elements;
  var allKeywordsElement = elements[formElementName];
  if (allKeywordsElement != null)
    return allKeywordsElement.value;
  return '';    
}

function OSD_GetExpressionString(dlgname)
{
  var expr = new QueryExpression();
  OSD_GetExpression(dlgname, expr, false);
  return expr.GetExpressionString();
}

function OSD_GetExpression(dlgname, expr, excludeAllKeywords)
{      
  var booleanOperator = 'AND';
  var propertyName = '';  
  var propertyOperator = '';
  var propertyValue = '';
  var propertyRow = 0;
  var propertyId = '';
  
  var elements = document.forms[0].elements;
  for (elemIdx=0; elemIdx<elements.length; elemIdx++)
  {
    var elem = elements[elemIdx];
    var name = elem.name;
    if (name == null || name.indexOf(dlgname) == -1) continue;
    if (elem.value == '') continue;
    if (elem.tagName == 'INPUT')
    {
      if (elem.type == 'radio' && !elem.checked) continue;
      if (elem.type == 'checkbox' && !elem.checked) continue;
    }
    var dlgnameEndIdx = dlgname.length;
    
    /* Check for keywords */
    if (name.indexOf('_AllKeywords') == dlgnameEndIdx)
    {
      if (excludeAllKeywords) continue;
      expr.AddAllKeywords(elem.value); continue;
    } else if (name.indexOf('_KeywordPhrase') == dlgnameEndIdx)
    {
      expr.AddKeywordPhrase(elem.value); continue;
    } else if (name.indexOf('_AnyKeywords') == dlgnameEndIdx)
    {
      expr.AddAnyKeywords(elem.value); continue;
    } else if (name.indexOf('_NoKeywords') == dlgnameEndIdx)
    {
      expr.AddNoKeywords(elem.value); continue;
    } else if (name.indexOf('_ProximityKeywords') == dlgnameEndIdx)
    {
      expr.AddProximityKeywords(elem.value); continue;
    }    
    
    /* Check for properties */
    if (name.indexOf('_BO_') == dlgnameEndIdx)
    {
      booleanOperator = elem.value.toUpperCase();
    } else if (name.indexOf('_PP_') == dlgnameEndIdx)
    {
      val = elem.value;
      propertyId = val.substr(0, val.indexOf(':')); 
      propertyName = val.substr(val.indexOf(':')+1);       
      propertyRow = name.substr(dlgnameEndIdx+4);
    } else if (name.indexOf('_PN_') == dlgnameEndIdx)
    {
      propertyName = elem.value; 
      propertyId = '';
      continue;
    } else if (name.indexOf('_PO_') == dlgnameEndIdx)
    {
      if (propertyId != '' && name.indexOf(propertyRow + '_' + propertyId) == -1) continue;
      propertyOperator = elem.value; 
      continue;
    } else if (name.indexOf(dlgname+'_PV_') >= 0)
    {
      if (propertyId != '' && name.indexOf(propertyRow + '_' + propertyId) == -1) continue;    
      propertyValue = elem.value;
      expr.AddProperty(booleanOperator, propertyName, propertyOperator, propertyValue);
      propertyName = '';
      propertyOperator = '';
      propertyValue = '';
      booleanOperator = 'and';
      continue;
    } else if (name.indexOf('_QF') == dlgnameEndIdx)
    {
      expr.AddQuickFilter(elem.value);
    }
  }      
}

function OSD_GetTarget(dlgname)
{
  var target = null;
    
  if (IsDefined('searchResultPageUrl'))
  {  
    if (searchResultPageUrl != null && searchResultPageUrl != '')
    {
      target = searchResultPageUrl;
    }
  } 
  
  if (target == null)
  {
    target = window.location.href;
    var queryIndex = target.indexOf('?');
    if (queryIndex > 0)
      target = target.substr(0, queryIndex);
  }
    
  return target;
}

/*************************************************/
/* Property Builder Methods                      */
/*************************************************/

function OSD_UpdatePropertyPicker(name)
{  
  var elements = document.forms[0].elements;

  /* Locate Property Picker Drop Down */
  var operatorName = '';
  var valueName = '';
  var btncolid = '';
  var elemIdx=0;
  while (elemIdx<elements.length)
  {
    var elem = elements[elemIdx++];
    if (elem.name != name) continue;
    
    var propId = elem.value.substr(0, elem.value.indexOf(':'));    
    operatorName = name.replace('_PP_', '_PO_') + '_' + propId;
    valueName = name.replace('_PP_', '_PV_') + '_' + propId;
        
    break;
  }   
  
  /* Locate and unhide property operator */
  while (elemIdx<elements.length)
  {
    var elem = elements[elemIdx++];
    if (elem.name == operatorName) 
      elem.style.display = '';
    else
      elem.style.display = 'none';
      
    if (elem.name.indexOf('_PO_') == -1) 
    {
      elemIdx--;
      break;  
    }
  }  
  
  /* Locate and unhide property value */
  while (elemIdx<elements.length)
  {
    var elem = elements[elemIdx++];
    if (elem.name.indexOf('_PV_') == -1) break;  
    if (elem.name == valueName) 
      elem.style.display = '';
    else
      elem.style.display = 'none';      
  }  
  
  var currRowId = name.replace('_PP_', '_ROW_');
  OSD_UpdatePropertyButtons(currRowId);
}

function OSD_UpdatePropertyButtons(fromRowId)
{
  var form = document.forms[0];

  var idx = fromRowId.indexOf('_ROW_');
  var row = fromRowId.substr(idx+5);
  var rowNum = parseInt(row);
  var rowIdPrefix = fromRowId.substr(0, idx+5);
  
  while (true)
  {
    var currRowId = rowIdPrefix + rowNum;
    var currRowElement = document.getElementById(currRowId);
    if (currRowElement == null) break;
    
    /* Find Buttons */
    var delBtnId = currRowId.replace('_ROW_', '_DEL_');
    var delBtnElement = document.getElementById(delBtnId);    
    var addBtnId = currRowId.replace('_ROW_', '_ADD_');
    var addBtnElement = document.getElementById(addBtnId);

    /* Find next row */
    var nextRowNum = rowNum + 1;
    var nextRowId = rowIdPrefix + nextRowNum;
    var nextRowElement = document.getElementById(nextRowId);    

    /* Update Delete Button */    
    var pickerName = currRowId.replace('_ROW_', '_PP_');
    var pickerElement = form.elements[pickerName];
    if (pickerElement.selectedIndex == 0)
      delBtnElement.style.display = 'none';
    else
      delBtnElement.style.display = currRowElement.style.display;

    /* Update Add Button */    
    if (nextRowElement == null || pickerElement.selectedIndex == 0)
    {
      addBtnElement.style.display = 'none';
    } else if (nextRowElement.style.display == 'none')
    {
      addBtnElement.style.display = '';
    } else
    {
      addBtnElement.style.display = 'none';
    }
    
    if (nextRowElement == null) break;
    
    rowNum++;
  }  
}

function OSD_AddProperty(name)
{
  var idx = name.indexOf('_ADD_');
  var row = name.substr(idx+5);
  var nextRowNum = parseInt(row) + 1;
  var currRowId = name.replace('_ADD_', '_ROW_');
  var nextRowId = currRowId.substr(0, idx+5) + nextRowNum;
      
  /* Check for more rows */
  var nextRowElement = document.getElementById(nextRowId);
  if (nextRowElement != null)
  {
    nextRowElement.style.display = '';

    /* Select the same property in the new row. */
    var form = document.forms[0];
    var currentRowPropertyPickerName = name.replace('_ADD_', '_PP_');
    var nextRowPropertyPickerName = nextRowId.replace('_ROW_', '_PP_');
    form.elements[nextRowPropertyPickerName].selectedIndex = form.elements[currentRowPropertyPickerName].selectedIndex;   
    OSD_UpdatePropertyPicker(nextRowPropertyPickerName);
  }
  
  OSD_UpdatePropertyButtons(currRowId);
}

function OSD_RemoveProperty(name)
{
  var form = document.forms[0];
  var idx = name.indexOf('_DEL_');
  var row = name.substr(idx+5);
  var rowNum = parseInt(row);
  var rowIdPrefix = name.replace('_DEL_', '_ROW_').substr(0, idx+5);
  var pickerNamePrefix = name.replace('_DEL_', '_PP_').substr(0, idx+4);

  /* Pull following rows upwards */
  var prevRowNum = rowNum - 1;
  var nextRowNum = rowNum + 1;    
  
  var prevRowId = rowIdPrefix + prevRowNum;
  var currRowId = rowIdPrefix + rowNum;
  var nextRowId = rowIdPrefix + nextRowNum;

  var prevRowElement = document.getElementById(prevRowId);
  var currRowElement = document.getElementById(currRowId);
  var nextRowElement = document.getElementById(nextRowId);

  var currPickerName = pickerNamePrefix + rowNum;
  var currValueName = currPickerName.replace('_PP_', '_PV_');
  var currPickerElement = form.elements[currPickerName];
  
  
  if (nextRowElement == null)
  {
    currRowElement.style.display = 'none';      
    OSD_ClearPropertyRow(currRowId);
  } else if (nextRowElement.style.display != 'none')
  {
    OSD_ShiftPropertyRowsUp(currRowId);
  } else if (prevRowElement != null)
  {
    currRowElement.style.display = 'none';      
    OSD_ClearPropertyRow(currRowId);
  } else
  {
    OSD_ClearPropertyRow(currRowId);
    OSD_HideElement(name);
  }    

  if (prevRowElement != null)    
    OSD_UpdatePropertyButtons(prevRowId);
  else
    OSD_UpdatePropertyButtons(currRowId);
}

function OSD_ShiftPropertyRowsUp(fromRowId)
{
  var idx = fromRowId.indexOf('_ROW_');
  var row = fromRowId.substr(idx+5);
  var rowNum = parseInt(row);
  var rowIdPrefix = fromRowId.substr(0, idx+5);
  var destRowNum = rowNum;
  var sourceRowNum = rowNum + 1;
  
  while (true)
  {
    var destRowId = rowIdPrefix + destRowNum;
    var sourceRowId = rowIdPrefix + sourceRowNum;
    var destRowElement = document.getElementById(destRowId);
    var sourceRowElement = document.getElementById(sourceRowId);
    if (sourceRowElement == null || sourceRowElement.style.display == 'none')
    {
      destRowElement.style.display = 'none';      
      OSD_ClearPropertyRow(destRowId);    
      break;
    }
    OSD_CopyPropertyRow(sourceRowId, destRowId);
    
    destRowNum++;
    sourceRowNum++;
  }
}

function OSD_CopyPropertyRow(sourceRowId, destRowId)
{
  OSD_ClearPropertyRow(destRowId);
  var form = document.forms[0];

  /* Copy Picker Selection */
  var sourcePickerName = sourceRowId.replace('_ROW_', '_PP_');
  var destPickerName = destRowId.replace('_ROW_', '_PP_');
  var sourcePickerElement = form.elements[sourcePickerName];  
  var destPickerElement = form.elements[destPickerName];  
  var pickerValue = sourcePickerElement.value;
  var propertyId = pickerValue.substr(0, pickerValue.indexOf(':'));  
  destPickerElement.selectedIndex = sourcePickerElement.selectedIndex;
  
  /* Copy Operator Selection */
  var sourceOperatorName = sourcePickerName.replace('_PP_', '_PO_') + '_' + propertyId;
  var destOperatorName = destPickerName.replace('_PP_', '_PO_') + '_' + propertyId;
  var sourceOperatorElement = form.elements[sourceOperatorName];
  var destOperatorElement = form.elements[destOperatorName];
  destOperatorElement.selectedIndex = sourceOperatorElement.selectedIndex;

  /* Copy Value Selection */
  var sourceValueName = sourcePickerName.replace('_PP_', '_PV_') + '_' + propertyId;
  var destValueName = destPickerName.replace('_PP_', '_PV_') + '_' + propertyId;
  var sourceValueElement = form.elements[sourceValueName];
  var destValueElement = form.elements[destValueName];
  if (destValueElement.tagName == 'INPUT') 
    destValueElement.value = sourceValueElement.value;
  else if (destValueElement.tagName == 'SELECT')
    destValueElement.selectedIndex = sourceValueElement.selectedIndex;
  
  OSD_UpdatePropertyPicker(destPickerName);
}

function OSD_ClearPropertyRow(rowId)
{
  var form = document.forms[0];
  var pickerName = rowId.replace('_ROW_', '_PP_');
  var pickerElement = form.elements[pickerName];  
  if (pickerElement == null) return;

  /* Clear Property Picker */
  var pickerValue = pickerElement.value;
  var propertyId = pickerValue.substr(0, pickerValue.indexOf(':'));  
  var option = pickerElement.options[pickerElement.selectedIndex];
  option.className = option.className.replace('o-srch-inputhl', '');
  pickerElement.selectedIndex = 0;
  
  /* Clear Property Operator */
  var operatorName = pickerName.replace('_PP_', '_PO_') + '_' + propertyId;
  var operatorElement = form.elements[operatorName];
  if (operatorElement != null)
  {
    option = operatorElement.options[operatorElement.selectedIndex];
    option.className = option.className.replace('o-srch-inputhl', '');
  }
  
  /* Clear Property Value */
  var valueName = pickerName.replace('_PP_', '_PV_') + '_' + propertyId;
  var valueElement = form.elements[valueName];
  if (valueElement != null)
  { 
    if (valueElement.tagName == 'INPUT') 
      valueElement.value = '';
    else if (valueElement.tagName == 'SELECT')
      valueElement.selectedIndex = 0;
    valueElement.className = valueElement.className.replace('o-srch-inputhl', '');
  }
     
  OSD_UpdatePropertyPicker(pickerName);
}

function OSD_HideElement(id)
{
  var element = document.getElementById(id);
  if (element != null)
  {
    element.style.display = 'none';
  }
}


/*************************************************/
/* Search Result Methods                         */
/*************************************************/

function OSR_ViewDuplicates(itemID)
{
	 var resultRow = document.getElementById(itemID);  
   var path = resultRow.getAttribute('COL_Path');
   
   var queryUrl = new QueryUrl(window.location.href);
   var k = queryUrl.GetParameter('k');
   k += ' duplicates=\"' + path + '\"';
   queryUrl.SetParameter('k', k);
       
   window.location.href = queryUrl.ToString();
}

function OSR_OnResultItem(elm)
{
	if (!IsMenuEnabled())
		return false;
	if (IsMenuOn())
	{
		StartDeferItem(elm);
		return false;
	}
	if (itemTable !=null) OSR_OutItem();
	itemTable=elm;
	currentItemID=itemTable.getAttribute('ItemId');
  
	currentResultRow = document.getElementById(currentItemID);
  if (currentResultRow == null) return;
  var actionmask = currentResultRow.getAttribute('actionmask');
  if (actionmask == null || actionmask == '') return;
  if (actionmask.indexOf('1') == -1) return;

	if (browseris.nav6up)
		itemTable.className="o-menu-selectedtitlealternative ms-selectedtitlealternative";
	else
		itemTable.className="o-menu-selectedtitle ms-selectedtitle";

	itemTable.onmouseout=OSR_OutItem;
	var titleRow;
	titleRow=GetFirstChildElement(GetFirstChildElement(itemTable));
	if (titleRow !=null)
	{
		imageCell=GetLastChildElement(titleRow);
	}
	var imageTag=GetFirstChildElement(imageCell);
	imageTag.src='../images/menudark.gif'/*tpa=http://www.owens-minor.com/_layouts/images/menudark.gif*/;
	imageTag.style.visibility="visible";
	imageCell.className="o-menu-menuimagecell ms-menuimagecell";
	imageCell.onclick=OSR_CreateMenu;
	return false;
}

function OSR_OutItem()
{
	if (!IsMenuOn() && itemTable !=null)
	{
		itemTable.className="o-menu-unselectedtitle ms-selectedtitle";
		itemTable.onclick=null;
		itemTable.oncontextmenu=null;
		itemTable.onmouseout=null;
		if (imageCell !=null)
		{
			GetFirstChildElement(imageCell).style.visibility="hidden";
			imageCell.className="";
		}
		resetExecutionState();
	}
}

function OSR_CreateMenu(e)
{
	if (e==null) e=window.event;
	var srcElement=e.srcElement ? e.srcElement : e.target;
	if (itemTable==null || imageCell==null ||
		(onKeyPress==false &&
		 (srcElement.tagName=="A" ||
		  srcElement.parentNode.tagName=="A")))
		return;
	return OSR_CreateMenuEx(itemTable, e);
}

function OSR_CreateMenuEx(container, e)
{
  if (container==null) return;
	IsMenuShown=true;
	document.body.onclick="";
	var m;
	m=CMenu(currentItemID+"_menu");
	if (!m)	return;
	OSR_AddMenuItems(m);
	currentEditMenu=m;
	container.onmouseout=null;
	OMenu(m, container, null, null, -1);
	itemTable=GetSelectedElement(container, "TABLE");
	m._onDestroy=OSR_OutItem;
	e.cancelBubble=true;
	return false;
}

function OSR_AddMenuItems(m)
{
  var menuItems = document.getElementById('OSR_ActionMenuItems');
  if (menuItems == null) return;

  var actionmask = currentResultRow.getAttribute('actionmask');
      
  var maskIndex = 0;
  var itemIndex = 0;
	var menuOption;
  while (true)
  {
    menuItem = menuItems.childNodes[itemIndex++];
    if (menuItem == null) break;
    
    actionId = menuItem.getAttribute('actionId');
    actionGroupId = menuItem.getAttribute('groupId');
    if (actionId != null)
    {
      flag = actionmask.substr(maskIndex,1);
      maskIndex++;
      if (flag == '0') continue;

      actionTitle = menuItem.getAttribute('actionTitle');
      
      actionUrl = menuItem.getAttribute('actionUrl');
      if (actionUrl != null && (actionUrl.indexOf('javascript:') == -1 || actionUrl.indexOf('{') >= 0))
      {
        actionUrl = 'javascript:OSR_DoAction(\'' + actionId + '\')';
      }

      actionImageUrl = menuItem.getAttribute('actionImageUrl');
      menuOption=CAMOpt(m, actionTitle, actionUrl, actionImageUrl, null, 100);
	    menuOption.id="ID_" + actionId;      
    } else if (actionGroupId != null)
    {
    	CAMSep(m);
    }
  }
}

function OSR_ExecuteLinkAction(link)
{
  var actionParams = link.split('_');

  var actionId = actionParams[0];  
  var resultGroupName = actionParams[1];
  var resultRowId = actionParams[1] + '_' + actionParams[2];  
  var resultRow = document.getElementById(resultRowId);
  if (resultRow == null) return;

  currentResultRow = resultRow;
  currentItemID = resultRowId;
  
  var actionUrl = OSR_GetActionUrl(resultGroupName, actionId);
  if (actionUrl == null) return;
  
  if (actionUrl.indexOf('javascript:') == 0)
  {
    eval(actionUrl);
  } else
  {
    window.location = actionUrl;
  }  
}

function OSR_DoAction(actionId)
{
  if (currentResultRow != null)
  {
    var actionUrl = OSR_GetActionUrl('OSR', actionId, currentResultRow);
    if (actionUrl == null) return;
    
    if (actionUrl.indexOf('javascript:') == 0)
    {
      eval(actionUrl);
    } else
    {
      window.location = actionUrl;
    }
  }
}

function OSR_EditInApplication(bCheckOut)
{
  if (currentResultRow != null)
  {
    var path = OSR_GetResultColumn('Path');
    var siteName = OSR_GetResultColumn('SiteName');
    if (bCheckOut == null) bCheckOut = false;
    if (path != null)
    {
      editDocumentWithProgID2(path, '', 'SharePoint.OpenDocuments', bCheckOut, siteName, false);
    }
  }
}

function OSR_ViewProperties()
{  
  if (currentResultRow == null) return;
  var contentClass = OSR_GetResultColumn('ContentClass');
  var path = OSR_GetResultColumn('Path');
  if (contentClass == null || path == null) return;
  var isSupported = contentClass.indexOf('STS_ListItem_') == 0;
  if (isSupported && path.indexOf('?ID') > 0) isSupported = false;
  if (isSupported)
  {
    OSR_ActionRedirect('ViewProperties', path, contentClass);
  } else
  {
    alert('View Properties is not supported for this item.');
  }
}

function OSR_ViewDetails(target)
{  
  if (target != null && target.length > 0)
  {
    var workId = OSR_GetResultColumn('WorkId');
    if (workId != null)
    {
      var queryUrl = new QueryUrl(window.location.href);
      queryUrl.SetTarget(target);     
      queryUrl.SetParameter('w', workId);
      window.location.href = queryUrl.ToString();
    }
  }
}

function OSR_AddAlert()
{
  if (currentResultRow == null) return;
  var contentClass = OSR_GetResultColumn('ContentClass');
  var path = OSR_GetResultColumn('Path');
  if (contentClass == null || path == null) return;

  var isSupported = contentClass.indexOf('STS_List') == 0;
  if (isSupported)
  {
    OSR_ActionRedirect('AddAlert', path, contentClass);    
  } else
  {
    alert('Add Alert is not supported for this item.');
  }  
}

function OSR_ActionRedirect(action, path, contentClass)
{
  var siteName = OSR_GetResultColumn('SiteName');
  var actionUrl = siteName + '/_layouts/Ontolica/ActionRedirect.aspx?Action=' + action + '&ItemUrl=';
  actionUrl += encodeURIComponent(path);
  actionUrl += '&ContentClass=' + contentClass;
  actionUrl += '&ReturnUrl=' + encodeURIComponent(window.location.href);
  window.location = actionUrl;
}

function OSR_AddToMyLinks()
{  
  if (currentResultRow != null)
  { 
    var args = new Array();
    args[0] = OSR_GetResultColumn('Title');
    args[1] = OSR_GetResultColumn('Path');
    args[2] = (document.getElementById('ServerTemplate')) ? document.getElementById('ServerTemplate').value : '';
    var features = 'resizable=no,status=no,scrollbars=yes,menubar=no,directories=no,location=no,width=750,height=475';
    if (browseris.ie55up)
      features = 'resizable: no; status: no; scroll: yes; help: no; center: yes; dialogWidth: 750px; dialogHeight: 475px;';
    commonShowModalDialog(OSR_GetBaseUrl() + '_layouts/QuickLinksDialog.aspx', features, null, args);
  }
}

function OSR_AddToMyColleagues()
{  
  if (currentResultRow != null)
  { 
    var args = new Array();
    args[0] = 'FC10C33E-CF84-4664-B646-5CC76486D5CF';
    var features = 'resizable=no,status=no,scrollbars=yes,menubar=no,directories=no,location=no,width=750,height=475';
    if (browseris.ie55up)
      features = 'resizable: no; status: no; scroll: yes; help: no; center: yes; dialogWidth: 750px; dialogHeight: 475px;';
    commonShowModalDialog(OSR_GetBaseUrl() + '_layouts/QuickLinksDialog.aspx?Mode=Person', features, null, args);
  }
}

function OSR_GetBaseUrl()
{
  var baseUrl = null;
  var resultGroupNameEndIndex = currentItemID.indexOf('_');
  var resultGroupName = currentItemID.substr(0, resultGroupNameEndIndex);
  var resultLayoutTable = document.getElementById(resultGroupName);
  if (resultLayoutTable != null)
  {
    baseUrl = resultLayoutTable.getAttribute('baseUrl');
  }
  if (baseUrl == null) return '/';
  return baseUrl;
}

function OSR_GetResultColumn(name, rowNum)
{
  if (rowNum)
  {
    var row = document.getElementById('OSR_' + rowNum);
    if (row)
      return row.getAttribute('COL_' + name);
    return null;
  } else
  {
    return currentResultRow.getAttribute('COL_' + name);
  }
}

function OSR_GetActionUrl(resultGroupName, actionId)
{
  var actionUrlTemplate = null;
  var actionDefinitionsDivId = resultGroupName + '_ActionMenuItems';
  var menuItems = document.getElementById(actionDefinitionsDivId);
  if (menuItems == null) return;

  var itemIndex = 0;
  while (true)
  {
    menuItem = menuItems.childNodes[itemIndex++];
    if (menuItem == null) break;
    
    candidateActionId = menuItem.getAttribute('actionId');
    if (candidateActionId == null) continue;
    if (candidateActionId != actionId) continue;
    
    actionUrlTemplate = menuItem.getAttribute('actionUrl');    
    break;
  }
  
  if (actionUrlTemplate == null) return null;
  
  /* Replace parameters with result column values. */
  var param = null;
  var actionUrl = '';
  for (urlIdx = 0; urlIdx < actionUrlTemplate.length; urlIdx++)
  {
    ch = actionUrlTemplate.charAt(urlIdx);
    if (param == null && ch == '{')
    {
      param = '';
      continue;
    }
    
    if (param != null && ch == '}')
    {
      attrName = 'COL_' + param;
      value = currentResultRow.getAttribute(attrName);
      if (value != null)
      {
        actionUrl += value;
      }
      param = null;
      continue;
    }
    
    if (param != null)
      param += ch;
    else
      actionUrl += ch;
  }
  
  return actionUrl;
}

function OSR_WireLinks(url, action, env, start)
{
  var links=document.body.getElementsByTagName('a');
  OPL_AddHandler('window.onunload',function(){if(!QLC)OSR_SendSoap(url,action,env);});
  for (var i=0;i<links.length;i++)
  {
    if (/^(OSR|OBB|OHC)_/.exec(links[i].id)) 
    {
      if (links[i].onclick != null)
      {
        var methodStr = links[i].onclick.toString();
        var beginIndex = methodStr.indexOf('{');
        var endIndex = methodStr.indexOf('}');
        var methodStr = methodStr.substr(beginIndex+1, endIndex-beginIndex-1);
        links[i].onclick = null;
        links[i].onclick = function(){OSR_SendClick(url,action,env,start,this);QLC=1;eval(methodStr)};        
      } else
      {
        links[i].onclick = function(){OSR_SendClick(url,action,env,start,this);QLC=1;};
      }
    }
  }
}
function OSR_SendClick(postUrl, soapAction, env, startPos, elem) 
{    
  var id = elem.id;
  var relPosStr = /\d+$/.exec(id);
  var relPos = (relPosStr ? parseInt(relPosStr[0],10) : 0)
  var clickUrl = null;
  var bestBet = null;
  var nonClickedXml = null;

  var cont = /^(OSR_T|OSR_U)/.exec(id);
  if (cont)
  {
    clickUrl = OSR_GetResultColumn('Path');  
  }

  if (/^OBB_/.exec(id))
  {
    var bbelem = document.getElementById('OBB_T_'+relPosStr);
    if (bbelem) bestBet = bbelem.innerHTML;
  } else if (relPos > 1 && /^OSR_/.exec(id))
  {
    nonClickedXml = '';
    for (var i = 1; i < relPos && i < 10; i++)
    {
      var nonClickedElem = document.getElementById('OSR_T_'+i);
      if (nonClickedElem)
      {
        nonClickedXml += '<NoClick>'+OSR_GetResultColumn('Path', i)+'</NoClick>';
      }
    }
  }
  
  OSR_SendSoap(postUrl, soapAction, env, clickUrl, relPos + startPos, bestBet, cont, nonClickedXml);
  
  /*var cont = /^(CSR_RV|CSR_MRL|SRP_)/.exec(id);
  var bestBet = null;
  var nonClickedXml = null;

  if (/^OBB_/.exec(id))
  {
      var bbelem = document.getElementById('OBB_T_'+relPosStr);
      if (bbelem) bestBet = bbelem.innerHTML;
  }
  else if (relPos > 1 && startPos == 0 && /^OSR_/.exec(id))
  {
      nonClickedXml = '';
      for (var i = 1; i < relPos && i < 10; i++)
      {
          var nonClickedElem = document.getElementById('OSR_T_'+i);
          if (nonClickedElem)
          {
              nonClickedXml += '<z>'+nonClickedElem.href+'</z>';
          }
      }
  }
  
  OSR_SendSoap(postUrl, soapAction, env, (cont ? null : elem.href), relPos + startPos, bestBet, cont, nonClickedXml);*/
}

function OSR_SendSoap(postUrl, soapAction, env, clickUrl, pos, bestBet, cont, nonClickedXml)
{
  var req = (window.XMLHttpRequest) ? (new XMLHttpRequest())
          : (window.ActiveXObject) ? (new ActiveXObject('Msxml2.XMLHTTP')) : null;
  if (req)
  {
      req.open('POST', postUrl, true);
      req.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
      req.setRequestHeader('SOAPAction', soapAction);
      if (clickUrl) env = env.replace("</LogEntry>", '<Click>' + OSR_XmlEscape(clickUrl) + '</Click>' + "</LogEntry>");
      if (pos) env = env.replace('<Rank>0','<Rank>'+pos);
      if (cont) env = env.replace('<f>false','<f>true');
      if (bestBet) env = env.replace("</i>", '<y>' + OSR_XmlEscape(bestBet) + '</y>' + "</i>");
      if (nonClickedXml) env = env.replace("</LogEntry>", nonClickedXml + "</LogEntry>");
      req.send(env);
  }
}

function OSR_XmlEscape(text)
{
  return (text)?text.replace('&','&amp;').replace('>', '&gt;').replace('<','&lt;'):'';
}


/*************************************************************/
/* Ontolica Search Hint (OSH) related javascript methods     */
/*************************************************************/

function OSH_Search(queryStr)
{   
  var queryUrl = new QueryUrl(window.location.href);
  queryUrl.SetParameter('start1', null);
  queryUrl.SetParameter('w', null);
  queryUrl.SetParameter('f', null);    
  queryUrl.SetParameter('s', null);
  queryUrl.SetParameter('k', queryStr);
  window.location.href = queryUrl.ToString();
}


/*************************************************/
/* QueryUrl object                               */
/*************************************************/

function QueryUrl(href)
{
  anchorIndex = href.indexOf('#');
  if (anchorIndex > 0)
  {
    href = href.substr(0, anchorIndex);
  }

  queryIndex = href.indexOf('?');
  if (queryIndex == -1)
  {
    this.target = href;
    this.queryParams = null;
  } else
  {
    this.target = href.substr(0, queryIndex);
    this.queryParams = href.substr(queryIndex+1).split('&');
  }
      
  this.SetTarget = QueryUrl_SetTarget;
  this.GetParameter = QueryUrl_GetParameter;
  this.SetParameter = QueryUrl_SetParameter;
  this.ToString = QueryUrl_ToString;
}

function QueryUrl_SetTarget(target)
{
  this.target = target;
}

function QueryUrl_GetParameter(name)
{
  if (this.queryParams != null)
  {
    i = 0;
    while (i < this.queryParams.length)
    {
      param = this.queryParams[i];
      if (param != null && param.indexOf(name) == 0) 
      {
        var keyValuePair = this.queryParams[i].split('=');
        return decodeURIComponent(keyValuePair[1]);
      }
      i++;
    }
  }  
  return null;
}

function QueryUrl_SetParameter(name, value)
{
  if (this.queryParams == null)
  {
    if (value != null)
    {
      this.queryParams = new Array();
      this.queryParams[0] = name + '=' + encodeURIComponent(value);      
    }
  } else
  {
    i = 0;
    while (i < this.queryParams.length)
    {
      param = this.queryParams[i];
      if (param != null && param.indexOf(name) == 0) break;
      i++;
    }
    
    if (value != null)
      this.queryParams[i] = name + '=' + encodeURIComponent(value);
    else
      this.queryParams[i] = null;
  }
}

function QueryUrl_ToString()
{
  url = this.target;
  
  if (this.queryParams != null)
  {    
    first = true;
    for (i=0; i<this.queryParams.length; i++)
    {
      param = this.queryParams[i];      
      if (param == null) continue;
      if (param.indexOf('=') == (param.length-1)) continue;
         
      if (first)
      { 
        url += '?';
        first = false;
      } else
      {
        url += '&';      
      }
      url += param;
    }
  }
  
  return url;
}


/*************************************************/
/* QueryExpression object                        */
/*************************************************/

function QueryExpression()
{
  this.keywordExpr = '';
  this.propertyExpr = '';
  this.AddAllKeywords = QueryExpression_AddAllKeywords;
  this.AddKeywordPhrase = QueryExpression_AddKeywordPhrase;
  this.AddAnyKeywords = QueryExpression_AddAnyKeywords;
  this.AddNoKeywords = QueryExpression_AddNoKeywords;
  this.AddProximityKeywords = QueryExpression_AddProximityKeywords;
  this.AddProperty = QueryExpression_AddProperty;
  this.GetExpressionString = QueryExpression_GetExpressionString;
  this.AddKeywordOperator = QueryExpression_AddKeywordOperator;
  this.AddBooleanOperator = QueryExpression_AddBooleanOperator;
  this.AddQuickFilter = QueryExpression_AddQuickFilter;
  this.Split = QueryExpression_Split;
}

function QueryExpression_AddAllKeywords(keywords)
{
  if (this.keywordExpr != '') this.keywordExpr += ' ';

  var keywordsArray = keywords.split(" ");
  for (keywordIdx=0; keywordIdx<keywordsArray.length; keywordIdx++)
  {
    if (keywordIdx>0) this.keywordExpr += ' ';
    this.keywordExpr += keywordsArray[keywordIdx];
  }  
}

function QueryExpression_AddKeywordPhrase(keywords)
{
  this.AddKeywordOperator('AND');
  this.keywordExpr += '\'';
  this.keywordExpr += keywords;
  this.keywordExpr += '\'';
}

function QueryExpression_AddAnyKeywords(keywords)
{
  this.AddKeywordOperator('AND');
  var keywordsArray = this.Split(keywords);
        
  if (keywordsArray.length > 1) this.keywordExpr += '(';
  for (keywordIdx=0; keywordIdx<keywordsArray.length; keywordIdx++)
  {
    if (keywordIdx>0) this.keywordExpr += ' OR ';
    this.keywordExpr += keywordsArray[keywordIdx];
  }
  if (keywordsArray.length > 1) this.keywordExpr += ')';
}

function QueryExpression_AddNoKeywords(keywords)
{
  this.AddKeywordOperator('NOT');
  this.keywordExpr += '(';
  this.keywordExpr += keywords;
  this.keywordExpr += ')';
}

function QueryExpression_AddProximityKeywords(keywords)
{
  this.AddKeywordOperator('AND');
  var keywordsArray = this.Split(keywords);
  if (keywordsArray.length > 1) this.keywordExpr += '(';
  for (keywordIdx=0; keywordIdx<keywordsArray.length; keywordIdx++)
  {
    if (keywordIdx>0) this.keywordExpr += ' NEAR ';
    this.keywordExpr += keywordsArray[keywordIdx];
  }
  if (keywordsArray.length > 1) this.keywordExpr += ')';
}

function QueryExpression_AddProperty(booleanOperator, propertyName, propertyOperator, propertyValue)
{
  if (propertyName == null || propertyName == '') return;
  if (propertyOperator == null || propertyOperator == '') return;
  if (propertyValue == null || propertyValue == '') return;

  var beginIndex = this.propertyExpr.length;
  while (beginIndex > 0)
  {
    beginIndex--;
    if (this.propertyExpr[beginIndex-1] != ')') 
    {
      beginIndex++;
      if (beginIndex >= this.propertyExpr.length) beginIndex = 0
      break;
    }
  }  
  var closingBrackets = ')';
  if (beginIndex > 0) 
  {
    this.propertyExpr = this.propertyExpr.substr(0, beginIndex);
    closingBrackets += this.propertyExpr.substr(beginIndex);
  }

  this.AddBooleanOperator(booleanOperator);
  this.propertyExpr += '(';
        
  if (propertyName.indexOf(' ') >= 0)
  {
    this.propertyExpr += '\'';
    this.propertyExpr += propertyName;
    this.propertyExpr += '\'';    
  } else
  {
    this.propertyExpr += propertyName;
  }
  
  this.propertyExpr += propertyOperator;

  if (propertyValue.indexOf('(') >= 0 || propertyValue.indexOf(')') >= 0)
  {
    this.propertyExpr += '\'';
    this.propertyExpr += propertyValue;
    this.propertyExpr += '\'';      
  } else if (propertyValue.indexOf(' ') >= 0)
  {
    this.propertyExpr += '\'';
    this.propertyExpr += propertyValue;
    this.propertyExpr += '\'';    
  } else
  {
    this.propertyExpr += propertyValue;
  }

  this.propertyExpr += closingBrackets;    
}

function QueryExpression_AddQuickFilter(quickFilterExpr)
{
  this.AddBooleanOperator('AND');
  this.propertyExpr += quickFilterExpr;
}

function QueryExpression_GetExpressionString()
{
  var exprStr = this.keywordExpr;
  if (this.propertyExpr != '')
  {
    exprStr += ' ';
    exprStr += this.propertyExpr;
  }
    
  return exprStr;
}

function QueryExpression_AddKeywordOperator(operator)
{
  if (this.keywordExpr != '')
  {
    this.keywordExpr += ' ' + operator + ' ';
  }
}

function QueryExpression_AddBooleanOperator(operator)
{
  if (this.propertyExpr != '')
  {
    this.propertyExpr += ' ' + operator + ' ';
  }
}

function QueryExpression_Split(keywords)
{
  var keywordsArray = keywords.split(" ");
  var adjustedArray = new Array();
      
  var phrase = null;
  for (keywordIdx=0; keywordIdx<keywordsArray.length; keywordIdx++)
  {
    var keyword = keywordsArray[keywordIdx];
    
    if (phrase == null)
    {
      if (keyword.indexOf('\'') == 0 || keyword.indexOf('\"') == 0)
      {
        phrase = keyword;
      } else
      {
        adjustedArray[adjustedArray.length] = keyword;
      }
    } 
    
    else
    {
      phrase += ' ' + keyword;
      var lastIndex = keyword.length-1;
      if (keyword.indexOf('\'') == lastIndex || keyword.indexOf('\"') == lastIndex)
      {
        adjustedArray[adjustedArray.length] = phrase;      
        phrase = null;
      }
    }
  }
  
  return adjustedArray;
}

function OSS_ShowSendMailDialog(path) {
    commonShowModalDialog('/_layouts/Ontolica/SendMail.aspx?attachmentPath=' + path, 'dialogHeight: 400px; dialogWidth: 400px; center: Yes; resizable: No; status: No;', null, null)
}