function IsDefined(variable)
{
  return (typeof(window[variable]) == "undefined") ? false : true;
}

function OpenWindow( url, name, width, height, positionMode )
{
  var windowSize = 'width=' + width + ',height=' + height;
  parentWidth = screen.availWidth;
  parentHeight = screen.availHeight;
  if( positionMode == 'Center' )
  {
    posX = (parentWidth - width) / 2;
    posY = (parentHeight - height) / 2;
    windowSize = 'width=' + width + ',height=' + height + ',left=' + posX + ',top=' + posY;
  }  
  newWindow = window.open( url, name, windowSize );
  newWindow.focus();
  return newWindow;
}

function OpenDialogNew( url, name, width, height, callback )
{
  var args = new Array();
  args[0] = name;
  args[1] = '';
  args[2] = '';
  var features = 'resizable=no,status=no,scrollbars=yes,menubar=no,directories=no,location=no,width=' + width + ',height=' + height;
  if(browseris.ie55up)
  {
    features = 'resizable: no; status: no; scroll: yes; help: no; center: yes; dialogWidth: ' + width + 'px; dialogHeight: ' + height + 'px;';
  }
  commonShowModalDialog( url, features, callback, args );
}

function OpenDialog( url, name, width, height, positionMode )
{
  var windowSize = 'width=' + width + ',height=' + height;
  parentWidth = screen.availWidth;
  parentHeight = screen.availHeight;
  if( positionMode == 'Center' )
  {
    posX = (parentWidth - width) / 2;
    posY = (parentHeight - height) / 2;
    windowSize = 'width=' + width + ',height=' + height + ',left=' + posX + ',top=' + posY;
  }  
  newWindow = window.open( url, name, windowSize );
  newWindow.focus();
  return newWindow;
}

function OPL_AddHandler(e, f)
{
  var o=eval(e);
  eval(e+'=o?function(){o();f();}:f;');
}




