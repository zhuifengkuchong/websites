/**
* hoverIntent r6 // 2011.02.26 // jQuery 1.5.1+
* <http://cherne.net/brian/resources/jquery.hoverIntent.html>
* 
* @param  f  onMouseOver function || An object with configuration options
* @param  g  onMouseOut function  || Nothing (use configuration options object)
* @author    Brian Cherne brian(at)cherne(dot)net
* 
* This has been radically modified by Matthew Schlanger for NTT based on changes for McKesson
* 
*/

(function($){
	
	var menuNameList;
	
	function menuOver(){
		
		var dropDown = $('.menu-fullwidth',this);
	
		if(hoverIntentEffect === 'hover_fade'){
			$(dropDown).fadeIn(hoverIntentShow, hoverIntentEasing);
				$(this).hover(function() {
					$(dropDown).stop(true,true).fadeOut(hoverIntentHide, hoverIntentEasing);
				});
		}
		else if(hoverIntentEffect === 'hover_slide'){
			$(dropDown).slideDown(hoverIntentShow, hoverIntentEasing);
				$(this).hover(function() {
					$(dropDown).stop(true,true).slideUp(hoverIntentHide, hoverIntentEasing);
				});
		}
		else if(hoverIntentEffect === 'click_fade'){
			$(this).click(function() {
				$(dropDown).stop(true,true).fadeIn(hoverIntentShow, hoverIntentEasing);
				$(this).hover(function() {
					$(dropDown).stop(true,true).fadeOut(hoverIntentHide, hoverIntentEasing);
				});
			});
		}
		else if(hoverIntentEffect === 'click_slide'){
			$(this).click(function(){
				$(dropDown).slideDown(hoverIntentShow, hoverIntentEasing); 
				$(this).hover(function() {
					$(dropDown).stop(true,true).slideUp(hoverIntentHide, hoverIntentEasing);
				});
			});
		}
	}
	
	function getURLArgument(agrNum){
	  var urlstring = window.location.pathname;
	  var vars = urlstring.split("/");
	  return vars[agrNum];
	};
	
	function menuOut(){
		var dropDown = $('.menu-fullwidth',this);
		$(dropDown).hide();
		
	}
	
	//I don't konw what this is for yet.
	function menuTrigger(){
		
		var menuBar = $('.menu-fixed');
		
		$(menuBar).after('<a id="menu-trigger" href="#"></a>');
		
		if( menuBarHide === 1 ) {
			$(menuBar).hide(0);
			$('#menu-trigger').addClass("active");
		}
		
		$('#menu-trigger').click(function() {
			
			$(menuBar).slideToggle(300);
			$(this).addClass("active");
			return false;
			
		});
		
	}
	
	function setCurrent(){
		//used when closing the menu
		var locationString = getURLArgument(1);
		var index = jQuery.inArray( locationString, menuNameList);
		if (index !== -1 && menuNameList[index] !== ""){
			$('.menu').children('li').eq(index).addClass('current');
		}	
	}
	
	function menuClickOutside(){	
		$(document).click(function(){
			$('.menu-fullwidth').slideUp('fast');
			$('.menu').children('li').removeClass('active');
			$('.menu').children('li').removeClass('current');
			setCurrent();	
		});
		//$('.menu').click(function(event){
		//	event.stopPropagation();
		//});
	}

	$.fn.menuReloaded = function(options){

		var options = $.extend({
			menu_speed_show : 250, // Time (in milliseconds) to show a drop down
			menu_speed_hide : 250, // Time (in milliseconds) to hide a drop down
			menu_speed_delay : 100, // Time (in milliseconds) before showing a drop down
			menu_effect : 'open_close_slide', // Drop down effect, choose between 'hover_fade', 'hover_slide', 'click_fade', 'click_slide', 'open_close_fade', 'open_close_slide'
			menu_easing : 'jswing', // Easing Effect : 'easeInQuad', 'easeInElastic', etc.
			menu_click_outside : 1, // Clicks outside the drop down close it (1 = true, 0 = false)
			menu_show_onload : 0, // Drop down to show on page load (type the number of the drop down, 0 for none)
			menubar_trigger : 0, // Show the menu trigger (button to show / hide the menu bar), only for the fixed version of the menu (1 = show, 0 = hide)
			menubar_hide : 0 // Hides the menu bar on load (1 = hide, 0 = show)
		}, options);

			//MegaMenu close button
		$(".close-menu a").click(function(event) {
			$(".menu-fullwidth").slideUp(180);
			$(".menu-fullwidth").each(function(){
				if ($(this).parent().hasClass('current')){
					$(this).parent().removeClass('enabled');
					$(this).slideUp('fast',function(){
						$(this).parent().removeClass('active');
						$(this).parent().removeClass('current');
						setCurrent();
					});
					event.preventDefault();
				}
			});
			var delayEnable = window.setTimeout(function(){$('.menu').children('li').addClass('enabled');},500); 
		});
	
		return this.each(function() {
			
			var	menu = $(this),
				menuItem = $(menu).children('li'),
				menuItemSpan = $(menuItem).children('span'),
				menuDropDown = $(menuItem).children('.menu-fullwidth');
				menuDropDownScroller = $('.menu-scroller-container');
			
			//create an array of valid strings used to compare url first arguments against
			menuNameList = [];
			$(menuItem).each(function(){	
				var menuString = $('> span > a', this).text(); //not really needed as .text() versus .html will get text inside <a>
				if(menuString === undefined || menuString === ""){
					menuString = $('> span', this).text();
				}
				if (menuString !== undefined && menuString !== ""){
					menuString = menuString.toLowerCase().replace(/ /g,'-').replace(/\s/g, ''); //lower case, space to -, and remove white space
					if(menuString.charAt(menuString.length -1) === '-'){
						menuString = menuString.slice(0,menuString.length - 1);
					}
				}
				menuNameList.push(menuString);
			});
			//and make li's not selectable
			$(menuItem).each(function(){	
				if (!$(this).hasClass("search")){ //but not the search field
					$(this).attr('unselectable', 'on').css('user-select', 'none').on('selectstart', false); //make the li's non-selectable
					$(this).addClass('enabled');
				}
			});
			
			if ((menuDropDownScroller.length > 0)) {
				$(menuDropDownScroller).nanoScroller();                    
            }   
			
			$(menuDropDown).css('left', '0').hide();
			
			if( options.menubar_trigger === 1 ) {
				menuBarHide = options.menubar_hide;
				menuTrigger();
			}
			if(options.menu_click_outside === 1){
				menuClickOutside();
			}
	
			if (Modernizr.touch){
				
				$(menuItem).children('span').bind('touchstart', function() {
					
					var $this = $(this);
					if ($this.parent('li').hasClass('search')) return false;
					//$this.parent('li').siblings().removeClass('active').end().addClass('active');
					$this.parent('li').siblings().removeClass('current'); /*as per 12/20 document*/
					if ($this.parent('li').hasClass('current')){ //matt added removing and adding the class since using a toggle.
						$this.parent('li').removeClass('current');
						setCurrent();
					}else{
						if (!$this.parent('li').hasClass('search')){
							$this.parent('li').addClass('current');
						}
					}
					$this.parent('li').siblings().find(menuDropDown).stop(true,true).slideUp(options.menu_speed_hide);
					$this.parent('li').find(menuDropDown)
						.delay(options.menu_speed_delay)
						.slideToggle(options.menu_speed_show)
						.click(function(event){
							event.stopPropagation();
						});
					
				});
					
			}
			else if (options.menu_effect === 'hover_fade' || options.menu_effect === 'hover_slide' || options.menu_effect === 'click_fade' || options.menu_effect === 'click_slide'){
				
				hoverIntentEffect = options.menu_effect;
				hoverIntentEasing = options.menu_easing;
				hoverIntentShow = options.menu_speed_show;
				hoverIntentHide = options.menu_speed_hide;
				// HoverIntent Configuration
				var hoverIntentConfig = {
					sensitivity: 2, // number = sensitivity threshold (must be 1 or higher)
					interval: 100, // number = milliseconds for onMouseOver polling interval
					over: menuOver, // function = onMouseOver callback (REQUIRED)
					timeout: 200, // number = milliseconds delay before onMouseOut
					out: menuOut // function = onMouseOut callback (REQUIRED)
				};
				
				$(menuItem).hoverIntent(hoverIntentConfig);
	
			}
			else if (options.menu_effect == 'open_close_fade'){
	
				$(menuItem + ':nth-child(' + options.menu_show_onload + ')')
					.children('.menu-fullwidth').show()
					.parent(menuItem).addClass('active');
				$(menuItem).unbind('mouseenter mouseleave');
	
				$(menuItemSpan).click(function() {
					
					var $this = $(this);
					if ($this.parent('li').hasClass('search')) return false;
					$this.parent('li').siblings().removeClass('active').end().addClass('active');
					$this.parent('li').siblings().removeClass('current'); /*as per 12/20 document*/
					if ($this.parent('li').hasClass('current')){ //matt added removing and adding the class since using a toggle.
						$this.parent('li').removeClass('current');
						setCurrent();
					}else{
						if (!$this.parent('li').hasClass('search')){
							$this.parent('li').addClass('current');
						}
					}
					$this.parent('li').siblings().find(menuDropDown).stop(true,true).fadeOut(options.menu_speed_hide, options.menu_easing);
					$this.parent('li').find(menuDropDown)
						.delay(options.menu_speed_delay)
						.fadeToggle(options.menu_speed_show, options.menu_easing)
						.click(function(event){
							event.stopPropagation();
						});
							
				});
	
			}
			else if (options.menu_effect === 'open_close_slide') {
	
				$(menuItem + ':nth-child(' + options.menu_show_onload + ')')
					.children('.menu-fullwidth').show()
					.parent(menuItem).addClass('active');
				$(menuItem).unbind('mouseenter mouseleave');
				$(menuItemSpan).unbind('click');

				var clickCount = 0; 
                $(menuItemSpan).click(function (event) {
					
					var $this = $(this);
					//$this.parent('li').siblings().removeClass('active').end().addClass('active');
					/*as per 12/20 document*/
					/*$this.parent('li').siblings().removeClass('current'); 
					if ($this.parent('li').hasClass('current')){ //matt added removing and adding the class since using a toggle.
						$this.parent('li').removeClass('current');
						setCurrent();
					}else{
						$this.parent('li').addClass('current');
					}
					$this.parent('li').siblings().find(menuDropDown).stop(true,true).slideUp(options.menu_speed_hide, options.menu_easing);
					$this.parent('li').find(menuDropDown)
						.delay(options.menu_speed_delay)
						.slideToggle(options.menu_speed_show, options.menu_easing)
						.click(function(event){
							event.stopPropagation();
						});
					}*/
					/*re-written as per 12/20 document*/
					if ($this.parent('li').hasClass('search')) return false;
					if ($this.parent('li').hasClass('current') && $this.parent('li').hasClass('active')){ 
						//case of closeing an open menu
						$this.parent('li').removeClass('current');
						$this.parent('li').removeClass('active');
						$this.parent('li').find(menuDropDown)
										.delay(options.menu_speed_delay)
										.slideUp(options.menu_speed_show, options.menu_easing)
                                        .click(function (event) { event.stopPropagation(); })
						setCurrent();
						clickCount++;
					}else{
						//case of opening a closed menu
						if (!$this.parent('li').hasClass('search')){
							$this.parent('li').addClass('current');
							//$this.parent('li').addClass('active'); //changed to next line 1/6/2015
							$this.parent('li').siblings().removeClass('active').end().addClass('active');
						}
						var nothingOpened = true;
						$this.parent('li').siblings().each(function(){
							if ($(this).hasClass('current')){
								$(this).removeClass('current');
								$(this).find(menuDropDown).stop(true,true).slideUp({duration:options.menu_speed_hide/2, ease:options.menu_easing, complete:function(){
									nothingOpened = false;
									$this.parent('li').find(menuDropDown)
										.delay(options.menu_speed_delay )
										.slideDown(options.menu_speed_show, options.menu_easing)
										.click(function(event){ event.stopPropagation(); });
								}});
							}
						});
						if (nothingOpened){
							//case of no other menus were open
							$this.parent('li').find(menuDropDown)
										.delay(options.menu_speed_delay)
										.slideDown(options.menu_speed_show, options.menu_easing)
										.click(function(event){
											event.stopPropagation();
										});
						}
						
						clickCount++;
					}

					event.stopPropagation();
					event.preventDefault();
				});					
			}	
		}); // End each	
	};
	$('.menu-tabs-nav li a').click(function(e) {
		e.preventDefault();
	});
   	$.fn.menuTabs = function(el) {
        
   	    return this.each(function() {

			var	menuTabs = $(this);
				menuTabsNav = menuTabs.find(".menu-tabs-nav");
                
   	        menuTabsNav.on("click", "li > a", function() { //change to click if second level to click was mouseover
            
   	            var menuTabsLinkCurrent = menuTabs.find("a.current").attr("href").substring(1),
                
   	                $menuTabsLink = $(this),
   	                menuTabsLinkID = $menuTabsLink.attr("href").substring(1);

   	            if ((menuTabsLinkID !== menuTabsLinkCurrent) && ( menuTabs.find(":animated").length == 0)) {
                
   	            	//jQuery.fx.off = true; //what is the intent of this line? commented by MS to fix bug.
										
					menuTabs.find(".menu-tabs-nav li a").stop(true, true).removeClass("current");
					$menuTabsLink.stop(true,true).addClass("current");

   	                menuTabs.find("#"+menuTabsLinkCurrent).stop(true,true).fadeOut(200, function() {

   	                    menuTabs.find("#"+menuTabsLinkID).stop(true,true).fadeIn(300);
   	                    var newHeight = menuTabs.find("#"+menuTabsLinkID).height();

   	                });
                    
                }   
                
   	            return false;
				
            });
			
		});
            
    };

	
	
})(jQuery);

