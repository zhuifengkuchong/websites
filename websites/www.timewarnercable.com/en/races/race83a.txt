<script id = "race83a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race83={};
	myVars.races.race83.varName="Object[2544].uuid";
	myVars.races.race83.varType="@varType@";
	myVars.races.race83.repairType = "@RepairType";
	myVars.races.race83.event1={};
	myVars.races.race83.event2={};
	myVars.races.race83.event1.id = "disabled";
	myVars.races.race83.event1.type = "onblur";
	myVars.races.race83.event1.loc = "disabled_LOC";
	myVars.races.race83.event1.isRead = "False";
	myVars.races.race83.event1.eventType = "@event1EventType@";
	myVars.races.race83.event2.id = "modalLink";
	myVars.races.race83.event2.type = "onblur";
	myVars.races.race83.event2.loc = "modalLink_LOC";
	myVars.races.race83.event2.isRead = "True";
	myVars.races.race83.event2.eventType = "@event2EventType@";
	myVars.races.race83.event1.executed= false;// true to disable, false to enable
	myVars.races.race83.event2.executed= false;// true to disable, false to enable
</script>

