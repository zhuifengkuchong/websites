<script id = "race57b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race57={};
	myVars.races.race57.varName="Object[2544].uuid";
	myVars.races.race57.varType="@varType@";
	myVars.races.race57.repairType = "@RepairType";
	myVars.races.race57.event1={};
	myVars.races.race57.event2={};
	myVars.races.race57.event1.id = "Lu_Id_button_3";
	myVars.races.race57.event1.type = "onchange";
	myVars.races.race57.event1.loc = "Lu_Id_button_3_LOC";
	myVars.races.race57.event1.isRead = "True";
	myVars.races.race57.event1.eventType = "@event1EventType@";
	myVars.races.race57.event2.id = "Lu_Id_button_4";
	myVars.races.race57.event2.type = "onchange";
	myVars.races.race57.event2.loc = "Lu_Id_button_4_LOC";
	myVars.races.race57.event2.isRead = "False";
	myVars.races.race57.event2.eventType = "@event2EventType@";
	myVars.races.race57.event1.executed= false;// true to disable, false to enable
	myVars.races.race57.event2.executed= false;// true to disable, false to enable
</script>

