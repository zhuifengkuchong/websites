<script id = "race36a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race36={};
	myVars.races.race36.varName="Lu_Id_button_2__onblur";
	myVars.races.race36.varType="@varType@";
	myVars.races.race36.repairType = "@RepairType";
	myVars.races.race36.event1={};
	myVars.races.race36.event2={};
	myVars.races.race36.event1.id = "Lu_DOM";
	myVars.races.race36.event1.type = "onDOMContentLoaded";
	myVars.races.race36.event1.loc = "Lu_DOM_LOC";
	myVars.races.race36.event1.isRead = "False";
	myVars.races.race36.event1.eventType = "@event1EventType@";
	myVars.races.race36.event2.id = "Lu_Id_button_2";
	myVars.races.race36.event2.type = "onblur";
	myVars.races.race36.event2.loc = "Lu_Id_button_2_LOC";
	myVars.races.race36.event2.isRead = "True";
	myVars.races.race36.event2.eventType = "@event2EventType@";
	myVars.races.race36.event1.executed= false;// true to disable, false to enable
	myVars.races.race36.event2.executed= false;// true to disable, false to enable
</script>

