<script id = "race82b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race82={};
	myVars.races.race82.varName="Object[2544].uuid";
	myVars.races.race82.varType="@varType@";
	myVars.races.race82.repairType = "@RepairType";
	myVars.races.race82.event1={};
	myVars.races.race82.event2={};
	myVars.races.race82.event1.id = "disabled";
	myVars.races.race82.event1.type = "onblur";
	myVars.races.race82.event1.loc = "disabled_LOC";
	myVars.races.race82.event1.isRead = "True";
	myVars.races.race82.event1.eventType = "@event1EventType@";
	myVars.races.race82.event2.id = "Lu_Id_button_1";
	myVars.races.race82.event2.type = "onblur";
	myVars.races.race82.event2.loc = "Lu_Id_button_1_LOC";
	myVars.races.race82.event2.isRead = "False";
	myVars.races.race82.event2.eventType = "@event2EventType@";
	myVars.races.race82.event1.executed= false;// true to disable, false to enable
	myVars.races.race82.event2.executed= false;// true to disable, false to enable
</script>

