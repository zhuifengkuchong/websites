<script id = "race73a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race73={};
	myVars.races.race73.varName="Object[2544].uuid";
	myVars.races.race73.varType="@varType@";
	myVars.races.race73.repairType = "@RepairType";
	myVars.races.race73.event1={};
	myVars.races.race73.event2={};
	myVars.races.race73.event1.id = "modalLink";
	myVars.races.race73.event1.type = "onfocus";
	myVars.races.race73.event1.loc = "modalLink_LOC";
	myVars.races.race73.event1.isRead = "False";
	myVars.races.race73.event1.eventType = "@event1EventType@";
	myVars.races.race73.event2.id = "timerlength";
	myVars.races.race73.event2.type = "onfocus";
	myVars.races.race73.event2.loc = "timerlength_LOC";
	myVars.races.race73.event2.isRead = "True";
	myVars.races.race73.event2.eventType = "@event2EventType@";
	myVars.races.race73.event1.executed= false;// true to disable, false to enable
	myVars.races.race73.event2.executed= false;// true to disable, false to enable
</script>

