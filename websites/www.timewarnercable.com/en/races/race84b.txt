<script id = "race84b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race84={};
	myVars.races.race84.varName="Object[2544].uuid";
	myVars.races.race84.varType="@varType@";
	myVars.races.race84.repairType = "@RepairType";
	myVars.races.race84.event1={};
	myVars.races.race84.event2={};
	myVars.races.race84.event1.id = "timerlength";
	myVars.races.race84.event1.type = "onblur";
	myVars.races.race84.event1.loc = "timerlength_LOC";
	myVars.races.race84.event1.isRead = "True";
	myVars.races.race84.event1.eventType = "@event1EventType@";
	myVars.races.race84.event2.id = "modalLink";
	myVars.races.race84.event2.type = "onblur";
	myVars.races.race84.event2.loc = "modalLink_LOC";
	myVars.races.race84.event2.isRead = "False";
	myVars.races.race84.event2.eventType = "@event2EventType@";
	myVars.races.race84.event1.executed= false;// true to disable, false to enable
	myVars.races.race84.event2.executed= false;// true to disable, false to enable
</script>

