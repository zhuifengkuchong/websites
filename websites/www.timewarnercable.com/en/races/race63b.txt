<script id = "race63b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race63={};
	myVars.races.race63.varName="Object[2544].uuid";
	myVars.races.race63.varType="@varType@";
	myVars.races.race63.repairType = "@RepairType";
	myVars.races.race63.event1={};
	myVars.races.race63.event2={};
	myVars.races.race63.event1.id = "Lu_Id_input_4";
	myVars.races.race63.event1.type = "onchange";
	myVars.races.race63.event1.loc = "Lu_Id_input_4_LOC";
	myVars.races.race63.event1.isRead = "True";
	myVars.races.race63.event1.eventType = "@event1EventType@";
	myVars.races.race63.event2.id = "timerlength";
	myVars.races.race63.event2.type = "onchange";
	myVars.races.race63.event2.loc = "timerlength_LOC";
	myVars.races.race63.event2.isRead = "False";
	myVars.races.race63.event2.eventType = "@event2EventType@";
	myVars.races.race63.event1.executed= false;// true to disable, false to enable
	myVars.races.race63.event2.executed= false;// true to disable, false to enable
</script>

