$(document).ready(function () {
/*Start Alternating table row colors*/
   $(".bgAIGLightBlue tr:odd").addClass("odd");
   $(".bgAIGLightBlue tr:even").addClass("even");
   $(".bgAIGCoolGrey1 tr:odd").addClass("even");
   $(".bgAIGCoolGrey1 tr:even").addClass("odd");
   $(".bgAIGLighterBlue tr:odd").addClass("even");
   $(".bgAIGCoolGrey2 tr:odd").addClass("even");
/*End Alternating table row colors*/
   
//$("ul#navListPanel li div").css({'opacity': 'http://www.aig.com/Chartis/internet/US/en/0.90', 'filter':'alpha(opacity=90)'}); // topMenu Opacity
   $("a[name='backToList'], a[name='tab14Dtls']").attr('href','javascript:void(0);');
   $('#topMenu #navListPanel>li').hover(function () {
        $(this).children('#subNav3').show();
        $(this).addClass('linkExtend');
    }, function () {
        $(this).children('#subNav3').hide();
        $(this).removeClass('linkExtend');
    });
    $('<p class="copyrightTxt">' + $('#siteFooter').text() + '</p>').prependTo('#footerSection');
    $('#siteFooter').hide();
    $('#siteSearch .txtMedium').val('Search');
    $('.detailAccModule .detModTitle:odd').addClass('odd');
    $('.detailAccModule .detModTitle:even').addClass('even');
    $('#logobanner2').insertAfter('#siteSearch');
    $('#pageFooter').insertAfter('.copyrightTxt');

	if($(".date")){
		var value;
		var newvalue;
		$(".date").each(function(){
		var value = $(this).text();
		var newValue = value.replace(" ", "");
		$(this).text(newValue);
		});
	}

    /*Careers page banner rotation*/
    $('.rightModule .AIGBlack div:first').show().addClass('active');
    $('.rightModule .AIGBlack div').each(function(d){
        $(this).addClass('recog'+d);
    });

    setInterval(autoRecogRot, 5000);
    
    function autoRecogRot() {
        var $activeImg = $('.rightModule .AIGBlack .active');
        var $next = ($('.rightModule .AIGBlack .active').next().length > 0) ? $('.rightModule .AIGBlack .active').next() : $('.rightModule .AIGBlack div:first');
        $activeImg.fadeOut(function(){
            $activeImg.removeClass('active');
            $next.fadeIn().addClass('active');
        });
    }

// Copyright line break
//	var html = $(".copyrightTxt").html();
//	html = html.substring(0, 42) + "<br>" + html.substring(42);
//	$(".copyrightTxt").html(html); 
});

function setCookie(c_name,value,exdays)
	{
	var exdate=new Date();
	exdate.setDate(exdate.getDate() + exdays);
	var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
	document.cookie=c_name + "=" + c_value;
	}