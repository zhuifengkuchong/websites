var Count=0;
function carousalLocal(carouselID, displayCount) {
    var thmbWidth = $("" + carouselID + " ul.thumblist li").outerWidth(true);
    $("" + carouselID + " ul.thumblist >li").each(function () {
        if ($(this).outerWidth(true) < thmbWidth) {
            thmbWidth = $(this).outerWidth(true)
        }
    });
    /*if ($("" + carouselID + " ul.thumblist > li > ul").length) {
        var thmbDisplay = $("" + carouselID + " ul.thumblist > li > ul").size() - displayCount;
        var thmbPanelWidth = $("" + carouselID + " ul.thumblist > li > ul").size() * thmbWidth
    } else {
        var thmbDisplay = $("" + carouselID + " ul.thumblist > li").size() - displayCount;
        var thmbPanelWidth = $("" + carouselID + " ul.thumblist li").size() * thmbWidth
    }
    $("" + carouselID + " ul.thumblist").width(thmbPanelWidth);*/
    if (carouselID != "#footerSecDetails") {
        if ($("" + carouselID + " ul.thumblist > li").size() <= displayCount) {
            $("" + carouselID + " .btnlft, " + carouselID + " .btnrgt").css("visibility", "Hidden")
        }
    } else {
        var fthumblist = $("" + carouselID + " ul.thumblist").width();
        var fthumbholder = $("" + carouselID + " div.thumbholder").width();
        if (fthumblist <= fthumbholder) {
            $("" + carouselID + " .btnlft, " + carouselID + " .btnrgt").css("visibility", "Hidden")
        }
    }
    $("" + carouselID + " .btnlft").fadeTo("fast", 0.2);
    /*$(carouselID + " .btnrgt").click(function (e) {
        e.preventDefault();
        var thmbLeft = $("" + carouselID + " ul.thumblist").css("margin-left").replace("px", "");
        var animDetect = thmbLeft % thmbWidth;
        if (thmbLeft > -(thmbDisplay * thmbWidth) && (animDetect == 0)) {
            $("" + carouselID + " ul.thumblist").animate({
                marginLeft: "-=" + thmbWidth
            }, 1000);
            $("" + carouselID + " .btnlft").fadeTo("fast", 1);
            if (thmbLeft / thmbWidth == 1 - thmbDisplay) {
                $("" + carouselID + " .btnrgt").fadeTo("fast", 0.2)
            }
        }
    });
    $("" + carouselID + " .btnlft").click(function (e) {
        e.preventDefault();
        var thmbLeft = $("" + carouselID + " ul.thumblist").css("margin-left").replace("px", "");
        var animDetect = thmbLeft % thmbWidth;
        if (thmbLeft < 0 && (animDetect == 0)) {
            $("" + carouselID + " ul.thumblist").animate({
                marginLeft: "+=" + thmbWidth
            }, 1000);
            $("" + carouselID + " .btnrgt").fadeTo("fast", 1);
            if (thmbLeft / thmbWidth == -1) {
                $("" + carouselID + " .btnlft").fadeTo("fast", 0.2)
            }
        }
    }) */
	/*
	setInterval(function(){	
		if(Count == 99) { return false; }
		Count++;	
		if(Count > $(".bannerSection").length -1){	
			//$("#thumbnail ul li").removeClass("on");
			//$("#thumbnail ul li").eq(0).addClass("on");				
			//$('.bannerSection').fadeOut('slow');
			//$('.bannerSection').eq(Count).fadeIn(2000);
			Count=0;						
		}
		$("#thumbnail ul li").removeClass("on");	
		$("#thumbnail ul li").eq(Count).addClass("on");			
		$('.bannerSection').fadeOut('slow');
		$('.bannerSection').eq(Count).fadeIn('slow');		
	 }, 6000);
	*/
	 setInterval(function(){	
		if(Count == 99)
			return false;
		Count++;
		var thisMany = $('.bannerSection').length;
		if(Count > thisMany -1)
			Count=0;				
		if (thisMany > 1){
			$("#thumbnail ul li").removeClass("on");
			$("#thumbnail ul li").eq(Count).addClass("on");
			$('.bannerSection').fadeOut('slow');
			$('.bannerSection').eq(Count).fadeIn('slow');
		}
	 }, 8000);
}
function thumbSection(thumID) {
    if ($("#segmentBanner").length) {
        $("#" + thumID + " ul li").eq(0).attr("class", "on");
    }
    $("#" + thumID + " ul li").each(function (z) {
        var $tThis = $(this);
        $tThis.attr("id", "carImg" + z + 1 + "").hover(function () {
            $tThis.addClass("hover")
        }, function () {
            $tThis.removeClass("hover")
        }).click(function (e) {
            e.preventDefault();
			$("#" + thumID + " ul li").removeClass("on");	
			$tThis.attr("class", "on");
			showSlide($("#" + thumID + " ul li").index($tThis), "#" + thumID + "")
			//Count=$("#" + thumID + " ul li").index($tThis)-1;
			Count = 99;	
        })
    })
};
carousalLocal("#segmentBanner #thumbnail", $("#segmentBanner #thumbnail li").length);