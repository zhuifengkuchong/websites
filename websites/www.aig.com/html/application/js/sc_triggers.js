/*
*************************************************************
*   SiteCatalyst Utility JavaScript functions		    * 
*************************************************************
*/
function SCTrackFileDL(fn,obj){
  s.tempLTV=s.linkTrackVars
  s.tempLTE=s.linkTrackEvents
  s.linkTrackVars="events,eVar24,prop24,prop25,prop4,prop12,prop13,prop14,prop15"
  s.linkTrackEvents="event9"
  s.events="event9"
  s.eVar24=s.prop24=fn
  s.prop25=s.pageName
  s.tl(obj,'d',fn)
  s.events=''
  s.eVar24=s.prop24=s.prop25=""
  s.linkTrackVars=s.tempLTV
  s.linkTrackEvents=s.tempLTE
}

function SCTrackCustomLinkMetrics(obj, customLinkMetrics, searchTerm){
   s.linkTrackVars='None'; 
   s.linkTrackEvents='None'; 
   
   customLinkName = "";
   if(searchTerm == null || searchTerm == undefined) {
   s.prop5 = s.prop16; 
   customLinkName = customLinkMetrics + "-" + s.prop5   
   } else {
      s.prop5 = searchTerm; 
      customLinkName = customLinkMetrics + "-" + searchTerm;   
   }
   s.tl(obj,'o',customLinkName); 
}
