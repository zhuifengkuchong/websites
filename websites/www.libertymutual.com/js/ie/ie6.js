/**
 * IE6 JavaScript to handle the new navigation requirements.
 * 7/15/2011 - Liberty Mutual
 */

var ie6Nav = (function($){
	$('#megaMenuContainer li.firstLevel').bind("mouseover", function(){
		$(this).addClass("hoverIE");
	});
	$('#megaMenuContainer li.firstLevel').bind("mouseout", function(){
		$(this).removeClass("hoverIE");
	});
	//
	$('#megaMenuContainer ul > li.firstLevel:first').css("border-left", "none");
	$('#globalContainer .firstLevel li:first').css("border", "none");
	$('#megaMenuContainer ul.megaLevel li.hoverIE:first').css("padding", "0");
	//
	$('#megaMenuContainer div.subMenu').each(function(){
		var parentWidth = $(this).width();
		$(this).find(".columnDividerCover").css("width", (parentWidth-5) + "px");
	}); 
	//
	$('#globalContainer li.firstLevel').each(function(){
		var parentWidth = $(this).width();
		$(this).find(".coverBlend").css("width", (parentWidth) + "px");
	}); 
})(jQuery);