/* OnlineOpinion v4.1.6.0 */
/* This product and other products of OpinionLab, Inc. are protected by U.S. Patent No. 6606581, 6421724, 6785717 B1 and other patents pending. */
var OnlineOpinion=new Object();OnlineOpinion.util={SafeAddOnLoadEvent:function(func){if(!document.getElementById|!document.getElementsByTagName)return;var oldonload=window.onload;if(typeof window.onload!='function'){window.onload=func}else{window.onload=function(){oldonload();func()}}},SafeAddOnUnLoadEvent:function(func){if(!document.getElementById|!document.getElementsByTagName)return;var oldonunload=window.onunload;if(typeof window.onunload!='function'){window.onunload=func}else{window.onunload=function(){func();oldonunload()}}},popup:function(url,wname,wfeatures){var wpopup=window.open(url,wname,wfeatures);if(typeof wpopup=='undefined'){if(document.all){document.getElementById("test").href=url;document.getElementById("test").click()}else{var newWindow=window.open(url,'_e');newWindow.focus()}};return false},walkAnchors:function(node,depth,internal_links_re,ooObj){var MAX_NODES=1000;var count=0;while(node&&depth>0){count++;if(count>=MAX_NODES){var handler=function(){OnlineOpinion.util.walkAnchors(node,depth,internal_links_re,ooObj)};setTimeout(handler,50);return}if(node.tagName=="A"||node.tagName=="AREA"){if(internal_links_re.test(node.href)){node.onmousedown=function(){ooObj.Preferences.Plugins.Events.poX=0}}}if(node.tagName=="INPUT"){if(node.type=="submit"||node.type=="image"){node.onmousedown=function(){ooObj.Preferences.Plugins.Events.poX=0}}}if(node.tagName=="FORM"){if(typeof node.onsubmit!='function'){node.onsubmit=function(){ooObj.Preferences.Plugins.Events.poX=0}}else{var oldonsubmit=node.onsubmit;node.onsubmit=function(){ooObj.Preferences.Plugins.Events.poX=0;oldonsubmit()}}}if(node.nodeType==1){var skipre=/^(script|style|textarea)/i;if(!skipre.test(node.tagName)&&node.childNodes.length>0){node=node.childNodes[0];depth++;continue}}if(node.nextSibling){node=node.nextSibling}else{while(depth>0){node=node.parentNode;depth--;if(node==null)break;if(node.nextSibling){node=node.nextSibling;break}}}}}};OnlineOpinion.cookie=function(){this.cookie_name='oo_r';this.expiration=24*60*60*1000;this.rhex=function(num){var hex_chr='0123456789abcdef',_3='';for(var j=0;j<=3;j++)_3+=hex_chr.charAt((num>>(j*8+4))&0x0F)+hex_chr.charAt((num>>(j*8))&0x0F);return _3};this.str2blks_MD5=function(_3){var nblk=((_3.length+8)>>6)+1,blks=new Array(nblk*16);var i=0;for(;i<nblk*16;i++)blks[i]=0;for(i=0;i<_3.length;i++)blks[i>>2]|=_3.charCodeAt(i)<<((i%4)*8);blks[i>>2]|=0x80<<((i%4)*8);blks[nblk*16-2]=_3.length*8;return blks};this._4=function(x,y){var lsw=(x&0xFFFF)+(y&0xFFFF),msw=(x>>16)+(y>>16)+(lsw>>16);return(msw<<16)|(lsw&0xFFFF)};this.rol=function(num,cnt){return(num<<cnt)|(num>>>(32-cnt))};this.cmn=function(q,a,b,x,s,t){return this._4(this.rol(this._4(this._4(a,q),this._4(x,t)),s),b)};this._0=function(a,b,c,d,x,s){return this.cmn((b&c)|((~b)&d),a,0,x,s,0)};this._1=function(a,b,c,d,x,s){return this.cmn((b&c)|(b&d)|(c&d),a,0,x,s,1518500249)};this._2=function(a,b,c,d,x,s){return this.cmn(b^c^d,a,0,x,s,1859775393)};this._6=function(_3){var x=this.str2blks_MD5(_3),a=1732584193,b=-271733879,c=-1732584194,d=271733878;for(var i=0;i<x.length;i+=16){var olda=a,oldb=b,oldc=c,oldd=d;a=this._0(a,b,c,d,x[i+0],3);d=this._0(d,a,b,c,x[i+1],7);c=this._0(c,d,a,b,x[i+2],11);b=this._0(b,c,d,a,x[i+3],19);a=this._0(a,b,c,d,x[i+4],3);d=this._0(d,a,b,c,x[i+5],7);c=this._0(c,d,a,b,x[i+6],11);b=this._0(b,c,d,a,x[i+7],19);a=this._0(a,b,c,d,x[i+8],3);d=this._0(d,a,b,c,x[i+9],7);c=this._0(c,d,a,b,x[i+10],11);b=this._0(b,c,d,a,x[i+11],19);a=this._0(a,b,c,d,x[i+12],3);d=this._0(d,a,b,c,x[i+13],7);c=this._0(c,d,a,b,x[i+14],11);b=this._0(b,c,d,a,x[i+15],19);a=this._1(a,b,c,d,x[i+0],3);d=this._1(d,a,b,c,x[i+4],5);c=this._1(c,d,a,b,x[i+8],9);b=this._1(b,c,d,a,x[i+12],13);a=this._1(a,b,c,d,x[i+1],3);d=this._1(d,a,b,c,x[i+5],5);c=this._1(c,d,a,b,x[i+9],9);b=this._1(b,c,d,a,x[i+13],13);a=this._1(a,b,c,d,x[i+2],3);d=this._1(d,a,b,c,x[i+6],5);c=this._1(c,d,a,b,x[i+10],9);b=this._1(b,c,d,a,x[i+14],13);a=this._1(a,b,c,d,x[i+3],3);d=this._1(d,a,b,c,x[i+7],5);c=this._1(c,d,a,b,x[i+11],9);b=this._1(b,c,d,a,x[i+15],13);a=this._2(a,b,c,d,x[i+0],3);d=this._2(d,a,b,c,x[i+8],9);c=this._2(c,d,a,b,x[i+4],11);b=this._2(b,c,d,a,x[i+12],15);a=this._2(a,b,c,d,x[i+2],3);d=this._2(d,a,b,c,x[i+10],9);c=this._2(c,d,a,b,x[i+6],11);b=this._2(b,c,d,a,x[i+14],15);a=this._2(a,b,c,d,x[i+1],3);d=this._2(d,a,b,c,x[i+9],9);c=this._2(c,d,a,b,x[i+5],11);b=this._2(b,c,d,a,x[i+13],15);a=this._2(a,b,c,d,x[i+3],3);d=this._2(d,a,b,c,x[i+11],9);c=this._2(c,d,a,b,x[i+7],11);b=this._2(b,c,d,a,x[i+15],15);a=this._4(a,olda);b=this._4(b,oldb);c=this._4(c,oldc);d=this._4(d,oldd)}return this.rhex(a)+this.rhex(b)+this.rhex(c)+this.rhex(d)};this.read=function(n){var neq=n+"=";var ca=document.cookie.split(';');for(var i=0;i<ca.length;i++){var c=ca[i];while(c.charAt(0)==' ')c=c.substring(1,c.length);if(c.indexOf(neq)==0)return unescape(c.substring(neq.length,c.length))}return null};this.write=function(n,v){document.cookie=n+'='+v+';path=/;expires='+(new Date((new Date()).getTime()+this.expiration)).toGMTString()};this.matchurl=function(u,type){var i=0,c=this.read(this.cookie_name);if(type=='domain')u=window.location.hostname;n=this._6(u);if(c==null)return false;while(i<c.length){j=i+n.length;if(c.substring(i,j)==n){return(unescape(c.substring(j+1,j+2))==1)}i++}return false};this.tagurl=function(u,type){if(type=='domain')u=window.location.hostname;var prev_val="";if(this.read(this.cookie_name)!=null){prev_val=this.read(this.cookie_name).replace(eval('/'+escape(this._6(u))+'~1:/g'),'')}this.write(this.cookie_name,prev_val+(prev_val!=''?':':'')+escape(this._6(u))+'~1')}};function unescapeHTML(str){var div=document.createElement('div'),acc='';div.innerHTML=str.replace(/<\w+(\s+("[^"]*"|'[^']*'|[^>])+)?>|<\/\w+>/gi,'');for(var i=0;i<div.childNodes.length;i++){acc=acc+div.childNodes[i].nodeValue}return acc};OnlineOpinion.tleaf={get_session:function(c_name){if(document.cookie.length>0){c_start=document.cookie.indexOf(c_name+"=");if(c_start!=-1){c_start=c_start+c_name.length+1;c_end=document.cookie.indexOf(";",c_start);if(c_end==-1)c_end=document.cookie.length;return unescape(document.cookie.substring(c_start,c_end))}}return""}};OnlineOpinion.onPageCCSubmitted=false;OnlineOpinion.instanceCount=0;OnlineOpinion.renderOnPageCC=function(iframe_id,div_id,json_id){var oIframe=document.getElementById(iframe_id),oDoc=oIframe.contentDocument||oIframe.contentWindow.document;oDoc.open();oDoc.write(unescapeHTML(cc_html).replace("resize(_f('_g'));",''));var intervalID=window.setInterval(function(){if(oDoc.body){if(!window.opera)oDoc.close();window.clearInterval(intervalID);var innerBody=oDoc.body,innerDoc=oDoc||frames[iframe_id].document,onPageCCDiv=document.getElementById(div_id);oIframe.style.height=(innerBody.scrollHeight+1)+'px';innerBody.style.border="none";onPageCCDiv.style.display='block';onPageCCDiv.style.visibility='visible';innerDoc.getElementById('CommentCard').onsubmit=function(){OnlineOpinion.onPageCCSubmitted=true;document.body.removeChild(document.getElementById(json_id));oIframe.id='';onPageCCDiv.id=''}}},1000)};OnlineOpinion.ocode=function(name){this.name=name;OnlineOpinion.instanceCount++;this.instanceNum=OnlineOpinion.instanceCount;function rematch(val,restr){var re=new RegExp(restr);var m=re.exec(val);if(m==null||m==''){return''}else{var s="";for(i=0;i<m.length;i++){s=s+m[i]}return s}};this._h=function(_7){_c=this._i+',\\/,\\.,-,_,'+this._j+',%2F,%2E,%2D,%5F';_8=_c.split(',');for(i=0;i<5;i++){eval('_7=_7.replace(/'+_8[i]+'/g,_8[i+5])')}return _7};this._9=function(){this.engine=null;this.version=null;var useragent=navigator.userAgent.toLowerCase();if(window.ActiveXObject){this.engine='ie';this.version=rematch(useragent,"msie\\s[0-9]\.[0-9]+").replace('msie ','')}else{if(window.opera){this.engine='opera';this.version=rematch(useragent,"opera.[0-9]\.[0-9]+").replace('opera','').replace('/','')}else{if(document.childNodes&&!document.all&&!navigator.taintEnabled){if(rematch(useragent,"applewebkit\/[0-9]+")!=null){this.engine='webkit';this.version=rematch(useragent,"applewebkit\/[0-9]+").replace('applewebkit/','')}else{this.engine='khtml';this.version=rematch(useragent,"khtml\/[0-9]\.[0-9]\.[0-9]+").replace('khtml/','')}}else if(document.getBoxObjectFor!=null){this.engine='gecko';this.version=rematch(useragent,"gecko/[0-9]+").replace('gecko/','')}}}};this.serialize=function(obj,option){var str_out='',inc=0;for(var i in obj){if(typeof obj[i]!='function'&&typeof obj[i]!='undefined'&&obj[i]!=null&&obj[i]!=''){if(option==0){str_out+=(inc==0?'':'&')+i+'='+escape(obj[i])}else{if(option==1){str_out+=(inc==0?'':'|')+escape(obj[i])}}inc++}}return str_out};this._a=false;this.Preferences=new Object();this.Preferences.Persistence={enabled:true,cookie_type:'page',cookie_name:'oo_r',expiration:24*60*60*1000};this.Preferences.Render={type:'floating',main_div_id:'oo_feedback_float',img_path:'Unknown_83_filename'/*tpa=https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/onlineopinion/oo_black.gif*/,feedback_html:'FEEDBACK',click_html:'Click here to<br>rate this page',ty_html:'',float_style:'fixed'};this.Metrics=new Object();this.Metrics.core={'width':screen.width,'height':screen.height,'referer':window.location.href,'prev':document.referrer,'time1':(new Date()).getTime(),'time2':null};this.Metrics.custom=new Object();this.Preferences.Plugins=new Object();this.Preferences.Plugins.Events={poE:0.0,onEntryDelay:0,poX:0.0,poC:{id:'',value:0.0},clickDelay:0,poWC:0.0};this.onEntry=function(){this._5();if(Math.random()>=1.0-this.Preferences.Plugins.Events.poE){if(this.Preferences.Plugins.Events.onEntryDelay){thisObj=this;setTimeout(function(){thisObj.show()},this.Preferences.Plugins.Events.onEntryDelay*1000)}else{this.show()}this.Preferences.Plugins.Events.poX=0.0}};this.onExit=function(){this._5();if(Math.random()>=1.0-this.Preferences.Plugins.Events.poX)this.show()};this.OnClick=function(){this._5();if(this.Preferences.Render.type=='inline'){this.show()}else if(Math.random()>=1.0-this.Preferences.Plugins.Events.poC){if(this.Preferences.Plugins.Events.clickDelay){thisObj=this;setTimeout(function(){thisObj.show()},this.Preferences.Plugins.Events.clickDelay*1000)}else{this.show()}this.Preferences.Plugins.Events.poX=0.0;this.Preferences.Plugins.Events.poC=0.0}};this.Preferences.Plugins.URLRewrite={active:false,full_url_rewrite:null,regex_search_pattern:'',regex_replace_pattern:''};this.Preferences.Plugins.CardOnPage={enabled:false};this._5=function(){if(this._a)return true;this._a=true;this.resetReferer();if(typeof OnlineOpinion.cookie!='undefined'&&this.Preferences.Persistence.cookie_type!=null){this.Cookie=new OnlineOpinion.cookie();if(typeof this.Preferences.Persistence.cookie_name!='undefined')this.Cookie.cookie_name=this.Preferences.Persistence.cookie_name;this.Cookie.expiration=1000*this.Preferences.Persistence.expiration;if(this.Cookie.matchurl(this.Metrics.core.referer,this.Preferences.Persistence.cookie_type)==1&&this.Preferences.Plugins.Events){this.Preferences.Plugins.Events.poX=0;this.Preferences.Plugins.Events.poE=0;return false}}else{this.Cookie=null}return true};this.render=function(onclick_func){this._5();this.Preferences.Render.main_div_id=this.Preferences.Render.main_div_id||'oo_feedback_'+this.instanceNum;var b=new this._9(),d=document,de=d.documentElement,db=d.body,w=window,divID=this.Preferences.Render.main_div_id,bVersion=parseFloat(b.version),compliant=d.compatMode=='CSS1Compat',sObj=compliant?de:db,tempContainer=document.createDocumentFragment();if(b.engine=="webkit"){sObj=db}if(b.engine==null||b.version==null||isNaN(parseInt(b.version,10))||(b.engine=='ie'&&parseFloat(b.version)<6)||(b.engine=='opera'&&parseInt(b.version,10)<8)||(b.engine=='gecko'&&parseInt(b.version,10)<20041107))return false;var mainDivObj=d.getElementById(divID);if(mainDivObj==null){if(this.Preferences.Render.type=='floating'){mainDivObj=d.createElement("div");mainDivObj.id=divID;mainDivObj.className='oo_feedback_float'}}if(mainDivObj.childNodes.length==0){if(this.Preferences.Render.type=='floating'){if(this.Preferences.Persistence.enabled&&this.Preferences.Persistence.cookie_type!=null){if(this.Cookie.matchurl(this.Metrics.core.referer,this.Preferences.Persistence.cookie_type)==1)return false}db.appendChild(mainDivObj);var olUpObj=tempContainer.appendChild(d.createElement("div"));olUpObj.className='olUp';var olOverObj=tempContainer.appendChild(d.createElement("div"));olOverObj.className='olOver';if((b.engine=='ie'&&b.version<7||!compliant)&&this.Preferences.Render.click_html&&!this.Preferences.Render.disable_rollover){olUpObj.onmouseover=(function(olUpObj,olOverObj){return function(){olOverObj.style.display='block';olUpObj.style.display='none'}})(olUpObj,olOverObj);olOverObj.onmouseout=(function(olUpObj,olOverObj){return function(){olUpObj.style.display='block';olOverObj.style.display='none'}})(olUpObj,olOverObj)}if(this.Preferences.Render.img_path){var ooImg=olUpObj.appendChild(d.createElement("img"));ooImg.src=this.Preferences.Render.img_path}var fbText=olUpObj.appendChild(d.createElement("span"));fbText.className='fbText';fbText.innerHTML=this.Preferences.Render.feedback_html;if(this.Preferences.Render.transparent_background){var op_bg=olUpObj.appendChild(d.createElement("div"));op_bg.className='oo_transparent'}var feedback_text=null;if(this.Preferences.Render.div_alt_text){feedback_text=this.Preferences.Render.div_alt_text}else{if(document.all){feedback_text=fbText.innerText}else{feedback_text=fbText.textContent}}mainDivObj.alt=feedback_text;mainDivObj.title=feedback_text;olOverObj.innerHTML=this.Preferences.Render.click_html}if(this.Preferences.Render.type=='static'){mainDivObj.innerHTML=this.Preferences.Render.main_html}}mainDivObj.appendChild(tempContainer);if(this.Preferences.Render.type=='floating'){if(!this.Preferences.Render.float_style)this.Preferences.Render.float_style='fixed';var mdoStyle=mainDivObj.style,_b=this,floatStyle=this.Preferences.Render.float_style;if(floatStyle.match(/Content/)){var contentDiv=d.getElementById(_b.Preferences.Render.main_content_id||"content");if(contentDiv==null){floatStyle=this.Preferences.Render.float_style='fixed'}function getRightOfContent(){return contentDiv.offsetWidth+contentDiv.offsetLeft+1}function fixBackground(){if(_b.Preferences.Render.fix_background){db.style.backgroundAttachment="scroll";var margin=null;if(document.defaultView&&document.defaultView.getComputedStyle){margin=parseInt(document.defaultView.getComputedStyle(contentDiv,null).getPropertyValue("margin-left"),10)}else{margin=parseInt(contentDiv.currentStyle.marginLeft,10)}if(isNaN(margin)||margin==0){margin=contentDiv.offsetLeft||0}db.style.backgroundPosition=(Math.floor(contentDiv.scrollWidth*-0.5)-2+margin)+'px 0'}}}var handleIEQuirks=function(e){if(floatStyle.match(/^fixed/)){var newLeft=sObj.scrollLeft+sObj.clientWidth-mainDivObj.clientWidth;if(floatStyle=="fixedPreserveContent"&&newLeft<getRightOfContent()){newLeft=getRightOfContent()+'px'}else if(floatStyle=="fixedContentMax"&&(contentDiv.offsetWidth+mainDivObj.clientWidth)<sObj.clientWidth){newLeft=getRightOfContent()+'px'}mdoStyle.left=newLeft}else if(floatStyle=="rightOfContent"){mdoStyle.left=getRightOfContent()+'px'}mdoStyle.top=sObj.scrollTop+sObj.clientHeight-mainDivObj.clientHeight;var nullOrLoad=e==null||e.type=='load';if(nullOrLoad)mdoStyle.visibility='visible';if(nullOrLoad||e.type=='resize')fixBackground()};if(b.engine=='ie'&&(bVersion<7||!compliant)){mdoStyle.position='absolute';function mapEvents(target,events){for(var idx=0;idx<events.length;idx++){var curevent=events[idx];target.attachEvent("on"+curevent,handleIEQuirks)}};mapEvents(mainDivObj,["mouseover","mouseout"]);mapEvents(w,["resize","scroll"]);handleIEQuirks()}else{mdoStyle.position='fixed';var resizeMove=null;if(floatStyle.match(/^fixed/)){mdoStyle.right='0px';mdoStyle.bottom='0px';if(floatStyle=="fixedContentMax"){var maxContentWidth=contentDiv.offsetWidth+mainDivObj.offsetWidth;resizeMove=function(e){if(sObj.clientWidth>maxContentWidth){mdoStyle.left=getRightOfContent()-sObj.scrollLeft+'px';mdoStyle.right=null}else{mdoStyle.left=null;mdoStyle.right='0px'}}}}else if(floatStyle=="rightOfContent"){var rightOfContent=getRightOfContent()-sObj.scrollLeft+'px';mdoStyle.bottom='0px';mdoStyle.left=rightOfContent}if(floatStyle.match(/Content$/)){var preserve=floatStyle=="fixedPreserveContent";var gutter=d.createElement("div");db.replaceChild(gutter,mainDivObj);gutter.appendChild(mainDivObj);gutter.style.position="absolute";gutter.style.width=mainDivObj.offsetWidth+'px';gutter.style.right='0px';gutter.style.top='0px';gutter.style.height=sObj.scrollHeight+'px';resizeMove=function(e){var right=getRightOfContent();var runFix=false;if(preserve){var left=sObj.offsetWidth-mainDivObj.offsetWidth;if(left<=right)runFix=true}else{runFix=true}if(runFix){mdoStyle.left=right-sObj.scrollLeft+'px';if(e==null||e.type=='resize'){gutter.style.left=right+'px';fixBackground()}}else{gutter.style.right='0px';gutter.style.left=null;mdoStyle.right="0px";mdoStyle.left=null}}}if(resizeMove){if(b.engine=='ie'){w.attachEvent("onresize",resizeMove);w.attachEvent("onscroll",resizeMove)}else{w.addEventListener("resize",resizeMove,false);w.addEventListener("scroll",resizeMove,false)}resizeMove()}mainDivObj.style.visibility='visible'}}mainDivObj.onclick=onclick_func;return true};this.launchCC=function(){if(this.Cookie.matchurl(this.Metrics.core.referer,this.Preferences.Persistence.cookie_type)!=1){if(this.Preferences.Plugins.CardOnPage.enabled==true){if(this.Preferences.Plugins.CardOnPage.shown&&!OnlineOpinion.onPageCCSubmitted){var temp1=document.getElementById('onlineopinion_cc_div_'+this.instanceNum).style.display='block';var temp2=document.getElementById('onlineopinion_cc_overlay').style.display='block';temp2.onclick=this.hideCC(temp1,temp2)}else{var b=new this._9(),bVersion=parseFloat(b.version),d=document,db=document.body,de=d.documentElement,w=window,onPageCCdiv=db.appendChild(d.createElement("div")),onPageCCiframe=onPageCCdiv.appendChild(d.createElement("iframe")),onPageCCdivOverlay=d.getElementById("onlineopinion_cc_overlay")||db.appendChild(d.createElement("div")),close_link=d.createElement("a"),JSONP2=document.createElement("script");OnlineOpinion.CCcount++;onPageCCdivOverlay.id="onlineopinion_cc_overlay";onPageCCdiv.id='onlineopinion_cc_div_'+this.instanceNum;onPageCCdiv.className='onlineopinion_cc_div';onPageCCiframe.id='onlineopinion_iframe_'+this.instanceNum;onPageCCiframe.className='onlineopinion_cc_frame';onPageCCiframe.setAttribute("frameborder","0");if((b.engine=='ie'&&bVersion>=7)||b.engine=='opera'){var onPageCCShadow=d.createElement("div"),onPageCCShadowTop=d.createElement("div"),onPageCCShadowBottom=d.createElement("div"),tempFrag=d.createDocumentFragment();onPageCCShadow.className='onlineopinion_cc_shadow';onPageCCShadowTop.className='onlineopinion_cc_shadow_top';onPageCCShadowBottom.className='onlineopinion_cc_shadow_bottom';tempFrag.appendChild(onPageCCShadow);tempFrag.appendChild(onPageCCShadowTop);tempFrag.appendChild(onPageCCShadowBottom);onPageCCdiv.appendChild(tempFrag)}close_link.id='onlineopinion_cc_close_link_'+this.instanceNum;close_link.onclick=this.hideCC(onPageCCdiv,onPageCCdivOverlay);close_link.href="javascript:void(0)";close_link.className="onlineopinion_cc_closelink";close_link.title=this.Preferences.Plugins.CardOnPage.close_link;onPageCCdiv.insertBefore(close_link,onPageCCiframe);var W=(b.engine=='webkit')?w.innerWidth:(b.engine=='opera'?db.clientWidth:de.clientWidth),H=(b.engine=='webkit')?w.innerHeight:(b.engine=='opera'?db.clientHeight:de.clientHeight),wx=535;onPageCCdiv.style.left=parseInt((W-wx)/2,10)+'px';JSONP2.id='onlineopinion_cc_jsonp_'+this.instanceNum;document.body.appendChild(JSONP2);if(!window.XMLHttpRequest){var blah=this;var interval=window.setInterval(function(){if(typeof(cc_html)!='undefined')window.clearInterval(interval);JSONP2.src='https://secure.opinionlab.com/ccc01/comment_card_json.asp?'+(blah.Preferences.Render.type=='asm'?'asm=1&':'')+blah.serialize(blah.Metrics.core,0)+'&custom_var='+blah.serialize(blah.Metrics.custom,1)+'&jsonp='+escape('OnlineOpinion.renderOnPageCC("'+onPageCCiframe.id+'","'+onPageCCdiv.id+'","'+JSONP2.id+'")')},1000)}else JSONP2.src='https://secure.opinionlab.com/ccc01/comment_card_json.asp?'+(this.Preferences.Render.type=='asm'?'asm=1&':'')+this.serialize(this.Metrics.core,0)+'&custom_var='+this.serialize(this.Metrics.custom,1)+'&jsonp='+escape('OnlineOpinion.renderOnPageCC("'+onPageCCiframe.id+'","'+onPageCCdiv.id+'","'+JSONP2.id+'")');if(b.engine=='ie'&&bVersion<7){onPageCCdiv.style.position='absolute';onPageCCdivOverlay.style.position='absolute';onPageCCdivOverlay.style.height=document.documentElement.clientHeight+'px';onPageCCdivOverlay.style.width=document.documentElement.clientWidth+'px'}onPageCCdiv.style.visibility='hidden';onPageCCdiv.style.display='block';onPageCCdivOverlay.style.display='block';this.Preferences.Plugins.CardOnPage.shown=true;OnlineOpinion.onPageCCSubmitted=false}}else{var omtr_obj=typeof omtr_survey=='object'?this.serialize(omtr_survey,0):'';OnlineOpinion.util.popup('https://secure.opinionlab.com/ccc01/comment_card.asp?'+(this.Preferences.Render.type=='asm'?'asm=1&':'')+this.serialize(this.Metrics.core,0)+'&custom_var='+this.serialize(this.Metrics.custom,1)+omtr_obj,'OnlineOpinion','resizable=yes,copyhistory=yes,scrollbars='+(this.Preferences.Render.type=='asm'?'yes':'no')+',location=no,status=no,fullscreen=no,width=545,height=200,top='+parseInt((this.Metrics.core.height-200)/2,10)+',left='+parseInt((this.Metrics.core.width-545)/2,10))}}};this.hideCC=function(onPageCCdiv,onPageCCOverlay){return function(){onPageCCdiv.style.display='none';onPageCCOverlay.style.display='none'}};this.show=function(){this._5();this.Metrics.core.time2=(new Date()).getTime();this.launchCC();if(this.Preferences.Persistence.enabled){var mdivid=document.getElementById(this.Preferences.Render.main_div_id);if(mdivid)mdivid.style.display='none'}if(this.Cookie!=null&&this.Preferences.Persistence.cookie_type!=null){this.Cookie.tagurl(this.Metrics.core.referer,this.Preferences.Persistence.cookie_type)}this.resetReferer()};this.resetReferer=function(alt_referer){var location=window.location.href;if(alt_referer)location=alt_referer;var rewriter=this.Preferences.Plugins.URLRewrite;if(rewriter&&rewriter.active==true){this.Metrics.core.referer=this.Preferences.Plugins.URLRewrite.full_url_rewrite||location.replace(rewriter.regex_search_pattern,rewriter.regex_replace_pattern)}else{this.Metrics.core.referer=location}};this.post=function(){this.Metrics.core.time2=(new Date()).getTime();var mainDivObj=document.getElementById(this.Preferences.Render.main_div_id);var JSONP=mainDivObj.appendChild(document.createElement("script"));var odata=this.serialize(this.Metrics.core,0)+'&custom_var='+this.serialize(this.Metrics.custom,1);var elements=document.forms[this.Preferences.Render.main_div_id+'_d'].elements;var formdata=[];for(idx=0;idx<elements.length;idx++){var curElement=elements[idx];try{var name=curElement.name;if(name!=undefined&&curElement.value!=undefined){switch(curElement.type){case"radio":if(curElement.checked)formdata.push(curElement.name+'='+encodeURIComponent(curElement.value));break;case"checkbox":if(curElement.checked)formdata.push(curElement.name+'='+encodeURIComponent(curElement.value));break;default:formdata.push(curElement.name+'='+encodeURIComponent(curElement.value))}}}catch(e){}}var cdata=formdata.join('&');var osignature=this.Cookie!=null?this.Cookie._6(odata):"";JSONP.src="https://secure.opinionlab.com/rate36s.asp?"+odata+"&"+cdata+"&signature="+osignature;if(this.Preferences.Persistence.enabled){var mainFormDiv=document.getElementById(this.Preferences.Render.main_div_id+"_d");mainFormDiv.style.display='none'}if(this.Preferences.Render.ty_html!=''){var TYDivObj=document.getElementById(this.Preferences.Render.ty_div_id);TYDivObj.innerHTML=this.Preferences.Render.ty_html;TYDivObj.style.display='block'}if(this.Cookie!=null&&this.Preferences.Persistence.cookie_type!=null){this.Cookie.tagurl(this.Metrics.core.referer,this.Preferences.Persistence.cookie_type)}this.resetReferer()}};
/*!
 * jQuery JavaScript Library v1.5.2
 * http://jquery.com/
 *
 * Copyright 2011, John Resig
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * Includes Sizzle.js
 * http://sizzlejs.com/
 * Copyright 2011, The Dojo Foundation
 * Released under the MIT, BSD, and GPL Licenses.
 *
 * Date: Thu Mar 31 15:28:23 2011 -0400
 */
(function(a0,I){var am=a0.document;var b=(function(){var bo=function(bI,bJ){return new bo.fn.init(bI,bJ,bm)},bD=a0.jQuery,bq=a0.$,bm,bH=/^(?:[^<]*(<[\w\W]+>)[^>]*$|#([\w\-]+)$)/,bw=/\S/,bs=/^\s+/,bn=/\s+$/,br=/\d/,bk=/^<(\w+)\s*\/?>(?:<\/\1>)?$/,bx=/^[\],:{}\s]*$/,bF=/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,bz=/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,bt=/(?:^|:|,)(?:\s*\[)+/g,bi=/(webkit)[ \/]([\w.]+)/,bB=/(opera)(?:.*version)?[ \/]([\w.]+)/,bA=/(msie) ([\w.]+)/,bC=/(mozilla)(?:.*? rv:([\w.]+))?/,bG=navigator.userAgent,bE,bl,e,bv=Object.prototype.toString,bp=Object.prototype.hasOwnProperty,bj=Array.prototype.push,bu=Array.prototype.slice,by=String.prototype.trim,bf=Array.prototype.indexOf,bh={};bo.fn=bo.prototype={constructor:bo,init:function(bI,bM,bL){var bK,bN,bJ,bO;if(!bI){return this}if(bI.nodeType){this.context=this[0]=bI;this.length=1;return this}if(bI==="body"&&!bM&&am.body){this.context=am;this[0]=am.body;this.selector="body";this.length=1;return this}if(typeof bI==="string"){bK=bH.exec(bI);if(bK&&(bK[1]||!bM)){if(bK[1]){bM=bM instanceof bo?bM[0]:bM;bO=(bM?bM.ownerDocument||bM:am);bJ=bk.exec(bI);if(bJ){if(bo.isPlainObject(bM)){bI=[am.createElement(bJ[1])];bo.fn.attr.call(bI,bM,true)}else{bI=[bO.createElement(bJ[1])]}}else{bJ=bo.buildFragment([bK[1]],[bO]);bI=(bJ.cacheable?bo.clone(bJ.fragment):bJ.fragment).childNodes}return bo.merge(this,bI)}else{bN=am.getElementById(bK[2]);if(bN&&bN.parentNode){if(bN.id!==bK[2]){return bL.find(bI)}this.length=1;this[0]=bN}this.context=am;this.selector=bI;return this}}else{if(!bM||bM.jquery){return(bM||bL).find(bI)}else{return this.constructor(bM).find(bI)}}}else{if(bo.isFunction(bI)){return bL.ready(bI)}}if(bI.selector!==I){this.selector=bI.selector;this.context=bI.context}return bo.makeArray(bI,this)},selector:"",jquery:"1.5.2",length:0,size:function(){return this.length},toArray:function(){return bu.call(this,0)},get:function(bI){return bI==null?this.toArray():(bI<0?this[this.length+bI]:this[bI])},pushStack:function(bJ,bL,bI){var bK=this.constructor();if(bo.isArray(bJ)){bj.apply(bK,bJ)}else{bo.merge(bK,bJ)}bK.prevObject=this;bK.context=this.context;if(bL==="find"){bK.selector=this.selector+(this.selector?" ":"")+bI}else{if(bL){bK.selector=this.selector+"."+bL+"("+bI+")"}}return bK},each:function(bJ,bI){return bo.each(this,bJ,bI)},ready:function(bI){bo.bindReady();bl.done(bI);return this},eq:function(bI){return bI===-1?this.slice(bI):this.slice(bI,+bI+1)},first:function(){return this.eq(0)},last:function(){return this.eq(-1)},slice:function(){return this.pushStack(bu.apply(this,arguments),"slice",bu.call(arguments).join(","))},map:function(bI){return this.pushStack(bo.map(this,function(bK,bJ){return bI.call(bK,bJ,bK)}))},end:function(){return this.prevObject||this.constructor(null)},push:bj,sort:[].sort,splice:[].splice};bo.fn.init.prototype=bo.fn;bo.extend=bo.fn.extend=function(){var bR,bK,bI,bJ,bO,bP,bN=arguments[0]||{},bM=1,bL=arguments.length,bQ=false;if(typeof bN==="boolean"){bQ=bN;bN=arguments[1]||{};bM=2}if(typeof bN!=="object"&&!bo.isFunction(bN)){bN={}}if(bL===bM){bN=this;--bM}for(;bM<bL;bM++){if((bR=arguments[bM])!=null){for(bK in bR){bI=bN[bK];bJ=bR[bK];if(bN===bJ){continue}if(bQ&&bJ&&(bo.isPlainObject(bJ)||(bO=bo.isArray(bJ)))){if(bO){bO=false;bP=bI&&bo.isArray(bI)?bI:[]}else{bP=bI&&bo.isPlainObject(bI)?bI:{}}bN[bK]=bo.extend(bQ,bP,bJ)}else{if(bJ!==I){bN[bK]=bJ}}}}}return bN};bo.extend({noConflict:function(bI){a0.$=bq;if(bI){a0.jQuery=bD}return bo},isReady:false,readyWait:1,ready:function(bI){if(bI===true){bo.readyWait--}if(!bo.readyWait||(bI!==true&&!bo.isReady)){if(!am.body){return setTimeout(bo.ready,1)}bo.isReady=true;if(bI!==true&&--bo.readyWait>0){return}bl.resolveWith(am,[bo]);if(bo.fn.trigger){bo(am).trigger("ready").unbind("ready")}}},bindReady:function(){if(bl){return}bl=bo._Deferred();if(am.readyState==="complete"){return setTimeout(bo.ready,1)}if(am.addEventListener){am.addEventListener("DOMContentLoaded",e,false);a0.addEventListener("load",bo.ready,false)}else{if(am.attachEvent){am.attachEvent("onreadystatechange",e);a0.attachEvent("onload",bo.ready);var bI=false;try{bI=a0.frameElement==null}catch(bJ){}if(am.documentElement.doScroll&&bI){bg()}}}},isFunction:function(bI){return bo.type(bI)==="function"},isArray:Array.isArray||function(bI){return bo.type(bI)==="array"},isWindow:function(bI){return bI&&typeof bI==="object"&&"setInterval" in bI},isNaN:function(bI){return bI==null||!br.test(bI)||isNaN(bI)},type:function(bI){return bI==null?String(bI):bh[bv.call(bI)]||"object"},isPlainObject:function(bJ){if(!bJ||bo.type(bJ)!=="object"||bJ.nodeType||bo.isWindow(bJ)){return false}if(bJ.constructor&&!bp.call(bJ,"constructor")&&!bp.call(bJ.constructor.prototype,"isPrototypeOf")){return false}var bI;for(bI in bJ){}return bI===I||bp.call(bJ,bI)},isEmptyObject:function(bJ){for(var bI in bJ){return false}return true},error:function(bI){throw bI},parseJSON:function(bI){if(typeof bI!=="string"||!bI){return null}bI=bo.trim(bI);if(bx.test(bI.replace(bF,"@").replace(bz,"]").replace(bt,""))){return a0.JSON&&a0.JSON.parse?a0.JSON.parse(bI):(new Function("return "+bI))()}else{bo.error("Invalid JSON: "+bI)}},parseXML:function(bK,bI,bJ){if(a0.DOMParser){bJ=new DOMParser();bI=bJ.parseFromString(bK,"text/xml")}else{bI=new ActiveXObject("Microsoft.XMLDOM");bI.async="false";bI.loadXML(bK)}bJ=bI.documentElement;if(!bJ||!bJ.nodeName||bJ.nodeName==="parsererror"){bo.error("Invalid XML: "+bK)}return bI},noop:function(){},globalEval:function(bK){if(bK&&bw.test(bK)){var bJ=am.head||am.getElementsByTagName("head")[0]||am.documentElement,bI=am.createElement("script");if(bo.support.scriptEval()){bI.appendChild(am.createTextNode(bK))}else{bI.text=bK}bJ.insertBefore(bI,bJ.firstChild);bJ.removeChild(bI)}},nodeName:function(bJ,bI){return bJ.nodeName&&bJ.nodeName.toUpperCase()===bI.toUpperCase()},each:function(bL,bP,bK){var bJ,bM=0,bN=bL.length,bI=bN===I||bo.isFunction(bL);if(bK){if(bI){for(bJ in bL){if(bP.apply(bL[bJ],bK)===false){break}}}else{for(;bM<bN;){if(bP.apply(bL[bM++],bK)===false){break}}}}else{if(bI){for(bJ in bL){if(bP.call(bL[bJ],bJ,bL[bJ])===false){break}}}else{for(var bO=bL[0];bM<bN&&bP.call(bO,bM,bO)!==false;bO=bL[++bM]){}}}return bL},trim:by?function(bI){return bI==null?"":by.call(bI)}:function(bI){return bI==null?"":bI.toString().replace(bs,"").replace(bn,"")},makeArray:function(bL,bJ){var bI=bJ||[];if(bL!=null){var bK=bo.type(bL);if(bL.length==null||bK==="string"||bK==="function"||bK==="regexp"||bo.isWindow(bL)){bj.call(bI,bL)}else{bo.merge(bI,bL)}}return bI},inArray:function(bK,bL){if(bL.indexOf){return bL.indexOf(bK)}for(var bI=0,bJ=bL.length;bI<bJ;bI++){if(bL[bI]===bK){return bI}}return -1},merge:function(bM,bK){var bL=bM.length,bJ=0;if(typeof bK.length==="number"){for(var bI=bK.length;bJ<bI;bJ++){bM[bL++]=bK[bJ]}}else{while(bK[bJ]!==I){bM[bL++]=bK[bJ++]}}bM.length=bL;return bM},grep:function(bJ,bO,bI){var bK=[],bN;bI=!!bI;for(var bL=0,bM=bJ.length;bL<bM;bL++){bN=!!bO(bJ[bL],bL);if(bI!==bN){bK.push(bJ[bL])}}return bK},map:function(bJ,bO,bI){var bK=[],bN;for(var bL=0,bM=bJ.length;bL<bM;bL++){bN=bO(bJ[bL],bL,bI);if(bN!=null){bK[bK.length]=bN}}return bK.concat.apply([],bK)},guid:1,proxy:function(bK,bJ,bI){if(arguments.length===2){if(typeof bJ==="string"){bI=bK;bK=bI[bJ];bJ=I}else{if(bJ&&!bo.isFunction(bJ)){bI=bJ;bJ=I}}}if(!bJ&&bK){bJ=function(){return bK.apply(bI||this,arguments)}}if(bK){bJ.guid=bK.guid=bK.guid||bJ.guid||bo.guid++}return bJ},access:function(bI,bQ,bO,bK,bN,bP){var bJ=bI.length;if(typeof bQ==="object"){for(var bL in bQ){bo.access(bI,bL,bQ[bL],bK,bN,bO)}return bI}if(bO!==I){bK=!bP&&bK&&bo.isFunction(bO);for(var bM=0;bM<bJ;bM++){bN(bI[bM],bQ,bK?bO.call(bI[bM],bM,bN(bI[bM],bQ)):bO,bP)}return bI}return bJ?bN(bI[0],bQ):I},now:function(){return(new Date()).getTime()},uaMatch:function(bJ){bJ=bJ.toLowerCase();var bI=bi.exec(bJ)||bB.exec(bJ)||bA.exec(bJ)||bJ.indexOf("compatible")<0&&bC.exec(bJ)||[];return{browser:bI[1]||"",version:bI[2]||"0"}},sub:function(){function bJ(bL,bM){return new bJ.fn.init(bL,bM)}bo.extend(true,bJ,this);bJ.superclass=this;bJ.fn=bJ.prototype=this();bJ.fn.constructor=bJ;bJ.subclass=this.subclass;bJ.fn.init=function bK(bL,bM){if(bM&&bM instanceof bo&&!(bM instanceof bJ)){bM=bJ(bM)}return bo.fn.init.call(this,bL,bM,bI)};bJ.fn.init.prototype=bJ.fn;var bI=bJ(am);return bJ},browser:{}});bo.each("Boolean Number String Function Array Date RegExp Object".split(" "),function(bJ,bI){bh["[object "+bI+"]"]=bI.toLowerCase()});bE=bo.uaMatch(bG);if(bE.browser){bo.browser[bE.browser]=true;bo.browser.version=bE.version}if(bo.browser.webkit){bo.browser.safari=true}if(bf){bo.inArray=function(bI,bJ){return bf.call(bJ,bI)}}if(bw.test("\xA0")){bs=/^[\s\xA0]+/;bn=/[\s\xA0]+$/}bm=bo(am);if(am.addEventListener){e=function(){am.removeEventListener("DOMContentLoaded",e,false);bo.ready()}}else{if(am.attachEvent){e=function(){if(am.readyState==="complete"){am.detachEvent("onreadystatechange",e);bo.ready()}}}}function bg(){if(bo.isReady){return}try{am.documentElement.doScroll("left")}catch(bI){setTimeout(bg,1);return}bo.ready()}return bo})();var a="then done fail isResolved isRejected promise".split(" "),aA=[].slice;b.extend({_Deferred:function(){var bh=[],bi,bf,bg,e={done:function(){if(!bg){var bk=arguments,bl,bo,bn,bm,bj;if(bi){bj=bi;bi=0}for(bl=0,bo=bk.length;bl<bo;bl++){bn=bk[bl];bm=b.type(bn);if(bm==="array"){e.done.apply(e,bn)}else{if(bm==="function"){bh.push(bn)}}}if(bj){e.resolveWith(bj[0],bj[1])}}return this},resolveWith:function(bk,bj){if(!bg&&!bi&&!bf){bj=bj||[];bf=1;try{while(bh[0]){bh.shift().apply(bk,bj)}}catch(bl){}finally{bi=[bk,bj];bf=0}}return this},resolve:function(){e.resolveWith(this,arguments);return this},isResolved:function(){return !!(bf||bi)},cancel:function(){bg=1;bh=[];return this}};return e},Deferred:function(bf){var e=b._Deferred(),bh=b._Deferred(),bg;b.extend(e,{then:function(bj,bi){e.done(bj).fail(bi);return this},fail:bh.done,rejectWith:bh.resolveWith,reject:bh.resolve,isRejected:bh.isResolved,promise:function(bj){if(bj==null){if(bg){return bg}bg=bj={}}var bi=a.length;while(bi--){bj[a[bi]]=e[a[bi]]}return bj}});e.done(bh.cancel).fail(e.cancel);delete e.cancel;if(bf){bf.call(e,e)}return e},when:function(bk){var bf=arguments,bg=0,bj=bf.length,bi=bj,e=bj<=1&&bk&&b.isFunction(bk.promise)?bk:b.Deferred();function bh(bl){return function(bm){bf[bl]=arguments.length>1?aA.call(arguments,0):bm;if(!(--bi)){e.resolveWith(e,aA.call(bf,0))}}}if(bj>1){for(;bg<bj;bg++){if(bf[bg]&&b.isFunction(bf[bg].promise)){bf[bg].promise().then(bh(bg),e.reject)}else{--bi}}if(!bi){e.resolveWith(e,bf)}}else{if(e!==bk){e.resolveWith(e,bj?[bk]:[])}}return e.promise()}});(function(){b.support={};var bf=am.createElement("div");bf.style.display="none";bf.innerHTML="   <link/><table></table><a href='https://www.libertymutual.com/a' style='color:red;float:left;opacity:.55;'>a</a><input type='checkbox'/>";var bo=bf.getElementsByTagName("*"),bm=bf.getElementsByTagName("a")[0],bn=am.createElement("select"),bg=bn.appendChild(am.createElement("option")),bl=bf.getElementsByTagName("input")[0];if(!bo||!bo.length||!bm){return}b.support={leadingWhitespace:bf.firstChild.nodeType===3,tbody:!bf.getElementsByTagName("tbody").length,htmlSerialize:!!bf.getElementsByTagName("link").length,style:/red/.test(bm.getAttribute("style")),hrefNormalized:bm.getAttribute("href")==="/a",opacity:/^0.55$/.test(bm.style.opacity),cssFloat:!!bm.style.cssFloat,checkOn:bl.value==="on",optSelected:bg.selected,deleteExpando:true,optDisabled:false,checkClone:false,noCloneEvent:true,noCloneChecked:true,boxModel:null,inlineBlockNeedsLayout:false,shrinkWrapBlocks:false,reliableHiddenOffsets:true,reliableMarginRight:true};bl.checked=true;b.support.noCloneChecked=bl.cloneNode(true).checked;bn.disabled=true;b.support.optDisabled=!bg.disabled;var bh=null;b.support.scriptEval=function(){if(bh===null){var bq=am.documentElement,br=am.createElement("script"),bt="script"+b.now();try{br.appendChild(am.createTextNode("window."+bt+"=1;"))}catch(bs){}bq.insertBefore(br,bq.firstChild);if(a0[bt]){bh=true;delete a0[bt]}else{bh=false}bq.removeChild(br)}return bh};try{delete bf.test}catch(bj){b.support.deleteExpando=false}if(!bf.addEventListener&&bf.attachEvent&&bf.fireEvent){bf.attachEvent("onclick",function bp(){b.support.noCloneEvent=false;bf.detachEvent("onclick",bp)});bf.cloneNode(true).fireEvent("onclick")}bf=am.createElement("div");bf.innerHTML="<input type='radio' name='radiotest' checked='checked'/>";var bi=am.createDocumentFragment();bi.appendChild(bf.firstChild);b.support.checkClone=bi.cloneNode(true).cloneNode(true).lastChild.checked;b(function(){var br=am.createElement("div"),e=am.getElementsByTagName("body")[0];if(!e){return}br.style.width=br.style.paddingLeft="1px";e.appendChild(br);b.boxModel=b.support.boxModel=br.offsetWidth===2;if("zoom" in br.style){br.style.display="inline";br.style.zoom=1;b.support.inlineBlockNeedsLayout=br.offsetWidth===2;br.style.display="";br.innerHTML="<div style='width:4px;'></div>";b.support.shrinkWrapBlocks=br.offsetWidth!==2}br.innerHTML="<table><tr><td style='padding:0;border:0;display:none'></td><td>t</td></tr></table>";var bq=br.getElementsByTagName("td");b.support.reliableHiddenOffsets=bq[0].offsetHeight===0;bq[0].style.display="";bq[1].style.display="none";b.support.reliableHiddenOffsets=b.support.reliableHiddenOffsets&&bq[0].offsetHeight===0;br.innerHTML="";if(am.defaultView&&am.defaultView.getComputedStyle){br.style.width="1px";br.style.marginRight="0";b.support.reliableMarginRight=(parseInt(am.defaultView.getComputedStyle(br,null).marginRight,10)||0)===0}e.removeChild(br).style.display="none";br=bq=null});var bk=function(e){var br=am.createElement("div");e="on"+e;if(!br.attachEvent){return true}var bq=(e in br);if(!bq){br.setAttribute(e,"return;");bq=typeof br[e]==="function"}return bq};b.support.submitBubbles=bk("submit");b.support.changeBubbles=bk("change");bf=bo=bm=null})();var aG=/^(?:\{.*\}|\[.*\])$/;b.extend({cache:{},uuid:0,expando:"jQuery"+(b.fn.jquery+Math.random()).replace(/\D/g,""),noData:{embed:true,object:"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000",applet:true},hasData:function(e){e=e.nodeType?b.cache[e[b.expando]]:e[b.expando];return !!e&&!Q(e)},data:function(bh,bf,bj,bi){if(!b.acceptData(bh)){return}var bm=b.expando,bl=typeof bf==="string",bk,bn=bh.nodeType,e=bn?b.cache:bh,bg=bn?bh[b.expando]:bh[b.expando]&&b.expando;if((!bg||(bi&&bg&&!e[bg][bm]))&&bl&&bj===I){return}if(!bg){if(bn){bh[b.expando]=bg=++b.uuid}else{bg=b.expando}}if(!e[bg]){e[bg]={};if(!bn){e[bg].toJSON=b.noop}}if(typeof bf==="object"||typeof bf==="function"){if(bi){e[bg][bm]=b.extend(e[bg][bm],bf)}else{e[bg]=b.extend(e[bg],bf)}}bk=e[bg];if(bi){if(!bk[bm]){bk[bm]={}}bk=bk[bm]}if(bj!==I){bk[bf]=bj}if(bf==="events"&&!bk[bf]){return bk[bm]&&bk[bm].events}return bl?bk[bf]:bk},removeData:function(bi,bg,bj){if(!b.acceptData(bi)){return}var bl=b.expando,bm=bi.nodeType,bf=bm?b.cache:bi,bh=bm?bi[b.expando]:b.expando;if(!bf[bh]){return}if(bg){var bk=bj?bf[bh][bl]:bf[bh];if(bk){delete bk[bg];if(!Q(bk)){return}}}if(bj){delete bf[bh][bl];if(!Q(bf[bh])){return}}var e=bf[bh][bl];if(b.support.deleteExpando||bf!=a0){delete bf[bh]}else{bf[bh]=null}if(e){bf[bh]={};if(!bm){bf[bh].toJSON=b.noop}bf[bh][bl]=e}else{if(bm){if(b.support.deleteExpando){delete bi[b.expando]}else{if(bi.removeAttribute){bi.removeAttribute(b.expando)}else{bi[b.expando]=null}}}}},_data:function(bf,e,bg){return b.data(bf,e,bg,true)},acceptData:function(bf){if(bf.nodeName){var e=b.noData[bf.nodeName.toLowerCase()];if(e){return !(e===true||bf.getAttribute("classid")!==e)}}return true}});b.fn.extend({data:function(bi,bk){var bj=null;if(typeof bi==="undefined"){if(this.length){bj=b.data(this[0]);if(this[0].nodeType===1){var e=this[0].attributes,bg;for(var bh=0,bf=e.length;bh<bf;bh++){bg=e[bh].name;if(bg.indexOf("data-")===0){bg=bg.substr(5);aV(this[0],bg,bj[bg])}}}}return bj}else{if(typeof bi==="object"){return this.each(function(){b.data(this,bi)})}}var bl=bi.split(".");bl[1]=bl[1]?"."+bl[1]:"";if(bk===I){bj=this.triggerHandler("getData"+bl[1]+"!",[bl[0]]);if(bj===I&&this.length){bj=b.data(this[0],bi);bj=aV(this[0],bi,bj)}return bj===I&&bl[1]?this.data(bl[0]):bj}else{return this.each(function(){var bn=b(this),bm=[bl[0],bk];bn.triggerHandler("setData"+bl[1]+"!",bm);b.data(this,bi,bk);bn.triggerHandler("changeData"+bl[1]+"!",bm)})}},removeData:function(e){return this.each(function(){b.removeData(this,e)})}});function aV(bg,bf,bh){if(bh===I&&bg.nodeType===1){bh=bg.getAttribute("data-"+bf);if(typeof bh==="string"){try{bh=bh==="true"?true:bh==="false"?false:bh==="null"?null:!b.isNaN(bh)?parseFloat(bh):aG.test(bh)?b.parseJSON(bh):bh}catch(bi){}b.data(bg,bf,bh)}else{bh=I}}return bh}function Q(bf){for(var e in bf){if(e!=="toJSON"){return false}}return true}b.extend({queue:function(bf,e,bh){if(!bf){return}e=(e||"fx")+"queue";var bg=b._data(bf,e);if(!bh){return bg||[]}if(!bg||b.isArray(bh)){bg=b._data(bf,e,b.makeArray(bh))}else{bg.push(bh)}return bg},dequeue:function(bh,bg){bg=bg||"fx";var e=b.queue(bh,bg),bf=e.shift();if(bf==="inprogress"){bf=e.shift()}if(bf){if(bg==="fx"){e.unshift("inprogress")}bf.call(bh,function(){b.dequeue(bh,bg)})}if(!e.length){b.removeData(bh,bg+"queue",true)}}});b.fn.extend({queue:function(e,bf){if(typeof e!=="string"){bf=e;e="fx"}if(bf===I){return b.queue(this[0],e)}return this.each(function(bh){var bg=b.queue(this,e,bf);if(e==="fx"&&bg[0]!=="inprogress"){b.dequeue(this,e)}})},dequeue:function(e){return this.each(function(){b.dequeue(this,e)})},delay:function(bf,e){bf=b.fx?b.fx.speeds[bf]||bf:bf;e=e||"fx";return this.queue(e,function(){var bg=this;setTimeout(function(){b.dequeue(bg,e)},bf)})},clearQueue:function(e){return this.queue(e||"fx",[])}});var aE=/[\n\t\r]/g,a5=/\s+/,aI=/\r/g,a4=/^(?:href|src|style)$/,g=/^(?:button|input)$/i,D=/^(?:button|input|object|select|textarea)$/i,l=/^a(?:rea)?$/i,R=/^(?:radio|checkbox)$/i;b.props={"for":"htmlFor","class":"className",readonly:"readOnly",maxlength:"maxLength",cellspacing:"cellSpacing",rowspan:"rowSpan",colspan:"colSpan",tabindex:"tabIndex",usemap:"useMap",frameborder:"frameBorder"};b.fn.extend({attr:function(e,bf){return b.access(this,e,bf,true,b.attr)},removeAttr:function(e,bf){return this.each(function(){b.attr(this,e,"");if(this.nodeType===1){this.removeAttribute(e)}})},addClass:function(bl){if(b.isFunction(bl)){return this.each(function(bo){var bn=b(this);bn.addClass(bl.call(this,bo,bn.attr("class")))})}if(bl&&typeof bl==="string"){var e=(bl||"").split(a5);for(var bh=0,bg=this.length;bh<bg;bh++){var bf=this[bh];if(bf.nodeType===1){if(!bf.className){bf.className=bl}else{var bi=" "+bf.className+" ",bk=bf.className;for(var bj=0,bm=e.length;bj<bm;bj++){if(bi.indexOf(" "+e[bj]+" ")<0){bk+=" "+e[bj]}}bf.className=b.trim(bk)}}}}return this},removeClass:function(bj){if(b.isFunction(bj)){return this.each(function(bn){var bm=b(this);bm.removeClass(bj.call(this,bn,bm.attr("class")))})}if((bj&&typeof bj==="string")||bj===I){var bk=(bj||"").split(a5);for(var bg=0,bf=this.length;bg<bf;bg++){var bi=this[bg];if(bi.nodeType===1&&bi.className){if(bj){var bh=(" "+bi.className+" ").replace(aE," ");for(var bl=0,e=bk.length;bl<e;bl++){bh=bh.replace(" "+bk[bl]+" "," ")}bi.className=b.trim(bh)}else{bi.className=""}}}}return this},toggleClass:function(bh,bf){var bg=typeof bh,e=typeof bf==="boolean";if(b.isFunction(bh)){return this.each(function(bj){var bi=b(this);bi.toggleClass(bh.call(this,bj,bi.attr("class"),bf),bf)})}return this.each(function(){if(bg==="string"){var bk,bj=0,bi=b(this),bl=bf,bm=bh.split(a5);while((bk=bm[bj++])){bl=e?bl:!bi.hasClass(bk);bi[bl?"addClass":"removeClass"](bk)}}else{if(bg==="undefined"||bg==="boolean"){if(this.className){b._data(this,"__className__",this.className)}this.className=this.className||bh===false?"":b._data(this,"__className__")||""}}})},hasClass:function(e){var bh=" "+e+" ";for(var bg=0,bf=this.length;bg<bf;bg++){if((" "+this[bg].className+" ").replace(aE," ").indexOf(bh)>-1){return true}}return false},val:function(bm){if(!arguments.length){var bg=this[0];if(bg){if(b.nodeName(bg,"option")){var bf=bg.attributes.value;return !bf||bf.specified?bg.value:bg.text}if(b.nodeName(bg,"select")){var bk=bg.selectedIndex,bn=[],bo=bg.options,bj=bg.type==="select-one";if(bk<0){return null}for(var bh=bj?bk:0,bl=bj?bk+1:bo.length;bh<bl;bh++){var bi=bo[bh];if(bi.selected&&(b.support.optDisabled?!bi.disabled:bi.getAttribute("disabled")===null)&&(!bi.parentNode.disabled||!b.nodeName(bi.parentNode,"optgroup"))){bm=b(bi).val();if(bj){return bm}bn.push(bm)}}if(bj&&!bn.length&&bo.length){return b(bo[bk]).val()}return bn}if(R.test(bg.type)&&!b.support.checkOn){return bg.getAttribute("value")===null?"on":bg.value}return(bg.value||"").replace(aI,"")}return I}var e=b.isFunction(bm);return this.each(function(br){var bq=b(this),bs=bm;if(this.nodeType!==1){return}if(e){bs=bm.call(this,br,bq.val())}if(bs==null){bs=""}else{if(typeof bs==="number"){bs+=""}else{if(b.isArray(bs)){bs=b.map(bs,function(bt){return bt==null?"":bt+""})}}}if(b.isArray(bs)&&R.test(this.type)){this.checked=b.inArray(bq.val(),bs)>=0}else{if(b.nodeName(this,"select")){var bp=b.makeArray(bs);b("option",this).each(function(){this.selected=b.inArray(b(this).val(),bp)>=0});if(!bp.length){this.selectedIndex=-1}}else{this.value=bs}}})}});b.extend({attrFn:{val:true,css:true,html:true,text:true,data:true,width:true,height:true,offset:true},attr:function(bf,e,bk,bn){if(!bf||bf.nodeType===3||bf.nodeType===8||bf.nodeType===2){return I}if(bn&&e in b.attrFn){return b(bf)[e](bk)}var bg=bf.nodeType!==1||!b.isXMLDoc(bf),bj=bk!==I;e=bg&&b.props[e]||e;if(bf.nodeType===1){var bi=a4.test(e);if(e==="selected"&&!b.support.optSelected){var bl=bf.parentNode;if(bl){bl.selectedIndex;if(bl.parentNode){bl.parentNode.selectedIndex}}}if((e in bf||bf[e]!==I)&&bg&&!bi){if(bj){if(e==="type"&&g.test(bf.nodeName)&&bf.parentNode){b.error("type property can't be changed")}if(bk===null){if(bf.nodeType===1){bf.removeAttribute(e)}}else{bf[e]=bk}}if(b.nodeName(bf,"form")&&bf.getAttributeNode(e)){return bf.getAttributeNode(e).nodeValue}if(e==="tabIndex"){var bm=bf.getAttributeNode("tabIndex");return bm&&bm.specified?bm.value:D.test(bf.nodeName)||l.test(bf.nodeName)&&bf.href?0:I}return bf[e]}if(!b.support.style&&bg&&e==="style"){if(bj){bf.style.cssText=""+bk}return bf.style.cssText}if(bj){bf.setAttribute(e,""+bk)}if(!bf.attributes[e]&&(bf.hasAttribute&&!bf.hasAttribute(e))){return I}var bh=!b.support.hrefNormalized&&bg&&bi?bf.getAttribute(e,2):bf.getAttribute(e);return bh===null?I:bh}if(bj){bf[e]=bk}return bf[e]}});var aR=/\.(.*)$/,a2=/^(?:textarea|input|select)$/i,L=/\./g,ab=/ /g,ax=/[^\w\s.|`]/g,F=function(e){return e.replace(ax,"\\$&")};b.event={add:function(bi,bm,bt,bk){if(bi.nodeType===3||bi.nodeType===8){return}try{if(b.isWindow(bi)&&(bi!==a0&&!bi.frameElement)){bi=a0}}catch(bn){}if(bt===false){bt=a7}else{if(!bt){return}}var bg,br;if(bt.handler){bg=bt;bt=bg.handler}if(!bt.guid){bt.guid=b.guid++}var bo=b._data(bi);if(!bo){return}var bs=bo.events,bl=bo.handle;if(!bs){bo.events=bs={}}if(!bl){bo.handle=bl=function(bu){return typeof b!=="undefined"&&b.event.triggered!==bu.type?b.event.handle.apply(bl.elem,arguments):I}}bl.elem=bi;bm=bm.split(" ");var bq,bj=0,bf;while((bq=bm[bj++])){br=bg?b.extend({},bg):{handler:bt,data:bk};if(bq.indexOf(".")>-1){bf=bq.split(".");bq=bf.shift();br.namespace=bf.slice(0).sort().join(".")}else{bf=[];br.namespace=""}br.type=bq;if(!br.guid){br.guid=bt.guid}var bh=bs[bq],bp=b.event.special[bq]||{};if(!bh){bh=bs[bq]=[];if(!bp.setup||bp.setup.call(bi,bk,bf,bl)===false){if(bi.addEventListener){bi.addEventListener(bq,bl,false)}else{if(bi.attachEvent){bi.attachEvent("on"+bq,bl)}}}}if(bp.add){bp.add.call(bi,br);if(!br.handler.guid){br.handler.guid=bt.guid}}bh.push(br);b.event.global[bq]=true}bi=null},global:{},remove:function(bt,bo,bg,bk){if(bt.nodeType===3||bt.nodeType===8){return}if(bg===false){bg=a7}var bw,bj,bl,bq,br=0,bh,bm,bp,bi,bn,e,bv,bs=b.hasData(bt)&&b._data(bt),bf=bs&&bs.events;if(!bs||!bf){return}if(bo&&bo.type){bg=bo.handler;bo=bo.type}if(!bo||typeof bo==="string"&&bo.charAt(0)==="."){bo=bo||"";for(bj in bf){b.event.remove(bt,bj+bo)}return}bo=bo.split(" ");while((bj=bo[br++])){bv=bj;e=null;bh=bj.indexOf(".")<0;bm=[];if(!bh){bm=bj.split(".");bj=bm.shift();bp=new RegExp("(^|\\.)"+b.map(bm.slice(0).sort(),F).join("\\.(?:.*\\.)?")+"(\\.|$)")}bn=bf[bj];if(!bn){continue}if(!bg){for(bq=0;bq<bn.length;bq++){e=bn[bq];if(bh||bp.test(e.namespace)){b.event.remove(bt,bv,e.handler,bq);bn.splice(bq--,1)}}continue}bi=b.event.special[bj]||{};for(bq=bk||0;bq<bn.length;bq++){e=bn[bq];if(bg.guid===e.guid){if(bh||bp.test(e.namespace)){if(bk==null){bn.splice(bq--,1)}if(bi.remove){bi.remove.call(bt,e)}}if(bk!=null){break}}}if(bn.length===0||bk!=null&&bn.length===1){if(!bi.teardown||bi.teardown.call(bt,bm)===false){b.removeEvent(bt,bj,bs.handle)}bw=null;delete bf[bj]}}if(b.isEmptyObject(bf)){var bu=bs.handle;if(bu){bu.elem=null}delete bs.events;delete bs.handle;if(b.isEmptyObject(bs)){b.removeData(bt,I,true)}}},trigger:function(bf,bk,bh){var bo=bf.type||bf,bj=arguments[3];if(!bj){bf=typeof bf==="object"?bf[b.expando]?bf:b.extend(b.Event(bo),bf):b.Event(bo);if(bo.indexOf("!")>=0){bf.type=bo=bo.slice(0,-1);bf.exclusive=true}if(!bh){bf.stopPropagation();if(b.event.global[bo]){b.each(b.cache,function(){var bt=b.expando,bs=this[bt];if(bs&&bs.events&&bs.events[bo]){b.event.trigger(bf,bk,bs.handle.elem)}})}}if(!bh||bh.nodeType===3||bh.nodeType===8){return I}bf.result=I;bf.target=bh;bk=b.makeArray(bk);bk.unshift(bf)}bf.currentTarget=bh;var bl=b._data(bh,"handle");if(bl){bl.apply(bh,bk)}var bq=bh.parentNode||bh.ownerDocument;try{if(!(bh&&bh.nodeName&&b.noData[bh.nodeName.toLowerCase()])){if(bh["on"+bo]&&bh["on"+bo].apply(bh,bk)===false){bf.result=false;bf.preventDefault()}}}catch(bp){}if(!bf.isPropagationStopped()&&bq){b.event.trigger(bf,bk,bq,true)}else{if(!bf.isDefaultPrevented()){var bg,bm=bf.target,e=bo.replace(aR,""),br=b.nodeName(bm,"a")&&e==="click",bn=b.event.special[e]||{};if((!bn._default||bn._default.call(bh,bf)===false)&&!br&&!(bm&&bm.nodeName&&b.noData[bm.nodeName.toLowerCase()])){try{if(bm[e]){bg=bm["on"+e];if(bg){bm["on"+e]=null}b.event.triggered=bf.type;bm[e]()}}catch(bi){}if(bg){bm["on"+e]=bg}b.event.triggered=I}}}},handle:function(e){var bn,bg,bf,bp,bo,bj=[],bl=b.makeArray(arguments);e=bl[0]=b.event.fix(e||a0.event);e.currentTarget=this;bn=e.type.indexOf(".")<0&&!e.exclusive;if(!bn){bf=e.type.split(".");e.type=bf.shift();bj=bf.slice(0).sort();bp=new RegExp("(^|\\.)"+bj.join("\\.(?:.*\\.)?")+"(\\.|$)")}e.namespace=e.namespace||bj.join(".");bo=b._data(this,"events");bg=(bo||{})[e.type];if(bo&&bg){bg=bg.slice(0);for(var bi=0,bh=bg.length;bi<bh;bi++){var bm=bg[bi];if(bn||bp.test(bm.namespace)){e.handler=bm.handler;e.data=bm.data;e.handleObj=bm;var bk=bm.handler.apply(this,bl);if(bk!==I){e.result=bk;if(bk===false){e.preventDefault();e.stopPropagation()}}if(e.isImmediatePropagationStopped()){break}}}}return e.result},props:"altKey attrChange attrName bubbles button cancelable charCode clientX clientY ctrlKey currentTarget data detail eventPhase fromElement handler keyCode layerX layerY metaKey newValue offsetX offsetY pageX pageY prevValue relatedNode relatedTarget screenX screenY shiftKey srcElement target toElement view wheelDelta which".split(" "),fix:function(bh){if(bh[b.expando]){return bh}var bf=bh;bh=b.Event(bf);for(var bg=this.props.length,bj;bg;){bj=this.props[--bg];bh[bj]=bf[bj]}if(!bh.target){bh.target=bh.srcElement||am}if(bh.target.nodeType===3){bh.target=bh.target.parentNode}if(!bh.relatedTarget&&bh.fromElement){bh.relatedTarget=bh.fromElement===bh.target?bh.toElement:bh.fromElement}if(bh.pageX==null&&bh.clientX!=null){var bi=am.documentElement,e=am.body;bh.pageX=bh.clientX+(bi&&bi.scrollLeft||e&&e.scrollLeft||0)-(bi&&bi.clientLeft||e&&e.clientLeft||0);bh.pageY=bh.clientY+(bi&&bi.scrollTop||e&&e.scrollTop||0)-(bi&&bi.clientTop||e&&e.clientTop||0)}if(bh.which==null&&(bh.charCode!=null||bh.keyCode!=null)){bh.which=bh.charCode!=null?bh.charCode:bh.keyCode}if(!bh.metaKey&&bh.ctrlKey){bh.metaKey=bh.ctrlKey}if(!bh.which&&bh.button!==I){bh.which=(bh.button&1?1:(bh.button&2?3:(bh.button&4?2:0)))}return bh},guid:100000000,proxy:b.proxy,special:{ready:{setup:b.bindReady,teardown:b.noop},live:{add:function(e){b.event.add(this,o(e.origType,e.selector),b.extend({},e,{handler:ag,guid:e.handler.guid}))},remove:function(e){b.event.remove(this,o(e.origType,e.selector),e)}},beforeunload:{setup:function(bg,bf,e){if(b.isWindow(this)){this.onbeforeunload=e}},teardown:function(bf,e){if(this.onbeforeunload===e){this.onbeforeunload=null}}}}};b.removeEvent=am.removeEventListener?function(bf,e,bg){if(bf.removeEventListener){bf.removeEventListener(e,bg,false)}}:function(bf,e,bg){if(bf.detachEvent){bf.detachEvent("on"+e,bg)}};b.Event=function(e){if(!this.preventDefault){return new b.Event(e)}if(e&&e.type){this.originalEvent=e;this.type=e.type;this.isDefaultPrevented=(e.defaultPrevented||e.returnValue===false||e.getPreventDefault&&e.getPreventDefault())?i:a7}else{this.type=e}this.timeStamp=b.now();this[b.expando]=true};function a7(){return false}function i(){return true}b.Event.prototype={preventDefault:function(){this.isDefaultPrevented=i;var bf=this.originalEvent;if(!bf){return}if(bf.preventDefault){bf.preventDefault()}else{bf.returnValue=false}},stopPropagation:function(){this.isPropagationStopped=i;var bf=this.originalEvent;if(!bf){return}if(bf.stopPropagation){bf.stopPropagation()}bf.cancelBubble=true},stopImmediatePropagation:function(){this.isImmediatePropagationStopped=i;this.stopPropagation()},isDefaultPrevented:a7,isPropagationStopped:a7,isImmediatePropagationStopped:a7};var aa=function(bg){var bf=bg.relatedTarget;try{if(bf&&bf!==am&&!bf.parentNode){return}while(bf&&bf!==this){bf=bf.parentNode}if(bf!==this){bg.type=bg.data;b.event.handle.apply(this,arguments)}}catch(bh){}},aM=function(e){e.type=e.data;b.event.handle.apply(this,arguments)};b.each({mouseenter:"mouseover",mouseleave:"mouseout"},function(bf,e){b.event.special[bf]={setup:function(bg){b.event.add(this,e,bg&&bg.selector?aM:aa,bf)},teardown:function(bg){b.event.remove(this,e,bg&&bg.selector?aM:aa)}}});if(!b.support.submitBubbles){b.event.special.submit={setup:function(bf,e){if(this.nodeName&&this.nodeName.toLowerCase()!=="form"){b.event.add(this,"click.specialSubmit",function(bi){var bh=bi.target,bg=bh.type;if((bg==="submit"||bg==="image")&&b(bh).closest("form").length){aP("submit",this,arguments)}});b.event.add(this,"keypress.specialSubmit",function(bi){var bh=bi.target,bg=bh.type;if((bg==="text"||bg==="password")&&b(bh).closest("form").length&&bi.keyCode===13){aP("submit",this,arguments)}})}else{return false}},teardown:function(e){b.event.remove(this,".specialSubmit")}}}if(!b.support.changeBubbles){var a8,k=function(bf){var e=bf.type,bg=bf.value;if(e==="radio"||e==="checkbox"){bg=bf.checked}else{if(e==="select-multiple"){bg=bf.selectedIndex>-1?b.map(bf.options,function(bh){return bh.selected}).join("-"):""}else{if(bf.nodeName.toLowerCase()==="select"){bg=bf.selectedIndex}}}return bg},Y=function Y(bh){var bf=bh.target,bg,bi;if(!a2.test(bf.nodeName)||bf.readOnly){return}bg=b._data(bf,"_change_data");bi=k(bf);if(bh.type!=="focusout"||bf.type!=="radio"){b._data(bf,"_change_data",bi)}if(bg===I||bi===bg){return}if(bg!=null||bi){bh.type="change";bh.liveFired=I;b.event.trigger(bh,arguments[1],bf)}};b.event.special.change={filters:{focusout:Y,beforedeactivate:Y,click:function(bh){var bg=bh.target,bf=bg.type;if(bf==="radio"||bf==="checkbox"||bg.nodeName.toLowerCase()==="select"){Y.call(this,bh)}},keydown:function(bh){var bg=bh.target,bf=bg.type;if((bh.keyCode===13&&bg.nodeName.toLowerCase()!=="textarea")||(bh.keyCode===32&&(bf==="checkbox"||bf==="radio"))||bf==="select-multiple"){Y.call(this,bh)}},beforeactivate:function(bg){var bf=bg.target;b._data(bf,"_change_data",k(bf))}},setup:function(bg,bf){if(this.type==="file"){return false}for(var e in a8){b.event.add(this,e+".specialChange",a8[e])}return a2.test(this.nodeName)},teardown:function(e){b.event.remove(this,".specialChange");return a2.test(this.nodeName)}};a8=b.event.special.change.filters;a8.focus=a8.beforeactivate}function aP(bf,bh,e){var bg=b.extend({},e[0]);bg.type=bf;bg.originalEvent={};bg.liveFired=I;b.event.handle.call(bh,bg);if(bg.isDefaultPrevented()){e[0].preventDefault()}}if(am.addEventListener){b.each({focus:"focusin",blur:"focusout"},function(bh,e){var bf=0;b.event.special[e]={setup:function(){if(bf++===0){am.addEventListener(bh,bg,true)}},teardown:function(){if(--bf===0){am.removeEventListener(bh,bg,true)}}};function bg(bi){var bj=b.event.fix(bi);bj.type=e;bj.originalEvent={};b.event.trigger(bj,null,bj.target);if(bj.isDefaultPrevented()){bi.preventDefault()}}})}b.each(["bind","one"],function(bf,e){b.fn[e]=function(bl,bm,bk){if(typeof bl==="object"){for(var bi in bl){this[e](bi,bm,bl[bi],bk)}return this}if(b.isFunction(bm)||bm===false){bk=bm;bm=I}var bj=e==="one"?b.proxy(bk,function(bn){b(this).unbind(bn,bj);return bk.apply(this,arguments)}):bk;if(bl==="unload"&&e!=="one"){this.one(bl,bm,bk)}else{for(var bh=0,bg=this.length;bh<bg;bh++){b.event.add(this[bh],bl,bj,bm)}}return this}});b.fn.extend({unbind:function(bi,bh){if(typeof bi==="object"&&!bi.preventDefault){for(var bg in bi){this.unbind(bg,bi[bg])}}else{for(var bf=0,e=this.length;bf<e;bf++){b.event.remove(this[bf],bi,bh)}}return this},delegate:function(e,bf,bh,bg){return this.live(bf,bh,bg,e)},undelegate:function(e,bf,bg){if(arguments.length===0){return this.unbind("live")}else{return this.die(bf,null,bg,e)}},trigger:function(e,bf){return this.each(function(){b.event.trigger(e,bf,this)})},triggerHandler:function(e,bg){if(this[0]){var bf=b.Event(e);bf.preventDefault();bf.stopPropagation();b.event.trigger(bf,bg,this[0]);return bf.result}},toggle:function(bg){var e=arguments,bf=1;while(bf<e.length){b.proxy(bg,e[bf++])}return this.click(b.proxy(bg,function(bh){var bi=(b._data(this,"lastToggle"+bg.guid)||0)%bf;b._data(this,"lastToggle"+bg.guid,bi+1);bh.preventDefault();return e[bi].apply(this,arguments)||false}))},hover:function(e,bf){return this.mouseenter(e).mouseleave(bf||e)}});var aJ={focus:"focusin",blur:"focusout",mouseenter:"mouseover",mouseleave:"mouseout"};b.each(["live","die"],function(bf,e){b.fn[e]=function(bp,bm,br,bi){var bq,bn=0,bo,bh,bt,bk=bi||this.selector,bg=bi?this:b(this.context);if(typeof bp==="object"&&!bp.preventDefault){for(var bs in bp){bg[e](bs,bm,bp[bs],bk)}return this}if(b.isFunction(bm)){br=bm;bm=I}bp=(bp||"").split(" ");while((bq=bp[bn++])!=null){bo=aR.exec(bq);bh="";if(bo){bh=bo[0];bq=bq.replace(aR,"")}if(bq==="hover"){bp.push("mouseenter"+bh,"mouseleave"+bh);continue}bt=bq;if(bq==="focus"||bq==="blur"){bp.push(aJ[bq]+bh);bq=bq+bh}else{bq=(aJ[bq]||bq)+bh}if(e==="live"){for(var bl=0,bj=bg.length;bl<bj;bl++){b.event.add(bg[bl],"live."+o(bq,bk),{data:bm,selector:bk,handler:br,origType:bq,origHandler:br,preType:bt})}}else{bg.unbind("live."+o(bq,bk),br)}}return this}});function ag(bp){var bm,bh,bv,bj,e,br,bo,bq,bn,bu,bl,bk,bt,bs=[],bi=[],bf=b._data(this,"events");if(bp.liveFired===this||!bf||!bf.live||bp.target.disabled||bp.button&&bp.type==="click"){return}if(bp.namespace){bk=new RegExp("(^|\\.)"+bp.namespace.split(".").join("\\.(?:.*\\.)?")+"(\\.|$)")}bp.liveFired=this;var bg=bf.live.slice(0);for(bo=0;bo<bg.length;bo++){e=bg[bo];if(e.origType.replace(aR,"")===bp.type){bi.push(e.selector)}else{bg.splice(bo--,1)}}bj=b(bp.target).closest(bi,bp.currentTarget);for(bq=0,bn=bj.length;bq<bn;bq++){bl=bj[bq];for(bo=0;bo<bg.length;bo++){e=bg[bo];if(bl.selector===e.selector&&(!bk||bk.test(e.namespace))&&!bl.elem.disabled){br=bl.elem;bv=null;if(e.preType==="mouseenter"||e.preType==="mouseleave"){bp.type=e.preType;bv=b(bp.relatedTarget).closest(e.selector)[0]}if(!bv||bv!==br){bs.push({elem:br,handleObj:e,level:bl.level})}}}}for(bq=0,bn=bs.length;bq<bn;bq++){bj=bs[bq];if(bh&&bj.level>bh){break}bp.currentTarget=bj.elem;bp.data=bj.handleObj.data;bp.handleObj=bj.handleObj;bt=bj.handleObj.origHandler.apply(bj.elem,arguments);if(bt===false||bp.isPropagationStopped()){bh=bj.level;if(bt===false){bm=false}if(bp.isImmediatePropagationStopped()){break}}}return bm}function o(bf,e){return(bf&&bf!=="*"?bf+".":"")+e.replace(L,"`").replace(ab,"&")}b.each(("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error").split(" "),function(bf,e){b.fn[e]=function(bh,bg){if(bg==null){bg=bh;bh=null}return arguments.length>0?this.bind(e,bh,bg):this.trigger(e)};if(b.attrFn){b.attrFn[e]=true}});
/**/
(function(){var bp=/((?:\((?:\([^()]+\)|[^()]+)+\)|\[(?:\[[^\[\]]*\]|['"][^'"]*['"]|[^\[\]'"]+)+\]|\\.|[^ >+~,(\[\\]+)+|[>+~])(\s*,\s*)?((?:.|\r|\n)*)/g,bq=0,bt=Object.prototype.toString,bk=false,bj=true,br=/\\/g,bx=/\W/;[0,0].sort(function(){bj=false;return 0});var bh=function(bC,e,bF,bG){bF=bF||[];e=e||am;var bI=e;if(e.nodeType!==1&&e.nodeType!==9){return[]}if(!bC||typeof bC!=="string"){return bF}var bz,bK,bN,by,bJ,bM,bL,bE,bB=true,bA=bh.isXML(e),bD=[],bH=bC;do{bp.exec("");bz=bp.exec(bH);if(bz){bH=bz[3];bD.push(bz[1]);if(bz[2]){by=bz[3];break}}}while(bz);if(bD.length>1&&bl.exec(bC)){if(bD.length===2&&bm.relative[bD[0]]){bK=bu(bD[0]+bD[1],e)}else{bK=bm.relative[bD[0]]?[e]:bh(bD.shift(),e);while(bD.length){bC=bD.shift();if(bm.relative[bC]){bC+=bD.shift()}bK=bu(bC,bK)}}}else{if(!bG&&bD.length>1&&e.nodeType===9&&!bA&&bm.match.ID.test(bD[0])&&!bm.match.ID.test(bD[bD.length-1])){bJ=bh.find(bD.shift(),e,bA);e=bJ.expr?bh.filter(bJ.expr,bJ.set)[0]:bJ.set[0]}if(e){bJ=bG?{expr:bD.pop(),set:bn(bG)}:bh.find(bD.pop(),bD.length===1&&(bD[0]==="~"||bD[0]==="+")&&e.parentNode?e.parentNode:e,bA);bK=bJ.expr?bh.filter(bJ.expr,bJ.set):bJ.set;if(bD.length>0){bN=bn(bK)}else{bB=false}while(bD.length){bM=bD.pop();bL=bM;if(!bm.relative[bM]){bM=""}else{bL=bD.pop()}if(bL==null){bL=e}bm.relative[bM](bN,bL,bA)}}else{bN=bD=[]}}if(!bN){bN=bK}if(!bN){bh.error(bM||bC)}if(bt.call(bN)==="[object Array]"){if(!bB){bF.push.apply(bF,bN)}else{if(e&&e.nodeType===1){for(bE=0;bN[bE]!=null;bE++){if(bN[bE]&&(bN[bE]===true||bN[bE].nodeType===1&&bh.contains(e,bN[bE]))){bF.push(bK[bE])}}}else{for(bE=0;bN[bE]!=null;bE++){if(bN[bE]&&bN[bE].nodeType===1){bF.push(bK[bE])}}}}}else{bn(bN,bF)}if(by){bh(by,bI,bF,bG);bh.uniqueSort(bF)}return bF};bh.uniqueSort=function(by){if(bs){bk=bj;by.sort(bs);if(bk){for(var e=1;e<by.length;e++){if(by[e]===by[e-1]){by.splice(e--,1)}}}}return by};bh.matches=function(e,by){return bh(e,null,null,by)};bh.matchesSelector=function(e,by){return bh(by,null,null,[e]).length>0};bh.find=function(bE,e,bF){var bD;if(!bE){return[]}for(var bA=0,bz=bm.order.length;bA<bz;bA++){var bB,bC=bm.order[bA];if((bB=bm.leftMatch[bC].exec(bE))){var by=bB[1];bB.splice(1,1);if(by.substr(by.length-1)!=="\\"){bB[1]=(bB[1]||"").replace(br,"");bD=bm.find[bC](bB,e,bF);if(bD!=null){bE=bE.replace(bm.match[bC],"");break}}}}if(!bD){bD=typeof e.getElementsByTagName!=="undefined"?e.getElementsByTagName("*"):[]}return{set:bD,expr:bE}};bh.filter=function(bI,bH,bL,bB){var bD,e,bz=bI,bN=[],bF=bH,bE=bH&&bH[0]&&bh.isXML(bH[0]);while(bI&&bH.length){for(var bG in bm.filter){if((bD=bm.leftMatch[bG].exec(bI))!=null&&bD[2]){var bM,bK,by=bm.filter[bG],bA=bD[1];e=false;bD.splice(1,1);if(bA.substr(bA.length-1)==="\\"){continue}if(bF===bN){bN=[]}if(bm.preFilter[bG]){bD=bm.preFilter[bG](bD,bF,bL,bN,bB,bE);if(!bD){e=bM=true}else{if(bD===true){continue}}}if(bD){for(var bC=0;(bK=bF[bC])!=null;bC++){if(bK){bM=by(bK,bD,bC,bF);var bJ=bB^!!bM;if(bL&&bM!=null){if(bJ){e=true}else{bF[bC]=false}}else{if(bJ){bN.push(bK);e=true}}}}}if(bM!==I){if(!bL){bF=bN}bI=bI.replace(bm.match[bG],"");if(!e){return[]}break}}}if(bI===bz){if(e==null){bh.error(bI)}else{break}}bz=bI}return bF};bh.error=function(e){throw"Syntax error, unrecognized expression: "+e};var bm=bh.selectors={order:["ID","NAME","TAG"],match:{ID:/#((?:[\w\u00c0-\uFFFF\-]|\\.)+)/,CLASS:/\.((?:[\w\u00c0-\uFFFF\-]|\\.)+)/,NAME:/\[name=['"]*((?:[\w\u00c0-\uFFFF\-]|\\.)+)['"]*\]/,ATTR:/\[\s*((?:[\w\u00c0-\uFFFF\-]|\\.)+)\s*(?:(\S?=)\s*(?:(['"])(.*?)\3|(#?(?:[\w\u00c0-\uFFFF\-]|\\.)*)|)|)\s*\]/,TAG:/^((?:[\w\u00c0-\uFFFF\*\-]|\\.)+)/,CHILD:/:(only|nth|last|first)-child(?:\(\s*(even|odd|(?:[+\-]?\d+|(?:[+\-]?\d*)?n\s*(?:[+\-]\s*\d+)?))\s*\))?/,POS:/:(nth|eq|gt|lt|first|last|even|odd)(?:\((\d*)\))?(?=[^\-]|$)/,PSEUDO:/:((?:[\w\u00c0-\uFFFF\-]|\\.)+)(?:\((['"]?)((?:\([^\)]+\)|[^\(\)]*)+)\2\))?/},leftMatch:{},attrMap:{"class":"className","for":"htmlFor"},attrHandle:{href:function(e){return e.getAttribute("href")},type:function(e){return e.getAttribute("type")}},relative:{"+":function(bD,by){var bA=typeof by==="string",bC=bA&&!bx.test(by),bE=bA&&!bC;if(bC){by=by.toLowerCase()}for(var bz=0,e=bD.length,bB;bz<e;bz++){if((bB=bD[bz])){while((bB=bB.previousSibling)&&bB.nodeType!==1){}bD[bz]=bE||bB&&bB.nodeName.toLowerCase()===by?bB||false:bB===by}}if(bE){bh.filter(by,bD,true)}},">":function(bD,by){var bC,bB=typeof by==="string",bz=0,e=bD.length;if(bB&&!bx.test(by)){by=by.toLowerCase();for(;bz<e;bz++){bC=bD[bz];if(bC){var bA=bC.parentNode;bD[bz]=bA.nodeName.toLowerCase()===by?bA:false}}}else{for(;bz<e;bz++){bC=bD[bz];if(bC){bD[bz]=bB?bC.parentNode:bC.parentNode===by}}if(bB){bh.filter(by,bD,true)}}},"":function(bA,by,bC){var bB,bz=bq++,e=bv;if(typeof by==="string"&&!bx.test(by)){by=by.toLowerCase();bB=by;e=bf}e("parentNode",by,bz,bA,bB,bC)},"~":function(bA,by,bC){var bB,bz=bq++,e=bv;if(typeof by==="string"&&!bx.test(by)){by=by.toLowerCase();bB=by;e=bf}e("previousSibling",by,bz,bA,bB,bC)}},find:{ID:function(by,bz,bA){if(typeof bz.getElementById!=="undefined"&&!bA){var e=bz.getElementById(by[1]);return e&&e.parentNode?[e]:[]}},NAME:function(bz,bC){if(typeof bC.getElementsByName!=="undefined"){var by=[],bB=bC.getElementsByName(bz[1]);for(var bA=0,e=bB.length;bA<e;bA++){if(bB[bA].getAttribute("name")===bz[1]){by.push(bB[bA])}}return by.length===0?null:by}},TAG:function(e,by){if(typeof by.getElementsByTagName!=="undefined"){return by.getElementsByTagName(e[1])}}},preFilter:{CLASS:function(bA,by,bz,e,bD,bE){bA=" "+bA[1].replace(br,"")+" ";if(bE){return bA}for(var bB=0,bC;(bC=by[bB])!=null;bB++){if(bC){if(bD^(bC.className&&(" "+bC.className+" ").replace(/[\t\n\r]/g," ").indexOf(bA)>=0)){if(!bz){e.push(bC)}}else{if(bz){by[bB]=false}}}}return false},ID:function(e){return e[1].replace(br,"")},TAG:function(by,e){return by[1].replace(br,"").toLowerCase()},CHILD:function(e){if(e[1]==="nth"){if(!e[2]){bh.error(e[0])}e[2]=e[2].replace(/^\+|\s*/g,"");var by=/(-?)(\d*)(?:n([+\-]?\d*))?/.exec(e[2]==="even"&&"2n"||e[2]==="odd"&&"2n+1"||!/\D/.test(e[2])&&"0n+"+e[2]||e[2]);e[2]=(by[1]+(by[2]||1))-0;e[3]=by[3]-0}else{if(e[2]){bh.error(e[0])}}e[0]=bq++;return e},ATTR:function(bB,by,bz,e,bC,bD){var bA=bB[1]=bB[1].replace(br,"");if(!bD&&bm.attrMap[bA]){bB[1]=bm.attrMap[bA]}bB[4]=(bB[4]||bB[5]||"").replace(br,"");if(bB[2]==="~="){bB[4]=" "+bB[4]+" "}return bB},PSEUDO:function(bB,by,bz,e,bC){if(bB[1]==="not"){if((bp.exec(bB[3])||"").length>1||/^\w/.test(bB[3])){bB[3]=bh(bB[3],null,null,by)}else{var bA=bh.filter(bB[3],by,bz,true^bC);if(!bz){e.push.apply(e,bA)}return false}}else{if(bm.match.POS.test(bB[0])||bm.match.CHILD.test(bB[0])){return true}}return bB},POS:function(e){e.unshift(true);return e}},filters:{enabled:function(e){return e.disabled===false&&e.type!=="hidden"},disabled:function(e){return e.disabled===true},checked:function(e){return e.checked===true},selected:function(e){if(e.parentNode){e.parentNode.selectedIndex}return e.selected===true},parent:function(e){return !!e.firstChild},empty:function(e){return !e.firstChild},has:function(bz,by,e){return !!bh(e[3],bz).length},header:function(e){return(/h\d/i).test(e.nodeName)},text:function(bz){var e=bz.getAttribute("type"),by=bz.type;return"text"===by&&(e===by||e===null)},radio:function(e){return"radio"===e.type},checkbox:function(e){return"checkbox"===e.type},file:function(e){return"file"===e.type},password:function(e){return"password"===e.type},submit:function(e){return"submit"===e.type},image:function(e){return"image"===e.type},reset:function(e){return"reset"===e.type},button:function(e){return"button"===e.type||e.nodeName.toLowerCase()==="button"},input:function(e){return(/input|select|textarea|button/i).test(e.nodeName)}},setFilters:{first:function(by,e){return e===0},last:function(bz,by,e,bA){return by===bA.length-1},even:function(by,e){return e%2===0},odd:function(by,e){return e%2===1},lt:function(bz,by,e){return by<e[3]-0},gt:function(bz,by,e){return by>e[3]-0},nth:function(bz,by,e){return e[3]-0===by},eq:function(bz,by,e){return e[3]-0===by}},filter:{PSEUDO:function(bz,bE,bD,bF){var e=bE[1],by=bm.filters[e];if(by){return by(bz,bD,bE,bF)}else{if(e==="contains"){return(bz.textContent||bz.innerText||bh.getText([bz])||"").indexOf(bE[3])>=0}else{if(e==="not"){var bA=bE[3];for(var bC=0,bB=bA.length;bC<bB;bC++){if(bA[bC]===bz){return false}}return true}else{bh.error(e)}}}},CHILD:function(e,bA){var bD=bA[1],by=e;switch(bD){case"only":case"first":while((by=by.previousSibling)){if(by.nodeType===1){return false}}if(bD==="first"){return true}by=e;case"last":while((by=by.nextSibling)){if(by.nodeType===1){return false}}return true;case"nth":var bz=bA[2],bG=bA[3];if(bz===1&&bG===0){return true}var bC=bA[0],bF=e.parentNode;if(bF&&(bF.sizcache!==bC||!e.nodeIndex)){var bB=0;for(by=bF.firstChild;by;by=by.nextSibling){if(by.nodeType===1){by.nodeIndex=++bB}}bF.sizcache=bC}var bE=e.nodeIndex-bG;if(bz===0){return bE===0}else{return(bE%bz===0&&bE/bz>=0)}}},ID:function(by,e){return by.nodeType===1&&by.getAttribute("id")===e},TAG:function(by,e){return(e==="*"&&by.nodeType===1)||by.nodeName.toLowerCase()===e},CLASS:function(by,e){return(" "+(by.className||by.getAttribute("class"))+" ").indexOf(e)>-1},ATTR:function(bC,bA){var bz=bA[1],e=bm.attrHandle[bz]?bm.attrHandle[bz](bC):bC[bz]!=null?bC[bz]:bC.getAttribute(bz),bD=e+"",bB=bA[2],by=bA[4];return e==null?bB==="!=":bB==="="?bD===by:bB==="*="?bD.indexOf(by)>=0:bB==="~="?(" "+bD+" ").indexOf(by)>=0:!by?bD&&e!==false:bB==="!="?bD!==by:bB==="^="?bD.indexOf(by)===0:bB==="$="?bD.substr(bD.length-by.length)===by:bB==="|="?bD===by||bD.substr(0,by.length+1)===by+"-":false},POS:function(bB,by,bz,bC){var e=by[2],bA=bm.setFilters[e];if(bA){return bA(bB,bz,by,bC)}}}};var bl=bm.match.POS,bg=function(by,e){return"\\"+(e-0+1)};for(var bi in bm.match){bm.match[bi]=new RegExp(bm.match[bi].source+(/(?![^\[]*\])(?![^\(]*\))/.source));bm.leftMatch[bi]=new RegExp(/(^(?:.|\r|\n)*?)/.source+bm.match[bi].source.replace(/\\(\d+)/g,bg))}var bn=function(by,e){by=Array.prototype.slice.call(by,0);if(e){e.push.apply(e,by);return e}return by};try{Array.prototype.slice.call(am.documentElement.childNodes,0)[0].nodeType}catch(bw){bn=function(bB,bA){var bz=0,by=bA||[];if(bt.call(bB)==="[object Array]"){Array.prototype.push.apply(by,bB)}else{if(typeof bB.length==="number"){for(var e=bB.length;bz<e;bz++){by.push(bB[bz])}}else{for(;bB[bz];bz++){by.push(bB[bz])}}}return by}}var bs,bo;if(am.documentElement.compareDocumentPosition){bs=function(by,e){if(by===e){bk=true;return 0}if(!by.compareDocumentPosition||!e.compareDocumentPosition){return by.compareDocumentPosition?-1:1}return by.compareDocumentPosition(e)&4?-1:1}}else{bs=function(bF,bE){var bC,by,bz=[],e=[],bB=bF.parentNode,bD=bE.parentNode,bG=bB;if(bF===bE){bk=true;return 0}else{if(bB===bD){return bo(bF,bE)}else{if(!bB){return -1}else{if(!bD){return 1}}}}while(bG){bz.unshift(bG);bG=bG.parentNode}bG=bD;while(bG){e.unshift(bG);bG=bG.parentNode}bC=bz.length;by=e.length;for(var bA=0;bA<bC&&bA<by;bA++){if(bz[bA]!==e[bA]){return bo(bz[bA],e[bA])}}return bA===bC?bo(bF,e[bA],-1):bo(bz[bA],bE,1)};bo=function(by,e,bz){if(by===e){return bz}var bA=by.nextSibling;while(bA){if(bA===e){return -1}bA=bA.nextSibling}return 1}}bh.getText=function(e){var by="",bA;for(var bz=0;e[bz];bz++){bA=e[bz];if(bA.nodeType===3||bA.nodeType===4){by+=bA.nodeValue}else{if(bA.nodeType!==8){by+=bh.getText(bA.childNodes)}}}return by};(function(){var by=am.createElement("div"),bz="script"+(new Date()).getTime(),e=am.documentElement;by.innerHTML="<a name='"+bz+"'/>";e.insertBefore(by,e.firstChild);if(am.getElementById(bz)){bm.find.ID=function(bB,bC,bD){if(typeof bC.getElementById!=="undefined"&&!bD){var bA=bC.getElementById(bB[1]);return bA?bA.id===bB[1]||typeof bA.getAttributeNode!=="undefined"&&bA.getAttributeNode("id").nodeValue===bB[1]?[bA]:I:[]}};bm.filter.ID=function(bC,bA){var bB=typeof bC.getAttributeNode!=="undefined"&&bC.getAttributeNode("id");return bC.nodeType===1&&bB&&bB.nodeValue===bA}}e.removeChild(by);e=by=null})();(function(){var e=am.createElement("div");e.appendChild(am.createComment(""));if(e.getElementsByTagName("*").length>0){bm.find.TAG=function(by,bC){var bB=bC.getElementsByTagName(by[1]);if(by[1]==="*"){var bA=[];for(var bz=0;bB[bz];bz++){if(bB[bz].nodeType===1){bA.push(bB[bz])}}bB=bA}return bB}}e.innerHTML="<a href='#'></a>";if(e.firstChild&&typeof e.firstChild.getAttribute!=="undefined"&&e.firstChild.getAttribute("href")!=="#"){bm.attrHandle.href=function(by){return by.getAttribute("href",2)}}e=null})();if(am.querySelectorAll){(function(){var e=bh,bA=am.createElement("div"),bz="__sizzle__";bA.innerHTML="<p class='TEST'></p>";if(bA.querySelectorAll&&bA.querySelectorAll(".TEST").length===0){return}bh=function(bL,bC,bG,bK){bC=bC||am;if(!bK&&!bh.isXML(bC)){var bJ=/^(\w+$)|^\.([\w\-]+$)|^#([\w\-]+$)/.exec(bL);if(bJ&&(bC.nodeType===1||bC.nodeType===9)){if(bJ[1]){return bn(bC.getElementsByTagName(bL),bG)}else{if(bJ[2]&&bm.find.CLASS&&bC.getElementsByClassName){return bn(bC.getElementsByClassName(bJ[2]),bG)}}}if(bC.nodeType===9){if(bL==="body"&&bC.body){return bn([bC.body],bG)}else{if(bJ&&bJ[3]){var bF=bC.getElementById(bJ[3]);if(bF&&bF.parentNode){if(bF.id===bJ[3]){return bn([bF],bG)}}else{return bn([],bG)}}}try{return bn(bC.querySelectorAll(bL),bG)}catch(bH){}}else{if(bC.nodeType===1&&bC.nodeName.toLowerCase()!=="object"){var bD=bC,bE=bC.getAttribute("id"),bB=bE||bz,bN=bC.parentNode,bM=/^\s*[+~]/.test(bL);if(!bE){bC.setAttribute("id",bB)}else{bB=bB.replace(/'/g,"\\$&")}if(bM&&bN){bC=bC.parentNode}try{if(!bM||bN){return bn(bC.querySelectorAll("[id='"+bB+"'] "+bL),bG)}}catch(bI){}finally{if(!bE){bD.removeAttribute("id")}}}}}return e(bL,bC,bG,bK)};for(var by in e){bh[by]=e[by]}bA=null})()}(function(){var e=am.documentElement,bz=e.matchesSelector||e.mozMatchesSelector||e.webkitMatchesSelector||e.msMatchesSelector;if(bz){var bB=!bz.call(am.createElement("div"),"div"),by=false;try{bz.call(am.documentElement,"[test!='']:sizzle")}catch(bA){by=true}bh.matchesSelector=function(bD,bF){bF=bF.replace(/\=\s*([^'"\]]*)\s*\]/g,"='$1']");if(!bh.isXML(bD)){try{if(by||!bm.match.PSEUDO.test(bF)&&!/!=/.test(bF)){var bC=bz.call(bD,bF);if(bC||!bB||bD.document&&bD.document.nodeType!==11){return bC}}}catch(bE){}}return bh(bF,null,null,[bD]).length>0}}})();(function(){var e=am.createElement("div");e.innerHTML="<div class='test e'></div><div class='test'></div>";if(!e.getElementsByClassName||e.getElementsByClassName("e").length===0){return}e.lastChild.className="e";if(e.getElementsByClassName("e").length===1){return}bm.order.splice(1,0,"CLASS");bm.find.CLASS=function(by,bz,bA){if(typeof bz.getElementsByClassName!=="undefined"&&!bA){return bz.getElementsByClassName(by[1])}};e=null})();function bf(by,bD,bC,bG,bE,bF){for(var bA=0,bz=bG.length;bA<bz;bA++){var e=bG[bA];if(e){var bB=false;e=e[by];while(e){if(e.sizcache===bC){bB=bG[e.sizset];break}if(e.nodeType===1&&!bF){e.sizcache=bC;e.sizset=bA}if(e.nodeName.toLowerCase()===bD){bB=e;break}e=e[by]}bG[bA]=bB}}}function bv(by,bD,bC,bG,bE,bF){for(var bA=0,bz=bG.length;bA<bz;bA++){var e=bG[bA];if(e){var bB=false;e=e[by];while(e){if(e.sizcache===bC){bB=bG[e.sizset];break}if(e.nodeType===1){if(!bF){e.sizcache=bC;e.sizset=bA}if(typeof bD!=="string"){if(e===bD){bB=true;break}}else{if(bh.filter(bD,[e]).length>0){bB=e;break}}}e=e[by]}bG[bA]=bB}}}if(am.documentElement.contains){bh.contains=function(by,e){return by!==e&&(by.contains?by.contains(e):true)}}else{if(am.documentElement.compareDocumentPosition){bh.contains=function(by,e){return !!(by.compareDocumentPosition(e)&16)}}else{bh.contains=function(){return false}}}bh.isXML=function(e){var by=(e?e.ownerDocument||e:0).documentElement;return by?by.nodeName!=="HTML":false};var bu=function(e,bE){var bC,bA=[],bB="",bz=bE.nodeType?[bE]:bE;while((bC=bm.match.PSEUDO.exec(e))){bB+=bC[0];e=e.replace(bm.match.PSEUDO,"")}e=bm.relative[e]?e+"*":e;for(var bD=0,by=bz.length;bD<by;bD++){bh(e,bz[bD],bA)}return bh.filter(bB,bA)};b.find=bh;b.expr=bh.selectors;b.expr[":"]=b.expr.filters;b.unique=bh.uniqueSort;b.text=bh.getText;b.isXMLDoc=bh.isXML;b.contains=bh.contains})();var X=/Until$/,aj=/^(?:parents|prevUntil|prevAll)/,aY=/,/,bb=/^.[^:#\[\.,]*$/,N=Array.prototype.slice,G=b.expr.match.POS,ap={children:true,contents:true,next:true,prev:true};b.fn.extend({find:function(e){var bg=this.pushStack("","find",e),bj=0;for(var bh=0,bf=this.length;bh<bf;bh++){bj=bg.length;b.find(e,this[bh],bg);if(bh>0){for(var bk=bj;bk<bg.length;bk++){for(var bi=0;bi<bj;bi++){if(bg[bi]===bg[bk]){bg.splice(bk--,1);break}}}}}return bg},has:function(bf){var e=b(bf);return this.filter(function(){for(var bh=0,bg=e.length;bh<bg;bh++){if(b.contains(this,e[bh])){return true}}})},not:function(e){return this.pushStack(aw(this,e,false),"not",e)},filter:function(e){return this.pushStack(aw(this,e,true),"filter",e)},is:function(e){return !!e&&b.filter(e,this).length>0},closest:function(bo,bf){var bl=[],bi,bg,bn=this[0];if(b.isArray(bo)){var bk,bh,bj={},e=1;if(bn&&bo.length){for(bi=0,bg=bo.length;bi<bg;bi++){bh=bo[bi];if(!bj[bh]){bj[bh]=b.expr.match.POS.test(bh)?b(bh,bf||this.context):bh}}while(bn&&bn.ownerDocument&&bn!==bf){for(bh in bj){bk=bj[bh];if(bk.jquery?bk.index(bn)>-1:b(bn).is(bk)){bl.push({selector:bh,elem:bn,level:e})}}bn=bn.parentNode;e++}}return bl}var bm=G.test(bo)?b(bo,bf||this.context):null;for(bi=0,bg=this.length;bi<bg;bi++){bn=this[bi];while(bn){if(bm?bm.index(bn)>-1:b.find.matchesSelector(bn,bo)){bl.push(bn);break}else{bn=bn.parentNode;if(!bn||!bn.ownerDocument||bn===bf){break}}}}bl=bl.length>1?b.unique(bl):bl;return this.pushStack(bl,"closest",bo)},index:function(e){if(!e||typeof e==="string"){return b.inArray(this[0],e?b(e):this.parent().children())}return b.inArray(e.jquery?e[0]:e,this)},add:function(e,bf){var bh=typeof e==="string"?b(e,bf):b.makeArray(e),bg=b.merge(this.get(),bh);return this.pushStack(C(bh[0])||C(bg[0])?bg:b.unique(bg))},andSelf:function(){return this.add(this.prevObject)}});function C(e){return !e||!e.parentNode||e.parentNode.nodeType===11}b.each({parent:function(bf){var e=bf.parentNode;return e&&e.nodeType!==11?e:null},parents:function(e){return b.dir(e,"parentNode")},parentsUntil:function(bf,e,bg){return b.dir(bf,"parentNode",bg)},next:function(e){return b.nth(e,2,"nextSibling")},prev:function(e){return b.nth(e,2,"previousSibling")},nextAll:function(e){return b.dir(e,"nextSibling")},prevAll:function(e){return b.dir(e,"previousSibling")},nextUntil:function(bf,e,bg){return b.dir(bf,"nextSibling",bg)},prevUntil:function(bf,e,bg){return b.dir(bf,"previousSibling",bg)},siblings:function(e){return b.sibling(e.parentNode.firstChild,e)},children:function(e){return b.sibling(e.firstChild)},contents:function(e){return b.nodeName(e,"iframe")?e.contentDocument||e.contentWindow.document:b.makeArray(e.childNodes)}},function(e,bf){b.fn[e]=function(bj,bg){var bi=b.map(this,bf,bj),bh=N.call(arguments);if(!X.test(e)){bg=bj}if(bg&&typeof bg==="string"){bi=b.filter(bg,bi)}bi=this.length>1&&!ap[e]?b.unique(bi):bi;if((this.length>1||aY.test(bg))&&aj.test(e)){bi=bi.reverse()}return this.pushStack(bi,e,bh.join(","))}});b.extend({filter:function(bg,e,bf){if(bf){bg=":not("+bg+")"}return e.length===1?b.find.matchesSelector(e[0],bg)?[e[0]]:[]:b.find.matches(bg,e)},dir:function(bg,bf,bi){var e=[],bh=bg[bf];while(bh&&bh.nodeType!==9&&(bi===I||bh.nodeType!==1||!b(bh).is(bi))){if(bh.nodeType===1){e.push(bh)}bh=bh[bf]}return e},nth:function(bi,e,bg,bh){e=e||1;var bf=0;for(;bi;bi=bi[bg]){if(bi.nodeType===1&&++bf===e){break}}return bi},sibling:function(bg,bf){var e=[];for(;bg;bg=bg.nextSibling){if(bg.nodeType===1&&bg!==bf){e.push(bg)}}return e}});function aw(bh,bg,e){if(b.isFunction(bg)){return b.grep(bh,function(bj,bi){var bk=!!bg.call(bj,bi,bj);return bk===e})}else{if(bg.nodeType){return b.grep(bh,function(bj,bi){return(bj===bg)===e})}else{if(typeof bg==="string"){var bf=b.grep(bh,function(bi){return bi.nodeType===1});if(bb.test(bg)){return b.filter(bg,bf,!e)}else{bg=b.filter(bg,bf)}}}}return b.grep(bh,function(bj,bi){return(b.inArray(bj,bg)>=0)===e})}var ac=/ jQuery\d+="(?:\d+|null)"/g,ak=/^\s+/,P=/<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/ig,d=/<([\w:]+)/,w=/<tbody/i,U=/<|&#?\w+;/,M=/<(?:script|object|embed|option|style)/i,n=/checked\s*(?:[^=]|=\s*.checked.)/i,ao={option:[1,"<select multiple='multiple'>","</select>"],legend:[1,"<fieldset>","</fieldset>"],thead:[1,"<table>","</table>"],tr:[2,"<table><tbody>","</tbody></table>"],td:[3,"<table><tbody><tr>","</tr></tbody></table>"],col:[2,"<table><tbody></tbody><colgroup>","</colgroup></table>"],area:[1,"<map>","</map>"],_default:[0,"",""]};ao.optgroup=ao.option;ao.tbody=ao.tfoot=ao.colgroup=ao.caption=ao.thead;ao.th=ao.td;if(!b.support.htmlSerialize){ao._default=[1,"div<div>","</div>"]}b.fn.extend({text:function(e){if(b.isFunction(e)){return this.each(function(bg){var bf=b(this);bf.text(e.call(this,bg,bf.text()))})}if(typeof e!=="object"&&e!==I){return this.empty().append((this[0]&&this[0].ownerDocument||am).createTextNode(e))}return b.text(this)},wrapAll:function(e){if(b.isFunction(e)){return this.each(function(bg){b(this).wrapAll(e.call(this,bg))})}if(this[0]){var bf=b(e,this[0].ownerDocument).eq(0).clone(true);if(this[0].parentNode){bf.insertBefore(this[0])}bf.map(function(){var bg=this;while(bg.firstChild&&bg.firstChild.nodeType===1){bg=bg.firstChild}return bg}).append(this)}return this},wrapInner:function(e){if(b.isFunction(e)){return this.each(function(bf){b(this).wrapInner(e.call(this,bf))})}return this.each(function(){var bf=b(this),bg=bf.contents();if(bg.length){bg.wrapAll(e)}else{bf.append(e)}})},wrap:function(e){return this.each(function(){b(this).wrapAll(e)})},unwrap:function(){return this.parent().each(function(){if(!b.nodeName(this,"body")){b(this).replaceWith(this.childNodes)}}).end()},append:function(){return this.domManip(arguments,true,function(e){if(this.nodeType===1){this.appendChild(e)}})},prepend:function(){return this.domManip(arguments,true,function(e){if(this.nodeType===1){this.insertBefore(e,this.firstChild)}})},before:function(){if(this[0]&&this[0].parentNode){return this.domManip(arguments,false,function(bf){this.parentNode.insertBefore(bf,this)})}else{if(arguments.length){var e=b(arguments[0]);e.push.apply(e,this.toArray());return this.pushStack(e,"before",arguments)}}},after:function(){if(this[0]&&this[0].parentNode){return this.domManip(arguments,false,function(bf){this.parentNode.insertBefore(bf,this.nextSibling)})}else{if(arguments.length){var e=this.pushStack(this,"after",arguments);e.push.apply(e,b(arguments[0]).toArray());return e}}},remove:function(e,bh){for(var bf=0,bg;(bg=this[bf])!=null;bf++){if(!e||b.filter(e,[bg]).length){if(!bh&&bg.nodeType===1){b.cleanData(bg.getElementsByTagName("*"));b.cleanData([bg])}if(bg.parentNode){bg.parentNode.removeChild(bg)}}}return this},empty:function(){for(var e=0,bf;(bf=this[e])!=null;e++){if(bf.nodeType===1){b.cleanData(bf.getElementsByTagName("*"))}while(bf.firstChild){bf.removeChild(bf.firstChild)}}return this},clone:function(bf,e){bf=bf==null?false:bf;e=e==null?bf:e;return this.map(function(){return b.clone(this,bf,e)})},html:function(bh){if(bh===I){return this[0]&&this[0].nodeType===1?this[0].innerHTML.replace(ac,""):null}else{if(typeof bh==="string"&&!M.test(bh)&&(b.support.leadingWhitespace||!ak.test(bh))&&!ao[(d.exec(bh)||["",""])[1].toLowerCase()]){bh=bh.replace(P,"<$1></$2>");try{for(var bg=0,bf=this.length;bg<bf;bg++){if(this[bg].nodeType===1){b.cleanData(this[bg].getElementsByTagName("*"));this[bg].innerHTML=bh}}}catch(bi){this.empty().append(bh)}}else{if(b.isFunction(bh)){this.each(function(bj){var e=b(this);e.html(bh.call(this,bj,e.html()))})}else{this.empty().append(bh)}}}return this},replaceWith:function(e){if(this[0]&&this[0].parentNode){if(b.isFunction(e)){return this.each(function(bh){var bg=b(this),bf=bg.html();bg.replaceWith(e.call(this,bh,bf))})}if(typeof e!=="string"){e=b(e).detach()}return this.each(function(){var bg=this.nextSibling,bf=this.parentNode;b(this).remove();if(bg){b(bg).before(e)}else{b(bf).append(e)}})}else{return this.length?this.pushStack(b(b.isFunction(e)?e():e),"replaceWith",e):this}},detach:function(e){return this.remove(e,true)},domManip:function(bl,bp,bo){var bh,bi,bk,bn,bm=bl[0],bf=[];if(!b.support.checkClone&&arguments.length===3&&typeof bm==="string"&&n.test(bm)){return this.each(function(){b(this).domManip(bl,bp,bo,true)})}if(b.isFunction(bm)){return this.each(function(br){var bq=b(this);bl[0]=bm.call(this,br,bp?bq.html():I);bq.domManip(bl,bp,bo)})}if(this[0]){bn=bm&&bm.parentNode;if(b.support.parentNode&&bn&&bn.nodeType===11&&bn.childNodes.length===this.length){bh={fragment:bn}}else{bh=b.buildFragment(bl,this,bf)}bk=bh.fragment;if(bk.childNodes.length===1){bi=bk=bk.firstChild}else{bi=bk.firstChild}if(bi){bp=bp&&b.nodeName(bi,"tr");for(var bg=0,e=this.length,bj=e-1;bg<e;bg++){bo.call(bp?aZ(this[bg],bi):this[bg],bh.cacheable||(e>1&&bg<bj)?b.clone(bk,true,true):bk)}}if(bf.length){b.each(bf,ba)}}return this}});function aZ(e,bf){return b.nodeName(e,"table")?(e.getElementsByTagName("tbody")[0]||e.appendChild(e.ownerDocument.createElement("tbody"))):e}function t(e,bl){if(bl.nodeType!==1||!b.hasData(e)){return}var bk=b.expando,bh=b.data(e),bi=b.data(bl,bh);if((bh=bh[bk])){var bm=bh.events;bi=bi[bk]=b.extend({},bh);if(bm){delete bi.handle;bi.events={};for(var bj in bm){for(var bg=0,bf=bm[bj].length;bg<bf;bg++){b.event.add(bl,bj+(bm[bj][bg].namespace?".":"")+bm[bj][bg].namespace,bm[bj][bg],bm[bj][bg].data)}}}}}function ad(bf,e){if(e.nodeType!==1){return}var bg=e.nodeName.toLowerCase();e.clearAttributes();e.mergeAttributes(bf);if(bg==="object"){e.outerHTML=bf.outerHTML}else{if(bg==="input"&&(bf.type==="checkbox"||bf.type==="radio")){if(bf.checked){e.defaultChecked=e.checked=bf.checked}if(e.value!==bf.value){e.value=bf.value}}else{if(bg==="option"){e.selected=bf.defaultSelected}else{if(bg==="input"||bg==="textarea"){e.defaultValue=bf.defaultValue}}}}e.removeAttribute(b.expando)}b.buildFragment=function(bj,bh,bf){var bi,e,bg,bk=(bh&&bh[0]?bh[0].ownerDocument||bh[0]:am);if(bj.length===1&&typeof bj[0]==="string"&&bj[0].length<512&&bk===am&&bj[0].charAt(0)==="<"&&!M.test(bj[0])&&(b.support.checkClone||!n.test(bj[0]))){e=true;bg=b.fragments[bj[0]];if(bg){if(bg!==1){bi=bg}}}if(!bi){bi=bk.createDocumentFragment();b.clean(bj,bk,bi,bf)}if(e){b.fragments[bj[0]]=bg?bi:1}return{fragment:bi,cacheable:e}};b.fragments={};b.each({appendTo:"append",prependTo:"prepend",insertBefore:"before",insertAfter:"after",replaceAll:"replaceWith"},function(e,bf){b.fn[e]=function(bg){var bj=[],bm=b(bg),bl=this.length===1&&this[0].parentNode;if(bl&&bl.nodeType===11&&bl.childNodes.length===1&&bm.length===1){bm[bf](this[0]);return this}else{for(var bk=0,bh=bm.length;bk<bh;bk++){var bi=(bk>0?this.clone(true):this).get();b(bm[bk])[bf](bi);bj=bj.concat(bi)}return this.pushStack(bj,e,bm.selector)}}});function a3(e){if("getElementsByTagName" in e){return e.getElementsByTagName("*")}else{if("querySelectorAll" in e){return e.querySelectorAll("*")}else{return[]}}}b.extend({clone:function(bi,bk,bg){var bj=bi.cloneNode(true),e,bf,bh;if((!b.support.noCloneEvent||!b.support.noCloneChecked)&&(bi.nodeType===1||bi.nodeType===11)&&!b.isXMLDoc(bi)){ad(bi,bj);e=a3(bi);bf=a3(bj);for(bh=0;e[bh];++bh){ad(e[bh],bf[bh])}}if(bk){t(bi,bj);if(bg){e=a3(bi);bf=a3(bj);for(bh=0;e[bh];++bh){t(e[bh],bf[bh])}}}return bj},clean:function(bg,bi,bp,bk){bi=bi||am;if(typeof bi.createElement==="undefined"){bi=bi.ownerDocument||bi[0]&&bi[0].ownerDocument||am}var bq=[];for(var bo=0,bj;(bj=bg[bo])!=null;bo++){if(typeof bj==="number"){bj+=""}if(!bj){continue}if(typeof bj==="string"&&!U.test(bj)){bj=bi.createTextNode(bj)}else{if(typeof bj==="string"){bj=bj.replace(P,"<$1></$2>");var br=(d.exec(bj)||["",""])[1].toLowerCase(),bh=ao[br]||ao._default,bn=bh[0],bf=bi.createElement("div");bf.innerHTML=bh[1]+bj+bh[2];while(bn--){bf=bf.lastChild}if(!b.support.tbody){var e=w.test(bj),bm=br==="table"&&!e?bf.firstChild&&bf.firstChild.childNodes:bh[1]==="<table>"&&!e?bf.childNodes:[];for(var bl=bm.length-1;bl>=0;--bl){if(b.nodeName(bm[bl],"tbody")&&!bm[bl].childNodes.length){bm[bl].parentNode.removeChild(bm[bl])}}}if(!b.support.leadingWhitespace&&ak.test(bj)){bf.insertBefore(bi.createTextNode(ak.exec(bj)[0]),bf.firstChild)}bj=bf.childNodes}}if(bj.nodeType){bq.push(bj)}else{bq=b.merge(bq,bj)}}if(bp){for(bo=0;bq[bo];bo++){if(bk&&b.nodeName(bq[bo],"script")&&(!bq[bo].type||bq[bo].type.toLowerCase()==="text/javascript")){bk.push(bq[bo].parentNode?bq[bo].parentNode.removeChild(bq[bo]):bq[bo])}else{if(bq[bo].nodeType===1){bq.splice.apply(bq,[bo+1,0].concat(b.makeArray(bq[bo].getElementsByTagName("script"))))}bp.appendChild(bq[bo])}}}return bq},cleanData:function(bf){var bi,bg,e=b.cache,bn=b.expando,bl=b.event.special,bk=b.support.deleteExpando;for(var bj=0,bh;(bh=bf[bj])!=null;bj++){if(bh.nodeName&&b.noData[bh.nodeName.toLowerCase()]){continue}bg=bh[b.expando];if(bg){bi=e[bg]&&e[bg][bn];if(bi&&bi.events){for(var bm in bi.events){if(bl[bm]){b.event.remove(bh,bm)}else{b.removeEvent(bh,bm,bi.handle)}}if(bi.handle){bi.handle.elem=null}}if(bk){delete bh[b.expando]}else{if(bh.removeAttribute){bh.removeAttribute(b.expando)}}delete e[bg]}}}});function ba(e,bf){if(bf.src){b.ajax({url:bf.src,async:false,dataType:"script"})}else{b.globalEval(bf.text||bf.textContent||bf.innerHTML||"")}if(bf.parentNode){bf.parentNode.removeChild(bf)}}var af=/alpha\([^)]*\)/i,al=/opacity=([^)]*)/,aO=/-([a-z])/ig,z=/([A-Z]|^ms)/g,a1=/^-?\d+(?:px)?$/i,a9=/^-?\d/,aX={position:"absolute",visibility:"hidden",display:"block"},ah=["Left","Right"],aT=["Top","Bottom"],V,az,aN,m=function(e,bf){return bf.toUpperCase()};b.fn.css=function(e,bf){if(arguments.length===2&&bf===I){return this}return b.access(this,e,bf,true,function(bh,bg,bi){return bi!==I?b.style(bh,bg,bi):b.css(bh,bg)})};b.extend({cssHooks:{opacity:{get:function(bg,bf){if(bf){var e=V(bg,"opacity","opacity");return e===""?"1":e}else{return bg.style.opacity}}}},cssNumber:{zIndex:true,fontWeight:true,opacity:true,zoom:true,lineHeight:true},cssProps:{"float":b.support.cssFloat?"cssFloat":"styleFloat"},style:function(bh,bg,bm,bi){if(!bh||bh.nodeType===3||bh.nodeType===8||!bh.style){return}var bl,bj=b.camelCase(bg),bf=bh.style,bn=b.cssHooks[bj];bg=b.cssProps[bj]||bj;if(bm!==I){if(typeof bm==="number"&&isNaN(bm)||bm==null){return}if(typeof bm==="number"&&!b.cssNumber[bj]){bm+="px"}if(!bn||!("set" in bn)||(bm=bn.set(bh,bm))!==I){try{bf[bg]=bm}catch(bk){}}}else{if(bn&&"get" in bn&&(bl=bn.get(bh,false,bi))!==I){return bl}return bf[bg]}},css:function(bj,bi,bf){var bh,bg=b.camelCase(bi),e=b.cssHooks[bg];bi=b.cssProps[bg]||bg;if(e&&"get" in e&&(bh=e.get(bj,true,bf))!==I){return bh}else{if(V){return V(bj,bi,bg)}}},swap:function(bh,bg,bi){var e={};for(var bf in bg){e[bf]=bh.style[bf];bh.style[bf]=bg[bf]}bi.call(bh);for(bf in bg){bh.style[bf]=e[bf]}},camelCase:function(e){return e.replace(aO,m)}});b.curCSS=b.css;b.each(["height","width"],function(bf,e){b.cssHooks[e]={get:function(bi,bh,bg){var bj;if(bh){if(bi.offsetWidth!==0){bj=p(bi,e,bg)}else{b.swap(bi,aX,function(){bj=p(bi,e,bg)})}if(bj<=0){bj=V(bi,e,e);if(bj==="0px"&&aN){bj=aN(bi,e,e)}if(bj!=null){return bj===""||bj==="auto"?"0px":bj}}if(bj<0||bj==null){bj=bi.style[e];return bj===""||bj==="auto"?"0px":bj}return typeof bj==="string"?bj:bj+"px"}},set:function(bg,bh){if(a1.test(bh)){bh=parseFloat(bh);if(bh>=0){return bh+"px"}}else{return bh}}}});if(!b.support.opacity){b.cssHooks.opacity={get:function(bf,e){return al.test((e&&bf.currentStyle?bf.currentStyle.filter:bf.style.filter)||"")?(parseFloat(RegExp.$1)/100)+"":e?"1":""},set:function(bh,bi){var bg=bh.style;bg.zoom=1;var e=b.isNaN(bi)?"":"alpha(opacity="+bi*100+")",bf=bg.filter||"";bg.filter=af.test(bf)?bf.replace(af,e):bg.filter+" "+e}}}b(function(){if(!b.support.reliableMarginRight){b.cssHooks.marginRight={get:function(bg,bf){var e;b.swap(bg,{display:"inline-block"},function(){if(bf){e=V(bg,"margin-right","marginRight")}else{e=bg.style.marginRight}});return e}}}});if(am.defaultView&&am.defaultView.getComputedStyle){az=function(bj,e,bh){var bg,bi,bf;bh=bh.replace(z,"-$1").toLowerCase();if(!(bi=bj.ownerDocument.defaultView)){return I}if((bf=bi.getComputedStyle(bj,null))){bg=bf.getPropertyValue(bh);if(bg===""&&!b.contains(bj.ownerDocument.documentElement,bj)){bg=b.style(bj,bh)}}return bg}}if(am.documentElement.currentStyle){aN=function(bi,bg){var bj,bf=bi.currentStyle&&bi.currentStyle[bg],e=bi.runtimeStyle&&bi.runtimeStyle[bg],bh=bi.style;if(!a1.test(bf)&&a9.test(bf)){bj=bh.left;if(e){bi.runtimeStyle.left=bi.currentStyle.left}bh.left=bg==="fontSize"?"1em":(bf||0);bf=bh.pixelLeft+"px";bh.left=bj;if(e){bi.runtimeStyle.left=e}}return bf===""?"auto":bf}}V=az||aN;function p(bg,bf,e){var bi=bf==="width"?ah:aT,bh=bf==="width"?bg.offsetWidth:bg.offsetHeight;if(e==="border"){return bh}b.each(bi,function(){if(!e){bh-=parseFloat(b.css(bg,"padding"+this))||0}if(e==="margin"){bh+=parseFloat(b.css(bg,"margin"+this))||0}else{bh-=parseFloat(b.css(bg,"border"+this+"Width"))||0}});return bh}if(b.expr&&b.expr.filters){b.expr.filters.hidden=function(bg){var bf=bg.offsetWidth,e=bg.offsetHeight;return(bf===0&&e===0)||(!b.support.reliableHiddenOffsets&&(bg.style.display||b.css(bg,"display"))==="none")};b.expr.filters.visible=function(e){return !b.expr.filters.hidden(e)}}var j=/%20/g,ai=/\[\]$/,be=/\r?\n/g,bc=/#.*$/,at=/^(.*?):[ \t]*([^\r\n]*)\r?$/mg,aQ=/^(?:color|date|datetime|email|hidden|month|number|password|range|search|tel|text|time|url|week)$/i,aD=/^(?:about|app|app\-storage|.+\-extension|file|widget):$/,aF=/^(?:GET|HEAD)$/,c=/^\/\//,J=/\?/,aW=/<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi,q=/^(?:select|textarea)/i,h=/\s+/,bd=/([?&])_=[^&]*/,S=/(^|\-)([a-z])/g,aL=function(bf,e,bg){return e+bg.toUpperCase()},H=/^([\w\+\.\-]+:)(?:\/\/([^\/?#:]*)(?::(\d+))?)?/,A=b.fn.load,W={},r={},av,s;try{av=am.location.href}catch(an){av=am.createElement("a");av.href="";av=av.href}s=H.exec(av.toLowerCase())||[];function f(e){return function(bi,bk){if(typeof bi!=="string"){bk=bi;bi="*"}if(b.isFunction(bk)){var bh=bi.toLowerCase().split(h),bg=0,bj=bh.length,bf,bl,bm;for(;bg<bj;bg++){bf=bh[bg];bm=/^\+/.test(bf);if(bm){bf=bf.substr(1)||"*"}bl=e[bf]=e[bf]||[];bl[bm?"unshift":"push"](bk)}}}}function aK(bf,bo,bj,bn,bl,bh){bl=bl||bo.dataTypes[0];bh=bh||{};bh[bl]=true;var bk=bf[bl],bg=0,e=bk?bk.length:0,bi=(bf===W),bm;for(;bg<e&&(bi||!bm);bg++){bm=bk[bg](bo,bj,bn);if(typeof bm==="string"){if(!bi||bh[bm]){bm=I}else{bo.dataTypes.unshift(bm);bm=aK(bf,bo,bj,bn,bm,bh)}}}if((bi||!bm)&&!bh["*"]){bm=aK(bf,bo,bj,bn,"*",bh)}return bm}b.fn.extend({load:function(bg,bj,bk){if(typeof bg!=="string"&&A){return A.apply(this,arguments)}else{if(!this.length){return this}}var bi=bg.indexOf(" ");if(bi>=0){var e=bg.slice(bi,bg.length);bg=bg.slice(0,bi)}var bh="GET";if(bj){if(b.isFunction(bj)){bk=bj;bj=I}else{if(typeof bj==="object"){bj=b.param(bj,b.ajaxSettings.traditional);bh="POST"}}}var bf=this;b.ajax({url:bg,type:bh,dataType:"html",data:bj,complete:function(bm,bl,bn){bn=bm.responseText;if(bm.isResolved()){bm.done(function(bo){bn=bo});bf.html(e?b("<div>").append(bn.replace(aW,"")).find(e):bn)}if(bk){bf.each(bk,[bn,bl,bm])}}});return this},serialize:function(){return b.param(this.serializeArray())},serializeArray:function(){return this.map(function(){return this.elements?b.makeArray(this.elements):this}).filter(function(){return this.name&&!this.disabled&&(this.checked||q.test(this.nodeName)||aQ.test(this.type))}).map(function(e,bf){var bg=b(this).val();return bg==null?null:b.isArray(bg)?b.map(bg,function(bi,bh){return{name:bf.name,value:bi.replace(be,"\r\n")}}):{name:bf.name,value:bg.replace(be,"\r\n")}}).get()}});b.each("ajaxStart ajaxStop ajaxComplete ajaxError ajaxSuccess ajaxSend".split(" "),function(e,bf){b.fn[bf]=function(bg){return this.bind(bf,bg)}});b.each(["get","post"],function(e,bf){b[bf]=function(bg,bi,bj,bh){if(b.isFunction(bi)){bh=bh||bj;bj=bi;bi=I}return b.ajax({type:bf,url:bg,data:bi,success:bj,dataType:bh})}});b.extend({getScript:function(e,bf){return b.get(e,I,bf,"script")},getJSON:function(e,bf,bg){return b.get(e,bf,bg,"json")},ajaxSetup:function(bg,e){if(!e){e=bg;bg=b.extend(true,b.ajaxSettings,e)}else{b.extend(true,bg,b.ajaxSettings,e)}for(var bf in {context:1,url:1}){if(bf in e){bg[bf]=e[bf]}else{if(bf in b.ajaxSettings){bg[bf]=b.ajaxSettings[bf]}}}return bg},ajaxSettings:{url:av,isLocal:aD.test(s[1]),global:true,type:"GET",contentType:"application/x-www-form-urlencoded",processData:true,async:true,accepts:{xml:"application/xml, text/xml",html:"text/html",text:"text/plain",json:"application/json, text/javascript","*":"*/*"},contents:{xml:/xml/,html:/html/,json:/json/},responseFields:{xml:"responseXML",text:"responseText"},converters:{"* text":a0.String,"text html":true,"text json":b.parseJSON,"text xml":b.parseXML}},ajaxPrefilter:f(W),ajaxTransport:f(r),ajax:function(bj,bh){if(typeof bj==="object"){bh=bj;bj=I}bh=bh||{};var bn=b.ajaxSetup({},bh),bB=bn.context||bn,bq=bB!==bn&&(bB.nodeType||bB instanceof b)?b(bB):b.event,bA=b.Deferred(),bx=b._Deferred(),bl=bn.statusCode||{},bm,br={},bz,bi,bv,bo,bs,bk=0,bg,bu,bt={readyState:0,setRequestHeader:function(e,bC){if(!bk){br[e.toLowerCase().replace(S,aL)]=bC}return this},getAllResponseHeaders:function(){return bk===2?bz:null},getResponseHeader:function(bC){var e;if(bk===2){if(!bi){bi={};while((e=at.exec(bz))){bi[e[1].toLowerCase()]=e[2]}}e=bi[bC.toLowerCase()]}return e===I?null:e},overrideMimeType:function(e){if(!bk){bn.mimeType=e}return this},abort:function(e){e=e||"abort";if(bv){bv.abort(e)}bp(0,e);return this}};function bp(bH,bF,bI,bE){if(bk===2){return}bk=2;if(bo){clearTimeout(bo)}bv=I;bz=bE||"";bt.readyState=bH?4:0;var bC,bM,bL,bG=bI?a6(bn,bt,bI):I,bD,bK;if(bH>=200&&bH<300||bH===304){if(bn.ifModified){if((bD=bt.getResponseHeader("Last-Modified"))){b.lastModified[bm]=bD}if((bK=bt.getResponseHeader("Etag"))){b.etag[bm]=bK}}if(bH===304){bF="notmodified";bC=true}else{try{bM=E(bn,bG);bF="success";bC=true}catch(bJ){bF="parsererror";bL=bJ}}}else{bL=bF;if(!bF||bH){bF="error";if(bH<0){bH=0}}}bt.status=bH;bt.statusText=bF;if(bC){bA.resolveWith(bB,[bM,bF,bt])}else{bA.rejectWith(bB,[bt,bF,bL])}bt.statusCode(bl);bl=I;if(bg){bq.trigger("ajax"+(bC?"Success":"Error"),[bt,bn,bC?bM:bL])}bx.resolveWith(bB,[bt,bF]);if(bg){bq.trigger("ajaxComplete",[bt,bn]);if(!(--b.active)){b.event.trigger("ajaxStop")}}}bA.promise(bt);bt.success=bt.done;bt.error=bt.fail;bt.complete=bx.done;bt.statusCode=function(bC){if(bC){var e;if(bk<2){for(e in bC){bl[e]=[bl[e],bC[e]]}}else{e=bC[bt.status];bt.then(e,e)}}return this};bn.url=((bj||bn.url)+"").replace(bc,"").replace(c,s[1]+"//");bn.dataTypes=b.trim(bn.dataType||"*").toLowerCase().split(h);if(bn.crossDomain==null){bs=H.exec(bn.url.toLowerCase());bn.crossDomain=!!(bs&&(bs[1]!=s[1]||bs[2]!=s[2]||(bs[3]||(bs[1]==="http:"?80:443))!=(s[3]||(s[1]==="http:"?80:443))))}if(bn.data&&bn.processData&&typeof bn.data!=="string"){bn.data=b.param(bn.data,bn.traditional)}aK(W,bn,bh,bt);if(bk===2){return false}bg=bn.global;bn.type=bn.type.toUpperCase();bn.hasContent=!aF.test(bn.type);if(bg&&b.active++===0){b.event.trigger("ajaxStart")}if(!bn.hasContent){if(bn.data){bn.url+=(J.test(bn.url)?"&":"?")+bn.data}bm=bn.url;if(bn.cache===false){var bf=b.now(),by=bn.url.replace(bd,"$1_="+bf);bn.url=by+((by===bn.url)?(J.test(bn.url)?"&":"?")+"_="+bf:"")}}if(bn.data&&bn.hasContent&&bn.contentType!==false||bh.contentType){br["Content-Type"]=bn.contentType}if(bn.ifModified){bm=bm||bn.url;if(b.lastModified[bm]){br["If-Modified-Since"]=b.lastModified[bm]}if(b.etag[bm]){br["If-None-Match"]=b.etag[bm]}}br.Accept=bn.dataTypes[0]&&bn.accepts[bn.dataTypes[0]]?bn.accepts[bn.dataTypes[0]]+(bn.dataTypes[0]!=="*"?", */*; q=0.01":""):bn.accepts["*"];for(bu in bn.headers){bt.setRequestHeader(bu,bn.headers[bu])}if(bn.beforeSend&&(bn.beforeSend.call(bB,bt,bn)===false||bk===2)){bt.abort();return false}for(bu in {success:1,error:1,complete:1}){bt[bu](bn[bu])}bv=aK(r,bn,bh,bt);if(!bv){bp(-1,"No Transport")}else{bt.readyState=1;if(bg){bq.trigger("ajaxSend",[bt,bn])}if(bn.async&&bn.timeout>0){bo=setTimeout(function(){bt.abort("timeout")},bn.timeout)}try{bk=1;bv.send(br,bp)}catch(bw){if(status<2){bp(-1,bw)}else{b.error(bw)}}}return bt},param:function(e,bg){var bf=[],bi=function(bj,bk){bk=b.isFunction(bk)?bk():bk;bf[bf.length]=encodeURIComponent(bj)+"="+encodeURIComponent(bk)};if(bg===I){bg=b.ajaxSettings.traditional}if(b.isArray(e)||(e.jquery&&!b.isPlainObject(e))){b.each(e,function(){bi(this.name,this.value)})}else{for(var bh in e){v(bh,e[bh],bg,bi)}}return bf.join("&").replace(j,"+")}});function v(bg,bi,bf,bh){if(b.isArray(bi)&&bi.length){b.each(bi,function(bk,bj){if(bf||ai.test(bg)){bh(bg,bj)}else{v(bg+"["+(typeof bj==="object"||b.isArray(bj)?bk:"")+"]",bj,bf,bh)}})}else{if(!bf&&bi!=null&&typeof bi==="object"){if(b.isArray(bi)||b.isEmptyObject(bi)){bh(bg,"")}else{for(var e in bi){v(bg+"["+e+"]",bi[e],bf,bh)}}}else{bh(bg,bi)}}}b.extend({active:0,lastModified:{},etag:{}});function a6(bn,bm,bj){var bf=bn.contents,bl=bn.dataTypes,bg=bn.responseFields,bi,bk,bh,e;for(bk in bg){if(bk in bj){bm[bg[bk]]=bj[bk]}}while(bl[0]==="*"){bl.shift();if(bi===I){bi=bn.mimeType||bm.getResponseHeader("content-type")}}if(bi){for(bk in bf){if(bf[bk]&&bf[bk].test(bi)){bl.unshift(bk);break}}}if(bl[0] in bj){bh=bl[0]}else{for(bk in bj){if(!bl[0]||bn.converters[bk+" "+bl[0]]){bh=bk;break}if(!e){e=bk}}bh=bh||e}if(bh){if(bh!==bl[0]){bl.unshift(bh)}return bj[bh]}}function E(br,bj){if(br.dataFilter){bj=br.dataFilter(bj,br.dataType)}var bn=br.dataTypes,bq={},bk,bo,bg=bn.length,bl,bm=bn[0],bh,bi,bp,bf,e;for(bk=1;bk<bg;bk++){if(bk===1){for(bo in br.converters){if(typeof bo==="string"){bq[bo.toLowerCase()]=br.converters[bo]}}}bh=bm;bm=bn[bk];if(bm==="*"){bm=bh}else{if(bh!=="*"&&bh!==bm){bi=bh+" "+bm;bp=bq[bi]||bq["* "+bm];if(!bp){e=I;for(bf in bq){bl=bf.split(" ");if(bl[0]===bh||bl[0]==="*"){e=bq[bl[1]+" "+bm];if(e){bf=bq[bf];if(bf===true){bp=e}else{if(e===true){bp=bf}}break}}}}if(!(bp||e)){b.error("No conversion from "+bi.replace(" "," to "))}if(bp!==true){bj=bp?bp(bj):e(bf(bj))}}}}return bj}var ar=b.now(),u=/(\=)\?(&|$)|\?\?/i;b.ajaxSetup({jsonp:"callback",jsonpCallback:function(){return b.expando+"_"+(ar++)}});b.ajaxPrefilter("json jsonp",function(bo,bk,bn){var bm=(typeof bo.data==="string");if(bo.dataTypes[0]==="jsonp"||bk.jsonpCallback||bk.jsonp!=null||bo.jsonp!==false&&(u.test(bo.url)||bm&&u.test(bo.data))){var bl,bg=bo.jsonpCallback=b.isFunction(bo.jsonpCallback)?bo.jsonpCallback():bo.jsonpCallback,bj=a0[bg],e=bo.url,bi=bo.data,bf="$1"+bg+"$2",bh=function(){a0[bg]=bj;if(bl&&b.isFunction(bj)){a0[bg](bl[0])}};if(bo.jsonp!==false){e=e.replace(u,bf);if(bo.url===e){if(bm){bi=bi.replace(u,bf)}if(bo.data===bi){e+=(/\?/.test(e)?"&":"?")+bo.jsonp+"="+bg}}}bo.url=e;bo.data=bi;a0[bg]=function(bp){bl=[bp]};bn.then(bh,bh);bo.converters["script json"]=function(){if(!bl){b.error(bg+" was not called")}return bl[0]};bo.dataTypes[0]="json";return"script"}});b.ajaxSetup({accepts:{script:"text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},contents:{script:/javascript|ecmascript/},converters:{"text script":function(e){b.globalEval(e);return e}}});b.ajaxPrefilter("script",function(e){if(e.cache===I){e.cache=false}if(e.crossDomain){e.type="GET";e.global=false}});b.ajaxTransport("script",function(bg){if(bg.crossDomain){var e,bf=am.head||am.getElementsByTagName("head")[0]||am.documentElement;return{send:function(bh,bi){e=am.createElement("script");e.async="async";if(bg.scriptCharset){e.charset=bg.scriptCharset}e.src=bg.url;e.onload=e.onreadystatechange=function(bk,bj){if(!e.readyState||/loaded|complete/.test(e.readyState)){e.onload=e.onreadystatechange=null;if(bf&&e.parentNode){bf.removeChild(e)}e=I;if(!bj){bi(200,"success")}}};bf.insertBefore(e,bf.firstChild)},abort:function(){if(e){e.onload(0,1)}}}}});var y=b.now(),K,au;function B(){b(a0).unload(function(){for(var e in K){K[e](0,1)}})}function aC(){try{return new a0.XMLHttpRequest()}catch(bf){}}function ae(){try{return new a0.ActiveXObject("Microsoft.XMLHTTP")}catch(bf){}}b.ajaxSettings.xhr=a0.ActiveXObject?function(){return !this.isLocal&&aC()||ae()}:aC;au=b.ajaxSettings.xhr();b.support.ajax=!!au;b.support.cors=au&&("withCredentials" in au);au=I;if(b.support.ajax){b.ajaxTransport(function(e){if(!e.crossDomain||b.support.cors){var bf;return{send:function(bl,bg){var bk=e.xhr(),bj,bi;if(e.username){bk.open(e.type,e.url,e.async,e.username,e.password)}else{bk.open(e.type,e.url,e.async)}if(e.xhrFields){for(bi in e.xhrFields){bk[bi]=e.xhrFields[bi]}}if(e.mimeType&&bk.overrideMimeType){bk.overrideMimeType(e.mimeType)}if(!e.crossDomain&&!bl["X-Requested-With"]){bl["X-Requested-With"]="XMLHttpRequest"}try{for(bi in bl){bk.setRequestHeader(bi,bl[bi])}}catch(bh){}bk.send((e.hasContent&&e.data)||null);bf=function(bu,bo){var bp,bn,bm,bs,br;try{if(bf&&(bo||bk.readyState===4)){bf=I;if(bj){bk.onreadystatechange=b.noop;delete K[bj]}if(bo){if(bk.readyState!==4){bk.abort()}}else{bp=bk.status;bm=bk.getAllResponseHeaders();bs={};br=bk.responseXML;if(br&&br.documentElement){bs.xml=br}bs.text=bk.responseText;try{bn=bk.statusText}catch(bt){bn=""}if(!bp&&e.isLocal&&!e.crossDomain){bp=bs.text?200:404}else{if(bp===1223){bp=204}}}}}catch(bq){if(!bo){bg(-1,bq)}}if(bs){bg(bp,bn,bs,bm)}};if(!e.async||bk.readyState===4){bf()}else{if(!K){K={};B()}bj=y++;bk.onreadystatechange=K[bj]=bf}},abort:function(){if(bf){bf(0,1)}}}}})}var O={},aq=/^(?:toggle|show|hide)$/,aH=/^([+\-]=)?([\d+.\-]+)([a-z%]*)$/i,aU,ay=[["height","marginTop","marginBottom","paddingTop","paddingBottom"],["width","marginLeft","marginRight","paddingLeft","paddingRight"],["opacity"]];b.fn.extend({show:function(bh,bk,bj){var bg,bi;if(bh||bh===0){return this.animate(aS("show",3),bh,bk,bj)}else{for(var bf=0,e=this.length;bf<e;bf++){bg=this[bf];bi=bg.style.display;if(!b._data(bg,"olddisplay")&&bi==="none"){bi=bg.style.display=""}if(bi===""&&b.css(bg,"display")==="none"){b._data(bg,"olddisplay",x(bg.nodeName))}}for(bf=0;bf<e;bf++){bg=this[bf];bi=bg.style.display;if(bi===""||bi==="none"){bg.style.display=b._data(bg,"olddisplay")||""}}return this}},hide:function(bg,bj,bi){if(bg||bg===0){return this.animate(aS("hide",3),bg,bj,bi)}else{for(var bf=0,e=this.length;bf<e;bf++){var bh=b.css(this[bf],"display");if(bh!=="none"&&!b._data(this[bf],"olddisplay")){b._data(this[bf],"olddisplay",bh)}}for(bf=0;bf<e;bf++){this[bf].style.display="none"}return this}},_toggle:b.fn.toggle,toggle:function(bg,bf,bh){var e=typeof bg==="boolean";if(b.isFunction(bg)&&b.isFunction(bf)){this._toggle.apply(this,arguments)}else{if(bg==null||e){this.each(function(){var bi=e?bg:b(this).is(":hidden");b(this)[bi?"show":"hide"]()})}else{this.animate(aS("toggle",3),bg,bf,bh)}}return this},fadeTo:function(e,bh,bg,bf){return this.filter(":hidden").css("opacity",0).show().end().animate({opacity:bh},e,bg,bf)},animate:function(bi,bf,bh,bg){var e=b.speed(bf,bh,bg);if(b.isEmptyObject(bi)){return this.each(e.complete)}return this[e.queue===false?"each":"queue"](function(){var bl=b.extend({},e),bp,bm=this.nodeType===1,bn=bm&&b(this).is(":hidden"),bj=this;for(bp in bi){var bk=b.camelCase(bp);if(bp!==bk){bi[bk]=bi[bp];delete bi[bp];bp=bk}if(bi[bp]==="hide"&&bn||bi[bp]==="show"&&!bn){return bl.complete.call(this)}if(bm&&(bp==="height"||bp==="width")){bl.overflow=[this.style.overflow,this.style.overflowX,this.style.overflowY];if(b.css(this,"display")==="inline"&&b.css(this,"float")==="none"){if(!b.support.inlineBlockNeedsLayout){this.style.display="inline-block"}else{var bo=x(this.nodeName);if(bo==="inline"){this.style.display="inline-block"}else{this.style.display="inline";this.style.zoom=1}}}}if(b.isArray(bi[bp])){(bl.specialEasing=bl.specialEasing||{})[bp]=bi[bp][1];bi[bp]=bi[bp][0]}}if(bl.overflow!=null){this.style.overflow="hidden"}bl.curAnim=b.extend({},bi);b.each(bi,function(br,bv){var bu=new b.fx(bj,bl,br);if(aq.test(bv)){bu[bv==="toggle"?bn?"show":"hide":bv](bi)}else{var bt=aH.exec(bv),bw=bu.cur();if(bt){var bq=parseFloat(bt[2]),bs=bt[3]||(b.cssNumber[br]?"":"px");if(bs!=="px"){b.style(bj,br,(bq||1)+bs);bw=((bq||1)/bu.cur())*bw;b.style(bj,br,bw+bs)}if(bt[1]){bq=((bt[1]==="-="?-1:1)*bq)+bw}bu.custom(bw,bq,bs)}else{bu.custom(bw,bv,"")}}});return true})},stop:function(bf,e){var bg=b.timers;if(bf){this.queue([])}this.each(function(){for(var bh=bg.length-1;bh>=0;bh--){if(bg[bh].elem===this){if(e){bg[bh](true)}bg.splice(bh,1)}}});if(!e){this.dequeue()}return this}});function aS(bf,e){var bg={};b.each(ay.concat.apply([],ay.slice(0,e)),function(){bg[this]=bf});return bg}b.each({slideDown:aS("show",1),slideUp:aS("hide",1),slideToggle:aS("toggle",1),fadeIn:{opacity:"show"},fadeOut:{opacity:"hide"},fadeToggle:{opacity:"toggle"}},function(e,bf){b.fn[e]=function(bg,bi,bh){return this.animate(bf,bg,bi,bh)}});b.extend({speed:function(bg,bh,bf){var e=bg&&typeof bg==="object"?b.extend({},bg):{complete:bf||!bf&&bh||b.isFunction(bg)&&bg,duration:bg,easing:bf&&bh||bh&&!b.isFunction(bh)&&bh};e.duration=b.fx.off?0:typeof e.duration==="number"?e.duration:e.duration in b.fx.speeds?b.fx.speeds[e.duration]:b.fx.speeds._default;e.old=e.complete;e.complete=function(){if(e.queue!==false){b(this).dequeue()}if(b.isFunction(e.old)){e.old.call(this)}};return e},easing:{linear:function(bg,bh,e,bf){return e+bf*bg},swing:function(bg,bh,e,bf){return((-Math.cos(bg*Math.PI)/2)+0.5)*bf+e}},timers:[],fx:function(bf,e,bg){this.options=e;this.elem=bf;this.prop=bg;if(!e.orig){e.orig={}}}});b.fx.prototype={update:function(){if(this.options.step){this.options.step.call(this.elem,this.now,this)}(b.fx.step[this.prop]||b.fx.step._default)(this)},cur:function(){if(this.elem[this.prop]!=null&&(!this.elem.style||this.elem.style[this.prop]==null)){return this.elem[this.prop]}var e,bf=b.css(this.elem,this.prop);return isNaN(e=parseFloat(bf))?!bf||bf==="auto"?0:bf:e},custom:function(bj,bi,bh){var e=this,bg=b.fx;this.startTime=b.now();this.start=bj;this.end=bi;this.unit=bh||this.unit||(b.cssNumber[this.prop]?"":"px");this.now=this.start;this.pos=this.state=0;function bf(bk){return e.step(bk)}bf.elem=this.elem;if(bf()&&b.timers.push(bf)&&!aU){aU=setInterval(bg.tick,bg.interval)}},show:function(){this.options.orig[this.prop]=b.style(this.elem,this.prop);this.options.show=true;this.custom(this.prop==="width"||this.prop==="height"?1:0,this.cur());b(this.elem).show()},hide:function(){this.options.orig[this.prop]=b.style(this.elem,this.prop);this.options.hide=true;this.custom(this.cur(),0)},step:function(bh){var bm=b.now(),bi=true;if(bh||bm>=this.options.duration+this.startTime){this.now=this.end;this.pos=this.state=1;this.update();this.options.curAnim[this.prop]=true;for(var bj in this.options.curAnim){if(this.options.curAnim[bj]!==true){bi=false}}if(bi){if(this.options.overflow!=null&&!b.support.shrinkWrapBlocks){var bg=this.elem,bn=this.options;b.each(["","X","Y"],function(bo,bp){bg.style["overflow"+bp]=bn.overflow[bo]})}if(this.options.hide){b(this.elem).hide()}if(this.options.hide||this.options.show){for(var e in this.options.curAnim){b.style(this.elem,e,this.options.orig[e])}}this.options.complete.call(this.elem)}return false}else{var bf=bm-this.startTime;this.state=bf/this.options.duration;var bk=this.options.specialEasing&&this.options.specialEasing[this.prop];var bl=this.options.easing||(b.easing.swing?"swing":"linear");this.pos=b.easing[bk||bl](this.state,bf,0,1,this.options.duration);this.now=this.start+((this.end-this.start)*this.pos);this.update()}return true}};b.extend(b.fx,{tick:function(){var bf=b.timers;for(var e=0;e<bf.length;e++){if(!bf[e]()){bf.splice(e--,1)}}if(!bf.length){b.fx.stop()}},interval:13,stop:function(){clearInterval(aU);aU=null},speeds:{slow:600,fast:200,_default:400},step:{opacity:function(e){b.style(e.elem,"opacity",e.now)},_default:function(e){if(e.elem.style&&e.elem.style[e.prop]!=null){e.elem.style[e.prop]=(e.prop==="width"||e.prop==="height"?Math.max(0,e.now):e.now)+e.unit}else{e.elem[e.prop]=e.now}}}});if(b.expr&&b.expr.filters){b.expr.filters.animated=function(e){return b.grep(b.timers,function(bf){return e===bf.elem}).length}}function x(bg){if(!O[bg]){var e=b("<"+bg+">").appendTo("body"),bf=e.css("display");e.remove();if(bf==="none"||bf===""){bf="block"}O[bg]=bf}return O[bg]}var T=/^t(?:able|d|h)$/i,Z=/^(?:body|html)$/i;if("getBoundingClientRect" in am.documentElement){b.fn.offset=function(bs){var bi=this[0],bl;if(bs){return this.each(function(e){b.offset.setOffset(this,bs,e)})}if(!bi||!bi.ownerDocument){return null}if(bi===bi.ownerDocument.body){return b.offset.bodyOffset(bi)}try{bl=bi.getBoundingClientRect()}catch(bp){}var br=bi.ownerDocument,bg=br.documentElement;if(!bl||!b.contains(bg,bi)){return bl?{top:bl.top,left:bl.left}:{top:0,left:0}}var bm=br.body,bn=aB(br),bk=bg.clientTop||bm.clientTop||0,bo=bg.clientLeft||bm.clientLeft||0,bf=bn.pageYOffset||b.support.boxModel&&bg.scrollTop||bm.scrollTop,bj=bn.pageXOffset||b.support.boxModel&&bg.scrollLeft||bm.scrollLeft,bq=bl.top+bf-bk,bh=bl.left+bj-bo;return{top:bq,left:bh}}}else{b.fn.offset=function(bp){var bj=this[0];if(bp){return this.each(function(bq){b.offset.setOffset(this,bp,bq)})}if(!bj||!bj.ownerDocument){return null}if(bj===bj.ownerDocument.body){return b.offset.bodyOffset(bj)}b.offset.initialize();var bm,bg=bj.offsetParent,bf=bj,bo=bj.ownerDocument,bh=bo.documentElement,bk=bo.body,bl=bo.defaultView,e=bl?bl.getComputedStyle(bj,null):bj.currentStyle,bn=bj.offsetTop,bi=bj.offsetLeft;while((bj=bj.parentNode)&&bj!==bk&&bj!==bh){if(b.offset.supportsFixedPosition&&e.position==="fixed"){break}bm=bl?bl.getComputedStyle(bj,null):bj.currentStyle;bn-=bj.scrollTop;bi-=bj.scrollLeft;if(bj===bg){bn+=bj.offsetTop;bi+=bj.offsetLeft;if(b.offset.doesNotAddBorder&&!(b.offset.doesAddBorderForTableAndCells&&T.test(bj.nodeName))){bn+=parseFloat(bm.borderTopWidth)||0;bi+=parseFloat(bm.borderLeftWidth)||0}bf=bg;bg=bj.offsetParent}if(b.offset.subtractsBorderForOverflowNotVisible&&bm.overflow!=="visible"){bn+=parseFloat(bm.borderTopWidth)||0;bi+=parseFloat(bm.borderLeftWidth)||0}e=bm}if(e.position==="relative"||e.position==="static"){bn+=bk.offsetTop;bi+=bk.offsetLeft}if(b.offset.supportsFixedPosition&&e.position==="fixed"){bn+=Math.max(bh.scrollTop,bk.scrollTop);bi+=Math.max(bh.scrollLeft,bk.scrollLeft)}return{top:bn,left:bi}}}b.offset={initialize:function(){var e=am.body,bf=am.createElement("div"),bi,bk,bj,bl,bg=parseFloat(b.css(e,"marginTop"))||0,bh="<div style='position:absolute;top:0;left:0;margin:0;border:5px solid #000;padding:0;width:1px;height:1px;'><div></div></div><table style='position:absolute;top:0;left:0;margin:0;border:5px solid #000;padding:0;width:1px;height:1px;' cellpadding='0' cellspacing='0'><tr><td></td></tr></table>";b.extend(bf.style,{position:"absolute",top:0,left:0,margin:0,border:0,width:"1px",height:"1px",visibility:"hidden"});bf.innerHTML=bh;e.insertBefore(bf,e.firstChild);bi=bf.firstChild;bk=bi.firstChild;bl=bi.nextSibling.firstChild.firstChild;this.doesNotAddBorder=(bk.offsetTop!==5);this.doesAddBorderForTableAndCells=(bl.offsetTop===5);bk.style.position="fixed";bk.style.top="20px";this.supportsFixedPosition=(bk.offsetTop===20||bk.offsetTop===15);bk.style.position=bk.style.top="";bi.style.overflow="hidden";bi.style.position="relative";this.subtractsBorderForOverflowNotVisible=(bk.offsetTop===-5);this.doesNotIncludeMarginInBodyOffset=(e.offsetTop!==bg);e.removeChild(bf);b.offset.initialize=b.noop},bodyOffset:function(e){var bg=e.offsetTop,bf=e.offsetLeft;b.offset.initialize();if(b.offset.doesNotIncludeMarginInBodyOffset){bg+=parseFloat(b.css(e,"marginTop"))||0;bf+=parseFloat(b.css(e,"marginLeft"))||0}return{top:bg,left:bf}},setOffset:function(bh,bq,bk){var bl=b.css(bh,"position");if(bl==="static"){bh.style.position="relative"}var bj=b(bh),bf=bj.offset(),e=b.css(bh,"top"),bo=b.css(bh,"left"),bp=(bl==="absolute"||bl==="fixed")&&b.inArray("auto",[e,bo])>-1,bn={},bm={},bg,bi;if(bp){bm=bj.position()}bg=bp?bm.top:parseInt(e,10)||0;bi=bp?bm.left:parseInt(bo,10)||0;if(b.isFunction(bq)){bq=bq.call(bh,bk,bf)}if(bq.top!=null){bn.top=(bq.top-bf.top)+bg}if(bq.left!=null){bn.left=(bq.left-bf.left)+bi}if("using" in bq){bq.using.call(bh,bn)}else{bj.css(bn)}}};b.fn.extend({position:function(){if(!this[0]){return null}var bg=this[0],bf=this.offsetParent(),bh=this.offset(),e=Z.test(bf[0].nodeName)?{top:0,left:0}:bf.offset();bh.top-=parseFloat(b.css(bg,"marginTop"))||0;bh.left-=parseFloat(b.css(bg,"marginLeft"))||0;e.top+=parseFloat(b.css(bf[0],"borderTopWidth"))||0;e.left+=parseFloat(b.css(bf[0],"borderLeftWidth"))||0;return{top:bh.top-e.top,left:bh.left-e.left}},offsetParent:function(){return this.map(function(){var e=this.offsetParent||am.body;while(e&&(!Z.test(e.nodeName)&&b.css(e,"position")==="static")){e=e.offsetParent}return e})}});b.each(["Left","Top"],function(bf,e){var bg="scroll"+e;b.fn[bg]=function(bj){var bh=this[0],bi;if(!bh){return null}if(bj!==I){return this.each(function(){bi=aB(this);if(bi){bi.scrollTo(!bf?bj:b(bi).scrollLeft(),bf?bj:b(bi).scrollTop())}else{this[bg]=bj}})}else{bi=aB(bh);return bi?("pageXOffset" in bi)?bi[bf?"pageYOffset":"pageXOffset"]:b.support.boxModel&&bi.document.documentElement[bg]||bi.document.body[bg]:bh[bg]}}});function aB(e){return b.isWindow(e)?e:e.nodeType===9?e.defaultView||e.parentWindow:false}b.each(["Height","Width"],function(bf,e){var bg=e.toLowerCase();b.fn["inner"+e]=function(){return this[0]?parseFloat(b.css(this[0],bg,"padding")):null};b.fn["outer"+e]=function(bh){return this[0]?parseFloat(b.css(this[0],bg,bh?"margin":"border")):null};b.fn[bg]=function(bi){var bj=this[0];if(!bj){return bi==null?null:this}if(b.isFunction(bi)){return this.each(function(bn){var bm=b(this);bm[bg](bi.call(this,bn,bm[bg]()))})}if(b.isWindow(bj)){var bk=bj.document.documentElement["client"+e];return bj.document.compatMode==="CSS1Compat"&&bk||bj.document.body["client"+e]||bk}else{if(bj.nodeType===9){return Math.max(bj.documentElement["client"+e],bj.body["scroll"+e],bj.documentElement["scroll"+e],bj.body["offset"+e],bj.documentElement["offset"+e])}else{if(bi===I){var bl=b.css(bj,bg),bh=parseFloat(bl);return b.isNaN(bh)?bl:bh}else{return this.css(bg,typeof bi==="string"?bi:bi+"px")}}}}});a0.jQuery=a0.$=b})(window);
/*!
 * jQuery UI 1.8.16
 *
 * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI
 */
(function(c,j){function k(a,b){var d=a.nodeName.toLowerCase();if("area"===d){b=a.parentNode;d=b.name;if(!a.href||!d||b.nodeName.toLowerCase()!=="map")return false;a=c("img[usemap=#"+d+"]")[0];return!!a&&l(a)}return(/input|select|textarea|button|object/.test(d)?!a.disabled:"a"==d?a.href||b:b)&&l(a)}function l(a){return!c(a).parents().andSelf().filter(function(){return c.curCSS(this,"visibility")==="hidden"||c.expr.filters.hidden(this)}).length}c.ui=c.ui||{};if(!c.ui.version){c.extend(c.ui,{version:"https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/1.8.16",
keyCode:{ALT:18,BACKSPACE:8,CAPS_LOCK:20,COMMA:188,COMMAND:91,COMMAND_LEFT:91,COMMAND_RIGHT:93,CONTROL:17,DELETE:46,DOWN:40,END:35,ENTER:13,ESCAPE:27,HOME:36,INSERT:45,LEFT:37,MENU:93,NUMPAD_ADD:107,NUMPAD_DECIMAL:110,NUMPAD_DIVIDE:111,NUMPAD_ENTER:108,NUMPAD_MULTIPLY:106,NUMPAD_SUBTRACT:109,PAGE_DOWN:34,PAGE_UP:33,PERIOD:190,RIGHT:39,SHIFT:16,SPACE:32,TAB:9,UP:38,WINDOWS:91}});c.fn.extend({propAttr:c.fn.prop||c.fn.attr,_focus:c.fn.focus,focus:function(a,b){return typeof a==="number"?this.each(function(){var d=
this;setTimeout(function(){c(d).focus();b&&b.call(d)},a)}):this._focus.apply(this,arguments)},scrollParent:function(){var a;a=c.browser.msie&&/(static|relative)/.test(this.css("position"))||/absolute/.test(this.css("position"))?this.parents().filter(function(){return/(relative|absolute|fixed)/.test(c.curCSS(this,"position",1))&&/(auto|scroll)/.test(c.curCSS(this,"overflow",1)+c.curCSS(this,"overflow-y",1)+c.curCSS(this,"overflow-x",1))}).eq(0):this.parents().filter(function(){return/(auto|scroll)/.test(c.curCSS(this,
"overflow",1)+c.curCSS(this,"overflow-y",1)+c.curCSS(this,"overflow-x",1))}).eq(0);return/fixed/.test(this.css("position"))||!a.length?c(document):a},zIndex:function(a){if(a!==j)return this.css("zIndex",a);if(this.length){a=c(this[0]);for(var b;a.length&&a[0]!==document;){b=a.css("position");if(b==="absolute"||b==="relative"||b==="fixed"){b=parseInt(a.css("zIndex"),10);if(!isNaN(b)&&b!==0)return b}a=a.parent()}}return 0},disableSelection:function(){return this.bind((c.support.selectstart?"selectstart":
"mousedown")+".ui-disableSelection",function(a){a.preventDefault()})},enableSelection:function(){return this.unbind(".ui-disableSelection")}});c.each(["Width","Height"],function(a,b){function d(f,g,m,n){c.each(e,function(){g-=parseFloat(c.curCSS(f,"padding"+this,true))||0;if(m)g-=parseFloat(c.curCSS(f,"border"+this+"Width",true))||0;if(n)g-=parseFloat(c.curCSS(f,"margin"+this,true))||0});return g}var e=b==="Width"?["Left","Right"]:["Top","Bottom"],h=b.toLowerCase(),i={innerWidth:c.fn.innerWidth,innerHeight:c.fn.innerHeight,
outerWidth:c.fn.outerWidth,outerHeight:c.fn.outerHeight};c.fn["inner"+b]=function(f){if(f===j)return i["inner"+b].call(this);return this.each(function(){c(this).css(h,d(this,f)+"px")})};c.fn["outer"+b]=function(f,g){if(typeof f!=="number")return i["outer"+b].call(this,f);return this.each(function(){c(this).css(h,d(this,f,true,g)+"px")})}});c.extend(c.expr[":"],{data:function(a,b,d){return!!c.data(a,d[3])},focusable:function(a){return k(a,!isNaN(c.attr(a,"tabindex")))},tabbable:function(a){var b=c.attr(a,
"tabindex"),d=isNaN(b);return(d||b>=0)&&k(a,!d)}});c(function(){var a=document.body,b=a.appendChild(b=document.createElement("div"));c.extend(b.style,{minHeight:"100px",height:"auto",padding:0,borderWidth:0});c.support.minHeight=b.offsetHeight===100;c.support.selectstart="onselectstart"in b;a.removeChild(b).style.display="none"});c.extend(c.ui,{plugin:{add:function(a,b,d){a=c.ui[a].prototype;for(var e in d){a.plugins[e]=a.plugins[e]||[];a.plugins[e].push([b,d[e]])}},call:function(a,b,d){if((b=a.plugins[b])&&
a.element[0].parentNode)for(var e=0;e<b.length;e++)a.options[b[e][0]]&&b[e][1].apply(a.element,d)}},contains:function(a,b){return document.compareDocumentPosition?a.compareDocumentPosition(b)&16:a!==b&&a.contains(b)},hasScroll:function(a,b){if(c(a).css("overflow")==="hidden")return false;b=b&&b==="left"?"scrollLeft":"scrollTop";var d=false;if(a[b]>0)return true;a[b]=1;d=a[b]>0;a[b]=0;return d},isOverAxis:function(a,b,d){return a>b&&a<b+d},isOver:function(a,b,d,e,h,i){return c.ui.isOverAxis(a,d,h)&&
c.ui.isOverAxis(b,e,i)}})}})(jQuery);
;/*!
 * jQuery UI Widget 1.8.16
 *
 * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI/Widget
 */
(function(b,j){if(b.cleanData){var k=b.cleanData;b.cleanData=function(a){for(var c=0,d;(d=a[c])!=null;c++)try{b(d).triggerHandler("remove")}catch(e){}k(a)}}else{var l=b.fn.remove;b.fn.remove=function(a,c){return this.each(function(){if(!c)if(!a||b.filter(a,[this]).length)b("*",this).add([this]).each(function(){try{b(this).triggerHandler("remove")}catch(d){}});return l.call(b(this),a,c)})}}b.widget=function(a,c,d){var e=a.split(".")[0],f;a=a.split(".")[1];f=e+"-"+a;if(!d){d=c;c=b.Widget}b.expr[":"][f]=
function(h){return!!b.data(h,a)};b[e]=b[e]||{};b[e][a]=function(h,g){arguments.length&&this._createWidget(h,g)};c=new c;c.options=b.extend(true,{},c.options);b[e][a].prototype=b.extend(true,c,{namespace:e,widgetName:a,widgetEventPrefix:b[e][a].prototype.widgetEventPrefix||a,widgetBaseClass:f},d);b.widget.bridge(a,b[e][a])};b.widget.bridge=function(a,c){b.fn[a]=function(d){var e=typeof d==="string",f=Array.prototype.slice.call(arguments,1),h=this;d=!e&&f.length?b.extend.apply(null,[true,d].concat(f)):
d;if(e&&d.charAt(0)==="_")return h;e?this.each(function(){var g=b.data(this,a),i=g&&b.isFunction(g[d])?g[d].apply(g,f):g;if(i!==g&&i!==j){h=i;return false}}):this.each(function(){var g=b.data(this,a);g?g.option(d||{})._init():b.data(this,a,new c(d,this))});return h}};b.Widget=function(a,c){arguments.length&&this._createWidget(a,c)};b.Widget.prototype={widgetName:"widget",widgetEventPrefix:"",options:{disabled:false},_createWidget:function(a,c){b.data(c,this.widgetName,this);this.element=b(c);this.options=
b.extend(true,{},this.options,this._getCreateOptions(),a);var d=this;this.element.bind("remove."+this.widgetName,function(){d.destroy()});this._create();this._trigger("create");this._init()},_getCreateOptions:function(){return b.metadata&&b.metadata.get(this.element[0])[this.widgetName]},_create:function(){},_init:function(){},destroy:function(){this.element.unbind("."+this.widgetName).removeData(this.widgetName);this.widget().unbind("."+this.widgetName).removeAttr("aria-disabled").removeClass(this.widgetBaseClass+
"-disabled ui-state-disabled")},widget:function(){return this.element},option:function(a,c){var d=a;if(arguments.length===0)return b.extend({},this.options);if(typeof a==="string"){if(c===j)return this.options[a];d={};d[a]=c}this._setOptions(d);return this},_setOptions:function(a){var c=this;b.each(a,function(d,e){c._setOption(d,e)});return this},_setOption:function(a,c){this.options[a]=c;if(a==="disabled")this.widget()[c?"addClass":"removeClass"](this.widgetBaseClass+"-disabled ui-state-disabled").attr("aria-disabled",
c);return this},enable:function(){return this._setOption("disabled",false)},disable:function(){return this._setOption("disabled",true)},_trigger:function(a,c,d){var e=this.options[a];c=b.Event(c);c.type=(a===this.widgetEventPrefix?a:this.widgetEventPrefix+a).toLowerCase();d=d||{};if(c.originalEvent){a=b.event.props.length;for(var f;a;){f=b.event.props[--a];c[f]=c.originalEvent[f]}}this.element.trigger(c,d);return!(b.isFunction(e)&&e.call(this.element[0],c,d)===false||c.isDefaultPrevented())}}})(jQuery);
;/*
 * jQuery UI Accordion 1.8.16
 *
 * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI/Accordion
 *
 * Depends:
 *	jquery.ui.core.js
 *	jquery.ui.widget.js
 */
(function(c){c.widget("ui.accordion",{options:{active:0,animated:"slide",autoHeight:true,clearStyle:false,collapsible:false,event:"click",fillSpace:false,header:"> li > :first-child,> :not(li):even",icons:{header:"ui-icon-triangle-1-e",headerSelected:"ui-icon-triangle-1-s"},navigation:false,navigationFilter:function(){return this.href.toLowerCase()===location.href.toLowerCase()}},_create:function(){var a=this,b=a.options;a.running=0;a.element.addClass("ui-accordion ui-widget ui-helper-reset").children("li").addClass("ui-accordion-li-fix");
a.headers=a.element.find(b.header).addClass("ui-accordion-header ui-helper-reset ui-state-default ui-corner-all").bind("mouseenter.accordion",function(){b.disabled||c(this).addClass("ui-state-hover")}).bind("mouseleave.accordion",function(){b.disabled||c(this).removeClass("ui-state-hover")}).bind("focus.accordion",function(){b.disabled||c(this).addClass("ui-state-focus")}).bind("blur.accordion",function(){b.disabled||c(this).removeClass("ui-state-focus")});a.headers.next().addClass("ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom");
if(b.navigation){var d=a.element.find("a").filter(b.navigationFilter).eq(0);if(d.length){var h=d.closest(".ui-accordion-header");a.active=h.length?h:d.closest(".ui-accordion-content").prev()}}a.active=a._findActive(a.active||b.active).addClass("ui-state-default ui-state-active").toggleClass("ui-corner-all").toggleClass("ui-corner-top");a.active.next().addClass("ui-accordion-content-active");a._createIcons();a.resize();a.element.attr("role","tablist");a.headers.attr("role","tab").bind("keydown.accordion",
function(f){return a._keydown(f)}).next().attr("role","tabpanel");a.headers.not(a.active||"").attr({"aria-expanded":"false","aria-selected":"false",tabIndex:-1}).next().hide();a.active.length?a.active.attr({"aria-expanded":"true","aria-selected":"true",tabIndex:0}):a.headers.eq(0).attr("tabIndex",0);c.browser.safari||a.headers.find("a").attr("tabIndex",-1);b.event&&a.headers.bind(b.event.split(" ").join(".accordion ")+".accordion",function(f){a._clickHandler.call(a,f,this);f.preventDefault()})},_createIcons:function(){var a=
this.options;if(a.icons){c("<span></span>").addClass("ui-icon "+a.icons.header).prependTo(this.headers);this.active.children(".ui-icon").toggleClass(a.icons.header).toggleClass(a.icons.headerSelected);this.element.addClass("ui-accordion-icons")}},_destroyIcons:function(){this.headers.children(".ui-icon").remove();this.element.removeClass("ui-accordion-icons")},destroy:function(){var a=this.options;this.element.removeClass("ui-accordion ui-widget ui-helper-reset").removeAttr("role");this.headers.unbind(".accordion").removeClass("ui-accordion-header ui-accordion-disabled ui-helper-reset ui-state-default ui-corner-all ui-state-active ui-state-disabled ui-corner-top").removeAttr("role").removeAttr("aria-expanded").removeAttr("aria-selected").removeAttr("tabIndex");
this.headers.find("a").removeAttr("tabIndex");this._destroyIcons();var b=this.headers.next().css("display","").removeAttr("role").removeClass("ui-helper-reset ui-widget-content ui-corner-bottom ui-accordion-content ui-accordion-content-active ui-accordion-disabled ui-state-disabled");if(a.autoHeight||a.fillHeight)b.css("height","");return c.Widget.prototype.destroy.call(this)},_setOption:function(a,b){c.Widget.prototype._setOption.apply(this,arguments);a=="active"&&this.activate(b);if(a=="icons"){this._destroyIcons();
b&&this._createIcons()}if(a=="disabled")this.headers.add(this.headers.next())[b?"addClass":"removeClass"]("ui-accordion-disabled ui-state-disabled")},_keydown:function(a){if(!(this.options.disabled||a.altKey||a.ctrlKey)){var b=c.ui.keyCode,d=this.headers.length,h=this.headers.index(a.target),f=false;switch(a.keyCode){case b.RIGHT:case b.DOWN:f=this.headers[(h+1)%d];break;case b.LEFT:case b.UP:f=this.headers[(h-1+d)%d];break;case b.SPACE:case b.ENTER:this._clickHandler({target:a.target},a.target);
a.preventDefault()}if(f){c(a.target).attr("tabIndex",-1);c(f).attr("tabIndex",0);f.focus();return false}return true}},resize:function(){var a=this.options,b;if(a.fillSpace){if(c.browser.msie){var d=this.element.parent().css("overflow");this.element.parent().css("overflow","hidden")}b=this.element.parent().height();c.browser.msie&&this.element.parent().css("overflow",d);this.headers.each(function(){b-=c(this).outerHeight(true)});this.headers.next().each(function(){c(this).height(Math.max(0,b-c(this).innerHeight()+
c(this).height()))}).css("overflow","auto")}else if(a.autoHeight){b=0;this.headers.next().each(function(){b=Math.max(b,c(this).height("").height())}).height(b)}return this},activate:function(a){this.options.active=a;a=this._findActive(a)[0];this._clickHandler({target:a},a);return this},_findActive:function(a){return a?typeof a==="number"?this.headers.filter(":eq("+a+")"):this.headers.not(this.headers.not(a)):a===false?c([]):this.headers.filter(":eq(0)")},_clickHandler:function(a,b){var d=this.options;
if(!d.disabled)if(a.target){a=c(a.currentTarget||b);b=a[0]===this.active[0];d.active=d.collapsible&&b?false:this.headers.index(a);if(!(this.running||!d.collapsible&&b)){var h=this.active;j=a.next();g=this.active.next();e={options:d,newHeader:b&&d.collapsible?c([]):a,oldHeader:this.active,newContent:b&&d.collapsible?c([]):j,oldContent:g};var f=this.headers.index(this.active[0])>this.headers.index(a[0]);this.active=b?c([]):a;this._toggle(j,g,e,b,f);h.removeClass("ui-state-active ui-corner-top").addClass("ui-state-default ui-corner-all").children(".ui-icon").removeClass(d.icons.headerSelected).addClass(d.icons.header);
if(!b){a.removeClass("ui-state-default ui-corner-all").addClass("ui-state-active ui-corner-top").children(".ui-icon").removeClass(d.icons.header).addClass(d.icons.headerSelected);a.next().addClass("ui-accordion-content-active")}}}else if(d.collapsible){this.active.removeClass("ui-state-active ui-corner-top").addClass("ui-state-default ui-corner-all").children(".ui-icon").removeClass(d.icons.headerSelected).addClass(d.icons.header);this.active.next().addClass("ui-accordion-content-active");var g=this.active.next(),
e={options:d,newHeader:c([]),oldHeader:d.active,newContent:c([]),oldContent:g},j=this.active=c([]);this._toggle(j,g,e)}},_toggle:function(a,b,d,h,f){var g=this,e=g.options;g.toShow=a;g.toHide=b;g.data=d;var j=function(){if(g)return g._completed.apply(g,arguments)};g._trigger("changestart",null,g.data);g.running=b.size()===0?a.size():b.size();if(e.animated){d={};d=e.collapsible&&h?{toShow:c([]),toHide:b,complete:j,down:f,autoHeight:e.autoHeight||e.fillSpace}:{toShow:a,toHide:b,complete:j,down:f,autoHeight:e.autoHeight||
e.fillSpace};if(!e.proxied)e.proxied=e.animated;if(!e.proxiedDuration)e.proxiedDuration=e.duration;e.animated=c.isFunction(e.proxied)?e.proxied(d):e.proxied;e.duration=c.isFunction(e.proxiedDuration)?e.proxiedDuration(d):e.proxiedDuration;h=c.ui.accordion.animations;var i=e.duration,k=e.animated;if(k&&!h[k]&&!c.easing[k])k="slide";h[k]||(h[k]=function(l){this.slide(l,{easing:k,duration:i||700})});h[k](d)}else{if(e.collapsible&&h)a.toggle();else{b.hide();a.show()}j(true)}b.prev().attr({"aria-expanded":"false",
"aria-selected":"false",tabIndex:-1}).blur();a.prev().attr({"aria-expanded":"true","aria-selected":"true",tabIndex:0}).focus()},_completed:function(a){this.running=a?0:--this.running;if(!this.running){this.options.clearStyle&&this.toShow.add(this.toHide).css({height:"",overflow:""});this.toHide.removeClass("ui-accordion-content-active");if(this.toHide.length)this.toHide.parent()[0].className=this.toHide.parent()[0].className;this._trigger("change",null,this.data)}}});c.extend(c.ui.accordion,{version:"https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/1.8.16",
animations:{slide:function(a,b){a=c.extend({easing:"swing",duration:300},a,b);if(a.toHide.size())if(a.toShow.size()){var d=a.toShow.css("overflow"),h=0,f={},g={},e;b=a.toShow;e=b[0].style.width;b.width(parseInt(b.parent().width(),10)-parseInt(b.css("paddingLeft"),10)-parseInt(b.css("paddingRight"),10)-(parseInt(b.css("borderLeftWidth"),10)||0)-(parseInt(b.css("borderRightWidth"),10)||0));c.each(["height","paddingTop","paddingBottom"],function(j,i){g[i]="hide";j=(""+c.css(a.toShow[0],i)).match(/^([\d+-.]+)(.*)$/);
f[i]={value:j[1],unit:j[2]||"px"}});a.toShow.css({height:0,overflow:"hidden"}).show();a.toHide.filter(":hidden").each(a.complete).end().filter(":visible").animate(g,{step:function(j,i){if(i.prop=="height")h=i.end-i.start===0?0:(i.now-i.start)/(i.end-i.start);a.toShow[0].style[i.prop]=h*f[i.prop].value+f[i.prop].unit},duration:a.duration,easing:a.easing,complete:function(){a.autoHeight||a.toShow.css("height","");a.toShow.css({width:e,overflow:d});a.complete()}})}else a.toHide.animate({height:"hide",
paddingTop:"hide",paddingBottom:"hide"},a);else a.toShow.animate({height:"show",paddingTop:"show",paddingBottom:"show"},a)},bounceslide:function(a){this.slide(a,{easing:a.down?"easeOutBounce":"swing",duration:a.down?1E3:200})}}})})(jQuery);
;/*
 * jQuery UI Tabs 1.8.16
 *
 * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI/Tabs
 *
 * Depends:
 *	jquery.ui.core.js
 *	jquery.ui.widget.js
 */
(function(d,p){function u(){return++v}function w(){return++x}var v=0,x=0;d.widget("https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/ui.tabs",{options:{add:null,ajaxOptions:null,cache:false,cookie:null,collapsible:false,disable:null,disabled:[],enable:null,event:"click",fx:null,idPrefix:"ui-tabs-",load:null,panelTemplate:"<div></div>",remove:null,select:null,show:null,spinner:"<em>Loading&#8230;</em>",tabTemplate:"<li><a href='#{href}'><span>#{label}</span></a></li>"},_create:function(){this._tabify(true)},_setOption:function(b,e){if(b=="selected")this.options.collapsible&&
e==this.options.selected||this.select(e);else{this.options[b]=e;this._tabify()}},_tabId:function(b){return b.title&&b.title.replace(/\s/g,"_").replace(/[^\w\u00c0-\uFFFF-]/g,"")||this.options.idPrefix+u()},_sanitizeSelector:function(b){return b.replace(/:/g,"\\:")},_cookie:function(){var b=this.cookie||(this.cookie=this.options.cookie.name||"ui-tabs-"+w());return d.cookie.apply(null,[b].concat(d.makeArray(arguments)))},_ui:function(b,e){return{tab:b,panel:e,index:this.anchors.index(b)}},_cleanup:function(){this.lis.filter(".ui-state-processing").removeClass("ui-state-processing").find("span:data(label.tabs)").each(function(){var b=
d(this);b.html(b.data("https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/label.tabs")).removeData("https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/label.tabs")})},_tabify:function(b){function e(g,f){g.css("display","");!d.support.opacity&&f.opacity&&g[0].style.removeAttribute("filter")}var a=this,c=this.options,h=/^#.+/;this.list=this.element.find("ol,ul").eq(0);this.lis=d(" > li:has(a[href])",this.list);this.anchors=this.lis.map(function(){return d("a",this)[0]});this.panels=d([]);this.anchors.each(function(g,f){var i=d(f).attr("href"),l=i.split("#")[0],q;if(l&&(l===location.toString().split("#")[0]||
(q=d("base")[0])&&l===q.href)){i=f.hash;f.href=i}if(h.test(i))a.panels=a.panels.add(a.element.find(a._sanitizeSelector(i)));else if(i&&i!=="#"){d.data(f,"https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/href.tabs",i);d.data(f,"https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/load.tabs",i.replace(/#.*$/,""));i=a._tabId(f);f.href="#"+i;f=a.element.find("#"+i);if(!f.length){f=d(c.panelTemplate).attr("id",i).addClass("ui-tabs-panel ui-widget-content ui-corner-bottom").insertAfter(a.panels[g-1]||a.list);f.data("https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/destroy.tabs",true)}a.panels=a.panels.add(f)}else c.disabled.push(g)});if(b){this.element.addClass("ui-tabs ui-widget ui-widget-content ui-corner-all");
this.list.addClass("ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all");this.lis.addClass("ui-state-default ui-corner-top");this.panels.addClass("ui-tabs-panel ui-widget-content ui-corner-bottom");if(c.selected===p){location.hash&&this.anchors.each(function(g,f){if(f.hash==location.hash){c.selected=g;return false}});if(typeof c.selected!=="number"&&c.cookie)c.selected=parseInt(a._cookie(),10);if(typeof c.selected!=="number"&&this.lis.filter(".ui-tabs-selected").length)c.selected=
this.lis.index(this.lis.filter(".ui-tabs-selected"));c.selected=c.selected||(this.lis.length?0:-1)}else if(c.selected===null)c.selected=-1;c.selected=c.selected>=0&&this.anchors[c.selected]||c.selected<0?c.selected:0;c.disabled=d.unique(c.disabled.concat(d.map(this.lis.filter(".ui-state-disabled"),function(g){return a.lis.index(g)}))).sort();d.inArray(c.selected,c.disabled)!=-1&&c.disabled.splice(d.inArray(c.selected,c.disabled),1);this.panels.addClass("ui-tabs-hide");this.lis.removeClass("ui-tabs-selected ui-state-active");
if(c.selected>=0&&this.anchors.length){a.element.find(a._sanitizeSelector(a.anchors[c.selected].hash)).removeClass("ui-tabs-hide");this.lis.eq(c.selected).addClass("ui-tabs-selected ui-state-active");a.element.queue("tabs",function(){a._trigger("show",null,a._ui(a.anchors[c.selected],a.element.find(a._sanitizeSelector(a.anchors[c.selected].hash))[0]))});this.load(c.selected)}d(window).bind("unload",function(){a.lis.add(a.anchors).unbind(".tabs");a.lis=a.anchors=a.panels=null})}else c.selected=this.lis.index(this.lis.filter(".ui-tabs-selected"));
this.element[c.collapsible?"addClass":"removeClass"]("ui-tabs-collapsible");c.cookie&&this._cookie(c.selected,c.cookie);b=0;for(var j;j=this.lis[b];b++)d(j)[d.inArray(b,c.disabled)!=-1&&!d(j).hasClass("ui-tabs-selected")?"addClass":"removeClass"]("ui-state-disabled");c.cache===false&&this.anchors.removeData("https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/cache.tabs");this.lis.add(this.anchors).unbind(".tabs");if(c.event!=="mouseover"){var k=function(g,f){f.is(":not(.ui-state-disabled)")&&f.addClass("ui-state-"+g)},n=function(g,f){f.removeClass("ui-state-"+
g)};this.lis.bind("https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/mouseover.tabs",function(){k("hover",d(this))});this.lis.bind("https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/mouseout.tabs",function(){n("hover",d(this))});this.anchors.bind("https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/focus.tabs",function(){k("focus",d(this).closest("li"))});this.anchors.bind("https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/blur.tabs",function(){n("focus",d(this).closest("li"))})}var m,o;if(c.fx)if(d.isArray(c.fx)){m=c.fx[0];o=c.fx[1]}else m=o=c.fx;var r=o?function(g,f){d(g).closest("li").addClass("ui-tabs-selected ui-state-active");f.hide().removeClass("ui-tabs-hide").animate(o,o.duration||"normal",
function(){e(f,o);a._trigger("show",null,a._ui(g,f[0]))})}:function(g,f){d(g).closest("li").addClass("ui-tabs-selected ui-state-active");f.removeClass("ui-tabs-hide");a._trigger("show",null,a._ui(g,f[0]))},s=m?function(g,f){f.animate(m,m.duration||"normal",function(){a.lis.removeClass("ui-tabs-selected ui-state-active");f.addClass("ui-tabs-hide");e(f,m);a.element.dequeue("tabs")})}:function(g,f){a.lis.removeClass("ui-tabs-selected ui-state-active");f.addClass("ui-tabs-hide");a.element.dequeue("tabs")};
this.anchors.bind(c.event+".tabs",function(){var g=this,f=d(g).closest("li"),i=a.panels.filter(":not(.ui-tabs-hide)"),l=a.element.find(a._sanitizeSelector(g.hash));if(f.hasClass("ui-tabs-selected")&&!c.collapsible||f.hasClass("ui-state-disabled")||f.hasClass("ui-state-processing")||a.panels.filter(":animated").length||a._trigger("select",null,a._ui(this,l[0]))===false){this.blur();return false}c.selected=a.anchors.index(this);a.abort();if(c.collapsible)if(f.hasClass("ui-tabs-selected")){c.selected=
-1;c.cookie&&a._cookie(c.selected,c.cookie);a.element.queue("tabs",function(){s(g,i)}).dequeue("tabs");this.blur();return false}else if(!i.length){c.cookie&&a._cookie(c.selected,c.cookie);a.element.queue("tabs",function(){r(g,l)});a.load(a.anchors.index(this));this.blur();return false}c.cookie&&a._cookie(c.selected,c.cookie);if(l.length){i.length&&a.element.queue("tabs",function(){s(g,i)});a.element.queue("tabs",function(){r(g,l)});a.load(a.anchors.index(this))}else throw"jQuery UI Tabs: Mismatching fragment identifier.";
d.browser.msie&&this.blur()});this.anchors.bind("https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/click.tabs",function(){return false})},_getIndex:function(b){if(typeof b=="string")b=this.anchors.index(this.anchors.filter("[href$="+b+"]"));return b},destroy:function(){var b=this.options;this.abort();this.element.unbind(".tabs").removeClass("ui-tabs ui-widget ui-widget-content ui-corner-all ui-tabs-collapsible").removeData("tabs");this.list.removeClass("ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all");this.anchors.each(function(){var e=
d.data(this,"https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/href.tabs");if(e)this.href=e;var a=d(this).unbind(".tabs");d.each(["href","load","cache"],function(c,h){a.removeData(h+".tabs")})});this.lis.unbind(".tabs").add(this.panels).each(function(){d.data(this,"https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/destroy.tabs")?d(this).remove():d(this).removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active ui-state-hover ui-state-focus ui-state-disabled ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide")});b.cookie&&this._cookie(null,b.cookie);return this},add:function(b,
e,a){if(a===p)a=this.anchors.length;var c=this,h=this.options;e=d(h.tabTemplate.replace(/#\{href\}/g,b).replace(/#\{label\}/g,e));b=!b.indexOf("#")?b.replace("#",""):this._tabId(d("a",e)[0]);e.addClass("ui-state-default ui-corner-top").data("https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/destroy.tabs",true);var j=c.element.find("#"+b);j.length||(j=d(h.panelTemplate).attr("id",b).data("https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/destroy.tabs",true));j.addClass("ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide");if(a>=this.lis.length){e.appendTo(this.list);j.appendTo(this.list[0].parentNode)}else{e.insertBefore(this.lis[a]);
j.insertBefore(this.panels[a])}h.disabled=d.map(h.disabled,function(k){return k>=a?++k:k});this._tabify();if(this.anchors.length==1){h.selected=0;e.addClass("ui-tabs-selected ui-state-active");j.removeClass("ui-tabs-hide");this.element.queue("tabs",function(){c._trigger("show",null,c._ui(c.anchors[0],c.panels[0]))});this.load(0)}this._trigger("add",null,this._ui(this.anchors[a],this.panels[a]));return this},remove:function(b){b=this._getIndex(b);var e=this.options,a=this.lis.eq(b).remove(),c=this.panels.eq(b).remove();
if(a.hasClass("ui-tabs-selected")&&this.anchors.length>1)this.select(b+(b+1<this.anchors.length?1:-1));e.disabled=d.map(d.grep(e.disabled,function(h){return h!=b}),function(h){return h>=b?--h:h});this._tabify();this._trigger("remove",null,this._ui(a.find("a")[0],c[0]));return this},enable:function(b){b=this._getIndex(b);var e=this.options;if(d.inArray(b,e.disabled)!=-1){this.lis.eq(b).removeClass("ui-state-disabled");e.disabled=d.grep(e.disabled,function(a){return a!=b});this._trigger("enable",null,
this._ui(this.anchors[b],this.panels[b]));return this}},disable:function(b){b=this._getIndex(b);var e=this.options;if(b!=e.selected){this.lis.eq(b).addClass("ui-state-disabled");e.disabled.push(b);e.disabled.sort();this._trigger("disable",null,this._ui(this.anchors[b],this.panels[b]))}return this},select:function(b){b=this._getIndex(b);if(b==-1)if(this.options.collapsible&&this.options.selected!=-1)b=this.options.selected;else return this;this.anchors.eq(b).trigger(this.options.event+".tabs");return this},
load:function(b){b=this._getIndex(b);var e=this,a=this.options,c=this.anchors.eq(b)[0],h=d.data(c,"https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/load.tabs");this.abort();if(!h||this.element.queue("tabs").length!==0&&d.data(c,"https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/cache.tabs"))this.element.dequeue("tabs");else{this.lis.eq(b).addClass("ui-state-processing");if(a.spinner){var j=d("span",c);j.data("https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/label.tabs",j.html()).html(a.spinner)}this.xhr=d.ajax(d.extend({},a.ajaxOptions,{url:h,success:function(k,n){e.element.find(e._sanitizeSelector(c.hash)).html(k);e._cleanup();a.cache&&d.data(c,
"https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/cache.tabs",true);e._trigger("load",null,e._ui(e.anchors[b],e.panels[b]));try{a.ajaxOptions.success(k,n)}catch(m){}},error:function(k,n){e._cleanup();e._trigger("load",null,e._ui(e.anchors[b],e.panels[b]));try{a.ajaxOptions.error(k,n,b,c)}catch(m){}}}));e.element.dequeue("tabs");return this}},abort:function(){this.element.queue([]);this.panels.stop(false,true);this.element.queue("tabs",this.element.queue("tabs").splice(-2,2));if(this.xhr){this.xhr.abort();delete this.xhr}this._cleanup();return this},
url:function(b,e){this.anchors.eq(b).removeData("https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/cache.tabs").data("https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/load.tabs",e);return this},length:function(){return this.anchors.length}});d.extend(d.ui.tabs,{version:"https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/1.8.16"});d.extend(d.ui.tabs.prototype,{rotation:null,rotate:function(b,e){var a=this,c=this.options,h=a._rotate||(a._rotate=function(j){clearTimeout(a.rotation);a.rotation=setTimeout(function(){var k=c.selected;a.select(++k<a.anchors.length?k:0)},b);j&&j.stopPropagation()});e=a._unrotate||(a._unrotate=!e?function(j){j.clientX&&
a.rotate(null)}:function(){t=c.selected;h()});if(b){this.element.bind("tabsshow",h);this.anchors.bind(c.event+".tabs",e);h()}else{clearTimeout(a.rotation);this.element.unbind("tabsshow",h);this.anchors.unbind(c.event+".tabs",e);delete this._rotate;delete this._unrotate}return this}})})(jQuery);
;/*
 * jQuery UI Effects 1.8.16
 *
 * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI/Effects/
 */
jQuery.effects||function(f,j){function m(c){var a;if(c&&c.constructor==Array&&c.length==3)return c;if(a=/rgb\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*\)/.exec(c))return[parseInt(a[1],10),parseInt(a[2],10),parseInt(a[3],10)];if(a=/rgb\(\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*\)/.exec(c))return[parseFloat(a[1])*2.55,parseFloat(a[2])*2.55,parseFloat(a[3])*2.55];if(a=/#([a-fA-F0-9]{2})([a-fA-F0-9]{2})([a-fA-F0-9]{2})/.exec(c))return[parseInt(a[1],
16),parseInt(a[2],16),parseInt(a[3],16)];if(a=/#([a-fA-F0-9])([a-fA-F0-9])([a-fA-F0-9])/.exec(c))return[parseInt(a[1]+a[1],16),parseInt(a[2]+a[2],16),parseInt(a[3]+a[3],16)];if(/rgba\(0, 0, 0, 0\)/.exec(c))return n.transparent;return n[f.trim(c).toLowerCase()]}function s(c,a){var b;do{b=f.curCSS(c,a);if(b!=""&&b!="transparent"||f.nodeName(c,"body"))break;a="backgroundColor"}while(c=c.parentNode);return m(b)}function o(){var c=document.defaultView?document.defaultView.getComputedStyle(this,null):this.currentStyle,
a={},b,d;if(c&&c.length&&c[0]&&c[c[0]])for(var e=c.length;e--;){b=c[e];if(typeof c[b]=="string"){d=b.replace(/\-(\w)/g,function(g,h){return h.toUpperCase()});a[d]=c[b]}}else for(b in c)if(typeof c[b]==="string")a[b]=c[b];return a}function p(c){var a,b;for(a in c){b=c[a];if(b==null||f.isFunction(b)||a in t||/scrollbar/.test(a)||!/color/i.test(a)&&isNaN(parseFloat(b)))delete c[a]}return c}function u(c,a){var b={_:0},d;for(d in a)if(c[d]!=a[d])b[d]=a[d];return b}function k(c,a,b,d){if(typeof c=="object"){d=
a;b=null;a=c;c=a.effect}if(f.isFunction(a)){d=a;b=null;a={}}if(typeof a=="number"||f.fx.speeds[a]){d=b;b=a;a={}}if(f.isFunction(b)){d=b;b=null}a=a||{};b=b||a.duration;b=f.fx.off?0:typeof b=="number"?b:b in f.fx.speeds?f.fx.speeds[b]:f.fx.speeds._default;d=d||a.complete;return[c,a,b,d]}function l(c){if(!c||typeof c==="number"||f.fx.speeds[c])return true;if(typeof c==="string"&&!f.effects[c])return true;return false}f.effects={};f.each(["backgroundColor","borderBottomColor","borderLeftColor","borderRightColor",
"borderTopColor","borderColor","color","outlineColor"],function(c,a){f.fx.step[a]=function(b){if(!b.colorInit){b.start=s(b.elem,a);b.end=m(b.end);b.colorInit=true}b.elem.style[a]="rgb("+Math.max(Math.min(parseInt(b.pos*(b.end[0]-b.start[0])+b.start[0],10),255),0)+","+Math.max(Math.min(parseInt(b.pos*(b.end[1]-b.start[1])+b.start[1],10),255),0)+","+Math.max(Math.min(parseInt(b.pos*(b.end[2]-b.start[2])+b.start[2],10),255),0)+")"}});var n={aqua:[0,255,255],azure:[240,255,255],beige:[245,245,220],black:[0,
0,0],blue:[0,0,255],brown:[165,42,42],cyan:[0,255,255],darkblue:[0,0,139],darkcyan:[0,139,139],darkgrey:[169,169,169],darkgreen:[0,100,0],darkkhaki:[189,183,107],darkmagenta:[139,0,139],darkolivegreen:[85,107,47],darkorange:[255,140,0],darkorchid:[153,50,204],darkred:[139,0,0],darksalmon:[233,150,122],darkviolet:[148,0,211],fuchsia:[255,0,255],gold:[255,215,0],green:[0,128,0],indigo:[75,0,130],khaki:[240,230,140],lightblue:[173,216,230],lightcyan:[224,255,255],lightgreen:[144,238,144],lightgrey:[211,
211,211],lightpink:[255,182,193],lightyellow:[255,255,224],lime:[0,255,0],magenta:[255,0,255],maroon:[128,0,0],navy:[0,0,128],olive:[128,128,0],orange:[255,165,0],pink:[255,192,203],purple:[128,0,128],violet:[128,0,128],red:[255,0,0],silver:[192,192,192],white:[255,255,255],yellow:[255,255,0],transparent:[255,255,255]},q=["add","remove","toggle"],t={border:1,borderBottom:1,borderColor:1,borderLeft:1,borderRight:1,borderTop:1,borderWidth:1,margin:1,padding:1};f.effects.animateClass=function(c,a,b,
d){if(f.isFunction(b)){d=b;b=null}return this.queue(function(){var e=f(this),g=e.attr("style")||" ",h=p(o.call(this)),r,v=e.attr("class");f.each(q,function(w,i){c[i]&&e[i+"Class"](c[i])});r=p(o.call(this));e.attr("class",v);e.animate(u(h,r),{queue:false,duration:a,easing:b,complete:function(){f.each(q,function(w,i){c[i]&&e[i+"Class"](c[i])});if(typeof e.attr("style")=="object"){e.attr("style").cssText="";e.attr("style").cssText=g}else e.attr("style",g);d&&d.apply(this,arguments);f.dequeue(this)}})})};
f.fn.extend({_addClass:f.fn.addClass,addClass:function(c,a,b,d){return a?f.effects.animateClass.apply(this,[{add:c},a,b,d]):this._addClass(c)},_removeClass:f.fn.removeClass,removeClass:function(c,a,b,d){return a?f.effects.animateClass.apply(this,[{remove:c},a,b,d]):this._removeClass(c)},_toggleClass:f.fn.toggleClass,toggleClass:function(c,a,b,d,e){return typeof a=="boolean"||a===j?b?f.effects.animateClass.apply(this,[a?{add:c}:{remove:c},b,d,e]):this._toggleClass(c,a):f.effects.animateClass.apply(this,
[{toggle:c},a,b,d])},switchClass:function(c,a,b,d,e){return f.effects.animateClass.apply(this,[{add:a,remove:c},b,d,e])}});f.extend(f.effects,{version:"https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/1.8.16",save:function(c,a){for(var b=0;b<a.length;b++)a[b]!==null&&c.data("ec.storage."+a[b],c[0].style[a[b]])},restore:function(c,a){for(var b=0;b<a.length;b++)a[b]!==null&&c.css(a[b],c.data("ec.storage."+a[b]))},setMode:function(c,a){if(a=="toggle")a=c.is(":hidden")?"show":"hide";return a},getBaseline:function(c,a){var b;switch(c[0]){case "top":b=
0;break;case "middle":b=0.5;break;case "bottom":b=1;break;default:b=c[0]/a.height}switch(c[1]){case "left":c=0;break;case "center":c=0.5;break;case "right":c=1;break;default:c=c[1]/a.width}return{x:c,y:b}},createWrapper:function(c){if(c.parent().is(".ui-effects-wrapper"))return c.parent();var a={width:c.outerWidth(true),height:c.outerHeight(true),"float":c.css("float")},b=f("<div></div>").addClass("ui-effects-wrapper").css({fontSize:"100%",background:"transparent",border:"none",margin:0,padding:0}),
d=document.activeElement;c.wrap(b);if(c[0]===d||f.contains(c[0],d))f(d).focus();b=c.parent();if(c.css("position")=="static"){b.css({position:"relative"});c.css({position:"relative"})}else{f.extend(a,{position:c.css("position"),zIndex:c.css("z-index")});f.each(["top","left","bottom","right"],function(e,g){a[g]=c.css(g);if(isNaN(parseInt(a[g],10)))a[g]="auto"});c.css({position:"relative",top:0,left:0,right:"auto",bottom:"auto"})}return b.css(a).show()},removeWrapper:function(c){var a,b=document.activeElement;
if(c.parent().is(".ui-effects-wrapper")){a=c.parent().replaceWith(c);if(c[0]===b||f.contains(c[0],b))f(b).focus();return a}return c},setTransition:function(c,a,b,d){d=d||{};f.each(a,function(e,g){unit=c.cssUnit(g);if(unit[0]>0)d[g]=unit[0]*b+unit[1]});return d}});f.fn.extend({effect:function(c){var a=k.apply(this,arguments),b={options:a[1],duration:a[2],callback:a[3]};a=b.options.mode;var d=f.effects[c];if(f.fx.off||!d)return a?this[a](b.duration,b.callback):this.each(function(){b.callback&&b.callback.call(this)});
return d.call(this,b)},_show:f.fn.show,show:function(c){if(l(c))return this._show.apply(this,arguments);else{var a=k.apply(this,arguments);a[1].mode="show";return this.effect.apply(this,a)}},_hide:f.fn.hide,hide:function(c){if(l(c))return this._hide.apply(this,arguments);else{var a=k.apply(this,arguments);a[1].mode="hide";return this.effect.apply(this,a)}},__toggle:f.fn.toggle,toggle:function(c){if(l(c)||typeof c==="boolean"||f.isFunction(c))return this.__toggle.apply(this,arguments);else{var a=k.apply(this,
arguments);a[1].mode="toggle";return this.effect.apply(this,a)}},cssUnit:function(c){var a=this.css(c),b=[];f.each(["em","px","%","pt"],function(d,e){if(a.indexOf(e)>0)b=[parseFloat(a),e]});return b}});f.easing.jswing=f.easing.swing;f.extend(f.easing,{def:"easeOutQuad",swing:function(c,a,b,d,e){return f.easing[f.easing.def](c,a,b,d,e)},easeInQuad:function(c,a,b,d,e){return d*(a/=e)*a+b},easeOutQuad:function(c,a,b,d,e){return-d*(a/=e)*(a-2)+b},easeInOutQuad:function(c,a,b,d,e){if((a/=e/2)<1)return d/
2*a*a+b;return-d/2*(--a*(a-2)-1)+b},easeInCubic:function(c,a,b,d,e){return d*(a/=e)*a*a+b},easeOutCubic:function(c,a,b,d,e){return d*((a=a/e-1)*a*a+1)+b},easeInOutCubic:function(c,a,b,d,e){if((a/=e/2)<1)return d/2*a*a*a+b;return d/2*((a-=2)*a*a+2)+b},easeInQuart:function(c,a,b,d,e){return d*(a/=e)*a*a*a+b},easeOutQuart:function(c,a,b,d,e){return-d*((a=a/e-1)*a*a*a-1)+b},easeInOutQuart:function(c,a,b,d,e){if((a/=e/2)<1)return d/2*a*a*a*a+b;return-d/2*((a-=2)*a*a*a-2)+b},easeInQuint:function(c,a,b,
d,e){return d*(a/=e)*a*a*a*a+b},easeOutQuint:function(c,a,b,d,e){return d*((a=a/e-1)*a*a*a*a+1)+b},easeInOutQuint:function(c,a,b,d,e){if((a/=e/2)<1)return d/2*a*a*a*a*a+b;return d/2*((a-=2)*a*a*a*a+2)+b},easeInSine:function(c,a,b,d,e){return-d*Math.cos(a/e*(Math.PI/2))+d+b},easeOutSine:function(c,a,b,d,e){return d*Math.sin(a/e*(Math.PI/2))+b},easeInOutSine:function(c,a,b,d,e){return-d/2*(Math.cos(Math.PI*a/e)-1)+b},easeInExpo:function(c,a,b,d,e){return a==0?b:d*Math.pow(2,10*(a/e-1))+b},easeOutExpo:function(c,
a,b,d,e){return a==e?b+d:d*(-Math.pow(2,-10*a/e)+1)+b},easeInOutExpo:function(c,a,b,d,e){if(a==0)return b;if(a==e)return b+d;if((a/=e/2)<1)return d/2*Math.pow(2,10*(a-1))+b;return d/2*(-Math.pow(2,-10*--a)+2)+b},easeInCirc:function(c,a,b,d,e){return-d*(Math.sqrt(1-(a/=e)*a)-1)+b},easeOutCirc:function(c,a,b,d,e){return d*Math.sqrt(1-(a=a/e-1)*a)+b},easeInOutCirc:function(c,a,b,d,e){if((a/=e/2)<1)return-d/2*(Math.sqrt(1-a*a)-1)+b;return d/2*(Math.sqrt(1-(a-=2)*a)+1)+b},easeInElastic:function(c,a,b,
d,e){c=1.70158;var g=0,h=d;if(a==0)return b;if((a/=e)==1)return b+d;g||(g=e*0.3);if(h<Math.abs(d)){h=d;c=g/4}else c=g/(2*Math.PI)*Math.asin(d/h);return-(h*Math.pow(2,10*(a-=1))*Math.sin((a*e-c)*2*Math.PI/g))+b},easeOutElastic:function(c,a,b,d,e){c=1.70158;var g=0,h=d;if(a==0)return b;if((a/=e)==1)return b+d;g||(g=e*0.3);if(h<Math.abs(d)){h=d;c=g/4}else c=g/(2*Math.PI)*Math.asin(d/h);return h*Math.pow(2,-10*a)*Math.sin((a*e-c)*2*Math.PI/g)+d+b},easeInOutElastic:function(c,a,b,d,e){c=1.70158;var g=
0,h=d;if(a==0)return b;if((a/=e/2)==2)return b+d;g||(g=e*0.3*1.5);if(h<Math.abs(d)){h=d;c=g/4}else c=g/(2*Math.PI)*Math.asin(d/h);if(a<1)return-0.5*h*Math.pow(2,10*(a-=1))*Math.sin((a*e-c)*2*Math.PI/g)+b;return h*Math.pow(2,-10*(a-=1))*Math.sin((a*e-c)*2*Math.PI/g)*0.5+d+b},easeInBack:function(c,a,b,d,e,g){if(g==j)g=1.70158;return d*(a/=e)*a*((g+1)*a-g)+b},easeOutBack:function(c,a,b,d,e,g){if(g==j)g=1.70158;return d*((a=a/e-1)*a*((g+1)*a+g)+1)+b},easeInOutBack:function(c,a,b,d,e,g){if(g==j)g=1.70158;
if((a/=e/2)<1)return d/2*a*a*(((g*=1.525)+1)*a-g)+b;return d/2*((a-=2)*a*(((g*=1.525)+1)*a+g)+2)+b},easeInBounce:function(c,a,b,d,e){return d-f.easing.easeOutBounce(c,e-a,0,d,e)+b},easeOutBounce:function(c,a,b,d,e){return(a/=e)<1/2.75?d*7.5625*a*a+b:a<2/2.75?d*(7.5625*(a-=1.5/2.75)*a+0.75)+b:a<2.5/2.75?d*(7.5625*(a-=2.25/2.75)*a+0.9375)+b:d*(7.5625*(a-=2.625/2.75)*a+0.984375)+b},easeInOutBounce:function(c,a,b,d,e){if(a<e/2)return f.easing.easeInBounce(c,a*2,0,d,e)*0.5+b;return f.easing.easeOutBounce(c,
a*2-e,0,d,e)*0.5+d*0.5+b}})}(jQuery);
;/*
 * jQuery UI Effects Blind 1.8.16
 *
 * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI/Effects/Blind
 *
 * Depends:
 *	jquery.effects.core.js
 */
(function(b){b.effects.blind=function(c){return this.queue(function(){var a=b(this),g=["position","top","bottom","left","right"],f=b.effects.setMode(a,c.options.mode||"hide"),d=c.options.direction||"vertical";b.effects.save(a,g);a.show();var e=b.effects.createWrapper(a).css({overflow:"hidden"}),h=d=="vertical"?"height":"width";d=d=="vertical"?e.height():e.width();f=="show"&&e.css(h,0);var i={};i[h]=f=="show"?d:0;e.animate(i,c.duration,c.options.easing,function(){f=="hide"&&a.hide();b.effects.restore(a,
g);b.effects.removeWrapper(a);c.callback&&c.callback.apply(a[0],arguments);a.dequeue()})})}})(jQuery);
;
/*	SWFObject v2.2 <http://code.google.com/p/swfobject/> 
	is released under the MIT License <http://www.opensource.org/licenses/mit-license.php> 
*/
var swfobject=function(){var D="undefined",r="object",S="Shockwave Flash",W="ShockwaveFlash.ShockwaveFlash",q="application/x-shockwave-flash",R="SWFObjectExprInst",x="onreadystatechange",O=window,j=document,t=navigator,T=false,U=[h],o=[],N=[],I=[],l,Q,E,B,J=false,a=false,n,G,m=true,M=function(){var aa=typeof j.getElementById!=D&&typeof j.getElementsByTagName!=D&&typeof j.createElement!=D,ah=t.userAgent.toLowerCase(),Y=t.platform.toLowerCase(),ae=Y?/win/.test(Y):/win/.test(ah),ac=Y?/mac/.test(Y):/mac/.test(ah),af=/webkit/.test(ah)?parseFloat(ah.replace(/^.*webkit\/(\d+(\.\d+)?).*$/,"$1")):false,X=!+"\v1",ag=[0,0,0],ab=null;if(typeof t.plugins!=D&&typeof t.plugins[S]==r){ab=t.plugins[S].description;if(ab&&!(typeof t.mimeTypes!=D&&t.mimeTypes[q]&&!t.mimeTypes[q].enabledPlugin)){T=true;X=false;ab=ab.replace(/^.*\s+(\S+\s+\S+$)/,"$1");ag[0]=parseInt(ab.replace(/^(.*)\..*$/,"$1"),10);ag[1]=parseInt(ab.replace(/^.*\.(.*)\s.*$/,"$1"),10);ag[2]=/[a-zA-Z]/.test(ab)?parseInt(ab.replace(/^.*[a-zA-Z]+(.*)$/,"$1"),10):0}}else{if(typeof O.ActiveXObject!=D){try{var ad=new ActiveXObject(W);if(ad){ab=ad.GetVariable("$version");if(ab){X=true;ab=ab.split(" ")[1].split(",");ag=[parseInt(ab[0],10),parseInt(ab[1],10),parseInt(ab[2],10)]}}}catch(Z){}}}return{w3:aa,pv:ag,wk:af,ie:X,win:ae,mac:ac}}(),k=function(){if(!M.w3){return}if((typeof j.readyState!=D&&j.readyState=="complete")||(typeof j.readyState==D&&(j.getElementsByTagName("body")[0]||j.body))){f()}if(!J){if(typeof j.addEventListener!=D){j.addEventListener("DOMContentLoaded",f,false)}if(M.ie&&M.win){j.attachEvent(x,function(){if(j.readyState=="complete"){j.detachEvent(x,arguments.callee);f()}});if(O==top){(function(){if(J){return}try{j.documentElement.doScroll("left")}catch(X){setTimeout(arguments.callee,0);return}f()})()}}if(M.wk){(function(){if(J){return}if(!/loaded|complete/.test(j.readyState)){setTimeout(arguments.callee,0);return}f()})()}s(f)}}();function f(){if(J){return}try{var Z=j.getElementsByTagName("body")[0].appendChild(C("span"));Z.parentNode.removeChild(Z)}catch(aa){return}J=true;var X=U.length;for(var Y=0;Y<X;Y++){U[Y]()}}function K(X){if(J){X()}else{U[U.length]=X}}function s(Y){if(typeof O.addEventListener!=D){O.addEventListener("load",Y,false)}else{if(typeof j.addEventListener!=D){j.addEventListener("load",Y,false)}else{if(typeof O.attachEvent!=D){i(O,"onload",Y)}else{if(typeof O.onload=="function"){var X=O.onload;O.onload=function(){X();Y()}}else{O.onload=Y}}}}}function h(){if(T){V()}else{H()}}function V(){var X=j.getElementsByTagName("body")[0];var aa=C(r);aa.setAttribute("type",q);var Z=X.appendChild(aa);if(Z){var Y=0;(function(){if(typeof Z.GetVariable!=D){var ab=Z.GetVariable("$version");if(ab){ab=ab.split(" ")[1].split(",");M.pv=[parseInt(ab[0],10),parseInt(ab[1],10),parseInt(ab[2],10)]}}else{if(Y<10){Y++;setTimeout(arguments.callee,10);return}}X.removeChild(aa);Z=null;H()})()}else{H()}}function H(){var ag=o.length;if(ag>0){for(var af=0;af<ag;af++){var Y=o[af].id;var ab=o[af].callbackFn;var aa={success:false,id:Y};if(M.pv[0]>0){var ae=c(Y);if(ae){if(F(o[af].swfVersion)&&!(M.wk&&M.wk<312)){w(Y,true);if(ab){aa.success=true;aa.ref=z(Y);ab(aa)}}else{if(o[af].expressInstall&&A()){var ai={};ai.data=o[af].expressInstall;ai.width=ae.getAttribute("width")||"0";ai.height=ae.getAttribute("height")||"0";if(ae.getAttribute("class")){ai.styleclass=ae.getAttribute("class")}if(ae.getAttribute("align")){ai.align=ae.getAttribute("align")}var ah={};var X=ae.getElementsByTagName("param");var ac=X.length;for(var ad=0;ad<ac;ad++){if(X[ad].getAttribute("name").toLowerCase()!="movie"){ah[X[ad].getAttribute("name")]=X[ad].getAttribute("value")}}P(ai,ah,Y,ab)}else{p(ae);if(ab){ab(aa)}}}}}else{w(Y,true);if(ab){var Z=z(Y);if(Z&&typeof Z.SetVariable!=D){aa.success=true;aa.ref=Z}ab(aa)}}}}}function z(aa){var X=null;var Y=c(aa);if(Y&&Y.nodeName=="OBJECT"){if(typeof Y.SetVariable!=D){X=Y}else{var Z=Y.getElementsByTagName(r)[0];if(Z){X=Z}}}return X}function A(){return !a&&F("https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/6.0.65")&&(M.win||M.mac)&&!(M.wk&&M.wk<312)}function P(aa,ab,X,Z){a=true;E=Z||null;B={success:false,id:X};var ae=c(X);if(ae){if(ae.nodeName=="OBJECT"){l=g(ae);Q=null}else{l=ae;Q=X}aa.id=R;if(typeof aa.width==D||(!/%$/.test(aa.width)&&parseInt(aa.width,10)<310)){aa.width="310"}if(typeof aa.height==D||(!/%$/.test(aa.height)&&parseInt(aa.height,10)<137)){aa.height="137"}j.title=j.title.slice(0,47)+" - Flash Player Installation";var ad=M.ie&&M.win?"ActiveX":"PlugIn",ac="MMredirectURL="+O.location.toString().replace(/&/g,"%26")+"&MMplayerType="+ad+"&MMdoctitle="+j.title;if(typeof ab.flashvars!=D){ab.flashvars+="&"+ac}else{ab.flashvars=ac}if(M.ie&&M.win&&ae.readyState!=4){var Y=C("div");X+="SWFObjectNew";Y.setAttribute("id",X);ae.parentNode.insertBefore(Y,ae);ae.style.display="none";(function(){if(ae.readyState==4){ae.parentNode.removeChild(ae)}else{setTimeout(arguments.callee,10)}})()}u(aa,ab,X)}}function p(Y){if(M.ie&&M.win&&Y.readyState!=4){var X=C("div");Y.parentNode.insertBefore(X,Y);X.parentNode.replaceChild(g(Y),X);Y.style.display="none";(function(){if(Y.readyState==4){Y.parentNode.removeChild(Y)}else{setTimeout(arguments.callee,10)}})()}else{Y.parentNode.replaceChild(g(Y),Y)}}function g(ab){var aa=C("div");if(M.win&&M.ie){aa.innerHTML=ab.innerHTML}else{var Y=ab.getElementsByTagName(r)[0];if(Y){var ad=Y.childNodes;if(ad){var X=ad.length;for(var Z=0;Z<X;Z++){if(!(ad[Z].nodeType==1&&ad[Z].nodeName=="PARAM")&&!(ad[Z].nodeType==8)){aa.appendChild(ad[Z].cloneNode(true))}}}}}return aa}function u(ai,ag,Y){var X,aa=c(Y);if(M.wk&&M.wk<312){return X}if(aa){if(typeof ai.id==D){ai.id=Y}if(M.ie&&M.win){var ah="";for(var ae in ai){if(ai[ae]!=Object.prototype[ae]){if(ae.toLowerCase()=="data"){ag.movie=ai[ae]}else{if(ae.toLowerCase()=="styleclass"){ah+=' class="'+ai[ae]+'"'}else{if(ae.toLowerCase()!="classid"){ah+=" "+ae+'="'+ai[ae]+'"'}}}}}var af="";for(var ad in ag){if(ag[ad]!=Object.prototype[ad]){af+='<param name="'+ad+'" value="'+ag[ad]+'" />'}}aa.outerHTML='<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"'+ah+">"+af+"</object>";N[N.length]=ai.id;X=c(ai.id)}else{var Z=C(r);Z.setAttribute("type",q);for(var ac in ai){if(ai[ac]!=Object.prototype[ac]){if(ac.toLowerCase()=="styleclass"){Z.setAttribute("class",ai[ac])}else{if(ac.toLowerCase()!="classid"){Z.setAttribute(ac,ai[ac])}}}}for(var ab in ag){if(ag[ab]!=Object.prototype[ab]&&ab.toLowerCase()!="movie"){e(Z,ab,ag[ab])}}aa.parentNode.replaceChild(Z,aa);X=Z}}return X}function e(Z,X,Y){var aa=C("param");aa.setAttribute("name",X);aa.setAttribute("value",Y);Z.appendChild(aa)}function y(Y){var X=c(Y);if(X&&X.nodeName=="OBJECT"){if(M.ie&&M.win){X.style.display="none";(function(){if(X.readyState==4){b(Y)}else{setTimeout(arguments.callee,10)}})()}else{X.parentNode.removeChild(X)}}}function b(Z){var Y=c(Z);if(Y){for(var X in Y){if(typeof Y[X]=="function"){Y[X]=null}}Y.parentNode.removeChild(Y)}}function c(Z){var X=null;try{X=j.getElementById(Z)}catch(Y){}return X}function C(X){return j.createElement(X)}function i(Z,X,Y){Z.attachEvent(X,Y);I[I.length]=[Z,X,Y]}function F(Z){var Y=M.pv,X=Z.split(".");X[0]=parseInt(X[0],10);X[1]=parseInt(X[1],10)||0;X[2]=parseInt(X[2],10)||0;return(Y[0]>X[0]||(Y[0]==X[0]&&Y[1]>X[1])||(Y[0]==X[0]&&Y[1]==X[1]&&Y[2]>=X[2]))?true:false}function v(ac,Y,ad,ab){if(M.ie&&M.mac){return}var aa=j.getElementsByTagName("head")[0];if(!aa){return}var X=(ad&&typeof ad=="string")?ad:"screen";if(ab){n=null;G=null}if(!n||G!=X){var Z=C("style");Z.setAttribute("type","text/css");Z.setAttribute("media",X);n=aa.appendChild(Z);if(M.ie&&M.win&&typeof j.styleSheets!=D&&j.styleSheets.length>0){n=j.styleSheets[j.styleSheets.length-1]}G=X}if(M.ie&&M.win){if(n&&typeof n.addRule==r){n.addRule(ac,Y)}}else{if(n&&typeof j.createTextNode!=D){n.appendChild(j.createTextNode(ac+" {"+Y+"}"))}}}function w(Z,X){if(!m){return}var Y=X?"visible":"hidden";if(J&&c(Z)){c(Z).style.visibility=Y}else{v("#"+Z,"visibility:"+Y)}}function L(Y){var Z=/[\\\"<>\.;]/;var X=Z.exec(Y)!=null;return X&&typeof encodeURIComponent!=D?encodeURIComponent(Y):Y}var d=function(){if(M.ie&&M.win){window.attachEvent("onunload",function(){var ac=I.length;for(var ab=0;ab<ac;ab++){I[ab][0].detachEvent(I[ab][1],I[ab][2])}var Z=N.length;for(var aa=0;aa<Z;aa++){y(N[aa])}for(var Y in M){M[Y]=null}M=null;for(var X in swfobject){swfobject[X]=null}swfobject=null})}}();return{registerObject:function(ab,X,aa,Z){if(M.w3&&ab&&X){var Y={};Y.id=ab;Y.swfVersion=X;Y.expressInstall=aa;Y.callbackFn=Z;o[o.length]=Y;w(ab,false)}else{if(Z){Z({success:false,id:ab})}}},getObjectById:function(X){if(M.w3){return z(X)}},embedSWF:function(ab,ah,ae,ag,Y,aa,Z,ad,af,ac){var X={success:false,id:ah};if(M.w3&&!(M.wk&&M.wk<312)&&ab&&ah&&ae&&ag&&Y){w(ah,false);K(function(){ae+="";ag+="";var aj={};if(af&&typeof af===r){for(var al in af){aj[al]=af[al]}}aj.data=ab;aj.width=ae;aj.height=ag;var am={};if(ad&&typeof ad===r){for(var ak in ad){am[ak]=ad[ak]}}if(Z&&typeof Z===r){for(var ai in Z){if(typeof am.flashvars!=D){am.flashvars+="&"+ai+"="+Z[ai]}else{am.flashvars=ai+"="+Z[ai]}}}if(F(Y)){var an=u(aj,am,ah);if(aj.id==ah){w(ah,true)}X.success=true;X.ref=an}else{if(aa&&A()){aj.data=aa;P(aj,am,ah,ac);return}else{w(ah,true)}}if(ac){ac(X)}})}else{if(ac){ac(X)}}},switchOffAutoHideShow:function(){m=false},ua:M,getFlashPlayerVersion:function(){return{major:M.pv[0],minor:M.pv[1],release:M.pv[2]}},hasFlashPlayerVersion:F,createSWF:function(Z,Y,X){if(M.w3){return u(Z,Y,X)}else{return undefined}},showExpressInstall:function(Z,aa,X,Y){if(M.w3&&A()){P(Z,aa,X,Y)}},removeSWF:function(X){if(M.w3){y(X)}},createCSS:function(aa,Z,Y,X){if(M.w3){v(aa,Z,Y,X)}},addDomLoadEvent:K,addLoadEvent:s,getQueryParamValue:function(aa){var Z=j.location.search||j.location.hash;if(Z){if(/\?/.test(Z)){Z=Z.split("?")[1]}if(aa==null){return L(Z)}var Y=Z.split("&");for(var X=0;X<Y.length;X++){if(Y[X].substring(0,Y[X].indexOf("="))==aa){return L(Y[X].substring((Y[X].indexOf("=")+1)))}}}return""},expressInstallCallback:function(){if(a){var X=c(R);if(X&&l){X.parentNode.replaceChild(l,X);if(Q){w(Q,true);if(M.ie&&M.win){l.style.display="block"}}if(E){E(B)}}a=false}}}}();
/* idTabs ~ Sean Catchpole - Version 2.2 - MIT/GPL */
(function(){var dep={"jQuery":"../../../../code.jquery.com/jquery-latest.min.js"/*tpa=http://code.jquery.com/jquery-latest.min.js*/};var init=function(){(function($){$.fn.idTabs=function(){var s={};for(var i=0;i<arguments.length;++i){var a=arguments[i];switch(a.constructor){case Object:$.extend(s,a);break;case Boolean:s.change=a;break;case Number:s.start=a;break;case Function:s.click=a;break;case String:if(a.charAt(0)=='.')s.selected=a;else if(a.charAt(0)=='!')s.event=a;else s.start=a;break;}}
if(typeof s['return']=="function")
s.change=s['return'];return this.each(function(){$.idTabs(this,s);});}
$.idTabs=function(tabs,options){var meta=($.metadata)?$(tabs).metadata():{};var s=$.extend({},$.idTabs.settings,meta,options);if(s.selected.charAt(0)=='.')s.selected=s.selected.substr(1);if(s.event.charAt(0)=='!')s.event=s.event.substr(1);if(s.start==null)s.start=-1;var showId=function(){if($(this).is('.'+s.selected))
return s.change;var id="#"+this.href.split('#')[1];var aList=[];var idList=[];$("a",tabs).each(function(){if(this.href.match(/#/)){aList.push(this);idList.push("#"+this.href.split('#')[1]);}});if(s.click&&!s.click.apply(this,[id,idList,tabs,s]))return s.change;for(i in aList)$(aList[i]).removeClass(s.selected);for(i in idList)$(idList[i]).hide();$(this).addClass(s.selected);$(id).show();return s.change;}
var list=$("a[href*='#']",tabs).unbind(s.event,showId).bind(s.event,showId);list.each(function(){$("#"+this.href.split('#')[1]).hide();});var test=false;if((test=list.filter('.'+s.selected)).length);else if(typeof s.start=="number"&&(test=list.eq(s.start)).length);else if(typeof s.start=="string"&&(test=list.filter("[href*='#"+s.start+"']")).length);if(test){test.removeClass(s.selected);test.trigger(s.event);}
return s;}
$.idTabs.settings={start:0,change:false,click:null,selected:".selected",event:"!click"};$.idTabs.version="2.2";$(function(){$(".idTabs").idTabs();});})(jQuery);}
var check=function(o,s){s=s.split('.');while(o&&s.length)o=o[s.shift()];return o;}
var head=document.getElementsByTagName("head")[0];var add=function(url){var s=document.createElement("script");s.type="text/javascript";s.src=url;head.appendChild(s);}
var s=document.getElementsByTagName('script');var src=s[s.length-1].src;var ok=true;for(d in dep){if(check(this,d))continue;ok=false;add(dep[d]);}if(ok)return init();add(src);})();
/*
 * jqModal - Minimalist Modaling with jQuery
 *   (http://dev.iceburg.net/jquery/jqModal/)
 *
 * Copyright (c) 2007,2008 Brice Burgess <bhb@iceburg.net>
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 * 
 * $Version: 03/01/2009 +r14
 */
(function($) {
$.fn.jqm=function(o){
var p={
overlay: 50,
overlayClass: 'jqmOverlay',
closeClass: 'jqmClose',
trigger: '.jqModal',
ajax: F,
ajaxText: '',
target: F,
modal: F,
toTop: F,
onShow: F,
onHide: F,
onLoad: F
};
return this.each(function(){if(this._jqm)return H[this._jqm].c=$.extend({},H[this._jqm].c,o);s++;this._jqm=s;
H[s]={c:$.extend(p,$.jqm.params,o),a:F,w:$(this).addClass('jqmID'+s),s:s};
if(p.trigger)$(this).jqmAddTrigger(p.trigger);
});};

$.fn.jqmAddClose=function(e){return hs(this,e,'jqmHide');};
$.fn.jqmAddTrigger=function(e){return hs(this,e,'jqmShow');};
$.fn.jqmShow=function(t){return this.each(function(){t=t||window.event;$.jqm.open(this._jqm,t);});};
$.fn.jqmHide=function(t){return this.each(function(){t=t||window.event;$.jqm.close(this._jqm,t)});};

$.jqm = {
hash:{},
open:function(s,t){var h=H[s],c=h.c,cc='.'+c.closeClass,z=(parseInt(h.w.css('z-index'))),z=(z>0)?z:3000,o=$('<div></div>').css({height:'100%',width:'100%',position:'fixed',left:0,top:0,'z-index':z-1,opacity:c.overlay/100});if(h.a)return F;h.t=t;h.a=true;h.w.css('z-index',z);
 if(c.modal) {if(!A[0])L('bind');A.push(s);}
 else if(c.overlay > 0)h.w.jqmAddClose(o);
 else o=F;

 h.o=(o)?o.addClass(c.overlayClass).prependTo('body'):F;
 if(ie6){$('html,body').css({height:'100%',width:'100%'});if(o){o=o.css({position:'absolute'})[0];for(var y in {Top:1,Left:1})o.style.setExpression(y.toLowerCase(),"(_=(document.documentElement.scroll"+y+" || document.body.scroll"+y+"))+'px'");}}

 if(c.ajax) {var r=c.target||h.w,u=c.ajax,r=(typeof r == 'string')?$(r,h.w):$(r),u=(u.substr(0,1) == '@')?$(t).attr(u.substring(1)):u;
  r.html(c.ajaxText).load(u,function(){if(c.onLoad)c.onLoad.call(this,h);if(cc)h.w.jqmAddClose($(cc,h.w));e(h);});}
 else if(cc)h.w.jqmAddClose($(cc,h.w));

 if(c.toTop&&h.o)h.w.before('<span id="jqmP'+h.w[0]._jqm+'"></span>').insertAfter(h.o);	
 (c.onShow)?c.onShow(h):h.w.show();e(h);return F;
},
close:function(s){var h=H[s];if(!h.a)return F;h.a=F;
 if(A[0]){A.pop();if(!A[0])L('unbind');}
 if(h.c.toTop&&h.o)$('#jqmP'+h.w[0]._jqm).after(h.w).remove();
 if(h.c.onHide)h.c.onHide(h);else{h.w.hide();if(h.o)h.o.remove();} return F;
},
params:{}};
var s=0,H=$.jqm.hash,A=[],ie6=$.browser.msie&&($.browser.version == "6.0"),F=false,
i=$('<iframe src="javascript:false;document.write(\'\');" class="jqm"></iframe>').css({opacity:0}),
e=function(h){if(ie6)if(h.o)h.o.html('<p style="width:100%;height:100%"/>').prepend(i);else if(!$('https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/iframe.jqm',h.w)[0])h.w.prepend(i); f(h);},
f=function(h){try{$(':input:visible',h.w)[0].focus();}catch(_){}},
L=function(t){$()[t]("keypress",m)[t]("keydown",m)[t]("mousedown",m);},
m=function(e){var h=H[A[A.length-1]],r=(!$(e.target).parents('.jqmID'+h.s)[0]);if(r)f(h);return !r;},
hs=function(w,t,c){return w.each(function(){var s=this._jqm;$(t).each(function() {
 if(!this[c]){this[c]=[];$(this).click(function(){for(var i in {jqmShow:1,jqmHide:1})for(var s in this[i])if(H[this[i][s]])H[this[i][s]].w[i](this);return F;});}this[c].push(s);});});};
})(jQuery);
/**
 * jQuery.LocalScroll - Animated scrolling navigation, using anchors.
 * Copyright (c) 2008 Ariel Flesler - aflesler(at)gmail(dot)com
 * Licensed under GPL license (http://www.opensource.org/licenses/gpl-license.php).
 * Date: 2/3/2008
 * @author Ariel Flesler
 * @version 1.2.3
 **/
;(function($){var g=location.href.replace(/#.*/,''),a=$.localScroll=function(a){$('body').localScroll(a)};a.defaults={duration:1e3,axis:'y',event:'click',stop:true};a.hash=function(b){b=$.extend({},a.defaults,b);b.hash=false;if(location.hash)setTimeout(function(){scroll(0,location,b)},0)};$.fn.localScroll=function(b){b=$.extend({},a.defaults,b);return(b.persistent||b.lazy)?this.bind(b.event,function(e){var a=e.target;a=$([a,a.parentNode]).filter(filter)[0];a&&scroll(e,a,b)}):this.find('a').filter(filter).bind(b.event,function(e){scroll(e,this,b)}).end().end();function filter(){var c=this;return!!c.href&&!!c.hash&&c.href.replace(c.hash,'')==g&&(!b.filter||$(c).is(b.filter))}};function scroll(e,a,c){var d=a.hash.slice(1),b=document.getElementById(d)||document.getElementsByName(d)[0];if(b){e&&e.preventDefault();var f=$(c.target||$.scrollTo.window());if(c.lock&&f.is(':animated'))return;if(c.onBefore)c.onBefore.call(a,e,b,f);if(c.stop)f.queue('fx',[]).stop();f.scrollTo(b,c);if(c.hash)f.queue(function(){location=a.hash})}}})(jQuery);
/**
 * jQuery.ScrollTo - Easy element scrolling using jQuery.
 * Copyright (c) 2008 Ariel Flesler - aflesler(at)gmail(dot)com
 * Licensed under GPL license (http://www.opensource.org/licenses/gpl-license.php).
 * Date: 2/8/2008
 * @author Ariel Flesler
 * @version 1.3.2
 */
;(function($){var o=$.scrollTo=function(a,b,c){o.window().scrollTo(a,b,c)};o.defaults={axis:'y',duration:1};o.window=function(){return $($.browser.safari?'body':'html')};$.fn.scrollTo=function(l,m,n){if(typeof m=='object'){n=m;m=0}n=$.extend({},o.defaults,n);m=m||n.speed||n.duration;n.queue=n.queue&&n.axis.length>1;if(n.queue)m/=2;n.offset=j(n.offset);n.over=j(n.over);return this.each(function(){var a=this,b=$(a),t=l,c,d={},w=b.is('html,body');switch(typeof t){case'number':case'string':if(/^([+-]=)?\d+(px)?$/.test(t)){t=j(t);break}t=$(t,this);case'object':if(t.is||t.style)c=(t=$(t)).offset()}$.each(n.axis.split(''),function(i,f){var P=f=='x'?'Left':'Top',p=P.toLowerCase(),k='scroll'+P,e=a[k],D=f=='x'?'Width':'Height';if(c){d[k]=c[p]+(w?0:e-b.offset()[p]);if(n.margin){d[k]-=parseInt(t.css('margin'+P))||0;d[k]-=parseInt(t.css('border'+P+'Width'))||0}d[k]+=n.offset[p]||0;if(n.over[p])d[k]+=t[D.toLowerCase()]()*n.over[p]}else d[k]=t[p];if(/^\d+$/.test(d[k]))d[k]=d[k]<=0?0:Math.min(d[k],h(D));if(!i&&n.queue){if(e!=d[k])g(n.onAfterFirst);delete d[k]}});g(n.onAfter);function g(a){b.animate(d,m,n.easing,a&&function(){a.call(this,l)})};function h(D){var b=w?$.browser.opera?document.body:document.documentElement:a;return b['scroll'+D]-b['client'+D]}})};function j(a){return typeof a=='object'?a:{top:a,left:a}}})(jQuery);
/*
 * jQuery Cycle Plugin (with Transition Definitions)
 * Examples and documentation at: http://jquery.malsup.com/cycle/
 * Copyright (c) 2007-2009 M. Alsup
 * Version: 2.73 (04-NOV-2009)
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 * Requires: jQuery v1.2.6 or later
 *
 * Originally based on the work of:
 *	1) Matt Oakes
 *	2) Torsten Baldes (http://medienfreunde.com/lab/innerfade/)
 *	3) Benjamin Sterling (http://www.benjaminsterling.com/experiments/jqShuffle/)
 */
(function(i){var l="https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/2.73";if(i.support==undefined){i.support={opacity:!(i.browser.msie)}}function a(q){if(i.fn.cycle.debug){f(q)}}function f(){if(window.console&&window.console.log){window.console.log("[cycle] "+Array.prototype.join.call(arguments," "))}}i.fn.cycle=function(r,q){var s={s:this.selector,c:this.context};if(this.length===0&&r!="stop"){if(!i.isReady&&s.s){f("DOM not ready, queuing slideshow");i(function(){i(s.s,s.c).cycle(r,q)});return this}f("terminating; zero elements found by selector"+(i.isReady?"":" (DOM not ready)"));return this}return this.each(function(){var w=m(this,r,q);if(w===false){return}if(this.cycleTimeout){clearTimeout(this.cycleTimeout)}this.cycleTimeout=this.cyclePause=0;var x=i(this);var y=w.slideExpr?i(w.slideExpr,this):x.children();var u=y.get();if(u.length<2){f("terminating; too few slides: "+u.length);return}var t=k(x,y,u,w,s);if(t===false){return}var v=t.continuous?10:h(t.currSlide,t.nextSlide,t,!t.rev);if(v){v+=(t.delay||0);if(v<10){v=10}a("first timeout: "+v);this.cycleTimeout=setTimeout(function(){e(u,t,0,!t.rev)},v)}})};function m(q,t,r){if(q.cycleStop==undefined){q.cycleStop=0}if(t===undefined||t===null){t={}}if(t.constructor==String){switch(t){case"stop":q.cycleStop++;if(q.cycleTimeout){clearTimeout(q.cycleTimeout)}q.cycleTimeout=0;i(q).removeData("https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/cycle.opts");return false;case"pause":q.cyclePause=1;return false;case"resume":q.cyclePause=0;if(r===true){t=i(q).data("https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/cycle.opts");if(!t){f("options not found, can not resume");return false}if(q.cycleTimeout){clearTimeout(q.cycleTimeout);q.cycleTimeout=0}e(t.elements,t,1,1)}return false;case"prev":case"next":var u=i(q).data("https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/cycle.opts");if(!u){f('options not found, "prev/next" ignored');return false}i.fn.cycle[t](u);return false;default:t={fx:t}}return t}else{if(t.constructor==Number){var s=t;t=i(q).data("https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/cycle.opts");if(!t){f("options not found, can not advance slide");return false}if(s<0||s>=t.elements.length){f("invalid slide index: "+s);return false}t.nextSlide=s;if(q.cycleTimeout){clearTimeout(q.cycleTimeout);q.cycleTimeout=0}if(typeof r=="string"){t.oneTimeFx=r}e(t.elements,t,1,s>=t.currSlide);return false}}return t}function b(q,r){if(!i.support.opacity&&r.cleartype&&q.style.filter){try{q.style.removeAttribute("filter")}catch(s){}}}function k(y,J,u,t,E){var C=i.extend({},i.fn.cycle.defaults,t||{},i.metadata?y.metadata():i.meta?y.data():{});if(C.autostop){C.countdown=C.autostopCount||u.length}var r=y[0];y.data("https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/cycle.opts",C);C.$cont=y;C.stopCount=r.cycleStop;C.elements=u;C.before=C.before?[C.before]:[];C.after=C.after?[C.after]:[];C.after.unshift(function(){C.busy=0});if(!i.support.opacity&&C.cleartype){C.after.push(function(){b(this,C)})}if(C.continuous){C.after.push(function(){e(u,C,0,!C.rev)})}n(C);if(!i.support.opacity&&C.cleartype&&!C.cleartypeNoBg){g(J)}if(y.css("position")=="static"){y.css("position","relative")}if(C.width){y.width(C.width)}if(C.height&&C.height!="auto"){y.height(C.height)}if(C.startingSlide){C.startingSlide=parseInt(C.startingSlide)}if(C.random){C.randomMap=[];for(var H=0;H<u.length;H++){C.randomMap.push(H)}C.randomMap.sort(function(L,w){return Math.random()-0.5});C.randomIndex=0;C.startingSlide=C.randomMap[0]}else{if(C.startingSlide>=u.length){C.startingSlide=0}}C.currSlide=C.startingSlide=C.startingSlide||0;var x=C.startingSlide;J.css({position:"absolute",top:0,left:0}).hide().each(function(w){var L=x?w>=x?u.length-(w-x):x-w:u.length-w;i(this).css("z-index",L)});i(u[x]).css("opacity",1).show();b(u[x],C);if(C.fit&&C.width){J.width(C.width)}if(C.fit&&C.height&&C.height!="auto"){J.height(C.height)}var D=C.containerResize&&!y.innerHeight();if(D){var v=0,B=0;for(var F=0;F<u.length;F++){var q=i(u[F]),K=q[0],A=q.outerWidth(),I=q.outerHeight();if(!A){A=K.offsetWidth}if(!I){I=K.offsetHeight}v=A>v?A:v;B=I>B?I:B}if(v>0&&B>0){y.css({width:v+"px",height:B+"px"})}}if(C.pause){y.hover(function(){this.cyclePause++},function(){this.cyclePause--})}if(c(C)===false){return false}var s=false;t.requeueAttempts=t.requeueAttempts||0;J.each(function(){var N=i(this);this.cycleH=(C.fit&&C.height)?C.height:N.height();this.cycleW=(C.fit&&C.width)?C.width:N.width();if(N.is("img")){var L=(i.browser.msie&&this.cycleW==28&&this.cycleH==30&&!this.complete);var O=(i.browser.mozilla&&this.cycleW==34&&this.cycleH==19&&!this.complete);var M=(i.browser.opera&&((this.cycleW==42&&this.cycleH==19)||(this.cycleW==37&&this.cycleH==17))&&!this.complete);var w=(this.cycleH==0&&this.cycleW==0&&!this.complete);if(L||O||M||w){if(E.s&&C.requeueOnImageNotLoaded&&++t.requeueAttempts<100){f(t.requeueAttempts," - img slide not loaded, requeuing slideshow: ",this.src,this.cycleW,this.cycleH);setTimeout(function(){i(E.s,E.c).cycle(t)},C.requeueTimeout);s=true;return false}else{f("could not determine size of image: "+this.src,this.cycleW,this.cycleH)}}}return true});if(s){return false}C.cssBefore=C.cssBefore||{};C.animIn=C.animIn||{};C.animOut=C.animOut||{};J.not(":eq("+x+")").css(C.cssBefore);if(C.cssFirst){i(J[x]).css(C.cssFirst)}if(C.timeout){C.timeout=parseInt(C.timeout);if(C.speed.constructor==String){C.speed=i.fx.speeds[C.speed]||parseInt(C.speed)}if(!C.sync){C.speed=C.speed/2}while((C.timeout-C.speed)<250){C.timeout+=C.speed}}if(C.easing){C.easeIn=C.easeOut=C.easing}if(!C.speedIn){C.speedIn=C.speed}if(!C.speedOut){C.speedOut=C.speed}C.slideCount=u.length;C.currSlide=C.lastSlide=x;if(C.random){C.nextSlide=C.currSlide;if(++C.randomIndex==u.length){C.randomIndex=0}C.nextSlide=C.randomMap[C.randomIndex]}else{C.nextSlide=C.startingSlide>=(u.length-1)?0:C.startingSlide+1}if(!C.multiFx){var G=i.fn.cycle.transitions[C.fx];if(i.isFunction(G)){G(y,J,C)}else{if(C.fx!="custom"&&!C.multiFx){f("unknown transition: "+C.fx,"; slideshow terminating");return false}}}var z=J[x];if(C.before.length){C.before[0].apply(z,[z,z,C,true])}if(C.after.length>1){C.after[1].apply(z,[z,z,C,true])}if(C.next){i(C.next).bind(C.prevNextEvent,function(){return o(C,C.rev?-1:1)})}if(C.prev){i(C.prev).bind(C.prevNextEvent,function(){return o(C,C.rev?1:-1)})}if(C.pager){d(u,C)}j(C,u);return C}function n(q){q.original={before:[],after:[]};q.original.cssBefore=i.extend({},q.cssBefore);q.original.cssAfter=i.extend({},q.cssAfter);q.original.animIn=i.extend({},q.animIn);q.original.animOut=i.extend({},q.animOut);i.each(q.before,function(){q.original.before.push(this)});i.each(q.after,function(){q.original.after.push(this)})}function c(w){var u,s,r=i.fn.cycle.transitions;if(w.fx.indexOf(",")>0){w.multiFx=true;w.fxs=w.fx.replace(/\s*/g,"").split(",");for(u=0;u<w.fxs.length;u++){var v=w.fxs[u];s=r[v];if(!s||!r.hasOwnProperty(v)||!i.isFunction(s)){f("discarding unknown transition: ",v);w.fxs.splice(u,1);u--}}if(!w.fxs.length){f("No valid transitions named; slideshow terminating.");return false}}else{if(w.fx=="all"){w.multiFx=true;w.fxs=[];for(p in r){s=r[p];if(r.hasOwnProperty(p)&&i.isFunction(s)){w.fxs.push(p)}}}}if(w.multiFx&&w.randomizeEffects){var t=Math.floor(Math.random()*20)+30;for(u=0;u<t;u++){var q=Math.floor(Math.random()*w.fxs.length);w.fxs.push(w.fxs.splice(q,1)[0])}a("randomized fx sequence: ",w.fxs)}return true}function j(r,q){r.addSlide=function(u,v){var t=i(u),w=t[0];if(!r.autostopCount){r.countdown++}q[v?"unshift":"push"](w);if(r.els){r.els[v?"unshift":"push"](w)}r.slideCount=q.length;t.css("position","absolute");t[v?"prependTo":"appendTo"](r.$cont);if(v){r.currSlide++;r.nextSlide++}if(!i.support.opacity&&r.cleartype&&!r.cleartypeNoBg){g(t)}if(r.fit&&r.width){t.width(r.width)}if(r.fit&&r.height&&r.height!="auto"){$slides.height(r.height)}w.cycleH=(r.fit&&r.height)?r.height:t.height();w.cycleW=(r.fit&&r.width)?r.width:t.width();t.css(r.cssBefore);if(r.pager){i.fn.cycle.createPagerAnchor(q.length-1,w,i(r.pager),q,r)}if(i.isFunction(r.onAddSlide)){r.onAddSlide(t)}else{t.hide()}}}i.fn.cycle.resetState=function(r,q){q=q||r.fx;r.before=[];r.after=[];r.cssBefore=i.extend({},r.original.cssBefore);r.cssAfter=i.extend({},r.original.cssAfter);r.animIn=i.extend({},r.original.animIn);r.animOut=i.extend({},r.original.animOut);r.fxFn=null;i.each(r.original.before,function(){r.before.push(this)});i.each(r.original.after,function(){r.after.push(this)});var s=i.fn.cycle.transitions[q];if(i.isFunction(s)){s(r.$cont,i(r.elements),r)}};function e(x,q,w,y){if(w&&q.busy&&q.manualTrump){i(x).stop(true,true);q.busy=false}if(q.busy){return}var u=q.$cont[0],A=x[q.currSlide],z=x[q.nextSlide];if(u.cycleStop!=q.stopCount||u.cycleTimeout===0&&!w){return}if(!w&&!u.cyclePause&&((q.autostop&&(--q.countdown<=0))||(q.nowrap&&!q.random&&q.nextSlide<q.currSlide))){if(q.end){q.end(q)}return}if(w||!u.cyclePause){var v=q.fx;A.cycleH=A.cycleH||i(A).height();A.cycleW=A.cycleW||i(A).width();z.cycleH=z.cycleH||i(z).height();z.cycleW=z.cycleW||i(z).width();if(q.multiFx){if(q.lastFx==undefined||++q.lastFx>=q.fxs.length){q.lastFx=0}v=q.fxs[q.lastFx];q.currFx=v}if(q.oneTimeFx){v=q.oneTimeFx;q.oneTimeFx=null}i.fn.cycle.resetState(q,v);if(q.before.length){i.each(q.before,function(B,C){if(u.cycleStop!=q.stopCount){return}C.apply(z,[A,z,q,y])})}var s=function(){i.each(q.after,function(B,C){if(u.cycleStop!=q.stopCount){return}C.apply(z,[A,z,q,y])})};if(q.nextSlide!=q.currSlide){q.busy=1;if(q.fxFn){q.fxFn(A,z,q,s,y)}else{if(i.isFunction(i.fn.cycle[q.fx])){i.fn.cycle[q.fx](A,z,q,s)}else{i.fn.cycle.custom(A,z,q,s,w&&q.fastOnEvent)}}}q.lastSlide=q.currSlide;if(q.random){q.currSlide=q.nextSlide;if(++q.randomIndex==x.length){q.randomIndex=0}q.nextSlide=q.randomMap[q.randomIndex]}else{var t=(q.nextSlide+1)==x.length;q.nextSlide=t?0:q.nextSlide+1;q.currSlide=t?x.length-1:q.nextSlide-1}if(q.pager){i.fn.cycle.updateActivePagerLink(q.pager,q.currSlide)}}var r=0;if(q.timeout&&!q.continuous){r=h(A,z,q,y)}else{if(q.continuous&&u.cyclePause){r=10}}if(r>0){u.cycleTimeout=setTimeout(function(){e(x,q,0,!q.rev)},r)}}i.fn.cycle.updateActivePagerLink=function(q,r){i(q).each(function(){i(this).find("a").removeClass("activeSlide").filter("a:eq("+r+")").addClass("activeSlide")})};function h(v,s,u,r){if(u.timeoutFn){var q=u.timeoutFn(v,s,u,r);while((q-u.speed)<250){q+=u.speed}a("calculated timeout: "+q+"; speed: "+u.speed);if(q!==false){return q}}return u.timeout}i.fn.cycle.next=function(q){o(q,q.rev?-1:1)};i.fn.cycle.prev=function(q){o(q,q.rev?1:-1)};function o(r,u){var q=r.elements;var t=r.$cont[0],s=t.cycleTimeout;if(s){clearTimeout(s);t.cycleTimeout=0}if(r.random&&u<0){r.randomIndex--;if(--r.randomIndex==-2){r.randomIndex=q.length-2}else{if(r.randomIndex==-1){r.randomIndex=q.length-1}}r.nextSlide=r.randomMap[r.randomIndex]}else{if(r.random){if(++r.randomIndex==q.length){r.randomIndex=0}r.nextSlide=r.randomMap[r.randomIndex]}else{r.nextSlide=r.currSlide+u;if(r.nextSlide<0){if(r.nowrap){return false}r.nextSlide=q.length-1}else{if(r.nextSlide>=q.length){if(r.nowrap){return false}r.nextSlide=0}}}}if(i.isFunction(r.prevNextClick)){r.prevNextClick(u>0,r.nextSlide,q[r.nextSlide])}e(q,r,1,u>=0);return false}function d(r,s){var q=i(s.pager);i.each(r,function(t,u){i.fn.cycle.createPagerAnchor(t,u,q,r,s)});i.fn.cycle.updateActivePagerLink(s.pager,s.startingSlide)}i.fn.cycle.createPagerAnchor=function(u,v,s,t,w){var r;if(i.isFunction(w.pagerAnchorBuilder)){r=w.pagerAnchorBuilder(u,v)}else{r='<a href="#">'+(u+1)+"</a>"}if(!r){return}var x=i(r);if(x.parents("body").length===0){var q=[];if(s.length>1){s.each(function(){var y=x.clone(true);i(this).append(y);q.push(y[0])});x=i(q)}else{x.appendTo(s)}}x.bind(w.pagerEvent,function(A){A.preventDefault();w.nextSlide=u;var z=w.$cont[0],y=z.cycleTimeout;if(y){clearTimeout(y);z.cycleTimeout=0}if(i.isFunction(w.pagerClick)){w.pagerClick(w.nextSlide,t[w.nextSlide])}e(t,w,1,w.currSlide<u);return false});if(w.pagerEvent!="click"){x.click(function(){return false})}if(w.pauseOnPagerHover){x.hover(function(){w.$cont[0].cyclePause++},function(){w.$cont[0].cyclePause--})}};i.fn.cycle.hopsFromLast=function(t,s){var r,q=t.lastSlide,u=t.currSlide;if(s){r=u>q?u-q:t.slideCount-q}else{r=u<q?q-u:q+t.slideCount-u}return r};function g(s){function r(t){t=parseInt(t).toString(16);return t.length<2?"0"+t:t}function q(w){for(;w&&w.nodeName.toLowerCase()!="html";w=w.parentNode){var t=i.css(w,"background-color");if(t.indexOf("rgb")>=0){var u=t.match(/\d+/g);return"#"+r(u[0])+r(u[1])+r(u[2])}if(t&&t!="transparent"){return t}}return"#ffffff"}s.each(function(){i(this).css("background-color",q(this))})}i.fn.cycle.commonReset=function(v,t,u,r,s,q){i(u.elements).not(v).hide();u.cssBefore.opacity=1;u.cssBefore.display="block";if(r!==false&&t.cycleW>0){u.cssBefore.width=t.cycleW}if(s!==false&&t.cycleH>0){u.cssBefore.height=t.cycleH}u.cssAfter=u.cssAfter||{};u.cssAfter.display="none";i(v).css("zIndex",u.slideCount+(q===true?1:0));i(t).css("zIndex",u.slideCount+(q===true?0:1))};i.fn.cycle.custom=function(B,v,q,s,r){var A=i(B),w=i(v);var t=q.speedIn,z=q.speedOut,u=q.easeIn,y=q.easeOut;w.css(q.cssBefore);if(r){if(typeof r=="number"){t=z=r}else{t=z=1}u=y=null}var x=function(){w.animate(q.animIn,t,u,s)};A.animate(q.animOut,z,y,function(){if(q.cssAfter){A.css(q.cssAfter)}if(!q.sync){x()}});if(q.sync){x()}};i.fn.cycle.transitions={fade:function(r,s,q){s.not(":eq("+q.currSlide+")").css("opacity",0);q.before.push(function(v,t,u){i.fn.cycle.commonReset(v,t,u);u.cssBefore.opacity=0});q.animIn={opacity:1};q.animOut={opacity:0};q.cssBefore={top:0,left:0}}};i.fn.cycle.ver=function(){return l};i.fn.cycle.defaults={fx:"fade",timeout:4000,timeoutFn:null,continuous:0,speed:1000,speedIn:null,speedOut:null,next:null,prev:null,prevNextClick:null,prevNextEvent:"click",pager:null,pagerClick:null,pagerEvent:"click",pagerAnchorBuilder:null,before:null,after:null,end:null,easing:null,easeIn:null,easeOut:null,shuffle:null,animIn:null,animOut:null,cssBefore:null,cssAfter:null,fxFn:null,height:"auto",startingSlide:0,sync:1,random:0,fit:0,containerResize:1,pause:0,pauseOnPagerHover:0,autostop:0,autostopCount:0,delay:0,slideExpr:null,cleartype:!i.support.opacity,cleartypeNoBg:false,nowrap:0,fastOnEvent:0,randomizeEffects:1,rev:0,manualTrump:true,requeueOnImageNotLoaded:true,requeueTimeout:250}})(jQuery);
function FABridge(b,a){this.target=b;this.remoteTypeCache={};this.remoteInstanceCache={};this.remoteFunctionCache={};this.localFunctionCache={};this.bridgeID=FABridge.nextBridgeID++;this.name=a;this.nextLocalFuncID=0;FABridge.instances[this.name]=this;FABridge.idMap[this.bridgeID]=this;return this}FABridge.TYPE_ASINSTANCE=1;FABridge.TYPE_ASFUNCTION=2;FABridge.TYPE_JSFUNCTION=3;FABridge.TYPE_ANONYMOUS=4;FABridge.initCallbacks={};FABridge.argsToArray=function(b){var a=[];for(var c=0;c<b.length;c++){a[c]=b[c]}return a};function instanceFactory(a){this.fb_instance_id=a;return this}function FABridge__invokeJSFunction(a){var c=a[0];var b=a.concat();b.shift();var d=FABridge.extractBridgeFromID(c);return d.invokeLocalFunction(c,b)}FABridge.addInitializationCallback=function(b,d){var c=FABridge.instances[b];if(c!=undefined){d.call(c);return}var a=FABridge.initCallbacks[b];if(a==null){FABridge.initCallbacks[b]=a=[]}a.push(d)};function FABridge__bridgeInitialized(g){var a=document.getElementsByTagName("object");var f=a.length;var d=[];if(f>0){for(var u=0;u<f;u++){if(typeof a[u].SetVariable!="undefined"){d[d.length]=a[u]}}}var n=document.getElementsByTagName("embed");var b=n.length;var t=[];if(b>0){for(var r=0;r<b;r++){if(typeof n[r].SetVariable!="undefined"){t[t.length]=n[r]}}}var c=d.length;var s=t.length;var h="bridgeName="+g;if((c==1&&!s)||(c==1&&s==1)){FABridge.attachBridge(d[0],g)}else{if(s==1&&!c){FABridge.attachBridge(t[0],g)}else{var v=false;if(c>1){for(var q=0;q<c;q++){var x=d[q].childNodes;for(var p=0;p<x.length;p++){var e=x[p];if(e.nodeType==1&&e.tagName.toLowerCase()=="param"&&e.name.toLowerCase()=="flashvars"&&e.value.indexOf(h)>=0){FABridge.attachBridge(d[q],g);v=true;break}}if(v){break}}}if(!v&&s>1){for(var o=0;o<s;o++){var w=t[o].attributes.getNamedItem("flashVars").nodeValue;if(w.indexOf(h)>=0){FABridge.attachBridge(t[o],g);break}}}}}return true}FABridge.nextBridgeID=0;FABridge.instances={};FABridge.idMap={};FABridge.refCount=0;FABridge.extractBridgeFromID=function(b){var a=(b>>16);return FABridge.idMap[a]};FABridge.attachBridge=function(a,c){var b=new FABridge(a,c);FABridge[c]=b;var e=FABridge.initCallbacks[c];if(e==null){return}for(var d=0;d<e.length;d++){e[d].call(b)}delete FABridge.initCallbacks[c]};FABridge.blockedMethods={toString:true,get:true,set:true,call:true};FABridge.prototype={root:function(){return this.deserialize(this.target.getRoot())},releaseASObjects:function(){return this.target.releaseASObjects()},releaseNamedASObject:function(b){if(typeof(b)!="object"){return false}else{var a=this.target.releaseNamedASObject(b.fb_instance_id);return a}},create:function(a){return this.deserialize(this.target.create(a))},makeID:function(a){return(this.bridgeID<<16)+a},getPropertyFromAS:function(b,a){if(FABridge.refCount>0){throw new Error("You are trying to call recursively into the Flash Player which is not allowed. In most cases the JavaScript setTimeout function, can be used as a workaround.")}else{FABridge.refCount++;retVal=this.target.getPropFromAS(b,a);retVal=this.handleError(retVal);FABridge.refCount--;return retVal}},setPropertyInAS:function(c,b,a){if(FABridge.refCount>0){throw new Error("You are trying to call recursively into the Flash Player which is not allowed. In most cases the JavaScript setTimeout function, can be used as a workaround.")}else{FABridge.refCount++;retVal=this.target.setPropInAS(c,b,this.serialize(a));retVal=this.handleError(retVal);FABridge.refCount--;return retVal}},callASFunction:function(b,a){if(FABridge.refCount>0){throw new Error("You are trying to call recursively into the Flash Player which is not allowed. In most cases the JavaScript setTimeout function, can be used as a workaround.")}else{FABridge.refCount++;retVal=this.target.invokeASFunction(b,this.serialize(a));retVal=this.handleError(retVal);FABridge.refCount--;return retVal}},callASMethod:function(b,c,a){if(FABridge.refCount>0){throw new Error("You are trying to call recursively into the Flash Player which is not allowed. In most cases the JavaScript setTimeout function, can be used as a workaround.")}else{FABridge.refCount++;a=this.serialize(a);retVal=this.target.invokeASMethod(b,c,a);retVal=this.handleError(retVal);FABridge.refCount--;return retVal}},invokeLocalFunction:function(d,b){var a;var c=this.localFunctionCache[d];if(c!=undefined){a=this.serialize(c.apply(null,this.deserialize(b)))}return a},getTypeFromName:function(a){return this.remoteTypeCache[a]},createProxy:function(c,b){var d=this.getTypeFromName(b);instanceFactory.prototype=d;var a=new instanceFactory(c);this.remoteInstanceCache[c]=a;return a},getProxy:function(a){return this.remoteInstanceCache[a]},addTypeDataToCache:function(d){newType=new ASProxy(this,d.name);var b=d.accessors;for(var c=0;c<b.length;c++){this.addPropertyToType(newType,b[c])}var a=d.methods;for(var c=0;c<a.length;c++){if(FABridge.blockedMethods[a[c]]==undefined){this.addMethodToType(newType,a[c])}}this.remoteTypeCache[newType.typeName]=newType;return newType},addPropertyToType:function(a,e){var f=e.charAt(0);var b;var d;if(f>="a"&&f<="z"){d="get"+f.toUpperCase()+e.substr(1);b="set"+f.toUpperCase()+e.substr(1)}else{d="get"+e;b="set"+e}a[b]=function(c){this.bridge.setPropertyInAS(this.fb_instance_id,e,c)};a[d]=function(){return this.bridge.deserialize(this.bridge.getPropertyFromAS(this.fb_instance_id,e))}},addMethodToType:function(a,b){a[b]=function(){return this.bridge.deserialize(this.bridge.callASMethod(this.fb_instance_id,b,FABridge.argsToArray(arguments)))}},getFunctionProxy:function(a){var b=this;if(this.remoteFunctionCache[a]==null){this.remoteFunctionCache[a]=function(){b.callASFunction(a,FABridge.argsToArray(arguments))}}return this.remoteFunctionCache[a]},getFunctionID:function(a){if(a.__bridge_id__==undefined){a.__bridge_id__=this.makeID(this.nextLocalFuncID++);this.localFunctionCache[a.__bridge_id__]=a}return a.__bridge_id__},serialize:function(d){var a={};var c=typeof(d);if(c=="number"||c=="string"||c=="boolean"||c==null||c==undefined){a=d}else{if(d instanceof Array){a=[];for(var b=0;b<d.length;b++){a[b]=this.serialize(d[b])}}else{if(c=="function"){a.type=FABridge.TYPE_JSFUNCTION;a.value=this.getFunctionID(d)}else{if(d instanceof ASProxy){a.type=FABridge.TYPE_ASINSTANCE;a.value=d.fb_instance_id}else{a.type=FABridge.TYPE_ANONYMOUS;a.value=d}}}}return a},deserialize:function(e){var a;var c=typeof(e);if(c=="number"||c=="string"||c=="boolean"||e==null||e==undefined){a=this.handleError(e)}else{if(e instanceof Array){a=[];for(var b=0;b<e.length;b++){a[b]=this.deserialize(e[b])}}else{if(c=="object"){for(var b=0;b<e.newTypes.length;b++){this.addTypeDataToCache(e.newTypes[b])}for(var d in e.newRefs){this.createProxy(d,e.newRefs[d])}if(e.type==FABridge.TYPE_PRIMITIVE){a=e.value}else{if(e.type==FABridge.TYPE_ASFUNCTION){a=this.getFunctionProxy(e.value)}else{if(e.type==FABridge.TYPE_ASINSTANCE){a=this.getProxy(e.value)}else{if(e.type==FABridge.TYPE_ANONYMOUS){a=e.value}}}}}}}return a},addRef:function(a){this.target.incRef(a.fb_instance_id)},release:function(a){this.target.releaseRef(a.fb_instance_id)},handleError:function(b){if(typeof(b)=="string"&&b.indexOf("__FLASHERROR")==0){var a=b.split("||");if(FABridge.refCount>0){FABridge.refCount--}throw new Error(a[1]);return b}else{return b}}};ASProxy=function(b,a){this.bridge=b;this.typeName=a;return this};ASProxy.prototype={get:function(a){return this.bridge.deserialize(this.bridge.getPropertyFromAS(this.fb_instance_id,a))},set:function(b,a){this.bridge.setPropertyInAS(this.fb_instance_id,b,a)},call:function(b,a){this.bridge.callASMethod(this.fb_instance_id,b,a)},addRef:function(){this.bridge.addRef(this)},release:function(){this.bridge.release(this)}};
/**
 * BxSlider v4.1.1 - Fully loaded, responsive content slider
 * http://bxslider.com
 *
 * Copyright 2013, Steven Wanderski - http://stevenwanderski.com - http://bxcreative.com
 * Written while drinking Belgian ales and listening to jazz
 *
 * Released under the MIT license - http://opensource.org/licenses/MIT
 */
!function(t){var e={},s={mode:"horizontal",slideSelector:"",infiniteLoop:!0,hideControlOnEnd:!1,speed:500,easing:null,slideMargin:0,startSlide:0,randomStart:!1,captions:!1,ticker:!1,tickerHover:!1,adaptiveHeight:!1,adaptiveHeightSpeed:500,video:!1,useCSS:!0,preloadImages:"visible",responsive:!0,touchEnabled:!0,swipeThreshold:50,oneToOneTouch:!0,preventDefaultSwipeX:!0,preventDefaultSwipeY:!1,pager:!0,pagerType:"full",pagerShortSeparator:" / ",pagerSelector:null,buildPager:null,pagerCustom:null,controls:!0,nextText:"Next",prevText:"Prev",nextSelector:null,prevSelector:null,autoControls:!1,startText:"Start",stopText:"Stop",autoControlsCombine:!1,autoControlsSelector:null,auto:!1,pause:4e3,autoStart:!0,autoDirection:"next",autoHover:!1,autoDelay:0,minSlides:1,maxSlides:1,moveSlides:0,slideWidth:0,onSliderLoad:function(){},onSlideBefore:function(){},onSlideAfter:function(){},onSlideNext:function(){},onSlidePrev:function(){}};t.fn.bxSlider=function(n){if(0==this.length)return this;if(this.length>1)return this.each(function(){t(this).bxSlider(n)}),this;var o={},r=this;e.el=this;var a=t(window).width(),l=t(window).height(),d=function(){o.settings=t.extend({},s,n),o.settings.slideWidth=parseInt(o.settings.slideWidth),o.children=r.children(o.settings.slideSelector),o.children.length<o.settings.minSlides&&(o.settings.minSlides=o.children.length),o.children.length<o.settings.maxSlides&&(o.settings.maxSlides=o.children.length),o.settings.randomStart&&(o.settings.startSlide=Math.floor(Math.random()*o.children.length)),o.active={index:o.settings.startSlide},o.carousel=o.settings.minSlides>1||o.settings.maxSlides>1,o.carousel&&(o.settings.preloadImages="all"),o.minThreshold=o.settings.minSlides*o.settings.slideWidth+(o.settings.minSlides-1)*o.settings.slideMargin,o.maxThreshold=o.settings.maxSlides*o.settings.slideWidth+(o.settings.maxSlides-1)*o.settings.slideMargin,o.working=!1,o.controls={},o.interval=null,o.animProp="vertical"==o.settings.mode?"top":"left",o.usingCSS=o.settings.useCSS&&"fade"!=o.settings.mode&&function(){var t=document.createElement("div"),e=["WebkitPerspective","MozPerspective","OPerspective","msPerspective"];for(var i in e)if(void 0!==t.style[e[i]])return o.cssPrefix=e[i].replace("Perspective","").toLowerCase(),o.animProp="-"+o.cssPrefix+"-transform",!0;return!1}(),"vertical"==o.settings.mode&&(o.settings.maxSlides=o.settings.minSlides),r.data("origStyle",r.attr("style")),r.children(o.settings.slideSelector).each(function(){t(this).data("origStyle",t(this).attr("style"))}),c()},c=function(){r.wrap('<div class="bx-wrapper"><div class="bx-viewport"></div></div>'),o.viewport=r.parent(),o.loader=t('<div class="bx-loading" />'),o.viewport.prepend(o.loader),r.css({width:"horizontal"https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/==o.settings.mode?100*o.children.length+215+"%":"auto",position:"relative"}),o.usingCSS&&o.settings.easing?r.css("-"+o.cssPrefix+"-transition-timing-function",o.settings.easing):o.settings.easing||(o.settings.easing="swing"),f(),o.viewport.css({width:"100%",overflow:"hidden",position:"relative"}),o.viewport.parent().css({maxWidth:v()}),o.settings.pager||o.viewport.parent().css({margin:"0 auto 0px"}),o.children.css({"float":"horizontal"==o.settings.mode?"left":"none",listStyle:"none",position:"relative"}),o.children.css("width",u()),"horizontal"==o.settings.mode&&o.settings.slideMargin>0&&o.children.css("marginRight",o.settings.slideMargin),"vertical"==o.settings.mode&&o.settings.slideMargin>0&&o.children.css("marginBottom",o.settings.slideMargin),"fade"==o.settings.mode&&(o.children.css({position:"absolute",zIndex:0,display:"none"}),o.children.eq(o.settings.startSlide).css({zIndex:50,display:"block"})),o.controls.el=t('<div class="bx-controls" />'),o.settings.captions&&P(),o.active.last=o.settings.startSlide==x()-1,o.settings.video&&r.fitVids();var e=o.children.eq(o.settings.startSlide);"all"==o.settings.preloadImages&&(e=o.children),o.settings.ticker?o.settings.pager=!1:(o.settings.pager&&T(),o.settings.controls&&C(),o.settings.auto&&o.settings.autoControls&&E(),(o.settings.controls||o.settings.autoControls||o.settings.pager)&&o.viewport.after(o.controls.el)),g(e,h)},g=function(e,i){var s=e.find("img, iframe").length;if(0==s)return i(),void 0;var n=0;e.find("img, iframe").each(function(){t(this).one("load",function(){++n==s&&i()}).each(function(){this.complete&&t(this).load()})})},h=function(){if(o.settings.infiniteLoop&&"fade"!=o.settings.mode&&!o.settings.ticker){var e="vertical"==o.settings.mode?o.settings.minSlides:o.settings.maxSlides,i=o.children.slice(0,e).clone().addClass("bx-clone"),s=o.children.slice(-e).clone().addClass("bx-clone");r.append(i).prepend(s)}o.loader.remove(),S(),"vertical"==o.settings.mode&&(o.settings.adaptiveHeight=!0),o.viewport.height(p()),r.redrawSlider(),o.settings.onSliderLoad(o.active.index),o.initialized=!0,o.settings.responsive&&t(window).bind("resize",B),o.settings.auto&&o.settings.autoStart&&H(),o.settings.ticker&&L(),o.settings.pager&&I(o.settings.startSlide),o.settings.controls&&W(),o.settings.touchEnabled&&!o.settings.ticker&&O()},p=function(){var e=0,s=t();if("vertical"==o.settings.mode||o.settings.adaptiveHeight)if(o.carousel){var n=1==o.settings.moveSlides?o.active.index:o.active.index*m();for(s=o.children.eq(n),i=1;i<=o.settings.maxSlides-1;i++)s=n+i>=o.children.length?s.add(o.children.eq(i-1)):s.add(o.children.eq(n+i))}else s=o.children.eq(o.active.index);else s=o.children;return"vertical"==o.settings.mode?(s.each(function(){e+=t(this).outerHeight()}),o.settings.slideMargin>0&&(e+=o.settings.slideMargin*(o.settings.minSlides-1))):e=Math.max.apply(Math,s.map(function(){return t(this).outerHeight(!1)}).get()),e},v=function(){var t="100%";return o.settings.slideWidth>0&&(t="horizontal"==o.settings.mode?o.settings.maxSlides*o.settings.slideWidth+(o.settings.maxSlides-1)*o.settings.slideMargin:o.settings.slideWidth),t},u=function(){var t=o.settings.slideWidth,e=o.viewport.width();return 0==o.settings.slideWidth||o.settings.slideWidth>e&&!o.carousel||"vertical"==o.settings.mode?t=e:o.settings.maxSlides>1&&"horizontal"==o.settings.mode&&(e>o.maxThreshold||e<o.minThreshold&&(t=(e-o.settings.slideMargin*(o.settings.minSlides-1))/o.settings.minSlides)),t},f=function(){var t=1;if("horizontal"==o.settings.mode&&o.settings.slideWidth>0)if(o.viewport.width()<o.minThreshold)t=o.settings.minSlides;else if(o.viewport.width()>o.maxThreshold)t=o.settings.maxSlides;else{var e=o.children.first().width();t=Math.floor(o.viewport.width()/e)}else"vertical"==o.settings.mode&&(t=o.settings.minSlides);return t},x=function(){var t=0;if(o.settings.moveSlides>0)if(o.settings.infiniteLoop)t=o.children.length/m();else for(var e=0,i=0;e<o.children.length;)++t,e=i+f(),i+=o.settings.moveSlides<=f()?o.settings.moveSlides:f();else t=Math.ceil(o.children.length/f());return t},m=function(){return o.settings.moveSlides>0&&o.settings.moveSlides<=f()?o.settings.moveSlides:f()},S=function(){if(o.children.length>o.settings.maxSlides&&o.active.last&&!o.settings.infiniteLoop){if("horizontal"==o.settings.mode){var t=o.children.last(),e=t.position();b(-(e.left-(o.viewport.width()-t.width())),"reset",0)}else if("vertical"==o.settings.mode){var i=o.children.length-o.settings.minSlides,e=o.children.eq(i).position();b(-e.top,"reset",0)}}else{var e=o.children.eq(o.active.index*m()).position();o.active.index==x()-1&&(o.active.last=!0),void 0!=e&&("horizontal"https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/==o.settings.mode?b(-e.left,"reset",0):"vertical"==o.settings.mode&&b(-e.top,"reset",0))}},b=function(t,e,i,s){if(o.usingCSS){var n="vertical"==o.settings.mode?"translate3d(0, "+t+"px, 0)":"translate3d("+t+"px, 0, 0)";r.css("-"+o.cssPrefix+"-transition-duration",i/1e3+"s"),"slide"==e?(r.css(o.animProp,n),r.bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd",function(){r.unbind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd"),D()})):"reset"==e?r.css(o.animProp,n):"ticker"==e&&(r.css("-"+o.cssPrefix+"-transition-timing-function","linear"),r.css(o.animProp,n),r.bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd",function(){r.unbind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd"),b(s.resetValue,"reset",0),N()}))}else{var a={};a[o.animProp]=t,"slide"==e?r.animate(a,i,o.settings.easing,function(){D()}):"reset"==e?r.css(o.animProp,t):"ticker"==e&&r.animate(a,speed,"linear",function(){b(s.resetValue,"reset",0),N()})}},w=function(){for(var e="",i=x(),s=0;i>s;s++){var n="";o.settings.buildPager&&t.isFunction(o.settings.buildPager)?(n=o.settings.buildPager(s),o.pagerEl.addClass("bx-custom-pager")):(n=s+1,o.pagerEl.addClass("bx-default-pager")),e+='<div class="bx-pager-item"><a href="" data-slide-index="'+s+'" class="bx-pager-link">'+n+"</a></div>"}o.pagerEl.html(e)},T=function(){o.settings.pagerCustom?o.pagerEl=t(o.settings.pagerCustom):(o.pagerEl=t('<div class="bx-pager" />'),o.settings.pagerSelector?t(o.settings.pagerSelector).html(o.pagerEl):o.controls.el.addClass("bx-has-pager").append(o.pagerEl),w()),o.pagerEl.delegate("a","click",q)},C=function(){o.controls.next=t('<a class="bx-next" href="">'+o.settings.nextText+"</a>"),o.controls.prev=t('<a class="bx-prev" href="">'+o.settings.prevText+"</a>"),o.controls.next.bind("click",y),o.controls.prev.bind("click",z),o.settings.nextSelector&&t(o.settings.nextSelector).append(o.controls.next),o.settings.prevSelector&&t(o.settings.prevSelector).append(o.controls.prev),o.settings.nextSelector||o.settings.prevSelector||(o.controls.directionEl=t('<div class="bx-controls-direction" />'),o.controls.directionEl.append(o.controls.prev).append(o.controls.next),o.controls.el.addClass("bx-has-controls-direction").append(o.controls.directionEl))},E=function(){o.controls.start=t('<div class="bx-controls-auto-item"><a class="bx-start" href="">'+o.settings.startText+"</a></div>"),o.controls.stop=t('<div class="bx-controls-auto-item"><a class="bx-stop" href="">'+o.settings.stopText+"</a></div>"),o.controls.autoEl=t('<div class="bx-controls-auto" />'),o.controls.autoEl.delegate(".bx-start","click",k),o.controls.autoEl.delegate(".bx-stop","click",M),o.settings.autoControlsCombine?o.controls.autoEl.append(o.controls.start):o.controls.autoEl.append(o.controls.start).append(o.controls.stop),o.settings.autoControlsSelector?t(o.settings.autoControlsSelector).html(o.controls.autoEl):o.controls.el.addClass("bx-has-controls-auto").append(o.controls.autoEl),A(o.settings.autoStart?"stop":"start")},P=function(){o.children.each(function(){var e=t(this).find("img:first").attr("title");void 0!=e&&(""+e).length&&t(this).append('<div class="bx-caption"><span>'+e+"</span></div>")})},y=function(t){o.settings.auto&&r.stopAuto(),r.goToNextSlide(),t.preventDefault()},z=function(t){o.settings.auto&&r.stopAuto(),r.goToPrevSlide(),t.preventDefault()},k=function(t){r.startAuto(),t.preventDefault()},M=function(t){r.stopAuto(),t.preventDefault()},q=function(e){o.settings.auto&&r.stopAuto();var i=t(e.currentTarget),s=parseInt(i.attr("data-slide-index"));s!=o.active.index&&r.goToSlide(s),e.preventDefault()},I=function(e){var i=o.children.length;return"short"==o.settings.pagerType?(o.settings.maxSlides>1&&(i=Math.ceil(o.children.length/o.settings.maxSlides)),o.pagerEl.html(e+1+o.settings.pagerShortSeparator+i),void 0):(o.pagerEl.find("a").removeClass("active"),o.pagerEl.each(function(i,s){t(s).find("a").eq(e).addClass("active")}),void 0)},D=function(){if(o.settings.infiniteLoop){var t="";0==o.active.index?t=o.children.eq(0).position():o.active.index==x()-1&&o.carousel?t=o.children.eq((x()-1)*m()).position():o.active.index==o.children.length-1&&(t=o.children.eq(o.children.length-1).position()),"horizontal"https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/==o.settings.mode?b(-t.left,"reset",0):"vertical"==o.settings.mode&&b(-t.top,"reset",0)}o.working=!1,o.settings.onSlideAfter(o.children.eq(o.active.index),o.oldIndex,o.active.index)},A=function(t){o.settings.autoControlsCombine?o.controls.autoEl.html(o.controls[t]):(o.controls.autoEl.find("a").removeClass("active"),o.controls.autoEl.find("a:not(.bx-"+t+")").addClass("active"))},W=function(){1==x()?(o.controls.prev.addClass("disabled"),o.controls.next.addClass("disabled")):!o.settings.infiniteLoop&&o.settings.hideControlOnEnd&&(0==o.active.index?(o.controls.prev.addClass("disabled"),o.controls.next.removeClass("disabled")):o.active.index==x()-1?(o.controls.next.addClass("disabled"),o.controls.prev.removeClass("disabled")):(o.controls.prev.removeClass("disabled"),o.controls.next.removeClass("disabled")))},H=function(){o.settings.autoDelay>0?setTimeout(r.startAuto,o.settings.autoDelay):r.startAuto(),o.settings.autoHover&&r.hover(function(){o.interval&&(r.stopAuto(!0),o.autoPaused=!0)},function(){o.autoPaused&&(r.startAuto(!0),o.autoPaused=null)})},L=function(){var e=0;if("next"==o.settings.autoDirection)r.append(o.children.clone().addClass("bx-clone"));else{r.prepend(o.children.clone().addClass("bx-clone"));var i=o.children.first().position();e="horizontal"https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/==o.settings.mode?-i.left:-i.top}b(e,"reset",0),o.settings.pager=!1,o.settings.controls=!1,o.settings.autoControls=!1,o.settings.tickerHover&&!o.usingCSS&&o.viewport.hover(function(){r.stop()},function(){var e=0;o.children.each(function(){e+="horizontal"https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/==o.settings.mode?t(this).outerWidth(!0):t(this).outerHeight(!0)});var i=o.settings.speed/e,s="horizontal"==o.settings.mode?"left":"top",n=i*(e-Math.abs(parseInt(r.css(s))));N(n)}),N()},N=function(t){speed=t?t:o.settings.speed;var e={left:0,top:0},i={left:0,top:0};"next"==o.settings.autoDirection?e=r.find(".bx-clone").first().position():i=o.children.first().position();var s="horizontal"https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/==o.settings.mode?-e.left:-e.top,n="horizontal"https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/==o.settings.mode?-i.left:-i.top,a={resetValue:n};b(s,"ticker",speed,a)},O=function(){o.touch={start:{x:0,y:0},end:{x:0,y:0}},o.viewport.bind("touchstart",X)},X=function(t){if(o.working)t.preventDefault();else{o.touch.originalPos=r.position();var e=t.originalEvent;o.touch.start.x=e.changedTouches[0].pageX,o.touch.start.y=e.changedTouches[0].pageY,o.viewport.bind("touchmove",Y),o.viewport.bind("touchend",V)}},Y=function(t){var e=t.originalEvent,i=Math.abs(e.changedTouches[0].pageX-o.touch.start.x),s=Math.abs(e.changedTouches[0].pageY-o.touch.start.y);if(3*i>s&&o.settings.preventDefaultSwipeX?t.preventDefault():3*s>i&&o.settings.preventDefaultSwipeY&&t.preventDefault(),"fade"!=o.settings.mode&&o.settings.oneToOneTouch){var n=0;if("horizontal"==o.settings.mode){var r=e.changedTouches[0].pageX-o.touch.start.x;n=o.touch.originalPos.left+r}else{var r=e.changedTouches[0].pageY-o.touch.start.y;n=o.touch.originalPos.top+r}b(n,"reset",0)}},V=function(t){o.viewport.unbind("touchmove",Y);var e=t.originalEvent,i=0;if(o.touch.end.x=e.changedTouches[0].pageX,o.touch.end.y=e.changedTouches[0].pageY,"fade"==o.settings.mode){var s=Math.abs(o.touch.start.x-o.touch.end.x);s>=o.settings.swipeThreshold&&(o.touch.start.x>o.touch.end.x?r.goToNextSlide():r.goToPrevSlide(),r.stopAuto())}else{var s=0;"horizontal"==o.settings.mode?(s=o.touch.end.x-o.touch.start.x,i=o.touch.originalPos.left):(s=o.touch.end.y-o.touch.start.y,i=o.touch.originalPos.top),!o.settings.infiniteLoop&&(0==o.active.index&&s>0||o.active.last&&0>s)?b(i,"reset",200):Math.abs(s)>=o.settings.swipeThreshold?(0>s?r.goToNextSlide():r.goToPrevSlide(),r.stopAuto()):b(i,"reset",200)}o.viewport.unbind("touchend",V)},B=function(){var e=t(window).width(),i=t(window).height();(a!=e||l!=i)&&(a=e,l=i,r.redrawSlider())};return r.goToSlide=function(e,i){if(!o.working&&o.active.index!=e)if(o.working=!0,o.oldIndex=o.active.index,o.active.index=0>e?x()-1:e>=x()?0:e,o.settings.onSlideBefore(o.children.eq(o.active.index),o.oldIndex,o.active.index),"next"==i?o.settings.onSlideNext(o.children.eq(o.active.index),o.oldIndex,o.active.index):"prev"==i&&o.settings.onSlidePrev(o.children.eq(o.active.index),o.oldIndex,o.active.index),o.active.last=o.active.index>=x()-1,o.settings.pager&&I(o.active.index),o.settings.controls&&W(),"fade"==o.settings.mode)o.settings.adaptiveHeight&&o.viewport.height()!=p()&&o.viewport.animate({height:p()},o.settings.adaptiveHeightSpeed),o.children.filter(":visible").fadeOut(o.settings.speed).css({zIndex:0}),o.children.eq(o.active.index).css("zIndex",51).fadeIn(o.settings.speed,function(){t(this).css("zIndex",50),D()});else{o.settings.adaptiveHeight&&o.viewport.height()!=p()&&o.viewport.animate({height:p()},o.settings.adaptiveHeightSpeed);var s=0,n={left:0,top:0};if(!o.settings.infiniteLoop&&o.carousel&&o.active.last)if("horizontal"==o.settings.mode){var a=o.children.eq(o.children.length-1);n=a.position(),s=o.viewport.width()-a.outerWidth()}else{var l=o.children.length-o.settings.minSlides;n=o.children.eq(l).position()}else if(o.carousel&&o.active.last&&"prev"==i){var d=1==o.settings.moveSlides?o.settings.maxSlides-m():(x()-1)*m()-(o.children.length-o.settings.maxSlides),a=r.children(".bx-clone").eq(d);n=a.position()}else if("next"==i&&0==o.active.index)n=r.find("> .bx-clone").eq(o.settings.maxSlides).position(),o.active.last=!1;else if(e>=0){var c=e*m();n=o.children.eq(c).position()}if("undefined"!=typeof n){var g="horizontal"https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/==o.settings.mode?-(n.left-s):-n.top;b(g,"slide",o.settings.speed)}}},r.goToNextSlide=function(){if(o.settings.infiniteLoop||!o.active.last){var t=parseInt(o.active.index)+1;r.goToSlide(t,"next")}},r.goToPrevSlide=function(){if(o.settings.infiniteLoop||0!=o.active.index){var t=parseInt(o.active.index)-1;r.goToSlide(t,"prev")}},r.startAuto=function(t){o.interval||(o.interval=setInterval(function(){"next"==o.settings.autoDirection?r.goToNextSlide():r.goToPrevSlide()},o.settings.pause),o.settings.autoControls&&1!=t&&A("stop"))},r.stopAuto=function(t){o.interval&&(clearInterval(o.interval),o.interval=null,o.settings.autoControls&&1!=t&&A("start"))},r.getCurrentSlide=function(){return o.active.index},r.getSlideCount=function(){return o.children.length},r.redrawSlider=function(){o.children.add(r.find(".bx-clone")).outerWidth(u()),o.viewport.css("height",p()),o.settings.ticker||S(),o.active.last&&(o.active.index=x()-1),o.active.index>=x()&&(o.active.last=!0),o.settings.pager&&!o.settings.pagerCustom&&(w(),I(o.active.index))},r.destroySlider=function(){o.initialized&&(o.initialized=!1,t(".bx-clone",this).remove(),o.children.each(function(){void 0!=t(this).data("origStyle")?t(this).attr("style",t(this).data("origStyle")):t(this).removeAttr("style")}),void 0!=t(this).data("origStyle")?this.attr("style",t(this).data("origStyle")):t(this).removeAttr("style"),t(this).unwrap().unwrap(),o.controls.el&&o.controls.el.remove(),o.controls.next&&o.controls.next.remove(),o.controls.prev&&o.controls.prev.remove(),o.pagerEl&&o.pagerEl.remove(),t(".bx-caption",this).remove(),o.controls.autoEl&&o.controls.autoEl.remove(),clearInterval(o.interval),o.settings.responsive&&t(window).unbind("resize",B))},r.reloadSlider=function(t){void 0!=t&&(n=t),r.destroySlider(),d()},d(),this}}(jQuery);
/*! Video.js v4.4.3 Copyright 2014 Brightcove, Inc. https://github.com/videojs/video.js/blob/master/LICENSE */ 
(function() {var b=void 0,f=!0,h=null,l=!1;function m(){return function(){}}function p(a){return function(){return this[a]}}function r(a){return function(){return a}}var t;document.createElement("video");document.createElement("audio");document.createElement("track");function u(a,c,d){if("string"===typeof a){0===a.indexOf("#")&&(a=a.slice(1));if(u.va[a])return u.va[a];a=u.u(a)}if(!a||!a.nodeName)throw new TypeError("The element or ID supplied is not valid. (videojs)");return a.player||new u.Player(a,c,d)}
var videojs=u;window.Wd=window.Xd=u;u.Rb="4.4";u.Ec="https:"==document.location.protocol?"https://":"http://";u.options={techOrder:["html5","flash"],html5:{},flash:{},width:300,height:150,defaultVolume:0,children:{mediaLoader:{},posterImage:{},textTrackDisplay:{},loadingSpinner:{},bigPlayButton:{},controlBar:{}},notSupportedMessage:'Sorry, no compatible source and playback technology were found for this video. Try using another browser like <a href="http://bit.ly/ccMUEC">Chrome</a> or download the latest <a href="http://adobe.ly/mwfN1">Adobe Flash Player</a>.'};
"GENERATED_CDN_VSN"!==u.Rb&&(videojs.options.flash.swf=u.Ec+"vjs.zencdn.net/"+u.Rb+"/video-js.swf");u.va={};"function"===typeof define&&define.amd?define([],function(){return videojs}):"object"===typeof exports&&"object"===typeof module&&(module.exports=videojs);u.ka=u.CoreObject=m();
u.ka.extend=function(a){var c,d;a=a||{};c=a.init||a.h||this.prototype.init||this.prototype.h||m();d=function(){c.apply(this,arguments)};d.prototype=u.k.create(this.prototype);d.prototype.constructor=d;d.extend=u.ka.extend;d.create=u.ka.create;for(var e in a)a.hasOwnProperty(e)&&(d.prototype[e]=a[e]);return d};u.ka.create=function(){var a=u.k.create(this.prototype);this.apply(a,arguments);return a};
u.d=function(a,c,d){var e=u.getData(a);e.z||(e.z={});e.z[c]||(e.z[c]=[]);d.s||(d.s=u.s++);e.z[c].push(d);e.U||(e.disabled=l,e.U=function(c){if(!e.disabled){c=u.ic(c);var d=e.z[c.type];if(d)for(var d=d.slice(0),k=0,q=d.length;k<q&&!c.pc();k++)d[k].call(a,c)}});1==e.z[c].length&&(document.addEventListener?a.addEventListener(c,e.U,l):document.attachEvent&&a.attachEvent("on"+c,e.U))};
u.o=function(a,c,d){if(u.mc(a)){var e=u.getData(a);if(e.z)if(c){var g=e.z[c];if(g){if(d){if(d.s)for(e=0;e<g.length;e++)g[e].s===d.s&&g.splice(e--,1)}else e.z[c]=[];u.ec(a,c)}}else for(g in e.z)c=g,e.z[c]=[],u.ec(a,c)}};u.ec=function(a,c){var d=u.getData(a);0===d.z[c].length&&(delete d.z[c],document.removeEventListener?a.removeEventListener(c,d.U,l):document.detachEvent&&a.detachEvent("on"+c,d.U));u.Ab(d.z)&&(delete d.z,delete d.U,delete d.disabled);u.Ab(d)&&u.tc(a)};
u.ic=function(a){function c(){return f}function d(){return l}if(!a||!a.Bb){var e=a||window.event;a={};for(var g in e)"layerX"!==g&&("layerY"!==g&&"keyboardEvent.keyLocation"!==g)&&("returnValue"==g&&e.preventDefault||(a[g]=e[g]));a.target||(a.target=a.srcElement||document);a.relatedTarget=a.fromElement===a.target?a.toElement:a.fromElement;a.preventDefault=function(){e.preventDefault&&e.preventDefault();a.returnValue=l;a.zb=c};a.zb=d;a.stopPropagation=function(){e.stopPropagation&&e.stopPropagation();
a.cancelBubble=f;a.Bb=c};a.Bb=d;a.stopImmediatePropagation=function(){e.stopImmediatePropagation&&e.stopImmediatePropagation();a.pc=c;a.stopPropagation()};a.pc=d;if(a.clientX!=h){g=document.documentElement;var j=document.body;a.pageX=a.clientX+(g&&g.scrollLeft||j&&j.scrollLeft||0)-(g&&g.clientLeft||j&&j.clientLeft||0);a.pageY=a.clientY+(g&&g.scrollTop||j&&j.scrollTop||0)-(g&&g.clientTop||j&&j.clientTop||0)}a.which=a.charCode||a.keyCode;a.button!=h&&(a.button=a.button&1?0:a.button&4?1:a.button&2?2:
0)}return a};u.j=function(a,c){var d=u.mc(a)?u.getData(a):{},e=a.parentNode||a.ownerDocument;"string"===typeof c&&(c={type:c,target:a});c=u.ic(c);d.U&&d.U.call(a,c);if(e&&!c.Bb()&&c.bubbles!==l)u.j(e,c);else if(!e&&!c.zb()&&(d=u.getData(c.target),c.target[c.type])){d.disabled=f;if("function"===typeof c.target[c.type])c.target[c.type]();d.disabled=l}return!c.zb()};u.T=function(a,c,d){function e(){u.o(a,c,e);d.apply(this,arguments)}e.s=d.s=d.s||u.s++;u.d(a,c,e)};var v=Object.prototype.hasOwnProperty;
u.e=function(a,c){var d,e;d=document.createElement(a||"div");for(e in c)v.call(c,e)&&(-1!==e.indexOf("aria-")||"role"==e?d.setAttribute(e,c[e]):d[e]=c[e]);return d};u.Z=function(a){return a.charAt(0).toUpperCase()+a.slice(1)};u.k={};u.k.create=Object.create||function(a){function c(){}c.prototype=a;return new c};u.k.ra=function(a,c,d){for(var e in a)v.call(a,e)&&c.call(d||this,e,a[e])};u.k.B=function(a,c){if(!c)return a;for(var d in c)v.call(c,d)&&(a[d]=c[d]);return a};
u.k.Wc=function(a,c){var d,e,g;a=u.k.copy(a);for(d in c)v.call(c,d)&&(e=a[d],g=c[d],a[d]=u.k.Ma(e)&&u.k.Ma(g)?u.k.Wc(e,g):c[d]);return a};u.k.copy=function(a){return u.k.B({},a)};u.k.Ma=function(a){return!!a&&"object"===typeof a&&"[object Object]"===a.toString()&&a.constructor===Object};u.bind=function(a,c,d){function e(){return c.apply(a,arguments)}c.s||(c.s=u.s++);e.s=d?d+"_"+c.s:c.s;return e};u.pa={};u.s=1;u.expando="vdata"+(new Date).getTime();
u.getData=function(a){var c=a[u.expando];c||(c=a[u.expando]=u.s++,u.pa[c]={});return u.pa[c]};u.mc=function(a){a=a[u.expando];return!(!a||u.Ab(u.pa[a]))};u.tc=function(a){var c=a[u.expando];if(c){delete u.pa[c];try{delete a[u.expando]}catch(d){a.removeAttribute?a.removeAttribute(u.expando):a[u.expando]=h}}};u.Ab=function(a){for(var c in a)if(a[c]!==h)return l;return f};u.n=function(a,c){-1==(" "+a.className+" ").indexOf(" "+c+" ")&&(a.className=""===a.className?c:a.className+" "+c)};
u.t=function(a,c){var d,e;if(-1!=a.className.indexOf(c)){d=a.className.split(" ");for(e=d.length-1;0<=e;e--)d[e]===c&&d.splice(e,1);a.className=d.join(" ")}};u.W=u.e("video");u.I=navigator.userAgent;u.Kc=/iPhone/i.test(u.I);u.Jc=/iPad/i.test(u.I);u.Lc=/iPod/i.test(u.I);u.Ic=u.Kc||u.Jc||u.Lc;var aa=u,w;var x=u.I.match(/OS (\d+)_/i);w=x&&x[1]?x[1]:b;aa.Id=w;u.Hc=/Android/i.test(u.I);var ba=u,y;var z=u.I.match(/Android (\d+)(?:\.(\d+))?(?:\.(\d+))*/i),A,B;
z?(A=z[1]&&parseFloat(z[1]),B=z[2]&&parseFloat(z[2]),y=A&&B?parseFloat(z[1]+"."+z[2]):A?A:h):y=h;ba.Fc=y;u.Mc=u.Hc&&/webkit/i.test(u.I)&&2.3>u.Fc;u.Ub=/Firefox/i.test(u.I);u.Jd=/Chrome/i.test(u.I);u.$b=!!("ontouchstart"in window||window.Gc&&document instanceof window.Gc);
u.wb=function(a){var c,d,e,g;c={};if(a&&a.attributes&&0<a.attributes.length){d=a.attributes;for(var j=d.length-1;0<=j;j--){e=d[j].name;g=d[j].value;if("boolean"===typeof a[e]||-1!==",autoplay,controls,loop,muted,default,".indexOf(","+e+","))g=g!==h?f:l;c[e]=g}}return c};
u.Nd=function(a,c){var d="";document.defaultView&&document.defaultView.getComputedStyle?d=document.defaultView.getComputedStyle(a,"").getPropertyValue(c):a.currentStyle&&(d=a["client"+c.substr(0,1).toUpperCase()+c.substr(1)]+"px");return d};u.yb=function(a,c){c.firstChild?c.insertBefore(a,c.firstChild):c.appendChild(a)};u.Ob={};u.u=function(a){0===a.indexOf("#")&&(a=a.slice(1));return document.getElementById(a)};
u.ta=function(a,c){c=c||a;var d=Math.floor(a%60),e=Math.floor(a/60%60),g=Math.floor(a/3600),j=Math.floor(c/60%60),k=Math.floor(c/3600);if(isNaN(a)||Infinity===a)g=e=d="-";g=0<g||0<k?g+":":"";return g+(((g||10<=j)&&10>e?"0"+e:e)+":")+(10>d?"0"+d:d)};u.Sc=function(){document.body.focus();document.onselectstart=r(l)};u.Dd=function(){document.onselectstart=r(f)};u.trim=function(a){return(a+"").replace(/^\s+|\s+$/g,"")};u.round=function(a,c){c||(c=0);return Math.round(a*Math.pow(10,c))/Math.pow(10,c)};
u.sb=function(a,c){return{length:1,start:function(){return a},end:function(){return c}}};
u.get=function(a,c,d){var e,g;"undefined"===typeof XMLHttpRequest&&(window.XMLHttpRequest=function(){try{return new window.ActiveXObject("Msxml2.XMLHTTP.6.0")}catch(a){}try{return new window.ActiveXObject("Msxml2.XMLHTTP.3.0")}catch(c){}try{return new window.ActiveXObject("Msxml2.XMLHTTP")}catch(d){}throw Error("This browser does not support XMLHttpRequest.");});g=new XMLHttpRequest;try{g.open("GET",a)}catch(j){d(j)}e=0===a.indexOf("file:")||0===window.location.href.indexOf("file:")&&-1===a.indexOf("http");
g.onreadystatechange=function(){4===g.readyState&&(200===g.status||e&&0===g.status?c(g.responseText):d&&d())};try{g.send()}catch(k){d&&d(k)}};u.vd=function(a){try{var c=window.localStorage||l;c&&(c.volume=a)}catch(d){22==d.code||1014==d.code?u.log("LocalStorage Full (VideoJS)",d):18==d.code?u.log("LocalStorage not allowed (VideoJS)",d):u.log("LocalStorage Error (VideoJS)",d)}};u.kc=function(a){a.match(/^https?:\/\//)||(a=u.e("div",{innerHTML:'<a href="'+a+'">x</a>'}).firstChild.href);return a};
u.log=function(){u.log.history=u.log.history||[];u.log.history.push(arguments);window.console&&window.console.log(Array.prototype.slice.call(arguments))};u.cd=function(a){var c,d;a.getBoundingClientRect&&a.parentNode&&(c=a.getBoundingClientRect());if(!c)return{left:0,top:0};a=document.documentElement;d=document.body;return{left:c.left+(window.pageXOffset||d.scrollLeft)-(a.clientLeft||d.clientLeft||0),top:c.top+(window.pageYOffset||d.scrollTop)-(a.clientTop||d.clientTop||0)}};u.ja={};
u.ja.Fb=function(a,c){var d,e,g;a=u.k.copy(a);for(d in c)c.hasOwnProperty(d)&&(e=a[d],g=c[d],a[d]=u.k.Ma(e)&&u.k.Ma(g)?u.ja.Fb(e,g):c[d]);return a};
u.b=u.ka.extend({h:function(a,c,d){this.c=a;this.g=u.k.copy(this.g);c=this.options(c);this.Q=c.id||(c.el&&c.el.id?c.el.id:a.id()+"_component_"+u.s++);this.kd=c.name||h;this.a=c.el||this.e();this.J=[];this.Ia={};this.Ja={};this.nc();this.H(d);if(c.uc!==l){var e,g;e=u.bind(this.C(),this.C().reportUserActivity);this.d("touchstart",function(){e();clearInterval(g);g=setInterval(e,250)});a=function(){e();clearInterval(g)};this.d("touchmove",e);this.d("touchend",a);this.d("touchcancel",a)}}});t=u.b.prototype;
t.dispose=function(){this.j({type:"dispose",bubbles:l});if(this.J)for(var a=this.J.length-1;0<=a;a--)this.J[a].dispose&&this.J[a].dispose();this.Ja=this.Ia=this.J=h;this.o();this.a.parentNode&&this.a.parentNode.removeChild(this.a);u.tc(this.a);this.a=h};t.c=f;t.C=p("c");t.options=function(a){return a===b?this.g:this.g=u.ja.Fb(this.g,a)};t.e=function(a,c){return u.e(a,c)};t.u=p("a");t.Ka=function(){return this.F||this.a};t.id=p("Q");t.name=p("kd");t.children=p("J");t.ed=function(a){return this.Ia[a]};
t.fa=function(a){return this.Ja[a]};t.Y=function(a,c){var d,e;"string"===typeof a?(e=a,c=c||{},d=c.componentClass||u.Z(e),c.name=e,d=new window.videojs[d](this.c||this,c)):d=a;this.J.push(d);"function"===typeof d.id&&(this.Ia[d.id()]=d);(e=e||d.name&&d.name())&&(this.Ja[e]=d);"function"===typeof d.el&&d.el()&&this.Ka().appendChild(d.el());return d};
t.removeChild=function(a){"string"===typeof a&&(a=this.fa(a));if(a&&this.J){for(var c=l,d=this.J.length-1;0<=d;d--)if(this.J[d]===a){c=f;this.J.splice(d,1);break}c&&(this.Ia[a.id]=h,this.Ja[a.name]=h,(c=a.u())&&c.parentNode===this.Ka()&&this.Ka().removeChild(a.u()))}};t.nc=function(){var a=this.g;if(a&&a.children){var c=this;u.k.ra(a.children,function(a,e){e!==l&&!e.loadEvent&&(c[a]=c.Y(a,e))})}};t.P=r("");t.d=function(a,c){u.d(this.a,a,u.bind(this,c));return this};
t.o=function(a,c){u.o(this.a,a,c);return this};t.T=function(a,c){u.T(this.a,a,u.bind(this,c));return this};t.j=function(a,c){u.j(this.a,a,c);return this};t.H=function(a){a&&(this.aa?a.call(this):(this.Ta===b&&(this.Ta=[]),this.Ta.push(a)));return this};t.Wa=function(){this.aa=f;var a=this.Ta;if(a&&0<a.length){for(var c=0,d=a.length;c<d;c++)a[c].call(this);this.Ta=[];this.j("ready")}};t.n=function(a){u.n(this.a,a);return this};t.t=function(a){u.t(this.a,a);return this};
t.show=function(){this.a.style.display="block";return this};t.D=function(){this.a.style.display="none";return this};function D(a){a.t("vjs-lock-showing")}t.disable=function(){this.D();this.show=m()};t.width=function(a,c){return E(this,"width",a,c)};t.height=function(a,c){return E(this,"height",a,c)};t.Yc=function(a,c){return this.width(a,f).height(c)};
function E(a,c,d,e){if(d!==b)return a.a.style[c]=-1!==(""+d).indexOf("%")||-1!==(""+d).indexOf("px")?d:"auto"===d?"":d+"px",e||a.j("resize"),a;if(!a.a)return 0;d=a.a.style[c];e=d.indexOf("px");return-1!==e?parseInt(d.slice(0,e),10):parseInt(a.a["offset"+u.Z(c)],10)}
u.q=u.b.extend({h:function(a,c){u.b.call(this,a,c);var d=l;this.d("touchstart",function(a){a.preventDefault();d=f});this.d("touchmove",function(){d=l});var e=this;this.d("touchend",function(a){d&&e.p(a);a.preventDefault()});this.d("click",this.p);this.d("focus",this.Pa);this.d("blur",this.Oa)}});t=u.q.prototype;
t.e=function(a,c){c=u.k.B({className:this.P(),innerHTML:'<div class="vjs-control-content"><span class="vjs-control-text">'+(this.oa||"Need Text")+"</span></div>",role:"button","aria-live":"polite",tabIndex:0},c);return u.b.prototype.e.call(this,a,c)};t.P=function(){return"vjs-control "+u.b.prototype.P.call(this)};t.p=m();t.Pa=function(){u.d(document,"keyup",u.bind(this,this.ba))};t.ba=function(a){if(32==a.which||13==a.which)a.preventDefault(),this.p()};
t.Oa=function(){u.o(document,"keyup",u.bind(this,this.ba))};u.N=u.b.extend({h:function(a,c){u.b.call(this,a,c);this.Rc=this.fa(this.g.barName);this.handle=this.fa(this.g.handleName);a.d(this.rc,u.bind(this,this.update));this.d("mousedown",this.Qa);this.d("touchstart",this.Qa);this.d("focus",this.Pa);this.d("blur",this.Oa);this.d("click",this.p);this.c.d("controlsvisible",u.bind(this,this.update));a.H(u.bind(this,this.update));this.O={}}});t=u.N.prototype;
t.e=function(a,c){c=c||{};c.className+=" vjs-slider";c=u.k.B({role:"slider","aria-valuenow":0,"aria-valuemin":0,"aria-valuemax":100,tabIndex:0},c);return u.b.prototype.e.call(this,a,c)};t.Qa=function(a){a.preventDefault();u.Sc();this.O.move=u.bind(this,this.Hb);this.O.end=u.bind(this,this.Ib);u.d(document,"mousemove",this.O.move);u.d(document,"mouseup",this.O.end);u.d(document,"touchmove",this.O.move);u.d(document,"touchend",this.O.end);this.Hb(a)};
t.Ib=function(){u.Dd();u.o(document,"mousemove",this.O.move,l);u.o(document,"mouseup",this.O.end,l);u.o(document,"touchmove",this.O.move,l);u.o(document,"touchend",this.O.end,l);this.update()};t.update=function(){if(this.a){var a,c=this.xb(),d=this.handle,e=this.Rc;isNaN(c)&&(c=0);a=c;if(d){a=this.a.offsetWidth;var g=d.u().offsetWidth;a=g?g/a:0;c*=1-a;a=c+a/2;d.u().style.left=u.round(100*c,2)+"%"}e.u().style.width=u.round(100*a,2)+"%"}};
function F(a,c){var d,e,g,j;d=a.a;e=u.cd(d);j=g=d.offsetWidth;d=a.handle;if(a.g.Ed)return j=e.top,e=c.changedTouches?c.changedTouches[0].pageY:c.pageY,d&&(d=d.u().offsetHeight,j+=d/2,g-=d),Math.max(0,Math.min(1,(j-e+g)/g));g=e.left;e=c.changedTouches?c.changedTouches[0].pageX:c.pageX;d&&(d=d.u().offsetWidth,g+=d/2,j-=d);return Math.max(0,Math.min(1,(e-g)/j))}t.Pa=function(){u.d(document,"keyup",u.bind(this,this.ba))};
t.ba=function(a){37==a.which?(a.preventDefault(),this.xc()):39==a.which&&(a.preventDefault(),this.yc())};t.Oa=function(){u.o(document,"keyup",u.bind(this,this.ba))};t.p=function(a){a.stopImmediatePropagation();a.preventDefault()};u.V=u.b.extend();u.V.prototype.defaultValue=0;u.V.prototype.e=function(a,c){c=c||{};c.className+=" vjs-slider-handle";c=u.k.B({innerHTML:'<span class="vjs-control-text">'+this.defaultValue+"</span>"},c);return u.b.prototype.e.call(this,"div",c)};u.la=u.b.extend();
function ca(a,c){a.Y(c);c.d("click",u.bind(a,function(){D(this)}))}u.la.prototype.e=function(){var a=this.options().Uc||"ul";this.F=u.e(a,{className:"vjs-menu-content"});a=u.b.prototype.e.call(this,"div",{append:this.F,className:"vjs-menu"});a.appendChild(this.F);u.d(a,"click",function(a){a.preventDefault();a.stopImmediatePropagation()});return a};u.M=u.q.extend({h:function(a,c){u.q.call(this,a,c);this.selected(c.selected)}});
u.M.prototype.e=function(a,c){return u.q.prototype.e.call(this,"li",u.k.B({className:"vjs-menu-item",innerHTML:this.g.label},c))};u.M.prototype.p=function(){this.selected(f)};u.M.prototype.selected=function(a){a?(this.n("vjs-selected"),this.a.setAttribute("aria-selected",f)):(this.t("vjs-selected"),this.a.setAttribute("aria-selected",l))};
u.R=u.q.extend({h:function(a,c){u.q.call(this,a,c);this.ua=this.La();this.Y(this.ua);this.K&&0===this.K.length&&this.D();this.d("keyup",this.ba);this.a.setAttribute("aria-haspopup",f);this.a.setAttribute("role","button")}});t=u.R.prototype;t.na=l;t.La=function(){var a=new u.la(this.c);this.options().title&&a.u().appendChild(u.e("li",{className:"vjs-menu-title",innerHTML:u.Z(this.A),Bd:-1}));if(this.K=this.createItems())for(var c=0;c<this.K.length;c++)ca(a,this.K[c]);return a};t.qa=m();
t.P=function(){return this.className+" vjs-menu-button "+u.q.prototype.P.call(this)};t.Pa=m();t.Oa=m();t.p=function(){this.T("mouseout",u.bind(this,function(){D(this.ua);this.a.blur()}));this.na?G(this):H(this)};t.ba=function(a){a.preventDefault();32==a.which||13==a.which?this.na?G(this):H(this):27==a.which&&this.na&&G(this)};function H(a){a.na=f;a.ua.n("vjs-lock-showing");a.a.setAttribute("aria-pressed",f);a.K&&0<a.K.length&&a.K[0].u().focus()}
function G(a){a.na=l;D(a.ua);a.a.setAttribute("aria-pressed",l)}
u.Player=u.b.extend({h:function(a,c,d){this.L=a;a.id=a.id||"vjs_video_"+u.s++;c=u.k.B(da(a),c);this.v={};this.sc=c.poster;this.rb=c.controls;a.controls=l;c.uc=l;u.b.call(this,this,c,d);this.controls()?this.n("vjs-controls-enabled"):this.n("vjs-controls-disabled");this.T("play",function(a){u.j(this.a,{type:"firstplay",target:this.a})||(a.preventDefault(),a.stopPropagation(),a.stopImmediatePropagation())});this.d("ended",this.ld);this.d("play",this.Kb);this.d("firstplay",this.md);this.d("pause",this.Jb);
this.d("progress",this.od);this.d("durationchange",this.qc);this.d("error",this.Gb);this.d("fullscreenchange",this.nd);u.va[this.Q]=this;c.plugins&&u.k.ra(c.plugins,function(a,c){this[a](c)},this);var e,g,j,k;e=u.bind(this,this.reportUserActivity);this.d("mousedown",function(){e();clearInterval(g);g=setInterval(e,250)});this.d("mousemove",e);this.d("mouseup",function(){e();clearInterval(g)});this.d("keydown",e);this.d("keyup",e);j=setInterval(u.bind(this,function(){this.ia&&(this.ia=l,this.userActive(f),
clearTimeout(k),k=setTimeout(u.bind(this,function(){this.ia||this.userActive(l)}),2E3))}),250);this.d("dispose",function(){clearInterval(j);clearTimeout(k)})}});t=u.Player.prototype;t.g=u.options;t.dispose=function(){this.j("dispose");this.o("dispose");u.va[this.Q]=h;this.L&&this.L.player&&(this.L.player=h);this.a&&this.a.player&&(this.a.player=h);clearInterval(this.Sa);this.wa();this.i&&this.i.dispose();u.b.prototype.dispose.call(this)};
function da(a){var c={sources:[],tracks:[]};u.k.B(c,u.wb(a));if(a.hasChildNodes()){var d,e,g,j;a=a.childNodes;g=0;for(j=a.length;g<j;g++)d=a[g],e=d.nodeName.toLowerCase(),"source"===e?c.sources.push(u.wb(d)):"track"===e&&c.tracks.push(u.wb(d))}return c}
t.e=function(){var a=this.a=u.b.prototype.e.call(this,"div"),c=this.L;c.removeAttribute("width");c.removeAttribute("height");if(c.hasChildNodes()){var d,e,g,j,k;d=c.childNodes;e=d.length;for(k=[];e--;)g=d[e],j=g.nodeName.toLowerCase(),"track"===j&&k.push(g);for(d=0;d<k.length;d++)c.removeChild(k[d])}a.id=c.id;a.className=c.className;c.id+="_html5_api";c.className="vjs-tech";c.player=a.player=this;this.n("vjs-paused");this.width(this.g.width,f);this.height(this.g.height,f);c.parentNode&&c.parentNode.insertBefore(a,
c);u.yb(c,a);return a};
function I(a,c,d){a.i&&(a.aa=l,a.i.dispose(),a.Db&&(a.Db=l,clearInterval(a.Sa)),a.Eb&&J(a),a.i=l);"Html5"!==c&&a.L&&(u.l.gc(a.L),a.L=h);a.xa=c;a.aa=l;var e=u.k.B({source:d,parentEl:a.a},a.g[c.toLowerCase()]);d&&(d.src==a.v.src&&0<a.v.currentTime&&(e.startTime=a.v.currentTime),a.v.src=d.src);a.i=new window.videojs[c](a,e);a.i.H(function(){this.c.Wa();if(!this.m.progressEvents){var a=this.c;a.Db=f;a.Sa=setInterval(u.bind(a,function(){this.v.mb<this.buffered().end(0)?this.j("progress"):1==this.bufferedPercent()&&
(clearInterval(this.Sa),this.j("progress"))}),500);a.i.T("progress",function(){this.m.progressEvents=f;var a=this.c;a.Db=l;clearInterval(a.Sa)})}this.m.timeupdateEvents||(a=this.c,a.Eb=f,a.d("play",a.Bc),a.d("pause",a.wa),a.i.T("timeupdate",function(){this.m.timeupdateEvents=f;J(this.c)}))})}function J(a){a.Eb=l;a.wa();a.o("play",a.Bc);a.o("pause",a.wa)}t.Bc=function(){this.fc&&this.wa();this.fc=setInterval(u.bind(this,function(){this.j("timeupdate")}),250)};t.wa=function(){clearInterval(this.fc)};
t.Kb=function(){u.t(this.a,"vjs-paused");u.n(this.a,"vjs-playing")};t.md=function(){this.g.starttime&&this.currentTime(this.g.starttime);this.n("vjs-has-started")};t.Jb=function(){u.t(this.a,"vjs-playing");u.n(this.a,"vjs-paused")};t.od=function(){1==this.bufferedPercent()&&this.j("loadedalldata")};t.ld=function(){this.g.loop&&(this.currentTime(0),this.play())};t.qc=function(){var a=K(this,"duration");a&&this.duration(a)};t.nd=function(){this.isFullScreen()?this.n("vjs-fullscreen"):this.t("vjs-fullscreen")};
t.Gb=function(a){u.log("Video Error",a)};function L(a,c,d){if(a.i&&!a.i.aa)a.i.H(function(){this[c](d)});else try{a.i[c](d)}catch(e){throw u.log(e),e;}}function K(a,c){if(a.i&&a.i.aa)try{return a.i[c]()}catch(d){throw a.i[c]===b?u.log("Video.js: "+c+" method not defined for "+a.xa+" playback technology.",d):"TypeError"==d.name?(u.log("Video.js: "+c+" unavailable on "+a.xa+" playback technology element.",d),a.i.aa=l):u.log(d),d;}}t.play=function(){L(this,"play");return this};
t.pause=function(){L(this,"pause");return this};t.paused=function(){return K(this,"paused")===l?l:f};t.currentTime=function(a){return a!==b?(L(this,"setCurrentTime",a),this.Eb&&this.j("timeupdate"),this):this.v.currentTime=K(this,"currentTime")||0};t.duration=function(a){if(a!==b)return this.v.duration=parseFloat(a),this;this.v.duration===b&&this.qc();return this.v.duration||0};
t.buffered=function(){var a=K(this,"buffered"),c=a.length-1,d=this.v.mb=this.v.mb||0;a&&(0<=c&&a.end(c)!==d)&&(d=a.end(c),this.v.mb=d);return u.sb(0,d)};t.bufferedPercent=function(){return this.duration()?this.buffered().end(0)/this.duration():0};t.volume=function(a){if(a!==b)return a=Math.max(0,Math.min(1,parseFloat(a))),this.v.volume=a,L(this,"setVolume",a),u.vd(a),this;a=parseFloat(K(this,"volume"));return isNaN(a)?1:a};
t.muted=function(a){return a!==b?(L(this,"setMuted",a),this):K(this,"muted")||l};t.Va=function(){return K(this,"supportsFullScreen")||l};t.oc=l;t.isFullScreen=function(a){return a!==b?(this.oc=a,this):this.oc};
t.requestFullScreen=function(){var a=u.Ob.requestFullScreen;this.isFullScreen(f);a?(u.d(document,a.ub,u.bind(this,function(c){this.isFullScreen(document[a.isFullScreen]);this.isFullScreen()===l&&u.o(document,a.ub,arguments.callee);this.j("fullscreenchange")})),this.a[a.vc]()):this.i.Va()?L(this,"enterFullScreen"):(this.fd=f,this.Zc=document.documentElement.style.overflow,u.d(document,"keydown",u.bind(this,this.jc)),document.documentElement.style.overflow="hidden",u.n(document.body,"vjs-full-window"),
this.j("enterFullWindow"),this.j("fullscreenchange"));return this};t.cancelFullScreen=function(){var a=u.Ob.requestFullScreen;this.isFullScreen(l);if(a)document[a.ob]();else this.i.Va()?L(this,"exitFullScreen"):(M(this),this.j("fullscreenchange"));return this};t.jc=function(a){27===a.keyCode&&(this.isFullScreen()===f?this.cancelFullScreen():M(this))};
function M(a){a.fd=l;u.o(document,"keydown",a.jc);document.documentElement.style.overflow=a.Zc;u.t(document.body,"vjs-full-window");a.j("exitFullWindow")}
t.src=function(a){if(a instanceof Array){var c;a:{c=a;for(var d=0,e=this.g.techOrder;d<e.length;d++){var g=u.Z(e[d]),j=window.videojs[g];if(j.isSupported())for(var k=0,q=c;k<q.length;k++){var n=q[k];if(j.canPlaySource(n)){c={source:n,i:g};break a}}}c=l}c?(a=c.source,c=c.i,c==this.xa?this.src(a):I(this,c,a)):this.a.appendChild(u.e("p",{innerHTML:this.options().notSupportedMessage}))}else a instanceof Object?window.videojs[this.xa].canPlaySource(a)?this.src(a.src):this.src([a]):(this.v.src=a,this.aa?
(L(this,"src",a),"auto"==this.g.preload&&this.load(),this.g.autoplay&&this.play()):this.H(function(){this.src(a)}));return this};t.load=function(){L(this,"load");return this};t.currentSrc=function(){return K(this,"currentSrc")||this.v.src||""};t.Ra=function(a){return a!==b?(L(this,"setPreload",a),this.g.preload=a,this):K(this,"preload")};t.autoplay=function(a){return a!==b?(L(this,"setAutoplay",a),this.g.autoplay=a,this):K(this,"autoplay")};
t.loop=function(a){return a!==b?(L(this,"setLoop",a),this.g.loop=a,this):K(this,"loop")};t.poster=function(a){if(a===b)return this.sc;this.sc=a;L(this,"setPoster",a);this.j("posterchange")};t.controls=function(a){return a!==b?(a=!!a,this.rb!==a&&((this.rb=a)?(this.t("vjs-controls-disabled"),this.n("vjs-controls-enabled"),this.j("controlsenabled")):(this.t("vjs-controls-enabled"),this.n("vjs-controls-disabled"),this.j("controlsdisabled"))),this):this.rb};u.Player.prototype.Qb;t=u.Player.prototype;
t.usingNativeControls=function(a){return a!==b?(a=!!a,this.Qb!==a&&((this.Qb=a)?(this.n("vjs-using-native-controls"),this.j("usingnativecontrols")):(this.t("vjs-using-native-controls"),this.j("usingcustomcontrols"))),this):this.Qb};t.error=function(){return K(this,"error")};t.ended=function(){return K(this,"ended")};t.seeking=function(){return K(this,"seeking")};t.ia=f;t.reportUserActivity=function(){this.ia=f};t.Pb=f;
t.userActive=function(a){return a!==b?(a=!!a,a!==this.Pb&&((this.Pb=a)?(this.ia=f,this.t("vjs-user-inactive"),this.n("vjs-user-active"),this.j("useractive")):(this.ia=l,this.i.T("mousemove",function(a){a.stopPropagation();a.preventDefault()}),this.t("vjs-user-active"),this.n("vjs-user-inactive"),this.j("userinactive"))),this):this.Pb};var N,O,P;P=document.createElement("div");O={};
P.Kd!==b?(O.vc="requestFullscreen",O.ob="exitFullscreen",O.ub="fullscreenchange",O.isFullScreen="fullScreen"):(document.mozCancelFullScreen?(N="moz",O.isFullScreen=N+"FullScreen"):(N="webkit",O.isFullScreen=N+"IsFullScreen"),P[N+"RequestFullScreen"]&&(O.vc=N+"RequestFullScreen",O.ob=N+"CancelFullScreen"),O.ub=N+"fullscreenchange");document[O.ob]&&(u.Ob.requestFullScreen=O);u.Ca=u.b.extend();
u.Ca.prototype.g={Pd:"play",children:{playToggle:{},currentTimeDisplay:{},timeDivider:{},durationDisplay:{},remainingTimeDisplay:{},progressControl:{},fullscreenToggle:{},volumeControl:{},muteToggle:{}}};u.Ca.prototype.e=function(){return u.e("div",{className:"vjs-control-bar"})};u.Xb=u.q.extend({h:function(a,c){u.q.call(this,a,c);a.d("play",u.bind(this,this.Kb));a.d("pause",u.bind(this,this.Jb))}});t=u.Xb.prototype;t.oa="Play";t.P=function(){return"vjs-play-control "+u.q.prototype.P.call(this)};
t.p=function(){this.c.paused()?this.c.play():this.c.pause()};t.Kb=function(){u.t(this.a,"vjs-paused");u.n(this.a,"vjs-playing");this.a.children[0].children[0].innerHTML="Pause"};t.Jb=function(){u.t(this.a,"vjs-playing");u.n(this.a,"vjs-paused");this.a.children[0].children[0].innerHTML="Play"};u.$a=u.b.extend({h:function(a,c){u.b.call(this,a,c);a.d("timeupdate",u.bind(this,this.da))}});
u.$a.prototype.e=function(){var a=u.b.prototype.e.call(this,"div",{className:"vjs-current-time vjs-time-controls vjs-control"});this.F=u.e("div",{className:"vjs-current-time-display",innerHTML:'<span class="vjs-control-text">Current Time </span>0:00',"aria-live":"off"});a.appendChild(this.F);return a};u.$a.prototype.da=function(){var a=this.c.Ua?this.c.v.currentTime:this.c.currentTime();this.F.innerHTML='<span class="vjs-control-text">Current Time </span>'+u.ta(a,this.c.duration())};
u.ab=u.b.extend({h:function(a,c){u.b.call(this,a,c);a.d("timeupdate",u.bind(this,this.da))}});u.ab.prototype.e=function(){var a=u.b.prototype.e.call(this,"div",{className:"vjs-duration vjs-time-controls vjs-control"});this.F=u.e("div",{className:"vjs-duration-display",innerHTML:'<span class="vjs-control-text">Duration Time </span>0:00',"aria-live":"off"});a.appendChild(this.F);return a};
u.ab.prototype.da=function(){var a=this.c.duration();a&&(this.F.innerHTML='<span class="vjs-control-text">Duration Time </span>'+u.ta(a))};u.bc=u.b.extend({h:function(a,c){u.b.call(this,a,c)}});u.bc.prototype.e=function(){return u.b.prototype.e.call(this,"div",{className:"vjs-time-divider",innerHTML:"<div><span>/</span></div>"})};u.gb=u.b.extend({h:function(a,c){u.b.call(this,a,c);a.d("timeupdate",u.bind(this,this.da))}});
u.gb.prototype.e=function(){var a=u.b.prototype.e.call(this,"div",{className:"vjs-remaining-time vjs-time-controls vjs-control"});this.F=u.e("div",{className:"vjs-remaining-time-display",innerHTML:'<span class="vjs-control-text">Remaining Time </span>-0:00',"aria-live":"off"});a.appendChild(this.F);return a};u.gb.prototype.da=function(){this.c.duration()&&(this.F.innerHTML='<span class="vjs-control-text">Remaining Time </span>-'+u.ta(this.c.duration()-this.c.currentTime()))};
u.Da=u.q.extend({h:function(a,c){u.q.call(this,a,c)}});u.Da.prototype.oa="Fullscreen";u.Da.prototype.P=function(){return"vjs-fullscreen-control "+u.q.prototype.P.call(this)};u.Da.prototype.p=function(){this.c.isFullScreen()?(this.c.cancelFullScreen(),this.a.children[0].children[0].innerHTML="Fullscreen"):(this.c.requestFullScreen(),this.a.children[0].children[0].innerHTML="Non-Fullscreen")};u.fb=u.b.extend({h:function(a,c){u.b.call(this,a,c)}});u.fb.prototype.g={children:{seekBar:{}}};
u.fb.prototype.e=function(){return u.b.prototype.e.call(this,"div",{className:"vjs-progress-control vjs-control"})};u.Yb=u.N.extend({h:function(a,c){u.N.call(this,a,c);a.d("timeupdate",u.bind(this,this.za));a.H(u.bind(this,this.za))}});t=u.Yb.prototype;t.g={children:{loadProgressBar:{},playProgressBar:{},seekHandle:{}},barName:"playProgressBar",handleName:"seekHandle"};t.rc="timeupdate";t.e=function(){return u.N.prototype.e.call(this,"div",{className:"vjs-progress-holder","aria-label":"video progress bar"})};
t.za=function(){var a=this.c.Ua?this.c.v.currentTime:this.c.currentTime();this.a.setAttribute("aria-valuenow",u.round(100*this.xb(),2));this.a.setAttribute("aria-valuetext",u.ta(a,this.c.duration()))};t.xb=function(){return this.c.currentTime()/this.c.duration()};t.Qa=function(a){u.N.prototype.Qa.call(this,a);this.c.Ua=f;this.Fd=!this.c.paused();this.c.pause()};t.Hb=function(a){a=F(this,a)*this.c.duration();a==this.c.duration()&&(a-=0.1);this.c.currentTime(a)};
t.Ib=function(a){u.N.prototype.Ib.call(this,a);this.c.Ua=l;this.Fd&&this.c.play()};t.yc=function(){this.c.currentTime(this.c.currentTime()+5)};t.xc=function(){this.c.currentTime(this.c.currentTime()-5)};u.cb=u.b.extend({h:function(a,c){u.b.call(this,a,c);a.d("progress",u.bind(this,this.update))}});u.cb.prototype.e=function(){return u.b.prototype.e.call(this,"div",{className:"vjs-load-progress",innerHTML:'<span class="vjs-control-text">Loaded: 0%</span>'})};
u.cb.prototype.update=function(){this.a.style&&(this.a.style.width=u.round(100*this.c.bufferedPercent(),2)+"%")};u.Wb=u.b.extend({h:function(a,c){u.b.call(this,a,c)}});u.Wb.prototype.e=function(){return u.b.prototype.e.call(this,"div",{className:"vjs-play-progress",innerHTML:'<span class="vjs-control-text">Progress: 0%</span>'})};u.Fa=u.V.extend({h:function(a,c){u.V.call(this,a,c);a.d("timeupdate",u.bind(this,this.da))}});u.Fa.prototype.defaultValue="00:00";
u.Fa.prototype.e=function(){return u.V.prototype.e.call(this,"div",{className:"vjs-seek-handle","aria-live":"off"})};u.Fa.prototype.da=function(){var a=this.c.Ua?this.c.v.currentTime:this.c.currentTime();this.a.innerHTML='<span class="vjs-control-text">'+u.ta(a,this.c.duration())+"</span>"};u.ib=u.b.extend({h:function(a,c){u.b.call(this,a,c);a.i&&(a.i.m&&a.i.m.volumeControl===l)&&this.n("vjs-hidden");a.d("loadstart",u.bind(this,function(){a.i.m&&a.i.m.volumeControl===l?this.n("vjs-hidden"):this.t("vjs-hidden")}))}});
u.ib.prototype.g={children:{volumeBar:{}}};u.ib.prototype.e=function(){return u.b.prototype.e.call(this,"div",{className:"vjs-volume-control vjs-control"})};u.hb=u.N.extend({h:function(a,c){u.N.call(this,a,c);a.d("volumechange",u.bind(this,this.za));a.H(u.bind(this,this.za));setTimeout(u.bind(this,this.update),0)}});t=u.hb.prototype;t.za=function(){this.a.setAttribute("aria-valuenow",u.round(100*this.c.volume(),2));this.a.setAttribute("aria-valuetext",u.round(100*this.c.volume(),2)+"%")};
t.g={children:{volumeLevel:{},volumeHandle:{}},barName:"volumeLevel",handleName:"volumeHandle"};t.rc="volumechange";t.e=function(){return u.N.prototype.e.call(this,"div",{className:"vjs-volume-bar","aria-label":"volume level"})};t.Hb=function(a){this.c.muted()&&this.c.muted(l);this.c.volume(F(this,a))};t.xb=function(){return this.c.muted()?0:this.c.volume()};t.yc=function(){this.c.volume(this.c.volume()+0.1)};t.xc=function(){this.c.volume(this.c.volume()-0.1)};
u.cc=u.b.extend({h:function(a,c){u.b.call(this,a,c)}});u.cc.prototype.e=function(){return u.b.prototype.e.call(this,"div",{className:"vjs-volume-level",innerHTML:'<span class="vjs-control-text"></span>'})};u.jb=u.V.extend();u.jb.prototype.defaultValue="00:00";u.jb.prototype.e=function(){return u.V.prototype.e.call(this,"div",{className:"vjs-volume-handle"})};
u.ea=u.q.extend({h:function(a,c){u.q.call(this,a,c);a.d("volumechange",u.bind(this,this.update));a.i&&(a.i.m&&a.i.m.volumeControl===l)&&this.n("vjs-hidden");a.d("loadstart",u.bind(this,function(){a.i.m&&a.i.m.volumeControl===l?this.n("vjs-hidden"):this.t("vjs-hidden")}))}});u.ea.prototype.e=function(){return u.q.prototype.e.call(this,"div",{className:"vjs-mute-control vjs-control",innerHTML:'<div><span class="vjs-control-text">Mute</span></div>'})};
u.ea.prototype.p=function(){this.c.muted(this.c.muted()?l:f)};u.ea.prototype.update=function(){var a=this.c.volume(),c=3;0===a||this.c.muted()?c=0:0.33>a?c=1:0.67>a&&(c=2);this.c.muted()?"Unmute"!=this.a.children[0].children[0].innerHTML&&(this.a.children[0].children[0].innerHTML="Unmute"):"Mute"!=this.a.children[0].children[0].innerHTML&&(this.a.children[0].children[0].innerHTML="Mute");for(a=0;4>a;a++)u.t(this.a,"vjs-vol-"+a);u.n(this.a,"vjs-vol-"+c)};
u.ma=u.R.extend({h:function(a,c){u.R.call(this,a,c);a.d("volumechange",u.bind(this,this.update));a.i&&(a.i.m&&a.i.m.Cc===l)&&this.n("vjs-hidden");a.d("loadstart",u.bind(this,function(){a.i.m&&a.i.m.Cc===l?this.n("vjs-hidden"):this.t("vjs-hidden")}));this.n("vjs-menu-button")}});u.ma.prototype.La=function(){var a=new u.la(this.c,{Uc:"div"}),c=new u.hb(this.c,u.k.B({Ed:f},this.g.Yd));a.Y(c);return a};u.ma.prototype.p=function(){u.ea.prototype.p.call(this);u.R.prototype.p.call(this)};
u.ma.prototype.e=function(){return u.q.prototype.e.call(this,"div",{className:"vjs-volume-menu-button vjs-menu-button vjs-control",innerHTML:'<div><span class="vjs-control-text">Mute</span></div>'})};u.ma.prototype.update=u.ea.prototype.update;u.Ea=u.q.extend({h:function(a,c){u.q.call(this,a,c);a.poster()&&this.src(a.poster());(!a.poster()||!a.controls())&&this.D();a.d("posterchange",u.bind(this,function(){this.src(a.poster())}));a.d("play",u.bind(this,this.D))}});var Q="backgroundSize"in u.W.style;
u.Ea.prototype.e=function(){var a=u.e("div",{className:"vjs-poster",tabIndex:-1});Q||a.appendChild(u.e("img"));return a};u.Ea.prototype.src=function(a){var c=this.u();a!==b&&(Q?c.style.backgroundImage='url("'+a+'")':c.firstChild.src=a)};u.Ea.prototype.p=function(){this.C().controls()&&this.c.play()};
u.Vb=u.b.extend({h:function(a,c){u.b.call(this,a,c);a.d("canplay",u.bind(this,this.D));a.d("canplaythrough",u.bind(this,this.D));a.d("playing",u.bind(this,this.D));a.d("seeked",u.bind(this,this.D));a.d("seeking",u.bind(this,this.show));a.d("seeked",u.bind(this,this.D));a.d("error",u.bind(this,this.show));a.d("waiting",u.bind(this,this.show))}});u.Vb.prototype.e=function(){return u.b.prototype.e.call(this,"div",{className:"vjs-loading-spinner"})};u.Ya=u.q.extend();
u.Ya.prototype.e=function(){return u.q.prototype.e.call(this,"div",{className:"vjs-big-play-button",innerHTML:'<span aria-hidden="true"></span>',"aria-label":"play video"})};u.Ya.prototype.p=function(){this.c.play()};
u.r=u.b.extend({h:function(a,c,d){c=c||{};c.uc=l;u.b.call(this,a,c,d);var e,g;g=this;e=this.C();a=function(){if(e.controls()&&!e.usingNativeControls()){var a;g.d("mousedown",g.p);g.d("touchstart",function(c){c.preventDefault();a=this.c.userActive()});g.d("touchmove",function(){a&&this.C().reportUserActivity()});var c,d,n,s;c=0;g.d("touchstart",function(){c=(new Date).getTime();n=f});s=function(){n=l};g.d("touchmove",s);g.d("touchleave",s);g.d("touchcancel",s);g.d("touchend",function(){n===f&&(d=(new Date).getTime()-
c,250>d&&this.j("tap"))});g.d("tap",g.pd)}};c=u.bind(g,g.sd);this.H(a);e.d("controlsenabled",a);e.d("controlsdisabled",c)}});t=u.r.prototype;t.sd=function(){this.o("tap");this.o("touchstart");this.o("touchmove");this.o("touchleave");this.o("touchcancel");this.o("touchend");this.o("click");this.o("mousedown")};t.p=function(a){0===a.button&&this.C().controls()&&(this.C().paused()?this.C().play():this.C().pause())};t.pd=function(){this.C().userActive(!this.C().userActive())};t.Mb=m();
t.m={volumeControl:f,fullscreenResize:l,progressEvents:l,timeupdateEvents:l};u.media={};u.media.Xa="play pause paused currentTime setCurrentTime duration buffered volume setVolume muted setMuted width height supportsFullScreen enterFullScreen src load currentSrc preload setPreload autoplay setAutoplay loop setLoop error networkState readyState seeking initialTime startOffsetTime played seekable ended videoTracks audioTracks videoWidth videoHeight textTracks defaultPlaybackRate playbackRate mediaGroup controller controls defaultMuted".split(" ");
function ea(){var a=u.media.Xa[i];return function(){throw Error('The "'+a+"\" method is not available on the playback technology's API");}}for(var i=u.media.Xa.length-1;0<=i;i--)u.r.prototype[u.media.Xa[i]]=ea();
u.l=u.r.extend({h:function(a,c,d){this.m.volumeControl=u.l.Tc();this.m.movingMediaElementInDOM=!u.Ic;this.m.fullscreenResize=f;u.r.call(this,a,c,d);for(d=u.l.bb.length-1;0<=d;d--)u.d(this.a,u.l.bb[d],u.bind(this.c,this.ad));(c=c.source)&&this.a.currentSrc===c.src&&0<this.a.networkState?a.j("loadstart"):c&&(this.a.src=c.src);if(u.$b&&a.options().nativeControlsForTouch!==l){var e,g,j,k;e=this;g=this.C();c=g.controls();e.a.controls=!!c;j=function(){e.a.controls=f};k=function(){e.a.controls=l};g.d("controlsenabled",
j);g.d("controlsdisabled",k);c=function(){g.o("controlsenabled",j);g.o("controlsdisabled",k)};e.d("dispose",c);g.d("usingcustomcontrols",c);g.usingNativeControls(f)}a.H(function(){this.L&&(this.g.autoplay&&this.paused())&&(delete this.L.poster,this.play())});this.Wa()}});t=u.l.prototype;t.dispose=function(){u.r.prototype.dispose.call(this)};
t.e=function(){var a=this.c,c=a.L,d;if(!c||this.m.movingMediaElementInDOM===l)c?(d=c.cloneNode(l),u.l.gc(c),c=d,a.L=h):c=u.e("video",{id:a.id()+"_html5_api",className:"vjs-tech"}),c.player=a,u.yb(c,a.u());d=["autoplay","preload","loop","muted"];for(var e=d.length-1;0<=e;e--){var g=d[e];a.g[g]!==h&&(c[g]=a.g[g])}return c};t.ad=function(a){this.j(a);a.stopPropagation()};t.play=function(){this.a.play()};t.pause=function(){this.a.pause()};t.paused=function(){return this.a.paused};t.currentTime=function(){return this.a.currentTime};
t.ud=function(a){try{this.a.currentTime=a}catch(c){u.log(c,"Video is not ready. (Video.js)")}};t.duration=function(){return this.a.duration||0};t.buffered=function(){return this.a.buffered};t.volume=function(){return this.a.volume};t.zd=function(a){this.a.volume=a};t.muted=function(){return this.a.muted};t.xd=function(a){this.a.muted=a};t.width=function(){return this.a.offsetWidth};t.height=function(){return this.a.offsetHeight};
t.Va=function(){return"function"==typeof this.a.webkitEnterFullScreen&&(/Android/.test(u.I)||!/Chrome|Mac OS X 10.5/.test(u.I))?f:l};t.hc=function(){var a=this.a;a.paused&&a.networkState<=a.Hd?(this.a.play(),setTimeout(function(){a.pause();a.webkitEnterFullScreen()},0)):a.webkitEnterFullScreen()};t.bd=function(){this.a.webkitExitFullScreen()};t.src=function(a){this.a.src=a};t.load=function(){this.a.load()};t.currentSrc=function(){return this.a.currentSrc};t.poster=function(){return this.a.poster};
t.Mb=function(a){this.a.poster=a};t.Ra=function(){return this.a.Ra};t.yd=function(a){this.a.Ra=a};t.autoplay=function(){return this.a.autoplay};t.td=function(a){this.a.autoplay=a};t.controls=function(){return this.a.controls};t.loop=function(){return this.a.loop};t.wd=function(a){this.a.loop=a};t.error=function(){return this.a.error};t.seeking=function(){return this.a.seeking};t.ended=function(){return this.a.ended};u.l.isSupported=function(){try{u.W.volume=0.5}catch(a){return l}return!!u.W.canPlayType};
u.l.nb=function(a){try{return!!u.W.canPlayType(a.type)}catch(c){return""}};u.l.Tc=function(){var a=u.W.volume;u.W.volume=a/2+0.1;return a!==u.W.volume};u.l.bb="loadstart suspend abort error emptied stalled loadedmetadata loadeddata canplay canplaythrough playing waiting seeking seeked ended durationchange timeupdate progress play pause ratechange volumechange".split(" ");
u.l.gc=function(a){if(a){a.player=h;for(a.parentNode&&a.parentNode.removeChild(a);a.hasChildNodes();)a.removeChild(a.firstChild);a.removeAttribute("src");if("function"===typeof a.load)try{a.load()}catch(c){}}};u.Mc&&(document.createElement("video").constructor.prototype.canPlayType=function(a){return a&&-1!=a.toLowerCase().indexOf("video/mp4")?"maybe":""});
u.f=u.r.extend({h:function(a,c,d){u.r.call(this,a,c,d);var e=c.source;d=c.parentEl;var g=this.a=u.e("div",{id:a.id()+"_temp_flash"}),j=a.id()+"_flash_api";a=a.g;var k=u.k.B({readyFunction:"videojs.Flash.onReady",eventProxyFunction:"videojs.Flash.onEvent",errorEventProxyFunction:"videojs.Flash.onError",autoplay:a.autoplay,preload:a.Ra,loop:a.loop,muted:a.muted},c.flashVars),q=u.k.B({wmode:"opaque",bgcolor:"#000000"},c.params),n=u.k.B({id:j,name:j,"class":"vjs-tech"},c.attributes),s;e&&(e.type&&u.f.hd(e.type)?
(a=u.f.zc(e.src),k.rtmpConnection=encodeURIComponent(a.qb),k.rtmpStream=encodeURIComponent(a.Nb)):k.src=encodeURIComponent(u.kc(e.src)));this.setCurrentTime=function(a){s=a;this.a.vjs_setProperty("currentTime",a)};this.currentTime=function(){return this.seeking()?s:this.a.vjs_getProperty("currentTime")};u.yb(g,d);c.startTime&&this.H(function(){this.load();this.play();this.currentTime(c.startTime)});u.Ub&&this.H(function(){u.d(this.u(),"mousemove",u.bind(this,function(){this.C().j({type:"mousemove",
bubbles:l})}))});if(c.iFrameMode===f&&!u.Ub){var C=u.e("iframe",{id:j+"_iframe",name:j+"_iframe",className:"vjs-tech",scrolling:"no",marginWidth:0,marginHeight:0,frameBorder:0});k.readyFunction="ready";k.eventProxyFunction="events";k.errorEventProxyFunction="errors";u.d(C,"load",u.bind(this,function(){var a,d=C.contentWindow;a=C.contentDocument?C.contentDocument:C.contentWindow.document;a.write(u.f.lc(c.swf,k,q,n));d.player=this.c;d.ready=u.bind(this.c,function(c){var d=this.i;d.a=a.getElementById(c);
u.f.pb(d)});d.events=u.bind(this.c,function(a,c){this&&"flash"===this.xa&&this.j(c)});d.errors=u.bind(this.c,function(a,c){u.log("Flash Error",c)})}));g.parentNode.replaceChild(C,g)}else u.f.$c(c.swf,g,k,q,n)}});t=u.f.prototype;t.dispose=function(){u.r.prototype.dispose.call(this)};t.play=function(){this.a.vjs_play()};t.pause=function(){this.a.vjs_pause()};
t.src=function(a){u.f.gd(a)?(a=u.f.zc(a),this.Td(a.qb),this.Ud(a.Nb)):(a=u.kc(a),this.a.vjs_src(a));if(this.c.autoplay()){var c=this;setTimeout(function(){c.play()},0)}};t.currentSrc=function(){var a=this.a.vjs_getProperty("currentSrc");if(a==h){var c=this.Rd(),d=this.Sd();c&&d&&(a=u.f.Ad(c,d))}return a};t.load=function(){this.a.vjs_load()};t.poster=function(){this.a.vjs_getProperty("poster")};t.Mb=m();t.buffered=function(){return u.sb(0,this.a.vjs_getProperty("buffered"))};t.Va=r(l);t.hc=r(l);
var R=u.f.prototype,S="rtmpConnection rtmpStream preload defaultPlaybackRate playbackRate autoplay loop mediaGroup controller controls volume muted defaultMuted".split(" "),T="error currentSrc networkState readyState seeking initialTime duration startOffsetTime paused played seekable ended videoTracks audioTracks videoWidth videoHeight textTracks".split(" ");function fa(){var a=S[U],c=a.charAt(0).toUpperCase()+a.slice(1);R["set"+c]=function(c){return this.a.vjs_setProperty(a,c)}}
function V(a){R[a]=function(){return this.a.vjs_getProperty(a)}}var U;for(U=0;U<S.length;U++)V(S[U]),fa();for(U=0;U<T.length;U++)V(T[U]);u.f.isSupported=function(){return 10<=u.f.version()[0]};u.f.nb=function(a){if(!a.type)return"";a=a.type.replace(/;.*/,"").toLowerCase();if(a in u.f.dd||a in u.f.Ac)return"maybe"};u.f.dd={"video/flv":"FLV","video/x-flv":"FLV","video/mp4":"MP4","video/m4v":"MP4"};u.f.Ac={"rtmp/mp4":"MP4","rtmp/flv":"FLV"};
u.f.onReady=function(a){a=u.u(a);var c=a.player||a.parentNode.player,d=c.i;a.player=c;d.a=a;u.f.pb(d)};u.f.pb=function(a){a.u().vjs_getProperty?a.Wa():setTimeout(function(){u.f.pb(a)},50)};u.f.onEvent=function(a,c){u.u(a).player.j(c)};u.f.onError=function(a,c){u.u(a).player.j("error");u.log("Flash Error",c,a)};
u.f.version=function(){var a="0,0,0";try{a=(new window.ActiveXObject("ShockwaveFlash.ShockwaveFlash")).GetVariable("$version").replace(/\D+/g,",").match(/^,?(.+),?$/)[1]}catch(c){try{navigator.mimeTypes["application/x-shockwave-flash"].enabledPlugin&&(a=(navigator.plugins["Shockwave Flash 2.0"]||navigator.plugins["Shockwave Flash"]).description.replace(/\D+/g,",").match(/^,?(.+),?$/)[1])}catch(d){}}return a.split(",")};
u.f.$c=function(a,c,d,e,g){a=u.f.lc(a,d,e,g);a=u.e("div",{innerHTML:a}).childNodes[0];d=c.parentNode;c.parentNode.replaceChild(a,c);var j=d.childNodes[0];setTimeout(function(){j.style.display="block"},1E3)};
u.f.lc=function(a,c,d,e){var g="",j="",k="";c&&u.k.ra(c,function(a,c){g+=a+"="+c+"&amp;"});d=u.k.B({movie:a,flashvars:g,allowScriptAccess:"always",allowNetworking:"all"},d);u.k.ra(d,function(a,c){j+='<param name="'+a+'" value="'+c+'" />'});e=u.k.B({data:a,width:"100%",height:"100%"},e);u.k.ra(e,function(a,c){k+=a+'="'+c+'" '});return'<object type="application/x-shockwave-flash"'+k+">"+j+"</object>"};u.f.Ad=function(a,c){return a+"&"+c};
u.f.zc=function(a){var c={qb:"",Nb:""};if(!a)return c;var d=a.indexOf("&"),e;-1!==d?e=d+1:(d=e=a.lastIndexOf("/")+1,0===d&&(d=e=a.length));c.qb=a.substring(0,d);c.Nb=a.substring(e,a.length);return c};u.f.hd=function(a){return a in u.f.Ac};u.f.Oc=/^rtmp[set]?:\/\//i;u.f.gd=function(a){return u.f.Oc.test(a)};
u.Nc=u.b.extend({h:function(a,c,d){u.b.call(this,a,c,d);if(!a.g.sources||0===a.g.sources.length){c=0;for(d=a.g.techOrder;c<d.length;c++){var e=u.Z(d[c]),g=window.videojs[e];if(g&&g.isSupported()){I(a,e);break}}}else a.src(a.g.sources)}});u.Player.prototype.textTracks=function(){return this.ya=this.ya||[]};function W(a,c,d){for(var e=a.ya,g=0,j=e.length,k,q;g<j;g++)k=e[g],k.id()===c?(k.show(),q=k):d&&(k.G()==d&&0<k.mode())&&k.disable();(c=q?q.G():d?d:l)&&a.j(c+"trackchange")}
u.w=u.b.extend({h:function(a,c){u.b.call(this,a,c);this.Q=c.id||"vjs_"+c.kind+"_"+c.language+"_"+u.s++;this.wc=c.src;this.Xc=c["default"]||c.dflt;this.Cd=c.title;this.Od=c.srclang;this.jd=c.label;this.$=[];this.kb=[];this.ga=this.ha=0;this.c.d("fullscreenchange",u.bind(this,this.Qc))}});t=u.w.prototype;t.G=p("A");t.src=p("wc");t.tb=p("Xc");t.title=p("Cd");t.label=p("jd");t.Vc=p("$");t.Pc=p("kb");t.readyState=p("ha");t.mode=p("ga");
t.Qc=function(){this.a.style.fontSize=this.c.isFullScreen()?140*(screen.width/this.c.width())+"%":""};t.e=function(){return u.b.prototype.e.call(this,"div",{className:"vjs-"+this.A+" vjs-text-track"})};t.show=function(){X(this);this.ga=2;u.b.prototype.show.call(this)};t.D=function(){X(this);this.ga=1;u.b.prototype.D.call(this)};
t.disable=function(){2==this.ga&&this.D();this.c.o("timeupdate",u.bind(this,this.update,this.Q));this.c.o("ended",u.bind(this,this.reset,this.Q));this.reset();this.c.fa("textTrackDisplay").removeChild(this);this.ga=0};function X(a){0===a.ha&&a.load();0===a.ga&&(a.c.d("timeupdate",u.bind(a,a.update,a.Q)),a.c.d("ended",u.bind(a,a.reset,a.Q)),("captions"===a.A||"subtitles"===a.A)&&a.c.fa("textTrackDisplay").Y(a))}
t.load=function(){0===this.ha&&(this.ha=1,u.get(this.wc,u.bind(this,this.qd),u.bind(this,this.Gb)))};t.Gb=function(a){this.error=a;this.ha=3;this.j("error")};t.qd=function(a){var c,d;a=a.split("\n");for(var e="",g=1,j=a.length;g<j;g++)if(e=u.trim(a[g])){-1==e.indexOf("--\x3e")?(c=e,e=u.trim(a[++g])):c=this.$.length;c={id:c,index:this.$.length};d=e.split(" --\x3e ");c.startTime=Y(d[0]);c.sa=Y(d[1]);for(d=[];a[++g]&&(e=u.trim(a[g]));)d.push(e);c.text=d.join("<br/>");this.$.push(c)}this.ha=2;this.j("loaded")};
function Y(a){var c=a.split(":");a=0;var d,e,g;3==c.length?(d=c[0],e=c[1],c=c[2]):(d=0,e=c[0],c=c[1]);c=c.split(/\s+/);c=c.splice(0,1)[0];c=c.split(/\.|,/);g=parseFloat(c[1]);c=c[0];a+=3600*parseFloat(d);a+=60*parseFloat(e);a+=parseFloat(c);g&&(a+=g/1E3);return a}
t.update=function(){if(0<this.$.length){var a=this.c.currentTime();if(this.Lb===b||a<this.Lb||this.Na<=a){var c=this.$,d=this.c.duration(),e=0,g=l,j=[],k,q,n,s;a>=this.Na||this.Na===b?s=this.vb!==b?this.vb:0:(g=f,s=this.Cb!==b?this.Cb:c.length-1);for(;;){n=c[s];if(n.sa<=a)e=Math.max(e,n.sa),n.Ha&&(n.Ha=l);else if(a<n.startTime){if(d=Math.min(d,n.startTime),n.Ha&&(n.Ha=l),!g)break}else g?(j.splice(0,0,n),q===b&&(q=s),k=s):(j.push(n),k===b&&(k=s),q=s),d=Math.min(d,n.sa),e=Math.max(e,n.startTime),n.Ha=
f;if(g)if(0===s)break;else s--;else if(s===c.length-1)break;else s++}this.kb=j;this.Na=d;this.Lb=e;this.vb=k;this.Cb=q;a=this.kb;c="";d=0;for(e=a.length;d<e;d++)c+='<span class="vjs-tt-cue">'+a[d].text+"</span>";this.a.innerHTML=c;this.j("cuechange")}}};t.reset=function(){this.Na=0;this.Lb=this.c.duration();this.Cb=this.vb=0};u.Sb=u.w.extend();u.Sb.prototype.A="captions";u.Zb=u.w.extend();u.Zb.prototype.A="subtitles";u.Tb=u.w.extend();u.Tb.prototype.A="chapters";
u.ac=u.b.extend({h:function(a,c,d){u.b.call(this,a,c,d);if(a.g.tracks&&0<a.g.tracks.length){c=this.c;a=a.g.tracks;var e;for(d=0;d<a.length;d++){e=a[d];var g=c,j=e.kind,k=e.label,q=e.language,n=e;e=g.ya=g.ya||[];n=n||{};n.kind=j;n.label=k;n.language=q;j=u.Z(j||"subtitles");g=new window.videojs[j+"Track"](g,n);e.push(g)}}}});u.ac.prototype.e=function(){return u.b.prototype.e.call(this,"div",{className:"vjs-text-track-display"})};
u.X=u.M.extend({h:function(a,c){var d=this.ca=c.track;c.label=d.label();c.selected=d.tb();u.M.call(this,a,c);this.c.d(d.G()+"trackchange",u.bind(this,this.update))}});u.X.prototype.p=function(){u.M.prototype.p.call(this);W(this.c,this.ca.Q,this.ca.G())};u.X.prototype.update=function(){this.selected(2==this.ca.mode())};u.eb=u.X.extend({h:function(a,c){c.track={G:function(){return c.kind},C:a,label:function(){return c.kind+" off"},tb:r(l),mode:r(l)};u.X.call(this,a,c);this.selected(f)}});
u.eb.prototype.p=function(){u.X.prototype.p.call(this);W(this.c,this.ca.Q,this.ca.G())};u.eb.prototype.update=function(){for(var a=this.c.textTracks(),c=0,d=a.length,e,g=f;c<d;c++)e=a[c],e.G()==this.ca.G()&&2==e.mode()&&(g=l);this.selected(g)};u.S=u.R.extend({h:function(a,c){u.R.call(this,a,c);1>=this.K.length&&this.D()}});
u.S.prototype.qa=function(){var a=[],c;a.push(new u.eb(this.c,{kind:this.A}));for(var d=0;d<this.c.textTracks().length;d++)c=this.c.textTracks()[d],c.G()===this.A&&a.push(new u.X(this.c,{track:c}));return a};u.Aa=u.S.extend({h:function(a,c,d){u.S.call(this,a,c,d);this.a.setAttribute("aria-label","Captions Menu")}});u.Aa.prototype.A="captions";u.Aa.prototype.oa="Captions";u.Aa.prototype.className="vjs-captions-button";
u.Ga=u.S.extend({h:function(a,c,d){u.S.call(this,a,c,d);this.a.setAttribute("aria-label","Subtitles Menu")}});u.Ga.prototype.A="subtitles";u.Ga.prototype.oa="Subtitles";u.Ga.prototype.className="vjs-subtitles-button";u.Ba=u.S.extend({h:function(a,c,d){u.S.call(this,a,c,d);this.a.setAttribute("aria-label","Chapters Menu")}});t=u.Ba.prototype;t.A="chapters";t.oa="Chapters";t.className="vjs-chapters-button";
t.qa=function(){for(var a=[],c,d=0;d<this.c.textTracks().length;d++)c=this.c.textTracks()[d],c.G()===this.A&&a.push(new u.X(this.c,{track:c}));return a};
t.La=function(){for(var a=this.c.textTracks(),c=0,d=a.length,e,g,j=this.K=[];c<d;c++)if(e=a[c],e.G()==this.A&&e.tb()){if(2>e.readyState()){this.Ld=e;e.d("loaded",u.bind(this,this.La));return}g=e;break}a=this.ua=new u.la(this.c);a.a.appendChild(u.e("li",{className:"vjs-menu-title",innerHTML:u.Z(this.A),Bd:-1}));if(g){e=g.$;for(var k,c=0,d=e.length;c<d;c++)k=e[c],k=new u.Za(this.c,{track:g,cue:k}),j.push(k),a.Y(k)}0<this.K.length&&this.show();return a};
u.Za=u.M.extend({h:function(a,c){var d=this.ca=c.track,e=this.cue=c.cue,g=a.currentTime();c.label=e.text;c.selected=e.startTime<=g&&g<e.sa;u.M.call(this,a,c);d.d("cuechange",u.bind(this,this.update))}});u.Za.prototype.p=function(){u.M.prototype.p.call(this);this.c.currentTime(this.cue.startTime);this.update(this.cue.startTime)};u.Za.prototype.update=function(){var a=this.cue,c=this.c.currentTime();this.selected(a.startTime<=c&&c<a.sa)};
u.k.B(u.Ca.prototype.g.children,{subtitlesButton:{},captionsButton:{},chaptersButton:{}});
if("undefined"!==typeof window.JSON&&"function"===window.JSON.parse)u.JSON=window.JSON;else{u.JSON={};var Z=/[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;u.JSON.parse=function(a,c){function d(a,e){var k,q,n=a[e];if(n&&"object"===typeof n)for(k in n)Object.prototype.hasOwnProperty.call(n,k)&&(q=d(n,k),q!==b?n[k]=q:delete n[k]);return c.call(a,e,n)}var e;a=String(a);Z.lastIndex=0;Z.test(a)&&(a=a.replace(Z,function(a){return"\\u"+("0000"+a.charCodeAt(0).toString(16)).slice(-4)}));
if(/^[\],:{}\s]*$/.test(a.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,"@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,"]").replace(/(?:^|:|,)(?:\s*\[)+/g,"")))return e=eval("("+a+")"),"function"===typeof c?d({"":e},""):e;throw new SyntaxError("JSON.parse(): invalid or malformed JSON data");}}
u.dc=function(){var a,c,d=document.getElementsByTagName("video");if(d&&0<d.length)for(var e=0,g=d.length;e<g;e++)if((c=d[e])&&c.getAttribute)c.player===b&&(a=c.getAttribute("data-setup"),a!==h&&(a=u.JSON.parse(a||"{}"),videojs(c,a)));else{u.lb();break}else u.Dc||u.lb()};u.lb=function(){setTimeout(u.dc,1)};"complete"===document.readyState?u.Dc=f:u.T(window,"load",function(){u.Dc=f});u.lb();u.rd=function(a,c){u.Player.prototype[a]=c};var ga=this;ga.Gd=f;function $(a,c){var d=a.split("."),e=ga;!(d[0]in e)&&e.execScript&&e.execScript("var "+d[0]);for(var g;d.length&&(g=d.shift());)!d.length&&c!==b?e[g]=c:e=e[g]?e[g]:e[g]={}};$("videojs",u);$("_V_",u);$("videojs.options",u.options);$("videojs.players",u.va);$("videojs.TOUCH_ENABLED",u.$b);$("videojs.cache",u.pa);$("videojs.Component",u.b);u.b.prototype.player=u.b.prototype.C;u.b.prototype.options=u.b.prototype.options;u.b.prototype.init=u.b.prototype.h;u.b.prototype.dispose=u.b.prototype.dispose;u.b.prototype.createEl=u.b.prototype.e;u.b.prototype.contentEl=u.b.prototype.Ka;u.b.prototype.el=u.b.prototype.u;u.b.prototype.addChild=u.b.prototype.Y;
u.b.prototype.getChild=u.b.prototype.fa;u.b.prototype.getChildById=u.b.prototype.ed;u.b.prototype.children=u.b.prototype.children;u.b.prototype.initChildren=u.b.prototype.nc;u.b.prototype.removeChild=u.b.prototype.removeChild;u.b.prototype.on=u.b.prototype.d;u.b.prototype.off=u.b.prototype.o;u.b.prototype.one=u.b.prototype.T;u.b.prototype.trigger=u.b.prototype.j;u.b.prototype.triggerReady=u.b.prototype.Wa;u.b.prototype.show=u.b.prototype.show;u.b.prototype.hide=u.b.prototype.D;
u.b.prototype.width=u.b.prototype.width;u.b.prototype.height=u.b.prototype.height;u.b.prototype.dimensions=u.b.prototype.Yc;u.b.prototype.ready=u.b.prototype.H;u.b.prototype.addClass=u.b.prototype.n;u.b.prototype.removeClass=u.b.prototype.t;u.b.prototype.buildCSSClass=u.b.prototype.P;u.Player.prototype.ended=u.Player.prototype.ended;$("videojs.MediaLoader",u.Nc);$("videojs.TextTrackDisplay",u.ac);$("videojs.ControlBar",u.Ca);$("videojs.Button",u.q);$("videojs.PlayToggle",u.Xb);
$("videojs.FullscreenToggle",u.Da);$("videojs.BigPlayButton",u.Ya);$("videojs.LoadingSpinner",u.Vb);$("videojs.CurrentTimeDisplay",u.$a);$("videojs.DurationDisplay",u.ab);$("videojs.TimeDivider",u.bc);$("videojs.RemainingTimeDisplay",u.gb);$("videojs.Slider",u.N);$("videojs.ProgressControl",u.fb);$("videojs.SeekBar",u.Yb);$("videojs.LoadProgressBar",u.cb);$("videojs.PlayProgressBar",u.Wb);$("videojs.SeekHandle",u.Fa);$("videojs.VolumeControl",u.ib);$("videojs.VolumeBar",u.hb);
$("videojs.VolumeLevel",u.cc);$("videojs.VolumeMenuButton",u.ma);$("videojs.VolumeHandle",u.jb);$("videojs.MuteToggle",u.ea);$("videojs.PosterImage",u.Ea);$("https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/videojs.Menu",u.la);$("videojs.MenuItem",u.M);$("videojs.MenuButton",u.R);u.R.prototype.createItems=u.R.prototype.qa;u.S.prototype.createItems=u.S.prototype.qa;u.Ba.prototype.createItems=u.Ba.prototype.qa;$("videojs.SubtitlesButton",u.Ga);$("videojs.CaptionsButton",u.Aa);$("videojs.ChaptersButton",u.Ba);$("videojs.MediaTechController",u.r);
u.r.prototype.features=u.r.prototype.m;u.r.prototype.m.volumeControl=u.r.prototype.m.Cc;u.r.prototype.m.fullscreenResize=u.r.prototype.m.Md;u.r.prototype.m.progressEvents=u.r.prototype.m.Qd;u.r.prototype.m.timeupdateEvents=u.r.prototype.m.Vd;u.r.prototype.setPoster=u.r.prototype.Mb;$("videojs.Html5",u.l);u.l.Events=u.l.bb;u.l.isSupported=u.l.isSupported;u.l.canPlaySource=u.l.nb;u.l.prototype.setCurrentTime=u.l.prototype.ud;u.l.prototype.setVolume=u.l.prototype.zd;u.l.prototype.setMuted=u.l.prototype.xd;
u.l.prototype.setPreload=u.l.prototype.yd;u.l.prototype.setAutoplay=u.l.prototype.td;u.l.prototype.setLoop=u.l.prototype.wd;u.l.prototype.enterFullScreen=u.l.prototype.hc;u.l.prototype.exitFullScreen=u.l.prototype.bd;$("videojs.Flash",u.f);u.f.isSupported=u.f.isSupported;u.f.canPlaySource=u.f.nb;u.f.onReady=u.f.onReady;$("videojs.TextTrack",u.w);u.w.prototype.label=u.w.prototype.label;u.w.prototype.kind=u.w.prototype.G;u.w.prototype.mode=u.w.prototype.mode;u.w.prototype.cues=u.w.prototype.Vc;
u.w.prototype.activeCues=u.w.prototype.Pc;$("videojs.CaptionsTrack",u.Sb);$("videojs.SubtitlesTrack",u.Zb);$("videojs.ChaptersTrack",u.Tb);$("videojs.autoSetup",u.dc);$("videojs.plugin",u.rd);$("videojs.createTimeRange",u.sb);$("https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/videojs.util",u.ja);u.ja.mergeOptions=u.ja.Fb;})();
;var conditionizr=function(options){var doc=document;var docEl=doc.documentElement;docEl.id='conditionizr';var settings={debug:false,scriptSrc:'js/conditionizr/',styleSrc:'css/conditionizr/',ieLessThan:{active:false,version:'9',scripts:false,styles:false,classes:true,customScript:false},chrome:{scripts:false,styles:false,classes:true,customScript:false},safari:{scripts:false,styles:false,classes:true,customScript:false},opera:{scripts:false,styles:false,classes:true,customScript:false},firefox:{scripts:false,styles:false,classes:true,customScript:false},ie10:{scripts:false,styles:false,classes:true,customScript:false},ie9:{scripts:false,styles:false,classes:true,customScript:false},ie8:{scripts:false,styles:false,classes:true,customScript:false},ie7:{scripts:false,styles:false,classes:true,customScript:false},ie6:{scripts:false,styles:false,classes:true,customScript:false},retina:{scripts:false,styles:false,classes:true,customScript:false},touch:{scripts:false,styles:false,classes:true,customScript:false},mac:true,win:true,x11:true,linux:true};function conditionizrMerge(obj1,obj2){for(var p in obj2){try{if(obj2[p].constructor==Object){obj1[p]=conditionizrMerge(obj1[p],obj2[p]);}else{obj1[p]=obj2[p];}}catch(e){obj1[p]=obj2[p];}}
return obj1;}
if(options){conditionizrMerge(settings,options);}
function conditionizrLoader(){for(var resourceType in browserSettings){var val=browserSettings[resourceType];var head=doc.getElementsByTagName('head')[0];if(resourceType==='classes'&&val){docEl.className+=' '+theBrowser;}
if(resourceType==='scripts'&&val){var scriptTag=doc.createElement('script');scriptTag.src=settings.scriptSrc+theBrowser+'.js';head.appendChild(scriptTag);}
if(resourceType==='styles'&&val){var linkTag=doc.createElement('link');linkTag.rel='stylesheet';linkTag.href=settings.styleSrc+theBrowser+'.css';head.appendChild(linkTag);}
if(resourceType==='customScript'&&val){var strip=browserSettings.customScript.replace(/\s/g,'');var customSplit=strip.split(',');for(var i=0;i<customSplit.length;i++){var customScriptTag=doc.createElement('script');customScriptTag.src=customSplit[i];head.appendChild(customScriptTag);}}}}
var actualBrowser='';var browsers=[{'testWith':'chrome','testSettings':settings.chrome},{'testWith':'safari','testSettings':settings.safari},{'testWith':'firefox','testSettings':settings.firefox},{'testWith':'opera','testSettings':settings.opera}];for(var i=0;i<browsers.length;i++){var currentBrowser=browsers[i];if(navigator.userAgent.toLowerCase().indexOf(currentBrowser.testWith)>-1){var browserSettings=currentBrowser.testSettings;var theBrowser=currentBrowser.testWith;conditionizrLoader();actualBrowser=theBrowser;break;}}
function getIEVersion(){var rv=-1;if(navigator.appName=='Microsoft Internet Explorer'){var ua=navigator.userAgent;var re=new RegExp('MSIE ([0-9]{1,}[\.0-9]{0,})');if(re.exec(ua)!=null){rv=parseFloat(RegExp.$1);}}
return rv;}
var version=getIEVersion();if(version>-1){if(version<settings.ieLessThan.version+'.0'){var theBrowser='lt-ie'+settings.ieLessThan.version;var browserSettings=settings.ieLessThan;conditionizrLoader();}
if(version===10.0){var browserSettings=settings.ie10;}
else if(version===9.0){var browserSettings=settings.ie9;}
else if(version===8.0){var browserSettings=settings.ie8;}
else if(version===7.0){var browserSettings=settings.ie7;}
else if(version===6.0){var browserSettings=settings.ie6;}
var theBrowser='ie'+version;conditionizrLoader();actualBrowser=theBrowser;}
var browserExtras='';if(window.devicePixelRatio>=2){var browserSettings=settings.retina;var theBrowser='retina';conditionizrLoader();browserExtras+=' '+theBrowser;theBrowser=actualBrowser;}else{docEl.className+=' no-retina';}
if('ontouchstart'in window){var browserSettings=settings.touch,theBrowser='touch';conditionizrLoader();browserExtras+=' '+theBrowser;theBrowser=actualBrowser;}else{docEl.className+=' no-touch';}
var oSys=[{'testWith':'Win','testSettings':settings.win},{'testWith':'Mac','testSettings':settings.mac},{'testWith':'X11','testSettings':settings.x11},{'testWith':'Linux','testSettings':settings.linux}];for(var i=0;i<oSys.length;i++){var currentPlatform=oSys[i];if(navigator.appVersion.indexOf(currentPlatform.testWith)>-1){var osSettings=currentPlatform.testSettings;var theOS=currentPlatform.testWith;if(osSettings){docEl.className+=' '+currentPlatform.testWith.toLowerCase();}
break;}}
if(settings.debug){console.log('Start Conditionizr Debug\n');console.log('Script location: '+settings.scriptSrc);console.log('Style location: '+settings.styleSrc);console.log('Browser: '+theBrowser);if(browserExtras){console.log('Browser Extras: '+browserExtras);}
console.log('OS: '+theOS);console.log('End Conditionizr Debug\n');}};conditionizr();var oOobj3=new OnlineOpinion.ocode();oOobj3.Preferences={Persistence:{enabled:false,cookie_name:'oo_inline',cookie_type:'null',expiration:0},Render:{type:'inline'},Plugins:{URLRewrite:{active:false,regex_search_pattern:'',regex_replace_pattern:'',full_url_rewrite:''},CardOnPage:{enabled:false,close_link:''}}};(function($,window,undefined){$.fn.jScrollPane=function(settings)
{function JScrollPane(elem,s)
{var settings,jsp=this,pane,paneWidth,paneHeight,container,contentWidth,contentHeight,percentInViewH,percentInViewV,isScrollableV,isScrollableH,verticalDrag,dragMaxY,verticalDragPosition,horizontalDrag,dragMaxX,horizontalDragPosition,verticalBar,verticalTrack,scrollbarWidth,verticalTrackHeight,verticalDragHeight,arrowUp,arrowDown,horizontalBar,horizontalTrack,horizontalTrackWidth,horizontalDragWidth,arrowLeft,arrowRight,reinitialiseInterval,originalPadding,originalPaddingTotalWidth,previousContentWidth,wasAtTop=true,wasAtLeft=true,wasAtBottom=false,wasAtRight=false,originalElement=elem.clone(false,false).empty(),mwEvent=$.fn.mwheelIntent?'https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/mwheelIntent.jsp':'https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/mousewheel.jsp';originalPadding=elem.css('paddingTop')+' '+
elem.css('paddingRight')+' '+
elem.css('paddingBottom')+' '+
elem.css('paddingLeft');originalPaddingTotalWidth=(parseInt(elem.css('paddingLeft'),10)||0)+
(parseInt(elem.css('paddingRight'),10)||0);function initialise(s)
{var isMaintainingPositon,lastContentX,lastContentY,hasContainingSpaceChanged,originalScrollTop,originalScrollLeft,maintainAtBottom=false,maintainAtRight=false;settings=s;if(pane===undefined){originalScrollTop=elem.scrollTop();originalScrollLeft=elem.scrollLeft();elem.css({overflow:'hidden',padding:0});paneWidth=elem.innerWidth()+originalPaddingTotalWidth;paneHeight=elem.innerHeight();elem.width(paneWidth);pane=$('<div class="jspPane" />').css('padding',originalPadding).append(elem.children());container=$('<div class="jspContainer" />').css({'width':paneWidth+'px','height':paneHeight+'px'}).append(pane).appendTo(elem);}else{elem.css('width','');maintainAtBottom=settings.stickToBottom&&isCloseToBottom();maintainAtRight=settings.stickToRight&&isCloseToRight();hasContainingSpaceChanged=elem.innerWidth()+originalPaddingTotalWidth!=paneWidth||elem.outerHeight()!=paneHeight;if(hasContainingSpaceChanged){paneWidth=elem.innerWidth()+originalPaddingTotalWidth;paneHeight=elem.innerHeight();container.css({width:paneWidth+'px',height:paneHeight+'px'});}
if(!hasContainingSpaceChanged&&previousContentWidth==contentWidth&&pane.outerHeight()==contentHeight){elem.width(paneWidth);return;}
previousContentWidth=contentWidth;pane.css('width','');elem.width(paneWidth);container.find('>.jspVerticalBar,>.jspHorizontalBar').remove().end();}
pane.css('overflow','auto');if(s.contentWidth){contentWidth=s.contentWidth;}else{contentWidth=pane[0].scrollWidth;}
contentHeight=pane[0].scrollHeight;pane.css('overflow','');percentInViewH=contentWidth/paneWidth;percentInViewV=contentHeight/paneHeight;isScrollableV=percentInViewV>1;isScrollableH=percentInViewH>1;if(!(isScrollableH||isScrollableV)){elem.removeClass('jspScrollable');pane.css({top:0,width:container.width()-originalPaddingTotalWidth});removeMousewheel();removeFocusHandler();removeKeyboardNav();removeClickOnTrack();}else{elem.addClass('jspScrollable');isMaintainingPositon=settings.maintainPosition&&(verticalDragPosition||horizontalDragPosition);if(isMaintainingPositon){lastContentX=contentPositionX();lastContentY=contentPositionY();}
initialiseVerticalScroll();initialiseHorizontalScroll();resizeScrollbars();if(isMaintainingPositon){scrollToX(maintainAtRight?(contentWidth-paneWidth):lastContentX,false);scrollToY(maintainAtBottom?(contentHeight-paneHeight):lastContentY,false);}
initFocusHandler();initMousewheel();initTouch();if(settings.enableKeyboardNavigation){initKeyboardNav();}
if(settings.clickOnTrack){initClickOnTrack();}
observeHash();if(settings.hijackInternalLinks){hijackInternalLinks();}}
if(settings.autoReinitialise&&!reinitialiseInterval){reinitialiseInterval=setInterval(function()
{initialise(settings);},settings.autoReinitialiseDelay);}else if(!settings.autoReinitialise&&reinitialiseInterval){clearInterval(reinitialiseInterval);}
originalScrollTop&&elem.scrollTop(0)&&scrollToY(originalScrollTop,false);originalScrollLeft&&elem.scrollLeft(0)&&scrollToX(originalScrollLeft,false);elem.trigger('jsp-initialised',[isScrollableH||isScrollableV]);}
function initialiseVerticalScroll()
{if(isScrollableV){container.append($('<div class="jspVerticalBar" />').append($('<div class="jspCap jspCapTop" />'),$('<div class="jspTrack" />').append($('<div class="jspDrag" />').append($('<div class="jspDragTop" />'),$('<div class="jspDragBottom" />'))),$('<div class="jspCap jspCapBottom" />')));verticalBar=container.find('>.jspVerticalBar');verticalTrack=verticalBar.find('>.jspTrack');verticalDrag=verticalTrack.find('>.jspDrag');if(settings.showArrows){arrowUp=$('<a class="jspArrow jspArrowUp" />').bind('https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/mousedown.jsp',getArrowScroll(0,-1)).bind('https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/click.jsp',nil);arrowDown=$('<a class="jspArrow jspArrowDown" />').bind('https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/mousedown.jsp',getArrowScroll(0,1)).bind('https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/click.jsp',nil);if(settings.arrowScrollOnHover){arrowUp.bind('https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/mouseover.jsp',getArrowScroll(0,-1,arrowUp));arrowDown.bind('https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/mouseover.jsp',getArrowScroll(0,1,arrowDown));}
appendArrows(verticalTrack,settings.verticalArrowPositions,arrowUp,arrowDown);}
verticalTrackHeight=paneHeight;container.find('>.jspVerticalBar>.jspCap:visible,>.jspVerticalBar>.jspArrow').each(function()
{verticalTrackHeight-=$(this).outerHeight();});verticalDrag.hover(function()
{verticalDrag.addClass('jspHover');},function()
{verticalDrag.removeClass('jspHover');}).bind('https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/mousedown.jsp',function(e)
{$('html').bind('https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/dragstart.jsp selectstart.jsp',nil);verticalDrag.addClass('jspActive');var startY=e.pageY-verticalDrag.position().top;$('html').bind('https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/mousemove.jsp',function(e)
{positionDragY(e.pageY-startY,false);}).bind('https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/mouseup.jsp mouseleave.jsp',cancelDrag);return false;});sizeVerticalScrollbar();}}
function sizeVerticalScrollbar()
{verticalTrack.height(verticalTrackHeight+'px');verticalDragPosition=0;scrollbarWidth=settings.verticalGutter+verticalTrack.outerWidth();pane.width(paneWidth-scrollbarWidth-originalPaddingTotalWidth);try{if(verticalBar.position().left===0){pane.css('margin-left',scrollbarWidth+'px');}}catch(err){}}
function initialiseHorizontalScroll()
{if(isScrollableH){container.append($('<div class="jspHorizontalBar" />').append($('<div class="jspCap jspCapLeft" />'),$('<div class="jspTrack" />').append($('<div class="jspDrag" />').append($('<div class="jspDragLeft" />'),$('<div class="jspDragRight" />'))),$('<div class="jspCap jspCapRight" />')));horizontalBar=container.find('>.jspHorizontalBar');horizontalTrack=horizontalBar.find('>.jspTrack');horizontalDrag=horizontalTrack.find('>.jspDrag');if(settings.showArrows){arrowLeft=$('<a class="jspArrow jspArrowLeft" />').bind('https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/mousedown.jsp',getArrowScroll(-1,0)).bind('https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/click.jsp',nil);arrowRight=$('<a class="jspArrow jspArrowRight" />').bind('https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/mousedown.jsp',getArrowScroll(1,0)).bind('https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/click.jsp',nil);if(settings.arrowScrollOnHover){arrowLeft.bind('https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/mouseover.jsp',getArrowScroll(-1,0,arrowLeft));arrowRight.bind('https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/mouseover.jsp',getArrowScroll(1,0,arrowRight));}
appendArrows(horizontalTrack,settings.horizontalArrowPositions,arrowLeft,arrowRight);}
horizontalDrag.hover(function()
{horizontalDrag.addClass('jspHover');},function()
{horizontalDrag.removeClass('jspHover');}).bind('https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/mousedown.jsp',function(e)
{$('html').bind('https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/dragstart.jsp selectstart.jsp',nil);horizontalDrag.addClass('jspActive');var startX=e.pageX-horizontalDrag.position().left;$('html').bind('https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/mousemove.jsp',function(e)
{positionDragX(e.pageX-startX,false);}).bind('https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/mouseup.jsp mouseleave.jsp',cancelDrag);return false;});horizontalTrackWidth=container.innerWidth();sizeHorizontalScrollbar();}}
function sizeHorizontalScrollbar()
{container.find('>.jspHorizontalBar>.jspCap:visible,>.jspHorizontalBar>.jspArrow').each(function()
{horizontalTrackWidth-=$(this).outerWidth();});horizontalTrack.width(horizontalTrackWidth+'px');horizontalDragPosition=0;}
function resizeScrollbars()
{if(isScrollableH&&isScrollableV){var horizontalTrackHeight=horizontalTrack.outerHeight(),verticalTrackWidth=verticalTrack.outerWidth();verticalTrackHeight-=horizontalTrackHeight;$(horizontalBar).find('>.jspCap:visible,>.jspArrow').each(function()
{horizontalTrackWidth+=$(this).outerWidth();});horizontalTrackWidth-=verticalTrackWidth;paneHeight-=verticalTrackWidth;paneWidth-=horizontalTrackHeight;horizontalTrack.parent().append($('<div class="jspCorner" />').css('width',horizontalTrackHeight+'px'));sizeVerticalScrollbar();sizeHorizontalScrollbar();}
if(isScrollableH){pane.width((container.outerWidth()-originalPaddingTotalWidth)+'px');}
contentHeight=pane.outerHeight();percentInViewV=contentHeight/paneHeight;if(isScrollableH){horizontalDragWidth=Math.ceil(1/percentInViewH*horizontalTrackWidth);if(horizontalDragWidth>settings.horizontalDragMaxWidth){horizontalDragWidth=settings.horizontalDragMaxWidth;}else if(horizontalDragWidth<settings.horizontalDragMinWidth){horizontalDragWidth=settings.horizontalDragMinWidth;}
horizontalDrag.width(horizontalDragWidth+'px');dragMaxX=horizontalTrackWidth-horizontalDragWidth;_positionDragX(horizontalDragPosition);}
if(isScrollableV){verticalDragHeight=Math.ceil(1/percentInViewV*verticalTrackHeight);if(verticalDragHeight>settings.verticalDragMaxHeight){verticalDragHeight=settings.verticalDragMaxHeight;}else if(verticalDragHeight<settings.verticalDragMinHeight){verticalDragHeight=settings.verticalDragMinHeight;}
verticalDrag.height(verticalDragHeight+'px');dragMaxY=verticalTrackHeight-verticalDragHeight;_positionDragY(verticalDragPosition);}}
function appendArrows(ele,p,a1,a2)
{var p1="before",p2="after",aTemp;if(p=="os"){p=/Mac/.test(navigator.platform)?"after":"split";}
if(p==p1){p2=p;}else if(p==p2){p1=p;aTemp=a1;a1=a2;a2=aTemp;}
ele[p1](a1)[p2](a2);}
function getArrowScroll(dirX,dirY,ele)
{return function()
{arrowScroll(dirX,dirY,this,ele);this.blur();return false;};}
function arrowScroll(dirX,dirY,arrow,ele)
{arrow=$(arrow).addClass('jspActive');var eve,scrollTimeout,isFirst=true,doScroll=function()
{if(dirX!==0){jsp.scrollByX(dirX*settings.arrowButtonSpeed);}
if(dirY!==0){jsp.scrollByY(dirY*settings.arrowButtonSpeed);}
scrollTimeout=setTimeout(doScroll,isFirst?settings.initialDelay:settings.arrowRepeatFreq);isFirst=false;};doScroll();eve=ele?'https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/mouseout.jsp':'https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/mouseup.jsp';ele=ele||$('html');ele.bind(eve,function()
{arrow.removeClass('jspActive');scrollTimeout&&clearTimeout(scrollTimeout);scrollTimeout=null;ele.unbind(eve);});}
function initClickOnTrack()
{removeClickOnTrack();if(isScrollableV){verticalTrack.bind('https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/mousedown.jsp',function(e)
{if(e.originalTarget===undefined||e.originalTarget==e.currentTarget){var clickedTrack=$(this),offset=clickedTrack.offset(),direction=e.pageY-offset.top-verticalDragPosition,scrollTimeout,isFirst=true,doScroll=function()
{var offset=clickedTrack.offset(),pos=e.pageY-offset.top-verticalDragHeight/2,contentDragY=paneHeight*settings.scrollPagePercent,dragY=dragMaxY*contentDragY/(contentHeight-paneHeight);if(direction<0){if(verticalDragPosition-dragY>pos){jsp.scrollByY(-contentDragY);}else{positionDragY(pos);}}else if(direction>0){if(verticalDragPosition+dragY<pos){jsp.scrollByY(contentDragY);}else{positionDragY(pos);}}else{cancelClick();return;}
scrollTimeout=setTimeout(doScroll,isFirst?settings.initialDelay:settings.trackClickRepeatFreq);isFirst=false;},cancelClick=function()
{scrollTimeout&&clearTimeout(scrollTimeout);scrollTimeout=null;$(document).unbind('https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/mouseup.jsp',cancelClick);};doScroll();$(document).bind('https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/mouseup.jsp',cancelClick);return false;}});}
if(isScrollableH){horizontalTrack.bind('https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/mousedown.jsp',function(e)
{if(e.originalTarget===undefined||e.originalTarget==e.currentTarget){var clickedTrack=$(this),offset=clickedTrack.offset(),direction=e.pageX-offset.left-horizontalDragPosition,scrollTimeout,isFirst=true,doScroll=function()
{var offset=clickedTrack.offset(),pos=e.pageX-offset.left-horizontalDragWidth/2,contentDragX=paneWidth*settings.scrollPagePercent,dragX=dragMaxX*contentDragX/(contentWidth-paneWidth);if(direction<0){if(horizontalDragPosition-dragX>pos){jsp.scrollByX(-contentDragX);}else{positionDragX(pos);}}else if(direction>0){if(horizontalDragPosition+dragX<pos){jsp.scrollByX(contentDragX);}else{positionDragX(pos);}}else{cancelClick();return;}
scrollTimeout=setTimeout(doScroll,isFirst?settings.initialDelay:settings.trackClickRepeatFreq);isFirst=false;},cancelClick=function()
{scrollTimeout&&clearTimeout(scrollTimeout);scrollTimeout=null;$(document).unbind('https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/mouseup.jsp',cancelClick);};doScroll();$(document).bind('https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/mouseup.jsp',cancelClick);return false;}});}}
function removeClickOnTrack()
{if(horizontalTrack){horizontalTrack.unbind('https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/mousedown.jsp');}
if(verticalTrack){verticalTrack.unbind('https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/mousedown.jsp');}}
function cancelDrag()
{$('html').unbind('https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/dragstart.jsp selectstart.jsp mousemove.jsp mouseup.jsp mouseleave.jsp');if(verticalDrag){verticalDrag.removeClass('jspActive');}
if(horizontalDrag){horizontalDrag.removeClass('jspActive');}}
function positionDragY(destY,animate)
{if(!isScrollableV){return;}
if(destY<0){destY=0;}else if(destY>dragMaxY){destY=dragMaxY;}
if(animate===undefined){animate=settings.animateScroll;}
if(animate){jsp.animate(verticalDrag,'top',destY,_positionDragY);}else{verticalDrag.css('top',destY);_positionDragY(destY);}}
function _positionDragY(destY)
{if(destY===undefined){destY=verticalDrag.position().top;}
container.scrollTop(0);verticalDragPosition=destY;var isAtTop=verticalDragPosition===0,isAtBottom=verticalDragPosition==dragMaxY,percentScrolled=destY/dragMaxY,destTop=-percentScrolled*(contentHeight-paneHeight);if(wasAtTop!=isAtTop||wasAtBottom!=isAtBottom){wasAtTop=isAtTop;wasAtBottom=isAtBottom;elem.trigger('jsp-arrow-change',[wasAtTop,wasAtBottom,wasAtLeft,wasAtRight]);}
updateVerticalArrows(isAtTop,isAtBottom);pane.css('top',destTop);elem.trigger('jsp-scroll-y',[-destTop,isAtTop,isAtBottom]).trigger('scroll');}
function positionDragX(destX,animate)
{if(!isScrollableH){return;}
if(destX<0){destX=0;}else if(destX>dragMaxX){destX=dragMaxX;}
if(animate===undefined){animate=settings.animateScroll;}
if(animate){jsp.animate(horizontalDrag,'left',destX,_positionDragX);}else{horizontalDrag.css('left',destX);_positionDragX(destX);}}
function _positionDragX(destX)
{if(destX===undefined){destX=horizontalDrag.position().left;}
container.scrollTop(0);horizontalDragPosition=destX;var isAtLeft=horizontalDragPosition===0,isAtRight=horizontalDragPosition==dragMaxX,percentScrolled=destX/dragMaxX,destLeft=-percentScrolled*(contentWidth-paneWidth);if(wasAtLeft!=isAtLeft||wasAtRight!=isAtRight){wasAtLeft=isAtLeft;wasAtRight=isAtRight;elem.trigger('jsp-arrow-change',[wasAtTop,wasAtBottom,wasAtLeft,wasAtRight]);}
updateHorizontalArrows(isAtLeft,isAtRight);pane.css('left',destLeft);elem.trigger('jsp-scroll-x',[-destLeft,isAtLeft,isAtRight]).trigger('scroll');}
function updateVerticalArrows(isAtTop,isAtBottom)
{if(settings.showArrows){arrowUp[isAtTop?'addClass':'removeClass']('jspDisabled');arrowDown[isAtBottom?'addClass':'removeClass']('jspDisabled');}}
function updateHorizontalArrows(isAtLeft,isAtRight)
{if(settings.showArrows){arrowLeft[isAtLeft?'addClass':'removeClass']('jspDisabled');arrowRight[isAtRight?'addClass':'removeClass']('jspDisabled');}}
function scrollToY(destY,animate)
{var percentScrolled=destY/(contentHeight-paneHeight);positionDragY(percentScrolled*dragMaxY,animate);}
function scrollToX(destX,animate)
{var percentScrolled=destX/(contentWidth-paneWidth);positionDragX(percentScrolled*dragMaxX,animate);}
function scrollToElement(ele,stickToTop,animate)
{var e,eleHeight,eleWidth,eleTop=0,eleLeft=0,viewportTop,viewportLeft,maxVisibleEleTop,maxVisibleEleLeft,destY,destX;try{e=$(ele);}catch(err){return;}
eleHeight=e.outerHeight();eleWidth=e.outerWidth();container.scrollTop(0);container.scrollLeft(0);while(!e.is('.jspPane')){eleTop+=e.position().top;eleLeft+=e.position().left;e=e.offsetParent();if(/^body|html$/i.test(e[0].nodeName)){return;}}
viewportTop=contentPositionY();maxVisibleEleTop=viewportTop+paneHeight;if(eleTop<viewportTop||stickToTop){destY=eleTop-settings.verticalGutter;}else if(eleTop+eleHeight>maxVisibleEleTop){destY=eleTop-paneHeight+eleHeight+settings.verticalGutter;}
if(destY){scrollToY(destY,animate);}
viewportLeft=contentPositionX();maxVisibleEleLeft=viewportLeft+paneWidth;if(eleLeft<viewportLeft||stickToTop){destX=eleLeft-settings.horizontalGutter;}else if(eleLeft+eleWidth>maxVisibleEleLeft){destX=eleLeft-paneWidth+eleWidth+settings.horizontalGutter;}
if(destX){scrollToX(destX,animate);}}
function contentPositionX()
{return-pane.position().left;}
function contentPositionY()
{return-pane.position().top;}
function isCloseToBottom()
{var scrollableHeight=contentHeight-paneHeight;return(scrollableHeight>20)&&(scrollableHeight-contentPositionY()<10);}
function isCloseToRight()
{var scrollableWidth=contentWidth-paneWidth;return(scrollableWidth>20)&&(scrollableWidth-contentPositionX()<10);}
function initMousewheel()
{container.unbind(mwEvent).bind(mwEvent,function(event,delta,deltaX,deltaY){var dX=horizontalDragPosition,dY=verticalDragPosition;jsp.scrollBy(deltaX*settings.mouseWheelSpeed,-deltaY*settings.mouseWheelSpeed,false);return dX==horizontalDragPosition&&dY==verticalDragPosition;});}
function removeMousewheel()
{container.unbind(mwEvent);}
function nil()
{return false;}
function initFocusHandler()
{pane.find(':input,a').unbind('https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/focus.jsp').bind('https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/focus.jsp',function(e)
{scrollToElement(e.target,false);});}
function removeFocusHandler()
{pane.find(':input,a').unbind('https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/focus.jsp');}
function initKeyboardNav()
{var keyDown,elementHasScrolled,validParents=[];isScrollableH&&validParents.push(horizontalBar[0]);isScrollableV&&validParents.push(verticalBar[0]);pane.focus(function()
{elem.focus();});elem.attr('tabindex',0).unbind('https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/keydown.jsp keypress.jsp').bind('https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/keydown.jsp',function(e)
{if(e.target!==this&&!(validParents.length&&$(e.target).closest(validParents).length)){return;}
var dX=horizontalDragPosition,dY=verticalDragPosition;switch(e.keyCode){case 40:case 38:case 34:case 32:case 33:case 39:case 37:keyDown=e.keyCode;keyDownHandler();break;case 35:scrollToY(contentHeight-paneHeight);keyDown=null;break;case 36:scrollToY(0);keyDown=null;break;}
elementHasScrolled=e.keyCode==keyDown&&dX!=horizontalDragPosition||dY!=verticalDragPosition;return!elementHasScrolled;}).bind('https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/keypress.jsp',function(e)
{if(e.keyCode==keyDown){keyDownHandler();}
return!elementHasScrolled;});if(settings.hideFocus){elem.css('outline','none');if('hideFocus'in container[0]){elem.attr('hideFocus',true);}}else{elem.css('outline','');if('hideFocus'in container[0]){elem.attr('hideFocus',false);}}
function keyDownHandler()
{var dX=horizontalDragPosition,dY=verticalDragPosition;switch(keyDown){case 40:jsp.scrollByY(settings.keyboardSpeed,false);break;case 38:jsp.scrollByY(-settings.keyboardSpeed,false);break;case 34:case 32:jsp.scrollByY(paneHeight*settings.scrollPagePercent,false);break;case 33:jsp.scrollByY(-paneHeight*settings.scrollPagePercent,false);break;case 39:jsp.scrollByX(settings.keyboardSpeed,false);break;case 37:jsp.scrollByX(-settings.keyboardSpeed,false);break;}
elementHasScrolled=dX!=horizontalDragPosition||dY!=verticalDragPosition;return elementHasScrolled;}}
function removeKeyboardNav()
{elem.attr('tabindex','-1').removeAttr('tabindex').unbind('https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/keydown.jsp keypress.jsp');}
function observeHash()
{if(location.hash&&location.hash.length>1){var e,retryInt,hash=escape(location.hash.substr(1));try{e=$('#'+hash+', a[name="'+hash+'"]');}catch(err){return;}
if(e.length&&pane.find(hash)){if(container.scrollTop()===0){retryInt=setInterval(function()
{if(container.scrollTop()>0){scrollToElement(e,true);$(document).scrollTop(container.position().top);clearInterval(retryInt);}},50);}else{scrollToElement(e,true);$(document).scrollTop(container.position().top);}}}}
function hijackInternalLinks()
{if($(document.body).data('jspHijack')){return;}
$(document.body).data('jspHijack',true);$(document.body).delegate('a[href*=#]','click',function(event){var href=this.href.substr(0,this.href.indexOf('#')),locationHref=location.href,hash,element,container,jsp,scrollTop,elementTop;if(location.href.indexOf('#')!==-1){locationHref=location.href.substr(0,location.href.indexOf('#'));}
if(href!==locationHref){return;}
hash=escape(this.href.substr(this.href.indexOf('#')+1));element;try{element=$('#'+hash+', a[name="'+hash+'"]');}catch(e){return;}
if(!element.length){return;}
container=element.closest('.jspScrollable');jsp=container.data('jsp');jsp.scrollToElement(element,true);if(container[0].scrollIntoView){scrollTop=$(window).scrollTop();elementTop=element.offset().top;if(elementTop<scrollTop||elementTop>scrollTop+$(window).height()){container[0].scrollIntoView();}}
event.preventDefault();});}
function initTouch()
{var startX,startY,touchStartX,touchStartY,moved,moving=false;container.unbind('touchstart.jsp touchmove.jsp touchend.jsp click.jsp-touchclick').bind('https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/touchstart.jsp',function(e)
{var touch=e.originalEvent.touches[0];startX=contentPositionX();startY=contentPositionY();touchStartX=touch.pageX;touchStartY=touch.pageY;moved=false;moving=true;}).bind('https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/touchmove.jsp',function(ev)
{if(!moving){return;}
var touchPos=ev.originalEvent.touches[0],dX=horizontalDragPosition,dY=verticalDragPosition;jsp.scrollTo(startX+touchStartX-touchPos.pageX,startY+touchStartY-touchPos.pageY);moved=moved||Math.abs(touchStartX-touchPos.pageX)>5||Math.abs(touchStartY-touchPos.pageY)>5;return dX==horizontalDragPosition&&dY==verticalDragPosition;}).bind('https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/touchend.jsp',function(e)
{moving=false;}).bind('click.jsp-touchclick',function(e)
{if(moved){moved=false;return false;}});}
function destroy(){var currentY=contentPositionY(),currentX=contentPositionX();elem.removeClass('jspScrollable').unbind('.jsp');elem.replaceWith(originalElement.append(pane.children()));originalElement.scrollTop(currentY);originalElement.scrollLeft(currentX);if(reinitialiseInterval){clearInterval(reinitialiseInterval);}}
$.extend(jsp,{reinitialise:function(s)
{s=$.extend({},settings,s);initialise(s);},scrollToElement:function(ele,stickToTop,animate)
{scrollToElement(ele,stickToTop,animate);},scrollTo:function(destX,destY,animate)
{scrollToX(destX,animate);scrollToY(destY,animate);},scrollToX:function(destX,animate)
{scrollToX(destX,animate);},scrollToY:function(destY,animate)
{scrollToY(destY,animate);},scrollToPercentX:function(destPercentX,animate)
{scrollToX(destPercentX*(contentWidth-paneWidth),animate);},scrollToPercentY:function(destPercentY,animate)
{scrollToY(destPercentY*(contentHeight-paneHeight),animate);},scrollBy:function(deltaX,deltaY,animate)
{jsp.scrollByX(deltaX,animate);jsp.scrollByY(deltaY,animate);},scrollByX:function(deltaX,animate)
{var destX=contentPositionX()+Math[deltaX<0?'floor':'ceil'](deltaX),percentScrolled=destX/(contentWidth-paneWidth);positionDragX(percentScrolled*dragMaxX,animate);},scrollByY:function(deltaY,animate)
{var destY=contentPositionY()+Math[deltaY<0?'floor':'ceil'](deltaY),percentScrolled=destY/(contentHeight-paneHeight);positionDragY(percentScrolled*dragMaxY,animate);},positionDragX:function(x,animate)
{positionDragX(x,animate);},positionDragY:function(y,animate)
{positionDragY(y,animate);},animate:function(ele,prop,value,stepCallback)
{var params={};params[prop]=value;ele.animate(params,{'duration':settings.animateDuration,'easing':settings.animateEase,'queue':false,'step':stepCallback});},getContentPositionX:function()
{return contentPositionX();},getContentPositionY:function()
{return contentPositionY();},getContentWidth:function()
{return contentWidth;},getContentHeight:function()
{return contentHeight;},getPercentScrolledX:function()
{return contentPositionX()/(contentWidth-paneWidth);},getPercentScrolledY:function()
{return contentPositionY()/(contentHeight-paneHeight);},getIsScrollableH:function()
{return isScrollableH;},getIsScrollableV:function()
{return isScrollableV;},getContentPane:function()
{return pane;},scrollToBottom:function(animate)
{positionDragY(dragMaxY,animate);},hijackInternalLinks:$.noop,destroy:function()
{destroy();}});initialise(s);}
settings=$.extend({},$.fn.jScrollPane.defaults,settings);$.each(['mouseWheelSpeed','arrowButtonSpeed','trackClickSpeed','keyboardSpeed'],function(){settings[this]=settings[this]||settings.speed;});return this.each(function()
{var elem=$(this),jspApi=elem.data('jsp');if(jspApi){jspApi.reinitialise(settings);}else{$("script",elem).filter('[type="text/javascript"],not([type])').remove();jspApi=new JScrollPane(elem,settings);elem.data('jsp',jspApi);}});};$.fn.jScrollPane.defaults={showArrows:false,maintainPosition:true,stickToBottom:false,stickToRight:false,clickOnTrack:true,autoReinitialise:false,autoReinitialiseDelay:500,verticalDragMinHeight:0,verticalDragMaxHeight:99999,horizontalDragMinWidth:0,horizontalDragMaxWidth:99999,contentWidth:undefined,animateScroll:false,animateDuration:300,animateEase:'linear',hijackInternalLinks:false,verticalGutter:4,horizontalGutter:4,mouseWheelSpeed:0,arrowButtonSpeed:0,arrowRepeatFreq:50,arrowScrollOnHover:false,trackClickSpeed:0,trackClickRepeatFreq:70,verticalArrowPositions:'split',horizontalArrowPositions:'split',enableKeyboardNavigation:true,hideFocus:false,keyboardSpeed:0,initialDelay:300,speed:30,scrollPagePercent:.8};})(jQuery,this);(function($){var types=['DOMMouseScroll','mousewheel'];$.event.special.mousewheel={setup:function(){if(this.addEventListener){for(var i=types.length;i;){this.addEventListener(types[--i],handler,false);}}else{this.onmousewheel=handler;}},teardown:function(){if(this.removeEventListener){for(var i=types.length;i;){this.removeEventListener(types[--i],handler,false);}}else{this.onmousewheel=null;}}};$.fn.extend({mousewheel:function(fn){return fn?this.bind("mousewheel",fn):this.trigger("mousewheel");},unmousewheel:function(fn){return this.unbind("mousewheel",fn);}});function handler(event){var orgEvent=event||window.event,args=[].slice.call(arguments,1),delta=0,returnValue=true,deltaX=0,deltaY=0;event=$.event.fix(orgEvent);event.type="mousewheel";if(event.wheelDelta){delta=event.wheelDelta/120;}
if(event.detail){delta=-event.detail/3;}
deltaY=delta;if(orgEvent.axis!==undefined&&orgEvent.axis===orgEvent.HORIZONTAL_AXIS){deltaY=0;deltaX=-1*delta;}
if(orgEvent.wheelDeltaY!==undefined){deltaY=orgEvent.wheelDeltaY/120;}
if(orgEvent.wheelDeltaX!==undefined){deltaX=-1*orgEvent.wheelDeltaX/120;}
args.unshift(event,delta,deltaX,deltaY);return $.event.handle.apply(this,args);}})(jQuery);(function($){$.extend($.fn,{validate:function(options){if(!this.length){options&&options.debug&&window.console&&console.warn("nothing selected, can't validate, returning nothing");return;}
var validator=$.data(this[0],'validator');if(validator){return validator;}
validator=new $.validator(options,this[0]);$.data(this[0],'validator',validator);if(validator.settings.onsubmit){this.find("input, button").filter(".cancel").click(function(){validator.cancelSubmit=true;});if(validator.settings.submitHandler){this.find("input, button").filter(":submit").click(function(){validator.submitButton=this;});}
this.submit(function(event){if(validator.settings.debug)
event.preventDefault();function handle(){if(validator.settings.submitHandler){if(validator.submitButton){var hidden=$("<input type='hidden'/>").attr("name",validator.submitButton.name).val(validator.submitButton.value).appendTo(validator.currentForm);}
validator.settings.submitHandler.call(validator,validator.currentForm);if(validator.submitButton){hidden.remove();}
return false;}
return true;}
if(validator.cancelSubmit){validator.cancelSubmit=false;return handle();}
if(validator.form()){if(validator.pendingRequest){validator.formSubmitted=true;return false;}
return handle();}else{validator.focusInvalid();return false;}});}
return validator;},valid:function(){if($(this[0]).is('form')){return this.validate().form();}else{var valid=true;var validator=$(this[0].form).validate();this.each(function(){valid&=validator.element(this);});return valid;}},removeAttrs:function(attributes){var result={},$element=this;$.each(attributes.split(/\s/),function(index,value){result[value]=$element.attr(value);$element.removeAttr(value);});return result;},rules:function(command,argument){var element=this[0];if(command){var settings=$.data(element.form,'validator').settings;var staticRules=settings.rules;var existingRules=$.validator.staticRules(element);switch(command){case"add":$.extend(existingRules,$.validator.normalizeRule(argument));staticRules[element.name]=existingRules;if(argument.messages)
settings.messages[element.name]=$.extend(settings.messages[element.name],argument.messages);break;case"remove":if(!argument){delete staticRules[element.name];return existingRules;}
var filtered={};$.each(argument.split(/\s/),function(index,method){filtered[method]=existingRules[method];delete existingRules[method];});return filtered;}}
var data=$.validator.normalizeRules($.extend({},$.validator.metadataRules(element),$.validator.classRules(element),$.validator.attributeRules(element),$.validator.staticRules(element)),element);if(data.required){var param=data.required;delete data.required;data=$.extend({required:param},data);}
return data;}});$.extend($.expr[":"],{blank:function(a){return!$.trim(""+a.value);},filled:function(a){return!!$.trim(""+a.value);},unchecked:function(a){return!a.checked;}});$.validator=function(options,form){this.settings=$.extend(true,{},$.validator.defaults,options);this.currentForm=form;this.init();};$.validator.format=function(source,params){if(arguments.length==1)
return function(){var args=$.makeArray(arguments);args.unshift(source);return $.validator.format.apply(this,args);};if(arguments.length>2&&params.constructor!=Array){params=$.makeArray(arguments).slice(1);}
if(params.constructor!=Array){params=[params];}
$.each(params,function(i,n){source=source.replace(new RegExp("\\{"+i+"\\}","g"),n);});return source;};$.extend($.validator,{defaults:{messages:{},groups:{},rules:{},errorClass:"error",validClass:"valid",errorElement:"label",focusInvalid:true,errorContainer:$([]),errorLabelContainer:$([]),onsubmit:true,ignore:[],ignoreTitle:false,onfocusin:function(element){this.lastActive=element;if(this.settings.focusCleanup&&!this.blockFocusCleanup){this.settings.unhighlight&&this.settings.unhighlight.call(this,element,this.settings.errorClass,this.settings.validClass);this.addWrapper(this.errorsFor(element)).hide();}},onfocusout:function(element){if(!this.checkable(element)&&(element.name in this.submitted||!this.optional(element))){this.element(element);}},onkeyup:function(element){if(element.name in this.submitted||element==this.lastElement){this.element(element);}},onclick:function(element){if(element.name in this.submitted)
this.element(element);else if(element.parentNode.name in this.submitted)
this.element(element.parentNode);},highlight:function(element,errorClass,validClass){$(element).addClass(errorClass).removeClass(validClass);},unhighlight:function(element,errorClass,validClass){$(element).removeClass(errorClass).addClass(validClass);}},setDefaults:function(settings){$.extend($.validator.defaults,settings);},messages:{required:"This field is required.",remote:"Please fix this field.",email:"Please enter a valid email address.",url:"Please enter a valid URL.",date:"Please enter a valid date.",dateISO:"Please enter a valid date (ISO).",number:"Please enter a valid number.",digits:"Please enter only digits.",creditcard:"Please enter a valid credit card number.",equalTo:"Please enter the same value again.",accept:"Please enter a value with a valid extension.",maxlength:$.validator.format("Please enter no more than {0} characters."),minlength:$.validator.format("Please enter at least {0} characters."),rangelength:$.validator.format("Please enter a value between {0} and {1} characters long."),range:$.validator.format("Please enter a value between {0} and {1}."),max:$.validator.format("Please enter a value less than or equal to {0}."),min:$.validator.format("Please enter a value greater than or equal to {0}.")},autoCreateRanges:false,prototype:{init:function(){this.labelContainer=$(this.settings.errorLabelContainer);this.errorContext=this.labelContainer.length&&this.labelContainer||$(this.currentForm);this.containers=$(this.settings.errorContainer).add(this.settings.errorLabelContainer);this.submitted={};this.valueCache={};this.pendingRequest=0;this.pending={};this.invalid={};this.reset();var groups=(this.groups={});$.each(this.settings.groups,function(key,value){$.each(value.split(/\s/),function(index,name){groups[name]=key;});});var rules=this.settings.rules;$.each(rules,function(key,value){rules[key]=$.validator.normalizeRule(value);});function delegate(event){var validator=$.data(this[0].form,"validator"),eventType="on"+event.type.replace(/^validate/,"");validator.settings[eventType]&&validator.settings[eventType].call(validator,this[0]);}
$(this.currentForm).validateDelegate(":text, :password, :file, select, textarea","focusin focusout keyup",delegate).validateDelegate(":radio, :checkbox, select, option","click",delegate);if(this.settings.invalidHandler)
$(this.currentForm).bind("invalid-form.validate",this.settings.invalidHandler);},form:function(){this.checkForm();$.extend(this.submitted,this.errorMap);this.invalid=$.extend({},this.errorMap);if(!this.valid())
$(this.currentForm).triggerHandler("invalid-form",[this]);this.showErrors();return this.valid();},checkForm:function(){this.prepareForm();for(var i=0,elements=(this.currentElements=this.elements());elements[i];i++){this.check(elements[i]);}
return this.valid();},element:function(element){element=this.clean(element);this.lastElement=element;this.prepareElement(element);this.currentElements=$(element);var result=this.check(element);if(result){delete this.invalid[element.name];}else{this.invalid[element.name]=true;}
if(!this.numberOfInvalids()){this.toHide=this.toHide.add(this.containers);}
this.showErrors();return result;},showErrors:function(errors){if(errors){$.extend(this.errorMap,errors);this.errorList=[];for(var name in errors){this.errorList.push({message:errors[name],element:this.findByName(name)[0]});}
this.successList=$.grep(this.successList,function(element){return!(element.name in errors);});}
this.settings.showErrors?this.settings.showErrors.call(this,this.errorMap,this.errorList):this.defaultShowErrors();},resetForm:function(){if($.fn.resetForm)
$(this.currentForm).resetForm();this.submitted={};this.prepareForm();this.hideErrors();this.elements().removeClass(this.settings.errorClass);},numberOfInvalids:function(){return this.objectLength(this.invalid);},objectLength:function(obj){var count=0;for(var i in obj)
count++;return count;},hideErrors:function(){this.addWrapper(this.toHide).hide();},valid:function(){return this.size()==0;},size:function(){return this.errorList.length;},focusInvalid:function(){if(this.settings.focusInvalid){try{$(this.findLastActive()||this.errorList.length&&this.errorList[0].element||[]).filter(":visible").focus().trigger("focusin");}catch(e){}}},findLastActive:function(){var lastActive=this.lastActive;return lastActive&&$.grep(this.errorList,function(n){return n.element.name==lastActive.name;}).length==1&&lastActive;},elements:function(){var validator=this,rulesCache={};return $([]).add(this.currentForm.elements).filter(":input").not(":submit, :reset, :image, [disabled]").not(this.settings.ignore).filter(function(){!this.name&&validator.settings.debug&&window.console&&console.error("%o has no name assigned",this);if(this.name in rulesCache||!validator.objectLength($(this).rules()))
return false;rulesCache[this.name]=true;return true;});},clean:function(selector){return $(selector)[0];},errors:function(){return $(this.settings.errorElement+"."+this.settings.errorClass,this.errorContext);},reset:function(){this.successList=[];this.errorList=[];this.errorMap={};this.toShow=$([]);this.toHide=$([]);this.currentElements=$([]);},prepareForm:function(){this.reset();this.toHide=this.errors().add(this.containers);},prepareElement:function(element){this.reset();this.toHide=this.errorsFor(element);},check:function(element){element=this.clean(element);if(this.checkable(element)){element=this.findByName(element.name).not(this.settings.ignore)[0];}
var rules=$(element).rules();var dependencyMismatch=false;for(var method in rules){var rule={method:method,parameters:rules[method]};try{var result=$.validator.methods[method].call(this,element.value.replace(/\r/g,""),element,rule.parameters);if(result=="dependency-mismatch"){dependencyMismatch=true;continue;}
dependencyMismatch=false;if(result=="pending"){this.toHide=this.toHide.not(this.errorsFor(element));return;}
if(!result){this.formatAndAdd(element,rule);return false;}}catch(e){this.settings.debug&&window.console&&console.log("exception occured when checking element "+element.id
+", check the '"+rule.method+"' method",e);throw e;}}
if(dependencyMismatch)
return;if(this.objectLength(rules))
this.successList.push(element);return true;},customMetaMessage:function(element,method){if(!$.metadata)
return;var meta=this.settings.meta?$(element).metadata()[this.settings.meta]:$(element).metadata();return meta&&meta.messages&&meta.messages[method];},customMessage:function(name,method){var m=this.settings.messages[name];return m&&(m.constructor==String?m:m[method]);},findDefined:function(){for(var i=0;i<arguments.length;i++){if(arguments[i]!==undefined)
return arguments[i];}
return undefined;},defaultMessage:function(element,method){return this.findDefined(this.customMessage(element.name,method),this.customMetaMessage(element,method),!this.settings.ignoreTitle&&element.title||undefined,$.validator.messages[method],"<strong>Warning: No message defined for "+element.name+"</strong>");},formatAndAdd:function(element,rule){var message=this.defaultMessage(element,rule.method),theregex=/\$?\{(\d+)\}/g;if(typeof message=="function"){message=message.call(this,rule.parameters,element);}else if(theregex.test(message)){message=jQuery.format(message.replace(theregex,'{$1}'),rule.parameters);}
this.errorList.push({message:message,element:element});this.errorMap[element.name]=message;this.submitted[element.name]=message;},addWrapper:function(toToggle){if(this.settings.wrapper)
toToggle=toToggle.add(toToggle.parent(this.settings.wrapper));return toToggle;},defaultShowErrors:function(){for(var i=0;this.errorList[i];i++){var error=this.errorList[i];this.settings.highlight&&this.settings.highlight.call(this,error.element,this.settings.errorClass,this.settings.validClass);this.showLabel(error.element,error.message);}
if(this.errorList.length){this.toShow=this.toShow.add(this.containers);}
if(this.settings.success){for(var i=0;this.successList[i];i++){this.showLabel(this.successList[i]);}}
if(this.settings.unhighlight){for(var i=0,elements=this.validElements();elements[i];i++){this.settings.unhighlight.call(this,elements[i],this.settings.errorClass,this.settings.validClass);}}
this.toHide=this.toHide.not(this.toShow);this.hideErrors();this.addWrapper(this.toShow).show();},validElements:function(){return this.currentElements.not(this.invalidElements());},invalidElements:function(){return $(this.errorList).map(function(){return this.element;});},showLabel:function(element,message){var label=this.errorsFor(element);if(label.length){label.removeClass().addClass(this.settings.errorClass);label.attr("generated")&&label.html(message);}else{label=$("<"+this.settings.errorElement+"/>").attr({"for":this.idOrName(element),generated:true}).addClass(this.settings.errorClass).html(message||"");if(this.settings.wrapper){label=label.hide().show().wrap("<"+this.settings.wrapper+"/>").parent();}
if(!this.labelContainer.append(label).length)
this.settings.errorPlacement?this.settings.errorPlacement(label,$(element)):label.insertAfter(element);}
if(!message&&this.settings.success){label.text("");typeof this.settings.success=="string"?label.addClass(this.settings.success):this.settings.success(label);}
this.toShow=this.toShow.add(label);},errorsFor:function(element){var name=this.idOrName(element);return this.errors().filter(function(){return $(this).attr('for')==name;});},idOrName:function(element){return this.groups[element.name]||(this.checkable(element)?element.name:element.id||element.name);},checkable:function(element){return/radio|checkbox/i.test(element.type);},findByName:function(name){var form=this.currentForm;return $(document.getElementsByName(name)).map(function(index,element){return element.form==form&&element.name==name&&element||null;});},getLength:function(value,element){switch(element.nodeName.toLowerCase()){case'select':return $("option:selected",element).length;case'input':if(this.checkable(element))
return this.findByName(element.name).filter(':checked').length;}
return value.length;},depend:function(param,element){return this.dependTypes[typeof param]?this.dependTypes[typeof param](param,element):true;},dependTypes:{"boolean":function(param,element){return param;},"string":function(param,element){return!!$(param,element.form).length;},"function":function(param,element){return param(element);}},optional:function(element){return!$.validator.methods.required.call(this,$.trim(element.value),element)&&"dependency-mismatch";},startRequest:function(element){if(!this.pending[element.name]){this.pendingRequest++;this.pending[element.name]=true;}},stopRequest:function(element,valid){this.pendingRequest--;if(this.pendingRequest<0)
this.pendingRequest=0;delete this.pending[element.name];if(valid&&this.pendingRequest==0&&this.formSubmitted&&this.form()){$(this.currentForm).submit();this.formSubmitted=false;}else if(!valid&&this.pendingRequest==0&&this.formSubmitted){$(this.currentForm).triggerHandler("invalid-form",[this]);this.formSubmitted=false;}},previousValue:function(element){return $.data(element,"previousValue")||$.data(element,"previousValue",{old:null,valid:true,message:this.defaultMessage(element,"remote")});}},classRuleSettings:{required:{required:true},email:{email:true},url:{url:true},date:{date:true},dateISO:{dateISO:true},dateDE:{dateDE:true},number:{number:true},numberDE:{numberDE:true},digits:{digits:true},creditcard:{creditcard:true}},addClassRules:function(className,rules){className.constructor==String?this.classRuleSettings[className]=rules:$.extend(this.classRuleSettings,className);},classRules:function(element){var rules={};var classes=$(element).attr('class');classes&&$.each(classes.split(' '),function(){if(this in $.validator.classRuleSettings){$.extend(rules,$.validator.classRuleSettings[this]);}});return rules;},attributeRules:function(element){var rules={};var $element=$(element);for(var method in $.validator.methods){var value=$element.attr(method);if(value){rules[method]=value;}}
if(rules.maxlength&&/-1|2147483647|524288/.test(rules.maxlength)){delete rules.maxlength;}
return rules;},metadataRules:function(element){if(!$.metadata)return{};var meta=$.data(element.form,'validator').settings.meta;return meta?$(element).metadata()[meta]:$(element).metadata();},staticRules:function(element){var rules={};var validator=$.data(element.form,'validator');if(validator.settings.rules){rules=$.validator.normalizeRule(validator.settings.rules[element.name])||{};}
return rules;},normalizeRules:function(rules,element){$.each(rules,function(prop,val){if(val===false){delete rules[prop];return;}
if(val.param||val.depends){var keepRule=true;switch(typeof val.depends){case"string":keepRule=!!$(val.depends,element.form).length;break;case"function":keepRule=val.depends.call(element,element);break;}
if(keepRule){rules[prop]=val.param!==undefined?val.param:true;}else{delete rules[prop];}}});$.each(rules,function(rule,parameter){rules[rule]=$.isFunction(parameter)?parameter(element):parameter;});$.each(['minlength','maxlength','min','max'],function(){if(rules[this]){rules[this]=Number(rules[this]);}});$.each(['rangelength','range'],function(){if(rules[this]){rules[this]=[Number(rules[this][0]),Number(rules[this][1])];}});if($.validator.autoCreateRanges){if(rules.min&&rules.max){rules.range=[rules.min,rules.max];delete rules.min;delete rules.max;}
if(rules.minlength&&rules.maxlength){rules.rangelength=[rules.minlength,rules.maxlength];delete rules.minlength;delete rules.maxlength;}}
if(rules.messages){delete rules.messages;}
return rules;},normalizeRule:function(data){if(typeof data=="string"){var transformed={};$.each(data.split(/\s/),function(){transformed[this]=true;});data=transformed;}
return data;},addMethod:function(name,method,message){$.validator.methods[name]=method;$.validator.messages[name]=message!=undefined?message:$.validator.messages[name];if(method.length<3){$.validator.addClassRules(name,$.validator.normalizeRule(name));}},methods:{required:function(value,element,param){if(!this.depend(param,element))
return"dependency-mismatch";switch(element.nodeName.toLowerCase()){case'select':var val=$(element).val();return val&&val.length>0;case'input':if(this.checkable(element))
return this.getLength(value,element)>0;default:return $.trim(value).length>0;}},remote:function(value,element,param){if(this.optional(element))
return"dependency-mismatch";var previous=this.previousValue(element);if(!this.settings.messages[element.name])
this.settings.messages[element.name]={};previous.originalMessage=this.settings.messages[element.name].remote;this.settings.messages[element.name].remote=previous.message;param=typeof param=="string"&&{url:param}||param;if(this.pending[element.name]){return"pending";}
if(previous.old===value){return previous.valid;}
previous.old=value;var validator=this;this.startRequest(element);var data={};data[element.name]=value;$.ajax($.extend(true,{url:param,mode:"abort",port:"validate"+element.name,dataType:"json",data:data,success:function(response){validator.settings.messages[element.name].remote=previous.originalMessage;var valid=response===true;if(valid){var submitted=validator.formSubmitted;validator.prepareElement(element);validator.formSubmitted=submitted;validator.successList.push(element);validator.showErrors();}else{var errors={};var message=response||validator.defaultMessage(element,"remote");errors[element.name]=previous.message=$.isFunction(message)?message(value):message;validator.showErrors(errors);}
previous.valid=valid;validator.stopRequest(element,valid);}},param));return"pending";},minlength:function(value,element,param){return this.optional(element)||this.getLength($.trim(value),element)>=param;},maxlength:function(value,element,param){return this.optional(element)||this.getLength($.trim(value),element)<=param;},rangelength:function(value,element,param){var length=this.getLength($.trim(value),element);return this.optional(element)||(length>=param[0]&&length<=param[1]);},min:function(value,element,param){return this.optional(element)||value>=param;},max:function(value,element,param){return this.optional(element)||value<=param;},range:function(value,element,param){return this.optional(element)||(value>=param[0]&&value<=param[1]);},email:function(value,element){return this.optional(element)||/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i.test(value);},url:function(value,element){return this.optional(element)||/^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(value);},date:function(value,element){return this.optional(element)||!/Invalid|NaN/.test(new Date(value));},dateISO:function(value,element){return this.optional(element)||/^\d{4}[\/-]\d{1,2}[\/-]\d{1,2}$/.test(value);},number:function(value,element){return this.optional(element)||/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value);},digits:function(value,element){return this.optional(element)||/^\d+$/.test(value);},creditcard:function(value,element){if(this.optional(element))
return"dependency-mismatch";if(/[^0-9-]+/.test(value))
return false;var nCheck=0,nDigit=0,bEven=false;value=value.replace(/\D/g,"");for(var n=value.length-1;n>=0;n--){var cDigit=value.charAt(n);var nDigit=parseInt(cDigit,10);if(bEven){if((nDigit*=2)>9)
nDigit-=9;}
nCheck+=nDigit;bEven=!bEven;}
return(nCheck%10)==0;},accept:function(value,element,param){param=typeof param=="string"?param.replace(/,/g,'|'):"png|jpe?g|gif";return this.optional(element)||value.match(new RegExp(".("+param+")$","i"));},equalTo:function(value,element,param){var target=$(param).unbind(".validate-equalTo").bind("blur.validate-equalTo",function(){$(element).valid();});return value==target.val();}}});$.format=$.validator.format;})(jQuery);;(function($){var pendingRequests={};if($.ajaxPrefilter){$.ajaxPrefilter(function(settings,_,xhr){var port=settings.port;if(settings.mode=="abort"){if(pendingRequests[port]){pendingRequests[port].abort();}
pendingRequests[port]=xhr;}});}else{var ajax=$.ajax;$.ajax=function(settings){var mode=("mode"in settings?settings:$.ajaxSettings).mode,port=("port"in settings?settings:$.ajaxSettings).port;if(mode=="abort"){if(pendingRequests[port]){pendingRequests[port].abort();}
return(pendingRequests[port]=ajax.apply(this,arguments));}
return ajax.apply(this,arguments);};}})(jQuery);;(function($){if(!jQuery.event.special.focusin&&!jQuery.event.special.focusout&&document.addEventListener){$.each({focus:'focusin',blur:'focusout'},function(original,fix){$.event.special[fix]={setup:function(){this.addEventListener(original,handler,true);},teardown:function(){this.removeEventListener(original,handler,true);},handler:function(e){arguments[0]=$.event.fix(e);arguments[0].type=fix;return $.event.handle.apply(this,arguments);}};function handler(e){e=$.event.fix(e);e.type=fix;return $.event.handle.call(this,e);}});};$.extend($.fn,{validateDelegate:function(delegate,type,handler){return this.bind(type,function(event){var target=$(event.target);if(target.is(delegate)){return handler.apply(target,arguments);}});}});})(jQuery);jQuery.fn.validate=function(options){if(!this.length){options&&options.debug&&window.console&&console.warn("nothing selected, can't validate, returning nothing");return;}
var validator=$.data(this[0],'validator');if(validator){return validator;}
validator=new $.validator(options,this[0]);$.data(this[0],'validator',validator);if(validator.settings.onsubmit){this.find("input, button").filter(".cancel").click(function(){validator.cancelSubmit=true;});if(validator.settings.submitHandler){this.find("input, button").filter(":submit").click(function(){validator.submitButton=this;});}
this.submit(function(event){validator.clickedSubmit=true;if(validator.settings.debug)
event.preventDefault();function handle(){if(validator.settings.submitHandler){if(validator.submitButton){var hidden=$("<input type='hidden'/>").attr("name",validator.submitButton.name).val(validator.submitButton.value).appendTo(validator.currentForm);}
validator.settings.submitHandler.call(validator,validator.currentForm);if(validator.submitButton){hidden.remove();}
return false;}
return true;}
if(validator.cancelSubmit){validator.cancelSubmit=false;return handle();}
if(validator.form()){if(validator.pendingRequest){validator.formSubmitted=true;return false;}
return handle();}else{validator.focusInvalid();return false;}});}
return validator;}
jQuery.validator.prototype.element=function(element){element=this.clean(element);this.lastElement=element;this.prepareElement(element);this.currentElements=$(element);var result=this.check(element);if(result){delete this.invalid[element.name];}else{this.invalid[element.name]=true;}
if(!this.numberOfInvalids()){this.toHide=this.toHide.add(this.containers);if(!this.displayOK&&this.clickedSubmit){$("#formErrors").after($("<div id='validForm'>Errors have been corrected, please resubmit the form</div>"));this.displayOK=true;}}else{$("#validForm").remove();this.displayOK=false;}
this.showErrors();return result;}
jQuery.validator.prototype.defaultShowErrors=function(){for(var i=0;this.errorList[i];i++){var error=this.errorList[i];this.settings.highlight&&this.settings.highlight.call(this,error.element,this.settings.errorClass,this.settings.validClass);this.showLabel(error.element,error.message);}
if(this.errorList.length){this.toShow=this.toShow.add(this.containers);}
if(this.settings.success){for(var i=0;this.successList[i];i++){this.showLabel(this.successList[i]);}}
if(this.settings.unhighlight){for(var i=0,elements=this.validElements();elements[i];i++){this.settings.unhighlight.call(this,elements[i],this.settings.errorClass,this.settings.validClass);}}
this.toHide=this.toHide.not(this.toShow);this.hideErrors();this.addWrapper(this.toShow).show();};(function($){$.each({focus:'focusin',blur:'focusout'},function(original,fix){$.event.special[fix]={setup:function(){if($.browser.msie)return false;this.addEventListener(original,$.event.special[fix].handler,true);},teardown:function(){if($.browser.msie)return false;this.removeEventListener(original,$.event.special[fix].handler,true);},handler:function(e){arguments[0]=$.event.fix(e);arguments[0].type=fix;return $.event.handle.apply(this,arguments);}};});$.extend($.fn,{delegate:function(type,delegate,handler){return this.bind(type,function(event){var target=$(event.target);if(target.is(delegate)){return handler.apply(target,arguments);}});},triggerEvent:function(type,target){return this.triggerHandler(type,[jQuery.event.fix({type:type,target:target})]);}})})(jQuery);;(function($){$.ui=$.ui||{};$.fn.extend({accordion:function(options,data){var args=Array.prototype.slice.call(arguments,1);return this.each(function(){if(typeof options=="string"){var accordion=$.data(this,"ui-accordion");accordion[options].apply(accordion,args);}else if(!$(this).is(".ui-accordion"))
$.data(this,"ui-accordion",new $.ui.accordion(this,options));});},activate:function(index){return this.accordion("activate",index);}});$.ui.accordion=function(container,options){this.options=options=$.extend({},$.ui.accordion.defaults,options);this.element=container;$(container).addClass("ui-accordion");if(options.navigation){var current=$(container).find("a").filter(options.navigationFilter);if(current.length){if(current.filter(options.header).length){options.active=current;}else{options.active=current.parent().parent().prev();current.addClass("current");}}}
options.headers=$(container).find(options.header);options.active=findActive(options.headers,options.active);if(options.fillSpace){var maxHeight=$(container).parent().height();options.headers.each(function(){maxHeight-=$(this).outerHeight();});var maxPadding=0;options.headers.next().each(function(){maxPadding=Math.max(maxPadding,$(this).innerHeight()-$(this).height());}).height(maxHeight-maxPadding);}else if(options.autoheight){var maxHeight=0;options.headers.next().each(function(){maxHeight=Math.max(maxHeight,$(this).outerHeight());}).height(maxHeight);}
options.headers.not(options.active||"").next().hide();options.active.parent().andSelf().addClass(options.selectedClass);if(options.event)
$(container).bind((options.event||"")+".ui-accordion",clickHandler);};$.ui.accordion.prototype={activate:function(index){clickHandler.call(this.element,{target:findActive(this.options.headers,index)[0]});},enable:function(){this.options.disabled=false;},disable:function(){this.options.disabled=true;},destroy:function(){this.options.headers.next().css("display","");if(this.options.fillSpace||this.options.autoheight){this.options.headers.next().css("height","");}
$.removeData(this.element,"ui-accordion");$(this.element).removeClass("ui-accordion").unbind(".ui-accordion");}};function scopeCallback(callback,scope){return function(){return callback.apply(scope,arguments);};};function completed(cancel){if(!$.data(this,"ui-accordion"))
return;var instance=$.data(this,"ui-accordion");var options=instance.options;options.running=cancel?0:--options.running;if(options.running)
return;if(options.clearStyle){options.toShow.add(options.toHide).css({height:"",overflow:"",display:"block"});}
$(this).triggerHandler("change.ui-accordion",[options.data],options.change);}
function toggle(toShow,toHide,data,clickedActive,down){var options=$.data(this,"ui-accordion").options;options.toShow=toShow;options.toHide=toHide;options.data=data;var complete=scopeCallback(completed,this);options.running=toHide.size()==0?toShow.size():toHide.size();if(options.animated){if(!options.alwaysOpen&&clickedActive){$.ui.accordion.animations[options.animated]({toShow:jQuery([]),toHide:toHide,complete:complete,down:down,autoheight:options.autoheight});}else{$.ui.accordion.animations[options.animated]({toShow:toShow,toHide:toHide,complete:complete,down:down,autoheight:options.autoheight});}}else{if(!options.alwaysOpen&&clickedActive){toShow.toggle();}else{toHide.hide();toShow.show();}
complete(true);}}
function clickHandler(event){var options=$.data(this,"ui-accordion").options;if(options.disabled)
return false;if(!event.target&&!options.alwaysOpen){options.active.parent().andSelf().toggleClass(options.selectedClass);var toHide=options.active.next(),data={instance:this,options:options,newHeader:jQuery([]),oldHeader:options.active,newContent:jQuery([]),oldContent:toHide},toShow=options.active=$([]);toggle.call(this,toShow,toHide,data);return false;}
var clicked=$(event.target);if(clicked.parents(options.header).length)
while(!clicked.is(options.header))
clicked=clicked.parent();var clickedActive=clicked[0]==options.active[0];if(options.running||(options.alwaysOpen&&clickedActive))
return false;if(!clicked.is(options.header))
return;options.active.parent().andSelf().toggleClass(options.selectedClass);if(!clickedActive){clicked.parent().andSelf().addClass(options.selectedClass);}
var toShow=clicked.next(),toHide=options.active.next(),data={instance:this,options:options,newHeader:clicked,oldHeader:options.active,newContent:toShow,oldContent:toHide},down=options.headers.index(options.active[0])>options.headers.index(clicked[0]);options.active=clickedActive?$([]):clicked;toggle.call(this,toShow,toHide,data,clickedActive,down);return false;};function findActive(headers,selector){return selector!=undefined?typeof selector=="number"?headers.filter(":eq("+selector+")"):headers.not(headers.not(selector)):selector===false?$([]):headers.filter(":eq(0)");}
$.extend($.ui.accordion,{defaults:{selectedClass:"selected",alwaysOpen:true,animated:'slide',event:"click",header:"a",autoheight:true,running:0,navigationFilter:function(){return this.href.toLowerCase()==location.href.toLowerCase();}},animations:{slide:function(options,additions){options=$.extend({easing:"swing",duration:300},options,additions);if(!options.toHide.size()){options.toShow.animate({height:"show"},options);return;}
var hideHeight=options.toHide.height(),showHeight=options.toShow.height(),difference=showHeight/hideHeight;options.toShow.css({height:0,overflow:'hidden'}).show();options.toHide.filter(":hidden").each(options.complete).end().filter(":visible").animate({height:"hide"},{step:function(now){var current=(hideHeight-now)*difference;if(navigator.userAgent.indexOf('MSIE')!=-1&&(document.documentMode==8||document.documentMode==7))
current+=difference;options.toShow.height(current);},duration:options.duration,easing:options.easing,complete:function(){if(!options.autoheight){if(options.toShow[0]!=null){if(options.toShow[0].className=='officeSummSection'){options.toShow.css("height",'auto');}else{options.toShow.css("height",showHeight);}}else{options.toShow.css("height",'auto');}
options.toShow.css("display","block");}
options.complete();}});},bounceslide:function(options){this.slide(options,{easing:options.down?"bounceout":"swing",duration:options.down?1000:200});},easeslide:function(options){this.slide(options,{easing:"easeinout",duration:700});}}});})(jQuery);jQuery.autocomplete=function(input,options){var me=this;var $input=$(input).attr("autocomplete","off");if(options.inputClass)$input.addClass(options.inputClass);var results=document.createElement("div");var $results=$(results);$results.hide().addClass(options.resultsClass).css("position","absolute");if(options.width>0)$results.css("width",options.width);$("body").append(results);input.autocompleter=me;var timeout=null;var prev="";var active=-1;var cache={};var keyb=false;var hasFocus=false;var lastKeyPressCode=null;function flushCache(){cache={};cache.data={};cache.length=0;};flushCache();if(options.data!=null){var sFirstChar="",stMatchSets={},row=[];if(typeof options.url!="string")options.cacheLength=1;for(var i=0;i<options.data.length;i++){row=((typeof options.data[i]=="string")?[options.data[i]]:options.data[i]);if(row[0].length>0){sFirstChar=row[0].substring(0,1).toLowerCase();if(!stMatchSets[sFirstChar])stMatchSets[sFirstChar]=[];stMatchSets[sFirstChar].push(row);}}
for(var k in stMatchSets){options.cacheLength++;addToCache(k,stMatchSets[k]);}}
$input.keydown(function(e){lastKeyPressCode=e.keyCode;switch(e.keyCode){case 38:e.preventDefault();moveSelect(-1);break;case 40:e.preventDefault();moveSelect(1);break;case 9:case 13:if(selectCurrent()){$input.get(0).blur();e.preventDefault();}
break;default:active=-1;if(timeout)clearTimeout(timeout);timeout=setTimeout(function(){onChange();},options.delay);break;}}).focus(function(){hasFocus=true;}).blur(function(){hasFocus=false;hideResults();});hideResultsNow();function onChange(){if(lastKeyPressCode==46||(lastKeyPressCode>8&&lastKeyPressCode<32))return $results.hide();var v=$input.val();if(v==prev)return;prev=v;if(v.length>=options.minChars){$input.addClass(options.loadingClass);requestData(v);}else{$input.removeClass(options.loadingClass);$results.hide();}};function moveSelect(step){var lis=$("li",results);if(!lis)return;active+=step;if(active<0){active=0;}else if(active>=lis.size()){active=lis.size()-1;}
lis.removeClass("ac_over");$(lis[active]).addClass("ac_over");};function selectCurrent(){var li=$("li.ac_over",results)[0];if(!li){var $li=$("li",results);if(options.selectOnly){if($li.length==1)li=$li[0];}else if(options.selectFirst){li=$li[0];}}
if(li){selectItem(li);return true;}else{return false;}};function selectItem(li){if(!li){li=document.createElement("li");li.extra=[];li.selectValue="";}
var v=$.trim(li.selectValue?li.selectValue:li.innerHTML);input.lastSelected=v;prev=v;$results.html("");$input.val(v);hideResultsNow();if(options.onItemSelect)setTimeout(function(){options.onItemSelect(li)},1);};function createSelection(start,end){var field=$input.get(0);if(field.createTextRange){var selRange=field.createTextRange();selRange.collapse(true);selRange.moveStart("character",start);selRange.moveEnd("character",end);selRange.select();}else if(field.setSelectionRange){field.setSelectionRange(start,end);}else{if(field.selectionStart){field.selectionStart=start;field.selectionEnd=end;}}
field.focus();};function autoFill(sValue){if(lastKeyPressCode!=8){$input.val($input.val()+sValue.substring(prev.length));createSelection(prev.length,sValue.length);}};function showResults(){var pos=findPos(input);var iWidth=(options.width>0)?options.width:$input.width();$results.css({width:parseInt(iWidth)+"px",top:(pos.y+input.offsetHeight)+"px",left:pos.x+"px"}).show();};function hideResults(){if(timeout)clearTimeout(timeout);timeout=setTimeout(hideResultsNow,200);};function hideResultsNow(){if(timeout)clearTimeout(timeout);$input.removeClass(options.loadingClass);if($results.is(":visible")){$results.hide();}
if(options.mustMatch){var v=$input.val();if(v!=input.lastSelected){selectItem(null);}}};function receiveData(q,data){if(data){$input.removeClass(options.loadingClass);results.innerHTML="";if(!hasFocus)return hideResultsNow();if(data.length==0){$results.html("<div><h4>No Matching Terms Found</h4></div>");}
if($.browser.msie){$results.append(document.createElement('iframe'));}
results.appendChild(dataToDom(data));showResults();}else{$input.removeClass(options.loadingClass);$results.html("<h4>No Matching Terms Found</h4>");showResults();}};function parseData(data){if(!data)return null;var parsed=[];var rows=data.split(options.lineSeparator);for(var i=0;i<rows.length;i++){var row=$.trim(rows[i]);if(row){parsed[parsed.length]=row.split(options.cellSeparator);}}
return parsed;};function dataToDom(data){var div=document.createElement("div");var h4=document.createElement("h4");h4.innerHTML="Word Suggestions";var ul=document.createElement("ul");var num=data.length;if((options.maxItemsToShow>0)&&(options.maxItemsToShow<num))num=options.maxItemsToShow;for(var i=0;i<num;i++){var row=data[i];if(!row)continue;var li=document.createElement("li");if(options.formatItem){li.innerHTML=options.formatItem(row,i,num);li.selectValue=row[0];}else{li.innerHTML=row[0];li.selectValue=row[0];}
var extra=null;if(row.length>1){extra=[];for(var j=1;j<row.length;j++){extra[extra.length]=row[j];}}
li.extra=extra;ul.appendChild(li);div.appendChild(h4);div.appendChild(ul);$(li).hover(function(){$("li",ul).removeClass("ac_over");$(this).addClass("ac_over");active=$("li",ul).indexOf($(this).get(0));},function(){$(this).removeClass("ac_over");}).click(function(e){e.preventDefault();e.stopPropagation();selectItem(this)});}
return div;};function requestData(q){if(!options.matchCase)q=q.toLowerCase();var data=options.cacheLength?loadFromCache(q):null;if(data){receiveData(q,data);}else if((typeof options.url=="string")&&(options.url.length>0)){$.get(makeUrl(q),function(data){data=parseData(data);addToCache(q,data);receiveData(q,data);});}else{$input.removeClass(options.loadingClass);}};function makeUrl(q){var url=options.url+"?q="+encodeURI(q);for(var i in options.extraParams){url+="&"+i+"="+encodeURI(options.extraParams[i]);}
return url;};function loadFromCache(q){if(!q)return null;if(cache.data[q])return cache.data[q];if(options.matchSubset){for(var i=q.length-1;i>=options.minChars;i--){var qs=q.substr(0,i);var c=cache.data[qs];if(c){var csub=[];for(var j=0;j<c.length;j++){var x=c[j];var x0=x[0];if(matchSubset(x0,q)){csub[csub.length]=x;}}
return csub;}}}
return null;};function matchSubset(s,sub){if(!options.matchCase)s=s.toLowerCase();var i=s.indexOf(sub);if(i==-1)return false;return i==0||options.matchContains;};this.flushCache=function(){flushCache();};this.setExtraParams=function(p){options.extraParams=p;};this.findValue=function(){var q=$input.val();if(!options.matchCase)q=q.toLowerCase();var data=options.cacheLength?loadFromCache(q):null;if(data){findValueCallback(q,data);}else if((typeof options.url=="string")&&(options.url.length>0)){$.get(makeUrl(q),function(data){data=parseData(data)
addToCache(q,data);findValueCallback(q,data);});}else{findValueCallback(q,null);}}
function findValueCallback(q,data){if(data)$input.removeClass(options.loadingClass);var num=(data)?data.length:0;var li=null;for(var i=0;i<num;i++){var row=data[i];if(row[0].toLowerCase()==q.toLowerCase()){li=document.createElement("li");if(options.formatItem){li.innerHTML=options.formatItem(row,i,num);li.selectValue=row[0];}else{li.innerHTML=row[0];li.selectValue=row[0];}
var extra=null;if(row.length>1){extra=[];for(var j=1;j<row.length;j++){extra[extra.length]=row[j];}}
li.extra=extra;}}
if(options.onFindValue)setTimeout(function(){options.onFindValue(li)},1);}
function addToCache(q,data){if(!data||!q||!options.cacheLength)return;if(!cache.length||cache.length>options.cacheLength){flushCache();cache.length++;}else if(!cache[q]){cache.length++;}
cache.data[q]=data;};function findPos(obj){var curleft=obj.offsetLeft||0;var curtop=obj.offsetTop||0;while(obj=obj.offsetParent){curleft+=obj.offsetLeft
curtop+=obj.offsetTop}
return{x:curleft,y:curtop};}}
jQuery.fn.autocomplete=function(url,options,data){options=options||{};options.url=url;options.data=((typeof data=="object")&&(data.constructor==Array))?data:null;options.inputClass=options.inputClass||"ac_input";options.resultsClass=options.resultsClass||"ac_results";options.lineSeparator=options.lineSeparator||"\n";options.cellSeparator=options.cellSeparator||"|";options.minChars=options.minChars||1;options.delay=options.delay||400;options.matchCase=options.matchCase||0;options.matchSubset=options.matchSubset||1;options.matchContains=options.matchContains||0;options.cacheLength=options.cacheLength||1;options.mustMatch=options.mustMatch||0;options.extraParams=options.extraParams||{};options.loadingClass=options.loadingClass||"ac_loading";options.selectFirst=options.selectFirst||false;options.selectOnly=options.selectOnly||false;options.maxItemsToShow=options.maxItemsToShow||-1;options.autoFill=options.autoFill||false;options.width=parseInt(options.width,10)||0;this.each(function(){var input=this;new jQuery.autocomplete(input,options);});return this;}
jQuery.fn.autocompleteArray=function(data,options){return this.autocomplete(null,options,data);}
jQuery.fn.indexOf=function(e){for(var i=0;i<this.length;i++){if(this[i]==e)return i;}
return-1;};(function(jQuery){jQuery.each(['backgroundColor','borderBottomColor','borderLeftColor','borderRightColor','borderTopColor','color','outlineColor'],function(i,attr){jQuery.fx.step[attr]=function(fx){if(fx.state==0){fx.start=getColor(fx.elem,attr);fx.end=getRGB(fx.end);}
fx.elem.style[attr]="rgb("+[Math.max(Math.min(parseInt((fx.pos*(fx.end[0]-fx.start[0]))+fx.start[0]),255),0),Math.max(Math.min(parseInt((fx.pos*(fx.end[1]-fx.start[1]))+fx.start[1]),255),0),Math.max(Math.min(parseInt((fx.pos*(fx.end[2]-fx.start[2]))+fx.start[2]),255),0)].join(",")+")";}});function getRGB(color){var result;if(color&&color.constructor==Array&&color.length==3)
return color;if(result=/rgb\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*\)/.exec(color))
return[parseInt(result[1]),parseInt(result[2]),parseInt(result[3])];if(result=/rgb\(\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*\)/.exec(color))
return[parseFloat(result[1])*2.55,parseFloat(result[2])*2.55,parseFloat(result[3])*2.55];if(result=/#([a-fA-F0-9]{2})([a-fA-F0-9]{2})([a-fA-F0-9]{2})/.exec(color))
return[parseInt(result[1],16),parseInt(result[2],16),parseInt(result[3],16)];if(result=/#([a-fA-F0-9])([a-fA-F0-9])([a-fA-F0-9])/.exec(color))
return[parseInt(result[1]+result[1],16),parseInt(result[2]+result[2],16),parseInt(result[3]+result[3],16)];return colors[jQuery.trim(color).toLowerCase()];}
function getColor(elem,attr){var color;do{color=jQuery.curCSS(elem,attr);if(color!=''&&color!='transparent'||jQuery.nodeName(elem,"body"))
break;attr="backgroundColor";}while(elem=elem.parentNode);return getRGB(color);};var colors={aqua:[0,255,255],azure:[240,255,255],beige:[245,245,220],black:[0,0,0],blue:[0,0,255],brown:[165,42,42],cyan:[0,255,255],darkblue:[0,0,139],darkcyan:[0,139,139],darkgrey:[169,169,169],darkgreen:[0,100,0],darkkhaki:[189,183,107],darkmagenta:[139,0,139],darkolivegreen:[85,107,47],darkorange:[255,140,0],darkorchid:[153,50,204],darkred:[139,0,0],darksalmon:[233,150,122],darkviolet:[148,0,211],fuchsia:[255,0,255],gold:[255,215,0],green:[0,128,0],indigo:[75,0,130],khaki:[240,230,140],lightblue:[173,216,230],lightcyan:[224,255,255],lightgreen:[144,238,144],lightgrey:[211,211,211],lightpink:[255,182,193],lightyellow:[255,255,224],lime:[0,255,0],magenta:[255,0,255],maroon:[128,0,0],navy:[0,0,128],olive:[128,128,0],orange:[255,165,0],pink:[255,192,203],purple:[128,0,128],violet:[128,0,128],red:[255,0,0],silver:[192,192,192],white:[255,255,255],yellow:[255,255,0]};})(jQuery);(function($){$.fn.hoverIntent=function(f,g){var cfg={sensitivity:7,interval:100,timeout:0};cfg=$.extend(cfg,g?{over:f,out:g}:f);var cX,cY,pX,pY;var track=function(ev){cX=ev.pageX;cY=ev.pageY;};var compare=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);if((Math.abs(pX-cX)+Math.abs(pY-cY))<cfg.sensitivity){$(ob).unbind("mousemove",track);ob.hoverIntent_s=1;return cfg.over.apply(ob,[ev]);}else{pX=cX;pY=cY;ob.hoverIntent_t=setTimeout(function(){compare(ev,ob);},cfg.interval);}};var delay=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);ob.hoverIntent_s=0;return cfg.out.apply(ob,[ev]);};var handleHover=function(e){var p=(e.type=="mouseover"?e.fromElement:e.toElement)||e.relatedTarget;while(p&&p!=this){try{p=p.parentNode;}catch(e){p=this;}}
if(p==this){return false;}
var ev=jQuery.extend({},e);var ob=this;if(ob.hoverIntent_t){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);}
if(e.type=="mouseover"){pX=ev.pageX;pY=ev.pageY;$(ob).bind("mousemove",track);if(ob.hoverIntent_s!=1){ob.hoverIntent_t=setTimeout(function(){compare(ev,ob);},cfg.interval);}}else{$(ob).unbind("mousemove",track);if(ob.hoverIntent_s==1){ob.hoverIntent_t=setTimeout(function(){delay(ev,ob);},cfg.timeout);}}};return this.mouseover(handleHover).mouseout(handleHover);};})(jQuery);;(function($){$.fn.superfish=function(op){var sf=$.fn.superfish,c=sf.c,$arrow=$(['<span class="',c.arrowClass,'"> &nbsp;</span>'].join('')),over=function(){var $$=$(this),menu=getMenu($$);clearTimeout(menu.sfTimer);$$.showSuperfishUl().siblings().hideSuperfishUl();},out=function(){var $$=$(this),menu=getMenu($$),o=sf.op;clearTimeout(menu.sfTimer);menu.sfTimer=setTimeout(function(){o.retainPath=($.inArray($$[0],o.$path)>-1);$$.hideSuperfishUl();if(o.$path.length&&$$.parents(['li.',o.hoverClass].join('')).length<1){over.call(o.$path);}},o.delay);},getMenu=function($menu){var menu=$menu.parents(['ul.',c.menuClass,':first'].join(''))[0];sf.op=sf.o[menu.serial];return menu;},addArrow=function($a){$a.addClass(c.anchorClass).append($arrow.clone());};return this.each(function(){var s=this.serial=sf.o.length;var o=$.extend({},sf.defaults,op);o.$path=$('li.'+o.pathClass,this).slice(0,o.pathLevels).each(function(){$(this).addClass([o.hoverClass,c.bcClass].join(' ')).filter('li:has(ul)').removeClass(o.pathClass);});sf.o[s]=sf.op=o;if(sf.op.click){$('li:has(ul)',this).click(over).hover(function(){},out).each(function(){if(o.autoArrows)addArrow($('>a:first-child',this));}).not('.'+c.bcClass).hideSuperfishUl();}else{$('li:has(ul)',this)[($.fn.hoverIntent&&!o.disableHI)?'hoverIntent':'hover'](over,out).each(function(){if(o.autoArrows)addArrow($('>a:first-child',this));}).not('.'+c.bcClass).hideSuperfishUl();}
var $a=$('a',this);$a.each(function(i){var $li=$a.eq(i).parents('li');$a.eq(i).focus(function(){over.call($li);}).blur(function(){out.call($li);});});o.onInit.call(this);}).each(function(){var menuClasses=[c.menuClass];if(sf.op.dropShadows&&!($.browser.msie&&$.browser.version<7))menuClasses.push(c.shadowClass);$(this).addClass(menuClasses.join(' '));});};var sf=$.fn.superfish;sf.o=[];sf.op={};sf.IE7fix=function(){var o=sf.op;if($.browser.msie&&$.browser.version>6&&o.dropShadows&&o.animation.opacity!=undefined)
this.toggleClass(sf.c.shadowClass+'-off');};sf.c={bcClass:'sf-breadcrumb',menuClass:'sf-js-enabled',anchorClass:'sf-with-ul',arrowClass:'sf-sub-indicator',shadowClass:'sf-shadow'};sf.defaults={hoverClass:'sfHover',pathClass:'overideThisToUse',pathLevels:1,delay:0,animation:{opacity:'show'},speed:'fast',autoArrows:true,dropShadows:true,disableHI:true,onInit:function(){},onBeforeShow:function(){},onShow:function(){},onHide:function(){}};$.fn.extend({hideSuperfishUl:function(){var o=sf.op,not=(o.retainPath===true)?o.$path:'';o.retainPath=false;var $ul=$(['li.',o.hoverClass].join(''),this).add(this).not(not).removeClass(o.hoverClass).find('>ul').hide().css('display','none');o.onHide.call($ul);return this;},showSuperfishUl:function(){var o=sf.op,sh=sf.c.shadowClass+'-off',$ul=this.addClass(o.hoverClass).find('>ul:hidden').css('display','block');sf.IE7fix.call($ul);o.onBeforeShow.call($ul);return this;}});})(jQuery);
var XHTMLcontent="";jQuery.fn.getContent=function(){return XHTMLcontent;}
if(top!=self){top.location.replace(self.location.href);}
function loadGotham(){var gothamLink=document.createElement('link');gothamLink.rel='stylesheet';gothamLink.type='text/css';gothamLink.href='../../../../public.libertymutual-cdn.com/MediaAssets/fonts/238524/A1421A9D5414391E8.css'/*tpa=https://cloud.typography.com/6818492/728006/css/fonts.css*/;document.getElementsByTagName('head')[0].appendChild(gothamLink);}
$(document).ready(function(){if($('body').innerWidth()<998){$('#pageShadow').width(1000);}
if(typeof initializeSiteCatalyst=='function'){initializeSiteCatalyst();}
if(typeof registerScrollCheckOnAddThis=='function'){registerScrollCheckOnAddThis();}
$(window).resize(function(){if($('body').innerWidth()<998){$('#pageShadow').width(1000);}else{$('#pageShadow').width($('body').innerWidth())}
$(".floatingModuleErrors").jqmHide();$('.floatingModuleErrorsHighlight').removeClass('error').removeClass('floatingModuleErrorsHighlight');});window.log=function(string)
{if(typeof console=='object'&&appJSGlobals.isJavascriptDebugMode=="ON")
{console.log(string);}}
$("a.printIcon").click(function(){var myTitle=$(this).parent().parent().parent();var myList=$(this).parent().parent().parent().next();var myParagraph=$(this).parent().parent().parent().next().next("p");$(".jsList, .jsInstTitle").addClass("printOff");$(myTitle).removeClass("printOff");$(myList).removeClass("printOff");$("#jsInstructionsContent h3, #jsInstructionsContent ul, #jsInstructionsContent p").addClass("printOff");$(myParagraph).removeClass("printOff");window.print();return false;});$("#printAll").click(function(){$(".printOff").removeClass("printOff");window.print();return false;});$('img').each(function(n){if(this.src!="")
{if(this.height==0||this.height==1)
{$(this).removeAttr("height");}
if(this.width==0||this.width==1)
{$(this).removeAttr("width");}}});if($("#searchItem").length<=0)
{$("#homeLogin").attr("id",'homeLoginMove');}
else
{$("#homeLogin").attr("id",'homeLogin');}
$("ul#zoomControl, div#mapContainer button").css("display","block");$("https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/p.zoom").css("display","inline");$("#printerFriendlyLink").click(function(){window.print();return false;});$("form").attr("autocomplete","off");$("a[target=_popup]").click(function(){window.open($(this).attr("href"),"popup","status=0,toolbar=0,scrollbars=1,menubar=0,location=0,width=800,height=600");return false;});if($("#centerModSet").is("div")){var centerModules=$("#centerModSet").children("div");var moduleHeights=[];for(i=0;i<centerModules.length;i++){moduleHeights.push($(centerModules[i]).height());}
moduleHeights.sort(function(a,b){return a-b});$("#centerModSet").height(moduleHeights[moduleHeights.length-1]+10);}
var locationHref=location.href;var varIndex=locationHref.indexOf("?");if(varIndex!=-1)
{locationHref=locationHref.substring(0,varIndex);}
$("#topNav ul li a").each(function(){var linkHref=this.href;varIndex=linkHref.indexOf("?");if(varIndex!=-1)
{linkHref=linkHref.substring(0,varIndex);}
if(locationHref.indexOf(linkHref)>=0){$(this).attr("id","navLinkDisabled");if(linkHref==locationHref){$(this).attr("href","#");$(this).click(function(){return false;});}}});$("#searchOp ul li a.homeLink").each(function(){var linkHref=this.href;varIndex=linkHref.indexOf("?");if(varIndex!=-1)
{linkHref=linkHref.substring(0,varIndex);}
if(linkHref==locationHref){$(this).attr("id","navLinkDisabled");$(this).attr("href","#");$(this).click(function(){return false;});}});$("#locAgentZip").bind('keypress',function(event){return submitOnEnter(event,'goButton');});$("#searchForString").bind('keypress',function(event){return submitOnEnter(event,'goButton');});$("#searchQuery").bind('focusin',function(event){if($(this).hasClass("searchBlur"))
{$(this).val('');$(this).removeClass("searchBlur");$(this).addClass("searchFocus");}});$("#searchQuery").bind('blur',function(event){if($(this).hasClass("searchFocus"))
{if($(this).val()=='')
{$(this).removeClass("searchFocus");$(this).addClass("searchBlur");$(this).val('search');}}});$("#searchQuery").bind('keypress',function(event){return submitOnEnter(event,'searchButton');});$("#searchButton").bind('click',function(event){if($("#searchQuery").hasClass("searchBlur"))
{$("#searchQuery").val('');}});$("#quoteZipCode").bind('keypress',function(event){return submitOnEnter(event,'getQuoteButton');});$("#locClaimsOfficeZip").bind('keypress',function(event){return submitOnEnter(event,'claimOfficeZipGoButton');});$("a.sessionClear").click(function(){$.cookie("JSESSIONID",null,{path:'/',domain:'.libertymutual.com'});return true;});$("a.browseToButton").append("<span class='browse'></span>");if($.browser.msie&&(parseInt($.browser.version)==6)||(parseInt($.browser.version)==7)){$("#contentArea a.browseTo").append("<span class='browseLink'></span>").removeClass("browseTo");}});$.fn.enable=function(){return this.each(function(){if(typeof this.disabled!="undefined")
this.disabled=false;});}
$.fn.disable=function(){return this.each(function(){if(typeof this.disabled!="undefined"){this.disabled=true;}});}
$.fn.listHandlers=function(events,outputFunction){return this.each(function(i){var elem=this,dEvents=$(this).data('events');if(!dEvents){return;}
$.each(dEvents,function(name,handler){if((new RegExp('^('+(events==='*'?'.+':events.replace(',','|').replace(/^on/i,''))+')$','i')).test(name)){$.each(handler,function(i,handler){outputFunction(elem,'\n'+i+': ['+name+'] : '+handler);});}});});};function stripLeadingTrailingSpace(str){return str.replace(/^\s+|\s+$/g,'');}
function submitOnEnter(e,commandButtonId,commandButton)
{var keycode;if(window.event)
keycode=window.event.keyCode;else if(e)
keycode=e.which;else
return true;if(keycode==13)
{if(commandButton!=null)
{commandButton.click();return false;}
else if(commandButtonId)
{var cb="#"+commandButtonId;$(cb).click();return false;}}
else
{return true;}}
function transformForFlash(element)
{if(element.length==0)return null;var reTag=/(<\/?\w+)([^>]*>)/g;var reAttr=/(\w+=)((['"])[\s\S]*\3|[^\s>]+)/g;str=element.innerHTML;function fixAttr($0,$1,$2,$3){if($3)return $1.toLowerCase()+$2;else return $1.toLowerCase()+'"'+$2+'"'}
function fixTag($0,$1,$2){return $1.toLowerCase()+$2.replace(reAttr,fixAttr);}
str=str.replace(reTag,fixTag);str=fixTargetTag(str,"_blank");str=fixTargetTag(str,"_self");str=fixTargetTag(str,"_popup");var reSelfClosing=new RegExp("\<(area|base|basefont|br|hr|img|input)(.*?)>","g");str=str.replace(reSelfClosing,"<$1$2/>");str=str.replace(/<\/li>|<\/dt>/g,"");str=str.replace(/\s*<li([^>]*)>/g,"<\/li><li$1>");str=str.replace(/<\/ul>/g,"<\/li><\/ul>");str=str.replace(/<ul([^>]*)>\s*<\/li>/g,"<ul$1>");str=str.replace(/\s*<dd([^>]*)>/g,"<\/dt><dd$1>");return str;}
function fixTargetTag(str,attrValue)
{var oldStr="target="+attrValue;var newStr="target=\""+attrValue+"\"";return replaceAll(str,oldStr,newStr);}
function replaceAll(Source,stringToFind,stringToReplace)
{var temp=Source;var index=temp.indexOf(stringToFind);while(index!=-1)
{temp=temp.replace(stringToFind,stringToReplace);index=temp.indexOf(stringToFind);}
return temp;}
function stripPageName(uri)
{var p=uri;if(uri.substring(uri.length-1,uri.length)=='/')
{p=uri.substring(0,uri.length-1);}
var loc=p.lastIndexOf("/");if(loc>=0&&loc+1<=p.length)
{p=p.substring(loc+1);}
return p;}
function stripDomainName(uri)
{var p=uri;var loc=p.indexOf("/MediaBin");if(loc>=0&&loc+1<=p.length)
{p=p.substring(0,loc);}
return p;}
function stripFileExtensionFromURL(uri)
{var p=uri;if(uri.substring(uri.length-1,uri.length)=='.')
{p=uri.substring(0,uri.length-1);}
var loc=p.lastIndexOf(".");if(loc>=0&&loc+1<=p.length)
{p=p.substring(0,loc);}
return p;}
function stripFileFromURL(uri)
{var p=uri;if(uri.substring(uri.length-1,uri.length)=='/')
{p=uri.substring(0,uri.length-1);}
var loc=p.lastIndexOf("/");if(loc>=0&&loc+1<=p.length)
{p=p.substring(0,loc);}
return p;}
function prepareForFlash(divIdPrefix,instantiationMethod,qualifyingClassName)
{var customClass=divIdPrefix;if(qualifyingClassName!=null)
{var selector='body[class*='+qualifyingClassName+']'+' div[id*='+divIdPrefix+']';}
else
{var selector='div[id*='+divIdPrefix+']';}
$(selector).each(function(intIndex){$(this).addClass(customClass);flashId=$(this).attr("id");flashResource=$(this).find(".flashResource").text();flashHeight=$(this).find(".flashHeight").text();flashWidth=$(this).find(".flashWidth").text();if(swfobject.hasFlashPlayerVersion(appJSGlobals.requiredFlashPlayerVersion))
{var instMethod=instantiationMethod+"(flashId, flashResource, flashHeight, flashWidth);";eval(instMethod);}
else
{$("div.upgradeYourFlash").css("display","block");}});}
function instantiateFlash(flashId,flashResource,flashHeight,flashWidth)
{var variables={"lm_env":appJSGlobals.mediaCenterEnvironment};var parameters={"allowScriptAccess":"always","quality":"high","wmode":"transparent"};var attributes={};swfobject.embedSWF(flashResource,flashId,flashWidth,flashHeight,appJSGlobals.requiredFlashPlayerVersion,"",variables,parameters,attributes);}
function instantiateFlashSenior(flashId,flashResource,flashHeight,flashWidth)
{myAffRes=$(".affinityRestriction").text();mybase=$(".flashSrcBase").text();mymovie=$(".flashSrcNoExtension").text();myDomain=stripDomainName(flashResource)
mybase=stripFileFromURL(flashResource)+'/';mymovie=stripFileExtensionFromURL(flashResource);var variables={"aff_res":myAffRes,"lm_env":appJSGlobals.mediaCenterEnvironment,"server_path":appJSGlobals.gamesDataServices+"/","locale":"en_US"};var parameters={"allowScriptAccess":"always","quality":"high","wmode":"transparent","play":"true","loop":"true","align":"top","quality":"high","devicefont":"false","id":"SeniorDriversGameFlash","name":"SeniorDriversGameFlash","allowScriptAccess":"always","allowFullScreen":"true","align":"t","scale":"noscale","menu":"true","type":"application/x-shockwave-flash","codebase":"http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,0,0","pluginspage":"http://www.adobe.com/go/getflashplayer","base":mybase,"movie":mymovie};var attributes={};swfobject.embedSWF(flashResource,flashId,flashWidth,flashHeight,appJSGlobals.requiredFlashPlayerVersion,"",variables,parameters,attributes);}
function areCookiesEnabled()
{var r=false;if(navigator.cookieEnabled)
{r=true;}
return r;}
function adjustSkipLinks()
{var skipDiv=document.getElementById('skipmenu');if(skipDiv!=null)
{if(document.getElementById('moduleArea'))
{}
else
{var skipLink=document.getElementById('skipToRightModule');if(skipLink!=null)
{skipDiv.removeChild(skipLink);}}
if(document.getElementById('componentArea'))
{}
else
{var skipLink=document.getElementById('skipTocomponentArea');if(skipLink!=null)
{skipDiv.removeChild(skipLink);}}
if(document.getElementById('contentArea'))
{}
else
{var skipLink=document.getElementById('skipToContent');if(skipLink!=null)
{skipDiv.removeChild(skipLink);}}}}
$(document).ready(function(){if(!areCookiesEnabled())
{$("#mainCookiesMessage").css("display","block");}});jQuery.extend(jQuery.fn,{createPromoSlideshow:function(options){if(!this.length){options&&options.debug&&window.console&&console.warn("nothing selected, can't ctreate slideshow, returning nothing");return;}
var promoSlideshow=jQuery.data(this[0],'promoSlideshow');if(promoSlideshow){return promoSlideshow;}
promoSlideshow=new jQuery.promoSlideshow(options,this[0]);jQuery.data(this[0],'promoSlideshow',promoSlideshow);return promoSlideshow;}});$.promoSlideshow=function(options,container){this.settings=jQuery.extend({},jQuery.promoSlideshow.defaults,options);this.create(container,this);};jQuery.extend(jQuery.promoSlideshow,{defaults:{fx:'fade',speed:200,timeout:5000,height:266,pager:'#promonav',manualTrump:true,fit:true,requeueOnImageNotLoaded:false,pagerAnchorBuilder:function(idx,slide){return'<a name="promoAnchor_'+idx+'" href="#">&nbsp;</a>';},cssBefore:{height:260},before:function(e){if($(this).html().indexOf("PromoGreen")>=0)
{$('#promonav a').each(function(){$(this).addClass('darkPromoButton');});}
else
{$('#promonav a').each(function(){$(this).removeClass('darkPromoButton');});}},after:function onAfter(curr,next,opts,fwd){$(this).css("height","260px");$('#promonav a').each(function(){if((navigator.userAgent.indexOf('Safari')!=-1&&navigator.userAgent.indexOf('Chrome')==-1&&((navigator.userAgent.match(/iPad/i))||(navigator.userAgent.match(/iPhone/i))))||(navigator.userAgent.match(/Android/i)))
{if($(this).attr("class")=="activeSlide")
{$(this).css('background-image','url(../../../images/light-bg_selected.png)');}
else
{var backgroundImage=$(this).css("background-image");if(backgroundImage.indexOf("Unknown_83_filename"/*tpa=https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/light-bg_hover.png*/)>=0)
{$(this).css('background-image','url(../../../images/light-bg_hover.png)');}
else
{$(this).css('background-image','url(../../../images/light-bg_normal.png)');}}}
else
{}});}},setDefaults:function(settings){jQuery.extend(jQuery.promoSlideshow.defaults,settings);},prototype:{create:function(container,myThis){$(container).after('<div id="promonav">').cycle(myThis.settings);}}});jQuery.extend(jQuery.fn,{createCenterModuleSlider:function(options){if(!this.length){options&&options.debug&&window.console&&console.warn("nothing selected, can't ctreate slideshow, returning nothing");return;}
var slider=jQuery.data(this[0],'centerModuleSlider');if(slider){return slider;}
slider=new jQuery.centerModuleSlider(options,this[0]);jQuery.data(this[0],'centerModuleSlider',slider);return slider;}});$.centerModuleSlider=function(options,container){this.settings=jQuery.extend({},jQuery.centerModuleSlider.defaults,options);this.create(container,this);};jQuery.extend(jQuery.centerModuleSlider,{defaults:{fit:true,requeueOnImageNotLoaded:false,pagerAnchorBuilder:function(idx,slide){return'<a href="#">&nbsp;</a>';}},setDefaults:function(settings){jQuery.extend(jQuery.centerModuleSlider.defaults,settings);},prototype:{create:function(container,myThis){$(container).after('<div id="sliderPager">').cycle(myThis.settings);}}});jQuery().ready(function(){prepareForFlash('flashTextImbedded','instantiateFlash');prepareForFlash('flashHeaderSectionModule','instantiateFlash');});$(window).load(function(){$('#pageContainer').find('a[tabindex=1]').focus();});jQuery().ready(function()
{$("span.firstLevelLink").click(function(){$(this).css('color','#333');});$("span.firstLevelLink").mouseover(function(){$(this).css('color','#333');});$("span.firstLevelLink").hover(function(){},function(){$(this).css('color','#FFF');});$("li.firstLevel").mouseover(function(){$(this).find('span.firstLevelLink').css('color','#333');});$("li.firstLevel").hover(function(){},function(){$(this).find('span.firstLevelLink').css('color','#FFF');});$('ul.gl-menu').superfish({delay:0,speed:'fast',click:true,autoArrows:false,dropShadows:false});$('ul.mm-menu').superfish({delay:0,click:false,autoArrows:false,dropShadows:false});$('#pageContainer').mouseup(function(){$('#megaMenuContainer li.sfHover a').blur();});var setNavigation=(function()
{var helper={};var PAGE_WIDTH=962;var MEGA_MENU_MARGIN=12;helper.setGlobalMenu=setGlobalMenu;function setGlobalMenu()
{$('#globalContainer li.firstLevel button').mousedown(function()
{$(this).addClass("ieActiveShift");});$('#globalContainer li.firstLevel button').mouseleave(function()
{$(this).removeClass("ieActiveShift");});$('#globalContainer li.firstLevel button').mouseup(function()
{$(this).removeClass("ieActiveShift");});}
helper.setMegaMenu=setMegaMenu;function setMegaMenu()
{var menuWidth=$('.mm-menu').width();var totalWidth=0;var i=0;var numElements=0;var widthArray=new Array;var linkWidth=0,linkLeftPadding=0,linkRightPadding=0,arrowWidth=0,arrowLeftPadding=0,arrowRightPadding=0;$('#megaMenuContainer li.firstLevel').each(function()
{var newItem=new Object();linkWidth=$(this).find('span.firstLevelLink').width();linkLeftPadding=$(this).find('span.firstLevelLink').css('margin-left').replace(/[A-Za-z$-]/g,"");linkRightPadding=$(this).find('span.firstLevelLink').css('margin-right').replace(/[A-Za-z$-]/g,"");arrowWidth=$(this).find('span.megaArrow').width();arrowLeftPadding=$(this).find('span.megaArrow').css('margin-left').replace(/[A-Za-z$-]/g,"");arrowRightPadding=$(this).find('span.megaArrow').css('margin-right').replace(/[A-Za-z$-]/g,"");var itemWidth=(parseInt(linkWidth)+parseInt(linkLeftPadding)+parseInt(linkRightPadding)+parseInt(arrowWidth)+parseInt(arrowLeftPadding)+parseInt(arrowRightPadding));newItem.itemW=itemWidth;totalWidth+=itemWidth;numElements++;widthArray.push(newItem);});if(totalWidth<=menuWidth)
{var diff=Math.floor((menuWidth-totalWidth)/numElements);var combinedWidth=0;$('#megaMenuContainer li.firstLevel').each(function()
{var itemObject=widthArray[i];var newItemWidth=(parseInt(itemObject.itemW)+diff);if((i+1)===numElements)
{combinedWidth=combinedWidth+((numElements)*2);$(this).css('width',(menuWidth-combinedWidth-2)+'px');}
else
{$(this).css('width',(newItemWidth)+'px');}
combinedWidth+=newItemWidth;i++;});}
setSubMenuLocation();}
function setSubMenuLocation()
{$('#megaMenuContainer li.firstLevel').each(function()
{var parentPosition=$(this).position().left;var itemWidth=$(this).find('div.subMenu').width();if(itemWidth>$(this).width())
{if((parentPosition+itemWidth)<(PAGE_WIDTH-2))
{}
else if(((parentPosition-itemWidth)+$(this).width())>0)
{$(this).find('div.subMenu').css("left",Math.round(((itemWidth-$(this).width())*-1)+2)+'px');}
else if((parentPosition+itemWidth)>=PAGE_WIDTH)
{var combinedValue=((parentPosition+itemWidth)-PAGE_WIDTH)*-1;$(this).find('div.subMenu').css("left",combinedValue-MEGA_MENU_MARGIN+'px');}}});}
$("#megaMenuCover").css("display","none");helper.setLeftNav=setLeftNav;function setLeftNav()
{if($("#localNavAccordion li.selected h4.selected + .fourthLevelLinks").children().size()===0)
{$("#localNavAccordion li.selected h4.selected").css("border-bottom","none");}
if($("#localNavAccordion li.selected h3.selected + .fourthLevelLinks").children().size()===0)
{$("#localNavAccordion li.selected h3.selected").css("border-bottom","none");}}
return helper;})();setNavigation.setGlobalMenu();setNavigation.setMegaMenu();setNavigation.setLeftNav();var siteMapFound=false;$("#footer .footerContents #footerTextSection a").each(function()
{var linkText=$(this).html();if(linkText.toLowerCase()=="site map")
{siteMapFound=true;return false;}});$("#pageFooter .footerContents #footerTextSection a").each(function()
{var linkText=$(this).html();if(linkText.toLowerCase()=="site map")
{siteMapFound=true;return false;}});if(!siteMapFound)
{$("#fullSiteMapLink").addClass('hideSiteMapLink');}});$(function(){if(typeof cdaPageVars=='object')
{oOobj3.Metrics.custom.webID=cdaPageVars.repWebID;if(cdaPageVars.affinityWebID!=null&&cdaPageVars.affinityWebID.length==10)
{oOobj3.Metrics.custom.webID=cdaPageVars.affinityWebID;}}
if(typeof oOobj3=='object')
{$(".onlineOpinionClickable").bind('click',function(event){return oOobj3.OnClick();});}});jQuery().ready(function(){prepareForFlash('flashStandardRightModule','instantiateFlash');});jQuery().ready(function(){prepareForFlash('flashTwoColumnContentModule','instantiateFlashSenior','twoColumnContentPageSenior');});jQuery().ready(function(){prepareForFlash('flashSecondaryContentModule','instantiateFlash','defaultSecondaryFlash');});jQuery().ready(function(){prepareForFlash('flashSecondaryContentModule','instantiateFlashSenior','seniorSecondaryFlash');});$(document).ready(function(){if($.browser.msie==true&&($('#affinityBottomWrapContainer')!=null||$('#affinityBottomWrapContainerIE6')!=null))
{var ieVerContainer;if(parseInt($.browser.version.charAt(0))=="6"){ieVerContainer="#affinityBottomWrapContainerIE6";}
else{ieVerContainer="#affinityBottomWrapContainer";}
var wrapHeight=$(ieVerContainer+' div.affinityBottomWrap').height();var sepHeight=$(ieVerContainer+' div.bottomWrapSeperatorContainer').height();var logoHeight=$(ieVerContainer+' div.affinityLogoContainer img.affinityLogo').height();var textBannerHeight=$(ieVerContainer+' div.wrapBannerContainer').height();var m=0;if(logoHeight>1){m=(wrapHeight-logoHeight)/2;$('div.affinityBottomWrap div.affinityLogoContainer').css('margin-top',m);m=(wrapHeight-textBannerHeight)/2;$('div.wrapBannerContainer').css('margin-top',m);}
else{m=(wrapHeight-textBannerHeight)/2;if(m>12){$('div.affinityBottomWrap div.affinityLogoContainer').css('margin-top',12);}
else{$('div.affinityBottomWrap div.affinityLogoContainer').css('margin-top',m);}
$('div.wrapBannerContainer').css('margin-top',m);}
m=(wrapHeight-sepHeight)/2;$('div.bottomWrapSeperatorContainer').css('margin-top',m);$('div.bottomWrapSeperatorContainer').css('margin-bottom',m);if(parseInt($.browser.version.charAt(0))=="6"){if($('#affinityBottomWrapContainerIE6').css('visibility')!="hidden"){$('#affinityBottomWrapContainerIE6 div.affinityBottomWrap').css('visibility','visible');}}
else{if($('#affinityBottomWrapContainer').css('visibility')!="hidden"){$('#affinityBottomWrapContainer div.affinityBottomWrap').css('visibility','visible');}}}
var isKindleFire=false;var isAndroid=false;function checkVersion(){var agent=window.navigator.userAgent,start=agent.indexOf('OS ');var androidUser=window.navigator.userAgent.toLowerCase();if((agent.indexOf('iPhone')>-1||agent.indexOf('iPad')>-1)&&start>-1){return window.Number(agent.substr(start+3,3).replace('_','.'));}
else if(androidUser.indexOf("android")!=-1){isAndroid=true;return(parseFloat(androidUser.slice(androidUser.indexOf("android")+8)));}
return 0;}
ver=checkVersion();if(navigator.platform=='iPad'||navigator.platform=='iPhone'||navigator.platform=='iPod'){if(ver<5){$(".affinityWrapTopBorderShadow").css("visibility","hidden");$('#affinityBottomWrapContainer').css('position','absolute');$(window).scroll(function(e){$('#affinityBottomWrapContainer').css('bottom',"-"+window.pageYOffset+'px');});}}
if(isAndroid==true){$(".affinityBottomWrapInner").css("background","url(../images/affinity_banner_wrap_shadow_v4.png)");$("#utilityLinksContainer").css("background"," url('../../images/test.png') repeat-x bottom");$("#utilityLinksContainerBg").css("background","url('../../images/test.png') repeat-x bottom");if(ver>=3.0){}else{}}
if($('#affinityBottomWrapContainer')!=null){var h=$('#affinityBottomWrapContainer').height();if(h>90){$('div.affinityBottomWrapPageMargin').height(h);}}});jQuery.cookie=function(name,value,options){if(typeof value!='undefined'){options=options||{};if(value===null){value='';options.expires=-1;}
var expires='';if(options.expires&&(typeof options.expires=='number'||options.expires.toUTCString)){var date;if(typeof options.expires=='number'){date=new Date();date.setTime(date.getTime()+(options.expires*24*60*60*1000));}else{date=options.expires;}
expires='; expires='+date.toUTCString();}
var path=options.path?'; path='+(options.path):'';var domain=options.domain?'; domain='+(options.domain):'';var secure=options.secure?'; secure':'';document.cookie=[name,'=',encodeURIComponent(value),expires,path,domain,secure].join('');}else{var cookieValue=null;if(document.cookie&&document.cookie!=''){var cookies=document.cookie.split(';');for(var i=0;i<cookies.length;i++){var cookie=jQuery.trim(cookies[i]);if(cookie.substring(0,name.length+1)==(name+'=')){cookieValue=decodeURIComponent(cookie.substring(name.length+1));break;}}}
return cookieValue;}};
jQuery().ready(function(){$('#modulesAccordion h3').addClass('moduleClosed');$('#modulesAccordion').accordion({autoheight:false,header:".slideHeader",active:".activate"});$('#modulesAccordion').accordion();$('#modulesAccordion ul li.selected div.moduleContent').css('display','block');if($('#module_getYourQuote li.zipCode').next().attr('class')=='PinContainer'){$('#module_getYourQuote .getQuoteForm').css('padding-bottom','8px');if(navigator.userAgent.indexOf('MSIE')!=-1&&document.documentMode==8){$('#modulesAccordion #module_getYourQuote form').css('height','176px');}}
$('#modulesAccordion h3').keypress(function(event){if(event.keyCode==13){event.preventDefault();$(this).click();return false;}});$("a.manageYourPolicyIcon").keypress(function(event){var calcIndex=0;$("#modulesAccordion div.moduleClosed").each(function(index){if($("div.moduleClosed:eq("+index+") a.manageYourPolicyIcon").length>0){calcIndex=index;}});if($("div.moduleClosed:eq("+calcIndex+")").hasClass("selected")){var popupLocation=$('#modulesAccordion h3 div a').attr('href');window.open(popupLocation,"popup","status=0,toolbar=0,scrollbars=1,menubar=0,location=0,width=800,height=600");}});});$(document).ready(function(){jQuery.extend(jQuery.fn,{createCenterModuleSlider:function(options){if(!this.length){options&&options.debug&&window.console&&console.warn("nothing selected, can't ctreate slideshow, returning nothing");return;}
var slider=jQuery.data(this[0],'centerModuleSlider');if(slider){return slider;}
slider=new jQuery.centerModuleSlider(options,this[0]);jQuery.data(this[0],'centerModuleSlider',slider);return slider;}});$.centerModuleSlider=function(options,container){this.settings=jQuery.extend({},jQuery.centerModuleSlider.defaults,options);this.create(container,this);};jQuery.extend(jQuery.centerModuleSlider,{prototype:{create:function(container,myThis){if(container.id=='sliderImageContainer'){$("#sliderPagination").append('<div id="sliderPager">');$('#sliderPager').before('<div id="slideLeftButton">');$('#sliderPager').after('<div id="slideRightButton">');$(container).cycle(myThis.settings);}else{$(container).after('<div id="sliderPager">').cycle(myThis.settings);}}}});if($('#sliderCMContainer').size()>0)
{var centerModuleSlider=$('#centerModuleSlider #sliderCMContainer').createCenterModuleSlider({fx:'fade',prev:'#slideLeftButton',next:'#slideRightButton',speed:200,height:'auto',pager:'#sliderPager',timeout:0,nowrap:false,fit:true,requeueOnImageNotLoaded:false,pagerAnchorBuilder:function(idx,slide){return'<span><a href="#">&nbsp;</a></span>';}});}
if($('#sliderImageContainer').size()>0)
{var centerModuleSlider=$('#centerModuleSlider #sliderImageContainer').createCenterModuleSlider({fx:'fade',prev:'#slideLeftButton',next:'#slideRightButton',before:function(curSlide,nextSlide,opt,fwFlag){loadImageUrls(nextSlide,fwFlag);},speed:200,height:'auto',pager:'#sliderPager',timeout:0,nowrap:false,fit:true,requeueOnImageNotLoaded:false,pagerAnchorBuilder:function(idx,slide){return'<span><a href="#">&nbsp;</a></span>';}});}
$('#colapseReps span').click(function(){$('#carouselContainer').removeClass('hidden');$('#noCarouselContainer').addClass('hidden');});$('#expandReps span').click(function(){$('#noCarouselContainer').removeClass('hidden');$('#carouselContainer').addClass('hidden');});$('#slideLeftButton').click(function(){$("#socialLikeContainer div span").css("height","20px");$("#socialLikeContainer div span").css("width","75px");$("#socialLikeContainer div span iframe").css("height","20px");$("#socialLikeContainer div span iframe").css("width","75px");});$('#slideRightButton').click(function(){$("#socialLikeContainer div span").css("height","20px");$("#socialLikeContainer div span").css("width","75px");$("#socialLikeContainer div span iframe").css("height","20px");$("#socialLikeContainer div span iframe").css("width","75px");});$('#expandReps span').click(function(){$("#socialLikeContainer div span").css("height","20px");$("#socialLikeContainer div span").css("width","75px");$("#socialLikeContainer div span iframe").css("height","20px");$("#socialLikeContainer div span iframe").css("width","75px");});function loadImageUrls(nextSlide){if($('#sliderImageContainer').size()>0){var childId=nextSlide.children[0].id;childId=childId.substring(14,childId.length);var imageUrl=$('#sliderImageContainer #imageCenterMod'+childId+' .imageModulePicture').attr('src');var dataUrl=$('#sliderImageContainer #imageCenterMod'+childId+' .imageModulePicture').attr('data-imageurl');if(dataUrl!='null'&&imageUrl!=dataUrl){$('#sliderImageContainer #imageCenterMod'+childId+' .imageModulePicture').attr('src',dataUrl);}}}});var validators=[];var initializeFloatingModuleErrors=function(theId){$("#"+theId).jqm({modal:false,overlay:1,onShow:floatingModuleErrorsOpen,onHide:floatingModuleErrorsClose,toTop:true});}
var showFloatingModuleErrors=function(validator,theId,theErrorList,formId){var errorOffsetRight=19;var errorOffsetLeft=19;if(theErrorList.length>0){var newHtml="";for(var i=0;i<theErrorList.length;i++){newHtml=newHtml+"<p>"+theErrorList[i].message+"</p>";$(theErrorList[i].element).addClass('error').addClass('floatingModuleErrorsHighlight');if(i==0){$(theErrorList[i].element).addClass('firstErroredField');}}
$("#"+theId+" .floatingModuleErrorsText").html(newHtml);var errorPositionTop=$(theErrorList[0].element).offset();var errorOffset=errorOffsetRight;var errorImg="<img src='../../../images/floatingModuleErrorsArrowLeft.png'/*tpa=https://www.libertymutual.com/images/floatingModuleErrorsArrowLeft.png*/ class='floatingModuleErrorsArrowLeft'/>";if($("#"+formId).closest("div#moduleArea").length){var moduleArea=$("#"+formId).closest("div#moduleArea");var moduleAreaOffset=moduleArea.offset().left;var moduleAreaRightOffset=moduleAreaOffset+moduleArea.width();if($("#"+formId).closest(".bodyCenterContainer").length){var bodyCenterContainer=$("#"+formId).closest(".bodyCenterContainer");var bodyCenterContainerOffset=bodyCenterContainer.offset().left;var bodyCenterContainerRightOffset=bodyCenterContainerOffset+bodyCenterContainer.width();var floatingErrorWidth=$("#"+theId).width();if((bodyCenterContainerRightOffset-moduleAreaRightOffset)<(floatingErrorWidth+20)){errorOffset=($("#"+formId).width()+floatingErrorWidth+errorOffsetLeft)*-1;errorImg="<img src='../../../images/floatingModuleErrorsArrowRight.png'/*tpa=https://www.libertymutual.com/images/floatingModuleErrorsArrowRight.png*/ class='floatingModuleErrorsArrowRight'/>";}}}
errorPosition=$("#"+formId).offset().left+$("#"+formId).width()+errorOffset;$("#"+theId).css("left",Math.floor(errorPosition));$("#"+theId).css("top",errorPositionTop.top-20);$("div.floatingModuleErrorsArrow").html(errorImg);if(errorPositionTop.top>25){$("#"+theId).jqmShow();}
$("#"+theId+" .floatingModuleErrorsCloseButtonX").click(function(e){closeFloatingModuleErrors(validator,theId,theErrorList);});}
else{closeFloatingModuleErrors(validator,theId,theErrorList);}}
var closeFloatingModuleErrors=function(validator,theId,theErrorList){$('.floatingModuleErrorsHighlight').removeClass('error').removeClass('floatingModuleErrorsHighlight');$("#"+theId+" .floatingModuleErrorsText").html("");$("#"+theId).jqmHide();if(validator!=null){validator.resetForm();}}
var floatingModuleErrorsOpen=function(hash){hash.w.show();}
var floatingModuleErrorsClose=function(hash){hash.o.remove();hash.w.hide();for(var i=0;i<validators.length;i++){validators[i].resetForm();}}
jQuery().ready(function(){jQuery.validator.addMethod("validPin",function(value,element){if(!/[^a-zA-Z0-9]/.test(value)&&/.{0,20}/.test(value)){return true;}
else{return false;}});jQuery.validator.addMethod("validKroger",function(value,element){var isValid=true;if(/[^a-zA-Z0-9]/.test(value)||value.length<10||value.length>13||(value.length==0||value=='Kroger Plus Card # or ALT ID:'))
{isValid=false;}
return isValid},"Please enter a valid Kroger Plus Card # or ALT ID");jQuery.validator.addMethod("validShopYourWay",function(value,element){var isValid=true;if(value=='Membership # (no spaces):'||value.length==0){isValid=true;}else if(!/\D+/.test(value)&&/.{16}/.test(value)&&/^7081/.test(value))
{isValid=true;}
else{isValid=false;}
return isValid;},"Please enter a valid Membership # (no spaces)");jQuery.validator.addMethod("validHiltonHonors",function(value,element){var isValid=true;if(value=='Hilton HHonors Number:'||value.length==0){isValid=true;}else if(!/\D+/.test(value)&&/.{9}/.test(value)){var MemNum=value;var first8=parseInt(MemNum.substring(0,8));var last1=parseInt(MemNum.substring(8));var resultValue=(first8-(Math.floor(first8/9)*9))+1;if(resultValue!==last1)
{isValid=false;}}else{isValid=false;}
return isValid;},"Please enter a valid Hilton HHonors Number");jQuery.validator.addMethod("zipLabelCleared",function(value,element){if(value.toLowerCase()=="zip code"){return false;}
else{return true;}});jQuery.validator.addMethod("validQuoteID",function(value,element){if(value.length==0)
{return true;}
else if(value.length==8)
{return((/\d{8,}/i).test(value));}
else if(value.length==15)
{for(i=0;i<value.length;i++)
{if(i==3||i==9)
{if(value.charAt(i)!="-")
{return false;}}}
return true;}
else
{return false;}});if($("#module_activeCatastrophies").length>0){var validateActiveCatastrophes=$("form[id$=activeCatastropheForm]").validate({highlight:function(element,errorClass){$("#activeCatastrophesErrors").show()
$(element).addClass(errorClass);},unhighlight:function(element,errorClass){$(element).removeClass(errorClass);},errorLabelContainer:"#activeCatastrophesErrors",wrapper:"p",rules:{catastropheState:{required:true}},messages:{catastropheState:{required:"You must select a state in order to proceed"}}});validators.push(validateActiveCatastrophes);$("li#module_activeCatastrophies div.moduleContent .goButton").mousedown(function(){$("li#module_activeCatastrophies form").submit();});}});$(function(){var formIdRoot="getQuoteForm";var forms=$("form[id^="+formIdRoot+"]").each(function(n){var suffix=this.id.substring(formIdRoot.length);var myErrors=formIdRoot+suffix+"Errors";var myRules=new Object();var myMinValue=0;var myMaxValue=99999;var myMinMessage="Must be valid Zip Code";if($(".0000018072"+suffix).length>0)
{myMinValue=43001;myMaxValue=45999;myMinMessage="Must be valid Ohio Zip code";}
myRules['quoteZipCode'+suffix]={zipLabelCleared:true,required:true,minlength:5,maxlength:5,min:myMinValue,max:myMaxValue,number:true};myRules['quoteType'+suffix]={required:true};myRules['quotePinNumber'+suffix]={validPin:true};$.validator.addClassRules("0000018072"+suffix,{validKroger:true});$.validator.addClassRules("0000018095"+suffix,{validShopYourWay:true});$.validator.addClassRules("0000015796"+suffix,{validHiltonHonors:true});$(".0000018072"+suffix).attr("maxlength",13);$(".0000015796"+suffix).attr("maxlength",9);var myMessages=new Object();myMessages['quoteZipCode'+suffix]={zipLabelCleared:"Zip Code is mandatory",required:"Zip Code is mandatory",minlength:"Zip Code should always be 5 digits",maxlength:"Zip Code should always be 5 digits",number:"Zip Code should be numeric",min:myMinMessage,max:myMinMessage};myMessages['quoteType'+suffix]={required:"Insurance Type is mandatory"};myMessages['quotePinNumber'+suffix]={validPin:"Pin can only contain letters and numbers with no spaces"};var validateZipCode=$("form[id="+formIdRoot+suffix+"]").validate({highlight:function(element,errorClass){$("#"+myErrors).show();$(element).addClass(errorClass);},unhighlight:function(element,errorClass){$(element).removeClass(errorClass);},errorLabelContainer:"#"+myErrors,wrapper:"p",rules:myRules,messages:myMessages,submitHandler:function(form){var pinStr="quotePinNumber"+suffix;if((null!=$('#'+pinStr).val())&&($('#'+pinStr).val().toLowerCase()=="pin")){$('#'+pinStr).val("");}
form.submit();}});validators.push(validateZipCode);$("form[id="+formIdRoot+suffix+"]").find("button").click(function(){return validateZipCode.form();});});});$(function(){var formIdRoot="RailComponent_getQuoteForm";var forms=$("form[id^="+formIdRoot+"]").each(function(n){var suffix=this.id.substring(formIdRoot.length);var myErrors=formIdRoot+suffix+"Errors";var myMinValue=0;var myMaxValue=99999;var myMinMessage="Must be valid Zip Code";if($(".0000018072"+suffix).length>0)
{myMinValue=43001;myMaxValue=45999;myMinMessage="Must be valid Ohio Zip code";}
initializeFloatingModuleErrors(myErrors);var myRules=new Object();myRules['quoteZipCode'+suffix]={zipLabelCleared:true,required:true,minlength:5,maxlength:5,min:myMinValue,max:myMaxValue,number:true};myRules['quoteType'+suffix]={required:true};myRules['quotePinNumber'+suffix]={validPin:true}
$.validator.addClassRules("0000018072"+suffix,{validKroger:true});$.validator.addClassRules("0000018095"+suffix,{validShopYourWay:true});$.validator.addClassRules("0000015796"+suffix,{validHiltonHonors:true});$(".0000018072"+suffix).attr("maxlength",13);$(".0000015796"+suffix).attr("maxlength",9);var myMessages=new Object();myMessages['quoteZipCode'+suffix]={zipLabelCleared:"Zip Code is mandatory",required:"Zip Code is mandatory",minlength:"Zip Code should always be 5 digits",maxlength:"Zip Code should always be 5 digits",number:"Zip Code should be numeric",min:myMinMessage,max:myMinMessage};myMessages['quoteType'+suffix]={required:"Insurance Type is mandatory"};myMessages['quotePinNumber'+suffix]={validPin:"Pin can only contain letters and numbers with no spaces"};var validateZipCode=$("form[id="+formIdRoot+suffix+"]").validate({errorLabelContainer:"#"+myErrors,wrapper:"p",rules:myRules,messages:myMessages,submitHandler:function(form){var pinStr="quotePinNumber"+suffix;if((null!=$('#'+pinStr).val())&&($('#'+pinStr).val().toLowerCase()=="pin")){$('#'+pinStr).val("");}
form.submit();},showErrors:function(errorMap,errorList){showFloatingModuleErrors(validateZipCode,myErrors,errorList,formIdRoot+suffix);}});validators.push(validateZipCode);$("form[id="+formIdRoot+suffix+"]").find("button").click(function(){return validateZipCode.form();});});});$(function(){var formIdRoot="retrieveSavedQuoteForm";var forms=$("form[id^="+formIdRoot+"]").each(function(n){var suffix=this.id.substring(formIdRoot.length);var myErrors=formIdRoot+suffix+"Errors";initializeFloatingModuleErrors(myErrors);var myRules=new Object();myRules["quoteId"+suffix]={required:true,minlength:8,maxlength:15,validQuoteID:true};myRules["emailAddress"+suffix]={required:true,email:true};myRules["zipCodeRecall"+suffix]={required:true,minlength:5,maxlength:5,number:true};var myMessages=new Object();myMessages["quoteId"+suffix]={required:"Quote ID is mandatory",minlength:"Please enter a valid Quote ID",validQuoteID:"Please enter a valid Quote ID"};myMessages["emailAddress"+suffix]={required:"Email is mandatory",email:"Please enter a valid email address"};myMessages["zipCodeRecall"+suffix]={required:"Zip Code is mandatory",minlength:"Please enter a valid Zip Code",number:"Please enter a valid Zip Code"};var validateRetrieveSavedQuote=$("form[id="+formIdRoot+suffix+"]").validate({errorLabelContainer:"#"+myErrors,wrapper:"p",rules:myRules,messages:myMessages,showErrors:function(errorMap,errorList){showFloatingModuleErrors(validateRetrieveSavedQuote,myErrors,errorList,formIdRoot+suffix);},submitHandler:function(form){if($("button[id=retrieveSavedQuoteButton"+suffix+"]").attr('disabled')!='true'){$("button[id=retrieveSavedQuoteButton"+suffix+"]").attr('disabled','true');$("button[id=retrieveSavedQuoteButton"+suffix+"]").addClass('cursor-default');form.submit();}}});validators.push(validateRetrieveSavedQuote);$("form[id="+formIdRoot+suffix+"]").find("button").click(function(){if($("button[id=retrieveSavedQuoteButton"+suffix+"]").attr('disabled')!='true'){return validateRetrieveSavedQuote.form();}});});});$(function(){var formIdRoot="advancedSearchForm";var forms=$("form[id^="+formIdRoot+"]").each(function(n){var suffix=this.id.substring(formIdRoot.length);var myErrors=formIdRoot+suffix+"Errors";var myRules=new Object();myRules['locOfficeZipAdv'+suffix]={required:"#officeStateSelect"+suffix+":blank",minlength:5,maxlength:5,number:true};myRules['officeCity'+suffix]={required:"#locOfficeZipAdv"+suffix+":blank"};myRules['officeStateSelect'+suffix]={required:"#locOfficeZipAdv"+suffix+":blank"};myRules['allServicesOffered'+suffix]={required:true};var myMessages=new Object();myMessages['locOfficeZipAdv'+suffix]={required:"Zip code is empty, you must search by valid City/State or Zip",minlength:"Zip Code should always be 5 digits",maxlength:"Zip Code should always be 5 digits",number:"Zip Code should be numeric"};myMessages['officeCity'+suffix]={required:"City empty, you must search by valid City/State or Zip"};myMessages['officeStateSelect'+suffix]={required:"State empty, you must search by valid City/State or Zip"};myMessages['allServicesOffered'+suffix]={required:"One or more Services must be selected for advanced search"};var validateAdvancedSearch=$("form[id="+formIdRoot+suffix+"]").validate({highlight:function(element,errorClass){$("#"+myErrors).show();$(element).addClass(errorClass);},unhighlight:function(element,errorClass){$(element).removeClass(errorClass);if($(".advancedSearchErrors > p > label[for!="+element.id+"]:visible").length==0){}},errorLabelContainer:"#"+myErrors,wrapper:"p",rules:myRules,messages:myMessages});validators.push(validateAdvancedSearch);$("form[id="+formIdRoot+suffix+"]").find("input[type=submit]").click(function(){return validateAdvancedSearch.form();});});});$(function(){var formIdRoot="findAgentForm";var forms=$("form[id^="+formIdRoot+"]").each(function(n){var suffix=this.id.substring(formIdRoot.length);var myErrors="findAgent"+suffix+"Errors";var myRules=new Object();myRules['locAgentZip'+suffix]={required:true,minlength:5,maxlength:5,number:true};var myMessages=new Object();myMessages['locAgentZip'+suffix]={required:"Zip Code is mandatory",minlength:"Zip Code should always be 5 digits",maxlength:"Zip Code should always be 5 digits",number:"Zip Code should be numeric"};var validateFindAgentZip=$("form[id="+formIdRoot+suffix+"]").validate({highlight:function(element,errorClass){$("#"+myErrors).show()
$(element).addClass(errorClass);},unhighlight:function(element,errorClass){$(element).removeClass(errorClass);},errorLabelContainer:"#"+myErrors,wrapper:"p",rules:myRules,messages:myMessages});validators.push(validateFindAgentZip);$("form[id="+formIdRoot+suffix+"]").find("button[type='submit']").click(function(){return validateFindAgentZip.form();});$("#agentFirstName"+suffix).focus(function(){validateFindAgentZip.resetForm();});$("#agentLastName"+suffix).focus(function(){validateFindAgentZip.resetForm();});});});$(function(){var formIdRoot="RailComponent_findAgentForm";var forms=$("form[id^="+formIdRoot+"]").each(function(n){var suffix=this.id.substring(formIdRoot.length);var myErrors=formIdRoot+suffix+"Errors";initializeFloatingModuleErrors(myErrors);var myRules=new Object();myRules['locAgentZip'+suffix]={required:true,minlength:5,maxlength:5,number:true};var myMessages=new Object();myMessages['locAgentZip'+suffix]={required:"Zip Code is mandatory",minlength:"Zip Code should always be 5 digits",maxlength:"Zip Code should always be 5 digits",number:"Zip Code should be numeric"};var validateFindAgentZip=$("form[id="+formIdRoot+suffix+"]").validate({errorLabelContainer:"#"+myErrors,wrapper:"p",rules:myRules,messages:myMessages,showErrors:function(errorMap,errorList){showFloatingModuleErrors(validateFindAgentZip,myErrors,errorList,formIdRoot+suffix);}});validators.push(validateFindAgentZip);$("form[id="+formIdRoot+suffix+"]").find("button[type='submit']").click(function(){return validateFindAgentZip.form();});$("#agentFirstName"+suffix).focus(function(){validateFindAgentZip.resetForm();});$("#agentLastName"+suffix).focus(function(){validateFindAgentZip.resetForm();});});});$(function(){var formIdRoot="advancedSearchZipForm";var forms=$("form[id^="+formIdRoot+"]").each(function(n){var suffix=this.id.substring(formIdRoot.length);var myErrors=formIdRoot+suffix+"Errors";initializeFloatingModuleErrors(myErrors);var myRules=new Object();myRules['locOfficeZipAdv'+suffix]={required:true,minlength:5,maxlength:5,number:true};var myMessages=new Object();myMessages['locOfficeZipAdv'+suffix]={required:"Zip Code is mandatory",minlength:"Zip Code should always be 5 digits",maxlength:"Zip Code should always be 5 digits",number:"Zip Code should be numeric"};var validateAdvancedSearchZipForm=$("form[id="+formIdRoot+suffix+"]").validate({errorLabelContainer:"#"+myErrors,wrapper:"p",rules:myRules,messages:myMessages,showErrors:function(errorMap,errorList){showFloatingModuleErrors(validateAdvancedSearchZipForm,myErrors,errorList,(formIdRoot+suffix));}});validators.push(validateAdvancedSearchZipForm);$("form[id="+formIdRoot+suffix+"]").find("button[type='submit']").click(function(){return validateAdvancedSearchZipForm.form();});});});$(function(){var formIdRoot="advancedSearchAddressForm";var forms=$("form[id^="+formIdRoot+"]").each(function(n){var suffix=this.id.substring(formIdRoot.length);var myErrors=formIdRoot+suffix+"Errors";initializeFloatingModuleErrors(myErrors);var myRules=new Object();myRules['officeCity'+suffix]={required:"#officeCity"+suffix+":blank"};myRules['officeStateSelect'+suffix]={required:"#officeStateSelect"+suffix+":blank"};var myMessages=new Object();myMessages['officeCity'+suffix]={required:"City empty, you must search by valid City/State or Zip"};myMessages['officeStateSelect'+suffix]={required:"State empty, you must search by valid City/State or Zip"};var validateAdvancedSearchAddressForm=$("form[id="+formIdRoot+suffix+"]").validate({errorLabelContainer:"#"+myErrors,wrapper:"p",rules:myRules,messages:myMessages,showErrors:function(errorMap,errorList){showFloatingModuleErrors(validateAdvancedSearchAddressForm,myErrors,errorList,(formIdRoot+suffix));}});validators.push(validateAdvancedSearchAddressForm);$("form[id="+formIdRoot+suffix+"]").find("button[type='submit']").click(function(){return validateAdvancedSearchAddressForm.form();});});});$(function(){var formIdRoot="findAgentNameForm";var forms=$("form[id^="+formIdRoot+"]").each(function(n){var suffix=this.id.substring(formIdRoot.length);var myErrors="findAgentName"+suffix+"Errors";var myRules=new Object();myRules['agentLastName'+suffix]={required:true,minlength:2};var myMessages=new Object();myMessages['agentLastName'+suffix]={required:"Last Name is mandatory"};var validateFindAgentName=$("form[id="+formIdRoot+suffix+"]").validate({highlight:function(element,errorClass){$("#"+myErrors).show()
$(element).addClass(errorClass);},unhighlight:function(element,errorClass){$(element).removeClass(errorClass);},errorLabelContainer:"#"+myErrors,wrapper:"p",rules:myRules,messages:myMessages});validators.push(validateFindAgentName);$("form[id="+formIdRoot+suffix+"]").find("button[type='submit']").click(function(){return validateFindAgentName.form();});$("#locAgentZip"+suffix).focus(function(){validateFindAgentName.resetForm();});});});$(function(){var formIdRoot="RailComponent_findAgentNameForm";var forms=$("form[id^="+formIdRoot+"]").each(function(n){var suffix=this.id.substring(formIdRoot.length);var myErrors=formIdRoot+suffix+"Errors";initializeFloatingModuleErrors(myErrors);var myRules=new Object();myRules['agentLastName'+suffix]={required:true,minlength:2};var myMessages=new Object();myMessages['agentLastName'+suffix]={required:"Last Name is mandatory"};var validateFindAgentName=$("form[id="+formIdRoot+suffix+"]").validate({errorLabelContainer:"#"+myErrors,wrapper:"p",rules:myRules,messages:myMessages,showErrors:function(errorMap,errorList){showFloatingModuleErrors(validateFindAgentName,myErrors,errorList,formIdRoot+suffix);}});validators.push(validateFindAgentName);$("form[id="+formIdRoot+suffix+"]").find("button[type='submit']").click(function(){return validateFindAgentName.form();});$("#locAgentZip"+suffix).focus(function(){validateFindAgentName.resetForm();});});});$(function(){var formIdRoot="ClaimsLogin";var forms=$("form[id^="+formIdRoot+"]").each(function(n){var suffix=this.id.substring(formIdRoot.length);var myErrors="ClaimsLogin"+suffix+"Errors";var myRules=new Object();myRules['USER']={required:true};myRules['PASSWORD']={required:true};var myMessages=new Object();myMessages['USER']={required:"Username is mandatory"};myMessages['PASSWORD']={required:"Password is mandatory"};var validateManageClaim=$("form[id="+formIdRoot+suffix+"]").validate({highlight:function(element,errorClass){$("#"+myErrors).show()
$(element).addClass(errorClass);},unhighlight:function(element,errorClass){$(element).removeClass(errorClass);},errorLabelContainer:"#"+myErrors,wrapper:"p",rules:myRules,messages:myMessages});validators.push(validateManageClaim);$("form[id="+formIdRoot+suffix+"]").find("input[type='submit']").click(function(){return validateManageClaim.form();});});});$(function(){var formIdRoot="manageClaimZipForm";var forms=$("form[id^="+formIdRoot+"]").each(function(n){var suffix=this.id.substring(formIdRoot.length);var myErrors=formIdRoot+suffix+"Errors";var myRules=new Object();myRules['locClaimsOfficeZip'+suffix]={required:true,minlength:5,maxlength:5,number:true};var myMessages=new Object();myMessages['locClaimsOfficeZip'+suffix]={required:"Zip Code is mandatory",minlength:"Zip Code should always be 5 digits",maxlength:"Zip Code should always be 5 digits",number:"Zip Code should be numeric"};var validateManageClaimZip=$("form[id="+formIdRoot+suffix+"]").validate({highlight:function(element,errorClass){$("#"+myErrors).show()
$(element).addClass(errorClass);},unhighlight:function(element,errorClass){$(element).removeClass(errorClass);},errorLabelContainer:"#"+myErrors,wrapper:"p",rules:myRules,messages:myMessages});validators.push(validateManageClaimZip);$("form[id="+formIdRoot+suffix+"]").find("button[type='submit']").click(function(){return validateManageClaimZip.form();});});});$(function(){var formIdRoot="eServiceLogin";var forms=$("form[id^="+formIdRoot+"]").each(function(n){var suffix=this.id.substring(formIdRoot.length);var myErrors="managePolicy"+suffix+"Errors";var myRules=new Object();myRules["USER"]={required:true};myRules["PASSWORD"]={required:true};var myMessages=new Object();myMessages["USER"]={required:"Username is mandatory"};myMessages["PASSWORD"]={required:"Password is mandatory"};var validateManagePolicy=$("form[id="+formIdRoot+suffix+"]").validate({highlight:function(element,errorClass){$("#"+myErrors).show()
$(element).addClass(errorClass);},unhighlight:function(element,errorClass){$(element).removeClass(errorClass);},errorLabelContainer:"#"+myErrors,wrapper:"p",rules:myRules,messages:myMessages});validators.push(validateManagePolicy);$("form[id="+formIdRoot+suffix+"]").find("button").click(function(){return validateManagePolicy.form();});});});$(function(){var formIdRoot="RailComponent_eServiceLogin";var forms=$("form[id^="+formIdRoot+"]").each(function(n){var suffix=this.id.substring(formIdRoot.length);var myErrors=formIdRoot+suffix+"Errors";initializeFloatingModuleErrors(myErrors);var myRules=new Object();myRules["USER"]={required:true};myRules["PASSWORD"]={required:true};var myMessages=new Object();myMessages["USER"]={required:"Username is mandatory"};myMessages["PASSWORD"]={required:"Password is mandatory"};var validateManagePolicy=$("form[id="+formIdRoot+suffix+"]").validate({errorLabelContainer:"#"+myErrors,wrapper:"p",rules:myRules,messages:myMessages,showErrors:function(errorMap,errorList){showFloatingModuleErrors(validateManagePolicy,myErrors,errorList,formIdRoot+suffix);}});validators.push(validateManagePolicy);$("form[id="+formIdRoot+suffix+"]").find("button").click(function(){return validateManagePolicy.form();});});});$(function(){var formIdRoot="getDirectionsForm";var forms=$("form[id="+formIdRoot+"]").each(function(n){var myErrors="getDirectionsFormErrors";var myRules=new Object();myRules["startAddress"]={required:true,minlength:2};var myMessages=new Object();myMessages["startAddress"]={required:"Start Address is mandatory",minlength:"Please enter a valid Start Address"};var validateGetDirections=$("form[id="+formIdRoot+"]").validate({highlight:function(element,errorClass){$("#"+myErrors).show()
$(element).addClass(errorClass);},unhighlight:function(element,errorClass){$(element).removeClass(errorClass);},errorLabelContainer:"#"+myErrors,wrapper:"p",rules:myRules,messages:myMessages});validators.push(validateGetDirections);$("form[id="+formIdRoot+"]").find("input[type=submit]").click(function(){return validateGetDirections.form();});});});$(document).ready(function(){$('a.lightBoxEnabled, span.lightBoxEnabled').live("click",function(e){$('div.jqmWindow.activeLightBox').each(function(index){var displayValue=$(this).css("display");if(displayValue=="block"){var idKey='#'+$(this).attr('id');$(idKey).jqmHide();}})
var idKey='#'+$(this).next('span').text();showLightBoxById(idKey);scroll(0,0);try
{playerView.disableAllVideoControls();}
catch(err)
{}});$('div.lightboxV2').each(function(intIndex){$(this).children('input:first').click(function(e){var idKey='#'+$(this).attr('id');$(idKey).jqmHide();});reflect($(this));});function reflect(lightBox){}});function myLightBoxOpen(hash){hash.w.fadeIn("2000");var ie6=$.browser.msie&&($.browser.version=="6.0");if(ie6){$('#quoteTypeWithOthers').toggle();$('#quoteType').toggle();}};function myLightBoxClose(hash){$("div.floatingModuleErrors").each(function(index){closeFloatingModuleErrors(null,$(this).attr('id'),null);});var ie6=$.browser.msie&&($.browser.version=="6.0");if(ie6){$('#quoteTypeWithOthers').toggle();$('#quoteType').toggle();}
hash.o.remove();hash.w.fadeOut('2000',function(){try
{playerView.enableAllVideoControls();}
catch(err)
{}});};var showLightBoxById=function(idKey){$(idKey).jqm({modal:true,toTop:true,onShow:myLightBoxOpen,onHide:myLightBoxClose,overlay:75});$(idKey).jqmShow();}
$(document).ready(function(){if($('#slideshowContainer').size()>0)
{var promoSlideshow=$('#slideshowContainer').createPromoSlideshow({timeout:0,debug:true});}
$('.promoPlayerModule .promoModule').addClass('visible');});$(document).ready(function(){if($('#slideshowContainerRotating').size()>0)
{var promoSlideshow=$('#slideshowContainerRotating').createPromoSlideshow({timeout:5000,debug:true});}
$('.promoPlayerModule .promoModule').addClass('visible');});jQuery().ready(function(){$("#localNavAccordion .thirdLevelLinks h3").each(function(){$(this).click(function(i){hRef=$(this).find(".navLink").attr('hRef');if(location.href!=hRef){location.href=hRef;}});});if($("#localNavAccordion .fifthLevelLinks .selected").length>0){$("#localNavAccordion a.fourthLevelLinks span.selected").addClass('parentOfSelected');}});function getPageNameFromHref(href){last=href.substr(href.lastIndexOf("/")+1);return last;}
jQuery().ready(function(){prepareForFlash('flashPromoRightModule','instantiateFlash');});$(document).ready(function(){var inError=false;var formIdRoot="retrieveSavedQuoteForm";var forms=$("form[id^="+formIdRoot+"]").each(function(n){var suffix=this.id.substring(formIdRoot.length);$("#retrieveSavedQuoteButton"+suffix).click(function(event){$("#"+this.form.id).find("#property\\(quoteID\\)").val($("#quoteID"+suffix).val());$("#"+this.form.id).find("#property\\(emailAddress\\)").val($("#emailAddress"+suffix).val());$("#"+this.form.id).find("#property\\(zipCodeRecall\\)").val($("#zipCodeRecall"+suffix).val());});});var submitEmailRequest=function(){$.ajax({url:appJSGlobals.ajaxRequestHandlerUrl
+"?ajaxRequest=submitCallBackRequest"
+"&custName="+$("#fullName").val()
+"&callTime="+$("#callTime").val()
+"&phoneNumber="+$("#phoneArea").val()+$("#phoneExch").val()+$("#phoneLFour").val()
+"&extension="+$("#phoneExt").val()
+"&quoteID="+$("#quoteIDCallBack").val()
+"&zip="+$("#zipCodeRecallCallBack").val()
+"&email="+$("#emailAddressCallBack").val(),type:"get",dataType:"text",success:function(data){if(data=="true")
showConfirmationWindow();else
alert("Your request could not be submitted at this time, please try again.");},error:function(data){alert("Your request could not be submitted at this time, please try again.");}});}
jQuery.validator.addMethod("zipCheckCallBackForm",function(value,element)
{if(value.length!=0&&value.length!=5)
{return false;}
else
{if(value.length==5)
{if((/[0-9]/).test(value))
{return true;}
else
{return false;}}
else
{return true;}}});jQuery.validator.addMethod("emailCheckCallBackForm",function(value,element)
{if(value.length!=0)
{var emailRegex=/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;return emailRegex.test(value);}
else
{return true;}});$("#phoneArea").bind('keyup',function(event){if($("#phoneArea").val().length==3)
{$("#phoneExch").focus();}});$("#phoneExch").bind('keyup',function(event){if($("#phoneExch").val().length==3)
{$("#phoneLFour").focus();}});$("#phoneLFour").bind('keyup',function(event){if($("#phoneLFour").val().length==4)
{$("#phoneExt").focus();}});var formIdRoot="requestCallBackForm";var forms=$("form[id^="+formIdRoot+"]").each(function(n){var myErrors="requestCallBackForm"+"Errors";var myRules=new Object();myRules["fullName"]={required:true};myRules["phoneArea"]={required:true,number:true,minlength:3,maxlength:3};myRules["phoneExch"]={required:true,number:true,minlength:3,maxlength:3};myRules["phoneLFour"]={required:true,number:true,minlength:4,maxlength:4};myRules["emailAddressCallBack"]={emailCheckCallBackForm:true};myRules['zipCodeRecallCallBack']={zipCheckCallBackForm:true};myRules["quoteIDCallBack"]={minlength:8,maxlength:15,validQuoteID:true};myRules["callTime"]={required:true};var myMessages=new Object();myMessages["fullName"]={required:"Please enter your full name"};myMessages["phoneArea"]={required:"Please enter a valid phone number with area code",number:"Please enter a valid phone number with area code",minlength:"Please enter a valid phone number with area code",maxlength:"Please enter a valid phone number with area code"};myMessages["phoneExch"]={required:"Please enter a valid phone number with area code",number:"Please enter a valid phone number with area code",minlength:"Please enter a valid phone number with area code",maxlength:"Please enter a valid phone number with area code"};myMessages["phoneLFour"]={required:"Please enter a valid phone number with area code",number:"Please enter a valid phone number with area code",minlength:"Please enter a valid phone number with area code",maxlength:"Please enter a valid phone number with area code"};myMessages["emailAddressCallBack"]={emailCheckCallBackForm:"Please enter a valid email address"};myMessages['zipCodeRecallCallBack']={zipCheckCallBackForm:"Please enter a valid Zip Code"};myMessages["quoteIDCallBack"]={minlength:"Please enter a valid Quote ID",validQuoteID:"Please enter a valid Quote ID"};myMessages["callTime"]={required:"Please indicate the best time to call"};var validateRequestCallBack=$("form[id="+formIdRoot+"]").validate({highlight:function(element,errorClass){var name=$(element).attr("name");$("#"+myErrors).show();$(element).addClass(errorClass);},unhighlight:function(element,errorClass){var name=$(element).attr("name");$(element).removeClass(errorClass);},errorLabelContainer:"#"+myErrors,wrapper:"p",rules:myRules,messages:myMessages,groups:{fullPhone:"phoneArea phoneExch phoneLFour"}});$("form[id="+formIdRoot+"]").find("button").click(function(){if(validateRequestCallBack.form())
{submitEmailRequest();}
else
{inError=true;}});$("#phoneArea").bind('blur',function(event){validatePhoneFields();});$("#phoneExch").bind('blur',function(event){validatePhoneFields();});$("#phoneLFour").bind('blur',function(event){validatePhoneFields();});var validatePhoneFields=function()
{var noErr=true;if($("#phoneArea").val().length>0)
{noErr=validateRequestCallBack.element("#phoneArea");}
if(noErr&&$("#phoneExch").val().length>0)
{noErr=validateRequestCallBack.element("#phoneExch");}
if(noErr&&$("#phoneLFour").val().length>0)
{noErr=validateRequestCallBack.element("#phoneLFour");}
if(noErr==false)
{$("#"+myErrors+" p label[for='fullPhone']").css("display","block");}
return noErr;}});$("#navUtility li a").click(function(e){$("#quoteIDCallBack").val($("#property\\(quoteID\\)").val());$("#zipCodeRecallCallBack").val($("#property\\(zipCodeRecall\\)").val());$("#emailAddressCallBack").val($("#property\\(emailAddress\\)").val());});$("#retrieveYourQuoteContentAreaCover").css("display","none");});function showConfirmationWindow(){$("#requestCallBackContent").css("display","none");$("#emailConfirmation").css("display","block");$("#emailConfirmationText").css("display","block");}
$(document).ready(function(){var sliderPagerCounter=0;jQuery.extend(jQuery.fn,{createModuleSlider:function(options){if(!this.length){options&&options.debug&&window.console&&console.warn("nothing selected, can't create slideshow, returning nothing");return;}
var slider=jQuery.data(this[0],'moduleSlider');if(slider){return slider;}
slider=new jQuery.moduleSlider(options,this[0]);jQuery.data(this[0],'moduleSlider',slider);return slider;}});$.moduleSlider=function(options,container){this.settings=jQuery.extend({},jQuery.moduleSlider.defaults,options);this.create(container,this);};jQuery.extend(jQuery.moduleSlider,{prototype:{create:function(container,myThis){$(container).after('<div id="sliderPager'+sliderPagerCounter+'" class="sliderPager">').cycle(myThis.settings);}}});$(".moduleSlider").each(function(){var moduleSlider=$(this).find('.sliderContainer').createModuleSlider({fx:'fade',prev:$(this).find(".sliderLeftButton").eq(0),next:$(this).find(".sliderRightButton").eq(0),speed:200,height:'auto',pager:"#sliderPager"+sliderPagerCounter,timeout:0,nowrap:false,fit:true,requeueOnImageNotLoaded:false,timeout:5000,pagerAnchorBuilder:function(idx,slide){return'<span><a href="#">&nbsp;</a></span>';}});sliderPagerCounter++;});});jQuery().ready(function(){var acParams={serviceLang:"EN-US",serviceRestriction:"auto",isActive:false,browseLoaded:false,isExpanded:false}
var glossaryNav=function(clicked){if(clicked&&clicked.length>0)
{$("#gInterface li.active").removeClass("active");$(clicked).addClass("active");if(acParams.isExpanded){$("#termDescription").css('display','none');$("#termContainer").animate({height:"0px"},function(){$("#termDescription").css('display','block');acParams.isExpanded=false;});}
if($(clicked).attr("id").substr(3,6)=='browse'){$("#searchForm").css('display','none');$("#browseForm").css('display','block');if(acParams.browseLoaded==false){$.ajax({url:appJSGlobals.ajaxRequestHandlerUrl+"?ajaxRequest="+'glossaryFindAll',type:"get",dataType:"text",success:function(data){var c=new Array;var dataArray=data.split("@@");for(i=0;i<dataArray.length;i++){var n=dataArray[i].split("|")
c.push("<option value='"+n[1]+"'>"+n[0]+"</option>")}
$("#browseList").html(c.join(""));},error:function(){$("#glossaryError").html("<p>We're sorry, the request failed. The service is temporaily unavailable.</p><p>Please <a href=\"\">browse glossary terms manually</a> for more information</p>").fadeIn();}});acParams.browseLoaded=true;}}else{$("#searchForm").css('display','block');$("#browseForm").css('display','none');}}}
var showDef=function(termObj){if(termObj){$("#termDescription").addClass('def_loading');if(acParams.isExpanded==false){$("#termContainer").animate({height:"200px"},function(){$("#termDescription").css('overflow','auto');acParams.isExpanded=true;});}
$("#searchTerm").attr("value","");$.ajax({url:appJSGlobals.ajaxRequestHandlerUrl+"?ajaxRequest=glossaryFindDefinitions"+"&q="+termObj.id+"&lang="+acParams.serviceLang+"&restriction="+acParams.serviceRestriction,type:"get",dataType:"text",success:function(data){$("#termDescription").removeClass('def_loading');$("#termDescription").html(data);},error:function(){$("#glossaryError").html("<p>We're sorry, the request failed. The service is temporaily unavailable.</p><p>Please <a href=\"\">browse glossary terms manually</a> for more information</p>").fadeIn();}});}}
$('#glossaryError').hide();glossaryNav($("#gInterface .active"));$("#moduleGlossary #searchTerm").click(function(){$(this).attr("value","");})
$("#browseList").change(function(){var termObj={id:$("#browseList").attr("value")}
showDef(termObj);})
$("#gInterface li").click(function(){glossaryNav($(this))})
$("#moduleGlossary #searchTerm").autocomplete(appJSGlobals.ajaxRequestHandlerUrl,{delay:100,minChars:2,matchSubset:1,lineSeparator:"@@",extraParams:{'ajaxRequest':'glossaryFindTerms',lang:acParams.serviceLang,restriction:acParams.serviceRestriction},matchContains:1,cacheLength:10,onItemSelect:function(li){var termObj={id:li.extra}
showDef(termObj);},formatItem:function(row){return row[0];},autoFill:true});});$(document).ready(function(){var formIdRoot="advancedSearchForm";var forms=$("form[id^="+formIdRoot+"]").each(function(n){var suffix=this.id.substring(formIdRoot.length);var q1=document.getElementById("insuranceCheck"+suffix).checked;var q2=document.getElementById("claimsCheck"+suffix).checked;if(!q1&&!q2){$("#insuranceCheck"+suffix).attr('checked','checked');}
$("#insuranceCheck"+suffix).bind('click',function(event){populateAllServicesOffered(suffix);});$("#claimsCheck"+suffix).bind('click',function(event){populateAllServicesOffered(suffix);});populateAllServicesOffered(suffix);});$('#module_advancedSearch .byZip select').change(function(){$('#module_advancedSearch .asAddress input[name^=searchWithin]').val($('#module_advancedSearch .byZip select').val());});});function populateAllServicesOffered(suffix){var q1=document.getElementById("insuranceCheck"+suffix).checked;var q2=document.getElementById("claimsCheck"+suffix).checked;if(q1||q2){$("#allServicesOffered"+suffix).val("SERVICESELECTED");}
else{$("#allServicesOffered"+suffix).val("");}
$("form[id^=advancedSearchForm]").validate().element("#allServicesOffered"+suffix);}
$(document).ready(function(){$('.managePolicyForm').each(function(selindex){populateManagePolicy($(this));});});function eServiceLoginSubmit(eServiceLoginForm){if(eServiceLoginForm.REMEMBERID){if(eServiceLoginForm.USER.value!=eServiceLoginForm.origUser.value)
{deleteCookie("ESRVUSERMASK");}}}
function eServiceLoginUsernameChange(){deleteCookie("ESRVUSERMASK");}
function deleteCookie(name){var options={path:'/',expires:31536000,domain:'.libertymutual.com'};var cookieId=$.cookie(name,null,options);}
function populateManagePolicy(loginForm){if(document.cookie.indexOf("ESRVUSERMASK")>=0){var allcookies=document.cookie;cookiearray=allcookies.split(';');for(var i=0;i<cookiearray.length;i++){var name=cookiearray[i].split('=')[0];var value=cookiearray[i].split('=')[1];if(name==" ESRVUSERMASK"){$(loginForm).find("#USER").attr("value",value);$(loginForm).find("#origUser").attr("value",value);$(loginForm).find("#remember").attr("value","on");$(loginForm).find("#remember").attr("checked",true);}}}}
$(document).ready(function(){var inError=false;var formIdRoot="RailComponent_getQuoteForm";var forms=$("form[id^="+formIdRoot+"]").each(function(n){var suffix=this.id.substring(formIdRoot.length);var memberIdCleared=false;var default_val='';$("#quoteMemberId"+suffix).focus(function(){if($(this).val()==$(this).data('default_val')||!$(this).data('default_val'))
{$(this).data('default_val',$(this).val());$(this).val('');$(this).addClass("normal");}});$("#quoteMemberId"+suffix).blur(function(){if($(this).val()=='')
{$(this).val($(this).data('default_val'));$(this).removeClass("normal");}});});});
jQuery().ready(function(){mediaCenterHandler.initDisplay();});var mediaCenterHandler=(function()
{var helper={};helper.initDisplay=initDisplay;function initDisplay()
{var isApple=isAppleDevice();if($("#mcPlayerArea").size()>0)
{log("*** mc5Xml ***\n"+cdaPageVars.mc5Xml);displayFlashPlayer();}}
helper.displayFlashPlayer=displayFlashPlayer;function displayFlashPlayer()
{if(booleanShouldDisplayFlash=="false")
{displayMc5textArea();}
if(appJSGlobals.mc5Deactivated=="ON")
{log("*** mc5 deactivated ***");if($("#HTML5MediaCenterContainer").length<=0){displayMc5textArea();displayUpgradeFlash();}}
else if(retPlaylistXml().length<=0)
{log("*** could not find playListXml ***");displayMc5textArea();displayUpgradeFlash();}
else if(!swfobject.hasFlashPlayerVersion(appJSGlobals.requiredFlashPlayerVersion))
{if($("#HTML5MediaCenterContainer").length<=0){displayMc5textArea();}
var isiPad=navigator.userAgent.match(/iPad/i)!=null;var isiPhone=navigator.userAgent.match(/iPhone/i)!=null;var isiPod=navigator.userAgent.match(/iPod/i)!=null;if(isiPad==false&&isiPhone==false&&isiPod==false&&$("#HTML5MediaCenterContainer").length<=0)
{displayUpgradeFlash();}else{}}
else if(cdaPageVars.playerMediaFile!=undefined)
{swfobject.registerObject("mcplayer","10.0.0","Unknown_83_filename"/*tpa=https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/expressInstall.swf*/);$("object#mcplayer").css('visibility','visible');FABridge.addInitializationCallback("lm_mcplayer_bridge",mcplayerCallback);}}
function isAppleDevice()
{var appleDevice=true;var isiPad=navigator.userAgent.match(/iPad/i)!=null;var isiPhone=navigator.userAgent.match(/iPhone/i)!=null;var isiPod=navigator.userAgent.match(/iPod/i)!=null;if(isiPad==false&&isiPhone==false&&isiPod==false)
{appleDevice=false;}
return appleDevice;}
function displayMc5textArea()
{$(".mcTitle").each(function(index,value){var stringToCompare="(TM)";var textInTitle=$(value).text();var beggining=textInTitle.indexOf(stringToCompare);var end=beggining+stringToCompare.length;if(beggining!=-1){htmlString="<h1 class='mcTitle' id='mcTitle"+index+"'>";var title=textInTitle.substr(0,beggining);var title2=textInTitle.substr(end,textInTitle.length-1);htmlString+=title+"<sup class='superscriptFormated'>TM</sup>"+title2+"</h1>";$(value).replaceWith(htmlString);$(".headerWrapper").css("visibility","visible");}else{$(".headerWrapper").css("visibility","visible");}});$(".thumbTitle").each(function(index,value){var stringToCompare="(TM)";var textInTitle=$(value).text();var beggining=textInTitle.indexOf(stringToCompare);var end=beggining+stringToCompare.length;if(beggining!=-1){htmlString="<div class='thumbTitle titleUnSelected'>";var title=textInTitle.substr(0,beggining);var title2=textInTitle.substr(end,textInTitle.length-1);htmlString+=title+"<sup class='superscriptFormated2'>TM</sup>"+title2+"</div>";$(value).replaceWith(htmlString);$(".thumbTitle").css("visibility","visible");}else{$(".thumbTitle").css("visibility","visible");}});$("div#mcTextArea").css("display","block");$("div.activateJavascript").css("display","none");$("div#playerInnerArea").css("display","none");var innerContainerHeight=$('#plainNoFlashInnerContainer').height();var mediaHeight=$('#plainNoFlashMedia').height();if(innerContainerHeight>mediaHeight)
{$('#plainNoFlashMedia').jScrollPane({scrollbarWidth:35,dragMaxHeight:55,scrollbarOnLeft:true,scrollbarMargin:15,verticalDragMinHeight:55,verticalDragMaxHeight:55});}
$("#mcPlayerArea").height($("div#mcTextArea").height());}
function displayUpgradeFlash()
{$("div.upgradeYourFlash").css("display","block");$("#mcPlayerArea").height("auto");}
function mcplayerCallback()
{var flexApp=null;try
{flexApp=FABridge.lm_mcplayer_bridge.root();}
catch(err)
{displayMc5textArea();}
if(flexApp)
{flexApp.setPlaylist(retPlaylistXml());}}
function retPlaylistXml()
{var x=cdaPageVars.mc5Xml;x=replaceAll(x,"&amp;quot;","\"");x=replaceAll(x,"&amp;#","&#");x=replaceAll(x,"&amp;#39;","'");return x;}
return helper;})();function showLightBoxCallback(id)
{showLightBoxById('#'+id);}
window.Swipe=function(element,options){if(!element)return null;var _this=this;this.options=options||{};this.index=this.options.startSlide||0;this.speed=this.options.speed||300;this.callback=this.options.callback||function(){};this.delay=this.options.auto||0;this.container=element;this.element=this.container.children[0];this.container.style.overflow='hidden';this.element.style.listStyle='none';this.setup();this.begin();if(this.element.addEventListener){this.element.addEventListener('touchstart',this,false);this.element.addEventListener('touchmove',this,false);this.element.addEventListener('touchend',this,false);this.element.addEventListener('webkitTransitionEnd',this,false);this.element.addEventListener('msTransitionEnd',this,false);this.element.addEventListener('oTransitionEnd',this,false);this.element.addEventListener('transitionend',this,false);window.addEventListener('resize',this,false);}};Swipe.prototype={setup:function(){this.slides=this.element.children;this.length=this.slides.length;if(this.length<2)return null;this.width=this.container.getBoundingClientRect().width;if(!this.width)return null;this.container.style.visibility='hidden';this.element.style.width=(this.slides.length*this.width)+'px';var index=this.slides.length;while(index--){var el=this.slides[index];el.style.width=this.width+'px';el.style.display='table-cell';el.style.verticalAlign='top';}
this.slide(this.index,0);this.container.style.visibility='visible';},slide:function(index,duration){var style=this.element.style;style.webkitTransitionDuration=style.MozTransitionDuration=style.msTransitionDuration=style.OTransitionDuration=style.transitionDuration=duration+'ms';style.webkitTransform='translate3d('+-(index*this.width)+'px,0,0)';style.msTransform=style.MozTransform=style.OTransform='translateX('+-(index*this.width)+'px)';this.index=index;},getPos:function(){return this.index;},prev:function(delay){this.delay=delay||0;clearTimeout(this.interval);if(this.index)this.slide(this.index-1,this.speed);},next:function(delay){this.delay=delay||0;clearTimeout(this.interval);if(this.index<this.length-1)this.slide(this.index+1,this.speed);else this.slide(0,this.speed);},begin:function(){var _this=this;this.interval=(this.delay)?setTimeout(function(){_this.next(_this.delay);},this.delay):0;},stop:function(){this.delay=0;clearTimeout(this.interval);},resume:function(){this.delay=this.options.auto||0;this.begin();},handleEvent:function(e){switch(e.type){case'touchstart':this.onTouchStart(e);break;case'touchmove':this.onTouchMove(e);break;case'touchend':this.onTouchEnd(e);break;case'webkitTransitionEnd':case'msTransitionEnd':case'oTransitionEnd':case'transitionend':this.transitionEnd(e);break;case'resize':this.setup();break;}},transitionEnd:function(e){if(this.delay)this.begin();this.callback(e,this.index,this.slides[this.index]);},onTouchStart:function(e){this.start={pageX:e.touches[0].pageX,pageY:e.touches[0].pageY,time:Number(new Date())};this.isScrolling=undefined;this.deltaX=0;this.element.style.webkitTransitionDuration=0;},onTouchMove:function(e){if(e.touches.length>1||e.scale&&e.scale!==1)return;this.deltaX=e.touches[0].pageX-this.start.pageX;if(typeof this.isScrolling=='undefined'){this.isScrolling=!!(this.isScrolling||Math.abs(this.deltaX)<Math.abs(e.touches[0].pageY-this.start.pageY));}
if(!this.isScrolling){e.preventDefault();clearTimeout(this.interval);this.deltaX=this.deltaX/((!this.index&&this.deltaX>0||this.index==this.length-1&&this.deltaX<0)?(Math.abs(this.deltaX)/this.width+1):1);this.element.style.webkitTransform='translate3d('+(this.deltaX-this.index*this.width)+'px,0,0)';}},onTouchEnd:function(e){var isValidSlide=Number(new Date())-this.start.time<250&&Math.abs(this.deltaX)>20||Math.abs(this.deltaX)>this.width/2,isPastBounds=!this.index&&this.deltaX>0||this.index==this.length-1&&this.deltaX<0;if(!this.isScrolling){this.slide(this.index+(isValidSlide&&!isPastBounds?(this.deltaX<0?1:-1):0),this.speed);}}};jQuery().ready(function(){if($("#centerModSet").is("div")){var centerModules=$("#centerModSet").children("div");var moduleHeights=[];for(i=0;i<centerModules.length;i++){moduleHeights.push($(centerModules[i]).height());}
moduleHeights.sort(function(a,b){return a-b});$("#centerModSet").height(moduleHeights[moduleHeights.length-1]+10);}
if($("#centerModSet2").is("div")){var centerModules=$("#centerModSet2").children("div");if(centerModules.length>0)
{var moduleHeights=[];for(i=0;i<centerModules.length;i++){moduleHeights.push($(centerModules[i]).height());}
moduleHeights.sort(function(a,b){return a-b});$("#centerModSet2").height(moduleHeights[moduleHeights.length-1]+10);}}
if($(".boxedContentCenterModule").size()>0||$(".boxedContentCenterModule2").size()>0){if($(".boxedContentCenterModuleIntroduction").length==0){$(".secondaryEndText").insertAfter("#.boxedContentCenterModule:last");$(".secondaryEndText").insertAfter("#.boxedContentCenterModule2:last");}else{$(".secondaryEndText").insertAfter(".boxedContentCenterModuleIntroduction");}
$(".secondaryEndText").css("clear","none");$(".secondaryEndText").css("float","left");$(".secondaryEndText").css("padding-top","15px");$(".secondaryEndText p").css("width","500px");$("#contentPage #moduleArea, .isAccordion").css("min-height","490px");var height=0;$(".boxedContentCenterModule").each(function(index){if(index==0){height=$(this).height();}else{if($(this).height()>height){height=$(this).height();}}});$(".boxedContentCenterModule").css("height",height+"px");var height2=0;$(".boxedContentCenterModule2").each(function(index){if(index==0){height2=$(this).height();}else{if($(this).height()>height2){height2=$(this).height();}}});$(".boxedContentCenterModule2").css("height",height2+"px");var pHeight=0;$(".boxedContentCenterModuleText").each(function(index){if(index==0){pHeight=$(this).height();}else{if($(this).height()>pHeight){pHeight=$(this).height();}}});$(".boxedContentCenterModuleText").each(function(index){$(this).height(pHeight);});$(".boxedContentCenterModuleText2").each(function(index){if(index==0){pHeight=$(this).height();}else{if($(this).height()>pHeight){pHeight=$(this).height();}}});$(".boxedContentCenterModuleText2").each(function(index){$(this).height(pHeight);});$(".boxedContentCenterModule").css("visibility","visible");}});$(document).ready(function(){$("#tabCenterModSet").css('display','block');$("#tabs p.noscriptTabCenterModTitle").css('display','none');$("#tabs ul").css('display','block');var centertabs=$('#tabs');if(centertabs!==null&&centertabs.length>0)
{centertabs.tabs();}});jQuery.extend({VideoModel:function(mediaXMLString)
{var that=this;var maxCarousel=0;var NUM_THUMBS_PAGE=5;var currentlySelected=new Object();var previouslySelected=new Object();var videoObjectArray=new Array();this.populatePlaylistArray=function()
{var theXML=that.convertStringToXML(mediaXMLString);$(theXML).find('"[nodeName=\'pm:videoInteractiveMedia\']"').each(function(i)
{var newObj=new $.TileModel(this,{id:"thumb"+i,num:i});if(i===0)
{that.setCurrentlySelected(newObj);}
that.pushToArray(newObj);});}
this.createCarouselArray=function()
{var newArray=[0];for(var i=1;i<6;i++)
{newArray[i]=NUM_THUMBS_PAGE*i;}
return newArray;}
this.returnSelectedObject=function(idString)
{for(i=0;i<videoObjectArray.length;i++)
{var retObj=new Object();if(videoObjectArray[i].id.toString()===idString.toString())
{retObj=videoObjectArray[i];break;}}
that.currentlySelected=retObj;return retObj;}
this.convertStringToXML=function(theString)
{if(window.ActiveXObject)
{var oXML=new ActiveXObject("Microsoft.XMLDOM");oXML.loadXML(theString);return oXML;}
else
{return(new DOMParser()).parseFromString(theString,"text/xml");}}
this.pushToArray=function(obj)
{videoObjectArray.push(obj);}
this.returnVideoArray=function()
{return videoObjectArray;}
this.setCurrentlySelected=function(obj)
{that.currentlySelected=obj;}
this.returnCurrentlySelected=function()
{return that.currentlySelected;}
this.setPreviouslySelected=function(obj)
{that.previouslySelected=obj;}
this.returnPreviouslySelected=function()
{return that.previouslySelected;}
this.setMaxCarousel=function(objArray)
{that.maxCarousel=Math.ceil(objArray.length/NUM_THUMBS_PAGE);}
this.returnMaxCarousel=function()
{return that.maxCarousel;}
this.returnNumThumbsPage=function()
{return NUM_THUMBS_PAGE;}},TileModel:function(element,options){var that=this;var settings=$.extend(this,options||{});this.returnID=function()
{return that.id;}}});jQuery.extend({PlayerView:function()
{var that=this;this.setAttribute=function(element,attribute,value)
{$(element).attr(attribute,value);}
this.highlightSelected=function(id)
{$("#"+id).addClass('thumbnailSelected');$("#"+id).find(".thumbImage img").animate({marginTop:"0",width:"100%",height:"100%"},250);}
this.clearHighlightSelected=function(id)
{$("#"+id).find('div').remove(".playButton");$("#"+id).removeClass('thumbnailSelected');}
this.resetRadioIndicator=function(num)
{$(".radioNav").each(function()
{$(this).removeClass("radioSelected");});$("#radio"+num).addClass("radioSelected");}
this.moveCarousel=function(value)
{carouselSwipe.slide(value,300);}
this.setTileText=function(obj)
{$("#mcTitle").html(obj.titleText);$("#mcIntro").html(obj.introText);}
this.stopVideo=function(num)
{var videoElementString="videoElement"+num;var currentTimeValue=document.getElementById(videoElementString).currentTime;if(currentTimeValue>0)
{document.getElementById(videoElementString).currentTime=0.1;}
document.getElementById(videoElementString).pause();}
this.showShim=function(num)
{$("#shimImage"+num).fadeIn('normal',function(){$("#videoElement"+num).css("display","none");});$(".posterPlayButton").fadeIn('normal');}
this.playVideo=function(num)
{var videoElementString="videoElement"+num;document.getElementById(videoElementString).play();$("#videoElement"+num).css("display","block");$("#shimImage"+num).fadeOut('normal');$(".posterPlayButton").css("display","none");}
this.disableVideoControls=function(num)
{$("#videoElement"+num).removeAttr("controls");}
this.enableVideoControls=function(num)
{$("#videoElement"+num).attr("controls","true");}
this.disableAllVideoControls=function()
{$('.videoElement').each(function()
{$(this).removeAttr("controls");});}
this.enableAllVideoControls=function()
{$('.videoElement').each(function()
{$(this).attr("controls","true");});}
this.videoPaused=function(obj)
{var videoElementString="videoElement"+obj.num;var boolPaused=document.getElementById(videoElementString).paused;return boolPaused;}},VideoView:function()
{var that=this;},TileView:function(options){var that=this;}});jQuery.extend({VideoController:function(playerView,model)
{var that=this;var CAROUSEL_POSITIONS=[0,585.5,1293,1939];var tileView=new $.TileView();model.populatePlaylistArray();var currentCarousel=1;var boolPageLoaded=false;var boolThumbClick=false;var changeCarouselArray=model.createCarouselArray();var thumbCount=0;try{var videoArray=model.returnVideoArray();var numVideos=videoArray.length;model.setMaxCarousel(videoArray);if(numVideos>0&&deviceFunctionalityGroup=="fg_html5video")
{$("div#mcTextArea").css("display","none");$("#mcPlayerArea").height($("div#mcHTML5Video").height());}}
catch(err)
{}
var mCarousel=model.returnMaxCarousel();$('.firstLevelLink').hover(function()
{var cSelected=model.returnCurrentlySelected();if(cSelected!=null)
{playerView.disableVideoControls(cSelected.num);}});$('.subMenuInner').hover(function()
{var cSelected=model.returnCurrentlySelected();if(cSelected!=null)
{playerView.disableVideoControls(cSelected.num);}});$('.mm-menu').mouseout(function()
{var cSelected=model.returnCurrentlySelected();if(cSelected!=null)
{playerView.enableVideoControls(cSelected.num);}});$('.videoElement').each(function(i)
{var that=this;$(this).bind("ended",function(e)
{that.currentTime=0.1;});$(this).bind("playing",function(e)
{playerView.enableVideoControls(i);});});playerView.disableAllVideoControls();$(".posterPlayButton").bind('click',function()
{var cSelected=model.returnCurrentlySelected();if(cSelected!=null)
{playerView.playVideo(cSelected.num);}});$(".thumbnail").each(function()
{$(this).attr("id","thumb"+thumbCount);var self=that;$(this).click(function()
{self.boolThumbClick=true;var myIDNum=Number($(this).attr("id").slice(5));var pSelected=model.returnPreviouslySelected();var cSelected=model.returnSelectedObject($(this).attr("id"));if(pSelected!=cSelected)
{playerView.stopVideo(pSelected.num);playerView.showShim(pSelected.num);}
mySwipe.slide(myIDNum,300);self.setThumbnailVisibility($(this).attr("id"),myIDNum);playerView.disableAllVideoControls();});thumbCount++;});this.setThumbnailVisibility=function(id,index)
{var currentSelection=model.returnCurrentlySelected();if(boolPageLoaded===false)
{playerView.highlightSelected("thumb0");model.setPreviouslySelected(currentSelection);boolPageLoaded=true;}
else
{var videoObject=model.returnSelectedObject(id);var pSelected=model.returnPreviouslySelected();var cSelected=videoObject;if(pSelected!=cSelected)
{playerView.highlightSelected(cSelected.id);playerView.clearHighlightSelected(pSelected.id);model.setCurrentlySelected(cSelected);}
model.setPreviouslySelected(videoObject);that.setCarouselPosition(index);}}
this.setRadioIndicators=function()
{var i=0;var videoArray=model.returnVideoArray();var videosPerCarousel=model.returnNumThumbsPage();var max=Math.ceil(videoArray.length/videosPerCarousel);var mCarousel=model.returnMaxCarousel();if(mCarousel>=2)
{for(i=0;i<max;i++)
{playerView.addRadioButton(i);}}}
this.setHeaderText=function(obj)
{playerView.setTileText(obj);}
this.onSlideComplete=function(index)
{var id="thumb"+index;if(!boolThumbClick)
{that.setThumbnailVisibility(id,index);}
boolThumbClick=false;var cSelected=model.returnCurrentlySelected();}
this.onCarouselSlideComplete=function(index)
{playerView.resetRadioIndicator(index+1);}
this.setCarouselPosition=function(num)
{var index=(num+1);if(index>changeCarouselArray[currentCarousel-1]&&index<changeCarouselArray[currentCarousel])
{}
else if(index<changeCarouselArray[currentCarousel])
{currentCarousel--;playerView.moveCarousel((currentCarousel-1));playerView.resetRadioIndicator(currentCarousel);}
else if(index>changeCarouselArray[currentCarousel])
{currentCarousel++;playerView.moveCarousel((currentCarousel-1));playerView.resetRadioIndicator(currentCarousel);}
else
{}}
this.setVideoElementSRC=function(obj)
{playerView.setAttribute($("#videoElement"+obj.num),"src",obj.mp4Path);}
if(numVideos>0)
{try{this.setThumbnailVisibility("thumb0",0);this.setRadioIndicators();}
catch(err)
{}}}});$(function(){if(cdaPageVars.mc5Xml){try{playerView=new $.PlayerView();var VideoController=new $.VideoController(playerView,new $.VideoModel(cdaPageVars.mc5Xml));}
catch(err)
{}}
try
{window.mySwipe=new Swipe(document.getElementById('slider'),{callback:function(){VideoController.onSlideComplete(this.getPos());}});}
catch(err)
{}
try
{window.carouselSwipe=new Swipe(document.getElementById('carouselContainer'),{callback:function(){VideoController.onCarouselSlideComplete(this.getPos());}});}
catch(err)
{}});function videoFailed(e){switch(e.target.error.code){case e.target.error.MEDIA_ERR_ABORTED:alert('You aborted the video playback.');break;case e.target.error.MEDIA_ERR_NETWORK:alert('A network error caused the video download to fail part-way.');break;case e.target.error.MEDIA_ERR_DECODE:alert('The video playback was aborted due to a corruption problem or because the video used features your browser did not support.');break;case e.target.error.MEDIA_ERR_SRC_NOT_SUPPORTED:alert('The video could not be loaded, either because the server or network failed or because the format is not supported.');break;default:alert('An unknown error occurred.');break;}}
(function($){$.customMediaPlayer=function(element,options){var defaults={'delay':2000,'fadeSpeed':500,'dilimiterConfig':5,'thumbnailToggleClass':'selectedThumbnail','alternativeCSS':{},'thumbnailContainer':'#thumbnailContainer','resources':'.mediaResource','resourceParent':'#mediaResources','resourceChildren':'','thumbnails':'#thumbnails','thumbnailChildren':'#thumbnails div','playerSelector':'#HTML5MediaCenterContainer','thumbnailClass':'.thumbnailResource','autoPlay':true,'shimImageClass':'tileBackgroundImage','resourceWidth':666,'mediaCenterExtensions':[],'mediaCenterTileContainer':'.mediaCenterTileContainer'};var plugin=this;plugin.settings={};var $element=$(element),element=element;plugin.init=function(){if(options){plugin.settings=$.extend({},defaults,options);}else{plugin.settings=$.extend({},defaults);}
if(plugin.settings.alternativeCSS[0]!='undefined'){for(var key in plugin.settings.alternativeCSS){$(key).toggleClass(plugin.settings.alternativeCSS[key]);}}
if(plugin.settings.mediaCenterExtensions.length==0){initializeMediaCenterSingleTile(plugin.settings);}else{for(var i=0;i<plugin.settings.mediaCenterExtensions.length;i++){plugin.settings.mediaCenterExtensions[i].init(plugin.settings);}}
$(plugin.settings.playerSelector).css("display","block");}
plugin.numMediaResources=function(){return $(plugin.settings.resources).length;}
plugin.init();};$.fn.customMediaPlayer=function(options){return this.each(function(){if(undefined==$(this).data('customMediaPlayer')){var plugin=new $.customMediaPlayer(this,options);$(this).data('customMediaPlayer',plugin);}});};})(jQuery);function initCssProps(div,count){for(var ii=0;ii<count;ii++){if(div.eq(ii).find(".html5IlluminationTile").hasClass('html5IlluminationTile')){;}else if(isTileDirectionLeft(div,ii)){div.eq(ii).find(".mediaCenterTileContainer").css({left:'-22px'});}else{div.eq(ii).find(".mediaCenterTileContainer").css({right:'-415px'});}}}
function getAnimProps(div,divIndex){var props=null;if(div.eq(divIndex).find(".html5IlluminationTile").hasClass('html5IlluminationTile')){props={opacity:'1.0'};}else if(isTileDirectionLeft(div,divIndex)){props={left:'0px',opacity:'1.0'};}else{props={right:'-395px',opacity:'1.0'};}
return props;}
function getCssProps(div,divIndex){var props=null;if(div.eq(divIndex).find(".html5IlluminationTile").hasClass('html5IlluminationTile')){props={opacity:'0.0',display:'none'};}else if(isTileDirectionLeft(div,divIndex)){props={left:'-22px',opacity:'0.0',display:'none'};}else{props={right:'-415px',opacity:'0.0',display:'none'};}
return props;}
function isTileDirectionLeft(div,divIndex){var directionLeft=true;if(div.eq(divIndex).find(".rightAlignedTile").hasClass('rightAlignedTile')){directionLeft=false;}
return directionLeft;}
jQuery.easing['jswing']=jQuery.easing['swing'];jQuery.extend(jQuery.easing,{def:'easeOutQuad',swing:function(x,t,b,c,d){return jQuery.easing[jQuery.easing.def](x,t,b,c,d);},easeInQuad:function(x,t,b,c,d){return c*(t/=d)*t+b;},easeOutQuad:function(x,t,b,c,d){return-c*(t/=d)*(t-2)+b;},easeInOutQuad:function(x,t,b,c,d){if((t/=d/2)<1)return c/2*t*t+b;return-c/2*((--t)*(t-2)-1)+b;},easeInCubic:function(x,t,b,c,d){return c*(t/=d)*t*t+b;},easeOutCubic:function(x,t,b,c,d){return c*((t=t/d-1)*t*t+1)+b;},easeInOutCubic:function(x,t,b,c,d){if((t/=d/2)<1)return c/2*t*t*t+b;return c/2*((t-=2)*t*t+2)+b;},easeInQuart:function(x,t,b,c,d){return c*(t/=d)*t*t*t+b;},easeOutQuart:function(x,t,b,c,d){return-c*((t=t/d-1)*t*t*t-1)+b;},easeInOutQuart:function(x,t,b,c,d){if((t/=d/2)<1)return c/2*t*t*t*t+b;return-c/2*((t-=2)*t*t*t-2)+b;},easeInQuint:function(x,t,b,c,d){return c*(t/=d)*t*t*t*t+b;},easeOutQuint:function(x,t,b,c,d){return c*((t=t/d-1)*t*t*t*t+1)+b;},easeInOutQuint:function(x,t,b,c,d){if((t/=d/2)<1)return c/2*t*t*t*t*t+b;return c/2*((t-=2)*t*t*t*t+2)+b;},easeInSine:function(x,t,b,c,d){return-c*Math.cos(t/d*(Math.PI/2))+c+b;},easeOutSine:function(x,t,b,c,d){return c*Math.sin(t/d*(Math.PI/2))+b;},easeInOutSine:function(x,t,b,c,d){return-c/2*(Math.cos(Math.PI*t/d)-1)+b;},easeInExpo:function(x,t,b,c,d){return(t==0)?b:c*Math.pow(2,10*(t/d-1))+b;},easeOutExpo:function(x,t,b,c,d){return(t==d)?b+c:c*(-Math.pow(2,-10*t/d)+1)+b;},easeInOutExpo:function(x,t,b,c,d){if(t==0)return b;if(t==d)return b+c;if((t/=d/2)<1)return c/2*Math.pow(2,10*(t-1))+b;return c/2*(-Math.pow(2,-10*--t)+2)+b;},easeInCirc:function(x,t,b,c,d){return-c*(Math.sqrt(1-(t/=d)*t)-1)+b;},easeOutCirc:function(x,t,b,c,d){return c*Math.sqrt(1-(t=t/d-1)*t)+b;},easeInOutCirc:function(x,t,b,c,d){if((t/=d/2)<1)return-c/2*(Math.sqrt(1-t*t)-1)+b;return c/2*(Math.sqrt(1-(t-=2)*t)+1)+b;},easeInElastic:function(x,t,b,c,d){var s=1.70158;var p=0;var a=c;if(t==0)return b;if((t/=d)==1)return b+c;if(!p)p=d*.3;if(a<Math.abs(c)){a=c;var s=p/4;}
else var s=p/(2*Math.PI)*Math.asin(c/a);return-(a*Math.pow(2,10*(t-=1))*Math.sin((t*d-s)*(2*Math.PI)/p))+b;},easeOutElastic:function(x,t,b,c,d){var s=1.70158;var p=0;var a=c;if(t==0)return b;if((t/=d)==1)return b+c;if(!p)p=d*.3;if(a<Math.abs(c)){a=c;var s=p/4;}
else var s=p/(2*Math.PI)*Math.asin(c/a);return a*Math.pow(2,-10*t)*Math.sin((t*d-s)*(2*Math.PI)/p)+c+b;},easeInOutElastic:function(x,t,b,c,d){var s=1.70158;var p=0;var a=c;if(t==0)return b;if((t/=d/2)==2)return b+c;if(!p)p=d*(.3*1.5);if(a<Math.abs(c)){a=c;var s=p/4;}
else var s=p/(2*Math.PI)*Math.asin(c/a);if(t<1)return-.5*(a*Math.pow(2,10*(t-=1))*Math.sin((t*d-s)*(2*Math.PI)/p))+b;return a*Math.pow(2,-10*(t-=1))*Math.sin((t*d-s)*(2*Math.PI)/p)*.5+c+b;},easeInBack:function(x,t,b,c,d,s){if(s==undefined)s=1.70158;return c*(t/=d)*t*((s+1)*t-s)+b;},easeOutBack:function(x,t,b,c,d,s){if(s==undefined)s=1.70158;return c*((t=t/d-1)*t*((s+1)*t+s)+1)+b;},easeInOutBack:function(x,t,b,c,d,s){if(s==undefined)s=1.70158;if((t/=d/2)<1)return c/2*(t*t*(((s*=(1.525))+1)*t-s))+b;return c/2*((t-=2)*t*(((s*=(1.525))+1)*t+s)+2)+b;},easeInBounce:function(x,t,b,c,d){return c-jQuery.easing.easeOutBounce(x,d-t,0,c,d)+b;},easeOutBounce:function(x,t,b,c,d){if((t/=d)<(1/2.75)){return c*(7.5625*t*t)+b;}else if(t<(2/2.75)){return c*(7.5625*(t-=(1.5/2.75))*t+.75)+b;}else if(t<(2.5/2.75)){return c*(7.5625*(t-=(2.25/2.75))*t+.9375)+b;}else{return c*(7.5625*(t-=(2.625/2.75))*t+.984375)+b;}},easeInOutBounce:function(x,t,b,c,d){if(t<d/2)return jQuery.easing.easeInBounce(x,t*2,0,c,d)*.5+b;return jQuery.easing.easeOutBounce(x,t*2-d,0,c,d)*.5+c*.5+b;}});intializePlayer();loadGotham();$(function(){if($("#HTML5MediaCenterContainer").length>0){var mediaResourceCount=$('.mediaResource').size();if(mediaResourceCount==1){$('#HTML5MediaCenterContainer').customMediaPlayer({});}else if(mediaResourceCount>1){$('#HTML5MediaCenterContainer').customMediaPlayer({'delay':3500,'fadeSpeed':200,'mediaCenterExtensions':[new MediaCenterThumbnailExtension()]});}
setUpMediaArea(mediaResourceCount);intializeQuoteTileDefaultMediaCenter();$('#HTML5MediaCenterContainer #mediaResources p.subHeader').each(function(){if($(this).width()>$('#HTML5MediaCenterContainer').width()){$(this).css('font-size','13px');}});var is_safari=navigator.userAgent.indexOf("Safari")>-1;if(!is_safari){_V_.options.flash.swf="Unknown_83_filename"/*tpa=https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/video-js.swf*/;}
_V_.options.flash.iFrameMode=true;}});function setTitleToNewSize(){if($("#pageTitleContainer").size()>0){$(".mainHeader").css("font-size","14px");}}
function setUpMediaArea(resourcesInMediaArea){$("#HTML5MediaCenterContainer .mainheader").css("font-family","Arial Black,Sans-Serif !important");setTitleToNewSize();var isIOS=(navigator.userAgent.match(/(iPad|iPhone|iPod)/g)?true:false);if(isIOS){$("#mcPlayerArea").css("height","470px");}else{$("#mcPlayerArea").css("height","440px");}
if(resourcesInMediaArea==1){if(isIOS){$("#mcPlayerArea").css("height","325px");}else{$("#mcPlayerArea").css("height","310px");}
$("#HTML5MediaCenterContainer").toggleClass("singleAssetPlayer");var firstResourceChild=$('#mediaResources').children()[0].children[0];if($(firstResourceChild).is('p')){$("#HTML5MediaCenterContainer").css('border-top-left-radius','0px');$("#HTML5MediaCenterContainer").css('border-top-right-radius','0px');}
if($('#pageTitleContainer').hasClass('noTitleArea')){$("#HTML5MediaCenterContainer").css('overflow','visible');}
$("#mediaResources p.mainHeader").css("font-size","14px");$(".tileBackgroundImage").toggleClass("singleMediaAssetGradient");}else if(resourcesInMediaArea<=5){if(resourcesInMediaArea==4){$(".bx-viewport").toggleClass("fourThumbnails");}
if(resourcesInMediaArea==3){$(".bx-viewport").toggleClass("threeThumbnails");}
if(resourcesInMediaArea==2){$(".bx-viewport").toggleClass("twoThumbnails");}}
$("#mcTextArea").css("display","none");}
function intializeDefaultNavigationButtons(){$("#nextButtonBackground").click(function(){$(".bx-next").click();});$("#prevButtonBackground").click(function(){$(".bx-prev").click();});$("#prevButtonBackground").hover(function(){if($.browser.msie){$("#prevButtonBackground").toggleClass("playerButtonsBackgroundTileIE");}else{$("#prevButtonBackground").toggleClass("playerButtonsBackgroundTile");}});$("#nextButtonBackground").hover(function(){if($.browser.msie){$("#nextButtonBackground").toggleClass("playerButtonsBackgroundTileIE");}else{$("#nextButtonBackground").toggleClass("playerButtonsBackgroundTile");}});$("#nextButtonBackground").mousedown(function(){if($.browser.msie){$("#nextButtonBackground").toggleClass("playerButtonsBackgroundTileIE");}else{$("#nextButtonBackground").toggleClass("playerButtonsBackgroundTile");}}).mouseup(function(){if($.browser.msie){$("#nextButtonBackground").toggleClass("playerButtonsBackgroundTileIE");}else{$("#nextButtonBackground").toggleClass("playerButtonsBackgroundTile");}});$("#prevButtonBackground").mousedown(function(){if($.browser.msie){$("#prevButtonBackground").toggleClass("playerButtonsBackgroundTileIE");}else{$("#prevButtonBackground").toggleClass("playerButtonsBackgroundTile");}}).mouseup(function(){if($.browser.msie){$("#prevButtonBackground").toggleClass("playerButtonsBackgroundTileIE");}else{$("#prevButtonBackground").toggleClass("playerButtonsBackgroundTile");}});$(".bx-prev").hover(function(){if($.browser.msie){$("#prevButtonBackground").toggleClass("playerButtonsBackgroundTileIE");}else{$("#prevButtonBackground").toggleClass("playerButtonsBackgroundTile");}});}
function intializeDefaultMediaPlayer(){$(".mediaResource").each(function(){if($(this).children(".learnMoreLinkStaticTile").length>0){$(this).children(".subHeader").append($(this).children(".learnMoreLinkStaticTile"));$(this).children(".subHeader").append($(this).children(".learnMoreLinkSpan"));}
var alternativeTitle=$(this).children(".alternativeHeader").text();if(alternativeTitle.length>0){if(alternativeTitle.replace(/\s/g,"")=="none"){$(this).children(".mainHeader").html("<p class='mainHeader'>"+""+"</p>");$(this).children(".subHeader").html("<p class='subHeader'>"+""+"</p>");}else{$(this).children(".mainHeader").html("<p class='mainHeader'>"+alternativeTitle+"</p>");}}});var isiPad=navigator.userAgent.match(/iPad/i)!=null;var is_android=navigator.userAgent.indexOf("Android")>-1;if(isiPad==true){if($(".thumbTitle").length>0){$(".thumbTitle").css("color","black !important");$(".thumbTitle").css("visibility","visible !important");}}}
function intializePlayer(){if($("#HTML5MediaCenterContainer").size()>0){intializeDefaultMediaPlayer();intializeDefaultNavigationButtons();}}
function intializeQuoteTileDefaultMediaCenter(){$(".quoteTileZipcode").focus(function(){$(this).val("");if($(".quoteTileZipcode").hasClass("quoteValidationError")){$(".quoteTileZipcode").toggleClass("quoteValidationError");}
$(".validationDiv").css("visibility","hidden");});$(".quoteTileZipcode").keypress(function(e){var key_codes=[48,49,50,51,52,53,54,55,56,57,0,8,13];if(!($.inArray(e.which,key_codes)>=0)){e.preventDefault();}});$(".quoteTileForm").unbind();$(".quoteTileForm").submit(function(e){if($(".quoteTileZipcode").val().length==5){return true;}else{if(!$(".quoteTileZipcode").hasClass("quoteValidationError")){$(".quoteTileZipcode").toggleClass("quoteValidationError");}
$(".validationDiv").css("visibility","visible");e.preventDefault();return false;}});$(".mediaResource > .mainHeader").each(function(){var titleOfTile=$(this).text();titleOfTile=titleOfTile.replace("&#8482;","<sup class='mainHeaderSuperscript'>TM</sup>");$(this).html("<p class='mainHeader'>"+titleOfTile+"</p>");});if($(".rightAlignedTile").size()>0){$(".rightAlignedTile").each(function(){$(this).parent().find(".validationDiv").toggleClass("rightAlignedValidation");});}
$("#HTML5MediaCenterContainer").css('visibility','visible');$(".bx-loading").css('visibility','hidden');var pageTitle=$("#pageTitleContainer");if(pageTitle.size()>0&&pageTitle.is(":visible")){$("#mediaResources p.mainHeader").css("font-size","14px");$("#mediaResources p.mainHeader").toggleClass("gothamSsm");}}
var myPlayer=new Array();var counter=0;var currentVideoPlaying=null;var isIOS=(navigator.userAgent.match(/(iPad|iPhone|iPod)/g)?true:false);$(".mediaResource").each(function(index){var videoID=$(this).find(".video-js").attr("id");$(".vjs-tech").attr("poster","");if(typeof videoID!="undefined"){myPlayer[counter]=videojs(videoID);counter++;}});if(myPlayer.length>0){if(isIOS){$(".vjs-closeVideo div").toggleClass("nativeCloseButton");}
for(var videoItem in myPlayer){var continuePlaying=true;var videoID=myPlayer[videoItem].id();myPlayer[videoItem].on("play",function(){playVideo(this);currentVideoPlaying=this;videoData={};var videoName=$(".vjs-playing video").attr("name");if(videoName){videoData.Name=videoName;}
videoData.Resource="MCVideo";videoData.Event="play";videoData.Duration=this.duration();videoData.Offset=this.currentTime();VDPlayEvent();});myPlayer[videoItem].on("fullscreenchange",function(){fullscreenChange(this.isFullScreen());var vid=document.getElementById(this.id());handleFullscreen(vid)});myPlayer[videoItem].on("ended",function(){if(!$.browser.mozilla&&this.isFullScreen()){this.cancelFullScreen();}
closeVideo(this);videoData.Event="ended";videoData.Duration=this.duration();videoData.Offset=this.currentTime();VDEndEvent();});}}
$(".vjs-closeVideo div").click(function(){closeVideo(currentVideoPlaying);});function closeVideo(currentVideo){var runCode=true;if(currentVideo==undefined||currentVideo==null){runCode=false}else{runCode=true;}
if(runCode){if(!$.browser.mozilla||($.browser.mozilla&&currentVideo.isFullScreen()==false)){$("#HTML5MediaCenterContainer").toggleClass("videoPlaying");$(".vjs-poster").css("display","block");if($(".vjs-closeVideo div").hasClass("displayCloseButton")){$(".vjs-closeVideo div").toggleClass("displayCloseButton");}}
currentVideo.currentTime(0);currentVideo.pause();if(!$(".vjs-big-play-button").hasClass("displayBigPlayButton")){$(".vjs-big-play-button").toggleClass("displayBigPlayButton");}
if($("#"+currentVideo.id()).hasClass("videoInPlay")&&currentVideo.isFullScreen()==false){$("#"+currentVideo.id()).toggleClass("videoInPlay");}
if(!$.browser.mozilla||($.browser.mozilla&&currentVideo.isFullScreen()==false)){if($("#HTML5MediaCenterContainer").hasClass("videoPlayingInFullscreen")){$("#HTML5MediaCenterContainer").toggleClass("videoPlayingInFullscreen");}
if(currentVideo.controls()){currentVideo.controls(false);}}
var mediaResourcesCount=$('.mediaResource').size();if(mediaResourcesCount==1&&currentVideo.isFullScreen()==false){$("#mcPlayerArea").toggleClass("mcPlayerHeightToggle");}}}
function playVideo(currentVideo){if(!$("#HTML5MediaCenterContainer").hasClass("videoPlaying")){$("#HTML5MediaCenterContainer").toggleClass("videoPlaying");}
if(!$(".vjs-closeVideo div").hasClass("displayCloseButton")&&$(".vjs-using-native-controls").size()==0){$(".vjs-closeVideo div").toggleClass("displayCloseButton");}else if(!$(".vjs-closeVideo div").hasClass("displayCloseButton")&&$(".vjs-using-native-controls").size()>0){$(".vjs-closeVideo div").toggleClass("displayCloseButton");}
if($(".vjs-big-play-button").hasClass("displayBigPlayButton")){$(".vjs-big-play-button").toggleClass("displayBigPlayButton");}
if(currentVideo.controls()==false){currentVideo.controls(true);}
if(!$("#"+currentVideo.id()).hasClass("videoInPlay")){$("#"+currentVideo.id()).toggleClass("videoInPlay");}
var mediaResourcesCount=$('.mediaResource').size();if((mediaResourcesCount==1)&&!$("#mcPlayerArea").hasClass("mcPlayerHeightToggle")){if(!$("#mcPlayerArea").hasClass("mcPlayerHeightToggle")){$("#mcPlayerArea").toggleClass("mcPlayerHeightToggle");}}}
function fullscreenChange(isFullscreenEnabled){if(isFullscreenEnabled==true){if(!$("#HTML5MediaCenterContainer").hasClass("videoPlayingInFullscreen")){$("#HTML5MediaCenterContainer").toggleClass("videoPlayingInFullscreen");}
scroll(0,1000);}else if(isFullscreenEnabled==false){if($("#HTML5MediaCenterContainer").hasClass("videoPlayingInFullscreen")){$("#HTML5MediaCenterContainer").toggleClass("videoPlayingInFullscreen");}
scroll(0,0);}else{}}
function handleFullscreen(videoElement,isFullscreen){if(!videoElement.fullscreen){videoElement.fullscreen=false;}
if(videoElement.requestFullscreen&&videoElement.fullscreen==false){videoElement.msRequestFullscreen();videoElement.fullscreen=true;}
if(document.msExitFullscreen&&videoElement.fullscreen==true){document.msExitFullscreen();videoElement.fullscreen=false;}}
$(document).ready(function(){if($(".blogVideo").size()>=1&&navigator.userAgent.match(/iPad/i)!=null){$("video").each(function(){var srcAttr=$(this).attr("src");if(srcAttr.indexOf(".mp4")!=-1){var videoPath=srcAttr.substring(0,srcAttr.indexOf(".mp4"));var newVideoSrc=videoPath+".m3u8";var environment=srcAttr.substring(0,srcAttr.indexOf("/video/")+7);var videoURL=srcAttr.substring(srcAttr.indexOf("/video/")+7);var m3u8Path=videoURL.substring(0,videoURL.indexOf("/"))+"/ios";var m3u8=videoURL.substring(videoURL.indexOf("/"));var testSrc=environment+m3u8Path+m3u8.replace("mp4","m3u8");newVideoSrc=newVideoSrc.replace("mp4","m3u8");testSrc=testSrc.replace("mp4","m3u8");$(this).attr("src",testSrc);}});}
if($(".blogVideo").size()>=1){var blogPlayer=new Array();$(".blogVideo").each(function(index){blogPlayer[index]=videojs($(this).attr("id"));});for(var videoItem in blogPlayer){var blogShim=$("#"+blogPlayer[videoItem].id()).parent().find(".blogVideoShim");var blogShimSRC=$("#"+blogPlayer[videoItem].id()).parent().find(".blogVideoShim").attr("src");blogPlayer[videoItem].poster(blogShimSRC);$("#"+blogPlayer[videoItem].id()+" .vjs-poster").css("display","block");blogPlayer[videoItem].on("play",function(){videoData={};var videoName=$(".vjs-playing video").attr("name");if(videoName){videoData.Name=videoName;};videoData.Resource="blogVideo";videoData.Event="play";videoData.Duration=this.duration();videoData.Offset=this.currentTime();VDPlayEvent();if(!$("#voltronContentWrapper").hasClass("videoPlaying")){$("#voltronContentWrapper").toggleClass("videoPlaying");}});blogPlayer[videoItem].on("ended",function(){videoData.Event="ended";videoData.Duration=this.duration();videoData.Offset=this.currentTime();VDEndEvent();if(!$.browser.mozilla||($.browser.mozilla&&this.isFullScreen()==false)){this.cancelFullScreen();this.currentTime(0);this.pause();$(".vjs-poster").css("display","block");}
if($("#voltronContentWrapper").hasClass("videoPlaying")){$("#voltronContentWrapper").toggleClass("videoPlaying");}});blogPlayer[videoItem].on("fullscreenchange",function(){if($("#featuredContent").size()>0){if(this.isFullScreen()==true){scroll(0,1000);$("#voltronCenterModSet").hide();$("#voltronCollectionHeader").hide();}else if(this.isFullScreen()==false){scroll(0,0);$("#voltronCenterModSet").show();$("#voltronCollectionHeader").show();}}});};}});MediaCenterThumbnailExtension=function(){}
MediaCenterThumbnailExtension.prototype.init=function(inputProperties){var currentShimIndex=0;var hoveredThumbnailIndex=0;var continueAutoPlay=true;var slider=null;var div=$(inputProperties.resources);var count=div.length;var interval=null;var iOS=(navigator.userAgent.match(/(iPad|iPhone|iPod)/g)?true:false);var mediaObject=this;var widthOfResources=count*inputProperties.resourceWidth;var isInternetExplorer=false;var initThumbnail=true;if($.browser.msie&&$.browser.version<=9){isInternetExplorer=true;loadAllShimResources(inputProperties.resources);}else{$(inputProperties.resourceParent).css("width",widthOfResources+"px");$(inputProperties.resources).css("display","block");$(inputProperties.resources).css("float","left");$(inputProperties.resources).css("position","static");$("#contentWrapper").css("overflow","hidden");$("#homePageBodyMedia").css("overflow","hidden");$(".mediaCenterTileContainer").css("display","none");loadAllShimResources(inputProperties.resources);mediaObject.gotoThumbnail(initThumbnail,0,inputProperties,isInternetExplorer);}
initThumbnail=false;slider=intializeThumnbnailSlider(slider,inputProperties.playerSelector,inputProperties.thumbnails,inputProperties.dilimiterConfig,inputProperties.thumbnailContainer,count);intializeShimSlider(inputProperties.resourceParent,inputProperties.resources,inputProperties.resourceWidth,count,inputProperties.thumbnailClass,inputProperties.thumbnailChildren,slider,inputProperties.dilimiterConfig);var tileStart=calculateWebQuery();if(tileStart!=null){var slideStart=0;currentShimIndex=tileStart;hoveredThumbnailIndex=tileStart;slideStart=Math.ceil((tileStart+1)/5)-1;slider.goToSlide(slideStart);}else{tileStart=0;}
$(inputProperties.thumbnailChildren+":not(.bx-clone):eq("+tileStart+")").toggleClass("selectedThumbnail");div.eq(tileStart).show();initCssProps(div,count);div.eq(tileStart).find(".mediaCenterTileContainer").animate(getAnimProps(div,tileStart),1000);if(count==1){$(inputProperties.thumbnailContainer).css("display","none");$(inputProperties.playerSelector).css("border-style","none");inputProperties.autoPlay=false;$(inputProperties.playerSelector).toggleClass("singleAssetPlayer");}else if(count<=inputProperties.dilimiterConfig){$(".bx-clone").remove();$(".bx-pager, .bx-prev, .bx-next").css("display","none");$("#prevButtonBackground, #nextButtonBackground").css("display","none");$(inputProperties.playerSelector).css('visibility','visible');$(".bx-loading").css('visibility','hidden');interval=setInterval(startMediaPlayer,inputProperties.delay);$(div).click(function(){if(interval!=null){clearInterval(interval);}});}else if(count>inputProperties.dilimiterConfig){interval=setInterval(startMediaPlayer,inputProperties.delay);}
if($("#mediaResources > .autoRotate").length<=0){inputProperties.autoPlay=false;}
$(inputProperties.thumbnailChildren).hover(function(){$(this).addClass("hovered");var hoveredIndex=$(this).index();if(currentShimIndex!=hoveredThumbnailIndex&&hoveredThumbnailIndex!=hoveredIndex){currentShimIndex=hoveredThumbnailIndex;}
$(inputProperties.thumbnailChildren).each(function(){continueAutoPlay=false;if($(this).hasClass(inputProperties.thumbnailToggleClass)){$(this).toggleClass(inputProperties.thumbnailToggleClass);}});hoveredThumbnailIndex=hoveredIndex;if(interval!=null){clearInterval(interval);}
loadShimResource(hoveredIndex,inputProperties.shimImageClass,div);mediaObject.gotoThumbnail(initThumbnail,hoveredIndex,inputProperties,isInternetExplorer,currentShimIndex);$(this).toggleClass(inputProperties.thumbnailToggleClass);},function(){$(this).removeClass("hovered");});$(inputProperties.thumbnailChildren).click(function(){var thumbnailId="."+$(this).attr('class').split(' ')[1];newIndex=$(this).index();if(newIndex!=i){$("."+inputProperties.thumbnailToggleClass).each(function(){$(this).toggleClass(inputProperties.thumbnailToggleClass);});$(thumbnailId).toggleClass(inputProperties.thumbnailToggleClass);mediaObject.gotoThumbnail(initThumbnail,newIndex,inputProperties,isInternetExplorer,i);i=newIndex;}
continueAutoPlay=false;clearInterval(interval);});$(inputProperties.playerSelector).hover(function(){$(".bx-pager-link[href]").each(function(){$(this).removeAttr("href").css("cursor","pointer");var pagerHTML=$(this).parent().html();$(this).click(function(){var slideNumber=$(this).attr("data-slide-index");slider.goToSlide(slideNumber);});});},function(){});$(inputProperties.playerSelector).click(function(){if(count>1){if(interval!=null){clearInterval(interval);}}});function startMediaPlayer(){if(inputProperties.autoPlay!=true||continueAutoPlay!=true){if(interval!=null){clearInterval(interval);}}
if((inputProperties.autoPlay==true&&continueAutoPlay==true)){$(inputProperties.thumbnailChildren).each(function(){if($(this).hasClass(inputProperties.thumbnailToggleClass)&&!$(this).hasClass("hovered")){$(this).toggleClass(inputProperties.thumbnailToggleClass);}});div.eq(currentShimIndex).find(".mediaCenterTileContainer").css(getCssProps(div,currentShimIndex));var prevIndex=currentShimIndex;currentShimIndex=(currentShimIndex+1>=count)?0:currentShimIndex+1;$(inputProperties.thumbnailClass+currentShimIndex).toggleClass(inputProperties.thumbnailToggleClass);if(currentShimIndex%inputProperties.dilimiterConfig==0&&count>inputProperties.dilimiterConfig){$(".bx-next").click();}
if(currentShimIndex==0){continueAutoPlay=false;}
mediaObject.gotoThumbnail(initThumbnail,currentShimIndex,inputProperties,isInternetExplorer,prevIndex);}}}
MediaCenterThumbnailExtension.prototype.nextThumbnail=function(){}
MediaCenterThumbnailExtension.prototype.prevThumbnail=function(){}
MediaCenterThumbnailExtension.prototype.nextSlide=function(){}
MediaCenterThumbnailExtension.prototype.prevSlide=function(){}
MediaCenterThumbnailExtension.prototype.gotoThumbnail=function(initThumbnail,index,properties,isIE,previousIndex){if(isIE==false){$(properties.resourceParent).css("-webkit-transform","translateX("+index*-properties.resourceWidth+"px)");$(properties.resourceParent).css("transform","translateX("+index*-properties.resourceWidth+"px)");$(properties.resources).eq(index).find(properties.mediaCenterTileContainer).css('display','block');if(initThumbnail==false){$(properties.resources).eq(index).find(properties.mediaCenterTileContainer).animate(getAnimProps($(properties.resources),index),1000);}}else{$(properties.resources).each(function(){$(this).css("display","none");});$(properties.resources).eq(previousIndex).fadeOut(properties.fadeSpeed);$(properties.resources).eq(index).fadeIn(properties.fadeSpeed);$(properties.resources).eq(index).find(properties.mediaCenterTileContainer).css('display','block');$(properties.resources).eq(index).find(properties.mediaCenterTileContainer).animate(getAnimProps($(properties.resources),index),1000);}}
function intializeThumnbnailSlider(contentSlider,playerSelector,thumbnailContainer,resourcesPerSlide,thumbnailParentContainer,resourceSize){var isTouchEnabled=true;if(resourceSize<=resourcesPerSlide){isTouchEnabled=false;}
contentSlider=$(thumbnailContainer).bxSlider({slideWidth:90,minSlides:1,preventDefaultSwipeX:false,touchEnabled:isTouchEnabled,maxSlides:resourcesPerSlide,slideMargin:13,speed:500,onSliderLoad:function(){$(playerSelector).css('visibility','visible');$(".thumbTitle").css("visibility","visible");$("#thumbnails").css("left","0px");$(".bx-clone").remove();},infiniteLoop:true,onSlideAfter:function(){}});$(thumbnailParentContainer).find(".bx-next").unbind("click");$(thumbnailParentContainer).find(".bx-prev").unbind("click");$(thumbnailParentContainer).find(".bx-next").click(function(e){e.preventDefault();var totalSlides=Math.ceil(contentSlider.getSlideCount()/resourcesPerSlide);totalSlides=totalSlides-1;if(contentSlider.getCurrentSlide()==totalSlides){contentSlider.goToSlide(0);}else{contentSlider.goToNextSlide();}});$(thumbnailParentContainer).find(".bx-prev").click(function(e){e.preventDefault();if(contentSlider.getCurrentSlide()!=0){contentSlider.goToPrevSlide();}});return contentSlider;}
function loadShimResource(mediaResourceIndex,resourceSelector,listOfResources){if($(listOfResources.eq(mediaResourceIndex)).find("img").hasClass(resourceSelector)){var src=$(listOfResources.eq(mediaResourceIndex)).find("img").attr("src");var newsrc=$(listOfResources.eq(mediaResourceIndex)).find("img").attr("newsrc");if(src==""){$(listOfResources.eq(mediaResourceIndex)).find("img").attr("src",newsrc).delay(200);}}}
function loadAllShimResources(mediaResources){$(mediaResources).each(function(){var src=$(this).find("img").attr("src");var newSrc=$(this).find("img").attr("newsrc");if(src==""){$(this).find("img").attr("src",newSrc);}else{}});}
function intializeShimSlider(resourceSelector,resourcesToLoad,width,amountOfResources,thumbs,thumbChildren,theSlider,resourcesPerSlide){continueAutoPlay=true;var originalPageX=0;var isMouseDown=false;var endX=0;var continueShimMovement=true;$(resourcesToLoad).bind("touchstart",function(e){if(e.srcElement.className=="vjs-poster"||e.srcElement.className=="tileBackgroundImage"){e.preventDefault();continueShimMovement=true;var calculatedSlide=Math.floor($(this).index()/resourcesPerSlide);if(calculatedSlide!=theSlider.getCurrentSlide()){theSlider.goToSlide(calculatedSlide);}
var totalWidth=$(resourcesToLoad).size()*width;if($(resourceSelector).hasClass("addTransitionTime")){$(resourceSelector).toggleClass("addTransitionTime");}
$(resourceSelector).css("-webkit-transform","translateX("+$(this).index()*-width+"px)");originalPageX=e.originalEvent.touches[0].pageX;isMouseDown=true;$(this).bind('touchmove',function(evn){if(continueShimMovement==false){return false;}
evn.preventDefault();var distanceMoved=originalPageX-evn.originalEvent.touches[0].pageX;$(resourceSelector).css("-webkit-transform","translateX("+($(this).index()*-width-distanceMoved)+"px)");endX=evn.originalEvent.touches[0].pageX;});}});$(resourcesToLoad).bind('touchend',function(ev){if(ev.srcElement.className=="vjs-poster"||ev.srcElement.className=="tileBackgroundImage"){if(continueShimMovement==false){return false;}
$(resourceSelector).toggleClass("addTransitionTime");$(thumbChildren).each(function(){continueAutoPlay=false;if($(this).hasClass("selectedThumbnail")){$(this).toggleClass("selectedThumbnail");}});var distanceMoved=endX-originalPageX;isMouseDown=false;var newSelectionIndex=0;if(distanceMoved>100&&$(this).index()!=0){$(resourceSelector).css("-webkit-transform","translateX("+($(this).index()-1)*-width+"px)");$(thumbs+($(this).index()-1)).toggleClass("selectedThumbnail");newSelectionIndex=$(this).index()-1;if(($(this).index()-1)%5==4){$(".bx-prev").click();}}else if(distanceMoved<-100&&$(this).index()!=(amountOfResources-1)){$(resourceSelector).css("-webkit-transform","translateX("+($(this).index()+1)*-width+"px)");$(thumbs+($(this).index()+1)).toggleClass("selectedThumbnail");newSelectionIndex=$(this).index()+1;if(($(this).index()+1)%5==0){$(".bx-next").click();}}else{$(resourceSelector).css("-webkit-transform","translateX("+$(this).index()*-width+"px)");newSelectionIndex=$(this).index();$(thumbs+($(this).index())).toggleClass("selectedThumbnail");}
$(".mediaCenterTileContainer").css("display","block");$(resourceSelector).toggleClass("addTransitionTime");$(resourcesToLoad).each(function(index){if(index!=newSelectionIndex){}else{$(resourcesToLoad).eq(newSelectionIndex).find(".mediaCenterTileContainer").animate(getAnimProps($(resourcesToLoad),newSelectionIndex),1000);}});originalPageX=0;$(this).unbind("touchmove");}});}
function calculateWebQuery(){var calculatedQuery="";var thumbIndex=null;if(window.location.search==""){}else if(window.location.search!=""&&window.location.search.indexOf("movie=")!=-1){if(window.location.search.indexOf("&")!=-1){calculatedQuery=window.location.search.substring(window.location.search.indexOf("movie=")+6);if(calculatedQuery.indexOf("&")!=-1){calculatedQuery=calculatedQuery.substring(0,calculatedQuery.indexOf("&"));}}else{calculatedQuery=window.location.search.substring(window.location.search.indexOf("movie=")+6);}
$(".mediaResource").each(function(index){if($(this).attr("class").indexOf(calculatedQuery)!=-1){thumbIndex=index;}});}else{}
return thumbIndex;}
function initializeMediaCenterSingleTile(inputProperties){var div=$(inputProperties.resources);var count=div.length;var iOS=(navigator.userAgent.match(/(iPad|iPhone|iPod)/g)?true:false);$(inputProperties.resourceParent).css("-webkit-transform","translateX("+0*-inputProperties.resourceWidth+"px)");$(inputProperties.resourceParent).css("transform","translateX("+0*-inputProperties.resourceWidth+"px)");$(inputProperties.resources).eq(0).find(inputProperties.mediaCenterTileContainer).css('display','block');$(inputProperties.resources).eq(0).find(inputProperties.mediaCenterTileContainer).animate(getAnimProps($(inputProperties.resources),0),1000);$(inputProperties.playerSelector).css('visibility','visible');loadShimResource(0,inputProperties.shimImageClass,div);div.eq(0).show();initCssProps(div,count);div.eq(0).find(inputProperties.mediaCenterTileContainer).animate(getAnimProps(div,0),1000);$(inputProperties.thumbnailContainer).css("display","none");$(inputProperties.playerSelector).css("border-style","none");}
jQuery().ready(function(){$("#agentSetMsg").fadeIn(700).fadeTo(4000,1).fadeOut(2000);$("a#whatsThisLink").mouseover(function(){$("div#whatsThisHelp").show();});$("div#whatsThisHelp").hover(function(){},function(){$("div#whatsThisHelp").hide();});$("body.agentPageV2 .agentBioArea #agentBioExpandableArea div:first-child div p").addClass("salesRepBio")
$("#slideLeftButton, #slideRightButton, #carouselToggle").show();$("ul.agentAwardList li a.tooltip").click(function(e){e.preventDefault();});var heightLimit=190;if($('#agentBioExpandableArea').height()-20>heightLimit){$('#agentBioExpandableArea').addClass("agentBioExpandableArea");}else{$('#agentBioExpandableArea').addClass("noOverflow");}
if($('#agentBioExpandableArea').hasClass("noOverflow")&&$('.agentAwardsInfoArea').length==0){$("#agentBioToggle").css('display','none');}else{$("#agentBioToggle").css('display','block');}
$("#agentBioToggle").click(function(e){e.preventDefault();if(!$('#agentBioExpandableArea').hasClass("noOverflow")){if($('#agentBioExpandableArea').hasClass("agentBioExpandableArea")){$('#agentBioExpandableArea').removeClass("agentBioExpandableArea");}else{$('#agentBioExpandableArea').addClass("agentBioExpandableArea");}}
$(".agentAwardsInfoArea").toggle();$('.more').toggle();$('.close').toggle();});$("#carouselToggle a").click(function(e){e.preventDefault();var theHeight=$("body.agentPageV2 .sliderCMContainer ul").height();if($('#noCarouselContainer').children().length<=0){var theHeight=theHeight*$("#sliderCMContainer").children("ul").length;$('#sliderCMContainer').clone().appendTo('#noCarouselContainer');$("#noCarouselContainer ul").css("position","relative").css("opacity","1").css("display","block");$("#noCarouselContainer").css("height",theHeight+"px");}
$('#carouselContainer').toggle();$('#noCarouselContainer').toggle();$('#carouselViewToggle .viewAll').toggle();$('#carouselViewToggle .viewLess').toggle();});if($("#sliderCMContainer").children("ul").length==1){$('#sliderCMContainer').clone().appendTo('#noCarouselContainer');$("#noCarouselContainer ul").css("position","relative").css("opacity","1").css("display","block");var theHeight=theHeight*$("#sliderCMContainer").children("ul").length;$("#noCarouselContainer").css("height",theHeight+"px");$('#carouselContainer').toggle();$('#noCarouselContainer').toggle();$('#carouselViewToggle').hide();}});jQuery().ready(function(){var loc=window.location.href;var idx=loc.lastIndexOf("#question");if(idx!=null&&idx!=-1)
{id=loc.substring(idx+1);$("h3#"+id).removeClass("closed");$("h3#"+id).addClass("open");runEffect(id.substring(8));}
$("h3.question").click(function(){if($(this).hasClass('closed')){$(this).removeClass("closed");$(this).addClass("open");$(this).find('.faqState').html('-');}
else{$(this).removeClass('open');$(this).addClass("closed");$(this).find('.faqState').html('+');}
var questionId=$(this).attr("id");runEffect(questionId.substr(8));return false;});$("div#expandAll").click(function(){$(this).css('display','none');$('div#collapseAll').css('display','block');$("h3.question").each(function(){$(this).removeClass("closed");$(this).addClass("open");});$("div.answer").css('display','block');});$("div#collapseAll").click(function(){$(this).css('display','none');$('div#expandAll').css('display','block');$("h3.question").each(function(){$(this).removeClass("open");$(this).addClass("closed");});$("div.answer").css('display','none');});});function runEffect(id){var selectedEffect="blind";var options={};var effectId="#answer"+id;$(effectId).toggle(selectedEffect,options,500);};jQuery().ready(function(){var loc=window.location.href;var idx=loc.lastIndexOf("#question");if(idx!=null&&idx!=-1)
{id=loc.substring(idx+1);id='question['+id.replace('question','')+']';$(escapeCharacters(".expandingContent h4#"+id)).removeClass("closed");$(escapeCharacters(".expandingContent h4#"+id)).addClass("open");showHideContent(id.substring(8));var pos=$('.expandingContent h4.open').offset();window.scrollTo(0,pos.top)}
if($("div#expandCollapseAll").hasClass('expanded')){$(".expandingContent h4.question").each(function(){$(this).removeClass("closed");$(this).addClass("open");});$(".expandingContent div.answer").css('display','block');}
$(".expandingContent h4.question").click(function(){if($(this).hasClass('closed')){$(this).removeClass("closed");$(this).addClass("open");}
else{$(this).removeClass('open');$(this).addClass("closed");}
var questionId=$(this).attr("id");showHideContent(questionId.substr(8));return false;});$("div#expandCollapseAll").click(function(){if($(this).hasClass('expanded')){$(".expandingContent h4.question").each(function(){$(this).removeClass("open");$(this).addClass("closed");});$(".expandingContent div.answer").css('display','none');$(this).removeClass("expanded");$(this).addClass("collapsed");$(this).text('Expand All');}else{$(".expandingContent h4.question").each(function(){$(this).removeClass("closed");$(this).addClass("open");});$(".expandingContent div.answer").css('display','block');$(this).removeClass("collapsed");$(this).addClass("expanded");$(this).text('Collapse All');}});});function showHideContent(id){var selectedEffect="blind";var options={};var effectId=escapeCharacters(".expandingContent #answer"+id);$(effectId).toggle(selectedEffect,options,500);};function escapeCharacters(a){a=a.replace("[","\\[");a=a.replace("]","\\]");return a;};$(function(){$('#officesAccordion').accordion({autoheight:false,header:".offAccordHead",active:".offActive"});$('#officesNewAccordion').accordion({autoheight:false,header:".offAccordHead",active:".offActive",alwaysOpen:false});$('#mapResults').accordion({autoheight:false,header:".mapAccordHead",active:".mapActive",alwaysOpen:false});$('#mapAgentResults').accordion({autoheight:false,header:".mapAccordHead",active:".mapActive",alwaysOpen:false});$('#mapDirectionsResults').accordion({autoheight:false,header:".mapAccordHead",active:".mapActive",alwaysOpen:false});});$(document).ready(function(){if($(location).attr('href').indexOf('find-sales-office-results')>=0||$(location).attr('href').indexOf('find-claims-office-results')>=0||$(location).attr('href').indexOf('sales-representative-search-results')>=0){$('#nonavBreadcrumbBar').addClass('toShow');}
if($('#allAgents').children().length==0&&$('#officeReps')){$('#officeReps').css('display','none');}
$(".expanded").css("background","url('../../images/agentCard-expanded.png') no-repeat");});$(function(){if($("#getDirectionsIsPresent").length>0){zoom=1;panh=0;panv=0;$("div#mapWrapper").show();$("https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/ul#zoomControl, .zoomButton, .zoom").show();$(".zoomButton").click(function(){changeDirectionsMap($(this),defaultStateChanged,'directionsMap');return false;});}
$("form[id*=directionsAddressForm]").submit(function(){$("#directionsZip, #directionsAddress, #directionsCity").keyup(function(){var result=checkDirectionsForm();});var result=checkDirectionsForm();if(result==false){return false;}});});var changeDirectionsMap=function(buttonRef,stateChanged,ajaxService){$("#nearbyMapURL").attr("src","Unknown_83_filename"/*tpa=https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/images/mapimage.gif*/);var buttonID=$(buttonRef).attr("id");var passOption=1;var passZoom=null;var zoomLevelSet=$("ul#zoomControl li a");zoomLevelSet=jQuery.grep(zoomLevelSet,function(n,i){return(i!=0&&i!=6);});switch(buttonID){case'north':passOption=5;break;case'south':passOption=6;break;case'east':passOption=3;break;case'west':passOption=4;break;case'zoomIn':passOption=1;break;case'zoomOut':passOption=2;break;case'zoom1':passOption=1;passZoom=0.25;$(zoomLevelSet).removeClass("selected");$(zoomLevelSet[0]).addClass("selected");break;case'zoom2':passOption=1;passZoom=0.5;$(zoomLevelSet).removeClass("selected");$(zoomLevelSet[1]).addClass("selected");break;case'zoom3':passOption=1;passZoom=1;$(zoomLevelSet).removeClass("selected");$(zoomLevelSet[2]).addClass("selected");break;case'zoom4':passOption=1;passZoom=2;$(zoomLevelSet).removeClass("selected");$(zoomLevelSet[3]).addClass("selected");break;case'zoom5':passOption=1;passZoom=4;$(zoomLevelSet).removeClass("selected");$(zoomLevelSet[4]).addClass("selected");break;}
showNearbyMap(passOption,passZoom,stateChanged,ajaxService);}
var checkDirectionsForm=function(){var errorMsg="";var trimmedZip=jQuery.trim($("#directionsZip").attr('value'));$("#directionsZip").attr('value',trimmedZip);var trimmedAddr=jQuery.trim($("#directionsAddress").attr('value'));$("#directionsAddress").attr('value',trimmedAddr);var trimmedCity=jQuery.trim($("#directionsCity").attr('value'));$("#directionsCity").attr('value',trimmedCity);if(trimmedZip==''&&trimmedAddr==''&&trimmedCity==''){$("#formErrors ul").html("<li>You must enter either an address OR a zip code in order to obtain directions.</li>");$("#formErrors").show();$("#directionsZip, #directionsAddress, #directionsCity").addClass("error");return false;}else if(trimmedZip!=''&&(trimmedZip.length<5)||isNaN(trimmedZip)){$("#directionsZip, #directionsAddress, #directionsCity").removeClass("error");$("#directionsZip").addClass("error");$("#formErrors ul").html("<li>Zip code must be a five-digit number.</li>");$("#formErrors").show();return false;}else{$("#formErrors").hide();$("#directionsZip, #directionsAddress, #directionsCity").removeClass("error");}}
var map=null;var frmNum=100;var locations=new Array();$(window).load(function(){if($("#myMap").length>0){GetMap("myMap");var startLat=document.getElementById("mapStartLat").value;var startLong=document.getElementById("mapStartLong").value;map.LoadMap(new VELatLong(startLat,startLong),10,VEMapStyle.Road,false,VEMapMode.Mode2D,false,0,GetMapOptions());LoadOffices();if(map.GetZoomLevel()>17){map.SetZoomLevel(17);}}
if($("#myAgentMap").length>0){GetMap("myAgentMap");map.LoadMap(null,10,VEMapStyle.Road,false,VEMapMode.Mode2D,false,0,GetMapOptions());LoadReps();if(map.GetZoomLevel()>17){map.SetZoomLevel(17);}}
if($("#myDirectionsMap").length>0){GetMap("myDirectionsMap");var startLat=document.getElementById("mapStartLat").value;var startLong=document.getElementById("mapStartLong").value;var latLng=null;if(startLat!=''){latLng=new VELatLong(startLat,startLong);}
map.LoadMap(latLng,12,VEMapStyle.Road,false,VEMapMode.Mode2D,false,0,GetMapOptions());LoadDirectionsOffice();}
if($("#localOfficeMap").length>0){GetMap("localOfficeMap");calcHeight=$('#officeDescription').height();$("#localOfficeMap").height(calcHeight-35);var startLat=document.getElementById("mapStartLat").value;var startLong=document.getElementById("mapStartLong").value;var latLng=null;if(startLat!=''){latLng=new VELatLong(startLat,startLong);}
map.LoadMap(latLng,12,VEMapStyle.Road,false,VEMapMode.Mode2D,false,0,GetMapOptions());LoadDirectionsOffice();}
$('input[id=startAddress]').keypress(function(e){if(e&&e.keyCode==13){e.preventDefault();$("#getDirectonsButton").click();return false;}});$("form[id=getDirectionsForm]").submit(function(e){e.preventDefault();});$("#getDirectonsButton").click(function(){if($("form[id=getDirectionsForm]").validate().form()){map.Clear();var startAdd=$('input[id=startAddress]').val();var startLat=document.getElementById("mapStartLat").value;var startLong=document.getElementById("mapStartLong").value;var latLng=new VELatLong(startLat,startLong);$('span[id=displayStartAdd]').html(startAdd);var options=new VERouteOptions();options.DrawRoute=true;options.SetBestMapView=true;options.ShowDisambiguation=true;options.RouteCallback=onGotRoute;map.GetDirections([startAdd,latLng],options);}});$("h4[id^=officeTitle_]").click(function(){if($(this).hasClass('selected')){$('#officesNewAccordion').accordion("activate",-1);}});$('h4[id^=officeTitle_]').click(function(){var officenum=this.id;officenum=officenum.substr(12);var agentDivSecID='agentsSection_'+officenum;var secHtml=$('div[id='+agentDivSecID+']').html();if(secHtml.length<=10){$.ajax({url:appJSGlobals.ajaxRequestHandlerUrl
+"?ajaxRequest=officeSalesRepInfoRequest"
+"&officenumber="+officenum,type:"get",dataType:"text",success:function(data){if(data!=""){var myJSONObject=eval('('+data+')');var agentDivId='agentsSection_'+myJSONObject.salesreps[0].officeNumber;var cmsContactUrl=$('input[id=cmsPageUrl_salesRepContactMe]').val();var agentSection="";for(var i=0;i<myJSONObject.salesreps.length;i=i+1)
{agentSection=agentSection+createAgentInfoSection(myJSONObject.salesreps[i],cmsContactUrl);}
$('div[id='+agentDivId+']').html(agentSection);$('div[id='+agentDivId+']').show("slow");}},error:function(data){}});}});});function GetMap(mapName)
{map=new VEMap(mapName);map.SetCredentials(document.getElementById("mapAuthKey").value);}
function GetMapOptions(){var mapOptions=new VEMapOptions();mapOptions.UseEnhancedRoadStyle=true;mapOptions.EnableBirdseye=false;return mapOptions;}
function createAgentInfoSection(salesRep,cmsContactUrl){var infoSec='<div class="agentInfoSection">';infoSec=infoSec+'<a href="##HOMEPAGE##"><img width="##IMGWIDTH##" height="##IMGHEIGHT##" alt="" src="##IMGSRC##"></a>';infoSec=infoSec+'<ul><li><h4>##REPNAME####QUALIFICATIONS##</h4></li>'
infoSec=infoSec+'<li>##PHONEDIRECT##</li>'
infoSec=infoSec+'<li><a href="'+cmsContactUrl+'?alias=##ALIAS##">Email this Agent</a></li>'
infoSec=infoSec+'<li><a href="##HOMEPAGE##">Visit Agent\'s Home Page</a></li></ul>'
var quoteRestrict=document.getElementById("getQuoteRestrict").value;if(quoteRestrict=="false"){infoSec+=createAgentQuoteForm(salesRep);}
infoSec+='<ul id="agentSubSection">';if(salesRep.licenseInfo){infoSec=infoSec+'<li>##LICENSEINFO##</li>'}
if(salesRep.license){infoSec=infoSec+'<li>##LICENSE##</li>'}
if(salesRep.languages){infoSec=infoSec+'<li>Languages Spoken: ##LANGUAGES##</li>'}
infoSec=infoSec+'</ul></div>';infoSec=infoSec.replace("##ALIAS##",salesRep.alias);infoSec=infoSec.replace("##IMGSRC##",salesRep.repImg);infoSec=infoSec.replace("##IMGWIDTH##",salesRep.repImgWidth);infoSec=infoSec.replace("##IMGHEIGHT##",salesRep.repImgHeight);infoSec=infoSec.replace(/##HOMEPAGE##/g,salesRep.agentHomepage);infoSec=infoSec.replace("##REPNAME##",salesRep.name);var qual=salesRep.qualification;if(qual!=null){qual="<span id='repQualification'>"+qual+"</span>";}else{qual="";}
infoSec=infoSec.replace("##QUALIFICATIONS##",qual);infoSec=infoSec.replace("##LICENSE##",salesRep.license);infoSec=infoSec.replace("##LICENSEINFO##",salesRep.licenseInfo);infoSec=infoSec.replace("OFFICENUMBER##",salesRep.officeNumber);infoSec=infoSec.replace("##PHONEDIRECT##",salesRep.phoneDirect);infoSec=infoSec.replace("##REPNUM##",salesRep.repNumber);infoSec=infoSec.replace("##REPWEBID##",salesRep.repWebId);infoSec=infoSec.replace("##LANGUAGES##",salesRep.languages);return infoSec;}
function createAgentQuoteForm(salesRep){var frmSec='<form id="getQuoteForm##NUM##" action="https://www.libertymutual.com/ajaxRequestHandler.ado?ajaxRequest=submissionHandler&bean=GoGetAQuoteBean.goGetAQuoteNative" enctype="application/x-www-form-urlencoded" method="post">';frmSec+='<ul class="agentInfoRightCol"><li><div id="getQuoteForm##NUM##Errors" class="officeGetQuoteError hidden"></div>';frmSec+='<label for="quoteType##NUM##"><span id="officeSearchProductTypeLabel##NUM##">Get a Quote</span></label></li><li>';frmSec+='<select name="quoteType##NUM##" id="quoteType##NUM##">';var agentSelectItems=salesRep.selectItems;for(var i=0;i<agentSelectItems.length;i=i+1)
{frmSec+='<option value="';frmSec+=agentSelectItems[i].selValue;frmSec+='">';frmSec+=agentSelectItems[i].selLabel;frmSec+='</option>';}
frmSec+='</select></li>';frmSec+='<li><label class="subLabel" for="quoteZipCode##NUM##">Enter Zip Code:</label></li>';frmSec+='<li><input type="text" name="quoteZipCode##NUM##" id="quoteZipCode##NUM##" style="margin-right: 4px;"><input type="hidden" value="##NUM##" name="formID">';frmSec+='<input type="hidden" name="pagename##NUM##" id="pagename##NUM##" value="/find-sales-office-results">';frmSec+='<input type="hidden" name="officeSearchSalesRepWebId##NUM##" id="officeSearchSalesRepWebId##NUM##" value="##REPWEBID##">';frmSec+='<input type="hidden" name="officeSearchSalesRepNumber##NUM##" id="officeSearchSalesRepNumber##NUM##" value="##REPNUM##">';frmSec+='<button title="Start" value="" id="officeSearchStartQuote" type="submit" class="short-button orange-white-button">Start</button>';frmSec+='</li></ul></form>'
frmSec=frmSec.replace(/##NUM##/g,frmNum);frmNum+=1;return frmSec;}
function LoadOffices(){var shapes=new Array();var mapData=document.getElementById("officeMapData").value;var shapeLayer=new VEShapeLayer();var options=new VEClusteringOptions();var customIcon=new VECustomIconSpecification();options.Callback=ClusteringCallback;var myJSONObject=eval('('+mapData+')');var officeSection="";if(myJSONObject.offices!='undefined'&&myJSONObject.offices!=null){for(var i=0;i<myJSONObject.offices.length;i=i+1)
{var office=myJSONObject.offices[i];var latlong=new VELatLong(office.latitude,office.longitude);var title="<a href='"+office.alias+"'>"+office.offName+"</a>";var desc=office.address+"<br>"+office.city+", "+office.state;shapes[i]=createShape(latlong,title,desc,office.offNum);shapeLayer.AddShape(shapes[i]);locations[i]=office.offNum;}
shapeLayer.SetClusteringConfiguration(VEClusteringType.Grid,options);map.AddShapeLayer(shapeLayer);map.SetMapView(shapeLayer.GetBoundingRectangle());}}
function createShape(latLong,title,desc,uniqueId){var shape=new VEShape(VEShapeType.Pushpin,latLong);shape.SetCustomIcon("<div id='mapIconDiv'><img width='35px' height='47px' src='Unknown_83_filename'/*tpa=https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/images/iepngfix/blank.gif*/ alt='' id='icon"+uniqueId+"' name='icon"+uniqueId+"' onmouseover='officeMouseOver(\""+uniqueId+"\")' onmouseout='officeMouseOut(\""+uniqueId+"\")' /></div>");shape.SetTitle(title);shape.SetDescription(desc);return shape;}
function ClusteringCallback(clusters){try{if(clusters!=null){for(var i=0;i<clusters.length;i++){var cluster=clusters[i];var clusterShape=clusters[i].GetClusterShape();clusterShape.SetCustomIcon("<div id='mapMultiIconDiv'><img width='42px' height='52px' src='Unknown_83_filename'/*tpa=https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/images/iepngfix/blank.gif*/ alt='' id='cluster"+i+"' name='cluster"+i+"' /></div>");clusterShape.SetTitle("There are "+cluster.Shapes.length+" offices here:");var description="";for(var s=0;s<cluster.Shapes.length;s++){var shape=cluster.Shapes[s];description+="<div style='font-size: x-small;'>"+cluster.Shapes[s].Title+"</div>";}
description+="<div style='font-size: x-small;'><br>Zoom in for details.</div>";clusterShape.SetDescription(description);}}}catch(e){alert(e.message);}}
function ClusteringRepCallback(clusters){try{if(clusters!=null){for(var i=0;i<clusters.length;i++){var cluster=clusters[i];var clusterShape=clusters[i].GetClusterShape();clusterShape.SetCustomIcon("<div id='mapMultiIconDiv'><img width='42px' height='52px' src='Unknown_83_filename'/*tpa=https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/images/iepngfix/blank.gif*/ alt='' id='cluster"+i+"' name='cluster"+i+"' /></div>");clusterShape.SetTitle("There are "+cluster.Shapes.length+" sales representatives here:");var description="";for(var s=0;s<cluster.Shapes.length;s++){var shape=cluster.Shapes[s];description+="<div style='font-size: x-small;'>"+cluster.Shapes[s].Title+"</div>";}
description+="<div style='font-size: x-small;'><br>Zoom in for details.</div>";clusterShape.SetDescription(description);}}}catch(e){alert(e.message);}}
function officeMouseOver(id){var officeCard=document.getElementById("officeTitle_"+id);if(officeCard!=null){if($('h4[id=officeTitle_'+id+']').hasClass('selected')==false){$('h4[id=officeTitle_'+id+']').click();for(var i=0;i<locations.length;i=i+1){if(id==locations[i]){$('#officesNewAccordion').accordion("activate",i)}}}}}
function officeMouseOut(id){var officeCard=document.getElementById("officeTitle_"+id);}
function LoadReps(){var shapes=new Array();var mapData=document.getElementById("repMapData").value;var shapeLayer=new VEShapeLayer();var options=new VEClusteringOptions();var customIcon=new VECustomIconSpecification();options.Callback=ClusteringRepCallback;var myJSONObject=eval('('+mapData+')');for(var i=0;i<myJSONObject.salesreps.length;i=i+1)
{var rep=myJSONObject.salesreps[i];var latlong=new VELatLong(rep.latitude,rep.longitude);var title="<a href='"+rep.repAlias+"'>"+rep.repName+"</a>";var desc=rep.offName+"<br>"+rep.repPhone;shapes[i]=createShape(latlong,title,desc,rep.repAlias);shapeLayer.AddShape(shapes[i]);locations[i]=myJSONObject.salesreps[i].offNum;}
shapeLayer.SetClusteringConfiguration(VEClusteringType.Grid,options);map.AddShapeLayer(shapeLayer);map.SetMapView(shapeLayer.GetBoundingRectangle());}
function onGotRoute(route)
{var legs=route.RouteLegs;var turns="";var leg=null;var routeText=document.getElementById("routeDistance");routeText.innerHTML="<strong>Route: "+route.Distance.toFixed(1)+" miles, "+secondsToHms(route.Time)+"</strong>";turns+="<ul>";for(var i=0;i<legs.length;i++)
{leg=legs[i];var turn=null;var dispNum="";for(var j=0;j<leg.Itinerary.Items.length;j++)
{turns+="<li>";turn=leg.Itinerary.Items[j];dispNum=j;if(j==0){dispNum="START: ";}else if(j==leg.Itinerary.Items.length-1){dispNum="END: "}else{dispNum=j+". ";}
turns+="<div class='dirLeg'>"+dispNum+turn.Text+"</div><div class='dirDist'>"+turn.Distance.toFixed(1)+" mi</div>";turns+="</li>";}}
turns+="</ul>";turns="<div>"+turns+"</div>";$('li[id=routePoints]').html(turns);}
function LoadDirectionsOffice(){var startLat=document.getElementById("mapStartLat").value;var startLong=document.getElementById("mapStartLong").value;var latlong=new VELatLong(startLat,startLong);var shape=createShape(latlong,null,null,"dirOffice");var shapeLayer=new VEShapeLayer();shapeLayer.AddShape(shape);map.AddShapeLayer(shapeLayer);}
function secondsToHms(d){d=Number(d);var h=Math.floor(d/3600);var m=Math.floor(d%3600/60);var s=Math.floor(d%3600%60);return((h>0?h+" hrs ":"")+(m>0?m+" min ":"")+(m==0&&h==0?" < 1 min ":""));}
function registerScrollCheckOnAddThis(){function isScrolledIntoView(elem)
{var docViewTop=$(window).scrollTop();var docViewBottom=docViewTop+$(window).height();var elemTopObj=$(elem).offset();if(!elemTopObj){return false;}else{var elemTop=$(elem).offset().top;return((elemTop<=docViewBottom)&&(elemTop>=docViewTop));}}
var scrollHandlerArray=['.socialBarBottom','.socialBar','.addthisBreadCrumb','.socialFeed','#share','#socialLikeContainer'];var scrollHandler=function(){$.each(scrollHandlerArray,function(key,value){var elem=$(value);if(elem.length>0&&isScrolledIntoView(elem)){$(window).unbind("load resize scroll",scrollHandler);initSocialMedia();return false;}});}
$(window).bind("load resize scroll",scrollHandler);}
function initSocialMedia(){function getNewLocation(){var environment="../../../index-1.htm"/*tpa=http://www.libertymutual.com/*/;var urlLocation=location.protocol+'//'+location.host+location.pathname;var index=urlLocation.indexOf('.libertymutual.com');var newLocation="";if(index!=-1){newLocation=environment+urlLocation.substring((index+18),urlLocation.length);}else{if(urlLocation.split(":").length-1>=2){var subhttp=urlLocation.substring(7,urlLocation.length);index=subhttp.indexOf("/");if(index!=-1){newLocation=environment+subhttp.substring((index),subhttp.length);}}}
return newLocation;}
if($('.sharethis').length>0){$('.addthis').parent().addClass('sharethis_toolbox');var switchTo5x=true;var script='../../../../w.sharethis.com/button/buttons.js'/*tpa=http://w.sharethis.com/button/buttons.js*/;$('.sharethis').attr('st_url',getNewLocation());$.getScript(script,function(){stLight.options({publisher:"c54c8a67-a8c2-4571-9016-f6141d12900f",doNotHash:false,doNotCopy:false,nativeCount:true,hashAddressBar:false});stLight.subscribe("click",trackWithOmniture);});}
if($('.addthis').length>0){$('.addthis').parent().addClass('addthis_toolbox addthis_default_style');if(window.addthis){window.addthis=null;}
addthis_config={pubid:"pmitlmcom",username:"pmitlmcom",data_track_clickback:true,services_compact:"email, facebook, digg, delicious, stumbleupon, favorites, more"};$('.addthis').attr('addthis:url',getNewLocation());var script=document.location.protocol+'//s7.addthis.com/js/250/addthis_widget.js#pubid=pmitlmcom&domready=1';$.getScript(script,function(){addthis.init();setTimeout(function(){if($('.addthis_button_facebook_like iframe').width()!=0){$('.addthis_button_facebook_like').css('width',$('.addthis_button_facebook_like iframe').width()+'px');}
if($('.addthis_button_tweet iframe').width()!=0){$('.addthis_button_tweet').css('width',$('.addthis_button_tweet iframe').width()+'px');}
if($('.addthis_button_linkedin_counter').length>0){if($('.addthis_button_linkedin_counter iframe').width()!=0){$('.addthis_button_linkedin_counter').css('width',$('.addthis_button_linkedin_counter iframe').width()+'px');}
$('.addthis_button_google_plusone').css('margin-left','0px');$('.addthis_button_google_plusone').css('max-width','75px');}
if($('.addthis_button_google_plusone iframe').width()!=0){$('.addthis_button_google_plusone').css('width',$('.addthis_button_google_plusone iframe').width()+'px');}},3000);});}
if($('.fb-like').length>0||$('.fb-comments').length>0||$('.fb-like-box').length>0){window.fbAsyncInit=function(){FB.init({status:true,xfbml:true});};if($('.fb-comments').length>0){$('.fb-comments').attr('data-href',location.protocol+'//'+location.host+location.pathname);}
(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(d.getElementById(id)){return;}
js=d.createElement(s);js.id=id;js.src="../../../../connect.facebook.net/en_US/all-1.js"/*tpa=https://connect.facebook.net/en_US/all.js*/;fjs.parentNode.insertBefore(js,fjs);}(document,'script','facebook-jssdk'));}
if($('.twitter-timeline').length>0){!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="../../../../platform.twitter.com/widgets.js"/*tpa=https://platform.twitter.com/widgets.js*/;fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");}}/*Revision Documentation variables */
var s_version = "H.22.1";
var sc_code_rev = "https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/1.2.89"; //LMCOM Release May 2015
var sc_code_loc = "PROD";

var s_account = "libertymutualcom";
if (document.URL.indexOf("libertymutual") < 0) {
    if (document.URL.indexOf("phase1") > 0) {
        s_account = "libertymutualcotydev"
    } else if (document.URL.indexOf("coachoftheyear") > 0) {
        s_account = "libertymutualcoty"
    } else if (document.URL.indexOf("responsibilityproject") > 0) {
        s_account = "libertymutualrespproj"
    } else if (document.URL.indexOf("responsiblesports") > 0) {
        s_account = "libertymutualrespsports"
    } else if (document.URL.indexOf("theatomgroup") > 0) {
        s_account = "libertymutualcotydev"
    } else if (document.URL.indexOf("https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/dev.hhcc") > 0) {
        s_account = "libertymutualrespprojdev"
    } else if (document.URL.indexOf("w.ztelligence") > 0) {
        s_account = "libertymutualrespprojdev"
    } else if (document.URL.indexOf("deploy.ztelligence") > 0) {
        s_account = "libertymutualrespproj"
    }
}
if (document.domain.indexOf("-") > 0) {
    var s_prefix = document.domain.substring(0, document.domain.indexOf("-"));
    switch (s_prefix) {
        case "responsibility":
            s_account = "libertymutualrespproj";
            break;
        case "ete":
            sc_code_loc = s_prefix.toUpperCase();
            s_account = "libertymutual-ete";
            break;
        case "test":
        case "load":
            sc_code_loc = s_prefix.toUpperCase();
            s_account = "libertymutual-qa";
            break;
        case "uat":
        case "2pr":
            sc_code_loc = s_prefix.toUpperCase();
            s_account = "libertymutual-prodmirror";
            break;
        case "dev":
            sc_code_loc = s_prefix.toUpperCase();
            s_account = "libertymutualdev";
            if (document.URL.indexOf("coachoftheyear") >= 0) {
                s_account = "libertymutualcotydev"
            }
            if (document.URL.indexOf("responsibility") >= 0) {
                s_account = "libertymutualrespprojdev"
            };
        default:
            break
    }
} else if (document.URL.indexOf("https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/betastag.resp") >= 0) {
    s_account = "libertymutualrespprojdev"
} else if (document.domain.indexOf("209.") == 0) {
    sc_code_loc = "TEST";
    s_account = "libertymutual-qa"
} else if (document.domain.indexOf("10.") == 0 || document.domain.toLowerCase().indexOf("localhost") == 0) {
    sc_code_loc = "DEV";
    s_account = "libertymutualcomdev"
}


/**
 ** CUSTOMER CODE BEGIN
 ** This is the only custom line of code, this following sets the account from our
 ** global javascript application data. see properties.xml for the source of the value
 **/

/*
This function takes in a URL and converts it to an array separating the various parts of the URL, including domain and path, which are being used to 
set s_channel and other TBD prop variables for page data tracking.  Also available if needed later are query variables which could also be further 
broken down as needed.
*/

function parseUri(sourceUri) {
    var uriPartNames = ["source", "protocol", "authority", "domain", "port", "path", "directoryPath", "fileName", "query", "anchor"];
    var uriParts = new RegExp("^(?:([^:/?#.]+):)?(?://)?(([^:/?#]*)(?::(\\d*))?)?((/(?:[^?#](?![^?#/]*\\.[^?#/.]+(?:[\\?#]|$)))*/?)?([^?#/]*))?(?:\\?([^#]*))?(?:#(.*))?").exec(sourceUri);

    var uri = {};

    for (var i = 0; i < 10; i++) {
        uri[uriPartNames[i]] = (uriParts[i] ? uriParts[i] : "");
    }

    // Always end directoryPath with a trailing backslash if a path was present in the source URI
    // Note that a trailing backslash is NOT automatically inserted within or appended to the "path" key
    if (uri.directoryPath.length > 0) {
        uri.directoryPath = uri.directoryPath.replace(/\/?$/, "/");
    }

    return uri;
}

// End of URl parsing function


/* Creating below s_channel section hierarchy values from the post domain path information in the URL. 

Function taking parceUri path value (or any other "/" delimited string limited to 4 sections) and places the sections
into 4 parts of an array.  This will provide for a deeper view hierachy of traffic data.   

Note: The array doesn't use the first array row as the uri.path string starts with a "/", so the first piece in the array gets
set to unknown. 
*/

/* Function to get s_channel value from the url domain.
This function separates out the first part of the domain to set as s_channel.  Example:  Takes "https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/auto-insurance.libertymutual.com" 
and splits out "auto-insurance" to be used as s_channel value.  If the first section starts with "www" or "libertymutual", the return
value is set as "homepage".  More work will need to be done as exceptions occur.

*/

function findchannel(sourcedomain) {
    var chpointer = sourcedomain.indexOf(".");
    var prefixExclude = [/^dev-\w-/, /^test-\w-/, /^ete-/, /^uat-/];
    var retchannel = sourcedomain.substr(0, chpointer);

    for (var i = 0; i < prefixExclude.length; i++) {
        retchannel = retchannel.replace(prefixExclude[i], "");
    }
    retchannel = retchannel.replace(/www2/, "www");

    return retchannel;
}

// End get s_channel info function

/*
 *  Domain manipulation functions
 *  These are currently designed with assumption that you know the pattern of the domain before use.
 *
 */
// This functions strip domain of anything after the first dot to isolate auto-insurance from auto-insurance.libertymutual.com or
// removes .com from end of domain or final path values
function removeAfterDot(pathSent) {
    var dotSpot = pathSent.indexOf(".");
    var retPath = pathSent.substr(0, dotSpot);

    return retPath;
}

// Removes everything before first . in domain and removes .com from end of domain leaving just the core
function getStrippedDomain(a) {
    var firstDot = a.indexOf(".");
    var firstStrip = a.substring(firstDot + 1);
    var secondDot = firstStrip.indexOf(".");
    var retDomain = firstStrip.substr(0, secondDot);
    return retDomain;
}

/* Creating below s_channel section hierarchy values from the post domain path information in the URL. 
        Function taking parceUri path value (or any other "/" delimited string limited to 4 sections) and places the sections
        into 4 parts of an array.  This will provide for a deeper view hierarchy of traffic data.   

        Note: The array doesn't use the first array row as the uri.path string starts with a "/", so the first piece in the array gets
        set to unknown. 
*/
function parsepath(sourcepath) {
        var pathpartnames = ["path1", "path2", "path3", "path4", "path5", "path6"];
        var pathparts = sourcepath.split("/");
        var patharray = {};

        for (var k = 1; k < 7; k++) {
            patharray[pathpartnames[k]] = (pathparts[k] ? pathparts[k] : "");
            if (patharray[pathpartnames[k]] != "") {
                if (patharray[pathpartnames[k]].indexOf(".") > 1) {
                    patharray[pathpartnames[k]] = removeAfterDot(patharray[pathpartnames[k]]);
                }
            }
        }
        return patharray;
    }
    // End of path function


/**
 * * Application Execution Context API * * This Section gets the appliction
 * execution context parameters * that define the API between the application
 * and analytics. * This is the messy part of mapping betwen two different ECs.
 */

/** Function to take a list if minisite Section1 values (first part of path after domain) and resurn a flag value to
 * use as a trigger to change the section and channel variables.
 */

function matchStringToList(a, b) {

    var lookingFor = a;
    var bArray = b.split(",");
    var myFlag = 1;
    for (var i = 0; i < bArray.length; i++) {
        if (bArray[i] == lookingFor) {
            myFlag = 2;
            break;
        }
    }
    return myFlag;
}


function InitializeAEC(s) {

    var aec = new Object();

    /* Set static sitetype. Options are: PM-LM(LM.com), PM-CF(CFI), PM-CS, PM-PS, PM-LP. This
	 is set manually for each .js version.
	 */
    aec.sitetype = "PM-LM";

    // Set channel and hierachy parts to pull into s_code in the do_plugins function later
    if (!s.pageName) {
        s.pageName = "NOPAGENAME";
    }

    if (s.pageName) {
        if (s.pageName.indexOf("PM") != 0) {
            aec.originalPageName = s.pageName;
        }
        if (s.pageName.indexOf('blog-posts') > -1 && !s.getQueryParam('CAT')) {
            s.pageName = "safe-and-smart|blog|blog-posts";
        }
        if (s.getQueryParam('CAT')) {
            if (s.pageName.indexOf('blog') > -1) {
                s.pageName = "safe-and-smart|blog|AllPosts|" + s.getQueryParam('CAT');
            }
        }
    }


    /*  Set variables to use as s_props and s_channel from function calls.
	Call URL and use split function to get pieces to use 
	*/

    // Use the parseUri function and associated functions to do initial splitting of page url 

    aec.uriResults = parseUri(document.URL); // returns uri;  an array with 10 parts, two of which are path and domain
    aec.uriResults = s.getParentVal(aec.uriResults, "a_ur", 0);

    var tempchannel = findchannel(aec.uriResults.domain); // returns string of first part of the domain from uriresults.domain stripping out the libertymutual.com

    aec.pathparts = parsepath(aec.uriResults.path); //returns array with path parts from uriresults.path
    aec.pathparts = s.getParentVal(aec.pathparts, "a_pp", 0);


    var B2BList = "employers,assoc,alumni";
    // check to see if outside referrer to drive landing page designation	
    var checkLP = 2;
    if (tempchannel == "welcome" || aec.uriResults.domain == "https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/www.libertymutualonline.com" || matchStringToList(aec.pathparts.path2, B2BList) == 2) {
        checkLP = 1;

        aec.checkB2B = matchStringToList(aec.pathparts.path2, B2BList);
    }
    // check for sales rep page

    var checksalesrep = document.body.className;
    if (checksalesrep == "agentPage") {
        aec.pathparts.path3 = aec.pathparts.path2;
        aec.pathparts.path2 = "Agent";
    }


    // check for local office page

    var checkoffice = document.body.className;
    if (checksalesrep == "localOfficesPage") {
        aec.pathparts.path3 = aec.pathparts.path2;
        aec.pathparts.path2 = "Office";
    }

    // check for get direction page

    var checkoffice = document.body.className;
    if (checksalesrep == "noRightModules getDirectionsPage") {
        aec.pathparts.path3 = aec.pathparts.path2;
        aec.pathparts.path2 = "Directions";
    }

    // Check to see if page is a Mini-site	such as teen driver or senior driver
    /*    var miniSiteList = "teen-driving,senior-driving";
    	var checkMiniSite = matchStringToList(aec.pathparts.path2,miniSiteList);
    */
    var checkMiniSite = 1;


    //check to see if site is in other domains list of minisites
    var otherDomainsList = "befiresmart,coachoftheyear,learnreturn,libertymutualsolutions,responsibilityproject,responsiblesport,youcovered";
    var coreDomain = getStrippedDomain(aec.uriResults.domain);
    var miniSiteFlag = matchStringToList(coreDomain, otherDomainsList);
    if (checkLP != 1) {
        if (aec.pathparts.path2 != "" && checkMiniSite != 2) {
            if (s.isMicroSite(aec.pathparts.path2)) {
                checkLP = 1;
            }
        }
    }

    // set parts for channel and pagename based on type of page from flags	
    if ((tempchannel.indexOf("www") >= 0 || checkMiniSite == 2 || checkLP == 1) && miniSiteFlag != 2) {
        if (aec.pathparts.path2.length > 0) {
            aec.s_channel = aec.pathparts.path2;
            aec.s_channel = s.getParentVal(aec.s_channel, "a_ch", 0);
            if (checkLP == 1)
                s.setMicroSite(aec.s_channel);

            aec.section1 = (aec.pathparts.path3 == "" ? "main" : aec.pathparts.path3);

            aec.section2 = aec.pathparts.path4;

            aec.section3 = aec.pathparts.path5;

            aec.section4 = aec.pathparts.path6;
        } else {
            aec.s_channel = "homepage";
            aec.s_channel = s.getParentVal(aec.s_channel, "a_ch", 0);
            aec.section1 = "main";
            aec.section1 = s.getParentVal(aec.section1, "a_s1", 0);
            aec.section2 = aec.pathparts.path3;
            aec.section3 = aec.pathparts.path4;
            aec.section4 = aec.pathparts.path5;
        }
        if (aec.checkB2B == 2) {
            aec.sitetype = "PM-LM";
            aec.s_channel = "B2B";
            aec.section1 = aec.pathparts.path2;
            aec.section2 = aec.pathparts.path3;
            aec.section3 = aec.pathparts.path4;
            aec.section4 = aec.pathparts.path5;
        }
        if (aec.checkB2B != 2 && aec.checkLP == 2) {
            aec.sitetype = "PM-LP";
        }
    } else {
        if (miniSiteFlag == 2) {
            aec.s_channel = coreDomain;
            aec.s_channel = s.getParentVal(aec.s_channel, "a_ch", 0);
            aec.sitetype = "PM-SS";
        } else {
            aec.s_channel = tempchannel;
            aec.s_channel = s.getParentVal(aec.s_channel, "a_ch", 0);
            if (checkLP == 1 && aec.checkB2B != 2) {
                aec.sitetype = "PM-LP";
            }
        }

        aec.section1 = (aec.pathparts.path2 == "" ? "main" : aec.pathparts.path2);
        aec.section1 = s.getParentVal(aec.section1, "a_s1", 0);

        aec.section2 = aec.pathparts.path3;

        aec.section3 = aec.pathparts.path4;

        aec.section4 = aec.pathparts.path5;
    }
    return aec;
}


function ApplicationTagHarness(s) {
    // Need to persists the API as long as SC s context persists
    // so that dialogs and popups instrumented by this page have the
    // same context.


    if (!s.aec_api) {
        s.aec_api = InitializeAEC(s);
    }
    var aec = s.aec_api;

    s.prop46 = aec.originalPageName;
    // Default LM.com naming setup for s.channel, s.hier1, s.pagename


    //Start Internal Search
    if (pLoad) {
        if (aec.originalPageName === "search-google-results" || aec.originalPageName === "safe-and-smart-search-results" ) {

            var ISCookieValue = s_readCookie("s_ISCookie");
            if (ISCookieValue) {
                s_CompareIS();
            } else {
                s_coldInternalSearch();
            }

        } else if (aec.originalPageName !== "search-google-results" || aec.originalPageName !== "safe-and-smart-search-results" ) {
            s_eraseCookie('s_ISCookie');
        }
    }

    function s_CompareIS() {
        //collect Page Data 
        //get the search term
        if ($("#searchForString").val()) {
            aec.searchLabelValue = $("#searchForString").val().toLowerCase();
        } else {
            aec.searchLabelValue = "NoValue";
        }
        //get the search results amount
        if ($("#searchResultsContent").length) {
            var resultsAmount = $("#searchResultsContent").children("h3").text().split(" ");
            var p = resultsAmount;
            var start = "about";
            var next = p[($.inArray(start, p) + 1) % p.length];
            aec.resultsAmount = next;
        } else {
            aec.resultsAmount = "NoValue";
        }
        //get the search Section
        if ($("#searchForSection :selected")) {
            aec.searchForSection = $("#searchForSection :selected").text().toLowerCase().replace(/\s/g, "");
        } else {
            aec.searchForSection = "NoValue";
        }
        aec.pageData = aec.searchLabelValue + "," + aec.searchForSection, aec.ISCookieValue = s_readCookie("s_ISCookie"), aec.ISSearch = aec.ISCookieValue.split(",")[0], aec.ISSection = aec.ISCookieValue.split(",")[1];


        if (aec.ISCookieValue === aec.pageData) {
            //this is a refresh - do nothing
            return
        } else {
            if (aec.searchForSection !== aec.ISSection) { //is section different?
                s_warmInternalSearch();
            } else if (aec.searchLabelValue !== aec.ISSearch) { //is search term different?
                s_coldInternalSearch();
            }
        }
    }

    function s_coldInternalSearch() {
        if ($("#searchForString").val()) {
            aec.searchLabelValue = $("#searchForString").val().toLowerCase();
        } else {
            aec.searchLabelValue = "NoValue";
        }
        //get the search results amount
        if ($("#searchResultsContent").length) {
            var resultsAmount = $("#searchResultsContent").children("h3").text().split(" ");
            var p = resultsAmount;
            var start = "about";
            var next = p[($.inArray(start, p) + 1) % p.length];
            aec.resultsAmount = next;
        } else {
            aec.resultsAmount = "NoValue";
        }
        //get the search Section
        if ($("#searchForSection :selected")) {
            aec.searchForSection = $("#searchForSection :selected").text().toLowerCase().replace(/\s/g, "");
        } else {
            aec.searchForSection = "NoValue";
        }

        s.linkTrackVars = s.apl(s.linkTrackVars, 'events', ',', 1);
        s.eVar3 = aec.searchLabelValue + ":" + aec.resultsAmount;
        s.linkTrackEvents = s.apl(s.linkTrackEvents, 'event96', ',', 1);
        s.events = s.apl(s.events, 'event96', ',', 1);

        s_createCookie('s_ISCookie', aec.searchLabelValue + "," + aec.searchForSection, 0); //create/overwrite ISCookie
    }

    function s_warmInternalSearch() {
        if ($("#searchForString").val()) {
            aec.searchLabelValue = $("#searchForString").val().toLowerCase();
        } else {
            aec.searchLabelValue = "NoValue";
        }
        //get the search results amount
        if ($("#searchResultsContent").length) {
            var resultsAmount = $("#searchResultsContent").children("h3").text().split(" ");
            var p = resultsAmount;
            var start = "about";
            var next = p[($.inArray(start, p) + 1) % p.length];
            aec.resultsAmount = next;
        } else {
            aec.resultsAmount = "NoValue";
        }
        //get the search Section
        if ($("#searchForSection :selected")) {
            aec.searchForSection = $("#searchForSection :selected").text().toLowerCase().replace(/\s/g, "");
        } else {
            aec.searchForSection = "NoValue";
        }

        s.linkTrackVars = s.apl(s.linkTrackVars, 'events', ',', 1);
        s.eVar3 = aec.searchLabelValue + ":" + aec.resultsAmount + ":" + aec.searchForSection;
        s.linkTrackEvents = s.apl(s.linkTrackEvents, 'event97', ',', 1);
        s.events = s.apl(s.events, 'event97', ',', 1);

        s_createCookie('s_ISCookie', aec.searchLabelValue + "," + aec.searchForSection, 0); //create/overwrite ISCookie
    }


    //Ease of Access Remember Me
    if (pLoad) {
        if ($('#remember').is(':checked')) {
            aec.EOA = "checked";
            s_createCookie('s_ESCookie', "LM:" + aec.EOA, 0); //create/overwrite ESCookie
        } else {
            aec.EOA = "unchecked";
            s_createCookie('s_ESCookie', "LM:" + aec.EOA, 0); //create/overwrite ESCookie
        }
    }

    $('#loginbutton').click(function() {
        if ($('#remember').is(':checked')) {
            $('#remember').attr("data-attr", "checked");
            aec.EOA = "checked";
            s_createCookie('s_ESCookie', "LM:" + aec.EOA, 0); //create/overwrite ESCookie
        } else {
            $('#remember').attr("data-attr", "unchecked");
            aec.EOA = "unchecked";
            s_createCookie('s_ESCookie', "LM:" + aec.EOA, 0); //create/overwrite ESCookie
        }
    });



    if (s.prop3 == "PM-LD") {
        aec.sitetype = "PM-LD";
    }

    s.channel = aec.sitetype + "|" + aec.s_channel;

    if (pLoad) {
        var icp = s.getInternalChannel();
        s.prop30 = icp ? icp.property + "|" + icp.pagename + "|" + icp.placement + "|" + aec.originalPageName : s.prop46;

        var ic = s.eVar21 != "" ? s.eVar21 : s.getQueryParam('int');
        s.eVar21 = s.setInternalChannel(s.channel, aec.section1 + ":" + aec.originalPageName, ic);

    }

    if (aec.section4 != "") {
        s.hier2 = aec.sitetype + "|" + aec.s_channel + "|" + aec.section1 + "|" + aec.section2 + "|" + aec.section3;
        //s.pageName = aec.sitetype + "|" + aec.s_channel + "|" + aec.section1 + "|" + aec.section4;
    } else if (aec.section3 != "") {
        s.hier2 = aec.sitetype + "|" + aec.s_channel + "|" + aec.section1 + "|" + aec.section2;
        /**
		if(aec.checkB2B == 2 && aec.checkPath3 == 1){
			s.pageName = aec.sitetype + "|" + aec.s_channel + "|" + aec.section1 + "|" +aec.section2 + "|" + aec.section3;
		}
		else
			s.pageName = aec.sitetype + "|" + aec.s_channel + "|" + aec.section1 + "|" + aec.section3;
		**/
    } else if (aec.section2 != "") {
        s.hier2 = aec.sitetype + "|" + aec.s_channel + "|" + aec.section1;
        //s.pageName = aec.sitetype + "|" + aec.s_channel + "|" + aec.section1 + "|" + aec.section2;
    } else if (aec.section1 != "") {
        s.hier2 = aec.sitetype + "|" + aec.s_channel + "|" + aec.section1;
        //s.pageName = aec.sitetype + "|" + aec.s_channel + "|" + aec.section1 + "|main";
    } else {
        s.hier2 = aec.sitetype + "|" + aec.s_channel;
        //s.hier2 = aec.sitetype + "|" + aec.s_channel + "|" + aec.section1;
        //s.pageName = aec.sitetype + "|" + aec.s_channel + "|" + aec.section1;
    }


    return;
}



var DATA_LAYOUT_KEY = "data-layoutkey";
var DATA_CONTENT_KEY = "data-contentkey";
var DATA_ANALYTIC_CONTEXT = "data-analyticscontext";
var DATA_ANALYTIC_CONTENT = "data-analyticscontent";
var DATA_ANALYTIC_KEY = "data-analyticskey";

function sendOmnitureOnEserviceLogin(event, desc) {}

function s_createCookie(e, t, n) {
    if (n) {
        var r = new Date;
        r.setTime(r.getTime() + n * 24 * 60 * 60 * 1e3);
        var i = "; expires=" + r.toGMTString()
    } else var i = "";
    document.cookie = e + "=" + t + i + "; path=/; domain=.libertymutual.com"
}

function s_readCookie(e) {
    var t = e + "=";
    var n = document.cookie.split(";");
    for (var r = 0; r < n.length; r++) {
        var i = n[r];
        while (i.charAt(0) == " ") i = i.substring(1, i.length);
        if (i.indexOf(t) == 0) return i.substring(t.length, i.length)
    }
    return null
}

function s_eraseCookie(e) {
    s_createCookie(e, "", -1)
}


if (typeof jQuery != "undefined") {
    $(document).ready(function() {
    	    //begin place CPT DAKS
		$("#masthead").each(function(){s(this,"rail_major","header","")}),$("#masthead #topNav").each(function(){s(this,"rail_minor","top_right","navigation")}),$("#masthead #mainCSSMessage").each(function(){s(this,"rail_minor","warning","css_message")}),$("#masthead #globalContainer .firstLevel").each(function(){var e=this.id?this.id:"";if(!e){var i=jQuery(this).find(".firstLevelTitle");i.length>0&&(e=i[0].innerHTML.replace(/\s/g,""))}s(this,"rail_minor","global",e)}),$("#masthead .logoContainer").each(function(){s(this,"rail_minor","brand","logo")}),$("#masthead #utilityLinksContainer li").each(function(e){s(this,"rail_minor","utility","link_"+e)}),$("#masthead #utilityLinksContainer .socialIcons").each(function(){s(this,"rail_minor","utility","social_icons")}),$("#masthead #megaMenuContainer").each(function(){s(this,"rail_major","mega_menu","");for(var e=jQuery(this).find("ul.megaLevel>li.firstLevel"),i=0;i<e.length;i++){var t=jQuery(e[i]).find(".firstLevelLink"),o="";t.length>0&&(o=t[0].innerHTML.replace(/\s*/g,"")),s(e[i],"rail_minor","fl_"+i,o);for(var n=jQuery(e[i]).find(".subMenu .columnContent"),r=n.length,a=0;r>a;a++){var o="",t=jQuery(n[a]).find(".megaContentTitle");if(t.length>0){var l=jQuery(t[0]).find("a");o=l.length>0?l[0].innerHTML.replace(/\s/g,""):t[0].innerHTML.replace(/\s/g,"")}else t=jQuery(n[a]).find("img"),t.length>0&&(o="promo-"+t[0].alt.replace(/\s/g,""));s(n[a],"rail_promo","col_"+(a+1)+"_"+r,o.replace(/&\w*;/g,""))}}}),$("#nonavBreadcrumbBar").each(function(){s(this,"rail_major","breadcrumb_bar","")}),$("#nonavBreadcrumbBar .breadcrumbs").each(function(){s(this,"rail_minor","left","crumbs")}),$("#nonavBreadcrumbBar .pageLinkSet").each(function(){s(this,"rail_minor","right","page_link")}),$("#pageBody #contentWrapper, #voltronContentWrapper, .dealsPage #centerModRailWrapper").each(function(){s(this,"rail_major","main_content","")}),$("#mcPlayerArea").each(function(){s(this,"rail_minor","hero","mc_player")}),$("#pageBody #contentWrapper #mcPlayerArea #mcHTML5Video").each(function(){s(this,"rail_minor","mcHTML5_player","slider")}),$("#pageBody #contentWrapper #mcPlayerArea #mcHTML5Video  #controlsContainer ").each(function(){s(this,"rail_promo","mcHTML5_player","carousel")}),$("#pageBody #contentWrapper #searchResults").each(function(){s(this,"rail_minor","left_rail","search_results")}),$("#pageBody #contentWrapper .agentInfoColumn").each(function(){s(this,"rail_minor","left_rail","agent_info")}),$("#pageBody #contentWrapper .agentDetailsColumn").each(function(){s(this,"rail_minor","center_rail","agent_bio")}),$("#pageBody #contentWrapper #officesNewAccordion>ul>li").each(function(e){s(this,"rail_promo","acc_"+e,"offices_new")}),$("#pageBody #contentWrapper #rightRail").each(function(){s(this,"rail_minor","right_rail","action_links")}),$("#pageBody #contentWrapper #map_results").each(function(){s(this,"rail_promo","map","map_results")}),$("#pageBody #contentWrapper #quickLinksContainer").each(function(){s(this,"rail_minor","middle","quick_links")}),$("#pageBody #contentWrapper #homeCenterModSet").each(function(){s(this,"rail_minor","bottom","home_center")}),$("#pageBody #contentWrapper ul#homeCenterModSet>li").each(function(e){var i=(e%2?"right":"left")+"_"+Math.round((e+1)/2);s(this,"rail_promo","bottom_"+i,"LMFeatures")}),$("#pageBody #contentWrapper #tabCenterModSet").each(function(){s(this,"rail_minor","bottom","tab_center")}),$("#pageBody #contentWrapper #tabCenterModSet .ui-tabs-panel").each(function(e){var i=jQuery(this).find("div>p"),t="tab_center";i.length>0&&(t=i[0].innerHTML.replace(/\s/g,"")),s(this,"rail_promo","bottom_tab_"+(e+1),t)}),$("#pageBody #contentWrapper #secondaryContent").each(function(){s(this,"rail_minor","top","summary_content")}),$("#pageBody #contentWrapper #relatedLinks").each(function(){s(this,"rail_minor","bottom","related_links")}),$("#pageBody #contentWrapper #search").each(function(){s(this,"rail_minor","center","search_results")}),$("#pageBody #moduleArea").each(function(){s(this,"rail_major","module_rail","")}),$("#pageBody #moduleArea #agentCardContainer").each(function(){s(this,"rail_minor","aff_top","agent_card")}),$("#pageBody #moduleArea #managePolicyContainer").each(function(){s(this,"rail_minor","top","module")}),$("#pageBody #moduleArea #modulesAccordion .track_module").each(function(e){s(this,"rail_promo","acc_"+e,"module")}),$("#pageBody #moduleArea .promoSlideshowModule").each(function(){s(this,"rail_minor","right_bottom","promo_slide")}),$("#pageBody #moduleArea #localNavAccordion").each(function(){s(this,"rail_minor","left_top","local_nav")}),$("#pageBody #moduleArea .staticContentModule").each(function(e){this.className&&this.className.indexOf("actionLinks")>-1?s(this,"rail_minor","left_bottom","action_links"):s(this,"rail_minor","bottom_"+e,"promo_links")}),$("#footerNavMenu").each(function(){s(this,"rail_major","footer_nav","")}),$("#footerNavMenu #footerLinksContainer").each(function(){s(this,"rail_minor","top","footer_links")}),$("#footerNavMenu #footerLinksContainer .footerSectionHeader").each(function(e){var i="";if("DIV"==this.firstChild.nodeName){var t=jQuery(this.firstChild).find("span");i=t.length>0?t[0].innerHTML.replace(/\s*/g,""):this.innerHTML.replace(/\s*/g,"")}else{var t=jQuery(this).find("a>span");i=t.length>0?t[0].innerHTML.replace(/\s*/g,""):this.innerHTML.replace(/\s*/g,"")}s(this,"rail_promo","col_"+e,i)}),$("#footerNavMenu #fullSiteMapLink").each(function(){s(this,"rail_minor","bottom","site_map")}),$("#pageFooter").each(function(){s(this,"rail_major","page_footer","")}),$("#pageFooter #footerTextSection").each(function(){s(this,"rail_minor","content_left","corporate_info")}),$("#pageFooter #footerSocialMediaContents").each(function(){s(this,"rail_minor","top","social")}),$("#pageFooter #badges").each(function(){s(this,"rail_minor","content_right","badges")}),$("#fullPage #affinityBottomWrapContainer").each(function(){s(this,"rail_major","aff_bottom","cobrand_banner")}),$("#fullPage .jqmWindow").each(function(){s(this,"rail_major","modal","dialog")}),$("#fullPage .jqmWindow>div").filter(function(){return this.id}).each(function(){s(this,"rail_minor","lightbox",this.id)}),$("#fullPage .jqmWindow>li").filter(function(){return this.id}).each(function(){s(this,"rail_minor","lightbox",this.id)}),$(".track_module").each(function(){var e=this.getAttribute(DATA_CONTENT_KEY),i=jQuery(this).find("h3 span");i.length>0?e=i[0].innerHTML.replace(/\s*/g,""):(i=jQuery(this).find("img"),e=i.length>0?i[0].alt.replace(/\s*/g,""):e),this.getAttribute(DATA_CONTENT_KEY)?(this.removeAttribute(DATA_CONTENT_KEY),this.setAttribute(DATA_CONTENT_KEY,e)):s(this,"rail_promo","promo",e)}),$("#homePageBodyLeftRail").each(function(){s(this,"rail_major","module_rail","")}),$("#modulesAccordion").each(function(){s(this,"rail_minor","module_rail","Accordion")}),$('li[id^="module_"], .linkWithImage, #searchRightMod').each(function(e){this.id?this.id:"";s(this,"rail_minor","acc_"+e,this.id)}),$("#homePageBodyMedia, #ssWrapper").each(function(){s(this,"rail_major","main_content","")}),$("#homePageBodyCenter").each(function(){s(this,"rail_major","CenterModule","")}),$("#homePageBodyContent").each(function(){s(this,"rail_minor","top_left","responsibility")}),$("#homePageBodyCenterRight").each(function(){s(this,"rail_minor","top_right","spotlight")}),$("#homePageBodyCenterModuleSlider").each(function(){s(this,"rail_minor","bottom_left","features")}),$("#standardCenterModSet").each(function(){s(this,"rail_minor","CenterModule","")}),$(".standardCenterMod").each(function(){var e="";$(".standardCenterMod").each(function(e){$(this).attr("id","module"+e)}),this.id.length>0&&(e=this.id);var i=this.getAttribute(DATA_CONTENT_KEY),t=jQuery(this).find("h3");t.length>0&&(i=t[0].innerHTML.replace(/\s*/g,"").replace(/&/,"")),s(this,"rail_promo",e,i)}),$("#footerLinksMenu").each(function(){s(this,"rail_major","footerMenu","footerLinks")}),$(".footerLinksSectionHeader").each(function(e){var i="";if("DIV"==this.firstChild.nodeName){var t=jQuery(this.firstChild).find("span");i=t.length>0?t[0].innerHTML.replace(/\s*/g,""):this.innerHTML.replace(/\s*/g,"")}else{var t=jQuery(this).find("a>span");i=t.length>0?t[0].innerHTML.replace(/\s*/g,""):this.innerHTML.replace(/\s*/g,"")}s(this,"rail_promo","col_"+e,i)}),$("#pageFooterWithLinks").each(function(){s(this,"rail_major","footerModule","footerLinks")}),$(".footerSocialMediaContents").each(function(){s(this,"rail_minor","top","footerSocialMediaContents")}),$(".footerContents").each(function(){s(this,"rail_minor","middle","corp_info")}),$("#badges").each(function(){s(this,"rail_minor","bottom","badges")}),$("#componentArea").each(function(){s(this,"rail_major","module_rail","")}),$("#blogTop").each(function(){s(this,"rail_major","blog","")}),$(".blogNav").each(function(){var e="";$(".blogNav").each(function(e){$(this).attr("id","module"+e)}),this.id.length>0&&(e=this.id),s(this,"rail_minor",e,"blog_menu")}),$(".voltronModuleWrapper .blogCenterMod").each(function(){var e="";$("voltronModuleWrapper .blogCenterMod").each(function(e){$(this).attr("id","module"+e)}),this.id.length>0&&(e=this.id),s(this,"rail_minor",e,"blog_blurb")}),$(".voltronDealModuleWrapper .dealContainer").each(function(){var e="";$(".voltronDealModuleWrapper .dealContainer").each(function(e){$(this).attr("id","module"+e)}),this.id.length>0&&(e=this.id),s(this,"rail_minor",e,"deal_blurb")}),$("#blogCollectionFooter").each(function(){s(this,"rail_minor","blogFooter","")}),$("#voltronMenu ul#outerMenu li.home, #voltronMenu ul#outerMenu li#blogMenu, #voltronMenu ul#outerMenu li.deals").each(function(e){s(this,"rail_minor","voltronMenu","link_"+e)}),$("#voltronMenu ul#subMenu li").each(function(e){s(this,"rail_minor","voltronSubMenu","link_"+e)}),$(".featuredArticle").each(function(){s(this,"rail_minor","voltron_featured_Deal","")}),$(".rssFeedsLinkDiv").each(function(e){s(this,"rail_minor","voltron_RSS_Feed","link_"+e)}),$("#searchResults .ui-accordion").bind("click",function(){return $("div.ero-body").each(function(){s(this,"rail_major","ero","")}),$("div.ero-previewArea").each(function(){s(this,"rail_minor","leftbeak","map-popup")}),setTimeout('aec_setAccordionAttributes("#searchResults")',500),!0});
        //end CPT Place DAKs



        function e(e) {
            $(e + " .ui-accordion li.selected a").each(u);
            $(e + " .ui-accordion li.selected button").each(a);
            $(e + " .ui-accordion li.selected [" + DATA_ANALYTIC_KEY + "]").bind("click", r);
            $(".VE_Pushpin_Popup_Title a").each(u);
            $(".VE_Pushpin_Popup_Title a").bind("click", r)
        }

        function r(e) {
            var r = s_gi(s_account);
            var i = r.aec_api;
            var s = this.getAttribute(DATA_ANALYTIC_KEY);
            var o = this.getAttribute(DATA_ANALYTIC_CONTENT);
            var u = this.getAttribute(DATA_ANALYTIC_CONTEXT);
            if (u.indexOf("modal") < 0) {
                r.eVar21 = r.setInternalPlacement(s, o, u)
            }
            if (t) {
                r.prop30 = r.eVar39 = "Html:" + r.prop46 + "|" + u + ":" + o + ":" + s;
                r.prop24 = r.pageName;
                r.linkTrackVars = "prop30,prop24";
                r.linkTrackEvents = "";
                if (s.indexOf("cta$") == 0 || s.indexOf("img$") == 0) {
                    r.eVar21 += r.eVar21 ? "|" : "";
                    r.linkTrackVars = r.apl(r.linkTrackVars, "eVar21", ",", 1);
                    r.linkTrackVars = r.apl(r.linkTrackVars, "events", ",", 1);
                    r.linkTrackEvents = r.apl(r.linkTrackEvents, "event18", ",", 1);
                    r.events = r.apl(r.events, "event18", ",", 1)
                }
                if (n) {
                    r.linkTrackVars = r.apl(r.linkTrackVars, "eVar39", ",", 1);
                    r.linkTrackVars = r.apl(r.linkTrackVars, "events", ",", 1);
                    r.linkTrackEvents = r.apl(r.linkTrackEvents, "event25", ",", 1);
                    r.events = r.apl(r.events, "event25", ",", 1)
                }
                if (this.className.indexOf("HTML5") > -1) {
                    r.linkTrackVars = r.apl(r.linkTrackVars, "eVar39", ",", 1);
                    r.linkTrackVars = r.apl(r.linkTrackVars, "events", ",", 1);
                    r.linkTrackVars = r.apl(r.linkTrackVars, "prop26", ",", 1);
                    r.linkTrackEvents = r.apl(r.linkTrackEvents, "event25", ",", 1);
                    r.events = r.apl(r.events, "event25", ",", 1);
                    r.prop26 = r.apl(r.prop26, "HTMLMC", ",", 1)
                }
                if (this.className && this.className.indexOf("engagementIndex") > -1) {
                    r.eVar2 = r.channel + "|" + s;
                    r.linkTrackVars = r.apl(r.linkTrackVars, "eVar2", ",", 1)
                }
                if (this.className && this.className.indexOf("searchLink") > -1) {
                    r.linkTrackVars = r.apl(r.linkTrackVars, "events", ",", 1);
                    r.events = r.apl(r.events, "event98", ",", 1);
                    r.linkTrackEvents = "event98"
                }
                if (!$(this).hasClass("bx-next") && !$(this).hasClass("bx-prev") && !$(this).hasClass("bx-pager-link")) {
                    s_createCookie("s_timeStampS", (new Date).getTime() + "|" + r.prop46, 0);
                    r.tl(this, "o", "Html")
                }
            }
            return true
        }

        function i(e, t, n) {
            var r = e;
            var i = "";
            while (r && !i) {
                r = r.parentNode;
                try {
                    if (r && r.className && r.className.indexOf(t) >= 0) {
                        i = r.getAttribute(n)
                    }
                } catch (s) {}
            }
            return i
        }

        function s(e, t, n, r) {
            e.className += e.className ? " " + t : t;
            if (n) {
                e.setAttribute(DATA_LAYOUT_KEY, n)
            }
            if (r) {
                e.setAttribute(DATA_CONTENT_KEY, r)
            }
        }

        function o(e, t, n, r) {
            if (r) {
                e.setAttribute(DATA_ANALYTIC_KEY, r)
            }
            if (t && !e.getAttribute(DATA_ANALYTIC_CONTEXT)) {
                e.setAttribute(DATA_ANALYTIC_CONTEXT, t)
            }
            if (n && !e.getAttribute(DATA_ANALYTIC_CONTENT)) {
                e.setAttribute(DATA_ANALYTIC_CONTENT, n)
            }
        }

        function u(e) {
            var t = this;
            if (t.href && t.href.indexOf("https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/addthis.com") > -1) {
                return
            }
            var n = jQuery(t).find("img");
            var r = "";
            if (n.length > 0) {
                r = "img$" + n[0].alt.replace(/\s*/g, "")
            } else {
                var s = "text$";
                if (t.className && t.className.indexOf("button") > -1) {
                    s = "cta$"
                }
                r = s + (t.id ? t.id : t.innerHTML.replace(/\s*/g, ""))
            }
            var u = $(t).find("span");
            if (u.length > 0) {
                u = $(this).text().replace(/\s*/g, "");
                s = "text$";
                r = s + u
            }
            var a = $(this).closest(".headerWrapper").find(".mcTitle").text();
            if (a.length > 0) {
                context = a;
                s = "text$";
                r = context.replace(/\s*/g, "") + "_" + s + (t.id ? t.id : t.innerHTML.replace(/\s*/g, ""))
            }
            r = r.replace(/&\w*;/g, "");
            r = r.replace(/<\w*>/g, "");
            r = r.replace(/[-:,|]/g, "");
            var r = r;
            var f = r.indexOf("(");
            r = r.substring(0, f != -1 ? f : r.length);
            var f = r.indexOf("(");
            r = r.substring(0, f != -1 ? f : r.length);
            r = r.replace(/[-:,|]/g, "").replace(/\s*/g, "");
            if (r.length > 200) {
                r = r.substring(0, 15) + "..." + r.substring(r.length - 12)
            }
            var l = i(this, "rail_major", "data-layoutkey");
            var c = "",
                h = "",
                p = "",
                d = "";
            if (l) {
                c = i(this, "rail_minor", "data-layoutkey");
                if (c) {
                    p = i(this, "rail_minor", "data-contentkey")
                }
                h = i(this, "rail_promo", "data-layoutkey");
                if (h) {
                    d = i(this, "rail_promo", "data-contentkey")
                }
            } else {
                l = "None"
            }
            if (!p) {
                p = l
            }
            if (d) {
                if (d.length > 25) {
                    d = d.substring(0, 22) + "..."
                }
            } else {
                if (p.length > 25) {
                    p = p.substring(0, 22) + "..."
                }
            }
            var v = "[position" + $(".chevron").index(this) + "]";
            if ($(this).hasClass("chevron") && $(this).closest("div").hasClass("infoContainer")) {
                v
            }
            o(this, (h ? l + "-" + h : c ? l + "-" + c : l) + ($(this).hasClass("chevron") && $(this).closest("div").hasClass("infoContainer") ? "-" + v : ""), d ? d : p, r)
        }

        function a(e) {
        	var t = this.id ? this.id :this.className;
            switch (t) {
                case "goButton4":
                    t = "Go_findAnAgentZip";
                    break;
                case "goButton6":
                    t = "Go_findAnAgentZipModal";
                    break;
                case "claimOfficeZipGoButton5":
                    t = "Go_claimOfficeZip";
                    break;
                case "retieveSavedQuotebutton":
                    t = "retrieveSavedQuote";
                    break;
                case "getQuoteButton":
                    t = "startAQuote";
                    break;
                case "moduleButtonSubmit1":
                    t = "Go_contactByAgentName";
                    break
            }
            var n = i(this, "rail_major", "data-layoutkey");
            var r = "",
                s = "",
                u = "",
                a = "";
            if (n) {
                r = i(this, "rail_minor", "data-layoutkey");
                if (r) {
                    u = i(this, "rail_minor", "data-contentkey");
                    s = i(this, "rail_promo", "data-layoutkey");
                    if (s) {
                        a = i(this, "rail_promo", "data-contentkey")
                    }
                }
            } else {
                n = "None"
            }
            if (!u) {
                u = n
            }
            if (a) {
                if (a.length > 25) {
                    a = a.substring(0, 22) + "..."
                }
            } else {
                if (u.length > 25) {
                    u = u.substring(0, 22) + "..."
                }
            }
            o(this, s ? n + "-" + s : r ? n + "-" + r : n, a ? a : u, "cta$" + t)
        }
        
      
        $("#searchResultsContent dl dt a").addClass("searchLink");
        $(".RSSFeedSpanImg img").each(function(e) {
            if ($(this.className.indexOf("rssFeedImage"))) {
                var t = $(this).closest(".rssFeedsLinkDiv").find(".RSSFeedSpanText").text().replace(/\s*/g, "");
                $(this).attr("alt", t)
            }
        });
        $("a, #remember, #slideLeftButton , #slideRightButton, .RSSFeedSpanImg").not(".bx-next").each(u);
        $("button, input.quoteSubmit").each(a);
        $("[" + DATA_ANALYTIC_KEY + "]").bind("click", r);
        var t = true;
        var n = false
    });

    function VDPlayEvent() {
        if (videoData.Name === undefined) {
            if (videoData.Resource === "blogVideo") {
                if ($("#blogContainer h1")) {
                    videoData.Name = $("#blogContainer h1").text().replace(/\s*/g, "").substring(0, 100)
                } else if ($("#blogTitleContainer h1")) {
                    videoData.Name = $("#blogContainer h1").text().replace(/\s*/g, "").substring(0, 100)
                }
            } else if (videoData.Resource === "MCVideo") {
                if ($(".vjs-playing")) {
                    videoData.Name = $(".vjs-playing").prev().text().replace(/\s*/g, "").substring(0, 100)
                }
            } else {
                videoData.Name = $(".vjs-playing").attr("id")
            }
        }
        if (videoData.Event === "play") {
            if (videoData.Offset < 2) {
                s.linkTrackVars = "prop30,eVar5,eVar66,prop24,events";
                s.linkTrackEvents = "event67";
                s.eVar66 = "HTML5" + ":" + videoData.Name;
                s.prop30 = "HTML5" + ":" + videoData.Name;
                s.prop24 = s.pageName;
                s.events = "event67";
                s.tl(true, "o", "VideoPlay")
            } else {
                s.events = ""
            }
        }
    }

    function VDEndEvent() {
        if (videoData.Event === "ended") {
            s.linkTrackVars = "prop30,eVar5,eVar66,prop24,events";
            s.linkTrackEvents = "event68";
            s.eVar66 = "HTML5" + ":" + videoData.Name;
            s.prop30 = "HTML5" + ":" + videoData.Name;
            s.prop24 = s.pageName;
            s.events = "event68";
            s.tl(true, "o", "VideoComplete");
            videoData.Offset = 0
        }
    }
}
var x = s_readCookie("s_timeStampS");
if (x) {
    s_eraseCookie("s_timeStampS")
}
s_removeEvents = function(e, t) {
    var n = e.split(","),
        r = t.split(",");
    var i;
    for (i = 0; i < r.length; i += 1) {
        var s;
        for (s = 0; s < n.length; s += 1) {
            if (n[s] === r[i]) {
                n.splice(s, 1);
                s = s - 1
            }
        }
    }
    return n.join(",")
}

/**
 ** CUSTOMER CODE END
 **/

//Properties use the same variable to assign account such that same
//s_code used in dev, qa, staging, and production environments.
var s=s_gi(s_account); 


/************************** CONFIG SECTION **************************/
/* You may add or alter any code config here. */


/* Script currencyCode and charSet so they falls within the Modular Strategy or
 * are part of a CASE Statement which supports each division's specific
 * configuration requirements.
 * */
if(document.characterSet){s.charSet=document.characterSet;}
else{s.charSet="UTF-8";};
/* E-commerce Config */
s.currencyCode="USD";

/* Link Tracking Config */
s.trackDownloadLinks=true;
s.trackExternalLinks=true;
s.trackInlineStats=true;
s.linkDownloadFileTypes="exe,zip,wav,mp3,mov,mpg,avi,wmv,pdf,doc,docx,xls,xlsx,ppt,pptx,asx";

/* Script linkInternalFilters so that falls within the Modular Strategy or is part of a CASE Statement which supports each division's specific configuration requirements.*/
s.linkInternalFilters="javascript:,libertymutual.com,libertyquotes.com,whatsmypolicy.com,lmig.com,coachoftheyear.com,befiresmart.com,youcovered.com,whyresponsibility.com,libertymutualonline.com,libertymutualprotection.com,responsiblesports.com,responsibilityproject.com,whatsyourpolicy.com,learnreturn.com,dreamdinners.com,libertymutualteendriving.com,moneysmartblog.com,welcome.libertymutual.com,libertymutualrewards.com";


s.linkLeaveQueryString=false;
s.linkTrackVars="None";
s.linkTrackEvents="None";


/* Configuration of Channel manager channels */
s._extraSearchEngines="";
s._channelDomain="Facebook|facebook.com>Twitter|twitter.com>YouTube|youtube.com>LinkedIn|linkedin.com>MySpace|myspace.com>Other Social Media|digg.com,flickr.com,stumbleupon.com,del.icio.us,reddit.com,metacafe.com,technorati.com";
s._channelParameter="";
s._channelPattern="Display|aff_1>Email |aff_3>Offline|savings_,save_>Paid Inclusion|PM_PI";


/* WARNING: Changing the visitor namespace will cause drastic changes
to how your visitor data is collected.  Changes should only be made
when instructed to do so by your account manager.*/

s.visitorNamespace="libertymutual";
s.trackingServer="https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/metrics.libertymutual.com";
s.trackingServerSecure="https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/smetrics.libertymutual.com";
s.visitorMigrationKey="4E52BB59";   // Used to migrate to the FPC from the 3rd

/* Form Analysis Config (should be above doPlugins section) */
s.formList="";
s.trackFormList=true;
s.trackPageName=true;
s.useCommerce=true;
s.varUsed="eVar14";
s.eventList="event26"; /* Conversion Form Configuration (Abandon,Success,Error)*/


/* DynamicObjectIDs config */
function s_getObjectID(o) {
	var ID=o.href;
	return ID;
}s.getObjectID=s_getObjectID;

/* Check to run part of do_Plugins only on a pageload*/
var pLoad=true;
/* Plugin Config */
s.usePlugins=true;

function s_doPlugins(s) {
    
    /* 1.2.0.1 Standardized the pageName, channel, products, and hiers */
    ApplicationTagHarness(s);
    
	/* Add calls to plugins here */
	if(pLoad){
		pLoad=false;

        /* Revision Tracking Code */
        if(sc_code_rev && s_version && sc_code_loc){
            s.prop50=sc_code_rev+"|"+s_version+"|"+sc_code_loc;
        } else {s.prop50="unknown";}

		
		/*Collect Sales Rep*/
		if (!s.prop2){
			s.prop2 = s.getQueryParam('MM_webID');
		}	

	/* External Campaign Tracking */
		
			if(!s.campaign){
				s.campaign=s.getQueryParam('src');
				s.eVar59 = s.getQueryParam('pid');
				if (s.eVar59){
					if(s.getQueryParam('channel')){
						s.eVar59 = s.eVar59 + "|" + s.getQueryParam('channel');
					}else if(!s.getQueryParam('channel')){
						s.eVar59 = s.eVar59 + "|";
					}
					if(s.getQueryParam('position')){
						s.eVar59 = s.eVar59 + "|" + s.getQueryParam('position');
					}else if(!s.getQueryParam('position')){
						s.eVar59 = s.eVar59 + "|";
					}
					if(s.getQueryParam('age')){
						s.eVar59 = s.eVar59 + "|" + s.getQueryParam('age');
					}else if(!s.getQueryParam('age')){
						s.eVar59 = s.eVar59 + "|";
					}
					if(s.getQueryParam('insured')){
						s.eVar59 = s.eVar59 + "|" + s.getQueryParam('insured');
					}else if(!s.getQueryParam('insured')){
						s.eVar59 = s.eVar59 + "|";
					}
					if(s.getQueryParam('homeowner')){
						s.eVar59 = s.eVar59 + "|" + s.getQueryParam('homeowner');
					}else if(!s.getQueryParam('homeowner')){
						s.eVar59 = s.eVar59 + "|";
					}
					if(s.getQueryParam('incident')){
						s.eVar59 = s.eVar59 + "|" + s.getQueryParam('incident');
					}else if(!s.getQueryParam('incident')){
						s.eVar59 = s.eVar59 + "|";
					}
					if(s.getQueryParam('drivers')){
						s.eVar59 = s.eVar59 + "|" + s.getQueryParam('drivers');
					}else if(!s.getQueryParam('drivers')){
						s.eVar59 = s.eVar59 + "|";
					}
					if(s.getQueryParam('currins')){
						s.eVar59 = s.eVar59 + "|" + s.getQueryParam('currins');
					}else if(!s.getQueryParam('currins')){
						s.eVar59 = s.eVar59 + "|";
					}
					if(s.getQueryParam('married')){
						s.eVar59 = s.eVar59 + "|" + s.getQueryParam('married');
					}else if(!s.getQueryParam('married')){
						s.eVar59 = s.eVar59 + "|";
					}
					if(s.getQueryParam('adcopy')){
						s.eVar59 = s.eVar59 + "|" + s.getQueryParam('adcopy');
					}else if(!s.getQueryParam('adcopy')){
						s.eVar59 = s.eVar59 + "|";
					}
					if(s.getQueryParam('clickkey')){
						s.eVar59 = s.eVar59 + "|" + s.getQueryParam('clickkey');
					}else if(!s.getQueryParam('clickkey')){
						s.eVar59 = s.eVar59 + "|";
					}
					if(s.getQueryParam('clickid')){
						s.eVar59 = s.eVar59 + "|" + s.getQueryParam('clickid');
					}
				}
				s.eVar57 = s.getQueryParam('cmpgncde');
				s.eVar53 = s.getQueryParam('trc');
				
				if(s.campaign !== null && !s.eVar53){
					s.eVar54 = s.campaign;
					}
				if(!s.eVar75){
					s.eVar75 = s.getQueryParam('ef_id');
				}
				}
				
			s.campaign=s.getValOnce(s.campaign,"s_campaign",0);
			s.eVar53=s.getValOnce(s.eVar53,"s_trc",1);
			s.eVar54=s.getValOnce(s.eVar54,"s_src",1);
			
		//sundaySky Claim UID 
		if(s.getQueryParam('ssctx')){
			if(s.getQueryParam('ssctx') === "sky01"){
				if(s.getQueryParam('ssv')){
					s.eVar5 = "UID_" + s.getQueryParam('ssv');
				}
			}
		}
		
		//Kenshoo ID
		if (s.getQueryParam("k_clickid")) {
			s.eVar25 = s.getValOnce(s.getQueryParam("k_clickid"), "s_v25", 0);
		}
		
		//Amplifinity
		s.eVar30 = s.getQueryParam('ampID');
		// Email PIN TRacking 
		s.prop67 = s.getQueryParam('PIN');
		
		/* Cross Sell Tracking added 03/31/2009 */
		if(!s.eVar36){s.eVar36=s.getQueryParam('csell');}
		
		// Copy original referrer into s.referrer if it exists
		var tempVar;
		tempVar=s.getQueryParam('origref');
		if(tempVar){s.referrer=tempVar;}

		/* Set up page URL after direction removes tracking parameters */
		var origPageURL = s.pageURL;
		s.pageURL = s.pageURL ? s.pageURL : ' '+s.wd.location;
		if (s.campaign&&s.pageURL.indexOf("src")<0){
			s.pageURL += (s.pageURL.indexOf('?')<0 ? '?src=':'&src=')+ s.campaign;
		}
		/* Channel Manager Plug-In Configuration */
		s.channelManager('src,trc',':','s_cm','','1','');/* Have Channel Manager lookg for the query parameters cmp or cmpid or cid. If it finds more than one then it will concatonate the values using ':'. */
		s.pageURL = origPageURL;

        
        if(s._channel=="Natural Search"){
        	
         s._campaign= s._partner + "-" + s._channel + "-" + s._keywords.toLowerCase(); /* If the Channel is Organic then build a campaign for it by concatonating partner (Google or Yahoo, etc - the search engine referring traffic), channel, and keyword. */
         s.eVar46 = s._partner +":"+ s._keywords.toLowerCase().replace(/\+/g, " ");
         s.events = s.apl(s.events,'event31',',',1);
          }
        
        
        if (s._channel){
			if(s.getQueryParam('src')) {
			 s._channel=s.getQueryParam('src')
			   }
			if(s.getQueryParam('trc')) {
			 s._channel=s.getQueryParam('src');
			}
        }

        if(s._channel=="Referrers"){
        	
                s._channel=s._campaign ? "Non-Compliant":"Organic";/* If the Channel is 'Referrers' then rename it to 'Other Referrers'.*/
                s._campaign= s._campaign?s._campaign:s._channel + "-" + s._referringDomain;/* If the Channel is 'Other Referrers' then build the Other Refferrers Campaign by concatonating the Channel and the Referring Domain.*/
        }

        /* Channel Manager Variables need to be assigned */
       	if (s._partner){
       	s.eVar51=s._channel + "|" + s._partner;
        }
        else{
       	 	s.eVar51=s._channel;
       }
        
        /* ClickPast Events need to be assigned */
        s.clickPast(s._campaign,'event21','event22');

		
		/* set New/Repeat*/
		s.prop23=s.eVar28=s.getNewRepeat(365);
		s.prop23=s.getValOnce(s.prop23,'p23',0);

	} /* pLoad End */

	/* Form analysis plug-in v.2.1 */
	s.setupFormAnalysis();


	/* Plugin: timeparting - User Time - hour,day,weekday */
	s.eVar24=s.prop19=s.getTimeParting('d','0',new Date().getFullYear())+"|"+s.getTimeParting('h','0',new Date().getFullYear()); //set day and hour
	

	/* populate event 30 with linkHandler */
	var url=s.downloadLinkHandler('exe,zip,wav,mp3,mov,mpg,avi,wmv,pdf,doc,docx,xls,xlsx,ppt,pptx,asx');
	if(url){
		s.prop22=url;
		s.events=s.apl(s.events,'event30',',',1);
		s.linkTrackVars="prop24,prop22,events";
		s.linkTrackEvents="event30";
		s.prop24=s.pageName?s.pageName:s.wd.location;	
	}

	s.linkLeaveQueryString=true;
	url = s.linkHandler('ShowDoc','d');
	if(url){
		s.prop22=s.getQueryParam('cid','',url);
		s.events=s.apl(s.events,'event30',',',1);
		s.linkTrackVars="prop24,prop22,events";
		s.linkTrackEvents="event30";
		s.prop24=s.pageName?s.pageName:s.wd.location;
	}
	s.linkLeaveQueryString=false;

	/*Find a Sales within sales process */
	url=s.linkHandler("Find a Sales~[URL of Find a Sales]");
	if(url){
		s.events=s.apl(s.events,'event9',',',1);
		s.eVar33=s.getPreviousValue(s.pageName,'gpv_e33','');
		s.linkTrackEvents="event9";
		s.linkTrackVars="event9,eVar33";
	}

	/*Quote Time to Complete*/
	if(s.events){
		if(s.events.indexOf('event3')>-1)
			s.eVar32='start';
		if(s.events.indexOf('event4')>-1)
			s.eVar32='stop';
		s.eVar32=s.getTimeToComplete(s.eVar32,'ttc',0);
	}

	/* To setup Dynamic Object IDs for clickmap*/
	s.setupDynamicObjectIDs();

	/*store pagename into prop30 for pathing with flash*/
	//if(s.pageName){s.prop30=s.pageName;}

	/*personalization code*/
	if(s.prop33){s.c_w('p33',s.prop33,0);}
	else {s.prop33=s.c_r('p33');}
	s.eVar35=s.prop33;

	// Detect Flash Version/SilverLight Version
	s.detectRIA('s_ria','prop16','prop27','','','1');


	
	/* Plugin Example: trackTNT 1.0*/
	s.tnt = s.trackTNT();
	/*Custom variables*/
	/* Plugin Example: trackTNT 1.0*/
	//s.tnt = s.trackTNT("om_tnt","omn_tnt");
	/*Do not blank out variable global after it is read*/
	/* Plugin Example: trackTNT 1.0*/
	//s.tnt = s.trackTNT("om_tnt","omn_tnt", false);
         
}
s.doPlugins=s_doPlugins;

  /* Turn on and configure debugging here */
  s.debugTracking = true;
  s.trackLocal = true;
 
/************************** PLUGINS SECTION *************************/
/* You may insert any plugins you wish to use here.                 */

/*
* TNT Integration Plugin v1.0
*/
s.trackTNT =new Function("v","p","b",""
+"var s=this,n='s_tnt',p=p?p:n,v=v?v:n,r='',pm=false,b=b?b:true;if(s."
+"getQueryParam){pm=s.getQueryParam(p);}if(pm){r+=(pm+',');}if(s.wd[v"
+"]!=undefined){r+=s.wd[v];}if(b){s.wd[v]='';}return r;");

/* 1.2.2 Minor version outpyt for Flash
/*
 * Plugin: detectRIA v0.1 - detect and set Flash, Silverlight versions
 */
s.detectRIA=new Function("cn", "fp", "sp", "mfv", "msv", "sf", ""
+"cn=cn?cn:'s_ria';msv=msv?msv:2;mfv=mfv?mfv:10;var s=this,sv='',fv=-"
+"1,dwi=0,fr='',sr='',w,mt=s.n.mimeTypes,uk=s.c_r(cn),k=s.c_w('s_cc',"
+"'true',0)?'Y':'N';fk=uk.substring(0,uk.indexOf('|'));sk=uk.substrin"
+"g(uk.indexOf('|')+1,uk.length);if(k=='Y'&&s.p_fo('detectRIA')){if(u"
+"k&&!sf){if(fp){s[fp]=fk;}if(sp){s[sp]=sk;}return false;}if(!fk&&fp)"
+"{if(s.pl&&s.pl.length){if(s.pl['Shockwave Flash 2.0'])fv=2;x=s.pl['"
+"Shockwave Flash'];if(x){fv=0;z=x.description;if(z)fv=z.substring(16"
+",z.indexOf('.')+2);}}else if(navigator.plugins&&navigator.plugins.len"
+"gth){x=navigator.plugins['Shockwave Flash'];if(x){fv=0;z=x.descript"
+"ion;if(z)fv=z.substring(16,z.indexOf('.')+2);}}else if(mt&&mt.length)"
+"{x=mt['application/x-shockwave-flash'];if(x&&x.enabledPlugin)fv=0;}"
+"if(fv<=0)dwi=1;w=s.u.indexOf('Win')!=-1?1:0;if(dwi&&s.isie&&w&&exec"
+"Script){result=false;for(var i=mfv;i>=3&&result!=true;i--){execScri"
+"pt('on error resume next: result = IsObject(CreateObject(\"Shockwav"
+"eFlash.ShockwaveFlash.'+i+'\"))','VBScript');fv=i;}}fr=fv==-1?'flas"
+"h not detected':fv==0?'flash enabled (no version)':'flash '+fv;}if("
+"!sk&&sp&&s.apv>=4.1){var tc='try{x=new ActiveXObject(\"AgControl.A'"
+"+'gControl\");for(var i=msv;i>0;i--){for(var j=9;j>=0;j--){if(x.is'"
+"+'VersionSupported(i+\".\"+j)){sv=i+\".\"+j;break;}}if(sv){break;}'"
+"+'}}catch(e){try{x=navigator.plugins[\"Silverlight Plug-In\"];sv=x'"
+"+'.description.substring(0,x.description.indexOf(\".\")+2);}catch('"
+"+'e){}}';eval(tc);sr=sv==''?'silverlight not detected':'silverlight"
+" '+sv;}if((fr&&fp)||(sr&&sp)){s.c_w(cn,fr+'|'+sr,0);if(fr)s[fp]=fr;"
+"if(sr)s[sp]=sr;}}");
/*********************************************************************
* Function p_fo(x,y): Ensures the plugin code is fired only on the
*      first call of do_plugins
* Returns:
*     - 1 if first instance on firing
*     - 0 if not first instance on firing
*********************************************************************/

s.p_fo=new Function("n",""
+"var s=this;if(!s.__fo){s.__fo=new Object;}if(!s.__fo[n]){s.__fo[n]="
+"new Object;return 1;}else {return 0;}");


/*
 *	Plug-in: manageQueryParam v1.2 - Manages query string parameters
 *	by either encoding, swapping, or both encoding and swapping a value.
 */

s.manageQueryParam=new Function("p","w","e","u",""
+"var s=this,x,y,i,qs,qp,qv,f,b;u=u?u:(s.pageURL?s.pageURL:''+s.wd.lo"
+"cation);u=u=='f'?''+s.gtfs().location:u+'';x=u.indexOf('?');qs=x>-1"
+"?u.substring(x,u.length):'';u=x>-1?u.substring(0,x):u;x=qs.indexOf("
+"'?'+p+'=');if(x>-1){y=qs.indexOf('&');f='';if(y>-1){qp=qs.substring"
+"(x+1,y);b=qs.substring(y+1,qs.length);}else{qp=qs.substring(1,qs.le"
+"ngth);b='';}}else{x=qs.indexOf('&'+p+'=');if(x>-1){f=qs.substring(1"
+",x);b=qs.substring(x+1,qs.length);y=b.indexOf('&');if(y>-1){qp=b.su"
+"bstring(0,y);b=b.substring(y,b.length);}else{qp=b;b='';}}}if(e&&qp)"
+"{y=qp.indexOf('=');qv=y>-1?qp.substring(y+1,qp.length):'';var eui=0"
+";while(qv.indexOf('%25')>-1){qv=unescape(qv);eui++;if(eui==10)break"
+";}qv=s.rep(qv,'+',' ');qv=escape(qv);qv=s.rep(qv,'%25','%');qv=s.re"
+"p(qv,'%7C','|');qv=s.rep(qv,'%7c','|');qp=qp.substring(0,y+1)+qv;}i"
+"f(w&&qp){if(f)qs='?'+qp+'&'+f+b;else if(b)qs='?'+qp+'&'+b;else qs='"
+"?'+qp}else if(f)qs='?'+f+'&'+qp+b;else if(b)qs='?'+qp+'&'+b;else if"
+"(qp)qs='?'+qp;return u+qs;");

/*
 * Utility Function: vpr - set the variable vs with value v
 */
s.vpr=new Function("vs","v",
"if(typeof(v)!='undefined'){var s=this; eval('s.'+vs+'=\"'+v+'\"')}");
/*
 * Function - read combined cookies v 0.3
 */
if(!s.__ccucr){s.c_rr=s.c_r;s.__ccucr = true;
s.c_r=new Function("k",""
+"var s=this,d=new Date,v=s.c_rr(k),c=s.c_rr('s_pers'),i,m,e;if(v)ret"
+"urn v;k=s.ape(k);i=c.indexOf(' '+k+'=');c=i<0?s.c_rr('s_sess'):c;i="
+"c.indexOf(' '+k+'=');m=i<0?i:c.indexOf('|',i);e=i<0?i:c.indexOf(';'"
+",i);m=m>0?m:e;v=i<0?'':s.epa(c.substring(i+2+k.length,m<0?c.length:"
+"m));if(m>0&&m!=e)if(parseInt(c.substring(m+1,e<0?c.length:e))<d.get"
+"Time()){d.setTime(d.getTime()-60000);s.c_w(s.epa(k),'',d);v='';}ret"
+"urn v;");}
/*
 * Function - write combined cookies v 0.3
 */
if(!s.__ccucw){s.c_wr=s.c_w;s.__ccucw = true;
s.c_w=new Function("k","v","e",""
+"this.new2 = true;"
+"var s=this,d=new Date,ht=0,pn='s_pers',sn='s_sess',pc=0,sc=0,pv,sv,"
+"c,i,t;d.setTime(d.getTime()-60000);if(s.c_rr(k)) s.c_wr(k,'',d);k=s"
+".ape(k);pv=s.c_rr(pn);i=pv.indexOf(' '+k+'=');if(i>-1){pv=pv.substr"
+"ing(0,i)+pv.substring(pv.indexOf(';',i)+1);pc=1;}sv=s.c_rr(sn);i=sv"
+".indexOf(' '+k+'=');if(i>-1){sv=sv.substring(0,i)+sv.substring(sv.i"
+"ndexOf(';',i)+1);sc=1;}d=new Date;if(e){if(e.getTime()>d.getTime())"
+"{pv+=' '+k+'='+s.ape(v)+'|'+e.getTime()+';';pc=1;}}else{sv+=' '+k+'"
+"='+s.ape(v)+';';sc=1;}if(sc) s.c_wr(sn,sv,0);if(pc){t=pv;while(t&&t"
+".indexOf(';')!=-1){var t1=parseInt(t.substring(t.indexOf('|')+1,t.i"
+"ndexOf(';')));t=t.substring(t.indexOf(';')+1);ht=ht<t1?t1:ht;}d.set"
+"Time(ht);s.c_wr(pn,pv,d);}return v==s.c_r(s.epa(k));");}
/*
 * Plugin: getTimeToComplete 0.4 - return the time from start to stop
 */
s.getTimeToComplete=new Function("v","cn","e",""
+"var s=this,d=new Date,x=d,k;if(!s.ttcr){e=e?e:0;if(v=='start'||v=='"
+"stop')s.ttcr=1;x.setTime(x.getTime()+e*86400000);if(v=='start'){s.c"
+"_w(cn,d.getTime(),e?x:0);return '';}if(v=='stop'){k=s.c_r(cn);if(!s"
+".c_w(cn,'',d)||!k)return '';v=(d.getTime()-k)/1000;var td=86400,th="
+"3600,tm=60,r=5,u,un;if(v>td){u=td;un='days';}else if(v>th){u=th;un="
+"'hours';}else if(v>tm){r=2;u=tm;un='minutes';}else{r=.2;u=1;un='sec"
+"onds';}v=v*r/u;return (Math.round(v)/r)+' '+un;}}return '';");
/*
 * Utility Function: split v1.5 - split a string (JS 1.0 compatible)
 */
s.split=new Function("l","d",""
+"var i,x=0,a=new Array;while(l){i=l.indexOf(d);i=i>-1?i:l.length;a[x"
+"++]=l.substring(0,i);l=l.substring(i+d.length);}return a");

/*
 * channelManager v2.85 - Tracking External Traffic
 */
s.channelManager=new Function("a","b","c","d","e","f","g",""
+"var s=this,h=new Date,i=0,j,k,l,m,n,o,p,q,r,t,u,v,w,x,y,z,A,B,C,D,E"
+",F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T;h.setTime(h.getTime()+1800000);if(e)"
+"{i=1;if(s.c_r(e))i=0;if(!s.c_w(e,1,h))s.c_w(e,1,0);if(!s.c_r(e))i=0"
+";if(f&&s.c_r('s_tbm'+f))i=0;}j=s.referrer?s.referrer:document.refer"
+"rer;j=unescape(j.toLowerCase());if(!j)k=1;else {l=j.indexOf('?')>-1"
+"?j.indexOf('?'):j.length;m=j.substring(0,l);n=s.split(j,'/');n=s.sp"
+"lit(n[2],'?');o=n[0].toLowerCase();p=s.linkInternalFilters.toLowerC"
+"ase();p=s.split(p,',');for(q=0;q<p.length;q++){r=o.indexOf(p[q])==-"
+"1?'':j;if(r)break;}}if(!r&&!k){t=j;u=v=o;w='Other Natural Referrers"
+"';x=s.seList+'>'+s._extraSearchEngines;if(d==1){m=s.repl(m,'oogle',"
+"'%');m=s.repl(m,'ahoo','^');j=s.repl(j,'as_q','*');}y=s.split(x,'>'"
+");for(z=0;z<y.length;z++){A=y[z];A=s.split(A,'|');B=s.split(A[0],',"
+"');for(C=0;C<B.length;C++){D=m.indexOf(B[C]);if(D>-1){if(A[2])E=v=A"
+"[2];else E=o;if(d==1){E=s.repl(E,'#',' - ');j=s.repl(j,'*','as_q');"
+"E=s.repl(E,'^','ahoo');E=s.repl(E,'%','oogle');}F=s.split(A[1],',')"
+";for(G=0;G<F.length;G++){if(j.indexOf(F[G]+'=')>-1||j.indexOf('http"
+"s://www.google.')==0||j.indexOf('http://r.search.yahoo.com/')==0)H=1"
+";I=s.getQueryParam(F[G],'',j).toLowerCase();if(H||I)break;}}if(H||I"
+")break;}if(H||I)break;}}if(!r||g!='1'){J=s.split(a,',');K=0;while(!"
+"T&&K<J.length){T=s.getQueryParam(J[K],b);K++;}if(T){v=T;if(E)w='Pai"
+"d Search';else w='Unknown Paid Channel';}if(!T&&E&&H){v=E;w='Natura"
+"l Search';}}if(i&&k&&!T)t=u=v=w='Typed/Bookmarked';J=s._channelDoma"
+"in;if(J&&o&&!r){K=s.split(J,'>');for(L=0;L<K.length;L++){M=s.split("
+"K[L],'|');N=s.split(M[1],',');O=N.length;for(P=0;P<O;P++){Q=N[P].to"
+"LowerCase();R=o.indexOf(Q);if(R>-1){w=M[0];break;}}if(R>-1)break;}}"
+"J=s._channelParameter;if(J){K=s.split(J,'>');for(L=0;L<K.length;L++"
+"){M=s.split(K[L],'|');N=s.split(M[1],',');O=N.length;for(P=0;P<O;P+"
+"+){R=s.getQueryParam(N[P]);if(R){w=M[0];break;}}if(R)break;}}J=s._c"
+"hannelPattern;if(J){K=s.split(J,'>');for(L=0;L<K.length;L++){M=s.sp"
+"lit(K[L],'|');N=s.split(M[1],',');O=N.length;for(P=0;P<O;P++){Q=N[P"
+"].toLowerCase();R=T.toLowerCase();S=R.indexOf(Q);if(S==0){w=M[0];br"
+"eak;}}if(S==0)break;}}S=w?T+u+w+I:'';c=c?c:'c_m';if(c!='0')S=s.getV"
+"alOnce(S,c,0);if(S){s._campaignID=T?T:'n/a';s._referrer=t?t:'n/a';s"
+"._referringDomain=u?u:'n/a';s._campaign=v?v:'n/a';s._channel=w?w:'n"
+"/a';s._partner=E?E:'n/a';s._keywords=H?I?I:'Keyword Unavailable':'n"
+"/a';if(f&&w!='Typed/Bookmarked'){h.setTime(h.getTime()+f*86400000);"
+"s.c_w('s_tbm'+f,1,h);}}else s._campaignID=s._referrer=s._referringD"
+"omain=s._campaign=s._channel=s._partner=s._keywords='';");

/* Top 246 */
s.seList="7search.com|qu|7search.com>search.aol.com|q|AOL.com Search>nortonsafe.search.ask.com|q|Ask Jeeves>isearch.avg.com|q|AVG Secure Search>blekko.com|q|Blekko>lavasoft.blekko.com|q|Blekko>search.blekko.com|q|Blekko>so.360.cn|q|Comcast Search>m.search.daum.net|q|Daum>search.daum.net|q|Daum>dogpile.com|q|Dogpile>eniro.se|search_word|Eniro - Sweden>fruitmail.excite.co.jp|search|Excite - Japan>google.com|q|Google>google.com.af|q|Google - Afghanistan>google.dz|q|Google - Algerie>google.as|q|Google - American Samoa>google.com.ai|q|Google - Anguilla>google.com.ag|q|Google - Antigua and Barbuda>google.am|q|Google - Armenia>google.az|q|Google - Azerbaijan>google.by|q|Google - Belarus>google.com.bz|q|Google - Belize>google.co.bw|q|Google - Botswana>google.vg|q|Google - British Virgin Islands>google.com.bn|q|Google - Brunei>google.com.kh|q|Google - Cambodia>google.cm|q|Google - Cameroun>ditu.google.cn|q|Google - China>google.ci|q|Google - Cote D'Ivoire>google.me|q|Google - Crna Gora>google.dm|q|Google - Dominica>google.com|q|Google - Estonia>google.com.et|q|Google - Ethiopia>google.com.fj|q|Google - Fiji>google.gp|q|Google - Guadeloupe>google.gy|q|Google - Guyana>google.ht|q|Google - Haiti>mmx.websiteforever.com|q|Google - Haiti>google.iq|q|Google - Iraq>google.im|q|Google - Isle of Man>maps.google.co.jp|q|Google - Japan>google.je|q|Google - Jersey>google.kz|q|Google - Kazakhstan>google.kg|q|Google - Kyrgyzstan>google.la|q|Google - Laos>google.com.ly|q|Google - Libya>google.lu|q|Google - Luxembourg>google.mv|q|Google - Maldives>maps.google.mu|q|Google - Mauritius>google.fm|q|Google - Micronesia>google.md|q|Google - Moldova>google.mn|q|Google - Mongolia>google.com.na|q|Google - Namibia>google.com.np|q|Google - Nepal>startgoogle.startpagina.nl|q|Google - Netherlands (Startpagina)>google.com.om|q|Google - Oman>google.ps|q|Google - Palestinian territories>google.com.pa|q|Google - Panama>google.cd|q|Google - Rep. Dem. du Congo>google.ge|q|Google - Repulic of Georgia>google.com.vc|q|Google - Saint Vincent and the Grenadine>google.sn|q|Google - Senegal>google.rs|q|Google - Serbia>google.sc|q|Google - Seychelles>google.tg|q|Google - Togo>google.tn|q|Google - Tunisia>google.tm|q|Google - Turkmenistan>google.co.ug|q|Google - Uganda>google.co.zm|q|Google - Zambia>google.co.zw|q|Google - Zimbabwe>search.icq.com|q|icq>ixquick.com|query|ixquick>search.lycos.com|q|Lycos>m.baidu.com|word|m.baidu.com>m.bing.com|q|m.bing>go.mail.ru|q|Mail.ru>editorial.autos.msn.com|q|MSN>living.msn.com|q|MSN>realestate.msn.com|q|MSN>search.myway.com|searchfor|MyWay.com>search.mywebsearch.com|searchfor|mywebsearch>search.naver.com|query|Naver>ask.reference.com|q|Reference.com>addons.searchalot.com|q|Searchalot>jfilter.popxml.com|q|Searchalot>searchalot.com|q|Searchalot>www1.search-results.com|q|search-results.com>search.fenrir-inc.com|q|Sleipnir>sogou.com|query|Sogou>so-net.ne.jp|query|So-net>suche.web.de|su|Web.de>search.yahoo.com|p|Yahoo!>us.yhs4.search.yahoo.com|p|Yahoo!>ar.search.yahoo.com|p|Yahoo! - Argentina>au.search.yahoo.com|p|Yahoo! - Australia>at.search.yahoo.com|p|Yahoo! - Austria>ca.search.yahoo.com|p|Yahoo! - Canada>fr.search.yahoo.com|p|Yahoo! - France>de.search.yahoo.com|p|Yahoo! - Germany>hk.search.yahoo.com|p|Yahoo! - Hong Kong>in.search.yahoo.com|p|Yahoo! - India>it.search.yahoo.com|p|Yahoo! - Italy>search.yahoo.co.jp|p|Yahoo! - Japan>kr.search.yahoo.com|p|Yahoo! - Korea>malaysia.search.yahoo.com|p|Yahoo! - Malaysia>mx.search.yahoo.com|p|Yahoo! - Mexico>nz.search.yahoo.com|p|Yahoo! - New Zealand>ph.search.yahoo.com|p|Yahoo! - Philippines>sg.search.yahoo.com|p|Yahoo! - Singapore>es.search.yahoo.com|p|Yahoo! - Spain>games.search.yahoo.com|p|Yahoo! - Spain>espanol.search.yahoo.com|p|Yahoo! - Spanish (US : Telemundo)>tw.search.yahoo.com|p|Yahoo! - Taiwan>uk.search.yahoo.com|p|Yahoo! - UK and Ireland>vn.search.yahoo.com|p|Yahoo! - Viet Nam>yandex.ru|text|Yandex.ru>zensearch.com|q|ZenSearch>google.,googlesyndication.com,.googleadservices.com|q,as_q|"
+"Google>bing.com|q|Bing>yahoo.com,yahoo.co.jp|p,va|Yahoo!>ask.jp,ask"
+".co|q,ask|Ask>.aol.,suche.aolsvc.de|q,query|AOL>altavista.co,altavi"
+"sta.de|q,r|AltaVista>.mywebsearch.com|searchfor|MyWebSearch>webcraw"
+"ler.com|q|WebCrawler>wow.com|q|Wow>infospace.com|q|InfoSpace>blekko"
+".com|q|Blekko>dogpile.com|q|DogPile>alhea.com|q|Alhea>goduckgo.com|"
+"q|GoDuckGo>info.com|qkw|Info.com>contenko.com|q|Contenko>www.baidu."
+"com|wd|Baidu>daum.net,search.daum.net|q|Daum>icqit.com|q|icq>myway."
+"com|searchfor|MyWay.com>naver.com,search.naver.com|query|Naver>nets"
+"cape.com|query,search|Netscape Search>reference.com|q|Reference.com"
+">seznam|w|Seznam.cz>abcsok.no|q|Startsiden>tiscali.it,www.tiscali.c"
+"o.uk|key,query|Tiscali>virgilio.it|qs|Virgilio>yandex|text|Yandex.r"
+"u>optimum.net|q|Optimum Search";
/*
 * Plugin Utility: clickPast for introduction and channel bounce.
 */
s.clickPast=new Function("scp","ct_ev","cp_ev","cpc",""
+"var s=this,scp,ct_ev,cp_ev,cpc,ev,tct;if(s.p_fo(ct_ev)==1){if(!cpc)"
+"{cpc='s_cpc';}ev=s.events?s.events+',':'';if(scp){s.events=ev+ct_ev"
+";s.c_w(cpc,1,0);}else{if(s.c_r(cpc)>=1){s.events=ev+cp_ev;s.c_w(cpc"
+",0,0);}}}");

/*
 * Plugin Utility: Replace v1.0
 */
s.repl=new Function("x","o","n",""
+"var i=x.indexOf(o),l=n.length;while(x&&i>=0){x=x.substring(0,i)+n+x."
+"substring(i+o.length);i=x.indexOf(o,i+l)}return x");
/*
 * Plugin Utility: apl v1.1
 */
s.apl=new Function("L","v","d","u",""
+"var s=this,m=0;if(!L)L='';if(u){var i,n,a=s.split(L,d);for(i=0;i<a."
+"length;i++){n=a[i];m=m||(u==1?(n==v):(n.toLowerCase()==v.toLowerCas"
+"e()));}}if(!m)L=L?L+d+v:v;return L");
/*
 * s.join: 1.0 - s.join(v,p)
 */
s.join = new Function("v","p",""
+"var s = this;var f,b,d,w;if(p){f=p.front?p.front:'';b=p.back?p.back"
+":'';d=p.delim?p.delim:'';w=p.wrap?p.wrap:'';}var str='';for(var x=0"
+";x<v.length;x++){if(typeof(v[x])=='object' )str+=s.join( v[x],p);el"
+"se str+=w+v[x]+w;if(x<v.length-1)str+=d;}return f+str+b;");
/*
 *	Plug-in: crossVisitParticipation v1.6 - stacks values from
 *	specified variable in cookie and returns value
 */
s.crossVisitParticipation=new Function("v","cn","ex","ct","dl","ev","dv",""
+"var s=this,ce;if(typeof(dv)==='undefined')dv=0;if(s.events&&ev){var"
+" ay=s.split(ev,',');var ea=s.split(s.events,',');for(var u=0;u<ay.l"
+"ength;u++){for(var x=0;x<ea.length;x++){if(ay[u]==ea[x]){ce=1;}}}}i"
+"f(!v||v==''){if(ce){s.c_w(cn,'');return'';}else return'';}v=escape("
+"v);var arry=new Array(),a=new Array(),c=s.c_r(cn),g=0,h=new Array()"
+";if(c&&c!='')arry=eval(c);var e=new Date();e.setFullYear(e.getFullY"
+"ear()+5);if(dv==0&&arry.length>0&&arry[arry.length-1][0]==v)arry[ar"
+"ry.length-1]=[v,new Date().getTime()];else arry[arry.length]=[v,new"
+" Date().getTime()];var start=arry.length-ct<0?0:arry.length-ct;var "
+"td=new Date();for(var x=start;x<arry.length;x++){var diff=Math.roun"
+"d((td.getTime()-arry[x][1])/86400000);if(diff<ex){h[g]=unescape(arr"
+"y[x][0]);a[g]=[arry[x][0],arry[x][1]];g++;}}var data=s.join(a,{deli"
+"m:',',front:'[',back:']',wrap:\"'\"});s.c_w(cn,data,e);var r=s.join"
+"(h,{delim:dl});if(ce)s.c_w(cn,'');return r;");
/*
 * Plugin: getQueryParam 2.4
 */
s.getQueryParam=new Function("p","d","u","h",""
+"var s=this,v='',i,j,t;d=d?d:'';u=u?u:(s.pageURL?s.pageURL:s.wd.loca"
+"tion);if(u=='f')u=s.gtfs().location;while(p){i=p.indexOf(',');i=i<0"
+"?p.length:i;t=s.p_gpv(p.substring(0,i),u+'',h);if(t){t=t.indexOf('#"
+"')>-1?t.substring(0,t.indexOf('#')):t;}if(t)v+=v?d+t:t;p=p.substrin"
+"g(i==p.length?i:i+1)}return v");
s.p_gpv=new Function("k","u","h",""
+"var s=this,v='',q;j=h==1?'#':'?';i=u.indexOf(j);if(k&&i>-1){q=u.sub"
+"string(i+1);v=s.pt(q,'&','p_gvf',k)}return v");
s.p_gvf=new Function("t","k",""
+"if(t){var s=this,i=t.indexOf('='),p=i<0?t:t.substring(0,i),v=i<0?'T"
+"rue':t.substring(i+1);if(p.toLowerCase()==k.toLowerCase())return s."
+"epa(v)}return''");
/*
 * Plugin: getValOnce_v1.0
 */
s.getValOnce=new Function("v","c","e",""
+"var s=this,a=new Date,v=v?v:v='',c=c?c:c='s_gvo',e=e?e:0,k=s.c_r(c"
+");if(v){a.setTime(a.getTime()+e*86400000);s.c_w(c,v,e?a:0);}return"
+" v==k?'':v");
/*
 * Plugin: getParentVal_v1.0
 */
s.getParentVal = new Function("v", "c", "e", ""
+ "var s=this,a=new Date,v=v?v:v='',c=c?c:c='s_gvo',e=e?e:0,k=s.c_r(c"
+ ");if(v){a.setTime(a.getTime()+e*86400000);s.c_w(c,v,e?a:0);}return"
+ " v==''?k:v");
/* 1.2.0.5 - Plug-in to set the internal channel eVar for referring channel. */
/*
* Plugin: setInternalChannel
* ev := Internal Channel eVar
* ch  := Internal Channel
* path  := Top Section
* campaign := the campaign tracking code from referrer.
* cv  := cookie parameter
* ttl  := ttl (default 0)
*/
s.setInternalChannel = function (ch,path, campaign, cv,ttl) {
   var s = this,a=new Date,v=ch?ch:'',p=path?path:'', ic=campaign?campaign:'',
       c=cv?cv:'s_ich',e=ttl?ttl:0,k=s.c_r(c);
   if(v){
       a.setTime(a.getTime()+e*86400000);
       s.c_w(c,v+":"+p,e?a:0);
   }
   if (k){
      return (v != k.split(':')[0])? k+'|'+ic : '';
   }
   else {
       return v+":"+p+'|'+ic;
   }
};
s.setInternalCampaign = function (campaign, cv,ttl) {
	   var s = this,a=new Date,ic=campaign?campaign:'',
	       c=cv?cv:'s_ich',e=ttl?ttl:0,k=s.c_r(c);
	   if(k){
		   var v = k.split('|');
	       a.setTime(a.getTime()+e*86400000);
	       s.c_w(c,v[0]+"|"+v[1]+"|"+ic,e?a:0);
	   }
};
s.setInternalPlacement = function (cta, content, location, cv, ttl) {
	   var s = this,a=new Date, ic=cta?cta:'', cc=content?content:'', cl=location?location:'',
	       c=cv?cv:'s_ich',e=ttl?ttl:0,k=s.c_r(c);
	   if(k){
		   var v = k.split('|');
	       a.setTime(a.getTime()+e*86400000);
	       s.c_w(c,v[0]+"|"+v[1]+"|"+cl+":"+cc+":"+ic,e?a:0);
	   }
	   return s.c_r(c);
};
s.getInternalChannel = function (cv){
	var s = this,c=cv?cv:'s_ich',k=s.c_r(c);
	if (k){
		var v = k.split('|');
		var v1 = k.split(':');
		var result = new Object;
		result.property = v[0];
		result.channel = v1[0];
		result.section = v1[1];
		result.pagename = v1[2]?v1[2].split('|')[0]:"NOPAGENAME";
		result.placement = result.campaign = v[2];		
		return result;
	}
	return "";
};
/* 1.2.0.7 - Check if current channel is a micro site */
/**
 *  Plugin: Check For Micro Site
 */
s.setMicroSite = function (ch, cv, ttl) {
    var s = this,a=new Date,v=ch?ch:'',c=cv?cv:'s_mcs',e=ttl?ttl:0;
    if(v){a.setTime(a.getTime()+e*86400000);s.c_w(c,v,e?a:0);}
};
s.isMicroSite = function (ch, cv) {
    var s = this,v=ch?ch:'',c=cv?cv:'s_mcs',k=s.c_r(c);
   	return v==k;
};
/*
 * Time Parting Plugin - the code has been modified to get the users time
 */
s.getTimeParting=new Function("t","z","y",""
+"dc=new Date('1/1/2000');f=15;ne=8;if(dc.getDay()!=6||"
+"dc.getMonth()!=0){return'Data Not Available'}else{;z=parseInt(z);"
+"if(y=='2009'){f=8;ne=1};gmar=new Date('3/1/'+y);dsts=f-gmar.getDay("
+");gnov=new Date('11/1/'+y);dste=ne-gnov.getDay();spr=new Date('3/'"
+"+dsts+'/'+y);fl=new Date('11/'+dste+'/'+y);cd=new Date();"
+";utc=cd.getTime()"
+";tz=new Date(utc + (3600000*z));thisy=tz.getFullYear("
+");var days=['Sunday','Monday','Tuesday','Wednesday','Thursday','Fr"
+"iday','Saturday'];if(thisy!=y){return'Data Not Available'}else{;thi"
+"sh=tz.getHours();thismin=tz.getMinutes();thisd=tz.getDay();var dow="
+"days[thisd];var ap='AM';var dt='Weekday';var mint='00';if(thismin>3"
+"0){mint='30'}if(thish>=12){ap='PM';thish=thish-12};if (thish==0){th"
+"ish=12};if(thisd==6||thisd==0){dt='Weekend'};var timestring=thish+'"
+":'+mint+ap;var daystring=dow;var endstring=dt;if(t=='h'){return tim"
+"estring}if(t=='d'){return daystring};if(t=='w'){return en"
+"dstring}}};"
);
/*
 * Plugin: getNewRepeat 1.2 - Returns whether user is new or repeat
 */
s.getNewRepeat=new Function("d","cn",""
+"var s=this,e=new Date(),cval,sval,ct=e.getTime();d=d?d:30;cn=cn?cn:"
+"'s_nr';e.setTime(ct+d*24*60*60*1000);cval=s.c_r(cn);if(cval.length="
+"=0){s.c_w(cn,ct+'-New',e);return'New';}sval=s.split(cval,'-');if(ct"
+"-sval[0]<30*60*1000&&sval[1]=='New'){s.c_w(cn,ct+'-New',e);return'N"
+"ew';}else{s.c_w(cn,ct+'-Repeat',e);return'Repeat';}");

/*
 * Plugin: linkHandler 0.5 - identify and report custom links
 */
s.linkHandler=new Function("p","t",""
+"var s=this,h=s.p_gh(),i,l;t=t?t:'o';if(!h||(s.linkType&&(h||s.linkN"
+"ame)))return '';i=h.indexOf('?');h=s.linkLeaveQueryString||i<0?h:h."
+"substring(0,i);l=s.pt(p,'|','p_gn',h.toLowerCase());if(l){s.linkNam"
+"e=l=='[['?'':l;s.linkType=t;return h;}return '';");
s.p_gn=new Function("t","h",""
+"var i=t?t.indexOf('~'):-1,n,x;if(t&&h){n=i<0?'':t.substring(0,i);x="
+"t.substring(i+1);if(h.indexOf(x.toLowerCase())>-1)return n?n:'[[';}"
+"return 0;");
/*
 * Plugin: downloadLinkHandler 0.5 - identify and report download links
 */
s.downloadLinkHandler=new Function("p",""
+"var s=this,h=s.p_gh(),n='linkDownloadFileTypes',i,t;if(!h||(s.linkT"
+"ype&&(h||s.linkName)))return '';i=h.indexOf('?');t=s[n];s[n]=p?p:t;"
+"if(s.lt(h)=='d')s.linkType='d';else h='';s[n]=t;return h;");
/*
 * Utility Function: p_gh
 */
s.p_gh=new Function(""
+"var s=this;if(!s.eo&&!s.lnk)return '';var o=s.eo?s.eo:s.lnk,y=s.ot("
+"o),n=s.oid(o),x=o.s_oidt;if(s.eo&&o==s.eo){while(o&&!n&&y!='BODY'){"
+"o=o.parentElement?o.parentElement:o.parentNode;if(!o)return '';y=s."
+"ot(o);n=s.oid(o);x=o.s_oidt}}return o.href?o.href:'';");
/*
 * DynamicObjectIDs v1.4: Setup Dynamic Object IDs based on URL
 */
s.setupDynamicObjectIDs=new Function(""
+"var s=this;if(!s.doi){s.doi=1;if(s.apv>3&&(!s.isie||!s.ismac||s.apv"
+">=5)){if(s.wd.attachEvent)s.wd.attachEvent('onload',s.setOIDs);else"
+" if(s.wd.addEventListener)s.wd.addEventListener('load',s.setOIDs,fa"
+"lse);else{s.doiol=s.wd.onload;s.wd.onload=s.setOIDs}}s.wd.s_semapho"
+"re=1}");
s.setOIDs=new Function("e",""
+"var s=s_c_il["+s._in+"],b=s.eh(s.wd,'onload'),o='onclick',x,l,u,c,i"
+",a=new Array;if(s.doiol){if(b)s[b]=s.wd[b];s.doiol(e)}if(s.d.links)"
+"{for(i=0;i<s.d.links.length;i++){l=s.d.links[i];c=l[o]?''+l[o]:'';b"
+"=s.eh(l,o);z=l[b]?''+l[b]:'';u=s.getObjectID(l);if(u&&c.indexOf('s_"
+"objectID')<0&&z.indexOf('s_objectID')<0){u=s.repl(u,'\"','');u=s.re"
+"pl(u,'\\n','').substring(0,97);l.s_oc=l[o];a[u]=a[u]?a[u]+1:1;x='';"
+"if(c.indexOf('.t(')>=0||c.indexOf('.tl(')>=0||c.indexOf('s_gs(')>=0"
+")x='var x=\".tl(\";';x+='s_objectID=\"'+u+'_'+a[u]+'\";return this."
+"s_oc?this.s_oc(e):true';if(s.isns&&s.apv>=5)l.setAttribute(o,x);l[o"
+"]=new Function('e',x)}}}s.wd.s_semaphore=0;return true");
/*
 * Plugin: Form Analysis 2.1 (Success, Error, Abandonment)
 */
s.setupFormAnalysis=new Function(""
+"var s=this;if(!s.fa){s.fa=new Object;var f=s.fa;f.ol=s.wd.onload;s."
+"wd.onload=s.faol;f.uc=s.useCommerce;f.vu=s.varUsed;f.vl=f.uc?s.even"
+"tList:'';f.tfl=s.trackFormList;f.fl=s.formList;f.va=new Array('',''"
+",'','')}");
s.sendFormEvent=new Function("t","pn","fn","en",""
+"var s=this,f=s.fa;t=t=='s'?t:'e';f.va[0]=pn;f.va[1]=fn;f.va[3]=t=='"
+"s'?'Success':en;s.fasl(t);f.va[1]='';f.va[3]='';");
s.faol=new Function("e",""
+"var s=s_c_il["+s._in+"],f=s.fa,r=true,fo,fn,i,en,t,tf;if(!e)e=s.wd."
+"event;f.os=new Array;if(f.ol)r=f.ol(e);if(s.d.forms&&s.d.forms.leng"
+"th>0){for(i=s.d.forms.length-1;i>=0;i--){fo=s.d.forms[i];fn=fo.name"
+";tf=f.tfl&&s.pt(f.fl,',','ee',fn)||!f.tfl&&!s.pt(f.fl,',','ee',fn);"
+"if(tf){f.os[fn]=fo.onsubmit;fo.onsubmit=s.faos;f.va[1]=fn;f.va[3]='"
+"No Data Entered';for(en=0;en<fo.elements.length;en++){el=fo.element"
+"s[en];t=el.type;if(t&&t.toUpperCase){t=t.toUpperCase();var md=el.on"
+"mousedown,kd=el.onkeydown,omd=md?md.toString():'',okd=kd?kd.toStrin"
+"g():'';if(omd.indexOf('.fam(')<0&&okd.indexOf('.fam(')<0){el.s_famd"
+"=md;el.s_fakd=kd;el.onmousedown=s.fam;el.onkeydown=s.fam}}}}}f.ul=s"
+".wd.onunload;s.wd.onunload=s.fasl;}return r;");
s.faos=new Function("e",""
+"var s=s_c_il["+s._in+"],f=s.fa,su;if(!e)e=s.wd.event;if(f.vu){s[f.v"
+"u]='';f.va[1]='';f.va[3]='';}su=f.os[this.name];return su?su(e):tru"
+"e;");
s.fasl=new Function("e",""
+"var s=s_c_il["+s._in+"],f=s.fa,a=f.va,l=s.wd.location,ip=s.trackPag"
+"eName,p=s.pageName;if(a[1]!=''&&a[3]!=''){a[0]=!p&&ip?l.host+l.path"
+"name:a[0]?a[0]:p;if(!f.uc&&a[3]!='No Data Entered'){if(e=='e')a[2]="
+"'Error';else if(e=='s')a[2]='Success';else a[2]='Abandon'}else a[2]"
+"='';var tp=ip?a[0]+':':'',t3=e!='s'?':('+a[3]+')':'',ym=!f.uc&&a[3]"
+"!='No Data Entered'?tp+a[1]+':'+a[2]+t3:tp+a[1]+t3,ltv=s.linkTrackV"
+"ars,lte=s.linkTrackEvents,up=s.usePlugins;if(f.uc){s.linkTrackVars="
+"ltv=='None'?f.vu+',events':ltv+',events,'+f.vu;s.linkTrackEvents=lt"
+"e=='None'?f.vl:lte+','+f.vl;f.cnt=-1;if(e=='e')s.events=s.pt(f.vl,'"
+",','fage',2);else if(e=='s')s.events=s.pt(f.vl,',','fage',1);else s"
+".events=s.pt(f.vl,',','fage',0)}else{s.linkTrackVars=ltv=='None'?f."
+"vu:ltv+','+f.vu}s[f.vu]=ym;s.usePlugins=false;var faLink=new Object"
+"();faLink.href='#';s.tl(faLink,'o','Form Analysis');s[f.vu]='';s.us"
+"ePlugins=up}return f.ul&&e!='e'&&e!='s'?f.ul(e):true;");
s.fam=new Function("e",""
+"var s=s_c_il["+s._in+"],f=s.fa;if(!e) e=s.wd.event;var o=s.trackLas"
+"tChanged,et=e.type.toUpperCase(),t=this.type.toUpperCase(),fn=this."
+"form.name,en=this.name,sc=false;if(document.layers){kp=e.which;b=e."
+"which}else{kp=e.keyCode;b=e.button}et=et=='MOUSEDOWN'?1:et=='KEYDOW"
+"N'?2:et;if(f.ce!=en||f.cf!=fn){if(et==1&&b!=2&&'BUTTONSUBMITRESETIM"
+"AGERADIOCHECKBOXSELECT-ONEFILE'.indexOf(t)>-1){f.va[1]=fn;f.va[3]=e"
+"n;sc=true}else if(et==1&&b==2&&'TEXTAREAPASSWORDFILE'.indexOf(t)>-1"
+"){f.va[1]=fn;f.va[3]=en;sc=true}else if(et==2&&kp!=9&&kp!=13){f.va["
+"1]=fn;f.va[3]=en;sc=true}if(sc){nface=en;nfacf=fn}}if(et==1&&this.s"
+"_famd)return this.s_famd(e);if(et==2&&this.s_fakd)return this.s_fak"
+"d(e);");
s.ee=new Function("e","n",""
+"return n&&n.toLowerCase?e.toLowerCase()==n.toLowerCase():false;");
s.fage=new Function("e","a",""
+"var s=this,f=s.fa,x=f.cnt;x=x?x+1:1;f.cnt=x;return x==a?e:'';");


/* WARNING: Changing any of the below variables will cause drastic
changes to how your visitor data is collected.  Changes should only be
made when instructed to do so by your account manager.*/


/****************************** MODULES *****************************/
/* Module: Media */
s.m_Media_c="var m=s.m_i('Media');if(m.completeByCloseOffset==undefined)m.completeByCloseOffset=1;if(m.completeCloseOffsetThreshold==undefined)m.completeCloseOffsetThreshold=1;m.cn=function(n){var m="
+"this;return m.s.rep(m.s.rep(m.s.rep(n,\"\\n\",''),\"\\r\",''),'--**--','')};m.open=function(n,l,p,b){var m=this,i=new Object,tm=new Date,a='',x;n=m.cn(n);if(!l)l=-1;if(n&&p){if(!m.l)m.l=new Object;"
+"if(m.l[n])m.close(n);if(b&&b.id)a=b.id;if(a)for (x in m.l)if(m.l[x]&&m.l[x].a==a)m.close(m.l[x].n);i.n=n;i.l=l;i.o=0;i.x=0;i.p=m.cn(m.playerName?m.playerName:p);i.a=a;i.t=0;i.ts=0;i.s=Math.floor(tm"
+".getTime()/1000);i.lx=0;i.lt=i.s;i.lo=0;i.e='';i.to=-1;i.tc=0;i.fel=new Object;i.vt=0;i.sn=0;i.sx=\"\";i.sl=0;i.sg=0;i.sc=0;i.us=0;i.co=0;i.cot=0;i.lm=0;i.lom=0;m.l[n]=i}};m._delete=function(n){var"
+" m=this,i;n=m.cn(n);i=m.l[n];m.l[n]=0;if(i&&i.m)clearTimeout(i.m.i)};m.close=function(n){this.e(n,0,-1)};m.play=function(n,o,sn,sx,sl){var m=this,i;i=m.e(n,1,o,sn,sx,sl);if(i&&!i.m){i.m=new Object;"
+"i.m.m=new Function('var m=s_c_il['+m._in+'],i;if(m.l){i=m.l[\"'+m.s.rep(i.n,'\"','\\\\\"')+'\"];if(i){if(i.lx==1)m.e(i.n,3,-1);i.m.i=setTimeout(i.m.m,1000)}}');i.m.m()}};m.complete=function(n,o){th"
+"is.e(n,5,o)};m.stop=function(n,o){this.e(n,2,o)};m.track=function(n){this.e(n,4,-1)};m.bcd=function(vo,i){var m=this,ns='a.media.',v=vo.linkTrackVars,e=vo.linkTrackEvents,pe='m_i',pev3,c=vo.context"
+"Data,x;c['a.contentType']='video';c[ns+'name']=i.n;c[ns+'playerName']=i.p;if(i.l>0){c[ns+'length']=i.l;}c[ns+'timePlayed']=Math.floor(i.ts);if(!i.vt){c[ns+'view']=true;pe='m_s';i.vt=1}if(i.sx){c[ns"
+"+'segmentNum']=i.sn;c[ns+'segment']=i.sx;if(i.sl>0)c[ns+'segmentLength']=i.sl;if(i.sc&&i.ts>0)c[ns+'segmentView']=true}if(!i.cot&&i.co){c[ns+\"complete\"]=true;i.cot=1}if(i.lm>0)c[ns+'milestone']=i"
+".lm;if(i.lom>0)c[ns+'offsetMilestone']=i.lom;if(v)for(x in c)v+=',contextData.'+x;pev3='video';vo.pe=pe;vo.pev3=pev3;var d=m.contextDataMapping,y,a,l,n;if(d){vo.events2='';if(v)v+=',events';for(x i"
+"n d){if(x.substring(0,ns.length)==ns)y=x.substring(ns.length);else y=\"\";a=d[x];if(typeof(a)=='string'){l=m.s.sp(a,',');for(n=0;n<l.length;n++){a=l[n];if(x==\"a.contentType\"){if(v)v+=','+a;vo[a]="
+"c[x]}else if(y){if(y=='view'||y=='segmentView'||y=='complete'||y=='timePlayed'){if(e)e+=','+a;if(c[x]){if(y=='timePlayed'){if(c[x])vo.events2+=(vo.events2?',':'')+a+'='+c[x];}else if(c[x])vo.events"
+"2+=(vo.events2?',':'')+a}}else if(y=='segment'&&c[x+'Num']){if(v)v+=','+a;vo[a]=c[x+'Num']+':'+c[x]}else{if(v)v+=','+a;vo[a]=c[x]}}}}else if(y=='milestones'||y=='offsetMilestones'){x=x.substring(0,"
+"x.length-1);if(c[x]&&d[x+'s'][c[x]]){if(e)e+=','+d[x+'s'][c[x]];vo.events2+=(vo.events2?',':'')+d[x+'s'][c[x]]}}}vo.contextData=0}vo.linkTrackVars=v;vo.linkTrackEvents=e};m.bpe=function(vo,i,x,o){v"
+"ar m=this,pe='m_o',pev3,d='--**--';pe='m_o';if(!i.vt){pe='m_s';i.vt=1}else if(x==4)pe='m_i';pev3=m.s.ape(i.n)+d+Math.floor(i.l>0?i.l:1)+d+m.s.ape(i.p)+d+Math.floor(i.t)+d+i.s+d+(i.to>=0?'L'+Math.fl"
+"oor(i.to):'')+i.e+(x!=0&&x!=2?'L'+Math.floor(o):'');vo.pe=pe;vo.pev3=pev3};m.e=function(n,x,o,sn,sx,sl,pd){var m=this,i,tm=new Date,ts=Math.floor(tm.getTime()/1000),c,l,v=m.trackVars,e=m.trackEvent"
+"s,ti=m.trackSeconds,tp=m.trackMilestones,to=m.trackOffsetMilestones,sm=m.segmentByMilestones,so=m.segmentByOffsetMilestones,z=new Array,j,t=1,w=new Object,x,ek,tc,vo=new Object;n=m.cn(n);i=n&&m.l&&"
+"m.l[n]?m.l[n]:0;if(i){if(o<0){if(i.lx==1&&i.lt>0)o=(ts-i.lt)+i.lo;else o=i.lo}if(i.l>0)o=o<i.l?o:i.l;if(o<0)o=0;i.o=o;if(i.l>0){i.x=(i.o/i.l)*100;i.x=i.x>100?100:i.x}if(i.lo<0)i.lo=o;tc=i.tc;w.name"
+"=n;w.length=i.l;w.openTime=new Date;w.openTime.setTime(i.s*1000);w.offset=i.o;w.percent=i.x;w.playerName=i.p;if(i.to<0)w.mediaEvent=w.event='OPEN';else w.mediaEvent=w.event=(x==1?'PLAY':(x==2?'STOP"
+"':(x==3?'MONITOR':(x==4?'TRACK':(x==5?'COMPLETE':('CLOSE'))))));if(!pd){if(i.pd)pd=i.pd}else i.pd=pd;w.player=pd;if(x>2||(x!=i.lx&&(x!=2||i.lx==1))) {if(!sx){sn=i.sn;sx=i.sx;sl=i.sl}if(x){if(x==1)i"
+".lo=o;if((x<=3||x==5)&&i.to>=0){t=0;v=e=\"None\";if(i.to!=o){l=i.to;if(l>o){l=i.lo;if(l>o)l=o}z=tp?m.s.sp(tp,','):0;if(i.l>0&&z&&o>=l)for(j=0;j<z.length;j++){c=z[j]?parseFloat(''+z[j]):0;if(c&&(l/i"
+".l)*100<c&&i.x>=c){t=1;j=z.length;w.mediaEvent=w.event='MILESTONE';i.lm=w.milestone=c}}z=to?m.s.sp(to,','):0;if(z&&o>=l)for(j=0;j<z.length;j++){c=z[j]?parseFloat(''+z[j]):0;if(c&&l<c&&o>=c){t=1;j=z"
+".length;w.mediaEvent=w.event='OFFSET_MILESTONE';i.lom=w.offsetMilestone=c}}}}if(i.sg||!sx){if(sm&&tp&&i.l>0){z=m.s.sp(tp,',');if(z){z[z.length]='100';l=0;for(j=0;j<z.length;j++){c=z[j]?parseFloat('"
+"'+z[j]):0;if(c){if(i.x<c){sn=j+1;sx='M:'+l+'-'+c;j=z.length}l=c}}}}else if(so&&to){z=m.s.sp(to,',');if(z){z[z.length]=''+(i.l>0?i.l:'E');l=0;for(j=0;j<z.length;j++){c=z[j]?parseFloat(''+z[j]):0;if("
+"c||z[j]=='E'){if(o<c||z[j]=='E'){sn=j+1;sx='O:'+l+'-'+c;j=z.length}l=c}}}}if(sx)i.sg=1}if((sx||i.sx)&&sx!=i.sx){i.us=1;if(!i.sx){i.sn=sn;i.sx=sx}if(i.to>=0)t=1}if(x>=2&&i.lo<o){i.t+=o-i.lo;i.ts+=o-"
+"i.lo}if(x<=2||(x==3&&!i.lx)){i.e+=(x==1||x==3?'S':'E')+Math.floor(o);i.lx=(x==3?1:x)}if(!t&&i.to>=0&&x<=3){ti=ti?ti:0;if(ti&&i.ts>=ti){t=1;w.mediaEvent=w.event='SECONDS'}}i.lt=ts;i.lo=o}if(!x||i.x>"
+"=100){x=0;m.e(n,2,-1,0,0,-1,pd);v=e=\"None\";w.mediaEvent=w.event=\"CLOSE\"}if(x==5||(m.completeByCloseOffset&&(!x||i.x>=100)&&i.l>0&&o>=i.l-m.completeCloseOffsetThreshold)){w.complete=i.co=1;t=1}e"
+"k=w.mediaEvent;if(ek=='MILESTONE')ek+='_'+w.milestone;else if(ek=='OFFSET_MILESTONE')ek+='_'+w.offsetMilestone;if(!i.fel[ek]) {w.eventFirstTime=true;i.fel[ek]=1}else w.eventFirstTime=false;w.timePl"
+"ayed=i.t;w.segmentNum=i.sn;w.segment=i.sx;w.segmentLength=i.sl;if(m.monitor&&x!=4)m.monitor(m.s,w);if(x==0)m._delete(n);if(t&&i.tc==tc){vo=new Object;vo.contextData=new Object;vo.linkTrackVars=v;vo"
+".linkTrackEvents=e;if(!vo.linkTrackVars)vo.linkTrackVars='';if(!vo.linkTrackEvents)vo.linkTrackEvents='';if(m.trackUsingContextData)m.bcd(vo,i);else m.bpe(vo,i,x,o);m.s.t(vo);if(i.us){i.sn=sn;i.sx="
+"sx;i.sc=1;i.us=0}else if(i.ts>0)i.sc=0;i.e=\"\";i.lm=i.lom=0;i.ts-=Math.floor(i.ts);i.to=o;i.tc++}}}return i};m.ae=function(n,l,p,x,o,sn,sx,sl,pd,b){var m=this,r=0;if(n&&(!m.autoTrackMediaLengthReq"
+"uired||(length&&length>0)) &&p){if(!m.l||!m.l[n]){if(x==1||x==3){m.open(n,l,p,b);r=1}}else r=1;if(r)m.e(n,x,o,sn,sx,sl,pd)}};m.a=function(o,t){var m=this,i=o.id?o.id:o.name,n=o.name,p=0,v,c,c1,c2,x"
+"c=m.s.h,x,e,f1,f2='s_media_'+m._in+'_oc',f3='s_media_'+m._in+'_t',f4='s_media_'+m._in+'_s',f5='s_media_'+m._in+'_l',f6='s_media_'+m._in+'_m',f7='s_media_'+m._in+'_c',tcf,w;if(!i){if(!m.c)m.c=0;i='s"
+"_media_'+m._in+'_'+m.c;m.c++}if(!o.id)o.id=i;if(!o.name)o.name=n=i;if(!m.ol)m.ol=new Object;if(m.ol[i])return;m.ol[i]=o;if(!xc)xc=m.s.b;tcf=new Function('o','var e,p=0;try{if(o.versionInfo&&o.curre"
+"ntMedia&&o.controls)p=1}catch(e){p=0}return p');p=tcf(o);if(!p){tcf=new Function('o','var e,p=0,t;try{t=o.GetQuickTimeVersion();if(t)p=2}catch(e){p=0}return p');p=tcf(o);if(!p){tcf=new Function('o'"
+",'var e,p=0,t;try{t=o.GetVersionInfo();if(t)p=3}catch(e){p=0}return p');p=tcf(o)}}v=\"var m=s_c_il[\"+m._in+\"],o=m.ol['\"+i+\"']\";if(p==1){p='Windows Media Player '+o.versionInfo;c1=v+',n,p,l,x=-"
+"1,cm,c,mn;if(o){cm=o.currentMedia;c=o.controls;if(cm&&c){mn=cm.name?cm.name:c.URL;l=cm.duration;p=c.currentPosition;n=o.playState;if(n){if(n==8)x=0;if(n==3)x=1;if(n==1||n==2||n==4||n==5||n==6)x=2;}"
+"';c2='if(x>=0)m.ae(mn,l,\"'+p+'\",x,x!=2?p:-1,0,\"\",0,0,o)}}';c=c1+c2;if(m.s.isie&&xc){x=m.s.d.createElement('script');x.language='jscript';x.type='text/javascript';x.htmlFor=i;x.event='PlayStateC"
+"hange(NewState)';x.defer=true;x.text=c;xc.appendChild(x);o[f6]=new Function(c1+'if(n==3){x=3;'+c2+'}setTimeout(o.'+f6+',5000)');o[f6]()}}if(p==2){p='QuickTime Player '+(o.GetIsQuickTimeRegistered()"
+"?'Pro ':'')+o.GetQuickTimeVersion();f1=f2;c=v+',n,x,t,l,p,p2,mn;if(o){mn=o.GetMovieName()?o.GetMovieName():o.GetURL();n=o.GetRate();t=o.GetTimeScale();l=o.GetDuration()/t;p=o.GetTime()/t;p2=o.'+f5+"
+"';if(n!=o.'+f4+'||p<p2||p-p2>5){x=2;if(n!=0)x=1;else if(p>=l)x=0;if(p<p2||p-p2>5)m.ae(mn,l,\"'+p+'\",2,p2,0,\"\",0,0,o);m.ae(mn,l,\"'+p+'\",x,x!=2?p:-1,0,\"\",0,0,o)}if(n>0&&o.'+f7+'>=10){m.ae(mn,l"
+",\"'+p+'\",3,p,0,\"\",0,0,o);o.'+f7+'=0}o.'+f7+'++;o.'+f4+'=n;o.'+f5+'=p;setTimeout(\"'+v+';o.'+f2+'(0,0)\",500)}';o[f1]=new Function('a','b',c);o[f4]=-1;o[f7]=0;o[f1](0,0)}if(p==3){p='RealPlayer '"
+"+o.GetVersionInfo();f1=n+'_OnPlayStateChange';c1=v+',n,x=-1,l,p,mn;if(o){mn=o.GetTitle()?o.GetTitle():o.GetSource();n=o.GetPlayState();l=o.GetLength()/1000;p=o.GetPosition()/1000;if(n!=o.'+f4+'){if"
+"(n==3)x=1;if(n==0||n==2||n==4||n==5)x=2;if(n==0&&(p>=l||p==0))x=0;if(x>=0)m.ae(mn,l,\"'+p+'\",x,x!=2?p:-1,0,\"\",0,0,o)}if(n==3&&(o.'+f7+'>=10||!o.'+f3+')){m.ae(mn,l,\"'+p+'\",3,p,0,\"\",0,0,o);o.'"
+"+f7+'=0}o.'+f7+'++;o.'+f4+'=n;';c2='if(o.'+f2+')o.'+f2+'(o,n)}';if(m.s.wd[f1])o[f2]=m.s.wd[f1];m.s.wd[f1]=new Function('a','b',c1+c2);o[f1]=new Function('a','b',c1+'setTimeout(\"'+v+';o.'+f1+'(0,0)"
+"\",o.'+f3+'?500:5000);'+c2);o[f4]=-1;if(m.s.isie)o[f3]=1;o[f7]=0;o[f1](0,0)}};m.as=new Function('e','var m=s_c_il['+m._in+'],l,n;if(m.autoTrack&&m.s.d.getElementsByTagName){l=m.s.d.getElementsByTag"
+"Name(m.s.isie?\"OBJECT\":\"EMBED\");if(l)for(n=0;n<l.length;n++)m.a(l[n]);}');if(s.wd.attachEvent)s.wd.attachEvent('onload',m.as);else if(s.wd.addEventListener)s.wd.addEventListener('load',m.as,fal"
+"se);if(m.onLoad)m.onLoad(s,m)";
s.m_i("Media");

/* Module: Integrate */
s.m_Integrate_c="=function(~`L,m=p._m~||!`E.prototype~p.disable~;i<m.l.length;i++){p=m[m.l[i]]`7p&&!~){var m=this,~Integrate~;if(~||!`E.prototype[x]))~&&(!s.isopera||`I7)~};m.~m._fu(p,u)~=new ~m.s.lo"
+"adModule(~Object~p._d~_'+p._~return ~s.apv>=~tm.getTime()~)u=s.rep(u,'~){var p=this~.substring(~Math.~_'+m._in+'_~beacon~script~p[f](s,p)}~for(x in ~delay~ready~http~s.wd[~m.onLoad~p._c++;~;for(i=~"
+";p.RAND~)if(x&&~random~floor(~~var m=s.m_i('`6');m.add`0n,o`5p`7!o)o='s_`6_'+n`7!`Wo])`Wo]`C`E;m[n]`C`E;p=m[n];p._n=n;p._m=m;p._c=0;`F=0;`3=0;p.get=m.get;p.`T=m.`T;p.`U=m.`U;p.`P=m.`P;p.`Q=m.`Q;m.l"
+"[m.l.length]=n`A_g`0t`5s=m.s,i,p,f=(t?'use':'set')+'Vars',tcf`Z0`4`3&&p[f]){if(`I5`9){tcf`CFunction('s','p','f','var e;try{`Rcatch(e){}');tcf(s,p,f)}else `R}`A_t`0){this._g(1)`A_fu`0p,u`5s=m.s,x,v,"
+"tm`CDate`7u.toLowerCase()`M0,4) != '`V')u='`V://'+u`7s.ssl`K`V:','`Vs:')`a=Math&&`N`c?`N`d`N`c()*10000000000000):`J`a+=`N`d`J/10800000)%10;`Sp`bx`M0,1)!='_'&&(!`E`2`8{v=''+p[x]`7v==p[x]||parseFloat"
+"(v)==p[x]`K['+x+']',s.rep(escape(v),'+','%2B'))}`Hu`Aget`0u,v`1`7!`3){if(!v)v='s`O`6`Gn+'_get`Gc;`Yp.VAR=v;`F++;`D'`6:'+v,`B,0,1,p._n)}`A`T`0`L`7`F<=0)`F=1`A`U`0`1;`F=0`7!`3)m.s.dlt()`A_d`0`5i,p`Z0"
+"`4`3&&`F>0)`H1}`H0`A_x`0d,n`L[n],x`7!`3){`Sd`b(!`E`2`8p[x]=d[x];`F--}`A`P`0u`1,s=m.s,imn='s_i`O`6`Gn+'`Gc,im`7!`3&&s.d.images&&`I3`9&&(s.ns6<0||`I6.1)){`Yim=`Wimn]`CImage;im.src=`B}`A`Q`0u`1`7!`3)`"
+"D0,`B,0,1)`Al`CArray`7`X)`X(s,m)";
s.m_i("Integrate");

/************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/
var s_code='',s_objectID;function s_gi(un,pg,ss){var c="=fun^a(~){`Ns=`u~.substring(~#I ~.indexOf(~;$G~`f$G~=new Fun^a(~.toLowerCase()~.length~=new Object~`Ns#Nc_il['+s^H$s],~};s.~){$G~`ZMigrationSer"
+"ver~.toUpperCase~s.wd~','~);s.~')q='~ookieDomainPeriods~.location~^LingServer~var ~dynamicAccount~=='~link~s.m_~@Oc_i~=new Array~BufferedRequests~s.apv>=~Element~)$Gx^g!Object#kObject.prototype#kOb"
+"ject.prototype[x])~:#s+~visitor~#E@m(~referrer~@ZTime()~s.maxDelay~=''~else ~s.pt(~s.apv=parseFloat(~@l(~.protocol~=new Date~@OobjectID=@o=$W=$Wv1=$Wv2=$Wv3~}$G~}c#S(e){~javaEnabled~conne^a^S~for(~"
+"onclick~Name~ternalFilters~this~javascript~s.dl~@js.b.addBehavior(\"# default# ~Timeout(~'+tm@Z~cookie~while(~;i++){~&&s.~s.^h~o@Ooid~browser~.parent~colorDepth~String~.host~s.un~s.eo~.lastIndexOf("
+"'~s.vl_g~s.sq~parseInt(~._i~s.fl(~s.p_l~t=s.ot(o)~track~nload~');~j='1.~window~#eURL~}else{~Type~s.c_r(~s.c_w(~s.vl_l~lugins~'){q='~dynamicVariablePrefix~document~ction~Sampling~s.rc[un]~)s.d.write"
+"(~Event~#MURIComponent(x)~&&(~tfs~resolution~'s_~s.eh~s.isie~Secure~Height~tcf~isopera~ismac~escape(~.href~screen.~s#Ngi(~Version~harCode~'+$s~name~variableProvider~idth~')>=~)?'Y':'N'~u=m[t+1](~e&"
+"&l$oSESSION'~&&!~home#e~.src~,#2)~s.em=~s.oun~s.lnk~s.rl[u~s.ssl~o.type~s.vl_t~.inner~=s.sp(~Lifetime~s.gg('objectID~$8new Image;i~sEnabled~\"'+~){s.~.s_~.toPrecision)~o.textContent~ExternalLinks~s"
+"et~charSet~onerror~http~currencyCode~disable~=\"m_\"+n~.get~MigrationKey~(''+~'+(~f',~){p=~r=s[f](~u=m[t](~c+='s.'+k+'=~Opera~;try{~Math.~s.rep~s.ape~s.fsg~s.ppu~s.ns6~InlineStats~&&l$oNONE'~Track~"
+"'0123456789~true~height~loadModule~s.epa(~s.va_g~s.va_t~m._d~=1 border=~s.d.images~n=s.oid(o)~,'sqs',q);~LeaveQuery~?'&~'=')~n]=~'||t~\",\"\\~),\"\\~){t=~'_'+~'+n;~,255)}~if(~vo)~s.sampled~=s.oh(o)"
+";~'<im'+'g ~1);~:'';h=h?h~sess~campaign~lif~1900~ in ~un)~lnk=~.target;~s.co(~s.pe~s.c_d~s.brl~s.nrs~s[mn]~,'vo~s.pl~=(apn~space~\"s_gs(\")~vo._t~b.attach~2o7.net'~ alt=\"\">~Listener~Year(~d.creat"
+"e~=s.n.app~!='~+';'~&&o~)+'/~n+'~s()+'~){n=~){i=~():''~;n++)~a['!'+t]~://')i+=~){v=s.n.~channel~100~s.apv>3~o.value~g+\"_c\"]~')dc='1~\".tl(\")~etscape~s_')t=t~sr'+'c=~i)clear~omePage~+=(~='+~&&t~}"
+"}}~[b](e);~return~mobile~events~random~code~=s_~=un~,pev~'MSIE ~floor(~atch~transa~s.num(~s.c_gd~s.mr~Vars~,'lt~,id,ta~;s.gl(~,f1,f2~=s.p_c~',s.bc~page~Group,~.fromC~sByTag~++;~')<~||!~c){~x){x~){m"
+"~i);~y+=~&&m~)+'\"~'')~[t]=~[i]=~'+v]~+1))~\"s\",~=l[n];~!a[t])~~s._c=^jc';`G=^P`5!`G`Sn){`G`Sl`T;`G`Sn=0;}s^Hl=`G`Sl;s^Hn=`G`Sn;s^Hl[s^H$8s;`G`Sn#is.an#Nan;s.cls`0x,#l`Ni,y`e`5!c)c=`u.an;`qi=0;i<x"
+"`9^2n=x`2i,i+1)`5c`4n)>=0)#pn}`3y`Cfl`0x,l){`3x?@bx)`20,l):x`Cco`0o`D!o)`3o;`Nn`A,x;`qx$Ro)$Gx`4'select#j0&&x`4'filter#j0)n[x]=o[x];`3n`Cnum`0#m`e+x;`q`Np=0;p<x`9;p++)$G(@t')`4x`2p,p#w<0)`30;`31`Cr"
+"ep#Nrep;s.sp#Nsp;s.jn#Njn;@m`0x`1,h=@tABCDEF',i,c=s.@T,n,l,e,y`e;c=c?c`F$w`5#m`e+x`5@9=3)`3en^f;`6c`PAUTO'^g#s.c^wAt){`qi=0;i<x`9^2c=x`2i,i+$Ln=x.c^wAt(i)`5n>127){l=0;e`e;^1n||l<4){e=h`2n%16,n%16+1"
+")+e;n=(n-n%16)/16;l++}#p'%u'+e}`6c`P+')#p'%2B';`f#p^rc)}`3y^Rx=`i^r''+x),'+`H%2B')`5c^3em==1&&x`4'%u#j0&&x`4'%U#j0$vx`4'%^N^1i>=0){i++`5h`28)`4x`2i,i+1)`F())>=0)`3x`20,i)+'u00'+x`2#oi=x`4'%',i)#G}`"
+"3x`Cepa`0x`1`5#m`e+x;`3@9=3?de^f:un^r`ix,'+`H '))}`3x`Cpt`0x,d,f,a`1,t=x,z=0,y,r;^1t){y=t`4d);y=y<0?t`9:y;t=t`20,y);@ft,a)`5r)`3r;z+=y+d`9;t=x`2z,x`9);t=z<x`9?t:''}`3''`Cisf`0t,a){`Nc=a`4':')`5c>=0"
+")a=a`20,c)`5t`20,2)`P#9`22);`3(t!`e#F==a)`Cfsf`0t,a`1`5`ga,`H,'is@dt))@n#D@n!`e?`H`Yt;`30`Cfs`0x,f`1;@n`e;`gx,`H,'fs@df);`3@n`Csi`0`1,i,k,v,c#Ngi+'`N^u@M@A+'\"`Isa(@M^B+'\");';`qi=0;i<@y`9^2k=@y[i]"
+";v=s[k]`5v!#Odefined`Dtypeof(v)`Pstring')@h@Ms_fe(v#r;';`f@h'+v$p}}c+=\"@B=^C=s.`Q`s=s.`Q^S=`G`l`e;\";`3c`Cc_d`e;#Vf`0t,a`1`5!#Ut))`31;`30`Cc_gd`0`1,d=`G`L^A^y,n=s.fpC`K,p`5!n)n=s.c`K`5d@5$X$un?^Gn"
+"):2;n=n>2?n:2;p=d^D.')`5p>=0){^1p>=0&&n>1@ed^D.',p-$Ln--}$X=p>0&&`gd,'.`Hc_gd@d0)?d`2p):d}}`3$X`Cc_r`0k`1;k=@m(k);`Nc=' '+s.d.^0,i=c`4' '+k+$7,e=i<0?i:c`4';',i),v=i<0?'':@xc`2i+2+k`9,e<0?c`9:e));`3"
+"v$o[[B]]'?v:''`Cc_w`0k,v,e`1,d=#V(),l=s.^0@I,t;v`e+v;l=l?@bl)`F$w`5@4@r$C(v!`e?^Gl?l:0):-60)`5t){e`k;e.@STime(e`c+(t*#20))}`mk@r@Nd.^0=k+'`av!`e?v:'[[B]]')+'; path=/;@c@4?' expires#Ee.toGMT^9()$p`Y"
+"(d?' domain#Ed$p:'^N`3^Tk)==v}`30`Ceh`0o,e,r,f`1,b=^j'+e+$Ds^Hn,n=-1,l,i,x`5!^kl)^kl`T;l=^kl;`qi=0;i<l`9&&n<0;i++`Dl[i].o==o&&l[i].e==e)n=i`mn<0$ui;l[n]`A}x#yx.o=o;x.e=e;f=r?x.b:f`5r||f){x.b=r?0:o["
+"e];x.o[e]=f`mx.b){x.o[b]=x.b;`3b}`30`Ccet`0f,a,t,o,b`1,r,^o`5`V5^g!s.^p||`V7)){^o`7's`Hf`Ha`Ht`H`Ne,r@j@fa)`nr=s[t](e)}`3r^Nr=^o(s,f,a,t)^R$Gs.^q^3u`4#Q4@10)r=s[b](a);else{^k(`G,'@U',0,o);@fa`Ieh(`"
+"G,'@U',1)}}`3r`Cg^het`0e`1;`3^4`Cg^hoe`7'e`H`Bc;^k(^P,\"@U\",1`Ie^h=1;c=s.t()`5c^dc`Ie^h=0;`3@u'`Ig^hfb`0a){`3^P`Cg^hf`0w`1,p=w^7,l=w`L;^4=w`5p&&p`L!=l&&p`L^A==l^A){^4=p;`3s.g^hf(^4)}`3^4`Cg^h`0`1`"
+"5!^4){^4=`G`5!s.e^h)^4=s.cet('g^h@d^4,'g^het',s.g^hoe,'g^hfb')}`3^4`Cmrq`0u`1,l=@C],n,r;@C]=0`5l)`qn=0;n<l`9$x{r#y#W(0,0,r.r,0,r.t,r.u)}`Cbr`0id,rs`1`5s.@X`U#k^U^jbr',rs))$Y=rs`Cflush`U`0){`u.fbr(0"
+")`Cfbr`0id`1,br=^T^jbr')`5!br)br=$Y`5br`D!s.@X`U)^U^jbr`H'`Imr(0,0,br)}$Y=0`Cmr`0$N,q,rs#Z,u`1,dc=s.dc,t1=s.`M,t2=s.`M^m,tb=s.`MBase,p='.sc',ns=s.`Z`s$e,un=s.cls(u?u:(ns?ns:s.f$S),r`A,l,imn=^ji_@c$"
+"S,im,b,e`5!rs`Dt1`Dt2^3ssl)t1=t2^R$G!tb)tb='$i`5dc)dc=@bdc)`8;`fdc='d1'`5tb`P$i`Ddc`Pd1#612';`6dc`Pd2#622';p`e}t1=u$s.'+dc+'.'+p+tb}rs='@V@c@D?'s'`Y'://'+t1+'/b/ss/'+^B+'/@cs.#J?'5.1':'1'$rH.22.1/'"
+"+$N+'?AQB=1&ndh=1@cq?q`Y'&AQE=1'`5^l@5s.^q)rs=^Irs,2047)`5id@Nbr(id,rs);#I}`m$2&&`V3^g!s.^p||`V7)^g@p<0||`V6.1)`D!s.rc)s.rc`A`5!^c){^c=1`5!s.rl)s.rl`A;@Cn]`T;@S`y'$G^P`Sl)^P`Sl['+s^H$s].mrq(@Mu$s\""
+")',750)^Rl=@Cn]`5l){r.t=ta;r.u#O;r.r=rs;l[l`9]=r;`3''}imn+=$D^c;^c++}im=`G[imn]`5!im)im=`G[im@Km@Ol=0;im.o^M`7'e`H`u@Ol=1;`Nwd=^P,s`5wd`Sl){s=wd`Sl['+s^H$s];#Wq(@Mu$s\"`Inrs--`5!$Z)`Rm(\"rr\")}')`5"
+"!$Z@Nnrs=1;`Rm('rs')}`f$Z#iim@7=rs`5(!ta||ta`P_self$9a`P_top'||(`G.^y#Fa==`G.^y))&&rs`4'&pe=@10){b=e`k;^1!im@Ol&&e`c-b`c<500)e`k}`3''}`3$K#A@Mrs+'\" w@0=1 @v$10$j'`Cgg`0v`1`5!`G[^j#v)`G[^j#v`e;`3`G"
+"[^j#v`Cglf`0t,a`Dt`20,2)`P#9`22);`Ns=`u,v=s.gg(t)`5v)s#tv`Cgl`0v`1`5s.pg)`gv,`H,'gl@d0)`Crf`0x`1,y,i,j,h,l,a,b`e,c`e,t`5x){y`e+x;i=y`4'?')`5i>0){a=y`2i+$Ly=y`20,#oh=y`8;i=0`5h`20,7)`P@V$z7;`6h`20,8"
+")`P@Vs$z8;h=h`2#oi=h`4\"/\")`5i>0){h=h`20,i)`5h`4'google@10){a@Ha,'&')`5a`9>1){l=',q,ie,start,search_key,word,kw,cd,';`qj=0;j<a`9;j++$Ca[j];i=t`4$7`5i>0&&l`4`H+t`20,i)+`H)>=0)b#Db$6'`Yt;`fc#Dc$6'`Y"
+"t`mb&&#l#p'?'+b+'&'+c`5''+x!=y)x=y#G}}}`3x`Chav`0`1,qs`e,fv=s.`Q@s#X,fe=s.`Q@s^es,mn,i`5$W#nn=$W`20,1)`F()+$W`21)`5$a){fv=$a.^L#X;fe=$a.^L^es}}fv=fv?fv+`H+^V+`H+^V2:'';`qi=0;i<@z`9^2`Nk=@z[i],v=s[k"
+"],b=k`20,4),x=k`24),n=^Gx),q=k`5v&&k$o`Q`s'&&k$o`Q^S'`D$W||@B||^C`Dfv^g`H+fv+`H)`4`H+k+`H)<0)v`e`5k`P#K'&&fe)v=s.fs(v,fe)`mv`Dk`P^Y`JD';`6k`P`ZID`Jvid';`6k`P^Q^Xg';v=^Iv$F`6k`P`b^Xr';v=^Is.rf(v)$F`"
+"6k`Pvmk'||k`P`Z@a`Jvmt';`6k`P`E^Xvmf'`5@D^3`E^m)v`e}`6k`P`E^m^Xvmf'`5!@D^3`E)v`e}`6k`P@T^Xce'`5v`F()`PAUTO')v='ISO8859-1';`6@9=2||@9=3)v='UTF-8'}`6k`P`Z`s$e`Jns';`6k`Pc`K`Jcdp';`6k`P^0@I`Jcl';`6k`P"
+"^z`Jvvp';`6k`P@W`Jcc';`6k`P#1`Jch';`6k`P#T^aID`Jxact';`6k`P$O`Jv0';`6k`P^i`Js';`6k`P^8`Jc';`6k`P`v^v`Jj';`6k`P`o`Jv';`6k`P^0@L`Jk';`6k`P^6W@0`Jbw';`6k`P^6^n`Jbh';`6k`P`p`Jct';`6k`P@6`Jhp';`6k`Pp^W`"
+"Jp';`6#Ux)`Db`Pprop`Jc$E`6b`PeVar`Jv$E`6b`Plist`Jl$E`6b`Phier^Xh$Ev=^Iv$F`mv)qs+='&'+q+'=@ck`20,3)$opev'?@m(v):v)#G`3qs`Cltdf`0t,h$Ct?t`8$M`8:'';`Nqi=h`4'?^Nh=qi>=0?h`20,qi):h`5t&&h`2h`9-(t`9#w`P.'"
+"+t)`31;`30`Cltef`0t,h$Ct?t`8$M`8:''`5t&&h`4t)>=0)`31;`30`Clt`0h`1,lft=s.`QDow^MFile^Ss,lef=s.`QEx`t,$P=s.`QIn`t;$P=$P?$P:`G`L^A^y;h=h`8`5s.^LDow^MLinks&&lft&&`glft,`H#Yd@dh))`3'd'`5s.^L@R&&h`20,1)$"
+"o# '^glef||$P)^g!lef||`glef,`H#Ye@dh))^g!$P#k`g$P,`H#Ye@dh)))`3'e';`3''`Clc`7'e`H`Bb=^k(`u,\"`r\"`I$T$V`u`It(`I$T0`5b)`3`u#H`3@u'`Ibc`7'e`H`Bf,^o`5s.d^3d.all^3d.all.cppXYctnr)#I;^C=e@7`W?e@7`W:e$U^"
+"o`7#x\"`Ne@j$G^C^g^C.tag`s||^C^7`W||^C^7Node))s.t()`n}\");^o(s`Ieo=0'`Ioh`0o`1,l=`G`L,h=o^s?o^s:'',i,j,k,p;i=h`4':^Nj=h`4'?^Nk=h`4'/')`5h^gi<0||(j>=0&&i>j)||(k>=0&&i>k))@eo`j$q`j`9>1?o`j:(l`j?l`j:'"
+"^Ni=l.path^y^D/^Nh=(p?p+'//'`Y(o^A?o^A:(l^A?l^A:#s)+(h`20,1)$o/'?l.path^y`20,i<0?0:i$r'`Yh}`3h`Cot`0o){`Nt=o.tag`s;t=t#F`F?t`F$w`5t`PSHAPE')t`e`5t`D(t`PINPUT$9`PBUTTON')&&@E&&@E`F)t=@E`F();`6!t$q^s"
+")t='A';}`3t`Coid`0o`1,^K,p,c,n`e,x=0`5t@5^5@eo`j;c=o.`r`5o^s^gt`PA$9`PAREA')^g!c#kp||p`8`4'`v#j0))n$J`6c$u`i@l(`i@l@bc,\"\\r\",''$Bn\",''$Bt\",#s,' `H^Nx=2}`6t`PINPUT$9`PSUBMIT'`D#4)n=#4;`6o@GText)"
+"n=o@GText;`6@Q)n=@Q;x=3}`6o@7#F`PIMAGE')n=o@7`5n){^5=^In@8;^5t=x}}`3^5`Crqf`0t,un`1,e=t`4$7,u=e>=0?t`20,e):'',q=e>=0?@xt`2e#w:''`5u&&q^g`H+u+`H)`4`H+un+`H)>=0`Du!=^B^3un`4`H)>=0)q='&u#Eu+q+'&u=0';`"
+"3q}`3''`Crq`0un`D!$Sun=`u.un;`Ns=`u,c#O`4`H),v=^T^jsq'),q`e`5c<0)`3`gv,'&`Hrq@d$S;`3`gun,`H,'rq',0)`Csqp`0t,a`1,e=t`4$7,q=e<0?'':@xt`2e+1)`Isqq[q]`e`5e>=0)`gt`20,e),`H$4`30`Csqs`0un,q`1;^Fu[u$8q;`3"
+"0`Csq`0q`1,k=^jsq',v=^Tk),x,c=0;^Fq`A;^Fu`A;^Fq[q]`e;`gv,'&`Hsqp',0`Ipt(^B,`H$4v`e;`qx$R^Fu`X)^Fq[^Fu[x]]#D^Fq[^Fu[x]]?`H`Yx;`qx$R^Fq`X^3sqq[x]^gx==q||c<2)){v#Dv$6'`Y^Fq[x]+'`ax);c++}`3^Uk,v,0)`Cwd"
+"l`7'e`H`Br=@u,b=^k(`G,\"o^M\"),i,o,oc`5b)r=`u#H`qi=0;i<s.d.`Qs`9^2o=s.d.`Qs[i];oc=o.`r?\"\"+o.`r:\"\"`5(oc`4$f<0||oc`4\"@Ooc(\")>=0)$qc`4#7<0)^k(o,\"`r\",0,s.lc);}`3r^N`Gs`0`1`5#3^g!^l#ks.^q||`V5)`"
+"Ds.b^3$h^e)s.$h^e('`r#d);`6s.b^3b.add^e$k)s.b.add^e$k('click#d,false);`f^k(`G,'o^M',0,`Gl)}`Cvs`0x`1,v=s.`Z^b,g=s.`Z^b#fk=^jvsn_'+^B+(g?$Dg:#s,n=^Tk),e`k,y=e@Z$l);e.@S$ly+10+(y<$Q?$Q:0))`5v){v*=#2`"
+"5!n`D!^Uk,x,e))`30;n=x`mn%#200>v)`30}`31`Cdyasmf`0t,m`Dt#q&&m`4t)>=0)`31;`30`Cdyasf`0t,m`1,i=t?t`4$7:-1,n,x`5i>=0#q){`Nn=t`20,i),x=t`2i+1)`5`gx,`H,'dyasm@dm))`3n}`30`Cuns`0`1,x=s.`OSele^a,l=s.`OLis"
+"t,m=s.`OM#S,n,i;^B=^B`8`5x&&l`D!m)m=`G`L^A`5!m.toLowerCase)m`e+m;l=l`8;m=m`8;n=`gl,';`Hdyas@dm)`5n)^B=n}i=^B`4`H`Ifun=i<0?^B:^B`20,i)`Csa`0un`1;^B#O`5!@A)@A#O;`6(`H+@A+`H)`4`H+un+`H)<0)@A+=`H+un;^B"
+"s()`Cp_e`0i,c`1,p`5!^J)^J`A`5!^J[i]@e^J[i]`A;p^Hl=`G`Sl;p^Hn=`G`Sn;p^Hl[p^H$8p;`G`Sn#ip.i=i;p.s=s;p.si=s.p_si;p.sh=s.p_sh;p.cr#cr;p.cw#cw}p=^J[i]`5!p.e@5#lp.e=1`5!@o)@o`e;@o#D@o?`H`Yi}`3p`Cp`0i,l`1"
+",p=s.p_e(i,1),n`5l)`qn=0;n<l`9$xp[l[n].$8l[n].f`Cp_m`0n,a,c`1,m`A;m.n=n`5!#lc=a;a='\"p\",#x\"o\",\"e\"'}`fa='@M`ia,\",$A\",\\\"\"#r';eval('m.f`7'+a+',@M`i@l(`i@l(c,\"\\\\\",\"\\\\\\\\\"$B\"$A\\\\\""
+"\"$Br$A\\r\"$Bn$A\\n\"#r)^N`3m`Cp_si`0u){`Np=`u,s=p.s,n,i;n=^jp_i_'+p.i`5!p.u@5s.ss^d$K^y=\"^x\" @cu?'#A@Mu+'\" '`Y'@v=1 w@0$10$j^N`6u^gs.ios||s.ss)$v`G[n]?`G[n]:$2[n]`5!i)i=`G[@K@7=u}p.u=1`Cp_sh`0"
+"h){`Np=`u,s=p.s`5!p.h&&h^dh);p.h=1`Cp_cr`0k){`3`u.^Tk)`Cp_cw`0k,v,e){`3`u.^Uk,v,e)`Cp_r`0`1,p,n`5^J)`qn$R^J@e^J[n]`5p&&p.e`Dp.@Sup@5p.c)p.@Sup(p,s)`5p.r$Sp.run(p,s)`5!p.c)p.c=0;p.c++}}`Cm_i`0n,a`1,"
+"m,f=n`20,1),r,l,i`5!`Rl)`Rl`A`5!`Rnl)`Rnl`T;m=`Rl[n]`5!a#q&&m._e@5m^H)`Ra(n)`5!m#n`A,m._c=^jm';m^Hn=`G`Sn;m^Hl=s^Hl;m^Hl[m^H$8m;`G`Sn#im.s=s;m._n=n;m._l`T('_c`H_in`H_il`H_i`H_e`H_d`H_dl`Hs`Hn`H_r`H"
+"_g`H_g1`H_t`H_t1`H_x`H_x1`H_rs`H_rr`H_l'`Im_l[$8m;`Rnl[`Rnl`9]=n}`6m._r@5m._m){r=m._r;r._m=m;l=m._l;`qi=0;i<l`9;i++)$Gm[l[i]])r[l[i]]=m[l[i]];r^Hl[r^H$8r;m=`Rl[$8r`mf==f`F())s[$8m;`3m`Cm_a`7'n`Hg`H"
+"e`H$G!g)g@Y;`Bc=s[#5,m,x,f=0`5!c)c=`G[\"s_\"+#5`5c&&s_d)s[g]`7#xs_ft(s_d(c)));x=s[g]`5!x)x=`G[\\'s_\\'+g]`5!x)x=`G[g];m=`Ri(n,1)`5x^g!m^H||g!@Y)#n^H=f=1`5(\"\"+x)`4\"fun^a\")>=0)x(s);`f`Rm(\"x\",n,"
+"x,e)}m=`Ri(n,1)`5$0l)$0l=$0=0;`wt();`3f'`Im_m`0t,n,d,e$C$Dt;`Ns=`u,i,x,m,f=$Dt,r=0,u`5`Rl&&`Rnl)`qi=0;i<`Rnl`9^2x=`Rnl[i]`5!n||x==n#n=`Ri(x);u=m[t]`5u`D@bu)`4'fun^a@10`Dd&&e)@gd,e);`6d)@gd);`f@g)}`"
+"mu)r=1;u=m[t+1]`5u@5m[f]`D@bu)`4'fun^a@10`Dd&&e)@3d,e);`6d)@3d);`f@3)}}m[f]=1`5u)r=1}}`3r`Cm_ll`0`1,g=`Rdl,i,o`5g)`qi=0;i<g`9^2o=g[i]`5o)s.@w(o.n,o.u,o.d,o.l,o.e,$Lg#u0}`C@w`0n,u,d,l,e,ln`1,m=0,i,g"
+",o=0#b,c=s.h?s.h:s.b,b,^o`5n$vn`4':')`5i>=0){g=n`2i+$Ln=n`20,i)}`fg@Y;m=`Ri(n)`m(l||(n@5`Ra(n,g)))&&u^3d&&c^3$m`W`Dd){$0=1;$0l=1`mln`D@D)u=`iu,'@V:`H@Vs:^Ni=^js:'+s^H$s:^x:'+g;b='`Bo=s.d@Z`WById(@M"
+"i+'\")`5s$q`D!o.l&&`G.'+g+'){o.l=1`5o.#B`yo.#oo.i=0;`Ra(\"^x\",@Mg+'@M(e?',@Me+'\"'`Y')}';f2=b+'o.c++`5!`d)`d=250`5!o.l$q.c<(`d*2)/#2)o.i=@S`yo.f2@8}';f1`7'e',b+'}^N^o`7's`Hc`Hi`Hu`Hf1`Hf2`H`Ne,o=0"
+"@jo=s.$m`W(\"script\")`5o){@E=\"text/`v\";@cn?'o.id=i;o.defer=@u;o.o^M=o.onreadystatechange=f1;o.f2=f2;o.l=0;'`Y'o@7=u;c.appendChild(o);@cn?'o.c=0;o.i=@S`yf2@8'`Y'}`no=0}`3o^No=^o(s,c,i,u#b)^Ro`A;o"
+".n=$s:'+g;o.u=u;o.d=d;o.l=l;o.e=e;g=`Rdl`5!g)g=`Rdl`T;i=0;^1i<g`9&&g[i])i#ig#uo}}`6n#n=`Ri(n);m._e=1}`3m`Cvo1`0t,a`Da[t]||$y)`u#ta[t]`Cvo2`0t,a`D#z{a#t`u[t]`5#z$y=1}`Cdlt`7'`Bd`k,i,vo,f=0`5`wl)`qi="
+"0;i<`wl`9^2vo=`wl[i]`5vo`D!`Rm(\"d\")||d`c-$g>=`d){`wl#u0;s.t($H}`ff=1}`m`w#B`y`wi`Idli=0`5f`D!`wi)`wi=@S`y`wt,`d)}`f`wl=0'`Idl`0vo`1,d`k`5!$Hvo`A;`g^E,`H$b2',$H;$g=d`c`5!`wl)`wl`T;`wl[`wl`9]=vo`5!"
+"`d)`d=250;`wt()`Ct`0vo,id`1,trk=1,tm`k,sed=Math&&@k#L?@k#R@k#L()*#200000000000):tm`c,$N='s'+@k#Rtm`c/10800000)%10+sed,y=tm@Z$l),vt=tm@ZDate($r`zMonth($r@cy<$Q?y+$Q:y)+' `zHour$t:`zMinute$t:`zSecond"
+"$t `zDay()+' `zTimezoneOff@S(),^o,^h=s.g^h(),ta=-1,q`e,qs`e,#M`e,vb`A#a^E`Iuns(`Im_ll()`5!s.td){`Ntl=^h`L,a,o,i,x`e,c`e,v`e,p`e,bw`e,bh`e,^O0',k=^U^jcc`H@u',0@2,hp`e,ct`e,pn=0,ps`5^9&&^9.prototype)"
+"{^O1'`5j.m#S){^O2'`5tm.@SUTCDate){^O3'`5^l^3^q&&`V5)^O4'`5pn@P{^O5';a`T`5a.forEach){^O6';i=0;o`A;^o`7'o`H`Ne,i=0@ji=new Iterator(o)`n}`3i^Ni=^o(o)`5i&&i.next)^O7'#G}`m`V4)x=^tw@0+'x'+^t@v`5s.isns||"
+"s.^p`D`V3#0`o(@2`5`V4){c=^tpixelDepth;bw=`G@GW@0;bh=`G@G^n}}$c=s.n.p^W}`6^l`D`V4#0`o(@2;c=^t^8`5`V5){bw=s.d.^Z`W.off@SW@0;bh=s.d.^Z`W.off@S^n`5!s.^q^3b){^o`7's`Htl`H`Ne,hp=0`xh#C\");hp=s.b.isH#C(tl"
+")?\"Y\":\"N\"`n}`3hp^Nhp=^o(s,tl);^o`7's`H`Ne,ct=0`xclientCaps\");ct=s.b.`p`n}`3ct^Nct=^o(s)#G`fr`e`m$c)^1pn<$c`9&&pn<30){ps=^I$c[pn].^y@8$p`5p`4ps)<0)p+=ps;pn++}s.^i=x;s.^8=c;s.`v^v=j;s.`o=v;s.^0@"
+"L=k;s.^6W@0=bw;s.^6^n=bh;s.`p=ct;s.@6=hp;s.p^W=p;s.td=1`m$H{`g^E,`H$b2',vb`Ipt(^E,`H$b1',$H`m(vo&&$g)#k`Rm('d')`Ds.useP^W)s.doP^W(s);`Nl=`G`L,r=^h.^Z.`b`5!s.^Q)s.^Q=l^s?l^s:l`5!s.`b@5s._1_`b@N`b=r;"
+"s._1_`b=1}`Rm('g')`5@B||^C){`No=^C?^C:@B`5!o)`3'';`Np=s.#e`s,w=1,^K,$3,x=^5t,h,l,i,oc`5^C$q==^C){^1o@5n#F$oBODY'){o=o^7`W?o^7`W:o^7Node`5!o)`3'';^K;$3;x=^5t}oc=o.`r?''+o.`r:''`5(oc`4$f>=0$qc`4\"@Oo"
+"c(\")<0)||oc`4#7>=0)`3''`mn)ta=o$Uh$Ji=h`4'?^Nh=s.`Q$5^9||i<0?h:h`20,#ol=s.`Q`s;t=s.`Q^S?s.`Q^S`8:s.lt(h)`5t^gh||l))q+='&pe=lnk_@ct`Pd$9`Pe'?@m(t):'o')+(h$6pev1`ah)`Y(l$6pev2`al):'^N`ftrk=0`5s.^L@q"
+"`D!p@es.^Q;w=0}^K;i=o.sourceIndex`5@J')$u@J^Nx=1;i=1`mp&&n#F)qs='&pid`a^Ip,255))+(w$6pidt#Ew`Y'&oid`a^In@8)+(x$6oidt#Ex`Y'&ot`at)+(i$6oi#Ei:#s}`m!trk@5qs)`3'';$I=s.vs(sed)`5trk`D$I)#M=#W($N,(vt$6t`"
+"avt)`Ys.hav()+q+(qs?qs:s.rq()),0#Z);qs`e;`Rm('t')`5s.p_r)s.p_r(`I`b`e}^F(qs);^R`w($H;`m$H`g^E,`H$b1',vb`I$T^C=s.`Q`s=s.`Q^S=`G`l`e`5s.pg)`G@O$T`G@Oeo=`G@O`Q`s=`G@O`Q^S`e`5!id@5s.tc@Ntc=1;s.flush`U("
+")}`3#M`Ctl`0o,t,n,vo`1;@B=$Vo`I`Q^S=t;s.`Q`s=n;s.t($H}`5pg){`G@Oco`0o){`N^u\"_\",1,$L`3$Vo)`Cwd@Ogs`0$S{`N^uun,1,$L`3s.t()`Cwd@Odc`0$S{`N^uun,$L`3s.t()}}@D=(`G`L`j`8`4'@Vs@10`Id=^Z;s.b=s.d.body`5s."
+"d@Z`W#h`s@Nh=s.d@Z`W#h`s('HEAD')`5s.h)s.h=s.h[0]}s.n=navigator;s.u=s.n.userAgent;@p=s.u`4'N#86/^N`Napn$n`s,v$n^v,ie=v`4#Q'),o=s.u`4'@i '),i`5v`4'@i@10||o>0)apn='@i';^l$d`PMicrosoft Internet Explore"
+"r'`Iisns$d`PN#8'`I^p$d`P@i'`I^q=(s.u`4'Mac@10)`5o>0)`hs.u`2o+6));`6ie>0@Napv=^Gi=v`2ie+5))`5#3)`hi)}`6@p>0)`hs.u`2@p+10));`f`hv`Iem=0`5s.em@P@93;`6^9#g^w$v^r^9#g^w(256))`F(`Iem=(i`P%C4%80'?2:(i`P%U"
+"0#2'?1:0))}s.sa(un`Ivl_l='^Y,`ZID,vmk,`Z@a,`E,`E^m,ppu,@T,`Z`s$e,c`K,^0@I,#e`s,^Q,`b,@W';s.va_l@H^V,`H`Ivl_t=^V+',^z,#1,server,#e^S,#T^aID,purchaseID,$O,state,zip,#K,products,`Q`s,`Q^S';`q`Nn=1;n<7"
+"6$x@F+=',prop^x,eVar^x,hier^x,list$E^V2=',tnt,pe#P1#P2#P3,^i,^8,`v^v,`o,^0@L,^6W@0,^6^n,`p,@6,p^W';@F+=^V2;@z@H@F,`H`Ivl_g=@F+',`M,`M^m,`MBase,fpC`K,@X`U,#J,`Z^b,`Z^b#f`OSele^a,`OList,`OM#S,^LDow^M"
+"Links,^L@R,^L@q,`Q$5^9,`QDow^MFile^Ss,`QEx`t,`QIn`t,`Q@s#X,`Q@s^es,`Q`ss,lnk,eo,_1_`b';@y@H^E,`H`Ipg=pg#a^E)`5!ss)`Gs()",
w=window,l=w.s_c_il,n=navigator,u=n.userAgent,v=n.appVersion,e=v.indexOf('MSIE '),m=u.indexOf('Netscape6/'),a,i,s;if(un){un=un.toLowerCase();if(l)for(i=0;i<l.length;i++){s=l[i];if(!s._c||s._c=='s_c'){if(s.oun==un)return s;else if(s.fs&&s.sa&&s.fs(s.oun,un)){s.sa(un);return s;}}}}w.s_an='0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
w.s_sp=new Function("x","d","var a=new Array,i=0,j;if(x){if(x.split)a=x.split(d);else if(!d)for(i=0;i<x.length;i++)a[a.length]=x.substring(i,i+1);else while(i>=0){j=x.indexOf(d,i);a[a.length]=x.subst"
+"ring(i,j<0?x.length:j);i=j;if(i>=0)i+=d.length}}return a");
w.s_jn=new Function("a","d","var x='',i,j=a.length;if(a&&j>0){x=a[0];if(j>1){if(a.join)x=a.join(d);else for(i=1;i<j;i++)x+=d+a[i]}}return x");
w.s_rep=new Function("x","o","n","return s_jn(s_sp(x,o),n)");
w.s_d=new Function("x","var t='`^@$#',l=s_an,l2=new Object,x2,d,b=0,k,i=x.lastIndexOf('~~'),j,v,w;if(i>0){d=x.substring(0,i);x=x.substring(i+2);l=s_sp(l,'');for(i=0;i<62;i++)l2[l[i]]=i;t=s_sp(t,'');d"
+"=s_sp(d,'~');i=0;while(i<5){v=0;if(x.indexOf(t[i])>=0) {x2=s_sp(x,t[i]);for(j=1;j<x2.length;j++){k=x2[j].substring(0,1);w=t[i]+k;if(k!=' '){v=1;w=d[b+l2[k]]}x2[j]=w+x2[j].substring(1)}}if(v)x=s_jn("
+"x2,'');else{w=t[i]+' ';if(x.indexOf(w)>=0)x=s_rep(x,w,t[i]);i++;b+=62}}}return x");
w.s_fe=new Function("c","return s_rep(s_rep(s_rep(c,'\\\\','\\\\\\\\'),'\"','\\\\\"'),\"\\n\",\"\\\\n\")");
w.s_fa=new Function("f","var s=f.indexOf('(')+1,e=f.indexOf(')'),a='',c;while(s>=0&&s<e){c=f.substring(s,s+1);if(c==',')a+='\",\"';else if((\"\\n\\r\\t \").indexOf(c)<0)a+=c;s++}return a?'\"'+a+'\"':"
+"a");
w.s_ft=new Function("c","c+='';var s,e,o,a,d,q,f,h,x;s=c.indexOf('=function(');while(s>=0){s++;d=1;q='';x=0;f=c.substring(s);a=s_fa(f);e=o=c.indexOf('{',s);e++;while(d>0){h=c.substring(e,e+1);if(q){i"
+"f(h==q&&!x)q='';if(h=='\\\\')x=x?0:1;else x=0}else{if(h=='\"'||h==\"'\")q=h;if(h=='{')d++;if(h=='}')d--}if(d>0)e++}c=c.substring(0,s)+'new Function('+(a?a+',':'')+'\"'+s_fe(c.substring(o+1,e))+'\")"
+"'+c.substring(e+1);s=c.indexOf('=function(')}return c;");
c=s_d(c);if(e>0){a=parseInt(i=v.substring(e+5));if(a>3)a=parseFloat(i)}else if(m>0)a=parseFloat(u.substring(m+10));else a=parseFloat(v);if(a>=5&&v.indexOf('Opera')<0&&u.indexOf('Opera')<0){w.s_c=new Function("un","pg","ss","var s=this;"+c);return new s_c(un,pg,ss)}else s=new Function("un","pg","ss","var s=new Object;"+s_ft(c)+";return s");return s(un,pg,ss);}
(function (window){
	/**
	 * Copyright (c) 2013, Visual IQ Inc. All Rights Reserved.  
	 * Version 2.1.5 
	 */

	 /*
	 	QP is Query Parameter.
	 */
	var CONFIG = {
	 SEARCH_ENGINES : {
		GOOGLE : {
			NAME : "Google",
			QP : "q",
			HOST : "google"
		},

		YAHOO : {
			NAME : "Yahoo",
			QP : "p",
			HOST : "yahoo"
		},

		BING : {
			NAME : "Bing",
			QP : "q",
			HOST : "bing"
		},

		ASK : {
			NAME : "Ask",
			QP : "q",
			HOST : "ask"
		}
	 }
	};
	var _result;

	function trackRequest(referrerURL, currentURL, cmpId, additionalParams) {
		_result = checkReferrer(referrerURL, currentURL, cmpId);
		if( (_result.modeOfAccess != "DIRECT") && (additionalParams != null) ) {
			var referrerURLParams = parseUri(referrerURL);
	      	var referrerHost=referrerURLParams.host;
	      	_result=checkCustomAccessModes(referrerHost,additionalParams,_result);
		}
		_result.randomId = Math.floor(Math.random() * 100001);
		_result.timestamp = new Date().getTime(); 
		_result.Utils=Utils;
		return _result;
	}


	function checkCustomAccessModes(referrerHost,additionalParams, result){
		result= checkCustomModeByRefferer(referrerHost,additionalParams, result);
		result= checkCustomModeByCurrentPageParams(referrerHost,additionalParams, result);
		return result;
	}

	
	function checkCustomModeByCurrentPageParams(referrerHost,additionalParams, result){
		var custommodes=additionalParams["CUSTOM_ACCESS_MODES_BY_CURRENT_PAGE_PARAMS"];
		var found=false;
		var cHost;
		for (var mode in custommodes){
			var custommode=custommodes[mode];
			for (i=0 ; i< custommode.length ; i++){
				if(result.current_page_params.queryKey.hasOwnProperty(custommode[i])){
					result.source = referrerHost;
					result.modeOfAccess = mode;
				    found=true;
				    break;
				}
			}
			if(found){
				break;
			}
		}
		return result;
	}
	

	function checkCustomModeByRefferer(referrerHost,additionalParams, result){
		var custommodes=additionalParams["CUSTOM_ACCESS_MODES_BY_REFERRER"];
		var found=false;
		var cHost;
		for (var mode in custommodes){
			
			var custommode=custommodes[mode];
			for (i=0 ; i< custommode.length ; i++){
				cHost = custommode[i];
				if (referrerHost.indexOf(cHost.toLowerCase()) != -1) {
				    result.source = cHost;
				    result.modeOfAccess = mode;
				    found=true;
				    break;
				}
			}
			if(found){
				break;
			}
		}
	    return result;
	}


	function checkReferrer(referrerURL, currentURL, cmpId) {
		var result = new Object();
		var currentURLParams = parseUri(currentURL);

		// Initialize
		result.current_page_params=currentURLParams;
		result.modeOfAccess = "UNKNOWN";
		result.source = "NONE";
		result.isSearchEngine = false;

		if (!referrerURL || (referrerURL== null) || (referrerURL=="") )  {
			result.isRequestTracked = false;
			result.isSearchEngine = false;
			result.modeOfAccess = "DIRECT";
		} else {
			//result.isRequestTracked = "UNKNOWN";
			
			var searchEngines = CONFIG.SEARCH_ENGINES ;
			var referrerURLParams = parseUri(referrerURL);
			result.referrer_page_params=referrerURLParams;
			var referrerHost=referrerURLParams.host;
			for (var i in searchEngines){			
				var searchEngine = searchEngines[i];
				if (referrerHost.indexOf(searchEngine.HOST) != -1) {
					result.source = searchEngine.NAME;
					result.isSearchEngine = true;
					result.modeOfAccess = "ORGANIC";
					result.query = referrerURLParams['queryKey'][searchEngine.QP];
					result.isRequestTracked = validateCampaignIds(currentURLParams.queryKey,cmpId,referrerHost);
					if(result.isRequestTracked){
						result.modeOfAccess = "PAID";
					}
					break;
				}	
			}	
			
			if (!result.isSearchEngine) {
				result.source = encodeURI(referrerURL);
				result.isSearchEngine = false;
				result.isRequestTracked = validateCampaignIds(currentURLParams.queryKey,cmpId,referrerHost);
				
				if(result.isRequestTracked){
					result.modeOfAccess = "DISPLAY";
				} else {
					result.modeOfAccess = "UNKNOWN";				
				}
			}		
		}
			
		return result;
	}

	function validateCampaignIds(targetObject,domainIds,referrerHost){
		/*
			domainIds-> {'*':["gclid","cmp_id"],'https://www.libertymutual.com/jawrJsPath/gzip_N2121979551/bundles/t.co':["tw1","tw2"]}
			* applies to all domains
		*/
        var campainIdExistance=false;
        for(var domainval in domainIds){
          var cmpIds=domainIds[domainval];
          for(var cmpId in cmpIds){
            if( hasCampaignID(targetObject,cmpIds[cmpId]) && ( domainval=="*" || referrerHost.indexOf(domainval.toLowerCase()) != -1 ) ){
                return true;
            };
          }
        }
		return campainIdExistance;
    }

	function hasCampaignID(targetObject,cmpId){
		if(cmpId.indexOf('*')!=-1){
			return hasWildCardMatch(targetObject,cmpId)
		}
		return (cmpId != null) && (targetObject.hasOwnProperty(cmpId));
	}

	function hasWildCardMatch(targetObject,cmpId){
		var matches=false;
		cmpId=cmpId.replace(/\*/g,""); 
		var patternstr="^"+cmpId;
		var pattern=new RegExp(patternstr,"g");
		for(var key in targetObject){
			if(pattern.test(key)){
				matches=true;
				break;
			}
		}
		return matches;
	}

	// parseUri 1.2.2
	// (c) Steven Levithan <stevenlevithan.com>
	// MIT License

	function parseUri (str) {
		var	o   = parseUri.options,
			m   = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
			uri = {},
			i   = 14;

		while (i--) uri[o.key[i]] = m[i] || "";

		uri[o.q.name] = {};
		uri[o.key[12]].replace(o.q.parser, function ($0, $1, $2) {
			if ($1) uri[o.q.name][$1] = $2;
		});

		return uri;
	};

	parseUri.options = {
		strictMode: true,
		key: ["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","anchor"],
		q:   {
			name:   "queryKey",
			parser: /(?:^|&)([^&=]*)=?([^&]*)/g
		},
		parser: {
			strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@?]*)(?::([^:@?]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
			loose:  /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@?]*)(?::([^:@?]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
		}
	};

	function IQSeoTag(referrerURL, currentURL, cmpId, additionalParams){
		return trackRequest(referrerURL, currentURL, cmpId, additionalParams)
	}

	function valueMatches(list,val){
	    for (var li = 0, listlen = list.length; li < listlen; li++) {
			if(list[li].substring){
	            if(list[li].toLowerCase()==val){
	                return true;


					}
	        }
		}
	    return false;
	}

	var Utils={
		valueMatches:valueMatches,
		getCurrentPageParamsKey:function (queryKeyName){
			queryKeyName=queryKeyName.toLowerCase();
			if(_result.current_page_params.queryKey[queryKeyName])
			{
				return _result.current_page_params.queryKey[queryKeyName];
			}
			else
			{				
			if(_result.current_page_params.source.toLowerCase().indexOf(queryKeyName+"=") > -1){
			var queryKeyValue=_result.current_page_params.source.toLowerCase().split(queryKeyName+"=");
			queryKeyValue=queryKeyValue[1].split("&");
			_result.current_page_params.queryKey[queryKeyName]=queryKeyValue[0];
			return _result.current_page_params.queryKey[queryKeyName];
			}
			else
			{
			return;
			}
			}	
		},
		getReferrerPageParamsKey:function (queryKeyName){
				queryKeyName=queryKeyName.toLowerCase();
			if(_result.referrer_page_params.queryKey[queryKeyName])
			{
				return _result.referrer_page_params.queryKey[queryKeyName];
			}
			else
			{				
			if(_result.referrer_page_params.source.toLowerCase().indexOf(queryKeyName+"=") > -1){
			var queryKeyValue=_result.referrer_page_params.source.toLowerCase().split(queryKeyName+"=");
			queryKeyValue=queryKeyValue[1].split("&");
			_result.referrer_page_params.queryKey[queryKeyName]=queryKeyValue[0];
			return _result.referrer_page_params.queryKey[queryKeyName];
			}
			else
			{
			return;
			}
			}	
		},
		getReferrerPageParams:function(){
			if(!_result.referrer_page_params.query == "")
			{
			return _result.referrer_page_params.query;
			}
			else
			{
			if(_result.referrer_page_params.path.indexOf("&") > -1 || _result.referrer_page_params.path.indexOf("=") > -1){
			var dir_temp=_result.referrer_page_params.path.split("/");
				_result.referrer_page_params.query="";
				var slash_temp="";
				for(var il=0;il<dir_temp.length;il++){
				if(dir_temp[il].indexOf("=")>-1 || slash_temp =="/"){
				_result.referrer_page_params.query=_result.referrer_page_params.query+slash_temp+dir_temp[il];
				slash_temp="/";
				}
				}
				return _result.referrer_page_params.query;	
			}
			}
		},
		getCurrentPageParams:function (){
			if(!_result.current_page_params.query == "")
			{
			return _result.current_page_params.query;
			}
			else
			{
			if(_result.current_page_params.path.indexOf("&") > -1 || _result.current_page_params.path.indexOf("=") > -1){
			var dir_temp=_result.current_page_params.path.split("/");
				_result.current_page_params.query="";
				var slash_temp="";
				for(var il=0;il<dir_temp.length;il++){
				if(dir_temp[il].indexOf("=")>-1 || slash_temp =="/"){
				_result.current_page_params.query=_result.current_page_params.query+slash_temp+dir_temp[il];
				slash_temp="/";
				}
				}
				return _result.current_page_params.query;	
			}
			}
		},
		tolowercaseCurrentPageParamsKeys:function(){
		
		if(_result.current_page_params && _result.current_page_params.queryKey){
		
	
			for(key in _result.current_page_params.queryKey){				
				if(key.toLowerCase() != key){
					_result.current_page_params.queryKey[key.toLowerCase()] = _result.current_page_params.queryKey[key];
					delete _result.current_page_params.queryKey[key];
				}
			}
		}
		},
		getCookieVIQ:function(c_name){
			var c_value = document.cookie;
			var c_start = c_value.indexOf(c_name + "=");
			if (c_start == -1){
				c_start = c_value.indexOf(c_name + "=");
			}
			if (c_start == -1){
				c_value = null;
			}
			else{
				c_start = c_value.indexOf("=", c_start) + 1;
				var c_end = c_value.indexOf(";", c_start);
				if (c_end == -1){
					c_end = c_value.length;
				}
				c_value = decodeURIComponent(c_value.substring(c_start,c_end));
			}
			return c_value;
		},
		setCookieVIQ:function(c_name,value){
			var exdate=new Date();
			var c_value=encodeURIComponent(value);
			document.cookie=c_name + "=" + c_value;
			return;
		},
		checkCookieVIQ:function(c_name){
			var domainname=_result.Utils.getCookieVIQ(c_name);
			if (domainname!=null && domainname!=""){
				return domainname;
			}
			else {
				return "false";
			}
		}
	}	
	window.IQSeoTag=IQSeoTag;
}(window));
