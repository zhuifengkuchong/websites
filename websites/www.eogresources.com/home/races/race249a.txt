<script id = "race249a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race249={};
	myVars.races.race249.varName="Object[1641].currentMenuItem";
	myVars.races.race249.varType="@varType@";
	myVars.races.race249.repairType = "@RepairType";
	myVars.races.race249.event1={};
	myVars.races.race249.event2={};
	myVars.races.race249.event1.id = "Lu_Id_a_62";
	myVars.races.race249.event1.type = "onfocus";
	myVars.races.race249.event1.loc = "Lu_Id_a_62_LOC";
	myVars.races.race249.event1.isRead = "True";
	myVars.races.race249.event1.eventType = "@event1EventType@";
	myVars.races.race249.event2.id = "Lu_Id_a_5";
	myVars.races.race249.event2.type = "onmouseover";
	myVars.races.race249.event2.loc = "Lu_Id_a_5_LOC";
	myVars.races.race249.event2.isRead = "False";
	myVars.races.race249.event2.eventType = "@event2EventType@";
	myVars.races.race249.event1.executed= false;// true to disable, false to enable
	myVars.races.race249.event2.executed= false;// true to disable, false to enable
</script>

