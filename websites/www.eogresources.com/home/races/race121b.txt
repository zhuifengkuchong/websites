<script id = "race121b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race121={};
	myVars.races.race121.varName="Lu_Id_a_5__onmouseover";
	myVars.races.race121.varType="@varType@";
	myVars.races.race121.repairType = "@RepairType";
	myVars.races.race121.event1={};
	myVars.races.race121.event2={};
	myVars.races.race121.event1.id = "Lu_Id_a_5";
	myVars.races.race121.event1.type = "onmouseover";
	myVars.races.race121.event1.loc = "Lu_Id_a_5_LOC";
	myVars.races.race121.event1.isRead = "True";
	myVars.races.race121.event1.eventType = "@event1EventType@";
	myVars.races.race121.event2.id = "Lu_Id_script_10";
	myVars.races.race121.event2.type = "Lu_Id_script_10__parsed";
	myVars.races.race121.event2.loc = "Lu_Id_script_10_LOC";
	myVars.races.race121.event2.isRead = "False";
	myVars.races.race121.event2.eventType = "@event2EventType@";
	myVars.races.race121.event1.executed= false;// true to disable, false to enable
	myVars.races.race121.event2.executed= false;// true to disable, false to enable
</script>

