<script id = "race128a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race128={};
	myVars.races.race128.varName="Object[1641].currentMenuItem";
	myVars.races.race128.varType="@varType@";
	myVars.races.race128.repairType = "@RepairType";
	myVars.races.race128.event1={};
	myVars.races.race128.event2={};
	myVars.races.race128.event1.id = "Lu_Id_a_61";
	myVars.races.race128.event1.type = "onkeydown";
	myVars.races.race128.event1.loc = "Lu_Id_a_61_LOC";
	myVars.races.race128.event1.isRead = "True";
	myVars.races.race128.event1.eventType = "@event1EventType@";
	myVars.races.race128.event2.id = "Lu_Id_a_4";
	myVars.races.race128.event2.type = "onfocus";
	myVars.races.race128.event2.loc = "Lu_Id_a_4_LOC";
	myVars.races.race128.event2.isRead = "False";
	myVars.races.race128.event2.eventType = "@event2EventType@";
	myVars.races.race128.event1.executed= false;// true to disable, false to enable
	myVars.races.race128.event2.executed= false;// true to disable, false to enable
</script>

