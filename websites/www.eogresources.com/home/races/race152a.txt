<script id = "race152a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race152={};
	myVars.races.race152.varName="Object[1641].currentMenuItem";
	myVars.races.race152.varType="@varType@";
	myVars.races.race152.repairType = "@RepairType";
	myVars.races.race152.event1={};
	myVars.races.race152.event2={};
	myVars.races.race152.event1.id = "Lu_Id_a_37";
	myVars.races.race152.event1.type = "onkeydown";
	myVars.races.race152.event1.loc = "Lu_Id_a_37_LOC";
	myVars.races.race152.event1.isRead = "True";
	myVars.races.race152.event1.eventType = "@event1EventType@";
	myVars.races.race152.event2.id = "Lu_Id_a_4";
	myVars.races.race152.event2.type = "onfocus";
	myVars.races.race152.event2.loc = "Lu_Id_a_4_LOC";
	myVars.races.race152.event2.isRead = "False";
	myVars.races.race152.event2.eventType = "@event2EventType@";
	myVars.races.race152.event1.executed= false;// true to disable, false to enable
	myVars.races.race152.event2.executed= false;// true to disable, false to enable
</script>

