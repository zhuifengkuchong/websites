<script id = "race118b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race118={};
	myVars.races.race118.varName="Lu_Id_a_62__onfocus";
	myVars.races.race118.varType="@varType@";
	myVars.races.race118.repairType = "@RepairType";
	myVars.races.race118.event1={};
	myVars.races.race118.event2={};
	myVars.races.race118.event1.id = "Lu_Id_a_62";
	myVars.races.race118.event1.type = "onfocus";
	myVars.races.race118.event1.loc = "Lu_Id_a_62_LOC";
	myVars.races.race118.event1.isRead = "True";
	myVars.races.race118.event1.eventType = "@event1EventType@";
	myVars.races.race118.event2.id = "Lu_Id_script_10";
	myVars.races.race118.event2.type = "Lu_Id_script_10__parsed";
	myVars.races.race118.event2.loc = "Lu_Id_script_10_LOC";
	myVars.races.race118.event2.isRead = "False";
	myVars.races.race118.event2.eventType = "@event2EventType@";
	myVars.races.race118.event1.executed= false;// true to disable, false to enable
	myVars.races.race118.event2.executed= false;// true to disable, false to enable
</script>

