<script id = "race301b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race301={};
	myVars.races.race301.varName="Object[1641].currentMenuItem";
	myVars.races.race301.varType="@varType@";
	myVars.races.race301.repairType = "@RepairType";
	myVars.races.race301.event1={};
	myVars.races.race301.event2={};
	myVars.races.race301.event1.id = "Lu_Id_a_5";
	myVars.races.race301.event1.type = "onmouseover";
	myVars.races.race301.event1.loc = "Lu_Id_a_5_LOC";
	myVars.races.race301.event1.isRead = "False";
	myVars.races.race301.event1.eventType = "@event1EventType@";
	myVars.races.race301.event2.id = "Lu_Id_a_10";
	myVars.races.race301.event2.type = "onfocus";
	myVars.races.race301.event2.loc = "Lu_Id_a_10_LOC";
	myVars.races.race301.event2.isRead = "True";
	myVars.races.race301.event2.eventType = "@event2EventType@";
	myVars.races.race301.event1.executed= false;// true to disable, false to enable
	myVars.races.race301.event2.executed= false;// true to disable, false to enable
</script>

