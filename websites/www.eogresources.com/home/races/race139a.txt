<script id = "race139a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race139={};
	myVars.races.race139.varName="Object[1641].currentMenuItem";
	myVars.races.race139.varType="@varType@";
	myVars.races.race139.repairType = "@RepairType";
	myVars.races.race139.event1={};
	myVars.races.race139.event2={};
	myVars.races.race139.event1.id = "Lu_Id_a_50";
	myVars.races.race139.event1.type = "onkeydown";
	myVars.races.race139.event1.loc = "Lu_Id_a_50_LOC";
	myVars.races.race139.event1.isRead = "True";
	myVars.races.race139.event1.eventType = "@event1EventType@";
	myVars.races.race139.event2.id = "Lu_Id_a_4";
	myVars.races.race139.event2.type = "onfocus";
	myVars.races.race139.event2.loc = "Lu_Id_a_4_LOC";
	myVars.races.race139.event2.isRead = "False";
	myVars.races.race139.event2.eventType = "@event2EventType@";
	myVars.races.race139.event1.executed= false;// true to disable, false to enable
	myVars.races.race139.event2.executed= false;// true to disable, false to enable
</script>

