<script id = "race22b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race22={};
	myVars.races.race22.varName="Lu_Id_a_25__onkeydown";
	myVars.races.race22.varType="@varType@";
	myVars.races.race22.repairType = "@RepairType";
	myVars.races.race22.event1={};
	myVars.races.race22.event2={};
	myVars.races.race22.event1.id = "Lu_Id_a_25";
	myVars.races.race22.event1.type = "onkeydown";
	myVars.races.race22.event1.loc = "Lu_Id_a_25_LOC";
	myVars.races.race22.event1.isRead = "True";
	myVars.races.race22.event1.eventType = "@event1EventType@";
	myVars.races.race22.event2.id = "Lu_Id_script_10";
	myVars.races.race22.event2.type = "Lu_Id_script_10__parsed";
	myVars.races.race22.event2.loc = "Lu_Id_script_10_LOC";
	myVars.races.race22.event2.isRead = "False";
	myVars.races.race22.event2.eventType = "@event2EventType@";
	myVars.races.race22.event1.executed= false;// true to disable, false to enable
	myVars.races.race22.event2.executed= false;// true to disable, false to enable
</script>

