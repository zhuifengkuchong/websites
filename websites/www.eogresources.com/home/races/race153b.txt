<script id = "race153b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race153={};
	myVars.races.race153.varName="Object[1641].currentMenuItem";
	myVars.races.race153.varType="@varType@";
	myVars.races.race153.repairType = "@RepairType";
	myVars.races.race153.event1={};
	myVars.races.race153.event2={};
	myVars.races.race153.event1.id = "Lu_Id_a_4";
	myVars.races.race153.event1.type = "onfocus";
	myVars.races.race153.event1.loc = "Lu_Id_a_4_LOC";
	myVars.races.race153.event1.isRead = "False";
	myVars.races.race153.event1.eventType = "@event1EventType@";
	myVars.races.race153.event2.id = "Lu_Id_a_36";
	myVars.races.race153.event2.type = "onkeydown";
	myVars.races.race153.event2.loc = "Lu_Id_a_36_LOC";
	myVars.races.race153.event2.isRead = "True";
	myVars.races.race153.event2.eventType = "@event2EventType@";
	myVars.races.race153.event1.executed= false;// true to disable, false to enable
	myVars.races.race153.event2.executed= false;// true to disable, false to enable
</script>

