<script id = "race196a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race196={};
	myVars.races.race196.varName="Object[1641].currentMenuItem";
	myVars.races.race196.varType="@varType@";
	myVars.races.race196.repairType = "@RepairType";
	myVars.races.race196.event1={};
	myVars.races.race196.event2={};
	myVars.races.race196.event1.id = "Lu_Id_a_4";
	myVars.races.race196.event1.type = "onfocus";
	myVars.races.race196.event1.loc = "Lu_Id_a_4_LOC";
	myVars.races.race196.event1.isRead = "False";
	myVars.races.race196.event1.eventType = "@event1EventType@";
	myVars.races.race196.event2.id = "Lu_Id_a_15";
	myVars.races.race196.event2.type = "onfocus";
	myVars.races.race196.event2.loc = "Lu_Id_a_15_LOC";
	myVars.races.race196.event2.isRead = "True";
	myVars.races.race196.event2.eventType = "@event2EventType@";
	myVars.races.race196.event1.executed= false;// true to disable, false to enable
	myVars.races.race196.event2.executed= false;// true to disable, false to enable
</script>

