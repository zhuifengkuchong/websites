<script id = "race49a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race49={};
	myVars.races.race49.varName="Lu_Id_a_52__onkeydown";
	myVars.races.race49.varType="@varType@";
	myVars.races.race49.repairType = "@RepairType";
	myVars.races.race49.event1={};
	myVars.races.race49.event2={};
	myVars.races.race49.event1.id = "Lu_Id_script_10";
	myVars.races.race49.event1.type = "Lu_Id_script_10__parsed";
	myVars.races.race49.event1.loc = "Lu_Id_script_10_LOC";
	myVars.races.race49.event1.isRead = "False";
	myVars.races.race49.event1.eventType = "@event1EventType@";
	myVars.races.race49.event2.id = "Lu_Id_a_52";
	myVars.races.race49.event2.type = "onkeydown";
	myVars.races.race49.event2.loc = "Lu_Id_a_52_LOC";
	myVars.races.race49.event2.isRead = "True";
	myVars.races.race49.event2.eventType = "@event2EventType@";
	myVars.races.race49.event1.executed= false;// true to disable, false to enable
	myVars.races.race49.event2.executed= false;// true to disable, false to enable
</script>

