<script id = "race165a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race165={};
	myVars.races.race165.varName="Object[1641].currentMenuItem";
	myVars.races.race165.varType="@varType@";
	myVars.races.race165.repairType = "@RepairType";
	myVars.races.race165.event1={};
	myVars.races.race165.event2={};
	myVars.races.race165.event1.id = "Lu_Id_a_24";
	myVars.races.race165.event1.type = "onkeydown";
	myVars.races.race165.event1.loc = "Lu_Id_a_24_LOC";
	myVars.races.race165.event1.isRead = "True";
	myVars.races.race165.event1.eventType = "@event1EventType@";
	myVars.races.race165.event2.id = "Lu_Id_a_4";
	myVars.races.race165.event2.type = "onfocus";
	myVars.races.race165.event2.loc = "Lu_Id_a_4_LOC";
	myVars.races.race165.event2.isRead = "False";
	myVars.races.race165.event2.eventType = "@event2EventType@";
	myVars.races.race165.event1.executed= false;// true to disable, false to enable
	myVars.races.race165.event2.executed= false;// true to disable, false to enable
</script>

