<script id = "race251a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race251={};
	myVars.races.race251.varName="Object[1641].currentMenuItem";
	myVars.races.race251.varType="@varType@";
	myVars.races.race251.repairType = "@RepairType";
	myVars.races.race251.event1={};
	myVars.races.race251.event2={};
	myVars.races.race251.event1.id = "Lu_Id_a_60";
	myVars.races.race251.event1.type = "onfocus";
	myVars.races.race251.event1.loc = "Lu_Id_a_60_LOC";
	myVars.races.race251.event1.isRead = "True";
	myVars.races.race251.event1.eventType = "@event1EventType@";
	myVars.races.race251.event2.id = "Lu_Id_a_5";
	myVars.races.race251.event2.type = "onmouseover";
	myVars.races.race251.event2.loc = "Lu_Id_a_5_LOC";
	myVars.races.race251.event2.isRead = "False";
	myVars.races.race251.event2.eventType = "@event2EventType@";
	myVars.races.race251.event1.executed= false;// true to disable, false to enable
	myVars.races.race251.event2.executed= false;// true to disable, false to enable
</script>

