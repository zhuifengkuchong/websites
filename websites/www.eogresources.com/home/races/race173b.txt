<script id = "race173b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race173={};
	myVars.races.race173.varName="Object[1641].currentMenuItem";
	myVars.races.race173.varType="@varType@";
	myVars.races.race173.repairType = "@RepairType";
	myVars.races.race173.event1={};
	myVars.races.race173.event2={};
	myVars.races.race173.event1.id = "Lu_Id_a_4";
	myVars.races.race173.event1.type = "onfocus";
	myVars.races.race173.event1.loc = "Lu_Id_a_4_LOC";
	myVars.races.race173.event1.isRead = "False";
	myVars.races.race173.event1.eventType = "@event1EventType@";
	myVars.races.race173.event2.id = "Lu_Id_a_16";
	myVars.races.race173.event2.type = "onkeydown";
	myVars.races.race173.event2.loc = "Lu_Id_a_16_LOC";
	myVars.races.race173.event2.isRead = "True";
	myVars.races.race173.event2.eventType = "@event2EventType@";
	myVars.races.race173.event1.executed= false;// true to disable, false to enable
	myVars.races.race173.event2.executed= false;// true to disable, false to enable
</script>

