<script id = "race285a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race285={};
	myVars.races.race285.varName="Object[1641].currentMenuItem";
	myVars.races.race285.varType="@varType@";
	myVars.races.race285.repairType = "@RepairType";
	myVars.races.race285.event1={};
	myVars.races.race285.event2={};
	myVars.races.race285.event1.id = "Lu_Id_a_26";
	myVars.races.race285.event1.type = "onfocus";
	myVars.races.race285.event1.loc = "Lu_Id_a_26_LOC";
	myVars.races.race285.event1.isRead = "True";
	myVars.races.race285.event1.eventType = "@event1EventType@";
	myVars.races.race285.event2.id = "Lu_Id_a_5";
	myVars.races.race285.event2.type = "onmouseover";
	myVars.races.race285.event2.loc = "Lu_Id_a_5_LOC";
	myVars.races.race285.event2.isRead = "False";
	myVars.races.race285.event2.eventType = "@event2EventType@";
	myVars.races.race285.event1.executed= false;// true to disable, false to enable
	myVars.races.race285.event2.executed= false;// true to disable, false to enable
</script>

