<script id = "race119b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race119={};
	myVars.races.race119.varName="notifyme__onmouseover";
	myVars.races.race119.varType="@varType@";
	myVars.races.race119.repairType = "@RepairType";
	myVars.races.race119.event1={};
	myVars.races.race119.event2={};
	myVars.races.race119.event1.id = "Lu_Id_a_64";
	myVars.races.race119.event1.type = "onmouseover";
	myVars.races.race119.event1.loc = "Lu_Id_a_64_LOC";
	myVars.races.race119.event1.isRead = "True";
	myVars.races.race119.event1.eventType = "@event1EventType@";
	myVars.races.race119.event2.id = "Lu_window";
	myVars.races.race119.event2.type = "onload";
	myVars.races.race119.event2.loc = "Lu_window_LOC";
	myVars.races.race119.event2.isRead = "False";
	myVars.races.race119.event2.eventType = "@event2EventType@";
	myVars.races.race119.event1.executed= false;// true to disable, false to enable
	myVars.races.race119.event2.executed= false;// true to disable, false to enable
</script>

