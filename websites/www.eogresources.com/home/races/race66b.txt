<script id = "race66b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race66={};
	myVars.races.race66.varName="Lu_Id_a_10__onfocus";
	myVars.races.race66.varType="@varType@";
	myVars.races.race66.repairType = "@RepairType";
	myVars.races.race66.event1={};
	myVars.races.race66.event2={};
	myVars.races.race66.event1.id = "Lu_Id_a_10";
	myVars.races.race66.event1.type = "onfocus";
	myVars.races.race66.event1.loc = "Lu_Id_a_10_LOC";
	myVars.races.race66.event1.isRead = "True";
	myVars.races.race66.event1.eventType = "@event1EventType@";
	myVars.races.race66.event2.id = "Lu_Id_script_10";
	myVars.races.race66.event2.type = "Lu_Id_script_10__parsed";
	myVars.races.race66.event2.loc = "Lu_Id_script_10_LOC";
	myVars.races.race66.event2.isRead = "False";
	myVars.races.race66.event2.eventType = "@event2EventType@";
	myVars.races.race66.event1.executed= false;// true to disable, false to enable
	myVars.races.race66.event2.executed= false;// true to disable, false to enable
</script>

