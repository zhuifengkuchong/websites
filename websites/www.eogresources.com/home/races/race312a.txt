<script id = "race312a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race312={};
	myVars.races.race312.varName="Object[1641].currentMenuItem";
	myVars.races.race312.varType="@varType@";
	myVars.races.race312.repairType = "@RepairType";
	myVars.races.race312.event1={};
	myVars.races.race312.event2={};
	myVars.races.race312.event1.id = "Lu_Id_a_7";
	myVars.races.race312.event1.type = "onmouseover";
	myVars.races.race312.event1.loc = "Lu_Id_a_7_LOC";
	myVars.races.race312.event1.isRead = "False";
	myVars.races.race312.event1.eventType = "@event1EventType@";
	myVars.races.race312.event2.id = "Lu_Id_a_8";
	myVars.races.race312.event2.type = "onmouseover";
	myVars.races.race312.event2.loc = "Lu_Id_a_8_LOC";
	myVars.races.race312.event2.isRead = "True";
	myVars.races.race312.event2.eventType = "@event2EventType@";
	myVars.races.race312.event1.executed= false;// true to disable, false to enable
	myVars.races.race312.event2.executed= false;// true to disable, false to enable
</script>

