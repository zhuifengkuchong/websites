<script id = "race125b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race125={};
	myVars.races.race125.varName="Lu_Id_a_9__onmouseover";
	myVars.races.race125.varType="@varType@";
	myVars.races.race125.repairType = "@RepairType";
	myVars.races.race125.event1={};
	myVars.races.race125.event2={};
	myVars.races.race125.event1.id = "Lu_Id_a_9";
	myVars.races.race125.event1.type = "onmouseover";
	myVars.races.race125.event1.loc = "Lu_Id_a_9_LOC";
	myVars.races.race125.event1.isRead = "True";
	myVars.races.race125.event1.eventType = "@event1EventType@";
	myVars.races.race125.event2.id = "Lu_Id_script_10";
	myVars.races.race125.event2.type = "Lu_Id_script_10__parsed";
	myVars.races.race125.event2.loc = "Lu_Id_script_10_LOC";
	myVars.races.race125.event2.isRead = "False";
	myVars.races.race125.event2.eventType = "@event2EventType@";
	myVars.races.race125.event1.executed= false;// true to disable, false to enable
	myVars.races.race125.event2.executed= false;// true to disable, false to enable
</script>

