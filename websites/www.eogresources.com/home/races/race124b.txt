<script id = "race124b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race124={};
	myVars.races.race124.varName="Lu_Id_a_8__onmouseover";
	myVars.races.race124.varType="@varType@";
	myVars.races.race124.repairType = "@RepairType";
	myVars.races.race124.event1={};
	myVars.races.race124.event2={};
	myVars.races.race124.event1.id = "Lu_Id_a_8";
	myVars.races.race124.event1.type = "onmouseover";
	myVars.races.race124.event1.loc = "Lu_Id_a_8_LOC";
	myVars.races.race124.event1.isRead = "True";
	myVars.races.race124.event1.eventType = "@event1EventType@";
	myVars.races.race124.event2.id = "Lu_Id_script_10";
	myVars.races.race124.event2.type = "Lu_Id_script_10__parsed";
	myVars.races.race124.event2.loc = "Lu_Id_script_10_LOC";
	myVars.races.race124.event2.isRead = "False";
	myVars.races.race124.event2.eventType = "@event2EventType@";
	myVars.races.race124.event1.executed= false;// true to disable, false to enable
	myVars.races.race124.event2.executed= false;// true to disable, false to enable
</script>

