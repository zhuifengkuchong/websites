<script id = "race148a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race148={};
	myVars.races.race148.varName="Object[1641].currentMenuItem";
	myVars.races.race148.varType="@varType@";
	myVars.races.race148.repairType = "@RepairType";
	myVars.races.race148.event1={};
	myVars.races.race148.event2={};
	myVars.races.race148.event1.id = "Lu_Id_a_41";
	myVars.races.race148.event1.type = "onkeydown";
	myVars.races.race148.event1.loc = "Lu_Id_a_41_LOC";
	myVars.races.race148.event1.isRead = "True";
	myVars.races.race148.event1.eventType = "@event1EventType@";
	myVars.races.race148.event2.id = "Lu_Id_a_4";
	myVars.races.race148.event2.type = "onfocus";
	myVars.races.race148.event2.loc = "Lu_Id_a_4_LOC";
	myVars.races.race148.event2.isRead = "False";
	myVars.races.race148.event2.eventType = "@event2EventType@";
	myVars.races.race148.event1.executed= false;// true to disable, false to enable
	myVars.races.race148.event2.executed= false;// true to disable, false to enable
</script>

