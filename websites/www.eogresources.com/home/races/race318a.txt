<script id = "race318a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race318={};
	myVars.races.race318.varName="Object[1641].currentMenuItem";
	myVars.races.race318.varType="@varType@";
	myVars.races.race318.repairType = "@RepairType";
	myVars.races.race318.event1={};
	myVars.races.race318.event2={};
	myVars.races.race318.event1.id = "Lu_Id_a_9";
	myVars.races.race318.event1.type = "onmouseover";
	myVars.races.race318.event1.loc = "Lu_Id_a_9_LOC";
	myVars.races.race318.event1.isRead = "False";
	myVars.races.race318.event1.eventType = "@event1EventType@";
	myVars.races.race318.event2.id = "Lu_Id_a_10";
	myVars.races.race318.event2.type = "onmouseover";
	myVars.races.race318.event2.loc = "Lu_Id_a_10_LOC";
	myVars.races.race318.event2.isRead = "True";
	myVars.races.race318.event2.eventType = "@event2EventType@";
	myVars.races.race318.event1.executed= false;// true to disable, false to enable
	myVars.races.race318.event2.executed= false;// true to disable, false to enable
</script>

