var creditCardTypeVisa="V";
var creditCardTypeAmex="A";
var creditCardTypeMastercard="M";
var creditCardTypeDiscover="D";
var creditCardTypeDinersClub="D";
var creditCardTypeCarteBlanche="C";
var creditCardTypeEnRoute="E";
var creditCardTypeJCB="J";
function validateCreditCardInformation(F,K,I,J){var D=K;
if($.trim(F).length<1){return false
}if(I.length<1||J.length<1||isNaN(I)||isNaN(J)){return false
}var B=new Date();
var H=new Date();
H.setMonth(I-1);
H.setFullYear(J);
if(H.getFullYear()>=B.getFullYear()){if(H.getFullYear()==B.getFullYear()&&H.getMonth()<B.getMonth()){return false
}}else{return false
}D=$.trim(D);
if(D.length<5||isNaN(D)){return false
}var G="";
var E=D.substring(0,1);
var A=D.substring(0,2);
var C=D.substring(0,3);
var L=D.substring(0,4);
if(A=="34"||A=="37"){if(D.length==15){G=creditCardTypeAmex
}}if(A=="51"||A=="52"||A=="53"||A=="54"||A=="55"){if(D.length==16){G=creditCardTypeMastercard
}}if(L=="6011"){if(D.length==16){G=creditCardTypeDiscover
}}if(E=="4"){if(D.length==16){G=creditCardTypeVisa
}}if(C=="300"||C=="301"||C=="302"||C=="303"||C=="304"||C=="305"||A=="36"){if(D.length==14){G=creditCardTypeDinersClub
}}if(A=="38"){if(D.length==14){G=creditCardTypeCarteBlanche
}}if(L=="2014"||L=="2149"){if(D.length==15){G=creditCardTypeEnRoute
}}if(L=="2131"||L=="1800"){if(D.length==15){G=creditCardTypeJCB
}}if(E=="3"){if(D.length==16){G=creditCardTypeJCB
}}if($.trim(G).length<1){return false
}return($.trim(F).toUpperCase().substring(0,1)==$.trim(G).toUpperCase().substring(0,1))
}function validateSecurityCode(A,B){if($.trim(A).length<1){return false
}while(B.indexOf(" ")>-1){B=B.replace(" ","")
}if(isNaN(B)){return false
}A=$.trim(A).toUpperCase().substring(0,1);
if(A==creditCardTypeVisa||A==creditCardTypeMastercard||A==creditCardTypeJCB||A==creditCardTypeDiscover||A==creditCardTypeDinersClub){if(B.length==3){return true
}else{return false
}}if(A==creditCardTypeAmex){if(B.length==4){return true
}else{return false
}}return true
};