function CFC_popup(/*String*/ url, /*String*/ name, /*String*/ features)
{
		var defaultWidth = 500;
		var defaultHeight = 400;
			
		if (features.indexOf("width") == -1) features += ",width=" + defaultWidth;
		if (features.indexOf("height") == -1) features += ",height=" + defaultHeight;
		var w = window.open(url, name, features);
		w.focus();
}
