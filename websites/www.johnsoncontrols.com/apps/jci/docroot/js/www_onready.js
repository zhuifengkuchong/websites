
var piAId = '55162';
var piCId = '1020';

function async_load()
{
        var s = document.createElement('script'); s.type = 'text/javascript';
        s.src = ('https:' == document.location.protocol ? 'https://pi' : 'http://cdn/') + '.pardot.com/pd.js';
        var c = document.getElementsByTagName('script')[0]; c.parentNode.insertBefore(s, c);
    }


$(function(){

     if (document.location.protocol == 'https:')
     {
       window.location.href = 'http' + window.location.href.substring(5);
    }
     $('a[target="ReplaceMap"]').each(function()
                {
                    $(this).attr('data-origref', $(this).attr('href'));
                    $(this).removeAttr('href');
                    $(this).css( 'cursor', 'pointer' );
                });
                
                $('a[target="ReplaceMap"]').click(function()
                {
                    var div = $('div.iframe');
                    div.empty();
                    div.append("<iframe></iframe>");
                    var iframe = div.find("iframe");
                    iframe.attr('src', $(this).attr('data-origref'));
                    iframe.attr('frameborder', '0');
                    iframe.css("border","0");
                    iframe.css("width", "100%");
                    iframe.css("height", "500px");
                 //   $('div.image').hide();
                });
    

    $('a[target="lightbox"]').each(function()
    {
        var name = 'roadblock';
        if ( $(this).find('img').length > 0)
        {
            var img = $(this).find('img').first();
            var src = img.attr('src');
            name = src.substring(src.lastIndexOf('/'));
        }
        
        $(this).attr('data-lightbox', 'lightbox');
        $(this).attr('target', '_self');
    }); 

    if ($('div.discussionbanner').length > 0)
    {
        var banner = $('div.discussionbanner');
        var content = banner.parent().parent();
        if (content.attr('id') == 'content_wide')
        {
            var cookie = document.cookie;
            if (cookie.indexOf('wcmmode=edit') < 0)
            {
                banner.insertBefore(content);
                banner.css('margin','0 14px 0 14px');
            }
        }
    }

    if ($('div.sliderBox').length > 0)
    {
        var textHeight = 0;
        $('div.sliderBoxText').each(function() 
        {
            if ($(this).height() > textHeight)
                textHeight = $(this).height();
        });
        
        textHeight = textHeight + 63 + 'px';
        $('div.sliderBox').css({ height: textHeight });
        $('div.sliderInner').css({ height: textHeight });
        $('button.sliderButton').css({ left: '114px' });
        
    //  $('button.sliderButton').each(function()
    //  {
    //      var buttonOffset = 165 - ($(this).width() / 2) + 'px';
    //      $('this').css({ left: buttonOffset });
    //  });
        
    }
    
    // Remove the coda-slider-no-js class from the body
    $("body").removeClass("coda-slider-no-js");
    // Preloader
    $(".coda-slider").children('.panel').hide().end().prepend('<p class="loading">Loading...<br /><img src="../images/ajax-loader.gif"/*tpa=http://www.johnsoncontrols.com/apps/jci/docroot/images/ajax-loader.gif*/ alt="loading..." /></p>');
    
    if (($("div#content_right").length == 0) && ($("div#content_left").length == 0)) 
    {
        $('div.www-colctrl-lt0-c0').css('width', '306px');
        $('div.www-colctrl-lt0-c1').css('width', '306px');
        $('div.www-colctrl-lt0-c2').css('width', '306px');
        $('div.www-colctrl-lt1-c0').css('width', '466px');
        $('div.www-colctrl-lt1-c1').css('width', '466px');
        $('div.www-colctrl-lt3-c0').css('width', '624px');
        $('div.www-colctrl-lt3-c1').css('width', '308px');
    }
    else if (($("div#content_right").length != 0) && ($("div#content_left").length != 0))
    {
        $('div.www-colctrl-lt0-c0').css('width', '146px');
        $('div.www-colctrl-lt0-c1').css('width', '146px');
        $('div.www-colctrl-lt0-c2').css('width', '146px');
        $('div.www-colctrl-lt3-c0').css('width', '299px');
        $('div.www-colctrl-lt3-c1').css('width', '153px');
    }
    else if (($("div#content_right").length == 0) && ($("div#content_left").length != 0) || 
             ($("div#content_right").length != 0) && ($("div#content_left").length == 0))
    {
        $('div.www-colctrl-lt1').css('width', '710px');
        $('div.www-colctrl-lt1-c0').css('width', '346px');
        $('div.www-colctrl-lt1-c1').css('width', '346px');
        $('div.www-colctrl-lt5-c0').css('width', '166px');
        $('div.www-colctrl-lt5-c1').css('width', '166px');
        $('div.www-colctrl-lt5-c2').css('width', '166px');
        $('div.www-colctrl-lt5-c3').css('width', '166px');
    }

      $("img#jpn_srch_1").attr("alt","オフィスビル・庁舎");
        $("img#jpn_srch_2").attr("alt","病院・医療施設");
        $("img#jpn_srch_3").attr("alt","ホテル・宿泊施設");
        $("img#jpn_srch_4").attr("alt","商業施設・文化施設");
        $("img#jpn_srch_5").attr("alt","学校・教育施設");
        $("img#jpn_srch_6").attr("alt","工場・DHC");
        $("img#jpn_srch_7").attr("alt","データセンター");
        $("img#jpn_srch_8").attr("alt","企業管理向け");
        $("img#jpn_srch_1").attr("title","オフィスビル・庁舎");
        $("img#jpn_srch_2").attr("title","病院・医療施設");
        $("img#jpn_srch_3").attr("title","ホテル・宿泊施設");
        $("img#jpn_srch_4").attr("title","商業施設・文化施設");
        $("img#jpn_srch_5").attr("title","学校・教育施設");
        $("img#jpn_srch_6").attr("title","工場・DHC");
        $("img#jpn_srch_7").attr("title","データセンター");
        $("img#jpn_srch_8").attr("title","企業管理向け");
       var offset = 0;  
       
        var about = document.getElementById("aboutus");
        var aboutDiv = document.getElementById("about_us");
        if ((about != null) && (aboutDiv != null)) 
        {
            if (navigator.userAgent.indexOf("MSIE 6.0") > 0) 
            {
                aboutDiv.style.marginLeft = about.offsetLeft + "px";
            }
            else
            {
                var sibling = about.previousSibling;
                 while (sibling != null)
                {
                    if (sibling.nodeType == 1)
                        offset = offset + sibling.offsetWidth + 12;
                    sibling = sibling.previousSibling;
                }
                aboutDiv.style.marginLeft = offset + "px";
            }
        }

        var products = document.getElementById("products");
        var productsDiv = document.getElementById("products_solutions");
        if ((products != null) && (productsDiv != null)) 
        {
            if (navigator.userAgent.indexOf("MSIE 6.0") > 0) 
            {
                productsDiv.style.marginLeft = products.offsetLeft + "px";
            }
            else
            {
                offset = 0;
                var sibling = products.previousSibling;
                while (sibling != null)
                {
                    if (sibling.nodeType == 1)
                        offset = offset + sibling.offsetWidth + 12;
                    sibling = sibling.previousSibling;
                }
                productsDiv.style.marginLeft = offset + "px";
            }
        }

        var sustain = document.getElementById("sustainability");
        var sustainDiv = document.getElementById("sustainability_solutions");
        if ((sustain != null) && (sustainDiv != null)) 
        {
            if (navigator.userAgent.indexOf("MSIE 6.0") > 0) 
            {
                sustainDiv.style.marginLeft = sustain.offsetLeft + "px";
            }
            else
            {
                offset = 0;
                var sibling = sustain.previousSibling;
                while (sibling != null)
                {
                    if (sibling.nodeType == 1)
                        offset = offset + sibling.offsetWidth + 12;
                    sibling = sibling.previousSibling;
                }
            
                sustainDiv.style.marginLeft = offset + "px";
            }
        }
         
        var region = document.getElementById("region");

        if (region != null) {
            var margin = 751 - region.offsetWidth;

            if (navigator.userAgent.indexOf("MSIE") > 0) {
                //////////////////////////////////
                //IE margin starts after the logo
                //////////////////////////////////
                margin = margin - 152;
            }

            var links = document.getElementById("loginmenu");

            links.style.marginLeft = margin + "px";
        }
    
        window.webtrendsAsyncInit=function(){
            var dcs=new Webtrends.dcs().init({
                dcsid:"dcsh31lgs10000wge3sqdib3n_9l9y",
                domain:"http://www.johnsoncontrols.com/apps/jci/docroot/js/statse.webtrendslive.com",
                timezone:-6,
                i18n:true,
                offsite:false,
                social:true,
                slider:true,
                mail:true,
                crumb:true,
                form:true,
                download:true,
                onsitedoms:"",
                fpcdom:".johnsoncontrols.com",
                plugins:{
                    //hm:{src:"../../../../../s.webtrends.com/js/webtrends.hm.js"/*tpa=http://s.webtrends.com/js/webtrends.hm.js*/}
                }
                }).track();
        };
        (function(){
            var s=document.createElement("script"); s.async=true; s.src="webtrends.js"/*tpa=http://www.johnsoncontrols.com/apps/jci/docroot/js/webtrends.js*/;    
            var s2=document.getElementsByTagName("script")[0]; s2.parentNode.insertBefore(s,s2);
        }());
        
        
        if (((window.location.host == "http://www.johnsoncontrols.com/apps/jci/docroot/js/author.johnsoncontrols.com") || (window.location.host == "http://www.johnsoncontrols.com/apps/jci/docroot/js/www.johnsoncontrols.com")) && (window.location.pathname.indexOf("building_efficiency") >= 0))
        {
            if (window.attachEvent)
            {
                window.attachEvent('onload', async_load);
            }
            else
            {
                window.addEventListener('load', async_load, false); 
            }
        }

         if (window.navigator.userAgent.indexOf("MSIE" > 0))   
         {
             $('div.panel a').css('font-weight', 'normal');
         }
               
    
});
