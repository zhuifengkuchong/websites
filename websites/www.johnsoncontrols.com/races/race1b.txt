<script id = "race1b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race1={};
	myVars.races.race1.varName="top__onmouseover";
	myVars.races.race1.varType="@varType@";
	myVars.races.race1.repairType = "@RepairType";
	myVars.races.race1.event1={};
	myVars.races.race1.event2={};
	myVars.races.race1.event1.id = "loginmenu";
	myVars.races.race1.event1.type = "onmouseover";
	myVars.races.race1.event1.loc = "loginmenu_LOC";
	myVars.races.race1.event1.isRead = "True";
	myVars.races.race1.event1.eventType = "@event1EventType@";
	myVars.races.race1.event2.id = "Lu_DOM";
	myVars.races.race1.event2.type = "onDOMContentLoaded";
	myVars.races.race1.event2.loc = "Lu_DOM_LOC";
	myVars.races.race1.event2.isRead = "False";
	myVars.races.race1.event2.eventType = "@event2EventType@";
	myVars.races.race1.event1.executed= false;// true to disable, false to enable
	myVars.races.race1.event2.executed= false;// true to disable, false to enable
</script>

