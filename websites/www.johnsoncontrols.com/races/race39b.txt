<script id = "race39b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race39={};
	myVars.races.race39.varName="Function[873].active";
	myVars.races.race39.varType="@varType@";
	myVars.races.race39.repairType = "@RepairType";
	myVars.races.race39.event1={};
	myVars.races.race39.event2={};
	myVars.races.race39.event1.id = "feedLoader";
	myVars.races.race39.event1.type = "onload";
	myVars.races.race39.event1.loc = "feedLoader_LOC";
	myVars.races.race39.event1.isRead = "True";
	myVars.races.race39.event1.eventType = "@event1EventType@";
	myVars.races.race39.event2.id = "feedLoader";
	myVars.races.race39.event2.type = "onload";
	myVars.races.race39.event2.loc = "feedLoader_LOC";
	myVars.races.race39.event2.isRead = "False";
	myVars.races.race39.event2.eventType = "@event2EventType@";
	myVars.races.race39.event1.executed= false;// true to disable, false to enable
	myVars.races.race39.event2.executed= false;// true to disable, false to enable
</script>

