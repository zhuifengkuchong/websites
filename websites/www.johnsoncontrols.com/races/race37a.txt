<script id = "race37a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race37={};
	myVars.races.race37.varName="Window[26].get_rss_feed";
	myVars.races.race37.varType="@varType@";
	myVars.races.race37.repairType = "@RepairType";
	myVars.races.race37.event1={};
	myVars.races.race37.event2={};
	myVars.races.race37.event1.id = "Lu_Id_script_11";
	myVars.races.race37.event1.type = "Lu_Id_script_11__parsed";
	myVars.races.race37.event1.loc = "Lu_Id_script_11_LOC";
	myVars.races.race37.event1.isRead = "False";
	myVars.races.race37.event1.eventType = "@event1EventType@";
	myVars.races.race37.event2.id = "feedLoader";
	myVars.races.race37.event2.type = "onload";
	myVars.races.race37.event2.loc = "feedLoader_LOC";
	myVars.races.race37.event2.isRead = "True";
	myVars.races.race37.event2.eventType = "@event2EventType@";
	myVars.races.race37.event1.executed= false;// true to disable, false to enable
	myVars.races.race37.event2.executed= false;// true to disable, false to enable
</script>

