<script id = "race51b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race51={};
	myVars.races.race51.varName="Object[3670].display";
	myVars.races.race51.varType="@varType@";
	myVars.races.race51.repairType = "@RepairType";
	myVars.races.race51.event1={};
	myVars.races.race51.event2={};
	myVars.races.race51.event1.id = "products";
	myVars.races.race51.event1.type = "onmouseover";
	myVars.races.race51.event1.loc = "products_LOC";
	myVars.races.race51.event1.isRead = "True";
	myVars.races.race51.event1.eventType = "@event1EventType@";
	myVars.races.race51.event2.id = "login";
	myVars.races.race51.event2.type = "onmouseover";
	myVars.races.race51.event2.loc = "login_LOC";
	myVars.races.race51.event2.isRead = "False";
	myVars.races.race51.event2.eventType = "@event2EventType@";
	myVars.races.race51.event1.executed= false;// true to disable, false to enable
	myVars.races.race51.event2.executed= false;// true to disable, false to enable
</script>

