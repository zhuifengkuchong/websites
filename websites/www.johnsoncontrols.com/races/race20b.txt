<script id = "race20b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race20={};
	myVars.races.race20.varName="products_solutions__onmouseout";
	myVars.races.race20.varType="@varType@";
	myVars.races.race20.repairType = "@RepairType";
	myVars.races.race20.event1={};
	myVars.races.race20.event2={};
	myVars.races.race20.event1.id = "products_solutions";
	myVars.races.race20.event1.type = "onmouseout";
	myVars.races.race20.event1.loc = "products_solutions_LOC";
	myVars.races.race20.event1.isRead = "True";
	myVars.races.race20.event1.eventType = "@event1EventType@";
	myVars.races.race20.event2.id = "Lu_DOM";
	myVars.races.race20.event2.type = "onDOMContentLoaded";
	myVars.races.race20.event2.loc = "Lu_DOM_LOC";
	myVars.races.race20.event2.isRead = "False";
	myVars.races.race20.event2.eventType = "@event2EventType@";
	myVars.races.race20.event1.executed= false;// true to disable, false to enable
	myVars.races.race20.event2.executed= false;// true to disable, false to enable
</script>

