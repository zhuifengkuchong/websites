<script id = "race3a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race3={};
	myVars.races.race3.varName="Lu_Id_div_1__onmousedown";
	myVars.races.race3.varType="@varType@";
	myVars.races.race3.repairType = "@RepairType";
	myVars.races.race3.event1={};
	myVars.races.race3.event2={};
	myVars.races.race3.event1.id = "Lu_window";
	myVars.races.race3.event1.type = "onload";
	myVars.races.race3.event1.loc = "Lu_window_LOC";
	myVars.races.race3.event1.isRead = "False";
	myVars.races.race3.event1.eventType = "@event1EventType@";
	myVars.races.race3.event2.id = "Lu_Id_div_1";
	myVars.races.race3.event2.type = "onmousedown";
	myVars.races.race3.event2.loc = "Lu_Id_div_1_LOC";
	myVars.races.race3.event2.isRead = "True";
	myVars.races.race3.event2.eventType = "@event2EventType@";
	myVars.races.race3.event1.executed= false;// true to disable, false to enable
	myVars.races.race3.event2.executed= false;// true to disable, false to enable
</script>

