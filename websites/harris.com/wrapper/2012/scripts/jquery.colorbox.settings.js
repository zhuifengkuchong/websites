$(document).ready(function(){
	$(".youtube640x360").colorbox({iframe:true, innerWidth:640, innerHeight:360});
	$(".html5_video_player").colorbox({iframe:true, innerWidth:640, innerHeight:370});
	$(".youtubeRF").colorbox({iframe:true, innerWidth:510, innerHeight:287});		
	$(".HIMSS2013").colorbox({iframe:true, innerWidth:535, innerHeight:360});
	$(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
	$(".HomePageSpotlights").colorbox({iframe:true, width:"750", height:"450"});
	$(".HomePageSpotlightsOblivion").colorbox({ iframe: true, width: "930", height: "670" });
	$(".HomePageSpotlights760x420").colorbox({ iframe: true, width: "760", height: "420" });
	$(".iframeAR").colorbox({iframe:true, width:"964px", height:"630px"});
	$(".widget").colorbox({ iframe: true, width: "800px", height: "695px" });
	$(".widget750x550").colorbox({ iframe: true, width: "750px", height: "580px" });
	$(".widget750x620").colorbox({ iframe: true, width: "750px", height: "620px" });
	$(".widget730x820").colorbox({ iframe: true, width: "730px", height: "820px" });
				//Examples of how to assign the ColorBox event to elements
				$(".group1").colorbox({rel:'group1'});
				$(".group2").colorbox({rel:'group2', transition:"fade", width:"800",});
				$(".group3").colorbox({rel:'group3', transition:"fade", width:"50%", height:"50%"});
				//$(".group4").colorbox({rel:'group4', slideshow:true});
				//$(".ajax").colorbox();
				//$(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
				//$(".inline").colorbox({inline:true, width:"50%"});
				//$(".callbacks").colorbox({
				//	onOpen:function(){ alert('onOpen: colorbox is about to open'); },
				//	onLoad:function(){ alert('onLoad: colorbox has started to load the targeted content'); },
				//	onComplete:function(){ alert('onComplete: colorbox has displayed the loaded content'); },
				//	onCleanup:function(){ alert('onCleanup: colorbox has begun the close process'); },
				//	onClosed:function(){ alert('onClosed: colorbox has completely closed'); }
				//});
				
				//Example of preserving a JavaScript event for inline calls.
				//$("#click").click(function(){ 
				//	$('#click').css({"background-color":"#f00", "color":"#fff", "cursor":"inherit"}).text("Open this window again and this message will still be here.");
				//	return false;
				//});
});

$('.boilerplate').load('http://harris.com/wrapper/2012/CHQ/boilerplate.html');
$('.webcast').load('http://harris.com/wrapper/2012/CHQ/MenuContent/I-Webcast.html');
$('.StockQuote').load('http://harris.com/wrapper/2012/CHQ/StockQuotes.asp');