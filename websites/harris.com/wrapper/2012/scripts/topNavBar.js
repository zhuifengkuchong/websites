$(document).ready(function(){
      $('.Icon a[target="Menu"]').bind('click',function(event){  					// Look for any link items inside the Icon Div that have a target of Menu
			event.preventDefault();													// prevent the link from opening in current or new window
			var link = $(this).attr('href');										// Capture the HREF of the link that was clicked
			$(this).closest('.Icon').find('.activeTab').removeClass('activeTab');	// Find the Parent Icon and then find the current activeTab and remove that class name -- used to show which Icon is active
			this.className = "activeTab";											// Set the class name of the link that was clicked
			container = $(this).closest('.DropDown').find('.MenuContent');			// Locate the MenuContent Div inside this items parent Drop Down
			container.addClass("activeContent");									// Set the class name of the that MenuContent Div to activeContent	
			$(container).load(link);												// Load the HREF of the link clicked into the MenuContent Div
		});
});