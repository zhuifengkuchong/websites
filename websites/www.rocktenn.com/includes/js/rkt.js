window.onload = function () {
	if (!document.getElementsByTagName) return false;
}
//If the browser is W3 DOM compliant, execute setImageSwaps function
if (document.getElementsByTagName && document.getElementById) {
if (window.addEventListener) window.addEventListener('load', setImageSwaps, false);
else if (window.attachEvent) window.attachEvent('onload', setImageSwaps);
}

//When document loads, apply the prepareImageSwap function to various images with our desired settings
function setImageSwaps() {
prepareImageSwap(document.body);
}
//
function prepareImageSwap(elem,mouseOver,mouseOutRestore,mouseDown,mouseUpRestore,mouseOut,mouseUp) { 
//Non-Obtrusive Image Swap Script V1.1 by Hesido.com 
if (typeof(elem) == 'string') elem = document.getElementById(elem);
	if (elem == null) return;
	var regg = /(.*)(_nm\.)([^\.]{3,4})$/
	var prel = new Array(), img, imgList, imgsrc, mtchd;
	imgList = elem.getElementsByTagName('img');

	for (var i=0; img = imgList[i]; i++) {
		if (!img.rolloverSet && img.src.match(regg)) {
			mtchd = img.src.match(regg);
			img.hoverSRC = mtchd[1]+'_hv.'+ mtchd[3];
			img.outSRC = img.src;
			if (typeof(mouseOver) != 'undefined') {
				img.hoverSRC = (mouseOver) ? mtchd[1]+'_hv.'+ mtchd[3] : false;
				img.outSRC = (mouseOut) ? mtchd[1]+'_ou.'+ mtchd[3] : (mouseOver && mouseOutRestore) ? img.src : false;
				img.mdownSRC = (mouseDown) ? mtchd[1]+'_md.' + mtchd[3] : false;
				img.mupSRC = (mouseUp) ? mtchd[1]+'_mu.' + mtchd[3] : (mouseOver && mouseDown && mouseUpRestore) ? img.hoverSRC : (mouseDown && mouseUpRestore) ? img.src : false;
				}
			if (img.hoverSRC) {preLoadImg(img.hoverSRC); img.onmouseover = imgHoverSwap;}
			if (img.outSRC) {preLoadImg(img.outSRC); img.onmouseout = imgOutSwap;}
			if (img.mdownSRC) {preLoadImg(img.mdownSRC); img.onmousedown = imgMouseDownSwap;}
			if (img.mupSRC) {preLoadImg(img.mupSRC); img.onmouseup = imgMouseUpSwap;}
			img.rolloverSet = true;
		}
	}

	function preLoadImg(imgSrc) {
		prel[prel.length] = new Image(); prel[prel.length-1].src = imgSrc;
	}

}
//
function imgHoverSwap() {this.src = this.hoverSRC;}
function imgOutSwap() {this.src = this.outSRC;}
function imgMouseDownSwap() {this.src = this.mdownSRC;}
function imgMouseUpSwap() {this.src = this.mupSRC;}
//

/* ---------------------------- */
/* Dropdowns
/* ---------------------------- */

$(document).ready(function() {	
	function hideDropdowns() {
		$("#navholder").find("ul").hide();
		}
	function ulHover(whichDiv) {
		$(whichDiv).hover(function() {
				/* Do nothing */
			},function() { 
				hideDropdowns();
			});
		}
	$("#menu-about").hover(function() {
				hideDropdowns();
				$("#dropdown-about").show();
			}, function() {
				ulHover("#dropdown-about");
			});
	$("#menu-products").hover(function() {
				hideDropdowns();
				$("#dropdown-products").show();
			}, function() {
				ulHover("#dropdown-products");
			});
	$("#menu-career").hover(function() {
				hideDropdowns();
				$("#dropdown-career").show();
			}, function() {
				ulHover("#dropdown-career");
			});
	$("#menu-languages").hover(function() {
				hideDropdowns();
				$("#dropdown-languages").show();
			}, function() {
				ulHover("#dropdown-languages");
			});
	
	$("#menu-sustainability").hover(function() {
				hideDropdowns();
				$("#dropdown-sustainability").fadeIn("fast");
			}, function() {
				ulHover("#dropdown-sustainability");
			});
	/*
	$("#menu-investors").hover(function() {
				hideDropdowns();
				$("#dropdown-investors").fadeIn("fast");
			}, function() {
				ulHover("#dropdown-investors");
			});
	*/
	$("#navholder").hover(function(){ 
			/* Do nothing */ 
			}, function() {
				hideDropdowns();
			});
});