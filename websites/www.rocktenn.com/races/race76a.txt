<script id = "race76a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race76={};
	myVars.races.race76.varName="Object[7758].olddisplay";
	myVars.races.race76.varType="@varType@";
	myVars.races.race76.repairType = "@RepairType";
	myVars.races.race76.event1={};
	myVars.races.race76.event2={};
	myVars.races.race76.event1.id = "menu-sustainability";
	myVars.races.race76.event1.type = "onmouseover";
	myVars.races.race76.event1.loc = "menu-sustainability_LOC";
	myVars.races.race76.event1.isRead = "False";
	myVars.races.race76.event1.eventType = "@event1EventType@";
	myVars.races.race76.event2.id = "dropdown-about";
	myVars.races.race76.event2.type = "onmouseout";
	myVars.races.race76.event2.loc = "dropdown-about_LOC";
	myVars.races.race76.event2.isRead = "True";
	myVars.races.race76.event2.eventType = "@event2EventType@";
	myVars.races.race76.event1.executed= false;// true to disable, false to enable
	myVars.races.race76.event2.executed= false;// true to disable, false to enable
</script>

