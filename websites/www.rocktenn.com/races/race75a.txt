<script id = "race75a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race75={};
	myVars.races.race75.varName="Object[7738].olddisplay";
	myVars.races.race75.varType="@varType@";
	myVars.races.race75.repairType = "@RepairType";
	myVars.races.race75.event1={};
	myVars.races.race75.event2={};
	myVars.races.race75.event1.id = "menu-career";
	myVars.races.race75.event1.type = "onmouseover";
	myVars.races.race75.event1.loc = "menu-career_LOC";
	myVars.races.race75.event1.isRead = "False";
	myVars.races.race75.event1.eventType = "@event1EventType@";
	myVars.races.race75.event2.id = "dropdown-about";
	myVars.races.race75.event2.type = "onmouseout";
	myVars.races.race75.event2.loc = "dropdown-about_LOC";
	myVars.races.race75.event2.isRead = "True";
	myVars.races.race75.event2.eventType = "@event2EventType@";
	myVars.races.race75.event1.executed= false;// true to disable, false to enable
	myVars.races.race75.event2.executed= false;// true to disable, false to enable
</script>

