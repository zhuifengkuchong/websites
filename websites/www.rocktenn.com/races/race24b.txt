<script id = "race24b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race24={};
	myVars.races.race24.varName="dropdown-career__onmouseover";
	myVars.races.race24.varType="@varType@";
	myVars.races.race24.repairType = "@RepairType";
	myVars.races.race24.event1={};
	myVars.races.race24.event2={};
	myVars.races.race24.event1.id = "dropdown-career";
	myVars.races.race24.event1.type = "onmouseover";
	myVars.races.race24.event1.loc = "dropdown-career_LOC";
	myVars.races.race24.event1.isRead = "True";
	myVars.races.race24.event1.eventType = "@event1EventType@";
	myVars.races.race24.event2.id = "menu-career";
	myVars.races.race24.event2.type = "onmouseout";
	myVars.races.race24.event2.loc = "menu-career_LOC";
	myVars.races.race24.event2.isRead = "False";
	myVars.races.race24.event2.eventType = "@event2EventType@";
	myVars.races.race24.event1.executed= false;// true to disable, false to enable
	myVars.races.race24.event2.executed= false;// true to disable, false to enable
</script>

