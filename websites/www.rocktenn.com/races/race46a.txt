<script id = "race46a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race46={};
	myVars.races.race46.varName="dropdown-sustainability__onmouseout";
	myVars.races.race46.varType="@varType@";
	myVars.races.race46.repairType = "@RepairType";
	myVars.races.race46.event1={};
	myVars.races.race46.event2={};
	myVars.races.race46.event1.id = "menu-sustainability";
	myVars.races.race46.event1.type = "onmouseout";
	myVars.races.race46.event1.loc = "menu-sustainability_LOC";
	myVars.races.race46.event1.isRead = "False";
	myVars.races.race46.event1.eventType = "@event1EventType@";
	myVars.races.race46.event2.id = "dropdown-sustainability";
	myVars.races.race46.event2.type = "onmouseout";
	myVars.races.race46.event2.loc = "dropdown-sustainability_LOC";
	myVars.races.race46.event2.isRead = "True";
	myVars.races.race46.event2.eventType = "@event2EventType@";
	myVars.races.race46.event1.executed= false;// true to disable, false to enable
	myVars.races.race46.event2.executed= false;// true to disable, false to enable
</script>

