<script id = "race58a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race58={};
	myVars.races.race58.varName="Object[1389].guid";
	myVars.races.race58.varType="@varType@";
	myVars.races.race58.repairType = "@RepairType";
	myVars.races.race58.event1={};
	myVars.races.race58.event2={};
	myVars.races.race58.event1.id = "menu-products";
	myVars.races.race58.event1.type = "onmouseout";
	myVars.races.race58.event1.loc = "menu-products_LOC";
	myVars.races.race58.event1.isRead = "False";
	myVars.races.race58.event1.eventType = "@event1EventType@";
	myVars.races.race58.event2.id = "menu-career";
	myVars.races.race58.event2.type = "onmouseout";
	myVars.races.race58.event2.loc = "menu-career_LOC";
	myVars.races.race58.event2.isRead = "True";
	myVars.races.race58.event2.eventType = "@event2EventType@";
	myVars.races.race58.event1.executed= false;// true to disable, false to enable
	myVars.races.race58.event2.executed= false;// true to disable, false to enable
</script>

