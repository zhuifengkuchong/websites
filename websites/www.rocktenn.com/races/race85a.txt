<script id = "race85a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race85={};
	myVars.races.race85.varName="Object[7738].olddisplay";
	myVars.races.race85.varType="@varType@";
	myVars.races.race85.repairType = "@RepairType";
	myVars.races.race85.event1={};
	myVars.races.race85.event2={};
	myVars.races.race85.event1.id = "menu-career";
	myVars.races.race85.event1.type = "onmouseover";
	myVars.races.race85.event1.loc = "menu-career_LOC";
	myVars.races.race85.event1.isRead = "False";
	myVars.races.race85.event1.eventType = "@event1EventType@";
	myVars.races.race85.event2.id = "dropdown-career";
	myVars.races.race85.event2.type = "onmouseout";
	myVars.races.race85.event2.loc = "dropdown-career_LOC";
	myVars.races.race85.event2.isRead = "True";
	myVars.races.race85.event2.eventType = "@event2EventType@";
	myVars.races.race85.event1.executed= false;// true to disable, false to enable
	myVars.races.race85.event2.executed= false;// true to disable, false to enable
</script>

