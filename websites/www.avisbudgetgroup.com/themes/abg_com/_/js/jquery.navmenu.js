/*
 * jQuery Nav Menu plugin
 * @author Dean Whillier
 */
(function($){

	$.fn.navMenu = function(options)
	{
		// define option defaults
		var defaults = {
			fadeDuration : 200,
			hideDelay: 150,
			defineDepths: true,
			containerName: 'nav-container',
			levelName: 'nav-level',
			itemName: 'nav-item',
			hasChildrenName: 'has-children',
			firstName: 'first',
			lastName: 'last',
			openName: 'open',
			expandLeftName: 'expandLeft',
			childrenExpandLeftName: 'childrenExpandLeft',
			activeMenuItem: undefined
		};
		
		// merge provided and default options
		options = $.extend(defaults, options);
		
		// function applies css classes to menu tags
		function applyNavMenuClasses(level, object)
		{
			// apply ul container classes
			object.addClass(options.containerName + ' ' + options.levelName + '-' + level);
			if(options.defineDepths){
				object.css('z-index', level);
			}
			// apply list item classes
			object.find('> li').each(function(index){
				// add item index class
				$(this).addClass(options.itemName + ' ' + options.itemName + '-' + (index + 1));
				$(this).find('> a').addClass(options.itemName + ' ' + options.itemName + '-' + (index + 1));
				// add first-item class as applicable
				if(index == 0){
					$(this).addClass(options.firstName);
					$(this).find('> a').addClass(options.firstName);
				}
				// add last-item class
				if(index == $(this).parent().children().length - 1){
					$(this).addClass(options.lastName);
					$(this).find('> a').addClass(options.lastName);
				}
				// add has-children class as applicable
				if($(this).find('> ul').length){
					$(this).addClass(options.hasChildrenName);
					$(this).find('> a').addClass(options.hasChildrenName);
				}
			});
			// look for additional sub nav levels to apply classes to
			object.find('> li > ul').each(function(index){
				applyNavMenuClasses(level + 1, $(this));
			});
		}
		
		// function mimicks ActionScript localToGlobal functionality
		function localToGlobal( _element )
		{
			var target = _element, target_width = target.offsetWidth, target_height = target.offsetHeight, target_left = target.offsetLeft, target_top = target.offsetTop, gleft = 0, gtop = 0, rect = {};
			
			var traverse = function( _parent ) {
				if (!!_parent) {
					gleft += _parent.offsetLeft;
					gtop += _parent.offsetTop;
					traverse( _parent.offsetParent );
				} else {
					return rect = {
						top: target.offsetTop + gtop,
						left: target.offsetLeft + gleft,
						bottom: (target.offsetTop + gtop) + target_height,
						right: (target.offsetLeft + gleft) + target_width
					};
				}
			};
			traverse(target.offsetParent );
			return rect;
		}
		
		// return processed elements
		return this.each(function(index){
			
			var navContainer = $(this);
			
			// show all subnav levels (needed to calculate positions properly)
			navContainer.find('ul ul').show();
			
			// set active item if provided
			//console.log(options.activeMenuItem);
			if(options.activeMenuItem != undefined){
				options.activeMenuItem.addClass('active');
			}
			
			// apply nav container level classes
			applyNavMenuClasses(1, navContainer);
			
			// apply user interaction functionality
			navContainer.find('li').hover(function(){
				if(options.activeMenuItem == undefined || $(this).closest(options.activeMenuItem).get(0) != options.activeMenuItem.get(0)){
					$(this).addClass(options.openName);
					$(this).find('> ul').stop(true, true).fadeIn(options.fadeDuration);
					navContainer.find('li.active > ul').stop(true, true).fadeOut(options.fadeDuration);
				}
			}, function(){
				if(options.activeMenuItem == undefined || $(this).closest(options.activeMenuItem).get(0) != options.activeMenuItem.get(0)){
					$(this).removeClass(options.openName);
					$(this).find('> ul').stop(true, true).delay(options.hideDelay).fadeOut(options.fadeDuration);
					navContainer.find('li.active > ul').stop(true, true).delay(options.hideDelay).fadeIn(options.fadeDuration);
				}
			});
			
			// check for menus that open outside menu bounds and switch directions
			var menuBounds = localToGlobal($(navContainer).get(0));
			navContainer.find('li.has-children').each(function(index){
				if($(this).find('> ul').length){
					var container = $('> ul', this).get(0);
					var containerBounds = localToGlobal(container);
					if(containerBounds.right > menuBounds.right){
						$(container).addClass(options.expandLeftName).parent().parent().addClass(options.childrenExpandLeftName);
					}
				}
			});
			
			// (re)hide all subnav levels
			navContainer.find('ul ul').hide();
		
		});
	}

})(jQuery);