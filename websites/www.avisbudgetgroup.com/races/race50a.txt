<script id = "race50a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race50={};
	myVars.races.race50.varName="Lu_Id_li_18__onmouseout";
	myVars.races.race50.varType="@varType@";
	myVars.races.race50.repairType = "@RepairType";
	myVars.races.race50.event1={};
	myVars.races.race50.event2={};
	myVars.races.race50.event1.id = "Lu_DOM";
	myVars.races.race50.event1.type = "onDOMContentLoaded";
	myVars.races.race50.event1.loc = "Lu_DOM_LOC";
	myVars.races.race50.event1.isRead = "False";
	myVars.races.race50.event1.eventType = "@event1EventType@";
	myVars.races.race50.event2.id = "Lu_Id_li_19";
	myVars.races.race50.event2.type = "onmouseout";
	myVars.races.race50.event2.loc = "Lu_Id_li_19_LOC";
	myVars.races.race50.event2.isRead = "True";
	myVars.races.race50.event2.eventType = "@event2EventType@";
	myVars.races.race50.event1.executed= false;// true to disable, false to enable
	myVars.races.race50.event2.executed= false;// true to disable, false to enable
</script>

