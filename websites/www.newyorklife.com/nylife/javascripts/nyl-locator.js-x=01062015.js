
/* functions for map  */

var directionsDisplay;
var directionsService = new google.maps.DirectionsService();
var markers = [];    
var agentResultsHtml;
var lastAgentIndex;
var map ;
var infowindow = new google.maps.InfoWindow(); ;
var bounds = new google.maps.LatLngBounds();
var infoWindowAlreadyOpen = false;
var startMarker, endMarker;
var contents = [];
var areAgentsOnMap = true;  
  
function initMap(lat, lng, inputAddress) {
	
 // Create a renderer for directions and bind it to the map.
  var rendererOptions = {
    map: map,
    suppressMarkers : true
  };
  directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions);
  // Instantiate an info window to hold text for start/end point in driving direction.
  startEndDisplay = new google.maps.InfoWindow();

  var myLatLng = new google.maps.LatLng(lat, lng);
  var mapOptions = {
    zoom: 12,
    center: myLatLng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
  directionsDisplay.setMap(map);	
  directionsDisplay.setPanel(document.getElementById('agent-panel'));
  if  (inputAddress != null && inputAddress.length > 0)
	  showUserLocation(inputAddress);	
}
  
function backToResults(zoom, inputAddress) {
	document.getElementById("agent-panel").innerHTML = agentResultsHtml;
	// write back the driving-directions form value on embedded maps
	if ($('#driving-directions').length) {
		$('#driving-directions').val(inputAddress);
	}
	
	if  (areAgentsOnMap)
		showInfoWindow(lastAgentIndex);
	else {// when coming from agents for a GO page, show office info 
    	x = lastAgentIndex % 5;
    	if  (x < 3)
    		$('#agent-panel').animate({scrollTop:0}, 0);
    	else 
    		$('#agent-panel').animate({scrollTop:$('#agent-panel').height()}, 0);		
	}
	
	directionsDisplay.setMap(null);
    for (var i = 0; i < markers.length; i++) 
   		markers[i].setVisible(true);
	
	startEndDisplay.close();
	startMarker.setVisible(false);
	endMarker.setVisible(false);
	
	if  (areAgentsOnMap) {
		// Automatically center the map fitting all markers on the screen
		map.fitBounds(bounds);
		if (zoom) {
			map.setZoom(zoom);
		}
	} 
}
	
function showUserLocation(inputAddress) {
  geocoder = new google.maps.Geocoder();
  geocoder.geocode( { 'address': inputAddress}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
    	position = results[0].geometry.location;

		marker = new google.maps.Marker({
        position: position,
        map: map,
        title: "You are here"
		});
 		
		markers.push(marker);
		bounds.extend (position);
		map.fitBounds(bounds);
	} else {
      alert('Geocode was not successful for the following reason: ' + status);
    }
  });
}
	
	
function calcRoute(destinationAddress, inputAddress, contentIndex, zoom) {
	if (inputAddress === "") {
		alert("Please enter start address");
		return;
	}
	
	infowindow.close();
    directionsDisplay.setMap(map);
	agentResultsHtml = document.getElementById("agent-panel").innerHTML;
	lastAgentIndex = contentIndex;
	if (zoom !== "") {
		document.getElementById("agent-panel").innerHTML = "<button onClick=\"javascript:backToResults(" + zoom + ",'" + inputAddress + "');\">Back To Results</button>";
	} else {
		document.getElementById("agent-panel").innerHTML = "<button onClick=\"javascript:backToResults('','" + inputAddress + "');\">Back To Results</button>";
	}
    $("#agent-panel").append("<div id=\"panel\"><b>Mode of travel: </b><select id=\"mode\" onchange=\"changeTravelMode('" + destinationAddress + "','" + inputAddress + "');\"><option value=\"DRIVING\">Driving</option><option value=\"WALKING\">Walking</option><option value=\"BICYCLING\">Bicycling</option><option value=\"TRANSIT\">Transit</option></select></div>");
    
     for (var i = 0; i < markers.length; i++) 
   		markers[i].setVisible(false);

	var request = {
		origin : inputAddress,
		destination : destinationAddress,
		durationInTraffic : true,
		travelMode : google.maps.TravelMode.DRIVING
	};
	directionsService.route(request, function(response, status) {
		if (status == google.maps.DirectionsStatus.OK) {
			directionsDisplay.setDirections(response);
     		showStartEndMarkers(response, inputAddress, contentIndex);
		}
	});
}

function changeTravelMode(destinationAddress, inputAddress) {
	var selectedMode = document.getElementById('mode').value;
	var request = {
		origin : inputAddress,
		destination : destinationAddress,
		durationInTraffic : true,
		travelMode : google.maps.TravelMode[selectedMode]
	};
	directionsService.route(request, function(response, status) {
		if (status == google.maps.DirectionsStatus.OK) {
			directionsDisplay.setDirections(response); 	
		}
	});
}
	
function showStartEndMarkers(directionResult, inputAddress, contentIndex) {

  var myRoute = directionResult.routes[0].legs[0];

    startMarker = new google.maps.Marker({
      position: myRoute.start_location,
      map: map,
      icon: "http://mt.googleapis.com/vt/icon/name=icons/spotlight/spotlight-waypoint-a.png&text=A&psize=16&font=fonts/Roboto-Regular.ttf&color=ff333333&ax=44&ay=48&scale=1"
    });
    
    attachInstructionText(startMarker, inputAddress );
        
    endMarker = new google.maps.Marker({
      position: myRoute.end_location,
      map: map,
      icon: "http://mt.googleapis.com/vt/icon/name=icons/spotlight/spotlight-waypoint-b.png&text=B&psize=16&font=fonts/Roboto-Regular.ttf&color=ff333333&ax=44&ay=48&scale=1"
    });
   	

    attachInstructionText(endMarker, contents[contentIndex]);
    /* This was causing the markers to be out of the viewport (visible area of map) in some cases
    google.maps.event.trigger(startMarker, "click");
    google.maps.event.trigger(endMarker, "click");
    */

}

function attachInstructionText(marker, text) {
  
  google.maps.event.addListener(marker, 'click', function() {
    // Open an info window when the marker is clicked on,
    // containing the text of the step.
    startEndDisplay.setContent(text);
    startEndDisplay.open(map, marker);
  });
}

function highlightSelectedAgent(id) {
	 showInfoWindow(id);
}

function highlightSelectedAgent2(id) {
	/* this doesn't work after navigating to a different page
	$(".agent-info").removeClass("highlight");
	$("#agent<%=i%>").addClass("highlight");
	*/
	$(".agent-info").css("backgroundColor", "transparent");    	
	$("#agent" + id).css("backgroundColor", "yellow");
}

function showInfoWindow(id) {
	infowindow.close();
	google.maps.event.trigger(markers[id], 'click');
}

function closeInfoWindow() {
	$(".agent-info").css("backgroundColor", "transparent"); 
	infowindow.close();
}

var inputAddressInit = false;
function inputAddressClick(ele) {

	if  (!inputAddressInit) {
		inputAddressInit = true;
		ele.value = '';
		ele.style.color="black" ;
	}
}

function currentLocationClick(formId, fieldId) {

	if  ("geolocation" in navigator) {

		var geo_options = {
		  enableHighAccuracy: false, 
		  maximumAge        : 30*60*1000, // cache location for 30 min
		  timeout           : 30 * 1000   // timeout 30 sec for user permission
		};
		
		navigator.geolocation.getCurrentPosition(
			function(position) { // success
				console.log (position.coords.latitude + ", " + position.coords.longitude)
				codeLatLng(position.coords.latitude, position.coords.longitude, formId, fieldId);
			},function(err) { // failure
				alert (err.message);
			} , geo_options
		);
	} else {
		alert ("Sorry, cannot determine current location. Please input the location in search box.");
	}
}	

function codeLatLng(lat, lng, formId, fieldId) {

  var latlng = new google.maps.LatLng(lat, lng);
  geocoder = new google.maps.Geocoder();
  geocoder.geocode({'latLng': latlng}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      if (results[1]) {
        document.getElementById(fieldId).value=results[1].formatted_address;
        document.getElementById(formId).submit();
      } else {
        alert('No current location found');
      }
    } else {
      alert('Geocoder failed due to: ' + status);
    }
  });
}



/*  functions for pagination  */
//var nylife = {};
nyl.Pager = function() {
    this.currentPage = 1;
    this.pagingControlsContainer = '#pagingControls';


    this.numPages = function() {
        var numPages = 0;
        if (this.items != null && this.itemsPerPage != null) {
            numPages = Math.ceil(this.items.length / this.itemsPerPage);
        }
        
        return numPages;
    };

    this.showPage = function(page) {
        this.currentPage = page;
        
    	//get the element number where to start the slice from
    	startFrom = (page -1) * this.itemsPerPage;

    	//get the element number where to end the slice
    	endOn = startFrom + this.itemsPerPage;
    	
        //hide all children elements of content div, get specific items and show them  
        $(this.pagingContainerPath).children().css('display', 'none').slice(startFrom, endOn).css('display', 'block');  
        
        renderControls(this.pagingControlsContainer, this.currentPage, this.numPages());
        $(this.scrollContainerPath).animate({scrollTop:0});
    };

    var renderControls = function(container, currentPage, numPages) {
        var pagingControls = 'Page: <ul>';
        for (var i = 1; i <= numPages; i++) {
            if (i != currentPage) {
                pagingControls += '<li><a href="#" onclick="pager.showPage(' + i + '); closeInfoWindow(); return false;">' + i + '</a></li>';
            } else {
                pagingControls += '<li>' + i + '</li>';
            }
            
            if (i % 10 == 0) {
               pagingControls += '<br>';
            }
        }

        pagingControls += '</ul>';

        $(container).html(pagingControls);
    };
};

/* for embedded maps */

function googleMap(type, ids) {
	var url = "";
	var payload = "";
	
	if (type === "agent") {
		url = "/nylife/dispatcher/searchAgentsByAgentIds";
		payload = "agentIds="+ids;
	} else if (type === "office") {
		url = "/nylife/dispatcher/searchOfficesByOfficeCodes";
		payload = "officeCodes="+ids;
	}
	
	$.ajax({
		type : "POST",
		url : url,
		dataType : "html",
		data : payload,
		success : function(data) {
			$("#embedMap").append(data);
			initialize();
			pager = new nyl.Pager();
		    pager.itemsPerPage = 5; // set amount elements per page
		    pager.pagingContainerPath = '#agent-info-container';
		    pager.scrollContainerPath = '#agent-panel';
		    pager.items = $('div.agent-info', pager.pagingContainer); // set of required containers
		    pager.showPage(1);
		},
		error : function(request, status, error) {
			console.log("Error: Please try again later");
		}
	});
}

function getDrivingDirectionsValue() {
	return $("#driving-directions").val();
}