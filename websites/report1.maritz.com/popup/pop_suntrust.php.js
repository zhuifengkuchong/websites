// https | https://www.suntrust.com/ | / | www.suntrust.com https://www.suntrust.com/
/*jslint browser:true */
function setMaritzCookie(cookieName, cookieValue, cookieDays, showalert) {
    "use strict";
    var today = new Date(),
        expire = new Date(),
        name = cookieName,
        value = cookieValue;
    expire.setTime(today.getTime() + 3600000 * 24 * cookieDays);
    document.cookie = name + "=" + escape(value) + ";expires=" + expire.toGMTString() + ";path=/";
}

function findCookie(c_name) {
    "use strict";
    if (navigator.cookieEnabled) {
        var cookieData = new String(document.cookie),
            cookieHeader = c_name,
            cookieStart = cookieData.indexOf(cookieHeader) + cookieHeader.length,
            cookieEnd = cookieData.indexOf(";", cookieStart);
        if (cookieData.indexOf(cookieHeader) === -1) {
            return false;
        } else {
            return true;
        }
    }
}

function setDelayCookie() {
    "use strict";
    var cookieDate = new Date(),
        name = "st_keepalive",
        value = "yes";
    cookieDate.setTime(cookieDate.getTime() + 1000 * 10);
    document.cookie = name + "=" + escape(value) + ";expires=" + cookieDate.toGMTString() + ";path=/";
}

function setCountCookies() {
    "use strict";
    if ((findCookie('st_keepalive')) || (findCookie('st_invpopped'))) {
        var invitePopped = 'y';
        setDelayCookie();
    } else {
        var invitePopped = 'n';
    }
    var t,
        popInvite;
    t = setInterval(function(){
      if (invitePopped == 'y') {
        setDelayCookie();
      }
          },5000);
    
}


setCountCookies();
