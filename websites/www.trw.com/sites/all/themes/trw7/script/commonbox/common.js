// $Id$

/**
 * @file
 * #1 search
 * #2 Language switcher
 * @author: Deepak Behera https://drupal.org/user/2423068/
 *
 */
	
jQuery(document).ready(function($){
	
	var loc = window.location; 
	var base_url = loc.protocol + "//" + loc.host + "/" + loc.pathname.split('/')[0]; 
	var sub_url = ''; 
	var go_to_url = '';
	
	$("#search-submit").click(function(){
		var key= jQuery('input:text[name=search-field]').val(); 
		if(key == "Search ...")
			{
				alert("Please enter a valid text");
			}
		else
			{	
				var new_url=base_url+"/search/node/"+key;
				window.location= new_url;
			}
	});
	
	$("#searchData").keypress(function(event) {
	    if (event.which == 13) {
	        event.preventDefault();
			
			var key= jQuery('input:text[name=search-field]').val(); 
			
			if(key == "Search ...")
				{
					alert("Please enter a valid Text");
				}
			else
				{	
					 var new_url=base_url+"/search/node/"+key;
					 window.location= new_url;
				}
	        
	    }
	
		
	});
 
	$(window).load(function(){
		$('#searchData').attr('value','Search ...');
	});
	 
	jQuery("#searchData").focusout(function(){
		if($(this).val() == '')
			{
				$(this).val("Search ...");
			}
	
	});
	 	
	$("#searchData").click(function(){
		$(this).attr('value','');
	});	
	
	/* Language Switcher */
//	$("#selection").change(function(){
//		 sub_url = window.location.pathname;
//	     go_to_url = $(this).find(":selected").val();
//	     document.location.href = go_to_url+sub_url;
//      });
   
   /* Image Slider- setting for 2 images & 1 Image */   
   var visiblelist = $('#gi-liquidcarousel div ul li').size();
   if(visiblelist == 1){
	   	$('#gi-liquidcarousel span[class ="previous"]').hide();
		$('#gi-liquidcarousel span[class ="next"]').hide();
	   	$('#gi-liquidcarousel div ul li img').css('margin-left','27px');
		$('.small-slider').css('height','190px');
		$('#gi-liquidcarousel div ul li img ').css('height','170px');
		$('#gi-liquidcarousel div ul li img ').css('width','130%');
		$('#gi-liquidcarousel div ul li img ').css('margin-top','2px');
   }
   
   else if(visiblelist == 2){
	    $('#gi-liquidcarousel span[class ="previous"]').hide();
		$('#gi-liquidcarousel span[class ="next"]').hide();
	   	$('#gi-liquidcarousel div ul li').css('margin-left','52px');
		$('#gi-liquidcarousel div ul li').css('margin-right','72px');
		$('.small-slider').css('height','190px');
		$('#gi-liquidcarousel div ul li img ').css('height','170px');
		$('#gi-liquidcarousel div ul li img ').css('width','130%');
		$('#gi-liquidcarousel div ul li img ').css('margin-top','2px');	
   }
   
   else if(visiblelist == 3){
	   	$('#gi-liquidcarousel span[class ="previous"]').hide();
		$('#gi-liquidcarousel span[class ="next"]').hide();
	   	$('#gi-liquidcarousel div ul li img ').css('margin-left','27px');
		
   }
   if(visiblelist >= 3){
   		$('#gi-liquidcarousel').liquidcarousel({height:129, duration:100, hidearrows:false});   	
   }
   
	
});	 
