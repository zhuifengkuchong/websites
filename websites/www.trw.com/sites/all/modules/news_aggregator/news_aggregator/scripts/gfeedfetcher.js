/**
 * Plugin: jquery Ajax.Google RSSFeed
 * 
 * Version: 1.1.2
 * (c) Copyright 2012-2017, HCL Ltd
 * 
 * Description: jQuery plugin for display of RSS feeds via Google Feed API
 *              (Based on original plugin jGFeed by jQuery HowTo. Filesize function by Deepak B.)
 * 
 * History:
 * gAjax RSS Feeds Displayer- By Dynamic Drive, available at: http://www.dynamicdrive.com
 * Created: August 7th, 2012
 * Updated : Fixed issue in IE where labels would sometimes be associated with the incorrect feed items 
 * URL: http://trw.mediaroom.com/index.php?s=43&pagetemplate=rss
 **/

var gfeedfetcher_loading_image="indicator.gif"/*tpa=http://www.trw.com/sites//all//modules//news_aggregator//news_aggregator//scripts//indicator.gif*/ //Full URL to "loading" image. No need to config after this line!!

google.load("feeds", "1") //Load Google Ajax Feed API (version 1)


/*Drupal.behaviors.news_aggregator = function() {
 	var settings = Drupal.settings.newsAggregator; 
	 var newsfeed=new gfeedfetcher("rssfeeds", "rssfeedsclass", "_new")
	//newsfeed.addFeed("JQUERY BLOG", "http://jquery4u.com/rss/");
	newsfeed.addFeed("BLOGOOLA's Blog", "http://trw.mediaroom.com/index.php?s=43&pagetemplate=rss");
	newsfeed.displayoptions("label datetime snippet");
	newsfeed.setentrycontainer("p");
	newsfeed.filterfeed(10, "date");
	newsfeed.init();
}*/


function gfeedfetcher(divid, divClass, linktarget){ 
	this.linktarget=linktarget || "" //link target of RSS entries
	this.feedlabels=[] //array holding lables for each RSS feed
	this.feedurls=[]
	this.feedurl="";
	this.feeds=[] //array holding combined RSS feeds' entries from Feed API (result.feed.entries)
	this.feedsfetched=0 //number of feeds fetched
	this.feedlimit=5
	this.showoptions="" //Optional components of RSS entry to show (none by default)
	this.sortstring="date" //sort by "date" by default
	document.write('<div id="'+divid+'" class="'+divClass+'"></div>') //output div to contain RSS entries
	this.feedcontainer=document.getElementById(divid)
	this.itemcontainer="<li>" //default element wrapping around each RSS entry item
	this.region = 'news_aggregator';
}

gfeedfetcher.prototype.addFeed=function(label, url, path){
	this.feedlabels[this.feedlabels.length]=label
	this.feedurls[this.feedurls.length]=url
	this.feedurl = url;
	this.path = path;
}

gfeedfetcher.prototype.filterfeed=function(feedlimit, sortstr){
	this.feedlimit=feedlimit
	if (typeof sortstr!="undefined")
	this.sortstring=sortstr
}

//Added by HCL Dev. for determining the region for the feeds
gfeedfetcher.prototype.regiontype=function(region){
	this.region=region
}
gfeedfetcher.prototype.displayoptions=function(parts, image){
	this.showoptions=parts //set RSS entry options to show ("date, datetime, time, snippet, label, description")	
	this.image=image // set random image
}

gfeedfetcher.prototype.setentrycontainer=function(containerstr){  //set element that should wrap around each RSS entry item
this.itemcontainer="<"+containerstr.toLowerCase()+">"
}

gfeedfetcher.prototype.init=function(){
	this.feedsfetched=0 //reset number of feeds fetched to 0 (in case init() is called more than once)
	this.feeds=[] //reset feeds[] array to empty (in case init() is called more than once)
	this.feedcontainer.innerHTML='<p><img src="'+this.path+gfeedfetcher_loading_image+'" /> Retrieving RSS feed(s)</p>'
	var displayer=this
	for (var i=0; i<this.feedurls.length; i++){ //loop through the specified RSS feeds' URLs
		//create new instance of Google Ajax Feed API
		//Also add a unique parameter top prevent Google from caching the RSS feed
		var feedpointer=new google.feeds.Feed(this.feedurls[i]+"?t="+new Date().getTime())
		feedpointer.setResultFormat(google.feeds.Feed.MIXED_FORMAT); //Set Mixed Mode
		var items_to_show=(this.feedlimit<=this.feedurls.length)? 1 : Math.floor(this.feedlimit/this.feedurls.length) //Calculate # of entries to show for each RSS feed
		if (this.feedlimit%this.feedurls.length>0 && this.feedlimit>this.feedurls.length && i==this.feedurls.length-1) //If this is the last RSS feed, and feedlimit/feedurls.length yields a remainder
			items_to_show+=(this.feedlimit%this.feedurls.length) //Add that remainder to the number of entries to show for last RSS feed
		feedpointer.setNumEntries(items_to_show) //set number of items to display
		feedpointer.load(function(label){
			return function(r){
				displayer._fetch_data_as_array(r, label)
			}
		}(this.feedlabels[i])) //call Feed.load() to retrieve and output RSS feed.
	}
}

gfeedfetcher._formatdate=function(datestr, showoptions){
	var itemdate=new Date(datestr)
	/*var parseddate=(showoptions.indexOf("datetime")!=-1)? itemdate.toLocaleString() : (showoptions.indexOf("date")!=-1)? itemdate.toLocaleDateString() : (showoptions.indexOf("time")!=-1)? itemdate.toLocaleTimeString() : ""*/
	
	var parseddate = dateFormat(datestr, "mmmm dS, yyyy")
	
	return "<span class='datefield'>"+parseddate+"</span>"
}

gfeedfetcher._sortarray=function(arr, sortstr){
	var sortstr=(sortstr=="label")? "ddlabel" : sortstr //change "label" string (if entered) to "ddlabel" instead, for internal use
	if (sortstr=="title" || sortstr=="ddlabel"){ //sort array by "title" or "ddlabel" property of RSS feed entries[]
		arr.sort(function(a,b){
		var fielda=a[sortstr].toLowerCase()
		var fieldb=b[sortstr].toLowerCase()
		return (fielda<fieldb)? -1 : (fielda>fieldb)? 1 : 0
		})
	}
	else{ //else, sort by "publishedDate" property (using error handling, as "publishedDate" may not be a valid date str if an error has occured while getting feed
		try{
			arr.sort(function(a,b){return new Date(b.publishedDate)-new Date(a.publishedDate)})
		}
		catch(err){}
	}
}

jQuery(document).ready(function(){
	jQuery.fn.ns_filter = function(namespaceURI, localName) {
	  return jQuery(this).filter(function() {
	   var domnode = jQuery(this)[0];
	   return (domnode.namespaceURI == namespaceURI && domnode.localName == localName);
	  });
	 };
});


gfeedfetcher.prototype._fetch_data_as_array=function(result, ddlabel){
  	<!--mp_trans_process_disable_start--> 
		var region = this.region;
		var urlparts = this.feedurl.split('/');
		
		if(region == 'trw_safety_latest'){
			var items = result.xmlDocument.getElementsByTagName('item'); // Retreived all the items from xml document
		}
		
		var thisfeed=(!result.error)? result.feed.entries : "" //get all feed entries as a JSON array or "" if failed
		if (thisfeed==""){ //if error has occured fetching feed
			alert("Some blog posts could not be loaded: "+result.error.message)
		}
		for (var i=0; i<thisfeed.length; i++){ //For each entry within feed
			//alert(region);
			if(region == 'trw_safety_latest'){
				console.log((jQuery(items[i]).find('*').ns_filter('http://search.yahoo.com/mrss/', 'thumbnail').first().attr("url")));
				thumb_url = (jQuery(items[i]).find('*').ns_filter('http://search.yahoo.com/mrss/', 'thumbnail').first().attr("url")).split("://");
				result.feed.entries[i].thumb_url='http://'+thumb_url[1];
			}
			result.feed.entries[i].ddlabel=ddlabel //extend it with a "ddlabel" property
		}
		this.feeds=this.feeds.concat(thisfeed) //add entry to array holding all feed entries
		this._signaldownloadcomplete() //signal the retrieval of this feed as complete (and move on to next one if defined)
	<!--mp_trans_process_disable_end-->
}

gfeedfetcher.prototype._signaldownloadcomplete=function(){
	this.feedsfetched+=1
	if (this.feedsfetched==this.feedurls.length) //if all feeds fetched
		this._displayresult(this.feeds) //display results	
		
}


function do_carousel() {
	  var settings= {'jcarousel-dom-4':{"view_options":{"view_args":"","view_path":"node","view_base_path":null,"view_display_id":"block","view_name":"tabbed_slider_safety_sync","jcarousel_dom_id":1},"skin":"default","autoPause":1,"start":1,"selector":".jcarousel-dom-4"}};
	  jQuery.each(settings, function(key, options) {
		var $carousel = jQuery(options.selector + ':not(.jcarousel-processed)');

	    // If this carousel has already been processed or doesn't exist, move on.
	    if (!$carousel.length) {
	      return;
	    }
	    
	    // Callbacks need to be converted from a string to an actual function.
	    jQuery.each(options, function(optionKey) {
	      if (optionKey.match(/Callback$/) && typeof options[optionKey] == 'string') {
	        var callbackFunction = window;
	        var callbackParents = options[optionKey].split('.');
	        jQuery.each(callbackParents, function(objectParent) {
	          callbackFunction = callbackFunction[callbackParents[objectParent]];
	        });
	        options[optionKey] = callbackFunction;
	      }
	    });
	    
	    // Add standard options required for AJAX functionality.
	    if (options.ajax && !options.itemLoadCallback) {
	      options.itemLoadCallback = Drupal.jcarousel.ajaxLoadCallback;
	    }

	    // If auto-scrolling, pause animation when hoving over the carousel.
	    if (options.auto && options.autoPause && !options.initCallback) {
	      options.initCallback = function(carousel, state) {
	        Drupal.jcarousel.autoPauseCallback(carousel, state);
	      };
	    }
	    
	    // Add navigation to the carousel if enabled.
	    if (!options.setupCallback) {
	      options.setupCallback = function(carousel) {
	        Drupal.jcarousel.setupCarousel(carousel);
	        if (options.navigation) {
	          Drupal.jcarousel.addNavigation(carousel, options.navigation);
	        }
	      };
	      if (options.navigation && !options.itemVisibleInCallback) {
	        options.itemLastInCallback = {
	          onAfterAnimation: Drupal.jcarousel.updateNavigationActive
	        };
	      }
	    }

	    if (!options.hasOwnProperty('buttonNextHTML') && !options.hasOwnProperty('buttonPrevHTML')) {
	      options.buttonNextHTML = Drupal.theme('jCarouselButton', 'next');
	      options.buttonPrevHTML = Drupal.theme('jCarouselButton', 'previous');
	    }
	    
	    // Initialize the jcarousel.
	    $carousel.addClass('jcarousel-processed').jcarousel(options);
	  });
}

gfeedfetcher.prototype._displayresult=function(feeds){	//console.log(feeds)
	var rssoutput = "";	
	var region = this.region;
	var urlparts = this.feedurl.split('/');
	gfeedfetcher._sortarray(feeds, this.sortstring)
	
	if(region === 'news_aggregator'){
		rssoutput+= "<div class=\"box\">"		
			/*** Hide rss link, date and author on left box
			var itemnewsrss = "<ol><li><a href=\"" + this.feedurl + "\" target=\"" + this.linktarget + "\" class=\"rss\"></a>"			
			var itemnewsdate="<span>"+gfeedfetcher._formatdate(feeds[0].publishedDate, this.showoptions)+"</span></li>"		
			var itemnewsauthor=/label/i.test(this.showoptions)? "<li>"+this.feeds[0].ddlabel+"</li></ol>" : " "	
			***/		
			var itemnewstitle="<h3>" + feeds[0].title +"</h3>"		
			var feed_desc = feeds[0].content; 
			feed_desc = feed_desc.substring(0,200) + '. . . . .';
			var itemdescription=/description/i.test(this.showoptions)? "<p>"+feed_desc+"</p>" : /snippet/i.test(this.showoptions)? "<p>"+feed_desc+"</p>" : ""			
			//var itemdescription=/description/i.test(this.showoptions)? "<p>"+feeds[0].content+"</p>" : /snippet/i.test(this.showoptions)? "<p>"+feeds[0].content+"</p>" : ""		
			var image = /image/i.test(this.showoptions)? "<td>"+this.image+"</td>" : ""	
		//Don't display rss link, date and author
		//rssoutput+= itemnewsrss + " " + itemnewsdate + " " + itemnewsauthor + " " + itemnewstitle + " " + itemdescription + "\n\n"		
		rssoutput+= itemnewstitle + " " + itemdescription + "\n\n"		
		rssoutput+= "<p><a href=\"" + feeds[0].link + "\" target=\"" + this.linktarget + "\"  >...More &gt;</a></p>"	
		rssoutput+= image + "</div>"		
		rssoutput+= "<div class=\"box stories\"><a href=\"" + this.feedurl + "\" target=\"" + this.linktarget + "\" class=\"rss\"></a><h6>RECENT STORIES</h6>"	
		for (var i=1; i<feeds.length; i++){			
			var itemtitle="<div class = \"story\"><div class = \"story-title\"><a rel=\"nofollow\" href=\"" + feeds[i].link + "\" target=\"" + this.linktarget + "\" class=\"titlefield\"><p>" + feeds[i].title +"</p></a></div>"			
			var itemrss = "<ol><li>"			
			var itemdate="<span>"+gfeedfetcher._formatdate(feeds[i].publishedDate, this.showoptions)+"</span></li>"			
			var itemauthor=/label/i.test(this.showoptions)? "<li>"+this.feeds[i].ddlabel+"</li></ol></div>" : " "		
			rssoutput+= itemtitle + " " + itemrss + " " + itemdate + " " + itemauthor + "\n\n"
		}	
		rssoutput+= "<p><a href=\"" + urlparts[0] + "//" + urlparts[2] + "\" target=\"" + this.linktarget + "\"  >...More &gt;</a></p>"
		rssoutput+= "</div>";
		this.feedcontainer.innerHTML=rssoutput;
	}
	else if(region === 'trw_safety_latest'){
		rssoutput+= '<div class="view view-tabbed-slider-safety-sync view-id-tabbed_slider_safety_sync view-display-id-block view-dom-id-8">';
		rssoutput+= '<div class="view-content">';
		rssoutput+= '<ul class="jcarousel jcarousel-view--tabbed-slider-safety-extra--block jcarousel-dom-4 jcarousel-skin-default">';
		for (var i=0; i<feeds.length; i++){
			var elem ;
			if(i % 2 == 0){
				elem = "odd"
			}
			else{
				elem = "even"
			}
			//alert(feeds[i].thumb_url);
			var itemli = '<li class="jcarousel-item-'+(i+1)+' '+elem+'">';
			href_whole = feeds[i].link.split('?')[0] + '?utm_source=mainTRW&utm_medium=web&utm_campaign=Safety_News';
			var itemtitle = '<a target="_blank" style="text-decoration: none;" href="'+href_whole+'"><div><div class="tab-slider-title">'+ feeds[i].title +'</div></div>';
			var itemimage = '<div><img height="140px" width="250px" src='+ feeds[i].thumb_url +' /></div>';
			rssoutput += itemli + itemimage + itemtitle + '</a></li>';
		}
		rssoutput += '</ul>';
		rssoutput += '</div>';
		rssoutput += '</div>';
		this.feedcontainer.innerHTML=rssoutput;
		do_carousel();
	}
}


/*gfeedfetcher.prototype._displayresult=function(feeds){
	var rssoutput=(this.itemcontainer=="<li>")? "<ul>\n" : ""
	gfeedfetcher._sortarray(feeds, this.sortstring)
	for (var i=0; i<feeds.length; i++){
		var itemtitle="<a rel=\"nofollow\" href=\"" + feeds[i].link + "\" target=\"" + this.linktarget + "\" class=\"titlefield\">" + feeds[i].title + "</a>"
		var itemlabel=/label/i.test(this.showoptions)? '<span class="labelfield">['+this.feeds[i].ddlabel+']</span>' : " "
		var itemdate=gfeedfetcher._formatdate(feeds[i].publishedDate, this.showoptions)
		var itemdescription=/description/i.test(this.showoptions)? "<br />"+feeds[i].content : /snippet/i.test(this.showoptions)? "<br />"+feeds[i].contentSnippet  : ""
		rssoutput+=this.itemcontainer + itemtitle + " " + itemlabel + " " + itemdate + "\n" + itemdescription + this.itemcontainer.replace("<", "</") + "\n\n"
	}
	rssoutput+=(this.itemcontainer=="<li>")? "</ul>" : ""
	this.feedcontainer.innerHTML=rssoutput
}*/



/*
 * Date Format 1.2.3
 * (c) 2007-2009 Steven Levithan <stevenlevithan.com>
 * MIT license
 *
 * Includes enhancements by Scott Trenda <scott.trenda.net>
 * and Kris Kowal <cixar.com/~kris.kowal/>
 *
 * Accepts a date, a mask, or a date and a mask.
 * Returns a formatted version of the given date.
 * The date defaults to the current date/time.
 * The mask defaults to dateFormat.masks.default.
 var now = new Date();

now.format("m/dd/yy");
// Returns, e.g., 6/09/07

// Can also be used as a standalone function
dateFormat(now, "dddd, mmmm dS, yyyy, h:MM:ss TT");
// Saturday, June 9th, 2007, 5:46:21 PM

// You can use one of several named masks
now.format("isoDateTime");
// 2007-06-09T17:46:21

// ...Or add your own
dateFormat.masks.hammerTime = 'HH:MM! "Can\'t touch this!"';
now.format("hammerTime");
// 17:46! Can't touch this!

// When using the standalone dateFormat function,
// you can also provide the date as a string
dateFormat("Jun 9 2007", "fullDate");
// Saturday, June 9, 2007

// Note that if you don't include the mask argument,
// dateFormat.masks.default is used
now.format();
// Sat Jun 09 2007 17:46:21

// And if you don't include the date argument,
// the current date and time is used
dateFormat();
// Sat Jun 09 2007 17:46:22

// You can also skip the date argument (as long as your mask doesn't
// contain any numbers), in which case the current date/time is used
dateFormat("longTime");
// 5:46:22 PM EST

// And finally, you can convert local time to UTC time. Either pass in
// true as an additional argument (no argument skipping allowed in this case):
dateFormat(now, "longTime", true);
now.format("longTime", true);
// Both lines return, e.g., 10:46:21 PM UTC

// ...Or add the prefix "UTC:" to your mask.
now.format("UTC:h:MM:ss TT Z");
// 10:46:21 PM UTC
 
 
 */

var dateFormat = function () {
	var	token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
		timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
		timezoneClip = /[^-+\dA-Z]/g,
		pad = function (val, len) {
			val = String(val);
			len = len || 2;
			while (val.length < len) val = "0" + val;
			return val;
		};

	// Regexes and supporting functions are cached through closure
	return function (date, mask, utc) {
		var dF = dateFormat;

		// You can't provide utc if you skip other args (use the "UTC:" mask prefix)
		if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
			mask = date;
			date = undefined;
		}

		// Passing date through Date applies Date.parse, if necessary
		date = date ? new Date(date) : new Date;
		if (isNaN(date)) throw SyntaxError("invalid date");

		mask = String(dF.masks[mask] || mask || dF.masks["default"]);

		// Allow setting the utc argument via the mask
		if (mask.slice(0, 4) == "UTC:") {
			mask = mask.slice(4);
			utc = true;
		}

		var	_ = utc ? "getUTC" : "get",
			d = date[_ + "Date"](),
			D = date[_ + "Day"](),
			m = date[_ + "Month"](),
			y = date[_ + "FullYear"](),
			H = date[_ + "Hours"](),
			M = date[_ + "Minutes"](),
			s = date[_ + "Seconds"](),
			L = date[_ + "Milliseconds"](),
			o = utc ? 0 : date.getTimezoneOffset(),
			flags = {
				d:    d,
				dd:   pad(d),
				ddd:  dF.i18n.dayNames[D],
				dddd: dF.i18n.dayNames[D + 7],
				m:    m + 1,
				mm:   pad(m + 1),
				mmm:  dF.i18n.monthNames[m],
				mmmm: dF.i18n.monthNames[m + 12],
				yy:   String(y).slice(2),
				yyyy: y,
				h:    H % 12 || 12,
				hh:   pad(H % 12 || 12),
				H:    H,
				HH:   pad(H),
				M:    M,
				MM:   pad(M),
				s:    s,
				ss:   pad(s),
				l:    pad(L, 3),
				L:    pad(L > 99 ? Math.round(L / 10) : L),
				t:    H < 12 ? "a"  : "p",
				tt:   H < 12 ? "am" : "pm",
				T:    H < 12 ? "A"  : "P",
				TT:   H < 12 ? "AM" : "PM",
				Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
				o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
				S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
			};

		return mask.replace(token, function ($0) {
			return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
		});
	};
}();

// Some common format strings
dateFormat.masks = {
	"default":      "ddd mmm dd yyyy HH:MM:ss",
	shortDate:      "m/d/yy",
	mediumDate:     "mmm d, yyyy",
	longDate:       "mmmm d, yyyy",
	fullDate:       "dddd, mmmm d, yyyy",
	shortTime:      "h:MM TT",
	mediumTime:     "h:MM:ss TT",
	longTime:       "h:MM:ss TT Z",
	isoDate:        "yyyy-mm-dd",
	isoTime:        "HH:MM:ss",
	isoDateTime:    "yyyy-mm-dd'T'HH:MM:ss",
	isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Internationalization strings
dateFormat.i18n = {
	dayNames: [
		"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
		"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
	],
	monthNames: [
		"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
		"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
	]
};

// For convenience...
Date.prototype.format = function (mask, utc) {
	return dateFormat(this, mask, utc);
};


