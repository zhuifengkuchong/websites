//Show hide layers
    function toggle_visibility(id) {
       var e = document.getElementById(id);
       if (e.style.display == 'block') {
           e.style.display = 'none';           
           $("#"+id).attr('style', 'display: none !important');   
                   
       }
       else {
           e.style.display = 'block';
           $("#" + id).attr('style', 'display: block !important');           
       }
   }
  
//Columns Resize   
    function getDivHeight(modelDivId, destDivId, pixelsToAdd) {
        var centerHeight = $("#" + modelDivId).height();
        var destHeight = $("#" + destDivId).height();
        if (destHeight < centerHeight || pixelsToAdd != 0)
            $("#" + destDivId).height(centerHeight + pixelsToAdd);

    }

    