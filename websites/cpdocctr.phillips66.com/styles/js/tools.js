function runSearchApp2(evt)
{
if (!evt) var evt = window.event;
if (evt.keyCode == 13)
{
	runSearchApp();
	evt.returnValue = false;
}
}

function runSearchApp() {
	var qry,collection,newqry,sUrl,cmsPath;
	//qry=document.all["q1"].value;
	qry=document.getElementById("queryText").value;
	if (document.getElementById("collection")==null)
		collection="global";		
	else
		collection=document.getElementById("collection").value;
		
	if (document.getElementById("cmsPath")==null)
		cmsPath="/channels/www.conocophillips.com/utilities";		
	else
		cmsPath=document.getElementById("cmsPath").value;
		
	re = / /gi
	newqry = qry.replace(re,"+")
	
	//sUrl = "http://www.search.conoco.com/conocophillips/SearchResults.asp?ct=cop&c1=@all&image.x=16&image.y=11&q1="+newqry;
	sUrl = "http://w3.conocophillips.com/search/search.aspx?Query="+newqry+"&Collection="+collection+"&cmsPath="+cmsPath;
	if (qry=="" || qry==null) 
	{
		alert("Please fill in a search string");
	}
	else 
	{
		window.location.href = sUrl;
	}
}

// Create a cookie that stores the client TimeZone offset
var IDS_TIMEZONE_OFFSET = "CMS_TimeZoneOffset";
var currentTime = new Date();
document.cookie = IDS_TIMEZONE_OFFSET + "=" + currentTime.getTimezoneOffset() + "; path=/";


// Array for UI display (this is not UI-culture aware)
var ARRAY_MONTH_NAME = new Array ("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");

//	Write date in local time
function WBC_writeUTC2Local(strDate)
{
	var pDate = new Date(strDate);
	WBC_writeDate(WBC_convertUTC2Local(pDate));
}

//	Convert GMT time to local time
function WBC_convertUTC2Local(pDateUTC)
{
	return new Date(pDateUTC.getTime() - pDateUTC.getTimezoneOffset() * 60 * 1000);
}

//	Convert Local time to GMT time
function WBC_convertLocal2UTC(pDateLocal)
{
	return new Date(pDateLocal.getTime() + pDateLocal.getTimezoneOffset() * 60 * 1000);
}

//	Return a date string in the "MMM DD YYYY, hh:mm:ss am/pm" or 24 hour "MMM DD YYYY, hh:mm:ss" format
function WBC_formatDateToString( pDate, bAmPm )
{
	if (bAmPm)
	{
		var nHours =  pDate.getHours();
		var strAmPm;
		if (nHours == 0) {
			nHours = 12;
			strAmPm = "am";
		} else if (nHours == 12) {
			strAmPm = "pm";
		} else if (nHours > 12) {
			nHours = nHours - 12;
			strAmPm = "pm";
		} else {
			strAmPm = "am";
		}

		return ( ARRAY_MONTH_NAME[pDate.getMonth()] + " " + WBC_addLeadingZero(pDate.getDate()) + " " + WBC_fixJavaScriptYear(pDate.getFullYear()) + ", " + WBC_addLeadingZero(nHours) + ":" + WBC_addLeadingZero(pDate.getMinutes()) + ":" + WBC_addLeadingZero(pDate.getSeconds()) + " " + strAmPm );
	}
	else
	{
		return (ARRAY_MONTH_NAME[pDate.getMonth()] + " " + WBC_addLeadingZero(pDate.getDate()) + " " + WBC_fixJavaScriptYear(pDate.getFullYear()) + " " + WBC_addLeadingZero(pDate.getHours()) + ":" + WBC_addLeadingZero(pDate.getMinutes()) + ":" + WBC_addLeadingZero(pDate.getSeconds()));
	}
}


//	Write date using document.write()
function WBC_writeDate(pDate) 
{
	document.write( WBC_formatDateToString( pDate, true ) );
}

// returns an integer value representing the number of 
// milliseconds between midnight, January 1, 1970 and the time value in the Date object
function WBC_getTime( strDate ) 
{
	var pDate = new Date( strDate );
	return pDate.getTime();
}

// Add leading zero to number less than 10
function WBC_addLeadingZero(nNum)
{
	if (nNum < 10) 
	{
		return "0" + nNum;
	}
	else 
	{
		return nNum;
	}
}

// Returns correct year for any year after 1000
function WBC_fixJavaScriptYear( nYear )
{
	if (nYear < 1000) 
	{
		return nYear + 1900;
	}
	else 
	{
		return nYear;
	}
}

function setCookie(name, value) {
	saveCookie(name,value,3650);
	window.location.reload();
}

function saveCookie(name,value,days) {
	if (days) {
		var date=new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires="; expires="+date.toGMTString();
	} 
	else expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
	var nameEQ=name+"=";
	var ca=document.cookie.split(';');
	for(var i=0;i<ca.length;i++) {
		var c=ca[i];
		while (c.charAt(0)==' ') c=c.substring(1,c.length);
		if (c.indexOf(nameEQ)==0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

// ----------------------------------------------------------------------------
//
// Deprecated functions; retained for backward compatibility with Version 3.x
//
// ----------------------------------------------------------------------------

function WBC_formatDate( pDate )
{
	return WBC_formatDateToString( pDate, false );
}

function WBC_get4DigitsYear( nYear )
{
	return WBC_fixJavaScriptYear( nYear );
}

function openWindow(sUrl, dialog,width,height) {
	if (typeof myWin != 'undefined') 	{
		if (myWin) {
			try {myWin.close()} catch (er) {;}
		}
	}
	if (dialog) {
		if (typeof width == 'undefined') {
			tStr="width=640";
		} else {
			tStr="width="+width;
		}
		if (typeof height == 'undefined') {
			tStr=tStr +",height=480";
		} else {
			tStr=tStr +",height="+height;
		}
		tStr=tStr+",status=no,toolbar=no,menubar=no,scrollbars=yes,resizable=yes";
		myWin= open(sUrl, "myWin", tStr);
		myWin.focus();
	} else {
	window.open(sUrl);
	}
}
// ----------------------------------------------------------------------------
//
// Function for SharePoint drag drop
//
// ----------------------------------------------------------------------------
function MSOLayout_GetRealOffset(StartingObject, OffsetType, EndParent)            
	{                
	    var realValue=0;                
	    if (!EndParent) EndParent=document.body;                
	    for (var currentObject=StartingObject; 
	    currentObject && currentObject !=EndParent && currentObject != document.body; 
	    currentObject=currentObject.offsetParent)                
	    {                    
	        var offset = eval('currentObject.offset'+OffsetType);                    
	        if (offset) realValue+=offset;                
	    }                
	    return realValue;            
	}
	
	
function MSOLayout_MakeInvisibleIfEmpty()
{
	var allElements=document.getElementsByName("_invisibleIfEmpty");
	var agt=navigator.userAgent.toLowerCase();
	var isNav=((agt.indexOf('mozilla')!=-1)&&((agt.indexOf('spoofer')==-1) && (agt.indexOf('compatible')==-1)));
	var isIE=(agt.indexOf("msie")!=-1);
	for (var curElement=0; curElement < allElements.length; curElement++)
	{
		if ((isIE && allElements[curElement].childNodes.length==0)
			|| (isNav && allElements[curElement].childNodes.length <=1))
		{
			allElements[curElement].style.display="none";
		}
	}
}
