/*!
 * AECOM.COM google custom search V1 JavaScript Top Navigation
 * Author: Saurabh Pandit
 * Date: 8/29/2012
 * Pages included: aecomGlobalTools.jsp
 */	
	function OnLoadTop() {
	
		var linkURL = "/Internet/Search";
		var transTitle = "";
		var page_language = "";
		if(document.getElementById("translatedTitle") != null){
			transTitle = document.getElementById("translatedTitle").value;
		}
		if(document.getElementById("languagehoice") != null){
			page_language =  document.getElementById("languagehoice").value;
		}
		 
		//Defaulted to English
		var lr = 'lang_en';
		var hl = 'en';

		//Simplified Chinese
		if(page_language == 'zh_CN'){
			lr = 'lang_zh-CN';
			hl = 'zh-CN';
		} 
		//Traditional Chinese
		else if(page_language == 'zh_TW'){
			lr = 'lang_zh-TW';
			hl = 'zh-TW';
		} 
		//Spanish
		else if(page_language == 'es_ES'){
			lr = 'lang_es';
			hl = 'es';
		}
		//Russian
		else if(page_language == 'ru_RU'){
			lr = 'lang_ru';
			hl = 'ru';
		}
		//French
		else if(page_language == 'fr_CA'){
			lr = 'lang_fr';
			hl = 'fr';
		}
		
		var cseOptionsTop = {};  
		cseOptionsTop[google.search.Search.RESTRICT_EXTENDED_ARGS] = {
			'lr': lr,     		//Language Restrict
			'hl': hl,     		//Host(Interface) Language
			'c2coff' : '1'		//'1' = Do Not translate Traditional to Simplified Chinese in the search results.
	    	};
		var cseControlTop = new google.search.CustomSearchControl(
			'002257838092167024381:glb5ws_8tqq', cseOptionsTop);
		cseControlTop.setResultSetSize(google.search.Search.FILTERED_CSE_RESULTSET);
		var optionsTop = new google.search.DrawOptions();
		optionsTop.setSearchFormRoot('searchTop');
		optionsTop.setAutoComplete(true);
		optionsTop.enableSearchboxOnly(linkURL,'s',false,'?');
		cseControlTop.draw('searchTop', optionsTop);
		
		//Change Search Button Text to the Langauge on the page.
		var myTable = document.getElementById('searchTop');
		if(myTable != null){
			var nodes = myTable.getElementsByTagName('input');
			for(j = 0;j < nodes.length; j++){
				if(nodes[j].className == 'gsc-search-button'){
					nodes[j].value = '';
					nodes[j].title = transTitle;
				}
				else if(nodes[j].className == ' gsc-input'){
					nodes[j].title = transTitle;
				}
			}		
		}
  	}