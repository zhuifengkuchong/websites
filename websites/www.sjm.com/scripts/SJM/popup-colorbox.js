// Document.domain set so that third party sites like Resumeware and Thompson Reuters
//  Email (pop-iframe) close and submit functions do not get Permission Denied
//  Checking that we are not on the cm.domain.com environment because this causes issues in Preview (Ribbon)
//alert(' location.host ' + location.host.substring(3, 0 ));

// Limit to sjm.com so that careers.sjm.com and investors.sjm.com 
//      Email popup overlay close/cancel buttons do not give "Permission Denied" or "Access is denied" errors.
//      Remove search page, too since Coveo 6.2 Access Denied
if ((location.host != "localhost") && (location.host.substring(3, 0) != "cm.") &&
     (location.host.toLowerCase().match(".sjm.com")) &&
     !(location.host.toLowerCase().match("http://www.sjm.com/scripts/SJM/health.sjm.com")) &&
     !(location.pathname.toLowerCase().match("http://www.sjm.com/scripts/SJM/search.aspx"))) {
    // This did not work because all pages need to set document.domain on our side and on the third party side, including Coveo
    // Idea:  Limit this to careers.sjm.com and investors.sjm.com and our Email.aspx page called by them to avoid additional issues like 
    // with Coveo 6.1 in IE6 and IE7 with "Access Denied"
    //alert('location pathname ' + location.pathname + ' lower ' + location.pathname.toLowerCase() + ' ' + location.pathname.toLowerCase().match("http://www.sjm.com/scripts/SJM/email.aspx"));
    //if ( (location.host.substring(8,0) == "careers.") || (location.host.substring(10,0) == "investors.") || (location.pathname.toLowerCase().match("http://www.sjm.com/scripts/SJM/email.aspx") ) )
    //{
    var locationDomainParts = location.host.split('.');
    //window.document.domain = "http://www.sjm.com/scripts/SJM/sjm.com";
    if (locationDomainParts.length > 2) {
        //alert(' document.domain Parts > 2' + locationDomainParts[locationDomainParts.length - 2] + '.' + locationDomainParts[locationDomainParts.length - 1]);
        window.document.domain = locationDomainParts[locationDomainParts.length - 2] + '.' + locationDomainParts[locationDomainParts.length - 1];
    } else {
        //alert(' document.domain Parts <= 2' + locationDomainParts[0] + ' Ct ' + locationDomainParts.length);
        window.document.domain = locationDomainParts[0] + '.' + locationDomainParts[1];
    }
    //alert('document.domain ' + window.document.domain);
    //}
}

// avoid prototype.js collisions

$jq_v1_7_1 = jQuery.noConflict();



$jq_v1_7_1(function () {

    $jq_v1_7_1('a.popup').livequery('click',

        function () {

            var theURL = $jq_v1_7_1(this).attr('href');
            var theWidth = $jq_v1_7_1(this).attr('popupWidth')

            if (typeof theWidth === "undefined") { theWidth = "710"; }
            var theHeight = $jq_v1_7_1(this).attr('popupHeight');

            if (typeof theHeight === "undefined") { theHeight = "600"; }

            //alert("theWidth=" + theWidth + "      theHeight=" + theHeight);

            if (theURL.substring(0, 1) == "#") {
                $jq_v1_7_1.colorbox({ href: theURL, inline: true, width: theWidth, height: theHeight });
            } else {
                $jq_v1_7_1.colorbox({ href: theURL, iframe: true, width: theWidth, height: theHeight });
            }
            return false;

        }
	);

});

