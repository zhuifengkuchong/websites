// avoid prototype.js collisions
$jq = jQuery.noConflict();


$jq(function() {

    // Left Navigation Video Promo
    $jq('a#LbGo').livequery(
		'click',
		function() {

            var theURL = $jq('#HeaderNavigationRightDdl').val();
            //alert('Go',theURL);
		    var theTag = $jq('#HeaderNavigationRightDdl option:selected').text();
		    if ((theURL.length > 0) && (theTag.length > 0)) {
		        track_and_link_target(theTag, theURL);
		    }
		}
    );

});