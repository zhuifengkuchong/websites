var surveyCookieExpireDays = 365;

// Show survey after 2 minutes on the site
//var surveyShowMilliSeconds = 120 * 1000;
// Update to show survey after 3 minutes on the site (per Carrie Nelson 5/18/10)
var surveyShowMilliSeconds = 180 * 1000;

// Check every checkSurveyShowSeconds to see if we have reached the time on site
//var checkSurveyShowMilliSeconds = 2 * 1000;
var checkSurveyShowMilliSeconds = 5 * 1000;

var to;

// Code behind sets these variables
//var surveyUrl="http://www.sjm.com/HVA/Survey.aspx";
//var siteName - used for cookie names

function getCookie(c_name)
{
if (document.cookie.length>0) {
  c_start=document.cookie.indexOf(c_name + "=");
  if (c_start!=-1)
    {
    c_start=c_start + c_name.length+1;
    c_end=document.cookie.indexOf(";",c_start);
    if (c_end==-1) c_end=document.cookie.length;
    return unescape(document.cookie.substring(c_start,c_end));
    }
  }
return "";
}

function setCookie(c_name,value,expiredays)
{
    var exdate=new Date();
    exdate.setDate(exdate.getDate()+expiredays);
    document.cookie = c_name + "=" + escape(value) +
    ((expiredays == null) ? "" : ";expires=" + exdate.toGMTString()) +
    "; path=/";
    // Path set to / so same cookie for all directories/pages
}

/*
function clearCookie(c_name, value, expiredays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() - expiredays);
    document.cookie = c_name + "=" + value +
    ";expires=" + exdate.toGMTString() +
    "; path=/";
    // Path set to / so same cookie for all directories/pages
    //debugger;
}

function clearAllCookies() {
    //debugger;
    parmCookieNameBase = siteName;
    var SurveyShowCookieName = parmCookieNameBase + 'SurveyShown';
    var SurveyShowTimeStartCookieName = parmCookieNameBase + 'SurveyShownTimeStart';
    var SurveyShowTimeLastCheckedCookieName = parmCookieNameBase + 'SurveyShowTimeLastChecked';

    clearCookie(SurveyShowCookieName, "", surveyCookieExpireDays);
    clearCookie(SurveyShowTimeStartCookieName, "", surveyCookieExpireDays);
    clearCookie(SurveyShowTimeLastCheckedCookieName, "", surveyCookieExpireDays);
}
*/



function checkSurveyShow(parmCookieNameBase, parmSurveyUrl)
{
    var currentDate = new Date();
    var SurveyShowCookieName = parmCookieNameBase + 'SurveyShown';
    var SurveyShowTimeStartCookieName = parmCookieNameBase + 'SurveyShownTimeStart';
    var SurveyShowTimeLastCheckedCookieName = parmCookieNameBase + 'SurveyShowTimeLastChecked';
    var CheckSurveyShowString = "checkSurveyShow('" + parmCookieNameBase + "','" + parmSurveyUrl + "')";

    //alert('checkSurveyShow ' + SurveyShownTimeStart);
    
    // Set the last time checked every time we check
    setCookie(SurveyShowTimeLastCheckedCookieName, currentDate.toString(), surveyCookieExpireDays);
    
    // Issues Log:  When survey manually shown, it may come up again from here if we do not test for SurveyShown in here 
    var SurveyShown = getCookie(SurveyShowCookieName);
    if (SurveyShown == "true") {
        clearTimeout(to);
    }

    var SurveyShownTimeStart = getCookie(SurveyShowTimeStartCookieName);
   
    if (SurveyShown != "true" && SurveyShownTimeStart != null && SurveyShownTimeStart != "") {      
        var MilliSurveyShownTimeStart = Date.parse(SurveyShownTimeStart);
        //alert('checkSurveyShow SurveyShownTimeStart'+SurveyShownTimeStart+' MilliSurveyShownTimeStart '+MilliSurveyShownTimeStart);
        var MilliSecondsSinceStart = Date.parse(currentDate.toString()) - MilliSurveyShownTimeStart;
        //alert('checkSurveyShow MilliSecondsSinceStart '+MilliSecondsSinceStart+' surveyShowMilliSeconds '+surveyShowMilliSeconds);
        if (MilliSecondsSinceStart > surveyShowMilliSeconds) {
            //debugger;
                setCookie(SurveyShowCookieName, "true", surveyCookieExpireDays);
                clearTimeout(to);
                //alert('showing Survey! '+MilliSecondsSinceStart+' '+surveyShowMilliSeconds);

                //function(){
                //var theURL = $jq(this).attr('href');
//                jQuery('#js-content').append(
//				    '<table class="overlay-wrapper" cellspacing="0" cellpadding="0" border="0"><tr><td>' +
//					    '<table class="overlay-content" cellspacing="0" cellpadding="0" border="0"><tr><td>' +
//						    '<iframe src="' + parmSurveyUrl + '" width="710" height="450" frameborder="0"><\/iframe>' +
//					    '<\/td><\/tr><\/table>' +
//				    '<\/td><\/tr><\/table>'
//			    );
//                jQuery('html').addClass('overlay-active');
//                jQuery('#js-overlay').fadeIn(animspeed);
//                jQuery('#js-content').fadeIn(animspeed);
                jQuery().colorbox({ width: "710px", height: "450px", iframe: true, href: parmSurveyUrl });
                //return false;
                //}           
        }
        else 
        {
            // If false, check timer and show survey
            //alert('setting to');
            to = setTimeout(CheckSurveyShowString, checkSurveyShowMilliSeconds);
        }
    }
    else 
    {
        //alert('checkSurveyShow:  SurveyTimeStart is null '+exdate.toString());
        setCookie(SurveyShowTimeStartCookieName, currentDate.toString(), surveyCookieExpireDays);
        to = setTimeout(CheckSurveyShowString, checkSurveyShowMilliSeconds);
    }
}

function surveyCookieWithParms(parmCookieNameBase, parmSurveyUrl) {
    var exdate = new Date();
    var SurveyShowCookieName = parmCookieNameBase + 'SurveyShown';
    var SurveyShowTimeStartCookieName = parmCookieNameBase + 'SurveyShownTimeStart';
    var SurveyShowTimeLastCheckedCookieName = parmCookieNameBase + 'SurveyShowTimeLastChecked';
    var CheckSurveyShowString = "checkSurveyShow('" + parmCookieNameBase + "','" + parmSurveyUrl + "')";
    //debugger;
    var SurveyShown = getCookie(SurveyShowCookieName);
    //alert('surveyShownOnInit ' +SurveyShown);
    if (SurveyShown == null || SurveyShown == "" || SurveyShown != "true" )
    {
        //alert('setting SurveyShown to false '+SurveyShown);
        setCookie(SurveyShowCookieName, "false", surveyCookieExpireDays);                    
        // If false, check timer and show survey
        to = setTimeout(CheckSurveyShowString, checkSurveyShowMilliSeconds);
        //alert('surveyCookie ' + parmCookieNameBase + ' ' + parmSurveyUrl);

        // This function is called on every page, so check for SurveyShowTimeStart
        var SurveyShowTimeStart = getCookie(SurveyShowTimeStartCookieName);
        if (SurveyShowTimeStart == null || SurveyShowTimeStart == "") {            
            //alert('surveyCookieWithParms:  SurveyShowTimeStart is null:  Init dates ' + exdate.toString());
            setCookie(SurveyShowTimeStartCookieName, exdate.toString(), surveyCookieExpireDays);

            // Calling checkSurveyShow directly in case user paging quickly and timer doesn't go off
            to = setTimeout(CheckSurveyShowString, checkSurveyShowMilliSeconds);
            //checkSurveyShow(parmCookieNameBase, parmSurveyUrl);
        }
        else {
            // Timer starts the first browser visit, does not persist between (per Carrie Nelson 5/18/10)
            // If the last time we checked is greater than 2 X checkSurveyShowMilliSeconds,
            //  then do not show the survey, initialize the time
            var SurveyShowTimeLastChecked = getCookie(SurveyShowTimeLastCheckedCookieName);
            //alert('surveyCookieWithParms:  SurveyShowTimeLastChecked '+SurveyShowTimeLastChecked);
            var SurveyShowTimeLastCheckedDate = Date.parse(SurveyShowTimeLastChecked);
            var MilliSecondsSinceLastChecked = Date.parse(exdate.toString()) - SurveyShowTimeLastCheckedDate;
            if (MilliSecondsSinceLastChecked > (2 * checkSurveyShowMilliSeconds)) {
                // Initialize the timer since this value was from a previous browser visit
                //alert('surveyCookieWithParms:  SurveyShowTimeStart NOT null:  Init dates Last Checked ' + MilliSecondsSinceLastChecked + ' ' + checkSurveyShowMilliSeconds);
                setCookie(SurveyShowTimeStartCookieName, exdate.toString(), surveyCookieExpireDays);
                setCookie(SurveyShowTimeLastCheckedCookieName, exdate.toString(), surveyCookieExpireDays);
                // Calling checkSurveyShow directly in case user paging quickly and timer doesn't go off
                to = setTimeout(CheckSurveyShowString, checkSurveyShowMilliSeconds);
                //checkSurveyShow(parmCookieNameBase, parmSurveyUrl);
            }
        }
    }
    else
    {
        //alert('surveyCookie:  Survey Shown'+SurveyShown);
    }
    setCookie(SurveyShowTimeLastCheckedCookieName, exdate.toString(), surveyCookieExpireDays);
}

function surveyCookie() {
    //alert('site Name ' + siteName + ' ' + surveyUrl);
    return(surveyCookieWithParms(siteName,surveyUrl));
}

function setSurveyCookie() {
    //debugger;
    var SurveyShowCookieName = siteName + 'SurveyShown';
    setCookie(SurveyShowCookieName, "true", surveyCookieExpireDays); 
}