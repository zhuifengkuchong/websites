//standard GA
var _gaq = _gaq || [];
var _gat = _gaq || [];
var pluginUrl = '../../../www.google-analytics.com/plugins/ga/inpage_linkid.js'/*tpa=http://www.google-analytics.com/plugins/ga/inpage_linkid.js*/;
_gaq.push(['_require', 'inpage_linkid', pluginUrl]);
_gaq.push(['_setAccount', 'UA-4338456-1']);
_gaq.push(['_setDomainName', 'http://www.sjm.com/scripts/SJMSites/sjm.com']);
_gaq.push(['_trackPageview']);
(function () {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';  
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

// <!-- Universal GA -->

(function (i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
        (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date(); a = s.createElement(o),
      m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
})(window, document, 'script', '../../../www.google-analytics.com/analytics.js'/*tpa=http://www.google-analytics.com/analytics.js*/, 'ga');
ga('create', 'UA-4338456-79', 'http://www.sjm.com/scripts/SJMSites/sjm.com');
ga('send', 'pageview');