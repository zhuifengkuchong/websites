/* Generic Google Analytics tracking function to use everywhere so there is only one place to change if the metrics provider changes */

//var trackerId = "UA-4338456-1";  // Define in the calling page

// There was a problem with _gat being undefined so pages not tracked (found on sjmneuropro.com)
//var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
//document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
if (typeof (_gat) != 'object') document.write('<sc' + 'ript src="http' + (document.location.protocol == 'https:' ? 's://ssl' : '://www') + '.google-analytics.com/ga.js"></sc' + 'ript>');



function trackPage() {
    //alert('trackPage ');
    var pageTracker = _gat._getTracker(trackerId);
    pageTracker._trackPageview();
    //alert('trackPage trackerID ' + trackerId);
}

function trackIt(parmEvent,addUrl,pageIsValid)
{
    //alert('trackIt '+pageIsValid);
    try
    {
        var pageTracker = _gat._getTracker(trackerId);
        
         // pageIsValid is passed for .NET pages with client side validation so we do not track multiple times if page is not valid
         if (pageIsValid == true)
         {            
            var parmEventStr = parmEvent;
            if (addUrl == true)
            {
                parmEventStr = parmEvent+'/'+document.location.href;
            }           
            pageTracker._trackPageview(parmEventStr);
            //alert('trackIt '+parmEventStr);
        }
        
    } catch(err) {}
}

// .NET client side validation - determine if page is valid so we do not track page multiple times
// http://stackoverflow.com/questions/1066857/determine-if-page-is-valid-in-javascript-asp-net
function ValidatePage() 
{        
    var ReturnIsValid = false;
    if (typeof (Page_ClientValidate) == 'function') 
    {            
        Page_ClientValidate();        
    }        
    if (Page_IsValid) 
    {            
        // do something            
        //alert('Page is valid!');                        
        ReturnIsValid = true;
    }        
    else {            
        // do something else            
        //alert('Page is not valid!');        
        ReturnIsValid = false;
    } 
    return ReturnIsValid;   
}

function trackAndPrint()
{
    trackIt('Print',true,true);
    window.print();
}

function track_and_link(parmTag, parmUrl) {
    try {
        var pageTracker = _gat._getTracker(trackerId);
        //alert('Tag ' + parmTag);

        // Google Tracking
        // Replace white spaces with a dash
        parmTag = parmTag.replace(/ /g, "-");
        pageTracker._trackPageview(parmTag);
        //alert('track_and_link Tag ' + parmTag + ' trackerID ' + trackerId);
                
        if ((parmUrl.toString().length > 5) && (parmUrl.toString().substr(0, 5) == 'http:')) {
            //alert('External');
            window.open(parmUrl);
        }
        else {
            //alert('Internal'); 
            window.location = parmUrl;
        }
    } catch (err) { }
}

function track_and_link_target(parmTag, parmUrlAndTarget) {
    try {
        // parmUrlAndTarget is href='' target=''
        
        var pageTracker = _gat._getTracker(trackerId);
        //alert('Tag ' + parmTag);

        var parmUrl = '';
        var parmUrlFirstSingleQuote = parmUrlAndTarget.indexOf("'");
        //alert('first ' + parmUrlFirstSingleQuote);
        
        var parmUrlNextSingleQuote = parmUrlAndTarget.indexOf("'",parmUrlFirstSingleQuote+1);
   
        if (parmUrlAndTarget.indexOf('href=') >= 0) {
            parmUrl = parmUrlAndTarget.slice(parmUrlFirstSingleQuote+1, parmUrlNextSingleQuote);
        }
        //alert('parmUrl ' + parmUrl);

        var Target = '';
        var parmTargetFirstSingleQuote = parmUrlAndTarget.indexOf("'", parmUrlNextSingleQuote + 1);
        
        var parmTargetNextSingleQuote = parmUrlAndTarget.indexOf("'", parmTargetFirstSingleQuote + 1);
        if (parmUrlAndTarget.indexOf('target=') >= 0) {
            parmTarget = parmUrlAndTarget.slice(parmTargetFirstSingleQuote+1, parmTargetNextSingleQuote);
        }
        //alert('parmTarget ' + parmTarget);

        // Google Tracking
        // Replace white spaces with a dash
        parmTag = parmTag.replace(/ /g, "-");       
        pageTracker._trackPageview(parmTag);
        alert('track_and_link_target Tag ' + parmTag + ' trackerID ' + trackerId + ' url ' + parmUrl + ' target '+parmTarget);
        
        if ( parmTarget.indexOf("_blank") >= 0 ) {
            alert('External');
            window.open(parmUrl);
        }
        else {
            alert('Internal'); 
            window.location = parmUrl;
        }
    } catch (err) {}
}

function track_and_link_target_parms(parmTag, parmUrl, parmTarget) {
    try {
        // parmUrlAndTarget is href='' target=''

        var pageTracker = _gat._getTracker(trackerId);
        //alert('Tag ' + parmTag);

        // Google Tracking
        // Replace white spaces with a dash
        parmTag = parmTag.replace(/ /g, "-");
        pageTracker._trackPageview(parmTag);
        //alert('track_and_link_target_parms Tag ' + parmTag + ' trackerID ' + trackerId + ' url '+parmUrl + ' target '+parmTarget);

        if (parmTarget.indexOf("_blank") >= 0) {
            //alert('External');
            window.open(parmUrl);
        }
        else {
            //alert('Internal'); 
            window.location = parmUrl;
        }
    } catch (err) { }
}

function linktracking(tag) {
    var pageTracker = _gat._getTracker(trackerId);
    pageTracker._trackPageview(tag);
}

function page_name_only(fullPageName) {
    thePage = unescape(fullPageName)
    if (thePage.indexOf('?') !== -1)
        thePage = thePage.substring(0, thePage.indexOf('?'))
    //thePage = thePage.substr(thePage.lastIndexOf('/') + 1)
    thePage = thePage.substr(thePage.lastIndexOf('/'))
    //thePage = thePage.substr(0, thePage.lastIndexOf('.'))
    //alert(thePage)
    return (thePage);
}

// avoid prototype.js collisions
$jq = jQuery.noConflict();

$jq(function () {
    // Moved a.trackIt to Tracker.js
    $jq('a.trackIt').livequery(
		    'click',
		    function () {
		        var theURL = $jq(this).attr('href');
		        trackIt('/Link/' + theURL, false, true);
		    }
    );

    function endsWith(str, suffix) {
        return str.indexOf(suffix, str.length - suffix.length) !== -1;
    }

    $jq('a.trackEventPageToFrom').livequery(
		    'click',
		    function () {		        
		        var pageTracker = _gat._getTracker(trackerId);
		        var ga_category = $jq(this).attr('ga_category');
		        var ga_action = $jq(this).attr('ga_action');
		        var ga_label = $jq(this).attr('ga_label');

		        if ((ga_category != undefined) && (ga_action != undefined) && (ga_label != undefined)) {
		            // Google Event Track (Category, Action, Label, Value);		        
		            pageTracker._trackEvent(ga_category, ga_action, ga_label);
                }
		        else {
		            var pageFrom = window.location.pathname.toLowerCase();
		            var pageTo = $jq(this).attr('href').toLowerCase();

		            if (pageTo.substring(0, 8) == "~/media/") { // File
                        ga_category = "File";
                        ga_action = pageTo;
                        ga_label = page_name_only(pageTo).substr(1);
                        pageTracker._trackEvent(ga_category, ga_action, ga_label);
		            }
                    else if (endsWith(pageTo, ".mp4") || endsWith(pageTo, ".webm") || endsWith(pageTo, ".ogg") || endsWith(pageTo, ".flv") || endsWith(pageTo, ".wmv")) { // Video
                        ga_category = "Video";
                        ga_action = "View";
                        ga_label = page_name_only(pageTo).substr(1);
                        pageTracker._trackEvent(ga_category, ga_action, ga_label);
                    }
                    else if (pageTo.substring(0, 4) == "http") { // External Link
		                ga_category = 'External Link';
		                ga_action = 'Click';
		                ga_label = pageTo;
		                pageTracker._trackEvent(ga_category, ga_action, ga_label);
		            }
		        }

		        //alert('ga_category: ' + ga_category + "\n" + 'ga_action: ' + ga_action + "\n" + 'ga_label: ' + ga_label);
		    }
        );

    $jq('input.trackEventSubmit').livequery(
		    'click',
		    function () {
		        var pageTracker = _gat._getTracker(trackerId);

		        // Google Event Track (Category, Action, Label, Value);
		        //                      Page From, 'Submit'
		        var pageFrom = page_name_only(window.location.pathname);
		        //alert(' input from ' + pageFrom + ' Submit ');
		        pageTracker._trackEvent(pageFrom, 'Submit');
		    }
        );

});

