
// Placing this in a separate include file to include in layouts
//  to add event tracking to all links
$jq = jQuery.noConflict();
$jq(function () {
    //alert('a tags');
    // Google Analytics Event Tracking, adding to all links
    $jq("a").addClass("trackEventPageToFrom");
});