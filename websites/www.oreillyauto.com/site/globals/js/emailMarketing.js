var emailZipTipDefined=false;
var helperText="E-mail Address";
function signupEmailMarketingIfEnterWasPressed(b,a){if(enterKeyWasPressed(b)&&document.getElementById("signUpLoading"+a).style.display=="none"){signupEmailMarketing("emailMarketingAddress"+a);
return false
}return true
}function signupZipCodeMarketingIfEnterWasPressed(a,b){if(enterKeyWasPressed(a)){addMyEmailZip(b);
return false
}return true
}function signupEmailMarketing(e){var c=document.getElementById(e).value;
if(!emailZipTipDefined){var d="<div class='marketingQtip'><p>Want emails that apply specifically to your location?<span>Just enter your <b>zip code</b> here<input size='5' maxLength='5' class='emailZipTextBox' type='text' onkeypress='signupZipCodeMarketingIfEnterWasPressed(event, \""+e+"\");'/><input class='goButton' type='button' onclick='addMyEmailZip(\""+e+"\")'/></span></p></div>";
var a="Skip";
var b="Either way, thank you for signing up!";
$("#emailMarketingAddressHeader").qtip({content:{title:{text:d,button:a},text:b,prerender:true},style:{tip:"topRight",name:"oreilly",width:390},position:{corner:{target:"bottomMiddle",tooltip:"topRight"}},show:{effect:{type:"slide",length:250},when:false},hide:{when:{event:"unfocus"}},api:{onHide:emailToolTipHidden,onShow:zipShown}});
$("#emailMarketingAddressShoppingCart").qtip({content:{title:{text:d,button:a},text:b,prerender:true},style:{tip:"topLeft",name:"oreilly",width:390},position:{corner:{target:"leftBottom",tooltip:"topLeft"}},show:{effect:{type:"slide",length:250},when:false},hide:{when:{event:"unfocus"}},api:{onHide:emailToolTipHidden,onShow:zipShown}});
$("#emailMarketingAddressFooter").qtip({content:{title:{text:d,button:a},text:b,prerender:true},style:{tip:"rightBottom",name:"oreilly",width:390},position:{corner:{target:"leftMiddle",tooltip:"rightBottom"}},show:{effect:{type:"slide",length:250},when:false},hide:{when:{event:"unfocus"}},api:{onHide:emailToolTipHidden,onShow:zipShown}});
emailZipTipDefined=true
}$.ajax({type:"POST",url:"/site/EmailMarketingServlet",data:"EmailMarketingEmailAddress="+c,dataType:"text",success:function(){try{doSetEmailMarketingSuccess()
}catch(f){ajaxFailure()
}},error:function(g){try{doSetEmailMarketingError(g)
}catch(f){ajaxFailure()
}}});
$("#signUpButtonHeader").css("display","none");
$("#signUpLoadingHeader").css("display","block");
$("#signUpButtonFooter").css("display","none");
$("#signUpLoadingFooter").css("display","block");
$("#signUpButtonShoppingCart").css("display","none");
$("#signUpLoadingShoppingCart").css("display","block")
}function doSetEmailMarketingSuccess(){if(isHeaderQtip()){$("#emailMarketingAddressHeader").qtip("show")
}else{if(isFooterQtip()){$("#emailMarketingAddressFooter").qtip("show")
}else{$("#emailMarketingAddressShoppingCart").qtip("show")
}}}function zipShown(){if(isHeaderQtip()){$(".emailZipTextBox:first")[0].focus()
}else{if(isFooterQtip()){$(".emailZipTextBox:last")[0].focus()
}else{$(".emailZipTextBox:eq(1)").focus()
}}}function addMyEmailZip(c){var b;
if(isHeaderQtip()){b=$(".emailZipTextBox:first").val()
}else{if(isFooterQtip()){b=$(".emailZipTextBox:last").val()
}else{b=$(".emailZipTextBox:eq(1)").val()
}}var a=document.getElementById(c).value;
$.ajax({type:"POST",url:"/site/EmailMarketingServlet",data:"EmailZipCode="+b+"&EmailMarketingEmailAddress="+a,dataType:"text",success:function(){try{doSetZipMarketingSuccess()
}catch(d){ajaxFailure()
}},error:function(){try{doSetZipMarketingError()
}catch(d){ajaxFailure()
}}})
}function doSetEmailMarketingError(a){$("#signUpLoadingHeader").css("display","none");
$("#signUpButtonHeader").css("display","block");
$("#signUpLoadingFooter").css("display","none");
$("#signUpButtonFooter").css("display","block");
$("#signUpLoadingShoppingCart").css("display","none");
$("#signUpButtonShoppingCart").css("display","block");
if($.browser.msie||$.browser.opera){if(a.responseXML.hasChildNodes()){alert("The email you entered is not valid!")
}else{alert("Sorry, your session has expired. Please refresh the page.")
}}else{if(a.responseXML!=null){alert("The email you entered is not valid!")
}else{alert("Sorry, your session has expired. Please refresh the page.")
}}}function doSetZipMarketingSuccess(){if(isHeaderQtip()){$("#emailMarketingAddressHeader").qtip("hide")
}else{if(isFooterQtip()){$("#emailMarketingAddressFooter").qtip("hide")
}else{$("#emailMarketingAddressShoppingCart").qtip("hide");
$(".promoEmailContainer").hide();
$(".cartAdSpace").css("background","#D9D4B7")
}}}function doSetZipMarketingError(){alert("The zip code you entered is not valid!")
}function emailToolTipHidden(){$("#signUpLoadingHeader").css("display","none");
$("#signUpButtonHeader").css("display","block");
$("#signUpLoadingFooter").css("display","none");
$("#signUpButtonFooter").css("display","block");
$("#signUpLoadingShoppingCart").css("display","none");
$("#signUpButtonShoppingCart").css("display","block");
$(".promoEmailContainer").hide();
$(".cartAdSpace").toggleClass("promoHighlight");
$("#emailMarketingAddressHeader").val(helperText);
$("#emailMarketingAddressFooter").val(helperText);
$("#emailMarketingAddressShoppingCart").val(helperText);
$(".emailZipTextBox").val("")
}function isHeaderQtip(){var a=$("#emailMarketingAddressHeader").val();
if(a==null){return false
}else{return a!=helperText
}}function isFooterQtip(){var a=$("#emailMarketingAddressFooter").val();
if(a==null){return false
}else{return a!=helperText
}};