function SearchSuggestions(){if(typeof XMLHttpRequest!="undefined"){this.http=new XMLHttpRequest()
}else{if(typeof ActiveXObject!="undefined"){this.http=new ActiveXObject("MSXML2.XmlHttp")
}else{alert("No XMLHttpRequest object available. This functionality will not work.")
}}}SearchSuggestions.prototype.requestSuggestions=function(oAutoSuggestControl,bTypeAhead){var searchValue=oAutoSuggestControl.textbox.value;
if(searchValue.length>=3){var oHttp=this.http;
if(oHttp.readyState!=0){oHttp.abort()
}var sURL="/site/PreSearchServlet?queryString="+encodeURIComponent(searchValue);
oHttp.open("get",sURL,true);
oHttp.onreadystatechange=function(){if(oHttp.readyState==4){if(oHttp.status==200){var aSuggestions=eval(oHttp.responseText);
oAutoSuggestControl.autosuggest(aSuggestions,bTypeAhead)
}else{oAutoSuggestControl.hideSuggestions()
}}};
oHttp.send(null)
}else{oAutoSuggestControl.hideSuggestions()
}};