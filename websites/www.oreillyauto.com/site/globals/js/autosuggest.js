var submittingForm=false;
function AutoSuggestControl(b,a,c){this.cur=-1;
this.layer=null;
this.provider=a;
this.textbox=b;
this.keywordText=c;
this.init()
}AutoSuggestControl.prototype.autosuggest=function(b,a){if(b.length>0){if(a){this.typeAhead(b[0])
}this.showSuggestions(b)
}else{this.hideSuggestions()
}};
AutoSuggestControl.prototype.createDropDown=function(){var a=this;
this.layer=document.createElement("div");
this.layer.className="suggestions";
this.layer.style.visibility="hidden";
this.layer.onmousedown=this.layer.onmouseup=this.layer.onmouseover=function(b){b=b||window.event;
oTarget=b.target||b.srcElement;
if(b.type=="mousedown"){a.textbox.value=oTarget.firstChild.nodeValue;
a.hideSuggestions();
setTimeout('document.getElementById("searchLoading").style.visibility = "visible"',60);
$("#searchBarForm").submit()
}else{if(b.type=="mouseover"){a.highlightSuggestion(oTarget)
}else{a.textbox.focus()
}}};
document.getElementById("search").appendChild(this.layer)
};
AutoSuggestControl.prototype.getLeft=function(){var b=this.textbox;
var a=0;
while(b.tagName!="BODY"){a+=b.offsetLeft;
b=b.offsetParent
}if(navigator.appVersion.indexOf("MSIE 7")>-1){a-=10
}else{if(navigator.appVersion.indexOf("MSIE 8")>-1){a-=11
}}return a
};
AutoSuggestControl.prototype.getTop=function(){var b=this.textbox;
var a=0;
while(b.tagName!="BODY"){a+=b.offsetTop;
b=b.offsetParent
}if(navigator.appVersion.indexOf("MSIE 7")>-1){a-=4
}else{if(navigator.appVersion.indexOf("MSIE 8")>-1){a-=4
}else{if(navigator.appVersion.indexOf("MSIE 6")>-1){a-=1
}}}return a
};
AutoSuggestControl.prototype.handleKeyDown=function(a){switch(a.keyCode){case 38:this.previousSuggestion();
break;
case 40:this.nextSuggestion();
break;
case 13:this.hideSuggestions();
break;
default:this.cur=-1
}};
AutoSuggestControl.prototype.handleKeyUp=function(a){if(submittingForm){return
}var b=a.keyCode;
if(b==8||b==46){this.provider.requestSuggestions(this,false)
}else{if(b<32||(b>=33&&b<46)||(b>=112&&b<=123)){}else{this.provider.requestSuggestions(this,false)
}}};
AutoSuggestControl.prototype.hideSuggestions=function(){this.layer.style.visibility="hidden"
};
AutoSuggestControl.prototype.highlightSuggestion=function(a){for(var b=0;
b<this.layer.childNodes.length;
b++){var c=this.layer.childNodes[b];
if(c==a){c.className="current"
}else{if(c.className=="current"){c.className=""
}}}};
AutoSuggestControl.prototype.init=function(){var a=this;
this.textbox.onkeyup=function(b){if(!b){b=window.event
}a.handleKeyUp(b)
};
this.textbox.onkeydown=function(b){if(!b){b=window.event
}a.handleKeyDown(b)
};
this.textbox.onblur=function(){if(a.textbox.value==""){a.textbox.value=a.keywordText;
a.textbox.style.color="#A29B84"
}a.hideSuggestions()
};
this.createDropDown()
};
AutoSuggestControl.prototype.nextSuggestion=function(){var b=this.layer.childNodes;
if(b.length>0&&this.cur<b.length-1){var a=b[++this.cur];
this.highlightSuggestion(a);
this.textbox.value=a.firstChild.nodeValue
}};
AutoSuggestControl.prototype.previousSuggestion=function(){var b=this.layer.childNodes;
if(b.length>0&&this.cur>0){var a=b[--this.cur];
this.highlightSuggestion(a);
this.textbox.value=a.firstChild.nodeValue
}else{if(this.cur==0){var a=b[this.cur];
this.textbox.focus();
this.textbox.select();
a.className=""
}}};
AutoSuggestControl.prototype.selectRange=function(a,b){if(this.textbox.createTextRange){var c=this.textbox.createTextRange();
c.moveStart("character",a);
c.moveEnd("character",b-this.textbox.value.length);
c.select()
}else{if(this.textbox.setSelectionRange){this.textbox.setSelectionRange(a,b)
}}this.textbox.focus()
};
AutoSuggestControl.prototype.showSuggestions=function(c){var a=null;
this.layer.innerHTML="";
for(var b=0;
b<c.length;
b++){a=document.createElement("div");
a.appendChild(document.createTextNode(c[b]));
this.layer.appendChild(a)
}this.layer.style.visibility="visible"
};
AutoSuggestControl.prototype.typeAhead=function(b){if(this.textbox.createTextRange||this.textbox.setSelectionRange){var a=this.textbox.value.length;
this.textbox.value=b;
this.selectRange(a,b.length)
}};