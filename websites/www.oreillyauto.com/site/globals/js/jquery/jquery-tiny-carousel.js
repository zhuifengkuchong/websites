(function(a){a.tiny=a.tiny||{};
a.tiny.carousel={options:{start:1,display:1,axis:"x",controls:true,pager:false,interval:false,intervaltime:3000,rewind:false,animation:true,duration:1000,callback:null}};
a.fn.tinycarousel=function(c){var c=a.extend({},a.tiny.carousel.options,c);
this.each(function(){a(this).data("tcl",new b(a(this),c))
});
return this
};
a.fn.tinycarousel_start=function(){a(this).data("tcl").start()
};
a.fn.tinycarousel_stop=function(){a(this).data("tcl").stop()
};
a.fn.tinycarousel_move=function(c){a(this).data("tcl").move(c-1,true)
};
function b(q,e){var i=this;
var h=a(".viewport:first",q);
var g=a(".overview:first",q);
var k=g.children();
var f=a(".next:first",q);
var d=a(".prev:first",q);
var l=a(".pager:first",q);
var w,u,p,j,o,n=true,s=e.axis=="x";
function c(){w=s?a(k[0]).outerWidth(true):a(k[0]).outerHeight(true);
var x=Math.ceil(((s?h.outerWidth():h.outerHeight())/(w*e.display))-1);
u=Math.max(1,Math.ceil(k.length/e.display)-x);
p=Math.min(u,Math.max(1,e.start))-2;
g.css(s?"width":"height",(w*k.length));
i.move(1);
r();
return i
}function r(){if(e.controls&&d.length>0&&f.length>0){d.click(function(){i.move(-1);
return false
});
f.click(function(){i.move(1);
return false
})
}if(e.interval){q.hover(i.stop,i.start)
}if(e.pager&&l.length>0){a("a",l).click(v)
}}function m(){if(e.controls){d.toggleClass("disable",!(p>0));
f.toggleClass("disable",!(p+1<u))
}if(e.pager){var x=a(".pagenum",l);
x.removeClass("active");
a(x[p]).addClass("active")
}}function v(x){if(a(this).hasClass("pagenum")){i.move(parseInt(this.rel),true)
}return false
}function t(){if(e.interval&&!o){clearTimeout(j);
j=setTimeout(function(){p=p+1==u?-1:p;
n=p+1==u?false:p==0?true:n;
i.move(n?1:-1)
},e.intervaltime)
}}this.stop=function(){clearTimeout(j);
o=true
};
this.start=function(){o=false;
t()
};
this.move=function(y,z){p=z?y:p+=y;
if(p>-1&&p<u){var x={};
x[s?"left":"top"]=-(p*(w*e.display));
g.animate(x,{queue:false,duration:e.animation?e.duration:0,complete:function(){if(typeof e.callback=="function"){e.callback.call(this,k[p],p)
}}});
m();
t()
}};
return c()
}})(jQuery);