$(document).ready(function() {

// Megamenu initialization

$().jetmenu();

// Select menu replacement 
$('select').selectric({responsive:true});


// Custom Radio buttons
$('input:radio').screwDefaultButtons({
        image: 'url("https://media.mhfi.com/designimages/radio.png")',
     //   image: 'url("../images/radio.png")',
        width: 22,
        height: 22
});

// Custom Checkboxes
$('input:checkbox').screwDefaultButtons({
        image: 'url("https://media.mhfi.com/designimages/checkbox.png")',
     //   image: 'url("../images/checkbox.png")',
        width: 22,
        height: 22
});

// Fix to make checkboxes work in Bootstrap accordion
$('#accordion-location').on('https://www.mhfi.com/includes/8c85b2d745751557030ffdf3f68a837f/shown.bs.collapse.in', function () {
    $('input:radio').screwDefaultButtons("uncheck");
    $('.collapse.in').prevAll().find('input:radio').screwDefaultButtons("check");
});

// Fix to make checkboxes work in Bootstrap accordion
$('#accordion-contentType').on('https://www.mhfi.com/includes/8c85b2d745751557030ffdf3f68a837f/shown.bs.collapse.in', function () {
    $('input:radio').screwDefaultButtons("uncheck");
    $('.collapse.in').prevAll().find('input:radio').screwDefaultButtons("check");
});



// Back to top initialization for mobile
$( ".backtotop a" ).click(function() {
    $('body').animatescroll({scrollSpeed:700,easing:'easeInOutQuart'});
});


// Homepage Slider
$(".homeSlider").royalSlider({
            keyboardNavEnabled: true,
            autoScaleSlider: true,
            autoScaleSliderWidth: 3,
            autoScaleSliderHeight: 2,
            imageScaleMode: 'none',
            fadeinLoadedSlide: true,
            imageAlignCenter:false,
            imageScalePadding: 0,
            slidesSpacing: 0,
            numImagesToPreload: 2,
            arrowsNavAutoHide:false,

            controlNavigation: 'bullets',
            loop: true,
            transitionType: 'fade',
    	    autoPlay: {
                enabled: true,
                delay: 5000
            },
            block: {
         // animated blocks options go gere
    		fadeEffect: true,
    		moveEffect: 'left'
    	}
});  


// General Image Only Slider
$(".imageSlider").royalSlider({
            keyboardNavEnabled: true,
            fadeinLoadedSlide: true,
            autoScaleSlider: true,
            autoScaleSliderWidth: 1170,
            autoScaleSliderHeight: 500,
            imageScaleMode: 'none',
            imageAlignCenter:false,
            imageScalePadding: 0,
            slidesSpacing: 0,
            numImagesToPreload: 2,
            loop: true,
      //      transitionType: 'fade',
    	    autoPlay: {
                enabled: true,
                delay: 5000
            }
});  



// Full Width Slider with text
$(".fullwidthSlider").royalSlider({
            keyboardNavEnabled: true,
            fadeinLoadedSlide: true,
            autoScaleSlider: true, 
            autoScaleSliderWidth: 1670,
            autoScaleSliderHeight: 500,
            imageScaleMode: 'fill',
            slidesSpacing: 0,
           arrowsNavAutoHide:false,

            controlNavigation: 'bullets',
            numImagesToPreload: 2,
            loop: true,
         //   transitionType: 'fade',
            transitionSpeed: 500,
    	    autoPlay: {
                enabled: true,
                delay: 5000
            },
            block: {
    		// animated blocks options go gere
    		fadeEffect: true,
    		moveEffect: 'bottom',
    		speed: 	1000
    	    }
}); 


    // Auto-hide bullets if less than 1 slide
if(slider) { 
    var slider = $('.royalSlider'),
    bullets = slider.find('.rsNav');
    if (slider.data('royalSlider').numSlides <= 1) {
    bullets.hide();
    }
}



// Generic content toggle
$('.toggle').hide();
$('.trigger').click(function() {
    $(this).next().animate({
            height: "toggle",
            opacity: "toggle"
        }, "normal");
});




// Report Piracy Form Validation
var validateCaptcha = false;
var SELECTOR_ERRORS = $('.error-box'),
    SELECTOR_SUCCESS = $('.success-box');
	
var validator = new FormValidator('form-report-piracy', [{
    name: 'contact-name',
    display: 'Name', 
    rules: 'required'
},{
    name: 'contact-email',
    display: 'Email', 
    rules: 'required|valid_email'
},{ 
    name: 'contact-phone',
    display: 'Phone', 
    rules: 'required|numeric'
 
}], function(errors, evt) {
	SELECTOR_ERRORS.empty();
    var validateCaptcha = validateRecaptcha();
	if (errors.length > 0 || validateCaptcha  == false) {
		for (var i = 0, errorLength = errors.length; i < errorLength; i++) {
			SELECTOR_ERRORS.append(errors[i].message + '<br />');
        }
        SELECTOR_SUCCESS.css({ display: 'none' });
        SELECTOR_ERRORS.fadeIn(600);
    	if (evt && evt.preventDefault) {
    		evt.preventDefault();
        } else if (event) {
    		event.returnValue = false;
        }
    } else {
        SELECTOR_ERRORS.css({ display: 'none' });
        SELECTOR_SUCCESS.fadeIn(600);
    }
});

// Captcha validation
function validateRecaptcha()
{		
    var challengeField = $("#recaptcha_challenge_field").val();
    var responseField = $("#recaptcha_response_field").val();
	var dataSent = "recaptcha_challenge_field=" + challengeField + "&recaptcha_response_field=" + responseField;
	var html = $.ajax({
					type: "POST",
					url: "/templates/MHF_Validate_Recaptcha?clearcache="+Math.random()*20,
					data: dataSent,
					async: false
					}).responseText;
	var getResult = $.trim(html);
    if(getResult == "false")
    {				
        SELECTOR_ERRORS.append('Your Captcha is incorrect. Please try again. <br />');							
        Recaptcha.reload();
        return false;
    }
    else
    {
    	return true;
    }
}


var validatorAd = new FormValidator('Online_Request_Form', [{
	name: 'name',
	display: 'Name',
	rules: 'required'
},{
	name: 'email',
	display: 'Email',
	rules: 'valid_email|required'
},{
	name: 'company_name',
	display: 'Company/Organization',
	rules: 'required'
},{
	name: 'phone_number',
	display: 'Phone Number', 
	rules: 'numeric|required'
}], function(errors, evt) {
	SELECTOR_ERRORS.empty();
    var validateCaptcha = validateRecaptcha();
	if (errors.length > 0 || validateCaptcha  == false) {
		for (var i = 0, errorLength = errors.length; i < errorLength; i++) {
			SELECTOR_ERRORS.append(errors[i].message + '<br />');
        }
        SELECTOR_SUCCESS.css({ display: 'none' });
        SELECTOR_ERRORS.fadeIn(600);
    	if (evt && evt.preventDefault) {
    		evt.preventDefault();
        } else if (event) {
    		event.returnValue = false;
        }
    } else {
        SELECTOR_ERRORS.css({ display: 'none' });
        SELECTOR_SUCCESS.fadeIn(600);
    }
    
});	

// Change class on the stock ticker if the value is negative and add a plus (+) sign if positive
var newPrice = parseFloat($(".positive").text());

if (newPrice > 0.0) {
  newPrice = '+' + newPrice;
  $(".positive").text(newPrice);
}

$(".positive:contains('-')").removeClass( "positive" ).addClass( "negative" );

// change the indicators icons in the drop down when in mobile view
   $( ".indicator" ).on( "click", function() {
   if($(this).html() === "+"){
      $(this).html('-');
  }else{   
    event.preventDefault();
    $('.indicator').html('+');
  };
  });  
  
     $( ".icon" ).on( "click", function() {  
     //console.log('icon - clicked');
     //console.log($( window ).width());
     mywidth = $( window ).width();
     
     if($( window ).width() < 480){
     
       $("ul.jetmenu > li > a ").find('span:first').on("click", function(){
	$(this).unbind();
 	event.stopPropagation();
     	});
      };
    
});
    
});
