<script id = "race35a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race35={};
	myVars.races.race35.varName="Lu_Id_div_2__onmouseout";
	myVars.races.race35.varType="@varType@";
	myVars.races.race35.repairType = "@RepairType";
	myVars.races.race35.event1={};
	myVars.races.race35.event2={};
	myVars.races.race35.event1.id = "Lu_DOM";
	myVars.races.race35.event1.type = "onDOMContentLoaded";
	myVars.races.race35.event1.loc = "Lu_DOM_LOC";
	myVars.races.race35.event1.isRead = "False";
	myVars.races.race35.event1.eventType = "@event1EventType@";
	myVars.races.race35.event2.id = "Lu_Id_div_2";
	myVars.races.race35.event2.type = "onmouseout";
	myVars.races.race35.event2.loc = "Lu_Id_div_2_LOC";
	myVars.races.race35.event2.isRead = "True";
	myVars.races.race35.event2.eventType = "@event2EventType@";
	myVars.races.race35.event1.executed= false;// true to disable, false to enable
	myVars.races.race35.event2.executed= false;// true to disable, false to enable
</script>

