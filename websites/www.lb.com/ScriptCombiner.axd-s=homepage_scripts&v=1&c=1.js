
(function($){$.fn.extend({center:function(x,y){return this.each(function(){if(x==undefined){x=true;}
if(y==undefined){y=true;}
var $this=$(this);var $window=$(window);$this.css("position","absolute");if(x){var left=($window.width()-$this.outerWidth())/2+$window.scrollLeft();$this.css('left',left)}
if(!y==false){var top=($window.height()-$this.outerHeight())/2+$window.scrollTop();$this.css('top',top);}
return $(this);});}});})(jQuery);﻿
(function($){$.fn.fadeMessage=function(msg,options){var imagepath="/assets/images/admin/";var opts=$.extend({},$.fn.fadeMessage.defaults,options);var msgHtml="<div class='fade_message'></div><div id='fade_message' title='click to close'><img id='fade_close' src='"+imagepath+"closebutton.png' alt='close' /><div id='fade_message_text'></div></div>";var o=$.meta?$.extend({},opts,$(this).data()):opts;$(this).prepend(msgHtml);$msg=$(this).find("#fade_message");$container=$(this).find(".fade_message");if(o.bg!='true')
$container.hide();switch(o.type){case'0':$msg.css("background-image","url("+imagepath+"info.gif)");break;case'1':$msg.css("background-image","url("+imagepath+"question.gif)");break;case'2':$msg.css("background-image","url("+imagepath+"warning.gif)");break;case'3':$msg.css("background-image","url("+imagepath+"error.gif)");break;default:$msg.css("background-image","url("+imagepath+"info.gif)");}
$msg.css("background-repeat","no-repeat");if(o.backgroundcolor.length>0)
$msg.css("background-color",o.backgroundcolor);if(o.bordercolor.length>0)
$msg.css("border-color",o.bordercolor);if(msg.length>300)
$msg.width("600px");$msg.html(msg);$msg.show();$msg.center();if(o.allowClose=='true'){$msg.click(function(){$msg.hide();$container.hide();if(o.redirect&&o.redirect.length>0)
document.location=o.redirect;});}
if(o.fadeOut!='false'){$container.hide();$msg.animate({opacity:1.0},o.timeout);$msg.fadeOut(o.speed);}
else{$container.height($(document).height());}}
$.fn.fadeMessage.defaults={type:'0',fadeOut:'false',bg:'false',redirect:'',timeout:2000,speed:1000,backgroundcolor:'',bordercolor:'',allowClose:'true'};})(jQuery);function displayFadeMessage(msg,type,fadeOut,modalBg,redirect,bck,brdr,allowClose){$(document).ready(function(){var options={type:type,fadeOut:fadeOut,bg:modalBg,redirect:redirect,backgroundcolor:bck,bordercolor:brdr,allowClose:allowClose}
try
{$("body").fadeMessage(msg,options);}
catch(e)
{alert(msg);}});}﻿
var jaaulde=window.jaaulde||{};jaaulde.utils=jaaulde.utils||{};jaaulde.utils.cookies=(function()
{var cookies=[];var defaultOptions={hoursToLive:null,path:'/',domain:null,secure:false};var resolveOptions=function(options)
{var returnValue;if(typeof options!=='object'||options===null)
{returnValue=defaultOptions;}
else
{returnValue={hoursToLive:(typeof options.hoursToLive==='number'&&options.hoursToLive!==0?options.hoursToLive:defaultOptions.hoursToLive),path:(typeof options.path==='string'&&options.path!==''?options.path:defaultOptions.path),domain:(typeof options.domain==='string'&&options.domain!==''?options.domain:defaultOptions.domain),secure:(typeof options.secure==='boolean'&&options.secure?options.secure:defaultOptions.secure)};}
return returnValue;};var expiresGMTString=function(hoursToLive)
{var dateObject=new Date();dateObject.setTime(dateObject.getTime()+(hoursToLive*60*60*1000));return dateObject.toGMTString();};var assembleOptionsString=function(options)
{options=resolveOptions(options);return((typeof options.hoursToLive==='number'?'; expires='+expiresGMTString(options.hoursToLive):'')+'; path='+options.path+
(typeof options.domain==='string'?'; domain='+options.domain:'')+
(options.secure===true?'; secure':''));};var splitCookies=function()
{cookies={};var pair,name,value,separated=document.cookie.split(';');for(var i=0;i<separated.length;i=i+1)
{pair=separated[i].split('=');name=pair[0].replace(/^\s*/,'').replace(/\s*$/,'');value=decodeURIComponent(pair[1]);cookies[name]=value;}
return cookies;};var constructor=function(){};constructor.prototype.get=function(cookieName)
{var returnValue;splitCookies();if(typeof cookieName==='string')
{returnValue=(typeof cookies[cookieName]!=='undefined')?cookies[cookieName]:null;}
else if(typeof cookieName==='object'&&cookieName!==null)
{returnValue={};for(var item in cookieName)
{if(typeof cookies[cookieName[item]]!=='undefined')
{returnValue[cookieName[item]]=cookies[cookieName[item]];}
else
{returnValue[cookieName[item]]=null;}}}
else
{returnValue=cookies;}
return returnValue;};constructor.prototype.filter=function(cookieNameRegExp)
{var returnValue={};splitCookies();if(typeof cookieNameRegExp==='string')
{cookieNameRegExp=new RegExp(cookieNameRegExp);}
for(var cookieName in cookies)
{if(cookieName.match(cookieNameRegExp))
{returnValue[cookieName]=cookies[cookieName];}}
return returnValue;};constructor.prototype.set=function(cookieName,value,options)
{if(typeof value==='undefined'||value===null)
{if(typeof options!=='object'||options===null)
{options={};}
value='';options.hoursToLive=-8760;}
var optionsString=assembleOptionsString(options);document.cookie=cookieName+'='+encodeURIComponent(value)+optionsString;};constructor.prototype.del=function(cookieName,options)
{var allCookies={};if(typeof options!=='object'||options===null)
{options={};}
if(typeof cookieName==='boolean'&&cookieName===true)
{allCookies=this.get();}
else if(typeof cookieName==='string')
{allCookies[cookieName]=true;}
for(var name in allCookies)
{if(typeof name==='string'&&name!=='')
{this.set(name,null,options);}}};constructor.prototype.test=function()
{var returnValue=false,testName='cT',testValue='data';this.set(testName,testValue);if(this.get(testName)===testValue)
{this.del(testName);returnValue=true;}
return returnValue;};constructor.prototype.setOptions=function(options)
{if(typeof options!=='object')
{options=null;}
defaultOptions=resolveOptions(options);};return new constructor();})();(function()
{if(window.jQuery)
{(function($)
{$.cookies=jaaulde.utils.cookies;var extensions={cookify:function(options)
{return this.each(function()
{var i,resolvedName=false,resolvedValue=false,name='',value='',nameAttrs=['name','id'],nodeName,inputType;for(i in nameAttrs)
{if(!isNaN(i))
{name=$(this).attr(nameAttrs[i]);if(typeof name==='string'&&name!=='')
{resolvedName=true;break;}}}
if(resolvedName)
{nodeName=this.nodeName.toLowerCase();if(nodeName!=='input'&&nodeName!=='textarea'&&nodeName!=='select'&&nodeName!=='img')
{value=$(this).html();resolvedValue=true;}
else
{inputType=$(this).attr('type');if(typeof inputType==='string'&&inputType!=='')
{inputType=inputType.toLowerCase();}
if(inputType!=='radio'&&inputType!=='checkbox')
{value=$(this).val();resolvedValue=true;}}
if(resolvedValue)
{if(typeof value!=='string'||value==='')
{value=null;}
$.cookies.set(name,value,options);}}});},cookieFill:function()
{return this.each(function()
{var i,resolvedName=false,name='',value,nameAttrs=['name','id'],iteration=0,nodeName;for(i in nameAttrs)
{if(!isNaN(i))
{name=$(this).attr(nameAttrs[i]);if(typeof name==='string'&&name!=='')
{resolvedName=true;break;}}}
if(resolvedName)
{value=$.cookies.get(name);if(value!==null)
{nodeName=this.nodeName.toLowerCase();if(nodeName==='input'||nodeName==='textarea'||nodeName==='select')
{$(this).val(value);}
else
{$(this).html(value);}}}
iteration=0;});},cookieBind:function(options)
{return this.each(function()
{$(this).cookieFill().change(function()
{$(this).cookify(options);});});}};$.each(extensions,function(i)
{$.fn[i]=this;});})(window.jQuery);}})();﻿
$(document).ready(function(){try{if($("#slideShow").length>0){$("#slideShow").slidepanel({autoPlay:true,pause:5,trans:'fade',slideButtonsAutoCreate:false,slideButtonsAutoActions:true,slideButtonsClass:'slideButton',externalAction:function(){$('.slideButton').removeClass('active');$("#tabs #indicator").stop(true)
$('.selected').addClass('active');$("#tabs #indicator").animate({left:$('.selected').position().left},500);}});$("#tabs .slideButton").click(function(){$("#tabs .slideButton").removeClass('selected');var index=($('#tabs .slideButton').index($(this)));$("#slideShow").trigger('loadSlide',index);$("#slideShow").trigger('slidePlay',false);$(this).addClass('selected');});$("#tabHolder").show();}}
catch(e){}
$("#slideShow").removeClass("load");})﻿
$(document).ready(function(){if($.browser.msie&&$.browser.version=="6.0"){$('#banner_logo').attr('src','assets/images/main_logo.gif'/*tpa=http://www.lb.com/assets/images/main_logo.gif*/)};$(".filedownload").each(function(){$(this).attr("href","/handlers/filedownload.ashx?filepath="+$(this).attr("href"));});$("a[target=filedownload]").each(function(){$(this).attr("href","/handlers/filedownload.ashx?filepath="+$(this).attr("href"));});$("#framemask").each(function(){$(this).css("height",$(this).next().css("height"));});$("#mainNavContainer div ul li").hover(function(){$(this).addClass('submenuHover');},function(){$(this).removeClass('submenuHover');});$(".mainNavParent").hover(function(){ul=$(this).find("ul");$(this).addClass('hovered');var position=$(this).position();ul.show();ul.css('position','absolute');ul.css('left',position.left);},function(){$(this).removeClass('hovered');ul=$(this).find("ul");ul.hide();});$(".current").parent().show();$(".group").click(function(){ul=$(this).next();ul.slideToggle();});var mainAlert='mainAlert';var smallAlert='smallAlert';if(!AlertShown(mainAlert)){$("#rumorControl").animate({height:"200px"},1500);}
else{$("#rumorControl").hide();}
if(!AlertShown(smallAlert)){$("#smallRumorControl").show();}
else{$("#smallRumorControl").hide();}
$("#rumorClose").click(function(){HideAlert(mainAlert);$("#rumorControl").slideUp();});$("#smallRumorClose").click(function(){HideAlert(smallAlert);$("#smallRumorControl").slideUp();});$(".watermark").focus(function(){$(this).val("");$(this).removeClass("watermark");});$(".watermark").each(function(){if($(this).val()!="Search"){$(this).removeClass("watermark");}});$(".watermark").blur(function(){if($(this).val().length==0){$(this).addClass("watermark");$(this).val("Search");}});$("#banner_logo").click(function(){document.location="index.htm"/*tpa=http://www.lb.com/*/;});$(".summaryToggle").click(function(){$(this).next().slideToggle();});$("#footerlinks li:first-child a").css("color","#9d9d9d");$("#rightNav .sf_simpleList li:last-child").css("border","none");$("#rightNav .sf_simpleList li:last-child").css("padding","0");$("#breadcrumb").css("color","#033F73");try{if($("#breadcrumb").length>0){if($("#breadcrumb").html().length===0){$("#breadcrumb").hide();}}}
catch(e){}
$(".eventPanel table td").each(function(index){if($(this).text().length===0){$(this).parent().css("display","none");}});$("iframe").load(function(){$("#framemask").fadeOut("slow");});if($("#int_slideShow").length>0){if($("#int_slideShow li").size()>1){$("#int_slideShow").slidepanel({autoPlay:false,trans:'fade',slideButtonsAutoActions:true,nextBtn:'.next',prevBtn:'.prev'});}
else{$("#int_slideShow .prev").hide();$("#int_slideShow .next").hide();}
$("#int_slideShow").fadeIn()}
$("a").each(function(){if($(this).attr("href")=="undefined"||$(this).attr("href")==null){return;}
if($(this).attr("href").indexOf("pdf")>-1){$(this).attr("target","_blank");}
if($(this).attr("href").indexOf("http://www.lb.com/investors/financial_information/financial_packet.aspx")>-1){$(this).attr("target","_blank");}});$(".ecb_title").each(function(){$(this).click(function(){$(this).next().slideToggle("slow");})});})
function HideSitefinityClasses(){}
function AlertShown(cookieName){try{if($.cookies.get(cookieName)===null){return false;}
else{return true;}}
catch(e)
{}}
function HideAlert(cookieName){AlertShownCookieExpires=new Date();AlertShownCookieExpires.setTime(AlertShownCookieExpires.getTime()+(1000*60));$.cookies.set(cookieName,'1',{expiresAt:AlertShownCookieExpires});}﻿
$(document).ready(function(){$("#framemask").fadeOut("slow");if(document.location.href.indexOf("cmspagemode=preview")>-1){$("#framemask").hide();}
if(document.location.href.indexOf("cmspagemode=edit")>-1){$("#rumorControl").show();$("#middleSection").css("overflow","auto");$("#banner").hover(function(){$(this).hide();});$(".mainNavParent").hover(function(){ul=$(this).find("ul");ul.hide();});$("#contentTwo").css("min-height","2000px");$("#slides").css("position","relative");$("#slides").css("top","100px");$("#slideShow div").css("display","none");$("iframe").attr("src","http://www.lb.com/iframe.htm");$(".filedownload-container").parent().css("width","170px");}});﻿
(function($){$.fn.slidepanel=function(options){var defaults={pause:5,start:0,autoPlay:true,loop:true,height:null,playBtn:null,pauseBtn:null,nextBtn:null,prevBtn:null,slideButtons:null,slideButtonsAutoCreate:true,slideButtonsClass:"button",slideButtonsSelectedClass:"selected",endOfLoopClass:"endOfLoop",grouped:true,trans:'fade',transSpeed:500,externalAction:function(){}};var options=$.extend(defaults,options);return this.each(function(){var parent=this;var current=options.start;var prev=null;var playStatus=true;var slideButtons=null;$(this).append("<div class='ejb_timer'></div>");var timer=$(this).children('.ejb_timer');function setup(){var temp=$(parent).children('li');if(options.trans.toLowerCase()=='fade'){$(temp).css({'display':'none','position':'absolute'});}
if(options.trans.toLowerCase()=='slide'){$(temp).css({'float':'left'});var element=document.createElement("div");$(parent).css({'overflow':'hidden'});$(element).css('width',500000);$(temp).wrapAll($(element));var panelWidth=$(parent).width();var widthIndex=0;var distanceTillNextSlide=0;$(temp).removeClass('slidePanelMarker');$(temp).each(function(){$(this).data('position',{left:widthIndex});var oldWidth=widthIndex;widthIndex=parseInt($(this).outerWidth(true))+widthIndex;if(options.grouped){if(widthIndex>distanceTillNextSlide){$(this).addClass('slidePanelMarker');distanceTillNextSlide=oldWidth+panelWidth;}}});if(options.grouped){$(temp[0]).addClass('slidePanelMarker');temp=$(parent).find('.slidePanelMarker');}}
return(temp);}
var obj=setup();if(options.playBtn){$(options.playBtn).click(function(){slidePlay(true);});}
if(options.pauseBtn){$(options.pauseBtn).click(function(){slidePlay(false);});}
if(options.nextBtn){$(options.nextBtn).click(function(){slideNext();});}
if(options.prevBtn){$(options.prevBtn).click(function(){slidePrev();});}
if(options.slideButtons){for(i=0;i!=obj.length;i++){var element=document.createElement("div");$(element).addClass(options.slideButtonsClass);$(element).data('slideIndex',i);$(options.slideButtons).append($(element));}
slideButtons=$('.'+options.slideButtonsClass);$(slideButtons).click(function(){if($(this).data('slideIndex')!=current){current=$(this).data('slideIndex');$(timer).stop(true);loadSlide(current);}});}
if(!options.slideButtonsAutoCreate){slideButtons=$('.'+options.slideButtonsClass);}
function loopStatus(cur){if(cur<=0){$(options.prevBtn).addClass(options.endOfLoopClass);}else{$(options.prevBtn).removeClass(options.endOfLoopClass);}
if(cur>=obj.length-1){$(options.nextBtn).addClass(options.endOfLoopClass);}else{$(options.nextBtn).removeClass(options.endOfLoopClass);}}
function animateOut(who){switch(options.trans.toLowerCase()){case'fade':$(who).fadeOut(options.transSpeed);break;case'slide':break;}}
function animateIn(who){switch(options.trans.toLowerCase()){case'fade':$(who).fadeIn(options.transSpeed);break;case'slide':$(who).parent().animate({marginLeft:-$(who).data('position').left},options.transSpeed);break;}}
function slidePlay(status){if(status){$(timer).stop(true);options.autoPlay=true;$(timer).fadeTo(options.transSpeed,100,function(){current++;if(current>=obj.length){current=(options.loop)?0:obj.length-1;loadSlide(current);}else{loadSlide(current);}});}else{options.autoPlay=false;$(timer).stop(true);}}
function slidePrev(){current--;$(timer).stop(true);if(current<0){current=(options.loop)?obj.length-1:0;if(options.loop)loadSlide(current);}else{loadSlide(current);}}
function slideNext(){current++;$(timer).stop(true);if(current>obj.length-1){current=(options.loop)?0:obj.length-1;if(options.loop)loadSlide(current);}else{loadSlide(current);}}
function loadSlide(which){animateOut(obj);animateIn(obj[which]);if(!options.loop)loopStatus(which);if(slideButtons){$(slideButtons).removeClass(options.slideButtonsSelectedClass);$(slideButtons[which]).addClass(options.slideButtonsSelectedClass);}
try{options.externalAction();}
catch(err){};if(options.autoPlay){$(timer).fadeTo(options.pause*1000,100,function(){current++;if(current>=obj.length){current=(options.loop)?0:obj.length-1;loadSlide(current);}else{loadSlide(current);}});}}
loadSlide(current);$(this).bind('loadSlide',function(event,slideNumber){current=slideNumber;loadSlide(slideNumber);});$(this).bind('slideNext',function(event){slideNext();});$(this).bind('slidePrev',function(event){slidePrev();});$(this).bind('slidePlay',function(event,status){slidePlay(status);});$(this).bind('changeSetting',function(event,obj){options[obj.setting]=obj.value;});});}})(jQuery);﻿
(function($){var types=['DOMMouseScroll','mousewheel'];$.event.special.mousewheel={setup:function(){if(this.addEventListener){for(var i=types.length;i;){this.addEventListener(types[--i],handler,false);}}else{this.onmousewheel=handler;}},teardown:function(){if(this.removeEventListener){for(var i=types.length;i;){this.removeEventListener(types[--i],handler,false);}}else{this.onmousewheel=null;}}};$.fn.extend({mousewheel:function(fn){return fn?this.bind("mousewheel",fn):this.trigger("mousewheel");},unmousewheel:function(fn){return this.unbind("mousewheel",fn);}});function handler(event){var orgEvent=event||window.event,args=[].slice.call(arguments,1),delta=0,returnValue=true,deltaX=0,deltaY=0;event=$.event.fix(orgEvent);event.type="mousewheel";if(event.wheelDelta){delta=event.wheelDelta/120;}
if(event.detail){delta=-event.detail/3;}
deltaY=delta;if(orgEvent.axis!==undefined&&orgEvent.axis===orgEvent.HORIZONTAL_AXIS){deltaY=0;deltaX=-1*delta;}
if(orgEvent.wheelDeltaY!==undefined){deltaY=orgEvent.wheelDeltaY/120;}
if(orgEvent.wheelDeltaX!==undefined){deltaX=-1*orgEvent.wheelDeltaX/120;}
args.unshift(event,delta,deltaX,deltaY);return $.event.handle.apply(this,args);}})(jQuery);﻿
(function(b,a,c){b.fn.jScrollPane=function(f){function d(C,L){var au,N=this,V,ah,v,aj,Q,W,y,q,av,aB,ap,i,H,h,j,X,R,al,U,t,A,am,ac,ak,F,l,ao,at,x,aq,aE,g,aA,ag=true,M=true,aD=false,k=false,Z=b.fn.mwheelIntent?"http://www.lb.com/mwheelIntent.jsp":"http://www.lb.com/mousewheel.jsp";aE=C.css("paddingTop")+" "+C.css("paddingRight")+" "+C.css("paddingBottom")+" "+C.css("paddingLeft");g=(parseInt(C.css("paddingLeft"))||0)+(parseInt(C.css("paddingRight"))||0);an(L);function an(aH){var aL,aK,aJ,aG,aF,aI;au=aH;if(V==c){C.css({overflow:"hidden",padding:0});ah=C.innerWidth()+g;v=C.innerHeight();C.width(ah);V=b('<div class="jspPane" />').wrap(b('<div class="jspContainer" />').css({width:ah+"px",height:v+"px"}));C.wrapInner(V.parent());aj=C.find(">.jspContainer");V=aj.find(">.jspPane");V.css("padding",aE)}else{C.css("width","");aI=C.outerWidth()+g!=ah||C.outerHeight()!=v;if(aI){ah=C.innerWidth()+g;v=C.innerHeight();aj.css({width:ah+"px",height:v+"px"})}aA=V.innerWidth();if(!aI&&V.outerWidth()==Q&&V.outerHeight()==W){if(aB||av){V.css("width",aA+"px");C.css("width",(aA+g)+"px")}return}V.css("width","");C.css("width",(ah)+"px");aj.find(">.jspVerticalBar,>.jspHorizontalBar").remove().end()}aL=V.clone().css("position","absolute");aK=b('<div style="width:1px; position: relative;" />').append(aL);b("body").append(aK);Q=Math.max(V.outerWidth(),aL.outerWidth());aK.remove();W=V.outerHeight();y=Q/ah;q=W/v;av=q>1;aB=y>1;if(!(aB||av)){C.removeClass("jspScrollable");V.css({top:0,width:aj.width()-g});n();D();O();w();af()}else{C.addClass("jspScrollable");aJ=au.maintainPosition&&(H||X);if(aJ){aG=ay();aF=aw()}aC();z();E();if(aJ){K(aG);J(aF)}I();ad();if(au.enableKeyboardNavigation){P()}if(au.clickOnTrack){p()}B();if(au.hijackInternalLinks){m()}}if(au.autoReinitialise&&!aq){aq=setInterval(function(){an(au)},au.autoReinitialiseDelay)}else{if(!au.autoReinitialise&&aq){clearInterval(aq)}}C.trigger("jsp-initialised",[aB||av])}function aC(){if(av){aj.append(b('<div class="jspVerticalBar" />').append(b('<div class="jspCap jspCapTop" />'),b('<div class="jspTrack" />').append(b('<div class="jspDrag" />').append(b('<div class="jspDragTop" />'),b('<div class="jspDragBottom" />'))),b('<div class="jspCap jspCapBottom" />')));R=aj.find(">.jspVerticalBar");al=R.find(">.jspTrack");ap=al.find(">.jspDrag");if(au.showArrows){am=b('<a class="jspArrow jspArrowUp" />').bind("http://www.lb.com/mousedown.jsp",az(0,-1)).bind("http://www.lb.com/click.jsp",ax);ac=b('<a class="jspArrow jspArrowDown" />').bind("http://www.lb.com/mousedown.jsp",az(0,1)).bind("http://www.lb.com/click.jsp",ax);if(au.arrowScrollOnHover){am.bind("http://www.lb.com/mouseover.jsp",az(0,-1,am));ac.bind("http://www.lb.com/mouseover.jsp",az(0,1,ac))}ai(al,au.verticalArrowPositions,am,ac)}t=v;aj.find(">.jspVerticalBar>.jspCap:visible,>.jspVerticalBar>.jspArrow").each(function(){t-=b(this).outerHeight()});ap.hover(function(){ap.addClass("jspHover")},function(){ap.removeClass("jspHover")}).bind("http://www.lb.com/mousedown.jsp",function(aF){b("html").bind("http://www.lb.com/dragstart.jsp selectstart.jsp",function(){return false});ap.addClass("jspActive");var s=aF.pageY-ap.position().top;b("html").bind("http://www.lb.com/mousemove.jsp",function(aG){S(aG.pageY-s,false)}).bind("http://www.lb.com/mouseup.jsp mouseleave.jsp",ar);return false});o()}}function o(){al.height(t+"px");H=0;U=au.verticalGutter+al.outerWidth();V.width(ah-U-g);if(R.position().left==0){V.css("margin-left",U+"px")}}function z(){if(aB){aj.append(b('<div class="jspHorizontalBar" />').append(b('<div class="jspCap jspCapLeft" />'),b('<div class="jspTrack" />').append(b('<div class="jspDrag" />').append(b('<div class="jspDragLeft" />'),b('<div class="jspDragRight" />'))),b('<div class="jspCap jspCapRight" />')));ak=aj.find(">.jspHorizontalBar");F=ak.find(">.jspTrack");h=F.find(">.jspDrag");if(au.showArrows){at=b('<a class="jspArrow jspArrowLeft" />').bind("http://www.lb.com/mousedown.jsp",az(-1,0)).bind("http://www.lb.com/click.jsp",ax);x=b('<a class="jspArrow jspArrowRight" />').bind("http://www.lb.com/mousedown.jsp",az(1,0)).bind("http://www.lb.com/click.jsp",ax);if(au.arrowScrollOnHover){at.bind("http://www.lb.com/mouseover.jsp",az(-1,0,at));x.bind("http://www.lb.com/mouseover.jsp",az(1,0,x))}ai(F,au.horizontalArrowPositions,at,x)}h.hover(function(){h.addClass("jspHover")},function(){h.removeClass("jspHover")}).bind("http://www.lb.com/mousedown.jsp",function(aF){b("html").bind("http://www.lb.com/dragstart.jsp selectstart.jsp",function(){return false});h.addClass("jspActive");var s=aF.pageX-h.position().left;b("html").bind("http://www.lb.com/mousemove.jsp",function(aG){T(aG.pageX-s,false)}).bind("http://www.lb.com/mouseup.jsp mouseleave.jsp",ar);return false});l=aj.innerWidth();ae()}else{}}function ae(){aj.find(">.jspHorizontalBar>.jspCap:visible,>.jspHorizontalBar>.jspArrow").each(function(){l-=b(this).outerWidth()});F.width(l+"px");X=0}function E(){if(aB&&av){var aF=F.outerHeight(),s=al.outerWidth();t-=aF;b(ak).find(">.jspCap:visible,>.jspArrow").each(function(){l+=b(this).outerWidth()});l-=s;v-=s;ah-=aF;F.parent().append(b('<div class="jspCorner" />').css("width",aF+"px"));o();ae()}if(aB){V.width((aj.outerWidth()-g)+"px")}W=V.outerHeight();q=W/v;if(aB){ao=1/y*l;if(ao>au.horizontalDragMaxWidth){ao=au.horizontalDragMaxWidth}else{if(ao<au.horizontalDragMinWidth){ao=au.horizontalDragMinWidth}}h.width(ao+"px");j=l-ao;ab(X)}if(av){A=1/q*t;if(A>au.verticalDragMaxHeight){A=au.verticalDragMaxHeight}else{if(A<au.verticalDragMinHeight){A=au.verticalDragMinHeight}}ap.height(A+"px");i=t-A;aa(H)}}function ai(aG,aI,aF,s){var aK="before",aH="after",aJ;if(aI=="os"){aI=/Mac/.test(navigator.platform)?"after":"split"}if(aI==aK){aH=aI}else{if(aI==aH){aK=aI;aJ=aF;aF=s;s=aJ}}aG[aK](aF)[aH](s)}function az(aF,s,aG){return function(){G(aF,s,this,aG);this.blur();return false}}function G(aH,aF,aK,aJ){aK=b(aK).addClass("jspActive");var aI,s=function(){if(aH!=0){T(X+aH*au.arrowButtonSpeed,false)}if(aF!=0){S(H+aF*au.arrowButtonSpeed,false)}},aG=setInterval(s,au.arrowRepeatFreq);s();aI=aJ==c?"http://www.lb.com/mouseup.jsp":"http://www.lb.com/mouseout.jsp";aJ=aJ||b("html");aJ.bind(aI,function(){aK.removeClass("jspActive");clearInterval(aG);aJ.unbind(aI)})}function p(){w();if(av){al.bind("http://www.lb.com/mousedown.jsp",function(aH){if(aH.originalTarget==c||aH.originalTarget==aH.currentTarget){var aG=b(this),s=setInterval(function(){var aI=aG.offset(),aJ=aH.pageY-aI.top;if(H+A<aJ){S(H+au.trackClickSpeed)}else{if(aJ<H){S(H-au.trackClickSpeed)}else{aF()}}},au.trackClickRepeatFreq),aF=function(){s&&clearInterval(s);s=null;b(document).unbind("http://www.lb.com/mouseup.jsp",aF)};b(document).bind("http://www.lb.com/mouseup.jsp",aF);return false}})}if(aB){F.bind("http://www.lb.com/mousedown.jsp",function(aH){if(aH.originalTarget==c||aH.originalTarget==aH.currentTarget){var aG=b(this),s=setInterval(function(){var aI=aG.offset(),aJ=aH.pageX-aI.left;if(X+ao<aJ){T(X+au.trackClickSpeed)}else{if(aJ<X){T(X-au.trackClickSpeed)}else{aF()}}},au.trackClickRepeatFreq),aF=function(){s&&clearInterval(s);s=null;b(document).unbind("http://www.lb.com/mouseup.jsp",aF)};b(document).bind("http://www.lb.com/mouseup.jsp",aF);return false}})}}function w(){F&&F.unbind("http://www.lb.com/mousedown.jsp");al&&al.unbind("http://www.lb.com/mousedown.jsp")}function ar(){b("html").unbind("http://www.lb.com/dragstart.jsp selectstart.jsp mousemove.jsp mouseup.jsp mouseleave.jsp");ap&&ap.removeClass("jspActive");h&&h.removeClass("jspActive")}function S(s,aF){if(!av){return}if(s<0){s=0}else{if(s>i){s=i}}if(aF==c){aF=au.animateScroll}if(aF){N.animate(ap,"top",s,aa)}else{ap.css("top",s);aa(s)}}function aa(aF){if(aF==c){aF=ap.position().top}aj.scrollTop(0);H=aF;var aI=H==0,aG=H==i,aH=aF/i,s=-aH*(W-v);if(ag!=aI||aD!=aG){ag=aI;aD=aG;C.trigger("jsp-arrow-change",[ag,aD,M,k])}u(aI,aG);V.css("top",s);C.trigger("jsp-scroll-y",[-s,aI,aG])}function T(aF,s){if(!aB){return}if(aF<0){aF=0}else{if(aF>j){aF=j}}if(s==c){s=au.animateScroll}if(s){N.animate(h,"left",aF,ab)}else{h.css("left",aF);ab(aF)}}function ab(aF){if(aF==c){aF=h.position().left}aj.scrollTop(0);X=aF;var aI=X==0,aH=X==j,aG=aF/j,s=-aG*(Q-ah);if(M!=aI||k!=aH){M=aI;k=aH;C.trigger("jsp-arrow-change",[ag,aD,M,k])}r(aI,aH);V.css("left",s);C.trigger("jsp-scroll-x",[-s,aI,aH])}function u(aF,s){if(au.showArrows){am[aF?"addClass":"removeClass"]("jspDisabled");ac[s?"addClass":"removeClass"]("jspDisabled")}}function r(aF,s){if(au.showArrows){at[aF?"addClass":"removeClass"]("jspDisabled");x[s?"addClass":"removeClass"]("jspDisabled")}}function J(s,aF){var aG=s/(W-v);S(aG*i,aF)}function K(aF,s){var aG=aF/(Q-ah);T(aG*j,s)}function Y(aR,aM,aG){var aK,aH,aI,s=0,aQ=0,aF,aL,aO,aN,aP;try{aK=b(aR)}catch(aJ){return}aH=aK.outerHeight();aI=aK.outerWidth();aj.scrollTop(0);aj.scrollLeft(0);while(!aK.is(".jspPane")){s+=aK.position().top;aQ+=aK.position().left;aK=aK.offsetParent();if(/^body|html$/i.test(aK[0].nodeName)){return}}aF=aw();aL=aF+v;if(s<aF||aM){aN=s-au.verticalGutter}else{if(s+aH>aL){aN=s-v+aH+au.verticalGutter}}if(aN){J(aN,aG)}viewportLeft=ay();aO=viewportLeft+ah;if(aQ<viewportLeft||aM){aP=aQ-au.horizontalGutter}else{if(aQ+aI>aO){aP=aQ-ah+aI+au.horizontalGutter}}if(aP){K(aP,aG)}}function ay(){return-V.position().left}function aw(){return-V.position().top}function ad(){aj.unbind(Z).bind(Z,function(aI,aJ,aH,aF){var aG=X,s=H;T(X+aH*au.mouseWheelSpeed,false);S(H-aF*au.mouseWheelSpeed,false);return aG==X&&s==H})}function n(){aj.unbind(Z)}function ax(){return false}function I(){V.unbind("http://www.lb.com/focusin.jsp").bind("http://www.lb.com/focusin.jsp",function(s){if(s.target===V[0]){return}Y(s.target,false)})}function D(){V.unbind("http://www.lb.com/focusin.jsp")}function P(){var aF,s;C.attr("tabindex",0).unbind("http://www.lb.com/keydown.jsp").bind("http://www.lb.com/keydown.jsp",function(aJ){if(aJ.target!==C[0]){return}var aH=X,aG=H,aI=aF?2:16;switch(aJ.keyCode){case 40:S(H+aI,false);break;case 38:S(H-aI,false);break;case 34:case 32:J(aw()+Math.max(32,v)-16);break;case 33:J(aw()-v+16);break;case 35:J(W-v);break;case 36:J(0);break;case 39:T(X+aI,false);break;case 37:T(X-aI,false);break}if(!(aH==X&&aG==H)){aF=true;clearTimeout(s);s=setTimeout(function(){aF=false},260);return false}});if(au.hideFocus){C.css("outline","none");if("hideFocus"in aj[0]){C.attr("hideFocus",true)}}else{C.css("outline","");if("hideFocus"in aj[0]){C.attr("hideFocus",false)}}}function O(){C.attr("tabindex","-1").removeAttr("tabindex").unbind("http://www.lb.com/keydown.jsp")}function B(){if(location.hash&&location.hash.length>1){var aG,aF;try{aG=b(location.hash)}catch(s){return}if(aG.length&&V.find(aG)){if(aj.scrollTop()==0){aF=setInterval(function(){if(aj.scrollTop()>0){Y(location.hash,true);b(document).scrollTop(aj.position().top);clearInterval(aF)}},50)}else{Y(location.hash,true);b(document).scrollTop(aj.position().top)}}}}function af(){b("a.jspHijack").unbind("click.jsp-hijack").removeClass("jspHijack")}function m(){af();b("a[href^=#]").addClass("jspHijack").bind("click.jsp-hijack",function(){var s=this.href.split("#"),aF;if(s.length>1){aF=s[1];if(aF.length>0&&V.find("#"+aF).length>0){Y("#"+aF,true);return false}}})}b.extend(N,{reinitialise:function(aF){aF=b.extend({},aF,au);an(aF)},scrollToElement:function(aG,aF,s){Y(aG,aF,s)},scrollTo:function(aG,s,aF){K(aG,aF);J(s,aF)},scrollToX:function(aF,s){K(aF,s)},scrollToY:function(s,aF){J(s,aF)},scrollBy:function(aF,s,aG){N.scrollByX(aF,aG);N.scrollByY(s,aG)},scrollByX:function(s,aG){var aF=ay()+s,aH=aF/(Q-ah);T(aH*j,aG)},scrollByY:function(s,aG){var aF=aw()+s,aH=aF/(W-v);S(aH*i,aG)},animate:function(aF,aI,s,aH){var aG={};aG[aI]=s;aF.animate(aG,{duration:au.animateDuration,ease:au.animateEase,queue:false,step:aH})},getContentPositionX:function(){return ay()},getContentPositionY:function(){return aw()},getIsScrollableH:function(){return aB},getIsScrollableV:function(){return av},getContentPane:function(){return V},scrollToBottom:function(s){S(i,s)},hijackInternalLinks:function(){m()}})}f=b.extend({},b.fn.jScrollPane.defaults,f);var e;this.each(function(){var g=b(this),h=g.data("jsp");if(h){h.reinitialise(f)}else{h=new d(g,f);g.data("jsp",h)}e=e?e.add(g):g});return e};b.fn.jScrollPane.defaults={showArrows:false,maintainPosition:true,clickOnTrack:true,autoReinitialise:false,autoReinitialiseDelay:500,verticalDragMinHeight:0,verticalDragMaxHeight:99999,horizontalDragMinWidth:0,horizontalDragMaxWidth:99999,animateScroll:false,animateDuration:300,animateEase:"linear",hijackInternalLinks:false,verticalGutter:4,horizontalGutter:4,mouseWheelSpeed:10,arrowButtonSpeed:10,arrowRepeatFreq:100,arrowScrollOnHover:false,trackClickSpeed:30,trackClickRepeatFreq:100,verticalArrowPositions:"split",horizontalArrowPositions:"split",enableKeyboardNavigation:true,hideFocus:false}})(jQuery,this);