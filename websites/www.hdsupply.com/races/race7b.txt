<script id = "race7b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race7={};
	myVars.races.race7.varName="facilities-maintenance__onmouseover";
	myVars.races.race7.varType="@varType@";
	myVars.races.race7.repairType = "@RepairType";
	myVars.races.race7.event1={};
	myVars.races.race7.event2={};
	myVars.races.race7.event1.id = "facilities-maintenance";
	myVars.races.race7.event1.type = "onmouseover";
	myVars.races.race7.event1.loc = "facilities-maintenance_LOC";
	myVars.races.race7.event1.isRead = "True";
	myVars.races.race7.event1.eventType = "@event1EventType@";
	myVars.races.race7.event2.id = "Lu_DOM";
	myVars.races.race7.event2.type = "onDOMContentLoaded";
	myVars.races.race7.event2.loc = "Lu_DOM_LOC";
	myVars.races.race7.event2.isRead = "False";
	myVars.races.race7.event2.eventType = "@event2EventType@";
	myVars.races.race7.event1.executed= false;// true to disable, false to enable
	myVars.races.race7.event2.executed= false;// true to disable, false to enable
</script>

