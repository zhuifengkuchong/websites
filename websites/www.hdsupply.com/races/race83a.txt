<script id = "race83a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race83={};
	myVars.races.race83.varName="Object[1249].triggered";
	myVars.races.race83.varType="@varType@";
	myVars.races.race83.repairType = "@RepairType";
	myVars.races.race83.event1={};
	myVars.races.race83.event2={};
	myVars.races.race83.event1.id = "whitecap";
	myVars.races.race83.event1.type = "onmouseout";
	myVars.races.race83.event1.loc = "whitecap_LOC";
	myVars.races.race83.event1.isRead = "False";
	myVars.races.race83.event1.eventType = "@event1EventType@";
	myVars.races.race83.event2.id = "cti";
	myVars.races.race83.event2.type = "onmouseout";
	myVars.races.race83.event2.loc = "cti_LOC";
	myVars.races.race83.event2.isRead = "True";
	myVars.races.race83.event2.eventType = "@event2EventType@";
	myVars.races.race83.event1.executed= false;// true to disable, false to enable
	myVars.races.race83.event2.executed= false;// true to disable, false to enable
</script>

