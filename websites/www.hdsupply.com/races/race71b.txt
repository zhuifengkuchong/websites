<script id = "race71b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race71={};
	myVars.races.race71.varName="Object[1249].triggered";
	myVars.races.race71.varType="@varType@";
	myVars.races.race71.repairType = "@RepairType";
	myVars.races.race71.event1={};
	myVars.races.race71.event2={};
	myVars.races.race71.event1.id = "Lu_Id_a_33";
	myVars.races.race71.event1.type = "onmouseout";
	myVars.races.race71.event1.loc = "Lu_Id_a_33_LOC";
	myVars.races.race71.event1.isRead = "False";
	myVars.races.race71.event1.eventType = "@event1EventType@";
	myVars.races.race71.event2.id = "buildyourcity";
	myVars.races.race71.event2.type = "onmouseover";
	myVars.races.race71.event2.loc = "buildyourcity_LOC";
	myVars.races.race71.event2.isRead = "True";
	myVars.races.race71.event2.eventType = "@event2EventType@";
	myVars.races.race71.event1.executed= false;// true to disable, false to enable
	myVars.races.race71.event2.executed= false;// true to disable, false to enable
</script>

