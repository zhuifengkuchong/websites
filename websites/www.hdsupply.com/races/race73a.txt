<script id = "race73a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race73={};
	myVars.races.race73.varName="Object[1249].triggered";
	myVars.races.race73.varType="@varType@";
	myVars.races.race73.repairType = "@RepairType";
	myVars.races.race73.event1={};
	myVars.races.race73.event2={};
	myVars.races.race73.event1.id = "Lu_Id_div_4";
	myVars.races.race73.event1.type = "onmouseover";
	myVars.races.race73.event1.loc = "Lu_Id_div_4_LOC";
	myVars.races.race73.event1.isRead = "True";
	myVars.races.race73.event1.eventType = "@event1EventType@";
	myVars.races.race73.event2.id = "Lu_Id_a_33";
	myVars.races.race73.event2.type = "onmouseout";
	myVars.races.race73.event2.loc = "Lu_Id_a_33_LOC";
	myVars.races.race73.event2.isRead = "False";
	myVars.races.race73.event2.eventType = "@event2EventType@";
	myVars.races.race73.event1.executed= false;// true to disable, false to enable
	myVars.races.race73.event2.executed= false;// true to disable, false to enable
</script>

