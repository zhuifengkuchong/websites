<script id = "race30b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race30={};
	myVars.races.race30.varName="Lu_Id_div_4__onmouseout";
	myVars.races.race30.varType="@varType@";
	myVars.races.race30.repairType = "@RepairType";
	myVars.races.race30.event1={};
	myVars.races.race30.event2={};
	myVars.races.race30.event1.id = "buildyourcity";
	myVars.races.race30.event1.type = "onmouseout";
	myVars.races.race30.event1.loc = "buildyourcity_LOC";
	myVars.races.race30.event1.isRead = "True";
	myVars.races.race30.event1.eventType = "@event1EventType@";
	myVars.races.race30.event2.id = "Lu_Id_script_13";
	myVars.races.race30.event2.type = "Lu_Id_script_13__parsed";
	myVars.races.race30.event2.loc = "Lu_Id_script_13_LOC";
	myVars.races.race30.event2.isRead = "False";
	myVars.races.race30.event2.eventType = "@event2EventType@";
	myVars.races.race30.event1.executed= false;// true to disable, false to enable
	myVars.races.race30.event2.executed= false;// true to disable, false to enable
</script>

