<script id = "race43b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race43={};
	myVars.races.race43.varName="repair-remodel__onmouseout";
	myVars.races.race43.varType="@varType@";
	myVars.races.race43.repairType = "@RepairType";
	myVars.races.race43.event1={};
	myVars.races.race43.event2={};
	myVars.races.race43.event1.id = "repair-remodel";
	myVars.races.race43.event1.type = "onmouseout";
	myVars.races.race43.event1.loc = "repair-remodel_LOC";
	myVars.races.race43.event1.isRead = "True";
	myVars.races.race43.event1.eventType = "@event1EventType@";
	myVars.races.race43.event2.id = "Lu_DOM";
	myVars.races.race43.event2.type = "onDOMContentLoaded";
	myVars.races.race43.event2.loc = "Lu_DOM_LOC";
	myVars.races.race43.event2.isRead = "False";
	myVars.races.race43.event2.eventType = "@event2EventType@";
	myVars.races.race43.event1.executed= false;// true to disable, false to enable
	myVars.races.race43.event2.executed= false;// true to disable, false to enable
</script>

