<script id = "race66a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race66={};
	myVars.races.race66.varName="Object[1249].triggered";
	myVars.races.race66.varType="@varType@";
	myVars.races.race66.repairType = "@RepairType";
	myVars.races.race66.event1={};
	myVars.races.race66.event2={};
	myVars.races.race66.event1.id = "cti";
	myVars.races.race66.event1.type = "onmouseover";
	myVars.races.race66.event1.loc = "cti_LOC";
	myVars.races.race66.event1.isRead = "True";
	myVars.races.race66.event1.eventType = "@event1EventType@";
	myVars.races.race66.event2.id = "Lu_Id_a_33";
	myVars.races.race66.event2.type = "onmouseout";
	myVars.races.race66.event2.loc = "Lu_Id_a_33_LOC";
	myVars.races.race66.event2.isRead = "False";
	myVars.races.race66.event2.eventType = "@event2EventType@";
	myVars.races.race66.event1.executed= false;// true to disable, false to enable
	myVars.races.race66.event2.executed= false;// true to disable, false to enable
</script>

