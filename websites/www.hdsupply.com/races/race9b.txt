<script id = "race9b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race9={};
	myVars.races.race9.varName="waterworks__onmouseover";
	myVars.races.race9.varType="@varType@";
	myVars.races.race9.repairType = "@RepairType";
	myVars.races.race9.event1={};
	myVars.races.race9.event2={};
	myVars.races.race9.event1.id = "waterworks";
	myVars.races.race9.event1.type = "onmouseover";
	myVars.races.race9.event1.loc = "waterworks_LOC";
	myVars.races.race9.event1.isRead = "True";
	myVars.races.race9.event1.eventType = "@event1EventType@";
	myVars.races.race9.event2.id = "Lu_DOM";
	myVars.races.race9.event2.type = "onDOMContentLoaded";
	myVars.races.race9.event2.loc = "Lu_DOM_LOC";
	myVars.races.race9.event2.isRead = "False";
	myVars.races.race9.event2.eventType = "@event2EventType@";
	myVars.races.race9.event1.executed= false;// true to disable, false to enable
	myVars.races.race9.event2.executed= false;// true to disable, false to enable
</script>

