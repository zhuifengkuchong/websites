function writeLink(name, domain, text, subject) {
	if (subject == undefined) {
		subject = "";
	}
	document.write("<a href='mailto:"+name+"&#64;"+domain+"?subject="+subject+"'>"+text+"</a>");
}

sfHover = function() {
	var sfEls = document.getElementById("nav").getElementsByTagName("LI");
	for (var i=0; i<sfEls.length; i++) {
		sfEls[i].onmouseover=function() {
			this.className+=" sfhover";
		}
		sfEls[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" sfhover\\b"), "");
		}
	}
}

if (window.attachEvent) window.attachEvent("onload", sfHover);

if(typeof sIFR == "function"){
    sIFR.replaceElement("h1", named({sFlashSrc: "Unknown_83_filename"/*tpa=http://11.224.42.80/www.hdsupply.com/media/gotham-medium.swf*/, sColor: "#000000", sCase: "upper", sWmode: "transparent"}));
	sIFR.replaceElement("h2", named({sFlashSrc: "Unknown_83_filename"/*tpa=http://11.224.42.80/www.hdsupply.com/media/gotham-medium.swf*/, sColor: "#666666", sCase: "upper", sWmode: "transparent"}));
	sIFR.replaceElement("h3", named({sFlashSrc: "Unknown_83_filename"/*tpa=http://11.224.42.80/www.hdsupply.com/media/gotham-medium.swf*/, sColor: "#FFFFFF", sCase: "upper", sWmode: "transparent"}));
};

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}
//-->