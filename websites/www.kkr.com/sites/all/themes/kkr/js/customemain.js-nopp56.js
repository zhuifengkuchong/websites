$(document).ready( function() {
 // add by E0817 for the video section 
	  $('.btn-play').click(function(){
		videowidth = $(".video-content-block-img").width();
		videoheight = $(".video-content-block-img").height();
		videourl = $(this).data('divid')+"?autoplay=true";
		
        video = '<aside class="__videoPaly"><iframe src="'+videourl+'" height="'+videoheight+'" width="'+videowidth+'" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></aside>';
        
        
        $(this).parents(".video-content-block-img").replaceWith(video);
        //$(this).parents(".video-content-block-img").fadeOut(1000);
        //$(this).parents(".video-content-block-img").find('img').hide();
        //$(this).parents(".video-content-block-img").find('.__videoPaly').fadeIn(1000);
        //$(".video-content-block-img").fadeIn(1000);
        //$(this).hide();
        
		return false;
    });

	$('.btn-play-timeline').click(function(){
		videowidth = $(".video-content-block-img").width();
		videoheight = $(".video-content-block-img").height();
		videourl = $(this).data('divid')+"?autoplay=true";
		
        video = '<aside class="__videoPaly"><iframe src="'+videourl+'" height="'+videoheight+'" width="'+videowidth+'" webkitAllowFullScreen mozallowfullscreen allowfullscreen></iframe></aside>';
        
        $(this).parents('.timeline-section').find(".video-content-block-img").replaceWith(video);
        $(this).parents('.timeline-section').find(".video-timeline-txt").fadeOut(1000);
        // $(".video-content-block-img iframe").src(video);
        //$(".video-content-block-img").find('.__videoPaly').show();
        //$(".video-content-block-img").find('img').hide();
        $(this).hide();
        //$(".video-content-block-img").fadeIn(1000);
        
		return false;
    });
});