var host = document.location.host.toString().toLowerCase();
var ext = [".pdf", ".doc", ".xls", ".ppt", ".mp3", ".csv", ".zip", ".wav", ".mov", ".docx", ".xlsx", ".pptx" ];

function autotag() 
{
	for (var i=0; i<document.links.length; i++ )
	{
		iLink = document.links[i];
		if (iLink.href.toLowerCase().indexOf(host) == -1) 
		{
			if (iLink.target == "_blank" || popUp(iLink))
			{
				if(iLink.onclick == "" || iLink.onclick == null)
				{	
					iLink.onclick = function() {ntptEventTag('ev=externallink&link=' + encodeURIComponent(this.href) );};
				}	
				else
				{
					if(window.attachEvent)
					{
						iLink.tmpclick = iLink.onclick;
						iLink.onclick = function() {ntptEventTag('ev=externallink&link=' + encodeURIComponent(this.href) ); return this.tmpclick();};
					}
					else
						EV(iLink, "click", function() {ntptEventTag('ev=externallink&link=' + encodeURIComponent(this.href) );});
				}
			}
			else
			{					
				if(iLink.onclick == "" || iLink.onclick == null)
					iLink.onclick = function() {return ntptLinkTag(this, 'lc=' + encodeURIComponent(document.URL) + '&ev=externallink&link=' + encodeURIComponent(this.href))};
				else
				{
					if (window.attachEvent && iLink.onclick.toString().indexOf("ntptLinkTag") > -1)
					{
						iLink.tmpclick = iLink.onclick;
						iLink.onclick = function() {ntptEventTag('ev=externallink&link=' + encodeURIComponent(this.href) ); return this.tmpclick();};
					}
					else if (window.attachEvent)
					{
						iLink.tmpclick = iLink.onclick;
						iLink.onclick = function() {return ntptLinkTag(this, 'lc=' + encodeURIComponent(document.URL) + '&ev=externallink&link=' + encodeURIComponent(this.href)); return this.tmpclick();};
					}
					else if (iLink.onclick.toString().indexOf("ntptLinkTag") > -1)
						EV(iLink, "mousedown", function() {ntptEventTag('ev=externallink&link=' + encodeURIComponent(this.href) );});
					else
						EV(iLink, "click", function() {return ntptLinkTag(this, 'lc=' + encodeURIComponent(document.URL) + '&ev=externallink&link=' + encodeURIComponent(this.href));});
				}	
			}
		}
		else
		{
			for (var j=0; j<ext.length; j++)
			{
				if (iLink.href.toLowerCase().indexOf(ext[j]) > -1)
				{
					//non-HTML link
					if (iLink.target == "_blank" || popUp(iLink))
					{
						//EV(iLink, "click", function eTag() {alert(arguments[0]); ntptEventTag('lc='  + "&rf=" + encodeURIComponent(document.location)); } );
						if(iLink.onclick == "" || iLink.onclick == null)
						{	
							iLink.onclick = function() {ntptEventTag('lc=' + encodeURIComponent(this.href) + '&rf=' + encodeURIComponent(document.location));};
						}	
						else
						{
							if(window.attachEvent)
							{
								iLink.tmpclick = iLink.onclick;
								iLink.onclick = function() {ntptEventTag('lc=' + encodeURIComponent(this.href) + '&rf=' + encodeURIComponent(document.location)); return this.tmpclick(); };
							}
							else
								EV(iLink, "click", function() {ntptEventTag('lc=' + encodeURIComponent(this.href) + '&rf=' + encodeURIComponent(document.location));});
						}
					}
					else
					{		
						if(iLink.onclick == "" || iLink.onclick == null)
							iLink.onclick = function() {return ntptLinkTag(this)};
						else if (window.attachEvent && iLink.onclick.toString().indexOf("ntptLinkTag") > -1)
					{
						iLink.tmpclick = iLink.onclick;
						iLink.onclick = function() {ntptEventTag('lc=' + encodeURIComponent(this.href) + '&rf=' + encodeURIComponent(document.location)); return this.tmpclick();};
					}
					else if (window.attachEvent)
					{
						iLink.tmpclick = iLink.onclick;
						iLink.onclick = function() {return ntptLinkTag(this); return this.tmpclick();};
					}
					else if (iLink.onclick.toString().indexOf("ntptLinkTag") > -1)
						EV(iLink, "mousedown", function() {ntptEventTag('lc=' + encodeURIComponent(this.href) + '&rf=' + encodeURIComponent(document.location));});
					else
						EV(iLink, "click", function() {return ntptLinkTag(this);});
						}
					
				}
			}
		}
	};
}

function popUp(linkobj)
{
	if (linkobj.onclick != undefined)
		if (linkobj.onclick.toString().toLowerCase().indexOf("http://www.bms.com/javascript/window.open") > -1) 
			return true; 
		else
			return false;
	else
		return false;
}

function EV(a,b,c,d)
{
    if(a.addEventListener)
    {
    	a.addEventListener(b,c,false)
    }
    else if(a.attachEvent)
    {
    	a.attachEvent(((d==1)?"":"on")+b,c)
    }
}

EV(window,"load", autotag);