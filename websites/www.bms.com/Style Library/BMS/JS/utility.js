///////////////////////////////////////////////////////////////////////////////////
// This method pops a window in a new browser
///////////////////////////////////////////////////////////////////////////////////
// Parameters:
//	url			– URL to be displayed in the window
//  name		– Name of the window. Default is "BMS PopUp"
//	width		- Width of the window in pixels
//	height	-	Height of the window in pixels
//	scrollbars	-	yes|no|1|0	- Whether or not to display scroll bars. Default is yes
//	resizable		-	yes|no|1|0	- Whether or not the window is resizable. Default is yes
//	menubar			-	yes|no|1|0	- Whether or not to display the menu bar, toolbar, status bar, and address field. Default is yes
function PopupWindow(url, name, width, height, scrollbars, resizable, menubar)
{ 
	if(name == null || name == "")
	{
		name="BMS PopUp";
	}
	if((scrollbars != 0 && scrollbars != 1) && (scrollbars == null || scrollbars == ""))
	{
		scrollbars = 1;
	}
	if((resizable != 0 && resizable != 1) && (resizable == null || resizable == ""))
	{
		resizable = 1;
	}
	if((menubar != 0 && menubar != 1) && (menubar == null || menubar == ""))
	{
		menubar = 1;
	}
	var windowFeatures = "height="+height+",width="+width+",scrollbars="+scrollbars+",menubar="+menubar+",toolbar="+menubar+",location="+menubar+",status="+menubar+",statusbar="+menubar+",resizable="+resizable;
  window.open(url,name,windowFeatures);
  return false;
}

// Open a new window from drop down.  This is currently only used by
// the LinkPart webpart
function NewLinkWindow() 
{
  var control = document.getElementById('SiteDropDown');
  window.open(control.value, 'this', '', '');
}

// Character Count Script
function KeyUpdate(varTxt, varCount) 
{
  var maxLength = document.getElementById(varTxt).getAttribute("maxlength");
  var text = document.getElementById(varTxt);
  var control = document.getElementById(varCount);
  if (text.value.length > maxLength) 
  {
		text.value = text.value.substring(0, maxLength);
  }
  else 
  {
		control.innerText = maxLength - text.value.length;
  }
}

// Submits form for Product code searches
function submitForm(theForm) 
{
	var lotNum;
	var codeNum;
	codeNum=document.getElementById('WPQ3ndc').value.replace(/-/g, "");
	lotNum=document.getElementById('WPQ3lot').value.toUpperCase();
  myWindow = window.open('http://ctr.bms.com/cpsc/CertificateServlet?ndc=' + codeNum + '&lot=' + 	lotNum,'mywin','left=20,top=20,width=500,height=500,toolbar=1,resizable=1');
  myWindow.focus();
}

// Overrides the ProcessImn function in Core.js.
// Relates to browser issues, see http://support.microsoft.com/?kbid=931509
function ProcessImn()
{
}

// Not sure what this does.  Appears to change the "submit" behavior
// in non-Microsoft browsers.  Method is being called in master pages.
function replaceOnKeyPress()
{ 
	if(navigator.appName.substring(0,9)!= "Microsoft")   
	{
		var myid = '';
		var mysubmitid = '';
		var elem = document.forms['aspnetForm'].elements;
	            
		for(var i = 0; i < elem.length; i++)
		{
			if (elem[i].name.match("InputKeywords"))
			{
				myid = elem[i].id;

				var myIdElements = new Array();                                                        
				myIdElements = myid.split('_');                            

				mysubmitid = myIdElements[myIdElements.length - 2];
			}                
		}                                                                                                    
    
    var theFunction = mysubmitid + "_Submit();";
    var theInputID = myid;
    var txt = document.getElementById(theInputID);
    var browser=navigator.appName;

		if (txt && (browser == "Netscape"))
		{
			txt.onkeypress = function(e)
			{ 
				var key = String.fromCharCode(e.keyCode);

				if (key == "\n" || key == "\r")
				{
						eval(theFunction);
						return false;     
				}  
			}
		}                               
	}
}

// Not currently used (copied from former landing.js)
function createCookie(name,value,days) {
	if (days) 
	{
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else 
	{
		var expires = "";
	}
	document.cookie = name+"="+value+expires;
}

// Not currently used (copied from former landing.js)
function createSessionCookie(name,value) 
{
	document.cookie = name + "=" + value;
}

// Not currently used (copied from former landing.js)
function readCookie(name) 
{
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) 
	{
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

//Added for IPAD/IPHONE Hover worldwide hover issues
$(document).ready(function() {
	if ((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i)) || (navigator.userAgent.match(/iPad/i))) {
		$("#menu li a").click(function() {
			//we just need to attach a click event listener to provoke iPhone/iPod/iPad's hover event
			//strange
		});
	}
});

