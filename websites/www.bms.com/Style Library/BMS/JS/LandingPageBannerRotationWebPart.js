$(document).ready(function() {

	var playFlag = true;

	//get the slide transition delay setting
	var slideTransition = $("#hSlideTransitionDelay").val();

	var numLiItems = $('#Horzslider li').length;

	if (numLiItems > 1) {
		$('#Horzslider').show().InsiteSlider({
			mode: 'fade',
			auto: true,
			autoControls: true,
			autoControlsCombine: true,
			responsive: false,
			speed: 1000,
			pause: 8000,
			autoHidePager: true
		});
	}
	else {
		$('#Horzslider').show().InsiteSlider({
			mode: 'fade',
			auto: true,
			autoControls: false,
			autoControlsCombine: false,
			pager: false,
			responsive: false,
			speed: 1000,
			pause: 8000,
			autoHidePager: true
		});
	}
});

