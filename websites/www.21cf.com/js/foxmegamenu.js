$(window).ready(function() {
	
	
	// **display screen width for testing
   		 var wi = $(window).width();
    	 $("p.testp").text('Screen width is currently: ' + wi + 'px');
        
   		$(window).resize(function() {
	        var wi = $(window).width();
 			// display screen width for testing
	        $("p.testp").text('Screen width is currently: ' + wi + 'px');
            
	    });      
	    
    // speed of animation
    var menuSpeed = 0.2;
    
    // menu arrays
	var triggerMenu = $(".mMenu");
	var nochildMenu = $(".nochild");
	var megaMenu = $("div.submenu");
	var menuState = false;	    
	
	
		
	//OPEN MEGA-MENU WHEN HOVERING ON A "mMenu" CLASS NAV ITEM
	triggerMenu.hoverIntent(
		function() {	
			$(megaMenu).css("display","none");
			$(megaMenu[triggerMenu.index(this)]).toggle();
			TweenLite.to(megaMenu[triggerMenu.index(this)], menuSpeed, {opacity:1});
			var tempHeight = $(megaMenu[triggerMenu.index(this)]).outerHeight();
			TweenLite.to($("#megaMenu"), menuSpeed, {height:tempHeight, ease:Sine.easeOut});
			$(this).addClass("menu-current");
			menuState = true;
		},
		function() {
			if (  !($('#megaMenu').hasClass('hover')) && !($('nav').hasClass('hover'))  ) {
				TweenLite.to($("#megaMenu"), menuSpeed, {height:0, ease:Sine.easeIn, onComplete:function() { 
					$("#megaMenu").css("display","none");		
					if($(".menu-current")[0] !== triggerMenu.index(this)) {
						$(".menu-current").removeClass("menu-current");
					}
				}} );
				TweenLite.to($(megaMenu), menuSpeed, {opacity:0.0});
				menuState = false;
			}
		}
	);
	// give menu highlight class to current selected menu item 
	$(triggerMenu).mouseenter(function(){
		$("#megaMenu").css("display","block");
		$(".menu-current").removeClass("menu-current");
		$(this).addClass("menu-current");
	});
		
	function closeMegaMenu() {
		// make sure nav is open before closing
		if(menuState) {
			menuState = false;
			// animate menu closed and turn off mega menu
			TweenLite.to($("#megaMenu"), menuSpeed, {height:0, ease:Sine.easeIn, onComplete:function() { 
					$(megaMenu).css("opacity","0.0");		
					$("#megaMenu").css("display","none");		
					$(".menu-current").removeClass("menu-current");
			}} );
		}
		
	}   
	
	$("#megaMenu").hoverIntent(function() {},function() { if(!($('nav').hasClass('hover'))) { closeMegaMenu(); }});
	nochildMenu.hover(closeMegaMenu);	
	$("#logo").mouseenter(closeMegaMenu);
	$("#maincontentcontainer").mouseenter(closeMegaMenu);
	$("#search").mouseenter(closeMegaMenu);
		$("#megaMenu, nav").hover(function() {
		$(this).toggleClass('hover');
	});
});