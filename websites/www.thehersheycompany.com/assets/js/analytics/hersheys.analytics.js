var SITE_ANALYTICS = false;
if (typeof HERSHEYS === 'undefined'){
  HERSHEYS = {};
}
HERSHEYS.Analytics = (function(){
  /*create default config (will be overridden in HERSHEYS.Analytics.init calls)*/
  var CONFIG = {
    businessUnit:'',
    trackers:[]
  };

  return {
    init: function(config){
      var _this = this;
      /*need to ensure this stuff always has a reference to jquery, so we pass in the actual 'jQuery' object as $ instead of assuming $ will always reference jquery (in case some other library steals the $) -- NOTE: we don't want to mess w/ jQuery noConflict because we'd have to update EVERY site to use the same no conflict)*/
      return (function($){
        $(document).ready(function(){
          /*merge passed config into global config (overridding any defaults)*/
          CONFIG = $.extend({},CONFIG,config);
          if (typeof _herfogaq === 'undefined'){
            /*if the page didn't make a copy of the _gaq array, just track the default account*/
            CONFIG.trackers = [''];
          } else {
            /*loop thru the _herfogaq array and copy out the '_setAccount' prefixes so we can track across all accounts*/
            for (var i = 0, ii = _herfogaq.length; i < ii; ++i) {
              var key = _herfogaq[i][0];
              if (key.match(/_setAccount/)) {
               CONFIG.trackers.push(key.replace(/_setAccount/,''));
              }
            }
          }
          $('.share-links').live('click', function(e){
            HERSHEYS.Analytics.doShareTracking($(this).attr('id').replace(/-link/,''));
          });
          $('.share-links-kitchens').live('click', function(e){
            e.preventDefault();
            var $a = $(this);
            HERSHEYS.Analytics.doShareTracking($a.attr('name').replace(/-link/,''));
            setTimeout(function(){
              /*should these open in new tabs?*/
              document.location.href = $a.attr('href');
            },250);
          });
          /* To Track Recipe Prints */
          $('.print_recipe').live('click', function(){
            if(typeof OMNITURE_RECIPE_ID === 'undefined') {
              if($('.recipe-photo img').length) {
                var recID = $('.recipe-photo img').attr('src');
                recID = recID.split('id=');
                var finalID = recID[1].split('&');
                HERSHEYS.Analytics.doRecipePrint(finalID[0]);
              }
              else {
                var spliturl = window.location.toString().split('/');
                HERSHEYS.Analytics.doRecipePrint(spliturl[spliturl.length - 1]);
              }
            } else {
              HERSHEYS.Analytics.doRecipePrint(OMNITURE_RECIPE_ID);
            }
          });

          // ADDITIONAL GOOGLE TRACKING (EXTERNAL LINKS, MAILTO LINKS AND FILE DOWNLOADS)
          var filetypes = /\.(pdf|doc*|xls*|ppt*|mp4|sit|zip)$/i;
          $('a').each(function() {
            var href = $(this).attr('href');
            if (href && (href.match(/^https?\:/i)) && (!href.match(document.domain))) {
              // GOOGLE TRACK EXTERNAL LINK.
              // bind to custom namespace so that global.js can override with a bumperpage and do tracking event
              $(this).bind('click.herfgaq', function(e) {
                HERSHEYS.Analytics.doExternalLink(href);
                if ($(this).attr('target') !== '_blank') {
                  /*only prevent default/setTimeout for non target="_blank" links. target blank should have plenty of time to resolve since the current tab/window doesn't close/get replaced*/
                  e.preventDefault();
                  setTimeout('document.location = "' + href + '"', 100);
                }
              });
            } else if (href && href.match(/^mailto\:/i)) {
              // GOOGLE TRACK MAILTO LINK
              $(this).bind('click', function() {
                var mailLink = href.replace(/^mailto\:/i, '');
                HERSHEYS.Analytics.pushIt(['_trackEvent', 'Email', 'Click', mailLink]);
              });
            } else if (href && href.match(filetypes)) {
              // GOOGLE TRACK DOWNLOAD
              $(this).bind('click', function(e) {
                e.preventDefault();
                var extension = (/[.]/.exec(href)) ? (/[^.]+$/.exec(href)) : undefined;
                var filePath = href;
                HERSHEYS.Analytics.pushIt(['_trackEvent', 'Download', 'Click-' + extension.toString().toUpperCase(), filePath]);
                if ($(this).attr('target') == '_blank') {
                  setTimeout("window.open('" + href + "')", 100);
                } else {
                  setTimeout('document.location = "' + href + '"', 100);
                }
              });
            }
          });
        });
        return _this;
      })(jQuery);
    },
    pushIt: function(arr){
      /*loop thru all trackers*/
      for (var i = 0, ii = CONFIG.trackers.length; i < ii; ++i) {
        /*make a copy of the array, prepend the account prefix, and push it thru to _gaq*/
        var newarr = arr.slice(0);
        newarr[0] = CONFIG.trackers[i] + newarr[0];
        _gaq.push(newarr);
      }
    },
    doShareTracking: function(code){
      this.doSocialTracking(code, 'share');
    },
    doSocialTracking: function(network, action, opt_target, opt_pagePath){
      this.pushIt(['_trackSocial', network, action, opt_target, opt_pagePath]);
    },
    getBusinessUnit: function(){
      return CONFIG.businessUnit;
    },
    doProductView: function(productNameIn){
      /* Check if BusinessUnit is spaces.  If so, it's a root site like thehersheycompany.com or maunaloa.com and no need to append BusinessUnit to pageview */
      var businessUnit = this.getBusinessUnit() != ' ' ? '/' + this.getBusinessUnit() : '';
      this.pushIt(['_trackPageview', businessUnit + '/products/#' + productNameIn]);
    },
    doNutritionView: function(productNameIn){
      /* Check if BusinessUnit is spaces.  If so, it's a root site like thehersheycompany.com or maunaloa.com and no need to append BusinessUnit to pageview */
      var businessUnit = this.getBusinessUnit() != ' ' ? '/' + this.getBusinessUnit() : '';
      this.pushIt(['_trackPageview', businessUnit + '/nutrition/#' + productNameIn]);
    },
    doPageView: function(pUrl){
      /* Check if BusinessUnit is spaces.  If so, it's a root site like thehersheycompany.com or maunaloa.com and no need to append BusinessUnit to pageview */
      var businessUnit = this.getBusinessUnit() != ' ' ? '/' + this.getBusinessUnit() : '';
      this.pushIt(['_trackPageview', businessUnit + '/' + pUrl]);
    },
    doRecipePrint: function(pRecipeID){
      /*need to force convert to a string or it never makes it*/
      this.pushIt(['_trackEvent', 'Recipe', 'Print', ''+pRecipeID]);
    },
    doRecipeSubmission: function(){
      this.pushIt(['_trackEvent', 'Recipe', 'Submission', 'UserRecipe']);
    },
    doCraftPrint: function(pCraftID){
      /*need to force convert to a string or it never makes it*/
      this.pushIt(['_trackEvent', 'Craft', 'Print', ''+pCraftID]);
    },
    doExternalLink: function(href){
      this.pushIt(['_trackEvent', 'External', 'Click', href.replace(/^https?\:\/\//i, '')]);
    },
    doInternalLink: function(href){
      this.pushIt(['_trackEvent', 'Internal', 'Click', href.replace(/^https?\:\/\//i, '')]);
    },
    doSubscription: function(pProperty){
      /*need to force convert to a string or it never makes it*/
      this.pushIt(['_trackEvent', 'Newsletter', 'Signup', ''+pProperty]);
    },	
    doDownload: function(pUrl){
      var extension = (/[.]/.exec(pUrl)) ? (/[^.]+$/.exec(pUrl)) : undefined;
      this.pushIt(['_trackEvent', 'Download', 'Click-' + extension.toString().toUpperCase(), pUrl]);
    },
    doAssetInteraction: function(assettype, assetname){
      /* Check if BusinessUnit is spaces.  If so, it's a root site like thehersheycompany.com or maunaloa.com and no need to append BusinessUnit to pageview */
      var businessUnit = this.getBusinessUnit() != ' ' ? '/' + this.getBusinessUnit() : '';
      this.pushIt(['_trackEvent', 'Asset Interaction', assettype, businessUnit + '/#' + assetname]);
    },
    doPureSmoresView: function(section, pageName){
      /* Check if BusinessUnit is spaces.  If so, it's a root site like thehersheycompany.com or maunaloa.com and no need to append BusinessUnit to pageview */
      var businessUnit = this.getBusinessUnit() != ' ' ? '/' + this.getBusinessUnit() : '';
      this.pushIt(['_trackEvent', 'Pure Smores View', section, businessUnit + '/#' + pageName]);
    },
    doPrintAdView: function(channel, pageName){
      /* Check if BusinessUnit is spaces.  If so, it's a root site like thehersheycompany.com or maunaloa.com and no need to append BusinessUnit to pageview */
      var businessUnit = this.getBusinessUnit() != ' ' ? '/' + this.getBusinessUnit() : '';
      this.pushIt(['_trackEvent', 'Print Ad', 'View', businessUnit + '/' + channel + '/' + pageName]);
    },
    doSaveToRecipeBox: function(pRecipeID){
      /*need to force convert to a string or it never makes it*/
      this.pushIt(['_trackEvent', 'Recipe', 'Saved', ''+pRecipeID]);
    },
    doRateRecipe: function(pRecipeID){
      /*need to force convert to a string or it never makes it*/
      this.pushIt(['_trackEvent', 'Recipe', 'Rate', ''+pRecipeID]);
    },
    doReviewRecipe: function(pRecipeID){
      /*need to force convert to a string or it never makes it*/
      this.pushIt(['_trackEvent', 'Recipe', 'Review', ''+pRecipeID]);
    },
    doEmailRecipe: function(pRecipeID){
      /*need to force convert to a string or it never makes it*/
      this.pushIt(['_trackEvent', 'Recipe', 'Email', ''+pRecipeID]);
    },	
    doReviewCraft: function(pCraftID){
      /*need to force convert to a string or it never makes it*/
      this.pushIt(['_trackEvent', 'Craft', 'Review', ''+pCraftID]);
    },
    doEmailCraft: function(pCraftID){
      /*need to force convert to a string or it never makes it*/
      this.pushIt(['_trackEvent', 'Craft', 'Email', ''+pCraftID]);
    },
    doAddToGroceryList: function(pRecipeID){
      /*need to force convert to a string or it never makes it*/
      this.pushIt(['_trackEvent', 'Recipe', 'AddedToGroceryList', ''+pRecipeID]);
    },
    trackVideo: function(videoID, action){
      /* We must split the video id by an underscore - the first half should be the type, the second half should be the video name */
      var videoSplit = videoID.split('_');
      var videoCategory = videoSplit.length == 2 ? videoSplit[0].replace(/-/g, ' ') : 'Unclassified';
      var videoName = videoSplit.length == 2 ? videoSplit[1].replace(/-/g, ' ') : videoID;
      this.pushIt(['_trackEvent', 'Video', action, videoCategory + ':' + videoName]);
    },
	 doTrackVideo: function(videoID, action){
      /* We must split the video id by an underscore - the first half should be the type, the second half should be the video name */
      this.pushIt(['_trackEvent', 'Videos', action, videoID]);
    },
    trackRecipeView : function(opts){
      var recipeURL = opts['recipeURL'].replace('#share_modal', '');
      this.pushIt(['_trackPageview', recipeURL]);
    },
    doSetCustomVar: function(index, name, value, opt_scope){
      this.pushIt(['_setCustomVar', index, name, value, opt_scope]);
    },
    doTrackEvent: function(category, action, opt_label, opt_value, opt_noninteraction){
      this.pushIt(['_trackEvent', category, action, opt_label, opt_value, opt_noninteraction]);
    },
    doTrackPageView: function(page){
      this.pushIt(['_trackPageview', page]);
    },
    trackYouTube: function(iframe_id, name, video_id){
      /*
      this is called two ways:
      1) without video_id: binds YT.Player to iframe
      2) with video_id: swaps out video of existing YT.Player
      */
      var _this = this,
          $iframe = $('#' + iframe_id),
          player = $iframe.data('player'),
          trackIt = function(){
            /*save off player data for future onStateChanges (because we can't call "new YT.Player" a second time)*/
            $iframe.data({
              player:player,
              name:name,
              started:false
            });
            if (typeof YT !== 'undefined' && typeof YT.Player !== 'undefined') {
              if (!player) {
                /*save off the YT.Player so we can pull it back later and swap video ids*/
                $iframe.data('player', new YT.Player(iframe_id, {
                  events:{
                    onStateChange: function(e){
                      var state = e.data,
                          idata = $iframe.data();
                      /*track "video start" and "video end"*/
                      if (state === YT.PlayerState.PLAYING && !idata.started) {
                        /*NOTE: "PLAYING" fires every time the user pauses/unpause or seeks to a different point, ensure we only track the play once*/
                        _this.doTrackEvent('Video', 'Play', idata.name);
                        $iframe.data('started', true);
                      } else if (state === YT.PlayerState.ENDED) {
                        _this.doTrackEvent('Video', 'End', idata.name);
                        $iframe.data('started', false);
                      }
                    }
                  }
                }));
              }
            }
            /*if video id passed in, load it*/
            if (player && video_id) {
              player.loadVideoById(video_id);
            }
          };

      /*load the youtube iframe api*/
      if (!window.onYouTubeIframeAPIReady) {
        var tag = document.createElement('script');
        tag.src = '../../../../www.youtube.com/iframe_api.js'/*tpa=http://www.youtube.com/iframe_api*/;
        var first = document.getElementsByTagName('script')[0];
        first.parentNode.insertBefore(tag, first);
        window.onYouTubeIframeAPIReady = function(){ trackIt(); };
      } else {
        trackIt();
      }
    }
  };
})();