/*
 * Custom sbux jQuery plugin to configure 'add to cart' link functionality. 
 * Permits products featured in starbucks.com to be added into the shopping cart of store.starbucks.com via OCAPI (Open Commerce API, Demandware's API)
 * First generation implementation verifies a successful API call and then immediately takes user to the checkout page of the store.
 * Takes an options object on initialization as in following sample usage:
 * $('myAddToCartButton').enableAddToCart({
 *      quantity: <set a defaut quantity> || defaults to 1
 *      beforeAjaxCall: <function to run before add to cart is fired> || default is null
 *      successCallback: <function to run on success> || default is directToCartPage() to send user to the cart page
 *      errorCallback: <function to run on error> || default is to show an alert box
 * });
 * The element being initialized can be any element. It is expected to have a data-product-id attribute containing the product's ID in the store.
 */

(function (sb, $) {

    'use strict';

    $.fn.enableAddToCart = function (options) {

        var dwClientId = sb.config.ecomm.StarbucksStoreApiClientId,
            dwBaseUrl = sb.config.ecomm.StarbucksStoreApiBaseUrl,
            storeHostName = sb.config.ecomm.StarbucksStoreHostName,
            currentHref,
            inTransit = false,
            loaderTimer,
            settings = $.extend({
                //defaults
                quantity: 1,
                beforeAjaxCall: null,
                successCallback: directToCartPage,
                errorCallback: showErrorMsg
            }, options);

        function addAjaxLoader($element) {
            loaderTimer = setTimeout(function () {
                $element.append($('<div class="css_loader" />'));
            }, 1000);
        }

        function removeAjaxLoader($element) {
            $element.find('.css_loader').remove();
        }

        function addToBasket($button, productID) {

            var urlBasketAdd = '//' + dwBaseUrl + '/basket/this/add?client_id=' + dwClientId + '&format=json',
                data = JSON.stringify({ "product_id": productID, "quantity": settings.quantity });

            addAjaxLoader($button);

            if (typeof settings.beforeAjaxCall === 'function') {
                settings.beforeAjaxCall();
            }
            $.ajax({
                dataType: 'json',
                type: 'post',
                contentType: 'application/json;charset=UTF-8',
                context: $button,
                url: urlBasketAdd,
                data: data,
                timeout: 10000, //ten seconds
                xhrFields: { withCredentials: true },
                crossDomain: true
            })
            .done(function (data) {
                //console.log(data);
                //console.log("Successfully added to cart");
                settings.successCallback(this); // 'this' is the context set in the ajax call
            })
            .fail(function (xhr, textStatus, thrownError) {
                //console.log("Error adding to basket");
                //console.log('xhr', xhr);
                //console.log('xhrStatus', xhr.status);
                //console.log('thrownError', thrownError);
                settings.errorCallback(this); // 'this' is the context set in the ajax call
            })
            .always(function () {
                clearTimeout(loaderTimer);
                removeAjaxLoader(this); // 'this' is the context set in the ajax call
                inTransit = false;
            });

        }

        function directToCartPage($button) {
            window.location = $button.attr('href');
        }

        function showErrorMsg($button) {
            //find .notice element in relation to button
            //given inconsistent possible implementations, search two spots
            var $noticeSibling = ($button.hasClass('button')) ? $button : $button.parent('.button'),
                $notice = $noticeSibling.next('.notice');
            $notice.slideDown('fast', function () {
                $notice.focus();
            });
        }

        function showLightboxMsg($button) {
            //example of firing an error message in a lightbox, this alternative is not currently used
            var productID = $button.attr('data-product-id'),
                $html = $('<div />').addClass('alert_content'),
                $msg = $('<p>Hmm, something went wrong on our end. Why don\'t you try </p>'),
                $link = $('<a>visiting this product\'s store page?</a>'),
                url = 'https://' + storeHostName + '/-/' + productID + ',default,pd.html';
            $msg.append($link);
            $link.attr('href', url);
            if (sb.lightbox) {
                sb.lightbox.open({
                    maskOpacity: 0,
                    contentClass: 'lightbox_alert'
                });
                sb.lightbox.setContent($html.html($msg));
            }
        }

        return this.each(function (i, button) {
            var $button = $(button);
            $button.click(function (e) {
                var $this = $(this),
                    id = $this.attr('data-product-id');
                e.preventDefault();
                if (!inTransit) {
                    inTransit = true;
                    addToBasket($this, id);
                }
            });
        });

        //testing add to cart
        //addToBasket("011038092", 1, updateCart);

    };

    $(document).trigger('addToCartScriptLoaded');

}(window.sb = window.sb || {}, jQuery));