var isMobile;
var mobileWidth = screen.availWidth;


var isIpad = false;

var mySession;

// begin geomap

function getLocation() {
	
	$("#bellairecontainer").html("Loading...");
		
		if (navigator.geolocation) {
			
		navigator.geolocation.getCurrentPosition(showPosition);	
			
			
		} else {
			
		alert("Geolocation not supported");	
			
			
		}
		
		function showPosition(position){
			
		var lat = position.coords.latitude;
		var long = position.coords.longitude;
		
		$.ajax({
		
		url:'http://open.mapquestapi.com/directions/v2/route?key=Fmjtd%7Cluub2d62nq%2Caw%3Do5-9u7aqr&ambiguities=ignore&from='+lat+','+long+'&to=29.7059988,-95.5614768',
		dataType:'json',
		success:function(data){
			
		$("#bellairecontainer").html(data.route.distance + " miles <br />");
		
		$("#bellairecontainer").append("<br /><a href='https://www.google.com/maps/preview?hl=en#!q=10200+Bellaire+Boulevard%2C+Houston%2C+TX&data=!4m15!2m14!1m13!1s0x8640dd32a2a87f75%3A0x814bf60884de92c!3m8!1m3!1d78766!2d-95.5527509!3d29.7079638!3m2!1i1920!2i1091!4f13.1!4m2!3d29.7059988!4d-95.5614768' target='_blank'>Map</a><P>");
		
		for (var i = 0; i < 20; i++) {
			
		
			
		$("#bellairecontainer").append(data.route.legs[0].maneuvers[i].narrative + "<br/>");
			
		}
		
		
		
		}
		
		
		
	});
		
		
			
			
		}
		
		
	}
	
// end geo map

// This function changes the mobile site to the full site

function fullSite(){
	
	
	mySession = sessionStorage.setItem("mobileCheck",false);
	location.reload();
	
}

function EmailSocial() {
	
	
	var url = document.URL;
	var title = document.title;
	
	location.href='mailto:recipient@recipienturl.com?subject=Halliburton&body=I%20thought%20this%20web%20page%20might%20interest%20you- ' +url + '%20-Title:' + title ;
	
	
}


$(document).ready(function () {

   // $('head').append('<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">');
	
	var docWidth = $(document).width();
	mySession = sessionStorage.getItem("mobileCheck");
	
	var susHeader = 'Unknown_83_filename'/*tpa=http://www.halliburton.com/public/about_us/pubsdata/images/HAL_sustain_banner.jpg*/;
	
	var heavyOilHeader = '../../../solutions/contents/Heavy_Oil/Overview/images/HO_Banner_sm.JPG'/*tpa=http://www.halliburton.com/public/solutions/contents/Heavy_Oil/Overview/images/HO_Banner_sm.JPG*/;
	
	var aboutHeader = '../../images/about_us.jpg'/*tpa=http://www.halliburton.com/public/pubsdata/images/about_us.jpg*/;
	
	$("body").find("img").each(function() {
			
		 var susTest = $(this).attr('src');
		 
		 if (susTest == susHeader) {
			 
			$(this).addClass('bigImage'); 
			 
		 } else if (susTest == heavyOilHeader) {
			 
			 $(this).addClass('bigImage');
			 
		 } else if (susTest == aboutHeader) {
			 
			 $(this).addClass('bigImage');
			 
		 }
			
			
			
	});
	
	
	//alert(mobileWidth);
	//alert(docWidth);
	
	
	if (navigator.userAgent.match(/iPad/i) != null) {
	
		isIpad = true;
		
			
		
	}
	
	if (mySession == null) {
	
		mySession = true;
		//$("head").append('<link href="../css/ipad.css"/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/css/ipad.css*/ rel="stylesheet" type="text/css"/>');	
		
	} else if (mySession == "false"){
		
			
		isIpad = false;
		$("link[href='../css/mobile.css'/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/css/mobile.css*/]").remove();
		
	} 
	
	  
	  if ($("#breadCrumbComponent").is(':hidden')){
		  
		  isMobile = true;
		  
		  
	  } else {
		  
		isMobile = false;  
		  
	  }
		
		

    if ((isMobile == true || isIpad == true || mobileWidth < 721 || docWidth < 680) && (mySession == true)) {
		
		
		
		$("#globalNavLogoLink img").prop('src', '../img/mobile_img/mobilelogo.png'/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/img/mobile_img/mobilelogo.png*/);
		
		$("#globalNavLogoLink img").show();

        var searchButtonMargin = $(window).width() - 34;

		$("body").find("table").each(function() {
			
			if ($(this).width() > 300) {
			
			$(this).addClass('bigTable');
				
			}
			
		});
		
		// go table class is on careers page
		
		$(".gotable:first").before('<img src="../img/mobile_img/careersbanner.png"/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/img/mobile_img/careersbanner.png*//>');
		
		$("body").find("img").each(function() {
			
			if ($(this).width() > mobileWidth) {
			
			$(this).addClass('bigImage');
				
			}
			
			
			
		});
		
		$("body").find("a").each(function() {
			
			
			
			if ($(this).attr("title") == "Launch iCem Service website") {
			
			$(this).parent().css({'position':'relative','float':'left'});
				
			}
			
			
			if ($(this).attr("title") == "Launch WellLock Resin website") {
			
			$(this).parent().css({'position':'relative','float':'left'});
				
			}
			
			
			
		});
		
		
		
		$("#challengeOverviewComponent").find("img").each(function() {
			
			if ($(this).width() > mobileWidth) {
			
			$(this).addClass('bigImage');
				
			}
			
		});
		
		$("body").find("object").each(function() {
			
			$(this).hide();
			
		});
		
		$("body").find("embed").each(function() {
			
			$(this).hide();
			
		});
		
		/*$("#challange_case").clone().appendTo(".bigTable td:first-child").css('float','left').css('width','95%').css('left','0px');
		$("#challange_case").parent().css('width','99%');
		
		$(".bigTable td:last-child #challange_case").hide();
		$("#deeptable td:first-child").attr('width','95%');*/
		
		
		/****************** Case Studies - tool resources begin ************************/
		
		$("#documentListing_1368526481103_0_btn_search").parent().css('padding-left','5px');
		
		/****************** Case Studies - tool resources end ************************/	
		
		/****************** Technical Papers - tool resources begin ************************/
		
		$("#documentListing_1368526481094_0_btn_search").parent().css('padding-left','5px');
		
		/****************** Technical Papers - tool resources end ************************/
		
		/****************************** Events begin *************************************/
		
		$("#main").parent().css('padding-left','0px');
		$("#colheader30").children().css('width','32%');
		$("#colheader32").children().css('width','32%');
		
		$(".colnames:last-child").css('display','none');
		$(".colnames2:last-child").css('display','none');
		
		/****************************** Events end *************************************/
			
		$("#location_table").find("td:last-child").each(function() {
			
			$(this).attr('width','100%');
			$(this).before("</tr><tr>");
			
		});
		
		$("#baroid_masthead").parent().css('display','none');
        
		$("#centerColumnWrapper").attr('style', 'width:100% !important');

        $("#searchbutton_globalNavigationSearchHalcom_SearchInput").css('margin-left', searchButtonMargin);

        $("#searchbox_globalNavigationSearchHalcom_SearchInput").attr('placeholder', 'Enter Search...');
		
		// old slider

       // $("#home_left_wrapper").load("http://www.halliburton.com/public/pubsdata/customer-resources/img/mobile_img/slider.html");
		
		// new slider
		
		//$("#home_left_wrapper").load("http://www.halliburton.com/public/pubsdata/customer-resources/mainslider/mainslider.html");
		
		/*********************************** background position in headers ************************************************/
		
			var newsheadPosition = $("#home_news_header").width() - 50;
			var midwrapperBulletPosition = $(".halcomHomeComponents > #home_bottom_wrapper > #home_bottom_mid > #home_middle_wrapper table").width();
			var mostPopularPosition = $(".halcomHomeComponents > #home_bottom_wrapper > #home_bottom_mid > #home_bottom_mid_head").width() -60;
			var caseHeaderPosition = $(".halcomHomeComponents > #home_bottom_wrapper > #home_bottom_right > #home_right_wrapper_casestudy > #home_casestudy_header").width() -50;
			
			var downloadsHeaderPosition = $(".halcomHomeComponents > #home_bottom_wrapper > #home_bottom_right > #home_bottom_right_head").width() -50;
			
			
			
			
			$(".halcomHomeComponents > #home_mid_wrapper > #home_right_wrapper > #home_news_header").attr('style','background-position:' + newsheadPosition + 'px ' + '0px'); 
			$(".halcomHomeComponents > #home_bottom_wrapper > #home_bottom_mid > #home_middle_wrapper table").attr('style','background-position:' + midwrapperBulletPosition + 'px ' + '20px');
			$(".halcomHomeComponents > #home_bottom_wrapper > #home_bottom_left > .home_bottom_left_item").attr('style','background-position:' + midwrapperBulletPosition + 'px ' + '20px');
			/*$(".halcomHomeComponents > #home_bottom_wrapper > #home_bottom_mid > #home_bottom_mid_head").attr('style','background-position:' + mostPopularPosition + 'px ' + '0px');*/
			$(".halcomHomeComponents > #home_bottom_wrapper > #home_bottom_right > #home_right_wrapper_casestudy > #home_casestudy_header").attr('style','background-position:' + caseHeaderPosition + 'px ' + '0px');
			
			$(".halcomHomeComponents > #home_bottom_wrapper > #home_bottom_right > #home_bottom_right_head").attr('style','background-position:' + downloadsHeaderPosition + 'px ' + '0px');
			
			
			
		
		/*********************************** background position in headers ************************************************/
		
		/****************************** secondary nav clicks begin ************************************/
		
		
	$(".secondaryNavCurrent").parents("li").css('background', 'url(/public/pubsdata/customer-resources/img/mobile_img/secondary_nav_arrow_down.png) no-repeat');
    $(".secondaryNavCurrent").parents("li").css('background-position', '90% 5px');
	$(".secondaryNavCurrent").parents("li").css('background-size', '13px 9px');
    $(".secondaryNavCurrent").parent().css('background-color', '#555555');
	
    $(".secondaryNavCurrent").parents("li").css('background-color', '#555555');
	
	$(".secondaryNav > ul > li > ul li .secondaryNavCurrent").parent("li").css('background', 'url(/public/pubsdata/customer-resources/img/mobile_img/secondary_nav_second_arrow.png) no-repeat');
    $(".secondaryNav > ul > li > ul li .secondaryNavCurrent").parent("li").css('background-position', '0px 7px');
	
	/*	
	if ($(".secondaryNav > ul > li > ul li ul li a").hasClass('secondaryNavCurrent')) {
		
		$(".secondaryNav > ul > li > ul li").css('background-position', '-300px 7px');
		 $(".secondaryNav > ul > li > ul li .secondaryNavCurrent").parent("li").css('background-position', '0px 7px');
		
		
	}*/
		
		
		/********************************* secondary nav clicks end*************************************/
		
		/*********************************Tools Resources begin ***************************************/
		
		if ($("#toolsAndResourcesComponent").is(':visible')) {

       

        $("#centerColumnWrapper").attr('style', 'width:100% !important;margin-left:8px');

        
    }
	
	var toolsURL = document.URL.split('=')[1];
		
		if (toolsURL == "hfci3yh7") {
			
				
			$("#centerColumnWrapper").attr('style', 'width:100% !important;margin-left:0px !important');
			//$("#centerColumn").attr('style', 'margin-left:0px !important');
			$("#centerColumn").removeAttr('style');
			//$(".genericContentComponent").attr('style', 'padding:0px 0px 0px 0px !important');
			
		}
		
		$("body").find(".icon_Title").each(function () {

        var mytext = $(this).text();
		var toolsWidth = $(window).width() - 70;

        if (mytext == "Calculators") {

            $(this).parent().css({ 'background': 'url(/web-resources/common/img/tools-calc-back.png)', 'background-repeat': 'no-repeat', 'background-position': 'right 0px' });

        } else if (mytext == "Case Studies") {

            $(this).parent().css({ 'background': 'url(/web-resources/common/img/tools-case-back.png)', 'background-repeat': 'no-repeat', 'background-position': 'right 0px' });

        } else if (mytext == "Downloads & Mobile Apps") {

            $(this).parent().css({ 'background': 'url(/web-resources/common/img/tools-downloads-back.png)', 'background-repeat': 'no-repeat', 'background-position': 'right 0px' });

        } else if (mytext == "Safety") {

            $(this).parent().css({ 'background': 'url(/web-resources/common/img/tools-safety-back.png)', 'background-repeat': 'no-repeat', 'background-position': 'right 0px' });

        } else if (mytext == "Simulators") {

            $(this).parent().css({ 'background': 'url(/web-resources/common/img/tools-simulators-back.png)', 'background-repeat': 'no-repeat', 'background-position': 'right 0px' });

        } else if (mytext == "Technical Papers") {

            $(this).parent().css({ 'background': 'url(/web-resources/common/img/tools-techpapers-back.png)', 'background-repeat': 'no-repeat', 'background-position': 'right 0px' });

        } else if (mytext == "Training") {

            $(this).parent().css({ 'background': 'url(/web-resources/common/img/tools-training-back.png)', 'background-repeat': 'no-repeat', 'background-position': 'right -5px' });

        }

    });
		
		
		/********************************Tools Resources end ******************************************/
		
		
		/******************************* Careers Homepage begin **************************************/
		
		var careersHomeURL = document.URL.split('=')[1];
		
		if (careersHomeURL == "hgbr8q6n") {
			
				
			$("#centerColumnWrapper").attr('style', 'width:100% !important;margin-left:0px !important');
			//$("#centerColumn").attr('style', 'margin-left:0px !important');
			$("#centerColumn").removeAttr('style');
			$(".genericContentComponent").attr('style', 'padding:0px 0px 0px 0px !important');
			
		}
		
		
		/****************************** Careers Homepage end *****************************************/
		
		
		/****************************** Contact Begin ************************************************/
		
		
		var contactHomeURL = document.URL.split('=')[1];
		
		if (contactHomeURL == "hgbr8q6p") {
			
				
			$("#centerColumnWrapper").attr('style', 'width:100% !important;margin-left:0px !important');
			//$("#centerColumn").attr('style', 'margin-left:0px !important');
			$("#centerColumn").removeAttr('style');
			$(".genericContentComponent").attr('style', 'padding:0px 0px 0px 0px !important');
			
			$("h1").attr('style','display:block !important;');
			
		}
		
		
		
		
		/****************************** Contact End ************************************************/
		
		/****************************** Consulting Begin ************************************************/
		
		
		var consultingHomeURL = document.URL.split('=')[1];
		
		if (consultingHomeURL == "hfqelac0") {
			
				
			$("#centerColumnWrapper").attr('style', 'width:100% !important;margin-left:0px !important');
			//$("#centerColumn").attr('style', 'margin-left:0px !important');
			$("#centerColumn").removeAttr('style');
			$(".genericContentComponent").attr('style', 'padding:0px 0px 0px 0px !important');
			
			
			
		}
		
		
		
		
		/****************************** Consulting End ************************************************/
		
		
		/****************************** Project Management Begin ************************************************/
		
		
		var projectURL = document.URL.split('=')[1];
		
		if (projectURL == "hgoxc0ih") {
			
				
			$("#centerColumnWrapper").attr('style', 'width:100% !important;margin-left:0px !important');
			//$("#centerColumn").attr('style', 'margin-left:0px !important');
			$("#centerColumn").removeAttr('style');
			$(".genericContentComponent").attr('style', 'padding:0px 0px 0px 0px !important');
			
			
			
		}
		
		
		
		
		/****************************** Project Management End ************************************************/
		
		
		/****************************** Deepwater Begin ************************************************/
		
		
		var projectURL = document.URL.split('=')[1];
		
		if (projectURL == "hgeyxss4") {
			
				
			$("#centerColumnWrapper").attr('style', 'width:100% !important;margin-left:0px !important');
			//$("#centerColumn").attr('style', 'margin-left:0px !important');
			$("#centerColumn").removeAttr('style');
			$(".genericContentComponent").attr('style', 'padding:0px 0px 0px 0px !important');
			
			
			
		}
		
		
		
		
		/****************************** Deepwater End ************************************************/
		
		
		/****************************** Unconventional Begin ************************************************/
		
		
		var projectURL = document.URL.split('=')[1];
		
		if (projectURL == "hgjyd46v") {
			
				
			$("#centerColumnWrapper").attr('style', 'width:100% !important;margin-left:0px !important');
			//$("#centerColumn").attr('style', 'margin-left:0px !important');
			$("#centerColumn").removeAttr('style');
			$(".genericContentComponent").attr('style', 'padding:0px 0px 0px 0px !important');
			
			
			
		}
		
		
		
		
		/****************************** Unconventional End ************************************************/
		
		
		/****************************** mature fields Begin ************************************************/
		
		
		var matureURL = document.URL.split('=')[1];
		
		if (matureURL == "hgjyd46l") {
			
				
			$("#centerColumnWrapper").attr('style', 'width:100% !important;margin-left:0px !important');
			//$("#centerColumn").attr('style', 'margin-left:0px !important');
			$("#centerColumn").removeAttr('style');
			$(".genericContentComponent").attr('style', 'padding:0px 0px 0px 0px !important');
			
			
			
		}
		
		
		
		
		/****************************** mature fields End ************************************************/
		
			
        
        $(".globalNavLogo").append('<a href="javascript:void()" id="socialBtn"><img src="../img/mobile_img/social_mobileBtn.png"/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/img/mobile_img/social_mobileBtn.png*/ style="float:right;"/></a><a href="javascript:void()" id="searchBtn"><img src="../img/mobile_img/searchBtn.png"/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/img/mobile_img/searchBtn.png*/ style="float:right;"/></a><a href="javascript:void()" id="menuBtn"><img src="../img/mobile_img/menuBtn.png"/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/img/mobile_img/menuBtn.png*/ style="float:right;"/></a><div class="shareBox2" style="display:none"><div><img src="../img/mobile_img/primarynavarrow.png"/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/img/mobile_img/primarynavarrow.png*/ style="float:right"/><img alt = "Share on LinkedIn"  src="../../../../web-resources/common/img/social-li.png"/*tpa=http://www.halliburton.com/web-resources/common/img/social-li.png*/ /> <span style="vertical-align:top"><a onclick="shareLinkedin()" href="javascript:void();">LinkedIn</a></span></div><div><img src="../img/mobile_img/primarynavarrow.png"/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/img/mobile_img/primarynavarrow.png*/ style="float:right"/><img alt = "Share on Facebook" src="../../../../web-resources/common/img/social-fb.png"/*tpa=http://www.halliburton.com/web-resources/common/img/social-fb.png*/ /> <span style="vertical-align:top"><a onclick="shareFacebook()" href="javascript:void();">Facebook</a></span></div><div><img src="../img/mobile_img/primarynavarrow.png"/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/img/mobile_img/primarynavarrow.png*/ style="float:right"/><img alt = "Share on Twitter" src="../../../../web-resources/common/img/social-tw.png"/*tpa=http://www.halliburton.com/web-resources/common/img/social-tw.png*/ /> <span style="vertical-align:top"><a onclick="shareTwitter()" href="javascript:void();">Twitter</a></span></div><div><img src="../img/mobile_img/primarynavarrow.png"/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/img/mobile_img/primarynavarrow.png*/ style="float:right"/><img alt = "OilPro" src="../../../../web-resources/common/img/social-op.png"/*tpa=http://www.halliburton.com/web-resources/common/img/social-op.png*/ /> <span style="vertical-align:top"><a onclick="shareOilpro()" href="javascript:void();">OilPro</a></span></div><div><img src="../img/mobile_img/primarynavarrow.png"/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/img/mobile_img/primarynavarrow.png*/ style="float:right"/><img alt = "Share on Google+" src="../../../../web-resources/common/img/social-gp.png"/*tpa=http://www.halliburton.com/web-resources/common/img/social-gp.png*/ /> <span style="vertical-align:top"><a onclick="shareGoogle()" href="javascript:void();">Google+</a></span></div><div><img src="../img/mobile_img/primarynavarrow.png"/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/img/mobile_img/primarynavarrow.png*/ style="float:right"/><img alt = "Print" src="../../../../web-resources/common/img/social-print.png"/*tpa=http://www.halliburton.com/web-resources/common/img/social-print.png*/ /> <span style="vertical-align:top"><a href="javascript:window.print()">Print</a></span></div></div>');
		
		if (navigator.userAgent.match(/iPhone/)) {
			
			$("#socialBtn img").css('margin-top','-1px');	
			$("#menuBtn img").css('margin-top','-1px');
			$("#searchBtn img").css('margin-top','-1px');
			
			
		}
		
		$("#globalNavWrapper .primaryNavLast").before('<li><a href="https://identity.halliburton.com/HalcomLogin/login.jsp?xHalTemp=halcom">Sign in | Register</a></li>');
		$("#globalNavWrapper .primaryNavLast").after('<li><a href="http://www.halliburton.com/en-US/locations/regional-sites.page?node-id=hgeyxtav">Regional Sites</a></li>');

        // $(".containerComponent h1:first-child").before('<a href="javascript:void()" id="secondMenu"><img src="Unknown_83_filename"/*tpa=http://www.halliburton.com/web-resources/common/img/mobile_img/secondnav_menu.png*/ style="float:left;margin-top:5px;padding-right:5px;color:#555555;"/></a>');

       /* $("#toolsAndResourcesComponent .featured_twoCol img").before('<a href="javascript:void()" id="secondMenu"><img src="Unknown_83_filename"/*tpa=http://www.halliburton.com/web-resources/common/img/mobile_img/secondnav_menu.png*//></a>');*/

        /* $(".genericContentComponent h1:first-child").before('<a href="javascript:void()" id="secondMenu">
        <img src="Unknown_83_filename"/*tpa=http://www.halliburton.com/web-resources/common/img/mobile_img/secondnav_menu.png*/ style="float:left;margin-top:5px;padding-right:5px;color:#555555;"/>
        </a>');*/

       // $("h1 span").before('<a href="javascript:void()" id="secondMenu"><img src="Unknown_83_filename"/*tpa=http://www.halliburton.com/web-resources/common/img/mobile_img/secondnav_menu.png*//></a>');

      /*  $("#centerColumn h1:first").before('<a href="javascript:void()" id="secondMenu"><img src="../img/mobile_img/secondnav_menu.png"/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/img/mobile_img/secondnav_menu.png*//></a>'); */
	  
	  $("#breadcrumbArea").append('<a href="javascript:void()" id="secondMenu"><img src="../img/mobile_img/secondnav_menu.png"/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/img/mobile_img/secondnav_menu.png*//></a>');
		
		/* $(".containerComponentColumnInner h2:first").before('<a href="javascript:void()" id="secondMenu"><img src="../img/mobile_img/secondnav_menu.png"/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/img/mobile_img/secondnav_menu.png*//></a>'); */
       
       
        $(".secondaryNav .secondaryNavBottom").after('<br/><p class="secondParagraph"><a href="../../../../en-US/default.page.htm"/*tpa=http://www.halliburton.com/en-US/default.page*/><img src="../img/mobile_img/home_secondarynav.png"/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/img/mobile_img/home_secondarynav.png*/ border="0" /></a></p><p class="secondParagraph"><a href="http://www.halliburton.com/en-US/locations/locations-home.page?node-id=hgbr8q6p"><img src="../img/mobile_img/contact_secondarynav.png"/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/img/mobile_img/contact_secondarynav.png*/ border="0"/></a></p><p class="secondParagraph"><a href="http://www.halliburton.com/router/PostAuth.aspx?url=aHR0cCUzYSUyZiUyZnd3dy5oYWxsaWJ1cnRvbi5jb20lMmZlbi1VUyUyZmRlZmF1bHQucGFnZQ=="><img src="../img/mobile_img/register_secondarynav.png"/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/img/mobile_img/register_secondarynav.png*/ border="0"/></a></p><p class="secondParagraph"><a href="http://www.halliburton.com/en-US/locations/regional-sites.page?node-id=hgeyxtav"><img src="../img/mobile_img/regional_secondarynav.png"/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/img/mobile_img/regional_secondarynav.png*/ border="0"/></a></p><p class="secondParagraph"></p><div class="socialheader">Follow Halliburton:</div><p class="social"><img src="../img/mobile_img/fbicon.png"/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/img/mobile_img/fbicon.png*/ style="vertical-align:middle;padding-right:9px;"/><a href="javascript:void()" onclick="getURL()">Facebook</a></p><p class="social"><img src="../img/mobile_img/twittericon.png"/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/img/mobile_img/twittericon.png*/ style="vertical-align:middle;padding-right:9px;"/><a href="https://twitter.com/halliburton" target="_blank">Twitter</a></p><p class="social"><img src="../img/mobile_img/linkedinicon.png"/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/img/mobile_img/linkedinicon.png*/ style="vertical-align:middle;padding-right:9px;"/><a href="http://www.linkedin.com/company/halliburton" target="_blank">LinkedIn</a></p><p class="social"><img src="../img/mobile_img/gplusicon.png"/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/img/mobile_img/gplusicon.png*/ style="vertical-align:middle;padding-right:9px;"/><a href="https://plus.google.com/+halliburton/videos" target="_blank">Google +</a></p><p class="social"><img src="../img/mobile_img/youtubeicon.png"/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/img/mobile_img/youtubeicon.png*/ style="vertical-align:middle;padding-right:9px;"/><a href="http://www.youtube.com/halliburton" target="_blank">YouTube</a></p> <p class="social"><img src="../img/mobile_img/emailpageicon.png"/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/img/mobile_img/emailpageicon.png*/ style="vertical-align:middle;padding-right:9px;"/><a href="javascript:void()" onclick="EmailSocial()">Email Page</a></p></div>');
		
		
		/************************ ADJUST HOMEPAGE SPACING BEGIN ************************************/
		
		if($("#centerColumn-HomePageLayout").is(':visible')) {
			
			$("#contentWrapper").attr('style','margin:-5px 0px 0px 0px !important');
			
			
		}
		
		
		
		/************************ ADJUST HOMEPAGE SPACING END ************************************/

        $("#mainSearchResults li:last-child").after('<a href="javascript:void()" id="refine-resultsbutton"><img src="../img/mobile_img/refine-results.png"/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/img/mobile_img/refine-results.png*/ /></a>');

        $(".Refine div:first-child").before('<a href="javascript:void()" id="goback-resultsbutton"><img src="Unknown_83_filename"/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/img/mobile_img/goBack.png*/ /></a>');


      //  $("#productsProductOverviewComponent table tbody tr:last-child").clone().appendTo("#productsProductOverviewComponent");
		
		$('#mobile3').html('<div id="fullsite"><a href="javascript:void()" onclick="fullSite()">Full Site</a></div>');

        $("#documentListing_1368526481057_0_txtLocation").attr('placeholder','Location');
		$("#documentListing_1368526481094_0_txtLocation").attr('placeholder','Location');
		$("#documentListing_1368526481103_0_txtLocation").attr('placeholder','Location');
		$("#documentListing_1368526481103_0_hlCollapseAll").text('Collapse');
		$("#documentListing_1368526481103_0_hlExpandAll").text('Expand');
		$("#documentListing_1368526481094_0_hlCollapseAll").text('Collapse');
		$("#documentListing_1368526481094_0_hlExpandAll").text('Expand');
		$("#documentListing_1368526481057_0_hlCollapseAll").text('Collapse');
		$("#documentListing_1368526481057_0_hlExpandAll").text('Expand');
		
		//$("#map_base").parent().css('display','none');
		
		$(".secondaryNavContactUs a").attr("target","_blank");
		
		
		
		// $(".secondaryNavContactUs a").css('display','none');
		
		// $(".secondaryNavContactUs a").after("<a href='"+contactURL+"' target='_blank'>Contact Us</a>");
		
		$(".secondaryNavContactUs").click(function() {
		
			var contactURL = $(".secondaryNavContactUs a").attr("href");
			
						
			$("meta[name='viewport']").attr('content','width=device-width, initial-scale=1.0, maximum-scale=1.0');
			$("#secondMenu").css('left','0px');
			$(".secondaryNav").hide();
			$(".secondaryNav").css("width", "0px");
			$("#centerColumn").css('margin-left', '0px');
            $(".tabbedNavComponent").css('margin-left', '0px');
			$(".globalNavUtility").css('margin-left', '0px');
			$("#footerWrapper").css('left', '0px');
			$("#menuBtn img").show();
			$("#searchBtn img").show();
			$("#socialBtn img").show();
			$("#centerColumn").css('margin-left', '0px');
			$("#centerColumn").css('background', "#ffffff");
			$("#pageWrapper").attr('style', "background:#ffffff !important");
			$("#contentWrapper").css('border','6px solid #363636');
			$("#leftColumn").css('width','0px');
			/*$("#footerWrapper").show();
            
            
            
            $("#rightColumn").show();

            */
            
			
			
		
		});
		
		
		

    }

    
	// window.resize was here
	
	
	
	// window.resize here
	

    $("#socialBtn").click(function () {

        if ($(".shareBox2").is(':hidden')) {

            $(".shareBox2").show("fast");
			
			$("#socialBtn img").attr('src', '../img/mobile_img/social_mobileBtn_selected.png'/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/img/mobile_img/social_mobileBtn_selected.png*/);
			$(".primaryNav").hide("fast");
            $("#searchbox_globalNavigationSearchHalcom_SearchInput").hide("slow");
            $("#searchbutton_globalNavigationSearchHalcom_SearchInput").hide("slow");
            $("#searchBtn img").attr('src', '../img/mobile_img/searchBtn.png'/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/img/mobile_img/searchBtn.png*/);
			$("#menuBtn img").attr('src', '../img/mobile_img/menuBtn.png'/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/img/mobile_img/menuBtn.png*/);
        } else {

            $(".shareBox2").hide("fast");
            $("#socialBtn img").attr('src', '../img/mobile_img/social_mobileBtn.png'/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/img/mobile_img/social_mobileBtn.png*/);

        }

    });
	
	$("#menuBtn").click(function () {

        if ($(".primaryNav").is(':hidden')) {

            $(".primaryNav").show("fast");
            $("#menuBtn img").attr('src', '../img/mobile_img/menuBtn_selected.png'/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/img/mobile_img/menuBtn_selected.png*/);
            $("#searchbox_globalNavigationSearchHalcom_SearchInput").hide("slow");
            $("#searchbutton_globalNavigationSearchHalcom_SearchInput").hide("slow");
			$(".shareBox2").hide("slow");
            $("#searchBtn img").attr('src', '../img/mobile_img/searchBtn.png'/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/img/mobile_img/searchBtn.png*/);
			$("#socialBtn img").attr('src', '../img/mobile_img/social_mobileBtn.png'/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/img/mobile_img/social_mobileBtn.png*/);
        } else {

            $(".primaryNav").hide("fast");
            $("#menuBtn img").attr('src', '../img/mobile_img/menuBtn.png'/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/img/mobile_img/menuBtn.png*/);

        }

    });

    $("#searchBtn").click(function () {

        if ($("#searchbox_globalNavigationSearchHalcom_SearchInput").is(':hidden')) {

            $("#searchbutton_globalNavigationSearchHalcom_SearchInput img").attr('src', '../img/mobile_img/magnifysearch.png'/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/img/mobile_img/magnifysearch.png*/);

            $("#searchbox_globalNavigationSearchHalcom_SearchInput").show("slow");

            $("#menuBtn img").attr('src', '../img/mobile_img/menuBtn.png'/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/img/mobile_img/menuBtn.png*/);
            $("#searchBtn img").attr('src', '../img/mobile_img/searchBtn_selected.png'/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/img/mobile_img/searchBtn_selected.png*/);
			$("#socialBtn img").attr('src', '../img/mobile_img/social_mobileBtn.png'/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/img/mobile_img/social_mobileBtn.png*/);
            $(".primaryNav").hide("fast");
			$(".shareBox2").hide("slow");
            $("#searchbutton_globalNavigationSearchHalcom_SearchInput").show("slow");

        } else {

            $("#searchbox_globalNavigationSearchHalcom_SearchInput").hide("slow");
            $("#searchbutton_globalNavigationSearchHalcom_SearchInput").hide("slow");
            $("#searchBtn img").attr('src', '../img/mobile_img/searchBtn.png'/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/img/mobile_img/searchBtn.png*/);
        }

    });

   /* $(window).scroll(function () {

        var winwidth = $(window).width();

        if (winwidth <= 480) {

            $("#searchbox_globalNavigationSearchHalcom_SearchInput").hide("slow");
            $("#searchbutton_globalNavigationSearchHalcom_SearchInput").hide("slow");
            $("#searchBtn img").attr('src', 'Unknown_83_filename'/*tpa=http://www.halliburton.com/web-resources/common/img/mobile_img/searchBtn.png*/);

        }
    });*/

    $("#secondMenu").click(function () {

        if ($(".secondaryNav").is(':hidden')) {

            $(".secondaryNav").show();

            $(".secondaryNav").animate({ "width": "80%" }, "slow");

            var winwidth = $(window).width() - 50;
            var footerwidth = $(window).width() - 60;

            $("#centerColumn").css('margin-left', winwidth);
			$("#centerColumn").css('background', "#ffffff");
			$("#pageWrapper").attr('style', "background:#363636 !important;overflow:hidden");
			//$("#breadcrumbArea").css('margin-left', winwidth);
			$(this).css('left',winwidth);
            $("#footerWrapper").css('left', winwidth);
						
            $(".tabbedNavComponent").css('margin-left', winwidth);
			$("#contentWrapper").css('border','0px');

            $("#rightColumn").hide();

            //$(".globalNavUtility").hide();
			$(".globalNavUtility").css('margin-left', winwidth);
			$("#menuBtn img").hide();
			$("#searchBtn img").hide();
			$("#socialBtn img").hide();
            $(".globalNavRegister").hide();
            $(".secondParagraph").show();
            $(".share").show();
			$("#leftColumn").css('width','230px');
			
			$("#secondMenu img").attr('src','../img/mobile_img/secondnav_menu_selected.png'/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/img/mobile_img/secondnav_menu_selected.png*/);
			
			

        } else {

            $(".secondaryNav").hide();
			$("#footerWrapper").show();
            $(".secondaryNav").css("width", "0px");
			//$("#breadcrumbArea").css('margin-left', '0px');
			$(this).css('left','0px');
            $("#footerWrapper").css('left', '0px');
			$(".globalNavUtility").css('margin-left', '0px');
			$("#menuBtn img").show();
			$("#searchBtn img").show();
			$("#socialBtn img").show();
            $("#centerColumn").css('margin-left', '0px');
			$("#centerColumn").css('background', "#ffffff");
			$("#pageWrapper").attr('style', "background:#ffffff !important");
            $(".tabbedNavComponent").css('margin-left', '0px');
            $("#rightColumn").show();
			$("#contentWrapper").css('border','6px solid #363636');

            $(".globalNavUtility").show();
            //$(".globalNavRegister").show();
			
			$("#leftColumn").css('width','0px');

			$("#secondMenu img").attr('src','../img/mobile_img/secondnav_menu.png'/*tpa=http://www.halliburton.com/public/pubsdata/customer-resources/img/mobile_img/secondnav_menu.png*/);
			
			
        }

    });

    $("#refine-resultsbutton").click(function () {

        /*$(".Refine").show();
        $("#mainSearchResults").hide();*/

        $("#Refine").clone().appendTo("#mainSearchResults");
        $("#mainSearchResults #Refine").css('margin-left', '11px');



    });

    $("#goback-resultsbutton").click(function () {

        $(".Refine").hide();
        $("#mainSearchResults").show();


    });
	
	// geomap begin
	
	$("#bellairelink").click(function() {
		
		
	
	getLocation();
	

	
	
	});
	
	
	
	// geomap end
	
	$(window).bind('orientationchange', function(e) {

  		switch ( window.orientation ) {

   		 case 0:
        		
				//location.reload();
				//alert('portrait mode');
    			break;

    	case 90:
        		
				//location.reload();
				//alert('landscape mode screen turned to the left');
				break;

    	case -90:
        		
				
				//location.reload();
				//alert('landscape mode screen turned to the right');
    			break;

  		}

	});

});

          function getURL() {

          var myurl = document.URL;
          var mytitle = document.title;

          var mystring = 'https://www.facebook.com/dialog/feed?app_id=458358780877780&amp;link=' + myurl + '&amp;picture=http://www.halliburton.com/public/pubsdata/Home_Page/images/HalHome/home_material.jpg&amp;name=Halliburton&amp;caption='+mytitle+'&amp;description='+mytitle+'.&amp;redirect_uri=http://mighty-lowlands-6381.herokuapp.com/';


          window.location=mystring;



      }



      // begin easy slider

      (function ($) {



          $.fn.easySlider = function (options) {

              // default configuration properties
              var defaults = {
                  prevId: 'prevBtn',
                  prevText: 'Previous',
                  nextId: 'nextBtn',
                  nextText: 'Next',
                  controlsShow: false,
                  controlsBefore: '',
                  controlsAfter: '',
                  controlsFade: true,
                  firstId: 'firstBtn',
                  firstText: 'First',
                  firstShow: false,
                  lastId: 'lastBtn',
                  lastText: 'Last',
                  lastShow: false,
                  vertical: false,
                  speed: 1000,
                  auto: true,
                  pause: 5000,
                  continuous: true,
                  numeric: false,
                  numericId: 'controls'
              };

              var options = $.extend(defaults, options);

              this.each(function () {
                  var obj = $(this);
                  var s = $("li", obj).length;
                  var w = $("li", obj).width();
                  var h = $("li", obj).height();
                  var clickable = true;
                  obj.width(w);
                  obj.height(h);
                  obj.css("overflow", "hidden");
                  var ts = s - 1;
                  var t = 0;
                  $("ul", obj).css('width', s * w);

                  if (options.continuous) {
                      $("ul", obj).prepend($("ul li:last-child", obj).clone().css("margin-left", "-" + w + "px"));
                      $("ul", obj).append($("ul li:nth-child(2)", obj).clone());
                      $("ul", obj).css('width', (s + 1) * w);
                  };

                  if (!options.vertical) $("li", obj).css('float', 'left');

                  if (options.controlsShow) {
                      var html = options.controlsBefore;
                      if (options.numeric) {
                          html += '<ol id="' + options.numericId + '"></ol>';
                      } else {
                          if (options.firstShow) html += '<span id="' + options.firstId + '"><a href=\"javascript:void(0);\">' + options.firstText + '</a></span>';
                          html += ' <span id="' + options.prevId + '"><a href=\"javascript:void(0);\">' + options.prevText + '</a></span>';
                          html += ' <span id="' + options.nextId + '"><a href=\"javascript:void(0);\">' + options.nextText + '</a></span>';
                          if (options.lastShow) html += ' <span id="' + options.lastId + '"><a href=\"javascript:void(0);\">' + options.lastText + '</a></span>';
                      };

                      html += options.controlsAfter;
                      $(obj).after(html);
                  };

                  if (options.numeric) {
                      for (var i = 0; i < s; i++) {
                          $(document.createElement("li"))
							.attr('id', options.numericId + (i + 1))
							.html('<a rel=' + i + ' href=\"javascript:void(0);\">' + (i + 1) + '</a>')
							.appendTo($("#" + options.numericId))
							.click(function () {
							    animate($("a", $(this)).attr('rel'), true);
							});
                      };
                  } else {
                      $("a", "#" + options.nextId).click(function () {
                          animate("next", true);
                      });
                      $("a", "#" + options.prevId).click(function () {
                          animate("prev", true);
                      });
                      $("a", "#" + options.firstId).click(function () {
                          animate("first", true);
                      });
                      $("a", "#" + options.lastId).click(function () {
                          animate("last", true);
                      });
                  };

                  function setCurrent(i) {
                      i = parseInt(i) + 1;
                      $("li", "#" + options.numericId).removeClass("current");
                      $("li#" + options.numericId + i).addClass("current");
                  };

                  function adjust() {
                      if (t > ts) t = 0;
                      if (t < 0) t = ts;
                      if (!options.vertical) {
                          $("ul", obj).css("margin-left", (t * w * -1));
                      } else {
                          $("ul", obj).css("margin-left", (t * h * -1));
                      }
                      clickable = true;
                      if (options.numeric) setCurrent(t);
                  };

                  function animate(dir, clicked) {
                      if (clickable) {
                          clickable = false;
                          var ot = t;
                          switch (dir) {
                              case "next":
                                  t = (ot >= ts) ? (options.continuous ? t + 1 : ts) : t + 1;
                                  break;
                              case "prev":
                                  t = (t <= 0) ? (options.continuous ? t - 1 : 0) : t - 1;
                                  break;
                              case "first":
                                  t = 0;
                                  break;
                              case "last":
                                  t = ts;
                                  break;
                              default:
                                  t = dir;
                                  break;
                          };
                          var diff = Math.abs(ot - t);
                          var speed = diff * options.speed;
                          if (!options.vertical) {
                              p = (t * w * -1);
                              $("ul", obj).animate(
								{ marginLeft: p },
								{ queue: false, duration: speed, complete: adjust }
							);
                          } else {
                              p = (t * h * -1);
                              $("ul", obj).animate(
								{ marginTop: p },
								{ queue: false, duration: speed, complete: adjust }
							);
                          };

                          if (!options.continuous && options.controlsFade) {
                              if (t == ts) {
                                  $("a", "#" + options.nextId).hide();
                                  $("a", "#" + options.lastId).hide();
                              } else {
                                  $("a", "#" + options.nextId).show();
                                  $("a", "#" + options.lastId).show();
                              };
                              if (t == 0) {
                                  $("a", "#" + options.prevId).hide();
                                  $("a", "#" + options.firstId).hide();
                              } else {
                                  $("a", "#" + options.prevId).show();
                                  $("a", "#" + options.firstId).show();
                              };
                          };

                          if (clicked) clearTimeout(timeout);
                          if (options.auto && dir == "next" && !clicked) {
                              ;
                              timeout = setTimeout(function () {
                                  animate("next", false);
                              }, diff * options.speed + options.pause);
                          };

                      };

                  };
                  // init
                  var timeout;
                  if (options.auto) {
                      ;
                      timeout = setTimeout(function () {
                          animate("next", false);
                      }, options.pause);
                  };

                  if (options.numeric) setCurrent(0);

                  if (!options.continuous && options.controlsFade) {
                      $("a", "#" + options.prevId).hide();
                      $("a", "#" + options.firstId).hide();
                  };

              });

          };

      })(jQuery);



      // end easy slider
	  
	  
	  
	  