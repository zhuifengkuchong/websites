/*
	Title: Client JavaScript functions. 
	Description: The functions contained here are not built/updated as part of a 'code', but controlled by Halcom Marketing. 
	
	History:	2013-09-16 -- Added dlsClick querystring functionality.
	
*/
$(document).ready(function () {

// Adding Facebook meta tag

var fb = '<meta property="fb:admins" content="100001953774509,100003975865030,100002298049202" /> ';

$('head').append(fb);

var skypeCheck = document.URL.split('=')[1];

if (skypeCheck == "hgbr8q6p") {
	
	
$("head").append('<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />');
$(".skype_c2c_textarea_span img").css('display','none');

}

// End of Facebook meta tag code

	/* 
		Fires a click event on the Document Listing Sidebar link matching the dlsClick= query string parameter.
	*/
	$(location.search.slice(1).split('&')).each(function(qsKey, qsVal) {	
		if(qsVal.toLowerCase().indexOf('dlsclick=') != -1) {
			
			var matchValue = decodeURIComponent(qsVal.substring(qsVal.indexOf('=')+1)).toLowerCase();
			
			// Look for a match in any document listing sidebar (only the first match will fire).
			$('[id*=divSidebarDocumentListingComponent] a').each(function(key, a) {
				if($(a).text().toLowerCase().indexOf(matchValue) != -1)
				{			
					// clear 'dlsClick' querystring from the Form's action attribute, otherwise we'll get into a loop!
					var action = $('#Form1').attr('action');
					var hashes = (action.indexOf('?') != -1 ? action.slice(action.indexOf('?') + 1).split('&') : '');
					var page = (action.indexOf('?') != -1 ? action.substring(0, action.indexOf('?')) : action);
					var qs = "";
					for(var i = 0; i < hashes.length; i++)
					{        
						if(hashes[i].toLowerCase().indexOf('dlsclick') == -1)
							qs += (qs.length > 0 ? '&' : '') + hashes[i];
					}
					$('#Form1').attr('action', page + (qs.length > 0 ? '?' + qs : ''));
					
					// fire the click event
					a.click();
				}
			});
		}
	});
});
