
// Global ie flag
var isIE = function () {
    if (!$.support.cssFloat) {
        return (/msie|MSIE/.test(navigator.userAgent));
    } else {
        return false;
    }
};

// Global ie7 flag
var isIE7 = function () {
    if (!$.support.cssFloat) {
        return (/msie|MSIE 7/.test(navigator.userAgent));
    } else {
        return false;
    }
};

// Global ie8 flag
var isIE8 = function () {
    if (!$.support.cssFloat) {
        return (/msie|MSIE 8/.test(navigator.userAgent));
    } else {
        return false;
    }
};

// detects IE8 compatablity mode
var isIE8CompatMode = function () {
	//if( $.browser.msie && $.browser.version == 7.0 && document.documentMode && document.documentMode == 8)
	if( isIE7() && document.documentMode && document.documentMode == 8 )
		return true;
	else
		return false;
};

// Has function
var has = function (obj, val) {
    if (obj != null && obj != undefined && obj != '') {
        if (typeof obj == 'object') {
            return obj.hasOwnProperty(val);
        } else if (typeof obj == 'array' || typeof obj == 'string') {
            return (obj.indexOf(val) != -1) ? true : false;
        } else {
            return false;
        }
    } else {
        return false;
    }
};


//Global Spinner
var halSpinner = function (id, status) {
    /* arguments
    -- id: id of parent element (required string)
    -- status: status of spinner [on or off] (required string)
    */
    var spinner = false;
    //add spinner
    if (status == 'on' &&  document.getElementById(id)) {
        $('#' + id).append('<span class="spinner"></span>');
        spinner = $('#' + id + ' span.spinner').fadeIn();
    }
    //remove spinner
    if (status == 'off' && document.getElementById(id)) {
        $('#' + id).children('span.spinner').remove();
    }
    
    return spinner;
};


//Global Tabbed Action function
var halTabbedAction = function (id, action) {
    /* arguments
    -- id: the id of the tab to trigger event on
    -- action: any action on tab [remove, clone, draggable]
    */
    // start on page load
    $(document).ready(function () {

        if (document.getElementById(id)) {
            var callback = (arguments.length > 3) ? arguments[3] : false;
            switch (action) {
                case 'remove':
                    $('#' + id).remove();
                    break;
            }
            if (callback) {
                callback(id, action);
            }
        }
    });
};


//Global Tabbed Event function
var halTabbedEvent = function (on, id) {
    /* arguments
    -- on: any event such as [click,mouseover,drop]
    -- id: the id of the tab to trigger event on
    */
    var callback = (arguments.length > 2) ? arguments[2] : false;
    //check if tab nav exists
    if (document.getElementById('tabbedNav') && document.getElementById(id)) {
        var tabNav = '#tabbedNav .tabbedNavComponent';
        //check if component exists
        if ($(tabNav).length > 0) {
            var tabNavLi = $(tabNav + ':eq(0) ul li');
            //check if tab exists
            if (tabNavLi.length > 0 && $('#' + id + ' a').length > 0) {
                var tabNavA = $('#' + id + ' a');
                tabNavA.trigger(on);
                if (callback) {
                    callback(on, id);
                }
            }
        }
    }
    return true;
};


//Global Append List with height limit
var halAppendList = function (options) {
    /* options arguments
    -- parent: id of parent element (requierd string)
    -- children: children element name (optional string)
    -- data: data to append to parent (required string)
    -- limit: parent height bound (required number)
    -- height: default height of the list item (optional number)
    */
    if (has(options, 'parent') && has(options, 'data') && has(options, 'limit')) {        
        var data = options.data;
        var limit = options.limit;
        var parent = $(options.parent);
        var child = has(options, 'child') ? options.child : 'li';
        var jchild = parent.children(child);
        var fchild = jchild.first();
        var omargint = parseInt(fchild.css('margin-top'));
        var omarginb = parseInt(fchild.css('margin-bottom'));
        var oheight = parseInt(fchild.css('line-height')) + omarginb + omargint;
        var height = has(options, 'height') ? options.height : (jchild.length > 0) ? oheight : 26;
        var colorbox = has(options, 'colorbox') ? options.colorbox : false;
        var callback = has(options, 'callback') ? true : false;
        //append wrapped data to parent
        if (parent.height() + height < limit) {
            parent.append('<' + child + '>' + data + '</' + child + '>');
            parent.children(child).last().css('visibility', 'hidden');
            parent = $(options.parent);
        }
        var check = has(options, 'check') ? options.check(parent.children(child).last()) : false;
        //show last item if within limit
        if (parent.height() < (limit + 10) || check) {
            var cur = parent.children(child).last();
            cur.css('visibility', 'visible');
            //set colorbox if any           
            if (colorbox != false && !isiPhone()) {
                var acur = $(cur).children('a').first();
                $(acur).off('click').on('click', function (e) {
                    e.preventDefault();
                   
                        var opt = setColorbox(this);
                        if (typeof colorbox == 'object') {
                            for (var c in colorbox) {
                                opt[c] = colorbox[c];
                            }
                        }
                        $(this).colorbox(opt);
                    
                });
            }
            //set callback function if any
            if (callback) {
                options.callback(cur, parent);
            }
        }
    }
};


// Global Toggle Function
var halToggle = function (options) {
    /* options properties
    -- button: button to target event to (required string)
    -- target: target element to toggle (required string)
    -- active: active class for target element (required string)
    -- activebtn: active class for button element (optional string)
    -- siblings: siblings of target element (optional string)
    -- speed: speed in milliseconds of toggle (optional number)
    -- index: button index [this or parent] (optional string)
    -- before: before toggle callback function (optional function)
    -- after: after toggle callback function (optional function)
    -- type: toggle type [tab or accordion] (optional string)
    */
    //check for option properties
    var toggle = false;
    if (has(options, 'button') && has(options, 'target') && has(options, 'active')) {
        //Set default variables
        var button = options.button, target = options.target, active = options.active;
        var siblings = has(options, 'siblings') ? options.siblings : 'div';
        var index = has(options, 'index') ? options.index : 'this';
        var speed = has(options, 'speed') ? options.speed : 1;
        var activebtn = (has(options, 'activebtn')) ? options.activebtn : active;
        var type = (has(options, 'type')) ? options.type : 'tab';
        var before = false, after = false;
        //Set click event
        toggle = $(button).click(function (event) {
            //prevent defaults
            event.preventDefault();
            event.stopPropagation();
            //Set this and index
            var t = (index == 'parent') ? $(this).parent() : $(this);
            var idx = t.index();
            //----------------------------
            //Run before callback function
            if (has(options, 'before')) {
                //call the before function
                before = options.before(t, options, idx);
                //update options object
                options = (has(before, 'options')) ? before.options : options;
                //add spinner if any
                if (has(options, 'spinner')) {
                    halSpinner(options.spinner, true);
                }
            }
            //---------------------
            //Toggle target element
            if ($(target)) {
                //toggle tab active class 
                if (type == 'tab') {
                    //slow down toggle speed
                    if (speed > 1) {
                        $(target + ':eq(' + idx + ')').fadeIn(speed, function () {
                            $(this).addClass(active);
                        });
                        $(target + ':eq(' + idx + ')').siblings(siblings).hide().removeClass(active);
                        //toggle with no speed
                    } else {
                        $(target + ':eq(' + idx + ')').addClass(active).siblings(siblings).removeClass(active);
                    }
                }
                //toggle accordion active class
                if (type == 'accordion') {
                    //slow down toggle speed
                    if (speed > 1) {
                        $(t).next().children().css('visibility','hidden');
                        $(t).next().show(speed, function () {
                            $(t).next().children().css('visibility', 'visible');
                            $(this).addClass(active);
                        }).siblings(siblings).hide(speed, function () {
                            $(this).removeClass(active);
                        });
                        //toggle with no speed
                    } else {
                        $(t).next().addClass(active).siblings().removeClass(active);
                    }
                }
                //toggle this/parent active class
                $(t).addClass(activebtn).siblings().removeClass(activebtn);
                //toggle button active class
                if (activebtn != active) {
                    $(button).not(this).removeClass(activebtn);
                    $(this).addClass(activebtn);
                }
            }
            //---------------------------
            //Run after callback function
            if (has(options, 'after')) {
                after = options.after(t, options, idx);
                options = (has(after, 'options')) ? after.options : options;
                //remove spinner if any
                if (has(options, 'spinner')) {
                    halSpinner(options.spinner, false);
                }
            }
        });
    }
    return options;
};

// Handle Event for Primary Nav
var primaryNavMargin = parseInt($("#globalNavWrapper .primaryNav .primaryNavDropdown").css("margin-top"));
var primaryNav = function (id) {
    if (id.indexOf('<') == -1) {
        // Top navigation hover
        $('#A_' + id).hover(function () {
            $('#UL_' + id).css("margin-top", primaryNavMargin - $(window).scrollTop()).show();

        }, function () {
            $('#UL_' + id).hide();
        });
        // Top navigation child ul hover
        $('#UL_' + id).hover(function () {
            $('#UL_' + id).css("margin-top", primaryNavMargin - $(window).scrollTop()).show();
        }, function () {
            $('#UL_' + id).hide();
        });
    }
};

//load sidebar document listing component
var loadSidebarComponents = {
    comp: [],
    load: function () {
        var lsc = loadSidebarComponents.comp;
        var loaded = [], y = 0;
        for (var x in lsc) {
            if (typeof lsc[x] == 'function') {
                loaded[y] = $.Deferred();
                $.when(lsc[x]()).done(function (data) {
                    loaded[y].resolve(data);
                });
                y++;
            }
        }
        return loaded;
    }
};

// Secondary Navigation Highlight
var secNavHighlight = function () {
    var sn = $('.secondaryNavCurrent'), nb = true;
    //find parent in breadcrumb
    if (sn.length > 1) {
        try {
            var bc = document.getElementById('breadCrumbComponent');
            if (bc) {
                var sa = $('#breadCrumbComponent ul li span a:contains(' + sn[0].innerText + ')');
                if (sa) {
                    var pn = $(sa).parent().prev().prev().children()[0].innerText;
                    //remove if parent not same as breadcrumb
                    $('.secondaryNavCurrent').parent().parent().parent().each(function () {
                        if (this.children[0].innerText != pn) {
                            $(this).find('a.secondaryNavCurrent').removeClass('secondaryNavCurrent');
                            nb = false;
                        }
                    });
                }
            }
        } catch (e) {
            //do nothing
        }
    }
    //result to default instead
    if (nb) {
        $('.secondaryNavCurrent:gt(0)').removeClass('secondaryNavCurrent');
    }
};


// Execute on document ready
$(document).ready(function () {
    //Contact Us Background FIX
    if ($('.contactUsComponent').length > 0) {
        if (top.location.href != document.location.href) {
            $('html').css('overflow', 'hidden');
            $('html').css('overflow-y', 'scroll');
            $('#pageWrapper').css({ 'width': 'auto', 'background': '#FFFFFF' });
            $('body:eq(0)').addClass('bodyIframe');
            $('#centerColumn-OverlayLayout').addClass('contactLayout');
            $('#centerColumn-OneColumnLayout').css({ 'margin': 0, 'padding': 0, 'width': 520 });
        }
    }

    //IE FIX: Add round corner to the top right corner of center column
    if (!$.support.cssFloat) {
        var findCenterColumn = [];
        //check for centerColumn
        if (document.getElementById('centerColumn')) {
            findCenterColumn.push('#centerColumn');
        }
        //check for centercolumn-onecolumn layout
        if (document.getElementById('centerColumn-OneColumnLayout')) {
            findCenterColumn.push('#centerColumn-OneColumnLayout');
        }
        var cCol = findCenterColumn.join(',');
        $(cCol).prepend('<div class="border-top-right"></div>');
        if (!document.getElementById('tab-rollup-1')) {
            $(cCol).prepend('<div class="border-top-left"></div>');
            window.setTimeout(function () {
                if (isIE7() && document.getElementById('tabbedNavRow') &&
                    $('#tabbedNav > .ls-area > .ls-area-body').text() == '' &&
                    $('.ext-ie').length == 0 && $('#tabbedNav').hasClass('is-empty') == false
                ) {
                    $('#tabbedNavRow').hide();
                    $('#centerColumn').css('margin-top', '1px');
                }
            }, 300)
        }
    }


    //Set focus on menu item hover
    $('#globalNavWrapper .primaryNav > ul > li').mouseover(function () {
        $(this).focus();
    });

    //Toggle active menu item on hover
    $(".primaryNavDropdown").hover(function () {
        $(this).siblings('a').addClass('active');
    }, function () {
        $(this).siblings('a').removeClass('active');
        if (!isIE7()) {
            $(this).css('display', 'none');
        }
    });

    //Wait for all sidebar components to load
    jQuery.when(loadSidebarComponents.load()).done(function (data) {
        //check for line breaks first
        if (jQuery('.halSidebarLineBreak').length > 0) {
            //remove last line break
            jQuery(jQuery('.halSidebarLineBreak')[$('.halSidebarLineBreak').length - 1]).remove();
        }
    });
});


// BEGIN document listing common

function toggleBox(szDivIDStr, imageIDStr) /* 1 visible, 0 hidden */
{
	var szDivID = xGetElementById(szDivIDStr);
	var imageID = xGetElementById(imageIDStr);
	(document.getElementById) /*gecko(NN6) + IE 5+*/
	{
		var obj = szDivID;
		obj.style.display = (obj.style.display == 'none') ? 'block' : 'none';
		if (obj.style.display == 'none')
			imageID.src = "../../uc/img/TPMAX2.GIF"/*tpa=http://www.halliburton.com/web-resources/uc/img/TPMAX2.GIF*/;
		else
			imageID.src = "../../uc/img/TPMIN2.GIF"/*tpa=http://www.halliburton.com/web-resources/uc/img/TPMIN2.GIF*/;
	}
}

function Expand(szDivIDStr, imageIDStr) {
	var szDivID = xGetElementById(szDivIDStr);
	var imageID = xGetElementById(imageIDStr);
	if (document.layers)	   /*NN4+*/
		szDivID.visibility = "show";
	else if (document.getElementById) /*gecko(NN6) + IE 5+*/
		szDivID.style.display = "block";
	else if (document.all)	/* IE 4 */
		szDivID.style.visibility = "visible";
	imageID.src = "../../uc/img/TPMIN2.GIF"/*tpa=http://www.halliburton.com/web-resources/uc/img/TPMIN2.GIF*/;
}

function Collapse(szDivIDStr, imageIDStr) {
	var szDivID = xGetElementById(szDivIDStr);
	var imageID = xGetElementById(imageIDStr);
	if (document.layers)	   /*NN4+*/
		szDivID.visibility = "hide";
	else if (document.getElementById) /*gecko(NN6) + IE 5+*/
		szDivID.style.display = "none";
	else if (document.all)	/* IE 4 */
		szDivID.style.visibility = "hidden";
	imageID.src = "../../uc/img/TPMAX2.GIF"/*tpa=http://www.halliburton.com/web-resources/uc/img/TPMAX2.GIF*/;
}

function xGetElementById(e) {
	if (typeof (e) != 'string') return e;
	if (document.getElementById)
		e = document.getElementById(e);
	else if (document.all)
		e = document.all[e];
	else
		e = null;
	return e;
}

/*
	This method clears 'DocListingBundle' from the querystring. It's called from the doc listing sidebar
	to clear the bundle querystring, but may also be called from other places. 
*/
function ClearDocListingBundleAction() {

	var action = $('#Form1').attr('action');
    var hashes = (action.indexOf('?') != -1 ? action.slice(action.indexOf('?') + 1).split('&') : '');
	var page = (action.indexOf('?') != -1 ? action.substring(0, action.indexOf('?')) : action);
	var qs = "";
    for(var i = 0; i < hashes.length; i++)
    {        
		if(hashes[i].indexOf('DocListingBundle') == -1)
			qs += (qs.length > 0 ? '&' : '') + hashes[i];
    }

	$('#Form1').attr('action', page + (qs.length > 0 ? '?' + qs : ''));
}
// END document listing common

// START Apply #centerColumn tab visiblity rules
function ApplyCenterColumnTabVisiblity() {
    //get tabbed nav component
    var tabn = $('.tabbedNavComponentTab');
    //ignore first tab in loop
    for (var x = 1; x < tabn.length; x++) {
        //clone tab contents
        var clon = $('.tab-rollup-' + x).clone();
        //get content script if any
        var scrp = clon.children('script');
        //remove script if any
        if(scrp.length > 0) {
            clon = scrp.remove().parent().first();
        }
        //trim content
        var cont = $.trim(clon.text());
        //show tab if content
		if (cont != '') {
		    $(tabn[x]).show();
        //hide tab if no content
		} else {
		    $(tabn[x]).hide();
		}
    }
}
// END Apply #centerColumn tab visiblity rules

//check if it is iPhone or iPad browser
function isiPhone() {
    return (
    //Detect iPhone
        (navigator.platform.indexOf("iPhone") != -1) ||
    //Detect iPad
        (navigator.platform.indexOf("iPad") != -1)
    );
}

// detect print events
(function() {
    var beforePrint = function() {
		$("body").append($('<style id="printStyleInjection">#centerColumnWrapper { width:auto !important; }</style>'));
    };
    var afterPrint = function() {
        $("#printStyleInjection").remove();
    };

    if (window.matchMedia) {
        var mediaQueryList = window.matchMedia('print');
        mediaQueryList.addListener(function(mql) {
            if (mql.matches) {
                beforePrint();
            } else {
                afterPrint();
            }
        });
    }

    window.onbeforeprint = beforePrint;
    window.onafterprint = afterPrint;
}());

