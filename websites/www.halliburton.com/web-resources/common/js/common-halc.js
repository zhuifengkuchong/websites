//

// BEGIN document listing common

function toggleBox(szDivIDStr, imageIDStr) /* 1 visible, 0 hidden */
{
    var szDivID = xGetElementById(szDivIDStr);
    var imageID = xGetElementById(imageIDStr);
    (document.getElementById) /*gecko(NN6) + IE 5+*/
    {
        var obj = szDivID;
        obj.style.display = (obj.style.display == 'none') ? 'block' : 'none';
        if (obj.style.display == 'none')
            imageID.src = "../img/arrow_down_new.gif"/*tpa=http://www.halliburton.com/web-resources/common/img/arrow_down_new.gif*/;
        else
            imageID.src = "../img/arrow_up.gif"/*tpa=http://www.halliburton.com/web-resources/common/img/arrow_up.gif*/;
    }
}

function Expand(szDivIDStr, imageIDStr) {
    var szDivID = xGetElementById(szDivIDStr);
    var imageID = xGetElementById(imageIDStr);
    if (document.layers)	   /*NN4+*/
        szDivID.visibility = "show";
    else if (document.getElementById) /*gecko(NN6) + IE 5+*/
        szDivID.style.display = "block";
    else if (document.all)	/* IE 4 */
        szDivID.style.visibility = "visible";
    imageID.src = "../img/arrow_up.gif"/*tpa=http://www.halliburton.com/web-resources/common/img/arrow_up.gif*/;
}

function xGetElementById(e) {
    if (typeof (e) != 'string') return e;
    if (document.getElementById)
        e = document.getElementById(e);
    else if (document.all)
        e = document.all[e];
    else
        e = null;
    return e;
}

function Collapse(szDivIDStr, imageIDStr) {
    var szDivID = xGetElementById(szDivIDStr);
    var imageID = xGetElementById(imageIDStr);
    if (document.layers)	   /*NN4+*/
        szDivID.visibility = "hide";
    else if (document.getElementById) /*gecko(NN6) + IE 5+*/
        szDivID.style.display = "none";
    else if (document.all)	/* IE 4 */
        szDivID.style.visibility = "hidden";
    imageID.src = "../img/arrow_down_new.gif"/*tpa=http://www.halliburton.com/web-resources/common/img/arrow_down_new.gif*/;
}

// Halcom Send Email
function halcomSendEmail() {
    var from = $("#halcomSendEmail_fromEmail").val();
    var to = $('#halcomSendEmail_toEmail').val();
    var message = $('#halcomSendEmail_message').val();
    var subject = $('#halcomSendEmail_subject').val();
    var cc = $('#halcomSendEmail_cc').is(':checked');
    var reg = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
    var webMethod = "/web-resources/uc/services/HalcomEmailPage.asmx/SendEmail";
    var parameters = "{'toEmail':'" + to + "','fromEmail':'" + from + "','subject':'" + subject + "','message':'" + message + "','cc':'" + cc + "','path':'"+document.location.href+"'}";

    if (to == '') {
        alert('Missing To Email');
        return false;
    } else if (to.indexOf('@') == -1 || !reg.test(to) ) {
        alert('Invalid To Email');
        return false
    } else if (subject == '') {
        alert('Missing Subject');
        return false;
    } else if (from == '') {
        alert('Missing From Email');
        return false;
    } else if (from.indexOf('@') == -1 || !reg.test(to)) {
        alert('Invalid From Email');
        return false;
    } else if (message == '') {
        alert('Missing Message Body');
        return false;
    } else {
        $.when($.colorbox.close()).done(function () {
            $.ajax({
                type: "POST",
                url: webMethod,
                data: parameters,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    alert('You have successfully posted');
                },
                error: function (e) {
                    alert("There was an error while trying to post the comments. ");
                }
            });
        });
    }
};

function shareFacebook() {

    var page = document.URL;
    var title = document.title;
    window.open('http://www.facebook.com/sharer.php?u=' + encodeURI(page) + '&t=' + encodeURI('halliburton'));


}
function shareTwitter() {

    var page = document.URL;
    var title = document.title;
    window.open('https://twitter.com/share?url=' + page + '&text=' + ' @halliburton');


}

function shareLinkedin() {

    var page = document.URL;
    var title = document.title;
    window.open('http://www.linkedin.com/shareArticle?mini=true&url=' + page + '&title=' + title);


}

function shareOilpro() {

    var page = document.URL;
    var title = document.title;
    window.open('http://oilpro.com/links/submit?url=' + encodeURIComponent(page));


}

function shareGoogle() {

    var page = document.URL;
    var title = document.title;
    window.open('https://plus.google.com/share?ur\l=' + encodeURIComponent(page)); 

}

//wait until the document is ready
$(document).ready(function () {


    /******** check search url remove primary nav current on search***************/

    var searchURL = document.URL;

    try {

        if (searchURL.match(/search/gi)) {

            $(".primaryNav li a").removeClass("primaryNavCurrent");

        }

    } catch (e) {




    }


    /***************************** check search url end *********************************/

    /*********** check tabbed background begin***************/

    var tabWidth = $("#centerColumnRow").width();

    if ((tabWidth > 600) && $("#tab-rollup-1").is(':visible')) {

        // $(".tabbedNavComponent, .tabbedNavComponent > ul").attr('style', 'background:none !important');
        //$(".tabbedNavComponent > ul > li:first-child").attr('style', 'background:#f1f1f1 !important');
        $("#centerColumnWrapper").attr('style', 'width:765px !important;');
        $("#tabbedNav").attr('style', 'width:765px !important');

    }

    /*********** check tabbed background end***************/

    /************************* Contact Us Search and reset buttons begin **********************/

    $(".contactUsComponent > div #form_wrap > div.submit > input").addClass('Halcom_Search_Button2');
    $(".contactUsComponent > div #form_wrap > div.submit > img").addClass('Halcom_Reset_Button2');


    /************************* Contact Us Search and reset buttons end **********************/

    /************ Document List search and reset button start *********************/


    $(".Halcom_Search_Button").attr('src', '../../uc/img/halcom_search.jpg'/*tpa=http://www.halliburton.com/web-resources/uc/img/halcom_search.jpg*/);
    $(".Halcom_Search_Button").attr('style', 'display:inline !important');

    $(".Halcom_Search_Button2").attr('src', '../../uc/img/button_submit.gif'/*tpa=http://www.halliburton.com/web-resources/uc/img/button_submit.gif*/);
    $(".Halcom_Search_Button2").attr('style', 'display:inline !important');

    $(".Halcom_Reset_Button").attr('src', '../../uc/img/halcom_reset.jpg'/*tpa=http://www.halliburton.com/web-resources/uc/img/halcom_reset.jpg*/);
    $(".Halcom_Reset_Button").attr('style', 'display:inline !important');

    $(".Halcom_Reset_Button2").attr('src', '../../uc/img/halcom_reset.jpg'/*tpa=http://www.halliburton.com/web-resources/uc/img/halcom_reset.jpg*/);
    $(".Halcom_Reset_Button2").attr('style', 'display:inline !important');


    /************ Document List search and reset button end *********************/

    /************* Sidebar Doc Listing Append url to update tab location begin *************/


    try {


        $("body").find(".tabbedNavComponentTab").each(function () {

            var x = $(this).css('background-image');


            if (x.match(/tab_active_bg/gi)) {

                $(this).attr('style', 'background: url("../img/tab_active_bg_halcom.gif"/*tpa=http://www.halliburton.com/web-resources/common/img/tab_active_bg_halcom.gif*/) repeat-x scroll 0 0;height:50px;');

            }

            if (x.match(/tab_inactive_bg/gi)) {

                $(this).attr('style', 'background: url("../img/tab_inactive_bg_halcom.gif"/*tpa=http://www.halliburton.com/web-resources/common/img/tab_inactive_bg_halcom.gif*/) repeat-x scroll 0 0;height:49px');

            }
        });

    } catch (e) {




    }

    /************* Sidebar Doc Listing Append url to update tab location end *************/


    /************ Contact us Spacing begin *********************/

    if ($(".secondaryNav ul").is(':empty')) {

        $(".secondaryNavContactUs").attr('style', 'margin-top:10px !important');


    }


    /************ Contact us Spacing end *********************/

    /*********** Spotlight Click begin **********************/

    $(".spotlightBody").click(function () {

        $(this).find("a").each(function () {

            var x = $(this).attr('href');
            window.location = x;

        });


    });


    /*********** Spotlight Click end **********************/

    /*********** homepage item click begin***************/

    $(".downloaditem").click(function () {

        var x = $(this).find("a").attr('href');

        window.location = x;


    });

    $("#mostpopular li").click(function () {


        var y = $(this).find("a").attr('href');

        if (y.match(/popup/gi)) {

            window.open(y, '_blank');

        } else {

            window.location = y;

        }


    });

    $(".solutionhome").click(function () {


        var y = $(this).find("a").attr('href');

        if (y.match(/popup/gi)) {

            window.open(y, '_blank');

        } else {

            window.location = y;

        }


    });

    $(".home_bottom_left_item").click(function () {


        var y = $(this).find("a").attr('href');

        if (y.match(/popup/gi)) {

            window.open(y, '_blank');

        } else {

            window.location = y;

        }


    });


    /*********** homepage item click end***************/

    $("#searchbox_globalNavigationSearchHalcom_SearchInput").attr('placeholder', 'Search');

    if (navigator.userAgent.match(/MSIE 7.0/)) {

        $("#searchbox_globalNavigationSearchHalcom_SearchInput").val('Search');


    }

    if (navigator.userAgent.match(/MSIE 8.0/)) {

        $("#searchbox_globalNavigationSearchHalcom_SearchInput").val('Search');


    }

    $("#searchbox_globalNavigationSearchHalcom_SearchInput").click(function () {

        if (navigator.userAgent.match(/MSIE 7.0/)) {

            $(this).val('');

        }

        if (navigator.userAgent.match(/MSIE 8.0/)) {

            $(this).val('');

        }

    });

    $("#searchbutton_globalNavigationSearchHalcom_SearchInput img").attr('src', '../img/btn_search_new.gif'/*tpa=http://www.halliburton.com/web-resources/common/img/btn_search_new.gif*/);
    $("#searchbutton_globalNavigationSearchHalcom_SearchInput img").show();
    $(".halcomHomeComponents > #home_mid_wrapper > #home_right_wrapper >  #home_news_content_wrapper > #home_news_content_wrapper_ul > li:last-child").hide();
    var centerWidth = $("#centerColumnRow").width();
    $(".halcomHomeComponents > #home_bottom_wrapper > #home_bottom_mid > #home_middle_wrapper > table").last().attr('style', 'border-bottom:0px !important');

    $(".halcomHomeComponents > #home_bottom_wrapper > #home_bottom_mid > ul > li").last().attr('style', 'border-bottom:0px !important');

    $(".halcomHomeComponents > #home_bottom_wrapper > #home_bottom_left > .home_bottom_left_item").last().attr('style', 'border-bottom:0px !important');

    $("#home_left_wrapper").load("http://www.halliburton.com/public/pubsdata/customer-resources/mainslider/mainslider.html");

    if ($("#breadCrumbComponent") && $("#rightColumn").is(':visible')) {

        $("#contentWrapper").addClass('contentWrapperBack');
    }

    if (centerWidth > 600) {

        $("#contentWrapper").addClass('contentWrapperBack-twocol');

    }

    if ($("#centerColumn-OneColumnLayout").is(':visible')) {

        $("#contentWrapper").css('background-color', '#ffffff');

    }


    /****** Customer Center Begin ************/

    var cc = $(".secondaryNavTitle a").text();

    try {

        if (cc.match(/Petrobras/gi)) {

            $("#contentWrapper").addClass('contentWrapperBack-twocol');
            

        }

    } catch (e) {


    }

    try {

        if (cc.match(/ExxonMobil/gi)) {

            $("#contentWrapper").addClass('contentWrapperBack-twocol');


        }

    } catch (e) {


    }


    /******* Customer Center End **************/

    if ($("#toolsAndResourcesComponent").is(':visible')) {


        $("#contentWrapper").removeClass('contentWrapperBack-twocol');
        $("#contentWrapper").addClass('contentWrapperBack-toolsresources');
        $("#centerColumn").css({ 'padding': '0px', 'padding-left': '0px', 'padding-right': '0px' });

        $("#centerColumnWrapper").attr('style', 'width:769px !important;margin-left:8px');

        $("#tabbedNav").hide();

        if (navigator.userAgent.match(/MSIE 7.0/)) {

            $("#contentWrapper").attr('style', 'background: url(/web-resources/common/img/contentwrapperbackground-tools.png) repeat !important;overflow:hidden');


        }

        if (navigator.userAgent.match(/MSIE 8.0/)) {

            $("#contentWrapper").attr('style', 'background: url(/web-resources/common/img/contentwrapperbackground-tools.png) repeat !important;overflow:hidden');
            $("#centerColumnWrapper").attr('style', 'width:769px !important;margin-left:8px');
            $("#toolsAndResourcesComponent > #content_twoCol > .toolsAndResourcesHomeComponent > .content_twoCol_right").css('margin-right', '-1px');
        }

    }

    $("#searchbutton_ErrorPageSearchHalcom_SearchInput img").attr('src', '../../uc/img/halcom_search.jpg'/*tpa=http://www.halliburton.com/web-resources/uc/img/halcom_search.jpg*/);
    $("#searchbutton_ErrorPageSearchHalcom_SearchInput img").css('display', 'inline');

    /*********** tools and resources begin *********************/

    $(".toolsSection").click(function () {

        $(this).find("a").each(function () {

            var x = $(this).attr('href');
            window.location = x;


        });

    });

    $(".Halcom_Search_Button").hover(function () {

        $(this).attr('src', '../../uc/img/halcom_search_on.jpg'/*tpa=http://www.halliburton.com/web-resources/uc/img/halcom_search_on.jpg*/);


    }, function () {

        $(this).attr('src', '../../uc/img/halcom_search.jpg'/*tpa=http://www.halliburton.com/web-resources/uc/img/halcom_search.jpg*/);

    });

    $(".Halcom_Reset_Button").hover(function () {

        $(this).attr('src', '../../uc/img/halcom_reset_on.jpg'/*tpa=http://www.halliburton.com/web-resources/uc/img/halcom_reset_on.jpg*/);


    }, function () {

        $(this).attr('src', '../../uc/img/halcom_reset.jpg'/*tpa=http://www.halliburton.com/web-resources/uc/img/halcom_reset.jpg*/);

    });

    /*********** tools and resources end *********************/

    /********** check secondary nav title length begin ***********/

    $(".secondaryNavTitle").find("a").each(function () {

        var x = $(this).text().length;

        if (x >= 24) {

            $(this).css('line-height', 'normal');

        }


    });


    /********** check secondary nav title length end ***********/


    $(".secondaryNavCurrent").parents("li").css('background', 'url(/web-resources/common/img/secondary_nav_arrow_down.png) no-repeat');
    $(".secondaryNavCurrent").parents("li").css('background-position', '205px 8px');
    $(".secondaryNavCurrent").parent().css('background-color', '#f1f1f1');

    $(".secondaryNavCurrent").parents("li").css('background-color', '#f1f1f1');

    $("#productsProductListComponent > .productList > table tr:last-child .productListDescr").css('border-bottom', '0px');


    $(".secondaryNav > ul > li > ul li .secondaryNavCurrent").parent("li").css('background', 'url(/web-resources/common/img/secondary_nav_second_arrow.png) no-repeat');
    $(".secondaryNav > ul > li > ul li .secondaryNavCurrent").parent("li").css('background-position', '0px 7px');

    $("#toolsAndResourcesComponent > #content_twoCol > .toolsAndResourcesHomeComponent > .content_twoCol_left > .icon:first").css('border-top', '0px');
    $("#toolsAndResourcesComponent > #content_twoCol > .toolsAndResourcesHomeComponent > .content_twoCol_right > .icon:first").css('border-top', '0px');

    $("body").find(".icon_Title").each(function () {

        var mytext = $(this).text();

        if (mytext == "Calculators") {

            $(this).parent().css({ 'background': 'url(/web-resources/common/img/tools-calc-back.png)', 'background-repeat': 'no-repeat', 'background-position': '300px 0px' });

        } else if (mytext == "Case Studies") {

            $(this).parent().css({ 'background': 'url(/web-resources/common/img/tools-case-back.png)', 'background-repeat': 'no-repeat', 'background-position': '300px 0px' });

        } else if (mytext == "Downloads & Mobile Apps") {

            $(this).parent().css({ 'background': 'url(/web-resources/common/img/tools-downloads-back.png)', 'background-repeat': 'no-repeat', 'background-position': '300px 0px' });

        } else if (mytext == "Safety") {

            $(this).parent().css({ 'background': 'url(/web-resources/common/img/tools-safety-back.png)', 'background-repeat': 'no-repeat', 'background-position': '320px 0px' });

        } else if (mytext == "Simulators") {

            $(this).parent().css({ 'background': 'url(/web-resources/common/img/tools-simulators-back.png)', 'background-repeat': 'no-repeat', 'background-position': '320px 0px' });

        } else if (mytext == "Technical Papers") {

            $(this).parent().css({ 'background': 'url(/web-resources/common/img/tools-techpapers-back.png)', 'background-repeat': 'no-repeat', 'background-position': '320px 0px' });

        } else if (mytext == "Training") {

            $(this).parent().css({ 'background': 'url(/web-resources/common/img/tools-training-back.png)', 'background-repeat': 'no-repeat', 'background-position': '315px -5px' });

        }

    });

    if ($(".productListDescr").is(':hidden')) {

        $(".productList > table > tbody > tr > td > input[type=image]").attr('src', '../img/arrow_down_new.gif'/*tpa=http://www.halliburton.com/web-resources/common/img/arrow_down_new.gif*/);

    } else {

        $(".productList > table > tbody > tr > td > input[type=image]").attr('src', '../img/arrow_up.gif'/*tpa=http://www.halliburton.com/web-resources/common/img/arrow_up.gif*/);
    }

    $(".productList > table > tbody > tr > td > input[type=image]").on("click", function () {

        var test = $(this).attr('src');
        $(this).parent().css({ 'width': '19px', 'max-width': '19px' });

        if (test == '../img/arrow_down_new.gif'/*tpa=http://www.halliburton.com/web-resources/common/img/arrow_down_new.gif*/) {

            $(this).attr('src', '../img/arrow_up.gif'/*tpa=http://www.halliburton.com/web-resources/common/img/arrow_up.gif*/);


        } else {

            $(this).attr('src', '../img/arrow_down_new.gif'/*tpa=http://www.halliburton.com/web-resources/common/img/arrow_down_new.gif*/);

        }

    });

    /************* position sharebox in ie ******************/

    if (navigator.userAgent.match(/MSIE 8.0/)) {


        var x = $("#globalNavigationRegistrationLinks_regional").position();

        // $(".shareBox").css({ 'left': x.left, 'top': '128px' });

        $(".globalNavUtility").css({ 'position': 'relative', 'z-index': '2000' });

        $(".shareBox").removeAttr('z-index');

        $("html").attr('style', 'max-width:100% !important; overflow-x:hidden !important');


    }

    if (navigator.userAgent.match(/MSIE 7.0/)) {



        $("html").attr('style', 'max-width:100% !important; overflow-x:hidden !important');

        var x = $("#globalNavigationRegistrationLinks_regional").position();

        // $(".shareBox").css({ 'left': x.left, 'top': '128px' });

        $(".globalNavUtility").css({ 'position': 'relative', 'z-index': '2000' });

        $(".shareBox").removeAttr('z-index');

        $("#home_bottom_left").css('height', '575px');
        $("#home_bottom_mid").css('height', '575px');
        $("#home_bottom_right").css('height', '575px');




    }

    /************* position sharebox in ie end ******************/

    /************* breadcrumb table position in ie begin ******************/

    if (navigator.userAgent.match(/MSIE 8.0/)) {

        /*var x = $("#globalNavigationRegistrationLinks_regional").position();
        $(".shareBox").css({ 'left': x.left, 'top': '128px' });
        $("#rightColumn").css('z-index', '1');*/

        $(".mainShowcaseTable").css({ 'position': 'relative', 'top': '-1px' });
        $(".sidebarShowcaseComponent table tr td").attr('style', 'height:30px !important');
        $(".showcaseHeader_1").css({ 'position': 'relative', 'top': '1px' });
        $(".showcaseHeader_2").css({ 'position': 'relative', 'top': '1px' });
        $(".showcaseHeader_3").css({ 'position': 'relative', 'top': '1px' });

    }

    /************* breadcrumb table position in ie end ******************/



    $(".sidebarShowcaseComponent  table  tbody  tr  td  table  tbody  tr  td  div:first").css('border-bottom', '1px solid #cccccc');
    $(".sidebarShowcaseComponent  table  tbody  tr  td  table  tbody  tr  td  div:nth-child(2)").css('color', '#363636');
    $(".sidebarShowcaseComponent  table  tbody  tr  td  table  tbody  tr  td  div:nth-child(2)").css('font-weight', 'bold');
    $(".Halcom_Showcase").show();
    $(".halMainDocTable tr td .HalcomDataGridImage").attr('src', '../img/arrow_up.gif'/*tpa=http://www.halliburton.com/web-resources/common/img/arrow_up.gif*/);
    $(".HalcomDataGridImage").show();
    $(".lockClass").attr('src', '../../uc/img/lock.png'/*tpa=http://www.halliburton.com/web-resources/uc/img/lock.png*/).show();
    $(".lockClass").attr('title', 'Sign in required');
    /*$(".lockClass").removeAttr('align');
    $(".lockClass").css({ 'float': 'right', 'padding-bottom': '5px' });*/
    $(".lockClassTop").attr('src', '../../uc/img/lock.png'/*tpa=http://www.halliburton.com/web-resources/uc/img/lock.png*/).show();


    // careers home begin

    var careerURL = document.URL.split('=')[1];

    if (careerURL == "hgbr8q6n") {

        $("#centerColumn").css('padding', '0px');
        $("#centerColumn").css('margin-left', '7px');
        $("h1").css('display', 'none');
        $("#centerColumnWrapper").css('margin-left', '0px');
        $("#centerColumnWrapper").attr('style', 'width:776px !important; margin-left:0px;float:right');
        $("#tabbedNavRow").css('display', 'none');

    }

    // careers home end

    /****************************** Consulting homepage begin ********************************/

    var consultingURL = document.URL.split('=')[1];
    var hp = $("h1").text();
    var hp2 = $.trim(hp);

    if ((consultingURL == "hfqelac0") && (hp2 == "Consulting")) {

        $("#centerColumn").css('padding', '0px');
        $("#centerColumn").css('margin-left', '7px');
        $("h1").css('display', 'none');
        $("#centerColumnWrapper").css('margin-left', '0px');
        $("#centerColumnWrapper").attr('style', 'width:776px !important; margin-left:0px;float:right');
        $("#tabbedNavRow").css('display', 'none');

    }



    /****************************** Consulting homepage end ********************************/

    /****************************** Project Management homepage begin ********************************/

    var projectURL = document.URL.split('=')[1];

    if (projectURL == "hgoxc0ih") {

        $("#centerColumn").css('padding', '0px');
        $("#centerColumn").css('margin-left', '7px');
        $("h1").css('display', 'none');
        $("#centerColumnWrapper").css('margin-left', '0px');
        $("#centerColumnWrapper").attr('style', 'width:776px !important; margin-left:0px;float:right');
        $("#tabbedNavRow").css('display', 'none');

    }



    /****************************** Project Management homepage end ********************************/


    /****************************** deepwater homepage begin ********************************/

    var deepURL = document.URL.split('=')[1];

    if (deepURL == "hgeyxss4") {

        $("#centerColumn").css('padding', '0px');
        $("#centerColumn").css('margin-left', '7px');
        $("h1").css('display', 'none');
        $("#centerColumnWrapper").css('margin-left', '0px');
        $("#centerColumnWrapper").attr('style', 'width:776px !important; margin-left:0px;float:right');
        $("#tabbedNavRow").css('display', 'none');

    }



    /****************************** deepwater homepage end ********************************/

    /****************************** unconventional homepage begin ********************************/

    var unconventionalURL = document.URL.split('=')[1];

    if (unconventionalURL == "hgjyd46v") {

        $("#centerColumn").css('padding', '0px');
        $("#centerColumn").css('margin-left', '7px');
        $("h1").css('display', 'none');
        $("#centerColumnWrapper").css('margin-left', '0px');
        $("#centerColumnWrapper").attr('style', 'width:776px !important; margin-left:0px;float:right');
        $("#tabbedNavRow").css('display', 'none');
        $(".spotlightComponent .spotlightBody").attr('style', 'width:95%;margin-left:15px;');

    }



    /****************************** unconventional homepage end ********************************/

    /****************************** mature fields homepage begin ********************************/

    var maturefieldsURL = document.URL.split('=')[1];

    if (maturefieldsURL == "hgjyd46l") {

        $("#centerColumn").css('padding', '0px');
        $("#centerColumn").css('margin-left', '7px');
        $("h1").css('display', 'none');
        $("#centerColumnWrapper").css('margin-left', '0px');
        $("#centerColumnWrapper").attr('style', 'width:776px !important; margin-left:0px;float:right');
        $("#tabbedNavRow").css('display', 'none');
        $(".spotlightComponent .spotlightBody").attr('style', 'width:95%;margin-left:15px');

    }



    /****************************** mature fields homepage end ********************************/


    /************ Product List Component background begin **************/

    var productURL = document.URL.split('=')[1];

    if (productURL == "h8cyv98a") {

        $("#contentWrapper").addClass('contentWrapperBack-products');

    }


    /************ Product List Component background end **************/


    // contact home begin

    var contactURL = document.URL.split('=')[1];



    if (contactURL == "hgbr8q6p") {

        $("#centerColumn").css('padding', '0px');
        $("#centerColumn").css('margin-left', '7px');
        $("h1").css('display', 'none');
        $("#centerColumnWrapper").css('margin-left', '0px');
        $("#centerColumnWrapper").attr('style', 'width:776px !important; margin-left:0px;float:right');
        $("#tabbedNavRow").css('display', 'none');

    } else if (contactURL == "hgbr8q6p&SRC") {

        $("#centerColumn").css('padding', '0px');
        $("#centerColumn").css('margin-left', '7px');
        $("h1").css('display', 'none');
        $("#centerColumnWrapper").css('margin-left', '0px');
        $("#centerColumnWrapper").attr('style', 'width:776px !important; margin-left:0px;float:right');
        $("#tabbedNavRow").css('display', 'none');

    }

    $("#location_sub_table tr:nth-child(odd)").css('background', '#f1f1f1');

    // contact home end

    // contact us map request

    $("#the_map").load('http://www.halliburton.com/public/pubsdata/Home_Page/locations/newmap.html');

    // end contact map request

    // remove skype image

    $(".skype_pnh_container").attr("onmouseover", "").attr("onclick", "");

    // $("head").append('<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />');

    // end skype removal

    //Halcom FIX: Extend menu item of last child
    var primaryNavLi = 0;
    var primaryNavBase = 6;
    var primaryNavWidth = $('.primaryNav:eq(0)').width();
    $('.primaryNav > ul > li').each(function () { primaryNavLi += $(this).width(); });
    var primaryNavDiff = (primaryNavWidth - primaryNavLi);
    var primaryNavTotal = (primaryNavBase + primaryNavDiff);
    //extend if full width
    if (primaryNavDiff < (primaryNavBase * 5)) {
        $('#globalNavWrapper .primaryNav > ul > li:last-child').css('border-right', '0px!important');
        $('#globalNavWrapper .primaryNav > ul > li:last-child a').css('width', '100%');
        //ie7 exception
        if (!$.support.cssFloat && isIE7()) {
            $('#globalNavWrapper .primaryNav > ul > li:last-child a').css('padding-right', primaryNavTotal);
        }
    }

    //Email Page Link in Footer
    $('.halcomSendEmail').on('click', function (e) {
        e.stopPropagation();
        e.preventDefault();
        var sende = '<div class="halSendEmail">';
        sende += '<div class="halSendEmailWrapper"><h1>Email This Page</h1><div>Separate multiple addresses with commas:</div><br>'
        sende += '<div class="listing"><div class="label">Email To: </div><input type="text" name="toEmail" id="halcomSendEmail_toEmail"></div>';
        sende += '<div class="listing"><div class="label">Subject: </div><input type="text" name="subject" id="halcomSendEmail_subject"></div>';
        sende += '<div class="listing"><div class="label">Your email address: </div><input type="text" name="fromEmail" id="halcomSendEmail_fromEmail"></div>';
        sende += '<div class="listing"><div class="label">&nbsp; </div><label><input type="checkbox" name="cc" id="halcomSendEmail_cc" />Copy Me</label></div>';
        sende += '<div class="listing"><div class="label">Message </div><textarea name="txtMessage" id="halcomSendEmail_message" rows="5"></textarea></div>';
        sende += '<div class="submit"><input type="button" value="Send" onclick="javascript:halcomSendEmail();"></div>';
        sende += '</div></div>';
        $.colorbox({ html: sende, width: 450, height: 400 });
        return false;
    });


});