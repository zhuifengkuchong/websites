/* Image Slider */
jQuery(document).ready(function() {
  
    //Set Default State of each portfolio piece
	var imageCount=jQuery(".paging a").size();
	
 
	
      
    //Get size of images, how many there are, then determin the size of the image reel.
    var imageWidth = jQuery(".window").width();
    var imageSum = jQuery(".image_reel img").size();
    var imageReelWidth = imageWidth * imageSum;
    
    //Adjust the image reel to its new size
    jQuery(".image_reel").css({'width' : imageReelWidth});
    
    //Paging + Slider Function
    rotate = function(){  
		
      //Get number of times to slide
	   var triggerID = jQueryactive.attr("rel") - 1;
      var image_reelPosition = triggerID * imageWidth; //Determines the distance the image reel needs to slide
	
      jQuery(".paging a").removeClass('active'); //Remove all active class
      jQueryactive.addClass('active'); //Add active class (the jQueryactive is declared in the rotateSwitch function)
      
      //Slider Animation
      jQuery(".image_reel").animate({ 
        left: -image_reelPosition
      }, 200 );
	  

      
    }; 
    
    //Rotation + Timing Event
    rotateSwitch = function(){  
	
      play = setInterval(function(){ //Set timer - this will repeat itself every 3 seconds
        jQueryactive = jQuery('.paging a.active').next();
        if ( jQueryactive.length === 0) { //If paging reaches the end...
          jQueryactive = jQuery('.paging a:first'); //go back to first
        }
        rotate(); //Trigger the paging and slider function
      }, 11000); //Timer speed in milliseconds (6 seconds)
    };
    
	if(imageCount > 1)
	{
	   jQuery(".paging").show();
    jQuery(".paging a:first").addClass("active");
		rotateSwitch(); //Run function on launch
		}
		else
		{
		   jQuery(".paging").hide();
		    jQuery("#playpause").hide();
    //jQuery(".paging a:first").removeClass("active");
		}
		
    /* commented out this section because of the play/pause feature
    //On Hover
    jQuery(".image_reel a").hover(function() {
      clearInterval(play); //Stop the rotation
    }, function() {
      rotateSwitch(); //Resume rotation
    });  
    */
    
    /* new hover section for the play/pause feature */
    jQuery("#pausebutton").hover(function() {
      jQuery(this).attr("src","Unknown_83_filename"/*tpa=https://www2.dteenergy.com/wps/wcm/connect/079147ee-700e-4ef9-94bc-29736d55c4ef/images/icons/sliderPause_down.png*/)
    }, function() {
      jQuery(this).attr("src","Unknown_83_filename"/*tpa=https://www2.dteenergy.com/wps/wcm/connect/079147ee-700e-4ef9-94bc-29736d55c4ef/images/icons/sliderPause_up.png*/)
    });
    
    jQuery("#playbutton").hover(function() {
      jQuery(this).attr("src","Unknown_83_filename"/*tpa=https://www2.dteenergy.com/wps/wcm/connect/079147ee-700e-4ef9-94bc-29736d55c4ef/images/icons/sliderPlay_down.png*/)
    }, function() {
      jQuery(this).attr("src","Unknown_83_filename"/*tpa=https://www2.dteenergy.com/wps/wcm/connect/079147ee-700e-4ef9-94bc-29736d55c4ef/images/icons/sliderPlay_up.png*/)
    });

    
    //On Click
    jQuery(".paging a").click(function() {  
      jQueryactive = jQuery(this); //Activate the clicked paging
      //Reset Timer
      clearInterval(play); //Stop the rotation
      rotate(); //Trigger rotation immediately
      if (jQuery("#playbutton").css("display") == "none")
      {
        //alert("no play button visible, slideshow continuing");
        rotateSwitch(); // Resume rotation
      }
      return false; //Prevent browser jump to link anchor
    });  
    
  });

  function playClick()
  {
    jQuery("#pausebutton").show();
    jQuery("#playbutton").hide();
    rotateSwitch();
  }
  
  function pauseClick()
  {
    jQuery("#playbutton").show();
    jQuery("#pausebutton").hide();
    clearInterval(play);
  }