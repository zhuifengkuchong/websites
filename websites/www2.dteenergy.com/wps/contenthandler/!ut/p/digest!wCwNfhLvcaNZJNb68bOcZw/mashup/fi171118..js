(function(){
var w=window,i$=function(){
if(i$.qel){
return i$.qel.apply(this,arguments);
}
};
w.i$=i$;
i$.global=w;
if(typeof (console)=="undefined"){
var f=function(){
};
console={log:f,debug:f,info:f,warn:f,error:f,assert:f};
}
i$.partial=function(f){
var _1=i$.toArray(arguments).slice(1);
return function(){
var _2=_1.slice(0),_3=i$.toArray(arguments),i=0;
for(;i<_2.length;i++){
if(_2[i]===undefined){
_2[i]=_3.shift();
}
}
_2.push.apply(_2,_3);
return f.apply(this,_2);
};
};
i$.scope=function(s,f){
var of=f;
f=function(){
return (i$.isString(of)?s[of]:of).apply(s,arguments);
};
return i$.partial.apply(this,i$.toArray(arguments).slice(1));
};
i$.error=function(_4,_5){
console.error(_5||new Error(_4));
};
i$.forEach=function(_6,f,_7){
if(_7==null){
_7=0;
}
for(var i=(_7>=0)?_7:0;i<_6.length;i++){
f(_6[i],i,_6);
}
};
i$.forIn=function(o,f){
for(var i in o){
f(o[i],i,o);
}
};
i$.each=function(o,f,s){
if(s){
f=i$.scope(s,f);
}
if(o){
if(o instanceof Array||typeof o.length==="number"){
i$.forEach(o,f);
}else{
i$.forIn(o,f);
}
}
};
i$.some=function(a,f,s){
if(s){
f=i$.scope(s,f);
}
for(var i=0;i<a.length;i++){
if(f(a[i])){
return true;
}
}
return false;
};
i$.every=function(o,f,s){
if(s){
f=i$.scope(s,f);
}
return !i$.some(o,function(_8){
return !f(_8);
});
};
i$.wrap=function(o,n,f){
var fn=o[n];
o[n]=function(){
return f.call(this,fn,arguments);
};
o[n]._wrapped=fn;
return o[n];
};
i$.unwrap=function(o,n){
var fn=o[n];
if(fn&&fn._wrapped){
o[n]=fn._wrapped;
}
return o[n];
};
i$.copyShallow=function(o){
var r=i$.isArrayLike(o)?[]:{};
i$.forIn(o,function(v,k){
r[k]=v;
});
return r;
};
var _9=function(_a,_b,_c,_d){
if(_c||_b[_d]===undefined){
_b[_d]=function(){
return this[_a][_d].apply(this[_a],arguments);
};
}
},_e=function(_f,_10,_11,_12){
if(_11||_10[_12]===undefined){
_10[_12]=function(){
return _f[_12].apply(_f,arguments);
};
}
};
i$.shadow=function(s,t,_13,_14){
i$.each(_13,i$.partial(i$.isString(s)?_9:_e,s,t,_14));
};
var _15=function(_16,c,s){
var i,p,ts=s||i$.global;
for(i=0;ts!=null,i<_16.length,p=_16[i];i++){
if(ts[p]==null){
if(c){
ts[p]={};
}else{
ts=null;
break;
}
}
ts=ts[p];
}
return ts;
};
i$.fromPath=function(n,c,s){
var _17=n.split(".");
return _15(_17,c,s);
};
i$.toPath=function(n,v,s){
var _18=n.split("."),p=_18.pop(),o=_15(_18,true,s);
o[p]=v;
return v;
};
i$.cachedFn=function(f,s){
var val;
var fn=function(){
if(!fn.called){
fn.called=true;
val=f.apply(s,arguments);
}
return val;
};
return fn;
};
i$.xhrFmts={text:function(xhr){
return xhr.responseText;
},json:function(xhr){
return !(/[^,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t]/.test(xhr.responseText.replace(/"(\\.|[^"\\])*"/g,"")))&&eval("("+xhr.responseText+")");
},xml:function(xhr){
return xhr.responseXML;
},javascript:function(xhr){
if((/[^,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t]/.test(str.replace(/"(\\.|[^"\\])*"/g,"")))){
throw new SyntaxError("Invalid characters in javascript object");
}else{
return eval("("+xhr.responseText+")");
}
}};
})();
(function(){
(function(ua){
var _19=function(_1a){
return parseFloat(_1a);
},_1b=[["IE",/MSIE\s*([\S]+)*/],["FF",/Firefox\/([\S]+)*/],["Opera",/Opera[\s\/]([\S]+)*/],["Safari",/Version\/([\S]+)*[\s\S]*Safari/],["Chrome",/Chrome\/([\S]+)*/],["WebKit",/AppleWebKit\/([\S]+)*/]];
i$.each(_1b,function(_1c){
var m=_1c[1].exec(ua);
if(m&&m.length>1){
i$["is"+_1c[0]]=_19(m[1]);
}
});
})(navigator.userAgent);
var _1d=document.documentMode;
if(_1d&&_1d!=5&&Math.floor(i$.isIE)!=_1d){
i$.isIE=_1d;
}
i$.isNode=function(o){
return typeof o==="object"&&typeof o.nodeType==="number"&&typeof o.nodeName==="string";
};
i$.isFunction=function(o){
return typeof o==="function"||o instanceof Function;
};
i$.isObject=function(o){
return typeof o==="object";
};
i$.isArray=function(o){
return o instanceof Array;
};
i$.isString=function(o){
return typeof o==="string";
};
i$.isNumber=function(o){
return typeof o==="number";
};
i$.isBoolean=function(o){
return typeof o==="boolean";
};
i$.isLikeArray=function(o){
return o instanceof Array||typeof o.length==="number";
};
i$.toArray=function(o){
return Array.prototype.slice.call(o);
};
if(i$.isIE){
var _1e=i$.toArray;
i$.toArray=function(o){
try{
return _1e(o);
}
catch(err){
var a=new Array(o.length);
for(var i=0;i<o.length;i++){
a[i]=o[i];
}
return a;
}
};
}
var _1f=false,_20=[],_21=[];
i$._initPage=function(){
var fn;
_1f=true;
if(window.detachEvent){
window.detachEvent("onload",i$._initPage);
}
while(_20.length>0){
if(fn=_20.shift()){
fn();
}
}
};
i$._exitPage=function(){
var fn;
while(_21.length>0){
if(fn=_21.shift()){
fn();
}
}
};
i$._addEvent=function(e,f,o){
var w=o?o:window;
var s=w.attachEvent?e:e.substring(2);
var a=w.attachEvent||w.addEventListener;
a(s,function(){
f.apply(w,arguments);
},false);
};
i$._addEvent("onload",i$._initPage);
i$._addEvent("onunload",i$._exitPage);
if(document.addEventListener){
document.addEventListener("DOMContentLoaded",i$._initPage,false);
}
i$.addOnLoad=function(f,o){
if(o){
f=i$.scope(o,f);
}
if(_1f){
f();
}else{
_20.push(f);
}
};
i$.addOnUnload=function(f,o){
if(o){
f=i$.scope(o,f);
}
_21.push(f);
};
var mx=function(o,m){
for(var p in m){
if(m.hasOwnProperty(p)){
o[p]=m[p];
}
}
},mxn=function(o,m,_22){
i$.forEach(_22,function(p){
if(m.hasOwnProperty(p)){
o[p]=m[p];
}
});
};
i$.mash=function(o){
i$.forEach(arguments,function(v){
mx(o,v);
},1);
return o;
};
i$.mashSpec=function(n,o){
i$.forEach(arguments,function(v){
mxn(o,v,n);
},2);
return o;
};
i$.augment=function(f){
var r=f;
if(f&&f.prototype){
f=f.prototype;
i$.mash.apply(i$,arguments);
}
return r;
};
i$.make=(function(){
var l=function(){
};
return function(o){
l.prototype=o;
o=new l();
return i$.mash.apply(i$,arguments);
};
})();
var _23=/^\s+/g;
i$.trim=function(str){
str=str.replace(_23,"");
var i=str.length-1;
while(str.charAt(i)==" "){
i--;
}
return str.substring(0,i+1);
};
var _24=i$.isArray,_25=i$.isObject;
i$.merge=function(_26,_27,_28){
var _28=_28||[],v,c;
_27=_27||i$.global;
if(_24(_26)&&_24(_27)){
_27.push.apply(_27,_26);
}else{
for(var x in _26){
if(_26.hasOwnProperty(x)){
v=_26[x],c=_27[x];
if(c!=null&&((_24(v)&&_24(c))||(_25(v)&&_25(c)))){
_27[x]=i$.merge(v,c,_28.concat(x));
}else{
_27[x]=v;
}
}
}
}
return _27;
};
var _29;
i$.isRTL=function(_2a){
if(!_29){
_29=i$.fromPath("ibmCfg.themeConfig.RTLMap");
}
var _2b=_29||{"iw":1,"he":1,"ar":1};
return (_2a.substring(0,2) in _2b);
};
})();

(function(){
	if(i$.isIE){
		document.createElement('article');
		document.createElement('aside');
		document.createElement('footer');
		document.createElement('header');
		document.createElement('hgroup');
		document.createElement('nav');
		document.createElement('section');
	}
	if(i$.isIE == 7){ document.getElementsByTagName("html")[0].className+=" wptheme_ie7"; }
	if(i$.isIE == 8){ document.getElementsByTagName("html")[0].className+=" wptheme_ie8"; }
})();

var wpthemeAriaRegionCounter = 0;
var wpthemeAriaRegionIdArray = [];(function(){
i$.Promise=function(){
this._cbs=[];
this._stat=-1;
};
i$.promise={};
i$.mash(i$.promise,{isPromise:function(o){
return o&&i$.isFunction(o.then);
},resolved:function(o){
var p=new i$.Promise();
p.resolve(o);
return p;
},rejected:function(_1){
var p=new i$.Promise();
p.reject(_1);
return p;
},join:function(_2){
var _3=new i$.Promise(),_4=new Array(_2.length),_5=0,_6=false,_7=function(){
if(++_5>=_4.length){
_3[_6?"reject":"resolve"](_4);
}
};
if(_2.length>0){
i$.each(_2,function(p,i){
p.then(function(v){
_4[i]=v;
_7();
},function(e){
_4[i]=e;
_7();
});
});
}else{
_3.resolve([]);
}
return _3;
}});
i$.mash(i$,{when:function(o){
return i$.promise.isPromise(o)?o:i$.promise.resolved(o);
},whenAll:function(o){
var a=[];
i$.each(arguments,function(p){
a.push(i$.when(p));
});
return i$.promise.join(a);
}});
i$.Promise.prototype={_fin:function(v,s){
if(this._stat!==-1){
throw new Error("Promise already resolved");
}
this._v=v||null;
this._stat=s;
this._cbk();
return this;
},_cbk:function(){
var st=this._stat,_8=this._cbs,v=this._v,f;
if(st===0){
if(i$.promise.isPromise(v)){
while(_8.length>0){
v.then.apply(v,_8.shift());
}
}
}
while(_8.length>0){
f=_8.shift()[st];
if(f){
try{
f(v);
}
catch(err){
}
}
}
},_delegate:function(fn){
var p=new i$.Promise();
this.then(i$.partial(fn,p),i$.scope(p,"reject"));
return p;
},resolve:function(v){
return this._fin(v,0);
},reject:function(e){
return this._fin(e,1);
},progress:function(p){
i$.each(this._cbs,function(_9){
if(_9[2]){
_9[2](p);
}
});
return this;
},then:function(_a,_b,_c){
var p=new i$.Promise();
this._cbs.push([function(v){
try{
if(_a){
var rv=_a(v);
if(rv!==undefined){
v=rv;
}
}
p.resolve(v);
}
catch(exc){
p.reject(exc);
}
},function(e){
var rv=e;
try{
if(_b){
rv=_b(e);
if(rv===undefined){
rv=e;
}
}
}
catch(exc){
rv=exc;
}
p.reject(rv);
},_c]);
if(this._stat!==-1){
this._cbk();
}
return p;
},call:function(_d,_e){
return this._delegate(function(p,_f){
if(_f&&i$.isFunction(_f[_d])){
p.resolve(_f[_d].apply(_f,_e));
}else{
p.reject(new Error(_d+" is not a function on "+o));
}
});
},get:function(_10){
return this._delegate(function(p,_11){
if(_11){
p.resolve(_11[_10]);
}else{
p.reject(new Error(_11+" is null or undefined"));
}
});
}};
})();
(function(){
i$.getXHR=typeof XMLHttpRequest!=="undefined"?function(){
return new XMLHttpRequest();
}:function(){
return new ActiveXObject("MSXML2.XMLHTTP.3.0");
};
i$.toQuery=function(o){
var q=[];
i$.each(o,function(v,k){
if(i$.isString(v)){
q.push(k+"="+v);
}else{
if(i$.isArray(v)){
i$.each(v,function(av,i){
q.push(k+"="+av);
});
}
}
});
return q.join("&");
};
i$.fromQuery=function(q){
var o={};
i$.each(q.split("&"),function(av,i){
var p=av.split("="),k=p[0],v=p[1],cv=o[k];
if(cv){
if(!i$.isArray(cv)){
cv=o[k]=[cv];
}
cv.push(v);
}else{
o[k]=v;
}
});
return o;
};
i$.xhr=function(_12,_13){
var _12=_12||"GET",_14=new i$.Promise(),url=_13.url||"",_15=_13.sync||false,cb=_13.callback||function(){
},_16=_13.responseType||"text",xhr=i$.getXHR();
var _17=function(){
if(xhr.readyState===4){
xhr.onreadystatechange=i$.isIE<8?new Function():null;
if(xhr.status>=400){
var err=new Error(xhr.status+": "+xhr.responseText);
try{
cb(err,xhr);
}
finally{
_14.reject({data:err,xhr:xhr});
}
}else{
try{
var ret="";
if(i$.xhrFmts[_16]){
ret=i$.xhrFmts[_16](xhr);
}
}
catch(err){
cb(err,xhr);
return;
}
try{
cb(ret,xhr);
}
finally{
_14.resolve({data:ret,xhr:xhr});
}
}
}
};
if(!_15){
xhr.onreadystatechange=_17;
}
xhr.open(_12,url,!_15);
i$.each(_13.headers,function(v,k){
xhr.setRequestHeader(k,v);
});
xhr.send();
if(_15){
_17();
}
return _14;
};
i$.each(["Get","Put","Post","Delete"],function(m){
i$["xhr"+m]=i$.partial(i$.xhr,m.toUpperCase());
});
i$.loadScript=function(_18){
var _19=document.getElementsByTagName("head")[0],_1a=document.createElement("script"),_1b=new i$.Promise(),_1c=false,_1d=function(_1e,_1f){
_1a.onreadystatechange=_1a.onload=null;
_1c=true;
_1b[_1e?"resolve":"reject"](_1f);
if(_18.callback){
_18.callback();
}
_19.removeChild(_1a);
_1a=null;
};
_1a.type="text/javascript";
_1a.onreadystatechange=function(){
if(this.readyState==="loaded"||this.readyState==="complete"){
_1d(true);
}
};
_1a.onload=function(){
_1d(true);
};
i$.each(_18.scriptAttrs,function(v,k){
if(v!=null){
_1a.setAttribute(k,v);
}
});
_1a.src=_18.url;
_19.appendChild(_1a);
if(_18.timeout){
setTimeout(function(){
if(!_1c){
_1d(false,new Error("Timeout exceeded"));
}
},_18.timeout);
}
return _1b;
};
})();
(function(){
if(typeof (JSON)!="undefined"&&JSON.parse){
i$.fromJson=function(str){
return JSON.parse(str);
};
i$.toJson=function(obj,_20){
return JSON.stringify(obj,null,_20?"\t":"");
};
}else{
i$.fromJson=function(str){
return eval(["(",str,")"].join(""));
};
var _21=function(str){
return ["\"",str.replace(/["]/g,"\\\"").replace(/[\\]/g,"\\\\").replace(/[\r]/g,"\\r").replace(/[\n]/g,"\\n").replace(/[\b]/g,"\\b").replace(/[\t]/g,"\\t").replace(/[\f]/g,"\\f"),"\""].join("");
},_22=function(obj,p,_23,_24){
var ap,_25;
if(_23){
_24=_24||"";
_25=_24+"\t";
}
if(obj===null){
p.push("null");
}else{
if(obj===undefined){
p.push("undefined");
}else{
if(i$.isBoolean(obj)||i$.isNumber(obj)){
p.push(obj);
}else{
if(i$.isString(obj)){
p.push(_21(obj));
}else{
if(i$.isFunction(obj.toJson)){
p.push(obj.toJson());
}else{
if(i$.isArray(obj)){
p.push("[");
ap=[];
i$.each(obj,function(el){
var _26=[];
_22(el,_26,_23,_25);
ap.push(_26.join(""));
});
if(ap.length>0){
if(_23){
p.push("\n"+_25);
}
p.push(ap.join(_23?",\n"+_25:","));
if(_23){
p.push("\n"+_24);
}
}
p.push("]");
}else{
if(i$.isObject(obj)){
p.push("{");
ap=[];
i$.each(obj,function(el,key){
var _27=[_21(key),": "];
_22(el,_27,_23,_25);
ap.push(_27.join(""));
});
if(ap.length>0){
if(_23){
p.push("\n"+_25);
}
p.push(ap.join(_23?",\n"+_25:","));
if(_23){
p.push("\n"+_24);
}
}
p.push("}");
}
}
}
}
}
}
}
};
i$.toJson=function(obj,_28){
var p=[];
_22(obj,p,_28);
return p.join("");
};
}
i$.xhrFmts.json=function(xhr){
return i$.fromJson(xhr.responseText);
};
})();
(function(){
var _29=function(){
this._evts={};
},_2a=function(_2b,_2c){
return _2b._evts[_2c]||(_2b._evts[_2c]={l:[],b:[]});
},add=function(_2d,_2e,_2f,fn){
var e=_2a(_2d,_2e),c=e[_2f].push(fn);
return [_2e,_2f,c-1];
},_30=function(_31,_32){
var e=_2a(_31,_32[0]);
delete e[_32[1]][_32[2]];
},_33=function(evt,_34,_35){
var _36=evt.b,_35=_35||0,b,r;
for(var i=_35;i<_36.length;i++){
b=_36[i];
if(b){
_34=typeof _34==="undefined"?[]:_34;
r=b.apply(null,_34||[]);
if(i$.promise.isPromise(r)){
return r.then(function(_37){
if(_37!==false){
return _33(evt,_34,i+1);
}
return _37;
});
}
}
}
},_38=function(evt,_39){
var _3a=evt.l,l;
for(var i=0;i<_3a.length;i++){
l=_3a[i];
if(l){
l.apply(null,_39||[]);
}
}
},_3b=function(_3c,_3d,_3e){
var e=_2a(_3c,_3d);
return i$.when(_33(e,_3e)).then(function(_3f){
if(_3f!==false){
_38(e,_3e);
}
return _3f;
});
};
i$.augment(_29,{addListener:function(_40,fn){
return add(this,_40,"l",fn);
},removeListener:function(_41){
return _30(this,_41);
},addBroker:function(_42,fn){
return add(this,_42,"b",fn);
},removeBroker:function(_43){
return _30(this,_43);
},fireEvent:function(_44,_45){
return _3b(this,_44,_45);
}});
var _46=new _29();
i$.each(["addListener","removeListener","addBroker","removeBroker","fireEvent"],function(n){
i$[n]=i$.scope(_46,n);
});
})();
(function(){
var _47=document.createElement("div");
i$.byId=function(id){
if(i$.isNode(id)){
return id;
}else{
return document.getElementById(id);
}
};
i$.createDom=function(_48,_49,_4a){
var el=document.createElement(_48);
i$.each(_49,function(v,k){
el.setAttribute(k,v);
});
if(_4a){
_4a.appendChild(el);
}
return el;
};
var _4b=_47.addEventListener?function(n){
return n.indexOf("on")==0?n.substr(2):n;
}:function(n){
return n.indexOf("on")!=0?"on"+n:n;
},add=_47.addEventListener?function(_4c,_4d,f){
_4c.addEventListener(_4d,f,false);
}:function(_4e,_4f,f){
_4e.attachEvent(_4f,f);
},_50=_47.removeEventListener?function(_51,_52,f){
_51.removeEventListener(_52,f,false);
}:function(_53,_54,f){
_53.detachEvent(_54,f);
};
i$.isDescendant=function(_55,anc){
if(anc){
while(_55){
if(_55==anc){
return true;
}
_55=_55.parentNode;
}
}
return false;
};
i$.bindDomEvt=function(_56,_57,f){
_57=_4b(_57);
if((_57=="mouseleave"||_57=="mouseenter")&&!i$.isIE){
var fp=f;
_57=_57=="mouseleave"?"mouseout":"mouseover";
f=function(e){
if(!i$.isDescendant(e.relatedTarget,_56)){
return fp.call(this,e);
}
};
}
add(_56,_57,f);
return [_56,_57,f];
};
i$.unbindDomEvt=function(_58){
if(_58[0]){
_50(_58[0],_58[1],_58[2]);
}
_58.splice(0,3);
};
if("classList" in _47){
i$.mash(i$,{addClass:function(_59,_5a){
_59&&_59.classList&&_59.classList.add(_5a);
},removeClass:function(_5b,_5c){
_5b&&_5b.classList&&_5b.classList.remove(_5c);
},hasClass:function(_5d,_5e){
return _5d&&_5d.classList&&_5d.classList.contains(_5e);
},toggleClass:function(_5f,_60){
_5f&&_5f.classList&&_5f.classList.toggle(_60);
}});
}else{
var _61=function(str,_62){
if(!str){
return -1;
}
var len=_62.length,i=str.indexOf(_62),_63,_64;
while(i>-1){
_64=str.charAt(i+len);
_63=str.charAt(i-1);
if((!_64||_64==" ")&&(!_63||_63==" ")){
break;
}
i=str.indexOf(_62,i+1);
}
return i;
};
i$.mash(i$,{addClass:function(_65,_66){
if(!_65){
return;
}
if(_61(_65.className,_66)<0){
_65.className+=" "+_66;
}
},removeClass:function(_67,_68){
if(!_67){
return;
}
var str=_67.className,len=_68.length,i=_61(str,_68),val=[];
if(i>-1){
if(i>0){
val.push(str.substring(0,i));
}
if(str.length>i+len){
val.push(str.substr(i+len));
}
_67.className=i$.trim(val.join());
}
},hasClass:function(_69,_6a){
if(!_69){
return;
}
return _61(_69.className,_6a)>-1;
},toggleClass:function(_6b,_6c){
if(!_6b){
return;
}
i$[i$.hasClass(_6b,_6c)?"removeClass":"addClass"](_6b,_6c);
}});
}
})();
(function(){
var _6d=/([^_]+)_([^_]+)_deferred_?([\d]+)?/,_6e=/alternate/i,_6f=function(t){
return document.getElementsByTagName(t);
},_70=function(){
return _6f("head")[0];
},_71=function(url){
i$.createDom("link",{rel:"stylesheet",type:"text/css",href:url},_70());
return i$.promise.resolved();
},_72=function(url){
return i$.loadScript({url:url});
},_73=function(url,_74,_75){
return i$.xhrGet({url:url,responseType:"text"}).then(function(_76){
var _77=document.createDocumentFragment(),tmp=i$.createDom("div");
tmp.innerHTML=_76.data;
while(tmp.firstChild){
_77.appendChild(tmp.firstChild);
}
var _78=i$.createDom("div",{"id":"wpthemeComplementaryContent","role":"region"});
_78.appendChild(_77);
_74.insertBefore(_78,_75);
});
},_79=function(_7a){
if(_6e.test(_7a.rel)){
var id=_7a.id,_7b=id.match(_6d);
if(_7b){
return {node:_7a,url:_7a.href,id:id,p:_7b[1],t:_7b[2],i:_7b[3]};
}
}
},_7c=function(a,b){
if(a.t!=b.t){
return a.t<b.t?-1:1;
}
return (a.i||0)-(b.i||0);
},_7d=function(){
var m={head:[],config:[]},_7e={},_7f=[],_80=_6f("link"),_81=_6f("a");
i$.each([_80,_81],function(_82){
i$.each(_82,function(_83){
var mod=_79(_83);
if(mod&&!_7e[mod.id]){
_7e[mod.id]=mod;
if(m[mod.p]){
m[mod.p].push(mod);
}
}
});
});
i$.each(m,function(_84){
_84.sort(_7c);
_7f.push.apply(_7f,_84);
});
return _7f;
},_85=function(_86){
var m=_86.shift(),p;
if(m){
switch(m.t){
case "css":
p=_71(m.url);
break;
case "js":
p=_72(m.url);
break;
case "markup":
p=_73(m.url,m.node.parentNode,m.p!="head"?m.node:null);
break;
}
}
return i$.when(p).then(function(){
return _86.length>0?_85(_86):true;
},function(err){
console.error(err);
});
},_87=false,_88=null,_89=false,_8a=false,_8b=new i$.Promise(),_8c=function(cbk){
i$.addOnLoad(function(){
if(!_87){
_88=_7d();
_89=_88.length>0?false:true;
if(_89){
_8b.resolve();
}
_87=true;
}
if(cbk){
cbk();
}
});
};
i$.modules={};
i$.mash(i$.modules,{areLoaded:function(){
return _89;
},areLoading:function(){
return _8a;
},loadDeferred:function(){
if(_8a){
return _8b;
}
var cbk=function(){
if(!_89){
_8a=true;
_85(_88).then(function(){
_89=true;
_8a=false;
_8b.resolve();
},function(e){
_8b.reject(e);
});
}
};
_8c(cbk);
return _8b;
},addAfterLoaded:function(f){
var cbk=function(){
_8b.then(f);
};
_8c(cbk);
}});
var _8d=i$.addOnLoad,_8e=[];
i$.addOnLoad=function(f,o){
if(_8a){
if(o){
f=i$.scope(o,f);
}
_8e.push(f);
}else{
_8d(f,o);
}
};
i$.modules.addAfterLoaded(function(){
while(_8e.length>0){
if(fn=_8e.shift()){
fn();
}
}
});
})();

