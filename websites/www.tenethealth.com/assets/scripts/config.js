/**
 * Application configuration declaration.
 *
 * This configuration file is shared between the website and the build script so
 * that values don't have to be duplicated across environments. Any non-shared,
 * environment-specific configuration should placed in appropriate configuration
 * files.
 *
 * Paths to vendor libraries may be added here to provide short aliases to
 * otherwise long and arbitrary paths. If you're using bower to manage vendor
 * scripts, running `grunt install` will automatically add paths aliases as
 * needed.
 *
 * @example
 *     paths: {
 *         'jquery': '../vendor/jquery/jquery',
 *         'jquery-cookie': '../vendor/jquery-cookie/jquery-cookie'
 *     }
 *
 * Shims provide a means of managing dependencies for non-modular, or non-AMD
 * scripts. For example, jQuery UI depends on jQuery, but it assumes jQuery is
 * available globally. Because RequireJS loads scripts asynchronously, jQuery
 * may or may not be available which will cause a runtime error. Shims solve
 * this problem.
 *
 * @example
 *     shim: {
 *         'jquery-cookie': {
 *             deps: ['jquery'],
 *             exports: null
 *          }
 *     }
 *
 */
require.config({
    paths: {
        angular: '../vendor/angular/angular',
        requirejs: '../vendor/requirejs/require',
        jquery: '../vendor/jquery/jquery.min',
        'jquery-touchswipe': '../vendor/jquery-touchswipe/jquery.touchSwipe',
        'jquery-debounce': '../vendor/jquery-debounce/jquery.debounce',
        richmarker: '../vendor/google/richmarker-compiled',
        markercluster: '../vendor/google/markerclusterer_compiled',
        locationmap: '../scripts/maps/locationmap',
        doTCompiler: '../vendor/doT/doT',
        text: '../vendor/requirejs-text/text',
        dot: '../vendor/requirejs-dot/doT',
        modernizr: '../vendor/modernizr/modernizr'
    },
    shim: {
        angular: {
            exports: 'angular',
            deps: ['jquery']
        },
        'jquery-touchswipe': {
            exports: 'swipe',
            deps: ['jquery']
        },
        'jquery-debounce': {
            deps: ['jquery']
        },
        locationmap: {
            exports: null,
            deps: [
                'jquery',
                'markercluster'
            ]
        },
        markercluster: {
            exports: 'MarkerClusterer'
        },
        richmarker: {
            exports: 'RichMarker'
        }
    },
    waitSeconds: 120
});
