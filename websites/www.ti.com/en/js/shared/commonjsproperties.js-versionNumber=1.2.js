if (!window.com) {
    com = new Object()
}
if (!com.TI) {
    com.TI = new Object()
}
if (!com.TI.promo) {
    com.TI.promo = new Object()
}

var promoIdCount = '';
var homePagePromoIdCount = '';

var PromoVariables = {
			"website"  : 'EN',
			"productCookie"  : "",
			"searchCookie"  : "",
			"techdocCookie"  : "",
			"productCookieLength"  : 2,
			"searchCookieLength"  : 2,
			"techdocCookieLength"  : 2,
			"productCookieName"  : 'AB_PRODUCT_EN',
			"searchCookieName"  : 'AB_SEARCH_EN',
			"techDocCookieName"  : 'AB_TECHDOC_EN',
			"preferenceCookieName"  : 'AB_PREFERENCE_EN',
			"promoTrackerCookieName"  : 'PROMO_TRACKER_EN',
			"bookMarks": {},
			"expandProductContent": "",
			"collapseProductContent": "",
			"description"  : "",
			"datasheet"  : "",
			"samples"  : "",
			"evm"  : "",
			"buy"  : "",
			"familyId"  : "",
			"software"  : "",
			"values"  : "",
			"product"  : "",
			"activityBarHtmlContent"  : "",
			"expand_label"  : 'Expand',
			"hide_history"  : 'Hide History',
			"my_products"  : 'My products',
			"no_products"  : 'No Products in your history',
			"my_techdocs"  : 'My technical documents',
			"no_techdocs"  : 'No documents in your history',
			"my_searches"  : 'My searches',
			"no_searches"  : 'No Searches in your history',
			"collapse_label"  : 'Collapse',
			"show_history"  : 'Show my history',
			"expandSearchContent"  : "",
			"collapseSearchContent"  : "",
			"expandTechdocContent"  : "",
			"collapseTechdocContent"  : "",
			"maxCookieValue"  : 10,
			"promoWsUrl"  : 'http://www.ti.com/promo/docs/promotions/getpromodetails.tsp',
			"linkId" : 'en_US',
			"products_pin_message" : 'You can pin up to 5 products',
			"techdocs_pin_message" : 'You can pin up to 5 technical documents',
			"searches_pin_message" : 'You can pin up to 5 searches',
			"pinned_label" : 'pinned',
			"pin_tool_tip" : 'Pin to keep this item on the list',
			"unpin_tool_tip" : 'Unpin this item',
			"delete_tool_tip" : 'Delete this item',
			"disabled_tool_tip" : 'You\’ve reached the limit of 5 pinned items',
			"pin_loading_msg" : 'Pinning item&nbsp;<img src="../../../graphics/shared/loading.gif"/*tpa=http://www.ti.com/graphics/shared/loading.gif*/ alt="" />',
			"unpin_loading_msg" : 'Unpinning item&nbsp;<img src="../../../graphics/shared/loading.gif"/*tpa=http://www.ti.com/graphics/shared/loading.gif*/ alt="" />',
			"trash_loading_msg" : 'Deleting item&nbsp;<img src="../../../graphics/shared/loading.gif"/*tpa=http://www.ti.com/graphics/shared/loading.gif*/ alt="" />',
			"pin_limit" : 5,
			"enable_pin_trash" : 'Y',
			"ipad_disabled_tool_tip" : 'You\’ve reached the limit of 5 pinned items. <span id="touch_tooltip_close" title="Close"></span> <div class="row btnWrapper"><a class="button primary" href="url">OK</a></div>',
			"ipad_trash_tool_tip" : 'Are you sure you want to delete this item? <span id="tool_tip_close" title="Close"></span> <div class="row btnWrapper"> <a id="tool_tip_cancel" class="button primary" href="url">Cancel</a> <a id="tool_tip_delete" class="button primary" href="url">Yes, delete</a></div>'
};

var NarrowSearch = {
			"Everything"  : '1',
			"Products"  : '2',
			"Technical documents"  : '3',			
			"Applications"  : '5',
			"E2E Forums"  : '6',
			"Blogs"  : '7',
			"Cross reference"  : '8',
			"Design network"  : '9',
			"Developer wiki"  : '10',
			"Training"  : '11',
			"Videos"  : '12',
			"Support"  : '13',
			"NarrowSearch_label"  : 'Narrow your search',
			"RecentSearch_label"  : 'Recent searches',
            "Search_label"  : "Search"			
};

var linkSearch = {
			"1"  : 'Everything',
			"2"  : 'Products',
			"3"  : 'Technical documents',			
			"5"  : 'Applications',
			"6"  : 'E2E Forums',
			"7"  : 'Blogs',
			"8"  : 'Cross reference',
			"9"  : 'Design network',
			"10"  : 'Developer wiki',
			"11"  : 'Training',
			"12"  : 'Videos',
			"13"  : 'Support'

};