var metricsPrefix = '';
var metricsVersion = '76';
var metricsHost = 'http://www.ti.com/assets/js/headerfooter/www.ti.com';
var metricsProtocol = location.protocol;
if (location.hostname.indexOf('http://www.ti.com/assets/js/headerfooter/-uat.itg.ti.com') != -1 ) {
	metricsPrefix = 'uat-';
	metricsHost = 'http://www.ti.com/assets/js/headerfooter/www-uat.itg.ti.com';
} else if (location.hostname.indexOf('http://www.ti.com/assets/js/headerfooter/-sat.itg.ti.com') != -1 ) {
	metricsPrefix = 'sat-';
	metricsHost = 'http://www.ti.com/assets/js/headerfooter/www-sat.itg.ti.com';
} else if (location.hostname.indexOf('http://www.ti.com/assets/js/headerfooter/-int.itg.ti.com') != -1 ) {
	metricsPrefix = 'int-';
	metricsHost = 'http://www.ti.com/assets/js/headerfooter/www-int.itg.ti.com';
}
document.write('<script language=\"JavaScript\" src=\''+metricsProtocol+'//'+metricsHost+'/assets/js/headerfooter/'+metricsPrefix+'metrics-min.js?version='+metricsVersion+'\' type=\"text/javascript\"></script>');