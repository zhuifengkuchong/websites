    var _isOpen = [],
        _keepFlyoutOpen = false,
        _lastNavClickId = null,
        closeStyledDropdown = function(e) {
           $("div.ti_tools_software_by_device_list .dropdown dd div").hide();
        },
        showFlyout = function (e) {
            //close all that are open
            var alreadyIn = false,
                o = _isOpen;
            for (var i = 0; i < _isOpen.length; i++) {
                if ($('#' + _isOpen[i]).attr('id') != $(this).attr('id')) {
                    $('http://www.ti.com/assets/en/js/homepage/div.sub', $('#' + _isOpen[i])).hide();
                    $('#' + _isOpen[i]).removeClass('active');
                } else {
                    alreadyIn = true;
                }
            }

            //resetset
            var id = $(this).attr('id'),
                t = $(this);

            if (!alreadyIn) {
                $('http://www.ti.com/assets/en/js/homepage/div.sub', $(this)).show(0, function () {
                    t.addClass('active');
                });
            }

            //reset
            _keepFlyoutOpen = false;
            _isOpen = [];
            _isOpen.push(id);
        },
        hideFlyout = function (e) {
            var id = $(this).attr('id'),
                parent = $(this);

            if (_keepFlyoutOpen) {
                return true;
            }

            //hide the panel
            $('http://www.ti.com/assets/en/js/homepage/div.sub', $(this)).hide(0, function () {
                parent.removeClass('active');
            });

            //lookup
            var i = jQuery.inArray(id, _isOpen);
            if (i >= 0) {
                _isOpen.remove(i);
            }

            //close drop down lists
            closeStyledDropdown(e);
        },
        openSubMenu = function() {
            $(this).find('.langbox').css('display', 'block');
        },
        closeSubMenu = function() {
            $(this).find('.langbox').css('display', 'none');
        },
        hIConfig = {
            over: showFlyout,
            timeout: 500,
            out: hideFlyout
        },
        _sliderResourceText = _sliderResourceText || { loading: '...Loading...'},
        _jqMegaTabsContent = $("div.megatabcontent", '#megatabs' );

    // Array Remove - By John Resig (MIT Licensed)
    Array.prototype.remove = function (from, to) {
        var rest = this.slice((to || from) + 1 || this.length);
        this.length = from < 0 ? this.length + from : from;
        return this.push.apply(this, rest);
    };

    //hookup mega tabs immediately
    // When a tab is clicked
    $("http://www.ti.com/assets/en/js/homepage/a.tab", '#megatabs').click(function (e) {
        //close dropdown list
        if (jQuery.isFunction(closeStyledDropdown)) {
            closeStyledDropdown(e);
        }

        // switch all tabs off
        $("a.active", '#megatabs').removeClass("active");

        // switch this tab on
        $(this).addClass("active");

        // slide all content up
        if(_jqMegaTabsContent == null || _jqMegaTabsContent.length == 0) {
            _jqMegaTabsContent = $("div.megatabcontent", '#megatabs' );
        }
        _jqMegaTabsContent.hide();

        // slide this content up
        var content_show = $(this).attr("alt");
        $("#" + content_show).show();
        return false;
    });

   //page ready
    $(function () {
        //enable inline field label on text boxes
        $('div.ti_samplebuy form.navProductSearchForm input.productNum').inlineFormLabel();

        try{
        //mod css rules to remove css mouse hover on nav menu items
        modCssRule('li.mega:hover .sub', function(rule) {
           rule.style.cssText = ''; //empty
        });
		}catch(e) {
			//alert(e);
		}

        //enable hover of menus
        $('http://www.ti.com/assets/en/js/homepage/li.mega', '#nav').hoverIntent(hIConfig);

        //enable close button on tabs
        $('.mega a.flyout-close', '#nav').click(function(e) {
            var p = $(this).data('closeparent');

            if(p) {
                p = $('#' + p);
                hideFlyout.call(p, e);
            }

            return false;
        });

        //iPad/mouse menu click
        $('http://www.ti.com/assets/en/js/homepage/a.nav', '#nav').click(function (e) {
            //close all that are open
            var id = $(this).parent().attr('id'),
                i = jQuery.inArray(id, _isOpen);

            if (i >= 0 && _lastNavClickId == id) {
                _lastNavClickId = null;
                hideFlyout.call($(this).parent(), e);
            } else {
                _lastNavClickId = id;
                showFlyout.call($(this).parent(), e);
            }

            return false;
        });

		// debugging
		function getURLParam(param) {
			var urlParams = (location.search) ? location.search.replace('?','').split('&') : [],
				params = (function() {
					var output = {};
					if (urlParams.length > 0) {
						for (var i=0;i<urlParams.length;i++) {
							var tmp = urlParams[i].split('=');
							output[tmp[0]] = tmp[1];
						}
					}
					return output;
				})();
			//console.log(params);
			return (params.hasOwnProperty(param)) ? params[param] : 0;
		}

		var	testParam = getURLParam('test'),
			badService = (testParam > 0),
			badDefault = (testParam > 1);
		
		function getLanguage(host) {
			var langMap = {
					"http://www.ti.com/assets/en/js/homepage/www.ti.com": 			"en",
					"http://www.ti.com/assets/en/js/homepage/www.ti.com.cn": 	"cn",
					"http://www.ti.com/assets/en/js/homepage/www.tij.co.jp": 	"jp"
				};	
			if (!langMap.hasOwnProperty(host)) return "en";
			return langMap[host];
		}

		function getRegion(host) {
			var regionMap = {
					"http://www.ti.com/assets/en/js/homepage/www.ti.com": 			"en_US",
					"http://www.ti.com/assets/en/js/homepage/www.ti.com.cn": 	"zh_CN",
					"http://www.ti.com/assets/en/js/homepage/www.tij.co.jp": 	"ja_JP"
				};			
			if (!regionMap.hasOwnProperty(host)) return "en_US";
			return regionMap[host];
		}

		var PROMOTYPE_OT = 1,
			PROMOTYPE_PP = 3;

		var regionCode = getLanguage(location.host),
			serviceRegion = (regionCode != 'en') ? regionCode + '/' : '',
			promoServiceUrl = (badService) ? 'blah' : "/promo/" + serviceRegion + "docs/promotions/getpromodetails.tsp?locationId=2",
			defaultDataUrl = (badDefault) ? 'blah' : "/assets/system-promos/gen/promos_" + regionCode + ".json";

		function useDefaultImages() {
			$.ajax({
				url: defaultDataUrl,
				type: "GET",
				dataType: 'json',
				cache: false
			})
			.done(function(data, textStatus, jqXHR) {
				//console.log('default data worked');			
				if (typeof data === 'string') data = $.parseJSON(data);
				parseStaticPromoData(data);
			})
			.fail(function(jqxhr, settings, exception) {
				//console.log('default data failed:' + exception);	
				initSliderWithFallbackData();
			});
		}

		function getFallbackData() {
			var items = [], 
				regionId = getRegion(location.host);

			switch (regionId) {
				case 'ja_JP':
					items.push({"promoId": '-1', "promoUrl": 'http://www.ti.com/pmp-webenchexport-bhp-en', "graphicPath": '../../../jp/images/homepage/Webench-SVA_HP-promo-jp-lg.jpg'/*tpa=http://www.ti.com/assets/jp/images/homepage/Webench-SVA_HP-promo-jp-lg.jpg*/, "thumbnailPath": '../../../jp/images/homepage/Webench-SVA_HP-promo-jp-sm.jpg'/*tpa=http://www.ti.com/assets/jp/images/homepage/Webench-SVA_HP-promo-jp-sm.jpg*/, "promoTypeId": 3});
					items.push({"promoId": '-2', "promoUrl": 'http://www.ti.com/pmp-webenchexport-bhp-en', "graphicPath": '../../../jp/images/homepage/16936_marcom_apps_lg_jp.jpg'/*tpa=http://www.ti.com/assets/jp/images/homepage/16936_marcom_apps_lg_jp.jpg*/, "thumbnailPath": '../../../jp/images/homepage/16936_marcom_apps_sm_jp.jpg'/*tpa=http://www.ti.com/assets/jp/images/homepage/16936_marcom_apps_sm_jp.jpg*/, "promoTypeId": 3});
					break;
				case 'zh_CN':
					items.push({"promoId": '-1', "promoUrl": 'http://www.ti.com/pmp-webenchexport-bhp-en', "graphicPath": '../../../cn/images/homepage/15683_webenchexport_lg-cn.jpg'/*tpa=http://www.ti.com/assets/cn/images/homepage/15683_webenchexport_lg-cn.jpg*/, "thumbnailPath": '../../../cn/images/homepage/15683_webenchexport_sm-cn.jpg'/*tpa=http://www.ti.com/assets/cn/images/homepage/15683_webenchexport_sm-cn.jpg*/, "promoTypeId": 3});
					items.push({"promoId": '-2', "promoUrl": 'http://www.ti.com/pmp-webenchexport-bhp-en', "graphicPath": '../../../cn/images/homepage/16832_Webench_lg_cn.jpg'/*tpa=http://www.ti.com/assets/cn/images/homepage/16832_Webench_lg_cn.jpg*/, "thumbnailPath": '../../../cn/images/homepage/16832_Webench_sm_cn.jpg'/*tpa=http://www.ti.com/assets/cn/images/homepage/16832_Webench_sm_cn.jpg*/, "promoTypeId": 3});
					break;
				default:
					items.push({"promoId": '-1', "promoUrl": 'http://www.ti.com/ccr2012-ticom', "graphicPath": '../../images/homepage/17379_ccr2012_hp_banner_lg_en.jpg'/*tpa=http://www.ti.com/assets/en/images/homepage/17379_ccr2012_hp_banner_lg_en.jpg*/, "thumbnailPath": '../../images/homepage/17379_ccr2012_hp_banner_sm_en.jpg'/*tpa=http://www.ti.com/assets/en/images/homepage/17379_ccr2012_hp_banner_sm_en.jpg*/, "promoTypeId": 3});
					items.push({"promoId": '-2', "promoUrl": 'http://www.ti.com/pmp-webenchexport-bhp-en', "graphicPath": '../../images/homepage/15683_webenchexport_lg_en.jpg'/*tpa=http://www.ti.com/assets/en/images/homepage/15683_webenchexport_lg_en.jpg*/, "thumbnailPath": '../../images/homepage/15683_webenchexport_sm_en.jpg'/*tpa=http://www.ti.com/assets/en/images/homepage/15683_webenchexport_sm_en.jpg*/, "promoTypeId": 3});
					items.push({"promoId": '-3', "promoUrl": 'http://www.ti.com/univ-blog-bhp-en', "graphicPath": '../../images/homepage/16335_univ_blog_lg_en.jpg'/*tpa=http://www.ti.com/assets/en/images/homepage/16335_univ_blog_lg_en.jpg*/, "thumbnailPath": '../../images/homepage/16335_univ_blog_sm_en.jpg'/*tpa=http://www.ti.com/assets/en/images/homepage/16335_univ_blog_sm_en.jpg*/, "promoTypeId": 3});
					items.push({"promoId": '-4', "promoUrl": 'http://www.ti.com/ti-mobile-bhp', "graphicPath": '../../images/homepage/16936_marcom_apps_lg_en.jpg'/*tpa=http://www.ti.com/assets/en/images/homepage/16936_marcom_apps_lg_en.jpg*/,  "thumbnailPath": '../../images/homepage/16936_marcom_apps_sm_en.jpg'/*tpa=http://www.ti.com/assets/en/images/homepage/16936_marcom_apps_sm_en.jpg*/, "promoTypeId": 3});
					break;
			}
			return items;
		}
		
		function initSliderWithFallbackData() {
			//console.log('using hardcoded fallback images');	
			initSlider(getPromosArray(getFallbackData()));
		}
		
		/**
		 * Used by useDefaultImages instead of parsePromoData because
		 * the static data is in a slightly different structure
		 */
		function parseStaticPromoData(result) {
			if (typeof result !== 'object') { 
				//console.log('result was not an object');
				return false;
			}
			
			var promos = [];
			// translate results into an array that we can sort
			for (var prop in result) {
				promos.push(result[prop]);
			}
			// sort
			// assuming sortOrder property is numeric
			// and that it actually exists for all items
			promos = promos.sort(function(a,b){
				return parseInt(a.sortOrder) - parseInt(b.sortOrder);
			});
			
			//console.log(promos);

			// add the items to the array
			initSlider(getPromosArray(promos));
			
			return true;
		}
		
		function parsePromoData(result) {
			if (typeof result !== 'object') { 
				//console.log('result was not an object');
				return false;
			}
			if (typeof result.FinalPromotionDetails !== 'object') { 
				//console.log('FinalPromotionDetails was not an object');
				return false;
			}
			if (typeof result.FinalPromotionDetails.personalizedPromoDetails !== 'object') { 
				//console.log('personalizedPromoDetails was not an object');
				return false;
			}
			if (result.FinalPromotionDetails.personalizedPromoDetails.length < 1) { 
				//console.log('personalizedPromoDetails had no items');
				return false;
			}

			var promos = result.FinalPromotionDetails.personalizedPromoDetails,
				isEmergency = (promos.length == 1 
					&& promos[0].hasOwnProperty('promoTypeId')
					&& promos[0].promoTypeId == PROMOTYPE_OT);

			// make sure that we at least have 3 items
			if ((promos.length < 3) && (!isEmergency)) { 
				var numToAdd = 3 - promos.length,
					items = getFallbackData();
				if (items.length < numToAdd) numToAdd = items.length;
				for (var i=0;i<numToAdd;i++) {
					promos.push(items[i]);
				}
			}
			
			//console.log(promos);
			initSlider(getPromosArray(promos, isEmergency), isEmergency);
			
			return true;
		}

		function getPromosArray(promos, isEmergency) {
			var tmp = [],
				i=0, len = promos.length,
				regionId = getRegion(location.host),
				langId =  getLanguage(location.host),
				promoListForMetrics = [];
			for (i=0;i<len;i++) {
				// validate that data is not null
				var promoId = promos[i].promoId,
					promoTrackId = (promos[i].promoTypeId == PROMOTYPE_PP) ? 'promo_hb_pp' : 'promo_hb_mm';
					
				if (isEmergency) promoTrackId = 'promo_hb_ot';
				promoTrackId += '_'+langId+'_'+promoId;

				var	promoUrl = promos[i].promoUrl,
					promoText = '&lid='+promoTrackId,
					graphicPath = promos[i].graphicPath,
					thumbnailPath = promos[i].thumbnailPath,
					impressionId = promoTrackId;
					promoType = promos[i].promoTypeId;
				if (promoUrl && promoText && graphicPath && thumbnailPath) {
					tmp.push(new SliderImages(
						promoId, 		// the unique id of the promo
						promoUrl,			// href
						promoText,		// linkId
						graphicPath,		// imgSrc
						thumbnailPath,		// thumbnailSrc
						impressionId,
						promoType
					));
				}
			}

			// TODO think of a better place to put this
			// update the global promo ID
			// if (!window.homePagePromoIDCount) window.homePagePromoIDCount = '';
			// homePagePromoIDCount = promoListForMetrics.join(':');
			// sethomePagePromoId(homePagePromoIDCount);
			// ClearVars();

			return tmp;
		}
		
		function initSlider(additionalImages, isEmergency) {
			//slider init
			var emergency = (typeof isEmergency != 'undefined') ? isEmergency : false;
			var options = {
				additionalImages: additionalImages,
				selector: '#slider',
				transitionTimeout: 20,
				loadingImageSrc: '../../images/homepage/loading.gif'/*tpa=http://www.ti.com/assets/en/images/homepage/loading.gif*/,
				isEmergency: emergency
			};
	
			$('#slider').tiSlider(options, _sliderResourceText);
		}
		
		// check for cookies
		if (typeof document.cookies !== 'string') {
			//console.log('we have cookies');
			// if cookies, try and get from service
				
			$.ajax({
				type: 'GET',
				timeout: 10000, //4000, changed for testsing
				url: promoServiceUrl,
				dataType: 'jsonp',
				contentType: "application/x-javascript",
				cache: false
			})
			.done(function(data, textStatus, jqXHR){
				// console.log('jsonp done:');
				var worked = parsePromoData(data);
				// if (!worked) console.log('parse failed, going to default');
				//if (!worked) alert('parse webservice data failed, going to default data'); // added for testsing
				if (!worked) useDefaultImages();
			})
			.fail(function(jqXHR, textStatus){
				// console.log('jsonp failed');
				//alert('webservice call failed, going to default data. status was ' + textStatus); // added for testsing
				useDefaultImages();
			})
			.always(function(data, textStatus, jqXHR){
				// if (textStatus == 'parsererror')
				// console.log('jsonp always: came back ' + textStatus);
				// var worked = parsePromoData(data);
				// if (!worked) useDefaultImages();
			});
		} else {
			//alert('no cookies, going to default data'); // added for testsing
			//console.log('no cookies');
			// no cookies, get the default json
			useDefaultImages();
		}
		
        //***
        var langRegion = getRegion(location.host);

        //Web bench panels init
        $('#flyout-products-webench').TIwebenchPanel({
            'width': 280,
            'height': 215,
            'lang': langRegion,
            'navBGColor': '#5e5e5e'
        });

        $('#flyout-tools-webench').TIwebenchPanel({
            'width': 280,
            'height': 215,
            'lang': langRegion,
            'navBGColor': '#5e5e5e'
        });

        $('#tab-products-webench').TIwebenchPanel({
            'width': 280,
            'height': 215,
            'lang': langRegion,
            'navBGColor': '#5e5e5e'
        });

        $('#tab-tools-webench').TIwebenchPanel({
            'width': 280,
            'height': 215,
            'lang': langRegion,
            'navBGColor': '#5e5e5e'
        });

        // language selector below //
        $('li#language').hover(openSubMenu, closeSubMenu);
        //***

        //check browser
        if ($.browser.msie && parseInt($.browser.version.substr(0, 1)) < 8) {
            // rule to remove outlines in IE 6 and IE 7
            $("a").each(function () {
                $(this).attr("hideFocus", "true").css("outline", "none");
            });
        }
    });