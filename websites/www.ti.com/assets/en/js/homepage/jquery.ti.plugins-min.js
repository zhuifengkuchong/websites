(function ($) {
	jQuery.fn.inlineFormLabel = function () {
		if (jQuery(this).length == 0) {
			return jQuery(this)
		}
		var f = jQuery(this).parents('form'),
		labelText = jQuery.trim(jQuery('label.' + jQuery(this).attr('id'), f).html());
		if (jQuery(this).val().length == 0) {
			jQuery(this).val(labelText)
		}
		jQuery(this).focus(function () {
			jQuery(this).prev().addClass("focus");
			if (jQuery(this).val() == labelText) {
				jQuery(this).val('')
			}
		});
		jQuery(this).keypress(function () {
			jQuery(this).prev().addClass("has-text").removeClass("focus")
		});
		jQuery(this).blur(function () {
			if (jQuery(this).val() == "") {
				jQuery(this).val(labelText).removeClass("has-text").removeClass("focus")
			}
		});
		return jQuery(this)
	},
	jQuery.fn.styledDropdownList = function (appendTo, cssClass, sourceUpdateCallback, useAnchorTag) {
		if (!appendTo) {
			throw new Error('Missing append to DOM element')
		}
		if (!jQuery.isFunction(sourceUpdateCallback)) {
			sourceUpdateCallback = function () {}
			
		}
		var sourceList = jQuery(this),
		options = (useAnchorTag) ? jQuery('a', sourceList) : jQuery('option', sourceList),
		selected = (useAnchorTag) ? [] : jQuery(options).find(":selected"),
		randNum = (Math.random() * 1000).toFixed(),
		styledListId = ((useAnchorTag) ? 'dropdownlist' : sourceList.attr('id')) + '-' + randNum;
		cssClass = cssClass || '';
		function getAnchor(url) {
			if (!url) {
				return ''
			}
			if (url[0] == '#') {
				return url
			}
			var i = url.indexOf('#');
			return url.substr(i)
		}
		jQuery(appendTo).append('<dl id="' + styledListId + '" class="' + cssClass + ' dropdown"></dl>');
		if (jQuery(selected).length == 0) {
			selected = jQuery(options)[0]
		}
		jQuery("#" + styledListId).append('<dt><a href="#' + styledListId + '">' + jQuery(selected).text() + '<span class="value">' + jQuery(selected).val() + '</span></a></dt>');
		var optionsCssClassName = 'options';
		if (typeof appendTo == 'string' && appendTo[0] == '#') {
			optionsCssClassName += ' ' + appendTo.substr(1)
		}
		jQuery("#" + styledListId).append('<dd><div id="' + styledListId + '-options" class="' + optionsCssClassName + '"><ul></ul><div></dd>');
		options.each(function () {
			jQuery("dd ul", '#' + styledListId).append('<li><a href="' + ((useAnchorTag) ? jQuery(this).attr('href') : '#' + styledListId) + '">' + jQuery(this).text() + '<span class="value">' + ((useAnchorTag) ? jQuery(this).attr('href') : jQuery(this).val()) + '</span></a></li>')
		});
		jQuery("dt a", '#' + styledListId).click(function () {
			var selector = getAnchor(jQuery(this).attr('href')) + "-options",
			offset = jQuery(this).offset(),
			h = jQuery(this).outerHeight(true) || 0;
			jQuery(selector).css({
				'top' : (offset.top + h) + 'px',
				'left' : offset.left + 'px'
			}).toggle();
			return false
		});
		jQuery("ul li a", '#' + styledListId + "-options").click(function () {
			var html = jQuery(this).html(),
			t = jQuery(this),
			selector = getAnchor(jQuery(this).attr('href'));
			jQuery("dt a", selector).html(html);
			jQuery(selector + "-options").hide();
			sourceUpdateCallback.call(t, jQuery(selector), html);
			return false
		});
		return jQuery('#' + styledListId + "-options")
	},
	jQuery.fn.showAboveAll = function (additionalCss, appendTo, cssElement, offsetElement) {
		if (jQuery(this).length == 0) {
			return jQuery(this)
		}
		var left,
		top,
		t = offsetElement || this,
		a = appendTo || 'body',
		c = cssElement || this;
		left = jQuery(t).offset().left;
		top = jQuery(t).offset().top;
		jQuery(this).appendTo(a);
		jQuery(c).css({
			'position' : 'absolute',
			'left' : left + 'px',
			'top' : top + 'px'
		});
		if (additionalCss) {
			jQuery(c).css(additionalCss)
		}
		return jQuery(this)
	},
	jQuery.fn.tiSlider = function (options, resourceText) {
		this.advanceCarousel = function (dir) {
			dir = (typeof dir == 'undefined') ? 1 : dir;
			if ((dir > 0)) {
				if ((currentThumbnailIndex + 1) < this.options.thumbnailViewportCount) {
					currentThumbnailIndex++
				} else {
					var startPos = this.indexOf(thumbnailViewModel[this.options.thumbnailViewportCount - 1]);
					buildThumbnailViewModel(startPos + 1, createThumbnails, dir)
				}
			} else if (dir < 0) {
				if ((currentThumbnailIndex - 1) >= 0) {
					currentThumbnailIndex--
				} else {
					var startPos = this.indexOf(thumbnailViewModel[0]) - this.options.thumbnailViewportCount;
					if (startPos < 0) {
						startPos += imgCount
					}
					buildThumbnailViewModel(startPos, createThumbnails, dir)
				}
			}
			var imgObj = thumbnailViewModel[currentThumbnailIndex];
			showLargerImage(imgObj)
		};
		this.indexOf = function (imgObj) {
			if (!imgObj) {
				return -1
			}
			var len = imagesCount();
			for (var i = 0; i < len; i++) {
				if (imgObj.promoId == _this.options.additionalImages[i].promoId) {
					return i
				}
			}
			return -1
		};
		this.options = {
			selector : '#tiSlider',
			additionalImages : [],
			existingPromoLink : null,
			existingPromoImage : null,
			transitionTimeout : 20,
			fadeTransitionSpeed : 500,
			startPosition : 0,
			controlPrevHtml : '&laquo;',
			controlNextHtml : '&raquo;',
			thumbnailViewportCount : 3,
			imageLoaderDivId : 'slider-images-loader',
			thumbnailNavCssClass : 'thumbNav',
			loadingImageSrc : 'Unknown_83_filename'/*tpa=http://www.ti.com/assets/en/js/homepage/images/loading.gif*/,
			isEmergency : false
		};
		options = options || {};
		resourceText = resourceText || {};
		this.options = jQuery.extend(true, {}, this.options, options);
		if (options && options.existingPromoImage) {
			var sImage = new SliderImages();
			sImage.imgSrc = $(options.existingPromoImage).attr('src');
			sImage.thumbnailSrc = $(options.existingPromoImage).data('thumbnail');
			sImage.linkName = $(options.existingPromoLink).attr('name');
			sImage.href = $(options.existingPromoLink).attr('href');
			this.options.additionalImages.splice(0, 0, sImage)
		}
		var _this = this,
		thumbnailViewModel = [],
		additionalImages = this.options.additionalImages,
		imagesCount = function () {
			return (_this.options.additionalImages || []).length
		},
		currentThumbnailIndex = 0,
		jqSelector = $(this.options.selector),
		imageLoadingIntervalHandler = null,
		thumbnailWindowIntervalTimerHandler = null,
		inMouseOverEvent = false,
		impressionsSentFor = [],
		resourceTextObject = {
			loading : 'Loading...'
		},
		buildThumbnailViewModel = function (startPos, updateUICallback, dir) {
			var imgCount = imagesCount(),
			endPos = thumbnailViewportSize,
			supplementalAdditions = [],
			thumbnailViewportSize = (_this.options.thumbnailViewportCount < imgCount) ? _this.options.thumbnailViewportCount : imgCount;
			dir = dir || 1;
			startPos = typeof startPos == 'undefined' ? 0 : startPos;
			if (dir > 0) {
				if (currentThumbnailIndex + 1 >= thumbnailViewportSize) {
					currentThumbnailIndex = 0;
					thumbnailViewModel = []
				}
			} else if (dir < 0) {
				if (currentThumbnailIndex - 1 < 0) {
					currentThumbnailIndex = thumbnailViewportSize - 1;
					thumbnailViewModel = []
				}
			}
			if (thumbnailViewModel.length == 0) {
				startPos = (startPos >= imgCount) ? 0 : startPos;
				startPos = (startPos < 0) ? 0 : startPos;
				if ((startPos + thumbnailViewportSize) >= imgCount) {
					endPos = imgCount;
					var o = (startPos + thumbnailViewportSize) - imgCount;
					for (var i = 0; i < o; i++) {
						supplementalAdditions.push(_this.options.additionalImages[i])
					}
				} else {
					endPos = (startPos + thumbnailViewportSize)
				}
				for (var i = startPos; i < endPos; i++) {
					thumbnailViewModel.push(_this.options.additionalImages[i])
				}
				thumbnailViewModel = thumbnailViewModel.concat(supplementalAdditions);
				try {
					if (updateUICallback && $.isFunction(updateUICallback)) {
						updateUICallback.call(this, thumbnailViewModel)
					}
				} catch (e) {}
				
			}
		},
		createThumbnails = function (model, addHighlight) {
			var thumbnails = $('.thumbnails', jqSelector).html('');
			for (var i = 0; i < model.length; i++) {
				thumbnails.append('<a href="#' + model[i].href + '" class="' + ((currentThumbnailIndex == i && addHighlight === true) ? 'thumb-highlight' : '') + '" name="' + model[i].linkName + '"><img src="' + (model[i].thumbnailSrc || model[i].imgSrc) + '" alt="" /></a>')
			}
		},
		showLargerImage = function (imgObj) {
			var img = imgObj || thumbnailViewModel[currentThumbnailIndex],
			largerImage = $('.slider-promo', jqSelector),
			loadingDiv = $('.slider-promo-loading', jqSelector),
			thumbLinks = $('.thumbnails a', jqSelector);
			thumbLinks.removeClass('thumb-highlight');
			if (img.loaded) {
				window.clearInterval(imageLoadingIntervalHandler);
				loadingDiv.hide();
				largerImage.fadeOut(_this.options.fadeTransitionSpeed, function () {
					$(this).find('a').attr({
						'href' : img.href,
						'name' : img.linkName
					}).find('img').attr('src', img.imgSrc);
					$(this).fadeIn(_this.options.fadeTransitionSpeed, function () {
						if (thumbLinks.length > 0 && (currentThumbnailIndex < thumbLinks.length)) {
							$(thumbLinks[currentThumbnailIndex]).addClass('thumb-highlight')
						}
					})
				});
				sendImpressionMetric(img);
			} else {
				if (thumbnailWindowIntervalTimerHandler != null) {
					pauseCarousel()
				}
				$(largerImage).hide();
				loadingDiv.show();
				imageLoadingIntervalHandler = setInterval(function () {
						var image = thumbnailViewModel[currentThumbnailIndex];
						if (image.loaded) {
							window.clearInterval(imageLoadingIntervalHandler);
							startCarousel();
							showLargerImage()
						}
					}, 200)
			}
		},
		loadImages = function (src, sliderImageObject, onloadEvent) {
			var img = document.createElement("img");
			img.onreadystatechange = function () {
				onloadEvent.call(img, sliderImageObject)
			};
			img.onload = function () {
				onloadEvent.call(img, sliderImageObject)
			};
			img.src = src;
			return img
		},
		startCarousel = function () {
			inMouseOverEvent = false;
			thumbnailWindowIntervalTimerHandler = window.setInterval(function () {
					_this.advanceCarousel()
				}, _this.options.transitionTimeout * 1000);
		},
		pauseCarousel = function () {
			try {
				window.clearInterval(thumbnailWindowIntervalTimerHandler);
				thumbnailWindowIntervalTimerHandler = null
			} catch (e) {}
		},
		sendImpressionMetric = function (img) {
			// track the impression (but only once)
			// and we only send impressions for personal promos (type = 3)
			if ((img.impressionId) && (img.promoType) && (img.promoType == 3) && ($.inArray(img.impressionId,impressionsSentFor) == -1)) {
				//sethomePagePromoId(img.impressionId);
				setTimeout(function() {sethomePagePromoId(img.impressionId)},4000);
				//ClearVars();
				impressionsSentFor.push(img.impressionId);
			}
		};
		resourceTextObject = $.extend(true, {}, resourceTextObject, resourceText);
		var jqImageLoader = $('<div id="' + this.options.imageLoaderDivId + '" style="display: none"></div>'),
		imgCount = imagesCount();
		for (var i = 0; i < imgCount; i++) {
			if (additionalImages[i].thumbnailSrc && additionalImages[i].thumbnailSrc.length > 0) {
				jqImageLoader.append(loadImages(additionalImages[i].thumbnailSrc, additionalImages[i], function (lazyLoadImgReference) {}))
			}
			if (additionalImages[i].imgSrc && additionalImages[i].imgSrc.length > 0) {
				jqImageLoader.append(loadImages(additionalImages[i].imgSrc, additionalImages[i], function (lazyLoadImgReference) {
						if (lazyLoadImgReference) {
							lazyLoadImgReference.loaded = true
						}
					}))
			}
		}
		var startingImg = this.options.additionalImages[this.options.startPosition];
		jqSelector.html('').append('<div class="slider-promo-loading"><img src="' + this.options.loadingImageSrc + '" alt="" /><span>' + resourceTextObject.loading + '</span></div><div class="slider-promo"><a href="' + startingImg.href + '" name="' + startingImg.linkName + '"><img src="' + startingImg.imgSrc + '" alt="" /></a></div><div class="control-prev">' + this.options.controlPrevHtml + '</div><div class="control-next">' + this.options.controlNextHtml + '</div>').append('<div class="thumbnail-container"><div class="thumbnails ' + this.options.thumbnailNavCssClass + '"></div></div>');

		// showLargerImage not called for first image, need send impression metrics manually 
		sendImpressionMetric(startingImg);

		if (this.options.isEmergency) {
			$('.control-next', jqSelector).hide();
			$('.control-prev', jqSelector).hide();
			$('.thumbnail-container', jqSelector).hide();
		} else {
			$(jqSelector).hover(function () {
				inMouseOverEvent = true;
				pauseCarousel()
			}, function () {
				startCarousel()
			});
			$('.thumbnails', jqSelector).on('click', 'a', function (e) {
				var t = $(this),
				href = t.attr('href'),
				name = t.attr('name'),
				elementIndex = t.parent().children().index(this),
				imgObj = thumbnailViewModel[elementIndex];
				if (imgObj) {
					showLargerImage(imgObj);
					currentThumbnailIndex = elementIndex
				}
				return false
			});
			$('.control-next', jqSelector).click(function () {
				_this.advanceCarousel(1)
			});
			$('.control-prev', jqSelector).click(function () {
				_this.advanceCarousel(-1)
			});
			buildThumbnailViewModel(this.options.startPosition, function (model) {
				createThumbnails(model, true)
			});
			startCarousel();
		}
	}
})(jQuery);
function SliderImages(promoId, href, linkName, imgSrc, thumbnailSrc, impressionId, promoType) {
	this.promoId = promoId;
	this.href = href;
	this.linkName = linkName;
	this.imgSrc = imgSrc;
	this.thumbnailSrc = thumbnailSrc;
	if (impressionId) this.impressionId = impressionId;
	if (promoType) this.promoType = promoType;
	this.loaded = false
}
var modCssRule = function (regExpression, foundCallback) {
	if (!document.styleSheets) {
		return
	}
	for (var c = 0; c < document.styleSheets.length; c++) {
		var thecss = new Array();
		if (document.styleSheets[c].cssRules) {
			thecss = document.styleSheets[c].cssRules
		} else {
			thecss = document.styleSheets[c].rules
		}
		for (var i = 0; i < thecss.length; i++) {
			if (thecss[i].selectorText.match(regExpression, 'gi')) {
				if (foundCallback) {
					foundCallback.call(this, thecss[i])
				}
			}
		}
	}
};