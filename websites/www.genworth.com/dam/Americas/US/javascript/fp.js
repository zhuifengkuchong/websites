var TMX_FLAG = 0;
$(document).ready(function() {
    var UNIQUE_SESSION_ID=$.cookie("GNW");     
    if ( (TMX_FLAG<1) && (UNIQUE_SESSION_ID != null) ) {
        UNIQUE_SESSION_ID=UNIQUE_SESSION_ID.split(".",5)[4];
        $('body').append('<script src="https://imgcdn.genworth.com/fp/tags.js?org_id=mwdbqph8&amp;session_id='+UNIQUE_SESSION_ID+'"></script>');
		
		// This URL is subject to change for PRODUCTION
		var ASSESS_URL = "https://www.genworth.com/assess";
		$("input[name=username]").focusout(function(){	
			$.post(ASSESS_URL,'{"username":"' + $(this).val()+'","session_id":"'+UNIQUE_SESSION_ID+'"}');
		});
    }
    TMX_FLAG++;
});