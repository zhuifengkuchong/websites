/* ===================================================
 * bootstrap-transition.js v2.3.2
 * http://getbootstrap.com/2.3.2/javascript.html#transitions
 * ===================================================
 * Copyright 2013 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================== */


!function ($) {

  "use strict"; // jshint ;_;


  /* CSS TRANSITION SUPPORT (http://www.modernizr.com/)
   * ======================================================= */

  $(function () {

    $.support.transition = (function () {

      var transitionEnd = (function () {

        var el = document.createElement('bootstrap')
          , transEndEventNames = {
               'WebkitTransition' : 'webkitTransitionEnd'
            ,  'MozTransition'    : 'transitionend'
            ,  'OTransition'      : 'oTransitionEnd otransitionend'
            ,  'transition'       : 'transitionend'
            }
          , name

        for (name in transEndEventNames){
          if (el.style[name] !== undefined) {
            return transEndEventNames[name]
          }
        }

      }())

      return transitionEnd && {
        end: transitionEnd
      }

    })()

  })

}(window.jQuery);
/* ==========================================================
 * bootstrap-carousel.js v2.3.2
 * http://getbootstrap.com/2.3.2/javascript.html#carousel
 * ==========================================================
 * Copyright 2013 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================== */


!function ($) {

  "use strict"; // jshint ;_;


 /* CAROUSEL CLASS DEFINITION
  * ========================= */

  var Carousel = function (element, options) {
    this.$element = $(element)
    this.$indicators = this.$element.find('.carousel-indicators')
    this.options = options
    this.options.pause == 'hover' && this.$element
      .on('mouseenter', $.proxy(this.pause, this))
      .on('mouseleave', $.proxy(this.cycle, this))
  }

  Carousel.prototype = {

    cycle: function (e) {
      if (!e) this.paused = false
      if (this.interval) clearInterval(this.interval);
      this.options.interval
        && !this.paused
        && (this.interval = setInterval($.proxy(this.next, this), this.options.interval))
      return this
    }

  , getActiveIndex: function () {
      this.$active = this.$element.find('.item.active')
      this.$items = this.$active.parent().children()
      return this.$items.index(this.$active)
    }

  , to: function (pos) {
      var activeIndex = this.getActiveIndex()
        , that = this

      if (pos > (this.$items.length - 1) || pos < 0) return

      if (this.sliding) {
        return this.$element.one('slid', function () {
          that.to(pos)
        })
      }

      if (activeIndex == pos) {
        return this.pause().cycle()
      }

      return this.slide(pos > activeIndex ? 'next' : 'prev', $(this.$items[pos]))
    }

  , pause: function (e) {
      if (!e) this.paused = true
      if (this.$element.find('.next, .prev').length && $.support.transition.end) {
        this.$element.trigger($.support.transition.end)
        this.cycle(true)
      }
      clearInterval(this.interval)
      this.interval = null
      return this
    }

  , next: function () {
      if (this.sliding) return
      return this.slide('next')
    }

  , prev: function () {
      if (this.sliding) return
      return this.slide('prev')
    }

  , slide: function (type, next) {
      var $active = this.$element.find('.item.active')
        , $next = next || $active[type]()
        , isCycling = this.interval
        , direction = type == 'next' ? 'left' : 'right'
        , fallback  = type == 'next' ? 'first' : 'last'
        , that = this
        , e

      this.sliding = true

      isCycling && this.pause()

      $next = $next.length ? $next : this.$element.find('.item')[fallback]()

      e = $.Event('slide', {
        relatedTarget: $next[0]
      , direction: direction
      })

      if ($next.hasClass('active')) return

      if (this.$indicators.length) {
        this.$indicators.find('.active').removeClass('active')
        this.$element.one('slid', function () {
          var $nextIndicator = $(that.$indicators.children()[that.getActiveIndex()])
          $nextIndicator && $nextIndicator.addClass('active')
        })
      }

      if ($.support.transition && this.$element.hasClass('slide')) {
        this.$element.trigger(e)
        if (e.isDefaultPrevented()) return
        $next.addClass(type)
        $next[0].offsetWidth // force reflow
        $active.addClass(direction)
        $next.addClass(direction)
        this.$element.one($.support.transition.end, function () {
          $next.removeClass([type, direction].join(' ')).addClass('active')
          $active.removeClass(['active', direction].join(' '))
          that.sliding = false
          setTimeout(function () { that.$element.trigger('slid') }, 0)
        })
      } else {
        this.$element.trigger(e)
        if (e.isDefaultPrevented()) return
        $active.removeClass('active')
        $next.addClass('active')
        this.sliding = false
        this.$element.trigger('slid')
      }

      isCycling && this.cycle()

      return this
    }

  }


 /* CAROUSEL PLUGIN DEFINITION
  * ========================== */

  var old = $.fn.carousel

  $.fn.carousel = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('carousel')
        , options = $.extend({}, $.fn.carousel.defaults, typeof option == 'object' && option)
        , action = typeof option == 'string' ? option : options.slide
      if (!data) $this.data('carousel', (data = new Carousel(this, options)))
      if (typeof option == 'number') data.to(option)
      else if (action) data[action]()
      else if (options.interval) data.pause().cycle()
    })
  }

  $.fn.carousel.defaults = {
    interval: 5000
  , pause: 'hover'
  }

  $.fn.carousel.Constructor = Carousel


 /* CAROUSEL NO CONFLICT
  * ==================== */

  $.fn.carousel.noConflict = function () {
    $.fn.carousel = old
    return this
  }

 /* CAROUSEL DATA-API
  * ================= */

  $(document).on('click.carousel.data-api', '[data-slide], [data-slide-to]', function (e) {
    var $this = $(this), href
      , $target = $($this.attr('data-target') || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '')) //strip for ie7
      , options = $.extend({}, $target.data(), $this.data())
      , slideIndex

    $target.carousel(options)

    if (slideIndex = $this.attr('data-slide-to')) {
      $target.data('carousel').pause().to(slideIndex).cycle()
    }

    e.preventDefault()
  })

}(window.jQuery);
var Brand={
    CATEGORIES:[
        "OTC_LTC_Leadform_test1","1",
        "OTC_Brand_FaceBook_test1","2",
        "OTC_COC_COCmap_test1","3",
        "OTC_Brand_Ellen_test1","2",
        "OTC_Pro_test1","4",
        "OTC_Annuity_test1","5"
    ]
};  // default as testing cid:slide
var CarouselCampaign = {
    //constants
    T: false,
    ID: '',
    ITEMS: '1',
    ACTIVE_SLIDE: '1',
    SLIDER_PARAM: 'opt',
    CID_PARAM: 'cid',
    CATEGORIES:["OTC_LTC_Leadform_test1","1","OTC_Brand_FaceBook_test1","2","OTC_COC_COCmap_test1","3","OTC_Brand_Ellen_test1","2"],   // default as testing cid:slide, used external
    store: CQ_Analytics.ProfileDataMgr,
    init: function(){
        $(CarouselCampaign.ID+' .item.'+CarouselCampaign.ACTIVE_SLIDE).addClass('active');
    },   
    getID: function(){
        return CarouselCampaign.ID;
    },
    setID: function(id){
        CarouselCampaign.ID = id;
    },
    setCategories: function(str){
        if(CarouselCampaign.utils.isOK(str)){
            CarouselCampaign.CATEGORIES = str.split(",");
        }console.log(CarouselCampaign.CATEGORIES);
    },
    setItems: function(n){
        if(CarouselCampaign.utils.isOK(n)){
            CarouselCampaign.ITEMS = n;
        }else{
            CarouselCampaign.ITEMS = "1";
        }
    },
    setActiveItem: function(n){
        if(CarouselCampaign.utils.isOK(n)&&n>0&&n<=CarouselCampaign.ITEMS){
            CarouselCampaign.ACTIVE_SLIDE = n;
        }else{
            CarouselCampaign.utils.log("Default slide used.  Invalid or Out of Range for ["+n+"]");
        }
    },
    setAutoTrans: function(time){
        if(CarouselCampaign.utils.isOK(time)){
            //var t = Number(time);
            if(time>0) CarouselCampaign.T = time*1000 | 0;
        }
        $(CarouselCampaign.ID).carousel({interval: CarouselCampaign.T});
    },
    getAutoTrans: function(){
        return CarouselCampaign.T;
    },
    tracking: function(mess){
        setEVar("Links", mess);
    },
    hasParameter: function(param){
        param = param.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + param + "=([^&#]*)"),
        results = regex.exec(location.search);
        if(results!=null){
            var value = decodeURIComponent(results[1].replace(/\+/g, " "));
            if(CarouselCampaign.utils.isOK(value)){
                if(isNaN(value)){// not slide number
                    var valStr = ""+value;
                    //var arr = CarouselCampaign.CATEGORIES;
                    var arr = Brand.CATEGORIES;
                    for (var i = 0, len = arr.length; i < len; i++){
                        if (valStr.toLowerCase()==arr[i].toLowerCase()){  
                            value = arr[i+1];
                        }
                    }
                     CarouselCampaign.utils.log("by Brand Name: ["+valStr+"]");
                }
                // set with valid value checked.
                CarouselCampaign.setActiveItem(value);
                return true;
            }
        }
        return false;
    },
    Cookies:{
        CURRENT_BRAND_PROP: 'cBrand', 
        set: function(val){
            CarouselCampaign.store.setProperty(CarouselCampaign.Cookies.CURRENT_BRAND_PROP, val);
        },
        get: function(){
            return CarouselCampaign.store.getProperty(CarouselCampaign.Cookies.CURRENT_BRAND_PROP);
        },
        remove: function(){
            CarouselCampaign.store.removeProperty(CarouselCampaign.Cookies.CURRENT_BRAND_PROP);
        },
        check: function(){
            var c = CarouselCampaign.store.getProperty(CarouselCampaign.Cookies.CURRENT_BRAND_PROP);
            if(CarouselCampaign.utils.isOK(c)){
                CarouselCampaign.ACTIVE_SLIDE = c;
                return true;
            }
            return false;
        }
    },
    utils:{
        isOK: function(val){
            return (val != null && typeof val !== 'undefined' && val != '');
        },
        log: function(message){
            if(console && console.log){
                console.log(message);
            }
        }
    }
};

/**  =============================================================================
 *   Carousel JS Constructor 
 *   @CarouselCampaign: An Object to hold all functionalies to drive the Carousel
 *   =============================================================================
 *   WHEN auto transition X (>0) seconds filled, then carousel transitions automatically based user input.
 *   OTHERWISE: Carousel has to be manually transitioned through carousel controls and carousel interval is set to FALSE.
 *
 *   Set all carouselID, Timer, Number of Items, and default Active Slide from the Dialog.  The ID can be empty, but the rest 
 *   will be validated to put default value if input is invalid or out of range.
 *
 *   The cookie will be set once the slider option parameter passed from the URL.  Then the cookied item will be used.
 *   If no cookie set, the default slide will be active.
 *
 *   Omniture tagging for button set in the Dialog. 
 *   ============================================================================== */
$(document).ready(function(){
    CarouselCampaign.setID($('.carousel-control').attr("href"));
    CarouselCampaign.setAutoTrans($(CarouselCampaign.getID()).attr("timer"));
    CarouselCampaign.setItems($(CarouselCampaign.getID()).attr("items"));
    CarouselCampaign.setActiveItem($(CarouselCampaign.getID()).attr("activeItem"));
    //CarouselCampaign.setCategories($('#jsStr').attr("categories"));

    if(CarouselCampaign.hasParameter(CarouselCampaign.SLIDER_PARAM)||CarouselCampaign.hasParameter(CarouselCampaign.CID_PARAM)){
        CarouselCampaign.Cookies.set(CarouselCampaign.ACTIVE_SLIDE); // cookies it
        CarouselCampaign.utils.log("slide number passed: ["+CarouselCampaign.ACTIVE_SLIDE+"]");
    }else if(CarouselCampaign.Cookies.check()){// if cookie found, set it active
        CarouselCampaign.utils.log("Brand Cookie Found: ["+CarouselCampaign.ACTIVE_SLIDE+"]");
    }
    // use default if not above
    CarouselCampaign.init();
        
    // trigger tagging for Carousel buttons ONLY
    $(CarouselCampaign.getID()+' .btn').click(function(){
        CarouselCampaign.tracking($(this).closest('.carousel-cta').attr("e5Tag"));
    });
});
// Gets number of carousel slides
function getNumSlides(){
    return $('.carousel').attr('items');
}

/**
 * Loops through all slides in the carousel and attaches click event to slides set as clickable
 */
function slideIsClickable(){
    var num = getNumSlides();
    for( i = 1; i <= num; i++ ){
        var slideItem = '.item.' + i;
        if ($(slideItem + ' .carousel-img').hasClass('carousel-slide-clickable')){
          $(slideItem).addClass('carousel-clickable-wrapper');
        }
     }
    
    $('.carousel-clickable-wrapper').click(function(){
        var btnUrl = $(this).find('.btn').attr('href');
        var newWin = $(this).find('.carousel-slide-clickable').attr('data-pagetarget');
        var tag  = $(this).find('.carousel-cta').attr("e5Tag");
        var fireTag = ((tag == '')||(tag == null)) ? false : true;
        
        if (fireTag) { setEVar("Links", tag); }
        if(newWin){ window.open(btnUrl); }
        else{ window.location.href = btnUrl; }
    });
}

$(document).ready(function(){
    // Find and set all slides desired to be clickable
    slideIsClickable();
});
