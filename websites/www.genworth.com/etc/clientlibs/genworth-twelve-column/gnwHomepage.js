$(document).ready(function(){
    
    var HAS_SM_ERROR = $('#smauthFail').length ;
    
    // Omniture: Tag on failed SM authentication
    //------------------------------------------
    if ( HAS_SM_ERROR ){
        setEVar('Links','gnw:Home_Consumer Login Failed');
    }
    
    // Form Validation
    // Validates any combo of blank fields and provides user friendly error messaging
    //
    // If IE8 then do custom validation otherwise run jquery validate. TODO, remove I8 specific code when IE8 is dropped
    // -----------------------------------------------------------------------------------------------------------------
    if(IS_IE8){
        $('#genhomeloginSubmit').click(function( event ) { // submit() binding is not supported
            if( ie8FormValidate() ){
                setEVar("Consumer Logins","gnw:Home_Consumer Login");
                return;           
            }
            event.preventDefault();
        });
    }else{
        $('#genhomeloginForm').validate({
            rules: {
                username: { required: true },
                password: { required: true }
            },
            messages: {
                username: 'Please enter your username.',
                password: 'Please enter your password.'
            },
            errorElement: 'p',
            errorClass: 'has-error',
            onfocusout: false,
            onkeyup: false,
            onclick: false,
            showErrors: function(errorMap, errorList) {
                if(this.numberOfInvalids() > 1){
                    $("#genhomeloginError").html('Please enter your username and password.');
                    this.defaultShowErrors();
                    $('#genhomeloginForm p.has-error').hide();
                }else{
                    $("#genhomeloginError").html('');
                    this.defaultShowErrors();
                    if( HAS_SM_ERROR ){ $('#genhomeloginSubmit').click(); }
                }
    
                // Hides the Siteminder message if present
                if( HAS_SM_ERROR ){ $('#smauthFail').hide(); }
            },
            submitHandler: function(form) {
                setEVar("Consumer Logins","gnw:Home_Consumer Login"); 
                form.submit();
            }
        });
    }
    
    
    // Hit enter key
    // --------------
    $('#genhomeloginBadge *').keypress(function(e){
        if(e.which === 13){
            $('#genhomeloginSubmit').focus().click();
        }
    });
});


// IE 8 Functions
// IE8 is not supported by jquery validate plugin
// TODO: remove when IE8 is no longer supported
// ----------------------------------------------
var IS_IE8=false;
if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)) { //test for MSIE x.x;
    var ieversion=new Number(RegExp.$1) // capture x.x portion and store as a number
    if (ieversion>=8){ IS_IE8=true; }
}


function ie8FormValidate() {
    
    var $user = $('#username');
    var $pass = $('#password');
    var valid = false;
    var errClass = 'has-error';
    
    if ( isNULLorEmpty( $user.val() ) ) {
        $user.addClass(errClass);
    }else{
        $user.removeClass(errClass).siblings('.has-error').remove();
        valid = true;
    }

    if ( isNULLorEmpty( $pass.val() ) ) {
        $pass.addClass(errClass);
        valid= false;
    }else{
        $pass.removeClass(errClass).siblings('.has-error').remove();
    }

    if(!valid){
        showErrors($user,$pass,errClass);
        return false;
    }else{
        return true;
    }  
}

function isNULLorEmpty(val) {
    return (val==null) || (val=='') ?true:false;
}

function showErrors(obj1,obj2,_errClass) {
    var obj1HasError = obj1.hasClass(_errClass);
    var obj2HasError = obj2.hasClass(_errClass);

    if ( obj1HasError && obj2HasError ) {
        $('#genhomeloginError').html('Please enter your username and password.');
        obj1.siblings('.has-error').remove();
        obj2.siblings('.has-error').remove();
    }
    else{
        $('#genhomeloginError').html('');
        
        if (obj1HasError) {
            obj1.siblings('.has-error').remove();
            $('<p class="has-error">Please enter your username.</p>').insertAfter( obj1 );
        }
        else{ 
            obj2.siblings('.has-error').remove();
            $('<p class="has-error">Please enter your password.</p>').insertAfter( obj2 ); }
    }
    
    return false;
}
