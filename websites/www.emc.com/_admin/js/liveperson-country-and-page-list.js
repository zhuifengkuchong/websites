// DO NOT MODIFY THIS LINE-- Set up EMC LP Object and default settings 
if (typeof(emclp)=='undefined') { var emclp={}; }

/* BEGIN COUNTRY SETUP */
// only allow LP code to execute on desired subdomains (countries)
// set country code for use setting lang later, and set useLP to true so LP code gets run
// to enable it everywhere except prod live, set "isLive" to false
// then set "lang" as instructed by Harry Li

emclp.domainList={
    
    www: { isLive: true, useLP: true, isEstore: false, skill: 'english', translation: 'en_US', divisions: ['rsa','iig'] },
    
    canada: { isLive: true, useLP: true, isEstore: false, skill: 'english', translation: 'en_US', divisions: ['iig'] },
    uk: { isLive: true, useLP: true, isEstore: false, skill: 'uk-english', translation: 'en_US', divisions: [] },
    ireland: { isLive: true, useLP: true, isEstore: false, skill: 'uk-english', translation: 'en_US', divisions: [] },
    australia: { isLive: true, useLP: true, isEstore: false, skill: 'australia-english', translation: 'en_US', divisions: [] },
    newzealand: { isLive: true, useLP: true, isEstore: false, skill: 'australia-english', translation: 'en_US', divisions: [] },
    france: { isLive: true, useLP: true, isEstore: false, skill: 'france-french', translation: 'fr_FR', divisions: [] },
    germany: { isLive: true, useLP: true, isEstore: false, skill: 'germany-german', translation: 'de_DE', divisions: [] },
    middleeast: { isLive: true, useLP: true, isEstore: false, skill: 'uk-english', translation: 'en_US', divisions: [] },
    southafrica: { isLive: true, useLP: true, isEstore: false, skill: 'southafrica-english', translation: 'en_US', divisions: [] },
    singapore: { isLive: true, useLP: true, isEstore: false, skill: 'singapore-english', translation: 'en_US', divisions: [] },
    india: { isLive: true, useLP: true, isEstore: false, skill: 'india-english', translation: 'en_US', divisions: [] },
    italy: { isLive: true, useLP: true, isEstore: false, skill: 'italy-italian', translation: 'it_IT', divisions: [] },
    china: { isLive: true, useLP: true, isEstore: false, skill: 'china-chinese', translation: 'zh_CN', divisions: [] },
    brazil: { isLive: true, useLP: true, isEstore: false, skill: 'brazil-portuguese', translation: 'pt_BR', divisions: [] },
    mexico: { isLive: true, useLP: true, isEstore: false, skill: 'mexico-spanish', translation: 'es_MX', divisions: [] },
    spain: { isLive: true, useLP: true, isEstore: false, skill: 'spain-spanish', translation: 'es_ES', divisions: [] },
    portugal: { isLive: true, useLP: true, isEstore: false, skill: 'portugal-portuguese', translation: 'pt_PT', divisions: [] },
    austria: { isLive: true, useLP: true, isEstore: false, skill: 'germany-german', translation: 'de_DE', divisions: [] },
    switzerland: { isLive: true, useLP: true, isEstore: false, skill: 'germany-german', translation: 'de_DE', divisions: [] },
    suisse: { isLive: true, useLP: true, isEstore: false, skill: 'france-french', translation: 'fr_FR', divisions: [] },
    netherlands: { isLive: true, useLP: true, isEstore: false, skill: 'uk-english', translation: 'en_US', divisions: [] },
    belgium: { isLive: true, useLP: true, isEstore: false, skill: 'uk-english', translation: 'en_US', divisions: [] },
    sweden: { isLive: true, useLP: true, isEstore: false, skill: 'uk-english', translation: 'en_US', divisions: [] },
    poland: { isLive: true, useLP: true, isEstore: false, skill: 'poland-polish', translation: 'pl_PL', divisions: [] },
    argentina: { isLive: true, useLP: true, isEstore: false, skill: 'mexico-spanish', translation: 'es_MX', divisions: [] },
    chile: { isLive: true, useLP: true, isEstore: false, skill: 'mexico-spanish', translation: 'es_MX', divisions: [] },
    colombia: { isLive: true, useLP: true, isEstore: false, skill: 'mexico-spanish', translation: 'es_MX', divisions: [] },
    peru: { isLive: true, useLP: true, isEstore: false, skill: 'mexico-spanish', translation: 'es_MX', divisions: [] },
    puertorico: { isLive: true, useLP: true, isEstore: false, skill: 'mexico-spanish', translation: 'es_MX', divisions: [] },
    venezuela: { isLive: true, useLP: true, isEstore: false, skill: 'mexico-spanish', translation: 'es_MX', divisions: [] },
    russia: { isLive: true, useLP: true, isEstore: false, skill: 'russia-russian', translation: 'ru_RU', divisions: [] },
    afrique: { isLive: true, useLP: true, isEstore: false, skill: 'france-french', translation: 'fr_FR', divisions: [] },
    czech: { isLive: true, useLP: true, isEstore: false, skill: 'czech-czech', translation: 'cs_CZ', divisions: [] },
    turkey: { isLive: true, useLP: true, isEstore: false, skill: 'turkey-turkish', translation: 'tr_TR', divisions: [] },
    africa: { isLive: true, useLP: true, isEstore: false, skill: 'uk-english', translation: 'en_US', divisions: [] },
    saudi: { isLive: true, useLP: true, isEstore: false, skill: 'uk-english', translation: 'en_US', divisions: [] },
    see: { isLive: true, useLP: true, isEstore: false, skill: 'uk-english', translation: 'en_US', divisions: [] },

    //japan: { isLive: true, useLP: true, isEstore: false, skill: 'japan-japanese', translation: 'ja_JP', divisions: [] },
    //korea: { isLive: true, useLP: true, isEstore: false, skill: 'korea-korean', translation: 'ko_KR', divisions: [] },

    // ecn
    community: { isLive: true, useLP: true, isEstore: false, skill: 'english', translation: 'en_US', divisions: [] },
    ecndev: { isLive: false, useLP: true, isEstore: false, skill: 'english', translation: 'en_US', divisions: [] },
    ecnstaging: { isLive: false, useLP: true, isEstore: false, skill: 'english', translation: 'en_US', divisions: [] },
    
    // estore
    estoredevadmin: { isLive: false, useLP: true, isEstore: true, skill: 'store-english', translation: 'en_US', divisions: [] },
    estoretstadmin: { isLive: false, useLP: true, isEstore: true, skill: 'store-english', translation: 'en_US', divisions: [] },
    estorestgadmin: { isLive: false, useLP: true, isEstore: true, skill: 'store-english', translation: 'en_US', divisions: [] },
    estoreadmin: { isLive: true, useLP: true, isEstore: true, skill: 'store-english', translation: 'en_US', divisions: [] },
    estorestg: { isLive: false, useLP: true, isEstore: true, skill: 'store-english', translation: 'en_US', divisions: [] },
    estoretst: { isLive: false, useLP: true, isEstore: true, skill: 'store-english', translation: 'en_US', divisions: [] },
    estoredev: { isLive: false, useLP: true, isEstore: true, skill: 'store-english', translation: 'en_US', divisions: [] },
    estore: { isLive: true, useLP: true, isEstore: true, skill: 'store-english', translation: 'en_US', divisions: [] },
    storestg: { isLive: false, useLP: true, isEstore: true, skill: 'store-english', translation: 'en_US', divisions: [] },
    storetst: { isLive: false, useLP: true, isEstore: true, skill: 'store-english', translation: 'en_US', divisions: [] },
    storedev: { isLive: false, useLP: true, isEstore: true, skill: 'store-english', translation: 'en_US', divisions: [] },
    store: { isLive: true, useLP: true, isEstore: true, skill: 'store-english', translation: 'en_US', divisions: [] },
    ebzappdev01: { isLive: false, useLP: true, isEstore: true, skill: 'store-english', translation: 'en_US', divisions: [] },
    ebzappdev02: { isLive: false, useLP: true, isEstore: true, skill: 'store-english', translation: 'en_US', divisions: [] },
    
    test: { isLive: false, useLP: true, isEstore: false, skill: 'english', translation: 'en_US', divisions: ['iig','rsa'] },
    emc2: { isLive: false, useLP: true, isEstore: false, skill: 'english', translation: 'en_US', divisions: ['iig','rsa'] },
    
    'default': { isLive: false, useLP: false, isEstore: false, skill: 'english', translation: 'en_US', divisions: ['iig','rsa'] }

};

// list of URL patterns for determining division and pro/re state
emclp.livePersonDirectoryList={ 
    divisions: {
        iig: [
            // iig division pages URL patterns
            '/case-management/',
            '/content-management/',
            '/enterprise-content-management/',
            // iig testing
            '/iig-test'
        ],
        rsa: [
            // rsa division pages URL patterns
            '/security/',
            '/support/rsa/',
            // rsa testing
            '/rsa-test',
	    '/rsa/',
	    '/rsa-'
        ]
    },
    proactive: [
        // directories forced to Proactive chat
        '/cloud/hybrid-cloud-computing/',
        '/storage/',
        '/backup-and-recovery/',
        '/cloud-virtualization/',
        '/big-data/',
        '/security/',
        '/enterprise-content-management/',
        '/data-center-management/',
        // industries URL patterns
        '/industry/',
        '/solutions/industry/',
        // platforms URL patterns
        '/platform/',
        '/storage/',
        // testing
        '/pro-test'
    ],
    reactive: [
        // directories forced to Reactive chat
        '/campaign/',
        '/campaigns/',
	'/security/',
	'/support/rsa/',
	'/rsa/',
	'/rsa-'
    ]
};



///////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////// END EDITABLE URL PATTERNS AND COUNTRY LISTS ///////////////////////////////////////////
/////////////////// DO NOT EDIT PAST THIS POINT! //////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////


// big int/base conversion support
var bigNum={add:function(x,y,base){var z=[];var n=Math.max(x.length,y.length);var carry=0;var i=0;while(i<n||carry){var xi=i<x.length?x[i]:0;var yi=i<y.length?y[i]:0;var zi=carry+xi+yi;z.push(zi%base);carry=Math.floor(zi/base);i++;}
return z;},multiplyByNumber:function(num,x,base){if(num<0)return null;if(num==0)return[];var result=[];var power=x;while(true){if(num&1){result=bigNum.add(result,power,base);}
num=num>>1;if(num===0)break;power=bigNum.add(power,power,base);}
return result;},parseToDigitsArray:function(str,base){var digits=str.split('');var ary=[];for(var i=digits.length-1;i>=0;i--){var n=parseInt(digits[i],base);if(isNaN(n))return null;ary.push(n);}
return ary;},convertBase:function(str,fromBase,toBase){var digits=bigNum.parseToDigitsArray(str,fromBase);if(digits===null)return null;var outArray=[];var power=[1];for(var i=0;i<digits.length;i++){if(digits[i]){outArray=bigNum.add(outArray,bigNum.multiplyByNumber(digits[i],power,toBase),toBase);}
power=bigNum.multiplyByNumber(fromBase,power,toBase);}
var out='';for(var i=outArray.length-1;i>=0;i--){out+=outArray[i].toString(toBase);}
return out;},decToHex:function(decStr){var hex=bigNum.convertBase(decStr,10,16);return hex?'0x'+hex:null;},hexToDec:function(hexStr){if(hexStr.substring(0,2)==='0x')hexStr=hexStr.substring(2);hexStr=hexStr.toLowerCase();return bigNum.convertBase(hexStr,16,10);}};


//get URL variables
var GETS={}; var hadGets=false; var sGet=window.location.search; if (sGet) { hadGets=true; processGETS(sGet); } function processGETS(sGet) { sGet=sGet.substr(1); var sNVPairs=sGet.split('&'); for (var i=0;i<sNVPairs.length;i++) { var sNV=sNVPairs[i].split('='); GETS[sNV[0]]=sNV[1]; } }

// vars for legacy support
var livePersonSkill; var livePersonLanguage; var useLP=false;

emclp.useLP=false;
emclp.livePersonLanguage='english';
emclp.simulatedURL=false;
emclp.livePersonIsEstore=false;
emclp.livePersonDebugOutput='Debug output from LivePerson Config:'; // will be added to later

if (typeof(GETS['simulatedurl'])!='undefined') {
    emclp.simulatedURL=GETS['simulatedurl'];
    subDomain=emclp.simulatedURL.slice(emclp.simulatedURL.indexOf('//')+2).split('.emc.com').join('').split('.emc.dev').join('').split('.sequelcommunications.com').join('');
} else {
    subDomain=window.location.hostname.split('.emc.com').join('').split('.emc.dev').join('').split('.sequelcommunications.com').join('');
}
subDomain=subDomain.split('-').join('');

if (subDomain.indexOf('/')>=0) { subDomain=subDomain.slice(0,subDomain.indexOf('/')); }
if (subDomain.indexOf(':')>=0) { subDomain=subDomain.slice(0,subDomain.indexOf(':')); }
if (subDomain.indexOf('.')>=0) { subDomain=subDomain.slice(0,subDomain.indexOf('.')); }

emclp.livePersonDebugOutput+='\nSubdomain: '+subDomain;
emclp.livePersonSkill='emc-sales-english';

emclp.checkForOverrides=function(valName,val) {
    // check for presence of emclpoverrides variable in the page and the presence of a value for the setting we're looking for, and return it if found, otherwise return initial value
    if (typeof(emclp.overrides)!='undefined') {
        if (typeof(emclp.overrides[valName])!='undefined') {
            return emclp.overrides[valName];
        } else {
            return val;
        }
    } else if ($j('.chat-override').length>0) {
	var temp='notsetyet';
	$j('.chat-override').each(function(ix,el) {
	    if ($j(el).attr('data-parameter')==valName) {
		temp=$j(el).attr('data-value');
	    }
	});
	if (temp!='notsetyet') {
	    return temp;
	} else {
	    return val;
	}
    } else {
        return val;
    }
};

emclp.overridesInUse=function() {
    if (typeof(emclp.overrides)!='undefined') {
        var temp='';
        for (var i in emclp.overrides) {
            temp+=i+':'+emclp.overrides[i]+', ';
        }
        return temp.slice(0,-2);
    } else {
        return 'none';
    }
};

emclp.getChatType=function() {
    var ct='RE'; // default
    // check directory list for initial setting
    for (var i in emclp.livePersonDirectoryList.proactive) {
        if (emclp.livePersonPageURL.indexOf(emclp.livePersonDirectoryList.proactive[i])==0) {
            ct='PRO';
        }
    }
    for (var i in emclp.livePersonDirectoryList.reactive) {
        if (emclp.livePersonPageURL.indexOf(emclp.livePersonDirectoryList.reactive[i])==0) {
            ct='RE';
        }
    }
    // check for variables set in the page to override this
    ct=emclp.checkForOverrides('chatType',ct);
    // check for adwords-enabled location and return RE for those, return either default or the override value from previous step for others
    return ct;
};

emclp.getDivision=function() {
    var dv='emc';
    // get division from lists if this is still needed
    for (var i in emclp.livePersonDirectoryList.divisions) {
        for (var k in emclp.livePersonDirectoryList.divisions[i]) {
            if (emclp.livePersonPageURL.indexOf(emclp.livePersonDirectoryList.divisions[i][k])>=0) {
                dv=i;
            }
        }
    }
    // check for variables set in the page to override this
    dv=emclp.checkForOverrides('division',dv);
    emclp.division=dv;
    return dv;
};

emclp.getDivisionForInsertion=function(dashBefore) {
    return ((emclp.getDivision()=='emc')?'':(dashBefore?'-':'')+emclp.getDivision()+(dashBefore?'':'-'));
};


emclp.getChatTypeValueFromString=function(inStr) {
    switch (inStr) {
        case 'OFF' : return 0;
        case 'RE' : return 2;
        default : return 1;
    }
};

emclp.getDomainValues=function(inSubDomain) {
    var tempVals=emclp.domainList['default'];
    var isLiveServer=false;
    
    inSubDomain=inSubDomain.split('-').join('').split('.').join('').toLowerCase();
    
    // if subdomain is found in list without modifiction, this is a live site and we should use
    // the value stored in "isLive" for "useLP". Otherwise we're not live and we should use the value in "useLP"
    
    if (typeof(emclp.domainList[inSubDomain])!='undefined') {
	tempVals=emclp.domainList[inSubDomain];
	isLiveServer=tempVals.isLive;
    } else {
	var temp=inSubDomain;
	for (var e=0;e<envsWithoutSides.length;e++) {
	    temp=temp.replace(envsWithoutSides[e],'');
	}
	for (var e=0;e<envsWithSides.length;e++) {
	    for (var si=0;si<sides.length;si++) {
		var thisDomain=envsWithSides[e].split('[]').join(sides[si]);
		temp=temp.replace(thisDomain,'');
	    }
	}
	if (temp.length<2) {
	    temp='www'; // we're on a US site
	}
	var tempVals2=emclp.domainList[temp];
	if (typeof(tempVals2)!='undefined') {
	    tempVals=tempVals2;
	}
	isLiveServer=false;
    }
    
    if (!emclp.simulatedURL && window.location.hostname.indexOf('.dev')>=0) {
	isLiveServer=false;
    }
    
    // set LP account info based on server
    if (!isLiveServer) {
	// dev account
	//emclp.lpserver='http://www.emc.com/_admin/js/dev.liveperson.net';
	//emclp.lpnumber='P5047164';
	//emclp.appkey='0a259e5db07b459bb67b993a02c4d59e';
	//emclp.livePersonDebugOutput+='\nLP Account: Dev (';
	// emc clone account
	emclp.lpserver='http://www.emc.com/_admin/js/sales.liveperson.net';
	emclp.lpnumber='60189845';
	emclp.appkey='13bcdcd2c24143ceb011aea0a24487e8';
	emclp.livePersonDebugOutput+='\nLP Account: Clone (';
    } else {
	// emc live account
	emclp.lpserver='http://www.emc.com/_admin/js/sales.liveperson.net';
	emclp.lpnumber='67761027';
	emclp.appkey='2483dffc679545b28cc525577e26772f';
	emclp.livePersonDebugOutput+='\nLP Account: Live (';
    }
    
    // do not edit this last part
    return {
	useLP: isLiveServer?tempVals.isLive:tempVals.useLP,
	livePersonLanguage: tempVals.skill,
	livePersonIsEstore: tempVals.isEstore,
        translation: tempVals.translation,
	divisions: tempVals.divisions
    };

};

// get page URL from root of site
if (emclp.simulatedURL) {
    var temp=emclp.simulatedURL.replace('http://','').replace('https://','');
    emclp.livePersonPageURL=temp;
    emclp.livePersonHostname=temp.slice(0,temp.indexOf('/'));
} else {    
    emclp.livePersonPageURL=window.location.href.replace('http://','').replace('https://','');
    emclp.livePersonHostname=window.location.hostname;
}

emclp.livePersonPageURL=emclp.livePersonPageURL.slice(emclp.livePersonPageURL.indexOf('/'));
if (emclp.livePersonPageURL.indexOf('?')>=0) {
    emclp.livePersonPageURL=emclp.livePersonPageURL.slice(0,emclp.livePersonPageURL.indexOf('?'));
}
if (emclp.livePersonPageURL.indexOf('#')>=0) {
    emclp.livePersonPageURL=emclp.livePersonPageURL.slice(0,emclp.livePersonPageURL.indexOf('#'));
}


emclp.pageValues=emclp.getDomainValues(subDomain);

emclp.livePersonDebugOutput+=emclp.lpnumber+') '+emclp.lpserver;

emclp.useLP=useLP=emclp.pageValues.useLP;
emclp.livePersonLanguage=livePersonLanguage=emclp.pageValues.livePersonLanguage;
emclp.livePersonLanguage=emclp.checkForOverrides('language',emclp.livePersonLanguage);
emclp.livePersonSkill=livePersonSkill='emc-sales-'+emclp.pageValues.livePersonLanguage;
emclp.livePersonIsEstore=livePersonIsEstore=emclp.pageValues.livePersonIsEstore;
emclp.livePersonTranslation=emclp.pageValues.translation;

emclp.livePersonPageList=[]; // empty now that all pages are enabled, still here just in case something references it

emclp.copyObj=function(inObj) {
    outObj={};
    for (o in inObj) {
         outObj[o]=inObj[o];
    }
    return outObj;
};

emclp.readCookie=function(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i=0;i < ca.length;i++) {
	var c = ca[i];
	while (c.charAt(0)==' ') c = c.substring(1,c.length);
	if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
};
emclp.createCookie=function(name,value,days) {
	//console.log('create cookie '+name+' '+value+' '+days);
	var expires='';
	if (days!=undefined) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		expires = '';//"; expires="+date.toGMTString();
	}
	document.cookie = name+"="+value+expires+"; path=/";
};
emclp.eraseCookie=function(name) {
     createCookie(name,"",-1);
};

// add Array.indexOf method to IE<=8
if(!Array.prototype.indexOf){Array.prototype.indexOf=function(searchElement){"use strict";if(this==null){throw new TypeError();} var t=Object(this);var len=t.length>>>0;if(len===0){return-1;} var n=0;if(arguments.length>0){n=Number(arguments[1]);if(n!=n){n=0;}else if(n!=0&&n!=Infinity&&n!=-Infinity){n=(n>0||-1)*Math.floor(Math.abs(n));}} if(n>=len){return-1;} var k=n>=0?n:Math.max(len-Math.abs(n),0);for(;k<len;k++){if(k in t&&t[k]===searchElement){return k;}} return-1;}}




function livePersonAlertDebugOutput() {
    alert(emclp.livePersonDebugOutput+'\n________________________________________________________');
}

emclp.livePersonFinishSetup=function() {

    // move eStore variables for backwards-compatibility
    emclp.livePersonIsEstore=(typeof(livePersonIsEstore)=='undefined')?false:livePersonIsEstore;
    emclp.eStoreChat=(typeof(eStoreChat)=='undefined')?false:eStoreChat;

    // debug output
    emclp.livePersonDebugOutput+='\nLanguage:  '+emclp.livePersonLanguage;
    emclp.livePersonDebugOutput+='\nPage URL:  '+emclp.livePersonHostname+emclp.livePersonPageURL + (emclp.simulatedURL?' (SIMULATED URL)':'');
    emclp.livePersonDebugOutput+='\nPage-specific overrides in use: '+emclp.overridesInUse();
 
    lpMTagConfig.lpServer = emclp.lpserver;
    lpMTagConfig.lpNumber = emclp.lpnumber;

    lpMTagConfig.lpTagSrv = lpMTagConfig.lpServer;
    lpMTagConfig.deploymentID = "emc-sales";
    
    lpMTagConfig.vars.push(["page","unit","emc-sales"]);
    lpMTagConfig.vars.push(["session","pageurl",emclp.livePersonHostname+emclp.livePersonPageURL.split('http://').join('').split('https://').join('')]);
    
    //emclp.createCookie('s_vi','[CS]v1|29B0962D051D1D05-6000013720000048[CE]',-1);
    // tracking pass-through
    if (emclp.readCookie('s_vi')!=null) {
	var temp=emclp.readCookie('s_vi');
	temp=temp.slice(7,-4);
	temp=temp.split('-');
	if (temp.length==2) {
            temp=bigNum.convertBase(temp[0],16,10)+'_'+bigNum.convertBase(temp[1],16,10);
	    lpMTagConfig.vars.push(['session','OmnitureVisitorID',temp]);
	}
    }
    if (emclp.readCookie('emc_MUP_ID_persist')!=null) {
	lpMTagConfig.vars.push(['session','MupID',emclp.readCookie('emc_MUP_ID_persist')]);
    }
    if (GETS['am_id']!=undefined) {
	lpMTagConfig.vars.push(['session','AudienceMemID',GETS['am_id']]);
    } else if (emclp.readCookie('emc_AM_ID_persist')!=null) {
	lpMTagConfig.vars.push(['session','AudienceMemID',emclp.readCookie('emc_AM_ID_persist')]);
    }
        
    // override language and unit variables for some implementations, add custom variables to others
    if (emclp.getDivision()!='emc') {
     
	// SPECIAL CASES
	// force all skills containing "english" in the name to the main "english" iig skill group by resetting the language value
	if (emclp.livePersonLanguage.indexOf('english')>=0) {
	    emclp.livePersonLanguage='english';
	}

        if (emclp.pageValues.divisions.indexOf(emclp.getDivision())<0) {
            emclp.livePersonDebugOutput+='\n'+emclp.getDivision().toUpperCase()+' division in unsupported language, removing chat';
            $j(document).ready(function() { $j('.inboundBar .ib-icon-sales-chat').remove(); });
        }
        
    } else if (emclp.livePersonIsEstore) {
    
	if (typeof(storeSessionId)!='undefined') {
	    lpMTagConfig.vars.push(["session","SessionID",storeSessionId]);
	}
        
    }

    lpMTagConfig.vars.push(["session","language",emclp.getDivisionForInsertion()+emclp.livePersonLanguage]);
    emclp.livePersonSkill='emc-sales-'+emclp.getDivisionForInsertion()+emclp.livePersonLanguage;
    
    lpMTagConfig.vars.push(["page","PageName",document.title]);
    lpMTagConfig.vars.push(["page","Section",emclp.livePersonPageURL.split('http://').join('').split('https://').join('').split('/')[1]]);

    // custom variable ChatEnabled -- determines whether the page gets the button, the button and proactive chat prompt, or nothing
 
    if (typeof(eStoreChat)=='undefined') {
       emclp.eStoreChat='OFF';
    }

    if (emclp.livePersonIsEstore) {
        
        lpMTagConfig.vars.push(["page","ChatEngage",emclp.getChatTypeValueFromString(emclp.eStoreChat)]);
        
    } else {
        
        lpMTagConfig.vars.push(["page","ChatEngage",emclp.getChatTypeValueFromString(emclp.getChatType())]);
            
        // debug output
        emclp.livePersonDebugOutput+='\nButton HTML is in the page?:  '+($j('#lpChatButton').length>0);

        // fix for missing lpChatButton on pages with share bars from HTML not include:
        if (emclp.getChatTypeValueFromString(emclp.getChatType()) && $j('#lpChatButton').length==0) {
            // debug output
            emclp.livePersonDebugOutput+='\n   --> Trying to add button HTML dynamically...';
             if ($j('.utility-item').length==0) {
               // debug output
               emclp.livePersonDebugOutput+='\n   --> No suitable location for button HTML!';
             } else {
               // debug output
               emclp.livePersonDebugOutput+='\n   --> Added button HTML to utility bar successfully!';
               $j('.utility-item').eq(0).before('<li class="utility-item"><div id="lpChatButton" style="margin: 5px 0px 0px 5px;"></div></li>');
            }
        }

        // if all else fails, add hidden lpChatButton <div> for simulated click functionality
        // also set up simultated click links (also used by Inbound Bar)
        //$j(document).ready(emclp.setupSimulatedClicks);
        // set up again at window.load in case pages are showing them on doc.ready
        $j(window).load(emclp.setupSimulatedClicks);
        
    }
     
    // debug output    
    if (!emclp.livePersonIsEstore) {
        emclp.livePersonDebugOutput+='\nChat setting:  '+emclp.getChatType();
        emclp.livePersonDebugOutput+='\nVariables being sent to LivePerson:';
        for (v in lpMTagConfig.vars) {
           if (lpMTagConfig.vars[v][1]!='undefined' && lpMTagConfig.vars[v][1]!=undefined) {
                emclp.livePersonDebugOutput+='\n   --> '+lpMTagConfig.vars[v][1]+':  '+lpMTagConfig.vars[v][2];
           }
        }
   } else {
        emclp.livePersonDebugOutput+='\nChat setting:  '+emclp.eStoreChat;
        emclp.livePersonDebugOutput+='\nVariables being sent to LivePerson:';
        for (v in lpMTagConfig.vars) {
           if (lpMTagConfig.vars[v][1]!='undefined' && lpMTagConfig.vars[v][1]!=undefined) {
                emclp.livePersonDebugOutput+='\n   --> '+lpMTagConfig.vars[v][1]+':  '+lpMTagConfig.vars[v][2];
           }
        }
    }
    emclp.livePersonDebugOutput+='\nDivision:  '+emclp.division;
    emclp.livePersonDebugOutput+='\nPage is in eStore?:  '+emclp.livePersonIsEstore;
     
    trace('================================================');
    trace(emclp.livePersonDebugOutput);
    trace('================================================');
 
    // for legacy support in case existing uses check old chat system activation
    livePersonSkill=emclp.livePersonSkill;

};

emclp.setupSimulatedClicks=function() {
    if (emclp.getChatTypeValueFromString(emclp.getChatType()) && $j('#lpChatButton').length==0) {
        $j('body').append('<div style="width: 0px; height: 0px; overflow: hidden;"><div id="lpChatButton" class="emclp-simulated-click-target"></div></div>');
        emclp.livePersonDebugOutput+='\nNo button present: adding hidden lpChatButton for simulated click support!';
    }
    $j('.emclp-simulate-click').each(function(ix,el) {
        if (!$j(el).hasClass('emclp-setup')) {
            $j(el).click(function(ev) {
                $j('#lpChatButton a img').first().click();
                ev.preventDefault();
            });
	    $j(el).closest('.emclp-simulate-click').addClass('emclp-setup');
        }
    });
};

emclp.livePersonNotEnabled=function() {
     // debug output
     emclp.livePersonDebugOutput+='\nLanguage:  '+emclp.livePersonLanguage;
     emclp.livePersonDebugOutput+='\nPage URL:  '+emclp.livePersonPageURL;
     emclp.livePersonDebugOutput+='\nLIVEPERSON NOT ENABLED FOR THIS COUNTRY';
    
     trace('================================================');
     trace(emclp.livePersonDebugOutput);
     trace('================================================');
};



if (emclp.useLP) {

if ($j('#lpChatButton').length==0) {
    // force button add at top of page if none is present in the HTML so far
    $j('body').prepend('<div style="width: 0px; height: 0px; overflow: hidden;"><div id="lpChatButton" class="emclp-simulated-click-target"></div></div>');
}

/* LIVE PERSON PROVIDED CODE BELOW, FOR UPDATES COPY AND PASTE OVER THE CODE BETWEEN THESE COMMENTS, DO NOT DISTURB CODE ABOVE OR BELOW COMMENTS */
var lpMTagConfig=lpMTagConfig||{};lpMTagConfig.vars=lpMTagConfig.vars||[];lpMTagConfig.dynButton=lpMTagConfig.dynButton||[];lpMTagConfig.lpProtocol=document.location.toString().indexOf("https:")==0?"https":"http";lpMTagConfig.pageStartTime=(new Date).getTime();if(!lpMTagConfig.pluginsLoaded)lpMTagConfig.pluginsLoaded=!1;
lpMTagConfig.loadTag=function(){for(var a=document.cookie.split(";"),b={},c=0;c<a.length;c++){var d=a[c].substring(0,a[c].indexOf("="));b[d.replace(/^\s+|\s+$/g,"")]=a[c].substring(a[c].indexOf("=")+1)}for(var a=b.HumanClickRedirectOrgSite,b=b.HumanClickRedirectDestSite,c=["lpTagSrv","lpServer","lpNumber","deploymentID"],d=!0,e=0;e<c.length;e++)lpMTagConfig[c[e]]||(d=!1,typeof console!="undefined"&&console.log&&console.log("LivePerson : lpMTagConfig."+c[e]+" is required and has not been defined before lpMTagConfig.loadTag()."));
if(!lpMTagConfig.pluginsLoaded&&d)lpMTagConfig.pageLoadTime=(new Date).getTime()-lpMTagConfig.pageStartTime,a="?site="+(a==lpMTagConfig.lpNumber?b:lpMTagConfig.lpNumber)+"&d_id="+lpMTagConfig.deploymentID+"&default=simpleDeploy",lpAddMonitorTag(lpMTagConfig.deploymentConfigPath!=null?lpMTagConfig.lpProtocol+"://"+lpMTagConfig.deploymentConfigPath+a:lpMTagConfig.lpProtocol+"://"+lpMTagConfig.lpTagSrv+"/visitor/addons/deploy2.asp"+a),lpMTagConfig.pluginsLoaded=!0};
function lpAddMonitorTag(a){if(!lpMTagConfig.lpTagLoaded){if(typeof a=="undefined"||typeof a=="object")a=lpMTagConfig.lpMTagSrc?lpMTagConfig.lpMTagSrc:lpMTagConfig.lpTagSrv?lpMTagConfig.lpProtocol+"://"+lpMTagConfig.lpTagSrv+"/hcp/html/mTag.js":"Unknown_83_filename"/*tpa=http://www.emc.com/hcp/html/mTag.js*/;a.indexOf("http")!=0?a=lpMTagConfig.lpProtocol+"://"+lpMTagConfig.lpServer+a+"?site="+lpMTagConfig.lpNumber:a.indexOf("site=")<0&&(a+=a.indexOf("?")<0?"?":"&",a=a+"site="+lpMTagConfig.lpNumber);var b=document.createElement("script");b.setAttribute("type",
"text/javascript");b.setAttribute("charset","iso-8859-1");b.setAttribute("src",a);document.getElementsByTagName("head").item(0).appendChild(b)}}window.attachEvent?window.attachEvent("onload",function(){lpMTagConfig.disableOnLoad||lpMTagConfig.loadTag()}):window.addEventListener("load",function(){lpMTagConfig.disableOnLoad||lpMTagConfig.loadTag()},!1);
function lpSendData(a,b,c){if(arguments.length>0)lpMTagConfig.vars=lpMTagConfig.vars||[],lpMTagConfig.vars.push([a,b,c]);if(typeof lpMTag!="undefined"&&typeof lpMTagConfig.pluginCode!="undefined"&&typeof lpMTagConfig.pluginCode.simpleDeploy!="undefined"){var d=lpMTagConfig.pluginCode.simpleDeploy.processVars();lpMTag.lpSendData(d,!0)}}function lpAddVars(a,b,c){lpMTagConfig.vars=lpMTagConfig.vars||[];lpMTagConfig.vars.push([a,b,c])};
/* LIVE PERSON PROVIDED CODE ABOVE, FOR UPDATES COPY AND PASTE OVER THE CODE BETWEEN THESE COMMENTS, DO NOT DISTURB CODE ABOVE OR BELOW COMMENTS */
        
emclp.livePersonFinishSetup();

} else {
     
emclp.livePersonNotEnabled();
     
}


/*
json2.js
*/if(typeof JSON!=="object"){JSON={}}(function(){"use strict";function f(e){return e<10?"0"+e:e}function quote(e){escapable.lastIndex=0;return escapable.test(e)?'"'+e.replace(escapable,function(e){var t=meta[e];return typeof t==="string"?t:"\\u"+("0000"+e.charCodeAt(0).toString(16)).slice(-4)})+'"':'"'+e+'"'}function str(e,t){var n,r,i,s,o=gap,u,a=t[e];if(a&&typeof a==="object"&&typeof a.toJSON==="function"){a=a.toJSON(e)}if(typeof rep==="function"){a=rep.call(t,e,a)}switch(typeof a){case"string":return quote(a);case"number":return isFinite(a)?String(a):"null";case"boolean":case"null":return String(a);case"object":if(!a){return"null"}gap+=indent;u=[];if(Object.prototype.toString.apply(a)==="[object Array]"){s=a.length;for(n=0;n<s;n+=1){u[n]=str(n,a)||"null"}i=u.length===0?"[]":gap?"[\n"+gap+u.join(",\n"+gap)+"\n"+o+"]":"["+u.join(",")+"]";gap=o;return i}if(rep&&typeof rep==="object"){s=rep.length;for(n=0;n<s;n+=1){if(typeof rep[n]==="string"){r=rep[n];i=str(r,a);if(i){u.push(quote(r)+(gap?": ":":")+i)}}}}else{for(r in a){if(Object.prototype.hasOwnProperty.call(a,r)){i=str(r,a);if(i){u.push(quote(r)+(gap?": ":":")+i)}}}}i=u.length===0?"{}":gap?"{\n"+gap+u.join(",\n"+gap)+"\n"+o+"}":"{"+u.join(",")+"}";gap=o;return i}}if(typeof Date.prototype.toJSON!=="function"){Date.prototype.toJSON=function(){return isFinite(this.valueOf())?this.getUTCFullYear()+"-"+f(this.getUTCMonth()+1)+"-"+f(this.getUTCDate())+"T"+f(this.getUTCHours())+":"+f(this.getUTCMinutes())+":"+f(this.getUTCSeconds())+"Z":null};String.prototype.toJSON=Number.prototype.toJSON=Boolean.prototype.toJSON=function(){return this.valueOf()}}var cx=/[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,escapable=/[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,gap,indent,meta={"\b":"\\b","	":"\\t","\n":"\\n","\f":"\\f","\r":"\\r",'"':'\\"',"\\":"\\\\"},rep;if(typeof JSON.stringify!=="function"){JSON.stringify=function(e,t,n){var r;gap="";indent="";if(typeof n==="number"){for(r=0;r<n;r+=1){indent+=" "}}else if(typeof n==="string"){indent=n}rep=t;if(t&&typeof t!=="function"&&(typeof t!=="object"||typeof t.length!=="number")){throw new Error("JSON.stringify")}return str("",{"":e})}}if(typeof JSON.parse!=="function"){JSON.parse=function(text,reviver){function walk(e,t){var n,r,i=e[t];if(i&&typeof i==="object"){for(n in i){if(Object.prototype.hasOwnProperty.call(i,n)){r=walk(i,n);if(r!==undefined){i[n]=r}else{delete i[n]}}}}return reviver.call(e,t,i)}var j;text=String(text);cx.lastIndex=0;if(cx.test(text)){text=text.replace(cx,function(e){return"\\u"+("0000"+e.charCodeAt(0).toString(16)).slice(-4)})}if(/^[\],:{}\s]*$/.test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,"@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,"]").replace(/(?:^|:|,)(?:\s*\[)+/g,""))){j=eval("("+text+")");return typeof reviver==="function"?walk({"":j},""):j}throw new SyntaxError("JSON.parse")}}})()
