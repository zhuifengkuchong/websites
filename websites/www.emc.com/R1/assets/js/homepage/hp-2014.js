var hp2014={};

hp2014.isMobile=(window.location.href.indexOf('?forceios')>=0 || navigator.userAgent.toLowerCase().search('android')>=0 || navigator.userAgent.toLowerCase().search('iphone')>=0 || navigator.userAgent.toLowerCase().search('ipad')>=0 || navigator.userAgent.toLowerCase().search('ipod')>=0);
hp2014.isIOS=(window.location.href.indexOf('?forceios')>=0 || navigator.userAgent.toLowerCase().search('iphone')>=0 || navigator.userAgent.toLowerCase().search('ipad')>=0 || navigator.userAgent.toLowerCase().search('ipod')>=0);
hp2014.isAndroid=navigator.userAgent.toLowerCase().search('android')>=0;

$j(document).ready(function() {
    if (!hp2014.isMobile) {
        hp2014.tick=setInterval(hp2014.navCheck,20);
    }
});

hp2014.boxPeekHeight=100;
hp2014.minLeadspaceHeight=542;

hp2014.navCheck=function() {
    if ($j('#hp-leadspace-bg-img-lg').css('display')=='block' || $j('#hp-leadspace-bg-img-xl').css('display')=='block') {
        if ($j(window).height()>hp2014.minLeadspaceHeight) {
            if ($j(window).height()<hp2014.minLeadspaceHeight+$j('#headerWrap').height()+hp2014.boxPeekHeight) {
                $j('#hp-leadspace').height(Math.min(hp2014.minLeadspaceHeight,$j(window).height()-hp2014.boxPeekHeight-$j('#headerWrap').height()));
            } else {
                $j('#hp-leadspace').attr('style','');
            }
        }
    } else {
        $j('#hp-leadspace').attr('style','');
    }
};

hp2014.testLeadspaceImages=function(largeImageURL,descriptionText,linkText,linkURL,cssClass) {
    $j('.hp-leadspace-bg-img.ri-xl > div').attr('style','background-image: url('+largeImageURL.split('-lg').join('-xl')+')');
    $j('.hp-leadspace-bg-img.ri-lg > div').attr('style','background-image: url('+largeImageURL+')');
    $j('.hp-leadspace-bg-img.ri-md').attr('src',largeImageURL.split('-lg').join('-md'));
    $j('.hp-leadspace-bg-img.ri-sm').attr('src',largeImageURL.split('-lg').join('-sm'));
    $j('#hp-leadspace-textimg-lg').attr('src',largeImageURL.split('-lg').join('-text-lg').split('.jpg').join('.png').split('.gif').join('.png'));
    if (typeof(descriptionText)!='undefined') {
        $j('.hp-leadspace-text').html(descriptionText);
    }
    if (typeof(linkText)!='undefined') {
        $j('.hp-inner .hp-button').html(linkText);
    }
    if (typeof(linkURL)!='undefined') {
        $j('.hp-leadspace-text').attr('href',descriptionText);
    }
    if (typeof(cssClass)!='undefined') {
        $j('#hp-leadspace').addClass(cssClass);
    }
};


/* WORLD 2015 ANIM */

var world2015anim={}
world2015anim.hexes={};
// top/left offsets are /2, widths and heights are actual image sizes
world2015anim.hexes['hex-1']={ left: -523, top: -230, width: 523, height: 458, sequence: 0 };
world2015anim.hexes['hex-2']={ left: -292, top: -85, width: 533, height: 467, sequence: 2 };
world2015anim.hexes['hex-3']={ left: -54, top: -224, width: 532, height: 467, sequence: 4 };
world2015anim.hexes['hex-4']={ left: 184, top: -96, width: 522, height: 459, sequence: 11 }; // main
world2015anim.hexes['hex-5']={ left: 423, top: -231, width: 535, height: 469, sequence: 7 };
world2015anim.hexes['hex-6']={ left: 646, top: -87, width: 521, height: 458, sequence: 9 };

world2015anim.hexes['hex-7']={ left: -521, top: 46, width: 524, height: 458, sequence: 1 };
world2015anim.hexes['hex-8']={ left: -258, top: 183, width: 522, height: 442, sequence: 3 };
world2015anim.hexes['hex-9']={ left: -50, top: 46, width: 533, height: 468, sequence: 5 };
world2015anim.hexes['hex-10']={ left: 184, top: 172, width: 533, height: 468, sequence: 6 };
world2015anim.hexes['hex-11']={ left: 412, top: 43, width: 533, height: 468, sequence: 8 };
world2015anim.hexes['hex-12']={ left: 641, top: 185, width: 524, height: 461, sequence: 10 };

world2015anim.isIE8=false;

world2015anim.initialDelay=0.1;
world2015anim.staggerDelay=0.3;
world2015anim.dropTime=0.6;

world2015anim.init=function() {
    $j.getScript('../../../../homepage/js/TweenMax.min.js'/*tpa=http://www.emc.com/homepage/js/TweenMax.min.js*/,world2015anim.loadImages);
};
world2015anim.isWatchNowVersion=false;
world2015anim.loadImages=function() {
    TweenLite.defaultEase=Quad.easeOut;
    if ($j('#hp-leadspace').hasClass('hp-leadspace-world-2015-anim')) {
        // check for speaker-specific animation
        var speakerName='';
        var watchNow='';
        if ($j('#hp-leadspace').attr('class').indexOf('hp-leadspace-world-2015-anim-speaker')>=0) {
            speakerName=$j('#hp-leadspace').attr('class').slice($j('#hp-leadspace').attr('class').indexOf('hp-leadspace-world-2015-anim-speaker-')+37);
            if (speakerName.indexOf(' ')>=0) {
                speakerName=speakerName.slice(0,speakerName.indexOf(' '));
            }
            speakerName='-'+speakerName;
            watchNow='-watchnow';
            world2015anim.isWatchNowVersion=true;
        }
        // dom insertions
        $j('#hp-leadspace .hp-inner').before('<div id="hex-holder"></div>');
        // ie8 test
        $j('body').append('<!--[if lte IE 8]><scr'+'ipt type="text/javascript">world2015anim.isIE8=true;</scr'+'ipt><![endif]-->');
        for (var i in world2015anim.hexes) {
            var insertVal='';
            var insertDirName='world2015/';
            switch (i) {
                case 'hex-2': 
                    insertVal=watchNow;
                    insertDirName=world2015anim.isWatchNowVersion?'world2015-':'world2015/';
                    break;
                case 'hex-4': 
                    insertVal=speakerName;
                    break;
                default:
            }
            var thisHex=$j('<div class="hex-anchor"><img class="hex" id="'+i+'" src="Unknown_83_filename"/*tpa=http://www.emc.com/images/home2014/hp-leadspace/%27+insertDirName+i+insertVal+%27.png*//></div>');
            var size=Math.round(Math.random()*50)+50;
            TweenMax.set(thisHex,{
                left: world2015anim.hexes[i].left+'px',
                top: world2015anim.hexes[i].top+'px'
            });
            $j('#hex-holder').append(thisHex);
        }
        $j('#hex-holder').append($j('#hex-4').closest('.hex-anchor')); // shuffle main to last/top of stacking order
        world2015anim.checkImageLoadsInt=setInterval(world2015anim.checkImageLoads,250);
    }
};
world2015anim.imageLoadedCount=0;
world2015anim.checkImageLoads=function() {
    world2015anim.imageLoadedCount=0;
    $j('#hex-holder .hex-anchor img').each(function(ix,el) {
        if ($j(el).width()>0) {
            world2015anim.imageLoadedCount++;
            TweenMax.set(el,{
                left: -Math.round(world2015anim.hexes[$j(el).attr('id')].width/4)+'px',
                top: -Math.round(world2015anim.hexes[$j(el).attr('id')].height/4)+'px',
                width:  Math.round(world2015anim.hexes[$j(el).attr('id')].width/2)+'px',
                height:  Math.round(world2015anim.hexes[$j(el).attr('id')].height/2)+'px'
            });
        }
    });
    if (world2015anim.imageLoadedCount>=$j('#hex-holder .hex-anchor img').length) {
        clearInterval(world2015anim.checkImageLoadsInt);
        world2015anim.doWorldAnimation();
    }
}
world2015anim.doWorldAnimation=function() {
    $j('#hex-holder').css('visibility','visible');
    if (world2015anim.isIE8) {
        $j('#hex-holder .hex-anchor img').each(function(ix,el) {
            TweenMax.from(el,world2015anim.dropTime,{
                delay: world2015anim.initialDelay+(world2015anim.hexes[$j(el).attr('id')].sequence*world2015anim.staggerDelay),
                display: 'none',
                left: (($j(el).attr('id')=='hex-4')?Math.round(world2015anim.hexes[$j(el).attr('id')].width/-8):Math.round(world2015anim.hexes[$j(el).attr('id')].width/-2))+'px',
                top: (($j(el).attr('id')=='hex-4')?Math.round(world2015anim.hexes[$j(el).attr('id')].height/-8):Math.round(world2015anim.hexes[$j(el).attr('id')].height/-2))+'px',
                width: (($j(el).attr('id')=='hex-4')?Math.round(world2015anim.hexes[$j(el).attr('id')].width/4):world2015anim.hexes[$j(el).attr('id')].width)+'px',
                height: (($j(el).attr('id')=='hex-4')?Math.round(world2015anim.hexes[$j(el).attr('id')].height/4):world2015anim.hexes[$j(el).attr('id')].height)+'px',
                onCompleteParams: [(ix==$j('#hex-holder .hex-anchor img').length-1)],
                onComplete: function(last) {if (last) { world2015anim.startPulseAnimation(); }}
            });
        });
    } else {
        $j('#hex-holder .hex-anchor img').each(function(ix,el) {
            var randRotate=0;
            while (randRotate<10 && randRotate>-10 && $j(el).attr('id')!='hex-4') {
                randRotate=(Math.random()*60)-30;
            }
            TweenMax.from(el,world2015anim.dropTime,{
                delay: world2015anim.initialDelay+(world2015anim.hexes[$j(el).attr('id')].sequence*world2015anim.staggerDelay),
                opacity: 0,
                display: 'none',
                rotation: randRotate,
                scaleX: ($j(el).attr('id')=='hex-4')?0.5:2,
                scaleY: ($j(el).attr('id')=='hex-4')?0.5:2,
                onCompleteParams: [(ix==$j('#hex-holder .hex-anchor img').length-1)],
                onComplete: function(last) {if (last) { world2015anim.startPulseAnimation(); }}
            });
        });
    }
};
world2015anim.startPulseAnimation=function() {
    if (world2015anim.isWatchNowVersion) {
        if (world2015anim.isIE8) {
            TweenMax.to('#hex-2',2,{
                width: Math.round($j('#hex-2').width()*1.05)+'px',
                height: Math.round($j('#hex-2').height()*1.05)+'px',
                left: Math.round($j('#hex-2').width()*-0.525)+'px',
                top: Math.round($j('#hex-2').height()*-0.525)+'px',
                yoyo: true,
                repeat: -1,
                ease: Quad.easeInOut
            });
        } else {
            TweenMax.to('#hex-2',2,{
                scaleX: 1.05,
                scaleY: 1.05,
                yoyo: true,
                repeat: -1,
                ease: Quad.easeInOut
            });
        }
    }
};
$j(window).load(world2015anim.init);
