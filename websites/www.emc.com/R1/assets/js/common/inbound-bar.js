if (typeof(inboundBar)=='undefined') {
    var inboundBar={ suppress: false };
}
if (typeof(inboundBar.suppressURLs)=='undefined') {
    inboundBar.suppressURLs=[];
}

inboundBar.suppressURLs.push('/emc-plus/');

inboundBar.loadScript = function (path) {
  var result = jQuery.Deferred(),
  script = document.createElement("script");
  script.async = "async";
  script.type = "text/javascript";
  script.src = path;
  script.onload = script.onreadystatechange = function (_, isAbort) {
      if (!script.readyState || /loaded|complete/.test(script.readyState)) {
         if (isAbort)
             result.reject();
         else
            result.resolve();
    }
  };
  script.onerror = function () { result.reject(); };
  jQuery("head")[0].appendChild(script);
  return result.promise();
};

// if the HTML was not parsed by Apache to place the "fulldomain" variable, this var will hold the text "${fulldomain}"
// since that would only happen on the responsive nav, if this condition is found just reset "fullDomain" to an
// empty string which will match the value the regular header has on EMC.com. Then the "isRemote" variable can be set
// to accurately determine whether we're on an internal or external site
//if (fullDomain=='${fulldomain}') {
    //fullDomain='';
//}
//var isRemote=(fullDomain.split('/').join('').split('-').join('').split('.').join('').indexOf(window.location.hostname.split('/').join('').split('-').join('').split('.').join(''))>=0);

inboundBar.getDomain=function() {
    return '';
    //return (isRemote?fullDomain.slice(0,-1):'');
};

inboundBar.maxH=0;
inboundBar.allSliders=[];
inboundBar.isOpen=false;
inboundBar.ease='';

// send debug info to console if present, otherwise fail silently
inboundBar.trace=function() {
    if (typeof(console)!='undefined') {
	if (typeof(console.log)!='undefined') {
	    for (i=0;i<arguments.length;i++) {
		console.log(arguments[i]);
	    }
	}
    }
};

inboundBar.disableSelection=function(target){
    if (typeof target.onselectstart!="undefined") //IE route
	target.onselectstart=function(){return false}
    else if (typeof target.style.MozUserSelect!="undefined") //Firefox route
	target.style.MozUserSelect="none"
    else //All other route (ie: Opera)
	target.onmousedown=function(){return false}
};

inboundBar.isMobile=function() { return (window.location.href.indexOf('?forceios')>=0 || navigator.userAgent.toLowerCase().search('android')>=0 || navigator.userAgent.toLowerCase().search('iphone')>=0 || navigator.userAgent.toLowerCase().search('ipad')>=0 || navigator.userAgent.toLowerCase().search('ipod')>=0); };

inboundBar.eventTypes={
    'down': function() {
        return inboundBar.isMobile()?'touchstart':'mousedown';
    },
    'up': function() {
        return inboundBar.isMobile()?'touchend':'mouseup';
    },
    'over': function() {
        return inboundBar.isMobile()?'touchstart':'mouseenter';
    },
    'out': function() {
        return inboundBar.isMobile()?'touchend':'mouseleave';
    }
};
    

// MAIN UI /////////////////////////////////////////////////////////////

inboundBar.timings={
    initial: 500,
    initialDelay: 100,
    openClose: 250,
    instant: 1
};

inboundBar.ghfDdRoTO=0; // global header-footer dropdown rollover timeout

inboundBar.init=function() {
    
    // suppress entire inbound bar for a page -- place "var inboundBar={ suppress: true };" in page's JS
    for (var i=0;i<inboundBar.suppressURLs.length;i++) {
        if (window.location.pathname.indexOf(inboundBar.suppressURLs[i])==0) {
            inboundBar.suppress=true;
        }
    }

    if ($j('#inboundBar').length && inboundBar.suppress!==true) {

        // suppress individual contact items by physically removing them from the page prior to processing
        // place "var inboundBar={ suppress: 'sales-chat,support-chat,request-quote,email-us,phone' };" in page's JS
        // (but only include the ones you actually want to remove in the list)
        if (typeof(inboundBar.suppress)=='string') {
            inboundBar.suppress=inboundBar.suppress.split(',');
            for (var i=0;i<inboundBar.suppress.length;i++) {
                $j('#inboundBar .ib-icon.ib-icon-'+inboundBar.suppress[i]).remove();
            }
        }
        
        if (typeof(jQuery.easing.easeInOutQuad)!='function') {
            // monkey patch with easing equation if it doesn't exist
            jQuery.extend(jQuery.easing,{
                    easeInOutQuad: function (x, t, b, c, d) {
                    if ((t/=d/2) < 1) return c/2*t*t + b;
                    return -c/2 * ((--t)*(t-2) - 1) + b;
                }
            });
        }
	
	// get translations file
	$j.ajax('inbound-bar2-translations.js'/*tpa=http://www.emc.com/R1/assets/js/common/inbound-bar2-translations.js*/,{
	    dataType: 'script',
	    async: false
	});

	// get initial y pos in case page has overridden it, store for use later
	inboundBar.initialY=parseInt($j('#inboundBar').css('top'));
	
	// prevent lync icon in phone numbers -- temporary fix until proper Lync support can be worked out
	// happens again when user clicks to open IB since Lync sometimes runs later than this line
	inboundBar.tempLyncWorkaround();

	// remove old style social button bars and copy HTML for responsive version on responsive pages
        $j('#socialmediabar, #socialBar').remove();
        if ($j('body').hasClass('responsive-page') && !$j('body').hasClass('sidebar-search-field_medium-size')) {
            $j('#navigation_wrapper').after($j('#inboundBar').clone().attr('id','inboundBarResp'));
            $j('#inboundBarResp').width(0).find('.ib-content-inner').attr('style','');
            $j('#navigation_wrapper #searchFormWrapper.searchFormWrapper').addClass('isHidden');
            if ($j('#headerWrap #header #headerTop .btn-inboundbar').length==0) {
                $j('#headerWrap #header #headerTop').append('<a class="btn btn-inboundbar"></a>');
            }
        }
	
	// more inits
        $j('body').addClass('hasInboundBar');
        inboundBar.allSliders=$j('#inboundBar .ib-slider');
            
        // copy text from localization HTML elements to JS objects, remove
	$j('#inboundBarText').children().each(function(ix,el) {
	    inboundBar.textPieces[$j(el).attr('class')]=$j(el).html();
	});
        $j('#inboundBarText').remove();
	
        // add tab, icon, error notice, and other structure (to simplify HTML)
        $j('#inboundBar .ib-slider').prepend('<div class="ib-tab-bg"><div class="ib-tab-topend"></div><div class="ib-tab-end"></div></div>');
        $j('#inboundBar .ib-icon').each(function(ix,el) {
	    $j(el).find('a').each(function(ix1,el1) {
		if (!$j(el).hasClass('ib-description-right')) {
		    $j(el1).find('span').remove();
		    var temp=$j(el1).text();
		    if (ix1==0) {
			//$j(el1).wrapInner('<div class="ib-icon-text-label" />').prepend('<div class="ib-icon-wrapper"></div>').append('<div class="clearBothTight"></div>');
			$j(el1).html('<div class="ib-icon-wrapper"></div><div class="ib-icon-text-label">'+temp+'</div><div class="clearBothTight"></div>');
		    } else {
			//$j(el1).wrapInner('<div class="ib-icon-text-label" />').append('<div class="clearBothTight"></div>');
			$j(el1).html('<div class="ib-icon-text-label">'+temp+'</div><div class="clearBothTight"></div>');
		    }
		} else {
		    $j(el1).wrapInner('<div class="ib-icon-text-label" />').prepend('<div class="ib-icon-wrapper"></div>').append('<div class="clearBothTight"></div>');
		}
		$j(el).append('<div class="clearBothTight"></div>');
	    });
	});
        $j('#inboundBarResp .ib-icon').each(function(ix,el) {
	    if ($j(el).find('a').length>1) {
		$j(el).wrapInner('<div class="ib-icon-multi-link-wrap"></div>').prepend('<div class="ib-icon-wrapper"></div>');
		$j(el).find('a').wrapInner('<div class="ib-icon-text-label" />').append('<div class="clearBothTight"></div>');
	    } else {
		$j(el).find('a').wrapInner('<div class="ib-icon-text-label" />').prepend('<div class="ib-icon-wrapper"></div>').append('<div class="clearBothTight"></div>');
	    }
	    $j(el).append('<div class="clearBothTight"></div>');
	});
        //$j('.inboundBar .ib-icon').append('<div class="clearBothTight"></div>');
	
	// fix addthis
	// first make sure it's available
	if (typeof(addthis)=='undefined') {
	    $j.getScript('../../../../../s7.addthis.com/js/250/addthis_widget.js-pub=ra-4d70fb56582d1bec&domready=1.js'/*tpa=http://s7.addthis.com/js/250/addthis_widget.js?pub=ra-4d70fb56582d1bec&domready=1*/,inboundBar.addThisHandler);
	} else {
	    inboundBar.addThisHandler();
	}

	// contact-specific setup, DOM additions
	$j('.ib-contact-content-inner').append('<div class="ib-error-popup"><table><tr><td align="center" valign="middle"><div class="ib-error-notice"></div><div class="ib-error-button ib-button">'+inboundBar.getTextPiece('ib-button-ok')+'</div></td></tr></table></div>');
	// open panel TL corner icon
	$j('#inboundBar .ib-sliderContact .ib-contact-content-inner').prepend('<div class="ib-content-inner-icon"></div>');
	
        $j('#inboundBar').addClass('setup');
                    
                    
        // layout calculations and HTML updates
        
        $j('#inboundBar .ib-slider').width(980);
        // adjust slider widths to fit icons dynamically
        var temp=0;
        $j('#inboundBar .ib-sliderContact .ib-content-inner li').each(function(ix,el) {
            temp+=$j(el).outerWidth()+parseInt($j(el).css('margin-right'));
        });
        $j('#inboundBar .ib-sliderContact .ib-content-inner').width(temp);
        temp=0;
        $j('#inboundBar .ib-sliderContact .ib-content-inner li').each(function(ix,el) {
            temp+=$j(el).outerWidth()+parseInt($j(el).css('margin-right'));
        });
        $j('#inboundBar .ib-sliderContact .ib-content-inner').width(temp);
        // adjust chat slider height to fit rollovers dynamically
        temp=0;
        $j('#inboundBar .ib-sliderContact .ib-content-inner .ib-contact-rollover').each(function(ix,el) {
            temp=Math.max($j(el).outerHeight(),temp);
        });
        //$j('#inboundBar .ib-sliderContact .ib-content-inner .ib-contact-rollovers').height(temp);        
        //$j('#inboundBar .ib-content').each(function(ix,el) {
        //    $j(el).height(Math.max($j(el).siblings('.ib-tab').outerHeight(),$j(el).find('.ib-content-inner').outerHeight()));
        //});
        //$j('#inboundBar .ib-sliderContact .ib-content-inner .ib-contact-rollovers').height(temp);        
        $j('.ib-sliderShare .ib-content').each(function(ix,el) {
            $j(el).height(Math.max($j(el).siblings('.ib-tab').outerHeight(),$j(el).find('.ib-content-inner').outerHeight()));
        });
        $j('#inboundBar .ib-slider').each(function(ix,el) {
            $j(el).width($j(el).find('.ib-tab').outerWidth()+$j(el).find('.ib-content').outerWidth());
        });
        $j('.inboundBar .ib-contact-rollover').each(function(ix,el) { $j(el).attr('ix',ix); });
        inboundBar.disableSelection($j('#inboundBar')[0]);
	
	// radio boxes
	inboundBar.forms.radio.init();
	
	// copy social "more" overlay to functional position
	setTimeout(function() {
	    $j('#nightShadeContainerContent').append($j('#ib-social-more-overlay'));
	},50);

	inboundBar.spinner.init();
	
        $j('#inboundBar').removeClass('setup').addClass('setup-complete');

        setTimeout(inboundBar.closeAllTabs,inboundBar.timings.initialDelay/2);
	                
        // attach events
        
        $j('#inboundBar .ib-tab, #inboundBar .ib-tab-topend, #inboundBar .ib-tab-end, #inboundBar .ib-tab-bg').bind(inboundBar.eventTypes.down(),inboundBar.onTabClick);
        $j('body').bind(inboundBar.eventTypes.down(),inboundBar.onTabBodyClick);
        $j('body').bind('click',inboundBar.onTabBodyClickBlock);
        if ($j('body').hasClass('responsive-page')) {
            $j('.btn.btn-inboundbar').click(function() {
                animateNav('right'); // in responsive GHF
            });
            // click icon to reveal search field
            $j('#navigation_wrapper #searchFormWrapper.searchFormWrapper.isHidden').click(function(ev) {
                $j('#navigation_wrapper #searchFormWrapper.searchFormWrapper').removeClass('isHidden').find('input').focus();
            });
            $j('#inboundBarResp .ib-icon, .inboundBar .ib-icon .emailUsOverlayLink').click(animateNav);
        }
        $j('#inboundBar .ib-sliderContact .ib-icon').bind('mouseenter touchstart',inboundBar.onContactRollover);
        $j('#inboundBar .ib-sliderContact .ib-icon').bind('click',inboundBar.onContactClick);
        $j('.inboundBar .ib-icon .emailUsOverlayLink').click(inboundBar.closeAllTabs);
        $j('.inboundBar .nullLink').click(function(ev) { ev.preventDefault(); });
        $j('#header #menu .menuItem').bind(inboundBar.eventTypes.over(),function(ev) {
            if (ev.type=='mouseenter') {
                inboundBar.ghfDdRoTO=setTimeout(inboundBar.closeAllTabs,200);
            }
        });
        $j('#header #menu .menuItem').bind(inboundBar.eventTypes.out(),function(ev) {
            if (ev.type=='mouseleave') {
                clearTimeout(inboundBar.ghfDdRoTO);
            }
        });
	$j('.ib-button-social-more').click(inboundBar.social.onMoreClick); 

	inboundBar.errors.init();

	//$j('.ib-form-field.ib-form-field-text,.ib-form-field.ib-form-field-textarea,.ib-form-field.ib-form-field-dropdown').live('change',inboundBar.forms.textFieldOnBlur);
	//$j('.ib-form-field.ib-form-field-text,.ib-form-field.ib-form-field-textarea,.ib-form-field.ib-form-field-dropdown').live('blur',inboundBar.forms.textFieldOnBlur);
	//$j('.ib-form-field.ib-form-field-text,.ib-form-field.ib-form-field-textarea,.ib-form-field.ib-form-field-dropdown').live('focus',inboundBar.forms.textFieldOnFocus);
	
        // reveal
        inboundBar.reveal();
        
    }
    
};

inboundBar.addThisHandler=function() {
    var temp=$j('#inboundBar .ib-sliderShare ul').clone();
    $j(temp).attr('class','').attr('id','ib-shareButtons');
    $j(temp).find('a').each(function(ix,el) {
	var tempB=$j(el).clone();
	if (tempB.hasClass('addthis_button_compact')) {
	    tempB.attr('class','').addClass('ib-button-social-more').attr('href','#');
	} else if (tempB.attr('class').indexOf('addthis')>=0) {
	    var classes=tempB.attr('class').split(' ');
	    tempB.attr('class','emc-addthis');
	    for (var i=0;i<classes.length;i++) {
		if (classes[i].indexOf('addthis')>=0) {
		    //tempB.addClass(classes[i].replace('addthis','dontaddthisyet'));
		    tempB.addClass(classes[i]);
		}
	    }
	    tempB.attr('href','#');
	}
	$j(el).after(tempB);
	$j(el).remove();
    });
    $j('#inboundBar .ib-sliderShare ul').after(temp[0]);
    $j('#inboundBar .ib-sliderShare ul.addthis_toolbox').remove();
    // now redo addthis from a clean slate using our own classes rather than their auto-discovery ones
    if (typeof(addthis)!='undefined') {
	addthis.toolbox('#ib-shareButtons');
    }
};

inboundBar.hasRevealed=false;
inboundBar.reveal=function() {
    if (!inboundBar.hasRevealed) {
	inboundBar.hasRevealed=true;
	$j('#inboundBar .ib-slider-wrapper').css('right','0px').delay(inboundBar.timings.initialDelay).animate({
	    right: '40px'
	},inboundBar.timings.initial,jQuery.easing.easeOutQuad);
    }
};

inboundBar.textPieces={};

// legacy support
inboundBar.getTextPiece=function(which) {
    return inboundBar.translations.getTranslation(which,inboundBar.translations.getDefaultLang());
    //return inboundBar.textPieces[which];
};

inboundBar.translations=inboundBar.translations?inboundBar.translations:{};

inboundBar.translations.getDomainKey=function() {
    if (emclp.simulatedURL) {
	var temp=emclp.simulatedURL.slice(emclp.simulatedURL.indexOf('//')+2);
	return temp.slice(0,temp.indexOf('/')).toLowerCase().split('.').join('').split('-').join('').split('emccom').join('').split('emclocal').join('');	
    } else {
	return window.location.hostname.toLowerCase().split('.').join('').split('-').join('').split('emccom').join('').split('emclocal').join('');	
    }
};

inboundBar.translations.getDefaultLang=function() {
    var lang='en_US';
    if (typeof(emcDomainMap[inboundBar.translations.getDomainKey()])!='undefined') {
	lang=emcDomainMap[inboundBar.translations.getDomainKey()];
    }
    return lang;
};

inboundBar.translations.getTranslation=function(piece,lang) {
    if (typeof(inboundBar.translations[lang])!='undefined') {
	if (typeof(inboundBar.translations[lang][piece])!='undefined') {
	    return inboundBar.translations[lang][piece];
	} else {
	    if (typeof(inboundBar.translations[inboundBar.translations.getP9Fallback()][piece])!='undefined') {
		return inboundBar.translations[inboundBar.translations.getP9Fallback()][piece];
	    } else {
		return '';
	    }
	}
    } else {
	if (typeof(inboundBar.translations[inboundBar.translations.getP9Fallback()][piece])!='undefined') {
	    return inboundBar.translations[inboundBar.translations.getP9Fallback()][piece];
	} else {
	    return '';
	}
    }
};

inboundBar.translations.getP9Fallback=function() {
    if (typeof(emcDomainMapP9[subDomain])!='undefined') {
	return emcDomainMapP9[subDomain];
    } else {
	return 'en_US';
    }
};


/* SPINNER PNG ANIM */
    
inboundBar.spinner={};
inboundBar.spinner.baseSpriteX=-86;
inboundBar.spinner.spriteW=32;
inboundBar.spinner.numFrames=12;
inboundBar.spinner.html='<div class="ib-spinner" data-spinner-frame="0"><div class="ib-spinner-inner"></div></div>';
inboundBar.spinner.ticker=function() {
    $j('.ib-spinner').each(function(ix,el) {
	var cFrame=$j(el).attr('data-spinner-frame');
	cFrame++;
	if (cFrame>=inboundBar.spinner.numFrames) { cFrame%=inboundBar.spinner.numFrames; }
	$j(el).attr('data-spinner-frame',cFrame);
	$j(el).find('.ib-spinner-inner').css({
	    backgroundPosition: (inboundBar.spinner.baseSpriteX-(cFrame*inboundBar.spinner.spriteW))+'px -2px'
	});
    });
};
inboundBar.spinner.init=function() {
    inboundBar.spinner.tick=setInterval(inboundBar.spinner.ticker,80);
};
inboundBar.spinner.create=function(el) {
    $j(el).children('.ib-spinner').remove();
    $j(el).css({ position: 'relative' });
    $j(el).append(inboundBar.spinner.html);
};
inboundBar.spinner.destroy=function(el) {
    $j(el).children('.ib-spinner').remove();
};



inboundBar.tempLyncWorkaround=function() {
    //if ($j('.ib-icon.ib-icon-phone a').attr('href').indexOf('tel:')<0) {
	$j('.ib-icon.ib-icon-phone nobr').each(function(ix,el) {
	    var temp=$j(el).find('span').text();
	    temp=temp.split(' ').join('<nobr>&nbsp;</nobr>');
	    temp=temp.split('+').join('<nobr>+</nobr>');
	    temp=temp.split('0').join('<nobr>0</nobr>');
	    temp=temp.split('1').join('<nobr>1</nobr>');
	    temp=temp.split('2').join('<nobr>2</nobr>');
	    temp=temp.split('3').join('<nobr>3</nobr>');
	    temp=temp.split('4').join('<nobr>4</nobr>');
	    temp=temp.split('5').join('<nobr>5</nobr>');
	    temp=temp.split('6').join('<nobr>6</nobr>');
	    temp=temp.split('7').join('<nobr>7</nobr>');
	    temp=temp.split('8').join('<nobr>8</nobr>');
	    temp=temp.split('9').join('<nobr>9</nobr>');
	    $j(el).find('span').replaceWith(temp);
	});
    //}
};


/* SLIDERS */
    
inboundBar.onTabBodyClick=function(ev) {
    if (inboundBar.isOpen) {
        if ($j(ev.target).closest('.ib-slider').length==0) {
            ev.preventDefault();
            inboundBar.closeAllTabs();
        }
    }
};
inboundBar.onTabBodyClickBlock=function(ev) {
    if (inboundBar.isOpen) {
        if ($j(ev.target).closest('.ib-slider').length==0) {
            ev.preventDefault();
        }
    }
};

inboundBar.onTabClick=function(ev) {
    var thisSlider=$j(ev.target).closest('.ib-slider');
    inboundBar.isOpen=true;
    inboundBar.onContactRollover();
    // reset
    $j('#inboundBar').css({
	top: '0px'
    });
    $j('#inboundBar .ib-slider-wrapper').css({
	top: inboundBar.initialY+'px'
    });
    if (!thisSlider.hasClass('ib-tab-open')) {
	inboundBar.adjustTabWidth(thisSlider);
	// force addthis to populate buttons if it hasn't already
	//if (typeof(addthis)=='undefined') {
	//    $j.getScript('../../../../../s7.addthis.com/js/250/addthis_widget.js-pub=ra-4d70fb56582d1bec&domready=1.js'/*tpa=http://s7.addthis.com/js/250/addthis_widget.js?pub=ra-4d70fb56582d1bec&domready=1*/,function() {
	//	addthis.toolbox('.ib-sliderShare ul.addthis_toolbox');
	//    });
	//} else {
	//    addthis.toolbox('.ib-sliderShare ul.addthis_toolbox');
	//}
    } else if (thisSlider.hasClass('ib-tab-open')) {
        // clicked on a tab that's open
        inboundBar.closeAllTabs();
    }
    inboundBar.tempLyncWorkaround();
};

inboundBar.adjustTabWidth=function(thisSlider) {
    var otherSliders=thisSlider.siblings('.ib-slider');
    // clicked on a tab that's not open
    thisSlider.addClass('ib-tab-open').removeClass('ib-tab-closed');
    otherSliders.addClass('ib-tab-closed').removeClass('ib-tab-open');
    thisSlider.stop().clearQueue().animate({
	width: (thisSlider.find('.ib-content-inner').outerWidth()+40)+'px',
	left: '-'+thisSlider.find('.ib-content-inner').outerWidth()+'px',
	height: inboundBar.getSliderHeight(thisSlider)+'px'
    },inboundBar.timings.openClose,inboundBar.ease,function() {
	thisSlider.addClass('ib-tab-open').removeClass('ib-tab-closed');
    });
    otherSliders.each(function(ix,el) {
	$j(el).stop().clearQueue().animate({
	    left: '0px',
	    height: $j(el).find('.ib-tab').outerHeight()+'px'
	},inboundBar.timings.openClose,inboundBar.ease);
    });
//    $j('#inboundBar').stop().clearQueue().animate({
//	width: (thisSlider.find('.ib-content-inner').outerWidth()+40)+'px',
//	height: inboundBar.getNewTotalHeight()
//    },inboundBar.timings.openClose);
    $j('#inboundBar').stop().css({height: '100%'}).clearQueue().animate({
	width: (thisSlider.find('.ib-content-inner').outerWidth()+40)+'px'
    },inboundBar.timings.openClose);
    // remove classes now that calcs are done, will be reapplied at end of anim for visual appearance
    thisSlider.removeClass('ib-tab-open').addClass('ib-tab-closed'); 
    if (thisSlider.attr('id')=='ib-sliderContact') {
	inboundBar.onContactRollover();
    }
};

/* pass-through for compatibility */
inboundBar.closeAll=function() { inboundBar.closeAllTabs(); }

inboundBar.closeAllTabs=function() {
    // reset
    $j('.ib-error-notice .ib-error-button').unbind('click',inboundBar.closeAllTabs);
    $j('#inboundBar').css({
	top: inboundBar.initialY+'px'
    });
    $j('#inboundBar .ib-slider-wrapper').css({
	top: '0px'
    });
    //inboundBar.isOpen=false;
    $j(inboundBar.allSliders).addClass('ib-tab-closed').removeClass('ib-tab-open');
    $j(inboundBar.allSliders).each(function(ix,el) {
        $j(el).stop().clearQueue().animate({
            left: '0px',
            height: $j(el).find('.ib-tab').outerHeight()+'px'
        },inboundBar.timings.openClose,inboundBar.ease);
    });
    //$j('#inboundBar').stop().clearQueue().delay(inboundBar.timings.openClose).animate({
    //    width: '41px',
    //    height: inboundBar.getNewTotalHeight()
    //},inboundBar.timings.instant,function() {
    //    inboundBar.isOpen=false;
    //});
    $j('#inboundBar').stop().clearQueue().delay(inboundBar.timings.openClose).animate({
        width: '41px',
        height: inboundBar.getNewTotalHeight()+$j('#inboundBar .ib-slider-wrapper').position().top+5
    },inboundBar.timings.instant,function() {
        inboundBar.isOpen=false;
    });
    //$j('#inboundBar').stop().clearQueue().delay(inboundBar.timings.openClose).animate({
    //    width: '41px'
    //},inboundBar.timings.instant,function() {
    //    inboundBar.isOpen=false;
    //});
};

inboundBar.getSliderHeight=function(el) {
    if ($j(el).hasClass('ib-tab-open')) {
        return Math.max($j(el).find('.ib-tab').outerHeight(),$j(el).find('.ib-content').outerHeight());
    } else {
        return Math.max($j(el).find('.ib-tab').outerHeight());
    }
};

inboundBar.getNewTotalHeight=function() {
    var nH=0;
    $j(inboundBar.allSliders).each(function(ix,el) {
        nH+=inboundBar.getSliderHeight(el);
    });
    return nH;
};


/* CONTACT SLIDER FUNC */

inboundBar.tempObj={ pos: -439 };
inboundBar.cContactRollover=0;

inboundBar.onContactRollover=function(ev) {
    var evEl;
    var force=false;
    var thisClass;
    var targetClass;
    if (typeof(ev)=='undefined') {
        force=true;
        evEl=$j('#inboundBar .ib-sliderContact .ib-icon').eq(inboundBar.cContactRollover);
    } else {
        evEl=$j(ev.target).closest('.ib-icon');
        inboundBar.cContactRollover=Number(evEl.attr('ix'));
    }
    if (typeof(ev)!='undefined' && !evEl.hasClass('ib-icon-rolledover')) {
        ev.preventDefault();
    }
    if (evEl.length) {
        evEl.addClass('ib-icon-rolledover').siblings('.ib-icon').removeClass('ib-icon-rolledover');
        thisClass=evEl[0].className.replace('ib-icon','').replace('ib-icon-no-rollover','').replace('ib-icon-rolledover','').replace('ib-icon-large','').replace('ib-icon-small','').split(' ').join('');
        targetClass=thisClass.replace('icon','rollover');
        var zeroPointOffset=-490;
        var iconOffset=0;
        if (!evEl.closest('.ib-slider').is(':animated') || force) {
            $j('#inboundBar .'+targetClass).show();
            $j('#inboundBar .'+targetClass).siblings().hide();
            iconOffset=Math.max(-439,Math.round(zeroPointOffset+(evEl.position().left-parseInt(evEl.closest('.ib-content-inner').css('padding-left'))+(evEl.width()/2))));
            if ($j('#inboundBar .ib-contact-rollovers').css('display')=='none') {
                $j('#inboundBar .ib-contact-rollovers').css({
                    //display: 'block',
                    backgroundPosition: iconOffset+'px -434px'
                });
                inboundBar.tempObj.pos=iconOffset;
            } else {
                $j(inboundBar.tempObj).stop().clearQueue().animate({
                    pos: iconOffset
                },{
                    step: function() {
                        $j('#inboundBar .ib-contact-rollovers').css({backgroundPosition: Math.round(inboundBar.tempObj.pos)+'px -434px'});
                    },
                    duration: 125,
                    easing: inboundBar.ease
                });
            }
        }
    }
};

//inboundBar.onContactRollout=function(ev) {
//    $j('#inboundBar .ib-contact-rollovers').hide();
//};

inboundBar.onContactClick=function(ev) {
    $j('.inboundBar .ib-contact-content').show();
    var evEl=$j(ev.target).closest('.ib-icon');
    var matchingClass='ib-contact-content'+evEl.attr('class').split('ib-icon-rolledover').join('').split('ib-icon-large').join('').split('ib-icon-small').join('').split('ib-icon').join('').split(' ').join('');
    if ($j('.'+matchingClass).length) {
	ev.preventDefault();
	$j(evEl).closest('.ib-slider').addClass('ib-has-icon-selected');
	$j(evEl).closest('.ib-slider').find('.ib-contact-rollovers').hide();
	$j(evEl).addClass('ib-icon-selected').siblings('.ib-icon').removeClass('ib-icon-selected');
	var thisClass=evEl[0].className.replace('ib-icon','').replace('ib-icon-no-rollover','').replace('ib-icon-rolledover','').replace('ib-icon-selected','').replace('ib-icon-large','').replace('ib-icon-small','').split(' ').join('').replace('ib-icon','ib-contact-content');
	$j(evEl).closest('.ib-content-inner').find('.'+thisClass).show().siblings().hide();
	// adjust slider width to fit content dynamically
	$j('#inboundBar .ib-sliderContact .ib-content-inner').width($j('#inboundBar .ib-contact-content').outerWidth());
	inboundBar.adjustTabWidth($j(evEl).closest('.ib-slider'));
	inboundBar.contactSliderInits[thisClass.replace('ib-contact-content-','')]();
	inboundBar.errors.close();
    } else {
	inboundBar.closeAllTabs();
    }
};


// SOCIAL /////////////////////////////////////////////////////////////

// retrofitting IB2 "More" button to IB1 system to fix security hole in AddThis
// SOCIAL /////////////////////////////////////////////////////////////
//$j(document).ready(function() {
//var temp=$j('.ib-content-inner a.addthis_button_compact').clone();
//temp.attr('class','').addClass('ib-button-social-more').attr('href','#');
//$j('.ib-content-inner a.addthis_button_compact').after(temp);
//$j('.ib-content-inner a.addthis_button_compact').remove();
//});

inboundBar.social={};
	
inboundBar.social.onMoreClick=function(ev) {
    ev.preventDefault();
    if (!$j('#nightShadeContainerContent').find('#ib-social-more-overlay').length) {
	$j('#nightShadeContainerContent').append('<div id="ib-social-more-overlay" class="overlayPage"><div id="ib-social-more-overlay-addthis" class="addthis_toolbox addthis_default_style addthis_32x32_style"><h3>'+$j('#inboundBar .ib-sliderShare .addthis_toolbox h3').eq(0).text()+'</h3></div></div>');
	for (var i=1;i<=20;i++) {
	    $j('#ib-social-more-overlay-addthis').append('<a class="addthis_button_preferred_'+i+'"></a>');
	    if (i%5==0) {
		$j('#ib-social-more-overlay-addthis').append('<br/>');
	    }
	}
	$j('#ib-social-more-overlay-addthis').append('<div class="clearBothTight"></div>');
	addthis.toolbox('#ib-social-more-overlay-addthis');
    }
    openOverlay('ib-social-more-overlay');
    inboundBar.closeAll();
};


// FORMS /////////////////////////////////////////////////////////////

inboundBar.forms={};

inboundBar.forms.validate=function(el,isBeforeSubmit) {
    var val;
    var defaultVal;
    var type=$j(el).attr('validate-type');
    var pass=false;
    var required=$j(el).hasClass('ib-form-field-required');
    $j(el).val($j(el).val().split('"').join(''));
    switch (type) {
	case 'email':
	    val=$j(el).val();
	    defaultVal=$j(el).attr('default-value');
	    var emailRegex=/^(['a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
	    pass=emailRegex.test(val);
	    break;
	case 'alpha_numeric':
	    val=$j(el).val();
   	    defaultVal=$j(el).attr('default-value');
	    pass=(val!='') && (val!=defaultVal);
	    break;
	case 'numeric':
	    val=$j(el).val();
	    defaultVal=$j(el).attr('default-value');
	    pass=(val!='') && (val!=defaultVal) && !isNaN(val) && !Boolean($j(el).val().match(/[^\d.]/g));
	    $j(el).val($j(el).val().replace(/[^\d.]/g, ''));
	    if ($j(el).val()=='') {
		$j(el).val($j(el).attr('default-value'));
	    }
	    break;
	case 'dropdown':
	    val=el.selectedIndex;
	    defaultVal=0;
	    pass=(val!=defaultVal) || (val==defaultVal && $j(el).hasClass('ib-form-field-disabled'));
	    break;
	default:
    }
    if (required && pass) {
	pass=true;
    } else if (!required && ((val!='') || (val!=defaultVal))) { 
	pass=true;
    } else {
	pass=false;
    }
    if (isBeforeSubmit) {
	if (pass) {
	    $j(el).removeClass('ib-field-failed-validation');
	    $j(inboundBar.forms.getJSSelectFromHTMLSelect(el)).removeClass('ib-field-failed-validation');
	} else {
	    $j(el).addClass('ib-field-failed-validation');
	    $j(inboundBar.forms.getJSSelectFromHTMLSelect(el)).addClass('ib-field-failed-validation');
	}
    } else {
	if (pass || val==defaultVal) {
	    $j(el).removeClass('ib-field-failed-validation');
	    $j(inboundBar.forms.getJSSelectFromHTMLSelect(el)).removeClass('ib-field-failed-validation');
	} else {
	    $j(el).addClass('ib-field-failed-validation');
	    $j(inboundBar.forms.getJSSelectFromHTMLSelect(el)).addClass('ib-field-failed-validation');
	}
    }
    return pass;
};

inboundBar.forms.hasRedirectableValueChosen=function(el) {
    var chosen=false;
    var el=$j(el);
    if (el.hasClass('ib-form-field-dropdown')) {
	el.find('option').each(function(ix1,el1) {
	    if (el1.selected && typeof($j(el1).attr('data-alt-url'))!='undefined') {
		chosen=true;
	    }
	});
    }
    return chosen;
};

inboundBar.forms.textFieldOnBlur=function(ev) {
    var el=$j(ev.target).closest('.ib-form-field');
    //window.alert(el);
    if ($j(el).val()=='' || $j(el).val()==$j(el).attr('default-value')) {
	$j(el).val($j(el).attr('default-value')).addClass('ib-form-field-default');
    }
    inboundBar.forms.validate(el);
};
inboundBar.forms.textFieldOnFocus=function(ev) {
    var el=$j(ev.target).closest('.ib-form-field');
    //window.alert(el);
    $j(el).removeClass('ib-field-failed-validation');
    if ($j(el).val()==$j(el).attr('default-value')) {
	$j(el).val('').removeClass('ib-form-field-default');
    }
};

inboundBar.forms.createField=function(target,params) {
    var fieldEl;

    var theDefaultClass=(params.lastKnownValue!='')?'':' ib-form-field-default';
    var theRequiredClass=(params.mandatory)?' ib-form-field-required':' ib-form-field-optional';

    switch (params.type) {
	
	case 'Text Field':
	    var theVal=(params.lastKnownValue!='')?params.lastKnownValue:params.label;
	    fieldEl=$j('<input class="ib-form-field ib-form-field-text'+theDefaultClass+theRequiredClass+'" type="text" order="'+params.order+'" validate-type="'+params.validationType+'" lpid="'+params.id+'" name="f'+params.id+'" id="ib-form-element-'+params.id+'" value="'+theVal+'" default-value="'+params.label+'"/>');
	    $j(target).append(fieldEl);
	    break;
	
	case 'Text Area':
	    var theVal=(params.lastKnownValue!='')?params.lastKnownValue:params.label;
	    var theDefaultClass=(params.lastKnownValue!='')?'':' ib-form-field-default';
	    fieldEl=$j('<textarea class="ib-form-field ib-form-field-textarea'+theDefaultClass+theRequiredClass+'" order="'+params.order+'" validate-type="'+params.validationType+'" lpid="'+params.id+'" name="f'+params.id+'" id="ib-form-element-'+params.id+'" value="'+theVal+'" default-value="'+params.label+'">'+theVal+'</textarea>');
	    $j(target).append(fieldEl);
	    break;
	
	case 'Dropdown Box':
	    var ddHTML='<select effect="up" class="ib-form-field ib-form-field-dropdown ib-form-field-dropdown-up'+theRequiredClass+'" order="'+params.order+'" validate-type="dropdown" lpid="'+params.id+'" name="ib-form-element-'+params.id+'" id="ib-form-element-'+params.id+'">';
	    ddHTML+='<option value="#">'+params.label+'</option>';
	    for (var i=0;i<params.entry.length;i++) {
		if (params.entry[i].displayValue.charAt(0)=='#') {
		    params.entry[i].displayValue=params.entry[i].displayValue.slice(1);
		    ddHTML+='<option value="'+params.entry[i].value+'" data-alt-url="technical-inquiry-redirect'+emclp.getDivisionForInsertion(true)+'">'+params.entry[i].displayValue+'</option>';
		} else {
		    ddHTML+='<option value="'+params.entry[i].value+'">'+params.entry[i].displayValue+'</option>';
		}
	    }
	    ddHTML+='</select>';
	    $j(target).append(ddHTML);
	    fieldEl=$j('#ib-form-element-'+params.id);
	    // js select box activation
	    $j(fieldEl).eq(0).selectbox({
		effect: 'up'
	    });
	    var jsSelectEl=inboundBar.forms.getJSSelectFromHTMLSelect(fieldEl);
	    // copy some CSS classes back and forth between the select and SB versions
	    if (fieldEl.hasClass('ib-form-field-dropdown-up')) {
		jsSelectEl.addClass('sbHolderUp');
	    }
	    jsSelectEl.attr('order',params.order);
	    // change event
	    if (typeof(params.onChange)!='undefined') {
		fieldEl.bind('change',params.onChange);
	    }
	    //var temp=$j(jsSelectEl).find('.sbSelector').text();
	    var fSize=parseInt($j(jsSelectEl).find('.sbSelector').css('font-size'));
	    while ($j(jsSelectEl).find('.sbSelector').height()>20 && fSize>10) {
		//temp=temp.slice(0,-1);
		//$j(jsSelectEl).find('.sbSelector').html(temp+'&hellip;');
		fSize--;
		$j(jsSelectEl).find('.sbSelector').css('font-size',fSize+'px');
	    }
	    break;
	
	default:
	    inboundBar.trace('IB2: Unimplemented form field type:');
	    inboundBar.trace(params);
	    
    }
    //$j(fieldEl).bind('change',inboundBar.forms.textFieldOnBlur);
    //$j(fieldEl).bind('blur',inboundBar.forms.textFieldOnBlur);
    //$j(fieldEl).bind('focus',inboundBar.forms.textFieldOnFocus);
};

inboundBar.forms.getJSSelectFromHTMLSelect=function(el) {
    return $j('#sbHolder_'+$j(el).attr('sb'));
};
inboundBar.forms.getHTMLSelectFromJSSelect=function(el) {
    return $j('select[sb="'+$j(el).attr('id').replace('ib-form-element-','')+'"]');
};

inboundBar.forms.staggerLayout=function(formWrapperEl) {
    $j(formWrapperEl).find('input.ib-form-field,.sbHolder,.ib-button,.ib-form-field-textarea').each(function(ix,el) {
	//if (!$j(el).hasClass('ib-form-field-disabled')) {
	    $j(el).removeClass('ib-form-field-right');
	    if (ix%2) {
		$j(el).addClass('ib-form-field-right');
	    }
	//}
    });
};

inboundBar.forms.radio={};
inboundBar.forms.radio.count=0;
inboundBar.forms.radio.init=function(ev) {
    $j('.ib-form-field-radio-group').each(function(ix,el) {
	inboundBar.forms.radio.create(el);
    });
}
inboundBar.forms.radio.create=function(el) {
    $j(el).attr('radio-group',inboundBar.forms.radio.count);
    inboundBar.forms.radio.count++;
    $j(el).find('.ib-form-field-radio').each(function(ix1,el1) {
	$j(el1).attr('radio-group',inboundBar.forms.radio.count);
	$j(el1).addClass('ib-form-field');
	if (ix1%2) {
	    $j(el1).addClass('ib-form-field-right');
	}
	$j(el).click(inboundBar.forms.radio.click);
	$j(el1).prepend('<div class="ib-form-field-radio-icon"></div>');
    });
};
inboundBar.forms.radio.click=function(ev) {
    var evEl=$j(ev.target).closest('.ib-form-field-radio');
    var groupEl=evEl.closest('.ib-form-field-radio-group');
    var oldVal=groupEl.attr('selected-value');
    evEl.siblings('.ib-form-field-radio').removeClass('ib-form-field-radio-on');
    evEl.addClass('ib-form-field-radio-on');
    groupEl.attr('selected-value',evEl.attr('value'));
    if (oldVal!=groupEl.attr('selected-value')) {
	groupEl.trigger('change');
    }
};


// ERROR DISPLAY /////////////////////////////////////////////////////////////

inboundBar.errors={
    init: function() {
	$j('.ib-error-button').click(inboundBar.errors.close);
    },
    display: function(text) {
	inboundBar.errors.close();
	$j('.ib-error-popup .ib-error-notice').html(text);
	$j('.ib-error-popup').show();
    },
    close: function(force) {
	if (force===true || $j('.ib-error-button').attr('suppress-close')!=='true') {
	    $j('.ib-error-popup .ib-error-notice').html('');
	    $j('.ib-error-popup').hide();
	}
    }
};



// SUB-SECTIONS

inboundBar.contactSliderInits={
    'sales-chat': function() {
	if (!inboundBar.chat.isInitted) {
	    //inboundBar.chat.apiInit();
	    inboundBar.spinner.create($j('.ib-contact-content-sales-chat'));
	//    $j.getScript(inboundBar.getDomain()+'/R1/assets/js/common/inbound-bar2-chat.js').fail(function() {
	//	inboundBar.errors.display(inboundBar.getTextPiece('ib-error-panel-loading'));
	//	inboundBar.spinner.destroy($j('.ib-contact-content-sales-chat'));
	//    });
	    var p=inboundBar.loadScript(inboundBar.getDomain()+'/R1/assets/js/common/inbound-bar2-chat.js');
	    p.fail=function() {
		inboundBar.errors.display(inboundBar.getTextPiece('ib-error-panel-loading'));
		inboundBar.spinner.destroy($j('.ib-contact-content-sales-chat'));
	    };
	//} else {
	    //inboundBar.chat.getAvailability();
	}
    },
    'support-chat': function() {
    },
    'request-quote': function() {
    },
    'email-us': function() {
    },
    'phone': function() {
    }
};

// CHAT
inboundBar.chat={ isInitted: false };




// UPDATE H3s (auto text size)

inboundBar.fitText=function(which,inText) {
    $j(which).attr('style','');
    //$j(which).html(S(inText).stripTags().toString());
    $j(which).html(inText);
    var temp=parseInt($j(which).css('font-size'));
    while ($j(which).height()>40 && temp>=11) {
	$j(which).css({
	    fontSize: temp+'px',
	    lineHeight: Math.ceil(temp*1.2)+'px',
	    fontWeight: (temp<14)?'bold':'normal'
	});
	temp--;
    }
};



// IB2-related HTML updates (to avoid updating IB1 HTML):
$j('#ib-sliderContact .ib-content-inner').wrapInner('<div class="ib-contact-initial"/>');
$j('#ib-sliderContact .ib-contact-initial').after('<div class="ib-contact-content"><div class="ib-contact-content-inner ib-contact-content-sales-chat"><div class="ib-initial-hide"><h3></h3><h4></h4><div class="ib-form-field-radio-group"><div class="ib-form-field-radio ib-button-label-alt-lang" value="pre-chat"></div><div class="ib-form-field-radio ib-button-label-leave-message" value="offline"></div></div><div class="ib-chat-alt-langs"></div><div class="ib-chat-form"><a class="ib-button ib-chat-start-button" href="#"><span class="ib-button-label ib-button-label-chat"></span><span class="ib-button-label ib-button-label-message"></span></a></div></div></div></div>');
$j('#ib-sliderContact').attr('id','').addClass('ib-sliderContact');        
$j('#ib-sliderShare').attr('id','').addClass('ib-sliderShare');
$j('.ib-chat-rollovers').addClass('ib-contact-rollovers').removeClass('ib-chat-rollovers');
$j('.ib-chat-rollover').addClass('ib-contact-rollover').removeClass('ib-chat-rollover');
$j('.ib-icon .emclp-simulate-click').removeClass('emclp-simulate-click');	


// delay to avoid conflicts with ??? -- some pages weren't getting the bar despite it working fine after initting manually
//inboundBar.init();
$j(window).load(function() {
    setTimeout(inboundBar.init,500);
    //setTimeout(inboundBar.reveal,500);
});



// libs

//selectbox
/*!
 * jQuery Selectbox plugin 0.2
 *
 * Copyright 2011-2012, Dimitar Ivanov (http://www.bulgaria-web-developers.com/projects/javascript/selectbox/)
 * Licensed under the MIT (http://www.opensource.org/licenses/mit-license.php) license.
 * 
 * Date: Tue Jul 17 19:58:36 2012 +0300
 * Heavily Modified by Tom Callahan March 2014
 */
(function (e, t) {
    function s() {
        this._state = [];
        this._defaults = {
            classHolder: "sbHolder",
            classHolderDisabled: "sbHolderDisabled",
            classSelector: "sbSelector",
            classOptions: "sbOptions",
            classGroup: "sbGroup",
            classSub: "sbSub",
            classDisabled: "sbDisabled",
            classToggleOpen: "sbToggleOpen",
            classToggle: "sbToggle",
            classFocus: "sbFocus",
            speed: 100,
            effect: "slide",
            onChange: null,
            onOpen: null,
            onClose: null
        }
    }
    var n = "selectbox",
        r = false,
        i = true;
    e.extend(s.prototype, {
        _isOpenSelectbox: function (e) {
            if (!e) {
                return r
            }
            var t = this._getInst(e);
            return t.isOpen
        },
        _isDisabledSelectbox: function (e) {
            if (!e) {
                return r
            }
            var t = this._getInst(e);
            return t.isDisabled
        },
        _attachSelectbox: function (t, s) {
	    
            function g() {
                var t, n, r = this.attr("id").split("_")[1];
                for (t in u._state) {
                    if (t !== r) {
                        if (u._state.hasOwnProperty(t)) {
                            n = e("select[sb='" + t + "']")[0];
                            if (n) {
                                u._closeSelectbox(n)
                            }
                        }
                    }
                }
            }

            function y() {
                var n = arguments[1] && arguments[1].sub ? true : false,
                    r = arguments[1] && arguments[1].disabled ? true : false;
                arguments[0].each(function (s) {
                    var o = e(this),
                        f = e("<li>"),
                        d;
                    if (o.is(":selected")) {
                        l.text(o.text());
                        p = i
                    }
                    if (s === m - 1) {
                        f.addClass("last")
                    }
                    if (!o.is(":disabled") && !r) {
                        d = e("<a>", {
                            href: "#" + o.val(),
                            rel: o.val()
                        }).text(o.text()).bind("http://www.emc.com/R1/assets/js/common/click.sb", function (n) {
                            if (n && n.preventDefault) {
                                n.preventDefault()
                            }
                            var r = c,
                                i = e(this),
                                s = r.attr("id").split("_")[1];
                            u._changeSelectbox(t, i.attr("rel"), i.text());
                            u._closeSelectbox(t)
                        }).bind("http://www.emc.com/R1/assets/js/common/mouseover.sb", function () {
                            var t = e(this);
                            t.parent().siblings().find("a").removeClass(a.settings.classFocus);
                            t.addClass(a.settings.classFocus)
                        }).bind("http://www.emc.com/R1/assets/js/common/mouseout.sb", function () {
                            e(this).removeClass(a.settings.classFocus)
                        });
                        if (n) {
                            d.addClass(a.settings.classSub)
                        }
                        if (o.is(":selected")) {
                            d.addClass(a.settings.classFocus)
                        }
                        d.appendTo(f)
                    } else {
                        d = e("<span>", {
                            text: o.text()
                        }).addClass(a.settings.classDisabled);
                        if (n) {
                            d.addClass(a.settings.classSub)
                        }
                        d.appendTo(f)
                    }
                    f.appendTo(h)
                })
            }
            if (this._getInst(t)) {
                return r
            }
            var o = e(t),
                u = this,
                a = u._newInst(o),
                f, l, c, h, p = r,
                d = o.find("optgroup"),
                v = o.find("option"),
                m = v.length;
            o.attr("sb", a.uid);
            e.extend(a.settings, u._defaults, s);
            u._state[a.uid] = r;
            o.hide();
            f = e("<div>", {
                id: "sbHolder_" + a.uid,
                "class": a.settings.classHolder,
                tabindex: o.attr("tabindex")
            });
            l = e("<a>", {
                id: "sbSelector_" + a.uid,
                href: "#",
                "class": a.settings.classSelector,
                click: function (n) {
                    n.preventDefault();
                    g.apply(e(this), []);
                    var r = e(this).attr("id").split("_")[1];
                    if (u._state[r]) {
                        u._closeSelectbox(t)
                    } else {
                        u._openSelectbox(t)
                    }
                }
            });
            c = e("<a>", {
                id: "sbToggle_" + a.uid,
                href: "#",
                "class": a.settings.classToggle,
                click: function (n) {
                    n.preventDefault();
                    g.apply(e(this), []);
                    var r = e(this).attr("id").split("_")[1];
                    if (u._state[r]) {
                        u._closeSelectbox(t)
                    } else {
                        u._openSelectbox(t)
                    }
                }
            });
            c.appendTo(f);
            h = e("<ul>", {
                id: "sbOptions_" + a.uid,
                "class": a.settings.classOptions,
                css: {
                    display: "none"
                }
            });
            o.children().each(function (t) {
                var n = e(this),
                    r, i = {};
                if (n.is("option")) {
                    y(n)
                } else if (n.is("optgroup")) {
                    r = e("<li>");
                    e("<span>", {
                        text: n.attr("label")
                    }).addClass(a.settings.classGroup).appendTo(r);
                    r.appendTo(h);
                    if (n.is(":disabled")) {
                        i.disabled = true
                    }
                    i.sub = true;
                    y(n.find("option"), i)
                }
            });
            if (!p) {
                l.text(v.first().text())
            }
            e.data(t, n, a);
            f.data("uid", a.uid).bind("http://www.emc.com/R1/assets/js/common/keydown.sb", function (t) {
                var r = t.charCode ? t.charCode : t.keyCode ? t.keyCode : 0,
                    i = e(this),
                    s = i.data("uid"),
                    o = i.siblings("select[sb='" + s + "']").data(n),
                    a = i.siblings(["select[sb='", s, "']"].join("")).get(0),
                    f = i.find("ul").find("a." + o.settings.classFocus);
                switch (r) {
                case 37:
                case 38:
                    if (f.length > 0) {
                        var l;
                        e("a", i).removeClass(o.settings.classFocus);
                        l = f.parent().prevAll("li:has(a)").eq(0).find("a");
                        if (l.length > 0) {
                            l.addClass(o.settings.classFocus).focus();
                            e("#sbSelector_" + s).text(l.text())
                        }
                    }
                    break;
                case 39:
                case 40:
                    var l;
                    e("a", i).removeClass(o.settings.classFocus);
                    if (f.length > 0) {
                        l = f.parent().nextAll("li:has(a)").eq(0).find("a")
                    } else {
                        l = i.find("ul").find("a").eq(0)
                    } if (l.length > 0) {
                        l.addClass(o.settings.classFocus).focus();
                        e("#sbSelector_" + s).text(l.text())
                    }
                    break;
                case 13:
                    if (f.length > 0) {
                        u._changeSelectbox(a, f.attr("rel"), f.text())
                    }
                    u._closeSelectbox(a);
                    break;
                case 9:
                    if (a) {
                        var o = u._getInst(a);
                        if (o) {
                            if (f.length > 0) {
                                u._changeSelectbox(a, f.attr("rel"), f.text())
                            }
                            u._closeSelectbox(a)
                        }
                    }
                    var c = parseInt(i.attr("tabindex"), 10);
                    if (!t.shiftKey) {
                        c++
                    } else {
                        c--
                    }
                    e("*[tabindex='" + c + "']").focus();
                    break;
                case 27:
                    u._closeSelectbox(a);
                    break
                }
                t.stopPropagation();
                return false
            }).delegate("a", "mouseover", function (t) {
                e(this).addClass(a.settings.classFocus)
            }).delegate("a", "mouseout", function (t) {
                e(this).removeClass(a.settings.classFocus)
            });
            l.appendTo(f);
            h.appendTo(f);
            f.insertAfter(o);
            e("html").live("mousedown", function (t) {
                t.stopPropagation();
                e("select").selectbox("close")
            });
            e([".", a.settings.classHolder, ", .", a.settings.classSelector].join("")).mousedown(function (e) {
                e.stopPropagation()
            })
        },
        _detachSelectbox: function (t) {
            var i = this._getInst(t);
            if (!i) {
                return r
            }
            e("#sbHolder_" + i.uid).remove();
            e.data(t, n, null);
            e(t).show()
        },
        _changeSelectbox: function (t, n, s) {
            var o, u = this._getInst(t);
            if (u) {
                o = this._get(u, "onChange");
		var temp=s;
                e("#sbSelector_" + u.uid).text(temp);
		while (e("#sbSelector_" + u.uid).height()>20) {
		    temp=temp.slice(0,-1);
		    e("#sbSelector_" + u.uid).html(temp+'&hellip;');
		}
            }
            n = n.replace(/\'/g, "\\'");
            e(t).find("option").attr("selected", r);
            e(t).find("option[value='" + n + "']").attr("selected", i);
            if (u && o) {
                o.apply(u.input ? u.input[0] : null, [n, u])
            } else if (u && u.input) {
                u.input.trigger("change")
            }
        },
        _enableSelectbox: function (t) {
            var i = this._getInst(t);
            if (!i || !i.isDisabled) {
                return r
            }
            e("#sbHolder_" + i.uid).removeClass(i.settings.classHolderDisabled);
            i.isDisabled = r;
            e.data(t, n, i)
        },
        _disableSelectbox: function (t) {
            var s = this._getInst(t);
            if (!s || s.isDisabled) {
                return r
            }
            e("#sbHolder_" + s.uid).addClass(s.settings.classHolderDisabled);
            s.isDisabled = i;
            e.data(t, n, s)
        },
        _optionSelectbox: function (t, i, s) {
            var o = this._getInst(t);
            if (!o) {
                return r
            }
            o[i] = s;
            e.data(t, n, o)
        },
        _openSelectbox: function (t) {
            var r = this._getInst(t);
            if (!r || r.isOpen || r.isDisabled) {
                return
            }
            var s = e("#sbOptions_" + r.uid),
                o = parseInt(e(window).height(), 10),
                u = e("#sbHolder_" + r.uid).offset(),
                a = e(window).scrollTop(),
                f = s.prev().outerHeight(),
                l = o - (u.top - a) - f / 2,
                c = this._get(r, "onOpen");
            s.css({
                display: "block",
                height: "0px"
            });
            var h = 0;
            var p = 0;
            s.find("li").each(function (t, n) {
                if (t > 0) {
                    h += e(n).outerHeight()
                }
                p = $j(n).outerHeight()
            });
            h = Math.min(h, p * 8);
            switch (r.settings.effect) {
            case "up":
                s.css({
                    top: "auto",
                    bottom: f + "px"
                });
                break;
            default:
                s.css({
                    top: f + "px",
                    bottom: "auto"
                })
            }
            s.animate({
                height: h + "px"
            }, 200);
            e("#sbToggle_" + r.uid).addClass(r.settings.classToggleOpen);
            this._state[r.uid] = i;
            r.isOpen = i;
            if (c) {
                c.apply(r.input ? r.input[0] : null, [r])
            }
            e.data(t, n, r)
        },
        _closeSelectbox: function (t) {
            var i = this._getInst(t);
            if (!i || !i.isOpen) {
                return
            }
            var s = e("#sbOptions_" + i.uid);
            var o = this._get(i, "onClose");
            s.animate({
                height: "0px"
            }, 200, function () {
                s.css("display", "none")
            });
            e("#sbToggle_" + i.uid).removeClass(i.settings.classToggleOpen);
            this._state[i.uid] = r;
            i.isOpen = r;
            if (o) {
                o.apply(i.input ? i.input[0] : null, [i])
            }
            e.data(t, n, i)
        },
        _newInst: function (e) {
            var t = e[0].id.replace(/([^A-Za-z0-9_-])/g, "\\\\$1");
            return {
                id: t,
                input: e,
                uid: Math.floor(Math.random() * 99999999),
                isOpen: r,
                isDisabled: r,
                settings: {}
            }
        },
        _getInst: function (t) {
            try {
                return e.data(t, n)
            } catch (r) {
                throw "Missing instance data for this selectbox"
            }
        },
        _get: function (e, n) {
            return e.settings[n] !== t ? e.settings[n] : this._defaults[n]
        }
    });
    e.fn.selectbox = function (t) {
        var n = Array.prototype.slice.call(arguments, 1);
        if (typeof t == "string" && t == "isDisabled") {
            return e.selectbox["_" + t + "Selectbox"].apply(e.selectbox, [this[0]].concat(n))
        }
        if (t == "option" && arguments.length == 2 && typeof arguments[1] == "string") {
            return e.selectbox["_" + t + "Selectbox"].apply(e.selectbox, [this[0]].concat(n))
        }
        return this.each(function () {
            typeof t == "string" ? e.selectbox["_" + t + "Selectbox"].apply(e.selectbox, [this].concat(n)) : e.selectbox._attachSelectbox(this, t)
        })
    };
    e.selectbox = new s;
    e.selectbox.version = "0.2"
})(jQuery);


/*
string.js - Copyright (C) 2012-2013, JP Richardson <jprichardson@gmail.com>
*/!function(){"use strict";function n(e,t){t!==null&&t!==undefined?typeof t=="string"?e.s=t:e.s=t.toString():e.s=t,e.orig=t,t!==null&&t!==undefined?e.__defineGetter__?e.__defineGetter__("length",function(){return e.s.length}):e.length=t.length:e.length=-1}function r(e){n(this,e)}function u(){for(var e in s)(function(e){var t=s[e];i.hasOwnProperty(e)||(o.push(e),i[e]=function(){return String.prototype.s=this,t.apply(this,arguments)})})(e)}function a(){for(var e=0;e<o.length;++e)delete String.prototype[o[e]];o.length=0}function c(){var e=h(),t={};for(var n=0;n<e.length;++n){var r=e[n],s=i[r];try{var o=typeof s.apply("teststring",[]);t[r]=o}catch(u){}}return t}function h(){var e=[];if(Object.getOwnPropertyNames)return e=Object.getOwnPropertyNames(i),e.splice(e.indexOf("valueOf"),1),e.splice(e.indexOf("toString"),1),e;var t={},n=[];for(var r in String.prototype)t[r]=r;for(var r in Object.prototype)delete t[r];for(var r in t)e.push(r);return e}function p(e){return new r(e)}function d(e,t){var n=[],r;for(r=0;r<e.length;r++)n.push(e[r]),t&&t.call(e,e[r],r);return n}var e="1.8.0",t={},i=String.prototype,s=r.prototype={between:function(e,t){var n=this.s,r=n.indexOf(e),i=n.indexOf(t,r+e.length);return i==-1&&t!=null?new this.constructor(""):i==-1&&t==null?new this.constructor(n.substring(r+e.length)):new this.constructor(n.slice(r+e.length,i))},camelize:function(){var e=this.trim().s.replace(/(\-|_|\s)+(.)?/g,function(e,t,n){return n?n.toUpperCase():""});return new this.constructor(e)},capitalize:function(){return new this.constructor(this.s.substr(0,1).toUpperCase()+this.s.substring(1).toLowerCase())},charAt:function(e){return this.s.charAt(e)},chompLeft:function(e){var t=this.s;return t.indexOf(e)===0?(t=t.slice(e.length),new this.constructor(t)):this},chompRight:function(e){if(this.endsWith(e)){var t=this.s;return t=t.slice(0,t.length-e.length),new this.constructor(t)}return this},collapseWhitespace:function(){var e=this.s.replace(/[\s\xa0]+/g," ").replace(/^\s+|\s+$/g,"");return new this.constructor(e)},contains:function(e){return this.s.indexOf(e)>=0},count:function(e){var t=0,n=this.s.indexOf(e);while(n>=0)t+=1,n=this.s.indexOf(e,n+1);return t},dasherize:function(){var e=this.trim().s.replace(/[_\s]+/g,"-").replace(/([A-Z])/g,"-$1").replace(/-+/g,"-").toLowerCase();return new this.constructor(e)},decodeHtmlEntities:function(){var e=this.s;return e=e.replace(/&#(\d+);?/g,function(e,t){return String.fromCharCode(t)}).replace(/&#[xX]([A-Fa-f0-9]+);?/g,function(e,t){return String.fromCharCode(parseInt(t,16))}).replace(/&([^;\W]+;?)/g,function(e,n){var r=n.replace(/;$/,""),i=t[n]||n.match(/;$/)&&t[r];return typeof i=="number"?String.fromCharCode(i):typeof i=="string"?i:e}),new this.constructor(e)},endsWith:function(e){var t=this.s.length-e.length;return t>=0&&this.s.indexOf(e,t)===t},escapeHTML:function(){return new this.constructor(this.s.replace(/[&<>"']/g,function(e){return"&"+m[e]+";"}))},ensureLeft:function(e){var t=this.s;return t.indexOf(e)===0?this:new this.constructor(e+t)},ensureRight:function(e){var t=this.s;return this.endsWith(e)?this:new this.constructor(t+e)},humanize:function(){if(this.s===null||this.s===undefined)return new this.constructor("");var e=this.underscore().replace(/_id$/,"").replace(/_/g," ").trim().capitalize();return new this.constructor(e)},isAlpha:function(){return!/[^a-z\xC0-\xFF]/.test(this.s.toLowerCase())},isAlphaNumeric:function(){return!/[^0-9a-z\xC0-\xFF]/.test(this.s.toLowerCase())},isEmpty:function(){return this.s===null||this.s===undefined?!0:/^[\s\xa0]*$/.test(this.s)},isLower:function(){return this.isAlpha()&&this.s.toLowerCase()===this.s},isNumeric:function(){return!/[^0-9]/.test(this.s)},isUpper:function(){return this.isAlpha()&&this.s.toUpperCase()===this.s},left:function(e){if(e>=0){var t=this.s.substr(0,e);return new this.constructor(t)}return this.right(-e)},lines:function(){return this.replaceAll("\r\n","\n").s.split("\n")},pad:function(e,t){t==null&&(t=" ");if(this.s.length>=e)return new this.constructor(this.s);e-=this.s.length;var n=Array(Math.ceil(e/2)+1).join(t),r=Array(Math.floor(e/2)+1).join(t);return new this.constructor(n+this.s+r)},padLeft:function(e,t){return t==null&&(t=" "),this.s.length>=e?new this.constructor(this.s):new this.constructor(Array(e-this.s.length+1).join(t)+this.s)},padRight:function(e,t){return t==null&&(t=" "),this.s.length>=e?new this.constructor(this.s):new this.constructor(this.s+Array(e-this.s.length+1).join(t))},parseCSV:function(e,t,n,r){e=e||",",n=n||"\\",typeof t=="undefined"&&(t='"');var i=0,s=[],o=[],u=this.s.length,a=!1,f=this,l=function(e){return f.s.charAt(e)};if(typeof r!="undefined")var c=[];t||(a=!0);while(i<u){var h=l(i);switch(h){case n:if(a&&(n!==t||l(i+1)===t)){i+=1,s.push(l(i));break}if(n!==t)break;case t:a=!a;break;case e:a&&t?s.push(h):(o.push(s.join("")),s.length=0);break;case r:a?s.push(h):c&&(o.push(s.join("")),c.push(o),o=[],s.length=0);break;default:a&&s.push(h)}i+=1}return o.push(s.join("")),c?(c.push(o),c):o},replaceAll:function(e,t){var n=this.s.split(e).join(t);return new this.constructor(n)},right:function(e){if(e>=0){var t=this.s.substr(this.s.length-e,e);return new this.constructor(t)}return this.left(-e)},setValue:function(e){return n(this,e),this},slugify:function(){var e=(new r(this.s.replace(/[^\w\s-]/g,"").toLowerCase())).dasherize().s;return e.charAt(0)==="-"&&(e=e.substr(1)),new this.constructor(e)},startsWith:function(e){return this.s.lastIndexOf(e,0)===0},stripPunctuation:function(){return new this.constructor(this.s.replace(/[^\w\s]|_/g,"").replace(/\s+/g," "))},stripTags:function(){var e=this.s,t=arguments.length>0?arguments:[""];return d(t,function(t){e=e.replace(RegExp("</?"+t+"[^<>]*>","gi"),"")}),new this.constructor(e)},template:function(e,t,n){var r=this.s,t=t||p.TMPL_OPEN,n=n||p.TMPL_CLOSE,i=t.replace(/[-[\]()*\s]/g,"\\$&").replace(/\$/g,"\\$"),s=n.replace(/[-[\]()*\s]/g,"\\$&").replace(/\$/g,"\\$"),o=new RegExp(i+"(.+?)"+s,"g"),u=r.match(o)||[];return u.forEach(function(i){var s=i.substring(t.length,i.length-n.length);typeof e[s]!="undefined"&&(r=r.replace(i,e[s]))}),new this.constructor(r)},times:function(e){return new this.constructor((new Array(e+1)).join(this.s))},toBoolean:function(){if(typeof this.orig=="string"){var e=this.s.toLowerCase();return e==="true"||e==="yes"||e==="on"}return this.orig===!0||this.orig===1},toFloat:function(e){var t=parseFloat(this.s);return e?parseFloat(t.toFixed(e)):t},toInt:function(){return/^\s*-?0x/i.test(this.s)?parseInt(this.s,16):parseInt(this.s,10)},trim:function(){var e;return typeof i.trim=="undefined"?e=this.s.replace(/(^\s*|\s*$)/g,""):e=this.s.trim(),new this.constructor(e)},trimLeft:function(){var e;return i.trimLeft?e=this.s.trimLeft():e=this.s.replace(/(^\s*)/g,""),new this.constructor(e)},trimRight:function(){var e;return i.trimRight?e=this.s.trimRight():e=this.s.replace(/\s+$/,""),new this.constructor(e)},truncate:function(e,t){var n=this.s;e=~~e,t=t||"...";if(n.length<=e)return new this.constructor(n);var i=function(e){return e.toUpperCase()!==e.toLowerCase()?"A":" "},s=n.slice(0,e+1).replace(/.(?=\W*\w*$)/g,i);return s.slice(s.length-2).match(/\w\w/)?s=s.replace(/\s*\S+$/,""):s=(new r(s.slice(0,s.length-1))).trimRight().s,(s+t).length>n.length?new r(n):new r(n.slice(0,s.length)+t)},toCSV:function(){function u(e){return e!==null&&e!==""}var e=",",t='"',n="\\",i=!0,s=!1,o=[];typeof arguments[0]=="object"?(e=arguments[0].delimiter||e,e=arguments[0].separator||e,t=arguments[0].qualifier||t,i=!!arguments[0].encloseNumbers,n=arguments[0].escape||n,s=!!arguments[0].keys):typeof arguments[0]=="string"&&(e=arguments[0]),typeof arguments[1]=="string"&&(t=arguments[1]),arguments[1]===null&&(t=null);if(this.orig instanceof Array)o=this.orig;else for(var a in this.orig)this.orig.hasOwnProperty(a)&&(s?o.push(a):o.push(this.orig[a]));var f=n+t,l=[];for(var c=0;c<o.length;++c){var h=u(t);typeof o[c]=="number"&&(h&=i),h&&l.push(t);if(o[c]!==null&&o[c]!==undefined){var p=(new r(o[c])).replaceAll(t,f).s;l.push(p)}else l.push("");h&&l.push(t),e&&l.push(e)}return l.length=l.length-1,new this.constructor(l.join(""))},toString:function(){return this.s},underscore:function(){var e=this.trim().s.replace(/([a-z\d])([A-Z]+)/g,"$1_$2").replace(/[-\s]+/g,"_").toLowerCase();return(new r(this.s.charAt(0))).isUpper()&&(e="_"+e),new this.constructor(e)},unescapeHTML:function(){return new this.constructor(this.s.replace(/\&([^;]+);/g,function(e,t){var n;return t in v?v[t]:(n=t.match(/^#x([\da-fA-F]+)$/))?String.fromCharCode(parseInt(n[1],16)):(n=t.match(/^#(\d+)$/))?String.fromCharCode(~~n[1]):e}))},valueOf:function(){return this.s.valueOf()}},o=[],f=c();for(var l in f)(function(e){var t=i[e];typeof t=="function"&&(s[e]||(f[e]==="string"?s[e]=function(){return new this.constructor(t.apply(this,arguments))}:s[e]=t))})(l);s.repeat=s.times,s.include=s.contains,s.toInteger=s.toInt,s.toBool=s.toBoolean,s.decodeHTMLEntities=s.decodeHtmlEntities,s.constructor=r,p.extendPrototype=u,p.restorePrototype=a,p.VERSION=e,p.TMPL_OPEN="{{",p.TMPL_CLOSE="}}",p.ENTITIES=t,typeof module!="undefined"&&typeof module.exports!="undefined"?module.exports=p:typeof define=="function"&&define.amd?define([],function(){return p}):window.S=p;var v={lt:"<",gt:">",quot:'"',apos:"'",amp:"&"},m={};for(var g in v)m[v[g]]=g;t={amp:"&",gt:">",lt:"<",quot:'"',apos:"'",AElig:198,Aacute:193,Acirc:194,Agrave:192,Aring:197,Atilde:195,Auml:196,Ccedil:199,ETH:208,Eacute:201,Ecirc:202,Egrave:200,Euml:203,Iacute:205,Icirc:206,Igrave:204,Iuml:207,Ntilde:209,Oacute:211,Ocirc:212,Ograve:210,Oslash:216,Otilde:213,Ouml:214,THORN:222,Uacute:218,Ucirc:219,Ugrave:217,Uuml:220,Yacute:221,aacute:225,acirc:226,aelig:230,agrave:224,aring:229,atilde:227,auml:228,ccedil:231,eacute:233,ecirc:234,egrave:232,eth:240,euml:235,iacute:237,icirc:238,igrave:236,iuml:239,ntilde:241,oacute:243,ocirc:244,ograve:242,oslash:248,otilde:245,ouml:246,szlig:223,thorn:254,uacute:250,ucirc:251,ugrave:249,uuml:252,yacute:253,yuml:255,copy:169,reg:174,nbsp:160,iexcl:161,cent:162,pound:163,curren:164,yen:165,brvbar:166,sect:167,uml:168,ordf:170,laquo:171,not:172,shy:173,macr:175,deg:176,plusmn:177,sup1:185,sup2:178,sup3:179,acute:180,micro:181,para:182,middot:183,cedil:184,ordm:186,raquo:187,frac14:188,frac12:189,frac34:190,iquest:191,times:215,divide:247,"OElig;":338,"oelig;":339,"Scaron;":352,"scaron;":353,"Yuml;":376,"fnof;":402,"circ;":710,"tilde;":732,"Alpha;":913,"Beta;":914,"Gamma;":915,"Delta;":916,"Epsilon;":917,"Zeta;":918,"Eta;":919,"Theta;":920,"Iota;":921,"Kappa;":922,"Lambda;":923,"Mu;":924,"Nu;":925,"Xi;":926,"Omicron;":927,"Pi;":928,"Rho;":929,"Sigma;":931,"Tau;":932,"Upsilon;":933,"Phi;":934,"Chi;":935,"Psi;":936,"Omega;":937,"alpha;":945,"beta;":946,"gamma;":947,"delta;":948,"epsilon;":949,"zeta;":950,"eta;":951,"theta;":952,"iota;":953,"kappa;":954,"lambda;":955,"mu;":956,"nu;":957,"xi;":958,"omicron;":959,"pi;":960,"rho;":961,"sigmaf;":962,"sigma;":963,"tau;":964,"upsilon;":965,"phi;":966,"chi;":967,"psi;":968,"omega;":969,"thetasym;":977,"upsih;":978,"piv;":982,"ensp;":8194,"emsp;":8195,"thinsp;":8201,"zwnj;":8204,"zwj;":8205,"lrm;":8206,"rlm;":8207,"ndash;":8211,"mdash;":8212,"lsquo;":8216,"rsquo;":8217,"sbquo;":8218,"ldquo;":8220,"rdquo;":8221,"bdquo;":8222,"dagger;":8224,"Dagger;":8225,"bull;":8226,"hellip;":8230,"permil;":8240,"prime;":8242,"Prime;":8243,"lsaquo;":8249,"rsaquo;":8250,"oline;":8254,"frasl;":8260,"euro;":8364,"image;":8465,"weierp;":8472,"real;":8476,"trade;":8482,"alefsym;":8501,"larr;":8592,"uarr;":8593,"rarr;":8594,"darr;":8595,"harr;":8596,"crarr;":8629,"lArr;":8656,"uArr;":8657,"rArr;":8658,"dArr;":8659,"hArr;":8660,"forall;":8704,"part;":8706,"exist;":8707,"empty;":8709,"nabla;":8711,"isin;":8712,"notin;":8713,"ni;":8715,"prod;":8719,"sum;":8721,"minus;":8722,"lowast;":8727,"radic;":8730,"prop;":8733,"infin;":8734,"ang;":8736,"and;":8743,"or;":8744,"cap;":8745,"cup;":8746,"int;":8747,"there4;":8756,"sim;":8764,"cong;":8773,"asymp;":8776,"ne;":8800,"equiv;":8801,"le;":8804,"ge;":8805,"sub;":8834,"sup;":8835,"nsub;":8836,"sube;":8838,"supe;":8839,"oplus;":8853,"otimes;":8855,"perp;":8869,"sdot;":8901,"lceil;":8968,"rceil;":8969,"lfloor;":8970,"rfloor;":8971,"lang;":9001,"rang;":9002,"loz;":9674,"spades;":9824,"clubs;":9827,"hearts;":9829,"diams;":9830}}.call(this);
