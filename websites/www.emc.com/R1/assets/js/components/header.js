/*new header.js - 6/28/13 modified by 
 *rodrigo.berlochi@razorfish.com
 *This object runs an ajax call 
 *to check if a user is logged in, and get
 *its values. Those info is ready to use on several
 *ftl templates to create the loginModal. On win load, 
 *runs the core code, updating my account link and modal
 *based on that information.
 */

//Legacy from old header.js
/* workaround for dependency on Prototype's "Array.first()" method */
/* can come out once that dependency in this file is removed */
/* added 4/5/13 by tom.callahan@emc.com */
if (typeof(Array.prototype.first)!='function') {
    Array.prototype.first = function() {
        return this[0];
    };
};

var omnitureVars = omnitureVars || {};

/* BEGIN SET LOGIN PATH TO HANDLE REMOTE SITES */
var remoteSubdomains = [
	{str: 'estoretst', domain: 'http://www.emc.com/R1/assets/js/components/stagefw-a.emc.com',   sufix: 'http://www.emc.com/R1/assets/js/components/sr=emc.com'},
	{str: 'estoredev', domain: 'http://www.emc.com/R1/assets/js/components/stagefw-a.emc.com',   sufix: 'http://www.emc.com/R1/assets/js/components/sr=emc.com'},
	{str: 'estorestg', domain: 'http://www.emc.com/R1/assets/js/components/stagefw-a.emc.com',   sufix: 'http://www.emc.com/R1/assets/js/components/sr=emc.com'},
	{str: 'store',     domain: 'http://www.emc.com/R1/assets/js/components/www.emc.com',         sufix: 'http://www.emc.com/R1/assets/js/components/sr=emc.com'}
];
var loginPath = 'http://www.emc.com/auth/login_redirect.htm';
for (var i = 0; i < remoteSubdomains.length; i++) {
	if (document.location.host.indexOf(remoteSubdomains[i].str) != -1) {
		loginPath = '//' + remoteSubdomains[i].domain + loginPath;
		break;
	}
}
/* END SET LOGIN PATH TO HANDLE REMOTE SITES */

//Create NS
Ext.ns('http://www.emc.com/R1/assets/js/components/Ext.EMC');


//Start header process
window.onload = function(){
	// do not run this if there is no header HTML present
	if (Ext.select('#header').elements.length) {
		Ext.select('#headerRight ul li').removeCls('hide-for-resume');
	}
    //ensuring header update on campaign pages w/ collapsed header
    var headerShowMenu = $j('#headerShowMenu');
    if(headerShowMenu.length > 0){
            Ext.EMC.uwHeader.updateHeader();
            //call trackOmniture here to ensure that function is ready
            if(typeof(trackOmniture) === "function"){
                trackOmniture();
            }else{
                //console.log('trackOmniture is not defined');
            }
    };
};


//Header-user object
Ext.EMC.uwHeader = {
	
    isUserLogged : {//it's used on template to check if loginModal should be created [e.g.: legacyLayout.ftl]
	    status : false, //user isn't logged 
		values : null //not user data
	}, 
    init: function(){
		//this.updateHeader();
	},
	showLogginButton: function(){ 
	    //show my account button when process ends
	    if(!jQuery('body').hasClass('remoteHeaderHideLogin')){
	    	if(!jQuery('body').hasClass('responsive-page')){
		        jQuery('#loginButton').show();
	    	}else{
	    		jQuery('#loginButton').addClass('showLogin');
	    	}
		}
	},
	createURLStoring: function(){
		/*Create a cookie to store the URL where is the user when it start the login process
		 * BE will read it to know where to redirect after the successful login*/
		
		try{
			var cookieName = "UW_LASTURLSTORED";
//			var redirectURL = window.location.host + window.location.pathname + window.location.search + window.location.hash;
			var redirectURL = window.location.host + window.location.pathname + window.location.search;
			//var domain = window.location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');	
			
			for (var i = 0; i < remoteSubdomains.length; i++) {
				if (document.location.host.indexOf(remoteSubdomains[i].str) != -1) {
					if(remoteSubdomains[i].sufix && redirectURL.indexOf(remoteSubdomains[i].sufix) < 0) {
						var sufix = (redirectURL.indexOf("?") > 0 ? "&" : "?") + remoteSubdomains[i].sufix;
						redirectURL = redirectURL + sufix;
					}
					break;
				}
			}
			//window.location.hash has a bug in firefox when decoding. Implementing a crossbrowser solution.
			if(window.location.hash) {
				redirectURL = redirectURL + '#' + location.href.split('#').splice(1).join('#')
			}
			
			document.cookie = "" + cookieName + "=" + encodeURIComponent(redirectURL) + ";domain=.emc.com;path=/";
			
		}catch(err){
			console.log("Last URL cookie error = " + err.message);
		}
		
	},
	updateHeader: function() {
	    var self = this;
	    //store DOM objects
	    var headerDesktop = Ext.select('#headerRight li a').first(); 
        var headerMobile = Ext.select('#loginButtonMobile a').first(); 
		
		
		//getUserInfo -> "not user logged in"
		if(this.isUserLogged.status == false){ 
			
			Ext.get(headerDesktop).set({href : 'http://www.emc.com/login.htm'});
			
			if (headerMobile){
			    Ext.get(headerMobile).set({href : 'http://www.emc.com/login.htm'});
			};

			this.showLogginButton();
			
			jQuery('a[href$="http://www.emc.com/login.htm"]').click(function (e){
				e.preventDefault();
				self.createURLStoring();
				window.location.href = loginPath;
			})
			return; 
			
		}else{
			    //getUserInfo -> "user logged in"
				
				//store a shortcut to the object literal
			    var ud = Ext.EMC.uwHeader.isUserLogged.values
				
			    if((ud.firstName == '') && (ud.lastName == '')){
			    	headerDesktop.dom.innerHTML = ud.userName;
			    }else{
			    	headerDesktop.dom.innerHTML = ud.firstName + ' '+ ud.lastName;	
			    }
				
				//avoid opening login lightbox if user is logged in
				headerDesktop.removeCls('openlightbox').set({href : ''});
				
				this.showLogginButton();
				
				if (headerMobile) {
					headerMobile.dom.innerHTML = 'Logout';
					Ext.get(headerMobile).set({href : 'http://www.emc.com/logout.htm'}).removeCls('mobileLogin').addCls('mobileLogout');
				}
				
				//create my account modal
				this.createMyAccountModal(ud, headerDesktop);
				
		} //END else
		
		
	    
	},
	resumeName: function(){
	    var limit = 26;
	    var chars = Ext.select('#headerRight li a').first();
        var numberchars = chars.dom.innerHTML;
        if (numberchars.length > limit) {
	        newchars = numberchars.substr(0, limit-1);
	        var totalchar = newchars + "...";
	        Ext.fly('headerRight').select('li a').first().update(totalchar);
	        Ext.select('#headerRight ul li').removeCls('hide-for-resume');
        }
	},
	createMyAccountModal : function(ud, headerDesktop){
	            var modalMyAccount = null; 
				try {
					var headerDesktop = Ext.select('#headerRight li a').first(); 
					headerDesktop.on('click', function(ev){
						ev.stopEvent();
						if (modalMyAccount) {
							modalMyAccount.modalShow();
						} else {
							//update user data
							var repFullname = jQuery('#myAccountModal .my-account-fullname');
							if (repFullname) {
								repFullname.html(ud.firstName + ' '+ ud.lastName); 
							}
							var repUsername = jQuery('#myAccountModal .my-account-username');
							if (repUsername) {
								repUsername.html(ud.userName); 
							}
							modalMyAccount = new Ext.EMC.Modal("#myAccountModal", {
								autoCenter: {
									vert:true,
									horz:true
								},
								width: 580,
								height: 265,
								buttons: [{
									text: 'Close',
									cls: 'close-btn-small'
								}],
								closeOnOverlayClick: true,
								destroy: false,
								rootNode :$j('body')[0]
							});
							Ext.get(modalMyAccount.getId()).select('.close-btn-small a').on('click', function(ev) { 
								ev.stopEvent();
								modalMyAccount.modalHide();
							});
						}
					});
				} catch (e) {
					console && console.log && console.log(e);
				}
	},
	setOmnitureValues: function(data){
		/*
		 * @data:  object wrapping user values 
		 * @sets those values for Omniture tracking
		 */
		omnitureVars = omnitureVars || {}; //it's defined globally in  omniture_script_header.inc
		var d = data, 
		    i = 0;
		
		for(i; i<d.length; i++){
		    
			var text = d[i].text;
			var code = d[i].code;
			
			//following attributes are transformed from string to array of integers
			if(code == "credentialEntitlements" || code == "identityEntitlements" || code == "programEntitlements"){
				text = text.split(','); //create array
			}
			
			omnitureVars["" + code + ""] = text;
		}
		
		
		/*
		 *trackOmniture(); -> invoking it below on doc ready to ensure it's defined
		 *It does the value tracking using the omnitureVars object, and it's defined externally
		 **/

	},
	getUserInfo: function(){
	
		function readAnswer(data){
		
			if(data.responseText !== '[]'){ //means user is logged
			  
				//var obj = jQuery.parse(data.responseText);
				var obj = Ext.JSON.decode(data.responseText);

				//so, change properties to user-logged state
				Ext.EMC.uwHeader.isUserLogged.status = true; 
				
				var firstName = lastName = userName = '';
				
				for(var i=0; i<obj.length; i++){
			        var value = obj[i].code;
			        switch(value){
			            case 'firstName':
			                firstName = obj[i].text;
			            break;
			            case 'lastName':
			                lastName = obj[i].text;
			            break;
			            case 'userName':
			                userName = obj[i].text;
			            break;
			            //default: do nothing
			       }
			    };
				
				Ext.EMC.uwHeader.isUserLogged.values = {
					userName : userName,
					firstName : firstName,
					lastName : lastName
				};
				
				Ext.EMC.uwHeader.setOmnitureValues(obj);
			}
			
			Ext.onReady(function(){
				Ext.EMC.uwHeader.updateHeader();
				callToOmniture();
			});
            
		};
		
        function outputErrAnswer(){
			console.log('Error on userInfo request');
			jQuery(document).ready(function(){
				Ext.EMC.uwHeader.updateHeader();
				
				callToOmniture();
			});
		};
        
        function callToOmniture(){
            //call trackOmniture here to ensure that function is ready
            if(typeof(trackOmniture) === "function"){
                trackOmniture();
            }else{
                //console.log('trackOmniture is not defined');
            }
        };
		
        Ext.Ajax.request({
           url: 'http://www.emc.com/userInfo.htm',
           method: 'GET',
           success: readAnswer,
           failure: outputErrAnswer
        });
			
	}
};

//running it so isUserLogged.status is available and 
//updated for the script which create the loginModal (FTL files)
Ext.EMC.uwHeader.getUserInfo();
