//try { console.log(); } catch(e) { console = { log: function(x) {alert(x);} } }
try { console.log(); } catch(e) { console = { log: function(x) {} } }

// this seems to be causing problems in IE. Ext Rulez! :(
if (Ext.isIE) { Ext.enableGarbageCollector = false; }

if (!Ext.EMC) {
	Ext.namespace('http://www.emc.com/R1/assets/js/components/Ext.EMC');
}

/**
 * @class Ext.EMC.Modal A modal dialog implementation for HTML fragments.
 * @extends Object
 * @cfg {boolean} closeOnOverlayClick Wether to close the overlay by (true) clicking outside the viewport or by (false) any other means
 * @cfg {object} autoCenter Wether to vertically or horizontally center the overlay (autoCenter.vert=true, autoCenter.horz=true)
 * @cfg {object} viewportOffset How far from the top-left edge does the viewport need to be (in pixels)
 * @cfg {object} overlayOffset How far from the top-left edge does the viewport need to be (in pixels)
 * @cfg {object} classes Names for the classes the modal dialog will use 
 * @cfg {object} buttons Any buttons you may want the modal to add programatically
 * @cfg {string} wrapperId the ID the outermost element will have (minus its ID)
 * @cfg {integer} width the viewport's width in pixels. Auto-calculated if empty
 * @cfg {integer} height the viewport's height in pixels. Auto-calculated if empty
 * @constructor
 * @param {string / object} element The element whose contents will appear in the modal dialog.Can either be a string or a DOMNode
 * @param {object} cfg an object containing the required configuration options for this class
**/
Ext.EMC.ModalsCache = {};

//Ext JS 4.x class definition
Ext.define('Ext.EMC.Modal', {
    extend: 'Ext.util.Observable',
    dom: {
        manip: Ext.core.DomHelper,
        query: Ext.DomQuery.select
    },
    id: null,
    isVisible: false,
    modals : new Array(),
    defaults: {
        width:false, 
        height:false,
        closeOnOverlayClick: true, //false if you want the modal to only be closable through other means
        overlayOffset: {top:0, left:0},
        viewportOffset: {top: 0,left: 0},
        classes: {
            visible:'visible',//an optionally interesing class to further theme the visible modal
            invisible:'hidden',//a class that is applied to hide the modal
            locked:'unscrollable',//a class that is applied to the body to prevent it from scrolling
            outerWrapper:'modal-dialog-overlay',//a class that is applied to the overlay itself
            buttonWrapper:'modal-dialog-buttons',//a class that is applied to the buttons container
            innerWrapper:'modal-dialog-viewport',//a class that is applied to the modal's "viewport"
            contentWrapper: 'modal-dialog-contents'//a class that is applied to the modal's actual content
        },
        buttons: [],
        destroy: false,
        wrapperId:'modal-dialog',
        rootNode : null
    },
    cfg: {},
    constructor: function(element, cfg){
        this.addEvents({
            "modalOpened" : true,
            "modalClosed" : true
        });
        
        this.listeners = cfg.listeners;
        

        var viewportID = this.dom.query('.modal-dialog-contents').length;
        Ext.apply(this.cfg, cfg, this.defaults);
        // call parent
        this.callParent([cfg]);
        
        this.id = parseInt(viewportID);
        this.cfg.elements = {
          toOverlay: Ext.select(element),
          body: Ext.get(document.getElementsByTagName('body')[0])
        };
        
        this.modals[this.id] = new Object();
        this.modals[this.id].width = this.cfg.width;
        this.buildOverlay();
        this.addHooks();
        Ext.EMC.ModalsCache[this.getId()] = this;
    },
  
    toggleGivenOverlay: function(){
        var overlay = this.getAttribute('id');
        if(!!Ext.EMC.ModalsCache[overlay].cfg.closeOnOverlayClick){
            Ext.EMC.ModalsCache[overlay].toggle();
        }
    },
  
    addHooks: function(){
        var wrappers = Ext.get(this.dom.query('.' + this.cfg.classes.outerWrapper)),
        contents = Ext.get(this.dom.query('.' + this.cfg.classes.innerWrapper)),
        me = this;
        wrappers.each(function(element){
            element.on('click', me.toggleGivenOverlay);
        });
        contents.on('click',function(e){
            e.stopPropagation();
        });
    },
  
    modalShow: function(){
        if (!this.isVisible) {
            Ext.get(this.getId()).removeCls(this.cfg.classes.invisible).addCls(this.cfg.classes.visible);
            this.cfg.elements.body.addCls(this.cfg.classes.locked);
            this.applyPositioning();
            this.isVisible = true;
            this.fireEvent('modalOpened', this.getId());
        }
    },
  
    applyPositioning: function(){
        var wWidth, wHeight, vWidth, vHeight, browserHeight, scrOfY, vTop, vLeft, oTop, oLeft, viewport, overlay;
        viewport = Ext.get(Ext.get(this.getId()).select('.'+this.cfg.classes.innerWrapper).elements[0]);
        overlay = Ext.get(Ext.get(this.getId()).select('.curtain').elements[0]);

        wWidth = (document.documentElement.clientWidth) ? document.documentElement.clientWidth : document.body.offsetWidth;
        wHeight = Ext.getBody().getHeight() + 35;
        
        if(!!this.modals[this.id].width){
            vWidth = this.modals[this.id].width;
            viewport.setWidth(vWidth);
        }
        else {
            vWidth = viewport.getWidth(); 
        }
         
        if(!!this.cfg.height){
            vHeight = this.cfg.height;
            viewport.setHeight(vHeight);
        }
        else {
            vHeight = viewport.getHeight();
        }
		         
        oLeft = !!this.cfg.overlayOffset.left?this.cfg.overlayOffset.left : 0;
        oTop = !!this.cfg.overlayOffset.top?this.cfg.overlayOffset.top : 0;

        if( typeof( window.innerWidth ) == 'number' ) { //!IE
            browserHeight = window.innerHeight;
            scrOfY = window.pageYOffset;
        } else if(document.documentElement.clientWidth) { //IE
            browserHeight = document.documentElement.clientHeight;
            scrOfY = document.documentElement.scrollTop;
        } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
            browserHeight = document.body.clientHeight;
            scrOfY = document.body.scrollTop;
        }
	
        if(this.cfg.autoCenter.horz){
            vLeft = '50%';
            vMarginLeft = -(vWidth/2)+'px';
        } else {
            vLeft = !!this.cfg.viewportOffset.left?this.cfg.viewportOffset.left : 0;
            vLeft = vLeft + oLeft + 'px';
            vMarginLeft = '0px';
        }
    
        if(this.cfg.autoCenter.vert){
            if((browserHeight >= vHeight) && (!Ext.isIE6)){
                vTop = '50%';
                vMarginTop = -(vHeight/2)+'px';
            } else {
                Ext.get(this.dom.query('.' + this.cfg.classes.outerWrapper)).setStyle({'position': 'absolute'});
                vTop = (scrOfY+10)+'px';
                vMarginTop = '0px';
            }
        } else {
            Ext.get(this.dom.query('.' + this.cfg.classes.outerWrapper)).setStyle({'position': 'absolute'});
            vTop = !!this.cfg.viewportOffset.top ? this.cfg.viewportOffset.top : 0;
            vTop = vTop + oTop + 'px';
            vMarginTop ='0px';
        }
        
        //overlay.setX(oLeft).setY(oTop).setHeight(Ext.Element.getViewportHeight()).setWidth(Ext.Element.getViewportWidth());
        viewport.setStyle({
            'margin-left': vMarginLeft,
            'margin-top': vMarginTop,
            'left': vLeft,
            'top': vTop
        });
		
		if( vHeight > wHeight ){
			$j('.curtain').addClass('fixed')
		}
    },
  
    modalHide: function(){
        if(this.isVisible){
            this.fireEvent('modalClosed', this.getId());
            if(this.cfg.destroy){
    //    		Ext.get(this.getId()).remove();
                return false;
            } else {
                try{
                    Ext.get(this.getId()).removeCls(this.cfg.classes.visible).addCls(this.cfg.classes.invisible);
                    this.cfg.elements.body.removeCls(this.cfg.classes.locked);
                    this.isVisible = false;
                } catch(err) {
                    console.log('caught error: ', err);
                }
            }
			if( $j('.curtain.fixed').length > 0 ){
				$j('.curtain').removeClass('fixed');
			}
        }
    },
  
    getId: function(){
        return this.cfg.wrapperId + '-' + this.id;
    },
    buildOverlay: function(){
        var wrap, overlay, clone,
        overlay = {
            tag:'div',
            cls:this.cfg.classes.outerWrapper +' '+ this.cfg.classes.invisible,
            id: this.getId(),
            children:[{
            tag: 'div',
            cls: 'curtain'
          },{
            tag: 'div',
            cls: this.cfg.classes.innerWrapper,
            children:[{
              tag: 'div',
              cls: this.cfg.classes.buttonWrapper
            },{
              tag: 'div',
              cls: this.cfg.classes.contentWrapper
            }]
          }]
        };
        var rootNode = this.cfg.rootNode || this.cfg.elements.body.dom;
        this.dom.manip.append( rootNode, overlay);
        this.appendToolbar('#'+this.getId());
        try {
            wrap = Ext.get(this.getId()).select('.'+this.cfg.classes.contentWrapper);
            clone = this.cfg.elements.toOverlay.elements[0];
            cloneId = !!clone.getAttribute('id')?clone.getAttribute('id') : this.getId() + this.id;
            clone.setAttribute('id',clone.getAttribute('id') + '-in-overlay');
            Ext.get(clone).addCls('in-overlay').removeCls('hidden');
            wrap.appendChild(clone);
            this.toggle();
        } catch (err) {
            if (console && console.warn) {
                console.warn('ERROR', err);
            }
        }
    },
    
    appendToolbar: function(overlayID){
        var overlay = Ext.get(this.dom.query('.'+this.cfg.classes.buttonWrapper, this.getId())[0]),
            buttonsList = '<ul class="overlay-toolbar" id="overlay-'+this.id+'-toolbar"></ul>',
            buttonTemplate = '<li id="overlay-{overlay}-button-{id}" class="overlay-{overlay} modal-dialog-overlay-button {cls}">\
            <a href="{href}" title="{text}"><span>{text}</span></a>\
            </li>\
            ', toolbarID = 'overlay-'+this.id+'-toolbar';
        overlay.update(buttonsList);
        for(var i=0; i < this.cfg.buttons.length; i++){
            var buttonObj = this.cfg.buttons[i],
            button = new Ext.Template(buttonTemplate),
            ref = button.append(toolbarID, {
                cls: buttonObj.cls,
                text: buttonObj.text,
                href:'javascript:;',
                overlay:this.id,
                id: i
            });
            if(buttonObj.click){
                Ext.get('overlay-'+this.id+'-button-'+i).on('click', buttonObj.click);
            }
        }    
    },
    toggle: function(){
        if(this.isVisible) {
            this.modalHide();
        } else {
            this.modalShow();
        }
    }
});

Ext.define('Ext.EMC.ModalDisclaimer', {
    extend: 'Ext.EMC.Modal',
    constructor: function(params){
        var config= {};
        Ext.apply(config, params, {
            autoCenter:{
              vert:true,
              horz:true
            },
            width: 770,
            height: 430,
            buttons: [{
                text: 'Close',
                cls: 'close-btn-small'
            }],
            closeOnOverlayClick: true,
            destroy: false,
            
            // modal variables
            disclaimerId : 'disclaimerId',
            disclaimerTitle : 'disclaimerTitle',
            disclaimerCloseText : 'disclaimerCloseText',
            disclaimerURL : '#',
            callBackFunction: 'callBackFunction'
        });
        
        // modal template
        var tpl = new Ext.Template([
            '<div id="{id}" class="hidden disclaimer-modal">',
            '<h2>{title}</h2>',
            '<div class="remote-text"></div>',
            '<a class="close-link" href="#">{closeText}</a>',
            '</div>'
        ]);
        
        var el = tpl.append( Ext.getBody(), {
            id : config.disclaimerId,
            title : config.disclaimerTitle,
            closeText : config.disclaimerCloseText
        });
        
        // call parent
        this.callParent([[el], config]);
        
        // grab text from url
        var self = this;
        Ext.Ajax.request({
            url: config.disclaimerURL,
            success: function(response, opts) {
    
                Ext.get(self.getId()).query('.remote-text').first().innerHTML = response.responseText;
    
			    if (config.callBackFunction != null){
			     config.callBackFunction();
			    }
            },
            failure: function(response, opts) {
                if (console && console.log) {
                    console.log('Failed to load url. CODE: ' + response.status);
                }
            }
        });
        
        // add close btns events
        var container = Ext.get(this.getId());
        container.select('.close-btn-small a, .close-link').on('click', function(ev) { 
            ev.stopEvent();
            self.modalHide();
        });
    }
});
