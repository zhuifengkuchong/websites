var $j = $j || jQuery.noConflict();

var EMC = EMC || {};
EMC.Components = EMC.Components || {}; 

/**
 * TypeAhead: class to create keyword suggestion 
 * type ahead functionality for input fields 
 * @param {Object}    required options                  Object containing all of the parameters
 * @param {Attribute} optional options.minChar          The minimum amount of characters typed before the results display
 * @param {Attribute} required options.wrapper          ID of dom object that wraps the input field
 * @param {Attribute} required options.url              Url that points to the web service to retrieve the keywords 
 * @param {Attribute} required options.keywordParam     Query 'key' parameter that is expected from the web service for the keyword value
 * @param {Object}    optional options.optionalParams   Optional parameters to be passed to the web service
 * @param {Attribute} required options.responseType     Response type for data returned from web service ("array" || "object"
 * @param {Attribute} optional options.responseProperty Response type for data returned from web service ("array" || "object"
 * @param {Attribute} optional options.dataId           Unique ID that is to be used and the input value instead of a string 
 * @param {Attribute} optional options.emptyResponse    Message to display if the response is empty 
 */
EMC.Components.TypeAhead = function(options){
  //Global keyboard key values
  this.keys = {
    arrowUp: 38,
    arrowDown: 40,
    enter: 13,
    esc: 27,
    tab: 9
  }

  //Set wrapper dom object for input wrapper
  this.$searchWrapper = $j("#" + options.wrapper);
  this.$input = $j("input", this.$searchWrapper).eq(0);
  this.$typeaheadContainer = $j(".type-ahead", this.$searchWrapper);
  
  //Set url to web service
  this.url = options.url;
  //set keyword parameter for web service
  this.keywordParam = options.keywordParam;

  //Build optional parameters
  if(typeof(options.optionalParams) != undefined){
    var _optionalQuery = [];
    for(key in options.optionalParams){
      _optionalQuery.push("&" +key+ "=" + options.optionalParams[key]);
    }
    this.optionalQuery = _optionalQuery.join("");   
  };

  //Set reponse type to determine what data should be parsed
  this.responseType = options.responseType,
  this.responseProperty = options.responseProperty || "";

  //Set unique id to determine data-value of the input
  this.dataId = options.uid || "",

  //Set the max number of keywords to display
  this.minChar = options.minChar || 1;

  //Set the message to display if the response is empty
  this.emptyResponse = options.emptyResponse || null;

  this.init();
};

EMC.Components.TypeAhead.prototype = {
  init: function(){
    this.$keywordsPrevVal = null;
    
    //set up events
    this.events();
  },
  events: function(){
      var that = this;

      that.$input.bind("keyup", $j.proxy(that.fetchTypeaheadList, that));

      //need to use keydown instead of keypress/keyup due to 
      //IE's behavour of unfocusing input on "enter"
      that.$input.bind("keydown", function(e){      
        that.navigateTypeahead(e);
      });

      //trigger selectTypeahead when li is clicked
      $j(".type-ahead li", this.$searchWrapper).live("click", function(e){
        that.selectTypeahead($j(e.currentTarget), e);
      });
  },
  fetchTypeaheadList: function(e){
    var that = this,
        keyVal = e.which,
        keywordsVal = that.$input.val();
        inputChanged = that.$keywordsPrevVal != keywordsVal;
      
      that.$keywordsPrevVal = keywordsVal;

      //Check to see if the input.keywords value has changed and the enter key was not pressed
      if(inputChanged && keyVal != that.keys.enter){
        
        //Check if type ahead has dataId option
        if(this.dataId !== ""){
          //Remove data-value from input
          that.$input.removeAttr("data-value");
        }

        if(keywordsVal.length >= this.minChar){          
          
          var data = function() {
              var _keywordQuery = that.keywordParam + "=" + encodeURIComponent(keywordsVal),
              _optionalQuery = typeof(that.optionalQuery) != undefined ? that.optionalQuery : "";
              return _keywordQuery + _optionalQuery;
          }

          //abort if ajax request is already in progress
          that.abortTypeahead();

          //set this.typeAheadRequest to ajax request and fetch new keywords
          that.typeAheadRequest = $j.ajax({
            type: "POST",
            dataType : "json",
            url: that.url,
            data: data(),
            success: function (data) {
              if(data.length > 0){
                $j.proxy(that.renderTypeahead(data), that);
              } else {
                //remove typeahead if the data is empty
                that.removeTypeahead();
              }
            },
            error: function(jqXHR, textStatus, errorThrown){
              if (that.emptyResponse !== null && textStatus === "parsererror") {
                $j.proxy(that.renderNoDataMessage(that.emptyResponse), that);
              } else {
                //remove typeahead if error
                that.removeTypeahead();
              }
              
            }
          });

        } else if (keywordsVal.length < this.minChar){
          //clear and remove typeahead
          that.removeTypeahead();
        };
      
      } else if(keyVal == that.keys.enter){
        var isSelectedKeyword = $j("li.active", this.$searchWrapper).length > 0;

        that.abortTypeahead();
        
        //check if any suggested keywords are actively highlighted
        if(!isSelectedKeyword){
          that.removeTypeahead();
        }
      }
      
  },
  /**
   * Check if ajax request is already in progress and abort if true
   */
  abortTypeahead: function(){
    var that = this;

    if(typeof(that.typeAheadRequest) != "undefined" && (that.typeAheadRequest.readyState > 0 && that.typeAheadRequest.readyState < 4)){
      //abort request
      that.typeAheadRequest.abort();
    };
  },
  /**
   * Render and show the typeahead dropdown
   */
  renderTypeahead: function(results){
      var that = this,
          results = results,
          compiledResults = '<ul>'+keywords()+'</ul>';
      
      //loop through keyword results and return html to append
      function keywords(){
          var keywordsHtml = '';
          //for each keyword return li
          for(var i=0; i < results.length; i++){
            var keyword = that.responseType === "string" ? results[i] : results[i][that.responseProperty],
                inputVal = that.$input.val(),
                inputValClean = $j.trim(inputVal),
                toMatch = new RegExp("(^\|[ \n\r\t.,'\"\+!?-]+)(" + that.regexEscape(inputValClean) + ")", "igm"),
                matchedString = keyword.replace(toMatch, '$1<span>$2</span>'),
                hasDataId = that.dataId !== "",
                dataIdAttr = hasDataId ? ' data-id="' + results[i][that.dataId] + '" ' : "";

           keywordsHtml += '<li' +dataIdAttr+ '>'+matchedString+'</li>';
          };

          return keywordsHtml;
      }; 

    if( !this.$typeaheadContainer.hasClass("open") ){
      this.bindBodyClick();
    }

    //show and render typeahead
    this.$typeaheadContainer
        .addClass("open")
        .html(compiledResults)
        .slideDown(100);
  },
  /**
   * Render and show the typeahead dropdown with the no data message
   */
  renderNoDataMessage: function(noDataMessage){
    console.log("renderNoDataMessage noDataMessage:", noDataMessage);
    var that = this,
        message = '<ul><li class="msg">'+noDataMessage+'</li></ul>';
    
    if( !this.$typeaheadContainer.hasClass("open") ){
      this.bindBodyClick();
    }

    //show and render typeahead
    this.$typeaheadContainer
        .addClass("open")
        .html(message)
        .slideDown(100);
  },
  /**
   * Remove typeahead drop down from the DOM
   */
  removeTypeahead: function(){
    this.$typeaheadContainer
        .slideUp(100)
        .html("")
        .removeClass("open");

    //reset stored $keywordsPrevVal
    this.$keywordsPrevVal = null;

    this.unbindBodyClick();
  },
  /**
   * Trigger removing the typeahead drop down if a user clicks outside of it
   * @param  {Object} e Event object that was triggered
   */
  closeTypeahead: function(e){
    var that = this,
        typeaheadIsOpen = this.$typeaheadContainer.hasClass("open"),
        targetIsTypeahead = $j(e.target).parents('.type-ahead').length > 0;

    if(typeaheadIsOpen && !targetIsTypeahead){
      that.removeTypeahead();
    }
  },
  /**
   * When a typeahead suggested keyword is selected, change the "input.keywords" 
   * value to the selected text and remove the typeahead dropdown from the DOM
   * @param  {String} $keyword Required: jQuery DOM element of selected keyword
   * @param  {Object} e        Optional: event object that was triggered
   */
  selectTypeahead: function($keyword, e){
    var that = this,
        $selectedKeyword = $keyword;
        $typeaheadItem = $j("li", that.$typeaheadContainer), 
        isNoResponseMsg = $typeaheadItem.hasClass("msg"),
        selectedKeywordText = $selectedKeyword.text(),
        hasDataId = typeof $selectedKeyword.attr('data-id') !== 'undefined';

    //Check if an event was triggered, if to prevent it from bubbling up
    if(typeof e != "undefined"){
      e.stopPropagation();  
    };

    //check to make sure the selected keyword is not blank
    if(selectedKeywordText != "" && !isNoResponseMsg){
      //insert selected type ahead to keywords input
      that.$input.val(selectedKeywordText);
      //add data-value attribute to input
      if(hasDataId){
        that.$input.attr("data-value", $selectedKeyword.attr("data-id")).attr("data-value", $selectedKeyword.attr("data-id"));
      } 
    }

    //close and remove type ahead
    that.removeTypeahead();
    
    //trigger change event that the input has changed values
    that.changeEvent();
  },
  /**
   * Navigate the typeahead drop down when rendered and open
   * @param  {Object} e Event object that was triggered
   */
  navigateTypeahead: function(e){
    var that = this,
        $typeaheadItem = $j("li", that.$typeaheadContainer), 
        typeaheadIsOpen = that.$typeaheadContainer.hasClass("open"),
        isNoResponseMsg = $typeaheadItem.hasClass("msg"),
        key = e.which,
        currentSelection = $j("li.active", that.$typeaheadContainer).index();
      
      if(typeaheadIsOpen && !isNoResponseMsg){
        switch (key){
          case that.keys.arrowUp: 
            //prevent default so that cursor doesn't jump to first position
            e.preventDefault();
            
            //increment currentSelection down
            currentSelection--;
            
            //check selection and highlight previous
            if(currentSelection < 0 && !that.$input.hasClass("active")){
              currentSelection = $typeaheadItem.length - 1;
            } else if(currentSelection < 0 && that.$input.hasClass("active")){
              currentSelection = $typeaheadItem.length - 1;
            };
            //highlight item currently selected
            that.highlightTypeaheadItem(currentSelection);           
            break;
          case that.keys.arrowDown:
            //increment currentSelection up
            currentSelection++;

            //check selection and highlight next
            if(currentSelection >= $typeaheadItem.length && !that.$input.hasClass("active")){
              currentSelection = 0;
            } else if(currentSelection >= $typeaheadItem.length && that.$input.hasClass("active")){
              currentSelection = 0;
            };
            
            //highlight item currently selected
            that.highlightTypeaheadItem(currentSelection);
            break;
          case that.keys.enter:
            var $selectedKeyword = $j("li.active", this.$searchWrapper);

            if($selectedKeyword.length > 0){
              e.preventDefault();
            }

            //select highlighted keyword when enter key is pressed
            that.selectTypeahead($selectedKeyword);

            break;
          case that.keys.esc:
          case that.keys.tab:
            //remove typeahead when escape or tab key is pressed
            that.removeTypeahead();
            break;
          default:
            break;
        };
      };
  },
  /**
   * Highlight currently selected item based its index number
   * @param [Number] idx Current highlighted typeahead item's index number
   */
  highlightTypeaheadItem: function(idx){
    var that = this,
        $typeaheadItem = $j("li", that.$typeaheadContainer);

    $typeaheadItem.removeClass("active");
    if(idx < 0){
      that.$input.val();
      that.$input.addClass("active");
    } else {  
      that.$input.removeClass("active");
      $typeaheadItem.eq(idx).addClass("active");  
    };
    that.scollTypeahead();
  },
  scollTypeahead: function(){
    var that = this,
        containerHeight = that.$typeaheadContainer.height(),
        containerScrollPos = that.$typeaheadContainer.scrollTop(),
        $item = $j("li.active", that.$typeaheadContainer),
        itemHeight = $item.outerHeight(),
        itemOffsetTop = $item.position().top + containerScrollPos,
        itemOffsetBottom = itemOffsetTop + itemHeight,
        isBelowView = (containerHeight + containerScrollPos) < itemOffsetBottom;
        isAboveView = (containerScrollPos) > itemOffsetTop;

    if(isBelowView){
       //scroll down
       that.$typeaheadContainer.scrollTop(itemOffsetBottom - containerHeight);
    } else if(isAboveView){
      //scroll up
      that.$typeaheadContainer.scrollTop(itemOffsetTop);
    }
  },
  /**
   * Broadcast event that the input has changed values
   */
  changeEvent: function(){
    this.$input.trigger("inputChanged");
  },  
  bindBodyClick: function(){
    $j("body").bind("click", $j.proxy(this.closeTypeahead, this));
  },
  unbindBodyClick: function(){
    $j("body").unbind("click", $j.proxy(this.closeTypeahead, this));
  },
  regexEscape: function(s) {
    return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
  }
};

/**
 * EMC.Components.Events: custom events object for components
 */
EMC.Components.Events = EMC.Components.Events || {};
EMC.Components.Events.broadcaster = EMC.Components.Events.broadcaster || $j(new Object());
EMC.Components.Events.typeAhead = {};
EMC.Components.Events.typeAhead.submitted = 'typeAheadSubmitted';
