var BCL = {}, type = "";
BCL.onTemplateLoad = function(experienceID) {
    BCL.player = brightcove.api.getExperience(experienceID), BCL.APIModules = brightcove.api.modules.APIModules, 
    BCL.adEvent = brightcove.api.events.AdEvent, BCL.captionsEvent = brightcove.api.events.CaptionsEvent, 
    BCL.contentEvent = brightcove.api.events.ContentEvent, BCL.cuePointEvent = brightcove.api.events.CuePointEvent, 
    BCL.mediaEvent = brightcove.api.events.MediaEvent;
}, BCL.onTemplateReady = function() {
    BCL.videoPlayer = BCL.player.getModule(BCL.APIModules.VIDEO_PLAYER), BCL.videoPlayer.addEventListener(brightcove.api.events.MediaEvent.PROGRESS, BCL.videoProgress), 
    $(".hero-feature").remove();
}, $(".hero.video .btn-primary").click(function() {
    return BCL.startHeroVideo(), !1;
}), $(".video .play").click(function() {
    return BCL.startInlineVideo(), !1;
}), BCL.videoProgress = function(e) {
    e.position > e.duration - .1 && BCL.videoComplete();
}, BCL.startHeroVideo = function() {
    type = "hero";
    var $img = $(".hero.video .image-holder"), $call = $(".hero.video .hero-callout"), $vid = $(".hero.video .video-holder");
    $("body, html").animate({
        scrollTop: 0
    }, 300), $call.animate({
        opacity: 0
    }, 400, function() {
        $call.hide();
    }), setTimeout(function() {
        $img.animate({
            opacity: 0
        }, 400), $vid.animate({
            opacity: 1
        }, 400);
    }, 1e3), BCL.videoPlayer.play();
}, BCL.startInlineVideo = function() {
    if (BCL.player == undefined) {
        BCL.onTemplateLoad();
    }
    if (BCL.videoPlayer == undefined) {
        BCL.onTemplateReady();
    }
    type = "inline";
    var $img = $(".video .still"), $call = $(".video .callout"), $vid = $(".video .video-holder"), $play = $(".video .play");
    setTimeout(function() {
        $img.animate({
            opacity: 0
        }, 400), $call.animate({
            opacity: 0
        }, 400, function() {
            $call.hide(), $play.hide();
        }), $vid.animate({
            opacity: 1
        }, 400);
    }, 1e3), BCL.videoPlayer.play();
}, BCL.videoComplete = function() {
    if ("inline" == type) var $img = $(".video .still"), $call = $(".video .callout"), $vid = $(".video .video-holder"); else var $img = $(".hero.video .image-holder"), $call = $(".hero.video .hero-callout"), $vid = $(".hero.video .video-holder");
    BCL.videoPlayer.pause(), $img.animate({
        opacity: 1
    }, 400, function() {
        BCL.videoPlayer.seek(0), BCL.videoPlayer.pause();
    }), $vid.animate({
        opacity: 0
    }, 400), setTimeout(function() {
        $call.show(), $call.animate({
            opacity: 1
        }, 400);
    }, 600);
};