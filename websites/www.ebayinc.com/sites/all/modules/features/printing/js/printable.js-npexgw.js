(function($) {
  $(document).ready(function() {
    $('.printable').click(function(e) {
      var URL = $(this).attr('href');
      var name = "Printable";

      var height = screen.height;
      var width = screen.width * 0.5;

      var left = (screen.width/2)-(width*.6);
      var top = (screen.height/2)-(height*.08);

      var specs = 'width='+width+', left='+left+', top='+top+', scrollbars=1,status=no,menubar=no,titlebar=no';
      var replace = false;

      window.open(URL,name,specs,replace);

      e.preventDefault();
      return false;
    });
  });
})(jQuery);