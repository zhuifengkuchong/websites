(function($) {
    Drupal.behaviors.ebaySocialLinks = {
        attach: function(context) {
            this.init();
            this.runFacebook();
            this.runTwitter();
            this.runPinterest();
        },
        runFacebook: function() {
            var Parent = this;
            $.each($('.social-share-facebook').not('.ebay-shareable'), function( index, element ) {
                var hrefContext, attributes;
                hrefContext = {
                    url: Parent.qualifyURL($(element).attr('href'))
                };
                if (typeof $(this).data('shareText') != 'undefined') {
                    hrefContext.description = $(this).data('shareText');
                }
                if (typeof $(this).data('shareTitle') != 'undefined') {
                    hrefContext.name = $(this).data('shareTitle');
                }
                if (typeof $(this).data('shareSubtitle') != 'undefined') {
                    hrefContext.caption = $(this).data('shareSubtitle');
                }
                if (typeof $(this).data('sharePicture') != 'undefined') {
                    hrefContext.picture = $(this).data('sharePicture');
                }
                var attributes = {
                    href: Parent.settings.facebook.shareUrl(hrefContext)
                };
                Parent.updateShareLink(element, attributes);
            });
        },
        runTwitter: function() {
            var Parent = this;
            var settings = this.settings.twitter;
            $.each($('.social-share-twitter').not('.ebay-shareable'), function( index, element ) {
                var hrefContext, attributes, shareText;
                shareText = $(this).data('shareText');
                if (typeof shareText == 'undefined') {
                    shareText = Parent.settings.twitter.default_text;
                }
                hrefContext = {
                    text: encodeURIComponent(shareText),
                    url: Parent.qualifyURL($(element).attr('href'))
                };
                var attributes = {
                    href: Parent.settings.twitter.shareUrl(hrefContext)
                };
                Parent.updateShareLink(element, attributes);
            });
        },
        runPinterest: function() {
            var Parent = this;
            $.each($('.social-share-pinterest').not('.ebay-shareable'), function( index, element ) {
                var url, hrefContext, attributes;

                shareText = $(this).data('shareText');
                if (typeof shareText == 'undefined') {
                    shareText = Parent.settings.pinterest.default_text;
                }

                shareMedia = $(element).data('shareImageUrl');
                if (typeof shareMedia == 'undefined') {
                    shareMedia = Parent.settings.pinterest.default_image;
                }
                hrefContext = {
                    media_url: shareMedia,
                    text: encodeURIComponent(shareText),
                    url: Parent.qualifyURL($(element).attr('href'))
                };
                var attributes = {
                    href: Parent.settings.pinterest.shareUrl(hrefContext)
                };
                Parent.updateShareLink(element, attributes);
            });
        },
        init: function() {
            var Parent = this;
            this.settings = Drupal.settings.Ebay.SocialLinks;
            // setup shareUrl template
            var sites = this.settings.sites;
            $.each(sites, function( index, element ) {
                if (typeof Parent.settings[index] == 'undefined') {
                    Parent.settings[index] = element;
                }
                if (typeof element.url_template == 'string' && element.url_template.length > 0) {
                    Parent.settings[index].shareUrl = Handlebars.compile(element.url_template);
                }

            });
        },
        updateShareLink: function(element, context) {

            $element = $(element);
            context.target = "_blank";

            // if the parent is an iframe, lets just do it up in the iframe!
            var frameContainer = $element.parents('#universe-pop-container');
            if (typeof frameContainer == 'undefined' || frameContainer.length == 0) {
                // no it's not an iframe
            }

            return $(element).attr(context).addClass('ebay-shareable');
        },
        /**
         * Ensure url is absolute - really cool trick!
         * Snippet from http://james.padolsey.com/javascript/getting-a-fully-qualified-url/
         */
        qualifyURL: function(url){
            var check = document.createElement('iframe');
            check.src = url; // set string url
            url = check.src; // get qualified url
            check.src = null; // no server request
            return url;
        }
    }
})(jQuery);



/**
 * BEHAVIOR PLUGINS & LIBRARY CODE
 */


/**
 * console.sprintf taken from https://gist.github.com/Calvein/2304021
 */
if(typeof(console) != 'undefined') {
    !function(a){var b=function(){function a(a){return Object.prototype.toString.call(a).slice(8,-1).toLowerCase()}var c=function(a,b,c){a+="",b=~~b;for(var d=[];b>0;d[--b]=a);return d.join(c==null?"":c)},d=function(){return d.cache.hasOwnProperty(arguments[0][0])||(d.cache[arguments[0][0]]=d.parse(arguments[0][0])),d.format.call(null,d.cache[arguments[0][0]],arguments[0])};return d.format=function(d,e){var f=1,g=d.length,h="",i,j=[],k,l,m,n,o,p;for(k=0;k<g;k++){h=a(d[k]);if(h==="string")j.push(d[k]);else if(h==="array"){m=d[k];if(m[2]){i=e[f];for(l=0;l<m[2].length;l++){if(!i.hasOwnProperty(m[2][l]))throw new Error(b('[console.sprintf] property "%s" does not exist',m[2][l]));i=i[m[2][l]]}}else m[1]?i=e[m[1]]:i=e[f++];if(/[^s]/.test(m[8])&&a(i)!="number")throw new Error(b("[console.sprintf] expecting number but found %s",a(i)));switch(m[8]){case"b":i=i.toString(2);break;case"c":i=String.fromCharCode(i);break;case"d":i=parseInt(i,10);break;case"e":i=m[7]?i.toExponential(m[7]):i.toExponential();break;case"f":i=m[7]?parseFloat(i).toFixed(m[7]):parseFloat(i);break;case"o":i=i.toString(8);break;case"s":i=(i=String(i))&&m[7]?i.substring(0,m[7]):i;break;case"u":i=Math.abs(i);break;case"x":i=i.toString(16);break;case"X":i=i.toString(16).toUpperCase()}i=/[def]/.test(m[8])&&m[3]&&i>=0?"+"+i:i,o=m[4]?m[4]=="0"?"0":m[4].charAt(1):" ",p=m[6]-String(i).length,n=m[6]?c(o,p):"",j.push(m[5]?i+n:n+i)}}if(typeof e=="string")return e;var q=j.length;return j.join("").split().concat([].filter.call(e,function(a,b){if(b>=q)return a}))},d.cache={},d.parse=function(a){var b=a,c=[],d=[],e=0;while(b){if((c=/^[^\x25]+/.exec(b))!==null)d.push(c[0]);else if((c=/^\x25{2}/.exec(b))!==null)d.push("%");else{if((c=/^\x25(?:([1-9]\d*)\$|\(([^\)]+)\))?(\+)?(0|'[^$])?(-)?(\d+)?(?:\.(\d+))?([b-fosuxX])/.exec(b))===null)throw new Error("[console.sprintf] huh?");if(c[2]){e|=1;var f=[],g=c[2],h=[];if((h=/^([a-z_][a-z_\d]*)/i.exec(g))===null)throw new Error("[console.sprintf] huh?");f.push(h[1]);while((g=g.substring(h[0].length))!=="")if((h=/^\.([a-z_][a-z_\d]*)/i.exec(g))!==null)f.push(h[1]);else{if((h=/^\[(\d+)\]/.exec(g))===null)throw new Error("[console.sprintf] huh?");f.push(h[1])}c[2]=f}else e|=2;if(e===3)throw new Error("[console.sprintf] mixing positional and named placeholders is not (yet) supported");d.push(c)}b=b.substring(c[0].length)}return d},d}();a.sprintf=function(){a.log.apply(a,b(arguments))}}(console);
}
/**
 sprintf() for JavaScript 0.7-beta1
 http://www.diveintojavascript.com/projects/javascript-sprintf

 Copyright (c) Alexandru Marasteanu <alexaholic [at) gmail (dot] com>
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * Neither the name of sprintf() for JavaScript nor the
 names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Alexandru Marasteanu BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


 Changelog:
 2010.09.06 - 0.7-beta1
 - features: vsprintf, support for named placeholders
 - enhancements: format cache, reduced global namespace pollution

 2010.05.22 - 0.6:
 - reverted to 0.4 and fixed the bug regarding the sign of the number 0
 Note:
 Thanks to Raphael Pigulla <raph (at] n3rd [dot) org> (http://www.n3rd.org/)
 who warned me about a bug in 0.5, I discovered that the last update was
 a regress. I appologize for that.

 2010.05.09 - 0.5:
 - bug fix: 0 is now preceeded with a + sign
 - bug fix: the sign was not at the right position on padded results (Kamal Abdali)
 - switched from GPL to BSD license

 2007.10.21 - 0.4:
 - unit test and patch (David Baird)

 2007.09.17 - 0.3:
 - bug fix: no longer throws exception on empty paramenters (Hans Pufal)

 2007.09.11 - 0.2:
 - feature: added argument swapping

 2007.04.03 - 0.1:
 - initial release
 **/

var sprintf=function(){function e(e){return Object.prototype.toString.call(e).slice(8,-1).toLowerCase()}function t(e,t){for(var n=[];t>0;n[--t]=e){}return n.join("")}var n=function(){if(!n.cache.hasOwnProperty(arguments[0])){n.cache[arguments[0]]=n.parse(arguments[0])}return n.format.call(null,n.cache[arguments[0]],arguments)};n.format=function(n,r){var i=1,s=n.length,o="",u,a=[],f,l,c,h,p,d;for(f=0;f<s;f++){o=e(n[f]);if(o==="string"){a.push(n[f])}else if(o==="array"){c=n[f];if(c[2]){u=r[i];for(l=0;l<c[2].length;l++){if(!u.hasOwnProperty(c[2][l])){throw sprintf('[sprintf] property "%s" does not exist',c[2][l])}u=u[c[2][l]]}}else if(c[1]){u=r[c[1]]}else{u=r[i++]}if(/[^s]/.test(c[8])&&e(u)!="number"){throw sprintf("[sprintf] expecting number but found %s",e(u))}switch(c[8]){case"b":u=u.toString(2);break;case"c":u=String.fromCharCode(u);break;case"d":u=parseInt(u,10);break;case"e":u=c[7]?u.toExponential(c[7]):u.toExponential();break;case"f":u=c[7]?parseFloat(u).toFixed(c[7]):parseFloat(u);break;case"o":u=u.toString(8);break;case"s":u=(u=String(u))&&c[7]?u.substring(0,c[7]):u;break;case"u":u=Math.abs(u);break;case"x":u=u.toString(16);break;case"X":u=u.toString(16).toUpperCase();break}u=/[def]/.test(c[8])&&c[3]&&u>=0?"+"+u:u;p=c[4]?c[4]=="0"?"0":c[4].charAt(1):" ";d=c[6]-String(u).length;h=c[6]?t(p,d):"";a.push(c[5]?u+h:h+u)}}return a.join("")};n.cache={};n.parse=function(e){var t=e,n=[],r=[],i=0;while(t){if((n=/^[^\x25]+/.exec(t))!==null){r.push(n[0])}else if((n=/^\x25{2}/.exec(t))!==null){r.push("%")}else if((n=/^\x25(?:([1-9]\d*)\$|\(([^\)]+)\))?(\+)?(0|'[^$])?(-)?(\d+)?(?:\.(\d+))?([b-fosuxX])/.exec(t))!==null){if(n[2]){i|=1;var s=[],o=n[2],u=[];if((u=/^([a-z_][a-z_\d]*)/i.exec(o))!==null){s.push(u[1]);while((o=o.substring(u[0].length))!==""){if((u=/^\.([a-z_][a-z_\d]*)/i.exec(o))!==null){s.push(u[1])}else if((u=/^\[(\d+)\]/.exec(o))!==null){s.push(u[1])}else{throw"[sprintf] huh?"}}}else{throw"[sprintf] huh?"}n[2]=s}else{i|=2}if(i===3){throw"[sprintf] mixing positional and named placeholders is not (yet) supported"}r.push(n)}else{throw"[sprintf] huh?"}t=t.substring(n[0].length)}return r};return n}();var vsprintf=function(e,t){t.unshift(e);return sprintf.apply(null,t)}