(function($) {
  
    Drupal.behaviors.ERSlideshow = {

        attach: function (context) {
            var self = this;
            
            $('.ER-slideshow', context).once('ER-slideshow',function(){

                var children = $('li', this);

                if (children.length > 1) {
                    $(this).flexslider();

		        
	            } else if (children.length === 1) {

	            }

                var slider = $(this).flexslider({
                    selector: ".field-items li",
                    startAt: 0,
                    slideshow: true,
                    initDelay: 5,
                    keyboard: true,
                    animation: 'slide',
                    contralNav: true,
                    directionNav: true,
                    touch: true,
                    pauseOnAction: true,
                    pauseOnHover: true,
                    slideshowSpeed: 7000,
                    useCSS: false,
                    video: true

                });
                                  
	        });
        }
        

        
    } // Behavior
})(jQuery);