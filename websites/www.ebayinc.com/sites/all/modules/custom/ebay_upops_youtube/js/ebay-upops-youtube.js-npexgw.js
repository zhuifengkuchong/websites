(function($) {

$(window).load(function() {
  Drupal.behaviors.EbayYoutubeThumbs.attachThumbHovers();
});

Drupal.behaviors.EbayYoutubeThumbs = {

  attachThumbHovers: function() {
    var Parent = this;
    this.settings = Drupal.settings.Ebay.YoutubePopups;

    // Add New Markup & Update Links
    $.each($('.video-popup-thumb'), function(e) {
      if ($(this).data('videoHoverAlways')) {
        Parent.settings.hover_always = $(this).data('videoHoverAlways');
      }
      // get/use first image found if we're wrapping
      $image = Parent.findImage(this, 0);

      if (typeof $image != 'undefined') {

        var position;
        var source = {};
        var buffer = 5;
        $image.addClass('hovering');
        source.position = Parent.XY($image);
        source.width = $image.width();
        source.height = $image.height();

        // ok lets add hover div
        hoverDiv = $(document.createElement('div'));
        divAttr = {
          'class': 'play-icon'
        };     
		timeLabel = $(document.createElement('div'));
        timeAttr = {
          'class': 'time-label'
        };
        divCss = {
          backgroundImage: 'url('+Parent.settings.hover_image+')',
          // backgroundRepeat: 'no-repeat',
          // backgroundPosition: 'center center',
          // marginLeft: '0px',
          // marginTop: -1 * (source.height+buffer),
          // width: source.width,
          // height: source.height,
          // position: 'relative',
          // opacity: 0
        }
        if (typeof Parent.settings != 'undefined') {
          
          if (typeof Parent.settings.enable_hover != 'undefined') {
            var hover_enable = Parent.settings.enable_hover;
            if (hover_enable != 1) {
              divCss.opacity = 1;
            }
          }

          if (typeof Parent.settings.enable_hover_dim != 'undefined') {
            if (Parent.settings.enable_hover_dim == 1) {
              divCss.backgroundImage = divCss.backgroundImage+', url('+Parent.settings.module_path+'/img/trans-50-black.png)';
            }
          }
          divData = Parent.settings;
        }
        $(hoverDiv).attr(divAttr).css(divCss).data(divData);
        $(timeLabel).attr(timeAttr).data(divData);
        $($image).after(hoverDiv); 
		$($image).after(timeLabel);
      }
    });

    // Attach Hover Events
    $('body')
      .on('mouseover', '.play-icon', function(e) {
        if (typeof $(this).data().enable_hover == 'undefined' || $(this).data().enable_hover > 0) {
          $(this).stop().animate({opacity: 1});
        }
      })
      .on('mouseout', '.play-icon', function(e) {
        if (typeof $(this).data().enable_hover == 'undefined' || $(this).data().enable_hover > 0) {
          $(this).stop().animate({opacity: 0});
        }
      });
    //   .on('click', '.play-icon', function(e) {
    //     $(this).prev().click();
    // });
  },
  /**
   * check the hovered element for enclosing (or inherint) image
   * @param  int index which image do you want? (0 = first)
   * @return object image (but not jquery)
   */
  findImage: function(element, index) {
    var image;
    if (element.tagName == 'IMG') {
      image = $(element);
    } else {
      // ok it's a link, lets check if an image is inside we can use
      // warning might be a little funky.
      if (element.tagName == 'A') {
        child_image = $('img', element);
        if (child_image.length > 0) {
          image = $(child_image[index]);
        }
      }
    }
    return image;
  },
  /**
   * Snipped from http://ckon.wordpress.com/2011/08/05/javascript-position-firefox/
   * @param {[type]} o [description]
   */
  XY: function(o) {
    var z=o, x=0,y=0, c; 
    while(z && !isNaN(z.offsetLeft) && !isNaN(z.offsetTop)) {        
    c = isNaN(window.globalStorage)?0:window.getComputedStyle(z,null); 
    x += z.offsetLeft-z.scrollLeft+(c?parseInt(c.getPropertyValue('border-left-width'),10):0);
    y += z.offsetTop-z.scrollTop+(c?parseInt(c.getPropertyValue('border-top-width'),10):0);
    z = z.offsetParent;
    } 
    return {x:o.X=x,y:o.Y=y};
  }
}
})(jQuery);