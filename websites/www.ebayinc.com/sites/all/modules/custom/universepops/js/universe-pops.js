(function($) {
Drupal.behaviors.universePops = {
  settings: {},
  attach: function(context) {
    if (typeof jQuery.colorbox != 'undefined')  {
      this.settings = Drupal.settings.universePops;
      Parent = this;
      // listen to custom elements
      $(document).on('click', '.cboxCustomClose', function(e) {
        $.colorbox.close();
        e.preventDefault();
      });
      // lets mark video popups
      $('*[data-video-node]').addClass('video-popup-thumb')

      jQuery(".upop-link", context).click(function(e) {
        Parent.trigger_popup(this, e);
      });
    }
  },
  trigger_popup: function(element, e) {
    e.preventDefault();
    Drupal.behaviors.universePops.active = element;

    var frameOptions = {
      iframe: true,
      fixed: true
    };

    // defaults
    frameOptions.width = "990px";
    frameOptions.height = "605px";
    frameOptions.scrolling = false;

    var nodeId, nodeUrl, viewUrl;

    nodeId = jQuery(element).data('videoNode');
    if (typeof nodeId != 'undefined') {
      nodeUrl = '/node/'+nodeId;
    } else {
      nodeUrl = jQuery(element).data('popupNodeUrl');
    }

    viewUrl = jQuery(element).data('viewUrl');

    if (typeof viewUrl != 'undefined') {

      frameOptions.width = "950px";
      frameOptions.height = "90%";
      var frameUrl = viewUrl;

    } else if (typeof nodeUrl != 'undefined') {
      
      var frameUrl = "/" + this.settings.iframeUrl + "?url=" + nodeUrl;

    } else {
      frameOptions.width = "950px";
      frameOptions.height = "90%";
      frameOptions.scrolling = true;
      var frameUrl = $(element).attr('href');
    }

    frameOptions.href = frameUrl;

    frameOptions.onOpen = function() { /* do nothing */ };
    frameOptions.onComplete = function() {
      $('#colorbox').append('<span class="cboxCustomClose"></span>');
    };
    frameOptions.onCleanup = function() { /* do nothing */ };
    frameOptions.onClosed = function() { /* do nothing */ };
    frameOptions.onLoad = function() {
      
    };

    jQuery.colorbox(frameOptions);
  }
}
})(jQuery);