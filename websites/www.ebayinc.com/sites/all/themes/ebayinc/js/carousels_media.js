// ugly redundant code, but works.
function createCarousels(){
	if (jQuery('.prod_carousel').length > 0) jQuery('.prod_carousel').carouFredSel({
		auto: false,
		responsive: true,
		width: '100%',
		pagination: "#pager1",
		prev: '#prev1',
		next: '#next1',
		items: {
			width: 320,
			visible: {
				min: 1,
				max: 3
			}
		}			
	});
    if (jQuery('.brol1_carousel').length > 0) jQuery('.broll_carousel').carouFredSel({
		auto: false,
		responsive: true,
		width: '100%',
		pagination: "#pager2",
		prev: '#prev2',
		next: '#next2',
		items: {
			width: 320,
			visible: {
				min: 1,
				max: 3
			}
		}			
	});
    if (jQuery('.campusphotos_carousel').length > 0) jQuery('.campusphotos_carousel').carouFredSel({
		auto: false,
		responsive: true,
		width: '100%',
		pagination: "#pager3",
		prev: '#prev3',
		next: '#next3',
		items: {
			width: 320,
			visible: {
				min: 1,
				max: 3
			}
		}			
	});
    if (jQuery('.spokespeople_carousel').length > 0) jQuery('.spokespeople_carousel').carouFredSel({
		auto: false,
		responsive: true,
		width: '100%',
		pagination: "#pager4",
		prev: '#prev4',
		next: '#next4',
		items: {
			width: 320,
			height: 320,
			visible: {
				min: 1,
				max: 3
			}
		}			
	});
    if (jQuery('.factsheets_carousel').length > 0) jQuery('.factsheets_carousel').carouFredSel({
		auto: false,
		direction: 'top',
		responsive: false,
		pagination: "#pager5",
		width: '100%',
		items: {
			width: 249,
			visible: 3
		}
		
	});
    if (jQuery('.licensing_carousel').length > 0) jQuery('.licensing_carousel').carouFredSel({
		auto: false,
		direction: 'top',
		responsive: false,
		pagination: "#pager6",
		width: '100%',
		items: {
			width: 249,
			visible: 3
		}
		
	});			
	if (jQuery(window).width() < 481) {
        if (jQuery('.factsheets_carousel').length > 0) jQuery('.factsheets_carousel').carouFredSel({
			auto: false,
			direction: 'top',
			responsive: 'false',
			scroll: 1,
			pagination: "#pager5",
			items: {
				width: 320,
				visible: 3
			}			
		});
        if (jQuery('.licensing_carousel').length > 0) jQuery('.licensing_carousel').carouFredSel({
			auto: false,
			direction: 'top',
			responsive: 'false',
			scroll: 1,
			pagination: "#pager6",
			items: {
				width: 320,
				visible: 3
			}			
		});
	}
	if (jQuery(window).width() > 481 && jQuery(window).width() < 769) {
        if (jQuery('.factsheets_carousel').length > 0) jQuery('.factsheets_carousel').carouFredSel({
			auto: false,
			direction: 'left',
			responsive: true,
			pagination: "#pager5",
			items: {
				width: 268,
				visible: {
					min: 1,
					max: 3
				},
				height: 'auto'
			}		
		});
        if (jQuery('.licensing_carousel').length > 0) jQuery('.licensing_carousel').carouFredSel({
			auto: false,
			direction: 'left',
			responsive: true,
			pagination: "#pager6",
			items: {
				width: 268,
				visible: 3,
				height: 'auto'
			}		
		});	
	}	
}
jQuery(document).ajaxComplete(function() {
	//createCarousels();
});
jQuery(function() {
	jQuery("#edit-submitted-opportunity-information-if-other-please-specify").attr("placeholder", "If other, please specify."); // not necessary to put in separate file. Makes placeholder 
	var s = '<a href="http://www.ebayinc.com/ebay/blog">> View eBay Inc Blog</a>';
	var div = document.createElement('div');
	div.innerHTML = s;
	jQuery(".page-node-3876 .heading").append(div);
	jQuery(div).addClass("bloglink");
	createCarousels();	
});