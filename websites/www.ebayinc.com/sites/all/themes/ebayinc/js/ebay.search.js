(function($) {
    Drupal.behaviors.search = {
        attach: function(context) {
      
            // Init the search once
            $('#header', context).once('search', function(){
                var searchBlock = $('#block-views-exp-search-page-search', this),
                icon = $('.search-icon', this);  
      

        
                // Toggle Visibility and class when icon is clicked  
                icon.click(function(){
                    searchBlock.slideToggle("fast");
                    icon.toggleClass('active');  
                });    
            });

            $('#views-exposed-form-search-page-search #edit-keywords').css('background',"#fff url(/sites/all/themes/ebayinc/images/search-input-icon.jpg) no-repeat 19px 16px").val('');
            $('#views-exposed-form-search-page-search #edit-keywords').focus(function() {
                $('#views-exposed-form-search-page-search #edit-keywords').css('background',"#fff");
            });
            $('#views-exposed-form-search-page-search #edit-keywords').blur(function() {
                if (this.value == '') {
                    $('#views-exposed-form-search-page-search #edit-keywords').css('background',"#fff url(/sites/all/themes/ebayinc/images/search-input-icon.jpg) no-repeat 19px 16px").val('');
                }

            });

        }
    };
})(jQuery);