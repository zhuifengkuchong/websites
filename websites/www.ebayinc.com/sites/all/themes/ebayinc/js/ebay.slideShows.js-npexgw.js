(function($) {
  Drupal.behaviors.slideShows = {
    attach: function(context) {
        if (!context) return;
        var divSlideshow1 = jQuery('div.slideshow', context);
        if (divSlideshow1 && divSlideshow1.length >0)
            divSlideshow1.fadeGallery({
        slides: 'div.slide',
        btnPrev: 'a.btn-prev',
        btnNext: 'a.btn-next',
        pagerLinks: '.pagination li',
        event: 'click',
        autoRotation: false,
        switchTime: 3000,
        animSpeed: 500
      });
        var divSlideshow2 = jQuery('div.slideshow-articles', context);
        if (divSlideshow2 && divSlideshow2.length >0)
            divSlideshow2.fadeGallery({
        slides: '.slide',
        btnPrev: 'a.btn-prev',
        btnNext: 'a.btn-next',
        pagerLinks: '.pagination li',
        event: 'click',
        autoRotation: false,
        autoHeight: true,
        switchTime: 3000,
        animSpeed: 500
      });
        var divSlideshow3 = jQuery('div.slideshow-photos', context);
        if (divSlideshow3 && divSlideshow3.length >0)
            divSlideshow3.fadeGallery({
        slides: 'div.slide',
        btnPrev: 'a.btn-prev',
        btnNext: 'a.btn-next',
        pagerLinks: '.pagination li',
        event: 'click',
        autoRotation: false,
        switchTime: 3000,
        animSpeed: 500
      });
    }
  };
})(jQuery);

/** PLUGINS **/

(function(e){function t(t){this.options=e.extend({slides:"ul.slideset > li",activeClass:"active",disabledClass:"disabled",btnPrev:"a.btn-prev",btnNext:"a.btn-next",generatePagination:false,pagerList:"<ul>",pagerListItem:'<li><a href="#"></a></li>',pagerListItemText:"a",pagerLinks:".pagination li",currentNumber:"span.current-num",totalNumber:"span.total-num",btnPlay:".btn-play",btnPause:".btn-pause",btnPlayPause:".btn-play-pause",galleryReadyClass:"gallery-js-ready",autorotationActiveClass:"autorotation-active",autorotationDisabledClass:"autorotation-disabled",autorotationStopAfterClick:false,circularRotation:true,switchSimultaneously:true,disableWhileAnimating:false,disableFadeIE:false,autoRotation:false,pauseOnHover:true,autoHeight:false,useSwipe:false,switchTime:4e3,animSpeed:600,event:"click"},t);this.init()}t.prototype={init:function(){if(this.options.holder){this.findElements();this.initStructure();this.attachEvents();this.refreshState(true);this.autoRotate();this.makeCallback("onInit",this)}},findElements:function(){this.gallery=e(this.options.holder).addClass(this.options.galleryReadyClass);this.slides=this.gallery.find(this.options.slides);this.slidesHolder=this.slides.eq(0).parent();this.stepsCount=this.slides.length;this.btnPrev=this.gallery.find(this.options.btnPrev);this.btnNext=this.gallery.find(this.options.btnNext);this.currentIndex=0;if(this.options.disableFadeIE&&e.browser.msie&&e.browser.version<9){this.options.animSpeed=0}if(typeof this.options.generatePagination==="string"){this.pagerHolder=this.gallery.find(this.options.generatePagination).empty();this.pagerList=e(this.options.pagerList).appendTo(this.pagerHolder);for(var t=0;t<this.stepsCount;t++){e(this.options.pagerListItem).appendTo(this.pagerList).find(this.options.pagerListItemText).text(t+1)}this.pagerLinks=this.pagerList.children()}else{this.pagerLinks=this.gallery.find(this.options.pagerLinks)}var n=this.slides.filter("."+this.options.activeClass);if(n.length){this.currentIndex=this.slides.index(n)}this.prevIndex=this.currentIndex;this.btnPlay=this.gallery.find(this.options.btnPlay);this.btnPause=this.gallery.find(this.options.btnPause);this.btnPlayPause=this.gallery.find(this.options.btnPlayPause);this.curNum=this.gallery.find(this.options.currentNumber);this.allNum=this.gallery.find(this.options.totalNumber);e(window).bind("load resize orientationchange",e.proxy(this.onWindowResize,this))},initStructure:function(){this.slides.css({display:"block",opacity:0}).eq(this.currentIndex).css({opacity:""})},attachEvents:function(){var t=this;this.btnPrev.bind(this.options.event,function(e){t.prevSlide();if(t.options.autorotationStopAfterClick){t.stopRotation()}e.preventDefault()});this.btnNext.bind(this.options.event,function(e){t.nextSlide();if(t.options.autorotationStopAfterClick){t.stopRotation()}e.preventDefault()});this.pagerLinks.each(function(n,r){e(r).bind(t.options.event,function(e){t.numSlide(n);if(t.options.autorotationStopAfterClick){t.stopRotation()}e.preventDefault()})});this.btnPlay.bind(this.options.event,function(e){t.startRotation();e.preventDefault()});this.btnPause.bind(this.options.event,function(e){t.stopRotation();e.preventDefault()});this.btnPlayPause.bind(this.options.event,function(e){if(!t.gallery.hasClass(t.options.autorotationActiveClass)){t.startRotation()}else{t.stopRotation()}e.preventDefault()});if(this.options.useSwipe&&e.fn.swipe){this.gallery.swipe({excludedElements:"",fallbackToMouseEvents:false,swipeLeft:function(){t.nextSlide()},swipeRight:function(){t.prevSlide()}})}if(this.options.pauseOnHover){this.gallery.hover(function(){if(t.options.autoRotation){t.galleryHover=true;t.pauseRotation()}},function(){if(t.options.autoRotation){t.galleryHover=false;t.resumeRotation()}})}},onWindowResize:function(){if(this.options.autoHeight){this.slidesHolder.css({height:this.slides.eq(this.currentIndex).outerHeight(true)})}},prevSlide:function(){if(!(this.options.disableWhileAnimating&&this.galleryAnimating)){this.prevIndex=this.currentIndex;if(this.currentIndex>0){this.currentIndex--;this.switchSlide()}else if(this.options.circularRotation){this.currentIndex=this.stepsCount-1;this.switchSlide()}}},nextSlide:function(e){if(!(this.options.disableWhileAnimating&&this.galleryAnimating)){this.prevIndex=this.currentIndex;if(this.currentIndex<this.stepsCount-1){this.currentIndex++;this.switchSlide()}else if(this.options.circularRotation||e===true){this.currentIndex=0;this.switchSlide()}}},numSlide:function(e){if(this.currentIndex!=e){this.prevIndex=this.currentIndex;this.currentIndex=e;this.switchSlide()}},switchSlide:function(){var e=this;if(this.slides.length>1){this.galleryAnimating=true;if(!this.options.animSpeed){this.slides.eq(this.prevIndex).css({opacity:0})}else{this.slides.eq(this.prevIndex).stop().animate({opacity:0},{duration:this.options.animSpeed})}this.switchNext=function(){if(!e.options.animSpeed){e.slides.eq(e.currentIndex).css({opacity:""})}else{e.slides.eq(e.currentIndex).stop().animate({opacity:1},{duration:e.options.animSpeed})}setTimeout(function(){e.slides.eq(e.currentIndex).css({opacity:""});e.galleryAnimating=false;e.autoRotate();e.makeCallback("onChange",e)},e.options.animSpeed)};if(this.options.switchSimultaneously){e.switchNext()}else{clearTimeout(this.switchTimer);this.switchTimer=setTimeout(function(){e.switchNext()},this.options.animSpeed)}this.refreshState();this.makeCallback("onBeforeChange",this)}},refreshState:function(e){this.slides.removeClass(this.options.activeClass).eq(this.currentIndex).addClass(this.options.activeClass);this.pagerLinks.removeClass(this.options.activeClass).eq(this.currentIndex).addClass(this.options.activeClass);this.curNum.html(this.currentIndex+1);this.allNum.html(this.stepsCount);if(this.options.autoHeight){if(e){this.slidesHolder.css({height:this.slides.eq(this.currentIndex).outerHeight(true)})}else{this.slidesHolder.stop().animate({height:this.slides.eq(this.currentIndex).outerHeight(true)},{duration:this.options.animSpeed})}}if(!this.options.circularRotation){this.btnPrev.add(this.btnNext).removeClass(this.options.disabledClass);if(this.currentIndex===0)this.btnPrev.addClass(this.options.disabledClass);if(this.currentIndex===this.stepsCount-1)this.btnNext.addClass(this.options.disabledClass)}},startRotation:function(){this.options.autoRotation=true;this.galleryHover=false;this.autoRotationStopped=false;this.resumeRotation()},stopRotation:function(){this.galleryHover=true;this.autoRotationStopped=true;this.pauseRotation()},pauseRotation:function(){this.gallery.addClass(this.options.autorotationDisabledClass);this.gallery.removeClass(this.options.autorotationActiveClass);clearTimeout(this.timer)},resumeRotation:function(){if(!this.autoRotationStopped){this.gallery.addClass(this.options.autorotationActiveClass);this.gallery.removeClass(this.options.autorotationDisabledClass);this.autoRotate()}},autoRotate:function(){var e=this;clearTimeout(this.timer);if(this.options.autoRotation&&!this.galleryHover&&!this.autoRotationStopped){this.gallery.addClass(this.options.autorotationActiveClass);this.timer=setTimeout(function(){e.nextSlide(true)},this.options.switchTime)}else{this.pauseRotation()}},makeCallback:function(e){if(typeof this.options[e]==="function"){var t=Array.prototype.slice.call(arguments);t.shift();this.options[e].apply(this,t)}}};e.fn.fadeGallery=function(n){return this.each(function(){e(this).data("FadeGallery",new t(e.extend(n,{holder:this})))})}})(jQuery)