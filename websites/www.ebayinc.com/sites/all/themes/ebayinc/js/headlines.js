(function($) {
    Drupal.behaviors.ebayHeadlines = {
      attach: function(context) {
          $('.stories-page-grid header h1 a', context)
            .dotdotdot({
              wrap: 'letter',
              height: 110
            });
          $('section.news article h1 a, section.news article h2 a', context)
            .css({
              display: 'block',
//              height: '147px'
            })
            .dotdotdot({
              wrap: 'letter'
            });
          $('section.news ul.news-list li a', context)
            .dotdotdot({
              wrap: 'letter',
//              height: 83
            });
          $('section.news article h2, section.news article h1', context).css('margin-bottom', 0);
      }
    };
 })(jQuery);