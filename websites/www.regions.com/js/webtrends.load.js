// WebTrends SmartSource Data Collector Tag v10.2.55
// Copyright (c) 2013 Webtrends Inc.  All rights reserved.
// Tag Builder Version: 4.1.0.42
// Created: 2013.01.22
window.webtrendsAsyncInit=function(){
    var dcs=new Webtrends.dcs().init({
        dcsid:"dcs4b71fc10000gs8u88h5t1k_6n2i",
        domain:"https://www.regions.com/JS/statse.webtrendslive.com",
        timezone:-6,
        i18n:true,
        adimpressions:true,
        adsparam:"adimpressions",
        offsite:true,
        download:true,
        downloadtypes:"xls,doc,pdf,txt,csv,zip,docx,xlsx,rar,gzip",
        anchor:true,
        javascript: true,
        onsitedoms:"https://www.regions.com/JS/regions.com, bridgetrack.com, geezeo.com",
        fpcdom:".regions.com",
        plugins:{
            //hm:{src:"../../s.webtrends.com/js/webtrends.hm-1.js"/*tpa=https://s.webtrends.com/js/webtrends.hm.js*/}
        }
        }).track();
};
(function(){
    var s=document.createElement("script"); s.async=true; s.src="webtrends.min.js"/*tpa=https://www.regions.com/JS/webtrends.min.js*/;    
    var s2=document.getElementsByTagName("script")[0]; s2.parentNode.insertBefore(s,s2);
}());
