<script id = "race121b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race121={};
	myVars.races.race121.varName="Tree[0x7f643b7af458]:MenuFinancialInfo";
	myVars.races.race121.varType="@varType@";
	myVars.races.race121.repairType = "@RepairType";
	myVars.races.race121.event1={};
	myVars.races.race121.event2={};
	myVars.races.race121.event1.id = "Lu_Id_div_36";
	myVars.races.race121.event1.type = "onmouseout";
	myVars.races.race121.event1.loc = "Lu_Id_div_36_LOC";
	myVars.races.race121.event1.isRead = "True";
	myVars.races.race121.event1.eventType = "@event1EventType@";
	myVars.races.race121.event2.id = "MenuFinancialInfo";
	myVars.races.race121.event2.type = "MenuFinancialInfo__parsed";
	myVars.races.race121.event2.loc = "MenuFinancialInfo_LOC";
	myVars.races.race121.event2.isRead = "False";
	myVars.races.race121.event2.eventType = "@event2EventType@";
	myVars.races.race121.event1.executed= false;// true to disable, false to enable
	myVars.races.race121.event2.executed= false;// true to disable, false to enable
</script>

