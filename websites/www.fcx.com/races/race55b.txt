<script id = "race55b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race55={};
	myVars.races.race55.varName="Tree[0x7f643b7af458]:MenuStocksDividends";
	myVars.races.race55.varType="@varType@";
	myVars.races.race55.repairType = "@RepairType";
	myVars.races.race55.event1={};
	myVars.races.race55.event2={};
	myVars.races.race55.event1.id = "Lu_Id_div_33";
	myVars.races.race55.event1.type = "onmouseover";
	myVars.races.race55.event1.loc = "Lu_Id_div_33_LOC";
	myVars.races.race55.event1.isRead = "True";
	myVars.races.race55.event1.eventType = "@event1EventType@";
	myVars.races.race55.event2.id = "MenuStocksDividends";
	myVars.races.race55.event2.type = "MenuStocksDividends__parsed";
	myVars.races.race55.event2.loc = "MenuStocksDividends_LOC";
	myVars.races.race55.event2.isRead = "False";
	myVars.races.race55.event2.eventType = "@event2EventType@";
	myVars.races.race55.event1.executed= false;// true to disable, false to enable
	myVars.races.race55.event2.executed= false;// true to disable, false to enable
</script>

