<script id = "race38a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race38={};
	myVars.races.race38.varName="Tree[0x7f643b7af458]:MenuIr";
	myVars.races.race38.varType="@varType@";
	myVars.races.race38.repairType = "@RepairType";
	myVars.races.race38.event1={};
	myVars.races.race38.event2={};
	myVars.races.race38.event1.id = "MenuIr";
	myVars.races.race38.event1.type = "MenuIr__parsed";
	myVars.races.race38.event1.loc = "MenuIr_LOC";
	myVars.races.race38.event1.isRead = "False";
	myVars.races.race38.event1.eventType = "@event1EventType@";
	myVars.races.race38.event2.id = "Lu_Id_img_7";
	myVars.races.race38.event2.type = "onmouseover";
	myVars.races.race38.event2.loc = "Lu_Id_img_7_LOC";
	myVars.races.race38.event2.isRead = "True";
	myVars.races.race38.event2.eventType = "@event2EventType@";
	myVars.races.race38.event1.executed= false;// true to disable, false to enable
	myVars.races.race38.event2.executed= false;// true to disable, false to enable
</script>

