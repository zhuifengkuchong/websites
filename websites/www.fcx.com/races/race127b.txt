<script id = "race127b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race127={};
	myVars.races.race127.varName="Tree[0x7f643b7af458]:MenuInvestorResources";
	myVars.races.race127.varType="@varType@";
	myVars.races.race127.repairType = "@RepairType";
	myVars.races.race127.event1={};
	myVars.races.race127.event2={};
	myVars.races.race127.event1.id = "Lu_Id_div_41";
	myVars.races.race127.event1.type = "onmouseout";
	myVars.races.race127.event1.loc = "Lu_Id_div_41_LOC";
	myVars.races.race127.event1.isRead = "True";
	myVars.races.race127.event1.eventType = "@event1EventType@";
	myVars.races.race127.event2.id = "MenuInvestorResources";
	myVars.races.race127.event2.type = "MenuInvestorResources__parsed";
	myVars.races.race127.event2.loc = "MenuInvestorResources_LOC";
	myVars.races.race127.event2.isRead = "False";
	myVars.races.race127.event2.eventType = "@event2EventType@";
	myVars.races.race127.event1.executed= false;// true to disable, false to enable
	myVars.races.race127.event2.executed= false;// true to disable, false to enable
</script>

