<script id = "race88b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race88={};
	myVars.races.race88.varName="Tree[0x7f643b7af458]:MenuCompany";
	myVars.races.race88.varType="@varType@";
	myVars.races.race88.repairType = "@RepairType";
	myVars.races.race88.event1={};
	myVars.races.race88.event2={};
	myVars.races.race88.event1.id = "Lu_Id_img_2";
	myVars.races.race88.event1.type = "onmouseout";
	myVars.races.race88.event1.loc = "Lu_Id_img_2_LOC";
	myVars.races.race88.event1.isRead = "True";
	myVars.races.race88.event1.eventType = "@event1EventType@";
	myVars.races.race88.event2.id = "MenuCompany";
	myVars.races.race88.event2.type = "MenuCompany__parsed";
	myVars.races.race88.event2.loc = "MenuCompany_LOC";
	myVars.races.race88.event2.isRead = "False";
	myVars.races.race88.event2.eventType = "@event2EventType@";
	myVars.races.race88.event1.executed= false;// true to disable, false to enable
	myVars.races.race88.event2.executed= false;// true to disable, false to enable
</script>

