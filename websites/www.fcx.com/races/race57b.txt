<script id = "race57b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race57={};
	myVars.races.race57.varName="Tree[0x7f643b7af458]:MenuFinancialInfo";
	myVars.races.race57.varType="@varType@";
	myVars.races.race57.repairType = "@RepairType";
	myVars.races.race57.event1={};
	myVars.races.race57.event2={};
	myVars.races.race57.event1.id = "Lu_Id_div_34";
	myVars.races.race57.event1.type = "onmouseover";
	myVars.races.race57.event1.loc = "Lu_Id_div_34_LOC";
	myVars.races.race57.event1.isRead = "True";
	myVars.races.race57.event1.eventType = "@event1EventType@";
	myVars.races.race57.event2.id = "MenuFinancialInfo";
	myVars.races.race57.event2.type = "MenuFinancialInfo__parsed";
	myVars.races.race57.event2.loc = "MenuFinancialInfo_LOC";
	myVars.races.race57.event2.isRead = "False";
	myVars.races.race57.event2.eventType = "@event2EventType@";
	myVars.races.race57.event1.executed= false;// true to disable, false to enable
	myVars.races.race57.event2.executed= false;// true to disable, false to enable
</script>

