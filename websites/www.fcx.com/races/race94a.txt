<script id = "race94a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race94={};
	myVars.races.race94.varName="Tree[0x7f643b7af458]:MenuIr";
	myVars.races.race94.varType="@varType@";
	myVars.races.race94.repairType = "@RepairType";
	myVars.races.race94.event1={};
	myVars.races.race94.event2={};
	myVars.races.race94.event1.id = "MenuIr";
	myVars.races.race94.event1.type = "MenuIr__parsed";
	myVars.races.race94.event1.loc = "MenuIr_LOC";
	myVars.races.race94.event1.isRead = "False";
	myVars.races.race94.event1.eventType = "@event1EventType@";
	myVars.races.race94.event2.id = "Lu_Id_img_4";
	myVars.races.race94.event2.type = "onmouseout";
	myVars.races.race94.event2.loc = "Lu_Id_img_4_LOC";
	myVars.races.race94.event2.isRead = "True";
	myVars.races.race94.event2.eventType = "@event2EventType@";
	myVars.races.race94.event1.executed= false;// true to disable, false to enable
	myVars.races.race94.event2.executed= false;// true to disable, false to enable
</script>

