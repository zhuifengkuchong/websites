<script id = "race32b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race32={};
	myVars.races.race32.varName="Tree[0x7f643b7af458]:MenuDevelop";
	myVars.races.race32.varType="@varType@";
	myVars.races.race32.repairType = "@RepairType";
	myVars.races.race32.event1={};
	myVars.races.race32.event2={};
	myVars.races.race32.event1.id = "Lu_Id_img_6";
	myVars.races.race32.event1.type = "onmouseover";
	myVars.races.race32.event1.loc = "Lu_Id_img_6_LOC";
	myVars.races.race32.event1.isRead = "True";
	myVars.races.race32.event1.eventType = "@event1EventType@";
	myVars.races.race32.event2.id = "MenuDevelop";
	myVars.races.race32.event2.type = "MenuDevelop__parsed";
	myVars.races.race32.event2.loc = "MenuDevelop_LOC";
	myVars.races.race32.event2.isRead = "False";
	myVars.races.race32.event2.eventType = "@event2EventType@";
	myVars.races.race32.event1.executed= false;// true to disable, false to enable
	myVars.races.race32.event2.executed= false;// true to disable, false to enable
</script>

