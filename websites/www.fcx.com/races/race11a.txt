<script id = "race11a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race11={};
	myVars.races.race11.varName="Tree[0x7f643b7af458]:MenuDevelop";
	myVars.races.race11.varType="@varType@";
	myVars.races.race11.repairType = "@RepairType";
	myVars.races.race11.event1={};
	myVars.races.race11.event2={};
	myVars.races.race11.event1.id = "MenuDevelop";
	myVars.races.race11.event1.type = "MenuDevelop__parsed";
	myVars.races.race11.event1.loc = "MenuDevelop_LOC";
	myVars.races.race11.event1.isRead = "False";
	myVars.races.race11.event1.eventType = "@event1EventType@";
	myVars.races.race11.event2.id = "Lu_Id_img_3";
	myVars.races.race11.event2.type = "onmouseover";
	myVars.races.race11.event2.loc = "Lu_Id_img_3_LOC";
	myVars.races.race11.event2.isRead = "True";
	myVars.races.race11.event2.eventType = "@event2EventType@";
	myVars.races.race11.event1.executed= false;// true to disable, false to enable
	myVars.races.race11.event2.executed= false;// true to disable, false to enable
</script>

