<script id = "race20a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race20={};
	myVars.races.race20.varName="Tree[0x7f643b7af458]:MenuOperations";
	myVars.races.race20.varType="@varType@";
	myVars.races.race20.repairType = "@RepairType";
	myVars.races.race20.event1={};
	myVars.races.race20.event2={};
	myVars.races.race20.event1.id = "MenuOperations";
	myVars.races.race20.event1.type = "MenuOperations__parsed";
	myVars.races.race20.event1.loc = "MenuOperations_LOC";
	myVars.races.race20.event1.isRead = "False";
	myVars.races.race20.event1.eventType = "@event1EventType@";
	myVars.races.race20.event2.id = "Lu_Id_img_4";
	myVars.races.race20.event2.type = "onmouseover";
	myVars.races.race20.event2.loc = "Lu_Id_img_4_LOC";
	myVars.races.race20.event2.isRead = "True";
	myVars.races.race20.event2.eventType = "@event2EventType@";
	myVars.races.race20.event1.executed= false;// true to disable, false to enable
	myVars.races.race20.event2.executed= false;// true to disable, false to enable
</script>

