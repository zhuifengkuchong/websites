<script id = "race6a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race6={};
	myVars.races.race6.varName="Tree[0x7f643b7af458]:MenuOperations";
	myVars.races.race6.varType="@varType@";
	myVars.races.race6.repairType = "@RepairType";
	myVars.races.race6.event1={};
	myVars.races.race6.event2={};
	myVars.races.race6.event1.id = "MenuOperations";
	myVars.races.race6.event1.type = "MenuOperations__parsed";
	myVars.races.race6.event1.loc = "MenuOperations_LOC";
	myVars.races.race6.event1.isRead = "False";
	myVars.races.race6.event1.eventType = "@event1EventType@";
	myVars.races.race6.event2.id = "Lu_Id_img_2";
	myVars.races.race6.event2.type = "onmouseover";
	myVars.races.race6.event2.loc = "Lu_Id_img_2_LOC";
	myVars.races.race6.event2.isRead = "True";
	myVars.races.race6.event2.eventType = "@event2EventType@";
	myVars.races.race6.event1.executed= false;// true to disable, false to enable
	myVars.races.race6.event2.executed= false;// true to disable, false to enable
</script>

