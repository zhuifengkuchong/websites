<script id = "race28b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race28={};
	myVars.races.race28.varName="Tree[0x7f643b7af458]:MenuCompany";
	myVars.races.race28.varType="@varType@";
	myVars.races.race28.repairType = "@RepairType";
	myVars.races.race28.event1={};
	myVars.races.race28.event2={};
	myVars.races.race28.event1.id = "Lu_Id_img_6";
	myVars.races.race28.event1.type = "onmouseover";
	myVars.races.race28.event1.loc = "Lu_Id_img_6_LOC";
	myVars.races.race28.event1.isRead = "True";
	myVars.races.race28.event1.eventType = "@event1EventType@";
	myVars.races.race28.event2.id = "MenuCompany";
	myVars.races.race28.event2.type = "MenuCompany__parsed";
	myVars.races.race28.event2.loc = "MenuCompany_LOC";
	myVars.races.race28.event2.isRead = "False";
	myVars.races.race28.event2.eventType = "@event2EventType@";
	myVars.races.race28.event1.executed= false;// true to disable, false to enable
	myVars.races.race28.event2.executed= false;// true to disable, false to enable
</script>

