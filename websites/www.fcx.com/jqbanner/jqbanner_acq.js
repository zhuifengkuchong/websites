// Simple JavaScript Rotating Banner Using jQuery
// www.mclelun.com
var jqb_vCurrent = 0;
var jqb_vTotal = 0;
var jqb_vDuration = 5200;
var jqb_intInterval = 0;
var jqb_vGo = 1;
var jqb_vIsPause = false;
var jqb_tmp = 20;
var jqb_title;
var jqb_imgW = 850;
var jqb_imgH = 380;

var captioncoords = [
    {
		"x": 600, 
		"y": 264,
		"width": 200,
		"height": 84,
		"bg":"Unknown_83_filename"/*tpa=http://www.fcx.com/jqbanner/jqbanner/img/11.png*/,
		"color":"#fff",
		"text":"Grasberg mine in Papua, Indonesia"},
	
	{
		"x": 28, 
		"y": 69,
		"width": 160,
		"height": 84,
		"bg":"Unknown_83_filename"/*tpa=http://www.fcx.com/jqbanner/jqbanner/img/12.png*/,
		"color":"#000",
		"text":"World's premier publicly traded copper company"},
		
	{
		"x": 663, 
		"y": 264,
		"width": 157,
		"height": 70,
		"bg":"Unknown_83_filename"/*tpa=http://www.fcx.com/jqbanner/jqbanner/img/17.png*/,
		"color":"#fff",
		"text":"High quality oil & gas portfolio"},
	
	
	{
		"x": 676, 
		"y": 273,
		"width": 144,
		"height": 84,
		"bg":"Unknown_83_filename"/*tpa=http://www.fcx.com/jqbanner/jqbanner/img/13.png*/,
		"color":"#000",
		"text":"Global leader in the production of molybdenum"},
	
	{
		"x": 676, 
		"y": 273,
		"width": 144,
		"height": 84,
		"bg":"Unknown_83_filename"/*tpa=http://www.fcx.com/jqbanner/jqbanner/img/14.png*/,
		"color":"#fff",
		"text":"Growing reserve base"},
	{
		"x": 28, 
		"y": 69,
		"width": 144,
		"height": 84,
		"bg":"Unknown_83_filename"/*tpa=http://www.fcx.com/jqbanner/jqbanner/img/15.png*/,
		"color":"#000",
		"text":"Development projects propel production growth"},
	
	{		
		"x": 28, 
		"y": 69,
		"width": 193,
		"height": 61,
		"bg":"Unknown_83_filename"/*tpa=http://www.fcx.com/jqbanner/jqbanner/img/16.png*/,
		"color":"#fff",
		"text":"Working toward sustainable development"}

	];

jQuery(document).ready(function() {	
	jqb_vTotal = $(".jqb_slides").children().size() -1;
	
	$('#captioncontainer').css('background', 'url('+captioncoords[0].bg+') no-repeat');
	$('#captioncontainer').css('width', captioncoords[0].width);
	$('#captioncontainer').css('height', captioncoords[0].height);
	$('#captioncontainer').css('left', captioncoords[0].x);
	$('#captioncontainer').css('top', captioncoords[0].y);
	$('#captioncontainer').css('color', captioncoords[0].color);
	//$('#captioncontainer').html(captioncoords[0].text);
	$( "#captioncontainer" ).animate({ height: "toggle", opacity: "toggle" }, { duration: 700 });
	
	jqb_intInterval = setInterval(jqb_fnLoop, jqb_vDuration);
	/*		
	//Horizontal
	$("#jqb_object").find(".jqb_slide").each(function(i) { 
		jqb_tmp = ((i - 1)*jqb_imgW) - ((jqb_vCurrent -1)*jqb_imgW);
		$(this).animate({"left": jqb_tmp+"px"}, 760);
	});
	*/
$("#jqb_object").find(".jqb_slide").each(function(i) { 	
	if(i == jqb_vCurrent){
		//$(".jqb_info").text($(this).attr("title"));
		$(this).animate({ opacity: 'show' }, 760);
	} else {
		$(this).animate({ opacity: 'hide' }, 760);
	}
});	
	/*
	//Vertical
	$("#jqb_object").find(".jqb_slide").each(function(i) { 
		jqb_tmp = ((i - 1)*jqb_imgH) - ((jqb_vCurrent -1)*jqb_imgH);
		$(this).animate({"top": jqb_tmp+"px"}, 760);
	});
	*/
	
	$("#btn_pauseplay").click(function() {
		if(jqb_vIsPause){
			jqb_fnChange();
			jqb_vIsPause = false;
			$("#btn_pauseplay").removeClass("jqb_btn_play");
			$("#btn_pauseplay").addClass("jqb_btn_pause");
		} else {
			clearInterval(jqb_intInterval);
			jqb_vIsPause = true;
			$("#btn_pauseplay").removeClass("jqb_btn_pause");
			$("#btn_pauseplay").addClass("jqb_btn_play");
		}
	});
	$("#btn_prev").click(function() {
		jqb_vGo = -1;
		jqb_fnChange();
	});
		
	$("#btn_next").click(function() {
		jqb_vGo = 1;
		jqb_fnChange();
	});
});

function jqb_fnChange(){
	clearInterval(jqb_intInterval);
	jqb_intInterval = setInterval(jqb_fnLoop, jqb_vDuration);
	jqb_fnLoop();
}

function jqb_fnLoop(){
	
	if(jqb_vGo == 1){
		jqb_vCurrent == jqb_vTotal ? jqb_vCurrent = 0 : jqb_vCurrent++;
	} else {
		jqb_vCurrent == 0 ? jqb_vCurrent = jqb_vTotal : jqb_vCurrent--;
	}
	
			
	$("#jqb_object").find(".jqb_slide").each(function(i) { 	
		if(i == jqb_vCurrent){
			$('#captioncontainer').css('display', 'none');
			$('#captioncontainer').css('background', 'url('+captioncoords[i].bg+') no-repeat');
			$('#captioncontainer').css('width', captioncoords[i].width);
			$('#captioncontainer').css('height', captioncoords[i].height);
			$('#captioncontainer').css('left', captioncoords[i].x);
			$('#captioncontainer').css('top', captioncoords[i].y);
			$('#captioncontainer').css('color', captioncoords[i].color);
			//$('#captioncontainer').html(captioncoords[i].text);
			$("#captioncontainer" ).animate({ height: "show", opacity: "show" }, { duration: 700 });
		} 
			
			
		//Horizontal Scrolling
		//jqb_tmp = ((i - 1)*jqb_imgW) - ((jqb_vCurrent -1)*jqb_imgW);
		//$(this).animate({"left": jqb_tmp+"px"}, 760);
		
		
		/*
		//Vertical Scrolling
		jqb_tmp = ((i - 1)*jqb_imgH) - ((jqb_vCurrent -1)*jqb_imgH);
		$(this).animate({"top": jqb_tmp+"px"}, 760);
		*/
		
		
		//Fade In & Fade Out
		if(i == jqb_vCurrent){
			//$(".jqb_info").text($(this).attr("title"));
			$(this).animate({ opacity: 'show' }, 760);
		} else {
			$(this).animate({ opacity: 'hide' }, 760);
		}
		
	});


}





