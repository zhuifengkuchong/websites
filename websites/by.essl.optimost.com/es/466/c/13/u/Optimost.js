/**
 * Created by davilat on 5/21/2015.
 */

var optimost = {
    A: {},
    C: {},
    D: document,
    L: document.location,
    M: [],
    Q: {},
    T: new Date(),
    U: '',
    V: '2.7',
    Enabled: true,
    ST: "script",
    SA: {
        "type": "text/javascript"
    },
    I: function() {
        var s = this.L.search;
        var c = this.D.cookie;
        if (s.length > 3) {
            for (var a = s.substring(1).split("&"), i = 0, l = a.length; i < l; i++) {
                var p = a[i].indexOf("=");
                if (p > 0) this.Q[a[i].substring(0, p)] = unescape(a[i].substring(p + 1));
            }
        }
        if (c.length > 3) {
            for (var a = c.split(";"), i = 0, b = a.length; i < b; i++) {
                var v = a[i].split("=");
                while (v[0].substring(0, 1) == " ") v[0] = v[0].substring(1, v[0].length);
                if (v.length == 2) this.C[v[0]] = unescape(v[1]);
            }
        }
    },
    B: function() {
        var n;
        this.A = {};
        var _o = this;
        this.A.D_ts = Math.round(_o.T.getTime() / 1000);
        this.A.D_tzo = _o.T.getTimezoneOffset();
        this.A.D_loc = _o.L.protocol + "//" + _o.L.hostname + _o.L.pathname;
        this.A.D_ckl = _o.D.cookie.length;
        this.A.D_ref = _o.D.referrer;
        if (typeof optrial == "object")
            for (n in optrial) this.A[n] = optrial[n];
        for (n in this.Q) this.A[n] = this.Q[n];
        for (n in this.C)
            if (n.substring(0, 2) == "op") this.A[n] = this.C[n];
    },
    S: function() {
        var q = '';
        for (var n in this.A)
            if (this.A[n] != null && this.A[n] != "") q += (q.length > 0 ? "&" : (this.U.indexOf("?") > 0 ? "&" : "?")) + n + "=" + escape(this.A[n]);
        return this.U + q;
    },
    SC: function(n, v, e, d) {
        var de = new Date();
        de.setTime(de.getTime() + e * 1000);
        this.D.cookie = n + "=" + escape(v) + ((e == null) ? "" : ("; expires=" + de.toGMTString())) + "; path=/" + ((d == null) ? "" : (";domain=" + d));
    },
    SLD: function() {
        var sld = this.D.domain;
        var dp = sld.split(".");
        var l = dp.length;
        if (l < 2) sld = null;
        else if (!isNaN(dp[l - 1]) && !isNaN(dp[l - 2])) sld = null;
        else sld = "." + dp[l - 2] + "." + dp[l - 1];
        return sld;
    },
    R: function(r, c, d, e) {
        if (this.Enabled) {
            var b = true;
            if (r < 1000) {
                b = (Math.floor(Math.random() * 1000) < r);
                if (c != null) {
                    if (this.C[c] != null) b = (this.C[c] != "mvt-no");
                    else this.SC(c, b ? "mvt-yes" : "mvt-no", e, d);
                }
            }
            if (b) {
                var t = '<' + this.ST + ' src="' + this.S() + '"';
                for (n in this.SA)
                    t += (" " + n + '="' + this.SA[n] + '"');
                t += '><\/' + this.ST + '>';
                this.D.write(t);
            }
        }
    },
    addModule: function(s, f) {
        this.M[s] = f;
    },
    displayModule: function(s) {
        if (typeof this.M[s] == "function") this.M[s]();
    },
    hasModules: function() {
        return count(this.M) > 0;
    }
};
optimost.I();
var optGlobal = {};
optGlobal.addStyle = function(sStyle) {
    var style1 = document.createElement('style');
    style1.setAttribute('type', 'text/css');
    var txt1 = document.createTextNode(sStyle);
    if (style1.styleSheet) {
        style1.styleSheet.cssText = txt1.nodeValue;
    } else {
        style1.appendChild(txt1);
    }
    var h = document.getElementsByTagName("head")[0];
    if (h && h.appendChild) {
        h.appendChild(style1);
    }
};
optGlobal.checkFlash = function() {
    var hasFlash = false;
    if (navigator.plugins && navigator.plugins["Shockwave Flash"]) {
        hasFlash = true;
    } else if (typeof(window.ActiveXObject) != "undefined") {
        try {
            var a = new ActiveXObject("ShockwaveFlash.ShockwaveFlash");
            if (a) {
                hasFlash = true;
            }
        } catch (err) {}
    }
    return hasFlash;
};
if (optimost.Q["opselect"]) {
    optimost.SC("opselect", optimost.Q["opselect"], null, optimost.SLD());
}
optGlobal.opselect = optimost.Q["opselect"] || optimost.C["opselect"] || "";
if (optGlobal.opselect == "qa") {
    optGlobal.isStage = true;
}
optGlobal.classTrial = function(trialUrl, throttle, cookie, domain, exp, func) {
    this.trialUrl = trialUrl;
    this.cookie = cookie || null;
    this.throttle = throttle || 1000;
    this.expire = exp || null;
    this.domain = domain || null;
    this.attribs = (typeof(func) == "function") ? func() : null;
}
optGlobal.runExperiment = function() {
    if (typeof(optimost) != "object") return false;
    for (var i = 0; i < optGlobal.aPageTrial.length; i++) {
        var opCurrentTrial = optGlobal.aPageTrial[i];
        window.optrial = window.optrial || {};
        if (opCurrentTrial.attribs) {
            optrial = opCurrentTrial.attribs;
        }
        //Robert Brennan
        //Step 1
        //Grab current (page URL) location of the user
        optrial.opCurrent = document.location.pathname;
        if (optrial.opCurrent == '/') {
            optrial.opCurrent = 'https://by.essl.optimost.com/home/home.aspx';
        }
        //Step 2
        //WIP
        //Step 3
        //If the cookie holding new or repeat visitor exists, pull that value
        var cookie, str, arr;
        cookie = optimost.C['https://by.essl.optimost.com/es/466/c/13/u/fsr.s.all'];
        if (cookie) {
            str = cookie.match(/"NeworRepeatVisitor":"[^"]*"/);
            if (str) {
                str = str.toString().replace(/"/g, '');
                arr = str.split(':');
                if (arr.length == 2) {
                    optrial.opNeworRepeatVisitor = arr[1];
                }
            }
        }

        opCurrentTrial.trialUrl = (opCurrentTrial.trialUrl.indexOf("http") == -1) ? optGlobal.strPath + opCurrentTrial.trialUrl : opCurrentTrial.trialUrl;
        (function() {
            var _o = optimost;
            _o.U = opCurrentTrial.trialUrl;
            _o.ST = "script";
            _o.SA = {
                "type": "text/javascript"
            };
            _o.B();
            _o.R(opCurrentTrial.throttle, opCurrentTrial.cookie, opCurrentTrial.domain, opCurrentTrial.expire);
        })();
    }
    return true;
}
optGlobal.getPageId = function() {
    var pageId = "";
    if (typeof(page) == "string") {
        pageId = page.toLowerCase();
        if (pageId.indexOf("seguro de auto y casa") > -1) pageId = "home_espanol";
        else if (pageId.indexOf("de seguro de auto a su manera") > -1) pageId = "threeways_espanol";
    }
    if ((pageId == "ccc_accountstatuserror" || pageId == "ccc_viewpolicyerror") && document.location.href.indexOf("https://by.essl.optimost.com/es/466/c/13/u/Error.aspx") > -1) {
        pageId = "ccc_error";
    }
    window.optrial = window.optrial || {};
    if (window.location.protocol.toLowerCase().indexOf("https") > -1) {
        if (pageId == "home" || pageId == "allstate_homepage") {
            pageId = "securehome";
        } else if (pageId == "auto_homepage") {
            pageId = "secure_auto_homepage";
        } else if (pageId == "miallstate_homepage") {}
    }
    /*
     if (!optGlobal.checkFlash()) {
     pageId = "noflash_" + pageId;
     }
     */
    if (document.location.hostname.indexOf("green") > -1) {
        pageId = "";
    }
    optrial.opPageName = pageId;
    return pageId;
}
optGlobal.getAttributes = function(page) {
    var objAttrib = {};
    if (page.match(/ccc_lostuser|ccc_accountstatuserror|ccc_viewpolicyerror/)) {
        objAttrib.page = page;
    }
    return objAttrib;
}
optGlobal.strPage = optGlobal.getPageId();

optGlobal.objTrials = {};
// Tests in here will fire on the page
// Used to handle multiple tests firing on the same page
optGlobal.objTrials.fire_on_page = [];
//Determine the screen size
var opScreenWidth = 0;
if (self.innerHeight) opScreenWidth = self.innerWidth;
else if (document.documentElement && document.documentElement.clientHeight) opScreenWidth = document.documentElement.clientWidth;
else if (document.body) opScreenWidth = document.body.clientWidth;
optrial.myWidth = opScreenWidth;
//GlobalNav T6
if (opScreenWidth < 768) optrial.opscreen = 'mobile';
else optrial.opscreen = 'desktop';


if (optGlobal.isStage) {
//Tools and Resources T8
    var arrAllowed = [
        'https://by.essl.optimost.com/tools-and-resources/car-insurance/what-is-comprehensive-auto-insurance.aspx',
        'https://by.essl.optimost.com/tools-and-resources/car-insurance/rental-car-insurance.aspx',
        'https://by.essl.optimost.com/tools-and-resources/renters-insurance/what-does-renters-insurance-cover.aspx',
        'https://by.essl.optimost.com/tools-and-resources/car-insurance/handling-a-deer-accident.aspx',
        'https://by.essl.optimost.com/tools-and-resources/car-insurance/liability-car-insurance-cover.aspx',
        'https://by.essl.optimost.com/tools-and-resources/car-insurance/what-is-full-coverage.aspx',
        'https://by.essl.optimost.com/tools-and-resources/car-insurance/uninsured-motorist-protection.aspx',
        'https://by.essl.optimost.com/tools-and-resources/car-insurance/car-payment-calculator.aspx',
        'https://by.essl.optimost.com/tools-and-resources/car-insurance/learner-driver.aspx',
        'https://by.essl.optimost.com/tools-and-resources/car-insurance/no-fault-insurance-cover.aspx',
        'https://by.essl.optimost.com/tools-and-resources/home-insurance/hazard-insurance.aspx',
        'https://by.essl.optimost.com/tools-and-resources/home-insurance/dwelling-insurance.aspx',
        'https://by.essl.optimost.com/tools-and-resources/home-insurance/change-home-air-filter.aspx',
        'https://by.essl.optimost.com/tools-and-resources/landlord/other-resources.aspx',
        'https://by.essl.optimost.com/tools-and-resources/car-insurance/know-when-to-cancel-insurance.aspx',
        'https://by.essl.optimost.com/tools-and-resources/car-insurance/check-brake-pads.aspx',
        'https://by.essl.optimost.com/tools-and-resources/car-insurance/gap-insurance-coverage.aspx',
        'https://by.essl.optimost.com/tools-and-resources/car-insurance/property-damage-liability-insurance.aspx',
        'https://by.essl.optimost.com/tools-and-resources/home-insurance/personal-umbrella-policy-what-is-it-and-do-i-need-one.aspx',
        'https://by.essl.optimost.com/tools-and-resources/car-insurance/car-insurance-cost.aspx',
        'https://by.essl.optimost.com/tools-and-resources/videos/full-coverage-auto-insurance.aspx',
        'https://by.essl.optimost.com/tools-and-resources/car-insurance/best-car-insurance-college-students.aspx',
        'https://by.essl.optimost.com/tools-and-resources/car-insurance/what-is-gap-insurance.aspx',
        'https://by.essl.optimost.com/tools-and-resources/renters-insurance/homeowners-or-landlord-insurance.aspx',
        ''];
//Desktop version targeted only
    if (screen && screen.width && screen.width >= 768) {
        for (i = 0; i < arrAllowed.length && optGlobal.strPage != "toolsAndResourcesT7"; i++) {
            if (document.location.pathname.indexOf(arrAllowed[i]) > -1) {
                optGlobal.strPage = "toolsAndResourcesT7";
            }
        }
    }
//END Tools and Resources T8
} else {
//Tools and Resources T7
    if (opScreenWidth < 386) {
        optrial.opscreen_tools = 'mobile';
    }
    else {
        optrial.opscreen_tools = 'desktop';
    }
// Tools and Resources T7
    var test_pages = [
        'https://by.essl.optimost.com/es/466/c/13/u/what-is-comprehensive-auto-insurance.aspx',
        'https://by.essl.optimost.com/es/466/c/13/u/rental-car-insurance.aspx',
        'https://by.essl.optimost.com/es/466/c/13/u/handling-a-deer-accident.aspx',
        'https://by.essl.optimost.com/es/466/c/13/u/liability-car-insurance-cover.aspx',
        'https://by.essl.optimost.com/es/466/c/13/u/what-is-full-coverage.aspx',
        'https://by.essl.optimost.com/es/466/c/13/u/uninsured-motorist-protection.aspx',
        'https://by.essl.optimost.com/es/466/c/13/u/learner-driver.aspx',
        'https://by.essl.optimost.com/es/466/c/13/u/no-fault-insurance-cover.aspx',
        'https://by.essl.optimost.com/es/466/c/13/u/know-when-to-cancel-insurance.aspx',
        'https://by.essl.optimost.com/es/466/c/13/u/check-brake-pads.aspx',
        'https://by.essl.optimost.com/es/466/c/13/u/gap-insurance-coverage.aspx',
        'https://by.essl.optimost.com/es/466/c/13/u/property-damage-liability-insurance.aspx',
        'https://by.essl.optimost.com/es/466/c/13/u/best-car-insurance-college-students.aspx',
        'https://by.essl.optimost.com/es/466/c/13/u/what-is-gap-insurance.aspx',
        ''];
    for (i = 0; i < test_pages.length && optGlobal.strPage != "toolsAndResourcesT7"; i++) {
        if (document.location.pathname == ('/tools-and-resources/car-insurance/' + test_pages[i])) {
            optGlobal.strPage = "toolsAndResourcesT7";
        }
    }
//END Tools and Resources T7
}

// Tools and Resources Banner T1
var trb_pages_allowed = [
    'https://by.essl.optimost.com/tools-and-resources/car-insurance/what-is-comprehensive-auto-insurance.aspx',
    'https://by.essl.optimost.com/tools-and-resources/car-insurance/rental-car-insurance.aspx',
    'https://by.essl.optimost.com/tools-and-resources/renters-insurance/what-does-renters-insurance-cover.aspx',
    'https://by.essl.optimost.com/tools-and-resources/car-insurance/handling-a-deer-accident.aspx',
    'https://by.essl.optimost.com/tools-and-resources/car-insurance/liability-car-insurance-cover.aspx',
    'https://by.essl.optimost.com/tools-and-resources/car-insurance/what-is-full-coverage.aspx',
    'https://by.essl.optimost.com/tools-and-resources/car-insurance/uninsured-motorist-protection.aspx',
    'https://by.essl.optimost.com/tools-and-resources/car-insurance/car-payment-calculator.aspx',
    'https://by.essl.optimost.com/tools-and-resources/car-insurance/learner-driver.aspx',
    'https://by.essl.optimost.com/tools-and-resources/car-insurance/no-fault-insurance-cover.aspx',
    'https://by.essl.optimost.com/tools-and-resources/home-insurance/hazard-insurance.aspx',
    'https://by.essl.optimost.com/tools-and-resources/home-insurance/dwelling-insurance.aspx',
    'https://by.essl.optimost.com/tools-and-resources/home-insurance/change-home-air-filter.aspx',
    'https://by.essl.optimost.com/tools-and-resources/car-insurance/know-when-to-cancel-insurance.aspx',
    'https://by.essl.optimost.com/tools-and-resources/car-insurance/check-brake-pads.aspx',
    'https://by.essl.optimost.com/tools-and-resources/car-insurance/gap-insurance-coverage.aspx',
    'https://by.essl.optimost.com/tools-and-resources/car-insurance/property-damage-liability-insurance.aspx',
    'https://by.essl.optimost.com/tools-and-resources/home-insurance/personal-umbrella-policy-what-is-it-and-do-i-need-one.aspx',
    'https://by.essl.optimost.com/tools-and-resources/car-insurance/car-insurance-cost.aspx',
    'https://by.essl.optimost.com/tools-and-resources/car-insurance/best-car-insurance-college-students.aspx',
    'https://by.essl.optimost.com/tools-and-resources/car-insurance/what-is-gap-insurance.aspx',
    'https://by.essl.optimost.com/tools-and-resources/renters-insurance/homeowners-or-landlord-insurance.aspx',
    ''];
for (i = 0; i < trb_pages_allowed.length; i++) {
    if (document.location.pathname == trb_pages_allowed[i]) {
        optGlobal.objTrials.fire_on_page[optGlobal.objTrials.fire_on_page.length] = new optGlobal.classTrial("Unknown_83_filename"/*tpa=https://by.essl.optimost.com/trial/466/p/toolsandresourcesbannertrial1/162/content.js*/);
        break;
    }
}
// END Tools and Resources Banner T1

optGlobal.strPath = "http://by.optimost.com/";
if (window.location.protocol.toLowerCase().indexOf("https") > -1) optGlobal.strPath = "https://by.essl.optimost.com/by";
var op_throttle = (optGlobal.isStage) ? 1000 : 500;
var op_cookie = (optGlobal.isStage) ? null : "opHomeThrot50";
var op_domain = (optGlobal.isStage) ? null : optimost.SLD();
var op_exp = (optGlobal.isStage) ? null : 2592000;
var op_throttle = 1000;
var op_cookie = op_domain = op_exp = null;
optGlobal.objTrials.house_and_home = new optGlobal.classTrial("Unknown_83_filename"/*tpa=https://by.essl.optimost.com/trial/466/p/househome/108/content.js*/);
optGlobal.objTrials.allstate_renters = new optGlobal.classTrial("Unknown_83_filename"/*tpa=https://by.essl.optimost.com/trial/466/p/renterslp.f62/52/content.js*/);
optGlobal.objTrials.discounts = new optGlobal.classTrial("Unknown_83_filename"/*tpa=https://by.essl.optimost.com/trial/466/p/autoleftnav/98/content.js*/);
optGlobal.objTrials.coverageoptions = new optGlobal.classTrial("Unknown_83_filename"/*tpa=https://by.essl.optimost.com/trial/466/p/autoleftnav/98/content.js*/);
optGlobal.objTrials.value_plan = new optGlobal.classTrial("Unknown_83_filename"/*tpa=https://by.essl.optimost.com/trial/466/p/autoleftnav/98/content.js*/);
optGlobal.objTrials.limits_deductible = new optGlobal.classTrial("Unknown_83_filename"/*tpa=https://by.essl.optimost.com/trial/466/p/autoleftnav/98/content.js*/);
optGlobal.objTrials.allstate_tools_resources = new optGlobal.classTrial("Unknown_83_filename"/*tpa=https://by.essl.optimost.com/trial/466/p/toolsandresources/110/content.js*/);
optGlobal.objTrials.allstate_financial_life_insurance = new optGlobal.classTrial("Unknown_83_filename"/*tpa=https://by.essl.optimost.com/trial/466/p/lifeinsuranceredesign/114/content.js*/);
optGlobal.objTrials['allstate_financial_life-insurance'] = new optGlobal.classTrial("Unknown_83_filename"/*tpa=https://by.essl.optimost.com/trial/466/p/lifeinsuranceredesign/114/content.js*/);
optGlobal.objTrials['allstate_financial_life-insurance'] = new optGlobal.classTrial("Unknown_83_filename"/*tpa=https://by.essl.optimost.com/trial/466/p/lifeinsuranceredesign/114/content.js*/);
optGlobal.objTrials.life_insurance_mvt = new optGlobal.classTrial("Unknown_83_filename"/*tpa=https://by.essl.optimost.com/trial/466/p/lifeinsuranceredesign/114/content.js*/);
optGlobal.objTrials.allstate_renters = new optGlobal.classTrial("Unknown_83_filename"/*tpa=https://by.essl.optimost.com/trial/466/p/spanishtranslationbanner/113/content.js*/);
optGlobal.objTrials.allstate_coverage_options = [new optGlobal.classTrial("Unknown_83_filename"/*tpa=https://by.essl.optimost.com/trial/466/p/newautoleftnav/118/content.js*/), new optGlobal.classTrial("../../../../../counter/466/-/161/event.js"/*tpa=https://by.essl.optimost.com/counter/466/-/161/event.js*/)];
optGlobal.objTrials.allstate_features = [new optGlobal.classTrial("Unknown_83_filename"/*tpa=https://by.essl.optimost.com/trial/466/p/newautoleftnav/118/content.js*/), new optGlobal.classTrial("../../../../../counter/466/-/162/event.js"/*tpa=https://by.essl.optimost.com/counter/466/-/162/event.js*/)];
optGlobal.objTrials.allstate_claims_satisfaction = [new optGlobal.classTrial("Unknown_83_filename"/*tpa=https://by.essl.optimost.com/trial/466/p/newautoleftnav/118/content.js*/), new optGlobal.classTrial("../../../../../counter/466/-/164/event.js"/*tpa=https://by.essl.optimost.com/counter/466/-/164/event.js*/)];
optGlobal.objTrials.allstate_auto_discounts = [new optGlobal.classTrial("Unknown_83_filename"/*tpa=https://by.essl.optimost.com/trial/466/p/newautoleftnav/118/content.js*/), new optGlobal.classTrial("../../../../../counter/466/-/165/event.js"/*tpa=https://by.essl.optimost.com/counter/466/-/165/event.js*/)];
optGlobal.objTrials.allstate_teen_driver = [new optGlobal.classTrial("Unknown_83_filename"/*tpa=https://by.essl.optimost.com/trial/466/p/newautoleftnav/118/content.js*/), new optGlobal.classTrial("../../../../../counter/466/-/166/event.js"/*tpa=https://by.essl.optimost.com/counter/466/-/166/event.js*/)];
optGlobal.objTrials.allstate_agent_availability = [new optGlobal.classTrial("Unknown_83_filename"/*tpa=https://by.essl.optimost.com/trial/466/p/newautoleftnav/118/content.js*/), new optGlobal.classTrial("../../../../../counter/466/-/167/event.js"/*tpa=https://by.essl.optimost.com/counter/466/-/167/event.js*/)];
optGlobal.objTrials.allstate_claims = new optGlobal.classTrial("../../../../../counter/466/-/168/event.js"/*tpa=https://by.essl.optimost.com/counter/466/-/168/event.js*/);
optGlobal.objTrials.allstate_glass_claims = new optGlobal.classTrial("../../../../../counter/466/-/169/event.js"/*tpa=https://by.essl.optimost.com/counter/466/-/169/event.js*/);
optGlobal.objTrials.allstate_auto_direct = new optGlobal.classTrial("../../../../../counter/466/-/170/event.js"/*tpa=https://by.essl.optimost.com/counter/466/-/170/event.js*/);
optGlobal.objTrials.allstate_claims_auto_faq = new optGlobal.classTrial("../../../../../counter/466/-/171/event.js"/*tpa=https://by.essl.optimost.com/counter/466/-/171/event.js*/);
optGlobal.objTrials.allstate_roadside_compare = new optGlobal.classTrial("../../../../../counter/466/-/172/event.js"/*tpa=https://by.essl.optimost.com/counter/466/-/172/event.js*/);
optGlobal.objTrials.allstate_roadside_motor_club = new optGlobal.classTrial("../../../../../counter/466/-/173/event.js"/*tpa=https://by.essl.optimost.com/counter/466/-/173/event.js*/);
optGlobal.objTrials.allstate_roadside_good_hands = new optGlobal.classTrial("../../../../../counter/466/-/174/event.js"/*tpa=https://by.essl.optimost.com/counter/466/-/174/event.js*/);
optGlobal.objTrials.allstate_roadside_towing = new optGlobal.classTrial("../../../../../counter/466/-/175/event.js"/*tpa=https://by.essl.optimost.com/counter/466/-/175/event.js*/);
if ( document.location.pathname == "/" ) {
    optGlobal.objTrials.fire_on_page[optGlobal.objTrials.fire_on_page.length] = new optGlobal.classTrial("Unknown_83_filename"/*tpa=https://by.essl.optimost.com/trial/466/p/allstatecomhomepage.5c0/1/content.js*/);
}
if ( document.location.pathname == "https://by.essl.optimost.com/auto-insurance/auto-insurance-discounts.aspx" ) {
    optGlobal.objTrials.fire_on_page[optGlobal.objTrials.fire_on_page.length] = new optGlobal.classTrial("../../../../../counter/466/-/165/event.js"/*tpa=https://by.essl.optimost.com/counter/466/-/165/event.js*/);
}
if ( document.location.pathname == "https://by.essl.optimost.com/test-concepts/home-page-old-navigation.aspx" ) {
    optGlobal.objTrials.fire_on_page[optGlobal.objTrials.fire_on_page.length] = new optGlobal.classTrial("Unknown_83_filename"/*tpa=https://by.essl.optimost.com/trial/466/p/mocktest/168/content.js*/);
}
if ( document.location.pathname == "https://by.essl.optimost.com/boat-insurance.aspx" ) {
    optGlobal.objTrials.fire_on_page[optGlobal.objTrials.fire_on_page.length] = new optGlobal.classTrial("Unknown_83_filename"/*tpa=https://by.essl.optimost.com/trial/466/p/boatinsurance.be1/74/content.js*/);
}

//Over monthly limit soon
//optGlobal.objTrials.fire_on_all_pages = [new optGlobal.classTrial("Unknown_83_filename"/*tpa=https://by.essl.optimost.com/trial/466/p/globalnavigation/116/content.js*/), new optGlobal.classTrial("Unknown_83_filename"/*tpa=https://by.essl.optimost.com/trial/466/p/allstatecomhomepage.5c0/1/content.js*/)];
//leave array blank
optGlobal.objTrials.fire_on_all_pages = [];
if (document.location.hostname.indexOf('https://by.essl.optimost.com/es/466/c/13/u/www.allstate.com') > -1) {
    optGlobal.objTrials.fire_on_all_pages = [new optGlobal.classTrial("Unknown_83_filename"/*tpa=https://by.essl.optimost.com/trial/466/p/globalnavigation/116/content.js*/)];
}
optGlobal.objTrials.allstate_auto_insurance = [new optGlobal.classTrial("Unknown_83_filename"/*tpa=https://by.essl.optimost.com/trial/466/p/autohomepage1/102/content.js*/)];
optGlobal.objTrials.allstate_auto = [new optGlobal.classTrial("Unknown_83_filename"/*tpa=https://by.essl.optimost.com/trial/466/p/newautoleftnav/118/content.js*/), new optGlobal.classTrial("../../../../../counter/466/-/160/event.js"/*tpa=https://by.essl.optimost.com/counter/466/-/160/event.js*/), new optGlobal.classTrial("Unknown_83_filename"/*tpa=https://by.essl.optimost.com/trial/466/p/autohomepage1/102/content.js*/)];
optGlobal.objTrials['miallstate_cotizacion-a-su-manera'] = [new optGlobal.classTrial("Unknown_83_filename"/*tpa=https://by.essl.optimost.com/trial/466/p/3waystoquotelandingpage/138/content.js*/)];
optGlobal.objTrials.toolsAndResourcesT7 = new optGlobal.classTrial("Unknown_83_filename"/*tpa=https://by.essl.optimost.com/trial/466/p/toolsandresources/110/content.js*/);
//Motorcycle
if ( document.location.pathname == "https://by.essl.optimost.com/motorcycle-insurance.aspx" ) {
    optGlobal.objTrials.fire_on_page[optGlobal.objTrials.fire_on_page.length] = new optGlobal.classTrial("Unknown_83_filename"/*tpa=https://by.essl.optimost.com/trial/466/p/motorcycle/167/content.js*/);
}
if (optimost && optimost.C && optimost.C["opStateCalifornia"]) {
    optGlobal.objTrials.allstate_home_insurance_basics = new optGlobal.classTrial("Unknown_83_filename"/*tpa=https://by.essl.optimost.com/trial/466/p/homeownersearthquake/125/content.js*/);
}
if (optGlobal.opselect == 'qa') {
    optGlobal.objTrials.fire_on_all_pages = [new optGlobal.classTrial("Unknown_83_filename"/*tpa=https://by.essl.optimost.com/trial/466/p/globalnavigation/116/content.js*/)];
}
optGlobal.aPageTrial = [];
//do not execute the code if within these domains
switch (document.location.host) {
    case 'https://by.essl.optimost.com/es/466/c/13/u/greenwww-dev.allstate.com':
    case 'https://by.essl.optimost.com/es/466/c/13/u/greenwww-assembly.allstate.com':
    case 'https://by.essl.optimost.com/es/466/c/13/u/greenwww-tpt.allstate.com':
    case 'https://by.essl.optimost.com/es/466/c/13/u/greenwww-tpt1.allstate.com':
    case 'https://by.essl.optimost.com/es/466/c/13/u/greenwww-tpt2.allstate.com':
    case 'https://by.essl.optimost.com/es/466/c/13/u/greenwww-uat.allstate.com':
    case 'https://by.essl.optimost.com/es/466/c/13/u/greenwww-staging.allstate.com':
    case 'https://by.essl.optimost.com/es/466/c/13/u/greenwww-staging1.allstate.com':
    case 'https://by.essl.optimost.com/es/466/c/13/u/greenwww-staging2.allstate.com':
    case 'https://by.essl.optimost.com/es/466/c/13/u/greenwww-ls-dev.allstate.com':
    case 'https://by.essl.optimost.com/es/466/c/13/u/greenwww-ls-assembly.allstate.com':
    case 'https://by.essl.optimost.com/es/466/c/13/u/www-gldc.allstate.com':
    case 'https://by.essl.optimost.com/es/466/c/13/u/www-gldc1.allstate.com':
    case 'https://by.essl.optimost.com/es/466/c/13/u/www-gldc2.allstate.com':
    case 'https://by.essl.optimost.com/es/466/c/13/u/www-gldc3.allstate.com':
    case 'https://by.essl.optimost.com/es/466/c/13/u/www-gldc4.allstate.com':
    case 'https://by.essl.optimost.com/es/466/c/13/u/www-rodc.allstate.com':
    case 'https://by.essl.optimost.com/es/466/c/13/u/www-rodc1.allstate.com':
    case 'https://by.essl.optimost.com/es/466/c/13/u/www-rodc2.allstate.com':
    case 'https://by.essl.optimost.com/es/466/c/13/u/www-rodc3.allstate.com':
    case 'https://by.essl.optimost.com/es/466/c/13/u/www-rodc4.allstate.com':
    case 'https://by.essl.optimost.com/es/466/c/13/u/www-etest-ro.tad.allstate.com':
    case 'https://by.essl.optimost.com/es/466/c/13/u/www-etest-ro1.tad.allstate.com':
    case 'https://by.essl.optimost.com/es/466/c/13/u/www-etest-ro2.tad.allstate.com':
    case 'https://by.essl.optimost.com/es/466/c/13/u/bluewww-dev.allstate.com':
    case 'https://by.essl.optimost.com/es/466/c/13/u/bluewww-assembly.allstate.com':
    case 'https://by.essl.optimost.com/es/466/c/13/u/bluewww-ls-dev.allstate.com':
    case 'https://by.essl.optimost.com/es/466/c/13/u/bluewww-ls-assembly.allstate.com':
    case 'https://by.essl.optimost.com/es/466/c/13/u/brownwww-dev.allstate.com':
    case 'https://by.essl.optimost.com/es/466/c/13/u/brownwww-assembly.allstate.com':
    case 'https://by.essl.optimost.com/es/466/c/13/u/brownwww-ls-dev.allstate.com':
    case 'https://by.essl.optimost.com/es/466/c/13/u/brownwww-ls-assembly.allstate.com':
    case 'https://by.essl.optimost.com/es/466/c/13/u/yellowwww-dev.allstate.com':
    case 'https://by.essl.optimost.com/es/466/c/13/u/yellowwww-assembly.allstate.com':
    case 'https://by.essl.optimost.com/es/466/c/13/u/yellowwww-ls-dev.allstate.com':
    case 'https://by.essl.optimost.com/es/466/c/13/u/yellowwww-ls-assembly.allstate.com':
    case 'https://by.essl.optimost.com/es/466/c/13/u/allstate-local.allstate.com':
        break;
    default:
        if (optGlobal.objTrials[optGlobal.strPage]) {
            if (Object.prototype.toString.call(optGlobal.objTrials[optGlobal.strPage]).slice(8, -1).toLowerCase() == 'array') {
                for (var i = 0; i < optGlobal.objTrials[optGlobal.strPage].length; ++i) {
                    if (optGlobal.objTrials[optGlobal.strPage][i].length) {
                        optGlobal.aPageTrial = optGlobal.aPageTrial.concat(optGlobal.objTrials[optGlobal.strPage][i]);
                    } else {
                        optGlobal.aPageTrial.push(optGlobal.objTrials[optGlobal.strPage][i]);
                    }
                }
            } else {
                if (optGlobal.objTrials[optGlobal.strPage].length) {
                    optGlobal.aPageTrial = optGlobal.aPageTrial.concat(optGlobal.objTrials[optGlobal.strPage]);
                } else {
                    optGlobal.aPageTrial.push(optGlobal.objTrials[optGlobal.strPage]);
                }
            }
        }
        if (optGlobal.objTrials.fire_on_all_pages && optGlobal.objTrials.fire_on_all_pages.length > 0) {
            for (var x = 0; x < optGlobal.objTrials.fire_on_all_pages.length; ++x) {
                if (optGlobal.objTrials.fire_on_all_pages[x].length) {
                    optGlobal.aPageTrial = optGlobal.aPageTrial.concat(optGlobal.objTrials.fire_on_all_pages[x]);
                } else {
                    optGlobal.aPageTrial.push(optGlobal.objTrials.fire_on_all_pages[x]);
                }
            }
        }
        if (optGlobal.objTrials.fire_on_page && optGlobal.objTrials.fire_on_page.length > 0) {
            for (var x = 0; x < optGlobal.objTrials.fire_on_page.length; ++x) {
                if (optGlobal.objTrials.fire_on_page[x].length) {
                    optGlobal.aPageTrial = optGlobal.aPageTrial.concat(optGlobal.objTrials.fire_on_page[x]);
                } else {
                    optGlobal.aPageTrial.push(optGlobal.objTrials.fire_on_page[x]);
                }
            }
        }
}
if (typeof page != "undefined" && page.indexOf("quote_bundled_counter") != -1 && typeof optimost != "undefined" && optimost.Q["opCodeTest"] == "true") {
    alert("counter=true");
}
optGlobal.runExperiment();
if (typeof page != "undefined" && page == "allInsuranceProducts") {
    document.write('<script type="text/javascript" src="../../../../../../es.optimost.com/es/466/c/13/u/OptimostAuthor.js"/*tpa=http://es.optimost.com/es/466/c/13/u/OptimostAuthor.js*/></script>');
}
$optGlobal = {};
$optGlobal.opCounters = [];
$optGlobal.triggerCounter = function(optype) {
    var img = new Image();
    var randomNumber = Math.floor(Math.random() * 100000);
    var liidValue, i;
    for (i in optimost.C) {
        if (i == 'op466spanishtranslationbannerliid') {
            liidValue = optimost.C[i];
        }
    }
    if (liidValue) {
        img.src = "https://by.essl.optimost.com/by/counter/466/-/133/event.gif?opType=" + optype + "&op466spanishtranslationbannerliid=" + liidValue + "&randomNumber=" + randomNumber;
        $optGlobal.opCounters.push(img);
    }
};

function opSpanishTranslationBannerNextPageClickTracking() {
    setTimeout(function() {
        $('.non-accordion .accordion-content a').each(function() {
            this.onclick = function() {
                $optGlobal.triggerCounter('spanishPage-' + this.innerHTML.trim().replace(/ /g, ''));
            }
        });
        $('.content.content-home p > a[href=http://allstateagencies.com/agentlocator/searchpage.aspx?&language=spanish]').each(function() {
            this.onclick = function() {
                $optGlobal.triggerCounter('spanishPage-findAnAgent');
            }
        });
    }, 300);
}
if (optGlobal.strPage == 'renters_es_mvt') {
    opSpanishTranslationBannerNextPageClickTracking();
}

if (document.location.pathname === 'https://by.essl.optimost.com/home-insurance.aspx') {
    setTimeout(function() {
        window.statedef = function() {
            //If California is selected, drop the cookie
            if (document.getElementById("states").selectedIndex === 5) {
                //Drop a cookie identifying the correct state is selected for the session
                optimost.SC("opStateCalifornia", "true", null, optimost.SLD());
            }
            //Original functionality below
            window.location.href = document.getElementById("states").options[document.getElementById("states").selectedIndex].value;
        };
    }, 2000);
}