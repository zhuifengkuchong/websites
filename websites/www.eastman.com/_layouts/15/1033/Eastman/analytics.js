yepnope({
    test: window.jQuery,
    nope: '../../../../JavaLibrary/jquery-1.5.1.min.js'/*tpa=http://www.eastman.com/JavaLibrary/jquery-1.5.1.min.js*/,
    complete: function () {
        //window.load
        $(window).load(function () {
            //documents
            var ext = new Array();
            ext = ['pdf', 'doc', 'docx', 'zip', 'ppt', 'pptx'];

            for (var i = 0; i < ext.length; i++) {
                var fileType = 'a[href$="' + ext[i] + '"]';

                jQuery(fileType).live('click', function () {
                    var link = jQuery(this).attr("href");
                    ga('send', 'pageview', link);

                    //catch plasticizers for custom logging
                    if (document.URL.indexOf("/Brands/Eastman_plasticizers/") > -1) {
                        ga('send', 'event', 'Download', 'click', link + " " + document.URL.replace(/^.*[\\\/]/, ''));
                    }
                    else {
                        ga('send', 'event', 'Download', 'click', link);
                    }
                });
            }
            //end documents

            ///Image Clicks
            $('img[class]').click(function () {

                // classNames will contain all applied classes
                var classNames = $(this).attr('class').split(/\s+/);
                // iterate over all class  
                $.each(classNames, function (index, item) {
                    // Find class that starts with trackimage-
                    if (item.indexOf("trackimage-") == 0) {
                        var imageName = item.substr(item.indexOf("-") + 1);
                        ga('send', 'event', 'image', 'click', imageName);
                    }
                });

            });
            ///end Image Clicks

            ///Left Nav Clicks
            $(".leftNav a").click(function () {
                ga('send', 'event', 'Left Nav', 'click', $(this).attr("href"));
            });
            ///end Left Nav Clicks

            ///External Links
            var externalLinks = $("a[href^='http://']").filter(function () {
                return this.hostname && this.hostname !== location.hostname;
            });

            externalLinks.click(function () {
                ga('send', 'event', 'External Link', 'click', $(this).attr("href"));
            });
            //end external links

            //Top Nav
            $("#EastmanTopNav a").click(function () {
                ga('send', 'event', 'Top Nav', 'click', $(this).attr("href"));
            });

            //Sec Top Nav
            $(".secondaryTopNav a").click(function () {
                ga('send', 'event', 'Secondary Top Nav', 'click', $(this).attr("href"));
            });

            //Brand Top Nav
            $("#brandTopNav a").click(function () {
                ga('send', 'event', 'Brand Top Nav', 'click', $(this).attr("href"));
            });

            //Bottom Nav
            $(".divBottomNav a").click(function () {
                ga('send', 'event', 'Bottom Nav', 'click', $(this).attr("href"));
            });

            //Mailto Links
            $("a[href^='mailto:']").click(function () {
                ga('send', 'event', 'Mails', 'click', $(this).attr("href").substring(7));
            });

            //videos
            var videos = $("video");

            videos.bind('play', function (e) {
                ga('send', 'event', 'Videos', 'play', e.currentTarget.currentSrc);
            });

            videos.bind('pause', function (e) {
                var vid = e.currentTarget;
                //get percentage of video watched when paused
                var percent = Math.round((vid.currentTime / vid.duration) * 100) + '%';
                //log pause and percentage of video watched as separate events
                ga('send', 'event', 'Videos', 'pause', vid.currentSrc);
                ga('send', 'event', 'Videos', 'percentage watched', vid.currentSrc + ' / ' + percent);
            });

            videos.bind('ended', function (e) {
                var vid = e.currentTarget;
                var percent = '100%';
                //log 100% of video watched when video ends since pause doesn't catch this
                ga('send', 'event', 'Videos', 'percentage watched', vid.currentSrc + ' / ' + percent);
            });
            //end videos

            //open all external links in a new tab
            $("a[href^='http://'], a[href^='https://']").filter(function () {
                return this.hostname && this.hostname !== location.hostname;
            }).attr("target", "_blank");

            //open all pdfs in a new tab
            $("a[href$='.pdf']").attr("target", "_blank");

            //add PeeZee image to left nav on plasticizer pages
            if (window.location.pathname.toLowerCase().indexOf("/brands/eastman_plasticizers/") != -1) {
                $("#tdLeftNav").append("<a href='http://www.eastman.com/Brands/Eastman_plasticizers/products/Pages/ProductHome.aspx?product=71100713' target='_blank'><img src='../../../../Brands/Eastman_plasticizers/PublishingImages/PZ-022F-FINAL.jpg'/*tpa=http://www.eastman.com/Brands/Eastman_plasticizers/PublishingImages/PZ-022F-FINAL.jpg*/ alt='PeeZee' style='width:175px;margin-left:15px;'/></a>");
            }

            //Marketo code on Omnia pages
            if (window.location.pathname.toLowerCase().indexOf("/brands/eastman_omnia/") != -1) {
                (function () {
                    var didInit = false;
                    function initMunchkin() {
                        if (didInit === false) {
                            didInit = true;
                            Munchkin.init('820-QKS-307', { "wsInfo": "i1RU" });
                        }
                    }
                    var s = document.createElement('script');
                    s.type = 'text/javascript';
                    s.async = true;
                    s.src = '../../../../../munchkin.marketo.net/munchkin.js'/*tpa=http://munchkin.marketo.net/munchkin.js*/;
                    s.onreadystatechange = function () {
                        if (this.readyState == 'complete' || this.readyState == 'loaded') {
                            initMunchkin();
                        }
                    };
                    s.onload = initMunchkin;
                    document.getElementsByTagName('head')[0].appendChild(s);
                })();
            }
            //end Marketo code on Omnia pages

            //Product Selector
            if (window.location.pathname.toLowerCase().indexOf("http://www.eastman.com/products/pages/product_selector.aspx") != -1) {
                //Product Group selected
                $("select[id$='lstProductGroup']").change(function () {
                    ga('send', 'event', 'Product Selector', 'Product Groups - Group clicked', $("select[id$='lstProductGroup'] option:selected").text());
                });

                //Product Groups - Product List clicked
                $(".link-group-products").click(function () {
                    ga('send', 'event', 'Product Selector', 'Product Groups - Product List clicked', $(this).attr("href"));
                });

                //Product Groups - Property Comparison clicked
                $(".link-group-comparison").click(function () {
                    ga('send', 'event', 'Product Selector', 'Product Groups - Property Comparison clicked', $(this).attr("href"));
                });

                //Product Brands - Brand Overview clicked
                $(".link-brand-overview").click(function () {
                    ga('send', 'event', 'Product Selector', 'Product Brands - Brand Overview clicked', $(this).attr("href"));
                });

                //Product Brands - Product List clicked
                $(".link-brand-products").click(function () {
                    ga('send', 'event', 'Product Selector', 'Product Brands - Product List clicked', $(this).attr("href"));
                });

                //Product Brands - Property Comparison clicked
                $(".link-brand-comparison").click(function () {
                    ga('send', 'event', 'Product Selector', 'Product Brands - Property Comparison clicked', $(this).attr("href"));
                });
                
                //Links clicked in Product List, Technologies, and Quick Links sections
                $("a.featurehometext").click(function () {
                    ga('send', 'event', 'Product Selector', 'Feature clicked', $(this).text());
                });
            }
            //end Product Selector

        });
        //end window.load
    }
});