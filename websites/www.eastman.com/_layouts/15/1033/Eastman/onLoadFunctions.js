/*******************************************************************************
' Name		On load javascript functions 
'
' Description		This file is used to handle the page load functions			
'
' Modification Log
'
'       Date                Author                  Modification 
'    23-Oct-2008   			Rakesh A R.             Initial Version
'*******************************************************************************/

function fnOnload() {
    // Use this to call all other functions

    // Setting default values for fields
    fnSetDefaultValuesForFields();

    // Display Line Break
    fnTitleValidate();

    // Set Style of Input Form Text Box
    fnSetInputFormTextBoxStyle();

    // Set home page background
    fnSetHomePageBackground();

    // Redirect to home page from 404
    fnRedirectToHomePage();
}


/*******************************************************************************
' Function Name		fnSetDefaultValuesForFields	(and related functions)			
'
' Description		This function sets the field default values
'					on pageload.
' Modification Log
'
'   Date       		Author                	Modification 
' 	23-Oct-2008	   	Rakesh A R			  	Initial version
'*******************************************************************************/

/*******************************************************************************
SharePoint Field Type	|	identifier		|	tagName
*******************************************************************************
Single Line of Text 	|	TextField 		|	input
Multiple Lines of Text 	|	TextField 		|	input
Number 			        |	TextField 		|	input
Choice (dropdown) 	    |	DropDownChoice	| 	select
Yes/No 			        |	BooleanField 	|	input
'*******************************************************************************/

function fnSetDefaultValuesForFields() {
    // Set the default values
    fnSetDefaultValues("select", "DropDownChoice", "Display Left Nav", "Yes");
    fnSetDefaultValues("select", "DropDownChoice", "Site Index", "No");
    fnSetDefaultValues("select", "DropDownChoice", "Window Style", "Self");
    fnSetDefaultValues("select", "DropDownChoice", "Display Line Break", "Yes");
    fnSetDefaultValues("select", "DropDownChoice", "Navigation Control", "Left Nav");
    fnSetDefaultValues("select", "DropDownChoice", "PlaceHolder Alignment", "Vertical");
    fnSetDefaultValues("select", "DropDownChoice", "Display Horizontal Nav", "Yes");
    fnSetDefaultValues("select", "DropDownChoice", "Language", "English");
    fnSetDefaultValues("select", "DropDownChoice", "Latest News", "Yes");
    fnSetDefaultValues("select", "DropDownChoice", "Whats New", "Yes");
    fnSetDefaultValues("select", "DropDownChoice", "Quarterly Earnings", "No");
    fnSetDefaultValues("input", "BooleanField", "Show RSS Feed", "Yes");
    fnSetDefaultValues("select", "DropDownChoice", "Region", "All Regions");
    fnSetDefaultValues("select", "DropDownChoice", "Site Type", "All Site Types");
}


function fnSetDefaultValues(tagName, identifier, title, value) {
    var theSelect = fnGetTagFromIdentifierAndTitle(tagName.toLowerCase(), identifier.toLowerCase(), title.toLowerCase());

    if (tagName.toLowerCase() == "select") {
        if (theSelect != null && theSelect.value == "")
            fnSetSelectedOption(theSelect, value);
    }
    else if (tagName.toLowerCase() == "input") {
        if (identifier.toLowerCase() == "booleanfield") {
            if (theSelect != null && theSelect.checked == false)
                theSelect.checked = true;
        }
        else {
            if (theSelect != null && theSelect.value == "")
                theSelect.value = value;
        }
    }
}

function fnSetSelectedOption(select, value) {
    var opts = select.options;
    var l = opts.length;

    if (select == null) return;

    for (var i = 0; i < l; i++) {
        if (opts[i].value.toLowerCase() == value.toLowerCase()) {
            select.selectedIndex = i;
            return true;
        }
    }
    return false;
}

function fnGetTagFromIdentifierAndTitle(tagName, identifier, title) {
    var len = identifier.length;
    var tags = document.getElementsByTagName(tagName);

    for (var i = 0; i < tags.length; i++) {
        var tempString = tags[i].id.toLowerCase();

        if (tags[i].title.toLowerCase() == title && (identifier == "" || tempString.indexOf(identifier) == tempString.length - len)) {
            return tags[i];
        }
    }

    return null;
}


/*******************************************************************************
' Function Name		fnTitleValidate				
'
' Description		This function handles hiding and displaying of underline
'					on pageload.
' Modification Log
'
'   Date       		Author                	Modification 
' 	23-Oct-2008	   	Rakesh A R			  	Initial version
'*******************************************************************************/

function fnTitleValidate() {
    if (document.getElementById("ctl00_PlaceHolderMain_rchPageTitle__ControlWrapper_RichHtmlField")) {
        var tableText = document.getElementById("ctl00_PlaceHolderMain_rchPageTitle__ControlWrapper_RichHtmlField");

        if (tableText.innerHTML == "") {
            document.getElementById("TitleRow").style.display = "none";
        }
    }

    if (document.getElementById("pageTitle")) {
        var tableText = document.getElementById("pageTitle");

        if (tableText.innerHTML == "") {
            document.getElementById("TitleRow").style.display = "none";
        }
    }
}

/*******************************************************************************
' Function Name		fnSetInputFormTextBoxStyle				
'
' Description		This function handles hiding and displaying of InputFormTextBox
'					on pageload.
' Modification Log
'
'   Date       		Author                	Modification 
' 	01-Jan-2009	   	Lakshman Raju		  	Initial version
'*******************************************************************************/

function fnSetInputFormTextBoxStyle() {
    if (document.getElementById("ctl00_PlaceHolderMain_fldFlashContent_ctl00_iftHtmlContent1_iframe")) {
        var iftiFrame = document.getElementById("ctl00_PlaceHolderMain_fldFlashContent_ctl00_iftHtmlContent1_iframe");
        iftiFrame.style.width = "100%";
    }

    if (document.getElementById("ctl00_PlaceHolderMain_fldFlashContent_ctl00_iftHtmlContent1_toolbar")) {
        var iftToolbar = document.getElementById("ctl00_PlaceHolderMain_fldFlashContent_ctl00_iftHtmlContent1_toolbar");
        iftToolbar.style.width = "100%";
    }
}

/*******************************************************************************
' Function Name		fnSetHomePageBackground				
'
' Description		This function handles the background change on the home page
'
' Modification Log
'
'   Date       		Author                	Modification 
' 	20-Feb-2009	   	Devender Mallya		  	Initial version
'*******************************************************************************/
function fnSetHomePageBackground() {
    // Only if the table exists
    if (document.getElementById('tblHomePage')) {
        var tblHomePage = document.getElementById('tblHomePage');

        // Get the location of BG images
        var imgLoc = '/SiteCollectionImages/HomePage/';

        // Set background for first link mouseover
        if (document.getElementById('achHome1')) {
            var achHome1 = document.getElementById('achHome1');

            if (achHome1.getElementsByTagName('img').length > 0) {
                var imgIndex1 = achHome1.getElementsByTagName('img')[0].src.lastIndexOf('/') + 3;

                var imgName1 = achHome1.getElementsByTagName('img')[0].src.substring(imgIndex1);

                achHome1.onmouseover = function ()
                { tblHomePage.style.background = 'url(' + imgLoc + imgName1 + ')'; };
            }
        }

        // Set background for second link mouseover
        if (document.getElementById('achHome2')) {
            var achHome2 = document.getElementById('achHome2');

            if (achHome2.getElementsByTagName('img').length > 0) {
                var imgIndex2 = achHome2.getElementsByTagName('img')[0].src.lastIndexOf('/') + 3;

                var imgName2 = achHome2.getElementsByTagName('img')[0].src.substring(imgIndex2);

                achHome2.onmouseover = function ()
                { tblHomePage.style.background = 'url(' + imgLoc + imgName2 + ')'; };
            }
        }

    }

}

/*******************************************************************************
' Function Name		fnRedirectToHomePage				
'
' Description		This function handles the redirection to home page from the 404 page
'
' Modification Log
'
'   Date       		Author                	Modification 
' 	22-Mar-2009	   	Devender Mallya		  	Initial version
'*******************************************************************************/
function fnRedirectToHomePage() {
    // Only if the table exists
    if (document.getElementById('tblPageNotFound')) {
        setTimeout("window.location = '../../../../Pages/Home.aspx.htm'/*tpa=http://www.eastman.com/Pages/Home.aspx*/;", 20000);
    }
}

// Push onload function

_spBodyOnLoadFunctionNames.push("fnOnload");

//set top padding of the workspace to the height of the ribbon
function setTopPadding() {
    var wrkElem = document.getElementById('s4-workspace');

    if (document.getElementById('ms-designer-ribbon')) {
        var ribbonHeight = document.getElementById('ms-designer-ribbon').offsetHeight;
        if (window.location.search.match("[?&]IsDlg=1")) {
            //margin works better for dialogs b/c of scrollbars
            wrkElem.style.marginTop = ribbonHeight + 'px';
            wrkElem.style.paddingTop = '0px';
        }
        else {
            //padding works better for the main window
            wrkElem.style.paddingTop = ribbonHeight + 'px';
        }
    }
}

// bind top padding reset to ribbon resize event so that the page always lays out correctly.
ExecuteOrDelayUntilScriptLoaded(function () { SP.UI.Workspace.add_resized(setTopPadding); }, "Unknown_83_filename"/*tpa=http://www.eastman.com/_layouts/15/1033/Eastman/init.js*/);