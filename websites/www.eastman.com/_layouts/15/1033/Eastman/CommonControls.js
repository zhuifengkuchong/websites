function ClearText(txtControl) {
    if (document.getElementById(txtControl) != null) {
        document.getElementById(txtControl).value = "";
        document.getElementById(txtControl).focus();
    }
}

function OpenTestLink(id) {
    var txtUrl = fnTrim(document.getElementById(id).value);

    if (txtUrl != '' && fnIsValidUrl(txtUrl)) {
        if (txtUrl.substr(0, 4).toLowerCase() == 'www.')
            txtUrl = 'http://' + txtUrl;
        window.open(txtUrl, '_blank');
    }
}

function fnIsValidUrl(url) {
    var str = url.replace('http://', '');

    if (str == '' || (url.indexOf('http://') > -1 && str.substr(0, 1) == '/'))
        return false;
    else
        return true;

}

/*******************************************************************************
' Function Name		fnShowDynamicImg				
'
' Description		This function displays the dynamic image for left nav hover
'
' Modification Log
'
'     Date                   Author                	 Modification 
' 	23-Dec-2008   		  Naveen Kumar             Initial Version
'*******************************************************************************/
function fnShowDynamicImg(sImg, sURL) {
    var defaultDivText = document.getElementById("divDefaultContent");
    var divText = document.getElementById("divDynamicContent");
    if (divText != null) {
        divText.style.display = "inline";
        var tdText = document.getElementById("tdDynamicContent");
        if (tdText != null) {
            tdText.innerHTML = "<a href='" + sURL + "'><img border=0 align='middle' src='" + sImg + "' /></a>";
        }
    }
    if (defaultDivText != null) {
        defaultDivText.style.display = "none";
    }
}

/*******************************************************************************
' Function Name		fnShowDefaultContent				
'
' Description		This function displays the default content for left nav hover
'
' Modification Log
'
'     Date                   Author                	 Modification 
' 	23-Dec-2008   		  Naveen Kumar             Initial Version
'*******************************************************************************/
function fnShowDefaultContent() {
    var defaultDivText = document.getElementById("divDefaultContent");
    var divText = document.getElementById("divDynamicContent");

    if (defaultDivText != null) {
        defaultDivText.style.display = "inline";
    }

    if (divText != null) {
        divText.style.display = "none";
    }

}

/*******************************************************************************
' Function Name		fnOpenMICWindow				
'
' Description		This function handles the window open event for MIC
'
' Modification Log
'
'     Date                   Author                	 Modification 
' 	23-Nov-2008   		Devender Mallya             Initial Version
'*******************************************************************************/
function fnOpenMICWindow(sUrl) {
    window.open(sUrl, 'OFTMessage', 'RESIZABLE=1,SCROLLBARS=YES,STATUS=0,TOOLBAR=0,width=680,height=500,top=50,left=50');
}

/*******************************************************************************
' Function Name		fnOpenNewWindow				
'
' Description		This function opens a new window
'
' Modification Log
'
'     Date                   Author                	 Modification 
' 	23-Nov-2008   		Devender Mallya             Initial Version
'*******************************************************************************/
function fnOpenNewWindow(sUrl) {
    window.open(sUrl);
}

/*******************************************************************************
' Function Name		fnMailLink			
'
' Description		This function mails a provided link
'
' Modification Log
'
'     Date                   Author                	 Modification 
' 	23-Nov-2008   		Devender Mallya             Initial Version
'*******************************************************************************/
function fnMailLink(sUrl) {
    window.open(sUrl, 'OFTMessage', 'RESIZABLE=1,SCROLLBARS=YES,STATUS=0,TOOLBAR=0,width=50,height=20,top=50,left=50');
}

function fnTrim(str) {
    return str.replace(/^\s+|\s+$/g, '');
}

/*******************************************************************************
' Function Name		fnSortHandler
'
' Description		This function saves the appropriate values in hidden 
'					variables for sorting of Campus Event Calendar. This is called 
'					on click of the hyperlinks provided for sorting.
'
' Modification Log
'
'     Date                  Author                        Modification 
' 	01-Dec-2008			  Rakesh A R		             Initial Version
'*******************************************************************************/
function fnSortHandler(hdnSortID, hdnChangeID, SortBy) {
    if (document.getElementById(hdnSortID).value == 'School_Org') {
        document.getElementById(hdnSortID).value = 'Date';
    }
    else {
        document.getElementById(hdnSortID).value = 'School_Org';
    }

    document.getElementById(hdnChangeID).value = 'True';
}

function MM_preloadImages() {
    var d = document; if (d.images) {
        if (!d.MM_p) d.MM_p = new Array();
        var i, j = d.MM_p.length, a = MM_preloadImages.arguments; for (i = 0; i < a.length; i++)
            if (a[i].indexOf("#") != 0) { d.MM_p[j] = new Image; d.MM_p[j++].src = a[i]; }
    }
}

function MM_swapImgRestore() {
    var i, x, a = document.MM_sr; for (i = 0; a && i < a.length && (x = a[i]) && x.oSrc; i++) x.src = x.oSrc;
}

function MM_findObj(n, d) {
    var p, i, x; if (!d) d = document; if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
        d = parent.frames[n.substring(p + 1)].document; n = n.substring(0, p);
    }
    if (!(x = d[n]) && d.all) x = d.all[n]; for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
    for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document);
    if (!x && d.getElementById) x = d.getElementById(n); return x;
}

function MM_swapImage() {
    var i, j = 0, x, a = MM_swapImage.arguments;
    document.MM_sr = new Array;

    for (i = 0; i < (a.length - 2) ; i += 3)

        if ((x = MM_findObj(a[i])) != null) { document.MM_sr[j++] = x; if (!x.oSrc) x.oSrc = x.src; x.src = a[i + 2]; }
}

/*******************************************************************************
' Function Name		fnTopNavSearchTextBoxEnterClick				
'
' Description		This function handles the enter key press. If enter key is 
'					pressed, it goes to the page with search value entered
'
' Modification Log
'
'      Date               Author                      Modification 
' 	10-Oct-2008			Rakesh A R					 Initial version
'*******************************************************************************/
function fnTopNavSearchTextBoxEnterClick(sBaseChannelName, e) {
    var ns = (!document.all) ? true : false
    var ie = (document.all) ? true : false
    var n;
    var enterKey = 13;
    (ns) ? n = e.which : n = e.keyCode

    var SearchValue = encodeURIComponent(document.getElementById('searchFor').value);

    if (n == enterKey) {
        if (fnTrim(SearchValue) == '') {
            window.location = 'http://www.eastman.com/Search/Pages/Search_Results.aspx';
        }
        else {
            if (sBaseChannelName == '') {
                window.location = 'http://www.eastman.com/Search/Pages/Search_Results.aspx?k=' + SearchValue;
            }
            else {
                if (sBaseChannelName.indexOf('http://') == 0) {
                    window.location = sBaseChannelName + '/Search/Pages/Search_Results.aspx?k=' + SearchValue;
                }
                else {
                    window.location = 'http:///' + sBaseChannelName + '/Search/Pages/Search_Results.aspx?k=' + SearchValue;
                }
            }
        }
        return false;
    }
}

/*******************************************************************************
' Function Name		fnTopNavSearchLinkClick				
'
' Description		This function is called when search button is pressed in 
'					top nav
'
' Modification Log
'
'      Date               Author                      Modification 
' 	10-Oct-2008			Rakesh A R					 Initial version
'*******************************************************************************/
function fnTopNavSearchLinkClick(sBaseChannelName) {
    var SearchValue = encodeURIComponent(document.getElementById('searchFor').value);

    if (fnTrim(SearchValue) == '') {
        window.location = 'http://www.eastman.com/Search/Pages/Search_Results.aspx';
    }
    else {
        if (sBaseChannelName == '') {
            window.location = 'http://www.eastman.com/Search/Pages/Search_Results.aspx?k=' + SearchValue;
        }
        else {
            if (sBaseChannelName.indexOf('http://') == 0) {
                window.location = sBaseChannelName + '/Search/Pages/Search_Results.aspx?k=' + SearchValue;
            }
            else {
                window.location = 'http:///' + sBaseChannelName + '/Search/Pages/Search_Results.aspx?k=' + SearchValue;
            }
        }
    }
}

/*******************************************************************************
' Function Name		fnTrim				
'
' Description		This function trims a selected string of spaces
'
' Modification Log
'
'      Date               Author                      Modification 
' 	10-Oct-2008			Rakesh A R					 Initial version
'*******************************************************************************/
function fnTrim(str) {
    return str.replace(/^\s+|\s+$/g, '');
}