var RSG = RSG || {};

_spBodyOnLoadFunctionNames.push('RSG.NonRSGPopup.initialize');

RSG.NonRSGPopup = (function () {

    var investorPopup;

    function initialize() {
        getInvestorPopup();
        BindEvents();
    }

    function BindEvents() {
        $(document).on('click', '#acceptInvestorRelationship', function (event) {
            event.preventDefault(event);
            window.parent.open("http://phx.corporate-ir.net/phoenix.zhtml?c=82381&p=irol-irhome");
            //window.opener.close();
            window.parent.location.href = window.parent.location.href;
        });
    }

    function getInvestorPopup() {
        var webToUse;
        var contextToUse;

        contextToUse = SP.ClientContext.get_current();
        webToUse = contextToUse.get_web();

        var reusableList = webToUse.get_lists().getByTitle("Reusable Content");
        var popupCamlQuery = new SP.CamlQuery();
        popupCamlQuery.set_viewXml(getPopupCaml());
        investorPopup = reusableList.getItems(popupCamlQuery);
        contextToUse.load(investorPopup, 'Include(ReusableHtml, Title)');

        contextToUse.executeQueryAsync(
                                    Function.createCallback(renderPopup),
                                    Function.createCallback(onSearchFailure)
                                  );
    }

    function getPopupCaml() {

        var query = "<View><Query>";
        query += "<Where>";
        query += "<Eq>";
        query += "<FieldRef Name='Title' />";
        query += "<Value Type='Text'>Investor_Popup</Value>";
        query += "</Eq>";
        query += "</Where>";
        query += "</Query>";
        query += "</View>";

        return query;

    }

    function renderPopup() {
        var listItemEnumerator = investorPopup.getEnumerator();
        var returnItem = "";

        while (listItemEnumerator.moveNext()) {
            var listItem = listItemEnumerator.get_current();
            var tempTitle = listItem.get_item("Title");
            if (tempTitle == "Investor_Popup") {
                returnItem = listItem.get_item("ReusableHtml");
                break;
            }
        }

        $('#popupDiv').html(returnItem);

    }

    function onSearchFailure(sender, args, err) {
        var errTxt = err;
    }

    return {
        getInvestorPopup: getInvestorPopup,
        initialize: initialize
    };
})();
