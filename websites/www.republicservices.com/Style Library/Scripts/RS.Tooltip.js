_spBodyOnLoadFunctionNames.push('initialize');


function initialize() {

    BindEvents();

}

function BindEvents() {
    $(window).resize(function () {
        $('[data-toggle="popover"]').popover('hide');
    });



    $('[data-toggle="popover"]').popover({
        container: "body",
        // Add more else stuff with parentheses for more sizes

        placement: function (context, source) {
            var $win, $source, winWidth, popoverWidth, popoverHeight, offset, toRight, toLeft, placement, scrollTop;
            $win = $(window);
            $source = $(source);
            placement = $source.attr('data-placement');
            popoverWidth = 400;
            popoverHeight = 110;
            offset = $source.offset();

            // Check for horizontal positioning and try to use it.
            if (placement == 'right' || placement == 'left' || placement == undefined) {
                winWidth = $win.width();
                toRight = winWidth - offset.left - source.offsetWidth;
                toLeft = offset.left;

                if (placement === 'left') {
                    if (toLeft > popoverWidth) {
                        return 'left';
                    }
                    else if (toRight > popoverWidth) {
                        return 'right';
                    }
                }
                else {
                    if (toRight > popoverWidth) {
                        return 'right';
                    }
                    else if (toLeft > popoverWidth) {
                        return 'left';
                    }
                }
            }

            // Handle vertical positioning.
            scrollTop = $win.scrollTop();
            if (placement === 'bottom') {
                if (($win.height() + scrollTop) - (offset.top + source.offsetHeight) > popoverHeight) {
                    return 'bottom';
                }
                return 'top';
            }
            else {
                if (offset.top - scrollTop > popoverHeight) {
                    return 'top';
                }
                return 'bottom';
            }
        }
    });

    $('[data-toggle="popover"]').click(function (e) {
        $('[data-toggle="popover"]').not(this).popover('hide');
        e.stopPropagation();
    });

    $(document).click(function (e) {
        if (($('.popover').has(e.target).length == 0) || $(e.target).is('.close')) {
            $('[data-toggle="popover"]').popover('hide');
        }
    });
}