//Change Notes

//Murali:12/22/2014
//Added new methods setPageProductID, getPageProductID, clearPageProductID to implement scenario-5 requirements on request services page.

//Murali: 11/11/2014
//Added new method "fixFooterPosition" to set the footer based on body height

//Murali: 11/05/2014
//Added condition to validateZipCodeRequired method to fix the zipcode validation issue(1234-)

//Murali : 10/31/2014
//Modified Textbox event from "KeyUp" to "KeyPress", because in some cases keyup event not fired in IE

//Jeff: 10/23/2014
//added dynamic rendering of the sidebar menu

//Jeff : 10/22/2014
//changed from using cookies to using localstorage for persistent data.
//added common function to load menu icons when needed. 
//removed LOB js files from appropriate page layouts, since we are loading
//them in core now.
//added the function to route the user to their preferred LOB based on
//where they had been previously.

//Jeff : 10/20/2014
//Added code to put the name attribute back on the radio buttons. This
//allows us put the customer type buttons in the page properties instead
//of the page layout.

//Murali : 10/16/2014
//To Close the video and set seeking time to starting position by clicking on close button in modal.

//Murali : 10/07/2014
//Hiding the default generated sharepoint object(macSharePointPlugin) for mac os(iphone and ipad) -Added code in document.ready function
//Added method to stop the vide playback (stopVideo)

//Jeff: 10/06/2014
//some cleanup. moving to adding event handlers to the .click instead of the way we do it now.
//also adding common code to get the address validation markup from reusable content
//and wiring it up. will also put this markup in sessionstorage for performance reasons.
//I'm adding this functionality in it's own namespace. RS.core.addressValidation

//Murali: 10/06/2014
//Updated document.ready function to fix the footer float - right issue in tab view

//Jeff: 09/25/2014
//added logic to redirect users based on their previous segmentation
//added logic to clear that segmentation based on a click of the logo

//Hidayath: 09/25/2014
//updated the top navigation link highlighting as it is not working for L3 Pages.
// Updated code in getProductIconCamlQuery() to accomodate C2bresidential ServiceLineIcons

//Jeff: 09/24/2014
//Due to weird issues with some of LT's implementation, made RS.master static in response to the business
//Since it is static now, removed those methods around building the menu for the master page
//Also changed the way the current menu item is highlighted, to keep it consistent
//with the way LT was doing it. Also simplified it a little.
//TODO: fold all the methods into RS.core namespace
//**************************************************************

var siteUrl = '/';
var results;
var menuItemCollection;
var context;
var web;
var list;
var tIDs = [];
var menuIcons = [];
var navigationTerm;
var newUserPopupContent;
var nonSupportIEContent;
var mobileMenuItemCollection;

var contextCookie = function (zipcode, address, lob) {
    this.zipcode = zipcode;
    this.address = address;
    this.lob = lob;
    this.scope = "none";
}

function menuIconItem(title, targetUrl, cssClass, sidebarCssClass) {
    this.Title = title;
    this.TargetURL = targetUrl;
    this.CSSClassName = cssClass;
    this.SidebarCSSClassName = sidebarCssClass;
}

function mobileMenuIconItem(title, targetUrl, cssClass, sidebarCssClass, PageNavigationTarget, Rank) {
    this.Title = title;
    this.TargetURL = targetUrl;
    this.CSSClassName = cssClass;
    this.SidebarCSSClassName = sidebarCssClass;
    this.PageNavigationTarget = PageNavigationTarget;
    this.Rank = Rank;
}

_spBodyOnLoadFunctionNames.push('coreInitialize');

//Jeff: 09/25/2014
//added logic to route based on lob preference

//Jeff: 09/24/2014
//added this logic for the homepage logo and mobile view
//since we aren't doing it in the master page anymore.
function coreInitialize() {

//setting canonical URL for the page as what user sees in browser as per HD ticket 1063824
$('link[rel=canonical]').remove();
$('head').append('<link rel="canonical" href="' + window.location.href  + '">');

    if ($('#global-search-2').length > 0) {

        $('#global-search-2').keypress(function (e) {
            if (e.keyCode == '13') {
                e.preventDefault();
                RS.core.doSearch($('#global-search-2'));
            }
        });

        $('#global-search-2').val(RS.core.getQueryString("k", "", false));
        $(this).toggleClass('active');
        $(this).parent().toggleClass('active');

    }

    var ieVersion = RS.core.getIEVersion();

    if ((ieVersion != 0) && (ieVersion < 9)) {
        RS.core.showNonSupportIEPopup();
    }
    else {
        //Shows new user popup if applicable.
        RS.core.newUserPopUp();
    }

    //fixNoResults();

    fixAllContentZonesForU200B();

    var pageToken = RS.core.getPageToken();

    if (pageToken.length == 0) {
        $("#bottom_logo").hide();
        $(".footer_menu").css("float", "left");
    }
    else {
        $("#bottom_logo").show();
    }

    $('#home_logo').on("click", RS.core.clearLobPreference);
    $('#bottom_logo').on("click", RS.core.clearLobPreference);
    $('#ftBusinesDevelopment').click(function () {
        RS.core.setLobPreference('businesses');
    });

    $('#global-search').keypress(function (e) {
        if (e.keyCode == '13') {
            e.preventDefault();
            RS.core.doSearch($('#global-search'));
        }
    });


    if ($('#section-search').length > 0) {

        $('#section-search').keypress(function (e) {
            if (e.keyCode == '13') {
                e.preventDefault();
                RS.core.doSearch($('#section-search'));
            }
        });
    }

    RS.core.setLobPreference();

    RS.core.setAddressFromContext();

    RS.core.setCustomerType();

    HighlightingMenuItem();


    //Murali - 10/07/2014
    //To hide iphone/ipad sharepoint object below the footer
    $(window).bind("load", function () {
        if ($('#macSharePointPlugin').length > 0)
            $('#macSharePointPlugin').hide();
    });

    //Request Services - business functionality change - Pritesh
    $(document).on('click', '#requestServices', function () {
        CheckIfComingFromBusiness();
    });


    $('.customer_type #liResident').attr("onclick", "$(this).tab('show');");
    $('.customer_type #liBusiness').attr("onclick", "$(this).tab('show');");


    //Murali - 10/16/2014
    //To Close the video and set seeking time to starting position.
    $('.modal-controls a').click(function () {
        $('.modalVideo')[0].pause();
        $('.modalVideo')[0].currentTime = 0;
    });


    //Jeff - 10/20/2014
    //fix customer type option buttons
    //that sharepoint doesn't like.
    //this lets us put all the content in the page
    //properties instead of in the page layout
    fixUpCustomerType();

    loadServiceIconsIfNeeded();

    RS.core.routeBasedOnLobPreference();

    fixPlaceholders();

    //Murali - 11/26/2014
    //Based on Scope (Local/Regioanal/National) redirect user to request service/Contact Us
    if ($('.businessScope').length > 0) {
        var tempCookie = RS.core.getContextCookie();
        if (tempCookie != null) {

            var tempScope = tempCookie.scope;
            if (tempScope == "regional" || tempScope == "national") {
                $('.businessScope').attr('href', '/customer-support/contact-us');
            }
            else {
                $('.businessScope').attr('href', '/request-services');
            }
        }
    }

    //Murali - 12/22/2014
    //set Product ID for Product Pages if applicable
    RS.core.setPageProductID();

    //Clear Page Product ID cookie if user directly click on request services link from Global Navigation
    $('#gblnkRequestServices').on("click", RS.core.clearPageProductID);

    //Murali : To Clear business Scope cookie on clicking of Global/Main Menu/Footer Menu's
    $('header .main #main_menu li a').on("click", clearBusinessScopeCookie);
    $('header .utility-nav li a').on("click", clearBusinessScopeCookie);
    $('footer .footer_menu li a').on("click", clearBusinessScopeCookie);

    ProcessImn();
    ProcessImnMarkers();
}

//functions which can be used to alert the sharepoint user for presence of active-x controls.
//These are used for displaing Active-X controls warning to the users.
function ProcessImn() { }
function ProcessImnMarkers() { }


//Murali: Clear Business Scope Cookie
function clearBusinessScopeCookie() {
    RS.core.setBusinessScope("none");
}


function executeSiteSearch(keyword) {

    var searchQuery = getSiteSearchQuery(keyword);

    $.ajax(
        {
            url: searchQuery,
            method: "GET",
            headers:
            {
                "accept": "application/json;odata=verbose",
            },
            success: executeSiteSearchSuccess,
            error: onSearchFailure
        }
    );

}

function executeSiteSearchSuccess(data) {

    var searchResultsCount = data.d.query.PrimaryQueryResult.RelevantResults.Table.Rows.results.length;

    if (searchResultsCount <= 0) {
        var markup = "<div class='noResultsSuggestionsDiv'>";
        markup += "<div class='noResultsSuggestions'>Here Are Some Suggestions:</div>";
        markup += "<ul>";
        markup += "<li>Make sure all words are spelled correctly</li>";
        markup += "<li>Try different search terms</li>";
        markup += "<li>Try more general search terms</li>";
        markup += "<li>Try fewer search terms</li>";
        markup += "<li>Contact <a href='http://www.republicservices.com/customer-support'>Customer Support</a> to speak to a representative.</li>";
        markup += "</ul>";
        markup += "</div>";
        $("#results-header").html("Nothing here matches your search");
        $("#WebPartWPQ3 #NoResult").html(markup);
    }
    else {
        $("#results-header").html("Here's what we found for you");
    }
}

function onSearchFailure(sender, args, err) {
    var errTxt = err;
}

// this gets query which fetches list of all SIteIDs which have Drop Off service
function getSiteSearchQuery(keyword) {
    var searchQuery = _spPageContextInfo.webAbsoluteUrl + "/_api/search/query?";
    searchQuery += "querytext='" + keyword + "'";
    searchQuery += "&rowlimit=500&QueryTemplatePropertiesUrl='spfile://webroot/queryparametertemplate.xml'";
    return searchQuery;
}

//Checking is user is New/Existing user based on browser cookie(IsNewUser) 
function newUserPopUp() {

    var userType = $.cookie('IsNewUser');

    //If user is new then show the popup
    if ((userType == 'undefined') || (userType == "") || (userType == null)) {
        checkIsNewUserPopupEnabled();
    }

}

//Checking is popup enabled from Reusable content
function checkIsNewUserPopupEnabled() {

    var webToUse;
    var contextToUse;
    contextToUse = SP.ClientContext.get_current();
    webToUse = contextToUse.get_web();

    var newUserPopup = webToUse.get_lists().getByTitle("Reusable Content");
    var newUserPopupCamlQuery = new SP.CamlQuery();
    newUserPopupCamlQuery.set_viewXml(getNewUserPopupCaml());
    newUserPopupContent = newUserPopup.getItems(newUserPopupCamlQuery);
    contextToUse.load(newUserPopupContent, 'Include(ReusableHtml, ReusableText, Title)');

    contextToUse.executeQueryAsync(
                                    Function.createCallback(loadIsNewUserPopupEnabled),
                                    Function.createCallback(retrieveListItemsFail)
                                  );
}

//if popup is enabled then bind html markup from Reusable content list to Popup window and show popup
function loadIsNewUserPopupEnabled() {

    var listItemEnumerator = newUserPopupContent.getEnumerator();
    var isEnabled;
    var newUserPopupMarkup = "";

    while (listItemEnumerator.moveNext()) {
        var listItem = listItemEnumerator.get_current();
        var tempTitle = listItem.get_item("Title");
        if (tempTitle == "NewUserPopupEnable") {
            isEnabled = listItem.get_item("ReusableText");
            break;
        }
    }

    if ((isEnabled == "True") || (isEnabled == true)) {
        while (listItemEnumerator.moveNext()) {
            var listItem = listItemEnumerator.get_current();
            var tempTitle = listItem.get_item("Title");
            if (tempTitle == "NewUserPopupMarkup") {
                newUserPopupMarkup = listItem.get_item("ReusableHtml");
                break;
            }
        }

        //Get popup content from reusable list and load popup window
        $("#divNewUserPopup").html(newUserPopupMarkup);
        $("#btnCloseNewUserPopup").click(function () {
            $("#divNewUserPopupModal").modal("hide");
        });
        $("#divNewUserPopupModal").modal("show");

        //Set cookie value
        $.cookie('IsNewUser', 'False', { expires: 365 }); //365-number of days to expire the cookie
    }

}



function getNewUserPopupCaml() {

    var query = "<View><Query>";
    query += "<Where>";
    query += "<Or>";
    query += "<Eq>";
    query += "<FieldRef Name='Title' />";
    query += "<Value Type='Text'>NewUserPopupEnable</Value>";
    query += "</Eq>";
    query += "<Or>";
    query += "<Eq>";
    query += "<FieldRef Name='Title' />";
    query += "<Value Type='Text'>NewUserPopupMarkup</Value>";
    query += "</Eq>";
    query += "<Eq>";
    query += "<FieldRef Name='Title' />";
    query += "<Value Type='Text'>CSR Phone Number</Value>";
    query += "</Eq>";
    query += "</Or>";
    query += "</Or>";
    query += "</Where>";
    query += "</Query>";
    query += "</View>";
    return query;

}

function getNonSupportIEPopupCaml() {

    var query = "<View><Query>";
    query += "<Where>";
    query += "<Eq>";
    query += "<FieldRef Name='Title' />";
    query += "<Value Type='Text'>NonSupportIEPopup</Value>";
    query += "</Eq>";
    query += "</Where>";
    query += "</Query>";
    query += "</View>";
    return query;

}


function fixNoResults() {

    var rawtoken = window.location.href.toLowerCase();
    if (rawtoken.indexOf("http://www.republicservices.com/results.aspx") > 0) {
        var searchKey = $('#global-search-2').val();
        executeSiteSearch(searchKey);
    }
}

function fixFooterPosition() {
    var bodyHeight = $("body").height();
    var windowHeight = $(window).height();
    if (windowHeight > bodyHeight) {
        $("footer").css("position", "fixed").css("bottom", 0).css("width", "100%");
    }
}

function fixAllContentZonesForU200B() {
    fixContentZoneForU200B('#ContentSection1');
    fixContentZoneForU200B('#ContentSection2');
    fixContentZoneForU200B('#ContentSection3');
    fixContentZoneForU200B('#ContentSection4');
    fixContentZoneForU200B('#ContentSection5');
    fixContentZoneForU200B('#ContentSection6');
    fixContentZoneForU200B('#ContentSection7');
    fixContentZoneForU200B('#ContentSection8');
    fixContentZoneForU200B('#ContentSection9');
    //have to call this after we fix up the content
    $(document).ready(function () {
        $($.fn.cycle.defaults.autoSelector).cycle();
    });
}

function fixPlaceholders() {
    if (RS.core.isIE9()) {
        fixSingleplaceholder($('#zip'), "Zip Code");
        fixSingleplaceholder($('#search-zip'), "Zip Code");
        fixSingleplaceholder($('#search-address'), "Address");
        fixSingleplaceholder($('#regional-search-zip'), "Zip Code");
        fixSingleplaceholder($('#regional-search-address'), "Address");
    }
}

function fixSingleplaceholder(element, placeholder) {
    if (element.length > 0) {
        element.prop("placeholder", placeholder);
        placeHolderInit(element[0]);
    }

}

function fixContentZoneForU200B(elementid) {
    var elementToClean = $(elementid);
    if (elementToClean.length > 0) {
        elementToClean.html(elementToClean.html().replace(/\u200B/g, ''));
    }
}

function loadServiceIconsIfNeeded() {
    //Reading value from PageToken instead of sessionStorage
    //menuIcons = JSON.parse(sessionStorage.getItem("menuicons"));
    //if (menuIcons == null || menuIcons.length == 0)
    //{
    if ($(".category_nav").length > 0 || $(".sidebar-menu").length > 0) {

        GetServiceLineIcons(RS.core.getPageToken());
    }

    loadMobileMenuIcons();
    //}
    //else
    //{
    //renderMenuIcons();
    //renderSidebarMenu();
    //}
}

function loadMobileMenuIcons() {
    var mobileMenuIcons = sessionStorage.getItem("mobileMenuIcons");

    if (mobileMenuIcons != null) {
        renderMobileMenuIcons();
    }
    else {
        bindMobileMenuIcons();
    }

}

function bindMobileMenuIcons() {

    var clientContext = new SP.ClientContext('/'); //gets the current context
    var web = clientContext.get_web(); //gets the web object
    var list = web.get_lists();
    var targetList = list.getByTitle("RS.MenuIcons"); //get the list details
    var camlQuery = new SP.CamlQuery(); //initiate the query object
    camlQuery.set_viewXml(getMobileMenuIconCamlQuery());
    mobileMenuItemCollection = targetList.getItems(camlQuery);
    //returns the item collectionbased on the query
    clientContext.load(mobileMenuItemCollection, 'Include(Title, CSSClassName, SidebarCSSClassName, TargetUrl,PageNavigationTarget,Rank)');
    clientContext.executeQueryAsync(Function.createCallback(bindMobileMenuIconsSuccess), Function.createCallback(retrieveListItemsFail));

}

function bindMobileMenuIconsSuccess() {
    var iconsList = mobileMenuItemCollection.getEnumerator();
    var mobileMenuIcons = [];
    while (iconsList.moveNext()) {
        var oListItem = iconsList.get_current();
        var newItem = new mobileMenuIconItem(oListItem.get_item('Title'), oListItem.get_item('TargetUrl').get_url(), oListItem.get_item('CSSClassName'), oListItem.get_item('SidebarCSSClassName'), oListItem.get_item('PageNavigationTarget'), oListItem.get_item('Rank'));
        mobileMenuIcons.push(newItem);
    }
    sessionStorage.setItem("mobileMenuIcons", JSON.stringify(mobileMenuIcons));

    renderMobileMenuIcons();
}

function renderSidebarMenu() {
    if ($(".sidebar-menu").length > 0) {
        var markup = "";
        $(".sidebar-menu").append(markup);

        for (var i = 0; i < menuIcons.length; i++) {
            var menuitem = menuIcons[i];
            var itemUrl = menuitem.TargetURL;
            var activeClass = "";
            var currentUrl = document.URL.toLowerCase();
            currentUrl = currentUrl.split("#")[0];
            currentUrl = currentUrl.split("?")[0];
            if (currentUrl == itemUrl.toLowerCase()) {
                activeClass = "active";
            }

            markup += "<li>";
            markup += "<a class='" + menuitem.SidebarCSSClassName + " " + activeClass + "' href='" + itemUrl + "'>";
            markup += menuitem.Title + "</a>";
            markup += "</li>";
        }

        $(".sidebar-menu").html(markup);

        if ($(".sidebar-tools .next").length > 0) {
            renderPreviousNextMenu();
        }
    }
}

function renderMobileMenuIcons() {

    var mobileMenuIcons = [];
    mobileMenuIcons = JSON.parse(sessionStorage.getItem("mobileMenuIcons"));
    if (mobileMenuIcons.length > 0) {
        var residentialMenuIcons = [];
        var businessMenuIcons = [];
        var communityMenuIcons = [];
        var residentialMenuMarkup = "";
        var businessMenuMarkup = "";
        var communityMenuMarkup = "";

        for (var i = 0; i < mobileMenuIcons.length; i++) {

            var menuitem = mobileMenuIcons[i];
            var lableName = menuitem.PageNavigationTarget.Label.toLowerCase();
            if (lableName == "residents") {
                residentialMenuIcons.push(menuitem);
            }
            else if (lableName == "businesses") {
                businessMenuIcons.push(menuitem);
            }
            else if (lableName == "communities") {
                communityMenuIcons.push(menuitem);
            }
        }

        residentialMenuIcons.sort(function (a, b) {
            return (a.Rank - b.Rank);
        });
        businessMenuIcons.sort(function (a, b) {
            return (a.Rank - b.Rank);
        });
        communityMenuIcons.sort(function (a, b) {
            return (a.Rank - b.Rank);
        });



        for (var i = 0; i < residentialMenuIcons.length; i++) {
            var menuitem = residentialMenuIcons[i];
            var itemUrl = menuitem.TargetURL;
            var activeClass = "";
            var currentUrl = document.URL.toLowerCase();
            currentUrl = currentUrl.split("#")[0];
            currentUrl = currentUrl.split("?")[0];
            if (currentUrl == itemUrl.toLowerCase()) {
                activeClass = "active";
            }
            residentialMenuMarkup += "<li>";
            residentialMenuMarkup += "<a class='" + menuitem.SidebarCSSClassName + " " + activeClass + "' href='" + itemUrl + "'>";
            residentialMenuMarkup += menuitem.Title + "</a>";
            residentialMenuMarkup += "</li>";

        }
        for (var i = 0; i < businessMenuIcons.length; i++) {
            var menuitem = businessMenuIcons[i];
            var itemUrl = menuitem.TargetURL;
            var activeClass = "";
            var currentUrl = document.URL.toLowerCase();
            currentUrl = currentUrl.split("#")[0];
            currentUrl = currentUrl.split("?")[0];
            if (currentUrl == itemUrl.toLowerCase()) {
                activeClass = "active";
            }
            businessMenuMarkup += "<li>";
            businessMenuMarkup += "<a class='" + menuitem.SidebarCSSClassName + " " + activeClass + "' href='" + itemUrl + "'>";
            businessMenuMarkup += menuitem.Title + "</a>";
            businessMenuMarkup += "</li>";



        }
        for (var i = 0; i < communityMenuIcons.length; i++) {
            var menuitem = communityMenuIcons[i];
            var itemUrl = menuitem.TargetURL;
            var activeClass = "";
            var currentUrl = document.URL.toLowerCase();
            currentUrl = currentUrl.split("#")[0];
            currentUrl = currentUrl.split("?")[0];
            if (currentUrl == itemUrl.toLowerCase()) {
                activeClass = "active";
            }
            communityMenuMarkup += "<li>";
            communityMenuMarkup += "<a class='" + menuitem.SidebarCSSClassName + " " + activeClass + "' href='" + itemUrl + "'>";
            communityMenuMarkup += menuitem.Title + "</a>";
            communityMenuMarkup += "</li>";

        }

        $('.mobile_dropdown .residential ul').empty();
        $('.mobile_dropdown .residential ul').html(residentialMenuMarkup);
        $('.mobile_dropdown .business ul').empty();
        $('.mobile_dropdown .business ul').html(businessMenuMarkup);
        $('.mobile_dropdown .community ul').empty();
        $('.mobile_dropdown .community ul').html(communityMenuMarkup);
    }
}

function getCurrentProductIndex() {
    var currentIndex = -1;
    var docURL = document.URL.toLowerCase();
    if (docURL.indexOf('#') > 0) {
        docURL = docURL.substr(0, docURL.indexOf('#'));
    }
    for (var i = 0; i < menuIcons.length; i++) {
        if (menuIcons[i].TargetURL.toLowerCase() == docURL) {
            currentIndex = i;
            break;
        }
    }

    return currentIndex;
}


function renderPreviousNextMenu() {
    var nextMessage = "";
    var previousMessage = "";
    var currentIndex = getCurrentProductIndex();
    var nextUrl;
    var previousUrl;
    if (currentIndex > -1) {
        var nextIndex = currentIndex + 1;
        var prevIndex = currentIndex - 1;
        if (nextIndex >= menuIcons.length) {
            nextIndex = 0;
        }
        if (currentIndex == 0) {
            prevIndex = menuIcons.length - 1;
        }
        nextMessage = menuIcons[nextIndex].Title + "<span class='next_arrow'></span>";
        previousMessage = menuIcons[prevIndex].Title + "<span class='previous_arrow'></span>";
        nextUrl = menuIcons[nextIndex].TargetURL;
        previousUrl = menuIcons[prevIndex].TargetURL;
    }
    $(".sidebar-tools .next .message").html(nextMessage);
    $(".sidebar-tools .next .message").addClass(menuIcons[nextIndex].SidebarCSSClassName);
    $(".sidebar-tools .next").prop("href", nextUrl);
    $(".sidebar-tools .previous .message").html(previousMessage);
    $(".sidebar-tools .previous .message").addClass(menuIcons[prevIndex].SidebarCSSClassName);
    $(".sidebar-tools .previous").prop("href", previousUrl);

    //Preparing next and previous links for mobile view
    var mMarkup = "<li class='back'>";
    mMarkup += "<a href='" + menuIcons[prevIndex].TargetURL + "'>Back</a>";
    mMarkup += "</li><li class='next'>";
    mMarkup += "<a href='" + menuIcons[nextIndex].TargetURL + "'>Next</a>";
    mMarkup += "</li>";
    $(".mobile_arrow_nav").html(mMarkup);
}

//Request Services - business functionality change - Pritesh
function CheckIfComingFromBusiness() {
    var tempCookie = RS.core.getContextCookie();
    if (tempCookie != null) {
        tempScope = tempCookie.scope;
        if (tempScope == "regional" || tempScope == "national") {
            $('#requestServices').attr('href', '/customer-support/businesses-contact-us');
        }
        else {
            $('#requestServices').attr('href', '/request-services');
        }
    }
}


function GetServiceLineIcons(headerName) {

    var clientContext = new SP.ClientContext('/'); //gets the current context
    var web = clientContext.get_web(); //gets the web object
    var list = web.get_lists();
    var targetList = list.getByTitle("RS.MenuIcons"); //get the list details
    var camlQuery = new SP.CamlQuery(); //initiate the query object
    camlQuery.set_viewXml(getProductIconCamlQuery());
    menuItemCollection = targetList.getItems(camlQuery);
    //returns the item collectionbased on the query
    clientContext.load(menuItemCollection, 'Include(Title, CSSClassName, SidebarCSSClassName, TargetUrl)');
    clientContext.executeQueryAsync(Function.createCallback(serviceLineIconsSuccess), Function.createCallback(retrieveListItemsFail));

}

function getProductIconCamlQuery() {
    var token = RS.core.getContextLob();
    // to accomodate C2bresidential ServiceLineIcons
    if (token.indexOf("c2bresidents") >= 0) {
        token = "residents";
    }

    var query = "<View><Query>";
    query += "<OrderBy>";
    query += "<FieldRef Name='Rank' />";
    query += "</OrderBy>";
    query += "<Where>";
    query += "<Eq>";
    query += "<FieldRef Name='PageNavigationTarget' />";
    query += "<Value Type='TaxonomyFieldType'>" + token + "</Value>";
    query += "</Eq>";
    query += "</Where>";
    query += "</Query></View>";

    return query;
}

function getMobileMenuIconCamlQuery() {
    var query = "<View><Query>";
    query += "<OrderBy>";
    query += "<FieldRef Name='PageNavigationTarget' />";
    query += "</OrderBy>";
    query += "</Query></View>";

    return query;
}

//Jeff:09/08/2014
//changed to use sprite and class name instead of an img tag
function serviceLineIconsSuccess() {

    var serviceIconsList = menuItemCollection.getEnumerator();
    menuIcons = [];

    while (serviceIconsList.moveNext()) {
        var oListItem = serviceIconsList.get_current();
        var newItem = new menuIconItem(oListItem.get_item('Title'), oListItem.get_item('TargetUrl').get_url(), oListItem.get_item('CSSClassName'), oListItem.get_item('SidebarCSSClassName'));
        menuIcons.push(newItem);
    }
    sessionStorage.setItem("menuicons", JSON.stringify(menuIcons));
    renderMenuIcons();
    renderSidebarMenu();
    if (RS.core.getPageToken() == "businesses") {
        renderBusinessType();
    }
}

function renderBusinessType() {
    var sBusinessContent = "<select id='businessTypeDdl'><option>Select Business Type</option>";

    for (var i = 0; i < menuIcons.length; i++) {
        var menuitem = menuIcons[i];
        //<option value="Retail">Retail</option>
        sBusinessContent += "<option value='" + menuitem.TargetURL + "'>" + menuitem.Title + "</option>";
    }

    sBusinessContent += "</select>";

    $("#businessTypeDiv").html(sBusinessContent);

    $('#businessTypeDdl').change(function () {
        window.location.href = this.options[this.selectedIndex].value;
    });
}

function renderMenuIcons() {
    if ($(".category_nav").length > 0) {
        var sMenuContent = "<ul class='clearfix'>";

        for (var i = 0; i < menuIcons.length; i++) {
            var menuitem = menuIcons[i];
            sMenuContent += "<li>";
            sMenuContent += "<a class='" + menuitem.CSSClassName + "' href='" + menuitem.TargetURL + "'>";
            sMenuContent += menuitem.Title + "</a>";
            sMenuContent += "</li>";
        }

        sMenuContent += "</ul>";

        $("#serviceLineIcons").html(sMenuContent);

        //To fit menu prev and next icons based on screen size.
        setServiceLineIconsWidth();
    }
}


//Murali : 11/13/2014
//Eliminating ALL.js method on window.load and window.resize functions and calling only for L2 pages
//Reset Service Line icon menu's

//Used to fit service line icons menu for L2 Pages
var category_nav_w = 0;
var win_w;
var is_mobile;
var is_tablet;
var is_desktop;
var category_nav_inc;
//Used to fit service line icons menu for L2 Pages - END

$(window).resize(function () {
    //Checking L2 Pages
    if ($('#serviceLineIcons').length > 0) {
        initializeServiceLineIconVariables();
        adjust_category_nav();
    }

});

function initializeServiceLineIconVariables() {
    var win_w = viewport().width;
    var is_mobile = (win_w < 768) ? true : false;
    var is_tablet = ((win_w >= 768) && (win_w <= 1279)) ? true : false;

}

function setServiceLineIconsWidth() {

    initializeServiceLineIconVariables();
    var is_desktop = (is_mobile || is_tablet) ? false : true;
    var category_nav_inc = (is_desktop) ? 8 : 1;

    $('.category_nav .nav_carousel').on('jcarousel:reload jcarousel:create', function () {
        // On init/reload
    }).jcarousel({
        // wrap: 'circular'
    }).swipe({
        swipeLeft: function (event, direction, distance, duration, fingerCount) {
            $(this).jcarousel('scroll', '+=1');
        },
        swipeRight: function (event, direction, distance, duration, fingerCount) {
            $(this).jcarousel('scroll', '-=1');
        }, excludedElements: ['a']		// Allow links to be swiped
    });

    // Init controls
    $('.category_nav .nav_ctrl .prev').on('jcarouselcontrol:active', function () {
        $(this).removeClass('inactive');
    }).on('jcarouselcontrol:inactive', function () {
        $(this).addClass('inactive');
    }).jcarouselControl({ target: '-=' + category_nav_inc });

    $('.category_nav .nav_ctrl .next').on('jcarouselcontrol:active', function () {
        $(this).removeClass('inactive');
    }).on('jcarouselcontrol:inactive', function () {
        $(this).addClass('inactive');
    }).jcarouselControl({ target: '+=' + category_nav_inc });

    var iconsWidth = 0;
    var count = 0;
    $('#serviceLineIcons ul li').each(function () {
        iconsWidth += $(this).width();
        count++;
    });

    $('#serviceLineIcons ul').css("width", iconsWidth + count + 1);

    category_nav_w = iconsWidth + count + 1;

    adjust_category_nav();

}

function viewport() {

    var e = window, a = 'inner';

    if (!('innerWidth' in window)) {
        a = 'client';
        e = document.documentElement || document.body;
    }

    return { width: e[a + 'Width'], height: e[a + 'Height'] };
}

function adjust_category_nav() {

    var container_w = $('.category_nav .container').outerWidth();

    if ((category_nav_w > container_w) || (is_mobile)) {
        $('.category_nav .nav_ctrl').fadeIn('fast');
        $('.category_nav .nav_carousel').css('margin', '0px 50px');
    }
    else {
        $('.category_nav .nav_ctrl').fadeOut('fast');
        $('.category_nav .nav_carousel').css('margin', '0px 0px');
    }
}

//Reset Service Line icon menu's -END



function retrieveListItemsFail(sender, args) {
    RS.core.writeToLog('Failed to get list items. Error:' + args.get_message());
}

//Jeff:09/24/2014
//LT eliminated the active class and went to a dynamically
//added inline style. 

// Hidayath: 09/25/2014
//updated the top navigation link highlighting as it is not working for L3 Pages.
function HighlightingMenuItem() {
    $('#main_menu a').each(function (index) {
        if (window.location.href.indexOf(this.href.trim()) >= 0)
            $(this).css("border-bottom", "1px solid #004a7c");
    });
}

function fixUpCustomerType() {
    if ($(".customer_type").length > 0) {
        $(".customer_type input").each(
            function () {
                $(this).prop("name", "customer-type");
            });
    }
}


//jeff added
var RS = RS || {};

RS.core = (function () {

    function doSearch(inputField) {
        var k = inputField.val();
        window.location.href = "http://www.republicservices.com/search/pages/results.aspx?k=" + k;

    }

    function routeBasedOnLobPreference() {
        var lob = getLobPreference();
        if (document.location.pathname.length == 1) {
            if (typeof lob === 'undefined' || lob == "undefined" || lob == null || lob == "") {
                //do nothing
            }
            else {
                if (lob.length > 0) {
                    window.location.href = "../../index.htm"/*tpa=http://www.republicservices.com/*/ + lob;
                }
            }
        }
    }

    function setLobPreference(lob) {
        if (typeof lob === 'undefined' || lob == "undefined" || lob == null || lob == "") {
            if (RS.core.getPageToken().length > 0) {
                var lobContext = getContextLob();
                if (lobContext == "residents" || lobContext == "businesses" || lobContext == "communities") {
                    setCookie("lobPreference", lobContext);
                }
            }
        }
        else {
            setCookie("lobPreference", lob);
        }
    }

    function clearLobPreference() {
        setCookie("lobPreference", "");
    }

    function setBusinessScope(scope) {
        sessionStorage.setItem("businessScope", scope);
    }
    function getBusinessScope() {
        return sessionStorage.getItem("businessScope");
    }
    function getLobPreference() {
        return getCookie("lobPreference");
    }

    function setAddressFromContext() {
        var tempCookie = RS.core.getContextCookie();

        if (tempCookie != null) {
            var zipcode = tempCookie.zipcode;
            var address = tempCookie.address;
            if (zipcode != null && zipcode != "" && zipcode.toLowerCase().indexOf('zip') < 0) {
                $('#zip').val(zipcode);
                $('#search-zip').val(zipcode);
                $('#regional-search-zip').val(zipcode);
                $('#ZipPostalCode').val(zipcode);
                $('#PostalCode').val(zipcode);
                $('#txtService_ZipCode').val(zipcode);
            }

            if (address != null && address != "" && address.toLowerCase() != 'address') {
                $('#search-address').val(tempCookie.address);
                $('#regional-search-address').val(tempCookie.address);
                $('#Address').val(tempCookie.address);
                $('#AddressLine1').val(tempCookie.address);
                $('#StreetAddress').val(tempCookie.address);
                $('#txtService_AddressLine1').val(tempCookie.address);
                $('#txtBilling_AddressLine1').val(tempCookie.address);
            }
        }
    }

    function setCustomerType() {
        var lob = getLobPreference();
        //this tells us if we have a customer type on the page.
        if ($(".customer_type").length > 0) {
            if (typeof lob === 'undefined' || lob == null || lob == "") {
                //$('#resident').prop('checked', true);
                if (RS.core.getPageToken() == "understanding your bill") {
                    $('#resident').prop('checked', true);
                }
                else {
                    $('#resident').prop('checked', false);
                    $('#business').prop('checked', false);
                    $('#community').prop('checked', false);
                }
            }
            else {
                if (lob == "residents") {
                    $('#resident').prop('checked', true);
                }
                else if (lob == "businesses") {
                    $('#business').prop('checked', true);
                }
                else if (lob == "communities") {
                    if (RS.core.getPageToken() == "understanding your bill") {
                        $('#resident').prop('checked', true);
                    }
                    else {
                        $('#community').prop('checked', true);
                    }
                }
            }
        }
    }

    function getContextLob() {
        var rawtoken = window.location.href.replace(_spPageContextInfo.siteAbsoluteUrl, "").toLowerCase();
        var pageToken = rawtoken.substring(1);
        var lobToken = "";
        var index = pageToken.indexOf("/");
        if (index > -1) {
            pageToken = pageToken.substring(0, index).toLowerCase();
        }
        pageToken = pageToken.split("#")[0];
        pageToken = pageToken.split("?")[0];

        if (pageToken == "residents" || pageToken == "businesses" || pageToken == "communities") {

            lobToken = pageToken;
        }

        return lobToken;
    }

    function setContextCookie(zipcode, address) {
        sessionStorage.setItem("zipcode", zipcode);
        sessionStorage.setItem("address", address);
    }

    function getContextCookie() {
        var returnItem = null;

        try {
            var tempZipcode = sessionStorage.getItem("zipcode");
            var tempAddress = sessionStorage.getItem("address");
            var tempLob = getLobPreference();
            var tempScope = getBusinessScope();

            returnItem = new contextCookie(tempZipcode, tempAddress, tempLob);
            returnItem.scope = tempScope;

        } catch (e) {
            RS.core.writeToLog("No context cookie in session");

        }

        return returnItem;
    }

    function setPageProductID() {

        var pageToken = RS.core.getPageToken();
        if (pageToken != 'request services') {
            clearPageProductID();
        }
        if ($('#divProductID').length > 0) {
            var productID = $('#divProductID a').text();
            setCookie("pageProductID", productID);
            RS.core.setProductPageLOB(RS.core.getLobPreference());
        }

    }

    function getPageProductID() {
        return getCookie("pageProductID");
    }

    function clearPageProductID() {
        setCookie("pageProductID", "");
    }

    function setProductPageLOB(lob) {
        setCookie("productPageLOB", lob);
    }

    function getProductPageLOB() {
        return getCookie("productPageLOB");
    }

    //Jeff: 10/8/2014
    //fixed this to give only the last token
    function getPageToken() {
        var pageToken = getPageNavigationToken();
        pageToken = pageToken.replace(/-/g, " ");
        return pageToken;
    }

    function getExactPageToken() {
        var pageToken = getPageNavigationToken();
        pageToken = pageToken.replace(/-/g, " ");

        return pageToken;
    }

    function getPageNavigationToken() {
        var rawtoken = window.location.href.replace(_spPageContextInfo.siteAbsoluteUrl, "");
        var pageToken = rawtoken.substring(1).toLowerCase();
        pageToken = pageToken.split("#")[0];
        pageToken = pageToken.split("?")[0];
        var lastSlash = pageToken.lastIndexOf("/");
        if (lastSlash > -1) {
            pageToken = pageToken.substring(lastSlash + 1);
        }
        //pagetoken = "residents";
        return pageToken;
    }

    function setCookie(key, value) {
        var storageContainer = window.localStorage || (window.globalStorage ? globalStorage[location.hostname] : null)
        if (storageContainer) {
            storageContainer.setItem(key, value);
        }
    }

    function getCookie(key) {
        var keyValue = "";
        var storageContainer = window.localStorage || (window.globalStorage ? globalStorage[location.hostname] : null)
        if (storageContainer) {
            keyValue = storageContainer.getItem(key);
        }
        return keyValue;
    }

    //generic error handler
    function onSearchFailure(sender, args, e) {
    }

    //borrowed from internets
    function getQueryString(key, default_, useReferrer) {
        if (default_ == null) default_ = "";
        key = key.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regex = new RegExp("[\\?&]" + key + "=([^&#]*)");
        var qs = null;
        if (useReferrer) {
            qs = regex.exec(document.referrer);
        }
        else {
            qs = regex.exec(window.location.href);
        }
        if (qs == null)
            return default_;
        else
            return decodeURI(qs[1]);
    }

    //TO stop the html5 video on close button of modal popup
    function stopVideo() {

        $('#modalVideo')[0].pause();
        $('#modalVideo')[0].currentTime = 0;
    }

    //credit to Brian Swalwell for this routine from the web.
    //Jeff modified to use jQuery
    //we may want to consider making this an extension
    function validateZipCodeRequired(field, errorBlock) {
        var valid = "0123456789-";
        var hyphencount = 0;
        var tempReturn = true;

        if (field.length > 0) {
            var fieldValue = field.val();
            //fix for ie9
            if (RS.core.isIE9()) {
                fieldValue = field[0].value;
                if (fieldValue != "") {
                    if (fieldValue == field[0].placeholder) {
                        fieldValue = "";
                    }
                }
            }
            //end fix for ie9
            if (fieldValue.length == 0) {
                errorBlock.text("Please enter a 5 digit or 5 digit + 4 zip code.");
                tempReturn = false;
            }
            else {
                if (fieldValue.length != 5 && fieldValue.length != 10) {
                    errorBlock.text("Please enter a 5 digit or 5 digit + 4 zip code.");
                    tempReturn = false;
                }
                else {
                    for (var i = 0; i < fieldValue.length; i++) {
                        temp = "" + fieldValue.substring(i, i + 1);
                        if (temp == "-") hyphencount++;
                        if (valid.indexOf(temp) == "-1") {
                            errorBlock.text("Zipcode must be in either 12345 or 12345-6789 format");
                            tempReturn = false;
                        }
                        if ((hyphencount > 0) && (fieldValue.charAt(5) != "-")) {
                            errorBlock.text("Zipcode must be in either 12345 or 12345-6789 format.");
                            tempReturn = false;
                        }
                        if ((hyphencount > 1) || ((fieldValue.length == 10) && "" + fieldValue.charAt(5) != "-")) {
                            errorBlock.text("Zipcode must be in either 12345 or 12345-6789 format.");
                            tempReturn = false;
                        }

                    }
                }
            }

            if (tempReturn) {
                errorBlock.hide();
            }
            else {
                errorBlock.show();
            }
        }

        return tempReturn;
    }

    function writeToLog(message) {
        if (window.console && console.log) {
            console.log(message);
        }
    }

    function validateAddressRequired(field, errorBlock) {

        var tempReturn = false;

        if (field.length > 0) {

            var fieldValue = field.val().trim();
            //fix for ie9
            if (RS.core.isIE9()) {
                fieldValue = field[0].value.trim();
                if (fieldValue != "") {
                    if (fieldValue == field[0].placeholder) {
                        fieldValue = "";
                    }
                }
            }
            //end fix for ie9
            if (fieldValue.length == 0) {
                errorBlock.text("Please enter an address.");
            }
            else {
                tempReturn = true;
            }

            if (tempReturn) {
                errorBlock.hide();
            }
            else {
                errorBlock.show();
            }
        }
        else {
            tempReturn = true;
        }

        return tempReturn;
    }

    function getIEVersion() {
        var ua = window.navigator.userAgent
        var msie = ua.indexOf("MSIE ")

        if (msie > 0)      // If Internet Explorer, return version number
            return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)))
        else                 // If another browser, return 0
            return 0

    }

    function isIE9() {
        return (RS.core.getIEVersion() != 0 && RS.core.getIEVersion() < 10);
    }

    //returns whether the given element has the given class name..
    function hasClassName(element, className) {
        //refactoring of Prototype's function..
        var elClassName = element.className;
        if (!elClassName)
            return false;
        var regex = new RegExp("(^|\\s)" + className + "(\\s|$)");
        return regex.test(element.className);
    }

    function showNonSupportIEPopup() {
        var webToUse;
        var contextToUse;
        contextToUse = SP.ClientContext.get_current();
        webToUse = contextToUse.get_web();

        var nonSupportIEPopup = webToUse.get_lists().getByTitle("Reusable Content");
        var nonSupportIEPopupCamlQuery = new SP.CamlQuery();
        nonSupportIEPopupCamlQuery.set_viewXml(getNonSupportIEPopupCaml());
        nonSupportIEContent = nonSupportIEPopup.getItems(nonSupportIEPopupCamlQuery);
        contextToUse.load(nonSupportIEContent, 'Include(ReusableHtml, ReusableText, Title)');

        contextToUse.executeQueryAsync(
                                        Function.createCallback(showNonSupportIEPopupSuccess),
                                        Function.createCallback(retrieveListItemsFail)
                                      );
    }

    function showNonSupportIEPopupSuccess() {
        var listItemEnumerator = nonSupportIEContent.getEnumerator();
        var nonSupportIEMarkup = "";

        while (listItemEnumerator.moveNext()) {
            var listItem = listItemEnumerator.get_current();
            var tempTitle = listItem.get_item("Title");
            if (tempTitle == "NonSupportIEPopup") {
                nonSupportIEMarkup = listItem.get_item("ReusableHtml");
                break;
            }
        }

        //Get popup content from reusable list and load popup window
        $("#divNonSupportIE").html(nonSupportIEMarkup);

        var ieVersion = RS.core.getIEVersion();
        if (ieVersion != 0 && ieVersion < 8) {
            $("#divNonSupportIEPopup").show();
            $("#divNonSupportIEPopup").css('z-index', '10000');
            $("#divNonSupportIEPopup .modal-close").click(function () { $("#divNonSupportIEPopup").hide(); });
        } else {

            $("#divNonSupportIEPopup").modal("show");
        }

    }



    return {
        getPageToken: getPageToken,
        getPageNavigationToken: getPageNavigationToken,
        getCookie: getCookie,
        setCookie: setCookie,
        onSearchFailure: onSearchFailure,
        getContextCookie: getContextCookie,
        setContextCookie: setContextCookie,
        getContextLob: getContextLob,
        setAddressFromContext: setAddressFromContext,
        getLobPreference: getLobPreference,
        setLobPreference: setLobPreference,
        clearLobPreference: clearLobPreference,
        routeBasedOnLobPreference: routeBasedOnLobPreference,
        setCustomerType: setCustomerType,
        doSearch: doSearch,
        getQueryString: getQueryString,
        stopVideo: stopVideo,
        validateZipCodeRequired: validateZipCodeRequired,
        validateAddressRequired: validateAddressRequired,
        setBusinessScope: setBusinessScope,
        writeToLog: writeToLog,
        getIEVersion: getIEVersion,
        hasClassName: hasClassName,
        isIE9: isIE9,
        fixFooterPosition: fixFooterPosition,
        setPageProductID: setPageProductID,
        getPageProductID: getPageProductID,
        clearPageProductID: clearPageProductID,
        setProductPageLOB: setProductPageLOB,
        getProductPageLOB: getProductPageLOB,
        newUserPopUp: newUserPopUp,
        showNonSupportIEPopup: showNonSupportIEPopup
    };

})();


//This is a fix for placeholder issues in IE9
//From http://stackoverflow.com/questions/6366021/placeholder-in-ie9
(function () {

    "use strict";

    //shim for String's trim function..
    function trim(string) {
        return string.trim ? string.trim() : string.replace(/^\s+|\s+$/g, "");
    }


    function removeClassName(element, className) {
        //refactoring of Prototype's function..
        var elClassName = element.className;
        if (!elClassName)
            return;
        element.className = elClassName.replace(
            new RegExp("(^|\\s+)" + className + "(\\s+|$)"), ' ');
    }

    function addClassName(element, className) {
        var elClassName = element.className;
        if (elClassName)
            element.className += " " + className;
        else
            element.className = className;
    }

    //strings to make event attachment x-browser.. 
    var addEvent = document.addEventListener ?
           'addEventListener' : 'attachEvent';
    var eventPrefix = document.addEventListener ? '' : 'on';

    //the class which is added when the placeholder is being used..
    var placeHolderClassName = 'ie9Fixup';

    //allows the given textField to use it's placeholder attribute
    //as if it's functionality is supported natively..
    window.placeHolderInit = function (textField) {
        //if it's just the empty string do nothing..
        var placeHolder = trim(textField.placeholder);
        if (placeHolder === '')
            return;

        //called on blur - sets the value to the place holder if it's empty..
        var onBlur = function () {
            if (textField.value !== '') //a space is a valid input..
                return;
            textField.value = placeHolder;
            addClassName(textField, placeHolderClassName);
        };

        //the blur event..
        textField[addEvent](eventPrefix + 'blur', onBlur, false);

        //the focus event - removes the place holder if required..
        textField[addEvent](eventPrefix + 'focus', function () {
            if (textField.value.toLowerCase().indexOf('zip') >= 0 || textField.value.toLowerCase().indexOf('address') >= 0) {
                if (RS.core.hasClassName(textField, placeHolderClassName)) {
                    removeClassName(textField, placeHolderClassName);
                    textField.value = "";
                }
            }
        }, false);

        //the submit event on the form to which it's associated - if the
        //placeholder is attached set the value to be empty..
        var form = textField.form;
        if (form) {
            form[addEvent](eventPrefix + 'submit', function () {
                if (RS.core.hasClassName(textField, placeHolderClassName))
                    textField.value = '';
            }, false);
        }

        addClassName(textField, placeHolderClassName);

        onBlur(); //call the onBlur to set it initially..
    };

}());