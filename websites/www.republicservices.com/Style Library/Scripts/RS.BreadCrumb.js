//change log
//Jeff: 10/29
//added an onclick to the home node of the breadcrumb to clear the lob cookie

var RSG = RSG || {};

_spBodyOnLoadFunctionNames.push('RSG.BreadCrumb.initialize');

RSG.BreadCrumb = (function () {
    function initialize() {
        BuildBreadCrumb();
    }

    function BuildBreadCrumb() {
        var part;
        var href = window.location.href;
        href = href.replace("http://", "");
        href = RemoveQueryString(href);
        href = RemoveHash(href);
        var urlParts = new Array();
        urlParts = href.split("/");
        var urlLength = urlParts.length;
        var markup = "";
        if (urlParts[1] != "") {
            if ((urlParts.length == 2 & urlParts[1] != 'residents' & urlParts[1] != 'businesses' & urlParts[1] != 'communities') || urlParts.length > 2) {
                if (urlParts[2] != "c2bresidents") {
                    document.getElementById('divBreadcrumb').style.display = 'block';
                    for (var i = 0; i < urlLength; i++) {
                        if (i == 0) {
                            markup += "<ol class='breadcrumb'><li><a onclick='RS.core.clearLobPreference()' href='http://" + urlParts[i] + "'>Home&nbsp; </a></li>";
                        }
                        else if (i == 1 && urlParts[i] == "case-studies") {
                            //do nothing
                        }
                        else if (i == urlLength - 1) {

                            part = toProperCase(replaceSpecialCharactes(RemoveHash(urlParts[i])));
                            if (part == "Hoa") {
                                part = "HOA";
                            }
                            markup += "<li class='active'>" + part + "</li></ol>";
                        }
                        else {
                            part = toProperCase(replaceSpecialCharactes(urlParts[i]));
                            if (urlParts[i] == "marketing") {
                                markup += "<li><a href='http://www.republicservices.com/request-services'>&nbsp;" + part + "&nbsp; </a></li>";
                            }
                            else {
                                markup += "<li><a href='/" + urlParts[i] + "'>&nbsp;" + part + "&nbsp; </a></li>";
                            }
                        }
                    }
                }
            }
        }
        $('#divBreadcrumb').html(markup);
    }

    function RemoveHash(s) {
        return s.split("#")[0];
    }

    function RemoveQueryString(s) {
        return s.split("?")[0];
    }

    function replaceSpecialCharactes(s) {
        return s.replace(/[^a-zA-Z0-9]/g, ' ');
    }

    function toProperCase(s) {
        if (s != null) {
            return s.toLowerCase().replace(/^(.)|\s(.)/g,
              function ($1) { return $1.toUpperCase(); });
        }
    }


    return {
        initialize: initialize
    };

})();
