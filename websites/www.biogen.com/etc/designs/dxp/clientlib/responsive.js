$(document).ready(function() {
    var windowsize = $(window).width();

    function getDesktopData() {

        $(".responsive-options").each(function(i) {
if($(this).data('desktop') == ""){
				$(this).parent("a").remove();
			}
			else{
				$(this).html($(this).data('desktop'));
			}
            $(this).attr('data-alt', $(this).data('desktopalt'));
            $(this).attr('href', $(this).data('desktoplink'));
            $(this).attr('class', $(this).data('desktopclass'));
            if ($(this).data('targetdesktoplink') === "external") {
                $(this).attr('target', '_blank');
            } else {
                $(this).removeAttr('target');
            }
            if ($(this).data('desktopdisablelink') === "disable") {
                $(this).removeAttr('href');
            }
        });
    }

    function getMobileData() {
        $(".responsive-options").each(function(i) {
            if($(this).data('mobile') == ""){
				$(this).parent('a').remove();
				}
				else{
				$(this).html($(this).data('mobile'));
				}

				$(this).attr('data-alt', $(this).data('mobilealt'));
				$(this).attr('href', $(this).data('mobilelink'));
				$(this).attr('class', $(this).data('mobileclass'));
            if ($(this).data('targetmobilelink') === "external") {
                $(this).attr('target', '_blank');
            } else {
                $(this).removeAttr('target');
            }
            if ($(this).data('mobiledisablelink') == "disable") {
                $(this).removeAttr('href');
            }
        });
    }

    function getResponsiveData() {
        if (windowsize > 769) {
            getDesktopData();
        } else if (windowsize < 769) {
            //if the window is greater than 440px wide then turn on jScrollPane..
            getMobileData();
        } else {
            return;
        }
    }
    getResponsiveData();
    $(window).resize(function() {
        windowsize = $(window).width();
        getResponsiveData();
    });
});
