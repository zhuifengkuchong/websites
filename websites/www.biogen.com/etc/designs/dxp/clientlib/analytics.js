// Media query to determine screen size

function media(){
    var desktop = window.matchMedia("(min-width: 1200px)");
    var tablet = window.matchMedia("(min-width: 768px)");

    if(desktop.matches){
        return "desktop";
    } else if (tablet.matches){
        return "tablet";
    } else {
        return "mobile";
    }
}


// Current time rounded to the last 5 minute mark

function roundToNearestFive(num){
    return (5 * Math.floor(num/5));
}

function padNumbers(num){
    return (num < 10)? "0" + num : num;
}

function formatedDateTime(){ 
    var now = new Date();
    if(typeof now ==='undefined'){
        return 'not available';
    }
    return "" + padNumbers((now.getMonth() + 1))      + "-" 
              + padNumbers(now.getDate())             + "-" 
              + ("" + now.getFullYear()).substring(2) + "|" 
              + padNumbers(now.getHours())            + "-" 
              + padNumbers(roundToNearestFive(now.getMinutes()));
}
