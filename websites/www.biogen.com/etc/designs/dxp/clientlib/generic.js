//Adding target=new attribute to the anchor tag which has whitelisted URL in the entire page.
// Separating local(url) and whitelisted(url) so as to treat internal AEM and whitlisted URL differently.
function listed(url, list){
    if(typeof url !== 'undefined'){
        for(var i = 0; i < list.length; i++){
            if(url.indexOf(list[i]) >= 0){
                return true;
            }
        }
    }
    return false;
}

function whitelisted(url){
    return listed(url, whitelist);
}

function whitelistExempt(url){
	return (local(url)||phone(url)||email(url));
}

function local(url){
	var origin = window.location.origin;
	return ((typeof url !== 'undefined')? Boolean(url.indexOf(origin) === 0 || url.match(/^\/.*/) || url.match(/^#.*/)) : false);
}
function phone(url){local
	return ((typeof url !== 'undefined')? Boolean(url.match(/^tel:.*/i)) :false);
}

function email(url){
	return ((typeof url !== 'undefined')? Boolean(url.match(/^mailto:.*/i)) :false);
}
