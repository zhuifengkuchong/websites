/*
selectivizr v1.0.2 - (c) Keith Clark, freely distributable under the terms 
of the MIT license.

selectivizr.com
*/
/* 
  
Notes about this source
-----------------------

 * The #DEBUG_START and #DEBUG_END comments are used to mark blocks of code
   that will be removed prior to building a final release version (using a
   pre-compression script)
  
  
References:
-----------
 
 * CSS Syntax          : http://www.w3.org/TR/2003/WD-css3-syntax-20030813/#style
 * Selectors           : http://www.w3.org/TR/css3-selectors/#selectors
 * IE Compatability    : http://msdn.microsoft.com/en-us/library/cc351024(VS.85).aspx
 * W3C Selector Tests  : http://www.w3.org/Style/CSS/Test/CSS3/Selectors/current/html/tests/
 
*/

(function(win) {

	// If browser isn't IE, then stop execution! This handles the script 
	// being loaded by non IE browsers because the developer didn't use 
	// conditional comments.
	if (/*@cc_on!@*/true) return;

	// =========================== Init Objects ============================

	var doc = document;
	var root = doc.documentElement;
	var xhr = getXHRObject();
	var ieVersion = /MSIE (\d+)/.exec(navigator.userAgent)[1];
	
	// If were not in standards mode, IE is too old / new or we can't create
	// an XMLHttpRequest object then we should get out now.
	if (doc.compatMode != 'CSS1Compat' || ieVersion<6 || ieVersion>8 || !xhr) {
		return;
	}
	
	
	// ========================= Common Objects ============================

	// Compatiable selector engines in order of CSS3 support. Note: '*' is
	// a placholder for the object key name. (basically, crude compression)
	var selectorEngines = {
		"NW"								: "*.Dom.select",
		"MooTools"							: "$$",
		"DOMAssistant"						: "*.$", 
		"Prototype"							: "$$",
		"YAHOO"								: "*.util.Selector.query",
		"Sizzle"							: "*", 
		"jQuery"							: "*",
		"dojo"								: "*.query"
	};

	var selectorMethod;
	var enabledWatchers 					= [];     // array of :enabled/:disabled elements to poll
	var domPatches							= [];
	var ie6PatchID 							= 0;      // used to solve ie6's multiple class bug
	var patchIE6MultipleClasses				= true;   // if true adds class bloat to ie6
	var namespace 							= "slvzr";
	
	// Stylesheet parsing regexp's
	var RE_COMMENT							= /(\/\*[^*]*\*+([^\/][^*]*\*+)*\/)\s*/g;
	var RE_IMPORT							= /@import\s*(?:(?:(?:url\(\s*(['"]?)(.*)\1)\s*\))|(?:(['"])(.*)\3))[^;]*;/g;
	var RE_ASSET_URL 						= /\burl\(\s*(["']?)(?!data:)([^"')]+)\1\s*\)/g;
	var RE_PSEUDO_STRUCTURAL				= /^:(empty|(first|last|only|nth(-last)?)-(child|of-type))$/;
	var RE_PSEUDO_ELEMENTS					= /:(:first-(?:line|letter))/g;
	var RE_SELECTOR_GROUP					= /(^|})\s*([^\{]*?[\[:][^{]+)/g;
	var RE_SELECTOR_PARSE					= /([ +~>])|(:[a-z-]+(?:\(.*?\)+)?)|(\[.*?\])/g; 
	var RE_LIBRARY_INCOMPATIBLE_PSEUDOS		= /(:not\()?:(hover|enabled|disabled|focus|checked|target|active|visited|first-line|first-letter)\)?/g;
	var RE_PATCH_CLASS_NAME_REPLACE			= /[^\w-]/g;
	
	// HTML UI element regexp's
	var RE_INPUT_ELEMENTS					= /^(INPUT|SELECT|TEXTAREA|BUTTON)$/;
	var RE_INPUT_CHECKABLE_TYPES			= /^(checkbox|radio)$/;

	// Broken attribute selector implementations (IE7/8 native [^=""], [$=""] and [*=""])
	var BROKEN_ATTR_IMPLEMENTATIONS			= ieVersion>6 ? /[\$\^*]=(['"])\1/ : null;

	// Whitespace normalization regexp's
	var RE_TIDY_TRAILING_WHITESPACE			= /([(\[+~])\s+/g;
	var RE_TIDY_LEADING_WHITESPACE			= /\s+([)\]+~])/g;
	var RE_TIDY_CONSECUTIVE_WHITESPACE		= /\s+/g;
	var RE_TIDY_TRIM_WHITESPACE				= /^\s*((?:[\S\s]*\S)?)\s*$/;
	
	// String constants
	var EMPTY_STRING						= "";
	var SPACE_STRING						= " ";
	var PLACEHOLDER_STRING					= "$1";

	// =========================== Patching ================================

	// --[ patchStyleSheet() ]----------------------------------------------
	// Scans the passed cssText for selectors that require emulation and
	// creates one or more patches for each matched selector.
	function patchStyleSheet( cssText ) {
		return cssText.replace(RE_PSEUDO_ELEMENTS, PLACEHOLDER_STRING).
			replace(RE_SELECTOR_GROUP, function(m, prefix, selectorText) {	
    			var selectorGroups = selectorText.split(",");
    			for (var c = 0, cs = selectorGroups.length; c < cs; c++) {
    				var selector = normalizeSelectorWhitespace(selectorGroups[c]) + SPACE_STRING;
    				var patches = [];
    				selectorGroups[c] = selector.replace(RE_SELECTOR_PARSE, 
    					function(match, combinator, pseudo, attribute, index) {
    						if (combinator) {
    							if (patches.length>0) {
    								domPatches.push( { selector: selector.substring(0, index), patches: patches } )
    								patches = [];
    							}
    							return combinator;
    						}		
    						else {
    							var patch = (pseudo) ? patchPseudoClass( pseudo ) : patchAttribute( attribute );
    							if (patch) {
    								patches.push(patch);
    								return "." + patch.className;
    							}
    							return match;
    						}
    					}
    				);
    			}
    			return prefix + selectorGroups.join(",");
    		});
	};

	// --[ patchAttribute() ]-----------------------------------------------
	// returns a patch for an attribute selector.
	function patchAttribute( attr ) {
		return (!BROKEN_ATTR_IMPLEMENTATIONS || BROKEN_ATTR_IMPLEMENTATIONS.test(attr)) ? 
			{ className: createClassName(attr), applyClass: true } : null;
	};

	// --[ patchPseudoClass() ]---------------------------------------------
	// returns a patch for a pseudo-class
	function patchPseudoClass( pseudo ) {

		var applyClass = true;
		var className = createClassName(pseudo.slice(1));
		var isNegated = pseudo.substring(0, 5) == ":not(";
		var activateEventName;
		var deactivateEventName;

		// if negated, remove :not() 
		if (isNegated) {
			pseudo = pseudo.slice(5, -1);
		}
		
		// bracket contents are irrelevant - remove them
		var bracketIndex = pseudo.indexOf("(")
		if (bracketIndex > -1) {
			pseudo = pseudo.substring(0, bracketIndex);
		}		
		
		// check we're still dealing with a pseudo-class
		if (pseudo.charAt(0) == ":") {
			switch (pseudo.slice(1)) {

				case "root":
					applyClass = function(e) {
						return isNegated ? e != root : e == root;
					}
					break;

				case "target":
					// :target is only supported in IE8
					if (ieVersion == 8) {
						applyClass = function(e) {
							var handler = function() { 
								var hash = location.hash;
								var hashID = hash.slice(1);
								return isNegated ? (hash == EMPTY_STRING || e.id != hashID) : (hash != EMPTY_STRING && e.id == hashID);
							};
							addEvent( win, "hashchange", function() {
								toggleElementClass(e, className, handler());
							})
							return handler();
						}
						break;
					}
					return false;
				
				case "checked":
					applyClass = function(e) { 
						if (RE_INPUT_CHECKABLE_TYPES.test(e.type)) {
							addEvent( e, "propertychange", function() {
								if (event.propertyName == "checked") {
									toggleElementClass( e, className, e.checked !== isNegated );
								} 							
							})
						}
						return e.checked !== isNegated;
					}
					break;
					
				case "disabled":
					isNegated = !isNegated;

				case "enabled":
					applyClass = function(e) { 
						if (RE_INPUT_ELEMENTS.test(e.tagName)) {
							addEvent( e, "propertychange", function() {
								if (event.propertyName == "$disabled") {
									toggleElementClass( e, className, e.$disabled === isNegated );
								} 
							});
							enabledWatchers.push(e);
							e.$disabled = e.disabled;
							return e.disabled === isNegated;
						}
						return pseudo == ":enabled" ? isNegated : !isNegated;
					}
					break;
					
				case "focus":
					activateEventName = "focus";
					deactivateEventName = "blur";
								
				case "hover":
					if (!activateEventName) {
						activateEventName = "mouseenter";
						deactivateEventName = "mouseleave";
					}
					applyClass = function(e) {
						addEvent( e, isNegated ? deactivateEventName : activateEventName, function() {
							toggleElementClass( e, className, true );
						})
						addEvent( e, isNegated ? activateEventName : deactivateEventName, function() {
							toggleElementClass( e, className, false );
						})
						return isNegated;
					}
					break;
					
				// everything else
				default:
					// If we don't support this pseudo-class don't create 
					// a patch for it
					if (!RE_PSEUDO_STRUCTURAL.test(pseudo)) {
						return false;
					}
					break;
			}
		}
		return { className: className, applyClass: applyClass };
	};

	// --[ applyPatches() ]-------------------------------------------------
	function applyPatches() {
		var elms, selectorText, patches, domSelectorText;

		for (var c=0; c<domPatches.length; c++) {

			(function(){

				var iterator = c;
				
				setTimeout(function(){

					selectorText = domPatches[iterator].selector;
					patches = domPatches[iterator].patches;
		
		// Although some selector libraries can find :checked :enabled etc. 
		// we need to find all elements that could have that state because 
		// it can be changed by the user.
					domSelectorText = selectorText.replace(RE_LIBRARY_INCOMPATIBLE_PSEUDOS, EMPTY_STRING);
		
		// If the dom selector equates to an empty string or ends with 
		// whitespace then we need to append a universal selector (*) to it.
		if (domSelectorText == EMPTY_STRING || domSelectorText.charAt(domSelectorText.length - 1) == SPACE_STRING) {
			domSelectorText += "*";
		}
		
		// Ensure we catch errors from the selector library
		try {
			elms = selectorMethod( domSelectorText );
		} catch (ex) {
			// #DEBUG_START
			log( "Selector '" + selectorText + "' threw exception '" + ex + "'" );
			// #DEBUG_END
		}


		if (elms) {
			for (var d = 0, dl = elms.length; d < dl; d++) {	
				var elm = elms[d];
				var cssClasses = elm.className;
				for (var f = 0, fl = patches.length; f < fl; f++) {
					var patch = patches[f];
					if (!hasPatch(elm, patch)) {
						if (patch.applyClass && (patch.applyClass === true || patch.applyClass(elm) === true)) {
							cssClasses = toggleClass(cssClasses, patch.className, true );
						}
					}
				}
				elm.className = cssClasses;
			}
		}
				}, 0);
			})();
		}
	};

	// --[ hasPatch() ]-----------------------------------------------------
	// checks for the exsistence of a patch on an element
	function hasPatch( elm, patch ) {
		return new RegExp("(^|\\s)" + patch.className + "(\\s|$)").test(elm.className);
	};
	
	
	// =========================== Utility =================================
	
	function createClassName( className ) {
		return namespace + "-" + ((ieVersion == 6 && patchIE6MultipleClasses) ?
			ie6PatchID++
		:
			className.replace(RE_PATCH_CLASS_NAME_REPLACE, function(a) { return a.charCodeAt(0) }));
	};

	// --[ log() ]----------------------------------------------------------
	// #DEBUG_START
	function log( message ) {
		if (win.console) {
			win.console.log(message);
		}
	};
	// #DEBUG_END

	// --[ trim() ]---------------------------------------------------------
	// removes leading, trailing whitespace from a string
	function trim( text ) {
		return text.replace(RE_TIDY_TRIM_WHITESPACE, PLACEHOLDER_STRING);
	};

	// --[ normalizeWhitespace() ]------------------------------------------
	// removes leading, trailing and consecutive whitespace from a string
	function normalizeWhitespace( text ) {
		return trim(text).replace(RE_TIDY_CONSECUTIVE_WHITESPACE, SPACE_STRING);
	};

	// --[ normalizeSelectorWhitespace() ]----------------------------------
	// tidies whitespace around selector brackets and combinators
	function normalizeSelectorWhitespace( selectorText ) {
		return normalizeWhitespace(selectorText.
			replace(RE_TIDY_TRAILING_WHITESPACE, PLACEHOLDER_STRING).
			replace(RE_TIDY_LEADING_WHITESPACE, PLACEHOLDER_STRING)
		);
	};

	// --[ toggleElementClass() ]-------------------------------------------
	// toggles a single className on an element
	function toggleElementClass( elm, className, on ) {
		var oldClassName = elm.className;
		var newClassName = toggleClass(oldClassName, className, on);
		if (newClassName != oldClassName) {
			elm.className = newClassName;
			elm.parentNode.className += EMPTY_STRING;
		}
	};

	// --[ toggleClass() ]--------------------------------------------------
	// adds / removes a className from a string of classNames. Used to 
	// manage multiple class changes without forcing a DOM redraw
	function toggleClass( classList, className, on ) {
		var re = RegExp("(^|\\s)" + className + "(\\s|$)");
		var classExists = re.test(classList);
		if (on) {
			return classExists ? classList : classList + SPACE_STRING + className;
		} else {
			return classExists ? trim(classList.replace(re, PLACEHOLDER_STRING)) : classList;
		}
	};
	
	// --[ addEvent() ]-----------------------------------------------------
	function addEvent(elm, eventName, eventHandler) {
		elm.attachEvent("on" + eventName, eventHandler);
	};

	// --[ getXHRObject() ]-------------------------------------------------
	function getXHRObject()
	{
		if (win.XMLHttpRequest) {
			return new XMLHttpRequest;
		}
		try	{ 
			return new ActiveXObject('Microsoft.XMLHTTP');
		} catch(e) { 
			return null;
		}
	};

	// --[ loadStyleSheet() ]-----------------------------------------------
	function loadStyleSheet( url ) {
		xhr.open("GET", url, false);
		xhr.send();
		return (xhr.status==200) ? xhr.responseText : EMPTY_STRING;	
	};
	
	// --[ resolveUrl() ]---------------------------------------------------
	// Converts a URL fragment to a fully qualified URL using the specified
	// context URL. Returns null if same-origin policy is broken
	function resolveUrl( url, contextUrl ) {
	
		function getProtocolAndHost( url ) {
			return url.substring(0, url.indexOf("/", 8));
		};
		
		// absolute path
		if (/^https?:\/\//i.test(url)) {
			return getProtocolAndHost(contextUrl) == getProtocolAndHost(url) ? url : null;
		}
		
		// root-relative path
		if (url.charAt(0)=="/")	{
			return getProtocolAndHost(contextUrl) + url;
		}

		// relative path
		var contextUrlPath = contextUrl.split(/[?#]/)[0]; // ignore query string in the contextUrl	
		if (url.charAt(0) != "?" && contextUrlPath.charAt(contextUrlPath.length - 1) != "/") {
			contextUrlPath = contextUrlPath.substring(0, contextUrlPath.lastIndexOf("/") + 1);
		}
		
		return contextUrlPath + url;
	};
	
	// --[ parseStyleSheet() ]----------------------------------------------
	// Downloads the stylesheet specified by the URL, removes it's comments
	// and recursivly replaces @import rules with their contents, ultimately
	// returning the full cssText.
	function parseStyleSheet( url ) {
		if (url) {
			return loadStyleSheet(url).replace(RE_COMMENT, EMPTY_STRING).
			replace(RE_IMPORT, function( match, quoteChar, importUrl, quoteChar2, importUrl2 ) { 
				return parseStyleSheet(resolveUrl(importUrl || importUrl2, url));
			}).
			replace(RE_ASSET_URL, function( match, quoteChar, assetUrl ) { 
				quoteChar = quoteChar || EMPTY_STRING;
				return " url(" + quoteChar + resolveUrl(assetUrl, url) + quoteChar + ") "; 
			});
		}
		return EMPTY_STRING;
	};
	
	// --[ init() ]---------------------------------------------------------
	function init() {
		// honour the <base> tag
		var url, stylesheet;
		var baseTags = doc.getElementsByTagName("BASE");
		var baseUrl = (baseTags.length > 0) ? baseTags[0].href : doc.location.href;
		
		/* Note: This code prevents IE from freezing / crashing when using 
		@font-face .eot files but it modifies the <head> tag and could
		trigger the IE stylesheet limit. It will also cause FOUC issues.
		If you choose to use it, make sure you comment out the for loop 
		directly below this comment.

		var head = doc.getElementsByTagName("head")[0];
		for (var c=doc.styleSheets.length-1; c>=0; c--) {
			stylesheet = doc.styleSheets[c]
			head.appendChild(doc.createElement("style"))
			var patchedStylesheet = doc.styleSheets[doc.styleSheets.length-1];
			
			if (stylesheet.href != EMPTY_STRING) {
				url = resolveUrl(stylesheet.href, baseUrl)
				if (url) {
					patchedStylesheet.cssText = patchStyleSheet( parseStyleSheet( url ) )
					stylesheet.disabled = true
					setTimeout( function () {
						stylesheet.owningElement.parentNode.removeChild(stylesheet.owningElement)
					})
				}
			}
		}
		*/
		
		for (var c = 0; c < doc.styleSheets.length; c++) {
			stylesheet = doc.styleSheets[c]
			if (stylesheet.href != EMPTY_STRING) {
				url = resolveUrl(stylesheet.href, baseUrl);
				if (url) {
					stylesheet.cssText = patchStyleSheet( parseStyleSheet( url ) );
				}
			}
		}
	};

	// --[ init() ]---------------------------------------------------------
	function init() {
		applyPatches();
		
		// :enabled & :disabled polling script (since we can't hook 
		// onpropertychange event when an element is disabled) 
		if (enabledWatchers.length > 0) {
			setInterval( function() {
				for (var c = 0, cl = enabledWatchers.length; c < cl; c++) {
					var e = enabledWatchers[c];
					if (e.disabled !== e.$disabled) {
						if (e.disabled) {
							e.disabled = false;
							e.$disabled = true;
							e.disabled = true;
						}
						else {
							e.$disabled = e.disabled;
						}
					}
				}
			},250)
		}
	};
	
	// Bind selectivizr to the ContentLoaded event. 
	ContentLoaded(win, function() {
		// Determine the "best fit" selector engine
		for (var engine in selectorEngines) {
			var members, member, context = win;
			if (win[engine]) {
				members = selectorEngines[engine].replace("*", engine).split(".");
				while ((member = members.shift()) && (context = context[member])) {}
				if (typeof context == "function") {
					selectorMethod = context;
					init();
					return;
				}
			}
		}
	});
	
	
	/*!
	 * ContentLoaded.js by Diego Perini, modified for IE<9 only (to save space)
	 *
	 * Author: Diego Perini (diego.perini at gmail.com)
	 * Summary: cross-browser wrapper for DOMContentLoaded
	 * Updated: 20101020
	 * License: MIT
	 * Version: 1.2
	 *
	 * URL:
	 * http://javascript.nwbox.com/ContentLoaded/
	 * http://javascript.nwbox.com/ContentLoaded/MIT-LICENSE
	 *
	 */

	// @w window reference
	// @f function reference
	function ContentLoaded(win, fn) {

		var done = false, top = true,
		init = function(e) {
			if (e.type == "readystatechange" && doc.readyState != "complete") return;
			(e.type == "load" ? win : doc).detachEvent("on" + e.type, init, false);
			if (!done && (done = true)) fn.call(win, e.type || e);
		},
		poll = function() {
			try { root.doScroll("left"); } catch(e) { setTimeout(poll, 50); return; }
			init('poll');
		};

		if (doc.readyState == "complete") fn.call(win, EMPTY_STRING);
		else {
			if (doc.createEventObject && root.doScroll) {
				try { top = !win.frameElement; } catch(e) { }
				if (top) poll();
			}
			addEvent(doc,"readystatechange", init);
			addEvent(win,"load", init);
		}
	};
})(this);
/*! matchMedia() polyfill - Test a CSS media type/query in JS. Authors & copyright (c) 2012: Scott Jehl, Paul Irish, Nicholas Zakas, David Knight. Dual MIT/BSD license */

window.matchMedia || (window.matchMedia = function() {
    "use strict";

    // For browsers that support matchMedium api such as IE 9 and webkit
    var styleMedia = (window.styleMedia || window.media);

    // For those that don't support matchMedium
    if (!styleMedia) {
        var style       = document.createElement('style'),
            script      = document.getElementsByTagName('script')[0],
            info        = null;

        style.type  = 'text/css';
        style.id    = 'matchmediajs-test';

        script.parentNode.insertBefore(style, script);

        // 'style.currentStyle' is used by IE <= 8 and 'window.getComputedStyle' for all other browsers
        info = ('getComputedStyle' in window) && window.getComputedStyle(style, null) || style.currentStyle;

        styleMedia = {
            matchMedium: function(media) {
                var text = '@media ' + media + '{ #matchmediajs-test { width: 1px; } }';

                // 'style.styleSheet' is used by IE <= 8 and 'style.textContent' for all other browsers
                if (style.styleSheet) {
                    style.styleSheet.cssText = text;
                } else {
                    style.textContent = text;
                }

                // Test if media query is true or false
                return info.width === '1px';
            }
        };
    }

    return function(media) {
        return {
            matches: styleMedia.matchMedium(media || 'all'),
            media: media || 'all'
        };
    };
}());
/*! matchMedia() polyfill addListener/removeListener extension. Author & copyright (c) 2012: Scott Jehl. Dual MIT/BSD license */
(function(){
    // Bail out for browsers that have addListener support
    if (window.matchMedia && window.matchMedia('all').addListener) {
        return false;
    }

    var localMatchMedia = window.matchMedia,
        hasMediaQueries = localMatchMedia('only all').matches,
        isListening     = false,
        timeoutID       = 0,    // setTimeout for debouncing 'handleChange'
        queries         = [],   // Contains each 'mql' and associated 'listeners' if 'addListener' is used
        handleChange    = function(evt) {
            // Debounce
            clearTimeout(timeoutID);

            timeoutID = setTimeout(function() {
                for (var i = 0, il = queries.length; i < il; i++) {
                    var mql         = queries[i].mql,
                        listeners   = queries[i].listeners || [],
                        matches     = localMatchMedia(mql.media).matches;

                    // Update mql.matches value and call listeners
                    // Fire listeners only if transitioning to or from matched state
                    if (matches !== mql.matches) {
                        mql.matches = matches;

                        for (var j = 0, jl = listeners.length; j < jl; j++) {
                            listeners[j].call(window, mql);
                        }
                    }
                }
            }, 30);
        };

    window.matchMedia = function(media) {
        var mql         = localMatchMedia(media),
            listeners   = [],
            index       = 0;

        mql.addListener = function(listener) {
            // Changes would not occur to css media type so return now (Affects IE <= 8)
            if (!hasMediaQueries) {
                return;
            }

            // Set up 'resize' listener for browsers that support CSS3 media queries (Not for IE <= 8)
            // There should only ever be 1 resize listener running for performance
            if (!isListening) {
                isListening = true;
                window.addEventListener('resize', handleChange, true);
            }

            // Push object only if it has not been pushed already
            if (index === 0) {
                index = queries.push({
                    mql         : mql,
                    listeners   : listeners
                });
            }

            listeners.push(listener);
        };

        mql.removeListener = function(listener) {
            for (var i = 0, il = listeners.length; i < il; i++){
                if (listeners[i] === listener){
                    listeners.splice(i, 1);
                }
            }
        };

        return mql;
    };
}());
/*!
 * enquire.js v2.1.2 - Awesome Media Queries in JavaScript
 * Copyright (c) 2014 Nick Williams - http://wicky.nillia.ms/enquire.js
 * License: MIT (http://www.opensource.org/licenses/mit-license.php)
 */

;(function (name, context, factory) {
	var matchMedia = window.matchMedia;

	if (typeof module !== 'undefined' && module.exports) {
		module.exports = factory(matchMedia);
	}
	else if (typeof define === 'function' && define.amd) {
		define(function() {
			return (context[name] = factory(matchMedia));
		});
	}
	else {
		context[name] = factory(matchMedia);
	}
}('enquire', this, function (matchMedia) {

	'use strict';

    /*jshint unused:false */
    /**
     * Helper function for iterating over a collection
     *
     * @param collection
     * @param fn
     */
    function each(collection, fn) {
        var i      = 0,
            length = collection.length,
            cont;

        for(i; i < length; i++) {
            cont = fn(collection[i], i);
            if(cont === false) {
                break; //allow early exit
            }
        }
    }

    /**
     * Helper function for determining whether target object is an array
     *
     * @param target the object under test
     * @return {Boolean} true if array, false otherwise
     */
    function isArray(target) {
        return Object.prototype.toString.apply(target) === '[object Array]';
    }

    /**
     * Helper function for determining whether target object is a function
     *
     * @param target the object under test
     * @return {Boolean} true if function, false otherwise
     */
    function isFunction(target) {
        return typeof target === 'function';
    }

    /**
     * Delegate to handle a media query being matched and unmatched.
     *
     * @param {object} options
     * @param {function} options.match callback for when the media query is matched
     * @param {function} [options.unmatch] callback for when the media query is unmatched
     * @param {function} [options.setup] one-time callback triggered the first time a query is matched
     * @param {boolean} [options.deferSetup=false] should the setup callback be run immediately, rather than first time query is matched?
     * @constructor
     */
    function QueryHandler(options) {
        this.options = options;
        !options.deferSetup && this.setup();
    }
    QueryHandler.prototype = {

        /**
         * coordinates setup of the handler
         *
         * @function
         */
        setup : function() {
            if(this.options.setup) {
                this.options.setup();
            }
            this.initialised = true;
        },

        /**
         * coordinates setup and triggering of the handler
         *
         * @function
         */
        on : function() {
            !this.initialised && this.setup();
            this.options.match && this.options.match();
        },

        /**
         * coordinates the unmatch event for the handler
         *
         * @function
         */
        off : function() {
            this.options.unmatch && this.options.unmatch();
        },

        /**
         * called when a handler is to be destroyed.
         * delegates to the destroy or unmatch callbacks, depending on availability.
         *
         * @function
         */
        destroy : function() {
            this.options.destroy ? this.options.destroy() : this.off();
        },

        /**
         * determines equality by reference.
         * if object is supplied compare options, if function, compare match callback
         *
         * @function
         * @param {object || function} [target] the target for comparison
         */
        equals : function(target) {
            return this.options === target || this.options.match === target;
        }

    };
    /**
     * Represents a single media query, manages it's state and registered handlers for this query
     *
     * @constructor
     * @param {string} query the media query string
     * @param {boolean} [isUnconditional=false] whether the media query should run regardless of whether the conditions are met. Primarily for helping older browsers deal with mobile-first design
     */
    function MediaQuery(query, isUnconditional) {
        this.query = query;
        this.isUnconditional = isUnconditional;
        this.handlers = [];
        this.mql = matchMedia(query);

        var self = this;
        this.listener = function(mql) {
            self.mql = mql;
            self.assess();
        };
        this.mql.addListener(this.listener);
    }
    MediaQuery.prototype = {

        /**
         * add a handler for this query, triggering if already active
         *
         * @param {object} handler
         * @param {function} handler.match callback for when query is activated
         * @param {function} [handler.unmatch] callback for when query is deactivated
         * @param {function} [handler.setup] callback for immediate execution when a query handler is registered
         * @param {boolean} [handler.deferSetup=false] should the setup callback be deferred until the first time the handler is matched?
         */
        addHandler : function(handler) {
            var qh = new QueryHandler(handler);
            this.handlers.push(qh);

            this.matches() && qh.on();
        },

        /**
         * removes the given handler from the collection, and calls it's destroy methods
         * 
         * @param {object || function} handler the handler to remove
         */
        removeHandler : function(handler) {
            var handlers = this.handlers;
            each(handlers, function(h, i) {
                if(h.equals(handler)) {
                    h.destroy();
                    return !handlers.splice(i,1); //remove from array and exit each early
                }
            });
        },

        /**
         * Determine whether the media query should be considered a match
         * 
         * @return {Boolean} true if media query can be considered a match, false otherwise
         */
        matches : function() {
            return this.mql.matches || this.isUnconditional;
        },

        /**
         * Clears all handlers and unbinds events
         */
        clear : function() {
            each(this.handlers, function(handler) {
                handler.destroy();
            });
            this.mql.removeListener(this.listener);
            this.handlers.length = 0; //clear array
        },

        /*
         * Assesses the query, turning on all handlers if it matches, turning them off if it doesn't match
         */
        assess : function() {
            var action = this.matches() ? 'on' : 'off';

            each(this.handlers, function(handler) {
                handler[action]();
            });
        }
    };
    /**
     * Allows for registration of query handlers.
     * Manages the query handler's state and is responsible for wiring up browser events
     *
     * @constructor
     */
    function MediaQueryDispatch () {
        if(!matchMedia) {
            throw new Error('matchMedia not present, legacy browsers require a polyfill');
        }

        this.queries = {};
        this.browserIsIncapable = !matchMedia('only all').matches;
    }

    MediaQueryDispatch.prototype = {

        /**
         * Registers a handler for the given media query
         *
         * @param {string} q the media query
         * @param {object || Array || Function} options either a single query handler object, a function, or an array of query handlers
         * @param {function} options.match fired when query matched
         * @param {function} [options.unmatch] fired when a query is no longer matched
         * @param {function} [options.setup] fired when handler first triggered
         * @param {boolean} [options.deferSetup=false] whether setup should be run immediately or deferred until query is first matched
         * @param {boolean} [shouldDegrade=false] whether this particular media query should always run on incapable browsers
         */
        register : function(q, options, shouldDegrade) {
            var queries         = this.queries,
                isUnconditional = shouldDegrade && this.browserIsIncapable;

            if(!queries[q]) {
                queries[q] = new MediaQuery(q, isUnconditional);
            }

            //normalise to object in an array
            if(isFunction(options)) {
                options = { match : options };
            }
            if(!isArray(options)) {
                options = [options];
            }
            each(options, function(handler) {
                if (isFunction(handler)) {
                    handler = { match : handler };
                }
                queries[q].addHandler(handler);
            });

            return this;
        },

        /**
         * unregisters a query and all it's handlers, or a specific handler for a query
         *
         * @param {string} q the media query to target
         * @param {object || function} [handler] specific handler to unregister
         */
        unregister : function(q, handler) {
            var query = this.queries[q];

            if(query) {
                if(handler) {
                    query.removeHandler(handler);
                }
                else {
                    query.clear();
                    delete this.queries[q];
                }
            }

            return this;
        }
    };

	return new MediaQueryDispatch();

}));
/*
     _ _      _       _
 ___| (_) ___| | __  (_)___
/ __| | |/ __| |/ /  | / __|
\__ \ | | (__|   < _ | \__ \
|___/_|_|\___|_|\_(_)/ |___/
                   |__/

 Version: 1.3.15
  Author: Ken Wheeler
 Website: http://kenwheeler.github.io
    Docs: http://kenwheeler.github.io/slick
    Repo: http://github.com/kenwheeler/slick
  Issues: http://github.com/kenwheeler/slick/issues

 */

/* global window, document, define, jQuery, setInterval, clearInterval */

(function(factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else if (typeof exports !== 'undefined') {
        module.exports = factory(require('jquery'));
    } else {
        factory(jQuery);
    }

}(function($) {
    'use strict';
    var Slick = window.Slick || {};

    Slick = (function() {

        var instanceUid = 0;

        function Slick(element, settings) {

            var _ = this,
                responsiveSettings, breakpoint;

            _.defaults = {
                accessibility: true,
                adaptiveHeight: false,
                appendArrows: $(element),
                appendDots: $(element),
                arrows: true,
                asNavFor: null,
                prevArrow: '<button type="button" data-role="none" class="slick-prev">Previous</button>',
                nextArrow: '<button type="button" data-role="none" class="slick-next">Next</button>',
                autoplay: false,
                autoplaySpeed: 3000,
                centerMode: false,
                centerPadding: '50px',
                cssEase: 'ease',
                customPaging: function(slider, i) {
                    return '<button type="button" data-role="none">' + (i + 1) + '</button>';
                },
                dots: false,
                dotsClass: 'slick-dots',
                draggable: true,
                easing: 'linear',
                fade: false,
                focusOnSelect: false,
                infinite: true,
                initialSlide: 0,
                lazyLoad: 'ondemand',
                onBeforeChange: null,
                onAfterChange: null,
                onInit: null,
                onReInit: null,
                onSetPosition: null,
                pauseOnHover: true,
                pauseOnDotsHover: false,
                respondTo: 'window',
                responsive: null,
                rtl: false,
                slide: 'div',
                slidesToShow: 1,
                slidesToScroll: 1,
                speed: 500,
                swipe: true,
                swipeToSlide: false,
                touchMove: true,
                touchThreshold: 5,
                useCSS: true,
                variableWidth: false,
                vertical: false,
                waitForAnimate: true
            };

            _.initials = {
                animating: false,
                dragging: false,
                autoPlayTimer: null,
                currentDirection: 0,
                currentLeft: null,
                currentSlide: 0,
                direction: 1,
                $dots: null,
                listWidth: null,
                listHeight: null,
                loadIndex: 0,
                $nextArrow: null,
                $prevArrow: null,
                slideCount: null,
                slideWidth: null,
                $slideTrack: null,
                $slides: null,
                sliding: false,
                slideOffset: 0,
                swipeLeft: null,
                $list: null,
                touchObject: {},
                transformsEnabled: false
            };

            $.extend(_, _.initials);

            _.activeBreakpoint = null;
            _.animType = null;
            _.animProp = null;
            _.breakpoints = [];
            _.breakpointSettings = [];
            _.cssTransitions = false;
            _.paused = false;
            _.positionProp = null;
            _.respondTo = null;
            _.shouldClick = true;
            _.$slider = $(element);
            _.$slidesCache = null;
            _.transformType = null;
            _.transitionType = null;
            _.windowWidth = 0;
            _.windowTimer = null;

            _.options = $.extend({}, _.defaults, settings);

            _.currentSlide = _.options.initialSlide;

            _.originalSettings = _.options;
            responsiveSettings = _.options.responsive || null;

            if (responsiveSettings && responsiveSettings.length > -1) {
                _.respondTo = _.options.respondTo || "window";
                for (breakpoint in responsiveSettings) {
                    if (responsiveSettings.hasOwnProperty(breakpoint)) {
                        _.breakpoints.push(responsiveSettings[
                            breakpoint].breakpoint);
                        _.breakpointSettings[responsiveSettings[
                            breakpoint].breakpoint] =
                            responsiveSettings[breakpoint].settings;
                    }
                }
                _.breakpoints.sort(function(a, b) {
                    return b - a;
                });
            }

            _.autoPlay = $.proxy(_.autoPlay, _);
            _.autoPlayClear = $.proxy(_.autoPlayClear, _);
            _.changeSlide = $.proxy(_.changeSlide, _);
            _.clickHandler = $.proxy(_.clickHandler, _);
            _.selectHandler = $.proxy(_.selectHandler, _);
            _.setPosition = $.proxy(_.setPosition, _);
            _.swipeHandler = $.proxy(_.swipeHandler, _);
            _.dragHandler = $.proxy(_.dragHandler, _);
            _.keyHandler = $.proxy(_.keyHandler, _);
            _.autoPlayIterator = $.proxy(_.autoPlayIterator, _);

            _.instanceUid = instanceUid++;

            // A simple way to check for HTML strings
            // Strict HTML recognition (must start with <)
            // Extracted from jQuery v1.11 source
            _.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/;

            _.init();

            _.checkResponsive();

        }

        return Slick;

    }());

    Slick.prototype.addSlide = function(markup, index, addBefore) {

        var _ = this;

        if (typeof(index) === 'boolean') {
            addBefore = index;
            index = null;
        } else if (index < 0 || (index >= _.slideCount)) {
            return false;
        }

        _.unload();

        if (typeof(index) === 'number') {
            if (index === 0 && _.$slides.length === 0) {
                $(markup).appendTo(_.$slideTrack);
            } else if (addBefore) {
                $(markup).insertBefore(_.$slides.eq(index));
            } else {
                $(markup).insertAfter(_.$slides.eq(index));
            }
        } else {
            if (addBefore === true) {
                $(markup).prependTo(_.$slideTrack);
            } else {
                $(markup).appendTo(_.$slideTrack);
            }
        }

        _.$slides = _.$slideTrack.children(this.options.slide);

        _.$slideTrack.children(this.options.slide).detach();

        _.$slideTrack.append(_.$slides);

        _.$slides.each(function(index, element) {
            $(element).attr("index",index);
        });

        _.$slidesCache = _.$slides;

        _.reinit();

    };

    Slick.prototype.animateSlide = function(targetLeft, callback) {

        var animProps = {}, _ = this;

        if(_.options.slidesToShow === 1 && _.options.adaptiveHeight === true && _.options.vertical === false) {
            var targetHeight = _.$slides.eq(_.currentSlide).outerHeight(true);
            _.$list.animate({height: targetHeight},_.options.speed);
        }

        if (_.options.rtl === true && _.options.vertical === false) {
            targetLeft = -targetLeft;
        }
        if (_.transformsEnabled === false) {
            if (_.options.vertical === false) {
                _.$slideTrack.animate({
                    left: targetLeft
                }, _.options.speed, _.options.easing, callback);
            } else {
                _.$slideTrack.animate({
                    top: targetLeft
                }, _.options.speed, _.options.easing, callback);
            }

        } else {

            if (_.cssTransitions === false) {

                $({
                    animStart: _.currentLeft
                }).animate({
                    animStart: targetLeft
                }, {
                    duration: _.options.speed,
                    easing: _.options.easing,
                    step: function(now) {
                        if (_.options.vertical === false) {
                            animProps[_.animType] = 'translate(' +
                                now + 'px, 0px)';
                            _.$slideTrack.css(animProps);
                        } else {
                            animProps[_.animType] = 'translate(0px,' +
                                now + 'px)';
                            _.$slideTrack.css(animProps);
                        }
                    },
                    complete: function() {
                        if (callback) {
                            callback.call();
                        }
                    }
                });

            } else {

                _.applyTransition();

                if (_.options.vertical === false) {
                    animProps[_.animType] = 'translate3d(' + targetLeft + 'px, 0px, 0px)';
                } else {
                    animProps[_.animType] = 'translate3d(0px,' + targetLeft + 'px, 0px)';
                }
                _.$slideTrack.css(animProps);

                if (callback) {
                    setTimeout(function() {

                        _.disableTransition();

                        callback.call();
                    }, _.options.speed);
                }

            }

        }

    };

    Slick.prototype.asNavFor = function(index) {
        var _ = this, asNavFor = _.options.asNavFor != null ? $(_.options.asNavFor).getSlick() : null;
        if(asNavFor != null) asNavFor.slideHandler(index, true);
    };

    Slick.prototype.applyTransition = function(slide) {

        var _ = this,
            transition = {};

        if (_.options.fade === false) {
            transition[_.transitionType] = _.transformType + ' ' + _.options.speed + 'ms ' + _.options.cssEase;
        } else {
            transition[_.transitionType] = 'opacity ' + _.options.speed + 'ms ' + _.options.cssEase;
        }

        if (_.options.fade === false) {
            _.$slideTrack.css(transition);
        } else {
            _.$slides.eq(slide).css(transition);
        }

    };

    Slick.prototype.autoPlay = function() {

        var _ = this;

        if (_.autoPlayTimer) {
            clearInterval(_.autoPlayTimer);
        }

        if (_.slideCount > _.options.slidesToShow && _.paused !== true) {
            _.autoPlayTimer = setInterval(_.autoPlayIterator,
                _.options.autoplaySpeed);
        }

    };

    Slick.prototype.autoPlayClear = function() {

        var _ = this;
        if (_.autoPlayTimer) {
            clearInterval(_.autoPlayTimer);
        }

    };

    Slick.prototype.autoPlayIterator = function() {

        var _ = this;

        if (_.options.infinite === false) {

            if (_.direction === 1) {

                if ((_.currentSlide + 1) === _.slideCount -
                    1) {
                    _.direction = 0;
                }

                _.slideHandler(_.currentSlide + _.options.slidesToScroll);

            } else {

                if ((_.currentSlide - 1 === 0)) {

                    _.direction = 1;

                }

                _.slideHandler(_.currentSlide - _.options.slidesToScroll);

            }

        } else {

            _.slideHandler(_.currentSlide + _.options.slidesToScroll);

        }

    };

    Slick.prototype.buildArrows = function() {

        var _ = this;

        if (_.options.arrows === true && _.slideCount > _.options.slidesToShow) {

            _.$prevArrow = $(_.options.prevArrow);
            _.$nextArrow = $(_.options.nextArrow);

            if (_.htmlExpr.test(_.options.prevArrow)) {
                _.$prevArrow.appendTo(_.options.appendArrows);
            }

            if (_.htmlExpr.test(_.options.nextArrow)) {
                _.$nextArrow.appendTo(_.options.appendArrows);
            }

            if (_.options.infinite !== true) {
                _.$prevArrow.addClass('slick-disabled');
            }

        }

    };

    Slick.prototype.buildDots = function() {

        var _ = this,
            i, dotString;

        if (_.options.dots === true && _.slideCount > _.options.slidesToShow) {

            dotString = '<ul class="' + _.options.dotsClass + '">';

            for (i = 0; i <= _.getDotCount(); i += 1) {
                dotString += '<li>' + _.options.customPaging.call(this, _, i) + '</li>';
            }

            dotString += '</ul>';

            _.$dots = $(dotString).appendTo(
                _.options.appendDots);

            _.$dots.find('li').first().addClass(
                'slick-active');

        }

    };

    Slick.prototype.buildOut = function() {

        var _ = this;

        _.$slides = _.$slider.children(_.options.slide +
            ':not(.slick-cloned)').addClass(
            'slick-slide');
        _.slideCount = _.$slides.length;

        _.$slides.each(function(index, element) {
            $(element).attr("index",index);
        });

        _.$slidesCache = _.$slides;

        _.$slider.addClass('slick-slider');

        _.$slideTrack = (_.slideCount === 0) ?
            $('<div class="slick-track"/>').appendTo(_.$slider) :
            _.$slides.wrapAll('<div class="slick-track"/>').parent();

        _.$list = _.$slideTrack.wrap(
            '<div class="slick-list"/>').parent();
        _.$slideTrack.css('opacity', 0);

        if (_.options.centerMode === true) {
            _.options.slidesToScroll = 1;
        }

        $('img[data-lazy]', _.$slider).not('[src]').addClass('slick-loading');

        _.setupInfinite();

        _.buildArrows();

        _.buildDots();

        _.updateDots();

        if (_.options.accessibility === true) {
            _.$list.prop('tabIndex', 0);
        }

        _.setSlideClasses(typeof this.currentSlide === 'number' ? this.currentSlide : 0);

        if (_.options.draggable === true) {
            _.$list.addClass('draggable');
        }

    };

    Slick.prototype.checkResponsive = function() {

        var _ = this,
            breakpoint, targetBreakpoint, respondToWidth;
        var sliderWidth = _.$slider.width();
        var windowWidth = window.innerWidth || $(window).width();
        if (_.respondTo === "window") {
          respondToWidth = windowWidth;
        } else if (_.respondTo === "slider") {
          respondToWidth = sliderWidth;
        } else if (_.respondTo === "min") {
          respondToWidth = Math.min(windowWidth, sliderWidth);
        }

        if (_.originalSettings.responsive && _.originalSettings
            .responsive.length > -1 && _.originalSettings.responsive !== null) {

            targetBreakpoint = null;

            for (breakpoint in _.breakpoints) {
                if (_.breakpoints.hasOwnProperty(breakpoint)) {
                    if (respondToWidth < _.breakpoints[breakpoint]) {
                        targetBreakpoint = _.breakpoints[breakpoint];
                    }
                }
            }

            if (targetBreakpoint !== null) {
                if (_.activeBreakpoint !== null) {
                    if (targetBreakpoint !== _.activeBreakpoint) {
                        _.activeBreakpoint =
                            targetBreakpoint;
                        _.options = $.extend({}, _.originalSettings,
                            _.breakpointSettings[
                                targetBreakpoint]);
                        _.refresh();
                    }
                } else {
                    _.activeBreakpoint = targetBreakpoint;
                    _.options = $.extend({}, _.originalSettings,
                        _.breakpointSettings[
                            targetBreakpoint]);
                    _.refresh();
                }
            } else {
                if (_.activeBreakpoint !== null) {
                    _.activeBreakpoint = null;
                    _.options = _.originalSettings;
                    _.refresh();
                }
            }

        }

    };

    Slick.prototype.changeSlide = function(event, dontAnimate) {

        var _ = this,
            $target = $(event.target),
            indexOffset, slideOffset, unevenOffset,navigables, prevNavigable;

        // If target is a link, prevent default action.
        $target.is('a') && event.preventDefault();

        unevenOffset = (_.slideCount % _.options.slidesToScroll !== 0);
        indexOffset = unevenOffset ? 0 : (_.slideCount - _.currentSlide) % _.options.slidesToScroll;

        switch (event.data.message) {

            case 'previous':
                slideOffset = indexOffset === 0 ? _.options.slidesToScroll : _.options.slidesToShow - indexOffset;
                if (_.slideCount > _.options.slidesToShow) {
                    _.slideHandler(_.currentSlide  - slideOffset, false, dontAnimate);
                }
                break;

            case 'next':
                slideOffset = indexOffset === 0 ? _.options.slidesToScroll : indexOffset;
                if (_.slideCount > _.options.slidesToShow) {
                    _.slideHandler(_.currentSlide + slideOffset, false, dontAnimate);
                }
                break;

            case 'index':
                var index = event.data.index === 0 ? 0 :
                    event.data.index || $(event.target).parent().index() * _.options.slidesToScroll;

                navigables = _.getNavigableIndexes();
                prevNavigable = 0;
                if(navigables[index] && navigables[index] === index) {
                    if(index > navigables[navigables.length -1]){
                        index = navigables[navigables.length -1];
                    } else {
                        for(var n in navigables) {
                            if(index < navigables[n]) {
                                index = prevNavigable;
                                break;
                            }
                            prevNavigable = navigables[n];
                        }
                    }
                }
                _.slideHandler(index, false, dontAnimate);

            default:
                return;
        }

    };

    Slick.prototype.clickHandler = function(event) {

        var _ = this;

        if(_.shouldClick === false) {
            event.stopImmediatePropagation();
            event.stopPropagation();
            event.preventDefault();
        }

    }

    Slick.prototype.destroy = function() {

        var _ = this;

        _.autoPlayClear();

        _.touchObject = {};

        $('.slick-cloned', _.$slider).remove();
        if (_.$dots) {
            _.$dots.remove();
        }
        if (_.$prevArrow && (typeof _.options.prevArrow !== 'object')) {
            _.$prevArrow.remove();
        }
        if (_.$nextArrow && (typeof _.options.nextArrow !== 'object')) {
            _.$nextArrow.remove();
        }
        if (_.$slides.parent().hasClass('slick-track')) {
            _.$slides.unwrap().unwrap();
        }

        _.$slides.removeClass(
            'slick-slide slick-active slick-center slick-visible')
            .removeAttr('index')
            .css({
                position: '',
                left: '',
                top: '',
                zIndex: '',
                opacity: '',
                width: ''
            });

        _.$slider.removeClass('slick-slider');
        _.$slider.removeClass('slick-initialized');

        _.$list.off('.slick');
        $(window).off('.slick-' + _.instanceUid);
        $(document).off('.slick-' + _.instanceUid);

    };

    Slick.prototype.disableTransition = function(slide) {

        var _ = this,
            transition = {};

        transition[_.transitionType] = "";

        if (_.options.fade === false) {
            _.$slideTrack.css(transition);
        } else {
            _.$slides.eq(slide).css(transition);
        }

    };

    Slick.prototype.fadeSlide = function(oldSlide, slideIndex, callback) {

        var _ = this;

        if (_.cssTransitions === false) {

            _.$slides.eq(slideIndex).css({
                zIndex: 1000
            });

            _.$slides.eq(slideIndex).animate({
                opacity: 1
            }, _.options.speed, _.options.easing, callback);

            _.$slides.eq(oldSlide).animate({
                opacity: 0
            }, _.options.speed, _.options.easing);

        } else {

            _.applyTransition(slideIndex);
            _.applyTransition(oldSlide);

            _.$slides.eq(slideIndex).css({
                opacity: 1,
                zIndex: 1000
            });

            _.$slides.eq(oldSlide).css({
                opacity: 0
            });

            if (callback) {
                setTimeout(function() {

                    _.disableTransition(slideIndex);
                    _.disableTransition(oldSlide);

                    callback.call();
                }, _.options.speed);
            }

        }

    };

    Slick.prototype.filterSlides = function(filter) {

        var _ = this;

        if (filter !== null) {

            _.unload();

            _.$slideTrack.children(this.options.slide).detach();

            _.$slidesCache.filter(filter).appendTo(_.$slideTrack);

            _.reinit();

        }

    };

    Slick.prototype.getCurrent = function() {

        var _ = this;

        return _.currentSlide;

    };

    Slick.prototype.getDotCount = function() {

        var _ = this;

        var breakPoint = 0;
        var counter = 0;
        var pagerQty = 0;

        if(_.options.infinite === true) {
            pagerQty = Math.ceil(_.slideCount / _.options.slidesToScroll);
        } else {
            while (breakPoint < _.slideCount){
                ++pagerQty;
                breakPoint = counter + _.options.slidesToShow;
                counter += _.options.slidesToScroll <= _.options.slidesToShow ? _.options.slidesToScroll  : _.options.slidesToShow;
            }
        }

        return pagerQty - 1;

    };

    Slick.prototype.getLeft = function(slideIndex) {

        var _ = this,
            targetLeft,
            verticalHeight,
            verticalOffset = 0,
            slideWidth,
            targetSlide;

        _.slideOffset = 0;
        verticalHeight = _.$slides.first().outerHeight();

        if (_.options.infinite === true) {
            if (_.slideCount > _.options.slidesToShow) {
                _.slideOffset = (_.slideWidth * _.options.slidesToShow) * -1;
                verticalOffset = (verticalHeight * _.options.slidesToShow) * -1;
            }
            if (_.slideCount % _.options.slidesToScroll !== 0) {
                if (slideIndex + _.options.slidesToScroll > _.slideCount && _.slideCount > _.options.slidesToShow) {
                    if(slideIndex > _.slideCount) {
                        _.slideOffset = ((_.options.slidesToShow - (slideIndex - _.slideCount)) * _.slideWidth) * -1;
                        verticalOffset = ((_.options.slidesToShow - (slideIndex - _.slideCount)) * verticalHeight) * -1;
                    } else {
                        _.slideOffset = ((_.slideCount % _.options.slidesToScroll) * _.slideWidth) * -1;
                        verticalOffset = ((_.slideCount % _.options.slidesToScroll) * verticalHeight) * -1;
                    }
                }
            }
        } else {
            if(slideIndex + _.options.slidesToShow > _.slideCount) {
                _.slideOffset = ((slideIndex + _.options.slidesToShow) - _.slideCount) * _.slideWidth;
                verticalOffset = ((slideIndex + _.options.slidesToShow) - _.slideCount) * verticalHeight;
            }
        }

        if (_.slideCount <= _.options.slidesToShow){
            _.slideOffset = 0;
            verticalOffset = 0;
        }

        if (_.options.centerMode === true && _.options.infinite === true) {
            _.slideOffset += _.slideWidth * Math.floor(_.options.slidesToShow / 2) - _.slideWidth;
        } else if (_.options.centerMode === true) {
            _.slideOffset = 0;
            _.slideOffset += _.slideWidth * Math.floor(_.options.slidesToShow / 2);
        }

        if (_.options.vertical === false) {
            targetLeft = ((slideIndex * _.slideWidth) * -1) + _.slideOffset;
        } else {
            targetLeft = ((slideIndex * verticalHeight) * -1) + verticalOffset;
        }

        if (_.options.variableWidth === true) {

            if(_.slideCount <= _.options.slidesToShow || _.options.infinite === false) {
                targetSlide = _.$slideTrack.children('.slick-slide').eq(slideIndex);
            } else {
                targetSlide = _.$slideTrack.children('.slick-slide').eq(slideIndex + _.options.slidesToShow);
            }
            targetLeft = targetSlide[0] ? targetSlide[0].offsetLeft * -1 : 0;
            if (_.options.centerMode === true) {
                if(_.options.infinite === false) {
                    targetSlide = _.$slideTrack.children('.slick-slide').eq(slideIndex);
                } else {
                    targetSlide = _.$slideTrack.children('.slick-slide').eq(slideIndex + _.options.slidesToShow + 1);
                }
                targetLeft = targetSlide[0] ? targetSlide[0].offsetLeft * -1 : 0;
                targetLeft += (_.$list.width() - targetSlide.outerWidth()) / 2;
            }
        }

         // 1680

        return targetLeft;

    };

    Slick.prototype.getNavigableIndexes = function() {

        var _ = this;

        var breakPoint = 0;
        var counter = 0;
        var indexes = [];

        while (breakPoint < _.slideCount){
            indexes.push(breakPoint);
            breakPoint = counter + _.options.slidesToScroll;
            counter += _.options.slidesToScroll <= _.options.slidesToShow ? _.options.slidesToScroll  : _.options.slidesToShow;
        }

        return indexes;

    };

    Slick.prototype.getSlideCount = function() {

        var _ = this, slidesTraversed;

        if(_.options.swipeToSlide === true) {
            var swipedSlide = null;
            _.$slideTrack.find('.slick-slide').each(function(index, slide){
                if (slide.offsetLeft + ($(slide).outerWidth() / 2) > (_.swipeLeft * -1)) {
                    swipedSlide = slide;
                    return false;
                }
            });
            slidesTraversed = Math.abs($(swipedSlide).attr('index') - _.currentSlide);
            return slidesTraversed;
        } else {
            return _.options.slidesToScroll;
        }

    };

    Slick.prototype.init = function() {

        var _ = this;

        if (!$(_.$slider).hasClass('slick-initialized')) {

            $(_.$slider).addClass('slick-initialized');
            _.buildOut();
            _.setProps();
            _.startLoad();
            _.loadSlider();
            _.initializeEvents();
            _.updateArrows();
            _.updateDots();
        }

        if (_.options.onInit !== null) {
            _.options.onInit.call(this, _);
        }

    };

    Slick.prototype.initArrowEvents = function() {

        var _ = this;

        if (_.options.arrows === true && _.slideCount > _.options.slidesToShow) {
            _.$prevArrow.on('click.slick', {
                message: 'previous'
            }, _.changeSlide);
            _.$nextArrow.on('click.slick', {
                message: 'next'
            }, _.changeSlide);
        }

    };

    Slick.prototype.initDotEvents = function() {

        var _ = this;

        if (_.options.dots === true && _.slideCount > _.options.slidesToShow) {
            $('li', _.$dots).on('click.slick', {
                message: 'index'
            }, _.changeSlide);
        }

        if (_.options.dots === true && _.options.pauseOnDotsHover === true && _.options.autoplay === true) {
            $('li', _.$dots)
                .on('mouseenter.slick', function(){
                    _.paused = true;
                    _.autoPlayClear();
                })
                .on('mouseleave.slick', function(){
                    _.paused = false;
                    _.autoPlay();
                });
        }

    };

    Slick.prototype.initializeEvents = function() {

        var _ = this;

        _.initArrowEvents();

        _.initDotEvents();

        _.$list.on('touchstart.slick mousedown.slick', {
            action: 'start'
        }, _.swipeHandler);
        _.$list.on('touchmove.slick mousemove.slick', {
            action: 'move'
        }, _.swipeHandler);
        _.$list.on('touchend.slick mouseup.slick', {
            action: 'end'
        }, _.swipeHandler);
        _.$list.on('touchcancel.slick mouseleave.slick', {
            action: 'end'
        }, _.swipeHandler);

        _.$list.on('click.slick', _.clickHandler);

        if (_.options.pauseOnHover === true && _.options.autoplay === true) {
            _.$list.on('mouseenter.slick', function(){
                _.paused = true;
                _.autoPlayClear();
            });
            _.$list.on('mouseleave.slick', function(){
                _.paused = false;
                _.autoPlay();
            });
        }

        if(_.options.accessibility === true) {
            _.$list.on('keydown.slick', _.keyHandler);
        }

        if(_.options.focusOnSelect === true) {
            $(_.options.slide, _.$slideTrack).on('click.slick', _.selectHandler);
        }

        $(window).on('orientationchange.slick.slick-' + _.instanceUid, function() {
            _.checkResponsive();
            _.setPosition();
        });

        $(window).on('resize.slick.slick-' + _.instanceUid, function() {
            if ($(window).width() !== _.windowWidth) {
                clearTimeout(_.windowDelay);
                _.windowDelay = window.setTimeout(function() {
                    _.windowWidth = $(window).width();
                    _.checkResponsive();
                    _.setPosition();
                }, 50);
            }
        });

        $('*[draggable!=true]', _.$slideTrack).on('dragstart', function(e){ e.preventDefault(); })

        $(window).on('load.slick.slick-' + _.instanceUid, _.setPosition);
        $(document).on('ready.slick.slick-' + _.instanceUid, _.setPosition);

    };

    Slick.prototype.initUI = function() {

        var _ = this;

        if (_.options.arrows === true && _.slideCount > _.options.slidesToShow) {

            _.$prevArrow.show();
            _.$nextArrow.show();

        }

        if (_.options.dots === true && _.slideCount > _.options.slidesToShow) {

            _.$dots.show();

        }

        if (_.options.autoplay === true) {

            _.autoPlay();

        }

    };

    Slick.prototype.keyHandler = function(event) {

        var _ = this;

        if (event.keyCode === 37 && _.options.accessibility === true) {
            _.changeSlide({
                data: {
                    message: 'previous'
                }
            });
        } else if (event.keyCode === 39 && _.options.accessibility === true) {
            _.changeSlide({
                data: {
                    message: 'next'
                }
            });
        }

    };

    Slick.prototype.lazyLoad = function() {

        var _ = this,
            loadRange, cloneRange, rangeStart, rangeEnd;

        function loadImages(imagesScope) {
            $('img[data-lazy]', imagesScope).each(function() {
                var image = $(this),
                    imageSource = $(this).attr('data-lazy');

                image
                  .load(function() { image.animate({ opacity: 1 }, 200); })
                  .css({ opacity: 0 })
                  .attr('src', imageSource)
                  .removeAttr('data-lazy')
                  .removeClass('slick-loading');
            });
        }

        if (_.options.centerMode === true) {
            if (_.options.infinite === true) {
                rangeStart = _.currentSlide + (_.options.slidesToShow/2 + 1);
                rangeEnd = rangeStart + _.options.slidesToShow + 2;
            } else {
                rangeStart = Math.max(0, _.currentSlide - (_.options.slidesToShow/2 + 1));
                rangeEnd = 2 + (_.options.slidesToShow/2 + 1) + _.currentSlide;
            }
        } else {
            rangeStart = _.options.infinite ? _.options.slidesToShow + _.currentSlide : _.currentSlide;
            rangeEnd = rangeStart + _.options.slidesToShow;
            if (_.options.fade === true ) {
                if(rangeStart > 0) rangeStart--;
                if(rangeEnd <= _.slideCount) rangeEnd++;
            }
        }

        loadRange = _.$slider.find('.slick-slide').slice(rangeStart, rangeEnd);
        loadImages(loadRange);

          if (_.slideCount <= _.options.slidesToShow){
              cloneRange = _.$slider.find('.slick-slide')
              loadImages(cloneRange)
          }else
        if (_.currentSlide >= _.slideCount - _.options.slidesToShow) {
            cloneRange = _.$slider.find('.slick-cloned').slice(0, _.options.slidesToShow);
            loadImages(cloneRange)
        } else if (_.currentSlide === 0) {
            cloneRange = _.$slider.find('.slick-cloned').slice(_.options.slidesToShow * -1);
            loadImages(cloneRange);
        }

    };

    Slick.prototype.loadSlider = function() {

        var _ = this;

        _.setPosition();

        _.$slideTrack.css({
            opacity: 1
        });

        _.$slider.removeClass('slick-loading');

        _.initUI();

        if (_.options.lazyLoad === 'progressive') {
            _.progressiveLazyLoad();
        }

    };

    Slick.prototype.postSlide = function(index) {

        var _ = this;

        if (_.options.onAfterChange !== null) {
            _.options.onAfterChange.call(this, _, index);
        }

        _.animating = false;

        _.setPosition();

        _.swipeLeft = null;

        if (_.options.autoplay === true && _.paused === false) {
            _.autoPlay();
        }

    };

    Slick.prototype.progressiveLazyLoad = function() {

        var _ = this,
            imgCount, targetImage;

        imgCount = $('img[data-lazy]', _.$slider).length;

        if (imgCount > 0) {
            targetImage = $('img[data-lazy]', _.$slider).first();
            targetImage.attr('src', targetImage.attr('data-lazy')).removeClass('slick-loading').load(function() {
                targetImage.removeAttr('data-lazy');
                _.progressiveLazyLoad();
            })
         .error(function () {
          targetImage.removeAttr('data-lazy');
          _.progressiveLazyLoad();
         });
        }

    };

    Slick.prototype.refresh = function() {

        var _ = this,
            currentSlide = _.currentSlide;

        _.destroy();

        $.extend(_, _.initials);

        _.init();

        _.changeSlide({
            data: {
                message: 'index',
                index: currentSlide,
            }
        }, true);

    };

    Slick.prototype.reinit = function() {

        var _ = this;

        _.$slides = _.$slideTrack.children(_.options.slide).addClass(
            'slick-slide');

        _.slideCount = _.$slides.length;

        if (_.currentSlide >= _.slideCount && _.currentSlide !== 0) {
            _.currentSlide = _.currentSlide - _.options.slidesToScroll;
        }

        if (_.slideCount <= _.options.slidesToShow) {
            _.currentSlide = 0;
        }

        _.setProps();

        _.setupInfinite();

        _.buildArrows();

        _.updateArrows();

        _.initArrowEvents();

        _.buildDots();

        _.updateDots();

        _.initDotEvents();

        if(_.options.focusOnSelect === true) {
            $(_.options.slide, _.$slideTrack).on('click.slick', _.selectHandler);
        }

        _.setSlideClasses(0);

        _.setPosition();

        if (_.options.onReInit !== null) {
            _.options.onReInit.call(this, _);
        }

    };

    Slick.prototype.removeSlide = function(index, removeBefore, removeAll) {

        var _ = this;

        if (typeof(index) === 'boolean') {
            removeBefore = index;
            index = removeBefore === true ? 0 : _.slideCount - 1;
        } else {
            index = removeBefore === true ? --index : index;
        }

        if (_.slideCount < 1 || index < 0 || index > _.slideCount - 1) {
            return false;
        }

        _.unload();

        if(removeAll === true) {
            _.$slideTrack.children().remove();
        } else {
            _.$slideTrack.children(this.options.slide).eq(index).remove();
        }

        _.$slides = _.$slideTrack.children(this.options.slide);

        _.$slideTrack.children(this.options.slide).detach();

        _.$slideTrack.append(_.$slides);

        _.$slidesCache = _.$slides;

        _.reinit();

    };

    Slick.prototype.setCSS = function(position) {

        var _ = this,
            positionProps = {}, x, y;

        if (_.options.rtl === true) {
            position = -position;
        }
        x = _.positionProp == 'left' ? position + 'px' : '0px';
        y = _.positionProp == 'top' ? position + 'px' : '0px';

        positionProps[_.positionProp] = position;

        if (_.transformsEnabled === false) {
            _.$slideTrack.css(positionProps);
        } else {
            positionProps = {};
            if (_.cssTransitions === false) {
                positionProps[_.animType] = 'translate(' + x + ', ' + y + ')';
                _.$slideTrack.css(positionProps);
            } else {
                positionProps[_.animType] = 'translate3d(' + x + ', ' + y + ', 0px)';
                _.$slideTrack.css(positionProps);
            }
        }

    };

    Slick.prototype.setDimensions = function() {

        var _ = this;

        if (_.options.vertical === false) {
            if (_.options.centerMode === true) {
                _.$list.css({
                    padding: ('0px ' + _.options.centerPadding)
                });
            }
        } else {
            _.$list.height(_.$slides.first().outerHeight(true) * _.options.slidesToShow);
            if (_.options.centerMode === true) {
                _.$list.css({
                    padding: (_.options.centerPadding + ' 0px')
                });
            }
        }

        _.listWidth = _.$list.width();
        _.listHeight = _.$list.height();


        if(_.options.vertical === false && _.options.variableWidth === false) {
            _.slideWidth = Math.ceil(_.listWidth / _.options.slidesToShow);
            _.$slideTrack.width(Math.ceil((_.slideWidth * _.$slideTrack.children('.slick-slide').length)));

        } else if (_.options.variableWidth === true) {
            var trackWidth = 0;
            _.slideWidth = Math.ceil(_.listWidth / _.options.slidesToShow);
            _.$slideTrack.children('.slick-slide').each(function(){
                trackWidth += Math.ceil($(this).outerWidth(true));
            });
            _.$slideTrack.width(Math.ceil(trackWidth) + 1);
        } else {
            _.slideWidth = Math.ceil(_.listWidth);
            _.$slideTrack.height(Math.ceil((_.$slides.first().outerHeight(true) * _.$slideTrack.children('.slick-slide').length)));
        }

        var offset = _.$slides.first().outerWidth(true) - _.$slides.first().width();
        if (_.options.variableWidth === false) _.$slideTrack.children('.slick-slide').width(_.slideWidth - offset);

    };

    Slick.prototype.setFade = function() {

        var _ = this,
            targetLeft;

        _.$slides.each(function(index, element) {
            targetLeft = (_.slideWidth * index) * -1;
            if (_.options.rtl === true) {
                $(element).css({
                    position: 'relative',
                    right: targetLeft,
                    top: 0,
                    zIndex: 800,
                    opacity: 0
                });
            } else {
                $(element).css({
                    position: 'relative',
                    left: targetLeft,
                    top: 0,
                    zIndex: 800,
                    opacity: 0
                });
            }
        });

        _.$slides.eq(_.currentSlide).css({
            zIndex: 900,
            opacity: 1
        });

    };

    Slick.prototype.setHeight = function() {

        var _ = this;

        if(_.options.slidesToShow === 1 && _.options.adaptiveHeight === true && _.options.vertical === false) {
            var targetHeight = _.$slides.eq(_.currentSlide).outerHeight(true);
            _.$list.css('height', targetHeight);
        }

    };

    Slick.prototype.setPosition = function() {

        var _ = this;

        _.setDimensions();

        _.setHeight();

        if (_.options.fade === false) {
            _.setCSS(_.getLeft(_.currentSlide));
        } else {
            _.setFade();
        }

        if (_.options.onSetPosition !== null) {
            _.options.onSetPosition.call(this, _);
        }

    };

    Slick.prototype.setProps = function() {

        var _ = this,
            bodyStyle = document.body.style;

        _.positionProp = _.options.vertical === true ? 'top' : 'left';

        if (_.positionProp === 'top') {
            _.$slider.addClass('slick-vertical');
        } else {
            _.$slider.removeClass('slick-vertical');
        }

        if (bodyStyle.WebkitTransition !== undefined ||
            bodyStyle.MozTransition !== undefined ||
            bodyStyle.msTransition !== undefined) {
            if(_.options.useCSS === true) {
                _.cssTransitions = true;
            }
        }

        if (bodyStyle.OTransform !== undefined) {
            _.animType = 'OTransform';
            _.transformType = "-o-transform";
            _.transitionType = 'OTransition';
            if (bodyStyle.perspectiveProperty === undefined && bodyStyle.webkitPerspective === undefined) _.animType = false;
        }
        if (bodyStyle.MozTransform !== undefined) {
            _.animType = 'MozTransform';
            _.transformType = "-moz-transform";
            _.transitionType = 'MozTransition';
            if (bodyStyle.perspectiveProperty === undefined && bodyStyle.MozPerspective === undefined) _.animType = false;
        }
        if (bodyStyle.webkitTransform !== undefined) {
            _.animType = 'webkitTransform';
            _.transformType = "-webkit-transform";
            _.transitionType = 'webkitTransition';
            if (bodyStyle.perspectiveProperty === undefined && bodyStyle.webkitPerspective === undefined) _.animType = false;
        }
        if (bodyStyle.msTransform !== undefined) {
            _.animType = 'msTransform';
            _.transformType = "-ms-transform";
            _.transitionType = 'msTransition';
            if (bodyStyle.msTransform === undefined) _.animType = false;
        }
        if (bodyStyle.transform !== undefined && _.animType !== false) {
            _.animType = 'transform';
            _.transformType = "transform";
            _.transitionType = 'transition';
        }
        _.transformsEnabled = (_.animType !== null && _.animType !== false);

    };


    Slick.prototype.setSlideClasses = function(index) {

        var _ = this,
            centerOffset, allSlides, indexOffset, remainder;

        _.$slider.find('.slick-slide').removeClass('slick-active').removeClass('slick-center');
        allSlides = _.$slider.find('.slick-slide');

        if (_.options.centerMode === true) {

            centerOffset = Math.floor(_.options.slidesToShow / 2);

            if(_.options.infinite === true) {

                if (index >= centerOffset && index <= (_.slideCount - 1) - centerOffset) {
                    _.$slides.slice(index - centerOffset, index + centerOffset + 1).addClass('slick-active');
                } else {
                    indexOffset = _.options.slidesToShow + index;
                    allSlides.slice(indexOffset - centerOffset + 1, indexOffset + centerOffset + 2).addClass('slick-active');
                }

                if (index === 0) {
                    allSlides.eq(allSlides.length - 1 - _.options.slidesToShow).addClass('slick-center');
                } else if (index === _.slideCount - 1) {
                    allSlides.eq(_.options.slidesToShow).addClass('slick-center');
                }

            }

            _.$slides.eq(index).addClass('slick-center');

        } else {

            if (index >= 0 && index <= (_.slideCount - _.options.slidesToShow)) {
                _.$slides.slice(index, index + _.options.slidesToShow).addClass('slick-active');
            } else if ( allSlides.length <= _.options.slidesToShow ) {
                allSlides.addClass('slick-active');
            } else {
                remainder = _.slideCount%_.options.slidesToShow;
                indexOffset = _.options.infinite === true ? _.options.slidesToShow + index : index;
                if(_.options.slidesToShow == _.options.slidesToScroll && (_.slideCount - index) < _.options.slidesToShow) {
                    allSlides.slice(indexOffset-(_.options.slidesToShow-remainder), indexOffset + remainder).addClass('slick-active');
                } else {
                    allSlides.slice(indexOffset, indexOffset + _.options.slidesToShow).addClass('slick-active');
                }
            }

        }

        if (_.options.lazyLoad === 'ondemand') {
            _.lazyLoad();
        }

    };

    Slick.prototype.setupInfinite = function() {

        var _ = this,
            i, slideIndex, infiniteCount;

        if (_.options.fade === true) {
            _.options.centerMode = false;
        }

        if (_.options.infinite === true && _.options.fade === false) {

            slideIndex = null;

            if (_.slideCount > _.options.slidesToShow) {

                if (_.options.centerMode === true) {
                    infiniteCount = _.options.slidesToShow + 1;
                } else {
                    infiniteCount = _.options.slidesToShow;
                }

                for (i = _.slideCount; i > (_.slideCount -
                    infiniteCount); i -= 1) {
                    slideIndex = i - 1;
                    $(_.$slides[slideIndex]).clone(true).attr('id', '')
                        .attr('index', slideIndex-_.slideCount)
                        .prependTo(_.$slideTrack).addClass('slick-cloned');
                }
                for (i = 0; i < infiniteCount; i += 1) {
                    slideIndex = i;
                    $(_.$slides[slideIndex]).clone(true).attr('id', '')
                        .attr('index', slideIndex+_.slideCount)
                        .appendTo(_.$slideTrack).addClass('slick-cloned');
                }
                _.$slideTrack.find('.slick-cloned').find('[id]').each(function() {
                    $(this).attr('id', '');
                });

            }

        }

    };

    Slick.prototype.selectHandler = function(event) {

        var _ = this;
        var index = parseInt($(event.target).parents('.slick-slide').attr("index"));
        if(!index) index = 0;

        if(_.slideCount <= _.options.slidesToShow){
            _.$slider.find('.slick-slide').removeClass('slick-active');
            _.$slides.eq(index).addClass('slick-active');
            if(_.options.centerMode === true) {
                _.$slider.find('.slick-slide').removeClass('slick-center');
                _.$slides.eq(index).addClass('slick-center');
            }
            _.asNavFor(index);
            return;
        }
        _.slideHandler(index);

    };

    Slick.prototype.slideHandler = function(index,sync,dontAnimate) {

        var targetSlide, animSlide, oldSlide, slideLeft, unevenOffset, targetLeft = null,
            _ = this;

        sync = sync || false;

        if (_.animating === true && _.options.waitForAnimate === true) {
            return;
        }

        if (_.options.fade === true && _.currentSlide === index) {
            return;
        }

        if (_.slideCount <= _.options.slidesToShow) {
            return;
        }

        if (sync === false) {
            _.asNavFor(index);
        }

        targetSlide = index;
        targetLeft = _.getLeft(targetSlide);
        slideLeft = _.getLeft(_.currentSlide);

        _.currentLeft = _.swipeLeft === null ? slideLeft : _.swipeLeft;

        if (_.options.infinite === false && _.options.centerMode === false && (index < 0 || index > _.getDotCount() * _.options.slidesToScroll)) {
            if(_.options.fade === false) {
                targetSlide = _.currentSlide;
                if(dontAnimate!==true) {
                    _.animateSlide(slideLeft, function() {
                        _.postSlide(targetSlide);
                    });
                } else {
                    _.postSlide(targetSlide);
                }
            }
            return;
        } else if (_.options.infinite === false && _.options.centerMode === true && (index < 0 || index > (_.slideCount - _.options.slidesToScroll))) {
            if(_.options.fade === false) {
                targetSlide = _.currentSlide;
                if(dontAnimate!==true) {
                    _.animateSlide(slideLeft, function() {
                        _.postSlide(targetSlide);
                    });
                } else {
                    _.postSlide(targetSlide);
                }
            }
            return;
        }

        if (_.options.autoplay === true) {
            clearInterval(_.autoPlayTimer);
        }

        if (targetSlide < 0) {
            if (_.slideCount % _.options.slidesToScroll !== 0) {
                animSlide = _.slideCount - (_.slideCount % _.options.slidesToScroll);
            } else {
                animSlide = _.slideCount + targetSlide;
            }
        } else if (targetSlide >= _.slideCount) {
            if (_.slideCount % _.options.slidesToScroll !== 0) {
                animSlide = 0;
            } else {
                animSlide = targetSlide - _.slideCount;
            }
        } else {
            animSlide = targetSlide;
        }

        _.animating = true;

        if (_.options.onBeforeChange !== null && index !== _.currentSlide) {
            _.options.onBeforeChange.call(this, _, _.currentSlide, animSlide);
        }

        oldSlide = _.currentSlide;
        _.currentSlide = animSlide;

        _.setSlideClasses(_.currentSlide);

        _.updateDots();
        _.updateArrows();

        if (_.options.fade === true) {
            if(dontAnimate!==true) {
                _.fadeSlide(oldSlide,animSlide, function() {
                    _.postSlide(animSlide);
                });
            } else {
                _.postSlide(animSlide);
            }
            return;
        }

        if(dontAnimate!==true) {
            _.animateSlide(targetLeft, function() {
                _.postSlide(animSlide);
            });
        } else {
            _.postSlide(animSlide);
        }

    };

    Slick.prototype.startLoad = function() {

        var _ = this;

        if (_.options.arrows === true && _.slideCount > _.options.slidesToShow) {

            _.$prevArrow.hide();
            _.$nextArrow.hide();

        }

        if (_.options.dots === true && _.slideCount > _.options.slidesToShow) {

            _.$dots.hide();

        }

        _.$slider.addClass('slick-loading');

    };

    Slick.prototype.swipeDirection = function() {

        var xDist, yDist, r, swipeAngle, _ = this;

        xDist = _.touchObject.startX - _.touchObject.curX;
        yDist = _.touchObject.startY - _.touchObject.curY;
        r = Math.atan2(yDist, xDist);

        swipeAngle = Math.round(r * 180 / Math.PI);
        if (swipeAngle < 0) {
            swipeAngle = 360 - Math.abs(swipeAngle);
        }

        if ((swipeAngle <= 45) && (swipeAngle >= 0)) {
            return (_.options.rtl === false ? 'left' : 'right');
        }
        if ((swipeAngle <= 360) && (swipeAngle >= 315)) {
            return (_.options.rtl === false ? 'left' : 'right');
        }
        if ((swipeAngle >= 135) && (swipeAngle <= 225)) {
            return (_.options.rtl === false ? 'right' : 'left');
        }

        return 'vertical';

    };

    Slick.prototype.swipeEnd = function(event) {

        var _ = this, slideCount;

        _.dragging = false;

        _.shouldClick = (_.touchObject.swipeLength > 10) ? false : true;

        if (_.touchObject.curX === undefined) {
            return false;
        }

        if (_.touchObject.swipeLength >= _.touchObject.minSwipe) {

            switch (_.swipeDirection()) {
                case 'left':
                    _.slideHandler(_.currentSlide + _.getSlideCount());
                    _.currentDirection = 0;
                    _.touchObject = {};
                    break;

                case 'right':
                    _.slideHandler(_.currentSlide - _.getSlideCount());
                    _.currentDirection = 1;
                    _.touchObject = {};
                    break;
            }
        } else {
            if(_.touchObject.startX !== _.touchObject.curX) {
                _.slideHandler(_.currentSlide);
                _.touchObject = {};
            }
        }

    };

    Slick.prototype.swipeHandler = function(event) {

        var _ = this;

        if ((_.options.swipe === false) || ('ontouchend' in document && _.options.swipe === false)) {
           return;
        } else if (_.options.draggable === false && event.type.indexOf('mouse') !== -1) {
           return;
        }

        _.touchObject.fingerCount = event.originalEvent && event.originalEvent.touches !== undefined ?
            event.originalEvent.touches.length : 1;

        _.touchObject.minSwipe = _.listWidth / _.options
            .touchThreshold;

        switch (event.data.action) {

            case 'start':
                _.swipeStart(event);
                break;

            case 'move':
                _.swipeMove(event);
                break;

            case 'end':
                _.swipeEnd(event);
                break;

        }

    };

    Slick.prototype.swipeMove = function(event) {

        var _ = this,
            curLeft, swipeDirection, positionOffset, touches;

        touches = event.originalEvent !== undefined ? event.originalEvent.touches : null;

        if (!_.dragging || touches && touches.length !== 1) {
            return false;
        }

        curLeft = _.getLeft(_.currentSlide);

        _.touchObject.curX = touches !== undefined ? touches[0].pageX : event.clientX;
        _.touchObject.curY = touches !== undefined ? touches[0].pageY : event.clientY;

        _.touchObject.swipeLength = Math.round(Math.sqrt(
            Math.pow(_.touchObject.curX - _.touchObject.startX, 2)));

        swipeDirection = _.swipeDirection();

        if (swipeDirection === 'vertical') {
            return;
        }

        if (event.originalEvent !== undefined && _.touchObject.swipeLength > 4) {
            event.preventDefault();
        }

        positionOffset = (_.options.rtl === false ? 1 : -1) * (_.touchObject.curX > _.touchObject.startX ? 1 : -1);

        if (_.options.vertical === false) {
            _.swipeLeft = curLeft + _.touchObject.swipeLength * positionOffset;
        } else {
            _.swipeLeft = curLeft + (_.touchObject
                .swipeLength * (_.$list.height() / _.listWidth)) * positionOffset;
        }

        if (_.options.fade === true || _.options.touchMove === false) {
            return false;
        }

        if (_.animating === true) {
            _.swipeLeft = null;
            return false;
        }

        _.setCSS(_.swipeLeft);

    };

    Slick.prototype.swipeStart = function(event) {

        var _ = this,
            touches;

        if (_.touchObject.fingerCount !== 1 || _.slideCount <= _.options.slidesToShow) {
            _.touchObject = {};
            return false;
        }

        if (event.originalEvent !== undefined && event.originalEvent.touches !== undefined) {
            touches = event.originalEvent.touches[0];
        }

        _.touchObject.startX = _.touchObject.curX = touches !== undefined ? touches.pageX : event.clientX;
        _.touchObject.startY = _.touchObject.curY = touches !== undefined ? touches.pageY : event.clientY;

        _.dragging = true;

    };

    Slick.prototype.unfilterSlides = function() {

        var _ = this;

        if (_.$slidesCache !== null) {

            _.unload();

            _.$slideTrack.children(this.options.slide).detach();

            _.$slidesCache.appendTo(_.$slideTrack);

            _.reinit();

        }

    };

    Slick.prototype.unload = function() {

        var _ = this;

        $('.slick-cloned', _.$slider).remove();
        if (_.$dots) {
            _.$dots.remove();
        }
        if (_.$prevArrow && (typeof _.options.prevArrow !== 'object')) {
            _.$prevArrow.remove();
        }
        if (_.$nextArrow && (typeof _.options.nextArrow !== 'object')) {
            _.$nextArrow.remove();
        }
        _.$slides.removeClass(
            'slick-slide slick-active slick-visible').css('width', '');

    };

    Slick.prototype.updateArrows = function() {

        var _ = this, centerOffset;

        centerOffset = Math.floor(_.options.slidesToShow / 2)

        if (_.options.arrows === true && _.options.infinite !==
            true && _.slideCount > _.options.slidesToShow) {
            _.$prevArrow.removeClass('slick-disabled');
            _.$nextArrow.removeClass('slick-disabled');
            if (_.currentSlide === 0) {
                _.$prevArrow.addClass('slick-disabled');
                _.$nextArrow.removeClass('slick-disabled');
            } else if (_.currentSlide >= _.slideCount - _.options.slidesToShow && _.options.centerMode === false) {
                _.$nextArrow.addClass('slick-disabled');
                _.$prevArrow.removeClass('slick-disabled');
            } else if (_.currentSlide > _.slideCount - _.options.slidesToShow + centerOffset  && _.options.centerMode === true) {
                _.$nextArrow.addClass('slick-disabled');
                _.$prevArrow.removeClass('slick-disabled');
            }
        }

    };

    Slick.prototype.updateDots = function() {

        var _ = this;

        if (_.$dots !== null) {

            _.$dots.find('li').removeClass('slick-active');
            _.$dots.find('li').eq(Math.floor(_.currentSlide / _.options.slidesToScroll)).addClass('slick-active');

        }

    };

    $.fn.slick = function(options) {
        var _ = this;
        return _.each(function(index, element) {

            element.slick = new Slick(element, options);

        });
    };

    $.fn.slickAdd = function(slide, slideIndex, addBefore) {
        var _ = this;
        return _.each(function(index, element) {

            element.slick.addSlide(slide, slideIndex, addBefore);

        });
    };

    $.fn.slickCurrentSlide = function() {
        var _ = this;
        return _.get(0).slick.getCurrent();
    };

    $.fn.slickFilter = function(filter) {
        var _ = this;
        return _.each(function(index, element) {

            element.slick.filterSlides(filter);

        });
    };

    $.fn.slickGoTo = function(slide, dontAnimate) {
        var _ = this;
        return _.each(function(index, element) {

            element.slick.changeSlide({
                data: {
                    message: 'index',
                    index: parseInt(slide)
                }
            }, dontAnimate);

        });
    };

    $.fn.slickNext = function() {
        var _ = this;
        return _.each(function(index, element) {

            element.slick.changeSlide({
                data: {
                    message: 'next'
                }
            });

        });
    };

    $.fn.slickPause = function() {
        var _ = this;
        return _.each(function(index, element) {

            element.slick.autoPlayClear();
            element.slick.paused = true;

        });
    };

    $.fn.slickPlay = function() {
        var _ = this;
        return _.each(function(index, element) {

            element.slick.paused = false;
            element.slick.autoPlay();

        });
    };

    $.fn.slickPrev = function() {
        var _ = this;
        return _.each(function(index, element) {

            element.slick.changeSlide({
                data: {
                    message: 'previous'
                }
            });

        });
    };

    $.fn.slickRemove = function(slideIndex, removeBefore) {
        var _ = this;
        return _.each(function(index, element) {

            element.slick.removeSlide(slideIndex, removeBefore);

        });
    };

    $.fn.slickRemoveAll = function() {
        var _ = this;
        return _.each(function(index, element) {

            element.slick.removeSlide(null, null, true);

        });
    };

    $.fn.slickGetOption = function(option) {
        var _ = this;
        return _.get(0).slick.options[option];
    };

    $.fn.slickSetOption = function(option, value, refresh) {
        var _ = this;
        return _.each(function(index, element) {

            element.slick.options[option] = value;

            if (refresh === true) {
                element.slick.unload();
                element.slick.reinit();
            }

        });
    };

    $.fn.slickUnfilter = function() {
        var _ = this;
        return _.each(function(index, element) {

            element.slick.unfilterSlides();

        });
    };

    $.fn.unslick = function() {
        var _ = this;
        return _.each(function(index, element) {

          if (element.slick) {
            element.slick.destroy();
          }

        });
    };

    $.fn.getSlick = function() {
        var s = null;
        var _ = this;
        _.each(function(index, element) {
            s = element.slick;
        });

        return s;
    };

}));

/**!
 * MixItUp v2.1.6
 *
 * @copyright Copyright 2014 KunkaLabs Limited.
 * @author    KunkaLabs Limited.
 * @link      https://mixitup.kunkalabs.com
 *
 * @license   Commercial use requires a commercial license.
 *            https://mixitup.kunkalabs.com/licenses/
 *
 *            Non-commercial use permitted under terms of CC-BY-NC license.
 *            http://creativecommons.org/licenses/by-nc/3.0/
 */

(function($, undf){
  
  /**
   * MixItUp Constructor Function
   * @constructor
   * @extends jQuery
   */
  
  $.MixItUp = function(){
    var self = this;
    
    self._execAction('_constructor', 0);
    
    $.extend(self, {
      
      /* Public Properties
      ---------------------------------------------------------------------- */
      
      selectors: {
        target: '.mix',
        filter: '.filter',
        sort: '.sort'
      },
        
      animation: {
        enable: true,
        effects: 'fade scale',
        duration: 600,
        easing: 'ease',
        perspectiveDistance: '3000',
        perspectiveOrigin: '50% 50%',
        queue: true,
        queueLimit: 1,
        animateChangeLayout: false,
        animateResizeContainer: true,
        animateResizeTargets: false,
        staggerSequence: false,
        reverseOut: false
      },
        
      callbacks: {
        onMixLoad: false,
        onMixStart: false,
        onMixBusy: false,
        onMixEnd: false,
        onMixFail: false,
        _user: false
      },
        
      controls: {
        enable: true,
        live: false,
        toggleFilterButtons: false,
        toggleLogic: 'or',
        activeClass: 'active'
      },

      layout: {
        display: 'inline-block',
        containerClass: '',
        containerClassFail: 'fail'
      },
      
      load: {
        filter: 'all',
        sort: false
      },
      
      /* Private Properties
      ---------------------------------------------------------------------- */
        
      _$body: null,
      _$container: null,
      _$targets: null,
      _$parent: null,
      _$sortButtons: null,
      _$filterButtons: null,
    
      _suckMode: false,
      _mixing: false,
      _sorting: false,
      _clicking: false,
      _loading: true,
      _changingLayout: false,
      _changingClass: false,
      _changingDisplay: false,
      
      _origOrder: [],
      _startOrder: [],
      _newOrder: [],
      _activeFilter: null,
      _toggleArray: [],
      _toggleString: '',
      _activeSort: 'default:asc',
      _newSort: null,
      _startHeight: null,
      _newHeight: null,
      _incPadding: true,
      _newDisplay: null,
      _newClass: null,
      _targetsBound: 0,
      _targetsDone: 0,
      _queue: [],
        
      _$show: $(),
      _$hide: $()
    });
  
    self._execAction('_constructor', 1);
  };
  
  /**
   * MixItUp Prototype
   * @override
   */
  
  $.MixItUp.prototype = {
    constructor: $.MixItUp,
    
    /* Static Properties
    ---------------------------------------------------------------------- */
    
    _instances: {},
    _handled: {
      _filter: {},
      _sort: {}
    },
    _bound: {
      _filter: {},
      _sort: {}
    },
    _actions: {},
    _filters: {},
    
    /* Static Methods
    ---------------------------------------------------------------------- */
    
    /**
     * Extend
     * @since 2.1.0
     * @param {object} new properties/methods
     * @extends {object} prototype
     */
    
    extend: function(extension){
      for(var key in extension){
        $.MixItUp.prototype[key] = extension[key];
      }
    },
    
    /**
     * Add Action
     * @since 2.1.0
     * @param {string} hook name
     * @param {string} namespace
     * @param {function} function to execute
     * @param {number} priority
     * @extends {object} $.MixItUp.prototype._actions
     */
    
    addAction: function(hook, name, func, priority){
      $.MixItUp.prototype._addHook('_actions', hook, name, func, priority);
    },
    
    /**
     * Add Filter
     * @since 2.1.0
     * @param {string} hook name
     * @param {string} namespace
     * @param {function} function to execute
     * @param {number} priority
     * @extends {object} $.MixItUp.prototype._filters
     */
    
    addFilter: function(hook, name, func, priority){
      $.MixItUp.prototype._addHook('_filters', hook, name, func, priority);
    },
    
    /**
     * Add Hook
     * @since 2.1.0
     * @param {string} type of hook
     * @param {string} hook name
     * @param {function} function to execute
     * @param {number} priority
     * @extends {object} $.MixItUp.prototype._filters
     */
    
    _addHook: function(type, hook, name, func, priority){
      var collection = $.MixItUp.prototype[type],
        obj = {};
        
      priority = (priority === 1 || priority === 'post') ? 'post' : 'pre';
        
      obj[hook] = {};
      obj[hook][priority] = {};
      obj[hook][priority][name] = func;

      $.extend(true, collection, obj);
    },
    
    
    /* Private Methods
    ---------------------------------------------------------------------- */
    
    /**
     * Initialise
     * @since 2.0.0
     * @param {object} domNode
     * @param {object} config
     */
    
    _init: function(domNode, config){
      var self = this;
      
      self._execAction('_init', 0, arguments);
      
      config && $.extend(true, self, config);
      
      self._$body = $('body');
      self._domNode = domNode;
      self._$container = $(domNode);
      self._$container.addClass(self.layout.containerClass);
      self._id = domNode.id;
      
      self._platformDetect();
      
      self._brake = self._getPrefixedCSS('transition', 'none');
      
      self._refresh(true);
      
      self._$parent = self._$targets.parent().length ? self._$targets.parent() : self._$container;
      
      if(self.load.sort){
        self._newSort = self._parseSort(self.load.sort);
        self._newSortString = self.load.sort;
        self._activeSort = self.load.sort;
        self._sort();
        self._printSort();
      }
      
      self._activeFilter = self.load.filter === 'all' ? 
        self.selectors.target : 
        self.load.filter === 'none' ?
          '' :
          self.load.filter;
      
      self.controls.enable && self._bindHandlers();
      
      if(self.controls.toggleFilterButtons){
        self._buildToggleArray();
        
        for(var i = 0; i < self._toggleArray.length; i++){
          self._updateControls({filter: self._toggleArray[i], sort: self._activeSort}, true);
        };
      } else if(self.controls.enable){
        self._updateControls({filter: self._activeFilter, sort: self._activeSort});
      }
      
      self._filter();
      
      self._init = true;
      
      self._$container.data('mixItUp',self);
      
      self._execAction('_init', 1, arguments);
      
      self._buildState();
      
      self._$targets.css(self._brake);
    
      self._goMix(self.animation.enable);
    },
    
    /**
     * Platform Detect
     * @since 2.0.0
     */
    
    _platformDetect: function(){
      var self = this,
        vendorsTrans = ['Webkit', 'Moz', 'O', 'ms'],
        vendorsRAF = ['webkit', 'moz'],
        chrome = window.navigator.appVersion.match(/Chrome\/(\d+)\./) || false,
        ff = typeof InstallTrigger !== 'undefined',
        prefix = function(el){
          for (var i = 0; i < vendorsTrans.length; i++){
            if (vendorsTrans[i] + 'Transition' in el.style){
              return {
                prefix: '-'+vendorsTrans[i].toLowerCase()+'-',
                vendor: vendorsTrans[i]
              };
            };
          }; 
          return 'transition' in el.style ? '' : false;
        },
        transPrefix = prefix(self._domNode);
        
      self._execAction('_platformDetect', 0);
      
      self._chrome = chrome ? parseInt(chrome[1], 10) : false;
      self._ff = ff ? parseInt(window.navigator.userAgent.match(/rv:([^)]+)\)/)[1]) : false;
      self._prefix = transPrefix.prefix;
      self._vendor = transPrefix.vendor;
      self._suckMode = window.atob && self._prefix ? false : true;

      self._suckMode && (self.animation.enable = false);
      (self._ff && self._ff <= 4) && (self.animation.enable = false);
      
      /* Polyfills
      ---------------------------------------------------------------------- */
      
      /**
       * window.requestAnimationFrame
       */
      
      for(var x = 0; x < vendorsRAF.length && !window.requestAnimationFrame; x++){
        window.requestAnimationFrame = window[vendorsRAF[x]+'RequestAnimationFrame'];
      }

      /**
       * Object.getPrototypeOf
       */

      if(typeof Object.getPrototypeOf !== 'function'){
        if(typeof 'test'.__proto__ === 'object'){
          Object.getPrototypeOf = function(object){
            return object.__proto__;
          };
        } else {
          Object.getPrototypeOf = function(object){
            return object.constructor.prototype;
          };
        }
      }

      /**
       * Element.nextElementSibling
       */
      
      if(self._domNode.nextElementSibling === undf){
        Object.defineProperty(Element.prototype, 'nextElementSibling',{
          get: function(){
            var el = this.nextSibling;
            
            while(el){
              if(el.nodeType ===1){
                return el;
              }
              el = el.nextSibling;
            }
            return null;
          }
        });
      }
      
      self._execAction('_platformDetect', 1);
    },
    
    /**
     * Refresh
     * @since 2.0.0
     * @param {boolean} init
     * @param {boolean} force
     */
    
    _refresh: function(init, force){
      var self = this;
        
      self._execAction('_refresh', 0, arguments);

      self._$targets = self._$container.find(self.selectors.target);
      
      for(var i = 0;  i < self._$targets.length; i++){
        var target = self._$targets[i];
          
        if(target.dataset === undf || force){
            
          target.dataset = {};
          
          for(var j = 0; j < target.attributes.length; j++){
            
            var attr =  target.attributes[j],
              name = attr.name,
              val = attr.value;
              
            if(name.indexOf('data-') > -1){
              var dataName = self._helpers._camelCase(name.substring(5,name.length));
              target.dataset[dataName] = val;
            }
          }
        }
        
        if(target.mixParent === undf){
          target.mixParent = self._id;
        }
      }
      
      if(
        (self._$targets.length && init) ||
        (!self._origOrder.length && self._$targets.length)
      ){
        self._origOrder = [];
        
        for(var i = 0;  i < self._$targets.length; i++){
          var target = self._$targets[i];
          
          self._origOrder.push(target);
        }
      }
      
      self._execAction('_refresh', 1, arguments);
    },
    
    /**
     * Bind Handlers
     * @since 2.0.0
     */
    
    _bindHandlers: function(){
      var self = this,
        filters = $.MixItUp.prototype._bound._filter,
        sorts = $.MixItUp.prototype._bound._sort;
      
      self._execAction('_bindHandlers', 0);
      
      if(self.controls.live){
        self._$body
          .on('click.mixItUp.'+self._id, self.selectors.sort, function(){
            self._processClick($(this), 'sort');
          })
          .on('click.mixItUp.'+self._id, self.selectors.filter, function(){
            self._processClick($(this), 'filter');
          });
      } else {
        self._$sortButtons = $(self.selectors.sort);
        self._$filterButtons = $(self.selectors.filter);
        
        self._$sortButtons.on('click.mixItUp.'+self._id, function(){
          self._processClick($(this), 'sort');
        });
        
        self._$filterButtons.on('click.mixItUp.'+self._id, function(){
          self._processClick($(this), 'filter');
        });
      }

      filters[self.selectors.filter] = (filters[self.selectors.filter] === undf) ? 1 : filters[self.selectors.filter] + 1;
      sorts[self.selectors.sort] = (sorts[self.selectors.sort] === undf) ? 1 : sorts[self.selectors.sort] + 1;
      
      self._execAction('_bindHandlers', 1);
    },
    
    /**
     * Process Click
     * @since 2.0.0
     * @param {object} $button
     * @param {string} type
     */
    
    _processClick: function($button, type){
      var self = this,
        trackClick = function($button, type, off){
          var proto = $.MixItUp.prototype;
            
          proto._handled['_'+type][self.selectors[type]] = (proto._handled['_'+type][self.selectors[type]] === undf) ? 
            1 : 
            proto._handled['_'+type][self.selectors[type]] + 1;

          if(proto._handled['_'+type][self.selectors[type]] === proto._bound['_'+type][self.selectors[type]]){
            $button[(off ? 'remove' : 'add')+'Class'](self.controls.activeClass);
            delete proto._handled['_'+type][self.selectors[type]];
          }
        };
      
      self._execAction('_processClick', 0, arguments);
      
      if(!self._mixing || (self.animation.queue && self._queue.length < self.animation.queueLimit)){
        self._clicking = true;
        
        if(type === 'sort'){
          var sort = $button.attr('data-sort');
          
          if(!$button.hasClass(self.controls.activeClass) || sort.indexOf('random') > -1){
            $(self.selectors.sort).removeClass(self.controls.activeClass);
            trackClick($button, type);
            self.sort(sort);
          }
        }
        
        if(type === 'filter') {
          var filter = $button.attr('data-filter'),
            ndx,
            seperator = self.controls.toggleLogic === 'or' ? ',' : '';
          
          if(!self.controls.toggleFilterButtons){
            if(!$button.hasClass(self.controls.activeClass)){
              $(self.selectors.filter).removeClass(self.controls.activeClass);
              trackClick($button, type);
              self.filter(filter);
            }
          } else {
            self._buildToggleArray();
            
            if(!$button.hasClass(self.controls.activeClass)){
              trackClick($button, type);
              
              self._toggleArray.push(filter);
            } else {
              trackClick($button, type, true);
              ndx = self._toggleArray.indexOf(filter);
              self._toggleArray.splice(ndx, 1);
            }
            
            self._toggleArray = $.grep(self._toggleArray,function(n){return(n);});
            
            self._toggleString = self._toggleArray.join(seperator);

            self.filter(self._toggleString);
          }
        }
        
        self._execAction('_processClick', 1, arguments);
      } else {
        if(typeof self.callbacks.onMixBusy === 'function'){
          self.callbacks.onMixBusy.call(self._domNode, self._state, self);
        }
        self._execAction('_processClickBusy', 1, arguments);
      }
    },
    
    /**
     * Build Toggle Array
     * @since 2.0.0
     */
    
    _buildToggleArray: function(){
      var self = this,
        activeFilter = self._activeFilter.replace(/\s/g, '');
      
      self._execAction('_buildToggleArray', 0, arguments);
      
      if(self.controls.toggleLogic === 'or'){
        self._toggleArray = activeFilter.split(',');
      } else {
        self._toggleArray = activeFilter.split('.');
        
        !self._toggleArray[0] && self._toggleArray.shift();
        
        for(var i = 0, filter; filter = self._toggleArray[i]; i++){
          self._toggleArray[i] = '.'+filter;
        }
      }
      
      self._execAction('_buildToggleArray', 1, arguments);
    },
    
    /**
     * Update Controls
     * @since 2.0.0
     * @param {object} command
     * @param {boolean} multi
     */
    
    _updateControls: function(command, multi){
      var self = this,
        output = {
          filter: command.filter,
          sort: command.sort
        },
        update = function($el, filter){
          (multi && type == 'filter' && !(output.filter === 'none' || output.filter === '')) ?
            $el.filter(filter).addClass(self.controls.activeClass) :
            $el.removeClass(self.controls.activeClass).filter(filter).addClass(self.controls.activeClass);
        },
        type = 'filter',
        $el = null;
        
      self._execAction('_updateControls', 0, arguments);
        
      (command.filter === undf) && (output.filter = self._activeFilter);
      (command.sort === undf) && (output.sort = self._activeSort);
      (output.filter === self.selectors.target) && (output.filter = 'all');
      
      for(var i = 0; i < 2; i++){
        $el = self.controls.live ? $(self.selectors[type]) : self['_$'+type+'Buttons'];
        $el && update($el, '[data-'+type+'="'+output[type]+'"]');
        type = 'sort';
      }
      
      self._execAction('_updateControls', 1, arguments);
    },
    
    /**
     * Filter (private)
     * @since 2.0.0
     */
    
    _filter: function(){
      var self = this;
      
      self._execAction('_filter', 0);
      
      for(var i = 0; i < self._$targets.length; i++){
        var $target = $(self._$targets[i]);
        
        if($target.is(self._activeFilter)){
          self._$show = self._$show.add($target);
        } else {
          self._$hide = self._$hide.add($target);
        }
      }
      
      self._execAction('_filter', 1);
    },
    
    /**
     * Sort (private)
     * @since 2.0.0
     */
    
    _sort: function(){
      var self = this,
        arrayShuffle = function(oldArray){
          var newArray = oldArray.slice(),
            len = newArray.length,
            i = len;

          while(i--){
            var p = parseInt(Math.random()*len);
            var t = newArray[i];
            newArray[i] = newArray[p];
            newArray[p] = t;
          };
          return newArray; 
        };
        
      self._execAction('_sort', 0);
      
      self._startOrder = [];
      
      for(var i = 0; i < self._$targets.length; i++){
        var target = self._$targets[i];
        
        self._startOrder.push(target);
      }
      
      switch(self._newSort[0].sortBy){
        case 'default':
          self._newOrder = self._origOrder;
          break;
        case 'random':
          self._newOrder = arrayShuffle(self._startOrder);
          break;
        case 'custom':
          self._newOrder = self._newSort[0].order;
          break;
        default:
          self._newOrder = self._startOrder.concat().sort(function(a, b){
            return self._compare(a, b);
          });
      }
      
      self._execAction('_sort', 1);
    },
    
    /**
     * Compare Algorithm
     * @since 2.0.0
     * @param {string|number} a
     * @param {string|number} b
     * @param {number} depth (recursion)
     * @return {number}
     */
    
    _compare: function(a, b, depth){
      depth = depth ? depth : 0;
    
      var self = this,
        order = self._newSort[depth].order,
        getData = function(el){
          return el.dataset[self._newSort[depth].sortBy] || 0;
        },
        attrA = isNaN(getData(a) * 1) ? getData(a).toLowerCase() : getData(a) * 1,
        attrB = isNaN(getData(b) * 1) ? getData(b).toLowerCase() : getData(b) * 1;
        
      if(attrA < attrB)
        return order == 'asc' ? -1 : 1;
      if(attrA > attrB)
        return order == 'asc' ? 1 : -1;
      if(attrA == attrB && self._newSort.length > depth+1)
        return self._compare(a, b, depth+1);

      return 0;
    },
    
    /**
     * Print Sort
     * @since 2.0.0
     * @param {boolean} reset
     */
    
    _printSort: function(reset){
      var self = this,
        order = reset ? self._startOrder : self._newOrder,
        targets = self._$parent[0].querySelectorAll(self.selectors.target),
        nextSibling = targets[targets.length -1].nextElementSibling,
        frag = document.createDocumentFragment();
        
      self._execAction('_printSort', 0, arguments);
      
      for(var i = 0; i < targets.length; i++){
        var target = targets[i],
          whiteSpace = target.nextSibling;

        if(target.style.position === 'absolute') continue;
      
        if(whiteSpace && whiteSpace.nodeName == '#text'){
          self._$parent[0].removeChild(whiteSpace);
        }
        
        self._$parent[0].removeChild(target);
      }
      
      for(var i = 0; i < order.length; i++){
        var el = order[i];

        if(self._newSort[0].sortBy == 'default' && self._newSort[0].order == 'desc' && !reset){
          var firstChild = frag.firstChild;
          frag.insertBefore(el, firstChild);
          frag.insertBefore(document.createTextNode(' '), el);
        } else {
          frag.appendChild(el);
          frag.appendChild(document.createTextNode(' '));
        }
      }
      
      nextSibling ? 
        self._$parent[0].insertBefore(frag, nextSibling) :
        self._$parent[0].appendChild(frag);
        
      self._execAction('_printSort', 1, arguments);
    },
    
    /**
     * Parse Sort
     * @since 2.0.0
     * @param {string} sortString
     * @return {array} newSort
     */
    
    _parseSort: function(sortString){
      var self = this,
        rules = typeof sortString === 'string' ? sortString.split(' ') : [sortString],
        newSort = [];
        
      for(var i = 0; i < rules.length; i++){
        var rule = typeof sortString === 'string' ? rules[i].split(':') : ['custom', rules[i]],
          ruleObj = {
            sortBy: self._helpers._camelCase(rule[0]),
            order: rule[1] || 'asc'
          };
          
        newSort.push(ruleObj);
        
        if(ruleObj.sortBy == 'default' || ruleObj.sortBy == 'random') break;
      }
      
      return self._execFilter('_parseSort', newSort, arguments);
    },
    
    /**
     * Parse Effects
     * @since 2.0.0
     * @return {object} effects
     */
    
    _parseEffects: function(){
      var self = this,
        effects = {
          opacity: '',
          transformIn: '',
          transformOut: '',
          filter: ''
        },
        parse = function(effect, extract, reverse){
          if(self.animation.effects.indexOf(effect) > -1){
            if(extract){
              var propIndex = self.animation.effects.indexOf(effect+'(');
              if(propIndex > -1){
                var str = self.animation.effects.substring(propIndex),
                  match = /\(([^)]+)\)/.exec(str),
                  val = match[1];

                  return {val: val};
              }
            }
            return true;
          } else {
            return false;
          }
        },
        negate = function(value, invert){
          if(invert){
            return value.charAt(0) === '-' ? value.substr(1, value.length) : '-'+value;
          } else {
            return value;
          }
        },
        buildTransform = function(key, invert){
          var transforms = [
            ['scale', '.01'],
            ['translateX', '20px'],
            ['translateY', '20px'],
            ['translateZ', '20px'],
            ['rotateX', '90deg'],
            ['rotateY', '90deg'],
            ['rotateZ', '180deg'],
          ];
          
          for(var i = 0; i < transforms.length; i++){
            var prop = transforms[i][0],
              def = transforms[i][1],
              inverted = invert && prop !== 'scale';
              
            effects[key] += parse(prop) ? prop+'('+negate(parse(prop, true).val || def, inverted)+') ' : '';
          }
        };
      
      effects.opacity = parse('fade') ? parse('fade',true).val || '0' : '1';
      
      buildTransform('transformIn');
      
      self.animation.reverseOut ? buildTransform('transformOut', true) : (effects.transformOut = effects.transformIn);

      effects.transition = {};
      
      effects.transition = self._getPrefixedCSS('transition','all '+self.animation.duration+'ms '+self.animation.easing+', opacity '+self.animation.duration+'ms linear');
    
      self.animation.stagger = parse('stagger') ? true : false;
      self.animation.staggerDuration = parseInt(parse('stagger') ? (parse('stagger',true).val ? parse('stagger',true).val : 100) : 100);

      return self._execFilter('_parseEffects', effects);
    },
    
    /**
     * Build State
     * @since 2.0.0
     * @param {boolean} future
     * @return {object} futureState
     */
    
    _buildState: function(future){
      var self = this,
        state = {};
      
      self._execAction('_buildState', 0);
      
      state = {
        activeFilter: self._activeFilter === '' ? 'none' : self._activeFilter,
        activeSort: future && self._newSortString ? self._newSortString : self._activeSort,
        fail: !self._$show.length && self._activeFilter !== '',
        $targets: self._$targets,
        $show: self._$show,
        $hide: self._$hide,
        totalTargets: self._$targets.length,
        totalShow: self._$show.length,
        totalHide: self._$hide.length,
        display: future && self._newDisplay ? self._newDisplay : self.layout.display
      };
      
      if(future){
        return self._execFilter('_buildState', state);
      } else {
        self._state = state;
        
        self._execAction('_buildState', 1);
      }
    },
    
    /**
     * Go Mix
     * @since 2.0.0
     * @param {boolean} animate
     */
    
    _goMix: function(animate){
      var self = this,
        phase1 = function(){
          if(self._chrome && (self._chrome === 31)){
            chromeFix(self._$parent[0]);
          }
          
          self._setInter();
          
          phase2();
        },
        phase2 = function(){
          var scrollTop = window.pageYOffset,
            scrollLeft = window.pageXOffset,
            docHeight = document.documentElement.scrollHeight;

          self._getInterMixData();
          
          self._setFinal();

          self._getFinalMixData();

          (window.pageYOffset !== scrollTop) && window.scrollTo(scrollLeft, scrollTop);

          self._prepTargets();
          
          if(window.requestAnimationFrame){
            requestAnimationFrame(phase3);
          } else {
            setTimeout(function(){
              phase3();
            },20);
          }
        },
        phase3 = function(){
          self._animateTargets();

          if(self._targetsBound === 0){
            self._cleanUp();
          }
        },
        chromeFix = function(grid){
          var parent = grid.parentElement,
            placeholder = document.createElement('div'),
            frag = document.createDocumentFragment();

          parent.insertBefore(placeholder, grid);
          frag.appendChild(grid);
          parent.replaceChild(grid, placeholder);
        },
        futureState = self._buildState(true);
        
      self._execAction('_goMix', 0, arguments);
        
      !self.animation.duration && (animate = false);

      self._mixing = true;
      
      self._$container.removeClass(self.layout.containerClassFail);
      
      if(typeof self.callbacks.onMixStart === 'function'){
        self.callbacks.onMixStart.call(self._domNode, self._state, futureState, self);
      }
      
      self._$container.trigger('mixStart', [self._state, futureState, self]);
      
      self._getOrigMixData();
      
      if(animate && !self._suckMode){
      
        window.requestAnimationFrame ?
          requestAnimationFrame(phase1) :
          phase1();
      
      } else {
        self._cleanUp();
      }
      
      self._execAction('_goMix', 1, arguments);
    },
    
    /**
     * Get Target Data
     * @since 2.0.0
     */
    
    _getTargetData: function(el, stage){
      var self = this,
        elStyle;
      
      el.dataset[stage+'PosX'] = el.offsetLeft;
      el.dataset[stage+'PosY'] = el.offsetTop;

      if(self.animation.animateResizeTargets){
        elStyle = window.getComputedStyle(el);
      
        el.dataset[stage+'MarginBottom'] = parseInt(elStyle.marginBottom);
        el.dataset[stage+'MarginRight'] = parseInt(elStyle.marginRight);
        el.dataset[stage+'Width'] = el.offsetWidth;
        el.dataset[stage+'Height'] = el.offsetHeight;
      }
    },
    
    /**
     * Get Original Mix Data
     * @since 2.0.0
     */
    
    _getOrigMixData: function(){
      var self = this,
        parentStyle = !self._suckMode ? window.getComputedStyle(self._$parent[0]) : {boxSizing: ''},
        parentBS = parentStyle.boxSizing || parentStyle[self._vendor+'BoxSizing'];
  
      self._incPadding = (parentBS === 'border-box');
      
      self._execAction('_getOrigMixData', 0);
      
      !self._suckMode && (self.effects = self._parseEffects());
    
      self._$toHide = self._$hide.filter(':visible');
      self._$toShow = self._$show.filter(':hidden');
      self._$pre = self._$targets.filter(':visible');

      self._startHeight = self._incPadding ? 
        self._$parent.outerHeight() : 
        self._$parent.height();
        
      for(var i = 0; i < self._$pre.length; i++){
        var el = self._$pre[i];
        
        self._getTargetData(el, 'orig');
      }
      
      self._execAction('_getOrigMixData', 1);
    },
    
    /**
     * Set Intermediate Positions
     * @since 2.0.0
     */
    
    _setInter: function(){
      var self = this;
      
      self._execAction('_setInter', 0);
      
      if(self._changingLayout && self.animation.animateChangeLayout){
        self._$toShow.css('display',self._newDisplay);

        if(self._changingClass){
          self._$container
            .removeClass(self.layout.containerClass)
            .addClass(self._newClass);
        }
      } else {
        self._$toShow.css('display', self.layout.display);
      }
      
      self._execAction('_setInter', 1);
    },
    
    /**
     * Get Intermediate Mix Data
     * @since 2.0.0
     */
    
    _getInterMixData: function(){
      var self = this;
      
      self._execAction('_getInterMixData', 0);
      
      for(var i = 0; i < self._$toShow.length; i++){
        var el = self._$toShow[i];
          
        self._getTargetData(el, 'inter');
      }
      
      for(var i = 0; i < self._$pre.length; i++){
        var el = self._$pre[i];
          
        self._getTargetData(el, 'inter');
      }
      
      self._execAction('_getInterMixData', 1);
    },
    
    /**
     * Set Final Positions
     * @since 2.0.0
     */
    
    _setFinal: function(){
      var self = this;
      
      self._execAction('_setFinal', 0);
      
      self._sorting && self._printSort();

      self._$toHide.removeStyle('display');
      
      if(self._changingLayout && self.animation.animateChangeLayout){
        self._$pre.css('display',self._newDisplay);
      }
      
      self._execAction('_setFinal', 1);
    },
    
    /**
     * Get Final Mix Data
     * @since 2.0.0
     */
    
    _getFinalMixData: function(){
      var self = this;
      
      self._execAction('_getFinalMixData', 0);
  
      for(var i = 0; i < self._$toShow.length; i++){
        var el = self._$toShow[i];
          
        self._getTargetData(el, 'final');
      }
      
      for(var i = 0; i < self._$pre.length; i++){
        var el = self._$pre[i];
          
        self._getTargetData(el, 'final');
      }
      
      self._newHeight = self._incPadding ? 
        self._$parent.outerHeight() : 
        self._$parent.height();

      self._sorting && self._printSort(true);
  
      self._$toShow.removeStyle('display');
      
      self._$pre.css('display',self.layout.display);
      
      if(self._changingClass && self.animation.animateChangeLayout){
        self._$container
          .removeClass(self._newClass)
          .addClass(self.layout.containerClass);
      }
      
      self._execAction('_getFinalMixData', 1);
    },
    
    /**
     * Prepare Targets
     * @since 2.0.0
     */
    
    _prepTargets: function(){
      var self = this,
        transformCSS = {
          _in: self._getPrefixedCSS('transform', self.effects.transformIn),
          _out: self._getPrefixedCSS('transform', self.effects.transformOut)
        };

      self._execAction('_prepTargets', 0);
      
      if(self.animation.animateResizeContainer){
        self._$parent.css('height',self._startHeight+'px');
      }
      
      for(var i = 0; i < self._$toShow.length; i++){
        var el = self._$toShow[i],
          $el = $(el);
        
        el.style.opacity = self.effects.opacity;
        el.style.display = (self._changingLayout && self.animation.animateChangeLayout) ?
          self._newDisplay :
          self.layout.display;
          
        $el.css(transformCSS._in);
        
        if(self.animation.animateResizeTargets){
          el.style.width = el.dataset.finalWidth+'px';
          el.style.height = el.dataset.finalHeight+'px';
          el.style.marginRight = -(el.dataset.finalWidth - el.dataset.interWidth) + (el.dataset.finalMarginRight * 1)+'px';
          el.style.marginBottom = -(el.dataset.finalHeight - el.dataset.interHeight) + (el.dataset.finalMarginBottom * 1)+'px';
        }
      }

      for(var i = 0; i < self._$pre.length; i++){
        var el = self._$pre[i],
          $el = $(el),
          translate = {
            x: el.dataset.origPosX - el.dataset.interPosX,
            y: el.dataset.origPosY - el.dataset.interPosY
          },
          transformCSS = self._getPrefixedCSS('transform','translate('+translate.x+'px,'+translate.y+'px)');

        $el.css(transformCSS);
        
        if(self.animation.animateResizeTargets){
          el.style.width = el.dataset.origWidth+'px';
          el.style.height = el.dataset.origHeight+'px';
          
          if(el.dataset.origWidth - el.dataset.finalWidth){
            el.style.marginRight = -(el.dataset.origWidth - el.dataset.interWidth) + (el.dataset.origMarginRight * 1)+'px';
          }
          
          if(el.dataset.origHeight - el.dataset.finalHeight){
            el.style.marginBottom = -(el.dataset.origHeight - el.dataset.interHeight) + (el.dataset.origMarginBottom * 1) +'px';
          }
        }
      }
      
      self._execAction('_prepTargets', 1);
    },
    
    /**
     * Animate Targets
     * @since 2.0.0
     */
    
    _animateTargets: function(){
      var self = this;

      self._execAction('_animateTargets', 0);
      
      self._targetsDone = 0;
      self._targetsBound = 0;
      
      self._$parent
        .css(self._getPrefixedCSS('perspective', self.animation.perspectiveDistance+'px'))
        .css(self._getPrefixedCSS('perspective-origin', self.animation.perspectiveOrigin));
      
      if(self.animation.animateResizeContainer){
        self._$parent
          .css(self._getPrefixedCSS('transition','height '+self.animation.duration+'ms ease'))
          .css('height',self._newHeight+'px');
      }
      
      for(var i = 0; i < self._$toShow.length; i++){
        var el = self._$toShow[i],
          $el = $(el),
          translate = {
            x: el.dataset.finalPosX - el.dataset.interPosX,
            y: el.dataset.finalPosY - el.dataset.interPosY
          },
          delay = self._getDelay(i),
          toShowCSS = {};
        
        el.style.opacity = '';
        
        for(var j = 0; j < 2; j++){
          var a = j === 0 ? a = self._prefix : '';
          
          if(self._ff && self._ff <= 20){
            toShowCSS[a+'transition-property'] = 'all';
            toShowCSS[a+'transition-timing-function'] = self.animation.easing+'ms';
            toShowCSS[a+'transition-duration'] = self.animation.duration+'ms';
          }
          
          toShowCSS[a+'transition-delay'] = delay+'ms';
          toShowCSS[a+'transform'] = 'translate('+translate.x+'px,'+translate.y+'px)';
        }
        
        if(self.effects.transform || self.effects.opacity){
          self._bindTargetDone($el);
        }
        
        (self._ff && self._ff <= 20) ? 
          $el.css(toShowCSS) : 
          $el.css(self.effects.transition).css(toShowCSS);
      }
      
      for(var i = 0; i < self._$pre.length; i++){
        var el = self._$pre[i],
          $el = $(el),
          translate = {
            x: el.dataset.finalPosX - el.dataset.interPosX,
            y: el.dataset.finalPosY - el.dataset.interPosY
          },
          delay = self._getDelay(i);
          
        if(!(
          el.dataset.finalPosX === el.dataset.origPosX &&
          el.dataset.finalPosY === el.dataset.origPosY
        )){
          self._bindTargetDone($el);
        }
        
        $el.css(self._getPrefixedCSS('transition', 'all '+self.animation.duration+'ms '+self.animation.easing+' '+delay+'ms'));
        $el.css(self._getPrefixedCSS('transform', 'translate('+translate.x+'px,'+translate.y+'px)'));
        
        if(self.animation.animateResizeTargets){
          if(el.dataset.origWidth - el.dataset.finalWidth && el.dataset.finalWidth * 1){
            el.style.width = el.dataset.finalWidth+'px';
            el.style.marginRight = -(el.dataset.finalWidth - el.dataset.interWidth)+(el.dataset.finalMarginRight * 1)+'px';
          }
          
          if(el.dataset.origHeight - el.dataset.finalHeight && el.dataset.finalHeight * 1){
            el.style.height = el.dataset.finalHeight+'px';
            el.style.marginBottom = -(el.dataset.finalHeight - el.dataset.interHeight)+(el.dataset.finalMarginBottom * 1) +'px';
          }
        }
      }
      
      if(self._changingClass){
        self._$container
          .removeClass(self.layout.containerClass)
          .addClass(self._newClass);
      }
      
      for(var i = 0; i < self._$toHide.length; i++){
        var el = self._$toHide[i],
          $el = $(el),
          delay = self._getDelay(i),
          toHideCSS = {};

        for(var j = 0; j<2; j++){
          var a = j === 0 ? a = self._prefix : '';

          toHideCSS[a+'transition-delay'] = delay+'ms';
          toHideCSS[a+'transform'] = self.effects.transformOut;
          toHideCSS.opacity = self.effects.opacity;
        }
        
        $el.css(self.effects.transition).css(toHideCSS);
      
        if(self.effects.transform || self.effects.opacity){
          self._bindTargetDone($el);
        };
      }
      
      self._execAction('_animateTargets', 1);

    },
    
    /**
     * Bind Targets TransitionEnd
     * @since 2.0.0
     * @param {object} $el
     */
    
    _bindTargetDone: function($el){
      var self = this,
        el = $el[0];
        
      self._execAction('_bindTargetDone', 0, arguments);
      
      if(!el.dataset.bound){
        
        el.dataset.bound = true;
        self._targetsBound++;
      
        $el.on('webkitTransitionEnd.mixItUp transitionend.mixItUp',function(e){
          if(
            (e.originalEvent.propertyName.indexOf('transform') > -1 || 
            e.originalEvent.propertyName.indexOf('opacity') > -1) &&
            $(e.originalEvent.target).is(self.selectors.target)
          ){
            $el.off('.mixItUp');
            delete el.dataset.bound;
            self._targetDone();
          }
        });
      }
      
      self._execAction('_bindTargetDone', 1, arguments);
    },
    
    /**
     * Target Done
     * @since 2.0.0
     */
    
    _targetDone: function(){
      var self = this;
      
      self._execAction('_targetDone', 0);
      
      self._targetsDone++;
      
      (self._targetsDone === self._targetsBound) && self._cleanUp();
      
      self._execAction('_targetDone', 1);
    },
    
    /**
     * Clean Up
     * @since 2.0.0
     */
    
    _cleanUp: function(){
      var self = this,
        targetStyles = self.animation.animateResizeTargets ? 'transform opacity width height margin-bottom margin-right' : 'transform opacity';
        unBrake = function(){
          self._$targets.removeStyle('transition', self._prefix);
        };
        
      self._execAction('_cleanUp', 0);
      
      !self._changingLayout ?
        self._$show.css('display',self.layout.display) :
        self._$show.css('display',self._newDisplay);
      
      self._$targets.css(self._brake);
      
      self._$targets
        .removeStyle(targetStyles, self._prefix)
        .removeAttr('data-inter-pos-x data-inter-pos-y data-final-pos-x data-final-pos-y data-orig-pos-x data-orig-pos-y data-orig-height data-orig-width data-final-height data-final-width data-inter-width data-inter-height data-orig-margin-right data-orig-margin-bottom data-inter-margin-right data-inter-margin-bottom data-final-margin-right data-final-margin-bottom');
        
      self._$hide.removeStyle('display');
      
      self._$parent.removeStyle('height transition perspective-distance perspective perspective-origin-x perspective-origin-y perspective-origin perspectiveOrigin', self._prefix);
      
      if(self._sorting){
        self._printSort();
        self._activeSort = self._newSortString;
        self._sorting = false;
      }
      
      if(self._changingLayout){
        if(self._changingDisplay){
          self.layout.display = self._newDisplay;
          self._changingDisplay = false;
        }
        
        if(self._changingClass){
          self._$parent.removeClass(self.layout.containerClass).addClass(self._newClass);
          self.layout.containerClass = self._newClass;
          self._changingClass = false;
        }
        
        self._changingLayout = false;
      }
      
      self._refresh();
      
      self._buildState();
      
      if(self._state.fail){
        self._$container.addClass(self.layout.containerClassFail);
      }
      
      self._$show = $();
      self._$hide = $();
      
      if(window.requestAnimationFrame){
        requestAnimationFrame(unBrake);
      }
      
      self._mixing = false;
      
      if(typeof self.callbacks._user === 'function'){
        self.callbacks._user.call(self._domNode, self._state, self);
      }
      
      if(typeof self.callbacks.onMixEnd === 'function'){
        self.callbacks.onMixEnd.call(self._domNode, self._state, self);
      }
      
      self._$container.trigger('mixEnd', [self._state, self]);
      
      if(self._state.fail){
        (typeof self.callbacks.onMixFail === 'function') && self.callbacks.onMixFail.call(self._domNode, self._state, self);
        self._$container.trigger('mixFail', [self._state, self]);
      }
      
      if(self._loading){
        (typeof self.callbacks.onMixLoad === 'function') && self.callbacks.onMixLoad.call(self._domNode, self._state, self);
        self._$container.trigger('mixLoad', [self._state, self]);
      }
      
      if(self._queue.length){
        self._execAction('_queue', 0);
        
        self.multiMix(self._queue[0][0],self._queue[0][1],self._queue[0][2]);
        self._queue.splice(0, 1);
      }
      
      self._execAction('_cleanUp', 1);
      
      self._loading = false;
    },
    
    /**
     * Get Prefixed CSS
     * @since 2.0.0
     * @param {string} property
     * @param {string} value
     * @param {boolean} prefixValue
     * @return {object} styles
     */
    
    _getPrefixedCSS: function(property, value, prefixValue){
      var self = this,
        styles = {};
    
      for(i = 0; i < 2; i++){
        var prefix = i === 0 ? self._prefix : '';
        prefixValue ? styles[prefix+property] = prefix+value : styles[prefix+property] = value;
      }
      
      return self._execFilter('_getPrefixedCSS', styles, arguments);
    },
    
    /**
     * Get Delay
     * @since 2.0.0
     * @param {number} i
     * @return {number} delay
     */
    
    _getDelay: function(i){
      var self = this,
        n = typeof self.animation.staggerSequence === 'function' ? self.animation.staggerSequence.call(self._domNode, i, self._state) : i,
        delay = self.animation.stagger ?  n * self.animation.staggerDuration : 0;
        
      return self._execFilter('_getDelay', delay, arguments);
    },
    
    /**
     * Parse MultiMix Arguments
     * @since 2.0.0
     * @param {array} args
     * @return {object} output
     */
    
    _parseMultiMixArgs: function(args){
      var self = this,
        output = {
          command: null,
          animate: self.animation.enable,
          callback: null
        };
        
      for(var i = 0; i < args.length; i++){
        var arg = args[i];

        if(arg !== null){
          if(typeof arg === 'object' || typeof arg === 'string'){
            output.command = arg;
          } else if(typeof arg === 'boolean'){
            output.animate = arg;
          } else if(typeof arg === 'function'){
            output.callback = arg;
          }
        }
      }
      
      return self._execFilter('_parseMultiMixArgs', output, arguments);
    },
    
    /**
     * Parse Insert Arguments
     * @since 2.0.0
     * @param {array} args
     * @return {object} output
     */
    
    _parseInsertArgs: function(args){
      var self = this,
        output = {
          index: 0,
          $object: $(),
          multiMix: {filter: self._state.activeFilter},
          callback: null
        };
      
      for(var i = 0; i < args.length; i++){
        var arg = args[i];
        
        if(typeof arg === 'number'){
          output.index = arg;
        } else if(typeof arg === 'object' && arg instanceof $){
          output.$object = arg;
        } else if(typeof arg === 'object' && self._helpers._isElement(arg)){
          output.$object = $(arg);
        } else if(typeof arg === 'object' && arg !== null){
          output.multiMix = arg;
        } else if(typeof arg === 'boolean' && !arg){
          output.multiMix = false;
        } else if(typeof arg === 'function'){
          output.callback = arg;
        }
      }
      
      return self._execFilter('_parseInsertArgs', output, arguments);
    },
    
    /**
     * Execute Action
     * @since 2.0.0
     * @param {string} methodName
     * @param {boolean} isPost
     * @param {array} args
     */
    
    _execAction: function(methodName, isPost, args){
      var self = this,
        context = isPost ? 'post' : 'pre';

      if(!self._actions.isEmptyObject && self._actions.hasOwnProperty(methodName)){
        for(var key in self._actions[methodName][context]){
          self._actions[methodName][context][key].call(self, args);
        }
      }
    },
    
    /**
     * Execute Filter
     * @since 2.0.0
     * @param {string} methodName
     * @param {mixed} value
     * @return {mixed} value
     */
    
    _execFilter: function(methodName, value, args){
      var self = this;
      
      if(!self._filters.isEmptyObject && self._filters.hasOwnProperty(methodName)){
        for(var key in self._filters[methodName]){
          return self._filters[methodName][key].call(self, args);
        }
      } else {
        return value;
      }
    },
    
    /* Helpers
    ---------------------------------------------------------------------- */

    _helpers: {
      
      /**
       * CamelCase
       * @since 2.0.0
       * @param {string}
       * @return {string}
       */

      _camelCase: function(string){
        return string.replace(/-([a-z])/g, function(g){
            return g[1].toUpperCase();
        });
      },
      
      /**
       * Is Element
       * @since 2.1.3
       * @param {object} element to test
       * @return {boolean}
       */
      
      _isElement: function(el){
        if(window.HTMLElement){
          return el instanceof HTMLElement;
        } else {
          return (
            el !== null && 
            el.nodeType === 1 &&
            el.nodeName === 'string'
          );
        }
      }
    },
    
    /* Public Methods
    ---------------------------------------------------------------------- */
    
    /**
     * Is Mixing
     * @since 2.0.0
     * @return {boolean}
     */
    
    isMixing: function(){
      var self = this;
      
      return self._execFilter('isMixing', self._mixing);
    },
    
    /**
     * Filter (public)
     * @since 2.0.0
     * @param {array} arguments
     */
    
    filter: function(){
      var self = this,
        args = self._parseMultiMixArgs(arguments);

      self._clicking && (self._toggleString = '');
      
      self.multiMix({filter: args.command}, args.animate, args.callback);
    },
    
    /**
     * Sort (public)
     * @since 2.0.0
     * @param {array} arguments
     */
    
    sort: function(){
      var self = this,
        args = self._parseMultiMixArgs(arguments);

      self.multiMix({sort: args.command}, args.animate, args.callback);
    },

    /**
     * Change Layout (public)
     * @since 2.0.0
     * @param {array} arguments
     */
    
    changeLayout: function(){
      var self = this,
        args = self._parseMultiMixArgs(arguments);
        
      self.multiMix({changeLayout: args.command}, args.animate, args.callback);
    },
    
    /**
     * MultiMix
     * @since 2.0.0
     * @param {array} arguments
     */
    
    multiMix: function(){
      var self = this,
        args = self._parseMultiMixArgs(arguments);

      self._execAction('multiMix', 0, arguments);

      if(!self._mixing){
        if(self.controls.enable && !self._clicking){
          self.controls.toggleFilterButtons && self._buildToggleArray();
          self._updateControls(args.command, self.controls.toggleFilterButtons);
        }
        
        (self._queue.length < 2) && (self._clicking = false);
      
        delete self.callbacks._user;
        if(args.callback) self.callbacks._user = args.callback;
      
        var sort = args.command.sort,
          filter = args.command.filter,
          changeLayout = args.command.changeLayout;

        self._refresh();

        if(sort){
          self._newSort = self._parseSort(sort);
          self._newSortString = sort;
          
          self._sorting = true;
          self._sort();
        }
        
        if(filter !== undf){
          filter = (filter === 'all') ? self.selectors.target : filter;
  
          self._activeFilter = filter;
        }
        
        self._filter();
        
        if(changeLayout){
          self._newDisplay = (typeof changeLayout === 'string') ? changeLayout : changeLayout.display || self.layout.display;
          self._newClass = changeLayout.containerClass || '';

          if(
            self._newDisplay !== self.layout.display ||
            self._newClass !== self.layout.containerClass
          ){
            self._changingLayout = true;
            
            self._changingClass = (self._newClass !== self.layout.containerClass);
            self._changingDisplay = (self._newDisplay !== self.layout.display);
          }
        }
        
        self._$targets.css(self._brake);
        
        self._goMix(args.animate ^ self.animation.enable ? args.animate : self.animation.enable);
        
        self._execAction('multiMix', 1, arguments);
        
      } else {
        if(self.animation.queue && self._queue.length < self.animation.queueLimit){
          self._queue.push(arguments);
          
          (self.controls.enable && !self._clicking) && self._updateControls(args.command);
          
          self._execAction('multiMixQueue', 1, arguments);
          
        } else {
          if(typeof self.callbacks.onMixBusy === 'function'){
            self.callbacks.onMixBusy.call(self._domNode, self._state, self);
          }
          self._$container.trigger('mixBusy', [self._state, self]);
          
          self._execAction('multiMixBusy', 1, arguments);
        }
      }
    },
    
    /**
     * Insert
     * @since 2.0.0
     * @param {array} arguments
     */
    
    insert: function(){
      var self = this,
        args = self._parseInsertArgs(arguments),
        callback = (typeof args.callback === 'function') ? args.callback : null,
        frag = document.createDocumentFragment(),
        target = (function(){
          self._refresh();
          
          if(self._$targets.length){
            return (args.index < self._$targets.length || !self._$targets.length) ? 
              self._$targets[args.index] :
              self._$targets[self._$targets.length-1].nextElementSibling;
          } else {
            return self._$parent[0].children[0];
          }
        })();
            
      self._execAction('insert', 0, arguments);
        
      if(args.$object){
        for(var i = 0; i < args.$object.length; i++){
          var el = args.$object[i];
          
          frag.appendChild(el);
          frag.appendChild(document.createTextNode(' '));
        }

        self._$parent[0].insertBefore(frag, target);
      }
      
      self._execAction('insert', 1, arguments);
      
      if(typeof args.multiMix === 'object'){
        self.multiMix(args.multiMix, callback);
      }
    },

    /**
     * Prepend
     * @since 2.0.0
     * @param {array} arguments
     */
    
    prepend: function(){
      var self = this,
        args = self._parseInsertArgs(arguments);
        
      self.insert(0, args.$object, args.multiMix, args.callback);
    },
    
    /**
     * Append
     * @since 2.0.0
     * @param {array} arguments
     */
    
    append: function(){
      var self = this,
        args = self._parseInsertArgs(arguments);
    
      self.insert(self._state.totalTargets, args.$object, args.multiMix, args.callback);
    },
    
    /**
     * Get Option
     * @since 2.0.0
     * @param {string} string
     * @return {mixed} value
     */
    
    getOption: function(string){
      var self = this,
        getProperty = function(obj, prop){
          var parts = prop.split('.'),
            last = parts.pop(),
            l = parts.length,
            i = 1,
            current = parts[0] || prop;

          while((obj = obj[current]) && i < l){
            current = parts[i];
            i++;
          }

          if(obj !== undf){
            return obj[last] !== undf ? obj[last] : obj;
          }
        };

      return string ? self._execFilter('getOption', getProperty(self, string), arguments) : self;
    },
    
    /**
     * Set Options
     * @since 2.0.0
     * @param {object} config
     */
    
    setOptions: function(config){
      var self = this;
      
      self._execAction('setOptions', 0, arguments);
      
      typeof config === 'object' && $.extend(true, self, config);
      
      self._execAction('setOptions', 1, arguments);
    },
    
    /**
     * Get State
     * @since 2.0.0
     * @return {object} state
     */
    
    getState: function(){
      var self = this;
      
      return self._execFilter('getState', self._state, self);
    },
    
    /**
     * Force Refresh
     * @since 2.1.2
     */
    
    forceRefresh: function(){
      var self = this;
      
      self._refresh(false, true);
    },
    
    /**
     * Destroy
     * @since 2.0.0
     * @param {boolean} hideAll
     */
    
    destroy: function(hideAll){
      var self = this;
      
      self._execAction('destroy', 0, arguments);
    
      self._$body
        .add($(self.selectors.sort))
        .add($(self.selectors.filter))
        .off('.mixItUp');
      
      for(var i = 0; i < self._$targets.length; i++){
        var target = self._$targets[i];

        hideAll && (target.style.display = '');

        delete target.mixParent;
      }
      
      self._execAction('destroy', 1, arguments);
      
      delete $.MixItUp.prototype._instances[self._id];
    }
    
  };
  
  /* jQuery Methods
  ---------------------------------------------------------------------- */
  
  /**
   * jQuery .mixItUp() method
   * @since 2.0.0
   * @extends $.fn
   */
  
  $.fn.mixItUp = function(){
    var args = arguments,
      dataReturn = [],
      eachReturn,
      _instantiate = function(domNode, settings){
        var instance = new $.MixItUp(),
          rand = function(){
            return ('00000'+(Math.random()*16777216<<0).toString(16)).substr(-6).toUpperCase();
          };
          
        instance._execAction('_instantiate', 0, arguments);

        domNode.id = !domNode.id ? 'MixItUp'+rand() : domNode.id;
        
        if(!instance._instances[domNode.id]){
          instance._instances[domNode.id] = instance;
          instance._init(domNode, settings);
        }
        
        instance._execAction('_instantiate', 1, arguments);
      };
      
    eachReturn = this.each(function(){
      if(args && typeof args[0] === 'string'){
        var instance = $.MixItUp.prototype._instances[this.id];
        if(args[0] == 'isLoaded'){
          dataReturn.push(instance ? true : false);
        } else {
          var data = instance[args[0]](args[1], args[2], args[3]);
          if(data !== undf)dataReturn.push(data);
        }
      } else {
        _instantiate(this, args[0]);
      }
    });
    
    if(dataReturn.length){
      return dataReturn.length > 1 ? dataReturn : dataReturn[0];
    } else {
      return eachReturn;
    }
  };
  
  /**
   * jQuery .removeStyle() method
   * @since 2.0.0
   * @extends $.fn
   */
  
  $.fn.removeStyle = function(style, prefix){
    prefix = prefix ? prefix : '';
  
    return this.each(function(){
      var el = this,
        styles = style.split(' ');
        
      for(var i = 0; i < styles.length; i++){
        for(var j = 0; j < 2; j++){
          var prop = j ? styles[i] : prefix+styles[i];
          if(
            el.style[prop] !== undf && 
            typeof el.style[prop] !== 'unknown' &&
            el.style[prop].length > 0
          ){
            el.style[prop] = '';
          }
          if(!prefix)break;
        }
      }
      
      if(el.attributes && el.attributes.style && el.attributes.style !== undf && el.attributes.style.value === ''){
        el.attributes.removeNamedItem('style');
      }
    });
  };
  
})(jQuery);
/*! tinyscrollbar - v2.2.4 - 2014-12-19
 * http://www.baijs.com/tinyscrollbar
 *
 * Copyright (c) 2014 Maarten Baijs <wieringen@gmail.com>;
 * Licensed under the MIT license */

!function(a){"function"==typeof define&&define.amd?define(["jquery"],a):a("object"==typeof exports?require("jquery"):jQuery)}(function(a){"use strict";function b(b,e){function f(){return o.update(),h(),o}function g(){t.css(y,o.thumbPosition),q.css(y,-o.contentPosition),r.css(x,o.trackSize),s.css(x,o.trackSize),t.css(x,o.thumbSize)}function h(){u?p[0].ontouchstart=function(a){1===a.touches.length&&(a.stopPropagation(),k(a.touches[0]))}:(t.bind("mousedown",function(a){a.stopPropagation(),k(a)}),s.bind("mousedown",function(a){k(a,!0)})),a(window).resize(function(){o.update("relative")}),o.options.wheel&&window.addEventListener?b[0].addEventListener(v,l,!1):o.options.wheel&&(b[0].onmousewheel=l)}function i(){return o.contentPosition>0}function j(){return o.contentPosition<=o.contentSize-o.viewportSize-5}function k(b,c){a("body").addClass("noSelect"),z=c?t.offset()[y]:w?b.pageX:b.pageY,u?(document.ontouchmove=function(a){(o.options.touchLock||i()&&j())&&a.preventDefault(),m(a.touches[0])},document.ontouchend=n):(a(document).bind("mousemove",m),a(document).bind("mouseup",n),t.bind("mouseup",n),s.bind("mouseup",n)),m(b)}function l(c){if(o.contentRatio<1){var d=c||window.event,e=-(d.deltaY||d.detail||-1/3*d.wheelDelta)/40,f=1===d.deltaMode?o.options.wheelSpeed:1;o.contentPosition-=e*f*o.options.wheelSpeed,o.contentPosition=Math.min(o.contentSize-o.viewportSize,Math.max(0,o.contentPosition)),o.thumbPosition=o.contentPosition/o.trackRatio,b.trigger("move"),t.css(y,o.thumbPosition),q.css(y,-o.contentPosition),(o.options.wheelLock||i()&&j())&&(d=a.event.fix(d),d.preventDefault())}}function m(a){if(o.contentRatio<1){var c=w?a.pageX:a.pageY,d=c-z;u&&(d=z-c);var e=Math.min(o.trackSize-o.thumbSize,Math.max(0,o.thumbPosition+d));o.contentPosition=e*o.trackRatio,b.trigger("move"),t.css(y,e),q.css(y,-o.contentPosition)}}function n(){o.thumbPosition=parseInt(t.css(y),10)||0,a("body").removeClass("noSelect"),a(document).unbind("mousemove",m),a(document).unbind("mouseup",n),t.unbind("mouseup",n),s.unbind("mouseup",n),document.ontouchmove=document.ontouchend=null}this.options=a.extend({},d,e),this._defaults=d,this._name=c;var o=this,p=b.find(".viewport"),q=b.find(".overview"),r=b.find(".scrollbar"),s=r.find(".track"),t=r.find(".thumb"),u="ontouchstart"in document.documentElement,v="onwheel"in document.createElement("div")?"wheel":void 0!==document.onmousewheel?"mousewheel":"DOMMouseScroll",w="x"===this.options.axis,x=w?"width":"height",y=w?"left":"top",z=0;return this.contentPosition=0,this.viewportSize=0,this.contentSize=0,this.contentRatio=0,this.trackSize=0,this.trackRatio=0,this.thumbSize=0,this.thumbPosition=0,this.update=function(a){var b=x.charAt(0).toUpperCase()+x.slice(1).toLowerCase();switch(this.viewportSize=p[0]["offset"+b],this.contentSize=q[0]["scroll"+b],this.contentRatio=this.viewportSize/this.contentSize,this.trackSize=this.options.trackSize||this.viewportSize,this.thumbSize=Math.min(this.trackSize,Math.max(this.options.thumbSizeMin,this.options.thumbSize||this.trackSize*this.contentRatio)),this.trackRatio=(this.contentSize-this.viewportSize)/(this.trackSize-this.thumbSize),r.toggleClass("disable",this.contentRatio>=1),a){case"bottom":this.contentPosition=Math.max(this.contentSize-this.viewportSize,0);break;case"relative":this.contentPosition=Math.min(Math.max(this.contentSize-this.viewportSize,0),Math.max(0,this.contentPosition));break;default:this.contentPosition=parseInt(a,10)||0}return this.thumbPosition=this.contentPosition/this.trackRatio,g(),o},f()}var c="tinyscrollbar",d={axis:"y",wheel:!0,wheelSpeed:40,wheelLock:!0,touchLock:!0,trackSize:!1,thumbSize:!1,thumbSizeMin:20};a.fn[c]=function(d){return this.each(function(){a.data(this,"plugin_"+c)||a.data(this,"plugin_"+c,new b(a(this),d))})}});
/*! Lazy Load 1.9.3 - MIT license - Copyright 2010-2013 Mika Tuupola */
!function(a,b,c,d){var e=a(b);a.fn.lazyload=function(f){function g(){var b=0;i.each(function(){var c=a(this);if(!j.skip_invisible||c.is(":visible"))if(a.abovethetop(this,j)||a.leftofbegin(this,j));else if(a.belowthefold(this,j)||a.rightoffold(this,j)){if(++b>j.failure_limit)return!1}else c.trigger("appear"),b=0})}var h,i=this,j={threshold:0,failure_limit:0,event:"scroll",effect:"show",container:b,data_attribute:"original",skip_invisible:!0,appear:null,load:null,placeholder:"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC"};return f&&(d!==f.failurelimit&&(f.failure_limit=f.failurelimit,delete f.failurelimit),d!==f.effectspeed&&(f.effect_speed=f.effectspeed,delete f.effectspeed),a.extend(j,f)),h=j.container===d||j.container===b?e:a(j.container),0===j.event.indexOf("scroll")&&h.bind(j.event,function(){return g()}),this.each(function(){var b=this,c=a(b);b.loaded=!1,(c.attr("src")===d||c.attr("src")===!1)&&c.is("img")&&c.attr("src",j.placeholder),c.one("appear",function(){if(!this.loaded){if(j.appear){var d=i.length;j.appear.call(b,d,j)}a("<img />").bind("load",function(){var d=c.attr("data-"+j.data_attribute);c.hide(),c.is("img")?c.attr("src",d):c.css("background-image","url('"+d+"')"),c[j.effect](j.effect_speed),b.loaded=!0;var e=a.grep(i,function(a){return!a.loaded});if(i=a(e),j.load){var f=i.length;j.load.call(b,f,j)}}).attr("src",c.attr("data-"+j.data_attribute))}}),0!==j.event.indexOf("scroll")&&c.bind(j.event,function(){b.loaded||c.trigger("appear")})}),e.bind("resize",function(){g()}),/(?:iphone|ipod|ipad).*os 5/gi.test(navigator.appVersion)&&e.bind("pageshow",function(b){b.originalEvent&&b.originalEvent.persisted&&i.each(function(){a(this).trigger("appear")})}),a(c).ready(function(){g()}),this},a.belowthefold=function(c,f){var g;return g=f.container===d||f.container===b?(b.innerHeight?b.innerHeight:e.height())+e.scrollTop():a(f.container).offset().top+a(f.container).height(),g<=a(c).offset().top-f.threshold},a.rightoffold=function(c,f){var g;return g=f.container===d||f.container===b?e.width()+e.scrollLeft():a(f.container).offset().left+a(f.container).width(),g<=a(c).offset().left-f.threshold},a.abovethetop=function(c,f){var g;return g=f.container===d||f.container===b?e.scrollTop():a(f.container).offset().top,g>=a(c).offset().top+f.threshold+a(c).height()},a.leftofbegin=function(c,f){var g;return g=f.container===d||f.container===b?e.scrollLeft():a(f.container).offset().left,g>=a(c).offset().left+f.threshold+a(c).width()},a.inviewport=function(b,c){return!(a.rightoffold(b,c)||a.leftofbegin(b,c)||a.belowthefold(b,c)||a.abovethetop(b,c))},a.extend(a.expr[":"],{"below-the-fold":function(b){return a.belowthefold(b,{threshold:0})},"above-the-top":function(b){return!a.belowthefold(b,{threshold:0})},"right-of-screen":function(b){return a.rightoffold(b,{threshold:0})},"left-of-screen":function(b){return!a.rightoffold(b,{threshold:0})},"in-viewport":function(b){return a.inviewport(b,{threshold:0})},"above-the-fold":function(b){return!a.belowthefold(b,{threshold:0})},"right-of-fold":function(b){return a.rightoffold(b,{threshold:0})},"left-of-fold":function(b){return!a.rightoffold(b,{threshold:0})}})}(jQuery,window,document);
/**
 * Simple router/controller that will kick off the init script for the current page.
 * At present, it grabs the 'data-route' attribute on the body tag and executes that route.
 * For example, if data-route="home", the executeRoute method will call APP.home.init()
 */

'use strict';

var APP = window.APP = window.APP || {};

APP.core = {};

APP.core.controller = (function(){

    //private variable
    var _route = '';

    var setRoute = function(strVal) {
        _route = strVal;
    };

    var getRoute = function() {
        return _route;
    };

    var executeRoute = function() {

        var ns = APP;
        var route = getRoute();
        var action = 'init';
        if ( route !== '' && ns[route] && typeof ns[route][action] === 'function' ) {
            ns[route][action]();
        }
    };

    // exposed methods will be prefixed with the word `public`
    var init = function() {

        console.log('https://www.biogen.com/etc/designs/dxp/corp/APP.controller.init');

        document.getElementsByClassName = function (className) {
            return document.querySelectorAll('.' + className);
        };

        var routeName = document.querySelectorAll('.js-page-controller-action')[0].getAttribute('data-action-name');

        console.log('APP.controller.init | route name = ' + routeName);
        setRoute(routeName);
        executeRoute();

    };

    /**
    * interfaces to public functions
    */
    return {
        init: init
    };

}());

// Avoid `console` errors in browsers that lack a console.
'use strict';
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

'use strict';

var APP = window.APP = window.APP || {};

APP.global = (function(){

    var initGlobalComponents = function() {
        APP.navigation.init();
        APP.navPrimary.init();
        APP.navSecondary.init();
        APP.stickyNav.init();
        APP.mastheadCarousel.init();
        APP.tabs.init();
        APP.tabsLanding.init();
        APP.cardWindowshade.init();
        APP.cardThreeCol.init();
        APP.cardTabs.init();
        APP.productPipeline.init();
        APP.cardSocial.init();
        APP.videoCard.init();
        APP.filterDd.init();
        APP.cardProfile.init();
        APP.exitModal.init();
    };

    var initPageComponents = function() {
        APP.core.controller.init();
    };

    var init = function() {
        
        /**
        * initialize global components
        */
        initGlobalComponents();

        /**
        * initialize components for the current page
        */
        initPageComponents();


        var videoOverlay = {

            validateOverlay: function(btn){


                if ($('.videoOverlay').length === 0 ) {
                    this.createOverlay(btn);
                }
                
                this.showOverlay(btn);
                this.configurePlayer(btn);

            },

            createOverlay: function(btn){

                var overlayText = $(btn).attr('data-overlayText'),
                    overlayLinkText = $(btn).attr('data-overlayLinkText'),
                    overlayLinkUrl = $(btn).attr('data-overlayLinkUrl');

                var overlay = '<div class="videoOverlay">';
                overlay += '<div class="overlayContent">';
                overlay += '<div id="overlayVideo"></div>';
                overlay += '<div class="overlayText">';
                if(overlayText){
                    overlay +=  '<p>'+ overlayText +'</p>';
                }
                if(overlayLinkText){
                    overlay += '<a href="'+ overlayLinkUrl +'" target="_blank">'+ overlayLinkText + '</a>';
                }
                overlay += '<div class="overlayCloseBtn bio-icon-modal-close"></div>';
                overlay += '</div>';
                overlay += '</div>';
                overlay += '</div>';

                $('html').append(overlay);
                
            },

            updateOverlay: function(){

            },

            showOverlay: function(){

                $('.videoOverlay').fadeIn();

            },

            closeOverlay: function(){

                var jwplayer = window.jwplayer || {};

                $('.videoOverlay').fadeOut(500, function(){
                    jwplayer('overlayVideo').remove();
                    $(this).remove();
                });

            },

            configurePlayer: function(btn){

                var jwplayer = window.jwplayer || {};

                var videoURL = $(btn).attr('data-videosrc'),
                    videoPoster = $(btn).attr('data-poster');

                //videoURL = videoURL.split('.mp4')[0];
                // playlist: [{
                //         image: videoPoster,
                //         sources: [{
                //             file: videoURL+'.mp4'
                //         },{
                //             file: videoURL+'.webm'
                //         }]
                //     }]
                
                jwplayer('overlayVideo').setup({
                    image: videoPoster,
                    file:videoURL,
                    width: '100%',
                    aspectratio: '16:9',
                    primary: "flash",
                    autostart: true,
                    androidhls: true,
                    'modes': [
                        { type: 'html5' },
                        { type: 'flash', src: 'clientlib/js/vendor/jwplayer.flash.swf'/*tpa=https://www.biogen.com/etc/designs/dxp/corp/clientlib/js/vendor/jwplayer.flash.swf*/ }
                    ]
                });

                function fireEvent(eventName){
                    if (typeof CQ_Analytics != 'undefined' && CQ_Analytics.Sitecatalyst && populate) {
                        var data = populateAnalytics(eventName,populate.componentPath,null,null,null,null,null);
                        data.values['videoURL'] = videoURL;
                        data.values['videoName'] = videoURL.substring(videoURL.lastIndexOf("/") + 1);
                        CQ_Analytics.record(data);
                    }
                }


                jwplayer().onPlay(function(event){
                    fireEvent("videoPlay");
                });

                jwplayer().onComplete(function(event){
                    fireEvent("videoComplete");
                });
                
                var time = 0;
                jwplayer().onTime(function(event){
                    
                    if(time === 0){
                        fireEvent("videoStart");
                        time = Math.floor(event.duration/4);
                    } else if (event.position >= time && Math.floor(event.position / event.duration * 100) == 25){
                        fireEvent("video25");
                        time = Math.floor(event.duration/2);
                    } else if (event.position >= time && Math.floor(event.position / event.duration * 100) == 50){
                        fireEvent("video50");
                        time = Math.floor(event.duration * 3/4);
                    } else if (event.position >= time && Math.floor(event.position / event.duration * 100) == 75){
                        fireEvent("video75");
                        time = Math.floor(event.duration);
                    }
                });

            },

            isMobile: function() {
                var isMobile = false,
                windowWidth = $(window).width();
                if (windowWidth < 640) {
                    isMobile = true;
                }
                return isMobile;
            }

        };


        var toggleMenu = '[data-toggle="megamenu"]',
            menuItem = '.mega-menu__item',
            menuClose = '.mega-menu__close',
            activeClass = 'nav-primary__link--active';

        //Global therapies handler
        $('#megamenu_therapies a').on('click', function(e){
            $(toggleMenu).removeClass(activeClass);
            $(menuItem).slideUp();
        });

        //Global video overlay handlers
        var videoOverlayTrigger = $('.js-videoOverlayTrigger');

        videoOverlayTrigger.on('click', function(e){
            if(!videoOverlay.isMobile()){
                videoOverlay.validateOverlay(this);
                //$('html').addClass('html--menu-active');
                e.preventDefault();    
            }else{
                window.location.href = $(this).attr('data-videosrc');
            }

        });

        $('html').on('click', '.overlayCloseBtn', function(e){
            videoOverlay.closeOverlay();
            //$('html').removeClass('html--menu-active');
        });
        //Global video overlay handlers

    };

    /**
    * interfaces to public functions
    */
    return {
        init: init
    };

}());

$( document ).ready( APP.global.init );

'use strict';

var APP = window.APP = window.APP || {};

APP.navigation = (function(){
    var $menuToggleLink = $('.js-navtoggle'),
        $nav = $('.mobile-menu-dropdown'),
        $html = $('html'),
        $navigationBasic = $('.navigation__basic'),
        offsetNav = 0;

    var bindEventsToUI = function() {

        $menuToggleLink.on('click',function(e){
            e.preventDefault();
            if( $nav.is(':visible') ) {
                $html.removeClass('html--menu-active');
                $menuToggleLink.removeClass('nav-toggle--active');
                $nav.slideUp();
                //still the bar on the offset when close
                $('html, body').animate({scrollTop: offsetNav}, 0); 
            } else {
                //set the value of the navbar on the fixed position
                offsetNav = $navigationBasic.offset().top;
                $html.addClass('html--menu-active');
                $menuToggleLink.addClass('nav-toggle--active');
                $nav.slideDown();
                //move the navbar to the offset
                $('html, body').animate({scrollTop: offsetNav}, 0);
                
            }
        });

        $('[data-toggle="slide-nav"]').on('click', function(e) {
            e.preventDefault();

            var child = $(this).next('.mobile-menu-dropdown__children'),
                childLeft = child.position().left;

            if ( childLeft === 0 ) {
                child.animate({'left':'100%'});
            } else {
                $('.mobile-menu-dropdown__children').animate({'left':'100%'});
                child.animate({'left':'0px'});
            }

        });

        $('.mobile-menu-dropdown__child-heading__link').on('click', function(e) {
            e.preventDefault();
            $(this).closest('.mobile-menu-dropdown__children').animate({'left':'100%'});
        });
    };

    var init = function() {
        bindEventsToUI();
    };

    /**
     * interfaces to public functions
     */
    return {
        init: init
    };

}());

'use strict';

var APP = window.APP = window.APP || {};

APP.windowshades= (function(){

    var bindEventsToUI = function(container) {
        $(container).find('.card-therapy__windowshade__title').on('click', function(e){
            e.preventDefault();
            var target = $(this).closest('.card-therapy__windowshade');
            var classOpen = container.replace(/./i, '')+'--close';
            var classClose = container.replace(/./i, '')+'--open';
            
            target.toggleClass(classOpen)
                .toggleClass(classClose);
            
            target.find('.card-therapy__windowshade__icon')
                .toggleClass('card-therapy__windowshade__icon--plus')
                .toggleClass('card-therapy__windowshade__icon--minus');
            
            target.find(container+'__body').slideToggle();
        });
        
    };

    var init = function(container) {
        container = typeof container !== 'undefined' ? container : '.single-windowshade';
        bindEventsToUI(container);
    };

    return {
        init: init
    };

}());
'use strict';

var APP = window.APP = window.APP || {};

APP.mastheadCarousel = (function(){


    var carousel = {

        carouselWrapper: $('.masthead-carousel'),
        carousel: $('.masthead-carousel .slider'),
        carouselSlides: $('.masthead-carousel .slider li'),
        carouselBubblesWrapper: $('.carousel__bubbles'),
        carouselBubbles: $('.carousel__bubbles a'),
        carouselContent: $('.masthead-carousel .slider .slide__content'),
        carouselDescription: $('.masthead-carousel .slider .slide__description'),

        init: function(){

            //Initialize this if we are in homepage
            if(this.carouselWrapper.length !== 0){
                this.eventHandlers();
                this.carouselSlick();
                this.showSlides();
            }

        },

        tools: {
            isIE8: function(){

                if($('html').hasClass('lt-ie9')){
                    return true;
                }else{
                    return false;
                }

            }
        },
        
        isOverlayOpen: function(){
            return this.carousel.hasClass('open');
        },

        showSlides: function(){
            $('.slide').show();
        },

        carouselSlick: function(){

            var that = this,
                draggable = true;

            if(that.tools.isIE8()){
                draggable = false;                
            }

            this.carousel.slick({
                slide: 'li',
                speed: 800,
                arrows: false,
                infinite: false,
                draggable: draggable,   
                cssEase: 'cubic-bezier(0.645, 0.045, 0.355, 1.000)',
                onBeforeChange: function(carousel, index){
                    if(that.isOverlayOpen() === false){
                        if(that.tools.isIE8()){
                            that.carouselBubbles.add(that.carouselContent).hide();
                        }else{
                            that.carouselBubbles.add(that.carouselContent).fadeOut(200);
                        }
                    }
                },
                onAfterChange: function(carousel, index){
                    if(index !== 0){
                        that.assignOpenClass($(that.carouselBubbles).eq(index-1));
                        that.updateSelectedItem(index-1);
                        that.addCloseBtn();
                    }else{
                        that.removeOpenClass();
                    }
                    
                    if(that.tools.isIE8()){
                        that.carouselBubbles.add(that.carouselContent).show();
                    }else{
                        that.carouselBubbles.add(that.carouselContent).fadeIn();
                    }
                    
                }
            });
        },

        updateSelectedItem: function(item){
            this.carouselBubbles.removeClass('selected');
            $(this.carouselBubbles).eq(item).addClass('selected');
        },

        goToSlide: function(link){
            var slide = $(link).attr('data-content');
            this.carousel.slickGoTo(slide);
        },

        assignOpenClass: function(btn){
            this.carousel.addClass('open');
            this.carouselBubblesWrapper.addClass('open');
            this.carouselBubbles.removeClass('selected');
            $(btn).addClass('selected');
        },

        removeOpenClass: function(){
            this.carousel.removeClass('open');
            this.carouselBubbles.removeClass('selected');
            this.carouselBubblesWrapper.removeClass('open');
        },

        addCloseBtn: function(){
            if($('.masthead-carousel .closeBtn').length === 0){
                var closeBtn = '<div class="closeBtn"></div>';
                this.carousel.append(closeBtn);
            }
        },

        eventHandlers: function(){

            var that = this,
                fadeTime;

            if(that.tools.isIE8()){
                fadeTime = 0;
            }else{
                fadeTime = 200;
            }

            //Carousel trigger
            this.carouselBubbles.on('click', function(e){
                
                var btn = this;
                e.preventDefault();

                if(that.isOverlayOpen() === false){
                    that.carouselBubbles.add(that.carouselContent).fadeOut(fadeTime, function(){
                        that.assignOpenClass(btn);
                        that.addCloseBtn();
                    });
                }else{
                    that.assignOpenClass(btn);
                }
                that.goToSlide(this);  
            });

            this.carousel.on('click', '.closeBtn', function(){
                that.carouselBubbles.add(that.carouselContent).fadeOut(fadeTime, function(){
                    that.carousel.slickGoTo(0);
                    that.removeOpenClass();
                });
            });


        }

    };

    var init = function() {
        carousel.init();
    };

    /**
     * interfaces to public functions
     */
    return {
        init: init
    };

}());

'use strict';

var APP = window.APP = window.APP || {};

APP.productPipeline = (function(){

    var products = {

        init: function(){
            this.productInfo.init();
            this.filters.init();
            console.log(this.tools.isPipelineCard());
        },

        tools: {

            isMobile: function(){
                var isMobile = false,
                windowWidth = $(window).width();
                if (windowWidth < 760) {
                    isMobile = true;
                }
                return isMobile;
            },

            isPipelineCard: function(){
                if($('.pipeline-card').length === 0){
                    return false;
                }else{
                    return true;
                }
            }

        },

        productInfo: {

            productsWrapper: $('.product-pipeline .product-pipeline__products'),
            productWrapper: $('.product-pipeline .product-pipeline__products--product'),

            init: function(){
                this.eventHandlers();
                this.customScrollbar();
            },

            customScrollbar: function(){

                
                if (products.tools.isPipelineCard() === true){
                    this.productsWrapper.tinyscrollbar({ thumbSize: 76, wheel: true, trackSize: 280 });
                }else{
                    // if(products.tools.isMobile() === false){
                    //     this.productsWrapper.tinyscrollbar({ thumbSize: 76, wheel: true, trackSize: 675 });
                    // }
                }
                

            },

            changeArrow: function(product){
                var arrow = $(product).find('.product__info').prev();

                if(arrow.hasClass('bio-icon-arrow-thin-down')){
                    arrow.removeClass('bio-icon-arrow-thin-down').addClass('bio-icon-arrow-thin-up');
                }else{
                    arrow.removeClass('bio-icon-arrow-thin-up').addClass('bio-icon-arrow-thin-down');
                }
            },

            slide: function(product){
                $(product).find('.product__info-moreInfo').slideToggle(500, function(){
                    var customScroll = $('.product-pipeline__products').data("plugin_tinyscrollbar");
                    
                    //Update scrollbar
                    if(customScroll !== undefined){
                        customScroll.update('relative');
                    }
                });
            },

            eventHandlers: function(){

                var that = this;

                this.productWrapper.on('click', function(e){
                    that.slide(this);
                    that.changeArrow(this);
                    e.preventDefault();
                });
            }            
        },


        filters: {

            filtersTrigger: $('.product-pipeline__filter-trigger'),
            filterBtns: $('.product-pipeline__filters ul li'),

            init: function(){
                this.eventHandlers();
            },

            filterFunctionality: {

                hideItems: function(){

                },

                updateDropdownData: function(filter){
                    var filterText = $(filter).html(),
                        filterParent = $(filter).parent().prev(),
                        filterCategory = $(filter).attr('data-category');

                    $(filterParent).find('.text').html(filterText).parent().attr('data-category', filterCategory);
                    $(filterParent).find('.bio-icon-arrow-thin-down').removeClass('arrow-down');
                },

                updateSelectedItem: function(filter){
                    $(filter).parent().find('li').removeClass('selected');
                    $(filter).addClass('selected');
                },

                closeDropdown: function(filter){
                    $(filter).parent().slideUp();
                },

                validateDropdowns: function(){
                    var dropdowns = products.filters.filtersTrigger,
                        that = this,
                        items = [];

                    $.each(dropdowns, function(index, dropdown){
                        items.push($(dropdown).next().find('.selected').attr('data-category'));
                    });

                    this.showFilteredItems(items);
                },

                showFilteredItems: function(items){

                    var filters = items[0] + ";" + items[1];

                    if (typeof CQ_Analytics != 'undefined' && CQ_Analytics.Sitecatalyst && populate) {
                        var analytics = populateAnalytics('filterView',populate.componentPath,null,null,null,null,null);
                        analytics.values['filters'] = filters;

                        if($('.pipeline-card').length > 0){
                            var name = $('.pipeline-card').closest('*[data-card-name]').attr('data-card-name');
                            var pos = $('.pipeline-card').closest('*[data-card-pos]').attr('data-card-pos');
                            var cards = name + ":" + pos;
                            analytics.values['cards'] = cards;
                        }

                        CQ_Analytics.record(analytics);
                    }
                    
                    var products = $('.product-pipeline__products--product'),
                        productSeparator = $('.product-pipeline__products--separator');

                    //Show all items
                    $(products).show();
                    productSeparator.show();

                    //Then check against selected items and hide as needed
                    $.each(products, function(index, product){

                        if(items[0] === "allAreas" && items[1] === "allPhases"){
                            //If all are selected, show all items
                            $(products).show();
                        }else{
                            if(items[0] === "allAreas" && items[1] !== "allPhases"){
                                //Show all Areas but filter by phase
                                if($(product).hasClass(items[1]) === false){
                                    $(product).hide();
                                    $(product).next().hide();
                                }
                            }else if(items[0] !== "allAreas" && items[1] === "allPhases"){
                                //Show all Phases but filter by area
                                if($(product).hasClass(items[0]) === false){
                                    $(product).hide();
                                    $(product).next().hide();
                                }
                            }else{
                                //Filter both items
                                if(items[0] !== "allAreas" && items[1] !== "allPhases"){
                                    if($(product).hasClass(items[0]) === false || $(product).hasClass(items[1]) === false){
                                        $(product).hide();
                                        $(product).next().hide();
                                    }
                                }
                            }
                        }

                    });


                    
                    var customScroll = $('.product-pipeline .product-pipeline__products').data("plugin_tinyscrollbar"),
                        numOfVisibleRows = $('.product-pipeline__products--product:visible').length;


                    //Update scrollbar
                    if(customScroll !== undefined && customScroll !== null){
                        customScroll.update();    
                    }

                    if(numOfVisibleRows < 4){
                        $('.track').hide();
                    }else {
                        $('.track').show();
                    }

                    //Hide Scrollbar if the content is small
                    // if(customScroll.contentSize < 690){
                    //     $('.track').hide();
                    // }else{
                    //     $('.track').show();
                    // }

                    //Show legend if empty
                    if(numOfVisibleRows === 0){
                        $('.product-pipeline__filtersLegend').show();
                    }else{
                        $('.product-pipeline__filtersLegend').hide();
                    }

                }

            },

            dropdownFunctionality: function(btn){

                var that = this;

                $.each(that.filtersTrigger, function(index, trigger){
                    if($(trigger).next().is(':visible')){
                        $(trigger).next().slideUp();
                        $(trigger).find('.bio-icon-arrow-thin-down').removeClass('arrow-down');
                    }

                });

                if($(btn).next().is(':visible') === false){
                    $(btn).next().slideDown();

                }else{
                    $(btn).next().slideUp();
                }

            },

            eventHandlers: function(){

                var that = this;

                this.filtersTrigger.on('click', function(){
                    if($(this).find('.bio-icon-arrow-thin-down').hasClass('arrow-down')){
                        $(this).find('.bio-icon-arrow-thin-down').removeClass('arrow-down');
                    } else {
                        $(this).find('.bio-icon-arrow-thin-down').addClass('arrow-down');
                    }
                    that.dropdownFunctionality(this);
                    
                });

                this.filterBtns.on('click', function(){
                    that.filterFunctionality.updateDropdownData(this);
                    that.filterFunctionality.updateSelectedItem(this);
                    that.filterFunctionality.closeDropdown(this);
                    that.filterFunctionality.validateDropdowns(this);
                });
            }

        }

    };

    var init = function() {
        products.init();
    };

    /**
     * interfaces to public functions
     */
    return {
        init: init
    };

}());

'use strict';

var APP = window.APP = window.APP || {};

APP.navSecondary = (function(){

    var bindEventsToUI = function() {

        var menu = 'nav-secondary-menus__menu',
                allMenus = $('.' + menu),
                allLinks = $('.nav-secondary__dropdown'),
                activeClass = 'nav-secondary__dropdown--active';

        $('[data-toggle="pushdown-menu"]').on('click', function(e) {
            e.preventDefault();

            var target = $(this).attr('data-target'),
                menutarget = '#' + menu + '--' + target;

            allMenus.slideUp();
            allLinks.removeClass(activeClass);

            if( $(menutarget).is(':visible') ) {
                $(menutarget).slideUp();
                $(this).parent('.nav-secondary__dropdown').removeClass(activeClass);
            } else {
                $(menutarget).slideDown('slow');
                $(this).parent('.nav-secondary__dropdown').addClass(activeClass);
            }

        });

        $('.nav-secondary-menus__close').on('click', function() {
            allMenus.slideUp();
            allLinks.removeClass(activeClass);
        });
    };

    var init = function() {
        console.log('APP.navSecondary');
        bindEventsToUI();
    };

    /**
     * interfaces to public functions
     */
    return {
        init: init
    };

}());

'use strict';

var APP = window.APP = window.APP || {};

APP.navPrimary = (function(){

    var toggleMenu = '[data-toggle="megamenu"]',
        menuItem = '.mega-menu__item',
        menuClose = '.mega-menu__close',
        activeClass = 'nav-primary__link--active';

    var megaMenuClick = function() {
        $(toggleMenu).on('click', function(e) {
            e.preventDefault();

            var target = $(this).attr('data-target'),
                targetTriggers = $(toggleMenu + '[data-target = ' + target + ']');

            // if any mega menu is open
            if ( $(menuItem).is(':visible') ) {

                $(toggleMenu).removeClass(activeClass);

                // if the open menu was not the one just clicked
                if ( !($(target).is(':visible')) ) {
                    $(menuItem).fadeOut();
                    $(target).delay(400).slideDown();
                    $(targetTriggers).addClass(activeClass);
                } else {
                    $(target).slideUp();
                }

            } else {
                // if no menus are open, slide down the target
                $(target).slideDown('slow');
                $(targetTriggers).addClass(activeClass);
            }
        });
    };

    var megaMenuClose = function() {
        $(menuClose).on('click', function(e) {
            e.preventDefault();
            $(toggleMenu).removeClass(activeClass);
            $(menuItem).slideUp();
        });
    };

    var bindEventsToUI = function() {
        megaMenuClick();
        megaMenuClose();
        $('body').on('click', function(e){
            $(toggleMenu).removeClass(activeClass);
            $(menuItem).slideUp();
        });

        $('.navigation__full').on('click', function(e){
            e.stopPropagation();
        });
    };

    var init = function() {
        console.log('APP.navPrimary');
        bindEventsToUI();
    };

    /**
     * interfaces to public functions
     */
    return {
        init: init
    };

}());

'use strict';

var APP = window.APP = window.APP || {};

APP.stickyNav = (function(){

    var $stickyNav = $('.sticky-nav');

    var bindEventsToUI = function() {
        $(window).on('scroll', function() {
            var offset = $(window).scrollTop(),
                stickyThreshold = 400,
                $megaMenus = $('.mega-menu');

            if (offset >= stickyThreshold && $stickyNav.not(':visible')) {
                $stickyNav.addClass('sticky-nav--stuck');
                $megaMenus.addClass('mega-menu--sticky');

            } else if (offset < stickyThreshold) {
                $stickyNav.removeClass('sticky-nav--stuck');
                $megaMenus.removeClass('mega-menu--sticky');
            }
        });
    };

    var init = function() {
        bindEventsToUI();
    };

    /**
     * interfaces to public functions
     */
    return {
        init: init
    };

}());

'use strict';

var APP = window.APP = window.APP || {};

APP.tabs = (function(){

    var bindEventsToUI = function() {
        openLayoutEvents();
        dropdownTabs();
        allTabs();
        chooseLogo();
        var hash = window.location.hash;
        if(hash !== ''){
            openWithHash(hash);
        }
        bindEvent(window,'hashchange',function(){
            var hash = window.location.hash;
            if(hash !== ''){
                openWithHash(hash);   
            }
               
        });
    };

    var openLayoutEvents = function () {
        var $readmore = $('.tabs__readmore, .tabs__plus'),
        $plus,
        $container,
        marginHeight = 0,
        isOpen = false;
        

        $readmore.click( function() {
            if(isMobile()){
                marginHeight = 75; 
            }else{
                marginHeight = 100; 
            }
            $container = $(this).closest('.tabs__contentcontainer');
            $plus = $container.find('.plus');
            
            if ($container.hasClass('open')) {
                //close layout
                $container.animate({height: '476px'});
                var id = $container.parent().attr('id');
                scrollToID(id); 
                isOpen = false;
                $container.find('.tabs__arrow-down').show();
                $container.find('.tabs__arrow-up').hide();
                $plus.removeClass('tabs__minus');
                $plus.addClass('tabs__plus');
                $container.removeClass('open');
            } else {
                //open layout
                var curHeight = $container.height(), autoHeight = $container.css({height: 'auto'}).height()+marginHeight;
                $container.height(curHeight).animate({height: autoHeight});
                $container.find('.tabs__arrow-down').hide();
                $container.find('.tabs__arrow-up').show();
                $container.addClass('open');
                isOpen = true;
                $plus.removeClass('tabs__plus');
                $plus.addClass('tabs__minus');
            }
        });

        $(window).resize(function(){
            if(isMobile()){
                marginHeight = 75; 
            }else{
                marginHeight = 100; 
            }
            if(isOpen){
                var curHeight = $container.height(), autoHeight = $container.css({height: 'auto'}).height()+marginHeight;
                $container.height(curHeight).animate({height: autoHeight});
            }
        });
    };

    var scrollToID = function (id) {
        if(isMobile()){
            $('html, body').animate({scrollTop: $('#'+id).offset().top-150}, 500);
        }else{
            $('html, body').animate({scrollTop: $('#'+id).offset().top-60}, 500, function(){
                $('.sticky-nav').addClass('sticky-nav--stuck');
            });     
        }
    };

    var allTabs = function () {
        var $allTabs = $('#tabAll');
        $allTabs.click(function(e){
            //e.preventDefault();
            $allTabs.addClass('active');
            $allTabs.addClass('selected');
            $('.tab-pane').each(function(i,t){
                $('.tabs__tabscontainer').removeClass('active');
                $(this).addClass('active');
            });
            $('.tabs__contentcontainer').removeClass('last');
            $('.tabs__contentcontainer').last().addClass('last');
            scrollToID('tabAll');
        });
    };

    var isMobile = function(){
        var isMobile = false,
        windowWidth = $(window).width();
        if (windowWidth < 640) {
            isMobile = true;
        }
        return isMobile;
    };

    var dropdownTabs = function() {
        
        $('.tabs__dropdown-activer').click(function(){
            $('.tabs__tabscontainer li').slideToggle();
            if($(this).find('span').hasClass('arrow-down')){
                $(this).find('span').removeClass('arrow-down');
            }else {
                $(this).find('span').addClass('arrow-down');    
            }
        });    
        $('.tabs__tabscontainer li').click(function(){
            $('.tabs__tabscontainer li').removeClass('active');
            $('.tabs__tabscontainer li').removeClass('selected');
            var $this = $(this),
            href = $this.find('a').attr('href'),
            $container = $(href).closest('.tabs__contentcontainer');
            $this.addClass('active');
            $this.addClass('selected');
            if(href !== '#'){
                $('.tab-pane').removeClass('active');
                $(href).addClass('active');
                $('.tabs__contentcontainer').removeClass('last');
                $(href).find('.tabs__contentcontainer').addClass('last');
                scrollToID(href.replace('#',''));   
            }
            $.each($('.tabs__contentcontainer'), function(index, val){
                //console.log($(this).hasClass('open'));
                if($(this).hasClass('open')){
                    $(this).find('.tabs__readmore').trigger('click');
                }
            });
            $('.tabs__dropdown-activer').find('p').html($this.find('a').text());
            $('.tabs__tabscontainer li').slideToggle(0);
            $('.tabs__dropdown-activer').find('span').removeClass('arrow-down');  
               
        });
    };

    var openWithHash = function(hashUrl) {
        if(hashUrl !== '#'){ //other tabs
            var hashesInURL = hashUrl.replace('#','').split('+');  //replacing # with + to avoid escaping issues
            var tabHash = '#'+hashesInURL[0];
            var logoHash = '#'+hashesInURL[1];
            $('.tabs__tabscontainer li').each(function(){
                $(this).removeClass('active');
                $(this).removeClass('selected');
            });
            $('.tab-pane').each(function(){
                $(this).removeClass('active');
            });
            var link="";
            $('.tabs__tabscontainer li').each(function(){
                link = $(this).find('a').attr('href');
                if(link === tabHash) {
                    $(this).addClass('active');
                    $(this).addClass('selected');
                    $('.tabs__dropdown-activer').find('p').html($(this).find('a').text());   
                } 
            });
            $('.tab-pane').each(function(){
                link = $(this).attr('id');
                if('#'+link === tabHash) {
                    $(this).addClass('active');
                    $('.tabs__contentcontainer').removeClass('last');
                    $(this).find('.tabs__contentcontainer').addClass('last');
                }
            });

            $(logoHash).trigger('click');
            scrollToID(tabHash.replace('#',''));

        } else { 

        }
    };

    var bindEvent = function (el, eventName, eventHandler) {
        if (el.addEventListener){
            el.addEventListener(eventName, eventHandler, false); 
        } else if (el.attachEvent){
            el.attachEvent('on'+eventName, eventHandler);
        }
    };

    var chooseLogo = function () {
        var isOneActive = false;
        $('.tabs__logo').click(function(){
            var id = $(this).attr('id'),
            $active = $('.tabs__logo_content').find('#'+id),
            $container = $(this).closest('.tabs__contentcontainer'),
            $plus = $container.find('.plus'),
            marginHeight = 0;  
            
            $('.tabs__logotherapy-container').hide();
                        
            if(!$active.hasClass('open')){
                $active.addClass('open');
                $container.find('.tabs__logo').addClass('lock');
                $(this).removeClass('lock');   
                
                // $('.tabs__logotherapy-container').hide();
                $active.slideToggle('fast',function(){
                    if($container.hasClass('open')){
                        if(isMobile()){
                            marginHeight = 75; 
                        }else{
                            marginHeight = 100; 
                        }
                        var curHeight = $container.height(), autoHeight = $container.css({height: 'auto'}).height()+marginHeight;
                        $container.height(curHeight).animate({height: autoHeight});
                    }
                });
                $(this).parents('.tab-pane').find('.tabs__readmore-text').addClass('open');
                $('.tabs__logotherapy-container').not($active).removeClass('open'); 
            }else{
                $('.tabs__logotherapy-container').removeClass('open');    
                $container.find('.tabs__logo').removeClass('lock');
            }

        });
    };


    var init = function() {
        console.log('https://www.biogen.com/etc/designs/dxp/corp/APP.tabs');
        bindEventsToUI();
    };

    /**
     * interfaces to public functions
     */
    return {
        init: init
    };

}());

'use strict';

var APP = window.APP = window.APP || {};

APP.tabsLanding = (function(){

    var bindEventsToUI = function() {
        openDropdown();
        var hash = window.location.hash;
        if(hash !== ''){
            openWithHash(hash);
        }
        bindEvent(window,'hashchange',function(){
            var hash = window.location.hash;
            if(hash !== ''){ //ignore empty hash IE
                openWithHash(hash);   
            }
               
        });
    };

    var openDropdown = function() {
        $('.tabs-landing__dropdown-activer').click(function(){
            $('.tabs-landing__tab').slideToggle();
            if($(this).find('span').hasClass('arrow-down')){
                $(this).find('span').removeClass('arrow-down');
            }else {
                $(this).find('span').addClass('arrow-down');
            }
        });

        $('.tabs-landing__subtab').click(function(e) {
            $('.tabs-landing__tab').slideToggle(0);
            $('.tabs-landing__dropdown-activer').find('span').removeClass('arrow-down');
            var windowWidth = $(window).width();
            if(windowWidth <=640) {//mobile
            }else if (windowWidth <= 1024) {//tablet
                $("html, body").animate({scrollTop: $(this).offset().top}, 500);
            }else {//desktop
                e.preventDefault();
                $("html, body").animate({scrollTop: $(this).offset().top-80}, 500);   
                var linkHash = $(this).find('a').attr('href').split('#')[1];
                openWithHash('#'+linkHash);
                //Change the url using HTML5 history API or location hash for older browser
                if(history.pushState) {
                    history.pushState(null, 'Tabbed Landing Page', '#'+linkHash);
                }else {
                    location.hash = '#'+linkHash;
                }
            }
            

            
        });
    };


    var bindEvent = function (el, eventName, eventHandler) {
        if (el.addEventListener){
            el.addEventListener(eventName, eventHandler, false); 
        } else if (el.attachEvent){
            el.attachEvent('on'+eventName, eventHandler);
        }
    };

    var openWithHash = function(hashUrl) {
        if(hashUrl !== '#'){//ignore empty hash other browsers.
            var link = '', idScroll= '';
            $('.tabs-landing__subtab').removeClass('active');
            $('.tabs-landing__subtab').each(function(){
                link = $(this).attr('id');
                if('#'+link === hashUrl) {
                    $(this).addClass('active');
                    
                }
            });
            $('.tabs-landing__subtabs-content').hide();
            $('.tabs-landing__subtabs-content').removeClass('active');
            $('.tabs-landing__subtabs-content').each(function(){
                link = $(this).attr('id');
                if('#'+link === hashUrl){
                    $(this).addClass('active');
                    $(this).delay(500).fadeIn();
                }
            });
            
        }
    };

    var init = function() {
        console.log('APP.tabsLanding');
        bindEventsToUI();
    };

    return {
        init: init
    };

}());

/*global enquire */

'use strict';

var APP = window.APP = window.APP || {};

APP.genericWidget = (function(){

    var $html = $('html');
	var $genericWidgetTitle = $('.execute__title--compress');
    var $genericWidgetText = $('.execute__text--compress');
    var title = $genericWidgetTitle.text();
    var text = $genericWidgetText.text();
    var delegateIE8 = false;
    
    
    var bindEventsToUI = function(type) {
        splitString(title, 160, $genericWidgetTitle, false);
        splitString(text, 130, $genericWidgetText, true);
        $genericWidgetTitle.fadeIn();

        if ($html.hasClass('lt-ie9')){
            delegateIE8 = true;
        }
        
        resizeFunction(type, delegateIE8);
        

    };

    var splitString = function(str, maxSize, content, ellipsed) {
        var mStr = $.trim(str);
        var strLength = mStr.length;

        if(strLength >= maxSize) {
            var strArray = mStr.split(' ');
            var index = 0;
            var newStr = '';
            var stop = false;
            while(index <= strArray.length && stop === false ){
            
                if(newStr.length + strArray[index].length > maxSize) {
                    stop = true;
                }
                else{
                    newStr=newStr+strArray[index]+' ';
                }
                index++;
            }
            if(ellipsed){
                newStr=newStr+'...';
            }
            content.text(newStr);
        }

    };

    var maxSize = function(str, maxSize) {
        var mStr = $.trim(str);
        var strLength = mStr.length;

        if(strLength >= maxSize) {
            return true;
        }

        return false;
    };

    var noResizeFunction = function () {
        if(!maxSize(title,120)){
            $genericWidgetText.fadeIn();
        }
    };

    var resizeFunction = function(type, delegateIE8) {
        enquire.register('(min-width:767px)', {
            //Desktop
            match : function() {
                
                if(!maxSize(title,120)){
                    $genericWidgetText.fadeIn();
                }
            }

        },delegateIE8);

        enquire.register('(max-width:768px)', {
            match : function() {
                if(type==='news'){
                    $genericWidgetText.hide();
                }
                else if(type==='stories'){
                    $genericWidgetText.fadeIn();
                }
            },

            unmatch : function() {
                if(maxSize(title,120)){
                    $genericWidgetText.hide();
                }
            }
        });
    };

    var init = function(type) {
        bindEventsToUI(type);
    };

    return {
        init: init
    };

}());
/*global enquire */

'use strict';

var APP = window.APP = window.APP || {};

APP.filterDd = (function(){

    var initDynamicSelect = function() {

        var filterWidget = '.filter-dd',
            filterTrigger = '.filter-dd__trigger',
            filterTriggerIcon = '.filter-dd__trigger__icon',
            filterItemList = '.filter-dd__list',
            optionItem = '.filter-dd__option',
            optionActiveClass = 'filter-dd__option--active',
            $filterLabels = $('.filter-by-title, .filter-dd__trigger__text');

        // hide dropdown
        $(filterItemList).hide();

        // reveal dropdown when trigger is clicked
        $(filterTrigger).on('click', function(e) {
            e.stopPropagation();
            
            // if the menu was already open, close it and toggle the arrow
            if( $(this).siblings(filterItemList).is(':visible') ) {
                $(this).find(filterTriggerIcon).removeClass('bio-icon-arrow-thin-up').addClass('bio-icon-arrow-thin-down');
                $(this).siblings(filterItemList).slideUp();
            } else {
                // otherwise, open the menu and toggle the arrow
                $(this).find(filterTriggerIcon).addClass('bio-icon-arrow-thin-up').removeClass('bio-icon-arrow-thin-down');
                $(this).siblings(filterItemList).slideDown();
            }
        });

        // If this is an AJAX filter rather than simple page links, do the following:
        if('.filter-dd--ajax'.length > 0) {

            var id = 'all';

            var url = document.URL.split("/").pop();
            var urlSelectors = url.split(".");
            if(urlSelectors.length > 2){
                id = "." + urlSelectors[urlSelectors.length - 2];
            }

            var $container = $('#filteredList');

            if(!$container.mixItUp('isLoaded')){
                $container.mixItUp({
                    callbacks:{
                        onMixLoad:function(state){
                            $("img.lazy.profile-list-item__img").lazyload({
                                effect : "fadeIn"
                            });
                        },
                        onMixEnd:function(state){
                            var filter = state.activeFilter.replace('.','');

                            if(filter === 'mix'){
                                filter = 'all';
                            }

                            $("img.lazy.profile-list-item__img").trigger("scroll");

                            if (track && typeof CQ_Analytics != 'undefined' && CQ_Analytics.Sitecatalyst && populate) {
                                var analytics = populateAnalytics('filterView',populate.componentPath,null,null,null,null,null);
                                analytics.values['filters'] = filter;
                                CQ_Analytics.record(analytics);
                            }

                            track = true;
                        }
                    },
                    load:{
                        filter:id
                    }
                });
            } 

            $(optionItem + ' a').on('click', function() {
                var newText = $(this).text();

                // Set active class on clicked item
                $(optionItem).removeClass(optionActiveClass);
                $(this).parent(optionItem).addClass(optionActiveClass);

                // Reset page title to filter name
                $filterLabels.text(newText);

                // Close list of filters once one is selected
                $(this).parents(filterWidget).find(filterItemList).delay(100).slideUp();
                $(this).parents(filterWidget).find(filterTriggerIcon).addClass('bio-icon-arrow-thin-down').removeClass('bio-icon-arrow-thin-up');
            });
        }
    };

    var init = function() {
        console.log('APP.filterDd');
        initDynamicSelect();
    };

    /**
     * interfaces to public functions
     */
    return {
        init: init
    };

}());

'use strict';

var APP = window.APP = window.APP || {};

APP.cardWindowshade = (function(){

    var bindEventsToUI = function() {

        var bg = '.card-windowshade__item__bg',
            contents = '.card-windowshade__item__content';

        var wsClose = function(ws) {
            ws.removeClass('card-windowshade__item--active');
            ws.find(contents).slideUp();
            ws.find(bg).fadeOut();
        };

        var wsOpen = function(ws) {
            ws.addClass('card-windowshade__item--active');
            ws.find(contents).slideDown('slow');
            ws.find(bg).fadeIn();
        };

        $(document).on('ready', function() {
            wsOpen( $('.card-windowshade__item--active') );
        });

        $('.card-windowshade__item__link').on('click', function(e) {
            e.preventDefault();
            console.log('click');
            var $this = $(this).closest('.card-windowshade__item'),
                $notThis = $('.card-windowshade__item__link').not(this).closest('.card-windowshade__item');


            if (typeof CQ_Analytics != 'undefined' && CQ_Analytics.Sitecatalyst && populate && !$this.hasClass('card-windowshade__item--active')) {

                var element = $this.find('.card-windowshade__item__heading').text();
                var analytics = populateAnalytics('openAccordian',populate.componentPath,null,element,null,null,null);
                
                CQ_Analytics.record(analytics);
            }

            wsClose($notThis);
            wsOpen($this);



        });
    };

    var init = function() {
        console.log('APP.cardWindowshade');
        bindEventsToUI();
    };

    /**
     * interfaces to public functions
     */
    return {
        init: init
    };

}());

'use strict';

var APP = window.APP = window.APP || {};

APP.cardThreeCol = (function(){

    var bindEventsToUI = function() {
        $('.card-three-col__item-wrapper').slick({
            dots: true,
            infinite: false,
            speed: 300,
            slidesToShow: 3,
            slidesToScroll: 3,
            arrows: false,
            responsive: [
                {
                    breakpoint: 1023,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    };

    var init = function() {
        console.log('APP.cardThreeCol');
        bindEventsToUI();
    };

    /**
     * interfaces to public functions
     */
    return {
        init: init
    };

}());

/*global enquire */

'use strict';

var APP = window.APP = window.APP || {};

APP.cardTherapy = (function(){

    var $body = $('body');
    var $html = $('html');
    var windowshadesContainer = '.card-therapy__windowshade';
    var $dropDownSelect = $('.card-therapy__dropdown__select');
    var $dropDownName = $('.card-therapy__dropdown__name');
    var $dropDownIcon = $('.card-therapy__dropdown__icon');
    var $dropDownList = $('.card-therapy__dropdown__list');
    var $dropDownOptions = $('.card-therapy__dropdown__item__link');
    var $bbCards = $('.card-therapy__therapies-content__item');
    var delegateIE8 = false;
    
    var bindEventsToUI = function() {

        if ($html.hasClass('lt-ie9')){
            delegateIE8 = true;
        }
        resizeFunction(delegateIE8);

        //windowshades for mobile
        APP.windowshades.init(windowshadesContainer);
        
        //in Safari, this will keep the "All Approved Therapies" drop down 
        //in the hover state while the mouse is over the drop down list
        $dropDownList.hover(function() {
            $dropDownSelect.toggleClass("hovered");
        });
    };

    var gridContainer = function() {
        //open close drop down
        $body.on('click',function(e){
            
            if(e.target.className.search(/card-therapy__dropdown/i)>=0){
                e.preventDefault();
                $dropDownList.slideToggle(300);
                if($dropDownIcon.hasClass('arrow-down')){
                    console.log('close');
                    //$('html').click();
                    $dropDownIcon.removeClass('arrow-down');
                }else{
                    $dropDownIcon.addClass('arrow-down');
                }
            }
            else{
                $dropDownList.slideUp(300);
                $dropDownIcon.removeClass('arrow-down');
            }
        });

        //Dropdown click event
        $dropDownOptions.on('click', function(e){
            e.preventDefault();
            var $this = $(this);
            var currentOption = $this.text();
            var newData = $this.attr('data-filter');
            var currentData = $dropDownSelect.attr('data-filter');
    
            //Update options
            $this.text($dropDownName.text());
            $dropDownName.text(currentOption);
    
            $this.attr('data-filter', currentData);
            $dropDownSelect.attr('data-filter', newData);
            $this.removeClass('active');
    
        });
    };

    var resizeFunction = function(delegateIE8) {

        enquire.register('(min-width:768px)', {
            //Desktop
            match : function() {
                var $container = $('#mixItupContainer');
                if(!$container.mixItUp('isLoaded')){
                    $container.mixItUp({
                        callbacks:{
                            onMixEnd:function(state){
                                var filter = state.activeFilter.replace('.','');

                                if(filter === 'mix'){
                                    filter = 'all';
                                }

                                if (track && typeof CQ_Analytics != 'undefined' && CQ_Analytics.Sitecatalyst && populate) {
                                    var analytics = populateAnalytics('filterView',populate.componentPath,null,null,null,null,null);
                                    analytics.values['filters'] = filter;

                                    var name = $('.card-therapy').attr('data-card-name');
                                    var pos = $('.card-therapy').attr('data-card-pos');

                                    analytics.values['cards'] = name + ":" + pos;

                                    CQ_Analytics.record(analytics);
                                }

                                track = true;
                            }
                        }
                    });
                    gridContainer();
                }
            }

        },delegateIE8);
    };

    var init = function() {
        bindEventsToUI();
        
    };

    return {
        init: init
    };

}());

'use strict';

var APP = window.APP = window.APP || {};

APP.cardTabs = (function(){

    var bindEventsToUI = function() {
        dropdownTabsFunctionality();
        openBox();
    };

    var dropdownTabsFunctionality = function() {
        if(!isMobile()){
            var $container, id='';
            $.each($('.card-tabs__tab-panel'), function(index, val) {
                if($(this).hasClass('active')){
                    $container = $(this);
                }   
            });
            if($container){
                id = $container.attr('id');
                $('.card-tabs__scrollbarcontainer').find('#'+id).closest('.card-tabs__scrollactiver').tinyscrollbar({ thumbSize: 76});    
            }   
        }
        $('.card-tabs__mobile-activer').click(function(){
            $('.card-tabs__tabs li').slideToggle();
            $(this).removeClass('active');
        });
        $('.card-tabs__tabcontent').click(function(){
            $('.card-tabs__tabcontent ').removeClass('active');
            var $this = $(this),
            id = $this.parent().attr('id');
            $this.addClass('active');
            $('.card-tabs__tab-panel').removeClass('active');
            $('.card-tabs__infobar').removeClass('active');
            $('.card-tabs__scrollbar').removeClass('active');
            $('.card-tabs__readmoretext').removeClass('active');
            $('.card-tabs__scrollactiver').removeClass('active');
            $('.card-tabs__contenttabs').find('#'+id).addClass('active');
            $('.card-tabs__infocontainer').find('#'+id).addClass('active');
            $('.card-tabs__scrollbarcontainer').find('#'+id).addClass('active');
            $('.card-tabs__readmorecontainerstatic').find('#'+id).addClass('active');
            $.each($('.card-tabs__tab-panel'), function(index, val) {
                if($(this).hasClass('open')){
                    $('.card-tabs__readmorecontainerstatic').find('#'+id).trigger('click');   
                }  
            });
            $('.card-tabs__mobile-activer').find('p').html($(this).text());
            $('.card-tabs__tabs li').slideToggle(0);
            $('.card-tabs__mobile-activer').addClass('active');
            if(!isMobile()){
                $('.card-tabs__scrollbarcontainer').find('#'+id).closest('.card-tabs__scrollactiver').tinyscrollbar({ thumbSize: 76 });
                $('.card-tabs__scrollactiver').each(function(i,t){
                    var $elementActive = $(this).data("plugin_tinyscrollbar");
                    if($elementActive){
                        $elementActive.update();    
                    }
                       
                });    
            }
        });
    };

    var isMobile = function(){
        var isMobile = false,
        windowWidth = $(window).width();
        if (windowWidth < 1025) {
            isMobile = true;
        }
        return isMobile;
    };

    var openBox = function () {
        $('.card-tabs__readmoretext').click(function(){
            var $container, heightToClose = 0;
            $.each($('.card-tabs__tab-panel'), function(index, val) {
                if($(this).hasClass('active')){
                    $container = $(this);
                }   
            });
            if(isMobile()){
                heightToClose = 200;
            }else{
                heightToClose = 300;
            }
            if($(this).find('.card-tabs__readmorearrowup').hasClass('hide')){//box is closed
                $(this).find('.card-tabs__readmorearrowup').removeClass('hide');
                $(this).find('.card-tabs__readmorearrowdown').addClass('hide');
                $container.addClass('open');
                var curHeight = $container.height(), autoHeight = $container.find('.card-tabs__contentbody').height();
                $container.height(curHeight).animate({height: autoHeight});
            } else {//box is open
                $(this).find('.card-tabs__readmorearrowup').addClass('hide');
                $(this).find('.card-tabs__readmorearrowdown').removeClass('hide');
                $container.animate({height: heightToClose+'px'});
                $container.removeClass('open');
            }
        });

        $(window).resize(function(){
            var $container, heightToClose = 0;
            $.each($('.card-tabs__tab-panel'), function(index, val) {
                if($(this).hasClass('open')){
                    $container = $(this);
                }

                if($container){
                    if(isMobile()){// to mobile
                        heightToClose = 200;
                        if($container.hasClass('open') && $container.hasClass('active')){// is open
                            var curHeight = $container.height(), autoHeight = $container.find('.card-tabs__contentbody').show().height();
                            $container.height(curHeight).animate({height: autoHeight},function(){
                                console.log('after delay');
                            });
                        }else{// is closed
                            $container.animate({height: heightToClose+'px'});
                        }
                    }else{// to desktop
                        heightToClose = 300;
                        $container.animate({height: heightToClose+'px'}, 0, function(){
                            var $elementActive, id='';
                            if(!isMobile()){
                                $('.card-tabs__scrollactiver').each(function(i,t){
                                    $elementActive = $(this).data("plugin_tinyscrollbar");
                                    if($elementActive){
                                        $elementActive.update();   
                                    }
                                       
                                });   
                            }   
                        });   
                    }
                        
                }

            }); 
 
        });
    };

    var init = function() {
        console.log('APP.cardTabs');
        bindEventsToUI();
    };

    /**
     * interfaces to public functions
     */
    return {
        init: init
    };

}());

'use strict';

var APP = window.APP = window.APP || {};

APP.cardSocial = (function(){

    var bindEventsToUI = function() {
        $('.card-social__item-wrapper').slick({
            dots: true,
            infinite: false,
            speed: 300,
            slidesToShow: 3,
            slidesToScroll: 3,
            arrows: false,
            adaptiveHeight: true,
            responsive: [
                {
                    breakpoint: 1023,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    };

    var init = function() {
        console.log('APP.cardSocial');
        bindEventsToUI();
    };

    /**
     * interfaces to public functions
     */
    return {
        init: init
    };

}());

'use strict';
var jwplayer = jwplayer || function(){};
var APP = window.APP = window.APP || {};

APP.videoCard = (function(){

    var videoCard = {

        init: function(){
            this.navigation.init();
            this.customScrollbar();
            this.mobile.carouselFunctionality();
        },

        tools: {
            isMobile: function(){
                var isMobile = false,
                windowWidth = $(window).width();
                if (windowWidth < 760) {
                    isMobile = true;
                }
                return isMobile;
            }
        },

        mobile: {
            carousel: $('.video-card .stories-slider'),
            carouselFunctionality: function(){
                this.carousel.slick({
                    // slide: 'li',
                    speed: 400,
                    arrows: false,
                    dots: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: false
                });
            }
        },

        customScrollbar: function(){
            //console.log('test');
            //if(videoCard.tools.isMobile() === false){
            setTimeout(function(){
                $('.video-card__story-nav').tinyscrollbar({ thumbSize: 76, wheel: true, trackSize: 585 });
            },500);
            
            //}            

        },

        navigation: {

            navTrigger: $('.video-card__story-nav li'),
            videoTrigger: $('.video-card__story-text-cta a'),
            overlayCloseBtn: $('.video-card__modalCloseBtn'),
            overlay: $('.video-card__modal'),
            init: function(){
                var that = this;

                this.navTrigger.on("click", function(){
                    that.setSelectedItem(this);
                    that.changeSlide(this);
                });

                this.videoTrigger.on("click", function(e){
                    e.preventDefault();
                    if($(this).attr('class') === 'text'){
                        
                        //var parentId = $(this).parents('.video-card__story-text').attr('id');
                        that.showTextOverlay($(this));
                        
                    }else{
                        if(videoCard.tools.isMobile() === false){
                            that.showOverlay();
                            that.configurePlayer(this);
                        } else {
                            window.location.href = $(this).attr('data-videosrc');
                        }
                    }
                });

                this.overlayCloseBtn.on("click", function(){
                    that.closeOverlay();
                });

                $('.video-card__modalCloseBtn span').on('click', function(){
                    that.closeTextOverlay();
                });

                $('.video-card__modalBackBtn').on('click', function(){
                    that.closeTextOverlay();
                });
            },

            changeSlide: function(link){
                var slide = $(link).attr('data-slide');
                $('.video-card .stories-slider').slickGoTo(slide);
            },

            setSelectedItem: function(link){
                this.navTrigger.removeClass('selected');
                $(link).addClass('selected');
            },

            showOverlay: function(){
                this.overlay.fadeIn(500
                //     , function(){
                //     jwplayer('video-cardOverlay-video').play();
                // }
                );
            },

            configurePlayer: function(btn){

                var jwplayer = window.jwplayer || {};

                var videoURL = $(btn).attr('data-videosrc'),
                    videoPoster = $(btn).attr('data-poster');
                
                jwplayer('video-cardOverlay-video').setup({
                    image: videoPoster,
                    file:videoURL,
                    width: '100%',
                    aspectratio: '16:9',
                    primary: "flash",
                    autostart: true,
                    androidhls: true,
                    'modes': [
                        { type: 'html5' },
                        { type: 'flash', src: 'clientlib/js/vendor/jwplayer.flash.swf'/*tpa=https://www.biogen.com/etc/designs/dxp/corp/clientlib/js/vendor/jwplayer.flash.swf*/ }
                    ]
                });

                function fireEvent(eventName){
                    if (typeof CQ_Analytics != 'undefined' && CQ_Analytics.Sitecatalyst && populate) {
                        var data = populateAnalytics(eventName,populate.componentPath,null,null,null,null,null);
                        data.values['videoURL'] = videoURL;
                        data.values['videoName'] = videoURL.substring(videoURL.lastIndexOf("/") + 1);
                        CQ_Analytics.record(data);
                    }
                }

                jwplayer().onPlay(function(event){
                    fireEvent("videoPlay");
                });

                jwplayer().onComplete(function(event){
                    fireEvent("videoComplete");
                });
                
                var time = 0;
                jwplayer().onTime(function(event){
                    
                    if(time === 0){
                        fireEvent("videoStart");
                        time = Math.floor(event.duration/4);
                    } else if (event.position >= time && Math.floor(event.position / event.duration * 100) == 25){
                        fireEvent("video25");
                        time = Math.floor(event.duration/2);
                    } else if (event.position >= time && Math.floor(event.position / event.duration * 100) == 50){
                        fireEvent("video50");
                        time = Math.floor(event.duration * 3/4);
                    } else if (event.position >= time && Math.floor(event.position / event.duration * 100) == 75){
                        fireEvent("video75");
                        time = Math.floor(event.duration);
                    }
                });

            },

            disableBodyHeight: function(){
                
                $('body, html').css('overflow', 'hidden');
                

            },

            enableBodyHeight: function(){

                $('body, html').css('overflow', 'auto');

            },

            closeTextOverlay: function(){
                
                $('.video-card__modalTextWrapper').fadeOut(500, function(){
                    $('.video-card__modalText-content').hide();    
                });

                if(videoCard.tools.isMobile() === true){
                    this.enableBodyHeight();
                }
                
            },

            showTextOverlay: function(trigger){

                //var id = parentId.split('slide-')[1];
                var path = trigger.attr('data-src');

                var winHeight = $(window).height(),
                    windowWidth = $(window).width();
                
                if(videoCard.tools.isMobile() === true){
                    this.disableBodyHeight();
                    $('.video-card__modalText-content').css('height', winHeight-50);
                }else{
                    $('.video-card__modalTextWrapper').css('height', winHeight);
                }

                $.ajax({
                    url: path + ".display.html?wcmmode=disabled",
                    dataType: "html",
                    success: function(data){
                        $('#text-modal').html(data);
                        $('#text-modal').show();
                        $('.video-card__modalTextWrapper').fadeIn(500, function(){
                            if(videoCard.tools.isMobile() === false){

                                var box = $('#text-modal').find('.modal-text').data("plugin_tinyscrollbar");

                                if(box !== undefined){
                                    box.update();    
                                }

                                if(windowWidth < 1000){
                                    $('#text-modal').find('.modal-text').tinyscrollbar({ thumbSize: 56, wheel: true, trackSize: 330 });
                                }else{
                                    $('#text-modal').find('.modal-text').tinyscrollbar({ thumbSize: 56, wheel: true, trackSize: 390 });    
                                }
                                
                            }
                        });
                    }
                });
            },

            closeOverlay: function(){
                this.overlay.fadeOut('fast');
                jwplayer('video-cardOverlay-video').stop();
            }

        }

    };

    var init = function() {
        videoCard.init();
    };

    return {
        init: init
    };

}());

'use strict';

var APP = window.APP = window.APP || {};

APP.cardProfile = (function(){

    var activeScrollbar = function() {
        $('.card-profile__scroll-activer').tinyscrollbar({ thumbSize: 76, wheel: true, trackSize: 510 });
    };

    var activeImages = function() {
        $('.card-profile__image').click(function() {
            $(this).closest('.card-profile__images').find('.card-profile__image__overlay').removeClass('active');
            $(this).find('.card-profile__image__overlay').addClass('active');
            $(this).closest('.card-profile__content').find('.card-profile__info__background').removeClass('active');
            $(this).closest('.card-profile__content').find('.card-profile__info').find('#'+$(this).attr('id')).addClass('active');
        });
    };

    var updateScrollOnResize = function () {
        $(window).load(function(){
            $('.card-profile__info__background').each(function(){
                if($(this).hasClass('active')){
                    var $img = $(this).find('img');
                    activeScrollbar();
                }
                var $scLeadership = $('#leadership').data("plugin_tinyscrollbar");
                var $scScientist = $('#scientist').data("plugin_tinyscrollbar");
                if($scLeadership){
                    $scLeadership.update();   
                }
                if($scScientist){
                    $scScientist.update();
                }
            });
        });
        $(window).resize(function(){
            $('.card-profile__info__background').each(function(){
                if($(this).hasClass('active')){
                    var $img = $(this).find('img');
                    var heightImg = $img.height();
                    $('.card-profile__images-container').height(heightImg);

                }
            });
            var $scLeadership = $('#leadership').data("plugin_tinyscrollbar");
            var $scScientist = $('#scientist').data("plugin_tinyscrollbar");
            if($scLeadership){
                $scLeadership.update();   
            }
            if($scScientist){
                $scScientist.update();
            }
        });
        
    };

    var activeMixItup = function() {
        $('#card-profile__mixItupContainer').mixItUp();
        $('#card-profile__mixItupContainer').on('mixEnd', function(e, state){
            var $scScientist = $('#scientist').data("plugin_tinyscrollbar");
            if($scScientist){
                $scScientist.update();
            }
            if(state.totalShow <=12) {
                $('#scientist').find('.card-profile__scrollbar-container').hide();
            }else{
                $('#scientist').find('.card-profile__scrollbar-container').show();
            }
        });
    };

    var activeDropdown = function() {
        $('.card-profile__dropdown-activer').click(function() {
            if($('.card-profile__arrow').hasClass('down')){
                $('.card-profile__arrow').removeClass('down');
            }else{
                $('.card-profile__arrow').addClass('down');    
            } 
            $('.card-profile__categories').slideToggle();
        });

        $('.card-profile__category').click(function(e) {
            $('.card-profile__arrow').removeClass('down');
            $('.card-profile__dropdown-activer').find('p').html($(this).text());
            $('.card-profile__categories').slideToggle();
        });
    };

    var bindEventsToUI = function() {
        activeImages();
        updateScrollOnResize();
        activeMixItup();
        activeDropdown();
    };

    var init = function() {
        console.log('APP.cardProfile');
        bindEventsToUI();
    };

    /**
     * interfaces to public functions
     */
    return {
        init: init
    };

}());

'use strict';

var APP = window.APP = window.APP || {};

APP.home = (function(){

    var bindEventsToUI = function() {
        
    };

    var init = function() {
        //APP.cardTherapy.init();
        //APP.genericWidget.init('news');
        bindEventsToUI();
    };

    /**
     * interfaces to public functions
     */
    return {
        init: init
    };

}());
'use strict';

var APP = window.APP = window.APP || {};

APP.styleguide = (function(){

    var bindEventsToUI = function() {

    };

    var init = function() {
        console.log('APP.styleguide');
        bindEventsToUI();
    };

    /**
     * interfaces to public functions
     */
    return {
        init: init
    };

}());

'use strict';

var APP = window.APP = window.APP || {};

APP.index = (function(){

    var bindEventsToUI = function() {

    };

    var init = function() {
        console.log('APP.index');
        bindEventsToUI();
    };

    /**
     * interfaces to public functions
     */
    return {
        init: init
    };

}());

'use strict';

var APP = window.APP = window.APP || {};

APP.investors = (function(){

    var bindEventsToUI = function() {

    };

    var init = function() {
        APP.genericWidget.init('news');
        bindEventsToUI();
    };

    /**
     * interfaces to public functions
     */
    return {
        init: init
    };

}());

'use strict';

var APP = window.APP = window.APP || {};

APP.patients = (function(){

    var bindEventsToUI = function() {

    };

    var init = function() {
        // APP.cardTherapy.init();
        // APP.genericWidget.init('news');
        bindEventsToUI();
    };

    /**
     * interfaces to public functions
     */
    return {
        init: init
    };

}());

'use strict';

var APP = window.APP = window.APP || {};

APP.contact = (function(){

    var contactPage = {

        init: function(){
            this.dropDown.init();
        },

        dropDown:{

            dropDownTrigger: $('.contact .contact__filters-trigger'),
            dropdown: $('.contact .contact__filtersDropdown'),
            dropdownItems: $('.contact .contact__filtersDropdown-item'),
            infoAreas: $('.contact .area'),

            init: function(){
                this.eventHandlers();
            },

            slideDropdown: function(){
                this.dropdown.slideToggle();
            },

            updateSelectedItem: function(btn){
                var area = $(btn).attr('data-area');

                this.dropdownItems.removeClass('selected');
                this.infoAreas.removeClass('selected');

                $('.'+area).addClass('selected');
                $(btn).addClass('selected');
            },

            updateTriggerItem: function(btn){
                var btnText = $(btn).html();
                this.dropDownTrigger.find('.text').html(btnText);
                this.dropDownTrigger.find('.bio-icon-arrow-thin-down').removeClass('arrow-down');
            },

            filterArea: function(btn){
                var area = $(btn).attr('data-area');
                this.infoAreas.hide();
                $('.'+ area).fadeIn();
            },

            eventHandlers: function(){
                
                var that = this;

                this.dropDownTrigger.on('click', function(){
                    that.slideDropdown();
                    console.log($(this));
                    if($(this).find('.bio-icon-arrow-thin-down').hasClass('arrow-down')){
                        $(this).find('.bio-icon-arrow-thin-down').removeClass('arrow-down');   
                    }else{
                        $(this).find('.bio-icon-arrow-thin-down').addClass('arrow-down');
                    }
                });

                this.dropdownItems.on('click', function(){
                    that.slideDropdown(); 
                    that.updateSelectedItem(this);
                    that.updateTriggerItem(this);
                    that.filterArea(this);
                });
            }
        }
    };

    var init = function() {
        contactPage.init();
    };

    /**
     * interfaces to public functions
     */
    return {
        init: init
    };

}());

/* ========================================================================
 * Bootstrap: modal.js v3.2.0
 * http://getbootstrap.com/javascript/#modals
 * ========================================================================
 * Copyright 2011-2014 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // MODAL CLASS DEFINITION
  // ======================

  var Modal = function (element, options) {
    this.options        = options
    this.$body          = $(document.body)
    this.$element       = $(element)
    this.$backdrop      =
    this.isShown        = null
    this.scrollbarWidth = 0

    if (this.options.remote) {
      this.$element
        .find('.modal-content')
        .load(this.options.remote, $.proxy(function () {
          this.$element.trigger('loaded.bs.modal')
        }, this))
    }
  }

  Modal.VERSION  = '3.2.0'

  Modal.DEFAULTS = {
    backdrop: true,
    keyboard: true,
    show: true
  }

  Modal.prototype.toggle = function (_relatedTarget) {
    return this.isShown ? this.hide() : this.show(_relatedTarget)
  }

  Modal.prototype.show = function (_relatedTarget) {
    var that = this
    var e    = $.Event('show.bs.modal', { relatedTarget: _relatedTarget })

    this.$element.trigger(e)

    if (this.isShown || e.isDefaultPrevented()) return

    this.isShown = true

    this.checkScrollbar()
    this.$body.addClass('modal-open')

    this.setScrollbar()
    this.escape()

    this.$element.on('click.dismiss.bs.modal', '[data-dismiss="modal"]', $.proxy(this.hide, this))

    this.backdrop(function () {
      var transition = $.support.transition && that.$element.hasClass('fade')

      if (!that.$element.parent().length) {
        that.$element.appendTo(that.$body) // don't move modals dom position
      }

      that.$element
        .show()
        .scrollTop(0)

      if (transition) {
        that.$element[0].offsetWidth // force reflow
      }

      that.$element
        .addClass('in')
        .attr('aria-hidden', false)

      that.enforceFocus()

      var e = $.Event('shown.bs.modal', { relatedTarget: _relatedTarget })

      transition ?
        that.$element.find('.modal-dialog') // wait for modal to slide in
          .one('bsTransitionEnd', function () {
            that.$element.trigger('focus').trigger(e)
          })
          .emulateTransitionEnd(300) :
        that.$element.trigger('focus').trigger(e)
    })
  }

  Modal.prototype.hide = function (e) {
    if (e) e.preventDefault()

    e = $.Event('hide.bs.modal')

    this.$element.trigger(e)

    if (!this.isShown || e.isDefaultPrevented()) return

    this.isShown = false

    this.$body.removeClass('modal-open')

    this.resetScrollbar()
    this.escape()

    $(document).off('focusin.bs.modal')

    this.$element
      .removeClass('in')
      .attr('aria-hidden', true)
      .off('click.dismiss.bs.modal')

    $.support.transition && this.$element.hasClass('fade') ?
      this.$element
        .one('bsTransitionEnd', $.proxy(this.hideModal, this))
        .emulateTransitionEnd(300) :
      this.hideModal()
  }

  Modal.prototype.enforceFocus = function () {
    $(document)
      .off('focusin.bs.modal') // guard against infinite focus loop
      .on('focusin.bs.modal', $.proxy(function (e) {
        if (this.$element[0] !== e.target && !this.$element.has(e.target).length) {
          this.$element.trigger('focus')
        }
      }, this))
  }

  Modal.prototype.escape = function () {
    if (this.isShown && this.options.keyboard) {
      this.$element.on('keyup.dismiss.bs.modal', $.proxy(function (e) {
        e.which == 27 && this.hide()
      }, this))
    } else if (!this.isShown) {
      this.$element.off('keyup.dismiss.bs.modal')
    }
  }

  Modal.prototype.hideModal = function () {
    var that = this
    this.$element.hide()
    this.backdrop(function () {
      that.$element.trigger('hidden.bs.modal')
    })
  }

  Modal.prototype.removeBackdrop = function () {
    this.$backdrop && this.$backdrop.remove()
    this.$backdrop = null
  }

  Modal.prototype.backdrop = function (callback) {
    var that = this
    var animate = this.$element.hasClass('fade') ? 'fade' : ''

    if (this.isShown && this.options.backdrop) {
      var doAnimate = $.support.transition && animate

      this.$backdrop = $('<div class="modal-backdrop ' + animate + '" />')
        .appendTo(this.$body)

      this.$element.on('click.dismiss.bs.modal', $.proxy(function (e) {
        if (e.target !== e.currentTarget) return
        this.options.backdrop == 'static'
          ? this.$element[0].focus.call(this.$element[0])
          : this.hide.call(this)
      }, this))

      if (doAnimate) this.$backdrop[0].offsetWidth // force reflow

      this.$backdrop.addClass('in')

      if (!callback) return

      doAnimate ?
        this.$backdrop
          .one('bsTransitionEnd', callback)
          .emulateTransitionEnd(150) :
        callback()

    } else if (!this.isShown && this.$backdrop) {
      this.$backdrop.removeClass('in')

      var callbackRemove = function () {
        that.removeBackdrop()
        callback && callback()
      }
      $.support.transition && this.$element.hasClass('fade') ?
        this.$backdrop
          .one('bsTransitionEnd', callbackRemove)
          .emulateTransitionEnd(150) :
        callbackRemove()

    } else if (callback) {
      callback()
    }
  }

  Modal.prototype.checkScrollbar = function () {
    if (document.body.clientWidth >= window.innerWidth) return
    this.scrollbarWidth = this.scrollbarWidth || this.measureScrollbar()
  }

  Modal.prototype.setScrollbar = function () {
    var bodyPad = parseInt((this.$body.css('padding-right') || 0), 10)
    if (this.scrollbarWidth) this.$body.css('padding-right', bodyPad + this.scrollbarWidth)
  }

  Modal.prototype.resetScrollbar = function () {
    this.$body.css('padding-right', '')
  }

  Modal.prototype.measureScrollbar = function () { // thx walsh
    var scrollDiv = document.createElement('div')
    scrollDiv.className = 'modal-scrollbar-measure'
    this.$body.append(scrollDiv)
    var scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth
    this.$body[0].removeChild(scrollDiv)
    return scrollbarWidth
  }


  // MODAL PLUGIN DEFINITION
  // =======================

  function Plugin(option, _relatedTarget) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.modal')
      var options = $.extend({}, Modal.DEFAULTS, $this.data(), typeof option == 'object' && option)

      if (!data) $this.data('bs.modal', (data = new Modal(this, options)))
      if (typeof option == 'string') data[option](_relatedTarget)
      else if (options.show) data.show(_relatedTarget)
    })
  }

  var old = $.fn.modal

  $.fn.modal             = Plugin
  $.fn.modal.Constructor = Modal


  // MODAL NO CONFLICT
  // =================

  $.fn.modal.noConflict = function () {
    $.fn.modal = old
    return this
  }


  // MODAL DATA-API
  // ==============

  $(document).on('click.bs.modal.data-api', '[data-toggle="modal"]', function (e) {
    var $this   = $(this)
    var href    = $this.attr('href')
    var $target = $($this.attr('data-target') || (href && href.replace(/.*(?=#[^\s]+$)/, ''))) // strip for ie7
    var option  = $target.data('bs.modal') ? 'toggle' : $.extend({ remote: !/#/.test(href) && href }, $target.data(), $this.data())

    if ($this.is('a')) e.preventDefault()

    $target.one('show.bs.modal', function (showEvent) {
      if (showEvent.isDefaultPrevented()) return // only register focus restorer if modal will actually get shown
      $target.one('hidden.bs.modal', function () {
        $this.is(':visible') && $this.trigger('focus')
      })
    })
    Plugin.call($target, option, this)
  })

}(jQuery);

'use strict';

var APP = window.APP = window.APP || {};


APP.exitModal= (function(){

    var modal = {

        externalLinks : $('a[href^="http:"], a[href^="https:"]').not('[href*="'+document.domain+'"]').not(".cancel"),
        continueButton : $("#continue-button"),
        leaveURL : "",
        modalText: $('#exit-site-modal-text'),
        altModalText: $('#exit-site-modal-alt-text'),
        init: function() {

            modal.externalLinks.click(function(e) {
                
                if($(this).hasClass('js-videoOverlayTrigger')){
                    return true;
                }
                modal.leaveURL = $(this).attr("href");

                if( whitelistExempt(modal.leaveURL) || !whitelisted(modal.leaveURL)){
                    e.preventDefault();

                    $('#exit-site').modal('show');

                    if(listed(modal.leaveURL,altlist)){
                        modal.modalText.hide();
                        modal.altModalText.show();
                    } else {
                        modal.modalText.show();
                        modal.altModalText.hide();
                    }

                    
                    return false;
                } else {
                    $(this).attr('target','_blank');
                    return true;
                }
            });

            modal.continueButton.click(function() {
                $('#exit-site').modal('hide');
                window.open(modal.leaveURL, '_blank');
            });
        }
    }


    var init = function(){
        modal.init();
        modal.modalText.hide();
        modal.altModalText.hide();
    }

    var exitURL = function(){
        return modal.leaveURL;
    }

    return {
        init: init,
        exitURL: exitURL
    };

}());
