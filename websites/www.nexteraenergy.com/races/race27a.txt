<script id = "race27a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race27={};
	myVars.races.race27.varName="Window[26].kp";
	myVars.races.race27.varType="@varType@";
	myVars.races.race27.repairType = "@RepairType";
	myVars.races.race27.event1={};
	myVars.races.race27.event2={};
	myVars.races.race27.event1.id = "Lu_Id_input_5";
	myVars.races.race27.event1.type = "onkeydown";
	myVars.races.race27.event1.loc = "Lu_Id_input_5_LOC";
	myVars.races.race27.event1.isRead = "False";
	myVars.races.race27.event1.eventType = "@event1EventType@";
	myVars.races.race27.event2.id = "searchField";
	myVars.races.race27.event2.type = "onmousedown";
	myVars.races.race27.event2.loc = "searchField_LOC";
	myVars.races.race27.event2.isRead = "False";
	myVars.races.race27.event2.eventType = "@event2EventType@";
	myVars.races.race27.event1.executed= false;// true to disable, false to enable
	myVars.races.race27.event2.executed= false;// true to disable, false to enable
</script>

