<script id = "race6a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race6={};
	myVars.races.race6.varName="searchField__onmousedown";
	myVars.races.race6.varType="@varType@";
	myVars.races.race6.repairType = "@RepairType";
	myVars.races.race6.event1={};
	myVars.races.race6.event2={};
	myVars.races.race6.event1.id = "Lu_window";
	myVars.races.race6.event1.type = "onload";
	myVars.races.race6.event1.loc = "Lu_window_LOC";
	myVars.races.race6.event1.isRead = "False";
	myVars.races.race6.event1.eventType = "@event1EventType@";
	myVars.races.race6.event2.id = "searchField";
	myVars.races.race6.event2.type = "onmousedown";
	myVars.races.race6.event2.loc = "searchField_LOC";
	myVars.races.race6.event2.isRead = "True";
	myVars.races.race6.event2.eventType = "@event2EventType@";
	myVars.races.race6.event1.executed= false;// true to disable, false to enable
	myVars.races.race6.event2.executed= false;// true to disable, false to enable
</script>

