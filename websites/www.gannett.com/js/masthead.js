function accordionInit () {
							//closedWidth, openWidth, padding, timeOut to next frame, type.
	$(".home #accordion").accordion(65,600,3,5000, "image");
	$(".marketing #accordion").accordion(82,600,3,7000, "image");
	$(".who-we-are #accordion").accordion(82,600,3,5000, "image");
	$(".careers #accordion").accordion(82,600,3,5000, "text");
	$(".brands-smasher").smasher();
	$("#blackout").css({"opacity":0});
}
function shutterCurtain () {
	$("#blackout").css({"opacity":0,"top":"0"});
	$("#blackout").animate({"opacity": 0.7}, 200, "linear");
	if (accordionActivated == true) $("#accordion").pause();
	return void(0);
}
function revealCurtain () {
	$("#blackout").animate({"opacity": 0}, 200, "linear", function () {
		$("#blackout").css({"top":"-999em"});
	});
	if (accordionActivated == true) $("#accordion").resume();
	return void(0);
}

function gannettNavigation () {
	$("#menu").bind('mouseenter', function (e){
			   shutterCurtain();
	})
	.bind('mouseleave', function (e){
			   revealCurtain();
	});
}

$(document).ready(function () {
	accordionInit();
	$(".shutter").click(shutterCurtain);
	$(".unshutter").click(revealCurtain);
	
	gannettNavigation ();	
});