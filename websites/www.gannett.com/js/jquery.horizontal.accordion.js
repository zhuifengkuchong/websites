/**
* Horizontal Accordion
*/
var accordionActivated;

(function(jQuery){
	var activateAlias;
	var current = -1;
	var startAlias = function () {return void(0);};
	var endAlias = function () {return void(0);};
	
	jQuery.fn.extend({  
		 accordion: function(closedWidth,openWidth,paddingWidth,autoTimer,type) {
			accordionActivated = true;
			if ($.browser.msie && $.browser.version < 7) ieSix = true;
			else ieSix = false;
			return this.each(function() {
				var $ul = $(this);
				
				$.each($ul.find('li'), function(){
					$(this).mouseenter(function(e){
						clearInterval(timeHandler);
						activate(this);
						return void(0);
					});
					$(this).mouseleave(function(e){
						if (autoPlay == true) startTimer();
						return void(0);
					});
				});
				//number of child elements
				var numChildren = $ul.children("li").size();
				
				var activate = function (el,timeVar, killVar){
					//find this thing
					var thisIndex = $(el).index();
					if (killVar == true && autoPlay == true) {
						autoPlay = false;
						endTimer();
					}
					//holder for previous widths
					var positionTracker = 0;
					if (timeVar == null || timeVar == undefined) {
						timeVar = 200;
					}
					if (thisIndex != current) {
						$ul.children("li").each(function (index) {
							$(this).stop(true,false);
							$(this).find(".hider img").stop(true,false);
							$(this).find(".hider .blade-content").stop(true,false);
							$(this).find(".rollover").stop(true,false);
							$(this).find(".rollover").css({"opacity":0});
							if (ieSix == true) $(this).find(".hider").stop(true,false);
							if (thisIndex == index) {
								current = thisIndex;
								//.animate( properties, [ duration ], [ easing ], [ callback ] )
								$(this).animate({width:openWidth,left:positionTracker}, timeVar, "linear", function() {
									if (thisIndex > 0) $(".intro-image").hide();
								});
								positionTracker +=  openWidth + paddingWidth;
								if (type == "image") {
									$(this).find(".hider img").each(function() {
										$(this).animate({left:0}, timeVar, "linear") ;
									});
								}
								else {
									$(this).find(".hider .blade-content").each(function() {
										$(this).animate({left:0}, timeVar, "linear") ;
									});
								}
								if (ieSix == true) $(this).find(".hider").animate({width:openWidth-2}, timeVar, "linear");
								
								$(this).find(".rollover").delay(400).animate({"opacity":1},400);
							}
							else {						
								$(this).animate({width:closedWidth,left:positionTracker}, timeVar, "linear") ;
								positionTracker +=  closedWidth + paddingWidth;
								if (type == "image") {
									$(this).find(".hider img").each(function() {
										$(this).animate({left:"-"+$(this).attr("index")+"px"}, timeVar, "linear") ;
									});
								}
								else {
									$(this).find(".hider .blade-content").each(function() {
										$(this).animate({left:"-"+$(this).attr("index")+"px"}, timeVar, "linear") ;
									});
								}
								if (ieSix == true) $(this).find(".hider").animate({width:closedWidth-2}, timeVar, "linear");
							}
							
						});
					}
				}
				activateAlias = activate;
				
				//automagic
				var autoPlay = true;
				var timeHandler =0;
				function startTimer () {
					if (autoPlay == true) {
						timeHandler = setInterval(
						function() {
							if (current+1 == numChildren) {
								activate( $ul.find("li:eq("+(0)+")"), null, true);
							}
							else {
								activate( $ul.find("li:eq("+(current+1)+")"));
							}
						}, autoTimer);
					}
				}
				function endTimer () {
					clearInterval(timeHandler);
				}
				
				//pick the first -- animation time = 0 so it starts right away. Slide in the unit, then start counting.
				activate( $ul.find("li:eq(0)"), 0);
				$ul.css({left:-1000});
				$ul.animate({left:0},500);
				startTimer();
				startAlias = startTimer;
				endAlias = endTimer;
				
				//buttons
				$ul.find(".button").each(function (index) {
					$(this).click(function () {
						$(this).parent().find("p.toggle, span.toggle").each(function () {
							if ($(this).hasClass("hidden")) $(this).removeClass("hidden");
							else  $(this).addClass("hidden");
						});
					});
				});
				// END buttons
			});
		},
		pause:function () {
			endAlias();
		},
		resume:function () {
			startAlias();
		}
	}); 
})(jQuery);