
(function(jQuery){
	
	jQuery.fn.extend({  
		 smasher: function() {
			var current;
			
			if ($.browser.msie && $.browser.version < 7) ieSix = true;
			else ieSix = false;
			
			return this.each(function() {
				var $div = $(this);
				var $ul = $(this).find("ul");
				var $ol = $div.find("#masthead-overlay");
				
				$div.find("#navbar").css({"opacity":0});
				$div.find("#navbar").delay(100).animate({"opacity":0.9, "top":"291px"},300);
				$div.mouseenter(function() {
					$(this).find("#navbar").stop(true,false);
					$(this).find("#navbar").animate({"top":"326px"},150);
				});
				$div.mouseleave(function(){
					$(this).find("#navbar").stop(true,false);
					$(this).find("#navbar").animate({"top":"291px"},150);
				});
				
				var reversed = false;
				
				var numChildren = $ul.children("li").size();
				var delayincrement = 70;
				if (reversed) var delaying = 0;
				else var delaying = (delayincrement*numChildren);
				
				$ul.children("li").each(function (index) {
					$el = $(this);
					if (reversed) $(this).css({left:1000});
					$(this).delay(delaying).animate({left:0},300,null, function () {initTrigger(index)});
					if (reversed) delaying += delayincrement;
					else delaying -= delayincrement;
				});
				function initTrigger (index) {
					$ul.find("li:eq("+index+")").click(function(e){
						triggerPopup($(this));
					});
				}
				
				function triggerPopup ($el) {
					var thisIndex = $el.index();
					if (thisIndex != current) {
						if (thisIndex == numChildren-1) $ol.css({"width":"672px"});
						else $ol.css({"width":"667px"});
						current  = thisIndex;
						var direction = "";
						var positionObj = $el.position();
						var pxPosition = positionObj.left;
						
						if ($el.hasClass("right")) {
							pxPosition -= $ol.width() - $el.width();
							$ol.css({"left":pxPosition+"px"});
						}
						else if ($el.hasClass("left")) {
							$ol.css({"left":pxPosition+"px"});
						}
						else {
							positionObj = $ul.find("li:eq(2)").position();
							pxPosition = positionObj.left;
							$ol.css({"left":pxPosition+"px"});
						}
						
						var bgImage = $el.attr("data");
						var hiddenNode = $el.find('.hidden-content').clone();
						
						
						$ol.empty();
						$ol.css({"opacity":0,"background-image":"url("+ bgImage + ")", top:0});
						$ol.append(hiddenNode);
						if (ieSix == false) $ol.find("p").css({"opacity":0});
						$ol.delay(100).animate({"opacity":1},200);
						if (ieSix == false) $ol.find("p").delay(600).animate({"opacity":1},300);
					}
					else {
						$ol.css({top:0});
					}
					//close 
					$div.find(".close-btn").click(function(e){
						closePopup();
					});
				}
				function closePopup () {
					$ol.css({top:"-999em"});
				}
				
			});
		}
	}); 
})(jQuery);