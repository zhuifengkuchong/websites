













	qmQuotes = new Array();



		 	qmQuotes[0] = new Object();
		    qmQuotes[0].symbol='<div class="qmjssymbol">WPZ</div>';
		    qmQuotes[0].longName='<div class="qmjsname">Williams Partners L.P. Units</div>';
		    qmQuotes[0].shortName='<div class="qmjsname">WPZ</div>';

            
                qmQuotes[0].marketplace='<div class="qmjsleft">Marketplace</div><div class="qmjsright">N/A</div>';
                qmQuotes[0].exchange='<div class="qmjsleft">Exchange</div><div class="qmjsright">NYE</div>';
            

            qmQuotes[0].entitlement='<div class="qmjsleft">Entitlement</div><div class="qmjsright">DL</div>';
            qmQuotes[0].last='<div class="qmjsleft">Last</div><div class="qmjsright">52.8095</div>';
		    qmQuotes[0].change='<div class="qmjsleft">Change</div><div class="qmjsright">-0.2405</div>';
		    qmQuotes[0].changePercent='<div class="qmjsleft">% Change</div><div class="qmjsright">-0.45%</div>';
		    qmQuotes[0].bid='<div class="qmjsleft">Bid</div><div class="qmjsright">52.80</div>';
		    qmQuotes[0].bidSize='<div class="qmjsleft">Bid Size</div><div class="qmjsright">400</div>';
		    qmQuotes[0].ask='<div class="qmjsleft">Ask</div><div class="qmjsright">52.83</div>';
		    qmQuotes[0].askSize='<div class="qmjsleft">Ask Size</div><div class="qmjsright">500</div>';
		    qmQuotes[0].volume='<div class="qmjsleft">Volume</div><div class="qmjsright">1.11m</div>';
		    qmQuotes[0].high52Week='<div class="qmjsleft">52 Week High</div><div class="qmjsright">62.919186</div>';
		    qmQuotes[0].low52Week='<div class="qmjsleft">52 Week Low</div><div class="qmjsright">44.85076</div>';
		    qmQuotes[0].yield='<div class="qmjsleft">Yield</div><div class="qmjsright">6.409</div>';
		    qmQuotes[0].dividend='<div class="qmjsleft">Dividend</div><div class="qmjsright">3.40</div>';
		    qmQuotes[0].dividendDate='<div class="qmjsleft">Div. Date</div><div class="qmjsright">05/05/15</div>';
		    qmQuotes[0].eps='<div class="qmjsleft">E.P.S.</div><div class="qmjsright">0.23</div>';
		    qmQuotes[0].peRatio='<div class="qmjsleft">PE Ratio</div><div class="qmjsright">243.00</div>';
            qmQuotes[0].quoteTime='<div class="qmjsleft">Quote Time</div><div class="qmjsright">06/08/2015 12:57</div>';
            qmQuotes[0].lastTradeTime='<div class="qmjsleft">Last Trade Time</div><div class="qmjsright">06/08/2015 12:57</div>';
            qmQuotes[0].volumeActual='<div class="qmjsleft">Volume (Actual)</div><div class="qmjsright">1105397</div>';
            qmQuotes[0].lastTradeVolume='<div class="qmjsleft">Last Trade Volume</div><div class="qmjsright">2000</div>';
            qmQuotes[0].premarketQuoteTime='<div class="qmjsleft">Pre Market Quote Time</div><div class="qmjsright">N/A</div>';
            qmQuotes[0].premarketLast='<div class="qmjsleft">Pre Market Last</div><div class="qmjsright">N/A</div>';
            qmQuotes[0].premarketVolume='<div class="qmjsleft">Pre Market  Volume</div><div class="qmjsright">0</div>';
            qmQuotes[0].aftermarketQuoteTime='<div class="qmjsleft">After Market Quote Time</div><div class="qmjsright">N/A</div>';
            qmQuotes[0].aftermarketLast='<div class="qmjsleft">After Market Last</div><div class="qmjsright">N/A</div>';
            qmQuotes[0].aftermarketVolume='<div class="qmjsleft">After Market Volume</div><div class="qmjsright">0</div>';

		
			qmQuotes[0].tick='<div class="qmjsleft">Tick</div><div class="qmjsright">&ndash;</div>';
		
		    qmQuotes[0].open='<div class="qmjsleft">Open</div><div class="qmjsright">52.83</div>';
		    qmQuotes[0].high='<div class="qmjsleft">High</div><div class="qmjsright">53.41</div>';
		    qmQuotes[0].low='<div class="qmjsleft">Low</div><div class="qmjsright">52.585</div>';
		    qmQuotes[0].close='<div class="qmjsleft">Close</div><div class="qmjsright">53.05</div>';
		    qmQuotes[0].tradeVolume='<div class="qmjsleft">Trade Volume</div><div class="qmjsright">10.22k</div>';
		    qmQuotes[0].tradeVolumeActual='<div class="qmjsleft">Trade Volume (Actual)</div><div class="qmjsright">10,218</div>';
		    qmQuotes[0].avg10DayVolume='<div class="qmjsleft">Average Daily Volume (10 Days)</div><div class="qmjsright">2,074,066</div>';
		    qmQuotes[0].avg30DayVolume='<div class="qmjsleft">Average Daily Volume (30 Days)</div><div class="qmjsright">3,509,083</div>';
		    qmQuotes[0].shares='<div class="qmjsleft">Shares</div><div class="qmjsright">600,643,297</div>';
		    qmQuotes[0].marketCap='<div class="qmjsleft">Market Cap</div><div class="qmjsright">31.72b</div>';
		    
		    	
			    	qmQuotes[0].symbolStatusCode='<div class="qmjsleft">Status Code</div><div class="qmjsright">-</div>';
			    
			
	

		 	qmQuotes[1] = new Object();
		    qmQuotes[1].symbol='<div class="qmjssymbol">WMB</div>';
		    qmQuotes[1].longName='<div class="qmjsname">Williams Cos.</div>';
		    qmQuotes[1].shortName='<div class="qmjsname">WMB</div>';

            
                qmQuotes[1].marketplace='<div class="qmjsleft">Marketplace</div><div class="qmjsright">N/A</div>';
                qmQuotes[1].exchange='<div class="qmjsleft">Exchange</div><div class="qmjsright">NYE</div>';
            

            qmQuotes[1].entitlement='<div class="qmjsleft">Entitlement</div><div class="qmjsright">DL</div>';
            qmQuotes[1].last='<div class="qmjsleft">Last</div><div class="qmjsright">48.4931</div>';
		    qmQuotes[1].change='<div class="qmjsleft">Change</div><div class="qmjsright">-0.2769</div>';
		    qmQuotes[1].changePercent='<div class="qmjsleft">% Change</div><div class="qmjsright">-0.57%</div>';
		    qmQuotes[1].bid='<div class="qmjsleft">Bid</div><div class="qmjsright">48.49</div>';
		    qmQuotes[1].bidSize='<div class="qmjsleft">Bid Size</div><div class="qmjsright">1000</div>';
		    qmQuotes[1].ask='<div class="qmjsleft">Ask</div><div class="qmjsright">48.50</div>';
		    qmQuotes[1].askSize='<div class="qmjsleft">Ask Size</div><div class="qmjsright">900</div>';
		    qmQuotes[1].volume='<div class="qmjsleft">Volume</div><div class="qmjsright">3.5m</div>';
		    qmQuotes[1].high52Week='<div class="qmjsleft">52 Week High</div><div class="qmjsright">59.77</div>';
		    qmQuotes[1].low52Week='<div class="qmjsleft">52 Week Low</div><div class="qmjsright">40.07</div>';
		    qmQuotes[1].yield='<div class="qmjsleft">Yield</div><div class="qmjsright">4.839</div>';
		    qmQuotes[1].dividend='<div class="qmjsleft">Dividend</div><div class="qmjsright">2.36</div>';
		    qmQuotes[1].dividendDate='<div class="qmjsleft">Div. Date</div><div class="qmjsright">06/10/15</div>';
		    qmQuotes[1].eps='<div class="qmjsleft">E.P.S.</div><div class="qmjsright">2.81</div>';
		    qmQuotes[1].peRatio='<div class="qmjsleft">PE Ratio</div><div class="qmjsright">17.30</div>';
            qmQuotes[1].quoteTime='<div class="qmjsleft">Quote Time</div><div class="qmjsright">06/08/2015 12:57</div>';
            qmQuotes[1].lastTradeTime='<div class="qmjsleft">Last Trade Time</div><div class="qmjsright">06/08/2015 12:57</div>';
            qmQuotes[1].volumeActual='<div class="qmjsleft">Volume (Actual)</div><div class="qmjsright">3503963</div>';
            qmQuotes[1].lastTradeVolume='<div class="qmjsleft">Last Trade Volume</div><div class="qmjsright">50</div>';
            qmQuotes[1].premarketQuoteTime='<div class="qmjsleft">Pre Market Quote Time</div><div class="qmjsright">7:48</div>';
            qmQuotes[1].premarketLast='<div class="qmjsleft">Pre Market Last</div><div class="qmjsright">48.80</div>';
            qmQuotes[1].premarketVolume='<div class="qmjsleft">Pre Market  Volume</div><div class="qmjsright">100</div>';
            qmQuotes[1].aftermarketQuoteTime='<div class="qmjsleft">After Market Quote Time</div><div class="qmjsright">N/A</div>';
            qmQuotes[1].aftermarketLast='<div class="qmjsleft">After Market Last</div><div class="qmjsright">N/A</div>';
            qmQuotes[1].aftermarketVolume='<div class="qmjsleft">After Market Volume</div><div class="qmjsright">0</div>';

		
			qmQuotes[1].tick='<div class="qmjsleft">Tick</div><div class="qmjsright"></div>';
		
		    qmQuotes[1].open='<div class="qmjsleft">Open</div><div class="qmjsright">48.42</div>';
		    qmQuotes[1].high='<div class="qmjsleft">High</div><div class="qmjsright">49.02</div>';
		    qmQuotes[1].low='<div class="qmjsleft">Low</div><div class="qmjsright">48.23</div>';
		    qmQuotes[1].close='<div class="qmjsleft">Close</div><div class="qmjsright">48.77</div>';
		    qmQuotes[1].tradeVolume='<div class="qmjsleft">Trade Volume</div><div class="qmjsright">24.85k</div>';
		    qmQuotes[1].tradeVolumeActual='<div class="qmjsleft">Trade Volume (Actual)</div><div class="qmjsright">24,846</div>';
		    qmQuotes[1].avg10DayVolume='<div class="qmjsleft">Average Daily Volume (10 Days)</div><div class="qmjsright">7,213,043</div>';
		    qmQuotes[1].avg30DayVolume='<div class="qmjsleft">Average Daily Volume (30 Days)</div><div class="qmjsright">8,036,540</div>';
		    qmQuotes[1].shares='<div class="qmjsleft">Shares</div><div class="qmjsright">748,961,482</div>';
		    qmQuotes[1].marketCap='<div class="qmjsleft">Market Cap</div><div class="qmjsright">36.32b</div>';
		    
		    	
			    	qmQuotes[1].symbolStatusCode='<div class="qmjsleft">Status Code</div><div class="qmjsright">-</div>';
			    
			
	

   