/* Modernizr 2.8.0 (Custom Build) | MIT & BSD
* Build: http://modernizr.com/download/#-opacity-cssgradients-applicationcache-canvas-canvastext-draganddrop-hashchange-history-audio-video-input-inputtypes-localstorage-postmessage-sessionstorage-websockets-websqldatabase-webworkers-shiv-cssclasses-teststyles-hasevent-prefixes-domprefixes-load
*/
;



window.Modernizr = (function( window, document, undefined ) {

    var version = '2.8.0',

    Modernizr = {},

    enableClasses = true,

    docElement = document.documentElement,

    mod = 'modernizr',
    modElem = document.createElement(mod),
    mStyle = modElem.style,

    inputElem  = document.createElement('input')  ,

    smile = ':)',

    toString = {}.toString,

    prefixes = ' -webkit- -moz- -o- -ms- '.split(' '),



    omPrefixes = 'Webkit Moz O ms',

    cssomPrefixes = omPrefixes.split(' '),

    domPrefixes = omPrefixes.toLowerCase().split(' '),


    tests = {},
    inputs = {},
    attrs = {},

    classes = [],

    slice = classes.slice,

    featureName, 


    injectElementWithStyles = function( rule, callback, nodes, testnames ) {

      var style, ret, node, docOverflow,
          div = document.createElement('div'),
                body = document.body,
                fakeBody = body || document.createElement('body');

      if ( parseInt(nodes, 10) ) {
                      while ( nodes-- ) {
              node = document.createElement('div');
              node.id = testnames ? testnames[nodes] : mod + (nodes + 1);
              div.appendChild(node);
          }
      }

                style = ['&#173;','<style id="s', mod, '">', rule, '</style>'].join('');
      div.id = mod;
          (body ? div : fakeBody).innerHTML += style;
      fakeBody.appendChild(div);
      if ( !body ) {
                fakeBody.style.background = '';
                fakeBody.style.overflow = 'hidden';
          docOverflow = docElement.style.overflow;
          docElement.style.overflow = 'hidden';
          docElement.appendChild(fakeBody);
      }

      ret = callback(div, rule);
        if ( !body ) {
          fakeBody.parentNode.removeChild(fakeBody);
          docElement.style.overflow = docOverflow;
      } else {
          div.parentNode.removeChild(div);
      }

      return !!ret;

    },



    isEventSupported = (function() {

      var TAGNAMES = {
        'select': 'input', 'change': 'input',
        'submit': 'form', 'reset': 'form',
        'error': 'img', 'load': 'img', 'abort': 'img'
      };

      function isEventSupported( eventName, element ) {

        element = element || document.createElement(TAGNAMES[eventName] || 'div');
        eventName = 'on' + eventName;

            var isSupported = eventName in element;

        if ( !isSupported ) {
                if ( !element.setAttribute ) {
            element = document.createElement('div');
          }
          if ( element.setAttribute && element.removeAttribute ) {
            element.setAttribute(eventName, '');
            isSupported = is(element[eventName], 'function');

                    if ( !is(element[eventName], 'undefined') ) {
              element[eventName] = undefined;
            }
            element.removeAttribute(eventName);
          }
        }

        element = null;
        return isSupported;
      }
      return isEventSupported;
    })(),


    _hasOwnProperty = ({}).hasOwnProperty, hasOwnProp;

    if ( !is(_hasOwnProperty, 'undefined') && !is(_hasOwnProperty.call, 'undefined') ) {
      hasOwnProp = function (object, property) {
        return _hasOwnProperty.call(object, property);
      };
    }
    else {
      hasOwnProp = function (object, property) { 
        return ((property in object) && is(object.constructor.prototype[property], 'undefined'));
      };
    }


    if (!Function.prototype.bind) {
      Function.prototype.bind = function bind(that) {

        var target = this;

        if (typeof target != "function") {
            throw new TypeError();
        }

        var args = slice.call(arguments, 1),
            bound = function () {

            if (this instanceof bound) {

              var F = function(){};
              F.prototype = target.prototype;
              var self = new F();

              var result = target.apply(
                  self,
                  args.concat(slice.call(arguments))
              );
              if (Object(result) === result) {
                  return result;
              }
              return self;

            } else {

              return target.apply(
                  that,
                  args.concat(slice.call(arguments))
              );

            }

        };

        return bound;
      };
    }

    function setCss( str ) {
        mStyle.cssText = str;
    }

    function setCssAll( str1, str2 ) {
        return setCss(prefixes.join(str1 + ';') + ( str2 || '' ));
    }

    function is( obj, type ) {
        return typeof obj === type;
    }

    function contains( str, substr ) {
        return !!~('' + str).indexOf(substr);
    }


    function testDOMProps( props, obj, elem ) {
        for ( var i in props ) {
            var item = obj[props[i]];
            if ( item !== undefined) {

                            if (elem === false) return props[i];

                            if (is(item, 'function')){
                                return item.bind(elem || obj);
                }

                            return item;
            }
        }
        return false;
    }    tests['canvas'] = function() {
        var elem = document.createElement('canvas');
        return !!(elem.getContext && elem.getContext('2d'));
    };

    tests['canvastext'] = function() {
        return !!(Modernizr['canvas'] && is(document.createElement('canvas').getContext('2d').fillText, 'function'));
    };
    tests['postmessage'] = function() {
      return !!window.postMessage;
    };


    tests['websqldatabase'] = function() {
      return !!window.openDatabase;
    };


    tests['hashchange'] = function() {
      return isEventSupported('hashchange', window) && (document.documentMode === undefined || document.documentMode > 7);
    };

    tests['history'] = function() {
      return !!(window.history && history.pushState);
    };

    tests['draganddrop'] = function() {
        var div = document.createElement('div');
        return ('draggable' in div) || ('ondragstart' in div && 'ondrop' in div);
    };

    tests['websockets'] = function() {
        return 'WebSocket' in window || 'MozWebSocket' in window;
    };

    tests['opacity'] = function() {
                setCssAll('opacity:.55');

                    return (/^0.55$/).test(mStyle.opacity);
    };

    tests['cssgradients'] = function() {
        var str1 = 'background-image:',
            str2 = 'gradient(linear,left top,right bottom,from(#9f9),to(white));',
            str3 = 'linear-gradient(left top,#9f9, white);';

        setCss(
                       (str1 + '-webkit- '.split(' ').join(str2 + str1) +
                       prefixes.join(str3 + str1)).slice(0, -str1.length)
        );

        return contains(mStyle.backgroundImage, 'gradient');
    };

    tests['video'] = function() {
        var elem = document.createElement('video'),
            bool = false;

            try {
            if ( bool = !!elem.canPlayType ) {
                bool      = new Boolean(bool);
                bool.ogg  = elem.canPlayType('video/ogg; codecs="theora"')      .replace(/^no$/,'');

                            bool.h264 = elem.canPlayType('video/mp4; codecs="avc1.42E01E"') .replace(/^no$/,'');

                bool.webm = elem.canPlayType('video/webm; codecs="vp8, vorbis"').replace(/^no$/,'');
            }

        } catch(e) { }

        return bool;
    };

    tests['audio'] = function() {
        var elem = document.createElement('audio'),
           bool = false;

        try {
            if ( bool = !!elem.canPlayType ) {
                bool      = new Boolean(bool);
                bool.ogg  = elem.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/,'');
                bool.mp3  = elem.canPlayType('audio/mpeg;')               .replace(/^no$/,'');

                                                    bool.wav  = elem.canPlayType('audio/wav; codecs="1"')     .replace(/^no$/,'');
                bool.m4a  = ( elem.canPlayType('audio/x-m4a;')            ||
                              elem.canPlayType('audio/aac;'))             .replace(/^no$/,'');
            }
        } catch(e) { }

        return bool;
    };


    tests['localstorage'] = function() {
        try {
            localStorage.setItem(mod, mod);
            localStorage.removeItem(mod);
            return true;
        } catch(e) {
            return false;
        }
    };

    tests['sessionstorage'] = function() {
        try {
            sessionStorage.setItem(mod, mod);
            sessionStorage.removeItem(mod);
            return true;
        } catch(e) {
            return false;
        }
    };


    tests['webworkers'] = function() {
        return !!window.Worker;
    };


    tests['applicationcache'] = function() {
        return !!window.applicationCache;
    };


    function webforms() {
                                            Modernizr['input'] = (function( props ) {
            for ( var i = 0, len = props.length; i < len; i++ ) {
                attrs[ props[i] ] = !!(props[i] in inputElem);
            }
            if (attrs.list){
                                  attrs.list = !!(document.createElement('datalist') && window.HTMLDataListElement);
            }
            return attrs;
        })('autocomplete autofocus list placeholder max min multiple pattern required step'.split(' '));
                            Modernizr['inputtypes'] = (function(props) {

            for ( var i = 0, bool, inputElemType, defaultView, len = props.length; i < len; i++ ) {

                inputElem.setAttribute('type', inputElemType = props[i]);
                bool = inputElem.type !== 'text';

                                                    if ( bool ) {

                    inputElem.value         = smile;
                    inputElem.style.cssText = 'position:absolute;visibility:hidden;';

                    if ( /^range$/.test(inputElemType) && inputElem.style.WebkitAppearance !== undefined ) {

                      docElement.appendChild(inputElem);
                      defaultView = document.defaultView;

                                        bool =  defaultView.getComputedStyle &&
                              defaultView.getComputedStyle(inputElem, null).WebkitAppearance !== 'textfield' &&
                                                                                  (inputElem.offsetHeight !== 0);

                      docElement.removeChild(inputElem);

                    } else if ( /^(search|tel)$/.test(inputElemType) ){
                                                                                    } else if ( /^(url|email)$/.test(inputElemType) ) {
                                        bool = inputElem.checkValidity && inputElem.checkValidity() === false;

                    } else {
                                        bool = inputElem.value != smile;
                    }
                }

                inputs[ props[i] ] = !!bool;
            }
            return inputs;
        })('search tel url email datetime date month week time datetime-local number range color'.split(' '));
        }
    for ( var feature in tests ) {
        if ( hasOwnProp(tests, feature) ) {
                                    featureName  = feature.toLowerCase();
            Modernizr[featureName] = tests[feature]();

            classes.push((Modernizr[featureName] ? '' : 'no-') + featureName);
        }
    }

    Modernizr.input || webforms();


     Modernizr.addTest = function ( feature, test ) {
       if ( typeof feature == 'object' ) {
         for ( var key in feature ) {
           if ( hasOwnProp( feature, key ) ) {
             Modernizr.addTest( key, feature[ key ] );
           }
         }
       } else {

         feature = feature.toLowerCase();

         if ( Modernizr[feature] !== undefined ) {
                                              return Modernizr;
         }

         test = typeof test == 'function' ? test() : test;

         if (typeof enableClasses !== "undefined" && enableClasses) {
           docElement.className += ' ' + (test ? '' : 'no-') + feature;
         }
         Modernizr[feature] = test;

       }

       return Modernizr; 
     };


    setCss('');
    modElem = inputElem = null;

    ;(function(window, document) {
                var version = '3.7.0';

            var options = window.html5 || {};

            var reSkip = /^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i;

            var saveClones = /^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i;

            var supportsHtml5Styles;

            var expando = '_html5shiv';

            var expanID = 0;

            var expandoData = {};

            var supportsUnknownElements;

        (function() {
          try {
            var a = document.createElement('a');
            a.innerHTML = '<xyz></xyz>';
                    supportsHtml5Styles = ('hidden' in a);

            supportsUnknownElements = a.childNodes.length == 1 || (function() {
                        (document.createElement)('a');
              var frag = document.createDocumentFragment();
              return (
                typeof frag.cloneNode == 'undefined' ||
                typeof frag.createDocumentFragment == 'undefined' ||
                typeof frag.createElement == 'undefined'
              );
            }());
          } catch(e) {
                    supportsHtml5Styles = true;
            supportsUnknownElements = true;
          }

        }());

            function addStyleSheet(ownerDocument, cssText) {
          var p = ownerDocument.createElement('p'),
          parent = ownerDocument.getElementsByTagName('head')[0] || ownerDocument.documentElement;

          p.innerHTML = 'x<style>' + cssText + '</style>';
          return parent.insertBefore(p.lastChild, parent.firstChild);
        }

            function getElements() {
          var elements = html5.elements;
          return typeof elements == 'string' ? elements.split(' ') : elements;
        }

            function getExpandoData(ownerDocument) {
          var data = expandoData[ownerDocument[expando]];
          if (!data) {
            data = {};
            expanID++;
            ownerDocument[expando] = expanID;
            expandoData[expanID] = data;
          }
          return data;
        }

            function createElement(nodeName, ownerDocument, data){
          if (!ownerDocument) {
            ownerDocument = document;
          }
          if(supportsUnknownElements){
            return ownerDocument.createElement(nodeName);
          }
          if (!data) {
            data = getExpandoData(ownerDocument);
          }
          var node;

          if (data.cache[nodeName]) {
            node = data.cache[nodeName].cloneNode();
          } else if (saveClones.test(nodeName)) {
            node = (data.cache[nodeName] = data.createElem(nodeName)).cloneNode();
         } else {
            node = data.createElem(nodeName);
          }

                                                    return node.canHaveChildren && !reSkip.test(nodeName) && !node.tagUrn ? data.frag.appendChild(node) : node;
        }

            function createDocumentFragment(ownerDocument, data){
          if (!ownerDocument) {
            ownerDocument = document;
          }
          if(supportsUnknownElements){
            return ownerDocument.createDocumentFragment();
          }
          data = data || getExpandoData(ownerDocument);
          var clone = data.frag.cloneNode(),
          i = 0,
          elems = getElements(),
          l = elems.length;
          for(;i<l;i++){
            clone.createElement(elems[i]);
          }
          return clone;
        }

            function shivMethods(ownerDocument, data) {
          if (!data.cache) {
            data.cache = {};
            data.createElem = ownerDocument.createElement;
            data.createFrag = ownerDocument.createDocumentFragment;
            data.frag = data.createFrag();
          }


          ownerDocument.createElement = function(nodeName) {
                    if (!html5.shivMethods) {
              return data.createElem(nodeName);
            }
            return createElement(nodeName, ownerDocument, data);
          };

          ownerDocument.createDocumentFragment = Function('h,f', 'return function(){' +
                                                          'var n=f.cloneNode(),c=n.createElement;' +
                                                          'h.shivMethods&&(' +
                                                                                                                getElements().join().replace(/[\w\-]+/g, function(nodeName) {
            data.createElem(nodeName);
            data.frag.createElement(nodeName);
            return 'c("' + nodeName + '")';
          }) +
            ');return n}'
                                                         )(html5, data.frag);
        }

            function shivDocument(ownerDocument) {
          if (!ownerDocument) {
            ownerDocument = document;
          }
          var data = getExpandoData(ownerDocument);

          if (html5.shivCSS && !supportsHtml5Styles && !data.hasCSS) {
            data.hasCSS = !!addStyleSheet(ownerDocument,
                                                                                'article,aside,dialog,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}' +
                                                                                    'mark{background:#FF0;color:#000}' +
                                                                                    'template{display:none}'
                                         );
          }
          if (!supportsUnknownElements) {
            shivMethods(ownerDocument, data);
          }
          return ownerDocument;
        }

            var html5 = {

                'elements': options.elements || 'abbr article aside audio bdi canvas data datalist details dialog figcaption figure footer header hgroup main mark meter nav output progress section summary template time video',

                'version': version,

                'shivCSS': (options.shivCSS !== false),

                'supportsUnknownElements': supportsUnknownElements,

                'shivMethods': (options.shivMethods !== false),

                'type': 'default',

                'shivDocument': shivDocument,

                createElement: createElement,

                createDocumentFragment: createDocumentFragment
        };

            window.html5 = html5;

            shivDocument(document);

    }(this, document));

    Modernizr._version      = version;

    Modernizr._prefixes     = prefixes;
    Modernizr._domPrefixes  = domPrefixes;
    Modernizr._cssomPrefixes  = cssomPrefixes;


    Modernizr.hasEvent      = isEventSupported;    Modernizr.testStyles    = injectElementWithStyles;    docElement.className = docElement.className.replace(/(^|\s)no-js(\s|$)/, '$1$2') +

                                                    (enableClasses ? ' js ' + classes.join(' ') : '');

    return Modernizr;

})(this, this.document);
/*yepnope1.5.4|WTFPL*/
(function(a,b,c){function d(a){return"[object Function]"==o.call(a)}function e(a){return"string"==typeof a}function f(){}function g(a){return!a||"loaded"==a||"complete"==a||"uninitialized"==a}function h(){var a=p.shift();q=1,a?a.t?m(function(){("c"==a.t?B.injectCss:B.injectJs)(a.s,0,a.a,a.x,a.e,1)},0):(a(),h()):q=0}function i(a,c,d,e,f,i,j){function k(b){if(!o&&g(l.readyState)&&(u.r=o=1,!q&&h(),l.onload=l.onreadystatechange=null,b)){"img"!=a&&m(function(){t.removeChild(l)},50);for(var d in y[c])y[c].hasOwnProperty(d)&&y[c][d].onload()}}var j=j||B.errorTimeout,l=b.createElement(a),o=0,r=0,u={t:d,s:c,e:f,a:i,x:j};1===y[c]&&(r=1,y[c]=[]),"object"==a?l.data=c:(l.src=c,l.type=a),l.width=l.height="0",l.onerror=l.onload=l.onreadystatechange=function(){k.call(this,r)},p.splice(e,0,u),"img"!=a&&(r||2===y[c]?(t.insertBefore(l,s?null:n),m(k,j)):y[c].push(l))}function j(a,b,c,d,f){return q=0,b=b||"j",e(a)?i("c"==b?v:u,a,b,this.i++,c,d,f):(p.splice(this.i++,0,a),1==p.length&&h()),this}function k(){var a=B;return a.loader={load:j,i:0},a}var l=b.documentElement,m=a.setTimeout,n=b.getElementsByTagName("script")[0],o={}.toString,p=[],q=0,r="MozAppearance"in l.style,s=r&&!!b.createRange().compareNode,t=s?l:n.parentNode,l=a.opera&&"[object Opera]"==o.call(a.opera),l=!!b.attachEvent&&!l,u=r?"object":l?"script":"img",v=l?"script":u,w=Array.isArray||function(a){return"[object Array]"==o.call(a)},x=[],y={},z={timeout:function(a,b){return b.length&&(a.timeout=b[0]),a}},A,B;B=function(a){function b(a){var a=a.split("!"),b=x.length,c=a.pop(),d=a.length,c={url:c,origUrl:c,prefixes:a},e,f,g;for(f=0;f<d;f++)g=a[f].split("="),(e=z[g.shift()])&&(c=e(c,g));for(f=0;f<b;f++)c=x[f](c);return c}function g(a,e,f,g,h){var i=b(a),j=i.autoCallback;i.url.split(".").pop().split("?").shift(),i.bypass||(e&&(e=d(e)?e:e[a]||e[g]||e[a.split("/").pop().split("?")[0]]),i.instead?i.instead(a,e,f,g,h):(y[i.url]?i.noexec=!0:y[i.url]=1,f.load(i.url,i.forceCSS||!i.forceJS&&"css"==i.url.split(".").pop().split("?").shift()?"c":c,i.noexec,i.attrs,i.timeout),(d(e)||d(j))&&f.load(function(){k(),e&&e(i.origUrl,h,g),j&&j(i.origUrl,h,g),y[i.url]=2})))}function h(a,b){function c(a,c){if(a){if(e(a))c||(j=function(){var a=[].slice.call(arguments);k.apply(this,a),l()}),g(a,j,b,0,h);else if(Object(a)===a)for(n in m=function(){var b=0,c;for(c in a)a.hasOwnProperty(c)&&b++;return b}(),a)a.hasOwnProperty(n)&&(!c&&!--m&&(d(j)?j=function(){var a=[].slice.call(arguments);k.apply(this,a),l()}:j[n]=function(a){return function(){var b=[].slice.call(arguments);a&&a.apply(this,b),l()}}(k[n])),g(a[n],j,b,n,h))}else!c&&l()}var h=!!a.test,i=a.load||a.both,j=a.callback||f,k=j,l=a.complete||f,m,n;c(h?a.yep:a.nope,!!i),i&&c(i)}var i,j,l=this.yepnope.loader;if(e(a))g(a,0,l,0);else if(w(a))for(i=0;i<a.length;i++)j=a[i],e(j)?g(j,0,l,0):w(j)?B(j):Object(j)===j&&h(j,l);else Object(a)===a&&h(a,l)},B.addPrefix=function(a,b){z[a]=b},B.addFilter=function(a){x.push(a)},B.errorTimeout=1e4,null==b.readyState&&b.addEventListener&&(b.readyState="loading",b.addEventListener("DOMContentLoaded",A=function(){b.removeEventListener("DOMContentLoaded",A,0),b.readyState="complete"},0)),a.yepnope=k(),a.yepnope.executeStack=h,a.yepnope.injectJs=function(a,c,d,e,i,j){var k=b.createElement("script"),l,o,e=e||B.errorTimeout;k.src=a;for(o in d)k.setAttribute(o,d[o]);c=j?h:c||f,k.onreadystatechange=k.onload=function(){!l&&g(k.readyState)&&(l=1,c(),k.onload=k.onreadystatechange=null)},m(function(){l||(l=1,c(1))},e),i?k.onload():n.parentNode.insertBefore(k,n)},a.yepnope.injectCss=function(a,c,d,e,g,i){var e=b.createElement("link"),j,c=i?h:c||f;e.href=a,e.rel="stylesheet",e.type="text/css";for(j in d)e.setAttribute(j,d[j]);g||(n.parentNode.insertBefore(e,n),m(c,0))}})(this,document);
Modernizr.load=function(){yepnope.apply(window,[].slice.call(arguments,0));};
;
/* jQuery SelectBox - https://github.com/claviska/jquery-selectBox */
if(jQuery)(function($){$.extend($.fn,{selectBox:function(method,data){var typeTimer,typeSearch='',isMac=navigator.platform.match(/mac/i);var init=function(select,data){var options;if(navigator.userAgent.match(/iPad|iPhone|Android|IEMobile|BlackBerry/i))return false;if(select.tagName.toLowerCase()!=='select')return false;select=$(select);if(select.data('selectBox-control'))return false;var control=$('<a class="selectBox" />'),inline=select.attr('multiple')||parseInt(select.attr('size'))>1;var settings=data||{};control.width(select.outerWidth()).addClass(select.attr('class')).attr('title',select.attr('title')||'').attr('tabindex',parseInt(select.attr('tabindex'))).css('display','inline-block').bind('focus.selectBox',function(){if(this!==document.activeElement&&document.body!==document.activeElement)$(document.activeElement).blur();if(control.hasClass('selectBox-active'))return;control.addClass('selectBox-active');select.trigger('focus')}).bind('blur.selectBox',function(){if(!control.hasClass('selectBox-active'))return;control.removeClass('selectBox-active');select.trigger('blur')});if(!$(window).data('selectBox-bindings')){$(window).data('selectBox-bindings',true).bind('scroll.selectBox',hideMenus).bind('resize.selectBox',hideMenus)}if(select.attr('disabled'))control.addClass('selectBox-disabled');select.bind('click.selectBox',function(event){control.focus();event.preventDefault()});if(inline){options=getOptions(select,'inline');control.append(options).data('selectBox-options',options).addClass('selectBox-inline selectBox-menuShowing').bind('keydown.selectBox',function(event){handleKeyDown(select,event)}).bind('keypress.selectBox',function(event){handleKeyPress(select,event)}).bind('mousedown.selectBox',function(event){if($(event.target).is('A.selectBox-inline'))event.preventDefault();if(!control.hasClass('selectBox-focus'))control.focus()}).insertAfter(select);if(!select[0].style.height){var size=select.attr('size')?parseInt(select.attr('size')):5;var tmp=control.clone().removeAttr('id').css({position:'absolute',top:'-9999em'}).show().appendTo('body');tmp.find('.selectBox-options').html('<li><a>\u00A0</a></li>');var optionHeight=parseInt(tmp.find('.selectBox-options A:first').html('&nbsp;').outerHeight());tmp.remove();control.height(optionHeight*size)}disableSelection(control)}else{var label=$('<span class="selectBox-label" />'),arrow=$('<span class="selectBox-arrow" />');label.attr('class',getLabelClass(select)).text(getLabelText(select));options=getOptions(select,'dropdown');options.appendTo('BODY');control.data('selectBox-options',options).addClass('selectBox-dropdown').append(label).append(arrow).bind('mousedown.selectBox',function(event){if(control.hasClass('selectBox-menuShowing')){hideMenus()}else{event.stopPropagation();options.data('selectBox-down-at-x',event.screenX).data('selectBox-down-at-y',event.screenY);showMenu(select)}}).bind('keydown.selectBox',function(event){handleKeyDown(select,event)}).bind('keypress.selectBox',function(event){handleKeyPress(select,event)}).bind('open.selectBox',function(event,triggerData){if(triggerData&&triggerData._selectBox===true)return;showMenu(select)}).bind('close.selectBox',function(event,triggerData){if(triggerData&&triggerData._selectBox===true)return;hideMenus()}).insertAfter(select);var labelWidth=control.width()-arrow.outerWidth()-parseInt(label.css('paddingLeft'))-parseInt(label.css('paddingLeft'));label.width(labelWidth);disableSelection(control)}select.addClass('selectBox').data('selectBox-control',control).data('selectBox-settings',settings).hide()};var getOptions=function(select,type){var options;var _getOptions=function(select,options){select.children('OPTION, OPTGROUP').each(function(){if($(this).is('OPTION')){if($(this).length>0){generateOptions($(this),options)}else{options.append('<li>\u00A0</li>')}}else{var optgroup=$('<li class="selectBox-optgroup" />');optgroup.text($(this).attr('label'));options.append(optgroup);options=_getOptions($(this),options)}});return options};switch(type){case'inline':options=$('<ul class="selectBox-options" />');options=_getOptions(select,options);options.find('A').bind('mouseover.selectBox',function(event){addHover(select,$(this).parent())}).bind('mouseout.selectBox',function(event){removeHover(select,$(this).parent())}).bind('mousedown.selectBox',function(event){event.preventDefault();if(!select.selectBox('control').hasClass('selectBox-active'))select.selectBox('control').focus()}).bind('mouseup.selectBox',function(event){hideMenus();selectOption(select,$(this).parent(),event)});disableSelection(options);return options;case'dropdown':options=$('<ul class="selectBox-dropdown-menu selectBox-options" />');options=_getOptions(select,options);options.data('selectBox-select',select).css('display','none').appendTo('BODY').find('A').bind('mousedown.selectBox',function(event){event.preventDefault();if(event.screenX===options.data('selectBox-down-at-x')&&event.screenY===options.data('selectBox-down-at-y')){options.removeData('selectBox-down-at-x').removeData('selectBox-down-at-y');hideMenus()}}).bind('mouseup.selectBox',function(event){if(event.screenX===options.data('selectBox-down-at-x')&&event.screenY===options.data('selectBox-down-at-y')){return}else{options.removeData('selectBox-down-at-x').removeData('selectBox-down-at-y')}selectOption(select,$(this).parent());hideMenus()}).bind('mouseover.selectBox',function(event){addHover(select,$(this).parent())}).bind('mouseout.selectBox',function(event){removeHover(select,$(this).parent())});var classes=select.attr('class')||'';if(classes!==''){classes=classes.split(' ');for(var i in classes)options.addClass(classes[i]+'-selectBox-dropdown-menu')}disableSelection(options);return options}};var getLabelClass=function(select){var selected=$(select).find('OPTION:selected');return('selectBox-label '+(selected.attr('class')||'')).replace(/\s+$/,'')};var getLabelText=function(select){var selected=$(select).find('OPTION:selected');return selected.text()||'\u00A0'};var setLabel=function(select){select=$(select);var control=select.data('selectBox-control');if(!control)return;control.find('.selectBox-label').attr('class',getLabelClass(select)).text(getLabelText(select))};var destroy=function(select){select=$(select);var control=select.data('selectBox-control');if(!control)return;var options=control.data('selectBox-options');options.remove();control.remove();select.removeClass('selectBox').removeData('selectBox-control').data('selectBox-control',null).removeData('selectBox-settings').data('selectBox-settings',null).show()};var refresh=function(select){select=$(select);select.selectBox('options',select.html())};var showMenu=function(select){select=$(select);var control=select.data('selectBox-control'),settings=select.data('selectBox-settings'),options=control.data('selectBox-options');if(control.hasClass('selectBox-disabled'))return false;hideMenus();var borderBottomWidth=isNaN(control.css('borderBottomWidth'))?0:parseInt(control.css('borderBottomWidth'));options.width(control.innerWidth()).css({top:control.offset().top+control.outerHeight()-borderBottomWidth,left:control.offset().left});if(select.triggerHandler('beforeopen'))return false;var dispatchOpenEvent=function(){select.triggerHandler('open',{_selectBox:true})};switch(settings.menuTransition){case'fade':options.fadeIn(settings.menuSpeed,dispatchOpenEvent);break;case'slide':options.slideDown(settings.menuSpeed,dispatchOpenEvent);break;default:options.show(settings.menuSpeed,dispatchOpenEvent);break}if(!settings.menuSpeed)dispatchOpenEvent();var li=options.find('.selectBox-selected:first');keepOptionInView(select,li,true);addHover(select,li);control.addClass('selectBox-menuShowing');$(document).bind('mousedown.selectBox',function(event){if($(event.target).parents().andSelf().hasClass('selectBox-options'))return;hideMenus()})};var hideMenus=function(){if($(".selectBox-dropdown-menu:visible").length===0)return;$(document).unbind('mousedown.selectBox');$(".selectBox-dropdown-menu").each(function(){var options=$(this),select=options.data('selectBox-select'),control=select.data('selectBox-control'),settings=select.data('selectBox-settings');if(select.triggerHandler('beforeclose'))return false;var dispatchCloseEvent=function(){select.triggerHandler('close',{_selectBox:true})};switch(settings.menuTransition){case'fade':options.fadeOut(settings.menuSpeed,dispatchCloseEvent);break;case'slide':options.slideUp(settings.menuSpeed,dispatchCloseEvent);break;default:options.hide(settings.menuSpeed,dispatchCloseEvent);break}if(!settings.menuSpeed)dispatchCloseEvent();control.removeClass('selectBox-menuShowing')})};var selectOption=function(select,li,event){select=$(select);li=$(li);var control=select.data('selectBox-control'),settings=select.data('selectBox-settings');if(control.hasClass('selectBox-disabled'))return false;if(li.length===0||li.hasClass('selectBox-disabled'))return false;if(select.attr('multiple')){if(event.shiftKey&&control.data('selectBox-last-selected')){li.toggleClass('selectBox-selected');var affectedOptions;if(li.index()>control.data('selectBox-last-selected').index()){affectedOptions=li.siblings().slice(control.data('selectBox-last-selected').index(),li.index())}else{affectedOptions=li.siblings().slice(li.index(),control.data('selectBox-last-selected').index())}affectedOptions=affectedOptions.not('.selectBox-optgroup, .selectBox-disabled');if(li.hasClass('selectBox-selected')){affectedOptions.addClass('selectBox-selected')}else{affectedOptions.removeClass('selectBox-selected')}}else if((isMac&&event.metaKey)||(!isMac&&event.ctrlKey)){li.toggleClass('selectBox-selected')}else{li.siblings().removeClass('selectBox-selected');li.addClass('selectBox-selected')}}else{li.siblings().removeClass('selectBox-selected');li.addClass('selectBox-selected')}if(control.hasClass('selectBox-dropdown')){control.find('.selectBox-label').text(li.text())}var i=0,selection=[];if(select.attr('multiple')){control.find('.selectBox-selected A').each(function(){selection[i++]=$(this).attr('rel')})}else{selection=li.find('A').attr('rel')}control.data('selectBox-last-selected',li);if(select.val()!==selection){select.val(selection);setLabel(select);select.trigger('change')}return true};var addHover=function(select,li){select=$(select);li=$(li);var control=select.data('selectBox-control'),options=control.data('selectBox-options');options.find('.selectBox-hover').removeClass('selectBox-hover');li.addClass('selectBox-hover')};var removeHover=function(select,li){select=$(select);li=$(li);var control=select.data('selectBox-control'),options=control.data('selectBox-options');options.find('.selectBox-hover').removeClass('selectBox-hover')};var keepOptionInView=function(select,li,center){if(!li||li.length===0)return;select=$(select);var control=select.data('selectBox-control'),options=control.data('selectBox-options'),scrollBox=control.hasClass('selectBox-dropdown')?options:options.parent(),top=parseInt(li.offset().top-scrollBox.position().top),bottom=parseInt(top+li.outerHeight());if(center){scrollBox.scrollTop(li.offset().top-scrollBox.offset().top+scrollBox.scrollTop()-(scrollBox.height()/2))}else{if(top<0){scrollBox.scrollTop(li.offset().top-scrollBox.offset().top+scrollBox.scrollTop())}if(bottom>scrollBox.height()){scrollBox.scrollTop((li.offset().top+li.outerHeight())-scrollBox.offset().top+scrollBox.scrollTop()-scrollBox.height())}}};var handleKeyDown=function(select,event){select=$(select);var control=select.data('selectBox-control'),options=control.data('selectBox-options'),settings=select.data('selectBox-settings'),totalOptions=0,i=0;if(control.hasClass('selectBox-disabled'))return;switch(event.keyCode){case 8:event.preventDefault();typeSearch='';break;case 9:case 27:hideMenus();removeHover(select);break;case 13:if(control.hasClass('selectBox-menuShowing')){selectOption(select,options.find('LI.selectBox-hover:first'),event);if(control.hasClass('selectBox-dropdown'))hideMenus()}else{showMenu(select)}break;case 38:case 37:event.preventDefault();if(control.hasClass('selectBox-menuShowing')){var prev=options.find('.selectBox-hover').prev('LI');totalOptions=options.find('LI:not(.selectBox-optgroup)').length;i=0;while(prev.length===0||prev.hasClass('selectBox-disabled')||prev.hasClass('selectBox-optgroup')){prev=prev.prev('LI');if(prev.length===0){if(settings.loopOptions){prev=options.find('LI:last')}else{prev=options.find('LI:first')}}if(++i>=totalOptions)break}addHover(select,prev);selectOption(select,prev,event);keepOptionInView(select,prev)}else{showMenu(select)}break;case 40:case 39:event.preventDefault();if(control.hasClass('selectBox-menuShowing')){var next=options.find('.selectBox-hover').next('LI');totalOptions=options.find('LI:not(.selectBox-optgroup)').length;i=0;while(next.length===0||next.hasClass('selectBox-disabled')||next.hasClass('selectBox-optgroup')){next=next.next('LI');if(next.length===0){if(settings.loopOptions){next=options.find('LI:first')}else{next=options.find('LI:last')}}if(++i>=totalOptions)break}addHover(select,next);selectOption(select,next,event);keepOptionInView(select,next)}else{showMenu(select)}break}};var handleKeyPress=function(select,event){select=$(select);var control=select.data('selectBox-control'),options=control.data('selectBox-options');if(control.hasClass('selectBox-disabled'))return;switch(event.keyCode){case 9:case 27:case 13:case 38:case 37:case 40:case 39:break;default:if(!control.hasClass('selectBox-menuShowing'))showMenu(select);event.preventDefault();clearTimeout(typeTimer);typeSearch+=String.fromCharCode(event.charCode||event.keyCode);options.find('A').each(function(){if($(this).text().substr(0,typeSearch.length).toLowerCase()===typeSearch.toLowerCase()){addHover(select,$(this).parent());keepOptionInView(select,$(this).parent());return false}});typeTimer=setTimeout(function(){typeSearch=''},1000);break}};var enable=function(select){select=$(select);select.attr('disabled',false);var control=select.data('selectBox-control');if(!control)return;control.removeClass('selectBox-disabled')};var disable=function(select){select=$(select);select.attr('disabled',true);var control=select.data('selectBox-control');if(!control)return;control.addClass('selectBox-disabled')};var setValue=function(select,value){select=$(select);select.val(value);value=select.val();var control=select.data('selectBox-control');if(!control)return;var settings=select.data('selectBox-settings'),options=control.data('selectBox-options');setLabel(select);options.find('.selectBox-selected').removeClass('selectBox-selected');options.find('A').each(function(){if(typeof(value)==='object'){for(var i=0;i<value.length;i++){if($(this).attr('rel')==value[i]){$(this).parent().addClass('selectBox-selected')}}}else{if($(this).attr('rel')==value){$(this).parent().addClass('selectBox-selected')}}});if(settings.change)settings.change.call(select)};var setOptions=function(select,options){select=$(select);var control=select.data('selectBox-control'),settings=select.data('selectBox-settings');switch(typeof(data)){case'string':select.html(data);break;case'object':select.html('');for(var i in data){if(data[i]===null)continue;if(typeof(data[i])==='object'){var optgroup=$('<optgroup label="'+i+'" />');for(var j in data[i]){optgroup.append('<option value="'+j+'">'+data[i][j]+'</option>')}select.append(optgroup)}else{var option=$('<option value="'+i+'">'+data[i]+'</option>');select.append(option)}}break}if(!control)return;control.data('selectBox-options').remove();var type=control.hasClass('selectBox-dropdown')?'dropdown':'inline';options=getOptions(select,type);control.data('selectBox-options',options);switch(type){case'inline':control.append(options);break;case'dropdown':setLabel(select);$("BODY").append(options);break}};var disableSelection=function(selector){$(selector).css('MozUserSelect','none').bind('selectstart',function(event){event.preventDefault()})};var generateOptions=function(self,options){var li=$('<li />'),a=$('<a />');li.addClass(self.attr('class'));li.data(self.data());a.attr('rel',self.val()).text(self.text());li.append(a);if(self.attr('disabled'))li.addClass('selectBox-disabled');if(self.attr('selected'))li.addClass('selectBox-selected');options.append(li)};switch(method){case'control':return $(this).data('selectBox-control');case'settings':if(!data)return $(this).data('selectBox-settings');$(this).each(function(){$(this).data('selectBox-settings',$.extend(true,$(this).data('selectBox-settings'),data))});break;case'options':if(data===undefined)return $(this).data('selectBox-control').data('selectBox-options');$(this).each(function(){setOptions(this,data)});break;case'value':if(data===undefined)return $(this).val();$(this).each(function(){setValue(this,data)});break;case'refresh':$(this).each(function(){refresh(this)});break;case'enable':$(this).each(function(){enable(this)});break;case'disable':$(this).each(function(){disable(this)});break;case'destroy':$(this).each(function(){destroy(this)});break;default:$(this).each(function(){init(this,method)});break}return $(this)}})})(jQuery);

/*!
 * jQuery Tools v1.2.7 - The missing UI library for the Web
 * 
 * dateinput/dateinput.js
 * overlay/overlay.js
 * overlay/overlay.apple.js
 * rangeinput/rangeinput.js
 * scrollable/scrollable.js
 * scrollable/scrollable.autoscroll.js
 * scrollable/scrollable.navigator.js
 * tabs/tabs.js
 * tabs/tabs.slideshow.js
 * toolbox/toolbox.expose.js
 * toolbox/toolbox.flashembed.js
 * toolbox/toolbox.history.js
 * toolbox/toolbox.mousewheel.js
 * tooltip/tooltip.js
 * tooltip/tooltip.dynamic.js
 * tooltip/tooltip.slide.js
 * validator/validator.js
 * 
 * NO COPYRIGHTS OR LICENSES. DO WHAT YOU LIKE.
 * 
 * http://flowplayer.org/tools/
 * 
 * jquery.event.wheel.js - rev 1 
 * Copyright (c) 2008, Three Dub Media (http://threedubmedia.com)
 * Liscensed under the MIT License (MIT-LICENSE.txt)
 * http://www.opensource.org/licenses/mit-license.php
 * Created: 2008-07-01 | Updated: 2008-07-14
 * 
 * -----
 * 
 */
(function(a,b){a.tools=a.tools||{version:"v1.2.7"};var c=[],d={},e,f=[75,76,38,39,74,72,40,37],g={};e=a.tools.dateinput={conf:{format:"mm/dd/yy",formatter:"default",selectors:!1,yearRange:[-5,5],lang:"en",offset:[0,0],speed:0,firstDay:0,min:b,max:b,trigger:0,toggle:0,editable:0,css:{prefix:"cal",input:"date",root:0,head:0,title:0,prev:0,next:0,month:0,year:0,days:0,body:0,weeks:0,today:0,current:0,week:0,off:0,sunday:0,focus:0,disabled:0,trigger:0}},addFormatter:function(a,b){d[a]=b},localize:function(b,c){a.each(c,function(a,b){c[a]=b.split(",")}),g[b]=c}},e.localize("en",{months:"January,February,March,April,May,June,July,August,September,October,November,December",shortMonths:"Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec",days:"Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday",shortDays:"Sun,Mon,Tue,Wed,Thu,Fri,Sat"});function h(a,b){return(new Date(a,b+1,0)).getDate()}function i(a,b){a=""+a,b=b||2;while(a.length<b)a="0"+a;return a}var j=a("<a/>");function k(a,b,c,e){var f=b.getDate(),h=b.getDay(),k=b.getMonth(),l=b.getFullYear(),m={d:f,dd:i(f),ddd:g[e].shortDays[h],dddd:g[e].days[h],m:k+1,mm:i(k+1),mmm:g[e].shortMonths[k],mmmm:g[e].months[k],yy:String(l).slice(2),yyyy:l},n=d[a](c,b,m,e);return j.html(n).html()}e.addFormatter("default",function(a,b,c,d){return a.replace(/d{1,4}|m{1,4}|yy(?:yy)?|"[^"]*"|'[^']*'/g,function(a){return a in c?c[a]:a})}),e.addFormatter("prefixed",function(a,b,c,d){return a.replace(/%(d{1,4}|m{1,4}|yy(?:yy)?|"[^"]*"|'[^']*')/g,function(a,b){return b in c?c[b]:a})});function l(a){return parseInt(a,10)}function m(a,b){return a.getFullYear()===b.getFullYear()&&a.getMonth()==b.getMonth()&&a.getDate()==b.getDate()}function n(a){if(a!==b){if(a.constructor==Date)return a;if(typeof a=="string"){var c=a.split("-");if(c.length==3)return new Date(l(c[0]),l(c[1])-1,l(c[2]));if(!/^-?\d+$/.test(a))return;a=l(a)}var d=new Date;d.setDate(d.getDate()+a);return d}}function o(d,e){var i=this,j=new Date,o=j.getFullYear(),p=e.css,q=g[e.lang],r=a("#"+p.root),s=r.find("#"+p.title),t,u,v,w,x,y,z=d.attr("data-value")||e.value||d.val(),A=d.attr("min")||e.min,B=d.attr("max")||e.max,C,D;A===0&&(A="0"),z=n(z)||j,A=n(A||new Date(o+e.yearRange[0],1,1)),B=n(B||new Date(o+e.yearRange[1]+1,1,-1));if(!q)throw"Dateinput: invalid language: "+e.lang;if(d.attr("type")=="date"){var D=d.clone(),E=D.wrap("<div/>").parent().html(),F=a(E.replace(/type/i,"type=text data-orig-type"));e.value&&F.val(e.value),d.replaceWith(F),d=F}d.addClass(p.input);var G=d.add(i);if(!r.length){r=a("<div><div><a/><div/><a/></div><div><div/><div/></div></div>").hide().css({position:"absolute"}).attr("id",p.root),r.children().eq(0).attr("id",p.head).end().eq(1).attr("id",p.body).children().eq(0).attr("id",p.days).end().eq(1).attr("id",p.weeks).end().end().end().find("a").eq(0).attr("id",p.prev).end().eq(1).attr("id",p.next),s=r.find("#"+p.head).find("div").attr("id",p.title);if(e.selectors){var H=a("<select/>").attr("id",p.month),I=a("<select/>").attr("id",p.year);s.html(H.add(I))}var J=r.find("#"+p.days);for(var K=0;K<7;K++)J.append(a("<span/>").text(q.shortDays[(K+e.firstDay)%7]));a("body").append(r)}e.trigger&&(t=a("<a/>").attr("href","#").addClass(p.trigger).click(function(a){e.toggle?i.toggle():i.show();return a.preventDefault()}).insertAfter(d));var L=r.find("#"+p.weeks);I=r.find("#"+p.year),H=r.find("#"+p.month);function M(b,c,e){z=b,w=b.getFullYear(),x=b.getMonth(),y=b.getDate(),e||(e=a.Event("api")),e.type=="click"&&!a.browser.msie&&d.focus(),e.type="beforeChange",G.trigger(e,[b]);e.isDefaultPrevented()||(d.val(k(c.formatter,b,c.format,c.lang)),e.type="change",G.trigger(e),d.data("date",b),i.hide(e))}function N(b){b.type="onShow",G.trigger(b),a(document).on("keydown.d",function(b){if(b.ctrlKey)return!0;var c=b.keyCode;if(c==8||c==46){d.val("");return i.hide(b)}if(c==27||c==9)return i.hide(b);if(a(f).index(c)>=0){if(!C){i.show(b);return b.preventDefault()}var e=a("#"+p.weeks+" a"),g=a("."+p.focus),h=e.index(g);g.removeClass(p.focus);if(c==74||c==40)h+=7;else if(c==75||c==38)h-=7;else if(c==76||c==39)h+=1;else if(c==72||c==37)h-=1;h>41?(i.addMonth(),g=a("#"+p.weeks+" a:eq("+(h-42)+")")):h<0?(i.addMonth(-1),g=a("#"+p.weeks+" a:eq("+(h+42)+")")):g=e.eq(h),g.addClass(p.focus);return b.preventDefault()}if(c==34)return i.addMonth();if(c==33)return i.addMonth(-1);if(c==36)return i.today();c==13&&(a(b.target).is("select")||a("."+p.focus).click());return a([16,17,18,9]).index(c)>=0}),a(document).on("click.d",function(b){var c=b.target;!a(c).parents("#"+p.root).length&&c!=d[0]&&(!t||c!=t[0])&&i.hide(b)})}a.extend(i,{show:function(b){if(!(d.attr("readonly")||d.attr("disabled")||C)){b=b||a.Event(),b.type="onBeforeShow",G.trigger(b);if(b.isDefaultPrevented())return;a.each(c,function(){this.hide()}),C=!0,H.off("change").change(function(){i.setValue(l(I.val()),l(a(this).val()))}),I.off("change").change(function(){i.setValue(l(a(this).val()),l(H.val()))}),u=r.find("#"+p.prev).off("click").click(function(a){u.hasClass(p.disabled)||i.addMonth(-1);return!1}),v=r.find("#"+p.next).off("click").click(function(a){v.hasClass(p.disabled)||i.addMonth();return!1}),i.setValue(z);var f=d.offset();/iPad/i.test(navigator.userAgent)&&(f.top-=a(window).scrollTop()),r.css({top:f.top+d.outerHeight({margins:!0})+e.offset[0],left:f.left+e.offset[1]}),e.speed?r.show(e.speed,function(){N(b)}):(r.show(),N(b));return i}},setValue:function(c,d,f){var g=l(d)>=-1?new Date(l(c),l(d),l(f==b||isNaN(f)?1:f)):c||z;g<A?g=A:g>B&&(g=B),typeof c=="string"&&(g=n(c)),c=g.getFullYear(),d=g.getMonth(),f=g.getDate(),d==-1?(d=11,c--):d==12&&(d=0,c++);if(!C){M(g,e);return i}x=d,w=c,y=f;var k=new Date(c,d,1-e.firstDay),o=k.getDay(),r=h(c,d),t=h(c,d-1),D;if(e.selectors){H.empty(),a.each(q.months,function(b,d){A<new Date(c,b+1,1)&&B>new Date(c,b,0)&&H.append(a("<option/>").html(d).attr("value",b))}),I.empty();var E=j.getFullYear();for(var F=E+e.yearRange[0];F<E+e.yearRange[1];F++)A<new Date(F+1,0,1)&&B>new Date(F,0,0)&&I.append(a("<option/>").text(F));H.val(d),I.val(c)}else s.html(q.months[d]+" "+c);L.empty(),u.add(v).removeClass(p.disabled);for(var G=o?0:-7,J,K;G<(o?42:35);G++)J=a("<a/>"),G%7===0&&(D=a("<div/>").addClass(p.week),L.append(D)),G<o?(J.addClass(p.off),K=t-o+G+1,g=new Date(c,d-1,K)):G<o+r?(K=G-o+1,g=new Date(c,d,K),m(z,g)?J.attr("id",p.current).addClass(p.focus):m(j,g)&&J.attr("id",p.today)):(J.addClass(p.off),K=G-r-o+1,g=new Date(c,d+1,K)),A&&g<A&&J.add(u).addClass(p.disabled),B&&g>B&&J.add(v).addClass(p.disabled),J.attr("href","#"+K).text(K).data("date",g),D.append(J);L.find("a").click(function(b){var c=a(this);c.hasClass(p.disabled)||(a("#"+p.current).removeAttr("id"),c.attr("id",p.current),M(c.data("date"),e,b));return!1}),p.sunday&&L.find("."+p.week).each(function(){var b=e.firstDay?7-e.firstDay:0;a(this).children().slice(b,b+1).addClass(p.sunday)});return i},setMin:function(a,b){A=n(a),b&&z<A&&i.setValue(A);return i},setMax:function(a,b){B=n(a),b&&z>B&&i.setValue(B);return i},today:function(){return i.setValue(j)},addDay:function(a){return this.setValue(w,x,y+(a||1))},addMonth:function(a){var b=x+(a||1),c=h(w,b),d=y<=c?y:c;return this.setValue(w,b,d)},addYear:function(a){return this.setValue(w+(a||1),x,y)},destroy:function(){d.add(document).off("click.d keydown.d"),r.add(t).remove(),d.removeData("dateinput").removeClass(p.input),D&&d.replaceWith(D)},hide:function(b){if(C){b=a.Event(),b.type="onHide",G.trigger(b);if(b.isDefaultPrevented())return;a(document).off("click.d keydown.d"),r.hide(),C=!1}return i},toggle:function(){return i.isOpen()?i.hide():i.show()},getConf:function(){return e},getInput:function(){return d},getCalendar:function(){return r},getValue:function(a){return a?k(e.formatter,z,a,e.lang):z},isOpen:function(){return C}}),a.each(["onBeforeShow","onShow","change","onHide"],function(b,c){a.isFunction(e[c])&&a(i).on(c,e[c]),i[c]=function(b){b&&a(i).on(c,b);return i}}),e.editable||d.on("focus.d click.d",i.show).keydown(function(b){var c=b.keyCode;if(C||a(f).index(c)<0)(c==8||c==46)&&d.val("");else{i.show(b);return b.preventDefault()}return b.shiftKey||b.ctrlKey||b.altKey||c==9?!0:b.preventDefault()}),n(d.val())&&M(z,e)}a.expr[":"].date=function(b){var c=b.getAttribute("type");return c&&c=="date"||a(b).data("dateinput")},a.fn.dateinput=function(b){if(this.data("dateinput"))return this;b=a.extend(!0,{},e.conf,b),a.each(b.css,function(a,c){!c&&a!="prefix"&&(b.css[a]=(b.css.prefix||"")+(c||a))});var d;this.each(function(){var e=new o(a(this),b);c.push(e);var f=e.getInput().data("dateinput",e);d=d?d.add(f):f});return d?d:this}})(jQuery);
(function(a){a.tools=a.tools||{version:"v1.2.7"},a.tools.overlay={addEffect:function(a,b,d){c[a]=[b,d]},conf:{close:null,closeOnClick:!0,closeOnEsc:!0,closeSpeed:"fast",effect:"default",fixed:!a.browser.msie||a.browser.version>6,left:"center",load:!1,mask:null,oneInstance:!0,speed:"normal",target:null,top:"10%"}};var b=[],c={};a.tools.overlay.addEffect("default",function(b,c){var d=this.getConf(),e=a(window);d.fixed||(b.top+=e.scrollTop(),b.left+=e.scrollLeft()),b.position=d.fixed?"fixed":"absolute",this.getOverlay().css(b).fadeIn(d.speed,c)},function(a){this.getOverlay().fadeOut(this.getConf().closeSpeed,a)});function d(d,e){var f=this,g=d.add(f),h=a(window),i,j,k,l=a.tools.expose&&(e.mask||e.expose),m=Math.random().toString().slice(10);l&&(typeof l=="string"&&(l={color:l}),l.closeOnClick=l.closeOnEsc=!1);var n=e.target||d.attr("rel");j=n?a(n):null||d;if(!j.length)throw"Could not find Overlay: "+n;d&&d.index(j)==-1&&d.click(function(a){f.load(a);return a.preventDefault()}),a.extend(f,{load:function(d){if(f.isOpened())return f;var i=c[e.effect];if(!i)throw"Overlay: cannot find effect : \""+e.effect+"\"";e.oneInstance&&a.each(b,function(){this.close(d)}),d=d||a.Event(),d.type="onBeforeLoad",g.trigger(d);if(d.isDefaultPrevented())return f;k=!0,l&&a(j).expose(l);var n=e.top,o=e.left,p=j.outerWidth({margin:!0}),q=j.outerHeight({margin:!0});typeof n=="string"&&(n=n=="center"?Math.max((h.height()-q)/2,0):parseInt(n,10)/100*h.height()),o=="center"&&(o=Math.max((h.width()-p)/2,0)),i[0].call(f,{top:n,left:o},function(){k&&(d.type="onLoad",g.trigger(d))}),l&&e.closeOnClick&&a.mask.getMask().one("click",f.close),e.closeOnClick&&a(document).on("click."+m,function(b){a(b.target).parents(j).length||f.close(b)}),e.closeOnEsc&&a(document).on("keydown."+m,function(a){a.keyCode==27&&f.close(a)});return f},close:function(b){if(!f.isOpened())return f;b=b||a.Event(),b.type="onBeforeClose",g.trigger(b);if(!b.isDefaultPrevented()){k=!1,c[e.effect][1].call(f,function(){b.type="onClose",g.trigger(b)}),a(document).off("click."+m+" keydown."+m),l&&a.mask.close();return f}},getOverlay:function(){return j},getTrigger:function(){return d},getClosers:function(){return i},isOpened:function(){return k},getConf:function(){return e}}),a.each("onBeforeLoad,onStart,onLoad,onBeforeClose,onClose".split(","),function(b,c){a.isFunction(e[c])&&a(f).on(c,e[c]),f[c]=function(b){b&&a(f).on(c,b);return f}}),i=j.find(e.close||".close"),!i.length&&!e.close&&(i=a("<a class=\"close\"></a>"),j.prepend(i)),i.click(function(a){f.close(a)}),e.load&&f.load()}a.fn.overlay=function(c){var e=this.data("overlay");if(e)return e;a.isFunction(c)&&(c={onBeforeLoad:c}),c=a.extend(!0,{},a.tools.overlay.conf,c),this.each(function(){e=new d(a(this),c),b.push(e),a(this).data("overlay",e)});return c.api?e:this}})(jQuery);
(function(a){var b=a.tools.overlay,c=a(window);a.extend(b.conf,{start:{top:null,left:null},fadeInSpeed:"fast",zIndex:9999});function d(a){var b=a.offset();return{top:b.top+a.height()/2,left:b.left+a.width()/2}}var e=function(b,e){var f=this.getOverlay(),g=this.getConf(),h=this.getTrigger(),i=this,j=f.outerWidth({margin:!0}),k=f.data("img"),l=g.fixed?"fixed":"absolute";if(!k){var m=f.css("backgroundImage");if(!m)throw"background-image CSS property not set for overlay";m=m.slice(m.indexOf("(")+1,m.indexOf(")")).replace(/\"/g,""),f.css("backgroundImage","none"),k=a("<img src=\""+m+"\"/>"),k.css({border:0,display:"none"}).width(j),a("body").append(k),f.data("img",k)}var n=g.start.top||Math.round(c.height()/2),o=g.start.left||Math.round(c.width()/2);if(h){var p=d(h);n=p.top,o=p.left}g.fixed?(n-=c.scrollTop(),o-=c.scrollLeft()):(b.top+=c.scrollTop(),b.left+=c.scrollLeft()),k.css({position:"absolute",top:n,left:o,width:0,zIndex:g.zIndex}).show(),b.position=l,f.css(b),k.animate({top:b.top,left:b.left,width:j},g.speed,function(){f.css("zIndex",g.zIndex+1).fadeIn(g.fadeInSpeed,function(){i.isOpened()&&!a(this).index(f)?e.call():f.hide()})}).css("position",l)},f=function(b){var e=this.getOverlay().hide(),f=this.getConf(),g=this.getTrigger(),h=e.data("img"),i={top:f.start.top,left:f.start.left,width:0};g&&a.extend(i,d(g)),f.fixed&&h.css({position:"absolute"}).animate({top:"+="+c.scrollTop(),left:"+="+c.scrollLeft()},0),h.animate(i,f.closeSpeed,b)};b.addEffect("apple",e,f)})(jQuery);
(function(a){a.tools=a.tools||{version:"v1.2.7"};var b;b=a.tools.rangeinput={conf:{min:0,max:100,step:"any",steps:0,value:0,precision:undefined,vertical:0,keyboard:!0,progress:!1,speed:100,css:{input:"range",slider:"slider",progress:"progress",handle:"handle"}}};var c,d;a.fn.drag=function(b){document.ondragstart=function(){return!1},b=a.extend({x:!0,y:!0,drag:!0},b),c=c||a(document).on("mousedown mouseup",function(e){var f=a(e.target);if(e.type=="mousedown"&&f.data("drag")){var g=f.position(),h=e.pageX-g.left,i=e.pageY-g.top,j=!0;c.on("https://www.westernunion.com/etc/designs/wu-loggedout/mousemove.drag",function(a){var c=a.pageX-h,e=a.pageY-i,g={};b.x&&(g.left=c),b.y&&(g.top=e),j&&(f.trigger("dragStart"),j=!1),b.drag&&f.css(g),f.trigger("drag",[e,c]),d=f}),e.preventDefault()}else try{d&&d.trigger("dragEnd")}finally{c.off("https://www.westernunion.com/etc/designs/wu-loggedout/mousemove.drag"),d=null}});return this.data("drag",!0)};function e(a,b){var c=Math.pow(10,b);return Math.round(a*c)/c}function f(a,b){var c=parseInt(a.css(b),10);if(c)return c;var d=a[0].currentStyle;return d&&d.width&&parseInt(d.width,10)}function g(a){var b=a.data("events");return b&&b.onSlide}function h(b,c){var d=this,h=c.css,i=a("<div><div/><a href='#'/></div>").data("rangeinput",d),j,k,l,m,n;b.before(i);var o=i.addClass(h.slider).find("a").addClass(h.handle),p=i.find("div").addClass(h.progress);a.each("min,max,step,value".split(","),function(a,d){var e=b.attr(d);parseFloat(e)&&(c[d]=parseFloat(e,10))});var q=c.max-c.min,r=c.step=="any"?0:c.step,s=c.precision;s===undefined&&(s=r.toString().split("."),s=s.length===2?s[1].length:0);if(b.attr("type")=="range"){var t=b.clone().wrap("<div/>").parent().html(),u=a(t.replace(/type/i,"type=text data-orig-type"));u.val(c.value),b.replaceWith(u),b=u}b.addClass(h.input);var v=a(d).add(b),w=!0;function x(a,f,g,h){g===undefined?g=f/m*q:h&&(g-=c.min),r&&(g=Math.round(g/r)*r);if(f===undefined||r)f=g*m/q;if(isNaN(g))return d;f=Math.max(0,Math.min(f,m)),g=f/m*q;if(h||!j)g+=c.min;j&&(h?f=m-f:g=c.max-g),g=e(g,s);var i=a.type=="click";if(w&&k!==undefined&&!i){a.type="onSlide",v.trigger(a,[g,f]);if(a.isDefaultPrevented())return d}var l=i?c.speed:0,t=i?function(){a.type="change",v.trigger(a,[g])}:null;j?(o.animate({top:f},l,t),c.progress&&p.animate({height:m-f+o.height()/2},l)):(o.animate({left:f},l,t),c.progress&&p.animate({width:f+o.width()/2},l)),k=g,n=f,b.val(g);return d}a.extend(d,{getValue:function(){return k},setValue:function(b,c){y();return x(c||a.Event("api"),undefined,b,!0)},getConf:function(){return c},getProgress:function(){return p},getHandle:function(){return o},getInput:function(){return b},step:function(b,e){e=e||a.Event();var f=c.step=="any"?1:c.step;d.setValue(k+f*(b||1),e)},stepUp:function(a){return d.step(a||1)},stepDown:function(a){return d.step(-a||-1)}}),a.each("onSlide,change".split(","),function(b,e){a.isFunction(c[e])&&a(d).on(e,c[e]),d[e]=function(b){b&&a(d).on(e,b);return d}}),o.drag({drag:!1}).on("dragStart",function(){y(),w=g(a(d))||g(b)}).on("drag",function(a,c,d){if(b.is(":disabled"))return!1;x(a,j?c:d)}).on("dragEnd",function(a){a.isDefaultPrevented()||(a.type="change",v.trigger(a,[k]))}).click(function(a){return a.preventDefault()}),i.click(function(a){if(b.is(":disabled")||a.target==o[0])return a.preventDefault();y();var c=j?o.height()/2:o.width()/2;x(a,j?m-l-c+a.pageY:a.pageX-l-c)}),c.keyboard&&b.keydown(function(c){if(!b.attr("readonly")){var e=c.keyCode,f=a([75,76,38,33,39]).index(e)!=-1,g=a([74,72,40,34,37]).index(e)!=-1;if((f||g)&&!(c.shiftKey||c.altKey||c.ctrlKey)){f?d.step(e==33?10:1,c):g&&d.step(e==34?-10:-1,c);return c.preventDefault()}}}),b.blur(function(b){var c=a(this).val();c!==k&&d.setValue(c,b)}),a.extend(b[0],{stepUp:d.stepUp,stepDown:d.stepDown});function y(){j=c.vertical||f(i,"height")>f(i,"width"),j?(m=f(i,"height")-f(o,"height"),l=i.offset().top+m):(m=f(i,"width")-f(o,"width"),l=i.offset().left)}function z(){y(),d.setValue(c.value!==undefined?c.value:c.min)}z(),m||a(window).load(z)}a.expr[":"].range=function(b){var c=b.getAttribute("type");return c&&c=="range"||a(b).filter("input").data("rangeinput")},a.fn.rangeinput=function(c){if(this.data("rangeinput"))return this;c=a.extend(!0,{},b.conf,c);var d;this.each(function(){var b=new h(a(this),a.extend(!0,{},c)),e=b.getInput().data("rangeinput",b);d=d?d.add(e):e});return d?d:this}})(jQuery);
(function(a){a.tools=a.tools||{version:"v1.2.7"},a.tools.scrollable={conf:{activeClass:"active",circular:!1,clonedClass:"cloned",disabledClass:"disabled",easing:"swing",initialIndex:0,item:"> *",items:".items",keyboard:!0,mousewheel:!1,next:".next",prev:".prev",size:1,speed:400,vertical:!1,touch:!0,wheelSpeed:0}};function b(a,b){var c=parseInt(a.css(b),10);if(c)return c;var d=a[0].currentStyle;return d&&d.width&&parseInt(d.width,10)}function c(b,c){var d=a(c);return d.length<2?d:b.parent().find(c)}var d;function e(b,e){var f=this,g=b.add(f),h=b.children(),i=0,j=e.vertical;d||(d=f),h.length>1&&(h=a(e.items,b)),e.size>1&&(e.circular=!1),a.extend(f,{getConf:function(){return e},getIndex:function(){return i},getSize:function(){return f.getItems().size()},getNaviButtons:function(){return n.add(o)},getRoot:function(){return b},getItemWrap:function(){return h},getItems:function(){return h.find(e.item).not("."+e.clonedClass)},move:function(a,b){return f.seekTo(i+a,b)},next:function(a){return f.move(e.size,a)},prev:function(a){return f.move(-e.size,a)},begin:function(a){return f.seekTo(0,a)},end:function(a){return f.seekTo(f.getSize()-1,a)},focus:function(){d=f;return f},addItem:function(b){b=a(b),e.circular?(h.children().last().before(b),h.children().first().replaceWith(b.clone().addClass(e.clonedClass))):(h.append(b),o.removeClass("disabled")),g.trigger("onAddItem",[b]);return f},seekTo:function(b,c,k){b.jquery||(b*=1);if(e.circular&&b===0&&i==-1&&c!==0)return f;if(!e.circular&&b<0||b>f.getSize()||b<-1)return f;var l=b;b.jquery?b=f.getItems().index(b):l=f.getItems().eq(b);var m=a.Event("onBeforeSeek");if(!k){g.trigger(m,[b,c]);if(m.isDefaultPrevented()||!l.length)return f}var n=j?{top:-l.position().top}:{left:-l.position().left};i=b,d=f,c===undefined&&(c=e.speed),h.animate(n,c,e.easing,k||function(){g.trigger("onSeek",[b])});return f}}),a.each(["onBeforeSeek","onSeek","onAddItem"],function(b,c){a.isFunction(e[c])&&a(f).on(c,e[c]),f[c]=function(b){b&&a(f).on(c,b);return f}});if(e.circular){var k=f.getItems().slice(-1).clone().prependTo(h),l=f.getItems().eq(1).clone().appendTo(h);k.add(l).addClass(e.clonedClass),f.onBeforeSeek(function(a,b,c){if(!a.isDefaultPrevented()){if(b==-1){f.seekTo(k,c,function(){f.end(0)});return a.preventDefault()}b==f.getSize()&&f.seekTo(l,c,function(){f.begin(0)})}});var m=b.parents().add(b).filter(function(){if(a(this).css("display")==="none")return!0});m.length?(m.show(),f.seekTo(0,0,function(){}),m.hide()):f.seekTo(0,0,function(){})}var n=c(b,e.prev).click(function(a){a.stopPropagation(),f.prev()}),o=c(b,e.next).click(function(a){a.stopPropagation(),f.next()});e.circular||(f.onBeforeSeek(function(a,b){setTimeout(function(){a.isDefaultPrevented()||(n.toggleClass(e.disabledClass,b<=0),o.toggleClass(e.disabledClass,b>=f.getSize()-1))},1)}),e.initialIndex||n.addClass(e.disabledClass)),f.getSize()<2&&n.add(o).addClass(e.disabledClass),e.mousewheel&&a.fn.mousewheel&&b.mousewheel(function(a,b){if(e.mousewheel){f.move(b<0?1:-1,e.wheelSpeed||50);return!1}});if(e.touch){var p={};h[0].ontouchstart=function(a){var b=a.touches[0];p.x=b.clientX,p.y=b.clientY},h[0].ontouchmove=function(a){if(a.touches.length==1&&!h.is(":animated")){var b=a.touches[0],c=p.x-b.clientX,d=p.y-b.clientY;f[j&&d>0||!j&&c>0?"next":"prev"](),a.preventDefault()}}}e.keyboard&&a(document).on("keydown.scrollable",function(b){if(!(!e.keyboard||b.altKey||b.ctrlKey||b.metaKey||a(b.target).is(":input"))){if(e.keyboard!="static"&&d!=f)return;var c=b.keyCode;if(j&&(c==38||c==40)){f.move(c==38?-1:1);return b.preventDefault()}if(!j&&(c==37||c==39)){f.move(c==37?-1:1);return b.preventDefault()}}}),e.initialIndex&&f.seekTo(e.initialIndex,0,function(){})}a.fn.scrollable=function(b){var c=this.data("scrollable");if(c)return c;b=a.extend({},a.tools.scrollable.conf,b),this.each(function(){c=new e(a(this),b),a(this).data("scrollable",c)});return b.api?c:this}})(jQuery);
(function(a){var b=a.tools.scrollable;b.autoscroll={conf:{autoplay:!0,interval:3e3,autopause:!0}},a.fn.autoscroll=function(c){typeof c=="number"&&(c={interval:c});var d=a.extend({},b.autoscroll.conf,c),e;this.each(function(){var b=a(this).data("scrollable"),c=b.getRoot(),f,g=!1;function h(){f&&clearTimeout(f),f=setTimeout(function(){b.next()},d.interval)}b&&(e=b),b.play=function(){f||(g=!1,c.on("onSeek",h),h())},b.pause=function(){f=clearTimeout(f),c.off("onSeek",h)},b.resume=function(){g||b.play()},b.stop=function(){g=!0,b.pause()},d.autopause&&c.add(b.getNaviButtons()).hover(b.pause,b.resume),d.autoplay&&b.play()});return d.api?e:this}})(jQuery);
(function(a){var b=a.tools.scrollable;b.navigator={conf:{navi:".navi",naviItem:null,activeClass:"active",indexed:!1,idPrefix:null,history:!1}};function c(b,c){var d=a(c);return d.length<2?d:b.parent().find(c)}a.fn.navigator=function(d){typeof d=="string"&&(d={navi:d}),d=a.extend({},b.navigator.conf,d);var e;this.each(function(){var b=a(this).data("scrollable"),f=d.navi.jquery?d.navi:c(b.getRoot(),d.navi),g=b.getNaviButtons(),h=d.activeClass,i=d.history&&history.pushState,j=b.getConf().size;b&&(e=b),b.getNaviButtons=function(){return g.add(f)},i&&(history.pushState({i:0},""),a(window).on("popstate",function(a){var c=a.originalEvent.state;c&&b.seekTo(c.i)}));function k(a,c,d){b.seekTo(c),d.preventDefault(),i&&history.pushState({i:c},"")}function l(){return f.find(d.naviItem||"> *")}function m(b){var c=a("<"+(d.naviItem||"a")+"/>").click(function(c){k(a(this),b,c)});b===0&&c.addClass(h),d.indexed&&c.text(b+1),d.idPrefix&&c.attr("id",d.idPrefix+b);return c.appendTo(f)}l().length?l().each(function(b){a(this).click(function(c){k(a(this),b,c)})}):a.each(b.getItems(),function(a){a%j==0&&m(a)}),b.onBeforeSeek(function(a,b){setTimeout(function(){if(!a.isDefaultPrevented()){var c=b/j,d=l().eq(c);d.length&&l().removeClass(h).eq(c).addClass(h)}},1)}),b.onAddItem(function(a,c){var d=b.getItems().index(c);d%j==0&&m(d)})});return d.api?e:this}})(jQuery);
(function(a){a.tools=a.tools||{version:"v1.2.7"},a.tools.tabs={conf:{tabs:"a",current:"current",onBeforeClick:null,onClick:null,effect:"default",initialEffect:!1,initialIndex:0,event:"click",rotate:!1,slideUpSpeed:400,slideDownSpeed:400,history:!1},addEffect:function(a,c){b[a]=c}};var b={"default":function(a,b){this.getPanes().hide().eq(a).show(),b.call()},fade:function(a,b){var c=this.getConf(),d=c.fadeOutSpeed,e=this.getPanes();d?e.fadeOut(d):e.hide(),e.eq(a).fadeIn(c.fadeInSpeed,b)},slide:function(a,b){var c=this.getConf();this.getPanes().slideUp(c.slideUpSpeed),this.getPanes().eq(a).slideDown(c.slideDownSpeed,b)},ajax:function(a,b){this.getPanes().eq(0).load(this.getTabs().eq(a).attr("href"),b)}},c,d;a.tools.tabs.addEffect("horizontal",function(b,e){if(!c){var f=this.getPanes().eq(b),g=this.getCurrentPane();d||(d=this.getPanes().eq(0).width()),c=!0,f.show(),g.animate({width:0},{step:function(a){f.css("width",d-a)},complete:function(){a(this).hide(),e.call(),c=!1}}),g.length||(e.call(),c=!1)}});function e(c,d,e){var f=this,g=c.add(this),h=c.find(e.tabs),i=d.jquery?d:c.children(d),j;h.length||(h=c.children()),i.length||(i=c.parent().find(d)),i.length||(i=a(d)),a.extend(this,{click:function(d,i){var k=h.eq(d),l=!c.data("tabs");typeof d=="string"&&d.replace("#","")&&(k=h.filter("[href*=\""+d.replace("#","")+"\"]"),d=Math.max(h.index(k),0));if(e.rotate){var m=h.length-1;if(d<0)return f.click(m,i);if(d>m)return f.click(0,i)}if(!k.length){if(j>=0)return f;d=e.initialIndex,k=h.eq(d)}if(d===j)return f;i=i||a.Event(),i.type="onBeforeClick",g.trigger(i,[d]);if(!i.isDefaultPrevented()){var n=l?e.initialEffect&&e.effect||"default":e.effect;b[n].call(f,d,function(){j=d,i.type="onClick",g.trigger(i,[d])}),h.removeClass(e.current),k.addClass(e.current);return f}},getConf:function(){return e},getTabs:function(){return h},getPanes:function(){return i},getCurrentPane:function(){return i.eq(j)},getCurrentTab:function(){return h.eq(j)},getIndex:function(){return j},next:function(){return f.click(j+1)},prev:function(){return f.click(j-1)},destroy:function(){h.off(e.event).removeClass(e.current),i.find("a[href^=\"#\"]").off("click.T");return f}}),a.each("onBeforeClick,onClick".split(","),function(b,c){a.isFunction(e[c])&&a(f).on(c,e[c]),f[c]=function(b){b&&a(f).on(c,b);return f}}),e.history&&a.fn.history&&(a.tools.history.init(h),e.event="history"),h.each(function(b){a(this).on(e.event,function(a){f.click(b,a);return a.preventDefault()})}),i.find("a[href^=\"#\"]").on("click.T",function(b){f.click(a(this).attr("href"),b)}),location.hash&&e.tabs=="a"&&c.find("[href=\""+location.hash+"\"]").length?f.click(location.hash):(e.initialIndex===0||e.initialIndex>0)&&f.click(e.initialIndex)}a.fn.tabs=function(b,c){var d=this.data("tabs");d&&(d.destroy(),this.removeData("tabs")),a.isFunction(c)&&(c={onBeforeClick:c}),c=a.extend({},a.tools.tabs.conf,c),this.each(function(){d=new e(a(this),b,c),a(this).data("tabs",d)});return c.api?d:this}})(jQuery);
(function(a){var b;b=a.tools.tabs.slideshow={conf:{next:".forward",prev:".backward",disabledClass:"disabled",autoplay:!1,autopause:!0,interval:3e3,clickable:!0,api:!1}};function c(b,c){var d=this,e=b.add(this),f=b.data("tabs"),g,h=!0;function i(c){var d=a(c);return d.length<2?d:b.parent().find(c)}var j=i(c.next).click(function(){f.next()}),k=i(c.prev).click(function(){f.prev()});function l(){g=setTimeout(function(){f.next()},c.interval)}a.extend(d,{getTabs:function(){return f},getConf:function(){return c},play:function(){if(g)return d;var b=a.Event("onBeforePlay");e.trigger(b);if(b.isDefaultPrevented())return d;h=!1,e.trigger("onPlay"),e.on("onClick",l),l();return d},pause:function(){if(!g)return d;var b=a.Event("onBeforePause");e.trigger(b);if(b.isDefaultPrevented())return d;g=clearTimeout(g),e.trigger("onPause"),e.off("onClick",l);return d},resume:function(){h||d.play()},stop:function(){d.pause(),h=!0}}),a.each("onBeforePlay,onPlay,onBeforePause,onPause".split(","),function(b,e){a.isFunction(c[e])&&a(d).on(e,c[e]),d[e]=function(b){return a(d).on(e,b)}}),c.autopause&&f.getTabs().add(j).add(k).add(f.getPanes()).hover(d.pause,d.resume),c.autoplay&&d.play(),c.clickable&&f.getPanes().click(function(){f.next()});if(!f.getConf().rotate){var m=c.disabledClass;f.getIndex()||k.addClass(m),f.onBeforeClick(function(a,b){k.toggleClass(m,!b),j.toggleClass(m,b==f.getTabs().length-1)})}}a.fn.slideshow=function(d){var e=this.data("slideshow");if(e)return e;d=a.extend({},b.conf,d),this.each(function(){e=new c(a(this),d),a(this).data("slideshow",e)});return d.api?e:this}})(jQuery);
(function(a){a.tools=a.tools||{version:"v1.2.7"};var b;b=a.tools.expose={conf:{maskId:"exposeMask",loadSpeed:"slow",closeSpeed:"fast",closeOnClick:!0,closeOnEsc:!0,zIndex:9998,opacity:.8,startOpacity:0,color:"#fff",onLoad:null,onClose:null}};function c(){if(a.browser.msie){var b=a(document).height(),c=a(window).height();return[window.innerWidth||document.documentElement.clientWidth||document.body.clientWidth,b-c<20?c:b]}return[a(document).width(),a(document).height()]}function d(b){if(b)return b.call(a.mask)}var e,f,g,h,i;a.mask={load:function(j,k){if(g)return this;typeof j=="string"&&(j={color:j}),j=j||h,h=j=a.extend(a.extend({},b.conf),j),e=a("#"+j.maskId),e.length||(e=a("<div/>").attr("id",j.maskId),a("body").append(e));var l=c();e.css({position:"absolute",top:0,left:0,width:l[0],height:l[1],display:"none",opacity:j.startOpacity,zIndex:j.zIndex}),j.color&&e.css("backgroundColor",j.color);if(d(j.onBeforeLoad)===!1)return this;j.closeOnEsc&&a(document).on("https://www.westernunion.com/etc/designs/wu-loggedout/keydown.mask",function(b){b.keyCode==27&&a.mask.close(b)}),j.closeOnClick&&e.on("https://www.westernunion.com/etc/designs/wu-loggedout/click.mask",function(b){a.mask.close(b)}),a(window).on("https://www.westernunion.com/etc/designs/wu-loggedout/resize.mask",function(){a.mask.fit()}),k&&k.length&&(i=k.eq(0).css("zIndex"),a.each(k,function(){var b=a(this);/relative|absolute|fixed/i.test(b.css("position"))||b.css("position","relative")}),f=k.css({zIndex:Math.max(j.zIndex+1,i=="auto"?0:i)})),e.css({display:"block"}).fadeTo(j.loadSpeed,j.opacity,function(){a.mask.fit(),d(j.onLoad),g="full"}),g=!0;return this},close:function(){if(g){if(d(h.onBeforeClose)===!1)return this;e.fadeOut(h.closeSpeed,function(){d(h.onClose),f&&f.css({zIndex:i}),g=!1}),a(document).off("https://www.westernunion.com/etc/designs/wu-loggedout/keydown.mask"),e.off("https://www.westernunion.com/etc/designs/wu-loggedout/click.mask"),a(window).off("https://www.westernunion.com/etc/designs/wu-loggedout/resize.mask")}return this},fit:function(){if(g){var a=c();e.css({width:a[0],height:a[1]})}},getMask:function(){return e},isLoaded:function(a){return a?g=="full":g},getConf:function(){return h},getExposed:function(){return f}},a.fn.mask=function(b){a.mask.load(b);return this},a.fn.expose=function(b){a.mask.load(b,this);return this}})(jQuery);
(function(){var a=document.all,b="http://www.adobe.com/go/getflashplayer",c=typeof jQuery=="function",d=/(\d+)[^\d]+(\d+)[^\d]*(\d*)/,e={width:"100%",height:"100%",id:"_"+(""+Math.random()).slice(9),allowfullscreen:!0,allowscriptaccess:"always",quality:"high",version:[3,0],onFail:null,expressInstall:null,w3c:!1,cachebusting:!1};window.attachEvent&&window.attachEvent("onbeforeunload",function(){__flash_unloadHandler=function(){},__flash_savedUnloadHandler=function(){}});function f(a,b){if(b)for(var c in b)b.hasOwnProperty(c)&&(a[c]=b[c]);return a}function g(a,b){var c=[];for(var d in a)a.hasOwnProperty(d)&&(c[d]=b(a[d]));return c}window.flashembed=function(a,b,c){typeof a=="string"&&(a=document.getElementById(a.replace("#","")));if(a){typeof b=="string"&&(b={src:b});return new j(a,f(f({},e),b),c)}};var h=f(window.flashembed,{conf:e,getVersion:function(){var a,b;try{b=navigator.plugins["Shockwave Flash"].description.slice(16)}catch(c){try{a=new ActiveXObject("ShockwaveFlash.ShockwaveFlash.7"),b=a&&a.GetVariable("$version")}catch(e){try{a=new ActiveXObject("ShockwaveFlash.ShockwaveFlash.6"),b=a&&a.GetVariable("$version")}catch(f){}}}b=d.exec(b);return b?[b[1],b[3]]:[0,0]},asString:function(a){if(a===null||a===undefined)return null;var b=typeof a;b=="object"&&a.push&&(b="array");switch(b){case"string":a=a.replace(new RegExp("([\"\\\\])","g"),"\\$1"),a=a.replace(/^\s?(\d+\.?\d*)%/,"$1pct");return"\""+a+"\"";case"array":return"["+g(a,function(a){return h.asString(a)}).join(",")+"]";case"function":return"\"function()\"";case"object":var c=[];for(var d in a)a.hasOwnProperty(d)&&c.push("\""+d+"\":"+h.asString(a[d]));return"{"+c.join(",")+"}"}return String(a).replace(/\s/g," ").replace(/\'/g,"\"")},getHTML:function(b,c){b=f({},b);var d="<object width=\""+b.width+"\" height=\""+b.height+"\" id=\""+b.id+"\" name=\""+b.id+"\"";b.cachebusting&&(b.src+=(b.src.indexOf("?")!=-1?"&":"?")+Math.random()),b.w3c||!a?d+=" data=\""+b.src+"\" type=\"application/x-shockwave-flash\"":d+=" classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\"",d+=">";if(b.w3c||a)d+="<param name=\"movie\" value=\""+b.src+"\" />";b.width=b.height=b.id=b.w3c=b.src=null,b.onFail=b.version=b.expressInstall=null;for(var e in b)b[e]&&(d+="<param name=\""+e+"\" value=\""+b[e]+"\" />");var g="";if(c){for(var i in c)if(c[i]){var j=c[i];g+=i+"="+encodeURIComponent(/function|object/.test(typeof j)?h.asString(j):j)+"&"}g=g.slice(0,-1),d+="<param name=\"flashvars\" value='"+g+"' />"}d+="</object>";return d},isSupported:function(a){return i[0]>a[0]||i[0]==a[0]&&i[1]>=a[1]}}),i=h.getVersion();function j(c,d,e){if(h.isSupported(d.version))c.innerHTML=h.getHTML(d,e);else if(d.expressInstall&&h.isSupported([6,65]))c.innerHTML=h.getHTML(f(d,{src:d.expressInstall}),{MMredirectURL:location.href,MMplayerType:"PlugIn",MMdoctitle:document.title});else{c.innerHTML.replace(/\s/g,"")||(c.innerHTML="<h2>Flash version "+d.version+" or greater is required</h2><h3>"+(i[0]>0?"Your version is "+i:"You have no flash plugin installed")+"</h3>"+(c.tagName=="A"?"<p>Click here to download latest version</p>":"<p>Download latest version from <a href='"+b+"'>here</a></p>"),c.tagName=="A"&&(c.onclick=function(){location.href=b}));if(d.onFail){var g=d.onFail.call(this);typeof g=="string"&&(c.innerHTML=g)}}a&&(window[d.id]=document.getElementById(d.id)),f(this,{getRoot:function(){return c},getOptions:function(){return d},getConf:function(){return e},getApi:function(){return c.firstChild}})}c&&(jQuery.tools=jQuery.tools||{version:"v1.2.7"},jQuery.tools.flashembed={conf:e},jQuery.fn.flashembed=function(a,b){return this.each(function(){jQuery(this).data("flashembed",flashembed(this,a,b))})})})();
(function(a){var b,c,d,e;a.tools=a.tools||{version:"v1.2.7"},a.tools.history={init:function(g){e||(a.browser.msie&&a.browser.version<"8"?c||(c=a("<iframe/>").attr("src","javascript:false;").hide().get(0),a("body").append(c),setInterval(function(){var d=c.contentWindow.document,e=d.location.hash;b!==e&&a(window).trigger("hash",e)},100),f(location.hash||"#")):setInterval(function(){var c=location.hash;c!==b&&a(window).trigger("hash",c)},100),d=d?d.add(g):g,g.click(function(b){var d=a(this).attr("href");c&&f(d);if(d.slice(0,1)!="#"){location.href="#"+d;return b.preventDefault()}}),e=!0)}};function f(a){if(a){var b=c.contentWindow.document;b.open().close(),b.location.hash=a}}a(window).on("hash",function(c,e){e?d.filter(function(){var b=a(this).attr("href");return b==e||b==e.replace("#","")}).trigger("history",[e]):d.eq(0).trigger("history",[e]),b=e}),a.fn.history=function(b){a.tools.history.init(this);return this.on("history",b)}})(jQuery);
(function(a){a.fn.mousewheel=function(a){return this[a?"on":"trigger"]("wheel",a)},a.event.special.wheel={setup:function(){a.event.add(this,b,c,{})},teardown:function(){a.event.remove(this,b,c)}};var b=a.browser.mozilla?"DOMMouseScroll"+(a.browser.version<"1.9"?" mousemove":""):"mousewheel";function c(b){switch(b.type){case"mousemove":return a.extend(b.data,{clientX:b.clientX,clientY:b.clientY,pageX:b.pageX,pageY:b.pageY});case"DOMMouseScroll":a.extend(b,b.data),b.delta=-b.detail/3;break;case"mousewheel":b.delta=b.wheelDelta/120}b.type="wheel";return a.event.handle.call(this,b,b.delta)}})(jQuery);
(function(a){a.tools=a.tools||{version:"v1.2.7"},a.tools.tooltip={conf:{effect:"toggle",fadeOutSpeed:"fast",predelay:0,delay:30,opacity:1,tip:0,fadeIE:!1,position:["top","center"],offset:[0,0],relative:!1,cancelDefault:!0,events:{def:"mouseenter,mouseleave",input:"focus,blur",widget:"focus mouseenter,blur mouseleave",tooltip:"mouseenter,mouseleave"},layout:"<div/>",tipClass:"tooltip"},addEffect:function(a,c,d){b[a]=[c,d]}};var b={toggle:[function(a){var b=this.getConf(),c=this.getTip(),d=b.opacity;d<1&&c.css({opacity:d}),c.show(),a.call()},function(a){this.getTip().hide(),a.call()}],fade:[function(b){var c=this.getConf();!a.browser.msie||c.fadeIE?this.getTip().fadeTo(c.fadeInSpeed,c.opacity,b):(this.getTip().show(),b())},function(b){var c=this.getConf();!a.browser.msie||c.fadeIE?this.getTip().fadeOut(c.fadeOutSpeed,b):(this.getTip().hide(),b())}]};function c(b,c,d){var e=d.relative?b.position().top:b.offset().top,f=d.relative?b.position().left:b.offset().left,g=d.position[0];e-=c.outerHeight()-d.offset[0],f+=b.outerWidth()+d.offset[1],/iPad/i.test(navigator.userAgent)&&(e-=a(window).scrollTop());var h=c.outerHeight()+b.outerHeight();g=="center"&&(e+=h/2),g=="bottom"&&(e+=h),g=d.position[1];var i=c.outerWidth()+b.outerWidth();g=="center"&&(f-=i/2),g=="left"&&(f-=i);return{top:e,left:f}}function d(d,e){var f=this,g=d.add(f),h,i=0,j=0,k=d.attr("title"),l=d.attr("data-tooltip"),m=b[e.effect],n,o=d.is(":input"),p=o&&d.is(":checkbox, :radio, select, :button, :submit"),q=d.attr("type"),r=e.events[q]||e.events[o?p?"widget":"input":"def"];if(!m)throw"Nonexistent effect \""+e.effect+"\"";r=r.split(/,\s*/);if(r.length!=2)throw"Tooltip: bad events configuration for "+q;d.on(r[0],function(a){clearTimeout(i),e.predelay?j=setTimeout(function(){f.show(a)},e.predelay):f.show(a)}).on(r[1],function(a){clearTimeout(j),e.delay?i=setTimeout(function(){f.hide(a)},e.delay):f.hide(a)}),k&&e.cancelDefault&&(d.removeAttr("title"),d.data("title",k)),a.extend(f,{show:function(b){if(!h){l?h=a(l):e.tip?h=a(e.tip).eq(0):k?h=a(e.layout).addClass(e.tipClass).appendTo(document.body).hide().append(k):(h=d.next(),h.length||(h=d.parent().next()));if(!h.length)throw"Cannot find tooltip for "+d}if(f.isShown())return f;h.stop(!0,!0);var o=c(d,h,e);e.tip&&h.html(d.data("title")),b=a.Event(),b.type="onBeforeShow",g.trigger(b,[o]);if(b.isDefaultPrevented())return f;o=c(d,h,e),h.css({position:"absolute",top:o.top,left:o.left}),n=!0,m[0].call(f,function(){b.type="onShow",n="full",g.trigger(b)});var p=e.events.tooltip.split(/,\s*/);h.data("__set")||(h.off(p[0]).on(p[0],function(){clearTimeout(i),clearTimeout(j)}),p[1]&&!d.is("input:not(:checkbox, :radio), textarea")&&h.off(p[1]).on(p[1],function(a){a.relatedTarget!=d[0]&&d.trigger(r[1].split(" ")[0])}),e.tip||h.data("__set",!0));return f},hide:function(c){if(!h||!f.isShown())return f;c=a.Event(),c.type="onBeforeHide",g.trigger(c);if(!c.isDefaultPrevented()){n=!1,b[e.effect][1].call(f,function(){c.type="onHide",g.trigger(c)});return f}},isShown:function(a){return a?n=="full":n},getConf:function(){return e},getTip:function(){return h},getTrigger:function(){return d}}),a.each("onHide,onBeforeShow,onShow,onBeforeHide".split(","),function(b,c){a.isFunction(e[c])&&a(f).on(c,e[c]),f[c]=function(b){b&&a(f).on(c,b);return f}})}a.fn.tooltip=function(b){var c=this.data("tooltip");if(c)return c;b=a.extend(!0,{},a.tools.tooltip.conf,b),typeof b.position=="string"&&(b.position=b.position.split(/,?\s/)),this.each(function(){c=new d(a(this),b),a(this).data("tooltip",c)});return b.api?c:this}})(jQuery);
(function(a){var b=a.tools.tooltip;b.dynamic={conf:{classNames:"top right bottom left"}};function c(b){var c=a(window),d=c.width()+c.scrollLeft(),e=c.height()+c.scrollTop();return[b.offset().top<=c.scrollTop(),d<=b.offset().left+b.width(),e<=b.offset().top+b.height(),c.scrollLeft()>=b.offset().left]}function d(a){var b=a.length;while(b--)if(a[b])return!1;return!0}a.fn.dynamic=function(e){typeof e=="number"&&(e={speed:e}),e=a.extend({},b.dynamic.conf,e);var f=a.extend(!0,{},e),g=e.classNames.split(/\s/),h;this.each(function(){var b=a(this).tooltip().onBeforeShow(function(b,e){var i=this.getTip(),j=this.getConf();h||(h=[j.position[0],j.position[1],j.offset[0],j.offset[1],a.extend({},j)]),a.extend(j,h[4]),j.position=[h[0],h[1]],j.offset=[h[2],h[3]],i.css({visibility:"hidden",position:"absolute",top:e.top,left:e.left}).show();var k=a.extend(!0,{},f),l=c(i);if(!d(l)){l[2]&&(a.extend(j,k.top),j.position[0]="top",i.addClass(g[0])),l[3]&&(a.extend(j,k.right),j.position[1]="right",i.addClass(g[1])),l[0]&&(a.extend(j,k.bottom),j.position[0]="bottom",i.addClass(g[2])),l[1]&&(a.extend(j,k.left),j.position[1]="left",i.addClass(g[3]));if(l[0]||l[2])j.offset[0]*=-1;if(l[1]||l[3])j.offset[1]*=-1}i.css({visibility:"visible"}).hide()});b.onBeforeShow(function(){var a=this.getConf(),b=this.getTip();setTimeout(function(){a.position=[h[0],h[1]],a.offset=[h[2],h[3]]},0)}),b.onHide(function(){var a=this.getTip();a.removeClass(e.classNames)}),ret=b});return e.api?ret:this}})(jQuery);
(function(a){var b=a.tools.tooltip;a.extend(b.conf,{direction:"up",bounce:!1,slideOffset:10,slideInSpeed:200,slideOutSpeed:200,slideFade:!a.browser.msie});var c={up:["-","top"],down:["+","top"],left:["-","left"],right:["+","left"]};b.addEffect("slide",function(a){var b=this.getConf(),d=this.getTip(),e=b.slideFade?{opacity:b.opacity}:{},f=c[b.direction]||c.up;e[f[1]]=f[0]+"="+b.slideOffset,b.slideFade&&d.css({opacity:0}),d.show().animate(e,b.slideInSpeed,a)},function(b){var d=this.getConf(),e=d.slideOffset,f=d.slideFade?{opacity:0}:{},g=c[d.direction]||c.up,h=""+g[0];d.bounce&&(h=h=="+"?"-":"+"),f[g[1]]=h+"="+e,this.getTip().animate(f,d.slideOutSpeed,function(){a(this).hide(),b.call()})})})(jQuery);
(function(a){a.tools=a.tools||{version:"v1.2.7"};var b=/\[type=([a-z]+)\]/,c=/^-?[0-9]*(\.[0-9]+)?$/,d=a.tools.dateinput,e=/^([a-z0-9_\.\-\+]+)@([\da-z\.\-]+)\.([a-z\.]{2,6})$/i,f=/^(https?:\/\/)?[\da-z\.\-]+\.[a-z\.]{2,6}[#&+_\?\/\w \.\-=]*$/i,g;g=a.tools.validator={conf:{grouped:!1,effect:"default",errorClass:"invalid",inputEvent:null,errorInputEvent:"keyup",formEvent:"submit",lang:"en",message:"<div/>",messageAttr:"data-message",messageClass:"error",offset:[0,0],position:"center right",singleError:!1,speed:"normal"},messages:{"*":{en:"Please correct this value"}},localize:function(b,c){a.each(c,function(a,c){g.messages[a]=g.messages[a]||{},g.messages[a][b]=c})},localizeFn:function(b,c){g.messages[b]=g.messages[b]||{},a.extend(g.messages[b],c)},fn:function(c,d,e){a.isFunction(d)?e=d:(typeof d=="string"&&(d={en:d}),this.messages[c.key||c]=d);var f=b.exec(c);f&&(c=i(f[1])),j.push([c,e])},addEffect:function(a,b,c){k[a]=[b,c]}};function h(b,c,d){c=a(c).first()||c;var e=b.offset().top,f=b.offset().left,g=d.position.split(/,?\s+/),h=g[0],i=g[1];e-=c.outerHeight()-d.offset[0],f+=b.outerWidth()+d.offset[1],/iPad/i.test(navigator.userAgent)&&(e-=a(window).scrollTop());var j=c.outerHeight()+b.outerHeight();h=="center"&&(e+=j/2),h=="bottom"&&(e+=j);var k=b.outerWidth();i=="center"&&(f-=(k+c.outerWidth())/2),i=="left"&&(f-=k);return{top:e,left:f}}function i(a){function b(){return this.getAttribute("type")==a}b.key="[type=\""+a+"\"]";return b}var j=[],k={"default":[function(b){var c=this.getConf();a.each(b,function(b,d){var e=d.input;e.addClass(c.errorClass);var f=e.data("https://www.westernunion.com/etc/designs/wu-loggedout/msg.el");f||(f=a(c.message).addClass(c.messageClass).appendTo(document.body),e.data("https://www.westernunion.com/etc/designs/wu-loggedout/msg.el",f)),f.css({visibility:"hidden"}).find("p").remove(),a.each(d.messages,function(b,c){a("<p/>").html(c).appendTo(f)}),f.outerWidth()==f.parent().width()&&f.add(f.find("p")).css({display:"inline"});var g=h(e,f,c);f.css({visibility:"visible",position:"absolute",top:g.top,left:g.left}).fadeIn(c.speed)})},function(b){var c=this.getConf();b.removeClass(c.errorClass).each(function(){var b=a(this).data("https://www.westernunion.com/etc/designs/wu-loggedout/msg.el");b&&b.css({visibility:"hidden"})})}]};a.each("email,url,number".split(","),function(b,c){a.expr[":"][c]=function(a){return a.getAttribute("type")===c}}),a.fn.oninvalid=function(a){return this[a?"on":"trigger"]("OI",a)},g.fn(":email","Please enter a valid email address",function(a,b){return!b||e.test(b)}),g.fn(":url","Please enter a valid URL",function(a,b){return!b||f.test(b)}),g.fn(":number","Please enter a numeric value.",function(a,b){return c.test(b)}),g.fn("[max]","Please enter a value no larger than $1",function(a,b){if(b===""||d&&a.is(":date"))return!0;var c=a.attr("max");return parseFloat(b)<=parseFloat(c)?!0:[c]}),g.fn("[min]","Please enter a value of at least $1",function(a,b){if(b===""||d&&a.is(":date"))return!0;var c=a.attr("min");return parseFloat(b)>=parseFloat(c)?!0:[c]}),g.fn("[required]","Please complete this mandatory field.",function(a,b){if(a.is(":checkbox"))return a.is(":checked");return b}),g.fn("[pattern]",function(a,b){return b===""||(new RegExp("^"+a.attr("pattern")+"$")).test(b)}),g.fn(":radio","Please select an option.",function(b){var c=!1,d=a("[name='"+b.attr("name")+"']").each(function(b,d){a(d).is(":checked")&&(c=!0)});return c?!0:!1});function l(b,c,e){var f=this,i=c.add(f);b=b.not(":button, :image, :reset, :submit"),c.attr("novalidate","novalidate");function l(b,c,d){if(e.grouped||!b.length){var f;if(d===!1||a.isArray(d)){f=g.messages[c.key||c]||g.messages["*"],f=f[e.lang]||g.messages["*"].en;var h=f.match(/\$\d/g);h&&a.isArray(d)&&a.each(h,function(a){f=f.replace(this,d[a])})}else f=d[e.lang]||d;b.push(f)}}a.extend(f,{getConf:function(){return e},getForm:function(){return c},getInputs:function(){return b},reflow:function(){b.each(function(){var b=a(this),c=b.data("https://www.westernunion.com/etc/designs/wu-loggedout/msg.el");if(c){var d=h(b,c,e);c.css({top:d.top,left:d.left})}});return f},invalidate:function(c,d){if(!d){var g=[];a.each(c,function(a,c){var d=b.filter("[name='"+a+"']");d.length&&(d.trigger("OI",[c]),g.push({input:d,messages:[c]}))}),c=g,d=a.Event()}d.type="onFail",i.trigger(d,[c]),d.isDefaultPrevented()||k[e.effect][0].call(f,c,d);return f},reset:function(c){c=c||b,c.removeClass(e.errorClass).each(function(){var b=a(this).data("https://www.westernunion.com/etc/designs/wu-loggedout/msg.el");b&&(b.remove(),a(this).data("https://www.westernunion.com/etc/designs/wu-loggedout/msg.el",null))}).off(e.errorInputEvent+".v");return f},destroy:function(){c.off(e.formEvent+".V reset.V"),b.off(e.inputEvent+".V change.V");return f.reset()},checkValidity:function(c,g){c=c||b,c=c.not(":disabled");var h={};c=c.filter(function(){var b=a(this).attr("name");if(!h[b]){h[b]=!0;return a(this)}});if(!c.length)return!0;g=g||a.Event(),g.type="onBeforeValidate",i.trigger(g,[c]);if(g.isDefaultPrevented())return g.result;var m=[];c.each(function(){var b=[],c=a(this).data("messages",b),h=d&&c.is(":date")?"onHide.v":e.errorInputEvent+".v";c.off(h),a.each(j,function(){var a=this,d=a[0];if(c.filter(d).length){var h=a[1].call(f,c,c.val());if(h!==!0){g.type="onBeforeFail",i.trigger(g,[c,d]);if(g.isDefaultPrevented())return!1;var j=c.attr(e.messageAttr);if(j){b=[j];return!1}l(b,d,h)}}}),b.length&&(m.push({input:c,messages:b}),c.trigger("OI",[b]),e.errorInputEvent&&c.on(h,function(a){f.checkValidity(c,a)}));if(e.singleError&&m.length)return!1});var n=k[e.effect];if(!n)throw"Validator: cannot find effect \""+e.effect+"\"";if(m.length){f.invalidate(m,g);return!1}n[1].call(f,c,g),g.type="onSuccess",i.trigger(g,[c]),c.off(e.errorInputEvent+".v");return!0}}),a.each("onBeforeValidate,onBeforeFail,onFail,onSuccess".split(","),function(b,c){a.isFunction(e[c])&&a(f).on(c,e[c]),f[c]=function(b){b&&a(f).on(c,b);return f}}),e.formEvent&&c.on(e.formEvent+".V",function(a){if(!f.checkValidity(null,a))return a.preventDefault();a.target=c,a.type=e.formEvent}),c.on("reset.V",function(){f.reset()}),b[0]&&b[0].validity&&b.each(function(){this.oninvalid=function(){return!1}}),c[0]&&(c[0].checkValidity=f.checkValidity),e.inputEvent&&b.on(e.inputEvent+".V",function(b){f.checkValidity(a(this),b)}),b.filter(":checkbox, select").filter("[required]").on("change.V",function(b){var c=a(this);(this.checked||c.is("select")&&a(this).val())&&k[e.effect][1].call(f,c,b)}),b.filter(":radio[required]").on("change.V",function(b){var c=a("[name='"+a(b.srcElement).attr("name")+"']");c!=null&&c.length!=0&&f.checkValidity(c,b)}),a(window).resize(function(){f.reflow()})}a.fn.validator=function(b){var c=this.data("validator");c&&(c.destroy(),this.removeData("validator")),b=a.extend(!0,{},g.conf,b);if(this.is("form"))return this.each(function(){var d=a(this);c=new l(d.find(":input"),d,b),d.data("validator",c)});c=new l(this,this.eq(0).closest("form"),b);return this.data("validator",c)}})(jQuery);
/*!
 * jQuery Cookie Plugin
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2011, Klaus Hartl
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.opensource.org/licenses/GPL-2.0
 */
(function($) {
    $.cookie = function(key, value, options) {

        // key and at least value given, set cookie...
        if (arguments.length > 1 && (!/Object/.test(Object.prototype.toString.call(value)) || value === null || value === undefined)) {
            options = $.extend({}, options);

            if (value === null || value === undefined) {
                options.expires = -1;
            }

            if (typeof options.expires === 'number') {
                var days = options.expires, t = options.expires = new Date();
                t.setDate(t.getDate() + days);
            }

            value = String(value);

            return (document.cookie = [
                encodeURIComponent(key), '=', options.raw ? value : encodeURIComponent(value),
                options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
                options.path    ? '; path=' + options.path : '',
                options.domain  ? '; domain=' + options.domain : '',
                options.secure  ? '; secure' : ''
            ].join(''));
        }

        // key and possibly options given, get cookie...
        options = value || {};
        var decode = options.raw ? function(s) { return s; } : decodeURIComponent;

        var pairs = document.cookie.split('; ');
        for (var i = 0, pair; pair = pairs[i] && pairs[i].split('='); i++) {
            if (decode(pair[0]) === key) return decode(pair[1] || ''); // IE saves cookies with empty string as "c; ", e.g. without "=" as opposed to EOMB, thus pair[1] may be undefined
        }
        return null;
    };
})(jQuery);
/** ** Megamenu Script * */


$(document).ready(function () {

	if($("nav").hasClass("app_nav")){
	        $("#showCookieCompliance").fadeIn("slow");
	        setTimeout(function(){
	        	$("#showCookieCompliance").fadeOut("slow");
	        	//creating marketing by default if it doesn't exist already
	        	if(!hasCookie("CookieOptIn")){
	        		setComplianceCookie("CKTXNL+CKPERF+CKMKTG");
	        	}

	        }, 10000);
	               //$("#showCookieCompliance").css("display", "none");
    }

	/*RTE Pop up function implementation*/

    var url="";
    	screenCheck = screen.width;
     $("a.Small, a.Medium, a.Big, a.Custom ").click(function(){
           url=$(this).attr("href");
           url=url+"?wcmmode=disabled";
           var height=$(this).attr("height");
           var width=$(this).attr("width");
           var left = (screen.width/2)-(width/2);
           var top = (screen.height/2)-(height/2);

           var style=$(this).attr("class");
           window.open(url,style,"directories=0,titlebar=0,toolbar=0,location=0,status=0,menubar=0,scrollbars=no,resizable=no,width="+height+",height="+width+",top="+top+",left="+left);
           url="";
           return false;
     });

	/* Left navigation toggle implementation */
	/*
    $('.leftnavigation>ul>li>a').click(function(e){
		e.preventDefault();
        $('.leftnavigation>ul>li').removeClass('selected').addClass('notselected');
        $(this).parent().removeClass('notselected').addClass('selected');
    });*/

	//dropdown fix
    setTimeout(function(){
      $('select').each(function(){
                $this = $(this);
                if($this.children().length == 0){
                   $(this).closest("div").children().find(".current").first().text("");
                }
    });},500);

    /* adding class for ie8 rounded corners fix */

   $('ul.megamenu li:first a').addClass('css3Style_ie');


/* country language drop down redirection start */
	 // dropdown script
    if(document.getElementById("defaultcountry")) {
	 $("#preferredCountry").val($('#defaultcountry').val().toLowerCase());
    $("#preferredLanguage").val($('#defaultlanguage').val().toLowerCase());
    $("#SMO_shoppingCountryTo").val($('#defaultcountry').val().toLowerCase());
    }


    $("#preferredCountry,#preferredLanguage").change(function(){
    $("#countryCode").val($('#preferredCountry').val());
    if($('#preferredLanguage').val() != null){

    $("#languageCode").val($('#preferredLanguage').val());

    }
        else{

            $("#languageCode").val($("#onelanguageCode").val());

        }

    var country = $("#countryCode").val().toUpperCase();
    var language = $("#languageCode").val().toLowerCase();

    setCookie("WUCountryCookie_",country,90,"https://www.westernunion.com/etc/designs/wu-loggedout/wu.com");
    setCookie("WULanguageCookie_",language,90,"https://www.westernunion.com/etc/designs/wu-loggedout/wu.com");
    var queryURL = "path:"+$('#queryPath').val()+ " " + $("#countryCode").val() + " "+$("#languageCode").val();
    var currResource = $("#currResource").val()+".countrylangquery.json";

    $.ajax({
    type: "GET",
    data: { query:queryURL, cols:"RedirectionURL" },
    url: currResource,
	dataType:"json",
	success: function (data) {

    var redirectURL = data.hits[0].RedirectionURL;

    if(redirectURL){
    window.location.replace(redirectURL);
    }
    else{


    $("#countryLanguageHeaderForm")[0].submit();
    }

   }
});
    });


// Add a custom common DD
//$("select, .selectBox").selectbox();

function setCookie(c_name,value,exdays,domain)
{
    var exdate=new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString())+";domain=."+domain+";path=/";
    document.cookie=c_name + "=" + c_value;
    return;
}


/* country language drop down redirection end */

	/*var count = $("ul.megamenu li").length;

	if((count == 1)||(count == 2)||(count == 3)){
	$("ul.megamenu").css("width", count * 200);
	var w = ($("ul.megamenu").width() / count);

	            }

	if(count == 4){
	                $("ul.megamenu").css("width", count * 180);
	            var w = ($("ul.megamenu").width() / count);
	            $("ul.megamenu li").css("width", w + "px");
	}
	if(count == 5){
	                $("ul.megamenu").css("width", count * 144);
	            var w = ($("ul.megamenu").width() / count);
	            $("ul.megamenu li").css("width", w + "px");
	}*/

    $(".changeCountry").change(function () {
        window.location.href = '#';
    });


        /* Home Promo slide width script */
    $(window).resize(function(){
        if($(window).width() >= 768) {
            var findTotalHomePromoSlide = $(".promo-marketing ul").children().length;
            var findTotalwidthHomePromo = $(".promo-marketing ul").width();
            var findParentWidthHomePromo = $(".promo-marketing ul").offsetParent().width();
            var findPercentFromHomePromo = Math.round(100*findTotalwidthHomePromo/findParentWidthHomePromo);
            var findSlideWidth = Math.round(findPercentFromHomePromo/findTotalHomePromoSlide);
            $(".promo-marketing ul li").attr("style","width:"+findSlideWidth+"%");
        } else if(($(window).width() <=768 && $(window).width() > 480) || $(window).width() <=480) {
            /* Remove the width */
            $(".promo-marketing ul li").css("width","");
        }
		/*if ($(window).width() < 1023 && $(window).width() >= 768) {
            var p = 0;
            $("ul.megamenu li").each(function () {
                p += $(this).width();
            });
            var count = $("ul.megamenu li").length;
            $("ul.megamenu").css("width", (p + (count * 2))-1);
        }*/
    }).resize();

	/* For device detection */

    var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};



    if( isMobile.Android() ) { $(".blue.button").attr("style","background: url(/etc/designs/wu/clientlibs/images/icons/button-arrow.png) right 12px no-repeat,-webkit-gradient(linear, 0% 0%, 0% 100%, from(#1599ba), to(#066690) );background: url(/etc/designs/wu/clientlibs/images/icons/button-arrow.png) right 12px no-repeat,-webkit-linear-gradient(top, #1599ba, #066690);background: url(/etc/designs/wu/clientlibs/images/icons/button-arrow.png) right 12px no-repeat, -moz-linear-gradient(top, #1599ba, #066690);background: url(/etc/designs/wu/clientlibs/images/icons/button-arrow.png) right 12px no-repeat,-ms-linear-gradient(top, #1599ba, #066690);");
                               $("#homeForm .blue.button").attr("style","background:-webkit-gradient(linear, 0% 0%, 0% 100%, from(#2e9fbc), to(#066791) );background: -webkit-linear-gradient(top, #2e9fbc, #066791);background: -moz-linear-gradient(top, #2e9fbc, #066791);   background: -ms-linear-gradient(top, #2e9fbc, #066791);");
                               $("span.rteBlueBTN").attr("style","white-space:nowrap;background: url(/etc/designs/wu/clientlibs/images/icons/button-arrow.png) right 14px no-repeat,-webkit-gradient(linear, 0% 0%, 0% 100%, from(#1599ba), to(#066690) );background: url(/etc/designs/wu/clientlibs/images/icons/button-arrow.png) right 14px no-repeat,-webkit-linear-gradient(top, #1599ba, #066690);background: url(/etc/designs/wu/clientlibs/images/icons/button-arrow.png) right 14px no-repeat, -moz-linear-gradient(top, #1599ba, #066690);background: url(/etc/designs/wu/clientlibs/images/icons/button-arrow.png) right 14px no-repeat,-ms-linear-gradient(top, #1599ba, #066690)");
                              $(".rteSmallBlueBTN").attr("style","white-space:nowrap;background: url(/etc/designs/wu/clientlibs/images/icons/button-arrow.png) right 7px no-repeat,-webkit-gradient(linear, 0% 0%, 0% 100%, from(#1599ba), to(#066690) );background: url(/etc/designs/wu/clientlibs/images/icons/button-arrow.png) right 7px no-repeat,-webkit-linear-gradient(top, #1599ba, #066690);background: url(/etc/designs/wu/clientlibs/images/icons/button-arrow.png) right 7px no-repeat, -moz-linear-gradient(top, #1599ba, #066690);background: url(/etc/designs/wu/clientlibs/images/icons/button-arrow.png) right 7px no-repeat,-ms-linear-gradient(top, #1599ba, #066690)");

}
    if( isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Android() || isMobile.Windows()) {
    //if($(window).width() < 1020) {
		$('#homeForm').removeClass("custom");
        $('.language-bar form#countryLanguageHeaderForm').removeClass("custom");
        //$('#countryLanguageHeaderForm').addClass("app_form");


    }



    /*new changes for Cheetah Mail start*/
        if (!$.support.placeholder) {

        var active = document.activeElement;

            $('input').focus(function() {
            if ($(this).attr('placeholder') != '' && $(this).val() == $(this).attr('placeholder')) {
                $(this).val('').removeClass('hasPlaceholder');
            }
        }).blur(function() {
            if ($(this).attr('placeholder') != '' && ($(this).val() == '' || $(this).val() == $(this).attr('placeholder'))) {
                $(this).val($(this).attr('placeholder')).addClass('hasPlaceholder');
            }
        });
        $('input').blur();

        $(active).focus();

    }


        /*new changes for Cheetah Mail end*/

    /* Rounded corner script for IE8 */
    	if (window.PIE) {
    		$('.css3Style_ie,.rteBlueBTN,.rteSmallBlueBTN').each(function() {
                PIE.attach(this);
            });
    	}
    /* End of rounded corner script for IE8 */


//script for accordian in send money page
    var firstEleHeight = $(".accordion-item:first-child .accordion-wrap .innerWrap .accordContent").height();
    $(".accordion-item:first-child .accordion-wrap .innerWrap").css({ height: firstEleHeight + 20 });
    $(".accordion-item:first-child .accordion-wrap h3 .ui-icon-plus").css({ 'background-position': "0px 0px" });

    $(".accordion-wrap h3").click(function (e) {

       $(this).find('.ui-icon-plus').css({ 'background-position': "0px 0px" });
        var acc_ht = $(this).parent('.accordion-wrap').find('.accordContent').height();
        $(this).parent('.accordion-wrap').find('.innerWrap').css("height", acc_ht + 20);
        $(this).parents('.accordion-item').siblings().find(".innerWrap").animate({ height: 0 }, 600);
        $(this).parents('.accordion-item').siblings().find(".ui-icon-plus").css({ 'background-position': "0px -24px" });
    });



});
/** ** END Megamenu Script * */




function refreshCaptcha(reqPath){

    $.getJSON(reqPath, function(data) {

        document.getElementById("cscaptchaimg").src=data.img;
        document.getElementById("cssd").value=data.cssd;

    });
}
$(function(){
$(window).resize(function(){

        if($(window).width() < 768) {
            if($(".language-bar div.custom").length > 0){
                //alert("yo");
            $("form.app_form").css("background","none");
            $("form#countryLanguageHeaderForm").removeClass("app_form");

        }
        }


    });

	 $(window).resize(function(){
    	if($(window).width() >= 768 && $(window).width() <= 1024) {
		if($(".language-bar div.custom").length > 0){
            //alert("yo");
		//$("form.app_form").css("background","none");
            if($("form#countryLanguageHeaderForm").hasClass("custom")){
				//alert("yo");
        	$("form#countryLanguageHeaderForm").removeClass("app_form");
            }
    	}
    	}
     });
   if($("html").hasClass("lt-ie9")){
		//$(document).foundation('dropdown', 'off');
        $('#homeForm').removeClass("custom");
        $('.language-bar form#countryLanguageHeaderForm').removeClass("custom");
        //$('select').selectBox();
        if($(".custom.dropdown.changeCountry.app_select").length > 0){
		$("#countryLanguageHeaderForm .custom.dropdown.changeCountry.app_select").remove();
            $("#homeForm .custom.dropdown.app_select").remove();
			}

    }/*else{
	$('#homeForm').removeClass("custom");
        $('.language-bar form#countryLanguageHeaderForm').removeClass("custom");
        $('select').selectBox();
    if($(".custom.dropdown.changeCountry.app_select").length > 0){
		$(".custom.dropdown.changeCountry.app_select").empty();
			}
		}*/
    if($("html").find("ul.selectBox-dropdown-menu").hasClass("changeCountry-selectBox-dropdown-menu")){
		$("ul.selectBox-dropdown-menu.changeCountry-selectBox-dropdown-menu").attr("id","headerCountry");
    }
    $(".mobile-menu").click(function(){
		$(".close-menu-padding").css("display","none");
        $(".mobile-menu-list").slideToggle(500);});

	/*$('.mobile-menu a.app_anch').click(function(e){
        e.preventDefault();
    $('.mobile-menu-list').slideDown(100);
  });*/
  /*$('.mobile-menu-list .close-menu-padding a').click(function(e){
      e.preventDefault();
    $('.mobile-menu-list').slideUp(100);
  });*/
    /*$('.mobile-menu a.app_anch').click(function(e){
          e.preventDefault();
        $('.mobile-menu-list').slideUp(100);
      });*/


});


function setComplianceCookie(c_value) {


	var email = new Array();
	var domain=window.location.hostname;

	var c_name = "CookieOptIn";
	var exdays = 1;
	var exdate = new Date();
	exdate.setDate(exdate.getDate() + exdays);
	var c_value = c_value
			+ ((exdays == null) ? "" : "; expires=" + exdate.toUTCString()) + "; path=/";
	document.cookie = c_name + "=" + c_value ;

}

function hasCookie(cookie_name) {

	if (document.cookie) {
		var index = document.cookie.indexOf(cookie_name);
		if (index != -1) {
			return true;

		}

	}
	return false;
}

$(window).load(function(){
//$(".wu-footer-quicklink").eq(1).hide();
//$(".wu-footer-copywrite").eq(1).hide();  
}); 
(function(a){a.tools=a.tools||{version:"v1.2.7"},a.tools.tabs={conf:{tabs:"a",current:"current",onBeforeClick:null,onClick:null,effect:"default",initialEffect:!1,initialIndex:0,event:"click",rotate:!1,slideUpSpeed:400,slideDownSpeed:400,history:!1},addEffect:function(a,c){b[a]=c}};var b={"default":function(a,b){this.getPanes().hide().eq(a).show(),b.call()},fade:function(a,b){var c=this.getConf(),d=c.fadeOutSpeed,e=this.getPanes();d?e.fadeOut(d):e.hide(),e.eq(a).fadeIn(c.fadeInSpeed,b)},slide:function(a,b){var c=this.getConf();this.getPanes().slideUp(c.slideUpSpeed),this.getPanes().eq(a).slideDown(c.slideDownSpeed,b)},ajax:function(a,b){this.getPanes().eq(0).load(this.getTabs().eq(a).attr("href"),b)}},c,d;a.tools.tabs.addEffect("horizontal",function(b,e){if(!c){var f=this.getPanes().eq(b),g=this.getCurrentPane();d||(d=this.getPanes().eq(0).width()),c=!0,f.show(),g.animate({width:0},{step:function(a){f.css("width",d-a)},complete:function(){a(this).hide(),e.call(),c=!1}}),g.length||(e.call(),c=!1)}});function e(c,d,e){var f=this,g=c.add(this),h=c.find(e.tabs),i=d.jquery?d:c.children(d),j;h.length||(h=c.children()),i.length||(i=c.parent().find(d)),i.length||(i=a(d)),a.extend(this,{click:function(d,i){var k=h.eq(d),l=!c.data("tabs");typeof d=="string"&&d.replace("#","")&&(k=h.filter("[href*=\""+d.replace("#","")+"\"]"),d=Math.max(h.index(k),0));if(e.rotate){var m=h.length-1;if(d<0)return f.click(m,i);if(d>m)return f.click(0,i)}if(!k.length){if(j>=0)return f;d=e.initialIndex,k=h.eq(d)}if(d===j)return f;i=i||a.Event(),i.type="onBeforeClick",g.trigger(i,[d]);if(!i.isDefaultPrevented()){var n=l?e.initialEffect&&e.effect||"default":e.effect;b[n].call(f,d,function(){j=d,i.type="onClick",g.trigger(i,[d])}),h.removeClass(e.current),k.addClass(e.current);return f}},getConf:function(){return e},getTabs:function(){return h},getPanes:function(){return i},getCurrentPane:function(){return i.eq(j)},getCurrentTab:function(){return h.eq(j)},getIndex:function(){return j},next:function(){return f.click(j+1)},prev:function(){return f.click(j-1)},destroy:function(){h.off(e.event).removeClass(e.current),i.find("a[href^=\"#\"]").off("click.T");return f}}),a.each("onBeforeClick,onClick".split(","),function(b,c){a.isFunction(e[c])&&a(f).on(c,e[c]),f[c]=function(b){b&&a(f).on(c,b);return f}}),e.history&&a.fn.history&&(a.tools.history.init(h),e.event="history"),h.each(function(b){a(this).on(e.event,function(a){f.click(b,a);return a.preventDefault()})}),i.find("a[href^=\"#\"]").on("click.T",function(b){f.click(a(this).attr("href"),b)}),location.hash&&e.tabs=="a"&&c.find("[href=\""+location.hash+"\"]").length?f.click(location.hash):(e.initialIndex===0||e.initialIndex>0)&&f.click(e.initialIndex)}a.fn.tabs=function(b,c){var d=this.data("tabs");d&&(d.destroy(),this.removeData("tabs")),a.isFunction(c)&&(c={onBeforeClick:c}),c=a.extend({},a.tools.tabs.conf,c),this.each(function(){d=new e(a(this),b,c),a(this).data("tabs",d)});return c.api?d:this}})(jQuery);
(function(a){var b;b=a.tools.tabs.slideshow={conf:{next:".forward",prev:".backward",disabledClass:"disabled",autoplay:!1,autopause:!0,interval:3e3,clickable:!0,api:!1}};function c(b,c){var d=this,e=b.add(this),f=b.data("tabs"),g,h=!0;function i(c){var d=a(c);return d.length<2?d:b.parent().find(c)}var j=i(c.next).click(function(){f.next()}),k=i(c.prev).click(function(){f.prev()});function l(){g=setTimeout(function(){f.next()},c.interval)}a.extend(d,{getTabs:function(){return f},getConf:function(){return c},play:function(){if(g)return d;var b=a.Event("onBeforePlay");e.trigger(b);if(b.isDefaultPrevented())return d;h=!1,e.trigger("onPlay"),e.on("onClick",l),l();return d},pause:function(){if(!g)return d;var b=a.Event("onBeforePause");e.trigger(b);if(b.isDefaultPrevented())return d;g=clearTimeout(g),e.trigger("onPause"),e.off("onClick",l);return d},resume:function(){h||d.play()},stop:function(){d.pause(),h=!0}}),a.each("onBeforePlay,onPlay,onBeforePause,onPause".split(","),function(b,e){a.isFunction(c[e])&&a(d).on(e,c[e]),d[e]=function(b){return a(d).on(e,b)}}),c.autopause&&f.getTabs().add(j).add(k).add(f.getPanes()).hover(d.pause,d.resume),c.autoplay&&d.play(),c.clickable&&f.getPanes().click(function(){f.next()});if(!f.getConf().rotate){var m=c.disabledClass;f.getIndex()||k.addClass(m),f.onBeforeClick(function(a,b){k.toggleClass(m,!b),j.toggleClass(m,b==f.getTabs().length-1)})}}a.fn.slideshow=function(d){var e=this.data("slideshow");if(e)return e;d=a.extend({},b.conf,d),this.each(function(){e=new c(a(this),d),a(this).data("slideshow",e)});return d.api?e:this}})(jQuery);
$(function() {		
$(".wu-image-slidetabs").tabs(".wu-image-slider > li.slide", {
	effect: 'fade',
	fadeOutSpeed: "slow",		
	rotate: true		
	}).slideshow({ autoplay: true, interval: 6000});
});
$(document).ready(function () {
    var email = $("#validateEmail").val();
    var currentDate = new Date();
    var currentYear = currentDate.getFullYear();
    var currentMonth = currentDate.getMonth() + 1;
    var currentDay = currentDate.getDate();

    // Contact-Us page Validations START
    $(".contactUs-container .errorbox").hide();
    $("#container .error-msg").hide();
    $("#firstName").change(function () {
        $(this).css("margin-bottom", "22px");
        document.getElementById("firstName").style.borderColor = "#BFBFBF";
        $(this).next('.error-message').remove();
        $(this).prev('label').css("color", "#525252");
        var name = $(this).val();
        if (!name.match(/^[a-zA-Z]+$/)) {
            $(this).after("<span class='error-message'>Enter Valid FirstName</span>")
            $(this).css("margin-bottom", "0px");
            document.getElementById("firstName").style.borderColor = "#cf1b24";
            $(this).prev('label').css("color", "#cf1b24");
            $(this).focus();
        }
    });
    $("#lastName").change(function () {
        $(this).css("margin-bottom", "22px");
        document.getElementById("lastName").style.borderColor = "#BFBFBF";
        $(this).next('.error-message').remove();
        $(this).prev('label').css("color", "#525252");
        var name = $(this).val();
        if (!name.match(/^[a-zA-Z]+$/)) {
            $(this).after("<span class='error-message'>Enter Valid LastName</span>")
            $(this).css("margin-bottom", "0px");
            document.getElementById("lastName").style.borderColor = "#cf1b24"
            $(this).prev('label').css("color", "#cf1b24");
            $(this).focus();
        }
    });
    function checkDecimal(num) {
        if (num.indexOf('.') !== -1) { return true; } else { return false; }
    }
    $("#validateEmail").change(function () {
        $(this).next('.error-message').remove();
        $(this).css("margin-bottom", "22px");
        document.getElementById("validateEmail").style.borderColor = "#BFBFBF";
        $(this).prev('label').css("color", "#525252");
        var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        var firstchar = /^[a-zA-Z].*$/
        var email = $(this).val();
        if (!firstchar.test(email) || !emailPattern.test(email) || email.charAt(email.indexOf("_")) === email.charAt(email.indexOf("_") + 1) || email.charAt(email.indexOf(".")) === email.charAt(email.indexOf(".") + 1)) {
            $(this).after("<span class='error-message'>Enter Valid Email</span>");
            $(this).css("margin-bottom", "0px");
            document.getElementById("validateEmail").style.borderColor = "#cf1b24"
            $(this).prev('label').css("color", "#cf1b24");
            $(this).focus();
        }
    });


   
    $("#numberTrack").change(function () {
        $(this).next('.error-message').remove();
        $(this).css("margin-bottom", "22px");
        document.getElementById("numberTrack").style.borderColor = "#BFBFBF";
        $(this).parent().prev('label').css("color", "#525252");
        $(this).parent().prev('label').children('span').css("color", "#525252");

        var number = $(this).val();
        if (!number.match(/^[0-9]+$/)) {
            $(this).after("<span class='error-message'>Enter Valid Track Number</span>");
            $(this).css("margin-bottom", "0px");
            document.getElementById("numberTrack").style.borderColor = "#cf1b24"
            $(this).parent().prev('label').css("color", "#cf1b24");
            $(this).parent().prev('label').children('span').css("color", "#cf1b24");
            $(this).focus();
        }
    });
    $(".text-description").keyup(function () {
        var len = $(this).val().length;
        if (len >= 4000) {
            var value = $(this).val().substring(0, 4000);
            $(this).text(value);
        } else {
            $('#charCount').text(4000 - len);
        }
    });

    /* Dropdown Validations */
    $("#choosingStates").change(function () {
        $(this).next().css("margin-bottom", "22px");
        document.getElementById("lastName").style.borderColor = "#BFBFBF";
        $(this).next().next('.error-message').remove();
        $(this).prev('label').css("color", "#525252");
        var name = $(this).val();
        if ($(this).val() == "Select") {
            $(this).next().after("<span class='error-message'>Choose Service/Department</span>")
            $(this).next().css("margin-bottom", "0px");
            document.getElementById("choosingStates").style.borderColor = "#cf1b24"
            $(this).prev('label').css("color", "#cf1b24");
            $(this).focus();
        }
    });
    /* function DOBValidation() {
    $("#year").css('color', '#666');
    $("#year").after("<span class='error-message'> Enter valid Year</span>");
    $("#year").css("margin-bottom", "0px");
    document.getElementById("year").style.borderColor = "#cf1b24"
    $('.orderdate').css("color", "#cf1b24");
    $('.orderdate').children('span').css("color", "#cf1b24");
    $("#year").focus();
    }*/
    function DOBValidation(selDate, selMonth, year) {

        // get DD ID from identifier        
        var dateSelectorVal = $("#selectDate").next("div.sbHolder").find('a.current').text();
        var monthSelectorVal = $("#selectMonth").next("div.sbHolder").find('a.current').text();

        //sbSelector_                 
        var errorflag = false;
        var yearValidate = true;
        var currentDate = new Date();
        var currentYear = currentDate.getFullYear();
        var currentMonth = currentDate.getMonth() + 1;
        var currentDay = currentDate.getDate();


        /*Checking Day & month if year entered*/
        if (year != "YYYY" && (year != "" && isNaN(year) || year != "" && checkDecimal(year) || year.length != 4 || year > currentYear)) {
            $("#year").next(".error-message").toggle();
            $("#year").after("<span class='error-message'>Enter Valid Year</span>");
            $("#year").css({ "margin-bottom": "0px" });
            document.getElementById("year").style.borderColor = "#cf1b24";
            $("#year").prev('label').css("color", "#cf1b24");
            $("#year").focus();
            $(".errorbox").show();
            errorflag = true;
            yearValidate = false;
        }

        if (yearValidate && year != "" && year != "YYYY") {
            $("#year").next(".error-message").remove();
            $("#year").attr("style", "border:1px solid #BFBFBF");
            $("#selectDate").next("div.sbHolder").next("span.error-message").remove();
            $("#selectDate").next("div.sbHolder").attr("style", "border:1px solid #E1E1E1;margin-bottom:21px");
            $("#selectDate").prev('label').css("color", "#414141");
            $("#selectMonth").next("div.sbHolder").next("span.error-message").remove();
            $("#selectMonth").next("div.sbHolder").attr("style", "border:1px solid #E1E1E1;margin-bottom:21px");
            $("#selectMonth").prev('label').css("color", "#414141");


            if (selDate == 0 || selDate == "-1") {
               $("#selectDate").next("div.sbHolder").next("span.error-message").remove();
               $("#selectDate").next("div.sbHolder").after("<span class='error-message' style='margin-top:33px;'>Select Date</span>");
               $("#selectDate").next("div.sbHolder").attr("style","border:1px solid #cf1b24;margin-bottom:0px");
               $("#selectDate").prev('label').css("color", "#cf1b24");
               $("#selectDate").focus();
               $(".errorbox").show();
                errorflag = true;
            } else if (selDate > 0) {
                if (year == currentYear && monthSelectorVal == currentMonth && dateSelectorVal > currentDay) {
                   $("#selectDate").next("div.sbHolder").next("span.error-message").remove();
                   $("#selectDate").next("div.sbHolder").after("<span class='error-message' style='margin-top:33px;'>Not Valid Date</span>");
                   $("#selectDate").next("div.sbHolder").attr("style","border:1px solid #cf1b24;margin-bottom:0px");
                   $("#selectDate").prev('label').css("color", "#cf1b24");
                   $("#selectDate").focus();
                   $(".errorbox").show();
                   errorflag=true;    
                }
            }


            if (selMonth == 0 || selMonth == "-1") {
               $("#selectMonth").next("div.sbHolder").next("span.error-message").remove();
               $("#selectMonth").next("div.sbHolder").after("<span class='error-message' style='margin-top:33px;'>Select Month</span>");
               $("#selectMonth").next("div.sbHolder").attr("style","border:1px solid #cf1b24;margin-bottom:0px");
               $("#selectMonth").prev('label').css("color", "#cf1b24");
               $("#selectMonth").focus();
               $(".errorbox").show();
                errorflag = true;
            } else if (selMonth > 0) {
                if (year == currentYear && monthSelectorVal > currentMonth) {
                     $("#selectMonth").next("div.sbHolder").next("span.error-message").remove();
                             $("#selectMonth").next("div.sbHolder").after("<span class='error-message' style='margin-top:33px;'>Not Valid Month</span>");
                             $("#selectMonth").next("div.sbHolder").attr("style","border:1px solid #cf1b24;margin-bottom:0px");
                             $("#selectMonth").prev('label').css("color", "#cf1b24");
                             $("#selectMonth").focus();
                             $(".errorbox").show();
                    errorflag = true;
                }
            }
        }
        return errorflag;

    }
    /* $("#selectDate").change(function () {
        if ($("#selectMonth").val() != "MM" || this.value.match(/^([0-9]{4})$/) || $(this).val() <= currentYear || $(this).val() != "DD" || $("#year").val() != "YYYY") {
            $("#year").next('.error-message').remove();
            $("#year").css("margin-bottom", "21px");
            document.getElementById("year").style.borderColor = "#BFBFBF";
            $('.orderdate').css("color", "#525252");
            $('.orderdate').children('span').css("color", "#525252");
        }
        if ($("#year").val() == currentYear) {
            if ($("#selectMonth").val() > currentMonth || $("#selectDate").val() > currentDay) {
                DOBValidation();
            }
        }
    });
    $("#selectMonth").change(function () {
        if ($("#selectDate").val() != "DD" || this.value.match(/^([0-9]{4})$/) || $(this).val() <= currentYear || $(this).val() != "MM") {
            $("#year").next('.error-message').remove();
            $("#year").css("margin-bottom", "21px");
            document.getElementById("year").style.borderColor = "#BFBFBF";
            $('.orderdate').css("color", "#525252");
            $('.orderdate').children('span').css("color", "#525252");
        }
        if ($("#year").val() == currentYear) {
            if ($("#selectMonth").val() > currentMonth || $("#selectDate").val() > currentDay) {
                DOBValidation();
            }
        }
    }); */
    $("#captcha").change(function () {
        if ($(this).val() == "") {
            $(this).after("<span class='error-message'>Capcha required</span>");
            $("#captcha").attr("style", "border:1px solid #cf1b24;margin-bottom:0px");
            $(this).prev('label').css("color", "#cf1b24");
            $(this).focus();
        } else {
            $("#captcha").attr("style", "border:1px solid #bfbfbf;margin-bottom:21px");
            $(this).next('.error-message').remove();
            $(this).prev('label').css("color", "#525252");
        }
    });

    /* $('#year').each(function () {
        var default_value = this.value;
        $(this).css('color', '#666');

        $(this).focus(function () {
            if (this.value == default_value) {
                this.value = '';
                $(this).css('color', '#666');
            }
        });
        $(this).focusout(function () {
            if (this.value == '') {
                this.value = default_value;
            }
        });
        $(this).change(function () {
            $(this).next('.error-message').remove();
            $(this).css("margin-bottom", "21px");
            document.getElementById("year").style.borderColor = "#BFBFBF";
            $('.orderdate').css("color", "#525252");
            $('.orderdate').children('span').css("color", "#525252");

            if (!this.value.match(/^([0-9]{4})$/) || $(this).val() > currentYear || $("#selectDate").val() == "DD" || $("#selectMonth").val() == "MM") {
                DOBValidation();
            }
            if ($(this).val() == currentYear) {
                if ($("#selectMonth").val() > currentMonth || $("#selectDate").val() > currentDay) {
                    DOBValidation();
                }
            }
        });
    }); */

     $('#year').each(function () {
        var default_value = this.value;
        $(this).css('color', '#666');

        $(this).focus(function () {
            if (this.value == default_value) {
                this.value = '';
                $(this).css('color', '#666');
            }
        });
        $(this).focusout(function () {
            if (this.value == '') {
                this.value = default_value;
            }
        });
    });

    $('#year,#selectDate,#selectMonth').change(function () {  
    	 var selectedDate	=	"";
         var selectedMonth	=	"";       
         if(document.getElementById("selectDate")) {
         	selectedDate 	= document.getElementById("selectDate").selectedIndex;        	
         }
         if(document.getElementById("selectMonth")) {
         	selectedMonth 	= document.getElementById("selectMonth").selectedIndex;        	
         }
         var givenYear = $("#year").val();         
         if(!isNaN(givenYear) && givenYear!="") {        	 
         	if(DOBValidation(selectedDate,selectedMonth,givenYear)) {
         		return false;
         	}
         }    else {        	 
        	 $("#year").val("");    
         }
    });



    $("#contact-send").click(function () {
        var firstName = $("#firstName").val();
        var lastName = $("#lastName").val();
        var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        var email = $("#validateEmail").val();
        var description = $(".text-description").val();
        var comments = $("#desc").val();
        var year = $("#year").val();
        var service = $("#choosingStates").val();
        var trackNumber = $("#numberTrack").val();
        var captchainput = $("#captcha").val();
        var selectedDate = null;
        var selectedMonth = null;
        if (document.getElementById("selectDate")) {
            selectedDate = document.getElementById("selectDate").selectedIndex;
        }
        if (document.getElementById("selectMonth")) {
            selectedMonth = document.getElementById("selectMonth").selectedIndex;
        }
        var errorflag = "false";
        if (firstName == "") {
            $("#firstName").next(".error-message").toggle();
            $("#firstName").after("<span class='error-message'>First Name is required</span>");
            $("#firstName").css({ "margin-bottom": "0px" });
            document.getElementById("firstName").style.borderColor = "#cf1b24"
            $("#firstName").prev('label').css("color", "#cf1b24");
            $("#firstName").focus();
            $(".errorbox").show();
        }
        if (lastName == "") {
            $("#lastName").next(".error-message").toggle();
            $("#lastName").after("<span class='error-message'>LastName is required</span>");
            $("#lastName").css({ "margin-bottom": "0px" });
            document.getElementById("lastName").style.borderColor = "#cf1b24"
            $("#lastName").prev('label').css("color", "#cf1b24");
            $("#lastName").focus();
            $(".errorbox").show();
        }
        if (email == "") {
            $("#validateEmail").next(".error-message").toggle();
            $("#validateEmail").after("<span class='error-message'>Email address is required</span>");
            $("#validateEmail").css({ "margin-bottom": "0px" });
            document.getElementById("validateEmail").style.borderColor = "#cf1b24"
            $("#validateEmail").prev('label').css("color", "#cf1b24");
            $("#validateEmail").focus();
            $(".errorbox").show();
        }
        if (service == "Select") {
            $("#choosingStates").next('div').next(".error-message").toggle();
            $("#choosingStates").next().after("<span class='error-message'>Choose Service/Department</span>")
            $("#choosingStates").next().css("margin-bottom", "0px");
            $("#choosingStates").prev('label').css("color", "#cf1b24");
            $(".errorbox").show();
        }
        /* if (year == "YYYY" || year == "") {
        $("#year").next(".error-message").toggle();
        $("#year").after("<span class='error-message'>Year required</span>");
        $("#year").css({ "margin-bottom": "0px" });
        document.getElementById("year").style.borderColor = "#cf1b24"
        $('.orderdate').css("color", "#cf1b24");
        $('.orderdate').children('span').css("color", "#cf1b24");
        $(".errorbox").show();
        }*/
        if (!isNaN(year) && year != "") {
            if (DOBValidation(selectedDate, selectedMonth, year)) {
                errorflag = "true";
            }
        }
        if (captchainput == "") {
            $("#captcha").next(".error-message").toggle();
            $("#captcha").after("<span class='error-message'>Captcha is required</span>");
            $("#captcha").css({ "margin-bottom": "0px" });
            document.getElementById("captcha").style.borderColor = "#cf1b24"
            $("#captcha").prev('label').css("color", "#cf1b24");
            $("#captcha").focus();
            $(".errorbox").show();
            errorflag = "true";
        } else {
            $("#captcha").next("span.error-message").remove();
            $("#captcha").attr("style", "border:1px solid #BFBFBF;margin-bottom:21px");
            $("#captcha").prev('label').css("color", "#414141");
        }

        /*if (!trackNumber.match(/^[0-9]+$/)) {
        $("#numberTrack").next(".error-message").toggle();
        $("#numberTrack").after("<span class='error-message'>Enter Valid Track Number</span>");
        $("#numberTrack").css("margin-bottom", "0px");
        document.getElementById("numberTrack").style.borderColor = "#cf1b24"
        $("#numberTrack").parent().prev('label').css("color", "#cf1b24");
        $("#numberTrack").parent().prev('label').children('span').css("color", "#cf1b24");
        $(".errorbox").show();
        $("#numberTrack").focus();
        }*/
        if (errorflag == "false") {
            if (year != "" && isNaN(year)) { $("#year").val(""); }
            if (isNaN($("#selectDate").val())) { $("#selectDate").val(""); }
            if (isNaN($("#selectMonth").val())) { $("#selectMonth").val(""); }
            $(".midWrap").next(".errorbox").hide();
            //return true;
            $("#contactusform").submit();
        }
        //return false;

    });
    /* Contact-Us page Validations END */



    //Track-Result Page validations START
    /* $(".Transfer-tabs li").click(function () {
    $(".Transfer-tabs li").not($(this)).removeClass("active");
    $(this).addClass("active");
    });  */

    $("#number_Track").change(function () {

        $(this).css("margin-bottom", "1em");
        document.getElementById("number_Track").style.borderColor = "#BFBFBF";
        $(this).next('.error-message').remove();
        $(this).prev('label').css("color", "#525252");
        var name = $(this).val();
        if (!name.match(/^[0-9]+$/)) {
            $(this).after("<span class='error-message'>Enter Valid Track Number</span>")
            $(this).css("margin-bottom", "0px");
            document.getElementById("number_Track").style.borderColor = "#cf1b24";
            $(this).prev('label').css("color", "#cf1b24");
            $(this).focus();
        }
    });

    $("#firstName_Transfer,#receivers_firstName").change(function () {
        $(this).css("margin-bottom", "1em");
        $(this).css('border', '1px solid #BFBFBF');
        $(this).next('.error-message').remove();
        $(this).prev('label').css("color", "#525252");
        var name = $(this).val();
        if (!name.match(/^[a-zA-Z]+$/)) {
            $(this).after("<span class='error-message'>Enter Valid FirstName</span>")
            $(this).css("margin-bottom", "0px");
            $(this).css('border', '1px solid #cf1b24');
            $(this).prev('label').css("color", "#cf1b24");
            $(this).focus();
        }

        if (name == "") {
            $(this).css("margin-bottom", "1em");
            $(this).css('border', '1px solid #BFBFBF');
            $(this).next('.error-message').remove();
            $(this).prev('label').css("color", "#525252");
        }
    });
    $("#lastName_Transfer,#receivers_lastName").change(function () {
        $(this).css("margin-bottom", "1em");
        $(this).css('border', '1px solid #BFBFBF');
        $(this).next('.error-message').remove();
        $(this).prev('label').css("color", "#525252");
        var name = $(this).val();
        if (!name.match(/^[a-zA-Z]+$/)) {
            $(this).after("<span class='error-message'>Enter Valid LastName</span>")
            $(this).css("margin-bottom", "0px");
            $(this).css('border', '1px solid #cf1b24');
            $(this).prev('label').css("color", "#cf1b24");
            $(this).focus();
        }
        if (name == "") {
            $(this).css("margin-bottom", "1em");
            $(this).css('border', '1px solid #BFBFBF');
            $(this).next('.error-message').remove();
            $(this).prev('label').css("color", "#525252");
        }
    });


    function checkDecimal(num) {
        if (num.indexOf('.') !== -1) { return true; } else { return false; }
    }
    $("#Track_submit").click(function () {
        var firstName = $("#firstName_Transfer").val();
        var lastName = $("#lastName_Transfer").val();
        var number = $("#number_Track").val();
        var recv_firstName = $("#receivers_firstName").val();
        var recv_lastName = $("#receivers_lastName").val();
        var input_capcha = $("input.input_capcha").val();
        var captcha_input = $("#captcha").val();
        var sender;
        sender = 0;
        var receiver;
        receiver = 0;
        var submit_t = true;
        // Remove span by default
        $("#number_Track").next('span.error-message').remove();
        $("#firstName_Transfer").next('span.error-message').remove();
        $("#lastName_Transfer").next('span.error-message').remove();
        $("#recv_firstName_Transfer").next('span.error-message').remove();
        $("#recv_lastName_Transfer").next('span.error-message').remove();
        $("#senderRecieverInfo").html("");
        $("#senderRecieverInfo").hide();

        if (captcha_input == "") {
            $("#captcha").next(".error-message").toggle();
            $("#captcha").after("<span class='error-message'>Captcha is required</span>");
            $("#captcha").css("margin-bottom", "0px");
            $("#captcha").css('border', '1px solid #cf1b24');
            $("#captcha").prev('label').css("color", "#cf1b24");
            //$("#number_Track").focus();
            $(".errorbox").show();
            submit_t = false;
        }
        if (number == "") {
            $("#number_Track").next(".error-message").toggle();
            $("#number_Track").after("<span class='error-message'>Tracking Number is required</span>");
            $("#number_Track").css("margin-bottom", "0px");
            $("#number_Track").css('border', '1px solid #cf1b24');
            $("#number_Track").prev('label').css("color", "#cf1b24");
            //$("#number_Track").focus();
            $(".errorbox").show();
            submit_t = false;
        } else if (number != "" && isNaN(number) || number != "" && checkDecimal(number)) {
            $("#number_Track").next(".error-message").toggle();
            $("#number_Track").after("<span class='error-message'>Enter Valid Track Number</span>");
            $("#number_Track").css("margin-bottom", "0px");
            document.getElementById("number_Track").style.borderColor = "#cf1b24";
            $("#number_Track").prev('label').css("color", "#cf1b24");
            $("#number_Track").prev('label').children('span').css("color", "#cf1b24");
            $("#number_Track").nextAll('.error').hide();
            // $("#number_Track").focus();
            submit_t = false;
        } else {
            $("#number_Track").prev('label').css("color", "#525252");
            $("#number_Track").next('span.error-message').remove();
            document.getElementById("number_Track").style.borderColor = "#BFBFBF";
        }
        if ((firstName == "") || (lastName == "")) {
            sender = sender + 1;
        }
        if ((recv_firstName == "") || (recv_lastName == "")) {
            receiver = receiver + 1;

        }
        if (((firstName == "") || (lastName == "")) && ((recv_firstName == "") || (recv_lastName == ""))) {
            $("#senderRecieverInfo").attr("style", "padding-left:5px;padding-top:10px;margin-bottom:-5px;");
            $("#senderRecieverInfo").html("Either Sender's detail or Receiver's Details is needed");
            $("#senderRecieverInfo").show();
        }
        if (((firstName != "") && (lastName != "")) && ((!firstName.match(/^[a-zA-Z]+$/))) && ((recv_firstName == "") || (recv_lastName == ""))) {
            $("#firstName_Transfer").next(".error-message").toggle();
            $("#firstName_Transfer").after("<span class='error-message'>Enter Valid FirstName</span>")
            $("#firstName_Transfer").css("margin-bottom", "0px");
            $("#firstName_Transfer").css('border', '1px solid #cf1b24');
            $("#firstName_Transfer").prev('label').css("color", "#cf1b24");
            // $("#firstName_Transfer").focus();

        }

        if (((firstName != "") && (lastName != "")) && ((!lastName.match(/^[a-zA-Z]+$/))) && ((recv_firstName == "") || (recv_lastName == ""))) {
            $("#lastName_Transfer").next(".error-message").toggle();
            $("#lastName_Transfer").after("<span class='error-message'>Enter Valid LastName</span>")
            $("#lastName_Transfer").css("margin-bottom", "0px");
            $("#lastName_Transfer").css('border', '1px solid #cf1b24');
            $("#lastName_Transfer").prev('label').css("color", "#cf1b24");
            // $("#lastName_Transfer").focus();

        }

        if (((recv_firstName != "") && (recv_lastName != "")) && ((!recv_firstName.match(/^[a-zA-Z]+$/))) && ((firstName == "") || (lastName == ""))) {
            $("#recv_firstName_Transfer").next(".error-message").toggle();
            $("#recv_firstName_Transfer").after("<span class='error-message'>Enter Valid FirstName</span>")
            $("#recv_firstName_Transfer").css("margin-bottom", "0px");
            $("#recv_firstName_Transfer").css('border', '1px solid #cf1b24');
            $("#recv_firstName_Transfer").prev('label').css("color", "#cf1b24");
            // $("#recv_firstName_Transfer").focus();

        }

        if (((recv_firstName != "") && (recv_lastName != "")) && ((!recv_lastName.match(/^[a-zA-Z]+$/))) && ((firstName == "") || (lastName == ""))) {
            $("#recv_lastName_Transfer").next(".error-message").toggle();
            $("#recv_lastName_Transfer").after("<span class='error-message'>Enter Valid LastName</span>")
            $("#recv_lastName_Transfer").css("margin-bottom", "0px");
            $("#recv_lastName_Transfer").css('border', '1px solid #cf1b24');
            $("#recv_lastName_Transfer").prev('label').css("color", "#cf1b24");
            // $("#recv_lastName_Transfer").focus();

        }
        if ((firstName != "") && (!firstName.match(/^[a-zA-Z]+$/))) {
            submit_t = false;

        }
        if ((lastName != "") && (!lastName.match(/^[a-zA-Z]+$/))) {
            submit_t = false;
        }
        if ((recv_firstName != "") && (!recv_firstName.match(/^[a-zA-Z]+$/))) {
            submit_t = false;

        }
        if ((recv_lastName != "") && (!recv_lastName.match(/^[a-zA-Z]+$/))) {
            submit_t = false;

        }

        if ((submit_t == true) && ((sender == 0) || (receiver == 0))) {
            return true;
        }
        else {
            return false;
        }
        //return false;
    });

    //Validations for Track_Result End

    //Validations for LoginEmail Verification Start
    $("#login_password").blur(function () {
        $(this).css("margin-bottom", "15px");
        document.getElementById("login_password").style.borderColor = "#BFBFBF";
        $(this).next('.error-message').remove();
        $(this).prev('label').css("color", "#525252");
        if ($(this).val() == "") {
            $(this).next(".error-message").toggle();
            $(this).after("<span class='error-message'>Password should not be empty</span>");
            $(this).css({ "margin-bottom": "0px" });
            document.getElementById("login_password").style.borderColor = "#cf1b24"
            $(this).prev('label').css("color", "#cf1b24");
        }
    })
    $("#login_Email").change(function () {
        $(this).next('.error-message').remove();
        $(this).css("margin-bottom", "15px");
        document.getElementById("login_Email").style.borderColor = "#BFBFBF";
        $(this).prev('label').css("color", "#525252");
        var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        var firstchar = /^[a-zA-Z].*$/
        var email = $(this).val();
        if (!firstchar.test(email) || !emailPattern.test(email) || email.charAt(email.indexOf("_")) === email.charAt(email.indexOf("_") + 1) || email.charAt(email.indexOf(".")) === email.charAt(email.indexOf(".") + 1)) {
            $(this).after("<span class='error-message'>Enter Valid Email</span>");
            $(this).css("margin-bottom", "0px");
            document.getElementById("login_Email").style.borderColor = "#cf1b24"
            $(this).prev('label').css("color", "#cf1b24");
            $(this).focus();
        }
    });
    $("#loginEmail_verify").click(function () {
        var password = $("#login_password").val();
        var login_email = $("#login_Email").val();
        if (login_email == "") {
            $("#login_Email").next(".error-message").toggle();
            $("#login_Email").after("<span class='error-message'>Email address is required</span>");
            $("#login_Email").css({ "margin-bottom": "0px" });
            document.getElementById("login_Email").style.borderColor = "#cf1b24"
            $("#login_Email").prev('label').css("color", "#cf1b24");
        }
        if (password == "") {
            $("#login_password").next(".error-message").toggle();
            $("#login_password").after("<span class='error-message'>Please enter password</span>");
            $("#login_password").css({ "margin-bottom": "0px" });
            document.getElementById("login_password").style.borderColor = "#cf1b24"
            $("#login_password").prev('label').css("color", "#cf1b24");
        }
        return false;
    });

    $("#login_Error").click(function () {
        var password = $("#login_password").val();
        var login_email = $("#login_Email").val();
        var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        var firstchar = /^[a-zA-Z].*$/
        if (login_email == "" || !firstchar.test(email) || !emailPattern.test(email) || email.charAt(email.indexOf("_")) === email.charAt(email.indexOf("_") + 1) || email.charAt(email.indexOf(".")) === email.charAt(email.indexOf(".") + 1)) {
            $("#login_Email").next(".error-message").toggle();
            $("#login_Email").after("<span class='error-message'>Email address is required</span>");
            $("#login_Email").css({ "margin-bottom": "0px" });
            document.getElementById("login_Email").style.borderColor = "#cf1b24"
            $("#login_Email").prev('label').css("color", "#cf1b24");
            $(".error-msg").show();
        }
        if (password == "") {
            $("#login_password").next(".error-message").toggle();
            $("#login_password").after("<span class='error-message'>Please enter password</span>");
            $("#login_password").css({ "margin-bottom": "0px" });
            document.getElementById("login_password").style.borderColor = "#cf1b24"
            $("#login_password").prev('label').css("color", "#cf1b24");
            $(".error-msg").show();
        }
        return false;
    });
    $(".error-msg a").click(function () {
        $(".error-msg").hide();
    });







    $("#login_reg_password").blur(function () {
        $(this).css("margin-bottom", "0px");
        document.getElementById("login_reg_password").style.borderColor = "#BFBFBF";
        $(this).next('.error-message').remove();
        $(this).prev('label').css("color", "#525252");
        if ($(this).val() == "") {
            $(this).next(".error-message").toggle();
            $(this).after("<span class='error-message'>Email address is required</span>");
            $(this).css({ "margin-bottom": "0px" });
            document.getElementById("login_reg_password").style.borderColor = "#cf1b24"
            $(this).prev('label').css("color", "#cf1b24");
        }
    })
    $("#login_reg_Email").change(function () {
        $(this).next('.error-message').remove();
        $(this).css("margin-bottom", "16px");
        document.getElementById("login_reg_Email").style.borderColor = "#BFBFBF";
        $(this).prev('label').css("color", "#525252");
        var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        var firstchar = /^[a-zA-Z].*$/
        var email = $(this).val();
        if (!firstchar.test(email) || !emailPattern.test(email) || email.charAt(email.indexOf("_")) === email.charAt(email.indexOf("_") + 1) || email.charAt(email.indexOf(".")) === email.charAt(email.indexOf(".") + 1)) {
            $(this).after("<span class='error-message'>Enter Valid Email</span>");
            $(this).css("margin-bottom", "0px");
            document.getElementById("login_reg_Email").style.borderColor = "#cf1b24"
            $(this).prev('label').css("color", "#cf1b24");
            $(this).focus();
        }
    });
    $("#loginReg_verify").click(function () {
        var password = $("#login_reg_password").val();
        var login_email = $("#login_reg_Email").val();
        if (login_email == "") {
            $("#login_reg_Email").next(".error-message").toggle();
            $("#login_reg_Email").after("<span class='error-message'>Email address is required</span>");
            $("#login_reg_Email").css({ "margin-bottom": "0px" });
            document.getElementById("login_reg_Email").style.borderColor = "#cf1b24"
            $("#login_reg_Email").prev('label').css("color", "#cf1b24");
        }
        if (password == "") {
            $("#login_reg_password").next(".error-message").toggle();
            $("#login_reg_password").after("<span class='error-message'>Please enter password</span>");
            $("#login_reg_password").css({ "margin-bottom": "0px" });
            document.getElementById("login_reg_password").style.borderColor = "#cf1b24"
            $("#login_reg_password").prev('label').css("color", "#cf1b24");
        }
        return false;
    });
    //Validations for LoginEmail Verification End
});
jQuery.fn.exists = function () {
    return jQuery(this).length > 0;
};
$(function () {
	jQuery.noConflict();
	$("select").selectBox();
        $("SELECT").selectBox("settings", {
            menuTransition: "slide",
            menuSpeed: "fast"
        }).focus(function () {});
        var select_list_count = 0;
        $("select").each(function () {
            $(this).addClass("drop_down_list_control-" + select_list_count.toString());
            select_list_count++;
        });
        select_list_count = 0;
        $(".selectBox-dropdown-menu").each(function () {
            $(this).addClass("drop_down_list-" + select_list_count.toString());
            select_list_count++;
            $("li:first-child", $(this)).remove();
        });
        if ($("header div.drop-down").exists()) {
            $("ul.drop_down_list-0, ul.drop_down_list-1").addClass("header_dropdown_body");
        }
});
function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
        vars[key] = value;
    });
    return vars;
}
$(document).ready(function () {
    $("#amountToSend").val(getUrlVars()["amount"]);
    $("#shoppingZipFrom").val(getUrlVars()["zipcode"]);
	if ($.cookie('shoppingDestCountry') != null) $("#shoppingCountryTo").val($.cookie('shoppingDestCountry'));
    $(".keypress-form input").keypress(function () {
        $(this).parents(".keypress-form").find(".btn-sm a.disabled", "btn-lrg a.disabled").removeClass("disabled");
    });
});
if (typeof TLT != "undefined") {
    if (TLT.Extensions != "undefined") {
        TLT.Extensions = {};
    }
    if (TLT.Extensions.captureMouseOvers == undefined) {
        TLT.Extensions.captureMouseOvers = function () {
            $("ul.megamenu li a.mm-item-link").bind("mouseover", function (evt) {
                TLT.processDOMEvent(evt);
            });
            $("ul.megamenu li a.mm-item-link").bind("mouseout", function (evt) {
                TLT.processDOMEvent(evt);
            });
        };
        TLT.Extensions.captureMouseOvers();
    }
    if (TLT.Extensions.captureDropdownChange == undefined) {
        TLT.Extensions.captureDropdownChange = function () {
            $("SELECT").bind("change", function (evt) {
                TLT.processDOMEvent(evt);
            });
        };
        TLT.Extensions.captureDropdownChange();
    }
    if (TLT.Extensions.captureMMItemLink == undefined) {
        TLT.Extensions.captureMMItemLink = function () {
            $(".mm-item").bind("click", function (evt) {
                TLT.processDOMEvent(evt);
            });
        };
        TLT.Extensions.captureMMItemLink();
    }
}
