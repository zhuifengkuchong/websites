var sid = readCookie('SessionId');    
//alert('Session ID is '+sid);
if (sid)
{                              
	var url =  '/presentationservice/rest/api/v1.0/TouchSession';
	var request = $
	.ajax({
		url : url,
		contentType :'application/json',
		async : false,
		crossDomain : true,
		complete : function(r) {
			if(r.responseText){
				if (r.status == 200){
					try{
                     var response=JSON.parse(r.responseText);
                     if(response!==undefined & response!==null && response.isSuccess!==undefined)
                     {
                     	if(response.isSuccess===true)
                     	{
                     		createSessionCookie("SessionId",sid, 14, "/", null);
                           // console.log('Touch Session is active....cookie reset');
                     	}else
                     	{
                     		// console.log('TouchSession is expired.....');
                     	}
                     }
                     
                 }catch(err)
                 {
                 	//console.log(err);
                 	//console.log('Touch Session Failed.');
                 }
					
				}
			}
		}
	});
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for ( var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ')
			c = c.substring(1, c.length);
		if (c.indexOf(nameEQ) == 0)
			return c.substring(nameEQ.length, c.length);
	}
	return null;
}

function createSessionCookie(name, value, expires, path, domain) {
        var docdomain = document.domain.split('.');
    var dom1 = "";
    if (typeof(docdomain[docdomain.length - 2]) != 'undefined') dom1 = docdomain[docdomain.length - 2] + '.';
    var domainname = dom1 + docdomain[docdomain.length - 1];
		var cookie = name + "=" + escape(value) + ";";

		if (expires) {
			// If it's a date
			if(expires instanceof Date) {
				// If it isn't a valid date
				if (isNaN(expires.getTime())){
					expires = new Date();
				}
			}else if(expires === null){
				expires = new Date(new Date().getTime() + parseInt(this.defaultExpires) * 1000 * 60 )
			}
			else{
				expires = new Date(new Date().getTime() + parseInt(expires) * 1000 * 60 );
			}
			cookie += "expires=" + expires.toGMTString() + ";";
		}
        path="/";
		if (path){
			cookie += "path="  + path +";";
		}
		 if (domain){
                cookie += "Domain=" + domain + ";";
            }else if(domainname !== "localhost" && isNaN(parseInt(domainname.charAt(0)))){
            	// handle domain is not localhost and ip address
            	cookie += "Domain=" + domainname + ";";
            }
		document.cookie = cookie;
	}