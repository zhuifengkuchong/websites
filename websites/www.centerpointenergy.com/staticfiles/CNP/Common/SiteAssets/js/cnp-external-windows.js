// -----------------------
// Set webroot change to http://info.CenterPointEnergy.com for prod
// -----------------------
var webRoot = "http://info.centerpointenergy.com/";

// -----------------------
// Dynamic date for curtailment page
// -----------------------
function showDate() {
	var d = new Date();
  var curr_date = d.getDate();
  var curr_month = d.getMonth();
  curr_month++;
  var curr_year = d.getFullYear();
  document.write(curr_month + "/" + curr_date + "/" + curr_year);
}

// -----------------------
// Link and close popup window
// -----------------------
function linkWindow(url) {
	window.opener.location.href=url;
  window.close();
}

// -----------------------
// MNG Calculator Windows
// -----------------------
/* 1 */
function loadMNEnergyCostCalc() {
    //window.open(webRoot + '/calculators/Minnesota/aggregate.asp?c=1&ap=on','','width=460,height=850,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
    window.open(webRoot + '/calculators/Minnesota/ResCostCompare.asp?c=1&ap=on','','width=630,height=910,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

/* 2 
function loadMNMakeupCalc() {
    window.open(webRoot + '/calculators/Minnesota/makeupCalc.asp?c=1&ap=on','','width=460,height=800,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}*/

/* 3 */
function loadMNGasPriceCalc() {
    //window.open(webRoot + '/calculators/Minnesota/cost_calc.asp?c=1&ap=on','','width=500,height=900,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
    window.open(webRoot + '/calculators/Minnesota/ResCostCompare.asp?c=1&ap=on','','width=630,height=910,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

/* 4 */
function loadMNHeatingWithNaturalGasCalc() {
    //window.open(webRoot + '/calculators/Minnesota/furnace.asp?c=1&ap=on','','width=460,height=470,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
    window.open(webRoot + '/calculators/Minnesota/ResCostCompare.asp?c=1&ap=on','','width=630,height=910,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

/* 5 */
function loadMNWaterHeaterCalc() {
    //window.open(webRoot + '/calculators/Minnesota/hotwater.asp?c=1&ap=on','','width=500,height=565,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
    window.open(webRoot + '/calculators/Minnesota/ResCostCompare.asp?c=1&ap=on','','width=630,height=910,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

/* 6 */
function loadMNGasDryerCalc() {
    //window.open(webRoot + '/calculators/Minnesota/dry.asp?c=1&ap=on','','width=460,height=470,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
    window.open(webRoot + '/calculators/Minnesota/ResCostCompare.asp?c=1&ap=on','','width=630,height=910,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

/* 7 */
function loadMNCookingWithGasCalc() {
    //window.open(webRoot + '/calculators/Minnesota/range.asp?c=1&ap=on','','width=460,height=470,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
    window.open(webRoot + '/calculators/Minnesota/ResCostCompare.asp?c=1&ap=on','','width=630,height=910,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

/* 8 */
function loadMNlargevolCalc() {
    window.open(webRoot + '/calculators/Minnesota/largevolCalc.asp?c=1&ap=on','','width=460,height=470,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

/* 9 */
function loadTXlargevolCalc() {
    window.open(webRoot + '/calculators/Texas/largevolCalc.asp?c=1&ap=on','','width=460,height=470,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

/* 10 */
function loadLAlargevolCalc() {
    window.open(webRoot + '/calculators/Louisiana/largevolCalc.asp?c=1&ap=on','','width=460,height=470,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

/* 11 */
function loadMSlargevolCalc() {
    window.open(webRoot + '/calculators/Mississippi/largevolCalc.asp?c=1&ap=on','','width=460,height=470,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

/* 12 */
function loadARlargevolCalc() {
    window.open(webRoot + '/calculators/Arkansas/largevolCalc.asp?c=1&ap=on','','width=460,height=470,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

/* 13 */
function loadOKlargevolCalc() {
    window.open(webRoot + '/calculators/Oklahoma/largevolCalc.asp?c=1&ap=on','','width=460,height=470,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}


// -----------------------
// Texas Calculator Windows
// -----------------------
function loadTXEnergyCostCalc() {
    //window.open(webRoot + '/calculators/Texas/aggregate.asp?c=1&ap=on','','width=460,height=850,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
    window.open(webRoot + '/calculators/Texas/ResCostCompare.asp?c=1&ap=on','','width=630,height=910,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

function loadTXHeatingWithNaturalGasCalc() {
    //window.open(webRoot + '/calculators/Texas/furnace.asp?c=1&ap=on','','width=460,height=470,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
    window.open(webRoot + '/calculators/Texas/ResCostCompare.asp?c=1&ap=on','','width=630,height=910,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

function loadTXWaterHeaterCalc() {
    //window.open(webRoot + '/calculators/Texas/hotwater.asp?c=1&ap=on','','width=500,height=565,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
    window.open(webRoot + '/calculators/Texas/ResCostCompare.asp?c=1&ap=on','','width=630,height=910,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

function loadTXGasDryerCalc() {
    //window.open(webRoot + '/calculators/Texas/dry.asp?c=1&ap=on','','width=460,height=470,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
    window.open(webRoot + '/calculators/Texas/ResCostCompare.asp?c=1&ap=on','','width=630,height=910,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

function loadTXCookingWithGasCalc() {
    //window.open(webRoot + '/calculators/Texas/range.asp?c=1&ap=on','','width=460,height=470,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
    window.open(webRoot + '/calculators/Texas/ResCostCompare.asp?c=1&ap=on','','width=630,height=910,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

// -----------------------
// Houston Calculator Windows
// -----------------------
function loadHOUEnergyCostCalc() {
    //window.open(webRoot + '/calculators/Houston/aggregate.asp?c=1&ap=on','','width=460,height=850,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
    window.open(webRoot + '/calculators/Houston/ResCostCompare.asp?c=1&ap=on','','width=630,height=910,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

function loadHOUHeatingWithNaturalGasCalc() {
    //window.open(webRoot + '/calculators/Houston/furnace.asp?c=1&ap=on','','width=460,height=470,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
    window.open(webRoot + '/calculators/Houston/ResCostCompare.asp?c=1&ap=on','','width=630,height=910,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

function loadHOUWaterHeaterCalc() {
    //window.open(webRoot + '/calculators/Houston/hotwater.asp?c=1&ap=on','','width=500,height=565,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
    window.open(webRoot + '/calculators/Houston/ResCostCompare.asp?c=1&ap=on','','width=630,height=910,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

function loadHOUGasDryerCalc() {
    //window.open(webRoot + '/calculators/Houston/dry.asp?c=1&ap=on','','width=460,height=470,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
    window.open(webRoot + '/calculators/Houston/ResCostCompare.asp?c=1&ap=on','','width=630,height=910,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

function loadHOUCookingWithGasCalc() {
    //window.open(webRoot + '/calculators/Houston/range.asp?c=1&ap=on','','width=460,height=470,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
    window.open(webRoot + '/calculators/Houston/ResCostCompare.asp?c=1&ap=on','','width=630,height=910,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

// -----------------------
// Louisiana Calculator Windows
// -----------------------
function loadLAEnergyCostCalc() {
   // window.open(webRoot + '/calculators/Louisiana/aggregate.asp?c=1&ap=on','','width=460,height=850,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
   window.open(webRoot + '/calculators/Louisiana/ResCostCompare.asp?c=1&ap=on','','width=630,height=910,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

function loadLAHeatingWithNaturalGasCalc() {
    //window.open(webRoot + '/calculators/Louisiana/furnace.asp?c=1&ap=on','','width=460,height=470,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
    window.open(webRoot + '/calculators/Louisiana/ResCostCompare.asp?c=1&ap=on','','width=630,height=910,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

function loadLAWaterHeaterCalc() {
	//window.open(webRoot + '/calculators/Louisiana/hotwater.asp?c=1&ap=on','','width=500,height=565,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
	window.open(webRoot + '/calculators/Louisiana/ResCostCompare.asp?c=1&ap=on','','width=630,height=910,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

function loadLAGasDryerCalc() {
    //window.open(webRoot + '/calculators/Louisiana/dry.asp?c=1&ap=on','','width=460,height=470,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
    window.open(webRoot + '/calculators/Louisiana/ResCostCompare.asp?c=1&ap=on','','width=630,height=910,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

function loadLACookingWithGasCalc() {
    //window.open(webRoot + '/calculators/Louisiana/range.asp?c=1&ap=on','','width=460,height=470,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
    window.open(webRoot + '/calculators/Louisiana/ResCostCompare.asp?c=1&ap=on','','width=630,height=910,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

// -----------------------
// Oklahoma Calculator Windows
// -----------------------
function loadOKEnergyCostCalc() {
    //window.open(webRoot + '/calculators/Oklahoma/aggregate.asp?c=1&ap=on','','width=460,height=850,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
    window.open(webRoot + '/calculators/Oklahoma/ResCostCompare.asp?c=1&ap=on','','width=630,height=910,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

function loadOKHeatingWithNaturalGasCalc() {
    //window.open(webRoot + '/calculators/Oklahoma/furnace.asp?c=1&ap=on','','width=460,height=470,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
    window.open(webRoot + '/calculators/Oklahoma/ResCostCompare.asp?c=1&ap=on','','width=630,height=910,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

function loadOKWaterHeaterCalc() {
    //window.open(webRoot + '/calculators/Oklahoma/hotwater.asp?c=1&ap=on','','width=500,height=565,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
    window.open(webRoot + '/calculators/Oklahoma/ResCostCompare.asp?c=1&ap=on','','width=630,height=910,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

function loadOKGasDryerCalc() {
    //window.open(webRoot + '/calculators/Oklahoma/dry.asp?c=1&ap=on','','width=460,height=470,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
    window.open(webRoot + '/calculators/Oklahoma/ResCostCompare.asp?c=1&ap=on','','width=630,height=910,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

function loadOKCookingWithGasCalc() {
    //window.open(webRoot + '/calculators/Oklahoma/range.asp?c=1&ap=on','','width=460,height=470,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
    window.open(webRoot + '/calculators/Oklahoma/ResCostCompare.asp?c=1&ap=on','','width=630,height=910,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

// -----------------------
// Arkansas Calculator Windows
// -----------------------
function loadAREnergyCostCalc() {
    //window.open(webRoot + '/calculators/Arkansas/aggregate.asp?c=1&ap=on','','width=460,height=850,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
    window.open(webRoot + '/calculators/Arkansas/ResCostCompare.asp?c=1&ap=on','','width=630,height=910,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

function loadARHeatingWithNaturalGasCalc() {
    //window.open(webRoot + '/calculators/Arkansas/furnace.asp?c=1&ap=on','','width=460,height=470,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
    window.open(webRoot + '/calculators/Arkansas/ResCostCompare.asp?c=1&ap=on','','width=630,height=910,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

function loadARWaterHeaterCalc() {
    //window.open(webRoot + '/calculators/Arkansas/hotwater.asp?c=1&ap=on','','width=500,height=565,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
    window.open(webRoot + '/calculators/Arkansas/ResCostCompare.asp?c=1&ap=on','','width=630,height=910,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

function loadARGasDryerCalc() {
    //window.open(webRoot + '/calculators/Arkansas/dry.asp?c=1&ap=on','','width=460,height=470,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
    window.open(webRoot + '/calculators/Arkansas/ResCostCompare.asp?c=1&ap=on','','width=630,height=910,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

function loadARCookingWithGasCalc() {
    //window.open(webRoot + '/calculators/Arkansas/range.asp?c=1&ap=on','','width=460,height=470,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
    window.open(webRoot + '/calculators/Arkansas/ResCostCompare.asp?c=1&ap=on','','width=630,height=910,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

// -----------------------
// Mississippi Calculator Windows
// -----------------------
function loadMSEnergyCostCalc() {
    //window.open(webRoot + '/calculators/Mississippi/aggregate.asp?c=1&ap=on','','width=460,height=850,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
    window.open(webRoot + '/calculators/Mississipp/ResCostCompare.asp?c=1&ap=on','','width=630,height=910,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

function loadMSHeatingWithNaturalGasCalc() {
    //window.open(webRoot + '/calculators/Mississippi/furnace.asp?c=1&ap=on','','width=460,height=470,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
    window.open(webRoot + '/calculators/Mississipp/ResCostCompare.asp?c=1&ap=on','','width=630,height=910,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

function loadMSWaterHeaterCalc() {
    //window.open(webRoot + '/calculators/Mississippi/hotwater.asp?c=1&ap=on','','width=500,height=565,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
    window.open(webRoot + '/calculators/Mississipp/ResCostCompare.asp?c=1&ap=on','','width=630,height=910,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

function loadMSGasDryerCalc() {
    //window.open(webRoot + '/calculators/Mississippi/dry.asp?c=1&ap=on','','width=460,height=470,top=0,left=0,resizeable=yes,scrollbars=?es,status=no,toolbar=no,menubar=no,location=no');
    window.open(webRoot + '/calculators/Mississipp/ResCostCompare.asp?c=1&ap=on','','width=630,height=910,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

function loadMSCookingWithGasCalc() {
    //window.open(webRoot + '/calculators/Mississippi/range.asp?c=1&ap=on','','width=460,height=470,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
    window.open(webRoot + '/calculators/Mississipp/ResCostCompare.asp?c=1&ap=on','','width=630,height=910,top=0,left=0,resizeable=yes,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

// -----------------------
// MNG Video Windows
// -----------------------
function loadEnergyTipsVideo() {
    window.open(webRoot + '/video/energysavingtips/index.asp?c=1&ap=on','','width=780,height=540,top=0,left=0,resizeable=no,scrollbars=no,status=no,toolbar=no,menubar=no,location=no');
}

function loadHeatingSystemVideo() {
    window.open(webRoot + '/video/heatingsystem/index.asp?c=1&ap=on','','width=780,height=540,top=0,left=0,resizeable=no,scrollbars=no,status=no,toolbar=no,menubar=no,location=no');
}

function loadGasAppliancesVideo() {
    window.open(webRoot + '/video/gasappliances/index.asp?c=1&ap=on','','width=780,height=540,top=0,left=0,resizeable=no,scrollbars=no,status=no,toolbar=no,menubar=no,location=no');
}

function loadEnergyAuditVideo() {
    window.open(webRoot + '/video/energyaudit/index.asp?c=1&ap=on','','width=780,height=540,top=0,left=0,resizeable=no,scrollbars=no,status=no,toolbar=no,menubar=no,location=no');
}

function loadCaulkingInsulationVideo() {
    window.open(webRoot + '/video/caulking/index.asp?c=1&ap=on','','width=780=,height=540,top=0,left=0,resizeable=no,scrollbars=no,status=no,toolbar=no,menubar=no,location=no');
}

function loadWaterConservationVideo() {
    window.open(webRoot + '/video/hotwater/index.asp?c=1&ap=on','','width=780,height=540,top=0,left=0,resizeable=no,scrollbars=no,status=no,toolbar=no,menubar=no,location=no');
}

function loadIndoorAirQualityVideo() {
    window.open(webRoot + '/video/iaq/index.asp?c=1&ap=on','','width=780,height=540,top=0,left=0,resizeable=no,scrollbars=no,status=no,toolbar=no,menubar=no,location=no');
}

function loadPreventingIceDamsVideo() {
    window.open(webRoot + '/video/icedams/index.asp?c=1&ap=on','','width=780,height=540,top=0,left=0,resizeable=no,scrollbars=no,status=no,toolbar=no,menubar=no,location=no');
}

function loadNaturalGasSafetyVideo() {
    window.open(webRoot + '/video/safety/index.asp?c=1&ap=on','','width=780,height=540,top=0,left=0,resizeable=no,scrollbars=no,status=no,toolbar=no,menubar=no,location=no');
}

// -----------------------
// demos
// -----------------------
function loadOLBDemo() {
window.open(webRoot + '/olbdemo/demoWelcome.asp?c=1&ap=on','olbdemo','width=755,height=430,top=0,left=0,resizeable=no,scrollbars=no,status=no,toolbar=no,menubar=no,location=no');
}

function loadmngbill() {
window.open(webRoot + '/newbill/mng_bill_format.asp?c=1&ap=on','','width=200,height=200,top=0,left=0,resizeable=no,scrollbars=no,status=no,toolbar=no,menubar=no,location=no');
}

function loadCEHGbill() {
window.open(webRoot + '/newbill/cehg_bill_format.asp?c=1&ap=on','','width=200,height=200,top=0,left=0,resizeable=no,scrollbars=no,status=no,toolbar=no,menubar=no,location=no');
}

function loadSGONbill() {
window.open(webRoot + '/newbill/sgo_north_bill_format.asp?c=1&ap=on','','width=200,height=200,top=0,left=0,resizeable=no,scrollbars=no,status=no,toolbar=no,menubar=no,location=no');
}

function loadSGOSbill() {
window.open(webRoot + '/newbill/sgo_south_bill_format.asp?c=1&ap=on','','width=200,height=200,top=0,left=0,resizeable=no,scrollbars=no,status=no,toolbar=no,menubar=no,location=no');
}

function loadMSbill() {
window.open(webRoot + '/newbill/ms_bill_format.asp?c=1&ap=on','','width=200,height=200,top=0,left=0,resizeable=no,scrollbars=no,status=no,toolbar=no,menubar=no,location=no');
}

function loadLAbill() {
window.open(webRoot + '/newbill/la_bill_format.asp?c=1&ap=on','','width=200,height=200,top=0,left=0,resizeable=no,scrollbars=no,status=no,toolbar=no,menubar=no,location=no');
}

function loadOKbill() {
window.open(webRoot + '/newbill/ok_bill_format.asp?c=1&ap=on','','width=200,height=200,top=0,left=0,resizeable=no,scrollbars=no,status=no,toolbar=no,menubar?no,location=no');
}

function loadtylerbill() {
window.open(webRoot + '/newbill/tyler_bill_format.asp?c=1&ap=on','','width=200,height=200,top=0,left=0,resizeable=no,scrollbars=no,status=no,toolbar=no,menubar=no,location=no');
}

function loadHOUbill() {
window.open(webRoot + '/newbill/hou_bill_format.asp?c=1&ap=on','','width=200,height=200,top=0,left=0,resizeable=no,scrollbars=no,status=no,toolbar=no,menubar=no,location=no');
}

function loadARbill() {
window.open(webRoot + '/newbill/ar_bill_format.asp?c=1&ap=on','','width=200,height=200,top=0,left=0,resizeable=no,scrollbars=no,status=no,toolbar=no,menubar=no,location=no');
}

function loadBillInsert() {
window.open(webRoot + '/ad/adnav.asp','','width=800,height=800,top=0,left=0,resizeable=no,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

function loadMNRatesTariffs() {
window.open(webRoot + '/aboutus/Minnesota/index.asp','','width=770,height=800,top=0,left=0,resizeable=no,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

function loadOLBFaq() {
window.open(webRoot + '/ocss/olb_faq.asp','','width=400,height=400,top=0,left=0,resizeable=no,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

function loadOnlineHelp() {
window.open(webRoot + '/ocss/online_help.asp','','width=400,height=400,top=0,left=0,resizeable=no,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

//------------------------
// MNG Application popups
//------------------------
function loadWaterHeater() {
window.open(webRoot + '/heating_cooling/water_heater_feature.asp','','width=460,height=530,top=0,left=0,resizeable=no,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

function loadCarrier() {
window.open(webRoot + '/heating_cooling/carrier_model_guide.asp','','width=400,height=400,top=0,left=0,resizeable=no,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

function loadFP() {
window.open(webRoot + '/heating_cooling/fp_guide.asp','','width=400,height=400,top=0,left=0,resizeable=no,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

function loadGlossary() {
window.open(webRoot + '/heating_cooling/glossary.asp','','width=550,height=400,top=0,left=0,resizeable=no,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

function loadHeatingRebateFAQ() {
window.open(webRoot + '/heating_cooling/Heating_Rebate_FAQ.asp','','width=550,height=400,top=0,left=0,resizeable=no,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

function loadEnergyAuditFAQ() {
window.open(webRoot + '/heating_cooling/Energy_audit_FAQ.asp','','width=550,height=400,top=0,left=0,resizeable=no,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

function loadHeatShare() {
window.open(webRoot + '/secure/heatshare.asp','','width=660,height=580,top=0,left=0,resizeable=no,scrollbars=no,status=no,toolbar=no,menubar=no,location=no');
}

//test
function loadmcn() {
window.open(webRoot + '/mcn.htm?c=1&ap=on','olbdemo','width=200,height=200,top=0,left=0,resizeable=no,scrollbars=no,status=no,toolbar=no,menubar=no,location=no');
}

function loadCSRFAQ() {
window.open(webRoot + '/ocss/olb_Faq.asp','help','width=400,height=400,top=0,left=0,resizeable=no,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}

function loadCSRStep() {
window.open(webRoot + '/ocss/online_help.asp','help','width=400,height=400,top=0,left=0,resizeable=no,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no');
}
