function showCESareas(loc) {
	var active = loc;
	var area = new Array("region","state","city");
	for (foo in area) {
		if (area[foo] == active) {
			if (document.getElementById(area[foo]) != null) {
				document.getElementById(active).style.display = "";
			}			
		} else if (area[foo] != active) {
			if (document.getElementById(area[foo]) != null) {
				document.forms['theForm'].elements["sel_" + area[foo]].value = 'd';
				document.getElementById(area[foo]).style.display = "none";
			}
		}
	}
	CESloc(active);
		
}

function CESloc(active) {
	var con;
	switch(active)
	{
	case 'region':
	  var cList = new Array("GULF","MW","MC","NC","NE","RK");
	  break;    
	case 'state':
	  var cList = new Array("AR","CO","IL","IN","LA","KS","KY","MN","MO","MI","PA","TX","WI","WV");
	  break; 
	case 'city':
	  var cList = new Array("CHICAGO","CROWN POINT","GRAND RAPIDS","DENVER","DALLAS","HOUSTON","HERSHEY","LOUISVILLE","LITTLEROCK","MADISON","MERRILLVILLE","MINNEAPOLIS","NEWIBERIA","SHREVEPORT","STLOUIS");
	  break;
	}
	
	var cArea = document.forms['theForm'].elements['sel_' + active].value;
	if (cArea != "") {
		document.getElementById(cArea).style.display = "inline";
	}
	for (con in cList) {
		if (cList[con] != cArea) {
			if (document.getElementById(cList[con]) != null) {
				document.getElementById(cList[con]).style.display = "none";
			}
		}
	}
}
	
function datecount(date) {
  now = new Date();
  x = date;
  eventDate = new Date(x);
  msEachDay = 24 * 60 * 60 * 1000;
  
  // Step 1:
  daysRemaining = (eventDate.getTime() - now.getTime()) / msEachDay;
  // Step 2:
  daysRemaining = Math.round(daysRemaining + 1);
  // Step 3:
  if (daysRemaining >= 1) {
	document.write ("<table cellpadding='0' cellspacing='0' border='0' style='width:190px; height:35px; border:solid 1px black; background-color:#e9e9cf;'>");
	document.write ("<tr>")
  	document.write ("<td valign='top' style='padding-left:8px;font-size:32px;font-family;times;color:red;'>" + daysRemaining + "</td>");
  	document.write ("<td valign='top' style='padding:8px 2px;font-size:11px;font-family:arial;'>");  
  	if (daysRemaining == 1) {
  		document.write (" <b>day</b> left to submit your rebate forms!");
  	} else {
  		document.write (" <b>days</b> left to submit your rebate forms!");
  	}
  	document.write ("</td>");
	document.write ("</tr>");
	document.write ("</table>");
  } else if (daysRemaining == 0) {
  	document.write ("<table cellpadding='0' cellspacing='0' border='0' style='width:190px; height:35px; border:solid 1px black; background-color:#e9e9cf;'>");
	document.write ("<tr>")
  	document.write ("<td valign='top' style='padding:8px 2px;font-size:11px;font-family:arial;'>");  
  	document.write (" <b>Today</b> is the last day to submit your rebate forms!");
  	document.write ("</td>");
	document.write ("</tr>");
	document.write ("</table>");
  }
}

function rebatecount() {
  now = new Date();
  eventDate = new Date("December 15, 2008");
  msEachDay = 24 * 60 * 60 * 1000 ;

  // Step 1:
  daysRemaining = (eventDate.getTime() - now.getTime()) / msEachDay;
  // Step 2:
  daysRemaining = Math.round(daysRemaining + 1);
  // Step 3:
  if (daysRemaining >= 1) {
	document.write ("<table cellpadding='0' cellspacing='0' border='0' style='width:190px; height:35px; border:solid 1px black; background-color:#e9e9cf;'>");
	document.write ("<tr>")
  	document.write ("<td valign='top' style='padding-left:8px;font-size:32px;font-family;times;color:red;'>" + daysRemaining + "</td>");
  	document.write ("<td valign='top' style='padding:8px 2px;font-size:11px;font-family:arial;'>");  
  	if (daysRemaining == 1) {
  		document.write (" <b>day</b> left to submit your rebate forms!");
  	} else {
  		document.write (" <b>days</b> left to submit your rebate forms!");
  	}
  	document.write ("</td>");
	document.write ("</tr>");
	document.write ("</table>");
  } else if (daysRemaining == 0) {
  	document.write ("<table cellpadding='0' cellspacing='0' border='0' style='width:190px; height:35px; border:solid 1px black; background-color:#e9e9cf;'>");
	document.write ("<tr>")
  	document.write ("<td valign='top' style='padding:8px 2px;font-size:11px;font-family:arial;'>");  
  	document.write (" <b>Today</b> is the last day to submit your rebate forms!");
  	document.write ("</td>");
	document.write ("</tr>");
	document.write ("</table>");
  }
}

// Show and hide Contact info 
function iExp(sa) {
	document.getElementById(sa + "-1").style.display = 'none';
	document.getElementById(sa + "-2").style.display = '';
	document.getElementById(sa + "-3").style.display = '';
}
function iCol(sa) {
	document.getElementById(sa + "-1").style.display = '';
	document.getElementById(sa + "-2").style.display = 'none';
	document.getElementById(sa + "-3").style.display = 'none';
}

// Show and hide MN Payment Locations 
function showPayStations() {
	var con;
	var cList = new Array("0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22"); 
			
	var cArea = document.forms['theForm'].elements['servicearea'].value;
	if (cArea != "") {
		document.getElementById(cArea).style.display = "inline";
	}
	for (con in cList) {
		if (cList[con] != cArea) {
			if (document.getElementById(cList[con]) != null) {
				document.getElementById(cList[con]).style.display = "none";
			}
		}
	}
}
	
	
// Show and hide MN Developer Contacts 
function showDevContacts() {
	var con;
	var cList = new Array("DOLAN","HAGER","HOUDE","LISHER","MONSON","SARGENT","SPANTON","WATSON");
	var cArea = document.forms['theForm'].elements['RepArea'].value;
	if (cArea != "") {
		document.getElementById(cArea).style.display = "inline";
	}
	for (con in cList) {
		if (cList[con] != cArea) {
			if (document.getElementById(cList[con]) != null) {
				document.getElementById(cList[con]).style.display = "none";
			}
		}
	}
}

// Show and hide Contact Info by Service Area 
function showCinfo() {
	var cinfo;
	var saList=new Array("DF","AR","HO","LA","MN","MS","OK","TX","OT"); 
	var thisSA = document.forms['theForm'].elements['servicearea'].value;
	if (thisSA != "") {
    	document.getElementById(thisSA).style.display = "inline";
    }
    for (cinfo in saList) {
    	if (saList[cinfo] != thisSA) {
    		if (document.getElementById(saList[cinfo]) != null) {
				document.getElementById(saList[cinfo]).style.display = "none";
			}
		}
	}
}

// Show and hide Grills and Lights Dealers by Area 
function showGLdealers() {
	var cinfo;
	var saList=new Array("DF","AL","CO","EB","HU","KA","LC","HO","SP","TW"); 
	var thisSA = document.forms['theForm'].elements['servicearea'].value;
	if (thisSA != "") {
    	document.getElementById(thisSA).style.display = "inline";
    }
    for (cinfo in saList) {
    	if (saList[cinfo] != thisSA) {
    		if (document.getElementById(saList[cinfo]) != null) {
				document.getElementById(saList[cinfo]).style.display = "none";
			}
		}
	}
}

// Show and hide Bill pay FAQ 
function showFAQ(value) {
	var cinfo;
	var saList=new Array("t1","t2","t3","t4","t5","t6","t7","t8"); 
	var thisSA = value;
	if (thisSA != "") {
    	document.getElementById(thisSA).style.display = "inline";
    }
    for (cinfo in saList) {
    	if (saList[cinfo] != thisSA) {
    		if (document.getElementById(saList[cinfo]) != null) {
				document.getElementById(saList[cinfo]).style.display = "none";
			}
		}
	}
}
		
// Display email address field for feedback form  
function showemail() {
  if (document.forms['theForm'].elements.reply[0].checked) {
    email.style.display = "block";
    document.forms['theForm'].elements['emailaddress'].value = "";
  } else if (document.forms['theForm'].elements.reply[1].checked) {
    email.style.display = "none";
    document.forms['theForm'].elements['emailaddress'].value = "http://www.centerpointenergy.com/staticfiles/CNP/Common/SiteAssets/js/na@none.com";
  }
}

// Check other amount option on Customar Care form  
function checkAmount() {
  if (document.forms['theForm'].elements.DonationAmount[2].checked) {
	aOther.style.display = "";
	dOther.style.display = "none";
	document.forms['theForm'].elements['OtherAmount'].value = "";
  } else {
    aOther.style.display = "none";
	dOther.style.display = "";
	document.forms['theForm'].elements['OtherAmount'].value = "0";
  }
}

// Hides email address field on feedback form on reset 
function resetemail() {
    email.style.display = "none";
    document.forms['theForm'].elements['emailaddress'].value = "http://www.centerpointenergy.com/staticfiles/CNP/Common/SiteAssets/js/na@none.com";
}

// Display fields for customer service on contact form  
function showacctinfo() {
  if (document.forms['theForm'].elements['department'].value == "Customer Service") {
    acctinfo.style.display = "block";
    document.forms['theForm'].elements['acctnumber'].value = "";
    document.forms['theForm'].elements['street'].value = "";
    document.forms['theForm'].elements['city'].value = "";
    document.forms['theForm'].elements['state'].value = "";
    document.forms['theForm'].elements['zip'].value = "";
  } else {
    acctinfo.style.display = "none";
    document.forms['theForm'].elements['acctnumber'].value = "na";
    document.forms['theForm'].elements['street'].value = "na";
    document.forms['theForm'].elements['city'].value = "na";
    document.forms['theForm'].elements['state'].value = "TX";
    document.forms['theForm'].elements['zip'].value = "na"; 
  }
}

// Hides customer service files on reset for contact form
function resetacctinfo() {
    acctinfo.style.display = "none";
    document.forms['theForm'].elements['acctnumber'].value = "na";
    document.forms['theForm'].elements['street'].value = "na";
    document.forms['theForm'].elements['city'].value = "na";
    document.forms['theForm'].elements['state'].value = "TX";
    document.forms['theForm'].elements['zip'].value = "na"; 
}

// Form Validation Logic   
function validateForm(theForm) {
	for(var i = 0; i < theForm.elements.length; i++) {
	
		// Loop through all hidden fields to perform validation...
		if(theForm.elements[i].type == "hidden") {
			if((j = theForm.elements[i].name.lastIndexOf("_")) != -1) {
				fieldName = theForm.elements[i].name.substring(0, j);
				if(theForm.elements[fieldName] != null) {
					fieldValue = theForm.elements[fieldName].value;
					rule = theForm.elements[i].name.substring(j + 1);
					message = theForm.elements[i].value;
					
					switch(rule) {
						// -------------------------
						// Handle required fields...
						// -------------------------
						case "required":
							var fieldTypeToValidate = theForm.elements[fieldName].type;
							switch(fieldTypeToValidate) {
							
								// Validate types: 'text', 'textarea', 'passsword'
								case "text": case "textarea": case "password":
								  if(isEmpty(fieldValue)) {
										theForm.elements[fieldName].select();
										theForm.elements[fieldName].focus();
										alert(message);
										return false;
									}
									break;
								// Validate types: 'select-one', 'select-multiple'
								case "select-one": case "select-multiple":
									if(theForm.elements[fieldName].selectedIndex == -1 || theForm.elements[fieldName].selectedIndex == 0) {
										theForm.elements[fieldName].focus();
										alert(message);
										return false;
									}		
									break;
								
								// Validate types: 'radio', 'checkbox', 'undefined' (to handle checkboxes and radio buttons)
								default: 
									var checked = false;
									for(var k=0; k<theForm.elements[fieldName].length; k++) {
										if(theForm.elements[fieldName][k].checked) {
											checked = true;
											break;
										}
									}
									if(!checked) {
										alert(message);
										return false;
									}
									break;
									
							} // end-switch(fieldTypeToValidate)
							break;
							
						// ----------------------------------
						// Handle fields that expect dates...
						// ----------------------------------				
						case "date":
							if(fieldValue != null || fieldValue != "") {
								if(!isDate(fieldValue)) {
									theForm.elements[fieldName].select();
									theForm.elements[fieldName].focus();
									alert( message );
									return false;
								}
							}
							break;
							
						// --------------------------------------------------
						// Handle fields that expect alpha characters only...
						// --------------------------------------------------
						case "letters":
							if(fieldValue != null || fieldValue != "") {
								if(!isLetters(fieldValue)) {
									theForm.elements[fieldName].select();
									theForm.elements[fieldName].focus();
									alert( message );
									return false;
								}
							}
							break;
							
						// ----------------------------------------------------
						// Handle fields that expect numeric characters only...
						// ----------------------------------------------------
						case "digits":
							if(fieldValue != null || fieldValue != "") {
								if(!isDigits(fieldValue)) {
									theForm.elements[fieldName].select();
									theForm.elements[fieldName].focus();
									alert(message);
									return false;
								}
							}
							break;
							
						// ----------------------------------------------------------
						// Handle fields that expect alpha-numeric characters only...
						// ----------------------------------------------------------	
						case "lettersOrDigits":
							if(fieldValue != null || fieldValue != "") {
								if ( !isLettersOrDigits(fieldValue) ) {
									theForm.elements[fieldName].select();
									theForm.elements[fieldName].focus();
									alert(message);
									return false;
								}
							}
							break;							
							
						// -----------------------------------------------------------------
						// Handle fields that expect any characters... [always returns true]
						// -----------------------------------------------------------------
						case "characters":
							if(fieldValue != null || fieldValue != "") {
								if (!isCharacters(fieldValue)) {
									theForm.elements[fieldName].select();
									theForm.elements[fieldName].focus();
									alert(message);
									return false;
								}
							}
							break;

						// ----------------------------------------------------------------------------
						// Handle fields that expect any characters or numeric... [always returns true]
						// ----------------------------------------------------------------------------
						case "charactersOrDigits":
							if(fieldValue != null || fieldValue != "") {
								if(!isCharactersOrDigits(fieldValue)) {
									theForm.elements[fieldName].select();
									theForm.elements[fieldName].focus();
									alert(message);
									return false;
								}
							}
							break;
							
						// -----------------------------------------------
						// Handle fields that expect a specified length...
						// -----------------------------------------------
						case "length":
							if(fieldValue != null || fieldValue != "") {
								if(!inLength(fieldValue, 0, 0)) {
									theForm.elements[fieldName].select();
									theForm.elements[fieldName].focus();
									alert(message);
									return false;
								}
							}
							break;
							
						// ----------------------------------------------
						// Handle fields that expect a specified range...
						// ----------------------------------------------
						case "range":
							if(fieldValue != null || fieldValue != "") {
								if (!isRange(fieldValue, 0, 0)) {
									theForm.elements[fieldName].select();
									theForm.elements[fieldName].focus();
									alert(message);
									return false;
								}
							}
							break;
							
						// ---------------------------------------------
						// Handle fields that expect an email address...
						// ---------------------------------------------
						case "email":
						  if(fieldValue != null || fieldValue != "") {
						  	if(!isEmail(fieldValue)) {
									theForm.elements[fieldName].select();
									theForm.elements[fieldName].focus();
									alert(message);
									return false;
								}
							}
							break;
							
						// ---------------------------------------------
						// Handle fields to check for emergency codes...
						// ---------------------------------------------
						// Required
						case "emergency":
						  if(fieldValue != null || fieldValue != "") {
						  	if(!isEmergency(fieldValue)) {
									theForm.elements[fieldName].select();
									theForm.elements[fieldName].focus();
									return false;
								}
							}
							break;									

            // Not-required
						case "nremergency":
					  	if(!isNREmergency(fieldValue)) {
								theForm.elements[fieldName].select();
								theForm.elements[fieldName].focus();
								return false;
							}
							break;									

						// ------------------------------------
						// Handle fields that expect a float...
						// ------------------------------------
						case "float":
							if(fieldValue != null || fieldValue != "") {
								if(!isFloat(fieldValue)) {
									theForm.elements[fieldName].select();
									theForm.elements[fieldName].focus();
									alert(message);
									return false;
								}
							}
							break;
							
					} // end-switch(rule)

				}
			}
		} // end-if(theForm.elements[i].type == "hidden")
		
		else if(theForm.elements[i].type == "textarea") {			
			  var str = theForm.elements[i].value;
			  var regEx = /\b(leak|smell|emergency|odor|carbon|monoxide|co|c.o.)\b/im;
			  //var regEx = /leak|smell|emergency|odor|carbon|monoxide|co | co | co\z/im;
			  if (regEx.test(str)) {
				alert("Terms indicating an emergency condition, such as a gas leak, were discovered in your message.\n\n" +
						  "Emergency conditions such as this must be reported by telephone from a safe location, away from where the condition exists.");
				return false;
			  } else {
				return true;                  //return false for testing.
			  }			
		} // else if(theForm.elements[i].type == "textarea") {
	} // end-for
	
	//disable "submit" button after validation completed
	theForm.submitButton.disabled = true;
	theForm.resetButton.disabled = true;
	
	return true;
}

function isLetter(source) {
	if ( source == null || source.length != 1 ) {
		return false;
	}
	else {
		if ( (source >= 'A' && source <= 'Z') || (source >= 'a' && source <= 'z') ) {
			return false;
		}
		
		return true;
	}
}

function isLetters(source) {
	if ( source == null || source.length == 0) {
		return false;
	}
	else {
		for (var i = 0; i < source.length; i++) {
			if ( !isLetter(source.charAt(i)) ) {
				return false;
			}
		}
		
		return true;
	}
}

function isDigit(source) {
	if ( source == null || source.length != 1 ) {
		return false;
	}
	else {
		if ( source < '0' || source > '9' ) {
			return false;
		}

		return true;
	}
}

function isDigits(source) {
	if ( source == null || source.length == 0 ) {
		return false;
	}
	else {
		for (var i = 0; i < source.length; i++) {
			if ( !isDigit(source.charAt(i)) ) {
				return false;
			}
		}
		
		return true;
	}
}

function isCharacters(source) {
	return true;
}

function isLettersOrDigits(source) {
	if ( source == null || source.length == 0) {
		return false;
	}
	else {
		for (var i = 0; i < source.length; i++) {
			if ( !isLetter(source.charAt(i)) && !isDigit(source.charAt(i)) ) {
				return false;
			}
		}
	}
	
	return true;
}

function isCharactersOrDigits(source) {
	return true;
}

function isFloat(source) {
	if ( source.length == 0) {
		return false;
	}
	else {
		for (var i = 0; i < source.length; i++) {
			if ( !isDigit(source.charAt(i)) && source.charAt(i) != "." ) {
				return false;
			}
		}
	}
	
	return true;
}

function isDate(source) {
	if ( source.length == 0) {
		return false;
	}
	else {
		if ( isNaN(Date.parse(source)) ) {
			return false;
		}
		else {
			return true;
		}
	}
}

function inLength(source, min, max) {
	if ( source == null || source.length < min || source.length > max ) {
		return false;
	}
	else {
		return true;
	}
}

function inRange(source, min, max) {
	return true;
}

function trim(source) {
	for (var i = 0; i < source.length; i++) {
		if ( source.charAt(i) != ' ' ) {
			source = source.substring(i);
			break;
		}
	}

	for (var i = source.length - 1; i >= 0 ; i--) {
		if ( source.charAt(i) != ' ' ) {
			source = source.substring(0, i + 1);
			break;
		}
	}

	return source;
}

function isEmail(source) {
  if ( source.length == 0) {
		return false;
	} else {
	  var regemail = /^[a-z][a-z-_0-9\.]+@[a-z-_=>0-9\.]+\.[a-z]{2,8}$/i;
    return regemail.test(source);
	} 	
}

function isEmpty(source) {
	var re = /\s/g;                 // Match any white space including space, tab, form-feed, etc. 
	re.multiline = true;            // IE support
	var str = source.replace(re, "");
	if(str.length == 0) {
	  return true;
	} else {
		return false;
	}
}

function isEmergency(source) {
  //var field = form.text;
  var str = source;
  var regEx = /leak|smell|emergency|odor|carbon|monoxide|co | co | co\z/im;
  if ( source.length == 0) {
		alert(message);
		return false;
	} else if (regEx.test(str)) {
    alert("Terms indicating an emergency condition, such as a gas leak, were discovered in your message.\n\n" +
		      "Emergency conditions such as this must be reported by telephone from a safe location, away from where the condition exists.");
    return false;
  } else {
    return true;                  //return false for testing.
  }
}

function isNREmergency(source) {
  //var field = form.text;
  var str = source;
  var regEx = /leak|smell|emergency|odor|carbon|monoxide|co | co | co\z/im;
  if (regEx.test(str)) {
    alert("Terms indicating an emergency condition, such as a gas leak, were discovered in your message.\n\n" +
		      "Emergency conditions such as this must be reported by telephone from a safe location, away from where the condition exists.");
    return false;
  } else {
    return true;                  //return false for testing.
  }
}
function validateLocalEmail(form)
{
	fieldValue = form.elements['EmailAddress'].value;
	
	if (!form.elements['PreferredContact'].checked)
	{
		if(fieldValue != null || fieldValue != "") 
		{
			if(!isEmail(fieldValue)) 
			{
				theForm.elements['EmailAddress'].select();
				theForm.elements['EmailAddress'].focus();
				
				alert('Please enter a valid email address.');
				return false;
			}
			if (fieldValue != form.elements['Email-AddressConfirm'].value)
			{
				alert('The confirmation email address doesn\'t match.\nPlease enter a valid email address.');
				return false;
			}
		}
	}

	return validateForm(form);
}
var dhtmlgoodies_slideSpeed = 10;	// Higher value = faster
var dhtmlgoodies_timer = 10;	// Lower value = faster

var objectIdToSlideDown = false;
var dhtmlgoodies_activeId = false;
var dhtmlgoodies_slideInProgress = false;
function showHideContent(e,inputId)
{
	if(dhtmlgoodies_slideInProgress)return;
	dhtmlgoodies_slideInProgress = true;
	if(!inputId)inputId = this.id;
	inputId = inputId + '';
	var numericId = inputId.replace(/[^0-9]/g,'');
	var answerDiv = document.getElementById('dhtmlgoodies_a' + numericId);

	objectIdToSlideDown = false;
	
	if(!answerDiv.style.display || answerDiv.style.display=='none'){		
		if(dhtmlgoodies_activeId &&  dhtmlgoodies_activeId!=numericId){			
			objectIdToSlideDown = numericId;
			slideContent(dhtmlgoodies_activeId,(dhtmlgoodies_slideSpeed*-1));
		}else{
			
			answerDiv.style.display='block';
			answerDiv.style.visibility = 'visible';
			
			slideContent(numericId,dhtmlgoodies_slideSpeed);
		}
	}else{
		slideContent(numericId,(dhtmlgoodies_slideSpeed*-1));
		dhtmlgoodies_activeId = false;
	}	
}

function slideContent(inputId,direction)
{
	
	var obj =document.getElementById('dhtmlgoodies_a' + inputId);
	var contentObj = document.getElementById('dhtmlgoodies_ac' + inputId);
	height = obj.clientHeight;
	if(height==0)height = obj.offsetHeight;
	height = height + direction;
	rerunFunction = true;
	if(height>contentObj.offsetHeight){
		height = contentObj.offsetHeight;
		rerunFunction = false;
	}
	if(height<=1){
		height = 1;
		rerunFunction = false;
	}

	obj.style.height = height + 'px';
	var topPos = height - contentObj.offsetHeight;
	if(topPos>0)topPos=0;
	contentObj.style.top = topPos + 'px';
	if(rerunFunction){
		setTimeout('slideContent(' + inputId + ',' + direction + ')',dhtmlgoodies_timer);
	}else{
		if(height<=1){
			obj.style.display='none'; 
			if(objectIdToSlideDown && objectIdToSlideDown!=inputId){
				document.getElementById('dhtmlgoodies_a' + objectIdToSlideDown).style.display='block';
				document.getElementById('dhtmlgoodies_a' + objectIdToSlideDown).style.visibility='visible';
				slideContent(objectIdToSlideDown,dhtmlgoodies_slideSpeed);				
			}else{
				dhtmlgoodies_slideInProgress = false;
			}
		}else{
			dhtmlgoodies_activeId = inputId;
			dhtmlgoodies_slideInProgress = false;
		}
	}
}



function initShowHideDivs()
{
	var divs = document.getElementsByTagName('DIV');
	var divCounter = 1;
	for(var no=0;no<divs.length;no++){
		if(divs[no].className=='dhtmlgoodies_question'){
			divs[no].onclick = showHideContent;
			divs[no].id = 'dhtmlgoodies_q'+divCounter;
			var answer = divs[no].nextSibling;
			while(answer && answer.tagName!='DIV'){
				answer = answer.nextSibling;
			}
			answer.id = 'dhtmlgoodies_a'+divCounter;	
			contentDiv = answer.getElementsByTagName('DIV')[0];
			contentDiv.style.top = 0 - contentDiv.offsetHeight + 'px'; 	
			contentDiv.className='dhtmlgoodies_answer_content';
			contentDiv.id = 'dhtmlgoodies_ac' + divCounter;
			answer.style.display='none';
			answer.style.height='1px';
			divCounter++;
		}		
	}	
}
window.onload = initShowHideDivs;

function concatenateTelephoneNumber()
{
	valuOfPhoneNumber1 = document.forms['theForm'].elements["Phone-Number1"].value;
	valuOfPhoneNumber2 = document.forms['theForm'].elements["Phone-Number2"].value;
	valuOfPhoneNumber3 = document.forms['theForm'].elements["Phone-Number3"].value; 
	document.forms['theForm'].elements["Phone-Number"].value = valuOfPhoneNumber1 + "-" + valuOfPhoneNumber2 + "-" + valuOfPhoneNumber3;
}
