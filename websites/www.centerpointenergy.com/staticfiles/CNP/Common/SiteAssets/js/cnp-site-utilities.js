// ----------------------------------------------------------------------
// CenterPoint Energy
// CNP Public Site - jQuery based JS functions for CNP site content.
// ----------------------------------------------------------------------
jQuery.noConflict();
jQuery(document).ready(function() {
	// Removes IE limitation of undefined 'console' object
	//if (typeof console === "undefined" || typeof console.log === "undefined") {
	//    console.log = function(log_message) {
	//        //alert(log_message);
	//    };
	//}
	//if(!window.console){ window.console = { log: function(){} }; }
	
	
	
	// ----------------------------------------------------------------------
	// Functions
	// ----------------------------------------------------------------------
	//delay execution of a function
	var delayedExecution = function(after, fn) {
		var timer;
		return function() {
			timer && clearTimeout(timer);
			timer = setTimeout(fn, after);
		};
	};

	//temporarily disable page scrolling
	var scrollStopper = delayedExecution(500, function() {
		//console.log('stopped it');
	});
	
	var randomIntegerFromRange = function(min,max) {
	    return ~~(Math.random()*(max-min+1)+min);
	};
	
	
	
	// ----------------------------------------------------------------------
	// Extensions (i.e. aliases to jQuery prototype proper)
	// ----------------------------------------------------------------------
	// Check if an element exists
	jQuery.fn.exists = function() { return this.length };
	
	// Append and display the 'href' location value for anchor tags
	jQuery.fn.displayLinkLocation = function() {
	    return this.filter('a').each(function(){
			jQuery(this).append(
			    ' (' + jQuery(this).attr('href') + ')'
			);
	    });
	};
	// jQuery('http://www.centerpointenergy.com/staticfiles/CNP/Common/SiteAssets/js/a.test').displayLinkLocation();


	jQuery.fn.requireAssociateFormInputElements = function() {
		//console.log(jQuery(this).attr('id'));
		//console.log(jQuery(this).siblings().length);
	};

	
	
	// ----------------------------------------------------------------------------------------------------
	// CNP Hovering Footnote
	// ----------------------------------------------------------------------------------------------------
	if (jQuery("a.footnote").exists()) {
		jQuery("a.footnote").footnotepopup();
	} // end-if (jQuery("a.footnote").exists())

	

	// ----------------------------------------------------------------------------------------------------
	// CNP Multiple Toggle Effect
	// http://www.centerpointenergy.com/promo/earthday/
	// ----------------------------------------------------------------------------------------------------
    //Enable the function for the multiple toggle animation effect
    var multipleToggleEffect = function(anElement) {

    	//if the requested element exists
        if(jQuery(anElement).exists()) {

        	//look for specific next level siblings to affect
        	var anElementWithDivTagSiblings = anElement + '> div';
        	var anElementWithAnchorTagSiblings = anElement + '> a';

        	//if those requested next level sibling elements exist, apply the "multiple toggle" effect
        	if((jQuery(anElementWithDivTagSiblings).exists()) && (jQuery(anElementWithAnchorTagSiblings).exists())) {
				jQuery(anElementWithDivTagSiblings).hide();
				jQuery(anElementWithAnchorTagSiblings).click(function() {
					jQuery(this).next('div').slideToggle().siblings('div:visible').slideUp();
					
					//To prevent "scroll to top" when clicking on links, add the "return false"
					return false;
				});
        	} // end-if((jQuery(anElementWithDivTagSiblings).exists()) && (jQuery(anElementWithAnchorTagSiblings).exists()))

        }  //end-if(jQuery(anElement).exists())

    };

    //Call elements requiring multiple toggle effect
    multipleToggleEffect('div.promo-earthday-toggle-container');
    multipleToggleEffect('div.promo-earthday-state-selector');
    multipleToggleEffect('div.promo-earthday-faq-selector');
    multipleToggleEffect('div.promo-natgasgenerator-toggle-container');
    multipleToggleEffect('div.promo-natgasgenerator-answer');
    multipleToggleEffect('div.mao-faq-toggle-container');
    multipleToggleEffect('div.mao-faq-category');

    
    
	// ----------------------------------------------------------------------------------------------------
	// CNP Generic Form Buttons Functions
	// ----------------------------------------------------------------------------------------------------
	// Print Button
	jQuery("#printButton").click(function() {
		window.print();
	});
	
	
	
	// ----------------------------------------------------------------------------------------------------------------------------
	// jQuery Input Validation Extensions - Customer Form Method
	// ----------------------------------------------------------------------------------------------------------------------------
	// HTML Form - select option (with a value)
	jQuery.validator.addMethod("valueNotEquals", function(value, element, argument) {
		return argument != value;
	}, "Value must not equal argument.");
	
	
	
	// ----------------------------------------------------------------------------------------------------------------------------
	// CNP Email Us Form
	// http://www.centerpointenergy.com/about/contact/email/
	// ----------------------------------------------------------------------------------------------------------------------------
	jQuery("#EmailUsForm").validate({
		rules: {
			FirstName: { required: true, minlength: 2 },
			LastName: { required: true, minlength: 2 },
			EmailAddress: { required: true, email: true }
		},
		messages: {
				FirstName: "Please enter the first name.",
				LastName: "Please enter the last name.",
				EmailAddress: "Please enter the email address."
		},
		submitHandler: function (form) {
	          //instead of "form.submit();", just open an alert box
	          alert('Form validation completed and submitted!');
		}
	});
	
	
	
	// ----------------------------------------------------------------------------------------------------------------------------
	// CNP Educator Resources Request Form
	// http://www.centerpointenergy.com/community/safetyandeducation/educatorresources/requestform/HO/
	// ----------------------------------------------------------------------------------------------------------------------------
	//NOTE: The following two IDs are also used on the Pre-Exisiting Claims form (listed in the next section)
	var hiddenCaptchaFieldCheck = jQuery('#Was_Captcha_Entered').val();

	if (hiddenCaptchaFieldCheck === 'yes'){
		jQuery('#EducatorResourcesRequestFormAlt :input').prop('disabled', false);
	} else {
		jQuery('#EducatorResourcesRequestFormAlt :input').prop('disabled', true);
	}

	if (jQuery("#EducatorResourcesRequestFormAlt").exists()) {
		jQuery("#EducatorResourcesRequestFormAlt").validate({
    		ignore: [],
    		rules: {
				FirstName: { required: true },
				LastName: { required: true },
				EmailAddress: { required: true, email: true },
				TelephoneNumber: { required: true, phoneUS: true },
				MailingAddress: { required: true },
				City: { required: true },
				State: { required: true },
				ZipCode: {
					required: true,
					zipcodeUS: true,
					digits: true,
					minlength: 5,
					maxlength: 5
				},
				School: { required: true },
				District: { required: true },
				OrderFor: { required: true }
    		},
    		messages: {
				FirstName: "Please enter the first name.",
				LastName: "Please enter the first name.",
				EmailAddress: "Please enter the email address.",
				TelephoneNumber: "Please enter the phone number.",
				MailingAddress: "Please enter the street address.",
				City: "Please enter the city.",
				State: "Please select the state.",
				ZipCode: " Houston area zip code required.",
				School: "Please enter the school name.",
				District: "Please enter the school district.",
				OrderFor: "Please select the type of order."   				
    		},
			errorPlacement: function(error, element) {
				switch (element.attr("name")) {
					case 'OrderFor':
						error.insertAfter(jQuery(element).siblings(":last"));
						break;
					default:
						error.insertAfter(element);
				}
			},
			submitHandler: function (form) {
		          //instead of "form.submit();", just open an alert box
		          alert('Form validation completed and submitted!');
			}
		}); //end-jQuery("#EducatorResourcesRequestFormAlt").validate()
	} //end-if (jQuery("#EducatorResourcesRequestFormAlt").exists()	
	
	// Function expression that evaluates CenterPoint Educator Resources (CER) service area zip codes in a JSON formatted file
    var evaluateCERRZipCode = function(jsonfile) {

    	jQuery.getJSON(jsonfile, function(response) {
    		var validZipCodes = response;

    		jQuery.validator.addMethod("postalcode", function(postalcode, element) {
    			return this.optional(element) || ((postalcode.match(/^([0-9]{5})$/) && jQuery.inArray(parseInt(postalcode), response) > -1));
    		}, " Houston area zip code required.");

    	});
    	
    	jQuery("#EducatorResourcesRequestForm").validate({
			rules: {
				FirstName: { required: true },
				LastName: { required: true },
				EmailAddress: { required: true, email: true },
				TelephoneNumber: { required: true, phoneUS: true },
				StreetService: { required: true },
				CityService: { required: true },
				StateService: { required: true },
				ZipService: {
					required: true,
					postalcode: true,
					digits: true,
					minlength: 5,
					maxlength: 5
				},
				School: { required: true },
				District: { required: true },
				OrderFor: { required: true },
				MouseHouseSurprise: { digits: true, maxlength: 3, range: [1, 100] },
				MouseHouseSurpriseTeachersBigBook: { digits: true, maxlength: 3, range: [1, 100] },
				LaCasadeLosRatoncitos: { digits: true, maxlength: 3, range: [1, 100] },
				LaCasadeLosRatoncitosTeachersBigBook: { digits: true, maxlength: 3, range: [1, 100] },
				TheTruthAboutElectricityAndNaturalGas: { digits: true, maxlength: 3, range: [1, 100] },
				DontGetZapped: { digits: true, maxlength: 3, range: [1, 100] },
				ElectricalSafetyAtHomeAndWork: { digits: true, maxlength: 3, range: [1, 100] },
				ElectricAndNaturalGasSafetyAtHomeAndWork: { digits: true, maxlength: 3, range: [1, 100] },
				AnElectricalEngineerPlansAnElectricallySafeOutdoorConcert: { digits: true, maxlength: 3, range: [1, 100] },
				ElectricityFromWaterWindAndSunlight: { digits: true, maxlength: 3, range: [1, 100] },
				EnergyEfficiencyWorld: { digits: true, maxlength: 3, range: [1, 100] },
				UnMundoEficienteEnEnergia: { digits: true, maxlength: 3, range: [1, 100] },
				KnowWhatLearnAboutCareers: { digits: true, maxlength: 3, range: [1, 100] },
				KnowWhatWeCanSaveElectricity: { digits: true, maxlength: 3, range: [1, 100] },
				KnowWhatWeKnowAboutRenewableEnergy: { digits: true, maxlength: 3, range: [1, 100] },
				OnBuildingStudySkills: { digits: true, maxlength: 3, range: [1, 100] },
				OnEnergyCareers: { digits: true, maxlength: 3, range: [1, 100] },
				OnYourCareer: { digits: true, maxlength: 3, range: [1, 100] },
				PathsForElectricity: { digits: true, maxlength: 3, range: [1, 100] },
				SafeAtHomeLeoLearnsHowToUseNaturalGasWisely: { digits: true, maxlength: 3, range: [1, 100] },
				SniffyTheSniffasaurusNaturalGasSafetyActivityBook: { digits: true, maxlength: 3, range: [1, 100] },
				WeAreTalkingElectricity: { digits: true, maxlength: 3, range: [1, 100] },
				CharLemosDeElectricidad: { digits: true, maxlength: 3, range: [1, 100] },
				WhatIsUpWithStudySkills: { digits: true, maxlength: 3, range: [1, 100] },
				WhatIsUpWithYourFuture: { digits: true, maxlength: 3, range: [1, 100] },
				AboutNaturalGas: { digits: true, maxlength: 3, range: [1, 100] },
				EnergyEfficiency: { digits: true, maxlength: 3, range: [1, 100] },
				NaturalGasSafetyAndYou: { digits: true, maxlength: 3, range: [1, 100] },
				GasNaturalMedidasDeSeguridadParaUsted: { digits: true, maxlength: 3, range: [1, 100] },
				NaturalGasUseItSafely: { digits: true, maxlength: 3, range: [1, 100] },
				UnderstandingElectricity: { digits: true, maxlength: 3, range: [1, 100] },
				BeCarefulNearElectricityAToZap: { digits: true, maxlength: 3, range: [1, 100] },
				HowATurbineGeneratorWorks: { digits: true, maxlength: 3, range: [1, 100] },
				HowElectricityIsDeliveredFromTheGeneratingPlantToYou: { digits: true, maxlength: 3, range: [1, 100] },
				TheShockingTruthAboutElectricityAndNaturalGas: { digits: true, maxlength: 3, range: [1, 100] },
				Total_Students: { digits: true, maxlength: 3, range: [1, 100] },
				Total_Classrooms: { digits: true, maxlength: 3, range: [1, 100] }
			},
			messages: {
				FirstName: "Please enter the first name.",
				LastName: "Please enter the first name.",
				EmailAddress: "Please enter the email address.",
				TelephoneNumber: "Please enter the phone number.",
				StreetService: "Please enter the street address.",
				CityService: "Please enter the city.",
				StateService: "Please select the state.",
				ZipService: " Houston area zip code required.",
				School: "Please enter the school name.",
				District: "Please enter the school district.",
				OrderFor: "Please select the type of order.",
				MouseHouseSurprise: { digits: "Digits only.", maxlength: "Max 3 digits.", range: "Digit range from 1-100." },
				MouseHouseSurpriseTeachersBigBook: { digits: "Digits only.", maxlength: "Max 3 digits.", range: "Digit range from 1-100." },
				LaCasadeLosRatoncitos: { digits: "Digits only.", maxlength: "Max 3 digits.", range: "Digit range from 1-100." },
				LaCasadeLosRatoncitosTeachersBigBook: { digits: "Digits only.", maxlength: "Max 3 digits.", range: "Digit range from 1-100." },
				TheTruthAboutElectricityAndNaturalGas: { digits: "Digits only.", maxlength: "Max 3 digits.", range: "Digit range from 1-100." },
				DontGetZapped: { digits: "Digits only.", maxlength: "Max 3 digits.", range: "Digit range from 1-100." },
				ElectricalSafetyAtHomeAndWork: { digits: "Digits only.", maxlength: "Max 3 digits.", range: "Digit range from 1-100." },
				ElectricAndNaturalGasSafetyAtHomeAndWork: { digits: "Digits only.", maxlength: "Max 3 digits.", range: "Digit range from 1-100." },
				AnElectricalEngineerPlansAnElectricallySafeOutdoorConcert: { digits: "Digits only.", maxlength: "Max 3 digits.", range: "Digit range from 1-100." },
				ElectricityFromWaterWindAndSunlight: { digits: "Digits only.", maxlength: "Max 3 digits.", range: "Digit range from 1-100." },
				EnergyEfficiencyWorld: { digits: "Digits only.", maxlength: "Max 3 digits.", range: "Digit range from 1-100." },
				UnMundoEficienteEnEnergia: { digits: "Digits only.", maxlength: "Max 3 digits.", range: "Digit range from 1-100." },
				KnowWhatLearnAboutCareers: { digits: "Digits only.", maxlength: "Max 3 digits.", range: "Digit range from 1-100." },
				KnowWhatWeCanSaveElectricity: { digits: "Digits only.", maxlength: "Max 3 digits.", range: "Digit range from 1-100." },
				KnowAboutRenewableEnergy: { digits: "Digits only.", maxlength: "Max 3 digits.", range: "Digit range from 1-100." },
				OnBuildingStudySkills: { digits: "Digits only.", maxlength: "Max 3 digits.", range: "Digit range from 1-100." },
				OnEnergyCareers: { digits: "Digits only.", maxlength: "Max 3 digits.", range: "Digit range from 1-100." },
				OnYourCareer: { digits: "Digits only.", maxlength: "Max 3 digits.", range: "Digit range from 1-100." },
				PathsForElectricity: { digits: "Digits only.", maxlength: "Max 3 digits.", range: "Digit range from 1-100." },
				SafeAtHomeLeoLearnsHowToUseNaturalGasWisely: { digits: "Digits only.", maxlength: "Max 3 digits.", range: "Digit range from 1-100." },
				SniffyTheSniffasaurusNaturalGasSafetyActivityBook: { digits: "Digits only.", maxlength: "Max 3 digits.", range: "Digit range from 1-100." },
				WeAreTalkingElectricity: { digits: "Digits only.", maxlength: "Max 3 digits.", range: "Digit range from 1-100." },
				CharLemosDeElectricidad: { digits: "Digits only.", maxlength: "Max 3 digits.", range: "Digit range from 1-100." },
				WhatIsUpWithStudySkills: { digits: "Digits only.", maxlength: "Max 3 digits.", range: "Digit range from 1-100." },
				WhatIsUpWithYourFuture: { digits: "Digits only.", maxlength: "Max 3 digits.", range: "Digit range from 1-100." },
				WhoKnewTheCareersIssue: { digits: "Digits only.", maxlength: "Max 3 digits.", range: "Digit range from 1-100." },
				AboutNaturalGas: { digits: "Digits only.", maxlength: "Max 3 digits.", range: "Digit range from 1-100." },
				EnergyEfficiency: { digits: "Digits only.", maxlength: "Max 3 digits.", range: "Digit range from 1-100." },
				NaturalGasSafetyAndYou: { digits: "Digits only.", maxlength: "Max 3 digits.", range: "Digit range from 1-100." },
				GasNaturalMedidasDeSeguridadParaUsted: { digits: "Digits only.", maxlength: "Max 3 digits.", range: "Digit range from 1-100." },
				NaturalGasUseItSafely: { digits: "Digits only.", maxlength: "Max 3 digits.", range: "Digit range from 1-100." },
				UnderstandingElectricity: { digits: "Digits only.", maxlength: "Max 3 digits.", range: "Digit range from 1-100." },
				BeCarefulNearElectricityAToZap: { digits: "Digits only.", maxlength: "Max 3 digits.", range: "Digit range from 1-100." },
				HowATurbineGeneratorWorks: { digits: "Digits only.", maxlength: "Max 3 digits.", range: "Digit range from 1-100." },
				HowElectricityIsDeliveredFromTheGeneratingPlantToYou: { digits: "Digits only.", maxlength: "Max 3 digits.", range: "Digit range from 1-100." },
				TheShockingTruthAboutElectricityAndNaturalGas: { digits: "Digits only.", maxlength: "Max 3 digits.", range: "Digit range from 1-100." },
				Total_Students: { digits: "Digits only.", maxlength: "Max 3 digits.", range: "Digit range from 1-100." },
				Total_Classrooms: { digits: "Digits only.", maxlength: "Max 3 digits.", range: "Digit range from 1-100." }
			},
			errorPlacement: function(error, element) {
				switch (element.attr("name")) {
					case 'OrderFor':
						error.insertAfter(jQuery(element).siblings(":last"));
						break;
					default:
						error.insertAfter(element);
				}
			}
		});
	};

	// Call function to evaluate whether given zip code is listed in CenterPoint Educator Resources (CER) area JSON file
	if (jQuery("#EducatorResourcesRequestForm").exists() || jQuery("#EducatorResourcesRequestFormAlt").exists()) {
    	evaluateCERRZipCode("http://www.centerpointenergy.com/staticfiles/CNP/Common/SiteAssets/json/valid-zipcodes-sa-houston.txt");
	}

	jQuery("#toggleViewStudentsTeacherGuideBlock").click(function() {
		if(jQuery("#ViewStudentsTeacherGuideBlock").exists()) {
			jQuery("#ViewStudentsTeacherGuideBlock").toggle();
		}
	});
	
	
	
	// ----------------------------------------------------------------------------------------------------
	// CNP Natural Gas Generators Promotion
	// http://www.centerpointenergy.com/generators/
	// ----------------------------------------------------------------------------------------------------
	// Function expression that evaluates CenterPoint Educator Resources (CER) service area zip codes in a JSON formatted file
    var evaluateNGGPromoZipCode = function(jsonfile) {

    	jQuery.getJSON(jsonfile, function(response) {
    		var validNGGPromoZipCodes = response;

    		jQuery.validator.addMethod("postalcode", function(postalcode, element) {
    			return this.optional(element) || ((postalcode.match(/^([0-9]{5})$/) && jQuery.inArray(parseInt(postalcode), response) > -1));
    		}, " Houston area zip code required.");

    	});

		if (jQuery('#Zip_Code').exists()) {
			jQuery("#theForm").validate({
				rules: {
					Zip_Code: {
						required: true,
						postalcode: true,
						digits: true,
						minlength: 5,
						maxlength: 5
					}
				},
				messages: {
					Zip_Code: {
						postalcode: " Please enter a valid zip code."
					}
				}
			});
		}
	};

	// Call function to evaluate whether given zip code is listed in CenterPoint Educator Resources (CER) area JSON file
	if (jQuery('#theForm').exists()) {
		evaluateNGGPromoZipCode("http://www.centerpointenergy.com/staticfiles/CNP/Common/SiteAssets/json/valid-zipcodes-sa-houston.txt");
	}

	// Randomize the selected div elements (Natural Gas Generator Promo)
	var promoNatGasGeneratorDivs = jQuery("div.promo-natgasgen-dealers-group").get().sort(function(){
		return Math.round(Math.random()) - 0.5; //random so we get the right +/- combo
	});

	if (promoNatGasGeneratorDivs.length) {
		jQuery(promoNatGasGeneratorDivs).appendTo(promoNatGasGeneratorDivs[0].parentNode).show();
	} // end-if (promoNatGasGeneratorDivs.length)

	// Add the 'wrapper' and 'caption' DIV elements necessary for the hovering text effect
	var promoNatGasGeneratorImageSet = jQuery("ul#natgas-generator-slideshow li img");
	for (var i = 0, numOfNatGasGeneratorImagesInSet = promoNatGasGeneratorImageSet.length; i < numOfNatGasGeneratorImagesInSet; i++) {
		var aNatGasGeneratorImage = promoNatGasGeneratorImageSet[i];
		if (aNatGasGeneratorImage.title && aNatGasGeneratorImage.title.length) {
			// Add wrapper and caption DIVs
			var aNatGasGeneratorImageTitle = aNatGasGeneratorImage.title;
			jQuery(aNatGasGeneratorImage).wrap('<div class="natgas-generator-wrapper" />').after('<div class=\'natgas-generator-caption\'>' + '<p class=\'from-image-title-attr\'>' + aNatGasGeneratorImageTitle + '</p>' + '</div>').removeAttr('title');

			// Append caption DIV to sibling element P element
			var aNatGasGeneratorExtHtmlParagraph = jQuery(aNatGasGeneratorImage).parent().next();
			jQuery(aNatGasGeneratorExtHtmlParagraph).appendTo(jQuery(aNatGasGeneratorImage).siblings('.natgas-generator-caption'));
		} // end-if (aNatGasGeneratorImage.title && aNatGasGeneratorImage.title.length)
	} // end-for (var i = 0, numOfNatGasGeneratorImagesInSet = promoNatGasGeneratorImageSet.length; i < numOfNatGasGeneratorImagesInSet; i++)

	// Add the animated 'hover' effect
	jQuery('.natgas-generator-wrapper').hover(function(){
		jQuery(this).find('img').animate({opacity: ".6"}, 300);
		jQuery(this).find('.natgas-generator-caption').animate({top:"-225px"}, 300);
	},
	function(){
		jQuery(this).find('img').animate({opacity: "1.0"}, 300);
		jQuery(this).find('.natgas-generator-caption').animate({top:"225px"}, 100);
	});

	// select a random image from the set of images for which to begin the slideshow
	var numOfNatGasGeneratorImages = jQuery('#natgas-generator-slideshow li img').exists();	
	var randomNatGasGeneratorImageIndexValue = randomIntegerFromRange(1,numOfNatGasGeneratorImages);
	//var randomNatGasGeneratorImageIndexValue = Math.floor(Math.random() * numOfNatGasGeneratorImages);
	//console.log('Random Image... ' + randomNatGasGeneratorImageIndexValue);
	
	if (jQuery('#natgas-generator-slideshow').exists()) {
		var promoNatGasGeneratorSlideshow = jQuery('#natgas-generator-slideshow').slideshow( {
			transition: 'fadeThroughBackground',
			initialIndex: randomNatGasGeneratorImageIndexValue,
			autoStyle: false,
			autoPlay: false,
			delay: 3000,
			duration: 400,
			show: function(event, params){
				//console.log('show ' + params);
			},
			showComplete: function(event, params){
				//console.log('showComplete ' + params);
			}
		}).data('slideshow'); // get the instance out of the element data

		if (jQuery('#showNextSlideShowElement').exists()) {
			jQuery('#showNextSlideShowElement').click(function(){
				promoNatGasGeneratorSlideshow.show('next');
			});
		} // end-if (jQuery('#showNextSlideShowElement').exists())

		if (jQuery('#showPreviousSlideShowElement').exists()) {
			jQuery('#showPreviousSlideShowElement').click(function(){
				promoNatGasGeneratorSlideshow.show('previous');
			});
		} // end-if (jQuery('#showPreviousSlideShowElement').exists())
	} // end-if (jQuery('#natgas-generator-slideshow').exists())

	// accordion effect for Natural Gas Generators Promotional page
	if (jQuery("#natgas-generator-faqs").exists()) {
		jQuery("#natgas-generator-faqs").accordion({ active: true, heightStyle: "content", disabled: false, collapsible: true });
	} // end-if (jQuery("#natgas-generator-faqs").exists()) {

	// fix/workaround for Natural Gas Generator Promo page to prevent it from scrolling
	// back up to the top of the page upon each 'click' or Previous or Next
	if (jQuery("#showPreviousSlideShowElement").exists() || jQuery("#showNextSlideShowElement").exists()) {
		jQuery('#showPreviousSlideShowElement').click(function(){ scrollStopper(); return false; });
		jQuery('#showNextSlideShowElement').click(function(){ scrollStopper(); return false; });
	} // end-if (jQuery("#showPreviousSlideShowElement").exists() || jQuery("#showNextSlideShowElement").exists())



	// ----------------------------------------------------------------------------------------------------
	// CNP Claims Form
	// http://www.centerpointenergy.com/about/claimpolicy/
	// ----------------------------------------------------------------------------------------------------
	//just for the demos, avoids form submit
	//jQuery.validator.setDefaults({
	//	debug: true,
	//	success: "valid"
	//});

    //NOTE: The following two IDs are also used on the Pre-Exisiting Claims form (listed in the next section)
	var hiddenCaptchaFieldCheck = jQuery('#Was_Captcha_Entered').val();
	jQuery('#iframeAjaxUpload').hide();

	if (hiddenCaptchaFieldCheck == 'yes'){
		jQuery('#ClaimsForm :input').prop('disabled', false);
		jQuery('#iframeAjaxUpload').show();
		//console.log('checked');
	} else {
		jQuery('#ClaimsForm :input').prop('disabled', true);
		jQuery('#iframeAjaxUpload').hide();
		//console.log('unchecked');
	}

	jQuery.validator.addMethod("timeFormat", function (TimeIncident, element) {
		return this.optional(element) || /^(([0-1]?[0-9])|([2][0-3])):([0-5]?[0-9])(:([0-5]?[0-9]))?$/i.test(TimeIncident);
	}, "Please enter a valid time format.");

	jQuery.validator.addMethod("currencyFormat", function(Auto_Property_Damage_Bodily_Injury_CurrencyFieldOne, element) {
		return this.optional(element) || /^(\d{1,3})(\.\d{1,2})?$/.test(Auto_Property_Damage_Bodily_Injury_CurrencyFieldOne);
	}, "Must be in US currency format.");

	jQuery('#ClaimsForm').validate({
		ignore: [],
		rules: {
			Was_Captcha_Entered: {required: true, equals: "yes"},
			Type_Of_Claim: {required: true},
			Type_Of_Facility: {required: true},
			Claimant_Information: {required: true},
			Self_First_Name: {required: '#SelfClaimantRadioButton:checked'},
			Self_Last_Name: {required: '#SelfClaimantRadioButton:checked'},
			Self_Telephone_Number: {required: '#SelfClaimantRadioButton:checked', phoneUS: true},
			Self_Street_Service: {required: '#SelfClaimantRadioButton:checked'},
			Self_City_Service: {required: '#SelfClaimantRadioButton:checked'},
			Self_State_Service: {required: '#SelfClaimantRadioButton:checked'},
			Self_Email_Address: {required: '#SelfClaimantRadioButton:checked', email: true},
			Self_Verify_Email_Address: {equalTo: '#Self_Email_Address', required: '#SelfClaimantRadioButton:checked', email: true},
			Insured_Insurance_Name: {required: '#InsuranceClaimantRadioButton:checked'},
			Insured_First_Name: {required: '#InsuranceClaimantRadioButton:checked'},
			Insured_Last_Name: {required: '#InsuranceClaimantRadioButton:checked'},
			Insurance_Claim: {required: '#InsuranceClaimantRadioButton:checked'},
			Insurance_Adjuster_Name: {required: '#InsuranceClaimantRadioButton:checked'},
			Insurance_Adjuster_Phone: {required: '#InsuranceClaimantRadioButton:checked'},
			Responsible_Party_First_Name: {required: '#ResponsiblePartyClaimantRadioButton:checked'},
			Responsible_Party_Last_Name: {required: '#ResponsiblePartyClaimantRadioButton:checked'},
			Responsible_Party_Telephone_Number: {required: '#ResponsiblePartyClaimantRadioButton:checked', phoneUS: true},
			Responsible_Party_Street_Service: {required: '#ResponsiblePartyClaimantRadioButton:checked'},
			Responsible_Party_City_Service: {required: '#ResponsiblePartyClaimantRadioButton:checked'},
			Responsible_Party_State_Service: {required: '#ResponsiblePartyClaimantRadioButton:checked'},
			Responsible_Party_Zip_Service: {required: '#ResponsiblePartyClaimantRadioButton:checked'},
			Responsible_Party_Email_Address: {required: '#ResponsiblePartyClaimantRadioButton:checked', email: true},
			Responsible_Party_Verify_Email_Address: {equalTo: '#Responsible_Party_Email_Address', required: '#ResponsiblePartyClaimantRadioButton:checked', email: true},
			DateIncident: {required: true, date: true},
			TimeIncident: {required: true, time12h: true},
			Location_Incident_Address: {required: true},
			Incident_City: {required: true},
			Incident_State: {required: true},
			Incident_State_Zip: {required: true},
			Brief_Description: {required: true}
		},
		messages: {
			Was_Captcha_Entered: "Please enter the CAPTCHA value.",
			Type_Of_Claim: "Please select the Type of Claim.",
			Type_Of_Facility: "Please select the Type of Facility.",
			Claimant_Information: "Please complete the Claimant Information.",
			Self_First_Name: "Please enter your first name.",
			Self_Last_Name: "Please enter your last name.",
			Self_Telephone_Number: "Please enter the claimant's phone number.",
			Self_Street_Service: "Please enter the claimant's street address.",
			Self_City_Service: "Please enter the claimant's city.",
			Self_State_Service: "Please enter the claimant's state.",
			Self_Zip_Service: "Please enter the claimant's zip code.",
			Self_Email_Address: "Please enter the claimant's email.",
			Self_Verify_Email_Address: "Please verify the claimant's email.",
			Insured_Insurance_Name: "Please enter the Insurance company name.",
			Insured_First_Name: "Please enter the Insured's first name.",
			Insured_Last_Name: "Please enter the Insured's last name.",
			Insurance_Claim: "Please enter the Insured's claim number.",
			Insurance_Adjuster_Name: "Please enter the Insurance Adjuster's Name.",
			Insurance_Adjuster_Phone: "Please enter the Insurance Adjuster's Phone Number.",
			Responsible_Party_First_Name: "Please enter the Responsible Party's First Name.",
			Responsible_Party_Last_Name: "Please enter Responsible Party's last name.",
			Responsible_Party_Telephone_Number: "Please enter the Responsible Party's phone number.",
			Responsible_Party_Street_Service: "Please enter the Responsible Party's street address.",
			Responsible_Party_City_Service: "Please enter the Responsible Party's city.",
			Responsible_Party_State_Service: "Please enter the Responsible Party's state.",
			Responsible_Party_Zip_Service: "Please enter the Responsible Party's zip code.",
			Responsible_Party_Email_Address: "Please enter the Responsible Party's email.",
			Responsible_Party_Verify_Email_Address: "Please verify the Responsible Party's email.",
			DateIncident: "Please enter the date of incident.",
			TimeIncident: "Please enter the time of incident.",
			Location_Incident_Address: "Please enter the location of incident address.",
			Incident_City: "Please enter the incident city.",
			Incident_State: "Please enter the incident state.",
			Incident_State_Zip: "Please enter the incident zip code.",
			Brief_Description: "Please provide brief description of incident."
		},
		errorPlacement: function(error, element) {
			if (element.is("#Was_Captcha_Entered")) {
				error.insertAfter("#Was_Captcha_Entered");
			}
			if (element.is("#CausedByMySelfOrThirdPartyRadioButton") || element.is("#CausedByCenterPointEnergyRadioButton")) {
				error.insertAfter("#CausedByCenterPointEnergyRadioButtonText");
			}
			if (element.is("#ElectricFacilityRadioButton") || element.is("#GasFacilityRadioButton") || element.is("#UnknownFacilityRadioButton")) {
				error.insertAfter("#UnknownFacilityRadioButtonText");
			}

			// Self Claimant Information (Hidden/Required Optional)
			if (element.is("#SelfClaimantRadioButton") || element.is("#InsuranceClaimantRadioButton") || element.is("#ResponsiblePartyClaimantRadioButton")) {
				error.insertAfter("#ResponsiblePartyClaimantRadioButtonText");
			}
			if (element.is("#Self_First_Name")) {
				error.insertAfter("#Self_First_Name");
			}
			if (element.is("#Self_Last_Name")) {
				error.insertAfter("#Self_Last_Name");
			}
			if (element.is("#Self_Telephone_Number")) {
				error.insertAfter("#Self_Telephone_Number");
			}
			if (element.is("#Self_Street_Service")) {
				error.insertAfter("#Self_Street_Service");
			}
			if (element.is("#Self_City_Service")) {
				error.insertAfter("#Self_City_Service");
			}
			if (element.is("#Self_State_Service")) {
				error.insertAfter("#Self_State_Service");
			}
			if (element.is("#Self_Zip_Service")) {
				error.insertAfter("#Self_Zip_Service");
			}
			if (element.is("#Self_Email_Address")) {
				error.insertAfter("#Self_Email_Address");
			}
			if (element.is("#Self_Verify_Email_Address")) {
				error.insertAfter("#Self_Verify_Email_Address");
			}

			// Claimant Insurance Information (Hidden/Required Optional)
			if (element.is("#Insured_Insurance_Name")) {
				error.insertAfter("#Insured_Insurance_Name");
			}
			if (element.is("#Insured_First_Name")) {
				error.insertAfter("#Insured_First_Name");
			}
			if (element.is("#Insured_Last_Name")) {
				error.insertAfter("#Insured_Last_Name");
			}
			if (element.is("#Insurance_Claim")) {
				error.insertAfter("#Insurance_Claim");
			}
			if (element.is("#Insurance_Adjuster_Name")) {
				error.insertAfter("#Insurance_Adjuster_Name");
			}
			if (element.is("#Insurance_Adjuster_Phone")) {
				error.insertAfter("#Insurance_Adjuster_Phone");
			}

			// Responsible Party Insurance Information (Hidden/Required Optional)
			if (element.is("#Responsible_Party_First_Name")) {
				error.insertAfter("#Responsible_Party_First_Name");
			}
			if (element.is("#Responsible_Party_Last_Name")) {
				error.insertAfter("#Responsible_Party_Last_Name");
			}
			if (element.is("#Responsible_Party_Telephone_Number")) {
				error.insertAfter("#Responsible_Party_Telephone_Number");
			}
			if (element.is("#Responsible_Party_Street_Service")) {
				error.insertAfter("#Responsible_Party_Street_Service");
			}
			if (element.is("#Responsible_Party_City_Service")) {
				error.insertAfter("#Responsible_Party_City_Service");
			}
			if (element.is("#Responsible_Party_State_Service")) {
				error.insertAfter("#Responsible_Party_State_Service");
			}
			if (element.is("#Responsible_Party_Zip_Service")) {
				error.insertAfter("#Responsible_Party_Zip_Service");
			}
			if (element.is("#Responsible_Party_Email_Address")) {
				error.insertAfter("#Responsible_Party_Email_Address");
			}
			if (element.is("#Responsible_Party_Verify_Email_Address")) {
				error.insertAfter("#Responsible_Party_Verify_Email_Address");
			}

			// Incident Information Section
			if (element.is("#DateIncident")) {
				error.insertAfter("#DateIncident");
			}
			if (element.is("#TimeIncident")) {
				error.insertAfter("#TimeIncident");
			}
			if (element.is("#Incident_City")) {
				error.insertAfter("#Incident_City");
			}
			if (element.is("#Incident_State")) {
				error.insertAfter("#Incident_State");
			}
			if (element.is("#Incident_State_Zip")) {
				error.insertAfter("#Incident_State_Zip");
			}
			if (element.is("#Location_Incident_Address")) {
				error.insertAfter("#Location_Incident_Address");
			}
			if (element.is("#Brief_Description")) {
				error.insertAfter("#Brief_Description");
			}
		},
		// set this class to error-labels to indicate valid fields
		success: function(label) {
		// set &nbsp; as text for IE
		label.html("&nbsp;").addClass("checked");
		}
	});

	// When a "Type Of Claim" Radio Button option is selected, show only its related subsection
	jQuery("input[name='Type_Of_Claim']").click(function() {
		var tagIDForTypeOfClaim = jQuery('input[name=Type_Of_Claim]:checked', '#ClaimsForm').attr('id');
		var valueForTypeOfClaim = jQuery('input[name=Type_Of_Claim]:checked', '#ClaimsForm').val();
		//console.log(tagIDForTypeOfClaim + "==>" + valueForTypeOfClaim);

		if(jQuery('#CausedByMySelfOrThirdPartyRadioButton').prop('checked')) {
			jQuery('#PropertyDamageBodilyInjuryRadioButton').hide().next().hide();
			jQuery('#AutoPropertyDamageBodilyInjuryRadioButton').hide().next().hide();
		}
		if(jQuery('#CausedByCenterPointEnergyRadioButton').prop('checked')) {
			jQuery('#PropertyDamageBodilyInjuryRadioButton').show().next().show();
			jQuery('#AutoPropertyDamageBodilyInjuryRadioButton').show().next().show();
		}
	});

	// When a "Claimant" Radio Button option is selected, show only its related subsection
	jQuery("input[name='Claimant_Information']").click(function() {
		var tagIDForClaimantInfo = jQuery('input[name=Claimant_Information]:checked', '#ClaimsForm').attr('id');
		var valueForClaimantInfo = jQuery('input[name=Claimant_Information]:checked', '#ClaimsForm').val();
		//console.log(tagIDForClaimantInfo + "==>" + valueForClaimantInfo);

		if(jQuery('#SelfClaimantRadioButton').prop('checked')) {
			jQuery('#SelfClaimantContentBlock').show().siblings().hide();
		}
		if(jQuery('#InsuranceClaimantRadioButton').prop('checked')) {
			jQuery('#InsuredClaimantContentBlock').show().siblings().hide();
		}
		if(jQuery('#ResponsiblePartyClaimantRadioButton').prop('checked')) {
			jQuery('#ResponsiblePartyClaimantContentBlock').show().siblings().hide();
		}
	});

	// When a "Damages" Radio Button option is selected, show only its related subsection
	jQuery("input[name='Damages']").click(function() {
		var tagIDForClaimaint = jQuery('input[name=Damages]:checked', '#ClaimsForm').attr('id');
		var valueForClaimaint = jQuery('input[name=Damages]:checked', '#ClaimsForm').val();
		//console.log(tagIDForClaimaint + "==>" + valueForClaimaint);

		if(jQuery('#PropertyDamageRadioButton').prop('checked')) {
			jQuery('#PropertyDamageOneContentBlock').show().siblings().hide();
			jQuery("#InsuranceInformationContentBlock").hide();
		}

		if(jQuery('#AutoPropertyDamageRadioButton').prop('checked')) {
			jQuery('#AutoPropertyDamageContentBlock').show().siblings().hide();
			jQuery("input[name='Auto_Property_Damage_Was_Insurance_Filed']").click(function() {
				if(jQuery('#AutoPropertyDamageWasFiledRadioButton').prop('checked')) {
					jQuery('#InsuranceInformationContentBlock').show();
				} else {
					jQuery('#InsuranceInformationContentBlock').hide();
				}
			});
		}

		if(jQuery('#PropertyDamageBodilyInjuryRadioButton').prop('checked')) {
			jQuery('#PropertyDamageBodilyInjuryOneContentBlock').show().siblings().hide();
			jQuery("#InsuranceInformationContentBlock").hide();
		}

		if(jQuery('#AutoPropertyDamageBodilyInjuryRadioButton').prop('checked')) {
			jQuery('#AutoPropertyDamageBodilyInjuryContentBlock').show().siblings().hide();
			jQuery("input[name='Auto_Property_Damage_Bodily_Injury_Was_Insurance_Filed']").click(function() {
				if(jQuery('#AutoPropertyDamageBodilyInjuryWasFiledRadioButton').prop('checked')) {
					jQuery('#InsuranceInformationContentBlock').show();
				} else {
					jQuery('#InsuranceInformationContentBlock').hide();
				}
			});
		}
	});

	// Hide (Remove) Insurance Information Block
	jQuery("#hideInsuranceInformationContentBlock").click(function(event) {
		if(jQuery("#InsuranceInformationContentBlock").exists()) {
			jQuery("#InsuranceInformationContentBlock").hide();
			//console.log(event.preventDefault());
		}
	});

	// Toggle Additional Property Damage Blocks
	jQuery("#togglePropertyDamageTwoContentBlock").click(function(event) {
		if(jQuery("#PropertyDamageTwoContentBlock").exists()) {
			jQuery("#PropertyDamageTwoContentBlock").toggle();
			//console.log(event.preventDefault());
		}
	});

	jQuery("#togglePropertyDamageThreeContentBlock").click(function() {
		if(jQuery("#PropertyDamageThreeContentBlock").exists()) {
			jQuery("#PropertyDamageThreeContentBlock").toggle();
		}
	});

	jQuery("#togglePropertyDamageBodilyInjuryTwoContentBlock").click(function() {
		if(jQuery("#PropertyDamageBodilyInjuryTwoContentBlock").exists()) {
			jQuery("#PropertyDamageBodilyInjuryTwoContentBlock").toggle();
		}
	});

	jQuery("#togglePropertyDamageBodilyInjuryThreeContentBlock").click(function() {
		if(jQuery("#PropertyDamageBodilyInjuryThreeContentBlock").exists()) {
			jQuery("#PropertyDamageBodilyInjuryThreeContentBlock").toggle();
		}
	});



	// ----------------------------------------------------------------------------------------------------
	// CNP Claims Form (Pre-Existing Claims)
	// http://www.centerpointenergy.com/about/claimpolicy/
	// ----------------------------------------------------------------------------------------------------
	if (hiddenCaptchaFieldCheck == 'yes'){
		jQuery('#PreExistingClaimsForm :input').prop('disabled', false);
		jQuery('#iframeAjaxUpload').show();
		//console.log('checked');
	} else {
		jQuery('#PreExistingClaimsForm :input').prop('disabled', true);
		jQuery('#iframeAjaxUpload').hide();
		//console.log('unchecked');
	}

	jQuery.validator.addMethod("equals", function(value, element, param) {
	  return this.optional(element) || value === param;
	}, jQuery.validator.format("You must enter {0}"));

	jQuery('#PreExistingClaimsForm').validate({
		ignore: [],
		rules: {
			Was_Captcha_Entered: {required: true, equals: "yes"},
			First_Name: {required: true},
			Last_Name: {required: true},
			Telephone_Number: {required: true, phoneUS: true},
			Email_Address: {required: true, email: true},
			Verify_Email_Address: {required: true, equalTo: '#Email_Address', email: true}
		},
		messages: {
			Was_Captcha_Entered: "Please enter the CAPTCHA value.",
			First_Name: "Please enter the first name.",
			Last_Name: "Please enter the last name.",
			Telephone_Number: "Please enter the phone number",
			Email_Address: "Please enter the email address.",
			Verify_Email_Address: "Please verify the email address."


		},
		errorPlacement: function(error, element) {
			if (element.is("#Was_Captcha_Entered")) {
				error.insertAfter("#Was_Captcha_Entered");
			}
			if (element.is("#First_Name")) {
				error.insertAfter("#First_Name");
			}
			if (element.is("#Last_Name")) {
				error.insertAfter("#Last_Name");
			}
			if (element.is("#Telephone_Number")) {
				error.insertAfter("#Telephone_Number");
			}
			if (element.is("#Email_Address")) {
				error.insertAfter("#Email_Address");
			}
			if (element.is("#Verify_Email_Address")) {
				error.insertAfter("#Verify_Email_Address");
			}
		}
	});

	// DatePicker jQuery UI Effect
	if (jQuery("#DateIncident").exists()) {
		jQuery("#DateIncident").datepicker();
	} // end-if (jQuery("#DateIncident").exists())

	// TimePicker jQuery UI Effect (add-on from...http://trentrichardson.com/examples/timepicker/)
	if (jQuery("#TimeIncident").exists()) {
		jQuery("#TimeIncident").timepicker({
			timeFormat: "hh:mm TT"
		});
	} // end-if (jQuery("#TimeIncident").exists())



	// ----------------------------------------------------------------------------------------------------
	// CNP National Accounts (MN)
	// http://www.centerpointenergy.com/services/naturalgas/business/nationalaccountinformation/MN/
	// ----------------------------------------------------------------------------------------------------
	jQuery("#ListOfRebatesForMN").click(function() {
		jQuery(this).siblings().slideToggle();
		jQuery(this).children(".show-hide-icon").toggleClass("ui-icon-circle-triangle-s").toggleClass("ui-icon-circle-triangle-e");
	});

	jQuery("#CalculateRebateSavings").click(function() {
		jQuery(this).siblings().slideToggle();
		jQuery(this).children(".show-hide-icon").toggleClass("ui-icon-circle-triangle-s").toggleClass("ui-icon-circle-triangle-e");
	});

	
	
	// ----------------------------------------------------------------------------------------------------
	// CNP Feedback Forum
	// http://www.centerpointenergy.com/promo/FeedbackForum
	// ----------------------------------------------------------------------------------------------------
	if (jQuery("#FeedbackForumFAQs").exists()) {
		jQuery("#FeedbackForumFAQs").accordion({ active: true, heightStyle: "content", disabled: false, collapsible: true });
	} // end-if (jQuery("#FeedbackForumFAQs").exists())
	
	
	
	// ----------------------------------------------------------------------------------------------------
	// CNP Construction Zone Directory (MN) - Right Rail
	// http://cms.mycenterpointenergy.com/community/inthecommunity/constructionzone/MN/
	// ----------------------------------------------------------------------------------------------------
	if (jQuery("#ConstructionZoneDirectory").exists()) {
		jQuery("#ConstructionZoneDirectory").accordion({ active: true, heightStyle: "content", autoHeight: false, clearStyle: true, disabled: false, collapsible: true });
	} // end-if (jQuery("#FeedbackForumFAQs").exists())

	
	
	// ----------------------------------------------------------------------------------------------------
	// CNP Construction Zone FAQ (MN) - Main Article Content
	// http://cms.mycenterpointenergy.com/community/inthecommunity/constructionzone/MN/
	// ----------------------------------------------------------------------------------------------------
	multipleToggleEffect('div.construction-zone-faq-container');
    multipleToggleEffect('div.construction-zone-faq-set');

    
    
	// ----------------------------------------------------------------------------------------------------------------------------
	// Speakers' Bureau Request Form...
	// http://www.centerpointenergy.com/community/safetyandeducation/requestspeakerform/HO/
	// ----------------------------------------------------------------------------------------------------------------------------
    if (jQuery('#SpeakersBureauForm').exists()) {
    	jQuery('#SpeakersBureauForm').validate({
    		ignore: [],
    		rules: {
    			FirstName: { required: true },
    			LastName: { required: true },
    			CompanyName: { required: true },
				TelephoneNumber: { required: true, phoneUS: true },
    			EmailAddress: { required: true, email: true },
    			StreetService: { required: true },
    			CityService: { required: true },
    			StateService: { required: true },
    			ZipService: { required: true },
    			DateIncident: { required: true },
    			TimeIncident: { required: true },
    			Location: { required: true },
    			Length: { required: true },
    			Audience: { required: true },
    			PresentationType: { required: true, valueNotEquals: "-1" }
    		},
    		messages: {
    			FirstName: "Please enter the first name.",
    			LastName: "Please enter the last name.",
    			CompanyName: "Please enter the company name.",
    			TelephoneNumber: "Please enter the phone number.",
    			EmailAddress: "Please enter the email address.",
    			StreetService: "Please enter the address.",
    			CityService: "Please enter the city.",
    			StateService: "Please enter the state.",
    			ZipService: "Please enter the zip.",
    			DateIncident: "Please enter the date.",
    			TimeIncident: "Please enter the time.",
    			Location: "Please enter the location.",
    			Length: "Please enter the duration (length).",
    			Audience: "Please enter the target audience.",
    			PresentationType: "Please select the type of presentation."
			}
	    });
    }
    
}); //end-jQuery(document).ready(function() {



