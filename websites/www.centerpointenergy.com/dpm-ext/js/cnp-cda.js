// ----------------------------------------------------------------------
// CenterPoint Energy
// CNP Public Site - JavaScript functions for site-specific actions.
// ----------------------------------------------------------------------

var activeServiceAreaMenus = $H(); // Stores all active Service Area Menus to control their time to live

/*
 Invokes the 'UpdateServiceAreaServlet' to handle a visitor's 
 request to change their active Service Area. The servlet
 action will also redirect the visitor to the appropriate page.
 If a 'destinationURL' is not provided, the action will redirect
 to the active page.
*/	
function updateServiceArea(serviceAreaCode, destinationURL) {
	
	var displayURL = (destinationURL != '' && destinationURL != undefined)
		? destinationURL 
		: top.location.href;
	var queryString = (destinationURL != '' && destinationURL != undefined) 
		? destinationURL.substring(destinationURL.indexOf('?'), destinationURL.length) 
		: top.location.search;
		
	// If query string exists, ensure to maintain all parameters and remove existing 'sa' parameter...
    if(queryString != '' && queryString != undefined) {
    	if(queryString.search('sa=') != -1) {
    		var normalizedURL = displayURL.substring(0, displayURL.indexOf(queryString));
    		var startIndex = queryString.indexOf('sa=');
    		var firstPart = queryString.substring(0, startIndex-1);
    		var lastPart = queryString.substring(startIndex+5, queryString.length); // this assumes Service Area code is two chars
    		displayURL = normalizedURL + firstPart + lastPart;
    	}	
    }
    
    // Redirect browser...
	window.location = "http://www.centerpointenergy.com/portal/updateSA/?sa="+serviceAreaCode+"&redirect="+escape(displayURL);
}

/*
 Invokes the requested Service Area Menu, rendering the menu just below 
 the specified element.
*/
function showServiceAreaMenu(elementObject, menuID) {
	var xy = Position.cumulativeOffset(elementObject);
	var selectorHeight = $(elementObject).getHeight();
	var serviceAreaMenu = jQuery("#service-area-menu-"+menuID);
	var leftPos = xy[0];
	var topPos = xy[1] + selectorHeight;
	serviceAreaMenu.offset({top:topPos, left:leftPos});
	serviceAreaMenu.css("visibility", "visible");
	stallServiceAreaMenuTimeout(menuID);
}

/*
 Hides the specified Service Area Menu.
*/
function hideServiceAreaMenu(menuID) {
	if(!activeServiceAreaMenus[menuID]) {
		var serviceAreaMenu = $("service-area-menu-"+menuID);
		hideElement(serviceAreaMenu);
	}
}

/*
 Starts the Service Area Menu display timeout so it will close when 
 it is inactive.
*/
function startServiceAreaMenuTimeout(menuID) {
	activeServiceAreaMenus.remove(menuID);
	setTimeout("hideServiceAreaMenu('"+menuID+"');", 1500);
}

/*
 Stalls the specified Service Area Menu display timeout so it will stay open.
*/
function stallServiceAreaMenuTimeout(menuID) {
	var keyValueSet = new Object();
    keyValueSet[menuID] = 'true';
	activeServiceAreaMenus.merge(keyValueSet);
}

/*
 Controls the swapping of panels for the 'Tabbed Services Menu' as a visitor
 mouses over each tab.
*/
function swapTabbedServicesPanel(panelID) {
	var panels = new Array('naturalgas', 'electric', 'energysales', 'pipelines', 'fieldservices', 'andmore');
	
	for(i=0;i<panels.length;i++) {
		var tab = $('tab-'+panels[i]);
		if(panels[i] == panelID) {
			var imageOn = '/staticfiles/CNP/Common/SiteAssets/img/tab-' + panels[i] + '-active.gif';
			tab.setAttribute('src', imageOn);
			showElementByIDWithCSS('panel-'+panels[i]);
		} else {
			var imageOff = '/staticfiles/CNP/Common/SiteAssets/img/tab-' + panels[i] + '-inactive.gif';
			tab.setAttribute('src', imageOff);
			hideElementByIDWithCSS('panel-'+panels[i]);
		}
	} // end-for
}

/*
 Controls the swapping of panels for the 'Tabbed Services Menu' as a visitor
 mouses over each tab.
*/
function swapTabbedServicesPanelOLD(panelID) {
	var panels = new Array('naturalgas', 'electric', 'pipelines', 'fieldservices', 'more');
	
	for(i=0;i<panels.length;i++) {
		var tab = $('tab-'+panels[i]);
		if(panels[i] == panelID) {
			var imageOn = '/staticfiles/CNP/Common/SiteAssets/img/tab-' + panels[i] + '-on.gif';
			tab.setAttribute('src', imageOn);
			showElementByIDWithCSS('panel-'+panels[i]);
		} else {
			var imageOff = '/staticfiles/CNP/Common/SiteAssets/img/tab-' + panels[i] + '-off.gif';
			tab.setAttribute('src', imageOff);
			hideElementByIDWithCSS('panel-'+panels[i]);
		}
	} // end-for
}

/*
 Post-pends the 'href' value of a provided anchor tag
 with the active name/value pair of the specified query 
 string parameter, if exists. The function also invokes
 a click action once the 'href' is transformed.
*/
function addActiveQueryParameterToHref(parameterName, theElement) {
	
	if(parameterName != null && parameterName.length > 0 && theElement != null && theElement != undefined) {

		var queryString = top.location.search;
		var parameterValue = null;

		// Parse out requested parameter value...
		if(queryString != '' && queryString != undefined) {
    			if(queryString.search(parameterName+'=') != -1) {
				var lengthOfParamName = parameterName.length;
    				var startIndex = queryString.indexOf(parameterName+'=');
				var lastPart = queryString.substring(startIndex+lengthOfParamName+1);
				var startOfNewParam = lastPart.indexOf('&'); // check for other params on the remaining query string...
				if(startOfNewParam != -1) {
					parameterValue = lastPart.substring(0, startOfNewParam);
				} else {
					parameterValue = lastPart;
				}
				//alert('value: '+parameterValue);
    			}
    		} // end-if(queryString != '' && queryString != undefined)

		if(parameterValue != null && parameterValue.length > 0) {

			// Append parameter to 'href' of calling element...
			var currentURL = theElement.href;
			if(currentURL != null && currentURL.length > 0) {
				//alert('curr href: '+currentURL);
				var updatedURL = currentURL;
				if(currentURL.indexOf('?') != -1) {
					updatedURL = currentURL + '&';
				} else {
					updatedURL = currentURL + '?';
				}
				updatedURL += parameterName + '=' + parameterValue;
				//alert('new href: '+updatedURL);
				
				theElement.href = updatedURL;
				theElement.click();
			}
		} // end-if(parameterValue != null && parameterValue.length > 0)
	} // end-if(parameterName != null && parameterName.length > 0 && theElement != null && theElement != undefined)
}

/*
 Sets the specified element ID to "visible".
*/
function showElementByID(elementID) {
	var elementObject = $(elementID);
	showElement(elementObject);
}


/*
 Sets the specified element ID to "hidden". 
*/
function hideElementByID(elementID) {
	var elementObject = $(elementID);
	hideElement(elementObject);
}

/*
 Sets the specified element to use the "visible" style
 by passing the ID of the element.
*/
function showElementByIDWithCSS(elementID) {
	var elementObject = $(elementID);
	showElementWithCSS(elementObject);
}

/*
 Sets the specified element to use the "hidden" style
 by passing the ID of the element object. 
*/
function hideElementByIDWithCSS(elementID) {
	var elementObject = $(elementID);
	hideElementWithCSS(elementObject);
}

/*
 Sets the specified element to "visible".
*/
function showElement(elementObject) {
	if(elementObject != undefined) elementObject.style.visibility = 'visible';
}

/*
 Sets the specified element to "hidden". 
*/
function hideElement(elementObject) {
	if(elementObject != undefined) elementObject.style.visibility = 'hidden';
}

/*
 Sets the specified element to use the "visible" style
 by passing the actual element object.
*/
function showElementWithCSS(elementObject) {
	if(elementObject != undefined) elementObject.className = 'visible';
}

/*
 Sets the specified element to use the "hidden" style
 by passing the actual element object. 
*/
function hideElementWithCSS(elementObject) {
	if(elementObject != undefined) elementObject.className = 'hidden';
}

/*
 Invokes a DPM component cache refresh for the specified VCMID
 of an active content region.
*/
function clearDPMCache(componentID) {
	var activeURL = top.location.href;
	window.location = "http://www.centerpointenergy.com/vgn-ext-templating/CNP/dpm-cache-refresh.jsp?id="+componentID+"&redirect="+escape(activeURL);
}

/*
 Toggles the DPM component cache controls (either "ON" of "OFF").
*/
function toggleDPMCacheControls(status) {
	var activeURL = top.location.href;
	window.location = "http://www.centerpointenergy.com/vgn-ext-templating/CNP/dpm-cache-toggle-controls.jsp?toggle="+status+"&redirect="+escape(activeURL);
}

/*
 Toggles the DPM component in-context-edit (ICE) controls (either "ON" of "OFF").
*/
function toggleDPMEditControls(status) {
	var activeURL = top.location.href;
	window.location = "http://www.centerpointenergy.com/vgn-ext-templating/CNP/dpm-edit-toggle-controls.jsp?toggle="+status+"&redirect="+escape(activeURL);
}
