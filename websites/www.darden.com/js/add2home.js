
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<META HTTP-EQUIV="refresh" content="5;URL=/">
<title>Darden Restaurants - Page Not Found</title>
<link href="../css/styles.css"/*tpa=http://www.darden.com/css/styles.css*/ rel="stylesheet" type="text/css" />
<link href="../css/nav.css"/*tpa=http://www.darden.com/css/nav.css*/ rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" type="text/css" href="../css/rss.css"/*tpa=http://www.darden.com/css/rss.css*//>

<script src="forms.js"/*tpa=http://www.darden.com/js/forms.js*/ type="text/javascript"></script>
<script type="text/javascript" src="jquery-1.2.6.min.js"/*tpa=http://www.darden.com/js/jquery-1.2.6.min.js*/></script>
<script type="text/javascript" src="jquery.mousewheel.js"/*tpa=http://www.darden.com/js/jquery.mousewheel.js*/></script>
<script type="text/javascript" src="jScrollPane.js"/*tpa=http://www.darden.com/js/jScrollPane.js*/></script>

<script type="text/javascript" src="swfobject.js"/*tpa=http://www.darden.com/js/swfobject.js*/></script>


<script type="text/javascript">
			var flashvars = {};
			var params = {};
			params.wmode = "transparent";
			var attributes = {};
			swfobject.embedSWF("../swf/home/dri_home_v1_new.swf"/*tpa=http://www.darden.com/swf/home/dri_home_v1_new.swf*/, "myAlternativeContent", "870", "280", "9.0.0", "../swf/expressInstall.swf"/*tpa=http://www.darden.com/swf/expressInstall.swf*/, flashvars, params, attributes);
</script>

<script type="text/javascript">
			
			$(function()
			{
				// this initialises the demo scollpanes on the page.
				$('#pane1').jScrollPane();
                
			});
			
		</script>

<script src="s_code.js"/*tpa=http://www.darden.com/js/s_code.js*/ type="text/javascript"></script>
</head>
<body>


<table class="main_table" width="870" border="0" cellpadding="0" cellspacing="0" >
  <tr>
    <td>
    
    <table width="850" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td colspan="2">
		
		</td>

<script src="dynamic-font-resize.js"/*tpa=http://www.darden.com/js/dynamic-font-resize.js*/ type="text/javascript"></script>
<!--[if lte IE 6]>
<script type="text/javascript" src="supersleight.js"/*tpa=http://www.darden.com/js/supersleight.js*/></script>
<![endif]-->

<!--BEGIN TOP SECTION-->
<div id="page_top_holder" style="margin-left: 10px;">

	<div id="page_top_simplenav">
    	<a href="http://www.darden.com/marketplace/gift_cards.asp" class="headerlink" >Gift Cards</a> 
        <a href="http://www.darden.com/locations" class="headerlink" >Locations</a> 
        <a href="http://www.darden.com/about/contact_us.asp" class="headerlink" >Contact</a>
    </div>
    
    <div id="logo"><a href="../index.htm"/*tpa=http://www.darden.com/*/ style="outline:none;"><img src="../images/core/logo_darden.png"/*tpa=http://www.darden.com/images/core/logo_darden.png*/ alt="Darden Restaurants" border="0" /></a></div>
    
    <div class="search">
    <!--
    			<form action="http://www.darden.com/search.asp" method="get" name="gs" id="gs">
                <img src="../images/locator/search_icon.gif"/*tpa=http://www.darden.com/images/locator/search_icon.gif*/ width="18" height="18" class="search_icon" />
              	<input name="imageField" type="image" onclick="searchClick(); return false;" class="search_button" src="../images/locator/search_arrow.gif"/*tpa=http://www.darden.com/images/locator/search_arrow.gif*//>
                <input type="hidden" name="start" value="" />
                <input type="hidden" name="site" value="base" />
                <input type="hidden" name="output" value="xml_no_dtd" />
                <input type="hidden" name="client" value="base" />
                <input type="hidden" name="restrict" value="dri" />
                <input type="hidden" name="access" 	value="" />
                <input type="hidden" name="lr" value="" />
                <input type="hidden" name="proxystylesheet" value="base" />
                <input type="hidden" name="ie" value="" />
                <input type="hidden" name="oe" value="" />
                <input type="text" id="q" name="q" onfocus="javascript:this.value=''" class="search_field" value="http://www.darden.com/js/Search Darden.com" autocomplete="off"/>
            	</form>
                -->
                <div style="width:140px; float:right; top:50px; margin-left:95px; position:absolute;">
                <a href="../index.htm"/*tpa=http://www.darden.com/*/ class="headerlink">English</a> | &nbsp; <a href="http://www.darden.com/espanol/" class="headerlink">Espa&ntilde;ol</a>
                </div>
	</div>
    
        <!--<div style="float:right; width:120px; margin-top:15px; margin-right:0px;">
                  <a href="../index.htm"/*tpa=http://www.darden.com/*/ class="headerlink">English  |</a>  <a href="http://www.darden.com/espanol/" class="headerlink">Espa&ntilde;ol</a>
        </div>-->
	</div>
    
<!--END TOP SECTION-->
        
        </td>
      </tr>
      <tr>
        <td colspan="2">
		
		<!--BEGIN NAV SHARED-->
            
<div id="nav_outer">
<ul id="lists">
    <li ><a href="http://www.darden.com/about/" class="tl_one">OUR COMPANY<!--[if IE 7]><!--></a><!--<![endif]-->
    <!--[if lte IE 6]><table><tr><td><![endif]-->
    <div class="pos1">
    <dl>
    <dt><a href="http://www.darden.com/about/executive_members.asp">Executive Leadership</a></dt>
    <dt><a href="http://www.darden.com/about/board_members.asp">Board of Directors</a></dt>
    <dt><a href="http://www.darden.com/about/photo_history.asp">History</a></dt>
    <dt><a href="http://www.darden.com/about/doing_business.asp">Doing Business with Darden</a></dt>
    <dt><a href="http://www.darden.com/about/contact_us.asp">Contact Us</a></dt>
    <dt><a href="http://www.darden.com/about/faq.asp">FAQs</a></dt>

    </dl>
    </div>
    <!--[if lte IE 6]></td></tr></table></a><![endif]-->
    </li>
    
    
    <li ><a href="http://www.darden.com/restaurants/" class="tl_two">OUR BRANDS<!--[if IE 7]><!--></a><!--<![endif]-->
    <!--[if lte IE 6]><table><tr><td><![endif]-->
    <div class="pos2">

    <dl>
    <dt><a href="http://www.darden.com/restaurants/olivegarden/">Olive Garden</a></dt>
    <dt><a href="http://www.darden.com/restaurants/longhorn/">LongHorn Steakhouse</a></dt>
    <dt><a href="http://www.darden.com/restaurants/bahamabreeze/">Bahama Breeze</a></dt>
    <dt><a href="http://www.darden.com/restaurants/seasons52/">Seasons 52</a></dt>
    <dt><a href="http://www.darden.com/restaurants/capitalgrille/">The Capital Grille</a></dt>
    <dt><a href="http://www.darden.com/restaurants/eddievs/">Eddie V's</a></dt>
    <dt><a href="http://www.darden.com/restaurants/yardhouse/">Yard House</a></dt>
    </dl>
    </div>
    <!--[if lte IE 6]></td></tr></table></a><![endif]-->
    </li>
    
    
    <li ><a href="http://www.darden.com/commitment/" class="tl_three">OUR COMMITMENTS<!--[if IE 7]><!--></a><!--<![endif]-->
    <!--[if lte IE 6]><table><tr><td><![endif]-->
    <div class="pos3">

    <dl>
    
    <dt><a href="http://www.darden.com/commitment/diversity.asp">Diversity &amp; Inclusion</a></dt>
    <dt style="width: 200px;"><a href="http://www.darden.com/commitment/community.asp">Foundation &amp; Community Affairs</a></dt>
    <dt><a href="http://www.darden.com/commitment/sustainability.asp">Sustainability</a></dt>
    </dl>

    </div>
    <!--[if lte IE 6]></td></tr></table></a><![endif]-->
    </li>
    
    
    <li><a href="http://investor.darden.com/" class="tl_four">INVESTORS<!--[if IE 7]><!--></a><!--<![endif]-->

    <!--[if lte IE 6]><table><tr><td><![endif]-->

   <div class="pos4">
    <dl>
    <!--dt><a href="http://investor.darden.com/releases.cfm" >Press Releases</a></dt>
    <dt><a href="http://investor.darden.com/events.cfm" >Events &amp; Presentation</a></dt>
    <dt><a href="http://investor.darden.com/financials.cfm" >Financial Information</a></dt>
    <dt><a href="http://investor.darden.com/stockquote.cfm" >Stock Information</a></dt>
    <dt><a href="http://investor.darden.com/profile.cfm" >Email Alerts</a></dt>
    <dt><a href="http://investor.darden.com/faq.cfm" target="_self">Investor FAQs</a></dt>
	<dt><a href="http://www.darden.com/investors/safe_harbor.asp" target="_self">Safe Harbor Notice</a></dt>
    <dt><a href="http://www.darden.com/corporate/" target="_self">Corporate Governance</a></dt>
	<dt><a href="http://investor.darden.com/contactus.cfm" target="_self">Investor Feedback/Comments</a></dt-->

    <dt><a href="http://investor.darden.com/investors/news-releases/default.aspx" >Press Releases</a></dt>
    <dt><a href="http://investor.darden.com/investors/events-and-presentations/default.aspx" >Events &amp; Presentation</a></dt>
    <dt><a href="http://investor.darden.com/investors/financial-information/default.aspx" >Financial Information</a></dt>
    <dt><a href="http://investor.darden.com/investors/stock-information/default.aspx" >Stock Information</a></dt>
    <dt><a href="http://investor.darden.com/investors/email-alerts/default.aspx" >Email Alerts</a></dt>
    <dt><a href="http://investor.darden.com/investors/investor-faqs/default.aspx" target="_self">Investor FAQs</a></dt>
	<dt><a href="http://investor.darden.com/investors/safe-harbor-notice/default.aspx" target="_self">Safe Harbor Notice</a></dt>
    <dt><a href="http://investor.darden.com/investors/corporate-governance/default.aspx" target="_self">Corporate Governance</a></dt>
	<dt><a href="http://investor.darden.com/investors/investor-feedbackcomments/default.aspx" target="_self">Investor Feedback/Comments</a></dt>
    </dl>
    </div>
    <!--[if lte IE 6]></td></tr></table></a><![endif]-->

    </li>
    
    
    <li ><a href="http://www.darden.com/media/default.asp" class="tl_five">MEDIA<!--[if IE 7]><!--></a><!--<![endif]-->
    <!--[if lte IE 6]><table><tr><td><![endif]-->
    <div class="pos5">
    <dl>
    <dt><a href="http://investor.darden.com/investors/news-releases/default.aspx">Press Releases</a></dt>
    <!--dt><a href="http://www.darden.com/about/news_room.asp">Darden in the News</a></dt-->
    <dt><a href="http://www.darden.com/media/media_contacts.asp">Media Contacts</a></dt>

    <dd><a href="http://www.darden.com/media/media_contacts.asp#darden">Darden</a></dd>
    <dd><a href="http://www.darden.com/media/media_contacts.asp#olivegarden">Olive Garden</a></dd>
    <dd><a href="http://www.darden.com/media/media_contacts.asp#longhorn">Longhorn Steakhouse</a></dd>
    <dd><a href="http://www.darden.com/media/media_contacts.asp#srg">Specialty Restaurants</a></dd>
    
    <dt><a href="http://www.darden.com/about/executive_members.asp">Leadership</a></dt>

    <dd><a href="http://www.darden.com/about/executive_members.asp">Bios</a></dd>
    <dd><a href="http://www.darden.com/about/executive_members.asp">Pictures</a></dd>

    </dl>
    
    <dl>
    <dt><a href="http://www.darden.com/media/media_brand_logos.asp">Downloads</a></dt>
    <!--dd><a href="http://www.darden.com/pdf/media/Download_Darden_Fact_Sheet.pdf" target="_blank">Fact Sheet</a></dd-->
    <!--dd><a href="http://www.darden.com/pdf/media/Download_Darden_Timeline.pdf" target="_blank">Timeline</a></dd-->
    <dd><a href="http://www.darden.com/media/media_brand_logos.asp">Brand Logos</a></dd>
    <dd><a href="http://www.darden.com/media/media_brand_photos.asp">Brand Photos</a></dd>

    </dl>
    
    </div>
    <!--[if lte IE 6]></td></tr></table></a><![endif]-->
    </li>

    
    
    <li><a href="http://www.darden.com/careers/" class="tl_six">CAREERS<!--[if IE 7]><!--></a><!--<![endif]-->
    <!--[if lte IE 6]><table><tr><td><![endif]-->
    <div class="pos6">

    <dl >
    <dt><a href="http://www.darden.com/careers/">Careers Overview </a></dt>
    <dt><a href="http://www.darden.com/careers/culture.asp">Our Culture</a></dt>
    <dt><a href="http://www.darden.com/careers/stories.asp">Our Stories</a></dt>
    <dt style="width:250px;"><a href="http://www.darden.com/careers/support_center.asp">Restaurant Support Center Careers</a></dt>
    <dt><a href="http://www.darden.com/careers/restaurants.asp">Careers at Our Restaurants</a></dt>
    <dt><a href="http://www.darden.com/careers/university_relations.asp">Darden University Relations</a></dt>
    </dl>
    </div>
   
    <!--[if lte IE 6]></td></tr></table></a><![endif]-->
    </li>
</ul>
</div>

            <!--END NAV SHARED--> 
        
        </td>
      </tr>
      <tr>
        <td colspan="2" >
        <!--BEGIN HEADER IMAGE-->
        <div class="header_shadow_sm">
        	<img src="../images/headers/header_darden_wall.jpg"/*tpa=http://www.darden.com/images/headers/header_darden_wall.jpg*/ />
        </div>
    <!--END HEADER IMAGE-->
    </td>
      </tr>
    </table>
    
    </td>
  </tr>
</table>

		
        
 
<div id="home_bottom_pods">

	<div align="center" class="home_bottom_ts"><img src="../images/misc/home_bottom_TS.png"/*tpa=http://www.darden.com/images/misc/home_bottom_TS.png*/ /></div>
    
    <div class="home_bottom_content_pad" style="padding-left:10px; padding-right:10px; width:850px; background-image: url(/images/core/side_shadows_repeater.png);
	background-repeat: repeat-y;">
    <div class="home_bottom_content">
          <div id="home_pod_left">
            <div id="home_video_callout" style="font-family:Arial, Helvetica, sans-serif; font-size:28px; color:#660000; line-height:28px; margin-bottom:15px;">Page Not Found</div>
            <div style="clear:both;"></div>
			<p>This page was not found or it has expired.<br /> 
You will now be redirected to our homepage.</p>
            </div>
            
            
            
        
      </div>
      </div>
  
  <div align="center" class="home_bottom_ts"><img src="../images/misc/home_bottom_BS.png"/*tpa=http://www.darden.com/images/misc/home_bottom_BS.png*/ /></div>
  
</div>


<div id="home_footer" style="text-align:center;">
 
  <script type="text/javascript" src="../scripts/basic-scripts.js"/*tpa=http://www.darden.com/scripts/basic-scripts.js*/></script>
<div align="center" style="margin-top:15px;">
<!--%  If DisplayDate >= CDate(#07/22/2011 6:00 AM#) Then %-->
    <!--a href="http://www.redlobster.com/" target="_blank"><img src="../images/core/rl_new.png"/*tpa=http://www.darden.com/images/core/rl_new.png*/ alt="RedLobster" border="0" /></a-->
<!--% Else %>
    <a href="http://www.redlobster.com/" target="_blank"><img src="../images/core/rl.png"/*tpa=http://www.darden.com/images/core/rl.png*/ alt="RedLobster" border="0" /></a>
<!--% End If %> -->

    <a href="http://www.olivegarden.com/" target="_blank"><img src="../images/core/olive_garden_logo_footer.png"/*tpa=http://www.darden.com/images/core/olive_garden_logo_footer.png*/ alt="Olive Garden Logo" border="0" /></a>
    <a href="http://www.longhornsteakhouse.com/" target="_blank"><img src="../images/core/lh.png"/*tpa=http://www.darden.com/images/core/lh.png*/ alt="Longhorn Steakhouse" border="0" /></a>
    <a href="http://www.bahamabreeze.com/" target="_blank"><img src="../images/core/bb.png"/*tpa=http://www.darden.com/images/core/bb.png*/ alt="Bahama Breeze" border="0" /></a>
    <a href="http://www.seasons52.com/" target="_blank"><img src="../images/core/s52.png"/*tpa=http://www.darden.com/images/core/s52.png*/ alt="Seasons 52" border="0" /></a>
    <a href="http://www.thecapitalgrille.com/" target="_blank"><img src="../images/core/tcg.png"/*tpa=http://www.darden.com/images/core/tcg.png*/ alt="The Capital Grille" border="0" /></a>
    <a href="http://www.eddiev.com/" target="_blank"><img src="../images/core/ev.png"/*tpa=http://www.darden.com/images/core/ev.png*/ alt="Eddie V's" border="0" /></a>
    <a href="http://www.yardhouse.com/" target="_blank"><img src="../images/core/yh.png"/*tpa=http://www.darden.com/images/core/yh.png*/ alt="Yard House" border="0" /></a>

    <br />
    <span class="copyright">Copyright &copy; <script type="text/javascript">document.write(GetYear());</script> 
        Darden Concepts, Inc. All rights reserved.<br />
        <a href="http://investor.darden.com/investors/corporate-governance/default.aspx" class="footerlink">Corporate Governance</a> &nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;
        <a href="http://www.darden.com/legal/legal.asp" class="footerlink">Legal Notices</a> &nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;
        <a href="http://www.darden.com/legal/privacy.asp" class="footerlink">Privacy Notice / Your California Privacy Rights</a>
    </span>
    <br /><br />
    
	<script type="text/javascript">        
                var addToHomeConfig = {
                    touchIcon: true
            };        
	</script>
<script src="add2home.js"/*tpa=http://www.darden.com/js/add2home.js*/ type="text/javascript"> </script>
</div>
</div>
<!-- SiteCatalyst code version: H.21.
Copyright 1996-2010 Adobe, Inc. All Rights Reserved
More info available at http://www.omniture.com -->



<script language="JavaScript" type="text/javascript"><!--
/* You may give each page an identifying name, server, and channel on
the next lines. */
s.pageName=""
s.pageType="errorPage"



/************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/
var s_code=s.t();if(s_code)document.write(s_code)//--></script>
<!--/DO NOT REMOVE/-->
<!-- End SiteCatalyst code version: H.21. -->
</body>
</html>
