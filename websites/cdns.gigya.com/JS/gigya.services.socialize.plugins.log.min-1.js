
gigya._.UI.registerPlugin(function() {
gigya = window.gigya;
/// <reference path="Unknown_83_filename"/*tpa=http://cdns.gigya.com/js/GSJSSDK.js*/ />
gigya.utils.object.add(gigya.log, {
	hide: function () {
		var divLog = document.getElementById('gigya_log');
		if (divLog) divLog.parentNode.removeChild(divLog);
	},
	clear: function () {
		gigya.log._log = [];
	},
	show: function () {
		var arHTML = ['<div style="text-align:right;background-color:#B9C3D5"><a style="text-align:right;font:15px arial" href="javascript:gigya.log.clear();gigya.log.show();">[ Clear ]</a><a style="text-align:right;font:15px arial" href="javascript:gigya.log.hide();">[ Close ]</a></div><div style="overflow:auto;height:500px">'];
		var arLog = gigya.log._log;

		for (var i = arLog.length - 1; i >= 0; i--) {
			arHTML.push('<pre style="border-bottom:1px solid #000;height: 200px;width:980px;overflow:auto;border:1px solid #000; padding:5px;margin:5px;">' + gigya.utils.sanitize.sanitizeHTML(arLog[i]) + '</pre>');
		}
		arHTML.push('</div>');
		var divLog = document.getElementById('gigya_log');
		if (!divLog) {
			divLog = document.createElement('div');
			divLog.id = 'gigya_log';
			with (divLog.style) {
				backgroundColor = "#ffffff";
				border = "2px solid #cccccc";
				position = "absolute";
				zIndex = "10000000";
				top = "5px";
				left = "5px";
				//width = '80%';
				border = '2px solid #000';
			}
			gigya.utils.DOM.appendToBody(divLog);
		}
		divLog.style.width = '1024px';
		divLog.style.height = '520px';
		divLog.innerHTML = arHTML.join('');
	},
	hideConfig: function () {
		gigya.log.divConfig.parentNode.removeChild(gigya.log.divConfig);
		delete gigya.log.divConfig;
	},
	showConfig: function () {
		if (!gigya.log.divConfig) {
			gigya.log.divConfig = gigya.utils.DOM.createTopLevelDiv('gigyaDebugConfig');
			gigya.log.divConfig.style.position = 'absolute';
			gigya.log.divConfig.style.top = '5px';
			gigya.log.divConfig.style.left = '5px';
			gigya.log.divConfig.innerHTML = [
				'<div style="font-size:12px; font-family: arial; padding:10px;background-color: #FFFFFF; border: 1px solid #000000;">',
					'<div><input type="checkbox" onchange="this.checked?gigya.log.enable():gigya.log.disable()" ' + (gigya.log._isEnabled() ? 'checked' : '') + '>Log gigya API calls, events &#38; callbacks</div>',
					'<div style="border-top:1px solid #000000;padding:5px;margin:5px;">You can drag the following links to your bookmarks toolbar for ease of use:<br /><a href="javascript:gigya.debug()">Config Debug</a><br /><a href="javascript:gigya.showLog()">Show Debug Log</a></div>',
					'<div><td align="center"><input type="button" value="Done" onclick="gigya.log.hideConfig()"></div>',
				'</div>'].join('');
			gigya.utils.DOM.appendToBody(gigya.log.divConfig);
		}
		gigya.log.divConfig.style.width = 500+'px';
		gigya.log.divConfig.style.height = 150+'px';
	}
});
});
