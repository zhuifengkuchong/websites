
function GenesisExchange_Gigya(c){
	this.conf=c;
	
	this.processAction=function(evt){
		if (typeof(this.conf)=='undefined' || typeof(this.conf.eventMap)=='undefined') 
			return null;
			
		var eventMap = this.eventLookup(evt);
				
		if(typeof(eventMap)=='object' && eventMap!=null) {
			var genConf={};
			if(typeof(eventMap.mapVars)=='object'){
				genConf=this.genEVarMap(evt,eventMap.mapVars);
			}
			if(typeof(eventMap.omtrEvents)=='string') {
				genConf["events"] = eventMap.omtrEvents;
			}
			else if(typeof(eventMap.omtrEvents)=='object') {
				genConf["events"] = eventMap.omtrEvents.join(',');
			}
			this.slb(genConf);
		}
	}
	
	this.eventLookup=function(evt){
		var en="", es="";
		
		// get the event name and source of the current event
		if(typeof(evt.eventName)!='undefined') {
			en=evt.eventName;
		} else {
			return null;
		}		
		if(typeof(evt.source)!='undefined') {
			es=evt.source;
		} 
		
		// Look for this event in the config, first matching event is used
		for (var i=0; i<this.conf.eventMap.length; i++) {
		    var c = this.conf.eventMap[i];
			if ( (c.gigEvent.toLowerCase()==en.toLowerCase() || c.gigEvent.toLowerCase()=="on"+en.toLowerCase()) 
					&& (typeof(c.gigSource)!="string" || c.gigSource.toLowerCase()==es.toLowerCase())) {
				return c;
			}
		}
		return null;
	}
	

	this.genEVarMap=function(evt, map) {
		var evars={};
		for(var i=0;i<map.length;i++) {
			var parts=map[i].split('=');
			if (parts.length==2) {
				var evar=parts[0].replace(/^\s+|\s+$/g,"");
				var val = parts[1].replace(/^\s+|\s+$/g, "");
				val=val.substr(val.length-2)=="()" ? val.substr(0,val.length-2) : val;
				var fv = this.getFieldValue(evt,val);
				if (typeof(evars[evar])!='undefined' && evars[evar]!='') 
					evars[evar]=evars[evar]+':'+fv;
				else
					evars[evar]=fv;
			}
			else {
				evars[parts[0]]='';
			}
		}
		return evars;
	}
	this.getFieldValue=function(evt,field) {
		if(typeof(this.conf[field])=='function') {
			// The field is a custom user function
			return this.conf[field].call(this.conf,evt);
		}
		else if(typeof(this[field])=='function') {
			// The field is a built-in function
			return this[field].call(this,evt);
		}
		else {
			var pathParts=field.split('.'), obj=evt;
			for (var i=0; i<pathParts.length; i++) {				
				if (typeof(obj[pathParts[i]])!='undefined') {
					obj=obj[pathParts[i]];
				}
				else
					return field;
			}
			if (typeof(obj)=="string" || typeof(obj)=="number") 
				return obj;
			// attempt to extract data from complex objects
			if (typeof(obj.length)=="number") {
				var r="";
				for(var i=0;i<obj.length;i++) {
					r+=(r?',':'')+obj[i];
				}
				return r
			}
			if (typeof(obj)=="object") {
				var r="";
				for(var f in obj) {
					r+=(r?',':'')+f;
				}
				return r;
			}
			return field;
		}
	}

	this.slb = function (st) {
		if (typeof (s_gi) != 'undefined') {
			var s_a = (this.conf['omtrAccount'] ? this.conf['omtrAccount'] : window.s_account);
			var s = s_gi(s_a), vl = '', ltvo = '', lteo = '', g = {};
			ltvo = s.linkTrackVars; lteo = s.linkTrackEvents;
			for (var p in st) {
				g[p] = (s[p] ? s[p] : "");
				if (st[p]) s[p] = st[p];
				vl += (vl ? ',' : '') + p;
				if (p == 'events') { s.linkTrackEvents = s.events = s[p] }
			}
			if (vl) { s.linkTrackVars = vl }
			else { s.linkTrackVars = s.linkTrackEvents = 'None' }
			s.tl(true, 'o', this.conf.linkName);
			s.linkTrackVars = ltvo; s.linkTrackEvents = lteo;
			for (var p2 in g) {
				s[p2] = g[p2];
			}
		}
	}


	// start built-in data extraction functions
	this.precision=function(p,v){
		if(typeof(v)=="string")
			return parseFloat(v).toFixed(p);
		else
			return v;
	}
	
	this.providerList=function(evt){
		var r='';
		if(typeof(evt)=='object' && typeof(evt.user)=='object' && typeof(evt.user.identities)=='object') {
			for(var p in evt.user.identities)
				r+=(r?',':'')+p;
		}
		return r;
	}
	// end built-in data extraction functions
}//end class GenesisExchange
