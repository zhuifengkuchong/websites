/// <reference path="Unknown_83_filename"/*tpa=http://cdns.gigya.com/js/GSJSSDK.js*/ />
if (typeof gigya == 'undefined') { gigya = new Object(); }
//if (typeof gigya.global == 'undefined') { gigya.global = {} }
if (typeof gigya.services == 'undefined') { gigya.services = {} }
if (typeof gigya.services.socialize == 'undefined') { gigya.services.socialize = {} }

//gigya.pluginUtils = gigya.pluginUtils == 'undefined' ? {} : gigya.pluginUtils;
//gigya.pluginUtils module
//gigya.pluginUtils.animation = {
//slideDown: function (el) {
//    var bp = gigya.global.getClassBordersAndPaddings(el.className);
//    var h = el.offsetHeight - bp.h;
//    var originalTransitionProperty = el.style.transitionProperty;
//    el.style.maxHeight = '1px';
//    el.style.transitionProperty = el.style.webkitTransitionProperty = 'max-height';
//    window.setTimeout(function () {
//        el.style.maxHeight = h + 'px';
//        el.style.transitionProperty = el.style.webkitTransitionProperty = originalTransitionProperty;
//    }, 100);
//    var fnOnTransitionEnd = function () {
//        el.style.maxHeight = '999px';
//        gigya.utils.DOM.removeEventListener(el, 'transitionend', fnOnTransitionEnd);
//        gigya.utils.DOM.removeEventListener(el, 'webkitTransitionEnd', fnOnTransitionEnd);
//    }
//    gigya.utils.DOM.addEventListener(el, 'transitionend', fnOnTransitionEnd);
//    gigya.utils.DOM.addEventListener(el, 'webkitTransitionEnd', fnOnTransitionEnd);
//    window.setTimeout(fnOnTransitionEnd, 500);
//}
//fadeIn: function (el, opacity) {
//    if (typeof opacity === 'undefined')
//        opacity = 1;

//    el.style.opacity = '0';
//    window.setTimeout(function () {
//        el.style.opacity = opacity + '';
//    }, 10);
//}
//};
//validation: {
//    isEmailValid: function (email) {
//        return gigya.pluginUtils.validation.isEmailListValid(email);
//    },
//    isEmailListValid: function (emails) {
//        var _email = "((?=(.{1,64}@.{1,255}))([!#$%&'*+\\-\\/=?\\^_`{|}~a-zA-Z0-9}]{1,64}(\\.[!#$%&'*+\\-\\/=?\\^_`{|}~a-zA-Z0-9]{1,}){0,})@((\\[(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}\\])|([a-zA-Z0-9-]{1,63}(\\.[a-zA-Z0-9-]{1,63}){1,})))";
//        var _nameAndEmail = '(("[^<]+<(' + _email + ')>")|(("[^"]*" *)?<(' + _email + ')>)|(' + _email + '))';
//        var _multipleEmails = '^( *)?(' + _nameAndEmail + ' *, *)*' + _nameAndEmail + '( *, *)?$';
//        var regex = new RegExp(_multipleEmails);

//        return regex.test(emails);
//    }
//},
//text: {
//    normalizeLinebreaks: function (t) {
//        if (!t || !t.replace) return t;
//        return t.replace(/\r/, '').replace(/\n/g, '\r\n');
//    }
//},

//gigya.pluginUtils.css = {
//    fixCss: function (css) {
//        if (gigya.localInfo.isIE7 || (gigya.localInfo.isIE && gigya.localInfo.quirksMode)) {
//            css = css.replace(/display:inline-block/g, 'display:inline-block;zoom:1;*display:inline');
//        }

//        return css
//            .replace(/gradient\((.*?),(.*?)\)/g,
//                'filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="$1", endColorstr="$2");' +
//                'background: linear-gradient(top,  $1,  $2);' +
//                'background: -ms-linear-gradient(top left, $1, $2);' +
//                'background: -webkit-gradient(linear, left top, left bottom, from($1), to($2));' +
//                'background: -moz-linear-gradient(top,  $1,  $2)');
//    }
//};

//gigya.pluginUtils.DOM = {
//setTextboxSubmitButton: function(elTextbox, elButton) {
//    var fnOnPress = function(e) {
//        var charCode;
//        if (e && e.which) {
//            e = e;
//            charCode = e.which;
//        } else {
//            e = event;
//            charCode = e.keyCode;
//        }
//        if (charCode == 13) {
//            elButton.click();
//        }
//    };

//    gigya.utils.DOM.addEventListener(elTextbox, 'keyup', fnOnPress);
//    gigya.utils.DOM.addEventListener(elButton, 'keyup', fnOnPress);
//},
//getRelativePosition: function (position, size, direction, margin, flip) {
//    var relativePosition = { top: 0, left: 0, bottom: 0, right: 0 };

//    if (direction === 'bottom') {
//        relativePosition.top = position.bottom + margin;
//        relativePosition.left = (flip ? position.right - size.w : position.left);
//    }
//    else if (direction === 'top') {
//        relativePosition.top = position.top - size.h - margin;
//        relativePosition.left = (flip ? position.right - size.w : position.left);
//    }
//    else if (direction === 'left') {
//        relativePosition.top = (flip ? position.bottom - size.h : position.top);
//        relativePosition.left = position.left - size.w - margin;
//    }
//    else {
//        relativePosition.top = (flip ? position.bottom - size.h : position.top);
//        relativePosition.left = position.right + margin;
//    }

//    relativePosition.bottom = relativePosition.top + size.h;
//    relativePosition.right = relativePosition.left + size.w;

//    return relativePosition;
//},
//placePopoverNearElement: function (elTarget, container, direction, margin, flip, adjustDirection) {
//    var targetPosition = gigya.utils.DOM.getElementPosition(elTarget);
//    size = { w: container.offsetWidth, h: container.offsetHeight };
//    var position = this.getRelativePosition(targetPosition, size, direction, margin, flip);

//    if (adjustDirection && !gigya.utils.viewport.isRectFullyVisible(position)) {
//        var opposite = {
//            bottom: 'top', left: 'right', right: 'left', top: 'bottom'
//        }

//        var newPosition = this.getRelativePosition(targetPosition, size, opposite[direction], margin);
//        if (gigya.utils.viewport.isRectHorizontallyVisible(newPosition))
//            position = newPosition;
//    }

//    if (!gigya.utils.viewport.isRectHorizontallyVisible(position)) {
//        var newPosition = this.getRelativePosition(targetPosition, size, direction, margin, !flip);
//        if (gigya.utils.viewport.isRectHorizontallyVisible(newPosition))
//            position = newPosition;
//    }

//    container.style.left = '' + position.left + 'px';
//    container.style.top = '' + position.top + 'px';
//    //container.style.minWidth = elPopover.offsetWidth + 'px';
//},
//addPopoverNearElement: function (elTarget, elPopover, direction, margin, flip, adjustDirection, animation, fnWhenRemoved, removeOnClickExlcludedElements, removeOnClickEvent) {
//    if (!elTarget || !elPopover) return;
//    if (!direction) direction = 'bottom';
//    if (!margin) margin = 0;
//    if (typeof adjustDirection === 'undefined') adjustDirection = true;
            
//    var container = document.createElement('div');
//    container.appendChild(elPopover);
//    container.style.position = 'absolute';
//    container.style.zIndex = (gigya.utils.DOM._nextZIndex++).toString();
//    container.style.left = '-1000px'; // Draw outside of screen to get the size
//    document.body.appendChild(container);
//    this.placePopoverNearElement(elTarget, container, direction, margin, flip, adjustDirection);
            
//    gigya.pluginUtils.DOM.removeElementOnDocClick(container, fnWhenRemoved, removeOnClickExlcludedElements,
//        removeOnClickEvent ? removeOnClickEvent : 'click');

//    if (animation && typeof gigya.pluginUtils.animation[animation] === 'function')
//        gigya.pluginUtils.animation[animation](elPopover);

//    return container;
//},
//removeElementOnDocClick: function (el, fnCallback, excludedElements, event) {
//    if (!el) return;
            
//    var fnRemove = function (e) {
//        var wasOpen = el && el.parentNode;
//        if (wasOpen) el.parentNode.removeChild(el);
//        if (fnCallback) fnCallback(e, wasOpen);
//    }

//    var arAllExcluded = [el];
//    if (excludedElements)
//        arAllExcluded = arAllExcluded.concat(excludedElements);

//    this.performOnDocClick(fnRemove, arAllExcluded, event);
//},
//hideElementOnDocClick: function (el, fnCallback, excludedElements) {
//    if (!el) return;

//    var fnHide = function () {
//        el.style.display = 'none';
//        if (fnCallback) fnCallback();
//    }

//    var arAllExcluded = [el];
//    if (excludedElements)
//        arAllExcluded.concat(excludedElements);

//    this.performOnDocClick(fnHide, arAllExcluded);
//},
//performOnDocClick: function (fnCallback, excludedElements, event) {
//    if (!event) event = 'mousedown';

//    var fnOnDocClick = function (e) {
//        var target = e.target || e.srcElement;
//        var clickInDiv = false;
//        while (target && target.parentNode) {
//            if (excludedElements && excludedElements.indexOf(target) != -1) {
//                clickInDiv = true;
//                break;
//            }
//            target = target.parentNode;
//        }
//        if (!clickInDiv) {
//            gigya.utils.DOM.removeEventListener(document, event, fnOnDocClick);
//            if (fnCallback) fnCallback(e);
//        }
//    }

//    gigya.utils.DOM.addEventListener(document, event, fnOnDocClick);
//}
//};

//gigya.pluginUtils.layout = {
//measureText: function (text, oStyle, maxW) {
//    if (isNaN(maxW) || maxW < 0) maxW = 0;
//    var ruler = document.getElementById('gigya_ruler');
//    var rulerText = document.getElementById('gigya_ruler_text');
//    if (ruler == null) {
//        ruler = document.createElement('div');
//        rulerText = document.createElement('span');

//        ruler.id = 'gigya_ruler';
//        rulerText.id = 'gigya_ruler_text';
//        ruler.style.visibility = 'hidden';
//        ruler.style.position = 'absolute';
//        ruler.style.margin = '0px';
//        ruler.style.padding = '0px';
//        ruler.appendChild(rulerText);
//        gigya.utils.DOM.appendToBody(ruler);
//    }
//    if (oStyle) {
//        ruler.style.lineHeight = oStyle.size + 'px';
//        ruler.style.fontFamily = oStyle.font;
//        ruler.style.fontSize = oStyle.size + 'px';
//        ruler.style.fontWeight = (('' + oStyle.bold).toLowerCase() == 'true') ? 'bold' : '';
//    }
//    if (maxW) {
//        ruler.style.whiteSpace = '';
//        ruler.style.width = '' + maxW + 'px';
//    } else {
//        ruler.style.whiteSpace = 'nowrap';
//        ruler.style.width = 'auto';
//    }
//    ruler.style.overflow = 'hidden';
//    ruler.style.display = '';
//    rulerText.innerHTML = text;
//    var w = rulerText.offsetWidth;
//    var h = rulerText.offsetHeight;
//    if (w == 0 || h == 0) {
//        w = ruler.offsetWidth;
//        h = ruler.offsetHeight;
//    }
//    ruler.style.display = 'none';
//    return { w: w, h: h };
//}
//};
//domain: {
//    isInDomain: function (domain) {
//        if (!domain) return;
//        var regexString = '^' + domain.replace(/\./g, '\\.').replace(/\*\\./g, '([a-zA-Z0-9]+\\.)*') + '$';
//        var regex = new RegExp(regexString);
//        return (regex.test(document.domain));
//    }
//}
//}

//gigya.pluginUtils.lang module
//gigya.pluginUtils.lang = {
//    // Get localized text from i18n injection
//	getLocalizedText: function (plugin, textKey, lang, replaceStr, withStr) {
//	    if (lang == 'te-st') return 'TEST_' + textKey.substring(0, 10);
//	    if (!lang) lang = 'en';

//	    var t = (gigya.i18n[plugin][lang] ? gigya.i18n[plugin][lang][textKey] : gigya.i18n[plugin]['en'][textKey]);
//	    if (t && replaceStr) t = t.replace(replaceStr, withStr);
//	    return t;
//	}
//}

//gigya.utils.mouse.getPosition = function () {
//	var posx = 0;
//	var posy = 0;
//	if (document.body) {
//		var e = this._mouseEventObject;
//		if (!e) e = { clientX: 0, clientY: 0 };
//		if (e.pageX || e.pageY) {
//			posx = e.pageX;
//			posy = e.pageY;
//		}
//		else if (e.clientX || e.clientY) {
//			posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
//			posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
//		}
//		return { x: posx, y: posy }
//	}
//}

//gigya.socialize._parseRID = function (rid) {
//    var ridp = rid.split('@');
//    var swf = document.getElementById(ridp[2] + '_' + ridp[1]);
//    var containerID = ridp[2];
//    var container;

//    if (null == (container = gigya.utils.DOM._pseudoContainers[containerID])) {
//        container = document.getElementById(ridp[2]);
//    }

//    var Reqs = null;
//    if (container != null) Reqs = container.Reqs;
//    var req = null;
//    if (Reqs != null) req = Reqs[parseInt(ridp[0])];

//    return { rid: rid, container: container, Reqs: Reqs, req: req, swf: swf };
//},


//gigya.socialize._closeComponent = function (rid, dispatchCloseEvent) {
//    var oRID = gigya.socialize._parseRID(rid);
//    var req = oRID.req;
//    if (gigya.socialize.GrayOut) gigya.socialize.GrayOut(false);
//    var ifr = document.getElementById('gigya_ifr_' + req.container.id)
//    if (ifr != null) ifr.style.display = 'none';
	
//    if (dispatchCloseEvent) {
//        gigya.events.dispatchForWidget({ eventName: 'close' }, req.p);
//    }
//    req.container.style.display = 'none';
//}
	
//gigya.services.socialize._createJSPopup = function (rid, resolver, captionText, noCaption) {
//    var oRID = gigya.services.socialize._parseRID(rid);
	
//    var req = oRID.req;
//    var params = req.p;
//    var conf = req.c;
//    if (gigya.utils.validation.isExplicitFalse(params['showCaption'])) noCaption = true;
	
//    var GS = gigya.services.socialize;
//    var container = document.getElementById(params['containerID']);
//    req.container = container;
	
//    var oBgStyle = resolver.Resolve('+/config/body/background');
//    var oCaptionStyle = resolver.Resolve('+/config/body/captions');
	
//    var s = '<table style="' + GS.getStyleString(oBgStyle) + ';empty-cells:show;" cellpadding="0" cellspacing="0" >';
//    if (!noCaption) {
//        s += '<tr style="' + GS.getStyleString(oCaptionStyle) + '"><td><table style="width:100%" width="100%" cellpadding="0" cellspacing="0"><tr id="' + params['containerID'] + '_caption"><td style="vertical-align:middle"><div style="' + GS.getStyleString(oCaptionStyle) + ';padding:7px">' + captionText + '</div></td><td style="vertical-align:middle">';
//        s += '<div align="right" style="padding-right: 7px; cursor: pointer;' + GS.getStyleString(oCaptionStyle) + '"><img onclick="gigya.services.socialize._closeComponent(\'' + rid + '\', true);"  src="' + gigya._.getCdnResource('../gs/i/HTMLLogin/closeIcon-1.gif'/*tpa=http://cdns.gigya.com/gs/i/HTMLLogin/closeIcon.gif*/) + '" alt="" /></div></td></tr></table></td></tr><tr><td colspan="2">';
//    } else {
//        s += '<tr><td>';
//    }
//    s += '<div id="' + params['containerID'] + '_container"></div></td></tr></table>';
//    container.innerHTML = s;
//    var p = {};
//    var c = {};
//    for (var param in params) {
//        p[param] = params[param];
//    }

//    for (param in conf) {
//        c[param] = conf[param];
//    }
	
//    var caption = document.getElementById(p['containerID'] + '_caption');

//    p['height'] = p['height'] - (oBgStyle['frame-thickness'] * 2);
//    if (!noCaption) p['height'] -= caption.offsetHeight;
//    p['width'] = p['width'] - (oBgStyle['frame-thickness'] * 2);
	
//    gigya.utils.DOM._popupContainers[p['containerID']] = container
//    p['containerID'] = p['containerID'] + '_container';
//    return { c: c, p: p }
//}
	
//gigya.services.socialize._createJSPopup2 = function (rid, resolver, borderSize, base, captionText, addBorder) {
//    var oRID = gigya.services.socialize._parseRID(rid);
	
//    var req = oRID.req;
//    var params = req.p;
//    var conf = req.c;

//    var GS = gigya.services.socialize;
//    var container = document.getElementById(params['containerID']);
//    req.container = container;

//    var oCaptionStyle = resolver.Resolve('+/config/body/captions');

//    if (!borderSize) borderSize = 10;
//    if (!base) base = gigya._.getCdnResource('/gs/i/Dialog/DialogBg_');
//    var imgStyle = 'width:' + borderSize + 'px;height:' + borderSize + 'px';

//    var borderString;
//    if (addBorder) {
//        borderString = '1px solid #555555';
//    }

//    var s = '<table style="font-size:1px;padding:0px;margin:0px;vertical-align:middle;font-size:1px;line-height:1px;empty-cells:show;" cellpadding="0" cellspacing="0" >';
//    s += '<tr><td height="' + borderSize + '" style="font-size:1px"><div style="height:' + borderSize + 'px"><img src="' + base + 'TopLeft.png" style="' + imgStyle + '" alt="" /></div></td><td height="' + borderSize + '" style="background-image:url(\'' + base + 'filler.png\');bakground-repeat:repeat"></td><td height="' + borderSize + '" style="font-size:1px;"><div style="height:' + borderSize + 'px"><img style="' + imgStyle + '" src="' + base + 'TopRight.png" alt="" /></div></td></tr>';
//    if (captionText) {
//        s += '<tr><td style="background-image:url(\'' + base + 'filler.png\');background-repeat:repeat"></td><td><table width="100%" cellspacing="0" cellpadding="0" style="border-top:' + borderString + ';border-left:' + borderString + ';border-right:' + borderString + ';width:100%;background-color:#FFFFFF"><tr><td style="padding-top:10px;padding-left:10px;' + GS.getStyleString(oCaptionStyle) + '">' + captionText + '</td><td align="right" valign="top" style="' + GS.getStyleString(oCaptionStyle) + '"><div style="padding-right:5px;padding-top:5px;text-align:right;"><img style="cursor:pointer" onclick="gigya.services.socialize._closeComponent(\'' + rid + '\', true);" src="' + base + 'close.png" alt="" /></div></td></tr></table></td><td style="background-image:url(\'' + base + 'filler.png\');background-repeat:repeat"></td></tr>';
//    }
//    s += '<tr><td style="background-image:url(\'' + base + 'filler.png\');background-repeat:repeat"></td><td><div style="border-bottom:' + borderString + ';border-left:' + borderString + ';border-right:' + borderString + ';background-color:#FFFFFF" id="' + params['containerID'] + '_container"></div></td><td style="background-image:url(\'' + base + 'filler.png\');background-repeat:repeat"></td></tr>';
//    s += '<tr><td height="' + borderSize + '" style="font-size:1px;"><div style="height:' + borderSize + 'px"><img style="' + imgStyle + '" src="' + base + 'BottomLeft.png" alt="" /></div></td><td height="' + borderSize + '" style="background-image:url(\'' + base + 'filler.png\');bakground-repeat:repeat"></td><td height="' + borderSize + '" style="font-size:1px;"><div style="height:' + borderSize + 'px"><img style="' + imgStyle + '" src="' + base + 'BottomRight.png" alt="" /></div></td></tr></table>';
//    container.innerHTML = s;
//    var p = {};
//    var c = {};
//    for (var param in params) {
//        p[param] = params[param];
//    }

//    for (param in conf) {
//        c[param] = conf[param];
//    }

//    gigya.utils.DOM._popupContainers[p['containerID']] = container
//    p['containerID'] = p['containerID'] + '_container';


//    p['height'] = p['height'] - borderSize * 2;
//    if (captionText) {
//        if (captionText.replace(/ /g, '') == '') {
//            p['height'] -= 10;
//        } else {
//            p['height'] -= gigya.pluginUtils.layout.measureText(captionText, oCaptionStyle).h + 10;
//        }
//    }
//    p['width'] = p['width'] - borderSize * 2;

//    return { c: c, p: p }
//}


//gigya.global.wbr = function (s, n) {
//	s = s.replace(/\n/g, '\n ');
//	var arWords = s.split(' ');
//	var chHyphen = '&#173;';
//	for (var i = 0; i < arWords.length; i++) {
//		if (arWords[i].length > n) {
//			var chHyphenCurrent = chHyphen;
//			var arWord = arWords[i].split('');
//			var stop = false;
//			var stopEndChar;
//			var currentN = -1;
//			for (var u = 0; u < arWord.length; u += 1) {
//				if (!stop) currentN++;
//				if (currentN == n) {
//					arWord.splice(u, 0, chHyphenCurrent);
//					currentN = -1;
//				}
//				if (arWord[u] == '&') {
//				    stop = true;
//				    stopEndChar = ';';
//				} else if (arWord[u] == '<') {
//				    stop = true;
//				    stopEndChar = '>';
//				}
//				if (stop && arWord[u] == stopEndChar) stop = false;
//			}
//			arWords[i] = arWord.join('');
//		}
//	}
//	return arWords.join(' ').replace(/\n /g, '\n');
//}
//gigya.global.scrollToElement = function (el) {
//    if (el) {
//        var pos = gigya.global._GetElementPos(el);
//        window.scrollTo(pos.left, pos.top);
//    }
//}

//gigya.global.setPlaceholder = function (textbox, placeholder, className) {
//    if (!textbox) return;

//	var onblur = function () {
//	    if (this.value == '') {
//	        if (className) {
//	            gigya.utils.DOM.addClassToElement(textbox, className);
//	        } else {
//	            textbox.style.color = '#585858';
//	        }
//	        this.value = placeholder
//	        this.placeholderCleared = false;
//	    }
//	};

//	var onfocus = function () {
//	    if (!this.placeholderCleared) {
//	        if (className) {
//	            gigya.utils.DOM.removeClassFromElement(textbox, className);
//	        } else {
//	            textbox.style.color = '';
//	        }
//	        this.value = '';
//	        this.placeholderCleared = true;
//	        if (textbox.setSelectionRange) { //to fix a bug where sometimes the caret disappears while the textbox is focused.
//	            textbox.focus();
//	            textbox.setSelectionRange(0, 0);
//	        }
//	        else if (textbox.createTextRange) {
//	            var range = textbox.createTextRange();
//	            range.collapse(true);
//	            range.moveEnd('character', 0);
//	            range.moveStart('character', 0);
//	            range.select();
//	        }
//	    }
//	};

//	textbox.refreshPlaceholder = function () {
//	    if (textbox.value) {
//            if (className)
//                gigya.utils.DOM.removeClassFromElement(textbox, className);
//            else
//                textbox.style.color = '';

//            this.placeholderCleared = true;
//	    } else {
//            if (className)
//                gigya.utils.DOM.addClassToElement(textbox, className);
//            else
//                textbox.style.color = '#585858';

//            textbox.value = placeholder;
//	        this.placeholderCleared = false;
//	    }
//	};

//	gigya.utils.DOM.addEventListener(textbox, 'blur', onblur);
//	gigya.utils.DOM.addEventListener(textbox, 'focus', onfocus);
//	gigya.utils.DOM.addEventListener(textbox, 'change', onblur);
//    textbox.refreshPlaceholder();
//}

//gigya.global.scaleImageToContainer = function (img, dontCenter, fnOnImgLoad) {
//	if (!dontCenter) {
//		img.parentNode.style.textAlign = 'left';
//		img.parentNode.style.verticalAlign = 'top';
//	}
//	var style = function (name) {
//	    var node = img.parentNode;
//	    var dim = gigya.global.getStyle(node, name);
//	    while (dim == 'auto') {
//	        node = node.parentNode;
//	        dim = gigya.global.getStyle(node, name);
//	    }
//	    var n = parseInt(dim);
//		if (isNaN(n)) n = 0;
//		return n;
//	}
//	//var size = Math.min(img.parentNode.offsetWidth, img.parentNode.offsetHeight);
//	var size = Math.min(style('width'), style('height'));
//	if (gigya.localInfo.quirksMode) {
//		var bordersAndPaddings = gigya.global.getBordersAndPaddings(img.parentNode);
//		var w = style('width') - bordersAndPaddings.w;
//		var h = style('height') - bordersAndPaddings.h;
//		size = Math.min(w, h);
//	}
//	gigya.global.scaleImage(img, size, dontCenter, fnOnImgLoad);
//}
	
//gigya.global.showLoader = function (container, className, h) {
//	if (container) {
//		if (!className) className = '';
//		if (!h) h = parseInt(gigya.global.getStyle(container, 'height'));
//		var paddingTop = (h - 32) / 2;
//		container.innerHTML = '<div class="' + className + '" style="height:' + h + 'px;background-image:url(\'' + gigya._.getCdnResource('../gs/i/gm/loader-1.gif'/*tpa=http://cdns.gigya.com/gs/i/gm/loader.gif*/)+ '\'); background-repeat:no-repeat;background-position:center center"></div>';
//	}
//}
	
//gigya.global.getPhotoURL = function (url, defaultURL) {
//	if (defaultURL && (!url || url == '')) {
//		return gigya.global.getPhotoURL(defaultURL);
//	}
//	if (url && url!='' && gigya.thisScript.protocol == 'https' && url.indexOf('http:') == 0) {
//	    return gigya._.getCdnResource('/proxy/photos.ashx?u=' + gigya.utils.URL.URLEncode(url));
//	} else {
//		return url;
//	}
//}
	
//gigya.global.scaleImage = function (img, size, dontCenter, fnOnImgLoad) {
//	var imgObj = img;
//	if (!img.width || !img.height) {
//		var imgObj = new Image();
//		imgObj.src = img.src;
//	}
//	var fnOnLoad = function () {
//		if (imgObj.width == 0) {
//			window.setTimeout(fnOnLoad, 100)
//		} else {
//			if (imgObj.width > imgObj.height) {
//				img.style.width = size + 'px';
//			} else {
//				img.style.height = size + 'px';
//			}
	
//			img.style.display = '';
//			if (fnOnImgLoad) fnOnImgLoad(img);
//			if (!dontCenter) {
//				img.style.marginTop = (size - img.offsetHeight) / 2 + 'px'
//				img.style.marginLeft = (size - img.offsetWidth) / 2 + 'px'
//			}
//		}
//	}
//	if (imgObj.width > 0) {
//		fnOnLoad();
//	} else {
//		img.style.display = 'none';
//		imgObj.onload = function () { window.setTimeout(fnOnLoad, 50) };
//	}
//}

//gigya.global.createGMBalloon = function (body, captionText, w, h, nubY, nubPosition, hideClose, id, dontHideOnClick, balloonClass) {
//    balloonClass = balloonClass ? 'gig-balloon ' + balloonClass : 'gig-balloon';
//	gigya.global.removeGMBalloon();
//	var now = (new Date()).getTime();
//	var balloonID = 'gig_gmBalloon_' + now;
//	if (id) balloonID = id;
//	var frameID = balloonID + '_frame';
//	var balloonHTML = gigya.global.getBalloonHTML(body, captionText, w, h, nubY, nubPosition, hideClose, 'gigya.global.removeGMBalloon()', frameID);
//	var div = document.createElement('div');
//	div.className = balloonClass;
//	div.style.position = 'absolute';
//	div.style.zIndex = gigya.utils.DOM._nextZIndex++;
//	div.innerHTML = balloonHTML;
//	div.id = balloonID;
	
//	gigya.global.fadeIn(div);
//	gigya.utils.DOM.appendToBody(div);
//	gigya.global.divGMBalloon = div;
	
//	if (!dontHideOnClick) {
//		window.setTimeout(function () {
//			if (document.attachEvent) {
//				document.attachEvent("onclick", gigya.global.removeGMBalloon)
//			} else if (document.addEventListener) {
//				document.addEventListener("click", gigya.global.removeGMBalloon, false)
//			}
//		}, 50);
//	}
//	return div;
//}
//gigya.global.fadeIn = function (el) {
//	if (el.interval) {
//		clearInterval(el.interval);
//	}
//	el.style.opacity = '0';
//	el.interval = setInterval(function () {
//		var op = parseFloat(el.style.opacity);
//		if (op >= 1) clearInterval(el.interval);
//		el.style.opacity = op + 0.1;
//	}, 10);
//}
	
//gigya.global.getBordersAndPaddings = function (el) {
//	var style = function (name) {
//		var n = parseInt(gigya.global.getStyle(el, name));
//		if (isNaN(n)) n = 0;
//		return n;
//	}
//	return {
//		w: style('border-left-width') + style('border-right-width') + style('padding-right') + style('padding-left'),
//		h: style('border-top-width') + style('border-bottom-width') + style('padding-top') + style('padding-bottom')
//	}
//}

//gigya.global.getClassInnerSize = function (className, w, h) {
//	var el = document.createElement('div');
//	el.className = className;
//	gigya.utils.DOM.appendToBody(el);
//	var bordersAndPaddings = gigya.global.getBordersAndPaddings(el);
//	w -= bordersAndPaddings.w;
//	h -= bordersAndPaddings.h;
//	el.parentNode.removeChild(el);
//	return { w: w, h: h }
//}

//gigya.global.applyEllipsis = function (text, maxRealChars) {
//	var pfx = '';
//	var tagstack = [];
//	var reTag = /\<(\/)?([^ \/>]+)[ ]?[^>]*(\/)?\>/i;
//	var realChars = 0;

//	for (var i = 0; ((i < text.length) && ((realChars < maxRealChars) || (maxRealChars == 0))); ) {
//		var ch = text.substr(i, 1);
//		if (ch == '<') {
//			var idxCloseTag = text.indexOf('>', i);
//			var tag = text.substring(i, idxCloseTag + 1);
//			reTag.lastIndex = 0;
//			var matches = reTag.exec(tag); // 1=/? 2=tagname 3=trailing / 0=all
//			/*if (matches == null) {
//			//alert(tag + '\n Is MALFORMED');
//			}*/
//			var tagname = matches[2].toLowerCase();
//			if (matches[1] == '/') { //closing tag
//				if ((tagstack.length > 0) && (tagstack[tagstack.length - 1].tagname == tagname)) {
//					tagstack.pop();
//				}
//			}
//			else { //new tag
//				if (matches[3] == '/') { //nothing to do (nothing to push onto the stack), rest is common
//				}
//				else {
//					var clAll = '</' + matches[2] + '>'
//					if (tagstack.length > 0) clAll += tagstack[tagstack.length - 1].cl;
//					tagstack.push({ tagname: matches[2], cl: clAll });

//				}
//			}
//			pfx += matches[0];
//			i += matches[0].length - 1;

//		}
//		else if (ch == '&') {
//			var idxCloseEntity = text.indexOf(';', i);
//			if (idxCloseEntity - i > 8) {
//				pfx += '&'
//			}
//			else {
//				pfx += text.substring(i, idxCloseEntity + 1);
//				i = idxCloseEntity;
//			}
//			realChars++;
//		}
//		else {
//			pfx += ch;
//			realChars++;
//		}
//		i++;
//	}
//	if (realChars < maxRealChars) {
//		return text;
//	}
//	else {
//		return pfx + '&#133;' + ((tagstack.length > 0) ? (tagstack[tagstack.length - 1].cl) : '');
//	}
//}

//gigya.global.getClassBordersAndPaddings = function (className) {
//	var el = document.createElement('div');
//	el.className = className;
//	gigya.utils.DOM.appendToBody(el);
//	var bordersAndPaddings = gigya.global.getBordersAndPaddings(el);
//	el.parentNode.removeChild(el);
//	return bordersAndPaddings;
//}

//gigya.global.getBalloonHTML = function (body, captionText, w, h, nubY, nubPosition, hideClose, sOnClose, frameID) {
//    var direction;
//    var nubW = 13;
//    var nubH = 25;
//    if (!gigya.global.addedBalloonCSS) {
//        gigya.global.addCSS([
//			'.gig-balloons *, div.gig-balloons, .gig-balloons span, .gig-balloons a:hover, .gig-balloons a:visited, .gig-balloons a:link, .gig-balloons a:active{',
//				'border:none; line-height:normal;padding:0px;margin:0px;color:inherit;text-decoration:none;width:auto;float:none;-moz-border-radius:0;border-radius:0;',
//				'font-family:arial;font-size:10px;color:#797979;background:none;text-align:left}',
//			'div.gig-balloon-frame {',
//				'zoom:1;padding:15px 10px 10px 10px; border:1px solid #b4b4b4; min-height:80px; background-color:#FFFFFF; z-index:' + (gigya.utils.DOM._nextZIndex++) + ';',
//				'box-shadow:0 0 5px RGBA(0,0,0,0.6);-webkit-box-shadow:0 0 5px RGBA(0,0,0,0.6);-moz-box-shadow:0 0 5px RGBA(0,0,0,0.6);}',
//            'div.gig-balloon-nub {left:+1px; position:relative;background-image:url("' + gigya._.getCdnResource('../gs/i/gm/nub-1.png'/*tpa=http://cdns.gigya.com/gs/i/gm/nub.png*/) + '");width:' + nubW + 'px;height:' + nubH + 'px; z-index:' + (gigya.utils.DOM._nextZIndex++) + ' }',
//			'div.gig-balloon-nub-right { left:-1px; position:relative;background-image:url("' + gigya._.getCdnResource('../gs/i/gm/nub_right-1.png'/*tpa=http://cdns.gigya.com/gs/i/gm/nub_right.png*/) + '");width:' + nubW + 'px;height:' + nubH + 'px; z-index:' + (gigya.utils.DOM._nextZIndex++) + ' }',
//            'div.gig-balloon-nub-up { top:+1px; position:relative;background-image:url("' + gigya._.getCdnResource('../gs/i/gm/nub_up-1.png'/*tpa=http://cdns.gigya.com/gs/i/gm/nub_up.png*/) + '");width:' + nubH + 'px;height:' + nubW + 'px; z-index:' + (gigya.utils.DOM._nextZIndex++) + ' }',
//            'div.gig-balloon-nub-down { top:-1px; position:relative;background-image:url("' + gigya._.getCdnResource('../gs/i/gm/nub_down-1.png'/*tpa=http://cdns.gigya.com/gs/i/gm/nub_down.png*/) + '");width:' + nubH + 'px;height:' + nubW + 'px; z-index:' + (gigya.utils.DOM._nextZIndex++) + ' }',
//			'span.gig-balloon-closeImage { cursor:pointer; float:right;background-image:url("' + gigya._.getCdnResource('../gs/i/gm/CloseButton-1.png'/*tpa=http://cdns.gigya.com/gs/i/gm/CloseButton.png*/) + '"); width:14px;height:14px; margin:-7px -2px 0 0  }',
//			'div.gig-balloon-caption {font-family:arial; font-size: 12px; font-weight:bold; color: #0b81c1; border-bottom: 1px solid #e8e8e8;padding-bottom:2px;}',
//			'span.gig-balloon-caption-text {float:left}'
//        ]);
//        if (gigya.localInfo.isIE && !(!gigya.localInfo.quirksMode && gigya.localInfo.isIE9)) {
//            gigya.global.addCSS([
//				'div.gig-balloon-frame {',
//					'-ms-filter: "progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color=\'#B4B4B4\');";',
//					'filter: progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color=\'#B4B4B4\');}',
//				'}',
//				'div.gig-balloon-nub-right { left:-5px;}'
//            ]);
//        }
//        gigya.global.addedBalloonCSS = true;
//    }
//    var frameStyle = '';
//    if (gigya.localInfo.quirksMode) {
//        var bordersAndPaddings = gigya.global.getClassBordersAndPaddings('gig-balloon-frame');
//        if (w) w = w + bordersAndPaddings.w;
//        if (h) h = h + bordersAndPaddings.h;
//    }
//    if (w) frameStyle += 'width: ' + w + 'px;';
//    if (h) frameStyle += 'height: ' + h + 'px;';
//    var nubStyle = '';
//    if (nubY) {
//        if ((nubPosition === 'left') || (nubPosition === 'right'))
//            nubStyle += 'top: ' + nubY + 'px;';
//        else // ((nubPosition === 'up') || (nubPosition === 'down'))
//            nubStyle += 'left: ' + nubY + 'px;';
//    }

//    var innerHTML = '';
//    if (captionText) {
//        innerHTML += '<div class="gig-balloon-caption"><span class="gig-balloon-caption-text" style="width:' + (w - 20) + 'px">' + captionText + '</span>';
//    } else {
//        innerHTML += '<div class="gig-balloon-caption" style="border-bottom:none">';
//    }
//    if (!hideClose && sOnClose) {
//        innerHTML += '<span class="gig-balloon-closeImage" onclick="' + sOnClose + '"></span>';
//    }
//    innerHTML += '<div style="clear:both;height:0;width:0;font-size:1px;"><!-- empty divs=100% height IE --></div></div><div class="gig-balloon-body">' + body + '</div>';
//    var s;
//    if (!nubY) {
//        s = '<div class="gig-balloon gig-balloon-frame" style="' + frameStyle + '">' + innerHTML + '</div>';
//    } else {
//        s = '<table cellpadding="0" cellspacing="0" class="gig-balloon"><tr>';
//        if (nubPosition === 'up') {
//            s += '<td style=""><div class="gig-balloon-nub-up" style="' + nubStyle + '"></div></td></tr><tr>';
//        }
//        if (nubPosition === 'left') { // check for other values
//            s += '<td style="vertical-align:top"><div class="gig-balloon-nub" style="' + nubStyle + '"></div></td>';
//        }
//        s += '<td style="vertical-align:top"><div id="' + frameID + '" class="gig-balloon-frame" style="' + frameStyle + '">' + innerHTML + '</div></td>';
//        if (nubPosition === 'right') {
//            s += '<td style="vertical-align:top"><div class="gig-balloon-nub-right" style="' + nubStyle + '"></div></td>';
//        }
//        if (nubPosition === 'down') {
//            s += '</tr><tr><td style="vertical-align:top"><div class="gig-balloon-nub-down" style="' + nubStyle + '"></div></td>';
//        }
//        s += '</tr></table>';
//        s += '<div style="clear:both;height:0;width:0;font-size:1px;"><!-- empty divs=100% height IE --></div>';
//    }
//    return s;
//}

//gigya.global.addIframeShim = function (el, parent) {
//	if (el.shim) return;
//	var shim = document.createElement('IFRAME');
//	shim.frameborder = "0";
//	shim.frameBorder = "0";
//	shim.allowtransparency = true;
//	shim.style.position = 'absolute';
//	shim.update = function (updateDimensions) {
//		if (shim && el) {
//		    if (parent) {
//				shim.style.left = el.offsetLeft + 'px';
//				shim.style.top = el.offsetTop + 'px';
//			} else {
//				var pos = gigya.global._GetElementPos(el);
//				shim.style.left = pos.left + 'px';
//				shim.style.top = pos.top + 'px';
//			}
//			if (updateDimensions) {
//				shim.style.width = el.offsetWidth + 'px';
//				shim.style.height = el.offsetHeight + 'px';
//			}
//		}
//	}
//	shim.update(true);
//	if (parent) {
//		shim.style.zIndex = gigya.utils.DOM._nextZIndex++;
//		el.style.zIndex = gigya.utils.DOM._nextZIndex++;
//		parent.appendChild(shim);
//	} else {
//		gigya.utils.DOM.appendToBody(shim);
//	}

//	el.shim = shim;
//}

//gigya.global.removeIframeShim = function (el) {
//	if (el && el.shim && el.shim.parentNode) {
//		el.shim.parentNode.removeChild(el.shim);
//		el.shim = null;
//	}
//}

//gigya.global.fillUserActionTemplate = function (userAction, o) {
//	for (var p in userAction) {
//		var field = userAction[p];
//		if (typeof field == 'string') {
//			userAction[p] = gigya.utils.templates.fill(userAction[p], o);
//		}
//		if (p == 'mediaItems') {
//			for (var i = 0; i < field.length; i++) {
//				if (field[i].src) {
//					field[i].src = gigya.utils.templates.fill(field[i].src, o);
//				}
//			}
//		}
//	}
//	return userAction;
//}

//gigya.global.preloadImages = function (arImages, fnCallback) {
//	var arImageObjects = [];
//	var loaded = 0;
//	var fncImgOnload = function () {
//		loaded++;
//		if (loaded == arImages.length) {
//			fnCallback();
//		}
//	}
//	for (var i = 0; i < arImages.length; i++) {
//		if (arImages[i] == null) {
//			fncImgOnload();
//		} else {
//			var img = new Image();
//			img.onload = fncImgOnload;
//			img.onerror = fncImgOnload;
//			img.src = arImages[i];
//			arImageObjects.push(img);
//		}
//	}
//}

//gigya.global.putGMBalloonNextTo = function (el, body, caption, w, h, hideClose, id, dontHideOnClick, preferredOrientation, balloonClass) {
//    balloonClass = balloonClass || '';
//    preferredOrientation = preferredOrientation || 'left';

//    if ((preferredOrientation === 'right') || (preferredOrientation === 'left'))
//        gigya.global.createGMBalloon(body, caption, w, h, 47, 'left', hideClose, id, dontHideOnClick, balloonClass);
//    else // ((preferredOrientation === 'up') || (preferredOrientation === 'down'))
//        gigya.global.createGMBalloon(body, caption, w, h, 10, 'down', hideClose, id, dontHideOnClick, balloonClass);

//    var offsetWidth = gigya.global.divGMBalloon.offsetWidth;
//    var offsetHeight = gigya.global.divGMBalloon.offsetHeight;
//    var pos = gigya.global._GetElementPos(el);
//    var left;
//    var top;

//    var dst = el.style;

//    var de = document.documentElement;
//    var db = document.body;

//    var clientHeight = de.clientHeight;
//    if (clientHeight == 0) clientHeight = db.clientHeight;
//    var clientWidth = de.clientWidth;
//    if (clientWidth == 0) clientWidth = db.clientWidth;

//    if (window.innerHeight) {
//        clientHeight = window.innerHeight;
//        clientWidth = window.innerWidth;
//    }

//    var scrl = gigya.utils.viewport.getScroll();
//    var vpt = scrl.top;
//    var vpl = scrl.left;
//    var bottomRightTop = vpt + clientHeight;
//    var bottomRightLeft = vpl + clientWidth;

//    var arrowDirection;
//    var arrowOffset;

//    if ((preferredOrientation === 'right') || (preferredOrientation === 'left')) {
//        arrowOffset = 47;

//        top = pos.top + (el.offsetHeight / 2) - 47 - 25 / 2;
//        if (top + offsetHeight > bottomRightTop) {
//            top = bottomRightTop - offsetHeight - 10;
//        }

//        var leftWithLeftOrientation = pos.left - offsetWidth - 5;
//        var leftWithRightOrientation = pos.left + el.offsetWidth + 5;

//        if (preferredOrientation === 'right') {
//            // assume there is always enough space on the right
//            left = leftWithRightOrientation;
//            arrowDirection = 'left';
//            balloonClass += ' gig-balloon-right';
//        } else { // left orientation
//            // check that there is enough space on the left - otherwise switch to right orientation
//            if (leftWithLeftOrientation > 10) {
//                left = leftWithLeftOrientation;
//                arrowDirection = 'right';
//                balloonClass += ' gig-balloon-left';
//            } else { // not enough space for left orientation
//                left = leftWithRightOrientation;
//                arrowDirection = 'left';
//                balloonClass += ' gig-balloon-right';
//            }
//        }
//    }

//    if ((preferredOrientation === 'up') || (preferredOrientation === 'down')) {
//        arrowOffset = 10;
//        left = pos.left;

//        var topWithUpOrientation = pos.top - offsetHeight - 10;
//        var topWithDownOrientation = pos.top + el.offsetHeight + 5;

//        if (preferredOrientation === 'down') {
//            // assume there is always enough space down
//            top = topWithDownOrientation;
//            arrowDirection = 'up';
//            balloonClass += ' gig-balloon-down';
//        } else { // up orientation
//            // check that there is enough space above - otherwise switch to down orientation
//            if (topWithUpOrientation < 5) {
//                top = topWithDownOrientation;
//                arrowDirection = 'up';
//                balloonClass += ' gig-balloon-down';
//            } else { // not enough space for up orientation
//                top = topWithUpOrientation;
//                arrowDirection = 'down';
//                balloonClass += ' gig-balloon-up';
//            }
//        }
//    }

//    gigya.global.createGMBalloon(body, caption, w, h, arrowOffset, arrowDirection, hideClose, id, dontHideOnClick, balloonClass);

//    if (left < 10) left = 10;
//    if (top < 10) top = 10;
//    gigya.global.divGMBalloon.style.left = left + 'px';
//    gigya.global.divGMBalloon.style.top = top + 'px';
//    if (gigya.localInfo.isIE) {
//        var frame = document.getElementById(gigya.global.divGMBalloon.id + '_frame');
//        if (frame) gigya.global.addIframeShim(frame);
//    }
//}

//gigya.global.removeGMBalloon = function (e) {
//	var a;
//	if (e) {
//		a = e.target;
//		if (!a) a = e.srcElement;
//		while (a != null && a.offsetParent && a.id.indexOf(gigya.global.divGMBalloon.id) == -1) {
//			a = a.offsetParent;
//		}
//	}
//	if (!a || a.id.indexOf(gigya.global.divGMBalloon.id) == -1) { //to make sure this wasn't triggered by click inside
//		try {
//			gigya.global.removeIframeShim(document.getElementById(gigya.global.divGMBalloon.id + '_frame'));
//			gigya.global.divGMBalloon.parentNode.removeChild(gigya.global.divGMBalloon);
//			if (document.detachEvent) {
//				document.detachEvent("onclick", gigya.global.removeGMBalloon)
//			} else if (document.removeEventListener) {
//				document.removeEventListener("click", gigya.global.removeGMBalloon, false)
//			}
//		} catch (ex) { }
//	}
//}

//gigya.global._GetElementPos=function(obj) {
//	var pos=gigya.global.getPos(obj);
//	return {left:pos.x,top:pos.y};
//}

//gigya.global.getPos=function(el) {
//	var ua = navigator.userAgent.toLowerCase();
//	var isOpera = (ua.indexOf('opera') != -1);
//	var isIE = (ua.indexOf('msie') != -1 && !isOpera); // not opera spoof
//	if(el.parentNode === null || el.style.display == 'none') {
//		return false;
//	}      
//	var parent = null;
//	var pos = [];     
//	var box;     
//	if(el.getBoundingClientRect)    //IE
//	{         
//		box = el.getBoundingClientRect();
//		var scrollTop = Math.max(document.documentElement.scrollTop, document.body.scrollTop);
//		var scrollLeft = Math.max(document.documentElement.scrollLeft, document.body.scrollLeft);
//		return {x:box.left + scrollLeft, y:box.top + scrollTop};
//	}else if(document.getBoxObjectFor)    // gecko    
//	{
//		box = document.getBoxObjectFor(el); 
//		var borderLeft = (el.style.borderLeftWidth)?parseInt(el.style.borderLeftWidth):0; 
//		var borderTop = (el.style.borderTopWidth)?parseInt(el.style.borderTopWidth):0; 
//		pos = [box.x - borderLeft, box.y - borderTop];
//	} else    // safari & opera    
//	{
//		pos = [el.offsetLeft, el.offsetTop];  
//		parent = el.offsetParent;     
//		if (parent != el) { 
//			while (parent) {  
//				pos[0] += parent.offsetLeft; 
//				pos[1] += parent.offsetTop; 
//				parent = parent.offsetParent;
//			}  
//		}   
//		if (ua.indexOf('opera') != -1 || ( ua.indexOf('safari') != -1 && el.style.position == 'absolute' )) { 
//			pos[0] -= document.body.offsetLeft;
//			pos[1] -= document.body.offsetTop;         
//		}    
//	}              
//	if (el.parentNode) { 
//		parent = el.parentNode;
//	} else {
//		parent = null;
//	}
//	while (parent && parent.tagName != 'BODY' && parent.tagName != 'HTML') { // account for any scrolled ancestors
//		pos[0] -= parent.scrollLeft;
//		pos[1] -= parent.scrollTop;
//		if (parent.parentNode) {
//			parent = parent.parentNode;
//		} else {
//			parent = null;
//		}
//	}
//	return {x:pos[0], y:pos[1]};
//}

//gigya.global.getStyle=function(obj, styleProp)
//{
//  if (obj.currentStyle)
//	return obj.currentStyle[styleProp];
//  else if (window.getComputedStyle)
//	return document.defaultView.getComputedStyle(obj,null).getPropertyValue(styleProp);
//}

//gigya.services.socialize.getStyleString=function(oStyle, blnIsLink) {
//	var s='';
//	s+='line-height: normal;';
//	if (blnIsLink) s+='cursor:pointer;';
//	if (oStyle.underline) s+='text-decoration:underline;';
//	if (oStyle.font) s+='font-family:'+oStyle.font+';';
//	if (oStyle.size) s+='font-size:'+oStyle.size+'px;';
//	if (oStyle.color) s+='color:'+oStyle.color+';';
//	if (oStyle.bold) s+='font-weight:bold;';
//	if (oStyle['frame-thickness']) s+='border-style:solid; border-width:'+oStyle['frame-thickness']+'px;';
//	if (oStyle['frame-color']) s+='border-color:'+oStyle['frame-color']+';';
//	if (oStyle['background-color']) s+='background-color:'+oStyle['background-color']+';';
//	return s;
//}

//gigya.services.socialize.GrayOut=function(vis, opt, extra) {
//	  var options = opt || {};
//	  var zindex = options.zindex || 50;
//	  var opacity = options.opacity || 70;
//	  var opaque = (opacity / 100);
//	  var bgcolor = options.bgcolor || '#000000';
//	  var dark=document.getElementById('darkenScreenObject');
//	  var tbody = document.body;
//	  if (!dark) {
//		var isCompMode=(gigya.localInfo.isMobile || gigya.localInfo.isIE6 || document.compatMode=='BackCompat');
//		var scrollTop=document.documentElement.scrollTop;
//		if (scrollTop==0) scrollTop=document.body.scrollTop;	  
//		var tnode = document.createElement('div');
//			if (isCompMode) {
//				tnode.style.position='absolute';
//				tnode.style.top=''+scrollTop+'px';
//			} else {
//				tnode.style.position='fixed';
//				tnode.style.top='0px'; 
//			}
//			tnode.style.overflow='hidden';	
//			tnode.style.left='0px';
//			tnode.style.display='none';
//			tnode.id='darkenScreenObject';
//			tnode.innerHTML='&#160;';
//		tbody.appendChild(tnode);
//		dark=document.getElementById('darkenScreenObject');
//		if (!gigya.attachedFixGray) {
//			gigya.attachedFixGray = true;		
//			if (window.addEventListener) {                
//				window.addEventListener('resize',gigya.services.socialize._fixGraySize,false);
//			}
//			if (window.attachEvent) {                
//				window.attachEvent('onresize', gigya.services.socialize._fixGraySize);
//			}
//			if (isCompMode) {
//				if (window.addEventListener) {
//					window.addEventListener('scroll',gigya.services.socialize._fixGrayPosition,false);
//				}
//				if (window.attachEvent) {
//					window.attachEvent('onscroll', gigya.services.socialize._fixGrayPosition);
//				}
//			}
//		}
//	  }
//	  if (vis) {
//		var vph;
//		var vpw;
//		if (window.innerHeight) {
//			vph=window.innerHeight;
//			vpw=window.innerWidth;
//		}
//		if (typeof vph=='undefined') {
//			var de=document.documentElement;
//			vph=de.clientHeight;
//			vpw=de.clientWidth;
//		}
//		if (vpw==0) vpw=tbody.clientWidth;   
//		if (vph==0) vph=tbody.clientHeight;   
//		dark.style.opacity=opaque;
//		dark.style.MozOpacity=opaque;
//		dark.style.width= '' + vpw + 'px';
//		dark.style.height= '' + vph + 'px';		
//		dark.style.filter='alpha(opacity='+opacity+')';
//		dark.style.zIndex=zindex;
//		dark.style.backgroundColor=bgcolor;
//		dark.style.display='block';	
//	  } else {
//		 tbody.removeChild(dark);
//	  }    
//},

//gigya.services.socialize._fixGrayPosition=function(){
//	var dark=document.getElementById('darkenScreenObject');
//	if (dark) {
//		var de=document.documentElement;
//		var db=document.body;
//		var scrollTop=de.scrollTop;
//		if (scrollTop==0) scrollTop=db.scrollTop;
//		var scrollLeft=de.scrollLeft;
//		if (scrollLeft==0) scrollLeft=db.scrollLeft;        
//		var clientHeight=de.clientHeight;
//		if (clientHeight==0) clientHeight=db.clientHeight;    
//		var clientWidth=de.clientWidth;
//		if (clientWidth==0) clientWidth=db.clientWidth;    
//		if (gigya.localInfo.isIE6) clientWidth-=1;
//		dark.style.top=scrollTop;
//		dark.style.left=scrollLeft;
//	}
//},

//gigya.services.socialize._fixGraySize=function() {
//	var dark=document.getElementById('darkenScreenObject');
//	if (dark) {
//		var de=document.documentElement;
//		var db=document.body;
//		var clientHeight=de.clientHeight;
//		if (clientHeight==0) clientHeight=db.clientHeight;    
//		var clientWidth=de.clientWidth;
//		if (clientWidth==0) clientWidth=db.clientWidth;    
//		if (gigya.localInfo.isIE6) clientWidth-=1;

//		dark.style.width=''+clientWidth+'px';
//		dark.style.height=''+clientHeight+'px';  
//	}
//},

//gigya.utils.DOM.createTopLevelDiv=function(id) {
//	var ifrel;
//	ifrel = document.createElement('IFRAME');
//	ifrel.id='gigya_ifr_'+id;
//	ifrel.frameborder="0";
//	ifrel.frameBorder="0";
//	ifrel.allowtransparency=true;
//	ifrel.style.position='absolute';
//	if (gigya.localInfo.isChrome) {
//		ifrel.style.width='30px';
//		ifrel.style.height='1px';
//	}
//	else {
//		ifrel.style.width='1px';
//		ifrel.style.height='1px';
//	}
//	if (ifrel.style.zIndex!=null) {
//		ifrel.style.zIndex=gigya.utils.DOM._nextZIndex++;
//	}

//	if (gigya.localInfo.isIE6) {
//	    ifrel.src = gigya._.getCdnResource('http://cdns.gigya.com/gs/blank.htm');
//	}

//	var el = document.createElement('div');
//	el.style.position='absolute';
//	if (el.style.zIndex!=null) {
//		el.style.zIndex=gigya.utils.DOM._nextZIndex++;
//	}
//	el.innerHTML = '';
//	if (id) el.id=id;

		
//	if (document.body) {
//		if(document.body.insertBefore) {
//			if (document.body.firstChild) {
//				if (ifrel!=null) {
//					document.body.insertBefore(ifrel, document.body.firstChild);
//				}
//				document.body.insertBefore(el, document.body.firstChild);
//			} else if (document.body.appendChild) {
//				if (ifrel!=null) {
//					document.body.appendChild(ifrel);
//				}			
//				document.body.appendChild(el);
//			}
//		} 
//	}
//	return el;
//},

//gigya.services.socialize.hideUI = function () {
//	var params = gigya.utils.object.merge([gigya.thisScript.globalConf, arguments]);
//	gigya.services.socialize.GrayOut(false);
//	for (var containerID in gigya.utils.DOM._popupContainers) {
//		var container=gigya.utils.DOM._popupContainers[containerID];
//		if (container!=null) {
//			container.innerHTML = '';
//			if (container.parentNode) container.style.display='none';
//		}

//		var ifrel=document.getElementById('gigya_ifr_'+containerID);
//		if ( null!=ifrel ) { 
//			//ifrel.parentNode.removeChild(ifrel);
//			ifrel.style.display = 'none';     
//		}
//	}
//	var fbRoot=document.getElementById('fb-root');
//	if (fbRoot) fbRoot.style.visibility='hidden';
//	gigya.events.global.dispatch({eventName: 'HideUIRequested'});
//	if (typeof params.callback == 'function') {
//		var oResponse = {
//			status: 'OK',
//			statusMessage: '',
//			operation: 'hideUI',
//			context: params.context,
//			errorMessage: '',
//			errorCode: 0
//		};
//		params.callback(oResponse);
//	}
//}

//gigya.global.isEmail=function(s){
//	if (s.indexOf(' ')>=0) return false;
		
//	var emailParts=s.split('@');
//	if (emailParts.length!=2) return false;
//	if (emailParts[0].length==0) return false;
//	if (emailParts[1].length==0) return false;		
//	var domainParts=emailParts[1].split('.');
//	if (domainParts.length<2) return false;
//	for (var d=0;d<domainParts.length;d++){
//		if (domainParts[d].length==0 || domainParts[d].indexOf(' ')>0) return false;
//	}
//	return true;
//}

//gigya.socialize._AddJSRequest = function (servicedBy, methodName, needsContainer, params) {
//    if (!document.body) {
//        var args = arguments;
//        window.setTimeout(function () { gigya.socialize._AddJSRequest.apply(this, args) }, 300);
//        return;
//    }
//    if (!params) params = {};
//    gigya.log.logCall(methodName.split('.').pop(), params);
//    var containerID = params.containerID;
		
//    var blnCenter = false;
//    var container;
//    if (!needsContainer) { //no container
//        if (gigya.utils.DOM._pseudoContainers[containerID] == null) gigya.utils.DOM._pseudoContainers[containerID] = {};
//        container = gigya.utils.DOM._pseudoContainers[containerID]; //so it will be able to hold .Reqs on it.
//    } else if (typeof containerID == 'undefined' || containerID == '') { //popup
//        containerID = gigya.utils.DOM.getCenteredDivID(methodName);
//        params.containerID = containerID;
//        blnCenter = true;
//        params.isPopup = true;
//        container = document.getElementById(containerID);
//        if (container == null) container = gigya.utils.DOM.createTopLevelDiv(containerID);
//    }
//    else { //has container
//        container = document.getElementById(containerID);
//    }

//    if (!container) {
//        gigya.events.dispatchInvalidParamError(params, 'containerID');
//        return;
//    }
	
//    var localMethodName = methodName.split('.').pop();
//    if (container.setAttribute) container.setAttribute('gigid', (params.source && params.source != localMethodName ? params.source + '_' : '') + localMethodName);

//    if (container.style) {
//        container.style.display = '';
//        container.style.visibility = '';
//        container.innerHTML = '';
//        var ifrel = document.getElementById('gigya_ifr_' + containerID);
//        if (ifrel) {
//            ifrel.style.display = '';
//            ifrel.style.visibility = '';
//        }

//        var fncFixPosition = function () {
//            if (ifrel != null) {
//                gigya.utils.DOM.setSize(ifrel, params.width, params.height, blnCenter);
//                /*if (gigya.localInfo.isIE) {*/ifrel.style.visibility = 'visible'; /*}*/
//            }
//            gigya.utils.DOM.setSize(container, params.width, params.height, blnCenter);
//        };

//        fncFixPosition();
//    }

//    var Reqs = (container.Reqs ? container.Reqs : (container.Reqs = []));
//    var rid = Reqs.length + '@' + servicedBy + '@' + containerID;
//    params.rid = rid;
//    var operation = methodName;
//    if (params._operation) operation = params._operation;
//    var req = Reqs[Reqs.length] = {
//        rid: rid,
//        servicedBy: servicedBy,
//        container: container,
//        method: methodName,
//        context: params['context'],
//        c: params,
//        p: params,
//        i: params,
//        operation: operation,
//        isHTML: true
//    };


//    var methodNameParts = methodName.split('.');
//    var node = eval(methodNameParts[0]);
//    var pfxi = 1;
//    while (null != (node = node[methodNameParts[pfxi]]) && (++pfxi < methodNameParts.length)) {
//        //alert(pfxi);
//    };
//    var func = node;
//    gigya.utils.script.load(gigya._.getCdnResource('/js/' + servicedBy + '.min.js' + (params['lang'] ? '?lang=' + params['lang'] : '')), null, function () {
//        gigya.reports.reportLoad(methodName, params);
//        gigya.utils.functions.callFunction(methodName, [params, params, params]);
//    }, true);
//    return containerID;
//}

//gigya.utils.object.add(gigya.utils.DOM, {
//    enableSafeCopy: function (el) {
//        if (el.addedSafeCopy) return;
//        gigya.utils.DOM.addEventListener(el, 'copy', function (e) { //to let users copy URLs without soft hyphens
//            e = e || window.event;
//            var target = e.target || e.srcElement;
//            var nodename = target && target.nodeName ? target.nodeName.toLowerCase() : '';
//            if (nodename == 'textarea' || nodename == 'input') return;
//            var divCopy = document.createElement('div');
//            divCopy.style.color = window.getComputedStyle ? window.getComputedStyle(document.body).backgroundColor : '#FFFFFF';
//            divCopy.style.fontSize = '0px';
//            gigya.utils.DOM.appendToBody(divCopy);
//            var shyRegex = new RegExp("(&shy;|" + String.fromCharCode(173) + ")", "g");
//            if (window.getSelection) {
//                e.stopPropagation();
//                selection = window.getSelection();
//                range = selection.getRangeAt(0);
//                divCopy.appendChild(range.cloneContents());
//                divCopy.innerHTML = divCopy.innerHTML.replace(shyRegex, ''); //it's the soft hyphen char
//                selection.selectAllChildren(divCopy);
//                fnRestore = function () {
//                    divCopy.parentNode.removeChild(divCopy);
//                    selection.removeAllRanges();
//                    selection.addRange(range);
//                };
//            } else {
//                e.cancelBubble = true;
//                selection = window.document.selection;
//                range = selection.createRange();
//                divCopy.innerHTML = range.htmlText;
//                divCopy.innerHTML = divCopy.innerHTML.replace(shyRegex, ''); //it's the soft hyphen char
//                range2 = document.body.createTextRange();
//                range2.moveToElementText(divCopy);
//                range2.select();
//                fnRestore = function () {
//                    divCopy.parentNode.removeChild(divCopy);
//                    if (range.text !== "") {
//                        range.select();
//                    }
//                };
//            }
//            window.setTimeout(fnRestore, 0);
//        });
//        el.addedSafeCopy = true;
//    }
//});

    //gigya.utils.object.add(gigya.pluginUtils.lang, {
    //    getDateString: function (widgetParams, date) {
    //        if (!widgetParams || !widgetParams['dateFormat']) return;
    //        var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    //        var dateFormat = widgetParams['dateFormat'];
    //        var d = date.getDate();
    //        var m = date.getMonth() + 1
    //        var monthName = monthNames[date.getMonth()];
    //        var y = date.getFullYear();

    //        return dateFormat
    //            .replace(/%dd/g, ('0' + d).slice(-2))
    //            .replace(/%d/g, d)
    //            .replace(/%MMMM/g, monthName)
    //            .replace(/%MMM/g, monthName.substr(0, 3))
    //            .replace(/%MM/g, ('0' + m).slice(-2))
    //            .replace(/%M/g, m)
    //            .replace(/%yyyy/g, y)
    //            .replace(/%yy/g, y.toString().slice(-2))
    //    }
    //})


//gigya.global.XMLUtils module
//gigya.global.XMLUtils = {
//    CreateXMLFromString: function (sXML) {
//        var doc = null;
//        if (window.ActiveXObject) {
//            doc = new ActiveXObject("Microsoft.XMLDOM");
//            doc.async = "false";
//            doc.loadXML(sXML);
//        }
//        else {
//            var parser = new DOMParser();
//            doc = parser.parseFromString(sXML, "text/xml");
//        }

//        return doc;
//    },

//    CopyProperties: function (propBag, targetBag, pfx, Proccesor) {
//        if (typeof pfx == 'undefined') pfx = '';
//        if (typeof targetBag == 'undefined') targetBag = {};
//        if (Proccesor != null) {
//            for (var propName1 in propBag) {
//                targetBag[pfx + propName1] = Proccesor(propName1, propBag[propName1]);
//            }
//        }
//        else {
//            for (var propName2 in propBag) {
//                targetBag[pfx + propName2] = propBag[propName2];
//            }
//        }
//        return targetBag;
//    },

//    CollectAttributesFromXMLPathToObject: function (xmlobjResource, ResourceNodePathSegments, res, blnCollectFromAncestors, processor) {
//        for (var iPathSegment = 0; ((iPathSegment < ResourceNodePathSegments.length) && (typeof xmlobjResource != 'undefined')) ; iPathSegment++) {
//            xmlobjResource = xmlobjResource.childNodes[ResourceNodePathSegments[iPathSegment]];
//            if (xmlobjResource != null && blnCollectFromAncestors) {
//                this.CopyProperties(xmlobjResource.attributes, res, '', processor);
//            }
//        }

//        if (typeof xmlobjResource != 'undefined') {
//            this.CopyProperties(xmlobjResource.attributes, res, '', processor)
//        }
//    },

//    mergeNodes: function (srcNode, targetObject) {

//        if (typeof targetObject.attributes == 'undefined') targetObject.attributes = {};
//        if (typeof targetObject.childNodes == 'undefined') targetObject.childNodes = {};

//        var targetAttributes = targetObject.attributes;
//        var srcNodeAttributes = srcNode.attributes;
//        /* for (var attName in srcNodeAttributes) {
//		targetAttributes[attName] = srcNodeAttributes[attName];
//		}*/

//        if (srcNodeAttributes) {
//            //this.CopyAttributesToObject(srcNodeAttributes, targetAttributes, '');
//            for (var i = 0; i < srcNodeAttributes.length; i++) {
//                if (srcNodeAttributes[i].value != null) {
//                    targetAttributes[srcNodeAttributes[i].nodeName] = srcNodeAttributes[i].value;
//                } else {
//                    targetAttributes[srcNodeAttributes[i].nodeName] = srcNodeAttributes[i].nodeValue;
//                }
//            }
//        }
//        var targetChildNode;
//        for (var u = 0; u < srcNode.childNodes.length; u++) { //add missing child nodes
//            var srcChildNode = srcNode.childNodes[u];
//            targetChildNode = targetObject.childNodes[srcChildNode.nodeName];
//            if (typeof targetChildNode == 'undefined') {
//                targetChildNode = targetObject.childNodes[srcChildNode.nodeName] = { attributes: {}, childNodes: {} };
//            }
//            this.mergeNodes(srcChildNode, targetChildNode);
//        }
//        return targetObject;
//    }
//}

//gigya.global.resolver = function (proccessor) {
//    this.configurations = [];
//    this.mergedConfig = {};
//    this.isMerged = true;
//    this._processor = proccessor;
//    this.merge = function () {
//        if (!this.isMerged) {
//            this.mergedConfig = { attributes: {}, childNodes: {} };
//            for (var i = 0; i < this.configurations.length; i++) {
//                var xmlDoc = this.configurations[i];
//                gigya.global.XMLUtils.mergeNodes(xmlDoc, this.mergedConfig);
//            }
//            this.isMerged = true;
//        }
//    }
//    this.Resolve = function () {
//        if (this.merge) this.merge();
//        var res = {};
//        if (typeof arguments[arguments.length - 1] == 'object') {
//            res = arguments[arguments.length - 1];
//        }
//        for (var u = 0; u < arguments.length; u++) {
//            var arg = arguments[u];
//            var ctor = arg.constructor;
//            if (ctor == String) {

//                var arPaths = arg.split('|');
//                for (var i = arPaths.length - 1; i >= 0; i--) {
//                    var path = this.trimCharsAtSuffix(this.trimCharsAtPrefix(arPaths[i], ' \n\r\t'), ' \n\r\t');
//                    var recur = (path.charAt(0) == '+');
//                    var arPath = path.split('/').slice(1);
//                    if (recur) {
//                        path = path.substring(1);
//                    }
				
//                    gigya.global.XMLUtils.CollectAttributesFromXMLPathToObject(this.mergedConfig, arPath, res, recur, this._processor);
//                }
//            } else if (ctor == Array) {
//                for (var n = 0; n < arg.length; n++) {
//                    Resolve(arg[n], res);
//                }
//            }
//        }
//        return res;
//    }
					
//    this.trimCharsAtPrefix = function (s, chars) {
//        var idx = 0;
//        if ((typeof s == 'undefined') || (s.length == 0)) return '';
//        var sl = s.length;
//        while ((idx <= sl) && (chars.indexOf(s.charAt(idx)) > -1)) {
//            idx++;
//        }
//        return s.substring(idx, sl);
//    }
//    this.trimCharsAtSuffix = function (s, chars) {
//        if ((typeof s == 'undefined') || (s.length == 0)) return '';
//        var idx = s.length - 1;
//        while ((idx >= 0) && (chars.indexOf(s.charAt(idx)) > -1)) {
//            idx--;
//        }
//        return s.substring(0, idx + 1);
	
//    }
//    this.addConfig = function (sXML) {
//        if (typeof sXML == 'string') {
//            this.configurations.push(gigya.global.XMLUtils.CreateXMLFromString(sXML));
//            this.isMerged = false;
//        }
//    }
//    this.getTextFromKey = function (textKey) {
//        return this.Resolve('/config/lang/' + textKey)['text'];
//    }
//}
		
//gigya.global.resolver.defaultProccessor = function (PropertyName, PropertyValue) {
//    var lcasePropertyName = PropertyName.toLowerCase();
//    var lastDashIndex = lcasePropertyName.indexOf('-');
//    if (lastDashIndex != -1) {
//        lcasePropertyName = lcasePropertyName.substring(lastDashIndex + 1, lcasePropertyName.length);
//    }
//    switch (lcasePropertyName) {
//        case 'bold':
//        case 'italic':
//        case 'underline':
//            return PropertyValue.toString().toLowerCase() == 'true';
//            break;
//        case 'width':
//        case 'height':
//            return PropertyValue.replace('%', '');
//            break;
//        case 'size':
//            return parseInt(PropertyValue);
//            break;
//        default:
//            return PropertyValue;
//    }
//}

//gigya.global.getSpriteRenderers = function (oGroups) {
//    //group: {path,w,h}
//    var arPaths = [];
//    var x = 0;
//    var renderers = {};
//    for (var groupID in oGroups) {
//        var group = oGroups[groupID];
//        if (!group.pixelRatio) group.pixelRatio = 1;
//        arPaths.push(group.path + '|' + group.w * group.pixelRatio + ',' + group.h * group.pixelRatio);
//    }
//    var src = gigya._.getCdnResource('/gs/GetSprite.ashx?path=' + encodeURIComponent(arPaths.join('^').replace(/\[\]/, '')));
//    for (var groupID in oGroups) {
//        var group = oGroups[groupID];
//        var spriteGroupData = {};
//        spriteGroupData.spriteData = {};

//        var regexp = /\[(.*?)\]/g;
//        var arMatches = [];
//        var iMatch = 0;
//        spriteGroupData.srcTemplate = gigya._.getCdnResource('/gs/i' + group.path.replace(regexp, function (_, placeholder) {
//            arMatches.push(placeholder.split(','));
//            return '{' + (iMatch++) + '}';
//        }));
//        var arSpriteIDs = gigya.global.getCombination(arMatches);
//        if (arSpriteIDs.length == 0) arSpriteIDs.push(['']);

//        for (var u = 0; u < arSpriteIDs.length; u++) {
//            var id = arSpriteIDs[u].join('-');
//            if (id == '') {
//                id = 'default';
//            }
//            spriteGroupData.spriteData[id] = {
//                groupId: groupID,
//                id: id,
//                x: x,
//                w: group.w,
//                h: group.h
//            };
//            x += group.w;
//        }
//        renderers[groupID] = new gigya.global.SpriteRenderer(src, spriteGroupData.srcTemplate, spriteGroupData.spriteData, group.w, group.h, group.pixelRatio);
//    }
//    return renderers;
//}

//gigya.global.SpriteRenderer = function (src, srcTemplate, spriteData, spriteW, spriteH, pixelRatio) {
//    this.src = src;
//    this.srcTemplate = srcTemplate;
//    this.spriteData = spriteData;
//    this.spriteW = spriteW;
//    this.spriteH = spriteH;
//    this.pixelRatio = pixelRatio;
//}

//gigya.global.SpriteRenderer.prototype = {
//    getSpriteData: function (id, dontUseSprites) {
//        var oSprite = this.spriteData[id];
//        if (oSprite && !dontUseSprites) {
//            return this.spriteData[id];
//        } else {
//            var i = 0;
//            var params = id.split('-');
//            var regexp = /\{(.*?)\}/g;
//            var src = this.srcTemplate.replace(regexp, function (_, group) {
//                var param = params[i++];
//                if (param) {
//                    return param;
//                } else {
//                    return '';
//                }
//            })

//            return {
//                isSingleImage: true,
//                src: src
//            }
//        }
//    },

//    getStyleString: function (spriteId, autoMargin) {
//        var oSprite = this.getSpriteData(spriteId);
//        var arStyle = [];

//        if (oSprite) {
//            arStyle.push("background-image:url('" + this.src + "');");
//            arStyle.push("background-position:-" + oSprite.x + "px 0px;");

//            if (this.pixelRatio > 1) {
//                arStyle.push("background-size:auto " + this.spriteH + "px;");
//            }

//            if (autoMargin) {
//                arStyle.push("margin: 0 auto;");
//            }
//            arStyle.push("width: " + this.spriteW + "px;");
//            arStyle.push("height: " + this.spriteH + "px;line-height: " + this.spriteH + "px;");
//            arStyle.push("background-repeat:no-repeat;");
//            //arStyle.push("font-size:1px;");
//            arStyle.push("position:static;");
//        }
//        return arStyle.join('');
//    },

//    getHTML: function (spriteId, autoMargin, dontUseSprites, elID) {


//        /* ----------------------------------------------------------------------------------------*/
//        // see bug: 33448
//        var disableSpriteOnChrome = ((this.pixelRatio > 1) && gigya.localInfo.isChrome);
//        dontUseSprites = dontUseSprites || disableSpriteOnChrome;
//        /* ----------------------------------------------------------------------------------------*/

//        var oSprite = this.getSpriteData(spriteId, dontUseSprites);
//        var arHTML = [];
//        if (oSprite) {
//            if (oSprite.isSingleImage) {
//                arHTML.push('<img src="' + oSprite.src + '" style="width:' + this.spriteW + 'px;height:' + this.spriteH + 'px;position:static;margin:0" alt="" />');
//            } else {
//                arHTML.push('<div style="');
//                arHTML.push(this.getStyleString(spriteId, autoMargin));
//                arHTML.push('"');
//                if (elID) arHTML.push(' id="' + elID + '"');
//                arHTML.push('></div>');
//            }
//        }
//        return arHTML.join('');
//    }
//}
//gigya.global.getCombination = function (ar, index) {
//    if (!index) index = 0;
//    if (!ar || ar.length == 0) return [];
//    var currentArray = ar[index];
//    if (index == ar.length - 1) {
//        var arCombinations = [];
//        for (var i = 0; i < currentArray.length; i++) {
//            arCombinations.push([currentArray[i]]);
//        }
//        return arCombinations;
//    } else {
//        var arCombinations = [];
//        var arCombination = gigya.global.getCombination(ar, index + 1)
//        if (currentArray) {
//            for (var i = 0; i < currentArray.length; i++) {
//                for (var u = 0; u < arCombination.length; u++) {
//                    arCombinations.push([currentArray[i]].concat(arCombination[u]));
//                }
//            }
//        }
//        return arCombinations;
//    }
//}

//gigya.global.resolveProviders = function (enabledProviders, disabledProviders, requiredCapabilities) {

//    // setting the providers array.
//    // setting the array of the providers:
//    var eProviders = gigya.utils.array.getArrayFromString(enabledProviders, ',', true);
//    var dProviders = gigya.utils.array.getArrayFromString(disabledProviders, ',', true);
//    var rCapabilities = gigya.utils.array.getArrayFromString(requiredCapabilities, ',', true);

//    // removing the disabled providers.
//    for (var i = 0; i < dProviders.length; i++) {
//        gigya.utils.array.removeByValue(eProviders, dProviders[i]);
//    }

//    // converting the providrs string into the providers Object from the array of providers in the internal '_' sdk.
//    var providers = gigya._.providers.getProvidersByName(eProviders.join(','));

//    // removing the disabled providers.
//    for (var i = 0; i < dProviders.length; i++) {
//        gigya.utils.array.removeByProperty(providers, 'name', dProviders[i]);
//    }

//    // only providers that support the capabilities will remain.
//    return gigya.socialize.getProvidersForRequiredCapabilities(providers, rCapabilities);
//}