/// <reference path="gigya.services.socialize.plugins-1.js"/*tpa=http://cdns.gigya.com/js/gigya.services.socialize.plugins.js*/ />
(function () {



    if (typeof gigya.services.socialize.plugins == 'undefined') gigya.services.socialize.plugins = {};
    var _pi = gigya.services.socialize.plugins;
    var privateScope = {
        get: function (url, onLoad) {
            var script = document.createElement('script');
            script.src = url;
            script.charset = 'UTF-8'
            script.onreadystatechange = function (args) {
                if (script.readyState == 'loaded' || script.readyState == 'complete') {
                    script.onreadystatechange = null;
                    if (onLoad) onLoad();
                }
            }
            script.onload = script.onerror = function () {
                script.onload = null;
                if (onLoad) onLoad();
            }
            var head = document.getElementsByTagName('head')[0];
            (head || document.body).appendChild(script);
        },
        getCacheKey: function (provider, url) {
            return 'getProviderShareCount_' + provider.toLowerCase() + '_' + encodeURIComponent(url);
        },
        getProviderResponse: function (provider, url, callback, cacheTimeout) {
            provider = provider.toLowerCase();
            gigya.utils.sessionCache.get(privateScope.getCacheKey(provider, url), cacheTimeout, function (response) {
                if (response != null) {
                    callback(response, provider, false, true, url);
                } else {
                    privateScope.getProviderShareCount(provider, url, callback);
                }
            });
        },
        getProviderShareCount: function (provider, url, callback) {
            var jsonpURL;
            provider = provider.toLowerCase();
            var callbackName = 'gig_pc_' + provider + '_' + (new Date()).getTime() + '_' + Math.random().toString().split('.')[1];
            var countField;
            var encodedURL = encodeURIComponent(url);
            switch (provider) {

                case 'vkontakte':
                    jsonpURL = 'https://vk.com/share.php?act=count&index=1&url=' + encodedURL;
                    break;

                case 'facebook':
                    //jsonpURL = 'https://graph.facebook.com/?ids=' + encodedURL + '&callback=' + callbackName;
                    jsonpURL = 'https://graph.facebook.com/fql?q=SELECT%20total_count%20FROM%20link_stat%20WHERE%20url=%22' + encodedURL + '%22&callback=' + callbackName;
                    countField = '.data[0].total_count';
                    break;
                case 'twitter':
                    jsonpURL = 'http://cdn.api.twitter.com/1/urls/count.json?url=' + encodedURL + '&callback=' + callbackName;
                    if (document.location.protocol == 'https:') jsonpURL = jsonpURL.replace('http:', 'https:');
                    countField = '.count';
                    break;
                case 'delicious':
                    jsonpURL = 'http://feeds.delicious.com/v2/json/urlinfo/data?url=' + encodedURL + '&callback=' + callbackName;
                    countField = '[0].total_posts';
                    break;
                case 'stumbleupon':
                    jsonpURL = 'http://www.stumbleupon.com/services/1.1/badge.getinfo?url=' + encodedURL + '&format=jsonp&callback=' + callbackName;
                    countField = '.result.views';
                    break;
                case 'pinterest':
                    jsonpURL = 'http://api.pinterest.com/v1/urls/count.json?url=' + encodedURL + '&callback=' + callbackName;
                    countField = '.count';
                    break;
                case 'linkedin':
                    jsonpURL = 'https://www.linkedin.com/countserv/count/share?format=jsonp&url=' + encodedURL + '&callback=' + callbackName;
                    countField = '.count';
                    break;
            }
            if (document.location.protocol == 'https:' && jsonpURL.indexOf('http:') == 0) jsonpURL = null;
            if (jsonpURL) {

                window[callbackName] = function (res) {

                    var count;
                    try {
                        count = eval('res' + countField);
                    } catch (ex) { }

                    if (!count || isNaN(count)) count = 0;
                    callback(count, provider, null, null, url);
                    window[callbackName] = null

                }

                if (provider == 'vkontakte') {
                    window.VK = window.VK || {};
                    window.VK.Share = window.VK.Share || {};

                    var origCount = window.VK.Share.count;
                    window.VK.Share.count = function (idx, count) {

                        callback(count, provider, null, null, url);
                        // prevent the overide for the count
                        // by the generic callback.
                        window[callbackName] = null;

                        // revert to the original
                        if (origCount) {
                            window.VK.Share.count = origCount;
                        }
                    }
                }

                this.get(jsonpURL, function () {
                    if (window[callbackName]) {
                        callback(0, provider, true, null, url);
                        window[callbackName] = null;
                    }
                });
            } else {
                callback(0, provider, null, null, url);
            }
        }
    }

    _pi.shareCounts = {
        getProviderShareCounts: function (p) {

            var providers = [{ name: 'facebook' }, { name: 'twitter' }, { name: 'delicious' }, { name: 'stumbleUpon' }, { name: 'pinterest' }, { name: 'linkedin' }, { name: 'vkontakte' }];

            var enabledProviders = p['enabledProviders'] ? p['enabledProviders'] : '*';
            var disabledProviders = p['disabledProviders'] ? p['disabledProviders'] : '';

            var arProviders = gigya.services.socialize.getProvidersByName(enabledProviders, providers);
            arProviders = gigya.services.socialize.hideProvidersByName(arProviders, disabledProviders, providers);

            var doneProviders = 0;
            var response = {
                errorCode: 0,
                errorMessage: '',
                operation: 'getProviderShareCounts',
                context: p['context'],
                shareCounts: {}
            }

            var cacheTimeout = p.cacheTimeout;
            for (var i = 0; i < arProviders.length; i++) {
                var url = p[arProviders[i].name.toLowerCase() + 'URL'];
                if (!url) url = p['URL'];
                if (!url) {
                    var metas = document.getElementsByTagName('meta');
                    for (var u = 0; u < metas.length; u++) {
                        if (metas[u].getAttribute('property') == 'og:url' && metas[u].getAttribute('content')) {
                            url = metas[u].getAttribute('content');
                            break;
                        }
                    }
                }
                if (!url) url = document.location.href;

                var fnOnResponse = function (count, provider, failed, cached, url) {
                    if (!cached) gigya.utils.sessionCache.set(privateScope.getCacheKey(provider, url), count);

                    response.shareCounts[provider] = parseInt(count);
                    doneProviders++;
                    if (doneProviders == arProviders.length) {
                        if (p['callback']) p['callback'](response);
                    }
                }
                privateScope.getProviderResponse(arProviders[i].name, url, fnOnResponse, cacheTimeout);
            }
        }
    }

})();