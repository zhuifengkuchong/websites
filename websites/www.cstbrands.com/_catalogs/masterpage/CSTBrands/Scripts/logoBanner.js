$(function()
{
	var numToDisplay = 6;
	var timeToDisplay = 3000; //5 seconds
	var fadeOutSpeed = 1000; //2 seconds
	var fadeInSpeed = 1000; //1 second
	var logoObj = [];
	var curItems = [];
	var nextItems = [];

	var requestUri = _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/GetByTitle('Logo Banner')/items";
		//trasmit GET request to SP
		jqhxr = $.ajax({
        url: requestUri,
        type: 'GET',
        dataType: 'json',
        success: onSuccess,
        error: onFail,
        beforeSend: setHeader
    });
	
	function onFail(errorObject, errorMessage) {
   		alert("Error: " + errorMessage + " - Status: " + errorObject.status.toString() + ", " + errorObject.statusText);
	}
	
	function onSuccess(data){
		mainFunc(data);
	}
	
	function mainFunc(data)
	{
		$.each(data.d.results, function() {
			logoObj.push({ image: this.Logo.Url, url: this.URL, isLink:this.IsLink, id: ("cst" + this.GUID)});
		});
		
		fisherYates(logoObj);
		
		if(logoObj.length > numToDisplay)
		{
			curItems = logoObj.slice(0,numToDisplay);
			nextItems = logoObj.slice(numToDisplay, logoObj.length);
			
			buildImageList(curItems);
			
			//execute swap based on timeToDisplay
			//replace div id and div html
		setInterval(function() {
			$("#" + curItems[0].id).fadeOut(fadeOutSpeed,function()
			{
				$(this).attr("id", nextItems[0].id);
				
				if(nextItems[0].isLink)
				{	
					$(this).html("<a href='" + nextItems[0].url + "' target='_blank'><img src='" + nextItems[0].image + "'/></a>").fadeIn(fadeInSpeed);
				}
				else
				{
					$(this).html("<img src='" + nextItems[0].image + "'/>").fadeIn(fadeInSpeed);
				}
				
				nextItems.push(curItems[0]);
				curItems.shift();
				curItems.push(nextItems[0]);
				nextItems.shift();				
			});		
		}, timeToDisplay);
		}
		else
		{
			buildImageList(logoObj);
		}
	}
	
	function buildImageList(arr)
	{
		$('#cstbrands_brands').append("<ul id='cstbrands_brandslist' class='horz-list'></ul>");
		var tmpArr = arr.slice(0);
		fisherYates(tmpArr);
		
		for (i = 0; i < tmpArr.length; i++) {
			if(arr[i].isLink)
			{
				$("#cstbrands_brandslist").append("<li><div id='" + tmpArr[i].id + "'><a href='" + tmpArr[i].url + "' target='_blank'><img src='" + tmpArr[i].image + "'/></a></div></li>");
			}
			else
			{
				$("#cstbrands_brandslist").append("<li><div id='" + tmpArr[i].id + "'><img src='" + tmpArr[i].image + "'/></div></li>");
			}
		}
	}
	
	function fisherYates ( myArray ) {
	  var i = myArray.length, j, tempi, tempj;
	  if ( i == 0 ) return false;
	  while ( --i ) {
		 j = Math.floor( Math.random() * ( i + 1 ) );
		 tempi = myArray[i];
		 tempj = myArray[j];
		 myArray[i] = tempj;
		 myArray[j] = tempi;
	   }
	}
	
	function setHeader(xhr) {
		 xhr.setRequestHeader("Accept", "application/json; odata=verbose");
	}
});