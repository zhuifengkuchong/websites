$(document).ready(function () {

	var animationFrame = $('#slider li p.cta').parent('li');

	animationFrame.addClass('animated');

	var headerMain = animationFrame.find('h1');
	var imageMain = animationFrame.find('img');
	var headerSub = animationFrame.find('h1 em');
	var textMain = animationFrame.find('h1 + p');
	var textCTA = animationFrame.find('http://www.cliffsnaturalresources.com/Style Library/cliffs/js/p.cta');

	function resetSlide() {
		headerMain.removeAttr("style");
		imageMain.removeAttr("style");
		textMain.removeAttr("style");
		headerSub.removeAttr("style");

		textCTA.delay(6000).animate({
			top: '-600px'
		}, {
			duration: 1500,
			easing: 'easeOutQuad',
			queue: true,
			complete: function () {
				animateSlide();
			}
		});
	}

	function animateSlide() {
		imageMain.delay(1000).animate({
			left: '0px'
		}, {
			duration: 8000,
			easing: 'linear',
			queue: false
		});
		headerMain.delay(1000).animate({
			right: '-48px'
		}, {
			duration: 1500,
			easing: 'easeOutQuad',
			queue: true,
			complete: function () {
				textMain.delay(3000).animate({
					top: '0px'
				}, {
					duration: 1500,
					easing: 'easeOutQuad',
					queue: true,
					complete: function () {
						textCTA.delay(3000).animate({
							top: '0px'
						}, {
							duration: 1500,
							easing: 'easeOutQuad',
							queue: true,
							complete: function () {
								$('#slider').data('AnythingSlider').startStop(true);
								resetSlide();
							}
						});
					}
				});
			}
		});

		headerSub.delay(3000).animate({
			right: '0px'
		}, {
			duration: 1500,
			easing: 'easeOutQuad',
			queue: true
		});
	}

	animateSlide();


});