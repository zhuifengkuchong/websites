//Global Menu Navigation 
var PrevSelectedMenu = "";
var FadeorSlide= "";
		$('#home').hoverIntent(
		function () {
			HideMenu();
		},
		function () { }
        );
        $('#why-dish').hoverIntent(
            function () {
                ShowMenu("#why-dish-subnav");
            },
            function () { }
        );
            $('#entertainment').hoverIntent(
            function () {
                ShowMenu("#entertainment-subnav");
            },
            function () { }
        );
            $('#technology').hoverIntent(
            function () {
                ShowMenu("#technology-subnav");
            },
            function () { }
        );
            $('#support').hoverIntent(
            function () {
                ShowMenu("#support-subnav");
            },
            function () { }
        );
            $('#special').hoverIntent(
            function () {
                HideMenu();
            },
            function () { }
        );
            $('#search').hoverIntent(
            function () {
                ShowMenu("#search-subnav");
            },
            function () { }
        );
			$('#my-account').hoverIntent(
            function () {
                ShowMenu("#my-account-subnav");
            },
            function () { }
        );
			$('#perks').hoverIntent(
            function () {
                ShowMenu("#perks-subnav");
            },
            function () { }
        );
			$('#upgrades').hoverIntent(
            function () {
                ShowMenu("#upgrades-subnav");
            },
            function () { }
        );
			$('#ppv').hoverIntent(
            function () {
                ShowMenu("#ppv-subnav");
            },
            function () { }
        );
            $('#main-nav-wrap').hoverIntent(
            function () { },
            function () {
                HideMenu();
            }
        );

function ShowMenu(selectedMenu) {
    var menu1 = "#why-dish-subnav";
    var menu2 = "#entertainment-subnav";
    var menu3 = "#technology-subnav";
    var menu4 = "#support-subnav";
    var menu5 = "#search-subnav";
	var menu6 = "#my-account-subnav";
	var menu7 = "#perks-subnav";
	var menu8 = "#upgrades-subnav";
	var menu9 = "#ppv-subnav";

    if (selectedMenu != menu1) { $(menu1).hide(); $(menu1).slideUp('slow'); }
    if (selectedMenu != menu2) { $(menu2).hide(); $(menu2).slideUp('slow'); }
    if (selectedMenu != menu3) { $(menu3).hide(); $(menu3).slideUp('slow'); }
    if (selectedMenu != menu4) { $(menu4).hide(); $(menu4).slideUp('slow'); }
    if (selectedMenu != menu5) { $(menu5).hide(); $(menu5).slideUp('slow'); }
	if (selectedMenu != menu6) { $(menu6).hide(); $(menu6).slideUp('slow'); }
	if (selectedMenu != menu7) { $(menu7).hide(); $(menu7).slideUp('slow'); }
	if (selectedMenu != menu8) { $(menu8).hide(); $(menu8).slideUp('slow'); }
	if (selectedMenu != menu9) { $(menu9).hide(); $(menu9).slideUp('slow'); }

    $(selectedMenu).height(selectedMenu);
	if ( $.browser.msie)
	{	   
	    if (FadeorSlide == "fade") {
	        $(selectedMenu).show();
	    }
	    else {
	        $(selectedMenu).stop(true, true).slideDown();
	        FadeorSlide = "fade";
	    }
	} 
    else {    
        if (FadeorSlide == "fade") {
                $(selectedMenu).fadeIn(350);         
	        }
	        else {
	            $(selectedMenu).stop(true, true).slideDown();
	            FadeorSlide = "fade";
	        }
	}
    PrevSelectedMenu = selectedMenu;
}
function HideMenu() {
    FadeorSlide = "";
    $(PrevSelectedMenu).slideUp('slow');
}

// Stop hover animation on menu anchors
$('#main-subnav ul li a').click(function (e) {
    $("#" + e.target.parentNode.parentNode.parentNode.parentNode.id).css('display', 'none');
});
//End of Global Menu Navigation 

//IMPORTANT for IE8
//This prototype is provided by the Mozilla foundation and
//is distributed under the MIT license.
//http://www.ibiblio.org/pub/Linux/LICENSES/mit.license

if (!Array.prototype.forEach) {
    Array.prototype.forEach = function (fun /*, thisp*/) {
        var len = this.length;
        if (typeof fun != "function")
            throw new TypeError();

        var thisp = arguments[1];
        for (var i = 0; i < len; i++) {
            if (i in this)
                fun.call(thisp, this[i], i, this);
        }
    };
}
//End IMPORTANT for IE8


var host = "../index.htm"/*tpa=http://www.dish.com/*/;
//if (location.hostname == "http://www.dish.com/js/www.dish.com") { host = "../index.htm"/*tpa=http://www.dish.com/*/; }

//google search appliance
function validate() {
    dcsMultiTrack('DCS.dcsuri', '/vpv/navigation/search-subnav/search');
    var searchField = document.getElementById("search-subnav-search-box");
    if (searchField.value == "Search Examples: HBO, DVR, HD, Program My Remote, TV Everywhere" || searchField.value == "") {

        var theForm = document.getElementById("siteSearch");
        theForm.action = host+"/gsearch/search_adv.aspx";
        theForm.submit();
    }
    else {
        var theForm = document.getElementById("siteSearch");

        theForm.action = host+"/gsearch/";
        theForm.submit();
    }

}
//END google search appliance

//DROPDOWN-SELECTION BOX
$('.selected-box span').live("click", function () {
    $('#' + $(this).parents().get(1).id + ' .select-box').css('display', 'block'),
		$('.select-box').bind("mouseleave", function () {
		    $('.select-box').css('display', 'none')
		});
    $('.select-box .selection').live("click", function () {
        thisBoxID = $(this).parents().get(2).id;
        $('#' + thisBoxID + ' .selected-box .selection').detach();
        $(this).clone().appendTo('#' + thisBoxID + ' .selected-box');
        //"Colorized" selected states
        if ($(this).hasClass("selection-red")) {
            $('#' + thisBoxID + ' .selected-box').removeClass().addClass("selected-box button red-button");
        }
        else if ($(this).hasClass("selection-black")) {
            $('#' + thisBoxID + ' .selected-box').removeClass().addClass("selected-box button black-button");
        }
        else if ($(this).hasClass("selection-navy")) {
            $('#' + thisBoxID + ' .selected-box').removeClass().addClass("selected-box button navy-button");
        }
        else if ($(this).hasClass("selection-teal")) {
            $('#' + thisBoxID + ' .selected-box').removeClass().addClass("selected-box button teal-button");
        }
        else if ($(this).hasClass("selection-gold")) {
            $('#' + thisBoxID + ' .selected-box').removeClass().addClass("selected-box button gold-button");
        }
        else if ($(this).hasClass("selection-green")) {
            $('#' + thisBoxID + ' .selected-box').removeClass().addClass("selected-box button green-button");
        }
        //End "Colorized" selected states
        $('.select-box').css('display', 'none');
     
    });
    return true;
});
//End of DROPDOWN-SELECTION BOX

//SEARCHBOX EMPTY/BLUR

function emptySearch(e) {
    var thisSearchBox = e.target;
    if ($(thisSearchBox).val() == $(thisSearchBox).attr("defaultValue")) { $(thisSearchBox).val(""); }
}
function reloadSearch(e) {
    var thisSearchBox = e.target;
    if ($(thisSearchBox).val() == "") { $(thisSearchBox).val($(thisSearchBox).attr('defaultValue')); }
}
$('input[type=text],input[type=password],textarea').on("focus", emptySearch);
$('input[type=text],input[type=password],textarea').on("blur", reloadSearch);

//End of SEARCHBOX EMPTY/BLUR

//SMART-REPOSITION of ELEMENTS by SCREENSIZE

//$(".size-smart").delay(800).css('visibility', 'visible').delay(800);

	
jQuery.event.add(window, "load", smartRepo);
jQuery.event.add(window, "resize", smartRepo);

function smartRepo() {
    var w = $(window).width();
    var konstant = 960;
    var goofyShift = -300;
    try { goofyShift = gS; }
    catch (err) { }
// ~ Android issue
	if(navigator.userAgent.toLowerCase().indexOf("android")>-1)
		{
			$(".size-smart").removeClass("size-smart");
		}  // additionally, carousels require outside JS (carousel.js for Android compatibility
    if (w > konstant) {
        shiftAmt = (w - konstant) / 2;
            //Flash Movie on HP
        //$("#hero-swf").css("margin-left", (-120 + shiftAmt) + "px");
		 $("#home-overlay-swf").attr("width", w);
		 $("#hero-swf").attr("width", w);
        //static panels over 1024 width
        $(".size-smart").css("background-position", goofyShift + shiftAmt);
        //end static panels
    } else {
        $(".size-smart").css("background-position", goofyShift);
    }
}
window.setTimeout(function() {
    $(".size-smart").css('visibility', 'visible');
}, 800);

//End of SMART-REPOSITION of ELEMENTS by SCREENSIZE
function isCanvasSupported(){
  var elem = document.createElement('canvas');
  return !!(elem.getContext && elem.getContext('2d'));
}

function EnterKeySubmission(e, buttonId) {
    var evt = e ? e : window.event;
    var bt = document.getElementById(buttonId);
    if (bt) {
        if (evt.keyCode == 13) {
            if (valSubmit() == true) {
                //bt.click();
                __doPostBack(buttonId, "");
                return false;
            }
            else {
                return false;
            }
        }
    }
}

function httpGet(theUrl)
{
    var xmlHttp = null;

    xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", theUrl, false );
    xmlHttp.send( null );
    return xmlHttp.responseText;
}
function getQS(variable)
{
       var query = window.location.search.substring(1);
       var vars = query.split("&");
       for (var i=0;i<vars.length;i++) {
               var pair = vars[i].split("=");
               if(pair[0] == variable){return pair[1];}
       }
       return(false);
}
