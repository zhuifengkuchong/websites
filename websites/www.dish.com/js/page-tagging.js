function visitCookieCheck() {
	var getCount = getCookie("vcDish");
	var getState = getCookie("vcCurrent");
	
	if (getState == null) {
		if (getCount == null) {
			createCookie("vcDish",1,30);
		} else {
			createCookie("vcDish",parseInt(getCount)+1,30);
		}
	}
	createCookie("vcCurrent",1,0.021);
}
visitCookieCheck();

function queryS(key) {
    key = key.replace(/[*+?^$.\[\]{}()|\\\/]/g, "\\$&"); // escape RegEx meta chars
    var match = location.search.match(new RegExp("[?&]"+key+"=([^&]+)(&|$)"));
    return match && decodeURIComponent(match[1].replace(/\+/g, " "));
}
function WT_Meta(type, content) {
	switch (type) 
	{
		case "TFN":
			$('head').append('<meta name="DCSext.w_KBID" content="' + content + '" />');
			break;
		case "DNI":
			$('head').append('<meta name="DCSext.w_DNI" content="' + content + '" />');
			Webtrends.multiTrack({argsa:['http://www.dish.com/js/WT.dl','dni','DCSext.w_DNI',content]});
			break;
	}
}
function cleanText(textitem) {
	if (textitem != null) {
		textitem = textitem.replace(/\'/g,'');
		textitem = textitem.replace(/\n/g,'');
		textitem = textitem.replace(/,/g,'');
	} else {
		textitem = "";
	}
	textitem = textitem.trim();
	return textitem;
}
function divCheck(currDiv) {
	if 
	(
		(currDiv == "tabs") ||
		(currDiv == "faqtabs")
	)
	{
		return false;
	} else {
		return true;
	}
}
function linkTrack(e,section) {
	var currType = "textlink";
	var currText = "";
	var currSection = "";
	var currURL = e.attr('href');
	var currTracking = "";
	if (e.is('[tracking]')) {
		currTracking = e.attr('tracking');
	} else {
		currTracking = "Standard";
	}
	if (e.children('img').attr('alt') != null) {
		currText = e.children('img').attr('alt');
		currType = "image";
	} else {
		if (e.parent().is('[class]')) {
			if (e.parent().attr('class').indexOf('button') >= 0) {
				currType = "button";
			}
		}
		currText = e.text();
	}
	if (section == null) {
		currSection = e.closest('div[id]').attr('id');
	} else {
		currSection = section;
	}
	currText = cleanText(currText);
	if (divCheck(e.closest('div[id]').attr('id'))) {
		var currTag = "Webtrends.multiTrack({element:this, argsa:['http://www.dish.com/js/WT.dl','99','DCSext.w_linkSection','" + currSection + "','DCSext.w_linkType','" + currType + "','DCSext.w_linkText','" + currText + "','DCSext.w_linkTracking','" + currTracking + "']})";
		var currClick = e.attr('onclick');
		if (e.is('[onclick]')) {
			if (currClick.indexOf("Webtrends.multiTrack") < 0) {
				currTag = currTag + ";" + currClick;
			} else {
				currTag = currClick;
			}
		}
		e.attr('onclick', currTag);
	}
}
function areaTrack(e,section) {
	var currType = "imagemap";
	var currText = "";
	var currSection = "";
	var currURL = e.attr('href');
	var currTracking = "";
	if (e.is('[tracking]')) {
		currTracking = e.attr('tracking');
	} else {
		currTracking = "Standard";
	}
	if (e.attr('alt') != null) {
		currText = e.attr('alt');
	} else {
		currText = "No Text";
	}
	if (section == null) {
		currSection = e.closest('div[id]').attr('id');
	} else {
		currSection = section;
	}
	if (divCheck(e.closest('div[id]').attr('id'))) {
		var currTag = "Webtrends.multiTrack({element:this, argsa:['http://www.dish.com/js/WT.dl','99','DCSext.w_linkSection','" + currSection + "','DCSext.w_linkType','" + currType + "','DCSext.w_linkText','" + currText + "','DCSext.w_linkTracking','" + currTracking + "']})";
		var currClick = e.attr('onclick');
		if (e.is('[onclick]')) {
			if (currClick.indexOf("Webtrends.multiTrack") < 0) {
				currTag = currTag + ";" + currClick;
			} else {
				currTag = currClick;
			}
		}
		e.attr('onclick', currTag);
	}
}
$(window).load(function(){
	var aId = Adometry_Integration_Data.id || "noAdid";
	$('head').append('<meta name="DCSext.w_AdmID" content="' + aId + '" />');

	$('#search form').each(function() {
		// Search form tracking
		searchTopNavTag = "Webtrends.multiTrack({element:this, argsa:['DCS.dcsuri','/search" + unescape(window.location.pathname.replace('.aspx','/')) + $('#siteSearch').closest('div[id]').attr('id').toLowerCase() + "/search/','http://www.dish.com/js/WT.dl','20']})";
		$('#siteSearch').attr('onsubmit', searchTopNavTag);
	});
	$('a[href]').each(function(){
		linkTrack($(this));
	});
	$('area[href]').each(function(){
		areaTrack($(this));
	});
});

function refreshWTTags() {
	setTimeout(function() {
		$('a[href]').each(function(){
			linkTrack($(this));
		});
	}, 500);
}