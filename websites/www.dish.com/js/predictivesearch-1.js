function queryAutoSuggestHandler(userQ){
	var xhr = new XMLHttpRequest();
	xhr.open("GET", "http://www.dish.com/gsearch/autosuggesthandler.ashx?q="+userQ, true);
	xhr.onreadystatechange = function () {
		if (xhr.readyState == 4) {
			var rawResp = xhr.responseText;
			rawResp = rawResp.replace(/\s+/g," ");
			var resp = JSON.parse(rawResp);
			data=resp;
			buildSearchResultsList(data.results);
		}
	}
	xhr.send();
}
function buildSearchResultsList(resultsList){
	$("#as-list").remove();
	if (resultsList!=null){
		toAppend="<ul id='as-list' class='as-list'></ul>";
		$("#top-nav #search #siteSearch").append(toAppend);
		
		toAppendLI="";
		count=0;
		for (var key in resultsList) {
		   var obj = resultsList[key];
		   if (count<10){
			  toAppendLI+= "<li class='as-result-item'>";
			  toAppendLI+= "<a href='/gsearch/?q="+obj.name+"'>";
			  toAppendLI+= obj.name.substring(0,28)+"</a></li>";
			  }
			  else {break;}
			count++;
		}
		$("#top-nav #search #siteSearch #as-list").append(toAppendLI);
	}
	else {$("#as-list").remove();}
}
function checkSearchBox(){ 
	$.ajaxSetup({ cache: false });
	var data;
	var uQuery="";
	var runQuery = false;
	if ($("#search-subnav-search-box")){ 
		if ($("#search-subnav-search-box").val()){
			if ($("#search-subnav-search-box").val().length>1){
				uQuery = $("#search-subnav-search-box").val().replace(/[^a-z0-9\s]/gi, ''); 
				runQuery=true;
			}
			else
			{ 
				buildSearchResultsList(null);
			}
		}else{buildSearchResultsList(null);}
	}
	if (runQuery){
		queryAutoSuggestHandler(uQuery);
	}
}
function clearResultsList(){
	if ($("#search-subnav-search-box").val()){
		if ($("#search-subnav-search-box").val().length<2){
			buildSearchResultsList(null);
		}
	}else{buildSearchResultsList(null);}
}
$(document).ready(function () {
	$('#search-subnav-search-box').keyup(function () {
		checkSearchBox();
	});
	$('#search-subnav-search-box').change(function () { 
		clearResultsList();
	});
	$('body').click(function () { 
		clearResultsList();
	});
});