// JavaScript Document


var staticPhoneNumber="800-333-DISH";
	//phonenumber shenanigans
function useStaticPhone(){
		jQuery(document).ready(function(){
				jQuery('.staticPhone').each(function(index) {
					jQuery(this).text(staticPhoneNumber);
		  		});
		});
}
function getCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}
function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}
function createCookie(name,value,years) {
	if (years) {
		var date = new Date();
		date.setTime(date.getTime()+(years*365*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+";domain=.dish.com;path=/";
}
function createCookieInDays(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days  * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    }
    else var expires = "";
    document.cookie = name + "=" + value + expires + ";domain=.dish.com;path=/";
}
function eraseCookie(name) {
	createCookie(name,"",-1);
}
function CustomerRedirect(redirectUrl) {
    var is_customer = readCookie("is_customer");
    createCookie("is_customer", "yes", 1);
    window.location = redirectUrl;

}
// Read a page's GET URL variables and return them as an associative array.
function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}
//This function returns environment 'DEV','TEST','' - for prod.
var hostname = $('<a>').prop('href', location.pathname).prop('hostname').toUpperCase();
function getEnvironment() {
    var env = "";    
    if (hostname.indexOf('DEV') > -1)
        env = '-dev';
    else if (hostname.indexOf('TEST') > -1)
        env ='-test';

    return env;
}
function getSiteURL(siteName) {
    var siteURL = "http://www.dish.com/js/www.dish.com";
    switch(siteName.toUpperCase())
    {
        case "CSA":
            siteURL = "https://csa" + getEnvironment() + ".dishnetwork.com";
            break;
    }
    return siteURL;
}
function ComparePackagesOverlay(oComparePackLink, vals) {
    var compareUrl = getSiteURL('CSA') + "/packages/compare-overlay.aspx?vals=" + vals + "&App=dish.com";
    $(oComparePackLink).colorbox({ href: compareUrl, iframe: true, width: 1030, height: 1050, top: 50 });
}
function CompareIntlPackagesOverlay(oComparePackLink, vals) {
    compareUrl = "/entertainment/packages/international-compare/?language=" + vals;
    $(oComparePackLink).colorbox({ href: compareUrl, iframe: true, width: 1010, height: 675, top: 50 });
}