<script id = "race42a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race42={};
	myVars.races.race42.varName="Function[5209].guid";
	myVars.races.race42.varType="@varType@";
	myVars.races.race42.repairType = "@RepairType";
	myVars.races.race42.event1={};
	myVars.races.race42.event2={};
	myVars.races.race42.event1.id = "special";
	myVars.races.race42.event1.type = "onmouseover";
	myVars.races.race42.event1.loc = "special_LOC";
	myVars.races.race42.event1.isRead = "False";
	myVars.races.race42.event1.eventType = "@event1EventType@";
	myVars.races.race42.event2.id = "search";
	myVars.races.race42.event2.type = "onmouseover";
	myVars.races.race42.event2.loc = "search_LOC";
	myVars.races.race42.event2.isRead = "True";
	myVars.races.race42.event2.eventType = "@event2EventType@";
	myVars.races.race42.event1.executed= false;// true to disable, false to enable
	myVars.races.race42.event2.executed= false;// true to disable, false to enable
</script>

