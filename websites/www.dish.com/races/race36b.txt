<script id = "race36b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race36={};
	myVars.races.race36.varName="Function[5209].guid";
	myVars.races.race36.varType="@varType@";
	myVars.races.race36.repairType = "@RepairType";
	myVars.races.race36.event1={};
	myVars.races.race36.event2={};
	myVars.races.race36.event1.id = "home";
	myVars.races.race36.event1.type = "onmouseover";
	myVars.races.race36.event1.loc = "home_LOC";
	myVars.races.race36.event1.isRead = "True";
	myVars.races.race36.event1.eventType = "@event1EventType@";
	myVars.races.race36.event2.id = "Lu_window";
	myVars.races.race36.event2.type = "onload";
	myVars.races.race36.event2.loc = "Lu_window_LOC";
	myVars.races.race36.event2.isRead = "False";
	myVars.races.race36.event2.eventType = "@event2EventType@";
	myVars.races.race36.event1.executed= false;// true to disable, false to enable
	myVars.races.race36.event2.executed= false;// true to disable, false to enable
</script>

