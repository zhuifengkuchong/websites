<script id = "race8a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race8={};
	myVars.races.race8.varName="search-subnav-search-box__onblur";
	myVars.races.race8.varType="@varType@";
	myVars.races.race8.repairType = "@RepairType";
	myVars.races.race8.event1={};
	myVars.races.race8.event2={};
	myVars.races.race8.event1.id = "Lu_Id_script_22";
	myVars.races.race8.event1.type = "Lu_Id_script_22__parsed";
	myVars.races.race8.event1.loc = "Lu_Id_script_22_LOC";
	myVars.races.race8.event1.isRead = "False";
	myVars.races.race8.event1.eventType = "@event1EventType@";
	myVars.races.race8.event2.id = "search-subnav-search-box";
	myVars.races.race8.event2.type = "onblur";
	myVars.races.race8.event2.loc = "search-subnav-search-box_LOC";
	myVars.races.race8.event2.isRead = "True";
	myVars.races.race8.event2.eventType = "@event2EventType@";
	myVars.races.race8.event1.executed= false;// true to disable, false to enable
	myVars.races.race8.event2.executed= false;// true to disable, false to enable
</script>

