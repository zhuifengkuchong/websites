<script id = "race38a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race38={};
	myVars.races.race38.varName="Function[5209].guid";
	myVars.races.race38.varType="@varType@";
	myVars.races.race38.repairType = "@RepairType";
	myVars.races.race38.event1={};
	myVars.races.race38.event2={};
	myVars.races.race38.event1.id = "why-dish";
	myVars.races.race38.event1.type = "onmouseover";
	myVars.races.race38.event1.loc = "why-dish_LOC";
	myVars.races.race38.event1.isRead = "False";
	myVars.races.race38.event1.eventType = "@event1EventType@";
	myVars.races.race38.event2.id = "entertainment";
	myVars.races.race38.event2.type = "onmouseover";
	myVars.races.race38.event2.loc = "entertainment_LOC";
	myVars.races.race38.event2.isRead = "True";
	myVars.races.race38.event2.eventType = "@event2EventType@";
	myVars.races.race38.event1.executed= false;// true to disable, false to enable
	myVars.races.race38.event2.executed= false;// true to disable, false to enable
</script>

