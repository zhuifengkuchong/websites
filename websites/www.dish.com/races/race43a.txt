<script id = "race43a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race43={};
	myVars.races.race43.varName="Object[6100].mousemove";
	myVars.races.race43.varType="@varType@";
	myVars.races.race43.repairType = "@RepairType";
	myVars.races.race43.event1={};
	myVars.races.race43.event2={};
	myVars.races.race43.event1.id = "search";
	myVars.races.race43.event1.type = "onmouseover";
	myVars.races.race43.event1.loc = "search_LOC";
	myVars.races.race43.event1.isRead = "False";
	myVars.races.race43.event1.eventType = "@event1EventType@";
	myVars.races.race43.event2.id = "main-nav-wrap";
	myVars.races.race43.event2.type = "onmouseover";
	myVars.races.race43.event2.loc = "main-nav-wrap_LOC";
	myVars.races.race43.event2.isRead = "False";
	myVars.races.race43.event2.eventType = "@event2EventType@";
	myVars.races.race43.event1.executed= false;// true to disable, false to enable
	myVars.races.race43.event2.executed= false;// true to disable, false to enable
</script>

