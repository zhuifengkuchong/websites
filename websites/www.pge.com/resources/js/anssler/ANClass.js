//Base class used for all other classes
jQuery.namespace( 'anssler' );

anssler.ANClass = Class.extend({
	init: function(config){
		// Making a shadow copy of the data if it exists
		if(this.data && config && config.data) {
			var dataHolder = {};
			$.extend(dataHolder, this.data);
			$.extend(dataHolder, config.data);
			config.data = dataHolder;
		}
		$.extend(this, config);

		//wvo_v2.currentPageObject.init();
	}
});