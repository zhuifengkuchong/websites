var AN = {
		
	//Pass the component and the component handler method
	registerComponent : function(component)
	{
		//Checking if the component is an instance of component
		if (!(component instanceof anssler.component.Component))
			{
				alert("Only components that extend from anssler.component.Component can be registered");
				return;
			}
		$(this).bind("loadComponent",jQuery.proxy(component.ready, component));
	},
	
	onComponentsLoaded : function(object,componentsLoadedHandler)
	{
		$(this).bind("componentsLoaded",jQuery.proxy(componentsLoadedHandler, object));
	},
	
	ready : function()
	{
		if (!this.currentPageObject)
			this.currentPageObject = new anssler.page.Page();
	 		
		//Load the page
		this.currentPageObject.onLoad();
		
		
		// Triggers the components to be loaded
		$(this).trigger("loadComponent");
		
		//After page and components have been loaded
		$(this).trigger("componentsLoaded");

	},
	
	getPage : function()
	{
		return this.currentPageObject;
	},
	
	/**
	 * Blocks the dialog.
	 * @param message
	 * @param title (optional)
	 * @param showWaitingImage (optional)
	 * @param height (optional)
	 * @param width (optional)
	 */
  	openWaitingDialog : function(message,title, showWaitingImage, height, width)
  	{
		var dialogHtml = '<div class="waiting-dialog"';
		if (title) {
			dialogHtml = dialogHtml + ' title="'+ title +'">';
		}
		if (showWaitingImage == 'true') {
			dialogHtml = dialogHtml + '<div class="image-container">&nbsp;</div>';
		} 
		dialogHtml = dialogHtml + '<div class="message-container"><p>' + message + '</p></div></div>';
  		$('body:first').append(dialogHtml);
  		
  		//$('body:first').append(
  		//	'<div class="waiting_dialog" title="'+ title +'" style="text-align: center;">' +
  		//	'<p>' + message + '</p>' +
  		//	'</div>');
  		
  		// Set height and width defaults if passed in values are empty
  		if (height == null || height == '') {
  			height = 'auto';
  		}
  		
  		if (width == null || width == '') {
  			width = 300;
  		}
  		
		$( ".waiting-dialog" ).dialog({
			modal: true,
			height : height,
			width : width,
			open: function(event, ui) {
				if ($.browser.safari == true){
					window.setTimeout(function() {
			            jQuery(document).unbind('mousedown.dialog-overlay');
			            jQuery(document).unbind('mouseup.dialog-overlay');
						}, 100);
					}
				},
			closeOnEscape : false,
			draggable : false,
			buttons: {},
			resizable : false,
			dialogClass: "loadingScreenWindow"
			
		});
  	},
  	  	
  	closeWaitingDialog : function()
  	{
		$( ".waiting-dialog" ).dialog('close');
		$( ".waiting-dialog" ).remove();
  	},

	openDialog : function(message, title,height,width,modal,thisDialogClass) {

  		var dialogHtml = '<div class="dialogBox"';
  		
  		if (title)
  		{
  			var dialogHtml = dialogHtml + ' title="' + title + '"';
  		}
  		
  		var dialogHtml = dialogHtml + '>' + message + '</div>';
  		
		$('body:first').append(dialogHtml);

		$("body > .dialogBox:last").dialog({
			modal: false,
			height: height,
			width: width,
			resizable : true,
			dialogClass: thisDialogClass,
			open: function(event, ui) {
				if ($.browser.safari == true){
					window.setTimeout(function() {
			            jQuery(document).unbind('mousedown.dialog-overlay');
			            jQuery(document).unbind('mouseup.dialog-overlay');
						}, 100);
					}
				},
			close : function(e) {
				$(this).remove();
			}
		});
	},
	/**
	 * Add commas used to add commas to numbers
	 * http://www.mredkj.com/javascript/nfbasic.html
	 * @param nStr number to be converted into a string with commas
	 */
	addCommas : function(nStr) {
		nStr += '';
		x = nStr.split('.');
		x1 = x[0];
		x2 = x.length > 1 ? '.' + x[1] : '';
		var rgx = /(\d+)(\d{3})/;
		while (rgx.test(x1)) {
			x1 = x1.replace(rgx, '$1' + ',' + '$2');
		}
		return x1 + x2;
	},
	/**
	 * Retrieves a param from url query based on passed in name
	 * @param name of param to look for
	 * @return value of param or '' if it doesnt find it
	 */
	getQueryAttr : function(name) {
		name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
		var regexS = "[\\?&]" + name + "=([^&#]*)";
		var regex = new RegExp(regexS);
		var results = regex.exec(window.location.href);
		if (results == null)
			return '';
		else
			return results[1];
	},
	/**
	 * Copied from http://javascript.internet.com/forms/currency-format.html
	 * @param num currencyNumber
	 */
	formatCurrency : function(num) {
		num = num.toString().replace(/\$|\,/g, '');
		if (isNaN(num))
			num = "0";
		sign = (num == (num = Math.abs(num)));
		num = Math.floor(num * 100 + 0.50000000001);
		cents = num % 100;
		num = Math.floor(num / 100).toString();
		if (cents < 10)
			cents = "0" + cents;
		for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
			num = num.substring(0, num.length - (4 * i + 3)) + ',' +
					num.substring(num.length - (4 * i + 3));span
		return (((sign) ? '' : '-') + num + '.' + cents);
	},
	/**
	 * Get query string by name
	 * copied from http://stackoverflow.com/questions/901115/get-query-string-values-in-javascript
	 */
	getParameterByName : function(name) {
	  name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
	  var regexS = "[\\?&]" + name + "=([^&#]*)";
	  var regex = new RegExp(regexS);
	  var results = regex.exec(window.location.href);
	  if(results == null)
		return "";
	  else
		return decodeURIComponent(results[1].replace(/\+/g, " "));
	},

	/**
	 * Browser safe debug statement
	 * Copied from http://www.onelineofcode.net/code/safeconsolelogging
	 * @param obj
	 * @param label Optional, displays the debug with a label line before it
	 */
	debug : function(/*object*/ obj, /*String*/ label) {
		// When the site goes to production, we should disable this on prod or add in some secured parameter for log messages
		if (window.console) {
			if(label) {
				console.log(label);
			}
			console.log(obj);
		}
	},


	/**
	 * This loads the browser sniffer logic
	 */
	loadBrowserSniffer : function() {
		var userAgent = navigator.userAgent.toLowerCase();
		if ($.browser.mozilla) {
			//Is it Firefox?
			if ((navigator.userAgent.toLowerCase().indexOf('navigator') != -1) || (navigator.userAgent.toLowerCase().indexOf('seamonkey') != -1)) {
				//Since I only care if it is Firefox, I am setting it to false if it is not.
				$.browser.mozilla = false;
			}
		}
		
		if (!$.browser.chrome && !$.browser.safari && !$.browser.mozilla){
			if ($.browser.msie) {				
				userAgent = $.browser.version;
				userAgent = userAgent.substring(0, userAgent.indexOf('.'));
				version = userAgent;
				if(version < 7){					
					if(!this.checkCookie()){
						this.loadBrowserMessage();
					}
				}
			}
			else{
				if(!this.checkCookie()){
					this.loadBrowserMessage();
				}
			}
		}

	},
	/**
	 * This loads the browser sniffer dialog box
	 */
	loadBrowserMessage : function() {
  			$('body:first').append(
  	  			'<div class="browser-dialog-titlebar-close" title="Browser Information">' +
  	  			'<p><strong>This website may not be fully functional on the internet browser version you are using.</strong></p>'+
  	  			'<p>Before you log in to your account, we suggest you use on of the browsers listed below.  If you do not '+
  	  			'have any of these browsers, you can download for free.  Please note that if you continue using your current browser, '+
  	  			'your experience on the website will not be optimal.</p>' +
  	  			'<p><a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?WT.mc_id=MSCOM_EN_US_HP_CAROUSEL_121LMUS007473" target="_blank">Microsoft<span>&#174;</span> Internet Explorer<span>&#174;</span></a>'+
  	  			'<br/><br/><a href="http://www.mozilla.com/en-US/firefox/fx/"  target="_blank">Mozilla<span>&#174;</span> Firefox<span>&#174;</span></a><br/><br/>' +
  	  			'<a href="http://www.apple.com/safari/download/"  target="_blank">Apple<span>&#174;</span> Safari<span>&#174;</span></a><br/><br/><a href="http://www.google.com/chrome"  target="_blank">Google Crome&#8482</a></p>'+  
  	  			'</div>');     
  	  		
  			$( ".browser-dialog-titlebar-close" ).dialog({
  				modal: true,
  				closeOnEscape : false,
  				draggable : false,
  				buttons: [{
  					text : 'Continue to website',
  					click : function() {
  						/* creating the cookie */
  						document.cookie='BROWSER_TEST=true; path=/';
  						
  						$(this).dialog('close');
  						$('.browser-dialog-titlebar-close').dialog('destroy');
					return false;
  					}
  				}],
  				resizable : false,
  				width: 475,
  				height: 700,
  				dialogClass: 'loadingScreenWindow'
  			});
	},
	
	/**
	 * This checks for the cookie created by the browser sniffer
	 */
	 checkCookie : function() {		  
		  var cookieName = 'BROWSER_TEST';
		  var cookie = this.getCookie(cookieName);
		  if (cookie == 'true')
		  {			 
		    return true;
		  }
		  else return false;
	},
	
	/**
	 * This checks for the cookie created by the browser sniffer
	 */
	 getCookie : function(cookieName) {
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++)
		{
		  x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
		  y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
		  x=x.replace(/^\s+|\s+$/g,"");
		  if (x==cookieName)
		    {
		    return unescape(y);
		    }
		  }
	},
	
	secondsToTime : function(secs) {
	    var hours = Math.floor(secs / (60 * 60));
	   
	    var divisor_for_minutes = secs % (60 * 60);
	    var minutes = Math.floor(divisor_for_minutes / 60);
	 
	    var divisor_for_seconds = divisor_for_minutes % 60;
	    var seconds = Math.ceil(divisor_for_seconds);
	   
	    var obj = {
	        "h": hours,
	        "m": minutes,
	        "s": seconds
	    };
	    return obj;
	},

	isDate :  function(dateStr) {
			var datePat = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
			var matchArray = dateStr.match(datePat); // is the format ok?
	
			if (matchArray == null) {
				//alert("Please enter date as either mm/dd/yyyy or mm-dd-yyyy.");
				return false;
			}
	
			month = matchArray[1]; // p@rse date into variables
			day = matchArray[3];
			year = matchArray[5];

			if (month < 1 || month > 12) { // check month range
				//alert("Month must be between 1 and 12.");
				return false;
			}
			if (day < 1 || day > 31) {
				//alert("Day must be between 1 and 31.");
				return false;
			}
			if ((month==4 || month==6 || month==9 || month==11) && day==31) {
				//alert("Month "+month+" doesn`t have 31 days!")
				return false;
			}

			if (month == 2) { // check for february 29th
				var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
				if (day > 29 || (day==29 && !isleap)) {
					//alert("February " + year + " doesn`t have " + day + " days!");
					return false;
				}
			}
			return true; // date is valid
	},

	validateDate : function (dateFormat, strDate) {
		var cs = this;

		if (dateFormat != "mm/dd/yyyy") {
			var dates = strDate.split("/");
			var year = dates[2];
			var month = dates[1];
			var day = dates[0];
			strDate = month + "/" + day + "/" + year;
		}
		if (this.isDate(strDate)) {
			var strDates = strDate.split("/");
			var date = new Date();
			date.setFullYear(strDates[2], strDates[0]-1, strDates[1]);
			return date;
		}
		return null;
	},

	/* set, and delete cookies */
	setCookie : function ( name, value, expires, path, domain, secure ) {
		var today = new Date();
		today.setTime( today.getTime() );
		if ( expires ) {
			expires = expires * 1000 * 60 * 60 * 24;
		}
		var expires_date = new Date( today.getTime() + (expires) );
		document.cookie = name+"="+escape( value ) +
			( ( expires ) ? ";expires="+expires_date.toGMTString() : "" ) + //expires.toGMTString()
			( ( path ) ? ";path=" + path : "" ) +
			( ( domain ) ? ";domain=" + domain : "" ) +
			( ( secure ) ? ";secure" : "" );
	},
		
	deleteCookie : function ( name, path, domain ) {
		if ( this.getCookie (name)) {
			document.cookie = name + "=" +
				( ( path ) ? ";path=" + path : "") +
				( ( domain ) ? ";domain=" + domain : "" ); +
				";expires=Thu, 01-Jan-1970 00:00:01 GMT";
		}
	},

	deepCopy : function(object) {
		return jQuery.extend(true, {}, object);
	},
	validateForm: function(formId, componentId, errorType, errMesg) {
		var validator=$("#"+formId).validate(
				{
					highlight : function(input) {
						$(input).addClass("ui-state-error");
						// $(input).parents('.addressValue').parents(".formField").addClass("ui-state-error");
					},
					unhighlight : function(input) {
						$(input).removeClass("ui-state-error");
						$(input).parents('.addressValue')
								.parents(".formField")
								.removeClass("ui-state-error");
					},
					errorElement : "span",
					wrapper : 'div',
					errorClass : "fieldError",
					errorPlacement : function(error, element) {
						var errorIcon = '<span class="ui-icon ui-icon-circle-arrow-n inline-error-icon"/>';
						// error.find("span:first").addClass("inline-error-msg");
						error.prepend(errorIcon).appendTo(
								element.parent());
						
						error.addClass('ui-state-error');
					},
					invalidHandler: function(form, validator) {
						var errors = validator.numberOfInvalids();
						if (errors) {
							AN.getPage().clearAllComponentMessages(componentId);
							AN.getPage().displayStatusMessage(componentId, formId.toUpperCase(), errorType, errMesg);
						}

						
					}
				});
		return validator;
	}
};


$(document).ready(function() {
	try {
	 	console.log("loading components");
 	} catch(e) {

	}
	AN.ready();
});
