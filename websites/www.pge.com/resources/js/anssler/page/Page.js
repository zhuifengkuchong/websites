//Base Page Object which all objects should inherit from
jQuery.namespace('http://www.pge.com/resources/js/anssler/page/anssler.page');
/**
* DEFINE ALL THE GLOBAL EVENT TYPES
**/
var EVENT_TYPES = {
  //This is the event sent for displaying the report outage info window
  DISPLAY_REPORT_OUTAGE_INFO_WINDOW : 1, 
  DISPLAY_OUTAGE_DETAIL : 2,
  //This event is sent by the map component to the report outage component 
  UPDATE_ADDRESS_OUTAGE: 3,
  UPDATE_MAP_TO_ZOOM_LEVEL:4, //This event is called when the zoom level is changed
  DISPLAY_MULTIPLE_ADDRESS_DIALOG: 5,
  DISPLAY_EI_NOTIFICATION_SUCCESS_DIALOG: 6,
  SEARCH_BY_PREMISEID: 7,
  UPDATE_MAP_TO_LOCATION: 8
};

anssler.page.Page = anssler.ANClass
		.extend( {
		
			//An array of listeners for update_map
			LISTENER_LIST : [],
		
			init : function(config) {
				this._super(config);
			},
			//Ready when JQuery is loaded
			onLoad : function() {
			},

			loaded : false,

			//Method used to bind objects to the onLoad function of the page
			onReady : function(handler) {
				$(this).bind("ready", handler);
			},

			statusMessageBox : null,
			statusMessageRemoveClass : 'msgToRemove',

			/**
			 * Displays status messages @ the top of the page.
			 * @componentId : the component id calling the displaymessage
			 * @messageId: the Id of the message
			 * @status: one of the following: INFO, WARNING, ERROR, SUCCESS, NONE
			 */
			displayStatusMessage : function(componentId, messageId, status,
					message) {
				var cs = this;
				if (this.statusMessageBox == null) {
					$(".main-row")
							.prepend(
									"<div id='dynamicStatusMessages' class='sixteen columns ui-state-highlight ui-corner-all'><span class='closeDynamicStatusMessages'>x</span></div>");
					this.statusMessageBox = $(".main-row").find(
							"#dynamicStatusMessages");
					this.statusMessageBox.find('.closeDynamicStatusMessages')
							.click( function() {
								cs.statusMessageBox.remove();
								return false;
							});
				}

				var messageIcon;

				switch (status) {
				case "INFO":
					messageIcon = "ui-icon-info";
					break;
				case "WARNING":
					messageIcon = "ui-icon-notice";
					break;
				case "ERROR":
					messageIcon = "ui-icon-alert";
					break;
				case "SUCCESS":
					messageIcon = "ui-icon-check";
					break;
				default:
					messageIcon = "";
				}

				this.statusMessageBox
						.append("<div class='statusMessage "
								+ componentId
								+ "_"
								+ messageId
								+ "'><span style='float: left; margin-right: 0.3em;' class='ui-icon "
								+ messageIcon + "'></span>" + message
								+ "</div>");

			},

			/**
			 * Clears a specific message from the message box
			 * @componentId : the component id calling the clearMessage
			 * @messageId : the Id of the message
			 */
			clearMessage : function(componentId, messageId) {
				alert("Should clear message: " + messageId
						+ " for component id " + componentId);
			},

			/**
			 * Clears all messages for the specific componentId from the message box
			 * @componentId : the component id calling the clearAllComponentMessages
			 */
			clearAllComponentMessages : function(componentId) {
				var cs = this;
				var classPattern = new RegExp(componentId, 'g');

				$(cs.statusMessageBox).find('.statusMessage').each(
						function() {
							var classes = this.className.split(' ');
							$.each(classes, function(index, value) {
								if (classPattern.test(value)) {
									$('.' + value).addClass(
											cs.statusMessageRemoveClass);
								}
							});
						});

				$('div.' + cs.statusMessageRemoveClass).remove();
			},
			/**
			 * This method registers the event for which the listener
			 * wants the notification fo
			 * Since javascript doesn't store the object reference when
			 * we store the function reference, we have to store both to be able 
			 * to call it.
			 *
			 **/
			registerEvent : function(eventType, obj, funcRef ) {
				var cs=this;
				var objRef = {
				  'obj':obj,
				  'func':funcRef
				} 
				if(cs.LISTENER_LIST[eventType]) {
				 cs.LISTENER_LIST[eventType].push(objRef);
				} else {
				 cs.LISTENER_LIST[eventType]=new Array();
				 cs.LISTENER_LIST[eventType].push(objRef);
				}
				
			}, 
			/**
			 * This method calls the function with the object reference for access to data elements
			 * 
			 *
			 **/
			pushEvent: function(eventType, arrParams) {
				var cs=this;
				if(cs.LISTENER_LIST[eventType]) {
				  for(i=0;i<cs.LISTENER_LIST[eventType].length;i++) {
				    var objRef=cs.LISTENER_LIST[eventType][i];
				    objRef.func(objRef.obj, arrParams);
				  }
				}
			}			
		});