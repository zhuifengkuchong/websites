jQuery.namespace( 'anssler.component' );

//Base Component which all components should inherit from
anssler.component.Component = anssler.ANClass.extend({
  init: function(config){
    this._super(config);
    var cs = this;

    AN.registerComponent(cs);
  },
  	
   //This line is executed once the page is loaded. Needs to be implemented by the
   //extending classes
   ready : function() {
		alert("This component should overwrite this function: ready");
   },
   
    /* For wwwextqa server only
		 * If page is in livesite and the leftnav is pointing to a page in teamsite the URL will have
		 * http:wwww.pge.com which will take the the user to production server. The code below will
		 * replace www.pge.com with wwwextqa so the user can test correctly.
		*/
   leftNavQA: function() {
	var host = window.location.host;
	
	 if (host.indexOf('wwwextqa') >= 0) {
		$('#an_c12-sidebar_navigation').children('ul').children('li').each(function (index, element) {
			
			if ($(element).find('ul').length >0) {
				$(element).find('ul').children('li').each(function (index, elem) {
				   replaceHref(elem); 
				});
			} else {         
				  replaceHref(element);
			}
		});
	  }
	},

   replaceHref: function(element) {
     var href = $(element).find('a').attr('href');
    
    if (href.indexOf('http://www.pge.com/resources/js/anssler/component/www.pge.com') >=  0) {
        href = href.replace('http://www.pge.com/resources/js/anssler/component/www.pge.com', 'wwwextqa');
        $(element).find('a').attr("href", href);
    }
  },
   updateLabel: function(inputId, labelId,labelValue) {
     var input=$("input[id='"+inputId+"']");
     var label=$("label[for='"+labelId+"']");

     $(input).live( "click",function() {
	     $(label).html('');
     });

     $(input).focus( function() {
	     $(label).html('');
     });

     $(input).focusout( function(event) {
       var srchValue=$(this).val();
       if(srchValue=='') {
         $(label).html(labelValue);
       }
       event.stopPropagation();
	   });

      /** If input is not empty then dont show label **/
      if($(input).val() !='') {
	     $(label).html('');
	}
  },

  updateInputLabel: function(inputId, inputValue) {
    var input=$("input[id='"+inputId+"']");
    $(input).click( function() {
      $(input).select();
    });  

    $(input).focusout( function(event) {
      if($(input).val()==''){
        $(input).val(inputValue);
      }
	  });
  }
});
