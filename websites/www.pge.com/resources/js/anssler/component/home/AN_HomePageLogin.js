jQuery.namespace('http://www.pge.com/resources/js/anssler/component/home/anssler.component.home');

anssler.component.home.AN_HomePageLogin = anssler.component.Component.extend( {
	data : {
		userNameLabel :'username',
		passwordLabel :'password',
		userNameLabelValue :'',
		passwordLabelValue :''
	},

	ready : function() {
	    var cs=this;
//set the cookie value
//if ( $.cookie('cs_home_uname') != '' && $.cookie('cs_home_uname') != null ){
      if(cs.getCookie('cs_home_uname')) { 
        $("#username").val(cs.getCookie('cs_home_uname'));
        $("input[id='rememberusername']").each(function(){ this.checked = true; });
      }
      
      // checkbox remember username
      $('#rememberusername').click(function() {
        if( $("#rememberusername:checked").val() != null ) {
	      cs.setCookie('cs_home_uname', $("#username").val(), 365);
	    } 
	    else {
	      cs.setCookie('cs_home_uname', $("#username").val(), -7); 
	    }
      });       
      
	  var userNameLabel=$("label[for='"+cs.data.userNameLabel+"']");
	  var passwordLabel=$("label[for='"+cs.data.passwordLabel+"']");
   	  cs.data.userNameLabelValue = $(userNameLabel).html();
   	  cs.data.passwordLabelValue = $(passwordLabel).html();

	    cs.updateLabel(cs.data.userNameLabel,cs.data.userNameLabel,cs.data.userNameLabelValue);
	    cs.updateLabel(cs.data.passwordLabel,cs.data.passwordLabel,cs.data.passwordLabelValue);

   $("form[id='login-form']").submit(function() {
	return cs.checkLogin();
   });


	    //var userNameInputVal=$("input[id='"+cs.data.userNameLabel+"']").val();
	    //var passwordInputVal=$("input[id='"+cs.data.passwordLabel+"']").val();
	    //cs.updateInputLabel(cs.data.userNameLabel,userNameInputVal);
	    //cs.updateInputLabel(cs.data.passwordLabelValue,passwordInputVal);
		
		// Enable login button if login button was disabled and user hits the back button on the browser from another window 
		// or if users refreshes page and login button has been disabled
		$('#login-btn').attr('disabled', false);
	} ,

 checkLogin:function() { 
    if( $("#username").val() == '' ){ 
      alert( 'Please enter a valid user name' ); 
      return false; 
    } 
    if( $("#password").val() == '' ){ 
      alert( 'Please enter a valid password' ); 
      return false; 
    } 
    if( $("#rememberusername:checked").val() != null ) {
	   //$.cookie('cs_home_uname', $("#username").val(), { expires: 24 }); 
	  this.setCookie('cs_home_uname', $("#username").val(), 365);
	} 
	else {
	  this.setCookie('cs_home_uname', $("#username").val(), -7);
	}
	
	// disable login button after user/password have been entered
	$('#login-btn').attr('disabled', true);
	$('#login-btn').removeClass('an_c55_submit-btn');
	$('#login-btn').addClass('an_c55_submit-btn-disable');
	
	return true; 	
  },
  
  setCookie: function(c_name,value,exdays)
  {
    var exdate=new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
    document.cookie=c_name + "=" + c_value;
  },
  
  getCookie: function(c_name)
  {
    var i,x,y,ARRcookies=document.cookie.split(";");
    for (i=0;i<ARRcookies.length;i++)
    {
      x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
      y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
      x=x.replace(/^\s+|\s+$/g,"");
      if (x==c_name)
      {
        return unescape(y);
      }
    }
  }
});
