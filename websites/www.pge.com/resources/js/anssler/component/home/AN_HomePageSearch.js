jQuery.namespace('http://www.pge.com/resources/js/anssler/component/home/anssler.component.home');

anssler.component.home.AN_HomePageSearch = anssler.component.Component.extend( {
	data : {
	searchLabel :''
	},
	ready : function() {
	    var cs=this;
	    var labelId='global-search';
	    var label=$("label[for='"+labelId+"']");
   	    cs.data.searchLabel = $(label).html();
	    cs.updateLabel(labelId,labelId,cs.data.searchLabel);
	
	   if(navigator.platform.indexOf("Mac") != -1) {
               $('#an_homep-glb-search-container').css('margin-top', '-30px');
            }
	}
	
});
