// SMB Blog share this
(function(d,s,i,r) {
	var pathName = window.location.pathname;
	if (pathName.indexOf('http://www.pge.com/en/mybusiness/save/smbblog/index.page') >= 0) {
		if (d.getElementById(i)){return;}
		var n=d.createElement(s),e=d.getElementsByTagName(s)[0];
		n.id=i;n.src='../../../s7.addthis.com/js/250/addthis_widget.js-ver=2.5.js#username=pgeperspectives'/*tpa=http://s7.addthis.com/js/250/addthis_widget.js?ver=2.5#username=pgeperspectives*/;
		e.parentNode.insertBefore(n, e);
	}
})(document,"script","addthis",300000);

/* begin: webtrends tagging */
// http://www.pge.com/en/myhome/outages/addresources/index.page
// http://www.pge.com/en/myhome/outages/addresources/index.page


function webtrendsLink (link, url) {
	setTimeout( function() {
		var pageTitle =  document.title;
		var pathName = window.location.pathname;
		var linkTitle = $(link).attr('title');
		
	}, 100 );
	
	setTimeout( function() {
		window.location = url; 
	}, 500 );
}		
/* end: WEBTRENDS tagging */

// IXMI: Moved Optimizely code to Analytics.xsl - 07.16.14
/*A3AD: Optimizely - check if file already included. if not, add to page - 05.30.14
  function isOptimizelyAlreadyIncluded(){
    var scripts = document.getElementsByTagName("script");
	var src = "../../../cdn.optimizely.com/js/1115471569.js"/*tpa=http://cdn.optimizely.com/js/1115471569.js*/;

	for(var i = 0; i < scripts.length; i++) {
		//alert(scripts[i].getAttribute('src'));
       if(scripts[i].getAttribute('src') == src) {
		//alert('its a match!');
	   return false; }
	}
	
	document.write('<scr' + 'ipt type="text\/javascript" src="\/\/cdn.optimizely.com\/js\/1115471569.js"><\/script>');
};
document.write("<script type=text\/javascript>isOptimizelyAlreadyIncluded();</script>");
End Optimizely */ 
 
 
 /*A3AD: Foresee - check if file already included. if not, add to page - 04.28.14*/
 /* A3AD Removing call to Foresee for Tealium 08212014
 function isScriptAlreadyIncluded(){
    var scripts = document.getElementsByTagName("script");
	var src = "";
	if (window.location.hostname == "http://www.pge.com/resources/js/pge.com")
	{
	src = "https:\/\/www." + window.location.hostname + "\/foresee10\/foresee\/foresee-trigger.js";
    }
	else
	{
	src = "https:\/\/" + window.location.hostname + "\/foresee10\/foresee\/foresee-trigger.js";
	}
	for(var i = 0; i < scripts.length; i++) {
       if(scripts[i].getAttribute('src') == src) {
		//alert('its a match!');
	   return false; }
	}
	if (window.location.hostname == "http://www.pge.com/resources/js/pge.com")
	{
	 document.write('<scr' + 'ipt type="text\/javascript" src="https:\/\/www.'+ window.location.hostname +'\/foresee10\/foresee\/foresee-trigger.js"><\/script>');
	 }
	 else
	 {
	 document.write('<scr' + 'ipt type="text\/javascript" src="https:\/\/'+ window.location.hostname +'\/foresee10\/foresee\/foresee-trigger.js"><\/script>');
	 }
};
document.write("<script type=text\/javascript>isScriptAlreadyIncluded();</script>");
*/
/*End foresee */ 

/* ixmi 06/13/14: Start of Async HubSpot Analytics Code */
function isHubSpotlreadyIncluded(){
	var scripts = document.getElementsByTagName("script");
	var src = "Unknown_83_filename"/*tpa=http://www.pge.com/resources/js/js.hs-analytics.net*/;

	for(var i = 0; i < scripts.length; i++) {
		if((scripts[i].getAttribute('src') != null) && (scripts[i].getAttribute('src').indexOf(src)>= 0)) {
			return false;
		}
	}
	var html = '<script type="text/javascript"> (function(d,s,i,r) { if (d.getElementById(i)){return;} var n=d.createElement(s),e=d.getElementsByTagName(s)[0]; n.id=i;n.src="//js.hs-analytics.net/analytics/"+(Math.ceil(new Date()/r)*r)+"/393785.js"; e.parentNode.insertBefore(n, e); })(document,"script","hs-analytics",300000); </script>'

	document.write(html);
}
document.write("<script type=text\/javascript>isHubSpotlreadyIncluded();</script>");
/* End of Async HubSpot Analytics Code */


/* Moved from inline */
(function(){
	var ua = navigator.userAgent;
	var ie = /MSIE/.test(ua) && ua.replace(/.*?MSIE (.).*/,"$1");
	var ff = /Firefox/.test(ua) && ua.replace(/.*?Firefox\/(.)\.(.).*/,"$1_$2");
	var safari = /Safari/.test(ua) && ua.replace(/.*?Version\/(.).*/,"$1");
	var chrome = /Chrome/.test(ua) && ua.replace(/.*?Chrome\/(.).*/,"$1");
	var opera = /Opera/.test(ua) && ua.replace(/.*?Opera\/(.).*/,"$1");
	document.getElementsByTagName("html")[0].className="js"+(ie?" ie ie"+ie : ff?" ff ff"+ff : chrome?" chrome chrome"+chrome : safari?" safari safari"+safari : opera?" opera opera"+opera : "");
})();

function openSocialMediaDialog(url, media) {		
	var width  = 575,
		height = 400,
		left   = ($(window).width()  - width)  / 2,
		top    = ($(window).height() - height) / 2,
		url    = url,
		opts   = 'status=1' +
				 ',width='  + width  +
				 ',height=' + height +
				 ',top='    + top    +
				 ',left='   + left;

	window.open(url, media, opts);

	return false;
}

// instantiate PGE object
if (window.pge) _pge = pge;

// global PG&E object : loads external JS scripts dynamically based on the existance of corresponding class name in the DOM
pge = {
    info: 'Pacific Gas & Electric Base JavaScript',
    lang: 'en', // change this to an expression that gets the correct language value [en,es,vi,zh]
    log: function () {
        try { console.log.apply('', arguments); } catch (e) { }
    },
    gbl: {},
    //scriptsLoc: location.pathname.split('/').slice(0, -1).join('/') + '/includes/js/modules/', // change to absolute path once we integrate into platform
    scriptsLoc: '/resources/js/', // change to absolute path once we integrate into platform
    mod: {
        'accordion': {},
        'calendar': {},
        'carousel': {},
        'cookie': {},
        'dashboardbill': {},
        'datepicker': {},
        'disabletable': {},
        'dismissable_alert': {},
        'malleable_form': {},
        'maphilight': {},
        'modalhelp': {},
        'password': {},
        'payment_option_group': {},
        'programs-filter': {},
        'req_pay_arrangement_bill_summary': {},
        'scroll_table_bdy': {},
        'simplemodal': {},
        'services': {},
        'slider': {},
        'stickie': {},
        'tablefilter': {},
        'tablesorter': {},
        'tabviewer': {},
        'tipsfilter': {},
        'toggle': {},
        'tooltip': {},
        'usage_utilities': {},
        'usage_meter_widget': {},
        'validate': {},
        'youtube_module': {}
    },
    loaded: [],
    init: function () {
		var hostName = 'http%3A%2F%2F' + window.location.host;
		var pathName = window.location.pathname;		
		pathName = hostName + pathName.replace(/\//g, '%2F');

		// SMB Blog Twitter share for landing page
		$('.twitterSmbBlogShareDlg').click(function(event) {
			var href = $(this).closest('.pge_blogarticlepromo').find('.module-inner').find('a').attr('href');
			var tweetTxt = $(this).closest('.pge_blogarticlepromo').find('.module-inner').find('a').text();
			href = href.replace(/\//g, '%2F');
			
			tweetTxt = '&text=' + tweetTxt.replace(/\s/g, '%20');
			tweetTxt = tweetTxt.replace(/#/g, '%23');

			var tweetUrl = this.href + '?original_referer=' + pathName + tweetTxt +'&url=' + href;
			return openSocialMediaDialog(tweetUrl, 'tweet');
		});
		
		// SMB Blog Facebook share for landing page
		$('.fbSmbBlogShareDlg').click(function(event) {
			var href = $(this).closest('.pge_blogarticlepromo').find('.module-inner').find('a').attr('href');
			href = href.replace(/\//g, '%2F');		
			
			var fbUrl = this.href + '&u=' + href;
		
			return openSocialMediaDialog(fbUrl, 'facebook');
		});		
		
		// SMB Blog Twitter share for article page
		$('#twitterSmbBlogArtShare').click(function(event) {
			var artTweetText = $('#blogTweetContent').text();
			var tweetTxt = '&text=' + artTweetText.replace(/\s/g, '%20');
			var tweetTxt = tweetTxt.replace(/#/g, '%23');
			var pageUrl = '&url=' + pathName;
			
			var tweetUrl = this.href + '?original_referer=' + pathName + tweetTxt + pageUrl;
			return openSocialMediaDialog(tweetUrl, 'tweet');
		});
		
		// SMB Blog Facebook share for article page
		$('#fbSmbBlogArtShare').click(function(event) {
			var artFbText = '&t=' + $('#blogFacebookContent').text();
			var fbTxt = artFbText.replace(/\s/g, '%20');
			var pageUrl = '&u=' + pathName;
			
			var fbUrl = this.href + pageUrl;
		
			return openSocialMediaDialog(fbUrl, 'facebook');
		});
	
        var temp;

        // remove non-js message from DOM
        $('.enableJS').remove();

        // preload images
        (function () {
            var imagesToLoad = [
      				'../../includes/css/images/bg-tooltip-left.png'/*tpa=http://www.pge.com/includes/css/images/bg-tooltip-left.png*/,
      				'../../includes/css/images/bg-tooltip-right.png'/*tpa=http://www.pge.com/includes/css/images/bg-tooltip-right.png*/,
      				'../../includes/css/images/bg-tooltip-image-left.png'/*tpa=http://www.pge.com/includes/css/images/bg-tooltip-image-left.png*/,
      				'../../includes/css/images/bg-tooltip-image-right.png'/*tpa=http://www.pge.com/includes/css/images/bg-tooltip-image-right.png*/
      			];
            $('<div class="no-print" style="position:absolute;left:-9999px;top:-9999px;height:0;width:0;overflow:hidden;"/>').appendTo('body').html('<img alt="" src="' + imagesToLoad.join('"/><img alt="" src="') + '"/>');
        })();

        // [plugin] treat spacebar 'keypress' event the same as the mouse 'click' event
        $.fn.klik = function (f) {
            return this.bind('click keypress', function (e) {
                if (!e.keyCode || e.keyCode == 32) {
                    return f.call(this, e);
                }
            });
        };

        // add easeOutQuad to the easing options and make it the default option
        jQuery.extend(jQuery.easing, {
            def: 'easeOutQuad',
            easeOutQuad: function (x, t, b, c, d) {
                return -c * (t /= d) * (t - 2) + b;
            }
        });

        // instantiate language selection and set cookie on selection
        temp = $('#glb-hdr-toolbar .heading').klik(function () {

            var p = this.parentNode;
            $(p).toggleClass('open');
            pge.overlays.pushUnique(p);

            $('#language-options a').klik(function (e) {
                $.cookie('pgeLang', $(this).attr('class'));
            });

            return false;

        })[0];

        // prevent text selection (of language selector text) in ie
        if (temp && temp.attachEvent) {
            temp.attachEvent('onselectstart', function () { return false });
        }

        $("a.disabled").click(function () {
            return false;
        });

        // submit form
        $('ul#language-options a').klik(function () {
            $('input[name=langSel]').attr('value', $(this).attr('class'));
            $('#change-lang').submit();
        });

        // button roll-over
        if ($.browser.msie && $.browser.version == 6) {
            $('input.move-forward, a.move-forward, a.move-backward').hover(function () {
                var y = $(this).css('background-position-y');
                var newY = parseInt(y) - 50;
                $(this).css('background-position-y', newY);
            },
      			function () {
      			    var y = $(this).css('background-position-y');
      			    var newY = parseInt(y) + 50;
      			    $(this).css('background-position-y', newY);
      			});
        }
        else {
            $('input.move-forward, a.move-forward').hover(function () { $(this).addClass('rollover-forward') }, function () { $(this).removeClass('rollover-forward') });
            $('a.move-backward').hover(function () { $(this).addClass('rollover-backward') }, function () { $(this).removeClass('rollover-backward') });
        }

        // setup and, if needed, load modules & plugins from other files
        $.each(pge.mod, function (moduleName) {
            $.extend(pge.mod[moduleName], {
                className: '.mod-' + moduleName,
                load: function (m) {
                    if ($.inArray(moduleName, pge.loaded) < 0) {

                        var s = document.createElement('script');
                        s.src = pge.scriptsLoc + 'pge.' + moduleName.replace(/_/g, '-') + '.js';
                        $('head').append(s);

                        (function () {
                            if (!pge.mod[moduleName].init) {
                                setTimeout(arguments.callee, 30);
                                return;
                            }
                            pge.mod[moduleName].init(m);
                        })();

                        pge.loaded.push(moduleName);
                    }
                }
            });

            var module = $('.' + moduleName.replace(/_/g, '-'));
            if (module.size()) {
                pge.mod[moduleName].load(module);
            }
        });

        // primary nav li:hover
        $('#glb-hdr-primary-nav > li').hover(
    			function () {
    			    if ($('.tertiary-nav', this).size()) {
    			        $(this).addClass('over');
    			    }
    			},
    			function () {
    			    $(this).removeClass('over');
    			}
    		);

        // printing - "print-page" class
        $('.print-page').click(function () { window.print() });

        // special treatment for the login page for ie6
        // why not just use a css expression? because it isn't evaluated if you "maximize" or "restore" the window size, or if you only resize it vertically
        $('.ie6 .login').each(function () {
            window.onresize = function () {
                setTimeout(function () {
                    $('.login .glb-wrapper,.login .glb-wrapper-inner').css({ height: Math.max(575, $('body').height()) });
                }, 50);
            };
        });

        //L2 Carousel
        if($('.an_c9-carousel-list').length) {
            var numItems = $('.an_c9-carousel-list ul li').length;
            var indx = 0;
            var rightIndex = 2;
            var animationSpeed = 500;
            var animationIncrement = 0;
            var $next = $('.an_c9-carousel-nav .next span');
            var $prev = $('.an_c9-carousel-nav .prev span');
            var disabledBtnOpacity = "0.2";
            var vStartingBgPosition = -306;
            var curTitle = "";
            var curDesc = "";
            var timer;
            var timerEnabled = true;
            
            var timerInterval = null;

            var $firstItem = $('.an_c9-carousel-list ul li a img:first');
            if ($firstItem) {
                var margin = $firstItem.parent().parent().css('margin-right');
                animationIncrement = Number($firstItem.attr('width')) + Number(margin.substring(0, margin.length - 2));
                animationIncrement = 55;
            }

            var selectItem = function () {
                // Pause Video Player
				
				  // jwplayer(window.currentJWPlay).pause(true);
				 //  jwplayer('flashplayer-0').pause(true);
				 $('.an_c9-jw-container').each(function () {
				   try {
						var playerContainer=$(this);
						var fPlayer=$(playerContainer).find('flashPlayer-0');
						var fHtml=$(playerContainer).html();
						$(playerContainer).html('');
						$(playerContainer).html(fHtml);				   	
					} catch(e) {
				  //alert("error");
                  //console.debug(e);
					}
				});	
                     
                var $item = $('.an_c9-carousel-list ul li').eq(indx).find('a');
                $('.an_c9-carousel-content-block').hide();
                var remainder = (((rightIndex + 1) - indx) % 3);
                var offset = (remainder == 0) ? 3 : remainder;
                var bgPosition = ((3 * animationIncrement) - (offset * animationIncrement));
                var pointerPosition = vStartingBgPosition + bgPosition;
                $('.an_c9-item-highlight').css('left', bgPosition.toString() + 'px');
                $('.an_c9-item-highlight').show();
                $('.an_c9-carousel-nav-col2').css('background-position', pointerPosition.toString() + 'px 0px');
                $('#carousel-info').html($item.attr('rel'));
                $('#an_c9-content' + indx.toString()).show();

                curTitle = $item.attr('rel');

                handleNavButtons();
            };

            var handleNavButtons = function () {
                if (rightIndex == numItems - 1) {
                    disableButton($next);
                    enableButton($prev);

                } else if (rightIndex == 2) {
                    disableButton($prev);
                    enableButton($next);

                } else {
                    enableButton($next);
                    enableButton($prev);
                }
            };

            var startTimer = function () {
                timerInterval = setInterval(function() {
                  indx++;
                  if(indx >= numItems) indx = 0;
                  goToNextItem();
                }, 6000);
            };

            var cancelTimer = function () {
              //console.debug("timer has been cancelled");
              clearInterval(timerInterval);
              timerInterval = null;
            };

            var disableButton = function ($el) {
                $el.css('opacity', disabledBtnOpacity);
                $el.css('cursor', 'default');
                $el.closest('a').addClass('an_c9-disabled');
            };

            var enableButton = function ($el) {
                $el.css('opacity', '1.0');
                $el.css('cursor', 'pointer');
                $el.closest('a').removeClass('an_c9-disabled');
            };

            var goToNextItem = function () {
                // Pause Video Player
                try { jwplayer(window.currentJWPlay).pause(true); } catch(e) {}
            
                if (indx > rightIndex) {
                    disableButton($next);
                    $('.an_c9-item-highlight').hide();
                    $('.an_c9-carousel-list ul').animate({ left: '-=' + animationIncrement.toString() + 'px' }, animationSpeed, function () {
                        // Animation complete.
                        rightIndex = rightIndex + 1;
                        selectItem();
                    });
                } else if (indx == 0 && rightIndex >= 3) {
                  disableButton($prev);
                  $('.an_c9-item-highlight').hide();
                  $('.an_c9-carousel-list ul').animate({ left: '+=' + (animationIncrement*(numItems-3)).toString() + 'px' }, animationSpeed, function () {
                      // Animation complete.
                      rightIndex = 2;
                      selectItem();
                  });
                } else {
                    selectItem();
                }
            };

            if (numItems > 3) {
                $next.css('visibility', 'visible');
                $prev.css('visibility', 'visible');
                disableButton($prev);
                rightIndex = 2;
            }

            $('.an_c9-carousel-nav .next').click(function (e) {
                if( $(this).hasClass('an_c9-disabled') )
                  return false;
                e.preventDefault();
                cancelTimer();
                indx = (indx == numItems - 1) ? indx = 0 : rightIndex + 1;
                if (indx > rightIndex) {
                    disableButton($next);
                    $('.an_c9-carousel-list ul').animate({ left: '-=' + animationIncrement.toString() + 'px' }, animationSpeed, function () {
                        // Animation complete.
                        rightIndex = rightIndex + 1;
                        handleNavButtons();
                    });
                    $('.an_c9-item-highlight').animate({ left: '-=' + animationIncrement.toString() + 'px' }, animationSpeed);

                    var ccBgPos;
                    if ($('.ie').length) {
                        ccBgPos = $('.an_c9-carousel-nav-col2').css('background-position-x');
                    } else {
                        ccBgPos = $('.an_c9-carousel-nav-col2').css('background-position');
                    }

                    var ccBgPosVal = Number(ccBgPos.substring(0, ccBgPos.indexOf('px')));
                    var newCCPos = ccBgPosVal - animationIncrement;
                    $('.an_c9-carousel-nav-col2').animate({ backgroundPosition: newCCPos.toString() + 'px 0' }, animationSpeed);
                }
            });

            $('.an_c9-carousel-nav .prev').click(function (e) {
                if( $(this).hasClass('an_c9-disabled') )
                  return false;
                e.preventDefault();
                if ($prev.css('opacity') == disabledBtnOpacity) return;
                cancelTimer();
                indx = (indx == 0) ? numItems - 1 : rightIndex - 3;
                if (indx < rightIndex - 2 && indx >= 0) {
                    disableButton($prev);
                    $('.an_c9-carousel-list ul').animate({ left: '+=' + animationIncrement.toString() + 'px' }, animationSpeed, function () {
                        // Animation complete.
                        rightIndex = rightIndex - 1;
                        handleNavButtons();
                    });
                    $('.an_c9-item-highlight').animate({ left: '+=' + animationIncrement.toString() + 'px' }, animationSpeed);

                    var ccBgPos;
                    if ($('.ie').length) {
                        ccBgPos = $('.an_c9-carousel-nav-col2').css('background-position-x');
                    } else {
                        ccBgPos = $('.an_c9-carousel-nav-col2').css('background-position');
                    }

                    var ccBgPosVal = Number(ccBgPos.substring(0, ccBgPos.indexOf('px')));
                    var newCCPos = ccBgPosVal + animationIncrement;
                    $('.an_c9-carousel-nav-col2').animate({ backgroundPosition: newCCPos.toString() + 'px 0' }, animationSpeed);
                }
            });

            $('.an_c9-carousel-list ul li').click(function (e) {
                e.preventDefault();
                // Pause Video Player
                try { jwplayer(window.currentJWPlay).pause(true); } catch(e) {}

                cancelTimer();
                $('.an_c9-item-hover').hide();
                indx = Number($(this).find('a img').attr('value'));
                selectItem();
            });

            $('.an_c9-carousel-list ul li').hover(function (e) {
                $(this).find('.an_c9-item-hover').show();

                curTitle = $('#carousel-info').html();

                var itemTitle = $(this).find('a').attr('rel').toString();

                $('#carousel-info').html(itemTitle);

            }, function () {
                $('#carousel-info').html(curTitle);

                if (indx.toString() != $(this).find('a img').attr('value')) {
                    $(this).find('.an_c9-item-hover').hide();
                }
            });

            // FIX: if click inside carousel region, stop timer
            
            // IE 10 does not like click apparently, so use mousedown
            $('div.an_c9-jw-container').mousedown(function (e) {
                cancelTimer();
              }
            );

            $('div.an_c9-jw-container').click(function (e) {
                cancelTimer();
              }
            );
            // end fix
            
            if(numItems > 1) {
              selectItem();
              startTimer();
              $('.an_c9-carousel-nav').show().css('visibility', 'visible');
            } else {
              $('.an_c9-carousel-nav').hide().css('visibility', 'hidden');
              $('.an_c9-hero-carousel').addClass('hero-carousel-1item');
            }

            $('.jwVideo').each(function () {
                var $player = $(this);
                var $container = $player.parents('.an_c9-jw-container');
                var flashvars = {};
                flashvars.file = $player.attr('video');
                flashvars.image = $player.attr('image');
                flashvars.autostart = "false";
                flashvars.stretching = "exactfit";
                //flashvars.plugins = "tweetit-1,fbit-1&tweetit-1.position=top&tweetit-1.size=100&fbit-1.position=left&fbit-1.size=100";
                // flashvars.plugins = "tweetit-1,fbit-1";
                // flashvars.dock = "true";
                flashvars.skin = "http://www.pge.com/includes/swf/lulu.zip";
                flashvars.controlbar = "over"; // over provides hover appearance
                flashvars.icons = false; // comment this line to remove CTA button

                var params = {};
                params.menu = "false";
                params.allowfullscreen = "true";
                params.allowscriptaccess = "always";
                params.bgcolor = "#ffffff"
                params.wmode = "opaque";

                var attributes = {};
                attributes.id = $player.attr('embedID');
                attributes.name = $player.attr('embedID');

                swfobject.embedSWF("Unknown_83_filename"/*tpa=http://www.pge.com/includes/swf/player.swf*/, $player.attr('embedID'), $player.attr('videoWidth'), $player.attr('videoHeight'), "9.0.0.0", false, flashvars, params, attributes, flashLoaded);

                // comment these two line to remove CTA button
                //$('.video-play-button').css('margin-left', (Number($(this).attr('videoWidth')) - Number($('.video-play-button').attr('width'))) / 2 + "px");
                //$('.video-play-button').css('margin-top', (Number($(this).attr('videoHeight')) - Number($('.video-play-button').attr('height'))) / 2 + "px");

                function flashLoaded(e) {
                    if($player.data('hasEvents')) return false;
                    $player.data('hasEvents', true);
                    jwplayer(e.ref).onReady(function () {
                        $container.find('.video-button').hide();
                        //$container.find('.video-play-button').show();
                        //$container.find('.video-fb-button').show();
                        //$container.find('.video-twitter-button').show();

                        $container.find('.video-play-button a').click(function () {
                            jwplayer(e.ref).play();
                        });

                        $container.find('.video-replay-button a').click(function () {
                            jwplayer(e.ref).play();
                        });

                        $container.find('.video-fb-button a').attr('target','_blank').attr('href',
                          'http://www.facebook.com/sharer.php?u=' + encodeURIComponent(location.href) + '&t=' + encodeURIComponent(
                            $.trim( $('#carousel-info').text() ) 
                          )
                        );
                        $container.find('.video-twitter-button a').attr('target','_blank').attr('href',
                          'http://twitter.com/share?url=' + encodeURIComponent(location.href) + '&text=' + encodeURIComponent(document.title)
                        );
                        
                        var ctaText = $.trim( $container.find('.video-ctn-button').text() );
                        if(ctaText == '') $container.find('.video-ctn-button').remove();                   
                    });

                    jwplayer(e.ref).onPause(function () {
                        //$container.find('.video-play-button').show();
                        $container.find('.video-fb-button').show();
                        $container.find('.video-twitter-button').show();
                    }); 

                    jwplayer(e.ref).onPlay(function () {
                        $container.find('.video-button').hide();
                        window.currentJWPlay = $player.attr('embedID');
                        cancelTimer();
                    });

                    jwplayer(e.ref).onBuffer(function () {
                        $container.find('.video-button').hide();
                        window.currentJWPlay = $player.attr('embedID');
                        cancelTimer();
                    });

                    jwplayer(e.ref).onComplete(function () {
                        $container.find('.video-button').show();
                        $container.find('.video-play-button').hide();
                        $container.find('.video-fb-button').show();
                        $container.find('.video-twitter-button').show();

                        /*
                        $('.video-ctn-button').css('display', 'block');
                        $('.video-replay-button').css('display', 'block');
                        pge.log("flashLoaded: player complete");
                        $('.video-ctn-button').css('display', 'block');
                        $('.video-replay-button').css('display', 'block');
                        $('.video-fb-button').css('display', 'block');
                        $('.video-twitter-button').css('display', 'block');
                        */

                    });
                }
            });
            
            
        }
        
      // don't display the text if graphic title exists
   //   if ($('#imagetitle').size() > 0)
     //    $('#texttitle').hide();

      //  $('.an_c9-carousel-content-block input[type=text]').each(function () {
       //     var defaultText = $(this).val();
         //   $(this).focus(function () { if ($(this).val() == defaultText) $(this).val(''); });
           // $(this).blur(function () { if ($(this).val().length == 0) $(this).val(defaultText); });
     //   });
	 
	  // don't display the text if graphic title exists
      if ($('#imagetitle').size() > 0)
         $('#texttitle').hide();

        $('.carousel-content-block input[type=text]').each(function () {
            var defaultText = $(this).val();
            $(this).focus(function () { if ($(this).val() == defaultText) $(this).val(''); });
            $(this).blur(function () { if ($(this).val().length == 0) $(this).val(defaultText); });
        });

        if($('.carousel-list').length) {
            var numItems = $('.carousel-list ul li').length;
            var indx = 0;
			var prevIndx = indx;
            var rightIndex = 2;
            var animationSpeed = 500;
            var animationIncrement = 0;
            var $next = $('.carousel-nav .next span');
            var $prev = $('.carousel-nav .prev span');
            var disabledBtnOpacity = "0.2";
            var vStartingBgPosition = -306;
            var curTitle = "";
            var curDesc = "";
            var timer;
            var timerEnabled = true;
            
            var timerInterval = null;

            var $firstItem = $('.carousel-list ul li a img:first');
            if ($firstItem) {
                var margin = $firstItem.parent().parent().css('margin-right');
                animationIncrement = Number($firstItem.attr('width')) + Number(margin.substring(0, margin.length - 2));
                animationIncrement = 55;
            }

            var selectItem = function () {
                // Pause Video Player
                try { jwplayer(window.currentJWPlay).pause(true); } catch(e) {}
                
                var $item = $('.carousel-list ul li').eq(indx).find('a');
                $('.carousel-content-block').hide();
                var remainder = (((rightIndex + 1) - indx) % 3);
                var offset = (remainder == 0) ? 3 : remainder;
                var bgPosition = ((3 * animationIncrement) - (offset * animationIncrement));
                var pointerPosition = vStartingBgPosition + bgPosition;
                $('.item-highlight').css('left', bgPosition.toString() + 'px');
                $('.item-highlight').show();
                $('.carousel-nav-col2').css('background-position', pointerPosition.toString() + 'px 0px');
                $('#carousel-info').html($item.attr('rel'));
                $('#content' + indx.toString()).show();

                curTitle = $item.attr('rel');

                handleNavButtons();
            };

            var handleNavButtons = function () {
                if (rightIndex == numItems - 1) {
                    disableButton($next);
                    enableButton($prev);

                } else if (rightIndex == 2) {
                    disableButton($prev);
                    enableButton($next);

                } else {
                    enableButton($next);
                    enableButton($prev);
                }
            };

            var startTimer = function () {
                timerInterval = setInterval(function() {
                  indx++;
                  if(indx >= numItems) indx = 0;
                  goToNextItem();
                }, 6000);
            };

            var cancelTimer = function () {
              clearInterval(timerInterval);
              timerInterval = null;
            };

            var disableButton = function ($el) {
                $el.css('opacity', disabledBtnOpacity);
                $el.css('cursor', 'default');
                $el.closest('a').addClass('disabled');
            };

            var enableButton = function ($el) {
                $el.css('opacity', '1.0');
                $el.css('cursor', 'pointer');
                $el.closest('a').removeClass('disabled');
            };

            var goToNextItem = function () {
                // Pause Video Player
                try { jwplayer(window.currentJWPlay).pause(true); } catch(e) {}
            
                if (indx > rightIndex) {
                    disableButton($next);
                    $('.item-highlight').hide();
                    $('.carousel-list ul').animate({ left: '-=' + animationIncrement.toString() + 'px' }, animationSpeed, function () {
                        // Animation complete.
                        rightIndex = rightIndex + 1;
                        selectItem();
                    });
                } else if (indx == 0 && rightIndex >= 3) {
                  disableButton($prev);
                  $('.item-highlight').hide();
                  $('.carousel-list ul').animate({ left: '+=' + (animationIncrement*(numItems-3)).toString() + 'px' }, animationSpeed, function () {
                      // Animation complete.
                      rightIndex = 2;
                      selectItem();
                  });
                } else {
                    selectItem();
                }
            };

            if (numItems > 3) {
                $next.css('visibility', 'visible');
                $prev.css('visibility', 'visible');
                disableButton($prev);
                rightIndex = 2;
            }

            $('.carousel-nav .next').click(function (e) {
                if( $(this).hasClass('disabled') )
                  return false;
                e.preventDefault();
                cancelTimer();
				prevIndx=indx;
                indx = (indx == numItems - 1) ? indx = 0 : rightIndex + 1;
                if (indx > rightIndex) {
                    disableButton($next);
                    $('.carousel-list ul').animate({ left: '-=' + animationIncrement.toString() + 'px' }, animationSpeed, function () {
                        // Animation complete.
                        rightIndex = rightIndex + 1;
                        handleNavButtons();
                    });
                    $('.item-highlight').animate({ left: '-=' + animationIncrement.toString() + 'px' }, animationSpeed);

                    var ccBgPos;
                    if ($('.ie').length) {
                        ccBgPos = $('.carousel-nav-col2').css('background-position-x');
                    } else {
                        ccBgPos = $('.carousel-nav-col2').css('background-position');
                    }

                    var ccBgPosVal = Number(ccBgPos.substring(0, ccBgPos.indexOf('px')));
                    var newCCPos = ccBgPosVal - animationIncrement;
                    $('.carousel-nav-col2').animate({ backgroundPosition: newCCPos.toString() + 'px 0' }, animationSpeed);
                }
            });

            $('.carousel-nav .prev').click(function (e) {
                if( $(this).hasClass('disabled') )
                  return false;
                e.preventDefault();
                if ($prev.css('opacity') == disabledBtnOpacity) return;
                cancelTimer();
				prevIndx=indx;
                indx = (indx == 0) ? numItems - 1 : rightIndex - 3;
                if (indx < rightIndex - 2 && indx >= 0) {
                    disableButton($prev);
                    $('.carousel-list ul').animate({ left: '+=' + animationIncrement.toString() + 'px' }, animationSpeed, function () {
                        // Animation complete.
                        rightIndex = rightIndex - 1;
                        handleNavButtons();
                    });
                    $('.item-highlight').animate({ left: '+=' + animationIncrement.toString() + 'px' }, animationSpeed);

                    var ccBgPos;
                    if ($('.ie').length) {
                        ccBgPos = $('.carousel-nav-col2').css('background-position-x');
                    } else {
                        ccBgPos = $('.carousel-nav-col2').css('background-position');
                    }

                    var ccBgPosVal = Number(ccBgPos.substring(0, ccBgPos.indexOf('px')));
                    var newCCPos = ccBgPosVal + animationIncrement;
                    $('.carousel-nav-col2').animate({ backgroundPosition: newCCPos.toString() + 'px 0' }, animationSpeed);
                }
            });

            $('.carousel-list ul li').click(function (e) {
                e.preventDefault();
                // Pause Video Player
                try { jwplayer(window.currentJWPlay).pause(true); } catch(e) {}

                cancelTimer();
                $('.item-hover').hide();
                prevIndx=indx;
				indx = Number($(this).find('a img').attr('value'));
                selectItem();
            });

            $('.carousel-list ul li').hover(function (e) {
                $(this).find('.item-hover').show();

                curTitle = $('#carousel-info').html();

                var itemTitle = $(this).find('a').attr('rel').toString();

                $('#carousel-info').html(itemTitle);

            }, function () {
                $('#carousel-info').html(curTitle);

                if (indx.toString() != $(this).find('a img').attr('value')) {
                    $(this).find('.item-hover').hide();
                }
            });

            /*
            $('.hero-carousel').hover(function (e) {
                cancelTimer();
            }, function () {
                if( (timerInterval == null) && (numItems > 1) )
                  startTimer();
            });
            */
            
            if(numItems > 1) {
              selectItem();
              startTimer();
              $('.carousel-nav').show().css('visibility', 'visible');
            } else {
              $('.carousel-nav').hide().css('visibility', 'hidden');
              $('.hero-carousel').addClass('hero-carousel-1item');
            }

            $('.jwVideo').each(function () {
                var $player = $(this);
                var $container = $player.parents('.jw-container');
                var flashvars = {};
                flashvars.file = $player.attr('video');
                flashvars.image = $player.attr('image');
                flashvars.autostart = "false";
                flashvars.stretching = "exactfit";
                //flashvars.plugins = "tweetit-1,fbit-1&tweetit-1.position=top&tweetit-1.size=100&fbit-1.position=left&fbit-1.size=100";
                // flashvars.plugins = "tweetit-1,fbit-1";
                // flashvars.dock = "true";
                flashvars.skin = "http://www.pge.com/includes/swf/lulu.zip";
                flashvars.controlbar = "over"; // over provides hover appearance
                flashvars.icons = false; // comment this line to remove CTA button

                var params = {};
                params.menu = "false";
                params.allowfullscreen = "true";
                params.allowscriptaccess = "always";
                params.bgcolor = "#ffffff"
                params.wmode = "opaque";

                var attributes = {};
                attributes.id = $player.attr('embedID');
                attributes.name = $player.attr('embedID');

                swfobject.embedSWF("Unknown_83_filename"/*tpa=http://www.pge.com/resources/js/player.swf*/, $player.attr('embedID'), $player.attr('videoWidth'), $player.attr('videoHeight'), "9.0.0.0", false, flashvars, params, attributes, flashLoaded);

                // comment these two line to remove CTA button
                //$('.video-play-button').css('margin-left', (Number($(this).attr('videoWidth')) - Number($('.video-play-button').attr('width'))) / 2 + "px");
                //$('.video-play-button').css('margin-top', (Number($(this).attr('videoHeight')) - Number($('.video-play-button').attr('height'))) / 2 + "px");

                function flashLoaded(e) {
                    if($player.data('hasEvents')) return false;
                    $player.data('hasEvents', true);
                    jwplayer(e.ref).onReady(function () {
                        $container.find('.video-button').hide();
                        //$container.find('.video-play-button').show();
                        //$container.find('.video-fb-button').show();
                        //$container.find('.video-twitter-button').show();

                        $container.find('.video-play-button a').click(function () {
                            jwplayer(e.ref).play();
                        });

                        $container.find('.video-replay-button a').click(function () {
                            jwplayer(e.ref).play();
                        });

                        $container.find('.video-fb-button a').attr('target','_blank').attr('href',
                          'http://www.facebook.com/sharer.php?u=' + encodeURIComponent(location.href) + '&t=' + encodeURIComponent(
                            $.trim( $('#carousel-info').text() ) 
                          )
                        );
                        $container.find('.video-twitter-button a').attr('target','_blank').attr('href',
                          'http://twitter.com/share?url=' + encodeURIComponent(location.href) + '&text=' + encodeURIComponent(document.title)
                        );
                        
                        var ctaText = $.trim( $container.find('.video-ctn-button').text() );
                        if(ctaText == '') $container.find('.video-ctn-button').remove();                   
                    });

                    jwplayer(e.ref).onPause(function () {
                        //$container.find('.video-play-button').show();
                        $container.find('.video-fb-button').show();
                        $container.find('.video-twitter-button').show();
                    });

                    jwplayer(e.ref).onPlay(function () {
                        $container.find('.video-button').hide();
                        window.currentJWPlay = $player.attr('embedID');
                        cancelTimer();
                    });

                    jwplayer(e.ref).onBuffer(function () {
                        $container.find('.video-button').hide();
                        window.currentJWPlay = $player.attr('embedID');
                        cancelTimer();
                    });

                    jwplayer(e.ref).onComplete(function () {
                        $container.find('.video-button').show();
                        $container.find('.video-play-button').hide();
                        $container.find('.video-fb-button').show();
                        $container.find('.video-twitter-button').show();

                        /*
                        $('.video-ctn-button').css('display', 'block');
                        $('.video-replay-button').css('display', 'block');
                        pge.log("flashLoaded: player complete");
                        $('.video-ctn-button').css('display', 'block');
                        $('.video-replay-button').css('display', 'block');
                        $('.video-fb-button').css('display', 'block');
                        $('.video-twitter-button').css('display', 'block');
                        */

                    });
                }
            });
            
            
        }

        // intelligent auto-close-open-overlays (class name of "open" is used to control whether the overlay is 'open')
        $('body').klik(function (e) {
            var targ = e.target,
    				i = 0,
    				keepOpen,
    				list = pge.overlays.list;

            while (targ && ++i < 10) {
                for (var j in list) {
                    if (targ == list[j]) {
                        keepOpen = list[j];
                    }
                }
                targ = targ.parentNode;
            }

            for (var j in list) {
                if (keepOpen != list[j]) {
                    $(list[j]).removeClass('open'); // or, we could just .hide() it... or similar...
                }
            }
        });

        // global search box
        /**
        * inputHint $ plugin
        * @description: simulates default-text existing inside of <input>s by overlaying them with their <label>s
        *               (<label class="hint" for="input-id"> may be substituted by <div title="for:input-id">)
        *               http://remysharp.com/2007/03/19/a-few-more-$-plugins-crop-labelover-and-pluck/#labelOver
        * @param overClass: the className to apply to the input hint (label or div)
        */
        $.fn.inputHint = function (overClass) {
            return this.each(function () {
                var hint = $(this);
                // in this scope, "var f" means the element attribute "for"
                // .. "for" is a reserved word in the ECMA grammar
                var f = hint.attr('for') || hint.attr('title').split('for:').pop();
                if (f) {
                    var input = $('#' + f).data('input-hint', hint[0]);

                    this.hide = function () {
                        hint.css({ left: -9999 });
                    };

                    this.show = function () {
                        if (input.val() == '') {
                            hint.css({ left: 0 });
                        }
                    };

                    // handlers
                    input.focus(this.hide);
                    input.blur(this.show);
                    hint.addClass(overClass).click(function () { input.focus() });

                    if (input.val() != '') {
                        this.hide();
                    }
                }
            });
        };

        // apply plugin
        $('.input-hint').inputHint('hint-over');

        // no thanks links
        $('.no-thanks').klik(function () {
            var p = $(this).parent().parent();
            var div = p.parent().parent();
            if ($(this).parents('.carousel').size()) {
                p.find('p').css({ 'width': p.width() });
                p.animate({ 'width': 0, 'opacity': 0 }, 200, 'linear', function () {
                    p.remove();
                });
                // trigger the scroll_update now, so that if this is the last item, the list will scroll
                div.trigger('scroll_update', p);
            } else {
                p.parent().animate({ 'height': 0, 'opacity': 0 }, 200, 'linear', function () {
                    p.parent().remove();
                });
            }
            return false;
        });

        // pop new window for payment inserts
        $('.pop-inserts').klik(function () {
            window.open('http://www.pge.com/resources/js/i3-bill-payment-view-bill-insert.html', 'PacificGasElectricInserts', 'width=701,height=725,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,copyhistory=no,resizable=no');
            return false;
        });

        // pop new window for bill explanation
        $('.pop-explanation').klik(function () {
            window.open('http://www.pge.com/resources/js/i3-bill-payment-bill-explanation.html', 'PacificGasElectricBillExplanation', 'width=701,height=729,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,copyhistory=no,resizable=no');
            return false;
        });
        
        // Level 3 Sortable Content List
        $('.detail .content-list .sort-by select').bind('change', function() {
            var selector = $(this).val();
            $list = $(this).parents('.content-list').find('ul');
            $items = $list.children();
            $items.each(function(i, itema) {
              var current = $(this).find(selector).text();
              $list.children().each(function(j, itemb) {
                if(itema != itemb) {
                  if(current > $(this).find(selector).text())
                    $(itemb).insertBefore(itema);
                }
              });
              
            })
        });
        
        $('.detail .content-list .sort-by select').trigger('change');

        $('ul#main_navigation li').hover(function() {
            $(this).addClass('scriptfocus');
        }, function() {
            $(this).removeClass('scriptfocus');
        });
    },

    // keep track of open overlays so that we are able to intelligently auto-close open overlays
    overlays: {
        list: [],
        pushUnique: function (o) {
            var l = pge.overlays.list;
            for (var i in l) {
                if (l[i] == o) {
                    return;
                }
            }
            l.push(o);
        }
    }


};

// instantiate PGE object
pge.init();
