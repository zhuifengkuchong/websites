if (typeof PGE=="undefined"||!PGE){var PGE={};}
PGE.hybrid = {}; //for Module specific methods, vars, and objects

$(document).ready(function() {
	var pathName = window.location.pathname;

	// ixmi: MyEnergy Outage message
	// Not able to do a virtual include for the file below
	// in PGE_CollapseLogin.xsl; therefore, using ajax to get the data
	// and then appended to the outage div. This only applies to 
	// LiveSite pages
	if (pathName.indexOf('/en/myhome') >= 0 || pathName.indexOf('/en/safety') >= 0) {
		$.ajax({
			url : "http://www.pge.com/nots/login_myaccount/landing_login_home.html?mobile=no",
			success : function(data){
				$('#an_homep_outage').append(data);
				
				addOutageClass();
			}
		});
	} else {
		addOutageClass();
	}
	
	$('#login-header-tab-top-nav').click(function(){
		if ($('#login-form-tab-top-nav').hasClass('collapse')) {
			$('#login-form-tab-top-nav').removeClass('collapse');
			$('#login-header-tab-top-nav').find('a').removeClass('up-arrow');
			$('#an_homep_outage').show();
		} else {
			$('#login-form-tab-top-nav').addClass('collapse');
			$('#login-header-tab-top-nav').find('a').addClass('up-arrow');
			$('#an_homep_outage').hide();
		}
	});
	
	
	
	updateLabel('username', 'username', 'Username');
	updateLabel('password', 'password', 'Password');
	updateLabel('an_c2_search_address_input', 'an_c2_search_address_input', 'Search');
	
	// Active outages
	updateLabel('an_c2_search_address_input', 'an_c2_search_address_input', 'Enter street address, city or zip');
	updateLabel('rptOutYourName', 'rptOutYourName', 'First Last');
	
	// Enable login button if login button was disabled and user hits the back button on the browser from another window
	// or if users refreshes page and login button has been disabled
	$('#login-btn').attr('disabled', false); 
	
	// ixmi: NAV/UX CoC if page is not in the sitemap for myhome.segment or safety.segment then only show Return links
	if (($('#an_c12-sidebar_navigation').html() === '') && (pathName.indexOf('/en/myhome') >= 0 || pathName.indexOf('/en/safety') >= 0)) {
		var leftNav = '<ul><li><a href="../../index.htm"/*tpa=http://www.pge.com/*/>Return to Home Page</a></li><li><a href="javascript: history.back();">Return to Previous Page</a></li></ul>';
		$('#an_c12-sidebar_navigation').append(leftNav);
	}
	
	// ixmi: need to remove top border from components
	if (pathName.indexOf('http://www.pge.com/en/myhome/saveenergymoney/plans/smartrate/index.page') >= 0) {
		$('[id="pge_content-grid-2col-combo"]').css('border-top', 'none');
		$('#an_c17-title').css('border-bottom', '1px solid #CCCCCC');
		$('#an_c17-title').css('padding-bottom', '35px');
		$('#an_c17-title').css('width', '731px');
		
	} 
	
	// ixmi 04/17/14:  calculate the content with the largest height
	// and use the value to set the contet-grid's height
	if ($('#pge_l0-content-grid-3col').length > 0) {
		var height = 0;
			$('.pge_content').each(function(i, obj) {
				var h2 = $(this).height();
				if (height <= h2) {
					height = h2;
				}
			});
			
		$('#pge_content-block').css("height", height);
	}
	
	// ixmi 04/17/14: If alerts are displayed add a margin-top to the promo modules
	if ($('.level0-alerts').length > 0) {
		$('#pge_l0-promo-3col-modules').css('margin-top', 10 + 'px');
	}
	
		
	// ixmi 04/17/14: display warning message on homepage to users using a IE 7 or less.
	// this message will also be displayed if the user has compatibility view on
	var ua = window.navigator.userAgent;
	var msie = ua.indexOf("MSIE ");
			
	var version = parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)));
	if (msie > 0 && version < 8) {
		if (pathName === '/' || pathName === 'http://www.pge.com/en/myhome/mobile.page') {
			if (pathName === 'http://www.pge.com/en/myhome/mobile.page') {
				$('#an_global_header').before('<div id="pge_ie-compatability" style="height: 30px; font-family: Arial; width: 980px; position: relative; color: rgb(0, 0, 0); background: none repeat scroll 0% 0% rgb(255, 255, 225);"> <p style="font-size: 12px; padding-top: 5px; padding-right: 5px; padding-left: 10px;">It looks like your browser has Compatibility Mode turned on. To have the best experience on our site, we recommend turning off Compatibility Mode. <a href="#" id="pge_close-ie-comp"><span style="float: right;" class="ui-icon ui-icon-closethick">close</span></a></p></div>)');
			}
		
			$('#pge_ie-compatability').show();
			$('#login-form-tab-top-nav').css('top', 120 + 'px');
			$('#an_c2-glb-hdr-tab-nav').css('top', 67 + 'px');
		}
	}
		// Meta tags		
		var mt = $('meta[http-equiv=X-Frame-Options]');
        mt = mt.length ? mt : $('<meta http-equiv="X-Frame-Options" />').appendTo('head');
        mt.attr('content', 'deny');
				
	$('#pge_close-ie-comp').live( "click", function() {
		$('#pge_ie-compatability').hide();
		
		$('#login-form-tab-top-nav').css('top', 90 + 'px');
		$('#an_c2-glb-hdr-tab-nav').css('top', 40 + 'px');
	});
	
	
	if ($("#resultstable").length) {
		$("#resultstable").tablesorter();
	}
	
	if ($(".ecsearch").length) {
		$('#ls-gen8-ls-area-body').css('border-top', '6px solid #d8d8d8');
	}
	
	// SmartAC mobile page
	if ($('#an_c46-glb-hdr-toolbar').is(':hidden')) {
		$('#brightcove_player').hide();
		$('#brightcove_player_mobile').show();
		
		$('#smartac-hero').hide();
		$('#smartac-hero-mobile').show();
		
		$('.an_c46-logo').attr('href', 'https://m.pge.com/');
	}
	
	// BEGIN: AllConnect
	if ($('#pge_allconnect-form').length > 0) {
		updateLabel('street_address', 'lblstreet_address', $("#lblstreet_address").text());
		updateLabel('unit_num', 'lblunit_num', $("#lblunit_num").text());
		updateLabel('zip_code', 'lblzip_code', $("#lblzip_code").text());
	
		$('#btn-submit-allconnect').click(function(){
			var streetAddress = $('#street_address').val();
			var unitNum = $('#unit_num').val();
			var zipCode = $('#zip_code').val();
			var service = $('#service').val();
			
			var action = '';
			if (unitNum != '')
				action = '?streetAddress=' + streetAddress + '&unitNum=' + unitNum + '&zipCode=' + zipCode + '&service=' + service;
			else 
				action = '?streetAddress=' + streetAddress + '&zipCode=' + zipCode + '&service=' + service;
			
			$("#pge_allconnect-form").attr("action", "http://www.pge.com/en/myhome/saveenergymoney/financialassistance/care/enrollrecertify/index.page" + action);
		});
	}
	// END: AllConnect
	
	// BEGIN: Local pages
	$('#btn-submit-localsignup').click(function () {	
		var array = parent.window.location.pathname.split('/');
		var page = array[array.length-2];
		$('#pge_division').val(page);
	});
	
	$('#pge_localdivision').on('change', function() {
		var division = $(this).val();
		var array = pathName.split('/');
		var page = array[array.length-2];
		
		if ( (page != division) && (division != 'none')) {
			var host = window.location.host;
			var path = $('#pge_division-path').val();
			
			window.location.href = 'http:///' + host + '/en/' + path + '/' + division + '/index.page';
		}
	});
	
	var lblText = $("#lblemail").text();
	updateLabel('localsignup_sender_email', 'localsignup_sender_email', lblText);
	
	if ($('#pge_cm-article-noleftnav').length > 0) {
		(function(d,s,i,r) {
			if (d.getElementById(i)){return;}
			var n=d.createElement(s),e=d.getElementsByTagName(s)[0];
			n.id=i;n.src='../../../s7.addthis.com/js/250/addthis_widget.js-ver=2.5.js#username=pgeperspectives'/*tpa=http://s7.addthis.com/js/250/addthis_widget.js?ver=2.5#username=pgeperspectives*/;
			e.parentNode.insertBefore(n, e);
		})(document,"script","addthis",300000);
	}
	
	if ($('#pge_cm-article-noleftnav').length > 0) {
		var array = pathName.split('/');
		var page = array[array.length-1];
		if (page === 'http://www.pge.com/resources/js/index.page') {
			$('#pge_cm-summary').show();
		} else {
			$('#pge_cm-summary').show();
			$('#pge_cm-article').show();
		}
	}
	
	$('.pge_cg-300x184-noleftnav-text').each(function() {
		var html = $(this).html();
		if (html.length > 160) {			
			if( html.charAt(161) === ' ') {
				html = html.substring(0, 160);
			} else {
				var i = 160;
				var ch = html.charAt(i);
				while (ch != ' ') {
					i--;
					ch = html.charAt(i);
				}
			}
			
			html = html.substring(0, i) + '...';
			$(this).html(html)
		}
	});
	// END: Local pages
});

function checkLogin() {
	if( $("#username").val() == '' ){
		alert( 'Please enter a valid user name' );
		return false;
	}
	
	if( $("#password").val() == '' ){
		alert( 'Please enter a valid password' );
		return false;
	}
	
	// disable login button after user/password have been entered
	$('#login-btn').attr('disabled', true);
	$('#login-btn').removeClass('an_c55_submit-btn');
	$('#login-btn').addClass('an_c55_submit-btn-disable');
	return true;
} 

function checkCCOLogin() {
	if( $("#ccoUserName").val() == '' ){
		alert( 'Please enter a valid user name' );
		return false;
	}
	
	if( $("#ccoPassword").val() == '' ){
		alert( 'Please enter a valid password' );
		return false;
	}
	
	return true;
} 

function addOutageClass() {
	var outage=$('#an_homep_outage').html();
	if($.trim(outage)) {
		$('#an_homep_outage').addClass('an_homep_outage');
		
		// If MyAccount is expanded then show the outage message
		// this is the case for pge.com landing page
		if (!$('#login-form-tab-top-nav').hasClass('collapse')) {
			$('#an_homep_outage').show();
		}
	} 
	
}


function updateLabel (inputId, labelId,labelValue) {
	var input=$("input[id='"+inputId+"']");
	var label=$("label[for='"+labelId+"']");
		$(input).live( "click",function() {
		$(label).html('');
	});
	
	$(input).focus( function() {
		$(label).html('');
	});
	
	$(input).focusout( function(event) {
		var srchValue=$(this).val();
		if(srchValue=='') {
			$(label).html(labelValue);
		}
		event.stopPropagation();
	});
	
	/** If input is not empty then dont show label **/
	if($(input).val() !='') {
		$(label).html('');
	}
} 

(function ($) {

	var ua = navigator.userAgent;
	var ie = /MSIE/.test(ua) && ua.replace(/.*?MSIE (.).*/,"$1");
	var ff = /Firefox/.test(ua) && ua.replace(/.*?Firefox\/(.)\.(.).*/,"$1_$2");
	var safari = /Safari/.test(ua) && ua.replace(/.*?Version\/(.).*/,"$1");
	var chrome = /Chrome/.test(ua) && ua.replace(/.*?Chrome\/(.).*/,"$1");
	var opera = /Opera/.test(ua) && ua.replace(/.*?Opera\/(.).*/,"$1");
	document.getElementsByTagName("html")[0].className="js"+(ie?" ie ie"+ie : ff?" ff ff"+ff : chrome?" chrome chrome"+chrome : safari?" safari safari"+safari : opera?" opera opera"+opera : "");

	/*************************************
	PNG Fix
	*************************************/
	PGE.hybrid.pngFix = function () {
		DD_belatedPNG.fix('.png_fix');
	};
	
	
	/************************************* 	HYBRID - ACCORDION 	*************************************/
    PGE.hybrid.initAccordions = function () {
		//alert("init");
        // accomodate non-JS users: by default all accordian elements open, then use JS to hide all but the first
        $('.an_c23-accordion div.an_c23_accordion_hdr').each(function(index) {$(this).removeClass('an_c23-hdr-closed-wide an_c23-hdr-open-wide')});
		$('.an_c23-accordion div.an_c23_accordion_hdr').each(function(index) {if (index==0) $(this).addClass('an_c23-hdr-open')});
		$('.an_c23-accordion div.an_c23_accordion_hdr').each(function(index) {if (index!=0) $(this).removeClass('an_c23-hdr-open an_c23-hdr-open-wide')});
        $('.an_c23-accordion div.an_c23-content').each(function(index) {if (index!=0) $(this).removeClass('open')});
		
		
        $('.an_c23-accordion li.an_c23-expand-all').click(function () {
			//A3AD 05/05/2014 Open the accordions in that section only 
			$(this).parent().parent().find('.an_c23-content').addClass('open').removeClass('close');
			$(this).parent().parent().find('.an_c23_accordion_hdr').addClass('an_c23-hdr-open').removeClass('an_c23-hdr-closed an_c23-hdr-closed-wide');
            return false;
        });
        $('.an_c23-accordion li.an_c23-collapse-all').click(function () {
			//A3AD 05/05/2014 Close the accordions in that section only 
			$(this).parent().parent().find('.an_c23-content').addClass('close').removeClass('open').css('display','none');
            $(this).parent().parent().find('.an_c23_accordion_hdr').addClass('an_c23-hdr-closed').removeClass('an_c23-hdr-open an_c23-hdr-closed-wide');
            return false;
        });
        /*04152013*/
        $('.an_c23-accordion .an_c23_accordion_hdr').click(function () {
			$(this).next('div.an_c23-content').slideToggle(300).toggleClass('open');
			//alert($(this).Class);
            if ($(this).hasClass('an_c23-hdr-open')) {
				//alert("test is open");
                $(this).addClass('an_c23-hdr-closed').removeClass('an_c23-hdr-open');
            } else {
				//alert("test is closed");
                $(this).addClass('an_c23-hdr-open').removeClass('an_c23-hdr-closed');
            }
            
            return false;
        });

	
        /* 11/12/13: Added by Swaran - for Accordion in CoC Contact Us page*/
        $('.pge-accordion h3.pge-hdr-open').each(function(index) {if (index!=0) $(this).removeClass('pge-hdr-open')});
        $('.pge-accordion div.an_c25-content').each(function(index) {if (index!=0) $(this).removeClass('open')});

        $('.pge-accordion .pge_accordion_hdr').click(function () {
			$(this).next('div.an_c25-content').slideToggle(300).toggleClass('open');
			//alert($(this).Class);
            if ($(this).hasClass('pge-hdr-open')) {
				//alert("test is open");
                $(this).addClass('pge-hdr-closed').removeClass('pge-hdr-open');
            } else {
				//alert("test is closed");
                $(this).addClass('pge-hdr-open').removeClass('pge-hdr-closed');
            }
            
            return false;
        });
    };
	
	
	
	
}($));
