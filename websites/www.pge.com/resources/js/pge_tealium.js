window.onload = function(){
	//Page name
	var tealium_page_name = '';
	var tealium_page_title = '';
	function pageNameF() {
		$( "h1" ).each(function( index ) {
			if(!$(this).hasClass( "replace" )) {
				if(!$(this).hasClass( "logo" )) {
					var t = $(this).clone();
					t.find("br").replaceWith(" ");
					tealium_page_name = t.text();
					//tealium_page_name = $(this).text();
				}
			}
		});
		//If no h1, use document title
		if(!tealium_page_name || tealium_page_name == "\xa0"){
			tealium_page_name = pageTitleF();
		}
		//If no h1 and no document title, use the first h2
		if(!tealium_page_name || tealium_page_name == "false"){
			if($( "h2:first" ).text())
				tealium_page_name = $( "h2:first" ).text();
			else
				return false;
		}
		return tealium_page_name;
		
	}
	
	//Page title
	function pageTitleF() {
		if($("meta[name=title]").attr("content")) {
			return $("meta[name=title]").attr("content");
		}
		else if (document.title) {
			return document.title;
		}
		else if (tealium_page_name) {
			return pageNameF();
		}
		else {
			return false;
		}
	}

	//Page class
	var tealium_page_class = '';
	if ($(location).attr('pathname') == "" || $(location).attr('pathname') == "/") {
		tealium_page_class = "PG&E Home Page";
	}
	else if (window.location.href.indexOf("cn/mybusiness") > -1) {
		tealium_page_class = "Chinese My Business";
	}
	else if (window.location.href.indexOf("http://www.pge.com/resources/js/cn/myhome/index.page") > -1) {
		tealium_page_class = "Chinese Language Home Page";
	}
	else if (window.location.href.indexOf("cn/myhome") > -1) {
		tealium_page_class = "Chinese My Home";
	}
	else if (window.location.href.indexOf("cn/about") > -1) {
		tealium_page_class = "Chinese About";
	}
	else if (window.location.href.indexOf("es/mybusiness") > -1) {
		tealium_page_class = "Spanish My Business";
	}
	else if (window.location.href.indexOf("http://www.pge.com/resources/js/es/myhome/index.page") > -1) {
		tealium_page_class = "Spanish Language Home Page";
	}
	else if (window.location.href.indexOf("es/myhome") > -1) {
		tealium_page_class = "Spanish My Home";
	}
	else if (window.location.href.indexOf("es/about") > -1) {
		tealium_page_class = "Spanish About";
	}
	else if (window.location.href.indexOf("pipeline") > -1) {
		tealium_page_class = "Pipeline";
	}
	else if (window.location.href.indexOf("http://www.pge.com/resources/js/en/mybusiness/index.page") > -1) {
		tealium_page_class = "My Business Home Page";
	}
	else if (window.location.href.indexOf("http://www.pge.com/resources/js/en/b2b/index.page") > -1) {
		tealium_page_class = "B2B Home Page";
	}
	else if (window.location.href.indexOf("http://www.pge.com/resources/js/en/about/index.page") > -1) {
		tealium_page_class = "About Home Page";
	}
	else if (window.location.href.indexOf("http://www.pge.com/resources/js/en/safety/index.page") > -1) {
		tealium_page_class = "Safety Home Page";
	}
	else if (window.location.href.indexOf("http://www.pge.com/resources/js/search/index.page") > -1) {
		tealium_page_class = "Search Results Page";
	}
	else if (window.location.href.indexOf("myhome") > -1) {
		tealium_page_class = "My Home";
	}
	else if (window.location.href.indexOf("mybusiness") > -1) {
		tealium_page_class = "My Business";
	}
	else if (window.location.href.indexOf("safety") > -1) {
		tealium_page_class = "Safety";
	}
	else if (window.location.href.indexOf("b2b") > -1) {
		tealium_page_class = "B2B";
	}
	else if (window.location.href.indexOf("about") > -1) {
		tealium_page_class = "About";
	}

	//Search Term
	var tealium_search_term = '';
	if (window.location.href.indexOf("search") > -1)
	{
		var urlParams;
		(window.onpopstate = function () {
			var match,
				pl     = /\+/g,  // Regex for replacing addition symbol with a space
				search = /([^&=]+)=?([^&]*)/g,
				decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
				query  = window.location.search.substring(1);

			urlParams = {};
			while (match = search.exec(query))
			   urlParams[decode(match[1])] = decode(match[2]);
		})();
		tealium_search_term = urlParams["query"];
	}

	//utag
  window.utag_data = {
	'page_type' : 'http://www.pge.com/resources/js/Pge.com',
	'page_class' : tealium_page_class,
	'page_name' : pageNameF(),
	'page_title' : pageTitleF(),
	'content_group' : '',
	'content_subgroup' : '',
	'pge_accountid' : '',
	'ldap_user_id' : '',
	'pge_serviceagmt_id' : '',
	'site_mobile' : '',
	'search_term' : tealium_search_term,
	'search_results' : '',
	'video_name' : '',
	};
	
	var envT = "prod";
	if($(location).attr('hostname').indexOf("wwwextqa") >= 0) {
		envT = "qa";
	}
	else if ($(location).attr('hostname').indexOf("wwwextdev") >= 0) {
		envT = "dev";
	}
	else if ($(location).attr('hostname').indexOf("wwwexttest") >= 0 || $(location).attr('hostname').indexOf("wwwexttst") >= 0) {
		envT = "test";
	}


//Universal Tag Code Snippet
var teaScript = document.createElement("script");
teaScript.type = "text/javascript";
teaScript.text = "(function(a,b,c,d){ \
a='//tags.tiqcdn.com/utag/pge/main/" + envT + "/utag.js'; \
b=document;c='script';d=b.createElement(c);d.src=a;d.type='text/java'+c;d.async=true; \
a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a); \
})(); ";
var tealiumBodyFirst = document.body.firstChild;
tealiumBodyFirst.parentNode.insertBefore(teaScript, tealiumBodyFirst);



}
//});