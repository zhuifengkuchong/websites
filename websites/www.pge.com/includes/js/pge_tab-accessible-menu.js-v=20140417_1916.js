$(document).ready(function() {	
	$('.acc-nav').setup_navigation();
	
	var pathName = window.location.pathname;
	if (pathName .toLowerCase().indexOf("/safety/") >= 0)
	{
		$( '[name=safetyHeader]' ).css({"background-color":"#FFA100"});
		$( '[name=safetyHeader]' ).css({"color":"#FFFFFF"});
		$( '[name=safetyHeader]' ).css({"background-image":"none"});
		$( '[name=safetyHeader]' ).css({"background-repeat": "no-repeat"});
	}
	
	$( '.active1' ).css({"background-color":"#FFA100"});
	$( '.active1' ).css({"color":"#FFFFFF"});
	$( '.active1' ).css({"background-image":"none"});
	$( '.active1' ).css({"background-repeat": "no-repeat"});

	
	if (pathName.indexOf('/myhome') >= 0 || pathName.indexOf('/safety') >= 0)
	$('#an_c2-active-nav-item').prev().find(">:first-child").css("background-image", "none");
	
	
	var activeClass = 'pge_active-nav-item';
	 // Highlight selected item from dropdown in myhome
	if (pathName.indexOf('/myhome/myaccount') >= 0) {
		$('#an_c2-glb-hdr-tab-nav').children('li').each(function() {
			var link = $(this).find(">:first-child");
			var href = $(link).attr('href');
			if (href.indexOf('/myaccount') >= 0) {
				$(link).addClass(activeClass);
				$(this).prev().find('a').css("background-image", "none")
				return false;
			}
		});
	} else if (pathName.indexOf('/myhome/servicerequests') >= 0) {
		$('#an_c2-glb-hdr-tab-nav').children('li').each(function() {
			var link = $(this).find(">:first-child");
			var href = $(link).attr('href');
			if (href.indexOf('/servicerequests') >= 0) {
				$(link).addClass(activeClass);
				$(this).prev().find('a').css("background-image", "none")
				return false;
			}
		});
	} else if (pathName.indexOf('/myhome/outages') >= 0) {
		$('#an_c2-glb-hdr-tab-nav').children('li').each(function() {
			var link = $(this).find(">:first-child");
			var href = $(link).attr('href');
			if (href.indexOf('/outages') >= 0) {
				$(link).addClass(activeClass);
				$(this).prev().find('a').css("background-image", "none")
				return false;
			}
		});
	} else if (pathName.indexOf('/myhome/saveenergymoney') >= 0) {
		$('#an_c2-glb-hdr-tab-nav').children('li').each(function() {
			var link = $(this).find(">:first-child");
			var href = $(link).attr('href');
			if (href.indexOf('/saveenergymoney') >= 0) {
				$(link).addClass(activeClass);
				$(this).prev().find('a').css("background-image", "none")
				return false;
			}
		});
	} else if (pathName.indexOf('/myhome/addservices') >= 0) {
		$('#an_c2-glb-hdr-tab-nav').children('li').each(function() {
			var link = $(this).find(">:first-child");
			var href = $(link).attr('href');
			if (href.indexOf('/addservices') >= 0) {
				$(link).addClass(activeClass);
				$(this).prev().find('a').css("background-image", "none")
				return false;
			}
		});
	}
	
	// ixmi: CoC/NAVUX-Emergency link clicked on header of all pages in myhome and safety segments
	$('#an_c46-glb-hdr-primary-nav li').click(function(e) {
		if ($(this).find('a').text().toLowerCase() === "emergency") {
			$(this).find('a').attr('href', '#');
			openEmergencyDialog();
		}
		
	});
	
	// ixmi: CoC/NAVUX-Emergency link clicked on pge.com landing page
	$('#pge_emergency_link').click(function(e) {
			openEmergencyDialog();		
	});
	
	// CoC  MyAccount and search
	updateLabel('username', 'username', 'Username');
	updateLabel('password', 'password', 'Password');
	updateLabel('global-l0-search', 'global-l0-search', 'Search');
	
	// Active Outages
	updateLabel('an_c2_search_address_input', 'an_c2_search_address_input', 'Enter street address, city or zip');
	updateLabel('rptOutYourName', 'rptOutYourName', 'First Last');
});

function updateLabel (inputId, labelId,labelValue) {
	var input=$("input[id='"+inputId+"']");
	var label=$("label[for='"+labelId+"']");
		$(input).live( "click",function() {
		$(label).html('');
	});
	
	$(input).focus( function() {
		$(label).html('');
	});
	
	$(input).focusout( function(event) {
		var srchValue=$(this).val();
		if(srchValue=='') {
			$(label).html(labelValue);
		}
		event.stopPropagation();
	});
	
	/** If input is not empty then dont show label **/
	if($(input).val() !='') {
		$(label).html('');
	}
} 

/* Open Emergency dialog
*/
function openEmergencyDialog() {
    var emergencyHtml = '<div id="pge_emergency-dialog"><div id="pge_emergency-notice">If you smell natural gas, see downed powerlines, or suspect another emergency situation, leave the area immediately and then call 9-1-1 or PG&amp;E at 1-800-743-5000.</div><div id="pge_emergency-info">24-hour Customer Service Line: <span>800-PGE-5000</span>.<br />24-hour Power Outage Line: <span>800-PGE-5002</span>.</div></div>';
    
	if ($('.pge_emergency-dialog').length > 0) {
		$('.pge_emergency-dialog').dialog('close');
	}
   // AN.openDialog(emergencyHtml,"Emergency",155,365,true,'pge_emergency-dialog');
	
	var dialogHtml = '<div class="dialogBox" title="Emergency">' + emergencyHtml + '</div>';

	$('body:first').append(dialogHtml);
	$("body > .dialogBox:last").dialog({
		modal: true,
		height: 155,
		width: 375	,
		resizable : false,
		dialogClass: 'pge_emergency-dialog',
		open: function(event, ui) {
			if ($.browser.safari == true){
				window.setTimeout(function() {
					jQuery(document).unbind('mousedown.dialog-overlay');
					jQuery(document).unbind('mouseup.dialog-overlay');
				}, 100);
			}
		},
		close : function(e) {
			$(this).remove();
		}
	}); 
}

function checkLogin() {
	if( $("#username").val() == '' ){
		alert( 'Please enter a valid user name' );
		return false;
	}
	
	if( $("#password").val() == '' ){
		alert( 'Please enter a valid password' );
		return false;
	}
	
	// disable login button after user/password have been entered
	$('#login-btn').attr('disabled', true);
	$('#login-btn').removeClass('an_c55_submit-btn');
	$('#login-btn').addClass('an_c55_submit-btn-disable');
	return true;
}

var keyCodeMap = {
	48:"0", 49:"1", 50:"2", 51:"3", 52:"4", 53:"5", 54:"6", 55:"7", 56:"8", 57:"9", 59:";",
	65:"a", 66:"b", 67:"c", 68:"d", 69:"e", 70:"f", 71:"g", 72:"h", 73:"i", 74:"j", 75:"k", 76:"l",
	77:"m", 78:"n", 79:"o", 80:"p", 81:"q", 82:"r", 83:"s", 84:"t", 85:"u", 86:"v", 87:"w", 88:"x", 89:"y", 90:"z",
	96:"0", 97:"1", 98:"2", 99:"3", 100:"4", 101:"5", 102:"6", 103:"7", 104:"8", 105:"9"
}

 /**
  * function setup_navigation
  *
  */
$.fn.setup_navigation = function(settings) {

	settings = jQuery.extend({
	menuHoverClass: 'over'
	}, settings);

  var top_level_links = $(this).find('> li > a');
  var tabIndex;
	//$(top_level_links).attr('tabindex','0');
        $(top_level_links).each(function(index, item) {
            tabIndex = index+2;
   	    $(item).attr('tabindex', ''+tabIndex);
        });

  // 2 callback functions: one when the pointer moves over the item, and one when it leaves
  $('li.an_header_link').hover(
    // hover over
    function(){
      $(this).addClass(settings.menuHoverClass).children('div').children('ul').attr('aria-hidden', 'false');
    },
    // hover out
    function(){
		  $(this).closest('ul')
      .find('.'+settings.menuHoverClass)
      .removeClass(settings.menuHoverClass)
      .find('.an_c2-tertiary-nav')
      .children('ul')
      .attr('aria-hidden', 'true');
    }
  );
   /**
     * IE 7 fix for click on the menu to work 
     *
     **/
  $('li.an_header_link').live("click",
    // hover over
    function(event){
      $(this).addClass(settings.menuHoverClass).children('div').children('ul').attr('aria-hidden', 'false');
     event.stopPropagation();
    }
  );

	//$(top_level_links).hover(
  /*$('li.an_c46-nav').hover(
    function(){
		  $(this).closest('ul')
      .find('.'+settings.menuHoverClass)
      .removeClass(settings.menuHoverClass)
      .find('.an_c2-tertiary-nav')
      .children('ul')
      .attr('aria-hidden', 'true');

      $(this).next('div')
      .parent('li')
      .addClass(settings.menuHoverClass)
      .children('div')
      .children('ul')
      .attr('aria-hidden', 'false');
	  }
  );*/

	$(top_level_links).focus(function(){
		$(this).closest('ul')
		.find('.'+settings.menuHoverClass)
		.removeClass(settings.menuHoverClass)
		.find('.an_c2-tertiary-nav')
		.children('ul')
		.attr('aria-hidden', 'true');
		
		$(this).next('div')
		.parent('li')
		.addClass(settings.menuHoverClass)
		.children('div')
		.children('ul')
		.attr('aria-hidden', 'false');
	});

	// Bind arrow keys for navigation
	$(top_level_links).keydown(function(e){
		if(e.keyCode == 37) { // left arrow
			e.preventDefault();
			// This is the first item
			if($(this).parent('li').prev('li').length == 0) {
				$(this).parents('ul').find('> li').last().find('a').first().focus();
			} else {
				$(this).parent('li').prev('li').find('a').first().focus();
			}
		} else if(e.keyCode == 38) { // up arrow
			e.preventDefault();
			if($(this).parent('li').find('ul').length > 0) {
				$(this).parent('li').find('ul')
				.attr('aria-hidden', 'false')
				.addClass(settings.menuHoverClass)
				.find('a')
				.last().focus();
			}
		} else if(e.keyCode == 39)  {// right arrow
			e.preventDefault();
			// This is the last item
			if($(this).parent('li').next('li').length == 0) {
				$(this).parents('ul').find('> li').first().find('a').first().focus();
			} else {
				$(this).parent('li').next('li').find('a').first().focus();
			}
		} else if(e.keyCode == 40) { // down arrow
			e.preventDefault();
			if($(this).parent('li').find('ul').length > 0) {
				$(this).parent('li').find('ul')
				.attr('aria-hidden', 'false')
				.addClass(settings.menuHoverClass)
				.find('a')
				.first().focus();
			}
		} else if(e.keyCode == 9) { // tab	
			// tabbing out of topnav
			if($(this).parent('li').next('li').length == 0 || $(this).parent('li').prev('li').length == 0) {
				$(this).closest('ul')
				.find('.'+settings.menuHoverClass)
				.removeClass(settings.menuHoverClass)
				.find('.an_c2-tertiary-nav')
				.children('ul')
				.attr('aria-hidden', 'true');
			}
		} else {
			$(this).parent('li').find('ul[aria-hidden=false] a').each(function(){
				if($(this).text().substring(0,1).toLowerCase() == keyCodeMap[e.keyCode]) {
					$(this).focus();
					return false;
				}
			});
		}
	});
	
	var second_level_links = $(top_level_links).parent('li').find('ul').find('a');
	
	$(second_level_links).keydown(function(e){
		if(e.keyCode == 38) { // up arrow
		e.preventDefault();
		// This is the first item
		if($(this).parent('li').prev('li').length == 0) {
			$(this).parents('ul').parents('li').find('a').first().focus();
		} else {
			$(this).parent('li').prev('li').find('a').first().focus();
		}
	} else if(e.keyCode == 40) { // down arrow
		e.preventDefault();
		if($(this).parent('li').next('li').length == 0) {
			$(this).parents('ul').parents('li').find('a').first().focus();
		} else {
			$(this).parent('li').next('li').find('a').first().focus();
		}
	} else if(e.keyCode == 32) { //spacebar
		e.preventDefault();
		window.location = $(this).attr('href');
	} else if(e.keyCode == 9) { // tab
		$(this)
		.closest('ul')
		.attr('aria-hidden', 'true')
		.closest('li')
		.removeClass(settings.menuHoverClass);
	
		if ($(this).closest('div').parent().next('li').length > 0) {
			$(this).closest('div').parent().next('li')
			.addClass(settings.menuHoverClass)
			.children('div')
			.children('ul')
			.attr('aria-hidden', 'false');
			
			$(this).closest('div')
			.parent()
			.find('a')
			.focus();
		}
	} else {
		var found = false;
		$(this).parent('li').nextAll('li').find('a').each(function(){
			if($(this).text().substring(0,1).toLowerCase() == keyCodeMap[e.keyCode]) {
				$(this).focus();
				found = true;
				return false;
			}
		});
		if(!found) {
			$(this).parent('li').prevAll('li').find('a').each(function(){
			if($(this).text().substring(0,1).toLowerCase() == keyCodeMap[e.keyCode]) {
				$(this).focus();
				return false;
			}
			});
		}
	}
	});

	
	// Hide menu if click or focus occurs outside of navigation
	$(this).find('a').last().keydown(function(e){
		if(e.keyCode == 9) { // tab
		// If the user tabs out of the navigation hide all menus
		$('.'+settings.menuHoverClass)
		.attr('aria-hidden', 'true')
		.removeClass(settings.menuHoverClass);
		}
	});
	
	$(document).click(function(){ 
		$('.'+settings.menuHoverClass)
		.removeClass(settings.menuHoverClass)
		.children('div')
		.children('ul')
		.attr('aria-hidden', 'true');
	});
    	 $("#an_c2-glb-hdr-tab-nav li").mouseout(function(){
      $(this).removeClass("over");
    }).mouseover(function(){
      $(this).addClass("over");
    });
	$(document).click(function (event) {
         if ($(event.target).parent().parent().attr('id')==="change-Sel") {
            $("#change-Sel ul").toggle();
        } else {
            $("#change-Sel ul").hide();
        }
    });
        $(document).click(function (event) {
 	if ($(event.target).parent().parent().attr('id')==="change-lang") {
            $("#change-lang ul").toggle();
        } else {
            $("#change-lang ul").hide();
        }
    });
	$('#resultstable tr:odd').addClass('an_odd');
		$(this).click(function(e){
		e.stopPropagation();
	});
}