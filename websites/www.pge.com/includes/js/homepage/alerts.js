$(document).ready(function() {

	$('.level0-alerts ul li').eq(0).show();
	var alertsInterval = null;
	var alertsIndex = 0;
	var alertsIntervalLength = 6000;
	var alertsCount = $('.level0-alerts ul li').size();
	var expandedSizeClass = 'level0-alerts-expanded-' + alertsCount;
        
	// Update NumAlerts Text
	$('.level0-alerts .alerts-toggle span').text(
		alertsCount + ' Alerts'
	);

	$('.level0-alerts .alerts-controls .alerts-toggle').bind('click', function() {
		var itemsCount = $('.level0-alerts ul li').size();
		if( $('.level0-alerts').hasClass('level0-alerts-expanded') ) {
			$('.level0-alerts').removeClass('level0-alerts-expanded');
			$('.level0-alerts').removeClass(expandedSizeClass);
			$('#home-dialog-open').show();
			switchAlert();
			startAlertsInterval();
		} else {
			$('.level0-alerts').addClass('level0-alerts-expanded');
			$('.level0-alerts').addClass(expandedSizeClass);
			$('.level0-alerts ul li').show();
			$('#home-dialog-open').hide();
			stopAlertsInterval();
		}
		return false;
	});
        
	var switchAlert = function() {
		alertsIndex++;
		var $nextItem = $('.level0-alerts ul li').eq(alertsIndex % alertsCount);
		$('.level0-alerts ul li').hide();
		$nextItem.fadeIn('slow');  
	}

	var startAlertsInterval = function() {
		if(alertsInterval == null)
			alertsInterval = setInterval(switchAlert, alertsIntervalLength);
	}
        
	var stopAlertsInterval = function() {
		if(alertsInterval != null) {
			clearInterval(alertsInterval);
			alertsInterval = null;
		}
	}

	if(alertsCount > 1) {
		$('.level0-alerts .alerts-controls').fadeIn();
		startAlertsInterval();
		$('#home-dialog-open').css({top:'50px'})
	} else {
		$('#home-dialog-open').css({top:'0px'})
	}
        
	if(alertsCount >= 1) {
		$('#home-dialog-open').css({top:'50px'})
	} else {
		$('#home-dialog-open').css({top:'0px'})
	}

	// Window Resize
	$('.vertical-gradient-container0').toggle( $('body').width() > 1440 )
	$(window).bind('resize', function() {
	$('.vertical-gradient-container0').toggle( $('body').width() > 1440 )
	});

});