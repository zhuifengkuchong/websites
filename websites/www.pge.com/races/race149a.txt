<script id = "race149a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race149={};
	myVars.races.race149.varName="Object[3869].ul ";
	myVars.races.race149.varType="@varType@";
	myVars.races.race149.repairType = "@RepairType";
	myVars.races.race149.event1={};
	myVars.races.race149.event2={};
	myVars.races.race149.event1.id = "Lu_Id_a_3";
	myVars.races.race149.event1.type = "onfocus";
	myVars.races.race149.event1.loc = "Lu_Id_a_3_LOC";
	myVars.races.race149.event1.isRead = "False";
	myVars.races.race149.event1.eventType = "@event1EventType@";
	myVars.races.race149.event2.id = "Lu_Id_a_40";
	myVars.races.race149.event2.type = "onfocus";
	myVars.races.race149.event2.loc = "Lu_Id_a_40_LOC";
	myVars.races.race149.event2.isRead = "True";
	myVars.races.race149.event2.eventType = "@event2EventType@";
	myVars.races.race149.event1.executed= false;// true to disable, false to enable
	myVars.races.race149.event2.executed= false;// true to disable, false to enable
</script>

