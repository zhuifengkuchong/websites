<script id = "race127a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race127={};
	myVars.races.race127.varName="Object[3869].ul[aria-hidden=false] a ";
	myVars.races.race127.varType="@varType@";
	myVars.races.race127.repairType = "@RepairType";
	myVars.races.race127.event1={};
	myVars.races.race127.event2={};
	myVars.races.race127.event1.id = "Lu_Id_a_3";
	myVars.races.race127.event1.type = "onkeydown";
	myVars.races.race127.event1.loc = "Lu_Id_a_3_LOC";
	myVars.races.race127.event1.isRead = "False";
	myVars.races.race127.event1.eventType = "@event1EventType@";
	myVars.races.race127.event2.id = "Lu_Id_a_5";
	myVars.races.race127.event2.type = "onkeydown";
	myVars.races.race127.event2.loc = "Lu_Id_a_5_LOC";
	myVars.races.race127.event2.isRead = "True";
	myVars.races.race127.event2.eventType = "@event2EventType@";
	myVars.races.race127.event1.executed= false;// true to disable, false to enable
	myVars.races.race127.event2.executed= false;// true to disable, false to enable
</script>

