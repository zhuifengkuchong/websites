<script id = "race54b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race54={};
	myVars.races.race54.varName="Object[2309].triggered";
	myVars.races.race54.varType="@varType@";
	myVars.races.race54.repairType = "@RepairType";
	myVars.races.race54.event1={};
	myVars.races.race54.event2={};
	myVars.races.race54.event1.id = "hig-suggestresults-wrapper";
	myVars.races.race54.event1.type = "onmouseover";
	myVars.races.race54.event1.loc = "hig-suggestresults-wrapper_LOC";
	myVars.races.race54.event1.isRead = "True";
	myVars.races.race54.event1.eventType = "@event1EventType@";
	myVars.races.race54.event2.id = "Lu_window";
	myVars.races.race54.event2.type = "onload";
	myVars.races.race54.event2.loc = "Lu_window_LOC";
	myVars.races.race54.event2.isRead = "False";
	myVars.races.race54.event2.eventType = "@event2EventType@";
	myVars.races.race54.event1.executed= false;// true to disable, false to enable
	myVars.races.race54.event2.executed= false;// true to disable, false to enable
</script>

