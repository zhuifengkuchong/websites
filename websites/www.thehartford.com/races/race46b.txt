<script id = "race46b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race46={};
	myVars.races.race46.varName="BODY[0xb47c20].blur";
	myVars.races.race46.varType="@varType@";
	myVars.races.race46.repairType = "@RepairType";
	myVars.races.race46.event1={};
	myVars.races.race46.event2={};
	myVars.races.race46.event1.id = "Lu_Id_input_15";
	myVars.races.race46.event1.type = "onblur";
	myVars.races.race46.event1.loc = "Lu_Id_input_15_LOC";
	myVars.races.race46.event1.isRead = "True";
	myVars.races.race46.event1.eventType = "@event1EventType@";
	myVars.races.race46.event2.id = "Lu_window";
	myVars.races.race46.event2.type = "onload";
	myVars.races.race46.event2.loc = "Lu_window_LOC";
	myVars.races.race46.event2.isRead = "False";
	myVars.races.race46.event2.eventType = "@event2EventType@";
	myVars.races.race46.event1.executed= false;// true to disable, false to enable
	myVars.races.race46.event2.executed= false;// true to disable, false to enable
</script>

