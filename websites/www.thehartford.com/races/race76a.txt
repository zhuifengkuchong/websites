<script id = "race76a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race76={};
	myVars.races.race76.varName="Object[2309].triggered";
	myVars.races.race76.varType="@varType@";
	myVars.races.race76.repairType = "@RepairType";
	myVars.races.race76.event1={};
	myVars.races.race76.event2={};
	myVars.races.race76.event1.id = "Lu_window";
	myVars.races.race76.event1.type = "onload";
	myVars.races.race76.event1.loc = "Lu_window_LOC";
	myVars.races.race76.event1.isRead = "False";
	myVars.races.race76.event1.eventType = "@event1EventType@";
	myVars.races.race76.event2.id = "Lu_Id_div_17";
	myVars.races.race76.event2.type = "onmousemove";
	myVars.races.race76.event2.loc = "Lu_Id_div_17_LOC";
	myVars.races.race76.event2.isRead = "True";
	myVars.races.race76.event2.eventType = "@event2EventType@";
	myVars.races.race76.event1.executed= false;// true to disable, false to enable
	myVars.races.race76.event2.executed= false;// true to disable, false to enable
</script>

