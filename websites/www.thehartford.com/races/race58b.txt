<script id = "race58b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race58={};
	myVars.races.race58.varName="Object[2309].triggered";
	myVars.races.race58.varType="@varType@";
	myVars.races.race58.repairType = "@RepairType";
	myVars.races.race58.event1={};
	myVars.races.race58.event2={};
	myVars.races.race58.event1.id = "Lu_Id_div_17";
	myVars.races.race58.event1.type = "onmouseover";
	myVars.races.race58.event1.loc = "Lu_Id_div_17_LOC";
	myVars.races.race58.event1.isRead = "True";
	myVars.races.race58.event1.eventType = "@event1EventType@";
	myVars.races.race58.event2.id = "Lu_window";
	myVars.races.race58.event2.type = "onload";
	myVars.races.race58.event2.loc = "Lu_window_LOC";
	myVars.races.race58.event2.isRead = "False";
	myVars.races.race58.event2.eventType = "@event2EventType@";
	myVars.races.race58.event1.executed= false;// true to disable, false to enable
	myVars.races.race58.event2.executed= false;// true to disable, false to enable
</script>

