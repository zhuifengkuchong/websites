<script id = "race52a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race52={};
	myVars.races.race52.varName="Object[2309].triggered";
	myVars.races.race52.varType="@varType@";
	myVars.races.race52.repairType = "@RepairType";
	myVars.races.race52.event1={};
	myVars.races.race52.event2={};
	myVars.races.race52.event1.id = "Lu_window";
	myVars.races.race52.event1.type = "onload";
	myVars.races.race52.event1.loc = "Lu_window_LOC";
	myVars.races.race52.event1.isRead = "False";
	myVars.races.race52.event1.eventType = "@event1EventType@";
	myVars.races.race52.event2.id = "utility-links-list-item-4";
	myVars.races.race52.event2.type = "onmouseover";
	myVars.races.race52.event2.loc = "utility-links-list-item-4_LOC";
	myVars.races.race52.event2.isRead = "True";
	myVars.races.race52.event2.eventType = "@event2EventType@";
	myVars.races.race52.event1.executed= false;// true to disable, false to enable
	myVars.races.race52.event2.executed= false;// true to disable, false to enable
</script>

