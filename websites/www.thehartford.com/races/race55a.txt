<script id = "race55a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race55={};
	myVars.races.race55.varName="Function[9427].guid";
	myVars.races.race55.varType="@varType@";
	myVars.races.race55.repairType = "@RepairType";
	myVars.races.race55.event1={};
	myVars.races.race55.event2={};
	myVars.races.race55.event1.id = "utility-links-list-item";
	myVars.races.race55.event1.type = "onmouseover";
	myVars.races.race55.event1.loc = "utility-links-list-item_LOC";
	myVars.races.race55.event1.isRead = "False";
	myVars.races.race55.event1.eventType = "@event1EventType@";
	myVars.races.race55.event2.id = "hig-suggestresults-wrapper";
	myVars.races.race55.event2.type = "onmouseover";
	myVars.races.race55.event2.loc = "hig-suggestresults-wrapper_LOC";
	myVars.races.race55.event2.isRead = "True";
	myVars.races.race55.event2.eventType = "@event2EventType@";
	myVars.races.race55.event1.executed= false;// true to disable, false to enable
	myVars.races.race55.event2.executed= false;// true to disable, false to enable
</script>

