(function($) {
    $.widget("q4.previewToolbar", {
        options: {
            styles: true,
            dashboard: true,
            selectbox: false,
            theme: false
        },

        _create: function () {
            var _ = this, o = this.options, el = _.element;

            // exit if not in preview
            if (GetViewType() !== '0') {
                return;
            }

            if (o.dashboard){
                el.find('.PreviewDateControls').append('<a class="PreviewActionButton" href="https://q4widgets.q4web.com/">Dashboard</a>');
            }

            if (o.styles) {
                el.append('<link rel="stylesheet" type="text/css" href="../css/q4preview.css"/*tpa=https://q4widgets.q4web.com/preview/css/q4preview.css*/>');
            }

            if (o.theme){
                el.append('<link rel="stylesheet" type="text/css" href="../../../code.jquery.com/ui/1.11.4/themes/ui-darkness/jquery-ui.css"/*tpa=https://code.jquery.com/ui/1.11.4/themes/ui-darkness/jquery-ui.css*/>');
            }

            if (o.selectbox) {
                el.append('<link rel="stylesheet" type="text/css" href="../css/selectbox.css"/*tpa=https://q4widgets.q4web.com/preview/css/selectbox.css*/>');
                $.getScript('https://q4widgets.q4web.com/preview/js/jquery.selectbox-0.3.min.js', function(){
                    _.updateToolbar();
                });
            } else {
                _.updateToolbar();
            }


        },

        updateToolbar: function () {
            var el = this.element;

            el.attr('href', '#').on('click', '#lnkPostback', function (e) {
                e.preventDefault();
                el.toggleClass('open');
            }).find('select').selectbox();
        },

        destroy: function () {
            this.element.html('');
        },

        _setOption: function(option, value) {
            this._superApply(arguments);
        }
        
    });
})(jQuery);


