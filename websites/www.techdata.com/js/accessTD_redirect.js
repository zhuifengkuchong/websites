/// <reference path="jquery.min.js"/*tpa=http://www.techdata.com/js/jquery.min.js*/ />
/// <reference path="modernizr.js"/*tpa=http://www.techdata.com/js/modernizr.js*/ />
/// <reference path="plugins/redirection-mobile.js"/*tpa=http://www.techdata.com/js/plugins/redirection-mobile.js*/ />
/// <reference path="plugins/parseUri.js"/*tpa=http://www.techdata.com/js/plugins/parseUri.js*/ />

; (function () {
    "use strict";

    var init = function () {
        var mobileRedirectPromptViewed = 'NO';

        var uri = parseUri(window.location.href);
        var hasReferrerRedirect = ((uri.query.toLowerCase().indexOf("referrer") > -1)); // returns true if referrer is present


        // if we don't find a referrer query parameter
        // then we want to trigger the prompt
        if (!hasReferrerRedirect) {

            if (Modernizr.sessionstorage) {
                if (typeof sessionStorage.mobileRedirectPromptViewed !== 'undefined') {
                    mobileRedirectPromptViewed = sessionStorage.mobileRedirectPromptViewed;
                }
            }

            SA.redirection_mobile({
                mobile_url: "http://www.techdata.com/js/m.techdata.com?referrer=techdata.com",
                tablet_host: "http://www.techdata.com/js/m.techdata.com?referrer=techdata.com",
                mobile_scheme: "https",
                cookie_hours: "24",
                beforeredirection_callback: (function () {
                    var shouldRedirect = false;

                    // if the user hasn't seen the prompt for the browser session, then show it
                    if (mobileRedirectPromptViewed === 'NO') {
                        shouldRedirect = confirm('http://www.techdata.com/js/It appears you are visiting techdata.com from a mobile device. Click OK if you would prefer to visit our mobile optimized site m.techdata.com')

                        if (Modernizr.sessionstorage) {
                            // let's not show it again for the session
                            sessionStorage.mobileRedirectPromptViewed = 'YES';
                        }
                    }

                    return shouldRedirect;
                })
            });
        }            

    };

    $(init);
}());