/* exported Parter */
function Parter () {
  var parter = this;
  var callIfDefined = function (fn, otherwise) {
    var isDefined = function (test) {
      return (typeof(test) !== 'undefined' && test !== null);
    };
    if (isDefined(fn)) return fn();
    if (isDefined(otherwise)) return otherwise();
    return false;
  };
  this._now = null;
  this.now = function () {
    if (!parter._now) parter._now = new Date();
    return parter._now;
  };
  this.reset = function () {
    parter._now = new Date();
    return this;
  };
  this._fmtTimeWithNow = function (time) {
    var now   = parter.now(),
      sptTime = time.toString().split(':'),
      nowFmt  = now.getHours() * 100 + now.getMinutes(),
      timeFmt = parseInt(sptTime[0], 10) * 100 + (parseInt(sptTime[1], 10) || 0);

    return [nowFmt, timeFmt];
  };
  this._fmtDateWithNow = function (date) {
    var now   = parter.now(),
      sptDate = date.toString().split('/'),
      nowFmt  = (now.getMonth() + 1) * 100 + now.getDate(),
      dateFmt = parseInt(sptDate[0], 10) * 100 + (parseInt(sptDate[1], 10) || 0),
      yearPrs = parseInt(sptDate[2], 10),
      yearFmt = (sptDate[2].length > 2) ? yearPrs : yearPrs + 2000;

    return [nowFmt, dateFmt, now.getFullYear(), yearFmt];
  };
  this._timeIsBefore = function (time) {
    var times = parter._fmtTimeWithNow(time);
    return times[0] < times[1];
  };
  this._timeIsAfter = function (time) {
    return !parter._timeIsBefore(time);
  };
  this._dateIsBefore = function (date) {
    var dates = parter._fmtDateWithNow(date);
    return dates[0] <= dates[1] && dates[2] <= dates[3];
  };
  this._dateIsAfter = function (date) {
    return !parter._dateIsBefore(date);
  };
  this._makeBetweenFn = function (type) {
    return function (start, end, fnA, fnB)  {
      var a = '_'+type+'IsAfter';
      var b = '_'+type+'IsBefore';
      if (parter[a](start) && parter[b](end)) {
        return callIfDefined(fnA, function () { return true; });
      } else {
        return callIfDefined(fnB, function () { return false; });
      }
    };
  };
  this.time = {
    before: parter._timeIsBefore,
    after: parter._timeIsAfter,
    between: function (start, end, fnA, fnB) {
      return parter._makeBetweenFn('time')(start, end, fnA, fnB);
    }
  };
  this.date = {
    before: parter._dateIsBefore,
    after: parter._dateIsAfter,
    between: function (start, end, fnA, fnB) {
      return parter._makeBetweenFn('date')(start, end, fnA, fnB);
    }
  };
}
