$.youtubify = {
  id: 'Youtubify',
  version: '1.0'
};

(function ($) {
  $.fn.extend({
    youtubify: function (params, binder) {
      var tag = document.createElement('script');
      var firstScriptTag = document.getElementsByTagName('script')[0];
      tag.src = "../../../../www.youtube.com/player_api-1.js"/*tpa=https://www.youtube.com/player_api*/;
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
      var defaults = {
        showControls: true,
        onReady:  function () {},
        onChange: function (stateID) {
            if(stateID.data ==0){
              
              $('#container').children().show();
              setTimeout(function(){
               $('#container-yt-wrap').css({ visibility: 'hidden' });
               $("#container").show();
              }, 100);

            }



        }
      };
      var opts = $.extend({}, defaults, params);
      var player = {
        interval: function (onReady) {
          window.onYouTubeIframeAPIReady = function () {
            try {
              onReady();
              clearInterval(interval);
            } catch (err) {
              return;
            }
            return true;
          };
          interval = setInterval(window.onYouTubeIframeAPIReady, 200);
        },
        init: function (id, opts) {
          var player = new YT.Player(id, {
            videoId: opts.id,
            width:  opts.width  || '100%',
            height: opts.height || '100%',
            playerVars: {
              autoplay: opts.autoplay || 0,
              controls: opts.controls || true,
              modestbranding: 1,
              rel: 0,
              showinfo: 0,
              wmode: 'transparent'
            },
            events: {
              "onReady": opts.onReady,
              "onStateChange": opts.onChange

            }
          });
          if (typeof(binder) !== 'undefined') binder(player);
          return player;
        }
      };
      return this.each(function () {
        var elementId = $(this).attr("id");
        $(this).attr({ "wmode": "opaque" });
        player.interval(function () {
          player.init(elementId, opts, true);
        });
      });
    }
  });
})(jQuery);
