var $stateILI = newState({ stop: false });

function playDesktop (player) {
  return function () {
    $('#container').css({ background: 'black' });
    $stateILI.update(function () { return { stop: true }; });
    setTimeout(function () {
      player.playVideo();
      setTimeout(function () {
        $('#container').hide();
        $("#cta").remove();

      }, 100);
    }, 500);
  };
}

function playAndTrack (player, tag) {
  return function () {
    if (typeof(tag) !== 'undefined' && typeof(_gaq) !== 'undefined')
      _gaq.push(['_trackEvent', 'Play Video', 'Click', tag]);

    $('#container').children().hide();
    $("#cta").remove();
    $('#container-yt-wrap').css({ visibility: 'visible' });

//    $('#cta').html('<img src="../img/viewall.png"/*tpa=http://www.mcdonalds.com/us_assets/sirloin-reminder-hpto/img/viewall.png*/>');
     

    ifTablet(function () {
      $('#container').hide();
      $("#cta").remove();

    }, playDesktop(player));
  };
}

$(window).load(function () {
  var parter = new Parter();
  var tag, video;
  var path = '/us_assets/sirloin-reminder-hpto/';
  $('#graphic').attr('src', path + 'img/graphic-1.jpg');


var today = new Date();
var dd = today.getDate().toString();
var mm = (today.getMonth()+1).toString(); //January is 0!



var vids = [];

vids['528'] = 'RikNNRIQfs0';
vids['529'] = 'lER5wNq8GRM';
vids['530'] = 'gBKZWjzDqOk';
vids['531'] = '5GMVKRBqN30';
vids['61'] = '-raDxYv5IF4';
vids['62'] = 'yWMoVujskgY';
vids['63'] = 'yWMoVujskgY';
vids['64'] = 'qWKOqbmFskI';
vids['65'] = '2phic2sNeJs';
vids['66'] = 'MxRmxpjP_tE';
vids['67'] = '-sgz4Zz-cRQ';
vids['68'] = 'Hser3GU1KfM';
vids['69'] = '9VR0DFAIEq4';
vids['610'] = 'RVXwLgPrPUw';
vids['611'] = 'OI-mcjOK6Z4';




video = vids[mm+dd];



if(!video || video==''){video = 'RikNNRIQfs0';}

//console.log("video" + video + " - mm+dd: " + mm+dd);

tag = "Sirloin Video Play";


  $('#container-yt').youtubify({ id: video }, function (player) {
    $('#cta').click(playAndTrack(player, tag));
    $('#cta2').click(function(){window.location='https://www.youtube.com/playlist?list=PLKkYatWHDPavF2455Iv1xi1ZNSt4_3WbZ';} );

  });
});
