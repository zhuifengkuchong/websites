requirejs.config({
	waitSeconds: 0
});

require([

	'require'

], function(){

	window.env = 'prod';  //prod,dev
	window.internal = false;

	//Brightcove
	window.bc = {
		videoSingle: {
			playerID: "4029272025001",
			playerKey: "AQ~~,AAAAukPAlqE~,oAVq1qtdRjzvprzv7b_de_OOilq0kg4d"
		},
		videoPlaylist: {
			playerID: "4036162527001",
			playerKey: "AQ~~,AAAAukPAlqE~,oAVq1qtdRjzdRuL2dETluVibkbdlNeAX",
			width: 610,
			height: 618
		},
		innovation: {
			videoSingle: {
				playerID: "4081532015001",
				playerKey: "AQ~~,AAAAukPAlqE~,oAVq1qtdRjzMPsJ4Cx7sgDC6yHiu8IrZ"
			},
			videoPlaylist: {
				playerID: "4081532014001",
				playerKey: "AQ~~,AAAAukPAlqE~,oAVq1qtdRjz5xpS-b0ctTmnPyxFh_Yoh",
				width: 610,
				height: 618
			}
		}
	};

	//AddThis
	window.addThis = {
		pubid: 'boeing2016',
		services: ['email', 'facebook', 'twitter', 'linkedin', 'blogger', 'more' ]
	};

	//Coremetrics
	window.cm = {
		clientID : "51680000|Boeing.comEXTERNAL",
		clientManaged : true,
		collectionDomain : "http://www.boeing.com/assets/js/data.coremetrics.com",
		cookieDomain : "http://www.boeing.com/assets/js/boeing.com"
	};

	if(window.env == 'dev')
	{
		require(['config'], function(){
			require(['main'], function(){});	
		});
	}
	else 
	{
		require(['http://www.boeing.com/assets/js/main.min'], function(){});
	}	

});