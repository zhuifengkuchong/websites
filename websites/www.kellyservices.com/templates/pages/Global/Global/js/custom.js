// JavaScript Document

function isMobile (){
	if ($('#isMobile').css('display') != 'none'){
		return true
	} else {return false}
};

function animateInsideTab(){
		if (isMobile()){ // mobile animations

		} else { // desktop animations
			$('#index #logo img').animate({
				height: "54px"
			});

			$('.tab-row .col-md-12').show("slide", { direction: "down" }, 500);

			if ($(window).width() < 962){
				    $('#rightNav').stop();
					$('#rightNav').animate({top: '-14px'}, 1000);
			}

		}
}


function animateBackHome(){
		if (isMobile()){ // mobile animations

		} else { // desktop animations
		$('#home').fadeIn();

			$('#index #logo img').animate({
				height: "72px"
			});

			$('.tab-row .col-md-12').hide("slide", { direction: "down" }, 1000);
			//$('.tab-row .col-md-12').hide("slide", { direction: "down" }, 1000);

			if ($(window).width() < 962){
				$('#rightNav').stop();
					$('#rightNav').animate({top: '23px'}, 1000);

			}
		}

	}

	function checkHeight(){
			var showPaddTop = parseInt($('.align-show').css('padding-top').replace("px", ""));
			var showPaddBot = parseInt($('.align-show').css('padding-bottom').replace("px", ""));
			$('#autoHeight').animate({
				height: $('.align-show').height()+showPaddTop+showPaddBot // probably should be dynamic, this is the set height of padding top on this row
			});
	}

	// code to be included on both functions below
	function buttonClickShared() {
		$('#home').removeClass('align-show');
		$('#home').addClass('align-right');
		$("#home").fadeOut(1000);
		setTimeout(function(){
			if (!isMobile()){
			  animateInsideTab()
			}
			checkHeight()
		}, 500);
	};
	// code for each specific button/tab
	function openCareers() {
		window.location.hash = '#Careers';
		buttonClickShared();
		$('#careersBus').removeClass('align-left').addClass('align-show');
		$('#BusContent').hide()
		$('#careersContent').show();
		$('#careersTab').addClass('active');
		$('#busTab').removeClass('active');
		$("#menu").trigger("http://www.kellyservices.com/templates/pages/Global/Global/js/close.mm");

	}
	function openBusiness() {
		window.location.hash = '#Business-Services';
		buttonClickShared();
		$('#careersBus').removeClass('align-left').addClass('align-show');
		$('#careersContent').hide();
		$('#BusContent').show();
		$('#careersTab').removeClass('active');
		$('#busTab').addClass('active');
		$("#menu").trigger("http://www.kellyservices.com/templates/pages/Global/Global/js/close.mm");

	}

	// on page load, does it have a sub-tab selected
	if ( document.location.href.indexOf('#Careers') > -1 ) {
		setTimeout(function(){
			openCareers();
			}, 0);
    }
	if ( document.location.href.indexOf('#Business-Services') > -1 ) {
       setTimeout(function(){
			openBusiness()
			}, 0)
    }

// used a few times, checks if tabs are shown, and resolution is under 962, to move nav up or down
function moveRightNav() {
	function ShouldNavBeUp () {
		var toReturn = false;
		var AreTabsOn = $('#careersTab').is(':visible');
		console.log(AreTabsOn);
		if (AreTabsOn && ($(window).width() <= 962)) {toReturn = true} else {toReturn = false}
		return toReturn;
	}

	if (ShouldNavBeUp()) {
		$('#rightNav').stop();
		$('#rightNav').animate({top: '-14px'});
		//console.log('nav@14');
	} else  {
		$('#rightNav').stop();
		$('#rightNav').animate({top: '23px'});
		//console.log('nav@23');
	}


}



// Open Jquery
jQuery(function ($) {
    $('.align a.careers, #index a#careersTab').click(function () {
        openCareers();
    });
    $('.align a.business, #index a#busTab').click(function () {
        openBusiness();
    });

    // Home pressed
    $('#index #logo, .mobileTitle .back').click(function () {
        if (!isMobile()) {
			animateBackHome()
		};
		$("#home").fadeIn();
        $('#careersBus').removeClass('align-show').addClass('align-left');
        $('#home').removeClass('align-right').addClass('align-show');
        checkHeight();
        $('#careersTab, #busTab').removeClass('active');
        window.location.hash = ''

    });

    //$('.logo-row>.container .row .col-md-6:nth-child(2)').css('height', '72px');
    $(window).resize(function () {
        doWindowResize();
    });
    if (isMobile()) { $('#logo img').attr('src', '../../../../../images/logo-grn.png'/*tpa=http://www.kellyservices.com/images/logo-grn.png*/).css('height', '65px'); } else { $('#index #logo img').attr('src', '../../../../../images/logo.png'/*tpa=http://www.kellyservices.com/images/logo.png*/).css('height', 'auto'); }

    var showPaddTop = parseInt($('.align-show').css('padding-top').replace("px", ""));
    var showPaddBot = parseInt($('.align-show').css('padding-bottom').replace("px", ""));
    //$('#autoHeight').height($('.align-show').height()+showPaddTop+showPaddBot);
    setTimeout(function () {
        $('#autoHeight').height($('.align-show').height() + showPaddTop + showPaddBot);
        resizeFooterBar();
    }, 100);

    $(window).bind('orientationchange', function () {
        $('#autoHeight').height($('.align-show').height() + showPaddTop + showPaddBot);
        resizeFooterBar();
    });
    //$(".dropdown-menu li a").click(function(){
    //  $(this).parent('li').parent('ul').parent('.btn-group').children("button").html($(this).text()+'<span class="caret"></span>');
    //  $(this).parent('li').parent('ul').parent('.btn-group').children("button").val($(this).text());
    //
    //});

    //start ebrahim's cutom

    function resizeFooterBar() {
        var width = $(window).width() - $("#footer_right_image").width();
        $("#footer_repeat").width(width);
    }


    function doWindowResize() {
        resizeFooterBar();

        if (isMobile()) { // on resize, and mobile
            $('#logo img').attr('src', '../../../../../images/logo-grn.png'/*tpa=http://www.kellyservices.com/images/logo-grn.png*/);
            $('#index #logo img').css('height', '65px');
            $('.tab-row>.col-md-12').hide();
        } else {
            // on resize, and desktop
            $('#logo img').attr('src', '../../../../../images/logo.png'/*tpa=http://www.kellyservices.com/images/logo.png*/);
            $('#index #logo img').css('height', 'auto')

            if (!$('#home').hasClass('align-show')) { // on resize, desktop, and not on homepage
                $('.tab-row>.col-md-12').show();
                $('#logo img').css('height', '54px');
            }
            moveRightNav();
        }

        // if we are NOT on mobile and NOT on the home page, animate the top tabs into view
        setTimeout(function () {
            if (!isMobile() && !$('#home').hasClass('align-show')) {
                animateInsideTab()
            }
            checkHeight()
        }, 150);
    }

    function handleCountryChange(selectedElement) {

        selectedElement.parent('li').parent('ul').parent('.btn-group').children("button").html(selectedElement.text() + '&nbsp;&nbsp;&nbsp;<span class="caret"></span>');
        selectedElement.parent('li').parent('ul').parent('.btn-group').children("button").val(selectedElement.text());
        // start my custom code. get the string from the textbox
        var selectedValue = selectedElement.text();  //$(this).parent('li').parent('ul').parent('.btn-group').children("button").val($(this).text());
        //var selectedIndex = $(this).parent('li').attr('value');

        if ($("#careersTab").attr('class') == "active") {
            $("#CountryListBSbtn").html(selectedValue + '&nbsp;&nbsp;&nbsp;<span class="caret"></span>');
        } else {
            $("#CountryListbtn").html(selectedValue + '&nbsp;&nbsp;&nbsp;<span class="caret"></span>');
        }

        var defaultLanguageContent = 'Select Language&nbsp;&nbsp;&nbsp; <span class="caret"></span>'
        defaultLanguageContent = selectedElement.parent('li').attr("title") + '&nbsp;&nbsp;&nbsp; <span class="caret"></span>';
        $("#divLanguagesBS > button, #divLanguagesBSMobile > button").removeAttr("value").html(defaultLanguageContent)
        $("#divLanguagesCareer > button, #divLanguagesCareerMobile > button").removeAttr("value").html(defaultLanguageContent)

        $.ajax({

            type: "POST",
            url: "/templates/Pages/Global/GlobalSiteWebService.aspx/Country_indexChanged",
            //url: "/templates/Pages/Global/WebServiceTest.asmx/Country_indexChanged",
            contentType: "application/json; charset=utf-8",
            data: "{country:'" + selectedValue + "'}",
            dataType: "json",
            success: function (data) {

                // Replace the div's content with the page method's return.
                // Success(date);
                var myData = data.d; // data.d is a JSON object that represents out SayHello class.
                // As it is already a JSON object we can just start using it

                $("#CareerExpertise").html(myData.careerMenu); $("#ctl00_ctl00_CareerExpertise").html(myData.careerMenu);
                $("#BSExpertise").html(myData.bsMenu); $("#ctl00_ctl00_BSExpertise").html(myData.bsMenu);

                $("#countrySiteDomain").attr("href", myData.siteURL);
                $("#countrySiteDomainBS").attr("href", myData.siteURL);
                $("#countrySiteDomain").text(myData.siteURL);
                $("#countrySiteDomainBS").text(myData.siteURL);

                $("#countrySiteDomainMobile").attr("href", myData.siteURL);
                $("#countrySiteDomainMobile").text(myData.siteURL);

                $("#countrySiteDomainBSMobile").attr("href", myData.siteURL);
                $("#countrySiteDomainBSMobile").text(myData.siteURL);


                $("#findjobSubmit").val(myData.lblSubmitTxt);
                $("#findjobSubmitURL").val(myData.JobSubmitURL);

                if (myData.JobSubmitURL == undefined) {
                    $("#divjobCategories").hide();
                    $("#divjobTypes").hide();
                    $("#divjobLocations").hide();
                    $("#keyword").hide();
                    $("#zip").hide();
                    $("#findjobSubmit").hide();
                    $(".txtjobsearch").hide();
				} else {
                    $(".txtjobsearch").show();
                    $("#divjobCategories").show();
                    $("#divjobTypes").show();
                    $("#divjobLocations").show();
                    $("#keyword").show();
                    $("#zip").show();
                    $("#findjobSubmit").show();

                    if (myData.JobCategories == "hide") {
                        $("#divjobCategories").hide();
                    }
                    else {
                        $("#uljobCategories").html(myData.JobCategories);
                        $("#divjobCategories").show();
                        $("#divjobCategories").children("button").html(myData.lblJobCategories + '<span class="caret"></span>');
                        $("#divjobCategories").children("button").val('0');
                    }
                    if (myData.JobLocations == "hide") {
                        $("#divjobLocations").hide();
                    } else {
                        $("#uljobLocations").html(myData.JobLocations);
                        $("#divjobLocations").show();
                        $("#divjobLocations").children("button").html(myData.lblJobLocation + '<span class="caret"></span>');
                    }
                    if (myData.JobTypes == "hide") {
                        $("#divjobTypes").hide();
                    } else {
                        $("#uljobTypes").html(myData.JobTypes);
                        $("#divjobTypes").show();
                        $("#divjobTypes").children("button").html(myData.lblJobType + '<span class="caret"></span>');
                        $("#divjobTypes").children("button").val('0');
                    }
                }

                $("#hiddenRequestStaffSubmitURL").val(myData.RequestStaffSubmitURL);
                $("#requestStaffBS").html(myData.staffRequest);



                if (myData.siteLangauges == "hide") {
                    $("#divLanguagesCareer").hide();
                    $("#divLanguagesBS").hide();
                    $("#divLanguagesCareerMobile").hide();
                    $("#divLanguagesBSMobile").hide();
                } else {
                    $("#langListCareer").html(myData.siteLangauges);
                    $("#langListBS").html(myData.siteLangauges);
                    $("#divLanguagesCareer").show();
                    $("#divLanguagesBS").show();

                    $("#langListCareerMobile").html(myData.siteLangauges);
                    $("#langListBSMobile").html(myData.siteLangauges);
                    $("#divLanguagesCareerMobile").show();
                    $("#divLanguagesBSMobile").show();
                }
                if (typeof selectedValue != "undefined") {
                    $(".lblcountry").text(selectedValue);
                }
                if (myData.lblJobSearch.length > 0) {
                    $(".txtjobsearch").text(myData.lblJobSearch);
                }
                else {
                    $(".txtjobsearch").text('Search Jobs');
                }
                if (myData.lblExpertiseArea.length > 0) {
                    $(".txtExpertiseArea").text(myData.lblExpertiseArea);
                } else {
                    $(".txtExpertiseArea").text('Areas of Expertise');
                }
                if (myData.lblRequestStaff.length > 0) {
                    $(".txtRequestStaff").text(myData.lblRequestStaff);
                } else {
                    $(".txtRequestStaff").text('Request Staff');
                }

                doWindowResize();

            },
            error: Failed
        });
        // done my custom code
    }

    $(document).on('click', '.dropdown-menu li a', function () {
        var ulID = $(this).parent('li').parent('ul').attr("id");

        if (ulID == "CountryList" || ulID == "CountryListBS" || ulID == "ctl00_ctl00_CountryList" || ulID == "ctl00_ctl00_CountryListBS" || ulID == "CountryListMobile" || ulID == "CountryListBSMobile") {
            handleCountryChange($(this))
        }
        else if (ulID == "uljobCategories" || ulID == "uljobTypes" || ulID == "uljobLocations") {
            $(this).parent('li').parent('ul').parent('.btn-group').children("button").html($(this).text() + '<span class="caret"></span>');
            var liElement = $(this).parent('li');
            var liValue = liElement.attr("value");
            $(this).parent('li').parent('ul').parent('.btn-group').children("button").val(liValue);

        }
        else {
            $(this).parent('li').parent('ul').parent('.btn-group').children("button").html($(this).text() + '<span class="caret"></span>');
            $(this).parent('li').parent('ul').parent('.btn-group').children("button").val($(this).text());
        }
    });

    $(document).on('click', '#langListCareer li a', function () {
        handleLanguageChange($(this))
    });
    $(document).on('click', '#langListBS li a', function () {
        handleLanguageChange($(this))
    });

    $(document).on('click', '#langListCareerMobile li a', function () {
        handleLanguageChange($(this))
    });
    $(document).on('click', '#langListBSMobile li a', function () {
        handleLanguageChange($(this))
    });

    $("#findjobSubmit").click(function (e) {
        e.preventDefault();
        var category = $("#divjobCategories").children("button").val();
        var location = $("#divjobLocations").children("button").val();
        var jobtype = $("#divjobTypes").children("button").val();
        var keywords = $("#keyword").val();
        var zip = $("#zip").val();

        if (category == "Category") {
            category = "0";
        }
        if (zip == "Postal Code") {
            zip = ""
        }
        if (keywords == "Keywords") {
            keywords = "";
        }
        if (location == "location") {
            location = "";
        }
        if (jobtype == "jobtype") {
            jobtype = "0";
        }
        if ($("#CountryListbtn").val() == 'United States' || $("#CountryListbtn").val() == 'Australia' || $("#CountryListbtn").val() == '') {
			if (category == "0") {
                category = "";
            }
            var url = $("#findjobSubmitURL").val();
            url += "&keywords=" + keywords
            url += "&location=" + zip
            url += "&jobCategoryList=" + category
            url += "&search=1"
            window.open(url, '_blank');
            // window.open(url, "category_search", "", true);
        }
        else {
            var url = $("#findjobSubmitURL").val();
            url += "&frm_keyword=" + keywords
            url += "&frm_job_type_id=" + jobtype
            url += "&frm_ind_id=" + category
            url += "&frm_loc_id=" + location
            window.open(url, '_blank');
            // window.open(url, "category_search", "", true);
        }
    });
    $("#requestStaffSubmit").click(function (e) {
        e.preventDefault();
        var url = $("#hiddenRequestStaffSubmitURL").val();

        window.open(url, '_blank');
    });
    $(".searchImg").click(function (e) {
        e.preventDefault();

        var keyword = $("#searchTxtbox").val();
        var url = "http://www.kellyservices.com/templates/pages/SearchResults.aspx?searchtext=";
        url += keyword
        window.open(url, '_blank');
    });
    $(".searchImgMobile").click(function (e) {
        e.preventDefault();

        var keyword = $("#searchTxtboxMobile").val();
        var url = "http://www.kellyservices.com/templates/pages/SearchResults.aspx?searchtext=";
        url += keyword
        window.open(url, '_blank');
    });

    // toggle search in mobile
    /* $('.mobileSearch a').click(function (e) {
    e.preventDefault();
    $(this).parent('span').toggleClass('active');
    $(this).toggleClass('searchImgMobile');
    });*/

    function handleLanguageChange(selectedElement) {
        doWindowResize();

        //$(this) = selectedElement;
        selectedElement.parent('li').parent('ul').parent('.btn-group').children("button").html(selectedElement.text() + '&nbsp;&nbsp;&nbsp;<span class="caret"></span>');
        selectedElement.parent('li').parent('ul').parent('.btn-group').children("button").val(selectedElement.text());
        // start my custom code. get the string from the textbox
        var selectedText = selectedElement.text();
        var selectedValue = selectedElement.text();  //$(this).parent('li').parent('ul').parent('.btn-group').children("button").val($(this).text());
        var selectedLanguageID = selectedElement.parent('li').attr('value');
        var selectedJobSearchCountry = selectedElement.parent('li').attr('id');  //$(this).attr('id');
        if ($("#careersTab").attr('class') == "active") {
            selectedValue = $("#CountryListbtn").text();
            $("#divLanguagesBS").children("button").html(selectedText + '&nbsp;&nbsp;&nbsp;<span class="caret"></span>');
        } else {

            selectedValue = $("#CountryListBSbtn").text();
            $("#divLanguagesCareer").children("button").html(selectedText + '&nbsp;&nbsp;&nbsp;<span class="caret"></span>');
        }


        $.ajax({

            type: "POST",
            url: "/templates/Pages/Global/GlobalSiteWebService.aspx/Language_indexChanged",
            //url: "/templates/Pages/Global/WebServiceTest.asmx/Language_indexChanged",
            contentType: "application/json; charset=utf-8",
            data: "{country:'" + selectedValue + "', languageID: '" + selectedLanguageID + "', globalJobSearchCountry: '" + selectedJobSearchCountry + "'}",
            dataType: "json",
            success: function (data) {

                // Replace the div's content with the page method's return.
                // Success(date);
                var myData = data.d; // data.d is a JSON object that represents out SayHello class.
                // As it is already a JSON object we can just start using it

                $("#CareerExpertise").html(myData.careerMenu);
                $("#BSExpertise").html(myData.bsMenu);
                $("#ctl00_ctl00_CareerExpertise").html(myData.careerMenu);
                $("#ctl00_ctl00_BSExpertise").html(myData.bsMenu);

                $("#countrySiteDomain").attr("href", myData.siteURL);
                $("#countrySiteDomainBS").attr("href", myData.siteURL);
                $("#countrySiteDomain").text(myData.siteURL);
                $("#countrySiteDomainBS").text(myData.siteURL);

                $("#countrySiteDomainMobile").attr("href", myData.siteURL);
                $("#countrySiteDomainMobile").text(myData.siteURL);

                $("#countrySiteDomainBSMobile").attr("href", myData.siteURL);
                $("#countrySiteDomainBSMobile").text(myData.siteURL);

                $("#findjobSubmit").val(myData.lblSubmitTxt);
                $("#findjobSubmitURL").val(myData.JobSubmitURL);

                if (myData.JobSubmitURL == undefined) {
                    $("#divjobCategories").hide();
                    $("#divjobTypes").hide();
                    $("#divjobLocations").hide();
                    $("#keyword").hide();
                    $("#zip").hide();
                    $("#findjobSubmit").hide();
                    $(".txtjobsearch").hide();
			 } else {
                    $(".txtjobsearch").show();
                    $("#divjobCategories").show();
                    $("#divjobTypes").show();
                    $("#divjobLocations").show();
                    $("#keyword").show();
                    $("#zip").show();
                    $("#findjobSubmit").show();

                    if (myData.JobCategories == "hide") {
                        $("#divjobCategories").hide();
                    }
                    else {
                        $("#uljobCategories").html(myData.JobCategories);
                        $("#divjobCategories").show();
                        $("#divjobCategories").children("button").html(myData.lblJobCategories + '<span class="caret"></span>');
                        $("#divjobCategories").children("button").val('0');
                    }
                    if (myData.JobLocations == "hide") {
                        $("#divjobLocations").hide();
                    } else {
                        $("#uljobLocations").html(myData.JobLocations);
                        $("#divjobLocations").show();
                        $("#divjobLocations").children("button").html(myData.lblJobLocation + '<span class="caret"></span>');
                    }
                    if (myData.JobTypes == "hide") {
                        $("#divjobTypes").hide();
                    } else {
                        $("#uljobTypes").html(myData.JobTypes);
                        $("#divjobTypes").show();
                        $("#divjobTypes").children("button").html(myData.lblJobType + '<span class="caret"></span>');
                        $("#divjobTypes").children("button").val('0');
                    }
                }

                $("#hiddenRequestStaffSubmitURL").val(myData.RequestStaffSubmitURL);
                $("#requestStaffBS").html(myData.staffRequest);



                if (myData.siteLangauges == "hide") {
                    $("#divLanguagesCareer").hide();
                    $("#divLanguagesBS").hide();
                    $("#divLanguagesCareerMobile").hide();
                    $("#divLanguagesBSMobile").hide();
                } else {
                    $("#langListCareer").html(myData.siteLangauges);
                    $("#langListBS").html(myData.siteLangauges);
                    $("#divLanguagesCareer").show();
                    $("#divLanguagesBS").show();

                    $("#langListCareerMobile").html(myData.siteLangauges);
                    $("#langListBSMobile").html(myData.siteLangauges);
                    $("#divLanguagesCareerMobile").show();
                    $("#divLanguagesBSMobile").show();
                }
                if (typeof selectedValue != "undefined") {
                    $(".lblcountry").text(selectedValue);
                }
                if (myData.lblJobSearch.length > 0) {
                    $(".txtjobsearch").text(myData.lblJobSearch);
                }
                else {
                    $(".txtjobsearch").text('Search Jobs');
                }
                if (myData.lblExpertiseArea.length > 0) {
                    $(".txtExpertiseArea").text(myData.lblExpertiseArea);
                } else {
                    $(".txtExpertiseArea").text('Areas of Expertise');
                }
                if (myData.lblRequestStaff.length > 0) {
                    $(".txtRequestStaff").text(myData.lblRequestStaff);
                } else {
                    $(".txtRequestStaff").text('Request Staff');
                }

                doWindowResize();

            },
            error: Failed
        });
        // done my custom code

    }

    $(document).ready(function () {

        var country = $("#hiddenDefaultCountry").val();
        if (country == undefined) {
            country = $("#ctl00_ctl00_hiddenDefaultCountry").val();
        }
        if (country != undefined) {
            $.ajax({
                type: "POST",
                //url: "global.aspx/Country_indexChanged",
                url: "/templates/Pages/Global/GlobalSiteWebService.aspx/Country_indexChanged",
                contentType: "application/json; charset=utf-8",
                data: "{country:'" + country + "'}",
                dataType: "json",
                success: function (data) {

                    // Replace the div's content with the page method's return.
                    // Success(date);
                    var myData = data.d; // data.d is a JSON object that represents out SayHello class.
                    // As it is already a JSON object we can just start using it

                    $("#CareerExpertise").html(myData.careerMenu); $("#ctl00_ctl00_CareerExpertise").html(myData.careerMenu);
                    $("#BSExpertise").html(myData.bsMenu); $("#ctl00_ctl00_BSExpertise").html(myData.bsMenu);

                    $("#countrySiteDomain").attr("href", myData.siteURL);
                    $("#countrySiteDomainBS").attr("href", myData.siteURL);
                    $("#countrySiteDomain").text(myData.siteURL);
                    $("#countrySiteDomainBS").text(myData.siteURL);

                    $("#countrySiteDomainMobile").attr("href", myData.siteURL);
                    $("#countrySiteDomainMobile").text(myData.siteURL);

                    $("#countrySiteDomainBSMobile").attr("href", myData.siteURL);
                    $("#countrySiteDomainBSMobile").text(myData.siteURL);

                    $("#findjobSubmit").val(myData.lblSubmitTxt);
                    $("#findjobSubmitURL").val(myData.JobSubmitURL);

                    if (myData.JobSubmitURL == undefined) {
                        $("#divjobCategories").hide();
                        $("#divjobTypes").hide();
                        $("#divjobLocations").hide();
                        $("#keyword").hide();
                        $("#zip").hide();
                        $("#findjobSubmit").hide();
                        $(".txtjobsearch").hide();
					} else {
                        $(".txtjobsearch").show();
                        $("#divjobCategories").show();
                        $("#divjobTypes").show();
                        $("#divjobLocations").show();
                        $("#keyword").show();
                        $("#zip").show();
                        $("#findjobSubmit").show();

                        if (myData.JobCategories == "hide") {
                            $("#divjobCategories").hide();
                        }
                        else {
                            $("#uljobCategories").html(myData.JobCategories);
                            $("#divjobCategories").show();
                            $("#divjobCategories").children("button").html(myData.lblJobCategories + '<span class="caret"></span>');
                            $("#divjobCategories").children("button").val('0');
                        }
                        if (myData.JobLocations == "hide") {
                            $("#divjobLocations").hide();
                        } else {
                            $("#uljobLocations").html(myData.JobLocations);
                            $("#divjobLocations").show();
                            $("#divjobLocations").children("button").html(myData.lblJobLocation + '<span class="caret"></span>');
                        }
                        if (myData.JobTypes == "hide") {
                            $("#divjobTypes").hide();
                        } else {
                            $("#uljobTypes").html(myData.JobTypes);
                            $("#divjobTypes").show();
                            $("#divjobTypes").children("button").html(myData.lblJobType + '<span class="caret"></span>');
                            $("#divjobTypes").children("button").val('0');
                        }
                    }

                    $("#hiddenRequestStaffSubmitURL").val(myData.RequestStaffSubmitURL);
                    $("#requestStaffBS").html(myData.staffRequest);



                    if (myData.siteLangauges == "hide") {
                        $("#divLanguagesCareer").hide();
                        $("#divLanguagesBS").hide();
                        $("#divLanguagesCareerMobile").hide();
                        $("#divLanguagesBSMobile").hide();
                    } else {
                        $("#langListCareer").html(myData.siteLangauges);
                        $("#langListBS").html(myData.siteLangauges);
                        $("#divLanguagesCareer").show();
                        $("#divLanguagesBS").show();

                        $("#langListCareerMobile").html(myData.siteLangauges);
                        $("#langListBSMobile").html(myData.siteLangauges);
                        $("#divLanguagesCareerMobile").show();
                        $("#divLanguagesBSMobile").show();
                    }
                    if (typeof selectedValue != "undefined") {
                        $(".lblcountry").text(selectedValue);
                    }
                    if (myData.lblJobSearch.length > 0) {
                        $(".txtjobsearch").text(myData.lblJobSearch);
                    }
                    else {
                        $(".txtjobsearch").text('Search Jobs');
                    }
                    if (myData.lblExpertiseArea.length > 0) {
                        $(".txtExpertiseArea").text(myData.lblExpertiseArea);
                    } else {
                        $(".txtExpertiseArea").text('Areas of Expertise');
                    }
                    if (myData.lblRequestStaff.length > 0) {
                        $(".txtRequestStaff").text(myData.lblRequestStaff);
                    } else {
                        $(".txtRequestStaff").text('Request Staff');
                    }

                },
                error: Failed
            });
        }

    });
    //end ebrahim custom





    /* Accordion */
    //$('.Accordion .content').hide();
    //$('.Accordion .tab.active').next('.content').show();
    $('.Accordion .tab').click(function () {
        if ($(this).hasClass('active')) {
            $(this).next('.content').slideUp();
            $('.Accordion .tab').removeClass('active');
        } else {
            $('.Accordion .tab').removeClass('active');
            $(this).addClass('active');
            $('.Accordion .content').slideUp();
            $(this).next('.content').slideDown();
        }
    });


    var showPaddTop = parseInt($('.align-show').css('padding-top').replace("px", ""));
    var showPaddBot = parseInt($('.align-show').css('padding-bottom').replace("px", ""));
    //$('#autoHeight').height($('.align-show').height()+showPaddTop+showPaddBot);
    setTimeout(function () {
        $('#autoHeight').height($('.align-show').height() + showPaddTop + showPaddBot);
    }, 400);

    moveRightNav();

});

$(document).ready(function () {
    $(".Accordion .tab:last").addClass('active');
    $('.Accordion .content').slideUp();
    $(".Accordion .tab:last").next('.content').slideDown();
});