<!DOCTYPE html>

<html ng-app="hjApp">
<head>
<meta charset="utf-8"/>
<title>Hotjar</title>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"/>
<script>
            (function(f,b,g){
                var xo=g.prototype.open,xs=g.prototype.send,c;
                f.hj=f.hj||function(){(f.hj.q=f.hj.q||[]).push(arguments)};
                f._hjSettings={hjid:2448, hjsv:2};
                function ls(){f.hj.documentHtml=b.documentElement.outerHTML;c=b.createElement("script");c.async=1;c.src="//static.hotjar.com/c/hotjar-2448.js?sv=2";b.getElementsByTagName("head")[0].appendChild(c);}
                if(b.readyState==="interactive"||b.readyState==="complete"||b.readyState==="loaded"){ls();}else{if(b.addEventListener){b.addEventListener("DOMContentLoaded",ls,false);}}
                if(!f._hjPlayback && b.addEventListener){
                    g.prototype.open=function(l,j,m,h,k){this._u=j;xo.call(this,l,j,m,h,k)};
                    g.prototype.send=function(e){var j=this;function h(){if(j.readyState===4){f.hj("_xhr",j._u,j.status,j.response)}}this.addEventListener("readystatechange",h,false);xs.call(this,e)};
                }
            })(window,document,window.XMLHttpRequest);
        </script>
<link href="//cdn.jsdelivr.net/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet"/>
<link href="/favicon.ico" rel="shortcut icon"/>
<link href="/static/insights-ff399a6387002f5c43e32d8312bb1717.css" rel="stylesheet"/>
<script src="https://js.braintreegateway.com/v2/braintree.js"></script>
<script src="//cdn.jsdelivr.net/ua-parser.js/0.7.0/ua-parser.min.js"></script>
<script async="" src="//fast.wistia.net/assets/external/E-v1.js"></script>
<script src="/static/globals-f660286fb5c4c4b989c5f5e7b7aa6ffb.js"></script>
<script src="//cdn.jsdelivr.net/jquery/2.1.1/jquery.min.js"></script>
<script src="//cdn.jsdelivr.net/g/jquery.ui@1.10%28jquery.ui.core.min.js+jquery.ui.widget.min.js+jquery.ui.mouse.min.js+jquery.ui.sortable.min.js%29"></script>
<script src="//cdn.jsdelivr.net/lodash/3.8.0/lodash.min.js"></script>
<script src="//cdn.jsdelivr.net/angularjs/1.2.20/angular.min.js"></script>
<script src="//cdn.jsdelivr.net/angular.bootstrap/0.11.0/ui-bootstrap.min.js"></script>
<script src="//cdn.jsdelivr.net/angular.bootstrap/0.11.0/ui-bootstrap-tpls.min.js"></script>
<script src="//cdn.jsdelivr.net/angular.ui-router/0.2.11/angular-ui-router.min.js"></script>
<script src="//cdn.jsdelivr.net/intro.js/0.9.0/intro.min.js"></script>
<script src="//cdn.jsdelivr.net/restangular/1.3.1/restangular.min.js"></script>
<script src="//cdn.jsdelivr.net/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="//cdn.jsdelivr.net/zeroclipboard/2.1.6/ZeroClipboard.min.js"></script>
<script src="//cdn.jsdelivr.net/chart.js/1.0.1-beta.4/Chart.min.js"></script>
<script src="//cdn.jsdelivr.net/parseuri/1.2.2/parseuri.min.js"></script>
<script src="/static/libs-4c45108377788107724f99e3db2cb0b1.js"></script>
<script src="/static/insights-dee9b292f7870ca2862a4149330088b3.js"></script>
<!-- GOOGLE ANALYTICS SNIPPET -->
<script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-51401671-1', 'hotjar.com');
        </script>
<!-- MIXPANEL SNIPPET -->
<script type="text/javascript">
            var mixpanelID = ((document.location.host === 'insights.hotjar.com') ? '6d7c50ad560e01715a871a117a2fbd90' : 'de220befb53275d738b897338b9faece');
            (function(f,b){if(!b.__SV){var a,e,i,g;window.mixpanel=b;b._i=[];b.init=function(a,e,d){function f(b,h){var a=h.split(".");2==a.length&&(b=b[a[0]],h=a[1]);b[h]=function(){b.push([h].concat(Array.prototype.slice.call(arguments,0)))}}var c=b;"undefined"!==typeof d?c=b[d]=[]:d="mixpanel";c.people=c.people||[];c.toString=function(b){var a="mixpanel";"mixpanel"!==d&&(a+="."+d);b||(a+=" (stub)");return a};c.people.toString=function(){return c.toString(1)+".people (stub)"};i="disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");
            for(g=0;g<i.length;g++)f(c,i[g]);b._i.push([a,e,d])};b.__SV=1.2;a=f.createElement("script");a.type="text/javascript";a.async=!0;a.src="undefined"!==typeof MIXPANEL_CUSTOM_LIB_URL?MIXPANEL_CUSTOM_LIB_URL:"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";e=f.getElementsByTagName("script")[0];e.parentNode.insertBefore(a,e)}})(document,window.mixpanel||[]);
            mixpanel.init(mixpanelID, {cross_subdomain_cookie : true});
        </script>
<!-- OPTIMIZELY SNIPPET -->
<script src="//cdn.optimizely.com/js/1407201747.js"></script>
<!-- GOODGLE ADWORDS CONVERSION TRACKING SCRIPT (https://developers.google.com/adwords-remarketing-tag/asynchronous/) -->
<script charset="utf-8" src="http://www.googleadservices.com/pagead/conversion_async.js" type="text/javascript"></script>
<!-- FACEBOOK CONVERSION TRACKING SCRIPT (https://developers.google.com/adwords-remarketing-tag/asynchronous/) -->
<script>
            (function() {
                var _fbq = window._fbq || (window._fbq = []);
                if (!_fbq.loaded) {
                    var fbds = document.createElement('script');
                    fbds.async = true;
                    fbds.src = '//connect.facebook.net/en_US/fbds.js';
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(fbds, s);
                    _fbq.loaded = true;
                }
            })();
        </script>
</head>
<script id="modal_account_type.html" type="text/ng-template">
        <div class="modal-body center">
    <h2 class="modal-title prn pbm">Quick question...</h2>
    <p>We're working on improving Hotjar for Agencies and Consultants.</p>
    <br /><br />
    <p class="font20 semibold">Do you use Hotjar with Clients?</p>
    <div class="pab">
        <button class="btn btn-silver pull-left" ng-click="yesClick()">Yes - I have clients</button>
        <button class="btn btn-silver pull-right" ng-click="noClick()">No - I don't work with clients</button>
    </div>
    <div class="clear"></div>
    <p class="mtb"><a ng-click="noClick()" class="black underline">I'm not sure what this means!</a></p>
</div>
    </script>
<script id="modal_payment.html" type="text/ng-template">
        <div class="modal-body pan" ng-class="{pro: model.plan === 'pro', business: model.plan === 'business'}">
    <button type="button" class="close close-dark" ng-click="$close()" ng-hide="model.hideCloseButton">&times;</button>
    <h2 class="modal-title" ng-bind-html="model.title | trustAsHtml"></h2>

    <!-- EXPIRED screen -->
    <div class="form-expired" ng-show="model.state === 'expired'">
        <form name="forms.expired" novalidate>

            <!-- SINGLE ORGANIZATION -->
            <div class="expired-content expired-single" ng-if="model.accountOrganizations.length === 1">
                <h2 class="font18">To continue enjoying uninterrupted and unlimited Insights with Hotjar <span class="uppercase">{{ model.plan }}</span>, please enter your payment details:</h2>

                <button type="button" class="btn btn-lg"
                        ng-class="{'btn-hotjar': model.plan === 'pro', 'btn-gold': model.plan === 'business'}"
                        ng-click-with-loader="updateOrganization()">
                    <span class="spinner"></span>
                    <span class="text-default">Continue with Hotjar {{ model.plan }}</span>
                    <span class="text-loading">Please wait...</span>
                </button>

                <h2 class="font14" ng-show="model.prices.symbol">Only {{ model.prices.symbol + model.totalCost }} per month for Unlimited Users, Sites and Insights.</h2>

                <hr />

                <p class="note">Or <a ng-click="downgradeToBasic()">downgrade</a> to our limited free account &ndash; Hotjar Basic</p>
                <a class="black underline" target="_blank" href="https://www.hotjar.com/compare-our-plans">Compare BASIC and <span class="uppercase">{{ model.plan }}</span></a>
            </div>

            <!-- MULTIPLE ORGANIZATIONS -->
            <div class="expired-content" ng-if="model.accountOrganizations.length > 1">
                <h2 class="font16">To continue enjoying uninterrupted and unlimited Insights with Hotjar, please enter your payment details:</h2>

                <table class="table less-td-padding table-hover mth mbm" table-data="accountOrganizations(st)" sortable-table st-initial-by="created" st-initial-reverse="false">
                    <thead>
                    <tr>
                        <th st-by="created">Date added</th>
                        <th st-by="name">Organization</th>
                        <th st-by="plan">Plan</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="organization in accountOrganizations(st)" ng-form="accountOrganization" ng-class="{'organization-discount': model.firstOrganization.id !== organization.id}">
                        <td>{{ organization.created | epochDate }}</td>
                        <td>{{ organization.name }}</td>
                        <td ng-if="model.firstOrganization.id === organization.id">
                            <select class="form-control inline widthAuto" ng-model="organization.plan">
                                <option value="basic">BASIC &ndash; FREE</option>
                                <option value="pro">PRO &ndash; {{ model.prices.symbol + model.prices.amounts.pro.standard.split('.')[0] }} / month</option>
                                <option value="business">BUSINESS &ndash; {{ model.prices.symbol + model.prices.amounts.business.standard.split('.')[0] }} / month</option>
                            </select>
                        </td>
                        <td ng-if="model.firstOrganization.id !== organization.id">
                            <select class="form-control inline widthAuto" ng-model="organization.plan">
                                <option value="basic">BASIC &ndash; FREE</option>
                                <option value="pro">PRO &ndash; {{ model.prices.symbol + model.prices.amounts.pro.discount.split('.')[0] }} / month</option>
                                <option value="business">BUSINESS &ndash; {{ model.prices.symbol + model.prices.amounts.business.discount.split('.')[0] }} / month</option>
                            </select>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div class="pull-left">
                    <a class="grey6 underline" target="_blank" href="https://www.hotjar.com/compare-our-plans">Compare Hotjar plans</a>
                </div>
                <div class="legend pull-right cursor-default" tooltip="You pay discounted rates for any extra organizations you add.">
                    <span class="legend-yellow"></span> <span class="underline-dashed">Discount applies</span>
                </div>
                <div class="clear"></div>

                <button type="button" class="btn btn-lg btn-primary" ng-click-with-loader="updateMultipleOrganizations()">
                    <span class="spinner"></span>
                    <span class="text-default">Continue</span>
                    <span class="text-loading">Please wait...</span>
                </button>

            </div>
        </form>
    </div>

    <!-- UPSELL screen -->
    <div class="form-upsell" ng-show="model.state === 'upsell'">
        <form name="forms.upsell" novalidate>
            <div class="upsell-content">
                <h2 class="font18">What's included in <span class="uppercase">{{ model.plan }}</span>:</h2>
                <ul ng-show="model.plan === 'pro'">
                    <li>Unlimited Heatmaps, Recordings, Funnels, Forms, Polls, Surveys and Recruiters.</li>
                    <li>Larger Snapshots ({{ 2000 | localeNumber }} visitors) for Heatmaps, Recordings and Forms.</li>
                    <li>Increased Sample size ({{ 10000 | localeNumber }} pageviews per day).</li>
                </ul>
                <ul ng-show="model.plan === 'business'">
                    <li>Larger Snapshots ({{ 5000 | localeNumber }} visitors) for Heatmaps, Recordings and Forms.</li>
                    <li>Increased Sample size ({{ 20000 | localeNumber }} pageviews per day).</li>
                    <li>Remove Hotjar branding from Polls, Surveys, Recruiters and shared pages.</li>
                </ul>

                <div ng-hide="model.isAccountAdminOfOrganization">
                    <h2 class="font14" ng-show="model.prices.symbol">Only {{ model.prices.symbol + model.totalCost }} per month.</h2>
                </div>

                <div ng-show="model.isAccountAdminOfOrganization">
                    <h2 class="font14" ng-if="!model.hasPaymentDetails && model.prices.symbol">Only {{ model.prices.symbol + model.totalCost }} per month.</h2>

                    <!-- Confirm payment -->
                    <div class="custom-check center inline" ng-if="model.hasPaymentDetails">
                        <input id="confirm_payment" name="confirm_payment" required type="checkbox" ng-model="model.confirmPayment" />
                        <label for="confirm_payment" class="normal" ng-class="{red: forms.upsell.submitted && forms.upsell.confirm_payment.$error.required}">
                            I understand I will be charged <span class="bold" ng-show="model.prices.symbol">{{ model.prices.symbol + model.totalCost }} per month</span>.<br />
                        </label>
                    </div>

                    <br/>

                    <button type="button" class="btn btn-lg"
                            ng-class="{'btn-hotjar': model.plan === 'pro', 'btn-gold': model.plan === 'business'}"
                            ng-click-with-loader="forms.upsell.submitted=true; updateOrganization()">
                        <span class="spinner"></span>
                        <span class="text-default">Upgrade to {{ model.plan }}</span>
                        <span class="text-loading">Please wait...</span>
                    </button>

                </div>

            </div>
        </form>
        <p class="note" ng-hide="model.isAccountAdminOfOrganization"><span class="icon-info-grey"></span>Only Account Owners can perform upgrades or downgrades.</p>
        <p class="note" ng-show="model.isAccountAdminOfOrganization">Downgrade plans anytime &ndash; directly from your account.</p>
    </div>

    <!-- COMPANY DETAILS screen -->
    <div class="form-info relative" ng-show="model.state === 'info'">
        <form name="forms.info" class="form-landscape" autocomplete="off" novalidate ng-submit-with-loader="forms.info.submitted=true; onSubmitCompanyDetails(companyDetails.company, companyDetails.address, companyDetails.countryCode, companyDetails.vatNumber)">

            <p class="note-top">For tax and invoicing purposes, please enter and verify this information about your Organization:</p>

            <!-- COMPANY NAME -->
            <div class="form-group clearfix mth" ng-class="{error: forms.info.submitted && forms.info.companyName.$invalid}">
                <label for="companyName" class="not-required">Org. Name</label>
                <div class="pull-left">
                    <input type="text" id="companyName" name="companyName" class="form-control companyName" maxlength="50" ng-model="companyDetails.company" placeholder="e.g. SomeCompany Inc.">
                    <div class="clear"></div>
                    <div class="form-error" ng-if="forms.info.submitted && forms.info.companyName.$invalid">This field is required.</div>
                </div>
            </div>

            <!-- ADDRESS -->
            <div class="form-group clearfix">
                <label for="address" class="not-required">Org. Address</label>
                <div class="pull-left">
                    <textarea id="address" name="address" class="form-control" maxlength="255" ng-model="companyDetails.address"></textarea>
                </div>
            </div>

            <!-- COUNTRY -->
            <div class="form-group clearfix" ng-class="{error: forms.info.submitted && forms.info.country.$invalid}">
                <label for="country">Country<span class="required-marker">*</span></label>
                <div class="pull-left">
                    <select id="country" name="country" class="form-control country"
                            required
                            ng-change="checkIfVatCountry()"
                            ng-model="companyDetails.countryCode"
                            ng-options="country.value as country.label for country in model.braintreeCountries">
                        <option value="">Please select...</option>
                    </select>
                    <div class="form-error" ng-if="forms.info.submitted && forms.info.country.$invalid">This field is required.</div>
                </div>
            </div>

            <!-- VAT NUMBER -->
            <div class="form-group form-group-with-addon clearfix" ng-class="{error: forms.info.submitted && forms.info.vatNumber.$invalid}" ng-show="model.isVatCountry">
                <label for="vatNumber" class="not-required">VAT Number</label>
                <div class="pull-left">
                    <span class="addon">{{ companyDetails.countryCode }}</span>
                    <input type="text" id="vatNumber" name="vatNumber" class="form-control vatNumber" maxlength="50" ng-model="companyDetails.vatNumber">
                    <div class="clear"></div>
                    <div class="form-error" ng-if="forms.info.submitted && forms.info.vatNumber.$invalid">This VAT number is invalid.</div>
                </div>
            </div>

            <!-- Button -->
            <div class="form-group mts">
                <label>&nbsp;</label>
                <div class="pull-left">
                    <button type="submit" class="btn btn-lg btn-primary">
                        <span class="spinner"></span>
                        <span class="text-default">Continue</span>
                        <span class="text-loading">Please wait...</span>
                    </button>
                </div>
                <div class="clear"></div>
            </div>

        </form>

        <a class="back-button grey6" ng-if="model.showBackButton && model.originalState !== 'info'" ng-click="goBack();">
            <span class="icon-back-small-grey"></span>Go Back
        </a>
    </div>

    <!-- PAYMENT screen -->
    <div class="form-payment relative" ng-show="model.state === 'payment'">
        <form name="forms.payment" class="form-landscape" autocomplete="off" novalidate ng-submit-with-loader="forms.payment.submitted=true; onUpdatePaymentDetails(paymentFields.nameOnCard, paymentFields.cardNumber, paymentFields.cvc, paymentFields.cardMonth, paymentFields.cardYear)">

            <p class="note-top">
                <span ng-show="model.updateOrganizationPlans.length === 1 && model.prices.symbol"><strong class="font15">Hotjar <span class="uppercase">{{ model.updateOrganizationPlans[0].plan }}</span>: {{ model.prices.symbol + model.totalCost }} per month <span class="font12 normal" ng-show="model.isVatCountry">(Excluding VAT)</span></strong><br /></span>
                <span ng-show="model.updateOrganizationPlans.length > 1 && model.prices.symbol"><strong class="font15">Total bill for {{ model.updateOrganizationPlans.length }} organizations: {{ model.prices.symbol + model.totalCost }} per month <span class="font12 normal" ng-show="model.isVatCountry">(Excluding VAT)</span></strong><br /></span>
                <span ng-show="model.originalState === 'upsell'">Enter your payment details for instant access:</span>
                <span ng-show="model.originalState !== 'upsell'">Please enter your payment details below:</span>
            </p>

            <div class="alert alert-danger mbn mtb" ng-if="forms.payment.submitted && forms.payment.errorMessage === 'invalid'">
                There was a problem with your payment details, please ensure they are correct and try again.<br />
                If the problem persists, please <a href="https://www.hotjar.com/contact" target="_blank">contact us</a> for assistance.</div>
            <div class="alert alert-danger mbn mtb" ng-if="forms.payment.submitted && forms.payment.errorMessage === 'amex'">
                We do not support American Express credit cards yet.<br />
                <a href="https://www.hotjar.com/contact" target="_blank">Contact us</a> for assistance.
            </div>

            <!-- CARD NUMBER -->
            <div class="form-group clearfix mth" ng-class="{error: forms.payment.submitted && forms.payment.cardNumber.$invalid}">
                <label for="cardNumber">Card number<span class="required-marker">*</span></label>
                <div class="pull-left">
                    <input type="text" id="cardNumber" name="cardNumber" class="form-control inline cardNumber" required maxlength="19" ng-model="paymentFields.cardNumber">
                    <div class="inline payment-options mlm" tooltip="Accepted Credit Cards: Visa, Master Card, Maestro, Maestro UK and Discover." data-tooltip-append-to-body="true"></div>
                    <div class="clear"></div>
                    <div class="form-error" ng-if="forms.payment.submitted && forms.payment.cardNumber.$invalid">This field is required.</div>
                </div>
            </div>

            <!-- VALID UNTIL -->
            <div class="form-group clearfix" ng-class="{error: forms.payment.submitted && (forms.payment.cardMonth.$invalid || forms.payment.cardYear.$invalid)}">
                <label>Valid until<span class="required-marker">*</span></label>
                <div class="pull-left">
                    <select id="cardMonth" name="cardMonth" class="form-control inline cardMonth" required ng-model="paymentFields.cardMonth" ng-options="month.value as month.label for month in model.months">
                    </select>
                    <select id="cardYear" name="cardYear" class="form-control inline cardYear" required ng-model="paymentFields.cardYear" ng-options="year.value as year.label for year in model.futureYears">
                    </select>
                    <div class="clear"></div>
                    <div class="form-error" ng-if="forms.payment.submitted && (forms.payment.cardMonth.$invalid || forms.payment.cardYear.$invalid)">Please specify expiry month and year.</div>
                </div>
            </div>

            <!-- CVC -->
            <div class="form-group clearfix" ng-class="{error: forms.payment.submitted && forms.payment.cvc.$invalid}">
                <label for="cvc">CVC<span class="required-marker">*</span></label>
                <div class="pull-left">
                    <input type="text" id="cvc" name="cvc" class="form-control inline cvc" required ng-model="paymentFields.cvc" maxlength="5">
                    <span class="inline icon-info info-cvc mlm" tooltip="The CVC number is a 3 or 4 digit security code printed on the front or back of your card." data-tooltip-append-to-body="true"></span>
                    <div class="clear"></div>
                    <div class="form-error" ng-if="forms.payment.submitted && forms.payment.cvc.$invalid">This field is required.</div>
                </div>
            </div>

            <!-- NAME ON CARD -->
            <div class="form-group clearfix" ng-class="{error: forms.payment.submitted && forms.payment.nameOnCard.$invalid}">
                <label for="nameOnCard">Name on card<span class="required-marker">*</span></label>
                <div class="pull-left">
                    <input type="text" id="nameOnCard" name="nameOnCard" class="form-control nameOnCard" maxlength="50" required ng-model="paymentFields.nameOnCard">
                    <div class="clear"></div>
                    <div class="form-error" ng-if="forms.payment.submitted && forms.payment.nameOnCard.$invalid">This field is required.</div>
                </div>
            </div>

            <!-- Button -->
            <div class="form-group mts">
                <label>&nbsp;</label>
                <div class="pull-left">
                    <button type="submit" class="btn btn-lg btn-primary">
                        <span class="spinner"></span>
                        <span class="text-default">Update details</span>
                        <span class="text-loading">Please wait...</span>
                    </button>
                </div>
                <div class="clear"></div>
            </div>

        </form>
        <p class="note" ng-show="model.isAccountAdminOfOrganization">Downgrade plans anytime &ndash; directly from your account.</p>
        <a class="back-button grey6" ng-if="model.showBackButton" ng-click="goBack();">
            <span class="icon-back-small-grey"></span>Go Back
        </a>
    </div>
</div>
    </script>
<script id="modal_prompt.html" type="text/ng-template">
        <div class="modal-body">
    <h2 ng-bind="header"></h2>
    <span ng-bind="text"></span>
</div>
<div class="modal-footer">
    <button class="btn btn-danger pull-left" ng-click="posClick()" ng-bind="posButtonText"></button>
    <button class="btn pull-right" ng-click="negClick()" ng-bind="negButtonText"></button>
    <div class="clear"></div>
</div>
    </script>
<script id="modal_targeting.html" type="text/ng-template">
        <div class="modal-body">
    <button type="button" class="close" ng-click="$close()">&times;</button>
    <h2 class="modal-title">
        Page targeting
        <a class="grey9 font12 mls underline" href="http://inside.hotjar.com/support/solutions/articles/1000144746-page-targeting-how-do-the-different-page-match-options-work-" target="_blank"
           tooltip-html-unsafe="Click for info on how<br />page targeting works"
           tooltip-placement="bottom"
           tooltip-append-to-body="true">How does this work?</a>
    </h2>

    <p class="sub-title" ng-show="model.module === 'polls'">Show the Poll on the following pages:</p>
    <p class="sub-title" ng-show="model.module === 'testers'">Show the Recruiter on the following pages:</p>
    <p class="sub-title" ng-show="model.module === 'survey'">Show the Survey invite popup on the following pages:</p>
    <ul ng-show="model.target.targeting.length > 0">
        <li ng-repeat="targetingRule in model.target.targeting" class="wordwrap">
                    <span ng-if="getMatchOperationLabel(targetingRule.match_operation) === 'Simple match' || getMatchOperationLabel(targetingRule.match_operation) === 'Exact match'">
                        {{ UserData.currentSite.url }}{{ targetingRule.pattern }}
                        &nbsp;<span>({{ getMatchOperationLabel(targetingRule.match_operation) }})</span>
                    </span>
                    <span ng-if="getMatchOperationLabel(targetingRule.match_operation) === 'Starts with'">
                        Pages that start with '{{ UserData.currentSite.url }}{{ targetingRule.pattern }}'
                    </span>
                    <span ng-if="getMatchOperationLabel(targetingRule.match_operation) === 'Ends with'">
                        Pages that end with '{{ targetingRule.pattern }}'
                    </span>
                    <span ng-if="getMatchOperationLabel(targetingRule.match_operation) === 'Contains'">
                        Pages that contain '{{ targetingRule.pattern }}'
                    </span>
                    <span ng-if="getMatchOperationLabel(targetingRule.match_operation) === 'Regular expression'">
                        Pages that match the regular expression '{{ targetingRule.pattern }}'
                    </span>
            <a ng-click="removeTargetingRule(targetingRule)">remove</a>
        </li>
    </ul>

    <form ng-submit="addTargetingRule()" name="targetingForm" ng-class="{clean: model.formError}" novalidate>
        <div class="pull-left">
            <select class="form-control inline" ng-model="model.newTargetingRule.matchOperation" ng-options="op.value as op.label for op in matchOperationChoices" ng-change="resetErrors()">
            </select>
            <input type="text" class="form-control inline" ng-model="model.newTargetingRule.pattern" ng-change="checkValidMatchOperation()" maxlength="750" required placeholder="{{ model.placeholderText[model.newTargetingRule.matchOperation] }}"/>
        </div>
        <button type="submit" class="btn btn-primary add-button pull-right" ng-disabled="targetingForm.$invalid">Add</button>
        <div class="clear"></div>
    </form>
    <div class="alert alert-info alert-field red" ng-show="model.formError">{{model.formError}}&nbsp;&nbsp;&nbsp;<a href="http://inside.hotjar.com/support/solutions/articles/1000144746-page-targeting-how-do-the-different-page-match-options-work-" target="_blank">Learn more</a></div>
    <div class="alert alert-info alert-field" ng-hide="model.formError" ng-switch="model.newTargetingRule.matchOperation">
                <span ng-switch-when="simple">
                    TIP: Copy and Paste the URL of the page you want to target.
                </span>
                <span ng-switch-when="exact">
                    TIP: Copy and Paste the URL of the page you want to target.
                </span>
                <span ng-switch-when="starts_with">
                    <strong>Starts with</strong> allows you to target all pages which URL start in a certain way.
                </span>
                <span ng-switch-when="ends_with">
                    <strong>Ends with</strong> allows you to target all pages which URL ends in a certain way.
                </span>
                <span ng-switch-when="contains">
                    <strong>Contains</strong> allows you to target all pages which contain something specific.
                </span>
                <span ng-switch-when="regex">
                    <strong>Regular expression</strong> <a href="http://inside.hotjar.com/support/solutions/articles/1000145265-how-can-i-use-regular-expressions-to-target-multiple-pages-" target="_blank">Learn more about regular expressions</a>.
                </span>
    </div>

    <div class="alert alert-info man mtb" ng-show="model.module === 'polls' || model.module === 'testers'">
        If two widgets are set up on the same page, the last one created will appear on the page.
    </div>

</div>
    </script>
<script id="modal_tracking_code.html" type="text/ng-template">
        <div class="modal-body">
    <button type="button" class="close close-dark" ng-click="$close()">&times;</button>
    <div ng-if="model.checkScriptResult === 'failure'" class="alert alert-warning alert-modal-top">{{ model.site.url }} has not reported any data in the past 24 hours.</div>
    <div ng-if="model.checkScriptResult === 'success'" class="alert alert-success alert-modal-top">{{ model.site.url }} reported data within the past {{ model.lastDataAge }}.</div>
    <h2 class="modal-title">Your Hotjar Tracking Code:</h2>
    <p>The Tracking Code below should be placed in the &lt;head&gt; tag of every page you want to track on your site.</p>
    <div class="tracking-code-holder mbh" data-tour-step="code">
        <pre class="scroll"><snippet id="snippet" highlight-content site-id="{{ model.site.id }}" site-name="{{ model.site.name }}"></snippet></pre>
        <button type="button" class="btn btn-sm" clip-copy clip-copy-selector="#snippet">Copy Tracking Code</button>
    </div>
    <div class="clear"></div>
    <div class="alert alert-info mbn" ng-class="{'alert-multiline': model.checkScriptResult === 'failure'}">
        <strong>Tracking Code status is updated every hour.</strong>
                    <span ng-if="model.checkScriptResult === 'failure'">
                    <br /><br />
                    For help on how to install Hotjar, please refer to our <a href="http://inside.hotjar.com/support/solutions/articles/1000163656-installing-hotjar" target="_blank">step by step installation guide</a> for <a href="http://inside.hotjar.com/support/solutions/articles/1000125978-installing-hotjar-on-your-website-manually" target="_blank">manual installation</a>, <a href="http://inside.hotjar.com/support/solutions/articles/1000134312-installing-hotjar-using-google-tag-manager" target="_blank">Google Tag Manager</a>, <a href="http://inside.hotjar.com/support/solutions/articles/1000127013-installing-hotjar-on-your-wordpress-website" target="_blank">Wordpress</a>, <a href="http://inside.hotjar.com/support/solutions/articles/1000150533-installing-hotjar-on-your-prestashop-store" target="_blank">Prestashop</a>, etc.
                    </span>
    </div>
</div>
    </script>
<script id="modal_set_company_details.html" type="text/ng-template">
        <div ng-show="model.step === 1">
    <form name="forms.setCompanyDetails" class="form-landscape" ng-submit-with-loader="submitted=true; goToStepTwo()" novalidate>
        <div class="modal-body">
            <h2 class="modal-title">Review your billing details</h2>

            <p>
                Thanks for using Hotjar! For tax and invoicing purposes, we need to collect your Value Added Tax (VAT) number and the full name and address of your Organization.
                <br />
                <strong>No VAT number? You can <a class="inherit-color underline" ng-click="submitted=true; goToStepTwo()">skip this step</a> or add it later.</strong>
            </p>

            <!-- COMPANY NAME -->
            <div class="form-group clearfix mth" ng-class="{error: submitted && forms.setCompanyDetails.companyName.$invalid}">
                <label for="companyName" class="not-required">Org. Name</label>
                <div class="pull-left">
                    <input type="text" id="companyName" name="companyName" class="form-control companyName" maxlength="50" ng-model="model.company" placeholder="e.g. SomeCompany Inc.">
                    <div class="clear"></div>
                    <div class="form-error" ng-if="submitted && forms.setCompanyDetails.companyName.$invalid">This field is required.</div>
                </div>
            </div>

            <!-- ADDRESS -->
            <div class="form-group clearfix">
                <label for="address" class="not-required">Org. Address</label>
                <div class="pull-left">
                    <textarea id="address" name="address" class="form-control" maxlength="250" ng-model="model.address"></textarea>
                </div>
            </div>

            <!-- COUNTRY -->
            <div class="form-group clearfix" ng-class="{error: submitted && forms.setCompanyDetails.country.$invalid}">
                <label for="country">Country<span class="required-marker">*</span></label>
                <div class="pull-left">
                    <select id="country" name="country" class="form-control country"
                            required
                            ng-change="checkIfVatCountry()"
                            ng-model="model.countryCode"
                            ng-options="country.value as country.label for country in model.braintreeCountries">
                        <option value="">Please select...</option>
                    </select>
                    <div class="form-error" ng-if="submitted && forms.setCompanyDetails.country.$invalid">This field is required.</div>
                </div>
            </div>

            <!-- VAT NUMBER -->
            <div class="form-group form-group-with-addon clearfix" ng-class="{error: submitted && forms.setCompanyDetails.vatNumber.$invalid}" ng-if="model.isVatCountry">
                <label for="vatNumber" class="not-required">VAT Number</label>
                <div class="pull-left">
                    <span class="addon">{{ model.countryCode }}</span>
                    <input type="text" id="vatNumber" name="vatNumber" class="form-control vatNumber" maxlength="50" ng-model="model.vatNumber">
                    <div class="clear"></div>
                    <div class="form-error" ng-if="submitted && forms.setCompanyDetails.vatNumber.$invalid">This VAT number is invalid.</div>
                </div>
            </div>

            <!-- Button -->
            <div class="form-group mts">
                <label>&nbsp;</label>
                <div class="pull-left">
                    <button type="submit" class="btn btn-lg btn-primary">
                        <span class="spinner"></span>
                        <span class="text-default">Continue</span>
                        <span class="text-loading">Please wait...</span>
                    </button>
                </div>
                <div class="clear"></div>
            </div>

        </div>
    </form>
</div>

<div ng-show="model.step === 2">
    <form name="forms.confirm" class="form-landscape" ng-submit-with-loader="submitted=true; updateCompanyDetails()" novalidate>
        <div class="modal-body step2 relative">
            <div class="form-confirm">
                <h2 class="modal-title">Review your billing details</h2>

                <p ng-show="model.countryCode !== 'MT'">
                    <strong>NOTE:</strong> Failure to supply a Value Added Tax (VAT) number requires us to add {{ prices.symbol }}{{ prices.vatAmount }} to your next bill ({{ prices.vatRate }}% VAT).
                    <br/><br/>
                    Don't have your VAT number available? You can add it later from 'Plans &amp; Billing' in your Settings.
                </p>
                <p ng-show="model.countryCode === 'MT'">
                    <strong>NOTE:</strong> We are required to add {{ prices.symbol }}{{ prices.vatAmount }} to your next bill ({{ prices.vatRate }}% VAT). Invoices that can be used to reclaim this charge, will be available within the 'Billing History' in your account.
                    <br/><br/>
                    Don't have your VAT number available? You can add it later from 'Plans &amp; Billing' in your Settings.
                </p>

                <div class="center mth">
                    <button type="submit" class="btn btn-lg btn-primary">
                        <span class="spinner"></span>
                        <span class="text-default">Agree &amp; Continue</span>
                        <span class="text-loading">Please wait...</span>
                    </button>
                </div>

                <a class="back-button grey6" ng-click="goBack();">
                    <span class="icon-back-small-grey"></span>Go Back
                </a>
            </div>
        </div>
    </form>
</div>
    </script>
<notification></notification>
<loader></loader>
<body class="{{ pageClass() }} pagetype-{{ pageType() }} user-{{ UserData.loggedIn }}" isaccountadmin="{{UserData.get('isAccountAdmin')}}" ng-class="{ 'sidebar-minimized': state.minimized }" ng-cloak="" ng-controller="AppCtrl" useraccess="{{ UserData.currentSite.organization.access }}">
<div id="site-container">
<!-- TOP BAR -->
<div id="topbar" ng-if="UserData.loggedIn &amp;&amp; !shouldHideFramework()">
<a id="logo" ng-style="{'background-image': 'url(' + model.accountLogo + ')'}" ui-sref="dashboard({siteId: UserData.currentSite.id})"></a>
<!-- SITES BUTTON -->
<div class="pull-left menu-button" data-tour-step="manage-sites" dropdown="">
<div class="dropdown-toggle" id="site-menu-button" ng-click="filterFocus()">
<span class="icon-site-dark"></span>
<span class="caret caret-light"></span>
</div>
<div class="dropdown-menu" id="site-menu" ng-click="$event.stopPropagation()" role="menu">
<div class="site-filter">
<span class="icon-x" ng-click="resetSiteNameFilter()" ng-if="model.nameFilter"></span>
<input class="form-control" id="site-name-filter" ng-change="scrollSiteListTop()" ng-click="$event.stopPropagation()" ng-model="model.nameFilter" placeholder="Filter by site name or Organization" type="text"/>
</div>
<div class="sites" id="sites-list-holder">
<div class="site" ng-repeat="site in getFilteredSiteList() track by site.id">
<div class="wordwrap" ng-if="site.organization.name !== getFilteredSiteList()[$index-1].organization.name">{{ site.organization.name }}</div>
<a class="wordwrap" ng-class="{active: site.id == UserData.currentSite.id &amp;&amp; pageType() !== 'setting'}" ng-click="changeSite(site)">
                                    {{ site.name }}
                                </a>
</div>
<div class="empty wordwrap bold" ng-if="getFilteredSiteList().length === 0">
                                No sites or Organizations for "{{model.nameFilter}}". <a ng-click="resetSiteNameFilter()">Reset filter</a>
</div>
</div>
<div class="dropdown-footer">
<a class="grey6" ui-sref="site.list">Sites &amp; Organizations settings</a>
<a class="grey6" ng-show="UserData.get('isAdminOfOneOrganization')" ui-sref="site.create">Add new site</a>
</div>
</div>
</div>
<!-- ADD BUTTON -->
<div class="pull-left menu-button" dropdown="" ng-show="UserData.get('isAdminOfOneOrganization')">
<div class="dropdown-toggle">
<span class="icon-add-dark"></span>
<span class="caret caret-light"></span>
</div>
<ul class="dropdown-menu" role="menu">
<li><a ui-sref="site.create">Add new site</a></li>
<li><a ui-sref="user.invite">Invite people</a></li>
</ul>
</div>
<div class="pull-right">
<!-- HELP BUTTON -->
<div class="pull-left menu-button" data-tour-step="docs" dropdown="">
<div class="dropdown-toggle">
<span class="icon-help"></span>
<span class="caret caret-light"></span>
</div>
<ul class="dropdown-menu dropdown-menu-right" role="menu">
<li><a href="http://inside.hotjar.com/support/home" target="_blank">Documentation &amp; FAQs</a></li>
<li class="divider"></li>
<li><a href="http://inside.hotjar.com/support/discussions" target="_blank">Hotjar Community</a></li>
<li><a href="http://inside.hotjar.com/support/discussions/topics/1000039114/" target="_blank">Product Roadmap</a></li>
</ul>
</div>
<!-- SETTINGS BUTTON -->
<div class="pull-left menu-button" data-tour-step="settings" dropdown="">
<div class="dropdown-toggle">
<span class="icon-cog"></span>
<span class="caret caret-light"></span>
</div>
<ul class="dropdown-menu dropdown-menu-right" role="menu">
<li required-access="account-admin"><a ui-sref="settings.account">Account Settings</a></li>
<li required-access="account-admin"><a ui-sref="settings.billing">Plans &amp; Billing</a></li>
<li required-access="account-admin"><a ui-sref="settings.billingHistory">Billing History</a></li>
<li class="divider" required-access="account-admin"></li>
<li><a ui-sref="user.list">Users</a></li>
<li><a ui-sref="site.list">Sites &amp; Organizations</a></li>
<li ng-show="UserData.get('isAdminOfOneOrganization')"><a ui-sref="settings.ip">IP Blocking</a></li>
<li class="divider"></li>
<li><a ui-sref="profile.details">Settings for {{ UserData.get('userName') }}</a></li>
<li><a ng-click="logout();">Logout</a></li>
</ul>
</div>
<!-- FEEDBACK BUTTON -->
<a class="pull-left menu-button btn-intercom" data-tour-step="feedback" href="mailto:c5ke8zbr@incoming.intercom.io">
<span class="icon-feedback"></span>
</a>
</div>
</div>
<!-- STICKY NOTIFICATION -->
<sticky-notification ng-show="UserData.loggedIn &amp;&amp; !shouldHideFramework()"></sticky-notification>
<!-- SITE BAR -->
<div class="sitebar-feature" id="sitebar" ng-if="UserData.loggedIn &amp;&amp; !shouldHideFramework()">
<a class="current-site pull-left ellipsis" ui-sref="dashboard({siteId: UserData.currentSite.id})">
                    {{ UserData.currentSite.name }}
                </a>
<div class="current-org pull-left">
<span class="icon-organization-white middle"></span>
<span class="org-name ellipsis">{{ UserData.currentSite.organization.name }}</span>
<span class="badge badge-plan badge-plan-lg {{UserData.currentSite.organization.effective_plan}}" ng-click="showUpsellModal('site-bar-link', 'pro')" ng-if="UserData.currentSite.organization.effective_plan === 'basic'">{{UserData.currentSite.organization.effective_plan}}</span>
<span class="badge badge-plan badge-plan-lg {{UserData.currentSite.organization.effective_plan}}" ng-if="UserData.currentSite.organization.effective_plan !== 'basic'">{{UserData.currentSite.organization.effective_plan}}</span>
</div>
<div class="upsell-link pull-left" ng-if="UserData.currentSite.organization.effective_plan === 'basic'"><a ng-click="showUpsellModal('site-bar-link', 'pro')">Get unlimited insights</a> with Hotjar Pro</div>
<div class="tracking-code pull-right tracking-code-inactive" data-tooltip-append-to-body="true" data-tooltip-placement="bottom" ng-click="showTrackingCodeModal(UserData.currentSite)" ng-show="UserData.currentSite.getTrackingState() === 'notActive' || UserData.currentSite.getTrackingState() === 'neverActive'" tooltip="No data received in the past hour.">
<span class="icon-warning-small"></span>Tracking Inactive
                </div>
<div class="tracking-code pull-right tracking-code-active" data-tooltip-append-to-body="true" data-tooltip-placement="bottom" ng-click="showTrackingCodeModal(UserData.currentSite)" ng-show="UserData.currentSite.getTrackingState() === 'active'" tooltip="Click for Tracking Code">
<span class="icon-success-small middle"></span>Tracking Active
                </div>
</div>
<div class="sitebar-setting" id="sitebar" ng-if="UserData.loggedIn &amp;&amp; !shouldHideFramework()">
<a class="current-site pull-left" ui-sref="dashboard({siteId: UserData.currentSite.id})">
<span class="icon-back icon-before"></span>Go back to {{ UserData.currentSite.name }}
                </a>
</div>
<!-- SIDEBAR -->
<div id="sidebar" ng-if="UserData.loggedIn &amp;&amp; !shouldHideFramework()">
<a class="sidebar-toggle" ng-class="{minimized: state.minimized}" ng-click="toggleMinimized()">
<span></span>
</a>
<!-- SETTINGS SIDEBAR -->
<div class="menu" ng-if="pageType() === 'setting'">
<ul class="sidebar-menu" data-tour-step="sidebar">
<li required-access="account-admin" ui-sref-active="active">
<a class="minimized-view relative" href="#" tooltip="Account Settings" tooltip-append-to-body="true" tooltip-placement="right" ui-sref="settings.account">
<span class="icon-account-settings"></span>
</a>
<a class="expanded-view relative" href="#" ui-sref="settings.account">
<span class="icon-account-settings"></span>
<span>Account Settings</span>
</a>
</li>
<li required-access="account-admin" ui-sref-active="active">
<a class="minimized-view relative" href="#" tooltip="Plans &amp; Billing" tooltip-append-to-body="true" tooltip-placement="right" ui-sref="settings.billing">
<span class="icon-billing-sidebar"></span>
</a>
<a class="expanded-view relative" href="#" ui-sref="settings.billing">
<span class="icon-billing-sidebar"></span>
<span>Plans &amp; Billing</span>
</a>
</li>
<li required-access="account-admin" ui-sref-active="active">
<a class="minimized-view relative" href="#" tooltip="Billing History" tooltip-append-to-body="true" tooltip-placement="right" ui-sref="settings.billingHistory">
<span class="icon-billing-history-sidebar"></span>
</a>
<a class="expanded-view relative" href="#" ui-sref="settings.billingHistory">
<span class="icon-billing-history-sidebar"></span>
<span>Billing History</span>
</a>
</li>
<li ui-sref-active="active">
<a class="minimized-view relative" href="#" tooltip="Users" tooltip-append-to-body="true" tooltip-placement="right" ui-sref="user()">
<span class="icon-users"></span>
</a>
<a class="expanded-view relative" href="#" ui-sref="user()">
<span class="icon-users"></span>
<span>Users</span>
</a>
</li>
<li ui-sref-active="active">
<a class="minimized-view relative" href="#" tooltip="Sites &amp; Organizations" tooltip-append-to-body="true" tooltip-placement="right" ui-sref="site()">
<span class="icon-site-sidebar"></span>
</a>
<a class="expanded-view relative" href="#" ui-sref="site()">
<span class="icon-site-sidebar"></span>
<span>Sites &amp; Organizations</span>
</a>
</li>
<li ng-show="UserData.get('isAdminOfOneOrganization')" ui-sref-active="active">
<a class="minimized-view relative" href="#" tooltip="IP Blocking" tooltip-append-to-body="true" tooltip-placement="right" ui-sref="settings.ip">
<span class="icon-ip"></span>
</a>
<a class="expanded-view relative" href="#" ui-sref="settings.ip">
<span class="icon-ip"></span>
<span>IP Blocking</span>
</a>
</li>
<li ui-sref-active="active">
<a class="minimized-view relative" href="#" tooltip="Settings for {{ UserData.get('userName') }}" tooltip-append-to-body="true" tooltip-placement="right" ui-sref="profile()">
<img class="icon- avatar" ng-src="{{ UserData.get('userPictureUrl') }}"/>
</a>
<a class="expanded-view relative ellipsis" href="#" ui-sref="profile()">
<img class="icon- avatar" ng-src="{{ UserData.get('userPictureUrl') }}"/>
<span>{{ UserData.get('userName') }}</span>
</a>
</li>
</ul>
</div>
<!-- FEATURES SIDEBAR -->
<div class="menu" ng-if="pageType() === 'feature'">
<ul class="sidebar-menu" data-tour-step="sidebar">
<li ui-sref-active="active">
<a class="minimized-view relative" href="#" tooltip="{{ UserData.currentSite.name }} Dashboard" tooltip-append-to-body="true" tooltip-placement="right" ui-sref="dashboard({siteId: UserData.currentSite.id})">
<span class="icon-dashboard"></span>
</a>
<a class="expanded-view relative" href="#" ui-sref="dashboard({siteId: UserData.currentSite.id})">
<span class="icon-dashboard"></span>
<span>Site Dashboard</span>
</a>
</li>
<label class="expanded-view">Analysis</label>
<li ui-sref-active="active">
<a class="minimized-view" href="#" tooltip="Heatmap" tooltip-append-to-body="true" tooltip-placement="right" ui-sref="heatmap({siteId: UserData.currentSite.id})">
<span class="icon-heatmaps"></span>
</a>
<a class="expanded-view" href="#" ui-sref="heatmap({siteId: UserData.currentSite.id})">
<span class="icon-heatmaps"></span>
<span>Heatmaps</span>
</a>
</li>
<li ui-sref-active="active">
<a class="minimized-view" href="#" tooltip="Recordings" tooltip-append-to-body="true" tooltip-placement="right" ui-sref="playbacks({siteId: UserData.currentSite.id})">
<span class="icon-playbacks"></span>
</a>
<a class="expanded-view" href="#" ui-sref="playbacks({siteId: UserData.currentSite.id})">
<span class="icon-playbacks"></span>
<span>Recordings</span>
</a>
</li>
<li ui-sref-active="active">
<a class="minimized-view" href="#" tooltip="Funnels" tooltip-append-to-body="true" tooltip-placement="right" ui-sref="funnel({siteId: UserData.currentSite.id})">
<span class="icon-funnels"></span>
</a>
<a class="expanded-view" href="#" ui-sref="funnel({siteId: UserData.currentSite.id})">
<span class="icon-funnels"></span>
<span>Funnels</span>
</a>
</li>
<li ui-sref-active="active">
<a class="minimized-view" href="#" tooltip="Forms" tooltip-append-to-body="true" tooltip-placement="right" ui-sref="form({siteId: UserData.currentSite.id})">
<span class="icon-forms"></span>
</a>
<a class="expanded-view" href="#" ui-sref="form({siteId: UserData.currentSite.id})">
<span class="icon-forms"></span>
<span>Forms</span>
</a>
</li>
<label class="expanded-view">Feedback</label>
<li ui-sref-active="active">
<a class="minimized-view" href="#" tooltip="Polls" tooltip-append-to-body="true" tooltip-placement="right" ui-sref="polls({siteId: UserData.currentSite.id})">
<span class="icon-polls"></span>
</a>
<a class="expanded-view" href="#" ui-sref="polls({siteId: UserData.currentSite.id})">
<span class="icon-polls"></span>
<span>Polls</span>
</a>
</li>
<li ui-sref-active="active">
<a class="minimized-view" href="#" tooltip="Surveys" tooltip-append-to-body="true" tooltip-placement="right" ui-sref="survey({siteId: UserData.currentSite.id})">
<span class="icon-surveys"></span>
</a>
<a class="expanded-view" href="#" ui-sref="survey({siteId: UserData.currentSite.id})">
<span class="icon-surveys"></span>
<span>Surveys</span>
</a>
</li>
<li ui-sref-active="active">
<a class="minimized-view" href="#" tooltip="Recruiters" tooltip-append-to-body="true" tooltip-placement="right" ui-sref="testers({siteId: UserData.currentSite.id})">
<span class="icon-testers"></span>
</a>
<a class="expanded-view" href="#" ui-sref="testers({siteId: UserData.currentSite.id})">
<span class="icon-testers"></span>
<span>Recruiters</span>
</a>
</li>
</ul>
</div>
<div id="social">
<a class="btn btn-twitter expanded-view" href="https://twitter.com/intent/follow?screen_name=@HotjarApps"><span class="icon-twitter"></span>Follow us on twitter</a>
<a class="btn btn-twitter minimized-view" href="https://twitter.com/intent/follow?screen_name=@HotjarApps" tooltip="Follow us on twitter" tooltip-append-to-body="true" tooltip-placement="right"><span class="icon-twitter man"></span></a>
<script src="//platform.twitter.com/widgets.js"></script>
</div>
</div>
<div ng-attr-id="{{ shouldHideFramework() ? '' : 'content-container' }}" ui-view="">
</div>
<div class="clear"></div>
</div>
</body>
</html>