(function($) {
	expandAll = 0;
	cchDefined = false;
	$(document).ready( function() {
		widthSet = false;
		ww = viewport().width;
		
		//set global navigation sub-columns
		doCols();
		//Responsive Design, Activate!
		responsiveActivate();
		
		$('#searchform input#s').focus(function() {
			if($(this).val() == 'Search') {
				$(this).val('');
			}
		});
		$('#searchform input#s').blur(function() {
			if($(this).val() == '') {
				$(this).val('Search');
			}
		});
		
		setTimeout(function(){
			centerColH = $('.center-col').height();
			rightColH = $('.right-col').height();
			if(rightColH > centerColH) {
					$('.center-col').css('height', rightColH);
					centerColH = $('.center-col').height();
					cchDefined = true;
			}
		}, 100);
		
		$(window).resize(function() {
			ww = viewport().width;
			respondResize();
			if(ww >= 820 && widthSet != true) {
				setWidths();
			}
		});
		
		//Accordion Content
		$('.accordion-content .expand-content').click(function() {
			var contentId = $(this).parent().attr('id');
			displayContent(contentId);
		});		
		
		$('.expand-them-all').click(function() {
			var expandAllLink = $(this);
			expandAllLink.toggleClass('active');
			if(expandAllLink.html().indexOf('Expand') > -1) {
				expandAll = 1;
				expand = true;
				expandAllLink.html('Collapse All <span>x</span>');
				if(cchDefined) {
					$('.center-col').css('height', 'auto');
				}
			}
			else {
				expandAll = 0;
				expand = false;
				expandAllLink.html('Expand All <span>+</span>');
				if(cchDefined) {
					$('.center-col').css('height', centerColH);
				}
			}
			
			$('.accordion-content').each(function() {
				expandLink = $(this).find('.expand-content');
				accordionDeet = $(this).find('.show-hide');
				linkTxt = expandLink.html();

				if(linkTxt.indexOf('Expand') > -1 && expand == true) {
					expandLink.html('Close <span>x</span>');
					expandLink.addClass('active');
				}
				else if(linkTxt.indexOf('Close') > -1 && expand == false) {
					expandLink.html('Expand <span>+</span>');
					expandLink.removeClass('active');
				}
				if(expand == true) {
					accordionDeet.slideDown();
				}
				else {
					accordionDeet.slideUp();
				}
			});
		});

		$('.left-nav .focus-areas .sub-pages a').click(function() {
			thisNav = $(this);
			var contentId = thisNav.attr('href').replace('/social-responsibility/focus-areas/#', '');
			displayContent(contentId);
		});
		$('.left-nav .enterprise-strategy a, #access .sub-pages .enterprise-strategy a').click(function() {
			thisNav = $(this);
			var contentId = thisNav.attr('href').replace('/about-itw/how-we-work/#', '');
			displayContent(contentId);
		});

		//IE fixes 
		if ( $.browser.msie ) {
			if ( $.browser.version <= 8 ) {
				$('.entry-content h3.orange-title:first').css({'border': '0px', 'margin-top': '20px', 'padding-top': '0px'});
			}
		}
		
		$('.left-col .left-nav > ul').click(function() {
			if(ww < 964) {
				$(this).children('li:not(.nav-header)').slideToggle();
				$(this).children('li.nav-header').toggleClass('active');
			}
		});
		$('#access > ul').click(function() {
			if(ww < 820) {
				$(this).children('li:not(.nav-header)').slideToggle();
				$(this).children('li.nav-header').toggleClass('active');
			}
		});
	});

	if(window.location.hash) {
		contentId = window.location.hash.replace('#', '');
		displayContent(contentId);
	}
	
	//show/hide accordion content
	function displayContent(contentId) {
		$('.accordion-content').each(function() {
			if($(this).attr('id') == contentId) {
				var expandLink = $(this).find('.expand-content');
				if(expandLink.hasClass('active')) {
					hideContent(contentId);
				}
				else {
					expandLink.toggleClass('active');
					
					var contentDeet = expandLink.siblings('.show-hide');
					var linkTxt = expandLink.html();
					
					if(linkTxt.indexOf('Expand') > -1) {
						expandLink.html('Close <span>x</span>');
						if(cchDefined) {
							$('.center-col').css('height', '100%');
						}
					}
					else {
						expandLink.html('Expand <span>+</span>');
						if(cchDefined) {
							$('.center-col').css('height', centerColH);
						}
					}
					contentDeet.slideToggle();
				}
			}
			else {
				if(expandAll == 0) {
					hideContent($(this).attr('id'));
				}
			}
		});
		$('.focus-areas li a').each(function() {
			thisHref = $(this).attr('href').replace('/social-responsibility/focus-areas/#', '');
			if(thisHref == contentId) {
				$(this).parent().addClass('current_page_item');
			}
			else {
				$(this).parent().removeClass('current_page_item');
			}
		});
	}
	function hideContent(contentId) {
		$('.accordion-content').each(function() {
			if($(this).attr('id') == contentId) {
				var expandLink = $(this).find('.expand-content');
				if(expandLink.hasClass('active')) {
					expandLink.removeClass('active');
					var contenttDeet = expandLink.siblings('.show-hide');
					var linkTxt = expandLink.html('Expand <span>+</span>');
					contenttDeet.slideToggle(function() {
						$('.focus-areas li a').each(function() {
							var thisHref = $(this).attr('href').replace('/social-responsibility/focus-areas/#', '');
							if(thisHref == contentId) {
								$(this).parent('li').removeClass('current_page_item');
							}
						});
					});
				}
			}
		});
	}
	
	function responsiveActivate() {
		if(ww < 964) {
			if($('.left-col').length > 0) {
				$('#access ul:first > li').each(function() {
					if($(this).hasClass('current_page_ancestor') || $(this).hasClass('current_page_item')) {
						pagename = $(this).children('a:first').text();
					}
				});
				if(typeof pagename !== 'undefined') {
					$('.left-col .left-nav > ul').prepend('<li class="nav-header">More in ' + pagename +'</li>');
				}
				else {
					$('.left-col .left-nav > ul').prepend('<li class="nav-header">More</li>');
				}
			}
		}
		if(ww < 820) {
			$('#access > ul').prepend('<li class="nav-header">Menu</li>');
		}
	}
	
	function respondResize() {
		if(ww >= 964) {
			$('.left-col .left-nav > ul > li').show();
			$('.left-col .left-nav .nav-header').hide();
		}
		if(ww >= 820) {
			$('#access > ul li').show();
			$('#access > ul .nav-header').hide();
		}
		if(ww < 964) {
			$('.left-col .left-nav > ul > li').hide();
			
			if($('.left-col .left-nav .nav-header').length == 0) {
				$('#access ul:first > li').each(function() {
					if($(this).hasClass('current_page_ancestor') || $(this).hasClass('current_page_item')) {
						pagename = $(this).children('a:first').text();
					}
				});
				if(typeof pagename !== 'undefined') {
					$('.left-col .left-nav > ul').prepend('<li class="nav-header">More in ' + pagename +'</li>');
				}
				else {
					$('.left-col .left-nav > ul').prepend('<li class="nav-header">More</li>');
				}
			}
			else {
				$('.left-col .left-nav .nav-header').show();
			}
		}
		if(ww < 820) {
			$('#access > ul li').hide();
			
			if($('#access > ul .nav-header').length == 0) {
				$('#access > ul').prepend('<li class="nav-header">Menu</li>');
			}
			else {
				$('#access > ul .nav-header').show();
			}
		}
	}
	
	//Set global navigation columns
  function doCols() {
	  var pages, page, page_children, ul_right, ul_left, li_left, li_right, len, half, left_child, i;
	  
   	  pages = $('#access .sub-pages');

	  pages.each( function(index) {
		 page = $(this);
		 page_children = page.children();	
	
		 if( page_children.length > 0 ) {
			page.append( "<ul class='children'></ul>" );	

			page_children = page.children();	
			ul_left = $(page_children[0]);
			ul_right = $(page_children[1]);

			li_left = ul_left.children();

			len = li_left.length;
			
			if( len >= 4 ) {
				half = Math.round( len/2 );

				for( i = len-1; i >=half; i-- ) {
					left_child = $(li_left[i]);
					left_child.remove();

					ul_right.prepend( left_child );
				}
			}
			else {
				ul_right.remove();
				ul_left.addClass('single-list');
			}
		 }
	  });
	  if(ww >= 820) {
	  	setWidths();
	  }
  }
  
	function setWidths() {
		widthSet = true;
		$('#access .sub-pages').each(function() {
			var thisNav = $(this);
			if(thisNav.find('.children').length > 0) {
				var cmwidth = 0;
				var ulHArray = [];
				thisNav.find('.children').each(function(){
					var ulWidth = 0;
					var ulHeight = 0;
					var ulArray = [];
					
					ulHArray.push($(this).height());
					ulArray.push($(this).outerWidth());

					ulWidth = Math.max.apply( Math, ulArray );
					$(this).css({'width': ulWidth});
					cmwidth += $(this).outerWidth();
				});
				ulHeight = Math.max.apply( Math, ulHArray );
				thisNav.find('.children').css('height', ulHeight);
				thisNav.css('width', cmwidth);
			}		
		});
  }

	function viewport() {
		var e = window, a = 'inner';
		if (!('innerWidth' in window )) {
			a = 'client';
			e = document.documentElement || document.body;
		}
		return { width : e[ a+'Width' ] };
	}
})(jQuery);


