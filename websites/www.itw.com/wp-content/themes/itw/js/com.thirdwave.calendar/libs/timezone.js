(function(){
	
	
	Date.getMinutesByUTCCode = function( timezone )
	{
		var code = timezone.toUpperCase();
		var diff = 0;
		
		switch( code )
		{
			case 'AST': case 'AT': diff = -4; break;
			case 'EST': case 'ET': diff = -5; break;
			case 'EDT':	diff = -4; break;
			case 'CST':	case 'CT': diff = -6; break;
			case 'CDT':	diff = -5; break;
			case 'MST':	case 'MT': diff = -7; break;
			case 'MDT':	diff = -6; break;
			case 'PST':	case 'PT': diff = -8; break;
			case 'PDT':	diff = -7; break;
			case 'AKST': diff = -9; break;
			case 'AKDT': diff = -8; break;
			case 'HAST': case 'HAT': diff = -10; break;
			case 'HADT': diff = -9; break;
			case 'SST':	case 'ST': diff = -11; break;
			case 'SDT':	diff = -10; break;
			case 'CHST': case 'CHT': diff = +10; break;
		}
		
		return diff * -60; 
	}
	
	Date.prototype.utcTimeByZone = function( timezone )
	{
		var minutes = Date.getMinutesByUTCCode(timezone);
		
		var date = this.clone();
		date.addMinutes( minutes );		
		return date;
	}
	
	Date.prototype.convertToUTC = function()
	{
		var date = this.clone();
		date.addMinutes( date.getTimezoneOffset() );
		
		return date;
	}
})();