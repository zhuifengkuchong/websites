// JavaScript Document

(function($) {
	$(document).ready( function() {
		ww = $(window).width();
		numImgs = $('#slideshow .slideshow-list li').length;
		changeSlide();
		
		$('.slide-nav a').click(function() {
			navClass = $(this).attr('class');
			currImg = $('#slideshow .slideshow-list li.currImg');
			if(!$('#slideshow .slideshow-list li').is(':animated')) {
				clearTimeout(slideTo);

				if(navClass == 'previous') {
					if(currImg.prev().length < 1) {
						nextImg = $('#slideshow .slideshow-list li:last');
					}
					else {
						nextImg = currImg.prev();
					}
				}
				else {
					if(currImg.next().length < 1) {
						nextImg = $('#slideshow .slideshow-list li:first');
					}
					else {
						nextImg = currImg.next();
					}
				}
				nextImg.addClass('nextImg');
				currImg.animate({
					opacity: 0
					}, 1000, function(){
					$(this).removeClass('currImg').css('opacity', '');
					nextImg.addClass('currImg').removeClass('nextImg');
				});
				changeSlide();
			}
		});	
	});
	
		//sliding gallery
	function changeSlide() {
		//animate the content from top to bottom out of view
		slideTo = setTimeout(slideTrans, 5000);	
		
	}
	
	function slideTrans() {
		currImg = $('#slideshow .slideshow-list li.currImg');
		if(currImg.next().length < 1) {
			nextImg = $('#slideshow .slideshow-list li:first');
		}
		else {
			nextImg = currImg.next();
		}
		nextImg.addClass('nextImg');
		//animate the image
		currImg.animate({
			opacity: 0
			}, 1000, function(){
			$(this).removeClass('currImg').css('opacity', '');
			nextImg.addClass('currImg').removeClass('nextImg');
		});
		changeSlide();
	}
})(jQuery);
