<script id = "race149a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race149={};
	myVars.races.race149.varName="Array[1477]$LEN";
	myVars.races.race149.varType="@varType@";
	myVars.races.race149.repairType = "@RepairType";
	myVars.races.race149.event1={};
	myVars.races.race149.event2={};
	myVars.races.race149.event1.id = "epd_mainnav_san_jacinto_marsh_restoration_1_417";
	myVars.races.race149.event1.type = "onmouseover";
	myVars.races.race149.event1.loc = "epd_mainnav_san_jacinto_marsh_restoration_1_417_LOC";
	myVars.races.race149.event1.isRead = "False";
	myVars.races.race149.event1.eventType = "@event1EventType@";
	myVars.races.race149.event2.id = "epd_mainnav_junior_achievement_1_413";
	myVars.races.race149.event2.type = "onmouseover";
	myVars.races.race149.event2.loc = "epd_mainnav_junior_achievement_1_413_LOC";
	myVars.races.race149.event2.isRead = "False";
	myVars.races.race149.event2.eventType = "@event2EventType@";
	myVars.races.race149.event1.executed= false;// true to disable, false to enable
	myVars.races.race149.event2.executed= false;// true to disable, false to enable
</script>

