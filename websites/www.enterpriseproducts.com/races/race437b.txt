<script id = "race437b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race437={};
	myVars.races.race437.varName="Function[815567].elem";
	myVars.races.race437.varType="@varType@";
	myVars.races.race437.repairType = "@RepairType";
	myVars.races.race437.event1={};
	myVars.races.race437.event2={};
	myVars.races.race437.event1.id = "nav_royalties_arrow_lt";
	myVars.races.race437.event1.type = "onmouseover";
	myVars.races.race437.event1.loc = "nav_royalties_arrow_lt_LOC";
	myVars.races.race437.event1.isRead = "True";
	myVars.races.race437.event1.eventType = "@event1EventType@";
	myVars.races.race437.event2.id = "nav_mediacenter_arrow_rt";
	myVars.races.race437.event2.type = "onmouseover";
	myVars.races.race437.event2.loc = "nav_mediacenter_arrow_rt_LOC";
	myVars.races.race437.event2.isRead = "False";
	myVars.races.race437.event2.eventType = "@event2EventType@";
	myVars.races.race437.event1.executed= false;// true to disable, false to enable
	myVars.races.race437.event2.executed= false;// true to disable, false to enable
</script>

