<script id = "race79a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race79={};
	myVars.races.race79.varName="epd_mainnav_pipeline_safety_1_220__onmouseover";
	myVars.races.race79.varType="@varType@";
	myVars.races.race79.repairType = "@RepairType";
	myVars.races.race79.event1={};
	myVars.races.race79.event2={};
	myVars.races.race79.event1.id = "Lu_DOM";
	myVars.races.race79.event1.type = "onDOMContentLoaded";
	myVars.races.race79.event1.loc = "Lu_DOM_LOC";
	myVars.races.race79.event1.isRead = "False";
	myVars.races.race79.event1.eventType = "@event1EventType@";
	myVars.races.race79.event2.id = "epd_mainnav_how_to_recognize_a_leak_1_263";
	myVars.races.race79.event2.type = "onmouseover";
	myVars.races.race79.event2.loc = "epd_mainnav_how_to_recognize_a_leak_1_263_LOC";
	myVars.races.race79.event2.isRead = "True";
	myVars.races.race79.event2.eventType = "@event2EventType@";
	myVars.races.race79.event1.executed= false;// true to disable, false to enable
	myVars.races.race79.event2.executed= false;// true to disable, false to enable
</script>

