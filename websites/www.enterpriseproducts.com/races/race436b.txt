<script id = "race436b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race436={};
	myVars.races.race436.varName="Function[817347].elem";
	myVars.races.race436.varType="@varType@";
	myVars.races.race436.repairType = "@RepairType";
	myVars.races.race436.event1={};
	myVars.races.race436.event2={};
	myVars.races.race436.event1.id = "nav_royalties_arrow_lt";
	myVars.races.race436.event1.type = "onmouseover";
	myVars.races.race436.event1.loc = "nav_royalties_arrow_lt_LOC";
	myVars.races.race436.event1.isRead = "True";
	myVars.races.race436.event1.eventType = "@event1EventType@";
	myVars.races.race436.event2.id = "nav_mediacenter_arrow_lt";
	myVars.races.race436.event2.type = "onmouseover";
	myVars.races.race436.event2.loc = "nav_mediacenter_arrow_lt_LOC";
	myVars.races.race436.event2.isRead = "False";
	myVars.races.race436.event2.eventType = "@event2EventType@";
	myVars.races.race436.event1.executed= false;// true to disable, false to enable
	myVars.races.race436.event2.executed= false;// true to disable, false to enable
</script>

