<script id = "race43a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race43={};
	myVars.races.race43.varName="epd_mainnav_operations_1_173__onmouseover";
	myVars.races.race43.varType="@varType@";
	myVars.races.race43.repairType = "@RepairType";
	myVars.races.race43.event1={};
	myVars.races.race43.event2={};
	myVars.races.race43.event1.id = "Lu_DOM";
	myVars.races.race43.event1.type = "onDOMContentLoaded";
	myVars.races.race43.event1.loc = "Lu_DOM_LOC";
	myVars.races.race43.event1.isRead = "False";
	myVars.races.race43.event1.eventType = "@event1EventType@";
	myVars.races.race43.event2.id = "epd_mainnav_natural_gas_processing_plants___related_ngl_marketing_1_406";
	myVars.races.race43.event2.type = "onmouseover";
	myVars.races.race43.event2.loc = "epd_mainnav_natural_gas_processing_plants___related_ngl_marketing_1_406_LOC";
	myVars.races.race43.event2.isRead = "True";
	myVars.races.race43.event2.eventType = "@event2EventType@";
	myVars.races.race43.event1.executed= false;// true to disable, false to enable
	myVars.races.race43.event2.executed= false;// true to disable, false to enable
</script>

