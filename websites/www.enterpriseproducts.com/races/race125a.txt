<script id = "race125a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race125={};
	myVars.races.race125.varName="Function[64954].elem";
	myVars.races.race125.varType="@varType@";
	myVars.races.race125.repairType = "@RepairType";
	myVars.races.race125.event1={};
	myVars.races.race125.event2={};
	myVars.races.race125.event1.id = "Lu_window";
	myVars.races.race125.event1.type = "onload";
	myVars.races.race125.event1.loc = "Lu_window_LOC";
	myVars.races.race125.event1.isRead = "False";
	myVars.races.race125.event1.eventType = "@event1EventType@";
	myVars.races.race125.event2.id = "epd_mainnav_corporate_governance_1_251";
	myVars.races.race125.event2.type = "onmouseover";
	myVars.races.race125.event2.loc = "epd_mainnav_corporate_governance_1_251_LOC";
	myVars.races.race125.event2.isRead = "True";
	myVars.races.race125.event2.eventType = "@event2EventType@";
	myVars.races.race125.event1.executed= false;// true to disable, false to enable
	myVars.races.race125.event2.executed= false;// true to disable, false to enable
</script>

