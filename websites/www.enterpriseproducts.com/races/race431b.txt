<script id = "race431b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race431={};
	myVars.races.race431.varName="Function[812113].elem";
	myVars.races.race431.varType="@varType@";
	myVars.races.race431.repairType = "@RepairType";
	myVars.races.race431.event1={};
	myVars.races.race431.event2={};
	myVars.races.race431.event1.id = "nav_royalties_arrow_rt";
	myVars.races.race431.event1.type = "onmouseover";
	myVars.races.race431.event1.loc = "nav_royalties_arrow_rt_LOC";
	myVars.races.race431.event1.isRead = "True";
	myVars.races.race431.event1.eventType = "@event1EventType@";
	myVars.races.race431.event2.id = "major_project_wrapper";
	myVars.races.race431.event2.type = "onmouseover";
	myVars.races.race431.event2.loc = "major_project_wrapper_LOC";
	myVars.races.race431.event2.isRead = "False";
	myVars.races.race431.event2.eventType = "@event2EventType@";
	myVars.races.race431.event1.executed= false;// true to disable, false to enable
	myVars.races.race431.event2.executed= false;// true to disable, false to enable
</script>

