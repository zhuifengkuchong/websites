<script id = "race48a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race48={};
	myVars.races.race48.varName="epd_mainnav_operations_1_173__onmouseover";
	myVars.races.race48.varType="@varType@";
	myVars.races.race48.repairType = "@RepairType";
	myVars.races.race48.event1={};
	myVars.races.race48.event2={};
	myVars.races.race48.event1.id = "Lu_DOM";
	myVars.races.race48.event1.type = "onDOMContentLoaded";
	myVars.races.race48.event1.loc = "Lu_DOM_LOC";
	myVars.races.race48.event1.isRead = "False";
	myVars.races.race48.event1.eventType = "@event1EventType@";
	myVars.races.race48.event2.id = "epd_mainnav_crude_oil_pipelines___services_1_200";
	myVars.races.race48.event2.type = "onmouseover";
	myVars.races.race48.event2.loc = "epd_mainnav_crude_oil_pipelines___services_1_200_LOC";
	myVars.races.race48.event2.isRead = "True";
	myVars.races.race48.event2.eventType = "@event2EventType@";
	myVars.races.race48.event1.executed= false;// true to disable, false to enable
	myVars.races.race48.event2.executed= false;// true to disable, false to enable
</script>

