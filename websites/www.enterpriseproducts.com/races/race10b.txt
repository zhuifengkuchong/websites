<script id = "race10b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race10={};
	myVars.races.race10.varName="epd_mainnav_about_us_1_416__onmouseover";
	myVars.races.race10.varType="@varType@";
	myVars.races.race10.repairType = "@RepairType";
	myVars.races.race10.event1={};
	myVars.races.race10.event2={};
	myVars.races.race10.event1.id = "epd_mainnav_code_of_conduct___related_policies_1_253";
	myVars.races.race10.event1.type = "onmouseover";
	myVars.races.race10.event1.loc = "epd_mainnav_code_of_conduct___related_policies_1_253_LOC";
	myVars.races.race10.event1.isRead = "True";
	myVars.races.race10.event1.eventType = "@event1EventType@";
	myVars.races.race10.event2.id = "Lu_DOM";
	myVars.races.race10.event2.type = "onDOMContentLoaded";
	myVars.races.race10.event2.loc = "Lu_DOM_LOC";
	myVars.races.race10.event2.isRead = "False";
	myVars.races.race10.event2.eventType = "@event2EventType@";
	myVars.races.race10.event1.executed= false;// true to disable, false to enable
	myVars.races.race10.event2.executed= false;// true to disable, false to enable
</script>

