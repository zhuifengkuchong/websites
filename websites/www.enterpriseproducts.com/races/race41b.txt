<script id = "race41b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race41={};
	myVars.races.race41.varName="epd_mainnav_operations_1_173__onmouseover";
	myVars.races.race41.varType="@varType@";
	myVars.races.race41.repairType = "@RepairType";
	myVars.races.race41.event1={};
	myVars.races.race41.event2={};
	myVars.races.race41.event1.id = "epd_mainnav_operations_1_173";
	myVars.races.race41.event1.type = "onmouseover";
	myVars.races.race41.event1.loc = "epd_mainnav_operations_1_173_LOC";
	myVars.races.race41.event1.isRead = "True";
	myVars.races.race41.event1.eventType = "@event1EventType@";
	myVars.races.race41.event2.id = "Lu_DOM";
	myVars.races.race41.event2.type = "onDOMContentLoaded";
	myVars.races.race41.event2.loc = "Lu_DOM_LOC";
	myVars.races.race41.event2.isRead = "False";
	myVars.races.race41.event2.eventType = "@event2EventType@";
	myVars.races.race41.event1.executed= false;// true to disable, false to enable
	myVars.races.race41.event2.executed= false;// true to disable, false to enable
</script>

