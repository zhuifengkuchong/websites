<script id = "race269b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race269={};
	myVars.races.race269.varName="Function[1476].#epd_mainnav_san_jacinto_marsh_restoration_1_417 ";
	myVars.races.race269.varType="@varType@";
	myVars.races.race269.repairType = "@RepairType";
	myVars.races.race269.event1={};
	myVars.races.race269.event2={};
	myVars.races.race269.event1.id = "epd_mainnav_about_boligee_1_380";
	myVars.races.race269.event1.type = "onmouseover";
	myVars.races.race269.event1.loc = "epd_mainnav_about_boligee_1_380_LOC";
	myVars.races.race269.event1.isRead = "False";
	myVars.races.race269.event1.eventType = "@event1EventType@";
	myVars.races.race269.event2.id = "epd_mainnav_san_jacinto_marsh_restoration_1_417";
	myVars.races.race269.event2.type = "onmouseover";
	myVars.races.race269.event2.loc = "epd_mainnav_san_jacinto_marsh_restoration_1_417_LOC";
	myVars.races.race269.event2.isRead = "False";
	myVars.races.race269.event2.eventType = "@event2EventType@";
	myVars.races.race269.event1.executed= false;// true to disable, false to enable
	myVars.races.race269.event2.executed= false;// true to disable, false to enable
</script>

