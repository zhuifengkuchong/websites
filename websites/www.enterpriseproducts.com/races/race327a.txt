<script id = "race327a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race327={};
	myVars.races.race327.varName="Array[989]$LEN";
	myVars.races.race327.varType="@varType@";
	myVars.races.race327.repairType = "@RepairType";
	myVars.races.race327.event1={};
	myVars.races.race327.event2={};
	myVars.races.race327.event1.id = "epd_mainnav_pipeline_integrity_1_267";
	myVars.races.race327.event1.type = "onmouseover";
	myVars.races.race327.event1.loc = "epd_mainnav_pipeline_integrity_1_267_LOC";
	myVars.races.race327.event1.isRead = "False";
	myVars.races.race327.event1.eventType = "@event1EventType@";
	myVars.races.race327.event2.id = "epd_mainnav_keeping_our_pipelines_safe_1_266";
	myVars.races.race327.event2.type = "onmouseover";
	myVars.races.race327.event2.loc = "epd_mainnav_keeping_our_pipelines_safe_1_266_LOC";
	myVars.races.race327.event2.isRead = "False";
	myVars.races.race327.event2.eventType = "@event2EventType@";
	myVars.races.race327.event1.executed= false;// true to disable, false to enable
	myVars.races.race327.event2.executed= false;// true to disable, false to enable
</script>

