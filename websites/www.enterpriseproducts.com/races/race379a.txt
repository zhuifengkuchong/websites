<script id = "race379a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race379={};
	myVars.races.race379.varName="Function[1476].#epd_mainnav_butane_isomerization_1_229 ";
	myVars.races.race379.varType="@varType@";
	myVars.races.race379.repairType = "@RepairType";
	myVars.races.race379.event1={};
	myVars.races.race379.event2={};
	myVars.races.race379.event1.id = "epd_mainnav_butane_isomerization_1_229";
	myVars.races.race379.event1.type = "onmouseover";
	myVars.races.race379.event1.loc = "epd_mainnav_butane_isomerization_1_229_LOC";
	myVars.races.race379.event1.isRead = "False";
	myVars.races.race379.event1.eventType = "@event1EventType@";
	myVars.races.race379.event2.id = "epd_mainnav_transportation_1_358";
	myVars.races.race379.event2.type = "onmouseover";
	myVars.races.race379.event2.loc = "epd_mainnav_transportation_1_358_LOC";
	myVars.races.race379.event2.isRead = "False";
	myVars.races.race379.event2.eventType = "@event2EventType@";
	myVars.races.race379.event1.executed= false;// true to disable, false to enable
	myVars.races.race379.event2.executed= false;// true to disable, false to enable
</script>

