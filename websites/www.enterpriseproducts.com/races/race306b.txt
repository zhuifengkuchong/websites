<script id = "race306b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race306={};
	myVars.races.race306.varName="Array[1477]$LEN";
	myVars.races.race306.varType="@varType@";
	myVars.races.race306.repairType = "@RepairType";
	myVars.races.race306.event1={};
	myVars.races.race306.event2={};
	myVars.races.race306.event1.id = "epd_mainnav_how_to_recognize_a_leak_1_263";
	myVars.races.race306.event1.type = "onmouseover";
	myVars.races.race306.event1.loc = "epd_mainnav_how_to_recognize_a_leak_1_263_LOC";
	myVars.races.race306.event1.isRead = "False";
	myVars.races.race306.event1.eventType = "@event1EventType@";
	myVars.races.race306.event2.id = "epd_mainnav_if_you_live_or_work_near_an_enterprise_products_pipeline_1_262";
	myVars.races.race306.event2.type = "onmouseover";
	myVars.races.race306.event2.loc = "epd_mainnav_if_you_live_or_work_near_an_enterprise_products_pipeline_1_262_LOC";
	myVars.races.race306.event2.isRead = "False";
	myVars.races.race306.event2.eventType = "@event2EventType@";
	myVars.races.race306.event1.executed= false;// true to disable, false to enable
	myVars.races.race306.event2.executed= false;// true to disable, false to enable
</script>

