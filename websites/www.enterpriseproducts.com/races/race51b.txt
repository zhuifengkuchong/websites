<script id = "race51b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race51={};
	myVars.races.race51.varName="epd_mainnav_operations_1_173__onmouseover";
	myVars.races.race51.varType="@varType@";
	myVars.races.race51.repairType = "@RepairType";
	myVars.races.race51.event1={};
	myVars.races.race51.event2={};
	myVars.races.race51.event1.id = "epd_mainnav_petrochemical___refined_products_services_1_224";
	myVars.races.race51.event1.type = "onmouseover";
	myVars.races.race51.event1.loc = "epd_mainnav_petrochemical___refined_products_services_1_224_LOC";
	myVars.races.race51.event1.isRead = "True";
	myVars.races.race51.event1.eventType = "@event1EventType@";
	myVars.races.race51.event2.id = "Lu_DOM";
	myVars.races.race51.event2.type = "onDOMContentLoaded";
	myVars.races.race51.event2.loc = "Lu_DOM_LOC";
	myVars.races.race51.event2.isRead = "False";
	myVars.races.race51.event2.eventType = "@event2EventType@";
	myVars.races.race51.event1.executed= false;// true to disable, false to enable
	myVars.races.race51.event2.executed= false;// true to disable, false to enable
</script>

