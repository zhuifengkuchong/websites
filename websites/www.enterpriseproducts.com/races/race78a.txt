<script id = "race78a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race78={};
	myVars.races.race78.varName="epd_mainnav_pipeline_safety_1_220__onmouseover";
	myVars.races.race78.varType="@varType@";
	myVars.races.race78.repairType = "@RepairType";
	myVars.races.race78.event1={};
	myVars.races.race78.event2={};
	myVars.races.race78.event1.id = "Lu_DOM";
	myVars.races.race78.event1.type = "onDOMContentLoaded";
	myVars.races.race78.event1.loc = "Lu_DOM_LOC";
	myVars.races.race78.event1.isRead = "False";
	myVars.races.race78.event1.eventType = "@event1EventType@";
	myVars.races.race78.event2.id = "epd_mainnav_if_you_live_or_work_near_an_enterprise_products_pipeline_1_262";
	myVars.races.race78.event2.type = "onmouseover";
	myVars.races.race78.event2.loc = "epd_mainnav_if_you_live_or_work_near_an_enterprise_products_pipeline_1_262_LOC";
	myVars.races.race78.event2.isRead = "True";
	myVars.races.race78.event2.eventType = "@event2EventType@";
	myVars.races.race78.event1.executed= false;// true to disable, false to enable
	myVars.races.race78.event2.executed= false;// true to disable, false to enable
</script>

