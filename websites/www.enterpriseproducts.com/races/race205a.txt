<script id = "race205a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race205={};
	myVars.races.race205.varName="Array[1477]$LEN";
	myVars.races.race205.varType="@varType@";
	myVars.races.race205.repairType = "@RepairType";
	myVars.races.race205.event1={};
	myVars.races.race205.event2={};
	myVars.races.race205.event1.id = "epd_mainnav_natural_gas_processing_plants___related_ngl_marketing_1_406";
	myVars.races.race205.event1.type = "onmouseover";
	myVars.races.race205.event1.loc = "epd_mainnav_natural_gas_processing_plants___related_ngl_marketing_1_406_LOC";
	myVars.races.race205.event1.isRead = "False";
	myVars.races.race205.event1.eventType = "@event1EventType@";
	myVars.races.race205.event2.id = "epd_mainnav_ngl_fractionation_1_407";
	myVars.races.race205.event2.type = "onmouseover";
	myVars.races.race205.event2.loc = "epd_mainnav_ngl_fractionation_1_407_LOC";
	myVars.races.race205.event2.isRead = "False";
	myVars.races.race205.event2.eventType = "@event2EventType@";
	myVars.races.race205.event1.executed= false;// true to disable, false to enable
	myVars.races.race205.event2.executed= false;// true to disable, false to enable
</script>

