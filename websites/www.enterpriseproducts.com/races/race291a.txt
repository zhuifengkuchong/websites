<script id = "race291a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race291={};
	myVars.races.race291.varName="Object[3319].mouseover";
	myVars.races.race291.varType="@varType@";
	myVars.races.race291.repairType = "@RepairType";
	myVars.races.race291.event1={};
	myVars.races.race291.event2={};
	myVars.races.race291.event1.id = "epd_mainnav_about_us_1_416";
	myVars.races.race291.event1.type = "onmouseover";
	myVars.races.race291.event1.loc = "epd_mainnav_about_us_1_416_LOC";
	myVars.races.race291.event1.isRead = "False";
	myVars.races.race291.event1.eventType = "@event1EventType@";
	myVars.races.race291.event2.id = "epd_mainnav_pipeline_safety_1_220";
	myVars.races.race291.event2.type = "onmouseover";
	myVars.races.race291.event2.loc = "epd_mainnav_pipeline_safety_1_220_LOC";
	myVars.races.race291.event2.isRead = "True";
	myVars.races.race291.event2.eventType = "@event2EventType@";
	myVars.races.race291.event1.executed= false;// true to disable, false to enable
	myVars.races.race291.event2.executed= false;// true to disable, false to enable
</script>

