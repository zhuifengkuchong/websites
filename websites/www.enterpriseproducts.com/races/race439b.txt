<script id = "race439b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race439={};
	myVars.races.race439.varName="Function[807899].elem";
	myVars.races.race439.varType="@varType@";
	myVars.races.race439.repairType = "@RepairType";
	myVars.races.race439.event1={};
	myVars.races.race439.event2={};
	myVars.races.race439.event1.id = "nav_royalties_arrow_lt";
	myVars.races.race439.event1.type = "onmouseover";
	myVars.races.race439.event1.loc = "nav_royalties_arrow_lt_LOC";
	myVars.races.race439.event1.isRead = "True";
	myVars.races.race439.event1.eventType = "@event1EventType@";
	myVars.races.race439.event2.id = "nav_major_projects_arrow_lt";
	myVars.races.race439.event2.type = "onmouseover";
	myVars.races.race439.event2.loc = "nav_major_projects_arrow_lt_LOC";
	myVars.races.race439.event2.isRead = "False";
	myVars.races.race439.event2.eventType = "@event2EventType@";
	myVars.races.race439.event1.executed= false;// true to disable, false to enable
	myVars.races.race439.event2.executed= false;// true to disable, false to enable
</script>

