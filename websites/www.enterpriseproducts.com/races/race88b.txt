<script id = "race88b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race88={};
	myVars.races.race88.varName="epd_mainnav_pipeline_safety_1_220__onmouseover";
	myVars.races.race88.varType="@varType@";
	myVars.races.race88.repairType = "@RepairType";
	myVars.races.race88.event1={};
	myVars.races.race88.event2={};
	myVars.races.race88.event1.id = "epd_mainnav_responding_to_emergencies_1_376";
	myVars.races.race88.event1.type = "onmouseover";
	myVars.races.race88.event1.loc = "epd_mainnav_responding_to_emergencies_1_376_LOC";
	myVars.races.race88.event1.isRead = "True";
	myVars.races.race88.event1.eventType = "@event1EventType@";
	myVars.races.race88.event2.id = "Lu_DOM";
	myVars.races.race88.event2.type = "onDOMContentLoaded";
	myVars.races.race88.event2.loc = "Lu_DOM_LOC";
	myVars.races.race88.event2.isRead = "False";
	myVars.races.race88.event2.eventType = "@event2EventType@";
	myVars.races.race88.event1.executed= false;// true to disable, false to enable
	myVars.races.race88.event2.executed= false;// true to disable, false to enable
</script>

