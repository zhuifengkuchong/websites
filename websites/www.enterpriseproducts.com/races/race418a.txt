<script id = "race418a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race418={};
	myVars.races.race418.varName="Function[807899].elem";
	myVars.races.race418.varType="@varType@";
	myVars.races.race418.repairType = "@RepairType";
	myVars.races.race418.event1={};
	myVars.races.race418.event2={};
	myVars.races.race418.event1.id = "nav_major_projects_arrow_lt";
	myVars.races.race418.event1.type = "onmouseover";
	myVars.races.race418.event1.loc = "nav_major_projects_arrow_lt_LOC";
	myVars.races.race418.event1.isRead = "False";
	myVars.races.race418.event1.eventType = "@event1EventType@";
	myVars.races.race418.event2.id = "nav_mediacenter_arrow_lt";
	myVars.races.race418.event2.type = "onmouseover";
	myVars.races.race418.event2.loc = "nav_mediacenter_arrow_lt_LOC";
	myVars.races.race418.event2.isRead = "True";
	myVars.races.race418.event2.eventType = "@event2EventType@";
	myVars.races.race418.event1.executed= false;// true to disable, false to enable
	myVars.races.race418.event2.executed= false;// true to disable, false to enable
</script>

