<script id = "race200a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race200={};
	myVars.races.race200.varName="Object[3319].mouseover";
	myVars.races.race200.varType="@varType@";
	myVars.races.race200.repairType = "@RepairType";
	myVars.races.race200.event1={};
	myVars.races.race200.event2={};
	myVars.races.race200.event1.id = "epd_mainnav_about_us_1_416";
	myVars.races.race200.event1.type = "onmouseover";
	myVars.races.race200.event1.loc = "epd_mainnav_about_us_1_416_LOC";
	myVars.races.race200.event1.isRead = "False";
	myVars.races.race200.event1.eventType = "@event1EventType@";
	myVars.races.race200.event2.id = "epd_mainnav_ngl_pipelines___services_1_179";
	myVars.races.race200.event2.type = "onmouseover";
	myVars.races.race200.event2.loc = "epd_mainnav_ngl_pipelines___services_1_179_LOC";
	myVars.races.race200.event2.isRead = "True";
	myVars.races.race200.event2.eventType = "@event2EventType@";
	myVars.races.race200.event1.executed= false;// true to disable, false to enable
	myVars.races.race200.event2.executed= false;// true to disable, false to enable
</script>

