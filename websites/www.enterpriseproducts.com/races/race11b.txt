<script id = "race11b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race11={};
	myVars.races.race11.varName="epd_mainnav_about_us_1_416__onmouseover";
	myVars.races.race11.varType="@varType@";
	myVars.races.race11.repairType = "@RepairType";
	myVars.races.race11.event1={};
	myVars.races.race11.event2={};
	myVars.races.race11.event1.id = "epd_mainnav_california_transparency_in_supply_chain_act_disclosure_1_260";
	myVars.races.race11.event1.type = "onmouseover";
	myVars.races.race11.event1.loc = "epd_mainnav_california_transparency_in_supply_chain_act_disclosure_1_260_LOC";
	myVars.races.race11.event1.isRead = "True";
	myVars.races.race11.event1.eventType = "@event1EventType@";
	myVars.races.race11.event2.id = "Lu_DOM";
	myVars.races.race11.event2.type = "onDOMContentLoaded";
	myVars.races.race11.event2.loc = "Lu_DOM_LOC";
	myVars.races.race11.event2.isRead = "False";
	myVars.races.race11.event2.eventType = "@event2EventType@";
	myVars.races.race11.event1.executed= false;// true to disable, false to enable
	myVars.races.race11.event2.executed= false;// true to disable, false to enable
</script>

