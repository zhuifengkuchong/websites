<script id = "race250b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race250={};
	myVars.races.race250.varName="Function[1476].#epd_mainnav_california_transparency_in_supply_chain_act_disclosure_1_260 ";
	myVars.races.race250.varType="@varType@";
	myVars.races.race250.repairType = "@RepairType";
	myVars.races.race250.event1={};
	myVars.races.race250.event2={};
	myVars.races.race250.event1.id = "epd_mainnav_lpg_terminal_locations_1_308";
	myVars.races.race250.event1.type = "onmouseover";
	myVars.races.race250.event1.loc = "epd_mainnav_lpg_terminal_locations_1_308_LOC";
	myVars.races.race250.event1.isRead = "False";
	myVars.races.race250.event1.eventType = "@event1EventType@";
	myVars.races.race250.event2.id = "epd_mainnav_california_transparency_in_supply_chain_act_disclosure_1_260";
	myVars.races.race250.event2.type = "onmouseover";
	myVars.races.race250.event2.loc = "epd_mainnav_california_transparency_in_supply_chain_act_disclosure_1_260_LOC";
	myVars.races.race250.event2.isRead = "False";
	myVars.races.race250.event2.eventType = "@event2EventType@";
	myVars.races.race250.event1.executed= false;// true to disable, false to enable
	myVars.races.race250.event2.executed= false;// true to disable, false to enable
</script>

