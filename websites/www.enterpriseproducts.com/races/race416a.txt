<script id = "race416a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race416={};
	myVars.races.race416.varName="Array[4158]$LEN";
	myVars.races.race416.varType="@varType@";
	myVars.races.race416.repairType = "@RepairType";
	myVars.races.race416.event1={};
	myVars.races.race416.event2={};
	myVars.races.race416.event1.id = "nav_mediacenter_arrow_rt";
	myVars.races.race416.event1.type = "onmouseover";
	myVars.races.race416.event1.loc = "nav_mediacenter_arrow_rt_LOC";
	myVars.races.race416.event1.isRead = "False";
	myVars.races.race416.event1.eventType = "@event1EventType@";
	myVars.races.race416.event2.id = "nav_mediacenter_arrow_lt";
	myVars.races.race416.event2.type = "onmouseover";
	myVars.races.race416.event2.loc = "nav_mediacenter_arrow_lt_LOC";
	myVars.races.race416.event2.isRead = "True";
	myVars.races.race416.event2.eventType = "@event2EventType@";
	myVars.races.race416.event1.executed= false;// true to disable, false to enable
	myVars.races.race416.event2.executed= false;// true to disable, false to enable
</script>

