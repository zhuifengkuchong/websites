<script id = "race55b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race55={};
	myVars.races.race55.varName="epd_mainnav_operations_1_173__onmouseover";
	myVars.races.race55.varType="@varType@";
	myVars.races.race55.repairType = "@RepairType";
	myVars.races.race55.event1={};
	myVars.races.race55.event2={};
	myVars.races.race55.event1.id = "epd_mainnav_propylene_fractionation___services_1_214";
	myVars.races.race55.event1.type = "onmouseover";
	myVars.races.race55.event1.loc = "epd_mainnav_propylene_fractionation___services_1_214_LOC";
	myVars.races.race55.event1.isRead = "True";
	myVars.races.race55.event1.eventType = "@event1EventType@";
	myVars.races.race55.event2.id = "Lu_DOM";
	myVars.races.race55.event2.type = "onDOMContentLoaded";
	myVars.races.race55.event2.loc = "Lu_DOM_LOC";
	myVars.races.race55.event2.isRead = "False";
	myVars.races.race55.event2.eventType = "@event2EventType@";
	myVars.races.race55.event1.executed= false;// true to disable, false to enable
	myVars.races.race55.event2.executed= false;// true to disable, false to enable
</script>

