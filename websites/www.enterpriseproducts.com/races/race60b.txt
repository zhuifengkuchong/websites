<script id = "race60b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race60={};
	myVars.races.race60.varName="epd_mainnav_customers_1_176__onmouseover";
	myVars.races.race60.varType="@varType@";
	myVars.races.race60.repairType = "@RepairType";
	myVars.races.race60.event1={};
	myVars.races.race60.event2={};
	myVars.races.race60.event1.id = "epd_mainnav_crude_oil_terminal_locations_1_307";
	myVars.races.race60.event1.type = "onmouseover";
	myVars.races.race60.event1.loc = "epd_mainnav_crude_oil_terminal_locations_1_307_LOC";
	myVars.races.race60.event1.isRead = "True";
	myVars.races.race60.event1.eventType = "@event1EventType@";
	myVars.races.race60.event2.id = "Lu_DOM";
	myVars.races.race60.event2.type = "onDOMContentLoaded";
	myVars.races.race60.event2.loc = "Lu_DOM_LOC";
	myVars.races.race60.event2.isRead = "False";
	myVars.races.race60.event2.eventType = "@event2EventType@";
	myVars.races.race60.event1.executed= false;// true to disable, false to enable
	myVars.races.race60.event2.executed= false;// true to disable, false to enable
</script>

