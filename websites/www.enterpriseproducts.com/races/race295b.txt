<script id = "race295b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race295={};
	myVars.races.race295.varName="Function[1016].guid";
	myVars.races.race295.varType="@varType@";
	myVars.races.race295.repairType = "@RepairType";
	myVars.races.race295.event1={};
	myVars.races.race295.event2={};
	myVars.races.race295.event1.id = "epd_mainnav_pipeline_safety_1_220";
	myVars.races.race295.event1.type = "onmouseover";
	myVars.races.race295.event1.loc = "epd_mainnav_pipeline_safety_1_220_LOC";
	myVars.races.race295.event1.isRead = "True";
	myVars.races.race295.event1.eventType = "@event1EventType@";
	myVars.races.race295.event2.id = "epd_mainnav_refined_products_terminaling___marketing_1_298";
	myVars.races.race295.event2.type = "onmouseover";
	myVars.races.race295.event2.loc = "epd_mainnav_refined_products_terminaling___marketing_1_298_LOC";
	myVars.races.race295.event2.isRead = "False";
	myVars.races.race295.event2.eventType = "@event2EventType@";
	myVars.races.race295.event1.executed= false;// true to disable, false to enable
	myVars.races.race295.event2.executed= false;// true to disable, false to enable
</script>

