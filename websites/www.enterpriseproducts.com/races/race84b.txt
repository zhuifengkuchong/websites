<script id = "race84b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race84={};
	myVars.races.race84.varName="epd_mainnav_pipeline_safety_1_220__onmouseover";
	myVars.races.race84.varType="@varType@";
	myVars.races.race84.repairType = "@RepairType";
	myVars.races.race84.event1={};
	myVars.races.race84.event2={};
	myVars.races.race84.event1.id = "epd_mainnav_pipeline_viewer_1_268";
	myVars.races.race84.event1.type = "onmouseover";
	myVars.races.race84.event1.loc = "epd_mainnav_pipeline_viewer_1_268_LOC";
	myVars.races.race84.event1.isRead = "True";
	myVars.races.race84.event1.eventType = "@event1EventType@";
	myVars.races.race84.event2.id = "Lu_DOM";
	myVars.races.race84.event2.type = "onDOMContentLoaded";
	myVars.races.race84.event2.loc = "Lu_DOM_LOC";
	myVars.races.race84.event2.isRead = "False";
	myVars.races.race84.event2.eventType = "@event2EventType@";
	myVars.races.race84.event1.executed= false;// true to disable, false to enable
	myVars.races.race84.event2.executed= false;// true to disable, false to enable
</script>

