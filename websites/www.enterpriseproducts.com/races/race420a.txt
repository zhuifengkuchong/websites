<script id = "race420a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race420={};
	myVars.races.race420.varName="Object[3832].display";
	myVars.races.race420.varType="@varType@";
	myVars.races.race420.repairType = "@RepairType";
	myVars.races.race420.event1={};
	myVars.races.race420.event2={};
	myVars.races.race420.event1.id = "Lu_DOM";
	myVars.races.race420.event1.type = "onDOMContentLoaded";
	myVars.races.race420.event1.loc = "Lu_DOM_LOC";
	myVars.races.race420.event1.isRead = "False";
	myVars.races.race420.event1.eventType = "@event1EventType@";
	myVars.races.race420.event2.id = "nav_mediacenter_arrow_lt";
	myVars.races.race420.event2.type = "onmouseover";
	myVars.races.race420.event2.loc = "nav_mediacenter_arrow_lt_LOC";
	myVars.races.race420.event2.isRead = "True";
	myVars.races.race420.event2.eventType = "@event2EventType@";
	myVars.races.race420.event1.executed= false;// true to disable, false to enable
	myVars.races.race420.event2.executed= false;// true to disable, false to enable
</script>

