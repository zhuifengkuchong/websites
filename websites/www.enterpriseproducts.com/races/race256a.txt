<script id = "race256a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race256={};
	myVars.races.race256.varName="Function[1476].#epd_mainnav_growth_strategy_1_206 ";
	myVars.races.race256.varType="@varType@";
	myVars.races.race256.repairType = "@RepairType";
	myVars.races.race256.event1={};
	myVars.races.race256.event2={};
	myVars.races.race256.event1.id = "epd_mainnav_growth_strategy_1_206";
	myVars.races.race256.event1.type = "onmouseover";
	myVars.races.race256.event1.loc = "epd_mainnav_growth_strategy_1_206_LOC";
	myVars.races.race256.event1.isRead = "False";
	myVars.races.race256.event1.eventType = "@event1EventType@";
	myVars.races.race256.event2.id = "epd_mainnav_marine_transportation_equipment_1_297";
	myVars.races.race256.event2.type = "onmouseover";
	myVars.races.race256.event2.loc = "epd_mainnav_marine_transportation_equipment_1_297_LOC";
	myVars.races.race256.event2.isRead = "False";
	myVars.races.race256.event2.eventType = "@event2EventType@";
	myVars.races.race256.event1.executed= false;// true to disable, false to enable
	myVars.races.race256.event2.executed= false;// true to disable, false to enable
</script>

