<script id = "race98b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race98={};
	myVars.races.race98.varName="epd_mainnav_careers_1_177__onmouseover";
	myVars.races.race98.varType="@varType@";
	myVars.races.race98.repairType = "@RepairType";
	myVars.races.race98.event1={};
	myVars.races.race98.event2={};
	myVars.races.race98.event1.id = "epd_mainnav_engineering_1_354";
	myVars.races.race98.event1.type = "onmouseover";
	myVars.races.race98.event1.loc = "epd_mainnav_engineering_1_354_LOC";
	myVars.races.race98.event1.isRead = "True";
	myVars.races.race98.event1.eventType = "@event1EventType@";
	myVars.races.race98.event2.id = "Lu_DOM";
	myVars.races.race98.event2.type = "onDOMContentLoaded";
	myVars.races.race98.event2.loc = "Lu_DOM_LOC";
	myVars.races.race98.event2.isRead = "False";
	myVars.races.race98.event2.eventType = "@event2EventType@";
	myVars.races.race98.event1.executed= false;// true to disable, false to enable
	myVars.races.race98.event2.executed= false;// true to disable, false to enable
</script>

