<script id = "race33b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race33={};
	myVars.races.race33.varName="Object[15371].uuid";
	myVars.races.race33.varType="@varType@";
	myVars.races.race33.repairType = "@RepairType";
	myVars.races.race33.event1={};
	myVars.races.race33.event2={};
	myVars.races.race33.event1.id = "navSearchField";
	myVars.races.race33.event1.type = "onchange";
	myVars.races.race33.event1.loc = "navSearchField_LOC";
	myVars.races.race33.event1.isRead = "True";
	myVars.races.race33.event1.eventType = "@event1EventType@";
	myVars.races.race33.event2.id = "searchSubmit";
	myVars.races.race33.event2.type = "onchange";
	myVars.races.race33.event2.loc = "searchSubmit_LOC";
	myVars.races.race33.event2.isRead = "False";
	myVars.races.race33.event2.eventType = "@event2EventType@";
	myVars.races.race33.event1.executed= false;// true to disable, false to enable
	myVars.races.race33.event2.executed= false;// true to disable, false to enable
</script>

