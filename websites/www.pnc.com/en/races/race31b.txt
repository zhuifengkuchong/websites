<script id = "race31b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race31={};
	myVars.races.race31.varName="Object[15371].uuid";
	myVars.races.race31.varType="@varType@";
	myVars.races.race31.repairType = "@RepairType";
	myVars.races.race31.event1={};
	myVars.races.race31.event2={};
	myVars.races.race31.event1.id = "userId";
	myVars.races.race31.event1.type = "onchange";
	myVars.races.race31.event1.loc = "userId_LOC";
	myVars.races.race31.event1.isRead = "True";
	myVars.races.race31.event1.eventType = "@event1EventType@";
	myVars.races.race31.event2.id = "olb-btn";
	myVars.races.race31.event2.type = "onchange";
	myVars.races.race31.event2.loc = "olb-btn_LOC";
	myVars.races.race31.event2.isRead = "False";
	myVars.races.race31.event2.eventType = "@event2EventType@";
	myVars.races.race31.event1.executed= false;// true to disable, false to enable
	myVars.races.race31.event2.executed= false;// true to disable, false to enable
</script>

