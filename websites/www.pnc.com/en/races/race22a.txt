<script id = "race22a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race22={};
	myVars.races.race22.varName="userId__onblur";
	myVars.races.race22.varType="@varType@";
	myVars.races.race22.repairType = "@RepairType";
	myVars.races.race22.event1={};
	myVars.races.race22.event2={};
	myVars.races.race22.event1.id = "Lu_DOM";
	myVars.races.race22.event1.type = "onDOMContentLoaded";
	myVars.races.race22.event1.loc = "Lu_DOM_LOC";
	myVars.races.race22.event1.isRead = "False";
	myVars.races.race22.event1.eventType = "@event1EventType@";
	myVars.races.race22.event2.id = "userId";
	myVars.races.race22.event2.type = "onblur";
	myVars.races.race22.event2.loc = "userId_LOC";
	myVars.races.race22.event2.isRead = "True";
	myVars.races.race22.event2.eventType = "@event2EventType@";
	myVars.races.race22.event1.executed= false;// true to disable, false to enable
	myVars.races.race22.event2.executed= false;// true to disable, false to enable
</script>

