<script id = "race42b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race42={};
	myVars.races.race42.varName="Object[15371].uuid";
	myVars.races.race42.varType="@varType@";
	myVars.races.race42.repairType = "@RepairType";
	myVars.races.race42.event1={};
	myVars.races.race42.event2={};
	myVars.races.race42.event1.id = "searchSubmit";
	myVars.races.race42.event1.type = "onblur";
	myVars.races.race42.event1.loc = "searchSubmit_LOC";
	myVars.races.race42.event1.isRead = "True";
	myVars.races.race42.event1.eventType = "@event1EventType@";
	myVars.races.race42.event2.id = "userId";
	myVars.races.race42.event2.type = "onblur";
	myVars.races.race42.event2.loc = "userId_LOC";
	myVars.races.race42.event2.isRead = "False";
	myVars.races.race42.event2.eventType = "@event2EventType@";
	myVars.races.race42.event1.executed= false;// true to disable, false to enable
	myVars.races.race42.event2.executed= false;// true to disable, false to enable
</script>

