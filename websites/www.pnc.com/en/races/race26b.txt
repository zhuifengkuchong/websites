<script id = "race26b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race26={};
	myVars.races.race26.varName="Object[15371].uuid";
	myVars.races.race26.varType="@varType@";
	myVars.races.race26.repairType = "@RepairType";
	myVars.races.race26.event1={};
	myVars.races.race26.event2={};
	myVars.races.race26.event1.id = "userId";
	myVars.races.race26.event1.type = "onkeypress";
	myVars.races.race26.event1.loc = "userId_LOC";
	myVars.races.race26.event1.isRead = "True";
	myVars.races.race26.event1.eventType = "@event1EventType@";
	myVars.races.race26.event2.id = "olb-btn";
	myVars.races.race26.event2.type = "onkeypress";
	myVars.races.race26.event2.loc = "olb-btn_LOC";
	myVars.races.race26.event2.isRead = "False";
	myVars.races.race26.event2.eventType = "@event2EventType@";
	myVars.races.race26.event1.executed= false;// true to disable, false to enable
	myVars.races.race26.event2.executed= false;// true to disable, false to enable
</script>

