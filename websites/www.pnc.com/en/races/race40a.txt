<script id = "race40a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race40={};
	myVars.races.race40.varName="Object[15371].uuid";
	myVars.races.race40.varType="@varType@";
	myVars.races.race40.repairType = "@RepairType";
	myVars.races.race40.event1={};
	myVars.races.race40.event2={};
	myVars.races.race40.event1.id = "Lu_Id_select_1";
	myVars.races.race40.event1.type = "onblur";
	myVars.races.race40.event1.loc = "Lu_Id_select_1_LOC";
	myVars.races.race40.event1.isRead = "False";
	myVars.races.race40.event1.eventType = "@event1EventType@";
	myVars.races.race40.event2.id = "olb-btn";
	myVars.races.race40.event2.type = "onblur";
	myVars.races.race40.event2.loc = "olb-btn_LOC";
	myVars.races.race40.event2.isRead = "True";
	myVars.races.race40.event2.eventType = "@event2EventType@";
	myVars.races.race40.event1.executed= false;// true to disable, false to enable
	myVars.races.race40.event2.executed= false;// true to disable, false to enable
</script>

