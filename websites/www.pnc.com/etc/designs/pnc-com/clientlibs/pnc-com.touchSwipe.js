jq191.fn.swipe=jq191.fn.swipe;
(function(a){var b=function(){var g=this,i=0,c=500,e=75,h;
if(pncMain.resolution==="large"||pncMain.resolution==="medium"){h=590
}if(pncMain.resolution==="small"){h=320
}a(window).on("resolutionChange",function(){if(pncMain.resolution==="large"||pncMain.resolution==="medium"){h=590
}if(pncMain.resolution==="small"){h=320
}});
var d=function(){var l=g.divs.parent().find(".slide-markers");
if(l.size()>0){l.show()
}else{l=a('<div class="related-markers slide-markers"></div>').appendTo(g.divs.parent());
for(var m=1;
m<=g.maxDivs;
m++){var n=(m===1)?' class="active-slide"':"";
l.append("<div"+n+">Circle - "+m+"</div>")
}}};
var f=function(){var l=g.divs.parent().find(".related-markers div");
a(l).removeClass("active-slide");
a(l).each(function(m){if(m===i){a(this).addClass("active-slide")
}})
};
var k=function(){i=Math.max(i-1,0);
g.scrollDivs(h*i,c);
f()
};
var j=function(){i=Math.min(i+1,g.maxDivs-1);
g.scrollDivs(h*i,c);
f()
};
g.scrollDivs=function(n,m){g.divs.css("-webkit-transition-duration",(m/1000).toFixed(1)+"s");
var l=(n<0?"":"-")+Math.abs(n).toString();
g.divs.css("-webkit-transform","translate3d("+l+"px,0px,0px)")
};
g.swipeOptions={triggerOnTouchEnd:true,swipeStatus:function(m,l,o,p){if(l==="move"&&(o==="left"||o==="right")){var n=0;
if(o==="left"){g.scrollDivs((h*i)+p,n)
}else{if(o==="right"){g.scrollDivs((h*i)-p,n)
}}}else{if(l==="cancel"){g.scrollDivs(h*i,c)
}else{if(l==="end"){if(o==="right"){k()
}else{if(o==="left"){j()
}}}}}}};
g.init=function(l){g.divs=a("#"+l);
g.maxDivs=a(g.divs).children().length;
d();
g.divs.swipe(g.swipeOptions).addClass("swipe-active")
}
};
a(function(){a(".swipeable").each(function(d){var e=this.id;
var c=new b();
if(pncMain.resolution==="small"){c.init(e)
}a(window).on("resolutionChange",function(){if(pncMain.resolution==="small"){if(a("#"+e).hasClass("swipe-active")===false){c.init(e)
}}else{a("#"+e+".swipe-active").swipe("destroy").removeAttr("style").removeClass("swipe-active");
a("#"+e).parent().find(".slide-markers").hide()
}})
});
if(a(".swipeable-tablet-mobile").length>0){a(".swipeable-tablet-mobile").each(function(d){var e=this.id;
var c=new b();
if(pncMain.resolution!=="xlarge"){c.init(e)
}a(window).on("resolutionChange",function(){if(pncMain.resolution!=="xlarge"){if(a("#"+e).hasClass("swipe-active")===false){c.init(e)
}}else{a("#"+e+".swipe-active").swipe("destroy").removeAttr("style").removeClass("swipe-active")
}})
})
}})
}(jq191));