var pncShareModule=new (function(a){var c=this,i=null,h=window.location.pathname.replace(".html","");
c.init=function(){d();
g();
a(window).on("resolutionChange",function(){g()
})
};
var d=function(){var m=a("#container");
var k=a(".compare-header",m);
var j=a(".simple-header"),m;
var l=a(".hero-header",m);
if(k.length){h=h+".compare-header-fragments.html"
}else{if(j.length){h+=".simple-header-fragments.html"
}else{if(l.length){h+=".hero-fragments.html"
}}}};
var g=function(){if((pncMain.resolution==="xlarge")||(pncMain.resolution==="large")||(pncMain.resolution==="medium")){if(c.shareAjaxData==null){e()
}}};
var e=function(){c.shareAjaxData=true;
var j="?sections=share";
a.ajax({url:h+j,success:function(k){f(a.trim(k))
},error:function(){}})
};
var f=function(l){var k=a(l),j={};
a("<div/>").appendTo("body").addClass("tmpData").css("display","none").html(k);
j.share=a(".tmpData").find(".social-links");
j.share.appendTo(".hero-social-controls");
a("div.tmpData").remove();
b()
};
var b=function(){var l;
var j=a(".social-links");
j.find(".share > a").on("mouseenter",function(){m(a(this).next())
}).on("mouseleave",function(){k(a(this).next())
});
j.find(".flyout").on("mouseenter",function(){m(a(this))
}).on("mouseleave",function(){k(a(this))
});
function m(n){clearTimeout(l);
n.show()
}function k(n){l=setTimeout(function(){n.hide()
},500)
}j.find(".share li a").each(function(){if(a(".simple-header").length){shareLink=a(this).attr("href")
}else{shareLink=a(this).attr("href")+window.location.href
}a(this).attr("href",shareLink)
});
j.find(".share li a").on("click",function(n){window.open(this.href,"pncshare","width="+a(this).data("width")+",height="+a(this).data("height")+",resizable=yes,scrollbars=yes");
n.preventDefault()
});
j.find("input").val(location.href).on("focus",function(){this.select()
}).on("mouseup",function(n){n.preventDefault();
return false
})
}
})(jq191);
(function(a){pncShareModule.init()
})(jq191);