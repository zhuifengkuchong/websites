var pncSimpleHeader=new (function(e){var d=this,b=null,a=window.location.pathname.replace(".html","")+".simple-header-fragments.html";
d.init=function(){pncMain.getResolution();
g();
e(window).on("resolutionChange",function(i,h){g(pncMain.resolution)
})
};
var g=function(){if((pncMain.resolution==="xlarge")||(pncMain.resolution==="large")||(pncMain.resolution==="medium")){if(d.headerAjaxData==null){f()
}}};
var f=function(){d.headerAjaxData=true;
var h="?sections=live-help";
e.ajax({url:a+h,success:function(i){c(e.trim(i))
},error:function(){}})
};
var c=function(j){var i=e(j),h={};
e("<div/>").appendTo("body").addClass("tmpData").css("display","none").html(i);
h.btnHelp=e(".tmpData").find(".live-help");
h.btnPlayVid=e(".tmpData").find(".play-video");
h.btnHelp.appendTo(e(".hero-social-controls"));
h.btnPlayVid.appendTo(e(".hero-social-controls"));
e("div.tmpData").remove();
pncLiveHelp.init();
e(".header-header-module").addClass("has-data").trigger("ready")
}
})(jq191);
jq191(function(){pncSimpleHeader.init()
});
var inviteChat=false;
var pncLiveHelp=new (function(b){var a=this;
a.init=function(){var d=b(".live-help"),c=b(".live-help-panel");
b(".btn-live-help",d).click(function(f){if(d.hasClass("open")){d.removeClass("open");
c.removeClass("open");
jQuery(".btn-live-help").html("<span>‹ </span>"+liveHelpText);
inviteChat=false
}else{d.addClass("open");
c.addClass("open")
}f.preventDefault()
});
b(".liveperson",c).click(function(f){f.preventDefault();
sendDataOnClick()
})
}
})(jq191);
var invitationChat=new (function(b){var a=this;
a.init=function(){var c=b(".live-help");
if(!c.hasClass("open")){b(".btn-live-help",c).trigger("click");
b(".btn-live-help").html("<span>‹ </span>"+inviteHelpText);
inviteChat=true
}else{b(".btn-live-help",c).trigger("click")
}}
})(jq191);
var pncShareModule=new (function(a){var c=this,i=null,h=window.location.pathname.replace(".html","");
c.init=function(){d();
g();
a(window).on("resolutionChange",function(){g()
})
};
var d=function(){var m=a("#container");
var k=a(".compare-header",m);
var j=a(".simple-header"),m;
var l=a(".hero-header",m);
if(k.length){h=h+".compare-header-fragments.html"
}else{if(j.length){h+=".simple-header-fragments.html"
}else{if(l.length){h+=".hero-fragments.html"
}}}};
var g=function(){if((pncMain.resolution==="xlarge")||(pncMain.resolution==="large")||(pncMain.resolution==="medium")){if(c.shareAjaxData==null){e()
}}};
var e=function(){c.shareAjaxData=true;
var j="?sections=share";
a.ajax({url:h+j,success:function(k){f(a.trim(k))
},error:function(){}})
};
var f=function(l){var k=a(l),j={};
a("<div/>").appendTo("body").addClass("tmpData").css("display","none").html(k);
j.share=a(".tmpData").find(".social-links");
j.share.appendTo(".hero-social-controls");
a("div.tmpData").remove();
b()
};
var b=function(){var l;
var j=a(".social-links");
j.find(".share > a").on("mouseenter",function(){m(a(this).next())
}).on("mouseleave",function(){k(a(this).next())
});
j.find(".flyout").on("mouseenter",function(){m(a(this))
}).on("mouseleave",function(){k(a(this))
});
function m(n){clearTimeout(l);
n.show()
}function k(n){l=setTimeout(function(){n.hide()
},500)
}j.find(".share li a").each(function(){if(a(".simple-header").length){shareLink=a(this).attr("href")
}else{shareLink=a(this).attr("href")+window.location.href
}a(this).attr("href",shareLink)
});
j.find(".share li a").on("click",function(n){window.open(this.href,"pncshare","width="+a(this).data("width")+",height="+a(this).data("height")+",resizable=yes,scrollbars=yes");
n.preventDefault()
});
j.find("input").val(location.href).on("focus",function(){this.select()
}).on("mouseup",function(n){n.preventDefault();
return false
})
}
})(jq191);
(function(a){pncShareModule.init()
})(jq191);