(function(a){a.fn.accordionify=function(){var b=this.length;
this.each(function(d){a(this).find(".subheading").append('<span class="toggle"/>').click(function(f){a(this).next().slideToggle("fast").parent().toggleClass("collapsed");
f.preventDefault()
});
var c=a(".accordion-content",a(this)).data("expanded");
if(!c){a(this).addClass("collapsed")
}})
};
a.fn.unaccordionify=function(){this.each(function(){a(this).removeClass("collapsed").find(".subheading").unbind("click").find(".toggle").remove();
a(this).find(".content").removeAttr("style")
})
};
a(document).ready(function(){a(".accordion-element").accordionify();
a(".accordion-reusable").accordionify();
a(".accordion-interactive").accordionify();
var c=window.location.hash;
var b;
if(a(c).parent().hasClass("collapsed")){a("html, body").animate({scrollTop:a(c).offset().top},1000);
if(a(c).parent().hasClass("collapsed")){a(c).parent().removeClass("collapsed")
}a(c).siblings("div").attr("data-expanded","true")
}a("a[href*=#]:not([href=#])").each(function(){a(this).click(function(){b=a(this).attr("href");
a("html, body").animate({scrollTop:a(b).offset().top},1000);
if(a(b).parent().hasClass("collapsed")){a(b).parent().removeClass("collapsed")
}a(b).siblings("div").attr("data-expanded","true");
a(b).siblings("div").css("display","block")
})
})
})
}(jq191));