(function(b){var a=new function(){var k=this;
var j,i,c,o,h;
var m=projectPath+"/admin/header.navigation-fragments.html";
var l=null;
var r=235;
k.init=function(){c=b(".header .search-container");
d();
if(b(window).width()>=980){g("mouseenter");
f("xlarge")
}else{g("click");
f("small")
}if(jQuery.browser.mobile||b(window).width()<980){n("click")
}else{n("mouseenter");
b(document).mousemove(function(y){var x=b(".item-l2");
if(!x.is(y.target)&&x.has(y.target).length===0){x.removeClass("selected");
x.next().removeClass("expanded")
}})
}if(jQuery.browser.mobile||b(window).width()<980){}else{var v=page_data;
if(v&&v!=="null"&&v!=="undefined"){var v=v.page_name.split("|");
var s="#484848";
var t;
var w;
var u;
if(v.length>1){if(v[0]=="personal-banking"){t=".main-nav ul.level-2.theme_orange a span";
s="#ed8726";
b(t).each(function(x,y){u=b(this).text().toLowerCase();
u=u.trim();
u=u.replace(/\s/g,"-");
u=u.replace(/&/g,"and");
if(u==v[1]){b(this).css("color",s)
}})
}else{if(v[0]=="small-business"){t=".main-nav ul.level-2.theme_green a span";
s="#6db33f";
b(t).each(function(x,y){u=b(this).text().toLowerCase();
u=u.trim();
u=u.replace(/\s/g,"-");
u=u.replace(/&/g,"and");
if(u==v[1]){b(this).css("color",s)
}})
}else{if(v[0]=="corporate-and-institutional"){t=".main-nav ul.level-2.theme_sky a span";
s="#0096d6";
b(t).each(function(x,y){u=b(this).text().toLowerCase();
u=u.trim();
u=u.replace(/\s/g,"-");
u=u.replace(/&/g,"and");
u=u.replace(/asset/g,"investment");
if(u==v[1]){b(this).css("color",s)
}})
}else{if(v[0]=="about-pnc"){t=".main-nav ul.level-2.theme_blue a span";
s="#0073b2";
b(t).each(function(x,y){u=b(this).text().toLowerCase();
u=u.trim();
u=u.replace(/\s/g,"-");
u=u.replace(/&/g,"and");
if(u==v[1]){b(this).css("color",s)
}})
}else{if(v[0]=="admin"){if(v[1]=="full-navigation"){jQuery(".aside-links li:nth-child(2) a").css("color","#ed8726")
}}}}}}}else{if(v.length==1){if(v[0]=="personal-banking"){t=".main-nav ul.level-2.theme_orange a span";
jQuery(t).css("color",s)
}else{if(v[0]=="small-business"){t=".main-nav ul.level-2.theme_green a span";
jQuery(t).css("color",s)
}else{if(v[0]=="corporate-and-institutional"){t=".main-nav ul.level-2.theme_sky a span";
jQuery(t).css("color",s)
}else{if(v[0]=="about-pnc"){t=".main-nav ul.level-2.theme_blue a span";
jQuery(t).css("color",s)
}else{if(v[0]=="customer-service"){jQuery(".aside-links li:nth-child(1) a").css("color","#ed8726")
}else{if(v[0]=="security-assurance"){jQuery(".aside-links li:nth-child(3) a").css("color","#ed8726")
}}}}}}}}}}b(document).mouseup(function(y){var x=b(".item-l2");
var z=b(".login-container");
if(!x.is(y.target)&&x.has(y.target).length===0){x.removeClass("selected");
x.next().removeClass("expanded")
}if(!z.is(y.target)&&z.has(y.target).length===0){z.removeClass("expanded")
}else{x.removeClass("selected");
x.next().removeClass("expanded")
}});
b(window).on("resolutionChange",function(y,x){q(x.resolution);
f(x.resolution)
});
b("#userId").focus()
};
var g=function(t){b(".level-1").each(function(){b(this).unbind()
});
var s=b(".level-2.expanded .item-l2",i).length;
i.attr("data-current-l2",s);
b(".level-1").each(function(){var v=b(this);
var u=b(this).next();
v.on(t,function(x){x.preventDefault();
if(v.hasClass("selected")){if(b(window).width()<980){v.removeClass("selected");
v.next().removeClass("expanded")
}}else{var w=b(".item-l2",b(u)).length;
c.attr("style","");
b(".item-l2:last-child",i).attr("style","");
b(".level-1").removeClass("selected");
if(b(window).width()<980){b(".level-2").removeClass("expanded");
v.next().addClass("expanded");
v.addClass("selected")
}}})
})
};
var n=function(s){b(".subitem",j).each(function(){b(this).unbind()
});
b(".subitem",j).each(function(){b(this).on(s,function(t){t.preventDefault();
var u=b(this).closest("li");
if(u.hasClass("selected")){u.removeClass("selected");
if(b(window).width()<980){}}else{b("ul.level-2 li",j).removeClass("selected");
b(this).closest("li").addClass("selected")
}})
})
};
var d=function(s){i=b(".header");
j=b(".main-nav");
c=b(".header .search-container");
o=b(".header .links-fragment");
h=b(".header .promos-fragment");
b(".nav-toggle").on("click",function(t){t.preventDefault();
j.toggleClass("expanded");
c.toggleClass("expanded")
});
b(".login-toggle").on("click",function(t){t.preventDefault();
b(this).parent().toggleClass("expanded");
j.removeClass("expanded");
c.removeClass("expanded")
});
if(b(window).width()>=980){e()
}};
var f=function(s){c=b(".header .search-container");
var t=c.width();
$text=b('input[type="text"]',c);
if(s=="xlarge"){$text.focus(function(){t=c.width();
c.animate({width:r},200);
if(parseInt(i.attr("data-current-l2"))==5){b(".item-l2:last-child",i).hide(200)
}});
$text.blur(function(){if(parseInt(i.attr("data-current-l2"))==5){c.animate({width:t},200);
b(".item-l2:last-child",i).show(200)
}})
}else{$text.unbind();
c.attr("style","")
}};
var e=function(){if(k.mainNavContent==null){k.mainNavContent=true;
b.ajax({url:m,success:function(s){k.mainNavContent=b.trim(s);
p(k.mainNavContent)
}})
}};
var p=function(u){var s=b(".header-wrapper .item-l2");
var t=b(".item-l2",b(u));
t.each(function(F){var y=t[F];
var x=s[F];
var w=b(".copy-text",y);
var E=b(".promos-fragment",x);
var G=b(".nav-promo-fragment",y);
var H=b(".nav-l3-fragment li:first-child",y);
H.addClass("title");
var A=b(".nav-l3-fragment li:last-child",y);
A.addClass("title");
var B=b(".first-column",x);
var D=b(".second-column",x);
var C=b(".see-all-link",H);
var z=b(".see-all-link",A);
var v=b('<li class="see-all"></li>').append(C);
var I=b('<li class="see-all"></li>').append(z);
B.prepend(H).append(v);
D.prepend(A).append(I);
b(".subitem",x).append(w);
E.append(G.html())
})
};
var q=function(s){if(s=="large"||s=="xlarge"){if(k.mainNavContent==null){e()
}else{j.removeClass("expanded");
c.removeClass("expanded")
}}if(b(window).width()>=980){g("mouseenter");
f("xlarge")
}else{g("click");
f("small")
}if(jQuery.browser.mobile||b(window).width()<980){n("click")
}else{n("mouseenter");
b(document).mousemove(function(u){var t=b(".item-l2");
if(!t.is(u.target)&&t.has(u.target).length===0){t.removeClass("selected");
t.next().removeClass("expanded")
}})
}}
};
b(function(){(function(e){(jQuery.browser=jQuery.browser||{}).mobile=/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|android|ipad|playbook|silk|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(e)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(e.substr(0,4))
})(navigator.userAgent||navigator.vendor||window.opera);
a.init();
var c="";
jQuery("#navSearchForm").submit(function(g){g.preventDefault();
var f=jQuery("#navSearchField").val();
if(f!=""){window.location="https://www.pnc.com/en/search-results.html?q="+encodeURIComponent(f)
}});
var d=function(h,e){var i=jQuery("#searchField").val();
var g={q:h.term,client:"pnc-com-custom",format:"rich"},f="pnc-com-all";
if(f.length!==0){g.site=f
}jQuery.getJSON(GSA_HOST+"/suggest?callback=?",g,function(k){var j=[];
b.each(k.results,function(){j.push(this.name)
});
e(j)
})
};
if(jQuery("#navSearchField").length){jQuery("#navSearchField:not(.ui-autocomplete-input)").autocomplete({source:d,select:function(e,f){}});
jQuery("#navSearchField").on("autocompleteselect",function(e,f){window.location="https://www.pnc.com/en/search-results.html?q="+encodeURIComponent(f.item.value)
})
}if(b(".external-pop")){b(".external-pop").each(function(g){var f=b(this).attr("href");
b(this).attr("href","#");
b(this).attr("rel",f)
})
}b(".external-pop").click(function(f){f.preventDefault();
c=b(f.currentTarget).attr("rel");
if(b("body #leaving-overlay").length==0){b("body").append("<div id='leaving-overlay'><img src='../../../../content/dam/pnc-com/images/universal/pnc_main_logo.png'/*tpa=https://www.pnc.com/content/dam/pnc-com/images/universal/pnc_main_logo.png*/ alt='PNC Bank' /> <h2>You are now leaving pnc.com</h2> <p><strong>Thanks for visiting.</strong></p><p>PNC provides links to third-party web sites as a convenience to our visitors.  We have no control over linked sites and make no representations about any content, products or services available at these locations.  Such sites may have different privacy, security and accessibility standards.  When you access another web site, we recommend that you review their terms and conditions, and privacy and security policies.</p><div style='width:200px;margin:0 auto;'><div class='btn-blue' id='continueBtn' style='float:left;cursor:pointer'><span><a>Continue</a></span></div><div class='btn-gray' id='canceleBtn' style='float:right;cursor:pointer'><span><a>Cancel</a></span></div></div></div>");
b("#continueBtn").on("click",function(g){window.open(c,"_blank");
jQuery.fancybox.close()
});
b("#canceleBtn").on("click",function(g){jQuery.fancybox.close()
})
}jQuery.fancybox.open({padding:30,openSpeed:150,closeSpeed:150,autoSize:false,height:300,helpers:{overlay:{css:{background:"transparent",filter:"progid:DXImageTransform.Microsoft.gradient(startColorstr=#D2eeeeee,endColorstr=#D2eeeeee)",zoom:"1",background:"rgba(238, 238, 238, 0.85)"}}},href:"#leaving-overlay"})
});
b(".signon-link").each(function(){var e=b("body").width();
if(e>=600){b(this).click(function(){setTimeout(function(){b(".login-container").addClass("expanded");
b("#userId").focus()
},200)
})
}else{b(this).attr("href","https://m.pnc.com")
}})
})
})(jq191);