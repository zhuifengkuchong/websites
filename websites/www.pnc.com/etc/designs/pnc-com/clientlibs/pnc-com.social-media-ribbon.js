var pncSocialMediaRibbon=new (function(c){var b=this;
b.init=function(){a(this);
c(window).on("resolutionChange.initSocialFeeds",c.proxy(b.initSocialFeeds,b))
};
var a=function(e){String.prototype.parseURL=function(){return this.replace(/[A-Za-z]+:\/\/[A-Za-z0-9-_]+\.[A-Za-z0-9-_:%&~\?\/.=]+/g,function(f){return'<a class="social-link" href="'+f+'" target="_blank">'+f+"</a>"
})
};
String.prototype.parseUsername=function(){return this.replace(/[@]+[A-Za-z0-9-_]+/g,function(f){var g=f.replace("@","");
return'<a class="tw-username" href="http://twitter.com/'+g+'" target="_blank">'+f+"</a>"
})
};
String.prototype.parseHashtag=function(){return this.replace(/[#]+[A-Za-z0-9-_]+/g,function(g){var f=g.replace("#","%23");
return'<a class="tw-tag" href="http://twitter.com/search?q='+f+'" target="_blank">'+g+"</a>"
})
};
if(pncMain.resolution!="xsmall"){d()
}else{c(window).on("resolutionChange.loadFeeds",d)
}function d(){c(window).off("resolutionChange.loadFeeds");
c.ajax({url:"/pnc-com/twitterfeed?service=TwitterPNCNews",dataType:"json",success:function(k){if(k.length){var f=k[0]["text"].parseURL().parseUsername().parseHashtag(),h=k[0]["link"],g=k[0]["user"]["profileImageUrl"],l=k[0]["user"]["name"],i=k[0]["user"]["screenName"];
var j='<a href="'+h+'" target="_blank">										<img src="'+g+'"></a>&#10;					                    <h4><a href="'+h+'" target="_blank">					                    &nbsp;@'+i+"</a></h4>&#10;					                	<p>"+f+"</p>&#10;";
c("#social-media-ribbon-carousel .smr-twitter .smr-feed").append(j);
c("#social-media-ribbon-carousel .smr-twitter .smr-feed p").dotdotdot({watch:true,height:48,tolerance:2})
}else{console.log("response came back empty",k)
}},error:function(f,g){console.log("error loading @PNCNews Twitter feed",f,g)
}});
c.ajax({url:"/pnc-com/youtubefeed?service=YouTubePNCBank",dataType:"json",success:function(f){if(f.data&&f.data.length){c("#social-media-ribbon-carousel .smr-youtube .smr-feed").append(function(){return'<a href="'+f.data[0]["url"]+'" target="_blank"><img src="'+f.data[0]["thumbnail"]+'"></a>									<h4><a href="'+f.data[0]["url"]+'" target="_blank">'+f.data[0]["title"]+"</a></h4>									<p>"+f.data[0]["duration"]+"</p>									<p>"+f.data[0]["viewCount"]+" views</p>"
}).find("h4").dotdotdot({watch:true,height:24,tolerance:2})
}else{console.log("response came back empty",f)
}},error:function(f,g){console.log("error loading YouTube feed",f,g)
}});
c.ajax({url:"/pnc-com/facebookfeed",dataType:"json",success:function(l){if(l.data&&l.data["postList"]&&l.data["postList"].length){l.data["postList"][0]["message"]!=undefined;
var u=l.data["postList"][0]["message"],q=(l.data["postList"][0]["link"])?l.data["postList"][0]["link"]:"https://www.facebook.com/pncbank",t=l.data["postList"][0]["createdTime"],j=l.data["postList"][0]["likes"],g=l.data["postList"][0]["shareCount"];
if(l.data&&l.data["userInfo"].length){var m=l.data["userInfo"][0]["profilePictureUrl"];
var i='<a href="'+q+'" target="_blank"><img src="'+m+'"/></a>'
}var f=j>0?j+" Likes":"";
var k=g>0?g+" Shares":"";
var o=new Date(parseInt(t)*1000).toISOString(),s=new Date(o),s=c("html").hasClass("lt-ie9")?o:s.toString();
var r='<a href="'+q+'" target="_blank"><time class="timeago" datetime="'+o+'">'+s+"</time></a>";
var n="<p>"+u.parseURL()+"</p>";
var h="<p>"+f+" "+k+"</p>";
var p=c("#social-media-ribbon-carousel .smr-facebook .smr-feed");
p.append(i).append(n).find("p").dotdotdot({watch:true,height:36,tolerance:2});
if(f!=""||k!=""){p.append(h)
}p.append(r).find(".timeago").timeago()
}else{console.log("response came back empty",l)
}},error:function(f,g){console.log("error loading Facebook feed",f,g)
}})
}};
if(!Date.prototype.toISOString){(function(){function d(f){var e=String(f);
if(e.length===1){e="0"+e
}return e
}Date.prototype.toISOString=function(){return this.getUTCFullYear()+"-"+d(this.getUTCMonth()+1)+"-"+d(this.getUTCDate())+"T"+d(this.getUTCHours())+":"+d(this.getUTCMinutes())+":"+d(this.getUTCSeconds())+"."+String((this.getUTCMilliseconds()/1000).toFixed(3)).slice(2,5)+"Z"
}
}())
}})(jq191);
jq191(function(){pncSocialMediaRibbon.init()
});