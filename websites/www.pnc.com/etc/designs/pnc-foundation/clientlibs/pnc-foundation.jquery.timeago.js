(function(a){if(typeof define==="function"&&define.amd){define(["jquery"],a)
}else{a(jQuery)
}})(function(f){function d(){var i=b(this);
var e=a.settings;
if(!isNaN(i.datetime)){if(e.cutoff==0||g(i.datetime)<e.cutoff){f(this).text(c(i.datetime))
}}return this
}function b(i){i=f(i);
if(!i.data("timeago")){i.data("timeago",{datetime:a.datetime(i)});
var e=f.trim(i.text());
if(a.settings.localeTitle){i.attr("title",i.data("timeago").datetime.toLocaleString())
}else{if(e.length>0&&!(a.isTime(i)&&i.attr("title"))){i.attr("title",e)
}}}return i.data("timeago")
}function c(i){return a.inWords(g(i))
}function g(i){return(new Date).getTime()-i.getTime()
}f.timeago=function(e){if(e instanceof Date){return c(e)
}else{if(typeof e==="string"){return c(f.timeago.parse(e))
}else{if(typeof e==="number"){return c(new Date(e))
}else{return c(f.timeago.datetime(e))
}}}};
var a=f.timeago;
f.extend(f.timeago,{settings:{refreshMillis:60000,allowFuture:false,localeTitle:false,cutoff:0,strings:{prefixAgo:null,prefixFromNow:null,suffixAgo:"ago",suffixFromNow:"from now",seconds:"less than a minute",minute:"about a minute",minutes:"%d minutes",hour:"about an hour",hours:"about %d hours",day:"a day",days:"%d days",month:"about a month",months:"%d months",year:"about a year",years:"%d years",wordSeparator:" ",numbers:[]}},inWords:function(z){function m(t,l){var n=f.isFunction(t)?t(l,z):t;
var u=k.numbers&&k.numbers[l]||l;
return n.replace(/%d/i,u)
}var k=this.settings.strings;
var e=k.prefixAgo;
var p=k.suffixAgo;
if(this.settings.allowFuture){if(z<0){e=k.prefixFromNow;
p=k.suffixFromNow
}}var A=Math.abs(z)/1000;
var j=A/60;
var y=j/60;
var x=y/24;
var v=x/365;
var w=A<45&&m(k.seconds,Math.round(A))||A<90&&m(k.minute,1)||j<45&&m(k.minutes,Math.round(j))||j<90&&m(k.hour,1)||y<24&&m(k.hours,Math.round(y))||y<42&&m(k.day,1)||x<30&&m(k.days,Math.round(x))||x<45&&m(k.month,1)||x<365&&m(k.months,Math.round(x/30))||v<1.5&&m(k.year,1)||m(k.years,Math.round(v));
var q=k.wordSeparator||"";
if(k.wordSeparator===undefined){q=" "
}return f.trim([e,w,p].join(q))
},parse:function(e){var i=f.trim(e);
i=i.replace(/\.\d+/,"");
i=i.replace(/-/,"/").replace(/-/,"/");
i=i.replace(/T/," ").replace(/Z/," UTC");
i=i.replace(/([\+\-]\d\d)\:?(\d\d)/," $1$2");
return new Date(i)
},datetime:function(i){var e=a.isTime(i)?f(i).attr("datetime"):f(i).attr("title");
return a.parse(e)
},isTime:function(e){return f(e).get(0).tagName.toLowerCase()==="time"
}});
var h={init:function(){var j=f.proxy(d,this);
j();
var e=a.settings;
if(e.refreshMillis>0){setInterval(j,e.refreshMillis)
}},update:function(e){f(this).data("timeago",{datetime:a.parse(e)});
d.apply(this)
},updateFromDOM:function(){f(this).data("timeago",{datetime:a.parse(a.isTime(this)?f(this).attr("datetime"):f(this).attr("title"))});
d.apply(this)
}};
f.fn.timeago=function(k,i){var j=k?h[k]:h.init;
if(!j){throw new Error('Unknown function name "'+k+'" for timeago')
}this.each(function(){j.call(this,i)
});
return this
};
document.createElement("abbr");
document.createElement("time")
});