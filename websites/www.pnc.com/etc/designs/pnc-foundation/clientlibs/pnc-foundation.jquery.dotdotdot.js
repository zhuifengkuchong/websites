(function(D){function B(j,f,E){var o=j.children(),n=!1;
j.empty();
for(var m=0,l=o.length;
l>m;
m++){var k=o.eq(m);
if(j.append(k),E&&j.append(E),y(j,f)){k.remove(),n=!0;
break
}E&&E.remove()
}return n
}function A(L,K,J,I,H){var G=L.contents(),F=!1;
L.empty();
for(var E="table, thead, tbody, tfoot, tr, col, colgroup, object, embed, param, ol, ul, dl, select, optgroup, option, textarea, script, style",f=0,e=G.length;
e>f&&!F;
f++){var d=G[f],a=D(d);
void 0!==d&&(L.append(a),H&&L[L.is(E)?"after":"append"](H),3==d.nodeType?y(J,I)&&(F=z(a,K,J,I,H)):F=A(a,K,J,I,H),F||H&&H.remove())
}return F
}function z(R,Q,P,O,N){var M=!1,L=R[0];
if(L===void 0){return !1
}for(var K="letter"==O.wrap?"":" ",J=u(L).split(K),I=-1,H=-1,G=0,F=J.length-1;
F>=G;
){var E=Math.floor((G+F)/2);
if(E==H){break
}H=E,v(L,J.slice(0,H+1).join(K)+O.ellipsis),y(P,O)?F=H:(I=H,G=H)
}if(-1==I||1==J.length&&0==J[0].length){var i=R.parent();
R.remove();
var g=N?N.length:0;
if(i.contents().size()>g){var f=i.contents().eq(-1-g);
M=z(f,Q,P,O,N)
}else{var e=i.prev(),L=e.contents().eq(-1)[0];
if(L!==void 0){var j=x(u(L),O);
v(L,j),N&&e.append(N),i.remove(),M=!0
}}}else{var j=x(J.slice(0,I+1).join(K),O);
M=!0,v(L,j)
}return M
}function y(d,c){return d.innerHeight()>c.maxHeight
}function x(a,d){for(;
D.inArray(a.slice(-1),d.lastCharacter.remove)>-1;
){a=a.slice(0,-1)
}return 0>D.inArray(a.slice(-1),d.lastCharacter.noEllipsis)&&(a+=d.ellipsis),a
}function w(b){return{width:b.innerWidth(),height:b.innerHeight()}
}function v(d,c){d.innerText?d.innerText=c:d.nodeValue?d.nodeValue=c:d.textContent&&(d.textContent=c)
}function u(b){return b.innerText?b.innerText:b.nodeValue?b.nodeValue:b.textContent?b.textContent:""
}function t(a,d){return a===void 0?!1:a?"string"==typeof a?(a=D(a,d),a.length?a:!1):"object"==typeof a?a.jquery===void 0?!1:a:!1:!1
}function s(h){for(var g=h.innerHeight(),l=["paddingTop","paddingBottom"],k=0,j=l.length;
j>k;
k++){var i=parseInt(h.css(l[k]),10);
isNaN(i)&&(i=0),g-=i
}return g
}function r(d,c){return d?(c="string"==typeof c?"dotdotdot: "+c:["dotdotdot:",c],window.console!==void 0&&window.console.log!==void 0&&window.console.log(c),!1):!1
}if(!D.fn.dotdotdot){D.fn.dotdotdot=function(h){if(0==this.length){return h&&h.debug===!1||r(!0,'No element found for "'+this.selector+'".'),this
}if(this.length>1){return this.each(function(){D(this).dotdotdot(h)
})
}var c=this;
c.data("dotdotdot")&&c.trigger("https://www.pnc.com/etc/designs/pnc-foundation/clientlibs/destroy.dot"),c.data("dotdotdot-style",c.attr("style")),c.css("word-wrap","break-word"),c.bind_events=function(){return c.bind("https://www.pnc.com/etc/designs/pnc-foundation/clientlibs/update.dot",function(g,m){g.preventDefault(),g.stopPropagation(),a.maxHeight="number"==typeof a.height?a.height:s(c),a.maxHeight+=a.tolerance,m!==void 0&&(("string"==typeof m||m instanceof HTMLElement)&&(m=D("<div />").append(m).contents()),m instanceof D&&(b=m)),d=c.wrapInner('<div class="dotdotdot" />').children(),d.empty().append(b.clone(!0)).css({height:"auto",width:"auto",border:"none",padding:0,margin:0});
var j=!1,i=!1;
return l.afterElement&&(j=l.afterElement.clone(!0),l.afterElement.remove()),y(d,a)&&(i="children"==a.wrap?B(d,a,j):A(d,c,d,a,j)),d.replaceWith(d.contents()),d=null,D.isFunction(a.callback)&&a.callback.call(c[0],i,b),l.isTruncated=i,i
}).bind("isTruncated.dot",function(g,e){return g.preventDefault(),g.stopPropagation(),"function"==typeof e&&e.call(c[0],l.isTruncated),l.isTruncated
}).bind("https://www.pnc.com/etc/designs/pnc-foundation/clientlibs/originalContent.dot",function(g,e){return g.preventDefault(),g.stopPropagation(),"function"==typeof e&&e.call(c[0],b),b
}).bind("https://www.pnc.com/etc/designs/pnc-foundation/clientlibs/destroy.dot",function(e){e.preventDefault(),e.stopPropagation(),c.unwatch().unbind_events().empty().append(b).attr("style",c.data("dotdotdot-style")).data("dotdotdot",!1)
}),c
},c.unbind_events=function(){return c.unbind(".dot"),c
},c.watch=function(){if(c.unwatch(),"window"==a.watch){var e=D(window),i=e.width(),g=e.height();
e.bind("resize.dot"+l.dotId,function(){i==e.width()&&g==e.height()&&a.windowResizeFix||(i=e.width(),g=e.height(),f&&clearInterval(f),f=setTimeout(function(){c.trigger("https://www.pnc.com/etc/designs/pnc-foundation/clientlibs/update.dot")
},10))
})
}else{k=w(c),f=setInterval(function(){var j=w(c);
(k.width!=j.width||k.height!=j.height)&&(c.trigger("https://www.pnc.com/etc/designs/pnc-foundation/clientlibs/update.dot"),k=w(c))
},100)
}return c
},c.unwatch=function(){return D(window).unbind("resize.dot"+l.dotId),f&&clearInterval(f),c
};
var b=c.contents(),a=D.extend(!0,{},D.fn.dotdotdot.defaults,h),l={},k={},f=null,d=null;
return l.afterElement=t(a.after,c),l.isTruncated=!1,l.dotId=C++,c.data("dotdotdot",!0).bind_events().trigger("https://www.pnc.com/etc/designs/pnc-foundation/clientlibs/update.dot"),a.watch&&c.watch(),c
},D.fn.dotdotdot.defaults={ellipsis:"… ",wrap:"word",lastCharacter:{remove:[" ",",",":",";",".","!","?"],noEllipsis:[]},tolerance:0,callback:null,after:null,height:null,watch:!1,windowResizeFix:!0,debug:!1};
var C=1,q=D.fn.html;
D.fn.html=function(b){return b!==void 0?this.data("dotdotdot")&&"function"!=typeof b?this.trigger("update",[b]):q.call(this,b):q.call(this)
};
var p=D.fn.text;
D.fn.text=function(a){if(a!==void 0){if(this.data("dotdotdot")){var d=D("<div />");
return d.text(a),a=d.html(),d.remove(),this.trigger("update",[a])
}return p.call(this,a)
}return p.call(this)
}
}})(jQuery);