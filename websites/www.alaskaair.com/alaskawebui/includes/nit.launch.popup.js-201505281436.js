if (!this.NIT) this.NIT = {}; // Ensure namespace exists if running as stand-alone script
if (!NIT.Launch) NIT.Launch = {};

//AKA-970, AKA-1022: Removed script source detection for the dynamic Agent path; using parent window location instead
if (window.location.hostname == 'http://www.alaskaair.com/alaskawebui/includes/www.alaskaair.com')
{
    NIT.agentURL = 'https://www.alaskaair.com/alaskawebui/Agent.aspx';
}
else
{
    NIT.agentURL = 'http://www.alaskaair.com/alaskawebui/includes/Agent.aspx';
}

//////////////////////////////////////
// Event Extensions
//////////////////////////////////////
NIT.addListener = function (obj, evt, cb) // Cross-browser event attaching ex: (obj, "onload", Init)
{
    if (obj.attachEvent)
    {
        obj.attachEvent(evt, cb); //"onload", Init );
    }
    else if (obj.addEventListener)
    {
        evt = (evt.toLowerCase().indexOf("on") == 0) ? evt.substr(2) : evt;
        obj.addEventListener(evt, cb, false); //"load", Init, false );
    }
};

NIT.getErrorMessage = function(e)
{
	// NOTE:  Permission Denied does not throw exception for: Safari 3 (Mac and PC), Chrome 1.0
	if (e.message) // IE6, IE7, FF3
		return e.message;
	else // FF1, FF1.5, FF2, FF3
		return e.toString();
};

NIT.Launch.repositionWindow = function(win, width, height, top, left)
{
	try
	{
		win.resizeTo(width, height); // Must resize before the move to make sure we can fit, otherwise the window doesn't move all the way (if moving off edge of screen)
		// The following line works best to successfully reposition, but is sometimes too much moving to look nice for smaller windows during the transition
		win.moveTo(0, 0); // This helps it work better when changing sizes, due to some OS's clipping to the visible screen.
		win.resizeTo(width, height);
		win.moveTo(left, top);
	}
	catch (e) // Permission denied? Happens if we change out of our domain, we should make sure it doesn't happen in any other situation, or comment out throw below
	{
		// if not permission denied, throw the error
		if (NIT.getErrorMessage(e).indexOf('denied') == -1)
		{
			throw e;
		}
	}
};

NIT.Launch.openWindow = function (url, windowName, align, width, height, top, left, layoutParent, parentWidth, parentHeight, options)
{
    align = (!align || align.toLowerCase() != 'left') ? 'right' : 'left'; // Default to right if not 'left'
    width = (width) ? width : 250; // Default width: 250px
    height = (height) ? height : screen.availHeight; // Default height: 100%
    top = (top) ? top : 0; // Default top: 0px
    left = (left) ? left : 0; // Default left: 0px
    layoutParent = (layoutParent) ? true : false;
    parentWidth = (parentWidth && parentWidth > 0) ? parentWidth : screen.availWidth; // Default height: 100%
    parentHeight = (parentHeight && parentHeight > 0) ? parentHeight : screen.availHeight; // Default height: 100%
    options = (options) ? options : 'scrollbars=no,menubar=no,resizable=yes,location=no,status=yes,titlebar=no,toolbar=no';

    var parentLeft = (align == 'left') ? width : 0;

    // Parent width not specified or not enough space for parent window and need to shrink to fit
    if (parentWidth + width > screen.availWidth)
    {
        parentWidth = screen.availWidth - width; // Must also make parent window fit
    }
    // If Agent height is taller than it can fit within the screenheight
    if (height + top > screen.availHeight)
    {
        height = screen.availHeight - top; // Must also make agent window fit
    }
    // Parent height not specified or taller than can fit
    if (parentHeight > screen.availHeight)
    {
        parentHeight = screen.availHeight; // Must also make parent window fit
    }

    if (align == 'right') // Detect "left" and ignore setting
    {
        if (parentWidth + self.width > screen.availWidth) // If not enough space, align on the right screen edge
        {
            left = screen.availWidth - self.width;
            //parentWidth = left; // Adjust parent width too so it sizes correctly if we do LayoutParent?
        }
        else // Align to the right of the parent window
        {
            left = parentWidth;
        }
    }

    var allOptions = 'width=' + width + 'px,height=' + height + 'px,left=' + left + ',top=' + top + ',' + options;

    var win;

    //JIRA:AKA-488:Begin
    //Even if we are in same-sub domain virtually, we need to use cookie to track agent launch
    //alaska changes the domain to remove 'www' certain pages (home page) that broke our earlier launch script
    var agentOpen = NIT.CookieUtil.getCookie('NIT_Session');

    if (agentOpen) //if cookie exists, agent is open, just focus on the window
    {
        win = window.open('', windowName, allOptions);
        if (url.indexOf("#") > 0)
            win.location.href = url;
        NIT.Launch.repositionWindow(win, width, height, top, left);
        win.focus();
    }
    else
    {
        //agent isn't open; set parent window association
        NIT.assignParentWindowName();
        //open agent window with correct URL
        win = window.open(url, windowName, allOptions);

        if (win)
        {
            NIT.Launch.repositionWindow(win, width, height, top, left);
            if (layoutParent)
                NIT.Launch.repositionWindow(self, parentWidth, parentHeight, 0, parentLeft);

            win.focus();
        }
    }
    //JIRA:AKA-488:End
    return win; // Return window reference in case we need to use it
};

NIT.Launch.openAgentWindow = function (launchPointName)
{
    var agentURL = NIT.agentURL;
    if (launchPointName)
    {
        agentURL += "#LaunchPointName=" + launchPointName;
    }

    NIT.Launch.openWindow(agentURL, 'Agent', 'right', 255, 680, 50, 0, true);
};

///////////////////Custom//////////////////////////////////////////
NIT.ParentWindowNameCookieName = 'ActiveAgent_ParentWindowName';
NIT.ParentWindowNamePrefixAgentAsChild = 'ParentWindowAgentAsChild';

NIT.refreshParentInHttps = function ()
{
    //check if parent window requires reload in https protocol
    var httpSProtocol = "https:";
    var httpProtocol = "http:";

    if (location.protocol.toLowerCase() == httpProtocol)
    {
        location.href = location.href.replace(httpProtocol, httpSProtocol);
    }
}

NIT.isAgentAssociatedParentWindow = function ()
{
    var returnValue = false;

    var windowName = window.name;
    var windowNameCookie = NIT.CookieUtil.getCookie(NIT.ParentWindowNameCookieName);

    if (windowNameCookie != null)
    {
        var expectedWindowName = windowNameCookie.value;

        if (expectedWindowName == windowName)
        {
            returnValue = true;
        }
    }

    return returnValue;
}

// Called when Agent is launched, just before refreshing into https
// Sets window.name and ActiveAgent_ParentWindowName cookie
NIT.assignParentWindowName = function ()
{
    var windowName = NIT.ParentWindowNamePrefixAgentAsChild + Math.floor(Math.random() * 9000 + 1000);
    window.name = windowName;

    var windowNameCookie = new NIT.Cookie(NIT.ParentWindowNameCookieName, windowName);
    windowNameCookie.save();
};

//////////////////////////////////////
// Begin: NIT.Cookie.js
//////////////////////////////////////
NIT.Cookie = function (name, sVals, exp)
{
    var me = this;
    this.name = name;
    this.value = null;
    this.values = new Object();
    // These three are for saving cookies
    this.expires = (exp) ? exp : null; // Leave null for session cookie (or if updating cookie)
    this.path = '/';
    this.secure = false;
    this.domain = NIT.CookieUtil.getDomain();

    if (sVals != null) // Parse specified value(s) 
    {
        var nvc = (typeof (sVals) == "string") ? sVals.split('&') : null; // Get the name-value collection from the cookie
        if (nvc != null && nvc.length > 0 && sVals.indexOf('=') > -1)
        {
            for (var i = 0; i < nvc.length; i++)
            {
                var nv = nvc[i].split('='); // Get the name and value of this entry
                if (nv.length > 1)
                    me.values[nv[0]] = nvc[i].substr(nv[0].length + 1); //nv[1]; // Add property to our Values (remove the name, since the content may also have '=' characters)
                else if (i == 0)
                    me.value = nv[0]; // If no equal sign and the first entry, it is the main property

            }
        }
        else // Single value cookie
            me.value = sVals;
    }

    // Methods
    this.save = function ()
    {
        var v = (me.value != null) ? me.value : '';
        for (var n in me.values)
        {
            var val = (me.values[n] != null) ? me.values[n] : '';
            v += '&' + n + '=' + val; //escape(val); // No longer escaped, now matching how .NET does it
        }
        if (v[0] == '&')
            v = v.substr(1);

        var c = this.name + '=' + v +
			((me.expires == null) ? "" : (";expires=" + me.expires.toGMTString())) +
			";path=" + this.path +
			((me.domain == null) ? "" : (";domain=" + me.domain)) +
			((me.secure) ? ";secure;" : ";");
        document.cookie = c;
    };

    this.remove = function ()
    {
        me.expires = new Date(1970, 1, 2); // "Fri, 02-Jan-1970 00:00:00 GMT" );
        me.save();
    };
};

NIT.CookieUtil = new function ()
{
    var me = this;
    this.getCookies = function () // Parses all available cookies
    {
        var all = new Object();
        if (document.cookie != "")
        {
            var cookies = document.cookie.split("; ");
            for (i = 0; i < cookies.length; i++)
            {
                var c = cookies[i];
                var idx = c.indexOf('=');
                var N = c.substr(0, idx);
                var V = '';
                if (c.length > idx + 1) // Not an empty value (just in case)
                    V = c.substring(idx + 1, c.length); //unescape( c.substring(idx+1, c.length) ); // No longer escaped, now matching how .NET does it
                all[N] = new NIT.Cookie(N, V);
            }
        }
        return all;
    };

    this.getCookie = function (name) // Selects a cookie by name
    {
        return me.getCookies()[name];
    };

    this.showCookies = function ()
    {
        var cookies = me.getCookies();
        var sCookie = '';
        for (var crumb in cookies)
        {
            sCookie += 'Name: ' + cookies[crumb].name + '\n';
            sCookie += 'Value: ' + cookies[crumb].value + '\n';
            // now show Values array for the current crumb
            for (var values in cookies[crumb].values)
            {
                sCookie += "    " + values + ": ";
                sCookie += cookies[crumb].values[values] + "\n";
            }
        }
        //alert(sCookie);
        return sCookie;
    };

    this.getDomain = function ()
    {
        var url = document.domain;
        var end = "";

        if (url.indexOf('.') > -1)
        {
            end = url.substr(url.lastIndexOf('.'));
            url = url.substring(0, url.lastIndexOf('.'));
        }
        if (url.indexOf('.') > -1)
        {
            url = url.substr(url.lastIndexOf('.') + 1);
        }
        url = url + end;
        if (url.indexOf('.') == -1)
        {
            url = null;
        }

        if (url && (/^[0-9]+.[0-9]+$/g).test(url)) // Fix for when we're referencing by IP address
            return null;

        return url;
    };

    this.isCookiesEnabled = function ()
    {
        // set a cookie then test to see if it was set properly
        var n = "Test";
        var c = new NIT.Cookie(n, n);
        c.save(); // Save in cookie collection
        c = me.getCookie(n); // Check that we can retrieve it
        if (c) c.remove(); // Cleanup

        return (c != null && c.value == n) ? true : false;
    };
};
//////////////////////////////////////
// End: NIT.Cookie.js
//////////////////////////////////////

//////////////////////////////////////
// Begin: CookieCommand Section (this section is shared between multiple files)
//////////////////////////////////////
NIT.CookieCommand = new function ()
{
    var me = this;
    var COOKIE_NAME = "NIT_CMD";

    this.navigateUrl;
    this.isNavigating = false;
    this.pageChanged = false; // New, for the agent side

    this.write = function ()
    {
        var c = new NIT.Cookie(COOKIE_NAME);
        if (me.navigateUrl)
        {
            // Avoid null string concatenation
            c.values.navigateUrl = encodeURIComponent(me.navigateUrl);
        }

        c.values.isNavigating = me.isNavigating;
        c.values.pageChanged = me.pageChanged;
        c.save();
    };

    this.read = function ()
    {
        me.navigateUrl = null;

        var c = NIT.CookieUtil.getCookie(COOKIE_NAME);
        if (c && c.values)
        {
            if (c.values.navigateUrl)
            {
                me.navigateUrl = decodeURIComponent(c.values.navigateUrl);
            }

            me.isNavigating = (c.values.isNavigating == "true");
            me.pageChanged = (c.values.pageChanged == "true");
        }
    };
};
//////////////////////////////////////
// End: CookieCommand Section
//////////////////////////////////////

//////////////////////////////////////
// Begin: InfoCookie Section 
//////////////////////////////////////
NIT.InfoCookie = new function ()
{
    var me = this;
    var COOKIE_NAME = "NIT_AS_INFO";

    this.pageName = '';
    this.formName = '';
    this.viewID = '';

    this.save = function ()
    {
        if (NIT.isAgentAssociatedParentWindow())
        {
            var url = me.pageName;
            if (url == '')
                url = document.URL;

            //remove querystring
            if (url.indexOf("?") > 0)
                url = url.substring(0, url.indexOf("?"));

            //remove hashmark
            if (url.indexOf("#") > 0)
                url = url.substring(0, url.indexOf("#"));

            var c = new NIT.Cookie(COOKIE_NAME);
            c.values.pageName = encodeURIComponent(url);
            c.values.formName = encodeURIComponent(me.formName);
            c.values.viewID = encodeURIComponent(me.viewID);
            c.save();
        }
    };

    this.read = function ()
    {
        me.pageName = '';
        me.formName = '';
        me.viewID = '';

        var c = NIT.CookieUtil.getCookie(COOKIE_NAME);
        if (c && c.values)
        {
            if (c.values.pageName)
            {
                me.pageName = c.values.pageName;
            }
            if (c.values.formName)
            {
                me.formName = c.values.formName;
            }
            if (c.values.viewID)
            {
                me.viewID = c.values.viewID;
            }
        }
    };
};
//////////////////////////////////////
// End: InfoCookie Section
//////////////////////////////////////

NIT.Launch.Integration = new function ()
{
    var me = this;


    NIT.CookieCommand.read();
    if (NIT.CookieCommand.isNavigating)
    {
        NIT.CookieCommand.isNavigating = false;
        NIT.CookieCommand.write();
    }
    else // Page changed, not navigated by agent
    {
        NIT.CookieCommand.pageChanged = true;
        NIT.CookieCommand.write();
    }

    // Setup cookie polling for two-way commands
    this.checkCommandCookie = function ()
    {
        if (NIT.isAgentAssociatedParentWindow()) //Only associated parent window performs the navigation
        {
            NIT.CookieCommand.read();
            if (NIT.CookieCommand.navigateUrl)
            {
                var url = NIT.CookieCommand.navigateUrl;

                location.href = url;

                NIT.CookieCommand.navigateUrl = null;
                NIT.CookieCommand.write();
            }
        }
        setTimeout(me.checkCommandCookie, 500);
    };
    this.checkCommandCookie();

//    this.onPageLoad = function ()
//    {
//        //NIT.InfoCookie.save();
//    };

//    this.onUnload = function ()
//    {
//        // do page unload stuff
//    };

//    //add window onload listener
//    NIT.addListener(window, "onload", this.onPageLoad);

//    //add window onunload listener
//    NIT.addListener(window, "onunload", this.onUnload);
};

/*************************************
Interface functions, can't be changed, These functions are consumed by Alaska
D0 NOT CHANGE METHOD NAME OR NUMBER OF PARAMETERS
**************************************/

function ActiveAgent_LaunchJenn(question, hasMP, hasVisa)
{
    var AgentUrl = NIT.agentURL;
    
    if (question && question.length > 0)
    {
        AgentUrl = NIT.agentURL + '#q=' + encodeURIComponent(question);
    }

    if (hasMP && hasMP.length > 0) 
    {
        AgentUrl += ((AgentUrl.indexOf('#') > -1) ? '&' : '#')  + 'hasMP=' + encodeURIComponent(hasMP);
    }

    if (hasVisa && hasVisa.length > 0) 
    {
        AgentUrl += ((AgentUrl.indexOf('#') > -1) ? '&' : '#') + 'hasVisa=' + encodeURIComponent(hasVisa);
    }

    //Agent window width/height and parent window width/height must match popupsettings control in agent.aspx
    var options = 'scrollbars=no,menubar=no,resizable=yes,location=no,status=yes,titlebar=no,toolbar=no';

    //JIRA:AKA-492: Parent window width changed to account of re-designed web site
    //If the Agent window is not already open, this also establishes parent window association
    NIT.Launch.openWindow(AgentUrl, 'AlaskaExternalAgent', 'right', 255, 680, 50, 0, true, 0, 0, options);

    //JIRA:AKA-1302
    NIT.InfoCookie.save();
}

function ActiveAgent_SetPageInfo(formName, viewID)
{
    NIT.InfoCookie.pageName = '';
    NIT.InfoCookie.formName = formName;
    NIT.InfoCookie.viewID = viewID;
    NIT.InfoCookie.save();
};
