//version [201505270728] cached at [5/28/2015 3:55:39 PM]
//combined files: widget_car_hotel.js profileUtils.js contentslider.js 
//starts: widget_car_hotel.js
// Need to add domain name for cross domain javascript issues, iseatz needs to do the same on their side
document.domain = 'http://www.alaskaair.com/files/alaskaair.com';

var alertTimerId = 0;
var errorPathHotel = "/Shopping/Hotel/WriteErrorMessage";
var errorPathCar = "/Shopping/Car/WriteErrorMessage";
var errorMessage = '';
var referringPage = '';
var log = '';
var exceptionMessageWidgetHotel = "iSeatz Hotel Widget Exception |";    //each page using the widget should set this variable to include the page name (see hotel deals page)
var exceptionMessageWidgetCar = "iSeatz Car Widget Exception |";

// Generic event handler invoked by inpath pages
// event: js object with at least 1 attribute:
//      .name = the name of the event
// opts: js object with extra arbitrary data

function iseatzEventHandler(event, opts) {

    try
    {

        if (event.name == 'SearchSubmitForm') {
            if (opts.searchType == 'Hotel') {
                if (referringPage == "DestinationsNav") {
                    referringPage = "CityGuidesHotelFormlet";
                }
                else if (referringPage == "HomePageNav") {
                    referringPage = "HomePageHotelFormlet";
                }
                else if (referringPage == "HotelDealsNav") {
                    referringPage = "HotelDealsFormlet";
                }
                saveSearchHotelParameters(opts);
                if (!hasHotelInCart) {
                    trackeVar59(referringPage);
                }
                redirectToPBHotel(opts);
            }
            else if (opts.searchType == 'Car') {
                if (referringPage == "DestinationsNav") {
                    referringPage = "CityGuidesCarFormlet";
                }
                else if (referringPage == "HomePageNav") {
                    referringPage = "HomePageCarFormlet";
                }
                else if (referringPage == "HotelDealsNav") {
                    referringPage = "HotelDealsFormlet";
                }

                saveSearchCarParameters(opts);
		        if (!hasCarInCart) {
		            trackeVar60(referringPage);
                }
                redirectToPBCar(opts);
            }
        }
    }
    catch (e) {
        // Set Generic Error Message
        var hoteliframe = document.getElementById('_hotelIframe');
        var cariframe = document.getElementById('_carIframe');
        if (hoteliframe != null) {
            SetErrorMessage(BuildErrorMessage(exceptionMessageWidgetHotel, "IseatzEventHandlerException", e.message, getCallstack(e), ""), hotelReferrer, hotelLogType);
            writeCarExceptionError();
        }
        else if (cariframe != null) {
            SetErrorMessage(BuildErrorMessage(exceptionMessageWidgetCar, "IseatzEventHandlerException", e.message, getCallstack(e), ""), carReferrer, carLogType);
            writeHotelExceptionError();
        }
    }
}

function saveSearchHotelParameters(opts) {
    var url = "/Shopping/Hotel/SaveSearchParameters";

    var destinationFor;
    var destinationName;
    var dateRange;
    var roomOccupancy;

    if (opts != null &&
			opts.searchParameters != null) {
        if (opts.searchParameters.For) { destinationFor = opts.searchParameters.For; }
        if (opts.searchParameters.ForName) { destinationName = opts.searchParameters.ForName; }
        if (opts.searchParameters.DateRange) { dateRange = opts.searchParameters.DateRange; }
        if (opts.searchParameters.RoomOccupancy) { roomOccupancy = opts.searchParameters.RoomOccupancy; }
    }

    if (opts != null &&
			opts.searchParameters != null &&
            destinationName != null &&
            dateRange != null &&
			destinationName.length > 0 &&
			dateRange.length > 0) {

        $.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            data: ({ destination: destinationFor, destinationname: destinationName, daterange: dateRange, roomoccupancy: roomOccupancy, formorigin: "WidgetHotel" }),
            complete: function (response, textStatus) {
                switch (textStatus) {
                    case "timeout":
                        $.showActionMessage(REQUEST_TIMED_OUT, true);
                        break;
                    case "error":
                        $.showActionMessage(ERROR_ENCOUNTERED, true);
                        break;
                    default:
                        var msg = response.responseText;
                        break;
                }
            }
        });
    }

    return false;
}

function redirectToPBHotel(opts) {
    // Redirecting to this secure URL = "https://www.alaskaair.com/Shopping/Hotel/Search";
    var frameableURL = opts.urlResults;

    var dynamicForm = document.createElement("FORM");
    document.body.appendChild(dynamicForm);
    dynamicForm.method = "POST";

    if (document.getElementById('RedirectHotelHREF')) {
        dynamicForm.action = $('#RedirectHotelHREF').attr('href');
    } else {
        dynamicForm.action = "https://www.alaskaair.com/Shopping/Hotel/Search";
    }

    var hiddenField = document.createElement("input");
    hiddenField.setAttribute("type", "hidden");
    hiddenField.setAttribute("name", "hotelSearchURL");
    hiddenField.setAttribute("value", frameableURL);
    dynamicForm.appendChild(hiddenField);

    var hiddenField2 = document.createElement("input");
    hiddenField2.setAttribute("type", "hidden");
    hiddenField2.setAttribute("name", "referringLink");
    hiddenField2.setAttribute("value", referringPage);
    dynamicForm.appendChild(hiddenField2);

    dynamicForm.submit();

    return false;
}

function saveSearchCarParameters(opts) {
    var url = "/Shopping/Car/SaveSearchParameters";

    var pickUpLocation;
    var pickUpLocationName;
    var dropOffLocation;
    var dropOffLocationName;
    var dateTimeRange;

    if (opts != null && opts.searchParameters != null) {
        if (opts.searchParameters.StartLocationId) { pickUpLocation = opts.searchParameters.StartLocationId; }
        if (opts.searchParameters.StartLocationName) { pickUpLocationName = opts.searchParameters.StartLocationName; }
        if (opts.searchParameters.EndLocationId) { dropOffLocation = opts.searchParameters.EndLocationId; }
        if (opts.searchParameters.EndLocationName) { dropOffLocationName = opts.searchParameters.EndLocationName; }
        if (opts.searchParameters.DateTimeRange) { dateTimeRange = opts.searchParameters.DateTimeRange; }
    }
    if (opts != null &&
			opts.searchParameters != null &&
            pickUpLocation != null &&
            dateTimeRange != null &&
			pickUpLocation.length > 0 &&
			dateTimeRange.length > 0) {

        $.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            data: ({ pickuplocation: pickUpLocation, pickuplocationname: pickUpLocationName, dropofflocation: dropOffLocation, dropofflocationname: dropOffLocationName, datetimerange: dateTimeRange, formorigin: "WidgetCar" }),
            complete: function (response, textStatus) {
                switch (textStatus) {
                    case "timeout":
                        $.showActionMessage(REQUEST_TIMED_OUT, true);
                        break;
                    case "error":
                        $.showActionMessage(ERROR_ENCOUNTERED, true);
                        break;
                    default:
                        var msg = response.responseText;
                        break;
                }
            }
        });
    }

    return false;
}

function redirectToPBCar(opts) {
    // Redirecting to this secure URL =  "https://www.alaskaair.com/Shopping/Car/Search";
    var frameableURL = opts.urlResults;

    var dynamicForm = document.createElement("FORM");
    document.body.appendChild(dynamicForm);
    dynamicForm.method = "POST";

    if (document.getElementById('RedirectCarHREF')) {
        dynamicForm.action = $('#RedirectCarHREF').attr('href');
    } else {
        dynamicForm.action = "https://www.alaskaair.com/Shopping/Car/Search";
    }

    var hiddenField = document.createElement("input");
    hiddenField.setAttribute("type", "hidden");
    hiddenField.setAttribute("name", "carSearchURL");
    hiddenField.setAttribute("value", frameableURL);
    dynamicForm.appendChild(hiddenField);

    var hiddenField2 = document.createElement("input");
    hiddenField2.setAttribute("type", "hidden");
    hiddenField2.setAttribute("name", "referringLink");
    hiddenField2.setAttribute("value", referringPage);
    dynamicForm.appendChild(hiddenField2);

    dynamicForm.submit();

    return false;
}

function writeStatusMessage(urlPath, errorMessage, referringLink, logType, loadStatus) {
    var url = urlPath;

    if (errorMessage != null &&
			errorMessage.length > 0) {

	    errorMessage = errorMessage + " | " + navigator.userAgent + " | from " + referringLink;

        $.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            data: ({ errormessage: errorMessage, referringLink: referringLink, logType: logType, loadStatus: loadStatus }),
            complete: function (response, textStatus) {
                switch (textStatus) {
                    case "timeout":
                        $.showActionMessage(REQUEST_TIMED_OUT, true);
                        break;
                    case "error":
                        $.showActionMessage(ERROR_ENCOUNTERED, true);
                        break;
                    default:
                        var msg = response.responseText;
                        break;
                }
            }
        });
    }

    return false;
}

function SetErrorMessage(message, referringLink, logType) {
    errorMessage = message;
    referringPage = referringLink;
    log = logType;
}

function writeHotelExceptionError() {
    $('#_hotelIframe').hide();
    $('#hotelInterstitial').hide();
    $('#hotelInterstitial').height(0);
    $('#_hotelIframe').height(0);
    $('#hoteliFrameError').show();
    $('#hoteliFrameError').focus();
    writeStatusMessage(errorPathHotel, errorMessage, referringPage, log, 'fail');
}

function writeCarExceptionError() {
    $('#_carIframe').hide();
    $('#carInterstitial').hide();
    $('#carInterstitial').height(0);
    $('#_carIframe').height(0);
    $('#cariFrameError').show();
    $('#cariFrameError').focus();
    writeStatusMessage(errorPathCar, errorMessage, referringPage, log, 'fail');
}
function writeHotelOffersExceptionError() {
    $('#_hotelIframe').hide();
    $('#_hotelIframe').height(0);
    $('#hotelInterstitial').hide();
    $('#hotelInterstitial').height(0);
    $('#hotelIFrameError').show();
    $('#hoteliFrameError').focus();
    writeStatusMessage(errorPathHotel, errorMessage, referringPage, log, 'fail');
}
function SetHotelTimer(timeout) {
    ResetTimer();
    alertTimerId = setTimeout("writeHotelExceptionError()", timeout);
}

function SetCarTimer(timeout) {
    ResetTimer();
    alertTimerId = setTimeout("writeCarExceptionError()", timeout);
}
function SetHotelOffersTimer(timeout) {
    ResetTimer();
    alertTimerId = setTimeout("writeHotelOffersExceptionError()", timeout);
}
function ResetTimer() {
    if (alertTimerId != 0)
        clearTimeout(alertTimerId);
}
function BuildErrorMessage(title, location, errorMessage, callStack, addlInfo) {
    // Error message format is "iSeatz PB Hotel Exception | page | error location/type | error message | call stack if available | error info (iSeatz url etc) | user agent | referring page"
    // (title variable already contains page)
    return title + " | " + location + " | " + errorMessage + " | " + callStack + " | " + addlInfo;  // user agent and referring page added when logging
}
function getCallstack(e)
{
	try {
		if (e.stack) { return e.stack; }	//depending on the exception, either the stack or the filename will be defined
		else if (e.filename) { return e.filename + ":" + e.lineNumber; }
		else { return ""; }		//some browsers (IE!) do not have any stacktrace info defined
	}
    catch (e) { return ""; }
}

//ends: widget_car_hotel.js
//starts: profileUtils.js
  var cval=document.cookie,x=0,dest,mbr,y=0,fam,card,emails,boardroom;
  cval=unescape(cval);
  x=cval.indexOf("AS_dest");
  if(x>0)
  {
	  x+=8;
	  dest=cval.substring(x,x+3);
	  dest='profile.PARAMETER1=' + dest;
	  x=0;
  }
  else dest="profile.PARAMETER1=None";

  x=cval.indexOf("AS_mbr");
  if(x>0)
  {
	  x+=7;
	  mbr=cval.substring(x,x+9);
	  y=mbr.indexOf("|");
	  mbr=mbr.substring(0,y);
	  mbr='profile.PARAMETER2=' + mbr;
	  x=0;
  }
  else mbr="profile.PARAMETER2=None";


  x=cval.indexOf("AS_fam");
  if(x>0)
  {
	  x+=7;
	  fam=cval.substring(x,x+3);
	  y=fam.indexOf("|");
	  fam=fam.substring(0,y);
	  fam='profile.PARAMETER3=' + fam;
	  x=0;
	  y=0;
  }
  else fam="profile.PARAMETER3=0";

  x=cval.indexOf("AS_card");
  if(x>0)
  {
	  x+=8;
	  card=cval.substring(x,x+4);
	  card='profile.PARAMETER4=' + card;
	  x=0;
  }
  else card="profile.PARAMETER4=None";

  x=cval.indexOf("AS_Subscrx");
  if(x>0)
  {
	  x+=11;
	  emails=cval.substring(x,x+31);
	  y=emails.indexOf("|");
	  if(y>0)
		  emails=emails.substring(0,y);
	  emails='profile.PARAMETER5=' + emails;
	  x=0;
	  y=0;
  }else emails = "profile.PARAMETER5=None";

var c = unescape(cval);
x = c.indexOf("AS_BR");
if (x > 0) {
    x += 13;
    boardroom = c.substring(x, x + 10);
    y = boardroom.indexOf("|");
    if (y > 0)
        boardroom = boardroom.substring(0, y);
    boardroom = 'profile.BOARDROOM=' + boardroom;
    x = 0;
    y = 0;
}
else boardroom = "profile.BOARDROOM=None";
//ends: profileUtils.js
//starts: contentslider.js
//** Featured Content Slider script- (c) Dynamic Drive DHTML code library: http://www.dynamicdrive.com.
var featuredcontentslider = {

    //3 variables below you can customize if desired:
    ajaxloadingmsg: '<div style="margin: 20px 0 0 20px"><img src="../images/spinwheel-waiting.gif"/*tpa=http://www.alaskaair.com//images/spinwheel-waiting.gif*/ /> Fetching content. Please wait...</div>',
    bustajaxcache: true, //bust caching of external ajax page after 1st request?
    enablepersist: true, //persist to last content viewed when returning to page?

    settingcaches: {}, //object to cache "setting" object of each script instance

    jumpTo: function (fcsid, pagenumber) { //public function to go to a slide manually.
        this.turnpage(this.settingcaches[fcsid], pagenumber);
    },

    ajaxconnect: function (setting) {
        var page_request = false;
        if (window.ActiveXObject) { //Test for support for ActiveXObject in IE first (as XMLHttpRequest in IE7 is broken)
            try {
                page_request = new ActiveXObject("Msxml2.XMLHTTP");
            }
            catch (e) {
                try {
                    page_request = new ActiveXObject("Microsoft.XMLHTTP");
                }
                catch (e) { }
            }
        }
        else if (window.XMLHttpRequest) // if Mozilla, Safari etc
            page_request = new XMLHttpRequest();
        else
            return false;
        var pageurl = setting.contentsource[1];
        page_request.onreadystatechange = function () {
            featuredcontentslider.ajaxpopulate(page_request, setting);
        };
        document.getElementById(setting.id).innerHTML = this.ajaxloadingmsg;
        var bustcache = (!this.bustajaxcache) ? "" : (pageurl.indexOf("?") != -1) ? "&" + new Date().getTime() : "?" + new Date().getTime();
        page_request.open('GET', pageurl + bustcache, true);
        page_request.send(null);
    },

    ajaxpopulate: function (page_request, setting) {
        if (page_request.readyState == 4 && (page_request.status == 200 || window.location.href.indexOf("http") == -1)) {
            document.getElementById(setting.id).innerHTML = page_request.responseText;
            this.buildpaginate(setting);
        }
    },

    buildcontentdivs: function (setting) {
        var alldivs = document.getElementById(setting.id).getElementsByTagName("div");
        for (var i = 0; i < alldivs.length; i++) {
            if (this.css(alldivs[i], "contentdiv", "check")) { //check for DIVs with class "contentdiv"
                setting.contentdivs.push(alldivs[i]);
                alldivs[i].style.display = "none"; //collapse all content DIVs to begin with
            }
        }
    },

    buildpaginate: function (setting) {
        this.buildcontentdivs(setting);
        var sliderdiv = document.getElementById(setting.id);
        var pdiv = document.getElementById("paginate-" + setting.id);
        var phtml = "";
        var toc = setting.toc;
        var nextprev = setting.nextprev;
        if (typeof toc == "string" && toc != "markup" || typeof toc == "object") {
            for (var i = 1; i <= setting.contentdivs.length; i++) {
                phtml += '<div class="box-links"><a href="#' + i + '" class="toc">' + (typeof toc == "string" ? toc.replace(/#increment/, i) : toc[i - 1]) + '</a></div> ';
            }
            phtml = (nextprev[0] != '' ? '<div class="box-container"><a href="#prev" class="prev">' + nextprev[0] + '</a> ' : '') + phtml + (nextprev[1] != '' ? '<a href="#next" class="next">' + nextprev[1] + '</a></div>' : '');
            pdiv.innerHTML = phtml;
        }
        var pdivlinks = pdiv.getElementsByTagName("a");
        var toclinkscount = 0; //var to keep track of actual # of toc links
        for (var i = 0; i < pdivlinks.length; i++) {
            if (this.css(pdivlinks[i], "toc", "check")) {
                if (toclinkscount > setting.contentdivs.length - 1) { //if this toc link is out of range (user defined more toc links then there are contents)
                    pdivlinks[i].style.display = "none"; //hide this toc link
                    continue;
                }
                pdivlinks[i].setAttribute("rel", ++toclinkscount); //store page number inside toc link
                pdivlinks[i][setting.revealtype] = function () {
                    featuredcontentslider.turnpage(setting, this.getAttribute("rel"));
                    return false;
                };
                setting.toclinks.push(pdivlinks[i]);
            }
            else if (this.css(pdivlinks[i], "prev", "check") || this.css(pdivlinks[i], "next", "check")) { //check for links with class "prev" or "next"
                pdivlinks[i].onclick = function () {
                    featuredcontentslider.turnpage(setting, this.className);
                    return false;
                };
            }
        }
        this.turnpage(setting, setting.currentpage, true);
        if (setting.autorotate[0]) { //if auto rotate enabled
            pdiv[setting.revealtype] = function () {
                featuredcontentslider.cleartimer(setting, window["fcsautorun" + setting.id]);
            };
            sliderdiv["onclick"] = function () { //stop content slider when slides themselves are clicked on
                featuredcontentslider.cleartimer(setting, window["fcsautorun" + setting.id]);
            };
            setting.autorotate[1] = setting.autorotate[1] + (1 / setting.enablefade[1] * 50); //add time to run fade animation (roughly) to delay between rotation
            this.autorotate(setting);
        }
    },

    urlparamselect: function (fcsid) {
        var result = window.location.search.match(new RegExp(fcsid + "=(\\d+)", "i")); //check for "?featuredcontentsliderid=2" in URL
        return (result == null) ? null : parseInt(RegExp.$1); //returns null or index, where index (int) is the selected tab's index
    },

    turnpage: function (setting, thepage, autocall) {
        var currentpage = setting.currentpage; //current page # before change
        var totalpages = setting.contentdivs.length;
        var turntopage = (/prev/i.test(thepage)) ? currentpage - 1 : (/next/i.test(thepage)) ? currentpage + 1 : parseInt(thepage);
        turntopage = (turntopage < 1) ? totalpages : (turntopage > totalpages) ? 1 : turntopage; //test for out of bound and adjust
        if (turntopage == setting.currentpage && typeof autocall == "undefined") //if a pagination link is clicked on repeatedly
            return;
        setting.currentpage = turntopage;
        setting.contentdivs[turntopage - 1].style.zIndex = ++setting.topzindex;
        this.cleartimer(setting, window["fcsfade" + setting.id]);
        setting.cacheprevpage = setting.prevpage;
        if (setting.enablefade[0] == true) {
            setting.curopacity = 0;
            this.fadeup(setting);
        }
        if (setting.enablefade[0] == false) { //if fade is disabled, fire onChange event immediately (verus after fade is complete)
            setting.contentdivs[setting.prevpage - 1].style.display = "none"; //collapse last content div shown (it was set to "block")
            setting.onChange(setting.prevpage, setting.currentpage);
        }
        setting.contentdivs[turntopage - 1].style.visibility = "visible";
        setting.contentdivs[turntopage - 1].style.display = "block";
        if (setting.prevpage <= setting.toclinks.length) //make sure pagination link exists (may not if manually defined via "markup", and user omitted)
            this.css(setting.toclinks[setting.prevpage - 1], "selected", "remove");
        if (turntopage <= setting.toclinks.length) //make sure pagination link exists (may not if manually defined via "markup", and user omitted)
            this.css(setting.toclinks[turntopage - 1], "selected", "add");
        setting.prevpage = turntopage;
        if (this.enablepersist)
            this.setCookie("fcspersist" + setting.id, turntopage);
    },

    setopacity: function (setting, value) { //Sets the opacity of targetobject based on the passed in value setting (0 to 1 and in between)
        var targetobject = setting.contentdivs[setting.currentpage - 1];
        if (targetobject.filters && targetobject.filters[0]) { //IE syntax
            if (typeof targetobject.filters[0].opacity == "number") //IE6
                targetobject.filters[0].opacity = value * 100;
            else //IE 5.5
                targetobject.style.filter = "alpha(opacity=" + value * 100 + ")";
        }
        else if (typeof targetobject.style.MozOpacity != "undefined") //Old Mozilla syntax
            targetobject.style.MozOpacity = value;
        else if (typeof targetobject.style.opacity != "undefined") //Standard opacity syntax
            targetobject.style.opacity = value;
        setting.curopacity = value;
    },

    fadeup: function (setting) {
        if (setting.curopacity < 1) {
            this.setopacity(setting, setting.curopacity + setting.enablefade[1]);
            window["fcsfade" + setting.id] = setTimeout(function () { featuredcontentslider.fadeup(setting) }, 50);
        }
        else { //when fade is complete
            if (setting.cacheprevpage != setting.currentpage) //if previous content isn't the same as the current shown div (happens the first time the page loads/ script is run)
                setting.contentdivs[setting.cacheprevpage - 1].style.display = "none"; //collapse last content div shown (it was set to "block")
            setting.onChange(setting.cacheprevpage, setting.currentpage);
        }
    },

    cleartimer: function (setting, timervar) {
        if (typeof timervar != "undefined") {
            clearTimeout(timervar);
            clearInterval(timervar);
            if (setting.cacheprevpage != setting.currentpage) { //if previous content isn't the same as the current shown div
                setting.contentdivs[setting.cacheprevpage - 1].style.display = "none";
            }
        }
    },

    css: function (el, targetclass, action) {
        var needle = new RegExp("(^|\\s+)" + targetclass + "($|\\s+)", "ig");
        if (action == "check")
            return needle.test(el.className);
        else if (action == "remove")
            el.className = el.className.replace(needle, "");
        else if (action == "add")
            el.className += " " + targetclass;
    },

    autorotate: function (setting) {
        window["fcsautorun" + setting.id] = setInterval(function () { featuredcontentslider.turnpage(setting, "next") }, setting.autorotate[1]);
    },

    getCookie: function (Name) {
        var re = new RegExp(Name + "=[^;]+", "i"); //construct RE to search for target name/value pair
        if (document.cookie.match(re)) //if cookie found
            return document.cookie.match(re)[0].split("=")[1]; //return its value
        return null;
    },

    setCookie: function (name, value) {
        document.cookie = name + "=" + value;

    },


    init: function (setting) {
        var persistedpage = this.getCookie("fcspersist" + setting.id) || 1;
        var urlselectedpage = this.urlparamselect(setting.id); //returns null or index from: mypage.htm?featuredcontentsliderid=index
        this.settingcaches[setting.id] = setting; //cache "setting" object
        setting.contentdivs = [];
        setting.toclinks = [];
        setting.topzindex = 0;
        setting.currentpage = urlselectedpage || ((this.enablepersist) ? persistedpage : 1);
        setting.prevpage = setting.currentpage;
        setting.revealtype = "on" + (setting.revealtype || "click");
        setting.curopacity = 0;
        setting.onChange = setting.onChange || function () { };
        if (setting.contentsource[0] == "inline")
            this.buildpaginate(setting);
        if (setting.contentsource[0] == "ajax")
            this.ajaxconnect(setting);
    }

};
//ends: contentslider.js
