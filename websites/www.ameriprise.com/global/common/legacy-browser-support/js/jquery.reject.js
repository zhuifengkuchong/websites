/*
 * jReject (jQuery Browser Rejection Plugin)
 * Version 1.0
 * URL: http://jreject.turnwheel.com/
 * Description: jReject is a easy method of rejecting specific browsers on your site
 * Author: Steven Bower (TurnWheel Designs) http://turnwheel.com/
 * Copyright: Copyright (c) 2009-2011 Steven Bower under dual MIT/GPL license.
 */ (function (b) {
    b.reject = function (a) {
        a = b.extend(!0, {
            reject: {
                all: !1,
                msie5: !0,
                msie6: !0
            },
            display: [],
            browserInfo: {
                firefox: {
                    text: "Firefox 8",
                    url: "http://www.mozilla.com/firefox/"
                },
                safari: {
                    text: "Safari 5",
                    url: "http://www.apple.com/safari/download/"
                },
                opera: {
                    text: "Opera 11",
                    url: "http://www.opera.com/download/"
                },
                chrome: {
                    text: "Chrome 15",
                    url: "http://www.google.com/chrome/"
                },
                msie: {
                    text: "Internet Explorer 9",
                    url: "http://www.microsoft.com/windows/Internet-explorer/"
                }
            },
            header: "Did you know that your Internet Browser is out of date?",
            paragraph1: "Your browser is out of date, and may not be compatible with our website. A list of the most popular web browsers can be found below.",
            paragraph2: "Just click on the icons to get to the download page",
            close: !0,
            closeMessage: "By closing this window you acknowledge that your experience on this website may be degraded",
            closeLink: "",
            closeURL: "#",
            closeESC: !0,
            closeCookie: !1,
            cookieSettings: {
                path: "/",
                expires: 0
            },
            imagePath: "/global/common/legacy-browser-support/images/",
            overlayBgColor: "#000",
            overlayOpacity: 0.5,
            fadeInTime: "fast",
            fadeOutTime: "fast",
            analytics: !1
        }, a);
        if (1 > a.display.length) a.display = "firefox,chrome,msie,safari,opera,gcf".split(",");
        b.isFunction(a.beforeReject) && a.beforeReject(a);
        if (!a.close) a.closeESC = !1;
        var f = function (a) {
                return (a.all ? !0 : !1) || (a[b.os.name] ? !0 : !1) || (a[b.layout.name] ? !0 : !1) || (a[b.browser.name] ? !0 : !1) || (a[b.browser.className] ? !0 : !1)
            };
        if (!f(a.reject)) {
            if (b.isFunction(a.onFail)) a.onFail(a);
            return !1
        }
        if (a.close && a.closeCookie) {
            var e = "jreject-close",
                c = function (c, e) {
                    if ("undefined" != typeof e) {
                        var d = "";
                        0 != a.cookieSettings.expires && (d = new Date, d.setTime(d.getTime() + a.cookieSettings.expires), d = "; expires=" + d.toGMTString());
                        var f = a.cookieSettings.path || "/";
                        document.cookie = c + "=" + encodeURIComponent(null == e ? "" : e) + d + "; path=" + f
                    } else {
                        f = null;
                        if (document.cookie && "" != document.cookie) for (var g = document.cookie.split(";"), h = g.length, i = 0; i < h; ++i) if (d = b.trim(g[i]), d.substring(0, c.length + 1) == c + "=") {
                            f = decodeURIComponent(d.substring(c.length + 1));
                            break
                        }
                        return f
                    }
                };
            if (null != c(e)) return !1
        }
        var d = '<div id="jr_overlay"></div><div id="jr_wrap"><div id="jr_inner"><h1 id="jr_header">' + a.header + "</h1>" + ("" === a.paragraph1 ? "" : "<p>" + a.paragraph1 + "</p>") + ("" === a.paragraph2 ? "" : "<p>" + a.paragraph2 + "</p>") + "<ul>",
            i = 0,
            l;
        for (l in a.display) {
            var m = a.display[l],
                j = a.browserInfo[m] || !1;
            if (j && (void 0 == j.allow || f(j.allow))) d += '<li id="jr_' + m + '"><div class="jr_icon"></div><div><a href="' + (j.url || "#") + '">' + (j.text || "Unknown") + "</a></div></li>", ++i
        } // Remind Me Button
        var d = d + ('</ul><div id="jr_close">' + (a.close ? '<a href="' + a.closeURL + '">' + a.closeLink + "</a><a class='remind_btn'>" + a.closeMessage + "</a>" : "") + "</div></div></div>"),
            g = b("<div>" + d + "</div>"),
            f = h(),
            d = k();
        g.bind("closejr", function () {
            if (!a.close) return !1;
            b.isFunction(a.beforeClose) && a.beforeClose(a);
            b(this).unbind("closejr");
            b("#jr_overlay,#jr_wrap").fadeOut(a.fadeOutTime, function () {
                b(this).remove();
                b.isFunction(a.afterClose) && a.afterClose(a)
            });
            b("embed, object, select, applet").show();
            a.closeCookie && c(e, "true");
            return !0
        });
        var n = function (b) {
                if (a.analytics) {
                    var c = b.split(/\/+/g)[1];
                    try {
                        _gaq.push(["_trackEvent", "External Links", c, b])
                    } catch (d) {
                        try {
                            pageTracker._trackEvent("External Links", c, b)
                        } catch (e) {}
                    }
                }
                window.open(b, "jr_" + Math.round(11 * Math.random()));
                return !1
            };
        g.find("#jr_overlay").css({
            width: f[0],
            height: f[1],
            background: a.overlayBgColor,
            opacity: a.overlayOpacity
        });
        g.find("#jr_wrap").css({
            top: d[1] + f[3] / 4,
            left: d[0]
        });
        g.find("#jr_inner").css({
            minWidth: 140 * i,
            maxWidth: 160 * i,
            width: "trident" == b.layout.name ? 155 * i : "auto"
        });
        
        g.find("#jr_inner li .jr_icon").each(function () {
            var c = b(this);
            c.css("background", "transparent url(" + a.imagePath + "browser_" + c.parent("li").attr("id").replace(/jr_/, "") + ".gif) no-repeat scroll left top");
            c.click(function () {
                var a = b(this).next("div").children("a").attr("href");
                n(a)
            })
        });
        g.find("#jr_inner li a").click(function () {
            n(b(this).attr("href"));
            return !1
        });
        g.find("#jr_close a").click(function () {
            b(this).trigger("closejr");
            if ("#" === a.closeURL) return !1
        });
        b("#jr_overlay").focus();
        b("embed, object, select, applet").hide();
        b("body").append(g.hide().fadeIn(a.fadeInTime));
        b(window).bind("resize scroll", function () {
            var a = h();
            b("#jr_overlay").css({
                width: a[0],
                height: a[1]
            });
            var c = k();
            b("#jr_wrap").css({
                top: c[1] + a[3] / 4,
                left: c[0]
            })
        });
        a.closeESC && b(document).bind("keydown", function (a) {
            27 == a.keyCode && g.trigger("closejr")
        });
        b.isFunction(a.afterReject) && a.afterReject(a);
        return !0
    };
    var h = function () {
            var a = window.innerWidth && window.scrollMaxX ? window.innerWidth + window.scrollMaxX : document.body.scrollWidth > document.body.offsetWidth ? document.body.scrollWidth : document.body.offsetWidth,
                b = window.innerHeight && window.scrollMaxY ? window.innerHeight + window.scrollMaxY : document.body.scrollHeight > document.body.offsetHeight ? document.body.scrollHeight : document.body.offsetHeight,
                e = window.innerWidth ? window.innerWidth : document.documentElement && document.documentElement.clientWidth ? document.documentElement.clientWidth : document.body.clientWidth,
                c = window.innerHeight ? window.innerHeight : document.documentElement && document.documentElement.clientHeight ? document.documentElement.clientHeight : document.body.clientHeight;
            return [a < e ? a : e, b < c ? c : b, e, c]
        },
        k = function () {
            return [window.pageXOffset ? window.pageXOffset : document.documentElement && document.documentElement.scrollTop ? document.documentElement.scrollLeft : document.body.scrollLeft, window.pageYOffset ? window.pageYOffset : document.documentElement && document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop]
        }
})(jQuery);
(function (b) {
    b.browserTest = function (h, k) {
        var a = function (a, b) {
                for (var d = 0; d < b.length; d += 1) a = a.replace(b[d][0], b[d][1]);
                return a
            },
            f = function (e, c, d, f) {
                c = {
                    name: a((c.exec(e) || ["unknown", "unknown"])[1], d)
                };
                c[c.name] = !0;
                c.version = c.opera ? window.opera.version() : (f.exec(e) || ["X", "X", "X", "X"])[3];
                if (/safari/.test(c.name) && 400 < c.version) c.version = "2.0";
                else if ("presto" === c.name) c.version = 9.27 < b.browser.version ? "futhark" : "linear_b";
                c.versionNumber = parseFloat(c.version, 10) || 0;
                e = 1;
                100 > c.versionNumber && 9 < c.versionNumber && (e = 2);
                c.versionX = "X" !== c.version ? c.version.substr(0, e) : "X";
                c.className = c.name + c.versionX;
                return c
            },
            h = (/Opera|Navigator|Minefield|KHTML|Chrome/.test(h) ? a(h, [
                [/(Firefox|MSIE|KHTML,\slike\sGecko|Konqueror)/, ""],
                ["Chrome Safari", "Chrome"],
                ["KHTML", "Konqueror"],
                ["Minefield", "Firefox"],
                ["Navigator", "Netscape"]
            ]) : h).toLowerCase();
        b.browser = b.extend(!k ? b.browser : {}, f(h, /(camino|chrome|firefox|netscape|konqueror|lynx|msie|opera|safari)/, [], /(camino|chrome|firefox|netscape|netscape6|opera|version|konqueror|lynx|msie|safari)(\/|\s)([a-z0-9\.\+]*?)(\;|dev|rel|\s|$)/));
        b.layout = f(h, /(gecko|konqueror|msie|opera|webkit)/, [
            ["konqueror", "khtml"],
            ["msie", "trident"],
            ["opera", "presto"]
        ], /(applewebkit|rv|konqueror|msie)(\:|\/|\s)([a-z0-9\.]*?)(\;|\)|\s)/);
        b.os = {
            name: (/(win|mac|linux|sunos|solaris|iphone)/.exec(navigator.platform.toLowerCase()) || ["unknown"])[0].replace("sunos", "solaris")
        };
        k || b("html").addClass([b.os.name, b.browser.name, b.browser.className, b.layout.name, b.layout.className].join(" "))
    };
    b.browserTest(navigator.userAgent)
})(jQuery);