
/*
@requires: jQuery > 1.5.1, jQuery ui.tabs.pack.js
@example:
<script id="dynamic-advisor-module" src="https://www.ameriprise.com/global/widgets/advisor-widget.asp?type=1"></script>
*/
/*
your-advisor.js
@author: Lance Vo
@requires: jQuery
@description: get advisor id from cookie and retrieve info from advisor web service, 
	and display A/B content based on the result from web service. All info is updated automatically by defining name and 
	type in the element data attribute 
	 
@example: 
	see /global/lvo/myadvisor/ for example
	
	advisor.init("#myAdvisor", "#noAdvisor", "#advisorWrapper");
	
	#myAdvisor: this element(container) will be displayed when the advisor id is valid and result return from web service
	#noAdvisor: this element(container) will be displayed when the advisor id is INvalid, or any error from web service
	#advisorWrapper: this is the parent element that contains both of the above elements

@how-it-works:
	It reads value from cookie "FACookie1",and retrieve the advisor id.
		- If no advisor id, it displays the parent container & noAdvisor container
		- If advisor id is found, it retrieves info from web service:
			if it's failed to retrieve the result or error code is returned from webservice, it renders parent container & noAdvisor container
			if valid info is returned from webservice, it updates the html in #myAdvisor container by looking for "data" attribute

@debug:			
	it is disabled by default. To enable it set  advisor.debug= true;
	The logs print out in browser console and #debug in HTML

*/
var advisor = advisor || {};
(function (ns) {
	ns.cookie = (typeof $.cookie != 'undefined') ? $.cookie("FACookie1") : null;
	ns.advisorId = '';
	ns.basePath = '';
	ns.callBack = null;
	ns.debug = location.search.match(/debug=true/gi);
	ns.parentEl = null;
	ns.advisorEl = null;
	ns.noAdvisorEl = null;
	ns.tracking = '';
	ns.viewModel = {
		error: {},
		displayname: '',
		address : '', // address1 + addres 2		
		city: '',
		state: '',
		zip:'',
		phonenumber:'',
		email: '',
		siteUrl:'',
		profilethumbnail: '',
		profilesmallimage: '',
		displayname: '',
		professionaldesignations:'',
		proffessionaltitle:'', // typo from API 
		referMeUrl:''				
	};

			
	ns.log= function(m) {				
		if (!ns.debug) 	return;		
		if ($("#debug").length) $("#debug").append(m).append("<br>");
		if (console) {
			console.log(m);
		}
	};
	

	ns.renderNoAdvisor = function() {
		if (ns.parentEl) ns.parentEl.show();	
		if (ns.noAdvisorEl) ns.noAdvisorEl.show(); 
		if (ns.callback != null)
			ns.callBack();
	}
	
	ns.render = function(o) {
		ns.log('RENDER:');
		ns.advisorEl.find('[data]').each(ns.updateData);
		if (ns.parentEl) ns.parentEl.show();	
		ns.advisorEl.show();
		
	}
	
	ns.updateData = function(el) {
		if ($(this).attr('data') == '') return;
		var t= $(this);
		
		// update element values based on defined data attribute 
		$($(this).attr('data').split(';')).each(function(i, kv) {
			kv = kv.split(':');
			
			switch( kv[0]) {
				case 'url' : t.attr('href',ns.viewModel[kv[1]]);
						break;
				case 'html': t.html(ns.viewModel[kv[1]]); 
						break;
				case 'text': t.text(ns.viewModel[kv[1]]); 
						break;
				case 'img': t.attr('src',ns.viewModel[kv[1]]);
						break;
				case 'mailto': t.attr('href', 'mailto:' + ns.viewModel[kv[1]]);
					break;
				case 'htmlphone' : t.html(ns.viewModel[kv[1]].substring(0,3) + '.' + ns.viewModel[kv[1]].substring(3,6) + '.' + ns.viewModel[kv[1]].substring(6,10));
					break;
				default: throw('updateData: Unknown data type ' + kv[0]); 	
			} 
		});
		
		ns.log($(this).attr('data'));
	
	}
	
	ns.api = function(advisorId) {
		advisorId = advisorId || ns.advisorId;
		
		if (!advisorId) {
			ns.log('advisor api: no advisor id')
			return null;
		}
		
		var api = ns.basePath + '/global/common/advisor-api.asp?advisorId='; 
		
		$.ajax({
			url: api + advisorId,
			dataType: 'jsonp',
			jsonpCallback: 'Advisor',
			success: function(o) {
				ns.log("advisor: ajax call made successful");
				// there's an error 
				if (o.error.code) {
					ns.log('error code ' + o.error.code);
					ns.renderNoAdvisor();
				} else {
					$.extend(ns.viewModel, o.result);
					$.extend(ns.viewModel.error, o.error);
					ns.viewModel.address = ns.viewModel.address1 + ((ns.viewModel.address2)? "<br/>" + ns.viewModel.address2 : "");
					ns.viewModel.referMeUrl += ns.tracking;
					ns.viewModel.siteUrl += ns.tracking;
					ns.viewModel.profilethumbnail = ns.viewModel.thumbImage;
					ns.viewModel.profilesmallimage = ns.viewModel.profilesmallimage;
					ns.log("ViewModel:");
					ns.log(ns.viewModel);
					ns.render(ns.viewModel);
				}
			},
			error: function(xhr, status, errorThrown) {
				ns.log("advisor API Error: " + status);
				ns.renderNoAdvisor(); 	
				throw "advisor: Error with advisor API - " + status; 
			}
		});	
	}	
	
	ns.init = function(advisorEl, noAdvisorEl, parentEl, advisorId, basePath, callBack) {

		if (parentEl) ns.parentEl = $(parentEl);
		if (advisorEl) ns.advisorEl = $(advisorEl);
		if (noAdvisorEl) ns.noAdvisorEl = $(noAdvisorEl);
		if (advisorId) ns.advisorId = advisorId;
		if (basePath) ns.basePath = basePath;
		if (callBack) ns.callBack = callBack;
		
		if(ns.advisorEl) ns.advisorEl.hide();
		if (ns.noAdvisorEl) ns.noAdvisorEl.hide();
				
		if (!ns.cookie && !ns.advisorId) {
			ns.log('advisor: no cookie');
			ns.renderNoAdvisor();
			return false;
		} else {
			if (!ns.advisorId)
				ns.advisorId = ns.cookie.split(",")[1].replace('"','');
			
			ns.log('advisor: cookie id ' + ns.advisorId); 
			ns.api(ns.advisorId);			
		}
	}
})(advisor);

var dynamicAdvisorWidget = {
	trackingCallback: null,
	extLocator: '',
	advisorId: null,
	basePath: '',
	pageType: 1,
	init: function(params) {
		dynamicAdvisorWidget.trackingCallback = params.trackingCallback;
		dynamicAdvisorWidget.extLocator = params.extLocator;
		dynamicAdvisorWidget.basePath = params.basePath;
		dynamicAdvisorWidget.pageType = params.pageType;
		dynamicAdvisorWidget.crCampaign = params.crCampaign;
		
		if (dynamicAdvisorWidget.extLocator == '')
			dynamicAdvisorWidget.extLocator = 'http://www.ameripriseadvisors.com/';
    
		var css = 'body div.alwpWidget-container div,body div.alwpWidget-container p,body div.alwpWidget-container a,body div.alwpWidget-container h1,body div.alwpWidget-container h2,body div.alwpWidget-container h3,body div.alwpWidget-container input,body div.alwpWidget-container img{border:none;font-family:Georgia;font-size:100%;font-weight:400;font-style:normal;text-decoration:none;outline:none;text-align:left;margin:0;padding:0;} body div.alwpWidget-container p{font-size:11px;line-height:16px;color:#333;margin-top:6px;} body div.alwpWidget-container,body div.alwpWidget-footer,body input.alwpWidget-textbox,body a.alwpWidget-searchbtn,body div.alwpWidget-dotline, body div.alwpWidget-speak, body a.alwpWidget-email, body a.alwpWidget-zip, body div.or-break{background-image:url("../../../www.ameripriseadvisors.com/widgets/assets/image/widget-search-thin.png"/*tpa=https://www.ameripriseadvisors.com/widgets/assets/image/widget-search-thin.png*/);} body div.alwpWidget-container .alwpWidget-clear{clear:left;line-height:1px;font-size:1px;} body div.alwpWidget-container .alwpWidget-dotline{line-height:1px;font-size:1px;height:2px;background-position:-260px -189px;margin:12px 0;} body div.alwpWidget-container{width:230px;position:relative;overflow:hidden;} body div.alwpWidget-container .alwpWidget-textbox,body div.alwpWidget-container .alwpWidget-searchbtn{border:none;height:15px;display:block;float:left;} body div.alwpWidget-container a.alwpWidget-searchbtn,body div.alwpWidget-container a.alwpWidget-searchbtn:link,body div.alwpWidget-container a.alwpWidget-searchbtn:visited,body div.alwpWidget-container a.alwpWidget-searchbtn:hover,body div.alwpWidget-container a.alwpWidget-searchbtn:active{background-position:-300px -155px;color:#333;font-size:12px;text-align:center;text-decoration:none;width:63px;margin-left:10px;font-family:Georgia;padding:6px 5px 5px 0;} body div.alwpWidget-container .alwpWidget-textbox{background-position:-230px -155px;background-color:Transparent;font-size:12px;color:#333;width:44px;padding:5px 10px;} body div.alwpWidget-container div.alwpWidget-searchForm{margin-top:5px;} body div.alwpWidget-container div.alwpWidget-header,body div.alwpWidget-container div.alwpWidget-results{padding:0 23px;} body div.alwpWidget-container div.alwpWidget-results{padding-bottom:175px;} body div.alwpWidget-container div.alwpWidget-header{padding-top:15px;} body div.alwpWidget-container div.alwpWidget-header h1{color:#333;font-size:22px;} body div.alwpWidget-container div.alwpWidget-result div.alwpWidget-photo{float:left;width:60px;} body div.alwpWidget-container div.alwpWidget-result div.alwpWidget-detail{float:left;width:120px;} body div.alwpWidget-container div.alwpWidget-noimage div.alwpWidget-detail{width:180px;} body div.alwpWidget-container div.alwpWidget-header .alwpWidget-noResults{margin-bottom:20px;} body div.alwpWidget-container a,body div.alwpWidget-container a:link,body div.alwpWidget-container a:visited,body div.alwpWidget-container a:hover,body div.alwpWidget-container a:active{color:#3E72AB;font-size:12px;text-decoration:none;font-weight:400;line-height:1.1em;} body div.alwpWidget-container div.alwpWidget-results h2 a,body div.alwpWidget-container div.alwpWidget-results h2 a:link,body div.alwpWidget-container div.alwpWidget-results h2 a:visited,body div.alwpWidget-container div.alwpWidget-results h2 a:hover,body div.alwpWidget-container div.alwpWidget-results h2 a:active{font-size:16px;} body div.alwpWidget-container div.alwpWidget-result p.alwpWidget-address,body div.alwpWidget-container div.alwpWidget-header p{font-family:Arial,Helvetica,Sans-Serif;font-size:11px;}  body div.alwpWidget-container div.alwpWidget-footer{ background-position: -230px 120px; line-height: 0; height: 25px; position: absolute; bottom: 0; left: 0; width: 230px; padding-bottom: 2px; } body div.alwpWidget-container div.alwpWidget-footer-text { background-image: url("../../../cdn.ameriprisecontent.com/cds/alwp/widgets/widget-search-2.jpg"/*tpa=https://cdn.ameriprisecontent.com/cds/alwp/widgets/widget-search-2.jpg*/); height: 105px; position: absolute; bottom: 52px; left: 3px; width: 225px; } body div.alwpWidget-container div.alwpWidget-footer-image { position: absolute; bottom: 20px; left: 0; padding-left: 65px; height: 30px; } body a.alwpWidget-footer-image { background-image: url("../../../cdn.ameriprisecontent.com/cds/alwp/widgets/get-started.png"/*tpa=https://cdn.ameriprisecontent.com/cds/alwp/widgets/get-started.png*/); background-repeat: no-repeat; cursor: pointer; display: block; width: 87px; height: 24px; } body div.alwpWidget-noResultsContainer div.alwpWidget-footer-text{ display:none; } body div.alwpWidget-noResultsContainer div.alwpWidget-footer-image{ display:none; }  body div.alwpWidget-hide,body div.dynamic-advisor-widget .ui-tabs-hide{display:none;} body div.alwpWidget-noResultsContainer .alwpWidget-searchForm,body div.alwpWidget-noResultsContainer .alwpWidget-results{display:none;} body div.alwpWidget-noResultsContainer .alwpWidget-speak{background-position:-245px -376px;background-repeat:no-repeat;padding-left:40px;} body div.alwpWidget-noResultsContainer .alwpWidget-speak p{font-size:16px!important;font-family:Georgia, serif!important;} body div.alwpWidget-noResultsContainer .alwpWidget-speak span{color:#999;font-size:13px;} body div.alwpWidget-noResultsContainer .alwpWidget-speak h3 { background: none!important;} body div.alwpWidget-noResultsContainer .or-break{background-position:-245px -345px;height:20px;margin:5px 0 0;} body div.alwpWidget-noResultsContainer .alwpWidget-email{background-position:-245px -286px;cursor:pointer;display:block;padding-left:40px;font-size:16px!important;padding-top:0!important;margin:5px 0;} body div.alwpWidget-noResultsContainer .alwpWidget-zip{background-position:-245px -213px;cursor:pointer;display:block;padding-left:40px;font-size:16px!important;padding-top:0!important;margin:5px 0;} body div.alwpWidget-noResultsContainer div.alwpWidget-footer{height:15px;background-position:-230px -1892px!important;}  body div.dynamic-advisor-widget div,body div.dynamic-advisor-widget p,body div.dynamic-advisor-widget a,body div.dynamic-advisor-widget h1,body div.dynamic-advisor-widget h2,body div.dynamic-advisor-widget h3,body div.dynamic-advisor-widget h4,body div.dynamic-advisor-widget h5,body div.dynamic-advisor-widget h6,body div.dynamic-advisor-widget input,body div.dynamic-advisor-widget img,body div.dynamic-advisor-widget form{border:none;text-decoration:none;outline:none;text-align:left;margin:0;padding:0;} body div.dynamic-advisor-widget ul{list-style-type:none;margin:0;padding:0;} body div.dynamic-advisor-widget{font:0.9em Arial,Helvetica,sans-serif;color:#333;} body div.dynamic-advisor-widget *{font-size:100%;} body div.dynamic-advisor-widget a,body div.dynamic-advisor-widget a:visited{color:#0D5FA0;text-decoration:none;} body div.dynamic-advisor-widget h2{font-size:1.6em;font-weight:400;} body div.dynamic-advisor-widget h3{font-size:2.1em;font-weight:400;} body div.dynamic-advisor-widget h4{font-size:1.5em;font-weight:400;} body div.dynamic-advisor-widget h5{font-size:1.8em;font-weight:400;} body div.dynamic-advisor-widget h6{font-size:1.4em;font-weight:400;} body div.dynamic-advisor-widget sup{font-family:Verdana,Geneva,sans-serif;font-size:0.7em;vertical-align:top;}  body div.dynamic-advisor-widget #meet-advisor-mod{color:#FFF;float:left;font:0.8em/1em Arial,Helvetica,sans-serif;margin-bottom:6px;width:228px;} body div.dynamic-advisor-widget #meet-advisor-mod-content{background:url("https://www.ameriprise.com/global/images/sidebar-mods/meet-advisor-cont-bg.png") repeat-y scroll 0 0 transparent;float:left;width:188px;padding:9px 20px 12px;} body div.dynamic-advisor-widget #meet-advisor-mod a{color:#76BDDB;} body div.dynamic-advisor-widget #meet-advisor-mod #tabs{background:url("https://www.ameriprise.com/global/images/sidebar-mods/meet-advisor-bg-top.png") no-repeat scroll center bottom transparent;float:left;overflow:hidden;padding-bottom:3px;width:228px;margin:0;} body div.dynamic-advisor-widget #meet-advisor-mod #tabs li{float:left;margin-right:5px;} body div.dynamic-advisor-widget #meet-advisor-mod #tabs li a.by-phone{background:url("https://www.ameriprise.com/global/images/sidebar-mods/adv-mod-phone-off.png") no-repeat scroll 0 0 transparent;display:block;height:32px;width:90px;} body div.dynamic-advisor-widget #meet-advisor-mod #tabs li.ui-tabs-selected a.by-phone, body div.dynamic-advisor-widget #meet-advisor-mod #tabs li.ui-tabs-active a.by-phone{background:url("https://www.ameriprise.com/global/images/sidebar-mods/adv-mod-phone-on.png") no-repeat scroll 0 0 transparent;} body div.dynamic-advisor-widget #meet-advisor-mod #tabs li a.in-person{background:url("https://www.ameriprise.com/global/images/sidebar-mods/adv-mod-person-off.png") no-repeat scroll 0 0 transparent;display:block;height:32px;width:95px;} body div.dynamic-advisor-widget #meet-advisor-mod #tabs li.ui-tabs-selected a.in-person, body div.dynamic-advisor-widget #meet-advisor-mod #tabs li.ui-tabs-active a.in-person{background:url("https://www.ameriprise.com/global/images/sidebar-mods/adv-mod-person-on.png") no-repeat scroll 0 0 transparent;} body div.dynamic-advisor-widget #meet-advisor-mod .form-container{float:left;width:100%;margin:4px 0;padding:0;} body div.dynamic-advisor-widget #meet-advisor-mod .form-container a.more{display:block;float:left;margin-top:2px;width:40px;} body div.dynamic-advisor-widget #meet-advisor-mod .search-btn{float:left;margin:0 10px 0 6px;} body div.dynamic-advisor-widget #meet-advisor-mod input#zipcode{background:url("https://www.ameriprise.com/global/images/zip.png") no-repeat scroll 0 0 transparent;border:medium none;color:#666;float:left;font-family:Georgia,"Times New Roman",Times,serif;font-size:13px;height:25px;margin-top:1px;width:56px;line-height:1.9;padding:0 0 0 7px;} body div.dynamic-advisor-widget #meet-advisor-mod .box{float:left;font-size:0.9em;margin-top:5px;width:92px;} body div.dynamic-advisor-widget #meet-advisor-mod .dotted{background:url("https://www.ameriprise.com/global/images/horiz-dot.png") repeat-x scroll 0 bottom transparent;float:left;height:2px;width:188px;margin:4px 0 3px;padding:0;} body div.dynamic-advisor-widget #meet-advisor-mod .box ul.social{float:left;padding-left:10px;} body div.dynamic-advisor-widget #meet-advisor-mod .box ul li{float:left;margin:0 3px;padding:0;} body div.dynamic-advisor-widget #meet-advisor-mod .phone-number{font:1.5em/1em Georgia,"Times New Roman",Times,serif;} body div.dynamic-advisor-widget #meet-advisor-mod h5{color:#FFF;font:1.8em/1.2em Georgia,"Times New Roman",Times,serif;} body div.dynamic-advisor-widget #meet-advisor-mod-content p,body div.dynamic-advisor-widget .sidebar-content p{margin:4px 0;} body div.dynamic-advisor-widget #meet-advisor-mod #tabs ul,body div.dynamic-advisor-widget .right-block .content img{margin:0;}  body div.dynamic-advisor-widget .sidebar-grd-container{background:url("https://www.ameriprise.com/global/images/sidebar-mods/sidebar-bg-grd-full.png") no-repeat scroll 0 top transparent;float:left;margin-bottom:6px;min-height:130px;} body div.dynamic-advisor-widget .sidebar-content{background:url("https://www.ameriprise.com/global/images/sidebar-mods/sidebar-bg-btm-grd.png") no-repeat scroll 0 bottom transparent;float:left;font-size:0.9em;min-height:70px;width:188px;padding:13px 20px 30px;} body div.dynamic-advisor-widget .sidebar-content .left-content{float:left;margin-top:10px;width:70px;} body div.dynamic-advisor-widget .sidebar-content .right-content{float:left;font-size:1em;margin-top:10px;width:118px;} body div.dynamic-advisor-widget .sidebar-content .refer{margin-top:10px;} body div.dynamic-advisor-widget .sidebar-content .right-content h6{font-size:1.2em;margin-top:0;} body div.dynamic-advisor-widget .sidebar-content a img{margin:10px 0 5px;} body div.dynamic-advisor-widget .sidebar-content h5{color:#333;font-size:24px;font-weight:400;line-height:28px;} body div.dynamic-advisor-widget .sidebar-content h2,h3,h4,h5,h6{font-family:Georgia,"Times New Roman",Times,serif;} body div.dynamic-advisor-widget .dotted{background:url("https://www.ameriprise.com/global/images/horiz-dot.png") repeat-x scroll 0 bottom transparent;margin-bottom:10px;padding-bottom:8px;}  body div.dynamic-advisor-widget .right-block{background:url("https://www.ameriprise.com/global/images/sidebar-mods/sidebar-bg-grd-full.png") no-repeat scroll 0 top transparent;width:228px;font-size:0.9em;margin-bottom:6px;font-family:Georgia,"Times New Roman",Times,serif;} body div.dynamic-advisor-widget .right-block img{margin:0 0 0 4px;} body div.dynamic-advisor-widget .right-block .content{background:url("https://www.ameriprise.com/global/images/sidebar-mods/sidebar-bg-btm-grd.png") no-repeat scroll 0 bottom transparent;min-height:70px;width:188px;margin:0;padding:0 20px 20px;} body div.dynamic-advisor-widget .right-block .content ul li{background:url("https://www.ameriprise.com/global/images/bullet_gray.png") no-repeat scroll 0 4px transparent;padding-left:12px;margin:10px 0;} body div.dynamic-advisor-widget .right-block .content br{display:block;margin:5px 0;} body div.dynamic-advisor-widget .right-block .content .dotted{margin:0 0 10px;padding:0 0 10px;} body div.dynamic-advisor-widget .right-block .content p.dotted{padding-bottom:20px;margin:10px 0;} body div.dynamic-advisor-widget .right-block .content p{margin:10px 0;} body div.dynamic-advisor-widget .right-block h5{font-size:1.8em;font-weight:400;line-height:1.2em;width:188px;margin:0;padding:13px 20px 0;} body div.dynamic-advisor-widget .content.border{background:url("https://www.ameriprise.com/global/images/sidebar-mods/sidebar-bg-btm-grd.png") no-repeat scroll 0 bottom transparent;} body div.dynamic-advisor-widget .right-block form label{font-size:11px;font-weight:700;} body div.dynamic-advisor-widget .right-block form .text{font:12px Arial,Helvetica,sans-serif;width:94px;padding:0;} body div.dynamic-advisor-widget .right-block form select{font:11px Arial,Helvetica,sans-serif;width:139px;} body div.dynamic-advisor-widget .right-block form .go-btn{vertical-align:top;} body div.dynamic-advisor-widget .right-block h6{color:#666;font-family:Georgia,"Times New Roman",Times,serif;font-size:1.2em !important;font-style:italic;margin:10px 0 5px !important;}  body div.dynamic-advisor-widget .cr-advisor-widget-v2  {background:#d7d6da; background:-webkit-linear-gradient(top,#d7d6da,#f1f1f3); background:-moz-linear-gradient(top,#d7d6da,#f1f1f3); background:-ms-linear-gradient(top,#d7d6da,#f1f1f3); background:-o-linear-gradient(top,#d7d6da,#f1f1f3); height:185px; padding:15px; border-radius:5px; box-shadow:3px 3px 5px #ccc; } body div.dynamic-advisor-widget .cr-advisor-widget-v2 .advisor-widget-cta  {float:left; width:45%;} body div.dynamic-advisor-widget .cr-advisor-widget-v2 .advisor-widget-cta h1  {font-size:24px;} body div.dynamic-advisor-widget .cr-advisor-widget-v2 .advisor-widget-cta p  {font-family:Times, Helvetica, sans-serif; font-size:17px;} body div.dynamic-advisor-widget .cr-advisor-widget-v2 .advisor-widget-cta p sup  {position:relative; top:0px; font-size:8px;} body div.dynamic-advisor-widget .cr-advisor-widget-v2 .advisor-widget-data  {float:right; width:55%;} body div.dynamic-advisor-widget .cr-advisor-widget-v2 .advisor-widget-data .widget-left-content  {float:left; width:50%;} body div.dynamic-advisor-widget .cr-advisor-widget-v2 .advisor-widget-data .widget-right-content  {float:right; width:45%; background:none !important; border-radius:none !important; font-style:none;} '
		if ($("#dynamic-advisor-widget-css").length == 0) {
			$('head').append('<style id="dynamic-advisor-widget-css">' + css + '</style>');
		}
		
		$.ajax({
			url: dynamicAdvisorWidget.basePath + '/global/common/get-current-auth.asp',
			dataType: 'jsonp',
			jsonpCallback: 'ID',
			success: function(o) {	
				if (o.advisorId != '' && o.advisorId != '00000000') {
        	        if (dynamicAdvisorWidget.pageType != 4) {
					    dynamicAdvisorWidget.advisorId = o.advisorId;
						dynamicAdvisorWidget.loadMyAdvisor();
					}
				} else {
					dynamicAdvisorWidget.loadNoCookieView();
					//dynamicAdvisorWidget.advisorId = '00072191';
                    //dynamicAdvisorWidget.advisorId = '0480240';
					//dynamicAdvisorWidget.loadMyAdvisor();
				}
			},
			error: function(xhr, status, errorThrown) {	
				console.log('HTTP Status: ' + status);
				dynamicAdvisorWidget.loadNoCookieView();
			}
		});	
	},
	wrapHtml: function(html) {
		if (html != null) {
			html = '<div class="dynamic-advisor-widget">' + html + '</div>';
		}
		return html;
	},
	appendHtml: function(htmlObj) {
		if (htmlObj != null) {
			$('#dynamic-advisor-module').after(htmlObj);
		}
	},
	loadNoCookieView: function() {
		var pageType = dynamicAdvisorWidget.pageType; 

		if (pageType == 1)  {
			var htmlObj = dynamicAdvisorWidget.loadMeetAdvisor();
			dynamicAdvisorWidget.appendHtml(htmlObj);
			$('#meet-advisor-mod').tabs();
		} else if (pageType == 2) {
			var htmlObj = dynamicAdvisorWidget.loadTargetedAdvisorWidget();
			dynamicAdvisorWidget.appendHtml(htmlObj);
		} else if (pageType == 3) {
			var htmlObj = dynamicAdvisorWidget.loadWhatDoesThisMeanBlock();
			dynamicAdvisorWidget.appendHtml(htmlObj);
		} else if (pageType == 4) {
			var htmlObj = dynamicAdvisorWidget.loadHowCouldYouBenefitBlock();
			dynamicAdvisorWidget.appendHtml(htmlObj);
		} else if (pageType == 5)  {
			var htmlObj = dynamicAdvisorWidget.loadCrCampaignWidget();
			dynamicAdvisorWidget.appendHtml(htmlObj);
		}
	},
	track: function(str) {
		if (dynamicAdvisorWidget.trackingCallback != null && dynamicAdvisorWidget.trackingCallback != '') {
			dynamicAdvisorWidget.executeFunctionByName(dynamicAdvisorWidget.trackingCallback);
		}
	},
	loadMyAdvisor: function() {
		switch(dynamicAdvisorWidget.pageType)  {
		case "5":
			var html = '<div class="cr-advisor-widget-v2">\
				<div class="advisor-widget-cta">\
					<h1>Contact your advisor today</h1>\
					<p>See how the <em>Confident Retirement</em><sup>&#174;</sup> approach can help bring your retirement goals more within reach.</p>\
				</div>\
				<div id="myAdvisor" class="advisor-widget-data">\
					<div class="widget-left-content "> <a data="url:siteUrl"><img data="img:profilesmallimage" /></a></div>\
				<div class="widget-right-content">\
					<h6><a href="#" data="url:siteUrl;html:displayname"></a></h6>\
					<p data="html:professionaldesignations"></p>\
					<p data="html:proffessionaltitle"></p>\
					<p data="htmlphone:phonenumber"></p>\
					<div><a data="mailto:email"><span data="html:email"></a></div>\
				</div>\
				<div style="clear:both;"></div>\
				</div><div style="clear:both;"></div>';
			
			html = dynamicAdvisorWidget.wrapHtml(html);
			dynamicAdvisorWidget.appendHtml(html);
		
			try {
				advisor.init("#myAdvisor","","",dynamicAdvisorWidget.advisorId, dynamicAdvisorWidget.basePath, dynamicAdvisorWidget.loadNoCookieView);	
			} catch (error) {
				dynamicAdvisorWidget.loadNoCookieView();
			}
		break;
		default:
			var html = '<div id="myAdvisor" class="sidebar-grd-container">\
			<div class="sidebar-content">\
				<h5 class="dotted">Your advisor</h5>\
				<div class="left-content "> <a data="url:siteUrl"><img data="img:profilethumbnail" /></a></div>\
				<div class="right-content">\
					<h6><a href="#" data="url:siteUrl;html:displayname"></a></h6>\
					<p data="html:professionaldesignations"></p>\
					<p data="html:proffessionaltitle"></p>\
					<p data="html:address"></p>\
					<a href="#" data="url:referMeUrl"><img class="refer" src="' + dynamicAdvisorWidget.basePath + '/global/images/sidebar-mods/refer-your-advior-btn.png" width="124" height="20" alt="Refer Your Advisor"></a></div>\
				</div>\
			</div>';
			
			html = dynamicAdvisorWidget.wrapHtml(html);
			dynamicAdvisorWidget.appendHtml(html);
		
			try {
				advisor.init("#myAdvisor","","",dynamicAdvisorWidget.advisorId, dynamicAdvisorWidget.basePath, dynamicAdvisorWidget.loadNoCookieView);	
			} catch (error) {
				dynamicAdvisorWidget.loadNoCookieView();
			}
		}
	},
	
	loadHowCouldYouBenefitBlock: function() {
		var html = '<div class="right-block">\
				<h5>How could this benefit you?</h5>\
				<div class="content">\
				  <p class="dotted">Connect with your Ameriprise financial advisor about using the secure site on ameriprise.com to access your account information, conduct transactions and communicate securely.</p>\
				  <h6>Not working with an advisor?</h6>\
				  <p><a id="advisorWidgetExtLocatorLink" href="javascript:void(); return false;" target="_blank">Find the right advisor</a> for you.</p>\
				</div>\
			</div>';
			
		html = dynamicAdvisorWidget.wrapHtml(html);
		
		var htmlObj = $(html);
		$('#advisorWidgetExtLocatorLink', htmlObj).attr('href',dynamicAdvisorWidget.extLocator);
		
		return htmlObj;
		
	},
	loadWhatDoesThisMeanBlock: function() {
		var html = '<div class="right-block">\
				<h5>What does this mean for you?</h5>\
				<div class="content">\
				  <p class="dotted">Ask your financial advisor about which investment strategies are right for your portfolio.</p>\
				  <h6>Not working with an advisor?</h6>\
				  <p><a id="advisorWidgetExtLocatorLink" href="javascript:void(); return false;" target="_blank">Find the right advisor</a> for you.</p>\
				</div>\
			</div>';
		
		html = dynamicAdvisorWidget.wrapHtml(html);
		
		var htmlObj = $(html);
		$('#advisorWidgetExtLocatorLink', htmlObj).attr('href',dynamicAdvisorWidget.extLocator);
		
		return htmlObj;
		
	},
	loadTargetedAdvisorWidget: function() {
		var html = '<div><div id="alwpSearchWidget-search" class="alwpWidget-container alwpWidget-hide">\
			<div class="alwpWidget-header">\
				<div class="alwpWidget-hasResults">\
					<h1>Advisors near you</h1>\
					<p>Displaying advisors near <span id="alwpSearchWidget-CityState"></span></p>\
				</div>\
				<div class="alwpWidget-noResults alwpWidget-hide">\
					<h1>Take the next step</h1>\
					<div class="alwpWidget-speak">\
						<p>Speak to a financial advisor</p>\
						<h3>800.257.8740</h3>\
						<span>8 am to 5 pm CT <br />Monday - Friday</span>\
					</div>\
					<div class="or-break"><!-- ie --></div>\
					<a class="alwpWidget-email" href="mailto:ameripriseadvisorcenter@ampf.com">\
						Email one of our financial advisors\
					</a>\
					<div class="or-break"><!-- ie --></div>\
					<a class="alwpWidget-zip" href="http://www.ameripriseadvisors.com/" target="_blank">\
						Search for an advisor by zip code\
					</a>\
				</div>\
				<div class="alwpWidget-searchForm">\
					<input type="text" value="" class="alwpWidget-textbox" maxlength="5" />\
					<a target="_blank" class="alwpWidget-searchbtn" href="javascript:void(0)" title="Update">Update</a>\
					<div class="alwpWidget-clear"><!-- ie --></div>\
				</div>\
			</div>\
			<div class="alwpWidget-results">\
		   </div>\
           <div class="alwpWidget-footer-text">&nbsp;</div>\
            <div class="alwpWidget-footer-image">\
                <a class="alwpWidget-footer-image" href="http://www.ameriprise.com/aac-online-interest/default.asp?id=29832" target="_blank">&nbsp;</a>\
            </div>\
		   <div class="alwpWidget-footer">&nbsp;</div>\
	   </div>\
	   <script id="alwpSearchWidget" src="//www.ameripriseadvisors.com/widgets/search/alwpSearchWidget?custom=true&results=3&ipa=198.82.15.240"></script></div>';
  
		var htmlObj = $(html);
		return htmlObj;
	},
	
	loadCrCampaignWidget: function()  {
		var html = '<div class="dynamic-advisor-widget">\
						<div class="cr-advisor-widget-v2 no-advisor">\
								<h1>Get started on your confident retirement</h1>\
								<p>The first step in reaching your goals is reaching someone who can help get you there. <a href="https://www.ameripriseadvisors.com/" target="_blank">Find an Ameriprise financial advisor near you.</a></p>\
						</div>\
					</div>';
		var htmlObj = $(html);
		return htmlObj;
	},

	loadMeetAdvisor: function() {
		var html = '<div id="meet-advisor-mod">\
			<ul id="tabs" class="advisor-mod-tabs">\
				<li><a class="in-person" href="#in-person"></a></li>\
				<li><a class="by-phone" href="#by-phone"></a></li>\
			</ul>\
			<div id="in-person">\
				<div id="meet-advisor-mod-content">\
					<h5>Find an advisor</h5>\
					<p>In your neighborhood</p>\
					<div class="form-container">\
						<form name="wwZipCodeSearch" action="https://www.ameriprise.com/global/widgets/zipwidget.asp">\
							<input id="zipcode" type="text" name="zip" value="ZIP" maxlength="5" class="text"  onblur="if(this.value==\'\')this.value=\'ZIP\'" onfocus="if(this.value==\'ZIP\')this.value=\'\';" />\
							<input type="image" src="' + dynamicAdvisorWidget.basePath + '/global/images/buttons/meet-advisor-mod-search-btn.gif" alt="Search for an advisor by zip code" class="search-btn" />\
							<input type="hidden" name="page" value="results" />\
							<input type="hidden" name="solc_id" value="28073" />\
							<input type="hidden" name="vend_cd" value="ALA" />\
							<input type="hidden" name="offer_id" value="1315" />\
						</form>\
					<a id="advisorWidgetExtLocatorLink" href="javascript:void(); return false;" class="more">More options</a> </div>\
					</div>\
					<div class="ui-round-bottom">\
                	</div>\
			</div>\
			<div id="by-phone">\
				<div id="meet-advisor-mod-content">\
					<h5>Talk to an advisor</h5>\
					<p>For personal attention over the phone</p>\
					<p class="phone-number">800.986.9594</p>\
					<div class="dotted"></div>\
					<div class="box">Does someone you know work with us?</div>\
					<div class="box">\
						<ul class="social">\
							 <li><a name="fb_share" type="button_count" target="_blank" href="https://www.facebook.com/#!/Ameriprise?v=app_161456727235471" onclick="dynamicAdvisorWidget.track(\'socialCTA_facebook\')"><img src="' + dynamicAdvisorWidget.basePath + '/global/images/social-media/fb-ico-small.png" alt="Facebook" /></a></li>\
							<li><a target="_blank" href="http://www.linkedin.com/" onclick="dynamicAdvisorWidget.track(\'socialCTA_linkedin\')" ><img src="' + dynamicAdvisorWidget.basePath + '/global/images/social-media/linkedin-ico-small.png"   alt="LinkedIn" /></a></li>\
							<li><a target="_blank" href="http://www.twitter.com/" onclick="dynamicAdvisorWidget.track(\'socialCTA_twitter\')"><img src="' + dynamicAdvisorWidget.basePath + '/global/images/social-media/twitter-ico-small.png" alt="Twitter" /></a></li>\
						</ul>\
					</div>\
				</div>\
                <div class="ui-round-bottom">\
                </div>\
			</div>\
		</div>';
		
		html = dynamicAdvisorWidget.wrapHtml(html);
		
		var htmlObj = $(html);
		$('#advisorWidgetExtLocatorLink', htmlObj).attr('href',dynamicAdvisorWidget.extLocator);

		return htmlObj;
	},
	executeFunctionByName: function(functionName) {
		var context = window;
		var args = Array.prototype.slice.call(arguments).splice(2);
		var namespaces = functionName.split(".");
		var func = namespaces.pop();
		
		for (var i = 0; i < namespaces.length; i++) {
			context = context[namespaces[i]];
		}

		if (typeof context[func] == 'function') {
			return context[func].apply(this, args);
		}
	},
	
}

var showWidget = true;
if (top != self) {
	var referr = document.referrer;
	if (referr != null && referr.indexOf("https://www.ameriprise.com/global/widgets/www.scl.bz") != -1) {
		showWidget = false;
	}
}

if (showWidget) {
	dynamicAdvisorWidget.init({'pageType':'2','trackingCallback':'trackOmnitureClickThrough','extLocator':'','basePath':'https://www.ameriprise.com','crCampaign':''});
}
