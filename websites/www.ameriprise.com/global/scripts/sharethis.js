/**
 * Parse out garbage #anchor stuff at end of url
 */
function getUrl() {
	//Grab our current Url
	var url = window.location.toString();
	//Remove anchor from url
	var anchor_index = url.indexOf('#');
	if (anchor_index != -1) {
		url = url.substring(0, anchor_index);
	}
	return escape(url);
}

function openReddit() {
	
	trackClick("st_reddit");
	 
	 
	var url = getUrl();
	var title = document.title; 
	
	url = addQuery(url, "cid", "st_reddit");
	window.open("http://reddit.com/submit?title="+title+"&url="+url); 
	
	
}

function openFacebook(e) {
    if (e !== undefined) {
        e.preventDefault();
    }
	trackClick("st_facebook"); 

	var url = getUrl();
	url = addQuery(url, "cid", "st_facebook");

	window.open("http://www.facebook.com/share.php?u="+url)
}

function openStumbleupon() {
	
	trackClick("st_stumbleupon"); 
	
	var url = getUrl();
	var title = document.title; 
	
	url = addQuery(url, "cid", "st_stumbleupon");
	
	window.open("http://www.stumbleupon.com/submit?url="+url+"&title="+title); 
	

}

function openDigg() {


	trackClick("st_digg"); 

	var url = getUrl();
	var title = document.title; 
	
	url = addQuery(url, "cid", "st_digg");
	
	window.open("http://digg.com/submit?phase=2&title="+title+"&url="+url); 
	
	
}

function openDelicious() { 
	
	trackClick("st_delicious"); 
		
	var url = getUrl();
	var title = document.title; 
	 
	url = addQuery(url, "cid", "st_delicious"); 
	
	window.open("http://del.icio.us/post?url="+url+"&title="+title); 
}
function openAmeripriseFacebook(){
    trackClick('logout_facebooklike');
	window.open("http://www.facebook.com/ameriprise"); 

}
function openYahooBuzz() {
 
 	trackClick("st_yahoobuzz"); 
 
	var url = getUrl();
	var title = document.title; 
	
	url = addQuery(url, "cid", "st_yahoobuzz"); 
	 
	window.open("http://buzz.yahoo.com/buzz?publisherurn=ameriprise&targetUrl="+url); 

	
}


function openTwitter(e) {
    if (e !== undefined) {
        e.preventDefault();
    }
	trackClick("st_twitter"); 
 
	var url = getUrl();
	var title = document.title;

	//store in a cookie to pass so html characters dont get injected
	$.cookie('twittertitle', title, { expires: 1, path: '/'});

	url = addQuery(url, "cid", "st_twitter"); 
	 
	window.open("/global/sitelets/sharethis/twitter.asp?url="+url); 
}



function openLinkedIn(e) {
    if (e !== undefined) {
        e.preventDefault();
    }
	trackClick("st_linkedin"); 
 
	var url = getUrl();
	var title = document.title; 
	
	url = addQuery(url, "cid", "st_linkedin"); 
	 
	window.open("http://www.linkedin.com/shareArticle?mini=true&url="+url+"&title="+title); 
	
	 

}



function trackClick(linkName) {
	 var s=s_gi('amppublic,ampglobal'); 
	 s.linkTrackVars='eVar21,events,prop17'; 
	 s.linkTrackEvents='event17'; 
	 s.eVar21=linkName; 
	 s.events='event17'; 
   s.prop17 = s_om.pageName;
	 s.tl(this,'e',linkName);
	 
	 
}

/**
 * Validates and sends email this form
 */ 
 
 
function submitEmailForm(form) {
	
	//alert("email this form: " + form["send-to"].value);
	
	
	var toEmail = form["send-to"].value;
	var fromEmail = form["your-address"].value;
	var firstName = form["first-name"].value;
	var lastName = form["last-name"].value;
	var message = form["message"].value;
	var sendToYourself = form["to-yourself"].checked;
	var title = document.title;
	var urlToSend = urlEncode(changeQueryStringValue(document.location.href, "cid", "amp_email_this_page"));
	
	
	if(sendToYourself) {
		toEmail += ", " + fromEmail;
	}
	
	
	var valid = true;
 
		
	/**  CHECK TO EMAIL **/
	
	var toEmailArray = toEmail.split(",");
	
	//if one email
	if(toEmailArray.length < 1) {
		valid = validateEmail(document.emailtestform.toEmail.value);

	} else {
		//if multiple emails			
		for(var i = 0; i < toEmailArray.length; i++) {
			var email = toEmailArray[i];
			 
			if(email != null && valid) {
				//trim the whitepsace
				valid = validateEmail(email.replace(/^\s+|\s+$/g, ''));
				if(!valid) return false;
			}
		} 
	}
 
	/**  CHECK FROM EMAIL **/
	valid = validateEmail(fromEmail);
	if(!valid) return false;
 
 
 	if(valid) {
	 
		var emailQuery = "toEmail="+htmlEncode(toEmail)+"&fromEmail="+htmlEncode(fromEmail)+"&firstName="+htmlEncode(firstName)+"&lastName="+htmlEncode(lastName)+"&message="+htmlEncode(message)+"&title="+htmlEncode(title)+"&urlToSend="+urlToSend;
		 
		 var s=s_gi('amppublic,ampglobal');
		 s.linkTrackVars='eVar21,events,prop17'; 
		 s.linkTrackEvents='event17'; 
		 s.eVar21='amp_email_this_page_submit'; 
		 s.events='event17'; 
		 s.prop17 = s_om.pageName;
		 s.tl(this,'o','amp_email_this_page_submit');
		 
		  
		var request = new getHTTPObject();

		request.open("POST", "/global/sitelets/email/default.asp?"+emailQuery, true)
		request.send(emailQuery)
			 
		$('#email-this-box-content').load('/global/sitelets/email/view.asp?'+emailQuery);
		  
	}
		
	return false;	
}


function submitEmailFormCustom(form) {

    var toEmail = form["sendto"].value;
    var fromEmail = form["youraddress"].value;
    var name = form["your-name"].value;
    var campaignTrackingID = form["omniture"].value;
    var firstName = name.substring(0, name.indexOf(" "));
    var lastName = name.substring(name.indexOf(" ") + 1);
    var message = form["message"].value;
    var sendToYourself = form["to-yourself"].checked;
    var title = document.title;
    var urlToSend = urlEncode(changeQueryStringValue(document.location.href, "cid", campaignTrackingID));

    if (sendToYourself) {
        toEmail += ", " + fromEmail;
    }


    var valid = true;


    /**  CHECK TO EMAIL **/

    var toEmailArray = toEmail.split(",");

    //if one email
    if (toEmailArray.length < 1) {
        valid = validateEmail(document.emailtestform.toEmail.value);
       

    } else {
        //if multiple emails			
        for (var i = 0; i < toEmailArray.length; i++) {
            var email = toEmailArray[i];

            if (email != null && valid) {
                //trim the whitepsace
                valid = validateEmail(email.replace(/^\s+|\s+$/g, ''));
                if (!valid) return false;
            }
        }
    }

    /**  CHECK FROM EMAIL **/
    valid = validateEmail(fromEmail);
    if (!valid) return false;


    if (valid) {

        var emailQuery = "toEmail=" + htmlEncode(toEmail) + "&fromEmail=" + htmlEncode(fromEmail) + "&firstName=" + htmlEncode(firstName) + "&lastName=" + htmlEncode(lastName) + "&message=" + htmlEncode(message) + "&title=" + htmlEncode(title) + "&urlToSend=" + urlToSend;

        var s = s_gi('amppublic,ampglobal');
        s.linkTrackVars = 'eVar21,events,prop17';
        s.linkTrackEvents = 'event17';
        s.eVar21 = 'amp_email_this_page_submit';
        s.events = 'event17';
        s.prop17 = s_om.pageName;
        s.tl(this, 'o', 'amp_email_this_page_submit');


        var request = new getHTTPObject();

        request.open("POST", "/global/sitelets/email/default.asp?" + emailQuery, true)
        request.send(emailQuery)

        $('#email-this-box-content').load('/global/sitelets/email/view.asp?' + emailQuery);

    }

    return false;
}


/**
 * Taken from 
 * http://cass-hacks.com/articles/code/js_url_encode_decode/
 */
function htmlEncode (clearString) {
  var output = '';
  var x = 0;
  clearString = clearString.toString();
  var regex = /(^[a-zA-Z0-9_.]*)/;
  while (x < clearString.length) {
    var match = regex.exec(clearString.substr(x));
    if (match != null && match.length > 1 && match[1] != '') {
    	output += match[1];
      x += match[1].length;
    } else {
      if (clearString[x] == ' ')
        output += '+';
      else {
        var charCode = clearString.charCodeAt(x);
        var hexVal = charCode.toString(16);
        output += '%' + ( hexVal.length < 2 ? '0' : '' ) + hexVal.toUpperCase();
      }
      x++;
    }
  }
  return output;
}


/**
 * DHTML email validation script. Courtesy of SmartWebby.com (http://www.smartwebby.com/dhtml/)
 * Cleaned up by Bao
 */
function validateEmail(str) {

	var valid = true;

	var at="@"
	var dot="."
	var lat=str.indexOf(at)
	var lstr=str.length
	var ldot=str.indexOf(dot)
	
	if (str.indexOf(at)==-1){
		valid = false;
	} else if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){
		valid = false;
	} else if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){
		valid = false;
	} else if (str.indexOf(at,(lat+1))!=-1){
		valid = false;
	} else if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){
		valid = false;
	} else if (str.indexOf(dot,(lat+2))==-1){
		valid = false;
	} else if (str.indexOf(" ")!=-1){
		valid = false;
	} 
	 
	 
	if(!valid) {
		alert("Invalid email \"" + str + "\"");
	}

	return valid					
}

/**
 * Dynamically inject a query string  into an existing url
 * Taken from http://www.planet-source-code.com/vb/scripts/ShowCode.asp?txtCodeId=3614&lngWId=2
 */
function changeQueryStringValue(action,qsname,qsvalue) {
	if (action.indexOf('?') == -1 ) {	 //If no querystring present
		return (action+"?"+qsname+"="+urlEncode(qsvalue)); //x.asp?Name=Peter+Paul
	} else {		
		if ( (action.indexOf("?"+qsname+"=") == -1 ) && (action.indexOf("&"+qsname+"=") == -1 ) ) { //Passed querystring not already present		
			return (action+"&"+qsname+"="+urlEncode(qsvalue)); //x.asp?Age=25&Name=Peter+Paul;
		} else { //Passed Querystring already present, replace it no matter if it is ?Name= or &Name=
	
			var replaceQSPrefix=(action.indexOf("?"+qsname+"=")==-1)?"&"+qsname+"=":"?"+qsname+"=";
			var replaceQSSuffix=""; //This will be John in case of ?Name=John or &Name=John 
			startpos=action.indexOf(replaceQSPrefix);
			
			for (a=(startpos+replaceQSPrefix.length);a<Action.length;a++) {
					if ( action.charAt(a)=='&' ) //Next querystring beginning
						break;
					else
						replaceQSSuffix += action.charAt(a);
				}
				newaction=action.replace(replaceQSPrefix+replaceQSSuffix,replaceQSPrefix+urlEncode(qsvalue));
				return newaction;
			}	
		}		
}

/**
 * Helper for changeQueryStringValue
 */
function urlEncode(text)  {		
	//text="https://www.ameriprise.com/global/scripts/A.asp?name=Amit Chauhan&Age=25"; //Example
 
	text=replaceAll(text, "?", "%3F");
	text=replaceAll(text, "=", "%3D");
	text=replaceAll(text, "&", "%26");
	text=replaceAll(text, " ", "+");
	text=replaceAll(text, ",", "%2c");	
	
	return text;
}

 
/**
 * Helper for changeQueryStringValue
 */  
function replaceAll(varb, replaceThis, replaceBy) {	
	newvarbarray=varb.split(replaceThis);
	newvarb=newvarbarray.join(replaceBy);	
	return newvarb;
}


function getHTTPObject() {
	var xhr = false;
	if(window.XMLHttpRequest) {
		xhr = new XMLHttpRequest();
	} else if(window.ActiveXObject) {
		xhr = new ActiveXObject("Microsoft.XMLHTTP");
	}
	return xhr;
}


function addQuery(url, name,value) { 
 
	
	var currQuery = location.search.substring(1); 
 
	url += (currQuery?"%26":"%3F") + name+"="+value; 
	return url;
} 

 


