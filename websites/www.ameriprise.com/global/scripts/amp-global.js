"use strict";

/* --- Misc Setup on Page Load --- */
jQuery(document).ready(function () {
    // video popups
    $('a.video').click(function () {
        var href = $(this).attr('href');
        window.open(href, 'secure', 'width=440,height=350,scrollbars=no,location=no,dependent=yes,resizable=no,top=100,left=100'); return false;
    });

    // login security link
    $('a#security').click(function () {
        var href = $(this).attr('href');
        window.open(href, 'secure', 'width=565,height=450,scrollbars=yes,location=no,dependent=yes,resizable=no,top=25,left=25'); return false;
    });

    // insert ending quotation marks
    //$('.black-holder p span').after('<img src="Unknown_83_filename"/*tpa=https://www.ameriprise.com/careers/global/images/cite-end.gif*/ class="cite-end" />');

    // add pdf icon to pdf links (prevent duplicate pdf icon on search page)
    if (window.location.hostname != 'https://www.ameriprise.com/global/scripts/ameriprise.guided.ss-omtrdc.net') {
        $('a[href$=".pdf"], a[href$=".PDF"]').attr("target", "blank").after(' <img src="../images/pdf-ico.gif"/*tpa=https://www.ameriprise.com/global/images/pdf-ico.gif*/ class="pdf-icon" title="This is a PDF document">');
    }

    // set up tabs (new jQuery UI needs parent div as target, not the actual tabs UL)
    $('#tabs').each(function () {
        if ($(this).attr('class') == undefined || $(this).attr('class').indexOf('ui-tabs') == -1) {
            if ($(this).is('ul')) {
                $(this).parent().tabs();
            } else {
                $(this).tabs();
            }
        }
    });

    // set up lightbox
    /*$('a.lightbox').lightBox({
    overlayBgColor: '#666',
    overlayOpacity: 0.6,
    imageBlank: '../images/buttons/lightbox-blank.gif'/*tpa=https://www.ameriprise.com/global/images/buttons/lightbox-blank.gif*/,
    imageLoading: '../images/buttons/lightbox-blank.gif'/*tpa=https://www.ameriprise.com/global/images/buttons/lightbox-blank.gif*/,
    imageBtnClose: '../images/buttons/close-window.gif'/*tpa=https://www.ameriprise.com/global/images/buttons/close-window.gif*/,
    imageBtnPrev: '../images/buttons/lightbox-btn-prev.gif'/*tpa=https://www.ameriprise.com/global/images/buttons/lightbox-btn-prev.gif*/,
    imageBtnNext: '../images/buttons/lightbox-btn-next.gif'/*tpa=https://www.ameriprise.com/global/images/buttons/lightbox-btn-next.gif*/,
    containerResizeSpeed: 350,
    txtImage: 'Image',
    txtOf: 'of'
    });*/

    // track lightbox click
    $('a.lightbox').click(function () {
        trackOmnitureClickThrough(this.href);
        return false;
    });

    // set up colorbox
    $.colorbox.settings.opacity = 0.5;
    $('.colorbox-iframe').colorbox();
    $('.colorbox').click(function () {
        var params = {};
        //detect data-attributes on element and add them to param object
        $.each($(this).data(), function (key, value) {
            params[key] = value;
        });
        $(this).colorbox(params);
    });

    // accordion links
    $(".accordion a").click(function () {
        $(this).next().toggle();
    }).next().hide();

    $("ul.exp li a.trigger").click(function () {
        $(this).toggleClass("open");
        $(this).next().toggle();
        return false;
    });

    // print page link
    $('#print-page a').click(function () {
        window.print(); return false;
    });

    // validate small find an advisor box
    $('.find-an-advisor form').attr('onsubmit', 'return validateZIP(this.zip.value)');

    $('.tools-close').click(function () { $('#email-this-box').toggleClass("open"); return false; });

    setUpEmailAndShare();

    // Close the tools boxex if link with this class is clicked
    $('.content').on('click', '.tools-close', function () {

        $('#email-this-box').toggleClass("open");
        return false;
    });
    //assign name attribute to login iframe
    $(".home-page-login iframe").attr( "name", "login-iframe");
});



function omnitureABTest(socialMediaName) {

    var testPath = $('.social-bar').attr('data-test-path');
    var socialMediaShortName = socialMediaName;

    if (socialMediaName == "facebooklike") {
        socialMediaShortName = "fblike";
    }
    else if(socialMediaName == "facebooksend") {
    socialMediaShortName = "fbsend";
    }

    var s = s_gi('amppublic,ampglobal');
    s.trackingServer = "https://www.ameriprise.com/global/scripts/ns.ameriprisestats.com";
    s.trackingServerSecure = "https://www.ameriprise.com/global/scripts/s.ameriprisestats.com";
    s.pageName = 'ameriprise:share:submit';
    s.channel = 'Ameriprise';
    s.linkTrackVars = 'eVar21,events,prop17,prop16';
    s.linkTrackEvents = 'event17';
    s.eVar21 = 'share_' + socialMediaName;
    s.events = 'event17';
    s.prop17 = window.location;
    s.prop16 = "share module:" + testPath;
    s.t();

}


/* --- Share / Email This Page --- */
function setUpEmailAndShare() {
    if (typeof openTwitter != 'undefined' && typeof openLinkedIn != 'undefined' && typeof openFacebook != 'undefined') {
        $('#share-twitter').on('click', openTwitter);
        $('#share-linkedin').on('click', openLinkedIn);
        $('#share-facebook').on('click', openFacebook);
    }

    $('#share-this-link').click(function(){
	    $('#tools div').removeClass("open");
		$('#share-this-box').toggleClass("open");

		var s=s_gi('amppublic,ampglobal'); 
		s.linkTrackVars='eVar21,events,prop17'; 
		s.linkTrackEvents='event17'; 
		s.eVar21='st_sharethis'; 
		s.events='event17'; 
		s.prop17 = s_om.pageName;
		s.tl(this,'o','st_sharethis');
		
        return false;
    });		

    $('#email-this-link').click(function(){
        $('#tools div').removeClass("open");
        $('#email-this-box').toggleClass("open");

        var s=s_gi('amppublic,ampglobal'); 
        s.linkTrackVars='eVar21,events,prop17'; 
        s.linkTrackEvents='event17'; 
        s.eVar21='amp_email_this_page_click'; 
        s.events='event17'; 
        s.prop17 = s_om.pageName;
        s.tl(this,'o','amp_email_this_page_click');
		
		return false;
});

$('#email-this-link2').click(function () {

    $('#tools div').removeClass("open");
    $('#email-this-box').toggleClass("open");
    omnitureABTest('email');



    jQuery.validator.addMethod('multiemail', function (value, element, param) {
        if (value == "")
            return false;

        var toEmailArray = value.split(",");

        var valid = true;
        //if one email
        if (toEmailArray.length < 1) {
            return validatorEmail(toEmailArray[0]);

        } else {
            //if multiple emails			
            for (var i = 0; i < toEmailArray.length; i++) {
                var email = toEmailArray[i];

                if (email != null) {
                    //trim the whitepsace
                    valid = validatorEmail(email.replace(/^\s+|\s+$/g, ''));
                    if (!valid) return false;
                }
            }
            return true;
        }
    }, "Please enter a valid email address");


    function validatorEmail(str) {

        var valid = true;

        var at = "@"
        var dot = "."
        var lat = str.indexOf(at)
        var lstr = str.length
        var ldot = str.indexOf(dot)

        if (str.indexOf(at) == -1) {
            valid = false;
        } else if (str.indexOf(at) == -1 || str.indexOf(at) == 0 || str.indexOf(at) == lstr) {
            valid = false;
        } else if (str.indexOf(dot) == -1 || str.indexOf(dot) == 0 || str.indexOf(dot) == lstr) {
            valid = false;
        } else if (str.indexOf(at, (lat + 1)) != -1) {
            valid = false;
        } else if (str.substring(lat - 1, lat) == dot || str.substring(lat + 1, lat + 2) == dot) {
            valid = false;
        } else if (str.indexOf(dot, (lat + 2)) == -1) {
            valid = false;
        } else if (str.indexOf(" ") != -1) {
            valid = false;
        }

        return valid
    }


    $("#emailform").validate({
        submitHandler: function (form) {
            submitEmailFormCustom(form);
            return false;
        },
        rules: {
            // simple rule, converted to {required:true}
            sendto: {
                multiemail: true
            },
            // compound rule
            youraddress: {
                multiemail: true
            },
            message: "required"

        }, messages: {
            message: "Enter a message."
        }
    });
    return false;
});		





}




/* --- Open Popup with Flash Video --- */
function popupVid352by240(vid) {
    var popup = window.open('/global/media/video-players/videopopup-352-240.asp?video=' + vid, 'video', 'location=no,menubar=no,toolbar=no,status=no,resizable=no,scrollbars=no,width=352,height=260');
}

function popupAndRedirect(popupUrl, redirectUrl) {
    //code to open the pop up URL
    window.open(popupUrl, 'secure', 'width=605,height=620,scrollbars=yes,location=no,dependent=yes,resizable=yes,top=25,left=25')
    // redirect current browser
    window.location = redirectUrl;
}

/* --- Wrappers for Getting and Setting Cookies --- */
var intdays = 366;
var now = new Date();
now.setTime(now.getTime() + intdays * 24 * 60 * 60 * 1000);
var cookie_path = "/"; //"/" as root
var cookie_domain = document.domain; //"https://www.ameriprise.com/global/scripts/www.ameriprise.com"

function getCookie(name) {
    var re = new RegExp(name + "=([^;]+)");
    var value = re.exec(document.cookie);
    return (value != null) ? unescape(value[1]) : null;
}

function setCookie(name, value, expires, path, domain, secure) {
    var curCookie = name + "=" + escape(value) +
      ((expires) ? "; expires=" + expires.toGMTString() : "") +
      ((domain) ? "; domain=" + domain : "") +
      ((path) ? "; path=" + path : "") +
      ((secure) ? "; secure" : "");
    document.cookie = curCookie;
}

/* --- Validate Fields for Small Find an Advisor Box --- */
function validateDropdown(url, value, errorMessage) {
    if (value == "") {
        alert(errorMessage);
    } else {
        location = url;
    }
}

function validateZIP(field) {
    var valid = "0123456789-";
    var hyphencount = 0;

    if (field.length != 5 && field.length != 10) {
        alert("Please enter a valid Zip/Postal code.");
        return false;
    }
    for (var i = 0; i < field.length; i++) {
        temp = "" + field.substring(i, i + 1);
        if (temp == "-") hyphencount++;
        if (valid.indexOf(temp) == "-1") {
            alert("Invalid characters in your zip code. Please try again.");
            return false;
        }
        if ((hyphencount > 1) || ((field.length == 10) && "" + field.charAt(5) != "-")) {
            alert("The hyphen character should be used with a properly formatted 5 digit+four zip code, like '12345-6789'. Please try again.");
            return false;
        }
    }
    return true;
}

/* --- Get Video Name From Query String --- */
function gup(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);
    if (results == null)
        return "";
    else
        return results[1];
}

/* --- Custom Tabs --- */
/*function initializeCustomTabs(navID, containerID, contentTag) {
    if (typeof contentTag == 'undefined') {
        contentTag = 'div';
    }
    $("#" + navID + " a").each(function (i) {
        var id = $(this).attr('href');
        if (i > 0) {
            $(id).hide();
        } else {
            $("#" + navID).attr("class", id.substring(1));
        }
        $(this).click(function (ev) {
            var myId = $(this).attr("href")
            ev.preventDefault();
            $("#" + containerID + " > " + contentTag).hide();
            $("#" + navID).attr("class", myId.substring(1));
            $(myId).show();
        });
    });
}*/
function initializeCustomTabs() {
    $('#tabs').click( function () {
        var tabsArray = $('#tabs-generic').children('ul').children('li');
        tabsArray.each(function() {
            $(this).removeClass('next-tab');
            $(this).removeClass('prev-tab');
        });
        tabsArray.each(function () {
            if ($(this).hasClass('ui-state-active')) {
                if ($(this).next('li').length !== 0) {
                    $(this).next('li').addClass('next-tab');
                } 
                if ($(this).prev('li').length !== 0) {
                    $(this).prev('li').addClass('prev-tab');
                }
            }
        });
    });
}

window.twttr = (function (d, s, id) {
    var t, js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return; js = d.createElement(s); js.id = id;
    js.src = "../../../platform.twitter.com/widgets.js"/*tpa=https://platform.twitter.com/widgets.js*/; fjs.parentNode.insertBefore(js, fjs);
    return window.twttr || (t = { _e: [], ready: function (f) { t._e.push(f) } });
} (document, "script", "twitter-wjs"));

function extractParamFromUri(uri, paramName) {
    if (!uri) {
        return;
    }
    var regex = new RegExp('[\\?&#]' + paramName + '=([^&#]*)');
    var params = regex.exec(uri);
    if (params != null) {
        return unescape(params[1]);
    }
    return;
}

function trackTwitter(intent_event) {
    if (intent_event) {
        var opt_pagePath;
        if (intent_event.target && intent_event.target.nodeName == 'IFRAME') {
            opt_target = extractParamFromUri(intent_event.target.src, 'url');
        }
        omnitureABTest('twitter');
    }
}

//Wrap event bindings - Wait for async js to load
twttr.ready(function (twttr) {
    //event bindings
    twttr.events.bind('tweet', trackTwitter);
});


(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "../../../connect.facebook.net/en_US/all-1.js#xfbml=1"/*tpa=https://connect.facebook.net/en_US/all.js#xfbml=1*/;
    fjs.parentNode.insertBefore(js, fjs);
} (document, 'script', 'facebook-jssdk'));

window.fbAsyncInit = function () {

    FB.Event.subscribe('https://www.ameriprise.com/global/scripts/message.send', function (targetUrl) {
        omnitureABTest('facebooksend');
    });

    FB.Event.subscribe('edge.create', function (targetUrl) {
        omnitureABTest('facebooklike');
    });
}

function plusOneCallback(obj) {
    omnitureABTest('google');
}

