"use strict";

//s_om.server="https://www.ameriprise.com/global/scripts/omniture/www.ameriprise.com";
var bodyText = (document.body ? document.body.innerHTML : "");
//findOffersRE(bodyText);
parseURLPath();
infoFind();
s_om.eVar12 = s_om.getQueryParam('vanity')
s_om.eVar50 = s_om.getQueryParam('s_kwcid');
s_om.eVar25 = s_om.getQueryParam('PartnerId');

var MANUAL_CLICKTHROUGH = false;


function findOffersRE(bodyString) {
    //find soid from offer URLs
    //split on OM URL and look for soid at beginning of string.
    //soid must start with o= and end with ",',&,> or space.
    //if the soid contains 2 dashes only the first two groups of numbers are returned
    //soid must be numeric, dash is optional UNLESS CID is present, then both dashes are required.
    //on second thought, let's just say the dash in the soid is required, the second one is optional.
    var impString = "";
    var sPattern = /https:\/\/www12\.ex\.is\.ameriprise\.com\/omi\/FS\?rt=f.+[;&]o=/gi;
    var ar = bodyString.split(sPattern);
    var sStr
    var RE = new RegExp('(^[0-9]+\-?[0-9]+)[-&"\' >]')
    for (i = 1; i <= ar.length - 1; i++) {
        if (RE.test(ar[i])) {
            sStr = ar[i].match(RE);
            if (impString == "") {
                impString = RegExp.$1;
            }
            else {
                impString = impString + ",;" + RegExp.$1;
            }
        }
    }



    if (impString.length > 0) {

        /* SiteCatalyst Variables */
        s_om.products = ";" + impString;
        if (impString.length > 0) addEvent("event13"); //offer impression only if offers are found
    }

}

/**
* Helper method for getting cookie.
* (Calling the method getCookieOmniture so there wont be any name conflicts)
* @source http://www.w3schools.com/JS/js_cookies.asp
* @author Bao
*/
function getCookieOmniture(c_name) {
    if (document.cookie.length > 0) {
        c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1) {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) {
                c_end = document.cookie.length;
            }
            return unescape(document.cookie.substring(c_start, c_end));
        }
    }
    return "";
}

function parseURLPath() {
    //Parses URL path and sets channel and sprop1-4 based on directory structure.

    var aURL = document.URL.toLowerCase().split('/');
    if (aURL.length >= 4) {

        //not used yet.. once this goes live, need to account for subdomains
        var indexOffset = 0;

        var hostname = aURL[2];

        //to account for hostnames from SEO
        //sknits seira
        if (hostname.indexOf("https://www.ameriprise.com/global/scripts/omniture/financial-planning.ameriprise.com") >= 0 ||
			hostname.indexOf("https://www.ameriprise.com/global/scripts/omniture/retirement.ameriprise.com") >= 0 ||
			hostname.indexOf("https://www.ameriprise.com/global/scripts/omniture/budgeting-investing.ameriprise.com") >= 0 ||
			hostname.indexOf("https://www.ameriprise.com/global/scripts/omniture/banking-credit.ameriprise.com") >= 0 ||
			hostname.indexOf("https://www.ameriprise.com/global/scripts/omniture/insurance.ameriprise.com") >= 0 ||
			hostname.indexOf("https://www.ameriprise.com/global/scripts/omniture/investment.ameriprise.com") >= 0 ||
			hostname.indexOf("https://www.ameriprise.com/global/scripts/omniture/mortgages-home-loan.ameriprise.com") >= 0 ||
			hostname.indexOf("https://www.ameriprise.com/global/scripts/omniture/financial-goals.ameriprise.com") >= 0 ||
			hostname.indexOf("https://www.ameriprise.com/global/scripts/omniture/investor-relations.ameriprise.com") >= 0) {

            // we need to stripHostname and than inject into the array
            aURL = addItem(aURL, stripHostname(hostname), 3);
        }

        //see if its more than just the homepage
        if (aURL.length >= 4) {

            //hostname
            s_om.server = aURL[2];

            s_om.channel = "Ameriprise";

            //if its careers
            if (aURL[2] == "https://www.ameriprise.com/global/scripts/omniture/joinameriprise.com" || aURL[2] == "https://www.ameriprise.com/global/scripts/omniture/www.joinameriprise.com") {
                s_om.channel = "Careers";
            }

            if (aURL[3] == "retire") {
                aURL[3] = "retirement";
            }

            //intercept for the subnav items under Products & Services
            if (aURL[3] == "banking-credit" || aURL[3] == "insurance" || aURL[3] == "investment" || aURL[3] == "mortgages-home-loan") {
                s_om.prop1 = capWords("financial-products");

                for (var i = 0; i < aURL.length; i++) {
                    if (i == 3) {
                        s_om.prop2 = capWords(aURL[3]);
                    } else if (i == 4) {
                        s_om.prop3 = capWords(aURL[4]);
                    } else if (i == 5) {
                        s_om.prop4 = capWords(aURL[5]);
                    }
                }

                //override page names
                var pagename = "financial-products";
                //track page name
                for (var i = 3; i < aURL.length; i++) {

                    var name = aURL[i];
                    name = removeQueryString(name);

                    if (name == "") {
                        name = "https://www.ameriprise.com/global/scripts/omniture/default.asp";
                    }
                    pagename += ":" + name;
                }

                s_om.pageName = removeQueryString(pagename);
                s_om.tempPageName = removeQueryString(pagename);


            } else {

                //default tracking behavior for s props
                for (var i = 0; i < aURL.length; i++) {

                    if (i == 3) {
                        s_om.prop1 = capWords(aURL[3]);
                    } else if (i == 4) {
                        s_om.prop2 = capWords(aURL[4]);
                    } else if (i == 5) {
                        s_om.prop3 = capWords(aURL[5]);
                    } else if (i == 6) {
                        s_om.prop4 = capWords(aURL[6]);
                    }
                }

                //override page names
                var pagename = "" + aURL[3];

                //for homepage:
                if (aURL[3] == "") {
                    pagename = "https://www.ameriprise.com/global/scripts/omniture/default.asp";
                }

                //track page name
                for (var i = 4; i < aURL.length; i++) {

                    var name = aURL[i];
                    name = removeQueryString(name);

                    if (name == "") {
                        name = "https://www.ameriprise.com/global/scripts/omniture/default.asp";
                    }
                    pagename += ":" + name;
                }

                //stupid race condition
                pagename = removeQueryString(pagename);
				if (pagename == "") { pagename = "https://www.ameriprise.com/global/scripts/omniture/default.asp"; }
				s_om.pageName = pagename;
				s_om.tempPageName = pagename;


                //check to see if they are client by reading cookie
                //var clientCookie = getCookieOmniture("CLIENT%5FRECOGNITION");
               /*var clientCookie = $.cookie("CLIENT%5FRECOGNITION");

                if (clientCookie == "true") {
                    s_om.eVar13 = "Client";
                }*/
            }
        }
    }

    checkForInternalLink();

    checkForImpressions();
    checkForClickThrough();

}


/**
* Checks to see if query string has 'int' in it.
*/
function checkForInternalLink() {
    if (s_om.getQueryParam("int") != null && s_om.getQueryParam("int") != "") {
        s_om.eVar21 = s_om.getQueryParam("int");
        s_om.events = 'event17';
    }
}

/**
* THE REPLACEMENT TO OFFERMATICA IMPRESSIONS
*/
function checkForImpressions() {

    if ($.cookie("ADROTATORIMPRESSION")) {

        //delete the cookie, no longer needed
        $.cookie("ADROTATORIMPRESSION", null);

        s_om.events = 'event13';

        var ADROTATOR_EVAR41 = $.cookie("ADROTATOREVAR41");
        if (ADROTATOR_EVAR41 != "" && ADROTATOR_EVAR41 != null) {
            s_om.eVar41 = ADROTATOR_EVAR41;
            $.cookie("ADROTATOREVAR41", null);
        }

        var ADROTATOR_EVAR42 = $.cookie("ADROTATOREVAR42");
        if (ADROTATOR_EVAR42 != "" && ADROTATOR_EVAR42 != null) {
            s_om.eVar42 = ADROTATOR_EVAR42;
            $.cookie("ADROTATOREVAR42", null);
        }

        var ADROTATOR_EVAR43 = $.cookie("ADROTATOREVAR43");
        if (ADROTATOR_EVAR43 != "" && ADROTATOR_EVAR43 != null) {

            s_om.eVar43 = ADROTATOR_EVAR43;
            $.cookie("ADROTATOREVAR43", null);
        }

        var ADROTATOR_EVAR44 = $.cookie("ADROTATOREVAR44");
        if (ADROTATOR_EVAR44 != "" && ADROTATOR_EVAR44 != null) {
            s_om.eVar44 = ADROTATOR_EVAR44;
            $.cookie("ADROTATOREVAR44", null);
        }

        var ADROTATOR_EVAR45 = $.cookie("ADROTATOREVAR45");
        if (ADROTATOR_EVAR45 != "" && ADROTATOR_EVAR45 != null) {
            s_om.eVar45 = ADROTATOR_EVAR45;
            $.cookie("ADROTATOREVAR45", null);
        }
    }
}

/**
* THE REPLACEMENT TO OFFERMATICA CLICK THROUGHS
*/
function checkForClickThrough() {

    var adrotatorCookie = $.cookie("ADROTATORSELECTED");

    if (adrotatorCookie != "" && adrotatorCookie != null) {
        var eVarCookie = $.cookie("ADROTATORSELECTEDEVAR");

        //find where the clickthrough was, and track against that evar
        switch (eVarCookie) {
            case "ADROTATOREVAR41":
                s_om.eVar41 = adrotatorCookie;
                break;
            case "ADROTATOREVAR42":
                s_om.eVar42 = adrotatorCookie;
                break;
            case "ADROTATOREVAR43":
                s_om.eVar43 = adrotatorCookie;
                break;
            case "ADROTATOREVAR44":
                s_om.eVar44 = adrotatorCookie;
                break;
            case "ADROTATOREVAR45":
                s_om.eVar45 = adrotatorCookie;
                break;
            default:
                s_om.eVar21 = adrotatorCookie;
        }
        s_om.events = 'event17';

        //destroy the cookies when we are done
        $.cookie('ADROTATORSELECTED', null, { path: '/', domain: 'https://www.ameriprise.com/global/scripts/omniture/ameriprise.com' });
        $.cookie('ADROTATORSELECTEDEVAR', null, { path: '/', domain: 'https://www.ameriprise.com/global/scripts/omniture/ameriprise.com' });

    }
}

/**
* An offer has been clicked, keep track of it in a cookie
* Also keep track of what evar it was clicked in
*/
function setAdClickThrough(clickThroughName, eVar) {

    //account for third party, using the trackOmnitureClickThroughWithEVar function
    if (!MANUAL_CLICKTHROUGH) {
        $.cookie('ADROTATORSELECTED', clickThroughName, { path: '/', domain: 'https://www.ameriprise.com/global/scripts/omniture/ameriprise.com' });
        $.cookie('ADROTATORSELECTEDEVAR', eVar, { path: '/', domain: 'https://www.ameriprise.com/global/scripts/omniture/ameriprise.com' });
    }
}



function removeQueryString(url) {
    var queryIndex = url.indexOf('?');

    if (queryIndex >= 0) {
        url = url.substring(0, queryIndex);
    }

    return url;
}


/**
* Adds item to an array positon and increments everything down
* @param array - the array
* @param item - the item you want to add
* @param index - where you want to inject the item into the array
*/
function addItem(array, item, index) {
    var tempArray = new Array();

    for (var i = 0; i < index; i++) {
        tempArray[i] = array[i];
    }
    tempArray[index] = item;

    for (var i = index; i < array.length; i++) {
        tempArray[i + 1] = array[i];
    }
    return tempArray;

}

/**
* Strip out hostname, QA or prod
*/
function stripHostname(hostname) {

    hostname = hostname.replace("qa.", "");
    hostname = hostname.replace(".ameriprise.com", "");

    return hostname;
}


function capWords(inputString) {

    inputString = removeQueryString(inputString);

    //remove default.asp from sprop
    if (inputString == "https://www.ameriprise.com/global/scripts/omniture/default.asp") {
        return "";
    }

    var tmpStr, tmpChar, preString, postString, strlen;
    tmpStr = inputString.toLowerCase();
    tmpStr = tmpStr.replace(/-/g, " ");
    tmpStr = tmpStr.replace(/_/g, " ");
    tmpStr = tmpStr.replace(".asp", "");
    var stringLen = tmpStr.length;
    if (stringLen > 0) {
        for (var i = 0; i < stringLen; i++) {
            if (i == 0) {
                tmpChar = tmpStr.substring(0, 1).toUpperCase();
                postString = tmpStr.substring(1, stringLen);
                tmpStr = tmpChar + postString;
            }
            else {
                tmpChar = tmpStr.substring(i, i + 1);
                if (tmpChar == " " && i < (stringLen - 1)) {
                    tmpChar = tmpStr.substring(i + 1, i + 2).toUpperCase();
                    preString = tmpStr.substring(0, i + 1);
                    postString = tmpStr.substring(i + 2, stringLen);
                    tmpStr = preString + tmpChar + postString;
                }
            }
        }
    }
    return tmpStr;
}

function infoFind() {



    //Add additional info find pages to this array. 
    var aInfoFind = new Array(
		"https://www.ameriprise.com/financial-planning-guide/first-financial-plan-meeting.asp",
		"/financial-planning-services/financial-planning-fees/",
		"https://www.ameriprise.com/financial-planning-services/financial-planning-fees/brokerage-commissions-and-fees.asp",
		"https://www.ameriprise.com/financial-planning-services/ameriprise-financial-advisors/financial-advisor-compensation.asp",
		"https://www.ameriprise.com/financial-planning-services/ameriprise-financial-advisors/financial-advisor-selection.asp",
		"https://www.ameriprise.com/financial-planning-services/ameriprise-financial-advisors/financial-advisor-expectations.asp",
		"https://www.ameriprise.com/financial-planning-services/ameriprise-financial-advisors/ameriprise-financial-advisor-credentials.asp",
		"/financial-planning-services/ameriprise-financial-inc/",
		"/financial-planning-services/our-client-stories/",
		"https://www.ameriprise.com/financial-planning-guide/financial-planning-checklist/personal-finances.asp",
		"https://www.ameriprise.com/financial-planning-guide/financial-planning-checklist/financial-planning-goals.asp",
		"https://www.ameriprise.com/financial-planning-guide/financial-planning-checklist/financial-goals-checklist.asp",
		"https://www.ameriprise.com/financial-planning-services/financial-planner/financial-planning-firm.asp",
		"https://www.ameriprise.com/financial-planning-services/financial-planner/financial-services.asp",
		"https://www.ameriprise.com/financial-planning-services/financial-planner/financial-goals.asp",
		"/financial-planning-services/financial-counseling-and-planning/",
		"/financial-planning-basics/",
		"https://www.ameriprise.com/financial-planning-basics/personal-financial-planning.asp",
		"https://www.ameriprise.com/financial-planning-basics/reach-your-financial-goals.asp",
		"/financial-planning-basics/workplace-financial-planning/",
		"/planning-for-retirement/",
		"/planning-for-retirement/retirement-advice/",
		"/planning-for-retirement/retirement-stages/",
		"/planning-for-retirement/top-retirement-questions/",
		"/planning-for-retirement/retirement-advice/",
		"https://www.ameriprise.com/financial-planning-articles/retirement-planning-information/baby-boomer-retirement.asp",
		"https://www.ameriprise.com/financial-planning-articles/retirement-planning-information/retirement-study.asp",
		"/budgeting-investing/financial-planning-articles/",
		"https://www.ameriprise.com/financial-planning-articles/retirement-planning-information/consolidate-retirement-accounts.asp",
		"/financial-planning-articles/retirement-planning-information/",
		"/financial-planning-articles/retirement-planning-information/",
		"/financial-planning-articles/retirement-planning-information/",
		"/financial-planning-articles/investing-guide/",
		"https://www.ameriprise.com/financial-planning-articles/estate-planning-information/estate-financial-planning.asp",
		"https://www.ameriprise.com/financial-planning-articles/estate-planning-information/updating-beneficiary-forms.asp"
		);
    for (var i = 0; i < aInfoFind.length; i++) {
        if (document.URL.toLowerCase().indexOf(aInfoFind[i]) > 0) {
            s_om.eVar5 = "Amp Info Find:" + s_om.getPageName();
            addEvent("event7");
            break;
        }
    }
}

function addEvent(sEvent) {
    if (!s_om.events) {
        s_om.events = sEvent;
    } else {
        s_om.events = s_om.events + "," + sEvent;
    }
}


/**
* Track click through
*/
function trackOmnitureClickThroughWithEVar(name, eVarName, eVarValue) {
    var s = s_gi('amppublic,ampglobal');

    MANUAL_CLICKTHROUGH = true;

    //destroy the cookies, since this emulates the clickthrough
    $.cookie('ADROTATORSELECTED', null, { path: '/', domain: 'https://www.ameriprise.com/global/scripts/omniture/ameriprise.com' });
    $.cookie('ADROTATORSELECTEDEVAR', null, { path: '/', domain: 'https://www.ameriprise.com/global/scripts/omniture/ameriprise.com' });


    s.eVar41 = "";
    s.eVar42 = "";
    s.eVar43 = "";
    s.eVar44 = "";
    s.eVar45 = "";
    s.eVar21 = "";

    switch (eVarName) {
        case "eVar41":
            s.linkTrackVars = 'eVar21,eVar41,events,prop17';
            s.eVar41 = eVarValue;
            break;
        case "eVar42":
            s.linkTrackVars = 'eVar21,eVar42,events,prop17';
            s.eVar42 = eVarValue;
            break;
        case "eVar43":
            s.linkTrackVars = 'eVar21,eVar43,events,prop17';
            s.eVar43 = eVarValue;
            break;
        case "eVar44":
            s.linkTrackVars = 'eVar21,eVar44,events,prop17';
            s.eVar44 = eVarValue;
            break;
        case "eVar45":
            s.linkTrackVars = 'eVar21,eVar45,events,prop17';
            s.eVar45 = eVarValue;
            break;
        default:
            s.linkTrackVars = 'eVar21,events,prop17';
            s.eVar21 = eVarValue;
    }


    s.linkTrackEvents = 'event17';
    s.eVar21 = name;
    s.events = 'event17';
    s.prop17 = s_om.pageName;
    s.trackingServer = 'https://www.ameriprise.com/global/scripts/omniture/ns.ameriprisestats.com';
    s.tl(this, 'o', name);



}


/**
* Track click through
*/
function trackOmnitureClickThrough(name) {
    var s = s_gi('amppublic,ampglobal');
    s.linkTrackVars = 'eVar21,events,prop17';
    s.linkTrackEvents = 'event17';
    s.eVar21 = name;
    s.events = 'event17';
    s.prop17 = s_om.pageName;
    s.trackingServer = 'https://www.ameriprise.com/global/scripts/omniture/ns.ameriprisestats.com';
    s.tl(this, 'o', name);
}


/**
* Track calculator
*/
function trackOmnitureCalculator(name) {
    var s = s_gi('amppublic,ampglobal');
    s.linkTrackVars = 'eVar21,events,prop17';
    s.linkTrackEvents = 'event17,event8';
    s.eVar21 = name;
    s.events = 'event17,event8';
    s.prop17 = s_om.pageName;
    s.tl(this, 'o', name);

}

/* Track Page Name with clickthrough */
function trackPage(name, sec1, sec2, sec3, sec4) {
    var s = s_gi(s_account);
    s.pageName = name;
    s.channel = sec1;
    s.prop1 = sec2;
    s.prop2 = sec3;
    s.prop3 = sec4;
    s.trackingServer = 'https://www.ameriprise.com/global/scripts/omniture/ns.ameriprisestats.com';
    s.t();
}

var FACookie1;

var advisorId;

FACookie1 = $.cookie("FACookie1");

if (typeof FACookie1 === "string") { 
    var s = FACookie1.split("|"); 
    s = s[s.length-1].split("^"); 
    s = s[s.length-1].split(","); 
    if (s.length >= 2) {
        advisorId = parseInt(s[1],10); 
    }
}

if (typeof s_om === "object" && typeof advisorId === "number") {
    s_om.eVar35 = advisorId;
};

var teaLeafCookie = $.cookie("TLTSID");

if (typeof s_om === "object") { s_om.eVar37 = teaLeafCookie; };