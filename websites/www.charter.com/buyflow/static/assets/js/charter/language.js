/**
 * Deprecated method.
 */
var switchLanguage = function(languageCode) {
	
    var inputData = {
        "arg1": languageCode,
        "arg2": window.location.href
    };
    
    var result = false;

    $.ajax({
        url : '/rest/bean/com/charter/commerce/services/content/LanguageFacade/switchLanguage',
        contentType : 'application/json',
        async       : false,        
        data        : JSON.stringify(inputData),
        success     : function(data) {
        	if(data.atgResponse !== undefined){
        	    
        		if(data.atgResponse==window.location.href){
        			window.location.reload();
        		}else{
        			window.location.href=data.atgResponse;
        		}
        	}
        	
        	//Strip lang parameter, if it exists.
        	// Regexp test cases: 
        	// https://cgi.charter.com/browse/homepage.jsp?lang=es#
            // https://cgi.charter.com/browse/homepage.jsp?lang=es&xthingy=value&ythingy=value2#
            // https://cgi.charter.com/browse/homepage.jsp?ythingy=value2&xthingy=value&lang=es#
            // https://cgi.charter.com/browse/homepage.jsp?xthingy=value1&lang=es&ythingy=value2#
            // https://cgi.charter.com/browse/tv-service/tv#Sports
            // https://cgi.charter.com/browse/tv-service/tv?lang=es#Sports
        	// https://cgi.charter.com/browse/tv-service/tv?lang=es&xthingy=value&ythingy=value2#Sports
            // https://cgi.charter.com/browse/tv-service/tv?ythingy=value2&xthingy=value&lang=es#Sports
        	// https://cgi.charter.com/browse/homepage.jsp?lang=es
            // https://cgi.charter.com/browse/homepage.jsp?lang=es&xthingy=value&ythingy=value2
            // https://cgi.charter.com/browse/homepage.jsp?ythingy=value2&xthingy=value&lang=es
            // https://cgi.charter.com/browse/homepage.jsp?xthingy=value1&lang=es&ythingy=value2
        	var noLangParameterLocation = window.location.href.replace(/([\?&])(lang=.+?)&/, '$1').replace(/([\?&])(lang=.+?)#/, '#').replace(/([\?&])(lang=.+)/, '');
        	window.location.href = noLangParameterLocation;
        }
    });
    return false;
};

/**
 * Deprecated method.
 */
var processAvailableLanguages = function(data) {
	
	
	if (data.success && data.availableLanguages !== undefined && data.availableLanguages.length>1) {
		var html = "<h3>Languages</h3><ul>";
		
		for (var i=0;i<data.availableLanguages.length;i++) { 
			var language = data.availableLanguages[i];
			if(language.selected){
				html +=  "<li>"+language.displayName+"</li>";
			}else{
				html += "<li><a href='#' onclick='switchLanguage(\"" + language.code + "\")'>" + language.displayName + "</a></li>";
			}
		}
		html += "</u>";
		$('#languageSection').html(html).show();
	
		return true;
	} else {
	    return false;
	}
};

/**
 * Deprecated method.
 */
var retrieveAvailableLanguages = function() {
    var inputData = {
        "atg-rest-depth": "2"
    };
    
    var result = false;

    $.ajax({
        url : '/rest/bean/com/charter/commerce/services/content/LanguageFacade/retrieveAvailableLanguages',
        contentType : 'application/json',
        async       : false,        
        data        : JSON.stringify(inputData),
        success     : function(data) {
        	processAvailableLanguages(data);
        }
    });
    
    return false;
};

var retrieveTranslationURL = function(languageCode, divId, labelText) {
    var inputData = {
            "arg1": languageCode,
            "arg2": window.location.href
        };
    
    var result = false;

    $.ajax({
        url : '/rest/bean/com/charter/commerce/services/content/LanguageFacade/retrieveURL',
        contentType : 'application/json',
        async       : false,        
        data        : JSON.stringify(inputData),
        success     : function(data) {
            processTranslationURL(data, divId, labelText);
        }
    });
    
    return false;
};


var processTranslationURL = function(data, divId, labelText) {
    if ((data.atgResponse !== undefined) && (data.atgResponse != '')) {
        var html = "<a href=\"" + data.atgResponse + "\">" + labelText + "</a>";
        $('#' + divId).html(html).show();
        return true;
    } else {
        $('#' + divId).html('').hide();
        return false;
    }
};
