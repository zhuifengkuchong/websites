var SAVECART = typeof SAVECART === 'undefined' ? {} : SAVECART;

/**
 * Javascript for savecart functionnality.
 */
$(document).ready(function () {
	SAVECART.bindSaveCartValidation();
	SAVECART.bindSaveCartLink();
});

/**
 * Bind the save cart validation.
 */
SAVECART.bindSaveCartValidation = function() {

	$('input#saveCartAddress').on('change blur', function(){
		CHARTER.validateUnsupportedCharacters($(this), false);
	});	

	$('input#saveCartApt').on('change blur', function(){
		CHARTER.validateUnsupportedCharacters($(this), false);
	});	

	$('input#saveCartZip').on('change blur', function(){
		CHARTER.validateZip($(this), false);
	});	

	$('input#saveCartEmail').on('change blur', function(){
		CHARTER.validateEmail($(this), false);
	});	

	$('input#modifySaveCartEmail').on('change blur', function(){
		CHARTER.validateEmail($(this), false);
	});	

};

SAVECART.validateRetrieveCartFields = function() {
	var valid = true;
	
	valid &= CHARTER.validateUnsupportedCharacters($('input#saveCartAddress'), false);
	valid &= CHARTER.validateUnsupportedCharacters($('input#saveCartApt'), false);
	valid &= CHARTER.validateZip($('input#saveCartZip'), false);
	valid &= CHARTER.validateEmail($('input#saveCartEmail'), false);

	return valid;
};


SAVECART.validateSaveCartFields = function() {
	var valid = true;
	
	valid &= CHARTER.validateEmail($('input#saveCartEmail'), false);

	return valid;
};

SAVECART.validateModifySaveCartFields = function() {
	var valid = true;
	
	valid &= CHARTER.validateEmail($('input#modifySaveCartEmail'), false);

	return valid;
};

/**
 * Bind the save/retrieve cart link.
 */
SAVECART.bindSaveCartLink = function() {
	$("#retrieveCartLink").click(function(e) {
		var valid= SAVECART.validateRetrieveCartFields();
		if (valid) {
			$("#retrieveCartForm").submit();
		}
		e.preventDefault();
	});

	$("#saveCartLink").click(function(e) {

		var valid= SAVECART.validateSaveCartFields();
		if (valid) {
			SAVECART.submitFormBeforeSave('form.save-cart');
			$("#saveCartForm").submit();
		}
		e.preventDefault();
	});
	
	$("#modifyMailLink").click(function(e) {
		var valid= SAVECART.validateModifySaveCartFields();
		if (valid) {
			SAVECART.submitFormBeforeSave('form.save-cart');
			$("#modifyMailForm").submit();
		}
		e.preventDefault();
	});
};

/**
 * 
 */
SAVECART.submitFormBeforeSave = function(formSelector) {
	$(formSelector).each(function(i, form){
		var $saveForm = SAVECART.copyForm($(form));
		$.ajax({
			dataType	: 'json',
			async		: false,
			cache		: false,
			data		: $saveForm.serialize()
		});
	});	
	return false;
};

/**
 * 
 */
SAVECART.copyForm = function($form) {
	var $saveForm = $("#save_"+$form.get(0).id);
	if($saveForm.length>0) {
		if ($form.get(0).id !== "enterYourPhonePortingForm") {
			var $inputs = $form.find('input.save-cart, select.save-cart');
			$inputs.each(function(i, input) {
				$saveInput = $saveForm.children('#save_'+input.name);
				if($saveInput.length>0) {
					SAVECART.copyInput(input,$saveInput);
				} else {
					window.console && console.log('No inputs to copy.');
				}
			});
		} else {
			populatePhoneFieldsForSavecart();
		}
	} else {
		window.console && console.log('unable to retrieve save form with id #save_'+$form.get(0).id);
	}
	return $saveForm;
};

/**
 * 
 */
SAVECART.copyInput = function(srcInput, $destInput){
	if(srcInput.tagName == "INPUT") {
		if(srcInput.type == "radio") {
			SAVECART.copyRadioInput(srcInput, $destInput);
		} else if(srcInput.type == "text") {
			SAVECART.copyTextInput(srcInput, $destInput);
		} else{
			window.console && console.log('input type not supported '+srcInput.type+' ('+srcInput.name+')');
		}
	} else if(srcInput.tagName == "SELECT") {
		SAVECART.copySelectInput(srcInput, $destInput);
	} else {
		window.console && console.log('tagname not supported '+srcInput.tagName+' ('+srcInput.name+')');
	}
};

/**
 * 
 */
SAVECART.copyTextInput = function(input, $destInput){
	$destInput.val(input.value);
	//window.console && console.log(input.name+'='+$destInput.val());
};

/**
 * 
 */
SAVECART.copyRadioInput = function(input, $destInput){
	if(input.checked){
		$destInput.val(input.value);
		//window.console && console.log(input.name+'='+$destInput.val());
	}
};

/**
 * 
 */
SAVECART.copySelectInput = function(select, $destInput){
	$destInput.val(select.options[select.selectedIndex].value);
	//window.console && console.log(select.name+'='+$destInput.val());
};