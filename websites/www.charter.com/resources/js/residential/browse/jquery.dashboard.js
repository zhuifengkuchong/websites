'use strict';

$(document).ready(function() {
	$('.dashboard .dashboard-inner').hide("fast");

	// Hide the Right Dashboard
	$('.dashboard a.close').click(function() {
        $('.dashboard').removeClass("show");
		$('.dashboard .dashboard-inner').animate(
                { marginRight: -375 , width: 0 }, // what we are animating
                'slow', // how fast we are animating
                'easeOutSine' // the type of easing
			);
			$('.dashboard .dashboard-inner').hide("fast");
	
	});

	// Show the Right Dashboard
	$('.dashboard-tab-link').click(function() {


			if( $('.dashboard-inner').width()  == 375 ) {
				$('.dashboard').removeClass("show");
				$('.dashboard .dashboard-inner').animate(
					{ marginRight: -375 , width: 0 }, // what we are animating
					'slow', // how fast we are animating
					'easeOutSine' // the type of easing
				);
				$('.dashboard .dashboard-inner').hide("fast");
	
			}
			else{
				$('.dashboard .dashboard-inner').show("fast");
				$('.dashboard').addClass("show");
				$('.dashboard .dashboard-inner').animate(
					{ marginRight: 0 , width: 375}, // what we are animating
					'slow', // how fast we are animating
					'easeOutSine' // the type of easing
				);	
				
				}

		hideErrorBox();
	});
});

/**
 * Hide Error Messages On homepage when you trigger an action (customer-module-body)
 */
function hideErrorBox(){
	var errorMsgs = $('#customer-module-body div.error-box');
	if (errorMsgs.length > 0) {
		$.each(errorMsgs, function(i, errorMsg){
			$(errorMsg).addClass('hidden');
		});
	}
}



