(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','../../www.google-analytics.com/analytics-1.js'/*tpa=https://www.google-analytics.com/analytics.js*/,'ga');
ga('create', 'UA-3514911-1', 'https://www.charter.com/js/charter.com');
ga('send', 'pageview');