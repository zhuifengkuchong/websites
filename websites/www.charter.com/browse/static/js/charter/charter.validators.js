var CHARTER = typeof CHARTER === 'undefined' ? {} : CHARTER;

CHARTER.SPEED = {
    'fast': 200,
    'normal': 500,
    'slow': 1000
};

CHARTER.VALIDATORS = {
    'has-value': function hasValue(value) {
        return value.length > 0;
    },
    'numeric': function isNumeric(value) {
        value = value.replace(/(\-|\.|\s|\(|\))/g, '');
        return !isNaN(value) && !(value % 1 !== 0);
    },
    'exact-length': function isExactLength(value, length) {
        value = value.replace(/(\-|\.|\s|\(|\))/g, '');
        return value.length === Number(length);
    },
    'range-length': function isRangeLength(value, length, maxLength) {
        return value.length >= Number(length) && value.length <= Number(maxLength);
    },
    'not-equal-to': function isNotEqualTo(value, notValue) {
        return value != notValue;
    },
    'email': function isEmail(value) {
    	var regEx = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; 
        return regEx.test(value);
    },
    'phone': function isPhone(value) {
        return /\(?\d{3}[)-.]?\d{3}[-.]?\d{4}/.test(value);
    	//return /^\d{10}$/.test(value);
    },
    'mobile': function isMobile(value) {
    	return /^\d{10}$/.test(value);
    },
    'URL': function isURL(value) {
        return /(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/.test(value);
    },
    'block-security-char': function blockSecurityChar(value) {
        var matchedHyphenCount = null;
        var matchedSingleQuotationCount = null;
        if(value != ''){
            matchedSingleQuotationCount = value.match(/'/g);
             if(matchedSingleQuotationCount != null && matchedSingleQuotationCount.length >=2){
                 return false; 
             }
        }
        if(value != ''){
            matchedHyphenCount = value.match(/(\--)/g);
            if(matchedHyphenCount != null && matchedHyphenCount.length >=1){
                 return false; 
            }
        }
        return (!(/[<|>|\\|"]/.test(value)));
    },
    'address-white-list': function addressWhiteList(value) {
    	return /^[\.\w\s\'#-]*$/.test(value);
    },
    'equal-to-field-value': function isEqualToFieldValue(value, otherField) {
    	return value == $('#' + otherField).val();
    }
};

CHARTER.processErrorLabel = function($element, silent, valid) {
	if (!silent && (typeof $element.attr('data-label') !== 'undefined')) {
		$label = $('label#' + $element.attr('data-label'));
		if (valid) {
			$label.removeClass('error');
		} else {
			$label.addClass('error');
		}
	}
};

CHARTER.processSpecificErrorLabel = function($label, silent, valid) {
	if (!silent) {
		if (valid) {
			$label.removeClass('error');
		} else {
			$label.addClass('error');
		}
	}
};

/**
 * Perform specified validation.
 * 
 * @param checker the validation method to be used.
 * @param checkerArguments array of arguments to be used by the specified validation method.
 * @param $errorMsgDiv optional JQuery object representing the error element to be shown.
 * @param silent when set to true, do not affect the error display element in any way, i.e. don't hide it if it's shown, don't show it if it's hidden.
 * @return true if field considered valid, false otherwise.
 */
CHARTER.validateField = function(checker, checkerArguments, $errorMsgDiv, silent) {
	var valid = checker.apply(this, checkerArguments);
	if (!silent && !valid && $errorMsgDiv) {
		$errorMsgDiv.removeClass('hidden');
	}
	return valid;
};

/**
 * Validate that the specified element has a non-empty value if it has the 'required' class.
 * 
 * @param $element JQuery selector representing the element to be validated.
 * @param silent when set to true, do not affect the error display element in any way, i.e. don't hide it if it's shown, don't show it if it's hidden.
 * @return true if field considered valid, false otherwise.
 */
CHARTER.validateRequired = function($element, silent) {
	if ((typeof $element === 'undefined') || ($element === null) || ($element.size() == 0)) {
		// nothing to do, element to be validated doesn't exist, return as valid
		return true;
	}	
	
	var $requiredErrMsgDiv = $('div#' + $element.attr('data-errMsgPrefix') + '-requiredErrMsg');

	if (!silent) {
	    $requiredErrMsgDiv.addClass('hidden');
	}

	var valid = true;

	if ($element.hasClass('required')) {
		valid = CHARTER.validateField(CHARTER.VALIDATORS['has-value'], [$element.val()], $requiredErrMsgDiv, silent);
	}

	return valid;
};

/**
 * Validate that the specified element does not contain unsupported characters.
 * 
 * @param $element JQuery selector representing the element to be validated.
 * @param silent when set to true, do not affect the error display element in any way, i.e. don't hide it if it's shown, don't show it if it's hidden.
 * @return true if field considered valid, false otherwise.
 */
CHARTER.validateUnsupportedCharacters = function($element, silent) {
	if ((typeof $element === 'undefined') || ($element === null) || ($element.size() == 0)) {
		// nothing to do, element to be validated doesn't exist, return as valid
		return true;
	}

	var $formatErrMsgDiv = $('div#' + $element.attr('data-errMsgPrefix') + '-formatErrMsg');
	
	if (!silent) {
	    $formatErrMsgDiv.addClass('hidden');
	}
	
	// do required check first
	var valid = CHARTER.validateRequired($element, silent);

	// only check if it's still considered valid
	if (valid) {
		valid = CHARTER.validateField(CHARTER.VALIDATORS['block-security-char'], [$element.val()], $formatErrMsgDiv, silent);
	}

	CHARTER.processErrorLabel($element, silent, valid);
	
	return valid;
};

/**
 * Validate that the specified element is a required input and has a valid e-mail format.
 * 
 * @param $element JQuery selector representing the element to be validated.
 * @param silent when set to true, do not affect the error display element in any way, i.e. don't hide it if it's shown, don't show it if it's hidden.
 * @return true if field considered valid, false otherwise.
 */
CHARTER.validateEmail = function($element, silent) {
	if ((typeof $element === 'undefined') || ($element === null) || ($element.size() == 0)) {
		// nothing to do, element to be validated doesn't exist, return as valid
		return true;
	}

	var $formatErrMsgDiv = $('div#' + $element.attr('data-errMsgPrefix') + '-formatErrMsg');
	
	if (!silent) {
		// reset error message before validation
	    $formatErrMsgDiv.addClass('hidden');
	}

	// do required check first
	var valid = CHARTER.validateRequired($element, silent);

	// only check if it's still considered valid
	if (valid) {
		valid = CHARTER.validateField(CHARTER.VALIDATORS['email'], [$element.val()], $formatErrMsgDiv, silent);
	}

	CHARTER.processErrorLabel($element, silent, valid);

	return valid;
};


/**
 * Validate that the specified element is a required input and is a valid zip code.
 * 
 * @param $element JQuery selector representing the element to be validated.
 * @param silent when set to true, do not affect the error display element in any way, i.e. don't hide it if it's shown, don't show it if it's hidden.
 * @return true if field considered valid, false otherwise.
 */
CHARTER.validateZip = function($element, silent) {
	if ((typeof $element === 'undefined') || ($element === null) || ($element.size() == 0)) {
		return true;
	}

	var $formatErrMsgDiv = $('div#' + $element.attr('data-errMsgPrefix') + '-formatErrMsg');

	if (!silent) {
	    $formatErrMsgDiv.addClass('hidden');
	}

	var valid = CHARTER.validateRequired($element, silent);
	
	// only check if it's still considered valid
	if (valid) {
		valid = CHARTER.validateField(CHARTER.VALIDATORS['numeric'], [$element.val()], $formatErrMsgDiv, silent);
	}

	// only check if it's still considered valid
	if (valid) {
		valid = CHARTER.validateField(CHARTER.VALIDATORS['exact-length'], [$element.val(), 5], $formatErrMsgDiv, silent);
	}

	CHARTER.processErrorLabel($element, silent, valid);

	return valid;
};
