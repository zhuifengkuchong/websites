$(document).ready(function(){
	
	/**
	 * TOOL TIPS
	 *
	 * To avoid - Uncaught TypeError: Object [object Object] has no method 'tooltip'
	 * on browse page.
	 *
	 * $(element).length validates if the object exists.
	 */
	/* TOOLTIPS  */
	if ($('.tooltip').length > 0) {
		$('.tooltip').tooltip();
	}
	
});

(function($) {
	$.fn.tooltip = function tooltip(args) {
		var defaults = {
		},
			args = $.extend(defaults, args);
		return this.each(function(i, item) {
			var trigger = {
					'$element': $(item)
				},
			tooltip = {
					'$element': $(trigger.$element.attr('href')).append('<div class="tooltip-pointer" />')
				},
				_init = function _init() {
					tooltip.width = tooltip.$element.outerWidth();
					tooltip.height = tooltip.$element.outerHeight();
					trigger.width = trigger.$element.outerWidth();
					trigger.height = trigger.$element.outerHeight();
					trigger.left = trigger.$element.offset().left;
					trigger.top = trigger.$element.offset().top;
//					console.log(tooltip.height);
					switch(pointerPosition) {
						case 'left':
							tooltip.$element.css({
								'left': trigger.left - tooltip.width - 15,
								'top': trigger.top - tooltip.height / 2 + trigger.height / 2,
							});
							tooltip.$pointer.css({
								'top': (tooltip.height / 2 - tooltip.$pointer.outerHeight() / 2)
							});
							break;
						case 'right':
							tooltip.$element.css({
								'left': trigger.left + trigger.width + 15,
								'top': trigger.top - tooltip.height / 2 + trigger.height / 2 - 5,
							});
							tooltip.$pointer.css({
								'top': (tooltip.height / 2 - tooltip.$pointer.outerHeight() / 2)
							});
							break;
						case 'top':
							tooltip.$element.css({
								'left': trigger.left - tooltip.width / 2 + trigger.width / 2 + 4,
								'top': trigger.top - tooltip.height - 15
							});
							break;
						case 'bottom':
							tooltip.$element.css({
								'left': trigger.left - tooltip.width / 2 + trigger.width / 2 + 4,
								'top': trigger.top + trigger.height + 15
							});
							break;
						default:
					}						
				}

			if (tooltip.$element.length !== 1) {
				return;
			}

//			var pointerPosition = tooltip.$element.attr('class').replace(/\s*tooltip-content\s*/, ''),
			var pointerPosition = tooltip.$element.attr('data-position'),
				$otherTooltips = $('.tooltip-content');

			if (pointerPosition === '') {
				pointerPosition = 'right';
				tooltip.$element.addClass('right');
			}

			tooltip.$pointer = tooltip.$element.find('.tooltip-pointer');
			tooltip.$element.addClass(pointerPosition).appendTo('body');

			trigger.$element.bind('click', function(e) {
				_init();
				var $this = $(this);
				
				$otherTooltips.removeClass('visible z-top');
				tooltip.$element.addClass('visible z-top');
				
				//To allow the user to enter his information since the default behaviour is to close the tooltip on document click				
				$(document).click(function(event) {
					if($("#retrieve-cart").is(":visible"))
					{
						if(($(event.target).parents().index($("#retrieve-cart")) == -1))
            				tooltip.$element.removeClass('visible');
					}
					else
					{
						if(($(event.target).parents().index($("#save-cart")) == -1))
            				tooltip.$element.removeClass('visible');
					}
				});
				
				$(window).bind('resize', function(e) {
					tooltip.$element.removeClass('visible');
					_init();
				});
				
				e.preventDefault();
				e.stopPropagation();
			});
		});

	};
})(jQuery);