/**
 *This file intended prior to display the processing overlay while localising customer address.
 *It calls functions addModal, showProcessingOverlay and coverPageWhenModalDisplayed, found in processing_overlay.js 
 */
$(document).ready(function() { 
	bindLocalizationForm("browse-localize-form-address","form-address-submit");
	bindSubmitButton();
});

function bindSubmitButton(){
	// home page localization
    $('#browse-localize-form-address').validator({
        'onValidateOK': function() {
        	$('#form-address-submit').data('isValidateOk', 'true');
        },
        'onValidateError': function() {
        	$('#form-address-submit').data('isValidateOk', 'false');
        },
	    'submitButtonId': 'form-address-submit'
    });
	
	$('#form-address-submit').on('click', function(){
		var isValidateOk = $(this).data('isValidateOk');
		$("#customer-module-body .error-box").remove();
		
		if(isValidateOk === 'true') {
			showLoadingIndicator(true);
		} else {
			$('#browse-localize-form-address').find('input[isEmptyField]').remove();
			initLocalizationJavascript("browse-localize-form-address", true);
			
			// don't continue and submit, return
			return false;
		}
	});
}

/**
 * Method to bind the find deals button for ajax submit.
 */
function bindLocalizationForm(formId, buttonId) {
	
	var $form = $("form#" + formId);

	$form.unbind();
	$form.bind("submit", function () {
		var formNode = $(this);
		
		//Analytics
		utag.link({non_conf_event_flag: 'start localization', localization_type: 'top-right'});
		
		$.ajax({
			type        : 'POST',
			dataType	: 'json',
			cache		: false,
			data		: formNode.serialize(),
			beforeSend  : beforeFormSubmit,	
			success		: processLocalizationResponse,
			error		: setErrorTrackingForAnalytics
		});		
		
		return false;
	});
	
	$('button#' + buttonId).attr('disabled', false);
	
}

function setErrorTrackingForAnalytics(request, status, error){
	utag.link({error_type: request.responseText});
	ajaxErrorDefault(request, status, error);
}

function beforeFormSubmit(data) {
	resetErrors();
}

function resetErrors() {	
	$("div.error-box").hide();
	$("input[type=text].error").each(function() {
		$(this).removeClass("error");
	});
}

/**
 * Call back function for localization form
 */
function processLocalizationResponse(data) {
	var LOCALIZE_CUSTOMER_TABS_FORM_FRAG = "https://www.charter.com/browse/shared/modules/mainnav_localization_form.jsp";
	var params = {};

	for(attribute in data){
    	value = data[attribute];
    	if(value instanceof Array){
    		addEntryToMap(params, attribute+'[]', value);
    	}else{
    		addEntryToMap(params, attribute, value);
    	}
    }
    
	if(data.success) {
		utag.link({non_conf_event_flag: 'localization success'});
		window.location = data.successURL;
	} else {
		if (data.showOrderSubmittedPopup) {
			location.reload(true); //force refresh to show popup on current page
		} else {
			addEntryToMap(params, "skipLocalizationLoadingIndicator", "true");
			$("div#customer-localization-form").load(LOCALIZE_CUSTOMER_TABS_FORM_FRAG, params, function() {
			
				//Set error msg for analytics
				setErrorForAnalyticsTracking($(this));
			
				reBindToolTip();

				// Rebind form fields
				initLocalizationJavascript("browse-localize-form-address", true);
				initLocalizationJavascript("localization-securitycode-form", true);
				initLocalizationJavascript("localization-myaccount-form", true);

				// Rebind localisation form
				bindLocalizationForm("browse-localize-form-address","form-address-submit");
				bindSubmitButton();
			});
			showLoadingIndicator(false);
		}
	}
}

function setErrorForAnalyticsTracking(localizationFormObj){
	var errorMsg = '';
	var errorBoxP = localizationFormObj.find('.error-box p');
	if(errorBoxP.length > 0){
		errorMsg = $(errorBoxP[0]).html();
		utag.link({error_type: errorMsg});
		return;
	}
	
	var errorBoxLi = localizationFormObj.find('.error-box li');
	if(errorBoxLi.length > 0){
		errorMsg = $(errorBoxLi[0]).html();
		utag.link({error_type: errorMsg});
		return;
	}
}

function initLocalizationJavascript(divId, shouldInitPlaceHolders) {

	// Init place holders
	if (shouldInitPlaceHolders) {
		$("form#" + divId).each(function() {
			EmptyValue.binding(this);
		});
	}
}
