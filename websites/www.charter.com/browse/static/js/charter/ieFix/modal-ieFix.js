;(function( $ ) {
	$.ModalWindow = function(contentId) {
		
		var defaults = {
            propertyName: 'value'
        }
		
		var plugin = this;
		
		plugin.settings = {}
		plugin.isVisible = false;
		plugin.instantiated = false;
		
		var _init = function() {
			plugin.modalWindow = $('#modal-window');
			if(plugin.modalWindow.length === 0){
				$('<div id="modal-window"><div id="modal-overlay"></div><div id="modal_content" class="lox-content"></div>').appendTo('body');
			}
			plugin.overlayDiv = $('#modal-overlay');
			plugin.content = $('#modal_content');
			$('#'+contentId).appendTo($('#modal_content'));
		};
		
		plugin.open = function() {
			if(plugin.isVisible){
				return plugin;
			}
			
			if(!plugin.instantiated){
				// Get the screen height and width
				var maskHeight = $(document).height();
				var maskWidth = $(window).width();
				plugin.overlayDiv .css({'width': maskWidth,'height': maskHeight});
				
				plugin.modalWindow.css('z-index', 3000);
				plugin.modalWindow.css('position', 'fixed');
				
				var winH = $(window).height();
				var winW = $(window).width();
				
				plugin.overlayDiv.css('opacity', 0.5);
				plugin.overlayDiv.css('position', 'absolute');
				
				plugin.overlayDiv.addClass('lox-overlay');
				$('#'+contentId).removeClass('off-screen');
				
				plugin.content.css('background', 'none repeat scroll 0 0 #FFFFFF');
				plugin.content.css('padding', '30px 0');
	
				// Set the popup window to center
				plugin.content.css('visibility', 'visible');
				plugin.content.css('opacity', 1);
				plugin.content.css('width', '450px');
				plugin.content.css('height', '145px');
				_resize(winH, winW);
				
				plugin.instantiated = true;
			}
			
			_resize
			// transition effect
			plugin.overlayDiv.fadeIn(1000);
			plugin.overlayDiv.fadeTo("slow", 0.8);
			
			$('#modal-window').removeClass('off-screen');
			plugin.content.fadeIn(2000);
			
			plugin.isVisible = true;
			
			return plugin;
		};
		
		plugin.close = function() {
			$('#modal-window').addClass('off-screen');
			plugin.isVisible = false;
			return plugin;
		};
		
		_init();
		
		var _resize = function(winH, winW){
			/* logic for "too tall, need to scroll */
			if (winW < plugin.content.outerHeight()) {
				plugin.modalWindow.css('position', 'absolute');
				plugin.overlayDiv.css('position', 'fixed');
			} else {
				plugin.modalWindow.css('position', 'fixed');
				plugin.overlayDiv.css('position', 'absolute');
			}
			var left = (winW - plugin.content.width()) / 2;
			var top = winH > plugin.content.height() ? (winH - plugin.content.height()) / 2 : $(document).scrollTop()
			plugin.content.css('top', top);
			plugin.content.css('left', left);
		}
		return plugin;
	}
	
})( jQuery );

(function($) {
	$.fn.outerHTML = function(s) {
		return (s) ? this.before(s).remove() : $('<p>').append(
				this.eq(0).clone()).html();
	}
})(jQuery);