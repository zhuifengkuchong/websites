var GEOLOC = typeof GEOLOC === 'undefined' ? {} : GEOLOC;

GEOLOC.retrieveGeolocationDetails = function() {
		
	var inputData = {
			"atg-rest-depth": "1"
	};

	$.ajax({
		type: 'POST',
		url : '/rest/bean/com/charter/geolocation/facade/GeolocationServiceFacade/retrieveGeolocationDetails',
		contentType : 'application/json',
		async       : true,        
		data        : JSON.stringify(inputData),
        success: function(data) {
        	if (typeof console != "undefined") {
    			var geolocation = data.geolocationServiceResponse;
    			if(geolocation != null){
    				window.console && console.log(JSON.parse(geolocation));
    			}
        	}
        },
		error: function(data) {
			if (typeof console != "undefined") {
				window.console && console.log(data);
        	}
        }
	});
};

GEOLOC.populateAndRetrieveGeolocationDetails = function(ipAddress,resultDiv) {
	
	var inputData = {
			"atg-rest-depth": "1",
			"arg1": ipAddress
	};

	$.ajax({
		type: 'POST',
		url : '/rest/bean/com/charter/geolocation/facade/GeolocationServiceFacade/populateAndRetrieveGeolocationDetails',
		contentType : 'application/json',
		async       : true,        
		data        : JSON.stringify(inputData),
        success: function(data) {
   			var geolocation = data.geolocationServiceResponse;
			if ($('#' + resultDiv) != undefined){
				$('#' + resultDiv).html(geolocation);
			}
        },
		error: function(data) {
			if ($('#' + resultDiv) != undefined){
	          	$('#' + resultDiv).html('Unable to populateAndRetrieveGeolocationDetails');
			}
        }
	});
};

GEOLOC.clearGeolocationDetails = function() {
	
	var inputData = {
			"atg-rest-depth": "1"
	};

	$.ajax({
		type: 'POST',
		url : '/rest/bean/com/charter/geolocation/facade/GeolocationServiceFacade/clearGeolocationDetails',
		contentType : 'application/json',
		async       : true,        
		data        : JSON.stringify(inputData),
        success: function(data) {
        	if (typeof console != "undefined") {
        		window.console && console.log("Geoloc information cleared.");
        	}
        },
		error: function(data) {
			if (typeof console != "undefined") {
				window.console && console.log(data);
        	}
        }
	});
};