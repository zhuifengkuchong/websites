var USEREXPERIENCE = typeof USEREXPERIENCE === 'undefined' ? {} : USEREXPERIENCE;

USEREXPERIENCE.userExperience = null;
/**
 * Utility method to populate User Experience.
 * map like : [{"name":"LeadReferredByUrl__c","value":leadRef},{"name":"CharterPhone","value":charterPhone}]
 */
USEREXPERIENCE.setUserExperienceInfoFromSession = function (map){
		var inputData = {
				"arg1": map
		};

		$.ajax({
			type: 'POST',
			url : '/rest/bean/com/charter/commerce/session/rest/RestUserExperienceFacade/setUserExperienceInfoIntoSession',
			contentType : 'application/json',
			async       : false,        
			data        : JSON.stringify(inputData)
		});
		
}

/**
 * Utility method to populate User Experience.
 */
USEREXPERIENCE.getUserExperienceInfoFromSession = function(){
	
			var inputData = {
					"arg1":['FirstName','LastName','FullName','Company','Phone','Email','ZipCode__c']
			};
			$.ajax({
				type: 'POST',
				url : '/rest/bean/com/charter/commerce/session/rest/RestUserExperienceFacade/getUserExperienceInfoFromSession',
				contentType : 'application/json',
				async       : false,        
				data        : JSON.stringify(inputData),
		        success: function(data) {
		        	USEREXPERIENCE.userExperience = data.atgResponse; 
		        }
			});
}

/**
 * Get specific info from session 
 * @param  array example : ['FirstName','LastName','FullName','Company','Phone','Email','ZipCode__c']
 */
USEREXPERIENCE.getUserExperienceSpecificInfoFromSession = function(keysArray){
	
			var callRestService = false;
			if (USEREXPERIENCE.userExperience != null) {
				for(var i= 0; i < keysArray.length; i++)
				{
					if (!(keysArray[i] in USEREXPERIENCE.userExperience)) {
						callRestService = true;
					}
				}
			}
			
	
			if (callRestService || USEREXPERIENCE.userExperience == null) {
				var inputData = {
						"arg1": keysArray
				};
				$.ajax({
					type: 'POST',
					url : '/rest/bean/com/charter/commerce/session/rest/RestUserExperienceFacade/getUserExperienceInfoFromSession',
					contentType : 'application/json',
					async       : false,        
					data        : JSON.stringify(inputData),
			        success: function(data) {
			        	USEREXPERIENCE.userExperience = data.atgResponse; 
			        }
				});
			}	
}

/**
 * get specific field from lead
 */
USEREXPERIENCE.getSpecificField = function(formSerialized)
{
	var map = [];
	var array = ['FirstName','LastName','FullName','Company','Phone','Email','ZipCode__c'];
	for (var i in formSerialized) {
		var obj = formSerialized[i];
		if ($.inArray(obj['name'],array)) {
			map.push(obj);
		}
	}
	return map;
}

/**
 * Utility method to populate User Experience into forms.
 */
USEREXPERIENCE.populateUserExperience = function(map){
	for (var key in map) {
		$("#"+key).val(map[key])
		// to complete 
	}
}

/**
 * By default, the cookie is deleted when the browser is closed. 
 * example : document.cookie="username=John Doe; expires=Thu, 18 Dec 2013 12:00:00 GMT";
 */ 
USEREXPERIENCE.setCookie = function(cname,cvalue, expires)
{
	if (expires === null || expires === "undefined" || expires === undefined) {
		document.cookie = cname + "=" + cvalue + "; path=/";
	}else {
		document.cookie = cname + "=" + cvalue + "; path=/;expires=" + expires;
	}
	
}
/**
 * function that returns the value of a specified cookie:
 */
USEREXPERIENCE.getCookie = function(cname)
{
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for(var i=0; i<ca.length; i++) 
	{
	  var c = $.trim(ca[i]);
	  if (c.indexOf(name)==0) return c.substring(name.length,c.length);
	}
	return "";
}

/**
 * Deleting a cookie : Just set the expires parameter to a passed date:
 */
USEREXPERIENCE.clearCookie = function(cname)
{
	document.cookie = cname + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT";
}

//set value on the element
USEREXPERIENCE.setElement = function(element,charterPhone)
{
	element.html(charterPhone);
}
