var eventLocalizationInitSent = false;

$(document).ready(function() { 
	EmptyValue.bindingAll();
});

EmptyValue = function(formElement) {
	this.formElement = formElement;
	this.form = $(formElement);
};
EmptyValue.prototype = {
	
	init : function() {
		this.bindingEmptyValue();
	},
	bindingEmptyValue : function() {
		this.form.find("input[emptyValue]").each(function() {
			var field = $(this);
			var defaultValue = field.attr("emptyValue");

			var emptyValueField = $("<input type='text' isEmptyField='true'/>");
			emptyValueField.attr("class", field.attr("class"));
			emptyValueField.attr("style", field.attr("style"));
			emptyValueField.val(defaultValue);
			
			headerLocalizationForm(field, emptyValueField);
			modalLocalizationForm(field, emptyValueField);
			
			field.before(emptyValueField);

			if (field.val() == null || field.val() == "") {
				field.hide();
				emptyValueField.css("display", "inline");
			} else if (field.val() == defaultValue) {
				field.val("");
				field.hide();
				emptyValueField.css("display", "inline");
			} else {
				emptyValueField.hide();
			}
			
			emptyValueField.focus(function() {
				emptyValueField.hide();
				field.show();
				field.focus();
				if (!eventLocalizationInitSent) {
					utag.link({non_conf_event_flag: 'localization initiated'});
					eventLocalizationInitSent = true;
				}
			});

			field.blur(function() {
				var thisField = $(this);
				if (thisField.val() == null || thisField.val() == "") {
					thisField.hide();
					emptyValueField.css("display", "inline");
				} else if (thisField.val() == defaultValue) {
					thisField.val("");
					thisField.hide();
					emptyValueField.css("display", "inline");
				}else{
					emptyValueField.hide();
					thisField.show();
				}
			});
		});
	}
};

/*
 * Only for Localization form on homepage
 */
function headerLocalizationForm(trueField, mockUpField) {
	var attribName = "name";
	var attribId = "id";
	
	if (trueField.attr(attribName) != null && trueField.attr(attribName) != "") {
		
		if ($.inArray(trueField.attr(attribId), ["form-address-address1", "form-address-address2", "form-address-zipcode"]) > -1) {
			if (trueField.attr(attribName) == "address") {
				mockUpField.css("width", "94px");
				mockUpField.css("margin-right", "6px");
			} 
			
			if (trueField.attr(attribName) == "unit") {
				mockUpField.css("width", "42px");		
			}
			
			if (trueField.attr(attribName) == "zip") {
				mockUpField.css("width", "58px");
				mockUpField.css("margin-right", "15px");
			}
		}
	}
}

/*
 * Only for Localization form popup
 */
function modalLocalizationForm(trueField, mockUpField) {
	var attribName = "name";
	var attribId = "id";
	
	//Contains not the specific element ids
	if ($.inArray(trueField.attr(attribId), ["modal-form-address-address1", "modal-form-address-address2", "modal-form-address-zipcode"])  > -1) {
		
		if (trueField.attr(attribName) == "address") {
			mockUpField.css("width", "250px");
			mockUpField.css("margin-right", "36px");
			mockUpField.css("margin-left", "0");
		} 
		
		if (trueField.attr(attribName) == "zip") {
			mockUpField.css("float", "right");			
		}
		
		mockUpField.css("border", "1px solid #7F888F");
		mockUpField.css("border-radius", "3px");
		mockUpField.css("padding", "10px 8px");
	}
}

EmptyValue.binding = function(form) {
	if ($(form).data("wrapper") == null) {
		var emptyValue = new EmptyValue(form);
		emptyValue.init();
	}
};

EmptyValue.bindingAll = function() {
	$("form").each(function() {
		EmptyValue.binding(this);
	});
};

