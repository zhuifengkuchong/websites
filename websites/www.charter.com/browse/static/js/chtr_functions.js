//**************************************************************************************************** 
// Set Default Language (and redirect)
// @param {ISO 639-1 Code} Language Code (en/es) 
// @param {String} URL or JS Href ("https://www.CHARTER.com" / window.location.href)
//**************************************************************************************************** 
function chtrSetLang(langCode,url) {
	var inputData = {};
	if (url=="undefined") {
		inputData["arg1"] = langCode;
		inputData["arg2"] = "";
	} else { 
		inputData["arg1"] = langCode;
		inputData["arg2"] = url;
	};
	$.ajax({
		url : '/rest/bean/com/charter/commerce/services/content/LanguageFacade/switchLanguage',
		contentType : 'application/json',
		async       : false,        
		data        : JSON.stringify(inputData),
		success     : function(data) {
			if (data.atgResponse!=null) {
				if (data.atgResponse==window.location.href){
					window.location.reload();
				} else {
					window.location.href=data.atgResponse;
				}
			}
		}
	});
};
	
//**************************************************************************************************** 
// Get current language
//**************************************************************************************************** 
function chtrGetLang() {
	var inputData = {"atg-rest-depth": "2"};
	var currentLang = "";
	$.ajax({
		url : '/rest/bean/com/charter/commerce/services/content/LanguageFacade/retrieveAvailableLanguages',
		contentType : 'application/json',
		async       : false,        
		data        : JSON.stringify(inputData),
		success     : function(data) {
			currLang = data.currentLanguage;
		}
	});
	return currLang
};

//**************************************************************************************************** 
// Return value of Parameter from Querystring or Session.
// @param {String} Parameter ("v" / "affpn")
//**************************************************************************************************** 
function chtrGetParam(_pname) {
	
	//ignore system parameters
	var ignoreParams = ["class","errors","serviceException","validationErrors","success"];
	
	var qs = $.deparam.querystring();
	var pVal = null;
	
	//check query string first
	if (!$.isEmptyObject(qs)) {
		$.each(qs, function(key, value) {
			if (key===_pname) {
				pVal = value;
			};
		});
	}
	//then check session parameters
	if (pVal==null) {
		$.ajax({
			url: "/rest/bean/com/charter/commerce/services/checkout/AffiliateFacade/retrieveAffiliateDynamicParams",
			type: "POST",
			async: false
		}).done(function(data) {
			$.each(data, function(key, value) {
				if ($.inArray(key, ignoreParams)==-1) {
					if (key===_pname) {
						pVal = value;
					};
				};
			});
		}).fail(function(){
			return false;
		});
	};
	return pVal;
};

//**************************************************************************************************** 
// Check value of Parameter in Querystring or Session. Return ture/false
// @param {String} Parameter ("v" / "affpn") 
// @param {String} Value ("1" / "1-888-123-4567")
//**************************************************************************************************** 
function chtrCheckParam(_pname, _value) {
	
	//ignore system parameters
	var ignoreParams = ["class","errors","serviceException","validationErrors","success"];

	var qs = $.deparam.querystring();
	var pVal = false;
	
	//check query string first
	if (!$.isEmptyObject(qs)) {
		$.each(qs, function(key, value) {
			if ((key===_pname)&&(value===_value)) {
				pVal = true;
			};
		});
	}
	//then check query string
	if (!pVal) {
		$.ajax({
			url: "/rest/bean/com/charter/commerce/services/checkout/AffiliateFacade/retrieveAffiliateDynamicParams",
			type: "POST",
			async: false
		}).done(function(data) {
			$.each(data, function(key, value) {
				if ($.inArray(key, ignoreParams)==-1) {
					if ((key==_pname)&&(value==_value)) {
						pVal = true;
					};
				};
			});
		});
	}
	return pVal;
};


//**************************************************************************************************** 
// Return value of Affilaite Parameter from Session.
// @param {String} Affilaite Flow ID ("1" / "OWEST")
// @param {String} Parameter ("g2bAffilaiteId" / "regions.zipCodes")
//**************************************************************************************************** 
function chtrGetAffParam(_floid, _pname) {
	var affVal = null;
	$.ajax({
		url: "/rest/bean/com/charter/commerce/services/checkout/AffiliateFacade/retrieveAffiliatePropertiesByFlowId",
		type: "POST",
		async: false
	}).done(function(data) {
		$.each(data, function(key, value) {
			if (key===_pname) {
				affVal = value;
			};
		});
	});
	return affVal;
};

//**************************************************************************************************** 
// Check value of Affiliate Parameter in Session. Return ture/false
// @param {String} Affilaite Flow ID ("1" / "OWEST")
// @param {String} Parameter ("g2bAffilaiteId" / "regions.zipCodes")
//**************************************************************************************************** 
function chtrCheckAffParam(_floid, _pname, _value) {
	
	//ignore system parameters
	var ignoreParams = ["class","errors","serviceException","validationErrors","success"];

	var qs = $.deparam.querystring();
	var pVal = false;
	
	//check query string first
	if (!$.isEmptyObject(qs)) {
		$.each(qs, function(key, value) {
			if ((key===_pname)&&(value===_value)) {
				pVal = true;
			};
		});
	}
	//then check query string
	if (!pVal) {
		$.ajax({
			url: "/rest/bean/com/charter/commerce/services/checkout/AffiliateFacade/retrieveAffiliateDynamicParams",
			type: "POST",
			async: false
		}).done(function(data) {
			$.each(data, function(key, value) {
				if ($.inArray(key, ignoreParams)==-1) {
					if ((key==_pname)&&(value==_value)) {
						pVal = true;
					};
				};
			});
		});
	}
	return pVal;
};


//**************************************************************************************************** 
// Localize user on Charter.com by redirecting to localization page
// @param {String} Addr1 ("9798 Hale")
// @param {String} Addr2 ("Apt 2") - Optional
// @param {String} AddrZip ("63123")
// @param {String} AddrV ("1") - Optional
//**************************************************************************************************** 
function chtrLocalize(_addr1, _addr2, _addrzip, _addrv) {
	var chtrAddrQS = "address1=" + _addr1;
	if (_addr2.length > 0) {
		chtrAddrQS += "&apt=" +_addr2;
	};
	chtrAddrQS += "&zip=" + _addrzip;
	if (_addrv.length > 0) {
		chtrAddrQS += "&v=" +_addrv;
	} else {
		chtrAddrQS += "&adp=true";
	};
	window.location.assign("/buyflow/buyflow-localization?" + chtrAddrQS);
};// JavaScript Document


//**************************************************************************************************** 
// Pull in query string variables into localization form as hidden fields
// If no query string, checks form for input field with "v" name, if not, adds it in (for legacy forms)
//**************************************************************************************************** 
$(document).ready(function() {
	var url_vars = $.deparam.querystring();
	var hiddenInputs = '';
	if (url_vars.v) {
		$('input[name="v"]').remove();
		$.each(url_vars, function( key, value ) {
			hiddenInputs += '<input type="hidden" name="'+key+'" value="'+value+'"/>';
		});
		$('form[action="/buyflow/buyflow-localization"]').prepend(hiddenInputs);
		
	} else if ($('input[name="v"]').val() == null) {
		hiddenInputs += '<input type="hidden" name="adp" value="true"/>';
		$('form[action="/buyflow/buyflow-localization"]').prepend(hiddenInputs);
	}
});

	
