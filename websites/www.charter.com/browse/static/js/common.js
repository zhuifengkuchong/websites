var LOADING_MODAL = typeof $.ModalWindow === 'undefined' ? null : LOADING_MODAL;
$(document).ready(function(){
	jQuery.fn.exists = function(){return this.length>0;}
	
	ajaxSetup();
	
	/**
	 * Menu
	 */
	var activeMenu = activeMenuId.replace('MenuItem', ''); 
	var menuLists = $('.main-links li');
	if(menuLists.length > 0){
		$.each(menuLists, function(index, menu){
			var dataMenu = $(menu).attr('data-menu');
			if(dataMenu === activeMenu){
				$(menu).addClass('selected');
			}
		});
		if(typeof(setFaqMenuToSelectedMenu) !== 'undefined') {
			setFaqMenuToSelectedMenu();
		}
	}
	

	/**
	 * HOMEPAGE CAROUSEL
	 */
	$('#home-promos-carousel').jcarousel({
   		vertical: false,
   		scroll: 1,
		wrap: "circular"
	});


	    /** 
     * APP CAROUSEL
     */
    $('#charter-app-carousel').jcarousel({
        vertical: false,
        scroll: 1,
        wrap: "circular",
        initCallback: initCallbackMessageApp,
        itemFirstInCallback: setPaginationApp
    });

    function setPaginationApp(a, b, c, d) {
        var channelName = $($(b).children().get(1)).attr('class').split(' ')[1];
        var currentIcon = $('.app-page-carousel .jcarousel-pagination [class^=' + channelName + ']');
        var icons = $('.app-page-carousel .jcarousel-pagination a');

        var classToSet;
        for (var i = 0; i < icons.length; i++) {
            classToSet = ($(currentIcon).text() == i + 1) ? 'active' : 'inactive';
            $(icons[i]).removeClass('active inactive');
            $(icons[i]).addClass(classToSet);
        }
    }

    function initCallbackMessageApp(carousel) {
        $('.app-page-carousel .jcarousel-pagination a').live('click', function () {
            var classes_arr = $(this).attr('class').split(' ');
            var q, appName;
            var classes_arr_len = classes_arr.length;
            for (q = 0; q < classes_arr_len; q++) {
                if (/icon/.test(classes_arr[q])) {
                    appName = classes_arr[q].split("-icon")[0];
                }
            }
            carousel.scroll(jQuery.jcarousel.intval(jQuery(this).text()));
            return false;
        });
    }

    $('.showApp').click(function () {
        $('.targetApp').hide();
        $('.app' + $(this).attr('target')).show();
        $('.targetAppLarge').hide();
        $('.appLarge' + $(this).attr('target')).show();
    });

    /** 
     * PREMIUM CHANNELS CAROUSEL
     */
    $('#premium-channels-carousel').jcarousel({
        vertical: false,
        scroll: 1,
        wrap: "circular",
        initCallback: initCallbackMessagePremiumChannels,
        itemFirstInCallback: setPaginationPremiumChannels
    });

    function setPaginationPremiumChannels(a, b, c, d) {
        var channelName = $($(b).children().get(1)).attr('class').split(' ')[1];
        var currentIcon = $('.tv-page-carousel .jcarousel-pagination [class^=' + channelName + ']');
        var icons = $('.tv-page-carousel .jcarousel-pagination a');

        var classToSet;
        for (var i = 0; i < icons.length; i++) {
            classToSet = ($(currentIcon).text() == i + 1) ? 'active' : 'inactive';
            $(icons[i]).removeClass('active inactive');
            $(icons[i]).addClass(classToSet);
        }
    }

    function initCallbackMessagePremiumChannels(carousel) {
        $('.tv-page-carousel .jcarousel-pagination a').live('click', function () {
            var classes_arr = $(this).attr('class').split(' ');
            var q, channelName;
            var classes_arr_len = classes_arr.length;
            for (q = 0; q < classes_arr_len; q++) {
                if (/icon/.test(classes_arr[q])) {
                    channelName = classes_arr[q].split("-icon")[0];
                }
            }
            if (channelName) {
                $.bbq.pushState({
                    "channel": channelName

                });
            }
            carousel.scroll(jQuery.jcarousel.intval(jQuery(this).text()));
            return false;
        });

        $("a.channel-popover").live('click', function () {

            var aElementId = $(this).attr('id');
            var channelName = aElementId.replace('-premium-details', '');

            // Get Carousel Item and Number 
            var slideNumber = Number($('#premium-channels-carousel li.jcarousel-item section.' + channelName).parent().attr('jcarouselindex'));

            // Scroll to slideNumber 
            carousel.scroll(slideNumber, 0);

            var initState = ($(this).parents(".premium-channels"));

            initState.fadeOut(300, function () {
                initState.addClass("hidden");
                initState.next(".premium-carousel-cont").fadeIn(1200);
            });

            return false;
        });
    }
    $(window).bind("hashchange", function (e) {
        var hash_fragment = $.deparam.fragment();
        var channelName;
        if (hash_fragment.channel) {
            channelName = hash_fragment.channel;
            $('html, body').animate({
                scrollTop: $("a[name='Premium-Channels']").offset().top
            }, 200, function () {
                $("a#" + channelName + "-premium-details").trigger("click");
            });
        }
    });
    $(window).trigger("hashchange");
	



	/**
	 * PHONE FEATURES CAROUSEL
	 */
	$('#phone-features-carousel').jcarousel({
   		vertical: false,
   		scroll: 1,
		wrap: "circular",
		initCallback: initCallbackMessagePhoneFeatures,
		itemFirstInCallback: setPaginationPhoneFeatures
	});
	
	function setPaginationPhoneFeatures(a,b,c,d){
		// for the pagination feature, we assumed that the images of the feature will always end by the number of the "dot" that should be highlighted.
		// for example:   blablabal-02.png should highlight the second "dot"
		var fileName = $(b).find('img').attr('src').split(".")[0];
		var currentElemNumb = parseInt(fileName.substring(fileName.length - 2, fileName.length));
		var icons = $('.phone-page-carousel .jcarousel-pagination a');

		var classToSet;
		for (var i = 0 ; i < icons.length; i++){
			classToSet = ( currentElemNumb == i+1 ) ? 'active' : 'inactive';
			$(icons[i]).removeClass('active inactive');
			$(icons[i]).addClass(classToSet);
		}
	}
	
	function initCallbackMessagePhoneFeatures(carousel) {
    	$('.phone-page-carousel .jcarousel-pagination a').bind('click', function() {
        	carousel.scroll(jQuery.jcarousel.intval(jQuery(this).text()));
        	return false;
    	});
	};

	/**
	 * PHONE CHANNELS FADE EFFECT
	 */
	$("a#phone-features-link").click(function () {
		var initState = ($(this).parents(".phone-features"));

		initState.fadeOut(300, function () {
			initState.addClass("hidden");
			initState.next(".features-carousel-cont").fadeIn(1200);
		});

		return false;
	});
	
	
	/**
	 * TV WHY CHARTER CAROUSEL
	 */
	$('#why-charter-carousel').jcarousel({
   		vertical: false,
   		scroll: 1,
		wrap: "circular"
	});
	
	/**
	 * TV WHY CHARTER CAROUSEL FADE EFFECT
	 */
	$("a#tv-why-carousel").click(function () {
		var initState = ($(this).parents(".why-charter"));
		
		initState.fadeOut(300, function () {
			initState.addClass("hidden");
			initState.next(".why-charter-carousel-cont").fadeIn(1200);
		});
		
		return false;
	});


	/**
	 * COMPARE INTERNET CAROUSEL
	 */
	$('#compare-internet-carousel').jcarousel({
   		vertical: false,
   		scroll: 1,
		wrap: "circular"
	});

	/**
	 * COMPARE INTERNET FADE EFFECT
	 */
	$("a.speed-compare-popover").click(function () {

		var aElementId = $(this).attr('id');
		var sectionName = aElementId.replace('-details', '');

		// Get Carousel Item and Number
		var slideNumber = $('#compare-internet-carousel li.jcarousel-item section.' + sectionName + '-flash').parent().attr('jcarouselindex');

		// Scroll to slideNumber
		$('#compare-internet-carousel').data('jcarousel').scroll(Number(slideNumber));

		var initState = ($(this).parents(".internet-speed"));

		initState.fadeOut(300, function () {
			initState.addClass("hidden");
			initState.next(".compare-carousel-cont").fadeIn(1200);
		});

		return false;
	});


	/**
	 * DASHBOARD
	 */

	$('.dashboard .dashboard-inner').hide("fast");

	// Hide the Right Dashboard
	$('.dashboard a.close').click(function() {
	  		$('.dashboard').removeClass("show");
		  	$('.dashboard .dashboard-inner').animate(
				{ marginRight: -375 , width: 0 }, // what we are animating
			    'slow', // how fast we are animating
			    'easeOutSine' // the type of easing
			);
			$('.dashboard .dashboard-inner').hide("fast");
	
	});

	// Show the Right Dashboard
	$('.dashboard-tab-link').click(function() {


			if( $('.dashboard-inner').width()  == 375 ) {
				$('.dashboard').removeClass("show");
				$('.dashboard .dashboard-inner').animate(
					{ marginRight: -375 , width: 0 }, // what we are animating
					'slow', // how fast we are animating
					'easeOutSine' // the type of easing
				);
				$('.dashboard .dashboard-inner').hide("fast");
	
			}
			else{
				$('.dashboard .dashboard-inner').show("fast");
				$('.dashboard').addClass("show");
				$('.dashboard .dashboard-inner').animate(
					{ marginRight: 0 , width: 375}, // what we are animating
					'slow', // how fast we are animating
					'easeOutSine' // the type of easing
				);	
				
				}

		hideErrorBox();
	});

	/**
	 * TV HD & DVR FADE EFFECT
	 */
	$("a#high-standard-definition").click(function () {
		var initState = ($(this).parents(".select-topic"));

		initState.fadeOut(300, function () {
			initState.addClass("hidden");
			initState.next(".high-standard-definition").fadeIn(1200);
		});

		return false;
	});


	/**
	 * BUNDLES ACCORDION BACKGROUND COLORS ALTERNATING
	 */
	$(".storeFront .package-list .package:even").css("background-color", "#E5EFF5");
	$("#compare tr:odd").css("background-color", "#E5EFF5");


	/**
	 * SLIDETOGGLE ACCORDION STOREFRONT
	 */
	$('a.toggle').click(function () {

        var contentID = $(this).parents(".summary").find("li.detail");
        if (!contentID.is(":visible")) {
			var ahref = contentID.parent().find("a.toggle")
			ahref.text('LESS DETAIL');
			ahref.addClass('grey-small-point-up');
			ahref.removeClass('grey-small-point-down');

        } else {
			var ahref = contentID.parent().find("a.toggle")
			ahref.text('SEE DETAIL');
			ahref.removeClass('grey-small-point-up');
			ahref.addClass('grey-small-point-down');
        }

        contentID.slideToggle("slow", function () {
            if (contentID.is(":visible")) {
				contentID.css("display","inline-block");
            }
        });
		return false;
    });

	/**
	 * 	Bundle Accordeon
	 *  Click Event on First (only displayed when not expanded) .trigger
	 */
    $('div.trigger').on('click', function () {

    	// Display Current Li Details
    	var currentLi = $(this).parent().parent();

    	currentLi.nextAll('li').removeClass('expanded').removeClass('clear-after');
    	currentLi.nextAll('li').find('.full-content, .price').hide();
    	currentLi.nextAll('li').find('.minified').show();
    	currentLi.nextAll('li').find('.trigger').not('.expanded').show();
    	currentLi.nextAll('li').find('.trigger.expanded').hide();
    	currentLi.nextAll('li').find('.price-block').removeClass('.expanded');
    	currentLi.nextAll('li').find('.fader').removeClass('go');

    	currentLi.prevAll('li').removeClass('expanded').removeClass('clear-after');
    	currentLi.prevAll('li').find('.full-content, .price').hide();
    	currentLi.prevAll('li').find('.minified').show();
    	currentLi.prevAll('li').find('.trigger.expanded').hide();
    	currentLi.prevAll('li').find('.trigger').not('.expanded').show();
    	currentLi.prevAll('li').find('.price-block').removeClass('.expanded');
    	currentLi.prevAll('li').find('.fader').removeClass('go');

    	currentLi.parent().addClass('expanded');
    	currentLi.find('.minified').hide();
    	currentLi.find('div.fader').addClass('go');
    	currentLi.find('.trigger.expanded').show();
    	currentLi.find('.trigger').not('.expanded').hide();
    	currentLi.find('.price-block').addClass('expanded');
    	currentLi.find('.full-content, .price').show();
    	currentLi.addClass('clear-after');
    	currentLi.addClass('expanded');

		var maxHeight = -1;
    	currentLi.each(function() {
    		//console.log($(this).find('.full-content.copy-area'));
		    var h = $(this).find('.full-content.copy-area').outerHeight(true);
		    //console.log('Height = ' + h);
		    maxHeight = (h > maxHeight ? h : maxHeight);
		});
		$('li.section').css('height', maxHeight + 'px');

		return false;
    });


	/**
	 * 	Bundle Accordeon
	 *  Click Event on Second (only displayed when expanded) .trigger
	 */
	$('.trigger.expanded').on('click', function () {
		//console.log('Inside');

    	// Reset all sections
    	$('li.section').find('.full-content, .price').show();
    	$('li.section').find('.minified').hide();
    	$('li.section').find('.copy-area').hide();
    	$('li.section div.fader').removeClass('go');
    	$('li.section').find('.trigger.expanded').hide();
    	$('li.section').find('.trigger').not('.expanded').show();

    	$('li.section').find('.price-block').removeClass('expanded');
        $('li.section').removeClass('expanded').removeClass('clear-after');
        $('ul.squeezebox').removeClass('expanded');

    	var maxHeight = -1;
		$("li.section").each(function() {
		    var h = $(this).find('.fader').outerHeight() + $(this).find('.price-block').outerHeight() + 30;
		    maxHeight = h > maxHeight ? h : maxHeight;
		});
		$('li.section').css('height', maxHeight + 'px');

	});

	/**
	 * SLIDETOGGLE DISCLAIMER
	 */
	$('.disclaimer a.link').click(function () {
        var contentID = $(this).next(".disclaimer-accordion");

		if (!contentID.is(":visible")) {
			var ahref = contentID.parent().find("https://www.charter.com/browse/static/js/a.link")
			ahref.text('Hide disclaimer');

        } else {
			var ahref = contentID.parent().find("https://www.charter.com/browse/static/js/a.link")
			ahref.text('Show disclaimer');
        }

		contentID.slideToggle("slow", function () {
		});
		return false;
    });

	/**
	 * POPUP OVERLAYS
	 *
	 * To avoid - Uncaught TypeError: Object [object Object] has no method 'lox'
	 * on browse page.
	 *
	 * $(element).length validates if the object exists.
	 */
	if ($('.modal').length > 0) {
		$('.modal').lox();
	}

	/**
	 * COMPARE TV OPTIONS MODAL
	 *
	 * To overwrite the generic lox style for the compare tv options modal
	 */
	$('.compare-tv-link').click(function () {
        $(".lox-content").addClass("compare-tv-modal");
		return false;
    });
	
    /**
	 * SPORT SECTION ANIMATION
	 */
	$('.thumbnail').mouseover(
       function(){
		   $(this).removeClass('anim-contract') &
		   $(this).addClass('anim-expand') &
		   $(this).children('.caption').removeClass('anim-fadeout') &
		   $('.caption').addClass('anim-fadein');
		   }
	)
	$('.thumbnail').mouseout(
       function(){
		   $(this).removeClass('anim-expand') &
		   $(this).addClass('anim-contract') &
		   $('.caption').removeClass('anim-fadein') &
		   $('.caption').addClass('anim-fadeout')
	   }
	)
	
	/**
	 * defect 94514 [HPQC DEFECT] #3739 TV, Sports section icons are not aligned in Chrome
	 */
	if (Boolean($('div#tv-page').length)) {
		//Make sure we are in the tv-page. Otherwise do not execute this commands.
		$('.thumbnail').children('.caption').addClass('anim-fadeout');
	}
	
		if (Boolean($('#charter-app-carousel .equalize-col').length)) {
        $("#charter-app-carousel .equalize-col").equalHeights(410, 1000);
    }

	/**
	 * EQUALIZE COLUMNS
	 */
	if ( Boolean($('#premium-channels-carousel .equalize-col').length)) {
		$("#premium-channels-carousel .equalize-col").equalHeights(410, 1000);
	}
	
	/**
	* BACK TO TOP BUTTON FUNCTIONALITY
	*/
	
	$('.back-to-top-btn').click(function () {
		$("html, body").animate({ scrollTop: 0 }, "300");
	});
	

	
	/**
	 * Defect 94546 [HPQC DEFECT] #3764 Fix tab index on hidden panel 
	 */
	if (Boolean($('div.dashboard').length)) {
		$('div.dashboard').find('a').each(function() {
			this.tabIndex = -1;
		});
	}	
	
	/**
	 * Fade effect on scroll for the "Back to Top" button & white bar in second nav
	 */
	$(window).scroll(function () {
    	$(".back-to-top-block").each(function () {
        	if ($(window).scrollTop() < 600) 
			{	
				$(this).stop().fadeTo('fast', 0);
				$(this).stop().fadeTo('fast', 0);
				$(".back-to-top-block a").css("cursor", "default");
			}
			else 
			{
           	 	$(this).stop().fadeTo('fast', 1);
				$(".back-to-top-block a").css("cursor", "pointer");
			}
    	});
	});
	
	var $gotoBundlerBuilderLink = $("a[id^='gotoBundleBuilderLink_']");
	
	if ($gotoBundlerBuilderLink.exists()) {
		$gotoBundlerBuilderLink.bind("click", function() {
			$("form#goto-bundle-builder-form").submit();
		});
	}
	
});

/**
 * Setup common AJAX values
 */
function ajaxSetup() {
	$.ajaxSetup({
		dataType : 'json',
		type : 'POST',
		cache : false,
		error: ajaxErrorDefault
	});
}


/**
 * Default method to call when ajax error occurs.
 * @param data
 */
function ajaxErrorDefault(request, status, error) {
	if (request.status == 0) {
		return;
	}
	
	if (status == "parsererror") {
		window.console && console.log("Error while parsing data: " + request.statusText + " (" + request.status + ", " + error + "\n" + request.responseText + ")");
		return;
	}

	// default behavior is to redirect to session expired page
	var ajaxErrorDefaultRedirectURL = typeof expiration_referral_url === 'undefined' ? '/?expiration=session' : expiration_referral_url;
	window.console && console.log("AJAX error when calling " + this.url + ": " + request.statusText + " (" + request.status + ", " + error + ") then redirecting to " + ajaxErrorDefaultRedirectURL);
	window.location = ajaxErrorDefaultRedirectURL;
}



//**********************************************************************************************
//ATG specific functions
//**********************************************************************************************

/**
* Update all session confirmation number.
*/
var dynSessConf="";
function updateDYNSessConf() {
	if(dynSessConf==""){
	    $.ajax({
	        url: '/rest/bean/com/charter/rest/RestSession/retrieveSessionConfirmationNumber',
	        async : false,
	        success: function(data) {
	            dynSessConf=data.atgResponse
	            populateDynSessConf();
	        }
	    });
	}else{
		populateDynSessConf();
	}
}

var populateDynSessConf = function() {
 $('input[name=_dynSessConf]').val(dynSessConf);
}


/**
 * Hide Error Messages On homepage when you trigger an action (customer-module-body)
 */
function hideErrorBox(){
	var errorMsgs = $('#customer-module-body div.error-box');
	if (errorMsgs.length > 0) {
		$.each(errorMsgs, function(i, errorMsg){
			$(errorMsg).addClass('hidden');
		});
	}
}

/**
* Simple method that adds a value to a map if the value is defined.
*/
function addEntryToMap(map, key, value) {
	if (typeof value != 'undefined') {
    	map[key] = value;
	}
}

function showLoadingIndicator(show) {
	if(LOADING_MODAL === undefined || LOADING_MODAL === null){
		LOADING_MODAL = new $.ModalWindow('loading-indicator');
	}
	if(show){
		LOADING_MODAL.open();
	} else {
		LOADING_MODAL.close();
	}
}

function reBindToolTip(){
	if ($('.tooltip').length > 0) {
		$('.tooltip').tooltip();
	}
}
