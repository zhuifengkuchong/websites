(function($) {
	$.fn.lox = function(args) {
		var defaults = {
				selector: null,
				speed: 500,
				type: 'inline',
				width: '50%',
				height: '50%'
			},
			args = $.extend(defaults, args);
			return this.each(function(_i, _item) {
				var $this = $(_item),
					$obj = args.selector == null ? $($this.attr('href')) : $(args.selector),
					isInitialized = Boolean($('div#lox').length),
					isVisible = false,
					$lox = $('#lox').length > 0 ? $('#lox') : $('<div id="lox" class="off-screen" />').appendTo('body'),
					$overlay = $('.lox-overlay').length > 0 ? $('.lox-overlay') : $('<div class="lox-overlay" />').appendTo($lox),
					$content = $('.lox-content').length > 0 ? $('.lox-content') : $('<div class="lox-content" />').appendTo($lox),
					$close = $('.lox-close').length > 0 ? $('.lox-close') : $('<span class="lox-close" />').appendTo($content),
					dimensions,
					_init,
					_show,
					_hide,
					_resize,
					_updateDimensions,
					contentType = $this.attr('data-modal-type') || 'inline';

				_updateDimensions = function() {
					if (contentType === 'iframe') {
					}
					dimensions = {
						w: $obj.outerWidth(),
						h: $obj.outerHeight()
					};
				};
				
				$this.bind('click', function(e) {
					$(window).unbind('resize').bind('resize', function() {
						if (!isVisible && isInitialized) {
							return false;
						}
						_resize();
					});
					$overlay.unbind('click').bind('click', function() {
						_hide();
					});
					$close.unbind('click').bind('click', function(e) {
						_hide();
						e.preventDefault();
					});
					$(document).unbind('keydown').bind('keydown', function(e) {
						if (e.which == 27) {
							_hide();
						}
					});
					if (contentType === 'iframe') {
						$obj = $('<iframe src="' + $this.attr('href') + '" class="modal-iframe" />').appendTo($content);
						_resize();
						$content.css('visibility', 'visible');
						_show();
					} else if (contentType === 'ajax') {
						var url = $(this).attr('href');

						$obj = $content.load(url, function() {
							$content.addClass('lox-inner-content');
							$content.width(args.width);
							$content.height(args.height);
							if (args.onLoad)
								args.onLoad();
							_resize();
							$content.css('visibility', 'visible');
							_show();							
						});
					} else if (contentType === 'inline') {
						$obj
							.appendTo($content, function() {
								_updateDimensions();
							})
							.removeClass('off-screen');
						if (args.onLoad)
							args.onLoad();
						_resize();
						$content.css('visibility', 'visible');
						_show();
					} else {
						alert('There was an error, please try again.');
					}
					e.preventDefault();
				});
				_show = function() {
					$lox.removeClass('off-screen');
					$overlay.css({
						width: $(window).width(),
						height: $(document).height()
					}).animate({
						opacity: 0.5
					}, args.speed);
					$content.animate({
						opacity: 1
					}, args.speed, function() {
						if (args.onShow)
							args.onShow();
					});
					isVisible = true;
				};
				_hide = function() {
					$overlay.animate({
						opacity: 0
					}, args.speed);
					$content.animate({
						opacity: 0
					}, args.speed, function() {
						$lox.addClass('off-screen');
						$overlay.css('position', 'absolute');
						if (contentType === 'ajax' || contentType === 'iframe') {
							$obj.remove();
						} else {
							$obj
								.addClass('off-screen')
								.appendTo('body');
						}
						if (args.onHide) {
							args.onHide($obj);
						}
					});
					isVisible = false;
				};
				_resize = function() {
					_updateDimensions();
					$overlay.css({
						width: $(window).width(),
						height: $(document).height()
					});
					/* logic for "too tall, need to scroll */
					if ($(window).height() < dimensions.h) {
						$lox.css('position', 'absolute');
						$overlay.css('position', 'fixed');
					} else {
						$lox.css('position', 'fixed');
						$overlay.css('position', 'absolute');
					}
					$content.animate({
						width: dimensions.w,
						height: dimensions.h,
						left: ($(window).width() - dimensions.w) / 2,
						top: $(window).height() > dimensions.h ? ($(window).height() - dimensions.h) / 2 : $(document).scrollTop()
					});
		
				};
				$.fn.lox.show = function() {
					_show();
				}
				$.fn.lox.hide = function() {
					_hide();
				}
				$.fn.lox.resize = function() {
					_resize();
				}

			return this;
		});
	};
})(jQuery);