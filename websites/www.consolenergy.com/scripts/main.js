$(function () {

    //When page loads...
    $(".video-content-1").hide(); //Hide all content
    $("ul.tabs-1 li:first ").addClass("active").show(); //Activate first tab
    $(".video-content-1:first").show(); //Show first tab content

    //On Click Event
    $("ul.tabs-1 li").click(function () {

        $("ul.tabs-1 li").removeClass("active"); //Remove any "active" class
        $(this).addClass("active"); //Add "active" class to selected tab
        $(".video-content-1").hide(); //Hide all tab content

        var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
        $(activeTab).fadeIn(500); //Fade in the active ID content
        return false;
    });

    //When page loads...
    $(".video-content-2").hide(); //Hide all content
    $("ul.tabs-2 li:first ").addClass("active").show(); //Activate first tab
    $(".video-content-2:first").show(); //Show first tab content

    //On Click Event
    $("ul.tabs-2 li").click(function () {

        $("ul.tabs-2 li").removeClass("active"); //Remove any "active" class
        $(this).addClass("active"); //Add "active" class to selected tab
        $(".video-content-2").hide(); //Hide all tab content

        var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
        $(activeTab).fadeIn(500); //Fade in the active ID content
        return false;
    });

    //When page loads...
    $(".video-content-3").hide(); //Hide all content
    $("ul.tabs-3 li:first ").addClass("active").show(); //Activate first tab
    $(".video-content-3:first").show(); //Show first tab content

    //On Click Event
    $("ul.tabs-3 li").click(function () {

        $("ul.tabs-3 li").removeClass("active"); //Remove any "active" class
        $(this).addClass("active"); //Add "active" class to selected tab
        $(".video-content-3").hide(); //Hide all tab content

        var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
        $(activeTab).fadeIn(500); //Fade in the active ID content
        return false;
    });

    //When page loads...
    $(".video-content-4").hide(); //Hide all content
    $("ul.tabs-4 li:first ").addClass("active").show(); //Activate first tab
    $(".video-content-4:first").show(); //Show first tab content

    //On Click Event
    $("ul.tabs-4 li").click(function () {

        $("ul.tabs-4 li").removeClass("active"); //Remove any "active" class
        $(this).addClass("active"); //Add "active" class to selected tab
        $(".video-content-4").hide(); //Hide all tab content

        var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
        $(activeTab).fadeIn(500); //Fade in the active ID content
        return false;
    });


/************************************************************
Expand/Collapse List
************************************************************/
    $(document).ready(function () {
		$('.expand-collapse > li > a').click(function () {
            var navIndex = $('.expand-collapse > li > a').index(this);
            $('.expand-collapse li ul').slideUp();
            if ($(this).next().is(":visible")) {
                $(this).next().slideUp();
            } else {
                $(this).next().slideToggle();
            }
            $('.expand-collapse li a').removeClass('active');
            $(this).addClass('active');
        });

        if ($("#newsScroll").length > 0) {
            $("#newsScroll").jScrollPane({
                showArrows: true,
                verticalArrowPositions: 'os',
                horizontalArrowPositions: 'os',
                autoReinitialise: true
            });
        }

        $(".search-box").watermark("Search");

    });
/************************************************************
Corporate Governance List
************************************************************/
    $('ol ol li').wrapInner('<span> </span>').addClass;


/************************************************************
Link Rollovers
************************************************************/
    $('.page-link a')
		.css({ backgroundPosition: "-343px 0px" })
		.mouseover(function () {
		    $(this).stop().animate({ backgroundPosition: "(0 0)" }, { duration: 300 })
		})
		.mouseout(function () {
		    $(this).stop().animate({ backgroundPosition: "(-343px 0)" }, { duration: 200, complete: function () {
		        $(this).css({ backgroundPosition: "-343px 0px" })
		    }
		    })
		})
    $('a.pretty-photo span')
		.css({ backgroundPosition: "-158px 0px" })
		.mouseover(function () {
		    $(this).stop().animate({ backgroundPosition: "(0 0)" }, { duration: 300 })
		})
		.mouseout(function () {
		    $(this).stop().animate({ backgroundPosition: "(-158px 0)" }, { duration: 200, complete: function () {
		        $(this).css({ backgroundPosition: "-158px 0px" })
		    }
		    })
		})


    $("#main-nav li")
        .each(function () {
            $(this).prepend($("<span></span>").text($(this).find("a").text())); //copy the text into an empty span tag right before the a tag
        })
        .hover(function () {	//On hover...
            $(this).find("span").first().stop().animate({
                marginTop: "-34" //Find the <span> tag and move it up 40 pixels
            }, 300);
        }, function () { //On hover out...
            $(this).find("span").first().stop().animate({
                marginTop: "0"  //Move the <span> back to its original state (0px)
            }, 200);
        });


    /* add pretty scrolling */
    $(".scroll, .top").click(function (event) {
        event.preventDefault();
        $('html,body').animate({ scrollTop: $(this.hash).offset().top }, 500);
    });

});

/************************************************************
    Map of Mines
************************************************************/
var currentPanel = 0;
var totalPanels = 0;
var lastPanel = 0;
var inTransition = false;
var activePanelLeft = 0;
var activePanelTop = 145;
var activePanelWidth = 665;
var activePanelHeight = 330;
var inactivePanelHeight = 665;
var inactivePanelWidth = 330;
var inactivePanelTop = 160;
var inactivePanelLeftLeft = 15;
var inactivePanelLeftRight = 15;
var mode = "flag";

/*
active panel left: 100px;
active panel top: 40px;
active panel width: 648px;
active panel height: 320px;

inactive panel height: 290px;
inactive panel width: 
inactive panel left (left side)
*/

$(document).ready(function () {
    totalPanels = $("#panels .panel").size();

    $("#map-of-mines-hotspots .cap").css("opacity", "0");

    $("#map-of-mines-hotspots .hotspot").mouseenter(function () {
        $("#map-of-mines-hotspots .hotspot").css("z-index", "5");
        $(this).css("z-index", "6");
        $(this).find(".cap").stop().animate({
            opacity: "1",
            marginTop: "-30px"
        }, 1000, 'easeOutQuint');
    }).mouseleave(function () {
        $(this).css("z-index", "5");
        $(this).find(".cap").stop().animate({
            opacity: "0",
            marginTop: "0px"
        }, 1000, 'easeOutQuint');
    }).click(function () {
        var num = parseInt($(this).find(".number").text());
        goToPanelView(num);
    });

    $(".panel-navigation").click(function () {
        var next = currentPanel + 1;
        if ($(this).attr("id") == "panel-previous") {
            next = currentPanel - 1;
        }

        if (next >= 0 && next <= totalPanels - 1) {
            changePanel(next);
        }
    });

    $(".panel-close").click(function () {
        goToFlagView();
    });
});

/** Home View **/
function goToFlagView() {
    if (mode !== "flag" && !inTransition) {
        inTransition = true;

        $("#map-states").animate({
            top: "600px"
        }, 600, 'easeInOutQuint', function () {
            $("#map-of-mines").animate({
                opacity: "1"
            }, 600, 'swing');

            $("#map-of-mines-hotspots").animate({
                opacity: "1"
            }, 600, 'swing', function () {
                mode = "flag";
                inTransition = false;
            });
        });
    }
}

/** Panel View **/

function goToPanelView(panelNum) {
    if (mode !== "panels" && !inTransition) {
        inTransition = true;
        lastPanel = panelNum - 1;
        if (lastPanel < 0) {
            lastPanel = 0;
        }
        currentPanel = panelNum;

        var panels = $("#panels .panel");

        for (var i = 0; i < panels.length; i++) {
            var panel = panels[i];
            var t = activePanelTop;
            var l = activePanelLeft;
            var z = 10;

            if (i !== currentPanel) {
                t = inactivePanelTop;
                z = 9;
            }

            if (i < currentPanel) {
                l = inactivePanelLeftLeft;
            }

            if (i > currentPanel) {
                l = inactivePanelLeftRight;
            }

            $(panel).css("left", l + "px");
            $(panel).css("top", t + "px");
            $(panel).css("z-index", z);
        }

        updateNavigation();

        $("#map-of-mines").animate({
            opacity: "0.5"
        }, 600, 'swing');

        $("#map-of-mines-hotspots").animate({
            opacity: "0.5"
        }, 600, 'swing', function () {
            $("#map-states").animate({
                top: "0px"
            }, 600, 'easeInOutQuint', function () {
                mode = "panels";
                inTransition = false;
            });
        });
    }
}

function changePanel(panelNum) {
    if (!inTransition) {
        inTransition = true;
        lastPanel = currentPanel;
        currentPanel = panelNum;

        var allPanels = $("#panels .panel");

        for (var i = 0; i < allPanels.length; i++) {
            if (i == lastPanel || i == currentPanel) {
                continue;
            }

            $(allPanels[i]).css("z-index", "8");
        }

        var nextPanel = $("#panels .panel");
        nextPanel = nextPanel[currentPanel];

        var lastP = $("#panels .panel");
        lastP = lastP[lastPanel];

        var lastProps = {
            top: inactivePanelTop + "px",
            left: inactivePanelLeftLeft + "px"
        };

        var flipLeft = "-650px";

        if (currentPanel < lastPanel) {
            lastProps.left = inactivePanelLeftRight + "px"
            flipLeft = "680px";
        }

        var nextProps = {
            top: activePanelTop + "px",
            left: activePanelLeft + "px"
        };

        $(lastP).css("z-index", "10");
        $(nextPanel).css("z-index", "9");

        updateNavigation();

        $(lastP).animate({
            top: lastProps.top,
            left: flipLeft
        }, 1000, 'easeInOutQuint', function () {
            $(lastP).css("z-index", "9");
            $(nextPanel).css("z-index", "10");
            $(lastP).animate({
                left: lastProps.left
            }, 1000, 'easeInOutQuint');

            $(nextPanel).animate({
                top: nextProps.top,
                left: nextProps.left
            }, 1000, 'easeInOutQuint', function () {
                inTransition = false;
            });
        });
    }
}

function updateNavigation() {
    if (currentPanel == 0) {
        $("#panel-previous").animate({
            backgroundPosition: "-17px 100px"
        }, 250, 'easeInOutQuint');

        $("#panel-next").animate({
            backgroundPosition: "-17px 100px"
        }, 250, 'easeInOutQuint');
    }

    if (currentPanel > 0 && currentPanel < totalPanels - 1) {
        $("#panel-previous").animate({
            backgroundPosition: "0px 100px"
        }, 250, 'easeInOutQuint');

        $("#panel-next").animate({
            backgroundPosition: "-17px 100px"
        }, 250, 'easeInOutQuint');
    }

    if (currentPanel == totalPanels - 1) {
        $("#panel-previous").animate({
            backgroundPosition: "0px 100px"
        }, 250, 'easeInOutQuint');

        $("#panel-next").animate({
            backgroundPosition: "0px 100px"
        }, 250, 'easeInOutQuint');
    }
}



/************************************************************
Sub-Page Tab Setup
************************************************************/
$(document).ready(function () {
    //Default Action
    $(".tab_content").hide(); //Hide all content
    $("ul.custom_tabs li:first").addClass("active").show(); //Activate first tab
    $(".tab_content:first").show(); //Show first tab content
    //On Click Event
    $("ul.custom_tabs li").click(function () {
        $("ul.custom_tabs li").removeClass("active"); //Remove any "active" class
        $(this).addClass("active"); //Add "active" class to selected tab
        $(".tab_content").hide(); //Hide all tab content
        var activeTab = $(this).find("a").attr("href"); //Find the rel attribute value to identify the active tab + content
        $(activeTab).fadeIn(); //Fade in the active content
        return false;
    });


    // Radio Functions
    $(".radio-table tr:last-child td").css("border-bottom", "none");
    $(".radio-table tr td:last-child").css("border-right", "none");

    if ($(".audio-wrapper").length > 0) {
        $(".playSound").click(function () {
            $(this).parent().find(".audio-player").slideToggle("fast");
            return false;
        });
    }
});

/*
*  Alternating Table Row Colors
**/
$(document).ready(function () {
    if ($(".alternate").length > 0) {
        $(".alternate").each(function(){
			$(this).find("tr:even td").addClass("gray");
		});
}

//* Start - Statistics Table Styling for Non-Developers *//

if ($(".stats").length > 0 && !$(".stats").hasClass("alternate")) {
    $(".stats").each(function () {
        $(this).find("tr:odd td").addClass("gray");
        $(this).find("tr:even td").removeClass("dark-gray");

        $(this).find("tr td").each(function () {
            if ($(this).hasClass("spacer") || $(this).hasClass("subhead") || $(this).hasClass("total") || $(this).hasClass("header")) {
                $(this).removeClass("gray");
            }
        });
    });
}

//* End - Statistics Table Style for Non-Developers *//

    if ($(".historical-moments").length > 0) {
        $(".historical-moments tr:even td").css("background-color", "#ECECEC");
    }

    if ($(".events.alternate").length > 0) {
        $(".events.alternate").each(function () {
            $(this).find("tr:even td").css("background-color", "#f6f6f6");
            $(this).find("td:last").css("border-bottom", "1px solid #E7E5E5");
            $(this).find("td").find("p:last").css("margin-bottom", "0px");
        });
    }

    InitGallery();
});



/*
*  Gallery Slideshow Controls
**/
function InitGallery() {
    /*
    *  Gallery Controls
    **/
    if ($(".gallery").length > 0) {

        $(".gallery").each(function () {

            var fullHeight = 0;
            var imageHeight = $(this).find(".slideshow").find(".image").outerHeight(true);
            var slideshowHeight = $(this).find(".slideshow").height();
            var position = $(this).find(".slideshow").find(".move").position();
            var currentTop = position.top;
            var maxTop = 0;

            $(this).find(".slideshow").find(".image").each(function () {
                fullHeight += $(this).outerHeight(true);
            });

            $(this).find(".slideshow").find(".move").css("height", fullHeight + "px");

            if (fullHeight <= slideshowHeight) {
                $(this).find(".btn-next").addClass("off");
            }

            maxTop = fullHeight - slideshowHeight;

            $(this).find(".btn-prev").click(function () {
                if (currentTop < 0) {
                    $(this).parent(".gallery").find(".btn-next").removeClass("off");

                    $(this).parent(".gallery").find(".slideshow .move").animate({
                        top: currentTop + imageHeight
                    }, 250, function () {
                        currentTop += imageHeight;
                        if (currentTop >= 0) {
                            $(this).parent(".slideshow").parent(".gallery").find(".btn-prev").addClass("off");
                        }
                    });
                }
                return false;
            });

            $(this).find(".btn-next").click(function () {
                if (currentTop > -maxTop) {
                    $(this).parent(".gallery").find(".btn-prev").removeClass("off");

                    $(this).parent(".gallery").find(".slideshow .move").animate({
                        top: currentTop - imageHeight
                    }, 250, function () {
                        currentTop -= imageHeight;
                        if (currentTop <= -maxTop) {
                            $(this).parent(".slideshow").parent(".gallery").find(".btn-next").addClass("off");
                        }
                    });
                }
                return false;
            });

            $(this).find(".slideshow").find(".image").click(function () {
                var url = $(this).find(".address").html();
                $(".player.commercials").html();
                $(".player.commercials").html("<iframe src='" + url + "' width='450' height='270' frameborder='0' scrolling='no'></iframe>");
                return false;
            });

        });

    }
}