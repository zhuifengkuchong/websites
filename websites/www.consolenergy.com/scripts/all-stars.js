var currentSlideId;
var currentDetailId;
var currentInfoId;
var currentSelection = 0;
var fullWidth = 0;

var imageWidth;
var slideshowWidth;
var position;
var currentLeft;
var maxLeft;

$(document).ready(function () {
    if ($("#allstars-gallery").length > 0) {
        topArrowsPosition();
        bottomArrowsPosition();
        showCurrent();

        $(".selectClick").click(function () {
            if ($(".current").is(":animated")) {
                return false;
            } else {
                currentSlideId = $(this).parent().find(".imgId").text();

                $("#details").find(".current").each(function () {
                    currentDetailId = $(this).find(".imgId").text();

                    if (currentDetailId == currentSlideId) {
                        $(".current").fadeOut(500);
                        $(this).fadeIn(500, function () {
                            $(".current").removeClass("active");
                            $(this).addClass("active");
                            currentSelection = $(this).find(".count").text();

                            $("#debug .current-image").text(currentSelection);
                        });
                    }
                });

                $("#flyout").find(".info").each(function () {
                    currentInfoId = $(this).find(".imgId").text();

                    if ($("#flyout").is(":visible")) {
                        if (currentInfoId == currentSlideId) {
                            $(".info").fadeOut(500);
                            $(this).fadeIn(500, function () {
                                $(".info").removeClass("active");
                                $(this).addClass("active");
                                currentSelection = $(this).find(".count").text();
                            });
                        }
                    } else {
                        if (currentInfoId == currentSlideId) {
                            $(".info").hide();
                            $(this).show();
                            $(".info").removeClass("active");
                            $(this).addClass("active");
                            currentSelection = $(this).find(".count").text();
                        }
                    }
                });

                $(".selectClick").parent().removeClass("active");
                $(this).parent(".slide").addClass("active");

                currentSlideLeft = $(".slide.active").position().left;
                $("#debug .current-slide-left").text(currentSlideLeft);

                currentLeft = $(".move").position().left;
                $("#debug .current-left").text(currentLeft);
            }
        });

        $(".btn-nextPic").click(function () {
            if ($(".current").is(":animated")) {
                return false;
            } else {
                getNextPic();
            }
        });

        $(".btn-prevPic").click(function () {
            if ($(".current").is(":animated")) {
                return false;
            } else {
                getPrevPic();
            }
        });

        var galleryHeight = $("#allstars-gallery").outerHeight();
        var detailsHeight = $("#details").outerHeight();

        $(".btn-flyout").click(function () {
            galleryHeight = $("#allstars-gallery").outerHeight();
            detailsHeight = $("#details").outerHeight();

            $("#allstars").append("<div id='overlay'></div>");
			
			

            $("#flyout").css("height", galleryHeight + "px");
            $("#flyout").show();

            $("#flyout .height").css("height", detailsHeight - 106 + "px");

            $(".fb-like iframe").css("width", "125px");
            $(".fb-like iframe").css("height", "22px");

            $("#overlay").css("z-index", "1");
            if ($.browser.msie && $.browser.version < 9.0) {
                $("#overlay").show();
            } else {
                $("#overlay").fadeIn(500);
            }

            $("#allstars-gallery").animate({
                width: "940px"
            }, 500, function () {
                $("#flyout .height .info.active").fadeIn(250);
            });

            $("#overlay").click(function () {
                closeFlyout();
            });
        });

        $(".btn-close").click(function () {
            closeFlyout();
        });
    }

//    $(".fb-share").click(function () {
//        shareFeed();
//    });
});

function closeFlyout() {
    $("#allstars-gallery").animate({
        width: 665 + "px"
    }, 500);

    $("#flyout .height .info.active").fadeOut(500, function () {
        $(this).hide();
        $(this).css("width", "100%");
        $("#flyout").hide();
    });

    $("#overlay").fadeOut(500, function () {
        $(this).remove();
    });
}

function showCurrent() {
    $("#allstars-gallery #details").find(".current").each(function () {
        var currentDetailId = $(this).find(".imgId").text();
        var currentSlideId = 0;
        var currentSlideLeft = 0;

        currentLeft = $(".move").position().left;
        $("#debug .current-left").text(currentLeft);

        if ($(this).is(":visible")) {
            $(this).addClass("active");

            $("#slider .move").find(".slide").each(function () {
                currentSlideId = $(this).find(".imgId").text();

                if (currentDetailId == currentSlideId) {
                    $(this).addClass("active");

                    currentSlideLeft = $(".slide.active").position().left;
                    $("#debug .current-slide-left").text(currentSlideLeft);
                } else {
                    $(this).removeClass("active");
                }
            });

            $("#allstars-gallery #flyout .height .info").each(function () {
                var activeContent = $(".current.active").find(".imgId").text();

                if ($(this).find(".imgId").text() == activeContent) {
                    $(this).addClass("active");
                } else {
                    $(this).removeClass("active");
                }
            });

            InitSlider();
            InitDetails();
        } else {
            $(this).removeClass("active");
        }
    });
}

var detailCount = 0;

function getPrevPic() {
    $(".current").each(function () {
        if ($(this).hasClass("active")) {
            //currentSelection = $(this).find(".count").text();

            if (currentSelection > 1) {
                currentSelection--;
                $("#debug .current-image").text(currentSelection);

                $(".current").each(function () {
                    if ($(this).hasClass("active")) {
                        $(this).fadeOut(500, function () {
                            $(this).removeClass("active");
                        });
                    } else if ($(this).find(".count").text() == currentSelection) {
                        $(this).fadeIn(500, function () {
                            $(this).addClass("active");
                        });
                    }
                });

                $("#flyout").find(".info").each(function () {
                    if ($(this).is(":visible")) {
                        if ($(this).hasClass("active")) {
                            $(this).fadeOut(500, function () {
                                $(this).removeClass("active");
                            });
                        } else if ($(this).find(".count").text() == currentSelection) {
                            $(this).fadeIn(500, function () {
                                $(this).addClass("active");
                            });
                        }
                    } else {
                        if ($(this).hasClass("active")) {
                            $(this).hide();
                            $(this).removeClass("active");
                        } else if ($(this).find(".count").text() == currentSelection) {
                            $(this).show();
                            $(this).addClass("active");
                        }
                    }
                });

                $("#slider").find(".slide").each(function () {
                    $(this).removeClass("active");

                    if (currentSelection == $(this).find(".count").text()) {
                        currentSlideLeft = $(this).position().left;

                        if (currentSlideLeft > maxLeft) {
                            currentLeft = $(".move").position().left;
                            $("#debug .current-left").text(currentLeft);

                            $(this).parent(".move").animate({
                                left: -maxLeft + "px"
                            }, 500);

                            $(".slide").removeClass("active");
                            $(this).addClass("active");
                            return false;
                        } else {
                            $("#debug .current-slide-left").text(currentSlideLeft);
                            $(this).parent(".move").animate({
                                left: -currentSlideLeft + "px"
                            }, 500, function () {
                                //currentLeft -= imageWidth;
                                currentLeft = $(".move").position().left;
                                $("#debug .current-left").text(currentLeft);

                                currentSlideLeft = $(".slide.active").position().left;
                                $("#debug .current-slide-left").text(currentSlideLeft);

                                if (currentSlideLeft < maxLeft) {
                                    $(".btn-nextSlide").removeClass("off");

                                    if (currentSlideLeft == 0) {
                                        $(".btn-prevSlide").addClass("off");
                                    }
                                }
                            });
                            $(this).addClass("active");
                        }
                    }
                });
            }
        }
    });
}

function getNextPic() {
    $(".current").each(function () {
        if ($(this).hasClass("active")) {
            //currentSelection = $(this).find(".count").text();

            if (currentSelection < detailCount) {
                currentSelection++;
                $("#debug .current-image").text(currentSelection);

                $(".current").each(function () {
                    if ($(this).hasClass("active")) {
                        $(this).fadeOut(500, function () {
                            $(this).removeClass("active");
                        });
                    } else if ($(this).find(".count").text() == currentSelection) {
                        $(this).fadeIn(500, function () {
                            $(this).addClass("active");
                        });
                    }
                });

                $("#flyout").find(".info").each(function () {
                    if ($(this).is(":visible")) {
                        if ($(this).hasClass("active")) {
                            $(this).fadeOut(500, function () {
                                $(this).removeClass("active");
                            });
                        } else if ($(this).find(".count").text() == currentSelection) {
                            $(this).fadeIn(500, function () {
                                $(this).addClass("active");
                            });
                        }
                    } else {
                        if ($(this).hasClass("active")) {
                            $(this).hide();
                            $(this).removeClass("active");
                        } else if ($(this).find(".count").text() == currentSelection) {
                            $(this).show();
                            $(this).addClass("active");
                        }
                    }
                });

                $("#slider").find(".slide").each(function () {
                    $(this).removeClass("active");

                    if (currentSelection == $(this).find(".count").text()) {
                        currentSlideLeft = $(this).position().left;
                        $("#debug .current-slide-left").text(currentSlideLeft);

                        //alert(currentLeft + " " + maxLeft);
                        if (currentSlideLeft <= maxLeft) {
                            $(this).parent(".move").animate({
                                left: -currentSlideLeft + "px"
                            }, 500, function () {
                                //currentLeft += imageWidth;
                                $(".btn-prevSlide").removeClass("off");
                                currentLeft = $(".move").position().left;
                                $("#debug .current-left").text(currentLeft);

                                currentSlideLeft = $(".slide.active").position().left;

                                if (currentSlideLeft == maxLeft) {
                                    $(".btn-nextSlide").addClass("off");
                                }
                            });
                            $(this).addClass("active");
                        } else {
                            $(".slide").removeClass("active");
                            $(".btn-nextSlide").addClass("off");
                            $(this).addClass("active");
                            return false;
                        }
                    }
                });
            }
        }
    });
}

var flyoutCount = 0;

function InitDetails() {
    
    $(".current").each(function () {
        detailCount++;
        $(this).append("<span class='count'>" + detailCount + "</span>");
        currentDetailId = $(this).find(".imgId").text();

        if ($(this).hasClass("active")) {
            currentSelection = $(this).find(".count").text();
        }
    });

    $("#flyout .height .info").each(function () {
        flyoutCount++;
        $(this).append("<span class='count'>" + flyoutCount + "</span>");
        currentDetailId = $(this).find(".imgId").text();

        if ($(this).hasClass("active")) {
            currentSelection = $(this).find(".count").text();
        }
    });

    $("#debug .total-count").text(detailCount);
    $("#debug .current-image").text(currentSelection);
}

function InitSlider() {
    /*
    *  Photo Gallery Controls
    **/
    var counter = 0;
    if ($("#slider").length > 0) {

        $("#slider").each(function () {

            fullWidth = 0;
            imageWidth = 154;
            slideshowWidth = $(this).width();
            position = $(this).find(".move").position();
            currentLeft = position.left;
            maxLeft = 0;

            $(this).find(".slide").each(function () {
                fullWidth += $(this).outerWidth(true);

                counter++;
                $(this).append("<span class='count'>" + counter + "</span>");
                currentSlideId = $(this).find(".imgId").text();
            });

            $(this).find(".move").css("width", fullWidth + "px");

            if (fullWidth <= slideshowWidth) {
                $(this).find(".btn-nextSlide").addClass("off");
            }

            maxLeft = fullWidth - slideshowWidth;
            $("#debug .max-left").text(maxLeft);

            $(this).find(".btn-prevSlide").click(function () {
                currentLeft = $(".move").position().left;

                if ($(".move").is(":animated")) {
                    return false;
                } else {
                    if (currentLeft < 0) {
                        $(this).parent().find(".btn-nextSlide").removeClass("off");

                        $(this).parent().find(".move").animate({
                            left: currentLeft + imageWidth
                        }, 250, function () {
                            currentLeft += imageWidth;
                            $("#debug .current-left").text(currentLeft);

                            if (currentLeft >= 0) {
                                $(this).parent().parent().find(".btn-prevSlide").addClass("off");
                            }
                        });
                    }
                    return false;
                }
            });

            $(this).find(".btn-nextSlide").click(function () {
                currentLeft = $(".move").position().left;

                if ($(".move").is(":animated")) {
                    return false;
                } else {
                    if (currentLeft > -maxLeft) {
                        $(this).parent().find(".btn-prevSlide").removeClass("off");

                        $(this).parent().find(".move").animate({
                            left: currentLeft - imageWidth
                        }, 250, function () {
                            currentLeft -= imageWidth;
                            $("#debug .current-left").text(currentLeft);

                            if (currentLeft <= -maxLeft) {
                                $(this).parent().parent().find(".btn-nextSlide").addClass("off");
                            }
                        });
                    }
                    return false;
                }
            });

        });

    }
}

function topArrowsPosition() {
    $("#allstars-gallery .top a").each(function () {
        var topHeight = $(this).parent().outerHeight();
        var thisHeight = $(this).outerHeight();
        var thisPosition = (topHeight - thisHeight) / 2;

        $(this).css("top", thisPosition - 2 + "px");
    });
}

function bottomArrowsPosition() {
    $("#slider .arrow").each(function () {
        var topHeight = $(this).parent().outerHeight();
        var thisHeight = $(this).outerHeight();
        var thisPosition = (topHeight - thisHeight) / 2;

        $(this).css("top", thisPosition + "px");
    });
}

function shareFeed(baseUrl){
    FB.ui(
        {
            method: 'feed',
            display: 'popup',
            /*
            redirect_uri specifies the page that the Facebook pop-up window will redirect to
            once the sharing process has been completed. It is generally pointed to a page that
            simply closes the window when it's done.
            */
            redirect_uri: 'http://bwerr.com/WindowCloser/index.html',
            name: 'CONSOL Energy Community All-Stars',
            caption: '', //If left blank, this will show the url that Share was sent from
            description: 'CONSOL Energy is recognizing Community All-Stars who help make the region a better place to live. Check it out the newest honorees.', //This is the description posted.
            link: document.location.href, //This is the link they will be directed to if they click it from friends feed		                
            picture: baseUrl + '/images/about-us/corporate-responsibility/community-news/community-allstars/fb-share.jpg', //This is the image shown with it (90x90 pixels)
            actions: [{
                name: 'Visit', //Customize the link at the bottom of the Share
                link: document.location.href //Can change where the small link at the bottom of Share goes to
            }],
            message: '' //If you want something to be populated in the Message field when they Share
        },
        function (response) {
            if (response && response.post_id) {
                //this fires if someone confirmed the Share to their newsfeed
            } else {
                //this fires if someone 'Canceled' the share
            }
        }
    );
}