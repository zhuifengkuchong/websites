$(document).ready(function () {
    if ($(".kids table").length > 0) {
        $(".kids table tr:odd td").css("background-color", "#e8e8e8");
    }

    $("#wrapper.fishForFun #content").jScrollPane({
        showArrows: true,
        verticalArrowPositions: 'os',
        horizontalArrowPositions: 'os',
        autoReinitialise: true
    });

    $("#termsPanel .interior").jScrollPane({
        showArrows: true,
        verticalArrowPositions: 'os',
        horizontalArrowPositions: 'os',
        autoReinitialise: true
    });

    $("#termsLink a").click(function () {
        $("#termsPanel").stop().animate({
            right: -282 + "px"
        }, 1000);
    });

    $("#termsPanel .close.slide").click(function () {
        $("#termsPanel").stop().animate({
            right: 306 + "px"
        }, 1000);
    });

    if ($("#thankYou").length > 0) {
        var headHeight = $("#header").outerHeight();
        var barHeight = $("#bar h2").outerHeight();
        var contentHeight = $("#content").outerHeight();
        var thankYouPadding = (contentHeight - (headHeight + barHeight)) / 2;

        $("#thankYou").css("padding-top", thankYouPadding + "px");
    }

    equalHeight($("#dateNav ul li a"));

//    $(".button").click(function () {
//        $("#date1, #date2, .dates").css("position", "absolute");

//        if ($(this).hasClass("date1")) {
//            $("#date1").css("position", "relative");
//            $("#date2").css("position", "absolute");

//            if (!$("#date1").is(":visible")) {
//                $(".button").removeClass("active");
//                $(this).addClass("active");

//                $("#date1").fadeIn(500);
//                $("#date2").fadeOut(500);
//            }
//        } else if ($(this).hasClass("date2")) {
//            $("#date1").css("position", "absolute");
//            $("#date2").css("position", "relative");

//            if (!$("#date2").is(":visible")) {
//                $(".button").removeClass("active");
//                $(this).addClass("active");

//                $("#date2").fadeIn(500);
//                $("#date1").fadeOut(500);
//            }
//        }
//    });
});

$("select").bind("change blur", function (e) {
    e.PreventDefault();
});

function equalHeight(group) {
    var tallest = 0;
    var buttonPadding;
    group.each(function () {
        var thisHeight = $(this).height();
        if (thisHeight > tallest) {
            tallest = thisHeight;
        }
    });

    group.each(function () {
        if ($(this).height() != tallest) {
            buttonPadding = ((tallest - $(this).height()) / 2) + 5;
            $(this).css("padding-top", buttonPadding + "px").css("padding-bottom", buttonPadding + "px");
        }
    });
}

function launchLightBox(){
	//var formUrl = $(this).attr("href");
	var formUrl = "http://www.consolenergy.com/FishForFun/Register.aspx";
    //$(this).removeAttr("href");

    $("#fishForFun").append("<div id='registerLightBox'></div><div id='overlay' onclick='removeForm()'></div>");

    $("#registerLightBox, #overlay").css("display", "none");

    //var overlayHeight = $(document).height() + 20;
    var overlayHeight = $("#overlay").outerHeight();
    var overlayWidth = $(document).width();
    $("#overlay").css("height", overlayHeight + "px");

    var wrapperWidth = $("#registerLightBox").outerWidth();
    var wrapperPosition = (overlayWidth - wrapperWidth) / 2;
    $("#registerLightBox").css("left", wrapperPosition + "px");

	$("#registerLightBox").html("<div class='closeBtn' onclick='removeForm()'>Close</div><iframe id='frame' frameborder='0' allowtransparency='true' scrolling='no' width='668' height='1315'></iframe>");
	$("#frame").attr("src", formUrl);

	$("#registerLightBox, #overlay").fadeIn(500, function () {
	    var resizeHeight = $("#registerLightBox").height() + 20;

	    $("#overlay").css("height", resizeHeight + "px");
	    $("#overlay").css("filter", "alpha(opacity=75)");
	});
	return false;
}

function removeForm(){
    $("#registerLightBox").fadeOut(500, function(){
		$(this).remove();
	});
    $(".closeBtn").fadeOut(500, function(){
		$(this).remove();
	});
    $("#overlay").fadeOut(500, function(){
		$(this).remove();
	});
	
	$(".registerForm").attr("href", "http://www.consolenergy.com/FishForFun/Register.aspx");
};