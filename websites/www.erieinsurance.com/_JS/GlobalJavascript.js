//==========================================================================================
// Verndale.com - GlobalJavascript.js
//==========================================================================================
/* In this file:
[jQuery Extensions] - all custom jQuery.extend functions
[Global Variables] - any variables needed globally
[JavaScript and jQuery Plugins] - all plugins written in javascript or jQuery
[Javascript Functions] - all none jQuery functions
[jQuery functions on load] - all modules / controls of the site
*/


//==========================================================================================
// BEGIN: JavaScript and jQuery Plugins
// END: JavaScript and jQuery Plugins

//==========================================================================================
// BEGIN: Extend jQuery
jQuery.extend({
    isUndefined: function (obj) {
        return typeof (obj) == "undefined" ? true : false;
    },
    exists: function (obj) {
        return jQuery(obj).length != 0 ? true : false;
    },
    browserDetection: function (appendTo, evalBrowser) {
        var userAgent = navigator.userAgent.toLowerCase();
        if (!evalBrowser) {
            var bindToElm = (jQuery.isUndefined(appendTo) ? jQuery("body") : jQuery(appendTo));
            jQuery.browser.chrome = /chrome/.test(userAgent);
            if (jQuery.browser.msie) { // Is this a version of IE?
                bindToElm.addClass("browser-ie");
                bindToElm.addClass("browser-ie" + jQuery.browser.version.substring(0, 1));
            }
            if (jQuery.browser.chrome) { // Is this a version of Chrome?
                bindToElm.addClass("browser-chrome");
                userAgent = userAgent.substring(userAgent.indexOf("chrome/") + 7);
                userAgent = userAgent.substring(0, 1);
                bindToElm.addClass("browser-chrome" + userAgent);
                jQuery.browser.safari = false; // If it is chrome then jQuery thinks it's safari so we have to tell it it isn't
            }
            if (jQuery.browser.safari) { // Is this a version of Safari?
                bindToElm.addClass("browser-safari");
                userAgent = userAgent.substring(userAgent.indexOf("version/") + 8);
                userAgent = userAgent.substring(0, 1);
                bindToElm.addClass("browser-safari" + userAgent);
            }
            if (jQuery.browser.mozilla) { // Is this a version of Mozilla?
                if (navigator.userAgent.toLowerCase().indexOf("firefox") != -1) { //Is it Firefox?
                    bindToElm.addClass('browser-firefox');
                    userAgent = userAgent.substring(userAgent.indexOf("firefox/") + 8);
                    userAgent = userAgent.substring(0, 1);
                    bindToElm.addClass("browser-firefox" + userAgent);
                } else { // If not then it must be another Mozilla
                    bindToElm.addClass("browser-mozilla");
                }
            }
        }
        else {
            var toEval = new RegExp(evalBrowser);
            return toEval.test(userAgent);
        }
    },
    getRootDomain: function () {
        var rootDomain = document.location.hostname;
        if (rootDomain.match(/localhost/gi)) {
            return finalDomain = "localhost";
        }
        else {
            var split = rootDomain.split('.');
            return "." + split[split.length - 2] + "." + split[split.length - 1];
        }
    },
    getHashValue: function () {
        return window.location.hash;
    },
    vcookie: function (c_name, value, exdays) {
        if (!value) {
            var i, x, y, ARRcookies = document.cookie.split(";");
            for (i = 0; i < ARRcookies.length; i++) {
                x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
                y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
                x = x.replace(/^\s+|\s+$/g, "");
                if (x == c_name) {
                    return unescape(y);
                }
            }
        }
        else if (value == "remove") {
            var c_value = escape(value) + "; expires=Thu, 1 Aug 1983 20:47:11 UTC";
            document.cookie = c_name + "=" + c_value;
        }
        else {
            var exdate = new Date(exdays);
            var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
            document.cookie = c_name + "=" + c_value;
        }
    },
    elmOverride: function (overrideName, overrideValue, overrideExdays) {
        //Do a relay to set cookie and attach override on target parent
        var c_name = overrideName;
        var value = (overrideValue == null ? null : overrideValue);
        var exdays = (typeof (overrideExdays) == 'undefined' ? '' : overrideExdays)

        jQuery.vcookie(c_name.replace(/[#.]/gi, ''), value, exdays);
        setFormDefaults().setValues(c_name);
    }
});

jQuery.fn.extend({
    trunc: function (count, append) {
        //Used for string truncation
        return (isNaN(count)) ? this : this.each(function () {
            var _this = jQuery(this);
            var curCount = 0;
            var boolCount = true;
            var curChar = "";
            var curString = jQuery.trim(_this.html());
            var tempString = "";
            var truncAppend = append;
            for (var i = 0; i < curString.length; i++) {
                curChar = curString.charAt([i]);
                tempString += curChar;
                switch (curChar) {
                    case "<":
                        boolCount = false;
                        break;
                    case ">":
                        boolCount = true;
                        break;
                    default:
                        if (boolCount) {
                            curCount++;
                        }
                }
                if (curCount == count) {
                    _this.html(tempString + (jQuery.isUndefined(truncAppend) ? "..." : truncAppend));
                    break;
                }
            }
        });
    },
    outerHTML: function (obj) {
        return jQuery("<div>").append(jQuery(this).clone()).html();
    },
    popupWindow: function (name, width, height, spec) {
        jQuery(this).live('click', function (e) {
            e.preventDefault();
            var winName = name.replace(/[ -\/&#]/gi, '');
            var theWidth = (width) ? width : 700;
            var theHeight = (height) ? height : 500;
            var winSpecs = 'width=' + width + ',height=' + height + ',' + (jQuery.isUndefined(spec) ? 'location=0,menuBar=1,resizable=1,scrollbars=1,status=1,toolbar=0,directories=0' : spec);
            window.open(this.href, winName, winSpecs);
        });
    },
    defaultText: function (activeClass, defautlAttr) {
        //Toggle class based on default value
        var _this = jQuery(this);
        _this.val() != _this.attr(defautlAttr) ? _this.removeClass(activeClass) : (_this.hasClass(activeClass) ? "" : _this.addClass(activeClass));
        _this.focus(function () {
            if (_this.val() == _this.attr(defautlAttr)) {
                _this.val('');
                _this.removeClass(activeClass);
            }
        }).blur(function () {
            if (_this.val() == "") {
                _this.val(_this.attr(defautlAttr));
                _this.addClass(activeClass);
            }
        });
    },
    customTransform: function (configValues) {
        var targetObject = jQuery(this);
        jQuery(targetObject).css('display', 'none');
        var myConfig = { "bindings": [configValues] };
        var _objectType = jQuery(targetObject).get(0).tagName.toLowerCase();
        var _objectAddClassContainer = typeof (myConfig.bindings[0].containerClass) == 'undefined' ? '' : myConfig.bindings[0].containerClass;
        var _objectAddClassLabel = typeof (myConfig.bindings[0].labelClass) == 'undefined' ? '' : myConfig.bindings[0].labelClass;
        var _objectAddClassDrop = typeof (myConfig.bindings[0].dropClass) == 'undefined' ? '' : myConfig.bindings[0].dropClass;
        var _objectDoPostBack = typeof (myConfig.bindings[0].postBack) == 'undefined' ? '' : myConfig.bindings[0].postBack;
        var _objectDoCallback = typeof (myConfig.bindings[0].callBack) == 'undefined' ? '' : myConfig.bindings[0].callBack;

        //More templates can be added to this, just add else + element type to extend.
        if (_objectType == 'select') {
            var selected = jQuery(':selected', targetObject).text();
            jQuery(targetObject).after(
         '<div class=\"uxselect select ' + _objectAddClassContainer + '\">'
          + '<a class=\"label ' + _objectAddClassLabel + '\" href=\"javascript:void(0);\"><span>' + selected + '</span></a>'
          + '<div style=\"display:none;\" class=\"dropdown ' + _objectAddClassDrop + '\"><ul></ul></div></div>'
         );

            var currentObj = jQuery('.uxselect.' + _objectAddClassContainer);
            jQuery('option', targetObject).each(function () {
                jQuery(this).attr('data-default') ? "" : jQuery('.dropdown ul', currentObj).append('<li><a data-value=\"' + jQuery(this).val() + '\" href=\"javascript:void(0);\">' + jQuery(this).text() + '</a></li>');
            });

            jQuery('.dropdown a', currentObj).live('click', function (e) {
                e.preventDefault();
                var text = jQuery(this).text();
                var index = jQuery(this).parent('li').index();
                jQuery('.label span', currentObj).html(jQuery(this).text());
                jQuery('option:eq(' + index + ')', jQuery(targetObject)).attr('selected', 'selected');
                jQuery('div.dropdown', currentObj).css('display', 'none');

                //If a postback is needed, get the postback value from the original select box and append to custom box.
                if (_objectDoPostBack != '') {
                    javascript: setTimeout('__doPostBack(\'' + jQuery(targetObject).attr('name') + '\',\'\')', 50);
                }

                //Makes a callback once a new value is selected. Passes original option value & text  
                if (_objectDoCallback != '') {
                    _objectDoCallback(jQuery(this).attr('data-value'), jQuery(this).text());
                }
            });

            jQuery('a.label', currentObj).live('click', function () {
                jQuery(this).next('div.dropdown').css('display', 'block');
                jQuery(this).parents('.uxselect').one('mouseleave', function () {
                    jQuery('.dropdown', this).css('display', 'none');
                });
            });
        }
    }
});

// END: Extend jQuery

//==========================================================================================
// BEGIN: Global Variables

// End: Global Variables

//==========================================================================================
// BEGIN: Functions

/*********************************************************
Begin: IE augmentation script, to add minimal help with older IE verions */
function initIEAugment() {
    jQuery('ul > li:first-child, ol > li:first-child').addClass('first-child');
    jQuery('ul > li:last-child, ol > li:last-child').addClass('last-child');
    jQuery('tr:odd').addClass('odd');
    jQuery('tr:even').addClass('even');
}
/* End: IE augmentation script, to help with older IE verions */


/*********************************************************
Begin: Initializing Tabs */
/* 
EXAMPLE

### HTML ###
<ul class="tabs">
<li><a href="#!">tab 1</a></li>
<li><a href="#!">tab 2</a></li>
</ul>
<div class="tab-content-wrap">
<div class="tab-content">tab content 1</div>
<div class="tab-content">tab content 2</div>
</div>

### JavaScript ###
iniTabController('ul.tabs > li > a','.tab-content','active');

*/
function iniTabController(source, destination, className) {
    // make first tab-content active on pageload
    jQuery(destination + ':eq(' + jQuery(source + '.' + className).index() + ')').addClass(className);
    // bind click event to tabs
    jQuery(source).on('click', function (e) {
        //e.preventDefault();
        var _this = jQuery(this);
        _this.parent().siblings().removeClass(className);
        _this.parent().addClass(className);
        jQuery(destination).removeClass(className);
        jQuery(destination + ":eq(" + _this.parent().index() + ")").addClass(className);
        return false;
    });
    // trigger initial click on first tab
    jQuery(source + ':eq(0)').click();
}
/* End: Initializing Tabs */


/*********************************************************
Begin: Initializing Blades - Expand / Collapse */
/*
EXAMPLE

### HTML ###
<ul>
<li class="blade-item">
<a href="#!" class="blade-link">blade link 1</a>
<div class="blade-content">blade content 1</div>
</li>
<li class="blade-item">
<a href="#!" class="blade-link">blade link 2</a>
<div class="blade-content">blade content 2</div>
</li>
<li class="blade-item">
<a href="#!" class="blade-link">blade link 3</a>
<div class="blade-content">blade content 3</div>
</li>
</ul>

### JavaScript ###
iniBlades('.blade-link','.blade-content','.blade-item', true);

*/
function iniBlades(objTarget, objContent, objParent, isAccordion, openFirstBlade, isExpand) {
    // hide all blades by default
    //jQuery(objContent).hide();
    // bind blade click functionality
    jQuery(objTarget).on('click', function (e) {
        e.preventDefault();
        var _this = jQuery(this);
        var _parent = _this.parents(objParent + ':first');
        var _content = jQuery(objContent, _parent);

        if (isAccordion) {


            if (_parent.hasClass('active')) {
                jQuery(objContent, '.active').slideUp('fast');
                jQuery(objParent).removeClass('active');
            } else {
                jQuery(objContent, '.active').slideUp('fast');
                jQuery(objParent).removeClass('active');
                _content.slideDown('fast');
                _parent.addClass('active');
            }
        } else {
            _content.slideToggle('fast');
            _parent.toggleClass('active');
        }
    });
}
/* End: Initializing Blades - Expand / Collapse */

function last_child() {
	if ($.browser.msie && parseInt($.browser.version, 10) <= 8) {
		$('*:last-child').addClass('last-child');
	}
	}
	
// END: Functions
var strElements = "abbr,article,aside,audio,canvas,datalist,details,figure,footer,header,hgroup,mark,menu,meter,nav,output,progress,section,time,video,figcaption,summary";
	strElements = strElements.split(",");
	for (var i = 0; i < strElements.length; i++) {
		document.createElement(strElements[i]);
	}; 		


function validateWebForms() {	

$(".scfValidationSummary").hide();
$('.scfForm').find(".scfValidatorRequired").each(function () {
	$(this).html($(this).attr("title"));
	$(this).parent().find("div").children().addClass("error");
	$(this).parent().find("label").html($(this).parent().find("label").html() + "<span class='scfRequired-1'>*</span>");
	
});

$('.scfForm').find(".scfRequired").each(function () {
	$(this).parent().find($(".scfSingleLineTextLabel")).html($(this).parent().find($(".scfSingleLineTextLabel")).html() + "<span class='scfRequired'>*</span>");
	$(this).hide();
});
			
			//return false;
		
	}
	
	
	
//==========================================================================================
// BEGIN: jQuery functions on load
//jQuery.noConflict();
var slider = "";
jQuery(document).ready(function ($) {
	
    //$.browserDetection('html');
	//initIEAugment(); 
    // If JavaScript is enabled this adds "j_on" to body tag.
    //$("body").addClass("j-on");

    // ------------------------------------------------------------------------------
    // BEGIN: Default Input Value
    // Give input fields a class 'default-text' to enable clear-text functionality
    $('.default-text').each(function () {
        $(this).defaultText('active', 'title');
    });
    // END: Default Input Value

    // ------------------------------------------------------------------------------
    // BEGIN: Primary Nav JS Augmentation
    // Add some additional redundancy checks to primary nav to provide ensured hover/active states
    $('li', '.primary-nav').each(function () {
        var _this = $(this);
        _this.on({
            mouseenter: function (e) {
                _this.addClass('hover');
            },
            mouseleave: function (e) {
                _this.removeClass('hover');
            }
        });
    });
    // END: Default Input Value

    //to find Image Dimensions
    if (window.location.search.indexOf("?imgDims") !== -1) {
        $("img").elementSizeDisplay();
    }

    /*$('.round-arrow-normal').corner();
    $('.round-arrow').corner();*/

    //Heroslider
   /* 
   //Commented by Manoj - as of heroslider is not requried as Erie is using single image only

   var autoSlider;
    slider = $('.heroSlider').heroSlider({
        mode: 'horizontal', //('horizontal','vertical', 'fade'
        infiniteLoop: true,
        hideControlOnEnd: false,
        autoHover: true,
        autoDelay: 6,
        controls:false,
        adaptiveHeight: false,
        preloadImages: 'all',
        onSliderLoad: function () {
            if (slider.getSlideCount() > 1) {
                autoSlider = true;
            } else {
                autoSlider = false;
            }
        },
        auto: autoSlider,
        autoStart: autoSlider
    });
*/
    //Secondary Left Menu Accordian
    iniBlades('.blade-item > span', '.blade-content', '.blade-item', true);

    //mobile global site search
    $('.site-search-icon').click(function (e) {
        if (!$(".site-search-icon").hasClass("activeDrop")) {
            $(".site-search-wrap").show();
            $(".site-search-icon").addClass("activeDrop");
            $(".site-search-wrap").removeClass("site-search-wrap").addClass("site-search-wrap-mob");

        } else {
            $(".site-search-wrap-mob").hide();
            $(".site-search-wrap-mob").removeClass("site-search-wrap-mob").addClass("site-search-wrap");
            $(".site-search-icon").removeClass("activeDrop")
            //$(".site-search-wrap").removeClass("site-search-wrap-mob").addClass("site-search-wrap-mob");	
        }
    });
    //mobile Primary Nav
    $('nav#menu-left').mmenu();


    //touch screen virtual keyboard visible
	if(Modernizr.touch){
			$('input, textarea').on('focus', function(e) {
			$('.persistent-toolbar-wrap').css({'position':'absolute'});
		}).on('blur', function(e) {
			$('.persistent-toolbar-wrap').css({'position':'fixed'});
		});
	}

	
	$('.home-image-wrapper h2.image-title a').trunc(95, "...");
	//alert($('.scfForm').find(".scfRequired").length);
	
	
	/*$('.scfForm').find(".scfRequired").each(function () {
		$(this).parent().find($(".scfSingleLineTextLabel")).html($(this).parent().find($(".scfSingleLineTextLabel")).html() + "<span class='scfRequired'>*</span>");
		$(this).hide();
	});*/
	$(".scfValidationSummary").hide();
	validateWebForms();
	
	
	
    $(".persistent-toolbar-content").hide();
    $(".tools-settings").click(function () {
        $(".persistent-toolbar-content").slideToggle();
    });
    $(".agent-close-icon a").click(function () {
        $(".persistent-toolbar-content").slideUp();
    });

    //FAQ Toggle Container
    $(".faq-expand-all").click(function () {
        var t = $(this);
        if (t.hasClass("faq-collapse-active")) {
            t.next($(".faq-expand-all li")).next().find($(".question")).addClass('active');
            t.next($(".faq-expand-all li")).next().find($(".answer")).slideDown();
            t.find($(".faq-expand-all a")).html("Collapse");
            t.addClass("faq-expand-active").removeClass("faq-collapse-active");
            if (typeof trackEvent !== "undefined") {
                trackEvent({
                    category: getFolderFromUrl(0).replace(" ", "-").replace("%20", "-").toLowerCase() + "-FAQs",
                    action: "Expand-FAQs",
                    label: t.closest(".faq-expand-all").prevAll("h3").filter(":first")[0].innerText
                });

            }
        } else {
            t.next($(".faq-expand-all li")).next().find($(".question")).removeClass('active');
            t.next($(".faq-expand-all dl")).next().find($(".answer")).slideUp();
            t.find($(".faq-expand-all a")).html("Expand");
            t.removeClass("faq-expand-active").addClass("faq-collapse-active");
        }
        return false;
    });

    //FAQ Toggle Indvidual Level
    $('.answer').hide();
    $('.question').click(function () {
        var t = $(this);
        t.toggleClass('active');
        t.next('.answer').slideToggle();
        if (typeof trackEvent !== "undefined") {
            trackEvent({
                category: getFolderFromUrl(0).replace(" ", "-").replace("%20", "-").toLowerCase() + "-FAQs",
                action: "Click-FAQ",
                label: t.context.innerHTML
            });
        }
    });



});
// END: jQuery functions on load
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
function getFolderFromUrl(index) {
    if(isNaN(index) || index < 0) {
        return null;
    }

    var loc = window.location,
        url = loc.pathname.substr(1),
        segments = url.split("/");
    
    if(index < segments.length) {
        return segments[index];
    } else {
        return null;
    }
}
//to find Image Dimensions
(function ($) {
    $.fn.elementSizeDisplay = function (settings) {
        return this.each(function () {
            var el = $(this);
            el.load(function () {
                var parent = el.offsetParent();
                var pos = el.position();

                var width = el.width();
                var height = el.height();

                var dimensionEl = $("<span class='test'>" + width + " x " + height + "</span>");
                parent.append(dimensionEl);

                dimensionEl.css(
          { "position": "absolute",
              "left": pos.left,
              "top": pos.top,
              "background": "rgba(180,180,180, .95)",
              "font-size": "12px",
              "padding": "5px",
              "color": "black"
          });
            });

        });
    };
})(jQuery);

//onWindow Resize
var $window = $(window);
$window.bind("load", function () {
    //$("#menu-left").hide();
    //Search Mobile
    $(".site-search-wrap-mob").hide();
    $(".site-search-wrap-mob").removeClass("site-search-wrap-mob").addClass("site-search-wrap");
    $(".site-search-icon").removeClass("activeDrop");
	
	

});

$window.bind("resize", function (e) {

    var screenSize = $window.width();
    
   	if (!$.exists('.teaser-desc')){
		if(screenSize <= 629){
			$('.teaser-desc').trunc(170, "...");
		}
	}

    if (typeof $.currentScreenWidth === "undefined" || screenSize != $.currentScreenWidth) { // checks if post-resize width matches recorded width (i.e., only height resized)
	    $(".site-search-wrap-mob").hide();
	    $(".site-search-wrap-mob").removeClass("site-search-wrap-mob").addClass("site-search-wrap");
	    $(".site-search-icon").removeClass("activeDrop");
	    $.currentScreenWidth = screenSize; // records new screen width
	}
});


$(document).ready(function() {
	if($('html').hasClass('browser-ie7') || $('html').hasClass('browser-ie8')) {
		if($('.large-3.columns.push-2-5 .homepageagent-wrap').length < 1 && $('.large-3.columns.push-2-5 .callout-wrap').length < 1 && $('.large-3.columns.push-2-5 .scfForm').length < 1) {
			$('html').addClass('bug-34219');
		}
	}
	
	
	$('.find-agent-listing .check-box-cols.checked .spn-checkbox, .find-agent-listing .agent-details-wrapper .lbl-select-agent, .checkbox-wrapper .lbl-select-agent').live('click', function() {
		$('.find-agent-listing .checked').removeClass('checked');
		$(this).parent().parent().addClass('checked');
	});
	
	$('.browser-ie7 img').removeAttr('width').removeAttr('height');

	$.currentScreenWidth = window.screen.width; // stores current screen size
});

function alpha(e) {
	var k;
	document.all ? k = e.keyCode : k = e.which;
	return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
}