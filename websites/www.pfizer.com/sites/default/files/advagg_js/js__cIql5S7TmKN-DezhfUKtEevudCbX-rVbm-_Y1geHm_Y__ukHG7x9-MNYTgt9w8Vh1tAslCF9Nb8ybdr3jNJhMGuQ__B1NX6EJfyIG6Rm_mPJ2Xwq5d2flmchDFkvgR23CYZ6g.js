(function ($) {

Drupal.behaviors.facebookApp = {
  attach: function (context, settings) {
    if ($('#fb-root').length == 0) {
      var fbRoot = $('<div id="fb-root"></div>');
      $('body').append(fbRoot);

      var d = document;
      var s = 'script';
      var id = 'facebook-jssdk';
      var fbAppID = settings.pfizerSocial.facebookAppID;

      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=" + fbAppID;
      fjs.parentNode.insertBefore(js, fjs);
    }
  }
};

})(jQuery);

;/**/
