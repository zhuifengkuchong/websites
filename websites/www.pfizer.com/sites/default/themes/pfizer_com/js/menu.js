(function ($) {
  $(document).ready(function () {
    var MOBILE_MENU_WIDTH_MIDDLE = 768,
        menusIndexes = [1, 3, 4, 5, 6, 7, 8, 9, 10],
        regionMenu = $('#region-menu'),
        RegionMenunew = $('#region-menunew'),
        winwidth = jQuery(window).width(),
        homeLang = $('#home_lang'),
        countrySelector = $('#country_finder'),
        mainMenu = $('#superfish-2'),
        topMenu = $('#superfish-12'),
        topNavManual = $('.mobile_topnav_wrapper a.topnav_manual');

    $.each(menusIndexes, function (index, val) {
      var menuSelector = '#superfish-' + val;
      adjustSubmenuSizes(menuSelector);
    })

    // adjust region menu height.
    subMenu = regionMenu.find('ul.sf-menu');
    var topMenuOffset = 0;
    var bottomMenuOffsetTop = 0;
    var bottomMenuHeight = 0;

    if (subMenu.length > 0) {
      topMenuOffset = subMenu.offset();
    }

    while (true) {
      subMenu = subMenu.find('> li.sf-breadcrumb > ul');
      if (subMenu.length > 0) {
        bottomMenuOffsetTop = subMenu.offset().top;
        bottomMenuHeight = subMenu.height();
      }
      else {
        break;
      }
    }
    var mainMenuHeight = bottomMenuOffsetTop - topMenuOffset.top + bottomMenuHeight;
    regionMenu.css('height', mainMenuHeight + 'px');

    $('#superfish-2 > li > a').append('<sup class="top_menu_arrow"> ▼</sup>');
    var external = $('<span class="ext"></span>');
    $('#menu-1752-12 > a, #menu-1753-12 > a').append(external);
    $('#menu-1754-12 > a, #menu-1755-12 > a, #menu-1756-12 > a').append("▾");

    RegionMenunew.before('<a class="toggleMenu" href="#"><img src="../images/menu_trans.png"/*tpa=http://www.pfizer.com/sites/default/themes/pfizer_com/images/menu_trans.png*/ /></a>');
    RegionMenunew.find('ul li a').addClass('parent');
    var toggleMenu = $('.toggleMenu');

    if (winwidth > 786) {
      homeLang.mouseover(function () {
        countrySelector.addClass('visible_country');
      });

      countrySelector.mouseleave(function () {
        $(this).removeClass('visible_country');
      });
    }
    else {
      topNavManual.click(function (e) {
        e.preventDefault();
        countrySelector.toggleClass('visible_country');
      });

      mainMenu.append(topMenu.html());
      mainMenu.append('<li class="no-desktop"><a href="http://www.pfizer.com/b2b/index">Business to Business</a><ul><li class="no-desktop"><a href="http://www.pfizer.com/b2b/suppliers/suppliers">Suppliers</a><ul><li class="no-desktop"><a href="http://www.pfizer.com/b2b/suppliers/what_we_buy">What We Buy</a></li><li class="no-desktop"><a href="http://www.pfizer.com/b2b/suppliers/po_terms_and_conditions">PO Terms &amp; Conditions</a></li><li class="no-desktop"><a href="http://www.pfizer.com/b2b/suppliers/policies_and_procedures">Policies &amp; Procedures</a></li><li class="no-desktop"><a href="http://www.pfizer.com/b2b/suppliers/supplier_notice">Supplier Notice</a></li><li class="no-desktop"><a href="http://www.pfizer.com/b2b/suppliers/supplier_diversity">Supplier Diversity</a></li><li class="no-desktop"><a href="http://www.pfizer.com/b2b/suppliers/supplier_conduct">Supplier Conduct</a></li><li class="no-desktop"><a href="http://www.pfizer.com/b2b/suppliers/supplier_network_collaboration">Supplier Network Collaboration</a></li></ul></li><li class="no-desktop"><a title="" href="http://www.pfizer.com/products/hcp/pfizer_authorized_distributors">Authorized Distributors</a></li><li class="no-desktop"><a  href="http://www.pfizeraccountspayable.com/apiqnet" target="_blank">Pfizer Accounts Payable</a></li><li class="no-desktop"><a href="https://ofb.pfizer.com/bjsp/v2/usp/orderEntryV2.do" >PfizerPharm</a></li><li class="no-desktop"><a href="https://ofb.pfizer.com/bjsp/v2/usp/orderEntryV2.do">Pfizer eCommerce</a></li><li class="no-desktop"><a href="http://pfpfizerusdev.prod.acquia-sites.com/files/do/barcode_standard.pdf">Pfizer Barcode Standard</a></li><li class="no-desktop"><a href="http://www.pfizer.com/b2b/translational_medicine/translational_medicine">Translational Medicine Research Opportunities</a><ul><li class="no-desktop"><a href="http://www.pfizer.com/b2b/translational_medicine/faqs_translational_medicine">FAQs</a></li><li class="no-desktop"><a href="http://www.pfizer.com/b2b/translational_medicine/inflammation_es">Inflammation</a></li><li class="no-desktop"><a href="http://www.pfizer.com/b2b/translational_medicine/ophthalmology_es">Ophthalmology</a></li><li class="no-desktop"><a href="http://www.pfizer.com/b2b/translational_medicine/oncology_es">Oncology</a></li><li class="no-desktop"><a href="http://www.pfizer.com/b2b/translational_medicine/neurosciences_es">Neurosciences</a></li></ul></li></ul></li>');
    }

    RegionMenunew.find('li a').each(function () {
      if ($(this).next().length > 0) {
        $(this).addClass('parent');
      }
    });

    toggleMenu.click(function (e) {
      e.preventDefault();
      $(this).toggleClass('active');
      RegionMenunew.toggle();
      if (RegionMenunew.is(':visible')) {
        toggleMenu.addClass('active-state');
      }
      else {
        toggleMenu.removeClass('active-state');
      }
    });

    adjustMenu(RegionMenunew, toggleMenu);
    if (RegionMenunew.is(':visible')) {
      toggleMenu.addClass('active-state');
    }

    $(window).bind('resize orientationchange', function () {
      adjustMenu(RegionMenunew, toggleMenu);
    });

    function adjustSubmenuWidth(menuLevel) {
      var count = menuLevel.length;
      if (count > 0) {
        var finalw = 940 / count;
        finalw = finalw - 5;
        if (finalw > 190) {
          finalw = 190;
        }
        var finalw = finalw + "px";
        menuLevel.css('width', finalw);
        return true;
      }
      return false;
    }

    function adjustSubmenuSizes(menuId) {
      var menuWrapper = $(menuId);
      if (menuWrapper.length <= 0) {
        return;
      }

      menuWrapper.find('li:not(.sf-breadcrumb)').addClass('width-fix-menu');

      // update menu levels width.
      var wrap = menuWrapper.find('> li');
      var status = true;
      while (true) {
        status = adjustSubmenuWidth(wrap);
        if (status) {
          wrap = wrap.filter('.sf-breadcrumb').find('ul:first > li');
        }
        else {
          break;
        }
      }

      // update menu levels height.
      wrap = menuWrapper;
      while (true) {
        var height = wrap.height();
        height += 10;
        wrap = wrap.find('> li.sf-breadcrumb ul');
        if (wrap.length > 0) {
          wrap.css('top', height);
        }
        else {
          break;
        }
      }

      // add decoration divs to active trails.
      wrap = menuWrapper;
      while (true) {
        var h = wrap.height() + 10;
        wrap = wrap.find('> li.sf-breadcrumb > ul');
        if (wrap.length > 0) {
          var parentTrail = wrap.parent();
          var c = parentTrail.css('backgroundColor');
          var w = parentTrail.width();
          parentTrail.prepend('<div class="attacherdiv sup6-1" style="height:' + h + 'px;width:' + w + 'px;background:' + c + '"/>');
        }
        else {
          break;
        }
      }

      // addjust li sizes.
      wrap = menuWrapper;
      height = wrap.height();
      wrap.find('> li').has('a').css('height', height);
      wrap.find('> li > a').css('height', height - 21);

      while (true) {
        wrap = wrap.find('> li.sf-breadcrumb > ul');
        if (wrap.length > 0) {
          height = wrap.height();
          wrap.find('> li').has('a').css('height', height);
          wrap.find('> li > a').css('height', height - 21);
        }
        else {
          break;
        }
      }
    }

    function adjustMenu (RegionMenunew, toggleMenu) {
      var PageToolsWrap = $('.pageTools_wrap'),
        RegionUserSecondForm = $('#region-user-second form'),
        pagetool = PageToolsWrap.width(),
        hdr_width = $('#section-header').width(),
        togglemenu = toggleMenu.width(),
        zone_user_width = hdr_width - 51,
        form_width = zone_user_width - 51,
        search_btn = $('#region-user-second .btn_home_blue').parent().width(),
        search_field_width = form_width - search_btn - 18,
        ww = document.body.clientWidth,
        rateBlock = $('.print-block-wrapper');
      if (ww <= MOBILE_MENU_WIDTH_MIDDLE) {
        RegionUserSecondForm.removeClass('nav_search');
        //to add active class for b2b section which is appended through formalize.js.
        $('*').find("a[href='" + location.pathname + "']").each(function () {
          $(this).removeClass('active');
          $(this).addClass('active');
        });

        $('#zone-user-wrapper').width(zone_user_width);
        RegionUserSecondForm.width(form_width);
        $('.mobile_search').width(search_field_width);
        //top seach, page tool and menu button alignment script end.
        $('.page-products-hcp .views-table').unwrap('<div class="table_wrapper" />').wrap('<div class="table_wrapper" />');
        $('.page-news-contact-contact-pfizer #cmsContent #basic_bordered').unwrap('<div class="table_wrapper" />').wrap('<div class="table_wrapper" />');
        // Mobile menu jquery start.
        RegionMenunew.find('ul.sf-menu > li > a').each(function () {
          var mv = ($(this).text().slice(0, 4));
          var result = mv.replace(/\s/gi, '_');
          $(this).parent().addClass(result);
        });
        RegionMenunew.find('li:not(:has(ul))').children('a').removeClass('parent');
        RegionMenunew.each(function () {
          $('span').remove('.nav-click');
          RegionMenunew.find('li').has('ul').prepend('<span class="nav-click"><i class="nav-arrow"></i></span>');
        });
        toggleMenu.css('display", "inline-block');
        if (!toggleMenu.hasClass('active')) {
          RegionMenunew.hide();
        }
        else {
          RegionMenunew.show();
        }
        RegionMenunew.find('li').unbind('mouseenter mouseleave');
        RegionMenunew.find('li span.nav-click').unbind('click').bind('click', function (e) {
          // Must be attached to anchor element to prevent bubbling.
          e.preventDefault();
          $(this).parent('li').toggleClass('hover');
        });
        // This code below forces mobile menu to be open on the page load.
        RegionMenunew.find('a.active').each(function () {
          // Exclude specific pages from default behavoiur.
          if ($('body').hasClass('page-products-medical-information')) {
            // Do not open mobile menu by default on the medinfo map page.
            return;
          }
          $('a.active')
            .parents('li')
            .addClass('hover')
            .parents('li')
            .children('span')
            .children()
            .parents('ul')
            .css({'display': 'block', 'visibility': 'visible'});
          RegionMenunew.css('display', 'block');
        });
        // Page tools show/hide.
        rateBlock.addClass('mobile_hide');
        PageToolsWrap.click(function () {
          $(this).toggleClass('active-state');
          rateBlock.toggleClass('mobile_hide');
        });
      }
      else if (ww > MOBILE_MENU_WIDTH_MIDDLE) {
        RegionUserSecondForm.addClass('nav_search');
        $('.page-products-hcp .views-table').unwrap('<div class="table_wrapper" />');
        $('.page-news-contact-contact-pfizer #cmsContent #basic_bordered').unwrap('<div class="table_wrapper" />');
        toggleMenu.css('display', 'none');
        RegionMenunew.show();
        RegionMenunew.find('li').removeClass('hover');
        RegionMenunew.find('li a').unbind('click');
      }

      if (ww <= MOBILE_MENU_WIDTH_MIDDLE) {
        $('#zone-user-wrapper').width(zone_user_width);
        RegionUserSecondForm.width(form_width);
        $('.mobile_search').width(search_field_width);
      }
      else if (ww > MOBILE_MENU_WIDTH_MIDDLE) {
        $('#zone-user-wrapper').css('width', '');
        RegionUserSecondForm.css('width', '');
        $('.mobile_search').css('width', '');
      }
    }
  });
})(jQuery);
