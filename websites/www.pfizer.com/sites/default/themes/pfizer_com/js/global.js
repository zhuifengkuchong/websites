(function ($) {
  $(document).ready(function () {
    var blockStystemMain = $('#block-system-main');

    // popup for external links.
    blockStystemMain.find('http://www.pfizer.com/sites//default//themes//pfizer_com//js//a.ext').click(function () {
      $(this).addClass('visit');
      $('#extlink-ok-btn').click(function () {
        blockStystemMain.find('a.ext.visit').addClass('visited');
      });
      $(this).addClass('visit');
      $('extlink-cancel-btn').click(function () {
        blockStystemMain.find('a.ext.visit').removeClass('visit');
      });
    });

    // Removes external links on buttons from right callouts.
    $(".intcalloutright > p > span").removeClass("ext");

    if (navigator.userAgent.indexOf('Mac OS X') != -1) {
      $('#superfish-2 > li').css({ 'padding-right': '9px' }).filter('.last').css({ 'padding-right': '0px' });
      //apend class in body tag for MAC machines
      $('body').addClass('mac_class');
    }

  });
})(jQuery);
