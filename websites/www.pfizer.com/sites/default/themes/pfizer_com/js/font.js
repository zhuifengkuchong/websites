(function ($) {

/**
 * See https://github.com/typekit/webfontloader.
 */
Drupal.behaviors.WebFontLoader = {
  attach: function (context, settings) {
    $('body', context).once('WebFontLoader', function () {
      var WebFontConfigID = settings.pfizer_settings.WebFontLoaderWebFontConfigID;

      if (WebFontConfigID) {
        WebFontConfig = {
          fontdeck: {
            id: WebFontConfigID
          }
        };
        var wf = document.createElement('script');
        wf.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
      }
    });
  }
};

})(jQuery);
