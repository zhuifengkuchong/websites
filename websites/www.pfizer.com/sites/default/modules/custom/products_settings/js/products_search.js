(function ($) {

Drupal.behaviors.productsSearchForm = {
  attach: function (context, settings) {
    var searchForm = $('#views-exposed-form-view-products-page-1', context);

    searchForm.once('productsSearchForm', function () {
      var searchField = $('#views-exposed-form-view-products-page-1 #edit-title', context);
      searchField.attr('placeholder', Drupal.t('Enter product name'));

      searchForm.submit(function (e) {
        e.preventDefault();
        setTimeout(submitSearch, 100);
      });

      function submitSearch() {
        var title = jQuery.trim(searchField.val());
        if (title.length > 0) {
          $.ajax({
            url: '/get-product-id/' + Drupal.encodePath(title),
            data: '{}',
            type: 'GET',
            dataType: 'json',
            success: function (resp) {
              var jsonObj = jQuery.parseJSON(resp);
              if(jsonObj.value) {
                window.location.href = 'http://www.pfizer.com/products/product-detail/' + jsonObj.value;
              }
              else {
                window.location.href = 'http://www.pfizer.com/search/google/' + title;
              }
            },
          });
        }
        return false;
      }

    });

  }
};

})(jQuery);
