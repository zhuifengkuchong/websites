function sendThisPage(){
  var mailto_link = 'mailto:?subject=Pfizer.com Press Release&body=I thought you might be interested in this Pfizer.com Press Release ' + window.location;
  var win = window.open(mailto_link,'emailWindow');
  if (win && win.open && !win.closed) {
    win.close();
  }
}

function openPrintPage(divid){
  var baseurl=Drupal.settings.pfizer_settings.base_url,
    tab = jQuery('.' + divid).html();
  tab = "<html><head><title>Printer Friendly</title><link href='../../../../themes/pfizer_com/css/static-pages-1.css'/*tpa=http://www.pfizer.com/sites/default/themes/pfizer_com/css/static-pages.css*/ rel='stylesheet' type='text/css'><link href='"+baseurl+"/sites/default/themes/pfizer_com/css/print.css' rel='stylesheet' type='text/css'></head><body>" + tab + "</body></html>";

  if (tab != null){
    winprint = window.open('','Print','width=900,height=900,fullscreen=yes,scrollbars=yes,menubar=no');
    winprint.document.write(tab);
    jQuery(winprint.document).contents().find('.contextual-links-processed').html('')   ;
    jQuery(winprint.document).contents().find('a').attr('href','#')   ;
    winprint.document.close();
    winprint.focus();
    winprint.print();
  }
}

function number_format(number, decimals, dec_point, thousands_sep) {
  // Strip all characters but numerical ones.
  number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    s = '',
    toFixedFix = function (n, prec) {
      var k = Math.pow(10, prec);
      return '' + Math.round(n * k) / k;
    };
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '').length < prec) {
    s[1] = s[1] || '';
    s[1] += new Array(prec - s[1].length + 1).join('0');
  }
  return s.join(dec);
}

function formatNumber(num) {
  num = "" + num;
  var dot = num.indexOf(".");
  if (dot > 0 && dot < num.length - 3) {
    return num.substring(0, dot + 3);
  }
  else {
    return num;
  }
}


function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}
