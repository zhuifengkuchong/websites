(function ($) {
  $(window).load(function () {
    s.linkTrackVars = 'prop27,prop28';
    s.prop27 = 'Document Complete';
    s.prop28 = document.title;
    s.tl(true, 'o', 'Document Complete');
    s.prop27 = s.prop28 = '';
  });
})(jQuery);
