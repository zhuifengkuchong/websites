(function ($) {

/**
 * Attach omniture tracking to quicktabs.
 */
Drupal.behaviors.omnitureQuicktabs = {
  attach: function (context, settings) {
    $('.quicktabs-tabs li a', context).bind('click', function () {
      vars = s_gi(s_account);
      s.pageName = s.hier1 + 'AJAX/' + $(this).text();
      s.t();
    });
  }
};

})(jQuery);
