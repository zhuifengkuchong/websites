(function ($) {

Drupal.behaviors.homeStockTicker = {
  attach: function (context, settings) {

    var homeStockTicker = $('#home-stock-ticker', context);

    homeStockTicker.once('homeStockTicker', function () {
      var tickerButton = homeStockTicker.find('#ticker-icon');
      var homeStockInfo = homeStockTicker.find('#home-stock-info');

      homeStockTicker.find('#ticker-icon').click(function () {
        homeStockInfo.toggle();
      });

      $.ajax({
        url: settings.customStockTicker.homeStockXml,
        type: 'POST',
        dataType: 'xml',
        success: function (resp) {
          if (resp) {
            fillHomeStockTicker(resp);
          }
        }
      });

      function fillHomeStockTicker(stock_xml) {
        $(stock_xml).find('StockQuotes').each(function () {
          $(this).find('Stock_Quote').each(function () {
            if($(this).attr('Company') == 'Pfizer') {
              var dattm = $(this).find('Date').text();
              var mkt_cap = $(this).find('MarketCap').text()
              mkt_cap = number_format(mkt_cap * 1 / 1000000, 2);
              var pricedown = $(this).find('Change').text() * 1;
              var percent_change = ($(this).find('Change').text() * 100) / ($(this).find('PreviousClose').text() * 1);
              percent_change=number_format(percent_change,2);
              var arrowImagePath = (pricedown < 0) ? settings.customStockTicker.smallDownArrowImagePath : settings.customStockTicker.smallUpArrowImagePath;
              var tickClass = (pricedown < 0) ? 'downtick' : 'uptick';
              var arrowImage = $('<img>').attr('src', arrowImagePath);
              homeStockTicker.find('#stock-chg').addClass(tickClass).html(pricedown).append(arrowImage);

              homeStockTicker.find('#stock-change').html($(this).find('Change').text());
              homeStockTicker.find('#ticker #stock-price').html($(this).find('Trade').text());
              homeStockInfo.find('.stock-date').html(dattm);
              homeStockInfo.find('#stock-percent-change').html(percent_change);
              homeStockInfo.find('#stock-52wk-high').html($(this).find('FiftyTwoWeekHigh').text());
              homeStockInfo.find('#stock-day-high').html($(this).find('High').text());
              homeStockInfo.find('#stock-day-low').html($(this).find('Low').text());
              homeStockInfo.find('#stock-52wk-low').html($(this).find('FiftyTwoWeekLow').text());
              homeStockInfo.find('#stock-market-cap').html(mkt_cap);
              homeStockInfo.find('#stock-open').html($(this).find('Open').text());
              homeStockInfo.find('#stock-prev-close').html($(this).find('PreviousClose').text());
              homeStockInfo.find('#stock-symbol').html($(this).attr('DisplayTicker'));
              homeStockInfo.find('#stock-vol').html(number_format($(this).find('Volume').text() * 1));
              homeStockInfo.find('#stock-exchange').html($(this).attr('Exchange'));
              homeStockTicker.find('#ticker #stock-txt').html($(this).attr('Exchange') + ':').append($('<strong>').html($(this).attr('DisplayTicker')));
            }
          });
        });
      }

    });

  }
};

})(jQuery);
