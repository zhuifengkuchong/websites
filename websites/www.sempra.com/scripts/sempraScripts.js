/*	NAVIGATION ACTIVE STATES
-----------------------------------------------------------*/
$(function(){
	var theurl = location.pathname;
	var thesection = theurl.split("/")[1];
	
	// Add active state to 1st level of top navigation
	if (thesection != ''){
		$("#topNav li#" + thesection).addClass("active");
	}
	
	// Add active state to 2nd level of top navigation
	$("#topNav a").each(function(){
		var hreflink = $(this).attr("href");
		if (hreflink.toLowerCase() == location.pathname.toLowerCase()){
			$(this).parent("li").addClass("active");
		}
	});
	
	// Add active state to left navigation
	if ($("#leftNav").length > 0){
		if(!$("#leftNav").hasClass('notSub')){
			var thesubsection = theurl.split("/")[2].split("-")[0];
			$("#topNav li#" + thesubsection).addClass("active");
		}
		$("#leftNav a").each(function(){
			var hreflink = $(this).attr("href");
			if (hreflink.toLowerCase() == location.pathname.toLowerCase()){
				$(this).parent("li").addClass("active");
				$(this).parents("li").addClass("active-trail");
				$(this).parents("li li").addClass("active");
			}
		});
	}
});

/*	HOME PAGE FEATURE
-----------------------------------------------------------*/
var isComplete = 1;
function changeSlide(slide){
	var linkNum = $(slide).parent().index() + 1;
	var theSlide = $('#featureSlides div:nth-child(' + linkNum + ')');
	// If selected slide isn't current slide
	if ($(theSlide).hasClass('active') == false && isComplete == 1){
		isComplete = 0;
		// Hide all slides except active slide
		$('#featureSlides div').each(
			function(){
				if ($(this).hasClass('active') == false){
					$(this).hide();
				}
			}
		);
		// Increase z-index of next slide
		$(theSlide).addClass('next');
		// Update active nav
		$("#featureNav li").removeClass('active');
		$(slide).parent().addClass('active');
		// Fade in next slide. Once fade is complete, decrease z-index and set to active 
		$(theSlide).fadeIn(750, function(){
			$(theSlide).removeClass('next');
			$('#featureSlides div').each(
				function(){
					$(this).removeClass('active');
				}
			);
			$(theSlide).addClass('active');
			isComplete = 1
		});
	}
}

function nextSlide(){
	if($('#featureNav li').last().hasClass('active')){
		$('#featureNav li').first().children().click();
	} else {
		$('#featureNav li.active').next().children().click();
	}
}

var slideTimer;

$(window).load(function() {
	slideTimer = setInterval("nextSlide()", 4000);
	$('#homeFeature').mouseenter(function(){
		clearInterval(slideTimer);
	});
	$('#homeFeature').mouseleave(function(){
		clearInterval(slideTimer);
		slideTimer = setInterval("nextSlide()", 4000);
	});
});

function stopSlideshow(){
	clearInterval(slideTimer);
}
function startSlideshow(){
	clearInterval(slideTimer);
	slideTimer = setInterval("nextSlide()", 4000);
}