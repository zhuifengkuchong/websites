$('document').ready(function() {
							 
	// homepage spotlight rollovers
	$('.spotlight-item img').hover(function() {
		$('.spotlight-item').removeClass('active');
		$(this).parents('.spotlight-item').addClass('active');
	}, function() {
	});
	
	// homepage image slider
	$('#slider').anythingSlider({
		width:458,
		height:476,
		buildNavigation:false,
		autoPlay:true,
		delay:5000,
		animationTime:700
	});
	
	/* clear mailing list default text value on click */
	document.getElementById('header-search').defaultValue = "Enter Search Term";
	document.getElementById('header-search').value = "Enter Search Term";
	$('#header-search').focus(function() {
		if (this.value == this.defaultValue) {
			this.value="";
		}
	}).blur(function() {
		if (!this.value.length) {
			this.value = this.defaultValue;
		}
	});
	
	// Main Nav funtionality
	var config = {    
    	over: menuOver, // function = onMouseOver callback (REQUIRED) 
    	sensitivity: 15,   
    	timeout: 300, // number = milliseconds delay before onMouseOut
    	out: menuOut // function = onMouseOut callback (REQUIRED)    
	};
	function menuOver() {
		$(".hide-from-ie6").addClass('hide-select');
		if( !$(this).hasClass('active') ) {
			$(this).children('a').css("color","#fff");
		} else {
			$(this).children('a').addClass('dd-hover');
		}
		$(this).children('.dd-container').show();
	}
	function menuOut() {
		$(this).children('.dd-container').fadeOut('fast');
		if( !$(this).hasClass('active') ) {
			$(this).children('a').animate({color:"#555"},200);
		} else {
			$(this).children('a').removeClass('dd-hover');
		}
		$(".hide-from-ie6").removeClass('hide-select');
	}
	$('#mainNav .menu > li').hoverIntent(config);
	
	// Top Nav functionality
	$('#topNav .menu > li').hover(function() {
		if( $(this).children('ul').length > 0 ) {
			$(this).children('a').addClass('dd-hover');
			$(this).children('ul').show();
		}
	}, function() {
		if( $(this).children('ul').length > 0 ) {
			$(this).children('a').removeClass('dd-hover');
			$(this).children('ul').hide();
		}
	});
	
	//Set active menu state
	$('#side-nav a').activeState();
	$('#topNav .menu a').activeState();
	$('#footer .menu a').activeState();
	$('.content-nav a').activeState();
	
	$('#breadcrumbs').breadcrumb();
	$('#mainNav .menu > li > a').activeMainNav();
	$('#side-nav a').activeSideNav();
	//$('.menu').rollover();
	
	// Shareholder.com stock feed
	var ticker = irxmlstockquote[0];
	$('.stockquote_lastprice').html(irxmlfunctions.currencyFormat(ticker.lastprice, 2,',','.',''));
	if(ticker.change > 0){
		$('#topNav .stockquote_change').html('<img src="../images/menu/topmenu/stockarrow_up.png"/*tpa=http://www.sempra.com/images/menu/topmenu/stockarrow_up.png*/ alt="up"> '+irxmlfunctions.currencyFormat(ticker.change, 2,',','.','')); 
		if("#home-stock-info") {
			$('#home-stock-info .stockquote_change').html('<img src="../images/homepage/stockarrow_up.png"/*tpa=http://www.sempra.com/images/homepage/stockarrow_up.png*/ alt="up"> '+irxmlfunctions.currencyFormat(ticker.change, 2,',','.','')); 
		}
	} else if(ticker.change < 0) {
		$('#topNav .stockquote_change').html('<img src="../images/menu/topmenu/stockarrow_down.png"/*tpa=http://www.sempra.com/images/menu/topmenu/stockarrow_down.png*/ alt="down"> '+irxmlfunctions.currencyFormat(ticker.change, 2,',','.','')); 
		if("#home-stock-info") {
			$('#home-stock-info .stockquote_change').html('<img src="../images/homepage/stockarrow_down.png"/*tpa=http://www.sempra.com/images/homepage/stockarrow_down.png*/ alt="down"> '+irxmlfunctions.currencyFormat(ticker.change, 2,',','.','')); 
		}
	} else {
		$('.stockquote_change').html('no change');
	} 
	
	// subpage <aside> image height
	
	$(".subpage aside img").load(function() {
    	var imageHeight = $(this).height() + 80;
		var subContentHeight = $(".subpage #content").height() - imageHeight;
		if(subContentHeight > 0) {
			var testHeight = 0;
			do {
				$("aside").css("margin-bottom",subContentHeight);
				
				var asideHeight = imageHeight + subContentHeight;
				var newContentHeight = $(".subpage #content").height();
				
				testHeight = newContentHeight - asideHeight;
				subContentHeight = subContentHeight + testHeight;
				
				
				//subContentPx = subContentHeight+"px";
				//subContentHeight = $(".subpage #content").height() - imageHeight - 80 - subContentHeight;
			}
			while( testHeight > 20 )

			
			
		}
    });

	
	// $(".date").append("hello");
	
	// homepage financial reports drop down
	if($("#financial-report-selection")) {
		$("#financial-report-selection select").change(function() {
			if( $(this).val().split(".")[1] == "pdf") {
		    	window.location = "http://www.sempra.com/pdf/financial-reports/"+$(this).val();
			} else {
		    	window.location = $(this).val();
			}
		});
	}
	
});

var path = window.location.pathname;

(function($){
	$.fn.activeState = function(){
		
		//var path = window.location.pathname.slice(1);
		return this.each(function(){
			var $a = $(this);
			var href = $a.attr('href');
			var hrefFileName;
			
			//set active state - assumes shared navigation resource
			if(href.lastIndexOf('/') != -1){
				hrefFileName = href.slice( (href.lastIndexOf('/') +1));
			}
			else{
				hrefFileName = href;
			}
			if(href === path){
				$a.attr('href', 'javascript:;');
				$a.css('cursor', 'default');
				$a.addClass('active');
				if( $a.parent().parent('li') ) {
					$a.parent().parent('li').addClass('active-trail');
				}
				if ( $a.parents('.expanded') ) {
					$a.parents('.expanded').parent().addClass('active-trail');
				}
				if( $a.parent('li') ) {
					$a.parent('li').addClass('active');
				}
				return;
			}
		});
	}
})(jQuery);

(function($){
	$.fn.breadcrumb = function(){
		var $pathAry = path.split("/");
		var $breadcrumb="";
		var $crumbPath = "/";
		for ($i=1;$i<$pathAry.length-1;$i++) {
			$crumbPath += $pathAry[$i]+"/";
			
			$pathName = $pathAry[$i].split("-");
			var $crumbName="";
			
			for ($j=0;$j<$pathName.length;$j++) {
				$crumbName += " "+$pathName[$j];
			}
			
			if($i == $pathAry.length-2) {
				$breadcrumb += "<li class='current'>"+$crumbName+"</li>";
			} else {
				$breadcrumb += "<li><a href='"+$crumbPath+"'>"+$crumbName+"</a></li>";
			}
			document.title = 'Sempra Energy | '+ucwords($crumbName);
		}
		$(this).append($breadcrumb);
	}
})(jQuery);

function ucwords (str) {
    return (str + '').replace(/^([a-z])|\s+([a-z])/g, function ($1) {
        return $1.toUpperCase();
    });
}

(function($){
	$.fn.activeMainNav = function(){
		this.each(function(){
			var $pathAry = path.split("/");
			var $section = "/"+$pathAry[1]+"/";
			var $a = $(this);
			var href = $a.attr('href');
			if(href == $section) {
				$a.parent('li').addClass('active');
			}
		});
	}
})(jQuery);

(function($){
	$.fn.activeSideNav = function(){
		var $pathAry = path.split("/");
		var $section = "";
		for(var $i=1; $i<4;$i++) {
			$section += "/"+$pathAry[$i];
		}
		$section += "/";
		if( $pathAry[4] ) {
			this.each(function(){
				var $a = $(this);
				var href = $a.attr('href');
				if(href == $section) {
					$a.addClass('active');
					if ( $a.parents('.expanded') ) {
						$a.parents('.expanded').parent().addClass('active-trail');
					}
				}
			});
		}
	}
})(jQuery);