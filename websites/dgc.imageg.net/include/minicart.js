(function($){ 
			var cartLoaded = false,
            itemInCart=0;
			var miniCartSubmit = window.miniCartSubmit = function(formObject,redirect) {	
			
			if (document.location.href.indexOf('product') > -1) {
					hideCart();	
				}
            $.getJSON('http://dgc.imageg.net/cartHandler/ajax.jsp', $(formObject).serialize() + '&async=true&no_cache=' + new Date().getTime(), function (json) { 
	            itemInCart=json.itemCount;
				if (json.rdir) {
                    window.location.href = json.rdir;
                } else { 
						if(json.itemCount==1){
							$("#myCart #myCartInfo #ItemCount").text(json.itemCount+" item, "); 
							}else{
							$("#myCart #myCartInfo #ItemCount").text(json.itemCount+" items, ");	
							}
						$("#myCart #myCartInfo #cartPrice").text(json.itemTotValue.replace('$ ','$'));
						if(json.itemCount>0){
												$('#myCart').addClass("open");
												cartLoaded=true;
											}
                        if(redirect) {
                            window.location.href = 'http://dgc.imageg.net/cart/index.jsp' + '?an_action=viewCart&ias2VwCartSkusAdded=' + json.skusAdded;
                        }
                        else {
                            window.minicartJson = json;
                            showCart(true, true); 
                            //showCart(true); 
                            
                            /* PLD 132295 : minicart doesn't scroll into view in Chrome browser */
                            if(navigator.userAgent.indexOf('Chrome/') > -1){
                                $('html, body').animate({
                                    scrollTop: 0
                                });
                            }
                        }
                } 
            }).error(function() {
                $(formObject).append('<input type="hidden" name="showProductInCart" value="true"/>').submit();
            });
			};
            
            
            var hideCart = function() {
               $('#minicart').hide();   
               $('#myCart').removeClass("open");
            };
            
            
			var Itemcount=$("#myCart #myCartInfo").text().charAt(0);
			
			
            /* PLD 132295 */
            var miniCartAlign = function(){ 
                
                // correct vertical scroll bar being cutoff in IE
                var legacy_ie = typeof(window.attachEvent) != "undefined" && !!$(".no-boxshadow").length;
                var mc = $("#minicart");
                if(legacy_ie){
                    try{
                        var items = $("#minicart8_content").eq(0).children("div");
                        var html = $("html");
                        if(items.length > 3 && !mc.hasClass("vert_scroll")){
                            mc.addClass("vert_scroll");
                        }
                        else if(items.length <= 3) {
                            mc.removeClass("vert_scroll");
                        }
                        if(!html.hasClass("ie")) html.addClass("ie");
                    }
                    catch(err){ alert(err.message)/* fail silently */}
                }
                
                // horizontally center minicart drop down
                try{
                    var adj = $("#myCart").outerWidth() - mc.outerWidth();
                    if(adj > 0){
                        mc.css({
                            "right" : (parseInt(adj/2) - 1) + "px"
                        });
                    }
                    else if(legacy_ie) {
                        mc.css({
                            "right" : (parseInt(adj/2)) + "px"
                        });
                    }
                }
                catch(err){/* fail silently */}
                
            }; // END miniCartAlign
            
            // mark for browser
            if(!!$.browser.msie && !$("html").hasClass("ie")) $("html").addClass("ie");
            
            /* END PLD 132295 */
			
			
        var showCart = function(init,cartAddAnalytics) { 
            if (cartLoaded && !init) {
                
                if (!(document.location.href.indexOf('product')) > -1) {
                    $('#minicart').show();
                }
                
            } else {
                $('#minicart').load('http://dgc.imageg.net/minicart/index.jsp', {includeCartAnalytics: cartAddAnalytics , t: new Date().getTime()}, function (response, status, xhr) {
                    if (status == "error") {
                        //nothing
                    }else{
                        
                        if (!(document.location.href.indexOf('product')>-1)) {
                            $('#minicart').show();
                            
                            /* PLD 132295 : align minicart drop down */
                            miniCartAlign();
                        }
                        
                        var countItem=$('#minicart .minicart8_white').length;
                        if(countItem>=1 && countItem<=3) {
                            $('#minicart8_content').addClass("cartLoaded sumcountItem"+countItem);
                        }else if(countItem>=4) {
                            $('#minicart8_content').addClass("cartLoaded sumMoreItem");
                        }
                        
                        var cartTop = $('#minicart').parent().position().top;
                        bodyelem = $('html, body');
                        if ($.browser.safari) {
                            bodyelem = $("body");
                        }
                        if (cartTop < bodyelem.scrollTop()) {
                            bodyelem.animate({scrollTop: cartTop}, 'slow');
                        }
                        
                        if (init) {
                            setMinicartTimeout(minicartTimeOut);
                        }
                        
                    }
                });
            }
            
            /* PLD 132295 : align minicart drop down */
            miniCartAlign();
        };

        var miniCartCloseTime;
        var minicartCloseTimer;
        var setMinicartTimeout = function(duration) {
            var now = new Date().getTime();
            miniCartCloseTime = now + duration;
            clearTimeout(minicartCloseTimer);
            minicartCloseTimer = setTimeout(hideCart, miniCartCloseTime - now);
        };


        $('#myCart').mouseenter(function(e) {
            
            if(!$(this).hasClass("open") && Itemcount>0){					 
                $('#myCart').addClass("open");
                $('#minicart').show();
                showCart(false,false);
                
                /* PLD 132295 horizontally center minicart drop down */
                miniCartAlign();
                
            }
            clearTimeout(minicartCloseTimer);
            
        }).mouseleave(function(e) {
            
            var now = new Date().getTime();
            if (now > miniCartCloseTime) {
                $('#myCart').removeClass("open"); 
                hideCart();
            } else {
                clearTimeout(minicartCloseTimer);
                minicartCloseTimer = setTimeout(hideCart, miniCartCloseTime - now);
            }
            
        });

    })(jQuery);
