<script id = "race62a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race62={};
	myVars.races.race62.varName="Window[26].Page_InvalidControlToBeFocused";
	myVars.races.race62.varType="@varType@";
	myVars.races.race62.repairType = "@RepairType";
	myVars.races.race62.event1={};
	myVars.races.race62.event2={};
	myVars.races.race62.event1.id = "ContactUsSublayout1_tbCountry";
	myVars.races.race62.event1.type = "onchange";
	myVars.races.race62.event1.loc = "ContactUsSublayout1_tbCountry_LOC";
	myVars.races.race62.event1.isRead = "False";
	myVars.races.race62.event1.eventType = "@event1EventType@";
	myVars.races.race62.event2.id = "ContactUsSublayout1_tbAddress";
	myVars.races.race62.event2.type = "onchange";
	myVars.races.race62.event2.loc = "ContactUsSublayout1_tbAddress_LOC";
	myVars.races.race62.event2.isRead = "False";
	myVars.races.race62.event2.eventType = "@event2EventType@";
	myVars.races.race62.event1.executed= false;// true to disable, false to enable
	myVars.races.race62.event2.executed= false;// true to disable, false to enable
</script>

