<script id = "race80a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race80={};
	myVars.races.race80.varName="DOMNode[0x7f0a6ee24c60].isvalid";
	myVars.races.race80.varType="@varType@";
	myVars.races.race80.repairType = "@RepairType";
	myVars.races.race80.event1={};
	myVars.races.race80.event2={};
	myVars.races.race80.event1.id = "ContactUsSublayout1_tbLastName";
	myVars.races.race80.event1.type = "onchange";
	myVars.races.race80.event1.loc = "ContactUsSublayout1_tbLastName_LOC";
	myVars.races.race80.event1.isRead = "True";
	myVars.races.race80.event1.eventType = "@event1EventType@";
	myVars.races.race80.event2.id = "ContactUsSublayout1_tbZip";
	myVars.races.race80.event2.type = "onchange";
	myVars.races.race80.event2.loc = "ContactUsSublayout1_tbZip_LOC";
	myVars.races.race80.event2.isRead = "False";
	myVars.races.race80.event2.eventType = "@event2EventType@";
	myVars.races.race80.event1.executed= false;// true to disable, false to enable
	myVars.races.race80.event2.executed= false;// true to disable, false to enable
</script>

