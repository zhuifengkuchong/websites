<script id = "race51b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race51={};
	myVars.races.race51.varName="Window[26].Page_InvalidControlToBeFocused";
	myVars.races.race51.varType="@varType@";
	myVars.races.race51.repairType = "@RepairType";
	myVars.races.race51.event1={};
	myVars.races.race51.event2={};
	myVars.races.race51.event1.id = "ContactUsSublayout1_tbEmail";
	myVars.races.race51.event1.type = "onchange";
	myVars.races.race51.event1.loc = "ContactUsSublayout1_tbEmail_LOC";
	myVars.races.race51.event1.isRead = "False";
	myVars.races.race51.event1.eventType = "@event1EventType@";
	myVars.races.race51.event2.id = "ContactUsSublayout1_tbPhone";
	myVars.races.race51.event2.type = "onchange";
	myVars.races.race51.event2.loc = "ContactUsSublayout1_tbPhone_LOC";
	myVars.races.race51.event2.isRead = "False";
	myVars.races.race51.event2.eventType = "@event2EventType@";
	myVars.races.race51.event1.executed= false;// true to disable, false to enable
	myVars.races.race51.event2.executed= false;// true to disable, false to enable
</script>

