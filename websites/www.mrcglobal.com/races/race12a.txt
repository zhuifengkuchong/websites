<script id = "race12a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race12={};
	myVars.races.race12.varName="RequestLiterature1_tbLastName__onkeypress";
	myVars.races.race12.varType="@varType@";
	myVars.races.race12.repairType = "@RepairType";
	myVars.races.race12.event1={};
	myVars.races.race12.event2={};
	myVars.races.race12.event1.id = "Lu_Id_script_17";
	myVars.races.race12.event1.type = "Lu_Id_script_17__parsed";
	myVars.races.race12.event1.loc = "Lu_Id_script_17_LOC";
	myVars.races.race12.event1.isRead = "False";
	myVars.races.race12.event1.eventType = "@event1EventType@";
	myVars.races.race12.event2.id = "RequestLiterature1_tbLastName";
	myVars.races.race12.event2.type = "onkeypress";
	myVars.races.race12.event2.loc = "RequestLiterature1_tbLastName_LOC";
	myVars.races.race12.event2.isRead = "True";
	myVars.races.race12.event2.eventType = "@event2EventType@";
	myVars.races.race12.event1.executed= false;// true to disable, false to enable
	myVars.races.race12.event2.executed= false;// true to disable, false to enable
</script>

