<script id = "race83a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race83={};
	myVars.races.race83.varName="Window[26].Page_InvalidControlToBeFocused";
	myVars.races.race83.varType="@varType@";
	myVars.races.race83.repairType = "@RepairType";
	myVars.races.race83.event1={};
	myVars.races.race83.event2={};
	myVars.races.race83.event1.id = "ContactUsSublayout1_tbZip";
	myVars.races.race83.event1.type = "onchange";
	myVars.races.race83.event1.loc = "ContactUsSublayout1_tbZip_LOC";
	myVars.races.race83.event1.isRead = "False";
	myVars.races.race83.event1.eventType = "@event1EventType@";
	myVars.races.race83.event2.id = "RequestLiterature1_tbFirstName";
	myVars.races.race83.event2.type = "onchange";
	myVars.races.race83.event2.loc = "RequestLiterature1_tbFirstName_LOC";
	myVars.races.race83.event2.isRead = "False";
	myVars.races.race83.event2.eventType = "@event2EventType@";
	myVars.races.race83.event1.executed= false;// true to disable, false to enable
	myVars.races.race83.event2.executed= false;// true to disable, false to enable
</script>

