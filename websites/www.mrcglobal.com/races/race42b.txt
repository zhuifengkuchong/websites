<script id = "race42b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race42={};
	myVars.races.race42.varName="Window[26].Page_InvalidControlToBeFocused";
	myVars.races.race42.varType="@varType@";
	myVars.races.race42.repairType = "@RepairType";
	myVars.races.race42.event1={};
	myVars.races.race42.event2={};
	myVars.races.race42.event1.id = "ContactUsSublayout1_tbTitle";
	myVars.races.race42.event1.type = "onchange";
	myVars.races.race42.event1.loc = "ContactUsSublayout1_tbTitle_LOC";
	myVars.races.race42.event1.isRead = "False";
	myVars.races.race42.event1.eventType = "@event1EventType@";
	myVars.races.race42.event2.id = "ContactUsSublayout1_tbLastName";
	myVars.races.race42.event2.type = "onchange";
	myVars.races.race42.event2.loc = "ContactUsSublayout1_tbLastName_LOC";
	myVars.races.race42.event2.isRead = "False";
	myVars.races.race42.event2.eventType = "@event2EventType@";
	myVars.races.race42.event1.executed= false;// true to disable, false to enable
	myVars.races.race42.event2.executed= false;// true to disable, false to enable
</script>

