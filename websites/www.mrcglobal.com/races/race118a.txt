<script id = "race118a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race118={};
	myVars.races.race118.varName="Window[26].Page_InvalidControlToBeFocused";
	myVars.races.race118.varType="@varType@";
	myVars.races.race118.repairType = "@RepairType";
	myVars.races.race118.event1={};
	myVars.races.race118.event2={};
	myVars.races.race118.event1.id = "RequestLiterature1_tbPhone";
	myVars.races.race118.event1.type = "onchange";
	myVars.races.race118.event1.loc = "RequestLiterature1_tbPhone_LOC";
	myVars.races.race118.event1.isRead = "False";
	myVars.races.race118.event1.eventType = "@event1EventType@";
	myVars.races.race118.event2.id = "RequestLiterature1_tbEmail";
	myVars.races.race118.event2.type = "onchange";
	myVars.races.race118.event2.loc = "RequestLiterature1_tbEmail_LOC";
	myVars.races.race118.event2.isRead = "False";
	myVars.races.race118.event2.eventType = "@event2EventType@";
	myVars.races.race118.event1.executed= false;// true to disable, false to enable
	myVars.races.race118.event2.executed= false;// true to disable, false to enable
</script>

