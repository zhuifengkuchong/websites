<script id = "race17b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race17={};
	myVars.races.race17.varName="RequestLiterature1_tbCountry__onkeypress";
	myVars.races.race17.varType="@varType@";
	myVars.races.race17.repairType = "@RepairType";
	myVars.races.race17.event1={};
	myVars.races.race17.event2={};
	myVars.races.race17.event1.id = "RequestLiterature1_tbCountry";
	myVars.races.race17.event1.type = "onkeypress";
	myVars.races.race17.event1.loc = "RequestLiterature1_tbCountry_LOC";
	myVars.races.race17.event1.isRead = "True";
	myVars.races.race17.event1.eventType = "@event1EventType@";
	myVars.races.race17.event2.id = "Lu_Id_script_17";
	myVars.races.race17.event2.type = "Lu_Id_script_17__parsed";
	myVars.races.race17.event2.loc = "Lu_Id_script_17_LOC";
	myVars.races.race17.event2.isRead = "False";
	myVars.races.race17.event2.eventType = "@event2EventType@";
	myVars.races.race17.event1.executed= false;// true to disable, false to enable
	myVars.races.race17.event2.executed= false;// true to disable, false to enable
</script>

