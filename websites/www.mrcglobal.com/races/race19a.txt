<script id = "race19a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race19={};
	myVars.races.race19.varName="RequestLiterature1_tbCity__onkeypress";
	myVars.races.race19.varType="@varType@";
	myVars.races.race19.repairType = "@RepairType";
	myVars.races.race19.event1={};
	myVars.races.race19.event2={};
	myVars.races.race19.event1.id = "Lu_Id_script_17";
	myVars.races.race19.event1.type = "Lu_Id_script_17__parsed";
	myVars.races.race19.event1.loc = "Lu_Id_script_17_LOC";
	myVars.races.race19.event1.isRead = "False";
	myVars.races.race19.event1.eventType = "@event1EventType@";
	myVars.races.race19.event2.id = "RequestLiterature1_tbCity";
	myVars.races.race19.event2.type = "onkeypress";
	myVars.races.race19.event2.loc = "RequestLiterature1_tbCity_LOC";
	myVars.races.race19.event2.isRead = "True";
	myVars.races.race19.event2.eventType = "@event2EventType@";
	myVars.races.race19.event1.executed= false;// true to disable, false to enable
	myVars.races.race19.event2.executed= false;// true to disable, false to enable
</script>

