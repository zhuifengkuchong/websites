<script id = "race132a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race132={};
	myVars.races.race132.varName="Window[26].Page_InvalidControlToBeFocused";
	myVars.races.race132.varType="@varType@";
	myVars.races.race132.repairType = "@RepairType";
	myVars.races.race132.event1={};
	myVars.races.race132.event2={};
	myVars.races.race132.event1.id = "RequestLiterature1_tbCountry";
	myVars.races.race132.event1.type = "onchange";
	myVars.races.race132.event1.loc = "RequestLiterature1_tbCountry_LOC";
	myVars.races.race132.event1.isRead = "False";
	myVars.races.race132.event1.eventType = "@event1EventType@";
	myVars.races.race132.event2.id = "RequestLiterature1_tbAddress";
	myVars.races.race132.event2.type = "onchange";
	myVars.races.race132.event2.loc = "RequestLiterature1_tbAddress_LOC";
	myVars.races.race132.event2.isRead = "False";
	myVars.races.race132.event2.eventType = "@event2EventType@";
	myVars.races.race132.event1.executed= false;// true to disable, false to enable
	myVars.races.race132.event2.executed= false;// true to disable, false to enable
</script>

