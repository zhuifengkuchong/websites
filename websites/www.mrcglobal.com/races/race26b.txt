<script id = "race26b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race26={};
	myVars.races.race26.varName="ContactUsSublayout1_tbEmail__onchange";
	myVars.races.race26.varType="@varType@";
	myVars.races.race26.repairType = "@RepairType";
	myVars.races.race26.event1={};
	myVars.races.race26.event2={};
	myVars.races.race26.event1.id = "ContactUsSublayout1_tbEmail";
	myVars.races.race26.event1.type = "onchange";
	myVars.races.race26.event1.loc = "ContactUsSublayout1_tbEmail_LOC";
	myVars.races.race26.event1.isRead = "True";
	myVars.races.race26.event1.eventType = "@event1EventType@";
	myVars.races.race26.event2.id = "Lu_Id_script_17";
	myVars.races.race26.event2.type = "Lu_Id_script_17__parsed";
	myVars.races.race26.event2.loc = "Lu_Id_script_17_LOC";
	myVars.races.race26.event2.isRead = "False";
	myVars.races.race26.event2.eventType = "@event2EventType@";
	myVars.races.race26.event1.executed= false;// true to disable, false to enable
	myVars.races.race26.event2.executed= false;// true to disable, false to enable
</script>

