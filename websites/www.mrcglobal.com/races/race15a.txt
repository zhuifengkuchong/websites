<script id = "race15a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race15={};
	myVars.races.race15.varName="RequestLiterature1_tbPhone__onkeypress";
	myVars.races.race15.varType="@varType@";
	myVars.races.race15.repairType = "@RepairType";
	myVars.races.race15.event1={};
	myVars.races.race15.event2={};
	myVars.races.race15.event1.id = "Lu_Id_script_17";
	myVars.races.race15.event1.type = "Lu_Id_script_17__parsed";
	myVars.races.race15.event1.loc = "Lu_Id_script_17_LOC";
	myVars.races.race15.event1.isRead = "False";
	myVars.races.race15.event1.eventType = "@event1EventType@";
	myVars.races.race15.event2.id = "RequestLiterature1_tbPhone";
	myVars.races.race15.event2.type = "onkeypress";
	myVars.races.race15.event2.loc = "RequestLiterature1_tbPhone_LOC";
	myVars.races.race15.event2.isRead = "True";
	myVars.races.race15.event2.eventType = "@event2EventType@";
	myVars.races.race15.event1.executed= false;// true to disable, false to enable
	myVars.races.race15.event2.executed= false;// true to disable, false to enable
</script>

