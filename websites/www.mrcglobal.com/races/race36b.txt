<script id = "race36b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race36={};
	myVars.races.race36.varName="RequestLiterature1_tbEmail__onchange";
	myVars.races.race36.varType="@varType@";
	myVars.races.race36.repairType = "@RepairType";
	myVars.races.race36.event1={};
	myVars.races.race36.event2={};
	myVars.races.race36.event1.id = "RequestLiterature1_tbEmail";
	myVars.races.race36.event1.type = "onchange";
	myVars.races.race36.event1.loc = "RequestLiterature1_tbEmail_LOC";
	myVars.races.race36.event1.isRead = "True";
	myVars.races.race36.event1.eventType = "@event1EventType@";
	myVars.races.race36.event2.id = "Lu_Id_script_17";
	myVars.races.race36.event2.type = "Lu_Id_script_17__parsed";
	myVars.races.race36.event2.loc = "Lu_Id_script_17_LOC";
	myVars.races.race36.event2.isRead = "False";
	myVars.races.race36.event2.eventType = "@event2EventType@";
	myVars.races.race36.event1.executed= false;// true to disable, false to enable
	myVars.races.race36.event2.executed= false;// true to disable, false to enable
</script>

