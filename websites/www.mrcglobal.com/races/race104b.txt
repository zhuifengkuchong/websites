<script id = "race104b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race104={};
	myVars.races.race104.varName="Window[26].Page_InvalidControlToBeFocused";
	myVars.races.race104.varType="@varType@";
	myVars.races.race104.repairType = "@RepairType";
	myVars.races.race104.event1={};
	myVars.races.race104.event2={};
	myVars.races.race104.event1.id = "RequestLiterature1_tbCompany";
	myVars.races.race104.event1.type = "onchange";
	myVars.races.race104.event1.loc = "RequestLiterature1_tbCompany_LOC";
	myVars.races.race104.event1.isRead = "False";
	myVars.races.race104.event1.eventType = "@event1EventType@";
	myVars.races.race104.event2.id = "RequestLiterature1_tbTitle";
	myVars.races.race104.event2.type = "onchange";
	myVars.races.race104.event2.loc = "RequestLiterature1_tbTitle_LOC";
	myVars.races.race104.event2.isRead = "False";
	myVars.races.race104.event2.eventType = "@event2EventType@";
	myVars.races.race104.event1.executed= false;// true to disable, false to enable
	myVars.races.race104.event2.executed= false;// true to disable, false to enable
</script>

