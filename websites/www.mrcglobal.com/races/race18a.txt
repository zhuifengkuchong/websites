<script id = "race18a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race18={};
	myVars.races.race18.varName="RequestLiterature1_tbAddress__onkeypress";
	myVars.races.race18.varType="@varType@";
	myVars.races.race18.repairType = "@RepairType";
	myVars.races.race18.event1={};
	myVars.races.race18.event2={};
	myVars.races.race18.event1.id = "Lu_Id_script_17";
	myVars.races.race18.event1.type = "Lu_Id_script_17__parsed";
	myVars.races.race18.event1.loc = "Lu_Id_script_17_LOC";
	myVars.races.race18.event1.isRead = "False";
	myVars.races.race18.event1.eventType = "@event1EventType@";
	myVars.races.race18.event2.id = "RequestLiterature1_tbAddress";
	myVars.races.race18.event2.type = "onkeypress";
	myVars.races.race18.event2.loc = "RequestLiterature1_tbAddress_LOC";
	myVars.races.race18.event2.isRead = "True";
	myVars.races.race18.event2.eventType = "@event2EventType@";
	myVars.races.race18.event1.executed= false;// true to disable, false to enable
	myVars.races.race18.event2.executed= false;// true to disable, false to enable
</script>

