var Q4Utils = Q4Utils || {};

Q4Utils.convertToServerDate = function (dateTime) {
    var milliseconds = dateTime.getTime() - dateTime.getTimezoneOffset() * 60 * 1000;
    return '/Date(' + milliseconds + ')/';
};

Q4Utils.convertToClientDate = function (serverDateString) {
    return new Date(serverDateString);
};

Q4Utils.cookie = function (key, value, options) {

    // key and at least value given, set cookie...
    if (arguments.length > 1 && (!/Object/.test(Object.prototype.toString.call(value)) || value === null || value === undefined)) {

        if (options === null || options === undefined) {
            options = {};
        }
        
        if (value === null || value === undefined) {
            options.expires = -1;
        }

        if (typeof options.expires === 'number') {
            var days = options.expires, t = options.expires = new Date();
            t.setDate(t.getDate() + days);
        }

        value = String(value);

        return (document.cookie = [
					encodeURIComponent(key), '=', options.raw ? value : encodeURIComponent(value),
					options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
					options.path ? '; path=' + options.path : '',
					options.domain ? '; domain=' + options.domain : '',
					options.secure ? '; secure' : ''
				].join(''));
    }

    // key and possibly options given, get cookie...
    options = value || {};
    var decode = options.raw ? function (s) { return s; } : decodeURIComponent;

    var pairs = document.cookie.split('; ');
    for (var i = 0, pair; pair = pairs[i] && pairs[i].split('='); i++) {
        if (decode(pair[0]) === key) return decode(pair[1] || ''); // IE saves cookies with empty string as "c; ", e.g. without "=" as opposed to EOMB, thus pair[1] may be undefined
    }
    return null;
};

Q4Utils.qs = function () {
    var href = location.href,
        arr = href.substring(href.indexOf('?') + 1).split(/[=&]/),
        ret = {}, i;
    for (i = 0; i < arr.length; ++i) {
        if (i % 2 === 0) {
            ret[arr[i]];
        } else {
            ret[arr[i - 1]] = decodeURIComponent(arr[i]);
        }
    }
    return ret;
};