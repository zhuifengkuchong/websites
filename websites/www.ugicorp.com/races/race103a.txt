<script id = "race103a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race103={};
	myVars.races.race103.varName="_ctrl0_ctl24_RadMenu1_m3_m1_m2__onfocus";
	myVars.races.race103.varType="@varType@";
	myVars.races.race103.repairType = "@RepairType";
	myVars.races.race103.event1={};
	myVars.races.race103.event2={};
	myVars.races.race103.event1.id = "_ctrl0_ctl24_RadMenu1_m3_m1";
	myVars.races.race103.event1.type = "onfocus";
	myVars.races.race103.event1.loc = "_ctrl0_ctl24_RadMenu1_m3_m1_LOC";
	myVars.races.race103.event1.isRead = "False";
	myVars.races.race103.event1.eventType = "@event1EventType@";
	myVars.races.race103.event2.id = "_ctrl0_ctl24_RadMenu1_m3_m1_m2";
	myVars.races.race103.event2.type = "onfocus";
	myVars.races.race103.event2.loc = "_ctrl0_ctl24_RadMenu1_m3_m1_m2_LOC";
	myVars.races.race103.event2.isRead = "True";
	myVars.races.race103.event2.eventType = "@event2EventType@";
	myVars.races.race103.event1.executed= false;// true to disable, false to enable
	myVars.races.race103.event2.executed= false;// true to disable, false to enable
</script>

