<script id = "race19a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race19={};
	myVars.races.race19.varName="_ctrl0_ctl24_RadMenu1_m1_m5__onkeydown";
	myVars.races.race19.varType="@varType@";
	myVars.races.race19.repairType = "@RepairType";
	myVars.races.race19.event1={};
	myVars.races.race19.event2={};
	myVars.races.race19.event1.id = "_ctrl0_ctl24_RadMenu1_m1";
	myVars.races.race19.event1.type = "onfocus";
	myVars.races.race19.event1.loc = "_ctrl0_ctl24_RadMenu1_m1_LOC";
	myVars.races.race19.event1.isRead = "False";
	myVars.races.race19.event1.eventType = "@event1EventType@";
	myVars.races.race19.event2.id = "_ctrl0_ctl24_RadMenu1_m1_m5";
	myVars.races.race19.event2.type = "onkeydown";
	myVars.races.race19.event2.loc = "_ctrl0_ctl24_RadMenu1_m1_m5_LOC";
	myVars.races.race19.event2.isRead = "True";
	myVars.races.race19.event2.eventType = "@event2EventType@";
	myVars.races.race19.event1.executed= false;// true to disable, false to enable
	myVars.races.race19.event2.executed= false;// true to disable, false to enable
</script>

