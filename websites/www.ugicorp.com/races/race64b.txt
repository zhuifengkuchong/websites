<script id = "race64b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race64={};
	myVars.races.race64.varName="_ctrl0_ctl24_RadMenu1_m1_m7_m4__onkeydown";
	myVars.races.race64.varType="@varType@";
	myVars.races.race64.repairType = "@RepairType";
	myVars.races.race64.event1={};
	myVars.races.race64.event2={};
	myVars.races.race64.event1.id = "_ctrl0_ctl24_RadMenu1_m1_m7_m4";
	myVars.races.race64.event1.type = "onkeydown";
	myVars.races.race64.event1.loc = "_ctrl0_ctl24_RadMenu1_m1_m7_m4_LOC";
	myVars.races.race64.event1.isRead = "True";
	myVars.races.race64.event1.eventType = "@event1EventType@";
	myVars.races.race64.event2.id = "_ctrl0_ctl24_RadMenu1_m1_m7";
	myVars.races.race64.event2.type = "onfocus";
	myVars.races.race64.event2.loc = "_ctrl0_ctl24_RadMenu1_m1_m7_LOC";
	myVars.races.race64.event2.isRead = "False";
	myVars.races.race64.event2.eventType = "@event2EventType@";
	myVars.races.race64.event1.executed= false;// true to disable, false to enable
	myVars.races.race64.event2.executed= false;// true to disable, false to enable
</script>

