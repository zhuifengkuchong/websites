<script id = "race250b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race250={};
	myVars.races.race250.varName="Object[22568]._domEventHandlingEnabled";
	myVars.races.race250.varType="@varType@";
	myVars.races.race250.repairType = "@RepairType";
	myVars.races.race250.event1={};
	myVars.races.race250.event2={};
	myVars.races.race250.event1.id = "_ctrl0_ctl24_RadMenu1_m3_m1_m2";
	myVars.races.race250.event1.type = "onkeydown";
	myVars.races.race250.event1.loc = "_ctrl0_ctl24_RadMenu1_m3_m1_m2_LOC";
	myVars.races.race250.event1.isRead = "True";
	myVars.races.race250.event1.eventType = "@event1EventType@";
	myVars.races.race250.event2.id = "Lu_Id_script_17";
	myVars.races.race250.event2.type = "Lu_Id_script_17__parsed";
	myVars.races.race250.event2.loc = "Lu_Id_script_17_LOC";
	myVars.races.race250.event2.isRead = "False";
	myVars.races.race250.event2.eventType = "@event2EventType@";
	myVars.races.race250.event1.executed= false;// true to disable, false to enable
	myVars.races.race250.event2.executed= false;// true to disable, false to enable
</script>

