<script id = "race391a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race391={};
	myVars.races.race391.varName="Object[19489].Focused";
	myVars.races.race391.varType="@varType@";
	myVars.races.race391.repairType = "@RepairType";
	myVars.races.race391.event1={};
	myVars.races.race391.event2={};
	myVars.races.race391.event1.id = "_ctrl0_ctl24_RadMenu1_m0_m0";
	myVars.races.race391.event1.type = "onfocus";
	myVars.races.race391.event1.loc = "_ctrl0_ctl24_RadMenu1_m0_m0_LOC";
	myVars.races.race391.event1.isRead = "False";
	myVars.races.race391.event1.eventType = "@event1EventType@";
	myVars.races.race391.event2.id = "_ctrl0_ctl24_RadMenu1_m0_m0";
	myVars.races.race391.event2.type = "onblur";
	myVars.races.race391.event2.loc = "_ctrl0_ctl24_RadMenu1_m0_m0_LOC";
	myVars.races.race391.event2.isRead = "False";
	myVars.races.race391.event2.eventType = "@event2EventType@";
	myVars.races.race391.event1.executed= false;// true to disable, false to enable
	myVars.races.race391.event2.executed= false;// true to disable, false to enable
</script>

