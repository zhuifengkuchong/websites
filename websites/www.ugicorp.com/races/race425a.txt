<script id = "race425a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race425={};
	myVars.races.race425.varName="Object[20644].Focused";
	myVars.races.race425.varType="@varType@";
	myVars.races.race425.repairType = "@RepairType";
	myVars.races.race425.event1={};
	myVars.races.race425.event2={};
	myVars.races.race425.event1.id = "_ctrl0_ctl24_RadMenu1_m1_m5";
	myVars.races.race425.event1.type = "onfocus";
	myVars.races.race425.event1.loc = "_ctrl0_ctl24_RadMenu1_m1_m5_LOC";
	myVars.races.race425.event1.isRead = "False";
	myVars.races.race425.event1.eventType = "@event1EventType@";
	myVars.races.race425.event2.id = "_ctrl0_ctl24_RadMenu1_m1_m5";
	myVars.races.race425.event2.type = "onblur";
	myVars.races.race425.event2.loc = "_ctrl0_ctl24_RadMenu1_m1_m5_LOC";
	myVars.races.race425.event2.isRead = "False";
	myVars.races.race425.event2.eventType = "@event2EventType@";
	myVars.races.race425.event1.executed= false;// true to disable, false to enable
	myVars.races.race425.event2.executed= false;// true to disable, false to enable
</script>

