<script id = "race421a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race421={};
	myVars.races.race421.varName="Object[20335].Focused";
	myVars.races.race421.varType="@varType@";
	myVars.races.race421.repairType = "@RepairType";
	myVars.races.race421.event1={};
	myVars.races.race421.event2={};
	myVars.races.race421.event1.id = "_ctrl0_ctl24_RadMenu1_m1_m4";
	myVars.races.race421.event1.type = "onfocus";
	myVars.races.race421.event1.loc = "_ctrl0_ctl24_RadMenu1_m1_m4_LOC";
	myVars.races.race421.event1.isRead = "False";
	myVars.races.race421.event1.eventType = "@event1EventType@";
	myVars.races.race421.event2.id = "_ctrl0_ctl24_RadMenu1_m1_m4";
	myVars.races.race421.event2.type = "onblur";
	myVars.races.race421.event2.loc = "_ctrl0_ctl24_RadMenu1_m1_m4_LOC";
	myVars.races.race421.event2.isRead = "False";
	myVars.races.race421.event2.eventType = "@event2EventType@";
	myVars.races.race421.event1.executed= false;// true to disable, false to enable
	myVars.races.race421.event2.executed= false;// true to disable, false to enable
</script>

