<script id = "race127a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race127={};
	myVars.races.race127.varName="Lu_DOM__onblur";
	myVars.races.race127.varType="@varType@";
	myVars.races.race127.repairType = "@RepairType";
	myVars.races.race127.event1={};
	myVars.races.race127.event2={};
	myVars.races.race127.event1.id = "_ctrl0_ctl24_RadMenu1_m3_m1";
	myVars.races.race127.event1.type = "onfocus";
	myVars.races.race127.event1.loc = "_ctrl0_ctl24_RadMenu1_m3_m1_LOC";
	myVars.races.race127.event1.isRead = "False";
	myVars.races.race127.event1.eventType = "@event1EventType@";
	myVars.races.race127.event2.id = "_ctrl0_ctl24_RadMenu1_m1_m1";
	myVars.races.race127.event2.type = "onblur";
	myVars.races.race127.event2.loc = "_ctrl0_ctl24_RadMenu1_m1_m1_LOC";
	myVars.races.race127.event2.isRead = "True";
	myVars.races.race127.event2.eventType = "@event2EventType@";
	myVars.races.race127.event1.executed= false;// true to disable, false to enable
	myVars.races.race127.event2.executed= false;// true to disable, false to enable
</script>

