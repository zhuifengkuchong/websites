<script id = "race100a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race100={};
	myVars.races.race100.varName="_ctrl0_ctl24_RadMenu1_m1_m7_m9__onfocus";
	myVars.races.race100.varType="@varType@";
	myVars.races.race100.repairType = "@RepairType";
	myVars.races.race100.event1={};
	myVars.races.race100.event2={};
	myVars.races.race100.event1.id = "_ctrl0_ctl24_RadMenu1_m1_m7";
	myVars.races.race100.event1.type = "onfocus";
	myVars.races.race100.event1.loc = "_ctrl0_ctl24_RadMenu1_m1_m7_LOC";
	myVars.races.race100.event1.isRead = "False";
	myVars.races.race100.event1.eventType = "@event1EventType@";
	myVars.races.race100.event2.id = "_ctrl0_ctl24_RadMenu1_m1_m7_m9";
	myVars.races.race100.event2.type = "onfocus";
	myVars.races.race100.event2.loc = "_ctrl0_ctl24_RadMenu1_m1_m7_m9_LOC";
	myVars.races.race100.event2.isRead = "True";
	myVars.races.race100.event2.eventType = "@event2EventType@";
	myVars.races.race100.event1.executed= false;// true to disable, false to enable
	myVars.races.race100.event2.executed= false;// true to disable, false to enable
</script>

