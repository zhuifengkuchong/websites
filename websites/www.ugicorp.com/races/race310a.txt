<script id = "race310a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race310={};
	myVars.races.race310.varName="Object[21100].State";
	myVars.races.race310.varType="@varType@";
	myVars.races.race310.repairType = "@RepairType";
	myVars.races.race310.event1={};
	myVars.races.race310.event2={};
	myVars.races.race310.event1.id = "_ctrl0_ctl24_RadMenu1_m1_m7_m0";
	myVars.races.race310.event1.type = "onfocus";
	myVars.races.race310.event1.loc = "_ctrl0_ctl24_RadMenu1_m1_m7_m0_LOC";
	myVars.races.race310.event1.isRead = "False";
	myVars.races.race310.event1.eventType = "@event1EventType@";
	myVars.races.race310.event2.id = "_ctrl0_ctl24_RadMenu1_m1_m7_m6";
	myVars.races.race310.event2.type = "onfocus";
	myVars.races.race310.event2.loc = "_ctrl0_ctl24_RadMenu1_m1_m7_m6_LOC";
	myVars.races.race310.event2.isRead = "True";
	myVars.races.race310.event2.eventType = "@event2EventType@";
	myVars.races.race310.event1.executed= false;// true to disable, false to enable
	myVars.races.race310.event2.executed= false;// true to disable, false to enable
</script>

