<script id = "race398a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race398={};
	myVars.races.race398.varName="Object[19607].Focused";
	myVars.races.race398.varType="@varType@";
	myVars.races.race398.repairType = "@RepairType";
	myVars.races.race398.event1={};
	myVars.races.race398.event2={};
	myVars.races.race398.event1.id = "_ctrl0_ctl24_RadMenu1_m0_m2";
	myVars.races.race398.event1.type = "onfocus";
	myVars.races.race398.event1.loc = "_ctrl0_ctl24_RadMenu1_m0_m2_LOC";
	myVars.races.race398.event1.isRead = "False";
	myVars.races.race398.event1.eventType = "@event1EventType@";
	myVars.races.race398.event2.id = "_ctrl0_ctl24_RadMenu1_m0_m2";
	myVars.races.race398.event2.type = "onblur";
	myVars.races.race398.event2.loc = "_ctrl0_ctl24_RadMenu1_m0_m2_LOC";
	myVars.races.race398.event2.isRead = "False";
	myVars.races.race398.event2.eventType = "@event2EventType@";
	myVars.races.race398.event1.executed= false;// true to disable, false to enable
	myVars.races.race398.event2.executed= false;// true to disable, false to enable
</script>

