<script id = "race10b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race10={};
	myVars.races.race10.varName="_ctrl0_ctl24_RadMenu1_m0_m3__onkeydown";
	myVars.races.race10.varType="@varType@";
	myVars.races.race10.repairType = "@RepairType";
	myVars.races.race10.event1={};
	myVars.races.race10.event2={};
	myVars.races.race10.event1.id = "_ctrl0_ctl24_RadMenu1_m0_m3";
	myVars.races.race10.event1.type = "onkeydown";
	myVars.races.race10.event1.loc = "_ctrl0_ctl24_RadMenu1_m0_m3_LOC";
	myVars.races.race10.event1.isRead = "True";
	myVars.races.race10.event1.eventType = "@event1EventType@";
	myVars.races.race10.event2.id = "_ctrl0_ctl24_RadMenu1_m0";
	myVars.races.race10.event2.type = "onfocus";
	myVars.races.race10.event2.loc = "_ctrl0_ctl24_RadMenu1_m0_LOC";
	myVars.races.race10.event2.isRead = "False";
	myVars.races.race10.event2.eventType = "@event2EventType@";
	myVars.races.race10.event1.executed= false;// true to disable, false to enable
	myVars.races.race10.event2.executed= false;// true to disable, false to enable
</script>

