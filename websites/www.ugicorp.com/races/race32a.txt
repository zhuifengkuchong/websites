<script id = "race32a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race32={};
	myVars.races.race32.varName="_ctrl0_ctl24_RadMenu1_m0_m1__onfocus";
	myVars.races.race32.varType="@varType@";
	myVars.races.race32.repairType = "@RepairType";
	myVars.races.race32.event1={};
	myVars.races.race32.event2={};
	myVars.races.race32.event1.id = "_ctrl0_ctl24_RadMenu1_m0";
	myVars.races.race32.event1.type = "onfocus";
	myVars.races.race32.event1.loc = "_ctrl0_ctl24_RadMenu1_m0_LOC";
	myVars.races.race32.event1.isRead = "False";
	myVars.races.race32.event1.eventType = "@event1EventType@";
	myVars.races.race32.event2.id = "_ctrl0_ctl24_RadMenu1_m0_m1";
	myVars.races.race32.event2.type = "onfocus";
	myVars.races.race32.event2.loc = "_ctrl0_ctl24_RadMenu1_m0_m1_LOC";
	myVars.races.race32.event2.isRead = "True";
	myVars.races.race32.event2.eventType = "@event2EventType@";
	myVars.races.race32.event1.executed= false;// true to disable, false to enable
	myVars.races.race32.event2.executed= false;// true to disable, false to enable
</script>

