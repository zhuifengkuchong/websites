<script id = "race202b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race202={};
	myVars.races.race202.varName="Object[19770].State";
	myVars.races.race202.varType="@varType@";
	myVars.races.race202.repairType = "@RepairType";
	myVars.races.race202.event1={};
	myVars.races.race202.event2={};
	myVars.races.race202.event1.id = "_ctrl0_ctl24_RadMenu1_m1_m1";
	myVars.races.race202.event1.type = "onfocus";
	myVars.races.race202.event1.loc = "_ctrl0_ctl24_RadMenu1_m1_m1_LOC";
	myVars.races.race202.event1.isRead = "True";
	myVars.races.race202.event1.eventType = "@event1EventType@";
	myVars.races.race202.event2.id = "_ctrl0_ctl24_RadMenu1_m1_m0";
	myVars.races.race202.event2.type = "onfocus";
	myVars.races.race202.event2.loc = "_ctrl0_ctl24_RadMenu1_m1_m0_LOC";
	myVars.races.race202.event2.isRead = "False";
	myVars.races.race202.event2.eventType = "@event2EventType@";
	myVars.races.race202.event1.executed= false;// true to disable, false to enable
	myVars.races.race202.event2.executed= false;// true to disable, false to enable
</script>

