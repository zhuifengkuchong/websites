<script id = "race157a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race157={};
	myVars.races.race157.varName="Lu_DOM__onblur";
	myVars.races.race157.varType="@varType@";
	myVars.races.race157.repairType = "@RepairType";
	myVars.races.race157.event1={};
	myVars.races.race157.event2={};
	myVars.races.race157.event1.id = "_ctrl0_ctl24_RadMenu1_m3_m1";
	myVars.races.race157.event1.type = "onfocus";
	myVars.races.race157.event1.loc = "_ctrl0_ctl24_RadMenu1_m3_m1_LOC";
	myVars.races.race157.event1.isRead = "False";
	myVars.races.race157.event1.eventType = "@event1EventType@";
	myVars.races.race157.event2.id = "_ctrl0_ctl24_RadMenu1_m1_m4_m1";
	myVars.races.race157.event2.type = "onblur";
	myVars.races.race157.event2.loc = "_ctrl0_ctl24_RadMenu1_m1_m4_m1_LOC";
	myVars.races.race157.event2.isRead = "True";
	myVars.races.race157.event2.eventType = "@event2EventType@";
	myVars.races.race157.event1.executed= false;// true to disable, false to enable
	myVars.races.race157.event2.executed= false;// true to disable, false to enable
</script>

