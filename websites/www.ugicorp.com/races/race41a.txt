<script id = "race41a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race41={};
	myVars.races.race41.varName="_ctrl0_ctl24_RadMenu1_m1_m3_m0__onkeydown";
	myVars.races.race41.varType="@varType@";
	myVars.races.race41.repairType = "@RepairType";
	myVars.races.race41.event1={};
	myVars.races.race41.event2={};
	myVars.races.race41.event1.id = "_ctrl0_ctl24_RadMenu1_m1_m3";
	myVars.races.race41.event1.type = "onfocus";
	myVars.races.race41.event1.loc = "_ctrl0_ctl24_RadMenu1_m1_m3_LOC";
	myVars.races.race41.event1.isRead = "False";
	myVars.races.race41.event1.eventType = "@event1EventType@";
	myVars.races.race41.event2.id = "_ctrl0_ctl24_RadMenu1_m1_m3_m0";
	myVars.races.race41.event2.type = "onkeydown";
	myVars.races.race41.event2.loc = "_ctrl0_ctl24_RadMenu1_m1_m3_m0_LOC";
	myVars.races.race41.event2.isRead = "True";
	myVars.races.race41.event2.eventType = "@event2EventType@";
	myVars.races.race41.event1.executed= false;// true to disable, false to enable
	myVars.races.race41.event2.executed= false;// true to disable, false to enable
</script>

