<script id = "race434b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race434={};
	myVars.races.race434.varName="Object[21100].Focused";
	myVars.races.race434.varType="@varType@";
	myVars.races.race434.repairType = "@RepairType";
	myVars.races.race434.event1={};
	myVars.races.race434.event2={};
	myVars.races.race434.event1.id = "_ctrl0_ctl24_RadMenu1_m1_m7";
	myVars.races.race434.event1.type = "onblur";
	myVars.races.race434.event1.loc = "_ctrl0_ctl24_RadMenu1_m1_m7_LOC";
	myVars.races.race434.event1.isRead = "False";
	myVars.races.race434.event1.eventType = "@event1EventType@";
	myVars.races.race434.event2.id = "_ctrl0_ctl24_RadMenu1_m1_m7_m0";
	myVars.races.race434.event2.type = "onfocus";
	myVars.races.race434.event2.loc = "_ctrl0_ctl24_RadMenu1_m1_m7_m0_LOC";
	myVars.races.race434.event2.isRead = "True";
	myVars.races.race434.event2.eventType = "@event2EventType@";
	myVars.races.race434.event1.executed= false;// true to disable, false to enable
	myVars.races.race434.event2.executed= false;// true to disable, false to enable
</script>

