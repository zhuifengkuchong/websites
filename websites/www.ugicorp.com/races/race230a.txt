<script id = "race230a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race230={};
	myVars.races.race230.varName="Object[21300]._domEventHandlingEnabled";
	myVars.races.race230.varType="@varType@";
	myVars.races.race230.repairType = "@RepairType";
	myVars.races.race230.event1={};
	myVars.races.race230.event2={};
	myVars.races.race230.event1.id = "Lu_Id_script_17";
	myVars.races.race230.event1.type = "Lu_Id_script_17__parsed";
	myVars.races.race230.event1.loc = "Lu_Id_script_17_LOC";
	myVars.races.race230.event1.isRead = "False";
	myVars.races.race230.event1.eventType = "@event1EventType@";
	myVars.races.race230.event2.id = "_ctrl0_ctl24_RadMenu1_m1_m7_m2";
	myVars.races.race230.event2.type = "onkeydown";
	myVars.races.race230.event2.loc = "_ctrl0_ctl24_RadMenu1_m1_m7_m2_LOC";
	myVars.races.race230.event2.isRead = "True";
	myVars.races.race230.event2.eventType = "@event2EventType@";
	myVars.races.race230.event1.executed= false;// true to disable, false to enable
	myVars.races.race230.event2.executed= false;// true to disable, false to enable
</script>

