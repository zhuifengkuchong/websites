<script id = "race3b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race3={};
	myVars.races.race3.varName="Search_input__onblur";
	myVars.races.race3.varType="@varType@";
	myVars.races.race3.repairType = "@RepairType";
	myVars.races.race3.event1={};
	myVars.races.race3.event2={};
	myVars.races.race3.event1.id = "Search_input";
	myVars.races.race3.event1.type = "onblur";
	myVars.races.race3.event1.loc = "Search_input_LOC";
	myVars.races.race3.event1.isRead = "True";
	myVars.races.race3.event1.eventType = "@event1EventType@";
	myVars.races.race3.event2.id = "Lu_Id_script_10";
	myVars.races.race3.event2.type = "Lu_Id_script_10__parsed";
	myVars.races.race3.event2.loc = "Lu_Id_script_10_LOC";
	myVars.races.race3.event2.isRead = "False";
	myVars.races.race3.event2.eventType = "@event2EventType@";
	myVars.races.race3.event1.executed= false;// true to disable, false to enable
	myVars.races.race3.event2.executed= false;// true to disable, false to enable
</script>

