// WebTrends SmartSource Data Collector Tag v10.4.1
// Copyright (c) 2014 Webtrends Inc.  All rights reserved.
// Tag Builder Version: 4.1.3.2
// Created: 2014.03.27
window.webtrendsAsyncInit=function(){
	var formscriptSrc = g_HttpRelativeWebRoot + "groups/webasset/documents/webasset/webtrends_forms.js";
    var dcs=new Webtrends.dcs().init({
        dcsid:"dcsoii17z6bv0hkz9fs1yfz5l_9g8j",
        domain:"http://www.fisglobal.com/ucmprdpub/groups/webasset/documents/webasset/statse.webtrendslive.com",
        timezone:-6,
		adimpressions:true,  
        adsparam: "http://www.fisglobal.com/ucmprdpub/groups/webasset/documents/webasset/WT.ac",
        i18n:true,
        offsite:true,
		javascript: true, 
        download:true,
        downloadtypes:"xls,doc,pdf,txt,csv,zip,docx,xlsx,rar,gzip",
        onsitedoms:"http://www.fisglobal.com/ucmprdpub/groups/webasset/documents/webasset/fisglobal.com",
        fpcdom:".fisglobal.com",
        plugins:{
            formTrack:{src:formscriptSrc},
            hm:{src:"../../../../../../s.webtrends.com/js/webtrends.hm.js"/*tpa=http://s.webtrends.com/js/webtrends.hm.js*/}
        }
        }).track();
};
(function(){
	var scriptSrc = g_HttpRelativeWebRoot + "groups/webasset/documents/webasset/webtrends_min.js";
    var s=document.createElement("script"); s.async=true; s.src=scriptSrc;    
    var s2=document.getElementsByTagName("script")[0]; s2.parentNode.insertBefore(s,s2);
}());
