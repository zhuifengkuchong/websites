<script id = "race13a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race13={};
	myVars.races.race13.varName="Tree[0x7fd552d21458]:nav4";
	myVars.races.race13.varType="@varType@";
	myVars.races.race13.repairType = "@RepairType";
	myVars.races.race13.event1={};
	myVars.races.race13.event2={};
	myVars.races.race13.event1.id = "nav4";
	myVars.races.race13.event1.type = "nav4__parsed";
	myVars.races.race13.event1.loc = "nav4_LOC";
	myVars.races.race13.event1.isRead = "False";
	myVars.races.race13.event1.eventType = "@event1EventType@";
	myVars.races.race13.event2.id = "Lu_Id_li_22";
	myVars.races.race13.event2.type = "onmouseover";
	myVars.races.race13.event2.loc = "Lu_Id_li_22_LOC";
	myVars.races.race13.event2.isRead = "True";
	myVars.races.race13.event2.eventType = "@event2EventType@";
	myVars.races.race13.event1.executed= false;// true to disable, false to enable
	myVars.races.race13.event2.executed= false;// true to disable, false to enable
</script>

