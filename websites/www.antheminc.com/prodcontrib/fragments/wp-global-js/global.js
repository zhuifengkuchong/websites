// Modified 11/21/2014 3:50 pm

// Author Soh Tanaka http://www.sohtanaka.com/web-design/automatic-image-slider-w-css-jquery/
//Show the paging and activate its first link
$(document).ready(function() {
    $(".paging").show();
    $(".paging a:first").addClass("active");

    //Get size of the image, how many images there are, then determin the size of the image reel.
    var imageWidth = $(".window").width();
    var imageSum = $(".image_reel div").size();
    var imageReelWidth = imageWidth * imageSum;

    //Adjust the image reel to its new size
    $(".image_reel").css({ 'width': imageReelWidth });

    //Paging  and Slider Function
    rotate = function() {
    	try{
        var triggerID = $active.attr("rel") - 1; //Get number of times to slide

        var image_reelPosition = triggerID * imageWidth; //Determines the distance the image reel needs to slide

        $(".paging a").removeClass('active'); //Remove all active class
        $active.addClass('active'); //Add active class (the $active is declared in the rotateSwitch function)
        //Slider Animation
        $(".image_reel").animate({
            left: -image_reelPosition
        }, 500);
      }catch(err){ 
      	//ignore error
      }
    };

    //Rotation  and Timing Event
    rotateSwitch = function() {
        play = setInterval(function() { //Set timer - this will repeat itself every 7 seconds
            $active = $('.paging a.active').next(); //Move to the next paging
            if ($active.length === 0) { //If paging reaches the end...
                $active = $('.paging a:first'); //go back to first
            }
            rotate(); //Trigger the paging and slider function
        }, 10000); //Timer speed in milliseconds (10 seconds)
    };

    rotateSwitch(); //Run function on launch

    //On clicking pause button
    $(".pause_marquee").click(function() {
        $(".pause_marquee").hide();
        $(".play_marquee").show();
        clearInterval(play); //Stop the rotation
    }, function() {
        rotateSwitch(); //Resume rotation timer
    });

    //On clicking play button
    $(".play_marquee").click(function() {
        $(".play_marquee").hide();
        $(".pause_marquee").show();
        rotate(); //Trigger rotation immediately
        rotateSwitch(); //Resume rotation timer
    });

    //Go to Previous marquee
    $(".prev_marquee").click(function() {

        $active = $('.paging a.active').prev(); //Move to the previous paging
        if ($active.length > 0) {
            clearInterval(play); //Stop the rotation
            rotate(); //Trigger rotation immediately
            rotateSwitch(); // Resume rotation timer
        }
        else {
            $active = $('.paging a:last');
            clearInterval(play); //Stop the rotation
            rotate(); //Trigger rotation immediately
            rotateSwitch(); // Resume rotation timer
        }
    });

    //Go to next marquee
    $(".next_marquee").click(function() {
        $active = $('.paging a.active').next(); //Move to the next paging
        if ($active.length > 0) {
            clearInterval(play); //Stop the rotation
            rotate(); //Trigger rotation immediately
            rotateSwitch(); // Resume rotation timer
        }
        else {
            $active = $('.paging a:first');
            clearInterval(play); //Stop the rotation
            rotate(); //Trigger rotation immediately
            rotateSwitch(); // Resume rotation timer
        }
    });

    //On Click
    $(".paging a").click(function() {
        $(".play_marquee").hide();
        $(".pause_marquee").show();
        $active = $(this); //Activate the clicked paging
        //Reset Timer
        clearInterval(play); //Stop the rotation
        rotate(); //Trigger rotation immediately
        rotateSwitch(); // Resume rotation timer
        return false; //Prevent browser jump to link anchor
    });
});
//Global js changes
$(function() {
    //fix png issue for IE6
    $(".logo,.marqueeFooter").pngFix();
    // Remove border from first list items
    $("#footer li:first").css({ "border-left": "none", "padding-left": "0" });
    $(".listBody_Basic").find("ul li:odd").css('background-color', '#ffffcc');
    $("table.altRow").find("tr:odd").css('background-color', '#F3F3F3');
    $(".itemsLeft ul li:last").css({ "border-bottom": "none" });
    $(".navi a:last").css({ "border-right": "none" });
    $(".content_Main p:first").css({ "margin-top": "0", "padding-top": "0" });
    $(".content_Main p:last, .content_Main ul:last").css({ "margin-bottom": "0", "padding-bottom": "0" });
    $(".marginboth .toutText p:first").css({ "margin-top": "0", "padding-top": "0" });

});
//add hover effects on buttons - use input type image for all buttons and add class=button
$(function() {
    if ($.browser.msie && $.browser.version == "6.0") {
        $("img[src$=_off.png],input[src$=_off.png] ").each(function(i, img) {
            // Replace with GIF versions
            img.src = img.src.replace(/\_off.png$/, 'Unknown_83_filename'/*tpa=http://www.antheminc.com/prodcontrib/fragments/wp-global-js/_off.gif*/)
        });

        //$('.button').attr("src").replace("Unknown_83_filename"/*tpa=http://www.antheminc.com/prodcontrib/fragments/wp-global-js/_off.png*/, "Unknown_83_filename"/*tpa=http://www.antheminc.com/prodcontrib/fragments/wp-global-js/_off.gif*/);
        $(".button").hover(function() {
            $(this).attr("src", $(this).attr("src").replace("_off", "_on"));
        }, function() {
            $(this).attr("src", $(this).attr("src").replace("_on", "_off"));
        });
    }
    else {
        $(".button").hover(function() {
            $(this).attr("src", $(this).attr("src").replace("_off", "_on"));
        }, function() {
            $(this).attr("src", $(this).attr("src").replace("_on", "_off"));
        });
    }
});
//Open Videos in various window sizes and video embed code
$(function() {
    $('a.modalVideo').click(function(e) {
        e.preventDefault();
        $(".pause_marquee").hide();
        $(".play_marquee").show();
        clearInterval(play); //Stop the rotation
        //        buttonUrl = $(this).attr('href');
        //        window.open(buttonUrl, 'Video', 'width=512,height=288,status,location=no,scrollbars=no,resizable=no,screenX=20,screenY=40,left=20,top=300');
        //        return false;

        var url = this.href;
        $("#loadDialog").html('<iframe id="modalIframeId" width="100%" height="100%" marginWidth="0" marginHeight="0" frameBorder="0" scrolling="auto" />').dialog("open");
        $("#modalIframeId").attr("src", url);
        return false;

    });
    $(function() {
        $("#loadDialog").dialog({
            modal: true,
            autoOpen: false,
            height: 275,
            title: 'Video',
            width: 385,
            position: [690, 160],
            buttons: [
                {
                    text: "CLOSE VIDEO",
                    click: function() {
                        $(this).dialog('close').empty();
                        $(".play_marquee").hide();
                        $(".pause_marquee").show();
                        rotate(); //Trigger rotation immediately
                        rotateSwitch(); //Resume rotation timer   
                    }
                }
            ],
            close: function(event, ui) {
                $(this).empty();
                $(".play_marquee").hide();
                $(".pause_marquee").show();
                rotate(); //Trigger rotation immediately
                rotateSwitch(); //Resume rotation timer
            }

        });
        //close dialog via clicking of overlay
        $('.ui-widget-overlay').live('click', function() {
            $("#loadDialog").dialog('close').empty();
        });
    });
});
//Historical Timeline Slider
// What is $(document).ready ? See: http://flowplayer.org/tools/documentation/basics.html#document_ready
$(document).ready(function() {

    // initialize scrollable together with the navigator plugin
    $("#browsable").scrollable().navigator();
    $(".items a").click(function(e) {
        e.preventDefault();
        //Get size of the image, how many images there are, then determin the size of the image reel.
        var historyWidth = $("#image_wrap").width();
        var historySum = $(".history_reel div").size();
        var historyReelWidth = historyWidth * historySum;

        //Adjust the image reel to its new size
        $(".history_reel").css({ 'width': historyReelWidth });

        var historytriggerID = $(this).attr("rel") - 1; //Get number of times to slide
        var history_reelPosition = historytriggerID * historyWidth; //Determines the distance the image reel needs to slide

        // see if same thumb is being clicked
        if ($(this).hasClass("active")) { return; }

        $(".history_reel").animate({
            left: -history_reelPosition
        }, 500);

        // the large image from www.flickr.com
        //var img = new Image();
        //var historyText = ('div.historyDetails');

        //historyText = history;

        // activate item
        $(".items a").removeClass("active");
        $(this).addClass("active");

        // when page loads simulate a "click" on the first image
    }).filter(":first").click();
    return false;
});

//*** Start News and Media images Slider. This uses the cycle plugin in jquery.dropdownPlain.js***//
$(document).ready(function() {
    $('.newsimages').cycle({
        fx: 'scrollLeft',
        timeout: 10000,  // milliseconds between slide transitions (0 to disable auto advance)
        speed: 1000
    });
});

function setExternalURLs() {
/*
    var a, b = "/";

    a = document.getElementById("delicious"); a.href = "http://delicious.com/save?v=5&noui&jump=close&url=" + encodeURIComponent(location.href) + "&title=" + encodeURIComponent(document.title);
    a = document.getElementById("facebook"); a.href = "http://www.facebook.com/sharer.php?u=" + encodeURIComponent(location.href) + "&t=" + encodeURIComponent(document.title);
    a = document.getElementById("reddit"); a.href = "http://www.reddit.com/submit?url=" + encodeURIComponent(window.location);
    a = document.getElementById("stumbleupon"); a.href = "http://www.stumbleupon.com/submit?url=" + encodeURIComponent(location.href) + "&title=" + encodeURIComponent(document.title);
    a = document.getElementById("newsvine"); a.href = "http://www.newsvine.com/_wine/save?popoff=1&u=" + encodeURIComponent(location.href)
    */
};

var links =
{
    //List domains to be excluded from opening in new window:
    excludedomains: ["http://www.antheminc.com/prodcontrib/fragments/wp-global-js/www.wellpointdiversity.com", "http://www.antheminc.com/prodcontrib/fragments/wp-global-js/www.careersatwellpoint.com", "http://www.antheminc.com/prodcontrib/fragments/wp-global-js/wellpointcareers.hodes.com",
                     "http://www.antheminc.com/prodcontrib/fragments/wp-global-js/phx.corporate-ir.net", "http://www.antheminc.com/prodcontrib/fragments/wp-global-js/ir.wellpoint.com", "http://www.antheminc.com/prodcontrib/fragments/wp-global-js/media.corporate-ir.net", "http://www.antheminc.com/prodcontrib/fragments/wp-global-js/wellpoint.aecglobal.com", "http://www.antheminc.com/prodcontrib/fragments/wp-global-js/www.wellpoint.com",
                     "http://www.antheminc.com/prodcontrib/fragments/wp-global-js/30.135.22.116", "http://www.antheminc.com/prodcontrib/fragments/wp-global-js/vadwvic001.corp.tghnet.com", "http://www.antheminc.com/prodcontrib/fragments/wp-global-js/30.135.16.85", "http://www.antheminc.com/prodcontrib/fragments/wp-global-js/worknet-dev02.auth.wellpoint.com", "http://www.antheminc.com/prodcontrib/fragments/wp-global-js/iirc.corporate-ir.net",
                     "http://www.antheminc.com/prodcontrib/fragments/wp-global-js/va10dwvstl001.us.ad.wellpoint.com", "http://www.antheminc.com/prodcontrib/fragments/wp-global-js/preprodcontrib-inter.wellpoint.com", "http://www.antheminc.com/prodcontrib/fragments/wp-global-js/30.130.51.212", "http://www.antheminc.com/prodcontrib/fragments/wp-global-js/30.130.51.213", "http://www.antheminc.com/prodcontrib/fragments/wp-global-js/30.130.51.23",
                     "va10pwveaa025", "http://www.antheminc.com/prodcontrib/fragments/wp-global-js/2.136.12.40", "va10pwveaa026", "http://www.antheminc.com/prodcontrib/fragments/wp-global-js/2.136.12.41",
                     "http://www.antheminc.com/prodcontrib/fragments/wp-global-js/search.wellpoint.com", "http://www.antheminc.com/prodcontrib/fragments/wp-global-js/testsearch.wellpoint.com", "http://www.antheminc.com/prodcontrib/fragments/wp-global-js/hodesinteractive.com", "http://www.antheminc.com/prodcontrib/fragments/wp-global-js/lexington.hodes.com", "http://www.antheminc.com/prodcontrib/fragments/wp-global-js/wellpointtest.aecglobal.com",
                     "http://www.antheminc.com/prodcontrib/fragments/wp-global-js/www.antheminc.com", "http://www.antheminc.com/prodcontrib/fragments/wp-global-js/ir.antheminc.com", "http://www.antheminc.com/prodcontrib/fragments/wp-global-js/www.uat1.va.antheminc.com", "http://www.antheminc.com/prodcontrib/fragments/wp-global-js/www.careers.antheminc.com", "http://www.antheminc.com/prodcontrib/fragments/wp-global-js/diversity.antheminc.com", "http://www.antheminc.com/prodcontrib/fragments/wp-global-js/antheminc.com"],

    //Target for links that should open in a new window:
    linktarget: "_blank",

    assigntarget: function() {
        var rexcludedomains = new RegExp(this.excludedomains.join("|"), "i")
        var all_links = document.getElementsByTagName("a")

        for (var i = 0; i <= (all_links.length - 1); i++) {
            if ((all_links[i].hostname.search(rexcludedomains) == -1 && all_links[i].href.indexOf("http") != -1)
              || all_links[i].href.toLowerCase().indexOf(".pdf") != -1
              || all_links[i].href.toLowerCase().indexOf(".jpg") != -1
              || all_links[i].href.toLowerCase().indexOf(".bmp") != -1
              || all_links[i].href.toLowerCase().indexOf(".ps") != -1
              || all_links[i].href.toLowerCase().indexOf(".gif") != -1
              || all_links[i].href.toLowerCase().indexOf(".jpeg") != -1) {
                all_links[i].target = links.linktarget;
            }


        }
    },

    init: function() {
        if (window.addEventListener)
            window.addEventListener("load", function() { links.assigntarget() }, false)
        else if (window.attachEvent)
            window.attachEvent("onload", function() { links.assigntarget() })
    }

}

links.init()

function loadVideo(divId, videoUrl) {
    $(divId).load(videoUrl);
}
