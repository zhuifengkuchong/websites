// Modified 04/28/2011 11:00 Am
// Modified 10/10/2012 8:00 am

//*** ALL CLIENTS OF THIS CODE MUST POINT TO THIER OWN PATHS FOR THE IMAGES AND STOCK FILE ***

var StockUpImageSrc = "";
var StockDownImageSrc = "";
var xmlDataFile = "";

var currentPage = window.location.href;
if ( currentPage.indexOf("devcontribution") > 1 ||
     currentPage.indexOf("http://www.antheminc.com/prodcontrib/fragments/wp-global-js/vadwvic001.corp.tghnet.com") > 1 ||
     currentPage.indexOf("http://www.antheminc.com/prodcontrib/fragments/wp-global-js/30.135.16.85") > 1 ||
     currentPage.indexOf("http://www.antheminc.com/prodcontrib/fragments/wp-global-js/30.135.22.116") > 1 ||
     currentPage.indexOf("http://www.antheminc.com/prodcontrib/fragments/wp-global-js/va10dwvstl001.us.ad.wellpoint.com") > 1
   )
{
    xmlDataFile = "../../../devcontribution/groups/public/@wp_corp_main/documents/wlp_assets/stockdata.xml";
    StockUpImageSrc = "Unknown_83_filename"/*tpa=http://www.antheminc.com/devcontribution/fragments/wp-global-css/images/icon_stock_up.gif*/;
    StockDownImageSrc = "Unknown_83_filename"/*tpa=http://www.antheminc.com/devcontribution/fragments/wp-global-css/images/icon_stocks_down.gif*/;
}
else if( currentPage.indexOf("uat1-") > 1 ||
         currentPage.indexOf("http://www.antheminc.com/prodcontrib/fragments/wp-global-js/preprodcontrib-inter.wellpoint.com") > 1 ||
         currentPage.indexOf("http://www.antheminc.com/prodcontrib/fragments/wp-global-js/30.130.49.12") > 1
       )
{
    xmlDataFile = "../../../wellpoint/groups/public/@wp_corp_main/documents/wlp_assets/stockdata.xml";
    StockUpImageSrc = "Unknown_83_filename"/*tpa=http://www.antheminc.com/wellpoint/fragments/wp-global-css/images/icon_stock_up.gif*/;
    StockDownImageSrc = "Unknown_83_filename"/*tpa=http://www.antheminc.com/wellpoint/fragments/wp-global-css/images/icon_stocks_down.gif*/;
}
else if( currentPage.indexOf("http://www.antheminc.com/prodcontrib/fragments/wp-global-js/30.130.51.23") > 1 ||
         currentPage.indexOf("http://www.antheminc.com/prodcontrib/fragments/wp-global-js/30.130.51.212") > 1 ||
         currentPage.indexOf("http://www.antheminc.com/prodcontrib/fragments/wp-global-js/30.130.51.213") > 1 ||
         currentPage.indexOf("http://www.antheminc.com/prodcontrib/fragments/wp-global-js/temp-www.wellpoint.com") > 1
       )
{
    xmlDataFile = "../../../prodcontrib/groups/public/@wp_corp_main/documents/wlp_assets/stockdata.xml";
    StockUpImageSrc = "../wp-global-css/images/icon_stock_up.gif"/*tpa=http://www.antheminc.com/prodcontrib/fragments/wp-global-css/images/icon_stock_up.gif*/;
    StockDownImageSrc = "../wp-global-css/images/icon_stocks_down.gif"/*tpa=http://www.antheminc.com/prodcontrib/fragments/wp-global-css/images/icon_stocks_down.gif*/;
}
else if( currentPage.indexOf("http://www.antheminc.com/prodcontrib/fragments/wp-global-js/www.uat1.va.antheminc.com") > 1 ||
         currentPage.indexOf("http://www.antheminc.com/prodcontrib/fragments/wp-global-js/INSERT_IP_OF_SERVER1_FOR_WWW.UAT1.VA.ANTHEMINC.COM") > 1 ||
         currentPage.indexOf("http://www.antheminc.com/prodcontrib/fragments/wp-global-js/INSERT_IP_OF_SERVER2_FOR_WWW.UAT1.VA.ANTHEMINC.COM") > 1 
       )
{
    xmlDataFile = "http://www.antheminc.com/wellpoint/groups/public/@wp_corp_main/documents/wlp_assets/stockdata.xml";
    StockUpImageSrc = "Unknown_83_filename"/*tpa=http://www.antheminc.com/wellpoint/fragments/wp-global-css/images/icon_stock_up.gif*/;
    StockDownImageSrc = "Unknown_83_filename"/*tpa=http://www.antheminc.com/wellpoint/fragments/wp-global-css/images/icon_stocks_down.gif*/;
}
else
{
    xmlDataFile = "../../../../prodcontrib/groups/public/@wp_corp_main/documents/wlp_assets/stockdata.xml";
    StockUpImageSrc = "../wp-global-css/images/icon_stock_up.gif"/*tpa=http://www.antheminc.com/prodcontrib/fragments/wp-global-css/images/icon_stock_up.gif*/;
    StockDownImageSrc = "../wp-global-css/images/icon_stocks_down.gif"/*tpa=http://www.antheminc.com/prodcontrib/fragments/wp-global-css/images/icon_stocks_down.gif*/;
}

$(document).ready(function stockFunction() {
    $.ajax({
        type: "GET",
        url: xmlDataFile,
        dataType: "xml",
        success: function(xml) {
            $(xml).find('Stock_Quote').each(function() {
                var PrimaryTicker = $(this).attr('PrimaryTicker');

                if (PrimaryTicker == "Yes") {
                    var Ticker = $(this).attr('Ticker');
                    var Trade = $(this).find('Trade').text();
                    var Change = $(this).find('Change').text();
                    var Date = $(this).find('Date').text();
                    var ImageSrc = "";
                    var ChangeUpDown = Change.substring(0, 1);
                    var ChangeDislay = Change.substring(1, $(this).find('Change').text().length);
                    var DateLength = Date.length;
                    var ModifiedDate = Date.substring(0, DateLength - 2);
                    var AMPM = Date.substring(DateLength - 2);
                    var FormattedDate = ModifiedDate + ' ' + AMPM;

                    if (ChangeUpDown.indexOf('-') == -1){ ChangeDislay = Change; }

                    if (ChangeUpDown.indexOf('-') > -1) { ImageSrc = "<img src='" + StockDownImageSrc + "' />"; }
                    else if(Change == 'http://www.antheminc.com/prodcontrib/fragments/wp-global-js/0.00') { ImageSrc = ""; }
                    else { ImageSrc = "<img src='" + StockUpImageSrc + "' />"; }

                    $('#stockInfoDiv').html('<p>NYSE - ' + Ticker + '&nbsp;&nbsp;' + '<b>' + Trade
    				+ '</b>&nbsp;&nbsp;&nbsp;' + ChangeDislay + ImageSrc
    				+ '<br / >' + 'as of ' + FormattedDate + ' ET</p>');
                }
            });
        }
    });
});
