/////////////////////////////////////////////////////////////////////////////
// Function : NavNode (constructor)
// Comments :
/////////////////////////////////////////////////////////////////////////////
function NavNode(id, label, href, parent)
{
	this.m_parent = null;
	this.m_level = 0;

	if (parent)
	{
		this.m_parent = parent;
		this.m_level = parent.m_level+1;
	}

	this.m_id = id;

	// assume that m_label will most often be used directly as HTML
	this.m_rawlabel = label;

	label = label.replace(/&/g, '&amp;');
	label = label.replace(/</g, '&lt;');
	label = label.replace(/>/g, '&gt;');
	label = label.replace(/"/g, '&quot;');

	this.m_label = label;

	this.m_href = href;
	this.m_subNodes = new Array();

	var argValues = NavNode.arguments;
	var argCount = NavNode.arguments.length;

	for (i = 4 ; i < argCount ; i++)
	{
		var eqPos = argValues[i].indexOf("==");
		var attrName = argValues[i].substring(0,eqPos);
		var attrValue = argValues[i].substring(eqPos+2);

		eval("this.cp_" + attrName + " = '" + attrValue + "';");
	}

	NavNode.prototype.addNode = addNode;
	NavNode.prototype.isSelected = isSelected;
}

/////////////////////////////////////////////////////////////////////////////
// Function : addNode
// Comments :
/////////////////////////////////////////////////////////////////////////////
function addNode(id, label, href)
{
	var newIndex = this.m_subNodes.length;
	var newNode = new NavNode(id, label, href, this);

	var argValues = addNode.arguments;
	var argCount = addNode.arguments.length;

	for (i = 3 ; i < argCount ; i++)
	{
		var eqPos = argValues[i].indexOf("==");
		var attrName = argValues[i].substring(0,eqPos);
		var attrValue = argValues[i].substring(eqPos+2);

		eval("newNode.cp_" + attrName + " = '" + attrValue + "';");
	}

	this.m_subNodes[newIndex] = newNode;
	return newNode;
}

/////////////////////////////////////////////////////////////////////////////
// Function : isSelected
// Comments :
/////////////////////////////////////////////////////////////////////////////
function isSelected()
{
    var pos = window.location.href.lastIndexOf("/");
    var docname = window.location.href.substring(pos+1, window.location.href.length);

    pos = this.m_href.lastIndexOf("/");
    var myname = this.m_href.substring(pos+1, this.m_href.length);

    if (docname == myname)
		return true;
	else
		return false;
}

/////////////////////////////////////////////////////////////////////////////
// Function : customSectionPropertyExists
// Comments :
/////////////////////////////////////////////////////////////////////////////
function customSectionPropertyExists(csp)
{
	return (typeof csp != _U && csp != null);
}

/////////////////////////////////////////////////////////////////////////////
// Function : getCustomSectionProperty
// Comments :
/////////////////////////////////////////////////////////////////////////////
function getCustomSectionProperty(csp)
{
	if (customSectionPropertyExists(csp))
	{
		return csp;
	}
	else
	{
		return "";
	}
}

/////////////////////////////////////////////////////////////////////////////

var g_navNode_Root = new NavNode('39','Home',ssUrlPrefix + 'index.htm',null,'NavigationalBar==header','secondaryUrlVariableField==WPMainBody');
g_navNode_0=g_navNode_Root.addNode('42','About Anthem, Inc.',ssUrlPrefix + 'AboutAnthemInc/index.htm','NavigationalBar==header','secondaryUrlVariableField==WPMainBody');
g_navNode_0_0=g_navNode_0.addNode('65','Purpose, Vision and Values',ssUrlPrefix + 'AboutAnthemInc/MissionValues/index.htm','secondaryUrlVariableField==WPMainBody');
g_navNode_0_1=g_navNode_0.addNode('66','Company History',ssUrlPrefix + 'AboutAnthemInc/CompanyHistory/index.htm','secondaryUrlVariableField==WPMainBody');
g_navNode_0_2=g_navNode_0.addNode('67','Business Strategy',ssUrlPrefix + 'AboutAnthemInc/BusinessStrategy/index.htm','secondaryUrlVariableField==WPMainBody');
g_navNode_0_3=g_navNode_0.addNode('75','Customer Segments',ssUrlPrefix + 'AboutAnthemInc/CustomerSegments/index.htm','secondaryUrlVariableField==WPMainBody');
g_navNode_0_4=g_navNode_0.addNode('74','Products \x26 Services',ssUrlPrefix + 'AboutAnthemInc/ProductsServices/index.htm','secondaryUrlVariableField==WPMainBody');
g_navNode_0_5=g_navNode_0.addNode('76','Leadership',ssUrlPrefix + 'AboutAnthemInc/Leadership/index.htm','secondaryUrlVariableField==WPMainBody');
g_navNode_0_5_0=g_navNode_0_5.addNode('77','Executive Leadership',ssUrlPrefix + 'AboutAnthemInc/Leadership/ExecutiveLeadership/index.htm','secondaryUrlVariableField==WPMainBody');
g_navNode_0_5_1=g_navNode_0_5.addNode('78','Board of Directors',ssUrlPrefix + 'AboutAnthemInc/Leadership/BoardofDirectors/index.htm','RedirectURL==http\x3a//ir.antheminc.com/phoenix.zhtml?c\x3d130104\x26p\x3dirol-govboardaw','secondaryUrlVariableField==WPMainBody');
g_navNode_0_5_2=g_navNode_0_5.addNode('217','Board Committees',ssUrlPrefix + 'AboutAnthemInc/Leadership/BoardCommittees/index.htm','RedirectURL==http\x3a//ir.antheminc.com/phoenix.zhtml?c\x3d130104\x26p\x3dirol-govcommcompaw','secondaryUrlVariableField==WPMainBody');
g_navNode_0_6=g_navNode_0.addNode('68','Government Relations',ssUrlPrefix + 'AboutAnthemInc/GovernmentRelations/index.htm','secondaryUrlVariableField==WPMainBody');
g_navNode_0_6_0=g_navNode_0_6.addNode('69','Political Contributions',ssUrlPrefix + 'AboutAnthemInc/GovernmentRelations/PoliticalContributions/index.htm','secondaryUrlVariableField==WPMainBody');
g_navNode_1=g_navNode_Root.addNode('43','Companies',ssUrlPrefix + 'Companies/index.htm','NavigationalBar==header','secondaryUrlVariableField==WPMainBody');
g_navNode_1_0=g_navNode_1.addNode('48','Anthem Blue Cross Blue Shield',ssUrlPrefix + 'Companies/AnthemBlueCrossBlueShield/index.htm','secondaryUrlVariableField==WPMainBody');
g_navNode_1_1=g_navNode_1.addNode('49','Anthem Blue Cross',ssUrlPrefix + 'Companies/AnthemBlueCross/index.htm','secondaryUrlVariableField==WPMainBody');
g_navNode_1_2=g_navNode_1.addNode('50','Empire BlueCross BlueShield',ssUrlPrefix + 'Companies/EmpireBlueCrossBlueShield/index.htm','secondaryUrlVariableField==WPMainBody');
g_navNode_1_3=g_navNode_1.addNode('51','Blue Cross and Blue Shield of Georgia',ssUrlPrefix + 'Companies/BlueCrossandBlueShieldofGeorgia/index.htm','secondaryUrlVariableField==WPMainBody');
g_navNode_1_4=g_navNode_1.addNode('52','Anthem Life Insurance',ssUrlPrefix + 'Companies/AnthemLifeInsurance/index.htm','secondaryUrlVariableField==WPMainBody');
g_navNode_1_5=g_navNode_1.addNode('53','Affiliated / Specialty Companies',ssUrlPrefix + 'Companies/AffiliatedSpecialtyCompanies/index.htm','secondaryUrlVariableField==WPMainBody');
g_navNode_2=g_navNode_Root.addNode('44','News \x26 Media',ssUrlPrefix + 'NewsMedia/index.htm','NavigationalBar==header','secondaryUrlVariableField==WPMainBody');
g_navNode_2_0=g_navNode_2.addNode('87','Press Releases',ssUrlPrefix + 'NewsMedia/PressReleases/index.htm','RedirectURL==http\x3a//ir.antheminc.com/phoenix.zhtml?c\x3d130104\x26p\x3dirol-news\x26nyo\x3d0','secondaryUrlVariableField==WPMainBody');
g_navNode_2_1=g_navNode_2.addNode('88','Anthem, Inc. in the News',ssUrlPrefix + 'NewsMedia/AnthemintheNews/index.htm','secondaryUrlVariableField==WPMainBody');
g_navNode_2_2=g_navNode_2.addNode('220','Multimedia Materials',ssUrlPrefix + 'NewsMedia/MultimediaMaterials/index.htm','secondaryUrlVariableField==WPMainBody');
g_navNode_2_3=g_navNode_2.addNode('90','Frequently Requested Materials',ssUrlPrefix + 'NewsMedia/FrequentlyRequestedMaterials/index.htm','secondaryUrlVariableField==WPMainBody');
g_navNode_2_3_0=g_navNode_2_3.addNode('94','Stats \x26 Facts',ssUrlPrefix + 'NewsMedia/FrequentlyRequestedMaterials/StatsFacts/index.htm','secondaryUrlVariableField==WPMainBody');
g_navNode_2_3_1=g_navNode_2_3.addNode('221','Photos',ssUrlPrefix + 'NewsMedia/FrequentlyRequestedMaterials/Photos/index.htm','secondaryUrlVariableField==WPMainBody');
g_navNode_2_3_2=g_navNode_2_3.addNode('96','Logos',ssUrlPrefix + 'NewsMedia/FrequentlyRequestedMaterials/Logos/index.htm','secondaryUrlVariableField==WPMainBody');
g_navNode_2_3_3=g_navNode_2_3.addNode('222','Executive Leadership Bios',ssUrlPrefix + 'NewsMedia/FrequentlyRequestedMaterials/ExecutiveLeadershipBios/index.htm','secondaryUrlVariableField==WPMainBody');
g_navNode_2_4=g_navNode_2.addNode('91','Research \x26 Position Papers',ssUrlPrefix + 'NewsMedia/ResearchPositionPapers/index.htm','secondaryUrlVariableField==WPMainBody');
g_navNode_2_4_0=g_navNode_2_4.addNode('225','View All Research',ssUrlPrefix + 'NewsMedia/ResearchPositionPapers/ViewAllResearch/index.htm','secondaryUrlVariableField==WPMainBody');
g_navNode_2_4_0_0=g_navNode_2_4_0.addNode('98','Market Research',ssUrlPrefix + 'NewsMedia/ResearchPositionPapers/ViewAllResearch/MarketResearch/index.htm','secondaryUrlVariableField==WPMainBody');
g_navNode_2_4_0_1=g_navNode_2_4_0.addNode('99','Health Services Research',ssUrlPrefix + 'NewsMedia/ResearchPositionPapers/ViewAllResearch/HealthServicesResearch/index.htm','secondaryUrlVariableField==WPMainBody');
g_navNode_2_4_0_2=g_navNode_2_4_0.addNode('100','Public Policy',ssUrlPrefix + 'NewsMedia/ResearchPositionPapers/ViewAllResearch/PublicPolicy/index.htm','secondaryUrlVariableField==WPMainBody');
g_navNode_2_4_1=g_navNode_2_4.addNode('224','Position Papers',ssUrlPrefix + 'NewsMedia/ResearchPositionPapers/PositionPapers/index.htm','secondaryUrlVariableField==WPMainBody');
g_navNode_2_5=g_navNode_2.addNode('92','Media Contact',ssUrlPrefix + 'NewsMedia/MediaContact/index.htm','secondaryUrlVariableField==WPMainBody');
g_navNode_3=g_navNode_Root.addNode('45','Investors',ssUrlPrefix + 'Investors/index.htm','NavigationalBar==header','RedirectURL==http\x3a//ir.antheminc.com/phoenix.zhtml?c\x3d130104\x26p\x3dirol-irHome','secondaryUrlVariableField==WPMainBody');
g_navNode_3_0=g_navNode_3.addNode('57','Financial Information',ssUrlPrefix + 'Investors/FinancialInformation/index.htm','RedirectURL==http\x3a//ir.antheminc.com/phoenix.zhtml?c\x3d130104\x26p\x3dirol-financial_information','secondaryUrlVariableField==WPMainBody');
g_navNode_3_0_0=g_navNode_3_0.addNode('115','Press Releases',ssUrlPrefix + 'Investors/FinancialInformation/PressReleases/index.htm','RedirectURL==http\x3a//ir.wellpoint.com/phoenix.zhtml?c\x3d130104\x26p\x3dirol-news_financial_invest\x26nyo\x3d0','secondaryUrlVariableField==WPMainBody');
g_navNode_3_0_1=g_navNode_3_0.addNode('197','SEC Filings',ssUrlPrefix + 'Investors/FinancialInformation/SECFilings/index.htm','RedirectURL==http\x3a//ir.wellpoint.com/phoenix.zhtml?c\x3d130104\x26p\x3dirol-sec','secondaryUrlVariableField==WPMainBody');
g_navNode_3_0_2=g_navNode_3_0.addNode('198','Annual Reports',ssUrlPrefix + 'Investors/FinancialInformation/AnnualReports/index.htm','RedirectURL==http\x3a//ir.wellpoint.com/phoenix.zhtml?c\x3d130104\x26p\x3dirol-reportsannual','secondaryUrlVariableField==WPMainBody');
g_navNode_3_0_3=g_navNode_3_0.addNode('199','Financial Strength Ratings',ssUrlPrefix + 'Investors/FinancialInformation/FinancialStrengthRatings/index.htm','RedirectURL==http\x3a//ir.wellpoint.com/phoenix.zhtml?c\x3d130104\x26p\x3dirol-financial_strength_ratings','secondaryUrlVariableField==WPMainBody');
g_navNode_3_1=g_navNode_3.addNode('105','Analyst Coverage',ssUrlPrefix + 'Investors/AnalystCoverage/index.htm','RedirectURL==http\x3a//ir.antheminc.com/phoenix.zhtml?c\x3d130104\x26p\x3dirol-analysts','secondaryUrlVariableField==WPMainBody');
g_navNode_3_2=g_navNode_3.addNode('106','Individual Investors',ssUrlPrefix + 'Investors/IndividualInvestors/index.htm','RedirectURL==http\x3a//ir.antheminc.com/phoenix.zhtml?c\x3d130104\x26p\x3dirol-individual_investors','secondaryUrlVariableField==WPMainBody');
g_navNode_3_2_0=g_navNode_3_2.addNode('108','Current Information',ssUrlPrefix + 'Investors/IndividualInvestors/LinkGoesHere/index.htm','RedirectURL==http\x3a//ir.wellpoint.com/phoenix.zhtml?c\x3d130104\x26p\x3dirol-current_information','secondaryUrlVariableField==WPMainBody');
g_navNode_3_2_1=g_navNode_3_2.addNode('193','FAQ',ssUrlPrefix + 'Investors/IndividualInvestors/FAQ/index.htm','RedirectURL==http\x3a//ir.wellpoint.com/phoenix.zhtml?c\x3d130104\x26p\x3dirol-faq','secondaryUrlVariableField==WPMainBody');
g_navNode_3_2_2=g_navNode_3_2.addNode('194','Individual Investor Contacts',ssUrlPrefix + 'Investors/IndividualInvestors/IndividualInvestorContacts/index.htm','RedirectURL==http\x3a//ir.wellpoint.com/phoenix.zhtml?c\x3d130104\x26p\x3dirol-contactindividual','secondaryUrlVariableField==WPMainBody');
g_navNode_3_2_3=g_navNode_3_2.addNode('196','Historical Anthem, Inc. Demutualization Info.',ssUrlPrefix + 'Investors/IndividualInvestors/HistoricalAnthemIncDemutualizationInfo/index.htm','RedirectURL==http\x3a//ir.wellpoint.com/phoenix.zhtml?c\x3d130104\x26p\x3dirol-demutualization','secondaryUrlVariableField==WPMainBody');
g_navNode_3_3=g_navNode_3.addNode('71','Corporate Governance',ssUrlPrefix + 'Investors/CorporateGovernance/index.htm','RedirectURL==http\x3a//ir.antheminc.com/phoenix.zhtml?c\x3d130104\x26p\x3dirol-govhighlights','secondaryUrlVariableField==WPMainBody');
g_navNode_3_3_0=g_navNode_3_3.addNode('109','Governance \x26 Corporate Documents',ssUrlPrefix + 'Investors/CorporateGovernance/GovernanceCorporateDocuments/index.htm','RedirectURL==http\x3a//ir.wellpoint.com/phoenix.zhtml?c\x3d130104\x26p\x3dirol-govguidelines','secondaryUrlVariableField==WPMainBody');
g_navNode_3_3_1=g_navNode_3_3.addNode('110','Transactions by Directors \x26 Officers',ssUrlPrefix + 'Investors/CorporateGovernance/TransactionsbyDirectorsOfficers/index.htm','RedirectURL==http\x3a//ir.wellpoint.com/phoenix.zhtml?c\x3d130104\x26p\x3dirol-govownership','secondaryUrlVariableField==WPMainBody');
g_navNode_3_3_2=g_navNode_3_3.addNode('72','Leadership',ssUrlPrefix + 'Investors/CorporateGovernance/Leadership/index.htm','secondaryUrlVariableField==WPMainBody');
g_navNode_3_3_2_0=g_navNode_3_3_2.addNode('73','Executive Leadership',ssUrlPrefix + 'Investors/CorporateGovernance/Leadership/ExecutiveLeadership/index.htm','secondaryUrlVariableField==WPMainBody');
g_navNode_3_3_2_1=g_navNode_3_3_2.addNode('114','Board of Directors',ssUrlPrefix + 'Investors/CorporateGovernance/Leadership/BoardofDirectors/index.htm','RedirectURL==http\x3a//ir.wellpoint.com/phoenix.zhtml?c\x3d130104\x26p\x3dirol-govboard');
g_navNode_3_3_2_2=g_navNode_3_3_2.addNode('113','Board Committees',ssUrlPrefix + 'Investors/CorporateGovernance/Leadership/BoardCommittees/index.htm','RedirectURL==http\x3a//ir.wellpoint.com/phoenix.zhtml?c\x3d130104\x26p\x3dirol-govcommcomp');
g_navNode_3_3_3=g_navNode_3_3.addNode('111','Contact Our Directors',ssUrlPrefix + 'Investors/CorporateGovernance/ContactOurDirectors/index.htm','RedirectURL==http\x3a//ir.wellpoint.com/phoenix.zhtml?c\x3d130104\x26p\x3dirol-aboutContact','secondaryUrlVariableField==WPMainBody');
g_navNode_3_4=g_navNode_3.addNode('175','Stock Information',ssUrlPrefix + 'Investors/StockInformation/index.htm','RedirectURL==http\x3a//ir.antheminc.com/phoenix.zhtml?c\x3d130104\x26p\x3dirol-stockquote','secondaryUrlVariableField==WPMainBody');
g_navNode_3_4_0=g_navNode_3_4.addNode('183','Stock Chart',ssUrlPrefix + 'Investors/StockInformation/StockChart/index.htm','RedirectURL==http\x3a//ir.wellpoint.com/phoenix.zhtml?c\x3d130104\x26p\x3dirol-stockchart','secondaryUrlVariableField==WPMainBody');
g_navNode_3_4_1=g_navNode_3_4.addNode('184','Historical Price Lookup',ssUrlPrefix + 'Investors/StockInformation/HistoricalPriceLookup/index.htm','RedirectURL==http\x3a//ir.wellpoint.com/phoenix.zhtml?c\x3d130104\x26p\x3dirol-stocklookup','secondaryUrlVariableField==WPMainBody');
g_navNode_3_4_2=g_navNode_3_4.addNode('185','Investment Calculator',ssUrlPrefix + 'Investors/StockInformation/InvestmentCalculator/index.htm','RedirectURL==http\x3a//ir.wellpoint.com/phoenix.zhtml?c\x3d130104\x26p\x3dirol-stockcalculator','secondaryUrlVariableField==WPMainBody');
g_navNode_3_5=g_navNode_3.addNode('176','Events/Webcasts',ssUrlPrefix + 'Investors/EventsWebcasts/index.htm','RedirectURL==http\x3a//ir.antheminc.com/phoenix.zhtml?c\x3d130104\x26p\x3dirol-calendar','secondaryUrlVariableField==WPMainBody');
g_navNode_3_5_0=g_navNode_3_5.addNode('180','Past Events',ssUrlPrefix + 'Investors/EventsWebcasts/PastEvents/index.htm','RedirectURL==http\x3a//ir.wellpoint.com/phoenix.zhtml?c\x3d130104\x26p\x3dirol-calendarSafeHarbor','secondaryUrlVariableField==WPMainBody');
g_navNode_3_6=g_navNode_3.addNode('177','Investor Contacts',ssUrlPrefix + 'Investors/InvestorContacts/index.htm','RedirectURL==http\x3a//ir.antheminc.com/phoenix.zhtml?c\x3d130104\x26p\x3dirol-contact','secondaryUrlVariableField==WPMainBody');
g_navNode_3_6_0=g_navNode_3_6.addNode('178','Email Alerts',ssUrlPrefix + 'Investors/InvestorContacts/EmailAlerts/index.htm','RedirectURL==http\x3a//ir.wellpoint.com/phoenix.zhtml?c\x3d130104\x26p\x3dirol-Alerts','secondaryUrlVariableField==WPMainBody');
g_navNode_3_6_1=g_navNode_3_6.addNode('179','Request Materials',ssUrlPrefix + 'Investors/InvestorContacts/RequestMaterials/index.htm','RedirectURL==http\x3a//iirc.corporate-ir.net/phoenix.zhtml?c\x3d130104\x26p\x3dirol-infoReq','secondaryUrlVariableField==WPMainBody');
g_navNode_4=g_navNode_Root.addNode('46','Corporate Responsibility',ssUrlPrefix + 'CR/index.htm','NavigationalBar==header','secondaryUrlVariableField==WPMainBody');
g_navNode_4_0=g_navNode_4.addNode('81','Diversity',ssUrlPrefix + 'CR/Diversity/index.htm','http://www.antheminc.com/prodcontrib/websites/wellpointcom/RedirectURL==http/x3a//diversity.antheminc.com','secondaryUrlVariableField==WPMainBody');
g_navNode_4_0_0=g_navNode_4_0.addNode('82','WellPoint\'s Commitment',ssUrlPrefix + 'CR/Diversity/WellPointsCommitment/index.htm','http://www.antheminc.com/prodcontrib/websites/wellpointcom/RedirectURL==http/x3a//www.wellpointdiversity.com/wellpoint-commitment.asp','secondaryUrlVariableField==WPMainBody');
g_navNode_4_0_1=g_navNode_4_0.addNode('83','In The Workplace',ssUrlPrefix + 'CR/Diversity/InTheWorkplace/index.htm','http://www.antheminc.com/prodcontrib/websites/wellpointcom/RedirectURL==http/x3a//www.wellpointdiversity.com/in-the-workplace.asp','secondaryUrlVariableField==WPMainBody');
g_navNode_4_0_2=g_navNode_4_0.addNode('84','In The Community',ssUrlPrefix + 'CR/Diversity/InTheCommunity/index.htm','http://www.antheminc.com/prodcontrib/websites/wellpointcom/RedirectURL==http/x3a//www.wellpointdiversity.com/in-the-community.asp','secondaryUrlVariableField==WPMainBody');
g_navNode_4_0_2_0=g_navNode_4_0_2.addNode('212','Ethnic Outreach Programs',ssUrlPrefix + 'CR/Diversity/InTheCommunity/EthnicOutreachPrograms/index.htm','http://www.antheminc.com/prodcontrib/websites/wellpointcom/RedirectURL==http/x3a//www.wellpointdiversity.com/ethnic-outreach-program.asp','secondaryUrlVariableField==WPMainBody');
g_navNode_4_0_2_1=g_navNode_4_0_2.addNode('213','Women\'s Health Programs',ssUrlPrefix + 'CR/Diversity/InTheCommunity/WomensHealthPrograms/index.htm','http://www.antheminc.com/prodcontrib/websites/wellpointcom/RedirectURL==http/x3a//www.wellpointdiversity.com/women-health-program.asp','secondaryUrlVariableField==WPMainBody');
g_navNode_4_0_3=g_navNode_4_0.addNode('214','Supplier Diversity',ssUrlPrefix + 'CR/Diversity/SupplierDiversity/index.htm','secondaryUrlVariableField==WPMainBody');
g_navNode_4_0_4=g_navNode_4_0.addNode('86','Diversity Recognition',ssUrlPrefix + 'CR/Diversity/DiversityRecognition/index.htm','http://www.antheminc.com/prodcontrib/websites/wellpointcom/RedirectURL==http/x3a//www.wellpointdiversity.com/diversity-recognition.asp','secondaryUrlVariableField==WPMainBody');
g_navNode_4_1=g_navNode_4.addNode('79','Charitable Foundations',ssUrlPrefix + 'CR/CharitableFoundations/index.htm','secondaryUrlVariableField==WPMainBody');
g_navNode_4_2=g_navNode_4.addNode('80','Community Outreach',ssUrlPrefix + 'CR/CommunityOutreach/index.htm','secondaryUrlVariableField==WPMainBody');
g_navNode_5=g_navNode_Root.addNode('47','Careers',ssUrlPrefix + 'Careers/index.htm','NavigationalBar==header','http://www.antheminc.com/prodcontrib/websites/wellpointcom/RedirectURL==http/x3a//www.careers.antheminc.com','secondaryUrlVariableField==WPMainBody');
g_navNode_5_0=g_navNode_5.addNode('215','Your Area of Talent',ssUrlPrefix + 'Careers/YourAreaofTalent/index.htm','http://www.antheminc.com/prodcontrib/websites/wellpointcom/RedirectURL==http/x3a//www.careers.antheminc.com/your-area-of-talent.aspx','secondaryUrlVariableField==WPMainBody');
g_navNode_5_1=g_navNode_5.addNode('155','Search Jobs',ssUrlPrefix + 'Careers/SearchJobs/index.htm','http://www.antheminc.com/prodcontrib/websites/wellpointcom/RedirectURL==http/x3a//www.careers.antheminc.com/Search-Jobs.aspx','secondaryUrlVariableField==WPMainBody');
g_navNode_5_1_0=g_navNode_5_1.addNode('172','Staffing Process',ssUrlPrefix + 'Careers/SearchJobs/StaffingProcess/index.htm','http://www.antheminc.com/prodcontrib/websites/wellpointcom/RedirectURL==http/x3a//careers.antheminc.com/Staffing-Process.aspx','secondaryUrlVariableField==WPMainBody');
g_navNode_5_1_1=g_navNode_5_1.addNode('173','Open Job Cart',ssUrlPrefix + 'Careers/SearchJobs/OpenJobCart/index.htm','http://www.antheminc.com/prodcontrib/websites/wellpointcom/RedirectURL==http/x3a//www.careersatwellpoint.com/JobCart.aspx','secondaryUrlVariableField==WPMainBody');
g_navNode_5_1_2=g_navNode_5_1.addNode('200','Actuarial',ssUrlPrefix + 'Careers/SearchJobs/Actuarial/index.htm','http://www.antheminc.com/prodcontrib/websites/wellpointcom/RedirectURL==http/x3a//www.careersatwellpoint.com/AOT_Actuarial.aspx','secondaryUrlVariableField==WPMainBody');
g_navNode_5_1_3=g_navNode_5_1.addNode('201','Administrative',ssUrlPrefix + 'Careers/SearchJobs/Administrative/index.htm','http://www.antheminc.com/prodcontrib/websites/wellpointcom/RedirectURL==http/x3a//www.careersatwellpoint.com/AOT_Administrative.aspx','secondaryUrlVariableField==WPMainBody');
g_navNode_5_1_4=g_navNode_5_1.addNode('202','Business Development / Sales',ssUrlPrefix + 'Careers/SearchJobs/BusinessDevelopmentSales/index.htm','http://www.antheminc.com/prodcontrib/websites/wellpointcom/RedirectURL==http/x3a//www.careersatwellpoint.com/AOT_Business.aspx','secondaryUrlVariableField==WPMainBody');
g_navNode_5_1_5=g_navNode_5_1.addNode('203','Communications',ssUrlPrefix + 'Careers/SearchJobs/Communications/index.htm','http://www.antheminc.com/prodcontrib/websites/wellpointcom/RedirectURL==http/x3a//www.careersatwellpoint.com/AOT_Communications.aspx','secondaryUrlVariableField==WPMainBody');
g_navNode_5_1_6=g_navNode_5_1.addNode('204','Customer Care',ssUrlPrefix + 'Careers/SearchJobs/CustomerCare/index.htm','http://www.antheminc.com/prodcontrib/websites/wellpointcom/RedirectURL==http/x3a//www.careersatwellpoint.com/AOT_Customer.aspx?clicked/x3d0','secondaryUrlVariableField==WPMainBody');
g_navNode_5_1_7=g_navNode_5_1.addNode('205','Finance',ssUrlPrefix + 'Careers/SearchJobs/Finance/index.htm','http://www.antheminc.com/prodcontrib/websites/wellpointcom/RedirectURL==http/x3a//www.careersatwellpoint.com/AOT_Finance.aspx?clicked/x3d0','secondaryUrlVariableField==WPMainBody');
g_navNode_5_1_8=g_navNode_5_1.addNode('206','Human Resources',ssUrlPrefix + 'Careers/SearchJobs/HumanResources/index.htm','http://www.antheminc.com/prodcontrib/websites/wellpointcom/RedirectURL==http/x3a//www.careersatwellpoint.com/AOT_Human.aspx','secondaryUrlVariableField==WPMainBody');
g_navNode_5_1_9=g_navNode_5_1.addNode('207','Medical',ssUrlPrefix + 'Careers/SearchJobs/Medical/index.htm','http://www.antheminc.com/prodcontrib/websites/wellpointcom/RedirectURL==http/x3a//www.careersatwellpoint.com/AOT_Medical.aspx?clicked/x3d0','secondaryUrlVariableField==WPMainBody');
g_navNode_5_1_10=g_navNode_5_1.addNode('208','Metrics \x26 Data',ssUrlPrefix + 'Careers/SearchJobs/MetricsData/index.htm','http://www.antheminc.com/prodcontrib/websites/wellpointcom/RedirectURL==http/x3a//www.careersatwellpoint.com/AOT_Metrics.aspx?clicked/x3d0','secondaryUrlVariableField==WPMainBody');
g_navNode_5_1_11=g_navNode_5_1.addNode('209','Operations',ssUrlPrefix + 'Careers/SearchJobs/Operations/index.htm','http://www.antheminc.com/prodcontrib/websites/wellpointcom/RedirectURL==http/x3a//www.careersatwellpoint.com/AOT_Operations.aspx?clicked/x3d0','secondaryUrlVariableField==WPMainBody');
g_navNode_5_1_12=g_navNode_5_1.addNode('210','Technology',ssUrlPrefix + 'Careers/SearchJobs/Technology/index.htm','http://www.antheminc.com/prodcontrib/websites/wellpointcom/RedirectURL==http/x3a//www.careersatwellpoint.com/AOT_Technology.aspx','secondaryUrlVariableField==WPMainBody');
g_navNode_5_1_13=g_navNode_5_1.addNode('211','Underwriting',ssUrlPrefix + 'Careers/SearchJobs/Underwriting/index.htm','http://www.antheminc.com/prodcontrib/websites/wellpointcom/RedirectURL==http/x3a//www.careersatwellpoint.com/AOT_Underwriting.aspx','secondaryUrlVariableField==WPMainBody');
g_navNode_5_2=g_navNode_5.addNode('156','Our Locations',ssUrlPrefix + 'Careers/OurLocations/index.htm','http://www.antheminc.com/prodcontrib/websites/wellpointcom/RedirectURL==http/x3a//www.careers.antheminc.com/Our-Locations.aspx','secondaryUrlVariableField==WPMainBody');
g_navNode_5_3=g_navNode_5.addNode('157','Why Choose Anthem',ssUrlPrefix + 'Careers/WhyChooseAnthem/index.htm','http://www.antheminc.com/prodcontrib/websites/wellpointcom/RedirectURL==http/x3a//www.careers.antheminc.com/Why-Choose-WellPoint.aspx','secondaryUrlVariableField==WPMainBody');
g_navNode_5_3_0=g_navNode_5_3.addNode('161','Company Information',ssUrlPrefix + 'Careers/WhyChooseAnthem/CompanyInformation/index.htm','http://www.antheminc.com/prodcontrib/websites/wellpointcom/RedirectURL==http/x3a//www.careersatwellpoint.com/Company-Information.aspx','secondaryUrlVariableField==WPMainBody');
g_navNode_5_3_1=g_navNode_5_3.addNode('162','Our Culture',ssUrlPrefix + 'Careers/WhyChooseAnthem/OurCulture/index.htm','http://www.antheminc.com/prodcontrib/websites/wellpointcom/RedirectURL==http/x3a//www.careersatwellpoint.com/Our-Culture.aspx','secondaryUrlVariableField==WPMainBody');
g_navNode_5_3_2=g_navNode_5_3.addNode('163','Life in Our Markets',ssUrlPrefix + 'Careers/WhyChooseAnthem/LifeinOurMarkets/index.htm','http://www.antheminc.com/prodcontrib/websites/wellpointcom/RedirectURL==http/x3a//www.careersatwellpoint.com/Life-in-Our-Markets.aspx','secondaryUrlVariableField==WPMainBody');
g_navNode_5_3_3=g_navNode_5_3.addNode('164','Benefits',ssUrlPrefix + 'Careers/WhyChooseAnthem/Benefits/index.htm','http://www.antheminc.com/prodcontrib/websites/wellpointcom/RedirectURL==http/x3a//www.careersatwellpoint.com/Benefits.aspx','secondaryUrlVariableField==WPMainBody');
g_navNode_5_3_4=g_navNode_5_3.addNode('165','Wellness Program',ssUrlPrefix + 'Careers/WhyChooseAnthem/WellnessProgram/index.htm','http://www.antheminc.com/prodcontrib/websites/wellpointcom/RedirectURL==http/x3a//www.careersatwellpoint.com/Wellness-Program.aspx','secondaryUrlVariableField==WPMainBody');
g_navNode_5_3_5=g_navNode_5_3.addNode('166','Life/Work Balance',ssUrlPrefix + 'Careers/WhyChooseAnthem/LifeWorkBalance/index.htm','http://www.antheminc.com/prodcontrib/websites/wellpointcom/RedirectURL==http/x3a//www.careersatwellpoint.com/Life-Work-Balance.aspx','secondaryUrlVariableField==WPMainBody');
g_navNode_5_3_6=g_navNode_5_3.addNode('167','Awards and Recognition',ssUrlPrefix + 'Careers/WhyChooseAnthem/AwardsandRecognition/index.htm','http://www.antheminc.com/prodcontrib/websites/wellpointcom/RedirectURL==http/x3a//www.careersatwellpoint.com/Awards-and-Recognition.aspx','secondaryUrlVariableField==WPMainBody');
g_navNode_5_4=g_navNode_5.addNode('158','Student Programs',ssUrlPrefix + 'Careers/StudentPrograms/index.htm','http://www.antheminc.com/prodcontrib/websites/wellpointcom/RedirectURL==http/x3a//www.careers.antheminc.com/Student-Programs.aspx','secondaryUrlVariableField==WPMainBody');
g_navNode_5_4_0=g_navNode_5_4.addNode('168','Undergraduate Programs',ssUrlPrefix + 'Careers/StudentPrograms/UndergraduatePrograms/index.htm','http://www.antheminc.com/prodcontrib/websites/wellpointcom/RedirectURL==http/x3a//www.careersatwellpoint.com/Undergraduate-Programs.aspx','secondaryUrlVariableField==WPMainBody');
g_navNode_5_4_1=g_navNode_5_4.addNode('169','Graduate Programs',ssUrlPrefix + 'Careers/StudentPrograms/GraduatePrograms/index.htm','http://www.antheminc.com/prodcontrib/websites/wellpointcom/RedirectURL==http/x3a//www.careersatwellpoint.com/Graduate-Programs.aspx','secondaryUrlVariableField==WPMainBody');
g_navNode_5_4_2=g_navNode_5_4.addNode('170','Parent\'s Resources',ssUrlPrefix + 'Careers/StudentPrograms/ParentsResources/index.htm','http://www.antheminc.com/prodcontrib/websites/wellpointcom/RedirectURL==http/x3a//www.careersatwellpoint.com/Parents-Resources.aspx','secondaryUrlVariableField==WPMainBody');
g_navNode_5_4_3=g_navNode_5_4.addNode('171','Campus Calendar',ssUrlPrefix + 'Careers/StudentPrograms/CampusCalendar/index.htm','http://www.antheminc.com/prodcontrib/websites/wellpointcom/RedirectURL==http/x3a//www.careersatwellpoint.com/Campus-Calendar.aspx','secondaryUrlVariableField==WPMainBody');
g_navNode_5_4_4=g_navNode_5_4.addNode('216','Associations',ssUrlPrefix + 'Careers/StudentPrograms/Associations/index.htm','http://www.antheminc.com/prodcontrib/websites/wellpointcom/RedirectURL==http/x3a//www.careersatwellpoint.com/Associations.aspx','secondaryUrlVariableField==WPMainBody');
g_navNode_5_5=g_navNode_5.addNode('159','Recruiting Events',ssUrlPrefix + 'Careers/RecruitingEvents/index.htm','http://www.antheminc.com/prodcontrib/websites/wellpointcom/RedirectURL==http/x3a//www.careers.antheminc.com/Recruiting-Events.aspx','secondaryUrlVariableField==WPMainBody');
g_navNode_5_6=g_navNode_5.addNode('160','Commitment to Diversity',ssUrlPrefix + 'Careers/CommitmenttoDiversity/index.htm','http://www.antheminc.com/prodcontrib/websites/wellpointcom/RedirectURL==http/x3a//www.careers.antheminc.com/Commitment-to-Diversity.aspx','secondaryUrlVariableField==WPMainBody');
g_navNode_7=g_navNode_Root.addNode('236','Legal',ssUrlPrefix + 'Legal/index.htm','NavigationalBar==footer','secondaryUrlVariableField==WPMainBody');
g_navNode_8=g_navNode_Root.addNode('237','Privacy',ssUrlPrefix + 'Privacy/index.htm','NavigationalBar==footer','secondaryUrlVariableField==WPMainBody');
g_navNode_10=g_navNode_Root.addNode('239','Associates',ssUrlPrefix + 'Employees/index.htm','NavigationalBar==footer','RedirectURL==https\x3a//hrsolutions.wellpoint.com/','secondaryUrlVariableField==WPMainBody');
g_navNode_11=g_navNode_Root.addNode('227','Suppliers',ssUrlPrefix + 'Suppliers/index.htm','NavigationalBar==footer','secondaryUrlVariableField==WPMainBody');
g_navNode_11_0=g_navNode_11.addNode('228','Supplier Registration Process',ssUrlPrefix + 'Suppliers/SupplierRegistrationProcess/index.htm','secondaryUrlVariableField==WPMainBody');
g_navNode_12=g_navNode_Root.addNode('240','Site Map',ssUrlPrefix + 'SiteMap/index.htm','NavigationalBar==footer','secondaryUrlVariableField==WPMainBody');
g_navNode_13=g_navNode_Root.addNode('241','Contact Us',ssUrlPrefix + 'ContactUs/index.htm','NavigationalBar==footer','secondaryUrlVariableField==WPMainBody');
