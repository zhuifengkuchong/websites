
$(document).ready(function () {

    // Creating custom :external selector
    $.expr[':'].external = function (obj) {
     //   alert('website' + obj.hostname)
        return !obj.href.match(/^mailto\:/)
            && !obj.href.match(/^javascript\:/)
                && (obj.hostname != location.hostname)
                && !((obj.hostname.match('http://www.kindermorgan.com/js/kindermorgan.com') || (obj.hostname.match('http://www.kindermorgan.com/js/kindermorgantreating.com')) || (obj.hostname.match('http://www.kindermorgan.com/js/businesswire.com')) || (obj.hostname.match('http://www.kindermorgan.com/js/peopleclick.com')) || (obj.hostname.match('http://www.kindermorgan.com/js/bostco.com')) || (obj.hostname.match('http://www.kindermorgan.com/js/taxpackages.com')) || (obj.hostname.match('http://www.kindermorgan.com/js/computershare.com'))));
    };

    // Add 'external' CSS class to all external links
    $('a:external').addClass('external');

    $('.external').click(function () {
        var link = $(this).attr('href');

        $('<div>You are about to leave the Kinder Morgan domain. The web site you are about to visit is not controlled by Kinder Morgan therefore Kinder Morgan is not responsible for any content in that web site. This link is provided to you only as a convenience. The presence of this link in the Kinder Morgan web site is not intended as a Kinder Morgan endorsement of the non-Kinder Morgan web site<br /> ' + (link) + ' <br /> are you sure you want to proceed?</div>').dialog({
            title: "External Link",
            modal: true,
            overlay: {
                backgroundColor: '#000',
                opacity: 0.5
            },
            buttons: {
                'Okay': function () {
                    $(this).dialog('close').remove();
                    window.open(link);
                },
                'Cancel': function () {
                    $(this).dialog('close').remove();
                    return false;
                }
            }
        });

        return false;
    });
});