Handlebars.registerHelper('stockChangeColor', function (change) {
    // If the 'change' defined for the row is greater than zero, style it with green, otherwise red
    if (change > 0) {
        return new Handlebars.SafeString('<span style="color: #229222;">' + change + '</span>');
    }
    else {
        return new Handlebars.SafeString('<span style="color: #c8141c;">' + change + '</span>');
    }
});

Handlebars.registerHelper('tickerTimestamp', function (rows) {
    //[as of May 12, 2014 13:30 PM ET]
    return new Handlebars.SafeString(rows[0].last_update);
});

Handlebars.registerHelper('presentationTable', function (rows) {
    // Build our data structure because our rows come back denormalized from the database
    // We need to group the documents by their event/confernence/etc
    // We are building a map of events denoted by the {}
    // The map is a source event title, mapping to a list of documents for that particular event
    var events = {};
    // Loop over each row returned from the database
    for (var i = 0; i < rows.length; i++) {
        var row = rows[i];
        // Get the source event from the row, or headline if it is NULL
        var source_event = row.source_event !== "NULL" ? row.source_event : row.headline;
        // If our source event is not in our map, add it now with an empty set of records
        if (!(source_event in events)) {
            events[source_event] = { SourceEvent: source_event, Date: row.news_date.split(" ")[0], Records: [] };
        }

        // Add our current row to the records
        events[source_event].Records.push({ Name: row.headline, Url: row.hyperlink, Instructions: row.link_instructions, Type: row.type });
    }
   

    //// Convert events to a list of objects instead of dictionary,
    //events = Object.keys(events).map(function (key) {
    //    return events[key];
    //});
    // convert dictionary to a list - coded for IE8 compatiblity - see objKeysFix.js
    var keys = Object.keys(events);
    eventValues = $.map(keys, function (key) { return events[key]; })

   
    // Build HTML using handlebars templates as helpers for formatting content
    var result = "";
    // Loop over our list of events
    for (var i = 0; i < eventValues.length; i++) {
        var event = eventValues[i];
        // Build the heading using another handlebars template
        // PresentationHead and PresentationRow are two templates defined in our handlebars-partials.hbs file
        result += $.handlebarTemplates.partials.PresentationHead(event);
        // Then loop over the records for the event
        for (var j = 0; j < event.Records.length; j++) {
            var record = event.Records[j];
            // And build the resulting HTML using another handlebars template
            result += $.handlebarTemplates.partials.PresentationRow(record);
        }
    }

    return new Handlebars.SafeString(result);
});

Handlebars.registerHelper('presentationIcon', function (fileName, recordType, Name ) {
    // Based on a record type, this will return the appropriate icon image source
    var icon = '';
    if ((recordType == "Webcast") || (Name.toLowerCase().indexOf("webcast") > -1 )) {
       icon = "../images/icon_webcast.gif"/*tpa=http://www.kindermorgan.com/images/icon_webcast.gif*/;
    } else
       { 
   
    var extension = fileName.substr( (fileName.lastIndexOf('.') +1) );
    switch(extension) {

    case 'pdf':
        icon = "../images/icon_pdf.gif"/*tpa=http://www.kindermorgan.com/images/icon_pdf.gif*/;
        break;
    case 'docx':
    case 'doc':
        icon = "../images/icon_word.gif"/*tpa=http://www.kindermorgan.com/images/icon_word.gif*/;
    case 'mov':
    case 'asf':
    case 'asx':
        icon = "../images/icon_mediaPlayer.gif"/*tpa=http://www.kindermorgan.com/images/icon_mediaPlayer.gif*/;
        break;
  
    default:
        icon = "../images/icon_HTML.gif"/*tpa=http://www.kindermorgan.com/images/icon_HTML.gif*/;
    }
    }

    return new Handlebars.SafeString('<img src="' + icon + '" />');
});

// The same approach as presentationTable is used for PPL Tariffs.
// Build a dictionary of lists, keying section to list of documents for that section
// This code is incomplete as it was done during a training session but will stay as an example or to be completed
Handlebars.registerHelper('TariffsTableFunc', function (rows) {
    // Build data structure
    //console.log('TariffsTableFunc');
    //console.log(rows);
    var sections = {};
    var sectionArray = {};
    for (var i = 0; i < rows.length; i++) {
        var row = rows[i];
        var section = row.SubCategory;
        var subcategory = row.SubCategory;
        if (!(section in sections)) {
            sections[section] = { Section: row.Section, SubCategory: subcategory, Records: [] };
        }
        sections[section].Records.push(row);
    }

// convert dictionary to a list - coded for IE8 compatiblity - see objKeysFix.js
    var keys = Object.keys(sections);
    sectionValues = $.map(keys, function (key) { return sections[key];})


    // Loop over our sections and records in each section, building the appropriate HTML by
    // utilizing other Handlebars templates
    var result = '';
    for (var i = 0; i < sectionValues.length; i++) {
        var section = sectionValues[i];

        if (section.Section.indexOf("Tariff") > -1) {
            var sectionHdr = $.handlebarTemplates.partials.TariffFullHead(section);
        }
        else {
            var sectionHdr = $.handlebarTemplates.partials.TariffHead(section);
        }
        result += sectionHdr;
        for (var j = 0; j < section.Records.length; j++) {
            var record = section.Records[j];
          //  record["DivClass"] = (j % 2 == 1) ? "grey" : "";
         //   console.log(section.Section.indexOf("Tariff"))
            if (section.Section.indexOf("Tariff") > -1) {
                result += $.handlebarTemplates.partials.TariffFullRow(record);
            }
            else {
              
            result += $.handlebarTemplates.partials.TariffRow(record);
            }
           
        }
        result += $.handlebarTemplates.partials.TariffFullTableEnd(section);
    }

    return new Handlebars.SafeString(result);
});
