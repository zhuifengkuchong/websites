<!--#?TariffsTable-->
<script>
    $("a[downloadOnImage|='serverdoc']").each(function () {
        $(this).click(function () {
            $('<iframe src="/pages/_services/Download.aspx?docId=' + $(this).attr("downloadOnImage").replace("serverdoc-", "") + '&docName=' + $(this).attr("serverDocName") + '"></iframe>').appendTo('body').hide();
        });
    });
</script>
<script>
    $("a[download|='serverdoc']").each(function () {
        $(this).click(function () {
            $('<iframe src="/pages/_services/Download.aspx?docId=' + $(this).attr("download").replace("serverdoc-", "") + '&docName=' + $(this).html() + '"></iframe>').appendTo('body').hide();
        });
    });
</script>
{{TariffsTableFunc rows}}
<!--#?end-->

<!--#?TariffHead-->

<div class="col-xs-12">&nbsp;</div>
<h4>{{Section}}</h4>
<table class="table table-responsive"  id="stripedTable">
	<thead>
	</thead>
	<tbody>
<!--#?end-->


<!--#?TariffRow-->
{{!<div class="row" id="rowContent">}}
    <tr><td>
  <div class="col-xs-10 {{DivClass}}">{{DocTitle}} {{Index}}</div>
  <div class="col-xs-2 {{DivClass}}">
      <a downloadOnImage='serverdoc-{{DocID}}' serverDocName="{{DocTitle}}" style='cursor:pointer;' title="Click to open {{DocTitle}}" >
      <img src="../images/icon_pdf.gif"/*tpa=http://www.kindermorgan.com/images/icon_pdf.gif*/ alt="click to open" />
    </a>
  </div>
 {{! <div class="col-xs-1"></div> }}
        </td></tr>
{{!</div>}}
<!--#?end-->

<!--#?TariffTable-->
<table id="stripedTable2">
	<thead>
		<tr><th>Product</th><th>Origin</th><th>Destination</th><th>Document</th></tr>
	</thead>
	<tbody>
		{{#each rows}}
		<tr>
		  <td data-label="Product">{{Product}}</td>
          <td data-label="Origin">{{Origin}}</td>
          <td data-label="Destination">{{Destination}}</td>
          <td data-label="Document">
              <a download='serverdoc-{{DocID}}'  style='cursor:pointer;'>{{DocTitle}}</a>
		  </td>
		</tr>
		{{/each}}
	</tbody>
</table>

<!--#?end-->

<!--#?TariffFullHead-->

<div class="col-xs-12">&nbsp;</div>
<h4>{{SubCategory}}</h4>
<table class="table-responsive"  id="stripedTable1">
	<thead>
		<tr><th width="20%">Product</th><th width="20%" >Origin</th><th width="20%">Destination</th><th width="40%">Document</th></tr>
	</thead>
	<tbody>

<!--#?end-->
        
<!--#?TariffFullTableEnd-->
	</tbody>
    </table>
<!--#?end-->	

<!--#?TariffFullRow-->
		<tr>
		  <td data-label="Product" class="{{DivClass}}" >{{Product}}</td>
          <td data-label="Origin" class="{{DivClass}}">{{Origin}}</td>
          <td data-label="Destination" class="{{DivClass}}">{{Destination}}</td>
          <td data-label="Document" class="{{DivClass}}">
        <a download='serverdoc-{{DocID}}' style='cursor:pointer;'>{{DocTitle}}</a>
		  </td>
		</tr>
	
<!--#?end-->

<!--#?Marquee-->
{{#each rows}}
  {{#if @first}}
    <div class="marquee" behavior="scroll" scrollamount="2" direction="left">
      <a href="{{link}}">{{headline}}</a>
    </div>    
  {{/if}}
{{/each}}
<!--#?end-->

<!--#?Ticker-->
<table border="0" cellpadding="0" cellspacing="0" width="180">
  <tbody>
    <tr>
      <td colspan="4" class="stock_timestamp">
        {{tickerTimestamp rows}}
      </td>
    </tr>
    {{#each rows}}
    <tr>
      <td width="100" class="stock">
        <a class="stock" href="http://www.kindermorgan.com/investor/{{stockSymbol}}_stock_quote.cfm" target="_parent">{{stockSymbol}}</a>
      </td>
      <td width="100" class="stock">
        <img src="{{stockImage}}" alt="stockdelta" />
      </td>
      <td width="100" class="stock">{{price}}</td>
      <td width="100" class="stock">
        {{stockChangeColor change}}
      </td>
    </tr>
    {{/each}}
  </tbody>
</table>
<!--#?end-->

<!--#?RecentNews-->
<ul class="newsLinks">
  {{#each rows}}
  <li class="newsLink">
    <a href="{{link}}" target="_blank">{{headline}}</a>
  </li>
  {{/each}}
</ul>
<!--#?end-->

<!--#?ErrorTemplate-->
<p>Error Retrieving Data</p>
<!--#?end-->

<!--#?PresentationTab-->
<ul class="nav nav-tabs" id="yearTabs">
  {{#each rows}}
  <li>
    <a href='#' data-toggle='tab'>{{yr}}</a>
  </li>
  {{/each}}
  <li>
    <a href='#' data-toggle='tab'>Archives</a>
  </li>
</ul>
<!--#?end-->

<!--#?PresentationTable-->
<div>{{presentationTable rows}}</div>
<!--#?end-->

<!--#?PresentationHead-->
<div class="row">
   <div class="col-md-2">
    <b>{{Date}}</b>
  </div>
  <div class="col-md-10"  id="rowHeader">{{SourceEvent}}</div>
   </div>
<!--#?end-->

<!--#?PresentationRow-->
<div class="row">
  <div class="col-xs-8 col-xs-offset-2"  style="border-top: 1px solid #CDCDCD;">{{Name}}&nbsp;{{Instructions}}</div>
  <div class="col-xs-2" style="border-top: 1px solid #CDCDCD;">
    <a href="{{Url}}" border="0" target="_blank">{{presentationIcon Url Type Name}}</a>
  </div>
</div>
<!--#?end-->


<!--#?DividendTable-->
<table class="responsive">
	<thead>
		<tr><th>Declared</th><th>Ex-Date</th><th>Record</th><th>Payable</th><th>Amount</th><th>Type</th></tr>
	</thead>
	<tbody>
		{{#each rows}}
		<tr>
		  <td data-label="DeclaredDate">{{DeclaredDate}}</td>
      <td data-label="ExDate">{{ExDate}}</td>
      <td data-label="Record">{{Record}}</td>
     <td data-label="Payable">{{Payable}}</td>
      <td data-label="Amount">{{Amount}}</td>
      <td data-label="Type">{{Type}}</td>
		</tr>
		{{/each}}
	</tbody>
</table>

<!--#?end-->