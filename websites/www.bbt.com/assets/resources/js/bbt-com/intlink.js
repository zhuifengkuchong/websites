$(document).ready(function(){
   if (jQuery.cookie("main") != null) {
      var strMain = getCookie("main");
	  strMain = strMain.substring(1,strMain.length-1);
	  strMain = strMain.replace(/xx/g, ',');
	  s.prop22 = strMain;
	  s.list1 = strMain;
   }
   
   $("a").click(function(){
   
      var link = this.innerHTML;
      if (link != null && link.length > 0) {
		  link = $.trim(link);
		  var linkid;
		  var classname;
	    
		  // if global navigation code (mega-flyout)
		  if ($(this).closest(".topNavigation").attr("role") != null ||
			  $(this).closest(".topTabs").attr("role") != null) {
			 var thisclassname = $(this).attr("class");
			 var liclassname = $(this).closest("li").attr("class");
			 var section = $(".activeSection").children(":first").text();
			 linkid = "area-global-navigation"; 
			 
			 if (thisclassname == null && liclassname != null && liclassname.indexOf("mainmenu") == 0) {
				link = section + ":" + link;
			 } else if (thisclassname != null && thisclassname.indexOf("linkhead") == 0){
				var menu = $(this).closest("ul").find(".linkhead").closest(".mainmenu").children(":first").text();
				link = section + ":" + menu + ":" + link;
			 } else if (thisclassname != null && (liclassname == null || liclassname.indexOf("ection") == -1)) {
				var header = $(this).closest("ul").find(".linkhead").text();
				var menu = $(this).closest("ul").find(".linkhead").closest(".mainmenu").children(":first").text();
				link = section + ":" + menu + ":" + header + ":" + link;
			 }
			 createCookie("intlinklabel", link.replace(/&amp;/g,"and").replace(/&/g,"and"), 60);
			 createCookie("intlinkurl", this.href, 60);
			 createCookie("intlinkid", linkid, 60);
		 }  else {
			 if (link.toLowerCase().indexOf("<img")!==-1) {
				link = link.substring(link.lastIndexOf("/")+1, link.lastIndexOf(".")+4);
				link = link.substring(0, link.indexOf(".")+4);
			 }
			 classname = $(this).closest("[class^=ls-area area]").attr("class");
			(classname == null) ? linkid = $(this).closest("[class^=area]").attr("class") : linkid =  classname.substring(8); 
			 createCookie("intlinklabel", link.replace(/&amp;/g,"and").replace(/&/g,"and"), 60);
			 createCookie("intlinkurl", this.href, 60);
			 createCookie("intlinkid", linkid, 60); 
		  }
      }
   });


   if (jQuery.cookie("intlinkurl") != null) {
      var intlinkurl = getCookie("intlinkurl");
      var intlinklabel = getCookie("intlinklabel");
      var intlinkid = getCookie("intlinkid");
      var intlinktype;
      (intlinkid == null) ? intlinktype="b" : intlinktype =  intlinkid.substring(5,6); 

      if (intlinklabel != null && intlinklabel.toLowerCase().indexOf("src=")==0) {
         intlinklabel = intlinklabel.substring(intlinklabel.toLowerCase().indexOf("src=")+5);
      } else if (intlinklabel != null) {
         intlinklabel = intlinklabel.toLowerCase().replace(/^\s+|\s+$/g, '').replace(/ /g, "_");
      } else {
		intlinklabel = "home";
	  }

      s.prop20 = intlinktype + ":" + intlinklabel;
      s.eVar20 = intlinktype + ":" + intlinklabel;

      // delete the cookie so we don't trigger this code for hyperlinks in 
      // the header, body, right rail or footer
      createCookie("intlinkurl", "", 0);
   }

});

$(document).ready(function(){
   $("#vid-buttons > a").click(function(){
		var link = this.innerHTML;
		var linkid = "area-video"; 
		
		createCookie("intlinklabel", link.replace(/&amp;/g,"and").replace(/&/g,"and").toLowerCase().replace(/^\s+|\s+$/g, '').replace(/ /g, "_", 60));
		//alert("href is " + this.href);
		createCookie("intlinkurl", this.href, 60);
		createCookie("intlinkid", linkid, 60); 
   });

   $(".res-link > a, #lnkMoreLocations, .c2aBtn1").click(function(){  //#lnkSubmitATMLocatorForm,
		//alert("test .res-link > a, #lnkMoreLocations, .c2aBtn1");
	   if (jQuery.cookie("intlinkurl") != null) {
		  var intlinkurl = getCookie("intlinkurl");
		  var intlinklabel = getCookie("intlinklabel");
		  var intlinkid = getCookie("intlinkid");
		  var intlinktype;
		  (intlinkid == null) ? intlinktype="b" : intlinktype =  intlinkid.substring(5,6); 

		  if (intlinklabel != null && intlinklabel.toLowerCase().indexOf("src=")==0) {
			 intlinklabel = intlinklabel.substring(intlinklabel.toLowerCase().indexOf("src=")+5);
		  } else if (intlinklabel != null) {
			 intlinklabel = intlinklabel.toLowerCase().replace(/^\s+|\s+$/g, '').replace(/ /g, "_");
		  } else {
			intlinklabel = "home";
		  }
		  s.prop20 = intlinktype + ":" + intlinklabel;
		  s.eVar20 = intlinktype + ":" + intlinklabel;
		  s.t();

		  // delete the cookie so we don't trigger this code for hyperlinks in 
		  // the header, body, right rail or footer
		  createCookie("intlinkurl", "", 0);
	   }
   });

});
