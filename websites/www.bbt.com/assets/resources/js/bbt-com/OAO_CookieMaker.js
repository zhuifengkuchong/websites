function getQuerystring(key, default_)
{
  if (default_==null) default_=""; 
  key = key.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regex = new RegExp("[\\?&]"+key+"=([^&#]*)");
  var qs = regex.exec(window.location.href);
  if(qs == null)
    return default_;
  else
    return qs[1];
}

function createCookie(c_name,value,exsecs)
{
  var exdate=new Date();
  exdate.setTime(exdate.getTime() + (exsecs*1000));
  var c_value=escape(value) + ((exsecs==null) ? "" : "; expires=" + exdate.toUTCString()) + ";domain=.bbt.com; path=/";
  document.cookie=c_name + "=" + c_value;
}

function setCookie(c_name,value,exdays)
{
var exdate=new Date();
exdate.setDate(exdate.getDate() + exdays);
var c_value=escape(value) + ((exdays==null) ? "" : "; expires=" + exdate.toUTCString()) + ";domain=.bbt.com; path=/";
//var c_value=escape(value) + ((exdays==null) ? "" : "; expires=" + exdate.toUTCString()) + "; path=/; domain=.bbt.com";
//alert("Cookie Name: " + c_name + "\n\n" + "Cookie Value: " + c_value);
document.cookie=c_name + "=" + c_value;
}

function getCookie(c_name)
{
var i,x,y,ARRcookies=document.cookie.split(";");
for (i=0;i<ARRcookies.length;i++)
{
  x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
  y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
  x=x.replace(/^\s+|\s+$/g,"");
  if (x==c_name)
    {
    return unescape(y);
    }
  }
}


// Create RSVP cookie.
// If a value for the param "RSVP" exists in the querystring AND an RSVP cookie for this page does not yet exist, set the cookie.
if  ( (getQuerystring('RSVP') != '') && (getCookie("RSVP") == null) ) {
	setCookie("RSVP", getQuerystring('RSVP') , 30);
}

//  Create company ID cookie.
// If a value for the param "CoID" exists in the querystring AND a CoID cookie for this page does not yet exist, set the cookie.
if  ( (getQuerystring('CoID') != '') && (getCookie("CoID") == null) ) {
	setCookie("CoID", getQuerystring('CoID') , 30);
}

// Create product ID cookie.
// If a value for the param "ProCD" exists in the querystring AND a ProCD cookie for this page does not yet exist, set the cookie.
if  ( (getQuerystring('ProCD') != '') && (getCookie("ProCD") == null) ) {
	setCookie("ProCD", getQuerystring('ProCD') , 30);
}

// Create cid cookie.
// If a value for the param "cid" exists in the querystring AND a cid cookie for this page does not yet exist, set the cookie.
if ( (getQuerystring('cid') != '') && (getCookie("cid") == null) ) {
	setCookie("cid", getQuerystring('cid') , 30);
}

//  Create product type cookie i.e. checking or savings.
// If a value for the param "prodtype" exists in the querystring AND a prodtype cookie for this page does not yet exist, set the cookie.
if ( (getQuerystring('prodtype') != '') && (getCookie("prodtype") == null) ) {
	setCookie("prodtype", getQuerystring('prodtype') , 30);
}

// Create product type cookie i.e. checking or savings.
// If a value for the param "prodcd" exists in the querystring AND a prodcd cookie for this page does not yet exist, set the cookie.
if ( (getQuerystring('prodcd') != '') && (getCookie("prodcd") == null) ) {
	setCookie("prodcd", getQuerystring('prodcd') , 30);
}

// Create referral source cookie i.e. google, msn, yahoo, bbtcom, directlink.
// If a value for the param "ReferralSource" exists in the querystring, set the cookie.
if (getQuerystring('ReferralSource') != '') {
	setCookie("ReferralSource", getQuerystring('ReferralSource') , 30);
}

// Create campaign major cookie which is part of the promo code.
// If a value for the param "CampIDMaj" exists in the querystring, set the cookie.
if (getQuerystring('CampIDMaj') != '') {
	setCookie("CampIDMaj", getQuerystring('CampIDMaj') , 30);
}

// Create campaign minor cookie which is part of the promo code.
// If a value for the param "CampIDMin" exists in the querystring, set the cookie.
if (getQuerystring('CampIDMin') != '') {
	setCookie("CampIDMin", getQuerystring('CampIDMin') , 30);
}