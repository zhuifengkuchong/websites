(function($) {
    $.fn.fixZIndex = function(params) {
        params = params || {};
        if (params.msieOnly && !$.browser.msie) return this;
        var num_of_jobj = this.length;
        for (var i = num_of_jobj; i--;) {
            var curr_element = this[i];
            var config_recursive = params.recursive || true;
            var config_exclude = params.exclude || null;
            while (curr_element != document.body) {
                if (!$(curr_element).hasClass(config_exclude) && ($(curr_element).css('position') == 'relative' || $(curr_element).css('position') == 'absolute')) {
                    if ($.data(curr_element, 'zIndex') == undefined) {
                        $.data(curr_element, 'zIndex', curr_element.style.zIndex || '-1');
                    }
                    curr_element.style.zIndex = params.zIndex || '9999';
                }
                curr_element = curr_element.parentNode;
                if (!config_recursive) break;
            }
        }
        return this;
    };

    // optional function to restore z-index if needed
    $.fn.restoreZIndex = function(params) {
        params = params || {};
        if (params.msieOnly && !$.browser.msie) return this;
        var num_of_jobj = this.length;
        for (var i = num_of_jobj; i--;) {
            var curr_element = this[i];
            var config_exclude = params.exclude || null;
            while (curr_element != document.body) {
                var currZIndex = $.data(curr_element, 'zIndex');
                if (currZIndex > -1 && !$(curr_element).hasClass(config_exclude)) {
                    curr_element.style.zIndex = currZIndex;
                    $.removeData(curr_element, 'zIndex');
                }
                else if (currZIndex == -1) {
                    curr_element.style.zIndex = '';
                }
                curr_element = curr_element.parentNode;
            }
        }
        return this;
    };
})(jQuery);

jQuery(document).ready(function() {
	/** START: Changes for JIRA PROD12-8602 **/
	
	jQuery('.menu_container').after("<div id='blockingDiv' style='width: 279px; height: 40px; position: absolute; left: 0px; top: 0px; display:none; opacity:0.5; filter: alpha(opacity=50); background:none repeat scroll 0 0 #FFFFFF'></div>");

	/** END: Changes for JIRA PROD12-8602 **/
	jQuery('ul.usm_menu').css('visibility','visible').hide();
	
	if(jQuery('.usm_menu').height() > 500){
		jQuery('.usm_menu > li:not(.linkaccount)').wrapAll('<div class="usm_scroll" />');
		jQuery('.usm_scroll').css('height', jQuery(window).height()-342 +'px');
	}
	
	if(jQuery.browser.msie && jQuery.browser.version < 9){
		jQuery('ul>li>a.current').fixZIndex({recursive: true, msieOnly: true, zIndex: 6, exclude: '#wrapper'});
		jQuery('ul.usm_menu').fixZIndex({recursive: true, msieOnly: true, zIndex: 5, exclude: '#wrapper'});
	}
	
	var usmOpened=false;
	
	if(jQuery('.usm_menu .selected') == true){
		jQuery('.menu_container div:first-child').next().css('backgroundPosition','100% -40px');
	}
	
	jQuery('.menu_container div').focus(function () {
		jQuery('ul.usm_menu').slideDown(400);
	});
	
	jQuery('.menu_container div').click(function(){
		if (usmOpened==false){
			jQuery('ul.usm_menu').slideDown(500);
			usmOpened=true;
		} else {
			jQuery('ul.usm_menu').slideUp(500);
			usmOpened=false;
		}
		return false;
	});
	
	/* CATO fixes for Keyboard Navigation - START */
	jQuery('.usm_menu a:last').keydown(function(e){
		var code = e.keyCode || e.which;
		if ((!e.shiftKey) && code == '9') {		
		jQuery('ul.usm_menu').slideUp(400);
		usmOpened=false;		
		}
	});
	
	jQuery('.usm_menu a:first').keydown(function(e){
		var code = e.keyCode || e.which;
		if ((e.shiftKey) && code == '9') {		
		jQuery('ul.usm_menu').slideUp(400);
		usmOpened=false;		
		}
	});
	/* CATO fixes for Keyboard Navigation - END */	
	
	jQuery('#usmModule').mouseleave(function(){
		jQuery('ul.usm_menu').slideUp(400);
		usmOpened=false;	
	});
	
	jQuery('.usm_menu li.usmTitle > a').click(function () {
		return false;
	});
	
	jQuery('.usm_menu li:not(.usmTitle, .linkaccount) > a').click(function () {
		/** START: Changes for JIRA PROD12-8602 **/
		
		jQuery('#blockingDiv').show();
		var timeOut = setTimeout(function(){
			window.location = window.location.href;
			clearTimeout(timeOut);
		}, 3000);
		
		/** END: Changes for JIRA PROD12-8602 **/
		jQuery('.usm_menu li a').removeClass('selected');
		jQuery(this).addClass('selected');
		if(jQuery(this).children().hasClass('noimg')){
			
			if(jQuery('.usm_scroll')){
				jQuery('a.usm_click').html('<img src="' + jQuery(this).parentsUntil('.usm_menu').children('div li a').find('img').attr('src') + '" width="30" height="30" alt="" />' + jQuery(this).html());
			} else {
				jQuery('a.usm_click').html('<img src="' + jQuery(this).parentsUntil('.usm_menu').children('li a').find('img').attr('src') + '" width="30" height="30" alt="" />' + jQuery(this).html());
			}
		} else {
			jQuery('a.usm_click').html(jQuery(this).html());
		}
		jQuery('.menu_container div:first-child').next().css('backgroundPosition','100% -40px');
		jQuery('ul.usm_menu').slideUp(400);
		usmOpened=false;
    	return false;
	});
	
});
