if(typeof globalNavDefaultSelections != "undefined" && typeof globalNavDefaultSelections.urls != "undefined" && typeof globalNavDefaultSelections.urls.urlMap != "undefined"){
	var urlMap = globalNavDefaultSelections.urls.urlMap;
	var footLinks = jQuery(".bottomLinks a").get();
	var segLinks = jQuery("#segMenuBar a").get();
	var auxLinks = jQuery("#globalNavUserInfo a").get();
	var navLinks = jQuery("#tieredNav a").get();

	var globalNavLinks = footLinks.concat(segLinks.concat(auxLinks.concat(navLinks)));

	jQuery.each(globalNavLinks, function(index, link){
		for(url in  urlMap){
			if(link.href.match(urlMap[url])){
				link.href = link.href.replace(urlMap[url],url)
			}
		}
	});
}