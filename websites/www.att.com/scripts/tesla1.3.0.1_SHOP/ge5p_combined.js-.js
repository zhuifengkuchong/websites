/**********************************
 *	US9639 - Next Gen/Single Code base WebService
 *	US7204 - NextGen/Single Code Base Tracer Bullet
 *	File: ge5p_combined.js
 *	Includes functions for site navigation and CATO/accessibility
 *	Created on 8/12/2013 by Robert J. Butler rb205p@att.com
 *	Updated on 3/25/2015 Version 1.3.0.1_SHOP.92 Final Release by Robert J. Butler rb205p@att.com
 *	Consumer Digital IT Solutions
 *	Developer: Robert J. Butler rb205p@att.com
 *	JS Dependent on /scripts/tesla1.3.0/ge5p_3rdparty.js OR
 *	jQuery version 1.5.2+ & jQuery UI 1.8.11 custom JS
 **********************************/
 /* Versions
1.0.0 - Initial Tesla Release
1.1.0 - Combined Tesla zone css files into one css file for performance
1.2.0 - Isolate all css to Tesla global nav elements only
1.2.0_CD - CD = Cross Domain JSONP functionality added for IE8 & IE9 to operate within non att.com domains
1.2.1 - JSONP functionality, Deferred Init call using setTimeout & setInterval
1.2.5 - My Linked Account Tab, Re-factored Cookie, User Auth, User Group conditionals
1.3.0 - Motion Point Project, Isolate Tesla JS with jQuery, MyAT&T Global Var for Change Language URL
1.3.0_SHOP - Tesla Version based on jQuery 1.5.2
1.3.0.1_SHOP - jQuery & Plugin-Free model of Tesla 1.3.0
*/

var GE5P = {
	/*  BINDS  */
	Init: function () {
		//Setup function - used for general jQuery events
		GE5P.setup();
		/* Primary Nav Elements - Primary (.ge5p_z2-primary-nav-el), Secondary (.ge5p_z2-secondary-nav-el), Tertiary (ge5p_z2-tertiary-nav-el)
		 * Primary Elements binds to - Touchstart (tablet), Hover (desktop), Focus (desktop)
		 */
		if(GE5P.ge5p_supportsTouch) {
			if(GE5P.ge5p_UA_isAndroid) {
				jQuery('.ge5p_z1-drop-down').live('click', GE5P.toggleSegUtilNav);
				jQuery('.ge5p_z2-primary-nav-el').live('click', GE5P.primaryNavActionTouch);
				jQuery('.ge5p_z2-secondary-nav-el').live('click', GE5P.secondaryNavActionTouch);
				jQuery('#searchForm input[type=submit').live('click', GE5P.validateSearchForm);
			} else {
				//iOS & others
				jQuery('.ge5p_z1-drop-down').live('touchstart', GE5P.toggleSegUtilNav);
				jQuery('#ge5p_z1-change-language').live('touchstart', GE5P.changeLanguage);
				jQuery('.ge5p_z2-primary-nav-el').live('touchstart', GE5P.primaryNavActionTouch);
				jQuery('.ge5p_z2-secondary-nav-el').live('touchstart', GE5P.secondaryNavActionTouch);
				jQuery('#searchForm input[type=submit').live('touchstart', GE5P.validateSearchForm);
			} 
		} else {
			/* Zone 1 Events */
			jQuery('#ge5p_z1 a').live('keydown', GE5P.keyboardSegUtilNavAction);
			jQuery('#ge5p_z1 a').live('mouseenter', GE5P.showSegUtilNav);
			jQuery('body').bind('click', GE5P.bodyReset);
			jQuery('html, body').live('mousewheel DOMMouseScroll onmousewheel scroll', GE5P.closeSegUtilNav);
			jQuery('.ge5p_z1-drop-down').live('click', GE5P.toggleSegUtilNav);
			/* Change Language */
			jQuery('#ge5p_z1-change-language').live('click', GE5P.changeLanguage);
			/* Zone 2 Primary Nav Functions */
			jQuery('.ge5p_z2-primary-nav-el').live('mouseenter mouseleave focusin keydown', GE5P.primaryNavActionEvent);
			jQuery('#searchForm input[type=submit]').live('click', GE5P.validateSearchForm);
			/* Secondary Nav Functions */
			//jQuery('#ge5p_z2').delegate('.ge5p_z2-secondary-nav-el', 'mouseenter mouseleave focusin keydown', GE5P.secondaryNavActionEvent);
			jQuery('.ge5p_z2-secondary-nav-el').live('mouseenter mouseleave focusin keydown', GE5P.secondaryNavActionEvent);
			jQuery('#ge5p_z2-zipcode-inner a').live('keydown', GE5P.keyboardUserInfo);
		}
		/* Tertiary Nav Functions */
		jQuery('.navTertiaryLink').bind('keydown', GE5P.tertiaryNavActionEvent);
		/* Search Form Keydown */
		jQuery('#ge5p_search').bind('keydown', GE5P.keyboardGNSearchBox);
		/* Skip Navigation Click/Keydown */
		//jQuery('#ge5p_z1-skip-navigation-link').bind('click', GE5P.skipNavigation);
	},
	/*	VAR UNDEFINED SECTION  */
	ge5p_supportsTouch: undefined,
	ge5p_dropDownSetTimeout: undefined,
	ge5p_newWindowText: undefined,
	heightPrimaryNavClosed: undefined,
	heightPrimaryNavTray: undefined,
	heightPrimaryNavOpen: undefined,
	heightPrimaryNavTraySecondaryNavTray: undefined,
	heightPrimaryNavSecondaryNavOpen: undefined,
	heightSecondaryNavTray: undefined,
	heightPrimaryNavBarSecondaryNavTray: undefined,
	heightTertiaryNavOpen: undefined,
	overrideKeys: undefined,
	keyCodePrimaryNav: undefined,
	navServiceJSON: undefined,
	ge5p_userCityVar: undefined,
	ge5p_userStateVar: undefined,
	ge5p_userZipCodeVar: undefined,
	ge5p_GNSESSuserGroups: undefined,
	ge5p_localLanguage: undefined,
	ge5p_changeLanguageURLVar: undefined,
	ge5p_myattChangeLanguageURLVar: undefined,
	ge5p_linkLogOutURL: undefined,
	ge5p_requestURL: undefined,
	ge5p_primary_nav: undefined,
	ge5p_is_user_authenticated: undefined,
	ge5p_is_user_authenticated_GNSESS: undefined,
	ge5p_secTrayDivMaxHeight: undefined,
	ge5p_is_msie: undefined,
	ge5p_is_msie8: undefined,
	ge5p_is_msie9: undefined,
	ge5p_is_msie10: undefined,
	ge5p_UA: undefined,
	ge5p_UA_isAndroid: undefined,
	ge5p_isAndroidNativeBrowser: undefined,
	ge5p_UA_isChrome: undefined,
	ge5p_UA_isiPad: undefined,
	ge5p_UA_isLinux: undefined,
	ge5p_userFirstName: undefined,
	ge5p_navSerHost: undefined,
	ge5p_theFirstVisibleIndex: undefined,
	ge5p_globalNavDefaultPrimaryActive: undefined,
	ge5p_globalNavDefaultSecondaryActive: undefined,
	ge5p_globalNavDefaultLocaleActive: undefined,
	ge5p_globalNavDefaultLocaleActiveSet: undefined,
	ge5p_globalNavDefaultLanguageChangeLink: undefined,
	ge5p_websiteurl: undefined,
	ge5p_websiteurlBase: undefined,
	ge5p_websiteurlSourse: undefined,
	ge5p_globalsearchurlBase: undefined,
	ge5p_websiteurlHost: undefined,
	ge5p_websiteurlHostname: undefined,
	ge5p_websiteurlRelative: undefined,
	ge5p_websiteurlFullRelative: undefined,
	ge5p_productionGlobalSearch: undefined,
	ge5p_navServiceUrl: undefined,
	ge5p_showMyLinkedAccountTab: undefined,
	ge5p_motionpoint: undefined,
	ge5p_motionpointProduction: undefined,
	ge5p_environmentProduction: undefined,
	ge5p_environmentHost: undefined,
	ge5p_environmentJSON: undefined,
	ge5p_environmentJSON_Path: undefined,
	ge5p_setTeslaLinksRelative: undefined,
	ge5p_storageAvailable: undefined,
	ge5p_teslaLSLocale: undefined,
	ge5p_teslaLSLocalePresent: undefined,
	ge5p_teslaGNSESSPresent: undefined,
	ge5p_teslaPageLocalePresent: undefined,
	ge5p_teslaCOLAMPresent: undefined,
	ge5p_teslaPDIDPresent: undefined,
	ge5p_pageRepainted: undefined,
	ge5p_tabIndexSkipNav: undefined,
	ge5p_tabIndexZone1: undefined,
	ge5p_tabIndexZone2: undefined,
	ge5p_tabIndexZone3: undefined,
	ge5p_tabIndexZone7: undefined,
	ge5p_currentYear: undefined,
	native_json_support: undefined,
	ge5p_json_stringify: undefined,
	
	/*	SETUP SECTION  */
	//Setup function - used for general jQuery events
	setup: function () {
		// Browser Checking and setting JS Vars
		GE5P.ge5p_UA = navigator.userAgent.toLowerCase();
		// Check for IE Browser
		GE5P.ge5p_is_msie = /msie/.test(GE5P.ge5p_UA);
		// Check for IE8 or IE9 or IE10
		GE5P.ge5p_is_msie8 = /8.0/.test(GE5P.ge5p_UA);
		GE5P.ge5p_is_msie9 = /9.0/.test(GE5P.ge5p_UA);
		GE5P.ge5p_is_msie10 = /10.0/.test(GE5P.ge5p_UA);
		// Check if Agent is Android
		GE5P.ge5p_UA_isAndroid = /android/.test(GE5P.ge5p_UA);
		GE5P.ge5p_UA_isiPad = /ipad/.test(GE5P.ge5p_UA);
		GE5P.ge5p_UA_isLinux = /linux/.test(GE5P.ge5p_UA);
		GE5P.ge5p_UA_isChrome = /chrome/.test(GE5P.ge5p_UA);
		GE5P.ge5p_UA_isSafari = /safari/.test(GE5P.ge5p_UA);
		GE5P.ge5p_isAndroidNativeBrowser = false;
		if(GE5P.ge5p_UA_isAndroid) {
			if(GE5P.ge5p_UA_isLinux && GE5P.ge5p_UA_isChrome && GE5P.ge5p_UA_isSafari) {
				// Android Chrome Browser
				GE5P.ge5p_isAndroidNativeBrowser = false;
			} else {
				GE5P.ge5p_isAndroidNativeBrowser = true;
			}
		}
		// Set New Window Text
		GE5P.ge5p_newWindowText = "This link will open a new window";
		// Native JSON
		GE5P.native_json_support = window.JSON && typeof JSON.stringify === 'function' && JSON.stringify(0) === '0' && !window.Prototype;
		// JSON.Stringify
		GE5P.ge5p_json_stringify = JSON.stringify;
		// Set Dropdown timeout
		GE5P.ge5p_dropDownSetTimeout = 50;
		// Set Page Repainted
		GE5P.ge5p_pageRepainted = false;
		// TO DO REMOVE Local Storage
			//GE5P.ge5p_storageAvailable = jQuery.jStorage.storageAvailable();
		// Current Year
		GE5P.ge5p_currentYear = new Date().getFullYear();
		// Page JS Vars
		if (typeof globalNavDefaultSelections != 'undefined') {
			if (typeof globalNavDefaultSelections.primary == 'undefined') {
				GE5P.ge5p_globalNavDefaultPrimaryActive = '';
			} else {
				GE5P.ge5p_globalNavDefaultPrimaryActive = globalNavDefaultSelections.primary;
			}
			if (typeof globalNavDefaultSelections.secondary == 'undefined') {
				GE5P.ge5p_globalNavDefaultSecondaryActive = '';
			} else {
				GE5P.ge5p_globalNavDefaultSecondaryActive = globalNavDefaultSelections.secondary;
			}
			if (typeof globalNavDefaultSelections.locale == 'undefined') {
				GE5P.ge5p_globalNavDefaultLocaleActive = '';
			} else {
				GE5P.ge5p_globalNavDefaultLocaleActive = globalNavDefaultSelections.locale;
			}
			if (typeof globalNavDefaultSelections.languageChangeLink == 'undefined') {
				GE5P.ge5p_globalNavDefaultLanguageChangeLink = true;
			} else {
				GE5P.ge5p_globalNavDefaultLanguageChangeLink = globalNavDefaultSelections.languageChangeLink;
			}
			if (typeof globalNavDefaultSelections.ge5p_changeLanguageURL == 'undefined') {
                GE5P.ge5p_changeLanguageURLVar = '';
			} else {
				GE5P.ge5p_changeLanguageURLVar = globalNavDefaultSelections.ge5p_changeLanguageURL;
			}
			if (typeof globalNavDefaultSelections.ge5p_myattChangeLanguageURL == 'undefined') {
                GE5P.ge5p_myattChangeLanguageURLVar = '';
			} else {
				GE5P.ge5p_myattChangeLanguageURLVar = globalNavDefaultSelections.ge5p_myattChangeLanguageURL;
			}
			if (typeof globalNavDefaultSelections.ge5p_productionGlobalSearch == 'undefined') {
                GE5P.ge5p_globalNavDefaultProductionGlobalSearch = true;
			} else {
				GE5P.ge5p_globalNavDefaultProductionGlobalSearch = globalNavDefaultSelections.ge5p_productionGlobalSearch;
			}
			if (typeof globalNavDefaultSelections.ge5p_showMyLinkedAccountTab == 'undefined' || globalNavDefaultSelections.ge5p_showMyLinkedAccountTab == false) {
                GE5P.ge5p_globalNavDefaultMyLinkedTabSet = false;
			} else {
				GE5P.ge5p_globalNavDefaultMyLinkedTabSet = globalNavDefaultSelections.ge5p_showMyLinkedAccountTab;
			}
			if (typeof globalNavDefaultSelections.ge5p_motionpoint == 'undefined') {
                GE5P.ge5p_motionpoint = false;
			} else {
				GE5P.ge5p_motionpoint = globalNavDefaultSelections.ge5p_motionpoint;
			}
			if (typeof globalNavDefaultSelections.ge5p_motionpointProduction == 'undefined') {
                GE5P.ge5p_motionpointProduction = false;
			} else {
				GE5P.ge5p_motionpointProduction = globalNavDefaultSelections.ge5p_motionpointProduction;
			}
			if (typeof globalNavDefaultSelections.ge5p_environmentJSON == 'undefined') {
                GE5P.ge5p_environmentJSON = false;
			} else {
				GE5P.ge5p_environmentJSON = globalNavDefaultSelections.ge5p_environmentJSON;
			}
			if (typeof globalNavDefaultSelections.ge5p_environmentJSON_Path == 'undefined') {
                GE5P.ge5p_environmentJSON_Path = '';
			} else {
				GE5P.ge5p_environmentJSON_Path = globalNavDefaultSelections.ge5p_environmentJSON_Path;
			}
			if (typeof globalNavDefaultSelections.ge5p_environmentProduction == 'undefined') {
                GE5P.ge5p_environmentProduction = true;
			} else {
				GE5P.ge5p_environmentProduction = globalNavDefaultSelections.ge5p_environmentProduction;
			}
			if (typeof globalNavDefaultSelections.ge5p_environmentHost == 'undefined' || globalNavDefaultSelections.ge5p_environmentHost == '') {
                GE5P.ge5p_environmentHost = '';
			} else {
				GE5P.ge5p_environmentHost = globalNavDefaultSelections.ge5p_environmentHost;
			}
			if (typeof globalNavDefaultSelections.ge5p_setTeslaLinksRelative == 'undefined') {
                GE5P.ge5p_setTeslaLinksRelative = false;
			} else {
				GE5P.ge5p_setTeslaLinksRelative = globalNavDefaultSelections.ge5p_setTeslaLinksRelative;
			}
		} else {
			// Set Global Variables if they were not set by the Page
			GE5P.ge5p_globalNavDefaultPrimaryActive = '';
			GE5P.ge5p_globalNavDefaultSecondaryActive = '';
			GE5P.ge5p_globalNavDefaultLocaleActive = '';
			GE5P.ge5p_globalNavDefaultLanguageChangeLink = true;
			GE5P.ge5p_changeLanguageURLVar = '';
			GE5P.ge5p_myattChangeLanguageURLVar = '';
			GE5P.ge5p_globalNavDefaultProductionGlobalSearch = true;
			GE5P.ge5p_globalNavDefaultMyLinkedTabSet = false;
			GE5P.ge5p_motionpoint = false;
			GE5P.ge5p_environmentProduction = true;
			GE5P.ge5p_environmentHost = '';
			GE5P.ge5p_environmentJSON = false;
			GE5P.ge5p_environmentJSON_Path = '';
			GE5P.ge5p_setTeslaLinksRelative = false;
		}
		// Website URL
		GE5P.ge5p_websiteurl = GE5P.getUrlSource();
		GE5P.ge5p_websiteurlBase = GE5P.getUrlBase();
		GE5P.ge5p_websiteurlSource = GE5P.getUrlSource();
		GE5P.ge5p_websiteurlHost = GE5P.getUrlHost();
		GE5P.ge5p_websiteurlHostname = GE5P.getUrlHostname();
		GE5P.ge5p_websiteurlRelative = GE5P.getUrlRelative();
		GE5P.ge5p_websiteurlFullRelative = GE5P.ge5p_websiteurlHost + GE5P.ge5p_websiteurlRelative;
		/* Force Language Change URL to Motion Point url */
		// Set global search url (Either Production or Development)
		if(GE5P.ge5p_globalNavDefaultProductionGlobalSearch) {
			// True, so set global search url to production url
			GE5P.ge5p_globalsearchurlBase = "../../index.htm"/*tpa=http://www.att.com/*/;
		} else {
			GE5P.ge5p_globalsearchurlBase = GE5P.ge5p_websiteurlBase;
		}
		jQuery('nav').mouseleave(function(event) {
            $this = jQuery(event.target);
			// Check if Language DD is open, if so leave it open
			if(jQuery('.ge5p_z1-language-drop-down > div.ge5p_z1-menu').is(':visible')) {
			} else {
				GE5P.closeSegUtilNav();
				jQuery('.ge5p_z2-nav-item, .ge5p_z2-nav-bar-subnav li').removeClass('active');
				jQuery('.ge5p_z2-nav-bar-subnav li').find('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').removeClass('active').hide();
				GE5P.closePrimaryNavTrays();
			}
			// Check if there are default Primary & Secondary Default elements - then show
			if(GE5P.ge5p_globalNavDefaultPrimaryActive.length > 0) {
				// Rename the Secondary Nav Link to show Highlighting
				GE5P.secondaryNavHighlightAdd();
				// Show the active Primary nav element
				$thisPrimaryActive = jQuery('.ge5p_z2-primary-nav-el.ge5p_z2-default-primary-active');
				$thisPrimaryActive.parent().addClass('active');
				$thisPrimaryActive.next().show();
				$thisPrimaryActive.attr('aria-expanded','true').attr('aria-selected','true');
				// Tray 1 height is Primary Nav Height + Primary Nav li.active
				var heightTray1Open = GE5P.heightPrimaryNavClosed + jQuery('#ge5p_z2-nav-bar li.active').height();
				// Increase height of PrimaryNav to show tray
				GE5P.animatePrimaryTray(heightTray1Open);
			}
		});
		// LOCALE Variable Conditionals (True or False)
		// TEMP REMOVED FROM LOGIC ->1. Local Storage 
		// 2. GNSESS COOKIE (Reordered logic - moved GNSESS up)
		// 3. Page
		// 4. COLAM (colam_ctn)
		/**** LOCAL STORAGE ****/
		// TO DO REVOVE Local Storage
		/*
		if(GE5P.ge5p_storageAvailable) {
			// Local Storage Available so check for MP LS var
			// TESLALSLOCALE=en OR TESLALSLOCALE=es
			GE5P.ge5p_teslaLSLocale = jQuery.jStorage.get('TESLALSLOCALE');
			// If TESLALSLOCALE is found & app is in scope
			if(GE5P.ge5p_teslaLSLocale != null && GE5P.ge5p_motionpoint) {
				GE5P.ge5p_teslaLSLocalePresent = true;
			} else {
				//GE5P.ge5p_teslaLSLocale = 'en';
				GE5P.ge5p_teslaLSLocalePresent = false;
			}
		} else {
			GE5P.ge5p_teslaLSLocalePresent = false;
			//GE5P.ge5p_teslaLSLocale = 'en';
		}
		*/
		/**** GNSESS ****/
		if(GE5P.readCookie("GNSESS")) {
			GE5P.ge5p_teslaGNSESSPresent = true;
		} else {
			GE5P.ge5p_teslaGNSESSPresent = false;
		}
		/**** Page ****/
		// Check if Page has set Locale variable
		if(GE5P.ge5p_globalNavDefaultLocaleActive.length > 0) {
			GE5P.ge5p_globalNavDefaultLocaleActiveSet = true;
			GE5P.ge5p_teslaPageLocalePresent = true;
		} else {
			GE5P.ge5p_globalNavDefaultLocaleActiveSet = false;
			GE5P.ge5p_teslaPageLocalePresent = false;
		}
		/**** COLAM_CTN ****/
		if(GE5P.readCookie("colam_ctn")) {
			GE5P.ge5p_teslaCOLAMPresent = true;
		} else {
			GE5P.ge5p_teslaCOLAMPresent = false;
		}
		/**** PD-ID ****/
		if(GE5P.readCookie("PD-ID")) {
			GE5P.ge5p_teslaPDIDPresent = true;
		} else {
			GE5P.ge5p_teslaPDIDPresent = false;
		}
		// Checking for Locale variable set in the following order: Local Storage, Page, GNSESS, colam
		// TEMP REMOVED FROM LOGIC ->1. Local Storage 
		// 2. Page
		// 3. PageGNSESS COOKIE (Reordered logic - moved GNSESS up)
		// 4. COLAM (colam_ctn)
		// 5. Default	
		/*
		if(GE5P.ge5p_teslaLSLocalePresent) {
			 // LS - TESLALSLOCALE=en OR TESLALSLOCALE=es
			 if(GE5P.ge5p_teslaLSLocale =='en') {
				 GE5P.ge5p_localLanguage = "en_US";
			 } else {
				 GE5P.ge5p_localLanguage = "es_US";
			 }
        }
		*/
		if(GE5P.ge5p_teslaPageLocalePresent) {
        	 // Page
        	 GE5P.ge5p_localLanguage = GE5P.ge5p_globalNavDefaultLocaleActive;
		} else if(GE5P.ge5p_teslaGNSESSPresent) {
        	// GNSESS
        	//Set jquery cookie options
			try {
				var ge5p_GNSESSvalueCookie = GE5P.readCookie('GNSESS');
				ge5p_GNSESSvalueCookie = ge5p_GNSESSvalueCookie.replace(/\\/g, "");
				if(typeof ge5p_GNSESSvalueCookie == "object"){
 					ge5p_GNSESSvalueCookie = GE5P.safeToJSON(ge5p_GNSESSvalueCookie);
 				}
 				// Turn cookie string into a json object
				var jdata = jQuery.parseJSON(ge5p_GNSESSvalueCookie);

				// Override cookie with page locale var
 				if(!GE5P.ge5p_globalNavDefaultLocaleActiveSet) {
 					GE5P.ge5p_localLanguage = jdata.LOCALE;
 				}			
			} catch(err) {
				console.log('The Global Navigation Script searching for Locale encountered a malformed GNSESS cookie. Error:'+err); 
				// The GNSESS is malformed so set LOCALE to Default
				GE5P.ge5p_localLanguage = "en_US";
 			}
			
        } else if(GE5P.ge5p_teslaCOLAMPresent) {
        	var ge5p_colam_cookie = GE5P.readCookie("colam_ctn");
        	// Turn cookie string into a json object
        	if(typeof ge5p_colam_cookie == "object"){
        		ge5p_colam_cookie2 = GE5P.safeToJSON(ge5p_colam_cookie);
        	}
        	ge5p_colam_cookie2 = unescape(ge5p_colam_cookie);
        	ge5pZ2_valueCookieArr = ge5p_colam_cookie2.split(';');
        	// Override colam_ctn cookie with page locale var if available
        	if(!GE5P.ge5p_globalNavDefaultLocaleActiveSet) {
        		jQuery.each(ge5pZ2_valueCookieArr, function (key, value) {
        			if(value.indexOf('l=') != -1) {
						// Found language var and value
						GE5P.ge5p_localLanguage = jQuery.trim(value.split('=')[1]);
					}
				});
        	 }
        } else {
        	// Default
			GE5P.ge5p_localLanguage = 'en_US';
        }
		/* Final check for local language
		*  Check address url for "es-us"
		*  Set the local language appropriately
		*/
		if(GE5P.ge5p_websiteurlSource.indexOf('es-us') != -1) {
			// User is on a Spanish language page
			// Check if Cookie is set to English
			if(GE5P.ge5p_localLanguage == "en_US") {
				// Change to Spanish
				GE5P.ge5p_localLanguage = 'es_US';
			}
		}
		//Set TESLA Local Storage Var with LOCALE INFO
		// TEMP REMOVED FROM LOGIC ->1. Local Storage 
		/*
		if(GE5P.ge5p_storageAvailable) {
			GE5P.teslaLocalStorage(GE5P.ge5p_localLanguage);
		}
		*/
		// Set User Group
		// Initially set UG
		GE5P.ge5p_GNSESSuserGroups = '';
		if(GE5P.ge5p_teslaGNSESSPresent) {
        	// GNSESS
        	//Set jquery cookie options
        	//jQuery.cookie.json = true;
        	//jQuery.cookie.raw = true;
        	try {
        		var ge5p_GNSESSvalueCookie = GE5P.readCookie('GNSESS');
				ge5p_GNSESSvalueCookie = ge5p_GNSESSvalueCookie.replace(/\\/g, "");
 				if(typeof ge5p_GNSESSvalueCookie == "object"){
 					ge5p_GNSESSvalueCookie = GE5P.safeToJSON(ge5p_GNSESSvalueCookie);
 				}
 				// Turn cookie string into a json object
				var jdata = jQuery.parseJSON(ge5p_GNSESSvalueCookie);
		
				if(jdata.UG) {
 					if(jdata.UG.length > 0) {
 						var ug_groups = '';
 						jQuery.each(jdata.UG, function(key, value) {
 							// Check if user is authenticated
 							if(value == 'Auth') {
 								GE5P.ge5p_is_user_authenticated_GNSESS = true;
 							}
 							ug_groups += value+'|';
 						});
 						GE5P.ge5p_GNSESSuserGroups = '&userGroups='+ug_groups;
 					} else {
 						GE5P.ge5p_GNSESSuserGroups = '';
 					}
 				} else {
 					GE5P.ge5p_GNSESSuserGroups = '';
 				}
 				if(jdata.FN) {
 					if(jdata.FN.length > 0) {
 						GE5P.ge5p_userFirstName = jdata.FN;
 					}
 				}
			} catch(err) {
				console.log('The Global Navigation Script searching for User Group encountered a malformed GNSESS cookie. Error:'+err); 
				// The GNSESS is malformed so set LOCALE to Default
				GE5P.ge5p_GNSESSuserGroups = '';
 			}
		}
		// Set User Authentication
		// Initial Set Auth values
		GE5P.ge5p_is_user_authenticated = false; 
		if(GE5P.ge5p_teslaPDIDPresent) {
			GE5P.ge5p_is_user_authenticated = true; 
		} else if(GE5P.ge5p_is_user_authenticated_GNSESS) {
			GE5P.ge5p_is_user_authenticated = true; 
		}		 
		if (typeof ge5p_userCity == 'undefined') {
		GE5P.ge5p_userCityVar = '';
		} else {
			if(ge5p_userCity.indexOf("$")>=0){
			GE5P.ge5p_userCityVar = '';
			} else {
				GE5P.ge5p_userCityVar = ge5p_userCity;
			}
		}
		if (typeof ge5p_userState == 'undefined') {
			GE5P.ge5p_userStateVar = '';
		} else {
			if(ge5p_userState.indexOf("$")>=0){
				GE5P.ge5p_userStateVar = '';
			} else {
				GE5P.ge5p_userStateVar = ge5p_userState;
			}
		}
		if (typeof ge5p_userZip == 'undefined') {
			GE5P.ge5p_userZipCodeVar = '';
		} else {
			if(ge5p_userZip.indexOf("$")>=0){
				GE5P.ge5p_userZipCodeVar = '';
			} else {
				GE5P.ge5p_userZipCodeVar = ge5p_userZip;
			}
		}
		if (typeof ge5p_linkLogOutURL == 'undefined') {
			GE5P.ge5p_linkLogOutURLVar = '';
		} else {
			GE5P.ge5p_linkLogOutURLVar = ge5p_linkLogOutURL;
		}
		if (typeof ge5p_requestURL == 'undefined') {
			GE5P.ge5p_requestURLVar = window.location.href;
		} else {
			if(ge5p_requestURL == '') {
				GE5P.ge5p_requestURLVar = window.location.href;
			} else {
				GE5P.ge5p_requestURLVar = ge5p_requestURL;
			}
		}
		if (typeof ge5p_changeLocale == 'undefined') {
			GE5P.ge5p_changeLocaleVar = '';
		} else {
			GE5P.ge5p_changeLocaleVar = ge5p_changeLocale;
		}
		if (typeof ge5p_requestURI == 'undefined') {
			GE5P.ge5p_requestURIVar = '';
		} else {
			GE5P.ge5p_requestURIVar = ge5p_requestURI;
		}
		if (typeof ge5p_queryString == 'undefined') {
			GE5P.ge5p_queryStringVar = '';
		} else {
			GE5P.ge5p_queryStringVar = ge5p_queryString;
		}
		if(GE5P.ge5p_localLanguage) {
			if (GE5P.ge5p_localLanguage.length > 0) {
				// There is a Locale cookie value
				if (GE5P.ge5p_localLanguage == GE5P.ge5p_changeLocaleVar) {
					if (GE5P.ge5p_changeLanguageURLVar != (GE5P.ge5p_requestURIVar+'?'+GE5P.ge5p_queryStringVar)) {
						window.location.replace(GE5P.ge5p_changeLanguageURLVar);
					}
				}
			}
		}
		// Last check for nav service vars to make sure it fires
		if (typeof GE5P.ge5p_GNSESSuserGroups == 'undefined') {
			GE5P.ge5p_GNSESSuserGroups = '';
		}
		/***************************************************************
		 * 
		 * NAV SERVICE VARIABLES
		 * 
		 ***************************************************************/
		// Check if global variable is set for Production vs Local Environment
		if(GE5P.ge5p_environmentProduction) {
			// Production
			GE5P.ge5p_navSerHost = '//www.att.com/';
			// Check to see if base url has trailing slash, if not add it
			GE5P.ge5p_websiteurlBase = GE5P.ge5p_websiteurlBase.replace(/\/?$/, '/');
		} else {
			// Development
			// Set Local Host if Page passes specific host url
			if(GE5P.ge5p_environmentHost.length > 0) {
				GE5P.ge5p_websiteurlBase = GE5P.ge5p_environmentHost;
			}
			// Check to see if base url has trailing slash, if not add it
			GE5P.ge5p_websiteurlBase = GE5P.ge5p_websiteurlBase.replace(/\/?$/, '/');
			// Set nav service host
			GE5P.ge5p_navSerHost = GE5P.ge5p_websiteurlBase;
		}
		if (!GE5P.ge5p_environmentProduction && GE5P.ge5p_environmentJSON){
			
			// Development/Local
			GE5P.ge5p_navServiceUrl = GE5P.ge5p_websiteurlBase+GE5P.ge5p_environmentJSON_Path;
            
            jQuery.ajax({
                type : "GET",
                dataType : "jsonp",
                jsonp: "jsonp",
                jsonpCallback: "gnjsonp",
                url : GE5P.ge5p_navServiceUrl, // ?callback=?
                success: function(data){
                    GE5P.nextGenGlobalNavSuccess(data);
                },
                error: function(jqXHR, textStatus, errorThrown ) {
                    var err = textStatus + ', ' + error;
                    console.log( "Global Nav Service Request Failed: " + textStatus + " "+err);
                }
            });
			
		} else {
			
			var localeLanguageNavService = "";
			// If Application is in scope for Motion Point, send English as parameter and 
			// let Motion Point translate nav/footer content
			if(GE5P.ge5p_motionpoint) {
				// Before we set the Local Language to EN for the purpose of the nav service content
				localeLanguageNavService = 'en_US';	
			} else {
				localeLanguageNavService = GE5P.ge5p_localLanguage;
			}
			/* Navigation Service Z1, Z2, Z7 JSON */
			GE5P.ge5p_navServiceUrl = GE5P.ge5p_navSerHost+'navservice/navservlet?locale='+localeLanguageNavService+'&isNextGen=Y'+GE5P.ge5p_GNSESSuserGroups+'&jsonp=gnjsonp';
	        
            jQuery.ajax({
                type : "GET",
                dataType : "jsonp",
                jsonp: "jsonp",
                jsonpCallback: "gnjsonp",
                url : GE5P.ge5p_navServiceUrl, // ?callback=?
                success: function(data){
                    GE5P.nextGenGlobalNavSuccess(data);
                },
                error: function(jqXHR, textStatus, errorThrown ) {
                    var err = textStatus + ', ' + error;
                    console.log( "Global Nav Service Request Failed: " + textStatus + " "+err);
                }
            });
			
		}
		/* Function to check if window supports
		 * touch devices
		**/
		GE5P.ge5p_supportsTouch = false;
		if(GE5P.ge5p_UA_isiPad || GE5P.ge5p_UA_isAndroid || 'ontouchstart' in window || navigator.msMaxTouchPoints) {
			GE5P.ge5p_supportsTouch = true;
		}
		/* Function to add console.log to non supported browsers
		 * On Hold for testing
		if ( typeof(console) === "undefined" ) {
			console = {};
			console.log = function() {};
		}
		**/
		/*******************************************
		 *          ZONE 2 SETUP FUNCTIONS
		 *******************************************/

		/********** END ZONE 2 SETUP ********/

		/* Function to add ipad class
		**/
		jQuery.extend(jQuery.browser,{SafariMobile : navigator.userAgent.toLowerCase().match(/iP(hone|ad)/i)});
		if(!!jQuery.browser.SafariMobile){jQuery("body").addClass("ipad")}
		
		/*******************************************
		 *          ZONE TABINDEX
		 *******************************************/
		GE5P.ge5p_tabIndexSkipNav = '1';
		GE5P.ge5p_tabIndexZone1 = '0';
		GE5P.ge5p_tabIndexZone2 = '0';
		GE5P.ge5p_tabIndexZone3 = '0';
		GE5P.ge5p_tabIndexZone7 = '0';
		
		
	}, // End of Setup
	/*****************************************************************
	 *
	 * ATT Global Experience Next Gen Functions
	 *
	 *****************************************************************/
	 /* Function to get json response from nav service
	 **/
	nextGenGlobalNavSuccess: function(data) {
			// Success in returning Nav service response
			/* Function for reporting - NOTE the new ge5p pre-pended CLASSES & IDs
			**/
			var reporting_ready = window.reporting_ready || new jQuery.Deferred();
			jQuery.when(reporting_ready).then(function (reporting) {
				reporting.capture([
					{selector: '#ge5p_z1-global-nav-container a', type: 'wtparam', name: 'wtLinkName'},
					{selector: '#ge5p_z1-global-nav-container a', type: 'wtparam', name: 'wtLinkLoc', value: 'GLBN_GM'},
					{selector: '#ge5p_z1-global-nav-container a', type: 'wtlink', name: '', params: 'wtLinkName,wtLinkLoc', trigger: 'mousedown'},
					{selector: '#ge5p_z2-primary-nav a', type: 'wtparam', name: 'wtLinkName'},
					{selector: '#ge5p_z2-primary-nav a', type: 'wtparam', name: 'wtLinkLoc', value: 'GLBN'},
					{selector: '#ge5p_z2-primary-nav a', type: 'wtlink', name: '', params: 'wtLinkName,wtLinkLoc', trigger: 'mousedown'},
					{selector: '#ge5p_z2-user-info a', type: 'wtparam', name: 'wtLinkName'},
					{selector: '#ge5p_z2-user-info a', type: 'wtparam', name: 'wtLinkLoc', value: 'GLBN_AN'},
					{selector: '#ge5p_z2-user-info a', type: 'wtlink', name: '', params: 'wtLinkName,wtLinkLoc', trigger: 'mousedown'},
					{selector: '#ge5p_z7 a', type: 'wtparam', name: 'wtLinkName'},
					{selector: '#ge5p_z7 a', type: 'wtparam', name: 'wtLinkLoc', value: 'GLBN_FTR'},
					{selector: '#ge5p_z7 a', type: 'wtlink', name: '', params: 'wtLinkName,wtLinkLoc', trigger: 'mousedown'},
					{selector: 'form#searchForm', type: 'wtmeta', name: 'wtAutoSuggestInd', deferred: true,
						value: function() {
							return jQuery('#autoSuggestBox:isvisible').length ? 'Y' : 'N';
						}
					}
				]);
			});
			//global autosuggest code
			var client_id = 1;
			function split( val ) {
				return val.split( /,\s*?/ );
			}
			// Append Zone 1 Seg Util menu containers to DOM
			jQuery('#ge5p_z1').html('<div id=\"ge5p_z1-global-nav-container\" class=\"container\"><ul id=\"ge5p_z1-nav-left-seg\" class=\"ge5p_z1-nav-left-seg\" aria-owns=\"\" role=\"tree\"></ul><ul id=\"ge5p_z1-nav-right-seg\" class=\"ge5p_z1-nav-right-seg\"aria-owns=\"\" role=\"tree\"></ul></div>');
			// Append Zone 2 Primary menu containers to DOM
			jQuery('#ge5p_z2').html('<div class=\"row-fluid\"><div id=\"ge5p_z2-user-info\"><div class=\"ge5p_z2-zipcode-outer\"><div id=\"ge5p_z2-zipcode-inner\" class=\"ge5p_z2-zipcode-inner\"><span id=\"ge5p_z2-zipcode-hi\"></span><span><a id=\"ge5p_z2-zipcode-firstname\" tabindex=\"'+GE5P.ge5p_tabIndexZone2+'\"></a></span><span id=\"ge5p_z2-zipcode-welcome\"></span><span id=\"ge5p_z2-zipcode-welcomeback\"></span><span><a id=\"ge5p_z2-zipcode-register\" href=\"#\" tabindex=\"'+GE5P.ge5p_tabIndexZone2+'\" class=\"ge5p_z2-zipcode-register\">Register</a></span><span id=\"ge5p_z2-zipcode-city\"></span><span id=\"ge5p_z2-zipcode-state\"></span><span id=\"ge5p_z2-zipcode-zip\"></span><span><a href=\"#\" class=\"openModal localModal ge5p_z2-zipcode-enter\" id=\"ge5p_z2-zipcode-enter\" tabindex=\"'+GE5P.ge5p_tabIndexZone2+'\"></a></span><span><a id=\"ge5p_z2-zipcode-change\" href=\"#\" class=\"openModal localModal ge5p_z2-zipcode-change\" tabindex=\"'+GE5P.ge5p_tabIndexZone2+'\">Change</a></span></div><a id=\"ge5p_z2-user-auth\" class=\"ge5p_z2-user-auth\" href=\"#\" tabindex=\"'+GE5P.ge5p_tabIndexZone2+'\"><img id=\"ge5p_z2-user-auth-button\" alt=\"\" src=\"../../../0.ecom.attccc.com/media/att/2011/global/nav/Login_button_AA0011EJ.png\"/*tpa=http://0.ecom.attccc.com/media/att/2011/global/nav/Login_button_AA0011EJ.png*/></a></div></div></div><div id=\"ge5p_z2-primary-nav-wrapper\" class=\"row-fluid\"><nav id=\"ge5p_z2-primary-nav\"><ul id=\"ge5p_z2-nav-bar\" aria-owns=\"\" role=\"tree\" aria-label=\"Primary Navigation Bar\"></ul></nav></div>');
			// Append Zone 7 Footer link container to DOM
			jQuery('#ge5p_z7').html('<div id="ge5p_z7-brand-elements" class="ge5p_z7-brand-elements row-fluid"><ul id="ge5p_z7-brand-elements-holder"></ul></div><div class="row-fluid"><hr class="ge5p-fancy-line"></div><div id="ge5p_z7-key-links" class="ge5p_z7-key-links row-fluid"><h2 class="hidden-spoken" id="ge5p_z7-key-links-heading">ATandT Key Links</h2><ul id="ge5p_z7-key-links-holder" aria-describedby="ge5p_z7-key-links-heading" class="text-left"></ul></div><div id="ge5p_z7-copyright-legal" class="ge5p_z7-copyright-legal row-fluid"><div class="text-left"><p><a href="http://www.att.com/gen/privacy-policy?pid=2587">&copy; '+GE5P.ge5p_currentYear+' AT&amp;T Intellectual Property</a>. All rights reserved. AT&amp;T, the AT&amp;T logo and all other AT&amp;T marks contained herein are trademarks of AT&amp;T Intellectual Property and/or AT&amp;T affiliated companies. AT&amp;T 36USC220506</p></div></div><div id="ge5p_z7-partners-affiliates-awards" class="ge5p_z7-partners-affiliates-awards row-fluid"><ul></ul></div>');
			// Primary Nav Element
			GE5P.ge5p_primary_nav = jQuery('#ge5p_z2-primary-nav');
			// Height of Primary Nav all Trays Closed
			GE5P.heightPrimaryNavClosed = jQuery('#ge5p_z2-primary-nav').height();
			// Listen for primary nav clicks - so you will not close the menu if the html is clicked
			jQuery('#ge5p_z2-primary-nav:not(a)').live('touchstart click', function(event){
				event.stopPropagation();
			});
			jQuery.each(data, function (index, element) {
				// Split nav element
				var navZone = element.nav_position.split('_')[1]; // Ex z1
				// Check for Zone 1 Nav
				if(navZone == 'z1') {
					// Returned is the Left or the Right Seg
					// Check what Seg is returned
					if(element.nav_position == 'ge5p_z1_1') {
						// Left Seg Menu returned - "z1_1"
						var ul_aria_owns = "";
						jQuery.each(element.children, function (key, value) {
							// Check if there is a menu or regular link
							if(value.children.length > 0) {
								var li = jQuery('<li class=\"ge5p_z1-tab ge5p_z1-has-submenu\"><a aria-selected=\"false\" aria-expanded=\"false\" aria-setsize="'+value.children.length+'" tabindex="'+GE5P.ge5p_tabIndexZone1+'" role=\"treeitem\" class=\"ge5p_z1-drop-down\" aria-level=\"1\"></a><div class=\"ge5p_z1-menu\"><ul aria-owns=\"\" role=\"group\"></ul></div></li>');
								// Append li to ul seg menu
								jQuery('#ge5p_z1-nav-left-seg').append(li);
								// Anchor Tag Data
								jQuery("a",li).html(value.displayName);
								jQuery("a",li).attr("href", value.url);
								jQuery("a",li).attr("id", value.id);
								jQuery("a",li).attr("aria-label", value.displayName);
								jQuery("ul",li).addClass(value.id);
								var urlID = "ge5p_z1_1_"+key;
								jQuery("ul",li).attr("id",urlID);
								jQuery("a",li).attr("aria-owns",urlID);
								jQuery("#"+urlID).attr("aria-labelledby",value.id); //new
								if(value.hiddenText.length > 0) {
									// There is an hidden text element
									jQuery("a",li).append('<span class=\"hidden-spoken\">'+value.hiddenText+'</span>');
								}
								if(GE5P.ge5p_supportsTouch) {
									// // Touch Device so add hidden-spoken
									jQuery("a",li).append('<span class=\"hidden-spoken\"></span>');
								}
								// Check if this links open in a new window
								if(value.windowLocation == 'Y') {
									// Link opens in new window
									jQuery("a",li).attr("target", "_blank");
									var iconNewWindow = jQuery('<i class=\"cssIcon-open-window-w\"></i><span class=\"hidden-spoken\">'+GE5P.ge5p_newWindowText+'</span>');
									jQuery("a",li).append(iconNewWindow);
								}
								if (ul_aria_owns.length > 0) {
									ul_aria_owns = ul_aria_owns+" "+value.id;
								} else {
									ul_aria_owns = value.id;
								}
								// Menu Data
								var aria_owns = "";
								var aria_posinset = 0;
								jQuery.each(value.children, function(x, subnavlinks) {
									var ul = jQuery('.'+value.id);
									aria_posinset = aria_posinset + 1;
									var li2 = jQuery('<li class=\"ge5p_z1-menuitem\"><a tabindex=\"'+GE5P.ge5p_tabIndexZone1+'\" role=\"treeitem\" aria-label=\"'+subnavlinks.displayName+'\" aria-setsize=\"'+value.children.length+'\" aria-posinset=\"'+aria_posinset+'\" aria-level=\"2\"></a></li>');
									ul.append(li2);
									jQuery("a",li2).html(subnavlinks.displayName);
									jQuery("a",li2).attr("id", subnavlinks.id);
									jQuery("a",li2).attr("href", subnavlinks.url);
									if(subnavlinks.hiddenText.length > 0) {
										// There is an hidden text element
										jQuery("a",li2).append('<span class=\"hidden-spoken\">'+subnavlinks.hiddenText+'</span>');
									}
									if(GE5P.ge5p_supportsTouch) {
										// Touch Device so add hidden-spoken
										jQuery("a",li2).append('<span class=\"hidden-spoken\"></span>');
									}
									if(subnavlinks.windowLocation == 'Y') {
										// Link opens in new window
										jQuery("a",li2).attr("target", "_blank");
										var iconNewWindow = jQuery('<i class=\"cssIcon-open-window-w\"></i><span class=\"hidden-spoken\">'+GE5P.ge5p_newWindowText+'</span>');
										jQuery("a",li2).append(iconNewWindow);
									}
									if (aria_owns.length > 0) {
										aria_owns = aria_owns+" "+subnavlinks.id;
									} else {
										aria_owns = subnavlinks.id;
									}
								});
								jQuery('#'+urlID).attr('aria-owns',aria_owns);
							} else {
								// Regular Link
								var li = jQuery('<li class=\"ge5p_z1-tab\"><a aria-label=\"'+value.displayName+'\" role=\"treeitem\" aria-setsize=\"'+value.children.length+'\" aria-posinset=\"'+key+'\" aria-level=\"1\" tabindex=\"'+GE5P.ge5p_tabIndexZone1+'\"></a></li>');
								// Append li to ul seg menu
								jQuery('#ge5p_z1-nav-left-seg').append(li);
								// Add Display Text to a
								jQuery("a",li).html(value.displayName);
								jQuery("a",li).attr("href", value.url);
								jQuery("a",li).attr("id", value.id);
								if(GE5P.ge5p_supportsTouch) {
									// Touch Device so add hidden-spoken
									jQuery("a",li).append('<span class=\"hidden-spoken\"></span>');
								}
								// Check if this links open in a new window
								if(value.windowLocation == 'Y') {
									// Link opens in new window
									jQuery("a",li).attr("target", "_blank");
									var iconNewWindow = jQuery('<i class=\"cssIcon-open-window-w\"></i><span class=\"hidden-spoken\">'+GE5P.ge5p_newWindowText+'</span>');
									jQuery("a",li).append(iconNewWindow);
								}
								if(value.hiddenText.length > 0) {
									// There is an hidden text element
									jQuery("a",li).append('<span class=\"hidden-spoken\">'+value.hiddenText+'</span>');
								}
								if (ul_aria_owns.length > 0) {
									ul_aria_owns = ul_aria_owns+" "+value.id;
								} else {
									ul_aria_owns = value.id;
								}
							}
						});
						var li_skip = jQuery('<li class=\"ge5p_z1-tab ge5p_z1-skip-navigation\"><a tabindex=\"'+GE5P.ge5p_tabIndexSkipNav+'\" href=\"../../index.htm\"/*tpa=http://www.att.com/*/ id=\"ge5p_z1-skip-navigation-link\" class=\"ge5p_z1-skip-navigation-link\" aria-label=\"Skip Navigation\" role=\"treeitem\" aria-posinset=\"1\" aria-level=\"1\">Skip Navigation</a></li>');
						// Append li to ul seg menu
						jQuery('#ge5p_z1-nav-left-seg').append(li_skip);
						ul_aria_owns = ul_aria_owns+" ge5p_z1-skip-navigation-link";
						jQuery('#ge5p_z1-nav-left-seg').attr('aria-owns',ul_aria_owns);
					}
					if(element.nav_position == 'ge5p_z1_2') {
						// Right Seg Menu returned
						var ul_right_aria_owns = "";
						jQuery.each(element.children, function (key, value) {
							// Check if this is the MyAT&T My Linked Account Link
							if (value.actionType == 'mylinkedaccount') {
								if (GE5P.ge5p_globalNavDefaultMyLinkedTabSet) { 
									var li = jQuery('<li class=\"ge5p_z1-tab ge5p_z1-has-submenu ge5p_z1-mylinked-drop-down\"><a id=\"ge5p_z1-mylinked-link" class=\"ge5p_z1-mylinked-link ge5p_z1-drop-down\" aria-selected=\"false\" aria-expanded=\"false\" aria-setsize="'+value.children.length+'" tabindex=\"0\" role=\"treeitem\" aria-level=\"1\"></a><div class=\"ge5p_z1-menu\"><ul class=\"ge5p_z1-mylinked_submenu\" role=\"group\"></ul></div></li>');
									// Append li to ul seg menu
									jQuery('#ge5p_z1-nav-right-seg').append(li);
									// Anchor Tag Data
									jQuery("a",li).html(value.displayName);
									jQuery("a",li).attr("href", '../../index.htm'/*tpa=http://www.att.com/*/); // was javascript:void(0) Removed due to mix warning in IE
									jQuery("a",li).attr("aria-label", value.displayName);
									jQuery("ul",li).addClass(value.id);
									var urlID = "ge5p_z1_1_"+key;
									jQuery("ul",li).attr("id",urlID);
									jQuery("a",li).attr("aria-owns",urlID);
									// Check if this links open in a new window
									if(value.windowLocation == 'Y') {
										// Link opens in new window
										jQuery("a",li).attr("target", "_blank");
										var iconNewWindow = jQuery('<i class=\"cssIcon-open-window-w\"></i><span class=\"hidden-spoken\">'+GE5P.ge5p_newWindowText+'</span>');
										jQuery("a",li).append(iconNewWindow);
									}
									if(value.hiddenText.length > 0) {
										// There is an hidden text element
										jQuery("a",li).append('<span class=\"hidden-spoken\">'+value.hiddenText+'</span>');
									}
									if(GE5P.ge5p_supportsTouch) {
										// // Touch Device so add hidden-spoken
										jQuery("a",li).append('<span class=\"hidden-spoken\"></span>');
									}
									if (ul_right_aria_owns.length > 0) {
										ul_right_aria_owns = ul_right_aria_owns+" "+value.id;
									} else {
										ul_right_aria_owns = value.id;
									}
									var aria_owns = "";
									var aria_posinset = 0;
									// Menu Data
									//var lastID = value.children.length - 1;
									jQuery.each(value.children, function(x, subnavlinks) {
										var ul = jQuery('.'+value.id);
										aria_posinset = aria_posinset + 1;
										var li2 = jQuery('<li class=\"ge5p_z1-menuitem\"><a tabindex=\"'+GE5P.ge5p_tabIndexZone1+'\" aria-label=\"'+subnavlinks.displayName+'\" role=\"treeitem\" aria-setsize=\"'+value.children.length+'\" aria-posinset=\"'+aria_posinset+'\" aria-level=\"2\"></a></li>');
										ul.append(li2);
										jQuery("a",li2).html(subnavlinks.displayName);
										jQuery("a",li2).attr("id", subnavlinks.id);
										jQuery("a",li2).attr("href", subnavlinks.url);
										if(subnavlinks.hiddenText.length > 0) {
											// There is an hidden text element
											jQuery("a",li2).append('<span class=\"hidden-spoken\">'+subnavlinks.hiddenText+'</span>');
										}
										if(GE5P.ge5p_supportsTouch) {
											// // Touch Device so add hidden-spoken
											jQuery("a",li2).append('<span class=\"hidden-spoken\"></span>');
										}
										if(subnavlinks.windowLocation == 'Y') {
											// Link opens in new window
											jQuery("a",li2).attr("target", "_blank");
											var iconNewWindow = jQuery('<i class=\"cssIcon-open-window-w\"></i><span class=\"hidden-spoken\">'+GE5P.ge5p_newWindowText+'</span>');
											jQuery("a",li2).append(iconNewWindow);
										}
										if (aria_owns.length > 0) {
											aria_owns = aria_owns+" "+subnavlinks.id;
										} else {
											aria_owns = subnavlinks.id;
										}
									});
									jQuery('#'+urlID).attr('aria-owns',aria_owns);
								}
							} else {
								
								// Check if this is the Language Link
								if(value.actionType == 'language' || value.displayName == 'Language' || value.displayName == 'Idioma') {
									var li = jQuery('<li class=\"ge5p_z1-tab ge5p_z1-has-submenu ge5p_z1-language-drop-down\"><a id=\"ge5p_z1-language-drop-down-link" class=\"ge5p_z1-language-drop-down ge5p_z1-drop-down\" aria-selected=\"false\" aria-expanded=\"false\" aria-setsize=\"'+value.children.length+'\" tabindex=\"'+GE5P.ge5p_tabIndexZone1+'\" role=\"treeitem\" aria-level=\"1\"></a><div class=\"ge5p_z1-menu\"><ul class=\"ge5p_z1-language_submenu\" role=\"group\"></ul></div></li>');
									// Append li to ul seg menu
									jQuery('#ge5p_z1-nav-right-seg').append(li);
									// Anchor Tag Data
									jQuery("a",li).html(value.displayName);
									jQuery("a",li).attr("href", value.url); // was javascript:void(0) Removed due to mix warning in IE
									jQuery("a",li).attr("aria-label", value.displayName);
									jQuery("ul",li).addClass(value.id);
									var urlID = "ge5p_z1_1_"+key;
									jQuery("ul",li).attr("id",urlID);
									jQuery("a",li).attr("aria-owns",urlID);
									// Check if this links open in a new window
									if(value.windowLocation == 'Y') {
										// Link opens in new window
										jQuery("a",li).attr("target", "_blank");
										var iconNewWindow = jQuery('<i class=\"cssIcon-open-window-w\"></i><span class=\"hidden-spoken\">'+GE5P.ge5p_newWindowText+'</span>');
										jQuery("a",li).append(iconNewWindow);
									}
									if(value.hiddenText.length > 0) {
										// There is an hidden text element
										jQuery("a",li).append('<span class=\"hidden-spoken\">'+value.hiddenText+'</span>');
									}
									if(GE5P.ge5p_supportsTouch) {
										// // Touch Device so add hidden-spoken
										jQuery("a",li).append('<span class=\"hidden-spoken\"></span>');
									}
									if (ul_right_aria_owns.length > 0) {
										ul_right_aria_owns = ul_right_aria_owns+" "+value.id;
									} else {
										ul_right_aria_owns = value.id;
									}
									var aria_owns = "";
									var aria_posinset = 0;
									// Language Children List
									/* The user will always see two links
									 * Either In Spanish & Other Languages (app is in scope)
									 * Or Page in Spanish & Other Languages (app out of scope)
									 */	
									jQuery.each(value.children, function(x, subnavlinks) {
										var showLanguageChildLink = false;
										var pageinspanishlink = false;
										switch(subnavlinks.actionType) {
											/* 	TO DO
												ADD Back once all apps are on 1.3
											case "motionpointinscope":
												// Show if App is in scope for Motion Point
												if(GE5P.ge5p_motionpoint) {
													showLanguageChildLink = true;
												}
												break;
											*/
											case "PageInEnglishOrSpanish":
												// Either Show the Link or Show the "enEspanol" link
												if(GE5P.ge5p_motionpoint) {
													/* Motion Point APP
													 * Add URL special here
													 * TO DO - Remove this conditional once all apps are on 1.3
													 */
													
													// Check GNSESS for locale then show opposite content
													if(GE5P.ge5p_localLanguage == 'en_US') {
														var subnavDisplayName = "en Espa&ntilde;ol";
													} else {
														var subnavDisplayName = "In English";
													}													
													 
													var ul = jQuery('.'+value.id);
													aria_posinset = aria_posinset + 1;
													var li2 = jQuery('<li class=\"ge5p_z1-menuitem\"><a tabindex=\"'+GE5P.ge5p_tabIndexZone1+'\" aria-label=\"'+subnavDisplayName+'\" role=\"treeitem\" aria-setsize=\"'+value.children.length+'\" aria-posinset=\"'+aria_posinset+'\" aria-level=\"2\"></a></li>');
													ul.append(li2);
													jQuery("a",li2).html(subnavDisplayName);
													jQuery("a",li2).attr("id", "ge5p_z1-change-language");
													jQuery("a",li2).attr("href", "http://www.att.com/es-us/");
													jQuery("a",li2).attr("mporgnav", "");
													jQuery("a",li2).attr("data-lang", "es"); // On Hold - Motion Point - GE5P.ge5p_teslaLSLocale = where I am = en - so needs to be where i'm going
													jQuery("a",li2).attr("data-href", "http://www.att.com/es-us/"); // Motion Point
													if(GE5P.ge5p_supportsTouch) {
														// // Touch Device so add hidden-spoken
														jQuery("a",li2).append('<span class=\"hidden-spoken\"></span>');
													}
													if (aria_owns.length > 0) {
														aria_owns = aria_owns+" ge5p_z1-change-language";
													} else {
														aria_owns = "ge5p_z1-change-language";
													}
												} else {
													if(GE5P.ge5p_globalNavDefaultLanguageChangeLink) { //true
														showLanguageChildLink = true;
														pageinspanishlink = true;
													} else {
														showLanguageChildLink = false;
														pageinspanishlink = false;
													}
												}
												break;
											case "siteinspanish":
												// Hide for Tesla 1.3+ & if Motion Point is set to true
												if(GE5P.ge5p_motionpoint) {
													showLanguageChildLink = false;
												} else {
													showLanguageChildLink = true;
												}
												break;
											case "":
												// Show always
												showLanguageChildLink = true;
												break;
										}
										if(showLanguageChildLink) {
											var ul = jQuery('.'+value.id);
											aria_posinset = aria_posinset + 1;
											var li2 = jQuery('<li class=\"ge5p_z1-menuitem\"><a tabindex=\"'+GE5P.ge5p_tabIndexZone1+'\" aria-label=\"'+subnavlinks.displayName+'\" role=\"treeitem\" aria-setsize=\"'+value.children.length+'\" aria-posinset=\"'+aria_posinset+'\" aria-level=\"2\"></a></li>');
											ul.append(li2);
											jQuery("a",li2).html(subnavlinks.displayName);
											jQuery("a",li2).attr("id", subnavlinks.id);
											jQuery("a",li2).attr("href", subnavlinks.url);
											if(pageinspanishlink) {
												jQuery("a",li2).addClass("ge5p_languageEventLink");
											}
											if(subnavlinks.actionType == "motionpointinscope") {
												jQuery("a",li2).attr("mporgnav", "");
												jQuery("a",li2).attr("data-lang", "es"); // On Hold - Motion Point - GE5P.ge5p_teslaLSLocale = where I am = en - so needs to be where i'm going
												jQuery("a",li2).attr("data-href", "http://www.att.com/es-us/"); // Motion Point
											}
											if(subnavlinks.hiddenText.length > 0) {
												// There is an hidden text element
												jQuery("a",li2).append('<span class=\"hidden-spoken\">'+subnavlinks.hiddenText+'</span>');
											}
											if(GE5P.ge5p_supportsTouch) {
												// // Touch Device so add hidden-spoken
												jQuery("a",li2).append('<span class=\"hidden-spoken\"></span>');
											}
											if(subnavlinks.windowLocation == 'Y') {
												// Link opens in new window
												jQuery("a",li2).attr("target", "_blank");
												var iconNewWindow = jQuery('<i class=\"cssIcon-open-window-w\"></i><span class=\"hidden-spoken\">'+GE5P.ge5p_newWindowText+'</span>');
												jQuery("a",li2).append(iconNewWindow);
											}
											if (aria_owns.length > 0) {
												aria_owns = aria_owns+" "+subnavlinks.id;
											} else {
												aria_owns = subnavlinks.id;
											}
										}
									}); // end each
									jQuery('#'+urlID).attr('aria-owns',aria_owns);
									// Check if the Language Tab is to be shown as set in the Page Global Vars
									
									// Check if Motion Point, then if Change Lang Link is active
									if(GE5P.ge5p_motionpoint) {
										
										/* 	TO DO
											Add Back once all apps are on 1.3
										jQuery('.ge5p_z1-language_submenu li').first().find('a').attr('id','ge5p_z1-change-language');
										*/
										
										// Add Change language Locale if exists in GNSESS cookie
										if(GE5P.ge5p_teslaGNSESSPresent) {
											// Cookie exists so check for locale info in cookie
											jQuery('.ge5p_z1-language_submenu li').first().find('a').addClass(GE5P.ge5p_localLanguage);
										} else {
											// Cookie does not exist so add default class of en_US
											jQuery('.ge5p_z1-language_submenu li').first().find('a').addClass('en_US');
										}
									} else {
										if(GE5P.ge5p_globalNavDefaultLanguageChangeLink) {
											// Add Change URL link
											jQuery('.ge5p_z1-language_submenu li').first().find('a.ge5p_languageEventLink').attr('href',GE5P.ge5p_changeLanguageURLVar);
											jQuery('.ge5p_z1-language_submenu li').first().find('a.ge5p_languageEventLink').attr('id','ge5p_z1-change-language');
											// Add lang attribute to anchor
											if(value.displayName == 'Language') {
												// Show es
												jQuery('.ge5p_z1-language_submenu li').first().find('a.ge5p_languageEventLink').attr('lang','es');
											} else {
												// Show en
												jQuery('.ge5p_z1-language_submenu li').first().find('a.ge5p_languageEventLink').attr('lang','en');
											}
											// Add Change language Locale if exists in GNSESS cookie
											if(GE5P.ge5p_teslaGNSESSPresent) {
												// Cookie exists so check for locale info in cookie
												jQuery('.ge5p_z1-language_submenu li').first().find('a.ge5p_languageEventLink').addClass(GE5P.ge5p_localLanguage);
											} else {
												// Cookie does not exist so add default class of en_US
												jQuery('.ge5p_z1-language_submenu li').first().find('a.ge5p_languageEventLink').addClass('en_US');
											}
										} else {
											// Hide Link
											jQuery('.ge5p_z1-language_submenu li').first().find('a.ge5p_languageEventLink').hide();
										}
									}
								} else {
									// Check if there is a menu or regular link
									if(value.children.length > 0) {
										var li = jQuery('<li class=\"ge5p_z1-tab\"><a aria-selected=\"false\" aria-expanded=\"false\" aria-setsize="'+value.children.length+'" tabindex=\"'+GE5P.ge5p_tabIndexZone1+'\" aria-posinset=\"'+aria_posinset+'\" role=\"treeitem\" class=\"ge5p_z1-drop-down\" aria-level=\"1\"></a><div class=\"ge5p_z1-menu\"><ul role=\"group\"></ul></div></li>');
										// Append li to ul seg menu
										jQuery('#ge5p_z1-nav-right-seg').append(li);
										// Anchor Tag Data
										jQuery("a",li).html(value.displayName);
										jQuery("a",li).attr("href", value.url);
										jQuery("a",li).attr("id", value.id);
										jQuery("ul",li).addClass(value.id);
										// Check if this links open in a new window
										if(value.windowLocation == 'Y') {
											// Link opens in new window
											jQuery("a",li).attr("target", "_blank");
											var iconNewWindow = jQuery('<i class=\"cssIcon-open-window-w\"></i><span class=\"hidden-spoken\">'+GE5P.ge5p_newWindowText+'</span>');
											jQuery("a",li).append(iconNewWindow);
										}
										if(value.hiddenText.length > 0) {
											// There is an hidden text element
											jQuery("a",li).append('<span class=\"hidden-spoken\">'+value.hiddenText+'</span>');
										}
										if(GE5P.ge5p_supportsTouch) {
											// // Touch Device so add hidden-spoken
											jQuery("a",li).append('<span class=\"hidden-spoken\"></span>');
										}
										if (ul_right_aria_owns.length > 0) {
											ul_right_aria_owns = ul_right_aria_owns+" "+value.id;
										} else {
											ul_right_aria_owns = value.id;
										}
										// Coverage Maps - Add class for this tab - Coverage Maps code = 002010
										var ge5p_coverageMapsTab = false;
										if(value.code == '002010') {
											ge5p_coverageMapsTab = true;
										}
										// Menu Data
										var aria_owns = "";
										var aria_posinset = 0;
										jQuery.each(value.children, function(x, subnavlinks) {
											var ul = jQuery('.'+value.id);
											// Coverage Maps - Add class for this tab - Coverage Maps code = 002010
											if(ge5p_coverageMapsTab) {
												ul.addClass('ge5p-coverage-maps');
											}
											aria_posinset = aria_posinset + 1;
											var li2 = jQuery('<li class=\"ge5p_z1-menuitem\"><a tabindex=\"'+GE5P.ge5p_tabIndexZone1+'\" aria-label=\"'+subnavlinks.displayName+'\" role=\"treeitem\" aria-setsize=\"'+value.children.length+'\" aria-posinset=\"'+aria_posinset+'\" aria-level=\"2\"></a></li>');
											ul.append(li2);
											jQuery("a",li2).html(subnavlinks.displayName);
											jQuery("a",li2).attr("id", subnavlinks.id);
											jQuery("a",li2).attr("href", subnavlinks.url);
											if(subnavlinks.hiddenText.length > 0) {
												// There is an hidden text element
												jQuery("a",li2).append('<span class=\"hidden-spoken\">'+subnavlinks.hiddenText+'</span>');
											}
											if(GE5P.ge5p_supportsTouch) {
												// // Touch Device so add hidden-spoken
												jQuery("a",li2).append('<span class=\"hidden-spoken\"></span>');
											}
											if(subnavlinks.windowLocation == 'Y') {
												// Link opens in new window
												jQuery("a",li2).attr("target", "_blank");
												var iconNewWindow = jQuery('<i class=\"cssIcon-open-window-w\"></i><span class=\"hidden-spoken\">'+GE5P.ge5p_newWindowText+'</span>');
												jQuery("a",li2).append(iconNewWindow);
											}
											if (aria_owns.length > 0) {
												aria_owns = aria_owns+" "+subnavlinks.id;
											} else {
												aria_owns = subnavlinks.id;
											}
										});
										jQuery('#'+urlID).attr('aria-owns',aria_owns);
									} else {
										// Regular Link
										var li = jQuery('<li class=\"ge5p_z1-tab\"><a aria-label=\"'+value.displayName+'\" role=\"treeitem\" tabindex=\"'+GE5P.ge5p_tabIndexZone1+'\" aria-setsize=\"'+value.children.length+'\" aria-posinset=\"'+key+'\" aria-level=\"1\"></a></li>');
										// Append li to ul seg menu
										jQuery('#ge5p_z1-nav-right-seg').append(li);
										// Add Display Text to a
										jQuery("a",li).html(value.displayName);
										jQuery("a",li).attr("href", value.url);
										jQuery("a",li).attr("id", value.id);
										if(value.hiddenText.length > 0) {
											// There is an hidden text element
											jQuery("a",li).append('<span class=\"hidden-spoken\">'+value.hiddenText+'</span>');
										}
										if(GE5P.ge5p_supportsTouch) {
											// // Touch Device so add hidden-spoken
											jQuery("a",li).append('<span class=\"hidden-spoken\"></span>');
										}
										// Check if this links open in a new window
										if(value.windowLocation == 'Y') {
											// Link opens in new window
											jQuery("a",li).attr("target", "_blank");
											var iconNewWindow = jQuery('<i class=\"cssIcon-open-window-w\"></i><span class=\"hidden-spoken\">'+GE5P.ge5p_newWindowText+'</span>');
											jQuery("a",li).append(iconNewWindow);
										}
										if (ul_right_aria_owns.length > 0) {
											ul_right_aria_owns = ul_right_aria_owns+" "+value.id;
										} else {
											ul_right_aria_owns = value.id;
										}
									}
								}
							}
						});
						jQuery('#ge5p_z1-nav-right-seg').attr('aria-owns',ul_right_aria_owns);
						// Check if this is the MyAT&T My Linked Link and add special class to adjust padding.
						if (GE5P.ge5p_teslaGNSESSPresent && GE5P.ge5p_globalNavDefaultMyLinkedTabSet) {
							jQuery('.ge5p_global_styles #ge5p_z1 .ge5p_z1-tab').addClass('ge5p_mylinked_show');
							setTimeout(function(){
								//Add Special Class if Nav is Spanish - this will fix Tab dropdown box alignment
								if(GE5P.ge5p_localLanguage == 'es_US') {
									jQuery('#ge5p_z1-mylinked-link').parent().addClass('ge5p_z1-mylinked-drop-down-spanish');
								} else {
									jQuery('#ge5p_z1-mylinked-link').parent().removeClass('ge5p_z1-mylinked-drop-down-spanish');
								}
							}, GE5P.ge5p_dropDownSetTimeout);
						}
					}
					if(element.nav_position == 'ge5p_z1_3') {
						// Miscellaneous items - Search, Register, Change, EnterZIP, Login, WelcometoATT
						jQuery.each(element.children, function (key, value) {
							switch(value.actionType) {
								case "Search":
									setTimeout(function(){
										var searchURL = value.url;
										var searchURLArr = searchURL.split('.com/');
										var searchURLPath = searchURLArr[1];
										jQuery('#searchForm').attr('action',GE5P.ge5p_globalsearchurlBase + "/" + searchURLPath);
										// Search Box Autocomplete Bind
										GE5P.searchAutocomplete();
										/* Search Form */
										jQuery('#searchForm').submit(GE5P.validateSearchForm);
									}, 500);
									break;
								case "Register":
									setTimeout(function(){
										jQuery('#ge5p_z2-zipcode-register').attr('href',value.url);
										jQuery('#ge5p_z2-zipcode-register').text(value.displayName);
										if(GE5P.ge5p_teslaGNSESSPresent){
										} else {
											if(!GE5P.ge5p_is_user_authenticated){
												jQuery('#ge5p_z2-zipcode-register').show();
											}
										}
									}, 500);
									break;
								case "Change":
									setTimeout(function(){
										jQuery('#ge5p_z2-zipcode-change').attr('href',value.url+GE5P.ge5p_requestURLVar);
										jQuery('#ge5p_z2-zipcode-change').text(value.displayName);
										if(jQuery('#ge5p_z2-zipcode-city').text().length > 0 || jQuery('#ge5p_z2-zipcode-state').text().length > 0 || jQuery('#ge5p_z2-zipcode-zip').text().length > 0) {
											jQuery('#ge5p_z2-zipcode-change').show();
										}
									}, 500);
									break;
								case "EnterZIP":
									jQuery('#ge5p_z2-zipcode-enter').attr('href',value.url+GE5P.ge5p_requestURLVar);
									jQuery('#ge5p_z2-zipcode-enter').text(value.displayName);
									break;
								case "Login":
									jQuery('#ge5p_z2-user-auth-button').attr('src',value.image);
									if(value.hiddenText) {
										if(value.hiddenText > 0) {
											jQuery('#ge5p_z2-user-auth-button').attr('alt',value.hiddenText);
											jQuery('#ge5p_z2-user-auth').append('<span class=\"hidden-spoken\">'+value.hiddenText+'</span>');
										} else {
											jQuery('#ge5p_z2-user-auth-button').attr('alt','Log In Button');
											jQuery('#ge5p_z2-user-auth').append('<span class=\"hidden-spoken\">Log In Button</span>');
										}
									} else {
										jQuery('#ge5p_z2-user-auth-button').attr('alt','Log In Button');
										jQuery('#ge5p_z2-user-auth').append('<span class=\"hidden-spoken\">Log In Button</span>');
									}
									jQuery('#ge5p_z2-user-auth').attr('href',value.url);
									jQuery('#ge5p_z2-user-auth-button').css('visibility', 'visible');
									break;
								case "Logout":
									jQuery('#ge5p_z2-user-auth-button').attr('src',value.image);
									if (GE5P.ge5p_linkLogOutURLVar) {
										if (GE5P.ge5p_linkLogOutURLVar.length > 0) {
											jQuery('#ge5p_z2-user-auth').attr('href',GE5P.ge5p_linkLogOutURLVar);
										} else {
											jQuery('#ge5p_z2-user-auth').attr('href',value.url);
										}
									} else {
										jQuery('#ge5p_z2-user-auth').attr('href',value.url);
									}
									if(value.hiddenText) {
										if(value.hiddenText > 0) {
											jQuery('#ge5p_z2-user-auth-button').attr('alt',value.hiddenText);
											jQuery('#ge5p_z2-user-auth').append('<span class=\"hidden-spoken\">'+value.hiddenText+'</span>');
										} else {
											jQuery('#ge5p_z2-user-auth-button').attr('alt','Log Out Button');
											jQuery('#ge5p_z2-user-auth').append('<span class=\"hidden-spoken\">Log Out Button</span>');
										}
									} else {
										jQuery('#ge5p_z2-user-auth-button').attr('alt','Log Out Button');
										jQuery('#ge5p_z2-user-auth').append('<span class=\"hidden-spoken\">Log Out Button</span>');
									}
									jQuery('#ge5p_z2-user-auth-button').css('visibility', 'visible');
									break;
									/* Removed per QC #8875
								case "WelcometoATT":
									setTimeout(function(){
										jQuery('#ge5p_z2-zipcode-welcome').text(value.displayName);
										if(GE5P.ge5p_teslaGNSESSPresent){
											// Returning User
											if(!GE5P.ge5p_is_user_authenticated){
												jQuery('#ge5p_z2-zipcode-welcome').show();
												jQuery('#ge5p_z2-zipcode-register').show();
											}
										}
									}, 500);
									break;
								*/
								case "WelcomeBack":
									setTimeout(function(){
										jQuery('#ge5p_z2-zipcode-welcomeback').text(value.displayName);
										if(GE5P.ge5p_teslaGNSESSPresent){
											// Returning User
											if(GE5P.ge5p_is_user_authenticated){
												jQuery('#ge5p_z2-zipcode-welcomeback').show();
											}
										}
									}, 500);
									break;
								case "Hi":
									setTimeout(function(){
										jQuery('#ge5p_z2-zipcode-hi').text(value.displayName);
										if(GE5P.ge5p_teslaGNSESSPresent){
											// Returning User
											if(GE5P.ge5p_is_user_authenticated == true){
												jQuery('#ge5p_z2-zipcode-hi').show();
											}
										}
									}, 500);
									break;
								case "FN":
									setTimeout(function(){
										if(GE5P.ge5p_teslaGNSESSPresent){
											// Returning User
											if(GE5P.ge5p_is_user_authenticated){
												// User Returning & auth - add name/url
												if(GE5P.ge5p_userFirstName) {
													if(GE5P.ge5p_userFirstName.length > 0) {
														jQuery('#ge5p_z2-zipcode-firstname').text(GE5P.ge5p_userFirstName);
													}
												} else {
													if(value.displayName) {
														jQuery('#ge5p_z2-zipcode-firstname').text(value.displayName);
													} else {
														jQuery('#ge5p_z2-zipcode-firstname').text('Placeholder First Name');
														jQuery('#ge5p_z2-zipcode-firstname').css('text-indent','-9999px');
														jQuery('#ge5p_z2-zipcode-firstname').attr('href',"../../index.htm"/*tpa=http://www.att.com/*/);
														jQuery('#ge5p_z2-zipcode-firstname').hide();
													}
												}
												if(value.url){
													jQuery('#ge5p_z2-zipcode-firstname').attr('href',value.url);
												} else {
													jQuery('#ge5p_z2-zipcode-firstname').attr('href',"../../index.htm"/*tpa=http://www.att.com/*/);
												}
												jQuery('#ge5p_z2-zipcode-firstname').css('text-indent','inherit');
												jQuery('#ge5p_z2-zipcode-firstname').show();
											} else {
												// User Returning but not auth - hide name/url
												jQuery('#ge5p_z2-zipcode-firstname').text('Placeholder First Name');
												jQuery('#ge5p_z2-zipcode-firstname').css('text-indent','-9999px');
												jQuery('#ge5p_z2-zipcode-firstname').attr('href',"../../index.htm"/*tpa=http://www.att.com/*/);
												jQuery('#ge5p_z2-zipcode-firstname').hide();
											}
										} else {
											// New User
											jQuery('#ge5p_z2-zipcode-firstname').text('Placeholder First Name');
											jQuery('#ge5p_z2-zipcode-firstname').css('text-indent','-9999px');
											jQuery('#ge5p_z2-zipcode-firstname').attr('href',"../../index.htm"/*tpa=http://www.att.com/*/);
											jQuery('#ge5p_z2-zipcode-firstname').hide();
										}
									}, 500);
									break;
							}
						});
					}
				}
				if(navZone == 'z2') {
					// Zone 2 Nav
					// AT&T has three Primary Links
					// ge5p_z2_1 - Shop
					// ge5p_z2_2 - myAT&T
					// ge5p_z2_3 - Support

					if(element.nav_position == 'ge5p_z2_1') {
						// Add Logo to ul
						var ge5p_z2_logo = jQuery('<li id=\"primaryLogo\" class=\"ge5p_z2-primaryLogo\"><a class=\"ge5p_z2-primary-nav-el\" title=\"AT&amp;T Home Page Link\" alt=\"AT&amp;T Home Page Link\" href=\"../../index.htm\"/*tpa=http://www.att.com/*/ tabindex=\"'+GE5P.ge5p_tabIndexZone2+'\" role=\"treeitem\" aria-setsize=\"4\" aria-level=\"1\" aria-posinset=\"1\">AT&amp;T<span class=\"hidden-spoken\">AT&amp;T Home Page Link</span></a></li>');
						jQuery('#ge5p_z2-nav-bar').append(ge5p_z2_logo);
						// Shop
						var liSec1 = jQuery('<li class=\"ge5p_z2-nav-item\"><a id=\"'+element.id+'\" class=\"ge5p_z2-primary-nav-el\" tabindex=\"'+GE5P.ge5p_tabIndexZone2+'\" aria-label=\"'+element.displayName+'\" role=\"treeitem\" aria-setsize=\"4\" aria-level=\"1\" aria-posinset=\"2\" aria-selected=\"false\" aria-expanded=\"false\" aria-owns=\"\" href=\"'+element.url+'\">'+element.displayName+'</a><ul aria-owns=\"\" role=\"group\" class=\"ge5p_z2-nav-bar-subnav\"></ul></li>');
						// Append li to ul seg menu
						jQuery('#ge5p_z2-nav-bar').append(liSec1);
						jQuery("ul",liSec1).addClass('sub-'+element.id);
						var ulID = "ge5p_z2_1_1";
						jQuery("ul",liSec1).attr("id",ulID);
						jQuery("a",liSec1).attr("aria-owns",ulID);
						if(element.windowLocation == 'Y') {
							// Link opens in new window
							jQuery("a",liSec1).attr("target", "_blank");
						}
						if(GE5P.ge5p_supportsTouch) {
							// // Touch Device so add hidden-spoken
							jQuery("a",liSec1).append('<span class=\"hidden-spoken\"></span>');
						}
						// Check if Menu Item is Active from Page
						if(element.code == GE5P.ge5p_globalNavDefaultPrimaryActive) {
							// This Primary item is active
							GE5P.closeSegUtilNav();
							// Tray open
							// Tray 1 = Primary Tray Open
							// Tray 2 = Secondary Tray Open
							// Remove "active" from all li's
							jQuery('.ge5p_z2-nav-item').removeClass('active');
							jQuery('.ge5p_z2-nav-bar-subnav li').removeClass('active');
							jQuery('.ge5p_z2-nav-bar-subnav li').find('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').removeClass('active').hide();
							// Add class "active" to li to turn background white
							jQuery("#"+element.id).parent().addClass('active');
							jQuery("#"+element.id).attr('aria-expanded','true').attr('aria-selected','true');
							// Tray 1 height is Primary Nav Height + Primary Nav li.active
							var heightTray1Open = GE5P.heightPrimaryNavClosed + jQuery('#ge5p_z2-nav-bar li.active').height();
							// Increase height of PrimaryNav to show tray
							GE5P.animatePrimaryTray(heightTray1Open);
							jQuery("#"+element.id).addClass('ge5p_z2-default-primary-active');
						}
						// Menu Data
						var aria_owns = "";
						var aria_posinset = 0;
						jQuery.each(element.children, function(x, subnavlinks) {
							var ul2 = jQuery('.sub-'+element.id);
							aria_posinset = aria_posinset + 1;
							var subNavLinkID = subnavlinks.id;
							var liTer1 = jQuery('<li class=\"'+subNavLinkID+'\"><a class=\"ge5p_z2-secondary-nav-el\" tabindex=\"'+GE5P.ge5p_tabIndexZone2+'\" aria-label=\"'+subnavlinks.displayName+'\" role=\"treeitem\" aria-setsize=\"'+element.children.length+'\" aria-posinset=\"'+aria_posinset+'\" aria-level=\"2\"></a></li>');
							ul2.append(liTer1);
							jQuery("a",liTer1).html(subnavlinks.displayName);
							jQuery("a",liTer1).attr("id", subNavLinkID);
							jQuery("a",liTer1).attr("href", subnavlinks.url);
							// Check if this links open in a new window
							if(subnavlinks.windowLocation == 'Y') {
								// Link opens in new window
								jQuery("a",liTer1).attr("target", "_blank");
								var iconNewWindow = jQuery('<i class=\"cssIcon-open-window-b\"></i><span class=\"hidden-spoken\">'+GE5P.ge5p_newWindowText+'</span>');
								jQuery("a",liTer1).append(iconNewWindow);
							}
							if(GE5P.ge5p_supportsTouch) {
								// // Touch Device so add hidden-spoken
								jQuery("a",liTer1).append('<span class=\"hidden-spoken\"></span>');
							}
							if (aria_owns.length > 0) {
								aria_owns = aria_owns+" "+subnavlinks.id;
							} else {
								aria_owns = subnavlinks.id;
							}
							// Check if link has a tertiary level
							if(subnavlinks.children.length > 0) {
								jQuery('#'+subNavLinkID).addClass('ge5p_z2-nav-bar-subnav-has-tray');
								var liSecNav = jQuery('.'+subNavLinkID);
								var liTer2 = jQuery('<div class=\"tray\"></div>');
								liSecNav.append(liTer2);
								var aria_ownsTeir = "";
								var aria_posinsetTeir = 0;
								var ulIDTeir;
								jQuery.each(subnavlinks.children, function(a, header) {
									var ulIDTeir = "ge5p_z2_1_1_"+a;
									if(header.isHead) {
										liSecNav.find('.tray').append('<div id=\"'+header.id+'\"><h3>'+header.displayName+'</h3><ul id=\"'+ulIDTeir+'\"></ul></div>');
										GE5P.trayColumnID = jQuery('#'+header.id);
									} else {
										if (GE5P.trayColumnID) {
											aria_posinsetTeir = aria_posinsetTeir + 1;
											GE5P.trayColumnID.find('ul').append('<li role=\"presentation\"><a id=\"'+header.id+'\" class=\"navTertiaryLink\" tabindex=\"'+GE5P.ge5p_tabIndexZone2+'\" href=\"'+header.url+'\" aria-label=\"'+header.displayName+'\" role=\"treeitem\" aria-setsize=\"'+subnavlinks.children.length+'\" aria-posinset=\"'+aria_posinsetTeir+'\" aria-level=\"3\">'+header.displayName+'</a></li>');
											if(header.windowLocation == 'Y') {
												// Link opens in new window
												jQuery('#'+header.id).attr("target", "_blank");
												var iconNewWindow = jQuery('<i class=\"cssIcon-open-window-b\"></i><span class=\"hidden-spoken\">'+GE5P.ge5p_newWindowText+'</span>');
												jQuery('#'+header.id).append(iconNewWindow);
											}
										}
									}
									if (aria_ownsTeir.length > 0) {
										aria_ownsTeir = aria_ownsTeir+" "+header.id;
									} else {
										aria_ownsTeir = header.id;
									}
								});
								jQuery('#'+ulIDTeir).attr('aria-owns',aria_ownsTeir);
							}
							// Check if Secondary Menu Item is Active from Page
							if(subnavlinks.code == GE5P.ge5p_globalNavDefaultSecondaryActive) {
								//subNavLinkID
								// We have a Tray so display
								jQuery('.ge5p_z2-nav-bar-subnav li').removeClass('active');
								jQuery('.ge5p_z2-nav-bar-subnav li').find('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').removeClass('active').hide();
								jQuery('#'+subNavLinkID).addClass('ge5p_z2-default-secondary-active');
							}
						});
						jQuery('#'+ulID).attr('aria-owns',aria_owns);
					}  // End Shop
					if(element.nav_position == 'ge5p_z2_2') {
						// myat&t
						var liSec2 = jQuery('<li class=\"ge5p_z2-nav-item\"><a id=\"'+element.id+'\" class=\"ge5p_z2-primary-nav-el\" tabindex=\"'+GE5P.ge5p_tabIndexZone2+'\" aria-label=\"'+element.displayName+'\" role=\"treeitem\" aria-setsize=\"4\" aria-level=\"1\" aria-posinset=\"2\" aria-selected=\"false\" aria-expanded=\"false\" aria-owns=\"\" href=\"'+element.url+'\">'+element.displayName+'</a><ul aria-owns=\"\" role=\"group\" class=\"ge5p_z2-nav-bar-subnav\"></ul></li>');
						// Append li to ul seg menu
						jQuery('#ge5p_z2-nav-bar').append(liSec2);
						jQuery("ul",liSec2).addClass('sub-'+element.id);
						var ulID = "ge5p_z2_1_2";
						jQuery("ul",liSec2).attr("id",ulID);
						jQuery("a",liSec2).attr("aria-owns",ulID);
						if(element.windowLocation == 'Y') {
							// Link opens in new window
							jQuery("a",liSec2).attr("target", "_blank");
							var iconNewWindow = jQuery('<i class=\"cssIcon-open-window-b\"></i><span class=\"hidden-spoken\">'+GE5P.ge5p_newWindowText+'</span>');
							jQuery("a",liSec2).append(iconNewWindow);
						}
						if(GE5P.ge5p_supportsTouch) {
							// // Touch Device so add hidden-spoken
							jQuery("a",liSec2).append('<span class=\"hidden-spoken\"></span>');
						}
						// Check if Menu Item is Active from Page
						if(element.code == GE5P.ge5p_globalNavDefaultPrimaryActive) {
							// This Primary item is active
							GE5P.closeSegUtilNav();
							// Tray open
							// Tray 1 = Primary Tray Open
							// Tray 2 = Secondary Tray Open
							// Remove "active" from all li's
							jQuery('.ge5p_z2-nav-item').removeClass('active');
							jQuery('.ge5p_z2-nav-bar-subnav li').removeClass('active');
							jQuery('.ge5p_z2-nav-bar-subnav li').find('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').removeClass('active').hide();
							// Add class "active" to li to turn background white
							jQuery("#"+element.id).parent().addClass('active');
							jQuery("#"+element.id).attr('aria-expanded','true').attr('aria-selected','true');
							// Tray 1 height is Primary Nav Height + Primary Nav li.active
							var heightTray1Open = GE5P.heightPrimaryNavClosed + jQuery('#ge5p_z2-nav-bar li.active').height();
							// Increase height of PrimaryNav to show tray
							GE5P.animatePrimaryTray(heightTray1Open);
							jQuery("#"+element.id).addClass('ge5p_z2-default-primary-active');
						}
						// Menu Data
						var aria_owns = "";
						var aria_posinset = 0;
						jQuery.each(element.children, function(x, subnavlinks) {
							var ul2 = jQuery('.sub-'+element.id);
							aria_posinset = aria_posinset + 1;
							var subNavLinkID = subnavlinks.id;
							var liTer1 = jQuery('<li class=\"'+subNavLinkID+'\" role=\"presentation\"><a class=\"ge5p_z2-secondary-nav-el\" tabindex=\"'+GE5P.ge5p_tabIndexZone2+'\" aria-label=\"'+subnavlinks.displayName+'\" role=\"treeitem\" aria-setsize=\"'+element.children.length+'\" aria-posinset=\"'+aria_posinset+'\" aria-level=\"2\"></a></li>');
							ul2.append(liTer1);
							jQuery("a",liTer1).html(subnavlinks.displayName);
							jQuery("a",liTer1).attr("id", subnavlinks.id);
							jQuery("a",liTer1).attr("href", subnavlinks.url);
							// Check if this links open in a new window
							if(subnavlinks.windowLocation == 'Y') {
								// Link opens in new window
								jQuery("a",liTer1).attr("target", "_blank");
								var iconNewWindow = jQuery('<i class=\"cssIcon-open-window-b\"></i><span class=\"hidden-spoken\">'+GE5P.ge5p_newWindowText+'</span>');
								jQuery("a",liTer1).append(iconNewWindow);
							}
							if(GE5P.ge5p_supportsTouch) {
								// // Touch Device so add hidden-spoken
								jQuery("a",liTer1).append('<span class=\"hidden-spoken\"></span>');
							}
							if (aria_owns.length > 0) {
								aria_owns = aria_owns+" "+subnavlinks.id;
							} else {
								aria_owns = subnavlinks.id;
							}
							// Check if link has a tertiary level
							if(subnavlinks.children.length > 0) {
								jQuery('#'+subNavLinkID).addClass('ge5p_z2-nav-bar-subnav-has-tray');
								var liSecNav = jQuery('.'+subNavLinkID);
								var liTer2 = jQuery('<div class=\"tray\"></div>');
								liSecNav.append(liTer2);
								var aria_ownsTeir = "";
								var aria_posinsetTeir = 0;
								var ulIDTeir;
								jQuery.each(subnavlinks.children, function(a, header) {
									var ulIDTeir = "ge5p_z2_1_1_"+a;
									if(header.isHead) {
										liSecNav.find('.tray').append('<div id=\"'+header.id+'\"><h3>'+header.displayName+'</h3><ul id=\"'+ulIDTeir+'\"></ul></div>');
										GE5P.trayColumnID = jQuery('#'+header.id);
									} else {
										if (GE5P.trayColumnID) {
											aria_posinsetTeir = aria_posinsetTeir + 1;
											GE5P.trayColumnID.find('ul').append('<li role=\"presentation\"><a id=\"'+header.id+'\" class=\"navTertiaryLink\" tabindex=\"'+GE5P.ge5p_tabIndexZone2+'\" href=\"'+header.url+'\" aria-label=\"'+header.displayName+'\" role=\"treeitem\" aria-setsize=\"'+subnavlinks.children.length+'\" aria-posinset=\"'+aria_posinsetTeir+'\" aria-level=\"3\">'+header.displayName+'</a></li>');
											if(header.windowLocation == 'Y') {
												// Link opens in new window
												jQuery('#'+header.id).attr("target", "_blank");
												var iconNewWindow = jQuery('<i class=\"cssIcon-open-window-b\"></i><span class=\"hidden-spoken\">'+GE5P.ge5p_newWindowText+'</span>');
												jQuery('#'+header.id).append(iconNewWindow);
											}
										}
									}
									if (aria_ownsTeir.length > 0) {
										aria_ownsTeir = aria_ownsTeir+" "+header.id;
									} else {
										aria_ownsTeir = header.id;
									}
								});
								jQuery('#'+ulIDTeir).attr('aria-owns',aria_ownsTeir);
							}
							// Check if Secondary Menu Item is Active from Page
							if(subnavlinks.code == GE5P.ge5p_globalNavDefaultSecondaryActive) {
								//subNavLinkID
								// We have a Tray so display
								jQuery('.ge5p_z2-nav-bar-subnav li').removeClass('active');
								jQuery('.ge5p_z2-nav-bar-subnav li').find('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').removeClass('active').hide();
								jQuery('#'+subNavLinkID).addClass('ge5p_z2-default-secondary-active');
							}
						});
						jQuery('#'+ulID).attr('aria-owns',aria_owns);
					} // End myAT&T
					if(element.nav_position == 'ge5p_z2_3') {
						// Support
						var liSec3 = jQuery('<li class=\"ge5p_z2-nav-item\"><a id=\"'+element.id+'\" class=\"ge5p_z2-primary-nav-el\" tabindex=\"'+GE5P.ge5p_tabIndexZone2+'\" aria-label=\"'+element.displayName+'\" role=\"treeitem\" aria-setsize=\"4\" aria-level=\"1\" aria-posinset=\"2\" aria-selected=\"false\" aria-expanded=\"false\" aria-owns=\"\" href=\"'+element.url+'\">'+element.displayName+'</a><ul aria-owns=\"\" role=\"group\" class=\"ge5p_z2-nav-bar-subnav\"></ul></li>');
						// Append li to ul seg menu
						jQuery('#ge5p_z2-nav-bar').append(liSec3);
						jQuery("ul",liSec3).addClass('sub-'+element.id);
						var ulID = "ge5p_z2_1_3";
						jQuery("ul",liSec3).attr("id",ulID);
						jQuery("a",liSec3).attr("aria-owns",ulID);
						if(element.windowLocation == 'Y') {
							// Link opens in new window
							jQuery("a",liSec3).attr("target", "_blank");
							var iconNewWindow = jQuery('<i class=\"cssIcon-open-window-b\"></i><span class=\"hidden-spoken\">'+GE5P.ge5p_newWindowText+'</span>');
							jQuery("a",liSec3).append(iconNewWindow);
						}
						if(GE5P.ge5p_supportsTouch) {
							// // Touch Device so add hidden-spoken
							jQuery("a",liSec3).append('<span class=\"hidden-spoken\"></span>');
						}
						// Check if Menu Item is Active from Page
						if(element.code == GE5P.ge5p_globalNavDefaultPrimaryActive) {
							// This Primary item is active
							GE5P.closeSegUtilNav();
							// Tray open
							// Tray 1 = Primary Tray Open
							// Tray 2 = Secondary Tray Open
							// Remove "active" from all li's
							jQuery('.ge5p_z2-nav-item').removeClass('active');
							jQuery('.ge5p_z2-nav-bar-subnav li').removeClass('active');
							jQuery('.ge5p_z2-nav-bar-subnav li').find('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').removeClass('active').hide();
							// Add class "active" to li to turn background white
							jQuery("#"+element.id).parent().addClass('active');
							jQuery("#"+element.id).attr('aria-expanded','true').attr('aria-selected','true');
							// Tray 1 height is Primary Nav Height + Primary Nav li.active
							var heightTray1Open = GE5P.heightPrimaryNavClosed + jQuery('#ge5p_z2-nav-bar li.active').height();
							// Increase height of PrimaryNav to show tray
							GE5P.animatePrimaryTray(heightTray1Open);
							jQuery("#"+element.id).addClass('ge5p_z2-default-primary-active');
						}
						// Menu Data
						var aria_owns = "";
						var aria_posinset = 0;
						jQuery.each(element.children, function(x, subnavlinks) {
							var ul2 = jQuery('.sub-'+element.id);
							aria_posinset = aria_posinset + 1;
							var subNavLinkID = subnavlinks.id;
							var liTer1 = jQuery('<li class=\"'+subNavLinkID+'\" role=\"presentation\"><a class=\"ge5p_z2-secondary-nav-el\" tabindex=\"'+GE5P.ge5p_tabIndexZone2+'\" aria-label=\"'+subnavlinks.displayName+'\" role=\"treeitem\" aria-setsize=\"'+element.children.length+'\" aria-posinset=\"'+aria_posinset+'\" aria-level=\"2\"></a></li>');
							ul2.append(liTer1);
							jQuery("a",liTer1).html(subnavlinks.displayName);
							jQuery("a",liTer1).attr("id", subnavlinks.id);
							jQuery("a",liTer1).attr("href", subnavlinks.url);
							// Check if this links open in a new window
							if(subnavlinks.windowLocation == 'Y') {
								// Link opens in new window
								jQuery("a",liTer1).attr("target", "_blank");
								var iconNewWindow = jQuery('<i class=\"cssIcon-open-window-b\"></i><span class=\"hidden-spoken\">'+GE5P.ge5p_newWindowText+'</span>');
								jQuery("a",liTer1).append(iconNewWindow);
							}
							if(GE5P.ge5p_supportsTouch) {
								// // Touch Device so add hidden-spoken
								jQuery("a",liTer1).append('<span class=\"hidden-spoken\"></span>');
							}
							if (aria_owns.length > 0) {
								aria_owns = aria_owns+" "+subnavlinks.id;
							} else {
								aria_owns = subnavlinks.id;
							}
							// Check if link has a tertiary level
							if(subnavlinks.children.length > 0) {
								jQuery('#'+subNavLinkID).addClass('ge5p_z2-nav-bar-subnav-has-tray');
								var liSecNav = jQuery('.'+subNavLinkID);
								var liTer2 = jQuery('<div class=\"tray\"></div>');
								liSecNav.append(liTer2);
								var aria_ownsTeir = "";
								var aria_posinsetTeir = 0;
								var ulIDTeir;
								jQuery.each(subnavlinks.children, function(a, header) {
									var ulIDTeir = "ge5p_z2_1_1_"+a;
									if(header.isHead) {
										liSecNav.find('.tray').append('<div id=\"'+header.id+'\"><h3>'+header.displayName+'</h3><ul id=\"'+ulIDTeir+'\"></ul></div>');
										GE5P.trayColumnID = jQuery('#'+header.id);
									} else {
										if (GE5P.trayColumnID) {
											aria_posinsetTeir = aria_posinsetTeir + 1;
											GE5P.trayColumnID.find('ul').append('<li role=\"presentation\"><a id=\"'+header.id+'\" class=\"navTertiaryLink\" tabindex=\"'+GE5P.ge5p_tabIndexZone2+'\" href=\"'+header.url+'\" aria-label=\"'+header.displayName+'\" role=\"treeitem\" aria-setsize=\"'+subnavlinks.children.length+'\" aria-posinset=\"'+aria_posinsetTeir+'\" aria-level=\"3\">'+header.displayName+'</a></li>');
											if(header.windowLocation == 'Y') {
												// Link opens in new window
												jQuery('#'+header.id).attr("target", "_blank");
												var iconNewWindow = jQuery('<i class=\"cssIcon-open-window-b\"></i><span class=\"hidden-spoken\">'+GE5P.ge5p_newWindowText+'</span>');
												jQuery('#'+header.id).append(iconNewWindow);
											}
										}
									}
									if (aria_ownsTeir.length > 0) {
										aria_ownsTeir = aria_ownsTeir+" "+header.id;
									} else {
										aria_ownsTeir = header.id;
									}
								});
								jQuery('#'+ulIDTeir).attr('aria-owns',aria_ownsTeir);
							}
							// Check if Secondary Menu Item is Active from Page
							if(subnavlinks.code == GE5P.ge5p_globalNavDefaultSecondaryActive) {
								//subNavLinkID
								// We have a Tray so display
								jQuery('.ge5p_z2-nav-bar-subnav li').removeClass('active');
								jQuery('.ge5p_z2-nav-bar-subnav li').find('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').removeClass('active').hide();
								jQuery('#'+subNavLinkID).addClass('ge5p_z2-default-secondary-active');
							}
						});
						jQuery('#'+ulID).attr('aria-owns',aria_owns);
						var searchForm = '<li id=\"primary_Search\"><form action=\"\" name="searchForm" id="searchForm" method="get" role="search">'
							+ '  <fieldset> '
							+ '	  <input type="hidden" value="HOME" name="App_ID"> '
							+ '	  <input type="hidden" value="FALSE" name="autoSuggest"> '
							+ '	  <input type="hidden" value="FALSE" id="tabPressed" name="tabPressed"> '
							+ '	  <input type="text" title="Search" class="span3 ask_a_question_box ui-autocomplete-input" id="ge5p_search" name="q" placeholder="Search" autocomplete="off" aria-autocomplete="list" aria-haspopup="true"> '
							+ '	  <input type="submit" class="cssIcon-search" value="Search Submit Button" alt="Search Submit Button">'
							+ '	  <div class="autoSuggest" id="autoSuggestBox"></div>'
							+ '  </fieldset> '
							+ '</form></li> ';
						jQuery('#ge5p_z2-nav-bar').append(searchForm);
						// Close Primary Nav Trays if the search box has focus
						// We have a Placeholder "Search" attribute in the Search input box
						// We check if the browser supports this feature (not IE)
						var nativePlaceholderSupport = (function() {
                            var i = document.createElement('input');
                            return i.placeholder !== undefined;
                        })();
                        if (nativePlaceholderSupport) {
							// Placeholder text should already be visible!
						} else {
							// no placeholder support :(
							// fall back to a scripted solution
							jQuery('#ge5p_search').val('Search');
						}
						if(GE5P.ge5p_localLanguage) {
							if(GE5P.ge5p_localLanguage == 'en_US' || GE5P.ge5p_localLanguage == 'en' || GE5P.ge5p_localLanguage == 'en-US') {
								// Change Search placeholder to english
								jQuery('#ge5p_search').attr('placeholder', 'Search');
							} else {
								// Change Search placeholder to spanish
								jQuery('#ge5p_search').attr('placeholder', 'Buscar');
							}
						} else {
							// Change Search placeholder to english
							jQuery('#ge5p_search').attr('placeholder', 'Search');
						}
					}// End Support
				}// End Zone 2 Menu
				if(navZone == 'z7') {
					// Expected four sections - ge5p_z7_1, ge5p_z7_2, ge5p_z7_3, ge5p_z7_4
					if(element.nav_position == 'ge5p_z7_1') {
						// Branding Elements
						jQuery.each(element.children, function(key, value) {
							var liz7_1 = jQuery('<li><p><a tabindex=\"'+GE5P.ge5p_tabIndexZone7+'\"></a></p></li>');
							jQuery('#ge5p_z7-brand-elements-holder').append(liz7_1);
							jQuery("a",liz7_1).attr("href", value.url);
							jQuery("a",liz7_1).attr("id", value.id);
							// Check if display name is att.com or .net - adjust globe image
							if (value.displayName.split('.')[1] == 'com') {
								jQuery("a",liz7_1).append('<img width=\"133px\" height=\"24px\" alt=\"'+value.additionalLabel+'\" src=\"../../media/att/2011/global/nav/en_US/logoATTdotcom.png\"/*tpa=http://www.att.com/media/att/2011/global/nav/en_US/logoATTdotcom.png*/>');
								jQuery("p",liz7_1).attr('class','ge5p_z7-dotCom');
							}
							if (value.displayName.split('.')[1] == 'net') {
								jQuery("a",liz7_1).append('<img width=\"173px\" height=\"21px\" alt=\"'+value.additionalLabel+'\" src=\"../../media/att/2011/global/nav/en_US/logoATTdotnet.png\"/*tpa=http://www.att.com/media/att/2011/global/nav/en_US/logoATTdotnet.png*/>');
								jQuery("p",liz7_1).attr('class','ge5p_z7-dotNet');
							}
							if(value.hiddenText.length > 0) {
								// There is an hidden text element
								jQuery("a",liz7_1).append('<span class=\"hidden-spoken\">'+value.hiddenText+'</span>');
							}
                            if(value.windowLocation == 'Y') {
								// Link opens in new window
								jQuery("a",liz7_1).attr("target", "_blank");
								var newWindow = jQuery('<i class=\"cssIcon-open-window-b\"></i><span class=\"hidden-spoken\">'+GE5P.ge5p_newWindowText+'</span>');
								jQuery("a",liz7_1).append(newWindow);
							}
						});
					}
					if(element.nav_position == 'ge5p_z7_2') {
						// Link Farm/Key Links
						jQuery.each(element.children, function(key, value) {
							var liz7_2 = jQuery('<li><a tabindex=\"'+GE5P.ge5p_tabIndexZone7+'\"></a></li>'); // was role=\"menuitem\"  removed role="menubar" from #ge5p_z7-key-links-holder
							jQuery('#ge5p_z7-key-links-holder').append(liz7_2);
							jQuery("a",liz7_2).text(value.displayName);
							jQuery("a",liz7_2).attr("href", value.url);
							jQuery("a",liz7_2).attr("id", value.id);
							if(value.windowLocation == 'Y') {
								// Link opens in new window
								jQuery("a",liz7_2).attr("target", "_blank");
								var newWindow = jQuery('<i class=\"cssIcon-open-window-b\"></i><span class=\"hidden-spoken\">'+GE5P.ge5p_newWindowText+'</span>');
								jQuery("a",liz7_2).append(newWindow);
							}
							if(value.additionalLabel.length > 0) {
								// There is an additional Label
								jQuery("a",liz7_2).after(' '+value.additionalLabel+' ');
							}
							if(value.hiddenText.length > 0) {
								// There is an hidden text element
								jQuery("a",liz7_2).append('<span class=\"hidden-spoken\">'+value.hiddenText+'</span>');
							}
						});
					}
					if(element.nav_position == 'ge5p_z7_3') {
						// Copyright
						var copyrightVal = jQuery('#ge5p_z7-copyright-legal');
						copyrightVal.empty();
						jQuery.each(element.children, function(key, value) {
							var liz7_3 = jQuery('<div class=\"text-left\"><p><a tabindex=\"'+GE5P.ge5p_tabIndexZone7+'\"></a> <span></span><p></div>');
							copyrightVal.append(liz7_3);
							jQuery("span",liz7_3).html(value.additionalLabel);
							jQuery("a",liz7_3).html(value.displayName);
							jQuery("a",liz7_3).append('<i class=\"cssIcon-open-window-b\"></i><span class=\"hidden-spoken\">'+value.displayName+' link. '+GE5P.ge5p_newWindowText+' '+value.hiddenText+'</span>');
							jQuery("a",liz7_3).attr("href", value.url);
							jQuery("a",liz7_3).attr("id", value.id);
							if(value.windowLocation == 'Y') {
								// Link opens in new window
								jQuery("a",liz7_3).attr("target", "_blank");
								var newWindow = jQuery('<span class=\"hidden-spoken\">'+GE5P.ge5p_newWindowText+'</span>');
								jQuery("a",liz7_3).append(newWindow);
							}
						});
					}
					if(element.nav_position == 'ge5p_z7_4') {
						// Partners
						jQuery.each(element.children, function(key, value) {
							var liz7_4 = jQuery('<li><a tabindex=\"'+GE5P.ge5p_tabIndexZone7+'\"></a></li>');
							// Append li to ul partners
							jQuery('#ge5p_z7-partners-affiliates-awards ul').append(liz7_4);
							// Anchor Tag Data
							jQuery("a",liz7_4).html('<img src=\"'+value.image+'\" alt=\"'+value.hiddenText+'\"/><span class=\"hidden-spoken\">'+value.displayName+' link. '+value.hiddenText+'</span>');
							jQuery("a",liz7_4).attr("href", value.url);
							jQuery("a",liz7_4).attr("id", value.id);
							if(value.windowLocation == 'Y') {
								// Link opens in new window
								jQuery("a",liz7_4).attr("target", "_blank");
								var newWindow = jQuery('<span class=\"hidden-spoken\">'+GE5P.ge5p_newWindowText+'</span>');
								jQuery("a",liz7_4).append(newWindow);
							}
						});
					}
				} // End Zone 7 Menu
			}); // End each
			// Add Zip Code/Login information
			GE5P.userInfoData();
			// Remove Right Side Border in Tray for IE8
			if(GE5P.ge5p_is_msie8) {
				jQuery('#ge5p_z2-nav-bar .ge5p_z2-nav-bar-subnav .tray > div:last-child').css('border-right','medium none');
			}
			setTimeout(function(){
				var userInfoSpans = jQuery('#ge5p_z2-zipcode-inner').find('span');
				jQuery.each(userInfoSpans, function(key, value) {
					if (jQuery.trim(jQuery(this).html()).length > 0 ) {
						if(jQuery(this).children('a').is(':visible')) {
							GE5P.ge5p_theFirstVisibleIndex = key;
					        return false;
						}
					}
				});
				if(GE5P.ge5p_UA_isAndroid) {
					// Add ARIA roles/values to android devices
					//GE5P.androidCatoAria();
				}
				if(GE5P.ge5p_setTeslaLinksRelative) {
					GE5P.setHostRelative();
				}
				/* 
				 * Call function and redirect to Spanish Session if Local Storage is Spanish (es)
				 */
				GE5P.checkLanguagePreference();
			}, 800);
			// Add Skip Nav #wrapper back in for SHOP
			jQuery('#ge5p_z1-skip-navigation-link').attr('href','#wrapper');
			GE5P.ge5p_pageRepainted = true;
	},
	/* Function to reset the Body JS elements including ARIA attributes
	 * once the body is clicked and all the menu dropdowns are closed
	 **/
	bodyReset: function(event) {
        if(jQuery('.ge5p_z1-drop-down').hasClass('active') || jQuery('.ge5p_z2-nav-item').hasClass('active') || jQuery('.ge5p_z2-secondary-nav-el').parent().hasClass('active')) {
            $this = jQuery(event.target);
			/* Check if this was on a Z1 element */
			if($this.hasClass('ge5p_z1-drop-down') || $this.parents().hasClass('ge5p_z1-menu') || $this.parent().hasClass('ge5p_z1-tab')) {

			} else {
				if(jQuery('.ge5p_z1-language-drop-down > div.ge5p_z1-menu').is(':visible')) {
					// Check if this is a click event Lang sub menu is open
					if(event.type == 'click' || event.type == 'touchstart' || event.type == 'touchend') {
						GE5P.closeSegUtilNav();
					}
				} else {
					GE5P.closeSegUtilNav();
				}
			}
			/* Check if this was on a Z2 element */
			if($this.hasClass('ge5p_z2-primary-nav-el') || $this.hasClass('ge5p_z2-secondary-nav-el') || $this.parents().hasClass('ge5p_z2-nav-bar-subnav')) {
				// On Primary Item
			} else {
				GE5P.closePrimaryNavTrays();
				//Reset active elements
				jQuery('.ge5p_z2-nav-bar-subnav li').removeClass('active');
				// Check if there are default Primary & Secondary Default elements - then show
				if(GE5P.ge5p_globalNavDefaultPrimaryActive.length > 0) {
					// Show the active Primary nav element
					$thisPrimaryActive = jQuery('.ge5p_z2-primary-nav-el.ge5p_z2-default-primary-active');
					$thisPrimaryActive.parent().addClass('active');
					$thisPrimaryActive.next().show();
					$thisPrimaryActive.attr('aria-expanded','true').attr('aria-selected','true');
					// Tray 1 height is Primary Nav Height + Primary Nav li.active
					var heightTray1Open = GE5P.heightPrimaryNavClosed + jQuery('#ge5p_z2-nav-bar li.active').height();
					// Increase height of PrimaryNav to show tray
					GE5P.animatePrimaryTray(heightTray1Open);
				}
			}
			if(GE5P.ge5p_supportsTouch) {
				// // Touch Device so add hidden-spoken
				jQuery('span.hidden-spoken').text('');
			}
			// Rename the Secondary Nav Link to show Highlighting
			GE5P.secondaryNavHighlightAdd();
			//event.stopImmediatePropagation(); /*This is affecting bootstrap js components */
			//event.preventDefault();
		}
	},
	/* Function to set the ARIA attributes once a main
	 * menu item with a sub menu is clicked
	 **/
	ariaEventSegUtilNav: function(event) {
		$this = jQuery(event.target);
		// Reset other Seg Util Nav ARIA attributes
		//jQuery('.ge5p_z1-menu ul').attr('aria-expanded', 'false').attr('aria-hidden', 'true');
		var x = setTimeout(function(){
			//$this.next().find('ul').attr('aria-expanded', 'true').attr('aria-hidden', 'false');
		}, GE5P.ge5p_dropDownSetTimeout);
	},
	/********************/
	/* Zone 1 Functions */
	/********************/
	/* Function to associate the key event
	 * with a Seg Util Nav
	 **/
	keyboardSegUtilNavAction: function(event) {
		$this = jQuery(event.target);
		$thisID = $this.attr('id');
		var keyCodeSegUtilNav = event.which;
		if (keyCodeSegUtilNav === 9 && event.shiftKey) {
			 if ($this.parent().hasClass('ge5p_z1-tab')) {
                 // Main menu item
                 jQuery('.ge5p_z1-drop-down').removeClass('active');
                 jQuery('.ge5p_z1-menu').hide();
                 setTimeout(function() {
                     $thisFocus = jQuery('#ge5p_z1-global-nav-container a:focus');
                     if ($thisFocus.hasClass('ge5p_z1-drop-down')) {
                         // Title element
                         // Check if Active - if so, move to Sub Menu
                         if ($thisFocus.parent().hasClass('active')) {
                             // Move to first sub menu item
                             $thisFocus.next().addClass('active').show();
                             $thisFocus.next().find('a').first().focus();
                         } else {
                            // First, remove "active" from all elements
                             jQuery('.ge5p_z1-drop-down').removeClass('active');
                             jQuery('.ge5p_z1-menu').hide();
                             // Add class "active" to a and menu to turn background white
                             $thisFocus.addClass('active');
                             $thisFocus.parent().find('div.ge5p_z1-menu').show();
                             // Reset ARIA
                             jQuery('.ge5p_z1-drop-down').attr('aria-selected', 'false').attr('aria-expanded', 'false');
                             $thisFocus.attr('aria-selected', 'true').attr('aria-expanded', 'true');
							 $this.parent().prev().find('div').find('a').focus();
                         }
                     }
                 }, GE5P.ge5p_dropDownSetTimeout);
             }

		} else {
			switch(keyCodeSegUtilNav) {
				case 9: // Tab Key
					GE5P.ge5pZ1_navigate(event,'tab');
				break;
				case 13: // Enter key
					GE5P.ge5pZ1_navigate(event,'enter');
				break;
				case 38: // Up Arrow
					GE5P.ge5pZ1_navigate(event,'up');
				break;
				case 40: // Down Arrow
					GE5P.ge5pZ1_navigate(event,'down');
				break;
				case 32: // Space bar
				break;
				case 27: // Escape key
					GE5P.ge5pZ1_navigate(event,'escape');
					// Check if any menu items are open, if so, close
					GE5P.closeSegUtilNav();
				break;
			}
		}
	},
	/* Function to listen to keyboard action on Global Nav
	 **/
	ge5pZ1_navigate: function(event,direction) {
		$this = jQuery(event.target);
		$thisID = $this.attr('id');
		key = event.which;
		switch(direction) {
			case 'tab':
				setTimeout(function() {
					$thisFocus = jQuery('#ge5p_z1-global-nav-container a:focus');
					// Check if Title link or Sub Menu
					if ($thisFocus.hasClass('ge5p_z1-drop-down')) {
						// Title element
						// First, remove "active" from all elements
						jQuery('.ge5p_z1-drop-down').removeClass('active');
						jQuery('.ge5p_z1-menu').hide();
						// Add class "active" to a and menu to turn background white
						$thisFocus.addClass('active');
						$thisFocus.parent().find('div.ge5p_z1-menu').show();
						// Reset ARIA
						//jQuery('.ge5p_z1-drop-down').attr('aria-selected', 'false').attr('aria-expanded', 'false');
						jQuery('.ge5p_z1-drop-down').attr('aria-selected', 'false').attr('aria-expanded', 'false');
						$thisFocus.attr('aria-selected', 'true').attr('aria-expanded', 'true');
					} else {
						// Check for Regular Title Link
						if ($thisFocus.parent().hasClass('ge5p_z1-tab')) {
							jQuery('.ge5p_z1-drop-down').removeClass('active');
							jQuery('.ge5p_z1-menu').hide();
							// Reset ARIA
							jQuery('.ge5p_z1-drop-down').attr('aria-selected', 'false').attr('aria-expanded', 'false');
						} else {
							// Submenu link
							// Check if inside Language sub menu
							if ($this.parent().parent().hasClass('ge5p_z1-language_submenu')) {
								$thisIndex = $this.parent().index();
								$thisAllIndex = $this.parent().parent().find('li').length;
								$thisAllIndex = $thisAllIndex - 2;
								if ($thisIndex <= $thisAllIndex) {
									// Keep going
								} else {
									// End of Language link so close
									jQuery('.ge5p_z1-drop-down').removeClass('active');
									jQuery('.ge5p_z1-menu').hide();
									// Reset ARIA
									jQuery('.ge5p_z1-drop-down').attr('aria-selected', 'false').attr('aria-expanded', 'false');
								}
							} else {
							}
						}
					}
				}, GE5P.ge5p_dropDownSetTimeout);
				break;
			case 'enter':
				// No code needed
				break;
			case 'up':
				event.preventDefault();
				setTimeout(function() {
					$thisFocus = jQuery('#ge5p_z1-global-nav-container a:focus');
					if ($thisFocus.hasClass('ge5p_z1-drop-down')) {
						// Title, so do nothing
					}
					if($this.parent().hasClass('ge5p_z1-menuitem')) {
						$thisIndex = $this.parent().index();
						$thisAllIndex = $this.parent().parent().find('li').length;
						$thisAllIndex = $thisAllIndex - 1;
						if ($thisIndex == 0) {
							// Wrap around to bottom
							$this.parent().parent().find('a').last().focus();
							event.preventDefault();
						} else {
							// Check if there is a divider hr
							if ($this.parent().prev().find('hr').length) {
								// hr divider prev prev
								$this.parent().prev().prev().find('a').focus();
							} else {
								// Continue up sub menu
								$this.parent().prev().find('a').first().focus();
								event.preventDefault();
							}
						}
					}
				}, GE5P.ge5p_dropDownSetTimeout);
				break;
			case 'down':
				event.preventDefault();
				setTimeout(function() {
					$thisFocus = jQuery('#ge5p_z1-global-nav-container a:focus');
					// Check if Title link or Sub Menu
					if ($thisFocus.hasClass('ge5p_z1-drop-down')) {
						// Title, so move to first element in sub menu
						// Check if there is 1 or more elements in the dropdown
						var countDDElements = $thisFocus.next().find('a:visible').length;
						if(countDDElements == 1) {
							$thisFocus.next().find('a:visible').focus();
						} else {
							$thisFocus.next().find('a').first().focus();
						}
					} else {
						// Check if this is a Menu Item
						if($this.parent().hasClass('ge5p_z1-menuitem')) {
							// Menu visible - check Index
							$thisIndex = $this.parent().index();
							$thisAllIndex = $this.parent().parent().find('li').length;
							$thisAllIndex = $thisAllIndex - 1;
							if($thisIndex < $thisAllIndex) {
								// Check if there is a divider hr
								if($this.parent().next().find('hr').length) {
									// hr divider next next
									$this.parent().next().next().find('a').focus();
								} else {
									$this.parent().next().find('a').focus();
								}
								event.preventDefault();
							} else {
								// loop back arround
								$this.parent().parent().find('a').first().focus();
								event.preventDefault();
							}
						}
					}
				}, GE5P.ge5p_dropDownSetTimeout);
				break;
		}
    },
	showSegUtilNav: function(event) {
		if(GE5P.ge5p_UA_isAndroid) {
			event.preventDefault();
		}
		
		$this = jQuery(event.target);
		if ($this.hasClass('ge5p_z1-language-drop-down') || $this.parent().hasClass('ge5p_z1-menuitem')) {
		} else {
			jQuery('.ge5p_z1-drop-down').removeClass('active');
			jQuery('.ge5p_z1-menu').hide();
			jQuery('.ge5p_z1-drop-down').attr('aria-selected', 'false').attr('aria-expanded', 'false');
			// show menu
			if ($this.hasClass('ge5p_z1-drop-down')) {
				$this.addClass('active');
				$this.next().addClass('active').show();
				$this.attr('aria-selected', 'true').attr('aria-expanded', 'true');
				if(GE5P.ge5p_supportsTouch) {
					// // Touch Device so add hidden-spoken
					$this.parent().find('span.hidden-spoken').text('Selected Menu Expanded');
				}
			}
			event.preventDefault();
		}
	},
	toggleSegUtilNav: function(event) {
		//event.preventDefault();
		/*
		if(GE5P.ge5p_supportsTouch) {
			event.preventDefault();
		*/
		$this = jQuery(event.target);
		GE5P.closePrimaryNavTrays();
		//event.stopPropagation();
        //event.preventDefault();
		// Check if menu title is active
		if($this.hasClass('ge5p_z1-language-drop-down')) {
			// Language Menu touched
			if ($this.hasClass('active')) {
				// Lang menu open
				$this.removeClass('active');
				$this.next().removeClass('active').hide();
				jQuery('.ge5p_z1-drop-down').attr('aria-selected', 'false').attr('aria-expanded', 'false');
				if(GE5P.ge5p_supportsTouch) {
					// // Touch Device so add hidden-spoken
					$this.parent().find('span.hidden-spoken').text('');
				}
			} else {
				// Lang menu closed, so open
				jQuery('.ge5p_z1-drop-down').removeClass('active');
				jQuery('.ge5p_z1-menu').hide();
				$this.addClass('active');
				$this.next().addClass('active').show();
				jQuery('.ge5p_z1-drop-down').attr('aria-selected', 'false').attr('aria-expanded', 'false');
				$this.attr('aria-selected', 'true').attr('aria-expanded', 'true');
				if(GE5P.ge5p_supportsTouch) {
					// // Touch Device so add hidden-spoken
					$this.parent().find('span.hidden-spoken').text('Selected Menu Expanded');
				}
			}
			return false;
		} else {
			// Other Menu

			if ($this.hasClass('active')) {
				// Continue to link
				$thisHREF = $this.arrt('href');
				$thisBlank = $this.attr('target');
				if($thisBlank) {
					window.open($thisHREF, '_blank');
				} else {
					window.location.replace($thisHREF);
				}
			} else {
				// Clear all sub menus
				jQuery('.ge5p_z1-drop-down').removeClass('active');
				jQuery('.ge5p_z1-menu').hide();
				// Open new sub menu
				$this.addClass('active');
				$this.next().addClass('active').show();
				jQuery('.ge5p_z1-drop-down').attr('aria-selected', 'false').attr('aria-expanded', 'false');
				$this.attr('aria-selected', 'true').attr('aria-expanded', 'true');
				if(GE5P.ge5p_supportsTouch) {
					// // Touch Device so add hidden-spoken
					$this.parent().find('span.hidden-spoken').text('Selected Menu Expanded');
				}
				return false;
			}
		}
	},
	toggleSegUtilLangNav: function(event) {
		$this = jQuery(event.target);
		if ($this.hasClass('active')) {
			// Lang menu open
			$this.removeClass('active');
			$this.next().removeClass('active').hide();
			jQuery('.ge5p_z1-drop-down').attr('aria-selected', 'false').attr('aria-expanded', 'false');
			if(GE5P.ge5p_supportsTouch) {
				// // Touch Device so add hidden-spoken
				$this.parent().find('span.hidden-spoken').text('');
			}
			event.preventDefault();
		} else {
			// Lang menu closed, so open
			jQuery('.ge5p_z1-drop-down').removeClass('active');
			jQuery('.ge5p_z1-menu').hide();
			$this.addClass('active');
			$this.next().addClass('active').show();
			jQuery('.ge5p_z1-drop-down').attr('aria-selected', 'false').attr('aria-expanded', 'false');
			$this.attr('aria-selected', 'true').attr('aria-expanded', 'true');
			if(GE5P.ge5p_supportsTouch) {
				// // Touch Device so add hidden-spoken
				$this.parent().find('span.hidden-spoken').text('Selected Menu Expanded');
			}
			event.preventDefault();
		}
		return false;
	},
	closeSegUtilNav: function() {
		jQuery('.ge5p_z1-drop-down').removeClass('active');
		jQuery('.ge5p_z1-menu').hide();
		jQuery('.ge5p_z1-drop-down').attr('aria-selected', 'false').attr('aria-expanded', 'false');
		if(GE5P.ge5p_supportsTouch) {
			// // Touch Device so add hidden-spoken
			jQuery('span.hidden-spoken').text('');
		}
	},
	/* ZONE 2 JS */
	keyboardUserInfo: function(event) {
		var $this = jQuery(event.target);
		var keyCodeUserInfo = event.which;
		$thisIndex = $this.parent(':visible').index();
		if (keyCodeUserInfo === 9 && event.shiftKey) {
			if($thisIndex == GE5P.ge5p_theFirstVisibleIndex) {
				event.preventDefault();
				// Reset Seg Menu
				jQuery('.ge5p_z1-drop-down').removeClass('active');
				jQuery('.ge5p_z1-menu').hide();
				jQuery('.ge5p_z1-drop-down').attr('aria-selected', 'false').attr('aria-expanded', 'false');
				// Open Language sub menu
				jQuery('#ge5p_z1-language-drop-down-link').addClass('active');
				jQuery('#ge5p_z1-language-drop-down-link').next().addClass('active').show();
				jQuery('#ge5p_z1-language-drop-down-link').attr('aria-selected', 'true').attr('aria-expanded', 'true');
				jQuery('.ge5p_z1-language-drop-down').find('a').last().focus();
			}
		}
	},
	primaryNavActionHover: function(event) {
		event.preventDefault();
		$this = jQuery(event.target); // Ensures lowest element in DOM was clicked - anchor tag
		if($this.parent().hasClass('ge5p_z2-primaryLogo')) {
			GE5P.closePrimaryNavTrays();
			return true;
		} else {
			if($this.parent().hasClass('active')) {
				// Follow to the URL of the link
				return true;
			} else {
				// Close Seg/Util Nav
				GE5P.closeSegUtilNav();
				// Tray open
				// Tray 1 = Primary Tray Open
				// Tray 2 = Secondary Tray Open
				// Remove "active" from all li's
				jQuery('.ge5p_z2-nav-item').removeClass('active');
				jQuery('.ge5p_z2-nav-bar-subnav li').removeClass('active');
				jQuery('.ge5p_z2-nav-bar-subnav li').find('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').removeClass('active').hide();
				// Add class "active" to li to turn background white
				$this.parent().addClass('active');
				$this.attr('aria-expanded','true').attr('aria-selected','true');
				// Tray 1 height is Primary Nav Height + Primary Nav li.active
				var heightTray1Open = GE5P.heightPrimaryNavClosed + jQuery('#ge5p_z2-nav-bar li.active').height();
				// Increase height of PrimaryNav to show tray
				GE5P.animatePrimaryTray(heightTray1Open);
				return false;
			}
		}
	},
	primaryNavActionTouch: function(event) {
		if(GE5P.ge5p_UA_isAndroid) {
			//event.preventDefault();
		}
		$this = jQuery(event.target); // Ensures lowest element in DOM was clicked - anchor tag
		if($this.parent().hasClass('ge5p_z2-primaryLogo')) {
			GE5P.closePrimaryNavTrays();
			return true;
		} else {
			event.preventDefault();
			if($this.parent().hasClass('active')) {
				// Follow to the URL of the link
				$thisHREF = $this.attr('href');
				$thisBlank = $this.attr('target');
				if($thisBlank) {
					window.open($thisHREF, '_blank');
				} else {
					window.location.replace($thisHREF);
				}
			} else {
				// Close Seg/Util Nav
				GE5P.closeSegUtilNav();
				// Tray open
				// Tray 1 = Primary Tray Open
				// Tray 2 = Secondary Tray Open
				// Remove "active" from all li's
				jQuery('.ge5p_z2-nav-item').removeClass('active');
				jQuery('.ge5p_z2-nav-bar-subnav li').removeClass('active');
				jQuery('.ge5p_z2-nav-bar-subnav li').find('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').removeClass('active').hide();
				// Add class "active" to li to turn background white
				$this.parent().addClass('active');
				$this.attr('aria-expanded','true').attr('aria-selected','true');
				if(GE5P.ge5p_supportsTouch) {
					// // Touch Device so add hidden-spoken
					$this.parent().find('span.hidden-spoken').text('Selected Menu Expanded');
				}
				// Tray 1 height is Primary Nav Height + Primary Nav li.active
				var heightTray1Open = GE5P.heightPrimaryNavClosed + jQuery('#ge5p_z2-nav-bar li.active').height();
				// Increase height of PrimaryNav to show tray
				GE5P.animatePrimaryTray(heightTray1Open);
				return false;
			}
		}
	},
	primaryNavActionFocus: function(event) {
		event.preventDefault();
		$this = jQuery(event.target); // Ensures lowest element in DOM was clicked - anchor tag
		if($this.parent().hasClass('ge5p_z2-primaryLogo')) {
			GE5P.closePrimaryNavTrays();
			return true;
		} else {
			// Close Seg/Util Nav
			GE5P.closeSegUtilNav();
			// Tray open
			// Tray 1 = Primary Tray Open
			// Tray 2 = Secondary Tray Open
			// Remove "active" from all li's
			jQuery('.ge5p_z2-nav-item').removeClass('active');
			jQuery('.ge5p_z2-nav-bar-subnav li').removeClass('active');
			jQuery('.ge5p_z2-nav-bar-subnav li').find('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').removeClass('active').hide();
			// Add class "active" to li to turn background white
			$this.parent().addClass('active');
			$this.attr('aria-expanded','true').attr('aria-selected','true');
			// Tray 1 height is Primary Nav Height + Primary Nav li.active
			var heightTray1Open = GE5P.heightPrimaryNavClosed + jQuery('#ge5p_z2-nav-bar li.active').height();
			// Increase height of PrimaryNav to show tray
			GE5P.animatePrimaryTray(heightTray1Open);
			return false;
		}
	},
	/*
	 *
	 * New Primary Nav Actions
	 *
	 */
	primaryNavActionEvent: function(event) {
		if(GE5P.ge5p_UA_isAndroid) {
			//event.preventDefault();
		}
		$this = jQuery(event.target); // Ensures lowest element in DOM was clicked - anchor tag
		// This finction captures mounsenter/mouseleave keydown focusin in that order
		// First check what action
		var eventType = event.type;
		switch(eventType) {
			case "mouseenter":
				if($this.parent().hasClass('ge5p_z2-primaryLogo')) {
					GE5P.closePrimaryNavTrays();
					return true;
				} else {
					if($this.parent().hasClass('active')) {
						// Follow to the URL of the link
						return true;
					} else {
						// Close Seg/Util Nav
						GE5P.closeSegUtilNav();
						// Tray open
						// Tray 1 = Primary Tray Open
						// Tray 2 = Secondary Tray Open
						// Remove "active" from all li's
						jQuery('.ge5p_z2-nav-item').removeClass('active');
						jQuery('.ge5p_z2-nav-bar-subnav li').removeClass('active');
						jQuery('.ge5p_z2-nav-bar-subnav li').find('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').removeClass('active').hide();
						// Add class "active" to li to turn background white
						$this.parent().addClass('active');
						$this.attr('aria-expanded','true').attr('aria-selected','true');
						// Tray 1 height is Primary Nav Height + Primary Nav li.active
						var heightTray1Open = GE5P.heightPrimaryNavClosed + jQuery('#ge5p_z2-nav-bar li.active').height();
						// Increase height of PrimaryNav to show tray
						GE5P.animatePrimaryTray(heightTray1Open);
						return false;
					}
				}
				break;
			case "keydown":
				GE5P.keyCodePrimaryNav = event.which;
				$thisIndex = $this.parent().index();
				$primaryNavAllIndex = jQuery('#ge5p_z2-nav-bar > li').length - 2; // Because of Logo & Search - should be 3
				if (GE5P.keyCodePrimaryNav === 9 && event.shiftKey) {
					// Inside Primary Nav - Either Logo/Shop/myAT&T/Support
					// Check if this is the First element
					if($thisIndex == 0) {
						// Send focus to Login button
						jQuery('#ge5p_z2-user-auth').focus();
						event.preventDefault();
					} else if ($thisIndex == 1) {
						// Send focus to Logo
						jQuery('#primaryLogo').find('a').focus();
						event.preventDefault();
					} else {
						// Move to Previous Primary nav element
						// Then down to the Secondary Tray and focus on last element
						$this.parent().prev().find('a').focus();
						jQuery('.ge5p_z2-secondary-nav-el').parent('.active').find('li').last().find('a').focus();
						event.preventDefault();
					}
				} else {
				
					switch(GE5P.keyCodePrimaryNav) {
						case 13: // enter
							if($this.parent().hasClass('ge5p_z2-primaryLogo')) {
								return true;
							} else {
								// Check if Primary menu is open, if so, close
								if(jQuery('.ge5p_z2-nav-bar-subnav').is(':visible')) {
									// Close
									return true;
								} else {
									event.preventDefault();
									// Open Primary Tray
									GE5P.openPrimaryNavTray(event);
								}
							}
							break;
						case 39: // right
							event.preventDefault();
							if($thisIndex == $primaryNavAllIndex) {
								jQuery('#ge5p_z2-nav-bar li').first().next().find('a').first().focus();
							} else {
								$this.parent().next().find('a').first().focus();
							}
							break;
						case 37: // left
							event.preventDefault();
							if($thisIndex == 1) {
								jQuery('#ge5p_z2-nav-bar li').last().prev().find('a').first().focus();
							} else {
								$this.parent().prev().find('a').first().focus();
							}
							break;
						case 38: // up
							event.preventDefault();
							break;
						case 40: // down
							event.preventDefault();
							// Check if Primary is active
							if($this.parent().hasClass('active')) {
								// If yes, move to First Secondary Nav element
								GE5P.secondaryNavHighlightRemove();
								$this.next().find('a').first().focus();
								$this.next().find('a').first().parent().addClass('active');
								GE5P.openSecondaryNavTray(event);
								event.preventDefault();
							} else {
								// Primary Menu is not active and Secondary Tray is not open
								GE5P.openPrimaryNavTray(event);
							}
							break;
					}
				}
				break;
			case "focusin":
				GE5P.primaryNavActionFocus(event);			
				break;
		}
	},
	/*
	 *
	 * New Secondary Nav Actions
	 *
	 */
	secondaryNavActionEvent: function(event) {
		if(GE5P.ge5p_UA_isAndroid) {
			//event.preventDefault();
		}
		$this = jQuery(event.target); // Ensures lowest element in DOM was clicked - anchor tag
		// This finction captures mounsenter/mouseleave keydown focusin in that order
		// First check what action
		var eventType = event.type;
		switch(eventType) {
			case "mouseenter":
				//Check if element has a sub menu
				if($this.hasClass('ge5p_z2-nav-bar-subnav-has-tray')) {
					// First rename the Secondary Nav Link IF it is Highlighted to remove highlighting
					GE5P.secondaryNavHighlightRemove();
					// We have a Tray so display
					jQuery('.ge5p_z2-nav-bar-subnav li').removeClass('active');
					jQuery('.ge5p_z2-nav-bar-subnav li').find('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').removeClass('active').hide();
					$this.parent().addClass('active');
					$this.next().addClass('active').show();
					var heightTray1Open = GE5P.heightPrimaryNavClosed + jQuery('#ge5p_z2-nav-bar li.active').height();
					var heightTray2Open = heightTray1Open + jQuery('.ge5p_z2-nav-bar-subnav li div.tray.active').height();
					if(GE5P.ge5p_is_msie8) {
						heightTray2Open = heightTray2Open + 1;
					}
					GE5P.animatePrimaryTray(heightTray2Open);
					//Make all column divs the height of tray
					heightOfActiveTray = jQuery('.tray.active').height();
					jQuery('.ge5p_z2-nav-bar-subnav').find('div.tray.active div').each(function(){
						jQuery(this).height(heightOfActiveTray);
                        //jQuery(this).innerHeight(heightOfActiveTray);
					});
					$this.attr('aria-expanded','true').attr('aria-selected','true');	
				} else {
					// Regular link in the Secondary menu so close the Tray
					$this.attr('aria-selected','true');
					GE5P.closeSecondaryNavTray(event);
				}
			break;
			case "keydown":
				GE5P.keyCodePrimaryNav = event.which;
				if (GE5P.keyCodePrimaryNav === 9 && event.shiftKey) {
					// Treat Shift+Tab as reverse Tab
					$thisIndex = $this.parent().index();
					$thisAllIndex = $this.parent().parent().find('li').length;
					$thisAllIndex = $thisAllIndex - 1;
					// Move focus to its Primary nav element if on first Secondary element
					if($thisIndex == 0) {
						// First Secondary Tray element
						jQuery('.ge5p_z2-nav-item.active').find('a').first().focus();
						event.preventDefault();
					} else {
						// Check if Prev Secondary nav element has a tray
						if($this.parent().prev().find('a').hasClass('ge5p_z2-nav-bar-subnav-has-tray')) {
							jQuery('.ge5p_z2-nav-bar-subnav li').removeClass('active');
							jQuery('.ge5p_z2-nav-bar-subnav li').find('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').removeClass('active').hide();
							$this.parent().prev().addClass('active');
							$this.parent().prev().find('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').addClass('active').show();
							var heightTray1Open = GE5P.heightPrimaryNavClosed + jQuery('#ge5p_z2-nav-bar li.active').height();
							var heightTray2Open = heightTray1Open + jQuery('.ge5p_z2-nav-bar-subnav li div.tray.active').height();
							GE5P.animatePrimaryTray(heightTray2Open);
							$this.parent().prev().find('div > div:last-child').find('a').focus();
							event.preventDefault();
						}
					}
				} else {
					// Another key was pressed
					// Check for Tab, Enter, Down, Up, left & Right keys
					switch(GE5P.keyCodePrimaryNav) {
						case 27: // escape
							break;
						case 13: // enter
							if($this.hasClass('ge5p_z2-nav-bar-subnav-has-tray')) {
								return true;
							}
							break;
						case 39: // right
							event.preventDefault();
							// If Secondary Nav
							if($this.hasClass('ge5p_z2-secondary-nav-el')) {
								$thisIndex = $this.parent().index();
								$thisAllIndex = $this.parents('.ge5p_z2-nav-item.active').find('.ge5p_z2-nav-bar-subnav > li').length;
								$thisAllIndex = $thisAllIndex - 1;
								$nodes = $this.parents('.ge5p_z2-nav-item.active').find('.ge5p_z2-nav-bar-subnav > li');
								// Check if the last index element, if yes, start over
								if($thisIndex == $thisAllIndex) {
									$nodes.eq(0).find('a.ge5p_z2-secondary-nav-el').focus();
								} else {
									$nodes.eq(++$thisIndex).find('a.ge5p_z2-secondary-nav-el').focus();
								}
							}
							break;
						case 37: // left
							event.preventDefault();
							// If Primary Nav
							
							// If Secondary Nav
							if($this.hasClass('ge5p_z2-secondary-nav-el')) {
								$thisIndex = $this.parent().index();
								$thisAllIndex = $this.parents('.ge5p_z2-nav-item.active').find('.ge5p_z2-nav-bar-subnav > li').length;
								$thisAllIndex = $thisAllIndex - 1;
								$nodes = $this.parents('.ge5p_z2-nav-item.active').find('.ge5p_z2-nav-bar-subnav > li');
								// Check if the last index element, if yes, start over
								if($thisIndex == 0) {
									$nodes.eq($thisAllIndex).find('a.ge5p_z2-secondary-nav-el').focus();
								} else {
									$nodes.eq(--$thisIndex).find('a.ge5p_z2-secondary-nav-el').focus();
								}
							}
							break;
						case 38: // up
							// Find Secondary Tray element and focus on
							jQuery('.ge5p_z2-nav-item.active').find('a').first().focus();
							event.preventDefault();
							break;
						case 40: // down
							event.preventDefault();
							// This Elements Index (place in the list);
							$thisIndex = $this.parent().index();
							// All Elements in the list (length)
							$thisAllIndex = $this.parents('.ge5p_z2-nav-item.active').find('.ge5p_z2-nav-bar-subnav > li').length;
							$thisAllIndex = $thisAllIndex - 1;
							// Check if Secondary Title is active
							if($this.parent().hasClass('active')) {
								// If yes, move to Tertiary Element
								// Check if this is the first
								GE5P.secondaryNavActionFocus(event);
								$this.next().find('a').first().focus();
								GE5P.secondaryNavHighlightRemove();
								// Make sure to put the Secondary Nav Link with a Highlight
								// Future Code Here
							} else {
								// If not, open Secondary
								// Check if link has Secondary Tray
								if($this.hasClass('ge5p_z2-nav-bar-subnav-has-tray')) {
									GE5P.secondaryNavActionFocus(event);
									$this.next().find('a').first().focus();
								} else {
									// Check if this is the last element
									if($thisIndex < $thisAllIndex) {
										// Move to next element in Secondary Nav
										$this.parent().next().find('a').focus();
									} else {
										// Reached the end of the Secondary Nav Elements
										// Move up DOM to Primary Level
										jQuery('#ge5p_z2-nav-bar').find('li.active').next().find('a').first().focus();
									}
								}
							}
							break;
					}
				}
			break;
			case "focusin":
				GE5P.secondaryNavActionEventFocus(event);
			break;
		}
	},
	secondaryNavActionEventFocus: function(event) {
		$this = jQuery(event.target); // Ensures lowest element in DOM was clicked - anchor tag
		//Check if element has a sub menu
		if($this.hasClass('ge5p_z2-nav-bar-subnav-has-tray')) {
			// First rename the Secondary Nav Link IF it is Highlighted to remove highlighting
			GE5P.secondaryNavHighlightRemove();
			// We have a Tray so display
			jQuery('.ge5p_z2-nav-bar-subnav li').removeClass('active');
			jQuery('.ge5p_z2-nav-bar-subnav li').find('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').removeClass('active').hide();
			$this.parent().addClass('active');
			$this.next().addClass('active').show();
			var heightTray1Open = GE5P.heightPrimaryNavClosed + jQuery('#ge5p_z2-nav-bar li.active').height();
			var heightTray2Open = heightTray1Open + jQuery('.ge5p_z2-nav-bar-subnav li div.tray.active').height();
			if(GE5P.ge5p_is_msie8) {
				heightTray2Open = heightTray2Open + 1;
			}
			GE5P.animatePrimaryTray(heightTray2Open);
			//Make all column divs the height of tray
			heightOfActiveTray = jQuery('.tray.active').height();
			jQuery('.ge5p_z2-nav-bar-subnav').find('div.tray.active div').each(function(){
				jQuery(this).height(heightOfActiveTray);
			});
			$this.attr('aria-expanded','true').attr('aria-selected','true');	
		} else {
			$thisNext = $this.next();
			if($thisNext.hasClass('tray')) {
			} else {
				// Regular link in the Secondary menu so close the Tray
				$this.attr('aria-selected','true');
				GE5P.closeSecondaryNavTray(event);
			}
		}
	},
	/*
	 *
	 * New Tertiary Nav Actions
	 *
	 */
	tertiaryNavActionEvent: function(event) {
		$this = jQuery(event.target); // Ensures lowest element in DOM was clicked - anchor tag
		GE5P.keyCodePrimaryNav = event.which;
		if (GE5P.keyCodePrimaryNav === 9 && event.shiftKey) {
			// Treat Shift+Tab as reverse Tab
			// Check if this is a Tertiary Nav
			if($this.hasClass('navTertiaryLink')) {
				$thisIndex = $this.parent().index(); // like 2
				$thisAllIndex = $this.parent().parent().find('li').length; // Like 5 links
				$thisAllIndex = $thisAllIndex - 1; // Adjust for index
				$thisDivIndex = $this.parentsUntil('.tray').closest('div').index();
				$tertiaryDivsIndex = $this.parents('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').find('div').length; // This could be like 5 divs
				$tertiaryDivsIndex = $tertiaryDivsIndex - 1;
				if($thisIndex == 0) {
					if($thisDivIndex == 0) {
						// At beginning Div
						jQuery('ul.ge5p_z2-nav-bar-subnav').find('li.active').find('a').first().focus();
						event.preventDefault();
					} else {
						// Move to previous Div and focus on first element
						$this.parentsUntil('.tray').closest('div').prev().find('a').last().focus();
						event.preventDefault();
					}
				} else {
					// Move up an element
					$this.parent().prev().find('a').focus();
					event.preventDefault();
				}
			}	
		} else {
			// Another key was pressed
			// Check for Tab, Enter, Down, Up, left & Right keys
			switch(GE5P.keyCodePrimaryNav) {
				case 27: // escape
					//If at Tertiary level
					if($this.hasClass('navTertiaryLink')) {
						// Move focus to its Primary Nav Element
						jQuery('.ge5p_z2-nav-item.active').find('a').first().focus();
						event.preventDefault();
						GE5P.closeSecondaryNavTray();
					}
					break;
				case 13: // enter
					// If Tertiary Nav
					return true;
					break;
				case 39: // right
				case 40: // down
					event.preventDefault();
					// If Tertiary Nav
					$tertiaryDivsIndex = $this.parents('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').find('div').length; // This could be like 5 divs
					$tertiaryDivNodes = $this.parents('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').find('div');
					$tertiaryDivsIndex = $tertiaryDivsIndex - 1;
					$thisIndex = $this.parent().index(); // Index we are currently at in the ul - like 0/1/2 etc.
					$thisDivIndex = $this.parentsUntil('.tray').closest('div').index();
					$tertiaryLength = $this.parent().parent().find('li').length; // This could be like 5 links
					$tertiaryNodes = $this.parent().parent().find('li');
					$tertiaryLength = $tertiaryLength - 1;
					$primaryNavAllIndex = jQuery('#ge5p_z2-nav-bar > li').length - 2; // Because of Logo & Search - should be 3
					$primaryNavActiveIndex = jQuery('#ge5p_z2-nav-bar').find('li.active').index();
					if($thisIndex < $tertiaryLength) {
						if($thisDivIndex < $tertiaryDivsIndex) {
							$this.parent().next().find('a').first().focus();
						} else {
							if($thisIndex < $tertiaryLength) {
								$this.parent().next().find('a').first().focus();
							} else {
								$this.parents('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').find('div').first().find('a').first().focus();
							}
						}
					} else {
						if($thisDivIndex < $tertiaryDivsIndex) {
							$this.parent().parent().parent().next().find('a').first().focus();
						} else {
							if(GE5P.keyCodePrimaryNav === 39) {
								// right key so move to beginning of section
								$this.parents('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').find('div').first().find('a').first().focus();
							} else {
								// down key so move to next sec nav element
								jQuery('.ge5p_z2-nav-bar-subnav li.active').next().find('a').first().focus();
							}
						}
					}
					
					break;
				case 37: // left
					event.preventDefault();
					$tertiaryDivsIndex = $this.parents('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').find('div').length; // This could be like 5 divs
					$tertiaryDivsIndex = $tertiaryDivsIndex - 1;
					$thisIndex = $this.parent().index(); // Index we are currently at in the ul - like 0/1/2 etc.
					$thisDivIndex = $this.parentsUntil('.tray').closest('div').index();
					$tertiaryLength = $this.parent().parent().find('li').length; // This could be like 5 links
					$tertiaryLength = $tertiaryLength - 1;
					$primaryNavAllIndex = jQuery('#ge5p_z2-nav-bar > li').length - 2; // Because of Logo & Search - should be 3
					$primaryNavActiveIndex = jQuery('#ge5p_z2-nav-bar').find('li.active').index();
					if($thisIndex == 0) {
						if($thisDivIndex == 0) {
							// At the first element of the first div, move focus to last div last element
							$this.parents('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').find('div').last().find('a').last().focus();
						} else {
							$this.parent().parent().parent().prev().find('a').last().focus();
						}
					} else {
						$this.parent().prev().find('a').first().focus();
					}
					break;
				case 38: // up
					$thisIndex = $this.parent().index(); // like 2
					$thisAllIndex = $this.parent().parent().find('li').length; // Like 5 links
					$thisAllIndex = $thisAllIndex - 1; // Adjust for index
					$thisDivIndex = $this.parentsUntil('.tray').closest('div').index();
					$tertiaryDivsIndex = $this.parents('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').find('div').length; // This could be like 5 divs
					$tertiaryDivsIndex = $tertiaryDivsIndex - 1;
					if($thisIndex == 0) {
						if($thisDivIndex == 0) {
							// At beginning Div
							jQuery('ul.ge5p_z2-nav-bar-subnav').find('li.active').find('a').first().focus();
							event.preventDefault();

						} else {
							// Move to previous Div and focus on first element
							$this.parentsUntil('.tray').closest('div').prev().find('a').last().focus();
							event.preventDefault();
						}
					} else {
						// Move up an element
						$this.parent().prev().find('a').focus();
						event.preventDefault();
					}
					break;
			}
		}
	},
	secondaryNavActionHover: function(event) {
		event.preventDefault();
		GE5P.secondaryNavActionFocus(event);
	},
	secondaryNavActionTouch: function(event) {
		event.preventDefault();
		$this = jQuery(event.target); // Ensures lowest element in DOM was clicked - anchor tag
		// Check if the Secondary Menu has a Tray
		if($this.hasClass('ge5p_z2-nav-bar-subnav-has-tray')) {
			// Check to see if the Tray is already open, if so, follow to url
			if($this.next().hasClass('active')) {
				// Follow to the URL of the link
				$thisHREF = $this.attr('href');
				$thisBlank = $this.attr('target');
				if($thisBlank) {
					window.open($thisHREF, '_blank');
				} else {
					window.location.replace($thisHREF);
				}
			} else {
				jQuery('.ge5p_z2-nav-bar-subnav li').removeClass('active');
				jQuery('.ge5p_z2-nav-bar-subnav li').find('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').removeClass('active').hide();
				$this.parent().addClass('active');
				$this.next().addClass('active').show();
				if(GE5P.ge5p_supportsTouch) {
					// // Touch Device so add hidden-spoken
					$this.parent().find('span.hidden-spoken').text('Selected Menu Expanded');
				}
				var heightTray1Open = GE5P.heightPrimaryNavClosed + jQuery('#ge5p_z2-nav-bar li.active').height();
				var heightTray2Open = heightTray1Open + jQuery('.ge5p_z2-nav-bar-subnav li div.tray.active').height();
				GE5P.animatePrimaryTray(heightTray2Open);
			}
		} else {
			// Follow to the URL of the link
			$thisHREF = $this.attr('href');
			window.location.replace($thisHREF);
		}
	},
	secondaryNavActionFocus: function(event) {
		event.preventDefault();
		var heightOfActiveTray = 0;
		$this = jQuery(event.target); // Ensures lowest element in DOM was clicked - anchor tag
		// Check if the Secondary Menu has a Tray
		if($this.hasClass('ge5p_z2-nav-bar-subnav-has-tray')) {
			// First rename the Secondary Nav Link IF it is Highlighted to remove highlighting
			GE5P.secondaryNavHighlightRemove();
			// We have a Tray so display
			jQuery('.ge5p_z2-nav-bar-subnav li').removeClass('active');
			jQuery('.ge5p_z2-nav-bar-subnav li').find('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').removeClass('active').hide();
			$this.parent().addClass('active');
			$this.next().addClass('active').show();
			var heightTray1Open = GE5P.heightPrimaryNavClosed + jQuery('#ge5p_z2-nav-bar li.active').height();
			var heightTray2Open = heightTray1Open + jQuery('.ge5p_z2-nav-bar-subnav li div.tray.active').height();
			if(GE5P.ge5p_is_msie8) {
				heightTray2Open = heightTray2Open + 1;
			}
			GE5P.animatePrimaryTray(heightTray2Open);
			//Make all column divs the height of tray
			heightOfActiveTray = jQuery('.tray.active').height();
			jQuery('.ge5p_z2-nav-bar-subnav').find('div.tray.active div').each(function(){
				jQuery(this).height(heightOfActiveTray);
			});
			$this.attr('aria-expanded','true').attr('aria-selected','true');
		} else {
			// Regular link in the Secondary menu so close the Tray
			$this.attr('aria-selected','true');
			GE5P.closeSecondaryNavTray(event);
			
			//jQuery('.ge5p_z2-nav-bar-subnav li').removeClass('active');
			//jQuery('.ge5p_z2-nav-bar-subnav li').find('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').removeClass('active').hide();
			//$this.parent().addClass('active');
			//var heightTray1Open = GE5P.heightPrimaryNavClosed + jQuery('#ge5p_z2-nav-bar li.active').height();
			//GE5P.animatePrimaryTray(heightTray1Open);
		}
	},
	secondaryNavActionFocusKeyboard: function(object) {
		//event.preventDefault();
		var heightOfActiveTray = 0;
		$this = jQuery(object); // Ensures lowest element in DOM was clicked - anchor tag
		// Check if the Secondary Menu has a Tray
		if($this.hasClass('ge5p_z2-nav-bar-subnav-has-tray')) {
			// First rename the Secondary Nav Link IF it is Highlighted to remove highlighting
			GE5P.secondaryNavHighlightRemove();
			// We have a Tray so display
			jQuery('.ge5p_z2-nav-bar-subnav li').removeClass('active');
			jQuery('.ge5p_z2-nav-bar-subnav li').find('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').removeClass('active').hide();
			$this.parent().addClass('active');
			$this.next().addClass('active').show();
			var heightTray1Open = GE5P.heightPrimaryNavClosed + jQuery('#ge5p_z2-nav-bar li.active').height();
			var heightTray2Open = heightTray1Open + jQuery('.ge5p_z2-nav-bar-subnav li div.tray.active').height();
			if(GE5P.ge5p_is_msie8) {
				heightTray2Open = heightTray2Open + 1;
			}
			GE5P.animatePrimaryTray(heightTray2Open);
			//Make all column divs the height of tray
			heightOfActiveTray = jQuery('.tray.active').height();
			jQuery('.ge5p_z2-nav-bar-subnav').find('div.tray.active div').each(function(){
				jQuery(this).height(heightOfActiveTray);
			});
			$this.attr('aria-expanded','true').attr('aria-selected','true');
		} else {
			$this.attr('aria-selected','true');
			GE5P.closeSecondaryNavTray(event);
			
			// Regular link in the Secondary menu so close the Tray
			/*
			jQuery('.ge5p_z2-nav-bar-subnav li').removeClass('active');
			jQuery('.ge5p_z2-nav-bar-subnav li').find('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').removeClass('active').hide();
			$this.parent().addClass('active');
			var heightTray1Open = GE5P.heightPrimaryNavClosed + jQuery('#ge5p_z2-nav-bar li.active').height();
			GE5P.animatePrimaryTray(heightTray1Open);
			*/
		}
	},
	openPrimaryNavTray: function(event) {
		$this = jQuery(event.target);
		// Check if Tray is open
		if(jQuery('.ge5p_z2-nav-item').hasClass('active')) {
			// Secondary Menu Already Open
			jQuery('.ge5p_z2-nav-item').removeClass('active');
			// Add class "active" to li to turn background white
			$this.parent().addClass('active');
			$this.attr('aria-expanded','true').attr('aria-selected','true');
			// Then Open Secondary Nav Tray
			// Element is a Primary Nav so just open Sec Nav Tray if there is one
			$this.next().find('a').first().focus();
				$thisFocus = jQuery('.ge5p_z2-nav-bar-subnav a:focus');
				$thisFocus.next().addClass('active').show();
				$thisFocus.parent().addClass('active');
				jQuery('.ge5p_z2-secondary-nav-el').attr('aria-expanded','false').attr('aria-selected','false');
				$thisFocus.attr('aria-expanded','true').attr('aria-selected','true');
				var heightOfActiveTray = jQuery('.ge5p_z2-nav-bar-subnav div.tray.active').height();
				var heightTray1Open = GE5P.heightPrimaryNavClosed + jQuery('#ge5p_z2-nav-bar li.active').height();
				var heightTray2Open = heightTray1Open + jQuery('.ge5p_z2-nav-bar-subnav li div.tray.active').height();
				if(GE5P.ge5p_is_msie8) {
					heightTray2Open = heightTray2Open + 1;
				}
				GE5P.animatePrimaryTray(heightTray2Open);
				event.preventDefault();
		} else {
			if($this.parent().hasClass('ge5p_z2-primaryLogo')) {
				// Logo - so do nothing
			} else {
				// Not open so lets open it
				$this.parent().addClass('active');
				GE5P.heightPrimaryNavTray = jQuery('#ge5p_z2-nav-bar').find('li.active').height();
				GE5P.heightPrimaryNavOpen = GE5P.heightPrimaryNavClosed + GE5P.heightPrimaryNavTray;
				// Increase height of PrimaryNav to show tray
				GE5P.animatePrimaryTray(GE5P.heightPrimaryNavOpen);
				$this.attr('aria-expanded','true').attr('aria-selected','true');
				event.preventDefault();
			}
		}
	},
	openSecondaryNavTray: function(event,status) {
		$this = jQuery(event.target);
		// Check if the event is from a Primary or Secondary element
		if($this.hasClass('ge5p_z2-primary-nav-el')) {
			// Element is a Primary Nav so just open Sec Nav Tray if there is one
			$this.next().find('a').first().focus();
			jQuery('.ge5p_z2-nav-bar-subnav').find('a:focus').next().addClass('active').show();
			$this.next().find('a:focus').parent().addClass('active');
			jQuery('.ge5p_z2-secondary-nav-el').attr('aria-expanded','false').attr('aria-selected','false');
			var heightOfActiveTray = jQuery('.ge5p_z2-nav-bar-subnav div.tray.active').height();
			var heightTray1Open = GE5P.heightPrimaryNavClosed + jQuery('#ge5p_z2-nav-bar li.active').height();
			var heightTray2Open = heightTray1Open + jQuery('.ge5p_z2-nav-bar-subnav li div.tray.active').height();
			if(GE5P.ge5p_is_msie8) {
				heightTray2Open = heightTray2Open + 1;
			}
			GE5P.animatePrimaryTray(heightTray2Open);
			$this.next().find('a:focus').attr('aria-expanded','true').attr('aria-selected','true');
		} else {
			if($this.hasClass('ge5p_z2-nav-bar-subnav-has-tray')) {
				if(status == 'tab') {
					jQuery('.ge5p_z2-secondary-nav-el').attr('aria-expanded','false').attr('aria-selected','false');
					$this.attr('aria-expanded','true').attr('aria-selected','true');
				} else {
					jQuery('.ge5p_z2-nav-bar-subnav li').removeClass('active');
					jQuery('.ge5p_z2-nav-bar-subnav li').find('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').removeClass('active').hide();
					$this.parent().addClass('active');
					// We have a Tray so display
					$this.next().addClass('active').show();
					var heightTray1Open = GE5P.heightPrimaryNavClosed + jQuery('#ge5p_z2-nav-bar li.active').height();
					var heightTray2Open = heightTray1Open + jQuery('.tray.active').height();
					//var heightTray2Open = heightTray1Open + heightOfActiveTray;
					if(GE5P.ge5p_is_msie8) {
						heightTray2Open = heightTray2Open + 1;
					}
					GE5P.animatePrimaryTray(heightTray2Open);
					//Make all column divs the height of tray
					var heightOfActiveTray = jQuery('.tray.active').height();
					if(GE5P.ge5p_is_msie8) {
						heightOfActiveTray = heightOfActiveTray - 1;
					}
					jQuery('.ge5p_z2-nav-bar-subnav').find('div.tray.active div').each(function(){
						jQuery(this).height(heightOfActiveTray);
					});
				}
			} else {
				// Regular link in the Secondary menu so close the Tray
				jQuery('.ge5p_z2-nav-bar-subnav li').removeClass('active');
				jQuery('.ge5p_z2-nav-bar-subnav li').find('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').removeClass('active').hide();
				var heightTray1Open = GE5P.heightPrimaryNavClosed + jQuery('#ge5p_z2-nav-bar li.active').height();
				GE5P.animatePrimaryTray(heightTray1Open);
				jQuery('.ge5p_z2-secondary-nav-el').attr('aria-expanded','false').attr('aria-selected','false');
			}
		}
	},
	closePrimaryNavTrays: function() {
		GE5P.closeSecondaryNavTray();
		// Decrease height of PrimaryNav original size
		GE5P.animatePrimaryTray(GE5P.heightPrimaryNavClosed);
		jQuery('.ge5p_z2-nav-item').removeClass('active');
		jQuery('.ge5p_z2-primary-nav-el').attr('aria-expanded','false').attr('aria-selected','false');
		// Check if there are default Primary & Secondary Default elements - then show
	},
	closeSecondaryNavTray: function(event) {
		if(event) {
			$this = jQuery(event.target); // Ensures lowest element in DOM was clicked - anchor tag
			$this.parent().addClass('active');
		}
		var heightTray1Open = GE5P.heightPrimaryNavClosed + jQuery('#ge5p_z2-nav-bar li.active').height();
		// Increase height of PrimaryNav to show tray
		GE5P.animatePrimaryTray(heightTray1Open);
		jQuery('.ge5p_z2-nav-bar-subnav li').removeClass('active');
		jQuery('.ge5p_z2-nav-bar-subnav li').find('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').removeClass('active').hide();
		jQuery('.ge5p_z2-secondary-nav-el').attr('aria-expanded','false').attr('aria-selected','false');
	},
	/* Function to adjust height of Primary Nav depending on elements
	 **/
	animatePrimaryTray: function(height) {
		// Assign Primary nav element
        jQuery('#ge5p_z2-primary-nav').height(height);
		//jQuery('#ge5p_z2-primary-nav').css('height', height+'px');
		jQuery('#ge5p_z2-nav-bar li').css('height','auto');
		jQuery('.ge5p_z2-nav-bar-subnav li div.tray').css('height','auto');
	},
	/* Function to associate the key event
	 * with a Primary, Secondary and Tertiary Menus
	 **/
	keyboardPrimaryNavBar: function(event) {
		jQuery('#ge5p_z2').on('focusin', '.ge5p_z2-secondary-nav-el', GE5P.secondaryNavActionFocus);
		$this = jQuery(event.target);
		GE5P.keyCodePrimaryNav = event.which;
		if (GE5P.keyCodePrimaryNav === 9 && event.shiftKey) {
            // Treat Shift+Tab as reverse Tab
            $thisIndex = $this.parent().index();
            $thisAllIndex = $this.parent().parent().find('li').length;
            $thisAllIndex = $thisAllIndex - 1;
			// Check if on Primary Nav
            if($this.hasClass('ge5p_z2-primary-nav-el')) {
                // Inside Primary Nav - Either Logo/Shop/myAT&T/Support
                // Check if this is the First element
                if($thisIndex == 0) {
                    // Send focus to Login button
                    jQuery('#ge5p_z2-user-auth').focus();
                    event.preventDefault();
                } else if ($thisIndex == 1) {
                    // Send focus to Logo
                    jQuery('#primaryLogo').find('a').focus();
                    event.preventDefault();
                } else {
                    // Move to Previous Primary nav element
                    // Then down to the Secondary Tray and focus on last element
                    $this.parent().prev().find('a').focus();
                    jQuery('.ge5p_z2-secondary-nav-el').parent('.active').find('li').last().find('a').focus();
                    event.preventDefault();
                }
            }
            // Check if this is the Secondary Nav
			if($this.hasClass('ge5p_z2-secondary-nav-el')) {
				// Move focus to its Primary nav element if on first Secondary element
                if($thisIndex == 0) {
                    // First Secondary Tray element
					jQuery('.ge5p_z2-nav-item.active').find('a').first().focus();
					event.preventDefault();
                } else {
					// Check if Prev Secondary nav element has a tray
					if($this.parent().prev().find('a').hasClass('ge5p_z2-nav-bar-subnav-has-tray')) {
						jQuery('.ge5p_z2-nav-bar-subnav li').removeClass('active');
						jQuery('.ge5p_z2-nav-bar-subnav li').find('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').removeClass('active').hide();
						$this.parent().prev().addClass('active');
						$this.parent().prev().find('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').addClass('active').show();
						var heightTray1Open = GE5P.heightPrimaryNavClosed + jQuery('#ge5p_z2-nav-bar li.active').height();
						var heightTray2Open = heightTray1Open + jQuery('.ge5p_z2-nav-bar-subnav li div.tray.active').height();
						GE5P.animatePrimaryTray(heightTray2Open);
						$this.parent().prev().find('div > div:last-child').find('a').focus();
						event.preventDefault();
					}
				}
			}
            // Check if this is a Tertiary Nav
			if($this.hasClass('navTertiaryLink')) {
				$thisIndex = $this.parent().index(); // like 2
				$thisAllIndex = $this.parent().parent().find('li').length; // Like 5 links
				$thisAllIndex = $thisAllIndex - 1; // Adjust for index
				$thisDivIndex = $this.parentsUntil('.tray').closest('div').index();
				$tertiaryDivsIndex = $this.parents('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').find('div').length; // This could be like 5 divs
				$tertiaryDivsIndex = $tertiaryDivsIndex - 1;
				if($thisIndex == 0) {
					if($thisDivIndex == 0) {
						// At beginning Div
						jQuery('ul.ge5p_z2-nav-bar-subnav').find('li.active').find('a').first().focus();
						event.preventDefault();
					} else {
						// Move to previous Div and focus on first element
						$this.parentsUntil('.tray').closest('div').prev().find('a').last().focus();
						event.preventDefault();
					}
				} else {
					// Move up an element
					$this.parent().prev().find('a').focus();
					event.preventDefault();
				}
			}
		} else {
			switch(GE5P.keyCodePrimaryNav) {
				case 9: // Tab Key
					
				break;
				case 13: // Enter key
					GE5P.ge5pZ2_navigate(event,'enter');
				break;
				case 37: // Left Arrow
					jQuery('#ge5p_z2').off('focusin', '.ge5p_z2-secondary-nav-el', GE5P.secondaryNavActionFocus);
					GE5P.ge5pZ2_navigate(event,'left');
					event.preventDefault();
					event.stopPropagation();
					return false;
				case 38: // Up Arrow
					GE5P.ge5pZ2_navigate(event,'up');
					event.preventDefault();
					event.stopPropagation();
					return false;
				break;
				case 39: // Right Arrow
					jQuery('#ge5p_z2').off('focusin', '.ge5p_z2-secondary-nav-el', GE5P.secondaryNavActionFocus);
					GE5P.ge5pZ2_navigate(event,'right');
					event.preventDefault();
					event.stopPropagation();
					return false;
				case 40: // Down Arrow
					GE5P.ge5pZ2_navigate(event,'down');
					event.preventDefault();
					event.stopPropagation();
					return false;
				break;
				case 32: // Space bar
					GE5P.ge5pZ2_navigate(event,'space');
					event.preventDefault();
					event.stopPropagation();
					return false;
				break;
				case 27: // Escape key
					GE5P.ge5pZ2_navigate(event,'escape');
					event.preventDefault();
					event.stopPropagation();
				break;
			}
		}
	},
	/* Function for Primary, Secondary and Tertiary Menu
	 * keyboard events
	 **/
	ge5pZ2_navigate: function(event,direction) {
		$this = jQuery(event.target);
		$thisID = $this.attr('id');
		switch(direction) {
			case 'tab':
				// If Primary nav
				if($this.hasClass('ge5p_z2-primary-nav-el')) {
					GE5P.openPrimaryNavTray(event);
				}
				// If Secondary nav
				if($this.hasClass('ge5p_z2-secondary-nav-el')) {
					var status = 'tab';
					GE5P.openSecondaryNavTray(event,status);
				}
				if($this.hasClass('navTertiaryLink')) {
					$tertiaryDivsIndex = $this.parents('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').find('div').length; // This could be like 5 divs
					$tertiaryDivsIndex = $tertiaryDivsIndex - 1;
					$thisIndex = $this.parent().index(); // Index we are currently at in the ul - like 0/1/2 etc.
					$thisDivIndex = $this.parentsUntil('.tray').closest('div').index();
					$tertiaryLength = $this.parent().parent().find('li').length; // This could be like 5 links
					$tertiaryLength = $tertiaryLength - 1;
					$primaryNavAllIndex = jQuery('#ge5p_z2-nav-bar > li').length - 2; // Because of Logo & Search - should be 3
					$primaryNavActiveIndex = jQuery('#ge5p_z2-nav-bar').find('li.active').index();
					if($thisIndex < $tertiaryLength) {
						$this.parent().next().find('a').focus();
						event.preventDefault();
					} else {
						// Ran out of Menu - check if there is another ul section or move up
						if($thisDivIndex < $tertiaryDivsIndex) {
							//$this.parent().parent().parent().next().find('a').first().focus();
							$this.parentsUntil('.tray').closest('div').next().find('a').first().focus();
							event.preventDefault();
						} else {
							// Ran out of divs, so check if there are more Secondary Nav elements
							// or send to Primary
							$thisAllIndex = $this.parents('.ge5p_z2-nav-item.active').find('.ge5p_z2-nav-bar-subnav > li').length; // like 8 links
							$thisAllIndex = $thisAllIndex - 1; // Adjust for 0 based index
							// Index of the Secondary element in the list
							$thisSecIndex = $this.parents('.ge5p_z2-nav-bar-subnav').find('li.active').index();
							// Check the elements location in the list
							if($thisSecIndex < $thisAllIndex) {
								GE5P.secondaryNavHighlightRemove();
								// There is a next Secondary Nav element like "Bundles"
								$this.parent().parent().parent().parent().parent().next().find('a').first().focus();
								$this.parent().parent().parent().parent().parent().removeClass('active');
								$thisFocus = jQuery('.ge5p_z2-nav-bar-subnav a:focus');
								jQuery('.ge5p_z2-nav-bar-subnav li').find('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').removeClass('active').hide();
								$thisFocus.parent().addClass('active');
								$thisFocus.parent().find('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').addClass('active').show();
								var heightTray1Open = GE5P.heightPrimaryNavClosed + jQuery('#ge5p_z2-nav-bar li.active').height();
								var heightTray2Open = heightTray1Open + jQuery('.ge5p_z2-nav-bar-subnav li div.tray.active').height();
								if(GE5P.ge5p_is_msie8) {
									heightTray2Open = heightTray2Open + 1;
								}
								GE5P.animatePrimaryTray(heightTray2Open);
								event.preventDefault();
							} else {
								// Check if there are no more Primary Nav Tabs left to activate
								if($primaryNavActiveIndex < $primaryNavAllIndex) {
									// Up to Primary
									jQuery('#ge5p_z2-nav-bar').find('li.active').next().find('a').focus();
									jQuery('#ge5p_z2-nav-bar').find('li.active').next().addClass('active');
									event.preventDefault();
								} else {
									// Focus on search
									jQuery('#ge5p_search').focus();
									event.preventDefault();
								}
							}
						}
					}
				}
				break;
			case 'escape':
				//If at Tertiary level
				if($this.hasClass('navTertiaryLink')) {
					// Move focus to its Primary Nav Element
					jQuery('.ge5p_z2-nav-item.active').find('a').first().focus();
					event.preventDefault();
					GE5P.closeSecondaryNavTray();
				}
				break;
			case 'enter':
				// If Primary nav
				if($this.hasClass('ge5p_z2-primary-nav-el')) {
					if($this.parent().hasClass('ge5p_z2-primaryLogo')) {
						return true;
					} else {
						// Check if Primary menu is open, if so, close
						if(jQuery('.ge5p_z2-nav-bar-subnav').is(':visible')) {
							// Close
							return true;
						} else {
							event.preventDefault();
							// Open Primary Tray
							GE5P.openPrimaryNavTray(event);
						}
					}
				}
				if($this.hasClass('ge5p_z2-nav-bar-subnav-has-tray')) {
					return true;
				}
				// If Tertiary Nav
				if($this.hasClass('navTertiaryLink')) {
					return true;
				}
				break;
			case 'left':
				event.preventDefault();
				// If Primary nav
				if($this.hasClass('ge5p_z2-primary-nav-el')) {
					$thisIndex = $this.parent().index();
					$primaryNavAllIndex = jQuery('#ge5p_z2-nav-bar > li').length - 2; // Because of Logo & Search - should be 3
					if($thisIndex == 1) {
						jQuery('#ge5p_z2-nav-bar li').last().prev().find('a').first().focus();
					} else {
						$this.parent().prev().find('a').first().focus();
					}
				}
				// If Secondary Nav
				if($this.hasClass('ge5p_z2-secondary-nav-el')) {
					$thisIndex = $this.parent().index();
					$thisAllIndex = $this.parents('.ge5p_z2-nav-item.active').find('.ge5p_z2-nav-bar-subnav > li').length;
					$thisAllIndex = $thisAllIndex - 1;
					if($thisIndex == 0) {
						$this.parents('.ge5p_z2-nav-item.active').find('.ge5p_z2-nav-bar-subnav > li:last-child').find('a').first().focus();
						$thisPrev = $this.parents('.ge5p_z2-nav-item.active').find('.ge5p_z2-nav-bar-subnav > li:last-child').find('a:focus');
						if($this.parents('.ge5p_z2-nav-item.active').find('.ge5p_z2-nav-bar-subnav > li:last-child a:focus').hasClass('ge5p_z2-nav-bar-subnav-has-tray')) {
							GE5P.secondaryNavHighlightRemove();
							GE5P.secondaryNavActionFocusKeyboard($this);
						} else {
							// Regular link in the Secondary menu so close the Tray
							jQuery('.ge5p_z2-nav-bar-subnav li').removeClass('active');
							jQuery('.ge5p_z2-nav-bar-subnav li').find('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').removeClass('active').hide();
							var heightTray1Open = GE5P.heightPrimaryNavClosed + jQuery('#ge5p_z2-nav-bar li.active').height();
							GE5P.animatePrimaryTray(heightTray1Open);
							jQuery('.ge5p_z2-secondary-nav-el').attr('aria-expanded','false').attr('aria-selected','false');
							$this.parent().last().find('a:focus').attr('aria-expanded','false').attr('aria-selected','true');
						}
					} else {
						$this.parent().prev().find('a').first().focus();
						$thisPrev = $this.parent().prev().find('a:focus');
						if($this.parent().prev().find('a:focus').hasClass('ge5p_z2-nav-bar-subnav-has-tray')) {
							GE5P.secondaryNavHighlightRemove();
							GE5P.secondaryNavActionFocusKeyboard($thisPrev);
						} else {
							// Regular link in the Secondary menu so close the Tray
							jQuery('.ge5p_z2-nav-bar-subnav li').removeClass('active');
							jQuery('.ge5p_z2-nav-bar-subnav li').find('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').removeClass('active').hide();
							var heightTray1Open = GE5P.heightPrimaryNavClosed + jQuery('#ge5p_z2-nav-bar li.active').height();
							GE5P.animatePrimaryTray(heightTray1Open);
							jQuery('.ge5p_z2-secondary-nav-el').attr('aria-expanded','false').attr('aria-selected','false');
							$this.parent().prev().find('a:focus').attr('aria-expanded','false').attr('aria-selected','true');
						}
					}
				}
				// If Tertiary Nav
				if($this.hasClass('navTertiaryLink')) {
					$tertiaryDivsIndex = $this.parents('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').find('div').length; // This could be like 5 divs
					$tertiaryDivsIndex = $tertiaryDivsIndex - 1;
					$thisIndex = $this.parent().index(); // Index we are currently at in the ul - like 0/1/2 etc.
					$thisDivIndex = $this.parentsUntil('.tray').closest('div').index();
					$tertiaryLength = $this.parent().parent().find('li').length; // This could be like 5 links
					$tertiaryLength = $tertiaryLength - 1;
					$primaryNavAllIndex = jQuery('#ge5p_z2-nav-bar > li').length - 2; // Because of Logo & Search - should be 3
					$primaryNavActiveIndex = jQuery('#ge5p_z2-nav-bar').find('li.active').index();
					if($thisIndex == 0) {
						if($thisDivIndex == 0) {
							// At the first element of the first div, move focus to last div last element
							$this.parents('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').find('div').last().find('a').last().focus();
						} else {
							$this.parent().parent().parent().prev().find('a').last().focus();
						}
					} else {
						$this.parent().prev().find('a').first().focus();
					}
				}
				break;
			case 'right':
				event.preventDefault();
				if($this.hasClass('ge5p_z2-primary-nav-el')) {
					$thisIndex = $this.parent().index();
					$primaryNavAllIndex = jQuery('#ge5p_z2-nav-bar > li').length - 2; // Because of Logo & Search - should be 3
					if($thisIndex == $primaryNavAllIndex) {
						jQuery('#ge5p_z2-nav-bar li').first().next().find('a').first().focus();
					} else {
						$this.parent().next().find('a').first().focus();
					}
				}
				// If Secondary Nav
				if($this.hasClass('ge5p_z2-secondary-nav-el')) {
					$thisIndex = $this.parent().index();
					$thisAllIndex = $this.parents('.ge5p_z2-nav-item.active').find('.ge5p_z2-nav-bar-subnav > li').length;
					$thisAllIndex = $thisAllIndex - 1;
					if($thisIndex == $thisAllIndex) {
						$this.parents('.ge5p_z2-nav-item.active').find('.ge5p_z2-nav-bar-subnav > li:first-child').find('a').first().focus();
						// Check if next parent has a tray
						if($this.parents('.ge5p_z2-nav-item.active').find('.ge5p_z2-nav-bar-subnav > li:first-child a:focus').hasClass('ge5p_z2-nav-bar-subnav-has-tray')) {
							// First rename the Secondary Nav Link IF it is Highlighted to remove highlighting
							GE5P.secondaryNavHighlightRemove();
							jQuery('.ge5p_z2-nav-bar-subnav li').removeClass('active');
							jQuery('.ge5p_z2-nav-bar-subnav li').find('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').removeClass('active').hide();
							$thisNEXT = $this.parents('.ge5p_z2-nav-item.active').find('.ge5p_z2-nav-bar-subnav > li:first-child a:focus');
							GE5P.secondaryNavActionFocusKeyboard($thisNEXT);
						} else {
							// Regular link in the Secondary menu so close the Tray
							jQuery('.ge5p_z2-nav-bar-subnav li').removeClass('active');
							jQuery('.ge5p_z2-nav-bar-subnav li').find('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').removeClass('active').hide();
							var heightTray1Open = GE5P.heightPrimaryNavClosed + jQuery('#ge5p_z2-nav-bar li.active').height();
							GE5P.animatePrimaryTray(heightTray1Open);
							jQuery('.ge5p_z2-secondary-nav-el').attr('aria-expanded','false').attr('aria-selected','false');
							$this.attr('aria-expanded','false').attr('aria-selected','true');
						}
					} else {
						$this.parent().next().find('a').first().focus();
						$thisNEXT = $this.parent().next().find('a:focus');
						// Check if next parent has a tray
						if($this.parent().next().find('a:focus').hasClass('ge5p_z2-nav-bar-subnav-has-tray')) {
							// First rename the Secondary Nav Link IF it is Highlighted to remove highlighting
							GE5P.secondaryNavHighlightRemove();
							GE5P.secondaryNavActionFocusKeyboard($thisNEXT);
						} else {
							// Regular link in the Secondary menu so close the Tray
							jQuery('.ge5p_z2-nav-bar-subnav li').removeClass('active');
							jQuery('.ge5p_z2-nav-bar-subnav li').find('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').removeClass('active').hide();
							var heightTray1Open = GE5P.heightPrimaryNavClosed + jQuery('#ge5p_z2-nav-bar li.active').height();
							GE5P.animatePrimaryTray(heightTray1Open);
							jQuery('.ge5p_z2-secondary-nav-el').attr('aria-expanded','false').attr('aria-selected','false');
							$this.attr('aria-expanded','false').attr('aria-selected','true');
						}
					}
				}
				// If Tertiary Nav
				if($this.hasClass('navTertiaryLink')) {
					$tertiaryDivsIndex = $this.parents('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').find('div').length; // This could be like 5 divs
					$tertiaryDivsIndex = $tertiaryDivsIndex - 1;
					$thisIndex = $this.parent().index(); // Index we are currently at in the ul - like 0/1/2 etc.
					$thisDivIndex = $this.parentsUntil('.tray').closest('div').index();
					$tertiaryLength = $this.parent().parent().find('li').length; // This could be like 5 links
					$tertiaryLength = $tertiaryLength - 1;
					$primaryNavAllIndex = jQuery('#ge5p_z2-nav-bar > li').length - 2; // Because of Logo & Search - should be 3
					$primaryNavActiveIndex = jQuery('#ge5p_z2-nav-bar').find('li.active').index();
					if($thisIndex < $tertiaryLength) {
						if($thisDivIndex < $tertiaryDivsIndex) {
							$this.parent().next().find('a').first().focus();
						} else {
							if($thisIndex < $tertiaryLength) {
								$this.parent().next().find('a').first().focus();
							} else {
								$this.parents('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').find('div').first().find('a').first().focus();
							}
						}
					} else {
						if($thisDivIndex < $tertiaryDivsIndex) {
							$this.parent().parent().parent().next().find('a').first().focus();
						} else {
							$this.parents('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').find('div').first().find('a').first().focus();
						}
					}
				}
				break;
			case 'down':
				// Check if this is Primary level
				if($this.parent().hasClass('ge5p_z2-nav-item')) {
					// Check if Primary is active
					if($this.parent().hasClass('active')) {
						// If yes, move to First Secondary Nav element
						GE5P.secondaryNavHighlightRemove();
						$this.next().find('a').first().focus();
						$this.next().find('a').first().parent().addClass('active');
						GE5P.openSecondaryNavTray(event);
						event.preventDefault();
					} else {
						// Primary Menu is not active and Secondary Tray is not open
						GE5P.openPrimaryNavTray(event);
					}
				}
				// Check if this is the Secondary level
				if($this.hasClass('ge5p_z2-secondary-nav-el')) {
					// This Elements Index (place in the list);
					$thisIndex = $this.parent().index();
					// All Elements in the list (length)
					$thisAllIndex = $this.parents('.ge5p_z2-nav-item.active').find('.ge5p_z2-nav-bar-subnav > li').length;
					$thisAllIndex = $thisAllIndex - 1;

					// Check if Secondary Title is active
					if($this.parent().hasClass('active')) {
						// If yes, move to Tertiary Element
						// Check if this is the first
						GE5P.secondaryNavActionFocus(event);
						$this.next().find('a').first().focus();
						GE5P.secondaryNavHighlightRemove();
						// Make sure to put the Secondary Nav Link with a Highlight
						// Future Code Here
					} else {
						// If not, open Secondary
						// Check if link has Secondary Tray
						if($this.hasClass('ge5p_z2-nav-bar-subnav-has-tray')) {
							GE5P.secondaryNavActionFocus(event);
							$this.next().find('a').first().focus();
						} else {
							// Check if this is the last element
							if($thisIndex < $thisAllIndex) {
								// Move to next element in Secondary Nav
								$this.parent().next().find('a').focus();
							} else {
								// Reached the end of the Secondary Nav Elements
								// Move up DOM to Primary Level
								jQuery('#ge5p_z2-nav-bar').find('li.active').next().find('a').first().focus();
							}
						}
					}
				}
				// Check if this is the Tertiary level
				if($this.hasClass('navTertiaryLink')) {
					$tertiaryDivsIndex = $this.parents('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').find('div').length; // This could be like 5 divs
					$tertiaryDivsIndex = $tertiaryDivsIndex - 1;
					$thisIndex = $this.parent().index(); // Index we are currently at in the ul - like 0/1/2 etc.
					$thisDivIndex = $this.parentsUntil('.tray').closest('div').index();
					$tertiaryLength = $this.parent().parent().find('li').length; // This could be like 5 links
					$tertiaryLength = $tertiaryLength - 1;
					$primaryNavAllIndex = jQuery('#ge5p_z2-nav-bar > li').length - 2; // Because of Logo & Search - should be 3
					$primaryNavActiveIndex = jQuery('#ge5p_z2-nav-bar').find('li.active').index();

					if($thisIndex < $tertiaryLength) {
						$this.parent().next().find('a').focus();
					} else {
						// Ran out of Menu - check if there is another ul section or move up
						if($thisDivIndex < $tertiaryDivsIndex) {
							//$this.parent().parent().parent().next().find('a').first().focus();
							$this.parentsUntil('.tray').closest('div').next().find('a').first().focus();
						} else {
							// Ran out of divs, so check if there are more Secondary Nav elements
							// or send to Primary
							$thisAllIndex = $this.parents('.ge5p_z2-nav-item.active').find('.ge5p_z2-nav-bar-subnav > li').length; // like 8 links
							$thisAllIndex = $thisAllIndex - 1; // Adjust for 0 based index
							// Index of the Secondary element in the list
							$thisSecIndex = $this.parents('.ge5p_z2-nav-bar-subnav').find('li.active').index();
							// Check the elements location in the list
							if($thisSecIndex < $thisAllIndex) {
								// There is a next element
								jQuery('ul.ge5p_z2-nav-bar-subnav').find('li.active').next().find('a').focus();
								GE5P.secondaryNavActionFocus(event);
							} else {
								// Check if there are no more Primary Nav Tabs left to activate
								if($primaryNavActiveIndex < $primaryNavAllIndex) {
									// Up to Primary
									jQuery('#ge5p_z2-nav-bar').find('li.active').next().find('a').focus();
									jQuery('#ge5p_z2-nav-bar').find('li.active').next().addClass('active');
								} else {
									// Focus on search
									jQuery('#ge5p_search').focus();
								}
							}
						}
					}
				}
				break;
			case 'up':
				// Check if Primary Level
				if($this.parent().hasClass('ge5p_z2-nav-item')) {
					// Check if Primary is active
					if($this.parent().hasClass('active')) {
					}
				}
				// Check if Secondary Level
				if($this.hasClass('ge5p_z2-secondary-nav-el')) {
                    // Find Secondary Tray element and focus on
					jQuery('.ge5p_z2-nav-item.active').find('a').first().focus();
					event.preventDefault();
                }
				// Check if Tertiary Level
				if($this.hasClass('navTertiaryLink')) {
					$thisIndex = $this.parent().index(); // like 2
					$thisAllIndex = $this.parent().parent().find('li').length; // Like 5 links
					$thisAllIndex = $thisAllIndex - 1; // Adjust for index
					$thisDivIndex = $this.parentsUntil('.tray').closest('div').index();
					$tertiaryDivsIndex = $this.parents('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').find('div').length; // This could be like 5 divs
					$tertiaryDivsIndex = $tertiaryDivsIndex - 1;
					if($thisIndex == 0) {
						if($thisDivIndex == 0) {
							// At beginning Div
							jQuery('ul.ge5p_z2-nav-bar-subnav').find('li.active').find('a').first().focus();
							event.preventDefault();

						} else {
							// Move to previous Div and focus on first element
							$this.parentsUntil('.tray').closest('div').prev().find('a').last().focus();
							event.preventDefault();
						}
					} else {
						// Move up an element
						$this.parent().prev().find('a').focus();
						event.preventDefault();
					}
				}
				break;
		}
	},
	/* SECONDARY HIGHLIGHT REMOVED */
	/* Function to First rename the Secondary Nav Link IF
	 * it is Highlighted to remove highlighting
	**/
	secondaryNavHighlightRemove: function() {
		var secondaryHighlightLink = jQuery('.ge5p_z2-nav-bar-subnav li').find('a.ge5p_z2-default-secondary-active');
		if(secondaryHighlightLink) {
			secondaryHighlightLink.removeClass('ge5p_z2-default-secondary-active').addClass('ge5p_z2-default-secondary-active2');
		}
	},
	/* SECONDARY HIGHLIGHT ADDED */
	/* Function to find Secondary Nav Link and
	 * add Highlighting
	**/
	secondaryNavHighlightAdd: function() {
		var secondaryHighlightLink = jQuery('.ge5p_z2-nav-bar-subnav li').find('a.ge5p_z2-default-secondary-active2');
		if(secondaryHighlightLink) {
			secondaryHighlightLink.removeClass('ge5p_z2-default-secondary-active2').addClass('ge5p_z2-default-secondary-active');
		}
	},
	/* USER INFO DATA FUNCTION */
	/* Function to collect and display user info including zip and register info
	**/
	userInfoData: function() {
		// Find Cookie info
		// Look up attPersistantLocalization cookie on user system
		// Remove cookie json
		//jQuery.cookie.json = false;
		var ge5p_attPersistantLocalization = GE5P.readCookie("attPersistantLocalization");
		if(ge5p_attPersistantLocalization) { //attPersistantLocalization
			ge5p_attPersistantLocalization = unescape(ge5p_attPersistantLocalization);
			ge5pZ2_valueCookieArr = ge5p_attPersistantLocalization.split('|');
			// State = arr 3, City = arr 4, Zip = arr 6
			// City
			var ge5pZ2_zipCode_City = jQuery.trim(ge5pZ2_valueCookieArr[4]).split('=')[1];
			// State
			var ge5pZ2_zipCode_State = GE5P.ge5p_z2_getStateAbbreviationByName(jQuery.trim(ge5pZ2_valueCookieArr[3]).split('=')[1]);
			// Zip
			var ge5pZ2_zipCode_Zip = jQuery.trim(ge5pZ2_valueCookieArr[6]).split('=')[1];
			// Change values in Zone 2 Zip Code block
			jQuery('#ge5p_z2-zipcode-city').html(ge5pZ2_zipCode_City +', ');
			jQuery('#ge5p_z2-zipcode-state').html(ge5pZ2_zipCode_State);
			jQuery('#ge5p_z2-zipcode-zip').html(ge5pZ2_zipCode_Zip);
			jQuery('.ge5p_z2-zipcode-enter').hide();
			jQuery('.ge5p_z2-zipcode-change').show();
			jQuery('#ge5p_z2-zipcode-city').show();
			jQuery('#ge5p_z2-zipcode-state').show();
			jQuery('#ge5p_z2-zipcode-zip').show();
		} else {
			// attPersistantLocalization is not found, so check for
			// server vars that contain the Akamai data
			if(GE5P.ge5p_userCityVar || GE5P.ge5p_userStateVar || GE5P.ge5p_userZipCodeVar) {
				if(GE5P.ge5p_userCityVar.length > 0 || GE5P.ge5p_userStateVar.length > 0 || GE5P.ge5p_userZipCodeVar.length > 0) {
					jQuery('.ge5p_z2-zipcode-enter').hide();
					jQuery('.ge5p_z2-zipcode-change').show();
					jQuery('#ge5p_z2-zipcode-city').show();
					jQuery('#ge5p_z2-zipcode-state').show();
					jQuery('#ge5p_z2-zipcode-zip').show();
				} else {
					jQuery('.ge5p_z2-zipcode-enter').show();
				}
			} else {
				jQuery('.ge5p_z2-zipcode-enter').show();
			}
			if(GE5P.ge5p_userCityVar) {
				if(GE5P.ge5p_userCityVar.length > 0) {
					jQuery('#ge5p_z2-zipcode-city').text(GE5P.ge5p_userCityVar +', ');
					jQuery('#ge5p_z2-zipcode-city').show();
				}
			}
			if(GE5P.ge5p_userStateVar) {
				if(GE5P.ge5p_userStateVar.length > 0) {
					jQuery('#ge5p_z2-zipcode-state').text(GE5P.ge5p_userStateVar);
					jQuery('#ge5p_z2-zipcode-state').show();
				}
			}
			if(GE5P.ge5p_userZipCodeVar) {
				if(GE5P.ge5p_userZipCodeVar.length > 0) {
					jQuery('#ge5p_z2-zipcode-zip').html(' '+GE5P.ge5p_userZipCodeVar);
					jQuery('#ge5p_z2-zipcode-zip').show();
				}
			}
		}
		/* Dynamic styles if elements exist */
		//State
		if(jQuery('#ge5p_z2-zipcode-state').text().length > 0) {
			jQuery('#ge5p_z2-zipcode-state').css('padding-right', '5px');
		}
	},
	/* STATE ABBREVIATION FUNCTION */
	/* Function to change long state name into short abbreviation
	**/
	ge5p_z2_getStateAbbreviationByName: function(state){
		var $state = state;
		if ($state=="Alaska"){ return "AK"; }
		if ($state=="Alabama"){ return "AL"; }
		if ($state=="Arkansas"){ return "AR"; }
		if ($state=="Arizona"){ return "AZ"; }
		if ($state=="California"){ return "CA"; }
		if ($state=="Colorado"){ return "CO"; }
		if ($state=="Connecticut"){ return "CT"; }
		if ($state=="District of Columbia"){ return "DC"; }
		if ($state=="Delaware"){ return "DE"; }
		if ($state=="Florida"){ return "FL"; }
		if ($state=="Georgia"){ return "GA"; }
		if ($state=="Hawaii"){ return "HI"; }
		if ($state=="Iowa"){ return "IA"; }
		if ($state=="Idaho"){ return "ID"; }
		if ($state=="Illinois"){ return "IL"; }
		if ($state=="Indiana"){ return "IN"; }
		if ($state=="Kansas"){ return "KS"; }
		if ($state=="Kentucky"){ return "KY"; }
		if ($state=="Louisiana"){ return "LA"; }
		if ($state=="Massachusetts"){ return "MA"; }
		if ($state=="Maryland"){ return "MD"; }
		if ($state=="Maine"){ return "ME"; }
		if ($state=="Michigan"){ return "MI"; }
		if ($state=="Minnesota"){ return "MN"; }
		if ($state=="Missouri"){ return "MO"; }
		if ($state=="Mississippi"){ return "MS"; }
		if ($state=="Montana"){ return "MT"; }
		if ($state=="North Carolin"){ return "NCa"; }
		if ($state=="North Dakota"){ return "ND"; }
		if ($state=="Nebraska"){ return "NE"; }
		if ($state=="New Hampshire"){ return "NH"; }
		if ($state=="New Jersey"){ return "NJ"; }
		if ($state=="New Mexico"){ return "NM"; }
		if ($state=="Nevada"){ return "NV"; }
		if ($state=="New York"){ return "NY"; }
		if ($state=="Ohio"){ return "OH"; }
		if ($state=="Oklahoma"){ return "OK"; }
		if ($state=="Oregon"){ return "OR"; }
		if ($state=="Pennsylvania"){ return "PA"; }
		if ($state=="Rhode Island"){ return "RI"; }
		if ($state=="South Carolina"){ return "SC"; }
		if ($state=="South Dakota"){ return "SD"; }
		if ($state=="Tennessee"){ return "TN"; }
		if ($state=="Texas"){ return "TX"; }
		if ($state=="Utah"){ return "UT"; }
		if ($state=="Virginia"){ return "VA"; }
		if ($state=="Vermont"){ return "VT"; }
		if ($state=="Washington"){ return "WA"; }
		if ($state=="Wisconsin"){ return "WI"; }
		if ($state=="West Virginia"){ return "WV"; }
		if ($state=="Wyoming"){ return "WY"; }
	},
	androidCatoAria: function(event) {
		setTimeout(function(){
			/* Add Accessibility attributes */
			/* Global - Seg/Util Nav */
			jQuery('#ge5p_z1-nav-left-seg').attr('role','menubar');
			jQuery('#ge5p_z1-nav-right-seg').attr('role','menubar');
			jQuery('#ge5p_z1-nav-left-seg li').attr('role','presentation');
			jQuery('#ge5p_z1-nav-right-seg li').attr('role','presentation');
			jQuery('.ge5p_z1-drop-down').attr('role','button').attr('aria-haspopup','true');
			jQuery('.ge5p_z1-menu ul').attr('role','menu').attr('aria-expanded','false').attr('aria-hidden','true');
			jQuery('.ge5p_z1-menu ul li').attr('role','presentation');
			jQuery('.ge5p_z1-menu ul li a').attr('role','menuitem').attr('tabindex','0');
		}, 1000);
	},
	/* SEARCH SHIFT-TAB FUNCTION */
	/* Function to set focus on Support element off shift tab from #search
	**/
	keyboardGNSearchBox: function(event) {
		$this = jQuery(event.target);
		$thisID = $this.attr('id');
        keyCodePrimaryNav = event.which;
		if (keyCodePrimaryNav === 9 && event.shiftKey) {
            // Treat Shift+Tab as reverse Tab
			jQuery('.ge5p_z2-nav-bar-subnav li').removeClass('active');
			jQuery('.ge5p_z2-nav-bar-subnav li').find('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').removeClass('active').hide();
			var $priSearch = $this.parents('#primary_Search');
			$priSearch.prev().addClass('active');
			$priSearch.prev().find('http://www.att.com/scripts/tesla1.3.0.1_SHOP/div.tray').addClass('active').show();
			jQuery('div.tray.active').parent().last().addClass('active');
			$priSearch.prev().find('div > div:last-child').find('a').focus();
			var heightTray1Open = GE5P.heightPrimaryNavClosed + jQuery('#ge5p_z2-nav-bar li.active').height();
			var heightTray2Open = heightTray1Open + jQuery('.ge5p_z2-nav-bar-subnav li div.tray.active').height();
			GE5P.animatePrimaryTray(heightTray2Open);
			event.preventDefault();
		}
	},
	/* SEARCH VALIDATEE FUNCTION */
	/* Function to validate value in Primary nav search
	**/
	validateSearchForm: function(event) {
		var  initVal = "", self, searchStr, srch = jQuery('#ge5p_search'); //new to handle Spanish
		var srchForm = jQuery('#searchForm');
		self = srch;
		searchStr = self.val();
		searchStr = GE5P.decodeHtml(searchStr);
		searchStr = searchStr.replace(/[^?+\w\s@:;#&$Ã©Ã±Ã¡ÃÃ­Ã³ÃºÃ¼-]/gi, '').trim();
		//alert(searchStr);
		if(self.val().toLowerCase() == 'search'){
			self.val('');
			initVal = "Search"; //new to handle Spanish
			self.val(initVal);
			return false;
		}
		else if(self.val().toLowerCase() == 'buscar'){ //new to handle Spanish
			self.val('');
			initVal = "Buscar";
			self.val(initVal);
			return false;
		} else if(self.val().toLowerCase() == '') {
			self.val('');
			initVal = "Search";
			self.val(initVal);
			return false;
		}else{
			self.val(searchStr);
			return;
		}
	},
	/* SEARCH AUTOCOMPLETE FUNCTION */
	/* Function to show autocomplete suggestion box for Primary nav search
	**/
	searchAutocomplete: function() {
        //global autosuggest code
        var client_id = 1;
        var initVal = "", self, srch = jQuery('#ge5p_search'); //new to handle spanish
        var srchForm = jQuery('#searchForm');
        srch.focus(function(){
            //Close menus
            GE5P.closeSegUtilNav();
            GE5P.closePrimaryNavTrays();
            self = jQuery(this);
            if(self.val().toLowerCase() == 'search'){
                self.val('');
                return initVal = "Search"; //new to handle spanish
            }
            else if(self.val().toLowerCase() == 'buscar'){ //new to handle spanish
                self.val('');
                return initVal = "Buscar";
            }
        });
        srch.blur(function(){
            self = jQuery(this);
            var searchStr = self.val();
            searchStr = searchStr.replace(/\s+/g, ' ');
            self.val(jQuery.trim(searchStr));
        });

		var autoSuggestBox = jQuery("#autoSuggestBox");
		var ui_autocomplete = jQuery(".ui-autocomplete");
		var asState = jQuery('input[name="autoSuggest"]');
		asState.val("FALSE"); //set hidden field to false as default until autosuggest is utilized and changes to TRUE in the select property below.
		
		jQuery("#ge5p_search").autocomplete({
			appendTo:"#autoSuggestBox",
			delay: 0,
			minLength: 1,
			open: function(event, ui) {
				autoSuggestBox.show();
				ui_autocomplete.css({"top":"0px","left":"0px","overflow":"auto"});
			},
			close: function(event, ui) {
				autoSuggestBox.hide();
			},
			position: { my : "left top", at: "left top", of:"#autoSuggestBox", collision: "fit"},
			select: function(event, ui) {
				srch.val(ui.item.value);
				asState.val("TRUE");
				srchForm.submit();
			},
			source: function(request, response) {
				jQuery.ajax({
					url: GE5P.ge5p_globalsearchurlBase + "/" + "global-search/gs_autosuggest.jsp?q=" + srch.val().replace(/[^?+\w\s@:;#Ã©&$Ã±Ã¡ÃÃ­Ã³ÃºÃ¼-]/gi, '').trim() + "&callback=insertAutoSuggestions",
					dataType: "jsonp",
					jsonpCallback: "insertAutoSuggestions",
					success: function(data) {
						var ra = [];
						if(data.length == 0) {
							jQuery("#autoSuggestBox").hide();
							return false;
						} else {
							jQuery.each(data, function(raindex, raval){
								ra[raindex] = {"label": raval["short"], "value": raval["short"]};
							});
							response(ra);
						}
					}
				});
			}
		});
	},
	changeLanguage: function(event) {
		$this = jQuery(event.target);
		var requesturl = $this.attr('href');
		var mp_url = $this.attr('data-href');
		var mp_lang = $this.attr('data-lang'); //es
		var locale = GE5P.ge5p_localLanguage; // current language
		if(GE5P.ge5p_teslaGNSESSPresent) {
			// Cookie exists so update
			if(GE5P.ge5p_motionpoint) {
				// Update cookie only
				requesturl = 'motionpoint';
			}
			GE5P.updateGNSESSCookie(locale,requesturl,mp_url,mp_lang);
		} else {
			// Create new GNSESS cookie
			if(GE5P.ge5p_motionpoint) {
				// Update cookie only
				requesturl = 'motionpoint';
			}
			GE5P.createGNSESSCookie(locale,requesturl,mp_url,mp_lang);
		}
		event.preventDefault();
		return false;
	},
	createGNSESSCookie: function(locale,requesturl,mp_url,mp_lang) {
		if(locale == 'en_US') {
			locale = 'es_US';
		} else {
			locale = 'en_US';
		}
		var cookieObj = {};
		var ugarray=["Unauth"];
		var ska=[["1","jsp"]];
		cookieObj['UG'] = ugarray;
		cookieObj['LOCALE'] = locale;
		cookieObj['SKA'] = ska;
		name="GNSESS";
		days="";
		path="/";
		domain=".att.com";
		GE5P.createCookie(name,cookieObj,days,domain,path);
		// Set Local Storage
		// TEMP REMOVED FUNCTION CALL -> GE5P.teslaLocalStorageSet(locale);
		// Fire off MP
		if(requesturl == 'motionpoint') {
			MP.switchLanguage(MP.tSite==mp_url?MP.oSite:mp_url, mp_lang);
		} else {
			window.location.replace(requesturl);
		}
	},
	updateGNSESSCookie: function(locale,requesturl,mp_url,mp_lang) {
		var locale2;
		if(locale == 'en_US') {
			locale2 = 'es_US';
		} else {
			locale2 = 'en_US';
		}
		var valueCookie = GE5P.readCookie('GNSESS');
		if(typeof valueCookie == "object"){
			valueCookie = GE5P.safeToJSON(valueCookie);
		}
		var GNSESSObj = jQuery.parseJSON(valueCookie);
		var newObj = {};
		jQuery.each(GNSESSObj, function(key, val) {
		   newObj[key] = GNSESSObj[key];
		   if(key == 'LOCALE') {
				newObj[key]=locale2;
		   }
		});
		name="GNSESS";
		days="";
		path="/";
		domain=".att.com";
		GE5P.createCookie(name,newObj,days,domain,path);
		// Set Local Storage
		// TEMP REMOVED FUNCTION CALL -> GE5P.teslaLocalStorageSet(locale);
		// Fire off MP
		if(requesturl == 'motionpoint') {
			MP.switchLanguage(MP.tSite==mp_url?MP.oSite:mp_url, mp_lang);
		} else {
			window.location.replace(requesturl);
		}
	},
	/* SKIP NAVIGATION FUNCTION */
	/* Function to override ship navigation bookmark and use js to send focus to body
	**/
	skipNavigation: function(event) {
		event.preventDefault();
		var wrapper = jQuery('#wrapper');
		if(wrapper.length){
			jQuery(document).scrollTop(wrapper.offset().top);
			wrapper.find("a:visible").first().focus();
		}
	},
	/* LOCALE Storage FUNCTION */
	/* Function to check/set TESLA Local Storage variable
	**/
	teslaLocalStorage: function(locale) {
		// We are setting the LS for the first time unless the user set it already
		// If Tesla LS var present...
		if(GE5P.ge5p_teslaLSLocalePresent) {
			if(GE5P.ge5p_teslaLSLocale =='en') {
				locale = "en_US";
			} else {
				locale = "es_US";
			}
			GE5P.teslaGNSESSCookie(locale);
		} else {
			// Set Local storage
			GE5P.teslaLocalStorageSet(locale);
			GE5P.teslaGNSESSCookie(locale);
		}
	},
	/* LOCALE Storage FUNCTION */
	/* Function to check/set TESLA Local Storage variable
	**/
	teslaLocalStorageSet: function(locale) {
		// Check if App is in scope
		if(GE5P.ge5p_motionpoint) {
			if(locale == "en_US") {
				jQuery.jStorage.set('TESLALSLOCALE','en');
			} else {
				jQuery.jStorage.set('TESLALSLOCALE','es');
			}
		}
	},
	teslaGNSESSCookie: function(locale) {
		// Create or Update GNSESS Cookie if present
		if(GE5P.ge5p_teslaGNSESSPresent) {
			// GNSESS present so update
			//jQuery.cookie.json = true;
			var valueCookie = GE5P.readCookie("GNSESS");
			if(typeof valueCookie == "object"){
				valueCookie = GE5P.safeToJSON(valueCookie);
			}
			var GNSESSObj = jQuery.parseJSON(valueCookie);
			var newObj = {};
			jQuery.each(GNSESSObj, function(key, val){
			   newObj[key] = GNSESSObj[key];
			   if(key == 'LOCALE') {
				   newObj[key]=locale;
			   }
			});
			//jQuery.cookie.raw=true;
			// Set GNSESS Cookie
			name="GNSESS";
			days="";
			path="/";
			domain=".att.com";
			GE5P.createCookie(name,newObj,days,domain,path);
		} else {
			// GNSESS not present so create
			var cookieObj = {};
			var ugarray=["Unauth"];
			var ska=[["1","jsp"]];
			cookieObj['UG'] = ugarray;
			cookieObj['LOCALE'] = locale;
			cookieObj['SKA'] = ska;
			name="GNSESS";
			days="";
			path="/";
			domain=".att.com";
			GE5P.createCookie(name,cookieObj,days,domain,path);
		}
	},
	/* Set Host FUNCTION */
	/* Function to set the Nav/Footer links to the relative host
	**/
	setHostRelative: function() {
		var teslaLinks = jQuery('body').find('.ge5p_global_styles a');
		teslaLinks.each(function(){
			var o = jQuery(this);
			var href = o.attr('href');
			/* NEW WAY */
			var hrefUrl = jQuery('<a>').attr('href', href);
			var hrefProtocol = hrefUrl.attr('protocol');
			var hrefHost = hrefUrl.attr('host');
			var hrefRelative = hrefUrl.attr('pathname');
			
			if(href.indexOf('http://www.att.com/scripts/tesla1.3.0.1_SHOP/www.att.com') != -1) {
				var newhrefHost = href.replace('http://www.att.com/scripts/tesla1.3.0.1_SHOP/www.att.com', GE5P.ge5p_websiteurlHost);
				var hrefFull = newhrefHost;
				o.attr('href',hrefFull);
			} else {
				o.attr('href',href);
			}
		});
	},
	teslaComplete: function(func, wait, times) {
		var interv = function() {
			if ((typeof times === "undefined" || times-- > 0)) {
				times = 1;
			}
			if ((typeof wait === "undefined" || wait-- > 0)) {
				wait = 500;
			}
			if(!GE5P.ge5p_pageRepainted) {
				setTimeout(interv, wait);
			} else {
				try {
					func.call(null);
					clearTimeout(setTeslaComplete);
				}
				catch(e) {
					times = 0;
					throw e.toString();
					clearTimeout(setTeslaComplete);
				}
			}
		};
		var setTeslaComplete = setTimeout(interv, wait);
	},
	checkLanguagePreference: function() {
		/*
		* 1. Check GNSESS for es
		* 2. If es, redirect to MP session
		*/
		if(GE5P.ge5p_motionpoint) {
			if(GE5P.ge5p_teslaGNSESSPresent) {
				if(GE5P.ge5p_websiteurlSource.indexOf('es-us') != -1 && GE5P.ge5p_localLanguage == 'es_US') {
					// User is on a Spanish language page so no redirect is necessary
				} else if(!GE5P.ge5p_websiteurlSource.indexOf('es-us') != -1 && GE5P.ge5p_localLanguage == 'es_US') {
					// User is on english page, but they perfer Spanish, so redirect
					// User is on an English page so redirect to Spanish
					$this = jQuery('#ge5p_z1-change-language');
					var mp_url = $this.attr('data-href');
					var mp_lang = $this.attr('data-lang'); //es
					MP.switchLanguage(MP.tSite==mp_url?MP.oSite:mp_url, mp_lang);
				}
			} else {
				GE5P.teslaGNSESSCookie(GE5P.ge5p_localLanguage);
			}
		}
	},
	decodeHtml: function(html) {
		var txt = document.createElement("textarea");
		txt.innerHTML = html;
		return txt.value;
	},
	safeToJSON: function(item) { 
		// Test if Native JSON.stringify is true or false
		if(GE5P.native_json_support) {
			// True - supported
			return JSON.stringify(item);
		} else {
			// False - not supported
			var _array_tojson = Array.prototype.toJSON;
			delete Array.prototype.toJSON;
			var r = GE5P.ge5p_json_stringify(item);
			Array.prototype.toJSON = _array_tojson;
			return r;
		}
	},
	/*
	safeToJSON: function(item) { 
		if ( typeof Prototype !== "undefined" ) {
			if ( typeof Array.prototype.toJSON !== "undefined" ) {
				JSON.stringify = function(item) {
					var _array_tojson = Array.prototype.toJSON;
					delete Array.prototype.toJSON;
					var r = GE5P.ge5p_json_stringify(item);
					Array.prototype.toJSON = _array_tojson;
					return r;
				};
			} else {
				return JSON.stringify(item);
			}
		} else {
			return JSON.stringify(item);
		}
	},
	*/
	ORIGINALcheckLanguagePreference: function() {
	/*
	* 1. Check Local storage for es
	* 2. If es, redirect to MP session
	*/
		if(GE5P.ge5p_motionpoint) {
			GE5P.ge5p_teslaLSLocale = jQuery.jStorage.get('TESLALSLOCALE');
			if(GE5P.ge5p_teslaLSLocale == 'es') {
				// Check if url already has es
				if(GE5P.ge5p_websiteurlSource.indexOf('es-us') != -1) {
					// User is on a Spanish language page so no redirect is necessary
				} else {
					// User is on an English page so redirect to Spanish
					$this = jQuery('#ge5p_z1-change-language');
					var mp_url = $this.attr('data-href');
					var mp_lang = $this.attr('data-lang'); //es
					console.log('redirect to MP');
					//MP.switchLanguage(MP.tSite==mp_url?MP.oSite:mp_url, mp_lang);
				}
			}
		}
	},
	/*****************************************
	*
	*	NEW FUNCTIONS TO REPLACE PLUGINS
	*
	******************************************/
	getUrlBase: function() {
		return window.location.protocol.split(':')[0] + '://' + window.location.host + '/';
	},
	getUrlSource: function() {
		return window.location.protocol.replace(/\:/g,'') + '://' + window.location.host + window.location.pathname;
	},
	getUrlHostname: function() {
		return window.document.location.hostname;
	},
	getUrlHost: function() {
		return window.document.location.host;
	},
	getUrlRelative: function() {
		return window.document.location.pathname;
	},
	createCookie: function(name,value,days,domain,path) {
		if (days) {
			var date = new Date();
			date.setTime(date.getTime()+(days*24*60*60*1000));
			var expires = "; expires="+date.toGMTString();
		}
		else var expires = "";
		document.cookie = name+"="+GE5P.safeToJSON(value)+expires+"; path="+path+"; domain="+domain;
	},
	readCookie: function(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for(var i=0;i < ca.length;i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1,c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
		}
		return null;
	},
	eraseCookie: function(name) {
		createCookie(name,"",-1);
	}	
} // End GE5P Var

/* 
 * Confirm Document is in ready state
 * before firing Int() function
 */
var GE5PInit = {  
	/*	VAR UNDEFINED SECTION  */
	ge5p_interval: undefined,
	ge5p_timeout: undefined,
	ge5p_docReadyIntervalTime: undefined,
	ge5p_docReadyTimeoutTime: undefined,
	/* Set up function */
	setup: function () {
		if (typeof globalNavDefaultSelections != 'undefined') {
			if (typeof globalNavDefaultSelections.ge5p_docReadyIntervalTime == 'undefined') {
				GE5PInit.ge5p_docReadyIntervalTime = '500';
			} else {
				GE5PInit.ge5p_docReadyIntervalTime = globalNavDefaultSelections.ge5p_docReadyIntervalTime;
			}
			if (typeof globalNavDefaultSelections.ge5p_docReadyTimeoutTime == 'undefined') {
				GE5PInit.ge5p_docReadyTimeoutTime = '200';
			} else {
				GE5PInit.ge5p_docReadyTimeoutTime = globalNavDefaultSelections.ge5p_docReadyTimeoutTime;
			}
		} else {
			// Default Global Vars
			GE5PInit.ge5p_docReadyIntervalTime = '500';
			GE5PInit.ge5p_docReadyTimeoutTime = '200';
		}
	},    
	/*	FUNCTION SECTION  */
	/* Start Interval FUNCTION */
	/* Function to check if document is complete and in ready state
	**/
	startInitInterval: function () {
		/* Call to setup function */
		GE5PInit.setup();
		/* Polling if Document Complete */
		jQuery(document).ready(function() {
			/* Tesla Global Nav Set Timeout */
			GE5PInit.ge5p_timeout = setTimeout(function(){
				/* Tesla Global Nav Set Interval */
				GE5PInit.ge5p_interval = setInterval(function(){
					if ( document.readyState !== 'complete' ) return;
					GE5PInit.clearInitTimeout();
					GE5PInit.clearInitInterval();
						GE5P.Init();
				},GE5PInit.ge5p_docReadyIntervalTime);
			}, GE5PInit.ge5p_docReadyTimeoutTime);	
		});
	},
	clearInitInterval: function () {
	    clearInterval(GE5PInit.ge5p_interval); 
	},
	clearInitTimeout: function () {
	    clearTimeout(GE5PInit.ge5p_timeout); 
	}   
}
/* 
 * Call GE5PInit Function to start Tesla GN polling
 */
GE5PInit.startInitInterval();
// Invoke Motion Point script per the Environment (Production or Development)	
if(GE5P.ge5p_environmentProduction && GE5P.ge5p_motionpointProduction || !GE5P.ge5p_environmentProduction && GE5P.ge5p_motionpointProduction) {
	// Use Production Motion Point code here
	var MP = {
		<!--mp_trans_disable_start -->
		Version: '3.1.1.1',
		SrcLang: 'en',
		<!--mp_trans_disable_end -->
		SrcUrl: decodeURIComponent('mp_js_orgin_url'),
		oSite: decodeURIComponent('mp_js_origin_baseUrl'),
		tSite: decodeURIComponent('mp_js_translated_baseUrl'),
		<!--mp_trans_disable_start -->
		init: function() {
			if (MP.oSite.indexOf('p_js_') == 1) {
				MP.SrcUrl = window.top.document.location.href;
				MP.oSite = MP.tSite = window.top.document.location.host;
				MP.UrlLang = MP.SrcLang;
			}
		},
		switchLanguage: function(url, pref) {
			lang = pref.substring(0, 2);
			//if(pref.substring(0,2)!='en'){		
			var oSite = MP.oSite.replace('http://', '').replace('https://', '');
			var tSite = MP.tSite.replace('http://', '').replace('https://', '');
			url = url.replace('http://', '').replace('https://', '');
			try {
				if (pref && (typeof MpStorage !== 'undefined') && (typeof MpStorage.updatePref !== 'undefined')) MpStorage.updatePref(url, pref);
				setTimeout(function() {
					var script = document.createElement('SCRIPT');
					if (MP.UrlLang == lang) {
						script.src = location.protocol + '//' + location.host.split(/[/?#]/)[0] + '/' + MP.SrcLang + MP.UrlLang + '/?1023749634;' + encodeURIComponent(location.href);
					} else {
					 if(MP.SrcLang==lang && tSite == oSite){return false;}
						script.src = location.protocol + '//' + location.host.split(/[/?#]/)[0] + '/' + MP.SrcLang + lang + '/?1023749632;' + encodeURIComponent(MP.SrcUrl);
					}
					var target = document.getElementsByTagName('script')[0];
					target.parentNode.insertBefore(script, target);
				}, 500);
			} catch (e) {
				var lang = '';
			}
			return false;
		},
		switchToLang: function(url) {
			if (window.top.location.href == url) {
				if ((typeof MpStorage !== 'undefined') && (typeof MpStorage.updatePref !== 'undefined')) MpStorage.updatePref(MP.oSite, MP.SrcLang);
			} else {
				var ge5p_pageRepaintedSetInterval = setInterval(function() {
					if (GE5P.ge5p_pageRepainted) {
						clearInterval(ge5p_pageRepaintedSetInterval);
						// We know the Nav Service has completed, redirect to Motion Point
						window.top.location.href = url;
					}
				}, 500);
			}
		}
		<!-- mp_trans_disable_end -->   
	};
} else {
	// Use development Motion Point code here
	var MP = {
		<!--mp_trans_disable_start -->
		Version: '3.1.1.1',
		SrcLang: 'en',
		<!--mp_trans_disable_end -->
		SrcUrl: decodeURIComponent('mp_js_orgin_url'),
		oSite: decodeURIComponent('mp_js_origin_baseUrl'),
		tSite: decodeURIComponent('mp_js_translated_baseUrl'),
		<!--mp_trans_disable_start -->
		init: function() {
			if (MP.oSite.indexOf('p_js_') == 1) {
				MP.SrcUrl = window.top.document.location.href;
				MP.oSite = MP.tSite = window.top.document.location.host;
				MP.UrlLang = MP.SrcLang;
			}
		},
		switchLanguage: function(url, pref) {
			lang = pref.substring(0, 2);
			//if(pref.substring(0,2)!='en'){		
			var oSite = MP.oSite.replace('http://', '').replace('https://', '');
			var tSite = MP.tSite.replace('http://', '').replace('https://', '');
			url = url.replace('http://', '').replace('https://', '');
			try {
				if (pref && (typeof MpStorage !== 'undefined') && (typeof MpStorage.updatePref !== 'undefined')) MpStorage.updatePref(url, pref);
				setTimeout(function() {
					var script = document.createElement('SCRIPT');
					if (MP.UrlLang == lang) {
						script.src = location.protocol + '//es-mp.att.com/' + MP.SrcLang + MP.UrlLang + '/?1023749634;' + encodeURIComponent(location.href);
					} else {
					 if(MP.SrcLang==lang && tSite == oSite){return false;}
						script.src = location.protocol + '//es-mp.att.com/' + MP.SrcLang + lang + '/?1023749632;' + encodeURIComponent(MP.SrcUrl);
					}
					var target = document.getElementsByTagName('script')[0];
					target.parentNode.insertBefore(script, target);
				}, 500);
			} catch (e) {
				var lang = '';
			}
			return false;
		},
		switchToLang: function(url) {
			if (window.top.location.href == url) {
				if ((typeof MpStorage !== 'undefined') && (typeof MpStorage.updatePref !== 'undefined')) MpStorage.updatePref(MP.oSite, MP.SrcLang);
			} else {
				var ge5p_pageRepaintedSetInterval = setInterval(function() {
					if (GE5P.ge5p_pageRepainted) {
						clearInterval(ge5p_pageRepaintedSetInterval);
						// We know the Nav Service has completed, redirect to Motion Point
						window.top.location.href = url;
					}
				}, 500);
			}
		}
		<!-- mp_trans_disable_end -->   
	};
}
	
MP.SrcUrl= decodeURIComponent ('mp_js_orgin_url');
MP.UrlLang='mp_js_current_lang';
MP.oSite=decodeURIComponent('mp_js_origin_baseUrl'); 
MP.tSite=decodeURIComponent('mp_js_translated_baseUrl'); 
MP.init();

/* TESLA 1.3.0_SHOP Required JS support LIBS */
function scriptLoader(url, callback){
	var doc = document, script = doc.createElement("script");
	script.type = "text/javascript";

	if (callback == undefined) callback = function(){};
	
	if (script.readyState){//IE
		script.onreadystatechange = function(){
			if (script.readyState == "loaded" || script.readyState == "complete"){
				script.onreadystatechange = null;
				callback();
			}
		};
	} else {//Others
		script.onload = function(){
			callback();
		};
	}

	script.src = url;
	doc.getElementsByTagName("head")[0].appendChild(script);
}
//scriptLoader('../../../0.ecom.attccc.com/scripts/jquery-ui-1.8.11.custom.min.js'/*tpa=http://0.ecom.attccc.com/scripts/jquery-ui-1.8.11.custom.min.js*/);
/*!
 HTML5 Shiv v3.7.0 | @afarkas @jdalton @jon_neal @rem | MIT/GPL2 Licensed
*/
(function(e,t){function n(){var e=v.elements;return"string"==typeof e?e.split(" "):e}function r(e){var t=p[e[c]];t||(t={},h++,e[c]=h,p[h]=t);return t}function i(e,n,i){n||(n=t);if(d)return n.createElement(e);i||(i=r(n));n=i.cache[e]?i.cache[e].cloneNode():f.test(e)?(i.cache[e]=i.createElem(e)).cloneNode():i.createElem(e);return n.canHaveChildren&&!a.test(e)?i.frag.appendChild(n):n}function s(e,t){if(!t.cache)t.cache={},t.createElem=e.createElement,t.createFrag=e.createDocumentFragment,t.frag=t.createFrag();e.createElement=function(n){return!v.shivMethods?t.createElem(n):i(n,e,t)};e.createDocumentFragment=Function("h,f","return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&("+n().join().replace(/[\w\-]+/g,function(e){t.createElem(e);t.frag.createElement(e);return'c("'+e+'")'})+");return n}")(v,t.frag)}function o(e){e||(e=t);var n=r(e);if(v.shivCSS&&!l&&!n.hasCSS){var i,o=e;i=o.createElement("p");o=o.getElementsByTagName("head")[0]||o.documentElement;i.innerHTML="x<style>article,aside,dialog,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}mark{background:#FF0;color:#000}template{display:none}</style>";i=o.insertBefore(i.lastChild,o.firstChild);n.hasCSS=!!i}d||s(e,n);return e}var u=e.html5||{},a=/^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i,f=/^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i,l,c="_html5shiv",h=0,p={},d;(function(){try{var e=t.createElement("a");e.innerHTML="<xyz></xyz>";l="hidden"in e;var n;if(!(n=1==e.childNodes.length)){t.createElement("a");var r=t.createDocumentFragment();n="undefined"==typeof r.cloneNode||"undefined"==typeof r.createDocumentFragment||"undefined"==typeof r.createElement}d=n}catch(i){d=l=!0}})();var v={elements:u.elements||"abbr article aside audio bdi canvas data datalist details dialog figcaption figure footer header hgroup main mark meter nav output progress section summary template time video",version:"3.7.0",shivCSS:!1!==u.shivCSS,supportsUnknownElements:d,shivMethods:!1!==u.shivMethods,type:"default",shivDocument:o,createElement:i,createDocumentFragment:function(e,i){e||(e=t);if(d)return e.createDocumentFragment();for(var i=i||r(e),s=i.frag.cloneNode(),o=0,u=n(),a=u.length;o<a;o++)s.createElement(u[o]);return s}};e.html5=v;o(t)})(this,document);