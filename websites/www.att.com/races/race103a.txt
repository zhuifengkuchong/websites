<script id = "race103a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race103={};
	myVars.races.race103.varName="Object[3383].uuid";
	myVars.races.race103.varType="@varType@";
	myVars.races.race103.repairType = "@RepairType";
	myVars.races.race103.event1={};
	myVars.races.race103.event2={};
	myVars.races.race103.event1.id = "closeKey";
	myVars.races.race103.event1.type = "onkeypress";
	myVars.races.race103.event1.loc = "closeKey_LOC";
	myVars.races.race103.event1.isRead = "False";
	myVars.races.race103.event1.eventType = "@event1EventType@";
	myVars.races.race103.event2.id = "attPrintPageKey";
	myVars.races.race103.event2.type = "onkeypress";
	myVars.races.race103.event2.loc = "attPrintPageKey_LOC";
	myVars.races.race103.event2.isRead = "True";
	myVars.races.race103.event2.eventType = "@event2EventType@";
	myVars.races.race103.event1.executed= false;// true to disable, false to enable
	myVars.races.race103.event2.executed= false;// true to disable, false to enable
</script>

