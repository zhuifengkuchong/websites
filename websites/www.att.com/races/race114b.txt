<script id = "race114b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race114={};
	myVars.races.race114.varName="Object[3383].uuid";
	myVars.races.race114.varType="@varType@";
	myVars.races.race114.repairType = "@RepairType";
	myVars.races.race114.event1={};
	myVars.races.race114.event2={};
	myVars.races.race114.event1.id = "tabPressed";
	myVars.races.race114.event1.type = "onkeypress";
	myVars.races.race114.event1.loc = "tabPressed_LOC";
	myVars.races.race114.event1.isRead = "True";
	myVars.races.race114.event1.eventType = "@event1EventType@";
	myVars.races.race114.event2.id = "ge5p_search";
	myVars.races.race114.event2.type = "onkeypress";
	myVars.races.race114.event2.loc = "ge5p_search_LOC";
	myVars.races.race114.event2.isRead = "False";
	myVars.races.race114.event2.eventType = "@event2EventType@";
	myVars.races.race114.event1.executed= false;// true to disable, false to enable
	myVars.races.race114.event2.executed= false;// true to disable, false to enable
</script>

