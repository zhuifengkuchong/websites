<script id = "race120b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race120={};
	myVars.races.race120.varName="Object[3383].uuid";
	myVars.races.race120.varType="@varType@";
	myVars.races.race120.repairType = "@RepairType";
	myVars.races.race120.event1={};
	myVars.races.race120.event2={};
	myVars.races.race120.event1.id = "Lu_Id_input_6";
	myVars.races.race120.event1.type = "onchange";
	myVars.races.race120.event1.loc = "Lu_Id_input_6_LOC";
	myVars.races.race120.event1.isRead = "True";
	myVars.races.race120.event1.eventType = "@event1EventType@";
	myVars.races.race120.event2.id = "attPrintPageKey";
	myVars.races.race120.event2.type = "onchange";
	myVars.races.race120.event2.loc = "attPrintPageKey_LOC";
	myVars.races.race120.event2.isRead = "False";
	myVars.races.race120.event2.eventType = "@event2EventType@";
	myVars.races.race120.event1.executed= false;// true to disable, false to enable
	myVars.races.race120.event2.executed= false;// true to disable, false to enable
</script>

