<script id = "race122b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race122={};
	myVars.races.race122.varName="Object[3383].uuid";
	myVars.races.race122.varType="@varType@";
	myVars.races.race122.repairType = "@RepairType";
	myVars.races.race122.event1={};
	myVars.races.race122.event2={};
	myVars.races.race122.event1.id = "Lu_Id_input_4";
	myVars.races.race122.event1.type = "onchange";
	myVars.races.race122.event1.loc = "Lu_Id_input_4_LOC";
	myVars.races.race122.event1.isRead = "True";
	myVars.races.race122.event1.eventType = "@event1EventType@";
	myVars.races.race122.event2.id = "Lu_Id_input_5";
	myVars.races.race122.event2.type = "onchange";
	myVars.races.race122.event2.loc = "Lu_Id_input_5_LOC";
	myVars.races.race122.event2.isRead = "False";
	myVars.races.race122.event2.eventType = "@event2EventType@";
	myVars.races.race122.event1.executed= false;// true to disable, false to enable
	myVars.races.race122.event2.executed= false;// true to disable, false to enable
</script>

