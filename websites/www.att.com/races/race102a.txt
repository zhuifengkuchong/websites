<script id = "race102a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race102={};
	myVars.races.race102.varName="Object[3383].uuid";
	myVars.races.race102.varType="@varType@";
	myVars.races.race102.repairType = "@RepairType";
	myVars.races.race102.event1={};
	myVars.races.race102.event2={};
	myVars.races.race102.event1.id = "printPageKey";
	myVars.races.race102.event1.type = "onkeypress";
	myVars.races.race102.event1.loc = "printPageKey_LOC";
	myVars.races.race102.event1.isRead = "False";
	myVars.races.race102.event1.eventType = "@event1EventType@";
	myVars.races.race102.event2.id = "closeKey";
	myVars.races.race102.event2.type = "onkeypress";
	myVars.races.race102.event2.loc = "closeKey_LOC";
	myVars.races.race102.event2.isRead = "True";
	myVars.races.race102.event2.eventType = "@event2EventType@";
	myVars.races.race102.event1.executed= false;// true to disable, false to enable
	myVars.races.race102.event2.executed= false;// true to disable, false to enable
</script>

