<script id = "race28a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race28={};
	myVars.races.race28.varName="__onchange";
	myVars.races.race28.varType="@varType@";
	myVars.races.race28.repairType = "@RepairType";
	myVars.races.race28.event1={};
	myVars.races.race28.event2={};
	myVars.races.race28.event1.id = "Lu_DOM";
	myVars.races.race28.event1.type = "onDOMContentLoaded";
	myVars.races.race28.event1.loc = "Lu_DOM_LOC";
	myVars.races.race28.event1.isRead = "False";
	myVars.races.race28.event1.eventType = "@event1EventType@";
	myVars.races.race28.event2.id = "ge5p_search";
	myVars.races.race28.event2.type = "onchange";
	myVars.races.race28.event2.loc = "ge5p_search_LOC";
	myVars.races.race28.event2.isRead = "True";
	myVars.races.race28.event2.eventType = "@event2EventType@";
	myVars.races.race28.event1.executed= false;// true to disable, false to enable
	myVars.races.race28.event2.executed= false;// true to disable, false to enable
</script>

