<script id = "race126b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race126={};
	myVars.races.race126.varName="Object[3383].uuid";
	myVars.races.race126.varType="@varType@";
	myVars.races.race126.repairType = "@RepairType";
	myVars.races.race126.event1={};
	myVars.races.race126.event2={};
	myVars.races.race126.event1.id = "userid";
	myVars.races.race126.event1.type = "onchange";
	myVars.races.race126.event1.loc = "userid_LOC";
	myVars.races.race126.event1.isRead = "True";
	myVars.races.race126.event1.eventType = "@event1EventType@";
	myVars.races.race126.event2.id = "userPassword";
	myVars.races.race126.event2.type = "onchange";
	myVars.races.race126.event2.loc = "userPassword_LOC";
	myVars.races.race126.event2.isRead = "False";
	myVars.races.race126.event2.eventType = "@event2EventType@";
	myVars.races.race126.event1.executed= false;// true to disable, false to enable
	myVars.races.race126.event2.executed= false;// true to disable, false to enable
</script>

