<script id = "race127b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race127={};
	myVars.races.race127.varName="Object[3383].uuid";
	myVars.races.race127.varType="@varType@";
	myVars.races.race127.repairType = "@RepairType";
	myVars.races.race127.event1={};
	myVars.races.race127.event2={};
	myVars.races.race127.event1.id = "Lu_Id_input_3";
	myVars.races.race127.event1.type = "onchange";
	myVars.races.race127.event1.loc = "Lu_Id_input_3_LOC";
	myVars.races.race127.event1.isRead = "True";
	myVars.races.race127.event1.eventType = "@event1EventType@";
	myVars.races.race127.event2.id = "userid";
	myVars.races.race127.event2.type = "onchange";
	myVars.races.race127.event2.loc = "userid_LOC";
	myVars.races.race127.event2.isRead = "False";
	myVars.races.race127.event2.eventType = "@event2EventType@";
	myVars.races.race127.event1.executed= false;// true to disable, false to enable
	myVars.races.race127.event2.executed= false;// true to disable, false to enable
</script>

