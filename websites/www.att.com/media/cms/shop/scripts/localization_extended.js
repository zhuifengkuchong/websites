var isServiceNotAvailable = function(info) {
	isServiceNotAvailable.notavailURL = "";
/* replacement for legacy Localization.config*/
	var requiredL22 = false;
	var requiredWireless = false;
	var service = null;
	
    
	requiredL22FromOtherPages = jQuery.inArray(pageLocation(), [ 'http://www.att.com/media/cms/shop/scripts/shop.html','http://www.att.com/media/cms/shop/scripts/bundles.html','http://www.att.com/media/cms/shop/scripts/home-phone.html','http://www.att.com/media/cms/shop/scripts/internet.html','http://www.att.com/media/cms/shop/scripts/u-verse.html' ])==-1?false:true;
	requiredWirelessFromSNAPage = jQuery.inArray(info.localizationOriginatingPage, [ 'http://www.att.com/media/cms/shop/scripts/availability.service-not-available-zip.html' ])==-1?false:true; 
	requiredL22FromSNAPage = jQuery.inArray(info.localizationOriginatingPage, [ 'http://www.att.com/media/cms/shop/scripts/availability.service-not-available-zip.html' ])==-1?false:true;
	requiredWirelessOtherPages = /\/shop\/(\bwireless)/.test(window.location.href);
	requiredWireless = requiredWirelessFromSNAPage || requiredWirelessOtherPages;
	requiredL22 = requiredL22FromOtherPages || requiredL22FromSNAPage;
	matches = jQuery.inArray(pageLocation(), [ 'http://www.att.com/media/cms/shop/scripts/bundles.html','http://www.att.com/media/cms/shop/scripts/home-phone.html','http://www.att.com/media/cms/shop/scripts/internet.html','http://www.att.com/media/cms/shop/scripts/u-verse.html','http://www.att.com/media/cms/shop/scripts/wireless.html' ])==-1?false:true;
	var service = (matches) ? matches[0] : null;
/* replacement for legacy serviceNotAvailable*/
	var redirectL22 = (info.outOfL22FootPrint && requiredL22);
	var redirectWire = (info.outOfWirelessFootPrint &&  requiredWireless);
	var redirectOut = (info.outOfL22FootPrint && info.outOfWirelessFootPrint);
	if (redirectL22 || redirectWire || redirectOut) {
		var url = "/shop/availability.service-not-available-zip.html?saveme=true&q_zipCode=" + jQuery("#zipCodeEntry").val();
				if (service) { url += "&q_serviceType=" + service; }
				var messageCode = (redirectOut == true) ? 'wls.byf.err.WLSB22020' : (redirectL22 == true) ? 'wls.byf.err.WLSU22021' : 'wls.byf.err.WLSU22019';
				isServiceNotAvailable.notavailURL = url + "&q_message=" + messageCode;

		}
	log('redirectL22: '+redirectL22+' redirectWire: '+redirectWire+' redirectOut: '+redirectOut+'--'+(redirectL22 || redirectWire || redirectOut));
	return (redirectL22 || redirectWire || redirectOut);
};


var localizationCallback = function(pData) {

	var info = jQuery.parseJSON(pData);
/* BAW 11-15-11: added logging */
	log('callback new. val: '+jQuery("#zipCodeEntry").val());
	log('parsed!:' + info);
	log('outOfMarketRedirectUrl: '+ info.outOfMarketRedirectUrl +', errorCode: '+info.errorCode + ', outOfL22FootPrint: '+ info.outOfL22FootPrint+ ', outOfWirelessFootPrint: '+ info.outOfWirelessFootPrint);
	
	if (info.errorCode) {/* display error */
		log('errorcode');
		jQuery('.wait').css('display','none');/*hide Ajax wait image*/
		ATT.FEM.display.inline(info.errorCode,'Error',{selector:'#zipValidate'},false);
		jQuery('.errorMsg:first').html("<p><strong>Error Message</strong><ul><li>"+info.errorMessage+"</li></ul></p>");
		jQuery('.errorMsg:first').show();
		jQuery.colorbox.resize();
	}
	else if (isServiceNotAvailable(info)) {
		log('window location');
		jQuery('.errorMsg').hide();
		jQuery.ajax({url: templateSelector('http://www.att.com/media/cms/shop/scripts/localizationInclude.xhr'),cache: false,success: function(html){
			jQuery(html).prependTo("#footer");}
		});
		setTimeout( function(){
				jQuery('.wait').css('display','none');
				jQuery.colorbox.close();
				window.location = isServiceNotAvailable.notavailURL;
			}, 2000);   
	}
	else {
		if (!info.errorCode) {
			var hitURL = function(url, callback){
				var hitTest= document.createElement("script");
				if(typeof callback == "function"){
					hitTest.onload = function(){callback.apply(hitTest, [true])}
					hitTest.onerror = function(){callback.apply(hitTest, [false])}
					hitTest.onreadystatechange = function() {
						if(this.readyState == "complete" | this.readyState == "loaded"){
							this.onreadystatechange = null;
							callback.apply(this, [true]);
						}
					}
				}
				hitTest.src = url;
				document.getElementsByTagName("head")[0].appendChild(hitTest);
			}
			jQuery.getJSON(templateSelector('http://www.att.com/media/cms/shop/scripts/localizationInclude.xhr'), function(localizationData){
				var reverseLocalizationCallsMade =0, reverseLocalizationCallsReturned = 0, reverseLocalizationCallback = function(result){
					reverseLocalizationCallsReturned++;
	
					if(reverseLocalizationCallsMade === reverseLocalizationCallsReturned){
						jQuery.ajax({url: templateSelector('http://www.att.com/media/cms/shop/scripts/setLocalizationSession.xhr'),dataType:'text',success: function(){
							jQuery('.wait').css('display','none');
							if (info.outOfMarketRedirectUrl) {
								window.location.assign(info.outOfMarketRedirectUrl);
							} else if (typeof ATT.globalVars.pendingLocalizationForward != "undefined") {
								window.location = ATT.globalVars.pendingLocalizationForward;
							} else if (typeof info.localizationOriginatingPage!= "undefined" ) {
								window.location = info.localizationOriginatingPage;
							} else{
								jQuery.colorbox.close();
								var currentPageUrl = window.location.href.substring(0, window.location.href.indexOf('?'));
								var qs = window.location.href.substring(window.location.href.indexOf('?') + 1, window.location.href.length);
								if(qs.indexOf('outofmarketzip')!=-1){
									window.location = currentPageUrl;
								}else{
									location.reload(true);
								}
								
								
							}
						}}); 
					}
				};
				jQuery.each(localizationData.reverseLocalizationUrls, function(index, url){
					reverseLocalizationCallsMade++;
					hitURL(url, reverseLocalizationCallback);
				});
			});
		}
		if(typeof ATT.globalVars.pendingLocalizationForward != "undefined"){
			window.location = ATT.globalVars.pendingLocalizationForward;
		}else if(typeof info.localizationOriginatingPage!= "undefined" ){
			window.location = info.localizationOriginatingPage;
		}
	}
};
