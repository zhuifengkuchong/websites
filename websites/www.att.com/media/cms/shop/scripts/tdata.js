/**
 HTML API
 data-tdata="<inventorySpaceID>": defines the inventory space container and ID.
 *data-tdata-replace="<functionName>": Will replace contents of tag and execute functionName(oldContent, newContent)
 *callBackfunction (oldSpace, newSpace, offer): called after validation and creation of new content.
 oldSpace: jQuery object referencing div surrounding content to be replaced
 newSpace: jQuery object referencing div surrounding content to be shown
 offer: JSON reference to current offer.
 
 JQuery API
 getRecommendation(args, function)
    args: Array with the following
       domain = 'domain' in args ? args.domain : 'http://www.att.com/media/cms/shop/scripts/tdata-offers-dev.stage.att.com',
       versionMajor = 'versionMajor' in args ? args.versionMajor : '1',
       versionMinor = 'versionMinor' in args ? args.versionMinor : '2',
       consumptionEngine = 'consumptionEngine' in args ? args.consumptionEngine : 'SHOP'
       pageID = 'pageID' in args ? args.pageID : '1',
       onErrorFunction(data) = function to call in case of error from server
       function: called after content has been added to the DOM.
 invSpaceSizing(<InventorySpaceContainer>) : resizes dynamic content
*/

// Animates the removal and addition of spaces
var tdataPromise = jQuery.Deferred();

// Assumes space is not visible and fades in new
function defaultTransition(oldSpace, newSpace) {
   oldSpace.parent().replaceWith(newSpace);
   newSpace.hide().delay(100).css('visibility', 'visible').fadeIn();
}

(function($) {
   $.fn.getRecommendations = function(args, callBackfunction) {
      var domain = 'domain' in args && args.domain.length ? args.domain : 'http://www.att.com/media/cms/shop/scripts/tdata-offers.att.com',
         versionMajor = 'versionMajor' in args ? args.versionMajor : '1',
         versionMinor = 'versionMinor' in args ? args.versionMinor : '2',
         consumptionEngine = 'consumptionEngine' in args ? args.consumptionEngine : 'SHOP',
         deviceType = 'deviceType' in args ? args.deviceType : '1',
         pageID = 'pageID' in args ? args.pageID : '',
         serviceURL = 'serviceURL' in args ? args.serviceURL : 'https://' + domain + '/nt/TDiceRecommendations/' + versionMajor + '/' + versionMinor + '/recommendations/http/' + consumptionEngine + '/' + pageID + '?deviceType=' + deviceType,
         onErrorFunction = 'onErrorFunction' in args ? args.onErrorFunction : function() {
            tdataPromise.reject();
         };

      if (console) console.log("serviceURL: " + serviceURL);
      $.ajax({
         url: serviceURL,
         dataType: 'jsonp',
         contentType: "application/json",
         type: 'GET',
         timeout: (typeof tdataTimeout != 'undefined' && parseInt(tdataTimeout) >= 1000) ? parseInt(tdataTimeout) : 5000,
         success: function(data) {
            if (console) console.log("T-Data request success");
            if (data === null || typeof(data.offerRecord) == "undefined" || !data.offerRecord.length) {
               tdataPromise.reject();
               onErrorFunction(data);
            } else {
               tdataPromise.resolve(data);
            }
         },
         error: function(data) {
            if (console) console.log("T-Data request error: " + data.statusText);
            tdataPromise.reject();
            onErrorFunction(data);
         }
      });
   };

   /*
    * executeDataTdata:  Traverses the Recommendation Response and for each InventorySpace find the corresponding InventorySpace in the DOM and apply the functions according to the data-tdata custom attributes.
    
    * @param {type} data
    *      data is the raw JSON object.
    * @param {type} callBackfunction
    *      callback function to execute after the DOM has been updated.
    * @returns {unresolved} */
   $.fn.executeDataTdata = function(data, teaserId, callBackfunction) {
      var offers = data.offerRecord;
      
      // Iterate through offers and data-tdata attributes
      for (i = 0; i < offers.length; i++) {
         var offer = offers[i];

         var tdataArr = $('[data-tdata]');
         for (j = 0; j < tdataArr.length; j++) {
            var tdataObject = $(tdataArr[j]), replaceSpace = tdataObject.data('tdata-replace'), inventorySpaceID = tdataObject.data('tdata');
            // Check for matching inventorySpaceID.
            if (offer.inventorySpaceId === inventorySpaceID) {
               $().replaceContent(offer, tdataObject, replaceSpace);
               if(!teaserId) {teaserId = 'body';}
               var staticImpr = offer.externalContentId.match(/[^/]+$/)[0]+'~~'+teaserId+'~'+inventorySpaceID+'~~TDATA-HR';
               $('<meta/>', {name: 'DCSext.ctTSImprId', content: staticImpr}).appendTo('head');
               tdataObject.removeAttr('data-tdata');
               if (console) console.log('T-Data: Replacing ' + inventorySpaceID + ' with T-Data content');
            }
         }
      }

      if (typeof callBackfunction === 'function') {
         callBackfunction(data);
      }
      $('[data-tdata]').css('visibility','visible').removeAttr('data-tdata');
      return data;
   };
   
   $.fn.executeMarqueeTdata = function(data) {
      var offers = data.offerRecord, panels = JSAM_JSON.panel;
      for (var i = 0; i < offers.length; i++) {
         var offer = offers[i];
         for (var j = 0; j < panels.length; j++) {
            var panel = panels[j];
            if (panel.slotId == offer.inventorySpaceId) {
               try {
                  JSAM_JSON.panel[j] = $.parseJSON(offer.html);
                  JSAM_JSON.panel[j].cmsId = 'TDATA-HR';
                  JSAM_JSON.panel[j].slotId = offer.inventorySpaceId;
                  JSAM_JSON.panel[j].cNodeUUID = offer.externalContentId.match(/[^/]+$/)[0];
                  if (console) console.log('T-Data: Replacing marquee panel ' + j + ' (' + offer.inventorySpaceId + ') with T-Data content');
               } catch(e) {
                  // Keep the old panel
                  if (console && offer.inventorySpaceId) console.log('T-Data: Parse error for ' + offer.inventorySpaceId);
               }
            }
         }
      }
   };

   $.fn.replaceContent = function(offer, tdataArrayObject, callbackFunction) {
      var oldSpace = tdataArrayObject.children().wrapAll('<div />').parent();
      var newSpace;
      var fn = window[callbackFunction];
      if (typeof offer.html != 'undefined') {
         offer.html = offer.html.replace(/\\\"/g, "\"").replace(/\\\//g, "\\/").replace(/\\r/g, "\r").replace(/\\n/g, "\n").replace(/\\t/g, "\t").replace(/\\u([0-9a-f][0-9a-f][0-9a-f][0-9a-f])/gi, function(m, p1) { return "&#x" + p1 + ";"; });
         newSpace = $('<div/>').html(offer.html);
         newSpace.insertAfter(oldSpace);
      } else {
         newSpace = tdataArrayObject.children().wrapAll('<div />').parent();
      }
      newSpace.attr('class', tdataArrayObject.attr('class'));
      newSpace.attr('style', tdataArrayObject.attr('style'));
      newSpace.attr('data-tisid',offer.inventorySpaceId);
      newSpace.find('[data-tdata]').removeAttr('data-tdata');
      if (typeof fn === 'function') {
         fn(oldSpace, newSpace, offer);
      } else {
         defaultTransition(oldSpace, newSpace);
      }
      tdataArrayObject.removeAttr('data-tdata');
   };
}(jQuery));


jQuery(document).ready(function() {
   try {
      jQuery().getRecommendations({
            pageID: (typeof(tdataPageID) == 'undefined') ? '' : tdataPageID,
            domain: (typeof(tdataDomain) == 'undefined') ? '' : tdataDomain,
            onErrorFunction : function(data){
               jQuery('[data-tdata]').css('visibility','visible').removeAttr('data-tdata');
            }
         },
         function(){
            jQuery('[data-tdata]').css('visibility','visible').removeAttr('data-tdata');;
         }
      );
   } catch(err){
      console.log("TDATA error");
      console.log(err);
   };
});

function teaserCallback(teaserId) {
   jQuery('[data-teaserId="' + teaserId + '"] [data-tdata]').css('visibility', 'hidden');
   tdataPromise.then(function(data) {
      jQuery().executeDataTdata(data, teaserId);
   }, function() {
      jQuery('[data-tdata]').css('visibility', 'visible').removeAttr('data-tdata');
   });
}

function tdataMarquee(width, height) {
   tdataPromise.then(function(data) {
      jQuery().executeMarqueeTdata(data);
      JSAM.create({ json: JSAM_JSON, width: width, height: height });
   }, function() {
      JSAM.create({ json: JSAM_JSON, width: width, height: height });
   });
}
