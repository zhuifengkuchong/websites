/*
OnlineOpinion v5.8.4
Released: 08/25/2014. Compiled 08/25/2014 11:42:12 AM -0500
Branch: master 1df595f72e14470fd6e8ee17101ac621a6f7bf30
Components: Invite, feedback, tab
UMD: disabled
The following code is Copyright 1998-2014 Opinionlab, Inc. All rights reserved. Unauthorized use is prohibited. This product and other products of OpinionLab, Inc. are protected by U.S. Patent No. 6606581, 6421724, 6785717 B1 and other patents pending. http://www.opinionlab.com
*/

/* global window, OOo */

/* Invitation configuration */
(function (w, o) {
    'use strict';

    var OpinionLabInit = function () {

        var uaTest = /Android|iPhone|iPad|iPod/i.test(navigator.userAgent);

        /* Inline configuration */
        o.oo_feedback = new o.Ocode({
            customVariables: {
                s_vi: OOo.readCookie('s_vi'),
                pagename: typeof s !== 'undefined' ? (typeof s.pageName !== 'undefined' ? s.pageName : '' ) : ''
            }
        });

        /* [+] Tab Icon configuration */
        if (!uaTest) {
        o.oo_tab = new o.Ocode({
            tab: {},
            disableMobile: true, // hides floating icon on mobile and touch devices
            customVariables: {
                s_vi: OOo.readCookie('s_vi'),
                pagename: typeof s !== 'undefined' ? (typeof s.pageName !== 'undefined' ? s.pageName : '' ) : ''
            }
        });
        }

        /* Invitation configuration */
        o.oo_invite = new o.Invitation({
        /* REQUIRED - Asset identification */
            pathToAssets: '/onlineopinionV5/',
        /* OPTIONAL - Configuration */
            responseRate: 100,
            repromptTime: 2592000,
            promptTrigger: /products/i || /learning-center/i || /claims/i,
            promptDelay: 2,
            popupType: 'popunder',
            companyLogo: '../../img/logo.png'/*tpa=http://www.amfam.com/site-assets/img/logo.png*/,
            referrerRewrite: {
                searchPattern: /:\/\/[^\/]*/,
                replacePattern: '://invite.amfam.com'
            },
            customVariables: {
                s_vi: OOo.readCookie('s_vi'),
                pagename: typeof s !== 'undefined' ? (typeof s.pageName !== 'undefined' ? s.pageName : '' ) : ''
            }
        });


    };

    o.addEventListener(w, 'load', OpinionLabInit, false);


})(window, OOo);
