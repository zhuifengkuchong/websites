(function (){
	var urid = "NO_URID";
	var baseURL = 'http://ads.mydas.mobi/uridSetup?urid=';
	//extra value of mm_urid parameter from location.search
        var regex = new RegExp('[?&](mm\_)?urid=([^&]*)');
	var uridKeyval = regex.exec(location.search);
	if(uridKeyval) {
		urid = decodeURIComponent(uridKeyval[2]);
	} else if( window.location !== window.parent.location){ //might be in parent of iframe
		uridKeyval = regex.exec(document.referrer);
		if(uridKeyval) {
			urid = decodeURIComponent(uridKeyval[2]);
		}
	}
	//construct image to mm url
	var imgTag = document.createElement('img');
	imgTag.src = baseURL + urid;
})();
