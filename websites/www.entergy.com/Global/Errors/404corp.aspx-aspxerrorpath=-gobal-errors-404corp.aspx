<html>
    <head>
        <META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Entergy - 404 Error Page Requested Not Found</title>
        <meta name="" content=""><meta name="ROBOTS" content="NOARCHIVE"> 
<meta HTTP-EQUIV="imagetoolbar" content="no">
<link rel="SHORTCUT ICON" href="favicon.ico">

<link rel="stylesheet" href="/Global/CSS/styles.css" type="text/css">
<link rel="stylesheet" href="/Global/CSS/menu.css" type="text/css">
<link rel="stylesheet" href="/Global/CSS/news.css" type="text/css">
<link rel="Stylesheet" href="/Global/CSS/print.css" type="text/css" media="print" />

<script language="JavaScript" src="/Global/Navigation/Script/mm_menu.js" type="text/javascript"></script>
<script src="/Global/Script/AC_OETags.js" language="javascript"></script>
<script src="/Global/Script/AC_RunActiveContent.js" type="text/javascript"></script>


<!-- START OF SmartSource Data Collector TAG -->
<!-- Copyright (c) 1996-2012 Webtrends Inc.  All rights reserved. -->
<!-- Version: 9.4.0 -->
<!-- Tag Builder Version: 4.1  -->
<!-- Created: 8/16/2012 5:41:23 PM -->

<script src="/Global/Script/webtrends.js" type="text/javascript"></script>

<!-- ----------------------------------------------------------------------------------- -->
<!-- Warning: The two script blocks below must remain inline. Moving them to an external -->
<!-- JavaScript include file can cause serious problems with cross-domain tracking.      -->
<!-- ----------------------------------------------------------------------------------- -->

<script type="text/javascript">
    //<![CDATA[
    var _tag = new WebTrends();
    _tag.dcsGetId();
    //]]>
</script>

<script type="text/javascript">
    //<![CDATA[
    _tag.dcsCustom = function() {
        // Add custom parameters here.
        //_tag.DCSext.param_name=param_value;
    }
    _tag.dcsCollect();
    //]]>
</script>

<noscript>
    <div>
        <img alt="DCSIMG" id="DCSIMG" width="1" height="1" src="//sdc.entergy.com/dcsqf1lrwwzr9plsxntqsvovh_8u9j/njs.gif?dcsuri=/nojavascript&amp;WT.js=No&amp;WT.tv=9.4.0&amp;dcssip=www.entergy.com" /></div>
</noscript>
<!-- END OF SmartSource Data Collector TAG -->
</head>
    <body topmargin="0" text="#000000" rightmargin="0" leftmargin="0" bgcolor="#FFFFFF">
        <table class="wireframe-all" cellspacing="0" cellpadding="0" border="0" align="center">
            <tr valign="top">
                <td><!-- begin header -->                <script language="javascript" type="text/javascript">
                    function ValidateSearch(searchTerm) {
                        if (searchTerm == "Search..." || searchTerm == "") {
                            alert("Please Enter a Search String.");
                            return false;
                        }
                        else {
                            return true;
                        }
                    }
                </script>
                <table width="100%" cellspacing="0" cellpadding="0" border="0" class="wireframe-corp-header">
                    <tr valign="top">
                        <td width="22%" align="right" style="padding-right: 20px;">
                            <a href="http://www.entergy.com"><img vspace="5" src="/Global/images/Corporate/logo-entergy-reg.gif" alt="" border="0" /></a>
                        </td>
                        <td valign="bottom">
                            <table cellspacing="0" cellpadding="0" border="0" width="98%">
                                <tr class="wireframe-corp-header-link">
                                    <td colspan="2" align="right" valign="top">
                                        <div>
                                            Download Our App: <a href=https://itunes.apple.com/us/app/entergy/id529855219 onclick="dcsMultiTrack(
'DCS.dcsuri', '/click itunes download',
'WT.ti', 'itunes Download')" target="_parent"><img src="/global/images/icons/app-store.jpg" alt="Entergy iPhone App"
                                    border="0" /></a>
                                    
                                    
<a href="https://play.google.com/store/apps/details?id=com.zehnder.entergy" onclick="dcsMultiTrack(
'DCS.dcsuri', '/click android download',
'WT.ti', 'Andriod Download')" target="_parent"><img src="/global/images/icons/android.jpg" alt="Entergy Android App"
                                    border="0" /></a>

                                    
                                    
                                    
                                    <a href="/essentialaccessibility/"><img src="/Global/images/global/essential-accessibility_icon.jpg" alt="Accessibility" border="0"/>
                                            </a>
                                      </div>
                                        <div>
                                            <a href="http://www.entergynewsroom.com">Newsroom</a>&nbsp; |&nbsp; 
                                            <a href="http://www.entergy.com/Careers/">Careers</a>&nbsp; |&nbsp; 
                                            <a href="http://www.entergy.com/faq/">FAQ</a>&nbsp; |&nbsp; 
                                            <a href="http://www.entergy.com/sitemap.aspx">Site Map</a>&nbsp; |&nbsp; 
                                            <a href="http://www.entergy.com/Contact_Us/">Contact Us</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="bottom"><a href="http://www.entergy.com/residential/"><img border="0" style="margin-right:4px" src="/Global/images/tab-residential-normal.jpg" alt="" onmouseover="this.src='/Global/images/tab-residential-hover.jpg'" onmouseout="this.src='/Global/images/tab-residential-normal.jpg'" /></a><a href="http://www.entergy.com/business/"><img border="0" src="/Global/images/tab-business-normal.jpg" alt="" onmouseover="this.src='/Global/images/tab-business-hover.jpg'" onmouseout="this.src='/Global/images/tab-business-normal.jpg'" /></a></td>
                                    <td width="220px"><form class="search-form" action="http://entergy.dev.entergy.com/Search/search.aspx" name="corpSearchForm" method="post"><input type="text" class="search-header-textbox" name="corpSearch" id="SearchTerm" value="Search..." onblur="if(this.value=='')this.value='Search...';" onfocus="if(this.value=='Search...')this.value='';" onkeydown="if(event.keyCode==13){event.keyCode=9;document.getElementById('SubmitSearch').click();}" /><input type="hidden" name="collection" value="All Entergy" /><input id="SubmitSearch" type="image" src="/Global/images/Gold-arrow.jpg" border="0" onclick="return ValidateSearch(document.getElementById('SearchTerm').value);"/></form></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
<!-- end header --></td>
            </tr>
            <tr valign="top">
                <td>
                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                        <tr valign="middle"><!-- begin navigation --><td width="0"> <script type="text/javascript">  /* <![CDATA[ */ function mmLoadMenus() {  if (window.mm_menu_0) return; window.mm_menu_0 = new Menu("Menu0",175,19,"Trebuchet MS, Helvetica, Arial, sans-serif",10,"#2f2a24","#ffffff","#f5e5b8","#1F1F1F","LEFT","MIDDLE",5,0,1000,-5,7,true,true,true,0,true,true); mm_menu_0.addMenuItem("EntergyFACTS","window.open('http://entergy.com/about_entergy/entergy_facts.aspx', '_self');"); mm_menu_0.addMenuItem("Social Media","window.open('http://entergy.com/social_media/', '_self');"); mm_menu_0.addMenuItem("Values & Ethics","window.open('http://entergy.com/about_entergy/ethics.aspx', '_self');"); mm_menu_0.addMenuItem("Entergy Companies","window.open('http://entergy.com/about_entergy/company_links.aspx', '_self');"); mm_menu_0.addMenuItem("Leadership","window.open('http://entergy.com/about_entergy/leadership.aspx', '_self');"); mm_menu_0.addMenuItem("Diversity and Inclusion","window.open('http://entergy.com/about_entergy/diversity_and_inclusion.aspx', '_self');"); mm_menu_0.addMenuItem("Speeches and Presentations","window.open('http://entergy.com/about_entergy/speeches.aspx', '_self');"); mm_menu_0.addMenuItem("Awards & Recognition","window.open('http://www.entergynewsroom.com/awards/', '_self');"); mm_menu_0.addMenuItem("Entergy in the News","window.open('http://www.entergy.com/news_room/news_coverage.aspx', '_self');"); mm_menu_0.addMenuItem("Company History","window.open('http://entergy.com/about_entergy/history.aspx', '_self');"); mm_menu_0.addMenuItem("Affiliate Rules Compliance","window.open('http://entergy.com/about_entergy/affiliate.aspx', '_self');");   mm_menu_0.hideOnMouseOut=true;window.mm_menu_1 = new Menu("Menu1",200,19,"Trebuchet MS, Helvetica, Arial, sans-serif",10,"#2f2a24","#ffffff","#f5e5b8","#1F1F1F","LEFT","MIDDLE",5,0,1000,-5,7,true,true,true,0,true,true); mm_menu_1.addMenuItem("Events","window.open('http://investor.shareholder.com/entergy/events.cfm', '_self');"); mm_menu_1.addMenuItem("Financial Performance","window.open('http://investor.shareholder.com/entergy/results.cfm', '_self');"); mm_menu_1.addMenuItem("Publications","window.open('http://investor.shareholder.com/entergy/publications.cfm', '_self');"); mm_menu_1.addMenuItem("SEC Filings","window.open('http://investor.shareholder.com/entergy/sec.cfm', '_self');"); mm_menu_1.addMenuItem("Stock & Dividends","window.open('http://investor.shareholder.com/entergy/stockquote.cfm', '_self');"); mm_menu_1.addMenuItem("Shareholder Services/Transfer Agent","window.open('http://www.entergy.com/investor_relations/shareholder_services.aspx', '_self');"); mm_menu_1.addMenuItem("Corporate Governance","window.open('http://www.entergy.com/investor_relations/corporate_governance.aspx', '_self');"); mm_menu_1.addMenuItem("Securitization Filings","window.open('http://www.entergy.com/investor_relations/securitization_filings.aspx', '_self');"); mm_menu_1.addMenuItem("Investor Relations Team","window.open('http://www.entergy.com/investor_relations/contact_ir.aspx', '_self');");   mm_menu_1.hideOnMouseOut=true;window.mm_menu_2 = new Menu("Menu2",200,19,"Trebuchet MS, Helvetica, Arial, sans-serif",10,"#2f2a24","#ffffff","#f5e5b8","#1F1F1F","LEFT","MIDDLE",5,0,1000,-5,7,true,true,true,0,true,true); mm_menu_2.addMenuItem("Environmental Footprint","window.open('http://www.entergy.com/environment/carbon_footprint.aspx', '_self');"); mm_menu_2.addMenuItem("Proactive Adaptation","window.open('http://www.entergy.com/environment/adaptation.aspx', '_self');"); mm_menu_2.addMenuItem("Compliance Leadership","window.open('http://www.entergy.com/environment/compliance.aspx', '_self');"); mm_menu_2.addMenuItem("Energy Efficiency","window.open('http://www.entergy.com/environment/energy_efficiency.aspx', '_self');"); mm_menu_2.addMenuItem("Clean Generation","window.open('http://www.entergy.com/environment/CleanGen.aspx', '_self');"); mm_menu_2.addMenuItem("Stakeholder Engagement","window.open('http://www.entergy.com/environment/StakeEngage.aspx', '_self');");   mm_menu_2.hideOnMouseOut=true;window.mm_menu_3 = new Menu("Menu3",350,19,"Trebuchet MS, Helvetica, Arial, sans-serif",10,"#2f2a24","#ffffff","#f5e5b8","#1F1F1F","LEFT","MIDDLE",5,0,1000,-5,7,true,true,true,0,true,true); mm_menu_3.addMenuItem("Corporate Social Responsibility","window.open('http://www.entergy.com/csr/', '_self');"); mm_menu_3.addMenuItem("Corporate and Foundation Giving","window.open('http://www.entergy.com/csr/giving.aspx', '_self');"); mm_menu_3.addMenuItem("Customer Assistance","window.open('http://www.entergy.com/our_community/low_income.aspx', '_self');"); mm_menu_3.addMenuItem("The Power to Care","window.open('http://www.entergy.com/our_community/power_to_Care_Video.aspx', '_self');"); mm_menu_3.addMenuItem("Corporate Social Responsibility Awards and Recognition","window.open('http://www.entergy.com/csr/awards.aspx', '_self');"); mm_menu_3.addMenuItem("Economic Development","window.open('http://www.entergy.com/economic_development/', '_self');");   mm_menu_3.hideOnMouseOut=true;window.mm_menu_4 = new Menu("Menu4",300,19,"Trebuchet MS, Helvetica, Arial, sans-serif",10,"#2f2a24","#ffffff","#f5e5b8","#1F1F1F","LEFT","MIDDLE",5,0,1000,-5,7,true,true,true,0,true,true); mm_menu_4.addMenuItem("Supply Chain","window.open('http://entergy.com/operations_information/supply_chain/', '_self');"); mm_menu_4.addMenuItem("Generation Portfolio","window.open('http://entergy.com/operations_information/generation_portfolio.aspx', '_self');"); mm_menu_4.addMenuItem("Hydro Operations","window.open('http://entergy.com/operations_information/hydro/default.aspx', '_self');"); mm_menu_4.addMenuItem("Transmission Organization","window.open('http://www.entergy.com/energydelivery/default.aspx', '_self');"); mm_menu_4.addMenuItem("Energy Management Organization","window.open('http://www.entergy.com/operations_information/emo.aspx', '_self');"); mm_menu_4.addMenuItem("Regulated Gas Generation Req.","window.open('https://spofossil.entergy.com/gaspost/SEND/index.htm', '_self');"); mm_menu_4.addMenuItem("ESI (System Planning and Operations) Requests for Proposals","window.open('https://spofossil.entergy.com', '_self');");   mm_menu_4.hideOnMouseOut=true;window.mm_menu_5 = new Menu("Menu5",270,19,"Trebuchet MS, Helvetica, Arial, sans-serif",10,"#2f2a24","#ffffff","#f5e5b8","#1F1F1F","LEFT","MIDDLE",5,0,1000,-5,7,true,true,true,0,true,true); mm_menu_5.addMenuItem("Electricity 101","window.open('http://www.entergy.com/energy_education/industry_info.aspx', '_self');"); mm_menu_5.addMenuItem("How Power Plants Work","window.open('http://www.entergy.com/energy_education/power_plants.aspx', '_self');"); mm_menu_5.addMenuItem("Electrical and Gas Safety","window.open('http://www.entergy.com/energy_education/safety.aspx', '_self');"); mm_menu_5.addMenuItem("Electrical Safety for Kids, Parents and Teachers","window.open('http://www.entergy.com/global/safety/esworld/index.html', '_self');"); mm_menu_5.addMenuItem("Save Money on Your Bill","window.open('http://www.entergy.com/save_money/', '_self');");   mm_menu_5.hideOnMouseOut=true;window.mm_menu_6 = new Menu("Menu6",225,19,"Trebuchet MS, Helvetica, Arial, sans-serif",10,"#2f2a24","#ffffff","#f5e5b8","#1F1F1F","LEFT","MIDDLE",5,0,1000,-5,7,true,true,true,0,true,true); mm_menu_6.addMenuItem("Entergy Arkansas, Inc.","window.open('http://www.entergy-arkansas.com/', '_self');"); mm_menu_6.addMenuItem("Entergy Louisiana, LLC","window.open('http://www.entergy-louisiana.com', '_self');"); mm_menu_6.addMenuItem("Entergy Gulf States Louisiana, L.L.C.","window.open('http://www.entergy-louisiana.com', '_self');"); mm_menu_6.addMenuItem("Entergy Mississippi, Inc.","window.open('http://www.entergy-mississippi.com', '_self');"); mm_menu_6.addMenuItem("Entergy New Orleans, Inc.","window.open('http://www.entergy-neworleans.com/', '_self');"); mm_menu_6.addMenuItem("Entergy Texas, Inc.","window.open('http://www.entergy-texas.com', '_self');"); mm_menu_6.addMenuItem("Entergy Nuclear","window.open('http://www.entergy-nuclear.com', '_self');"); mm_menu_6.addMenuItem("Entergy Wholesale Commodities","window.open('http://entergy.com/ewc/', '_self');");   mm_menu_6.hideOnMouseOut=true;mm_menu_6.writeMenus();} /* ]]> */ </script> </td><td><table width="100%" cellspacing="0" cellpadding="0" border="0" class="top-navigation-area"><tr style="height: 0px; background-color: transparent "><td colspan="13"><script language="javascript" type="text/javascript">mmLoadMenus();</script></td></tr><tr><td nowrap="nowrap" id="td0"><a id="menu0" href="http://entergy.com/about_entergy/" onMouseOut="MM_startTimeout();" onMouseOver="MM_showMenu(window.mm_menu_0,-4,20,null,'menu0','td0',-99 ,7);" style="background-color:transparent;" >About Us</a></td><td>|</td><td nowrap="nowrap" id="td1"><a id="menu1" href="http://www.entergy.com/investor_relations/default.aspx" onMouseOut="MM_startTimeout();" onMouseOver="MM_showMenu(window.mm_menu_1,-4,20,null,'menu1','td1',-99 ,7);" style="background-color:transparent;" >Investor Relations</a></td><td>|</td><td nowrap="nowrap" id="td2"><a id="menu2" href="http://www.entergy.com/environment/" onMouseOut="MM_startTimeout();" onMouseOver="MM_showMenu(window.mm_menu_2,-4,20,null,'menu2','td2',-99 ,7);" style="background-color:transparent;" >Environment 2020</a></td><td>|</td><td nowrap="nowrap" id="td3"><a id="menu3" href="http://www.entergy.com/our_community/" onMouseOut="MM_startTimeout();" onMouseOver="MM_showMenu(window.mm_menu_3,-4,20,null,'menu3','td3',-99 ,7);" style="background-color:transparent;" >Our Community</a></td><td>|</td><td nowrap="nowrap" id="td4"><a id="menu4" href="http://entergy.com/operations_information/" onMouseOut="MM_startTimeout();" onMouseOver="MM_showMenu(window.mm_menu_4,-4,20,null,'menu4','td4',-99 ,7);" style="background-color:transparent;" >Operations Information</a></td><td>|</td><td nowrap="nowrap" id="td5"><a id="menu5" href="http://www.entergy.com/energy_education/" onMouseOut="MM_startTimeout();" onMouseOver="MM_showMenu(window.mm_menu_5,-4,20,null,'menu5','td5',-99 ,7);" style="background-color:transparent;" >Energy Education & Safety</a></td><td>|</td><td nowrap="nowrap" id="td6"><a id="menu6" href="http://www.entergy.com/about_entergy/company_links.aspx" onMouseOut="MM_startTimeout();" onMouseOver="MM_showMenu(window.mm_menu_6,-4,20,null,'menu6','td6',-99 ,7);" style="background-color:transparent;" >Entergy Companies</a></td></tr></table></td><!-- end navigation --></tr>
                    </table>
                </td>
            </tr>
            <tr valign="top">
                <td class="wireframe-interior-content-area">
                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                        <tr valign="top">
                            <td class="wireframe-interior-content-area-buffer-row"></td>
                        </tr>
                        <tr valign="top">
                            <td>
                                <table width="100%" class="wireframe-candu-bread-crumbs-area" cellspacing="0" cellpadding="0" border="0">
                                    <tr valign="top">
                                        <td width="80%" class="default-font-style"><!-- begin bread crumbs area --><table width="100%" class="wireframe-candu-bread-crumbs-table" cellspacing="0" cellpadding="0" border="0">
                                                <tr valign="middle">
                                                    <td class="bread-crumbs-font-style" align="left"></td>
                                                </tr>
                                            </table><!-- end bread crumbs area --></td><td width="20%"><!-- begin print area --><table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                <tr valign="middle"></tr>
                                            </table><!-- end print area --></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" class="wireframe-candu-interior-page-content-table" cellspacing="0" cellpadding="0" border="0">
                                    <tr valign="top">
                                        <td class="wireframe-candu-interior-page-main-content-no-nav"><!-- begin main content area --><table width="100%" class="wireframe-candu-interior-page-main-content-stacking-table" cellspacing="0" cellpadding="0" border="0">
                                                <tr valign="top">
                                                    <td align="left"><table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tr valign="top">
        <td class="interior-content-text">
The page you requested cannot be found. The page you are looking for might 
have been removed, had its name changed, or is temporarily unavailable.
<p>


            </p>
Please try the following:<br xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">&bull; If you typed the page address in the Address 
bar, make sure that it is spelled correctly.<br xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">&bull; Open the <a xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" href="http://www.entergy.com">home 
page</a> and look for links to the information you want.<br xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">&bull; Use the 
navigation bar above to find the link you are looking for.<br xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">&bull; Click the 
Back button to try another link.<br xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">&bull; Enter a term in the search form above 
to look for information on Entergy.com sites.
<p>


            </p>

            <b xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">NOTE: If you bookmarked a page on the Entergy site before 
12/01/05, the bookmark will no longer work because on that day our new web 
site went online with new page addresses. However, the main address of the new 
site is the same as the old site: <a href="http://www.entergy.com">www.entergy.com</a>. 
</b>
</td>
    </tr>
</table>
</td>
                                                </tr>
                                            </table><!-- end main content area --></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%" class="wireframe-bottom-graphic-area" cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <td colspan="3"><img src="/Global/Images/Common/bottom_border.jpg" border="0"></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr valign="top">
                <td>
                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                        <tr valign="top" class="wireframe-footer">
                            <td align="center"><!-- Begin Footer --><table cellspacing="0" cellpadding="0" border="0">
    <tr>
        <td valign="top">
            <img src="/Global/images/Corporate/line_of_people_interior.jpg" alt="" />
        </td>
        <td style="width: 310px; vertical-align: bottom;">
            <div style="height: 70px;width:230px;">
                <div style="float: left;">
                    <a href="http://entergy.com/social_media/facebook.aspx" style="width: 28px;">
                        <img src="/Global/images/global/icon_facebook.jpg" alt="Facebook" border="0" style="border: 0px;" />
                    </a>
                </div>
                <div style="float: left;">
                    <a style="width: 28px;" href="http://entergy.com/social_media/twitter.aspx">
                        <img src="/Global/images/global/icon_twitter.jpg" alt="Twitter" border="0" style="border: 0px;" />
                    </a>
                </div>
                
                <div style="float: left;">
                    <a style="width: 28px;" href="https://plus.google.com/111631769444254162638" target="_blank">
                        <img src="/Global/images/global/icon_google_plus.jpg" alt="Google Plus" border="0" style="border: 0px;" />
                    </a>
                </div>
                
                <div style="float: left;">
                    <a style="width: 28px;" href="http://www.linkedin.com/company/entergy" target="_blank">
                        <img src="/Global/images/global/icon_linked_in.jpg" alt="Linked In" border="0" style="border: 0px;" />
                    </a>
                </div>
                
                <div style="float: left;">
                    <a href="http://www.youtube.com/entergy" target="_blank" style="width: 28px;">
                        <img src="/Global/images/global/icon_Utube.jpg" alt="YouTube" border="0" style="border: 0px;" />
                    </a>
                </div>
                <div style="float: left;">
                    <a style="width: 28px;" href="http://vimeo.com/entergy" target="_blank">
                        <img src="/Global/images/global/icon_vimeo.jpg" alt="Vimeo" border="0" style="border: 0px;" />
                    </a>
                </div>
                 <div style="float: left;">
                    <a style="width: 28px;" href="http://entergy.com/social_media/flickr.aspx">
                        <img src="/Global/images/global/icon_flickr.jpg" alt="Flickr" border="0" style="border: 0px;" />
                    </a>
                </div>
 				
 				

                
                <div style="float: left;">
                    <a style="width: 28px;" href="http://www.entergynewsroom.com/feed/news/">
                        <img style="border: 0;" src="/Global/Images/global/icon_rssfeed.jpg" alt="RSS Feed" />
                    </a>
                </div>
                <div style="float: left;">
                    <a style="width: 100px;" href="http://stormcenter.entergy.com">
                        <img style="border: 0;" src="/Global/images/global/icon_StormCenter.jpg" alt="Entergy Storm Center" />
                    </a>
                </div>
                <div style="float: left;">
                    <a style="width: 100px;" href="http://entergy.com/storm_center/outages.aspx">
                        <img style="border: 0;" src="/Global/images/global/icon_view_outage_sm_.jpg" alt="View Outages" />
                    </a>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="copyright-notice footer-area">
            <a href="http://entergy.com/privacy_legal/privacy.aspx">Privacy Policy</a> | <a href="http://entergy.com/privacy_legal/legal.aspx">
                Terms &amp; Conditions</a>
            <br />
            &#169; 1998-2015 Entergy Corporation, All Rights Reserved. The Entergy name and
            logo are registered service marks of
            <br />
            Entergy Corporation and may not be used without the express, written consent of
            Entergy Corporation.
        </td>
    </tr>
</table>
<!-- End Footer --></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>
