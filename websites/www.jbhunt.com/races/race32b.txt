<script id = "race32b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race32={};
	myVars.races.race32.varName="Lu_Id_a_17__onfocus";
	myVars.races.race32.varType="@varType@";
	myVars.races.race32.repairType = "@RepairType";
	myVars.races.race32.event1={};
	myVars.races.race32.event2={};
	myVars.races.race32.event1.id = "Lu_Id_a_17";
	myVars.races.race32.event1.type = "onfocus";
	myVars.races.race32.event1.loc = "Lu_Id_a_17_LOC";
	myVars.races.race32.event1.isRead = "True";
	myVars.races.race32.event1.eventType = "@event1EventType@";
	myVars.races.race32.event2.id = "Lu_window";
	myVars.races.race32.event2.type = "onload";
	myVars.races.race32.event2.loc = "Lu_window_LOC";
	myVars.races.race32.event2.isRead = "False";
	myVars.races.race32.event2.eventType = "@event2EventType@";
	myVars.races.race32.event1.executed= false;// true to disable, false to enable
	myVars.races.race32.event2.executed= false;// true to disable, false to enable
</script>

