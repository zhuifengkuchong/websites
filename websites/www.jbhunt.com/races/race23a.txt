<script id = "race23a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race23={};
	myVars.races.race23.varName="Lu_Id_a_9__onfocus";
	myVars.races.race23.varType="@varType@";
	myVars.races.race23.repairType = "@RepairType";
	myVars.races.race23.event1={};
	myVars.races.race23.event2={};
	myVars.races.race23.event1.id = "Lu_window";
	myVars.races.race23.event1.type = "onload";
	myVars.races.race23.event1.loc = "Lu_window_LOC";
	myVars.races.race23.event1.isRead = "False";
	myVars.races.race23.event1.eventType = "@event1EventType@";
	myVars.races.race23.event2.id = "Lu_Id_a_9";
	myVars.races.race23.event2.type = "onfocus";
	myVars.races.race23.event2.loc = "Lu_Id_a_9_LOC";
	myVars.races.race23.event2.isRead = "True";
	myVars.races.race23.event2.eventType = "@event2EventType@";
	myVars.races.race23.event1.executed= false;// true to disable, false to enable
	myVars.races.race23.event2.executed= false;// true to disable, false to enable
</script>

