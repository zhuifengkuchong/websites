<script id = "race130a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race130={};
	myVars.races.race130.varName="Window[26].currentFocus";
	myVars.races.race130.varType="@varType@";
	myVars.races.race130.repairType = "@RepairType";
	myVars.races.race130.event1={};
	myVars.races.race130.event2={};
	myVars.races.race130.event1.id = "Lu_Id_a_14";
	myVars.races.race130.event1.type = "onfocus";
	myVars.races.race130.event1.loc = "Lu_Id_a_14_LOC";
	myVars.races.race130.event1.isRead = "False";
	myVars.races.race130.event1.eventType = "@event1EventType@";
	myVars.races.race130.event2.id = "Lu_Id_a_15";
	myVars.races.race130.event2.type = "onfocus";
	myVars.races.race130.event2.loc = "Lu_Id_a_15_LOC";
	myVars.races.race130.event2.isRead = "False";
	myVars.races.race130.event2.eventType = "@event2EventType@";
	myVars.races.race130.event1.executed= false;// true to disable, false to enable
	myVars.races.race130.event2.executed= false;// true to disable, false to enable
</script>

