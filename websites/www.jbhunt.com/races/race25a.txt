<script id = "race25a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race25={};
	myVars.races.race25.varName="Lu_Id_a_11__onfocus";
	myVars.races.race25.varType="@varType@";
	myVars.races.race25.repairType = "@RepairType";
	myVars.races.race25.event1={};
	myVars.races.race25.event2={};
	myVars.races.race25.event1.id = "Lu_window";
	myVars.races.race25.event1.type = "onload";
	myVars.races.race25.event1.loc = "Lu_window_LOC";
	myVars.races.race25.event1.isRead = "False";
	myVars.races.race25.event1.eventType = "@event1EventType@";
	myVars.races.race25.event2.id = "Lu_Id_a_11";
	myVars.races.race25.event2.type = "onfocus";
	myVars.races.race25.event2.loc = "Lu_Id_a_11_LOC";
	myVars.races.race25.event2.isRead = "True";
	myVars.races.race25.event2.eventType = "@event2EventType@";
	myVars.races.race25.event1.executed= false;// true to disable, false to enable
	myVars.races.race25.event2.executed= false;// true to disable, false to enable
</script>

