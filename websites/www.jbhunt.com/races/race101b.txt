<script id = "race101b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race101={};
	myVars.races.race101.varName="Object[1497].triggered";
	myVars.races.race101.varType="@varType@";
	myVars.races.race101.repairType = "@RepairType";
	myVars.races.race101.event1={};
	myVars.races.race101.event2={};
	myVars.races.race101.event1.id = "searchForm:searchInput";
	myVars.races.race101.event1.type = "onkeypress";
	myVars.races.race101.event1.loc = "searchForm:searchInput_LOC";
	myVars.races.race101.event1.isRead = "True";
	myVars.races.race101.event1.eventType = "@event1EventType@";
	myVars.races.race101.event2.id = "Lu_DOM";
	myVars.races.race101.event2.type = "onDOMContentLoaded";
	myVars.races.race101.event2.loc = "Lu_DOM_LOC";
	myVars.races.race101.event2.isRead = "False";
	myVars.races.race101.event2.eventType = "@event2EventType@";
	myVars.races.race101.event1.executed= false;// true to disable, false to enable
	myVars.races.race101.event2.executed= false;// true to disable, false to enable
</script>

