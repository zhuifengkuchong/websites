<script id = "race20a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race20={};
	myVars.races.race20.varName="Lu_Id_a_6__onfocus";
	myVars.races.race20.varType="@varType@";
	myVars.races.race20.repairType = "@RepairType";
	myVars.races.race20.event1={};
	myVars.races.race20.event2={};
	myVars.races.race20.event1.id = "Lu_window";
	myVars.races.race20.event1.type = "onload";
	myVars.races.race20.event1.loc = "Lu_window_LOC";
	myVars.races.race20.event1.isRead = "False";
	myVars.races.race20.event1.eventType = "@event1EventType@";
	myVars.races.race20.event2.id = "Lu_Id_a_6";
	myVars.races.race20.event2.type = "onfocus";
	myVars.races.race20.event2.loc = "Lu_Id_a_6_LOC";
	myVars.races.race20.event2.isRead = "True";
	myVars.races.race20.event2.eventType = "@event2EventType@";
	myVars.races.race20.event1.executed= false;// true to disable, false to enable
	myVars.races.race20.event2.executed= false;// true to disable, false to enable
</script>

