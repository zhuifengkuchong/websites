<script id = "race26b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race26={};
	myVars.races.race26.varName="transportation_management_mn__onfocus";
	myVars.races.race26.varType="@varType@";
	myVars.races.race26.repairType = "@RepairType";
	myVars.races.race26.event1={};
	myVars.races.race26.event2={};
	myVars.races.race26.event1.id = "transportation_management_mn";
	myVars.races.race26.event1.type = "onfocus";
	myVars.races.race26.event1.loc = "transportation_management_mn_LOC";
	myVars.races.race26.event1.isRead = "True";
	myVars.races.race26.event1.eventType = "@event1EventType@";
	myVars.races.race26.event2.id = "Lu_window";
	myVars.races.race26.event2.type = "onload";
	myVars.races.race26.event2.loc = "Lu_window_LOC";
	myVars.races.race26.event2.isRead = "False";
	myVars.races.race26.event2.eventType = "@event2EventType@";
	myVars.races.race26.event1.executed= false;// true to disable, false to enable
	myVars.races.race26.event2.executed= false;// true to disable, false to enable
</script>

