<script id = "race91a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race91={};
	myVars.races.race91.varName="Lu_Id_a_71__onfocus";
	myVars.races.race91.varType="@varType@";
	myVars.races.race91.repairType = "@RepairType";
	myVars.races.race91.event1={};
	myVars.races.race91.event2={};
	myVars.races.race91.event1.id = "Lu_window";
	myVars.races.race91.event1.type = "onload";
	myVars.races.race91.event1.loc = "Lu_window_LOC";
	myVars.races.race91.event1.isRead = "False";
	myVars.races.race91.event1.eventType = "@event1EventType@";
	myVars.races.race91.event2.id = "Lu_Id_a_71";
	myVars.races.race91.event2.type = "onfocus";
	myVars.races.race91.event2.loc = "Lu_Id_a_71_LOC";
	myVars.races.race91.event2.isRead = "True";
	myVars.races.race91.event2.eventType = "@event2EventType@";
	myVars.races.race91.event1.executed= false;// true to disable, false to enable
	myVars.races.race91.event2.executed= false;// true to disable, false to enable
</script>

