<script id = "race48a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race48={};
	myVars.races.race48.varName="careers_mn__onfocus";
	myVars.races.race48.varType="@varType@";
	myVars.races.race48.repairType = "@RepairType";
	myVars.races.race48.event1={};
	myVars.races.race48.event2={};
	myVars.races.race48.event1.id = "Lu_window";
	myVars.races.race48.event1.type = "onload";
	myVars.races.race48.event1.loc = "Lu_window_LOC";
	myVars.races.race48.event1.isRead = "False";
	myVars.races.race48.event1.eventType = "@event1EventType@";
	myVars.races.race48.event2.id = "careers_mn";
	myVars.races.race48.event2.type = "onfocus";
	myVars.races.race48.event2.loc = "careers_mn_LOC";
	myVars.races.race48.event2.isRead = "True";
	myVars.races.race48.event2.eventType = "@event2EventType@";
	myVars.races.race48.event1.executed= false;// true to disable, false to enable
	myVars.races.race48.event2.executed= false;// true to disable, false to enable
</script>

