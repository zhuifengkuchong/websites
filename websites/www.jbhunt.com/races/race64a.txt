<script id = "race64a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race64={};
	myVars.races.race64.varName="Lu_Id_a_44__onfocus";
	myVars.races.race64.varType="@varType@";
	myVars.races.race64.repairType = "@RepairType";
	myVars.races.race64.event1={};
	myVars.races.race64.event2={};
	myVars.races.race64.event1.id = "Lu_window";
	myVars.races.race64.event1.type = "onload";
	myVars.races.race64.event1.loc = "Lu_window_LOC";
	myVars.races.race64.event1.isRead = "False";
	myVars.races.race64.event1.eventType = "@event1EventType@";
	myVars.races.race64.event2.id = "Lu_Id_a_44";
	myVars.races.race64.event2.type = "onfocus";
	myVars.races.race64.event2.loc = "Lu_Id_a_44_LOC";
	myVars.races.race64.event2.isRead = "True";
	myVars.races.race64.event2.eventType = "@event2EventType@";
	myVars.races.race64.event1.executed= false;// true to disable, false to enable
	myVars.races.race64.event2.executed= false;// true to disable, false to enable
</script>

