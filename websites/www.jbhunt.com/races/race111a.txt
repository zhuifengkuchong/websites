<script id = "race111a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race111={};
	myVars.races.race111.varName="Window[26].currentFocus";
	myVars.races.race111.varType="@varType@";
	myVars.races.race111.repairType = "@RepairType";
	myVars.races.race111.event1={};
	myVars.races.race111.event2={};
	myVars.races.race111.event1.id = "searchForm:searchInput";
	myVars.races.race111.event1.type = "onfocus";
	myVars.races.race111.event1.loc = "searchForm:searchInput_LOC";
	myVars.races.race111.event1.isRead = "False";
	myVars.races.race111.event1.eventType = "@event1EventType@";
	myVars.races.race111.event2.id = "searchForm:searchButton";
	myVars.races.race111.event2.type = "onfocus";
	myVars.races.race111.event2.loc = "searchForm:searchButton_LOC";
	myVars.races.race111.event2.isRead = "False";
	myVars.races.race111.event2.eventType = "@event2EventType@";
	myVars.races.race111.event1.executed= false;// true to disable, false to enable
	myVars.races.race111.event2.executed= false;// true to disable, false to enable
</script>

