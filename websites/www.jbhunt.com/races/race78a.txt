<script id = "race78a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race78={};
	myVars.races.race78.varName="Lu_Id_a_58__onfocus";
	myVars.races.race78.varType="@varType@";
	myVars.races.race78.repairType = "@RepairType";
	myVars.races.race78.event1={};
	myVars.races.race78.event2={};
	myVars.races.race78.event1.id = "Lu_window";
	myVars.races.race78.event1.type = "onload";
	myVars.races.race78.event1.loc = "Lu_window_LOC";
	myVars.races.race78.event1.isRead = "False";
	myVars.races.race78.event1.eventType = "@event1EventType@";
	myVars.races.race78.event2.id = "Lu_Id_a_58";
	myVars.races.race78.event2.type = "onfocus";
	myVars.races.race78.event2.loc = "Lu_Id_a_58_LOC";
	myVars.races.race78.event2.isRead = "True";
	myVars.races.race78.event2.eventType = "@event2EventType@";
	myVars.races.race78.event1.executed= false;// true to disable, false to enable
	myVars.races.race78.event2.executed= false;// true to disable, false to enable
</script>

