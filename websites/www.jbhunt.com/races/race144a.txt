<script id = "race144a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race144={};
	myVars.races.race144.varName="Window[26].currentFocus";
	myVars.races.race144.varType="@varType@";
	myVars.races.race144.repairType = "@RepairType";
	myVars.races.race144.event1={};
	myVars.races.race144.event2={};
	myVars.races.race144.event1.id = "Lu_Id_a_26";
	myVars.races.race144.event1.type = "onfocus";
	myVars.races.race144.event1.loc = "Lu_Id_a_26_LOC";
	myVars.races.race144.event1.isRead = "False";
	myVars.races.race144.event1.eventType = "@event1EventType@";
	myVars.races.race144.event2.id = "Lu_Id_a_27";
	myVars.races.race144.event2.type = "onfocus";
	myVars.races.race144.event2.loc = "Lu_Id_a_27_LOC";
	myVars.races.race144.event2.isRead = "False";
	myVars.races.race144.event2.eventType = "@event2EventType@";
	myVars.races.race144.event1.executed= false;// true to disable, false to enable
	myVars.races.race144.event2.executed= false;// true to disable, false to enable
</script>

