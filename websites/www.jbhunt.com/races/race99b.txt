<script id = "race99b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race99={};
	myVars.races.race99.varName="searchForm:searchInput__onfocus";
	myVars.races.race99.varType="@varType@";
	myVars.races.race99.repairType = "@RepairType";
	myVars.races.race99.event1={};
	myVars.races.race99.event2={};
	myVars.races.race99.event1.id = "searchForm:searchInput";
	myVars.races.race99.event1.type = "onmousedown";
	myVars.races.race99.event1.loc = "searchForm:searchInput_LOC";
	myVars.races.race99.event1.isRead = "True";
	myVars.races.race99.event1.eventType = "@event1EventType@";
	myVars.races.race99.event2.id = "Lu_window";
	myVars.races.race99.event2.type = "onload";
	myVars.races.race99.event2.loc = "Lu_window_LOC";
	myVars.races.race99.event2.isRead = "False";
	myVars.races.race99.event2.eventType = "@event2EventType@";
	myVars.races.race99.event1.executed= false;// true to disable, false to enable
	myVars.races.race99.event2.executed= false;// true to disable, false to enable
</script>

