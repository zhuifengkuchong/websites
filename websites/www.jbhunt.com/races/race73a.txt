<script id = "race73a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race73={};
	myVars.races.race73.varName="Lu_Id_a_53__onfocus";
	myVars.races.race73.varType="@varType@";
	myVars.races.race73.repairType = "@RepairType";
	myVars.races.race73.event1={};
	myVars.races.race73.event2={};
	myVars.races.race73.event1.id = "Lu_window";
	myVars.races.race73.event1.type = "onload";
	myVars.races.race73.event1.loc = "Lu_window_LOC";
	myVars.races.race73.event1.isRead = "False";
	myVars.races.race73.event1.eventType = "@event1EventType@";
	myVars.races.race73.event2.id = "Lu_Id_a_53";
	myVars.races.race73.event2.type = "onfocus";
	myVars.races.race73.event2.loc = "Lu_Id_a_53_LOC";
	myVars.races.race73.event2.isRead = "True";
	myVars.races.race73.event2.eventType = "@event2EventType@";
	myVars.races.race73.event1.executed= false;// true to disable, false to enable
	myVars.races.race73.event2.executed= false;// true to disable, false to enable
</script>

