var skipAnimation = false;
var currentImage = 1;
var rotateTo = 0;
var totalImages = 0;
var $sliderImages = [];
var keepAnimating = true;
var $videoWrapper = null;
var youtubeOk = true;
var milliseconds = (new Date).getTime();

jQuery(document).ready(function () {

    jQuery('a[href$=".pdf"]').each(function() {
        jQuery(this).attr('target', '_blank');
    });
	jQuery('a').each(function () {
	    if (this.host.contains(location.host) == false && this.host != "" && this.host != null) {
	    	var attr = jQuery(this).attr('target');
			if (typeof attr !== typeof undefined && attr !== false) {
				jQuery(this).attr('target', '_blank');
			}

			var oc = jQuery(this).attr('onClick');
			if (typeof oc !== typeof undefined && oc !== false) {
				jQuery(this).attr("onClick", "return recordOutboundLink(this);");
			}
	    }
	    else {
	        // internal
	    }
	});
	jQuery('a.nocache').each(function () {
		var href= jQuery(this).attr("href");
		var milliseconds = (new Date).getTime();
		if (href.indexOf("?") != -1){
			jQuery(this).attr("href", href + "&rev=" + milliseconds);
		}
		else{
			jQuery(this).attr("href", href + "?rev=" + milliseconds);
		}
	});
	jQuery('img.nocache').each(function () {
		var href= jQuery(this).attr("src");
		var milliseconds = (new Date).getTime();
		if (href.indexOf("?") != -1){
			jQuery(this).attr("src", href + "&rev=" + milliseconds);
		}
		else{
			jQuery(this).attr("src", href + "?rev=" + milliseconds);
		}
	});
    var currentpage = document.URL;
    var notfound = true;
        jQuery(".searchInput").val("");
    jQuery("#container .header ul li a").each(function () {
        if (currentpage.indexOf(jQuery(this).attr("href")) != -1 && jQuery(this).attr("href") != "/"){
            //jQuery(this).addClass("active");
            notfound = false;
        }
        else {
            //jQuery(this).removeClass("active");
        }

    });
    jQuery(".sideLink").each(function(){
        if (currentpage.indexOf(jQuery(this).attr("href")) != -1 && jQuery(this).attr("href") != "/"){
            jQuery(this).addClass("current");
            notfound = false;
        }
        else {
            jQuery(this).removeClass("current");
        }
    });
    //if (notfound == true){
    //    jQuery("#container .header ul li a:first").addClass("active");
    //}
    
    jQuery(".whitepaperDescription img").click(function(){
        var $link = jQuery("a.download", jQuery(this).parent());
        //.closest("a")[0]
        recordOutboundLink($link.closest("a")[0]);
    });
});
function rotateConroller(){
        var aniTime = 1000;
        var aniDelay = 9000;
        if (rotateTo == 0){

            if (keepAnimating){
                setTimeout(function() {rotateImages(aniTime);}, aniDelay);

            }
        }
        else{
            if (currentImage != rotateTo){
                setTimeout(function() {rotateImages(aniTime/2);}, 0);
            }
            else{
                rotateTo = 0;
                setTimeout(function() {rotateImages(aniTime);}, aniDelay * 2);
            }
        }
}
function rotateImages(duration) {
    var newimage = 0;
    $images = jQuery("#rotatorWrapper .item");
    if ($images.length > 1) {
        //reorder images so that the last image is the rotateTo image
        newimage = parseInt(jQuery("#rotatorWrapper .item:last").attr("id").replace("pri",""));
        while (newimage != rotateTo && rotateTo != 0){
            jQuery("#rotatorWrapper .item:last").prependTo("#rotatorWrapper");
            newimage = parseInt(jQuery("#rotatorWrapper .item:last").attr("id").replace("pri",""));
        }
        if (rotateTo == 0){
            jQuery("#rotatorWrapper .item:last").prependTo("#rotatorWrapper");
            newimage = parseInt(jQuery("#rotatorWrapper .item:last").attr("id").replace("pri",""));
        }
        var $nextImage = jQuery("#rotatorWrapper .item:last");


        //animate holder out
        jQuery("#currentImage").animate({ left: "+100%" }, duration, function () {
            //set holder to last image and reset
            jQuery(this).html("");
            $nextImage.clone().removeAttr("id").appendTo(this);
            jQuery(this).removeAttr("style");
            rotateConroller();
        });
        jQuery("#rotatorcontrols li").removeClass("active");
        jQuery("#pr" + newimage).parent().addClass("active");
        currentImage = newimage;
    }
    return newimage;
}
function manualClick(a){
    try{
            $("#pagerotator img").stop(true, true);
            $("#pagerotator").stop(true, true);
        }
    catch(ex){}
    var targetImage = parseInt(jQuery(a).attr("id").replace("pr",""));
    console.log(targetImage);
    rotateTo = targetImage;
    rotateConroller();
    keepAnimating = false;
}
function eventPush(eventtype, eventname, value, label) {
    //debugger;
    var pagename = window.location.pathname;
    try {
        _gaq.push(['_trackEvent', pagename.toLowerCase() + " " + eventtype.toLowerCase(), eventname.toLowerCase(), label.toLowerCase(), value, true]);
        //_gaq.push(['_trackEvent', 'video', 'pause', videoName, whereYouAt, true]);
        //console.log(eventtype + ", " + eventname + ", " + pagename + ": " + label + ", " + value);
    }
    catch (e) { }
}

jQuery.fn.reverse = [].reverse;

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

var image = new Image();
image.onload = function(){
// The user can access youtube
};
image.onerror = function(){
// The user can't access youtube
    youtubeOk = false;
};
image.src = "../../youtube.com/favicon.ico"/*tpa=http://youtube.com/favicon.ico*/;