var IPECanLaunch = true;
//map is an array of arrays, first value should be a possible key
//If key exists in map, returns all of map except for the key
//If key is undefined, null, or empty string returns undefinedValues array
//If key does not exist in map, returns other values
function getMappedValues(key, map, undefinedValues, otherValues) {
    if (typeof key === "undefined" || key === null || key === "") {
        return undefinedValues;
    }
    for (var i = 0; i < map.length; i++) {
        if (map[i][0] === key) {
            return map[i].slice(1);
        }
    }
    return otherValues;
}

var IPE_country = typeof(cms_country) !== "undefined" ? cms_country.toLowerCase() : "";
var IPE_lang = typeof(cms_lang) !== "undefined" ? cms_lang.toLowerCase() : "";
var IPE_usertype = typeof(cms_usertype) !== "undefined" ? cms_usertype.toLowerCase() : "";
var IPE_status = typeof(cms_status) !== "undefined" ? cms_status.toLowerCase() : "";
var IPE_env = typeof(cms_env) !== "undefined" ? cms_env.toLowerCase() : "";
var params = {};

function handleLogin() {
    var loginCodes = [ // Legacy
        ['authenticated', 241],
        ['identified', 242],
        // New Platform
        ['yes', 241],
        ['no', 242]
    ];

    var undefinedValues = [242];
    var otherValues = [242];
    var result = getMappedValues(IPE_status, loginCodes, undefinedValues, otherValues);
    params["hc"] = result[0];
};

function handleCountry() {
    var countryCodes = [
        ['us', 124875]
    ];

    var undefinedValues = [""];
    var otherValues = [""];
    var result = getMappedValues(IPE_country, countryCodes, undefinedValues, otherValues);
    params["hc2"] = result[0];
};

function handleLang() {
    var langCodes = [
        ['en', 124900]
    ];
    var undefinedValues = [""];
    var otherValues = [""];
    var result = getMappedValues(IPE_lang, langCodes, undefinedValues, otherValues);
    params["hc3"] = result[0];
};

function handleUserType() {
    var usertypeCodes = [ // Legacy
        ['fp', 128204],
        ['individual', 128205],
        ['inst', 128206],
        // New Platform
        ['financial_professional', 128204],
        ['individual', 128205],
        ['institutional', 128206]
    ];
    var undefinedValues = [128204];
    var otherValues = [128204];
    var result = getMappedValues(IPE_usertype, usertypeCodes, undefinedValues, otherValues);
    params["hc4"] = result[0];
};

function getParam() {

    var strParams = "";
    for (var param in params) {
        if (params.hasOwnProperty(param)) {
            if (params[param] !== "") {
                strParams += "&" + param + "=" + params[param];
            }
        }
    }
    if (strParams !== "") {
        strParams = strParams;
    }
    return strParams;
}

setTimeout(getVariables, 1000); //delay for a sec this allow the variables to be set correctly

var ipeUrlBelongToGroup = (function() {
    var IPEurl = location.href;
    var GroupUrls = ["blackrock.com/investing/financial-professionals/advisor-center", "blackrock.com/investing/financial-professionals/registered-investment-advisors", "blackrock.com/investing/library", "blackrock.com/investing/resources/forms-and-applications", "blackrock.com/investing/products/product-list", "blackrock.com/investing/products/fixed-income", "blackrock.com/investing/investment-ideas/bonds/first-call-for-bonds", "blackrock.com/investing/products/227660/", "blackrock.com/investing/products/227680/blackrock-global-allocation-institutional-class-fund", "blackrock.com/investing/products/227565/blackrock-multi-asset-income-inst-class-fund", "blackrock.com/investing/products/227777/blackrock-total-return-fund-inst-class-fund", "blackrock.com/investing/products/227539/blackrock-global-longshort-credit-inst-class-fund", "blackrock.com/investing/products/227581/blackrock-strategic-municipal-opportunitiesinst-fund", "blackrock.com/investing/products/227557/blackrock-high-yield-bondinstitutional-class-fund", "blackrock.com/investing/insights", "blackrock.com/investing/insights/economic-outlook", "blackrock.com/user-platform/signOn", "blackrock.com/investing/resources/account-access"];
    for (var i = 0; i < GroupUrls.length; i++) {
        if (IPEurl.indexOf(GroupUrls[i]) > -1) {
            return true;
        }
    }
    return false;
})();

var ipeSuppressUrl = (function() {
    var IPEurl = location.href;
    var GroupUrls = ["www.blackrock.com/investing/planning/blackrock-plan",
                     "www.blackrock.com/investing/planning/blackrock-plan-advisor",
                     "www.blackrock.com/investing/planning/blackrock-plan/build-plan",
                     "www.blackrock.com/investing/planning/blackrock-plan/frequently-asked-questions",
                     "www.blackrock.com/investing/planning/blackrock-plan/madeline",
                     "www.blackrock.com/investing/planning/blackrock-plan/deirdre",
                     "www.blackrock.com/investing/planning/blackrock-plan/perry",
                     "www.blackrock.com/investing/products/fixed-income",
                     "www.blackrock.com/investing/insights/weekly-commentary"];
    for (var i = 0; i < GroupUrls.length; i++) {
        if (IPEurl.indexOf(GroupUrls[i]) > -1) {
            return true;
        }
    }
    return false;
})();
function ipeUrlGroup() {

    if (ipeUrlBelongToGroup) {
        return "&hc4=219507";
    } else {
        return "&hc4=219508";
    }
}

function getVariables() {
    IPE_country = typeof(cms_country) !== "undefined" ? cms_country.toLowerCase() : "";
    IPE_lang = typeof(cms_lang) !== "undefined" ? cms_lang.toLowerCase() : "";
    IPE_usertype = typeof(cms_usertype) !== "undefined" ? cms_usertype.toLowerCase() : "";
    IPE_status = typeof(cms_status) !== "undefined" ? cms_status.toLowerCase() : "";
    IPE_env = typeof(cms_env) !== "undefined" ? cms_env.toLowerCase() : "";
    handleLogin();
    handleCountry();
    handleLang();
    //handleUserType();

    //only allow EN_US.	
    if ((IPE_country !== "us") || (IPE_lang !== "en")) IPECanLaunch = false;
	//Suppress URLs
	if (ipeSuppressUrl) IPECanLaunch = false;
    if (IPECanLaunch) {
        var rndNum = Math.floor(Math.random() * 100);
        var ipeRate = 0;
        if (ipeUrlBelongToGroup) {
            ipeRate = 40;
        } else {
            ipeRate = 40;
        }
        if (rndNum < ipeRate) {
            (function(d, f) {
                var s = d.createElement('script'),
                    a = "async",
                    b = "defer";
                s.setAttribute(a, a);
                s.setAttribute(b, b);
                s.type = 'text/javascript';
                s.src = f;
                d.getElementsByTagName('head')[0].appendChild(s);
            })(document, document.location.protocol + "//ips-invite.iperceptions.com/webValidator.aspx?sdfc=6f94bf71-113737-42ba72b4-5142-4da2-9f06-357f5e949d51&lID=1&loc=STUDY&cD=90&rF=False&iType=1" + getParam() + ipeUrlGroup() + "&domainname=0&source=91787");
        } else {
            document.cookie = "IPE_S_113737=0;Path=/;";
        }
    }
}