/*
BEGIN configuration Script
*/

var lpUASimagesPath = "/Images/Com/ClickToCall"; 

if (typeof(lpUASunit)=="undefined")
/******************************************************
replace "SampleSkill"  with any skill created in the admin console
******************************************************/
	var lpUASunit = "SampleSkill";  //<----------Change Skills here

if (typeof(lpUASimagesFolder)=="undefined")
/******************************************************
define lpUASimagesFolder before the call to configuration_skill.js to define a different images folder
******************************************************/
	var lpUASimagesFolder = "skillname";

var lpUAScontext = document.title;

var lpUASinvitePositionX = 50;
var lpUASinvitePositionY = 50;

var lpUASbuttonTitle = "Live Chat";
var lpUASinvitationTitle="Invitation popup window for live chat with a representative";
var lpUASinvitationCloseTitle="Close chat invitation";

/*
END configuration Script
*/
