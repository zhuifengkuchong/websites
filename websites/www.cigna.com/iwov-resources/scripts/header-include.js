$(document).ready(function(){	
	var temp = new Array();
	var pagename = $('#pagename').text();
	var globalNavSection = 	$('#globalNavSection').text();
	var subCategory = $("meta[name='WT.cg_s']").attr("content");
	temp = pagename.split("/");
	//Highlighting the Global Navigation.
	$('ul.topnav li a.mainNav').each(function(){					
		if(($(this).attr('href').indexOf(temp[1]) != -1) || ($(this).text() === globalNavSection) || ($(this).text() === subCategory))
		{			
			$(this).parent().closest('li').addClass("active");
		}	
	});	
	//Removing active class from Global Navigation for Landing Pages.
	$('ul.topnav li a.mainNav').each(function(){					
		if(subCategory === "main")
		{			
			$(this).parent().closest('li').removeClass();
		}	
	});
	if(pagename.indexOf("individual-and-family-plans") != -1){
			if($('#ifplogotext').text() === 'arizona'){				
				$('#ifp-default-text').hide();
				$('#ifp-arizona-text').removeClass("hidden");
				$('#ifp-dental-text').hide();					
				$('.visible-xs #ifp-default-text').hide();
				$('.visible-xs #ifp-arizona-text').removeClass("hidden");
				$('.visible-xs #ifp-dental-text').hide();
			}else{
				if(pagename.indexOf("state-policy-disclosures-2014") != -1){
					$('#ifp-default-text').hide();
					$('#ifp-arizona-text').hide();					
					$('#ifp-dental-text').removeClass("hidden");						
					$('.visible-xs #ifp-default-text').hide();
					$('.visible-xs #ifp-arizona-text').hide();
					$('.visible-xs #ifp-dental-text').removeClass("hidden");
				}else{
					$('#ifp-default-text').removeClass("hidden");
					$('#ifp-arizona-text').hide();
					$('#ifp-dental-text').hide();						
					$('.visible-xs #ifp-default-text').removeClass("hidden");
					$('.visible-xs #ifp-arizona-text').hide();
					$('.visible-xs #ifp-dental-text').hide();
				}
			}
			$('.find-dr a').attr("href",'/ifp-providers');
	}	
	if(pagename.indexOf("the-cigna-difference") != -1){
			if($('#ifplogotext').text() === 'arizona'){				
				$('#ifp-default-text').hide();
				$('#ifp-arizona-text').removeClass("hidden");
				$('#ifp-dental-text').hide();					
				$('.visible-xs #ifp-default-text').hide();
				$('.visible-xs #ifp-arizona-text').removeClass("hidden");
				$('.visible-xs #ifp-dental-text').hide();
			}else{
					$('#ifp-default-text').removeClass("hidden");
					$('#ifp-arizona-text').hide();
					$('#ifp-dental-text').hide();						
					$('.visible-xs #ifp-default-text').removeClass("hidden");
					$('.visible-xs #ifp-arizona-text').hide();
					$('.visible-xs #ifp-dental-text').hide();				
			}
	}	
	if(pagename.indexOf("summary-of-benefits") != -1){
			if($('#ifplogotext').text() === 'arizona'){				
				$('#ifp-default-text').hide();
				$('#ifp-arizona-text').removeClass("hidden");
				$('#ifp-dental-text').hide();					
				$('.visible-xs #ifp-default-text').hide();
				$('.visible-xs #ifp-arizona-text').removeClass("hidden");
				$('.visible-xs #ifp-dental-text').hide();
			}else{
				$('#ifp-default-text').removeClass("hidden");
				$('#ifp-arizona-text').hide();
				$('#ifp-dental-text').hide();						
				$('.visible-xs #ifp-default-text').removeClass("hidden");
				$('.visible-xs #ifp-arizona-text').hide();
				$('.visible-xs #ifp-dental-text').hide();
			}
	}		
	if(temp[0] === "business"){
		$('#default-securitylogo').hide();
		$('#bisuness-securitylogo').removeClass("hidden");   
		$('.logo a').attr("href","http://www.cigna.com/business/index.html");
	}
	if(temp[0] != "personal" && temp[0] != "business" && temp[0] != "healthcare-professionals" && temp[0] != "aboutcigna" && temp[0] != "careers"){
		var breadcrumb;
		
		$('ul.breadcrumb-list li>a').each(function(){			
			if($(this).text() === "Health & Wellness"){
				breadcrumb = $(this).text(); 
			}	
		});														
				// For the Health & Wellness Pages
			if( breadcrumb ==="Health & Wellness"){							
				$('ul.topnav li a.mainNav').each(function(){				
					if(breadcrumb === $(this).text())
					{			
						$(this).parent().closest('ul').css("display","block");	
						$(this).parent().closest('li').addClass("active");
					}	
				});	
			}
			/**
			This code is not Required since we created a new header for the NO Global Navigation.
			else{
				// For all the pages which doesn't have global navigation
				$('header').attr("class","navbar navbar-inverse bs-docs-nav header  no-global-nav");
				if($('li.personal').hasClass('active')){
					$('li.personal').attr("class","personal");
					$('.topnav').hide();
				}				
				$('.visible-xs ul.top-left-menus li>a:first').addClass("menu-visible no-highlight");
				$('.header .top-panel .mobile-logo-ifp-container').css("border-bottom","none");			
				$('.visible-xs .nav-row nav ul:first').remove();
			}**/
	}
	if(temp[0] === "personal"){
		if(temp[1] != "individual-and-family-plans"){
			$('.header .top-panel .mobile-logo-ifp-container').css("border-bottom","none");	
		}	
	}
	if(pagename === "ascension"){
		$(".header .search-panel").html('');
		$(".header .search-panel").switchClass('search-panel','navbar-brand-other');
		$(".header .pull-right").css('display','none');
		$('.header .top-panel').prepend('<div id="homeLink"><a href="http://www.cigna.com/iwov-resources/scripts/index.html" style="color:white;">Home</a></div>');
	}
	//This is for showing & hiding the Sitelinks in footer section for the careers pages
	if(temp[0] === "careers"){
		var language = $('#CareersLanguage').text().trim();
		if($('#CareersLanguage').text() != "English")
		{
			$('.first-column').html($("#" + language).html());			
		}
	}
	var fixShareIcons=function(){
		//remove conflicting classes
		$('.share-box a, .share-box a span').removeClass('facebook tumblr linkedin google email twitter pinterest digg delicious stumble at4-icon').css('border-radius','6px'); 
		$('.share-box a span span').css({background: 'none'}).html('');
		//check WIDTH of viewport to select the right set of icons
		$(window).on('resize', function(){
		mywindowsize = $(window).width();
		}).resize();

		if (mywindowsize >= 700) //use Desktop version
		{
			//Set Background position for each media.
			$('.aticon-facebook').css({'background-position':'0 0','background-color':'transparent'});
			$('.aticon-tumblr').css({'background-position':'0 -37px','background-color':'transparent'});
			$('.aticon-linkedin').css({'background-position':'0 -74px','background-color':'transparent'});
			$('.aticon-google_plusone_share').css({'background-position':'0 -111px','background-color':'transparent'}); 
			$('.aticon-email').css({'background-position':'0 -148px','background-color':'transparent'}); 
			$('.aticon-twitter').css({'background-position':'0 -185px','background-color':'transparent'}); 
			$('.aticon-pinterest_share').css({'background-position':'0 -222px','background-color':'transparent'}); 
			$('.aticon-digg').css({'background-position':'0 -259px','background-color':'transparent'}); 
			$('.aticon-delicious').css({'background-position':'0 -296px','background-color':'transparent'}); 
			$('.aticon-stumbleupon').css({'background-position':'0 -333px','background-color':'transparent'}); 
		}else //use Mobile version
		{
			//Set Background position for each media.
			$('.aticon-facebook').css({'background-position':'-39px 0','background-color':'transparent'});
			$('.aticon-tumblr').css({'background-position':'-39px -30px','background-color':'transparent'});
			$('.aticon-linkedin').css({'background-position':'-39px -60px','background-color':'transparent'});
			$('.aticon-google_plusone_share').css({'background-position':'-39px -90px','background-color':'transparent'}); 
			$('.aticon-email').css({'background-position':'-39px -120px','background-color':'transparent'}); 
			$('.aticon-twitter').css({'background-position':'-39px -150px','background-color':'transparent'}); 
			$('.aticon-pinterest_share').css({'background-position':'-39px -180px','background-color':'transparent'}); 
			$('.aticon-digg').css({'background-position':'-39px -210px','background-color':'transparent'}); 
			$('.aticon-delicious').css({'background-position':'-39px -240px','background-color':'transparent'}); 
			$('.aticon-stumbleupon').css({'background-position':'-39px -270px','background-color':'transparent'});
		}
	}
	$('a.share-link').click(fixShareIcons).hover(fixShareIcons);
});
$("#careers-id").click(function(e){		
        $('.global-nav').removeAttr("style");
   });