$(document).ready(setTracking);
function setTracking() {				 
	$("div.promo-area").mouseover(function (){		
			var qrystring = promoareaTrackMe($(this));
			var urlvalue = $(this).attr("data-targeturl");
			var hrefvalue = $(this).attr("href");			
			if(urlvalue != undefined){					
				var lastString = urlvalue.substring(urlvalue.length-6, urlvalue.length);
				var urlval = $(this).attr("data-targeturl");
				var urltarget = $(this).attr("data-targetframe");
				if(lastString != ".html?"){					
					$(this).attr("data-targeturl", urlval);
				}else{
					var link = urlval.slice(0, -1);
					$(this).attr("data-targeturl", link);
				}														
			}else{			
				$(this).attr("href", hrefvalue);									
			}	
			$('#QueyString-ct').text(qrystring);
	}).mousedown(function(){
		  var webtrendsParams = $("#QueyString-ct").text().split('=')[1].replace('%3F','?');
		  dcsMultiTrackForLPLinks(webtrendsParams);
	});
    $("div.promo-area").mouseout(function (){
		var urlvalue = $(this).attr("data-targeturl");
		var hrefvalue = $(this).attr("href");				
		if(urlvalue != undefined){												   
			$(this).attr('data-targeturl', urlvalue);					   
		}else{
			$(this).attr('href', hrefvalue);					   
		}
		$('#QueyString-ct').text('');
    });	
	$("div.skinny-promo-wrapper,div.skinny-promo-container").mouseover(function (){		
        var qrystring = promoctaclickableWrapperTrackMe($(this));
		var urlValue = $(this).attr("data-targeturl");
		var hrefvalue = $(this).attr("href");
		if(urlValue != undefined){	
			var urltarget = $(this).attr("data-targetframe");
			var lastString = urlValue.substring(urlValue.length-6, urlValue.length);
			var fullurl ="";
			if(lastString != ".html?"){				
				if(qrystring != ''){										
					if(urlValue.indexOf('?') != -1){
						$(this).attr("data-targeturl", urlValue + '&' + qrystring);
						fullurl = urlValue +'&'+ qrystring;
					}else{
						$(this).attr("data-targeturl", urlValue + '?' + qrystring);
						fullurl = urlValue +'?'+ qrystring;
					}													
				}else{					
					$(this).attr("data-targeturl", urlValue);
					fullurl = urlValue;	
				}
			}else{				
				if(qrystring != ''){						
					$(this).attr("data-targeturl", urlValue + qrystring);	
					fullurl = urlValue + qrystring;	
				}else{
					$(this).attr("data-targeturl", urlValue);
					fullurl = urlValue;
				}					
			}
			if(qrystring != ''){
				$('#QueyString-ct').text(fullurl);	
			}			
		}else if(hrefvalue != undefined){			
			if(qrystring != ''){	
				if(hrefvalue.indexOf('?') != -1){
					$(this).attr("href", hrefvalue + '&' + qrystring);
				}else{
					$(this).attr("href", hrefvalue + '?' + qrystring);
				}													
			}else{
				$(this).attr("href", hrefvalue);
			}
		}else{
			$("div.skinny-promo-wrapper a").each(function(){	
				var querystring = promoctaWrapperTrackMe($(this));
				var urlval = $(this).attr("href");
				if(qrystring != ''){	
					if(urlval.indexOf('?') != -1){
						$(this).attr("href", urlval + '&' + qrystring);
					}else{
						$(this).attr("href", urlval + '?' + qrystring);
					}													
				}else{
						$(this).attr("href", urlval);
				}	
			});
		}	
    }).mousedown(function(){			
		var str = $("#QueyString-ct").text().trim();		
		 if(str.indexOf('Redirect-Link') != -1){			 					 
			 var lastIndex = str.lastIndexOf(";");
			 var str = str.substring(0, lastIndex);	
			 var webtrendsParams = str.split('=')[1];			 
			 dcsMultiTrackForLPLinks(webtrendsParams);
			 var urlValue = $(this).attr("data-targeturl");
			 var urltarget = $(this).attr("data-targetframe");			 
			 window.open(urlValue, urltarget);	
			 $('#QueyString-ct').text('');	
		 }else if(str !=''){								 
			var fullurl = $("#QueyString-ct").text();			
			var urltarget = $(this).attr("data-targetframe");				
			window.open(fullurl, urltarget);
			$('#QueyString-ct').text('');			
		 }
			
	});
	$("div.skinny-promo-wrapper,div.skinny-promo-container").mouseout(function (){
		var urlvalue = $(this).attr("data-targeturl");
		var hrefvalue = $(this).attr("href");
		$('#QueyString-ct').text('');	
		if(urlvalue != undefined){
		   if($(this).attr('data-targeturl').indexOf('&WT.z_nav') != -1){
				$(this).attr('data-targeturl', urlvalue.substring(0,urlvalue.lastIndexOf('&')));
		   }else if(($(this).attr('data-targeturl').indexOf('?WT.z_nav') != -1)){
				$(this).attr('data-targeturl', urlvalue.substring(0,urlvalue.lastIndexOf('?')));
		   }else{
				$(this).attr('data-targeturl', urlvalue);
		   }									
		}else if(hrefvalue != undefined){
			   var url = $(this).attr('href');		
			   if(url.indexOf('&WT.z_nav') != -1){
					$(this).attr('href', url.substring(0,url.lastIndexOf('&')));
			   }else if(url.indexOf('?WT.z_nav') != -1){										
					$(this).attr('href', url.substring(0,url.lastIndexOf('?')));
			   }else{
					$(this).attr('href', url);
			   }	
		}else{
			$("div.skinny-promo-wrapper a").each(function(){					
			   var url = $(this).attr('href');		
			   if(url.indexOf('&WT.z_nav') != -1){
					$(this).attr('href', url.substring(0,url.lastIndexOf('&')));
			   }else if(url.indexOf('?WT.z_nav') != -1){										
					$(this).attr('href', url.substring(0,url.lastIndexOf('?')));
			   }else{
					$(this).attr('href', url);
			   }	
			});
		}
    });	
	$("div.button-cta.ctapromo-ct a").mouseover(function () {
        var qrystring = promoctaBannerTrackMe($(this));
		var urlval = $(this).attr("href");
		if(qrystring != ''){	
			if(urlval.indexOf('?') != -1){
				$(this).attr("href", urlval + '&' + qrystring);
			}else{
				$(this).attr("href", urlval + '?' + qrystring);
			}													
		}else{
			$(this).attr("href", urlval);
		}										
    });
    $("div.button-cta.ctapromo-ct a").mouseout(function () { 		
	   var url = $(this).attr('href');		
	   if(url.indexOf('&WT.z_nav') != -1){
			$(this).attr('href', url.substring(0,url.lastIndexOf('&')));
	   }else if(url.indexOf('?WT.z_nav') != -1){										
			$(this).attr('href', url.substring(0,url.lastIndexOf('?')));
	   }else{
			$(this).attr('href', url);
	   }		
    });
	$("div.mob-button.ctapromo-ct a").mouseover(function () {		
        var qrystring = promomobilectaBannerTrackMe($(this));
		var urlval = $(this).attr("href");
		if(qrystring != ''){	
			if(urlval.indexOf('?') != -1){
				$(this).attr("href", urlval + '&' + qrystring);
			}else{
				$(this).attr("href", urlval + '?' + qrystring);
			}													
		}else{
			$(this).attr("href", urlval);
		}									
    });
    $("div.mob-button.ctapromo-ct a").mouseout(function () {
       var url = $(this).attr('href');		
	   if(url.indexOf('&WT.z_nav') != -1){
			$(this).attr('href', url.substring(0,url.lastIndexOf('&')));
	   }else if(url.indexOf('?WT.z_nav') != -1){										
			$(this).attr('href', url.substring(0,url.lastIndexOf('?')));
	   }else{
			$(this).attr('href', url);
	   }	
    });	
	$("div.button-cta.heropromo-ct a").mouseover(function () {
        var qrystring = promoheroctaBannerTrackMe($(this));
		var urlval = $(this).attr("href");
		if(qrystring != ''){	
			if(urlval.indexOf('?') != -1){
				$(this).attr("href", urlval + '&' + qrystring);
			}else{
				$(this).attr("href", urlval + '?' + qrystring);
			}													
		}else{
			$(this).attr("href", urlval);
		}										
    });
    $("div.button-cta.heropromo-ct a").mouseout(function () { 		
	   var url = $(this).attr('href');		
	   if(url.indexOf('&WT.z_nav') != -1){
			$(this).attr('href', url.substring(0,url.lastIndexOf('&')));
	   }else if(url.indexOf('?WT.z_nav') != -1){										
			$(this).attr('href', url.substring(0,url.lastIndexOf('?')));
	   }else{
			$(this).attr('href', url);
	   }		
    });
	$("div.mob-button.heropromo-ct a").mouseover(function () {		
        var qrystring = promoheromobilectaBannerTrackMe($(this));
		var urlval = $(this).attr("href");
		if(qrystring != ''){	
			if(urlval.indexOf('?') != -1){
				$(this).attr("href", urlval + '&' + qrystring);
			}else{
				$(this).attr("href", urlval + '?' + qrystring);
			}													
		}else{
			$(this).attr("href", urlval);
		}									
    });
    $("div.mob-button.heropromo-ct a").mouseout(function () {
       var url = $(this).attr('href');		
	   if(url.indexOf('&WT.z_nav') != -1){
			$(this).attr('href', url.substring(0,url.lastIndexOf('&')));
	   }else if(url.indexOf('?WT.z_nav') != -1){										
			$(this).attr('href', url.substring(0,url.lastIndexOf('?')));
	   }else{
			$(this).attr('href', url);
	   }	
    });
	
	$("div.hero-img-info a").mouseover(function () {		
        var qrystring = heroBannerTrackMe($(this));
		var urlval = $(this).attr("href");
		if(qrystring != ''){	
			if(urlval.indexOf('?') != -1){
				$(this).attr("href", urlval + '&' + qrystring);
			}else{
				$(this).attr("href", urlval + '?' + qrystring);
			}													
		}else{
			$(this).attr("href", urlval);
		}										
    });
	
    $("div.hero-img-info a").mouseout(function () {
       var url = $(this).attr('href');		
	   if(url.indexOf('&WT.z_nav') != -1){
			$(this).attr('href', url.substring(0,url.lastIndexOf('&')));
	   }else if(url.indexOf('?WT.z_nav') != -1){										
			$(this).attr('href', url.substring(0,url.lastIndexOf('?')));
	   }else{
			$(this).attr('href', url);
	   }		
    });	
	$("div.news-row a").mouseover(function () {			
        var qrystring = newsAreaTrackMe($(this));
		var urlval = $(this).attr('href');
		if(qrystring != ''){	
			if(urlval.indexOf('?') != -1){
				$(this).attr("href", urlval + '&' + qrystring);
			}else{
				$(this).attr("href", urlval + '?' + qrystring);
			}													
		}else{
			$(this).attr("href", urlval);
		}										
    });
    $("div.news-row a").mouseout(function () {
       var url = $(this).attr('href');		
	   if(url.indexOf('&WT.z_nav') != -1){
			$(this).attr('href', url.substring(0,url.lastIndexOf('&')));
	   }else if(url.indexOf('?WT.z_nav') != -1){										
			$(this).attr('href', url.substring(0,url.lastIndexOf('?')));
	   }else{
			$(this).attr('href', url);
	   }		
    });
	if($('.header-wrapper').length != 0){
	//Right Rail Promo's for Cigna Health Spring Pages
		$('section.widget a').mouseover(function () {		
			var qrystring = rightRailPromoTrackMe($(this));
			var urlval = $(this).attr("href");
			if(qrystring != ''){	
				if(urlval.indexOf('?') != -1){
					$(this).attr("href", urlval + '&' + qrystring);
				}else{
					$(this).attr("href", urlval + '?' + qrystring);
				}													
			}else{
				$(this).attr("href", urlval);
			}									
		});
		$("section.widget a").mouseout(function () {
		   var url = $(this).attr('href');		
		   if(url.indexOf('&WT.z_nav') != -1){
				$(this).attr('href', url.substring(0,url.lastIndexOf('&')));
		   }else if(url.indexOf('?WT.z_nav') != -1){										
				$(this).attr('href', url.substring(0,url.lastIndexOf('?')));
		   }else{
				$(this).attr('href', url);
		   }	
		});
	}
	if($('#pagename').length != 0){
	$("div.side-widget,div.rectangular-box").mouseover(function (){	
			var qrystring = ifpRailPromoTrackMe($(this));       
			var urlValue = $(this).attr("data-targeturl");
			var hrefvalue = $(this).attr("href");
			if(urlValue != undefined){
				var lastString = urlValue.substring(urlValue.length-6, urlValue.length);
				if(lastString != ".html?"){					
					$(this).attr("data-targeturl", urlValue);
				}else{
					var link = urlValue.slice(0, -1);
					$(this).attr("data-targeturl", link);
				}
				$('#QueyString-ct').text(qrystring);		
			}else if(hrefvalue != undefined){	
				$(this).attr("href", hrefvalue);
				$('#QueyString-ct').text(qrystring);
			}else{				
				$("div.side-widget a,div.rectangular-box a").one("click",function(){						
					var querystring = ifpRailPromoTrackMeLinks($(this));
					var urlval = $(this).attr("href");
					if(querystring != ''){	
						if(urlval.indexOf('?') != -1){
							$(this).attr("href", urlval + '&' + querystring);
						}else{
							$(this).attr("href", urlval + '?' + querystring);
						}													
					}else{
							$(this).attr("href", urlval);
					}	
				});			
			}					
    }).mousedown(function(){
		 var str = $("#QueyString-ct").text().trim();			
		 if(str.indexOf('Redirect-Link') != -1){
			 var lastIndex = str.lastIndexOf(";");
			 var str = str.substring(0, lastIndex);	
			 var webtrendsParams = str.split('=')[1].replace('%3F','?');			
			 dcsMultiTrackForLPLinks(webtrendsParams);	
			 $('#QueyString-ct').text('');	
		 }else if(str !=''){								 
			 var webtrendsParams = str.split('=')[1].replace('%3F','?');			 			 
			 dcsMultiTrackForLPLinks(webtrendsParams);
			$('#QueyString-ct').text('');			
		 }
	});
	$("div.side-widget,div.rectangular-box").mouseout(function (){
		$('#QueyString-ct').text('');					
		$("div.side-widget a,div.rectangular-box a").one("click",function(){							   
		   var url = $(this).attr('href');		
		   if(url.indexOf('&WT.z_nav') != -1){
				$(this).attr('href', url.substring(0,url.lastIndexOf('&')));
		   }else if(url.indexOf('?WT.z_nav') != -1){										
				$(this).attr('href', url.substring(0,url.lastIndexOf('?')));
		   }else{
				$(this).attr('href', url);
		   }	
		});		
	});	
}	
	$("p#paragraph-ct a").mouseover(function () {		
        var qrystring = healthwellnessTrackMe($(this));
		var urlval = $(this).attr('href');
		if(qrystring != ''){	
			if(urlval.indexOf('?') != -1){
				$(this).attr("href", urlval + '&' + qrystring);
			}else{
				$(this).attr("href", urlval + '?' + qrystring);
			}													
		}else{
			$(this).attr("href", urlval);
		}										
    });
    $("p#paragraph-ct a").mouseout(function () {
       var url = $(this).attr('href');		
	   if(url.indexOf('&WT.z_nav') != -1){
			$(this).attr('href', url.substring(0,url.lastIndexOf('&')));
	   }else if(url.indexOf('?WT.z_nav') != -1){										
			$(this).attr('href', url.substring(0,url.lastIndexOf('?')));
	   }else{
			$(this).attr('href', url);
	   }	
    });	
	$('div.main-promotion a').mouseover(function () {		
        var qrystring = articleTrackMe($(this));
		var urlval = $(this).attr("href");
		if(qrystring != ''){	
			if(urlval.indexOf('?') != -1){
				$(this).attr("href", urlval + '&' + qrystring);
			}else{
				$(this).attr("href", urlval + '?' + qrystring);
			}													
		}else{
			$(this).attr("href", urlval);
		}									
    });
	 $("div.main-promotion a").mouseout(function () {
       var url = $(this).attr('href');		
	   if(url.indexOf('&WT.z_nav') != -1){
			$(this).attr('href', url.substring(0,url.lastIndexOf('&')));
	   }else if(url.indexOf('?WT.z_nav') != -1){										
			$(this).attr('href', url.substring(0,url.lastIndexOf('?')));
	   }else{
			$(this).attr('href', url);
	   }	
    });
	$("dd a").mouseover(function (){		
		var qrystring = accordion_links($(this));		
		if(qrystring != ''){				
			if(this.href.indexOf('?') != -1){
				$(this).attr("href", this.href + '&' + qrystring);
			}else{
				$(this).attr("href", this.href + '?' + qrystring);
			}													
		}else{
			$(this).attr("href", this.href);
		}	
	}).mousedown(function(){		 
		 var str = $("#QueyString-ct").text().trim();
		 if(str.indexOf('Redirect-Link') != -1){			 	
			 var lastIndex = str.lastIndexOf(";");
			 var str = str.substring(0, lastIndex);
			 var webtrendsParams = str.split('=')[1];
			 dcsMultiTrackForLPLinks(webtrendsParams);
			 $('#QueyString-ct').text('');
		 }
	});
	$("dd a").mouseout(function (){				
		var url = $(this).attr("href");		
	   if(url.indexOf('&WT.z_nav') != -1){
			$(this).attr('href', url.substring(0,url.lastIndexOf('&')));
	   }else if(url.indexOf('?WT.z_nav') != -1){
			$(this).attr('href', url.substring(0,url.lastIndexOf('?')));
	   }else{			
			$(this).attr('href', url);
	   }	
	   $('#QueyString-ct').text('');	
	});
	$("dt.toggle-head a").mouseover(function (){		
		var qrystring = accordion_mainlinks($(this));
		var urlval = $(this).attr("href");		
		$(this).attr("href", urlval);											
	    $('#QueyString-ct').text(qrystring);		
	}).mousedown(function(){
		  var webtrendsParams = $("#QueyString-ct").text().split('=')[1];					  
		  dcsMultiTrackForLPLinks(webtrendsParams);				  
	});
	$("dt.toggle-head a").mouseout(function (){				
		var urlval = $(this).attr("href");		
		$(this).attr("href", urlval);												    	
	    $('#QueyString-ct').text('');	
	});		
	$("div.breadcrumb a").mouseover(function () {		
		var qrystring = breadcrumb($(this));
		var urlval = $(this).attr("href");		
			if(qrystring != ''){	
				if(urlval.indexOf('?') != -1){
					$(this).attr("href", urlval + '&' + qrystring);
				}else{
					$(this).attr("href", urlval + '?' + qrystring);
					}													
			}else{
				$(this).attr("href", urlval);
			}								
	});
    $("div.breadcrumb a").mouseout(function () {
       var url = $(this).attr('href');		
	   if(url.indexOf('&WT.z_nav') != -1){
			$(this).attr('href', url.substring(0,url.lastIndexOf('&')));
	   }else if(url.indexOf('?WT.z_nav') != -1){										
			$(this).attr('href', url.substring(0,url.lastIndexOf('?')));
	   }else{
			$(this).attr('href', url);
	   }	
    });
	$("li.print a").mouseover(function () {		
		var qrystring = printicon($(this));
		var urlval = $(this).attr("href");		
		$(this).attr("href", urlval);				
	    $('#QueyString-ct').text(qrystring);			
	}).mousedown(function(){		  
		  var webtrendsParams = $("#QueyString-ct").text().split('=')[1];					  
		  dcsMultiTrackForLPLinks(webtrendsParams);				  
	});
    $("li.print a").mouseout(function () {
		var urlval = $(this).attr("href");		
		$(this).attr("href", urlval);				
	    $('#QueyString-ct').text('');
    });  	
	$("div.share-box ul li a").mouseover(function () {		
		var qrystring = share($(this));
		var urlval = $(this).attr("href");		
		$(this).attr("href", urlval);				
	    $('#QueyString-ct').text(qrystring);			
	}).mousedown(function(){		  
		  var webtrendsParams = $("#QueyString-ct").text().split('=')[1];					  
		  dcsMultiTrackForLPLinks(webtrendsParams);				  
	});
    $("div.share-box ul li a").mouseout(function () {
        var urlval = $(this).attr("href");		
		$(this).attr("href", urlval);				
	    $('#QueyString-ct').text('');		
    });
	/**For IFP CT**/
	$("a.need-help-tab").mouseover(function () {
    //This block is used to the track the leaf Layout Expansion click of all the ifp pages.	
		var qrystring = needHelpTrackme($(this));
		var urlval = $(this).attr("href");		
		$(this).attr("href", urlval);				
	    $('#QueyString-ct').text(qrystring);		
	}).mousedown(function(){		  
		  var webtrendsParams = $("#QueyString-ct").text().split('=')[1];					  		  
		  dcsMultiTrackForLPLinks(webtrendsParams);				  		  
	});
    $("a.need-help-tab").mouseout(function () {
        var urlval = $(this).attr("href");		
		$(this).attr("href", urlval);				
	    $('#QueyString-ct').text('');		
    });
	$('div.help-bucket a').mouseover(function (){
	//This block is used to track the Leaf Layout in all the ifp pages.
		var qrystring = needHelpLinksTrackme($(this));
		var urlval = $(this).attr("href");		
		if(qrystring != ''){	
			if(urlval.indexOf('?') != -1){
				$(this).attr("href", urlval + '&' + qrystring);
			}else{
				$(this).attr("href", urlval + '?' + qrystring);
				}													
		}else{
			$(this).attr("href", urlval);
		}			
	});
	$('div.help-bucket a').mouseout(function (){
       var url = $(this).attr('href');		
	   if(url.indexOf('&WT.z_nav') != -1){
			$(this).attr('href', url.substring(0,url.lastIndexOf('&')));
	   }else if(url.indexOf('?WT.z_nav') != -1){										
			$(this).attr('href', url.substring(0,url.lastIndexOf('?')));
	   }else{
			$(this).attr('href', url);
	   }		
    });	
	$('div.ifpTopHero .cta-wrapper a').mouseover(function (){
	//This block is used to the track left Navigation Bar in the state pages.
		var qrystring = ifpBanner($(this));
		var urlval = $(this).attr("href");		
		if(qrystring != ''){	
			if(urlval.indexOf('?') != -1){
				$(this).attr("href", urlval + '&' + qrystring);
			}else{
				$(this).attr("href", urlval + '?' + qrystring);
				}													
		}else{
			$(this).attr("href", urlval);
		}			
	});
	$('div.ifpTopHero .cta-wrapper a').mouseout(function (){
       var url = $(this).attr('href');		
	   if(url.indexOf('&WT.z_nav') != -1){
			$(this).attr('href', url.substring(0,url.lastIndexOf('&')));
	   }else if(url.indexOf('?WT.z_nav') != -1){										
			$(this).attr('href', url.substring(0,url.lastIndexOf('?')));
	   }else{
			$(this).attr('href', url);
	   }		
    });	
	$('div.plansLeftCont a').mouseover(function (){
	 //This block is used to track left Navigation Bar in the state pages.	
		var qrystring = planSelectorLeftContainer($(this));
		var urlval = $(this).attr("href");		
		if(qrystring != ''){	
			if(urlval.indexOf('?') != -1){
				$(this).attr("href", urlval + '&' + qrystring);
			}else{
				$(this).attr("href", urlval + '?' + qrystring);
				}													
		}else{
			$(this).attr("href", urlval);
		}			
	});
	$('div.plansLeftCont a').mouseout(function (){
       var url = $(this).attr('href');		
	   if(url.indexOf('&WT.z_nav') != -1){
			$(this).attr('href', url.substring(0,url.lastIndexOf('&')));
	   }else if(url.indexOf('?WT.z_nav') != -1){										
			$(this).attr('href', url.substring(0,url.lastIndexOf('?')));
	   }else{
			$(this).attr('href', url);
	   }		
    });
	$('#plan-sort').change(function(){ 
	//This block is used to track the Sort By:Metal Type Dropdown	at the top of state pages.
		var selectedPlan = $('#plan-sort option:selected').text();
		/**if(selectedPlan !='Metal Type')**/
			var webtrendsParams = "IFP-State Medical Plans Selector;Sort By;"+ selectedPlan;
			dcsMultiTrackForLPLinks(webtrendsParams);					
	});
	$('div.quote-block a,div.comparePlansCont a').mouseover(function (){
	//This block is used to track the compare plans and state pages(i.e arizona.page etc) Blue get a Quote Box	
		var urlval = $(this).attr("href");		
		$(this).attr("href", urlval);					   
	}).mousedown(function(){	
		if($('#pagename').text().indexOf("/dental-plans/index") != -1){		      
			//dental plans page	
			 planName = $(this).closest('div.plan-bucket').find('div.price-block h2').text().trim().replace(/[;]*/gi, '');
			//planName = $(this).closest('div.quote-block').siblings().find('h2').text().trim().replace(/[;]*/gi, '');  
			if($(this).attr("href").indexOf('tel:') === -1){
			      dcsMultiTrackForLinks("IFP-freequote", planName);
			}			 
		}else{     
			var url = window.location.href;
			var index = url.lastIndexOf("/") + 1;
			var filenameWithExtension = url.substr(index);
			var state = filenameWithExtension.split(".")[0]; 
			state = state.split("?")[0];				
			var countyAttr= 'city';
			var county = getQueryStringParams(countyAttr);
			if(county === null || county === undefined || county === ''){
				county = 'All Counties'
			}
			if($('.plans-type').length != 0){
				var planName = $(this).parent().siblings().find('h3').text().trim().replace(/[;]*/gi, '');	
				var partslength = $('.ifp-heading').text().trim().length;
				var parts = $('.ifp-heading').text().trim().slice(17,partslength).replace(/\s+/g,'-').toLowerCase();
				var StateVal = parts;
				var view = $('.plans-type ul li.selected').text().trim();
				view = encodeURIComponent(view);
				dcsMultiTrackForLinks("IFP-freequote", planName, view,StateVal);	
			}else{
				var planName = $(this).parent().siblings().find('h2').text().trim().replace(/[;]*/gi, '');
				dcsMultiTrackForLinks("IFP-freequote", planName,state,county);				  		  
			}	
		}		
	});
	$('div.quoteNewBtnCont a').mouseover(function (){	
	//This block is used to track the links in the plan pages	
		var urlval = $(this).attr("href");		
		$(this).attr("href", urlval);					   
	}).mousedown(function(){
			var planName = $('.heading-deatils').find('h2').text().trim().replace(/[;]*/gi, '');									
			dcsMultiTrackForLinks("IFP-freequote", planName);				  		  		
	});
	$('li.quoteNewBtn a').mouseover(function (){	
		//This block is used to track the links in the dental plans page	
			var urlval = $(this).attr("href");		
			$(this).attr("href", urlval);					   
		}).mousedown(function(){
				var planName = $('.heading-deatils').find('h2').text().trim().replace(/[;]*/gi, '');									
				dcsMultiTrackForLinks("IFP-freequote", planName);				  		  		
		});	
	$('div.panel-default').mouseover(function (){
	//This block is used to track the accordion Expansion links in the plan page	
		var qrystring = ifpaccordion_mainlinks($(this));
		var urlval = $(this).attr("href");		
		$(this).attr("href", urlval);											
		$('#QueyString-ct').text(qrystring);		
	}).mousedown(function(){
		  var webtrendsParams = $("#QueyString-ct").text().split('=')[1];					  		  
		  dcsMultiTrackForLPLinks(webtrendsParams);	
		  $('#QueyString-ct').text('');	
	});		
	$('div.complex-hero a').mouseover(function (){
	 //This block is used to track Dental Page Hero Banner.	
		var qrystring = dentalBannerTrackme($(this));
		var urlval = $(this).attr("href");		
		if(qrystring != ''){	
			if(urlval.indexOf('?') != -1){
				$(this).attr("href", urlval + '&' + qrystring);
			}else{
				$(this).attr("href", urlval + '?' + qrystring);
				}													
		}else{
			$(this).attr("href", urlval);
		}			
	}).mousedown(function(){		 
		 var str = $("#QueyString-ct").text().trim();
		 if(str.indexOf('Redirect-Link') != -1){			 
			 var lastIndex = str.lastIndexOf(";");
			 var str = str.substring(0, lastIndex);	
			 var webtrendsParams = str.split('=')[1];		
			 dcsMultiTrackForLPLinks(webtrendsParams);				 
			 $('#QueyString-ct').text('');	
		 }
	});
	$('div.complex-hero a').mouseout(function (){
       var url = $(this).attr('href');		
	   if(url.indexOf('&WT.z_nav') != -1){
			$(this).attr('href', url.substring(0,url.lastIndexOf('&')));
	   }else if(url.indexOf('?WT.z_nav') != -1){										
			$(this).attr('href', url.substring(0,url.lastIndexOf('?')));
	   }else{
			$(this).attr('href', url);
	   }		
    });	
	$("a").mouseover(function (){		
		var qrystring = trackMe($(this).parent(),$(this));        
		if(qrystring != ''){				
			if(this.href.indexOf('?') != -1){
				$(this).attr("href", this.href + '&' + qrystring);
			}else{
				$(this).attr("href", this.href + '?' + qrystring);
			}													
		}else{
			$(this).attr("href", this.href);
		}	
    }).mousedown(function(){
		 var str = $("#QueyString-ct").text().trim();
		 if(str.indexOf('Redirect-Link') != -1){			 	
			 var lastIndex = str.lastIndexOf(";");
			 var str = str.substring(0, lastIndex);	
			 var webtrendsParams = str.split('=')[1];
			 dcsMultiTrackForLPLinks(webtrendsParams);	
			 $('#QueyString-ct').text('');	
		 }
	});
    $("a").mouseout(function () {		
       var url = $(this).attr('href');		
	   if(url.indexOf('&WT.z_nav') != -1){
			$(this).attr('href', url.substring(0,url.lastIndexOf('&')));
	   }else if(url.indexOf('?WT.z_nav') != -1){
			$(this).attr('href', url.substring(0,url.lastIndexOf('?')));
	   }else{			
			$(this).attr('href', url);
	   }
		$('#QueyString-ct').text('');
    });	
}
$(window).load(setPromoTracking);
function setPromoTracking() {
	$("div.promo-box").mouseover(function () {			
        var qrystring = promoboxTrackMe($(this));
		var urlvalue = $(this).attr("data-targeturl");
		var hrefvalue = $(this).attr("href");			
			if(urlvalue != undefined){					
				var lastString = urlvalue.substring(urlvalue.length-6, urlvalue.length);
				var urlval = $(this).attr("data-targeturl");
				if(lastString != ".html?"){					
					$(this).attr("data-targeturl", urlval);
				}else{
					var link = urlval.slice(0, -1);
					$(this).attr("data-targeturl", link);
				}														
			}else{			
				$(this).attr("href", hrefvalue);									
			}	
			$('#QueyString-ct').text(qrystring);
	}).mousedown(function(){		  
		  var webtrendsParams = $("#QueyString-ct").text().split('=')[1].replace('%3F','?');
		  dcsMultiTrackForLPLinks(webtrendsParams);				  
	});
    $("div.promo-box").mouseout(function (){
		var urlvalue = $(this).attr("data-targeturl");
		var hrefvalue = $(this).attr("href");				
		if(urlvalue != undefined){												   
			$(this).attr('data-targeturl', urlvalue);					   
		}else{
			$(this).attr('href', hrefvalue);					   
		}
		$('#QueyString-ct').text('');  
    });	
}
function initTracking(param){
	if($('#pagename').text() === "aboutcigna/contact-us/index"){
		/* This is for the main Cigna.com homepage Contact Us landing page */
		return param += '=' + "contact-us";
	}else if($('#pagename').text() === "personal/individual-and-family-plans/dental-plans/index"){
		return param += '=' + "IFP-Dental Plans Selector";
	}else if($('#pagename').text() === "personal/individual-and-family-plans/shop-our-health-insurance-plans"){
		// Newly added this condition for the IFP Shop Health Plans
		return param += '=' + "IFP-State Medical Selector";
	}else if($('#pagename').text() === "personal/individual-and-family-plans/index"){
		return param += '=' + "IFP-home";
	}else if($('#pagename').text().indexOf("/individual-and-family-plans/dental-plans") != -1){
		return param += '=' + "IFP-Dental Plan Details";
	}else if($('#pagename').text() === "personal/individual-and-family-plans/cigna-dental-insurance"){
		return param += '=' + "IFP-Cigna Dental";		
	}else if($('#pagename').text() === "personal/individual-and-family-plans/ifp-knowledge-center/index"){
		return param += '=' + "IFP-Knowledge Center";
	}else if($('#pagename').text() === "personal/individual-and-family-plans/why-choose-cigna-services/index"){
		return param += '=' + "IFP-Why Choose Cigna?";
	}else if($('#pagename').text() === "personal/individual-and-family-plans/explore-plans-in-your-state-2014/comparePlan"){
		return param += '=' + "IFP-Compare Plan";
	}else if($('#pagename').text() === "personal/individual-and-family-plans/explore-plans-in-your-state-2014/productDetail"){
		return param += '=' + "IFP-Medical Plan Details";
	}else if($('#pagename').text().indexOf("personal/individual-and-family-plans/explore-plans-in-your-state-2014/") != -1){
		return param += '=' + "IFP-State Medical Plans Selector";
	}else if($('#pagename').length != 0){
		/* This is for the main Cigna.com homepage landing pages (other than Contact Us) */
		return param += '=' + $('li.active').attr('class').split(' ')[1].trim().replace(/[;]*/gi, '');
	}else if($('.header-wrapper').attr('data-pagename') === "cigna-healthspring"){
		return param += '=' + 'HS-home';
	}else if($('.header-wrapper').attr('data-pagename') === "medicare-advantage/index"){
		return param += '=' + 'HS-Medicare Advantage';
	}else if($('.header-wrapper').attr('data-pagename') === "medicare-advantage/plan-extras"){
		return param += '=' + 'HS-Medicare Advantage;Plan Extras';  
	}else if($('.header-wrapper').attr('data-pagename') === "medicare-advantage/enrollment"){
		return param += '=' + 'HS-Medicare Advantage;Enrollment';  
	}else if($('.header-wrapper').attr('data-pagename') === "part-d/index"){
		return param += '=' + 'HS-Medicare Part D'; 
	}else if($('.header-wrapper').attr('data-pagename') === "part-d/plan-extras"){
		return param += '=' + 'HS-Medicare Part D;Plan Extras';  
	}else if($('.header-wrapper').attr('data-pagename') === "part-d/enrollment-information"){
		return param += '=' + 'HS-Medicare Part D;Enrollment';  
	}else if($('.header-wrapper').attr('data-pagename') === "understanding-medicare/index"){
		return param += '=' + 'HS-Understanding Medicare';
	}else if($('.header-wrapper').attr('data-pagename') === "resources/index"){
		return param += '=' + 'HS-Customer Tools'; 
	}else if($('.header-wrapper').attr('data-pagename') === "resources/plan-extras"){
		return param += '=' + 'HS-Customer Tools;Plan Extras'; 
	}else if($('.header-wrapper').attr('data-pagename') === "resources/organization-determination"){
		return param += '=' + 'HS-Customer Tools;Enrollment'; 
	}else if($('.header-wrapper').attr('data-pagename') === "contact-us"){
		return param += '=' + 'HS-contact-us';
	}else if($('.header-wrapper').attr('data-pagename') === "about-us"){
		return param += '=' + 'HS-about-Cigna-HealthSpring';
	}
}
// Build the nav path based on the structure of the page
function trackMe(parentitem,currentitem){	
	var action = initTracking('WT.z_nav');
	//For the Top Navigation
	if(parentitem.closest('ul').attr("class") != undefined){
	if(parentitem.closest('ul').attr("class").indexOf("top-left-menus") != -1 || parentitem.closest('ul').attr("class").indexOf("top-right-menus") != -1)
	{	
		if(currentitem.attr("href").toLowerCase().indexOf('javascript:void(0)') === -1){
			if(action.indexOf('WT.z_nav=HS-') === 0){
				/* This is for the HealthSpring pages */
				if(parentitem.attr('class') != undefined){
					action += ';' + 'Top-Nav;'+parentitem.attr('class').trim().replace(/[;]*/gi, '');				
				}else{
					action += ';' + 'Top-Nav;'+currentitem.text().trim().replace(/[;]*/gi, '');					
				}
			}else{
				/* This is for the main Cigna.com homepage landing pages */
				//Other links in Top Nav				
					if(parentitem.attr('class').indexOf("active") != -1){
						action += ';' + 'Top-Nav;'+parentitem.attr('class').split(' ')[1].trim().replace(/[;]*/gi, '');
					}else{
						action += ';' + 'Top-Nav;'+parentitem.attr('class').trim().replace(/[;]*/gi, '');
						/* Code below commented out to use WT.z_nav QSP for top Personal navbar tab.
						   This isn't needed anymore because of Apache config change to not URLencode the QSP on the redirect */
						//if(currentitem.attr("href").indexOf('/personal') != -1){
						//	action +=';'+'Redirect-Link';
						//	$('#QueyString-ct').text(action);
						//	action = '';
						//}
					}
			}
		}else{
			action = "";
		}	
	}else if(parentitem.closest('ul').attr("class").indexOf("country-list") != -1){					
			//For the Top Navigation country Lists for Mobile and Desktop
			action += ';Top-Nav;'+parentitem.closest('ul').closest('li').find('a:first').text().trim().replace(/[;]*/gi, '')+';'+parentitem.attr('class').trim().replace(/[;]*/gi, '');							
	}else if(parentitem.closest('ul').attr("class").indexOf("topnav") != -1){	
			//Global Navigation Main Links in desktop
			action += ';Header';			
			action += ';' + currentitem.text().trim().replace(/[;]*/gi, '');
	}else if(parentitem.closest('ul').attr("class").indexOf("subnav") != -1){			
		//Global Navigation Drop down Links for Mobile and Desktop				
			if(parentitem.attr('title') != undefined){
				action += ';Header';
				if(action.indexOf('WT.z_nav=HS-') === 0){
					/* This is for the HealthSpring pages */
					action += ';' + parentitem.closest('ul').closest('li').find('a:first').text().trim().replace(/[;]*/gi, '');
					if(parentitem.closest('ul').siblings('h2').text().trim().replace(/[;]*/gi, '') != undefined && parentitem.closest('ul').siblings('h2').text().trim().replace(/[;]*/gi, '') != ''){
						action += ';' + parentitem.closest('ul').siblings('h2').text().trim().replace(/[;]*/gi, '');
					}
					action += ';' + parentitem.attr('title').trim().replace(/[;]*/gi, '');
				}else{
					/* This is for the main Cigna.com homepage landing pages */
					if(parentitem.parents('.right-nav').length != 0 && (parentitem.closest('ul').find('.col-xs-8').text() != "About Cigna" || parentitem.closest('ul').find('.col-xs-8').text() != "Careers")){	
						action += ';' + parentitem.closest('ul').closest('li').closest('ul').find('.col-xs-8:first').text().trim().replace(/[;]*/gi, '');
					}
					action += ';' + parentitem.closest('ul').closest('li').find('a:first').text().trim().replace(/[;]*/gi, '') +';'+parentitem.attr('title').trim().replace(/[;]*/gi, '');
				}
			}else if($.trim(currentitem.text()) === "back"){
				action = "";			
			}else{	
				if(parentitem.parents('.right-nav').length != 0 && (currentitem.text() != "About Cigna" && currentitem.text() != "Careers")){
					action += ';Header';					
					if(parentitem.closest('ul').find('.col-xs-8:first').text().trim() === "About Cigna" || parentitem.closest('ul').find('.col-xs-8:first').text().trim() === "Careers"){
						action += ';' +parentitem.closest('ul').find('.col-xs-8:first').text().trim().replace(/[;]*/gi, '');							
						action += ';' + currentitem.text().trim().replace(/[;]*/gi, '');
						// Added for the Expansion Condition in Mobile Mode for the About Cigna and Careers Links
						if(currentitem.siblings('ul.subnav').length != 0){
							action +=';'+'Redirect-Link';
							$('#QueyString-ct').text(action);
							action = '';
						}
					}else{
						action += ';' +parentitem.closest('ul').closest('li').closest('ul').find('.col-xs-8:first').text().trim().replace(/[;]*/gi, '');						
						action += ';' + currentitem.text().trim().replace(/[;]*/gi, '');
					}									
				}else{
					if($('.header-wrapper').length != 0){
						action += ';Header';
						action += ';' + currentitem.text().trim().replace(/[;]*/gi, '');
					}else{
						action += ';Header';
						action += ';' + currentitem.text().trim().replace(/[;]*/gi, '');
					}
				}				
			}
	}else if(parentitem.closest('ul').attr("class") === "icon-box-ct"){
			//Global Navigation Drop down Links icon Box's only for Desktop
			action += ';Header';
			action += ';' + parentitem.closest('ul').closest('li').find('a:first').text().trim().replace(/[;]*/gi, '');
			if(action.indexOf('WT.z_nav=HS-') === 0){
				/* This is for the HealthSpring pages */
				if(currentitem.text().trim().toLowerCase().replace(/[;]*/gi, '') === 'enroll now' || currentitem.text().trim().toLowerCase().replace(/[;]*/gi, '') === 'enroll or compare now'){
					action += ';Enroll Now';
				}else {
					action += ';Icon-' + currentitem.text().trim().replace(/[;]*/gi, '');
				}
			}else {
				/* This is for the main Cigna.com homepage landing pages */
				action += ';Icon-' + currentitem.text().trim().replace(/[;]*/gi, '');
				/* We need to track links to /ifp-providers and /ifp-drug-list differently due to them being vanity 404s and redirecting via a CGI script which doesn't retain the QSP (this is being looked into to hopefully fix).
				   The cignaforbrokers link has an odd format of "#!", which makes Webtrends think it is an anchor link, and messes up the QSP, so we have to track it differently */
				if(currentitem.attr("href").indexOf('/ifp-providers') != -1 || currentitem.attr("href").indexOf('/ifp-drug-list') != -1 || currentitem.attr("href").indexOf('https://cignaforbrokers.com/broker/#!/broker/cigna:home/') != -1){
					action +=';'+'Redirect-Link';
					$('#QueyString-ct').text(action);
					action = '';
				}
			}
	}else if(parentitem.closest('ul').attr("class") === "dropdown-list"){
			//Header Utility Links Dropdown for Desktop
			action += ';UtilityNav;UtilityDropdown';
			action += ';' +parentitem.closest('ul').prev('h4').text().trim().replace(/[;]*/gi, '')+';'+currentitem.text().trim().replace(/[;]*/gi, '');	
			/* The cignaforbrokers link has an odd format of "#!", which makes Webtrends think it is an anchor link, and messes up the QSP, so we have to track it differently */
			if(currentitem.attr("href").indexOf('https://cignaforbrokers.com/broker/#!/broker/cigna:home/') != -1 || currentitem.attr("href").indexOf('/ifp-providers') != -1){
					action +=';'+'Redirect-Link';
					$('#QueyString-ct').text(action);
					action = '';
			}
	}else if(parentitem.closest('ul').attr("class").indexOf("list-inline pull-right") != -1){
			//Header Utility Links Desktop and Mobile
			action += ';UtilityNav';
			action += ';'+currentitem.text().trim().replace(/[;]*/gi, '');
			if(currentitem.attr("href").indexOf('https://cignaforbrokers.com/broker/#!/broker/cigna:home/') != -1 || currentitem.attr("href").indexOf('/ifp-providers') != -1){
					action +=';'+'Redirect-Link';
					$('#QueyString-ct').text(action);
					action = '';
			}
	}else if(parentitem.closest('ul').attr("class").indexOf("sub-top-menu") != -1){
			//Top Navigation Main Links Personal,Business,HCP in Mobile
			if(currentitem.attr("href").toLowerCase().indexOf('javascript:void(0)') === -1){
				if($('.header-wrapper').length != 0){					
					action += ';Top-Nav';
					action += ';'+currentitem.text().trim().replace(/[;]*/gi, '');
				}else{
					action += ';Top-Nav';
					action += ';'+parentitem.attr('class').trim().replace(/[;]*/gi, '');
					/* Code below commented out to use WT.z_nav QSP for top Personal navbar tab.
					   This isn't needed anymore because of Apache config change to not URLencode the QSP on the redirect */
					//if(currentitem.attr("href").indexOf('/personal') != -1){
					//	action +=';'+'Redirect-Link';
					//	$('#QueyString-ct').text(action);
					//	action = '';
					//}
				}
			}else{
				action="";
			}
	}else if(parentitem.closest('ul').attr('class').indexOf("dl-menuopen") != -1){
			if(parentitem.closest('ul').attr('class').indexOf("right-nav") != -1){
				//Top Navigation Main Links Like USA,Contact Us in Mobile
				if(currentitem.attr("href").toLowerCase().indexOf('javascript:void(0)') === -1){					
					action += ';Top-Nav';
					action += ';'+currentitem.text().trim().replace(/[;]*/gi, '');
					if(currentitem.attr("href").indexOf('http://www.cigna.com/iwov-resources/scripts/click-tracking/aboutcigna/index.html') !=-1 || currentitem.attr("href").indexOf('http://www.cigna.com/iwov-resources/scripts/click-tracking/careers/index.html') !=-1){						
						action +=';'+'Redirect-Link';
						$('#QueyString-ct').text(action);
						action = '';
					}
				}else{					
					action = '';
				}
			}else{				
				//Global Navigation Main Links in Mobile				
				action += ';Header';
				action += ';'+currentitem.text().trim().replace(/[;]*/gi, '');
				//Added this condition to Handle the Expansion Functionality for the Global Nav Section.
				if(currentitem.siblings('ul.subnav').length != 0){
					action +=';'+'Redirect-Link';
					$('#QueyString-ct').text(action);
					action = '';
				}
			}						
	}else if(parentitem.closest('ul').attr('class') === "footer-top-row"){
				//Footer Top Links: News,Investors,Corporate Responsibility,Find A Doctor,Informed On Reform,Mobile Apps,Contact Us,Feedback
				action += ';FooterTopLinks';
				action += ';'+currentitem.text().trim().replace(/[;]*/gi, '');
				if(currentitem.attr("href").toLowerCase().indexOf('javascript:void(0)') != -1 || currentitem.attr("href").indexOf('/ifp-providers') != -1){
				//if(currentitem.text().trim() === "Feedback"){
					action +=';'+'Redirect-Link';
					$('#QueyString-ct').text(action);
					action = '';
				}	
	}else if(parentitem.closest('ul').attr('class') === "cignalogo-footer-ct"){
				//Footer Cigna Logo 
				action += ';FooterLogo';
				action += ';'+$('ul.cignalogo-footer-ct li a img').attr('title').trim().replace(/[;]*/gi, '');    
				/* Code below commented out to use WT.z_nav QSP for footer Cigna logo on Personal page.
				   This isn't needed anymore because of Apache config change to not URLencode the QSP on the redirect */
				//if(currentitem.attr("href").indexOf('/personal') != -1){
				//	action +=';'+'Redirect-Link';
				//	$('#QueyString-ct').text(action);
				//	action = '';
				//}	
	}else if(parentitem.closest('ul').attr('class') === "link-column"){
				//Footer Links
				action += ';FooterLinks';
				action += ';'+currentitem.text().trim().replace(/[;]*/gi, '');
				/* We need to track link to /ifp-drug-list differently due to it being vanity 404 and redirecting via a CGI script which doesn't retain the QSP (this is being looked into to hopefully fix) */
				if(currentitem.attr("href").indexOf('/ifp-drug-list') != -1 || currentitem.attr("href").indexOf('/ifp-providers') != -1){
					action +=';'+'Redirect-Link';
					$('#QueyString-ct').text(action);
					action = '';
				}
	}else if(parentitem.closest('ul').attr('class') === "socaillinks-ct"){
				//Footer Social Media Links
				action += ';FooterSocialLinks';
				action += ';'+currentitem.text().trim().replace(/[;]*/gi, ''); 
				if(currentitem.attr("href").toLowerCase().indexOf('http://www.cigna.com/pages/rssfeeds.jsp') != -1){
					action +=';'+'Redirect-Link';
					$('#QueyString-ct').text(action);
					action = '';
				}				
	}else if(parentitem.closest('ul').attr('class') === "disclaimer-ct"){
				//Footer Disclaimer Links 
				action += ';FooterDisclaimerLinks';
				action += ';'+currentitem.text().trim().replace(/[;]*/gi, '');       
	}else if(parentitem.closest('ul').attr('class') === "securitylogo-ct"){
				//Footer Security Logo
		if(currentitem.attr("class") === "icon-third-party"){
				action += ';FooterSSLCertificates';
				action += ';'+currentitem.text().trim().replace(/[;]*/gi, ''); 
		}else{
				action += ';FooterSecurityLogo';
				action += ';'+currentitem.attr("title").trim().replace(/[;]*/gi, '');       
		}
	}else if(parentitem.closest('ul').attr('class') === "Iwantto-ct" || parentitem.closest('ul').attr('class') === "ifpbanner-ct"){
				//Landing Pages "I Want To" Orange Banner Links Desktop and Mobile & for Cigna HealthSpring (we'll treat its orange bar like an "I Want To" bar)
				action += ';IWantTo';
				action += ';'+currentitem.text().trim().replace(/[;]*/gi, '');   				
				/* We need to track link to Colibrium differently due to it being vanity 404 and redirecting via a CGI script which doesn't retain the QSP (this is being looked into to hopefully fix) */
				if(currentitem.attr("href").toLowerCase().indexOf('http://www.cigna.com/sites/colibrium/switch.htm') != -1){
					action +=';'+'Redirect-Link';
					$('#QueyString-ct').text(action);
					action = '';
				}
	}else{
			return false;
	}
}else{			
		if(currentitem.closest('p').closest('div').attr('class') === "links" || currentitem.closest('p').closest('div').attr('class') === "external-disclaimer"){
				//Footer Paragraghs Links
				action += ';FooterParagraph';
				action += ';'+currentitem.text().trim().replace(/[;]*/gi, ''); 		
		}else if(parentitem.closest('div').attr("class").indexOf("logo-div") != -1 || parentitem.closest('div').attr("class").indexOf("brand-logo") != -1){
				//Header Cigna Logo 			
				action += ';CignaLogo';
				action += ';'+currentitem.text().trim().replace(/[;]*/gi, '');	
				
				/* Code below commented out to use WT.z_nav QSP for header Cigna logo on Personal page.
				   This isn't needed anymore because of Apache config change to not URLencode the QSP on the redirect */
				//if(currentitem.attr("href").indexOf('/personal') != -1){
				//	action +=';'+'Redirect-Link';
				//	$('#QueyString-ct').text(action);
				//	action = '';
				//}	
		}else{					
			return false;   
		}	
	 }	
	// encoding in the click tracking part	
	 if(action !=''){
		var urlparams = action.split("WT.z_nav=");
		action = encodeURIComponent(urlparams[1]);
		action = "WT.z_nav="+ action;
	}
	return action;
}	
function promoboxTrackMe(currentitem){	
	var action = initTracking('WT.z_nav');
	//For the Top Navigation
	if(currentitem.find('h2').text() != ''){		
		action += ';Promo-Blocks';
		action += ';'+ currentitem.find('h2').text().trim().replace(/[;]*/gi, '');
	}else{
		return false;
	}
	if(action !=''){
		var urlparams = action.split("WT.z_nav=");
		action = encodeURIComponent(urlparams[1]);
		action = "WT.z_nav="+ action;	
	}		
	return action;
}
function promoareaTrackMe(currentitem){	
	var action = initTracking('WT.z_nav');
	var needle1 = '<br>';
	var needle2 = '<br/>';
	var needle3 = '<br />';
	var tag1 = '<sup>?</sup>';
	var tag2 = '<sup>?</sup>';
	var tag3 = '<sup>&reg;</sup>';
	var tag4 = '<strong>';
	var tag5 = '</strong>';
	//For the Top Navigation
		action += ';Promo-3Blocks';
		if(currentitem.find('h3').html().indexOf(needle1) != -1 || currentitem.find('h3').html().indexOf(needle2) != -1 || currentitem.find('h3').html().indexOf(needle3) != -1){
			if(action.indexOf('WT.z_nav=HS-') === 0){
				/* This is for the HealthSpring pages */
				action += ';'+ currentitem.find('h2').text().trim().replace(/[;]*/gi, '')+' '+currentitem.find('h3 p').html().replace(needle1,' ').replace(needle2,' ').replace(needle3,' ').trim().replace(/\s{2,}/g, ' ').replace(tag1,'').replace(tag2,'').replace(tag3,'').replace(tag4,'').replace(tag5,'');
			}else{
				/* This is for the main Cigna.com homepage landing pages */
				action += ';'+ currentitem.find('h3 p').html().replace(needle1,' ').replace(needle2,' ').replace(needle3,' ').trim().replace(/\s{2,}/g, ' ').replace(tag1,'').replace(tag2,'').replace(tag3,'').replace(tag4,'').replace(tag5,'');
			}
		}else{
			if(action.indexOf('WT.z_nav=HS-') === 0){
				/* This is for the HealthSpring pages */
				action += ';'+currentitem.find('h2').text().trim().replace(/[;]*/gi, '')+' '+currentitem.find('h3').text().trim().replace(/[;]*/gi, '').replace(tag1,'').replace(tag2,'').replace(tag3,'').replace(tag4,'').replace(tag5,'');
			}else{
				/* This is for the main Cigna.com homepage landing pages */
				action += ';'+ currentitem.find('h3').text().trim().replace(/[;]*/gi, '').replace(tag1,'').replace(tag2,'').replace(tag3,'').replace(tag4,'').replace(tag5,'');
			}
		}		
		var urlparams = action.split("WT.z_nav=");
		action = encodeURIComponent(urlparams[1]);
		action = "WT.z_nav="+ action;		
	return action;
}
function promoctaclickableWrapperTrackMe(currentitem){	
		var action = initTracking('WT.z_nav');		
		if(action.indexOf('WT.z_nav=HS-') === 0){
			/* This is for the HealthSpring pages */
			action += ';InfoBars';
			action += ';'+currentitem.siblings('h2').text().trim().replace(/[;]*/gi, '')+';'+currentitem.find('h3').text().trim().replace(/[;]*/gi, '');
			if(currentitem.attr("data-targeturl").indexOf('http://www.cigna.com/iwov-resources/medicare-2015/docs/2015-otc-catalog.pdf') != -1 || currentitem.attr("data-targeturl").indexOf('http://www.mycignahealthspring.com/ChangeAddress.aspx') != -1 || currentitem.attr("data-targeturl").indexOf('http://communication.cigna.com/?elqPURLPage=1082/') != -1){		
				action +=';'+'Redirect-Link';
				$('#QueyString-ct').text(action);
				action = '';
			}	
		}else{
			/* This is for the main Cigna.com homepage landing pages */
			if(currentitem.siblings('h2').text().trim().replace(/[;]*/gi, '') != ''){
				action += ';Promo-CTA-Wrapper';
				action += ';'+currentitem.siblings('h2').text().trim().replace(/[;]*/gi, '')+';'+currentitem.find('h3').text().trim().replace(/[;]*/gi, '');
			}else{
				action += ';Promo-CTA-Wrapper';
				action += ';'+currentitem.find('h3').text().trim().replace(/[;]*/gi, '') +'-'+currentitem.find('a').text().trim().replace(/[;]*/gi, '');
			}
			/**if(currentitem.attr("data-targeturl").indexOf('http://cigna.promo.eprize.com/hcrig/?affiliate_id=cigna') != -1){**/
				action +=';'+'Redirect-Link';
				$('#QueyString-ct').text(action);
				action = '';
			/**}**/
		}  
		if(action !=''){
			var urlparams = action.split("WT.z_nav=");
			action = encodeURIComponent(urlparams[1]);
			action = "WT.z_nav="+ action;		
		}
	return action;
}
function promoctaWrapperTrackMe(currentitem){	
		var action = initTracking('WT.z_nav');		
		action += ';Promo-CTA-Wrapper';
		action += ';'+ currentitem.text().trim().replace(/[;]*/gi, '');
		var urlparams = action.split("WT.z_nav=");
		action = encodeURIComponent(urlparams[1]);
		action = "WT.z_nav="+ action;		
	return action;
}
function promoheroctaBannerTrackMe(currentitem){	
		var action = initTracking('WT.z_nav');				
		action += ';HeroImage-Banner';	
		if(currentitem.closest('div').siblings('h3').text().trim() != undefined && currentitem.closest('div').siblings('h3').text().trim() !=''){			
			action += ';'+currentitem.closest('div').siblings('h3').text().trim().replace(/[;]*/gi, '');
		}else{			
			if(action.indexOf('WT.z_nav=HS-') === 0){
				/* This is for the HealthSpring pages */
				action += ';'+currentitem.closest('div').siblings('h1').text().trim().replace(/[;]*/gi, '');
			}else{
				/* This is for the main Cigna.com homepage landing pages */
				action += ';'+currentitem.closest('div').siblings('h2').text().trim().replace(/[;]*/gi, '');
			}
		}
		action += '-' + currentitem.text().trim().replace(/[;]*/gi, '');
		var urlparams = action.split("WT.z_nav=");
		action = encodeURIComponent(urlparams[1]);
		action = "WT.z_nav="+ action;		
	return action;
}
function promoheromobilectaBannerTrackMe(currentitem){	
		var action = initTracking('WT.z_nav');				
		action += ';HeroImage-Banner';		
		if(currentitem.closest('div').siblings().find('h3').text().trim() != undefined && currentitem.closest('div').siblings().find('h3').text().trim() !=''){			
			action += ';'+currentitem.closest('div').siblings().find('h3').text().trim().replace(/[;]*/gi, '');
		}else{			
			if(action.indexOf('WT.z_nav=HS-') === 0){
				/* This is for the HealthSpring pages */
				action += ';'+currentitem.closest('div').siblings().find('h1').text().trim().replace(/[;]*/gi, '');
			}else{
				/* This is for the main Cigna.com homepage landing pages */
				action += ';'+currentitem.closest('div').siblings().find('h2').text().trim().replace(/[;]*/gi, '');
			}
		}
		action += '-' + currentitem.text().trim().replace(/[;]*/gi, '');
		var urlparams = action.split("WT.z_nav=");
		action = encodeURIComponent(urlparams[1]);
		action = "WT.z_nav="+ action;		
	return action;
}
function promoctaBannerTrackMe(currentitem){	
		var action = initTracking('WT.z_nav');				
		action += ';Promo-CTA-Banner';	
		if(currentitem.closest('div').siblings('h3').text().trim() != undefined && currentitem.closest('div').siblings('h3').text().trim() !=''){			
			if(action.indexOf('WT.z_nav=HS-') === 0){
				/* This is for the HealthSpring pages */
				action += ';'+currentitem.closest('div').siblings('h3').text().trim().replace(/[;]*/gi, '');
			}else{
				/* This is for the main Cigna.com homepage landing pages */
				action += ';'+currentitem.closest('div').siblings('h3').text().trim().replace(/[;]*/gi, '')+'-'+currentitem.text().trim().replace(/[;]*/gi, '');
			}
		}else{			
			if(action.indexOf('WT.z_nav=HS-') === 0){
				/* This is for the HealthSpring pages */
				action += ';'+currentitem.closest('div').siblings('h2').text().trim().replace(/[;]*/gi, '');
			}else{
				/* This is for the main Cigna.com homepage landing pages */
				action += ';'+currentitem.closest('div').siblings('h2').text().trim().replace(/[;]*/gi, '')+'-'+currentitem.text().trim().replace(/[;]*/gi, '');
			}
		}
		var urlparams = action.split("WT.z_nav=");
		action = encodeURIComponent(urlparams[1]);
		action = "WT.z_nav="+ action;		
	return action;
}
function promomobilectaBannerTrackMe(currentitem){	
		var action = initTracking('WT.z_nav');				
		action += ';Promo-CTA-Banner';		
		if(currentitem.closest('div').siblings().find('h3').text().trim() != undefined && currentitem.closest('div').siblings().find('h3').text().trim() !=''){			
			if(action.indexOf('WT.z_nav=HS-') === 0){
				/* This is for the HealthSpring pages */
				action += ';'+currentitem.closest('div').siblings().find('h3').text().trim().replace(/[;]*/gi, '');
			}else{
				/* This is for the main Cigna.com homepage landing pages */
				action += ';'+currentitem.closest('div').siblings().find('h3').text().trim().replace(/[;]*/gi, '')+'-'+currentitem.text().trim().replace(/[;]*/gi, '');
			}
		}else{			
			if(action.indexOf('WT.z_nav=HS-') === 0){
				/* This is for the HealthSpring pages */
				action += ';'+currentitem.closest('div').siblings().find('h2').text().trim().replace(/[;]*/gi, '');
			}else{
				/* This is for the main Cigna.com homepage landing pages */
				action += ';'+currentitem.closest('div').siblings().find('h2').text().trim().replace(/[;]*/gi, '')+'-'+currentitem.text().trim().replace(/[;]*/gi, '');
			}
		}
		var urlparams = action.split("WT.z_nav=");
		action = encodeURIComponent(urlparams[1]);
		action = "WT.z_nav="+ action;		
	return action;
}
function heroBannerTrackMe(currentitem){	
		var action = initTracking('WT.z_nav');		
		action += ';HeroImage-Banner';			
		if(currentitem.parent().siblings('h2').text().trim() != undefined && currentitem.parent().siblings('h2').text().trim() !=''){			
			action += ';'+currentitem.parent().siblings('h2').text().trim().replace(/[;]*/gi, '');
		}else{			
			action += ';'+currentitem.text().trim().replace(/[;]*/gi, '');
		}		
		var urlparams = action.split("WT.z_nav=");
		action = encodeURIComponent(urlparams[1]);
		action = "WT.z_nav="+ action;		
	return action;
}
function newsAreaTrackMe(currentitem){	
		var action = initTracking('WT.z_nav');		
		action += ';News-Area';
		if(currentitem.text().trim() === "Read More"){
			action += ';'+ currentitem.parent().prev('h3').text().trim().replace(/[;]*/gi, '');
		}else{
			action += ';'+ currentitem.text().trim().replace(/[;]*/gi, '');
		}
		var urlparams = action.split("WT.z_nav=");
		action = encodeURIComponent(urlparams[1]);
		action = "WT.z_nav="+ action;		
	return action;
}
function healthwellnessTrackMe(currentitem){	
		var action = initTracking('WT.z_nav');	
		if($('#pagename').text().indexOf('/individual-and-family-plans/index') != -1){
			action += ';KnowledgeCenter-Text';
		}else{
			action += ';Healthwellness-Text';
		}			
		action += ';'+ currentitem.text().trim().replace(/[;]*/gi, '');	
		var urlparams = action.split("WT.z_nav=");
		action = encodeURIComponent(urlparams[1]);
		action = "WT.z_nav="+ action;		
	return action;
}
function accordion_links(currentitem){						
		if(currentitem.attr('href').indexOf('mailto:') != -1 || currentitem.attr('href').indexOf('tel:') != -1){
			action ="";			
		}else{
			var action = initTracking('WT.z_nav');		
			action += ';accordion';
			action += ';'+ currentitem.closest('dd').prev().text().trim().replace(/[;]*/gi, '')+';'+currentitem.text().trim().replace(/[;]*/gi, '');	
			var urlparams = action.split("WT.z_nav=");
			action = encodeURIComponent(urlparams[1]);
			action = "WT.z_nav="+ action;
		}
		if(currentitem.attr("href").toLowerCase().indexOf('eloqua-contact-form') != -1 || currentitem.attr("href").toLowerCase().indexOf('/medicare') != -1){			
			action +=';'+'Redirect-Link';
			$('#QueyString-ct').text(action);
			action = '';
		}	
	return action;
}
function accordion_mainlinks(currentitem){								
	var action = initTracking('WT.z_nav');		
	action += ';accordion';
	action += ';'+currentitem.text().trim().replace(/[;]*/gi, '');	
	var urlparams = action.split("WT.z_nav=");
	action = encodeURIComponent(urlparams[1]);
	action = "WT.z_nav="+ action;			
	return action;
}
function breadcrumb(currentitem){	
		var action = initTracking('WT.z_nav');		
		action += ';breadcrumb';
		action += ';'+ currentitem.text().trim().replace(/[;]*/gi, '');	
		var urlparams = action.split("WT.z_nav=");
		action = encodeURIComponent(urlparams[1]);
		action = "WT.z_nav="+ action;		
	return action;
}
function printicon(currentitem){	
		var action = initTracking('WT.z_nav');
		if(action.indexOf('WT.z_nav=HS-') === 0){
			/* This is for the HealthSpring pages */
			if($('.carousal-text').length != 0){
				action += ';HeroImage-Banner';
			}else{
				action += ';No-Banner';
			}
		}else{
			/* This is for the main Cigna.com homepage landing pages */
			action += ';HeroImage-Banner';
		}
		action += ';'+ currentitem.attr('title').trim().replace(/[;]*/gi, '');
		var urlparams = action.split("WT.z_nav=");
		action = encodeURIComponent(urlparams[1]);
		action = "WT.z_nav="+ action;		
	return action;
}
function share(currentitem){	
		var action = initTracking('WT.z_nav');		
		if(action.indexOf('WT.z_nav=HS-') === 0){
			/* This is for the HealthSpring pages */
			if($('.carousal-text').length != 0){
				action += ';HeroImage-Banner';
			}else{
				action += ';No-Banner';
			}
		}else{
			/* This is for the main Cigna.com homepage landing pages */
			action += ';HeroImage-Banner';
		}
		action += ';Share;'+ currentitem.contents(":not(span)").text().trim().replace(/[;]*/gi, '');
		var urlparams = action.split("WT.z_nav=");
		action = encodeURIComponent(urlparams[1]);
		action = "WT.z_nav="+ action;		
	return action;
}


/* Below functions are for HealthSpring */
function rightRailPromoTrackMe(currentitem){	
		var action = initTracking('WT.z_nav');		
		action += ';Promo';
		if(currentitem.siblings('h2').text().trim().replace(/[;]*/gi, '') != undefined && currentitem.siblings('h2').text().trim().replace(/[;]*/gi, '') != ''){			
			if(currentitem.siblings('h2').text().trim().toLowerCase() === 'enroll or compare online now' || currentitem.siblings('h2').text().trim().toLowerCase() === 'enroll online now'){
				action += ';Enroll Now';
			}else{
				action += ';'+currentitem.siblings('h2').text().trim().replace(/[;]*/gi, '');
			}
		}else{			
			if(currentitem.closest('section').find('h2').text().trim().toLowerCase() === 'enroll or compare online now' || currentitem.closest('section').find('h2').text().trim().toLowerCase() === 'enroll online now'){
				action += ';Enroll Now';
			}else{
				action += ';'+currentitem.closest('section').find('h2').text().trim().replace(/[;]*/gi, '');
			}
		}
		var urlparams = action.split("WT.z_nav=");
		action = encodeURIComponent(urlparams[1]);
		action = "WT.z_nav="+ action;		
	return action;
}
function articleTrackMe(currentitem){
		if(currentitem.attr('href').indexOf('mailto:') != -1 || currentitem.attr('href').indexOf('tel:') != -1){
			action ="";			
		}else{
			var action = initTracking('WT.z_nav');		
			action += ';Body';
			if(currentitem.text().trim().toLowerCase() === 'enroll online now' || currentitem.text().trim().toLowerCase() === 'compare plans and enroll online'){
				action += ';Enroll Now';
			}else{
				action += ';'+currentitem.text().trim().replace(/[;]*/gi, '');
			}
			var urlparams = action.split("WT.z_nav=");
			action = encodeURIComponent(urlparams[1]);
			action = "WT.z_nav="+ action;
			if(currentitem.attr("href").toLowerCase().indexOf('http://www.cigna.com/sites/colibrium/switch.htm') != -1){
				action +=';'+'Redirect-Link';
				$('#QueyString-ct').text(action);
				action = '';
			}	
		}	
	return action;
}
function needHelpTrackme(currentitem){	
		var action = initTracking('WT.z_nav');		
		action += ';Leaf Pullout;Expansion';		
		var urlparams = action.split("WT.z_nav=");
		action = encodeURIComponent(urlparams[1]);
		action = "WT.z_nav="+ action;		
	return action;
}
function needHelpLinksTrackme(currentitem){	
		var action = initTracking('WT.z_nav');		
		action += ';Leaf Pullout';	
		if(currentitem.parent().closest('div').siblings('h4').text().trim().replace(/[;]*/gi, '') != ''){			
				action += ';'+currentitem.parent().closest('div').siblings('h4').text().trim().replace(/[;]*/gi, '')+';'+currentitem.text().trim().replace(/[;]*/gi, '');			
		}else{
			if(currentitem.attr('href').indexOf('aboutcigna/cigna-mobile') != -1){
				action += ';'+currentitem.siblings('strong').text().trim().replace(/[;]*/gi, '')+';'+currentitem.text().trim().replace(/[;]*/gi, '');
			}else if(currentitem.attr('href').indexOf('get-started') != -1){		
				action += ';'+currentitem.siblings('h4').text().trim().replace(/[;]*/gi, '');			
			}else{
				action += ';'+currentitem.parent().closest('div').siblings('h3').text().trim().replace(/[;]*/gi, '')+';'+currentitem.text().trim().replace(/[;]*/gi, '');
			}
		}
		var urlparams = action.split("WT.z_nav=");
		action = encodeURIComponent(urlparams[1]);
		action = "WT.z_nav="+ action;		
	return action;
}
function ifpBanner(currentitem){	
		var action = initTracking('WT.z_nav');		
		action += ';HeroImage-Banner';
		action += ';'+currentitem.text().trim().replace(/[;]*/gi, '');	
		var urlparams = action.split("WT.z_nav=");
		action = encodeURIComponent(urlparams[1]);
		action = "WT.z_nav="+ action;		
	return action;
}
function planSelectorLeftContainer(currentitem){	
		var action = initTracking('WT.z_nav');		
		if(currentitem.parent().prevAll('div.plansLeftTitle:first').text().trim().replace(/[;]*/gi, '') != undefined && currentitem.parent().prevAll('div.plansLeftTitle:first').text().trim().replace(/[;]*/gi, '') !=''){
			action += ';'+currentitem.parent().prevAll('div.plansLeftTitle:first').text().trim().replace(/[;]*/gi, '')+';'+currentitem.text().trim().replace(/[;]*/gi, '');	
		}else{
			action += ';'+currentitem.closest('div').prevAll('div.plansLeftTitle:first').text().trim().replace(/[;]*/gi, '')+';'+currentitem.text().trim().replace(/[;]*/gi, '');	
		}
		var urlparams = action.split("WT.z_nav=");
		action = encodeURIComponent(urlparams[1]);
		action = "WT.z_nav="+ action;
		if(currentitem.attr("href").indexOf('/ifp-drug-list') != -1 || currentitem.attr("href").indexOf('/ifp-providers') != -1){
			action +=';'+'Redirect-Link';
			$('#QueyString-ct').text(action);
			action = '';
		}					
	return action;
}
//This fn is for IFP Right Rail and Left Rail Promo Blocks
function ifpRailPromoTrackMe(currentitem){	
		var action = initTracking('WT.z_nav');		
		if(currentitem.hasClass('rectangular-box')){			
			action += ';LeftRail';	
		}else{			
			action += ';RightRail';	
		}		
		action += ';'+currentitem.find('h2').text().trim().replace(/[;]*/gi, '');		
		var urlparams = action.split("WT.z_nav=");
		action = encodeURIComponent(urlparams[1]);
		action = "WT.z_nav="+ action;		
	return action;
}
function ifpRailPromoTrackMeLinks(currentitem){
		var action = initTracking('WT.z_nav');	
		if (currentitem.parents('.rectangular-box').length){			
			action += ';LeftRail';	
		}else{
			action += ';RightRail';	
		}				
		action += ';'+currentitem.closest('section').find('h2').text().trim().replace(/[;]*/gi, '')+';'+currentitem.text().trim().replace(/[;]*/gi, '');	
		var urlparams = action.split("WT.z_nav=");
		action = encodeURIComponent(urlparams[1]);
		action = "WT.z_nav="+ action;		
		if(currentitem.attr("href").indexOf('/ifp-drug-list') != -1 || currentitem.attr("href").indexOf('/ifp-providers') != -1 || currentitem.attr("href").indexOf('/web/public/ifphcpdirectory/') != -1){
			action +=';'+'Redirect-Link';
			$('#QueyString-ct').text(action);
			action = '';
		}		
	return action;	
}	
function ifpaccordion_mainlinks(currentitem){
		var action = initTracking('WT.z_nav');		
		action += ';accordion';	
		action += ';'+currentitem.find('h4').text().trim().replace(/[;]*/gi, '');	
		var urlparams = action.split("WT.z_nav=");
		action = encodeURIComponent(urlparams[1]);
		action = "WT.z_nav="+ action;		
	return action;	
}	
function dentalBannerTrackme(currentitem){
		var action = initTracking('WT.z_nav');		
		action += ';HeroImage-Banner';	
		action += ';'+currentitem.text().trim().replace(/[;]*/gi, '');	
		var urlparams = action.split("WT.z_nav=");
		action = encodeURIComponent(urlparams[1]);
		action = "WT.z_nav="+ action;		
		if(currentitem.attr("href").toLowerCase().indexOf('http://www.cigna.com/sites/colibrium/switch.htm') != -1){
			action +=';'+'Redirect-Link';
			$('#QueyString-ct').text(action);
			action = '';
		}
	return action;
}
function getQueryStringParams(sParam)  
{  
    var sPageURL = window.location.search.substring(1);  
    var sURLVariables = sPageURL.split('&');  
     for (var i = 0; i < sURLVariables.length; i++)   
     {  
         var sParameterName = sURLVariables[i].split('=');  
         if (sParameterName[0] == sParam){  
			return sParameterName[1];  
		}  
	 }  
}