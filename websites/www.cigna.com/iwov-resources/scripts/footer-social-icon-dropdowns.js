$(document).ready(function () {

var $head = $('head');
if ($head.html().indexOf('../../file-not-found'/*tpa=http://www.cigna.com/iwov-resources/scripts/footer-social-icon-dropdowns.min.css*/) == -1) $head.append('<link rel="stylesheet" href="../css/footer-social-icon-dropdowns.min.css"/*tpa=http://www.cigna.com/iwov-resources/css/footer-social-icon-dropdowns.min.css*/>'); 

function viewport() { // use this viewport function instead of $(window).width() because viewport excludes the scrollbar

			//var isWebkit = 'WebkitAppearance' in document.documentElement.style;
			var browserIsSafari = navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0;

			if (browserIsSafari) {
				return { width: $('body').innerWidth() };
			}

		else {
	
			var e = window, a = 'inner';

			if (!('innerWidth' in window)) {
				a = 'client';
				e = document.documentElement || document.body;
			}
			
			return { width: e[a + 'Width'], height: e[a + 'Height'] };

		}
		}
		
		var winWidth = viewport().width;

	var $socialIcons = $('.social-links'),
		expandableSocialIcons = [		
		{
            class: '.facebook',
            expandableLinks: {			
				row0: [
					{
						txt: 'Cigna Corporate',
						url: 'https://www.facebook.com/CIGNA'
					}, 	
					{
						txt: 'Cigna India',
						url: 'https://www.facebook.com/CignattkInsurance?fref=ts'
					},					
					{
						txt: 'Cigna Taiwan',
						url: 'https://www.facebook.com/CignaTaiwan'
					}					
				],
				row1: [
					{
						txt: 'Cigna Careers',
						url: 'https://www.facebook.com/CIGNACareerspage'
					},	
					{
						txt: 'Cigna Indonesia',
						url: 'https://www.facebook.com/CignaIndonesia'
					},		
					{
						txt: 'Cigna Thailand',
						url: 'https://www.facebook.com/CignaThailand'
					}			
				],
				row2: [
					{
						txt: 'Cigna en Espa&#241;ol',
						url: 'https://www.facebook.com/cignaenespanol'
					},
					{
						txt: 'Cigna Korea',
						url: 'https://www.facebook.com/Linasns'
					},
					{
						txt: 'Cigna Together',
						url: 'https://www.facebook.com/cignatogether'
					}
				],
				row3: [
					{
						txt: 'Cigna - HealthSpring',
						url: 'https://www.facebook.com/cignahealthspring'
					},	
					{
						txt: 'Cigna New Zealand',
						url: 'https://www.facebook.com/CignaNZ'
					},					
					{
						txt: 'Cigna Turkey',
						url: 'https://www.facebook.com/CignaFinans.EH'
					}	
				]
			}			
        },
		{
            class: '.twitter',
            expandableLinks: {			
				row0: [
					{
						txt: 'Cigna Corporate',
						url: 'https://twitter.com/Cigna'
					}, 	
					{
						txt: 'Cigna India',
						url: 'https://twitter.com/CignaTTKHealth'
					},					
					{
						txt: 'Cigna Turkey',
						url: 'https://twitter.com/CignaFinans_EH'
					}					
				],
				row1: [
					{
						txt: 'Cigna Careers',
						url: 'https://twitter.com/Cignacareers'
					},	
					{
						txt: 'Cigna Indonesia',
						url: 'https://twitter.com/Cigna_ID'
					},
					{
						txt: 'Cigna UK',
						url: 'https://twitter.com/CignaUKHB'
					}				
				],
				row2: [
					{
						txt: 'Cigna en Espa&#241;ol',
						url: 'https://twitter.com/Cigna_Espanol'
					},
							{
						txt: 'Cigna Medical Group',
						url: 'https://twitter.com/CMG_AZ'
					},	
						{
						txt: 'Customer Service',
						url: 'https://twitter.com/cignaquestions'
					}
				],
				row3: [
					{
						txt: 'Cigna - HealthSpring',
						url: 'https://twitter.com/CignaHS'
					},
					{
						txt: 'Cigna New Zealand',
						url: 'https://twitter.com/cignanz'
					}
				]
			}			
			},
			{
            class: '.linkedin',
            expandableLinks: {			
				row0: [
					{
						txt: 'Cigna Corporate',
						url: 'http://www.linkedin.com/company/cigna'
					}, 		
					{
						txt: 'Cigna New Zealand',
						url: 'https://www.linkedin.com/company/cignanz'
					}										
				],
				row1: [
					{
						txt: 'Cigna - HealthSpring',
						url: 'http://www.linkedin.com/company/healthspring'
					},
					{
						txt: 'Cigna UK',
						url: 'http://www.linkedin.com/company/cigna-uk-healthcare-benefits'
					}						
				],
				row2: [
					{
						txt: 'Cigna India',
						url: 'http://www.linkedin.com/company/2793388?trk=tyah'
					},	
					{
						txt: 'Vanbreda',
						url: 'http://www.linkedin.com/company/vanbreda-international'
					}
				]				
			}			
        },
		{
            class: '.youtube',
            expandableLinks: {			
				row0: [
					{
						txt: 'Cigna Corporate',
						url: 'https://www.youtube.com/user/cigna'
					}, 
					{
						txt: 'Cigna New Zealand',
						url: 'https://www.youtube.com/user/CignaNZ'
					},	
					{
						txt: 'Cigna UK',
						url: 'http://www.youtube.com/user/CignaUK'
					}			
				],
				row1: [
					{
						txt: 'Cigna - HealthSpring',
						url: 'http://www.youtube.com/user/healthspring'
					},
						{
						txt: 'Cigna Taiwan',
						url: 'http://www.youtube.com/user/CignaTaiwan'
					},	
					{
						txt: 'Cigna University',
						url: 'http://www.youtube.com/user/CIGNAUniversity'
					}			
				],
				row2: [
					{
						txt: 'Cigna Hong Kong',
						url: 'http://www.youtube.com/user/hkcigna/videos'
					},
					{
						txt: 'Cigna Thailand',
						url: 'http://www.youtube.com/user/cignathailand'
					}		
				],
				row3: [
					{
						txt: 'Cigna Korea',
						url: 'http://www.youtube.com/user/Linasns'
					},	
						{
						txt: 'Cigna Turkey',
						url: 'http://www.youtube.com/user/CignaTurkey'
					}			
				]
			}			
			},
		{
            class: '.google-plus',
            expandableLinks: {			
				row0: [
					{
						txt: 'Cigna',
						url: 'https://plus.google.com/+cigna/'
					}					
				],
				row1: [
					{
						txt: 'Cigna - HealthSpring',
						url: 'https://plus.google.com/110927927163403973041'
					}					
				],
				row2: [
					{
						txt: 'Cigna Korea',
						url: 'https://plus.google.com/101043125132646361583/posts'
					}
				],
				row3: [
					{
						txt: 'Customer Taiwan',
						url: 'https://plus.google.com/+CignaTaiwan/'
					}
				]
			}			
			}
		],
		ddSlideTime = 250,
        dropdownClass = 'socialIconDropdown',
		$dropdown = $('.' + dropdownClass),
		socialIcons = { // this breakpoint refers to the split within the "mobile" view
			/*pinterest: '<a class="pinterest" href="http://www.pinterest.com/cignatogether/" target="_blank"><span></span>Pinterest</a>',
			weibo: '<a class="weibo" href="http://www.weibo.com/u/3546314801" target="_blank"><span></span>Weibo</a>',
			viewAll: '<a class="viewAll" href="http://www.cigna.com/aboutcigna/company-profile/social-media" target="_blank"><span></span>View All</a>'*/
		};

		$('footer .main-wrap .container').addClass('social-linksContainer');		
		
		$socialIcons.find('.row').children().each(function(){
			var $t = $(this),
				tHtml = $t.html();
			if (tHtml.length > 10) {				
				var name = $t.find('a').attr('class').split(' ')[0],
					$li = function() {
						return $t.find('li').length > 0 ? $t.find('li') : $t;
					}
				socialIcons[name] = $li().html().replace(/ last-child/g, '');													
			}								
		});

		$socialIcons.hide();		
				
		$socialIcons.after('<div class="social-links social-links2">' + socialIcons['linkedin'] + socialIcons['facebook'] + socialIcons['twitter'] + socialIcons['youtube'] + socialIcons['podcast'] + socialIcons['google-plus'] + socialIcons['pinterest'] + socialIcons['weibo'] + socialIcons['instagram'] + socialIcons['view-all'] + '</div>');	

		$socialIcons = $('.social-links2');

		$socialIcons.find('.youtube').after('<div class="socialIconDivider socialIconDivider0 socialIconDivider_400plus"></div>');
		$socialIcons.find('.twitter').after('<div class="socialIconDivider socialIconDivider0 socialIconDivider_below400"></div>');		
		$socialIcons.find('.google-plus').after('<div class="socialIconDivider socialIconDivider1 socialIconDivider_below400"></div>');
		$socialIcons.find('.weibo').after('<div class="socialIconDivider socialIconDivider1 socialIconDivider_400plus"></div>');
		
		/*$('.social-links > .row > div').each(function(){
			$t = $(this),
				_class = $t.attr('class');
			$t.removeClass(_class);
		});*/			
	
	//for (var icon in expandableSocialIcons) {	
	for (var icon in expandableSocialIcons) {	
		var _icon = expandableSocialIcons[icon],
			expandableLinks = _icon.expandableLinks;
			_icon.expandableLinksHtml = '<div class="linkTable">';
		/*$(_icon['class']).click(function(e){			
			return false;
			e.stopPropagation();
		});*/
		for (var row in expandableLinks) {
			_icon.expandableLinksHtml += '<div class="linkRow">';
			for (var link in expandableLinks[row]) {
				_icon.expandableLinksHtml += '<div class="linkCol"><a href="' + _icon.expandableLinks[row][link].url + '" target="_blank">' + _icon.expandableLinks[row][link].txt + '</a></div>';			
			}
			_icon.expandableLinksHtml += '</div>';
		}		
	}
	
	function closeOtherDropdowns(openDropdownId) {
		$('.socialIconDropdown').each(function(){
			var _t = $(this),
				id = openDropdownId + 'Dropdown',
				mobileIds = [id + '_mobile', id + '_mobile_below400', id + '_mobile_400plus'];
			if (_t.attr('id') != id && mobileIds.indexOf(_t.attr('id')) == -1) {
				_t.stop().slideUp(ddSlideTime,function(){
					_t.prev('.ddIconArrow').hide();
				});
			}					
		});
				
	}
	
	$('.socialIconDivider0.socialIconDivider_below400').append('<div class="' + dropdownClass + ' ' + dropdownClass + '_mobile" id="' + expandableSocialIcons[0].class.substring(1, expandableSocialIcons[0].class.length) + 'Dropdown_mobile_below400"><div>' + expandableSocialIcons[0].expandableLinksHtml + '</div></div></div><div class="' + dropdownClass + ' ' + dropdownClass + '_mobile" id="' + expandableSocialIcons[1].class.substring(1, expandableSocialIcons[1].class.length) + 'Dropdown_mobile_below400"><div>' + expandableSocialIcons[1].expandableLinksHtml + '</div></div></div><div class="' + dropdownClass + ' ' + dropdownClass + '_mobile" id="' + expandableSocialIcons[2].class.substring(1, expandableSocialIcons[2].class.length) + 'Dropdown_mobile_below400"><div>' + expandableSocialIcons[2].expandableLinksHtml + '</div></div></div>');
	
	$('.socialIconDivider1.socialIconDivider_below400').append('<div class="' + dropdownClass + ' ' + dropdownClass + '_mobile" id="' + expandableSocialIcons[3].class.substring(1, expandableSocialIcons[3].class.length) + 'Dropdown_mobile_below400"><div>' + expandableSocialIcons[3].expandableLinksHtml + '</div></div></div><div class="' + dropdownClass + ' ' + dropdownClass + '_mobile" id="' + expandableSocialIcons[4].class.substring(1, expandableSocialIcons[4].class.length) + 'Dropdown_mobile_below400"><div>' + expandableSocialIcons[4].expandableLinksHtml + '</div></div></div>');	
	
	$('.socialIconDivider0.socialIconDivider_400plus').append('<div class="' + dropdownClass + ' ' + dropdownClass + '_mobile" id="' + expandableSocialIcons[0].class.substring(1, expandableSocialIcons[0].class.length) + 'Dropdown_mobile_400plus"><div>' + expandableSocialIcons[0].expandableLinksHtml + '</div></div></div><div class="' + dropdownClass + ' ' + dropdownClass + '_mobile" id="' + expandableSocialIcons[1].class.substring(1, expandableSocialIcons[1].class.length) + 'Dropdown_mobile_400plus"><div>' + expandableSocialIcons[1].expandableLinksHtml + '</div></div></div><div class="' + dropdownClass + ' ' + dropdownClass + '_mobile" id="' + expandableSocialIcons[2].class.substring(1, expandableSocialIcons[2].class.length) + 'Dropdown_mobile_400plus"><div>' + expandableSocialIcons[2].expandableLinksHtml + '</div></div></div><div class="' + dropdownClass + ' ' + dropdownClass + '_mobile" id="' + expandableSocialIcons[3].class.substring(1, expandableSocialIcons[3].class.length) + 'Dropdown_mobile_400plus"><div>' + expandableSocialIcons[3].expandableLinksHtml + '</div></div></div>');	
	
	$('.socialIconDivider1.socialIconDivider_400plus').append('<div class="' + dropdownClass + ' ' + dropdownClass + '_mobile" id="' + expandableSocialIcons[4].class.substring(1, expandableSocialIcons[4].class.length) + 'Dropdown_mobile_400plus"><div>' + expandableSocialIcons[4].expandableLinksHtml + '</div></div></div>');	
	
	$.each(expandableSocialIcons, function(index, val) {
		var _icon = expandableSocialIcons[index];
		$('.social-links').find(_icon.class).append('<div class="ddIconArrow"></div><div class="' + dropdownClass + '" id="' + _icon.class.substring(1, _icon.class.length) + 'Dropdown"><div>' + _icon.expandableLinksHtml + '</div></div></div>').mouseenter(function(){	
		//$(this).find('.ddIconArrow').show();
		if (winWidth >= 400 && winWidth < 700) $('.' + dropdownClass + '#' + _icon.class.substring(1, _icon.class.length) + 'Dropdown_mobile_400plus').slideDown(ddSlideTime);				
		else if (winWidth < 400) $('.' + dropdownClass + '#' + _icon.class.substring(1, _icon.class.length) + 'Dropdown_mobile_below400').slideDown(ddSlideTime);	
		else $('.' + dropdownClass + '#' + _icon.class.substring(1, _icon.class.length) + 'Dropdown').slideDown(ddSlideTime);		
		
		closeOtherDropdowns(_icon.class.substring(1,_icon.class.length));			
		//autoCloseDropdown('#' + _icon.class.substring(1, _icon.class.length) + 'Dropdown');		
		});
	});
	
	$('#google-plusDropdown .linkTable, #google-plusDropdown_mobile_400plus .linkTable, #google-plusDropdown_mobile_below400 .linkTable').addClass('oneCol');		
	
	$('.linkCol').each(function(){
		var $t = $(this);
		if ($t.html().indexOf('undefined') != -1) $t.hide();
	});
	
	function repositionDropdowns() {
		$dropdown.each(function(){
			$t = $(this),
				_w = $t.width(),
				marginLeft = -_w/2;
			$t.css({marginLeft: marginLeft});
		});
	}
	
	function repositionDropdownArrows() {		
		//var docMinWidth = $('.ls-canvas').css('min-width');
		var newPos = {};
		if (winWidth >= 400 && winWidth < 700) {			
			//$('#twitterDropdown_mobile_400plus').css({backgroundPositionX: 196});
			//$('#facebookDropdown_mobile_400plus').css({backgroundPositionX: 32});
		}
		else if (winWidth < 400) {				
			if (winWidth <= 300) {
				newPos.linkedin = 44;				
				newPos.twitter = 234;				
			}
			else {
				newPos.linkedin = winWidth/2 - ($('.social-links > a').width() + 54);						
				newPos.twitter = winWidth/2 + $('.social-links > a').width() + 18;									
			}
			newPos.youtube = newPos.linkedin;	
			newPos.googlePlus = newPos.twitter;	
			$('#linkedinDropdown_mobile_below400').css({backgroundPosition: newPos.linkedin + 'px 0'});
			$('#twitterDropdown_mobile_below400').css({backgroundPosition: newPos.twitter + 'px 0'});
			$('#youtubeDropdown_mobile_below400').css({backgroundPosition: newPos.youtube + 'px 0'});
			$('#google-plusDropdown_mobile_below400').css({backgroundPosition: newPos.googlePlus + 'px 0'});
			//console.log(newPos.linkedin + ' 0');
		}		
	}
	
	$(document).click(function(e){
		$('.socialIconDropdown').stop().slideUp(ddSlideTime);
	});
	
	$('.socialIconDropdown').click(function(e){
		e.stopPropagation();
	});
	
	//for (var icon in expandableSocialIcons) {	
	for (var i = 0; i < expandableSocialIcons.length; i++) {	
		var $icon = $(expandableSocialIcons[i]['class']);
		$icon.click(function(e){			
			e.preventDefault();
			e.stopPropagation();
		});
		$icon.find('span').click(function(e){			
			e.preventDefault();
			e.stopPropagation();
		});		
			//inc = [$icon, $icon.find('span')];
		/*for (var sel in inc) {
			inc[sel].click(function(e){			
				e.preventDefault();
				e.stopPropagation();
			});			
		}*/		
	}	
	
	/*var extraIcons = {
		pinterest: '<div><a class="pinterest" href="http://www.pinterest.com/cignatogether/" target="_blank"><span></span>Pinterest</a></div>',
		viewAll: '<div><a class="viewAll" href="http://www.cigna.com/aboutcigna/company-profile/social-media" target="_blank"><span></span>View All</a></div>'
	}
	
	for (var icon in extraIcons) {
		$socialIcons.find('.row').append(extraIcons[icon]);
	}*/
	
	repositionDropdowns();
	repositionDropdownArrows();
	
	$socialIcons.mouseleave(function(){		
		$('.socialIconDropdown').stop().slideUp(ddSlideTime);		
	});
	
	$(window).resize(function(){
		function rFunc() {
			winWidth = viewport().width;
			if (winWidth<700) {	
				if (winWidth >= 400) {
					$('.socialIconDivider_400plus').show();								
					$('.socialIconDivider_below400').hide();
				}
				else {
					$('.socialIconDivider_400plus').hide();			
					$('.socialIconDivider_below400').show();					
				}
			//$('.socialIconDropdown_mobile').show();
				//$('.socialIconDropdown').addClass('belowBreakpoint');
			}
			else {
				$('.socialIconDivider').hide();
				//$('.socialIconDropdown').removeClass('belowBreakpoint');
			}
			repositionDropdowns();
			repositionDropdownArrows();
		}	
		rFunc();
		setTimeout(function(){
			rFunc();
		},100);
	}).resize();
	
	//click-tracking code is included in this file for the social media links. Since footer-social-icon-dropdowns.js is getting loaded after the click-tracking.js
	$('div.social-links2 a').mouseover(function (){		
		var qrystring = socialLinksTrackMe($(this));
		var urlval = $(this).attr("href");		
		if(qrystring != ''){	
			if(urlval.indexOf('?') != -1){
				$(this).attr("href", urlval + '&' + qrystring);
			}else{
				$(this).attr("href", urlval + '?' + qrystring);
				}													
		}else{
			$(this).attr("href", urlval);
		}			
	}).mousedown(function(){		 
		var str = $("#QueyString-ct").text().trim();
		if(str.indexOf('Redirect-Link') != -1){			 
		 var lastIndex = str.lastIndexOf(";");
		 var str = str.substring(0, lastIndex);	
		 var webtrendsParams = str.split('=')[1];		
		 dcsMultiTrackForLPLinks(webtrendsParams);				 
		 $('#QueyString-ct').text('');	
		}
	});
	$('div.social-links2 a').mouseout(function (){
	   var url = $(this).attr('href');		
	   if(url.indexOf('&WT.z_nav') != -1){
		$(this).attr('href', url.substring(0,url.lastIndexOf('&')));
	   }else if(url.indexOf('?WT.z_nav') != -1){										
		$(this).attr('href', url.substring(0,url.lastIndexOf('?')));
	   }else{
		$(this).attr('href', url);
	   }		
	});
	function socialLinksTrackMe(currentitem){
		var action = tracking('WT.z_nav');
		action += ';FooterSocialLinks';                	
		if(currentitem.parent().attr('class').indexOf('linkCol') != -1){ 		    
		     action += ';'+currentitem.closest('div.socialIconDropdown').closest('a').contents().filter(function() {return this.nodeType == 3;}).text().trim().replace(/[;]*/gi, '')+';'+currentitem.text().trim().replace(/[;]*/gi, '');	
		}else{
		     action += ';'+currentitem.contents().filter(function() {return this.nodeType == 3;}).text().trim().replace(/[;]*/gi, '');
		}	
		if(currentitem.attr("href").toLowerCase().indexOf('http://www.cigna.com/pages/rssfeeds.jsp') != -1){
			action +=';'+'Redirect-Link';
			$('#QueyString-ct').text(action);
			action = '';
		}
		return action;
	}
	function tracking(param){
	if($('#pagename').text() === "aboutcigna/contact-us/index"){
		/* This is for the main Cigna.com homepage Contact Us landing page */
		return param += '=' + "contact-us";
	}else if($('#pagename').text() === "personal/individual-and-family-plans/dental-plans/index"){
		return param += '=' + "IFP-Dental Plans Selector";
	}else if($('#pagename').text() === "personal/individual-and-family-plans/shop-our-health-insurance-plans"){
		// Newly added this condition for the IFP Shop Health Plans
		return param += '=' + "IFP-State Medical Selector";
	}else if($('#pagename').text() === "personal/individual-and-family-plans/index"){
		return param += '=' + "IFP-home";
	}else if($('#pagename').text().indexOf("/individual-and-family-plans/dental-plans") != -1){
		return param += '=' + "IFP-Dental Plan Details";
	}else if($('#pagename').text() === "personal/individual-and-family-plans/cigna-dental-insurance"){
		return param += '=' + "IFP-Cigna Dental";		
	}else if($('#pagename').text() === "personal/individual-and-family-plans/ifp-knowledge-center/index"){
		return param += '=' + "IFP-Knowledge Center";
	}else if($('#pagename').text() === "personal/individual-and-family-plans/why-choose-cigna-services/index"){
		return param += '=' + "IFP-Why Choose Cigna?";
	}else if($('#pagename').text() === "personal/individual-and-family-plans/explore-plans-in-your-state-2014/comparePlan"){
		return param += '=' + "IFP-Compare Plan";
	}else if($('#pagename').text() === "personal/individual-and-family-plans/explore-plans-in-your-state-2014/productDetail"){
		return param += '=' + "IFP-Medical Plan Details";
	}else if($('#pagename').text().indexOf("personal/individual-and-family-plans/explore-plans-in-your-state-2014/") != -1){
		return param += '=' + "IFP-State Medical Plans Selector";
	}else if($('#pagename').text().indexOf("search") != -1){
		return param += '=' + "Search";
	}else if($('#pagename').text().indexOf("hcpdirectory/") != -1){
		return param += '=' + "HCPDirectory";
	}else if($('#pagename').length != 0){
		/* This is for the main Cigna.com homepage landing pages (other than Contact Us) */
		return param += '=' + $('li.active').attr('class').split(' ')[1].trim().replace(/[;]*/gi, '');
	}else if($('.header-wrapper').attr('data-pagename') === "cigna-healthspring"){
		return param += '=' + 'HS-home';
	}else if($('.header-wrapper').attr('data-pagename') === "medicare-advantage/index"){
		return param += '=' + 'HS-Medicare Advantage';
	}else if($('.header-wrapper').attr('data-pagename') === "medicare-advantage/plan-extras"){
		return param += '=' + 'HS-Medicare Advantage;Plan Extras';  
	}else if($('.header-wrapper').attr('data-pagename') === "medicare-advantage/enrollment"){
		return param += '=' + 'HS-Medicare Advantage;Enrollment';  
	}else if($('.header-wrapper').attr('data-pagename') === "part-d/index"){
		return param += '=' + 'HS-Medicare Part D'; 
	}else if($('.header-wrapper').attr('data-pagename') === "part-d/plan-extras"){
		return param += '=' + 'HS-Medicare Part D;Plan Extras';  
	}else if($('.header-wrapper').attr('data-pagename') === "part-d/enrollment-information"){
		return param += '=' + 'HS-Medicare Part D;Enrollment';  
	}else if($('.header-wrapper').attr('data-pagename') === "understanding-medicare/index"){
		return param += '=' + 'HS-Understanding Medicare';
	}else if($('.header-wrapper').attr('data-pagename') === "resources/index"){
		return param += '=' + 'HS-Customer Tools'; 
	}else if($('.header-wrapper').attr('data-pagename') === "resources/plan-extras"){
		return param += '=' + 'HS-Customer Tools;Plan Extras'; 
	}else if($('.header-wrapper').attr('data-pagename') === "resources/organization-determination"){
		return param += '=' + 'HS-Customer Tools;Enrollment'; 
	}else if($('.header-wrapper').attr('data-pagename') === "contact-us"){
		return param += '=' + 'HS-contact-us';
	}else if($('.header-wrapper').attr('data-pagename') === "about-us"){
		return param += '=' + 'HS-about-Cigna-HealthSpring';
	}else{
		return param += '=' + 'Cigna.com';
	}
     }

});