$("div.skinny-promo-wrapper").click(function (){
	redirection($(this));
});	
$("div.skinny-promo-container").click(function (){
	redirection($(this));
});
function redirection(item){
	if($("#pagename").text() != "personal/index" || $("#pagename").text() != "business/index" || $("#pagename").text() != "healthcare-professionals/index" || $("#pagename").text() != "aboutcigna/index" || $("#pagename").text() != "aboutcigna/contact-us/index"){
		var url = item.attr("data-targeturl");		
		if( url != undefined){
			var substringVal = url.substring(url.length-6, url.length);
			if(substringVal !=".html?"){				
				var target =item.attr("data-targetframe");	
				window.open(url,target);	
			}else{
				var link = url.slice(0, -1);
				var target =item.attr("data-targetframe");	
				window.open(link,target);	
			}
			
		}
	}	
}
! function(a, b, c, d) {
    "use strict";

    function g() {        	
		var b = c(this).data("targeturl"),
            d = c(this).data("targetframe");		
		var lightbox = c(this).attr("href");	
			if(b != undefined){
				var e = b.length;
				var f = b.substring(e - 1, e);	
			}else{
				var e = lightbox.length;
				var f = lightbox.substring(e - 1, e);
			}																											
		
			"?" == f && (b = b.substring(0, e - 1)), "" !== b && "undefined" != typeof b && ("_blank" === d ? a.open(b) : a.location.href = b)
    }

    function h() {
        var d = null != navigator.userAgent.match(/iPad/i);
        d && c(a).width() > 700 ? c("body").addClass("ipad") : c("body").removeClass("ipad");
        try {
            "undefined" != typeof c.fn.dlmenu && c("#dl-menu").dlmenu({
                animationClasses: {
                    classin: "dl-animate-in-2",
                    classout: "dl-animate-out-2"
                }
            }), c(".dl-menu").find(".resizedModalWindow,.modalWindow,.modal-window, .overlay, .overlay-inline").click(function(a) {
                var d = c(this);
                a.preventDefault();
                var e = "overlay";
                d.hasClass("modal-window") || d.hasClass("modalWindow") ? e = "modalWindow" : d.hasClass("overlay-inline") && (e = "overlayInline");
                d.hasClass("resized-modal-window") || d.hasClass("resizedModalWindow") ? e = "resizedModalWindow" : d.hasClass("overlay-inline") && (e = "overlayInline");
                var f = b.config.fancyboxOptions[e];
                f.href = c(this).attr("href"), c.fancybox([f])
            }), c(".navbar-toggle").click(function() {
                c(this).toggleClass("highlight"), c(".sub-top-menu").hide(), c("body").find(".search-sm-xs").hasClass("highlight") && (c(".search-panel").hide(), c(".search-sm-xs").removeClass("highlight")), c(".login-cigna-tab.open").length && c(".login-cigna-tab.open button.close-link").click()
            }), c(".visible-md .country").click(function() {
                c(".visible-md #country-div").slideToggle("fast"), c(".top-right-menus").find("span.caret").toggleClass("up")
            }), c(".visible-xs .country").find("a#current-country, button.close-link").click(function() {
                c(".visible-xs #country-div").slideToggle("fast"), c(".right-nav .country").find("span.caret").toggleClass("up")
            }), c(".login-cigna-tab>a").click(function(a) {
                a.preventDefault(), c(this).parents(".login-cigna-tab").hasClass("open") || (c(".login-cigna-tab .login-dropdown-info").slideToggle("fast"), c(".login-cigna-tab").toggleClass("open"))
            }), c(".login-cigna-tab button.close-link").click(function(a) {
                a.preventDefault(), c(".login-cigna-tab.open .login-dropdown-info").slideToggle("fast"), c(".login-cigna-tab").removeClass("open")
            }), c("#mapGoContact").click(function() {
                var b = document.getElementById("selstate").options[document.getElementById("selstate").selectedIndex].value;
                if ("-1" == b || "" == b || "Select a State" == b) return alert("Please select a state"), !1;
                var c = !1;
                return c ? (a.event.returnValue = !1, a.location = b, !1) : (a.location = b, !1)
            });
            var e;
            c(".navbar-fixed-top").length > 0 && (e = c(".navbar-fixed-top").offset().top);
            var f = function() {
                var b = c(a).scrollTop(),
                    d = Modernizr.touch,
                    f = d ? "absolute" : "fixed",
                    g = null != navigator.userAgent.match(/iPad/i),
                    h = 0,
                    i = b + 92;
                g && c("#need-help").length && (c("#need-help").css("right", "-" + parseInt(c("#need-help").outerWidth() + "px")), h = c("#need-help").hasClass("active") ? 0 : "-" + c("#need-help").outerWidth(), c("#need-help").css({
                    top: i,
                    right: h
                })), b > e && !d ? c(".navbar-fixed-top").css({
                    position: "fixed",
                    top: 0,
                    left: 0
                }) : b > e && d ? c(".navbar-fixed-top").css({
                    position: f,
                    top: b,
                    left: 0
                }) : c(".navbar-fixed-top").css({
                    position: "relative",
                    top: 0
                })
            };
            f(), c(a).on("load resize scroll", function() {
                f()
            }), c(".search-sm-xs").on("click", function() {
                c(this).toggleClass("highlight"), c(".sub-top-menu").hide(), c(".mobile-logo-ifp-container").toggleClass("hide"), c(".search-panel").slideToggle("fast"), c("body").find(".navbar-toggle").hasClass("highlight") && (c("nav").removeClass("in").addClass("collapse"), c(".nav-row").removeClass("in").addClass("collapse"), c(".navbar-toggle").removeClass("highlight"))
            });
            var g = c(".visible-xs.visible-sm .top-left-menus > li");
            g.on("focus, click", function() {
                g.find(".sub-top-menu").slideToggle("fast"), c(document).find(".menu-visible").find("span.caret").toggleClass("up"), c("body").find(".search-sm-xs").hasClass("highlight") && (c(".search-panel").hide(), c(".search-sm-xs").removeClass("highlight")), c("body").find(".navbar-toggle").hasClass("highlight") && (c("nav").removeClass("in").addClass("collapse"), c(".nav-row").removeClass("in").addClass("collapse"), c(".navbar-toggle").removeClass("highlight"))
            }), g.find(".sub-top-menu li>a").click(function() {
                var a = c(this).text();
                a.length > 13 ? (g.find("a").addClass("two-lines"), g.find("a span:first").text(a)) : (g.find("a span:first").text(a), g.find("a").removeClass("two-lines"))
            })
        } catch (h) {}
    }

    function i() {
        var e, d = c(a);
        try {
            var f = function() {
                var a = [c(".carousal_gallery .slides li")];
                c.each(a, function() {
                    c(this).length && (b.utils.isMobile() ? c(this).parents(".related-media").length || J(c(this)) : c(this).parents(".promo-box-wrapper").length || (c(this).parents(".hero-container").length ? J(c(this)) : c(this).css("height", "auto")))
                })
            };
            d.load(function() {
                /* m(), c(".pointed-arrow-placement").css("visibility", "visible"); Modified by A.Fabrizi 2/25/2015 */
                m();
                var d = b.utils.getGridSize(".carousal_gallery");
                f(), c(".carousal_gallery").flexslider({
                    animation: "slide",
                    animationSpeed: 400,
                    animationLoop: !1,
                    itemWidth: 320,
                    itemMargin: 5,
                    minItems: d
                }), c(".flexslider-carousel").flexslider({
                    animation: "slide",
                    animationLoop: !0,
                    animationSpeed: 400,
                    controlNav: !0,
                    directionNav: !0,
                    start: function(a) {
                        e = a, c(this).fadeIn("slow"), c(".mod-hero.mod-h4").css("background", "none")
                    },
                    after: function(a) {
                        a.pause(), a.play()
                    }
                });
                var g = function() {
                        var a = c("body").hasClass("tuo-ie9") || c("body").hasClass("get-started-ie9") || c("body").hasClass("calculator-ie9");
                        a && (c("body").hasClass("calculator-ie9") ? c(".fancybox-wrap,.fancybox-inner").css("max-width", "580px") : c(".fancybox-wrap,.fancybox-inner").css("max-width", "442px"))
                    },
                    h = function() {
                        c("body").removeClass("tuo get-started calculator eloqua-form tuo-ie9 get-started-ie9 calculator-ie9")
                    };
                b.config.fancyboxOptions.modalWindow.afterShow = g, b.config.fancyboxOptions.modalWindow.afterClose = h, b.config.fancyboxOptions.overlay.afterShow = g, b.config.fancyboxOptions.overlay.afterClose = h, c(".overlay").fancybox(b.config.fancyboxOptions.overlay), c(".modal-window, .modalWindow").fancybox(b.config.fancyboxOptions.modalWindow), c(".resized-modal-window, .resizedModalWindow").fancybox(b.config.fancyboxOptions.resizedModalWindow), c(".overlay-inline").fancybox(b.config.fancyboxOptions.overlayInline);
                try {
                    var i = c(parent.document).find("iframe.fancybox-iframe");
                    if (i.length) {
                        var j = i.attr("src") && i.attr("src").indexOf("http") < 0 || 0 === i.attr("src").indexOf(a.location.protocol + "//" + a.location.host);
                        j && i.load(function() {
                            var a = c("html").hasClass("lt-ie10") && !c("html").hasClass("lt-ie9");
                            a && i.contents().find("head link").each(function() {
                                c(this).attr("href") && c(this).attr("href", c(this).attr("href") + "?ver=" + Date.now())
                            }), i.contents().find(".overlay").removeClass("overlay").addClass("someRandomClass"), i.contents().on("click", i.contents().find(".overlay"), function(a) {
                                var b = a.target || a.srcElement;
                                if (c(b).parents(".overlay").length || c(b).hasClass("overlay")) {
                                    var d = c(b).parents(".overlay").length ? c(b).parents(".overlay").attr("href") : c(b).attr("href");
                                    i.attr("src", d)
                                }
                            })
                        })
                    }
                } catch (k) {}
                c(".back-to-top>a").click(function(a) {
                    a.preventDefault();
                    var b = c(this).parents(".component-wrapper").find("#accordion-top");
                    c("html,body").animate({
                        scrollTop: b.offset().top - 100
                    }, 700), c(this).parents("#body-row").length > 0 && c("#body-row").animate({
                        scrollTop: 0
                    }, 700), c(".glossary_flexslider").length && c(".scroll-container").animate({
                        scrollTop: 0
                    }, 700)
                }), c(".scroll-container").data("letterTop", []), c(".scroll-container .glossary-section .heading>a").each(function() {
                    c(".scroll-container").data("letterTop").push({
                        key: "#" + this.id,
                        top: c(this).position().top
                    })
                });
                var n, l = 0;
                c(".glossary-section").each(function() {
                    l += c(this).height()
                }), c("#glossary-nav .nav > li a").click(function(a) {
                    var b = c(this).attr("href");
                    if (!(b.indexOf("healthwellness") >= 0)) {
                        if (a.preventDefault(), c(this).parents("li").hasClass("active")) return !1;
                        c(this).parents("li").removeClass("active");
                        var d = 0;
                        c(".scroll-container").find(b), c(c(".scroll-container").data("letterTop")).each(function() {
                            return this.key === b ? (d = this.top, !1) : void 0
                        }), n = l - d <= c(".scroll-container").height(), n && setTimeout(function() {
                            c("#glossary-nav .nav > li").each(function() {
                                c(this).removeClass("active")
                            })
                        }, 700), setTimeout(function() {
                            c(a.target).parent("li").addClass("active")
                        }, 1200), c(".scroll-container").animate({
                            scrollTop: d
                        }, 700)
                    }
                }), c(".mod-h4").length > 0 ? c(".slider-content,.tagline, .carousal-text").fadeIn("slow") : c(".slider-content,.tagline, .carousal-text").show()
            });
            var h = c(a).width();
            d.resize(function() {
                if (m(), r(), f(), b.utils.getGridSize(), h !== c(a).width()) {
                    if (c("#need-help").length && -1 == navigator.userAgent.toLowerCase().indexOf("msie")) {
                        if (c("#need-help").hasClass("active")) return !1;
                        var e = c("#need-help").outerWidth();
                        c("#need-help").css("right", "-" + e + "px")
                    }
                    i(), h = c(a).width()
                }
            }.debounce(500, !1)), c(".glossary_flexslider").length && (c(".glossary_flexslider").flexslider({
                animation: "slide",
                animationLoop: !1,
                itemWidth: 10,
                itemMargin: 0,
                minItems: 15,
                maxItems: b.utils.isMobile() ? 9 : b.utils.isTablet() ? 17 : 27,
                controlNav: !1,
                directionNav: !1,
                start: function(a) {
                    a.pause(), a.resize()
                }
            }), c(".glossary-prev, .glossary-next").on("click", function() {
                var a = c(this).attr("href");
                return c(".glossary_flexslider").flexslider(a), !1
            })), c(".promo-flex-slider.slider").each(function() {
                c(this).data("html") || c(this).data("html", c(this).parent().html())
            }), c(".four-column .promo-headline").remove();
            var i = function() {
                var f, d = c(".promo-flex-slider");
                d.each(function() {
                    var d = c(this);
                    d.data("originalPromo") || d.data("originalPromo", d.clone(!0)), f = d.find("li").closest(".slides").find("li").length <= 1 ? "fade" : "slide", 1 == d.find(".slides li").length && d.addClass("single-promo");
                    var g = c(a).width(),
                        h = c(a).height();
                    if (b.utils.isMobile()) d.data("flexslider") || d.flexslider({
                        animation: f,
                        animationLoop: !0,
                        minItems: 1,
                        maxItems: 1,
                        controlNav: !0,
                        directionNav: !0,
                        start: function(a) {
                            e = a, d.resize()
                        }
                    });
                    else if (b.utils.isTablet() && h > g) {
                        var i = c(".side-bar").attr("id");
                        "other-row" != i && c(".side-bar .promo-flex-slider .slides").each(function() {
                            var a = c(this).children("li").length;
                            a > 3 && (c(".side-bar .promo-flex-slider").addClass("tablet-slide"), d.flexslider({
                                animation: f,
                                animationLoop: !0,
                                minItems: 3,
                                itemWidth: 225,
                                controlNav: !0,
                                directionNav: !0,
                                start: function() {
                                    d.resize()
                                }
                            }))
                        })
                    } else d.data("flexslider", null), d.replaceWith(d.data("originalPromo"))
                });
                var h = b.utils.isMobile() ? 1 : 4;
                c(".promo-flex-slider.slider").parents(".four-column").length && (c(".promo-flex-slider.slider").each(function() {
                    c(this).parents(".promo-flex-slider.slider").length || (c(this).html(""), c(this).html(c(this).data("html")))
                }), c(".promo-flex-slider.slider .promo-box.cursor-pointer").off("click", g), c(".promo-flex-slider.slider .promo-box.cursor-pointer").on("click", g), c(".promo-flex-slider.slider").parents(".four-column").each(function() {
                    var a = c(this).find(".slides>li").length,
                        d = 4 >= a ? !1 : !0,
                        e = {
                            animation: "slide",
                            animationLoop: !0,
                            animationSpeed: 4000,
                            minItems: h,
                            maxItems: h,
                            itemWidth: 300,
                            controlNav: !0,
                            directionNav: !0,
                            touch: d
                        };
                    b.utils.isMobile() && (e = {
                        animation: "slide",
                        animationLoop: !0,
                        minItems: h,
                        maxItems: h,
                        controlNav: !0,
                        directionNav: !0
                    }), c(this).find(".promo-flex-slider.slider").flexslider(e)
                }))
            };
            i(), c(".scroll-top").on("click", function() {
                var a = c(".tab-content").offset();
                return c("html, body").animate({
                    scrollTop: a.top
                }), !1
            })
        } catch (j) {}
    }

    function m() {
        b.utils.isMobile() && (c(".hero-banner li.menu").off("click", n), c(".hero-banner li.menu").on("click", n))
    }

    function n() {
        var a = c(".hero-banner .sub-menu");
        c(this).find("span.caret").hasClass("dwn") ? (c(this).find("span.caret").removeClass("dwn").addClass("up"), a.css("display", "block")) : c(this).find("span.caret").hasClass("up") && (c(this).find("span.caret").removeClass("up").addClass("dwn"), a.css("display", "none"))
    }

    function o() {
        try {
            c(".sitemap-container .toggle-head>span.caret, .accordion .toggle-head>a").on("click", function(a) {
                a.preventDefault(), c(this).parent().hasClass("active") ? (c(this).parent().removeClass("active"), c(this).children(".caret").removeClass("dwn"), c(this).parent().next().addClass("collapse").removeClass("open")) : (c(this).parent().addClass("active"), c(this).children(".caret").addClass("dwn"), c(this).parent().next().removeClass("collapse").addClass("open"))
            }), c(".exp-all").on("click", function(a) {
                a.preventDefault();
                var b = c(this).parent().next("section");
                c(b).find(".toggle-head").addClass("active"), c(b).find(".caret").addClass("dwn"), c(this).parent().next("section").find(".collapse").removeClass("collapse").addClass("open")
            }), c(".collapse-all").on("click", function(a) {
                a.preventDefault();
                var b = c(this).parent().next("section");
                c(b).find(".toggle-head").removeClass("active"), c(b).find(".caret").removeClass("dwn"), c(this).parent().next("section").find(".open").removeClass("open").addClass("collapse")
            })
        } catch (a) {}
    }

    function p() {
        var d = new Date,
            e = d.getFullYear();
        c("footer #currentYear").length && c("footer #currentYear").html("&#169;&#32;" + e + "&#32;");
        var f = [99];
        if (/iP(hone|od|ad)/.test(navigator.platform)) {
            var g = navigator.appVersion.match(/OS (\d+)_(\d+)_?(\d+)?/);
            f = [parseInt(g[1], 10), parseInt(g[2], 10), parseInt(g[3] || 0, 10)]
        }
        f[0] < 5 && (a.onscroll = function() {
            var b = a.innerHeight + a.pageYOffset - 50;
            c(".oo_feedback_float").css({
                position: "absolute",
                top: b,
                height: "50px"
            })
        }), c(a).load(function() {
            b.utils.isTouchDevice() ? c(".oo_feedback_float, .feedback-show").on("click", function() {
                a.open("https://secure.opinionlab.com/ccc01/o.asp?id=sgHniFRK")
            }) : c(".feedback-show").on("click", function() {
                oo_feedback.show()
            })
        })
    }

    function q(a) {
        var b = c(a + " .result-row li").size(),
            d = 10;
        b > 10 ? c(a + " .result-row li:gt(9)").hide() : 10 > b && (c(a + " .pagination").addClass("hide"), c(a + " .pagination-top").addClass("hide")), c(a + " .result-row li:lt(" + d + ")").show(), c(a + " .pagination a").click(function() {
            d = b >= d + 10 ? d + 10 : b, c(a + " .result-row li:lt(" + d + ")").show(), c(a + " .visible-item").html(d), d == b && c(a + " .pagination").addClass("hide")
        })
    }

    function r() {
        var a = c(".component-wrapper").innerWidth(),
            b = c(".pointed-arrow-placement ul").innerWidth();
        /* c("html").hasClass("lt-ie8") ? c(".pointed-arrow-tab .left-pointed-arrow").css("margin-left", (a - b) / 2) : c(".pointed-arrow-tab ").css("margin-left", (a - b) / 2) Modified by A.Fabrizi 2/25/2015 */
    }

    function s() {
        0 === c("#sidebar-row .iw_component").length && c("#sidebar-row").hide()
    }

    function t() {
        0 === c("#breadcrumb-container .breadcrumb").length && 0 === c("#breadcrumb-container .social-list").length && c("#breadcrumb-container").hide()
    }

    function u() {
        c("#accordion .panel-heading a").on("click", function(a) {
            return c(this).parents(".panel").children(".panel-collapse").hasClass("in") ? (a.stopPropagation(), !1) : void 0
        })
    }

    function v() {
        c.urlParam = function(b) {
            var c = new RegExp(b + "=([^&]*)", "i").exec(a.location.search);
            return c && unescape(c[1]) || ""
        };
        var b = c.urlParam("errorCode");
        "403" == b ? (c(".404-page").addClass("hide"), c(".403-page").removeClass("hide")) : "404" == b && (c(".404-page").removeClass("hide"), c(".403-page").addClass("hide"))
    }

    function w() {
        var b = parent.location.href,
            d = G(b);
        c("#eff_state").val(d), x(d), c("#frm-state").validate({
            rules: {
                eff_state: {
                    valueNotEquals: "default"
                },
                cov_start: {
                    required: !0
                },
                message: "required"
            },
            messages: {
                eff_state: "State is required",
                cov_start: "Year is required",
                message: "Message is required"
            },
            submitHandler: function() {
                var f, b = c("#cov-start-after").is(":checked"),
                    d = c("#cov-start-before").is(":checked"),
                    e = c("#eff_state").val(),
                    g = c("input[name='plan_url']").val();
                b && ("california" == e ? a.open("http://www.cigna.com/sites/colibrium/switch.htm?link_case=quote") : (f = c("input[name=guided_sell_url]").val(), a.parent.location = f + "?state=" + e), parent.$.fancybox.close()), d && (g = g + e + ".html", parent.window.location.href = g)
            }
        })
    }

    function x(a) {
        switch (a) {
            case "south-carolina":
            case "connecticut":
                c("#cov-start-after").attr("disabled", !1), c("#cov-start-after").attr("checked", "checked"), c("#cov-start-before").attr("disabled", "disabled");
                break;
            case "arizona":
            case "colorado":
            case "california":
            case "florida":
            case "georgia":
            case "north-carolina":
            case "tennessee":
            case "texas":
                c("#cov-start-after").attr("disabled", !1), c("#cov-start-after").attr("checked", !1), c("#cov-start-before").attr("disabled", !1), c("#cov-start-before").attr("checked", !1)
        }
    }

    function y() {
        c("#contact-form").validate({
            rules: {
                firstname: "required",
                lastname: "required",
                email: {
                    required: !0,
                    email: !0
                },
                primaryphone: {
                    required: !0,
                    digits: !0
                }
            },
            submitHandler: function() {
                var a = c("input[name='contact_action']").val();
                c("#contact-form").attr("action", a), c("#contact-form")[0].submit()
            }
        })
    }

    function z() {
        c("#contact-form-two-step").validate({
            rules: {
                firstname: "required",
                lastname: "required",
                email: {
                    required: !0,
                    email: !0
                },
                primaryphone: {
                    required: !0,
                    digits: !0
                }
            },
            submitHandler: function() {
                c(".form-control").attr("disabled", !0), c(".step-two").removeClass("hidden"), c(".step-one").addClass("hidden")
            }
        })
    }

    function A() {
        c("#map").usmap({})
    }

    function D() {
        c("#subsidy-form").validate({
            rules: {
                income: {
                    required: !0,
                    digits: !0
                },
                people: {
                    required: !0,
                    digits: !0
                }
            },
            submitHandler: function() {
                var b, a = E();
                85 > a ? (c(".others").hide(), c(".regular,.plans").show(), c(".plans").text("You qualify for Medicaid"), c(".result_plan").text("Close").addClass("close"), c("#result4").removeClass("hidden").addClass("displayBlock"), c(".offMarketPlans").removeClass("hidden"), c(".offMarketDisclaimer").removeClass("hidden")) : a >= 85 && 150 > a ? (b = "100%-150%", c("#result1").removeClass("hidden").addClass("displayBlock"), c("#result1").find(".planC").show(), c(".offMarketPlans").addClass("hidden"), c(".offMarketDisclaimer").addClass("hidden")) : a >= 150 && 200 > a ? (b = "150%-200%", c("#result1").removeClass("hidden").addClass("displayBlock"), c("#result1").find(".planB").show(), c(".offMarketPlans").addClass("hidden"), c(".offMarketDisclaimer").addClass("hidden")) : a >= 200 && 250 > a ? (b = "200%-250%", c("#result1").removeClass("hidden").addClass("displayBlock"), c("#result1").find(".planA").show(), c(".offMarketPlans").addClass("hidden"), c(".offMarketDisclaimer").addClass("hidden")) : a >= 250 && 400 > a ? (b = "Tax Credit Subsidy", c("#result2").removeClass("hidden").addClass("displayBlock"), c(".offMarketPlans").addClass("hidden"), c(".offMarketDisclaimer").addClass("hidden")) : a >= 400 && (b = "regular", c("#result3").removeClass("hidden").addClass("displayBlock"), c(".offMarketPlans").removeClass("hidden"), c(".offMarketDisclaimer").removeClass("hidden")), c("#subsidy-form, .modal-header").hide(), c(".result").text(b), B = a, c(".result_plan_button").on("click", function() {
                    F()
                })
            }
        })
    }

    function E() {
        var d, e, f, g, a = parseInt(c("#income").val()),
            b = parseInt(c("#people").val());
        if (d = e = f = 0, b > 0) {
            switch (b) {
                case 1:
                    g = 11490;
                    break;
                case 2:
                    g = 15510;
                    break;
                case 3:
                    g = 19530;
                    break;
                case 4:
                    g = 23550;
                    break;
                case 5:
                    g = 27570;
                    break;
                case 6:
                    g = 31590;
                    break;
                case 7:
                    g = 35610;
                    break;
                case 8:
                    g = 39630;
                    break;
                default:
                    f = 4020 * (b - 8), g = 39630 + f
            }
            return e = Math.round(100 * (a / g))
        }
    }

    function F() {
        var e, f, b = parent.location.href,
            g = decodeURIComponent(H().state),
            h = c(".county", a.parent.document).val(),
            i = parent.location.href,
            j = G(i);
        switch (j) {
            case "texas":
                B >= 85 ? (e = b.split("?")[0], h != d && (f = e + "?fpl=" + B + "&city=" + h)) : (e = b.split("?")[0], h != d && (f = e + "?fpl=" + "&city=" + h));
                break;
            case "arizona":
            case "california":
            case "colorado":
            case "connecticut":
            case "florida":
            case "georgia":
			case "missouri":
            case "north-carolina":
            case "south-carolina":
            case "tennessee":
                B >= 85 ? (e = b.split("?")[0], f = e + "?fpl=" + B) : (e = b.split("?")[0], f = e + "?fpl=");
                break;
            default:
                var k = b.lastIndexOf("-plan-"),
                    l = b.indexOf("&"),
                    m = "";
                if (-1 != k && -1 != l && (m = b.substring(k + 6, l), -1 != m.indexOf("texas") && (m = m.substring(m.lastIndexOf("-") + 1))), "undefined" == g && 0 == m.length) f = c("input[name=guidedLanding]").val();
                else {
                    var n = c("input[name=listLanding]").val(),
                        h = b.substring(b.lastIndexOf("=") + 1, b.lastIndexOf("/"));
                    f = h != d && "texas" == m ? n + m + ".page?fpl=" + B + "&city=" + h : n + m + ".page?fpl=" + B
                }
        }
        parent.window.location.href = f
    }

    function G() {
        var b = parent.location.href,
            c = b.lastIndexOf("/") + 1,
            d = b.substr(c),
            e = d.split(".")[0];
        return e
    }

    function H() {
        for (var c, b = [], d = a.location.href.slice(a.location.href.indexOf("?") + 1).split("&"), e = 0; e < d.length; e++) c = d[e].split("="), b.push(c[0]), b[c[0]] = c[1];
        return b
    }

    function J(a) {
        var b = a,
            d = 0,
            e = 0;
        c(b).each(function(a, b) {
            e = parseInt(c(b).css("height")), e > d && (d = e)
        }), a.css("height", d)
    }
    var e = c("html").hasClass("lt-ie10") && !c("html").hasClass("lt-ie9");
    c(function() {
        h(), i(), o(), p(), t(), b.utils.isTouchDevice() && c("#share-this .share-link").click(function() {
            c(this).parents("#share-this").find(".share-box").toggle()
        }), c("#search-result").length > 0 && (q("#search-result #tab1"), q("#search-result #tab2")), c(".pointed-arrow-tab").length > 0 && r(), c("#sidebar-row").length > 0 && s(), c("#accordion").length > 0 && u(), c("#frm-state").length > 0 && w(), c(".frm-subsidy").length > 0 && D(), c(".frm-contact-two-step").length > 0 && z(), c(".frm-contact").length > 0 && y(), c("#map").length > 0 && (A(), a.parent.document.body.className = ""), c("#mapDetails").length > 0 && (a.parent.document.body.className = "cigna-map", e && (a.parent.document.body.className = "cigna-map-ie9")), c(".scrollbardiv").length && c(".scrollbardiv").mCustomScrollbar({
            autoHideScrollbar: !1,
            scrollInertia: 150,
            advanced: {
                updateOnContentResize: !0
            }
        }), v();
        var d = {
                html: !0,
                container: "body"
            },
            f = c(".inlineGlossary, .tooltip-trigger");
        b.utils.isTouchDevice() && (d.trigger = "click", f.on("show.bs.tooltip", function() {
            f.tooltip("hide")
        })), f.tooltip(d), c(".authors-name .author-info").fancybox({
            modal: !0
        }), c(document).on("click", 'a[href*="subsidy-calculator"]', function() {
            c("body").addClass("calculator" + (e ? "-ie9" : ""))
		}), c(document).on("click", 'div[href*="subsidy-calculator"]', function() {
            c("body").addClass("calculator" + (e ? "-ie9" : ""))
        }), c(document).on("click", "a.cigna-eloqua", function() {
            c("body").addClass("cigna-eloqua-form" + (e ? "-ie9" : ""))
        }), c(document).on("click", 'a[href*="get-started"]', function() {
            c("body").addClass("get-started" + (e ? "-ie9" : ""))
        }), c(document).on("click", 'a[href*="ifp-tuo"]', function() {
            c("body").addClass("tuo" + (e ? "-ie9" : ""))
        }), c(document).on("click", 'a[href*="david-cordani-corporate-responsibility"]', function() {
            c("body").addClass("ceo-video" + (e ? "-ie9" : ""))
        }), c(document).on("click", 'a[href*="world-hepatitis-day"]', function() {
            c("body").addClass("promo-video" + (e ? "-ie9" : ""))
		}), c(document).on("click", 'div[href*="world-hepatitis-day"]', function() {
            c("body").addClass("promo-video" + (e ? "-ie9" : ""))			
        }), c(document).on("click", 'a[href*="http://www.cigna.com/personal/individual-and-family-plans/open-enrollment-promo.html"]', function() {
            c("body").addClass("eloqua-form")
        }), c(document).on("click", 'a[href*="Regionalcontactus"]', function() {
            c("body").addClass("eloqua-form")
        }), c(document).on("click", 'a[href*="Governmentcontactus"]', function() {
            c("body").addClass("eloqua-form")
        }), c(document).on("click", 'a[href*="eloqua-form1-dental"]', function() {
            c("body").addClass("eloqua-form")
        }), c(document).on("click", 'a[href*="business-segments/small-employers/contact-us"]', function() {
            c("body").addClass("eloqua-form")
        }), c(document).on("click", 'div[href*="Regionalcontactus"]', function() {
            c("body").addClass("eloqua-form")
        }), c(document).on("click", 'div[href*="Governmentcontactus"]', function() {
            c("body").addClass("eloqua-form")
        }), c(document).on("click", 'div[href*="eloqua-form1-dental"]', function() {
            c("body").addClass("eloqua-form")
        }), c(document).on("click", 'div[href*="business-segments/small-employers/contact-us"]', function() {
            c("body").addClass("eloqua-form")
		}), c(document).on("click", 'div[href*="business-segments/small-employers/exchange-form"]', function() {
            c("body").addClass("eloqua-form small-employers")			
		}), c(document).on("click", 'a[href*="business-segments/small-employers/exchange-form"]', function() {
            c("body").addClass("eloqua-form small-employers")	
        }), c.validator.addMethod("valueNotEquals", function(a, b, c) {
            return c != a
        }, "This is a required field"), c("#eff_state").change(function() {
            var a;
            a = c("#eff_state").val(), "default" != a && x(a), a = a
        })
    });
    var f = c("#need-help");
    f && (c("#need-help").css("right", "-" + parseInt(c("#need-help").outerWidth() + "px")), f.find(".need-help-tab, .close-flyout, .btn-close").click(function(a) {
        a.preventDefault ? a.preventDefault() : a.returnValue = !1, f.hasClass("active") ? f.animate({
            right: "-" + parseInt(c("#need-help").outerWidth() + "px")
        }, 200).removeClass("active") : f.animate({
            right: "0"
        }, 200).addClass("active")
    })), c(".promo-box.cursor-pointer, .widget.cursor-pointer, .promo-area.cursor-pointer").off("click", g), c(".promo-box.cursor-pointer, .widget.cursor-pointer, .promo-area.cursor-pointer").on("click", g);
    var j = "ontouchstart" in a,
        k = c(".visible-md"),
        l = k.find(".topnav");
    j ? (c(document).on("touchend click", function(a) {
        var b = c(a.target);
        b.hasClass("topnav") || b.parents().hasClass("topnav") || c(".drop-down").is(":visible") && c(".drop-down").hide()
    }), l.find("li a.mainNav").on("click", function(a) {
        if (c(this).parents("li").find(".drop-down").length > 0) {
            var b = c(this);
            b.siblings("div.drop-down").is(":visible") ? c(".drop-down").hide() : (c(".drop-down").hide(), b.siblings("div.drop-down").show(), a.preventDefault())
        }
    })) : l.find("li").hover(function() {
        c(this).find(".drop-down").css({
            display: "block"
        })
    }, function() {
        c(this).find(".drop-down").css({
            display: "none"
        })
    }), a.onload = function() {
        var a = location.hash;
        if (a) {
            var b = a.split("#")[1],
                d = c("a[name*='" + b + "']");
            document.getElementsByName(b).length > 0 && (c("html,body").animate({
                scrollTop: d.offset().top - 100
            }, 1e3), c(d).trigger("click"))
        }
    }, c(".personal-hero").length && c(".hero-banner").parents(".component-wrapper").addClass("align-hero-banner"), c(".btn-edit").click(function() {
        c(".form-control").removeAttr("disabled"), c(".step-one").removeClass("hidden"), c(".step-two").addClass("hidden")
    }), c(".btn-continue").click(function() {
        var a = c("input[name='contact_action']").val();
        c(".form-control").removeAttr("disabled"), c("#contact-form-two-step").attr("action", a), c("#contact-form-two-step")[0].submit()
    });
    var B = 0;
    c(document).on("click", ".toggle-income-chart", function() {
        var a = c(this);
        c(".income-chart").slideToggle("slow", function() {
            c(this).is(":visible") ? (a.text("Hide Income Chart"), c(".caret").addClass("dwn")) : (a.text("View Income Chart"), c(".caret").removeClass("dwn"))
        })
    });
    var I = a.location.pathname;
    if (I.indexOf("arizona") > -1 && c(".ifp-info").addClass("arizona"), c(".plans-type ul li a").click(function() {
        var a = c(this).text();
        switch (a) {
            case "Individual":
                c("#compare-plan-outer").find(".individual").removeClass("hidden"), c("#compare-plan-outer").find(".family").addClass("hidden"), c(this).parents().find(".selected").removeClass("selected"), c(this).parent().addClass("selected");
                break;
            case "Family":
                c("#compare-plan-outer").find(".individual").addClass("hidden"), c("#compare-plan-outer").find(".family").removeClass("hidden"), c(this).parents().find(".selected").removeClass("selected"), c(this).parent().addClass("selected")
        }
    }), c(".detail-page").click(function() {
        var b, d, e = decodeURIComponent(H().fpl),
            f = decodeURIComponent(H().city);
        d = c("input[name=detail-url-1]").val(), b = c(this).parent().find("input.detailval").val(), d = "undefined" != e ? d + "plan=" + b + "&fpl=" + e : d + "plan=" + b + "&fpl=", "undefined" != f && "" != f && (d = d + "&city=" + f), a.location = d
    }), c("#guided-summary")[0]) {
        var K = a.location.href,
            L = K.length,
            M = L - 1,
            N = K.substr(M, L);
        switch (N) {
            case "1":
                c("#promo1").removeClass("hidden");
                break;
            case "2":
                c("#promo2").removeClass("hidden");
                break;
            case "3":
                c("#promo3").removeClass("hidden");
                break;
            case "4":
                c("#promo4").removeClass("hidden");
                break;
            case "5":
            case "6":
            default:
                c("#promo5").removeClass("hidden")
        }
    }
}(window, Cigna, jQuery);