$(document).ready(setPromosTracking);
function setPromosTracking(){
if(!window.initTracking){
$("div.promo-area").mouseover(function (){		
			var qrystring = promoareaTrackMe1($(this));
			var urlvalue = $(this).attr("data-targeturl");
			var hrefvalue = $(this).attr("href");			
			if(urlvalue != undefined){					
				var lastString = urlvalue.substring(urlvalue.length-6, urlvalue.length);
				var urlval = $(this).attr("data-targeturl");
				var urltarget = $(this).attr("data-targetframe");
				if(lastString != ".html?"){					
					$(this).attr("data-targeturl", urlval);
				}else{
					var link = urlval.slice(0, -1);
					$(this).attr("data-targeturl", link);
				}					
			}else{			
				$(this).attr("href", hrefvalue);	
				$('#QueyString-ct').text(qrystring);	
			}
		$('#QueyString-ct').text(qrystring);		
	}).mousedown(function(){		
		  var webtrendsParams = $("#QueyString-ct").text().split('=')[1].replace('%3F','?');
		  dcsMultiTrackForLPLinks(webtrendsParams);		
	});
    $("div.promo-area").mouseout(function (){		
		$('#QueyString-ct').text('');		
    });
	$("div.skinny-promo-wrapper,div.skinny-promo-container").mouseover(function (){		        
		var urlValue = $(this).attr("data-targeturl");
		var hrefvalue = $(this).attr("href");
		if(urlValue != undefined || hrefvalue != undefined){
			var qrystring = promoctaclickableWrapperTrackMe1($(this));
		}		
		if(urlValue != undefined){	
			var urltarget = $(this).attr("data-targetframe");
			var lastString = urlValue.substring(urlValue.length-6, urlValue.length);
			var fullurl ="";
			if(lastString != ".html?"){				
				if(qrystring != ''){										
					if(urlValue.indexOf('?') != -1){
						$(this).attr("data-targeturl", urlValue + '&' + qrystring);
						fullurl = urlValue +'&'+ qrystring;
					}else{
						$(this).attr("data-targeturl", urlValue + '?' + qrystring);
						fullurl = urlValue +'?'+ qrystring;
					}													
				}else{					
					$(this).attr("data-targeturl", urlValue);
					fullurl = urlValue;	
				}
			}else{				
				if(qrystring != ''){						
					$(this).attr("data-targeturl", urlValue + qrystring);	
					fullurl = urlValue + qrystring;	
				}else{
					$(this).attr("data-targeturl", urlValue);
					fullurl = urlValue;
				}					
			}
			if(qrystring != ''){
				$('#QueyString-ct').text(fullurl);	
			}			
		}else if(hrefvalue != undefined){	
			var str = $("#QueyString-ct").text().trim();
			var lastIndex = str.lastIndexOf(";");
			var str = str.substring(0, lastIndex);
			$('#QueyString-ct').text('');	
			if(qrystring != ''){	
				if(hrefvalue.indexOf('?') != -1){
					$(this).attr("href", hrefvalue + '&' + str);
				}else{
					$(this).attr("href", hrefvalue + '?' + str);
				}													
			}else{
				$(this).attr("href", hrefvalue);
			}
		}else{		
			$("div.skinny-promo-wrapper a, div.skinny-promo-container a").one("mousedown",function(){
				var querystring = promoctaWrapperTrackMe1($(this));				
				var urlval = $(this).attr("href");
				if(querystring != ''){	
					if(urlval.indexOf('?') != -1){
						$(this).attr("href", urlval + '&' + querystring);
					}else{
						$(this).attr("href", urlval + '?' + querystring);
					}													
				}else{
						$(this).attr("href", urlval);
				}	
			});
		}	
    }).mousedown(function(){
		var str = $("#QueyString-ct").text().trim();		
		 if(str.indexOf('Redirect-Link') != -1){			 					 
			 var lastIndex = str.lastIndexOf(";");
			 var str = str.substring(0, lastIndex);	
			 var webtrendsParams = str.split('=')[1];			 
			 dcsMultiTrackForLPLinks(webtrendsParams);
			 var urlValue = $(this).attr("data-targeturl");
			 var urltarget = $(this).attr("data-targetframe");			 
			 window.open(urlValue, urltarget);	
			 $('#QueyString-ct').text('');	
		 }else if(str !=''){								 
			var fullurl = $("#QueyString-ct").text();			
			var urltarget = $(this).attr("data-targetframe");				
			window.open(fullurl, urltarget);
			$('#QueyString-ct').text('');			
		 }		
	});
	$("div.skinny-promo-wrapper,div.skinny-promo-container").mouseout(function (){
		var urlvalue = $(this).attr("data-targeturl");
		var hrefvalue = $(this).attr("href");
		$('#QueyString-ct').text('');	
		if(urlvalue != undefined){
		   if($(this).attr('data-targeturl').indexOf('&WT.z_nav') != -1){
				$(this).attr('data-targeturl', urlvalue.substring(0,urlvalue.lastIndexOf('&')));
		   }else if(($(this).attr('data-targeturl').indexOf('?WT.z_nav') != -1)){
				$(this).attr('data-targeturl', urlvalue.substring(0,urlvalue.lastIndexOf('?')));
		   }else{
				$(this).attr('data-targeturl', urlvalue);
		   }									
		}else if(hrefvalue != undefined){
			   var url = $(this).attr('href');		
			   if(url.indexOf('&WT.z_nav') != -1){
					$(this).attr('href', url.substring(0,url.lastIndexOf('&')));
			   }else if(url.indexOf('?WT.z_nav') != -1){										
					$(this).attr('href', url.substring(0,url.lastIndexOf('?')));
			   }else{
					$(this).attr('href', url);
			   }	
		}else{
			$("div.skinny-promo-wrapper a, div.skinny-promo-container a").one("mousedown",function(){						
			   var url = $(this).attr('href');		
			   if(url.indexOf('&WT.z_nav') != -1){
					$(this).attr('href', url.substring(0,url.lastIndexOf('&')));
			   }else if(url.indexOf('?WT.z_nav') != -1){										
					$(this).attr('href', url.substring(0,url.lastIndexOf('?')));
			   }else{
					$(this).attr('href', url);
			   }	
			});
		}
    });
	$("div.l3-section-links .news-row a").mouseover(function () {		
		var qrystring = l3sectionLinks($(this));
		var urlval = $(this).attr("href");
		if(qrystring != ''){	
			if(urlval.indexOf('?') != -1){
				$(this).attr("href", urlval + '&' + qrystring);
			}else{
				$(this).attr("href", urlval + '?' + qrystring);
			}													
		}else{
			$(this).attr("href", urlval);
		}									
	});
	$("div.l3-section-links .news-row a").mouseout(function () {
	   var url = $(this).attr('href');		
	   if(url.indexOf('&WT.z_nav') != -1){
			$(this).attr('href', url.substring(0,url.lastIndexOf('&')));
	   }else if(url.indexOf('?WT.z_nav') != -1){										
			$(this).attr('href', url.substring(0,url.lastIndexOf('?')));
	   }else{
			$(this).attr('href', url);
	   }	
	});		
	$("div.article-authors .main-promotion a").mouseover(function (){		
		var qrystring = articleLinks($(this));
		var urlval = $(this).attr("href");
		if(qrystring != ''){	
			if(urlval.indexOf('?') != -1){
				$(this).attr("href", urlval + '&' + qrystring);
			}else{
				$(this).attr("href", urlval + '?' + qrystring);
			}													
		}else{
			$(this).attr("href", urlval);
		}									
	});
	$("div.article-authors .main-promotion a").mouseout(function () {
	   var url = $(this).attr('href');		
	   if(url.indexOf('&WT.z_nav') != -1){
			$(this).attr('href', url.substring(0,url.lastIndexOf('&')));
	   }else if(url.indexOf('?WT.z_nav') != -1){										
			$(this).attr('href', url.substring(0,url.lastIndexOf('?')));
	   }else{
			$(this).attr('href', url);
	   }	
	});
	$("div.authored-promo-component a").mouseover(function () {		
		var qrystring = authoredPromoTrackMe($(this));
		var urlval = $(this).attr("href");
		if(qrystring != ''){	
			if(urlval.indexOf('?') != -1){
				$(this).attr("href", urlval + '&' + qrystring);
			}else{
				$(this).attr("href", urlval + '?' + qrystring);
			}													
		}else{
			$(this).attr("href", urlval);
		}									
	});
	$("div.authored-promo-component a").mouseout(function () {
	   var url = $(this).attr('href');		
	   if(url.indexOf('&WT.z_nav') != -1){
			$(this).attr('href', url.substring(0,url.lastIndexOf('&')));
	   }else if(url.indexOf('?WT.z_nav') != -1){										
			$(this).attr('href', url.substring(0,url.lastIndexOf('?')));
	   }else{
			$(this).attr('href', url);
	   }	
	});	
	$("div.button-cta.ctapromo-ct a").mouseover(function () {
        var qrystring = promoctaBannerTrackMe1($(this));
		var urlval = $(this).attr("href");
		if(qrystring != ''){	
			if(urlval.indexOf('?') != -1){
				$(this).attr("href", urlval + '&' + qrystring);
			}else{
				$(this).attr("href", urlval + '?' + qrystring);
			}													
		}else{
			$(this).attr("href", urlval);
		}										
    });
    $("div.button-cta.ctapromo-ct a").mouseout(function () { 		
	   var url = $(this).attr('href');		
	   if(url.indexOf('&WT.z_nav') != -1){
			$(this).attr('href', url.substring(0,url.lastIndexOf('&')));
	   }else if(url.indexOf('?WT.z_nav') != -1){										
			$(this).attr('href', url.substring(0,url.lastIndexOf('?')));
	   }else{
			$(this).attr('href', url);
	   }		
    });
	$("div.mob-button.ctapromo-ct a").mouseover(function () {		
        var qrystring = promomobilectaBannerTrackMe1($(this));
		var urlval = $(this).attr("href");
		if(qrystring != ''){	
			if(urlval.indexOf('?') != -1){
				$(this).attr("href", urlval + '&' + qrystring);
			}else{
				$(this).attr("href", urlval + '?' + qrystring);
			}													
		}else{
			$(this).attr("href", urlval);
		}									
    });
    $("div.mob-button.ctapromo-ct a").mouseout(function () {
       var url = $(this).attr('href');		
	   if(url.indexOf('&WT.z_nav') != -1){
			$(this).attr('href', url.substring(0,url.lastIndexOf('&')));
	   }else if(url.indexOf('?WT.z_nav') != -1){										
			$(this).attr('href', url.substring(0,url.lastIndexOf('?')));
	   }else{
			$(this).attr('href', url);
	   }	
    });
	$("div.button-cta.heropromo-ct a").mouseover(function () {
        var qrystring = promoheroBannerTrackMe1($(this));
		var urlval = $(this).attr("href"); 
		if(qrystring != ''){	
			if(urlval.indexOf('?') != -1){
				$(this).attr("href", urlval + '&' + qrystring);
			}else{
				$(this).attr("href", urlval + '?' + qrystring);
			}													
		}else{
			$(this).attr("href", urlval);
		}										
    });
    $("div.button-cta.heropromo-ct a").mouseout(function () { 		
	   var url = $(this).attr('href');		
	   if(url.indexOf('&WT.z_nav') != -1){
			$(this).attr('href', url.substring(0,url.lastIndexOf('&')));
	   }else if(url.indexOf('?WT.z_nav') != -1){										
			$(this).attr('href', url.substring(0,url.lastIndexOf('?')));
	   }else{
			$(this).attr('href', url);
	   }		
    });
	$("div.mob-button.heropromo-ct a").mouseover(function () {		
        var qrystring = promoheromobileBannerTrackMe1($(this));
		var urlval = $(this).attr("href");
		if(qrystring != ''){	
			if(urlval.indexOf('?') != -1){
				$(this).attr("href", urlval + '&' + qrystring);
			}else{
				$(this).attr("href", urlval + '?' + qrystring);
			}													
		}else{
			$(this).attr("href", urlval);
		}									
    });
    $("div.mob-button.heropromo-ct a").mouseout(function () {
       var url = $(this).attr('href');		
	   if(url.indexOf('&WT.z_nav') != -1){
			$(this).attr('href', url.substring(0,url.lastIndexOf('&')));
	   }else if(url.indexOf('?WT.z_nav') != -1){										
			$(this).attr('href', url.substring(0,url.lastIndexOf('?')));
	   }else{
			$(this).attr('href', url);
	   }	
    });	
	$("div.hero-img-info a").mouseover(function () {		
        var qrystring = heroBannerTrackMe1($(this));
		var urlval = $(this).attr("href");
		if(qrystring != ''){	
			if(urlval.indexOf('?') != -1){
				$(this).attr("href", urlval + '&' + qrystring);
			}else{
				$(this).attr("href", urlval + '?' + qrystring);
			}													
		}else{
			$(this).attr("href", urlval);
		}										
    });	
    $("div.hero-img-info a").mouseout(function () {
       var url = $(this).attr('href');		
	   if(url.indexOf('&WT.z_nav') != -1){
			$(this).attr('href', url.substring(0,url.lastIndexOf('&')));
	   }else if(url.indexOf('?WT.z_nav') != -1){										
			$(this).attr('href', url.substring(0,url.lastIndexOf('?')));
	   }else{
			$(this).attr('href', url);
	   }		
    }); 	
	$("ul.Iwantto-ct li a,ul.ifpbanner-ct li a").mouseover(function (){
		 var qrystring = IWanttoBannerTrackMe($(this));
		var urlval = $(this).attr("href");
		if(qrystring != ''){	
			if(urlval.indexOf('?') != -1){
				$(this).attr("href", urlval + '&' + qrystring);
			}else{
				$(this).attr("href", urlval + '?' + qrystring);
			}													
		}else{
			$(this).attr("href", urlval);
		}
	}).mousedown(function(){			
		 var str = $("#QueyString-ct").text().trim();		
		 if(str.indexOf('Redirect-Link') != -1){			 					 
			 var lastIndex = str.lastIndexOf(";");
			 var str = str.substring(0, lastIndex);	
			 var webtrendsParams = str.split('=')[1];			 
			 dcsMultiTrackForLPLinks(webtrendsParams);			 	
			 $('#QueyString-ct').text('');	
		 }			
	});	
	 $("ul.Iwantto-ct li a,ul.ifpbanner-ct li a").mouseout(function (){
		   $('#QueyString-ct').text('');
		   var url = $(this).attr('href');		
		   if(url.indexOf('&WT.z_nav') != -1){
				$(this).attr('href', url.substring(0,url.lastIndexOf('&')));
		   }else if(url.indexOf('?WT.z_nav') != -1){										
				$(this).attr('href', url.substring(0,url.lastIndexOf('?')));
		   }else{
				$(this).attr('href', url);
		   }
	 });
	$("div.article-sec a").mouseover(function (){
		var qrystring = articlePromoTrackMe($(this));
		var urlval = $(this).attr("href");
		if(qrystring != ''){	
			if(urlval.indexOf('?') != -1){
				$(this).attr("href", urlval + '&' + qrystring);
			}else{
				$(this).attr("href", urlval + '?' + qrystring);
			}													
		}else{
			$(this).attr("href", urlval);
		}
	});
	$("div.article-sec a").mouseout(function (){
		   $('#QueyString-ct').text('');
		   var url = $(this).attr('href');		
		   if(url.indexOf('&WT.z_nav') != -1){
				$(this).attr('href', url.substring(0,url.lastIndexOf('&')));
		   }else if(url.indexOf('?WT.z_nav') != -1){										
				$(this).attr('href', url.substring(0,url.lastIndexOf('?')));
		   }else{
				$(this).attr('href', url);
		   }
	 });
	$("dd a").mouseover(function (){
		if($(this).attr('href') === undefined){
			return false;
		} 
		var qrystring = accordion_links1($(this));		
		if(qrystring != ''){				
			if(this.href.indexOf('?') != -1){
				$(this).attr("href", this.href + '&' + qrystring);
			}else{
				$(this).attr("href", this.href + '?' + qrystring);
			}													
		}else{
			$(this).attr("href", this.href);
		}	
	}).mousedown(function(){		 
		 var str = $("#QueyString-ct").text().trim();
		 if(str.indexOf('Redirect-Link') != -1){			 	
			 var lastIndex = str.lastIndexOf(";");
			 var str = str.substring(0, lastIndex);
			 var webtrendsParams = str.split('=')[1];
			 dcsMultiTrackForLPLinks(webtrendsParams);
			 $('#QueyString-ct').text('');
		 }
	});
	$("dd a").mouseout(function (){		
	   if($(this).attr('href') === undefined){
			return false;
	   }	
	   var url = $(this).attr("href");		
	   if(url.indexOf('&WT.z_nav') != -1){
			$(this).attr('href', url.substring(0,url.lastIndexOf('&')));
	   }else if(url.indexOf('?WT.z_nav') != -1){
			$(this).attr('href', url.substring(0,url.lastIndexOf('?')));
	   }else{			
			$(this).attr('href', url);
	   }	
	   $('#QueyString-ct').text('');	
	});
	$("dt.toggle-head a").mouseover(function (){		
		var qrystring = accordion_mainlinks1($(this));
		var urlval = $(this).attr("href");		
		$(this).attr("href", urlval);											
	    $('#QueyString-ct').text(qrystring);		
	}).mousedown(function(){
		  var webtrendsParams = $("#QueyString-ct").text().split('=')[1];					  
		  dcsMultiTrackForLPLinks(webtrendsParams);				  
	});
	$("dt.toggle-head a").mouseout(function (){				
		var urlval = $(this).attr("href");		
		$(this).attr("href", urlval);												    	
	    $('#QueyString-ct').text('');	
	});	
	$("div.side-widget,div.rectangular-box").mouseover(function (){	
			var qrystring = ifpRailPromoTrackMe1($(this));       
			var urlValue = $(this).attr("data-targeturl");
			var hrefvalue = $(this).attr("href");
			if(urlValue != undefined){
				var lastString = urlValue.substring(urlValue.length-6, urlValue.length);
				if(lastString != ".html?"){					
					$(this).attr("data-targeturl", urlValue);
				}else{
					var link = urlValue.slice(0, -1);
					$(this).attr("data-targeturl", link);
				}
				$('#QueyString-ct').text(qrystring);		
			}else if(hrefvalue != undefined){	
				$(this).attr("href", hrefvalue);
				$('#QueyString-ct').text(qrystring);
			}else{							
				$("div.side-widget a,div.rectangular-box a").one("mousedown",function(){						
					var querystring = ifpRailPromoTrackMeLinks1($(this));					
					var urlval = $(this).attr("href");
					if(querystring != ''){	
						if(urlval.indexOf('?') != -1){
							$(this).attr("href", urlval + '&' + querystring);
						}else{
							$(this).attr("href", urlval + '?' + querystring);
						}													
					}else{
							$(this).attr("href", urlval);
					}	
				});			
			}					
    }).mousedown(function(){
		 var str = $("#QueyString-ct").text().trim();			
		 if(str.indexOf('Redirect-Link') != -1){
			 var lastIndex = str.lastIndexOf(";");
			 var str = str.substring(0, lastIndex);	
			 var webtrendsParams = str.split('=')[1].replace('%3F','?');			
			 dcsMultiTrackForLPLinks(webtrendsParams);	
			 $('#QueyString-ct').text('');	
		 }else if(str !=''){								 
			 var webtrendsParams = str.split('=')[1].replace('%3F','?');			 			 
			 dcsMultiTrackForLPLinks(webtrendsParams);
			$('#QueyString-ct').text('');			
		 }
	});
	$("div.side-widget,div.rectangular-box").mouseout(function (){
		$('#QueyString-ct').text('');					
		$("div.side-widget a,div.rectangular-box a").one("mousedown",function(){							   
		   var url = $(this).attr('href');		
		   if(url.indexOf('&WT.z_nav') != -1){
				$(this).attr('href', url.substring(0,url.lastIndexOf('&')));
		   }else if(url.indexOf('?WT.z_nav') != -1){										
				$(this).attr('href', url.substring(0,url.lastIndexOf('?')));
		   }else{
				$(this).attr('href', url);
		   }	
		});		
	});	
	$("div.linked-list-cotainer a").mouseover(function (){
		var qrystring = linkListContainer($(this));
		var urlval = $(this).attr("href");
		if(qrystring != ''){	
			if(urlval.indexOf('?') != -1){
				$(this).attr("href", urlval + '&' + qrystring);
			}else{
				$(this).attr("href", urlval + '?' + qrystring);
			}													
		}else{
			$(this).attr("href", urlval);
		}
	});
	$("div.linked-list-cotainer a").mouseout(function (){
		   $('#QueyString-ct').text('');
		   var url = $(this).attr('href');		
		   if(url.indexOf('&WT.z_nav') != -1){
				$(this).attr('href', url.substring(0,url.lastIndexOf('&')));
		   }else if(url.indexOf('?WT.z_nav') != -1){										
				$(this).attr('href', url.substring(0,url.lastIndexOf('?')));
		   }else{
				$(this).attr('href', url);
		   }
	 });		
}
}
$(window).load(setPromoboxTracking);
function setPromoboxTracking(){
if(!window.initTracking){
	$("div.promo-box").mouseover(function () {			
        var qrystring = promoboxTrackMe1($(this));
		var urlvalue = $(this).attr("data-targeturl");
		var hrefvalue = $(this).attr("href");			
			if(urlvalue != undefined){					
				var lastString = urlvalue.substring(urlvalue.length-6, urlvalue.length);
				var urlval = $(this).attr("data-targeturl");
				if(lastString != ".html?"){					
					$(this).attr("data-targeturl", urlval);
				}else{
					var link = urlval.slice(0, -1);
					$(this).attr("data-targeturl", link);
				}														
			}else{			
				$(this).attr("href", hrefvalue);									
			}	
			$('#QueyString-ct').text(qrystring);
	}).mousedown(function(){		  
		  var webtrendsParams = $("#QueyString-ct").text().split('=')[1].replace('%3F','?');
		  dcsMultiTrackForLPLinks(webtrendsParams);				  
	});
    $("div.promo-box").mouseout(function (){
		var urlvalue = $(this).attr("data-targeturl");
		var hrefvalue = $(this).attr("href");				
		if(urlvalue != undefined){												   
			$(this).attr('data-targeturl', urlvalue);					   
		}else{
			$(this).attr('href', hrefvalue);					   
		}
		$('#QueyString-ct').text('');  
    });	
}
}
function promoareaTrackMe1(currentitem){	
	var action = parentTracking('WT.z_nav');
	var needle1 = '<br>';
	var needle2 = '<br/>';
	var needle3 = '<br />';
	var tag1 = '<sup>?</sup>';
	var tag2 = '<sup>?</sup>';
	var tag3 = '<sup>&reg;</sup>';
	var tag4 = '<strong>';
	var tag5 = '</strong>';
	//For the Top Navigation
		action += ';Promo-3Blocks';
		if(currentitem.find('h3').html().indexOf(needle1) != -1 || currentitem.find('h3').html().indexOf(needle2) != -1 || currentitem.find('h3').html().indexOf(needle3) != -1){
			if($('#pagename').length === 0){
				/* This is for the HealthSpring pages */
				action += ';'+ currentitem.find('h2').text().trim().replace(/[;]*/gi, '')+' '+currentitem.find('h3 p').html().replace(needle1,' ').replace(needle2,' ').replace(needle3,' ').trim().replace(/\s{2,}/g, ' ').replace(tag1,'').replace(tag2,'').replace(tag3,'').replace(tag4,'').replace(tag5,'');
			}else{
				/* This is for the main Cigna.com homepage landing pages */
				action += ';'+ currentitem.find('h3 p').html().replace(needle1,' ').replace(needle2,' ').replace(needle3,' ').trim().replace(/\s{2,}/g, ' ').replace(tag1,'').replace(tag2,'').replace(tag3,'').replace(tag4,'').replace(tag5,'');
			}
		}else{
			if($('#pagename').length === 0){
				/* This is for the HealthSpring pages */
				action += ';'+currentitem.find('h2').text().trim().replace(/[;]*/gi, '')+' '+currentitem.find('h3').text().trim().replace(/[;]*/gi, '').replace(tag1,'').replace(tag2,'').replace(tag3,'').replace(tag4,'').replace(tag5,'');
			}else{
				/* This is for the main Cigna.com homepage landing pages */
				action += ';'+ currentitem.find('h3').text().trim().replace(/[;]*/gi, '').replace(tag1,'').replace(tag2,'').replace(tag3,'').replace(tag4,'').replace(tag5,'');
			}
		}		
		var urlparams = action.split("WT.z_nav=");
		action = encodeURIComponent(urlparams[1]);
		action = "WT.z_nav="+ action;		
	return action;
}
function promoctaclickableWrapperTrackMe1(currentitem){	
		var action = parentTracking('WT.z_nav');		
		if($('#pagename').length === 0){
			/* This is for the HealthSpring pages */
			action += ';Promo-CTA-Wrapper'; 
			if(currentitem.siblings('h2').text().trim().replace(/[;]*/gi, '') != ''){
				action += ';'+currentitem.siblings('h2').text().trim().replace(/[;]*/gi, '')+';'+currentitem.find('h3').text().trim().replace(/[;]*/gi, '')+'-'+currentitem.find('a').text().trim().replace(/[;]*/gi, '');
			}else{
				action += ';'+currentitem.find('h3').text().trim().replace(/[;]*/gi, '') +'-'+currentitem.find('a').text().trim().replace(/[;]*/gi, '');
			}
			
			//if(currentitem.attr("data-targeturl").indexOf('http://www.cigna.com/iwov-resources/medicare-2015/docs/2015-otc-catalog.pdf') != -1 || currentitem.attr("data-targeturl").indexOf('http://www.mycignahealthspring.com/ChangeAddress.aspx') != -1 || currentitem.attr("data-targeturl").indexOf('http://communication.cigna.com/?elqPURLPage=1082/') != -1){		
				action +=';'+'Redirect-Link';
				$('#QueyString-ct').text(action);
				action = '';
			//}	
		}else{
			/* This is for the main Cigna.com homepage landing pages */
			if(currentitem.siblings('h2').text().trim().replace(/[;]*/gi, '') != ''){
				action += ';Promo-CTA-Wrapper';
				action += ';'+currentitem.siblings('h2').text().trim().replace(/[;]*/gi, '')+';'+currentitem.find('h3').text().trim().replace(/[;]*/gi, '')+'-'+currentitem.find('a').text().trim().replace(/[;]*/gi, '');
			}else{
				action += ';Promo-CTA-Wrapper';
				action += ';'+currentitem.find('h3').text().trim().replace(/[;]*/gi, '') +'-'+currentitem.find('a').text().trim().replace(/[;]*/gi, '');
			}			
				action +=';'+'Redirect-Link';
				$('#QueyString-ct').text(action);
				action = '';
		}  
		if(action !=''){
			var urlparams = action.split("WT.z_nav="); 
			action = encodeURIComponent(urlparams[1]);
			action = "WT.z_nav="+ action;		
		}
	return action;
}
function promoctaWrapperTrackMe1(currentitem){	 
		var action = parentTracking('WT.z_nav');		
		action += ';Promo-CTA-Wrapper';		
		action += ';'+ currentitem.parent().siblings('div .plan-heading').find('h3').text().trim().replace(/[;]*/gi, '') +'-'+ currentitem.text().trim().replace(/[;]*/gi, '');
		var urlparams = action.split("WT.z_nav=");
		action = encodeURIComponent(urlparams[1]);
		action = "WT.z_nav="+ action;		
	return action;
}
function ifpRailPromoTrackMe1(currentitem){	
		var action = parentTracking('WT.z_nav');		
		if($("#right-rail-page-wrapper").length==1){
			action += ';RightRail';
		}else{
			action += ';LeftRail';
		}		
		action += ';'+currentitem.find('h2').text().trim().replace(/[;]*/gi, '');		
		var urlparams = action.split("WT.z_nav=");
		action = encodeURIComponent(urlparams[1]);
		action = "WT.z_nav="+ action;		
	return action;
}
function ifpRailPromoTrackMeLinks1(currentitem){
		var action = parentTracking('WT.z_nav');	
		if($("#right-rail-page-wrapper").length==1){
			action += ';RightRail';
		}else{
			action += ';LeftRail';
		}
		if(currentitem.closest('section').find('h2').text().trim().toLowerCase() != '' && currentitem.closest('section').find('h2').text().trim().toLowerCase() != undefined){	
			action += ';'+currentitem.closest('section').find('h2').text().trim().replace(/[;]*/gi, '')+';'+currentitem.text().trim().replace(/[;]*/gi, '');	
		}else{
			action += ';'+currentitem.closest('div.widget').find('h2').text().trim().replace(/[;]*/gi, '')+';'+currentitem.text().trim().replace(/[;]*/gi, '');
		}
		if(currentitem.attr("href").indexOf('https://cignahealthspring.destinationrx.com/plancompare/consumer/type2/2015/') != -1){	
			action +=';'+'Redirect-Link';
			$('#QueyString-ct').text(action);
			action = '';
		}	
		if(action !=''){
			var urlparams = action.split("WT.z_nav=");
			action = encodeURIComponent(urlparams[1]);
			action = "WT.z_nav="+ action;		
		}		
	return action;	
}
function promoboxTrackMe1(currentitem){	
	var action = parentTracking('WT.z_nav');
	if(currentitem.find('h2').text() != ''){		
		action += ';Promo-Blocks';
		action += ';'+ currentitem.find('h2').text().trim().replace(/[;]*/gi, '');
	}else{
		return false;
	}
	if(action !=''){
		var urlparams = action.split("WT.z_nav=");
		action = encodeURIComponent(urlparams[1]);
		action = "WT.z_nav="+ action;	
	}		
	return action;
}
function l3sectionLinks(currentitem){	
	var action = parentTracking('WT.z_nav');
	if(currentitem.closest('.l3-section-links').find('h2.module-heading').text() != undefined && currentitem.closest('.l3-section-links').find('h2.module-heading').text() != ''){
		if(currentitem.parents('p').length){
			action += ';'+ currentitem.closest('.l3-section-links').find('h2.module-heading').text().trim().replace(/[;]*/gi, '')+';'+currentitem.closest('p').siblings('h3').text().trim().replace(/[;]*/gi, '')+';'+currentitem.text().trim().replace(/[;]*/gi, '');
		}else{
			action += ';'+ currentitem.closest('.l3-section-links').find('h2.module-heading').text().trim().replace(/[;]*/gi, '')+';'+currentitem.text().trim().replace(/[;]*/gi, '');
		}		
	}else{
		if(currentitem.parent().siblings('h3').text() != undefined && currentitem.parent().siblings('h3').text() != ''){
			action += ';'+currentitem.parent().siblings('h3').text().trim().replace(/[;]*/gi, '')+';'+currentitem.text().trim().replace(/[;]*/gi, '');
		}else{
			action += ';'+currentitem.text().trim().replace(/[;]*/gi, '');    
		}	
	}
	if(action !=''){
		var urlparams = action.split("WT.z_nav=");
		action = encodeURIComponent(urlparams[1]);
		action = "WT.z_nav="+ action;	
	}		
	return action;
}
function parentTracking(param){
	var pagename = location.pathname.replace(/^\/|\/$/g, '');		
	if(pagename.indexOf('/index') != -1){
		return param += '=' + pagename.split('/index')[0];
	}else{
		return param += '=' + pagename;	
	}	
}	  
function promoctaBannerTrackMe1(currentitem){	
		var action = parentTracking('WT.z_nav');				
		action += ';Promo-CTA-Banner';	
		if(currentitem.closest('div').siblings('h3').text().trim() != undefined && currentitem.closest('div').siblings('h3').text().trim() !=''){			
			if($('#pagename').length === 0){
				/* This is for the HealthSpring pages */
				action += ';'+currentitem.closest('div').siblings('h3').text().trim().replace(/[;]*/gi, '');
			}else{
				/* This is for the main Cigna.com homepage landing pages */
				action += ';'+currentitem.closest('div').siblings('h3').text().trim().replace(/[;]*/gi, '')+'-'+currentitem.text().trim().replace(/[;]*/gi, '');
			}
		}else{			
			if($('#pagename').length === 0){
				/* This is for the HealthSpring pages */
				action += ';'+currentitem.closest('div').siblings('h2').text().trim().replace(/[;]*/gi, '');
			}else{
				/* This is for the main Cigna.com homepage landing pages */
				action += ';'+currentitem.closest('div').siblings('h2').text().trim().replace(/[;]*/gi, '')+'-'+currentitem.text().trim().replace(/[;]*/gi, '');
			}
		}
		var urlparams = action.split("WT.z_nav=");
		action = encodeURIComponent(urlparams[1]);
		action = "WT.z_nav="+ action;		
	return action;
}
function promomobilectaBannerTrackMe1(currentitem){	
		var action = parentTracking('WT.z_nav');				
		action += ';Promo-CTA-Banner';		
		if(currentitem.closest('div').siblings().find('h3').text().trim() != undefined && currentitem.closest('div').siblings().find('h3').text().trim() !=''){			
			if($('#pagename').length === 0){
				/* This is for the HealthSpring pages */
				action += ';'+currentitem.closest('div').siblings().find('h3').text().trim().replace(/[;]*/gi, '');
			}else{
				/* This is for the main Cigna.com homepage landing pages */
				action += ';'+currentitem.closest('div').siblings().find('h3').text().trim().replace(/[;]*/gi, '')+'-'+currentitem.text().trim().replace(/[;]*/gi, '');
			}
		}else{			
			if($('#pagename').length === 0){
				/* This is for the HealthSpring pages */
				action += ';'+currentitem.closest('div').siblings().find('h2').text().trim().replace(/[;]*/gi, '');
			}else{
				/* This is for the main Cigna.com homepage landing pages */
				action += ';'+currentitem.closest('div').siblings().find('h2').text().trim().replace(/[;]*/gi, '')+'-'+currentitem.text().trim().replace(/[;]*/gi, '');
			}
		}
		var urlparams = action.split("WT.z_nav=");
		action = encodeURIComponent(urlparams[1]);
		action = "WT.z_nav="+ action;		
	return action;
}
function promoheroBannerTrackMe1(currentitem){	
		var action = parentTracking('WT.z_nav');				
		action += ';HeroImage-Banner';	
		if(currentitem.closest('div').siblings('h3').text().trim() != undefined && currentitem.closest('div').siblings('h3').text().trim() !=''){			
			action += ';'+currentitem.closest('div').siblings('h3').text().trim().replace(/[;]*/gi, '');
		}else if(currentitem.closest('div').siblings('h2').text().trim() != undefined && currentitem.closest('div').siblings('h2').text().trim() !=''){						
			action += ';'+currentitem.closest('div').siblings('h2').text().trim().replace(/[;]*/gi, '');			
		}else if(currentitem.closest('div').siblings('h1').text().trim() != undefined && currentitem.closest('div').siblings('h1').text().trim() !=''){
			action += ';'+currentitem.closest('div').siblings('h1').text().trim().replace(/[;]*/gi, '');
		}
		action += '-' + currentitem.text().trim().replace(/[;]*/gi, '');
		var urlparams = action.split("WT.z_nav=");
		action = encodeURIComponent(urlparams[1]);
		action = "WT.z_nav="+ action;		
	return action;
}
function promoheromobileBannerTrackMe1(currentitem){	
		var action = parentTracking('WT.z_nav');				
		action += ';HeroImage-Banner';		
		if(currentitem.closest('div').siblings().find('h3').text().trim() != undefined && currentitem.closest('div').siblings().find('h3').text().trim() !=''){			
			action += ';'+currentitem.closest('div').siblings().find('h3').text().trim().replace(/[;]*/gi, '');
		}else if(currentitem.closest('div').siblings().find('h2').text().trim() != undefined && currentitem.closest('div').siblings().find('h2').text().trim() !=''){	
			action += ';'+currentitem.closest('div').siblings().find('h2').text().trim().replace(/[;]*/gi, '');			
		}else if(currentitem.closest('div').siblings().find('h1').text().trim() != undefined && currentitem.closest('div').siblings().find('h1').text().trim() !=''){
			action += ';'+currentitem.closest('div').siblings().find('h1').text().trim().replace(/[;]*/gi, '');
		}
		action += '-' + currentitem.text().trim().replace(/[;]*/gi, '');
		var urlparams = action.split("WT.z_nav=");
		action = encodeURIComponent(urlparams[1]);
		action = "WT.z_nav="+ action;		
	return action;
}
function heroBannerTrackMe1(currentitem){	
		var action = parentTracking('WT.z_nav');		
		action += ';HeroImage-Banner';			
		if(currentitem.parent().siblings('h2').text().trim() != undefined && currentitem.parent().siblings('h2').text().trim() !=''){			
			action += ';'+currentitem.parent().siblings('h2').text().trim().replace(/[;]*/gi, '');
		}else{			
			action += ';'+currentitem.text().trim().replace(/[;]*/gi, '');
		}		
		var urlparams = action.split("WT.z_nav=");
		action = encodeURIComponent(urlparams[1]);
		action = "WT.z_nav="+ action;		
	return action;
}
function IWanttoBannerTrackMe(currentitem){	
		var action = parentTracking('WT.z_nav');		
		action += ';IWantTo';							
		action += ';'+currentitem.text().trim().replace(/[;]*/gi, '');	
		var urlparams = action.split("WT.z_nav=");
		action = encodeURIComponent(urlparams[1]);
		action = "WT.z_nav="+ action;
		if(currentitem.attr("href").toLowerCase().indexOf('http://www.cigna.com/sites/colibrium/switch.htm') != -1){
			action +=';'+'Redirect-Link';
			$('#QueyString-ct').text(action);
			action = '';
		}					
	return action;
}
function articlePromoTrackMe(currentitem){	
		var action = parentTracking('WT.z_nav');		
		action += ';Article-Promo';
		if(currentitem.closest('div.article-desc').siblings('h2').text() != undefined && currentitem.closest('div.article-desc').siblings('h2').text() !=''){
		      action += ';'+currentitem.closest('div.article-desc').siblings('h2').text().trim().replace(/[;]*/gi, '')+';'+currentitem.text().trim().replace(/[;]*/gi, '');		
		}else{
		      action += ';'+currentitem.closest('http://www.cigna.com/iwov-resources/scripts/global/div.row').siblings('http://www.cigna.com/iwov-resources/scripts/global/div.row').find('h2').text().trim().replace(/[;]*/gi, '')+';'+currentitem.text().trim().replace(/[;]*/gi, '');	
		}			
		var urlparams = action.split("WT.z_nav=");
		action = encodeURIComponent(urlparams[1]);
		action = "WT.z_nav="+ action;
		if(currentitem.attr("href").toLowerCase().indexOf('http://www.cigna.com/sites/colibrium/switch.htm') != -1){
			action +=';'+'Redirect-Link';
			$('#QueyString-ct').text(action);
			action = '';
		}					
	return action;
}
function authoredPromoTrackMe(currentitem){	
		var action = parentTracking('WT.z_nav');		
		action += ';AuthoredPromo';			
		if(currentitem.attr("href").toLowerCase().indexOf('javascript:void(0)') === -1){
			action += ';'+currentitem.closest('.content-container').find('h2').text().trim().replace(/[;]*/gi, '')+';'+currentitem.text().trim().replace(/[;]*/gi, '');	
		}else{
			action = '';
		}	
		if(action !=''){		
			var urlparams = action.split("WT.z_nav=");
			action = encodeURIComponent(urlparams[1]);
			action = "WT.z_nav="+ action;						
		}
	return action;
}
function articleLinks(currentitem){	
	var action = parentTracking('WT.z_nav');		
		action += ';Body';			
		if(currentitem.attr("href").toLowerCase().indexOf('javascript:') === -1 && currentitem.attr("href").toLowerCase().slice(0,1) != '#' && currentitem.attr("href").toLowerCase().indexOf('mailto:') === -1 && currentitem.attr('href').toLowerCase().indexOf('tel:') === -1){
			action += ';'+currentitem.text().trim().replace(/[;]*/gi, '');	
		}else{
			action = '';
		}		
		if(action !=''){		
			var urlparams = action.split("WT.z_nav=");
			action = encodeURIComponent(urlparams[1]);
			action = "WT.z_nav="+ action;									
		}		
	return action;
}
function accordion_links1(currentitem){						
		if(currentitem.attr('href').indexOf('mailto:') != -1 || currentitem.attr('href').indexOf('tel:') != -1){
			action ="";			
		}else{
			var action = parentTracking('WT.z_nav');		
			action += ';accordion';
			action += ';'+ currentitem.closest('dd').prev().text().trim().replace(/[;]*/gi, '')+';'+currentitem.text().trim().replace(/[;]*/gi, '');	
			var urlparams = action.split("WT.z_nav=");
			action = encodeURIComponent(urlparams[1]);
			action = "WT.z_nav="+ action;
		}
		if(currentitem.attr("href").toLowerCase().indexOf('eloqua-contact-form') != -1 || currentitem.attr("href").toLowerCase().indexOf('/medicare') != -1  || currentitem.attr('href').indexOf('#') != -1){			
			action +=';'+'Redirect-Link';
			$('#QueyString-ct').text(action);
			action = '';
		}	
	return action;
}
function accordion_mainlinks1(currentitem){								
	var action = parentTracking('WT.z_nav');		
	action += ';accordion';
	action += ';'+currentitem.text().trim().replace(/[;]*/gi, '');	
	var urlparams = action.split("WT.z_nav=");
	action = encodeURIComponent(urlparams[1]);
	action = "WT.z_nav="+ action;			
	return action;
}
function linkListContainer(currentitem){								
	var action = parentTracking('WT.z_nav');		
	action += ';link-List';
	if(currentitem.closest('.linked-list-cotainer').find('h4').text() != undefined && currentitem.closest('.linked-list-cotainer').find('h4').text() !=''){
		action += ';'+currentitem.closest('.linked-list-cotainer').find('h4').text().trim().replace(/[;]*/gi, '')+';'+currentitem.text().trim().replace(/[;]*/gi, '');
	}else{
		action += ';'+currentitem.text().trim().replace(/[;]*/gi, '');
	}	
	var urlparams = action.split("WT.z_nav=");
	action = encodeURIComponent(urlparams[1]);
	action = "WT.z_nav="+ action;			
	return action;
}