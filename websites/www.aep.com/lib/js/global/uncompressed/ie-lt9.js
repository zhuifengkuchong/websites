$(document).ready(function() {
	// Polyfill CSS3 styles
	// nth-child
	$(".block-grid.two-up>li:nth-child(2n+1)").css("clear", "left");
	$(".block-grid.three-up>li:nth-child(3n+1)").css("clear", "left");
	$(".block-grid.four-up>li:nth-child(4n+1)").css("clear", "left");

	$(".image-blocks.block-grid.two-up-at-900>li:nth-child(2n+1)").css("clear", "left");
	$(".image-blocks.block-grid.two-up-at-900>li:nth-child(3n+1)").css("clear", "none");

	$(".table-list tr:nth-child(2n+1) td").css("background-color", "#efeeed");
	$("table.alternate-rows tr:nth-child(2n+1) td").css("background-color", "#efeeed");
	$("table.filingsalternate-rows tr:nth-child(2n+1) td").css("background-color", "#efeeed");
});