var LatestStoriesViewModel = function (){
    var self = this;
    self.HomeStoriesData = ko.observableArray();
    self.FeaturedStory = ko.observableArray();
    self.NonFeaturedStories = ko.observableArray();


    self.getLatestStories = function () {
        var selValue = $('#stories-filter').val();

        $.get('http://www.generalmills.com/Scripts/gmi/service/LatestStoriesByCategory.ashx', { SelectedCategory: selValue }, function (data) {
            self.HomeStoriesData($.parseJSON(data));
            self.FeaturedStory(self.HomeStoriesData().FeaturedStory);
            self.NonFeaturedStories(self.HomeStoriesData().NonFeaturedStories);
        });
    };
};




$(document).ready(function () {
    var _viewModel = new LatestStoriesViewModel();
    _viewModel.getLatestStories();
    $('#stories-filter').change(function () {
        _viewModel.getLatestStories();
    });
    ko.applyBindings(_viewModel);
});