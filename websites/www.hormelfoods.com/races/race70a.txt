<script id = "race70a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race70={};
	myVars.races.race70.varName="Function[30886].elem";
	myVars.races.race70.varType="@varType@";
	myVars.races.race70.repairType = "@RepairType";
	myVars.races.race70.event1={};
	myVars.races.race70.event2={};
	myVars.races.race70.event1.id = "Lu_Id_li_6";
	myVars.races.race70.event1.type = "onmouseover";
	myVars.races.race70.event1.loc = "Lu_Id_li_6_LOC";
	myVars.races.race70.event1.isRead = "False";
	myVars.races.race70.event1.eventType = "@event1EventType@";
	myVars.races.race70.event2.id = "Lu_Id_li_5";
	myVars.races.race70.event2.type = "onmouseout";
	myVars.races.race70.event2.loc = "Lu_Id_li_5_LOC";
	myVars.races.race70.event2.isRead = "True";
	myVars.races.race70.event2.eventType = "@event2EventType@";
	myVars.races.race70.event1.executed= false;// true to disable, false to enable
	myVars.races.race70.event2.executed= false;// true to disable, false to enable
</script>

