<script id = "race63a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race63={};
	myVars.races.race63.varName="Function[30886].elem";
	myVars.races.race63.varType="@varType@";
	myVars.races.race63.repairType = "@RepairType";
	myVars.races.race63.event1={};
	myVars.races.race63.event2={};
	myVars.races.race63.event1.id = "Lu_Id_li_6";
	myVars.races.race63.event1.type = "onmouseover";
	myVars.races.race63.event1.loc = "Lu_Id_li_6_LOC";
	myVars.races.race63.event1.isRead = "False";
	myVars.races.race63.event1.eventType = "@event1EventType@";
	myVars.races.race63.event2.id = "Lu_Id_li_4";
	myVars.races.race63.event2.type = "onmouseout";
	myVars.races.race63.event2.loc = "Lu_Id_li_4_LOC";
	myVars.races.race63.event2.isRead = "True";
	myVars.races.race63.event2.eventType = "@event2EventType@";
	myVars.races.race63.event1.executed= false;// true to disable, false to enable
	myVars.races.race63.event2.executed= false;// true to disable, false to enable
</script>

