<script id = "race57a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race57={};
	myVars.races.race57.varName="Function[28825].elem";
	myVars.races.race57.varType="@varType@";
	myVars.races.race57.repairType = "@RepairType";
	myVars.races.race57.event1={};
	myVars.races.race57.event2={};
	myVars.races.race57.event1.id = "Lu_Id_li_5";
	myVars.races.race57.event1.type = "onmouseover";
	myVars.races.race57.event1.loc = "Lu_Id_li_5_LOC";
	myVars.races.race57.event1.isRead = "False";
	myVars.races.race57.event1.eventType = "@event1EventType@";
	myVars.races.race57.event2.id = "Lu_Id_li_3";
	myVars.races.race57.event2.type = "onmouseout";
	myVars.races.race57.event2.loc = "Lu_Id_li_3_LOC";
	myVars.races.race57.event2.isRead = "True";
	myVars.races.race57.event2.eventType = "@event2EventType@";
	myVars.races.race57.event1.executed= false;// true to disable, false to enable
	myVars.races.race57.event2.executed= false;// true to disable, false to enable
</script>

