<script id = "race56b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race56={};
	myVars.races.race56.varName="Function[30886].elem";
	myVars.races.race56.varType="@varType@";
	myVars.races.race56.repairType = "@RepairType";
	myVars.races.race56.event1={};
	myVars.races.race56.event2={};
	myVars.races.race56.event1.id = "Lu_Id_li_3";
	myVars.races.race56.event1.type = "onmouseout";
	myVars.races.race56.event1.loc = "Lu_Id_li_3_LOC";
	myVars.races.race56.event1.isRead = "True";
	myVars.races.race56.event1.eventType = "@event1EventType@";
	myVars.races.race56.event2.id = "Lu_Id_li_6";
	myVars.races.race56.event2.type = "onmouseover";
	myVars.races.race56.event2.loc = "Lu_Id_li_6_LOC";
	myVars.races.race56.event2.isRead = "False";
	myVars.races.race56.event2.eventType = "@event2EventType@";
	myVars.races.race56.event1.executed= false;// true to disable, false to enable
	myVars.races.race56.event2.executed= false;// true to disable, false to enable
</script>

