<script id = "race52b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race52={};
	myVars.races.race52.varName="Function[25463].elem";
	myVars.races.race52.varType="@varType@";
	myVars.races.race52.repairType = "@RepairType";
	myVars.races.race52.event1={};
	myVars.races.race52.event2={};
	myVars.races.race52.event1.id = "Lu_Id_li_2";
	myVars.races.race52.event1.type = "onmouseout";
	myVars.races.race52.event1.loc = "Lu_Id_li_2_LOC";
	myVars.races.race52.event1.isRead = "True";
	myVars.races.race52.event1.eventType = "@event1EventType@";
	myVars.races.race52.event2.id = "Lu_Id_li_3";
	myVars.races.race52.event2.type = "onmouseover";
	myVars.races.race52.event2.loc = "Lu_Id_li_3_LOC";
	myVars.races.race52.event2.isRead = "False";
	myVars.races.race52.event2.eventType = "@event2EventType@";
	myVars.races.race52.event1.executed= false;// true to disable, false to enable
	myVars.races.race52.event2.executed= false;// true to disable, false to enable
</script>

