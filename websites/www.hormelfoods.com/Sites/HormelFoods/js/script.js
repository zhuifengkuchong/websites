/*
* Client: Hormel Foods
* Author: Proof IC
*/

$(document).ready(function () {

    /*  ================================================
    SET VARIABLES
    ================================================  */

    var windowWidth, windowHeight, slideCount, slideWidth, currentSlide, navClick, slideParent, state, theCase, isIE7, bodyHeight;
    windowWidth = $(window).width();
    windowHeight = $(window).height();
    bodyHeight = $('body').height();
    isIE7 = ($.browser.msie && parseInt($.browser.version, 10) === 7);
    isIE = $.browser.msie && parseInt($.browser.version, 10);
    var FF = !(window.mozInnerScreenX == null);


    /*  ================================================
    RESIZE BROWSER
    ================================================  */

    $(window).bind('resize', function () {
        windowWidth = $(window).width();
        windowHeight = $(window).height();

        slideWidth = slideParent.css('width', slideCount * windowWidth);
        slideParent.children('li').css('width', windowWidth);
        // var currentSlide = slideParent.children('li.inView').index();

        slideNumber = currentSlide - 1;
        slideParent.css({ marginLeft: '-' + slideNumber * windowWidth + 'px' });

        if ($('body').hasClass('four-four')) {
            setFooter();
        };
    });


    /*  ================================================
    MAIN NAVIGATION DROPS
    ================================================  */

    $('nav.main-nav li.current').each(function () {
        $('li.current').find('.gap, .gaplast').addClass('transparent').end()
            .next('li').find('.gap').addClass('transparent');
    });

    $("nav.main-nav li").hover(function () {
        $linkClass = $(this).children('a').attr('class');
        thisNav = $(this).children('a');

        if (thisNav.hasClass('current')) {
        } else {
            $(this).find('.drop').slideDown(200, function () {
                //$(this).css({ borderBottom: '2px solid #fff' });
            }).dequeue().end()
                .find('.gap, .gaplast').dequeue().stop().animate({ opacity: 0 }, 200).end()
                .next('li').find('.gap').dequeue().stop().animate({ opacity: 0 }, 200);
            thisNav.addClass('isOver');
        }
    }, function () {
        // $(this).find('.drop').css({ borderBottom: 'none' }).dequeue().stop().slideUp(300).dequeue().end()
        $(this).find('.drop').dequeue().stop().slideUp(200).dequeue().end()
            .find('.gap, .gaplast').dequeue().stop().animate({ opacity: 1 }, 200).end()
            .next('li').find('.gap').dequeue().stop().animate({ opacity: 1 }, 200);
        thisNav.removeClass('isOver');
    });


    /*  ================================================
    SUBNAV HOVERS
    ================================================  */

    $('nav.sub-nav li a').bind("mouseover", function (event) {
        if ($(this).parent('li').next('li').find('a').hasClass('current')) {
            $(this).css('border-right', '1px solid transparent');
        } else if ($(this).parent('li').prev('li').find('a').hasClass('current')) {
            $(this).css('border-left', '1px solid transparent');
        };
    });


    /*  ================================================
    VIDEO OVERLAY & MAP OVERLAY
    ================================================  */

    $('.movie-btn, .vid-link, .vid-enlarge-btn, .hdr .call-out').live("click", function (e) {
        e.preventDefault();
        var iframeId;
        if (isIE7) {
            iframeId = $('#vidCont').attr('name');
            $('<iframe src="http://www.youtube.com/embed/' + iframeId + '" width="500" height="375" id="player" frameborder="0" allowfullscreen></iframe>').insertBefore('#vidCont span');
            $('#vidOverlay, #vidCont').fadeIn(500);
        } else {
            $('#vidOverlay').fadeIn(500, function () { $('#vidCont').fadeIn(500); });
        };
    });
    $('#vidCont #close').live("click", function (e) {
        e.preventDefault();
        $('#vidOverlay, #vidCont').fadeOut(600, function () {
            if (isIE7) { $('#player').remove(); };
        });
        if (!isIE7) { stopVideo(); };
    });
    $('.mapoverlay-link').live("click", function (e) {
        e.preventDefault();
        $('#vidOverlay, #mapCont').fadeIn(600);
    });
    $('#mapCont #close').live("click", function (e) {
        e.preventDefault();
        $('#vidOverlay, #mapCont').fadeOut(600);
    });
    $('#selectSite #close2').live("click", function (e) {
        e.preventDefault();
        $('#selectSite').fadeOut(600);
    });
    $('#selectSite.skippy #close2').live("click", function (e) {
        e.preventDefault();
        $('#selectSite').fadeOut(600);
        $('.cont .inner').fadeIn(800);
    });



    /*  ================================================
    BRAND PRODUCT INFO OVERLAY
    ================================================  */

    $('.info-open').live("click", function (e) {
        e.preventDefault();
        productUPC = $(this).attr('id');
        myPath = 'https://ws.hormelfoods.com/api/v1.0/Product/get/' + productUPC;
        template = 'nutritionTemplate';
        holder = 'nutritionPlaceholder';
        runAjax(myPath, template, holder);
        custom = 'fade';
       

    });

    function runAjax(myPath, template, holder, url) {
        $.ajax({
            url: myPath,
            dataType: "jsonp",
            processData: false,
            crossDomain: true,
            async: true,
            type: 'GET',
            success: function (data) {

               // alert('SUCCESS-1');
                var src = $('#' + template).html();
                var tmpl = Handlebars.compile(src);
                var jsons = data;
                $('#' + holder).html(tmpl(jsons));

                if (custom == 'fade') {  // CUSTOM FADE
                $('#nutritionPlaceholder').fadeIn(600);
                $('#nutritionPlaceholder img').error(function () { $(this).attr('src', '../../../img/ComingSoon.png'/*tpa=http://www.hormelfoods.com/img/ComingSoon.png*/) });
                }
                if (custom == 'productChange') {  // CUSTOM WRITE IN 
                if ($('.zip-locator').hasClass('automated')) {
                productNameChange();
                $('.search-text').text($('#productName option:selected').text());
                } else {
                $('#productName option:first').attr('selected', 'selected');
                $('.search-text').text($('#productName option:selected').text());
                }
                }
                if (custom == 'highlightItem') {  // CUSTOM HIGHLIGHT ITEM
                $('div.dropDown').first().addClass('current');
                }
            },
            error: function (data) {
                alert("ERROR");
            }
        });
    }

    $('a.info-close').live("click", function (e) {
        e.preventDefault();
        $('#nutritionPlaceholder').fadeOut(600);
    });


    /*  ================================================
    FULL WIDTH IMAGE SLIDER
    ================================================  */

    slideParent = $('#sliderArea ul');
    slideCount = slideParent.children('li').length;
    slideWidth = slideParent.css('width', slideCount * windowWidth);
    slideParent.children('li').css('width', windowWidth);
    currentSlide = 1;
    origCountPlus = slideParent.children('li').length + 1;
    var newObject;

    $('.sliderBtn').live("click", function (e) {
        e.preventDefault();
        navClick = $(this).attr('id');
        stopCycle();

        if ($('.sliderBtn').hasClass('active')) {  // DO NOTHING
        } else if (navClick == 'prevArrow') {
            $(this).addClass('active');
            fullSlidePrev();  // NEXT SLIDE
        } else if (navClick == 'nextArrow') {
            $(this).addClass('active');
            fullSlideNext();  // PREVIOUS SLIDE
        };
    });

    function fullSlidePrev() {
        if (currentSlide <= 1) {
            newObject = $('#sliderArea ul li').last();
            newObject.prependTo('#sliderArea ul');
            slideParent.css({ marginLeft: '-=' + windowWidth + 'px' });

            slideParent.animate({ marginLeft: '+=' + windowWidth + 'px' }, 800, function () { $('.sliderBtn').removeClass('active') });
        } else {
            currentSlide--;
            slideParent.animate({ marginLeft: '+=' + windowWidth + 'px' }, 800, function () { $('.sliderBtn').removeClass('active') });
        };
    }

    function fullSlideNext() {
        if (currentSlide == slideCount) {
            newObject = $('#sliderArea ul li').first();
            slideWidth = slideParent.css('marginLeft', '+=' + windowWidth);
            newObject.appendTo('#sliderArea ul');

            slideParent.animate({ marginLeft: '-=' + windowWidth + 'px' }, 800, function () { $('.sliderBtn').removeClass('active') });
        } else {
            currentSlide++;
            slideParent.animate({ marginLeft: '-=' + windowWidth + 'px' }, 800, function () { $('.sliderBtn').removeClass('active') });
        }
    }

    var cycleTimer;
    function startCycle() {
        cycleTimer = setInterval(function () {
            $('.sliderBtn').addClass('active');
            fullSlideNext();
        }, 8000);
    }
    function stopCycle() {
        clearInterval(cycleTimer);
        state = 1;
    }
    if ($('body').hasClass('home')) { startCycle() };

    $(document.documentElement).keyup(function (event) {
        if (event.keyCode == 37) {
            $('#prevArrow').click();
        } else if (event.keyCode == 39) {
            $('#nextArrow').click();
        };
    });


    /*  ================================================
    RECIPE TIP
    ================================================  */

    /* $('.recipe-pop .plus').bind("mousemove", function(event) {
    $('.recipe-pop .text, .recipe-pop .tick').stop().animate({ opacity: 1 }, 100).addClass('over');
    });
    $('.recipe-pop').bind("mouseleave", function() {
    $('.recipe-pop .text, .recipe-pop .tick').stop().delay(300).animate({ opacity: 0 }, 100).removeClass('over');
    }); */
    $('.recipe-pop .plus').live("click", function () {
        $(this).removeClass('icon-plus').addClass('icon-minus');
        $(this).parent('.recipe-pop').find('.text, .tick').animate({ opacity: 1 }, 100).addClass('over');
    });
    $('.recipe-pop .icon-minus').live("click", function () {
        $(this).removeClass('icon-minus').addClass('icon-plus');
        $(this).parent('.recipe-pop').find('.text, .tick').animate({ opacity: 0 }, 100).removeClass('over');
    });


    /*  ================================================
    BRANDS LEFT SIDEBAR
    ================================================  */

    $('.product-info .menu .link.nav').live("click", function (e) {
        e.preventDefault();
        if ($(this).hasClass('isOpen')) {
            $('.product-info .inner').slideUp(300).fadeOut(300);
            $('http://www.hormelfoods.com/Sites/HormelFoods/js/a.nav').css('background-position', 'right 15px');
            $('.link.nav').removeClass('isOpen');
        } else {
            $('.product-info .inner').slideUp(200).fadeOut(200);
            $('http://www.hormelfoods.com/Sites/HormelFoods/js/a.nav').css('background-position', 'right 15px');
            $('.link.nav').removeClass('isOpen');
            $(this).css('background-position', 'right -10px');
            $(this).next('.inner').delay(100).slideDown(300).fadeIn(300).end()
	    .addClass('isOpen');
        }
    });
    /* $('.product-info .menu').bind("mouseenter", function(event) {
    $(this).find('.inner').slideDown(300).fadeIn(300);
    $(this).find('http://www.hormelfoods.com/Sites/HormelFoods/js/a.nav').css('background-position','right -8px');
    });
    $('.product-info .menu').bind("mouseleave", function() {
    $(this).find('.inner').stop().delay(300).slideUp(300).fadeOut(300);
    $(this).find('http://www.hormelfoods.com/Sites/HormelFoods/js/a.nav').css('background-position','right 15px');
    }); */


    /*  ================================================
    CONTENT CAROUSELS
    ================================================  */

    currentPanel = 1;
    panelParent = $('.tweet-slide.slider ul');
    setTimeout(function () {
        panelCount = panelParent.children('li').length;
        panelWidth = panelParent.children('li').outerWidth(true);
        panelParent.css('width', panelCount * panelWidth + 'px');
    }, 800);

    fullCurrentPanel = 1;
    fullPanelParent = $('.content-slide .slider ul');
    fullPanelCount = fullPanelParent.children('li').length;
    fullPanelWidth = fullPanelParent.children('li').outerWidth(true);
    setTimeout(function () { fullPanelParent.css('width', fullPanelCount * fullPanelWidth + 'px') }, 200);

    awardPanel = 1;
    awardParent = $('.awards-slide ul');
    awardCount = awardParent.children('li').length;
    awardWidth = awardParent.children('li').outerWidth(true);
    setTimeout(function () { awardParent.css('width', awardCount * awardWidth + 'px') }, 200);

    imagePanel = 1;
    imageParent = $('.image-slider ul');
    imageCount = imageParent.children('li').length;
    imageWidth = imageParent.children('li').outerWidth(true);
    setTimeout(function () { imageParent.css('width', imageCount * imageWidth + 'px') }, 200);

    newsPanel = 1;
    newsParent = $('.news-slide .slider ul');
    newsCount = newsParent.children('li').length;
    newsWidth = newsParent.children('li').outerWidth(true);
    setTimeout(function () { newsParent.css('width', newsCount * newsWidth + 'px') }, 200);

    investPanel = 1;
    investParent = $('.investors-slide ul');
    investCount = investParent.children('li').length;
    investWidth = investParent.children('li').outerWidth(true);
    setTimeout(function () { investParent.css('width', investCount * investWidth + 'px') }, 200);


    /* TWITTER FACEBOOK CAROUSEL - 3 ITEMS */
    $('.tweetSlideBtn').live("click", function (e) {
        e.preventDefault();
        slideClick = $(this).attr('id');

        if (slideClick == 'prevSlideArrow') {
            if (currentPanel > 1) {
                currentPanel = currentPanel - 3;

                if (currentPanel < 2) {
                    panelParent.animate({ marginLeft: '0' }, 700);
                    currentPanel = 1;
                } else {
                    panelParent.animate({ marginLeft: '+=' + panelWidth * 3 + 'px' }, 700);
                }

                if (currentPanel <= 1) { $('#prevSlideArrow').addClass('none'); }
                $('#nextSlideArrow').removeClass('none');
            }
        } else if (slideClick == 'nextSlideArrow') {
            if (currentPanel < panelCount) {
                currentPanel = currentPanel + 3;

                if (panelCount - currentPanel < 3) {
                    panelMath = panelCount - currentPanel + 1;
                    panelParent.animate({ marginLeft: '-=' + panelWidth * panelMath + 'px' }, 700);
                    currentPanel = currentPanel - 3 + panelMath;
                } else {
                    panelParent.animate({ marginLeft: '-=' + panelWidth * 3 + 'px' }, 700);
                }

                if (panelCount - currentPanel < 3) { $('#nextSlideArrow').addClass('none'); }
                $('#prevSlideArrow').removeClass('none');
            }
        };
    });

    /* CONTENT CAROUSEL */
    $('.content-slide .slider ul li').each(function () {
        $('#pager').append('<span class="tick"></span>');
    });
    $('#pager .tick:first-child').first().addClass('current');

    $('.content-slide .contentSlideBtn').live("click", function (e) {
        e.preventDefault();
        $('.content-slide .panel').css('display', 'block');
        fullSlideClick = $(this).attr('id');

        if (fullSlideClick == 'prevSlideBtn') {
            if (fullCurrentPanel > 1) {
                fullCurrentPanel--;
                fullPanelParent.animate({ marginLeft: '+=' + fullPanelWidth + 'px' }, 700, function () {
                    $('.tick.current').removeClass('current').prev('.tick').addClass('current');
                });
                if (fullCurrentPanel <= 1) { $('.content-slide #prevSlideBtn').addClass('none'); }
                $('.content-slide #nextSlideBtn').removeClass('none');
            }
        } else if (fullSlideClick == 'nextSlideBtn') {
            if (fullCurrentPanel < fullPanelCount) {
                fullCurrentPanel++;
                fullPanelParent.animate({ marginLeft: '-=' + fullPanelWidth + 'px' }, 700, function () {
                    $('.tick.current').removeClass('current').next('.tick').addClass('current');
                });
                if (fullPanelCount == fullCurrentPanel) { $('.content-slide #nextSlideBtn').addClass('none'); }
                $('.content-slide #prevSlideBtn').removeClass('none');
            }
        };
    });

    /* TICK INDICATOR CLICK */
    $('.tick').live("click", function () {
        $index = $(this).index();
        $('.content-slide .panel').css('display', 'block');
        $current_tick = $('.tick.current').index();

        if ($(this).is(':last-child')) {
            $('#nextSlideBtn').addClass('none');
            $('#prevSlideBtn').removeClass('none');
        } else if ($(this).is(':first-child')) {
            $('#nextSlideBtn').removeClass('none');
            $('#prevSlideBtn').addClass('none');
        } else {
            $('#prevSlideBtn, #nextSlideBtn').removeClass('none');
        }

        if ($(this).hasClass('current')) {
        } else {
            if ($index < $current_tick) {
                $slide_amount = $current_tick - $index;
                fullPanelParent.animate({ marginLeft: '+=' + fullPanelWidth * $slide_amount + 'px' }, 700, function () { });
                $('.tick.current').removeClass('current');
                $(this).addClass('current');
                sliderCount = $current_tick - $index;
                fullCurrentPanel = fullCurrentPanel - sliderCount;
                //alert(fullCurrentPanel);

            } else {
                $slide_amount = $index - $current_tick;
                fullPanelParent.animate({ marginLeft: '-=' + fullPanelWidth * $slide_amount + 'px' }, 700, function () { });
                $('.tick.current').removeClass('current');
                $(this).addClass('current');
                sliderCount = $index - $current_tick;
                fullCurrentPanel = fullCurrentPanel + sliderCount;
                //alert(fullCurrentPanel);
            }
        }
    });




    /* AWARDS CAROUSEL - 4 ITEMS */
    $('.awards-slide .contentSlideBtn').live("click", function (e) {
        e.preventDefault();
        slideClick = $(this).attr('id');

        if (slideClick == 'prevSlideArrow') {
            if (awardPanel > 1) {
                awardPanel = awardPanel - 4;

                if (awardPanel < 3) {
                    awardParent.animate({ marginLeft: '0' }, 700);
                    awardPanel = 1;
                } else {
                    awardParent.animate({ marginLeft: '+=' + awardWidth * 4 + 'px' }, 700);
                }

                if (awardPanel <= 1) { $('.awards-slide .contentSlideBtn').first().addClass('none'); }
                $('.awards-slide .contentSlideBtn').last().removeClass('none');
            }
        } else if (slideClick == 'nextSlideArrow') {
            if (awardPanel < awardCount) {
                awardPanel = awardPanel + 4;

                if (awardCount - awardPanel < 4) {
                    panelMath = awardCount - awardPanel + 1;
                    awardParent.animate({ marginLeft: '-=' + awardWidth * panelMath + 'px' }, 700);
                    awardPanel = awardPanel - 4 + panelMath;
                } else {
                    awardParent.animate({ marginLeft: '-=' + awardWidth * 4 + 'px' }, 700);
                }

                if (awardCount - awardPanel < 4) { $('.awards-slide .contentSlideBtn').last().addClass('none'); }
                $('.awards-slide .contentSlideBtn').first().removeClass('none');
            }
        };
    });


    /* SINGLE IMAGE CAROUSEL */
    $('.image-slider .contentSlideBtn').live("click", function (e) {
        e.preventDefault();
        thisClick = $(this).attr('id');

        if (thisClick == 'prevSlideArrow') {
            if (imagePanel > 1) {
                imagePanel--;
                imageParent.animate({ marginLeft: '+=' + imageWidth + 'px' }, 300);
                if (imagePanel <= 1) { $('.image-slider #prevSlideArrow').addClass('none'); }
                $('.image-slider #nextSlideArrow').removeClass('none');
            }
        } else if (thisClick == 'nextSlideArrow') {
            if (imagePanel < imageCount) {
                imagePanel++;
                imageParent.animate({ marginLeft: '-=' + imageWidth + 'px' }, 300);
                if (imageCount == imagePanel) { $('.image-slider #nextSlideArrow').addClass('none'); }
                $('.image-slider #prevSlideArrow').removeClass('none');
            }
        };
    });

    /* NEWSROOM MAIN CAROUSEL - 3 ITEMS */
    $('.news-slide .contentSlideBtn').live("click", function (e) {
        e.preventDefault();
        newsClick = $(this).attr('id');

        if (newsClick == 'prevSlideBtn') {
            if (newsPanel > 1) {
                newsPanel = newsPanel - 3;

                if (newsPanel < 2) {
                    newsParent.animate({ marginLeft: '0' }, 700);
                    newsPanel = 1;
                } else {
                    newsParent.animate({ marginLeft: '+=' + newsWidth * 3 + 'px' }, 700);
                }

                if (newsPanel <= 1) { $('#prevSlideBtn').addClass('none'); }
                $('#nextSlideBtn').removeClass('none');
            }
        } else if (newsClick == 'nextSlideBtn') {
            if (newsPanel < newsCount) {
                newsPanel = newsPanel + 3;

                if (newsCount - newsPanel < 3) {
                    newsMath = newsCount - newsPanel + 1;
                    newsParent.animate({ marginLeft: '-=' + newsWidth * newsMath + 'px' }, 700);
                    newsPanel = newsPanel - 3 + newsMath;
                } else {
                    newsParent.animate({ marginLeft: '-=' + newsWidth * 3 + 'px' }, 700);
                }

                if (newsCount - newsPanel < 3) { $('#nextSlideBtn').addClass('none'); }
                $('#prevSlideBtn').removeClass('none');
            }
        };
    });

    /* INVEST CAROUSEL - 1 ITEM - BOTTOM NAVIGATION */
    $('.investors-slide .contentSlideBtn').live("click", function (e) {
        e.preventDefault();
        slideClick = $(this).attr('id');

        if (slideClick == 'prevSlideArrow') {
            if (investPanel > 1) {
                investPanel--;
                investParent.animate({ marginLeft: '+=' + investWidth + 'px' }, 600);
                if (investPanel <= 1) { $('.investors-slide #prevSlideArrow').addClass('none'); }
                $('.investors-slide #nextSlideArrow').removeClass('none');
                currentItem = investPanel - 1;
                $('.investors-slide .btn-row a').removeClass('current');
                $('.investors-slide .btn-row a:nth-child(' + currentItem + ')').addClass('current');
            }
        } else if (slideClick == 'nextSlideArrow') {
            if (investPanel < investCount) {
                investPanel++;
                investParent.animate({ marginLeft: '-=' + investWidth + 'px' }, 600);
                if (investCount == investPanel) { $('.investors-slide #nextSlideArrow').addClass('none'); }
                $('.investors-slide #prevSlideArrow').removeClass('none');
                currentItem = investPanel - 1;
                $('.investors-slide .btn-row a').removeClass('current');
                $('.investors-slide .btn-row a:nth-child(' + currentItem + ')').addClass('current');
            }
        };
    });
    $('.btn-row a').live("click", function (e) {
        e.preventDefault();
        thisIndex = $(this).index();
        thisItem = $(this).index() + 1;
        $('.btn-row a').removeClass('current');
        $(this).addClass('current');

        if (thisItem < investPanel) {
            investParent.animate({ marginLeft: '-' + investWidth * thisItem + 'px' }, 600);
            investPanel = thisItem + 1;
        } else if (thisItem >= investPanel) {
            investParent.animate({ marginLeft: '-' + investWidth * thisItem + 'px' }, 600);
            investPanel = thisItem + 1;
        }
        $('.investors-slide .contentSlideBtn').removeClass('none');
        if ($(this).is(':last-child')) {
            $('.investors-slide #nextSlideArrow').addClass('none');
        };
    });


    /*  ================================================
    FAQs ACCORDION & SITEMAP ACCORDION
    ================================================  */

    $('.accordion li a.question').live("click", function (e) {
        e.preventDefault();
        if ($('body').hasClass('about')) {
            hPosition = '-590px';
        } if ($('body').hasClass('careers')) {
            hPosition = '0';
        } if ($('body').hasClass('investors')) {
            hPosition = '-1182px';
        };
        if ($(this).hasClass('isOpen')) {
            $(this).removeClass('isOpen');
            if (isIE7) {
                $(this).next('.answer').hide();
            } else {
                $(this).next('.answer').fadeOut(300);
            }
            $('.question').css('background-position', +hPosition + ' -719px');
        } else {
            if (isIE7) {
                $('.answer').hide();
            } else {
                $('.answer').fadeOut(100);
            }
            $('a.question').removeClass('isOpen');
            $(this).addClass('isOpen');
            $('.question').css('background-position', +hPosition + ' -719px');
            if (isIE7) {
                $(this).next('.answer').delay(100).show().end()
                .css('background-position', +hPosition + ' -764px');
            } else {
                $(this).next('.answer').delay(100).fadeIn('300', 'linear').end()
	            .css('background-position', +hPosition + ' -764px');
            }
        }
    });

    $('.sitemap .show').live("click", function (e) {
        e.preventDefault();
        if ($(this).hasClass('isOpen')) {
            $(this).parent('li').find('ul').fadeOut(300);
            $(this).removeClass('isOpen').css('background-position', '-590px -719px');
        } else {
            $(this).parent('li').find('ul').fadeIn(300);
            $(this).addClass('isOpen').css('background-position', '-590px -764px');
        }
    });

    $('ul.sitemap li ul').each(function (i, items_list) {
        $(items_list).find('li').each(function (j) {
            $(items_list).parent('li').find('.arrow').addClass('show');
        })
    });




    /*  ================================================
    BIO ACCORDION
    ================================================  */

    $('http://www.hormelfoods.com/Sites/HormelFoods/js/a.bio').live("click", function (e) {
        e.preventDefault();
        if ($(this).hasClass('open')) {
            $('.hide').slideUp(200).fadeOut(200);
            $(this).removeClass('open');
        } else {
            $('http://www.hormelfoods.com/Sites/HormelFoods/js/a.bio').removeClass('open');
            $('.hide').slideUp(200).fadeOut(200);
            $(this).next('.hide').slideDown(300).fadeIn(300).end()
	    .addClass('open');
        }
    });
    $('a.close-bio').live("click", function (e) {
        e.preventDefault();
        $(this).parent('.hide').slideUp(300).fadeOut(300);
        $('http://www.hormelfoods.com/Sites/HormelFoods/js/a.bio').removeClass('open');
    });


    /*  ================================================
    PLACEHOLDER TEXT FIX
    ================================================  */

    if (!Modernizr.input.placeholder) {
        $("input[placeholder]").each(function () {
            var placeholder = $(this).attr("placeholder");

            $(this).val(placeholder).focus(function () {
                if ($(this).val() == placeholder) {
                    $(this).val("")
                }
            }).blur(function () {
                if ($(this).val() == "") {
                    $(this).val(placeholder)
                }
            });
        });
    }


    /*  ================================================
    PRODUCT LOCATOR DISPLAY
    ================================================  */

    $('.zip-locator').keydown(function (e) {
        if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9 || e.keyCode == 27 ||
            (e.keyCode == 65 && e.ctrlKey === true) || // Allow: Ctrl+A
            (e.keyCode >= 35 && e.keyCode <= 39)) { // Allow: home, end, left, right
            // let it happen, don't do anything
            return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        }
    });
    var theUPC;
    var theText;
    $('#input_UPC').keyup(function (e) {
        if (e.keyCode == 40 || e.keyCode == 38) { } else {
            setTimeout(function () {
                thisValue = $('#input_UPC').val();
                $('#productDrop').delay(400).fadeIn(200);
                //myPath = 'http://proofpoc.cloudapp.net/api/v1.0/Product/Search/' + thisValue + '';
                myPath = 'https://ws.hormelfoods.com/api/v1.0/Product/Search/' + thisValue + '';
                template = 'productsTemplate';
                holder = 'productDrop';
                custom = 'highlightItem';
                runAjax(myPath, template, holder);
            }, 300);
        }
    });


    $('form').keydown(function (e) {
        if (e.keyCode == 40) {
            if ($('div.dropDown.current').is(':last-child')) {
            } else {
                $('div.dropDown.current').removeClass('current').next('div.dropDown').addClass('current');
            }
        }
        if (e.keyCode == 38) {
            if ($('div.dropDown.current').is(':first-child')) {
            } else {
                $('div.dropDown.current').removeClass('current').prev('div.dropDown').addClass('current');
            }
        }
    });


    $('#productDrop div').live("click", function (e) {
        e.preventDefault();
        theUPC = $(this).attr('id');
        theText = $(this).html();
        $('#productDrop div').css('background', '#ffffff');
        $(this).css('background', '#dab850');
        $('select#productName').html('');
        $('select#productBrand option:first').attr('selected', 'selected');
        $('.search-text').text(theText);
        $('.productName-cont').css('color', '.333');
        $('#input_UPC').removeClass('off')
    });

    var locator_is_active = 0;


    function generateMap(storesArray, markersArray, miles, zip, map, infoWindow, bounds, locator_is_active) {

        var sideBarHtml = '';
        var distance_number = '';
        var storeFilterCount = 0;
        var icons = [];

        miles = parseFloat(miles, 10);
        var storesLength;
        if (storesArray.length >= 26) {
            storesLength = 26;
        } else {
            storesLength = storesArray.length;
        }

        for (var k = 0; k < storesLength; k++) {
            var point = new google.maps.LatLng(
                parseFloat(storesArray[k].Latitude),
                parseFloat(storesArray[k].Longitude)
            );
            var name = storesArray[k].Name;
            var zip_display = storesArray[k].Zip.substring(0, 5);
            var html = "<div class='infoWindow'><span class='locationTitle'>" + name + "</span><span class='locationAddress1'>" + storesArray[k].Address + "</span><span class='locationAddress2'>" + storesArray[k].City + ", " + storesArray[k].State + " " + zip_display + "</span></div>";
            var letter = String.fromCharCode("A".charCodeAt(0) + k);
            var marker = new google.maps.Marker({
                map: map,
                position: point,
                icon: '/img/map_leaf_' + letter + '.png',
                shadow: '../../../img/map_leaf_shadow.png'/*tpa=http://www.hormelfoods.com/img/map_leaf_shadow.png*/,
                printImage: 'http://maps.google.com/mapfiles/marker' + letter + 'ie.gif',
                mozPrintImage: 'http://maps.google.com/mapfiles/marker' + letter + 'ff.gif',
                title: name
            });
            sideBarHtml += '<li><a href="#" data-marker="' + k + '" class="redlink">' + storesArray[k].Name + '</a><br /><span>' + storesArray[k].DistanceDisplay + '</span></li>';
            markersArray[locator_is_active].push(marker);
            bindInfoWindow(marker, map, infoWindow, html);
            bounds.extend(point);
            storeFilterCount++;

            if (k == storesLength - 1) {
                var listener = google.maps.event.addListener(map, "idle", function () {
                    if (map.getZoom() > 16) map.setZoom(14);
                    google.maps.event.removeListener(listener);
                });
                generateSidebar();
            }
        }

        $('.product-list a').live('click', function (e) {
            e.preventDefault();
            mid = $(this).data('marker');
            for (var t = 0; t < markersArray[locator_is_active].length; t++) {
                if (t == mid) {
                    var selected_marker = markersArray[locator_is_active][t];
                    google.maps.event.trigger(map, 'resize');
                    google.maps.event.trigger(selected_marker, "click");
                }
            }
        });
        function generateSidebar() {
            map.fitBounds(bounds);
            // GENERATE SIDEBAR LIST //
            $('.product-list').html('').html(sideBarHtml);
            $('.product_title').text(theText);
            $('.miles_cont').text(miles);
            $('.zip_cont').text(zip);
            $('#results-list p').add('.product-list').fadeIn();
            $('#results-list p').css('opacity', '1');
            $('#results-list .no-results').css('display', 'none');

            $('.miles_cont').html(miles);
            $('#locator-results').addClass('dataLoaded');
            $('.locator-results').fadeIn(600);
            $('#googleMap, #locator-results').fadeIn(600);
            storesArray = '';
        }
    }

    function bindInfoWindow(marker, map, infoWindow, html) {
        google.maps.event.addListener(marker, 'click', function () {
            infoWindow.setContent(html);
            infoWindow.open(map, marker);
        });
    }

    function numbersToLetters(num) {
        num--;
        var letters = '';
        while (num > 0) {
            letters = String.fromCharCode(97 + (num % 26)) + letters;
            letters = letters.toUpperCase();
            num = Math.floor(num / 26);
        }
        return letters;
    }

    /* END MAP FUNCTIONALITY */



    $('#productDrop .dropDown').live("click", function () {
        thisItem = $(this).html();
        $('input#input_UPC').val(thisItem);
    });

    $('form').keydown(function (e) {
        if ($('#input_UPC').is(":focus") && e.keyCode == 13) {
            $('#productDrop .dropDown.current').click();
        } else if (e.keyCode == 13) {
            $(this).closest('form').submit();
        }
    });

    $("select#productBrand").change(function () {
        valueUPC = $("select#productBrand option:selected").attr('id');
        //myPath = 'http://proofpoc.cloudapp.net/api/v1.0/Product/Brand/' + valueUPC;
        //myPath = 'http://productlocator.infores.com/productlocator/products/products.pli?client_id=70&brand_id=HRML&prod_lvl=group&' + valueUPC;
        mypath = 'http://productlocator.infores.com/productlocator/products/products.pli?client_id=70&brand_id=HRML&group_id=' + valueUPC;

        template = 'productNameTemplate';
        holder = 'productName';
        runAjax(myPath, template, holder);
        custom = 'productChange';

        $('.productName-cont').css('color', '#333');
        $('#input_UPC').val('');
        $('#input_UPC').attr('placeholder') == 'Enter Product or UPC Code...';
        $('#productDrop').fadeOut(200);
    });

    $("select#productName").change(function () {
        theText = $('select#productName option:selected').text();
        $('.search-text').text(theText);
    });



    /*  ================================================
    PRODUCT LOCATOR DISPLAY
    ================================================  */

    // $('.brand-wall .inner').css('height','10px').animate({height: '100%'}, 300, function(){  });
    // if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1) { bounce = 10; } else if (isIE) { bounce = 9; } else { bounce = 70; };
    $('.brand-wall a.grid').css('opacity', '0').end()
    .find('.grid:nth-child(3n)').delay(300).animate({ opacity: '1' }, 700, function () { }).end() /* EVERY 3 */
    .find('.grid:nth-child(5n-3)').delay(450).animate({ opacity: '1' }, 700, function () { }).end() /* EVERY 5 & 2 */
    .find('.grid:nth-child(4n-3)').delay(600).animate({ opacity: '1' }, 700, function () { }).end() /* EVERY 4 & 1 */
    .find('a.grid:nth-child(2n)').delay(800).animate({ opacity: '1' }, 700, function () { }); /* EVERY 2 */
    $('.brand-wall a.grid').delay(1000).animate({ opacity: '1' }, 700, function () {  /* The Rest */
        $('.brand-wall a.grid').addClass('bouncey');
    });

    //$(".brand-wall a.grid.bouncey").live("hover", function () {
    //    if ($(this).hasClass('hormel')) {
    //        // DO NOTHING
    //    } else {
    //        $(this).dequeue().stop().animate({ 'marginTop': '-=3px', 'marginBottom': '+=3px' }, bounce, function () {
    //            $(this).animate({ 'marginTop': '+=5px', 'marginBottom': '-=5px' }, bounce, function () {
    //                $(this).animate({ 'marginTop': '-=2px', 'marginBottom': '+=2px' }, bounce);
    //            });
    //        });
    //    }
    //}, function () {
    //    if ($(this).hasClass('hormel')) {
    //    } else {
    //        $(this).dequeue().stop().animate({ 'marginTop': '+=2px', 'marginBottom': '-=2px' }, bounce, function () {
    //            $(this).animate({ 'marginTop': '-=3px', 'marginBottom': '+=3px' }, bounce, function () {
    //                $(this).animate({ 'marginTop': '+=1px', 'marginBottom': '-=1px' }, bounce);
    //            });
    //        });
    //    }
    //});

    $('.view-brands').live("click", function (e) {
        e.preventDefault();
        if ($(this).hasClass('allBrands')) {
            $(this).removeClass('allBrands');
            $('.brand-wall .logos').delay(200).fadeIn('400');
            $('.brand-wall #brand-columns').fadeOut('700');
            $(this).html('View all brands <img src="../../../img/hm-slide-smArrow.png"/*tpa=http://www.hormelfoods.com/img/hm-slide-smArrow.png*/ alt="">');
        } else {
            $(this).addClass('allBrands');
            $('.brand-wall .logos').fadeOut('400');
            $('.brand-wall #brand-columns').delay(200).fadeIn('700');
            $(this).html('View featured brands <img src="../../../img/hm-slide-smArrow.png"/*tpa=http://www.hormelfoods.com/img/hm-slide-smArrow.png*/ alt="">');
        }
    });


    /*  ================================================
    PRODUCT LOCATOR SEARCH BOX, LEFT NAV
    ================================================  */

    $('#search-locator').live("click", function (e) {
        e.preventDefault();
        var searchProduct, searchZip, searchUrl;
        //searchProduct = $('#productNameList option:selected').attr('value');
        searchProduct = $('#linkboxcontent_0_drpProdGroup option:selected').attr('value');
        searchZip = $('#zip-locator').val();
        searchBrand = $('#brand-locator').attr('class');
        if ($('#productNameList option:selected').attr('value') == '' && searchZip == '') {
            $('#productNameList').css('border-color', 'red');
            return false;
        } else if ($('#productNameList option:selected').attr('value') == '' && searchZip != '') {
            $('#productNameList').css('border-color', 'red');
            $('#zip-locator').css('border-color', '');
            return false;
        } else if ($('#productNameList option:selected').attr('value') != '' && searchZip == '') {
            $('#productNameList').css('border-color', '');
            $('#zip-locator').css('border-color', 'red');
            return false;
        } else if (searchZip == '' || searchZip.length < 5) {
            $('#productNameList').css('border-color', '');
            $('#zip-locator').css('border-color', 'red');
            return false;
        } else {
            $('#productNameList').css('border-color', '');
            $('#zip-locator').css('border-color', '');
            var Text = searchBrand;
            //Text = Text.replace(/ /g, '');
            //Text = Text.replace(/'/g, '');
            searchBrand = Text;
            //window.location = 'http:///' + document.domain + '/HormelFoods_New/brands-product-locator.html?UPC=' + searchProduct + '&ZIP=' + searchZip + '&BRAND=' + searchBrand;
            window.location = 'http:///' + document.domain + '/Brands/Product-Locator.aspx?UPC=' + searchProduct + '&ZIP=' + searchZip + '&BRAND=' + searchBrand;
        }
    });


    (function ($) {
        $.getQuery = function (query) {
            query = query.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
            var expr = "[\\?&]" + query + "=([^&#]*)";
            var regex = new RegExp(expr);
            var results = regex.exec(window.location.href);
            if (results !== null) {
                return results[1];
                return decodeURIComponent(results[1].replace(/\+/g, " "));
            } else {
                return false;
            }
        };
    })(jQuery);

    function productNameChange() {
        $('#productName').animate({}, 0, function () {
            $('#productName').find("option[value='" + q1 + "']").attr("selected", true).animate({}, 0, function () {
                $('.sbottom input.redBtn').click();
            });
        })
        $('.zip-locator').removeClass('automated');
    }



    /*  ================================================
    ALL BRANDS PAREMETER
    ================================================  */

    if (window.location.href.indexOf("?view=all") > -1) {
        var b1 = $.getQuery('view');
        $('a.view-brands').addClass('allBrands').html('View featured brands <img src="../../../img/hm-slide-smArrow.png"/*tpa=http://www.hormelfoods.com/img/hm-slide-smArrow.png*/ alt="">');
        $('.brand-wall .logos').css('display', 'none');
        $('.brand-wall #brand-columns').css('display', 'block');
    };
    if (window.location.href.indexOf("?view=all") < 0) {
        $('.sub-nav a.all-brands').removeClass('current');
    }



    /*  ================================================
    HOMEPAGE SELECT SITE OVERLAY
    ================================================  */

    var cookieName = 'HormelFoodsVisited';
    $(function () {
        checkCookie();
    });

    function checkCookie() {
        if (document.cookie.length > 0 && document.cookie.indexOf(cookieName + '=') != -1) {
            // do nothing, cookie already sent
        } else {
            $('#selectSite').delay(1500).animate({ top: '111px' }, 700, function () { });
            // document.cookie = cookieName + "=1";  // set the cookie to show user has already visited
            var expires = new Date();
            expires.setTime(expires.getTime() + 1000 * 60 * 60 * 24 * 60); // 60 Days
            document.cookie = cookieName + '=1' + ';expires=' + expires.toUTCString();
        }
    }

    var cookieName = 'HormelFoods_SkippyBrandes';
    $(function () {
        checkCookie();
    });

    function checkCookie() {
        if (document.cookie.length > 0 && document.cookie.indexOf(cookieName + '=') != -1) {
            // do nothing, cookie already sent
        } else {
            $('.cont .inner').fadeOut(800);
            $('#selectSite').delay(1200).animate({ top: '111px' }, 800, function () { });
            // document.cookie = cookieName + "=1";  // set the cookie to show user has already visited
            var expires = new Date();
            expires.setTime(expires.getTime() + 1000 * 60 * 60 * 24 * 60); // 60 Days
            document.cookie = cookieName + '=1' + ';expires=' + expires.toUTCString();
        }
    }


    /*  ================================================
    BLUR SELECT DROPS AFTER SELECTION - IE BACKGROUND COLOR
    ================================================  */

    $("select").change(function () {
        $(this).blur();
    });


    /*  ================================================
    SUBNAV WIDTH FIX FOR IE7
    ================================================  */

    if (isIE7) {
        var accum_width = 0;
        $('.sub-nav ul.current').find('li').each(function () {
            accum_width += $(this).width();
        });
        $('.sub-nav ul.current').width(accum_width);
    }


    /*  ================================================
    JOB SEARCH FORM
    ================================================  */

    $('#job-search').submit(function (e) {
        e.preventDefault();
        var searchItem = $('#careers-search').val();
        var searchURL = $('#job-search').attr('action');
        if (searchItem == '' || searchItem == 'Search By Keywords') {
            $('#careers-search').css('border-color', 'red').focus();
            return false;
        } else {
            $('#careers-search').css('border-color', '');
        }
        window.location = searchURL + searchItem;
    });


    /*  ================================================
    VARIOUS FORM VALIDATIONS
    ================================================  */

    $('.home .submit-btn').live("click", function (e) {
        e.preventDefault();
        var joinus = $('.joinus').val();
        if (joinus == '' || joinus == 'E-mail') {
            $('.joinus').css('border-color', 'red').focus();
        } else if (!isValidEmailAddress(joinus)) {
            $('.joinus').css('border-color', 'red').focus();
        } else {
            $('.join form').submit();
            $('.joinus').css('border-color', '');
        }
    });

    $('.search .content .search-icon').live("click", function (e) {
        e.preventDefault();
        var searchtext = $('.search .content #searchtext').val();
        if (searchtext == '' || searchtext == 'Search') {
            $('.search .content #searchtext').css('border-color', '#f83f00').focus();
            return false;
        } else {
            $('.search .content form').submit();
            $('.search .content #searchtext').css('border-color', '');
        }
    });

    $('#search .search-icon').live("click", function (e) {
        e.preventDefault();
        var navSearchtext = $('#search .search').val();
        if (navSearchtext == '' || navSearchtext == 'Search') {
            $('#search .search').css('border-color', '#f83f00').focus();
            return false;
        } else {
            $('#search .search').css('border-color', '');
            $('#search form').submit();
        }
    });

    $('#leftsectioncontent_0_field_E3E87504F4CC45B5936CCB09D4C1C2ED').blur(function () {
        var emailValueOne = $('#leftsectioncontent_0_field_9F0046954D5C4529AD5FE2F1104C18DD').val();
        var emailValueTwo = $(this).val();
        if (emailValueOne != emailValueTwo) {
            $('.emailError').remove();
            $('<span class="emailError"><i>Your Emails Must Match</i></span>').insertAfter($('#leftsectioncontent_0_field_E3E87504F4CC45B5936CCB09D4C1C2ED'));
            $(this).css('border', 'red');
        } else {
            $('.emailError').remove();
            $(this).css('border', '');
        }
    });

    $('.search-box .search-icon').live("click", function (e) {
        e.preventDefault();
        var searchtext = $('.search-box .search').val();
        if (searchtext == '' || searchtext == 'Search') {
            $('.search-box .search').css('border-color', '#f83f00').focus();
            return false;
        } else {
            $('.search-box .search').css('border-color', '');
            $('.search-box form').submit();
        }
    });
    $('input[type=text]').blur(function () {
        $(this).css('border-color', '');
    });

    function isValidEmailAddress(emailAddress) {
        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
        return pattern.test(emailAddress);
    };


    /*  ================================================
    FIX FOOTER TO BOTTOM
    ================================================  */


    if ($('body').hasClass('four-four')) {
        setFooter();
    };

    function setFooter() {
        if (windowHeight > bodyHeight - 80) {
            $('footer').css({
                position: 'absolute',
                bottom: '0',
                left: '0',
                width: '100%'
            });
        } else {
            $('footer').css({
                position: 'relative',
                bottom: '',
                left: '',
                width: ''
            });
        }
    }


    /*  ================================================
    CONTACT FORM MAX LENGTH FIX
    ================================================  */

    $('.scfMultipleLineTextBox').each(function () {
        $(this).attr('maxlength', '4096');
    });
    $('.scfEmailTextBox').each(function () {
        $(this).attr('maxlength', '75');
    });

    $('.scfMultipleLineTextBox').keypress(function (e) {
        var lengthF = $(this).val();

        if (lengthF.length > 4096) {
            e.preventDefault();
        }
    });



    /*  ================================================
    PIE FIX
    ================================================  */


    if (window.PIE) {
        $('li.corner').each(function () {
            PIE.attach(this);
        });
        $('.sub-nav').each(function () {
            PIE.attach(this);
        });
        $('#links').each(function () {
            PIE.attach(this);
        });
        $('#search').each(function () {
            PIE.attach(this);
        });
        $('.radius-all-2').each(function () {
            PIE.attach(this);
        });
        $('.radius-all-3').each(function () {
            PIE.attach(this);
        });
        $('.radius-all-4').each(function () {
            PIE.attach(this);
        });
        $('.radius-all-6').each(function () {
            PIE.attach(this);
        });
        $('#sliderArea li .inner').each(function () {
            PIE.attach(this);
        });
        $('.grid-1.hdr').each(function () {
            PIE.attach(this);
        });
        $('.grid-3.hdr').each(function () {
            PIE.attach(this);
        });
        $('.grid-content').each(function () {
            PIE.attach(this);
        });
        $('.grid .wrap').each(function () {
            PIE.attach(this);
        });
        $('.item-list .pic').each(function () {
            PIE.attach(this);
        });
        $('.columns.grids .product-content h2').each(function () {
            PIE.attach(this);
        });
        $('#vidCont').each(function () {
            PIE.attach(this);
        });
        $('.columns.grids h2.itemlist-title').each(function () {
            PIE.attach(this);
        });
        $('.brand-wall.inner').each(function () {
            PIE.attach(this);
        });
        $('.product-content.full .inner').each(function () {
            PIE.attach(this);
        });
    }

    /*  ================================================
    PRODUCT GRID FIX SIZE - OUTSIDE OF DOC.READY
    ================================================  */

    function product_grid_size() {
        //setTimeout(function () {

        var max_height = 60;
        var large_height = 0;
        var images = null;
        var setHeight;

        $('.prod img').each(function () {
            var cur_height = $(this).height();
            if (cur_height > max_height) {
                max_height = cur_height;
                images = this;

            } else {
                // DO NOTHING
            }
        });

        setTimeout(function () {
            setHeight = $(images).height();
            $('.product-grid .prod').css('height', setHeight + 48 + 'px');
            $('.product-grid .prod a').css('height', setHeight + 4 + 'px');
            $('.prod img').each(function () {
                var this_height = $(this).height();
                getPadding = setHeight - this_height;
                $(this).css('paddingTop', getPadding / 2 + 'px');
            });
        }, 500);
        //}, 200);
    };
});

function searchlocator() {
    var search_url = "http://productlocator.infores.com/productlocator/servlet/ProductLocator?clientid=70&productfamilyid=HRML&template=keg_nl.xsl&producttype=upc";
    //var search_url = "http://productlocator.infores.com/productlocator/servlet/ProductLocatorEngine?clientid=70&productfamilyid=HRML&producttype=upc";
    search_url += "&productid=" + document.getElementById('product').value;
    search_url += "&zip=" + document.getElementById('zip').value;
    search_url += "&searchradius=" + document.getElementById('searchradius').value;
    document.getElementById('results').src = search_url;
}

function showStores(thisform) {
    if (validateZip(thisform) == true) {
        thisform.submit();
    }
}

function validateZip(thisform) {
    returnVal = true;
    found = thisform.zip.value.search(/[^0-9]/);
    if (found != -1) {
        alert("Please enter a valid zip code.");
        thisform.productid.focus();
        returnVal = false;
    }
    if (document.keg.zip.value == "") {
        alert("Please enter a valid zip code.");
        returnVal = false;
        thisform.zip.focus();
    }
    return returnVal;
}


function isValidZip(zip) {
    return /(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zip);
}

// (START)********************* PAGE FUNCTIONS(drop downs and page elements) **************************** //
jQuery(document).ready(function () {
    // ------------- Mile Drop Down(start)
    var dropShown = false;
    var milesSelected = false;

    function doDropToggle() {
        if (dropShown == false) {
            jQuery('#MileDropList').slideDown('fast', function () {
                jQuery('body').bind('click', doDropToggle);
            });
            dropShown = true;
        }
        else {
            jQuery('#MileDropList').slideUp('fast');
            dropShown = false;
            jQuery('body').unbind('click', doDropToggle);
        }
        milesSelected = true;
    }

    jQuery('http://www.hormelfoods.com/Sites/HormelFoods/js/input.drop').click(function (event) { doDropToggle(); event.stopPropagation(); });
    jQuery('#MileDropList li').click(function () {
        jQuery('http://www.hormelfoods.com/Sites/HormelFoods/js/input.drop').val(jQuery(this).html());
        doDropToggle();
    });

    //Not sure what this is for?
    jQuery('#MileDropList li').blur(doDropToggle);
    // ------------- Mile Drop Down(end)


    // ------------- Submit Button(start)
    jQuery('.btnGo').click(function () {
        if (!isValidZip(jQuery(".txtZip").val()) || jQuery(".txtZip").val() == jQuery(".txtZip").attr('title')) {//Zip is NOT valid.
            jQuery('input.txtZip').addClass('error');
            alert('Please enter a valid Zip Code');
        }
        else {//Zip is valid.
            //open results page, need location information here.
            doZipLoc(jQuery(".txtZip").val());
        }
    });
    // ------------- Submit Button(end)


    //Dynamic Background for List of Stores
    jQuery('.storeLocation:odd').css('background-color', '#f2f2f2');
});
// (END)********************* PAGE FUNCTIONS(drop downs and page elements) **************************** //


// (START)********************* GOOGLE MAPS v3 API **************************** //
//Start Creating the Map
function initialize() {
    //starting location
    // var myLatlng = new google.maps.LatLng(queryString('lat'), queryString('long'));
    var myOptions = {
        zoom: 9,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }; //setup various map options
    var map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);    //draws the map in indicated element


    //(start)Build Markers
    var geocoder = new google.maps.Geocoder();
    var address;
    var marker;

    var i = parseInt(startNum);    //figures out what the starting number is going to be (this is to handle paging)
    var flags = [];
    //Loops through each of the storeLocation classes extracting the address from the p tags (probably don't need the index variable)
    var geoCounter = 0;

    jQuery('.storeLocation').each(function (index) {
        flags[index] = i;
        address = jQuery(this).find('p').text();   //grab the address from the p tag

        geocoder.geocode({ 'address': address }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {  //build and insert the marker
                if (geoCounter == 0)
                    map.setCenter(results[0].geometry.location);
                if (jQuery('#slResultHeader h3').html() != '0 Results') {
                    var marker = new google.maps.Marker({
                        position: results[0].geometry.location,
                        map: map//,
                        /*icon: '/App_Themes/UncleBensAssets/SiteImages/StoreLocator/loc' + flags[index] + '.png'*/
                    });
                }
                geoCounter++;

            }
            else {  //failed to retrieve the geocode
                alert("An Error Occured: " + status);
            }

        });
        i++;    //increment marker letter
    });
    //(end) Build Markers
}
// (END)*********************** GOOGLE MAPS v3 API **************************** //


// (START)********************* OTHER FUNCTIONS **************************** //
//Used to get values from the query string.
function queryString(key) {//Searches the query string for the supplied KEY.
    var re = new RegExp("[?&]" + key + "=([^&$]*)", "i");
    var offset = location.search.search(re);
    if (offset == -1) return null;
    return RegExp.$1;
}


function getMiles() {
    //var str = jQuery('input[id$=WTBMile]').val();
    var str = jQuery('input[id$=SFMile]').val();
    var miles = str.replace(' miles', '');
    return parseInt(miles);
}


function doZipLoc(zip) {//Use the provided zip to retrieve lat and lng.
    geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': zip }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            window.location.href = 'http://www.hormelfoods.com/store-locator/results.aspx?lat=' + results[0].geometry.location.lat() + '&long=' + results[0].geometry.location.lng() + '&miles=' + getMiles() + '&zip=' + zip;
        }
        else {
            jQuery('#StoreFinder input.field').addClass('error');
        }
    });
}


function showUpdateProgress() {
    jQuery('div[id$=PageUpdateProgress]').show();
    jQuery('div#ProgressLoader').show();
}


function hideUpdateProgress() {
    jQuery('div[id$=PageUpdateProgress]').hide();
    jQuery('div#ProgressLoader').hide();
}


//(start) GeoLocation Button (using W3C Geolocation, which is the preffered method)
function doGeoLoc() {
    navigator.geolocation.getCurrentPosition(function (position) {
        window.location.href = 'http://www.hormelfoods.com/store-locator/results.aspx?lat=' + position.coords.latitude + '&long=' + position.coords.longitude + '&miles=' + getMiles();
    });
}
//(end) GeoLocation Button
// (END)********************* OTHER FUNCTIONS **************************** //


