/**
 * @author sjosephraj
 * @Created:  22 July 2009
 * @Purpose: This function adds "Page From"'s parameters (Content Group, SubContent Group and Audience)  in the queryString
 * The needed WebForm names are added in the function as string literals.
 * The reason why this funtion in a seperate file is in the future if more Webforms falls under this category, we need
 * to update this one file with just this one function rather dojomain.js or metlifeJS.js.
 *
 * Current URLs:  DEV:  business/swathi/segments/13jul09/blankpage.html
 *                QA:   individual/insurance/webforms/eleadform.html
 *                PROD: individual/contact-metlife-representative/index.html
 * (from Connie Bao Hsu as of 24 July).
 */
function addParamToQSToPopulatePageForm(url){
	var URL1 = url;


	if ((URL1.search("https://www.metlife.com/system/js/business/swathi/segments/13jul09/blankpage.html")> 0) ||
		(URL1.search("https://www.metlife.com/system/js/individual/insurance/webforms/eleadform.html")> 0) ||
		(URL1.search("https://www.metlife.com/system/js/individual/contact-metlife-representative/index.html")> 0))
    { 
        //Calculate the pagefrom value from the current page.
        url = url + "?CG=" + (typeof(contentGroupDirectory) != "undefined" ? (contentGroupDirectory != "" ? contentGroupDirectory : "null") : "null") + "&SCG=" + (typeof(subContentGroupDirectory) != "undefined" ? (subContentGroupDirectory != "" ? subContentGroupDirectory : "null") : "null") + "&AU=" + (typeof(audience) != "undefined" ? (audience != "" ? audience : "null") : "null");
    }
    return (url)
}

