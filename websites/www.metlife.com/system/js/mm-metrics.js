function clearMultiTrackMeta()
{
	 // Right now, clear only wt.ad values,
	 // Which are values inserted by Portal Abstraction Layer for banners.
	 metaValues = document.getElementsByTagName("meta");

	 for (var i=0; i<metaValues.length; i++) {
         if (metaValues[i].name != null)
         {
             if (metaValues[i].name == "https://www.metlife.com/system/js/wt.ad") {
                 metaValues[i].name = "";
                 metaValues[i].content = "";
			 }
		 }
		 else {
			  // alert("iteration "+i+" of metavalues: " + metaValues[i]);
		 }
	 }

	 return 1;
}
function matchTaggingName(name,nameValuePairs,returnNull)
{
	 var taggingRegExp = new RegExp(name+":([^;]*)","i");
	 var taggingMatch = taggingRegExp.exec(nameValuePairs);
	 if (taggingMatch != null) {
		  return(taggingMatch[1]);
	 }
	 else {
		  if (returnNull)
			   return("null");
		  else 
			   return("");
	 }
}

function trackMMEvent(type, value)
{
      
        var nameValuePairs = new String(value);
       
        var scenarioName = matchTaggingName("scenarioName",nameValuePairs, false);
        var scenarioStep = matchTaggingName("scenarioStep",nameValuePairs, false);
        var scenarioLead = matchTaggingName("scenarioLead",nameValuePairs, false);
   	var title = matchTaggingName("title",nameValuePairs, false);
   	var localContentGroup = matchTaggingName("cg",nameValuePairs, false);
	var localSubContentGroup = matchTaggingName("scg",nameValuePairs, false);
	
	var localz_small3  = matchTaggingName("z_small3",nameValuePairs, false);
	var localz_age  = matchTaggingName("age",nameValuePairs, false);
	var localz_gnd  = matchTaggingName("gnd",nameValuePairs, false);
	var localz_tool  = matchTaggingName("tool",nameValuePairs, false);
	var lz_num3  = matchTaggingName("premium",nameValuePairs, false);
	var lz_num4  = matchTaggingName("z_num4",nameValuePairs, false);
	
	 
        if (type == "button") {
           var buttonName = matchTaggingName("buttonName",nameValuePairs);
 
	   if (buttonName == "field-name") {
		dcsMultiTrack("DCS.dcsuri", "/"+title+".html", "https://www.metlife.com/system/js/WT.ti", title, "WT.cg_n", localContentGroup,"WT.cg_s",localSubContentGroup,"WT.si_cs", scenarioLead, "WT.si_n",scenarioName ,"WT.si_x", scenarioStep,"https://www.metlife.com/system/js/WT.ad","","https://www.metlife.com/system/js/WT.ac","","DCSext.oc_id","","WT.z_small3","","WT.z_age","","WT.z_gnd","","WT.z_tool","","WT.z_num3","","WT.z_num4","")
	   }
	   if (buttonName == "get-quote" || buttonName == "recal-quote" ) {
	   	 dcsMultiTrack("DCS.dcsuri", "/"+title+".html", "https://www.metlife.com/system/js/WT.ti", title, "WT.cg_n", localContentGroup,"WT.cg_s",localSubContentGroup,"WT.si_cs", scenarioLead, "WT.si_n",scenarioName ,"WT.si_x", scenarioStep,"WT.z_age",localz_age ,"WT.z_gnd", localz_gnd, "WT.z_small3",localz_small3 ,"WT.z_tool",localz_tool,"https://www.metlife.com/system/js/WT.ad","","https://www.metlife.com/system/js/WT.ac","","DCSext.oc_id","","WT.z_num3",lz_num3 ,"WT.z_num4","")
	   }
	    if (buttonName == "get-premium") {
	   	    dcsMultiTrack("DCS.dcsuri", "/"+title+".html", "https://www.metlife.com/system/js/WT.ti", title, "WT.cg_n", localContentGroup,"WT.cg_s",localSubContentGroup,"WT.si_cs", scenarioLead, "WT.si_n",scenarioName ,"WT.si_x", scenarioStep,"WT.z_num3","" ,"WT.z_tool","","https://www.metlife.com/system/js/WT.ad","","https://www.metlife.com/system/js/WT.ac","","DCSext.oc_id","","WT.z_small3","","WT.z_age","","WT.z_gnd","","WT.z_num4","")
	   }
	  if (buttonName == "get-submit-info") {
	  	dcsMultiTrack("DCS.dcsuri", "/"+title+".html", "https://www.metlife.com/system/js/WT.ti", title, "WT.cg_n", localContentGroup,"WT.cg_s",localSubContentGroup,"WT.si_cs", scenarioLead, "WT.si_n",scenarioName ,"WT.si_x", scenarioStep,"WT.z_num4",lz_num4 ,"WT.z_tool",localz_tool,"https://www.metlife.com/system/js/WT.ad","","https://www.metlife.com/system/js/WT.ac","","DCSext.oc_id","","WT.z_small3","","WT.z_age","","WT.z_gnd","","WT.z_num3","")
	   }
	   if (buttonName == "get-popup") {
	   	  	dcsMultiTrack("DCS.dcsuri", "/"+title+".html", "https://www.metlife.com/system/js/WT.ti", title, "WT.cg_n", localContentGroup,"WT.cg_s",localSubContentGroup,"WT.si_cs", scenarioLead, "WT.si_n",scenarioName ,"WT.si_x", scenarioStep)
	   }
        }
}

function trackIOQEvent(type, value)
{
      
        var nameValuePairs = new String(value);
       
        var scenarioName = matchTaggingName("scenarioName",nameValuePairs, false);
        var scenarioStep = matchTaggingName("scenarioStep",nameValuePairs, false);
        var scenarioLead = matchTaggingName("scenarioLead",nameValuePairs, false);
   	var title = matchTaggingName("title",nameValuePairs, false);
   	var localContentGroup = matchTaggingName("cg",nameValuePairs, false);
	var localSubContentGroup = matchTaggingName("scg",nameValuePairs, false);
	
	var localz_small3  = matchTaggingName("z_small3",nameValuePairs, false);
	var localz_age  = matchTaggingName("age",nameValuePairs, false);
	var localz_gnd  = matchTaggingName("gnd",nameValuePairs, false);
	var localz_tool  = matchTaggingName("tool",nameValuePairs, false);
	var lz_num3  = matchTaggingName("premium",nameValuePairs, false);
	var lz_num4  = matchTaggingName("z_num4",nameValuePairs, false);
	
	 
        if (type == "button") {
           var buttonName = matchTaggingName("buttonName",nameValuePairs);
 
	   if (buttonName == "field-name") {
		dcsMultiTrack("DCS.dcsuri", "/"+title+".html", "https://www.metlife.com/system/js/WT.ti", title, "WT.cg_n", localContentGroup,"WT.cg_s",localSubContentGroup,"WT.si_cs", scenarioLead, "WT.si_n",scenarioName ,"WT.si_x", scenarioStep,"https://www.metlife.com/system/js/WT.ad","","https://www.metlife.com/system/js/WT.ac","","DCSext.oc_id","","WT.z_small3","","WT.z_age","","WT.z_gnd","","WT.z_tool",localz_tool,"WT.z_num3","","WT.z_num4",lz_num4 ,"WT.z_small4",tcmPageID)
	   }
	   if (buttonName == "get-quote" || buttonName == "recal-quote" || buttonName == "no-quote") {
	   	 dcsMultiTrack("DCS.dcsuri", "/"+title+".html", "https://www.metlife.com/system/js/WT.ti", title, "WT.cg_n", localContentGroup,"WT.cg_s",localSubContentGroup,"WT.si_cs", scenarioLead, "WT.si_n",scenarioName ,"WT.si_x", scenarioStep,"WT.z_age",localz_age ,"WT.z_gnd", localz_gnd, "WT.z_small3",localz_small3 ,"WT.z_tool",localz_tool,"https://www.metlife.com/system/js/WT.ad","","https://www.metlife.com/system/js/WT.ac","","DCSext.oc_id","","WT.z_num3",lz_num3 ,"WT.z_num4",lz_num4 ,"WT.z_small4",tcmPageID)
	   }
	   if (buttonName == "get-submit-info") {
	  	dcsMultiTrack("DCS.dcsuri", "/"+title+".html", "https://www.metlife.com/system/js/WT.ti", title, "WT.cg_n", localContentGroup,"WT.cg_s",localSubContentGroup,"WT.si_cs", scenarioLead, "WT.si_n",scenarioName ,"WT.si_x", scenarioStep,"WT.z_num4",lz_num4 ,"WT.z_tool",localz_tool,"https://www.metlife.com/system/js/WT.ad","","https://www.metlife.com/system/js/WT.ac","","DCSext.oc_id","","WT.z_small3","","WT.z_age","","WT.z_gnd","","WT.z_num3","","WT.z_small4",tcmPageID)
	   }
	  
        }
}

function trackHPIOQEvent(type, value)
{
      
        var nameValuePairs = new String(value);
       
        var scenarioName = matchTaggingName("scenarioName",nameValuePairs, false);
        var scenarioStep = matchTaggingName("scenarioStep",nameValuePairs, false);
        var scenarioLead = matchTaggingName("scenarioLead",nameValuePairs, false);
   	var title = matchTaggingName("title",nameValuePairs, false);
   	var localContentGroup = matchTaggingName("cg",nameValuePairs, false);
	var localSubContentGroup = matchTaggingName("scg",nameValuePairs, false);
	
	var localz_small3  = matchTaggingName("z_small3",nameValuePairs, false);
	var localz_age  = matchTaggingName("age",nameValuePairs, false);
	var localz_gnd  = matchTaggingName("gnd",nameValuePairs, false);
	var localz_tool  = matchTaggingName("tool",nameValuePairs, false);
	var lz_num3  = matchTaggingName("premium",nameValuePairs, false);
	var lz_num4  = matchTaggingName("z_num4",nameValuePairs, false);
	var bannerId  = matchTaggingName("bannerId",nameValuePairs, false);
	
	 
        if (type == "button") {
           var buttonName = matchTaggingName("buttonName",nameValuePairs);
 
	   if (buttonName == "field-name") {
		dcsMultiTrack("DCS.dcsuri", "/"+title+".html", "https://www.metlife.com/system/js/WT.ti", title, "WT.cg_n", localContentGroup,"WT.cg_s",localSubContentGroup,"WT.si_cs", scenarioLead, "WT.si_n",scenarioName ,"WT.si_x", scenarioStep,"https://www.metlife.com/system/js/WT.ad","","https://www.metlife.com/system/js/WT.ac",bannerId,"DCSext.oc_id",bannerId,"WT.z_small3","","WT.z_age","","WT.z_gnd","","WT.z_tool",localz_tool,"WT.z_num3","","WT.z_num4",lz_num4 ,"WT.z_small4",tcmPageID)
	   }
	   if (buttonName == "get-quote" || buttonName == "recal-quote" || buttonName == "no-quote") {
	   	 dcsMultiTrack("DCS.dcsuri", "/"+title+".html", "https://www.metlife.com/system/js/WT.ti", title, "WT.cg_n", localContentGroup,"WT.cg_s",localSubContentGroup,"WT.si_cs", scenarioLead, "WT.si_n",scenarioName ,"WT.si_x", scenarioStep,"WT.z_age",localz_age ,"WT.z_gnd", localz_gnd, "WT.z_small3",localz_small3 ,"WT.z_tool",localz_tool,"https://www.metlife.com/system/js/WT.ad","","https://www.metlife.com/system/js/WT.ac",bannerId,"DCSext.oc_id",bannerId,"WT.z_num3",lz_num3 ,"WT.z_num4",lz_num4 ,"WT.z_small4",tcmPageID)
	   }
	   if (buttonName == "get-submit-info") {
	  	dcsMultiTrack("DCS.dcsuri", "/"+title+".html", "https://www.metlife.com/system/js/WT.ti", title, "WT.cg_n", localContentGroup,"WT.cg_s",localSubContentGroup,"WT.si_cs", scenarioLead, "WT.si_n",scenarioName ,"WT.si_x", scenarioStep,"WT.z_num4",lz_num4 ,"WT.z_tool",localz_tool,"https://www.metlife.com/system/js/WT.ad","","https://www.metlife.com/system/js/WT.ac","","DCSext.oc_id","","WT.z_small3","","WT.z_age","","WT.z_gnd","","WT.z_num3","","WT.z_small4",tcmPageID)
	   }
	  
        }
}

function trackPageTracker(title,category,action,optional_label) {

if (typeof(pageTracker) != "undefined") {
// pageTracker is causing page flicker in IE6, asyncronously call it using setTimeout
//pageTracker._trackPageview(title);
setTimeout("pageTracker._trackPageview('" + title + "')",0);
}
if (typeof(pageTracker) != "undefined") {
// pageTracker is causing page flicker in IE6, asyncronously call it using setTimeout
//pageTracker._trackEvent(category,action,optional_label);
setTimeout("pageTracker._trackEvent('" + category + "','" + action + "','" + optional_label + "')",0); 
}

}

function trackDART(category) {


<!-- Start of DoubleClick Spotlight Tag: Please do not remove-->
<!-- Activity Name for this tag is:TLIIQStart -->
<!-- Web site URL where tag should be placed: https://www.metlife.com/campaign/search/term-life-quote -->
<!-- This tag must be placed within the opening <body> tag, as close to the beginning of it as possible-->
<!-- Creation Date:6/19/2009 -->

var axel = Math.random()+"";
var a = axel * 10000000000000;

dartIframe = document.createElement("IFRAME");
dartIframe.setAttribute("src","https://fls.doubleclick.net/activityi;src=1524815;type=tools516;cat="+category+";ord=1;num=" + a + "?");
dartIframe.setAttribute("width","1");
dartIframe.setAttribute("height","1");	
dartIframe.setAttribute("frameborder","0");
document.body.appendChild(dartIframe);


<!-- End of DoubleClick Spotlight Tag: Please do not remove-->
}

//Added for Mobile site Track events
function trackMobileIOQEvent(type, value)
{
      
        var nameValuePairs = new String(value);
       
        var scenarioName = matchTaggingName("scenarioName",nameValuePairs, false);
        var scenarioStep = matchTaggingName("scenarioStep",nameValuePairs, false);
        var scenarioLead = matchTaggingName("scenarioLead",nameValuePairs, false);
   	var title = matchTaggingName("title",nameValuePairs, false);
   	var localContentGroup = matchTaggingName("cg",nameValuePairs, false);
	var localSubContentGroup = matchTaggingName("scg",nameValuePairs, false);
	
	var localz_small3  = matchTaggingName("z_small3",nameValuePairs, false);
	var localz_age  = matchTaggingName("age",nameValuePairs, false);
	var localz_gnd  = matchTaggingName("gnd",nameValuePairs, false);
	var localz_tool  = matchTaggingName("tool",nameValuePairs, false);
	var lz_num3  = matchTaggingName("premium",nameValuePairs, false);
	var lz_num4  = matchTaggingName("z_num4",nameValuePairs, false);
	var bannerId  = matchTaggingName("bannerId",nameValuePairs, false);
	
	 
        if (type == "button") {
           var buttonName = matchTaggingName("buttonName",nameValuePairs);
 
	   if (buttonName == "field-name") {
		dcsMultiTrack("DCS.dcsuri", "/"+title+".aspx", "https://www.metlife.com/system/js/WT.ti", title, "WT.cg_n", localContentGroup,"WT.cg_s",localSubContentGroup,"WT.si_cs", scenarioLead, "WT.si_n",scenarioName ,"WT.si_x", scenarioStep,"https://www.metlife.com/system/js/WT.ad","","https://www.metlife.com/system/js/WT.ac",bannerId,"DCSext.oc_id",bannerId,"WT.z_small3","","WT.z_age","","WT.z_gnd","","WT.z_tool",localz_tool,"WT.z_num3","","WT.z_num4",lz_num4 ,"WT.z_small4",tcmPageID)
	   }
	  /* if (buttonName == "get-quote" || buttonName == "recal-quote" || buttonName == "no-quote") {
	   	 dcsMultiTrack("DCS.dcsuri", "/"+title+".html", "https://www.metlife.com/system/js/WT.ti", title, "WT.cg_n", localContentGroup,"WT.cg_s",localSubContentGroup,"WT.si_cs", scenarioLead, "WT.si_n",scenarioName ,"WT.si_x", scenarioStep,"WT.z_age",localz_age ,"WT.z_gnd", localz_gnd, "WT.z_small3",localz_small3 ,"WT.z_tool",localz_tool,"https://www.metlife.com/system/js/WT.ad","","https://www.metlife.com/system/js/WT.ac",bannerId,"DCSext.oc_id",bannerId,"WT.z_num3",lz_num3 ,"WT.z_num4",lz_num4 ,"WT.z_small4",tcmPageID)
	   }
	   if (buttonName == "get-submit-info") {
	  	dcsMultiTrack("DCS.dcsuri", "/"+title+".html", "https://www.metlife.com/system/js/WT.ti", title, "WT.cg_n", localContentGroup,"WT.cg_s",localSubContentGroup,"WT.si_cs", scenarioLead, "WT.si_n",scenarioName ,"WT.si_x", scenarioStep,"WT.z_num4",lz_num4 ,"WT.z_tool",localz_tool,"https://www.metlife.com/system/js/WT.ad","","https://www.metlife.com/system/js/WT.ac","","DCSext.oc_id","","WT.z_small3","","WT.z_age","","WT.z_gnd","","WT.z_num3","","WT.z_small4",tcmPageID)
	   }*/
	  
        }
}