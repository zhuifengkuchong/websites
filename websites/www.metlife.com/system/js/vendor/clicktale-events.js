//define SafeClickTaleTag
function SafeClickTaleTag(tag) {
    if (typeof ClickTaleTag == 'function') {
        ClickTaleTag(tag);
    }
}


dojo.connect(document, "onclick", function (e) {
    var el;
    if (e.target) {
        el = e.target;
    }
    else if (e.srcElement) {
        el = e.srcElement;
    }
    var CTname = el.name;
    var CTid = el.id;
    var CTalt = el.alt;
    //Tobacco/Nicotine - yes no
    if (CTname == "tobacco") {
        var CTidtemp = CTid.replace(/\-/g, " ");
        SafeClickTaleTag(CTidtemp);
    }
    //Sign in option
    if (CTname == "signinOption") {
        SafeClickTaleTag("Sign In Option Dropdown interacted");
    }
    //Get instant quote.
    if (CTid == "submitBtnImage") {
        SafeClickTaleTag("Get Instant Quote Pressed");
    }
    //buy online now
    if (CTalt == "ResultCtaBuyOnlineSSP") {
        SafeClickTaleTag("Buy Online Now");
    }
});

//Continue on buy online now
dojo.connect(dojo.query("form#requestFormRightNav1 input[alt='Submit']")[0], "onclick", function (evt) {
    if (this != window) {
        SafeClickTaleTag("Continue on Buy Online Now");
    }
});

//contact me later
dojo.connect(dojo.query("div#contact-havearep a")[0], "onclick", function (evt) {
    if (this != window) {
        SafeClickTaleTag("Contact me Later");
    }
});

//submit on contact me later
dojo.connect(dojo.query("form#requestFormRightNav input[alt='Submit']")[0], "onclick", function (evt) {
    if (this != window) {
        SafeClickTaleTag("Submit Info on Contact me Later");
    }
});