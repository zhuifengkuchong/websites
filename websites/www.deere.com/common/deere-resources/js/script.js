var ie = $(document.documentElement).hasClass('ie');
var ie7 = '';
var ie8 = '';
var ie9 = '';
var iframe = false;
var transitionTime = 5000;
var getTimeout = 5000;
var rotationTime = 5000; /*artf1212946 Added by Durgesh to control the rotation time of Slides*/
var langApply = "Apply";

var pdf;

var countryName = "United States/Canada";
var localeURL = "//www.deere.com/"; /*DD artf1221188 : Configure different links for different locales*/
var localeMYJDURL = "/wps/myportal/myjd/myjdDashboard";/*DD artf1287873*/

var region_homepage_link; // Created Global var for Global Region Homepage
var updated_cookies_agreement_content; //  Created Global var for Update Cookies Agreement Content | Global Region Homepage
/*DD artf1254378 Added Image Gallery Variable*/
/** Used text to show Image text  */
langIMAGE = "Image";

var downloadImage = "Download Image";
var sliderMouseDown;

var isEUCookieEnabled = false;
var isCookieCurtainTrigger = false;

/** Variable used in case of translation of "OF" word */
langOF = "of";

/* Variables used in the Image Gallery (Small, Medium & Large) */
langPrev = "Previous";
langNext = "Next";
langNextLine = "Next Line";
langPrevLine = "Previous Line";
langLoading = "Loading...";
langCancel = "Cancel";
langPlay = "Play again";
langPause = "Stop it";
langClose = "Shut this down";

/** Used in ACM page */
closeValue = "Close";

generalErrorMessage = 'There was some problem serving your request. Please try again later';

FlashInstallMessage = 'The latest Flash plugin is not enabled or installed!';

/*DD For Alt & Title on Logo*/
Logo_Alt_Txt = "John Deere";

/**DD Variable to Switch Cufon on/off**/
enableCufonFont = "true";

/**DD Variables to apply Cufon**/
cufonElements = ".MOD_FO_1 h1, .MOD_FO_1 h2, .MOD_FO_1 h3, .MOD_FO_1 h4";

/** Used as default search text in search input box */
Search_Default_Txt = 'Search';

/** Used in search text in search input box */
Search_ALT_Txt = 'Search';



/*VN Govt Sales App Global Variable*/
var noContracts = "No Contract Results Available";
var contracts = 'contracts';
var contractResults = 'Contract results';
var forText = 'for';
var moreText = 'For more information visit';
var officialwebSiteText = 'official web site';	
var viewContractsBtnText = 'View all state contracts';
var noErrorText = generalErrorMessage;
var paginationOutOfText = ' out of ';
var paginationResultsText = ' results';

//var absoluteURL = window.location.hostname;

/* Shadow box wcag start*/

var shadowBoxGalleryOpen = true;

/* Shadow box wcag end*/
/* Nozzle Selector translation and other variables Start */

var  nsInches = 'inches';
var	 nsMeters = 'meters';
var nsCentiMeters = 'centimeters';
var	 nsMph = 'mph';
var	 nsGallonsAcre = 'gallons/acre';
var	 nsLbsGallon = 'lbs/gallon';
var	 nsGallons1000SqFt = 'gallons/1000 sq ft';
var	 nsKph = 'kph';
var	 nsLitersHectare = 'liters/hectare';
var  nsKgLiter = 'kg/liter';
var  nsFeet = 'feet (5 - 22)';
var  nsMeters15 = 'meters (1.5 - 7.0)';
var  nsResultNumber = 0;
var  nsResultEachTime = 5;
var  nsFieldIsRequired = 'This field is required.';
var  nsFieldValidNum = 'Please enter a valid number.';
var  nsFieldGreaterThan = 'Please enter a value greater than or equal to 0.01.';
var  nsResultMessageHeadingSingle = 'NOZZLE';
var  nsResultMessageHeadingMulti = 'NOZZLES';
var  nsResultMessageText = 'match your selections';
var  nsResultErrorMessage = 'An error has occured while processing the query.';
var  nsResultNoResultMessage = 'No results found for your query.';	
var  nsRequiredPressure = 'Required Pressure';
var  nsAtFlowRate = 'At Flow Rate';
var  nsRequiredSwath = 'Required Swath';
var  nsPressure = 'Pressure';
var  nsGroundSpeed = 'Ground Speed';
var  nsDropletSize = 'Droplet Size';
var  nsBuyNow = 'buy now';
var  nsContactADealer = 'contact a dealer';
var  nsPressureRange = 'Pressure Range';
var  nsFlowRange = 'Flow Range';
var  nsTechnology = 'Technology';
var  nsBodyMaterial = 'Body Material';
var  nsOrificeMaterial = 'Orifice Material';
var  nsSprayPattern = 'Spray Pattern';
var  nsSprayAngle = 'Spray Angle';
var  nsDriftClassification = 'Drift Classification';
var  nsLitersPerMin = 'liters/min';
var  nsGallonsPerMin = 'gallons/min';
var  nsBackToTop = 'Back To Top';
var  nsDegrees = 'Degrees';

/* Nozzle Selector translation and other variables End */

/* ACM Locale Variables Map Start */

var acmLocaleVars = {
	'pleaseSelect' : 'Please Select',
	'otherSeries' : 'Other Series',
	'otherModels' : 'Other Models',
	'noCategory' : 'No Category',
	'noSeries' : 'No Series',
	'noModel' : 'No Model',
	'noAttachments' : 'No Attachment',
	'selectCategory' : 'Select Category',
	'selectSeries' : 'Select Series',
	'selectModel' : 'Select Model',
	'addModel' : 'Add Model',
	'addModelSubmit' : 'ADD',
	'modelsToCompare' : 'MODELS TO COMPARE',
	'acmEntryPointErr' : 'No results available for current selection. Please try again.',
	'noCompatibeAttachment' : 'No compatible attachments or implements were found for selected model. Please try again.',
	'viewOnly' : 'View only:',
	'clear': 'Clear',
	'allAttachments' : 'All Attachments',
	'acmCompareView' : 'Compare models',
	'acmListView' : 'List view',
	'acmHideModels' : 'Hide Models',
	'acmEntryPointPleaseSelectErr' : 'Please select drop down value to proceed.',
	'serverError' : 'Web service or web server is down. Please try later.',
	'acmSelectAModel' : 'Select A Model',
	'addModelErr' : 'Model is already existing . Please select different model.',
	'acmAttachmentType' : 'Attachment Type',
	'acmErrorNoDcr' : 'Model have not valid DCR path.',
	'acmErrorNoAttachmentTypes' : 'No compatible attachment type.',
	'modelsDisplayed' : 'Models Displayed'
}

/* ACM Locale Variables Map End */

/* SPFH Variables for currency localisation. */
var decimalSeparator = ",",
decimalValue = 2,
defaultLocale = "de-DE";

var rulerVals = [];
var leftNavPage = false; /*RS June29 creation of leftnavpage var */

/*RS PS*/
var helpToolTip = 'Help';
var backToResults='Back To Product Selector Results';
var _focusedElementBeforeModal; /* RS WCAG */

/*Start of strings for Print functionality*/
/*Start of export to word/excel/pdf options on print modal*/
var bExportToWord = true;
var bExportToPdf = true;
var bExportToExcel = true;

/*End of export to word/excel/pdf*/
var printExportOption = "PRINT/EXPORT OPTIONS";
var selectImageText = "Select images";
var sectionsToPrint = "Sections to Print";
var selectModel = "Select Model";
var primaryModelImageOnly = "Primary model image only";
var allImages = "All images";
var noImages = "No images";
var printText = "Print";
var saveAsPdf = "Save as PDF";
var exportToWord = "Export to Word";
var exportToExcel = "Export to Excel";
var getAdobeReader = "Get Adobe&reg; Reader&reg;";
var exportSpecificationToExcel = "Export Specifications to Excel";
var exportComparisonsToExcel = "Export Model Comparisons to Excel";
var headingExportOptions = "Export Options";

/*Start Of EU cookie Strings */
euCookieAlwaysOn = "Always On";
euCookieOn = "On";
euCookieOff = "Off";
/*End Of EU cookie Strings*/

/* postalCode text WCAG */
var postalCodeText = 'Postal code';
var _tabHeading = '';

/* Rich Media Players */
var urlStandardPlayer = "//www.deere.com/media/player/player_en_us.html?";
var urlAkamaiPlayer = "//www.deere.com/en_US/media/player/player.html?";
var urlComponentPlayer = "//www.deere.com/en_US/media/player/component_player.html?";

var Youtubeplayer = "http://www.deere.com/en_US/media/player/youtube_player.html?src=";
/* QC 738 - YouTube locale specific NewTab fix starts */
//alert("YouTube testing\n Locale URL: "+localeURL+"\nURL:"+window.location.href+" \n Path:"+window.location.pathname+" \n Host:"+window.location.hostname);
var regExLocale = /^\/.+?\//; /* Regex to generate the locale value from path */
var localeName = regExLocale.exec(window.location.pathname); /*return value like /en_US/ */

if (localeName == "/iw-preview/"){
	Youtubeplayer = "http://www.deere.com/en_US/media/player/youtube_player.html?src=";
}
else{
	Youtubeplayer = localeName+"media/player/youtube_player.html?src=";
}
/* QC 738 YouTube locale specific NewTab fix ends */
var localeLang = "en";

var viewMore = "View More",
	viewLess = "View Less";


var regExQuotesValue = /\"([^"]+)\"/; /* Regex to generate the value between the quotes. */

/* modelLinks global vars */
var modelLinks = {
	'imageGallery' : 'Image Gallery',
	'degreeView' : '360&deg View',
	'videos' : 'Videos'
}
var emailShare = "Email";
/*Special Offers Variables*/
var modelTitleSpecialOffer = {
	'specialOfferText' : 'SPECIAL OFFERS',
	'viewDetailsUrl' : 'View Details',
	'specialOfferTitlehtml' : '',
	'offerDurationText' : 'Offer Duration'
}
/*RS load script start*/
function getScript( url, callback ) {
  var script = document.createElement( "script" )
  script.type = "text/javascript";
  if(script.readyState) {  //IE
    script.onreadystatechange = function() {
      if ( script.readyState === "loaded" || script.readyState === "complete" ) {
        script.onreadystatechange = null;
        //callback();
      }
    };
  } else {  //Others
    script.onload = function() {
      //callback();
    };
  }

  script.src = url;
  document.getElementsByTagName( "head" )[0].appendChild( script );
}
/*End load script */

/*RS load css start*/
function getCss( url, callback ) {
  var css = document.createElement( "link" )
  css.type = "text/css";
  css.rel = "stylesheet";
  if(css.readyState) {  //IE
    css.onreadystatechange = function() {
      if ( css.readyState === "loaded" || css.readyState === "complete" ) {
        css.onreadystatechange = null;
        //callback();
      }
    };
  } else {  //Others
    css.onload = function() {
      //callback();
    };
  }

  css.href = url;
  document.getElementsByTagName( "head" )[0].appendChild( css );
}
/*End load css */

/* RS Parse valuse */
function parseValueAttribute(value){
	var _valueHtml = '';
	value.each(function(){
		var _this = $(this), 
			 _subValue = $(_this.find('value')[0]).text(),
			 _subUnit=$(_this.find('unit')[0]).text();
		_valueHtml +='<span class="displayBlock">'+ _subValue + ' ' +_subUnit +'</span>' ;
		
	})
	return _valueHtml;
}
/*RS WCAG II to show title tooltip for home page carousel*/
function homePageHeroTitle(currListElement){
	$('.homepage-slide-op-prev .homepage_carousel_prev a, .homepage-slide-op-next .homepage_carousel_next a').attr({'href':currListElement.children('a').eq(0).attr('href'), 'target':currListElement.children('a').eq(0).attr('target'), 'title':currListElement.children('a:first').children('img').attr('title')});
	return false;
}

function getURLParameter(name) {
return decodeURIComponent(
(new RegExp('[?(.+)|&(.+)]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))
||null;
}

/* Function to get query/search parameters */
function getUrlParams(name, customQuery){
var urlParams;
var match,
        pl     = /\+/g,  // Regex for replacing addition symbol with a space
        search = /([^&=]+)=?([^&]*)/g,
        decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
        query  = window.location.search.substring(1);
	query = (typeof customQuery !== "undefined") ? customQuery : query;
    urlParams = {};
    while (match = search.exec(query)){
       urlParams[decode(match[1])] = decode(match[2]);} 
	return (typeof name !== "undefined") ? urlParams[name] : urlParams;
}
/* Function to check hosts and return true if not production one */
function checkHost(){
	var hostName = window.location.host;
	return (hostName === "http://www.deere.com/common/deere-resources/js/delvmjddevps03.sapient.com" || hostName === "http://www.deere.com/common/deere-resources/js/delvmjdqaps05.sapient.com" || hostName === "http://www.deere.com/common/deere-resources/js/deereqa.sapient.com" || hostName === "http://www.deere.com/common/deere-resources/js/preview-deereqa.sapient.com" || hostName === "http://www.deere.com/common/deere-resources/js/devlapp835.tal.deere.com") ? true : false;
}
/* Function to return alphanumeric text out of given value */
var returnAlphaNumeric = function(_string){
		return _string.toLowerCase().replace(/[^\w]/gi, "");
}

function postBackToResultsPost(){
	document.postBackToResults.action = $('#formURL').val();
	document.postBackToResults.submit();
}
/*Following function sets locale information in footer for cookie settings link*/
function setLocaleInFooter() {
	var data = $("#cookieContent").val();
	if(data) {
		var dataToFooterLink = JSON.parse(data);
		var UrlData = dataToFooterLink.defaultPanelLinkURL.split("?");
		if($(".MOD_NO_7, .MOD_NO_7_1").length > 0) {
			$(".MOD_NO_7 p.links a, .MOD_NO_7_1 ul li a").each(function(i, el) {
				var href = el.href;
				if((href.indexOf('Cookie') != -1 || href.indexOf('cookie') != -1) && href.indexOf('privacy_and_data') != -1){
					if(href.indexOf('locale_name') != -1)					
					var locale_href = href.split("?");
					if(locale_href) { $(this).attr("href", locale_href[0]+"?"+UrlData[1]);}
					else { $(this).attr("href", href+"?"+UrlData[1]);}
				}
			});
		}		
	}	
}

/*Folowing function helps to search a string in an array can be used across
@param str is string to be searched in an array
@param strArray actual array to be loop through
*/
function searchStringInArray (str, strArray) {
    for (var j=0; j<strArray.length; j++) {
        if (strArray[j].match(str)) { return strArray[j]; }
        else if(strArray[j].match("myjdpreview")) {return "dcompreview"}
    }
    return "dcom";
}

function postBackToResults(url, psState){

  date = new Date();
  date.setDate(date.getDate() -1);
  
  var key = 'psState';
  document.cookie = key + '=;path=/;expires=' + date;
  key = 'currentUrl';
  document.cookie = key + '=;path=/;expires=' + date;
  key = 'pageLink';
  document.cookie = key + '=;path=/;expires=' + date;
 $('<form>', {
        "id": 'backToResults',
        "html": '<input type=\"text\" id=\"psState\" name=\"psState\" value=\'' +decodeURIComponent(psState)+ '\' /> <input type=\"text\" id=\"categorySetName\" name=\"categorySetName\" value=\'' +JSON.parse(psState).categorySetName+ '\' />',
        "action": url,
		"method" : 'post'
    }).appendTo(document.body).submit();
}

/* RS WCAG */
var focusableElementsString = "";
	
function trapTabKey(obj,evt) {
	switch (evt.which){
		case 9:
			// if tab or shift-tab pressed
			{
	
			// get list of all children elements in given object
			var o = obj.find('*');
	
			// get list of focusable items
			var focusableItems;
			focusableItems = o.filter(focusableElementsString).filter(':visible');
			// get currently focused item
			var focusedItem;
			focusedItem = jQuery(document.activeElement);
	
			// get the number of focusable items
			var numberOfFocusableItems;
			numberOfFocusableItems = focusableItems.length;
	
			// get the index of the currently focused item
			var focusedItemIndex;
			focusedItemIndex = focusableItems.index(focusedItem);
			if (evt.shiftKey) {
				//back tab
				// if focused on first item and user preses back-tab, go to the last focusable item
				if(focusedItemIndex==0){
					focusableItems.get(numberOfFocusableItems-1).focus();
					//focusableItems.get(numberOfFocusableItems-1).select();
					evt.preventDefault();
				}
				
			} else {
				//forward tab
				// if focused on the last item and user preses tab, go to the first focusable item
				if(focusedItemIndex==numberOfFocusableItems-1){
					focusableItems.get(0).focus();
					//focusableItems.get(numberOfFocusableItems-1).select();
					evt.preventDefault();				
				}
			}
			break;
		}
		case 27:
		// if escape pressed
		{
			$.nyroModalRemove();
			evt.preventDefault();
			break;
		}/*
		case 32:
		// if space key pressed
		{
			if(obj.is("input")){
				evt.preventDefault();
				break;
			}
		}*/
	}

}

function adjustFooter(){
	var socialLinks = $(".MOD_NO_7_1 .social_links").outerWidth();
	
	$(".MOD_NO_7_1 .footer_left_col").css("margin-right",socialLinks);
	$(".MOD_NO_7_1 .footer_links li:first-child").addClass("first");
	$(".MOD_NO_7_1 .footer_links li").prepend("<span class='stack_seperator'>&nbsp;</span> ");
}

function adjustAssetTitle(){
	
	$(".MOD_GC_10a, .MOD_GC_10b").each(function(){
		
		var seeAllLinkSpace = ($(this).find(".linkPane").outerWidth() + 5);
		
		if(seeAllLinkSpace >= 130) { 
			$(this).find(".linkPane").css("width", 130);
			$(this).find("h2").css("padding-right", 155);
		}else{
			$(this).find("h2").css("padding-right",seeAllLinkSpace);
		};

		var headingHeight = ($(this).find("h2").outerHeight() - $(this).find(".linkPane").outerHeight()) / 2;
		$(this).find(".linkPane").css("padding-top", headingHeight);
	})
}


function initCookie(){

	var cookieValue = $('#cookieContent').val();
	if(typeof cc != "undefined") {
		if (cookieValue !=null){
			var cookieValueJSON = JSON.parse(cookieValue);
			cc.initialise({
				cookies: {
					necessary: {},		
					analytics: {},
					social: {}			
				},
			strings: { 
				notificationTitleImplicit: cookieValueJSON.defaultPanelText,
				allowCookiesImplicit: cookieValueJSON.defaultPanelAcceptButton,
				seeDetailsImplicit: cookieValueJSON.defaultPanelCookieSettingsButton,
				viewDetailsImplicit: cookieValueJSON.defaultPanellinktext, 
				changeSettingUrl: cookieValueJSON.defaultPanelLinkURL,
				changeSettingUrlTarget: cookieValueJSON.defaultPanelLinkURLTarget,
				savePreference: cookieValueJSON.defaultPanelAcceptButton,
				necessaryDefaultTitle:cookieValueJSON.expandedPanelCookieCategoryHeaderText1,
				analyticsDefaultTitle:cookieValueJSON.expandedPanelCookieCategoryHeaderText2,
				socialDefaultTitle:cookieValueJSON.expandedPanelCookieCategoryHeaderText3, 
				necessaryDefaultDescription: cookieValueJSON.expandedPanelCookieCategoryDescriptionText1,
				analyticsDefaultDescription: cookieValueJSON.expandedPanelCookieCategoryDescriptionText2,
				socialDefaultDescription: cookieValueJSON.expandedPanelCookieCategoryDescriptionText3
			},
				settings: {
					consenttype: "implicit",
					hideprivacysettingstab: true,
					disableallsites: true,
					useSSL: true
				}
			});
		}else {
		
			cc.initialise({
				cookies: {
				necessary: {},		
				social: {},
				analytics: {}
					
				},
				settings: {
					consenttype: "implicit",
					hideprivacysettingstab: true,
					disableallsites: true,
					useSSL: true
				}
			});
			
		}
	}
}
var ctaNoValue = "Not Available";

(function($) {
	$.fn.menuAim = function(opts) {
        // Initialize menu-aim for all elements in jQuery collection
        this.each(function() {
            init.call(this, opts);
        });

        return this;
    };

    function init(opts) {
	
        var $menu = $(this),
            activeRow = null,
            mouseLocs = [],
            lastDelayLoc = null,
            timeoutId = null,
            options = $.extend({
                rowSelector: "> li",
                submenuSelector: "*",
                submenuDirection: "right",
                tolerance: 75,  // bigger = more forgivey when entering submenu
                enter: $.noop,
                exit: $.noop,
                activate: $.noop,
                deactivate: $.noop,
                exitMenu: $.noop
            }, opts);
        var MOUSE_LOCS_TRACKED = 3,  // number of past mouse locations to track
            DELAY = 300;  // ms delay when user appears to be entering submenu

        /**
         * Keep track of the last few locations of the mouse.
         */
        var mousemoveDocument = function(e) {
                mouseLocs.push({x: e.pageX, y: e.pageY});

                if (mouseLocs.length > MOUSE_LOCS_TRACKED) {
                    mouseLocs.shift();
                }
            };

        /**
         * Cancel possible row activations when leaving the menu entirely
         */
        var mouseleaveMenu = function() {
                if (timeoutId) {
                    clearTimeout(timeoutId);
                }
				
				deereGlobal.advanceFlyout.hideAll();
				
                // If exitMenu is supplied and returns true, deactivate the
                // currently active row on menu exit.
                if (options.exitMenu(this)) {				
                    if (activeRow) {					
						deereGlobal.advanceFlyout.hideAll();
                        options.deactivate(activeRow);
                    }
                    activeRow = null;
                }
                
                activeRow = this;
            };

        /**
         * Trigger a possible row activation whenever entering a new row.
         */
        var mouseenterRow = function() {
                if (timeoutId) {
                    // Cancel any previous activation delays
                    clearTimeout(timeoutId);
                }

                options.enter(this);
                possiblyActivate(this);

            },
            mouseleaveRow = function() {
				//deereGlobal.advanceFlyout.hideMenu(this);
                options.exit(this);
            };

        /*
         * Immediately activate a row if the user clicks on it.
         */
        var clickRow = function() {
                activate(this);
            };

        /**
         * Activate a menu row.
         */
        var activate = function(row) {
                if (row == activeRow) {
                    return;
                }

                if (activeRow) {
                    options.deactivate(activeRow);
                }

                options.activate(row);
                activeRow = row;
            };

        /**
         * Possibly activate a menu row. If mouse movement indicates that we
         * shouldn't activate yet because user may be trying to enter
         * a submenu's content, then delay and check again later.
         */
        var possiblyActivate = function(row) {
                var delay = activationDelay();

                if (delay) {
                    timeoutId = setTimeout(function() {
                        possiblyActivate(row);
                    }, delay);
                } else {
                    activate(row);
                }
            };

        /**
         * Return the amount of time that should be used as a delay before the
         * currently hovered row is activated.
         *
         * Returns 0 if the activation should happen immediately. Otherwise,
         * returns the number of milliseconds that should be delayed before
         * checking again to see if the row should be activated.
         */
        var activationDelay = function() {
                if (!activeRow || !$(activeRow).is(options.submenuSelector)) {
                    // If there is no other submenu row already active, then
                    // go ahead and activate immediately.
                    return 0;
                }

                var offset = $menu.offset(),
                    upperLeft = {
                        x: offset.left,
                        y: offset.top - options.tolerance
                    },
                    upperRight = {
                        x: offset.left + $menu.outerWidth(),
                        y: upperLeft.y
                    },
                    lowerLeft = {
                        x: offset.left,
                        y: offset.top + $menu.outerHeight() + options.tolerance
                    },
                    lowerRight = {
                        x: offset.left + $menu.outerWidth(),
                        y: lowerLeft.y
                    },
                    loc = mouseLocs[mouseLocs.length - 1],
                    prevLoc = mouseLocs[0];

                if (!loc) {
                    return 0;
                }

                if (!prevLoc) {
                    prevLoc = loc;
                }

                if (prevLoc.x < offset.left || prevLoc.x > lowerRight.x ||
                    prevLoc.y < offset.top || prevLoc.y > lowerRight.y) {
                    // If the previous mouse location was outside of the entire
                    // menu's bounds, immediately activate.
                    return 0;
                }

                if (lastDelayLoc &&
                        loc.x == lastDelayLoc.x && loc.y == lastDelayLoc.y) {
                    // If the mouse hasn't moved since the last time we checked
                    // for activation status, immediately activate.
                    return 0;
                }

                // Detect if the user is moving towards the currently activated
                // submenu.
                //
                // If the mouse is heading relatively clearly towards
                // the submenu's content, we should wait and give the user more
                // time before activating a new row. If the mouse is heading
                // elsewhere, we can immediately activate a new row.
                //
                // We detect this by calculating the slope formed between the
                // current mouse location and the upper/lower right points of
                // the menu. We do the same for the previous mouse location.
                // If the current mouse location's slopes are
                // increasing/decreasing appropriately compared to the
                // previous's, we know the user is moving toward the submenu.
                //
                // Note that since the y-axis increases as the cursor moves
                // down the screen, we are looking for the slope between the
                // cursor and the upper right corner to decrease over time, not
                // increase (somewhat counterintuitively).
                function slope(a, b) {
                    return (b.y - a.y) / (b.x - a.x);
                };

                var decreasingCorner = upperRight,
                    increasingCorner = lowerRight;

                // Our expectations for decreasing or increasing slope values
                // depends on which direction the submenu opens relative to the
                // main menu. By default, if the menu opens on the right, we
                // expect the slope between the cursor and the upper right
                // corner to decrease over time, as explained above. If the
                // submenu opens in a different direction, we change our slope
                // expectations.
                if (options.submenuDirection == "left") {
                    decreasingCorner = lowerLeft;
                    increasingCorner = upperLeft;
                } else if (options.submenuDirection == "below") {
                    decreasingCorner = lowerRight;
                    increasingCorner = lowerLeft;
                } else if (options.submenuDirection == "above") {
                    decreasingCorner = upperLeft;
                    increasingCorner = upperRight;
                }

                var decreasingSlope = slope(loc, decreasingCorner),
                    increasingSlope = slope(loc, increasingCorner),
                    prevDecreasingSlope = slope(prevLoc, decreasingCorner),
                    prevIncreasingSlope = slope(prevLoc, increasingCorner);

                if (decreasingSlope < prevDecreasingSlope &&
                        increasingSlope > prevIncreasingSlope) {
                    // Mouse is moving from previous location towards the
                    // currently activated submenu. Delay before activating a
                    // new menu row, because user may be moving into submenu.
                    lastDelayLoc = loc;
                    return DELAY;
                }

                lastDelayLoc = null;
                return 0;
            };

        /**
         * Hook up initial menu events
         */
        $menu
            .mouseleave(mouseleaveMenu,mouseleaveRow)
            .find(options.rowSelector)
                .mouseenter(mouseenterRow)
                .mouseleave(mouseleaveRow)
                .click(clickRow);

        $(document).mousemove(mousemoveDocument);

    };
})(jQuery);

$(document).ready(function () {
	// Youtube code Starts 
	var autoPlay = true;
	if ($("#video_gallery_youtube_player").length){
		$('.video_play_link').click(function() {
			var youtubeToPlay = $(this).attr('href');
			var video_description_title =  $(this).siblings(".video_description_title").html();
			var video_description = $(this).siblings(".video_description").html();
			var youTubePlayer = $(this).closest("div.video_list_container").next().find('#video_gallery_youtube_player');
			var youTubeDetails = $(this).closest("div.video_list_container").next().find('div.video_details');
			
			if (typeof youTubePlayer.attr("id") === "undefined"){
				youTubePlayer = $(this).closest("div.video_list").next().find('#video_gallery_youtube_player');
				youTubeDetails = $(this).closest("div.video_list").next().find('div.video_details');
			}
			
			if (youtubeToPlay.match(/www.youtube.com/)){
				youtubeToPlay = youtubeToPlay.replace("watch?v=","embed/");
				if (autoPlay){
					youtubeToPlay = youtubeToPlay+"?modestbranding=1&amp;controls=1&amp;cc_load_policy=1&amp;showinfo=0&amp;autoplay=1&amp;wmode=transparent&amp;rel=0&amp;iv_load_policy=3";
				}
				else{
					youtubeToPlay = youtubeToPlay+"?modestbranding=1&amp;controls=1&amp;cc_load_policy=1&amp;showinfo=0&amp;wmode=transparent&amp;rel=0&amp;iv_load_policy=3";
				}
				
				youTubePlayer.attr('src',youtubeToPlay);
				if (youTubeDetails.children("[id*='video_title']").length){
					youTubeDetails.children("[id*='video_title']").html(video_description_title);
					if (youTubeDetails.children("[id*='video_description']").length){
						youTubeDetails.children("[id*='video_description']").html(video_description);
					}
				}			
			}
			
		});
	}
	else{
		$('.video_play_link').click(function() {
			var youtubeToPlay = $(this).attr('href');
			if (youtubeToPlay.match(/www.youtube.com/)){
				youtubeToPlay = youtubeToPlay.replace("watch?v=","embed/");
				if (autoPlay){
					youtubeToPlay = youtubeToPlay+"?modestbranding=1&amp;controls=1&amp;cc_load_policy=1&amp;showinfo=0&amp;autoplay=1&amp;wmode=transparent&amp;rel=0&amp;iv_load_policy=3";
				}
				else{
					youtubeToPlay = youtubeToPlay+"?modestbranding=1&amp;controls=1&amp;cc_load_policy=1&amp;showinfo=0&amp;wmode=transparent&amp;rel=0&amp;iv_load_policy=3";
				}
				$("#youtube_player").attr('src',youtubeToPlay);
			}
		});
	}
	
	// Youtube code Ends 
	/*Code to initiate the CTA Link[genericDropDown] instances*/ 
	var ctaForm = $("form.form_cta_link")
	if(ctaForm.length){
		getScript('genericDropDown.js'/*tpa=http://www.deere.com/common/deere-resources/js/genericDropDown.js*/);	
	}
	/*Code to initiate the CTA Link instances*/ 
	
	
	/*Code to initiate the image gallery instances*/ 
	if($('.jd_image_gallery').length){
		getScript('image-gallery.js'/*tpa=http://www.deere.com/common/deere-resources/js/image-gallery.js*/);
	}
	
	/*Code to add the amp.new.player.js QC486 -RS*/ 
	if($('.embed_new_player').length){
		if($('.video_gallery').length > 0){
			setTimeout("getScript('amp.new.player.js'/*tpa=http://www.deere.com/common/deere-resources/js/amp.new.player.js*/)", 2500);
		}else{
			getScript('amp.new.player.js'/*tpa=http://www.deere.com/common/deere-resources/js/amp.new.player.js*/);
		}
		getCss("../css/amp.min.css"/*tpa=http://www.deere.com/common/deere-resources/css/amp.min.css*/);
	}
	
	
	/*Script to check if the content is loaded in iframe or not.*/
	if ( window.self === window.top ) {  } else { $("html").addClass('contentIframe'); iframe = true; }

	if ($("#cc-notification-button")[0]){
		jQuery("#cc-notification-button").bind('click', function() {
		
			initCookie();
			cc.showbanner();
			isCookieCurtainTrigger = true;
			jQuery.each(cc.cookies, function (a, b) {
					if (readCookie('cc_'+a) == "no") {
						jQuery("#cc-checkbox-" + a).attr("checked", false)
					} else if (readCookie('cc_'+a) == "yes"){
						jQuery("#cc-checkbox-" + a).attr("checked", true)
					} 
				});
			jQuery("#cc-notification-moreinfo").trigger('click');
			/*  Scroll top */
			$('#cc-notification').find('a:first').focus();
			var ccNotificationTop = $('#cc-notification').position().top;
				$('html, body').animate({
					  scrollTop: ccNotificationTop
			}, 'slow');
		});		
	}
	
	var url = window.location.pathname;
	if (typeof isEUCookieEnabled !== "undefined" && isEUCookieEnabled && (typeof cookieDialogForMYJD === "undefined" || cookieDialogForMYJD) && url.indexOf("iw-cc") === -1) {
		/*EU Cookie Implementation*/
		var pathname = window.location.pathname;
		pathname = JSON.stringify(pathname);
		pathname = pathname.split("/");
		//Krishna Added Condition to prevent this EU cookie curtain in TeamSite
		if($.inArray("iw-preview", pathname) ==-1 && $.inArray("iw-cc", pathname)== -1) {
			initCookie();
		}		
	}	
	/*Following function sets locale information for footer link got from euCookie*/
	setLocaleInFooter();

	/*RS EU Cookie*/
	 var euCookieCols = $('.eu_cookies_details .cols');
	if(euCookieCols.length>0){
		var maxHeight = Math.max.apply(null, euCookieCols.map(function ()
		{
			return $(this).height();
		}).get());
		euCookieCols.css('min-height',maxHeight);
	}
	

	if ($(".MOD_NO_7_1")[0]){
		adjustFooter();
	}

	if ($(".MOD_GC_10a, .MOD_GC_10b")[0]){
		adjustAssetTitle();
	}
	
	/*Special Offer CR*/
	$('a.more_page.nyroModal').bind('click', function() {
		setTimeout('$.applyHeight("#nyroModalContent .MOD_FO_63 .eligible_equipments ul", "li")', 1000);
	})
	
	$('a.more_page.nyroModal').nyroModal({modal: true});
	
	if($('.MOD_FO_63')[0]){
		$.applyHeight('.MOD_FO_63 .eligible_equipments ul', 'li');
	};
	
	ie7 = $(document.documentElement).hasClass('ie7');
	ie8 = $(document.documentElement).hasClass('ie8');
	ie9 = $(document.documentElement).hasClass('ie9');
	/* RS WCAG 
	$('.homepage-slide-op-prev .homepage_carousel_prev a, .homepage-slide-op-next .homepage_carousel_next a').click(function(evt) {
		var _keyCode = (evt.which) ? evt.which : evt.keyCode;
		var _activeLi = $(this).parents('.MOD_GC_12_2b').find('ul.homepage-slide-op li:visible');
		var win=window.open(_activeLi.children('a').attr('href'), _activeLi.children('a').attr('target'));
  		win.focus();
    });*/
	
	$('.homepage-slide-op-prev .homepage_carousel_prev a, .homepage-slide-op-next .homepage_carousel_next a').keydown(function(evt) {
		var _keyCode = (evt.which) ? evt.which : evt.keyCode;
		if(_keyCode === 13){
			evt.preventDefault();
			$(this).children('span').trigger('click');
		}
	});
	
	
	_tabHeading = $('.tab_heading');
	if(_tabHeading.length>0){
		var _tabMarginLeft = 99;
		if($('.spfh_component').length){
			_tabMarginLeft = 225;
		}else if($('.nozzle_selector').length){
			_tabMarginLeft = 125;
		}else{
			_tabMarginLeft = 99;
		}
		_tabHeading.map(function(index){
			$(this).css('margin-left', index*_tabMarginLeft);
		});/*RS WCAG3*/
	}
	focusableElementsString = (ie7) ? "a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), object, embed" : "a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), object, embed, *[tabindex], *[contenteditable]";
	
	
	
	/* RS WCAG */
	(iframe == false) ? document.documentElement.focus() : '';/*RS IE9 fix*/
	(iframe == false && ie9) ? _focusedElementBeforeModal = document.activeElement : '';
	$('body').delegate('#nyroModalWrapper, #nyroModal2Wrapper, .qtip, #addRemoveModel_Comp, #specLayer','keydown', function(event){trapTabKey($(this),event);})
	var _postalCode = $('#postalCode');
	if(_postalCode.length>0){
		_postalCode.attr('title', postalCodeText);
	}
	/* Function to capture the last focused element */
	$('body').delegate('#wrap', 'keydown', function(evt){
		if (evt.which == 13 ) {
			_focusedElementBeforeModal = document.activeElement;
		}
	});
	/*$('body').prepend('<p><a href="#jdfhfl-en-us-row-2" class="skip_to_content" rel="skip to content" title="skip to content link">Skip to Content</a></p>');*/
	$('body').delegate('#hero-banner .pause-resume', 'click', function(e){
		e.preventDefault();
		var _this = $(this),
			_children = _this.children('span');
		_children.trigger('click');
		if(_children.hasClass('resume')){
			_this.removeClass('pause');
			_this.addClass('resume');
		}
		if(_children.hasClass('pause')){
			_this.removeClass('resume');
			_this.addClass('pause');
		}
	})
	
//if(getURLParameter('currentUrl')!='' && getURLParameter('psState')!='' && getURLParameter('currentUrl')!=null && getURLParameter('psState')!=null){

//if($('#psState').val()!='' && $('#formURL').val()!='' && typeof $('#psState').val()!='undefined' && typeof $('#formURL').val()!='undefined'){	//'.wizardContent .wizard-bar .select-product-type .product-type'
//	$('#grey_bg_layout').prepend('<div class="grey_bg_layout"><a class="back_to_ps_results" href="javascript:postBackToResults();">'+backToResults+'</a></div>');
//}

if(getCookie('currentUrl')!=null && getCookie('psState')!=null  && getCookie('pageLink')!=null &&
	getCookie('currentUrl')!='notfound' && getCookie('psState')!='notfound' && getCookie('pageLink')!='notfound'
	&& (document.location.toString().indexOf(getCookie('pageLink'))!= -1)){ 	
	$('#grey_bg_layout').prepend('<div class="grey_bg_layout"><a class="back_to_ps_results" href="javascript:postBackToResults(\''+getCookie("currentUrl")+'\',\''+encodeURIComponent(getCookie('psState'))+'\');">'+backToResults+'</a></div>');
}

	if ($(".region_homepage")[0]){
		/*var regionList = $(".region_homepage .region_column ul li");
		var regionListLength = regionList.length;	
		var regionListColumn = parseInt($('.region_column .column_length').text());		
		for(var i = 0; i < regionListLength; i+=regionListColumn) {
		  regionList.slice(i, i+regionListColumn).wrapAll("<li class='region_column_container'><ul></ul></li>");
		}
		$(".region_homepage .region_column ul li.region_column_container:last").addClass("last");
		*/
		$(".region_homepage .region_column ul li.region_column_container").find('li:first h6:first, li:first .primary_subhead:first').addClass("removeH6Margin");
		
	}
	
	/* RS back to ps result issue */
	if($('.back_to_ps_results').length > 0){
		var backToResult = $('.back_to_ps_results').clone();
		backToResult =   $(backToResult).find("script").remove("script").end();
		backToResult = $(backToResult).wrap('<p/>').parent().html();
		backToResult = '<div class="grey_bg_layout">'+ backToResult + '</div>';
		
		$('#grey_bg_layout .back_to_ps_results').remove();
		$('.MOD_NO_8').after(backToResult);
	}
	
	// onclick respective tab will open
	$('a.tabAnchor').bind('click', function() {
		var getTabID = this.hash;
		$('.MOD_NO_2_S li, .tab_module li, .WCAG_MOD_NO_2_S .tab_heading, .wcag_tab_module .tab_heading').each(function(){
			var elementID = "#"+$(this).attr('id');
			if(elementID == getTabID){
				$(this).find("a").trigger('click');
			}
		});
	});
	/* We don't want it on Deere.com
	// Global  form Select Custom Element Design  | MG
	if($(':input').hasClass('customElement')) {
		$(".customElement").uniform();
	}
	if($(':input').hasClass('customElementSorting')) {
		$(".customElementSorting").uniform();
	}
	if($(':input').hasClass('customElementFilter')) {
		$(".customElementFilter").uniform();
	}
	if($(':input').hasClass('customElementWizard')) {
		$(".customElementWizard").uniform();
	}
	*/
	$('form').submit(function() {
		$('select.customElement').filter(function() {
			if($(this).hasClass('error_input')) {
				$(this).parent('div.selector').addClass('error_input');
				return true;
			} else {
				$(this).parent('div.selector').removeClass('error_input');
				return false;
			}
		});
	});	
	
	$('#Ghid000001, .MOD_NO_6 a.logo').attr('title', Logo_Alt_Txt);
	$('#Ghid000001, .MOD_NO_6 a.logo').attr('href', localeURL);	
	
	/* DD to check the construction pages.
	var construction = $("head").find("link");

	$(construction).each(function() {
		var linkCSS = $(this).attr("href");
		if(linkCSS == "../css/constructionModules.css"/*tpa=http://www.deere.com/common/deere-resources/css/constructionModules.css*/){
			$(document.documentElement).addClass("construction");
		}
	});
	*/	
	/**PK Crop insurance***/
	$('.MOD_FO_35 #crop_insurance').change(function () {
	$('.MOD_FO_35 .error').remove();
        var selVal = $(this).val();
		var noDataFound = true;
		$('.MOD_FO_35 .MOD_GC_11_Horizontal').show();
        $('.MOD_FO_35 .MOD_GC_11_Horizontal').each(function () {
            var obj = $(this).find('.states');
            //console.log(obj);
            var stateName = obj.text();
	    //console.log(stateName);
            //var val = obj.text().search(selVal);
            if (obj.text().search(selVal) >= 0) {
				noDataFound = false;
                obj.closest('.MOD_FO_35 .MOD_GC_11_Horizontal').show();
            }
			else if (selVal == "All States") {
				noDataFound = false;
				$('.MOD_FO_35 .MOD_GC_11_Horizontal').show();
			}
            else if (stateName.search("All States") >= 0) {
                noDataFound = false;
                obj.closest('.MOD_FO_35 .MOD_GC_11_Horizontal').show();
            }
			else{
			obj.closest('.MOD_FO_35 .MOD_GC_11_Horizontal').hide();
			}
        });
		
		/***if selected index value not found on page***/
		if(selVal != "" && noDataFound){
			$('.MOD_FO_35').append('<div class="error">'+ noRecordMsg +'</div>').show();
		};
		/* MG | Hide all states if All States not selected */
		if($(this).val() != 'All States') {
			$('div.MOD_FO_35 div.avail').hide();
		} else {
			$('div.MOD_FO_35 div.avail').show();
		}
    });
	/**PK crop insurance script ends**/

	/*DD If the page has MOD_FO_1 class then apply cufon else ignore this part.*/
	if ($(".MOD_FO_1")[0]){
		// if(enableCufonFont == "true"){
		// 	Cufon.replace(cufonElements);
		// 	Cufon.replace('.MOD_FO_1 .big_title h1', {fontSize:'52px', lineHeight: '52px'});
		// 	Cufon.replace('.MOD_FO_1 .medium_title h1', {fontSize:'38px', lineHeight: '38px'});
		// 	Cufon.replace('.MOD_FO_1 .medium_title_without_space h1', {fontSize:'24px', lineHeight: '24px'});
		// 	Cufon.replace('.MOD_FO_1 .small_title h1', {fontSize:'21px', lineHeight: '21px'});
		// 	Cufon.replace('.MOD_FO_1 .big_title h2, .MOD_FO_1 .medium_title h2, .MOD_FO_1 .small_title h2, .MOD_FO_1 .medium_title_without_space h2', {fontSize:'16px', lineHeight: '18px'});
		// 	Cufon.replace('.MOD_FO_1 .big_title .hero-alternate-title, .MOD_FO_1 .medium_title .hero-alternate-title, .MOD_FO_1 .medium_title_without_space .hero-alternate-title, .MOD_FO_1 .small_title .hero-alternate-title', {fontSize:'16px', lineHeight: '18px'});
		//  }
		
		/*DD To update the form action on page load for product selector.*/
		if($("#backToResults")[0]){
			var backToResultsFormURL = $('#backToResults #formURL').val();
			$('#backToResults').attr('action', backToResultsFormURL);
		}
	}
	
	/*****PK blog component Pagination and Last li call**/
	setTimeout('blogPagination()', 1000);
	setTimeout('lastLiBlog()', 1100);
	/*****data call for blog component**/
	

	/*PK artf1251983*/
	$("span.flyout img").click(function() {
			location.href=$(this).closest("a").attr("href");
	});

	$('.MOD_GC_12_5 #product-navigation').append('<div class="overlay_illusion" style="display:none;"><img src="../img/blank.gif" /></div>');

    $('.MOD_FO_6b table tbody > tr:nth-child(even)').addClass('bg');
    //DD Commented out as this not required now. $('.alphabet li:nth-child(10)').css({'visibility' : 'hidden'});
    persistlang();

    $('.MOD_GC_4 a').each(function () {
        $(this).click(function () {
            var relDia = $(this).attr('rel');
            var modalWidth = parseInt(relDia.substring(0, relDia.indexOf("|")))
            var ModalHeight = parseInt(relDia.substring(relDia.indexOf("|") + 1, relDia.length))

            setInterval(function () {
                $('#nyroModalWrapper').width(modalWidth);
                $('#nyroModalContent').width(modalWidth);
                $('#nyroModalWrapper').height(ModalHeight);
                $('#nyroModalContent').height(ModalHeight);
                //$('#nyroModalWrapper').css("top", ( $(window).height() - ModalHeight ) / 2+$(window).scrollTop() + "px");
                //$('#nyroModalWrapper').css("left", ( $(window).width() - modalWidth ) / 2+$(window).scrollLeft() + "px");
            }, 1000)
        })
    })

    $('#utilityNav .lang').mouseenter(function () {
        $(this).prev().addClass('on');
    });

    $('#utilityNav .lang').mouseleave(function () {
        $(this).prev().removeClass('on');
    });

    tallest = 0;
    $('.MOD_GC_14_2').each(function () {
        thisHeight = $(this).find('div').height();
        if (thisHeight > tallest) {
            tallest = thisHeight;
        }
    });
    $('.MOD_GC_14_2 div').height(tallest);
    $('.MOD_GC_14_2:lt(2) div').css('paddingTop', '0');

    $('img[align="left"]').addClass('imageLeft');
    $('img[align="right"]').addClass('imageRight');

    $('.carousel-pagination span').hover(function () {
        $(this).addClass('hover');
    }, function () {
        $(this).removeClass('hover');
    });

    $('#utilityNav .lang').click(function () {
        $(this).parent().find('.submenu').show();
    });
	
	$("#utilityNav li:first a").before("<span>" + countryName + "</span>");

    $('#close_lang').click(function () {
        $(this).parent().hide();
    });

    //Country Flag code Starts
	var flagUrl=window.location.pathname.split('/')[1];
	$('.MOD_NO_6 #utilityNav li:first-child').css('background-image','url(/common/deere-resources/img/flag/'+flagUrl+'_flag.png)');
	
	var siteNameFlag = getURLParameter('siteName');
	if (siteNameFlag != null){
		$('.MOD_NO_6 #utilityNav li:first-child').css('background-image','url(/common/deere-resources/img/flag/'+siteNameFlag+'_flag.png)');
	}
	var localeFlag = getURLParameter('locale');
	if (localeFlag != null){
		$('.MOD_NO_6 #utilityNav li:first-child').css('background-image','url(/common/deere-resources/img/flag/'+localeFlag+'_flag.png)');
	}
	//Flag hardcoding for UK/Ireland and Australia/New Zelland Starts here
	if((siteNameFlag || localeFlag || flagUrl) == "en_GB"){
			$('.MOD_NO_6 #utilityNav li:first-child>span').remove();
			$('.MOD_NO_6 #utilityNav li:first-child').prepend('<span>UK & </span><img src="../img/flag/en_IR_flag.png"/*tpa=http://www.deere.com/common/deere-resources/img/flag/en_IR_flag.png*/ class="global_landing_flag ireland_flag" alt="Ireland" title="Ireland"><span> Ireland</span>');
	}else if((siteNameFlag || localeFlag || flagUrl) == "en_AU"){
			$('.MOD_NO_6 #utilityNav li:first-child>span').remove();
			$('.MOD_NO_6 #utilityNav li:first-child').prepend('<span>Australia/ </span><img src="../img/flag/en_NZ_flag.png"/*tpa=http://www.deere.com/common/deere-resources/img/flag/en_NZ_flag.png*/ class="global_landing_flag ireland_flag" alt="Ireland" title="Ireland"><span> New Zealand</span>');
	}	
	var UK_Ireland_selector = '.region_homepage .country-wraper a:contains("UK/Ireland")';
	var UK_Ireland_link = $(UK_Ireland_selector).attr("data-deere-country-link");
	var UK_Ireland_href = $(UK_Ireland_selector).attr("href");
	var UK_Ireland_class = $(UK_Ireland_selector).attr("class");
	var irelandCountryWrap  = '<a data-deere-country-link="'+UK_Ireland_link+'" href="'+UK_Ireland_href+'" class="'+UK_Ireland_class+'">UK/</a><img src="../img/flag/en_IR_flag.png"/*tpa=http://www.deere.com/common/deere-resources/img/flag/en_IR_flag.png*/ class="global_landing_flag ireland_flag" alt="Ireland" title="Ireland"><a data-deere-country-link="'+UK_Ireland_link+'" href="'+UK_Ireland_href+'" class="'+UK_Ireland_class+'">Ireland</a>';
	$(UK_Ireland_selector).parent().prepend(irelandCountryWrap);
	$(UK_Ireland_selector).remove();

	/*RS WCAG II*/
    $('.MOD_NO_2_S li, .MOD_NO_2_S .tab_heading').bind('click', function () {
        if (!$(this).hasClass('on') && $(this).parents('.tabContent').length == 0) {
            var onClass = $(this).attr('class');
				onClass = $.trim(onClass.replace(/secondary_subhead_small|tab_heading_li|tab_heading|secondary_subhead/g,''));/*RS WCAG II*/
				
            $('.MOD_NO_2_S li, .MOD_NO_2_S .tab_heading, .tabContent').removeClass('on');
            $('.MOD_NO_2_S .' + onClass + ', .' + onClass).addClass('on');
			
			element = $(".on .MOD_FO_6a h3:first, .on .MOD_FO_6a .secondary_subhead:first, .on .MOD_FO_6b h3:first, .on .MOD_FO_6c h3:first, .on .MOD_GC_23a h3:first, .on .MOD_GC_22a h3:first, .on .MOD_FO_7a h3:first, .on .MOD_FO_13 h3:first");
			
			$(element).next().show();
			$(element).addClass('open');
        }
    });
	
	/*RS WCAG II*/
	$('.MOD_NO_2_S li, .MOD_NO_2_S .tab_heading').click(function () {
        if (!$(this).hasClass('on') && $(this).parents('.tabContent').length == 0) {
            $(this).siblings().removeClass('on');
            var openClass = $(this).attr('class');
            /*$('.tabContent').hide(); RS QC484 */
            $('.' + openClass + '').addClass('on').show();
            $('.' + openClass + '').siblings().removeClass('on');
            $('#mainNav .on').hide();
            $(this).addClass('on');
			
            if (!$('.' + openClass + '').find('.carousel-viewport')[0]) { 
                $('.' + openClass + '').find('.MOD_GC_6 .list ul').tileCarousel({
                    increment: 4,
                    loop: false,
                    pagination: true
                });
            }
        } else {
            $('.tabContent.on .MOD_GC_6 .list ul.thumbs').tileCarousel({
                increment: 4,
                loop: false,
                pagination: true
            });
        }
    });
	/*Special offer content appendig at header*/
	if ($("#model-special-offer").length){
		modelTitleSpecialOffer.specialOfferTitlehtml = '<div class="header-model-special-offer"><h2 class="subhead">'+modelTitleSpecialOffer.specialOfferText+'</h2><a href="#model-special-offer">'+modelTitleSpecialOffer.viewDetailsUrl+'</a></div>';
		$('.MOD_FO_1 .big_title,.MOD_FO_1 .medium_title,.MOD_FO_1 .small_title,.MOD_FO_1 .medium_title_without_space').append(modelTitleSpecialOffer.specialOfferTitlehtml);
		$('#model-special-offer .offer-duration-txt').html(modelTitleSpecialOffer.offerDurationText);
	}
	
	/*DD - Tweek to make gallery work within tab.*/
    //$("div.gallery_tab").addClass($("gallery_tab_disabled").hasClass("selected").toString());
    if ($("div.gallery_tab").hasClass('on')) {
        $("div.gallery_tab").removeClass('gallery_tab_disabled');
    } else {
        $("div.gallery_tab").addClass('gallery_tab_disabled');
    }
    $('.MOD_NO_2_S li, .MOD_NO_2_S .tab_heading').not('.WCAG_MOD_NO_2_S li').click(function () {
		var _this = $(this);
        if(_this.hasClass('gallery_tab')) {
            _this.next("div.gallery_tab").removeClass('gallery_tab_disabled').addClass('on');
        } else {
           _this.next("div.gallery_tab").addClass('gallery_tab_disabled').removeClass('on');
        }
    });
	
	
	

    /*DD Removed hierarchy as per Dax (.feature .inner) as seeallfeatures and specs was required through Content as well. RS direct child targeting for WCAG*/
    $('a#seeallfeatures').click(function () {
        $('.MOD_NO_2_S > li, .tabContent, .WCAG_MOD_NO_2_S > .tab_heading').removeClass('on');
        $('.MOD_NO_2_S .' + 'features' + ', .' + 'features').addClass('on');
    });
    $('a#seeallspecs').click(function () {
        $('.MOD_NO_2_S > li, .tabContent, .WCAG_MOD_NO_2_S > .tab_heading').removeClass('on');
        $('.MOD_NO_2_S .' + 'specifications' + ', .' + 'specifications').addClass('on');
    });
	
	


    // MOD_GC_12_5
    // cta does not work with overlay, so it's commented out for now. Remove the comment as it was changing the behaviour Piyush
    var point;
	
    /* Drop down product nav */
	$('.MOD_GC_12_5 #product-navigation-trigger a').mouseenter(function(e){
            e.stopPropagation();
            // Pause the banner slideshow
            $('.MOD_GC_12_5 #banner').cycle('pause');
            $(document.getElementById('product-navigation').getElementsByTagName('ul')).css('display', 'block');
            document.getElementById('slideShowOverlay').className = '';
            $('.pause-resume').removeClass('pause').addClass('resume');
            $(this).removeClass('open').addClass('close');
    });
	
	/*DD artf1255036 Conditional click event for Link*/
	$('.MOD_GC_12_5 #product-navigation-trigger a').click(function(e){
		if($(this).hasClass("close")){
			e.preventDefault();
            e.stopPropagation();
            $(document.getElementById('product-navigation').getElementsByTagName('ul')).css('display', 'none');
            document.getElementById('slideShowOverlay').className = 'hideOverlay';
            $('.MOD_GC_12_5 #product-navigation-trigger a').removeClass('close').addClass('open');
            // Resume the banner slideshow
            $('.MOD_GC_12_5 #banner').cycle('resume');
            $('.pause-resume').removeClass('resume').addClass('pause');
		}else{
			e.stopPropagation();
            // Pause the banner slideshow
            $('.MOD_GC_12_5 #banner').cycle('pause');
            $(document.getElementById('product-navigation').getElementsByTagName('ul')).css('display', 'block');
            document.getElementById('slideShowOverlay').className = '';
            $('.pause-resume').removeClass('pause').addClass('resume');
            $(this).removeClass('open').addClass('close');		
		}
    });
	
	/*DD artf1255036 When mouse leave the banner area*/
	$('#hero-banner').mouseleave(function(e){
		e.preventDefault();
		e.stopPropagation();
		$(document.getElementById('product-navigation').getElementsByTagName('ul')).css('display', 'none');
		document.getElementById('slideShowOverlay').className = 'hideOverlay';
		$('.MOD_GC_12_5 #product-navigation-trigger a').removeClass('close').addClass('open');
		
		$('#slideShowOverlay').mouseleave(function() {
			// Resume the banner slideshow
			$('.MOD_GC_12_5 #banner').cycle('resume');
			$('.pause-resume').removeClass('resume').addClass('pause');
		});
    });
	
	/*DD artf1255036*/
	$('#product-navigation').mouseleave(function(e){
		// Resume the banner slideshow
		$('.MOD_GC_12_5 #banner').cycle('resume');
		$('.pause-resume').removeClass('resume').addClass('pause');
    });
	
	/*DD artf1255036*/
	$('#slideShowOverlay, #product-navigation').mouseenter(function(e){
		// Pause the banner slideshow
		$('.MOD_GC_12_5 #banner').cycle('pause');
		$('.pause-resume').removeClass('pause').addClass('resume');
    });
	
	/*DD artf1255036 When we click on the overlay or the navigation icon.*/
	$('.MOD_GC_12_5 #slideShowOverlay').click(function(e){
			e.preventDefault();
            e.stopPropagation();
            $(document.getElementById('product-navigation').getElementsByTagName('ul')).css('display', 'none');
            document.getElementById('slideShowOverlay').className = 'hideOverlay';
            $('.MOD_GC_12_5 #product-navigation-trigger a').removeClass('close').addClass('open');

            // Resume the banner slideshow
            $('.MOD_GC_12_5 #banner').cycle('resume');
            $('.pause-resume').removeClass('resume').addClass('pause');
    });
	
	
    // Image Popup
    $(".MOD_GC_12_5 #product-navigation ul li a").hover(function (e) {
        var msie = $.browser == 'msie' && $.browser.version < 7; // target ie6
        var placement = {
            x: 219,
            y: 0
        }; // top of zero and a left value equal to the width of the list item
        var pointPlacement = {
            x: -15,
            y: 0
        }; // top absolute default positioning of the popup tip
        // CALCULATED Y (TOP) POSITION
        var flyoutHeight = $(e.target).find('.flyout').outerHeight();
        var positionsY = $(e.target).parent().position(),
            positionY = positionsY.top + flyoutHeight; //The number + the height of the popup block.
        if (positionY > 420) {
            placement.y = 420 - positionY; //height of the banner container - the height of the list item - positionY
            pointPlacement.y = flyoutHeight + positionsY.top - 420;
        }

        // CALCULATED X (LEFT) POSITION
        var positionsX = $(e.target).parent().position(),
            positionX = positionsX.left;
        if ($(this).parent().parent().hasClass('col-4')) {
            if (msie) {
                placement.x = -220;
            } else {
                placement.x = -219;
            }

            point = '<span class="point_left">&nbsp;</span>';

            //point = '<img class="point" src="../img/arrows-r.png" />';
            pointPlacement.x = 212;
            $('.col-4').css('z-index', '5');
        } else {
            $('.col-4').css('z-index', '1');
        }

        // (K) modified as per artf1162987
        if (placement.x == 219) {
            point = '<span class="point arrow_left">&nbsp;</span>';
			//point = '<img class="point" src="../img/arrows-l.png" />';
            pointPlacement.x = -15;
        } else if (placement.x == -219) {
            point = '<span class="point arrow_right">&nbsp;</span>';
			//point = '<img class="point" src="../img/arrows-r.png" />';
            pointPlacement.x = 212;
        }



        //		if (positionX >= 500) {
        //	      	if (msie) {
        //				placement.x = -220; // If the left position touches the outside container (set at 500) go the opposite direction
        //	    	  	$('.MOD_GC_12_5 #product-navigation ul').css('position', 'static');
        //			} else {
        //				placement.x = -219; // If the left position touches the outside container (set at 500) go the opposite direction
        //			}
        //			point = '<img class="point" src="../img/arrows-r.png" />';
        //			pointPlacement.x = 212;
        //		} else {
        //			point = '<img class="point" src="../img/arrows-l.png" />';
        //		}
        // (K) modified as per artf1162987		
        $(this).find('.flyout').prepend(point).find('span.point').css({
            top: pointPlacement.y,
            left: pointPlacement.x
        });
        $(this).find('.flyout').css({
            top: placement.y,
            left: placement.x
        }).show();
    }, function () {
        $(this).find('.flyout').find('.point').remove();
        $(this).find('.flyout').hide();
    });

    // MOD_GC_12_2
    $('.MOD_GC_12_2 .item').mouseenter(function () {
        $(this).addClass('on');
        $('.nav + .MOD_GC_13').hide();
        $(this).find('.subItem').show();
    });
    $('.MOD_GC_12_2 .item').mouseleave(function () {
        $(this).removeClass('on');
        $('.nav + .MOD_GC_13').show();
        $(this).find('.subItem').hide();
    });
    $('.MOD_GC_12_2 .item > a').focus(function () {
        $(this).parent().addClass('on');
        $('.nav + .MOD_GC_13').hide();
        $(this).next().show();
    });
    $('.MOD_GC_12_2 .item > a').blur(function () {
        $(this).parent().removeClass('on');
        $('.nav + .MOD_GC_13').show();
        $(this).next().hide();
    });

    $('.MOD_FO_4').mouseenter(function () {
        if (!$(this).hasClass('off')) {
            // add blank li to keep stuff beneath from moving
            /* DD Commented the below code for artf1218172
			if ($(this).is(':last-child')) {
                $(this).parent().append('<li> </li>').css("display", "block");
            }*/

            $(this).addClass('oon');
            //$(this).next().css('margin-top','25px');/*(K) as per artf1164296 */
        }
    }).mouseleave(function () {
        $(this).removeClass('oon');        
    });
	
    $('.MOD_FO_4 a').focus(function () {
		var _this = $(this).parent('li');
        if (!_this.hasClass('off')) {           
            _this.addClass('oon');
        }
    }).blur(function () {
		var _this = $(this).parent('li');
        _this.removeClass('oon');        
    });

    $('.MOD_FO_6d h3').click(function () {
        if (!$(this).hasClass('open')) {
            $('.MOD_FO_6d h3').removeClass('open').next().hide();
            $(this).next().show();
            $(this).addClass('open');
        } else {
            $('.MOD_FO_6d h3').removeClass('open').next().hide();
        }
    });

/*$('.MOD_FO_6a h3, .MOD_FO_6b h3, .MOD_FO_6c h3, .MOD_GC_23a h3, .MOD_GC_22a h3, .MOD_FO_7a h3, .MOD_FO_13 h3').toggle(function() {
		$(this).next().show();
		$(this).addClass('open');
	}, function() {
		$(this).next().hide();
		$(this).removeClass('open');
	});*/
	
	$('.MOD_FO_6a h3, .MOD_FO_6a .secondary_subhead, .MOD_FO_6b h3, .MOD_FO_6c h3, .MOD_GC_23a h3, .MOD_GC_22a h3, .MOD_FO_7a h3, .MOD_FO_13 h3').bind('click', function () {	
		showHidePanel(this);
    });
	
	$('.model-special-offer .MOD_FO_6a h3,.model-special-offer .MOD_FO_6a .primary_subhead').unbind('click');
	$('.model-special-offer .MOD_FO_6a h3 a,.model-special-offer .MOD_FO_6a .primary_subhead a').bind('click', function () {	
		showHidePanel($(this).parent());
    });
	
	
	/* DD Commented it out because we are using the same show/hide panel in PS
	$('.MOD_FO_6a h3, .MOD_FO_6b h3, .MOD_FO_6c h3, .MOD_GC_23a h3, .MOD_GC_22a h3, .MOD_FO_7a h3, .MOD_FO_13 h3').click(function () {
        showHidePanel(this);
    });
	*/

    $('.MOD_GC_22a div').css('display', 'none');

    $('.MOD_FO_6a a').click(function () {
        $('.collapse_all').removeClass('disabled');
        $('.expand_all').removeClass('disabled');
    });

    $('.collapse_all').addClass('disabled');

    $(".expand_all").click(function () {
        $(this).parent().parent().find('h3, .secondary_subhead').next().show();
        $(this).parent().parent().find('h3, .secondary_subhead').addClass('open');
        $('.collapse_all').removeClass('disabled');
        $('.expand_all').addClass('disabled');
    });

    $(".collapse_all").click(function () {
        $(this).parent().parent().find('h3, .secondary_subhead').next().hide();
        $(this).parent().parent().find('h3, .secondary_subhead').removeClass('open');
        $('.collapse_all').addClass('disabled');
    });

    $('.itemContent .item').each(function () {
        $('.modal .paginator').append('<li><a href="javascript:void(0)">&nbsp;</a></li>');
    });

    $('.modal .paginator li:first-child').children('a').addClass('on');
    $('.itemContent .item:first-child').addClass('on');

    $('.modal .next, .modal .prev').click(function () {

        obj = $(this).attr('class');
        var ttl = ($('.itemContent .item').length) - 1;
        var ind = 0;

        for (i = 0; i <= ttl; i++) {
            indice = $('.itemContent .item').eq(i).hasClass('on')
            if (indice == true) {
                active = i;
            }
        }

        switch (obj) {
        case "prev":
            if (active == 0) {
                $('.modal .paginator li').eq(0).find('a').removeClass('on');
                $('.modal .paginator li').eq(ttl).find('a').addClass('on');
                $('.itemContent .item').eq(0).removeClass('on');
                $('.itemContent .item').eq(ttl).addClass('on');
            } else {
                $('.modal .paginator li').eq(active).find('a').removeClass('on');
                $('.modal .paginator li').eq(active - 1).find('a').addClass('on');
                $('.itemContent .item').eq(active).removeClass('on');
                $('.itemContent .item').eq(active - 1).addClass('on');
            }
            break;
        case "next":
            if (active == ttl) {
                $('.modal .paginator li').eq(ttl).find('a').removeClass('on');
                $('.modal .paginator li').eq(0).find('a').addClass('on');
                $('.itemContent .item').eq(ttl).removeClass('on');
                $('.itemContent .item').eq(0).addClass('on');
            } else {
                $('.modal .paginator li').eq(active).find('a').removeClass('on');
                $('.modal .paginator li').eq(active + 1).find('a').addClass('on');
                $('.itemContent .item').eq(active).removeClass('on');
                $('.itemContent .item').eq(active + 1).addClass('on');
            }
            break;
        }
    });

    $('.listItems .ctItem').hide();

    $('.listItems h3').click(function () {
        $(this).toggleClass('on');
        $(this).next().toggle();
    });


    // MOD_GC_23a
    $('.rTable').attr('style', 'position: relative;');
    $('.rTable:first').show();

    $('.rTable .tablepag .prev_column').hide();
    $('.rTable .tablepag .next_column').hide();

    var ttl_columns = $('.rTable table:first thead th').length - 1;

    var ttl_page = 0;
    if (ttl_columns % 3 == 0) {
        ttl_page = ttl_columns / 3
    } else {
        ttl_page = parseInt(ttl_columns / 3) + 1
    }

    for (i = 0; i < ttl_page; i++) {
        if (i == 0) {
            $('.rTable .tablepag span').append('<a rel=' + i + ' class="p' + i + ' on">' + (i + 1) + '</a>');
        } else {
            $('.rTable .tablepag span').append('<a rel=' + i + ' class="p' + i + '">' + (i + 1) + '</a>');
        }
    }

    $('.rTable .tablepag span a').click(function () {
        // Click function for little yellow paging buttons
        $('.rTable .tablepag span a').removeClass('on');
        var index = $(this).attr('rel');
        paging_columns(index);
        $(this).addClass('on');
    });

    function paging_columns(page) {
        // called by paging buttons to move table to approproate position
        $('.rTable div:not(".fixed") table').each(function () {
            $(this).animate({
                left: (-(482) * page) - 121
            }, 300, showhide_buttonnav(page));
            $('.p' + page).addClass('on');
        });
    }

    if (ttl_columns > 3) {
        // if enough columns to scroll, show scroll button
        $('.rTable .tablepag .next_column').show();
    }

    $('.rTable div:not(".fixed") table').width((186 * ttl_columns) - 14);

    $('.rTable table').each(function () {
        $(this).parent().before('<div class="fixed" style="position:absolute;width:162px;overflow:hidden"></div>');
        $(this).clone().appendTo($(this).parent().prev());
    });
    $('.rTable .fixed th:first').attr('style', 'width:142px !important');

    //$('.rTable .fixed table tr th').css('backgroundColor','#f1f1f1')
/*$('.rTable div:not(".fixed") tr').each(function() {
		$(this).find('th:first, td:first').remove();
	});
	$('.rTable .fixed tr').each(function() {
		$(this).find('th:last, td:last').remove();
	});*/
    //.before('<div class="scroll" style="display: block; position: relative; left: 162px; width: 502px; overflow: hidden;">')
/*$('.rTable table').each(function() {
        var column_title = null;
        var column_body = null;
        $(this).find('thead tr').each(function() {
            column_title = '<tr><th style="height:'+($(this).height()+10)+'px;background:none">&nbsp;</th></tr>';
            $(this).parent().parent().before('<div style="position:absolute; display: block; width: 164px;"><table class="fixed"><thead>' + column_title + '<tbody>');
            $(this).find('th:first').remove();
        });
        $(this).find('tbody tr').each(function() {
            column_body = '<tr><td style="height:'+($(this).height()+10)+'px">' + $(this).find('td:first').html() + '</td></tr>';
            $(this).parent().parent().prev().find('tbody').append(column_body);
            $(this).parent().parent().prev().find('tbody tr:even').addClass('bg');
            $(this).find('td:first').remove();
        });
    }).before('<div class="scroll" style="display: block; position: relative; left: 162px; width: 502px; overflow: hidden;">');*/



    $('.rTable div:not(".fixed") table').each(function () {
        $(this).attr('style', 'position: relative;');
        $(this).appendTo($(this).prev());
        $(this).width(184 * ttl_columns);
    });
    var current = 0;

    $('.rTable .next_column').click(function () {
        $('.rTable .tablepag span a').removeClass('on');
        var el = $('.rTable div:not(".fixed") table');
        var position = el.position();
        current = 0;
        $('.rTable div:not(".fixed") table').animate({
            left: position.left - 482
        }, 300, function () {
            var e = $('.rTable div:not(".fixed") table');
            var f = e.position()
            current = (f.left * -1) / 486; //
            $('.p' + Math.round(current)).addClass('on');
            showhide_buttonnav(Math.round(current));
        });
        $('.rTable .tablepag .prev_column').show();
    });

    $('.rTable .prev_column').click(function () {
        // left scroll button clicked
        $('.rTable .tablepag span a').removeClass('on');
        var el = $('.rTable div:not(".fixed") table');
        var position = el.position()
        $('.rTable div:not(".fixed") table').animate({
            left: position.left + 482
        }, 300, function () {
            var e = $('.rTable div:not(".fixed") table');
            var f = e.position()
            current = (f.left * -1) / 485;
            $('.p' + Math.round(current)).addClass('on');
            showhide_buttonnav(Math.round(current));
        });
        $('.rTable .tablepag .next_column').show();
    })

    function showhide_buttonnav(pos) {
        // called ten times when scrolled?
        $('.rTable .prev_column, .rTable .next_column').show();
        if (pos == 0) {
            $('.rTable .prev_column').hide();
        }
        if (pos == (ttl_page - 1)) {
            $('.rTable .next_column').hide();
        }
    }

    $('.chooseModels').click(function () {
        var pos = $(this).offset();
        var postab = $('.tabContent').parent().offset();
        $('.addRemoveModel').hide().css('top', pos.top - (postab.top + 100));
        $('.addRemoveModel').css('left', pos.left - (postab.left + 20)).show();
    });
    $('.addRemoveModel a').click(function () {
        $('.addRemoveModel').hide();
    });

    $('.chooseModels').click(function () {
        // Called from clicking 'add/remove models', opens add/remove model box.
        var pos = $(this).offset();
        var postab = $('.tabContent').parent().offset();
        if (postab) {
            $('.addRemoveModel').hide().css('top', pos.top - (postab.top + 100));
            $('.addRemoveModel').css('left', pos.left - (postab.left + 20)).show();
        } else {
            // if not being loaded within a tab
            $('.addRemoveModel').hide().css('top', pos.top + 20);
            $('.addRemoveModel').css('left', pos.left - 20).show();
        }
    });

    $('.addRemoveModel a').click(function () {
        // closes the add/remove model box
        $('.addRemoveModel').hide();
    });


    $('#checkAllAutoLeft').click(function () {
        $(".checkLeft").attr('checked', $('#checkAllAutoLeft').is(':checked'));
    });

    $('#checkAllAutoRight').click(function () {
        $(".checkRight").attr('checked', $('#checkAllAutoRight').is(':checked'));
    });

    $('.listCompare li input, .listCompare2 li input, .topCompare input').click(function () {
        var a = 0;
        var b = 0;
        $('.listCompare li input').each(function () {
            if ($(this).attr('checked')) {
                a++;
            }
        });
        $('.listCompare2 li input').each(function () {
            if ($(this).attr('checked')) {
                b++;
            }
        });
        if (a >= 1 && b >= 1) {
            $('.MOD_FO_10 .header a').removeClass('noCompareModels').addClass('compareModels')
        } else {
            $('.MOD_FO_10 .header a').removeClass('compareModels').addClass('noCompareModels')
        }
        a = 0;
        b = 0;
    });

    $('.listType a').click(function () {
        $('.listType a').removeClass('on');
        $(this).addClass('on')
        $(this).parent().next().next().removeClass('list, grid');
        $(this).parent().next().next().addClass($(this).attr('rel'));
        $('.MOD_GC_18 .grid li:nth-child(3n)').css('marginRight', '0');
    });

    // adv search
    /* DD removed this while jquery update didn't found any references for the code.
    $(".UTIL_1_1_menu ul li").toggle(function () {
        $(this).children().next().css('display', 'block');
        $(this).addClass('open');
        $(this).addClass('on');
    }, function () {
        //$(this).css('display','none');
        $(this).removeClass('open');
        $(this).removeClass('on');
        $(".UTIL_1_1_menu > ul > li > ul").css('display', 'none');
    });

    $(".UTIL_1_1_menu ul li ul li ul li").click(function () {
        $(this).removeClass('open');
        $(this).removeClass('on');
    });

    $(".UTIL_1_1_menu ul li span").click(function () {
        $(this).removeClass('on');
        $(this).removeClass('open');
    });
*/
    /* pag reg-homepage *//*RS WCAG II*/
    $('.homepage-slide-op li:first').show();
    $('.homepage-slide-thumblist li:first').addClass('curr');

    $('.MOD_GC_12_2b *').bind({
        mouseover: function () {
            clearInterval(intervalo2);
        },
        mousemove: function () {
            clearInterval(intervalo2);
        },
		focusin: function () {
            clearInterval(intervalo2);
        },
        focus: function () {
            clearInterval(intervalo2);
        }
    });

    $('.MOD_GC_12_2b').bind('mouseout', function (evt) {
        evt.stopPropagation();
        timeIntervalo2();
    });
	
	/*RS WCAG II*/
	$('.MOD_GC_12_2b').bind('keydown', function (evt) {
        var _keyCode = (evt.which) ? evt.which : evt.keyCode;
		if(_keyCode === 9){
			var o = $('.MOD_GC_12_2b').find('*');
			var focusableItems;
			focusableItems = o.filter(focusableElementsString).filter(':visible');
			var focusedItem;
			focusedItem = jQuery(document.activeElement);
			var numberOfFocusableItems;
			numberOfFocusableItems = focusableItems.length;
			var focusedItemIndex;
			focusedItemIndex = focusableItems.index(focusedItem);
			if(focusedItemIndex==0 || focusedItemIndex==numberOfFocusableItems-1){
				timeIntervalo2();
			}
		}
    });
	/*
	$('body').bind('click', function(evt){
		var target = $( evt.target );
		if(target.is( ".MOD_GC_12_2b" ) ||  target.parents( ".MOD_GC_12_2b" ).length == 0){
			timeIntervalo2();
		}
	});
	*/
    $('.homepage-slide-op li').each(function (i) {
        $(this).attr('id', 'homepage-slide-' + ++i + '');
    });
    $('.homepage-slide-thumblist li').each(function (i) {
        $(this).find('a').attr('href', '#homepage-slide-' + ++i + '');
    });

    function timeIntervalo2() {
		/*RS WCAG II*/
		if($('.homepage-slide-op-prev .homepage_carousel_prev a').attr('href') == 'javascript:void(0);' || $('.homepage-slide-op-next .homepage_carousel_next a').attr('href') == 'javascript:void(0);'){
			var currFirst = $('.homepage-slide-op li:visible');
			homePageHeroTitle(currFirst);
			$('.homepage-slide-op-prev .homepage_carousel_prev a span').attr('title', langPrev);
			$('.homepage-slide-op-next .homepage_carousel_next a span').attr('title', langNext);
		}
        intervalo2 = setInterval(function () {
            //alert('enter');
            var curr = $('.homepage-slide-op li:visible');
            var currTumb = $('.homepage-slide-thumblist li.curr');
            curr.hide();
            if (curr.is(':last-child')) {
                $('.homepage-slide-op li:first').show();
                $('.homepage-slide-thumblist li:first').addClass('curr');
            } else {
                curr.next().show();
                currTumb.next().addClass('curr');
            }
            currTumb.removeClass('curr');
			var currActive = $('.homepage-slide-op li:visible');
			/*RS WCAG II*/
			homePageHeroTitle(currActive);
        }, getIntervalTime());
    }

    function getIntervalTime() {
        return transitionTime;
    }

    function timeIntervalo3() {
        intervalo2 = setInterval(function () {
            var curr = $('.homepage-slide-op li:visible');
            var currTumb = $('.homepage-slide-thumblist li.curr');
            curr.hide();
            if (curr.is(':last-child')) {
                $('.homepage-slide-op li:first').show();
                $('.homepage-slide-thumblist li:first').addClass('curr');
            } else {
                curr.next().show();
                currTumb.next().addClass('curr');
            }
            currTumb.removeClass('curr');
        }, 3000);
    }
    timeIntervalo2();

    $('.homepage-slide-thumblist li').click(function (event) {
        event.preventDefault();
        var openImg = $('a', this).attr('href');
        $('' + openImg + '').show();
        $('' + openImg + '').siblings('.homepage-slide-op li').hide();
        $(this).addClass('curr');
        $(this).siblings().removeClass('curr');
		/*RS WCAG II*/
		var currActive = $('.homepage-slide-op li:visible');
		homePageHeroTitle(currActive);
        clearInterval(intervalo2);
        //timeIntervalo3();
    });

        // qc296 starts //
	/*
	$('#homepage-slide-op-next').click(function () {
        var curr = $('.homepage-slide-op li:visible');
        var currTumb = $('.homepage-slide-thumblist li.curr');
        curr.hide();
        if (curr.is(':last-child')) {
            $('.homepage-slide-op li:first').show();
            $('.homepage-slide-thumblist li:first').addClass('curr');
        } else {
            curr.next().show();
            currTumb.next().addClass('curr');
        }
        currTumb.removeClass('curr');
        clearInterval(intervalo2);
        //timeIntervalo3();
    });

    $('#homepage-slide-op-prev').click(function () {
        var curr = $('.homepage-slide-op li:visible');
        var currTumb = $('.homepage-slide-thumblist li.curr');

        curr.hide();
        if (curr.is(':first-child')) {
            $('.homepage-slide-op li:last-child').show();
            $('.homepage-slide-thumblist li:last-child').addClass('curr');
        } else {
            curr.prev().show();
            currTumb.prev().addClass('curr');
        }
        currTumb.removeClass('curr');
        clearInterval(intervalo2);
        //timeIntervalo3();
    });
	*/

	/*DD artf1232530*/	
	/*
	$('#homepage-slide-op-prev a span, #homepage-slide-op-next a span').hover(function () {
		$(this).addClass("hover");
	}, function () {
		$(this).removeClass("hover");
	});
	*/
	$('.homepage-slide-op-span-prev, .homepage-slide-op-span-next').hover(function () {
		$(this).addClass("hover");
	}, function () {
		$(this).removeClass("hover");
	});
	
	$('.homepage-slide-op-span-prev').click(function () {
		var curr = $('.homepage-slide-op li:visible');
        var currTumb = $('.homepage-slide-thumblist li.curr');
        curr.hide();
        if (curr.is(':first-child')) {
            $('.homepage-slide-op li:last-child').show();
            $('.homepage-slide-thumblist li:last-child').addClass('curr');
        } else {
            curr.prev().show();
            currTumb.prev().addClass('curr');
        }
        currTumb.removeClass('curr');
		/*RS WCAG II*/
		var currActive = $('.homepage-slide-op li:visible');
		homePageHeroTitle(currActive);
        //clearInterval(intervalo2);
		return false;
	});
	$('.homepage-slide-op-span-next').click(function () {
		var curr = $('.homepage-slide-op li:visible');
        var currTumb = $('.homepage-slide-thumblist li.curr');
        curr.hide();
        if (curr.is(':last-child')) {
            $('.homepage-slide-op li:first').show();
            $('.homepage-slide-thumblist li:first').addClass('curr');
        } else {
            curr.next().show();
            currTumb.next().addClass('curr');
        }
        currTumb.removeClass('curr');
		/*RS WCAG II*/
		var currActive = $('.homepage-slide-op li:visible');
		homePageHeroTitle(currActive);
        //clearInterval(intervalo2);
        return false;
	});
	// end of qc296
	
	// end pag reg-homepage

    // slide MOD_GC_12_2a
    var slideTag = ".slide-op li";
    var slideNavTag = ".slide-nav li";
    var intervalo;
    var timeI = 1000;
    var currNav = 0;
    var slideLength = $(slideTag).length - 1;

    $('.slide-op').parent().append('<div class="slide-navigation"></div>');

    $('' + slideTag + ':first').show();
    $('<ul class="slide-nav"></ul>').appendTo('.slide-navigation');
    $('.slide-navigation').append('<a href="" class="slide-pause">Pause</a>').click(function (event) {
        event.preventDefault();
        clearInterval(intervalo);
    });

    $(slideTag).each(function (qnt) {
        var qnt = ++qnt;
        $(this).attr('id', 'slide-' + qnt + '');
        $('<li><a href="javascript:void(0)" targetId="#slide-' + qnt + '">' + qnt + '</a></li>').appendTo('.slide-nav');
    });

    $('' + slideNavTag + ':first').addClass('curr');

    function timeIntervalo() {
        intervalo = setInterval(function () {
            var curr = $('.slide-op li:visible');
            curr.hide();
            $(slideNavTag).removeClass('curr');
            if (currNav >= slideLength) {
                currNav = 0;
            }
            if (curr.is(':last-child')) {
                $('' + slideTag + ':first').show();
                $('' + slideNavTag + ':first').addClass('curr');
            } else {
                curr.next(slideTag).show();
                $('' + slideNavTag + ':eq(' + ++currNav + ')').addClass('curr');
            }
        }, timeI);
    }
    timeIntervalo();

    $('' + slideNavTag + ' a').click(function (event) {
        event.preventDefault();
        var openId = $(this).attr('targetId');
        $('' + openId + '').show();
        $('' + openId + '').siblings(slideTag).hide();

        $('#debug').text(openId);
        $(this).parents('li').addClass('curr');
        $(this).parents('li').siblings().removeClass('curr');
        clearInterval(intervalo);
    });
    // end 
    // caption
/*
	$(".MOD_GC_3 img").each(function(){
		var alt = $(this).attr("alt"); 
		var imgsize = $(this).width();
		if(alt){
			$(this).wrap("<div class='caption'></div>");
			$(this).parent('.caption').width(imgsize);	
			$(this).parent('.caption').append("<p>"+alt+"</p>");
			$(".MOD_GC_3 img[align='left']").parent('.caption').addClass('imageLeft');
			$(".MOD_GC_3 img[align='right']").parent('.caption').addClass('imageRight');
		}		
	});
	*/

    // tab_module	
    $('.tab_module_content.on').show();


    /*(K) added to make the worldwide tabs working*//*RS WCAG3*/
    $('.tab_module > li, .tab_module .tab_heading').click(function () {
        if (!$(this).hasClass('on')) {
            $(this).siblings().removeClass('on');
            var openClass = $(this).attr('class');
				openClass = $.trim(openClass.replace(/secondary_subhead_small|tab_heading_li|tab_heading|secondary_subhead/g,''));
            $('.tab_module_content').hide();
            $('.' + openClass + '').addClass('on').show();
            $('.' + openClass + '').siblings().removeClass('on');
            $('#mainNav .on').hide();
            $(this).addClass('on');
            //alert($('.'+openClass+'').find('.carousel-viewport').length);
        }
    });

    /*(K) added to make the worldwide tabs working*/

/*DD 07-Dec-11 Added the below codes.

.MOD_NO_4 li.nvl_2 h3:last-child span
.MOD_NO_4 li.nvl_3 ul:last-child h3:last-child span

*/
    // MOD_NO_4
    $('.MOD_NO_4 li.nvl_2 ul li:last-child, .MOD_NO_4 li.nvl_2 h3:last-child span, .MOD_NO_10 li.nvl_2 ul li:last-child').css('border', 'none');
    $('.MOD_NO_4 li.nvl_2_1 ul li:last-child, .MOD_NO_10 li.nvl_2_1 ul li:last-child').css('border', 'none');
    
	$('.MOD_NO_4 li.nvl_3 ul li:last-child a, .MOD_NO_10a li.nvl_3 ul li:last-child a, .MOD_NO_10b li.nvl_3 ul li:last-child a, .MOD_NO_10a li.nvl_3 ul h3:last-child a, .MOD_NO_10b li.nvl_3 ul h3:last-child a, .MOD_NO_4 li.nvl_3 ul:last-child h3:last-child span').css('border', 'none');

	$('.MOD_NO_4 li.nvl_3_1 ul li:last-child a, .MOD_NO_4 li.nvl_3_1 ul li:last-child h3 a, .MOD_NO_4 li.nvl_3_1 ul h3:last-child a, .MOD_NO_4 li.nvl_3_1 ul li:lastchild h3:last-child a').css('border', 'none'); /*DD artf1215990 / artf1219101 .MOD_NO_4 li.nvl_3_1 ul h3:last-child a*/
	
	$('.ie7 .MOD_NO_4 li.nvl_3_1 ul li:last-child a, .ie7 .MOD_NO_4 li.nvl_3_1 ul li:last-child h3 a, .ie7 .MOD_NO_4 li.nvl_3_1 ul h3:last-child a').css('border-bottom','1px dotted #9b9b9b');/*DD artf1254228*/
	$('.MOD_NO_4 li.nvl_3_1 ul li:last-child a, .ie7 .MOD_NO_4 li.nvl_3_1 ul > h3:last-child a').css('border', 'none'); /*DD Specifically for IE7*/
	$('.MOD_NO_4 li.nvl_3_1 ul li:last-child h3:last-child a').css('border-top', '1px dotted #9b9b9b'); /*DD Specifically for IE7 | artf1254228*/
	$('.MOD_NO_4 li.nvl_4 ul li:last-child a, .MOD_NO_10a li.nvl_4 ul li:last-child a, .MOD_NO_4 li.nvl_4 ul h3:last-child a').css('border', 'none'); /*DD artf1224315, artf1246893*/
	
	/*$('.MOD_NO_10a li.nvl_3 ul li div.flyout_dropdown li:nth-child(even), .MOD_NO_10b li.nvl_3 ul li div.flyout_dropdown li:nth-child(even)').addClass('last_item');*/

	/* MOD_NO_4 >  nvl_2 has no level l */
	if($('.MOD_NO_4 li.nvl_2 ul').length === 0) {
		$('.MOD_NO_4 li.nvl_2').addClass('list_last_item');
	}
	/* MOD_NO_4 >  nvl_3 has no level  */
	if($('ul.MOD_NO_4 li.nvl_3').find('ul li').length === 1 || $('ul.MOD_NO_4 li.nvl_3').find('ul li').length === 0 ) {
		$('.MOD_NO_4 li.nvl_3 ul li.leftnav_active_li, .MOD_NO_4 li.nvl_3 ul h3, .MOD_NO_4 li.nvl_3 ul .primary_subhead').parents('li.nvl_3').addClass('list_last_item');
	}

	/* MOD_NO_4 >  nvl_4 has active level  */
	if($('ul.MOD_NO_4 li.nvl_4').find('ul li:last:visible').hasClass('nvl_4_on') === true) {
		$('.MOD_NO_4 li.nvl_4').css('border-bottom','none');
	}
	

    //Added by Vikas Khera for Artifact artf1163280
    //$('.MOD_NO_4 li.nvl_2').css('border','none'); 
    var foundLI = $('MOD_NO_4 li.nvl_3 ul').find("li");
    if (foundLI.length == 0) {
        $('.MOD_NO_4 li.nvl_3 ul h3 a').css('border', 'none')
    }


    var foundLI2 = $('MOD_NO_4 li.nvl_2').find("ul");
    if (foundLI2.length == 0) {
        //$('.MOD_NO_4 li.nvl_2 h3').css('border-bottom','1px solid #ccc')  Commented by Swarn
        $('.MOD_NO_4 li.nvl_2 h3 a').css('border', 'none')
    }
	
    // new lightbox
    if ($.nyroModalSettings) {
        $.nyroModalSettings({
            // debug: true,
            processHandler: function (settings) {
				
                var url = settings.url;
				
				if(url != null){
					var modalWidth = getLinkVars(url)["width"];
					var modalHeight = getLinkVars(url)["height"];
					
					
					if (url && url.indexOf('../../../media/player/playerdata/Player.swf'/*tpa=http://www.deere.com/media/player/playerdata/Player.swf*/) == 0) {
						$.nyroModalSettings({
							// john deere video player default width & height
							height: 480,
							width: 720
						})
					}
					if (url && url.indexOf('//www.deere.com/en_US/ProductCatalog/HO/media/flash') == 0) {
						$.nyroModalSettings({
							// john deere 360 player default width & height
							height: 460,
							width: 614
						})
					}else if (url && url.indexOf('//www.deere.com/media/player') == 0){
						/*PK artf1232486 to have fixed height and width when video playing through player.html*/
						if(modalWidth == undefined && modalHeight == undefined){ /*DD When width and height are not defined.*/
							$.nyroModalSettings({
								minHeight: 526,
								minWidth: 858
							})						
						}else if(modalWidth != undefined && modalHeight == undefined){ /*DD When height is not defined.*/
							$.nyroModalSettings({
								minHeight: 526,
								minWidth: modalWidth
							})						
						}else if(modalWidth == undefined && modalHeight != undefined){ /*DD When width is not defined.*/
							$.nyroModalSettings({
								minHeight: modalHeight,
								minWidth: 858
							})
						}else if(modalWidth != undefined || modalHeight != undefined){ /*DD When width and height are defined.*/
							$.nyroModalSettings({
								minHeight: modalHeight,
								minWidth: modalWidth
							})
						}
					}else {
						$.nyroModalSettings({
							// john deere 360 player default width & height
							minHeight: modalHeight,
							minWidth: modalWidth
						})
					}
				}
            },
            endShowContent: function (elts, settings) {
				/* RS WCAG */
				//$('#wrap').attr('aria-hidden','true');
				$('#nyroModalWrapper #closeBut').focus();
				if($('.embed_new_player').length > 0){/*RS QC484 */
					(Object.size(newAkamaiPlayer) > 0) ? pauseAll(elts, "noId") : "";
				}
                $('.resizeLink', elts.contentWrapper).click(function (e) {
                    e.preventDefault();
                    $.nyroModalSettings({
                        width: Math.random() * 1000,
                        height: Math.random() * 1000
                    });
                    return false;
                });
                $('.bgLink', elts.contentWrapper).click(function (e) {
                    e.preventDefault();
                    $.nyroModalSettings({
                        bgColor: '#' + parseInt(255 * Math.random()).toString(16) + parseInt(255 * Math.random()).toString(16) + parseInt(255 * Math.random()).toString(16)
                    });
                    return false;
                });
            },
			endRemove: function(elts, settings){
				/*RS WACG 
				$('#wrap').attr('aria-hidden','false');*/
				$(_focusedElementBeforeModal).focus();
			}
        });
        $('#manual').click(function (e) {
            e.preventDefault();
            var content = 'Content wrote in JavaScript<br />';
            jQuery.each(jQuery.browser, function (i, val) {
                content += i + " : " + val + '<br />';
            });
            $.fn.nyroModalManual({
                bgColor: '#3333cc',
                content: content
            });
            return false;
        });
        $('#manual2').click(function (e) {
            e.preventDefault();
            $('#imgFiche').nyroModalManual({
                bgColor: '#cc3333'
            });
            return false;
        });
        $('#myValidForm').submit(function (e) {
            e.preventDefault();
            if ($("#myValidForm :text").val() != '') {
                $('#myValidForm').nyroModalManual();
            } else {
                alert("Enter a value before going to " + $('#myValidForm').attr("action"));
            }
            return false;
        });
        $('#block').nyroModal({
            'blocker': '#blocker'
        });

        function preloadImg(image) {
            var img = new Image();
            img.src = image;
        }

        //preloadImg('Unknown_83_filename'/*tpa=http://www.deere.com/common/deere-resources/js/img/ajaxLoader.gif*/);
        //preloadImg('Unknown_83_filename'/*tpa=http://www.deere.com/common/deere-resources/js/img/prev.gif*/);
        //preloadImg('Unknown_83_filename'/*tpa=http://www.deere.com/common/deere-resources/js/img/next.gif*/);
    }

    $('.MOD_GC_12_1 .icons li a').hover(function () {
        $(this).addClass('hover');
    }, function () {
        $(this).removeClass('hover');
    });

    //added by vikash
    $('.MOD_GC_12_1 .icons li a, .MOD_GC_12_1 #MOD_GC_12_1_nav li a').bind('click', function () {
        if ($(this).hasClass('on')) {
            if ($($(this).parents('.MOD_GC_12_1')[0]).find('div[class~="' + $($(this).parents('li')[0]).attr('class') + '_Cont"]')[0]) {
                $($(this).parents('ul.icons, #MOD_GC_12_1_nav')[0]).find('li a').removeClass('on');
		/*QC486 | RS*/
                $($(this).parents('.MOD_GC_12_1')[0]).find('div[class~="' + $($(this).parents('li')[0]).attr('class') + '_Cont"]').removeClass('visible_content').addClass('hidden_content');
                $($(this).parents('.MOD_GC_12_1')[0]).find('.defaultView').addClass('visible_content');
            }
        } else {
            if ($($(this).parents('.MOD_GC_12_1')[0]).find('div[class~="' + $($(this).parents('li')[0]).attr('class') + '_Cont"]')[0]) {
                $($(this).parents('ul.icons, #MOD_GC_12_1_nav')[0]).find('li a').removeClass('on');
                $(this).addClass('on');
		/*QC486 | RS*/
                $($(this).parents('.MOD_GC_12_1')[0]).find('.view').removeClass('visible_content').addClass('hidden_content');
                $($(this).parents('.MOD_GC_12_1')[0]).find('div[class~="' + $($(this).parents('li')[0]).attr('class') + '_Cont"]').addClass('visible_content').removeClass('hidden_content');
				/* RS WCAG */
				var curenticonClass = $($(this).parents('li')[0]).attr('class');
				if(curenticonClass == 'gallery'){
					$($(this).parents('.MOD_GC_12_1')[0]).find('div[class~="' + curenticonClass + '_Cont"]').find('#pager-numbers a.activeSlide').focus();
				}
				if(curenticonClass == 'videos'){
					$($(this).parents('.MOD_GC_12_1')[0]).find('div[class~="' + curenticonClass + '_Cont"]').find('div.vidbox a')[0].focus();
				}
            }
        }
    });

/*artf1212946 Added by Durgesh to control the rotation time of Slides*/
    if ($('.MOD_GC_12_5 #banner').cycle) {

        $('.MOD_GC_12_5 #banner').cycle({
            speed: 800,
            timeout: rotationTime,
            pager: '#pager-numbers',
            pagere: 'mouseover',
            pauseOnPagerHover: true
        });

        $('.MOD_GC_12_5 .pause').toggle(function () {
            $('.MOD_GC_12_5 #banner').cycle('pause');
            $(this).removeClass('pause').addClass('resume');
        }, function () {
            $('.MOD_GC_12_5 #banner').cycle('resume');
            $(this).removeClass('resume').addClass('pause');
        });
    }
	
/*artf1212946 Added by Durgesh to control the rotation time of Slides*/
    if ($('.MOD_GC_12_5a #banner').cycle) {

        $('.MOD_GC_12_5a #banner').cycle({
            speed: 800,
            timeout: rotationTime,
            pager: '#pager-numbers',
            pagere: 'mouseover',
            pauseOnPagerHover: true
        });

        // KARAN Artifact artf1164079 : Play button disappears on hover 
        //if($('#pager-numbers'== 0)){
        //$('#pager').hide();
        //}
        // KARAN Artifact artf1164079 : Play button disappears on hover 
        $('.MOD_GC_12_5a .pause').toggle(function () {
            $('.MOD_GC_12_5a #banner').cycle('pause');
            $(this).removeClass('pause').addClass('resume');
        }, function () {
            $('.MOD_GC_12_5a #banner').cycle('resume');
            $(this).removeClass('resume').addClass('pause');
        });
    }


    if ($('.MOD_GC_12_2a #banner-small').cycle) {

        $('.MOD_GC_12_2a #banner-small').cycle({
            speed: 800,
            timeout: getTimeout,
            pager: '#pager-numbers',
            pagere: 'mouseover',
            pauseOnPagerHover: true
        });

        $('.MOD_GC_12_2a .pause').toggle(function () {
            $('.MOD_GC_12_2a #banner-small').cycle('pause');
            $(this).removeClass('pause').addClass('resume');
        }, function () {
            $('.MOD_GC_12_2a #banner-small').cycle('resume');
            $(this).removeClass('resume').addClass('pause');
        });
    }

    if ($('.MOD_GC_12_2a_green #banner-small').cycle) {

        $('.MOD_GC_12_2a_green #banner-small').cycle({
            speed: 800,
            timeout: getTimeout,
            pager: '#pager-numbers',
            pagere: 'mouseover',
            pauseOnPagerHover: true
        });

        $('.MOD_GC_12_2a_green .pause').toggle(function () {
            $('.MOD_GC_12_2a_green #banner-small').cycle('pause');
            $(this).removeClass('pause').addClass('resume');
        }, function () {
            $('.MOD_GC_12_2a_green #banner-small').cycle('resume');
            $(this).removeClass('resume').addClass('pause');
        });
    }


    //VIEW OTHER OPTIONS POPUP
    $('.buying_options .bg_btn').click(function () {
        var nextNode = $(this).next();
        var url = $(nextNode).text();
        //changed the code to handle opening in defined target: Moiz.
        var target = $(nextNode).attr('target');
        window.open(url, target);
        //window.open(url);
        return false;
    });
    $('.buying_options .btn').click(function () {
        //Removed code in this method as it was not needed: Pradeep
        return false;
    });

    $('#jdnpfhfl-en-us-row-1').parent().css('background', 'none')


    $('.MOD_GC_25 table tbody > tr:nth-child(even)').addClass('bg');






/* DD artf1213653 : Wrong style coming from js 
    if ($('.MOD_GC_3').length > 1) {
        $('.MOD_GC_3:last p:last').css('margin', '0px');
    }
*/

    $('.MOD_GC_6 .list a.nextFrame').hover(function () {
        $(this).addClass('nextHover');
    }, function () {
        $(this).removeClass('nextHover');
    });

    $('.MOD_GC_6 .list a.prevFrame').hover(function () {
        $(this).addClass('prevHover');
    }, function () {
        $(this).removeClass('prevHover');
    });

    /*DD - To make the tab text vertically middle*//*RS WCAG II*/
    $('.MOD_NO_2_S li a span, .tab_module li a span, .MOD_NO_2_S .tab_heading a span, .tab_module .tab_heading a span').vAlign();/*RS WCAG II*/
	
	
	/*DD Vertical Align Button*/
	$('.MOD_FO_10 .header .noCompareModels').verticalAlign();

    /*DD Custom Select Menu*/
    //$('#Category, #Subcategory, #Series ,#Subseries').ieSelectStyle();

    $('.generic_table tbody > tr:nth-child(even)').addClass('bg');

	
	/*DD artf1221188 : Configure different links for different locales
	$('#Ghid000001').attr('href', localeURL);*/
	
	setTimeout('showTab()', 100);
	
	/*DD Check if the browser mode is Quirk*/
	if(checkBrowserMode){
		setTimeout('createJointClassPagination()', 1000);
	}
	
	/*DD To open the first accordion as expanded on load in tabs.*/
	showHidePanelActive = $(".tabContent .MOD_FO_6a h3:first, .tabContent .MOD_FO_6a .secondary_subhead:first").hasClass("open");
	
	if (showHidePanelActive == false){
		$('.tabContent .MOD_FO_6a h3:first, .tabContent .MOD_FO_6a .secondary_subhead:first').trigger("click");
	}
	
	
	
	// Cookies Agreement Popup
	/*$(".region_homepage #rdoRemember").click(function() {
		if($(this).is(':checked')) {
			$(".cookies_agreement_popUp_anchor").trigger('click');			
		}
	});*/	
	$(".cookies_agreement_popUp_anchor").nyroModal({
		modal: false,		
		closeButton:false,		
		minHeight: 10, // Minimum height	
		cssOpt:{
			wrapper:{
				'border-top': 'none',
				'padding-top':'15px'
			},
			wrapper2:{
				'padding-top':'0px'
			},
			content:{
				'padding-top':'0px',
				'padding-bottom':'18px'				
			}
		},
		wrap:{
			div: '<div class="cookies_agreement_popUp_wrapper"></div>'
		},		
		endRemove: function() {
			if (document.myform.rdoRemember.checked==true && $('.cookies_agreement_popUp_content a.btn_secondary').hasClass('agree')) {
				putCookie('ckname',region_homepage_link,'2','/','http://www.deere.com/common/deere-resources/js/deere.com','');
			}
			window.location = region_homepage_link;
		}
	});	
	$(".region_homepage .region_column ul li a.enableRdoRemember").click(function(e) {		
		if (document.myform.rdoRemember.checked==true) {			
			e.preventDefault();
			region_homepage_link = $(this).attr('href');			
			$(".cookies_agreement_popUp_anchor").trigger('click');
			updated_cookies_agreement_content = $(this).parents('li').find(".cookies_agreement_popUp_content").html();
			$(".cookies_agreement_popUp_content").html(updated_cookies_agreement_content);
		}		
	});
	/* Enable remember my selection on click without pop up */
	$(".region_homepage .region_column ul li a.enableCookieWithoutPopup").click(function(e) {
		if (document.myform.rdoRemember.checked==true) {			
			e.preventDefault();
			region_homepage_link = $(this).attr('href');
			putCookie('ckname',region_homepage_link,'2','/','http://www.deere.com/common/deere-resources/js/deere.com','');
			window.location = region_homepage_link;
		}
	});
	$('.cookies_agreement_popUp_content a.btn_secondary').live('click',function(e) {	
		//$('.region_homepage #rdoRemember').attr('checked', true);
		e.preventDefault();
		$(this).addClass('agree');
		$.nyroModalRemove(); return false;
	});
	$('.cookies_agreement_popUp_content a.btn_tertiary, .cookies_agreement_popUp_content #closeBut').live('click',function(e) {
		e.preventDefault();
		$(this).removeClass('agree');
		//$('.region_homepage #rdoRemember').attr('checked', false);
		$.nyroModalRemove(); return false;
	});
	
	if ($('#grey_bg_layout')[0]) {
        var a = $('.bt .btn_primary span').width(),
            b = 84;
		var aBig = $('.bt .btn_primary_large span').width(),
			bBig = 65;
        //alert(a);
        if (a > b || aBig > bBig) {
            $('div.Fo1_content').addClass(' big_btn');
        }else if(aBig < bBig){
			$('div.Fo1_content').addClass('small_btn');
		}
    }
    $('.MOD_NO_19 ul li:nth-child(even)').addClass('noMargin');
    $('.MOD_GC_12_5a #banner').css('height', '458px');
    //(K) added to add grey BG in Mod FO 2
    //if ($('div').hasClass('MOD_FO_1')){
    //	$('#content table .ls-canvas').addClass('greybg');
    //}
    //(K) added to add grey BG in Mod FO 2
    $('.buyingOption').click(function () {
        $('#buying').show();
        $('.feature').hide();
		$('#buying a.close').focus();/*RS 14567402 */
    });
    $('#buying .close').click(function () {
        $('#buying').hide();
        $('.feature').show();
		$('p.bt a.buyingOption').focus();/*RS 14567402 */
    });
    //$('.Mod_GC_14_panel').height($('.MOD_GC_14_2_left').outerHeight())
    //$('.Mod_GC_14_panel').height($('.MOD_GC_14_2_right').outerHeight())
    $('.MOD_GC_12_5 #product-navigation-trigger a').width($('.MOD_GC_12_5 #product-navigation-trigger a span').outerWidth());

    $('.MOD_GC_12_5 #product-navigation-trigger a span').width($('.MOD_GC_12_5 #product-navigation-trigger a span').outerWidth() - 10);

    //(VK)artf1180188
    var explorespan = $('.MOD_GC_12_5 #product-navigation-trigger a span').outerHeight();
    if (explorespan < 30) {
        $('.MOD_GC_12_5 #product-navigation-trigger span').height(30);
    }
    $('.agree').click(function () {
        $('.terms').hide();
        $('.terms_agree').show();
    });

    // Table alternate gray color
    $('table.table_alternate_gray_row tr:nth-child(odd) td').addClass('alternate_row');
	
    // Print Contract Overview | MG
    $('a.printContractOverview').click(function(e) {
        e.preventDefault();
        window.print();
    });

    // Prevent click on specification header/features header on print preview
    $('body').delegate('.print_preview .MOD_FO_6a h3, .print_preview .MOD_FO_6a .secondary_subhead', 'click', function(e){		
		$(this).next().show();		
	});
	
	/* Global Home Page | Delete Cookie on Country Change */
	var _utilLink = $('.MOD_NO_6 #topNav #utilityNav li:first a')
	var _utilLinkHREF = _utilLink.attr('href');
	_utilLinkHREF = _utilLinkHREF + "?CC=true";
	_utilLink.attr('href', _utilLinkHREF);

    // On DOM ready excutig disablePrevNextLinks function    
    disablePrevNextLinks(); 
	
	$('body').delegate('.page_navigation a', 'click', function(e){
     	disablePrevNextLinksFocus();		
    });

	
	//on tab clicks if video gallery enabled 	
	$('body').delegate('.tab .video_gallery ul.MOD_NO_2_S li, .tab .video_gallery .tab_heading', 'click', function(e){
     	//disablePrevNextLinksFocus(); // bug fix for the issue reported on regresson R27 JDIM-2036
		disablePrevNextLinks();
    });	
	
	
	$.fn.showFixedTooltip();
	$.fn.showFixedTooltipResults();
	$.fn.showInternalTooltip();
	$.fn.showTooltipSPFH();
	
	 // Hover on Video gallery Small | MG
	/*$('.video_gallery.small .MOD_FO_27 .video_list_container').find('.video_list .video').hover(function() {
		$(this).find('.hover_element, .hover_element .video_description_title, .hover_element .video_description').show();
		if($(this).hasClass('playing')) {
			$(this).addClass('hover_element_playing');
		}
	}, function() {		
		$(this).find('.hover_element, .hover_element .video_description_title, .hover_element .video_description').hide();
		$(this).removeClass('hover_element_playing');
	});*/
	
	//Focus on MOD_FO_32, MOD_FO_28 Video gallery Small | MG
	$('.MOD_FO_32, .MOD_FO_28, .MOD_FO_34').find('.video_list .video a.video_play_link').focus(function() {
		$(this).parent('div.video').addClass('focus');	
	});
	$('.MOD_FO_32, .MOD_FO_28, .MOD_FO_34').find('.video_list .video a.video_play_link').focusout(function() {		
		$(this).parent('div.video').removeClass('focus');
	});
	
	//prevent click on video thumb  | Video gallery Small | MG	
	$('a.video_play_link').click(function(e) {
		e.preventDefault();
	});
	// Capture Enter Key and trigger events
	$(document).keydown(function(event){
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if(keycode == '13'){			
			$('.MOD_FO_32, .MOD_FO_28, .MOD_FO_34').find('.video_list .video').removeClass('focus'); //removed focus class on enter key
		} 
	});
	// Update tabindex for Shadowbox. The tab feature will keep rotating in the opened window. | MG
	$('body').delegate('#sb-container','keydown', function(event){trapTabKey($(this),event);})	
	
	/*
	/*  contact Us Electronic | Ajax Form Submit 
	$('form#contact_us_electronic').submit(function(e) {
		e.preventDefault();
		var postData = $(this).serializeArray();
		var URL = $(this).attr('action');
		$.ajax({
			url: URL,
			cache: false,
			type: "POST",
			data : postData,
			timeout:35000,
			success: function( data, textStatus, jqXHR ){
				hideLoader();
				$('#contact_us_electronic').hide();
				$('#contact_us_thank_you').show();
			  
			},
			error: function(jqXHR, textStatus, errorThrown){
				hideLoader();
				$('#contact_us_thank_you').hide();
				$('#contact_us_electronic').show();
			},
			beforeSend: function(){
				showLoader();
			}
		}); 
	});	
	*/
	
	/* Left Nav Merged Code */
	/* Left Nav - MOD_NO_4 | MG */
	$('ul.MOD_NO_4 li ul').find('li:last').addClass('list_last_item');
	$('ul.MOD_NO_4 li ul').find('li').next('h3').prev('li').addClass('list_last_item_anchor');
	$('ul.MOD_NO_4:first').find('li:first h3:first, li:first .primary_subhead:first').parent('li').addClass('first_level'); /*RS WCAG3*/

	
	/*RS June 29 some common changes in left-nav.js*/
	leftNavPage = ($("#jdtcwol-en-us-row-5-area-1").length > 0) ? true : false;
	
	$('input.placeholder').focus(function (e) {
		if (e.target.value == e.target.defaultValue) {
			e.target.value = '';
			$(this).css('color', '#333');
		}
	});

	$('input.placeholder').blur(function (e) {
		if (e.target.value == '') {
			e.target.value = e.target.defaultValue;
			$(this).css('color', '#999');
		}
	});
	// Add additional class to first .errMsg for left nav
	$('.leftColumn ul.MOD_NO_10b:first, .leftColumn ul.MOD_NO_4:first').next('.errMsg:first:visible').addClass('errMsgFirst');
	
	//IE7 and IE Quirk Mode fix for Left Nav | Only First Element exist in second level
	if ($('html').hasClass('quirk-mode') || $('html').hasClass('ie7')) {
		if ($('.leftColumn ul li').children('ul').children('li').length === 1) {
			$('.leftColumn ul li').children('ul').addClass('singleNodeULWrapper');
		}
	}
	/*End Left Nav Code Merge*/
	
	/* Hero Carousel*/
	if($('.hero_simple_carousel ul#banner-small li').length <= 1) {
		$('.hero_simple_carousel #pager .pause-resume').hide();
	}
	/* Open page hash for Hide Show Panel & Tabs */
	openPageHashInHideShowPanel();
	openPageHashInTabsPanel();
	$(window).bind('hashchange', function(e) {
		openPageHashInHideShowPanel();
		openPageHashInTabsPanel();
		showTab();
	});
	
	/* Rich media player - Href updates */
	$('a.urlAkamaiPlayer').each(function() {
		var currentHref = $(this).attr('href');
		$(this).attr('href', urlAkamaiPlayer + currentHref)
	});
	// YouTube changes
	$('a.Youtubeplayer').each(function() {
		var currentHref = $(this).attr('href');
		$(this).attr('href', Youtubeplayer + currentHref)
	});
	
	$('a.urlComponentPlayer').each(function() {
		var currentHref = $(this).attr('href');
		$(this).attr('href', urlComponentPlayer + currentHref)
	});
	
	$('a.urlStandardPlayer').each(function() {
		var currentHref = $(this).attr('href');
		$(this).attr('href', urlStandardPlayer + currentHref)
	});
	
	/* Share this - social links */
	/*jshint -W030 */
	/*jshint -W107 */
	if ($("#shareThis")[0]) {
		"use strict";
		(function ($, window, document, undefined) {
			/**** our plugin constructor ****/
			var socialShare = function (elem, options) {
				this.elem = elem;
				this.$elem = $(elem);
				this.options = options;
				/**** our plugin share options ****/
				this.replace = function(input){
					var replaceEn = {
						   "'s":" "
						};
					input = input.replace(/'s/gi, function(matched){ return replaceEn[matched]; });
					return input;
				};
				this.share = {
					location: encodeURIComponent(window.location.href),
					title: encodeURIComponent(document.title+" "),
					image: encodeURIComponent(socialShare.getShareImage()),
					source: encodeURIComponent("John Deere"),
					description: encodeURIComponent($("meta[name='description']").attr("content"))
				};
				/**** our plugin shareable services configuration ****/
				this.socialServices = {
					"aolMail": "http://mail.aol.com/mail/ComposeMessage.aspx?subject=" + this.replace(this.share.title) + "&body=" + this.replace(this.share.description) + "  " + this.share.location,
						"facebookShare": "https://www.facebook.com/sharer.php?sdk=joey&u=" + window.location.href + "&display=popup",
						"googleplusShare": "https://plus.google.com/share?url=" + this.share.location,
						"gmailMail": "https://mail.google.com/mail/?ui=2&view=cm&fs=1&tf=1&su=" + this.share.title + "&body=" + this.share.description + "  " + this.share.location,
						"hotmailMail": "http://mail.live.com/default.aspx?rru=compose?subject=" + this.share.title + "&body=" + this.share.description + "  " + this.share.location,
						"linkedinShare": "http://www.linkedin.com/shareArticle?mini=true&url=" + this.share.location + "&title=" + this.share.title + "&summary=" + this.share.description + "&source=" + this.share.source,
						"mailShare": "mailto:?subject=" + this.share.title + "&body=" + this.share.description + "  " + this.share.location,
						"pinterestShare": "http://pinterest.com/pin/create/button/?url=" + this.share.location + "&media=" + this.share.image + "&description=" + this.share.title,
						"tumblrShare": "http://www.tumblr.com/share/link?url=" + this.share.location + "&source=" + this.share.image + "&name=" + this.share.title + "&description=" + this.share.description,
						"twitterShare": "https://twitter.com/share?original_referer=" + this.share.location + "&url=" + this.share.location + "&text=" + this.share.title,
						"yahooMail": "http://compose.mail.yahoo.com/?To=&subject=" + this.replace(this.share.title) + "&body=" + this.replace(this.share.description) + "  " + this.share.location,
						"weiboShare": "http://service.weibo.com/share/share.php?url=" + this.share.location + "&appkey=&title=" + this.share.title + "&pic=" + this.share.image + "&ralateUid=&language=zh_cn"
				};
				this.metadata = this.$elem.data();
			};
			/**** the plugin prototype ****/
			socialShare.prototype = {
				/**** function to getshare image ****/
				getShareImage: function () {
					var _fallBacklogoUrl = 'Unknown_83_filename'/*tpa=http://www.deere.com/common/deere-resources/img/deere_agriculture.png*/;
					if ($('link[rel*=style][href="../css/navcom/navcom.css"/*tpa=http://www.deere.com/common/deere-resources/css/navcom/navcom.css*/]').length > 0) {
						_fallBacklogoUrl = '../img/navcom/deere_navcom.png'/*tpa=http://www.deere.com/common/deere-resources/img/navcom/deere_navcom.png*/;
					}
					else if ($('link[rel*=style][href="../css/constructionModules.css"/*tpa=http://www.deere.com/common/deere-resources/css/constructionModules.css*/]').length > 0) {
						_fallBacklogoUrl = '../img/construction/deere_construction.png'/*tpa=http://www.deere.com/common/deere-resources/img/construction/deere_construction.png*/;
					}
					else if ($('link[rel*=style][href="../css/sabo/sabo.css"/*tpa=http://www.deere.com/common/deere-resources/css/sabo/sabo.css*/]').length > 0) {
						_fallBacklogoUrl = '../img/sabo/deere_sabo.png'/*tpa=http://www.deere.com/common/deere-resources/img/sabo/deere_sabo.png*/;
					}
					else if ($('link[rel*=style][href="../css/hitachi/hitachi.css"/*tpa=http://www.deere.com/common/deere-resources/css/hitachi/hitachi.css*/]').length > 0) {
						_fallBacklogoUrl = '../img/hitachi/deere_hitachi.png'/*tpa=http://www.deere.com/common/deere-resources/img/hitachi/deere_hitachi.png*/;
					}
					var _shareImage = $.trim($("#grey_bg_layout .defaultView:first img").attr("src"));
					_shareImage = (_shareImage !== "undefined" && _shareImage !== "") ? _shareImage : _fallBacklogoUrl;
					var pattern = new RegExp('^(?:[a-z]+:)?//', 'i');
					return (pattern.test(_shareImage)) ? _shareImage : window.location.protocol + "//" + window.location.host + _shareImage;
				},
				/**** widget initiator/creator ****/
				init: function () {
					this.config = $.extend({}, this.defaults, this.options,
					this.metadata);
					this.addData();
					return this;
				},
				/**** function to add data attributes ****/
				addData: function () {
					var _this = this,
						_socialServices = _this.socialServices,
						_shareThis = _this.$elem;
					_shareThis.find(".mail-share a.default-email").attr({
						"href": _socialServices.mailShare,
						"title": emailShare
					}).end().find(".mail-share a.mail-share-link img").attr({
						"title": emailShare,
						"alt": emailShare
					});
					$.each(_shareThis.find("li"), function () {
						var $this = $(this),
							_class = $.trim($this.attr("class")),
							_camelCaseClass = _this.camelCase(_class);
						if (_class !== "" && _class !== "mail-share") {
							$this.children("a").attr({
								"href": "javascript:void(0);",
									"data-href": _socialServices[_camelCaseClass],
									"data-name": _camelCaseClass,
									"data-service": $this.attr("class").split("-")[0],
									"rel": "nofollow"
							});
						}
					});
				},
				/**** function to open popup window ****/
				popup: function (_this) {
					var scrolling = 'yes',
						resizable = 'no',
						offsetY = 150,
						$this = $(_this),
						service = $this.data("service"),
						url = $this.data("href"),
						name = $this.data("name"),
						width = (service == "pinterest") ? 1000 : 600,
						height = 600;
					if (typeof window.screenLeft != "undefined") {
						x = document.documentElement.clientLeft + (document.documentElement.clientWidth - width) / 2;
						y = document.documentElement.clientTop + offsetY - document.documentElement.scrollTop;
						halfY = document.documentElement.clientTop + (document.documentElement.clientHeight - height) / 2;
						if (y < halfY) y = halfY;
					} else if (typeof window.screenX != "undefined") {
						x = window.screenX + (window.innerWidth - width) / 2;
						y = window.screenY + offsetY - window.pageYOffset;
						halfY = window.screenY + (window.innerHeight - height) / 2;
						if (y < halfY) y = halfY;
					} else {
						x = (screen.width - width) / 2;
						y = offsetY;
					}
					var options = 'height=600,width=600' + ",screenX=" + x + ",left=" + x + ",screenY=" + y + ",top=" + y + ',location=no,scrollbars=yes,menubar=no,resizable=' + resizable + ',status=no,toolbar=no';
					if (typeof omniObj != "undefined" && typeof omniObj.trackSocialShare != "undefined") {
						omniObj.trackSocialShare($this, service);
					}

					newWindow = window.open(url, name, options);
					if (window.focus) {
						newWindow.focus();
					}
					return false;
				},
				/**** function to convert case into camel csae ****/
				camelCase: function (input) {
					return input.toLowerCase().replace(/-(.)/g, function (match, group1) {
						return group1.toUpperCase();
					});
				}
			};

			/**** widget public part ****/
			socialShare.defaults = socialShare.prototype.defaults;
			socialShare.getShareImage = socialShare.prototype.getShareImage;
			socialShare.popup = socialShare.prototype.popup;
			/**** code to create jQuery socialShare plugin/widget ****/
			$.fn.socialShare = function (options) {
				return this.each(function () {
					var shareThis = $(this);
					shareThis.find("a.default-email").click(function (e) {
						if (typeof omniObj != "undefined" && typeof omniObj.trackSocialShare != "undefined") {
							omniObj.trackSocialShare($(this), "default");
						}
					});
					shareThis.find('.mail-share a.mail-share-link').click(function (e) {
						e.preventDefault();
						e.stopPropagation();
						shareThis.find('.mail-share-content').removeClass("hidden");
					});
					shareThis.find('.mail-share-content a#closeBut').click(function (e) {
						e.preventDefault();
						e.stopPropagation();
						shareThis.find('.mail-share-content').addClass("hidden");
					});
					shareThis.find('.mail-share-content').click(function (e) {
						e.stopPropagation();
					});
					$(document).click(function (e) {
						shareThis.find('.mail-share-content').addClass("hidden");
					});
					new socialShare(this, options).init();
				});
			};

			/**** pushing current scope to window scope ****/
			window.socialShare = socialShare;

		})(jQuery, window, document);
		$('#shareThis').socialShare();
		/* on window scroll float social icons */
		floatingPanel('#shareThis');
	}
	
	if($('.withViewMoreOption')[0]){
		deereGlobal.advanceFlyout.viewAllToggle();
		$(".viewAllLink .viewAllText").text(viewMore);
	}

	if($('ul.MOD_NO_5 li').length > 3) {
		$('ul.MOD_NO_5:not(.MOD_N0_5_Nospace) li:nth-child(3n)').after('<li class="clear"></li>');
	}
	if($('#MOD_GC_12_1_nav')[0]) {
		$('#MOD_GC_12_1_nav li.gallery a.g').html(modelLinks.imageGallery);
		$('#MOD_GC_12_1_nav li.degrees a#degreelink').html(modelLinks.degreeView);
		$('#MOD_GC_12_1_nav li.videos a.v').html(modelLinks.videos);
		$('#MOD_GC_12_1_nav').show();
	}

	if ($("#leftColumnSidebar")[0]) {

		$("ul.MOD_NO_10a li.leftnav_active_li,ul.MOD_NO_10b li.leftnav_active_li").next('li').addClass("next_to_active");
		
		if( !$(".leftColumn ul.MOD_NO_10a li.nvl_2")[0] && !$(".leftColumn ul.MOD_NO_10a li.nvl_3")[0] && !$(".leftColumn ul.MOD_NO_10a li.nvl_4")[0]){
			var $menu = $(".leftColumn ul.MOD_NO_10a");
		}else {
			var $menu = $(".leftColumn ul.MOD_NO_10a li > ul");
		}
		$menu.menuAim({
            activate: deereGlobal.advanceFlyout.showMenu,
            deactivate: deereGlobal.advanceFlyout.hideMenu
        });		
		$('.leftColumn ul.MOD_NO_10a li:not(.flyout_dropdown ul li)').hover(function (e) {
			currentNode = $(e.target);
			if(currentNode.is('li') || currentNode.is('a')) {
				$('.leftColumn ul.MOD_NO_10a li').removeClass('flyout_hover');
			}
			$(this).addClass('flyout_hover');
		}, function () {
			if($(this).find('.flyout_dropdown').hasClass('flyout-fixed') == false) {
				$(this).removeClass('flyout_hover');
			}
		});
		$('.leftColumn ul.MOD_NO_10a > li:not(.flyout_dropdown ul li)').hover(function () {
			if($(this).find('.flyout_dropdown').length === 0) {
				$('.flyout_dropdown').removeClass('flyout-fixed');
				$('.flyout_arrow, .flyout_dropdown').removeClass('enable_hover');
				$('.flyout_arrow').css('top', '0px');
				$('.flyout_dropdown').css('top', '-9999em');
			}
		});
		$('.leftColumn ul.MOD_NO_10a li a').focus(function (e) {
			var _this = $(this).parents('li');
			deereGlobal.advanceFlyout.showMenu(_this);
		});
		$('.leftColumn ul.MOD_NO_10a li > a').blur(function (e) {
			var _this = $(this).parents('li');
			deereGlobal.advanceFlyout.hideMenu(_this);
		});
		$('.leftColumn ul.MOD_NO_10a li .flyout_dropdown').delegate('a.close-flyout','click', function (e) {
			e.stopPropagation();
			e.preventDefault();
			$('.leftColumn ul.MOD_NO_10a li .flyout_dropdown').removeClass('flyout-fixed');
			deereGlobal.advanceFlyout.hideAll();
		});		
		$(document).click(function () {
			$('.leftColumn ul.MOD_NO_10a li .flyout_dropdown').removeClass('flyout-fixed');
			$('.leftColumn ul.MOD_NO_10a li').removeClass('flyout_hover');
			deereGlobal.advanceFlyout.hideAll();
		});
		$('.leftColumn ul.MOD_NO_10a li .flyout_dropdown').click(function (e) {
			 e.stopPropagation();
		});
	}
	/*
	* QC 591-Performance Curve
	*/
	$('.iframeNyro').nyroModal({
		width:550,
		height:300
	});
	
}); // end ready


var deereGlobal = {
	advanceFlyout : {
		viewAllToggle : function(){
			$(".viewAllLink").bind('click', function() {
				var $this = $(this),
					_thisParent = $this.parent("li.withViewMoreOption"),
					_thisParentDropDown = _thisParent.parents(".flyout_dropdown"),
					_thisTopParentLI = _thisParentDropDown.parent('li'),
					_thisSplitList = _thisParent.find("ul.subListSplitColumn2-1"),
					_currentColumnCount = _thisParent.hasClass("col1"),
					_colSelector = "",
					_viewAllLink = "",
					_invisibleDivHeight = "",
					_isSublistHidden = _thisSplitList.css('display') == 'none',
					_invisibleDivCalHeight = null;
					

				_colSelector = (_currentColumnCount) ?  ".col1" : ".col2";
				_currentColumnViewAllLinks = _thisParentDropDown.find(_colSelector+" div.viewAllLink"),
				_currentColumnCount = _thisParentDropDown.find(_colSelector+" ul.subListSplitColumn2-1");
				
				var _toggleText = function(){
					_currentColumnViewAllLinks.not($this).find(".viewAllText").text(viewMore);
					if($this.hasClass("open")){
						$this.find(".viewAllText").text(viewLess);
						_thisParentDropDown.addClass('flyout-fixed');
						if(_thisParentDropDown.find('a.close-flyout').length == 0 || _thisParentDropDown.find('a.close-flyout') == undefined ) {
							_thisParentDropDown.append('<a title="Close" class="close-flyout" href="#">Close</a>');
						}
					}else{
						$this.find(".viewAllText").text(viewMore);
						if(_thisParentDropDown.find('.viewAllLink').hasClass('open')==false) {
						_thisParentDropDown.removeClass('flyout-fixed');
						_thisParentDropDown.find('a.close-flyout').remove();
						}
					}
				}

				_viewAllLink = _thisParentDropDown.find(_colSelector+" .viewAllLink");
				_currentColumnCount.not(_thisSplitList).slideUp(100);
				_viewAllLink.not($this).removeClass('open');
				
				$(_thisSplitList).slideToggle(100, _toggleText);
				$this.toggleClass('open');			


				/*DD Code added to apply height only to the hidden div*/
				if(_isSublistHidden){ /*Condition to run code only if the menu has Sublist's */

					/*Condition to avoid multiple addition of invisible div*/
					if(_thisTopParentLI.attr('data-invisible') == undefined){ 
						_thisTopParentLI.attr('data-invisible','applied');
						_thisParentDropDown.append('<div class="invisibleDiv col1">&nbsp;</div><div class="invisibleDiv col2">&nbsp;</div>');
					}

					/*Applied SetTimeOut to apply height to invisible div after the slide toggle is finished*/
					setTimeout(function(){
						_thisSplitListHeight = _thisSplitList.outerHeight() + 100;
						_invisibleDivCalHeight = _thisParentDropDown.find(_colSelector+".invisibleDiv").outerHeight();

						_thisParentDropDown.find(".invisibleDiv" + _colSelector).css('opacity',0).show();

						if(_thisSplitListHeight > _invisibleDivCalHeight){
							_thisParentDropDown.find(".invisibleDiv" + _colSelector).css({ height: _thisSplitListHeight, bottom: -_thisSplitListHeight });
						}
					}, 100);
				}
				
				
			})
		},
		hideAll : function(){
			if($('.flyout_dropdown').hasClass('flyout-fixed')===false) {
				$('.flyout_arrow, .flyout_dropdown').removeClass('enable_hover');
				$('.flyout_arrow').css('top', '0px');
				$('.flyout_dropdown').css('top', '-9999em');
			}
		},
		hideMenu: function(elem){
			var elem = $(elem);

			if (elem.children('.flyout_dropdown')[0]) {
				if($('.flyout_dropdown').hasClass('flyout-fixed')===false) {
					elem.children('.flyout_arrow, .flyout_dropdown').removeClass('enable_hover');
					elem.children('.flyout_arrow').css('top', '0px');
					elem.children('.flyout_dropdown').css('top', '-9999em');
				}
			}
		},
        showMenu: function(elem){
			var elem = $(elem);
            if (elem.children('.flyout_dropdown')[0] && !elem.children('.flyout_dropdown').hasClass('enable_hover')) {
				
				$('.flyout_dropdown').removeClass('flyout-fixed');
				$(".flyout_arrow, .flyout_dropdown").removeClass("enable_hover");
				if(elem.children('.flyout_dropdown').find('span.flyout_title a').text().length == 0) {
					elem.children('.flyout_dropdown').find('span.flyout_title').css('background','none');
					elem.children('.flyout_dropdown').find('span.flyout_title a').html('&nbsp;');
				}
				var hasSubListColumns = elem.find('ul.listSplitColumn2')[0],
					hasSubList = elem.find('.withViewMoreOption')[0],
					viewLinkOpenOrClose = elem.find('.viewAllLink').hasClass("open");
				
				           
				/*Code to make the height consistent for the columns*/
				if(elem.attr('data-height') == undefined && hasSubListColumns != undefined){
					$(elem).find('.flyout_dropdown ul.listSplitColumn2').each(function () {
					    $(this).children('li').equalHeight();
					});

					elem.attr('data-height','applied');
				}

				elem.children('.flyout_arrow, .flyout_dropdown').addClass('enable_hover');

				if(!viewLinkOpenOrClose){ /*Condition to check if any sublist is open or not.*/
					elem.children('.flyout_dropdown').find(".invisibleDiv").hide();
				}

				if (elem.height() > 30) {
					elem.closest('.flyout_arrow').css('top', '8px');
				} else {
					elem.closest('.flyout_arrow').css('top', '0px');
				}
				
				deereGlobal.advanceFlyout.adjustWidth(elem);
				deereGlobal.advanceFlyout.adjustTop(elem);
				
            }
        }, 
		adjustWidth: function(elem){

			/*Code to create columns using CSS class*/
			var _columnDivided = elem.find("ul.listSplitColumn2 li.flyout_desc_box").hasClass("col1");

			if(!_columnDivided){
				elem.find("li.flyout_desc_box:even").addClass("col1");
			    elem.find("li.flyout_desc_box:odd").addClass("col2");
			}

			/* Code to apply custom width on Model/Series Listings Flyout as per the area required.*/
			if($('.enable_hover .flyout-model-listing')[0] && !$('.enable_hover .listSplitColumn2')[0] ){
				
				var modelListingCount = $(".enable_hover .flyout-model-listing:first a").length,
				flyoutWidthModels = ((($(".enable_hover .flyout-model-listing a").outerWidth() + 20) * modelListingCount) - 20),
				flyoutWidthSeries = $(".enable_hover .flyout_desc_box").outerWidth();
				$(".enable_hover .flyout-model-listing a:last-child").addClass("last");

				(flyoutWidthModels > flyoutWidthSeries) ? elem.children(".flyout_dropdown").css('width',flyoutWidthModels) : elem.children(".flyout_dropdown").css('width',flyoutWidthSeries);
			}
		}, 
		adjustTop: function(elem){
			var scrolltop = $(window).scrollTop();
			var windowHeight = $(window).height();
			var position = elem.children('.flyout_dropdown').offset().top;
			var titleBottomPosition = $('.MOD_GC_1a').offset().top + $('.MOD_GC_1a').outerHeight();
			var contentHeight = elem.children('.flyout_dropdown').outerHeight();
			var contenttop = position - scrolltop;
			var contentbottom = position + contentHeight - scrolltop;
			var clip = 0;
			if (contentHeight && contentHeight > 0) {
				if (contentbottom > windowHeight) {
					clip = contentbottom - windowHeight;
					clip = '-' + clip;
				}
			}

			elem.children('.flyout_dropdown').css('top', clip + 'px');
			if(clip <= "0" && contentHeight > "214"){
				elem.children('.flyout_dropdown').css('top', '-'+contentHeight/2+'px');
			}

			var flyoutTopPosition = elem.children('.flyout_dropdown').offset().top;
			var currentElementTopPos = elem.offset().top + 1;
			if(flyoutTopPosition < titleBottomPosition  && currentElementTopPos > titleBottomPosition){
				elem.children('.flyout_dropdown').css('top', '-'+(currentElementTopPos - titleBottomPosition)+'px');
			}
			if(currentElementTopPos < titleBottomPosition){
				elem.children('.flyout_dropdown').css('top', '0px');
			}

			if(scrolltop >= titleBottomPosition  && contentHeight > "214"){
				elem.children('.flyout_dropdown').css('top', '-'+(currentElementTopPos - scrolltop)/2+'px');
			}
		}
	}
};


/*DD Generic Function for Show/Hide Panel.*/
function showHidePanel(element){
	if ($(element).next().is(':hidden')) {
		$(element).next().show();
		$(element).addClass('open');
	} else {
		$(element).next().hide();
		$(element).removeClass('open');
	}
}

// Read a page's GET URL variables and return them as an associative array.
function getLinkVars(location)
{
    var vars = [], hash;
    var hashes = location.slice(location.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}


/*DD This is for IE Quirk mode, as CSS3 property of holding 2classes doesn't work (.page_link.first()) */
function createJointClassPagination(){
	var pageLinkFirst = $(".page_link").hasClass("first");
	var pageLinkLast = $(".page_link").hasClass("last");
	
	if(pageLinkFirst == true){
		$(".page_link.first").addClass("page_link_first");
	}
	
	if(pageLinkLast == true){
		$(".page_link.last").addClass("page_link_last");
	}
}


/*DD artf1272511 - Tabbing Components*/
function showTab(){
	var hashURL = window.location.hash;
	var hashURLDecode = decodeURI(hashURL);

	$('.MOD_NO_2_S li, .tab_module li, .WCAG_MOD_NO_2_S .tab_heading, .wcag_tab_module .tab_heading').each(function(){
		var elementID = "#"+$(this).attr('id');
		if(elementID == hashURLDecode){
			$(this).trigger('click');
		}
	});
}
$(window).scroll(function () {
    var box = $('.MOD_FO_9').position();
    var scrollbar = $(window).scrollTop();
    if (scrollbar >= 150) {
        $('.MOD_FO_9').css('top', (scrollbar + 2));
    } else {
        $('.MOD_FO_9').css('top', 'auto');
    }
});

// this method shows the tab module (Features/Specifications/compare/CompatibleEquipment/Overview)
// @ param  tabValue :  This contains the tab to be highlighted

function onProductTab(tabValue) {
    $('.MOD_NO_2_S li, .tabContent').removeClass('on');

    $('.MOD_NO_2_S .' + tabValue + ', .' + tabValue).addClass('on');
}

function viewOtherOption_Click($this) {
    $($this).next('.buying_options').show();
    $($this).next().next('.buying_options').show();
}

function divPopHeaderClose_Click($this) {
    $($this).parent().parent('.buying_options').hide();
}

function buyOptionBtn_Click($this) {
    //Removed code in this method as it was not needed: Pradeep
    return false;
}

function buyOptionBgBtn_Click($this) {
    var nextNode = $($this).next();
    var url = $(nextNode).text();
    window.open(url);
    return false;
}

function expandAll_Click($this) {
    $($this).parent().parent().find('h3').next().show();
    $($this).parent().parent().find('h3').addClass('open');
}

function collapseAll_Click($this) {
    $($this).parent().parent().find('h3').next().hide();
    $($this).parent().parent().find('h3').removeClass('open');
}

function expandAllSpecSeries_Click($this) {
    $($this).parent().parent().parent().find('h3').next().show();
    $($this).parent().parent().parent().find('h3').addClass('open');
}

function collapseAllSpecSeries_Click($this) {
    $($this).parent().parent().parent().find('h3').next().hide();
    $($this).parent().parent().parent().find('h3').removeClass('open');
}

function tabHeaders_Click($this) {
    if ($($this).hasClass('open')) {
        $($this).next().hide();
        $($this).removeClass('open');
    } else {
        $($this).next().show();
        $($this).addClass('open');
    }
}

function openCompEquipmentAccordion_Click($this) {
    if ($('.MOD_FO_6d h3:not(.print_preview .MOD_FO_6d h3)').hasClass('open')) {
        $('.MOD_FO_6d h3:not(.print_preview .MOD_FO_6d h3)').removeClass('open').next().hide();
    }
    if (!$('.print_preview .MOD_FO_6d h3').hasClass('open')) {
	    if ($($this).hasClass('open')) {
	        $($this).next().hide();
	        $($this).removeClass('open');
	    } else {
	        $($this).next().show();
	        $($this).addClass('open');
	    }
	}
}

function tabFeatureSpec_Click($this) {
    if ($($this).hasClass('open')) {
        $($this).next().hide();
        $($this).removeClass('open');
    } else {
        $($this).next().show();
        $($this).addClass('open');
    }
}

// old lightbox (deprecated)

function showHidePop(obj) {
    var e = document.getElementById(obj);
    var valScroll = (document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop;	// MG | artf1295824 
    if (document.all) {
        var displayReal = e.currentStyle.display;
    } else {
        var cs = document.defaultView.getComputedStyle(e, null);
        var displayReal = cs.getPropertyValue('display');
    }
    if (displayReal == 'block') {
        document.getElementById('bkgPop').style.display = 'none';
        document.getElementById('frm').style.display = 'none';
        document.getElementById(obj).style.display = 'none';
    } else {
        resizePop();
        document.getElementById('bkgPop').style.display = 'block';
        document.getElementById('frm').style.display = 'block';
        document.getElementById(obj).style.display = 'block';
        document.getElementById(obj).style.top = (valScroll + 50) + 'px';
    }
}

function resizePop() {
    if (!document.all) {
        document.getElementById('bkgPop').style.height = (window.innerHeight + 10) + 'px';
        document.getElementById('frm').style.height = (window.innerHeight + 10) + 'px';
        if (window.innerHeight < document.body.clientHeight) {
            document.getElementById('bkgPop').style.height = (document.body.clientHeight + 10) + 'px';
            document.getElementById('frm').style.height = (document.body.clientHeight + 10) + 'px';
        }
    } else {
        if (document.documentElement.clientHeight < document.body.clientHeight) {
            document.getElementById('bkgPop').style.height = (document.body.clientHeight + 10) + 'px';
            document.getElementById('frm').style.height = (document.body.clientHeight + 10) + 'px';
        } else {
            document.getElementById('bkgPop').style.height = (document.documentElement.clientHeight + 10) + 'px';
            document.getElementById('frm').style.height = (document.documentElement.clientHeight + 10) + 'px';
        }
    }
}

function changeLanguage(locale) {
    var url = window.location.href;
    var appName = url.indexOf('/wps/PA_jd_crp/');
    if (appName != -1) {
        optionParam = fetchParam('option');
        target = "/wps/PA_jd_crp/" + locale + "/Home.page?option=" + optionParam + "&locale=" + locale;
        window.location.href = target;
    }
    appName = url.indexOf('/wps/PA_jd_crp_preview/');
    if (appName != -1) {
        optionParam = fetchParam('option');
        target = "/wps/PA_jd_crp_preview/" + locale + "/Home.page?option=" + optionParam + "&locale=" + locale;
        window.location.href = target;
    }
}

function fetchParam(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);
    if (results == null) return "";
    else {
        return results[1];
    }
}

function persistlang() {
    var locale = fetchParam('locale');
    var selectObj = document.getElementById("langSelect");
    if (selectObj != null) {
        for (i = 0; i < selectObj.length; i++) {
            if (locale == selectObj.options[i].value) {
                selectObj.selectedIndex = i;
            }
        }
    }
}

function filterProducts(type) {
    if (type) {
        $('.MOD_FO_4:not(".off")').addClass('off');
        $('.' + type).removeClass('off');
    } else {
        $('.MOD_FO_4.off').removeClass('off');
    }
}
// SiteMap even li margin


//equalheight for MOD_GC_14_2
jQuery.fn.equalHeight = function (columnNum) {
    var height = 0;
    var maxHeight = 0;

    if (columnNum) {
        totalRowsCount = Math.ceil(this.length / columnNum);

        for (var i = 1; i <= totalRowsCount; i++) {
            var startNum = (i * columnNum) - columnNum;
            // Store the tallest element's height
            for (var j = startNum; j < startNum + columnNum; j++) {
                if (jQuery(this.eq(j))[0]) {
                    height = jQuery(this.eq(j)).outerHeight();
                    maxHeight = (height > maxHeight) ? height : maxHeight;
                } else {
                    break;
                }
            }
            //set t.css(property, minHeight + 'px');
            for (var k = startNum; k < startNum + columnNum; k++) {
                if (jQuery(this.eq(k))[0]) {
                    var t = jQuery(this.eq(k));
                    var minHeight = maxHeight - (t.outerHeight() - t.height());
                    var property = 'min-height';

                    t.css(property, minHeight + 'px');
                } else {
                    break;
                }
            }
            maxHeight = 0;
        }
    } else {
        // Store the tallest element's height
        this.each(function () {
            height = jQuery(this).outerHeight();
            maxHeight = (height > maxHeight) ? height : maxHeight;
        });

        // Set element's min-height to tallest element's height
        return this.each(function () {
            var t = jQuery(this);
            var minHeight = maxHeight - (t.outerHeight() - t.height());
            var property = 'min-height';

            t.css(property, minHeight + 'px');
        });
    }
};
//jQuery.each(jQuery.browser, function(i, val) {
//	if(i=="mozilla" && jQuery.browser.version.substr(0,3)=="1.9")
//	alert("Do stuff for firefox 3")
window.onload = function () {
    if ($('.MOD_GC_14_2')[0]) $('.MOD_GC_14_2').equalHeight(2);
}

/*Flash Detection*/
var MM_contentVersion = 6;
var plugin = (navigator.mimeTypes && navigator.mimeTypes["application/x-shockwave-flash"]) ? navigator.mimeTypes["application/x-shockwave-flash"].enabledPlugin : 0;
if (plugin) {
    var words = navigator.plugins["Shockwave Flash"].description.split(" ");
    for (var i = 0; i < words.length; ++i) {
        if (isNaN(parseInt(words[i]))) continue;
        var MM_PluginVersion = words[i];
    }
    var MM_FlashCanPlay = MM_PluginVersion >= MM_contentVersion;
} else if (navigator.userAgent && navigator.userAgent.indexOf("MSIE") >= 0 && (navigator.appVersion.indexOf("Win") != -1)) {
    document.write('<SCR' + 'IPT LANGUAGE=VBScript\> \n'); //FS hide this from IE4.5 Mac by splitting the tag
    document.write('on error resume next \n');
    document.write('MM_FlashCanPlay = ( IsObject(CreateObject("ShockwaveFlash.ShockwaveFlash." & MM_contentVersion)))\n');
    document.write('</SCR' + 'IPT\> \n');
}
//alert(MM_FlashCanPlay);

function flashDetect() {
    if (MM_FlashCanPlay == true) {
        //document.getElementById(manual).write('Flash is installed');
        //document.getElementById('manual1').innerHTML="<a href=\"http://www.deere.com/common/deere-resources/js/test.html\" title=\"Flash is installed\"> Flash is installed </a>";
        //alert("Flash Installed");
    } else {
        document.getElementById('degreelink').href = "http://www.deere.com/common/deere-resources/html/flashNotInstalled.html";
        //alert("Flash Installed");
    }
}

/*DD Script to check if the flash is enabled or not.*/
if (MM_FlashCanPlay == undefined) {
	$(document.documentElement).addClass("flash-disabled");
}


//PNG FIX JS INCLUDE

function loadScript(src, f) {
    var head = document.getElementsByTagName("head")[0];
    var script = document.createElement("script");
    script.src = src;
    var done = false;
    script.onload = script.onreadystatechange = function () {
        // attach to both events for cross browser finish detection:
        if (!done && (!this.readyState || this.readyState == "loaded" || this.readyState == "complete")) {
            done = true;
            if (typeof f == 'function') f();
            // cleans up a little memory:
            script.onload = script.onreadystatechange = null;
            head.removeChild(script);
        }
    };
    head.appendChild(script);
}

if ($.browser.msie && $.browser.version == "6.0")
// example:
loadScript('DD_belatedPNG.js'/*tpa=http://www.deere.com/common/deere-resources/js/DD_belatedPNG.js*/);
//PNG FIX JS INCLUDE
/*DD - To make the tabs text vertically middle*/
$.fn.vAlign = function () {
    return this.each(function (i) {
        var ah = $(this).height();
        var ph = $(this).parent().height();
        var mh = Math.ceil((ph - ah) / 2) - 2;
        var ih = Math.ceil((ph - mh) - 2);
		
		/*DD artf1226921*/
        if (mh>0){
			$(this).css('padding-top', mh);
		}
        $(this).css('height', ih);
    });
};

/*DD Vertical Align for button*/
$.fn.verticalAlign = function () {
    return this.each(function (i) {
			var ah = $(this).height();
			var ph = $(this).parent().height();
			var mh = Math.ceil((ph - ah) / 2) - 5;
			$(this).css('margin-top', mh);
		});
	};


/*Browser Detection Script - DD artf1189723 : Header Nav */

function css_browser_selector(u) {
    var ua = u.toLowerCase(),
        is = function (t) {
            return ua.indexOf(t) > -1
        },
        g = 'gecko',
        w = 'webkit',
        s = 'safari',
        o = 'opera',
        m = 'mobile',
        h = document.documentElement,
        b = [(!(/opera|webtv/i.test(ua)) && /msie\s(\d)/.test(ua)) ? ('ie ie' + RegExp.$1) : is('firefox/2') ? g + ' ff2' : is('firefox/3.5') ? g + ' ff3 ff3_5' : is('firefox/3.6') ? g + ' ff3 ff3_6' : is('firefox/3') ? g + ' ff3' : is('gecko/') ? g : is('opera') ? o + (/version\/(\d+)/.test(ua) ? ' ' + o + RegExp.$1 : (/opera(\s|\/)(\d+)/.test(ua) ? ' ' + o + RegExp.$2 : '')) : is('konqueror') ? 'konqueror' : is('blackberry') ? m + ' blackberry' : is('android') ? m + ' android' : is('chrome') ? w + ' chrome' : is('iron') ? w + ' iron' : is('applewebkit/') ? w + ' ' + s + (/version\/(\d+)/.test(ua) ? ' ' + s + RegExp.$1 : '') : is('mozilla/') ? g : '', is('j2me') ? m + ' j2me' : is('iphone') ? m + ' iphone' : is('ipod') ? m + ' ipod' : is('ipad') ? m + ' ipad' : is('mac') ? 'mac' : is('darwin') ? 'mac' : is('webtv') ? 'webtv' : is('win') ? 'win' + (is('windows nt 6.0') ? ' vista' : '') : is('freebsd') ? 'freebsd' : (is('x11') || is('linux')) ? 'linux' : '', 'js'];
    c = b.join(' ');
    h.className += ' ' + c;
    return c;
};
css_browser_selector(navigator.userAgent);


/*Added Function for Popup Lavina/Vaibhav*/

function openDeerecomPopup(url) {
    //var winDims = 'width=600,height=500';
    var newWindow = window.open(url, "deerecompopupwindow", ',resizable=yes,scrollbars=yes,menubar=yes,toolbar=yes,directories=yes,location=yes,status=yes,copyhistory=yes');
}


/*DD Added the review link.*/
function showtabsreview(){
	//$('.BVRRRatingSummaryLinks a').click(function () {
	$('.MOD_NO_2_S li, .tabContent').removeClass('on');
	$('.MOD_NO_2_S .' + 'reviews' + ', .' + 'reviews').addClass('on');
};

/*****Pagination and last li call for blog component starts here**/
function blogPagination(){
	if($('#blog')[0]) {
		$('#blog').pajinate({
			items_per_page :4,
			nav_label_prev : pagPrev,
			nav_label_next : pagNext
		});
	}
}

function lastLiBlog(){
	$('ul.content li:visible:last').addClass('last_border');
}
/*****Pagination call for blog component ends here**/


/*DD artf1284219*/
checkBrowserMode = document.compatMode != 'CSS1Compat';

/*DD Script to check the Browser Mode(Quirk or Normal)*/
if(checkBrowserMode){
	$(document.documentElement).addClass("quirk-mode");
}
//Navcom variables
navcomError1 = 'Error in fetching content for ';
navcomError2 = '. Please proceed with the default values.';

allCategories = 'All Categories';
allCurrentProducts = 'All Current Products';
allDiscontinuedProducts = 'All Discontinued Products';

/*Tooltip code added by AB*/

$(function() {

 
    $('[rel=tooltip]').mouseover(function(e) {
        var tip = $(this).attr('title');   
        $(this).attr('title','');
         
        //Append the tooltip template and its value
        $(this).append('<div id="tooltip"><div class="tooltip-body">' + tip + '</div></div>');    
            if($(this).hasClass("right-side"))   {

                $('#tooltip').css('top', e.pageY - 100 );
                $('#tooltip').css('left', e.pageX + 20 );

         }else{

            $('#tooltip').css('top', e.pageY + 10 );
            $('#tooltip').css('left', e.pageX - 50 );
            }
            
         
    }).mousemove(function(e) {
     
       
       if($(this).hasClass("right-side")){
       
           $('#tooltip').css('top', e.pageY - 100 );
                $('#tooltip').css('left', e.pageX + 20 );

            }else{

            $('#tooltip').css('top', e.pageY + 10 );
            $('#tooltip').css('left', e.pageX - 50 );
         }
         
    }).mouseout(function() {
     

        $(this).attr('title',$('.tooltip-body').html());
        $(this).children('div#tooltip').remove();
         
    });

});
/*
// Show Loader
function showLoader() {
    $('#frm_overlay').show();
    var docHeight = $('.wrapper').height() || $(document).height();
    $('#frm_overlay').height(docHeight);
};

// Hide Loader
function hideLoader() {
    $('#frm_overlay').hide();
};
*/
function showLoader(){
	var windowHeight = $(document).height();
	var screenFreeze = '<div id="screen-freeze" style="height:'+windowHeight+'px"></div>';
	var loaderImage = '<img id="loader-image" src="../img/screen_freeze_ajax_loader.gif"/*tpa=http://www.deere.com/common/deere-resources/img/screen_freeze_ajax_loader.gif*/ alt="'+langLoading+'" />';

	$('body').append(screenFreeze);
	$('body').append(loaderImage);	
}

function hideLoader(){
	$('#screen-freeze, #loader-image').remove();
}


/********* Global Home Page - Cookie Setup ********/
function getCookie(check_name){

	// first we'll split this cookie up into name/value pairs
	// note: document.cookie only returns name=value, not the other components
	var a_all_cookies = document.cookie.split( ';' );
	var a_temp_cookie = '';
	var cookie_name = '';
	var cookie_value = '';
	var b_cookie_found = false; // set boolean t/f default f


	for ( i = 0; i < a_all_cookies.length; i++ )
	{
		// now we'll split apart each name=value pair
		a_temp_cookie = a_all_cookies[i].split( '=' );


		// and trim left/right whitespace while we're at it
		cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g, '');
		// if the extracted name matches passed check_name
		if ( cookie_name == check_name )
		{

			b_cookie_found = true;
			// we need to handle case where cookie has no value but exists (no = sign, that is):
			if ( a_temp_cookie.length > 1 )
			{
				cookie_value = unescape( a_temp_cookie[1].replace(/^\s+|\s+$/g, '') );
			}
			// note that in cases where cookie is initialized but no value, null is returned

			return cookie_value;
			break;
		}
		a_temp_cookie = null;
		cookie_name = '';
	}
	if ( !b_cookie_found )
	{
		
		return 'notfound';
	}
}

function eraseCookie(name, value, expires, domain, path) {
	newCookie(name, value, expires, domain, path);
}

function newCookie( name, value, expires, domain, path, secure ) {
    var today = new Date();
	today.setTime( today.getTime() );

	var expires_date;
	
	if ( expires ){
		if(expires == '-1'){
			expires_date = 'Thu, 01 Jan 1970 00:00:01 GMT';
		}else{
			expires = expires * 1000 * 60 * 60 * 24;
			expires_date = new Date( today.getTime() + (expires) ).toGMTString();
		}
	}
	
	document.cookie = name + '=' + escape(value) + ';expires='+ expires_date + ';  path='+ path +';' + '; domain='+domain;
}


function readCookie(name) {
	var nameSG = name + "=";
	var nuller = '';
	if (document.cookie.indexOf(nameSG) == -1) return nuller;

	var ca = document.cookie.split(';');

	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') c = c.substring(1, c.length);
		if (c.indexOf(nameSG) == 0) return c.substring(nameSG.length, c.length);
	}
	return null;
}



function putCookie( name, value, expires, path, domain, secure ){
	if (document.myform.rdoRemember.checked==true && readCookie("cc_analytics")!='no'){
		var regioname= name;
		var regionvalue= value;
		var regionexpires= expires;
		var regionpath= path;
		var regiondomain= domain;
		var regionsecure= secure;

		newCookie(regioname,regionvalue,regionexpires,regiondomain,regionpath,regionsecure);
	}
}

/* ====== Plugin for Custom Form Element ======= */
(function(a){a.uniform={options:{selectClass:"selector",radioClass:"radio",checkboxClass:"checker",fileClass:"uploader",filenameClass:"filename",fileBtnClass:"action",fileDefaultText:"No file selected",fileBtnText:"Choose File",checkedClass:"checked",focusClass:"focus",disabledClass:"disabled",buttonClass:"button",activeClass:"active",hoverClass:"hover",useID:true,idPrefix:"uniform",resetSelector:false,autoHide:true},elements:[]};if(a.browser.msie&&a.browser.version<7){a.support.selectOpacity=false}else{a.support.selectOpacity=true}a.fn.uniform=function(k){k=a.extend(a.uniform.options,k);var d=this;if(k.resetSelector!=false){a(k.resetSelector).mouseup(function(){function l(){a.uniform.update(d)}setTimeout(l,10)})}function j(l){$el=a(l);$el.addClass($el.attr("type"));b(l)}function g(l){a(l).addClass("uniform");b(l)}function i(o){var m=a(o);var p=a("<div>"),l=a("<span>");p.addClass(k.buttonClass);if(k.useID&&m.attr("id")!=""){p.attr("id",k.idPrefix+"-"+m.attr("id"))}var n;if(m.is("a")||m.is("button")){n=m.text()}else{if(m.is(":submit")||m.is(":reset")||m.is("input[type=button]")){n=m.attr("value")}}n=n==""?m.is(":reset")?"Reset":"Submit":n;l.html(n);m.css("opacity",0);m.wrap(p);m.wrap(l);p=m.closest("div");l=m.closest("span");if(m.is(":disabled")){p.addClass(k.disabledClass)}p.bind({"mouseenter.uniform":function(){p.addClass(k.hoverClass)},"mouseleave.uniform":function(){p.removeClass(k.hoverClass);p.removeClass(k.activeClass)},"mousedown.uniform touchbegin.uniform":function(){p.addClass(k.activeClass)},"mouseup.uniform touchend.uniform":function(){p.removeClass(k.activeClass)},"click.uniform touchend.uniform":function(r){if(a(r.target).is("span")||a(r.target).is("div")){if(o[0].dispatchEvent){var q=document.createEvent("MouseEvents");q.initEvent("click",true,true);o[0].dispatchEvent(q)}else{o[0].click()}}}});o.bind({"focus.uniform":function(){p.addClass(k.focusClass)},"blur.uniform":function(){p.removeClass(k.focusClass)}});a.uniform.noSelect(p);b(o)}function e(o){var m=a(o);var p=a("<div />"),l=a("<span />");if(!m.css("display")=="none"&&k.autoHide){p.hide()}p.addClass(k.selectClass);if(k.useID&&o.attr("id")!=""){p.attr("id",k.idPrefix+"-"+o.attr("id"))}var n=o.find(":selected:first");if(n.length==0){n=o.find("option:first")}l.html(n.html());o.css("opacity",0);o.wrap(p);o.before(l);p=o.parent("div");l=o.siblings("span");o.bind({"change.uniform":function(){l.text(o.find(":selected").html());p.removeClass(k.activeClass)},"focus.uniform":function(){p.addClass(k.focusClass)},"blur.uniform":function(){p.removeClass(k.focusClass);p.removeClass(k.activeClass)},"mousedown.uniform touchbegin.uniform":function(){p.addClass(k.activeClass)},"mouseup.uniform touchend.uniform":function(){p.removeClass(k.activeClass)},"click.uniform touchend.uniform":function(){p.removeClass(k.activeClass)},"mouseenter.uniform":function(){p.addClass(k.hoverClass)},"mouseleave.uniform":function(){p.removeClass(k.hoverClass);p.removeClass(k.activeClass)},"keyup.uniform":function(){l.text(o.find(":selected").html())}});if(a(o).attr("disabled")){p.addClass(k.disabledClass)}a.uniform.noSelect(l);b(o)}function f(n){var m=a(n);var o=a("<div />"),l=a("<span />");if(!m.css("display")=="none"&&k.autoHide){o.hide()}o.addClass(k.checkboxClass);if(k.useID&&n.attr("id")!=""){o.attr("id",k.idPrefix+"-"+n.attr("id"))}a(n).wrap(o);a(n).wrap(l);l=n.parent();o=l.parent();a(n).css("opacity",0).bind({"focus.uniform":function(){o.addClass(k.focusClass)},"blur.uniform":function(){o.removeClass(k.focusClass)},"click.uniform touchend.uniform":function(){if(!a(n).attr("checked")){l.removeClass(k.checkedClass)}else{l.addClass(k.checkedClass)}},"mousedown.uniform touchbegin.uniform":function(){o.addClass(k.activeClass)},"mouseup.uniform touchend.uniform":function(){o.removeClass(k.activeClass)},"mouseenter.uniform":function(){o.addClass(k.hoverClass)},"mouseleave.uniform":function(){o.removeClass(k.hoverClass);o.removeClass(k.activeClass)}});if(a(n).attr("checked")){l.addClass(k.checkedClass)}if(a(n).attr("disabled")){o.addClass(k.disabledClass)}b(n)}function c(n){var m=a(n);var o=a("<div />"),l=a("<span />");if(!m.css("display")=="none"&&k.autoHide){o.hide()}o.addClass(k.radioClass);if(k.useID&&n.attr("id")!=""){o.attr("id",k.idPrefix+"-"+n.attr("id"))}a(n).wrap(o);a(n).wrap(l);l=n.parent();o=l.parent();a(n).css("opacity",0).bind({"focus.uniform":function(){o.addClass(k.focusClass)},"blur.uniform":function(){o.removeClass(k.focusClass)},"click.uniform touchend.uniform":function(){if(!a(n).attr("checked")){l.removeClass(k.checkedClass)}else{var p=k.radioClass.split(" ")[0];a("."+p+" span."+k.checkedClass+":has([name='"+a(n).attr("name")+"'])").removeClass(k.checkedClass);l.addClass(k.checkedClass)}},"mousedown.uniform touchend.uniform":function(){if(!a(n).is(":disabled")){o.addClass(k.activeClass)}},"mouseup.uniform touchbegin.uniform":function(){o.removeClass(k.activeClass)},"mouseenter.uniform touchend.uniform":function(){o.addClass(k.hoverClass)},"mouseleave.uniform":function(){o.removeClass(k.hoverClass);o.removeClass(k.activeClass)}});if(a(n).attr("checked")){l.addClass(k.checkedClass)}if(a(n).attr("disabled")){o.addClass(k.disabledClass)}b(n)}function h(q){var o=a(q);var r=a("<div />"),p=a("<span>"+k.fileDefaultText+"</span>"),m=a("<span>"+k.fileBtnText+"</span>");if(!o.css("display")=="none"&&k.autoHide){r.hide()}r.addClass(k.fileClass);p.addClass(k.filenameClass);m.addClass(k.fileBtnClass);if(k.useID&&o.attr("id")!=""){r.attr("id",k.idPrefix+"-"+o.attr("id"))}o.wrap(r);o.after(m);o.after(p);r=o.closest("div");p=o.siblings("."+k.filenameClass);m=o.siblings("."+k.fileBtnClass);if(!o.attr("size")){var l=r.width();o.attr("size",l/10)}var n=function(){var s=o.val();if(s===""){s=k.fileDefaultText}else{s=s.split(/[\/\\]+/);s=s[(s.length-1)]}p.text(s)};n();o.css("opacity",0).bind({"focus.uniform":function(){r.addClass(k.focusClass)},"blur.uniform":function(){r.removeClass(k.focusClass)},"mousedown.uniform":function(){if(!a(q).is(":disabled")){r.addClass(k.activeClass)}},"mouseup.uniform":function(){r.removeClass(k.activeClass)},"mouseenter.uniform":function(){r.addClass(k.hoverClass)},"mouseleave.uniform":function(){r.removeClass(k.hoverClass);r.removeClass(k.activeClass)}});if(a.browser.msie){o.bind("http://www.deere.com/common/deere-resources/js/click.uniform.ie7",function(){setTimeout(n,0)})}else{o.bind("change.uniform",n)}if(o.attr("disabled")){r.addClass(k.disabledClass)}a.uniform.noSelect(p);a.uniform.noSelect(m);b(q)}a.uniform.restore=function(l){if(l==undefined){l=a(a.uniform.elements)}a(l).each(function(){if(a(this).is(":checkbox")){a(this).unwrap().unwrap()}else{if(a(this).is("select")){a(this).siblings("span").remove();a(this).unwrap()}else{if(a(this).is(":radio")){a(this).unwrap().unwrap()}else{if(a(this).is(":file")){a(this).siblings("span").remove();a(this).unwrap()}else{if(a(this).is("button, :submit, :reset, a, input[type='button']")){a(this).unwrap().unwrap()}}}}}a(this).unbind(".uniform");a(this).css("opacity","1");var m=a.inArray(a(l),a.uniform.elements);a.uniform.elements.splice(m,1)})};function b(l){l=a(l).get();if(l.length>1){a.each(l,function(m,n){a.uniform.elements.push(n)})}else{a.uniform.elements.push(l)}}a.uniform.noSelect=function(l){function m(){return false}a(l).each(function(){this.onselectstart=this.ondragstart=m;a(this).mousedown(m).css({MozUserSelect:"none"})})};a.uniform.update=function(l){if(l==undefined){l=a(a.uniform.elements)}l=a(l);l.each(function(){var n=a(this);if(n.is("select")){var m=n.siblings("span");var p=n.parent("div");p.removeClass(k.hoverClass+" "+k.focusClass+" "+k.activeClass);m.html(n.find(":selected").html());if(n.is(":disabled")){p.addClass(k.disabledClass)}else{p.removeClass(k.disabledClass)}}else{if(n.is(":checkbox")){var m=n.closest("span");var p=n.closest("div");p.removeClass(k.hoverClass+" "+k.focusClass+" "+k.activeClass);m.removeClass(k.checkedClass);if(n.is(":checked")){m.addClass(k.checkedClass)}if(n.is(":disabled")){p.addClass(k.disabledClass)}else{p.removeClass(k.disabledClass)}}else{if(n.is(":radio")){var m=n.closest("span");var p=n.closest("div");p.removeClass(k.hoverClass+" "+k.focusClass+" "+k.activeClass);m.removeClass(k.checkedClass);if(n.is(":checked")){m.addClass(k.checkedClass)}if(n.is(":disabled")){p.addClass(k.disabledClass)}else{p.removeClass(k.disabledClass)}}else{if(n.is(":file")){var p=n.parent("div");var o=n.siblings(k.filenameClass);btnTag=n.siblings(k.fileBtnClass);p.removeClass(k.hoverClass+" "+k.focusClass+" "+k.activeClass);o.text(n.val());if(n.is(":disabled")){p.addClass(k.disabledClass)}else{p.removeClass(k.disabledClass)}}else{if(n.is(":submit")||n.is(":reset")||n.is("button")||n.is("a")||l.is("input[type=button]")){var p=n.closest("div");p.removeClass(k.hoverClass+" "+k.focusClass+" "+k.activeClass);if(n.is(":disabled")){p.addClass(k.disabledClass)}else{p.removeClass(k.disabledClass)}}}}}}})};return this.each(function(){if(a.support.selectOpacity){var l=a(this);if(l.is("select")){if(l.attr("multiple")!=true){if(l.attr("size")==undefined||l.attr("size")<=1){e(l)}}}else{if(l.is(":checkbox")){f(l)}else{if(l.is(":radio")){c(l)}else{if(l.is(":file")){h(l)}else{if(l.is(":text, :password, input[type='email']")){j(l)}else{if(l.is("textarea")){g(l)}else{if(l.is("a")||l.is(":submit")||l.is(":reset")||l.is("button")||l.is("input[type=button]")){i(l)}}}}}}}}})}})(jQuery);

/* ====== End Plugin for Custom Form Element ======= */

/*! qtip2 v2.0.0 | http://craigsworks.com/projects/qtip2/ | Licensed MIT, GPL */
(function(e,t,n){(function(e){"use strict";typeof define=="function"&&define.amd?define(["jquery"],e):jQuery&&!jQuery.fn.qtip&&e(jQuery)})(function(r){function _(n){E={pageX:n.pageX,pageY:n.pageY,type:"mousemove",scrollX:e.pageXOffset||t.body.scrollLeft||t.documentElement.scrollLeft,scrollY:e.pageYOffset||t.body.scrollTop||t.documentElement.scrollTop}}function D(e){var t=function(e){return e===o||"object"!=typeof e},n=function(e){return!r.isFunction(e)&&(!e&&!e.attr||e.length<1||"object"==typeof e&&!e.jquery&&!e.then)};if(!e||"object"!=typeof e)return s;t(e.metadata)&&(e.metadata={type:e.metadata});if("content"in e){if(t(e.content)||e.content.jquery)e.content={text:e.content};n(e.content.text||s)&&(e.content.text=s),"title"in e.content&&(t(e.content.title)&&(e.content.title={text:e.content.title}),n(e.content.title.text||s)&&(e.content.title.text=s))}return"position"in e&&t(e.position)&&(e.position={my:e.position,at:e.position}),"show"in e&&t(e.show)&&(e.show=e.show.jquery?{target:e.show}:{event:e.show}),"hide"in e&&t(e.hide)&&(e.hide=e.hide.jquery?{target:e.hide}:{event:e.hide}),"style"in e&&t(e.style)&&(e.style={classes:e.style}),r.each(w,function(){this.sanitize&&this.sanitize(e)}),e}function P(u,a,f,l){function q(e){var t=0,n,r=a,i=e.split(".");while(r=r[i[t++]])t<i.length&&(n=r);return[n||a,i.pop()]}function R(e){return T.concat("").join(e?"-"+e+" ":" ")}function U(){var e=a.style.widget,t=H.hasClass(j);H.removeClass(j),j=e?"ui-state-disabled":"qtip-disabled",H.toggleClass(j,t),H.toggleClass("ui-helper-reset "+R(),e).toggleClass(C,a.style.def&&!e),F.content&&F.content.toggleClass(R("content"),e),F.titlebar&&F.titlebar.toggleClass(R("header"),e),F.button&&F.button.toggleClass(S+"-icon",!e)}function z(e){F.title&&(F.titlebar.remove(),F.titlebar=F.title=F.button=o,e!==s&&m.reposition())}function W(){var e=a.content.title.button,t=typeof e=="string",n=t?e:closeValue;F.button&&F.button.remove(),e.jquery?F.button=e:F.button=r("<a />",{"class":"qtip-close "+(a.style.widget?"":S+"-icon"),title:n,"aria-label":n}).prepend(r("<span />",{"class":"ui-icon ui-icon-close",html:"&times;"})),F.button.appendTo(F.titlebar||H).attr("role","button").click(function(e){return H.hasClass(j)||m.hide(e),s})}function X(){var e=y+"-title";F.titlebar&&z(),F.titlebar=r("<div />",{"class":S+"-titlebar "+(a.style.widget?R("header"):"")}).append(F.title=r("<div />",{id:e,"class":S+"-title","aria-atomic":i})).insertBefore(F.content).delegate(".qtip-close","mousedown keydown mouseup keyup mouseout",function(e){r(this).toggleClass("ui-state-active ui-state-focus",e.type.substr(-4)==="down")}).delegate(".qtip-close","mouseover mouseout",function(e){r(this).toggleClass("ui-state-hover",e.type==="mouseover")}),a.content.title.button&&W()}function V(e){var t=F.button;if(!m.rendered)return s;e?W():t.remove()}function J(e,t){var n=F.title;if(!m.rendered||!e)return s;r.isFunction(e)&&(e=e.call(u,I.event,m));if(e===s||!e&&e!=="")return z(s);e.jquery&&e.length>0?n.empty().append(e.css({display:"block"})):n.html(e),t!==s&&m.rendered&&H[0].offsetWidth>0&&m.reposition(I.event)}function K(e){e&&r.isFunction(e.done)&&e.done(function(e){Q(e,null,s)})}function Q(e,t,i){function f(e){function a(n){n&&(delete u[n.src],clearTimeout(m.timers.img[n.src]),r(n).unbind(B)),r.isEmptyObject(u)&&(t!==s&&m.reposition(I.event),e())}var i,u={};if((i=o.find("img[src]:not([height]):not([width])")).length===0)return a();i.each(function(e,t){if(u[t.src]!==n)return;var i=0,s=3;(function o(){if(t.height||t.width||i>s)return a(t);i+=1,m.timers.img[t.src]=setTimeout(o,700)})(),r(t).bind("error"+B+" load"+B,function(){a(this)}),u[t.src]=t})}var o=F.content;return!m.rendered||!e?s:(r.isFunction(e)&&(e=e.call(u,I.event,m)||""),i!==s&&K(a.content.deferred),e.jquery&&e.length>0?o.empty().append(e.css({display:"block"})):o.html(e),m.rendered<0?H.queue("fx",f):(P=0,f(r.noop)),m)}function G(){function h(e){if(H.hasClass(j))return s;clearTimeout(m.timers.show),clearTimeout(m.timers.hide);var t=function(){m.toggle(i,e)};a.show.delay>0?m.timers.show=setTimeout(t,a.show.delay):t()}function p(e){if(H.hasClass(j)||A||P)return s;var t=r(e.relatedTarget||e.target),i=t.closest(N)[0]===H[0],u=t[0]===o.show[0];clearTimeout(m.timers.show),clearTimeout(m.timers.hide);if(n.target==="mouse"&&i||a.hide.fixed&&/mouse(out|leave|move)/.test(e.type)&&(i||u)){try{e.preventDefault(),e.stopImmediatePropagation()}catch(f){}return}a.hide.delay>0?m.timers.hide=setTimeout(function(){m.hide(e)},a.hide.delay):m.hide(e)}function d(e){if(H.hasClass(j))return s;clearTimeout(m.timers.inactive),m.timers.inactive=setTimeout(function(){m.hide(e)},a.hide.inactive)}function v(e){m.rendered&&H[0].offsetWidth>0&&m.reposition(e)}var n=a.position,o={show:a.show.target,hide:a.hide.target,viewport:r(n.viewport),document:r(t),body:r(t.body),window:r(e)},l={show:r.trim(""+a.show.event).split(" "),hide:r.trim(""+a.hide.event).split(" ")},c=r.browser.msie&&parseInt(r.browser.version,10)===6;H.bind("mouseenter"+B+" mouseleave"+B,function(e){var t=e.type==="mouseenter";t&&m.focus(e),H.toggleClass(L,t)}),/mouse(out|leave)/i.test(a.hide.event)&&a.hide.leave==="window"&&o.window.bind("mouseout"+B+" blur"+B,function(e){!/select|option/.test(e.target.nodeName)&&!e.relatedTarget&&m.hide(e)}),a.hide.fixed?(o.hide=o.hide.add(H),H.bind("mouseover"+B,function(){H.hasClass(j)||clearTimeout(m.timers.hide)})):/mouse(over|enter)/i.test(a.show.event)&&o.hide.bind("mouseleave"+B,function(e){clearTimeout(m.timers.show)}),(""+a.hide.event).indexOf("unfocus")>-1&&n.container.closest("html").bind("mousedown"+B+" touchstart"+B,function(e){var t=r(e.target),n=m.rendered&&!H.hasClass(j)&&H[0].offsetWidth>0,i=t.parents(N).filter(H[0]).length>0;t[0]!==u[0]&&t[0]!==H[0]&&!i&&!u.has(t[0]).length&&!t.attr("disabled")&&m.hide(e)}),"number"==typeof a.hide.inactive&&(o.show.bind("qtip-"+f+"-inactive",d),r.each(b.inactiveEvents,function(e,t){o.hide.add(F.tooltip).bind(t+B+"-inactive",d)})),r.each(l.hide,function(e,t){var n=r.inArray(t,l.show),i=r(o.hide);n>-1&&i.add(o.show).length===i.length||t==="unfocus"?(o.show.bind(t+B,function(e){H[0].offsetWidth>0?p(e):h(e)}),delete l.show[n]):o.hide.bind(t+B,p)}),r.each(l.show,function(e,t){o.show.bind(t+B,h)}),"number"==typeof a.hide.distance&&o.show.add(H).bind("mousemove"+B,function(e){var t=I.origin||{},n=a.hide.distance,r=Math.abs;(r(e.pageX-t.pageX)>=n||r(e.pageY-t.pageY)>=n)&&m.hide(e)}),n.target==="mouse"&&(o.show.bind("mousemove"+B,_),n.adjust.mouse&&(a.hide.event&&(H.bind("mouseleave"+B,function(e){(e.relatedTarget||e.target)!==o.show[0]&&m.hide(e)}),F.target.bind("mouseenter"+B+" mouseleave"+B,function(e){I.onTarget=e.type==="mouseenter"})),o.document.bind("mousemove"+B,function(e){m.rendered&&I.onTarget&&!H.hasClass(j)&&H[0].offsetWidth>0&&m.reposition(e||E)}))),(n.adjust.resize||o.viewport.length)&&(r.event.special.resize?o.viewport:o.window).bind("resize"+B,v),o.window.bind("scroll"+B,v)}function Y(){var n=[a.show.target[0],a.hide.target[0],m.rendered&&F.tooltip[0],a.position.container[0],a.position.viewport[0],a.position.container.closest("html")[0],e,t];m.rendered?r([]).pushStack(r.grep(n,function(e){return typeof e=="object"})).unbind(B):a.show.target.unbind(B+"-create")}var m=this,g=t.body,y=S+"-"+f,A=0,P=0,H=r(),B=".qtip-"+f,j="qtip-disabled",F,I;m.id=f,m.rendered=s,m.destroyed=s,m.elements=F={target:u},m.timers={img:{}},m.options=a,m.checks={},m.plugins={},m.cache=I={event:{},target:r(),disabled:s,attr:l,onTarget:s,lastClass:""},m.checks.builtin={"^id$":function(e,t,n){var o=n===i?b.nextid:n,u=S+"-"+o;o!==s&&o.length>0&&!r("#"+u).length&&(H[0].id=u,F.content[0].id=u+"-content",F.title[0].id=u+"-title")},"^content.text$":function(e,t,n){Q(a.content.text)},"^content.deferred$":function(e,t,n){K(a.content.deferred)},"^content.title.text$":function(e,t,n){if(!n)return z();!F.title&&n&&X(),J(n)},"^content.title.button$":function(e,t,n){V(n)},"^position.(my|at)$":function(e,t,n){"string"==typeof n&&(e[t]=new w.Corner(n))},"^position.container$":function(e,t,n){m.rendered&&H.appendTo(n)},"^show.ready$":function(){m.rendered?m.toggle(i):m.render(1)},"^style.classes$":function(e,t,n){H.attr("class",S+" qtip "+n)},"^style.width|height":function(e,t,n){H.css(t,n)},"^style.widget|content.title":U,"^events.(render|show|move|hide|focus|blur)$":function(e,t,n){H[(r.isFunction(n)?"":"un")+"bind"]("tooltip"+t,n)},"^(show|hide|position).(event|target|fixed|inactive|leave|distance|viewport|adjust)":function(){var e=a.position;H.attr("tracking",e.target==="mouse"&&e.adjust.mouse),Y(),G()}},r.extend(m,{_triggerEvent:function(e,t,n){var i=r.Event("tooltip"+e);return i.originalEvent=(n?r.extend({},n):o)||I.event||o,H.trigger(i,[m].concat(t||[])),!i.isDefaultPrevented()},render:function(e){if(m.rendered)return m;var t=a.content.text,n=a.content.title,o=a.position;return r.attr(u[0],"aria-describedby",y),H=F.tooltip=r("<div/>",{id:y,"class":[S,C,a.style.classes,S+"-pos-"+a.position.my.abbrev()].join(" "),width:a.style.width||"",height:a.style.height||"",tracking:o.target==="mouse"&&o.adjust.mouse,role:"alert","aria-live":"polite","aria-atomic":s,"aria-describedby":y+"-content","aria-hidden":i}).toggleClass(j,I.disabled).data("qtip",m).appendTo(a.position.container).append(F.content=r("<div />",{"class":S+"-content",id:y+"-content","aria-atomic":i})),m.rendered=-1,A=1,n.text?(X(),r.isFunction(n.text)||J(n.text,s)):n.button&&W(),(!r.isFunction(t)||t.then)&&Q(t,s),m.rendered=i,U(),r.each(a.events,function(e,t){r.isFunction(t)&&H.bind(e==="toggle"?"tooltipshow tooltiphide":"tooltip"+e,t)}),r.each(w,function(){this.initialize==="render"&&this(m)}),G(),H.queue("fx",function(t){m._triggerEvent("render"),A=0,(a.show.ready||e)&&m.toggle(i,I.event,s),t()}),m},get:function(e){var t,n;switch(e.toLowerCase()){case"dimensions":t={height:H.outerHeight(s),width:H.outerWidth(s)};break;case"offset":t=w.offset(H,a.position.container);break;default:n=q(e.toLowerCase()),t=n[0][n[1]],t=t.precedance?t.string():t}return t},set:function(e,t){function h(e,t){var n,r,i;for(n in l)for(r in l[n])if(i=(new RegExp(r,"i")).exec(e))t.push(i),l[n][r].apply(m,t)}var n=/^position\.(my|at|adjust|target|container)|style|content|show\.ready/i,u=/^content\.(title|attr)|style/i,f=s,l=m.checks,c;return"string"==typeof e?(c=e,e={},e[c]=t):e=r.extend(i,{},e),r.each(e,function(t,i){var s=q(t.toLowerCase()),o;o=s[0][s[1]],s[0][s[1]]="object"==typeof i&&i.nodeType?r(i):i,e[t]=[s[0],s[1],i,o],f=n.test(t)||f}),D(a),A=1,r.each(e,h),A=0,m.rendered&&H[0].offsetWidth>0&&f&&m.reposition(a.position.target==="mouse"?o:I.event),m},toggle:function(e,n){function b(){e?(r.browser.msie&&H[0].style.removeAttribute("filter"),H.css("overflow",""),"string"==typeof u.autofocus&&r(u.autofocus,H).focus(),u.target.trigger("qtip-"+f+"-inactive")):H.css({display:"",visibility:"",opacity:"",left:"",top:""}),m._triggerEvent(e?"visible":"hidden")}if(n){if(/over|enter/.test(n.type)&&/out|leave/.test(I.event.type)&&a.show.target.add(n.target).length===a.show.target.length&&H.has(n.relatedTarget).length)return m;I.event=r.extend({},n)}if(!m.rendered)return e?m.render(1):m;var o=e?"show":"hide",u=a[o],l=a[e?"hide":"show"],c=a.position,h=a.content,p=H[0].offsetWidth>0,d=e||u.target.length===1,v=!n||u.target.length<2||I.target[0]===n.target,g,y;return(typeof e).search("boolean|number")&&(e=!p),!H.is(":animated")&&p===e&&v?m:m._triggerEvent(o,[90])?(r.attr(H[0],"aria-hidden",!e),e?(I.origin=r.extend({},E),m.focus(n),r.isFunction(h.text)&&Q(h.text,s),r.isFunction(h.title.text)&&J(h.title.text,s),!M&&c.target==="mouse"&&c.adjust.mouse&&(r(t).bind("http://www.deere.com/common/deere-resources/js/mousemove.qtip",_),M=i),m.reposition(n,arguments[2]),!u.solo||r(N,u.solo).not(H).qtip("hide",r.Event("tooltipsolo"))):(clearTimeout(m.timers.show),delete I.origin,M&&!r(N+'[tracking="true"]:visible',u.solo).not(H).length&&(r(t).unbind("http://www.deere.com/common/deere-resources/js/mousemove.qtip"),M=s),m.blur(n)),u.effect===s||d===s?(H[o](),b.call(H)):r.isFunction(u.effect)?(H.stop(1,1),u.effect.call(H,m),H.queue("fx",function(e){b(),e()})):H.fadeTo(90,e?1:0,b),e&&u.target.trigger("qtip-"+f+"-inactive"),m):m},show:function(e){return m.toggle(i,e)},hide:function(e){return m.toggle(s,e)},focus:function(e){if(!m.rendered)return m;var t=r(N),n=parseInt(H[0].style.zIndex,10),i=b.zindex+t.length,s=r.extend({},e),o;return H.hasClass(k)||m._triggerEvent("focus",[i],s)&&(n!==i&&(t.each(function(){this.style.zIndex>n&&(this.style.zIndex=this.style.zIndex-1)}),t.filter("."+k).qtip("blur",s)),H.addClass(k)[0].style.zIndex=i),m},blur:function(e){return H.removeClass(k),m._triggerEvent("blur",[H.css("zIndex")],e),m},reposition:function(n,i){if(!m.rendered||A)return m;A=1;var o=a.position.target,u=a.position,f=u.my,l=u.at,g=u.adjust,y=g.method.split(" "),b=H.outerWidth(s),S=H.outerHeight(s),x=0,T=0,N=H.css("position"),C=u.viewport,k={left:0,top:0},L=u.container,O=H[0].offsetWidth>0,M=n&&n.type==="scroll",_=r(e),D,P;if(r.isArray(o)&&o.length===2)l={x:h,y:c},k={left:o[0],top:o[1]};else if(o==="mouse"&&(n&&n.pageX||I.event.pageX))l={x:h,y:c},n=E&&E.pageX&&(g.mouse||!n||!n.pageX)?{pageX:E.pageX,pageY:E.pageY}:(!n||n.type!=="resize"&&n.type!=="scroll"?n&&n.pageX&&n.type==="mousemove"?n:!g.mouse&&I.origin&&I.origin.pageX&&a.show.distance?I.origin:n:I.event)||n||I.event||E||{},N!=="static"&&(k=L.offset()),k={left:n.pageX-k.left,top:n.pageY-k.top},g.mouse&&M&&(k.left-=E.scrollX-_.scrollLeft(),k.top-=E.scrollY-_.scrollTop());else{o==="event"&&n&&n.target&&n.type!=="scroll"&&n.type!=="resize"?I.target=r(n.target):o!=="event"&&(I.target=r(o.jquery?o:F.target)),o=I.target,o=r(o).eq(0);if(o.length===0)return m;o[0]===t||o[0]===e?(x=w.iOS?e.innerWidth:o.width(),T=w.iOS?e.innerHeight:o.height(),o[0]===e&&(k={top:(C||o).scrollTop(),left:(C||o).scrollLeft()})):w.imagemap&&o.is("area")?D=w.imagemap(m,o,l,w.viewport?y:s):w.svg&&o[0].ownerSVGElement?D=w.svg(m,o,l,w.viewport?y:s):(x=o.outerWidth(s),T=o.outerHeight(s),k=w.offset(o,L)),D&&(x=D.width,T=D.height,P=D.offset,k=D.position);if(w.iOS>3.1&&w.iOS<4.1||w.iOS>=4.3&&w.iOS<4.33||!w.iOS&&N==="fixed")k.left-=_.scrollLeft(),k.top-=_.scrollTop();k.left+=l.x===d?x:l.x===v?x/2:0,k.top+=l.y===p?T:l.y===v?T/2:0}return k.left+=g.x+(f.x===d?-b:f.x===v?-b/2:0),k.top+=g.y+(f.y===p?-S:f.y===v?-S/2:0),w.viewport?(k.adjusted=w.viewport(m,k,u,x,T,b,S),P&&k.adjusted.left&&(k.left+=P.left),P&&k.adjusted.top&&(k.top+=P.top)):k.adjusted={left:0,top:0},m._triggerEvent("move",[k,C.elem||C],n)?(delete k.adjusted,i===s||!O||isNaN(k.left)||isNaN(k.top)||o==="mouse"||!r.isFunction(u.effect)?H.css(k):r.isFunction(u.effect)&&(u.effect.call(H,m,r.extend({},k)),H.queue(function(e){r(this).css({opacity:"",height:""}),r.browser.msie&&this.style.removeAttribute("filter"),e()})),A=0,m):m},disable:function(e){return"boolean"!=typeof e&&(e=!H.hasClass(j)&&!I.disabled),m.rendered?(H.toggleClass(j,e),r.attr(H[0],"aria-disabled",e)):I.disabled=!!e,m},enable:function(){return m.disable(s)},destroy:function(){var e=u[0],t=r.attr(e,O),n=u.data("qtip");m.destroyed=i,m.rendered&&(H.stop(1,0).remove(),r.each(m.plugins,function(){this.destroy&&this.destroy()})),clearTimeout(m.timers.show),clearTimeout(m.timers.hide),Y();if(!n||m===n)r.removeData(e,"qtip"),a.suppress&&t&&(r.attr(e,"title",t),u.removeAttr(O)),u.removeAttr("aria-describedby");return u.unbind(".qtip-"+f),delete x[m.id],u}})}function H(e,n){var u,a,f,l,c,h=r(this),p=r(t.body),d=this===t?p:h,v=h.metadata?h.metadata(n.metadata):o,m=n.metadata.type==="html5"&&v?v[n.metadata.name]:o,g=h.data(n.metadata.name||"qtipopts");try{g=typeof g=="string"?r.parseJSON(g):g}catch(y){}l=r.extend(i,{},b.defaults,n,typeof g=="object"?D(g):o,D(m||v)),a=l.position,l.id=e;if("boolean"==typeof l.content.text){f=h.attr(l.content.attr);if(l.content.attr===s||!f)return s;l.content.text=f}a.container.length||(a.container=p),a.target===s&&(a.target=d),l.show.target===s&&(l.show.target=d),l.show.solo===i&&(l.show.solo=a.container.closest("body")),l.hide.target===s&&(l.hide.target=d),l.position.viewport===i&&(l.position.viewport=a.container),a.container=a.container.eq(0),a.at=new w.Corner(a.at),a.my=new w.Corner(a.my);if(r.data(this,"qtip"))if(l.overwrite)h.qtip("destroy");else if(l.overwrite===s)return s;return l.suppress&&(c=r.attr(this,"title"))&&r(this).removeAttr("title").attr(O,c).attr("title",""),u=new P(h,l,e,!!f),r.data(this,"qtip",u),h.bind("remove.qtip-"+e+" removeqtip.qtip-"+e,function(){u.destroy()}),u}function B(e){var t=this,n=e.elements.tooltip,o=e.options.content.ajax,u=b.defaults.content.ajax,a=".qtip-ajax",f=/<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi,l=i,c=s,h;e.checks.ajax={"http://www.deere.com/common/deere-resources/js/^content.ajax":function(e,r,i){r==="ajax"&&(o=i),r==="once"?t.init():o&&o.url?t.load():n.unbind(a)}},r.extend(t,{init:function(){return o&&o.url&&n.unbind(a)[o.once?"one":"bind"]("tooltipshow"+a,t.load),t},load:function(n){function g(){var t;if(e.destroyed)return;l=s,v&&(c=i,e.show(n.originalEvent)),(t=u.complete||o.complete)&&r.isFunction(t)&&t.apply(o.context||e,arguments)}function y(t,n,i){var s;if(e.destroyed)return;d&&"string"==typeof t&&(t=r("<div/>").append(t.replace(f,"")).find(d)),(s=u.success||o.success)&&r.isFunction(s)?s.call(o.context||e,t,n,i):e.set("http://www.deere.com/common/deere-resources/js/content.text",t)}function b(t,n,r){if(e.destroyed||t.status===0)return;e.set("http://www.deere.com/common/deere-resources/js/content.text",n+": "+r)}if(c){c=s;return}var a=o.url.lastIndexOf(" "),p=o.url,d,v=!o.loading&&l;if(v)try{n.preventDefault()}catch(m){}else if(n&&n.isDefaultPrevented())return t;h&&h.abort&&h.abort(),a>-1&&(d=p.substr(a),p=p.substr(0,a)),h=r.ajax(r.extend({error:u.error||b,context:e},o,{url:p,success:y,complete:g}))},destroy:function(){h&&h.abort&&h.abort(),e.destroyed=i}}),t.init()}function j(e,t,n){var r=Math.ceil(t/2),i=Math.ceil(n/2),s={bottomright:[[0,0],[t,n],[t,0]],bottomleft:[[0,0],[t,0],[0,n]],topright:[[0,n],[t,0],[t,n]],topleft:[[0,0],[0,n],[t,n]],topcenter:[[0,n],[r,0],[t,n]],bottomcenter:[[0,0],[t,0],[r,n]],rightcenter:[[0,0],[t,i],[0,n]],leftcenter:[[t,0],[t,n],[0,i]]};return s.lefttop=s.bottomright,s.righttop=s.bottomleft,s.leftbottom=s.topright,s.rightbottom=s.topleft,s[e.string()]}function F(e,t){function A(e){var t=E.is(":visible");E.show(),e(),E.toggle(t)}function O(){x.width=g.height,x.height=g.width}function M(){x.width=g.width,x.height=g.height}function _(t,r,o,f){if(!b.tip)return;var l=m.corner.clone(),w=o.adjusted,E=e.options.position.adjust.method.split(" "),x=E[0],T=E[1]||E[0],N={left:s,top:s,x:0,y:0},C,k={},L;m.corner.fixed!==i&&(x===y&&l.precedance===u&&w.left&&l.y!==v?l.precedance=l.precedance===u?a:u:x!==y&&w.left&&(l.x=l.x===v?w.left>0?h:d:l.x===h?d:h),T===y&&l.precedance===a&&w.top&&l.x!==v?l.precedance=l.precedance===a?u:a:T!==y&&w.top&&(l.y=l.y===v?w.top>0?c:p:l.y===c?p:c),l.string()!==S.corner.string()&&(S.top!==w.top||S.left!==w.left)&&m.update(l,s)),C=m.position(l,w),C[l.x]+=P(l,l.x),C[l.y]+=P(l,l.y),C.right!==n&&(C.left=-C.right),C.bottom!==n&&(C.top=-C.bottom),C.user=Math.max(0,g.offset);if(N.left=x===y&&!!w.left)l.x===v?k["margin-left"]=N.x=C["margin-left"]:(L=C.right!==n?[w.left,-C.left]:[-w.left,C.left],(N.x=Math.max(L[0],L[1]))>L[0]&&(o.left-=w.left,N.left=s),k[C.right!==n?d:h]=N.x);if(N.top=T===y&&!!w.top)l.y===v?k["margin-top"]=N.y=C["margin-top"]:(L=C.bottom!==n?[w.top,-C.top]:[-w.top,C.top],(N.y=Math.max(L[0],L[1]))>L[0]&&(o.top-=w.top,N.top=s),k[C.bottom!==n?p:c]=N.y);b.tip.css(k).toggle(!(N.x&&N.y||l.x===v&&N.y||l.y===v&&N.x)),o.left-=C.left.charAt?C.user:x!==y||N.top||!N.left&&!N.top?C.left:0,o.top-=C.top.charAt?C.user:T!==y||N.left||!N.left&&!N.top?C.top:0,S.left=w.left,S.top=w.top,S.corner=l.clone()}function D(){var t=g.corner,n=e.options.position,r=n.at,o=n.my.string?n.my.string():n.my;return t===s||o===s&&r===s?s:(t===i?m.corner=new w.Corner(o):t.string||(m.corner=new w.Corner(t),m.corner.fixed=i),S.corner=new w.Corner(m.corner.string()),m.corner.string()!=="centercenter")}function P(e,t,n){t=t?t:e[e.precedance];var r=b.titlebar&&e.y===c,i=r?b.titlebar:E,s="border-"+t+"-width",o=function(e){return parseInt(e.css(s),10)},u;return A(function(){u=(n?o(n):o(b.content)||o(i)||o(E))||0}),u}function H(e){var t=b.titlebar&&e.y===c,n=t?b.titlebar:b.content,i=r.browser.mozilla,s=i?"-moz-":r.browser.webkit?"-webkit-":"",o="border-radius-"+e.y+e.x,u="border-"+e.y+"-"+e.x+"-radius",a=function(e){return parseInt(n.css(e),10)||parseInt(E.css(e),10)},f;return A(function(){f=a(u)||a(s+u)||a(s+o)||a(o)||0}),f}function B(e){function N(e,t,n){var r=e.css(t)||p;return n&&r===e.css(n)?s:f.test(r)?s:r}var t,n,o,u=b.tip.css("cssText",""),a=e||m.corner,f=/rgba?\(0, 0, 0(, 0)?\)|transparent|#123456/i,l="border-"+a[a.precedance]+"-color",h="background-color",p="transparent",d=" !important",y=b.titlebar,w=y&&(a.y===c||a.y===v&&u.position().top+x.height/2+g.offset<y.outerHeight(i)),S=w?y:b.content;A(function(){T.fill=N(u,h)||N(S,h)||N(b.content,h)||N(E,h)||u.css(h),T.border=N(u,l,"color")||N(S,l,"color")||N(b.content,l,"color")||N(E,l,"color")||E.css(l),r("*",u).add(u).css("cssText",h+":"+p+d+";border:0"+d+";")})}function F(e){var t=e.precedance===a,n=x[t?f:l],r=x[t?l:f],i=e.string().indexOf(v)>-1,s=n*(i?.5:1),o=Math.pow,u=Math.round,c,h,p,d=Math.sqrt(o(s,2)+o(r,2)),m=[N/s*d,N/r*d];return m[2]=Math.sqrt(o(m[0],2)-o(N,2)),m[3]=Math.sqrt(o(m[1],2)-o(N,2)),c=d+m[2]+m[3]+(i?0:m[0]),h=c/d,p=[u(h*r),u(h*n)],{height:p[t?0:1],width:p[t?1:0]}}function I(e,t,n){return"<qvml:"+e+' xmlns="urn:schemas-microsoft.com:vml" class="qtip-vml" '+(t||"")+' style="behavior: url(#default#VML); '+(n||"")+'" />'}var m=this,g=e.options.style.tip,b=e.elements,E=b.tooltip,S={top:0,left:0},x={width:g.width,height:g.height},T={},N=g.border||0,C=".qtip-tip",k=!!(r("<canvas />")[0]||{}).getContext,L;m.corner=o,m.mimic=o,m.border=N,m.offset=g.offset,m.size=x,e.checks.tip={"^position.my|style.tip.(corner|mimic|border)$":function(){m.init()||m.destroy(),e.reposition()},"^style.tip.(height|width)$":function(){x={width:g.width,height:g.height},m.create(),m.update(),e.reposition()},"^content.title.text|style.(classes|widget)$":function(){b.tip&&b.tip.length&&m.update()}},r.extend(m,{init:function(){var e=D()&&(k||r.browser.msie);return e&&(m.create(),m.update(),E.unbind(C).bind("tooltipmove"+C,_)),e},create:function(){var e=x.width,t=x.height,n;b.tip&&b.tip.remove(),b.tip=r("<div />",{"class":"qtip-tip"}).css({width:e,height:t}).prependTo(E),k?r("<canvas />").appendTo(b.tip)[0].getContext("2d").save():(n=I("shape",'coordorigin="0,0"',"position:absolute;"),b.tip.html(n+n),r("*",b.tip).bind("click mousedown",function(e){e.stopPropagation()}))},update:function(e,t){var n=b.tip,f=n.children(),l=x.width,y=x.height,C=g.mimic,L=Math.round,A,_,D,H,q;e||(e=S.corner||m.corner),C===s?C=e:(C=new w.Corner(C),C.precedance=e.precedance,C.x==="inherit"?C.x=e.x:C.y==="inherit"?C.y=e.y:C.x===C.y&&(C[e.precedance]=e[e.precedance])),A=C.precedance,e.precedance===u?O():M(),b.tip.css({width:l=x.width,height:y=x.height}),B(e),T.border!=="transparent"?(N=P(e,o),g.border===0&&N>0&&(T.fill=T.border),m.border=N=g.border!==i?g.border:N):m.border=N=0,D=j(C,l,y),m.size=q=F(e),n.css(q).css("line-height",q.height+"px"),e.precedance===a?H=[L(C.x===h?N:C.x===d?q.width-l-N:(q.width-l)/2),L(C.y===c?q.height-y:0)]:H=[L(C.x===h?q.width-l:0),L(C.y===c?N:C.y===p?q.height-y-N:(q.height-y)/2)],k?(f.attr(q),_=f[0].getContext("2d"),_.restore(),_.save(),_.clearRect(0,0,3e3,3e3),_.fillStyle=T.fill,_.strokeStyle=T.border,_.lineWidth=N*2,_.lineJoin="miter",_.miterLimit=100,_.translate(H[0],H[1]),_.beginPath(),_.moveTo(D[0][0],D[0][1]),_.lineTo(D[1][0],D[1][1]),_.lineTo(D[2][0],D[2][1]),_.closePath(),N&&(E.css("background-clip")==="border-box"&&(_.strokeStyle=T.fill,_.stroke()),_.strokeStyle=T.border,_.stroke()),_.fill()):(D="m"+D[0][0]+","+D[0][1]+" l"+D[1][0]+","+D[1][1]+" "+D[2][0]+","+D[2][1]+" xe",H[2]=N&&/^(r|b)/i.test(e.string())?parseFloat(r.browser.version,10)===8?2:1:0,f.css({coordsize:l+N+" "+(y+N),antialias:""+(C.string().indexOf(v)>-1),left:H[0],top:H[1],width:l+N,height:y+N}).each(function(e){var t=r(this);t[t.prop?"prop":"attr"]({coordsize:l+N+" "+(y+N),path:D,fillcolor:T.fill,filled:!!e,stroked:!e}).toggle(!!N||!!e),!e&&t.html()===""&&t.html(I("stroke",'weight="'+N*2+'px" color="'+T.border+'" miterlimit="1000" joinstyle="miter"'))})),t!==s&&m.position(e)},position:function(e){var t=b.tip,n={},i=Math.max(0,g.offset),o,p,d;return g.corner===s||!t?s:(e=e||m.corner,o=e.precedance,p=F(e),d=[e.x,e.y],o===u&&d.reverse(),r.each(d,function(t,r){var s,u,d;r===v?(s=o===a?h:c,n[s]="50%",n["margin-"+s]=-Math.round(p[o===a?f:l]/2)+i):(s=P(e,r),u=P(e,r,b.content),d=H(e),n[r]=t?u:i+(d>s?d:-s))}),n[e[o]]-=p[o===u?f:l],t.css({top:"",bottom:"",left:"",right:"",margin:""}).css(n),n)},destroy:function(){b.tip&&b.tip.remove(),b.tip=!1,E.unbind(C)}}),m.init()}function I(n){function y(){m=r(v,f).not("[disabled]").map(function(){return typeof this.focus=="function"?this:null})}function b(e){m.length<1&&e.length?e.not("body").blur():m.first().focus()}function E(e){var t=r(e.target),n=t.closest(".qtip"),i;i=n.length<1?s:parseInt(n[0].style.zIndex,10)>parseInt(f[0].style.zIndex,10),!i&&r(e.target).closest(N)[0]!==f[0]&&b(t)}var o=this,u=n.options.show.modal,a=n.elements,f=a.tooltip,l="#qtip-overlay",c=".qtipmodal",h=c+n.id,p="is-modal-qtip",d=r(t.body),v=w.modal.focusable.join(","),m={},g;n.checks.modal={"^show.modal.(on|blur)$":function(){o.init(),a.overlay.toggle(f.is(":visible"))},"^content.text$":function(){y()}},r.extend(o,{init:function(){return u.on?(g=o.create(),f.attr(p,i).css("z-index",w.modal.zindex+r(N+"["+p+"]").length).unbind(c).unbind(h).bind("tooltipshow"+c+" tooltiphide"+c,function(e,t,n){var i=e.originalEvent;if(e.target===f[0])if(i&&e.type==="tooltiphide"&&/mouse(leave|enter)/.test(i.type)&&r(i.relatedTarget).closest(g[0]).length)try{e.preventDefault()}catch(s){}else(!i||i&&!i.solo)&&o[e.type.replace("tooltip","")](e,n)}).bind("tooltipfocus"+c,function(e){if(e.isDefaultPrevented()||e.target!==f[0])return;var t=r(N).filter("["+p+"]"),n=w.modal.zindex+t.length,i=parseInt(f[0].style.zIndex,10);g[0].style.zIndex=n-2,t.each(function(){this.style.zIndex>i&&(this.style.zIndex-=1)}),t.end().filter("."+k).qtip("blur",e.originalEvent),f.addClass(k)[0].style.zIndex=n;try{e.preventDefault()}catch(s){}}).bind("tooltiphide"+c,function(e){e.target===f[0]&&r("["+p+"]").filter(":visible").not(f).last().qtip("focus",e)}),u.escape&&r(t).unbind(h).bind("keydown"+h,function(e){e.keyCode===27&&f.hasClass(k)&&n.hide(e)}),u.blur&&a.overlay.unbind(h).bind("click"+h,function(e){f.hasClass(k)&&n.hide(e)}),y(),o):o},create:function(){function i(){g.css({height:n.height(),width:n.width()})}var t=r(l),n=r(e);return t.length?a.overlay=t.insertAfter(r(N).last()):(g=a.overlay=r("<div />",{id:l.substr(1),html:"<div></div>",mousedown:function(){return s}}).hide().insertAfter(r(N).last()),n.unbind(c).bind("resize"+c,i),i(),g)},toggle:function(e,t,n){if(e&&e.isDefaultPrevented())return o;var a=u.effect,l=t?"show":"hide",c=g.is(":visible"),v=r("["+p+"]").filter(":visible").not(f),m;return g||(g=o.create()),g.is(":animated")&&c===t&&g.data("toggleState")!==s||!t&&v.length?o:(t?(g.css({left:0,top:0}),g.toggleClass("blurs",u.blur),u.stealfocus!==s&&(d.bind("focusin"+h,E),b(r("body :focus")))):d.unbind("focusin"+h),g.stop(i,s).data("toggleState",t),r.isFunction(a)?a.call(g,t):a===s?g[l]():g.fadeTo(parseInt(n,10)||90,t?1:0,function(){t||r(this).hide()}),t||g.queue(function(e){g.css({left:"",top:""}).removeData("toggleState"),e()}),o)},show:function(e,t){return o.toggle(e,i,t)},hide:function(e,t){return o.toggle(e,s,t)},destroy:function(){var e=g;return e&&(e=r("["+p+"]").not(f).length<1,e?(a.overlay.remove(),r(t).unbind(c)):a.overlay.unbind(c+n.id),d.unbind("focusin"+h)),f.removeAttr(p).unbind(c)}}),o.init()}function q(n){var o=this,u=n.elements,a=n.options,c=u.tooltip,h=".ie6-"+n.id,p=r("select, object").length<1,d=0,v=s,m;n.checks.ie6={"^content|style$":function(e,t,n){redraw()}},r.extend(o,{init:function(){var n=r(e),s;p&&(u.bgiframe=r('<iframe class="qtip-bgiframe" frameborder="0" tabindex="-1" src="javascript:\'\';"  style="display:block; position:absolute; z-index:-1; filter:alpha(opacity=0); -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";"></iframe>'),u.bgiframe.appendTo(c),c.bind("tooltipmove"+h,o.adjustBGIFrame)),m=r("<div/>",{id:"qtip-rcontainer"}).appendTo(t.body),o.redraw(),u.overlay&&!v&&(s=function(){u.overlay[0].style.top=n.scrollTop()+"px"},n.bind("scroll.qtip-ie6, resize.qtip-ie6",s),s(),u.overlay.addClass("qtipmodal-ie6fix"),v=i)},adjustBGIFrame:function(){var e=n.get("dimensions"),t=n.plugins.tip,r=u.tip,i,s;s=parseInt(c.css("border-left-width"),10)||0,s={left:-s,top:-s},t&&r&&(i=t.corner.precedance==="x"?["width","left"]:["height","top"],s[i[1]]-=r[i[0]]()),u.bgiframe.css(s).css(e)},redraw:function(){if(n.rendered<1||d)return o;var e=a.style,t=a.position.container,r,i,s,u;return d=1,e.height&&c.css(l,e.height),e.width?c.css(f,e.width):(c.css(f,"").appendTo(m),i=c.width(),i%2<1&&(i+=1),s=c.css("max-width")||"",u=c.css("min-width")||"",r=(s+u).indexOf("%")>-1?t.width()/100:0,s=(s.indexOf("%")>-1?r:1)*parseInt(s,10)||i,u=(u.indexOf("%")>-1?r:1)*parseInt(u,10)||0,i=s+u?Math.min(Math.max(i,u),s):i,c.css(f,Math.round(i)).appendTo(t)),d=0,o},destroy:function(){p&&u.bgiframe.remove(),c.unbind(h)}}),o.init()}var i=!0,s=!1,o=null,u="x",a="y",f="width",l="height",c="top",h="left",p="bottom",d="right",v="center",m="flip",g="flipinvert",y="shift",b,w,E,S="qtip",x={},T=["ui-widget","ui-tooltip"],N="div.qtip."+S,C=S+"-default",k=S+"-focus",L=S+"-hover",A="_replacedByqTip",O="oldtitle",M;b=r.fn.qtip=function(e,t,u){var a=(""+e).toLowerCase(),f=o,l=r.makeArray(arguments).slice(1),c=l[l.length-1],h=this[0]?r.data(this[0],"qtip"):o;if(!arguments.length&&h||a==="api")return h;if("string"==typeof e)return this.each(function(){var e=r.data(this,"qtip");if(!e)return i;c&&c.timeStamp&&(e.cache.event=c);if(a!=="option"&&a!=="options"||!t)e[a]&&e[a].apply(e[a],l);else{if(!r.isPlainObject(t)&&u===n)return f=e.get(t),s;e.set(t,u)}}),f!==o?f:this;if("object"==typeof e||!arguments.length)return h=D(r.extend(i,{},e)),b.bind.call(this,h,c)},b.bind=function(e,t){return this.each(function(o){function p(e){function t(){c.render(typeof e=="object"||u.show.ready),a.show.add(a.hide).unbind(l)}if(c.cache.disabled)return s;c.cache.event=r.extend({},e),c.cache.target=e?r(e.target):[n],u.show.delay>0?(clearTimeout(c.timers.show),c.timers.show=setTimeout(t,u.show.delay),f.show!==f.hide&&a.hide.bind(f.hide,function(){clearTimeout(c.timers.show)})):t()}var u,a,f,l,c,h;h=r.isArray(e.id)?e.id[o]:e.id,h=!h||h===s||h.length<1||x[h]?b.nextid++:x[h]=h,l=".qtip-"+h+"-create",c=H.call(this,h,e);if(c===s)return i;u=c.options,r.each(w,function(){this.initialize==="initialize"&&this(c)}),a={show:u.show.target,hide:u.hide.target},f={show:r.trim(""+u.show.event).replace(/ /g,l+" ")+l,hide:r.trim(""+u.hide.event).replace(/ /g,l+" ")+l},/mouse(over|enter)/i.test(f.show)&&!/mouse(out|leave)/i.test(f.hide)&&(f.hide+=" mouseleave"+l),a.show.bind("mousemove"+l,function(e){_(e),c.cache.onTarget=i}),a.show.bind(f.show,p),(u.show.ready||u.prerender)&&p(t)}).attr("data-hasqtip",i)},w=b.plugins={Corner:function(e){e=(""+e).replace(/([A-Z])/," $1").replace(/middle/gi,v).toLowerCase(),this.x=(e.match(/left|right/i)||e.match(/center/)||["inherit"])[0].toLowerCase(),this.y=(e.match(/top|bottom|center/i)||["inherit"])[0].toLowerCase();var t=e.charAt(0);this.precedance=t==="t"||t==="b"?a:u,this.string=function(){return this.precedance===a?this.y+this.x:this.x+this.y},this.abbrev=function(){var e=this.x.substr(0,1),t=this.y.substr(0,1);return e===t?e:this.precedance===a?t+e:e+t},this.invertx=function(e){this.x=this.x===h?d:this.x===d?h:e||this.x},this.inverty=function(e){this.y=this.y===c?p:this.y===p?c:e||this.y},this.clone=function(){return{x:this.x,y:this.y,precedance:this.precedance,string:this.string,abbrev:this.abbrev,clone:this.clone,invertx:this.invertx,inverty:this.inverty}}},offset:function(e,n){function c(e,t){i.left+=t*e.scrollLeft(),i.top+=t*e.scrollTop()}var i=e.offset(),s=e.closest("body"),o=r.browser.msie&&t.compatMode!=="CSS1Compat",u=n,a,f,l;if(u){do u.css("position")!=="static"&&(f=u.position(),i.left-=f.left+(parseInt(u.css("borderLeftWidth"),10)||0)+(parseInt(u.css("marginLeft"),10)||0),i.top-=f.top+(parseInt(u.css("borderTopWidth"),10)||0)+(parseInt(u.css("marginTop"),10)||0),!a&&(l=u.css("overflow"))!=="hidden"&&l!=="visible"&&(a=u));while((u=r(u[0].offsetParent)).length);(a&&a[0]!==s[0]||o)&&c(a||s,1)}return i},iOS:parseFloat((""+(/CPU.*OS ([0-9_]{1,5})|(CPU like).*AppleWebKit.*Mobile/i.exec(navigator.userAgent)||[0,""])[1]).replace("undefined","3_2").replace("_",".").replace("_",""))||s,fn:{attr:function(e,t){if(this.length){var n=this[0],i="title",s=r.data(n,"qtip");if(e===i&&s&&"object"==typeof s&&s.options.suppress)return arguments.length<2?r.attr(n,O):(s&&s.options.content.attr===i&&s.cache.attr&&s.set("http://www.deere.com/common/deere-resources/js/content.text",t),this.attr(O,t))}return r.fn["attr"+A].apply(this,arguments)},clone:function(e){var t=r([]),n="title",i=r.fn["clone"+A].apply(this,arguments);return e||i.filter("["+O+"]").attr("title",function(){return r.attr(this,O)}).removeAttr(O),i}}},r.each(w.fn,function(e,t){if(!t||r.fn[e+A])return i;var n=r.fn[e+A]=r.fn[e];r.fn[e]=function(){return t.apply(this,arguments)||n.apply(this,arguments)}}),r.ui||(r["cleanData"+A]=r.cleanData,r.cleanData=function(e){for(var t=0,i;(i=e[t])!==n;t++)try{r(i).triggerHandler("removeqtip")}catch(s){}r["cleanData"+A](e)}),b.version="2.0.0-nightly-15f5c6bc20",b.nextid=0,b.inactiveEvents="click dblclick mousedown mouseup mousemove mouseleave mouseenter".split(" "),b.zindex=15e3,b.defaults={prerender:s,id:s,overwrite:i,suppress:i,content:{text:i,attr:"title",deferred:s,title:{text:s,button:s}},position:{my:"top left",at:"bottom right",target:s,container:s,viewport:s,adjust:{x:0,y:0,mouse:i,resize:i,method:"flipinvert flipinvert"},effect:function(e,t,n){r(this).animate(t,{duration:200,queue:s})}},show:{target:s,event:"mouseenter",effect:i,delay:90,solo:s,ready:s,autofocus:s},hide:{target:s,event:"mouseleave",effect:i,delay:0,fixed:s,inactive:s,leave:"window",distance:s},style:{classes:"",widget:s,width:s,height:s,def:i},events:{render:o,move:o,show:o,hide:o,toggle:o,visible:o,hidden:o,focus:o,blur:o}},w.svg=function(e,n,i,s){var o=r(t),u=n[0],a={width:0,height:0,position:{top:1e10,left:1e10}},f,l,c,h,p;while(!u.getBBox)u=u.parentNode;if(u.getBBox&&u.parentNode){f=u.getBBox(),l=u.getScreenCTM(),c=u.farthestViewportElement||u;if(!c.createSVGPoint)return a;h=c.createSVGPoint(),h.x=f.x,h.y=f.y,p=h.matrixTransform(l),a.position.left=p.x,a.position.top=p.y,h.x+=f.width,h.y+=f.height,p=h.matrixTransform(l),a.width=p.x-a.position.left,a.height=p.y-a.position.top,a.position.left+=o.scrollLeft(),a.position.top+=o.scrollTop()}return a},w.ajax=function(e){var t=e.plugins.ajax;return"object"==typeof t?t:e.plugins.ajax=new B(e)},w.ajax.initialize="render",w.ajax.sanitize=function(e){var t=e.content,n;t&&"ajax"in t&&(n=t.ajax,typeof n!="object"&&(n=e.content.ajax={url:n}),"boolean"!=typeof n.once&&n.once&&(n.once=!!n.once))},r.extend(i,b.defaults,{content:{ajax:{loading:i,once:i}}}),w.tip=function(e){var t=e.plugins.tip;return"object"==typeof t?t:e.plugins.tip=new F(e)},w.tip.initialize="render",w.tip.sanitize=function(e){var t=e.style,n;t&&"tip"in t&&(n=e.style.tip,typeof n!="object"&&(e.style.tip={corner:n}),/string|boolean/i.test(typeof n.corner)||(n.corner=i),typeof n.width!="number"&&delete n.width,typeof n.height!="number"&&delete n.height,typeof n.border!="number"&&n.border!==i&&delete n.border,typeof n.offset!="number"&&delete n.offset)},r.extend(i,b.defaults,{style:{tip:{corner:i,mimic:s,width:6,height:6,border:i,offset:0}}}),w.modal=function(e){var t=e.plugins.modal;return"object"==typeof t?t:e.plugins.modal=new I(e)},w.modal.initialize="render",w.modal.sanitize=function(e){e.show&&(typeof e.show.modal!="object"?e.show.modal={on:!!e.show.modal}:typeof e.show.modal.on=="undefined"&&(e.show.modal.on=i))},w.modal.zindex=b.zindex-200,w.modal.focusable=["a[href]","area[href]","input","select","textarea","button","iframe","object","embed","[tabindex]","[contenteditable]"],r.extend(i,b.defaults,{show:{modal:{on:s,effect:i,blur:i,stealfocus:i,escape:i}}}),w.viewport=function(n,r,i,s,o,m,b){function j(e,t,n,i,s,o,u,a,f){var l=r[s],c=x[e],h=T[e],p=n===y,d=-O.offset[s]+A.offset[s]+A["scroll"+s],m=c===s?f:c===o?-f:-f/2,b=h===s?a:h===o?-a:-a/2,w=_&&_.size?_.size[u]||0:0,E=_&&_.corner&&_.corner.precedance===e&&!p?w:0,S=d-l+E,N=l+f-A[u]-d+E,C=m-(x.precedance===e||c===x[t]?b:0)-(h===v?a/2:0);return p?(E=_&&_.corner&&_.corner.precedance===t?w:0,C=(c===s?1:-1)*m-E,r[s]+=S>0?S:N>0?-N:0,r[s]=Math.max(-O.offset[s]+A.offset[s]+(E&&_.corner[e]===v?_.offset:0),l-C,Math.min(Math.max(-O.offset[s]+A.offset[s]+A[u],l+C),r[s]))):(i*=n===g?2:0,S>0&&(c!==s||N>0)?(r[s]-=C+i,H["invert"+e](s)):N>0&&(c!==o||S>0)&&(r[s]-=(c===v?-C:C)+i,H["invert"+e](o)),r[s]<d&&-r[s]>N&&(r[s]=l,H=x.clone())),r[s]-l}var w=i.target,E=n.elements.tooltip,x=i.my,T=i.at,N=i.adjust,C=N.method.split(" "),k=C[0],L=C[1]||C[0],A=i.viewport,O=i.container,M=n.cache,_=n.plugins.tip,D={left:0,top:0},P,H,B;if(!A.jquery||w[0]===e||w[0]===t.body||N.method==="none")return D;P=E.css("position")==="fixed",A={elem:A,height:A[(A[0]===e?"h":"outerH")+"eight"](),width:A[(A[0]===e?"w":"outerW")+"idth"](),scrollleft:P?0:A.scrollLeft(),scrolltop:P?0:A.scrollTop(),offset:A.offset()||{left:0,top:0}},O={elem:O,scrollLeft:O.scrollLeft(),scrollTop:O.scrollTop(),offset:O.offset()||{left:0,top:0}};if(k!=="shift"||L!=="shift")H=x.clone();return D={left:k!=="none"?j(u,a,k,N.x,h,d,f,s,m):0,top:L!=="none"?j(a,u,L,N.y,c,p,l,o,b):0},H&&M.lastClass!==(B=S+"-pos-"+H.abbrev())&&E.removeClass(n.cache.lastClass).addClass(n.cache.lastClass=B),D},w.imagemap=function(e,t,n,i){function E(e,t,n){var r=0,i=1,s=1,o=0,u=0,a=e.width,f=e.height;while(a>0&&f>0&&i>0&&s>0){a=Math.floor(a/2),f=Math.floor(f/2),n.x===h?i=a:n.x===d?i=e.width-a:i+=Math.floor(a/2),n.y===c?s=f:n.y===p?s=e.height-f:s+=Math.floor(f/2),r=t.length;while(r--){if(t.length<2)break;o=t[r][0]-e.position.left,u=t[r][1]-e.position.top,(n.x===h&&o>=i||n.x===d&&o<=i||n.x===v&&(o<i||o>e.width-i)||n.y===c&&u>=s||n.y===p&&u<=s||n.y===v&&(u<s||u>e.height-s))&&t.splice(r,1)}}return{left:t[0][0],top:t[0][1]}}t.jquery||(t=r(t));var s=e.cache.areas={},o=(t[0].shape||t.attr("shape")).toLowerCase(),u=t[0].coords||t.attr("coords"),a=u.split(","),f=[],l=r('img[usemap="#'+t.parent("map").attr("name")+'"]'),m=l.offset(),g={width:0,height:0,position:{top:1e10,right:0,bottom:0,left:1e10}},y=0,b=0,w;m.left+=Math.ceil((l.outerWidth()-l.width())/2),m.top+=Math.ceil((l.outerHeight()-l.height())/2);if(o==="poly"){y=a.length;while(y--)b=[parseInt(a[--y],10),parseInt(a[y+1],10)],b[0]>g.position.right&&(g.position.right=b[0]),b[0]<g.position.left&&(g.position.left=b[0]),b[1]>g.position.bottom&&(g.position.bottom=b[1]),b[1]<g.position.top&&(g.position.top=b[1]),f.push(b)}else{y=-1;while(y++<a.length)f.push(parseInt(a[y],10))}switch(o){case"rect":g={width:Math.abs(f[2]-f[0]),height:Math.abs(f[3]-f[1]),position:{left:Math.min(f[0],f[2]),top:Math.min(f[1],f[3])}};break;case"circle":g={width:f[2]+2,height:f[2]+2,position:{left:f[0],top:f[1]}};break;case"poly":g.width=Math.abs(g.position.right-g.position.left),g.height=Math.abs(g.position.bottom-g.position.top),n.abbrev()==="c"?g.position={left:g.position.left+g.width/2,top:g.position.top+g.height/2}:(s[n+u]||(g.position=E(g,f.slice(),n),i&&(i[0]==="flip"||i[1]==="flip")&&(g.offset=E(g,f.slice(),{x:n.x===h?d:n.x===d?h:v,y:n.y===c?p:n.y===p?c:v}),g.offset.left-=g.position.left,g.offset.top-=g.position.top),s[n+u]=g),g=s[n+u]),g.width=g.height=0}return g.position.left+=m.left,g.position.top+=m.top,g},w.ie6=function(e){var t=r.browser,n=e.plugins.ie6;return!t.msie||(""+t.version).charAt(0)!=="6"?s:"object"==typeof n?n:e.plugins.ie6=new q(e)},w.ie6.initialize="render"})})(window,document);


jQuery.fn.makeacolumnlists = function(settings){
	settings = jQuery.extend({
		cols: 2,				// set number of columns
		colWidth: 0,			// set width for each column or leave 0 for auto width
		equalHeight: false, 	// can be false, 'ul', 'ol', 'li'
		startN: 1				// first number on your ordered list
	}, settings);

	if(jQuery('> li', this)) {
		this.each(function(y) {
			var y=jQuery('.li_container').size(),
		    	height = 0,
		        maxHeight = 0,
				t = jQuery(this),
				classN = t.attr('class'),
				listsize = jQuery('> li', this).size(),
				percol = Math.ceil(listsize/settings.cols),
				contW = t.width(),
				bl = ( isNaN(parseInt(t.css('borderLeftWidth'),10)) ? 0 : parseInt(t.css('borderLeftWidth'),10) ),
				br = ( isNaN(parseInt(t.css('borderRightWidth'),10)) ? 0 : parseInt(t.css('borderRightWidth'),10) ),
				pl = parseInt(t.css('paddingLeft'),10),
				pr = parseInt(t.css('paddingRight'),10),
				ml = parseInt(t.css('marginLeft'),10),
				mr = parseInt(t.css('marginRight'),10),
				col_Width = Math.floor((contW - (settings.cols-1)*(bl+br+pl+pr+ml+mr))/settings.cols);
			if (settings.colWidth) {
				col_Width = settings.colWidth;
			}
			var colnum=1,
				percol2=percol;
			jQuery(this).addClass('li_cont1').wrap('<div id="li_container' + (++y) + '" class="li_container"></div>');
			for (var i=0; i<=listsize; i++) {
				if(i>=percol2) { percol2+=percol; colnum++; }
				var eq = jQuery('> li:eq('+i+')',this);
				eq.addClass('li_col'+ colnum);
				if(jQuery(this).is('ol')){eq.attr('value', ''+(i+settings.startN))+'';}
			}
			jQuery(this).css({float:'left', width:''+col_Width+'px'});
			for (colnum=2; colnum<=settings.cols; colnum++) {
				if(jQuery(this).is('ol')) {
					jQuery('li.li_col'+ colnum, this).appendTo('#li_container' + y).wrapAll('<ol class="li_cont'+colnum +' ' + classN + '" style="float:left; width: '+col_Width+'px;"></ol>');
				} else {
					jQuery('li.li_col'+ colnum, this).appendTo('#li_container' + y).wrapAll('<ul class="li_cont'+colnum +' ' + classN + '" style="float:left; width: '+col_Width+'px;"></ul>');
				}
			}
			if (settings.equalHeight=='li') {
				for (colnum=1; colnum<=settings.cols; colnum++) {
				    jQuery('#li_container'+ y +' li').each(function() {
				        var e = jQuery(this);
				        var border_top = ( isNaN(parseInt(e.css('borderTopWidth'),10)) ? 0 : parseInt(e.css('borderTopWidth'),10) );
				        var border_bottom = ( isNaN(parseInt(e.css('borderBottomWidth'),10)) ? 0 : parseInt(e.css('borderBottomWidth'),10) );
				        height = e.height() + parseInt(e.css('paddingTop'), 10) + parseInt(e.css('paddingBottom'), 10) + border_top + border_bottom;
				        maxHeight = (height > maxHeight) ? height : maxHeight;
				    });
				}
				for (colnum=1; colnum<=settings.cols; colnum++) {
					var eh = jQuery('#li_container'+ y +' li');
			        var border_top = ( isNaN(parseInt(eh.css('borderTopWidth'),10)) ? 0 : parseInt(eh.css('borderTopWidth'),10) );
			        var border_bottom = ( isNaN(parseInt(eh.css('borderBottomWidth'),10)) ? 0 : parseInt(eh.css('borderBottomWidth'),10) );
					mh = maxHeight - (parseInt(eh.css('paddingTop'), 10) + parseInt(eh.css('paddingBottom'), 10) + border_top + border_bottom );
			        eh.height(mh);
				}
			} else
			if (settings.equalHeight=='ul' || settings.equalHeight=='ol') {
				for (colnum=1; colnum<=settings.cols; colnum++) {
				    jQuery('#li_container'+ y +' .li_cont'+colnum).each(function() {
				        var e = jQuery(this);
				        var border_top = ( isNaN(parseInt(e.css('borderTopWidth'),10)) ? 0 : parseInt(e.css('borderTopWidth'),10) );
				        var border_bottom = ( isNaN(parseInt(e.css('borderBottomWidth'),10)) ? 0 : parseInt(e.css('borderBottomWidth'),10) );
				        height = e.height() + parseInt(e.css('paddingTop'), 10) + parseInt(e.css('paddingBottom'), 10) + border_top + border_bottom;
				        maxHeight = (height > maxHeight) ? height : maxHeight;
				    });
				}
				for (colnum=1; colnum<=settings.cols; colnum++) {
					var eh = jQuery('#li_container'+ y +' .li_cont'+colnum);
			        var border_top = ( isNaN(parseInt(eh.css('borderTopWidth'),10)) ? 0 : parseInt(eh.css('borderTopWidth'),10) );
			        var border_bottom = ( isNaN(parseInt(eh.css('borderBottomWidth'),10)) ? 0 : parseInt(eh.css('borderBottomWidth'),10) );
					mh = maxHeight - (parseInt(eh.css('paddingTop'), 10) + parseInt(eh.css('paddingBottom'), 10) + border_top + border_bottom );
			        eh.height(mh);
				}
			}
		    jQuery('#li_container' + y).append('<div style="clear:both; overflow:hidden; height:0px;"></div>');
		});
	}
}

jQuery.fn.uncolumnlists = function(){
	jQuery('.li_cont1').each(function(i) {
		var onecolSize = jQuery('#li_container' + (++i) + ' .li_cont1 > li').size();
		if(jQuery('#li_container' + i + ' .li_cont1').is('ul')) {
			jQuery('#li_container' + i + ' > ul > li').appendTo('#li_container' + i + ' ul:first');
			for (var j=1; j<=onecolSize; j++) {
				jQuery('#li_container' + i + ' ul:first li').removeAttr('class').removeAttr('style');
			}
			jQuery('#li_container' + i + ' ul:first').removeAttr('style').removeClass('li_cont1').insertBefore('#li_container' + i);
		} else {
			jQuery('#li_container' + i + ' > ol > li').appendTo('#li_container' + i + ' ol:first');
			for (var j=1; j<=onecolSize; j++) {
				jQuery('#li_container' + i + ' ol:first li').removeAttr('class').removeAttr('style');
			}
			jQuery('#li_container' + i + ' ol:first').removeAttr('style').removeClass('li_cont1').insertBefore('#li_container' + i);
		}
		jQuery('#li_container' + i).remove();
	});
}

/* Overwriting ajax enabled paginate plugin - Prev and Next links */
function disablePrevNextLinks () {
  if($('.page_navigation')[0]) {
    if($('.page_navigation a.first').hasClass('active_page')) {
      $('.page_navigation .previous_link, .page_navigation a.first_link').hide();	  
    } else {
      $('.page_navigation .previous_link, .page_navigation a.first_link').show();
    }
    if($('.page_navigation a.last').hasClass('active_page')) {
      $('.page_navigation .next_link, .page_navigation a.last_link').hide();	  
    } else {
      $('.page_navigation .next_link, .page_navigation a.last_link').show();
    }
  }
}

/* Overwriting ajax enabled paginate plugin - Prev and Next links, enabled focus  */
function disablePrevNextLinksFocus () {
  if($('.page_navigation')[0]) {
    if($('.page_navigation a.first').hasClass('active_page')) {
      $('.page_navigation .previous_link, .page_navigation a.first_link').hide();
	  $('.page_navigation a:visible:first').focus();
    } else {
      $('.page_navigation .previous_link, .page_navigation a.first_link').show();
    }
    if($('.page_navigation a.last').hasClass('active_page')) {
      $('.page_navigation .next_link, .page_navigation a.last_link').hide();
	  $('.page_navigation a:visible:last').focus();
    } else {
      $('.page_navigation .next_link, .page_navigation a.last_link').show();
    }
  }
}

var lightboxShowHidePanelActive = $("#jdlightbox .MOD_FO_6a h3:first, #jdlightbox .MOD_FO_6a .secondary_subhead:first").hasClass("open");
	
if (lightboxShowHidePanelActive == false){
	setTimeout(function(){$('#jdlightbox .MOD_FO_6a h3:first, #jdlightbox .MOD_FO_6a .secondary_subhead:first').trigger("click");}, 1000);
}

$.fn.showFixedTooltip = function (href, selector, wrapper, ajax) {
	   $(".externalTip").each(function(){
		   var _this = $(this);
		   _this.find('img').attr({alt: helpToolTip,title: helpToolTip});
		   _this.qtip({
				content: {
					text: '<img class="throbber" src="../img/screen_freeze_ajax_loader.gif"/*tpa=http://www.deere.com/common/deere-resources/img/screen_freeze_ajax_loader.gif*/ width="28" height="28" alt="'+langLoading+'" />',
					ajax: {
						url: $(this).data('href')
					},
					title: {
						button: true
					}
				},
				position: {
					my: 'left middle',
					at: 'right middle',
					viewport: $(window)
				},
				show: {
					event: 'click',
					solo: true
				},
				hide: {
					event: 'unfocus'
				},
				style: {
					classes: 'qtip-shadow qtip-light',
					tip: {
						corner: true,
						width: 16
					}
				},
				events: {
					visible: function(event, api) {
						var tooltip = api.elements.tooltip;
						tooltip.find('a.qtip-close.qtip-icon').attr('href', 'javascript:void(0);').focus();
					},
					hidden: function(event, api) {
						$(_focusedElementBeforeModal).focus();
					}
				}
			})
	   })
}

$.fn.showFixedTooltipResults = function (href, selector, wrapper, ajax) {
	   $(".externalTipResults").each(function(){
		   $(this).qtip({
				content: {
					text: '<img class="throbber" src="../img/screen_freeze_ajax_loader.gif"/*tpa=http://www.deere.com/common/deere-resources/img/screen_freeze_ajax_loader.gif*/ width="28" height="28" alt="'+langLoading+'" />',
					ajax: {
						url: $(this).data('href')
					},
					title: {
						button: true
					}
				},
				position: {
					my: 'left middle',
					at: 'right middle',
					viewport: $(window)
				},
				show: {
					event: 'click',
					solo: true
				},
				hide: {
					event: 'unfocus'
				},
				style: {
					classes: 'qtip-shadow qtip-light externalTipResults',
					tip: {
						corner: true,
						width: 16
					}
				},
				events: {
					visible: function(event, api) {
						var tooltip = api.elements.tooltip;
						tooltip.find('a.qtip-close.qtip-icon').attr('href', 'javascript:void(0);').focus();
					},
					hidden: function(event, api) {
						$(_focusedElementBeforeModal).focus();
					}
				}
			})
	   })
}

$.fn.showTooltipSPFH = function (href, selector, wrapper, ajax) {
	   $(".externalTipSPFH").each(function(){
		   	var _this = $(this);
			($.trim(_this.data('content')) == '') ? _this.remove() : '';
		   _this.qtip({
				content: {
					text: $(this).data('content'),
					title: {
						button: true
					}
				},
				position: {
					my: 'top center',
					at: 'bottom center',
					viewport: $(window)
				},
				show: {
					event: 'click',
					solo: true
				},
				hide: {
					event: 'unfocus'
				},
				style: {
					classes: 'qtip-shadow qtip-light externalTipResultsSPFH',
					tip: {
						corner: true,
						width: 16
					}
				},/*RS WCAG II tooltip focus */
				events: {
					visible: function(event, api) {
						var tooltip = api.elements.tooltip;
						tooltip.find('a.qtip-close.qtip-icon').attr('href', 'javascript:void(0);').focus();
					},
					hidden: function(event, api) {
						$(_focusedElementBeforeModal).focus();
					}
				}
			})
	   })
}
     
$.fn.showInternalTooltip = function () {
    $(".fixedTip").each(function () {
        var count = $(this).data('count');
        $(this).qtip({
            content: {
                text: $(".MOD_PS_M18.M_" + count),
                title: {
                    button: true
                }
            },
            position: {
				my: 'left middle',
				at: 'right middle',
				viewport: $(window)
            },
            show: {
                event: 'click',
                solo: false
            },
            hide: {
	    	event: 'unfocus'
	    },
			style: {
				classes: 'qtip-shadow qtip-light internalTip',
				tip: {
					corner: true,
					width: 16
				}
			},
			events: {
				visible: function(event, api) {
					var tooltip = api.elements.tooltip;
					tooltip.find('a.qtip-close.qtip-icon').attr('href', 'javascript:void(0);').focus();
				},
				hidden: function(event, api) {
					$(_focusedElementBeforeModal).focus();
				}
			}
        })
    });

}

/*Code for Equal Height*/
jQuery.extend({
    applyHeight: function(o, ele){
		$(o).each(function () {
			var _currentBar = $(this);
			var _currentAddonBar = _currentBar.find(ele);

			var _attachmentBaseHeight;

			var _currentBarDisplayElement = _currentBar.parent().parent();
			var _currentBarDisplay = _currentBarDisplayElement.css("display");

			if (_currentBarDisplay == "none") {
				$(_currentBarDisplayElement).css({
					visibility: 'hidden',
					display: 'block'
				});
			}

			$(_currentAddonBar).each(function () {
				var _attachmentHeight = $(this).outerHeight();
				_attachmentBaseHeight = (_attachmentBaseHeight >= _attachmentHeight) ? _attachmentBaseHeight : _attachmentHeight;
			})
			//console.log(_attachmentBaseHeight);
			if(_attachmentBaseHeight > "0"){
				$(_currentBar).find(ele).css('height', _attachmentBaseHeight);
			}
			if (_currentBarDisplay == "none") {
				$(_currentBarDisplayElement).css({
					visibility: 'visible',
					display: 'none'
				});
			}
		})
	}
});



/*jslint evil: true, regexp: true */

/*members "", "\b", "\t", "\n", "\f", "\r", "\"", JSON, "\\", apply,
call, charCodeAt, getUTCDate, getUTCFullYear, getUTCHours,
getUTCMinutes, getUTCMonth, getUTCSeconds, hasOwnProperty, join,
lastIndex, length, parse, prototype, push, replace, slice, stringify,
test, toJSON, toString, valueOf
*/


// Create a JSON object only if one does not already exist. We create the
// methods in a closure to avoid creating global variables.

if (typeof JSON !== 'object') {
    JSON = {};
}

(function () {
    'use strict';

    function f(n) {
        // Format integers to have at least two digits.
        return n < 10 ? '0' + n : n;
    }

    if (typeof Date.prototype.toJSON !== 'function') {

        Date.prototype.toJSON = function (key) {

            return isFinite(this.valueOf())
                ? this.getUTCFullYear() + '-' +
                    f(this.getUTCMonth() + 1) + '-' +
                    f(this.getUTCDate()) + 'T' +
                    f(this.getUTCHours()) + ':' +
                    f(this.getUTCMinutes()) + ':' +
                    f(this.getUTCSeconds()) + 'Z'
                : null;
        };

        String.prototype.toJSON =
            Number.prototype.toJSON =
            Boolean.prototype.toJSON = function (key) {
                return this.valueOf();
            };
    }

    var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
        escapable = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
        gap,
        indent,
        meta = { // table of character substitutions
            '\b': '\\b',
            '\t': '\\t',
            '\n': '\\n',
            '\f': '\\f',
            '\r': '\\r',
            '"' : '\\"',
            '\\': '\\\\'
        },
        rep;


    function quote(string) {

// If the string contains no control characters, no quote characters, and no
// backslash characters, then we can safely slap some quotes around it.
// Otherwise we must also replace the offending characters with safe escape
// sequences.

        escapable.lastIndex = 0;
        return escapable.test(string) ? '"' + string.replace(escapable, function (a) {
            var c = meta[a];
            return typeof c === 'string'
                ? c
                : '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
        }) + '"' : '"' + string + '"';
    }


    function str(key, holder) {

// Produce a string from holder[key].

        var i, // The loop counter.
            k, // The member key.
            v, // The member value.
            length,
            mind = gap,
            partial,
            value = holder[key];

// If the value has a toJSON method, call it to obtain a replacement value.

        if (value && typeof value === 'object' &&
                typeof value.toJSON === 'function') {
            value = value.toJSON(key);
        }

// If we were called with a replacer function, then call the replacer to
// obtain a replacement value.

        if (typeof rep === 'function') {
            value = rep.call(holder, key, value);
        }

// What happens next depends on the value's type.

        switch (typeof value) {
        case 'string':
            return quote(value);

        case 'number':

// JSON numbers must be finite. Encode non-finite numbers as null.

            return isFinite(value) ? String(value) : 'null';

        case 'boolean':
        case 'null':

// If the value is a boolean or null, convert it to a string. Note:
// typeof null does not produce 'null'. The case is included here in
// the remote chance that this gets fixed someday.

            return String(value);

// If the type is 'object', we might be dealing with an object or an array or
// null.

        case 'object':

// Due to a specification blunder in ECMAScript, typeof null is 'object',
// so watch out for that case.

            if (!value) {
                return 'null';
            }

// Make an array to hold the partial results of stringifying this object value.

            gap += indent;
            partial = [];

// Is the value an array?

            if (Object.prototype.toString.apply(value) === '[object Array]') {

// The value is an array. Stringify every element. Use null as a placeholder
// for non-JSON values.

                length = value.length;
                for (i = 0; i < length; i += 1) {
                    partial[i] = str(i, value) || 'null';
                }

// Join all of the elements together, separated with commas, and wrap them in
// brackets.

                v = partial.length === 0
                    ? '[]'
                    : gap
                    ? '[\n' + gap + partial.join(',\n' + gap) + '\n' + mind + ']'
                    : '[' + partial.join(',') + ']';
                gap = mind;
                return v;
            }

// If the replacer is an array, use it to select the members to be stringified.

            if (rep && typeof rep === 'object') {
                length = rep.length;
                for (i = 0; i < length; i += 1) {
                    if (typeof rep[i] === 'string') {
                        k = rep[i];
                        v = str(k, value);
                        if (v) {
                            partial.push(quote(k) + (gap ? ': ' : ':') + v);
                        }
                    }
                }
            } else {

// Otherwise, iterate through all of the keys in the object.

                for (k in value) {
                    if (Object.prototype.hasOwnProperty.call(value, k)) {
                        v = str(k, value);
                        if (v) {
                            partial.push(quote(k) + (gap ? ': ' : ':') + v);
                        }
                    }
                }
            }

// Join all of the member texts together, separated with commas,
// and wrap them in braces.

            v = partial.length === 0
                ? '{}'
                : gap
                ? '{\n' + gap + partial.join(',\n' + gap) + '\n' + mind + '}'
                : '{' + partial.join(',') + '}';
            gap = mind;
            return v;
        }
    }

// If the JSON object does not yet have a stringify method, give it one.

    if (typeof JSON.stringify !== 'function') {
        JSON.stringify = function (value, replacer, space) {

// The stringify method takes a value and an optional replacer, and an optional
// space parameter, and returns a JSON text. The replacer can be a function
// that can replace values, or an array of strings that will select the keys.
// A default replacer method can be provided. Use of the space parameter can
// produce text that is more easily readable.

            var i;
            gap = '';
            indent = '';

// If the space parameter is a number, make an indent string containing that
// many spaces.

            if (typeof space === 'number') {
                for (i = 0; i < space; i += 1) {
                    indent += ' ';
                }

// If the space parameter is a string, it will be used as the indent string.

            } else if (typeof space === 'string') {
                indent = space;
            }

// If there is a replacer, it must be a function or an array.
// Otherwise, throw an error.

            rep = replacer;
            if (replacer && typeof replacer !== 'function' &&
                    (typeof replacer !== 'object' ||
                    typeof replacer.length !== 'number')) {
                throw new Error('JSON.stringify');
            }

// Make a fake root object containing our value under the key of ''.
// Return the result of stringifying the value.

            return str('', {'': value});
        };
    }


// If the JSON object does not yet have a parse method, give it one.

    if (typeof JSON.parse !== 'function') {
        JSON.parse = function (text, reviver) {

// The parse method takes a text and an optional reviver function, and returns
// a JavaScript value if the text is a valid JSON text.

            var j;

            function walk(holder, key) {

// The walk method is used to recursively walk the resulting structure so
// that modifications can be made.

                var k, v, value = holder[key];
                if (value && typeof value === 'object') {
                    for (k in value) {
                        if (Object.prototype.hasOwnProperty.call(value, k)) {
                            v = walk(value, k);
                            if (v !== undefined) {
                                value[k] = v;
                            } else {
                                delete value[k];
                            }
                        }
                    }
                }
                return reviver.call(holder, key, value);
            }


// Parsing happens in four stages. In the first stage, we replace certain
// Unicode characters with escape sequences. JavaScript handles many characters
// incorrectly, either silently deleting them, or treating them as line endings.

            text = String(text);
            cx.lastIndex = 0;
            if (cx.test(text)) {
                text = text.replace(cx, function (a) {
                    return '\\u' +
                        ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
                });
            }

// In the second stage, we run the text against regular expressions that look
// for non-JSON patterns. We are especially concerned with '()' and 'new'
// because they can cause invocation, and '=' because it can cause mutation.
// But just to be safe, we want to reject all unexpected forms.

// We split the second stage into 4 regexp operations in order to work around
// crippling inefficiencies in IE's and Safari's regexp engines. First we
// replace the JSON backslash pairs with '@' (a non-JSON character). Second, we
// replace all simple value tokens with ']' characters. Third, we delete all
// open brackets that follow a colon or comma or that begin the text. Finally,
// we look to see that the remaining characters are only whitespace or ']' or
// ',' or ':' or '{' or '}'. If that is so, then the text is safe for eval.

            if (/^[\],:{}\s]*$/
                    .test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@')
                        .replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']')
                        .replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {

// In the third stage we use the eval function to compile the text into a
// JavaScript structure. The '{' operator is subject to a syntactic ambiguity
// in JavaScript: it can begin a block or an object literal. We wrap the text
// in parens to eliminate the ambiguity.

                j = eval('(' + text + ')');

// In the optional fourth stage, we recursively walk the new structure, passing
// each name/value pair to a reviver function for possible transformation.

                return typeof reviver === 'function'
                    ? walk({'': j}, '')
                    : j;
            }

// If the text is not JSON parseable, then a SyntaxError is thrown.

            throw new SyntaxError('JSON.parse');
        };
    }
}());


(function(a){function d(b){var c=b||window.event,d=[].slice.call(arguments,1),e=0,f=!0,g=0,h=0;return b=a.event.fix(c),b.type="mousewheel",c.wheelDelta&&(e=c.wheelDelta/120),c.detail&&(e=-c.detail/3),h=e,c.axis!==undefined&&c.axis===c.HORIZONTAL_AXIS&&(h=0,g=-1*e),c.wheelDeltaY!==undefined&&(h=c.wheelDeltaY/120),c.wheelDeltaX!==undefined&&(g=-1*c.wheelDeltaX/120),d.unshift(b,e,g,h),(a.event.dispatch||a.event.handle).apply(this,d)}var b=["DOMMouseScroll","mousewheel"];if(a.event.fixHooks)for(var c=b.length;c;)a.event.fixHooks[b[--c]]=a.event.mouseHooks;a.event.special.mousewheel={setup:function(){if(this.addEventListener)for(var a=b.length;a;)this.addEventListener(b[--a],d,!1);else this.onmousewheel=d},teardown:function(){if(this.removeEventListener)for(var a=b.length;a;)this.removeEventListener(b[--a],d,!1);else this.onmousewheel=null}},a.fn.extend({mousewheel:function(a){return a?this.bind("mousewheel",a):this.trigger("mousewheel")},unmousewheel:function(a){return this.unbind("mousewheel",a)}})})(jQuery);
/*custom scrollbar*/
(function(c){var b={init:function(e){var f={set_width:false,set_height:false,horizontalScroll:false,scrollInertia:950,mouseWheel:true,mouseWheelPixels:"auto",autoDraggerLength:true,autoHideScrollbar:false,snapAmount:null,snapOffset:0,scrollButtons:{enable:false,scrollType:"continuous",scrollSpeed:"auto",scrollAmount:40},advanced:{updateOnBrowserResize:true,updateOnContentResize:false,autoExpandHorizontalScroll:false,autoScrollOnFocus:true,normalizeMouseWheelDelta:false},contentTouchScroll:true,callbacks:{onScrollStart:function(){},onScroll:function(){},onTotalScroll:function(){},onTotalScrollBack:function(){},onTotalScrollOffset:0,onTotalScrollBackOffset:0,whileScrolling:function(){}},theme:"light"},e=c.extend(true,f,e);return this.each(function(){var m=c(this);if(e.set_width){m.css("width",e.set_width)}if(e.set_height){m.css("height",e.set_height)}if(!c(document).data("mCustomScrollbar-index")){c(document).data("mCustomScrollbar-index","1")}else{var t=parseInt(c(document).data("mCustomScrollbar-index"));c(document).data("mCustomScrollbar-index",t+1)}m.wrapInner("<div class='mCustomScrollBox mCS-"+e.theme+"' id='mCSB_"+c(document).data("mCustomScrollbar-index")+"' style='position:relative; height:100%; overflow:hidden; max-width:100%;' />").addClass("mCustomScrollbar _mCS_"+c(document).data("mCustomScrollbar-index"));var g=m.children(".mCustomScrollBox");if(e.horizontalScroll){g.addClass("mCSB_horizontal").wrapInner("<div class='mCSB_h_wrapper' style='position:relative; left:0; width:999999px;' />");var k=g.children(".mCSB_h_wrapper");k.wrapInner("<div class='mCSB_container' style='position:absolute; left:0;' />").children(".mCSB_container").css({width:k.children().outerWidth(),position:"relative"}).unwrap()}else{g.wrapInner("<div class='mCSB_container' style='position:relative; top:0;' />")}var o=g.children(".mCSB_container");if(c.support.touch){o.addClass("mCS_touch")}o.after("<div class='mCSB_scrollTools' style='position:absolute;'><div class='mCSB_draggerContainer'><div class='mCSB_dragger' style='position:absolute;' oncontextmenu='return false;'><div class='mCSB_dragger_bar' style='position:relative;'></div></div><div class='mCSB_draggerRail'></div></div></div>");var l=g.children(".mCSB_scrollTools"),h=l.children(".mCSB_draggerContainer"),q=h.children(".mCSB_dragger");if(e.horizontalScroll){q.data("minDraggerWidth",q.width())}else{q.data("minDraggerHeight",q.height())}if(e.scrollButtons.enable){if(e.horizontalScroll){l.prepend("<a class='mCSB_buttonLeft' oncontextmenu='return false;'></a>").append("<a class='mCSB_buttonRight' oncontextmenu='return false;'></a>")}else{l.prepend("<a class='mCSB_buttonUp' oncontextmenu='return false;'></a>").append("<a class='mCSB_buttonDown' oncontextmenu='return false;'></a>")}}g.bind("scroll",function(){if(!m.is(".mCS_disabled")){g.scrollTop(0).scrollLeft(0)}});m.data({mCS_Init:true,mCustomScrollbarIndex:c(document).data("mCustomScrollbar-index"),horizontalScroll:e.horizontalScroll,scrollInertia:e.scrollInertia,scrollEasing:"mcsEaseOut",mouseWheel:e.mouseWheel,mouseWheelPixels:e.mouseWheelPixels,autoDraggerLength:e.autoDraggerLength,autoHideScrollbar:e.autoHideScrollbar,snapAmount:e.snapAmount,snapOffset:e.snapOffset,scrollButtons_enable:e.scrollButtons.enable,scrollButtons_scrollType:e.scrollButtons.scrollType,scrollButtons_scrollSpeed:e.scrollButtons.scrollSpeed,scrollButtons_scrollAmount:e.scrollButtons.scrollAmount,autoExpandHorizontalScroll:e.advanced.autoExpandHorizontalScroll,autoScrollOnFocus:e.advanced.autoScrollOnFocus,normalizeMouseWheelDelta:e.advanced.normalizeMouseWheelDelta,contentTouchScroll:e.contentTouchScroll,onScrollStart_Callback:e.callbacks.onScrollStart,onScroll_Callback:e.callbacks.onScroll,onTotalScroll_Callback:e.callbacks.onTotalScroll,onTotalScrollBack_Callback:e.callbacks.onTotalScrollBack,onTotalScroll_Offset:e.callbacks.onTotalScrollOffset,onTotalScrollBack_Offset:e.callbacks.onTotalScrollBackOffset,whileScrolling_Callback:e.callbacks.whileScrolling,bindEvent_scrollbar_drag:false,bindEvent_content_touch:false,bindEvent_scrollbar_click:false,bindEvent_mousewheel:false,bindEvent_buttonsContinuous_y:false,bindEvent_buttonsContinuous_x:false,bindEvent_buttonsPixels_y:false,bindEvent_buttonsPixels_x:false,bindEvent_focusin:false,bindEvent_autoHideScrollbar:false,mCSB_buttonScrollRight:false,mCSB_buttonScrollLeft:false,mCSB_buttonScrollDown:false,mCSB_buttonScrollUp:false});if(e.horizontalScroll){if(m.css("max-width")!=="none"){if(!e.advanced.updateOnContentResize){e.advanced.updateOnContentResize=true}}}else{if(m.css("max-height")!=="none"){var s=false,r=parseInt(m.css("max-height"));if(m.css("max-height").indexOf("%")>=0){s=r,r=m.parent().height()*s/100}m.css("overflow","hidden");g.css("max-height",r)}}m.mCustomScrollbar("update");if(e.advanced.updateOnBrowserResize){var i,j=c(window).width(),u=c(window).height();c(window).bind("resize."+m.data("mCustomScrollbarIndex"),function(){if(i){clearTimeout(i)}i=setTimeout(function(){if(!m.is(".mCS_disabled")&&!m.is(".mCS_destroyed")){var w=c(window).width(),v=c(window).height();if(j!==w||u!==v){if(m.css("max-height")!=="none"&&s){g.css("max-height",m.parent().height()*s/100)}m.mCustomScrollbar("update");j=w;u=v}}},150)})}if(e.advanced.updateOnContentResize){var p;if(e.horizontalScroll){var n=o.outerWidth()}else{var n=o.outerHeight()}p=setInterval(function(){if(e.horizontalScroll){if(e.advanced.autoExpandHorizontalScroll){o.css({position:"absolute",width:"auto"}).wrap("<div class='mCSB_h_wrapper' style='position:relative; left:0; width:999999px;' />").css({width:o.outerWidth(),position:"relative"}).unwrap()}var v=o.outerWidth()}else{var v=o.outerHeight()}if(v!=n){m.mCustomScrollbar("update");n=v}},300)}})},update:function(){var n=c(this),k=n.children(".mCustomScrollBox"),q=k.children(".mCSB_container");q.removeClass("mCS_no_scrollbar");n.removeClass("mCS_disabled mCS_destroyed");k.scrollTop(0).scrollLeft(0);var y=k.children(".mCSB_scrollTools"),o=y.children(".mCSB_draggerContainer"),m=o.children(".mCSB_dragger");if(n.data("horizontalScroll")){var A=y.children(".mCSB_buttonLeft"),t=y.children(".mCSB_buttonRight"),f=k.width();if(n.data("autoExpandHorizontalScroll")){q.css({position:"absolute",width:"auto"}).wrap("<div class='mCSB_h_wrapper' style='position:relative; left:0; width:999999px;' />").css({width:q.outerWidth(),position:"relative"}).unwrap()}var z=q.outerWidth()}else{var w=y.children(".mCSB_buttonUp"),g=y.children(".mCSB_buttonDown"),r=k.height(),i=q.outerHeight()}if(i>r&&!n.data("horizontalScroll")){y.css("display","block");var s=o.height();if(n.data("autoDraggerLength")){var u=Math.round(r/i*s),l=m.data("minDraggerHeight");if(u<=l){m.css({height:l})}else{if(u>=s-10){var p=s-10;m.css({height:p})}else{m.css({height:u})}}m.children(".mCSB_dragger_bar").css({"line-height":m.height()+"px"})}var B=m.height(),x=(i-r)/(s-B);n.data("scrollAmount",x).mCustomScrollbar("scrolling",k,q,o,m,w,g,A,t);var D=Math.abs(q.position().top);n.mCustomScrollbar("scrollTo",D,{scrollInertia:0,trigger:"internal"})}else{if(z>f&&n.data("horizontalScroll")){y.css("display","block");var h=o.width();if(n.data("autoDraggerLength")){var j=Math.round(f/z*h),C=m.data("minDraggerWidth");if(j<=C){m.css({width:C})}else{if(j>=h-10){var e=h-10;m.css({width:e})}else{m.css({width:j})}}}var v=m.width(),x=(z-f)/(h-v);n.data("scrollAmount",x).mCustomScrollbar("scrolling",k,q,o,m,w,g,A,t);var D=Math.abs(q.position().left);n.mCustomScrollbar("scrollTo",D,{scrollInertia:0,trigger:"internal"})}else{k.unbind("mousewheel focusin");if(n.data("horizontalScroll")){m.add(q).css("left",0)}else{m.add(q).css("top",0)}y.css("display","none");q.addClass("mCS_no_scrollbar");n.data({bindEvent_mousewheel:false,bindEvent_focusin:false})}}},scrolling:function(h,p,m,j,w,e,A,v){var k=c(this);if(!k.data("bindEvent_scrollbar_drag")){var n,o;if(c.support.msPointer){j.bind("MSPointerDown",function(H){H.preventDefault();k.data({on_drag:true});j.addClass("mCSB_dragger_onDrag");var G=c(this),J=G.offset(),F=H.originalEvent.pageX-J.left,I=H.originalEvent.pageY-J.top;if(F<G.width()&&F>0&&I<G.height()&&I>0){n=I;o=F}});c(document).bind("MSPointerMove."+k.data("mCustomScrollbarIndex"),function(H){H.preventDefault();if(k.data("on_drag")){var G=j,J=G.offset(),F=H.originalEvent.pageX-J.left,I=H.originalEvent.pageY-J.top;D(n,o,I,F)}}).bind("MSPointerUp."+k.data("mCustomScrollbarIndex"),function(x){k.data({on_drag:false});j.removeClass("mCSB_dragger_onDrag")})}else{j.bind("mousedown touchstart",function(H){H.preventDefault();H.stopImmediatePropagation();var G=c(this),K=G.offset(),F,J;if(H.type==="touchstart"){var I=H.originalEvent.touches[0]||H.originalEvent.changedTouches[0];F=I.pageX-K.left;J=I.pageY-K.top}else{k.data({on_drag:true});j.addClass("mCSB_dragger_onDrag");F=H.pageX-K.left;J=H.pageY-K.top}if(F<G.width()&&F>0&&J<G.height()&&J>0){n=J;o=F}}).bind("touchmove",function(H){H.preventDefault();H.stopImmediatePropagation();var K=H.originalEvent.touches[0]||H.originalEvent.changedTouches[0],G=c(this),J=G.offset(),F=K.pageX-J.left,I=K.pageY-J.top;D(n,o,I,F)});c(document).bind("mousemove."+k.data("mCustomScrollbarIndex"),function(H){if(k.data("on_drag")){var G=j,J=G.offset(),F=H.pageX-J.left,I=H.pageY-J.top;D(n,o,I,F)}}).bind("mouseup."+k.data("mCustomScrollbarIndex"),function(x){k.data({on_drag:false});j.removeClass("mCSB_dragger_onDrag")})}k.data({bindEvent_scrollbar_drag:true})}function D(G,H,I,F){if(k.data("horizontalScroll")){k.mCustomScrollbar("scrollTo",(j.position().left-(H))+F,{moveDragger:true,trigger:"internal"})}else{k.mCustomScrollbar("scrollTo",(j.position().top-(G))+I,{moveDragger:true,trigger:"internal"})}}if(c.support.touch&&k.data("contentTouchScroll")){if(!k.data("bindEvent_content_touch")){var l,B,r,s,u,C,E;p.bind("touchstart",function(x){x.stopImmediatePropagation();l=x.originalEvent.touches[0]||x.originalEvent.changedTouches[0];B=c(this);r=B.offset();u=l.pageX-r.left;s=l.pageY-r.top;C=s;E=u});p.bind("touchmove",function(x){x.preventDefault();x.stopImmediatePropagation();l=x.originalEvent.touches[0]||x.originalEvent.changedTouches[0];B=c(this).parent();r=B.offset();u=l.pageX-r.left;s=l.pageY-r.top;if(k.data("horizontalScroll")){k.mCustomScrollbar("scrollTo",E-u,{trigger:"internal"})}else{k.mCustomScrollbar("scrollTo",C-s,{trigger:"internal"})}})}}if(!k.data("bindEvent_scrollbar_click")){m.bind("click",function(F){var x=(F.pageY-m.offset().top)*k.data("scrollAmount"),y=c(F.target);if(k.data("horizontalScroll")){x=(F.pageX-m.offset().left)*k.data("scrollAmount")}if(y.hasClass("mCSB_draggerContainer")||y.hasClass("mCSB_draggerRail")){k.mCustomScrollbar("scrollTo",x,{trigger:"internal",scrollEasing:"draggerRailEase"})}});k.data({bindEvent_scrollbar_click:true})}if(k.data("mouseWheel")){if(!k.data("bindEvent_mousewheel")){h.bind("mousewheel",function(H,J){var G,F=k.data("mouseWheelPixels"),x=Math.abs(p.position().top),I=j.position().top,y=m.height()-j.height();if(k.data("normalizeMouseWheelDelta")){if(J<0){J=-1}else{J=1}}if(F==="auto"){F=100+Math.round(k.data("scrollAmount")/2)}if(k.data("horizontalScroll")){I=j.position().left;y=m.width()-j.width();x=Math.abs(p.position().left)}if((J>0&&I!==0)||(J<0&&I!==y)){H.preventDefault();H.stopImmediatePropagation()}G=x-(J*F);k.mCustomScrollbar("scrollTo",G,{trigger:"internal"})});k.data({bindEvent_mousewheel:true})}}if(k.data("scrollButtons_enable")){if(k.data("scrollButtons_scrollType")==="pixels"){if(k.data("horizontalScroll")){v.add(A).unbind("mousedown touchstart MSPointerDown mouseup MSPointerUp mouseout MSPointerOut touchend",i,g);k.data({bindEvent_buttonsContinuous_x:false});if(!k.data("bindEvent_buttonsPixels_x")){v.bind("click",function(x){x.preventDefault();q(Math.abs(p.position().left)+k.data("scrollButtons_scrollAmount"))});A.bind("click",function(x){x.preventDefault();q(Math.abs(p.position().left)-k.data("scrollButtons_scrollAmount"))});k.data({bindEvent_buttonsPixels_x:true})}}else{e.add(w).unbind("mousedown touchstart MSPointerDown mouseup MSPointerUp mouseout MSPointerOut touchend",i,g);k.data({bindEvent_buttonsContinuous_y:false});if(!k.data("bindEvent_buttonsPixels_y")){e.bind("click",function(x){x.preventDefault();q(Math.abs(p.position().top)+k.data("scrollButtons_scrollAmount"))});w.bind("click",function(x){x.preventDefault();q(Math.abs(p.position().top)-k.data("scrollButtons_scrollAmount"))});k.data({bindEvent_buttonsPixels_y:true})}}function q(x){if(!j.data("preventAction")){j.data("preventAction",true);k.mCustomScrollbar("scrollTo",x,{trigger:"internal"})}}}else{if(k.data("horizontalScroll")){v.add(A).unbind("click");k.data({bindEvent_buttonsPixels_x:false});if(!k.data("bindEvent_buttonsContinuous_x")){v.bind("mousedown touchstart MSPointerDown",function(y){y.preventDefault();var x=z();k.data({mCSB_buttonScrollRight:setInterval(function(){k.mCustomScrollbar("scrollTo",Math.abs(p.position().left)+x,{trigger:"internal",scrollEasing:"easeOutCirc"})},17)})});var i=function(x){x.preventDefault();clearInterval(k.data("mCSB_buttonScrollRight"))};v.bind("mouseup touchend MSPointerUp mouseout MSPointerOut",i);A.bind("mousedown touchstart MSPointerDown",function(y){y.preventDefault();var x=z();k.data({mCSB_buttonScrollLeft:setInterval(function(){k.mCustomScrollbar("scrollTo",Math.abs(p.position().left)-x,{trigger:"internal",scrollEasing:"easeOutCirc"})},17)})});var g=function(x){x.preventDefault();clearInterval(k.data("mCSB_buttonScrollLeft"))};A.bind("mouseup touchend MSPointerUp mouseout MSPointerOut",g);k.data({bindEvent_buttonsContinuous_x:true})}}else{e.add(w).unbind("click");k.data({bindEvent_buttonsPixels_y:false});if(!k.data("bindEvent_buttonsContinuous_y")){e.bind("mousedown touchstart MSPointerDown",function(y){y.preventDefault();var x=z();k.data({mCSB_buttonScrollDown:setInterval(function(){k.mCustomScrollbar("scrollTo",Math.abs(p.position().top)+x,{trigger:"internal",scrollEasing:"easeOutCirc"})},17)})});var t=function(x){x.preventDefault();clearInterval(k.data("mCSB_buttonScrollDown"))};e.bind("mouseup touchend MSPointerUp mouseout MSPointerOut",t);w.bind("mousedown touchstart MSPointerDown",function(y){y.preventDefault();var x=z();k.data({mCSB_buttonScrollUp:setInterval(function(){k.mCustomScrollbar("scrollTo",Math.abs(p.position().top)-x,{trigger:"internal",scrollEasing:"easeOutCirc"})},17)})});var f=function(x){x.preventDefault();clearInterval(k.data("mCSB_buttonScrollUp"))};w.bind("mouseup touchend MSPointerUp mouseout MSPointerOut",f);k.data({bindEvent_buttonsContinuous_y:true})}}function z(){var x=k.data("scrollButtons_scrollSpeed");if(k.data("scrollButtons_scrollSpeed")==="auto"){x=Math.round((k.data("scrollInertia")+100)/40)}return x}}}if(k.data("autoScrollOnFocus")){if(!k.data("bindEvent_focusin")){h.bind("focusin",function(){h.scrollTop(0).scrollLeft(0);var x=c(document.activeElement);if(x.is("input,textarea,select,button,a[tabindex],area,object")){var G=p.position().top,y=x.position().top,F=h.height()-x.outerHeight();if(k.data("horizontalScroll")){G=p.position().left;y=x.position().left;F=h.width()-x.outerWidth()}if(G+y<0||G+y>F){k.mCustomScrollbar("scrollTo",y,{trigger:"internal"})}}});k.data({bindEvent_focusin:true})}}if(k.data("autoHideScrollbar")){if(!k.data("bindEvent_autoHideScrollbar")){h.bind("mouseenter",function(x){h.addClass("mCS-mouse-over");d.showScrollbar.call(h.children(".mCSB_scrollTools"))}).bind("mouseleave touchend",function(x){h.removeClass("mCS-mouse-over");if(x.type==="mouseleave"){d.hideScrollbar.call(h.children(".mCSB_scrollTools"))}});k.data({bindEvent_autoHideScrollbar:true})}}},scrollTo:function(e,f){var i=c(this),o={moveDragger:false,trigger:"external",callbacks:true,scrollInertia:i.data("scrollInertia"),scrollEasing:i.data("scrollEasing")},f=c.extend(o,f),p,g=i.children(".mCustomScrollBox"),k=g.children(".mCSB_container"),r=g.children(".mCSB_scrollTools"),j=r.children(".mCSB_draggerContainer"),h=j.children(".mCSB_dragger"),t=draggerSpeed=f.scrollInertia,q,s,m,l;if(!k.hasClass("mCS_no_scrollbar")){i.data({mCS_trigger:f.trigger});if(i.data("mCS_Init")){f.callbacks=false}if(e||e===0){if(typeof(e)==="number"){if(f.moveDragger){p=e;if(i.data("horizontalScroll")){e=h.position().left*i.data("scrollAmount")}else{e=h.position().top*i.data("scrollAmount")}draggerSpeed=0}else{p=e/i.data("scrollAmount")}}else{if(typeof(e)==="string"){var v;if(e==="top"){v=0}else{if(e==="bottom"&&!i.data("horizontalScroll")){v=k.outerHeight()-g.height()}else{if(e==="left"){v=0}else{if(e==="right"&&i.data("horizontalScroll")){v=k.outerWidth()-g.width()}else{if(e==="first"){v=i.find(".mCSB_container").find(":first")}else{if(e==="last"){v=i.find(".mCSB_container").find(":last")}else{v=i.find(e)}}}}}}if(v.length===1){if(i.data("horizontalScroll")){e=v.position().left}else{e=v.position().top}p=e/i.data("scrollAmount")}else{p=e=v}}}if(i.data("horizontalScroll")){if(i.data("onTotalScrollBack_Offset")){s=-i.data("onTotalScrollBack_Offset")}if(i.data("onTotalScroll_Offset")){l=g.width()-k.outerWidth()+i.data("onTotalScroll_Offset")}if(p<0){p=e=0;clearInterval(i.data("mCSB_buttonScrollLeft"));if(!s){q=true}}else{if(p>=j.width()-h.width()){p=j.width()-h.width();e=g.width()-k.outerWidth();clearInterval(i.data("mCSB_buttonScrollRight"));if(!l){m=true}}else{e=-e}}var n=i.data("snapAmount");if(n){e=Math.round(e/n)*n-i.data("snapOffset")}d.mTweenAxis.call(this,h[0],"left",Math.round(p),draggerSpeed,f.scrollEasing);d.mTweenAxis.call(this,k[0],"left",Math.round(e),t,f.scrollEasing,{onStart:function(){if(f.callbacks&&!i.data("mCS_tweenRunning")){u("onScrollStart")}if(i.data("autoHideScrollbar")){d.showScrollbar.call(r)}},onUpdate:function(){if(f.callbacks){u("whileScrolling")}},onComplete:function(){if(f.callbacks){u("onScroll");if(q||(s&&k.position().left>=s)){u("onTotalScrollBack")}if(m||(l&&k.position().left<=l)){u("onTotalScroll")}}h.data("preventAction",false);i.data("mCS_tweenRunning",false);if(i.data("autoHideScrollbar")){if(!g.hasClass("mCS-mouse-over")){d.hideScrollbar.call(r)}}}})}else{if(i.data("onTotalScrollBack_Offset")){s=-i.data("onTotalScrollBack_Offset")}if(i.data("onTotalScroll_Offset")){l=g.height()-k.outerHeight()+i.data("onTotalScroll_Offset")}if(p<0){p=e=0;clearInterval(i.data("mCSB_buttonScrollUp"));if(!s){q=true}}else{if(p>=j.height()-h.height()){p=j.height()-h.height();e=g.height()-k.outerHeight();clearInterval(i.data("mCSB_buttonScrollDown"));if(!l){m=true}}else{e=-e}}var n=i.data("snapAmount");if(n){e=Math.round(e/n)*n-i.data("snapOffset")}d.mTweenAxis.call(this,h[0],"top",Math.round(p),draggerSpeed,f.scrollEasing);d.mTweenAxis.call(this,k[0],"top",Math.round(e),t,f.scrollEasing,{onStart:function(){if(f.callbacks&&!i.data("mCS_tweenRunning")){u("onScrollStart")}if(i.data("autoHideScrollbar")){d.showScrollbar.call(r)}},onUpdate:function(){if(f.callbacks){u("whileScrolling")}},onComplete:function(){if(f.callbacks){u("onScroll");if(q||(s&&k.position().top>=s)){u("onTotalScrollBack")}if(m||(l&&k.position().top<=l)){u("onTotalScroll")}}h.data("preventAction",false);i.data("mCS_tweenRunning",false);if(i.data("autoHideScrollbar")){if(!g.hasClass("mCS-mouse-over")){d.hideScrollbar.call(r)}}}})}if(i.data("mCS_Init")){i.data({mCS_Init:false})}}}function u(w){this.mcs={top:k.position().top,left:k.position().left,draggerTop:h.position().top,draggerLeft:h.position().left,topPct:Math.round((100*Math.abs(k.position().top))/Math.abs(k.outerHeight()-g.height())),leftPct:Math.round((100*Math.abs(k.position().left))/Math.abs(k.outerWidth()-g.width()))};switch(w){case"onScrollStart":i.data("mCS_tweenRunning",true).data("onScrollStart_Callback").call(i,this.mcs);break;case"whileScrolling":i.data("whileScrolling_Callback").call(i,this.mcs);break;case"onScroll":i.data("onScroll_Callback").call(i,this.mcs);break;case"onTotalScrollBack":i.data("onTotalScrollBack_Callback").call(i,this.mcs);break;case"onTotalScroll":i.data("onTotalScroll_Callback").call(i,this.mcs);break}}},stop:function(){var g=c(this),e=g.children().children(".mCSB_container"),f=g.children().children().children().children(".mCSB_dragger");d.mTweenAxisStop.call(this,e[0]);d.mTweenAxisStop.call(this,f[0])},disable:function(e){var j=c(this),f=j.children(".mCustomScrollBox"),h=f.children(".mCSB_container"),g=f.children(".mCSB_scrollTools"),i=g.children().children(".mCSB_dragger");f.unbind("mousewheel focusin mouseenter mouseleave touchend");h.unbind("touchstart touchmove");if(e){if(j.data("horizontalScroll")){i.add(h).css("left",0)}else{i.add(h).css("top",0)}}g.css("display","none");h.addClass("mCS_no_scrollbar");j.data({bindEvent_mousewheel:false,bindEvent_focusin:false,bindEvent_content_touch:false,bindEvent_autoHideScrollbar:false}).addClass("mCS_disabled")},destroy:function(){var e=c(this);e.removeClass("mCustomScrollbar _mCS_"+e.data("mCustomScrollbarIndex")).addClass("mCS_destroyed").children().children(".mCSB_container").unwrap().children().unwrap().siblings(".mCSB_scrollTools").remove();c(document).unbind("mousemove."+e.data("mCustomScrollbarIndex")+" mouseup."+e.data("mCustomScrollbarIndex")+" MSPointerMove."+e.data("mCustomScrollbarIndex")+" MSPointerUp."+e.data("mCustomScrollbarIndex"));c(window).unbind("resize."+e.data("mCustomScrollbarIndex"))}},d={showScrollbar:function(){this.stop().animate({opacity:1},"fast")},hideScrollbar:function(){this.stop().animate({opacity:0},"fast")},mTweenAxis:function(g,i,h,f,o,y){var y=y||{},v=y.onStart||function(){},p=y.onUpdate||function(){},w=y.onComplete||function(){};var n=t(),l,j=0,r=g.offsetTop,s=g.style;if(i==="left"){r=g.offsetLeft}var m=h-r;q();e();function t(){if(window.performance&&window.performance.now){return window.performance.now()}else{if(window.performance&&window.performance.webkitNow){return window.performance.webkitNow()}else{if(Date.now){return Date.now()}else{return new Date().getTime()}}}}function x(){if(!j){v.call()}j=t()-n;u();if(j>=g._time){g._time=(j>g._time)?j+l-(j-g._time):j+l-1;if(g._time<j+1){g._time=j+1}}if(g._time<f){g._id=_request(x)}else{w.call()}}function u(){if(f>0){g.currVal=k(g._time,r,m,f,o);s[i]=Math.round(g.currVal)+"px"}else{s[i]=h+"px"}p.call()}function e(){l=1000/60;g._time=j+l;_request=(!window.requestAnimationFrame)?function(z){u();return setTimeout(z,0.01)}:window.requestAnimationFrame;g._id=_request(x)}function q(){if(g._id==null){return}if(!window.requestAnimationFrame){clearTimeout(g._id)}else{window.cancelAnimationFrame(g._id)}g._id=null}function k(B,A,F,E,C){switch(C){case"linear":return F*B/E+A;break;case"easeOutQuad":B/=E;return -F*B*(B-2)+A;break;case"easeInOutQuad":B/=E/2;if(B<1){return F/2*B*B+A}B--;return -F/2*(B*(B-2)-1)+A;break;case"easeOutCubic":B/=E;B--;return F*(B*B*B+1)+A;break;case"easeOutQuart":B/=E;B--;return -F*(B*B*B*B-1)+A;break;case"easeOutQuint":B/=E;B--;return F*(B*B*B*B*B+1)+A;break;case"easeOutCirc":B/=E;B--;return F*Math.sqrt(1-B*B)+A;break;case"easeOutSine":return F*Math.sin(B/E*(Math.PI/2))+A;break;case"easeOutExpo":return F*(-Math.pow(2,-10*B/E)+1)+A;break;case"mcsEaseOut":var D=(B/=E)*B,z=D*B;return A+F*(0.499999999999997*z*D+-2.5*D*D+5.5*z+-6.5*D+4*B);break;case"draggerRailEase":B/=E/2;if(B<1){return F/2*B*B*B+A}B-=2;return F/2*(B*B*B+2)+A;break}}},mTweenAxisStop:function(e){if(e._id==null){return}if(!window.requestAnimationFrame){clearTimeout(e._id)}else{window.cancelAnimationFrame(e._id)}e._id=null},rafPolyfill:function(){var f=["ms","moz","webkit","o"],e=f.length;while(--e>-1&&!window.requestAnimationFrame){window.requestAnimationFrame=window[f[e]+"RequestAnimationFrame"];window.cancelAnimationFrame=window[f[e]+"CancelAnimationFrame"]||window[f[e]+"CancelRequestAnimationFrame"]}}};d.rafPolyfill.call();c.support.touch=!!("ontouchstart" in window);c.support.msPointer=window.navigator.msPointerEnabled;var a=("https:"==document.location.protocol)?"https:":"http:";c.event.special.mousewheel||document.write('<script src="'+a+'//cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.0.6/jquery.mousewheel.min.js"><\/script>');c.fn.mCustomScrollbar=function(e){if(b[e]){return b[e].apply(this,Array.prototype.slice.call(arguments,1))}else{if(typeof e==="object"||!e){return b.init.apply(this,arguments)}else{c.error("Method "+e+" does not exist")}}}})(jQuery);


/* Plug-in Easy List Splitter */
/*
 * 	easyListSplitter 1.0.2 - jQuery Plugin
 *	written by Andrea Cima Serniotti
 *	http://www.madeincima.eu
 *
 *	Copyright (c) 2010 Andrea Cima Serniotti (http://www.madeincima.eu)
 *	Dual licensed under the MIT (MIT-LICENSE.txt)
 *	and GPL (GPL-LICENSE.txt) licenses.
 *
 *	Built for jQuery library
 *	http://jquery.com
 *
 */
/*
	To activate the plugin add the following code to your own js file:
	
	$('.your-list-class-name').easyListSplitter({ 
			colNumber: 3,
			direction: 'horizontal'
	});
	
 */
var j = 1;
(function (jQuery) {
	jQuery.fn.easyListSplitter = function (options) {
		var defaults = {
			colNumber: 2, // Insert here the number of columns you want. Consider that the plugin will create the number of cols requested only if there are enough items in the list.
			direction: 'vertical'
		};
		this.each(function () {
			var obj = jQuery(this);
			var settings = jQuery.extend(defaults, options);
			var totalListElements = jQuery(this).children('li').size();
			var baseColItems = Math.ceil(totalListElements / settings.colNumber);
			var listClass = jQuery(this).attr('class');
			// -------- Create List Elements given colNumber ------------------------------------------------------------------------------
			for (i = 1; i <= settings.colNumber; i++) {
				if (i == 1) {
					jQuery(this).addClass('listCol1').wrap('<div class="listContainer' + j + '"></div>');
				} else if (jQuery(this).is('ul')) { // Check whether the list is ordered or unordered
					jQuery(this).parents('.listContainer' + j).append('<ul class="listCol' + i + '"></ul>');
				} else {
					jQuery(this).parents('.listContainer' + j).append('<ol class="listCol' + i + '"></ol>');
				}
				jQuery('.listContainer' + j + ' > ul,.listContainer' + j + ' > ol').addClass(listClass);
			}
			var listItem = 0;
			var k = 1;
			var l = 0;
			if (settings.direction == 'vertical') { // -------- Append List Elements to the respective listCol  - Vertical -------------------------------
				jQuery(this).children('li').each(function () {
					listItem = listItem + 1;
					if (listItem > baseColItems * (settings.colNumber - 1)) {
						jQuery(this).parents('.listContainer' + j).find('.listCol' + settings.colNumber).append(this);
					} else {
						if (listItem <= (baseColItems * k)) {
							jQuery(this).parents('.listContainer' + j).find('.listCol' + k).append(this);
						} else {
							jQuery(this).parents('.listContainer' + j).find('.listCol' + (k + 1)).append(this);
							k = k + 1;
						}
					}
				});
				jQuery('.listContainer' + j).find('ol,ul').each(function () {
					if (jQuery(this).children().size() == 0) {
						jQuery(this).remove();
					}
				});
			} else { // -------- Append List Elements to the respective listCol  - Horizontal ----------------------------------------------------------
				jQuery(this).children('li').each(function () {
					l = l + 1;
					if (l <= settings.colNumber) {
						jQuery(this).parents('.listContainer' + j).find('.listCol' + l).append(this);
					} else {
						l = 1;
						jQuery(this).parents('.listContainer' + j).find('.listCol' + l).append(this);
					}
				});
			}
			jQuery('.listContainer' + j).find('ol:last,ul:last').addClass('last'); // Set class last on the last UL or OL	
			j = j + 1;
		});
		
	};
})(jQuery);
/* End Plug-in Easy List Splitter */

/* Open current page hash in Hide Show Panel */
function openPageHashInHideShowPanel() {
	var currentHash = window.location.hash;
	if(currentHash != '' && currentHash != '#' ) {
		$(currentHash).parents('.MOD_FO_6a').find('.secondary_subhead').removeClass('open');
		$(currentHash).parents('.MOD_FO_6a').find('.MOD_FO_6panel').hide();
		$('.MOD_FO_6a').find(currentHash).trigger('click');
	}
}
/* Open current page hash in Tabs Component
* It supports Hide Show Pnael as well if it is part of Tabs
 */
function openPageHashInTabsPanel() {
	var currentHash = window.location.hash;
	if(currentHash != '' && currentHash != '#' ) {
		$(currentHash).parents('.tabContent').prev('.tab_heading').trigger('click');
		$(currentHash).parents('.tabContent').find('.MOD_FO_6a .secondary_subhead').removeClass('open');
		$(currentHash).parents('.tabContent').find('.MOD_FO_6a .MOD_FO_6panel').hide();
		$('.tabContent  .MOD_FO_6a').find(currentHash).trigger('click');
		$('.MOD_NO_2_S .tab_heading').click(function() {
			$(currentHash).parents('.tabContent').find('.MOD_FO_6a .secondary_subhead').removeClass('open');
			$(currentHash).parents('.tabContent').find('.MOD_FO_6a .MOD_FO_6panel').hide();
			$('.tabContent  .MOD_FO_6a').find(currentHash).trigger('click');
		});
	}
}
/* 
* Floating panel
*/
function floatingPanel(element) {
	if($(element)[0]) {
		var element = $(element),
		elementPos = element.offset();
		function floatingPanelScroll() {
			if($(this).scrollTop() > (elementPos.top + 10) && element.css('position') == 'absolute') {
				element.css('top','0px').animate({'top':'50px'});
				element.addClass('fixed').css({'left':elementPos.left});
			} else if($(this).scrollTop() <= elementPos.top && element.hasClass('fixed')){
				element.removeClass('fixed');
				element.removeAttr('style');
			}
		};
		floatingPanelScroll();
		$(window).scroll(function() {
			floatingPanelScroll();				
		});
	}
};

function formValidator(formId) {
	$(formId).find('div.error').hide();
	$(formId).find(':input:visible').removeClass('error_input');
	var returnFlag = true;
	$(formId).find('.mandatory_field').each(function() {
		errorDivId = '#' + $(this).attr('id') + 'ErrDiv';
		if($(this).val() == '') {
			$(errorDivId).show();
			$(this).addClass('error_input');
			returnFlag = false;
		} else {
			$(this).removeClass('error_input');
			$(errorDivId).hide();
		}
	});
	$('.error_input').first().focus();
	return returnFlag;
}
function validateEmail(emailFieldId, errDivId){
	var emailAddressVal = $.trim($(emailFieldId).val());
	var reg = /^([_A-Za-z0-9\W])+\@([_A-Za-z0-9_\-\.])+\.([A-Za-z0-9]{1,4})$/;
   if(reg.test(emailAddressVal) == false) {
	  $(emailFieldId).addClass("error_input");
	  $(errDivId).show();
	  return false;
	}
	else{
	  $(emailFieldId).removeClass("error_input");
	  $(errDivId).hide();
	  return true;
	}
}
/*Performance Curve Implementation*/
function validatePerformance(){
	var focusthisField = true;
	var emailValidated = true;
	focusthisField = formValidator($('#performance-curve'));
	if(focusthisField){
		emailValidated = validateEmail('#performance-curve #email','#invalidEmailErr');
	}
	if(!(focusthisField && emailValidated)){
		return false;
	}
	return true;
}
function sumitForm(formobj){
	var validationFlag = validatePerformance();
	if(validationFlag){
		var win = window.open(pdf,"");
		formobj.link.value = pdf;
		formobj.action="https://ufo.deere.com/servlet/UFO?12293=1";
		formobj.method="post";
		window.parent.$("#closeBut").trigger("click");
		
	}else{
		return false;
	}
}
