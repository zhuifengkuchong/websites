function configureSearch(searchBaseUrl, searchProject){
	/* MODIFIED SEARCH.JS */
	
    // If this code is hosted elsewhere (e.g., the Velocity search server),
    // the absolute URL will have to be used for images...
    var resourceBaseUrl = "";
    if (typeof(siteBaseUrl) !== "undefined") {
        resourceBaseUrl = siteBaseUrl;
    }

    var query_autocomplete_dictionary = '_dDDC-AutoComplete';

    var link = document.createElement("a");
    link.href = searchBaseUrl;
    var hostname = link.hostname;
    var protocol = link.protocol;

    var acUrl = "";
    var projectParameter = "";

    if (searchProject.substring(4, 0) == "http") {
        // If using the production URL's...
        acUrl = searchProject;
    }
    else {
        // If using URL's which permit query parameters...
	acUrl = protocol + "//" + hostname + "/cgi-bin/velocity?v.app=autocomplete-jsonp&v.function=autocomplete-collection&bag-of-words=false&num=7&filter=&dictionary=" + query_autocomplete_dictionary;
	projectParameter = "<input value=\"" + searchProject + "\" name=\"v:project\" id=\"input-v:project\" type=\"hidden\" />";
    }

    document.write("<form action=\"" + searchBaseUrl + "\" method=\"GET\" class=\"search\" onsubmit=\"return validateForm();\" id=\"global-search-form\"> " + projectParameter + "<div class=\"yui-ac-container\" id=\"query-autocomplete-container-query\"> <input title='" + Search_Default_Txt + "' name=\"query\" onblur=\"if (this.value=='') this.value='" + Search_Default_Txt + "';\" onfocus=\"if (this.value=='" + Search_Default_Txt + "') this.value='';\" class=\"txt yui-ac-input\" id=\"input-query\" type=\"text\" value=\"" + Search_Default_Txt + "\" /> <div style=\"display: none;\" class=\"yui-ac-content\"><div style=\"display: none;\" class=\"yui-ac-hd\"></div><div class=\"yui-ac-bd\"><ul><li style=\"display: none;\"></li><li style=\"display: none;\"></li><li style=\"display: none;\"></li><li style=\"display: none;\"></li><li style=\"display: none;\"></li><li style=\"display: none;\"></li><li style=\"display: none;\"></li></ul></div><div style=\"display: none;\" class=\"yui-ac-ft\"></div></div></div> <input src=\"" + resourceBaseUrl + "/common/deere-resources/img/btn_search.jpg\" type=\"image\" title=" + Search_ALT_Txt + " alt=" + Search_ALT_Txt+ "> </form>");

    var query_autocomplete_datasource = new YAHOO.widget.DS_ScriptNode(acUrl, ["suggestions", "term", "image", "url", "description"]);
    query_autocomplete_datasource.scriptQueryParam = 'str';
    var query_autocomplete_widget = new YAHOO.widget.AutoComplete(
	'input-query', 
	'query-autocomplete-container-query', 
	query_autocomplete_datasource, 
	{
            forceSelection: false, 
            autoHighlight: false,
            typeAhead: true,
            minQueryLength: 1,
            queryDelay: 0.1,
            maxResultsDisplayed: 7, 
            formatResult: function(oResultItem, sQuery) {
          	var sMarkup = oResultItem[0].toString();
          	return sMarkup;
            }
        }	
    );

    var acSelectHandler = function(sType, aArgs) {
        var myAC = aArgs[0]; // reference back to the AC instance
        var elLI = aArgs[1]; // reference to the selected LI element
        var oData = aArgs[2]; // object literal of selected item's result data

		if (typeof(b_clickTrackSearch)!='undefined' && typeof(mboxClickTrack)!='undefined') {
			mboxFactoryDefault.getSignaler().signal('special','mboxClickTrack','clicked=clk_submit_search');
		}

		document.getElementById("global-search-form").submit();

        if (oData[2]) {
            window.location = oData[2];
        } 
    };
    
    
    query_autocomplete_widget.itemSelectEvent.subscribe(acSelectHandler);

    query_autocomplete_widget.resultTypeList = false;
    
    query_autocomplete_widget.formatResult = function(oResultData, sQuery, sResultMatch) {
        var sQueryArray = sQuery.toString().split(" ");
        var sKey = oResultData[0].toString();
        var sKeyArray = sKey.split(" ");

        var sQueryRegex = "";
        for (var i=0; i < sQueryArray.length; i++) {
            sQueryRegex = sQueryRegex + "^" + sQueryArray[i];
            if(i!=(sQueryArray.length - 1)) {
                sQueryRegex = sQueryRegex + "$|";
            }
        }
        var sqr = new RegExp(sQueryRegex, 'i');
        var aMarkup = "<div class='viv-ac-result'>";
        if(oResultData[1]) {
            aMarkup = aMarkup + "<img src=\"" + oResultData[1] + "\" \/>";
        }
        if(oResultData[2]) {
            aMarkup = aMarkup + "<a href=\"" + oResultData[2] + "\">";
        }
        for(var i=0; i < sKeyArray.length; i++) {
            var m = sKeyArray[i].match(sqr);
            if(m!=null) {
                var matchIndex = sKeyArray[i].indexOf(m[0]);
                var sKeyBefore = sKeyArray[i].substring(0, matchIndex);
                var sKeyMatch = sKeyArray[i].substring(matchIndex, matchIndex + m[0].length);
                var sKeyAfter = sKeyArray[i].substring(matchIndex + m[0].length);
                aMarkup = aMarkup + sKeyBefore + " <span class=\"viv-ac-format-match\">" + sKeyMatch + "<\/span>" + sKeyAfter;
                
            } else {
                aMarkup = aMarkup + " " + sKeyArray[i];
            }
        }
        if(oResultData[2]) {
            aMarkup = aMarkup + "<\/a>";
        }
        if(oResultData[3]) {
            aMarkup = aMarkup + "<div class=\"viv-ac-description\">" + oResultData[3] + "<\/div>";
        }
        aMarkup = aMarkup + "<\/div>";
        return aMarkup;
    };
    
    return { 
	query_autocomplete_datasource: query_autocomplete_datasource, 
	query_autocomplete_widget: query_autocomplete_widget
    } 
}

function onFocusEvent(){
    var sBox = document.getElementById('input-query');
    if(sBox.value== Search_Default_Txt)
        sBox.value='';
}

function onblurEvent(){
    var sBox = document.getElementById('input-query');

    if(sBox.value=='')
        sBox.value = Search_Default_Txt;
}

function validateForm() {
    var query = document.getElementById("input-query").value;

    if ((query.length == 0) || (query == Search_Default_Txt)) {
        alert("Please enter one or more keywords.");
        return false;
    }

    return true;
}


