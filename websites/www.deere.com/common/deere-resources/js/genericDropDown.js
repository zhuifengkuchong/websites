$.fn.verticalAlign = function(alignAs){
	var maxHeight = Math.max.apply(null, this.map(function ()
	{
		return $(this).height();
	}).get());
	$.each(this, function(){
		var _this = $(this),
			_currentDiff = (maxHeight - _this.height());
		if(alignAs== "base-align"){
			if(_this.hasClass("submit_btn")){
				var _siblingWidth = _this.siblings("div.col_1").outerWidth(),
					_searchWidth = 0;
				if(_siblingWidth <= 304 && _siblingWidth > 204){
					_searchWidth = 270;
				}else if(_siblingWidth <=204 && _siblingWidth >146){
					_searchWidth = 180;
				}else{
					_searchWidth = 132;
				}
				if(_this.outerWidth() <= _searchWidth)
					(_currentDiff > 0) ? _this.css("margin-top" , parseInt(_currentDiff+3) +"px") : "";
			}else{
				(_currentDiff > 0) ? _this.css("margin-top" , parseInt(_currentDiff+3) +"px") : "";
			}
		}else{
			(_currentDiff > 0) ? _this.css("margin-top" , _currentDiff+"px") : ""
		}
	});
};

$(function(){

	var ctaDropDownJSON = [],
		ctaForm = $("form.form_cta_link");


	
	function ctaSearchStatus(parentForm){
		var _secondVal = $.trim(parentForm.find(".second_dropdown").val()).split("###")[0],
			   _firstVal = $.trim(parentForm.find(".first_dropdown").val()).split("###")[0],
			   _submitButtonParent = parentForm.find(".submit_btn");
			if(_secondVal == ""  && _firstVal == ""){
				_submitButtonParent.addClass("grey_btn grey_btn_disabled").find("input[type=submit]").attr("disabled", "disabled").removeClass('submit_btnInput');
			}else{
				_submitButtonParent.removeClass("grey_btn grey_btn_disabled").find("input[type=submit]").removeAttr("disabled").addClass('submit_btnInput');
			}
	}
	
		var firstDropDown = ctaForm.find(".first_dropdown"),
			secondDropDown = ctaForm.find(".second_dropdown"),
			submitButton = ctaForm.find(".submit_btnInput"),
			formAction = "";
		ctaForm.each(function(index, element) {
			var _this = $(element),
				_currentDropDownJSON = ctaDropDownJSON[index] = _this.find(".dropdownJson").val();
				if(_currentDropDownJSON != '' || _currentDropDownJSON != undefined){
					_currentDropDownJSON = ctaDropDownJSON[index] = JSON.parse(_currentDropDownJSON);
					var _firstDropdownVal = _secondDropdownVal = '';
					_secondDropdownVal = '<option value="">'+ ctaNoValue +'</option>';
					_this.find(".second_dropdown").html(_secondDropdownVal).attr("disabled", "disabled");
					$.each(_currentDropDownJSON, function(index){
						_firstDropdownVal +='<option value="'+_currentDropDownJSON[index].url_submit+'###'+index+'###'+_currentDropDownJSON[index].first_dropdown+'###'+_currentDropDownJSON[index].target_type+'" >'+_currentDropDownJSON[index].first_dropdown+'</option>';
						});
					_this.find(".first_dropdown").html(_firstDropdownVal);
					window.setTimeout(function(){_this.find(".first_dropdown").trigger("change"); ctaSearchStatus(_this);}, 100);
				}
			(ie7) ? window.setTimeout(function(){_this.find("label").verticalAlign(); _this.find("> div").verticalAlign("base-align");}, 100) : (_this.find("label").verticalAlign(), _this.find("> div").verticalAlign("base-align"));
				
        });
		
		firstDropDown.change(function(e){
		   var _this = $(this),
		   	  firstValue =  _this.val(),
			   _currentIndex = firstValue.split("###")[1],
			   _parentForm = _this.closest("form.form_cta_link"),
			   _currentDropDownValue = ctaDropDownJSON[ctaForm.index(_parentForm)][_currentIndex],
			   _currentValue = _currentDropDownValue.second_dropdown,
			   _secondDropDown = _parentForm.find(".second_dropdown"),
			   _submitButton =  _parentForm.find(".submit_btnInput");
			_parentForm.attr("action", _currentDropDownValue.url_submit),
			_secondDropdownVal = "";
			if(_currentValue != undefined && !$.isEmptyObject(_currentValue)){
				selectSecond = '';
				$.each(_currentValue, function(key, value){
					_secondDropdownVal +='<option value="'+value.target_url+'###'+value.value+'###'+value.target_window+'" >'+value.value+'</option>';
				});
				_secondDropDown.html(_secondDropdownVal).removeAttr("disabled");
			}else{
				_secondDropdownVal = '<option value="">'+ ctaNoValue +'</option>';
				_secondDropDown.html(_secondDropdownVal).attr("disabled", "disabled");
			}
			ctaSearchStatus(_parentForm);
		});
		
		secondDropDown.change(function(e){
			var _this = $(this),
				_parentForm = _this.closest("form.form_cta_link");
				ctaSearchStatus(_parentForm);
		});
		
		ctaForm.delegate(".submit_btnInput", "click", function(e){
			e.preventDefault();
			var _this = $(this),
			   _parentForm = _this.closest("form.form_cta_link"),
			   secondVal = $.trim(_parentForm.find(".second_dropdown").val()).split("###")[0],
			   firstVal = $.trim(_parentForm.find(".first_dropdown").val()).split("###")[0],
			   firstDropdownTarget = $.trim(_parentForm.find(".first_dropdown").val()).split("###")[3],
			   secondDropdownTarget = $.trim(_parentForm.find(".second_dropdown").val()).split("###")[2],
			   submissionTarget = '_blank';
			if(secondVal != "" && secondVal != undefined){
				formAction = secondVal;
				submissionTarget = secondDropdownTarget;
			}else if(firstVal != "" && firstVal != undefined){
				formAction = firstVal;
				submissionTarget = firstDropdownTarget;
			}else{
				formAction = "";
			}
			if(submissionTarget != '_blank' && submissionTarget != '_self'){
				submissionTarget = '_blank';
			}
			_parentForm.attr("action", formAction);
			_parentForm.attr("target",submissionTarget);
			//console.log('First dropdown target : '+firstDropdownTarget+' -- Second dropdown target : '+secondDropdownTarget+' -- Submission target : '+submissionTarget);
			_parentForm.submit();
		});
	


}); // end ready