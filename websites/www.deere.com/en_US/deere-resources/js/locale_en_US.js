/** Variable used in Export to Excel Specifications for Product Model pages **/
exportToExcel = 'Export All Specifications';

/** Add Remove Model used Compare Tab to add/remove models to compare in Product Model Page */
AddRemoveModels = 'Add/Remove Models';

/** Variable used in Compare Tab to show all the models text in Product Model page */
showAllModels = 'Show All Models';

/** Used in button lable text for Expanding the features/specification/compare/ in Product Model page */
ExpandAll = 'Expand All';

/** Used in button lable text for Collapsing the features/specification/compare/ in Product Model page */
CollapseAll = 'Collapse All';

/** Used in Compare Tab when displaying all the John Deere and Competitior Models to compare in Product Model page */
modelsDisplayed ='Models Displayed';

/** Used in search text in search input box */
Search_ALT_Txt = 'Search';

/** Used as default search text in search input box */
Search_Default_Txt = 'Search';

/** Used as Logo image alt text in input box */
Logo_Alt_Txt ="John Deere";

/** Used in Compare Models in Product Model page */
compareModels = 'Compare Models';

/** Used in Compare Tab in Product Model */
johnDeereModels = 'John Deere Models';

/** Used in Compare Tab in Product Model */
competitorModels = "Competitor Models";

/** Used in Compare Tab in Check unCheck Text before checkboxes in Product Model page */
checkUncheck = 'Check/Uncheck All';

/** Used in Compare Tab when Models are displayed in Product Model page */
modelsDisplayed = 'Models Displayed';


ProductExplorer = 'Explore Products';
FlashInstallMessage = 'The latest Flash plugin is not enabled or installed!';

/** Used in the intermediate screen which comes in specification/compare when product has multiple base codes */
seeInformationFor = 'See information for';

/** Used in Compare Tab on export to excel link to export the comparison in Product Model page */
exportToExcelCompare = "Export All Comparisons";

/* Used in Compare Tab in Product Model */
selectModels = "Select Models to Compare to the";


BackToTop = "Back To Top";

/** Choose models to compare in Compare Models to show their specification result in Product Model */
chooseModelsShownOnSpecs = "Choose Models Shown on Specs Chart";

/** Error messag in case of ACM page when no attachment are products */
compatible_error_message = 'Please select attachment categories and products to view compatibility';

/** Alt text of download image */
downloadImage = "Download Image";

/** Export link text on ACM page */
exportToExcelACM = 'Export as xls';

/** Error message in case of ACM page when no compatible attachments present. */
compatible_noAttachment_Models = 'There are no compatible attachments for the products you have selected';

/** Used in drop down in ACM page */
All = 'All';

/** Used in tree view in Matrix of ACM page */
AllCategoriesSeriesAndModels = 'All Categories, Series & Models';

/** Used text above the drop downs in ACM page */
selectedModel = 'Selected Models';

/** Used text in ACM page */
displayed = 'Displayed';

viewProductPage = 'View Product Page';

/** General Error message when the Ajax request in Product/ACM recived exception from server */
generalErrorMessage = 'There was some problem serving your request. Please try again later';

/** Variables used in Calender */
month01 = "January";
month02 = "February";
month03 = "March";
month04 = "April";
month05 = "May";
month06 = "June";
month07 = "July";
month08 = "August";
month09 = "September";
month10 = "October";
month11 = "Nov";
month12 = "Dec";


/** Used text to show Image text  */
langIMAGE = "Image";

/** Variable used in case of translation of "OF" word */
langOF = "of";

/* Variables used in the Image Gallery (Small, Medium & Large) */
langPrev = "Previous";
langNext = "Next";
langNextLine = "Next Line";
langPrevLine = "Previous Line";
langLoading = "Loading...";
langCancel = "Cancel";
langPlay = "Play again";
langPause = "Stop it";
langClose = "Close";

/** Used in ACM page */
closeValue = "close";

/** Used in ACM page in drop down */
suffixCategory= "Category";

/** Used in ACM page in drop down and tree structure */
suffixSubCategory = "Sub Category";

/** Used in ACM page in drop down and tree structure */
suffixSubSeries = "Sub Series";

/** Used in ACM page in drop down and tree structure */
suffixSeries = "Series";

/** Used in ACM page in drop down and tree structure */
model = "Model";

/** Used in intermediate screen on base codes for specification and compare in Product Model */
Go = 'go';

/** Used text in Compatible Equipment in Product Model */
localeEnterZip = "Enter Zip";

/** Used in features of Product Model */
more = 'more';

/** Prefix for  used in ACM page */
prefixAll = 'All';

countryName = "United States";
bazarVoiceapiUrlPrev = //reviews.deere.com/static/1507-en_au/bvapi.js
bazarVoiceapiUrllive = //reviews.deere.com/static/1507-en_au/bvapi.js
reviewSubmissionURlLive = 'http://www.deere.com/wps/dcom/en_US/secured/reviewSubmission.page'
reviewSubmissionURlPrev = 'http://www.tal.deere.com/wps/dcompreview/en_US/secured/reviewSubmission.page'
/** Added the countryName variable for locale specific country name for the header. */
countryName = "United States";
/** Configure different links for different locales */
localeURL = "../../regional_home.page.htm"/*tpa=http://www.deere.com/en_US/regional_home.page*/;


/** Variable used in Export to Excel Specifications for Product Model pages **/
exportToPdf= 'Export to PDF';

/** Configure different link for different MYJD Dashboard Link **/
localeMYJDURL = 'http://myjohndeere.deere.com/wps/myportal/myjd/myjdDashboard';
/** Variable for locale specific error message (Nyromodal) **/
errorText = "<locale> The requested content cannot be loaded.<br />Please try again later.";
/** Variable for locale specific Previous and Next for Nyromodal. **/
nextText = "Next Locale Specific";
prevText = 'Prev Locale Specific';
/** Locale Specific Close Value (ACM and Nyromodal)*/
closeValue = "close";

var pagNext = 'Next';
var pagPrev = 'Prev';
var pagFirst = 'First';
var pagLast = 'Last';
var  velocitySeriesError = 'No Series available for current selection. Please click on Search button to proceed to search results page';
var serverError = "No Products and Series available currently. Please click on Search button to proceed to search results page.";
/** PR varaible added for Dashboard page to show viws sepcific to user */ 
var small = 'Small'; 
var medium = 'Medium'; 
var large = 'Large'; 
var view = 'ICONS :';

/** Locale Specific Close Value (ACM and Nyromodal)*/
closeValue = "close";


/*VN Govt Sales App Global Variable*/
var noContracts = "No Contract Results Available";
var contracts = 'contracts';
var contractResults = 'Contract results';
var forText = 'for';
var moreText = 'For more information visit';
var officialwebSiteText = 'official web site';	
var viewContractsBtnText = 'View all state contracts';
var noErrorText = generalErrorMessage;
var paginationOutOfText = ' out of ';
var paginationResultsText = ' results';


/*Nozzle Locale Start*/
var _inches = 'inches',
	 _meters = 'meters',
	 _mph = 'mph',
	 _gallons_acre = ' gallons/acre',
	 _lbs_gallon = 'lbs/gallon',
	 _gallons_1000_sq_ft = 'gallons/1000 sq ft',
	 _kph = 'kph',
	 _liters_hectare = 'liters/hectare',
	 _kg_liter = 'kg/liter',
	 _feet = 'feet (5 - 22)',
	 _meters_15 = 'meters (1.5 - 7.0)';
/*Nozzle Locale End*/
/* Nozzle Selector translation and other variables Start */

nsInches = 'inches';
nsMeters = 'meters';
nsMph = 'mph';
nsGallonsAcre = 'gallons/acre';
nsLbsGallon = 'lbs/gallon';
nsGallons1000SqFt = 'gallons/1000 sq ft';
nsKph = 'kph';
nsLitersHectare = 'liters/hectare';
nsKgLiter = 'kg/liter';
nsFeet = 'feet (5 - 22)';
nsMeters15 = 'meters (1.5 - 7.0)';
nsResultNumber = 0;
nsResultEachTime = 5;
nsFieldIsRequired = 'This field is required.';
nsFieldValidNum = 'Please enter a valid number.';
nsFieldGreaterThan = 'Please enter a value greater than or equal to 0.01.';
nsResultMessageHeadingSingle = 'NOZZLE';
nsResultMessageHeadingMulti = 'NOZZLES';
nsResultMessageText = 'match your selections';
nsResultErrorMessage = 'An error has occured while processing the query.';
nsResultNoResultMessage = 'No results found for your query.';	
nsRequiredPressure = 'Required Pressure';
nsAtFlowRate = 'At Flow Rate';
nsRequiredSwath = 'Required Swath';
nsPressure = 'Pressure';
nsGroundSpeed = 'Ground Speed';
nsDropletSize = 'Droplet Size';
nsBuyNow = 'buy now';
nsContactADealer = 'contact a dealer';
nsPressureRange = 'Pressure Range';
nsFlowRange = 'Flow Range';
nsTechnology = 'Technology';
nsBodyMaterial = 'Body Material';
nsOrificeMaterial = 'Orifice Material';
nsSprayPattern = 'Spray Pattern';
nsSprayAngle = 'Spray Angle';
nsDriftClassification = 'Drift Classification';
nsLitersPerMin = 'liters/min';
nsGallonsPerMin = 'gallons/min';
nsBackToTop = 'Back To Top';
nsDegrees = 'Degrees&deg;';
var backToResults = 'Back To Product Selector Results';
/* Nozzle Selector translation and other variables End */

/*Start of Print CR string capture*/

printExportOption = "PRINT/EXPORT OPTIONS";
selectImageText = "Select images";
sectionsToPrint = "Sections to Print";
selectModel = "Select Model";
primaryModelImageOnly = "Primary model image only";
allImages = "All images";
noImages = "No images";
printText = 'Print';
saveAsPdf = "SAVE AS A PDF";
exportToWord = 'SAVE AS WORD DOC';
exportToExcel = 'Export to Excel';

/*End of Print CR string capture */

/*Start of export to word/excel/pdf options on print modal*/
bExportToWord = true;
bExportToPdf = true;
bExportToExcel = true;
/*End of export to word/excel/pdf options on print modal*/
/*Start Of EU cookie Translation Strings */
euCookieAlwaysOn = "Always On";
euCookieOn = "On";
euCookieOff = "Off";
/*End Of EU cookie Strings*/
var isEUCookieEnabled = false;

var exportSpecificationToExcel = "Export Specifications to Excel";
var exportComparisonsToExcel = "Export Model Comparisons to Excel";
var headingExportOptions = "Export Options";

/** The following are used in Implements and Attachments page */

selectModelLabel = 'Select Model';
selectCategoryLabel='Select Category';
selectSeriesLabel='Select Series';
selectModelHeadingLabel='Select A Model';
attachmentHeadingGatewayLabel='Attachment/Implement Categories';
		/** The following are used in Implements and Attachments Gateway component. */

selectModelGatewayLabel= 'Select Model';
selectCategoryGatewayLabel='Select Category';
selectSeriesGatewayLabel='Select Series';
goButtonLabel='Show Attachments';
attachmentHeadingLabel ='Attachments and Implements';

acmLocaleVars.pleaseSelect = 'Please Select';
acmLocaleVars.otherSeries = 'Other Series';
acmLocaleVars.otherModels = 'Other Models';
acmLocaleVars.noCategory = 'No Category';
acmLocaleVars.noSeries = 'No Series';
acmLocaleVars.noModel = 'No Model';
acmLocaleVars.noAttachments = 'No Attachment';
acmLocaleVars.selectCategory = 'Select Category';
acmLocaleVars.selectSeries = 'Select Series';
acmLocaleVars.selectModel = 'Select Model';
acmLocaleVars.addModel = 'Add Model';
acmLocaleVars.addModelSubmit = 'ADD';
acmLocaleVars.modelsToCompare = 'MODELS TO COMPARE';
acmLocaleVars.acmEntryPointErr = 'No results are available for the current selection. Please try again.';
acmLocaleVars.noCompatibeAttachment = 'No results are available for the selected model. Please try again.';
acmLocaleVars.viewOnly = 'View only:';
acmLocaleVars.clear = 'Clear';
acmLocaleVars.allAttachments = 'All Attachments';
acmLocaleVars.acmCompareView = 'Compare models';
acmLocaleVars.acmListView = 'List view';
acmLocaleVars.acmHideModels = 'Hide Models';
acmLocaleVars.acmEntryPointPleaseSelectErr = 'Please select a drop down value to proceed.';
acmLocaleVars.serverError = 'Internal server error has occurred. Please try again later.';

acmLocaleVars.acmSelectAModel = 'Select A Model';
acmLocaleVars.addModelErr = 'This product is already selected. Please try a different product.';
acmLocaleVars.acmAttachmentType = 'Attachment Type';
acmLocaleVars.acmErrorNoDcr = 'The selected product does not have the required details. Please try a different selection.';
acmLocaleVars.acmErrorNoAttachmentTypes = 'No attachment/implements to display. Please try a different selection.';
acmLocaleVars.modelsDisplayed = 'Models Displayed';

/*Share functionality  Locale Variables */
var modelLinks = {
                'imageGallery' : 'Image Gallery',
                'degreeView' : '360&deg View',
                'videos' : 'Videos'
}


/*Start Of Two Layered Dropdown Strings */

ctaNoValue="Not Available";

/*End Of Two Layered Dropdown Strings */



/*Special Offer Locale Variables */
var soLocaleVars = soLocaleVars || {};
soLocaleVars.category = {"single" : "Category", "key" : "categoryFilter"};
soLocaleVars.series = {"single" : "Series", "key" : "seriesFilter"};
soLocaleVars.model = {"single" : "Models", "key" : "modelFilter"};
soLocaleVars.publishedWithinLast = {"single" : "Published Within Last", "key" : "date_published"};
soLocaleVars.offerEndsWithin = {"single" : "Offer Ends Within", "key" : "end_date"};
soLocaleVars.offer = {"single" : "Offer", "multi" : "Offers", "key" : "Offer"};
soLocaleVars.clear = "Clear";
soLocaleVars.clearAll = "Clear All Filters";
soLocaleVars.lessText = "Less";
soLocaleVars.moreText = "More";
soLocaleVars.offerDuration = "Offer Duration";
soLocaleVars.error = {};
soLocaleVars.error.noContent = "Currently, there are no special offers for this product line.";
soLocaleVars.error.noFilter = "Error, try again";
soLocaleVars.ee = "Eligible Equipment";
soLocaleVars.replaceData = {"day" : "Day", "days" : "Days", "week" : "Week", "weeks" : "Weeks", "month" : "Month", "months" : "Months", "year" : "Year", "years" : "Years"};
soLocaleVars.industryLocale = {"agriculture" : "Agriculture", "commercial" : "Commercial", "construction" : "Construction", "residential" : "Residential","golfsports": "Golf & Sports", "forestry": "Forestry", "enginesdrivetrain": "Engines & Drivetrain"," golf":"Golf"};
soLocaleVars.error.seriesFilter = "No Series present for this Category.";
var viewAllText = "View All";
var modelTitleSpecialOffer = {
	'specialOfferText' : 'SPECIAL OFFERS',
	'viewDetailsUrl' : 'View Details',
	'specialOfferTitlehtml' : '',
	'offerDurationText' : 'Offer Duration'
}
