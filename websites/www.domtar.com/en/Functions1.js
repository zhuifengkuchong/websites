/* LEVEL 4 *****************************************************************************************/ 
function toggle(id){
    ul = "ul_" + id;
    ulElement = document.getElementById(ul);
    if (ulElement){
            if (ulElement.className == 'closed')
                {
                    menuchilds = document.getElementById("MainLeftMenu").childNodes
                    for (var i=0;i<menuchilds.length;i++) {
                        if(menuchilds[i].nodeType==1) 
                        {
                            strUl = "ul_" + menuchilds[i].id;
                            if (document.getElementById(strUl)) 
                            {
                                UlMenu = document.getElementById(strUl)
                                if (UlMenu.className == 'open') {
                                    UlMenu.className='closed';
                                }
                            }
                        }
                    }
                    ulElement.className = "open";
                }
                else
                {
                    ulElement.className = "closed";
                }
            }
    }
 
 
/* SHOW HIDE STEP *****************************************************************************************/

function toggleDisplay(subobj){
el= document.getElementById(subobj);
mygif = document.getElementById('img'+subobj);
txt= document.getElementById('txt'+subobj);
 if(el.style.display=="block")
  {
  el.style.display="none";
  mygif.src="../CSS/btn_open.gif"/*tpa=http://www.domtar.com/CSS/btn_open.gif*/;
  txt.innerHTML = 'Learn More';
  
  }
else
  {
  el.style.display="block";
  mygif.src="../CSS/btn_close.gif"/*tpa=http://www.domtar.com/CSS/btn_close.gif*/;
  txt.innerHTML = 'Close';
  }
 }

/* SHOW HIDE CONTACT *****************************************************************************************/

function show(id)
{
 el = document.getElementById(id);
 if (el.style.display == 'none')
 {
  el.style.display = '';
  el = document.getElementById('more' + id);
  el.innerHTML = 'Hide Contacts';
 } else {
  el.style.display = 'none';
  el = document.getElementById('more' + id);
  el.innerHTML = 'Show Contacts';
 }
}

/* MAIN MENU *****************************************************************************************/

var arVersion = navigator.appVersion.split("MSIE")
var version = parseFloat(arVersion[1])
window.loadListeners = new Array();
window.addLoadListener = function(f) {
  window.loadListeners[window.loadListeners.length]=f;
}
function initPage() {
  for(var n=0;n<window.loadListeners.length;n++){
    if (typeof(window.loadListeners[n]) == 'function') {
      window.loadListeners[n]();
    } 
  }
}
if ( typeof(document.getElementById)!='undefined' ) {
  window.onload = initPage;
}
var menubar = {
    timer: 0,
    timerForSecondMenu: 0,
    menuchilds: [],
    allow_close: 0,
    active_item: 0,
    showmenu: function(element) {
        this.allow_close=1
        this.hidemenu(1)
        this.allow_close=0
       
        if(this.timerForSecondMenu) clearTimeout(this.timerForSecondMenu)
        if(element.childNodes[1] && element.childNodes[1].nodeType==1) {
            element.childNodes[1].style.display='block'
            this.active_item=element.childNodes[1]
        }
    },
    resetToDefault: function() {        
        var Selected = false;
        this.menuchilds = document.getElementById("menubar").childNodes
        for (var i=0;i<this.menuchilds.length;i++) {
            if(this.menuchilds[i].nodeType==1) {
                if (this.menuchilds[i].className.substr(0,8) == 'selected') {
                    this.menuchilds[i].onmouseover();
                    Selected=true;
                }
            }
        }
        
        if (!Selected) 
        {
            document.getElementById("defaultmnu").childNodes[1].style.display='block';
            if (this.active_item.id != "neverclose") {
                this.active_item.style.display='none';
            }
        }
    },
    hidemenu: function(now) {
        if (now && this.allow_close && this.active_item) {
            this.active_item.style.display='none'
        }
        if (!now && !this.allow_close) {
            this.allow_close=1
            if(this.timer) clearTimeout(this.timer)
            this.timer = setTimeout("menubar.hidemenu(1)",1000)
            this.timerForSecondMenu = setTimeout("menubar.resetToDefault()",1000)
            //this.init();
            //document.getElementById("defaultmnu").childNodes[1].style.display='block'
        }
    },

    init: function() {
        // put mouse event listener on every <li> in the menu
            var Selected = false;
            if(document.getElementById("menubar"))
            {
                this.menuchilds = document.getElementById("menubar").childNodes
                for (var i=0;i<this.menuchilds.length;i++) {
                    if(this.menuchilds[i].nodeType==1) {
                        this.menuchilds[i].onmouseover=function () { menubar.showmenu(this) }
                        this.menuchilds[i].onmouseout=function () { menubar.hidemenu(0) }
                        this.menuchilds[i].onfocus=function () { menubar.showmenu(this) }
                        this.menuchilds[i].onblur=function () { menubar.hidemenu(0) }
                        if (this.menuchilds[i].className.substr(0,8) == 'selected') {
                            this.menuchilds[i].onmouseover();
                            Selected=true;
                        }
                    }
                }
                if (!Selected) {document.getElementById("defaultmnu").childNodes[1].style.display='block';} 
             }
   }
}
// add initmenu to the list of functions to be called at the end of the page loading.
function initmenubar() {
 if (document.getElementsByTagName('body')[0]!=null) {
     menubar.init();
 }
}
      
addLoadListener(initmenubar);


// NEEDED FOR IMAGE PRELOAD
var menuHoverImgSrc = new Array();
menuHoverImgSrc['menu1'] = new Image();
menuHoverImgSrc['menu1'].src = '../CSS/bg_paperSelected.gif'/*tpa=http://www.domtar.com/CSS/bg_paperSelected.gif*/;

menuHoverImgSrc['menu2']= new Image();
menuHoverImgSrc['menu2'].src = '../CSS/bg_pulpSelected.gif'/*tpa=http://www.domtar.com/CSS/bg_pulpSelected.gif*/;

menuHoverImgSrc['menu3'] = new Image();
menuHoverImgSrc['menu3'].src ='../CSS/BG-Care-selected.gif'/*tpa=http://www.domtar.com/CSS/BG-Care-selected.gif*/;

menuHoverImgSrc['menu4'] = new Image();
menuHoverImgSrc['menu4'].src ='../CSS/bg_substainmabilitySelected.gif'/*tpa=http://www.domtar.com/CSS/bg_substainmabilitySelected.gif*/;

menuHoverImgSrc['menu5'] = new Image();
menuHoverImgSrc['menu5'].src='../CSS/bg_investorsSelected.gif'/*tpa=http://www.domtar.com/CSS/bg_investorsSelected.gif*/;

menuHoverImgSrc['menu6'] = new Image();
menuHoverImgSrc['menu6'].src='../CSS/bg_corporateSelected.gif'/*tpa=http://www.domtar.com/CSS/bg_corporateSelected.gif*/;

menuHoverImgSrc['menu7'] = new Image();
menuHoverImgSrc['menu7'].src='../CSS/bg_mediaSelected.gif'/*tpa=http://www.domtar.com/CSS/bg_mediaSelected.gif*/;

var subMenuHoverImgSrc = new Array();
subMenuHoverImgSrc['menu1'] = new Image();
subMenuHoverImgSrc['menu1'].src = '<img src="/CSS/bg_paperSubMenu_over.gif">';

subMenuHoverImgSrc['menu2'] = new Image();
subMenuHoverImgSrc['menu2'].src = '<img src="/CSS/bg_pulpSubMenu_over.gif">';

subMenuHoverImgSrc['menu3'] = new Image();
subMenuHoverImgSrc['menu3'].src = '<img src="/CSS/BG-CareSubMenu-hover.gif">';

subMenuHoverImgSrc['menu4'] = new Image();
subMenuHoverImgSrc['menu4'].src = '<img src="/CSS/bg_substainSubMenu_over.gif">';

subMenuHoverImgSrc['menu5'] = new Image();
subMenuHoverImgSrc['menu5'].src = '<img src="/CSS/bg_investorsSubMenu_over.gif">';

subMenuHoverImgSrc['menu6'] = new Image();
subMenuHoverImgSrc['menu6'].src = '<img src="/CSS/bg_corporateSubMenu_over.gif">';

subMenuHoverImgSrc['menu7'] = new Image();
subMenuHoverImgSrc['menu7'].src = '<img src="/CSS/bg_mediaSubMenu_over.gif">';


var ie4=document.all
var ns6=document.getElementById&&!document.all
var disappeardelay=250  //menu disappear speed onMouseout (in miliseconds)
if (ie4) {
    var verticaloffset=4;
}
else {
    var verticaloffset=7;
}
var horizontaloffset=0 //horizontal offset of menu from default location. (0-5 is a good value)
/////No further editting needed

if (ie4||ns6)
document.write('<div id="dropmenudiv" style="visibility:hidden;width: 160px" onMouseover="clearhidemenu()" onMouseout="dynamichide(event)"></div>')
function getposOffset(what, offsettype){
var totaloffset=(offsettype=="left")? what.offsetLeft : what.offsetTop;
var parentEl=what.offsetParent;
while (parentEl!=null){
    totaloffset=(offsettype=="left")? totaloffset+parentEl.offsetLeft : totaloffset+parentEl.offsetTop;
    parentEl=parentEl.offsetParent;
    }
return totaloffset;
}

function showhide(obj, e, visible, hidden, menuwidth){
if (ie4||ns6)
//dropmenuobj.style.left=dropmenuobj.style.top=-500
dropmenuobj.widthobj=dropmenuobj.style
dropmenuobj.widthobj.width=menuwidth
if (e.type=="click" && obj.visibility==hidden || e.type=="mouseover")
obj.visibility=visible
else if (e.type=="click")
obj.visibility=hidden
}
function iecompattest(){
return (document.compatMode && document.compatMode!="BackCompat")? document.documentElement : document.body
}
function clearbrowseredge(obj, whichedge){
    var edgeoffset=0
    if (whichedge=="rightedge"){
        var windowedge=ie4 && !window.opera? iecompattest().scrollLeft+iecompattest().clientWidth-15 : window.pageXOffset+window.innerWidth-15
        dropmenuobj.contentmeasure=dropmenuobj.offsetWidth
        if (windowedge-dropmenuobj.x-obj.offsetWidth < dropmenuobj.contentmeasure)
        edgeoffset=dropmenuobj.contentmeasure+obj.offsetWidth
    }
    else{
        var topedge=ie4 && !window.opera? iecompattest().scrollTop : window.pageYOffset
        var windowedge=ie4 && !window.opera? iecompattest().scrollTop+iecompattest().clientHeight-15 : window.pageYOffset+window.innerHeight-18
        dropmenuobj.contentmeasure=dropmenuobj.offsetHeight
        if (windowedge-dropmenuobj.y < dropmenuobj.contentmeasure){ //move menu up?
            edgeoffset=dropmenuobj.contentmeasure-obj.offsetHeight
            if ((dropmenuobj.y-topedge)<dropmenuobj.contentmeasure) //up no good either? (position at top of viewable window then)
            edgeoffset=dropmenuobj.y
        }
    }
    return edgeoffset
}
function populatemenu(what){
if (ie4||ns6)
dropmenuobj.innerHTML=what;
}

function dropdownmenu(obj, e, menucontentsname, menuwidth, menuColor,hiddenDefault){
    if (window.event) event.cancelBubble=true
    else if (e.stopPropagation) e.stopPropagation()
    clearhidemenu()
    dropmenuobj=document.getElementById? document.getElementById("dropmenudiv") : dropmenudiv
    menucontentsobj=document.getElementById(menucontentsname);
    populatemenu(menucontentsobj.innerHTML)
    if (!hiddenDefault) {
        if (ie4||ns6){
            showhide(dropmenuobj.style, e, "visible", "hidden", menuwidth)
            dropmenuobj.x=getposOffset(obj, "left")
            dropmenuobj.y=getposOffset(obj, "top")
            //dropmenuobj.style.left=dropmenuobj.x-clearbrowseredge(obj, "rightedge")+obj.offsetWidth+horizontaloffset+"px"
            dropmenuobj.style.left=dropmenuobj.x-clearbrowseredge(obj, "rightedge")+"px"
            //dropmenuobj.style.top=dropmenuobj.y-clearbrowseredge(obj, "bottomedge")+"px"
            dropmenuobj.style.top=dropmenuobj.y-clearbrowseredge(obj, "bottomedge")+obj.offsetHeight+verticaloffset+"px"
            //alert (dropmenuobj.style.bgColor);
            dropmenuobj.style.backgroundColor = menuColor;
            dropmenuobj.style.border = '1px solid rgb(0,0,0)';
        }
    }
    else (hiddenDefault)
    {
        dropmenuobj.style.border = 'none';
    }
    return clickreturnvalue()
}
function clickreturnvalue(){
if (ie4||ns6) return false
else return true
}
function contains_ns6(a, b) {
while (b.parentNode)
if ((b = b.parentNode) == a)
return true;
return false;
}
function dynamichide(e){
if (ie4&&!dropmenuobj.contains(e.toElement))
delayhidemenu()
else if (ns6&&e.currentTarget!= e.relatedTarget&& !contains_ns6(e.currentTarget, e.relatedTarget))
delayhidemenu()
}
function hidemenu(e){
if (typeof dropmenuobj!="undefined"){
        if (ie4||ns6)
        dropmenuobj.style.visibility="hidden"
    }
}
function delayhidemenu(){
    if (ie4||ns6) {
        delayhide=setTimeout("hidemenu()",disappeardelay)
        menubar.timerForSecondMenu = setTimeout("menubar.resetToDefault()",100)
        
    }
}
function clearhidemenu(){
    if (typeof delayhide!="undefined"){
        clearTimeout(delayhide)
    }
    if (menubar.timer!=0) {
        clearTimeout(menubar.timerForSecondMenu);
        clearTimeout(menubar.timer);
    }
}

function AC_AddExtension(src, ext)
{
  if (src.indexOf('?') != -1)
    return src.replace(/\?/, ext+'?'); 
  else
    return src + ext;
}
function AC_Generateobj(objAttrs, params, embedAttrs) 
{ 
  var str = '<object ';
  for (var i in objAttrs)
    str += i + '="' + objAttrs[i] + '" ';
  str += '>';
  for (var i in params)
    str += '<param name="' + i + '" value="' + params[i] + '" /> ';
  str += '<embed ';
  for (var i in embedAttrs)
    str += i + '="' + embedAttrs[i] + '" ';
  str += ' ></embed></object>';
  document.write(str);
}
function AC_FL_RunContent(){
  var ret = 
    AC_GetArgs
    (  arguments, "", "movie", "clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"
     , "application/x-shockwave-flash"
    );
  AC_Generateobj(ret.objAttrs, ret.params, ret.embedAttrs);
}
function AC_SW_RunContent(){
  var ret = 
    AC_GetArgs
    (  arguments, ".dcr", "src", "clsid:166B1BCA-3F9C-11CF-8075-444553540000"
     , null
    );
  AC_Generateobj(ret.objAttrs, ret.params, ret.embedAttrs);
}
function AC_GetArgs(args, ext, srcParamName, classid, mimeType){
  var ret = new Object();
  ret.embedAttrs = new Object();
  ret.params = new Object();
  ret.objAttrs = new Object();
  for (var i=0; i < args.length; i=i+2){
    var currArg = args[i].toLowerCase();    
    switch (currArg){ 
      case "classid":
        break;
      case "pluginspage":
        ret.embedAttrs[args[i]] = args[i+1];
        break;
      case "src":
      case "movie": 
        args[i+1] = AC_AddExtension(args[i+1], ext);
        ret.embedAttrs["src"] = args[i+1];
        ret.params[srcParamName] = args[i+1];
        break;
      case "onafterupdate":
      case "onbeforeupdate":
      case "onblur":
      case "oncellchange":
      case "onclick":
      case "ondblClick":
      case "ondrag":
      case "ondragend":
      case "ondragenter":
      case "ondragleave":
      case "ondragover":
      case "ondrop":
      case "onfinish":
      case "onfocus":
      case "onhelp":
      case "onmousedown":
      case "onmouseup":
      case "onmouseover":
      case "onmousemove":
      case "onmouseout":
      case "onkeypress":
      case "onkeydown":
      case "onkeyup":
      case "onload":
      case "onlosecapture":
      case "onpropertychange":
      case "onreadystatechange":
      case "onrowsdelete":
      case "onrowenter":
      case "onrowexit":
      case "onrowsinserted":
      case "onstart":
      case "onscroll":
      case "onbeforeeditfocus":
      case "onactivate":
      case "onbeforedeactivate":
      case "ondeactivate":
      case "type":
      case "codebase":
        ret.objAttrs[args[i]] = args[i+1];
        break;
      case "width":
      case "height":
      case "align":
      case "vspace": 
      case "hspace":
      case "class":
      case "title":
      case "accesskey":
      case "name":
      case "id":
      case "tabindex":
        ret.embedAttrs[args[i]] = ret.objAttrs[args[i]] = args[i+1];
        break;
      default:
        ret.embedAttrs[args[i]] = ret.params[args[i]] = args[i+1];
    }
  }
  ret.objAttrs["classid"] = classid;
  if (mimeType) ret.embedAttrs["type"] = mimeType;
  return ret;
}

///////homepage image randomiser///////
var theImages = new Array() 
theImages[0] = '../CSS/home1.jpg'/*tpa=http://www.domtar.com/CSS/home1.jpg*/
theImages[1] = '../CSS/home2.jpg'/*tpa=http://www.domtar.com/CSS/home2.jpg*/
theImages[2] = '../CSS/home3.jpg'/*tpa=http://www.domtar.com/CSS/home3.jpg*/
theImages[3] = '../CSS/home4.jpg'/*tpa=http://www.domtar.com/CSS/home4.jpg*/

var j = 0
var p = theImages.length;
var preBuffer = new Array()
for (i = 0; i < p; i++){
   preBuffer[i] = new Image()
   preBuffer[i].src = theImages[i]
}
var whichImage = Math.round(Math.random()*(p-1));
function showImage(){
    document.write('<img src="'+theImages[whichImage]+'">');
}
/*ADDED BY FREDERICK, CESART*/
/*END*/


/*Web Services functions*/

var n_count = 1;
var m_count = 2;
function check(actuel, tot) {
  
    var ID;
    if (actuel == 'plus2' && n_count != tot) {        
        ID = 'nom' + n_count;
        document.getElementById(ID).style.display = 'none';
        n_count = n_count + 1;
        ID = 'nom' + n_count;
        document.getElementById(ID).style.display = 'block';
    }
    if (actuel == 'minus2' && n_count != 1) {
        
        ID = 'nom' + n_count;
        document.getElementById(ID).style.display = 'none';
        n_count = n_count - 1;
        ID = 'nom' + n_count;
        document.getElementById(ID).style.display = 'block';        
    }
    
    if (n_count == 1){
        document.getElementById("prev").className = 'ButtonNextDisable';
    }
    else {
        document.getElementById("prev").className = 'ButtonNext';
    }
    if (n_count == tot){
        document.getElementById("next").className = 'ButtonPreviousDisable';
    }
    else {
        document.getElementById("next").className = 'ButtonPrevious';
    }
}

//nouvlle version check pour press release 8 nov 2008 
function checkHome(actuel, tot) {
  
    var ID;
    
    if (actuel == 'plus2' && n_count + 2 < tot) {  
      for(y=0;y<=2;y++)
      {      
        ID = 'nom' + (n_count + y);
        if(document.getElementById(ID))
              document.getElementById(ID).style.display = 'none';
         //alert('hide:' + ID )
      }

      for(y=1;y<=3;y++)
      {      
        
        
        ID = 'nom' + (n_count + 3);
        if(document.getElementById(ID))
            document.getElementById(ID).style.display = 'block';
        n_count = n_count + 1;
      }
    }
    if (actuel == 'minus2' && n_count > 1) {
      
      for(y=0;y<=2;y++)
      {      
        ID = 'nom' + (n_count + y);
        if(document.getElementById(ID))
            document.getElementById(ID).style.display = 'none';
         //alert('hide:' + ID )
      }

      for(y=3;y>=1;y--)
      {        
        
        
        ID = 'nom' + (n_count-1);
        //alert('show:' + ID )
      if(document.getElementById(ID))
            document.getElementById(ID).style.display = 'block';   
        n_count = n_count - 1; 
      }    
    }
    
    if (n_count <= 1){
        document.getElementById("prev").className = 'ButtonNextDisable';
    }
    else {
        document.getElementById("prev").className = 'ButtonNext';
    }
    if (n_count + 2 >= tot){
        document.getElementById("next").className = 'ButtonPreviousDisable';
    }
    else {
        document.getElementById("next").className = 'ButtonPrevious';
    }
}

function check2(actuel, tot) {
    var ID;
    var paire = true;
    
    if (tot % 2 == 1) {
        paire = false;
    }
    if (actuel == 'plus' && testEnd(m_count, tot) != true) {
        if (paire == true){
        //cas pair
            ID = 'num' + m_count;
            document.getElementById(ID).style.display = 'none';
            m_count = m_count - 1;
            ID = 'num' + m_count;
            document.getElementById(ID).style.display = 'none';
            
            m_count = m_count + 2;
            ID = 'num' + m_count;
            document.getElementById(ID).style.display = 'block';
            m_count = m_count + 1;
            ID = 'num' + m_count;
            document.getElementById(ID).style.display = 'block';
        }
        //cas impair
        else { if (m_count == tot-1 )
                //cas fin de la liste
                {                
                ID = 'num' + m_count;
                document.getElementById(ID).style.display = 'none';
                m_count = m_count - 1;
                ID = 'num' + m_count;
                document.getElementById(ID).style.display = 'none';
                
                m_count = m_count + 2;
                ID = 'num' + m_count;
                document.getElementById(ID).style.display = 'block';    
                m_count = m_count + 1;
                }
                else{
                    //cas pas fin de liste
                    ID = 'num' + m_count;
                    document.getElementById(ID).style.display = 'none';
                    m_count = m_count - 1;
                    ID = 'num' + m_count;
                    document.getElementById(ID).style.display = 'none';
                    
                    m_count = m_count + 2;
                    ID = 'num' + m_count;
                    document.getElementById(ID).style.display = 'block';
                    m_count = m_count + 1;
                    ID = 'num' + m_count;
                    document.getElementById(ID).style.display = 'block';
                }
        }
    }
    
    if (actuel == 'minus' && m_count != 2) {
        if (testEnd(m_count, tot) == false){
            ID = 'num' + m_count;
            document.getElementById(ID).style.display = 'none';
            m_count = m_count - 1;
            ID = 'num' + m_count;
            document.getElementById(ID).style.display = 'none';
            
            m_count = m_count - 2;
            ID = 'num' + m_count;
            document.getElementById(ID).style.display = 'block';
            m_count = m_count + 1;
            ID = 'num' + m_count;
            document.getElementById(ID).style.display = 'block';
        }
        else {
            if (paire == false){
                m_count = m_count - 1;
                ID = 'num' + m_count;
                document.getElementById(ID).style.display = 'none';
                }
            else{
                ID = 'num' + m_count;
                document.getElementById(ID).style.display = 'none';
                m_count = m_count - 1;
                ID = 'num' + m_count;
                document.getElementById(ID).style.display = 'none';
                }        
        m_count = m_count - 2;
        ID = 'num' + m_count;
        document.getElementById(ID).style.display = 'block';
        m_count = m_count + 1;
        ID = 'num' + m_count;
        document.getElementById(ID).style.display = 'block';
        }
    }
    
    if (m_count == 2){
        document.getElementById("prevlink").className = 'ButtonNextDisable';
    }
    else {
        document.getElementById("prevlink").className = 'ButtonNext';
    }
    if (testEnd(m_count, tot) == true){
        document.getElementById("nextlink").className = 'ButtonPreviousDisable';
    }
    else {
        document.getElementById("nextlink").className = 'ButtonPrevious';
    }
    
}
function testEnd(count, total) {
var rep;
var count;
var total;
var test = parseInt(total) + 1;
if ((count == total) || (count == test)){
    rep = true;
}
else {rep = false;}
return rep;
}

function load(total)
{
var total;
          if (total > 2){
            for (i=3; i<=total; i++)
            {
                ID = "num"  + i;
                document.getElementById(ID).style.display = 'none';
            }
            document.getElementById('num1').style.display = 'block';
            document.getElementById('num2').style.display = 'block';
        }
        else {
            document.getElementById("prevlink").style.visibility = 'hidden';
        }
}
function refresh()
{
document.location.reload(true);
}


currentImage = null;
function close_image(id) {
cache = "cache" + id;
visible = "visible" + id;
    if(id!=null) {
       document.getElementById(cache).style.display = "none";
       document.getElementById(visible).style.display = "block";
       arrow = "arrow" + id;
       document.getElementById(arrow).src = "../CSS/img_arrowRight.gif"/*tpa=http://www.domtar.com/CSS/img_arrowRight.gif*/;
    }
}
function collapse_image(id) {
cache = "cache" + id;
visible = "visible" + id;
    if ( id==currentImage ) 
    {
        close_image(id);
        currentImage=null;
        if (id == 0) 
        {
        document.getElementById(cache).style.display = "block"; 
        document.getElementById(visible).style.display = "none";
        } 
    }else {
                document.getElementById(cache).style.display = "block";
                document.getElementById(visible).style.display = "none";
                arrow = "arrow" + id;
                document.getElementById(arrow).src = "../CSS/img_arrowDown.gif"/*tpa=http://www.domtar.com/CSS/img_arrowDown.gif*/;
                currentImage = id;
                }
}
 


function reSizeIframe(elemName, ManualAddHeight)
{
    try
    {    
        
        
      var oFrame    =    document.getElementById(elemName);
      
    var doc = null;  
       if(oFrame.contentDocument)  
    {
          // Firefox, Opera  
          doc = oFrame.contentDocument;  
    }
       else if(oFrame.contentWindow)  
    {
          // Internet Explorer  
          doc = oFrame.contentWindow.document;  
    }
       else if(oFrame.document)  
    {
          // Others?  
          doc = oFrame.document;  
    }
  
       if(doc != null)  
      { 
        var oBody    =    doc.body;
      finalH = oBody.scrollHeight + (oBody.offsetHeight - oBody.clientHeight) + ManualAddHeight;
      
        oFrame.style.height = finalH;
        //we don't need to resize the width
        //oFrame.style.width = oBody.scrollWidth + (oBody.offsetWidth - oBody.clientWidth);
     }
    }
    catch(e)
    {
    window.status =    'Error: ' + e.number + '; ' + e.description;
    }
}