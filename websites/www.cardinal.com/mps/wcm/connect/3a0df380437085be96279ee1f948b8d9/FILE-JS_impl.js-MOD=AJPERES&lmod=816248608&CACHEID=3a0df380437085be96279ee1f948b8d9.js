// this line is to assign the background image with the little top arrow to the incomplete at the bottom of the top section of the flyouts.
// should not be unnecessary (though harmless) when all the links are filled out
jQuery(document).ready(function() {jQuery('.feat-nav-on .top .links span').css('background-image', jQuery('.feat-nav-on .top .links a').css('background-image'));});

jQuery(document).ready(function() {

	jQuery('.tout-1col > .2col').each(function(index, element) {
		jQuery(element).parent().addClass('tout-2col');
		jQuery(element).parent().removeClass('tout-1col');
		jQuery(element).children().unwrap();
	});
	jQuery('.tout-1col > .3col').each(function(index, element) {
		jQuery(element).parent().addClass('tout-3col');
		jQuery(element).parent().removeClass('tout-1col');
		jQuery(element).children().unwrap();
	});
	jQuery('.tout-1col:empty').remove();
	jQuery('#btm_section .touts_area').children(':not(.clear)').last().removeClass('left'); 
});

// for lightbox changes 

jQuery(document).ready(function() {
	jQuery('.video-box').each(function(index, element) {
		var fbParams = {
			'opacity' : true,
			'overlayColor' : '#000',
			'overlayOpacity': .85,
			'overlayShow' : true,
			'padding' : 14,
			'scrolling' : 'no',
			'speedIn' : 600,
			'speedOut' : 200,
			'titlePosition' : 'inside',
			'titleFormat' : videoCaption,
			'titleShow' : true,
			'type' : 'iframe',
			'width' : 480
		};

		if(jQuery(element).next().hasClass('fb-description')) {
			fbParams['description']=jQuery(element).next().html();
		} else {
			fbParams['description']=jQuery(element).attr('title');
		}

		jQuery(element).fancybox(fbParams); 

	}); 
});

function videoCaption(title, currentArray, currentIndex, currentOpts) {
	var result = jQuery('<div id="video-custom">');
	jQuery(result).append(jQuery('<div class="clear"/>'));
	if(undefined != currentOpts.description) {
		var caption = jQuery('<div id="gallery-caption"/>');
		jQuery(caption).html(currentOpts.description);
		jQuery(result).append(caption);
	}
	return result;
} 

jQuery(document).ready(function() {
	jQuery('.fb-ajax').each(function(index, element) {
		var fbParams = {
			'opacity' : true,
			'overlayColor' : '#000',
			'overlayOpacity': .85,
			'overlayShow' : true,
			'padding' : 14,
			'scrolling' : 'no',
			'speedIn' : 600,
			'speedOut' : 200,
			'titleShow' : false,
			'type' : 'ajax',
			'width' : 480
		};

		if(jQuery(element).next().hasClass('fb-description')) {
			fbParams['description']=jQuery(element).next().html();
		} else {
			fbParams['description']=jQuery(element).attr('title');
		}

		jQuery(element).fancybox(fbParams); 

	}); 
});

jQuery(document).ready(function() {

	var fbParams = {
		'opacity'		: true,
		'overlayColor'	: '#000',
		'overlayOpacity': .85,
		'overlayShow'	: true,
		'padding'		: 14,
		'speedIn'		: 600,
		'speedOut'		: 200,
		'titlePosition' : 'inside',
		'titleFormat'	: pageGalleryTitle,
		'titleShow'		: true,
		'type'			: 'ajax',
		'width'			: 550,
		'showNavArrows'	: false
	};

	jQuery("a.page-gallery").fancybox(fbParams);

 	function pageGalleryTitle(title, currentArray, currentIndex, currentOpts) {

		var result = jQuery('<div class="page-gallery-title"/>');
		if(0 != currentIndex) {
			//result.append('<span id="fancybox-left-ico"/>');
			result.append('<span id="prev">&lt;</span>');
		}
		if(currentArray.length != currentIndex + 1) {
			//result.append('<span id="fancybox-right-ico"/>');
			result.append('<span id="next">&gt;</span>');
		}
		result.append('<div id="page-gallery-caption">'+title+'</div>');

		return result;
	}
	
	jQuery("a.page-gallery").first().append(jQuery("img.page-gallery"));

	jQuery('.page-gallery-title #fancybox-left-ico, .page-gallery-title #prev').live('click', function() {
		jQuery.fancybox.prev();
	});
	jQuery('.page-gallery-title #fancybox-right-ico, .page-gallery-title #next').live('click', function() {
		jQuery.fancybox.next();
	});

});

/* Site Map - rebuild lists */

jQuery(document).ready(function() {
	var currentUl = null;
	jQuery('#sitemap .hide').remove();
	jQuery('#sitemap ul:first-child li').each(function(index, element) {
		if(jQuery(element).hasClass('new-column')) {
			currentUl = null;
		}
		if(null == currentUl) {
			currentUl = jQuery('<ul/>');
			jQuery('#sitemap').append(currentUl);
		}
		if(jQuery(element).hasClass('unlink')) {
			jQuery(element).html(jQuery(element).children().html());
		}
		jQuery(currentUl).append(element);
	});
	jQuery('#sitemap ul:first-child').remove();
	jQuery('#sitemap').css('display', 'block');

});


//side navigation for -SpecialtySolutions

// Show hide sidebar sub navigation
SideNav1 = {};
SideNav1.click = {
	// initialize sub nav
	init: function() {
		// Searces DOM for navArrow1 Divs and applies click behavior to them
		jQuery('.navArrow1').click(this.click);
		
		// Applies appropriate styling to sidenav1
		jQuery('li.open1').children('.navBlock1').children('.navArrow1').addClass('open1');
		jQuery('li.open1').children('.navBlock1').children('.link1').addClass('open1');
		jQuery('li.open1').children('.subnav1').addClass('open1');
		jQuery('li').has('ul').children('.navBlock1').children('.navArrow1').addClass('closed1');
		
		jQuery('li.open1').children('.subNavBlock1').children('.navArrow1').addClass('open1');
		jQuery('li.open1').children('.subNavBlock1').children('.link1').addClass('open1');
		jQuery('li.open1').children('.subNavBlock1').addClass('open1');
		jQuery('li').has('ul').children('.subNavBlock1').children('.navArrow1').addClass('closed1');

		jQuery('li.open1').children('.subnavBlock1').children('.navArrow1').addClass('open1');
		jQuery('li.open1').children('.subnavBlock1').children('.sub-link1').addClass('open1');
		jQuery('li.open1').children('.subnav1').addClass('open1');
		jQuery('li').has('ul').children('.subnavBlock1').children('.navArrow1').addClass('closed1');
		
		var subnavBlock1 = jQuery('li.open1').children('.subnavBlock1');
		if (subnavBlock1.children('a.sub-link1').hasClass('open1')) {
			subnavBlock1.addClass('open1');
		}
	},
	
	click: function() {
	
		var subList = jQuery(this).parent().parent().children('ul');
		if (jQuery(subList).hasClass('subnav1')) {
			var button = jQuery(this);
			// If subnavBlock1 line is hidden, show it
			if (!button.parent().hasClass('open1')) {
				button.parent().addClass('open1');
			}
			
			// Set .sub-link1 on/off
			button.parent().parent().children('.subnavBlock1').children('.sub-link1').toggleClass('open1');
			
			// Set .link1 on/off
			button.parent().parent().children('.navBlock1').children('.link1').toggleClass('open1');
		
			jQuery(subList).slideToggle(300, function() {
				// If subnavBlock1 line is visible, hide it
				if (button.parent().hasClass('open1') && !jQuery(this).parent().hasClass('open1')) {
					button.parent().removeClass('open1');
				}
			});
			
			jQuery(this).toggleClass('open1');
			jQuery(this).parent().parent().toggleClass('open1');
			
			jQuery('li.open1').children('.navBlock1').children('.navArrow1').addClass('open1');
			jQuery('li.open1').children('.subnavBlock1').children('.navArrow1').addClass('open1');
		}
	}
}


/* New Expandable Javascript */

// Document Ready
jQuery(document).ready(function() {
	initExpandable1();
});

// Expandable lists
function initExpandable1() {
jQuery('.expandable1').children('.title1').click(function() {
	jQuery(this).toggleClass('open1');
	jQuery(this).parent().children('.copy1').slideToggle(200);
});
} 

function collapse1() {	
jQuery('.expandable1').children('.copy1').children('.collapse1').click(function() {
		jQuery(this).closest('.copy1').parent().children('.title1').removeClass('open1');
		jQuery(this).closest('.copy1').slideUp(200);
	});
}
