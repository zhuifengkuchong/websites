/* Internal Javascript */

// Document Ready
jQuery(document).ready(function() {
	SwapImg.rollover.init();
	//SideNav.click.init(); /* <-- Comment out on launch */
	initExpandable();
	initFancyBox();
	initTableSorter();
});



// Show hide sidebar sub navigation
SideNav = {};
SideNav.click = {
	// initialize sub nav
	init: function() {
		// Searces DOM for navArrow Divs and applies click behavior to them
		jQuery('.navArrow').click(this.click);
		
		// Applies appropriate styling to sidenav
		jQuery('http://www.cardinal.com/mps/CacheProxyServlet/colorPalette/default/browserVendor/Netscape/browserName/Navigator/browserVersion/unknown/locale/en/forwardurl/mps/themes/html/Cardinal/js/li.open').children('.navBlock').children('.navArrow').addClass('open');
		jQuery('http://www.cardinal.com/mps/CacheProxyServlet/colorPalette/default/browserVendor/Netscape/browserName/Navigator/browserVersion/unknown/locale/en/forwardurl/mps/themes/html/Cardinal/js/li.open').children('.navBlock').children('.link').addClass('open');
		jQuery('http://www.cardinal.com/mps/CacheProxyServlet/colorPalette/default/browserVendor/Netscape/browserName/Navigator/browserVersion/unknown/locale/en/forwardurl/mps/themes/html/Cardinal/js/li.open').children('.subnav').addClass('open');
		jQuery('li').has('ul').children('.navBlock').children('.navArrow').addClass('closed');
		

		jQuery('http://www.cardinal.com/mps/CacheProxyServlet/colorPalette/default/browserVendor/Netscape/browserName/Navigator/browserVersion/unknown/locale/en/forwardurl/mps/themes/html/Cardinal/js/li.open').children('.subNavBlock').children('.navArrow').addClass('open');
		jQuery('http://www.cardinal.com/mps/CacheProxyServlet/colorPalette/default/browserVendor/Netscape/browserName/Navigator/browserVersion/unknown/locale/en/forwardurl/mps/themes/html/Cardinal/js/li.open').children('.subNavBlock').children('.sub-link').addClass('open');
		jQuery('http://www.cardinal.com/mps/CacheProxyServlet/colorPalette/default/browserVendor/Netscape/browserName/Navigator/browserVersion/unknown/locale/en/forwardurl/mps/themes/html/Cardinal/js/li.open').children('.subnav').addClass('open');
		jQuery('li').has('ul').children('.subNavBlock').children('.navArrow').addClass('closed');
		
		var subNavBlock = jQuery('http://www.cardinal.com/mps/CacheProxyServlet/colorPalette/default/browserVendor/Netscape/browserName/Navigator/browserVersion/unknown/locale/en/forwardurl/mps/themes/html/Cardinal/js/li.open').children('.subNavBlock');
		if (subNavBlock.children('a.sub-link').hasClass('open')) {
			subNavBlock.addClass('open');
		}
	},
	
	click: function() {
	
		var subList = jQuery(this).parent().parent().children('ul');
		if (jQuery(subList).hasClass('subnav')) {
			var button = jQuery(this);
			// If subNavBlock line is hidden, show it
			if (!button.parent().hasClass('open')) {
				button.parent().addClass('open');
			}
			
			// Set .sub-link on/off
			button.parent().parent().children('.subNavBlock').children('.sub-link').toggleClass('open');
			
			// Set .link on/off
			button.parent().parent().children('.navBlock').children('.link').toggleClass('open');
		
			jQuery(subList).slideToggle(300, function() {
				// If subNavBlock line is visible, hide it
				if (button.parent().hasClass('open') && !jQuery(this).parent().hasClass('open')) {
					button.parent().removeClass('open');
				}
			});
			
			jQuery(this).toggleClass('open');
			jQuery(this).parent().parent().toggleClass('open');
			
			jQuery('http://www.cardinal.com/mps/CacheProxyServlet/colorPalette/default/browserVendor/Netscape/browserName/Navigator/browserVersion/unknown/locale/en/forwardurl/mps/themes/html/Cardinal/js/li.open').children('.navBlock').children('.navArrow').addClass('open');
			jQuery('http://www.cardinal.com/mps/CacheProxyServlet/colorPalette/default/browserVendor/Netscape/browserName/Navigator/browserVersion/unknown/locale/en/forwardurl/mps/themes/html/Cardinal/js/li.open').children('.subNavBlock').children('.navArrow').addClass('open');
		}
	}
}


// Expandable lists
function initExpandable() {
	jQuery('.expandable').children('.title').click(function() {
		jQuery(this).toggleClass('open');
		jQuery(this).parent().children('.copy').slideToggle(200);
	});
}


//FancyBox
function initFancyBox() {
	jQuery("a.fb-gallery").each(function(index, element) {

		var fbOpts = {	
			'autoDimensions':	true,
			'opacity'		:	true,
			'overlayColor'	:	'#000',
			'overlayOpacity':	.85,
			'overlayShow'	:	true,
			'padding'		:	14,
			'scrolling'		:	'no',
			'showNavArrows'	:	true,
			'speedIn'		:	600,
			'speedOut'		:	200,
			'titlePosition'	:	'inside',
			'titleShow'		:	true,
			'titleFormat'	:	galleryTitle,
			'type'			:	'image'
		};

		if(jQuery(element).next().hasClass('fb-description')) {
			fbOpts['description']=jQuery(element).next().html();
		}

		jQuery(element).fancybox(fbOpts);

	});	
}

//FancyBox
jQuery(document).ready(function() {
	jQuery("a.fb-iframe").each(function(index, element) {
	
		var fbParams = {
			'opacity'		: true,
			'overlayColor'	: '#000',
			'overlayOpacity': .85,
			'overlayShow'	: true,
			'padding'		: 14,
			'scrolling'		: 'no',
			'showNavArrows'	: true,
			'speedIn'		: 600,
			'speedOut'		: 200,
			'titlePosition'	: 'inside',
			'titleShow'		: true,
			'titleFormat'	: galleryTitle,
			'type'			: 'iframe',
			'autoScale' 	: 	false,
			'autoDimensions':	true
		};

		if(jQuery(element).next().hasClass('fb-description')) {
			fbParams['description']=jQuery(element).next().html();
		}

		jQuery(element).fancybox(fbParams);
	});

	jQuery("a.fb-large").each(function(index, element) {
	
		var fbParams = {
			'opacity'		: true,
			'overlayColor'	: '#000',
			'overlayOpacity': .85,
			'overlayShow'	: true,
			'padding'		: 14,
			'scrolling'		: 'auto',
			'showNavArrows'	: true,
			'speedIn'		: 600,
			'speedOut'		: 200,
			'width'			: 750,
			'height'		: '95%',
			'titlePosition'	: 'inside',
			'titleShow'		: true,
			'titleFormat'	: galleryTitle,
			'type'			: 'iframe',
			'autoScale' 	: 	false,
			'autoDimensions':	true
		};

		if(jQuery(element).next().hasClass('fb-description')) {
			fbParams['description']=jQuery(element).next().html();
		}

		jQuery(element).fancybox(fbParams);
	});
});

function galleryTitle(title, currentArray, currentIndex, currentOpts) {

	var result = jQuery('<div id="gallery-custom">');
	if(undefined != title) {
		var titleDiv = jQuery('<div id="gallery-title"/>');
		jQuery(titleDiv).html(title);
		jQuery(result).append(titleDiv);
		jQuery(result).append(jQuery('<div class="clear"/>'));
	}
	if(undefined != currentOpts.description) {
		var caption = jQuery('<div id="gallery-caption"/>'); 
		jQuery(caption).html(currentOpts.description);
		jQuery(result).append(caption);
	}

	return result;
}

function initTableSorter() {
	// add new widget for assigning row styles
	jQuery.tablesorter.addWidget({
	    // give the widget a id 
	    id: "assignRowStyles", 

	    // format is called when the on init and when a sorting has finished 
	    format: function(table) { 
			jQuery("tr > td", table).removeClass("odd");
			jQuery("tr:odd > td", table).addClass("odd");
		}
	}); 

	// Trigger sortable tables
	jQuery(".sortable").tablesorter({
		//Default sort order is first col
		sortList: [[0,0]],
	    widgets: ['assignRowStyles'] 
	  }); 
}