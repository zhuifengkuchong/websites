/* Global Javascript */

jQuery.noConflict();

// Rollover image swap
SwapImg = {};
SwapImg.rollover = {
	init: function() {
		this.preload();
		jQuery('.ro').mouseover(this.over);
		jQuery('.ro').mouseout(this.out);
	},

	preload: function() {
		jQuery('.ro').each(function(key, elm) { jQuery('<img>').attr('src', SwapImg.rollover.newimage(jQuery(this).attr('src'))); });
	},

	over: function() {
		jQuery(this).attr('src', SwapImg.rollover.newimage(jQuery(this).attr('src')));
	},

	out: function() {
		jQuery(this).attr('src', jQuery(this).attr('src').replace(/_ro/, ''));
	},

	newimage: function(src) {
		return src.substring(0, src.search(/((_ro)*\.[f,g,i,n,p]+)/)) + '_ro' + src.match(/(\.[f,g,i,n,p]+)/)[0];
	}
}
