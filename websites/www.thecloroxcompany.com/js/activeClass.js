/*
* activeClass.js
* @author Jason Ware
* 
* This looks for elements matching the body id + "Nav":
* for instance, if there is a body id of #products and 
* a navigation element with an id of #productsNav, 
* a class of .active will be added to the element.
*
* 9/22/10 - Added a second selection for tabs that 
* are actually separate sub-pages and use a subpage derived body class
*
* 12/6/11 - simplified now, but supports "#nav_", "#subnav_", "#tab_", 
* with ".current" indicating the current page state
*
* 
* @version $Id: activeClass.js 1522 2011-12-16 18:07:27Z jware $
* 
*/

$(document).ready(function() {

	//get the body id
	var bodyId = $('body').attr('id');
	//get the elements of the body class string
	var bodyClass = $('body').attr('class').split(' ');
    
    // the body id match is always for the current page
    $('#nav_'+bodyId+',#subnav_'+bodyId).addClass('current');
    // any body class matches highlight the path
	$.each(bodyClass,function(index,value){
	   $('#nav_'+value+',#subnav_'+value+',#tab_'+value).addClass('active');
	})
	
	//special case for cr "products" section
	if($('body').attr('class').indexOf('corporate-responsibility')>=0){
	   $('#tab_products').removeClass('active');
	}

});