/*
 * Site Common Javascript
 *
 * May 10, 2010
 *
 */

// init the local namespace. All functions should be part of this namespace
var $tcc = window.$tcc || {};

$(document).ready(function() {

	// Set up accordions

	// default accordion functionality
	$('.accordion h4').click(function() {
	    if( !$(this).hasClass('nestedHeader') ) {
    	    $('.accordionContent').slideUp(100);  
    	    $('.accordion h4').removeClass("active");  
	    } else {
    	    $('.nestedAccordion').slideUp(100);
    	    $('.nestedHeader').removeClass("active");
	    }
		
		//IF THE NEXT SLIDE WASN'T OPEN THEN OPEN IT
		if($(this).next().is(':hidden') == true) {

			//ADD THE active CLASS TO THE BUTTON
			$(this).addClass('active');

			//OPEN THE SLIDE
			$(this).next().slideDown(100);
		 }
		 return false;
	});

	// Close all accordions on page load
    $('.accordion .accordionContent').hide();

    //check if hash tag exists in the URL
    if(window.location.hash) {

        //set the value as a variable, and remove the #
        var hash_value = window.location.hash.replace('#', '');

        // test to make sure the hashtag actually matches something in an accordion
        if($('.accordion #'+hash_value).length > 0){
            // trigger an open accordion based on the hash tag value
            $('.accordion #'+hash_value).trigger('click');
         }else{
            // or just open whichever one was set as default
            $(".accordion .open").trigger('click');
         }

    }else{

        // or just open whichever one was set as default
        $(".accordion .open").trigger('click');
    }

    // if a link on the same page as an accordion
    // has a class matching the id of one of the accordion
    // section + "_open" (e.g. class="product-supply_open" ) it will
    // open that corresponding accordion section

    if($('.accordion .accordionContent').length > 0){

        $('.accordion h4').each(function(){
            var theId = $(this).attr('id');

            $('a.'+theId+'_open').click(function(){

                $('#'+theId).trigger('click');
                console.info($(this));
            });
        });
    }


    $clxcommon.initExternal();
    $clxcommon.initWarning();
    $clxcommon.initWarningFrench();
    $clxcommon.initWarningSpanish();
    $clxcommon.attachClickAnalytics('clorox-promo');
    $clxcommon.attachClickAnalytics('clorox-video');
    $clxcommon.attachClickAnalytics('clorox-download');

 	//LINKED DIVS
 	//$clxcommon.createLink('#someDiv', '/some-path/');

	$clxcommon.createLink('#home #coupons', '/products/special-offers/');
	$clxcommon.createLink('#home #centennialPromo', '/corporate-responsibility/blog/');
	$clxcommon.createLink('#home #annualReport', 'http://annualreport.thecloroxcompany.com/');
	$clxcommon.createLink('#home #supplyChainsAct', 'https://www.thecloroxcompany.com/downloads/Clorox_CA_Supply_Chains_Act_Disclosure.pdf');

 	$clxcommon.createLink('#performance #promo6', '#');
  	$clxcommon.createLink('#performance #promo4', 'http://'+INVESTORS_DOMAIN);

  	$clxcommon.createLink('#people #promo8', '/corporate-responsibility/people/workplace-safety/');

 	$clxcommon.createLink('#products #promo10', '/products/ingredients-inside/');
 	$clxcommon.createLink('#products #promo11', '/corporate-responsibility/products/product-sustainability/');

 	$clxcommon.createLink('#careers #promo1', '/careers/working-at-clorox/');
 	$clxcommon.createLink('#careers #promo3', '/careers/who-we-are/');

 	$clxcommon.createLink('#company #promo1', '/company/mission-and-values/');
 	$clxcommon.createLink('#company #promo2', 'http://'+INVESTORS_DOMAIN+'/bios.cfm');
 	$clxcommon.createLink('#company #promo3', '/company/heritage/bottle-guide/');

 	$clxcommon.createLink('#products #promo1', '/products/our-brands');
 	$clxcommon.createLink('#products #promo2', '/products/special-offers');
 	$clxcommon.createLink('#products #promo3', 'http://cloroxconnects.com/');

	$clxcommon.createLink('#innovation #promo1', 'http://blog.cloroxconnects.com/');
	

	$clxcommon.createLink('#heritage #promo1', '/company/heritage/timeline');
	$clxcommon.createLink('#heritage #promo2', '/company/heritage/bottle-guide');

 	$clxcommon.createLink('#corporate-governance #promo1', 'http://'+INVESTORS_DOMAIN+'/bios.cfm');
 	$clxcommon.createLink('#corporate-governance #promo2', '/corporate-responsibility/performance/corporate-governance/board-of-directors/');



	// Set up Sliders
	// **************
	if( $('#slider1').length > 0)
	{

		$('#slider1').anythingSlider({
			resizeContents      : true, // If true, solitary images/objects in the panel will expand to fit the viewport
			navigationSize      : 11,     // Set this to the maximum number of visible navigation tabs; false to disable
			navigationFormatter : function(index, panel){ // Format navigation labels with text
				return ['1910s', '1920s', '1930s', '1940s', '1950s', '1960s', '1970s', '1980s', '1990s', '2000s', '2010s'][index - 1];
			},
			onSlideComplete: function(slider) {
				// keep the current navigation tab in view
				slider.navWindow( slider.currentPage );
			}
		});
		$('#slider1').css('visibility','visible');
	};

	// Set up modals
	// Load dialog on click
	// this first case is particular to modals with flowplayer
	// to prevent IE crashes when flash elements don't unload
	if( $('.modalWithUnload').length ){
	   $('.modalWithUnload').click(function (e) {
			e.preventDefault();
			var aux = $(this).attr('id');
        	aux = aux.replace(/[A-z]+/,'modalContent');
			$('#' + aux).modal({
				overlayClose: true,
				// this is the important part to this
				// particular version
				onClose: function (dialog) {
            	   $f().unload(function(){
            	       $.modal.close();
            	   });
            	}
			});

		});
	}
	//standard case opening/closing modal
	if( $('.modal').length ){
		$('.modal').click(function (e) {
			e.preventDefault();
			var aux = $(this).attr('id');
        	aux = aux.replace(/[A-z]+/,'modalContent');
			$('#' + aux).modal({
				overlayClose: true
			});

		});
	}

	//open simplemodal on cr page from a particular hashed link
	if(window.location.hash.indexOf('ingredients-inside-video') != -1){
	   $('#modalContent2').modal();

	}




});

//hide or show the career area div on select.

var lastDiv = "";
function showDiv(divName) {
	// hide last div
	if (lastDiv) {
		document.getElementById(lastDiv).className = "hiddenDiv";
	}
	//if value of the box is not nothing and an object with that name exists, then change the class
	if (divName && document.getElementById(divName)) {
		document.getElementById(divName).className = "visibleDiv";
		lastDiv = divName;
	}
}


//spinning centennial logo


//v1.7
// Flash Player Version Detection
// Detect Client Browser type
// Copyright 2005-2008 Adobe Systems Incorporated.  All rights reserved.
var isIE  = (navigator.appVersion.indexOf("MSIE") != -1) ? true : false;
var isWin = (navigator.appVersion.toLowerCase().indexOf("win") != -1) ? true : false;
var isOpera = (navigator.userAgent.indexOf("Opera") != -1) ? true : false;
function ControlVersion()
{
	var version;
	var axo;
	var e;
	// NOTE : new ActiveXObject(strFoo) throws an exception if strFoo isn't in the registry
	try {
		// version will be set for 7.X or greater players
		axo = new ActiveXObject("ShockwaveFlash.ShockwaveFlash.7");
		version = axo.GetVariable("$version");
	} catch (e) {
	}
	if (!version)
	{
		try {
			// version will be set for 6.X players only
			axo = new ActiveXObject("ShockwaveFlash.ShockwaveFlash.6");

			// installed player is some revision of 6.0
			// GetVariable("$version") crashes for versions 6.0.22 through 6.0.29,
			// so we have to be careful.

			// default to the first public version
			version = "WIN 6,0,21,0";
			// throws if AllowScripAccess does not exist (introduced in 6.0r47)
			axo.AllowScriptAccess = "always";
			// safe to call for 6.0r47 or greater
			version = axo.GetVariable("$version");
		} catch (e) {
		}
	}
	if (!version)
	{
		try {
			// version will be set for 4.X or 5.X player
			axo = new ActiveXObject("ShockwaveFlash.ShockwaveFlash.3");
			version = axo.GetVariable("$version");
		} catch (e) {
		}
	}
	if (!version)
	{
		try {
			// version will be set for 3.X player
			axo = new ActiveXObject("ShockwaveFlash.ShockwaveFlash.3");
			version = "WIN 3,0,18,0";
		} catch (e) {
		}
	}
	if (!version)
	{
		try {
			// version will be set for 2.X player
			axo = new ActiveXObject("ShockwaveFlash.ShockwaveFlash");
			version = "WIN 2,0,0,11";
		} catch (e) {
			version = -1;
		}
	}

	return version;
}
// JavaScript helper required to detect Flash Player PlugIn version information
function GetSwfVer(){
	// NS/Opera version >= 3 check for Flash plugin in plugin array
	var flashVer = -1;

	if (navigator.plugins != null && navigator.plugins.length > 0) {
		if (navigator.plugins["Shockwave Flash 2.0"] || navigator.plugins["Shockwave Flash"]) {
			var swVer2 = navigator.plugins["Shockwave Flash 2.0"] ? " 2.0" : "";
			var flashDescription = navigator.plugins["Shockwave Flash" + swVer2].description;
			var descArray = flashDescription.split(" ");
			var tempArrayMajor = descArray[2].split(".");
			var versionMajor = tempArrayMajor[0];
			var versionMinor = tempArrayMajor[1];
			var versionRevision = descArray[3];
			if (versionRevision == "") {
				versionRevision = descArray[4];
			}
			if (versionRevision[0] == "d") {
				versionRevision = versionRevision.substring(1);
			} else if (versionRevision[0] == "r") {
				versionRevision = versionRevision.substring(1);
				if (versionRevision.indexOf("d") > 0) {
					versionRevision = versionRevision.substring(0, versionRevision.indexOf("d"));
				}
			}
			var flashVer = versionMajor + "." + versionMinor + "." + versionRevision;
		}
	}
	// MSN/WebTV 2.6 supports Flash 4
	else if (navigator.userAgent.toLowerCase().indexOf("webtv/2.6") != -1) flashVer = 4;
	// WebTV 2.5 supports Flash 3
	else if (navigator.userAgent.toLowerCase().indexOf("webtv/2.5") != -1) flashVer = 3;
	// older WebTV supports Flash 2
	else if (navigator.userAgent.toLowerCase().indexOf("webtv") != -1) flashVer = 2;
	else if ( isIE && isWin && !isOpera ) {
		flashVer = ControlVersion();
	}
	return flashVer;
}
// When called with reqMajorVer, reqMinorVer, reqRevision returns true if that version or greater is available
function DetectFlashVer(reqMajorVer, reqMinorVer, reqRevision)
{
	versionStr = GetSwfVer();
	if (versionStr == -1 ) {
		return false;
	} else if (versionStr != 0) {
		if(isIE && isWin && !isOpera) {
			// Given "WIN 2,0,0,11"
			tempArray         = versionStr.split(" "); 	// ["WIN", "2,0,0,11"]
			tempString        = tempArray[1];			// "2,0,0,11"
			versionArray      = tempString.split(",");	// ['2', '0', '0', '11']
		} else {
			versionArray      = versionStr.split(".");
		}
		var versionMajor      = versionArray[0];
		var versionMinor      = versionArray[1];
		var versionRevision   = versionArray[2];
        	// is the major.revision >= requested major.revision AND the minor version >= requested minor
		if (versionMajor > parseFloat(reqMajorVer)) {
			return true;
		} else if (versionMajor == parseFloat(reqMajorVer)) {
			if (versionMinor > parseFloat(reqMinorVer))
				return true;
			else if (versionMinor == parseFloat(reqMinorVer)) {
				if (versionRevision >= parseFloat(reqRevision))
					return true;
			}
		}
		return false;
	}
}
function AC_AddExtension(src, ext)
{
  if (src.indexOf('?') != -1)
    return src.replace(/\?/, ext+'?');
  else
    return src + ext;
}
function AC_Generateobj(objAttrs, params, embedAttrs)
{
  var str = '';
  if (isIE && isWin && !isOpera)
  {
    str += '<object ';
    for (var i in objAttrs)
    {
      str += i + '="' + objAttrs[i] + '" ';
    }
    str += '>';
    for (var i in params)
    {
      str += '<param name="' + i + '" value="' + params[i] + '" /> ';
    }
    str += '</object>';
  }
  else
  {
    str += '<embed ';
    for (var i in embedAttrs)
    {
      str += i + '="' + embedAttrs[i] + '" ';
    }
    str += '> </embed>';
  }
  document.write(str);
}
function AC_FL_RunContent(){
  var ret =
    AC_GetArgs
    (  arguments, ".swf", "movie", "clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"
     , "application/x-shockwave-flash"
    );
  AC_Generateobj(ret.objAttrs, ret.params, ret.embedAttrs);
}
function AC_SW_RunContent(){
  var ret =
    AC_GetArgs
    (  arguments, ".dcr", "src", "clsid:166B1BCA-3F9C-11CF-8075-444553540000"
     , null
    );
  AC_Generateobj(ret.objAttrs, ret.params, ret.embedAttrs);
}
function AC_GetArgs(args, ext, srcParamName, classid, mimeType){
  var ret = new Object();
  ret.embedAttrs = new Object();
  ret.params = new Object();
  ret.objAttrs = new Object();
  for (var i=0; i < args.length; i=i+2){
    var currArg = args[i].toLowerCase();
    switch (currArg){
      case "classid":
        break;
      case "pluginspage":
        ret.embedAttrs[args[i]] = args[i+1];
        break;
      case "src":
      case "movie":
        args[i+1] = AC_AddExtension(args[i+1], ext);
        ret.embedAttrs["src"] = args[i+1];
        ret.params[srcParamName] = args[i+1];
        break;
      case "onafterupdate":
      case "onbeforeupdate":
      case "onblur":
      case "oncellchange":
      case "onclick":
      case "ondblclick":
      case "ondrag":
      case "ondragend":
      case "ondragenter":
      case "ondragleave":
      case "ondragover":
      case "ondrop":
      case "onfinish":
      case "onfocus":
      case "onhelp":
      case "onmousedown":
      case "onmouseup":
      case "onmouseover":
      case "onmousemove":
      case "onmouseout":
      case "onkeypress":
      case "onkeydown":
      case "onkeyup":
      case "onload":
      case "onlosecapture":
      case "onpropertychange":
      case "onreadystatechange":
      case "onrowsdelete":
      case "onrowenter":
      case "onrowexit":
      case "onrowsinserted":
      case "onstart":
      case "onscroll":
      case "onbeforeeditfocus":
      case "onactivate":
      case "onbeforedeactivate":
      case "ondeactivate":
      case "type":
      case "codebase":
      case "id":
        ret.objAttrs[args[i]] = args[i+1];
        break;
      case "width":
      case "height":
      case "align":
      case "vspace":
      case "hspace":
      case "class":
      case "title":
      case "accesskey":
      case "name":
      case "tabindex":
        ret.embedAttrs[args[i]] = ret.objAttrs[args[i]] = args[i+1];
        break;
      default:
        ret.embedAttrs[args[i]] = ret.params[args[i]] = args[i+1];
    }
  }
  ret.objAttrs["classid"] = classid;
  if (mimeType) ret.embedAttrs["type"] = mimeType;
  return ret;
}

// jQuery Tabs for Recognition page fiscal years //
$(function () {
    var tabContainers = $('section.recognition-years > div');

    $('section.recognition-years ul.tabBoxNav a').click(function () {
        tabContainers.hide().filter(this.hash).show().css('display','inline-block', 'margin-bottom', '50px');

        $('section.recognition-years ul.tabBoxNav a').removeClass('selected').css('background','#b2e1fb');
        $(this).addClass('selected').css('background','white');

        return false;
    }).filter(':first').click();
});