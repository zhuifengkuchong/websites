/**
 * Functionality specific to the Home page 
 *
 */
 
// init the local namespace. All functions should be part of this namespace
var $tcc = window.$tcc || {};

$(document).ready(function() {
	
	//Centennial Overlay - disabled for now.
	// select the overlay element - and "make it an overlay"
	
	/*$("#modalContentCentennial").overlay({
		effect: 'apple',
		start: {
		  top: 500,
		  left: null,
		  absolute: true
		},
	    speed: 2000,
	    mask: {
		    color: '#ffffff',  
		    loadSpeed: 0,  
		    show: "auto",
		    opacity: 1,
		    closeSpeed: 5000,
		    done: 'fadeOut'
	    },
	    closeOnClick: false,
	    load: true,
	    closeSpeed: 3000
	});
    
    setTimeout('$("#modalContentCentennial").data("overlay").close();', 3000);
    */
    
	// Set up homepage brands slider
	$('#mycarousel').jcarousel({
	// Configuration goes here
		visible: 7,
		scroll: 7,
		wrap: 'circular'
	});
	
	// Set up homepage accordion
	$("#homeAccordion").liteAccordion({ 
	    containerWidth : 930, 
	    containerHeight : 398,
	    headerWidth: 15,
	    slideSpeed : 400,
	    autoPlay: true,
	    cycleSpeed: 8000
	});


});


