function KeySubmitSearchQuery(ev)
{if(window.event)
{ev=window.event;}
if(ev.keyCode==13)
{document.getElementById("queryButton").click();return false;}}
function RaiseSearchQueryEvent()
{var queryBox=document.getElementById("queryTextBox");var queryText="";if(queryBox!=null&&queryBox!="undefined")
{queryText=TrimString(queryBox.value);}
if(queryText!="")
{var rootUrl=location.protocol+"//"+location.host;if(scriptFullSiteColUrl!=null&&scriptFullSiteColUrl!="")
{rootUrl=rootUrl+scriptFullSiteColUrl;}
else
{rootUrl=rootUrl+"/";}
window.top.location.href=rootUrl+"Pages/results.aspx?k="+queryText;}}
function TrimString(sStr)
{var iI=0;var iJ=0;var iTam=0;var sAux="";iTam=sStr.length;if(iTam==0)
{return(sStr);}
for(iI=0;iI<iTam;iI++)
{if(sStr.charAt(iI)!=' ')
{break;}}
if(iI>=iTam)
{return("");}
for(iJ=iTam-1;iJ>=0;iJ--)
{if(sStr.charAt(iJ)!=' ')
{break;}}
return(sStr.substring(iI,iJ+1));}
function LoadSearchQuery()
{var Query="";if(window.top.location.search!=0)
{Query=window.top.location.search;var keywordQuery=getParameter(Query,'k');if(keywordQuery!=null)
{var queryBox=document.getElementById("queryTextBox");if(queryBox)
{queryBox.value=keywordQuery;}}}}
function getParameter(queryString,parameterName)
{var parameterName=parameterName+"=";if(queryString.length>0)
{var begin=queryString.indexOf(parameterName);if(begin!=-1)
{begin+=parameterName.length;var end=queryString.indexOf("&",begin);if(end==-1)
{end=queryString.length;}
return decodeURIComponent(queryString.substring(begin,end));}}
return null;}