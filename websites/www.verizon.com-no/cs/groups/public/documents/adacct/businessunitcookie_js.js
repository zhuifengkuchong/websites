function getCookie(name){
	var dcookie=document.cookie;
	var cname=name + '=';
	var clen=dcookie.length;
	var cbegin=0;
	while (cbegin < clen){
		var vbegin=cbegin + cname.length;
		if(dcookie.substring(cbegin, vbegin)==cname){
			var vend=dcookie.indexOf (';', vbegin);
			if(vend==-1) vend=clen;
			return unescape(dcookie.substring(vbegin, vend));
		}
		cbegin=dcookie.indexOf(' ', cbegin) + 1;
		if(cbegin==0) break;
	}
	return null;
}

function getSubCookie(masterCookieName,subCookieName){
	var cookie = getCookie(masterCookieName);
	if(cookie == null){
		return null;
	}
	var subCookie = cookie.split("&");
	for(var element in subCookie){
		if(element != null && element.indexOf("=")!=-1){
			var subCookieDetails = element.split("=");
			if(subCookieDetails[0].equals(cookieName) && subCookieDetails.length > 1){
				return subCookieDetails[1];
			}
		}
	}
	return null;
}

function deleteCookie(name){
	if(getCookie(name) != null){
		if(getCookie(name).length > 0){
			document.cookie = name + "=;path=/;domain=.verizon.com;expires=Thu, 01-Jan-1970 00:00:01 GMT;"
		}
	}
}

function deleteSubCookie(masterCookieName,subCookieName){
	var cookie = getCookie(masterCookieName);
	if (cookie == null) {
		return true;
	}else{
		var subCookieVal = getSubCookie(masterCookieName,subCookieName);
		if(cookie.indexOf(subCookieName)!=-1){
			cookie =cookie.replace(subCookieName+"="+subCookieVal,"");
			cookie =cookie.replace("&&", "&");
		}
		if(cookie!="" && cookie.substring(0,1).equals("&")){
			cookie = cookie.substring(1, value.length());
		}
		if(!value.isEmpty() && cookie.substring(cookie.length-1, cookie.length)=="&"){
			cookie = cookie.substring(0, cookie.length-1) ;
		}
		document.cookie=masterCookieName +'='+ cookie + ';path=/;domain=.verizon.com'+((expiredays==null) ? ";" : ";expires="+exdate.toGMTString());
		return true;
	}
}

function setCookie(b,d,a){
	var e = new Date();
	var c = document.domain;
	e.setDate(e.getDate()+a);
	//alert(b+"="+escape(d));
	document.cookie=b+"="+escape(d)+";path=/;domain=.verizon.com"+((a==null)?";":";expires="+e.toGMTString())
	//alert(document.cookie);
}

function SetBusinessunitcookie(businessunit){
	var offset = false;
	if(typeof master_cookie_on!="undefined" && master_cookie_on.toUpperCase()=="Y"){
		offset = true;
	}
	var strbusinessUnit = businessunit;
	if(strbusinessUnit == 'wireless'){
		if(offset){
			setMasterCookie('BusinessUnit','wireless',60);
		}else{
			deleteCookie('BusinessUnit');
			setCookie('BusinessUnit','wireless',60);
		}
		window.location.href = "http://www.verizonwireless.com/?intcmp=VZ-GHOME-D-WIRELESS"
		return false;
	}else if(strbusinessUnit == 'business'){
		if(offset){
			setMasterCookie('BusinessUnit','business',60);
		}else{
			deleteCookie('BusinessUnit');
			setCookie('BusinessUnit','business',60);
		}
		window.location.href = 'http://www.verizon.com/home/verizonglobalhome/ghp_business.aspx';
		return false;
	}else if(strbusinessUnit == 'residential'){
		if(offset){
			setMasterCookie('BusinessUnit','residential',60);
		}else{
			deleteCookie('BusinessUnit');
			setCookie('BusinessUnit','residential',60);
		}
		window.location.href = "http://www.verizon.com/?lid=//global//residential";
		return false;
	}
}

function setMasterCookie(c_name,value,expiredays){
	var exdate=new Date();
	var domain = document.domain;
	exdate.setDate(exdate.getDate()+expiredays);
	var master = getCookie("VZ_ATLAS_SITE_PERS");
	if(master==null){
		document.cookie = 'VZ_ATLAS_SITE_PERS='+c_name +'='+escape(value) +';path=/;domain=.verizon.com'+((expiredays==null) ? ";" : ";expires="+exdate.toGMTString());
	}else{
		if(master.indexOf(c_name+"=")!=-1){
			var start = master.indexOf(c_name+"=");
			var len = start+c_name.length+1;
			var end = master.indexOf("&",len);
			if (end == -1) end = master.length;
			var subcookie = master.substring(start,end);
			master = master.replace(subcookie,"");
			master =master.replace("&&", "&");
			if(master!="" && master.substring(0,1)=="&"){
				master = master.substring(1, master.length);
			}
			if(master!="" && master.substring(master.length-1, master.length)=="&"){
				master = master.substring(0, master.length-1) ;
			}
			if(master==""){
				master=c_name +"="+escape(value);
			}else{
				master=master+"&"+c_name +"="+escape(value);
			}
		}else{
			master=master+"&"+c_name +"="+escape(value);
		}
		document.cookie='VZ_ATLAS_SITE_PERS='+master + ';path=/;domain=.verizon.com'+((expiredays==null) ? ";" : ";expires="+exdate.toGMTString());
	}
}

var expiredate = new Date();
expiredate.setMinutes( expiredate.getMinutes() + 2);
document.cookie = "hbxRet"+"="+"Y" + ";path=/;domain=.verizon.com;expires="+expiredate.toGMTString();
