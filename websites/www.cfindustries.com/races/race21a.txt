<script id = "race21a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race21={};
	myVars.races.race21.varName="DOMNode[0x7f2024cda6a0].className";
	myVars.races.race21.varType="@varType@";
	myVars.races.race21.repairType = "@RepairType";
	myVars.races.race21.event1={};
	myVars.races.race21.event2={};
	myVars.races.race21.event1.id = "Lu_Id_div_11";
	myVars.races.race21.event1.type = "onmouseover";
	myVars.races.race21.event1.loc = "Lu_Id_div_11_LOC";
	myVars.races.race21.event1.isRead = "False";
	myVars.races.race21.event1.eventType = "@event1EventType@";
	myVars.races.race21.event2.id = "Lu_Id_div_11";
	myVars.races.race21.event2.type = "onmouseout";
	myVars.races.race21.event2.loc = "Lu_Id_div_11_LOC";
	myVars.races.race21.event2.isRead = "True";
	myVars.races.race21.event2.eventType = "@event2EventType@";
	myVars.races.race21.event1.executed= false;// true to disable, false to enable
	myVars.races.race21.event2.executed= false;// true to disable, false to enable
</script>

