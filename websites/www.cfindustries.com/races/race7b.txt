<script id = "race7b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race7={};
	myVars.races.race7.varName="Lu_Id_div_11__onmouseover";
	myVars.races.race7.varType="@varType@";
	myVars.races.race7.repairType = "@RepairType";
	myVars.races.race7.event1={};
	myVars.races.race7.event2={};
	myVars.races.race7.event1.id = "Lu_Id_div_11";
	myVars.races.race7.event1.type = "onmouseover";
	myVars.races.race7.event1.loc = "Lu_Id_div_11_LOC";
	myVars.races.race7.event1.isRead = "True";
	myVars.races.race7.event1.eventType = "@event1EventType@";
	myVars.races.race7.event2.id = "Lu_Id_script_3";
	myVars.races.race7.event2.type = "Lu_Id_script_3__parsed";
	myVars.races.race7.event2.loc = "Lu_Id_script_3_LOC";
	myVars.races.race7.event2.isRead = "False";
	myVars.races.race7.event2.eventType = "@event2EventType@";
	myVars.races.race7.event1.executed= false;// true to disable, false to enable
	myVars.races.race7.event2.executed= false;// true to disable, false to enable
</script>

