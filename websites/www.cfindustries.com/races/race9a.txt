<script id = "race9a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race9={};
	myVars.races.race9.varName="Lu_Id_div_17__onmouseover";
	myVars.races.race9.varType="@varType@";
	myVars.races.race9.repairType = "@RepairType";
	myVars.races.race9.event1={};
	myVars.races.race9.event2={};
	myVars.races.race9.event1.id = "Lu_Id_script_3";
	myVars.races.race9.event1.type = "Lu_Id_script_3__parsed";
	myVars.races.race9.event1.loc = "Lu_Id_script_3_LOC";
	myVars.races.race9.event1.isRead = "False";
	myVars.races.race9.event1.eventType = "@event1EventType@";
	myVars.races.race9.event2.id = "Lu_Id_div_17";
	myVars.races.race9.event2.type = "onmouseover";
	myVars.races.race9.event2.loc = "Lu_Id_div_17_LOC";
	myVars.races.race9.event2.isRead = "True";
	myVars.races.race9.event2.eventType = "@event2EventType@";
	myVars.races.race9.event1.executed= false;// true to disable, false to enable
	myVars.races.race9.event2.executed= false;// true to disable, false to enable
</script>

