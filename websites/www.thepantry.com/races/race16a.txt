<script id = "race16a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race16={};
	myVars.races.race16.varName="Object[1013].staticHoverClass";
	myVars.races.race16.varType="@varType@";
	myVars.races.race16.repairType = "@RepairType";
	myVars.races.race16.event1={};
	myVars.races.race16.event2={};
	myVars.races.race16.event1.id = "Lu_Id_script_9";
	myVars.races.race16.event1.type = "Lu_Id_script_9__parsed";
	myVars.races.race16.event1.loc = "Lu_Id_script_9_LOC";
	myVars.races.race16.event1.isRead = "False";
	myVars.races.race16.event1.eventType = "@event1EventType@";
	myVars.races.race16.event2.id = "Menu1n3";
	myVars.races.race16.event2.type = "onmouseover";
	myVars.races.race16.event2.loc = "Menu1n3_LOC";
	myVars.races.race16.event2.isRead = "True";
	myVars.races.race16.event2.eventType = "@event2EventType@";
	myVars.races.race16.event1.executed= false;// true to disable, false to enable
	myVars.races.race16.event2.executed= false;// true to disable, false to enable
</script>

