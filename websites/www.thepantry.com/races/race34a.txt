<script id = "race34a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race34={};
	myVars.races.race34.varName="Window[26].__menuInterval";
	myVars.races.race34.varType="@varType@";
	myVars.races.race34.repairType = "@RepairType";
	myVars.races.race34.event1={};
	myVars.races.race34.event2={};
	myVars.races.race34.event1.id = "Menu1n1ItemsUp";
	myVars.races.race34.event1.type = "onmouseover";
	myVars.races.race34.event1.loc = "Menu1n1ItemsUp_LOC";
	myVars.races.race34.event1.isRead = "True";
	myVars.races.race34.event1.eventType = "@event1EventType@";
	myVars.races.race34.event2.id = "Menu1n0";
	myVars.races.race34.event2.type = "onmouseout";
	myVars.races.race34.event2.loc = "Menu1n0_LOC";
	myVars.races.race34.event2.isRead = "False";
	myVars.races.race34.event2.eventType = "@event2EventType@";
	myVars.races.race34.event1.executed= false;// true to disable, false to enable
	myVars.races.race34.event2.executed= false;// true to disable, false to enable
</script>

