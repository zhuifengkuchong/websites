<script id = "race31a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race31={};
	myVars.races.race31.varName="Window[26].__menuInterval";
	myVars.races.race31.varType="@varType@";
	myVars.races.race31.repairType = "@RepairType";
	myVars.races.race31.event1={};
	myVars.races.race31.event2={};
	myVars.races.race31.event1.id = "Menu1n2ItemsDn";
	myVars.races.race31.event1.type = "onmouseover";
	myVars.races.race31.event1.loc = "Menu1n2ItemsDn_LOC";
	myVars.races.race31.event1.isRead = "True";
	myVars.races.race31.event1.eventType = "@event1EventType@";
	myVars.races.race31.event2.id = "Menu1n0";
	myVars.races.race31.event2.type = "onmouseout";
	myVars.races.race31.event2.loc = "Menu1n0_LOC";
	myVars.races.race31.event2.isRead = "False";
	myVars.races.race31.event2.eventType = "@event2EventType@";
	myVars.races.race31.event1.executed= false;// true to disable, false to enable
	myVars.races.race31.event2.executed= false;// true to disable, false to enable
</script>

