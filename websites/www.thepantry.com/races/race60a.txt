<script id = "race60a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race60={};
	myVars.races.race60.varName="Window[26].__rootMenuItem";
	myVars.races.race60.varType="@varType@";
	myVars.races.race60.repairType = "@RepairType";
	myVars.races.race60.event1={};
	myVars.races.race60.event2={};
	myVars.races.race60.event1.id = "Menu1n5";
	myVars.races.race60.event1.type = "onmouseout";
	myVars.races.race60.event1.loc = "Menu1n5_LOC";
	myVars.races.race60.event1.isRead = "False";
	myVars.races.race60.event1.eventType = "@event1EventType@";
	myVars.races.race60.event2.id = "Menu1n7";
	myVars.races.race60.event2.type = "onmouseout";
	myVars.races.race60.event2.loc = "Menu1n7_LOC";
	myVars.races.race60.event2.isRead = "True";
	myVars.races.race60.event2.eventType = "@event2EventType@";
	myVars.races.race60.event1.executed= false;// true to disable, false to enable
	myVars.races.race60.event2.executed= false;// true to disable, false to enable
</script>

