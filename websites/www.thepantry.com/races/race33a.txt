<script id = "race33a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race33={};
	myVars.races.race33.varName="Window[26].__menuInterval";
	myVars.races.race33.varType="@varType@";
	myVars.races.race33.repairType = "@RepairType";
	myVars.races.race33.event1={};
	myVars.races.race33.event2={};
	myVars.races.race33.event1.id = "Menu1n1ItemsDn";
	myVars.races.race33.event1.type = "onmouseover";
	myVars.races.race33.event1.loc = "Menu1n1ItemsDn_LOC";
	myVars.races.race33.event1.isRead = "True";
	myVars.races.race33.event1.eventType = "@event1EventType@";
	myVars.races.race33.event2.id = "Menu1n0";
	myVars.races.race33.event2.type = "onmouseout";
	myVars.races.race33.event2.loc = "Menu1n0_LOC";
	myVars.races.race33.event2.isRead = "False";
	myVars.races.race33.event2.eventType = "@event2EventType@";
	myVars.races.race33.event1.executed= false;// true to disable, false to enable
	myVars.races.race33.event2.executed= false;// true to disable, false to enable
</script>

