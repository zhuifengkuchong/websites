<script id = "race20a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race20={};
	myVars.races.race20.varName="Window[26].__disappearAfter";
	myVars.races.race20.varType="@varType@";
	myVars.races.race20.repairType = "@RepairType";
	myVars.races.race20.event1={};
	myVars.races.race20.event2={};
	myVars.races.race20.event1.id = "Menu1n3";
	myVars.races.race20.event1.type = "onmouseover";
	myVars.races.race20.event1.loc = "Menu1n3_LOC";
	myVars.races.race20.event1.isRead = "False";
	myVars.races.race20.event1.eventType = "@event1EventType@";
	myVars.races.race20.event2.id = "Menu1n4";
	myVars.races.race20.event2.type = "onmouseover";
	myVars.races.race20.event2.loc = "Menu1n4_LOC";
	myVars.races.race20.event2.isRead = "False";
	myVars.races.race20.event2.eventType = "@event2EventType@";
	myVars.races.race20.event1.executed= false;// true to disable, false to enable
	myVars.races.race20.event2.executed= false;// true to disable, false to enable
</script>

