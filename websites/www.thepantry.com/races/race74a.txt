<script id = "race74a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race74={};
	myVars.races.race74.varName="Window[26].__menuInterval";
	myVars.races.race74.varType="@varType@";
	myVars.races.race74.repairType = "@RepairType";
	myVars.races.race74.event1={};
	myVars.races.race74.event2={};
	myVars.races.race74.event1.id = "Menu1n9";
	myVars.races.race74.event1.type = "onmouseout";
	myVars.races.race74.event1.loc = "Menu1n9_LOC";
	myVars.races.race74.event1.isRead = "False";
	myVars.races.race74.event1.eventType = "@event1EventType@";
	myVars.races.race74.event2.id = "Menu1n10";
	myVars.races.race74.event2.type = "onmouseout";
	myVars.races.race74.event2.loc = "Menu1n10_LOC";
	myVars.races.race74.event2.isRead = "True";
	myVars.races.race74.event2.eventType = "@event2EventType@";
	myVars.races.race74.event1.executed= false;// true to disable, false to enable
	myVars.races.race74.event2.executed= false;// true to disable, false to enable
</script>

