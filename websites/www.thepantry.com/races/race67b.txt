<script id = "race67b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race67={};
	myVars.races.race67.varName="Window[26].__disappearAfter";
	myVars.races.race67.varType="@varType@";
	myVars.races.race67.repairType = "@RepairType";
	myVars.races.race67.event1={};
	myVars.races.race67.event2={};
	myVars.races.race67.event1.id = "Menu1n8";
	myVars.races.race67.event1.type = "onmouseout";
	myVars.races.race67.event1.loc = "Menu1n8_LOC";
	myVars.races.race67.event1.isRead = "True";
	myVars.races.race67.event1.eventType = "@event1EventType@";
	myVars.races.race67.event2.id = "Menu1n5";
	myVars.races.race67.event2.type = "onmouseover";
	myVars.races.race67.event2.loc = "Menu1n5_LOC";
	myVars.races.race67.event2.isRead = "False";
	myVars.races.race67.event2.eventType = "@event2EventType@";
	myVars.races.race67.event1.executed= false;// true to disable, false to enable
	myVars.races.race67.event2.executed= false;// true to disable, false to enable
</script>

