<script id = "race66a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race66={};
	myVars.races.race66.varName="Window[26].__menuInterval";
	myVars.races.race66.varType="@varType@";
	myVars.races.race66.repairType = "@RepairType";
	myVars.races.race66.event1={};
	myVars.races.race66.event2={};
	myVars.races.race66.event1.id = "Menu1n7";
	myVars.races.race66.event1.type = "onmouseout";
	myVars.races.race66.event1.loc = "Menu1n7_LOC";
	myVars.races.race66.event1.isRead = "False";
	myVars.races.race66.event1.eventType = "@event1EventType@";
	myVars.races.race66.event2.id = "Menu1n8";
	myVars.races.race66.event2.type = "onmouseout";
	myVars.races.race66.event2.loc = "Menu1n8_LOC";
	myVars.races.race66.event2.isRead = "True";
	myVars.races.race66.event2.eventType = "@event2EventType@";
	myVars.races.race66.event1.executed= false;// true to disable, false to enable
	myVars.races.race66.event2.executed= false;// true to disable, false to enable
</script>

