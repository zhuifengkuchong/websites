<script id = "race54a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race54={};
	myVars.races.race54.varName="Window[26].__disappearAfter";
	myVars.races.race54.varType="@varType@";
	myVars.races.race54.repairType = "@RepairType";
	myVars.races.race54.event1={};
	myVars.races.race54.event2={};
	myVars.races.race54.event1.id = "Menu1n5";
	myVars.races.race54.event1.type = "onmouseover";
	myVars.races.race54.event1.loc = "Menu1n5_LOC";
	myVars.races.race54.event1.isRead = "False";
	myVars.races.race54.event1.eventType = "@event1EventType@";
	myVars.races.race54.event2.id = "Menu1n4";
	myVars.races.race54.event2.type = "onmouseout";
	myVars.races.race54.event2.loc = "Menu1n4_LOC";
	myVars.races.race54.event2.isRead = "True";
	myVars.races.race54.event2.eventType = "@event2EventType@";
	myVars.races.race54.event1.executed= false;// true to disable, false to enable
	myVars.races.race54.event2.executed= false;// true to disable, false to enable
</script>

