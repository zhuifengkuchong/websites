<script id = "race36a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race36={};
	myVars.races.race36.varName="Window[26].__menuInterval";
	myVars.races.race36.varType="@varType@";
	myVars.races.race36.repairType = "@RepairType";
	myVars.races.race36.event1={};
	myVars.races.race36.event2={};
	myVars.races.race36.event1.id = "Menu1n3";
	myVars.races.race36.event1.type = "onmouseover";
	myVars.races.race36.event1.loc = "Menu1n3_LOC";
	myVars.races.race36.event1.isRead = "True";
	myVars.races.race36.event1.eventType = "@event1EventType@";
	myVars.races.race36.event2.id = "Menu1n0";
	myVars.races.race36.event2.type = "onmouseout";
	myVars.races.race36.event2.loc = "Menu1n0_LOC";
	myVars.races.race36.event2.isRead = "False";
	myVars.races.race36.event2.eventType = "@event2EventType@";
	myVars.races.race36.event1.executed= false;// true to disable, false to enable
	myVars.races.race36.event2.executed= false;// true to disable, false to enable
</script>

