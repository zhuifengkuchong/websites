<script id = "race5b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race5={};
	myVars.races.race5.varName="Window[26].Menu1_Data";
	myVars.races.race5.varType="@varType@";
	myVars.races.race5.repairType = "@RepairType";
	myVars.races.race5.event1={};
	myVars.races.race5.event2={};
	myVars.races.race5.event1.id = "Menu1n4";
	myVars.races.race5.event1.type = "onkeyup";
	myVars.races.race5.event1.loc = "Menu1n4_LOC";
	myVars.races.race5.event1.isRead = "True";
	myVars.races.race5.event1.eventType = "@event1EventType@";
	myVars.races.race5.event2.id = "Lu_Id_script_9";
	myVars.races.race5.event2.type = "Lu_Id_script_9__parsed";
	myVars.races.race5.event2.loc = "Lu_Id_script_9_LOC";
	myVars.races.race5.event2.isRead = "False";
	myVars.races.race5.event2.eventType = "@event2EventType@";
	myVars.races.race5.event1.executed= false;// true to disable, false to enable
	myVars.races.race5.event2.executed= false;// true to disable, false to enable
</script>

