<script id = "race4b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race4={};
	myVars.races.race4.varName="Window[26].Menu1_Data";
	myVars.races.race4.varType="@varType@";
	myVars.races.race4.repairType = "@RepairType";
	myVars.races.race4.event1={};
	myVars.races.race4.event2={};
	myVars.races.race4.event1.id = "Menu1n3";
	myVars.races.race4.event1.type = "onkeyup";
	myVars.races.race4.event1.loc = "Menu1n3_LOC";
	myVars.races.race4.event1.isRead = "True";
	myVars.races.race4.event1.eventType = "@event1EventType@";
	myVars.races.race4.event2.id = "Lu_Id_script_9";
	myVars.races.race4.event2.type = "Lu_Id_script_9__parsed";
	myVars.races.race4.event2.loc = "Lu_Id_script_9_LOC";
	myVars.races.race4.event2.isRead = "False";
	myVars.races.race4.event2.eventType = "@event2EventType@";
	myVars.races.race4.event1.executed= false;// true to disable, false to enable
	myVars.races.race4.event2.executed= false;// true to disable, false to enable
</script>

