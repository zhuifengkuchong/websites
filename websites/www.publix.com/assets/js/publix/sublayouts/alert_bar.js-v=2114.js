/* Copyright 2013 Publix. All Right Reserved. */

require(["jquery", "publix/toggle"], function ($,toggle) {

    "use strict";

    // -----------------------
    // Run when DOM is loaded
    // -----------------------

    $(document).ready(function () {

        //setup collapse targets
        toggle.init();
    });
});


