/* Copyright 2013 Publix. All Right Reserved. */

require(['vendor/jquery.bxslider'], function () {

    'use strict';

    //private variables
    var pageTotalWidth = 1002;
    var numSlides,
        tabWidth,
        tabPct,
        slider;

    //create default options
    var options = {
        auto: ($('#autoPlay').val() === "1" ? true : false), // configured in Sitecore Slideshow data section
        autoHover: false,
        controls: false,
        infiniteLoop: true,
        touchEnabled: true,
        oneToOneTouch: true,
        preventDefaultSwipeX: true,
        preventDefaultSwipeY: false,
        pause: (parseInt($('#slideDelay').val(), 10) * 1000) // configured in Sitecore Slideshow data section
    };
    

    //set slider tab widths based on number of slides and slide borders.
    function setTabSize() {
        numSlides = $('.slider-pager > .tab').length;
        tabWidth = ((pageTotalWidth / numSlides) - (numSlides + 1) / numSlides);
        tabPct = ((tabWidth / pageTotalWidth) * 100);

        $('.slider-pager > .tab').each(function () {
            $(this).css('width', tabPct + '%');
        });
    }

    //create slider using bx-slider jQuery plugin
    function createSlider() {
        //check for custom pager.  add it if it is found on the page
        if ($('.slider-pager').length > 0) {
            options.pagerCustom = '.slider-pager';

            //set tab configuration for large slider
            setTabSize();
        } else {
            options.pager = true;
        }

        slider = $('.bx-slider').bxSlider(options);
    }

    //handle next/prev clicks
    function createNavHandler() {
        $('.nextNav, .prevNav').click(function () {
            if ($(this).hasClass('nextNav')) {
                slider.goToNextSlide();
            } else {
                slider.goToPrevSlide();
            }

            slider.stopAuto();
        });
    }

    //only show nav when user mouses over the hero
    function hideNav() {
        $('.nextNav, .prevNav').hide();

        //setup hover event handler
        $('.slider').hover(function () {
            //show nav on mouseover
            $('.nextNav, .prevNav').fadeIn();
        }, function () {
            //hide nav on mouseout
            $('.nextNav, .prevNav').hide();
        });
    }

    function init() {
        createSlider();
        createNavHandler();

        if ($('.hero').data('hidenav')) {
            hideNav();
        }

        //show slider, now that slider has been created
        $('.bx-slider').show();

        // wait 0 ms to prevent image 'flash'
        setTimeout(function () {
            //set heroes to visible
            $('.bx-slider .hero').css('visibility', 'visible');
        }, 0);
    }

    //initialize slider
    init();

});