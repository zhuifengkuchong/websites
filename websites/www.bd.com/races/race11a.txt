<script id = "race11a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race11={};
	myVars.races.race11.varName="#document[0x7f368de0c400].keyup";
	myVars.races.race11.varType="@varType@";
	myVars.races.race11.repairType = "@RepairType";
	myVars.races.race11.event1={};
	myVars.races.race11.event2={};
	myVars.races.race11.event1.id = "Lu_DOM";
	myVars.races.race11.event1.type = "onDOMContentLoaded";
	myVars.races.race11.event1.loc = "Lu_DOM_LOC";
	myVars.races.race11.event1.isRead = "False";
	myVars.races.race11.event1.eventType = "@event1EventType@";
	myVars.races.race11.event2.id = "form1";
	myVars.races.race11.event2.type = "onkeyup";
	myVars.races.race11.event2.loc = "form1_LOC";
	myVars.races.race11.event2.isRead = "True";
	myVars.races.race11.event2.eventType = "@event2EventType@";
	myVars.races.race11.event1.executed= false;// true to disable, false to enable
	myVars.races.race11.event2.executed= false;// true to disable, false to enable
</script>

