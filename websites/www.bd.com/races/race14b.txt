<script id = "race14b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race14={};
	myVars.races.race14.varName="Tree[0x7f368de0c458]:LocationDrop";
	myVars.races.race14.varType="@varType@";
	myVars.races.race14.repairType = "@RepairType";
	myVars.races.race14.event1={};
	myVars.races.race14.event2={};
	myVars.races.race14.event1.id = "Lu_Id_div_10";
	myVars.races.race14.event1.type = "onmouseover";
	myVars.races.race14.event1.loc = "Lu_Id_div_10_LOC";
	myVars.races.race14.event1.isRead = "True";
	myVars.races.race14.event1.eventType = "@event1EventType@";
	myVars.races.race14.event2.id = "LocationDrop";
	myVars.races.race14.event2.type = "LocationDrop__parsed";
	myVars.races.race14.event2.loc = "LocationDrop_LOC";
	myVars.races.race14.event2.isRead = "False";
	myVars.races.race14.event2.eventType = "@event2EventType@";
	myVars.races.race14.event1.executed= false;// true to disable, false to enable
	myVars.races.race14.event2.executed= false;// true to disable, false to enable
</script>

