function openWindow(imgURL,width,height) {
	var wParameters = "toolbar=no,width="+width+",height="+height+",directories=no,status=yes,scrollbars=yes,resize=yes,menubar=no,top=100,left=100";
    if (window.displayWindow) displayWindow.close();
	if (navigator.appName == 'Microsoft Internet Explorer') {
		msgWindow=window.open(imgURL,"ImageWindow", wParameters);
	}
	else {
		msgWindow=window.open(imgURL,"ImageWindow", wParameters);
    }
	msgWindow.focus();
}


$(document).ready(function() {

	$('.collapsible span').each(function() {
		$(this).click(function() {
		  $('.collapsible ul').hide('fast');
	 
		  var hadClass = ($(this).hasClass('collapsed'));
		  $('.collapsible span').removeClass('collapsed');    

		  if(!hadClass) {
			$(this).addClass('collapsed');  
			$('ul', $(this).parent()).show('fast');        
		  }
		});  
	  });
	  
	  $('.collapsible:first span').click();

});