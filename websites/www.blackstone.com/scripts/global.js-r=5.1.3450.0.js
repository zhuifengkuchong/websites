/// <reference path="../../ajax.aspnetcdn.com/ajax/jQuery/jquery-1.6.4-vsdoc.js"/*tpa=http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.6.4-vsdoc.js*/ />

function InitExpander(i) { }

function currentBXLanguage() {
    var domainStr = window.location.host.toLowerCase();
    //don't do exact matches... for ease of testing
    if (domainStr.indexOf('japan') != -1) {
        return 'ja';
    }
    else if (domainStr.indexOf('china') != -1) {
        return 'cn';
    }
    else {
        return 'en';
    }
}

$(document).ready(function () {
    $('div.expander').each(function (i, e) {
        var header = $('div.expandHeader', e);

        header.click(function () {
            $('div.expandSummary', e).toggle();
            $('div.expandContent', e).toggle();

            var currImg = $('.expandHeader img', e);
            var currImgSrc = currImg.attr('src');
            if (currImgSrc.indexOf('Unknown_83_filename'/*tpa=http://www.blackstone.com/scripts/expandGreen.gif*/) > -1) {
                currImg.attr('src', currImgSrc.replace('Unknown_83_filename'/*tpa=http://www.blackstone.com/scripts/expandGreen.gif*/, 'Unknown_83_filename'/*tpa=http://www.blackstone.com/scripts/collapseGreen.gif*/));
            }
            else {
                currImg.attr('src', currImgSrc.replace('Unknown_83_filename'/*tpa=http://www.blackstone.com/scripts/collapseGreen.gif*/, 'Unknown_83_filename'/*tpa=http://www.blackstone.com/scripts/expandGreen.gif*/));
            }
        });
    });
});

function getQueryString() {
  var result = {}, queryString = location.search.substring(1),
      re = /([^&=]+)=([^&]*)/g, m;

  while (m = re.exec(queryString)) {
    result[m[1]] = m[2];
  }

  return result;
}

function combineQueryString(qs) {
    var result = "?";
    for (var k in qs) {
        if (qs[k] !== undefined) {
            result = result + k + "=" + qs[k] + "&";
        }
    }

    return result.substring(0, result.length - 1);
}

function FilterByYear(dropdown) {
    
    var url = window.location.href;
    var query = window.location.search;

    var qs = getQueryString();
    qs['year'] = dropdown.value;

    delete qs['page']; 

    query = combineQueryString(qs);

    window.location = query;
}

function emailPage() {         
    var body = "The link below is from the Blackstone website.  " + escape(window.document.title +"\n"+ String.fromCharCode(13)+window.location.href);        
    var subject = "The Blackstone Group - Website Link";
    window.location.href = "mailto:?body="+body+"&subject="+subject;
}

function initTooltips() {
    $(".transactionWrapper").hover(transactionHoverIn, transactionHoverOut);
}

function transactionHoverIn(eventObj) {
    var tooltip = $(".transactionToolTipTop", eventObj.delegateTarget);
    var pos = $(this).position();
    tooltip.css({ left: (pos.left + 150), top: (pos.top + 120) });
    tooltip.show();
}

function transactionHoverOut(eventObj) {
    $(".transactionToolTipTop", eventObj.delegateTarget).hide();
}

$(document).ready(function () {
    var box = $('.sfsearchTxt');
    box.focus(function (e) {
        e.target.value = '';
        $(e.target).removeClass('sfSearchTxtBoxInactive');
    });
    box.blur(function (e) {
        if (e.target.value == '') {
            $(e.target).addClass('sfSearchTxtBoxInactive');
            /*<sl:translate>*/
            e.target.value = 'Search...';
            /*</sl:translate>*/
        }
    });
    box.blur();
});

function bxNavMenu() {
    var self = this;
    var opened = false;
    var timeout = 100;
    var closetimer = 0;
    var ddMenuItem = 0;
    var ddMenuBorder = null;
    var menuBg = $('<div id="bxNav_menu_bg" />').appendTo('body');
    var headerWrapper = $('#headerWrapper')


    var colorMap = {
        "bxNav_item_0": "#00B0CA",
        "bxNav_item_1": "#BED600",
        "bxNav_item_2": "#9B1889",
        "bxNav_item_3": "#00B2A9",
        "bxNav_item_4": "#A30050",
        "bxNav_item_5": "#0098C3"
    };

 $("ul.bxLeftNav_menu li:contains('Networks')").hide();
        

    //hack to fix width. 
    $('li.bxNav_text a[href*="/citizenship/social-environmental-sustainability"]').addClass('widthFix');

//gets number of tabs, then divides by 960 (width of bar), vaule is width of tabs.
    var num = $('.bxNav_menu > li').length;
    var newWidth = parseInt(960 / num);
    $('.bxNav_menu > li').css("width", newWidth + "px");



    $("#menuBar ul").each(
  function() {
    var elem = $(this);
    if (elem.children().length == 0) {
      elem.remove();
    }
  }
);


$("ul.bxNav_menu li:contains('The Firm'), ul.bxNav_menu li:contains('ファームについて'), ul.bxNav_menu li:contains('关于本公司')").addClass("colFirm");
$("ul.bxNav_menu li:contains('Investors'), ul.bxNav_menu li:contains('投资者')").addClass("colInv");
 $("ul.bxNav_menu li:contains('Businesses'), ul.bxNav_menu li:contains('事業内容'), ul.bxNav_menu li:contains('业务')").addClass("colBus");

$('.colFirm').find('li.bxNav_text').unwrap().each(function () {
    $(this).nextUntil('.bxNav_text').andSelf().wrapAll($('<ul/>'));
});

 $('.colBus').find('li.bxNav_text').unwrap().each(function () {
    $(this).nextUntil('.bxNav_text').andSelf().wrapAll($('<ul/>'));
});

 $('.colInv').find('li.bxNav_text').unwrap().each(function () {
    $(this).nextUntil('.bxNav_text').andSelf().wrapAll($('<ul/>'));
});
 

    //removes line in single ul tabs
    $('div.bxNav_submenu').filter(function () {
        return $(this).find('ul').length == 1;
    }).addClass('noLine ul');

//adds black and grey line between uls in flyout
    $('div.bxNav_submenu ul:first-child').addClass('addLine');

   
 //sets height for each ul depending on height of flyout. this gets the ul border to reach bottom 
    $(document).ready(function () {
        $.each($('.bxNav_submenu'), function () {
        var subH = $(this).height();
        $(this).find("ul").css("height", subH - 40 + "px");
        $("div.bxNav_submenu li:contains('Public Funds'), div.bxNav_submenu li:contains('公募ファンド'), div.bxNav_submenu li:contains('公共基金')").css( "margin-left", "5px" ).css( "font-weight", "bold" );
        $("div.bxNav_submenu li:contains('Public Companies')").css("margin-left", "5px").css("font-weight", "bold");
       
    });
    
     
    var pageLink = window.location.pathname.replace(/^\/([^\/]*).*$/, '$1');

        if (pageLink == "the-firm") {
          $(".bxNav_tab_0").addClass('inSec_0');
        }
        if (pageLink == "businesses") {
          $(".bxNav_tab_1").addClass('inSec_1');
        }
        if (pageLink == "citizenship") {
          $(".bxNav_tab_2").addClass('inSec_2');
        }
        if (pageLink == "news-views") {
          $(".bxNav_tab_4").addClass('inSec_4');
        }
        if (pageLink == "careers") {
          $(".bxNav_tab_5").addClass('inSec_5');
        }

  
});





    function cancelTimer() {
        if (closetimer) {
            window.clearTimeout(closetimer);
            closetimer = null;
        }
    };

    function animateOpen(ddMenuItem) {
        ddMenuItem.css('visibility', 'visible');
              
    };

  

    function addSelectedBg() {
        $('div.bxNav_border[data-sel-color]').each(function (i, e) {
            var $e = $(e);
            $e.css('background-color', $e.data('selColor'));
        });
    }

    var i = 0;
        
    $(".bxNav_headertext").each(function () {
        $(this).addClass("bxNav_tab_" + i);
        i++;
    });



    this.open = function (e) {
        var alreadyOpened = opened;
        cancelTimer();
        self.close(!alreadyOpened);

        opened = true;



        ddMenuItem = $(this).find('div.bxNav_submenu'); //.css('visibility', 'visible');
        ddMenuBorder = $(".bxNav_headertext", this);

        if (!alreadyOpened) {
            animateOpen(ddMenuItem);
        }
        else {
            menuBg.show();
            ddMenuItem.css('visibility', 'visible');
        }

        var c = $("span", this).attr("class");
        var newColor = colorMap[c];
        var indexNum2 = $(this).index();
       
        ddMenuBorder.addClass("topNavBtnOn");
        ddMenuBorder.addClass("bxNav_tab_" + indexNum2 + "_hov");

        loginManager.superHidePopup();

        e.stopPropagation();
    };

    this.close = function (adjustMargin) {
        if (ddMenuItem) {
            ddMenuItem.css('visibility', 'hidden');
        }
        if (ddMenuBorder) {
       
            ddMenuBorder.removeClass("topNavBtnOn");
            ddMenuBorder.removeClass("bxNav_tab_0_hov bxNav_tab_1_hov bxNav_tab_2_hov bxNav_tab_3_hov bxNav_tab_4_hov bxNav_tab_5_hov bxNav_tab_6_hov");
           
        }

        if (adjustMargin) {
            headerWrapper.css('margin-bottom', '');
            menuBg.hide();
            addSelectedBg();
            opened = false;
        }
    };

    this.timerProc = function() {
        closetimer = window.setTimeout(self.close, timeout);
    };
};

$(document).ready(function () {
    var nav = new bxNavMenu();
    $('.bxNav_menu > li:not(.bxNav_shim)').click(nav.open);
    $(".lpLoginDropdown").click(nav.close);
    $(document).click(nav.close);

      $('ul.bxNav_menu li:last-child .bxNav_submenu').addClass('submenuRight');
      
    
      $(".instagramImage").hover(function () {
          //On Hover - Works on ios
          $(".instagramImage").addClass("instagramHover");
      }, function () {
          //Hover Off - Hover off doesn't seem to work on iOS
          $(".instagramImage").removeClass("instagramHover");
      })

      $(".facebookImage").hover(function () {
          //On Hover - Works on ios
          $(".facebookImage").addClass("facebookHover");
      }, function () {
          //Hover Off - Hover off doesn't seem to work on iOS
          $(".facebookImage").removeClass("facebookHover");
      })

      $(".twitterImage").hover(function () {
          //On Hover - Works on ios
          $(".twitterImage").addClass("twitterHover");
      }, function () {
          //Hover Off - Hover off doesn't seem to work on iOS
          $(".twitterImage").removeClass("twitterHover");
      })


});



//Twitter Icon rollover code


var ALERT_TITLE = "Disclaimer";
var ALERT_BUTTON_TEXT = "Cancel";
var ALERT_BUTTON_YES = "Yes";

// over-ride the alert method only if this a newer browser.
// Older browser will see standard alerts
if (document.getElementById && window.location.pathname.indexOf('/Action/Edit') == -1 && window.location.pathname.indexOf('/cxonetwork/') == -1) {
    window.confirm = function (txt, href) {
        createCustomAlert(txt, href);
    }
}

function createCustomAlert(txt, href) {
    // shortcut reference to the document object
    d = document;

    // if the modalContainer object already exists in the DOM, bail out.
    if (d.getElementById("modalContainer")) return;

    // create the modalContainer div as a child of the BODY element
    mObj = d.getElementsByTagName("body")[0].appendChild(d.createElement("div"));
    mObj.id = "modalContainer";
    // make sure its as tall as it needs to be to overlay all the content on the page
    mObj.style.height = document.documentElement.scrollHeight + "px";

    // create the DIV that will be the alert 
    alertObj = mObj.appendChild(d.createElement("div"));
    alertObj.id = "alertBox";
    // MSIE doesnt treat position:fixed correctly, so this compensates for positioning the alert
    if (d.all && !window.opera) alertObj.style.top = document.documentElement.scrollTop + "px";
    // center the alert box
    alertObj.style.left = (d.documentElement.scrollWidth - alertObj.offsetWidth) / 2 + "px";

    // create an H1 element as the title bar
    h1 = alertObj.appendChild(d.createElement("h1"));
    h1.appendChild(d.createTextNode(ALERT_TITLE));

    // create a paragraph element to contain the txt argument
    msg = alertObj.appendChild(d.createElement("p"));
    msg.innerHTML = txt;

    // create an anchor element to use as the confirmation button.

    btn = alertObj.appendChild(d.createElement("a"));
    btn.id = "yesBtn";
    btn.appendChild(d.createTextNode(ALERT_BUTTON_YES));
    btn.href = href;


    btn = alertObj.appendChild(d.createElement("a"));
    btn.id = "closeBtn";
    btn.appendChild(d.createTextNode(ALERT_BUTTON_TEXT));
    btn.href = "#";
    // set up the onclick event to remove the alert when the anchor is clicked
    btn.onclick = function () { removeCustomAlert(); return false; }
}

// removes the custom alert from the DOM
function removeCustomAlert() {
    document.getElementsByTagName("body")[0].removeChild(document.getElementById("modalContainer"));
}

/* HACK: remove the extra BR sitefinity inserts into content blocks for the left nav */
$(document).ready(function () {
    $("#Body_T687B4377001_Col00 .sfContentBlock > br:last-child").remove();
});

var currentImg = null;

$(document).ready(function () {
    $("a[data-img]").hover(function (e) {
        if (currentImg !== null) {
            currentImg.remove();
            //return;
        }

        var src = $(this).attr("data-img");
        if (src == '' || src == undefined)
            return;

        var style = 'left: ' + (e.pageX + 5) + 'px; top: ' + (e.pageY + 5) + 'px';
        var ele = $("<div class='personImagePopup' style='" + style + "' />")
            .append("<img src='" + src + "' />");

        ele.appendTo($(document.body));
        ele.show();
        currentImg = ele;
    }
    , function () {
        if (currentImg !== null) {
            currentImg.remove();
        }
    }
    );
});

function popup(url) {
    var width = 450;
    var height = 430;
    var left = (screen.width - width) / 2;
    var top = (screen.height - height) / 2;
    var params = 'width=' + width + ', height=' + height;
    params += ', top=' + top + ', left=' + left;
    params += ', directories=no, location=no, menubar=no, resizable=no, scrollbars=no, status=no, toolbar=no';
    newwin = window.open(url, 'Contactus', params);
    if (window.focus) { newwin.focus() }
    return false;
}

$(document).ready(function () {
    var page = function (e, forwards) {
        var container = $(e.delegateTarget).parents('.imgPager');
        var curr = $('.imgPagerCurrent', container);

        var idx = curr.attr('data-img-idx');
        var max = parseInt(curr.attr('data-img-max'));
        var nextIdx = parseInt(idx) + (forwards ? 1 : -1);

        if (forwards && idx == (max - 1)) {
            nextIdx = 0;
        }
        else if (!forwards && idx == 0) {
            nextIdx = (max - 1);
        }

        $('.imgPagerCurrIdx', container).html(nextIdx + 1);

        var next = container.children('[data-img-idx=' + nextIdx.toString() + ']');
        curr.toggleClass('imgPagerHidden imgPagerCurrent');
        next.toggleClass('imgPagerHidden imgPagerCurrent');
    };

    $('.imgPager .imgPagerMaxIdx').html(function (e) {
        var max = $(this).parents('.imgPager').children('[data-img-max]').first().attr('data-img-max');
        return max;
    });

    $('.imgPager .imgPagerNext').click(function (e) {
        page(e, true);
        return false;
    });
    $('.imgPager .imgPagerPrev').click(function (e) {
        page(e, false);
        return false;
    });
});

String.prototype.format = function () {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function (match, number) {
        return typeof args[number] != 'undefined'
      ? args[number]
      : match
    ;
    });
};

(function ($) {
    $.fn.dropShadow = function() {      
        if (this.data('hasDropShadow') === true)
            return;

        this.addClass('dropshadow-imp');
        /*this.wrap('<div class="dropshadow2-out"></div>');

        var $p = this.parent();

        for (var a = 0; a < 4; a++)
        {
            $p.prepend('<div class="dropshadow2" style="top: {0}px; left: {1}px"></div>'.format(a+1, a+4));
            $p.prepend('<div class="dropshadow2" style="top: {0}px; left: {1}px"></div>'.format(a+4, a+1));
        }*/

        this.data('hasDropShadow', true);
    };

    $.fn.removeDropShadow = function () {
        /*$('.dropshadow2', this.parents('.dropshadow2-out')).remove();

        this.unwrap();*/

        this.removeClass('dropshadow-imp');
        this.data('hasDropShadow', false);
    };
})(jQuery);

$(document).ready(function () {
    $(".dropshadow").dropShadow();
});

$(document).ready(function () {
    $(".popupWindow").each(
        function (i, e) {
            var content = $(".normalContent", e);
            var popup = null;

            if (!($(e).data("encoded"))) {
                popup = $(".popupContent", e);
            }

            content.click(function (n) {
                if (popup === null) {
                    popup = $(eval($(e).data("popupId")));
                }

                if (typeof _gaq !== 'undefined') {
                    var popupName = $('.h5', content).text();
                    if (popupName === undefined || popupName === '') {
                        popupName = $('h2', content).text();
                    }
                    var path = window.location.pathname;
                    if (path.indexOf('/', path.length - 1) === -1) {
                        path = path + '/';
                    }
                    path = path + escape(popupName + '-Popup');
                    _gaq.push(['_trackPageview', popupName]);
                }

                popup.show();
                $.fancybox(
                {
                    content: popup,
                    overlayOpacity: .6,
                    overlayColor: '#000',
                    scrolling: 'no',
                    padding: 0,
                    autoScale: false
                });
            });
        }
    );
});

$(document).ready(function () {
    $(".hoverBlock").hover(
        function (e) {
            $(this).dropShadow();
        },
        function (e) {
            $(this).removeDropShadow();
        }
    );
});

$(document).ready(function () {
    $(".vidpreview")
        .wrap("<div class='vidhover-out' />")
        .before("<div class='vidhover-in' />");

    $(".vidhover-in").each(function (i, e) {
        $(e).css(
        {
            left: (($(e).parent().width() - 67) / 2) + "px",
            top: (($(e).parent().height() - 67) / 2) + "px"
        }
        );
    });
});

function printPopup() {
    printElement($("#fancybox-inner")[0]);
}

function printElement(element) {
    var w = window.open();
    w.document.write("<html><head>");
    $("link,style").each(function (i, e) {
        w.document.write(e.outerHTML);
    });
    w.document.write("</head><body>");
    w.document.write($(element).html());
    w.document.write("</body>");
    w.document.close();
    w.focus();
    w.print();
    w.close();
}

//twitter message inline javascript





(function ($) {
    $(document).ready(function () {
        if (currentBXLanguage() === 'ja') {
            $("#jp_article_37_disclosure_link").removeClass('hideMeAux');
        }
    });
})(jQuery);

var loginManager = new function () {
    var loadedHtml = false;
    var boxOpen = false;
    var addedPlaceholders = false;

    function addPlaceholders() {
        if (addedPlaceholders)
            return;

        addedPlaceholders = true;
        if (!$.support.placeholder) {
            var password = $(':password');
            var span = $('.passwordPlaceholder');

            if (password != undefined) {
                $('#loginForms').addClass('ie');
                span.show();

                $(':text').focus(function () {
                    if ($(this).attr('placeholder') != '' && $(this).attr('placeholder') !== undefined && $(this).val() == $(this).attr('placeholder')) {
                        $(this).val('').removeClass('hasPlaceholder');
                    }
                }).blur(function () {
                    if ($(this).attr('placeholder') != '' && $(this).attr('placeholder') !== undefined && $(this).attr('placeholder') !== undefined && ($(this).val() == '' || $(this).val() == $(this).attr('placeholder'))) {
                        $(this).val($(this).attr('placeholder')).addClass('hasPlaceholder');
                    }
                });

                password.focus(function () {
                    $(this).parent().children('.passwordPlaceholder').css('visibility', 'hidden');
                }).blur(function () {
                    if ($(this).val() == '' || $(this).val() == undefined) {
                        $(this).parent().children('.passwordPlaceholder').css('visibility', 'visible');
                    }
                });

                span.click(function () {
                    password.focus();
                });

                password.blur();
                $(':text').focus();
                $('form').submit(function () {
                    $(this).find('.hasPlaceholder').each(function () { $(this).val(''); });
                });
            }
        }
    }

    function onTabClick() {
        $('#appButtonTable td').each(function (i, x) {
            var newClass = $(x).attr('class').replace('buttonSelectedLogin', 'buttonLogin');
            $(x).attr('class', newClass);
            $(x).removeClass('btnSelectedLogin');
        });

        var newClass = $(this).attr('class').replace('buttonLogin', 'buttonSelectedLogin');
        $(this).attr('class', newClass);
        $(this).addClass('btnSelectedLogin');

        var container = $(this).data('containerDiv');
        $('.loginForms div').removeClass('loginFormSelected');
        $('#' + container).addClass('loginFormSelected');
        $('.loginForms :text').focus();
    };








    function hidePopup(e) {

        var $src = $(e.srcElement);


        if ($.browser.mozilla) {
            $("#loginBox").click(function () { return false; });
            $(document).one("click", function () {
                $('#outerLoginBox').hide();
                boxOpen = false;
            });

        }
        else {
            if ($src.parents('#loginBox').length == 0 && $src[0].id != 'loginBox') {
                loginManager.superHidePopup();
            }
        }

    }

    this.superHidePopup = function () {
        $('#outerLoginBox').hide();
        boxOpen = false;
    }

    this.init = function () {
        this.hookupTabs();
        $('.lpLoginDropdown').click(this.toggleLoginBox);
        $(document).click(hidePopup);
    };

    this.hookupTabs = function () {
        $('#appButtonTable').find('td').click(onTabClick);
    };

    this.toggleLoginBox = function (e) {
        $('#outerLoginBox').toggle();
        boxOpen = !boxOpen;

        if (boxOpen) {
            loadedHtml = $('#loginBox').children().length > 0;
            if (!loadedHtml) {
                $.get('/pages/login.aspx' + window.location.search, showLoginCallback);
            }
            else {
                addPlaceholders();
            }

            $('.loginForms :text').focus();
            if (e !== undefined) {
                //    alert('undefined');
                e.stopPropagation();
            }
        }
    };

    function showLoginCallback(data) {
        $('#loginBox').html(data);
        loginManager.hookupTabs();
        addPlaceholders();
        loadedHtml = true;
        if ($.browser.mozilla) {
        
            $('.formLink').on('click', function (e) {
       //         alert('in click');
                var link = $(this).attr('href');
                window.open(link);
                return false;
            });
        }

    }
};

$.support.placeholder = (function () {
    var i = document.createElement('input');
    return 'placeholder' in i;
})();

$(document).ready(function () {
    loginManager.init();
});

function getCookies() {
    var allCookies = document.cookie.split(';');
    var ret = [];
    var key, value;
    for (var i = 0; i < allCookies.length; i++) {
        key = allCookies[i].substr(0, allCookies[i].indexOf("="));
        value = allCookies[i].substr(allCookies[i].indexOf("=") + 1);
        key = key.replace(/^\s+|\s+$/g, "");

        ret[key] = value;
    }

    return ret;
}