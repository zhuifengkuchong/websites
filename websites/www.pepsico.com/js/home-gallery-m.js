$(function() {
	// ======================= imagesLoaded Plugin ===============================
	// https://github.com/desandro/imagesloaded

	// $('#my-container').imagesLoaded(myFunction)
	// execute a callback when all images have loaded.
	// needed because .load() doesn't work on cached images

	// callback function gets image collection as argument
	//  this is the container

	// original: mit license. paul irish. 2010.
	// contributors: Oren Solomianik, David DeSandro, Yiannis Chatzikonstantinou

	$.fn.imagesLoaded 		= function( callback ) {
	var $images = this.find('img'),
		len 	= $images.length,
		_this 	= this,
		blank 	= 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==';

	function triggerCallback() {
		callback.call( _this, $images );
	}

	function imgLoaded() {
		if ( --len <= 0 && this.src !== blank ){
			setTimeout( triggerCallback );
			$images.unbind( 'load error', imgLoaded );
		}
	}

	if ( !len ) {
		triggerCallback();
	}

	$images.bind( 'load error',  imgLoaded ).each( function() {
		// cached images don't fire load sometimes, so we reset src.
		if (this.complete || this.complete === undefined){
			var src = this.src;
			// webkit hack from http://groups.google.com/group/jquery-dev/browse_thread/thread/eee6ab7b2da50e1f
			// data uri bypasses webkit log warning (thx doug jones)
			this.src = blank;
			this.src = src;
		}
	});

	return this;
	};

	// gallery container
	var $rgGallery			= $('#home-rg-gallery-m'),
	// carousel container
	$esCarousel			= $rgGallery.find('div.home-es-carousel-wrapper'),
	// the carousel items
	$items				= $esCarousel.find('ul > li'),
	// total number of items
	itemsCount			= $items.length;
	
	Gallery				= (function() {
			// index of the current item
		var current			= 0, 
			// mode : carousel || fullview
			mode 			= 'carousel',
			// control if one image is being loaded
			anim			= false,
			init			= function() {
				
				// (not necessary) preloading the images here...
				$items.add('<img src="images/ajax-loader.gif"/*tpa=http://www.pepsico.com/js/images/ajax-loader.gif*//><img src="images/black.png"/*tpa=http://www.pepsico.com/js/images/black.png*//>').imagesLoaded( function() {
					// add options
					_addViewModes();
					
					// add large image wrapper
					_addImageWrapper();
					
					// show first image
					_showImage( $items.eq( current ) );
						
				});
				
				// initialize the carousel
					if( mode === 'carousel' )
				_initCarousel();
				
			},
			_initCarousel	= function() {
				
				// we are using the elastislide plugin:
				// http://tympanus.net/codrops/2011/09/12/elastislide-responsive-carousel/
				$esCarousel.show().elastislide({
					imageW 	: 180,
					margin : 25,
					onClick	: function( $item ) {
						if( anim ) return false;
						anim	= true;
						// on click show image
						_showImage($item);
						// change current
						current	= $item.index();
					}
				});
				
				// set elastislide's current to current
				$esCarousel.elastislide('setCurrent', current);
				
			},
			_addViewModes	= function() {
				
				// top right buttons: hide / show carousel
				
				var $viewfull	= $('<a href="#" class="home-rg-view-full"></a>'),
					$viewthumbs	= $('<a href="#" class="home-rg-view-thumbs home-rg-view-selected"></a>');
				
				$rgGallery.prepend( $('<div class="home-rg-view"/>').append( $viewfull ).append( $viewthumbs ) );
				
				$viewfull.bind('click.rgGallery', function( event ) {
						if( mode === 'carousel' )
							$esCarousel.elastislide( 'destroy' );
						$esCarousel.hide();
					$viewfull.addClass('home-rg-view-selected');
					$viewthumbs.removeClass('home-rg-view-selected');
					mode	= 'fullview';
					return false;
				});
				
				$viewthumbs.bind('click.rgGallery', function( event ) {
					_initCarousel();
					$viewthumbs.addClass('home-rg-view-selected');
					$viewfull.removeClass('home-rg-view-selected');
					mode	= 'carousel';
					return false;
				});
				
					if( mode === 'fullview' )
						$viewfull.trigger('click');
					
			},
			_addImageWrapper= function() {
				
				// adds the structure for the large image and the navigation buttons (if total items > 1)
				// also initializes the navigation events
				
				$('#img-wrapper-tmpl-m').tmpl( {itemsCount : itemsCount} ).prependTo( $rgGallery ); //Added by us, appendto to prependto
				
				if( itemsCount > 1 ) {
					// addNavigation
					var $navPrev		= $rgGallery.find('.es-nav-prev'),
						$navNext		= $rgGallery.find('.es-nav-next'),
						$imgWrapper		= $rgGallery.find('div.home-rg-image');
						
					$navPrev.bind('click.rgGallery', function( event ) {
						_navigate( 'left' );
						return false;
					});	
					
					$navNext.bind('click.rgGallery', function( event ) {
						_navigate( 'right' );
						return false;
					});
				
					// add touchwipe events on the large image wrapper
					$imgWrapper.touchwipe({
						wipeLeft			: function() {
							_navigate( 'right' );
						},
						wipeRight			: function() {
							_navigate( 'left' );
						},
						preventDefaultEvents: false
					});
				
					$(document).bind('keyup.rgGallery', function( event ) {
						if (event.keyCode == 39)
							_navigate( 'right' );
						else if (event.keyCode == 37)
							_navigate( 'left' );	
					});
					
				}
				
			},
			_navigate		= function( dir ) {
				
				// navigate through the large images
				
				if( anim ) return false;
				anim	= true;
				
				if( dir === 'right' ) {
					if( current + 1 >= itemsCount ){
						current = 0;
					}
					else{
						++current;
					}
				}
				else if( dir === 'left' ) {
					if( current - 1 < 0 ){
						current = itemsCount - 1;
					}
					else{
						--current;
					}
				}
				
				_showImage( $items.eq( current ) );
				
			},
			_showImage		= function( $item ) {
				
				// shows the large image that is associated to the $item
				
				var $loader	= $rgGallery.find('div.home-rg-loading').show();
				
				$items.removeClass('selected');
				$item.addClass('selected');
					 
				var $thumb		= $item.find('img'),
					largesrc	= $thumb.data('large'),
					title		= $thumb.data('description');
					heading		= $thumb.data('heading');
					thumb1src	= $thumb.data('thumb1src');
					thumb1url	= $thumb.data('thumb1url');
					thumb2src	= $thumb.data('thumb2src');
					thumb2url	= $thumb.data('thumb2url');
					thumb3src	= $thumb.data('thumb3src');
					thumb3url	= $thumb.data('thumb3url');
					thumb4src	= $thumb.data('thumb4src');
					thumb4url	= $thumb.data('thumb4url');
					thumb5src	= $thumb.data('thumb5src');
					thumb5url	= $thumb.data('thumb5url');
					thumb6src	= $thumb.data('thumb6src');
					thumb6url	= $thumb.data('thumb6url');
					ctatext = $thumb.data('ctatext');
					ctaurl = $thumb.data('ctaurl');


				
				$('<img/>').load( function() {
					
				    $rgGallery.find('div.home-rg-image img#bigImg-m').attr('src', largesrc);
					
					if(title)
						$rgGallery.find('div.home-rg-caption').show().children('p').empty().text( title );
						$rgGallery.find('div.home-rg-caption').children('h4').empty().text( heading );
						$rgGallery.find('div.home-rg-caption').children('ul.thumblist').empty();
						$rgGallery.find('div.home-rg-caption').children('div.home-rg-cta').empty();

					if(thumb1src)
						$rgGallery.find('div.home-rg-caption').children('ul.thumblist').append('<li><a target="_blank" href="'+ thumb1url +'"><img src="'+ thumb1src +'" alt=""/></a></li>');
					if(thumb2src)
						$rgGallery.find('div.home-rg-caption').children('ul.thumblist').append('<li><a target="_blank" href="'+ thumb2url +'"><img src="'+ thumb2src +'" alt=""/></a></li>');
					if(thumb3src)
						$rgGallery.find('div.home-rg-caption').children('ul.thumblist').append('<li><a target="_blank" href="'+ thumb3url +'"><img src="'+ thumb3src +'" alt=""/></a></li>');
					if(thumb4src)
						$rgGallery.find('div.home-rg-caption').children('ul.thumblist').append('<li><a target="_blank" href="'+ thumb4url +'"><img src="'+ thumb4src +'" alt=""/></a></li>');
					if(thumb5src)
						$rgGallery.find('div.home-rg-caption').children('ul.thumblist').append('<li><a target="_blank" href="'+ thumb5url +'"><img src="'+ thumb5src +'" alt=""/></a></li>');
					if(thumb6src)
					    $rgGallery.find('div.home-rg-caption').children('ul.thumblist').append('<li><a target="_blank" href="' + thumb6url + '"><img src="' + thumb6src + '" alt=""/></a></li>');

					if (ctatext)
					    $rgGallery.find('div.home-rg-caption').append('<div class="home-rg-cta"><a class="btnLink blue" target="_blank" href="' + ctaurl + '">' + ctatext + '</a></div>');
					$loader.hide();
					
					if( mode === 'carousel' ) {
						$esCarousel.elastislide( 'reload' );
						$esCarousel.elastislide( 'setCurrent', current );
					}
					
					anim	= false;
					
				}).attr( 'src', largesrc );
				
			};
		
		return { init : init };
	
	})();

	Gallery.init();
});