/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {


// To understand behaviors, see https://drupal.org/node/756722#behaviors
Drupal.behaviors.coremark = {
  attach: function(context, settings) {

  	var Site = {

  		init : function() {

	  		if ($('body').hasClass('front') || $('body').hasClass('section-careers')) { 
	          Slider.init($('#block-views-homepage-slider-block'));
	  		}
	        if ($('body').hasClass('section-retailer-programs')) {
	          Slider.init($('#block-views-programs-slider-block'));
	        }
	        if ($('body').hasClass('section-retailer-solutions')) {
	          Slider.init($('#block-views-programs-slider-block-1'));
	        }
	        if ($('body').hasClass('node-type-retailer-childpage')) {
	          var title = $('#page-title').text();
	          $('.d-page-contact span').text(title);
	        }         

  			$('#block-menu-menu-retailer ul.menu li').hover(function(){
  				$('> a', this).addClass('hovered');
  				$('http://www.core-mark.com/sites//all//themes//coremark//js//ul.menu', this).stop('true','true').slideDown('fast');	
  			}, function() {
  				var prxy = $(this);
  				$('http://www.core-mark.com/sites//all//themes//coremark//js//ul.menu', this).slideUp('fast', function(){
  					$(this).prev().removeClass('hovered');
  				});	
  			});

  		},

      contact : function() {

        $('.webform-client-form .form-submit').before('<div class="required-text">*Required fields</div>');

        $('.webform-client-form .form-text,.webform-client-form .form-textarea').focus(function(){
          var value = $(this).val();
          $(this).parents('.form-item').find('label').hide();
        });

        $('.webform-client-form .form-text,.webform-client-form .form-textarea').blur(function(){
          var value = $(this).val();

          if (value == '') {
            $(this).parents('.form-item').find('label').show();  
          } else {
            $(this).parents('.form-item').find('label').hide();
          }

        });

      }

  	}

  	var Slider = {

  		init : function(block) {


  			var sliderWidth = block.width(),
  				items		= $('.slider li', block).length,
  				counter		= 0;

  			if (items > 1) {

	  			$('.slider li:first-child', block).addClass('active');
	  			block.after('<div class="pointer"><ul></ul></div>');

	  			$('.slider li', block).each(function(){
	  				$('.pointer ul').append('<li></li>');
	  			});

	  			$('.pointer li:first-child').addClass('active');

	  			setInterval(function(){

	  				$('.pointer li').removeClass('active');

	  				$('.slider li.active', block).animate({
	  					'opacity': '0'
	  				}, 1e3, function(){
	  					$(this).removeClass('active');
	  				});

	  				if (counter == items-1) {
	  					
	  					$('.pointer li:first-child').addClass('active');

	  					$('.slider li:first-child', block).animate({
		  					'opacity':'1'
		  				}, 1e3, function(){
		  					$(this).addClass('active');	
		  					counter = 0;
		  				});

	  				} else {

	  					$('.pointer li:nth-child('+(counter+2)+')').addClass('active');

	  					$('.slider li:nth-child('+(counter+2)+')', block).animate({
		  					'opacity':'1'
		  				}, 1e3, function(){
		  					$(this).addClass('active');
		  					counter++;
		  				});

	  				}

	  			},1e4);

  			}

  			

  		}

  	}

    $(document).ready(function(){
    	Site.init();
		$('.page-node-32 .field-name-field-banner-header .field-items .field-item').html('<a class="iframe" href="http://www.core-mark.com/sites/all/themes/coremark/map-only.html">'+ jQuery('.field-name-field-banner-header .field-items .field-item').html() +'</a>');
		$(".iframe").colorbox({iframe:true, width:"90%", height:"90%"});	
		//$('.page-node-59 .field-name-field-banner-header .field-items .field-item').html('<div id="dialog" style="display:none;"></div>'+ jQuery('.field-name-field-banner-header .field-items .field-item').html() );
		/*$('.page-node-59 .field-name-field-banner-header .field-items .field-item').click(function() {        
			//$('#dialog').append('<iframe src="http://core-mark.com/map-only.html?keepThis=true"></iframe>').dialog();
			$(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});		
			return false;
		});*/    
	});

  }
};


})(jQuery, Drupal, this, this.document);