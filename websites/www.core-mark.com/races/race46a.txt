<script id = "race46a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race46={};
	myVars.races.race46.varName="Lu_Id_li_16__onmouseout";
	myVars.races.race46.varType="@varType@";
	myVars.races.race46.repairType = "@RepairType";
	myVars.races.race46.event1={};
	myVars.races.race46.event2={};
	myVars.races.race46.event1.id = "Lu_DOM";
	myVars.races.race46.event1.type = "onDOMContentLoaded";
	myVars.races.race46.event1.loc = "Lu_DOM_LOC";
	myVars.races.race46.event1.isRead = "False";
	myVars.races.race46.event1.eventType = "@event1EventType@";
	myVars.races.race46.event2.id = "Lu_Id_li_16";
	myVars.races.race46.event2.type = "onmouseout";
	myVars.races.race46.event2.loc = "Lu_Id_li_16_LOC";
	myVars.races.race46.event2.isRead = "True";
	myVars.races.race46.event2.eventType = "@event2EventType@";
	myVars.races.race46.event1.executed= false;// true to disable, false to enable
	myVars.races.race46.event2.executed= false;// true to disable, false to enable
</script>

