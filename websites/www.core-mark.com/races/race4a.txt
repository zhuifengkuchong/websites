<script id = "race4a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race4={};
	myVars.races.race4.varName="Lu_Id_li_6__onmouseover";
	myVars.races.race4.varType="@varType@";
	myVars.races.race4.repairType = "@RepairType";
	myVars.races.race4.event1={};
	myVars.races.race4.event2={};
	myVars.races.race4.event1.id = "Lu_DOM";
	myVars.races.race4.event1.type = "onDOMContentLoaded";
	myVars.races.race4.event1.loc = "Lu_DOM_LOC";
	myVars.races.race4.event1.isRead = "False";
	myVars.races.race4.event1.eventType = "@event1EventType@";
	myVars.races.race4.event2.id = "Lu_Id_li_9";
	myVars.races.race4.event2.type = "onmouseover";
	myVars.races.race4.event2.loc = "Lu_Id_li_9_LOC";
	myVars.races.race4.event2.isRead = "True";
	myVars.races.race4.event2.eventType = "@event2EventType@";
	myVars.races.race4.event1.executed= false;// true to disable, false to enable
	myVars.races.race4.event2.executed= false;// true to disable, false to enable
</script>

