<script id = "race10a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race10={};
	myVars.races.race10.varName="Object[12263].pause";
	myVars.races.race10.varType="@varType@";
	myVars.races.race10.repairType = "@RepairType";
	myVars.races.race10.event1={};
	myVars.races.race10.event2={};
	myVars.races.race10.event1.id = "ft-slideshowHolder10";
	myVars.races.race10.event1.type = "onmouseover";
	myVars.races.race10.event1.loc = "ft-slideshowHolder10_LOC";
	myVars.races.race10.event1.isRead = "False";
	myVars.races.race10.event1.eventType = "@event1EventType@";
	myVars.races.race10.event2.id = "ft-title-slideshowHolder";
	myVars.races.race10.event2.type = "onmouseover";
	myVars.races.race10.event2.loc = "ft-title-slideshowHolder_LOC";
	myVars.races.race10.event2.isRead = "False";
	myVars.races.race10.event2.eventType = "@event2EventType@";
	myVars.races.race10.event1.executed= false;// true to disable, false to enable
	myVars.races.race10.event2.executed= false;// true to disable, false to enable
</script>

