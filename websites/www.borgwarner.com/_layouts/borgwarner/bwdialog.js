function showDialog(locPage) {
    var options = { url: locPage, width: 550, height: 600, dialogReturnValueCallback: null };
    var m = SP.UI.ModalDialog.showModalDialog(options);
    return false;
}
//For products silverlight page
function showProductDialog(locPage) {
    var options = { url: locPage, width: 600, height: 600, dialogReturnValueCallback: null, allowMaximize: false };
    var m = SP.UI.ModalDialog.showModalDialog(options);

    ModifyButtonImage();

    return false;
}

//For products silverlight page
function showProductDialogV2(locPage) {


    var options = SP.UI.$create_DialogOptions();
    options.url = locPage;
    options.width = 600;
    options.height = 600;
    options.dialogReturnValueCallback = Function.createDelegate(null, CloseCallback);
    options.allowMaximize = false;
    
    //{ url: locPage, width: 600, height: 600,
    //allowMaximize: false };
    
    var m = SP.UI.ModalDialog.showModalDialog(options);

    ModifyButtonImage();

    return false;
}

function showProductDialogV3(locPage) {


    var options = SP.UI.$create_DialogOptions();
    options.url = locPage;
    //options.width = 600;
    //options.height = 600;
    options.autosize = true;
    options.autoSize = true;
    options.dialogReturnValueCallback = Function.createDelegate(null, CloseCallback);
    options.allowMaximize = false;

    //{ url: locPage, width: 600, height: 600,
    //allowMaximize: false };

    var m = SP.UI.ModalDialog.showModalDialog(options);

    ModifyButtonImage();

    return false;
}

function showProductDialogV4(locPage, popWidth) {


    var options = SP.UI.$create_DialogOptions();
    options.url = locPage;
    options.width = popWidth;
    //options.height = 600;
    options.dialogReturnValueCallback = Function.createDelegate(null, CloseCallback);
    options.allowMaximize = false;

    //{ url: locPage, width: 600, height: 600,
    //allowMaximize: false };

    var m = SP.UI.ModalDialog.showModalDialog(options);

    ModifyButtonImage();

    return false;
}

function CloseCallback(result, target) {

    //alert(target);
    if (result === SP.UI.DialogResult.OK) {
        window.location = target;
        //alert("Dialog Closed");
        //messageId = SP.UI.Notify.addNotification("<img src='Unknown_83_filename'/*tpa=http://www.borgwarner.com/_layouts/borgwarner/_layouts/images/loading.gif*/> Chuck stop clicking this link. <b>" + target + "</b>...", true, "Dialog response", null);
        //window.location = target;
    }
    if (result === SP.UI.DialogResult.cancel) {
        //alert("User cancelled");
    } 
}

//For locations silverlight page
function showLocationDialog(locPage) {
    var options = { url: locPage, width: 550, height: 550, dialogReturnValueCallback: null, allowMaximize: false };
    var m = SP.UI.ModalDialog.showModalDialog(options);

    ModifyButtonImage();

    return false;
}

function showCustomDialog(locPage, cWidth, cHeight) {
    if (isNaN(cWidth))
        cWidth = 550;
    if (isNaN(cHeight))
        cHeight = 600;
    var options = { url: locPage, width: cWidth, height: cHeight, dialogReturnValueCallback: null, allowMaximize: false, showClose: true};
    var m = SP.UI.ModalDialog.showModalDialog(options);

    ModifyButtonImage();

    return false;
}
function showCustomDialogClean(locPage, cWidth, cHeight) {
    if (isNaN(cWidth))
        cWidth = 550;
    if (isNaN(cHeight))
        cHeight = 600;
    var options = { url: locPage, width: cWidth, height: cHeight, dialogReturnValueCallback: null, allowMaximize: false, showClose: false};
    var m = SP.UI.ModalDialog.showModalDialog(options);
    return false;
}

function ModifyButtonImage()
{

    $(".ms-dlgCloseBtnImg").attr('src', '../images/fgimg1.png'/*tpa=http://www.borgwarner.com/_layouts/images/fgimg1.png*/);

//$('.ms-dlgContent').css({'background':'#fff;', 'border': '0'});
//$('.ms-dlgBorder').css({'border': '0'});
//$('.ms-dlgTitleBtns').css({'POSITION': 'absolute', 'PADDING-RIGHT': '2px', 'FLOAT': 'right', 'RIGHT': '-12px', 'PADDING-TOP': '2px', 'TOP': '-12px'});
//$('.ms-dlgTitle').css({'border': '0', 'MARGIN': '0px', 'padding' : '0', 'WHITE-SPACE': 'nowrap', 'HEIGHT': '0px', 'OVERFLOW': 'hidden', 'CURSOR': 'default' });
}