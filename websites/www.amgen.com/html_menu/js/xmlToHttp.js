// JavaScript Document
var xmlhttp;
var globalMenuTxt = '';
var mainMenuTxt = '';
var newstxt = '<b style="color:#007EC0;">IN THE NEWS</b>';
var globalSubitemTxt = new Array();
var menuTxt = '';
var subMenuTxt = '';
function loadXMLDoc(url, which) {
 xmlhttp=null;
 if (window.XMLHttpRequest)  {// code for IE7, Firefox, Mozilla, etc.
   xmlhttp=new XMLHttpRequest();
 } else if (window.ActiveXObject) {// code for IE5, IE6
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
 }
 if (xmlhttp!=null) {
  loadThis = eval('onResponse_' + which)
  xmlhttp.onreadystatechange=loadThis;
  xmlhttp.open("GET",url,true);
  xmlhttp.send(null);
 } else  {
    alert("Your browser does not support XMLHTTP.");
 }
}
function onResponse_menu() {
 
 if(xmlhttp.readyState!=4) return;
 if(xmlhttp.status!=200) {
  alert("Problem retrieving XML data. " + xmlhttp.status);
  return;
 }
 
 
 // deleted functions to pull global menus. Now using html_menu.js
 
 
 //now we can load the next XML file for news items 
 loadXMLDoc('http://www.amgen.com/home_featured.xml', 'featured');
}
function onResponse_news() {
 if(xmlhttp.readyState!=4) return;
 if(xmlhttp.status!=200) {
  alert("Problem retrieving XML data. " + xmlhttp.status);
  return;
 }
 
 $(document).ready(function(){
 $.ajax({
  type: "GET",
  url: "http://www.amgen.com/home_news.xml",
  dataType: "xml",
  success: function(xml) {
   $('#news').append('<p class="news_header">NEWS RELEASES</p>');
   $(xml).find('article').each(function(){
    var date = $(this).attr('date');
    var title = $(this).attr('title');
    var url = $(this).attr('url');
    $('#news').append('<p><strong>'+date+'</strong>&nbsp;/&nbsp; <a href="'+url+'">'+title+'</a></p>');
   });
   $('#news').append('<br><p class="moreNews"><a class="moreNews" href="http://www.amgen.com/media/pr.jsp">More Amgen News</a></p>');
  }
 });
 });
 
 
}
function onResponse_featured() {
 if(xmlhttp.readyState!=4) return;
 if(xmlhttp.status!=200) {
  alert("Problem retrieving XML data. " + xmlhttp.status);
  return;
 }
 
 featured=xmlhttp.responseXML.documentElement.getElementsByTagName("feature");
 for (i=0;i<featured.length;i++)  {
  headline=featured[i].getElementsByTagName("headline")
  subhead=featured[i].getElementsByTagName("subhead")
  text=featured[i].getElementsByTagName("text")
  url=featured[i].getElementsByTagName("url")
  pic=featured[i].getElementsByTagName("pic")
     {
      try {
    if (i==0) {
  //alert(headline[0].firstChild.nodeValue);
        //document.getElementById('featured'+i+'Headline').innerHTML = '<h1><a href="' + url[0].firstChild.nodeValue + '" class="mainHeadlineReadMore">' + headline[0].firstChild.nodeValue + '<span class="mainHeadlineSubhead">' + subhead[0].firstChild.nodeValue + '</span>' + text[0].firstChild.nodeValue + '</a></h1>';
        document.getElementById('featured'+i+'Headline').innerHTML = headline[0].firstChild.nodeValue;    } else {
        document.getElementById('featured'+i+'Headline').innerHTML =  '<a href="' + url[0].firstChild.nodeValue + '"><img src="' + pic[0].firstChild.nodeValue + '" border="0" /></a><br /> <div class="featuredHeadline"><a href="' + url[0].firstChild.nodeValue + '">' + headline[0].firstChild.nodeValue + '</a></div>';
        document.getElementById('featured'+i+'Text').innerHTML = '<a href="' + url[0].firstChild.nodeValue + '">' + text[0].firstChild.nodeValue + '</a>';
    }
      } catch (er) {
         
        }
     }
 }
 
 //now we can load the next XML file for featured items 
 loadXMLDoc('http://www.amgen.com/home_news.xml', 'news');
}