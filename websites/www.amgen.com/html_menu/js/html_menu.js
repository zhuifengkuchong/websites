// JavaScript Document
var menu = " <div id=\"header\"> " +
"    <div class=\"section clearfix\">   " +
"      <div class=\"region region-header\">   " +
"        <div id=\"block-superfish-3\" class=\"block block-superfish first odd\">   " +
"          <div class=\"content\">   " +
"           <ul id=\"superfish-3\" class=\"sf-menu menu-global-nav sf-horizontal sf-style-light-blue sf-total-items-7 sf-parent-items-3 sf-single-items-4\">   " +
"              <li id=\"menu-698-3\" class=\"first odd sf-item-1 sf-depth-1 sf-total-children-8 sf-parent-children-0 sf-single-children-8 menuparent\"><a href=\"http://www.amgen.com/about/overview.html\" title=\"\" class=\"sf-depth-1  menuparent\">About Amgen</a>   " +
"                <ul>   " +
"                  <li id=\"menu-712-3\" class=\"first odd sf-item-1 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/about/mission_values.html\" title=\"\" class=\"sf-depth-2 \">Mission &amp; Values</a></li>   " +
"                  <li id=\"menu-713-3\" class=\"middle even sf-item-2 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/about/leadership_team.html\" title=\"\" class=\"sf-depth-2 \">Leadership</a></li>   " +
"                  <li id=\"menu-714-3\" class=\"middle odd sf-item-3 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/about/corporate_governance.html\" title=\"\" class=\"sf-depth-2 \">Corporate Governance</a></li>   " +
"                  <li id=\"menu-715-3\" class=\"middle even sf-item-4 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/about/corporate_compliance.html\" title=\"\" class=\"sf-depth-2 \">Worldwide Compliance &amp; Business Ethics</a></li>   " +
"                  <li id=\"menu-716-3\" class=\"middle odd sf-item-5 sf-depth-2 sf-no-children\"><a href=\"http://www.amgenhistory.com/\" title=\"\" class=\"sf-depth-2 \">History</a></li>   " +
"                  <li id=\"menu-716-3\" class=\"middle odd sf-item-5 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/about/fact_sheets.html\" title=\"\" class=\"sf-depth-2 \">Fact Sheets</a></li>   " +
"                  <li id=\"menu-717-3\" class=\"middle even sf-item-6 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/about/locations.html\" title=\"\" class=\"sf-depth-2 \">Global Locations</a></li>   " +
"                  <li id=\"menu-718-3\" class=\"middle odd sf-item-7 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/about/environment/index.html\" title=\"\" class=\"sf-depth-2 \">Environmental Sustainability</a></li>   " +
"                  <li id=\"menu-719-3\" class=\"last even sf-item-8 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/about/amgen_policies.html\" title=\"\" class=\"sf-depth-2 \">Policies, Practices & Disclosures</a></li>   " +
"                </ul>   " +
"              </li>   " +
"              <li id=\"menu-691-3\" class=\"middle even sf-item-2 sf-depth-1 sf-total-children-5 sf-parent-children-0 sf-single-children-5 menuparent\"><a href=\"http://www.amgen.com/science/science.html\" title=\"\" class=\"sf-depth-1  menuparent\">Science</a>   " +
"                <ul>    " +
"                  <li id=\"menu-692-3\" class=\"first odd sf-item-1 sf-depth-2 sf-no-children\"><a href=\"http://www.amgenpipeline.com/pipeline/\" title=\"\" class=\"sf-depth-2 \">Pipeline</a></li>   " +
"                  <li id=\"menu-693-3\" class=\"middle even sf-item-2 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/science/clinical_trials.html\" title=\"\" class=\"sf-depth-2 \">Clinical Trials</a></li>   " +
"                  <li id=\"menu-694-3\" class=\"middle odd sf-item-3 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/science/ethical_research.html\" title=\"\" class=\"sf-depth-2 \">Ethical Research</a></li>   " +
"                  <li id=\"menu-695-3\" class=\"middle even sf-item-4 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/science/researchanddevelopmentstrategy.html\" title=\"\" class=\"sf-depth-2 \">Research and Development Strategy</a></li>   " +
"                  <li id=\"menu-695-3\" class=\"middle even sf-item-4 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/science/amgen_biosimilars.html\" title=\"\" class=\"sf-depth-2 \">Biosimilars</a></li>   " +
"                  <li id=\"menu-696-3\" class=\"last odd sf-item-5 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/science/web_resources.html\" title=\"\" class=\"sf-depth-2 \">Web Resources</a></li>   " +
"                </ul>   " +
"              </li>   " +
"              <li id=\"menu-699-3\" class=\"middle odd sf-item-3 sf-depth-1 sf-total-children-9 sf-parent-children-0 sf-single-children-9 menuparent\"><a href=\"http://www.amgen.com/citizenship/corporate_philanthropy.html\" title=\"\" class=\"sf-depth-1  menuparent\">Corporate Giving</a>   " +
"                <ul>   " +
"                  <li id=\"menu-720-3\" class=\"first odd sf-item-1 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/citizenship/foundation.html\" title=\"\" class=\"sf-depth-2 \">Amgen Foundation</a></li>   " +
"                  <li id=\"menu-728-3\" class=\"middle even sf-item-2 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/citizenship/apply_for_grant.html\" title=\"\" class=\"sf-depth-2 \">Apply for Foundation Grant</a></li>   " +
"                  <li id=\"menu-726-3\" class=\"middle even sf-item-4 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/citizenship/safetynet_foundation.html\" title=\"\" class=\"sf-depth-2 \">The Safety Net Foundation</a></li>   " +
"                  <li id=\"menu-725-3\" class=\"middle odd sf-item-5 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/citizenship/encourage_foundation.html\" title=\"\" class=\"sf-depth-2 \">ENcourage Foundation&reg;</a></li>   " +
"                  <li id=\"menu-724-3\" class=\"middle even sf-item-6 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/citizenship/IME_overview.html\" title=\"\" class=\"sf-depth-2 \">Independent Medical Education</a></li>   " +
"                  <li id=\"menu-723-3\" class=\"middle odd sf-item-7 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/citizenship/amgen_med_overview.html\" title=\"\" class=\"sf-depth-2 \">Healthcare Donations</a></li>   " +
"                  <li id=\"menu-723-3\" class=\"middle odd sf-item-7 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/citizenship/charitable_donations_sponsorships.html\" title=\"\" class=\"sf-depth-2 \">Non-Healthcare Donations</a></li>   " +
"                  <li id=\"menu-722-3\" class=\"middle even sf-item-8 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/citizenship/donation_list_overview.html\" title=\"\" class=\"sf-depth-2 \">Donation &amp; Grant Recipient List</a></li>   " +
"                  <li id=\"menu-721-3\" class=\"last odd sf-item-9 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/citizenship/amgen_equipment_donations.html\" title=\"\" class=\"sf-depth-2 \">Amgen Equipment Donations</a></li>   " +
"                </ul>   " +
"              </li>   " +
"              <li id=\"menu-699-3\" class=\"middle odd sf-item-3 sf-depth-1 sf-total-children-9 sf-parent-children-0 sf-single-children-9 menuparent\"><a href=\"\">Country Sites</a>   " +
"                <ul>   " +
"                  <li id=\"menu-720-3\" class=\"first odd sf-item-1 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com.au/\" title=\"\" class=\"sf-depth-2 \">Australia</a></li>   " +
"                  <li id=\"menu-720-3\" class=\"first odd sf-item-1 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.at/\" title=\"\" class=\"sf-depth-2 \">Austria</a></li>   " +
"                  <li id=\"menu-720-3\" class=\"first odd sf-item-1 sf-depth-2 sf-no-children\"><a href=\"http://www.amgenbrasil.com.br/\" title=\"\" class=\"sf-depth-2 \">Brazil</a></li>   " +
"                  <li id=\"menu-720-3\" class=\"first odd sf-item-1 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.bg/\" title=\"\" class=\"sf-depth-2 \">Bulgaria</a></li>   " +
"                  <li id=\"menu-720-3\" class=\"first odd sf-item-1 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.ca/\" title=\"\" class=\"sf-depth-2 \">Canada</a></li>   " +
"                  <li id=\"menu-720-3\" class=\"first odd sf-item-1 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.cn/\" title=\"\" class=\"sf-depth-2 \">China</a></li>   " +
"                  <li id=\"menu-720-3\" class=\"first odd sf-item-1 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.cz/\" title=\"\" class=\"sf-depth-2 \">Czech Republic</a></li>   " +
"                  <li id=\"menu-720-3\" class=\"first odd sf-item-1 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.dk/\" title=\"\" class=\"sf-depth-2 \">Denmark</a></li>   " +
"                  <li id=\"menu-720-3\" class=\"first odd sf-item-1 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.fi/\" title=\"\" class=\"sf-depth-2 \">Finland</a></li>   " +
"                  <li id=\"menu-720-3\" class=\"first odd sf-item-1 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.fr/\" title=\"\" class=\"sf-depth-2 \">France</a></li>   " +
"                  <li id=\"menu-720-3\" class=\"first odd sf-item-1 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.de/\" title=\"\" class=\"sf-depth-2 \">Germany</a></li>   " +
"                  <li id=\"menu-720-3\" class=\"first odd sf-item-1 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.gr/\" title=\"\" class=\"sf-depth-2 \">Greece</a></li>   " +
"                  <li id=\"menu-720-3\" class=\"first odd sf-item-1 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.co.hu/\" title=\"\" class=\"sf-depth-2 \">Hungary</a></li>   " +
"                  <li id=\"menu-720-3\" class=\"first odd sf-item-1 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.it/\" title=\"\" class=\"sf-depth-2 \">Italy</a></li>   " +
"                  <li id=\"menu-720-3\" class=\"first odd sf-item-1 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.jp/\" title=\"\" class=\"sf-depth-2 \">Japan</a></li>   " +
"                  <li id=\"menu-720-3\" class=\"first odd sf-item-1 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.mx/\" title=\"\" class=\"sf-depth-2 \">Mexico</a></li>   " +
"                  <li id=\"menu-720-3\" class=\"first odd sf-item-1 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.nl/\" title=\"\" class=\"sf-depth-2 \">Netherlands</a></li>   " +
"                  <li id=\"menu-720-3\" class=\"first odd sf-item-1 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.no/\" title=\"\" class=\"sf-depth-2 \">Norway</a></li>   " +
"                  <li id=\"menu-720-3\" class=\"first odd sf-item-1 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.pl/\" title=\"\" class=\"sf-depth-2 \">Poland</a></li>   " +
"                  <li id=\"menu-720-3\" class=\"first odd sf-item-1 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.pt/\" title=\"\" class=\"sf-depth-2 \">Portugal</a></li>   " +
"                  <li id=\"menu-720-3\" class=\"first odd sf-item-1 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.ro/\" title=\"\" class=\"sf-depth-2 \">Romania</a></li>   " +
"                  <li id=\"menu-720-3\" class=\"first odd sf-item-1 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.ru/\" title=\"\" class=\"sf-depth-2 \">Russia</a></li>   " +
"                  <li id=\"menu-720-3\" class=\"first odd sf-item-1 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.sk/\" title=\"\" class=\"sf-depth-2 \">Slovakia</a></li>   " +
"                  <li id=\"menu-720-3\" class=\"first odd sf-item-1 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.si/\" title=\"\" class=\"sf-depth-2 \">Slovenia</a></li>   " +
"                  <li id=\"menu-720-3\" class=\"first odd sf-item-1 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.es/\" title=\"\" class=\"sf-depth-2 \">Spain</a></li>   " +
"                  <li id=\"menu-720-3\" class=\"first odd sf-item-1 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.se/\" title=\"\" class=\"sf-depth-2 \">Sweden</a></li>   " +
"                  <li id=\"menu-720-3\" class=\"first odd sf-item-1 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.ch/\" title=\"\" class=\"sf-depth-2 \">Switzerland</a></li>   " +
"                  <li id=\"menu-720-3\" class=\"first odd sf-item-1 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com.tr/\" title=\"\" class=\"sf-depth-2 \">Turkey</a></li>   " +
"                  <li id=\"menu-720-3\" class=\"first odd sf-item-1 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.co.uk/\" title=\"\" class=\"sf-depth-2 \">UK and Ireland</a></li>   " +
"                </ul>   " +
"              </li>   " +
"              <li id=\"menu-697-3\" class=\"middle even sf-item-4 sf-depth-1 sf-no-children\"><a href=\"http://www.amgen.com/sitemap.html\" title=\"\" class=\"sf-depth-1 \">Sitemap</a></li>   " +
"              <li id=\"menu-701-3\" class=\"middle odd sf-item-5 sf-depth-1 sf-no-children\"><a href=\"http://www.amgen.com/privacy/privacy_terms.html\" title=\"\" class=\"sf-depth-1 \">Privacy &amp; Terms</a></li>   " +
"              <li id=\"menu-700-3\" class=\"middle even sf-item-6 sf-depth-1 sf-no-children\"><a href=\"http://www.amgen.com/search.html\" title=\"\" class=\"sf-depth-1 \">Search</a></li>   " +
"              <li id=\"menu-702-3\" class=\"last odd sf-item-7 sf-depth-1 sf-no-children\"><a href=\"http://www.amgen.com/contact_us/amgen_contact.html\" title=\"\" class=\"sf-depth-1 \">Contact Us</a></li>   " +
"            </ul>   " +
"          </div>   " +
"        </div>   " +
"        <!-- /.block -->   " +
"        <div id=\"block-block-9\" class=\"block block-block even\">   " +
"          <div id=\"UpperContainer-HTML\"><div id=\"amgenLogo\"><a href=\"../../index.htm\"/*tpa=http://www.amgen.com/*/><img src=\"../../files/Amgen-35_logo_wht_vertical.png\"/*tpa=http://www.amgen.com/files/Amgen-35_logo_wht_vertical.png*/></a></div></div>   " +
"        </div>   " +
"        <!-- /.block -->   " +
"        <div id=\"block-superfish-1\" class=\"block block-superfish last odd\">   " +
"          <div class=\"content\">   " +
"            <ul id=\"superfish-1\" class=\"sf-menu menu-global-nav---2nd-level sf-horizontal sf-style-light-blue sf-total-items-6 sf-parent-items-6 sf-single-items-0\">   " +
"              <li id=\"menu-706-1\" class=\"sf-item-1 sf-depth-1 sf-total-children-4 sf-parent-children-0 sf-single-children-4 menuparent\"><a href=\"http://www.amgen.com/patients/patients.html\" title=\"\" class=\"sf-depth-1  menuparent\">Patients</a>   " +
"                <ul>   " +
"                 <li id=\"menu-729-1\" class=\"sf-item-1 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/patients/products.html\" title=\"\" class=\"sf-depth-2 \">Products</a></li>   " +
"                 <li id=\"menu-730-1\" class=\"sf-item-2 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/patients/clinical_trials.html\" title=\"\" class=\"sf-depth-2 \">Clinical Trials</a></li>   " +
"                  <li id=\"menu-731-1\" class=\"sf-item-3 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/patients/resources.html\" title=\"\" class=\"sf-depth-2 \">Resources</a></li>   " +
"                  <li id=\"menu-732-1\" class=\"sf-item-4 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/patients/patient_access.html\" title=\"\" class=\"sf-depth-2 \">Amgen Assist&reg;</a></li>   " +
"                  <li id=\"menu-732-1\" class=\"sf-item-5 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/patients/medicinesafety/medicineSafety.html\" title=\"\" class=\"sf-depth-2 \">Medicine Safety</a></li>   " +
"                </ul>   " +
"              </li>   " +
"              <li id=\"menu-707-1\" class=\"sf-item-2 sf-depth-1 sf-total-children-6 sf-parent-children-0 sf-single-children-6 menuparent\"><a href=\"http://www.amgen.com/medpro/overview.html\" title=\"\" class=\"sf-depth-1  menuparent\">Medical Professionals</a>   " +
"                <ul>   " +
"                  <li id=\"menu-738-1\" class=\"sf-item-1 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/medpro/products.html\" title=\"\" class=\"sf-depth-2 \">Products</a></li>   " +
"                  <li id=\"menu-737-1\" class=\"sf-item-2 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/medpro/clinical_trials.html\" title=\"\" class=\"sf-depth-2 \">Clinical Trials</a></li>   " +
"                  <li id=\"menu-736-1\" class=\"sf-item-3 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/medpro/resources.html\" title=\"\" class=\"sf-depth-2 \">Resources</a></li>   " +
"                  <li id=\"menu-735-1\" class=\"sf-item-4 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/medpro/research_guiding_principles.html\" title=\"\" class=\"sf-depth-2 \">R&amp;D Guiding Principles</a></li>   " +
"                  <li id=\"menu-734-1\" class=\"sf-item-5 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/medpro/patient_access.html\" title=\"\" class=\"sf-depth-2 \">Amgen Assist&reg;</a></li>   " +
"                  <li id=\"menu-733-1\" class=\"sf-item-6 sf-depth-2 sf-no-children\"><a href=\"https://www.amgenmedinfo.com/Home\" title=\"\" class=\"sf-depth-2 \">Medical Information</a></li>   " +
"                </ul>   " +
"              </li>   " +
"              <li id=\"menu-708-1\" class=\"sf-item-3 sf-depth-1 sf-total-children-5 sf-parent-children-0 sf-single-children-5 menuparent\"><a href=\"http://www.amgen.com/partners/overview.html\" title=\"\" class=\"sf-depth-1  menuparent\">Partners</a>   " +
"                <ul>   " +
"                  <li id=\"menu-743-1\" class=\"sf-item-1 sf-depth-2 sf-no-children\"><a href=\"https://www.amgenbd.com/\" title=\"\" class=\"sf-depth-2 \">Business Development</a></li>   " +
"                  <li id=\"menu-742-1\" class=\"sf-item-2 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/partners/research.html\" title=\"\" class=\"sf-depth-2 \">Extramural Research</a></li>   " +
"                  <li id=\"menu-741-1\" class=\"sf-item-3 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/partners/amgen_ventures.html\" title=\"\" class=\"sf-depth-2 \">Amgen Ventures</a></li>   " +
"                  <li id=\"menu-740-1\" class=\"sf-item-4 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/partners/suppliers.html\" title=\"\" class=\"sf-depth-2 \">Suppliers</a></li>   " +
"                  <li id=\"menu-739-1\" class=\"sf-item-5 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/partners/us_wholesalers.html\" title=\"\" class=\"sf-depth-2 \">Wholesalers</a></li>   " +
"                </ul>   " +
"              </li>   " +
"              <li id=\"menu-709-1\" class=\"sf-item-4 sf-depth-1 sf-total-children-12 sf-parent-children-0 sf-single-children-12 menuparent\"><a href=\"http://investors.amgen.com/phoenix.zhtml?c=61656&p=irol-IRHome\" title=\"\" class=\"sf-depth-1  menuparent\">Investors</a>   " +
"                <ul>   " +
"                  <li id=\"menu-744-1\" class=\"sf-item-1 sf-depth-2 sf-no-children\"><a href=\"http://investors.amgen.com/phoenix.zhtml?c=61656&p=irol-calendar\" title=\"\" class=\"sf-depth-2 \">Events Calendar</a></li>   " +
"                  <li id=\"menu-745-1\" class=\"sf-item-2 sf-depth-2 sf-no-children\"><a href=\"http://investors.amgen.com/phoenix.zhtml?c=61656&p=irol-presentations\" title=\"\" class=\"sf-depth-2 \">Presentations</a></li>   " +
"                  <li id=\"menu-746-1\" class=\"sf-item-3 sf-depth-2 sf-no-children\"><a href=\"http://investors.amgen.com/phoenix.zhtml?c=61656&p=irol-reportsother\" title=\"\" class=\"sf-depth-2 \">Reconciliations</a></li>   " +
"                  <li id=\"menu-747-1\" class=\"sf-item-4 sf-depth-2 sf-no-children\"><a href=\"http://investors.amgen.com/phoenix.zhtml?c=61656&p=irol-stockquote\" title=\"\" class=\"sf-depth-2 \">Stock Info</a></li>   " +
"                  <li id=\"menu-747-1\" class=\"sf-item-5 sf-depth-2 sf-no-children\"><a href=\"http://investors.amgen.com/phoenix.zhtml?c=61656&p=irol-dividends\" title=\"\" class=\"sf-depth-2 \">Dividend History</a></li>   " +
"                  <li id=\"menu-748-1\" class=\"sf-item-6 sf-depth-2 sf-no-children\"><a href=\"http://investors.amgen.com/phoenix.zhtml?c=61656&p=irol-faq\" title=\"\" class=\"sf-depth-2 \">Investor FAQ</a></li>   " +
"                  <li id=\"menu-749-1\" class=\"sf-item-7 sf-depth-2 sf-no-children\"><a href=\"http://investors.amgen.com/phoenix.zhtml?c=61656&p=irol-sec\" title=\"\" class=\"sf-depth-2 \">SEC Filings</a></li>   " +
"                  <li id=\"menu-750-1\" class=\"sf-item-8 sf-depth-2 sf-no-children\"><a href=\"http://investors.amgen.com/phoenix.zhtml?c=61656&p=irol-investor-ekit\" title=\"\" class=\"sf-depth-2 \">Shareholder Center</a></li>   " +
"                  <li id=\"menu-751-1\" class=\"sf-item-9 sf-depth-2 sf-no-children\"><a href=\"http://investors.amgen.com/phoenix.zhtml?c=61656&p=irol-reportsannual\" title=\"\" class=\"sf-depth-2 \">Annual Reports</a></li>   " +
"                  <li id=\"menu-752-1\" class=\"sf-item-10 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/annualmeeting/index.html\" title=\"\" class=\"sf-depth-2 \">Annual Meeting</a></li>   " +
"                  <li id=\"menu-753-1\" class=\"sf-item-11 sf-depth-2 sf-no-children\"><a href=\"http://investors.amgen.com/phoenix.zhtml?c=61656&p=irol-contact\" title=\"\" class=\"sf-depth-2 \">Investor Contacts</a></li>   " +
"                  <li id=\"menu-754-1\" class=\"sf-item-12 sf-depth-2 sf-no-children\"><a href=\"http://investors.amgen.com/phoenix.zhtml?c=61656&p=irol-infoReq\" title=\"\" class=\"sf-depth-2 \">Request Materials</a></li>   " +
"                  <li id=\"menu-755-1\" class=\"sf-item-13 sf-depth-2 sf-no-children\"><a href=\"http://investors.amgen.com/phoenix.zhtml?c=61656&p=irol-alerts\" title=\"\" class=\"sf-depth-2 \">E-mail Alerts</a></li>   " +
"                </ul>   " +
"              </li>   " +
"              <li id=\"menu-710-1\" class=\"sf-item-5 sf-depth-1 sf-total-children-10 sf-parent-children-0 sf-single-children-10 menuparent\"><a href=\"http://careers.amgen.com/\" title=\"\" class=\"sf-depth-1  menuparent\">Amgen Careers</a>   " +
"                <ul>   " +
"                  <li id=\"menu-756-1\" class=\"sf-item-1 sf-depth-2 sf-no-children\"><a href=\"http://careers.amgen.com/life-at-amgen/\" title=\"\" class=\"sf-depth-2 \">Life at Amgen</a></li>   " +
"                  <li id=\"menu-757-1\" class=\"sf-item-2 sf-depth-2 sf-no-children\"><a href=\"http://careers.amgen.com/employment-benefits/\" title=\"\" class=\"sf-depth-2 \">Employment Benefits</a></li>   " +
"                  <li id=\"menu-758-1\" class=\"sf-item-3 sf-depth-2 sf-no-children\"><a href=\"http://careers.amgen.com/university-relations/\" title=\"\" class=\"sf-depth-2 \">University Relations</a></li>   " +
"                  <li id=\"menu-759-1\" class=\"sf-item-4 sf-depth-2 sf-no-children\"><a href=\"http://careers.amgen.com/global-job-locations/\" title=\"\" class=\"sf-depth-2 \">Global Locations</a></li>   " +
"                  <li id=\"menu-760-1\" class=\"sf-item-5 sf-depth-2 sf-no-children\"><a href=\"http://careers.amgen.com/staffing-process/\" title=\"\" class=\"sf-depth-2 \">Staffing Process</a></li>   " +
"                  <li id=\"menu-761-1\" class=\"sf-item-6 sf-depth-2 sf-no-children\"><a href=\"http://careers.amgen.com/career-news-events/\" title=\"\" class=\"sf-depth-2 \">Career News & Events</a></li>   " +
"                  <li id=\"menu-762-1\" class=\"sf-item-7 sf-depth-2 sf-no-children\"><a href=\"http://careers.amgen.com/job-search/\" title=\"\" class=\"sf-depth-2 \">Job Search</a></li>   " +
"                </ul>   " +
"              </li>   " +
"              <li id=\"menu-711-1\" class=\"sf-item-6 sf-depth-1 sf-total-children-7 sf-parent-children-0 sf-single-children-7 menuparent\"><a href=\"http://www.amgen.com/media/overview.html\" title=\"\" class=\"sf-depth-1  menuparent\">Media</a>   " +
"                <ul>   " +
"                  <li id=\"menu-766-1\" class=\"sf-item-1 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/media/fact_sheets.html\" title=\"\" class=\"sf-depth-2 \">Fact Sheets</a></li>   " +
"                  <li id=\"menu-767-1\" class=\"sf-item-2 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/media/featured_content.html\" title=\"\" class=\"sf-depth-2 \">Featured Content</a></li>   " +
"                  <li id=\"menu-768-1\" class=\"sf-item-3 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/media/pr.jsp\" title=\"\" class=\"sf-depth-2 \">News Releases</a></li>   " +
"                  <li id=\"menu-769-1\" class=\"sf-item-4 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/media/our_perspective.html\" title=\"\" class=\"sf-depth-2 \">Our Perspective</a></li>   " +
"                  <li id=\"menu-769-1\" class=\"sf-item-4 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/media/social_media.html\" title=\"\" class=\"sf-depth-2 \">Social Media</a></li>   " +
"                  <li id=\"menu-770-1\" class=\"sf-item-5 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/media/contacts.html\" title=\"\" class=\"sf-depth-2 \">Media Contacts</a></li>   " +
"                  <li id=\"menu-771-1\" class=\"sf-item-6 sf-depth-2 sf-no-children\"><a href=\"http://www.amgen.com/media/in_the_news.html\" title=\"\" class=\"sf-depth-2 \">In The News</a></li>   " +
"                </ul>   " +
"              </li>   " +
"            </ul>   " +
"          </div>   " +
"        </div>   " +
"        <!-- /.block -->    " +
"      </div>   " +
"      <!-- /.region -->    " +
"         " +
"    </div>   " +
"  </div>   ";
  document.write(menu);
  
  