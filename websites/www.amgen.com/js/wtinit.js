// WebTrends SmartSource Data Collector Tag v10.4.7
// Copyright (c) 2013 Webtrends Inc.  All rights reserved.
// Tag Builder Version: 4.1.2.4
// Created: 2013.08.13
window.webtrendsAsyncInit=function(){
    var dcs=new Webtrends.dcs().init({
        dcsid:"dcs6dveor10000k7bxhuy52b9_6m9r",
        domain:"http://www.amgen.com/js/statse.webtrendslive.com",
        timezone:-8,
        i18n:true,
        offsite:true,
        download:true,
        downloadtypes:"xls,doc,pdf,txt,csv,zip,docx,xlsx,rar,gzip",
        onsitedoms:"http://www.amgen.com/js/amgen.com",
        fpcdom:".amgen.com",
        plugins:{
            hm:{src:"../../s.webtrends.com/js/webtrends.hm.js"/*tpa=http://s.webtrends.com/js/webtrends.hm.js*/}
        }
        }).track();
};
(function(){
    var s=document.createElement("script"); s.async=true; s.src="wt-v10.js"/*tpa=http://www.amgen.com/js/wt-v10.js*/;    
    var s2=document.getElementsByTagName("script")[0]; s2.parentNode.insertBefore(s,s2);
}());