(function () {
	var window = this,
		$ = window.jQuery,
		DDC = (window.DDC = (window.DDC || {}));

	DDC.modules.directionsForm.refresh = function () {
		$(DDC.modules.directionsForm.selector).each(function () {
			if (!$(this).data('directions-form-initialized')) {
				var selectedAccount = $('[name="destinationAddress"] option:selected', this).text();

				$('[name="destinationAddress"]',this).bind('change', function () {
					selectedAccount = $('option:selected', this).text();
					$('li .vcard').each(function(){
						var org = $(this).find('.org').text(),
							re = new RegExp('^(' + selectedAccount + ')$', 'gi');
							org.match(re) && $(this).parent().trigger('mouseenter');
					});
				});


				$(this).bind('submit', function (ev) {
					var form = $(this),
						fields = {
							array: form.serializeArray()
						},
						o;

					if (!form.data('validator') || form.data('validator').checkValidity()) {
						for (o in fields.array) {
							if (fields.array.hasOwnProperty(o) && fields.array[o].name) {
								fields[fields.array[o].name] = fields.array[o].value;
							}
						}

						if (!fields.startAddress && fields.strt1 && fields.zipc1) {
							fields.startAddress = [fields.strt1, fields.zipc1].join(', ');
						}

						if (fields.startAddress && fields.destinationAddress) {

							// link for clearing directions
							var icon = document.createElement('span');
							icon.setAttribute ('class', 'align-right ui-icon ui-icon-circle-close');
							var resetLink = document.createElement('a');
							resetLink.appendChild (icon);
							resetLink.setAttribute ('href', '#');
							resetLink.setAttribute ('class', 'reset-link');

							// remove old vcard if it is on page already
							$(this).closest('.directions-form').find('div .vcard').parent().remove();

							// swap out drop down with vcard area with 'x' button
							var vCard = $('li .vcard:contains("' + selectedAccount + '")').parent().html();
							var vCardDiv = document.createElement('div');
							vCardDiv.setAttribute('class', 'ddc-content mod');
							$(vCardDiv).html(resetLink);
							$(vCardDiv).append(vCard);
							$(this).prepend(vCardDiv);
							//$('[name="destinationAddress"]',this).parent().hide('slow');

							$(resetLink)
							.bind('click', function() {
								$(this).closest('form').find(':input')
								.not(':button, :submit, :reset, :hidden')
								.val('')
								.removeAttr('checked')
								.removeAttr('selected');

								$(this).closest('.directions-form').find('form').find('label:hidden,.yui3-u-1').show('fast');
								$(this).parent().remove();

								$(document).trigger('directions-reset', ev);
							});

							$(document).trigger('directions-search', {
								directions: {
									destination: fields.destinationAddress,
									origin: fields.startAddress
								},
								targetMap: $.getDataAttributes(form).targetMap
							});
						}
					}

					return false;
				}).data('directions-form-initialized', true);
			}
		});
	};
}());


