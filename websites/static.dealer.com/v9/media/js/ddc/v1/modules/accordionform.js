(function () {
	var window = this,
		$ = window.jQuery,
		DDC = (window.DDC = (window.DDC || {}));
	
	DDC.modules.accordionForm.refresh = function () {
		$(DDC.modules.accordionForm.selector).each(function () {
			var nextFocus;
			
			if (!$(this).data('accordion-form-initialized')) {
				$(this).data('accordion-form-initialized', true)
					.bind('onFail', function (e, errors) {
						var index;
						
						if (errors && errors.length) {
							nextFocus = errors[0].input;
							index =
								$(errors[0].input)
									.parents('.ui-accordion')
									.find('.ui-accordion-content')
									.index($(errors[0].input)
										.parents('.ui-accordion-content'));
							$(errors[0].input)
								.parents('.ui-accordion')
								.accordion('activate', index);
							setTimeout(function () {
								nextFocus = errors[0].input;
								$(errors[0].input)
									.parents('.ui-accordion')
									.trigger('accordionchange');
							}, 750);
						}
					}).bind('accordionchange', function () {

						if (!nextFocus) {
							$(this).find(':input, select')
								.filter(':visible:enabled:eq(0)')
								.focus();
						} else {
							$(nextFocus).focus();
						}
						
						nextFocus = false;
					});
			}
		});
	};
}());
