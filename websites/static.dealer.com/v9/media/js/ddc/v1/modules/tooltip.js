(function () {
	var window = this,
		$ = window.jQuery,
		DDC = (window.DDC = (window.DDC || {}));
	
	$.tools.tooltip.addEffect('zoom', function (callback) { 
		this.getTip()
			.transform({scale: 0})
			.show()
			.css({
				opacity: 0.001,
				'-moz-transform-origin': '0 0',
				'-webkit-transform-origin': '0 0',
				'-o-transform-origin': '0 0'
			}).animate({
				scale: 1,
				opacity: 0.999
			}, 'fast', callback);
	}, function (callback) {
		this.getTip().animate({
			scale: 0,
			opacity: 0.001
		}, this.getConf().fadeOutSpeed, function () {
			$(this).transform({scale: 1});
			if ($.isFunction(callback)) {
				callback.apply(this);
				$(this).hide();
			}
		});
	});
	
	DDC.modules.tooltip.refresh = function () {
		var settings = {},
			returnFalse = function () {
				return false;
			};
		
		$(DDC.modules.tooltip.selector).each(function () {
			if (!$(this).data('initialized')) {
				settings = $.extend({
					effect: 'zoom',
					predelay: '100',
					relative: true,
					/*
					 * jQuery Tools tooltip internally uses the jQuery API
					 * method isDefaultPrevented to decide whether or not to
					 * show/hide tooltips.  jQuery 1.5 apparently changed the way this
					 * method works.  And jQuery Tools has not yet been
					 * updated for jQuery 1.5.
					 * 
					 * Without the following hack, tooltips are shown and hidden
					 * inconsistently.  Thus, we must hack the return
					 * value of these events' event objects' isDefaultPrevented
					 * methods to always return false (which was returned consistently
					 * in previous version of jQuery, but changed in jQuery 1.5 to
					 * be less consistent).
					 *
					 * These overrides may be removed when jQuery Tools tooltip is
					 * officially updated to support jQuery 1.5.
					 *
					 * More information
					 * - the specific issue:  http://flowplayer.org/tools/forum/30/58627
					 * - jQuery API isDefaultPrevented():  http://api.jquery.com/event.isDefaultPrevented/
					 */
					onBeforeShow: function (e) {
						e.isDefaultPrevented = returnFalse;
					},
					onBeforeHide: function (e) {
						e.isDefaultPrevented = returnFalse;
					}
				}, $.getDataAttributes(this));
				
				$(this).data('initialized', true).tooltip(settings);
			}
		});
	};
}());
