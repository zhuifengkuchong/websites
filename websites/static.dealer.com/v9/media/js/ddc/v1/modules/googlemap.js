/*jslint regexp: true, browser: true, confusion: true, unparam: true, sloppy: true, white: true, forin: true, nomen: true, plusplus: true, maxerr: 50, indent: 4 */
(function () {
	var window = this,
		$ = window.jQuery,
		DDC = (window.DDC = (window.DDC || {})),
		hide = false,
		show = true;

	DDC.modules.googleMap.refresh = function () {
		var google = window.google;

		//use a data attr flag on this map instance to check if it has already be initialized
		if (typeof google === 'object' && google.hasOwnProperty('load') && !$(this).data('google-map-initialized')) {

			$(this).data('google-map-initialized', true);

			google.load('maps', 'http://static.dealer.com/v9/media/js/ddc/v1/modules/3.14', {
				other_params: 'sensor=false&language=' + DDC.getLang() + '&client=gme-dealercom1',
				callback: function () {
					DDC.googleMaps = DDC.googleMaps || [];
					DDC.googleMapsInfoWindows = DDC.googleMapsInfoWindows || [];
					var myOptions = {},
						mapEl,
						map,
						ignoreBounds = true, // JIRA-7746 Ignores the mapBounds based on multiple markers. Fix for multiple dealerships in same location.
						directionsService = new google.maps.DirectionsService(),
						geocoder = new google.maps.Geocoder(),
						isCoord = function (a) {
							return $.isArray(a) && (a.length === 2) && !isNaN(a[0]) && !isNaN(a[1]);
						},
						isLatLng = function (a) {
							return a instanceof google.maps.LatLng;
						},
						isPoint = function (a) {
							return isLatLng(a) || isCoord(a);
						},
						containsLatLng = function (list, point) {
							var res = false;

							if (list && point) {
								res = !!$.grep(list, function (e, i) {
									return list[i].equals(point);
								}).length;
							}

							return res;
						},
						getMapBounds = function (list) {
							var res = false,
								i;

							// filter out any values that can't be calculated
							list = $.grep(list, isPoint);
							// convert all coords to LatLng points
							$.each(list, function (i, v) {
								if (isCoord(v)) {
									list[i] = new google.maps.LatLng(v[0], v[1]);
								}
							});
							if (list.length) {
								res = new google.maps.LatLngBounds(list[0], list[0]);
								for (i = 1; i < list.length; i++) {
									res.extend(list[i]);
									if (!list[i].equals(list[i - 1])) {
										ignoreBounds = false; // Use google maps Bounds based on multiple markers.
									}
								}
							}
							return res;
						},
						coords = (function () {
							var list = [];

							return {
								getList: function () {
									return list;
								},
								addToList: function (latlng, i) {
									if (isLatLng(latlng) && !containsLatLng(list)) {
										list[i] = latlng;
									} else {
										list[i] = '';
									}
								},
								addCoords: function (coordsArray, callback) {
									var self = this,
										geocodesOutstanding = coordsArray.length,
										i;

									if (geocodesOutstanding) {
										$.each(coordsArray, function (i, v) {
											if (!$.isArray(v) || v.length !== 2) {
												v = v.toString();
												// do geocoding if marker isn't a lat/lng coord
												geocoder.geocode({'address': v}, function (results, status) {
													if (status === google.maps.GeocoderStatus.OK) {
														// LatLng object returned
														self.addToList(results[0].geometry.location, i);
													}
													if (!--geocodesOutstanding) {
														if ($.isFunction(callback)) {
															callback(window);
														}
													}
												});
											} else {
												geocodesOutstanding--;
												if (isCoord(v)) {
													// convert lat/lng coords into LatLng object
													self.addToList(new google.maps.LatLng(v[0], v[1]), i);
												}
											}
										});
									}
								},
								getCenter: function () {
									var res = this.getBounds(list);
									if (res) {
										res = res.getCenter();
									}
									return res;
								},
								getBounds: function () {
									return getMapBounds(list);
								}
							};
						}()),
						initializeMap = function () {
							coords.center = coords.getCenter();

							if (coords.center) {
								myOptions.center = coords.center;

								if (!map) {
									map = new google.maps.Map(mapEl[0], {
										center: myOptions.center,
										zoom: myOptions.zoom,
										mapTypeId: myOptions.mapTypeId,
										streetViewControl: false,
										navigationControl: true,
										mapTypeControl: true
									});
									DDC.googleMaps.push(map);
								}

								if (myOptions.data && myOptions.data.circleAddress) {
									if (myOptions.data.circleAddress.toString().length === 4) {
										myOptions.data.circleAddress = '0' + myOptions.data.circleAddress.toString();
									}
									geocoder.geocode({'address': myOptions.data.circleAddress.toString()}, function (results, status) {
										var radius = myOptions.data.circleRadius;

										if (radius && (myOptions.data.circleRadiusUom === 'm')) {
											// convert miles to kilometers
											radius = (+radius * 1.609);
										}

										if (radius && (status === google.maps.GeocoderStatus.OK)) {
											myOptions.circle = new google.maps.Circle({
												center: results[0].geometry.location,
												map: map,
												radius: radius * 1000, // meters
												clickable: false,
												fillOpacity: 0.2,
												fillColor: '#0056B7',
												strokeWeight: 2,
												strokeColor: '#0056B7'
											});

											map.fitBounds(myOptions.circle.getBounds());
										}
									});
								}

								$(coords.getList()).each(function (i) {
									var j;

									if (coords.getList()[i] !== undefined && isLatLng(coords.getList()[i]) && !coords.getList()[i].marker) {

										coords.getList()[i].marker = new google.maps.Marker({
											position: coords.getList()[i],
											map: map
										});

										if (myOptions.infoWindows && myOptions.infoWindows[i] && !coords.getList()[i].infoWindow) {

											coords.getList()[i].infoWindow = new google.maps.InfoWindow({
												content: '<div style="min-height: 100px;">' + $(myOptions.infoWindows[i]).html() + '</div>'
											});
											DDC.googleMapsInfoWindows[myOptions.infoWindows[i]] = coords.getList()[i].infoWindow;
											if ($.getDataAttributes(myOptions.infoWindows[i]).infoWindowEvent) {
												$(myOptions.infoWindows[i]).bind($.getDataAttributes(myOptions.infoWindows[i]).infoWindowEvent, function () {
													google.maps.event.trigger(coords.getList()[i].marker, 'click');
												});
											}
											google.maps.event.addListener(coords.getList()[i].marker, 'click', function () {
												for (j = 0; j < coords.getList().length; j++) {
													if (coords.getList()[j].infoWindow) {
														coords.getList()[j].infoWindow.close();
													}
												}
												coords.getList()[i].infoWindow.open(map, coords.getList()[i].marker);

												$(document).trigger('marker-clicked.google-maps', coords.getList()[i].marker);
											});
										}
									}
								});
							}

							if (map && coords.getList().length > 1 && !ignoreBounds) {
								map.fitBounds(coords.getBounds());
							}
						},
						toggleMarkers = function (showMarkers) {
							$(coords.getList()).each(function (i) {
								coords.getList()[i].marker.setVisible(showMarkers);

								if (coords.getList()[i].infoWindow) {
									coords.getList()[i].infoWindow.close();
								}
							});
						};

					$(DDC.modules.googleMap.selector).each(function () {
						if (!$(this).data('google-maps-initialized')) {
							myOptions = $.getDataAttributes(this);
							mapEl = $(this);
							markerAddresses = [];
							myOptions.infoWindows = $.getDataAttributes(this).infoWindows ? $.getDataAttributes(this).infoWindows.split(',') : null;
							myOptions.data = $.getDataAttributes(this);
							if (myOptions.data.mapGetMarkersByAddress && myOptions.data.mapGetMarkersByAddress === true) {
								myOptions.data.markersList = myOptions.data.markersList.replace(/\'/g, '"');
								myOptions.markersList = myOptions.markersList ? $.parseJSON(myOptions.data.markersList) : null;
								for (var i = 0; i < myOptions.markersList.length; i++) {
									markerAddresses.push($.makeArray(myOptions.markersList[i].storeLocation));
								}
								myOptions.markersList = markerAddresses;
							} else {
								myOptions.markersList = myOptions.markersList ? $.parseJSON(myOptions.data.markersList) : null;
							}

							myOptions.mapTypeId = myOptions.mapTypeId ?
								google.maps.MapTypeId[$.getDataAttributes(this).mapTypeId] :
								google.maps.MapTypeId.ROADMAP;

							coords.addCoords(myOptions.markersList, initializeMap);
							// coords.addCoords(["boston, MA", "burlington, VT", "new hampshire", "nevada"], initializeMap);
							initializeMap();
							$(this).data('google-maps-initialized', true);
						}
					});

					// set up directions events
					$(document)
						.unbind('.google-maps')
						.bind('directions-search.google-maps', function (ev, data) {
							if (data && data.directions.origin && data.directions.destination) {
								$.extend(data.directions, {
									travelMode: google.maps.DirectionsTravelMode.DRIVING
								});
								directionsService.route(data.directions, function (results, status) {
									if (status === google.maps.DirectionsStatus.OK) {
										$.extend(data, {results: results});
										$(document).trigger('directions-results', data);
									}
								});
							} else {
								return data;
							}
						})
						.bind('directions-results.google-maps', function (ev, data) {

							toggleMarkers(hide);

							// if data.targetMap, what to do?
							if (data && data.results) {
								$.each(DDC.googleMaps, function (i, map) {
									if ($(map.getDiv()).is(':visible')) {
										map.directionsRenderer = map.directionsRenderer ||
											new google.maps.DirectionsRenderer();
										map.directionsRenderer.setMap(map);
										map.directionsRenderer.setDirections(data.results);
									}

									if ($('.directions-results').get(i) &&
										map.directionsRenderer) {
										map.directionsRenderer.setPanel($('.directions-results').get(i));
									}
								});
							}
						})
						.bind('directions-reset.google-maps', function (ev) {
							map.directionsRenderer.setMap(null);
							toggleMarkers(show);
							$('.directions-results').empty();
						})
						.bind('destination-change.google-maps', function (ev) {

					});

					// trigger on full initialization with directions events
					$(document).trigger('googleMapInitialized');
				}
			});
		} //end map exists check
	};
}());
