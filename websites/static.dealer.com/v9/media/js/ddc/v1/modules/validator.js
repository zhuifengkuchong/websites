(function () {
	var window = this,
		$ = window.jQuery,
		DDC = (window.DDC = (window.DDC || {})),
		formsV2 = DDC.hasFeature.formsV2;

	DDC.modules.validator.refresh = function () {
		var selectedForm,
			emailRequired = '',
			phoneRequired = '',
			emailRe = /^([a-z0-9_\.\-\+]+)@([\da-z\.\-]+)\.([a-z\.]{2,6})$/i,
			// http://net.tutsplus.com/tutorials/other/8-regular-expressions-you-should-know/
			setRequired = function (required, validate) {
				// takes an element and makes it required
				// adds <em>*</em>, .required class, [required=required] attribute,
				$required = $(required).filter('input, select, textarea').eq(0);
				if ($required.size()) {
					var $formGroup = $required.closest('.ddc-form-group');
					if ($formGroup.size() && !$('span:eq(0)', $formGroup).find('em').size()) {
						$('span:eq(0)', $formGroup.find('label')).append('<em>*</em>');
					} else if (!$('span:eq(0)', $required.closest('label')).find('em').size()) {
						$('span:eq(0)', $required.closest('label')).append('<em>*</em>');
					}
					$required.addClass('required').attr('required', 'required');
				}
				if (validate) {
					$required.closest('form').data('validator').checkValidity($required);
				}
			},
			clearRequired = function (set) {
				var conf, label;
				// removes required-ness from a set of form elements, including any validation error messages
				$set = $(set);
				conf = $set.closest('form').data('validator').getConf();
				if ($set.size()) {
					$set.filter('input, select, textarea').removeClass(conf.errorClass).removeClass('required').removeAttr('required').each(function () {

						if (formsV2) {
							var $formGroup = $(this).closest('.ddc-form-group');
							label = $formGroup.find('label');
							errorWrapper = $formGroup;
						} else {
							label = $(this).closest('label');
							errorWrapper = label;
						}

						label.find('em').filter(function () {
							return $(this).text() === '*';
						}).add($('.errors', errorWrapper)).remove();
					});
				}
			},
			contactMeByHandler = function (select) {

				// JIRA-11518: Cannot set both email and phone as required fields
				// This allows phone to be set to required if the form definition says
				// both it should be required.

				var targets = $(),
					activeTarget,
					$select = $(select),
					$opts = $select.find('option'),
					$form = $select.closest('form'),
					selector = $select.val().toLowerCase(),
					getTargetFromLabel = function(labelSelector) {
						// TODO: replace class-based coupling to form field id-based coupling i.e. $('[name="$formFieldId"]', form).find('input,textarea,select')
						if (formsV2) {
							target = $form.find('label.' + labelSelector).closest(".ddc-form-group").find('input, textarea, select');
						} else {
							target = $form.find('.' + labelSelector).find('input, textarea, select');
						}

						return target;
					};

				activeTarget = getTargetFromLabel(selector);

				// only works for select elements right now
				if ($opts.size()) {
					$opts.each(function () {
						var optionTarget;

						selector = $(this).val().toLowerCase();

						optionTarget = getTargetFromLabel(selector);

						if (optionTarget.size()) {
							// JIRA-11518: If check added for JIRA task
							if ( ( optionTarget.attr('name').indexOf('email') !== -1 && $form.attr('data-email-required') == 'false' ) ||
								( optionTarget.attr('name').indexOf('phone') !== -1 && $form.attr('data-phone-required') == 'false' ) ) {
									targets = targets.add(optionTarget);
								}
						}
					});
				}

				if ( targets.size() > 0 ) {
					clearRequired(targets);
				}
				setRequired(activeTarget);
			};

			if ($.tools && $.tools.validator && $.tools.validator.fn) {
				$.tools.validator.fn(':input[type="text"].date', function (el, value) {
					// just tests for proper MM/DD/YYYY formatting, not correct day/month/year calendar date
					return value === '' || (/^[0-1][0-9]\/[0-3][0-9]\/[\d]{4}/).test(value) ? true : "Invalid date";
				});

				$.tools.validator.fn(':email', function (el, value) {
					if (!el.hasClass('emailconfirm')) {
						return !value || emailRe.test(value);
					}
					return el.closest('.email').prevAll('.email:first').find(':input').val() === value;
				});

				//Custom validation logic for checkbox groups
				//on any checkbox's validation, ensure at least one is checked
				$.tools.validator.fn('span.group-required input[type="checkbox"]', function (el, value) {
					return $('span.group-required input[type="checkbox"]:checked').length > 0 ? true : 'Please select at least one.';
				});
			}

			if ($.tools && $.tools.validator && $.tools.validator.addEffect) {
				// add display-under effect
				$.tools.validator.addEffect("append-under", function (errors, event) {
					// show
					var conf = this.getConf();
					$.each(errors, function (index, error) {
						// invalid input
						var input = error.input.addClass(conf.errorClass),
							msg = input.data('http://static.dealer.com/v9/media/js/ddc/v1/modules/msg.el');

						if (!msg) {
							msg = conf.message.clone();

							if (formsV2 && DDC.mobile && input.is('select')) {
								input.closest('.select-wrap').after(msg);
							} else if (formsV2 ) {
								input.after(msg);
							} else {
								msg.appendTo(input.closest('label'));
							}

							input.data('http://static.dealer.com/v9/media/js/ddc/v1/modules/msg.el', msg);
						}

						msg.addClass('hide').children().remove();
						$.each(error.messages, function (i, v) {
							conf.messageItem.clone().append(v).appendTo(msg);
						});
						msg.removeClass('hide');
					});
				}, function (inputs) {
					// hide
					var conf = this.getConf();
					if (inputs && inputs.size()) {
						inputs.removeClass(conf.errorClass).each(function () {
							if ($(this).data('http://static.dealer.com/v9/media/js/ddc/v1/modules/msg.el')) {
								$(this).data('http://static.dealer.com/v9/media/js/ddc/v1/modules/msg.el').addClass('hide');
							}
						});
					}
				});
			}

			$(DDC.modules.validator.selector).each(function (i) {
				var $form = $(this);
				if (!$form.data('validator-initialized')) {

					emailRequired = false;
					phoneRequired = false;
					emailField = $form.find('[name$="email"]');
					phoneField = $form.find('[name$="phone"]');

					if (emailField && emailField.attr('required') === 'required') {
						emailRequired = true;
					}

					if (phoneField && phoneField.attr('required') === 'required') {
						phoneRequired = true;
					}

					$form.data('validator-initialized', true)
						.attr('data-email-required', emailRequired)
						.attr('data-phone-required', phoneRequired)
						.attr('novalidate','novalidate').validator({
						errorInputEvent: 'change',
						effect: 'append-under',
						message: $('<strong class="errors"/>'),
						messageItem: $('.templates .error:eq(0)', $form).size() ? $('.templates .error:eq(0)', $form) : $('<small/>'),
						onFail: function () {
							//unmark all checkboxes that are claiming to be invalid, all the times
							var form = this.getForm();
							// ensure reflow in IE
							if (!$.support.changeBubbles) {
								// Re-flows source elements in IE fixing DOM overlap issues
								$(form).closest('[class*=yui3-g]:eq(0)').addClass('reflow');
							}
							setTimeout(function () {
								$(form).closest('.ui-dialog').effect('shake', 40, function () {
									// autofocus the first invalid field
									$('.invalid:first', this).focus();
								});
							}, 50);
						}
					});
					// special handling for .preferred-contact field
					$form.find('.preferred-contact[required]').on('change', function (e) {
						contactMeByHandler($(e.target));
					}).each(function () {
						// make sure states are properly set
						contactMeByHandler($(this));
					});
					// If a widget needs to know when its specific validator is ready (as opposed to 'validatorReady' which will only happen once per page)
					$.publish('validatorInitialized', [$form]);
				}
			});
		};
}());
