(function () {
	var window = this,
		$ = window.jQuery,
		DDC = (window.DDC = (window.DDC || {}))
	;
	
	DDC.modules.hours.refresh = function () {
		$(DDC.modules.hours.selector + ':not(.consolidated)').each(function () {
			var container = $(this),
				li = container.find("li"),
				day = new Date().getDay(),
				firstDayOfWeek = 1, // Monday
				today = (day - firstDayOfWeek + 7) % 7;

			li.each(function(index){
				if ( $(this).index() == today ) {
					$(this).addClass("today");
					return false;
				}
			});
		});
	};
}());
