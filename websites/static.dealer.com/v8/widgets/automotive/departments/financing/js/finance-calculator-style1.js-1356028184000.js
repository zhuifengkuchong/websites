var calcForm;

function initialize( formName ) {
	calcForm = (formName==null)?document.forms[0]:$("#"+formName)[0];
//	calcForm.rate.value=defaultRate;
	populate(calculate());
}

function initializePaymentCalculator( formName ) {
	if( $("http://static.dealer.com/v8/widgets/automotive/departments/financing/js/label.rate").text().indexOf("%") == -1 ){
		$("http://static.dealer.com/v8/widgets/automotive/departments/financing/js/label.rate").append("<em>%</em>");
	}
	calcForm = (formName==null)?document.forms[0]:$("#"+formName)[0];
	var cashTrade = calcForm.cash_trade.value;
	if (cashTrade.indexOf("%") != "-1") {
		if(calcForm.price != undefined){
			price = normalize(calcForm.price.value);
			cashTrade = Math.floor((price * ( parseInt(cashTrade) / 100) ) / 100) * 100;
			calcForm.cash_trade.value = cashTrade;
		}else{
			calcForm.cash_trade.value = "";
		}		
	}
//	calcForm.rate.value=defaultRate;
	//populate(calculate()); //removed this due to ie6 back button bug, there is a custom initializer so this gets executed regardless - sng
}

function populate( moPay ) {
	if(calcForm.rate.value=="" || calcForm.rate.value=="0.") {return;};
	if(calcForm.price != undefined){
		var priceCheck = parseInt(calcForm.price.value)
	}else{
		var priceCheck = 1;
	}
	if( moPay=="http://static.dealer.com/v8/widgets/automotive/departments/financing/js/0.00" && priceCheck == 0 ) {
		calcForm.cash_trade.disabled=true;
		calcForm.retail.disabled=true;
		calcForm.rate.disabled=true;
		calcForm.term.disabled=true;
		calcForm.monthlypayment.value=0;
		calcForm.monthlypayment.disabled=true;		
		calcForm.price.value=verbage;
		calcForm.retail.value=verbage;
		calcForm.monthlypayment.value=verbage;
		return;
	} else if( calcForm.cash_trade.value.indexOf("-") != -1 || new Number(normalize(calcForm.cash_trade.value)) < 0 ){
		alert("Your down payment must be greater than zero.\nIf you have any questions, feel free to contact us via email.\nThank You.");
		return;
	}

	calcForm.cash_trade.disabled=false;
	if(calcForm.retail != undefined){
		calcForm.retail.disabled=false;
	}
	calcForm.rate.disabled=false;
	calcForm.term.disabled=false;
	if(calcForm.monthlypayment != undefined){
		calcForm.monthlypayment.disabled=false;
		calcForm.monthlypayment.value = moPay;
	}
}

function rePopulate( maxTerms ){
	clearTerm();
	populateTerms( maxTerms );
}

var price,
	retail,
	eventTimeout,
	lastValue;

function calculate(){
	if(calcForm.rate.value=="" ) {return;};
	if(calcForm.price != undefined){
		price = normalize(calcForm.price.value) - normalize(calcForm.cash_trade.value);
	}
	retail = normalize(calcForm.retail.value) - normalize(calcForm.cash_trade.value);
	rate = calcForm.rate.value/100;
	term = calcForm.term.options[calcForm.term.selectedIndex].value;

  if( rate == 0 ){
		pay = new String(price/term);
  }else{
		var monthIntRate = rate / 12;
		ratePlus = eval(monthIntRate+1);
		pow = Math.pow( ratePlus, term );
		pay = new String( ( price ) / (( 1 - (1/pow)) / monthIntRate) );
	}					
	if( pay == "NaN" ){
		return verbage;
	}else if( pay<0){
		return 0;
	}
	
	if (window.gaManager && format(pay) !== 'http://static.dealer.com/v8/widgets/automotive/departments/financing/js/0.00' && (lastValue !== format(pay))) {
		clearTimeout(eventTimeout);
		lastValue = format(pay);
		eventTimeout = setTimeout(function () {
			window.gaManager._trackEvent($(calcForm).attr('id'), 'calculate', format(pay));
		}, 1000);
	}
	
	return format(pay);
}

function populateTerms( max,sel,interval,customTerms ) {
	var i = 0;
	if (customTerms != '') {
		var terms = customTerms.split(',');
		for (var j = (terms.length - 1); j >= 0 ; j--) {
			calcForm.term.options[i++] = new Option(terms[j]+' mo.',terms[j]);
		}
	}
	else {
		for (var j = max; j >= 12; (j -= interval)) {
			calcForm.term.options[i++] = new Option(j+' mo.',j);
		}
	}
	calcForm.term.value = sel;	// AFAIK, doing a value assignment works in all modern browsers
}

function normalize( value ){
  if( value == null || value == "" ) {
    value = 0;
  } else {
    value = value.replace(/,/, "");
    value = value.replace(/$/, "");
  }
  return value;
}

function clearTerm() {
  // needs to cleanup all select objects below 'term'
  var start = 0;
  var end = calcForm.elements["term"].options.length;
  if( end < start )
      return false;
  for( i=end; i>=start; i-- ){
      calcForm.elements["term"].options[i] = null;
  }
}

function format( str ) {
  var fraction;
	numerals = new String(str);
  index = numerals.indexOf(".");
	if( index != -1 ){
		fraction = numerals.substring( index, index+3 );
		numerals = numerals.substring( 0, index );
	}
  ln = numerals.length;
  beg = numerals.substring( 0, ln-3 );
  end = numerals.substring( ln-3 );
  retVal=beg+((ln>3)?",":"")+end+((fraction==null)?".00":fraction);
    return retVal;
}

function calculatePurchasingPower() {
    var purchasingPower,
		monthlyPayment = calcForm.affordableMonthlyPayment.value,
		cashTrade      = parseFloat(0 + calcForm.cash_trade.value),
		rate = calcForm.rate.value / 1200,
		term = calcForm.term.value,
		salesTax = 0,
		divisor = 1;  // divisor modifies the overall purchasingPower to account for tax, if any
	
	if (window.DDC && DDC.PurchasingEstimator && DDC.PurchasingEstimator.salesTax) {
		salesTax = parseInt(DDC.PurchasingEstimator.salesTax.toString().replace(/[^0-9]/g, '') || '0', 10);
	}
	
	divisor += (salesTax / 100);
	
    if( rate == 0 ) {
        purchasingPower = ( monthlyPayment * term ) + cashTrade;
    } else {
        purchasingPower = monthlyPayment * ( ( 1 - ( 1 / Math.pow( 1 + rate, term ) ) ) / rate ) + cashTrade;
	}
	
	if (window.gaManager && purchasingPower && lastValue !== format(purchasingPower / divisor)) {
		clearTimeout(eventTimeout);
		lastValue = format(purchasingPower / divisor);
		eventTimeout = setTimeout(function () {
			window.gaManager._trackEvent($(calcForm).attr('id'), 'calculate', format(purchasingPower / divisor));
		}, 1000);
	}
	
    return format(purchasingPower / divisor);
}

function populatePurchasingPower( moPay ) {
    if(calcForm.rate.value=="" || calcForm.rate.value=="0.") {return;}
	if( moPay == "http://static.dealer.com/v8/widgets/automotive/departments/financing/js/NaN.00" || moPay == "NaN"  ) {
	   calcForm.purchasingPower.value=0;
		//calcForm.purchasingPower.disabled=true;
		calcForm.purchasingPower.value=verbage;
		return;
	} else if( calcForm.cash_trade.value.indexOf("-") != -1 || new Number(normalize(calcForm.cash_trade.value)) < 0 ){
	    alert("Your down payment must be greater than zero.\nIf you have any questions, feel free to contact us via email.\nThank You.");
		return;
	}

	calcForm.purchasingPower.disabled=false;
	calcForm.purchasingPower.value = moPay;
}
function resetCalcForm(formName){
	calcForm = $("#"+formName)[0];
}
function doBoth() {
	populate( calculate() );
	if(calcForm.affordableMonthlyPayment != undefined){
		populatePurchasingPower( calculatePurchasingPower() );
	 }
}