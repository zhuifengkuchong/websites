
function showVSCTab(){
    $('.slide-out-div').tabSlideOut({
        tabHandle: '.handle',
        pathToTabImage: '../assets/category_images/handle2.gif'/*tpa=http://www.dollartree.com/assets/category_images/handle2.gif*/,
        imageHeight: '200px',                               //height of tab image
        imageWidth: '40px',                                 //width of tab image
        tabLocation: 'right',                               //side of screen where tab lives, top, right, bottom, or left
        speed: 500,                                         //speed of animation
        action: 'hover',                                    //options: 'click' or 'hover', action to trigger animation
        topPos: '320px',                                    //position from the top
        fixedPosition: true                                 //options: true makes it stick(fixed position) on scroll
    });
};

function showSlideout() {
    var $slideOut = $('.slide-out-div');
    $slideOut.hide();
    setTimeout(function() {
        $slideOut.fadeIn("slow");
        $slideOut.css('background','url(/assets/category_images/full_tab3.gif)');
    }, 500);
};

    function showTabCom(objComDivId) {
        var imgPath = "http://www.dollartree.com/assets/images/shopping/";
        var objComDivArr = new Array("tab-more-info","tab-files","tab-review","tab-email-friend","tab-products");
        for (i = 0; i < objComDivArr.length; i++) {
            if (document.getElementById(objComDivArr[i] + "-img")) {
                document.getElementById(objComDivArr[i] + "-img").src = imgPath + objComDivArr[i] + "-off.gif";
                document.getElementById(objComDivArr[i]).style.display = "none";
            }
        }
        document.getElementById(objComDivId + "-img").src = imgPath + objComDivId + ".gif";
        document.getElementById(objComDivId).style.display = "block";
    };

    utilityBrowserVer = parseInt(navigator.appVersion);

    function imgOn(imgName) {
        if (utilityBrowserVer >= 3) {
            imgOnString = eval(imgName + "_on.src");
            document.images[imgName].src = imgOnString;
        }
    };

    function imgOff(imgName) {
        if (utilityBrowserVer >= 3) {
            imgOffString = eval(imgName + "_off.src");
            document.images[imgName].src = imgOffString;
        }
    };

    function goToLink(address) {
        var linkURL = address.options[address.selectedIndex].value;
        window.top.location.href = linkURL;
        address.selectedIndex=0;
    };

/*
 * This function launches a new web browser window to a specified width, height and features.
 * Features string is a comma separated window's feature needed for this new window. For Instance
 * If a new window needs a toolbar the feature string must be "toolbar" like needs scroll bar and
 * and toolbar then it must be "toolbar,scrollbar". Note that the order of the feature is not required.
 * Also it's case insensitive. Therefore, "scrollbar,toolbar" is identical to "Toolbar,ScrollBar".
 *
 * If the features string is ommitted then all the features are turned off. To turn all the features on
 * use the word "all" for features instead of specifying each feature.
 */

    function openWindow(address, width, height,features)
    {
        /* Find out what features need to be enable
         *
         */
        if(features)
            features = features.toLowerCase();
        else
            features = "";

        var toolbar = (features == "all" ? 1 : 0);
        var menubar = (features == "all" ? 1 : 0);
        var location = (features == "all" ? 1 : 0);
        var directories = (features == "all" ? 1 : 0);
        var status = (features == "all" ? 1 : 0);
        var scrollbars = (features == "all" ? 1 : 0);
        var resizable = (features == "all" ? 1 : 0);


        if(features != "all")
        {
            //split features
            var feature = features.split(",");
            for(i = 0; i < feature.length; i++)
            {
                if(feature[i] == "toolbar")
                    toolbar = 1;
                else if(feature[i] == "menubar")
                    menubar = 1;
                else if(feature[i] == "location")
                    location = 1;
                else if(feature[i] == "directories")
                    directories = 1;
                else if(feature[i] == "status")
                    status = 1;
                else if(feature[i] == "scrollbars")
                    scrollbars = 1;
                else if(feature[i] == "resizable")
                    resizable = 1;
            }

        }
        features = "toolbar=" + toolbar + ",";
        features += "menubar=" + menubar + ",";
        features += "location=" + location + ",";
        features += "directories=" + directories + ",";
        features += "status=" + status + ",";
        features += "scrollbars=" + scrollbars + ",";
        features += "resizable=" + resizable;

        var newWindow = window.open(address, 'Popup_Window', 'width=' + width + ',height=' + height + ',"' + features + '"');
        newWindow.focus();
    };

    function confirmWindow(url, text) {

        if (confirm(text)) {
            window.go = url;
            window.location = url;
        }
    };

    function setOperation(opType,needSubmit,addressIndex) {
        document.addUpdateDeleteAddressForm.operation.value = opType;
        if(addressIndex)
            document.addUpdateDeleteAddressForm.selectedAddressIndex.value = addressIndex;
        if(needSubmit) {
            if(opType == "EDIT")
                window.location='http://www.dollartree.com/user/address_book.jsp?addressIndex=' + addressIndex;
            else
                document.addUpdateDeleteAddressForm.submit();
        }
    };

    function setAddTo(addToType) {
        document.productForm.addTo.value = addToType;
    };

// This is an example of sending product data from the server
// to the client, then formatting the variants on the client-side
// based on certain criteria.
    function cbFormatter(list, attribute, opt)
    {
        // Get the price for this variant.. If this is the primary list, get all related
        // variants from the group and get their prices as well so we can build a range.
        var pvds = list.productVariantDropdownSupport;

        // Get all of the product variants for this attribute
        var variantGroup = attribute.getVariant().getProductVariantGroup();
        var groupVariants = variantGroup.getAllVariants();

        // Are all of the prices the same within the group?
        var lowGroupPrice = 999999.00;
        var highGroupPrice = -1.00;

        for (var idx = 0; idx < groupVariants.length; idx++)
        {
            lowGroupPrice = groupVariants[idx].numericPrice < lowGroupPrice ? groupVariants[idx].numericPrice : lowGroupPrice;
            highGroupPrice = groupVariants[idx].numericPrice > highGroupPrice ? groupVariants[idx].numericPrice : highGroupPrice;
        }

        // Are all of the prices the same within the variant?
        var variants = variantGroup.getVariantsMatching(attribute.name, attribute.value);
        var lowVariantPrice = 999999.00;
        var highVariantPrice = -1.00;

        var lowVariantDisplay = variants[0].displayPrice;
        var highVariantDisplay = variants[0].displayPrice;

        for (var idx = 0; idx < variants.length; idx++)
        {
            // Display really needs to be done before the re-assignment...
            lowVariantDisplay = variants[idx].numericPrice < lowVariantPrice ? variants[idx].displayPrice : lowVariantDisplay;
            lowVariantPrice = variants[idx].numericPrice < lowVariantPrice ? variants[idx].numericPrice : lowVariantPrice;
            highVariantDisplay = variants[idx].numericPrice > highVariantPrice ? variants[idx].displayPrice : highVariantDisplay;
            highVariantPrice = variants[idx].numericPrice > highVariantPrice ? variants[idx].numericPrice : highVariantPrice;
        }

        if (lowGroupPrice != highGroupPrice)
        {
            // Primary list should display a range
            if (pvds.isPrimary)
                if (lowVariantPrice != highVariantPrice)
                    opt.text = opt.text + " " + lowVariantDisplay + " - " + highVariantDisplay;
                else
                    opt.text = opt.text + " " + attribute.getVariant().displayPrice;
            else  // Secondary lists should show exact price
                opt.text = opt.text + " " + attribute.getVariant().displayPrice;
        }
        return opt;
    };

/* This function is called when a subject is changed so that actual subject text can be
 * stored in to a hidden subject field. Since value of the subject list is a keyword define
 * in the command configuration file to identify it's email address.
 */
    function setSubject(thisRef)
    {
        var formRef = document.forms["contactUsForm"];
        //set subject value
        formRef.elements["subject"].value = (thisRef.options[thisRef.selectedIndex]).text;
    };

    /**
     * These functions make it possible to make the forms a bit more interactive
     */
    function setClass(field, addClass, removeClass) {
        if (document.documentElement){
            if(!field.className)
                field.className = '';
            var clsnm = field.className;
            if(clsnm.match(removeClass))
                clsnm = clsnm.replace(removeClass,addClass);
            else if(!clsnm.match(addClass))
                clsnm = clsnm + " " + addClass + " ";
            field.className = clsnm;
        }
    };

    function makeCurrent(field) {
        setClass(field, "form-field-current", "form-field-normal");
    };

    function makeNormal(field) {
        setClass(field, "form-field-normal", "form-field-current");
    };

    function styleToggle(rdoVar) {
        for ( var i = 0; i < rdoVar.form.length; i++){
            if (rdoVar.form[i].name == rdoVar.name) {
                if(rdoVar.form[i].checked) {
                    setClass(rdoVar.form[i].parentNode, "form-field-radio-on", "form-field-radio-off");
                } else {
                    setClass(rdoVar.form[i].parentNode, "form-field-radio-off", "form-field-radio-on");
                }
            }
        }
    };

/* store locator support, moved here so we can call it from the homepage or section pages */
    function submitGeoCodeForm(form) {
        var geocoder = new google.maps.Geocoder();
        var searchValue = form.cityStateZip.value;
        //
        // we dont know what the coords are so go to google to grab
        // them.
        //
        geocoder.getPosition(
                searchValue,
                function(geocode) {
                    if (!geocode) {
                        //alert(form.cityStateZip.value + " not found");
                        submitGeoCodeResultsForm(form);
                        return false;
                    } else {
                        // submit the populated form
                        // alert( geocode.lat() + " " + geocode.lng() );
                        submitGeoCodeResultsForm(form, geocode.lat(), geocode.lng());
                    }
                }
        );
        return false;
    };

    function submitGeoCodeResultsForm(form, lat, lng) {
        // copy the passed latitude and longitude results to the form
        // and submit it
        if(lat!=null && lat!='undefined')
            form.latitude.value = lat;
        if(lng!=null && lng!='undefined')
            form.longitude.value = lng;
        if(form.hasUserSubmitted)
            form.hasUserSubmitted.value = "true";
        form.submit();
    };




    var doc = function() {
        if (self.innerHeight) {
            doc.pageYOffset = self.pageYOffset;
            doc.pageXOffset = self.pageXOffset;
            doc.innerHeight = self.innerHeight;
            doc.innerWidth = self.innerWidth;
        } else if (document.documentElement && document.documentElement.clientHeight) {
            doc.pageYOffset = document.documentElement.scrollTop;
            doc.pageXOffset = document.documentElement.scrollLeft;
            doc.innerHeight = document.documentElement.clientHeight;
            doc.innerWidth = document.documentElement.clientWidth;
        } else if (document.body) {
            doc.pageYOffset = document.body.scrollTop;
            doc.pageXOffset = document.body.scrollLeft;
            doc.innerHeight = document.body.clientHeight;
            doc.innerWidth = document.body.clientWidth;
        }
        return doc;
    };

    function getMouseCoord(e){
        (!e) ? e = window.event : e = e;
        objDoc = new doc();
        (e.pageX) ? objDoc.pageX = e.pageX : objDoc.pageX = e.clientX + objDoc.scrollLeft;
        (e.pageY) ? objDoc.pageY = e.pageY : objDoc.pageY = e.clientY + objDoc.scrollTop;
        return [e.pageX,e.pageY];
    };

    function findPosX(obj) {
        var curleft = 0;
        if (obj.offsetParent) {
            while (1) {
                curleft+=obj.offsetLeft;
                if (!obj.offsetParent) {
                    break;
                }
                obj=obj.offsetParent;
            }
        } else if (obj.x) {
            curleft+=obj.x;
        }
        return curleft;
    };

    function findPosY(obj) {
        var curtop = 0;
        if (obj.offsetParent) {
            while (1) {
                curtop+=obj.offsetTop;
                if (!obj.offsetParent) {
                    break;
                }
                obj=obj.offsetParent;
            }
        } else if (obj.y) {
            curtop+=obj.y;
        }
        return curtop;
    };


/*
 highlights left nav items based on pageName
 */
    function contentNavHighlighter() {
        var href = location.href;

        if (location.href.indexOf("pageName=") > -1) {
            var step1 = location.href.split("pageName=");
            var key = step1[1];

            if (key.indexOf("&") > -1) {
                //in case there are other attributes in the url
                //get only this one
                var step2 = key.split("&");
                key = step2[0];
            }

            contentNavMakeActive("nav_" + key);

            if ($("#nav_" + key).parent().hasClass("thirdLevel")) {
                // in cases where we are on a subnav page, this sets the parent LI of the parent UL to on
                var parentKey = $("#nav_" + key).parent().parent().attr("id");
                if (parentKey != null)
                    contentNavMakeActive(parentKey);
            }
        }
    };

    function contentNavMakeActive(fullID) {
        $("#" + fullID).addClass("on"); //set the LI to on, also displays any subnav UL
        $("#" + fullID + " > a").addClass("active"); //set the A to active
    };


/*
 The promo scroller method used on the homepage content area
 */
    var promoScroller = function($) {
        var scrollerActiveIndex = 0;
        var scrollerContentArr = new Array();
        var scrollerContentWidth = 0;
        var delay = 6000; // default delay in miliseconds
        var timerID = null;

        var timerInit = function() {
            timerID = window.setTimeout("promoScroller.autoScroll()", delay);
        };

        var left = function() {
            window.clearTimeout(timerID);
            //initialize and get widths
            var activeEl = scrollerContentArr[scrollerActiveIndex];
            var newIndex = scrollerActiveIndex - 1;
            if (newIndex < 0) {
                //proceed to the last item in the array
                newIndex = scrollerContentArr.length - 1;
            }
            var newEl = scrollerContentArr[newIndex];

            //move the new el into position just off to the left (the width of the active el)
            newEl.css("left", "-" + scrollerContentWidth + "px");
            newEl.show();

            //animate the current el off to the right
            activeEl.animate({ "left":"+=" + scrollerContentWidth + "px" });

            //animate the new el in from the left
            newEl.animate({ "left":"+=" + scrollerContentWidth + "px" });

            scrollerActiveIndex = newIndex;
            timerInit();
        };

        var right = function() {
            window.clearTimeout(timerID);
            //initialize and get widths
            var activeEl = scrollerContentArr[scrollerActiveIndex];
            var newIndex = scrollerActiveIndex + 1;
            var newEl = scrollerContentArr[newIndex];
            if (newEl == null) {
                //go back to 0 and start over
                newIndex = 0;
                newEl = scrollerContentArr[newIndex];
            }

            //move the new el into position just off to the right (the width of the active el)
            newEl.css("left", scrollerContentWidth + "px");
            newEl.show();

            //animate the current el off to the left
            activeEl.animate({ "left":"-=" + scrollerContentWidth + "px" });

            //animate the new el in from the right
            newEl.animate({ "left":"-=" + scrollerContentWidth + "px" });

            scrollerActiveIndex = newIndex;
            timerInit();
        };

        return {
            autoScroll : function() {
                right();
            },

            init : function(firstId,delayInSecs) {
                //if delay is not specified, used the default delay of 15 seconds
                if (delayInSecs > 0)
                    delay = delayInSecs * 1000;

                //get the content key and track the active index
                var key = firstId.slice(0,firstId.length - 1);

                //find siblings and build array
                var testEl = $("#" + firstId);
                scrollerContentWidth = testEl.width(); //set width for animations while we are here
                while ( testEl.attr("id") && testEl.attr("id").indexOf(key) > -1 ) {
                    scrollerContentArr.push(testEl);
                    if (testEl.next())
                        testEl = testEl.next();
                    else
                        break;
                }

                //display and bind scrollers if necessary
                if (scrollerContentArr.length > 1) {
                    $(".scroller_l").show();
                    $(".scroller_r").show();
                    $(".scroller_l a").click(left);
                    $(".scroller_r a").click(right);
                    timerInit();
                }
            }
        };
    }($);

/*
 This method will check to see if the threshold was met and will prompt the user
 with the thresholdPopup.
 */
    function thresholdChecker(id) {

        // Get the value and determine if it is >= the threshold units previously defined.
        var value = parseInt($("#" + id).val());

        if(value >= thresholdUnits && thresholdUnits != 0)
        {
            //Show the popup message if the selected # of units exceeds the threshold
            $("#thresholdPopupUnits").html(value);
            $("#thresholdCasePack").html($("#casePack").val());
            $("#thresholdPopup").show();
        }
        else
        {
            //Hide the popup message if the selected # of units does not exceed the threshold
            $("#thresholdPopup").hide();
        }
    };

    function thresholdMapChecker(id)
    {
        // Get the value and determine if it is >= the threshold units previously defined.
        var value = parseInt($("#" + id).val());
        var index = id.substring(id.lastIndexOf("_")+1);

        if(value >= thresholdUnitsMap[index] && thresholdUnitsMap.length > 0)
        {
            $("#upgradeLink_" + index).show();
        }
        else
        {
            $("#upgradeLink_" + index).hide();
        }
    };

    function createCookie(name, value, domain, secs, path)
    {
        if(secs)
        {
            var date = new Date();
            date.setTime(date.getTime()+(secs*1000));
            var expires = "; expires="+date.toGMTString();
        }
        else var expires = "";

        document.cookie = name+"="+value+expires+"; path=" + ((path) ? path : "/") + ((domain) ? "; domain=" + domain : "");
    };

    function ensembleThresholdChecker(id, productId, variantIndex) {
        // Get the value and determine if it is >= the threshold units previously defined.
        var value = parseInt($("#" + id).val());
        var unitThreshold = 0;
        var productName = "";
        var casePack = "";
        var vMatrix = get_vMatrix();
        for (var i = 0; i < vMatrix.length; i++) {
            if (vMatrix[i].productId == productId) {
                unitThreshold = vMatrix[i].unitThreshold;
                productName = vMatrix[i].productName;
                casePack = vMatrix[i].casePack;
            }
            //alert(vMatrix[i].productId + ' ' + vMatrix[i].productName + ' ' + vMatrix[i].unitThreshold +  ' ' + vMatrix[i].casePack);
        }

        if(value >= unitThreshold && unitThreshold != 0)
        {
            //Show the popup message if the selected # of units exceeds the threshold
            $("#thresholdPopupUnits").html(value);
            $("#thresholdProductName").html($("#prodStyleName_"+productId).html());
            $("#thresholdCasePack").html(casePack);
            $("#thresholdVariantIndex").html(variantIndex);
            //var inputOffset = $("#" + id).offset();
            //$("#thresholdPopup").css('top',inputOffset.top);
            //$("#thresholdPopup").css('left',inputOffset.left);
            var inputPosition = $("#" + id).position();
            $("#thresholdPopup").css('top',inputPosition.top);
            $("#thresholdPopup").css('left',inputPosition.left);
            $("#thresholdPopup").show();
        }
        else
        {
            //Hide the popup message if the selected # of units does not exceed the threshold
            $("#thresholdPopup").hide();
        }
    };

/*
 This method will take in an id and hide that element by id
 */
    function hideDiv(id) {
        $("#" + id).hide();
    };

/*
 This method is used to change the case quantity to 1
 */
    function upgradeToCase() {
        //Change quantity of case to 1
        var curVal = parseInt($("select[name=numOfCases]").val());

        //set the upgrade boolean flag to true
        $("#upgradeToCase").get(0).value = 'Y';

        if (curVal > 0)
        {
            curVal = curVal + 1;
            $("select[name=numOfCases]").val(curVal);
        }
        else
        {
            $("select[name=numOfCases]").val(1);
        }

        $("#numOfUnits").get(0).selectedIndex = 0;

        //Hide the popup
        hideDiv('thresholdPopup');
    };

    function prdUpgradeToCase() {
        //Change quantity of case to 1
        var curVal = parseInt($("select[name=numOfCases]").val());

        //set the upgrade boolean flag to true
        $("#upgradeToCase").get(0).value = 'Y';

        if (curVal > 0)
        {
            curVal = curVal + 1;
            $("select[name=numOfCases]").val(curVal);
        }
        else
        {
            $("select[name=numOfCases]").val(1);
        }

        $("#numOfUnits").get(0).selectedIndex = 0;

        //Hide the popup
        hideDiv('thresholdPopup');
    };

    function ensUpgradeToCase() {
        var vIndex = parseInt($("#thresholdVariantIndex").html());
        var curVal = parseInt($("#numOfCases" + vIndex).get(0).value);

        //set the upgrade boolean flag to true
        $("#upgradeToCase" + vIndex).get(0).value = 'true';

        if (curVal > 0)
        {
            curVal = curVal + 1;
            $("#numOfCases" + vIndex).get(0).value = curVal;
        }
        else
        {
            $("#numOfCases" + vIndex).get(0).value = 1;
        }

        $("#numOfUnits" + vIndex).get(0).selectedIndex = 0;

        //Hide the popup
        hideDiv('thresholdPopup');
    };

    function setCloseVars()
    {
        clearTimeout(ucHideTimeout);
    };

    var enableCreateAccount = function() {
        $('#createNewAccount').removeClass('input-disabled');
        $('#signIn').addClass('input-disabled');

        $('#password, #userName').val('');
        $('#signDiv .form-field-container').removeClass('form-field-error');
        $('p#userName').css('display', 'none');
        $('p#password').css('display', 'none');
    }

    var enableLogin = function() {
        $('#createNewAccount').addClass('input-disabled');
        $('#signIn').removeClass('input-disabled');
        $('#emailAddress').val('');
        $('#createDiv').removeClass('form-field-error');
        $('p#create').css('display', 'none');
    }

    // quick layer pops for product, ensemble, quickview
    function closeDTlayer(id, selectShowDiv) {
        $('#' + id).hide();
        if (selectShowDiv != null) {
            $('#' + selectShowDiv).css('visibility', 'visible');
        }
    };

    function showDTlayer(id, selectHideDiv) {
        if ($('#' + id).css('display')=='none') {
            $('#' + id).show();
            if (selectHideDiv != null) {
                $('#' + selectHideDiv).css('visibility', 'hidden');
            }
        } else {
            $('#' + id).hide();
            if (selectHideDiv != null) {
                $('#' + selectHideDiv).css('visibility', 'visible');
            }
        }
    };

/**
 * Ajax submission of OCP form - used by template_overlay
 * @param formName - what's in <html:form name value
 * @param callback - function to be executed on success
 */
function submitFormAjax(formName, callback) {
    var params = $("form[name="+formName+"]").serialize();
    $.ajax({
        type: "POST",
        url: $("form[name="+formName+"]").attr("action"),
        data: params,
        success: function(data) {
            callback(data);
        }
    });
    return false;
};

function bindFormSubmit(formId) {
    $("#"+formId).unbind("submit.bindFormSubmit").bind("submit.bindFormSubmit", function(e){
        var parentUrl = $(this).find("#parentUrl").val();
        e.preventDefault();
        if ($(this).find(".jsFormSubmit").hasClass('input-disabled')) {
            return false;
        } else {
            submitFormAjax(formId,function(data){
                iframedAjaxSuccess(data, formId, parentUrl);
            });
        }
    });
};

function bindFormButtons() {
    $(".jsFormSubmit").unbind("click.jsFormSubmit").bind("click.jsFormSubmit", function(e){
        e.preventDefault();
        $(this).parents("form").submit();
    });



    $('#closeme').unbind("click.closeme").bind("click.closeme", function(e) {
        e.preventDefault();
        var path= $("#parentUrl").val();
        closeParentColorbox(path);
    });

    //BEGIN LOGIN/CREATE ACCOUNT BUTTON ENABLE/DISABLE
    $('#signIn, #createNewAccount').addClass('input-disabled');

    if ($.trim($('input#emailAddress').val()) != '') {
        enableCreateAccount();
        $('#createNewAccount').focus();
    };
    if (($.trim($('input#password').val())) != '' && ($.trim($('input#userName').val()) != '')) {
        enableLogin();
        $('#signIn').focus();
    };

    $('input#emailAddress').bind('focus', enableCreateAccount);
    $('input#password').bind('focus', enableLogin);
    $('input#userName').bind('focus', enableLogin);

    //END LOGIN/CREATE ACCOUNT BUTTON ENABLE/DISABLE

};

function iframedAjaxSuccess(data, errorWrapId, parentUrl) {
    //no errors, json object
    if (typeof data == "object") {
        var dest = data.dest;
        var status = data.status;
        if (status == "complete") {
            //ENH00952: TEALIUM for Coremetrics (registration)
            if(errorWrapId == 'memberForm') {
                var x = {
                    event_type: "registration",
                    customer_email: $('input[name=emailAddress]').val(),
                    customer_zip: $('input[name=zipCode]').val(),
                    customer_country: "US"
                };

                if ($('input[name=city]').val().length > 0) {
                    x.customer_city = $('input[name=city]').val();
                }

                if ($('select[name=state]').val().length>0) {
                    x.customer_state = $('select[name=state]').val();
                }
                utag.link(x);
            }
            //close this layer & refresh parent
            ajaxCloseParentColorbox(parentUrl);
            window.parent.location = dest;
        } else {
            window.location = dest;
        }
    } else {
      var $popupContentRoot = $("#pop_content_wrap > div.inner");
      $popupContentRoot.html(data);
      if ($('#recaptcha_div').length > 0) {
        showRecaptcha('6Lf8ieESAAAAAKpFV9yrjnoWZirKdAAV7XScSsnR','recaptcha_div');
      }
    }
};

function showRecaptcha(public_key, element) {
    Recaptcha.create(public_key, element, {
        theme: "white",
        callback: Recaptcha.focus_response_field});
};

//colorbox parent messages
function ajaxCloseParentColorbox(parentUrl) {
            //console.log("ajaxCloseParentColorbox parentUrl: " + parentUrl);
    $.postMessage('colorbox_close', parentUrl, parent);

};

function closeParentColorbox(parentUrl) {
    //console.log("closeParentColorbox parentUrl: " + parentUrl);
    
        if((parentUrl.indexOf("https") > -1) || (parentUrl.indexOf("user") > -1)  || (parentUrl.indexOf("reviews") > -1)) {
            $.postMessage('redirect_to_home', parentUrl, parent);
        } else {
            $.postMessage('colorbox_close', parentUrl, parent);
        }
    
};

function resizeParentColorbox(obj) {
    $.postMessage('colorbox_resize:' + JSON.stringify(obj) );
};

//corresponding colorbox child messages


$.receiveMessage(function(e){
        if (e.data.indexOf("colorbox_close") > -1) {
            $.colorbox.close();
        } else if (e.data.indexOf("redirect_to_vsc") > -1) {
            window.location.href = 'http://www.dollartree.com/custserv/custserv.jsp?pageName=ValueSeekersClub';
        } else if (e.data.indexOf("redirect_to_home") > -1) {
            window.location.href = '../home.jsp.htm'/*tpa=http://www.dollartree.com/home.jsp*/;
        } else if (e.data.indexOf("colorbox_resize") > -1) {
            //this part of the script needs to be finished
            //console.log("colorbox_resize message received!");
        }
    }
);

//http://jqueryfordesigners.com/coda-popup-bubbles/
function getTooltipReady() {
    $('.bubbleInfo').each(function () {
        // options
        var distance = 10;
        var time = 250;
        var hideDelay = 800;

        var hideDelayTimer = null;

        // tracker
        var beingShown = false;
        var shown = false;

        var trigger = $('.trigger', this);
        var popupTooltip = $('.popupTooltip', this).css('opacity', 0);

        // set the mouseover and mouseout on both element
        
        $([trigger.get(0), popupTooltip.get(0)]).click(function () {
            // stops the hide event if we move from the trigger to the popupTooltip element
            if (hideDelayTimer) clearTimeout(hideDelayTimer);

            // don't trigger the animation again if we're being shown, or already visible
            if (beingShown || shown) {
                return;
            } else {
                        beingShown = true;

                        // reset position of popupTooltip box
                        popupTooltip.css({
                            //top: -100,
                            top: -130,
                            left: -33,
                            display: 'block' // brings the popupTooltip back in to view
                        })

                            // (we're using chaining on the popupTooltip) now animate it's opacity and position
                                .animate({
                                    top: '-=' + distance + 'px',
                                    opacity: 1
                                }, time, 'swing', function() {
                                    // once the animation is complete, set the tracker variables
                                    beingShown = false;
                                    shown = true;
                                });
                    }
                }).mouseout(function () {
                            // reset the timer if we get fired again - avoids double animations
                            if (hideDelayTimer) clearTimeout(hideDelayTimer);

                            // store the timer so that it can be cleared in the mouseover if required
                            hideDelayTimer = setTimeout(function () {
                                hideDelayTimer = null;
                                popupTooltip.animate({
                                    top: '-=' + distance + 'px',
                                    opacity: 0
                                }, time, 'swing', function () {
                                    // once the animate is complete, set the tracker variables
                                    shown = false;
                                    // hide the popupTooltip entirely after the effect (opacity alone doesn't do the job)
                                    popupTooltip.css('display', 'none');
                                });
                            }, hideDelay);
                        });
            });
        };

$(document).ready(function(){
    // for login link in header_main
    $(".jsLoginLink").click(function(e){
        e.preventDefault();
        $('#deliveryOptionsWrap').css('display','none');
        var href = $(this).attr("href");
        var paramSep = (href.indexOf("?") > 0) ? "&" : "?";
        var parentUrl = window.location.href;
        if(parentUrl.indexOf("user/logout") > 0){
            parentUrl = parentUrl.substring(-7,parentUrl.indexOf("user/logout")) + "home.jsp";
        }
        //console.log("parentUrl=" + parentUrl);
        $.colorbox({
            href: href + paramSep + "parentUrl=" + parentUrl,
            width: 682,
            height: 550,
            iframe:true
        });
    });

  $("body").on("click", ".jsZipLink", function(e){
        e.preventDefault();
        $('#deliveryOptionsWrap').css('display','none');
        var href = $(this).attr("href");
        var paramSep = (href.indexOf("?") > 0) ? "&" : "?";
        var parentUrl = window.location.href;
        //console.log('jsZipLink = ' + href + paramSep + "parentUrl=" + window.location.href);
        $.colorbox({
            href: href + paramSep + "parentUrl=" + parentUrl,
            width: 395,
            height: 170,
            overlayClose: true,
            iframe:true

        });
    });

     function checkField(){
         var zipFieldVal = $('#usrZip').val();
         if(zipFieldVal === "" || !zipFieldVal.match(/^\d+$/)){
            $('.zipOverlay').css('display','block');
         }else{
            $('.zipOverlay').css('display','none');
         }
     }
});