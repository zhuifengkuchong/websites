/**
 * Cookie management
 */
function createCookie(name, value, domain, secs, path) {
	if (secs) {
		var date = new Date();
		date.setTime(date.getTime()+(secs*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";

	document.cookie = name+"="+value+expires+"; path=" + ((path) ? path : "/") + ((domain) ? "; domain=" + domain : "");
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name, domain, path) {
	createCookie(name, "", domain, -1, path);
}

function DelCookie (name, path, domain) {
	eraseCookie(name, domain, path);
}

/*
* Tab management
*/
function resetAllTabs() {
	var allTabs = $("ul#universalTabs li");
	allTabs.removeClass("selected");
	$("ul#universalTabs li.css_dt_us img").attr("src","../assets/images/common/tabs_dt.png"/*tpa=http://www.dollartree.com/assets/images/common/tabs_dt.png*/);
	$("ul#universalTabs li.css_deals_us img").attr("src","../assets/images/common/tabs_deals.png"/*tpa=http://www.dollartree.com/assets/images/common/tabs_deals.png*/);
	$("ul#universalTabs li.css_dl_es img").attr("src","Unknown_83_filename"/*tpa=http://www.dollartree.com/assets/images/common/tabs_dl_es.png*/);
	$("ul#universalTabs li.css_dt_ca img").attr("src","../assets/images/common/tabs_dt_ca.png"/*tpa=http://www.dollartree.com/assets/images/common/tabs_dt_ca.png*/);
	$("ul#universalTabs li.css_corporate img").attr("src","../assets/images/common/tabs_corporate.png"/*tpa=http://www.dollartree.com/assets/images/common/tabs_corporate.png*/);
}

/**
 * Method removing param and its value in the url - when it appears once
 */
function removeParamValue(url, param) {
    url = url.replace("?", "");
    var params = url.split('&');
    var newUrl = '';
    for (var i = 0; i < params.length; i++)
        if (params[i].indexOf(param) == -1 && params[i] != '') {
            if (params[i].indexOf('?') >= 0)
                newUrl += params[i];
            else
                newUrl += '&' + params[i];
        }
    if (newUrl.charAt(0) == "&")
        newUrl = newUrl.substr(1);

    return "?" + newUrl;
}

/**
 *  Universal Document Ready
 */

$(document).ready(function() {

	//logic to select espanol and canada tabs
	var parts = location.hostname.split('.');
	var subdomain = parts.shift();
	var upperleveldomain = parts.join('.');

	if(subdomain == "espanol"){
		resetAllTabs();
		$("ul#universalTabs li.css_dt_es").addClass("selected");
		$("ul#universalTabs li.css_dt_es a img").attr("src","../assets/images/common/tabs_dt_es_active.png"/*tpa=http://www.dollartree.com/assets/images/common/tabs_dt_es_active.png*/);
	} else if(upperleveldomain == "http://www.dollartree.com/js/dollartreecanada.com"){
		resetAllTabs();
		$("ul#universalTabs li.css_dt_ca").addClass("selected");
		$("ul#universalTabs li.css_dt_ca a img").attr("src","../assets/images/common/tabs_dt_ca_active.png"/*tpa=http://www.dollartree.com/assets/images/common/tabs_dt_ca_active.png*/);
	} else if(upperleveldomain == "http://www.dollartree.com/js/dollartreeinfo.com"){
		resetAllTabs();
		$("ul#universalTabs li.css_corporate").addClass("selected");
		$("ul#universalTabs li.css_corporate a img").attr("src","../assets/images/common/tabs_corporate_active.png"/*tpa=http://www.dollartree.com/assets/images/common/tabs_corporate_active.png*/);
	}

	// show breadcrumb div if not empty
	if($('#breadcrumb').length)	{ } else { $('#breadcrumb_wrap').height(10); }

	// show only 7 top level categories
	var list = $("ul#menu > li:gt(8)");
	list.hide();
	$("ul#menu li.menu_left:last").css('border-right','none');
	$("ul#menu li.menu_left:last").css('background-image','none');

    // Set up columns for mega menu
    //    DO NOT DELETE - COLUMNIZER SCRIPT

     $('ul#menu .item0 #ideasNavigation ul.complex').each(function(index) {
     var listLength = $('li',this).length;
     var threshhold = 4;
     if(listLength) {
     var mylistitems = $('li', this);
     // one column layout
     if(listLength < threshhold) {
     $(this).wrap('<div class="col_1" />');
     totalWidth = $(this).outerWidth()+ 30;
     $(this).parent().parent().find('.megaArea').each(function() {
     totalWidth += $(this).outerWidth();
     });
     $(this).parent().parent().width(totalWidth);

     }
     // multi column layout
     if(listLength >= threshhold) {
     var totalWidth = 0;
     var $pArr = mylistitems;
     var pArrLen = $pArr.length;
     var pPerDiv = threshhold;
     for (var i = 0;i < pArrLen;i+=pPerDiv){
     $pArr.filter(':eq('+i+'),:lt('+(i+pPerDiv)+'):gt('+i+')').wrapAll('<div class="col_1" />').wrapAll('<ul class="complex" />');
     }

     var numCols = 0;
     $('ul.complex', this).each(function() {
     totalWidth += $(this).outerWidth()+ 10;
     numCols += 1;
     });

     $(this).parent().find('.megaArea').each(function() {
     totalWidth += $(this).outerWidth();
     });

     $(this).parent().removeClass('dropdown_1column').addClass('dropdown_'+numCols+'column').css('width',totalWidth+'px');
     $(this).replaceWith($(this).html());
     }
     } else {
     $(this).parent().remove();
     }
     });
    //    DO NOT DELETE - COLUMNIZER SCRIPT



    // Set up columns for mega menu
        /*
    $('ul#menu ul.simple').each(function(index) {
        var listLength = $('li',this).length;
        var threshhold = 4;
        if(listLength) {
            var mylistitems = $('li', this);
            // one column layout
            if(listLength < threshhold) {
                $(this).wrap('<div class="col_1" />');
                totalWidth = $(this).outerWidth()+ 30;
                $(this).parent().parent().find('.megaArea').each(function() {
                    totalWidth += $(this).outerWidth();
                });
                $(this).parent().parent().width(totalWidth);

            }
            // multi column layout
            if(listLength >= threshhold) {
                var totalWidth = 0;
                var $pArr = mylistitems;
                var pArrLen = $pArr.length;
                var pPerDiv = threshhold;
                for (var i = 0;i < pArrLen;i+=pPerDiv){
                    $pArr.filter(':eq('+i+'),:lt('+(i+pPerDiv)+'):gt('+i+')').wrapAll('<div class="col_1" />').wrapAll('<ul class="simple" />');
                }

                var numCols = 0;
                $('ul.simple', this).each(function() {
                    totalWidth += $(this).outerWidth()+ 10;
                    numCols += 1;
                });

                $(this).parent().find('.megaArea').each(function() {
                    totalWidth += $(this).outerWidth();
                });

                $(this).parent().removeClass('dropdown_1column').addClass('dropdown_'+numCols+'column').css('width',totalWidth+'px');
                $(this).replaceWith($(this).html());
            }
        } else {
            $(this).parent().remove();
        }
    });
                   */

    $('ul#menu li').each(function() {
		//hides last border
		$('.col_1:last', this).css('border','none');

		var maxHeight = -1;
		$('.mm_wrap .col_1', this).each(function() {
			maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
		});

		$('.mm_wrap .col_1', this).each(function() {
		 $(this).height(maxHeight);
	   });

	});

	    // mega menu left position
        var siteWidth  = $('#outer_wrap').outerWidth();    // 980px

        $("ul#menu li").hover(function () {
            var initialLeft = $(this).position().left;
            var offsetLeft = $(this).offset().left;
            var menuWidth = $("div.align_left",this).width();
            if(initialLeft+menuWidth > siteWidth) {
                var newOffset = siteWidth-initialLeft-menuWidth;
                $("div.align_left",this).css('left',newOffset);
            } else {
                $("div.align_left",this).css('left','');
            }
        },
        function () {
            $("div.align_left",this).css('left','');
        }
	);
}); //end (document).ready