<script id = "race251b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race251={};
	myVars.races.race251.varName="Object[3531].position";
	myVars.races.race251.varType="@varType@";
	myVars.races.race251.repairType = "@RepairType";
	myVars.races.race251.event1={};
	myVars.races.race251.event2={};
	myVars.races.race251.event1.id = "Lu_Id_li_120";
	myVars.races.race251.event1.type = "onmouseover";
	myVars.races.race251.event1.loc = "Lu_Id_li_120_LOC";
	myVars.races.race251.event1.isRead = "True";
	myVars.races.race251.event1.eventType = "@event1EventType@";
	myVars.races.race251.event2.id = "Lu_Id_li_13";
	myVars.races.race251.event2.type = "onmouseover";
	myVars.races.race251.event2.loc = "Lu_Id_li_13_LOC";
	myVars.races.race251.event2.isRead = "False";
	myVars.races.race251.event2.eventType = "@event2EventType@";
	myVars.races.race251.event1.executed= false;// true to disable, false to enable
	myVars.races.race251.event2.executed= false;// true to disable, false to enable
</script>

