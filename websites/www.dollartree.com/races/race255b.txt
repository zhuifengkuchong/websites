<script id = "race255b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race255={};
	myVars.races.race255.varName="Object[3531].position";
	myVars.races.race255.varType="@varType@";
	myVars.races.race255.repairType = "@RepairType";
	myVars.races.race255.event1={};
	myVars.races.race255.event2={};
	myVars.races.race255.event1.id = "Lu_Id_li_124";
	myVars.races.race255.event1.type = "onmouseover";
	myVars.races.race255.event1.loc = "Lu_Id_li_124_LOC";
	myVars.races.race255.event1.isRead = "True";
	myVars.races.race255.event1.eventType = "@event1EventType@";
	myVars.races.race255.event2.id = "Lu_Id_li_13";
	myVars.races.race255.event2.type = "onmouseover";
	myVars.races.race255.event2.loc = "Lu_Id_li_13_LOC";
	myVars.races.race255.event2.isRead = "False";
	myVars.races.race255.event2.eventType = "@event2EventType@";
	myVars.races.race255.event1.executed= false;// true to disable, false to enable
	myVars.races.race255.event2.executed= false;// true to disable, false to enable
</script>

