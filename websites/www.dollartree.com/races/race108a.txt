<script id = "race108a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race108={};
	myVars.races.race108.varName="Lu_Id_li_95__onmouseover";
	myVars.races.race108.varType="@varType@";
	myVars.races.race108.repairType = "@RepairType";
	myVars.races.race108.event1={};
	myVars.races.race108.event2={};
	myVars.races.race108.event1.id = "Lu_DOM";
	myVars.races.race108.event1.type = "onDOMContentLoaded";
	myVars.races.race108.event1.loc = "Lu_DOM_LOC";
	myVars.races.race108.event1.isRead = "False";
	myVars.races.race108.event1.eventType = "@event1EventType@";
	myVars.races.race108.event2.id = "Lu_Id_li_114";
	myVars.races.race108.event2.type = "onmouseover";
	myVars.races.race108.event2.loc = "Lu_Id_li_114_LOC";
	myVars.races.race108.event2.isRead = "True";
	myVars.races.race108.event2.eventType = "@event2EventType@";
	myVars.races.race108.event1.executed= false;// true to disable, false to enable
	myVars.races.race108.event2.executed= false;// true to disable, false to enable
</script>

