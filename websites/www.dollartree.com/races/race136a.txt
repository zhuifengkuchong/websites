<script id = "race136a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race136={};
	myVars.races.race136.varName="[0x7f08e0dfd700].blur";
	myVars.races.race136.varType="@varType@";
	myVars.races.race136.repairType = "@RepairType";
	myVars.races.race136.event1={};
	myVars.races.race136.event2={};
	myVars.races.race136.event1.id = "Lu_DOM";
	myVars.races.race136.event1.type = "onDOMContentLoaded";
	myVars.races.race136.event1.loc = "Lu_DOM_LOC";
	myVars.races.race136.event1.isRead = "False";
	myVars.races.race136.event1.eventType = "@event1EventType@";
	myVars.races.race136.event2.id = "sli_search_1";
	myVars.races.race136.event2.type = "onblur";
	myVars.races.race136.event2.loc = "sli_search_1_LOC";
	myVars.races.race136.event2.isRead = "True";
	myVars.races.race136.event2.eventType = "@event2EventType@";
	myVars.races.race136.event1.executed= false;// true to disable, false to enable
	myVars.races.race136.event2.executed= false;// true to disable, false to enable
</script>

