<script id = "race59a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race59={};
	myVars.races.race59.varName="Lu_Id_li_57__onmouseover";
	myVars.races.race59.varType="@varType@";
	myVars.races.race59.repairType = "@RepairType";
	myVars.races.race59.event1={};
	myVars.races.race59.event2={};
	myVars.races.race59.event1.id = "Lu_DOM";
	myVars.races.race59.event1.type = "onDOMContentLoaded";
	myVars.races.race59.event1.loc = "Lu_DOM_LOC";
	myVars.races.race59.event1.isRead = "False";
	myVars.races.race59.event1.eventType = "@event1EventType@";
	myVars.races.race59.event2.id = "Lu_Id_li_65";
	myVars.races.race59.event2.type = "onmouseover";
	myVars.races.race59.event2.loc = "Lu_Id_li_65_LOC";
	myVars.races.race59.event2.isRead = "True";
	myVars.races.race59.event2.eventType = "@event2EventType@";
	myVars.races.race59.event1.executed= false;// true to disable, false to enable
	myVars.races.race59.event2.executed= false;// true to disable, false to enable
</script>

