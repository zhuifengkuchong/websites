<script id = "race2b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race2={};
	myVars.races.race2.varName="sli_search_1__onkeyup";
	myVars.races.race2.varType="@varType@";
	myVars.races.race2.repairType = "@RepairType";
	myVars.races.race2.event1={};
	myVars.races.race2.event2={};
	myVars.races.race2.event1.id = "sli_search_1";
	myVars.races.race2.event1.type = "onkeyup";
	myVars.races.race2.event1.loc = "sli_search_1_LOC";
	myVars.races.race2.event1.isRead = "True";
	myVars.races.race2.event1.eventType = "@event1EventType@";
	myVars.races.race2.event2.id = "_script_sli-rac.stub.1.6.js";
	myVars.races.race2.event2.type = "onclick";
	myVars.races.race2.event2.loc = "_script_sli-rac.stub.1.6.js_LOC";
	myVars.races.race2.event2.isRead = "False";
	myVars.races.race2.event2.eventType = "@event2EventType@";
	myVars.races.race2.event1.executed= false;// true to disable, false to enable
	myVars.races.race2.event2.executed= false;// true to disable, false to enable
</script>

