<script id = "race239b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race239={};
	myVars.races.race239.varName="Object[3531].position";
	myVars.races.race239.varType="@varType@";
	myVars.races.race239.repairType = "@RepairType";
	myVars.races.race239.event1={};
	myVars.races.race239.event2={};
	myVars.races.race239.event1.id = "Lu_Id_li_109";
	myVars.races.race239.event1.type = "onmouseover";
	myVars.races.race239.event1.loc = "Lu_Id_li_109_LOC";
	myVars.races.race239.event1.isRead = "True";
	myVars.races.race239.event1.eventType = "@event1EventType@";
	myVars.races.race239.event2.id = "Lu_Id_li_13";
	myVars.races.race239.event2.type = "onmouseover";
	myVars.races.race239.event2.loc = "Lu_Id_li_13_LOC";
	myVars.races.race239.event2.isRead = "False";
	myVars.races.race239.event2.eventType = "@event2EventType@";
	myVars.races.race239.event1.executed= false;// true to disable, false to enable
	myVars.races.race239.event2.executed= false;// true to disable, false to enable
</script>

