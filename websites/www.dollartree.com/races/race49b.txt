<script id = "race49b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race49={};
	myVars.races.race49.varName="Lu_Id_li_36__onmouseover";
	myVars.races.race49.varType="@varType@";
	myVars.races.race49.repairType = "@RepairType";
	myVars.races.race49.event1={};
	myVars.races.race49.event2={};
	myVars.races.race49.event1.id = "Lu_Id_li_55";
	myVars.races.race49.event1.type = "onmouseover";
	myVars.races.race49.event1.loc = "Lu_Id_li_55_LOC";
	myVars.races.race49.event1.isRead = "True";
	myVars.races.race49.event1.eventType = "@event1EventType@";
	myVars.races.race49.event2.id = "Lu_DOM";
	myVars.races.race49.event2.type = "onDOMContentLoaded";
	myVars.races.race49.event2.loc = "Lu_DOM_LOC";
	myVars.races.race49.event2.isRead = "False";
	myVars.races.race49.event2.eventType = "@event2EventType@";
	myVars.races.race49.event1.executed= false;// true to disable, false to enable
	myVars.races.race49.event2.executed= false;// true to disable, false to enable
</script>

