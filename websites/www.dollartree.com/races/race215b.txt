<script id = "race215b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race215={};
	myVars.races.race215.varName="Object[3531].position";
	myVars.races.race215.varType="@varType@";
	myVars.races.race215.repairType = "@RepairType";
	myVars.races.race215.event1={};
	myVars.races.race215.event2={};
	myVars.races.race215.event1.id = "Lu_Id_li_86";
	myVars.races.race215.event1.type = "onmouseover";
	myVars.races.race215.event1.loc = "Lu_Id_li_86_LOC";
	myVars.races.race215.event1.isRead = "True";
	myVars.races.race215.event1.eventType = "@event1EventType@";
	myVars.races.race215.event2.id = "Lu_Id_li_13";
	myVars.races.race215.event2.type = "onmouseover";
	myVars.races.race215.event2.loc = "Lu_Id_li_13_LOC";
	myVars.races.race215.event2.isRead = "False";
	myVars.races.race215.event2.eventType = "@event2EventType@";
	myVars.races.race215.event1.executed= false;// true to disable, false to enable
	myVars.races.race215.event2.executed= false;// true to disable, false to enable
</script>

