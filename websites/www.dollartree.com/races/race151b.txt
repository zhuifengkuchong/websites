<script id = "race151b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race151={};
	myVars.races.race151.varName="Object[3531].position";
	myVars.races.race151.varType="@varType@";
	myVars.races.race151.repairType = "@RepairType";
	myVars.races.race151.event1={};
	myVars.races.race151.event2={};
	myVars.races.race151.event1.id = "Lu_Id_li_26";
	myVars.races.race151.event1.type = "onmouseover";
	myVars.races.race151.event1.loc = "Lu_Id_li_26_LOC";
	myVars.races.race151.event1.isRead = "True";
	myVars.races.race151.event1.eventType = "@event1EventType@";
	myVars.races.race151.event2.id = "Lu_Id_li_13";
	myVars.races.race151.event2.type = "onmouseover";
	myVars.races.race151.event2.loc = "Lu_Id_li_13_LOC";
	myVars.races.race151.event2.isRead = "False";
	myVars.races.race151.event2.eventType = "@event2EventType@";
	myVars.races.race151.event1.executed= false;// true to disable, false to enable
	myVars.races.race151.event2.executed= false;// true to disable, false to enable
</script>

