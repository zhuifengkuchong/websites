<script id = "race256a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race256={};
	myVars.races.race256.varName="Object[3531].position";
	myVars.races.race256.varType="@varType@";
	myVars.races.race256.repairType = "@RepairType";
	myVars.races.race256.event1={};
	myVars.races.race256.event2={};
	myVars.races.race256.event1.id = "Lu_Id_li_13";
	myVars.races.race256.event1.type = "onmouseover";
	myVars.races.race256.event1.loc = "Lu_Id_li_13_LOC";
	myVars.races.race256.event1.isRead = "False";
	myVars.races.race256.event1.eventType = "@event1EventType@";
	myVars.races.race256.event2.id = "Lu_Id_li_125";
	myVars.races.race256.event2.type = "onmouseover";
	myVars.races.race256.event2.loc = "Lu_Id_li_125_LOC";
	myVars.races.race256.event2.isRead = "True";
	myVars.races.race256.event2.eventType = "@event2EventType@";
	myVars.races.race256.event1.executed= false;// true to disable, false to enable
	myVars.races.race256.event2.executed= false;// true to disable, false to enable
</script>

