<script id = "race128a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race128={};
	myVars.races.race128.varName="Object[212857].lastValue";
	myVars.races.race128.varType="@varType@";
	myVars.races.race128.repairType = "@RepairType";
	myVars.races.race128.event1={};
	myVars.races.race128.event2={};
	myVars.races.race128.event1.id = "sli_search_1";
	myVars.races.race128.event1.type = "onkeydown";
	myVars.races.race128.event1.loc = "sli_search_1_LOC";
	myVars.races.race128.event1.isRead = "False";
	myVars.races.race128.event1.eventType = "@event1EventType@";
	myVars.races.race128.event2.id = "sli_search_1";
	myVars.races.race128.event2.type = "onkeyup";
	myVars.races.race128.event2.loc = "sli_search_1_LOC";
	myVars.races.race128.event2.isRead = "True";
	myVars.races.race128.event2.eventType = "@event2EventType@";
	myVars.races.race128.event1.executed= false;// true to disable, false to enable
	myVars.races.race128.event2.executed= false;// true to disable, false to enable
</script>

