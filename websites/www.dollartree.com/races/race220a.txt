<script id = "race220a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race220={};
	myVars.races.race220.varName="Object[3531].position";
	myVars.races.race220.varType="@varType@";
	myVars.races.race220.repairType = "@RepairType";
	myVars.races.race220.event1={};
	myVars.races.race220.event2={};
	myVars.races.race220.event1.id = "Lu_Id_li_13";
	myVars.races.race220.event1.type = "onmouseover";
	myVars.races.race220.event1.loc = "Lu_Id_li_13_LOC";
	myVars.races.race220.event1.isRead = "False";
	myVars.races.race220.event1.eventType = "@event1EventType@";
	myVars.races.race220.event2.id = "Lu_Id_li_91";
	myVars.races.race220.event2.type = "onmouseover";
	myVars.races.race220.event2.loc = "Lu_Id_li_91_LOC";
	myVars.races.race220.event2.isRead = "True";
	myVars.races.race220.event2.eventType = "@event2EventType@";
	myVars.races.race220.event1.executed= false;// true to disable, false to enable
	myVars.races.race220.event2.executed= false;// true to disable, false to enable
</script>

