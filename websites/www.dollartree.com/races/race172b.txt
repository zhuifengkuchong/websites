<script id = "race172b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race172={};
	myVars.races.race172.varName="Object[3531].position";
	myVars.races.race172.varType="@varType@";
	myVars.races.race172.repairType = "@RepairType";
	myVars.races.race172.event1={};
	myVars.races.race172.event2={};
	myVars.races.race172.event1.id = "Lu_Id_li_45";
	myVars.races.race172.event1.type = "onmouseover";
	myVars.races.race172.event1.loc = "Lu_Id_li_45_LOC";
	myVars.races.race172.event1.isRead = "True";
	myVars.races.race172.event1.eventType = "@event1EventType@";
	myVars.races.race172.event2.id = "Lu_Id_li_13";
	myVars.races.race172.event2.type = "onmouseover";
	myVars.races.race172.event2.loc = "Lu_Id_li_13_LOC";
	myVars.races.race172.event2.isRead = "False";
	myVars.races.race172.event2.eventType = "@event2EventType@";
	myVars.races.race172.event1.executed= false;// true to disable, false to enable
	myVars.races.race172.event2.executed= false;// true to disable, false to enable
</script>

