<script id = "race178a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race178={};
	myVars.races.race178.varName="Object[3531].position";
	myVars.races.race178.varType="@varType@";
	myVars.races.race178.repairType = "@RepairType";
	myVars.races.race178.event1={};
	myVars.races.race178.event2={};
	myVars.races.race178.event1.id = "Lu_Id_li_13";
	myVars.races.race178.event1.type = "onmouseover";
	myVars.races.race178.event1.loc = "Lu_Id_li_13_LOC";
	myVars.races.race178.event1.isRead = "False";
	myVars.races.race178.event1.eventType = "@event1EventType@";
	myVars.races.race178.event2.id = "Lu_Id_li_51";
	myVars.races.race178.event2.type = "onmouseover";
	myVars.races.race178.event2.loc = "Lu_Id_li_51_LOC";
	myVars.races.race178.event2.isRead = "True";
	myVars.races.race178.event2.eventType = "@event2EventType@";
	myVars.races.race178.event1.executed= false;// true to disable, false to enable
	myVars.races.race178.event2.executed= false;// true to disable, false to enable
</script>

