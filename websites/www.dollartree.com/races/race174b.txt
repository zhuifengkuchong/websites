<script id = "race174b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race174={};
	myVars.races.race174.varName="Object[3531].position";
	myVars.races.race174.varType="@varType@";
	myVars.races.race174.repairType = "@RepairType";
	myVars.races.race174.event1={};
	myVars.races.race174.event2={};
	myVars.races.race174.event1.id = "Lu_Id_li_47";
	myVars.races.race174.event1.type = "onmouseover";
	myVars.races.race174.event1.loc = "Lu_Id_li_47_LOC";
	myVars.races.race174.event1.isRead = "True";
	myVars.races.race174.event1.eventType = "@event1EventType@";
	myVars.races.race174.event2.id = "Lu_Id_li_13";
	myVars.races.race174.event2.type = "onmouseover";
	myVars.races.race174.event2.loc = "Lu_Id_li_13_LOC";
	myVars.races.race174.event2.isRead = "False";
	myVars.races.race174.event2.eventType = "@event2EventType@";
	myVars.races.race174.event1.executed= false;// true to disable, false to enable
	myVars.races.race174.event2.executed= false;// true to disable, false to enable
</script>

