/*Function to create the custom drop down. This is an update of the older used on other pages. 
$mySelect: jQuery object of the original select box.
$myCustom: jQuery object of the custom select box.
*/
function createCustomSelect($mySelect, $myCustom){
    if (!window.opera){
        $mySelect.css({"opacity": 0, "khtml-appearance": "none", "-webkit-appearance": "menulist-button"});
        $mySelect.on("change", function(){
            $myCustom.find(".selectText").html($mySelect.val());
        });
    }
}

if(location.pathname.indexOf("http://www.jpmorganchase.com/content/dam/jpmorganchase/en/legacy/corporate/includes/javascript/financial-capability-grants.htm") !== -1){

	//Variable that will be used often, making vars here to speed up and simplify program.

	//Width in px that the map must be for the map to function.
	var mapBreakPoint = 500;
	//Total number of cities in the JSON.
	var numberOfCities = cityJSON.cities.length;

	//jQuery variables, shortcuts so the DOM does not need to be queried multiple times.
	var $mapWrap = $(".mapWrap");
    var $toolTip = $(".toolTip");
    var $cityList = $(".cityList");
	
	//Populate spans of cities for maps, and list of cities in select box.
	var counter;
	for (counter = 0; counter < numberOfCities; counter++)
	{
		$mapWrap.append("<span class='circle' data-city='"+cityJSON.cities[counter].name+"' style='top:"+cityJSON.cities[counter].top+"%; left:"+cityJSON.cities[counter].left+"%'></span>");
		$cityList .append("<option value='"+cityJSON.cities[counter].name+"'>"+cityJSON.cities[counter].name+"</option>");
	}
	if ($mapWrap.width() > mapBreakPoint)
	{
		$(".circle").addClass("circleEnabled");
	}

    /*if (!window.opera) {
        // For creating custom drop downs.
        $('select').each(function(){
            var title = $('option:selected',this).text();
            $(this)
                .css({'z-index':10,'opacity':0,'-khtml-appearance':'none', 'position':'absolute'})
                .after('<div class="customSelect"><span>' + title + '</span></div><div class="downBox"><div class="dArrow"></div></div>')
                .change(function()
                {
                    val = $('option:selected',this).text();
                    $(this).siblings('div').find('span').html(val);
                });
        });
    }*/

    createCustomSelect($cityList, $(".customSelect"));

	$(".circle").click(function(){
		if ($mapWrap.width() > mapBreakPoint){
			//Add class circleEnabled.
		    var cityName = $(this).attr("data-city");
		    var i;
		    var j;
		    $toolTip.find("h4").empty();
		    $toolTip.find("ul").empty();
		    $toolTip.removeClass("pointRight");
		    $toolTip.removeClass("pointLeft");
		    $(".circle").removeClass("circleSelected");
		    $(".cityInfo").slideUp(function(){
		    	$(".cityInfo").removeClass("cityInfoActive");
		    });
		    $cityList.val("");
		    $(".customSelect span").html("Select a City");
		    for (i=0; i < numberOfCities; i++)
		    {
		    	if (cityJSON.cities[i].name === cityName){
		    		$toolTip.find("h4").html(cityJSON.cities[i].name);
		    		$toolTip.find(".totalAmount").html("Total Grant Amount: "+cityJSON.cities[i].money);

		    		for (j=0; j < cityJSON.cities[i].organizations.length; j++){
		    			$toolTip.find("ul").append("<li>"+cityJSON.cities[i].organizations[j]+"</li>");
		    		}

		    		$toolTip.addClass("toolTipActive");
				    
		    		if (cityJSON.cities[i].left > 59){
		    			$toolTip.addClass("pointRight");
		    			$toolTip.css("left", $(this).position().left - 278);
		    		}
		    		else{
		    			$toolTip.addClass("pointLeft");
		    			$toolTip.css("left", $(this).position().left + 27);
		    		}
		    		$(this).addClass("circleSelected");
		    		$toolTip.css("top", $(this).position().top -70);
		    		$toolTip.find("a.fa-close").focus();
		    	}
		    }
		}
		//Else remove class circleEnabled.
	});

	$cityList.change(function(){
	    var cityVal = $(this).val();
	    var $cityInfo = $(".cityInfo");
	    $cityInfo.find("ul").empty();
	    var i;
	    var j;
	    $(".toolTip").removeClass("toolTipActive");
	    $(".circle").removeClass("circleSelected");

	    for (i = 0; i < numberOfCities; i++ ){
	    	if (cityJSON.cities[i].name === cityVal){
	    		var currCity = cityJSON.cities[i];
	    		$cityInfo.slideDown();
	    		$cityInfo.addClass("cityInfoActive");
	    		$cityInfo.find("http://www.jpmorganchase.com/content/dam/jpmorganchase/en/legacy/corporate/includes/javascript/p .name").html(currCity.name);
	    		$cityInfo.find("p .money").html(currCity.money);

	    		for (j = 0; j < cityJSON.cities[i].organizations.length; j++){
	    			$cityInfo.find("ul").append("<li>"+currCity.organizations[j]+"</li>");
	    		}
    			$(".circle[data-city='"+currCity.name+"']").addClass("circleSelected");
	    	}
	    }
	});

	$(".toolTip a.fa-close").click( function(){
		$(".toolTip").removeClass("toolTipActive");
		$(".circle").removeClass("circleSelected");
		return false;
	});

}