require(['jpmc/$','commonui/component/responsive-menu', 'commonui/component/slide-menu','commonui/component/media-manager' ], function ($, ResponsiveMenu, SlideMenu, MediaManager) {
    var DOMtarget = "#jpmcNav",
        DOMsource = $(DOMtarget).clone(),
        rMenu = (function () {
            var isAttached = false,
                responsiveMenu;
            function attach() {
                if(!isAttached) {
                    responsiveMenu = new ResponsiveMenu({
                        lazy: true,
                        itemSelector: '.jpui-menu-item',
                        menuSelector: '.jpui-menu-item > .jpui-submenu'
                    });
                    $(function () {
                        responsiveMenu.render($(DOMtarget));
                    });
                    isAttached = true;
                }
            }
            function detach() {
                if(isAttached) {
                    $(DOMtarget).replaceWith(DOMsource);
                    DOMsource = $(DOMtarget).clone();
                    responsiveMenu && responsiveMenu.destroy();
                    isAttached = false;
                }
            }
            return {
                attach: attach,
                detach: detach
            };
        }()),
        sMenu = (function () {
            var isAttached = false,
                slideMenu;
            function attach() {
                if(!isAttached) {
                    slideMenu = new SlideMenu();
                    $(function () {
                        slideMenu.render($(DOMtarget));
                    });
                    isAttached = true;
                }
            }
            function detach() {
                if(isAttached) {
                    $(DOMtarget).replaceWith(DOMsource);
                    DOMsource = $(DOMtarget).clone();
                    slideMenu && slideMenu.destroy();
                    isAttached = false;
                }
            }
            return {
                attach: attach,
                detach: detach
            };
        }());

    MediaManager.register({
        large: {
            init: rMenu.attach,
            terminate: rMenu.detach
        },
        medium: {
            init: rMenu.attach,
            terminate: rMenu.detach
        },
        small: {
            init: sMenu.attach,
            terminate: sMenu.detach
        },
        xsmall: {
            init: sMenu.attach,
            terminate: sMenu.detach
        }
    });
});