/*! Distribution 31.1.8  */
$('#external-custom').heliumModal();
$(".external-custom").click(function() {
    var href = $(this).attr('href');
    href = href.toLowerCase();
    if(
        (href.indexOf("http://www.jpmorganchase.com/content/dam/jpmorganchase/en/legacy/corporate/includes/javascript/millenniumweb.com") === -1) && (href.indexOf("http://www.jpmorganchase.com/content/dam/jpmorganchase/en/legacy/corporate/includes/javascript/missionmainstreetgrants.com") === -1) && (href.indexOf("http://www.jpmorganchase.com/content/dam/jpmorganchase/en/legacy/corporate/includes/javascript/chase.com") === -1) && (href.indexOf("http://www.jpmorganchase.com/content/dam/jpmorganchase/en/legacy/corporate/includes/javascript/jpmorganchase.com") === -1) && (href.indexOf("http://www.jpmorganchase.com/content/dam/jpmorganchase/en/legacy/corporate/includes/javascript/chasepaymentech.com") === -1)
    ){
        $('#external-custom a.ok').attr('href', href);
        $('#external-custom').heliumModal('openModal');
        $('#external-custom a.ok').click(function(){
            $('#external-custom').heliumModal('closeModal');
        });
        return false;
    }
});



$('nav i').click(function(){
    var menulist = $(this).siblings('ul');
    var origheight = menulist.height();
    var autoheight = menulist.css('height','auto').height();
    menulist.height(origheight);
    if(origheight !== autoheight){
        menulist.height(autoheight);
        $(this).parents('nav').addClass('active');
    } else {
        menulist.height(0);
        $(this).parents('nav').removeClass('active');
    }
});



$(".mob-title").click(function(){
    if($(document).width() < 673){
        $(this).parents('aside').find('.item').css('height','auto');
        $(this).toggleClass('active');
        $(this).next().slideToggle();
        return false;
    }
});

$(".accordian-title").click(function(){
    if($(document).width() > 673){
        $(this).parents('aside').find('.item').css('height','auto');
        $(this).toggleClass('active');
        $(this).next().slideToggle();
        return false;
    }
});

//Grantee logo tool tips:
$(".col3-grantee-layout .img-wrap .medLogo").click( function(){
    var $descript = $(this).closest(".img-wrap").prev();
    $(".col3-grantee-layout .show-whitebox").removeClass('show-whitebox');
    $descript.addClass('show-whitebox');
    $(".col3-grantee-layout .copy-wrap a.faw-close").focus();
    return false;
});

$(".col3-grantee-layout .copy-wrap a.faw-close").click(function(){
    var $descript = $(this).closest(".copy-wrap");
    $descript.removeClass('show-whitebox');
    $descript.next().find("a").focus();
    return false;
});

//---Awards tool tips---//
// Currently not being used
/*
$(".col3-awards-layout .img-wrap .medLogo").click( function(){
    var $descript = $(this).closest(".img-wrap").prev();
    $(".col3-awards-layout .selectedItem").removeClass('selectedItem');
    $(".col3-awards-layout .locked").removeClass('locked');
    $descript.addClass('selectedItem locked');
    $(".col3-awards-layout .copy-wrap a.faw-close").focus();
    $descript.next().find(".medLogo").addClass("selectedItem locked");
    $descript.css("min-height",$(this).closest(".img-wrap").height());
    return false;
});

$(".col3-awards-layout .img-wrap .medLogo").mouseenter(function() {
    var $descript = $(this).closest(".img-wrap").prev();
    $descript.addClass('selectedItem');
    $descript.next().find(".medLogo").addClass("selectedItem");
    $descript.css("min-height",$(this).closest(".img-wrap").height());
    return false;
});

$(".col3-awards-layout .img-wrap .medLogo").mouseleave(function() {
    var $descript = $(this).closest(".img-wrap").prev();
    if ($descript.hasClass("locked")===false){
        $descript.removeClass("selectedItem");
        $descript.next().find(".medLogo").removeClass("selectedItem locked");
    }
});

$(".col3-awards-layout .copy-wrap a.faw-close").click(function(){
    var $descript = $(this).closest(".copy-wrap");
    $descript.removeClass('selectedItem locked');
    $descript.next().find(".medLogo").removeClass("selectedItem locked");
    $descript.next().find("a").focus();
    return false;
});


$(".col4-awards-layout .img-wrap .medLogo").click( function(){
    var $descript = $(this).closest(".img-wrap").prev();
    $(".col4-awards-layout .selectedItem").removeClass('selectedItem');
    $(".col4-awards-layout .locked").removeClass('locked');
    $descript.addClass('selectedItem locked');
    $(".col4-awards-layout .copy-wrap a.faw-close").focus();
    $descript.next().find(".medLogo").addClass("selectedItem locked");
    $descript.css("min-height",$(this).closest(".img-wrap").height());
    return false;
});

$(".col4-awards-layout .img-wrap .medLogo").mouseenter(function() {
    var $descript = $(this).closest(".img-wrap").prev();
    $descript.addClass('selectedItem');
    $descript.next().find(".medLogo").addClass("selectedItem");
    $descript.css("min-height",$(this).closest(".img-wrap").height());
    return false;
});

$(".col4-awards-layout .img-wrap .medLogo").mouseleave(function() {
    var $descript = $(this).closest(".img-wrap").prev();
    if ($descript.hasClass("locked")===false){
        $descript.removeClass("selectedItem");
        $descript.next().find(".medLogo").removeClass("selectedItem locked");
    }
});

$(".col4-awards-layout .copy-wrap a.faw-close").click(function(){
    var $descript = $(this).closest(".copy-wrap");
    $descript.removeClass('selectedItem locked');
    $descript.next().find(".medLogo").removeClass("selectedItem locked");
    $descript.next().find("a").focus();
    return false;
});
*/
//---- Careers tool tips -------//
var $allBizDetail = $(".bizDetail");
$(".seeBusinesses").click(function(){
    $allBizDetail.removeClass("bizDetail-active openLock");
    var $thisDetail = $(this).closest(".item").find(".bizDetail");
    if ($thisDetail.hasClass("pointLeft")){
        $thisDetail.css("left", ($(this).position().left) + 179 );
    }
    else if ($thisDetail.hasClass("pointRight")){
        $thisDetail.css("left", ($(this).position().left) - 257 );
    }
    $thisDetail.css("top", ($(this).position().top) - 60 ); 
    $thisDetail.addClass("bizDetail-active openLock");
    $thisDetail.find(".faw-close").focus();
    return false;
});

$(".seeBusinesses").mouseenter(function() {
    var $thisDetail = $(this).closest(".item").find(".bizDetail");
    if ($thisDetail.hasClass("pointLeft")){
        $thisDetail.css("left", ($(this).position().left) + 179 );
    }
    else if ($thisDetail.hasClass("pointRight")){
        $thisDetail.css("left", ($(this).position().left) - 257 );
    }
    $thisDetail.css("top", ($(this).position().top) - 60 ); 
    $thisDetail.addClass("bizDetail-active");
    return false;
});

$(".seeBusinesses").mouseleave(function(){
    if ( $(this).closest(".item").find(".bizDetail").hasClass("openLock") === false ){
        $(this).closest(".item").find(".bizDetail").removeClass("bizDetail-active");
    }
    return false;
});

$allBizDetail.find(".faw-close").click(function(){
   $allBizDetail.removeClass("openLock");
    $(this).closest(".cell").find(".seeBusinesses").focus();
    $allBizDetail.removeClass("bizDetail-active");
    return false;
});
///---- End of careers tool tips -----///

// ////------------------------------------ TOP NAV SCRIPTS ---------------------------------------////
// //These use GSAP jquery animation plug in.

//Shortcut variables for the top nav. These save code and memory so the DOM does not have to be querried each time.
var $navWrap = $(".nav-wrap");
var $mainbodyAndFooter = $(".mainbody, body.institute .footerWrapper");
var $mobileSearchLink = $(".mobile-search-link");
var $mobileMenuLink = $(".mobile-expand");
var $menuSections = $("ul.sections");
var $allLevel2 = $(".level2");
var $desktopSearchContainer = $(".nav-wrap .searchContainer");
var $lockedSearchButtonLink = $(".locked-search-link > a");
var $subNav = $("nav#sub");
var subNavHeight;
var subNavOffsetTop;

//Used for calculating position to lock the sub nav.
if ($subNav.length > 0){
    subNavHeight = $subNav.outerHeight();
    subNavOffsetTop = $subNav.offset().top;
}

//Mobile search button.
var $mobileSearchArea = $("nav#top .mobile-search-area");

var $backLink = $("<li class='backLink'><a href='#'><span class='fa fa-chevron-left'></span>Back</a></li>");

/*Point at which we go to mobile view for the nav (in pixels). This can be adjusted however it should be in be between 
the $med and $large breakpoints. Everything pertaining to the nav views should use this, not breakpoints. 
The .desktop-nav or .mobile-nav classes are controled by this. See the navigation style sheet. */
var switchPoint = 757;

var mobileHeaderHeight = $navWrap.outerHeight();
var mobileMenuWidth = 275;

/*Simple function to determine if which view is in use.
Returns true when in desktop view, otherwise returns false.
*/
function isDesktopView(){
    if ( $navWrap.hasClass("desktop-nav") ){
        return true;
    }
    else{
        return false;
    }
}

/*This function updates the backgroundcolor of the mobile menu button.
$button: jQuery object for the mobile menu button. This is usually .mobile-expand.
$matchElement: jQuery object to match the background color of.
copyFontColor: [Optional] Boolean to determine if the link should take the color of the 
    element it is copying.
*/
function updateMenuLinkBG($button, $matchElement, copyFontColor){
    var newColor = $matchElement.css("background-color");
    $button.animate({
        backgroundColor: newColor
    },{ 
        queue: false
    });
    if (copyFontColor === true){
        var newFontColor = $matchElement.find("a").css("color");
        $button.find(".fa").css("color",newFontColor);
        $button.find(".times").css("color",newFontColor);
        //Fallback if GSAP is not enabled.
        $button.css("background-color",newColor);
    }
}

//------------------------------ Mobile Menu Functions -----------------------------------//

function closeMobileMenu($menuTrigger){
    $menuTrigger.addClass("disable");

    $menuTrigger.animate({
        backgroundColor: "transparent"
    },{
        queue: false,
        complete: function(){
            $menuTrigger.removeClass("mobile-menu-active");
            //Fallback if GSAP is not enabled.
            $menuTrigger.css("background-color","transparent");
        }
    });

    $menuTrigger.find(".times").animate({
        opacity: 0
    },{ 
        queue: false
    });

    $menuTrigger.find(".fa-bars").css("color","black");
    $menuTrigger.find(".fa-bars").animate({
        opacity: 1
    },{
        queue: false,
    });

    $menuSections.animate({
        left: (mobileMenuWidth * -1).toString() + "px"
    },{
        queue: true
    });

    $mainbodyAndFooter.animate({
        left: "0px"
    },{
        queue: false,
        complete: function(){
            $navWrap.css("width","");
            $menuTrigger.removeClass("disable");
        }
    });
}


function showMobileSearch($element){

    //Make sure that the mobile header is visible, if the button is somehow selected while this is not visible.
    $navWrap.removeClass('nav-up').addClass('nav-down');
    //Make sure the mobile nav menu is closed.
    closeMobileMenu($mobileMenuLink);
    //Swap the icon in the search button.
    $element.find(".fa-search").animate({
        opacity: 0
    },{
        queue: false,
    });

    $element.find(".times").animate({
        opacity: 1
    },{
        queue: false,
    });
    
    //Animate the background color of the button.
    $element.animate({
        background: "rgb(228, 228, 228)"
    },{
        queue: false,
        complete: function(){
            $element.addClass("mobile-search-active");
        }
    });

    //Get the total distance to animate the search area down and the distance to move the mainbody+footer.
    var topVal = (mobileHeaderHeight + 50).toString();
    topVal = topVal + "px";

    $mainbodyAndFooter.animate({
        top: topVal
    },{
        queue: false
    });

    $mobileSearchArea.animate({
        height: "50px",
    },{
        queue: false,
        complete: function(){
            $mobileSearchArea.addClass("active");
            //When complete remove the inline styled used for animation.
            $mobileSearchArea.css("height","");
        }
    });
}

//Close the mobile search area.
function closeMobileSearch($element){
    $element.find(".fa-search").animate({
        opacity: 1
    },{
        queue: false
    });

    $element.find(".times").animate({
        opacity: 0
    },{
        queue: false
    });
    
    $element.animate({
        backgroundColor: "transparent"
    },{
        queue: false,
        complete: function(){
            $element.removeClass("mobile-search-active");
        }
    });

    //If closing the view due to chaning of views, return to css values.
    if (isDesktopView()){
        $mainbodyAndFooter.css("top","");
    }
    //Otherwise, animate the close, then return to the css values.
    else{
        $mainbodyAndFooter.animate({
            top: mobileHeaderHeight
        },{
            queue: false,
            complete: function(){
                $mainbodyAndFooter.css("top","");
            }
        });
    }

    $mobileSearchArea.animate({
        height: "0px"
    },{
        queue: false,
        complete: function(){
            $mobileSearchArea.removeClass("active");
        }
    });
}


/*Move the top nav on mobile deeper into the menu, after it is opened. (To the right)
$partentLink: jquery object of the link that was clicked. Sibiling of the new UL to go into.
animate: [optional] boolean variable for determining if the item should animate or not when moving. If left out, 
will be treated as 'false'. 
*/
function mobileMenuRight($parentLink, animate){
    var $currentMenu = $parentLink.next("ul");

    $currentMenu.css("left", mobileMenuWidth.toString() + "px");
    $currentMenu.addClass("mobile-display-on");

    //Create the back link. Clone makes sure this a copy, and you are not moving an existing object.
    $backLink.clone().prependTo($currentMenu);

    updateMenuLinkBG($mobileMenuLink, $currentMenu, true);

    if (animate === true){
        $currentMenu.animate({
            left: "0px"
        },{
            queue: false,
            complete: function(){
                $currentMenu.css("left","");
            }
        });
    }
    else{
        $currentMenu.css("left","");
    }

}

/*Move the top nav on mobile black one level. (To the left)
$childLink: jquery object of back link that was clicked. Child of UL to go to.
*/
function mobileMenuLeft($childLink){
    var $leavingMenu = $childLink.closest("ul");

    //Update the color of the X button.
    updateMenuLinkBG($mobileMenuLink, $leavingMenu.parent().closest("ul"), true);

    //Animate the menu moving out of the way.
    $leavingMenu.animate({
        left: mobileMenuWidth.toString() + "px"
    },{
        queue: false,
        complete: function(){
            //Remove the class marking this item as active.
            $leavingMenu.removeClass("mobile-display-on");
            //After the animation is complte, remove the left value from the item.
            $leavingMenu.css("left","");
            //Remove the back button.
            $childLink.closest("li").remove();
        }
    });
}

//------ Functions for shortcutting to items in mobile menu -------//

function jumpToShortCut($topUL){
    var $goToItem = $topUL.find(">li>a.shortCutHere");
    if ($goToItem.length > 0){
        mobileMenuRight($goToItem, false);     
        
        if ($goToItem.next("ul").find(">li>a.shortCutHere").length > 0 ){
            jumpToShortCut($goToItem.next("ul"));
        }
    }
    
}

function addShortCutClass($link){
    $link.addClass("shortCutHere");
    if ($link.closest("ul").is($menuSections)){

    }
    else{
        addShortCutClass($link.closest("ul").prev("a"));
    }
}

function openToItem(itemIDString){
    var $itemID = $("#"+itemIDString);
    if ($itemID.length > 0 ){
        addShortCutClass($itemID);
    }
}

//------- End of shortcut functions ------//

function showMobileMenu($menuTrigger){

    //Turn off the link for toggling the mobileMenu until this function is complete.
    $menuTrigger.addClass("disable");
    //Make sure the mobile nav is down.
    $navWrap.removeClass('nav-up').addClass('nav-down');

    //Close the mobile search area.
    closeMobileSearch($mobileSearchLink);

    $navWrap.css("width",$navWrap.width());

    //Fade in the close button and fade out the bars icon.
    $menuTrigger.find(".times").animate({
        opacity: 1
    },{
        queue: false
    });

    $menuTrigger.find(".fa-bars").animate({
        opacity: 0
    },{
        queue: false
    });

    //Add the class to the mainbody and footer.
    $mainbodyAndFooter.addClass("mobileMenuActive");

    //If there are elements with .mobile-display-on:
    if ( $(".mobile-display-on").length > 0){
        //Get the last item with mobile-display-on.
        var $lastMobileDisplayed = $(".mobile-display-on").last();
        //If it is level 2, change the background color, but not the text color.
        if ($lastMobileDisplayed.hasClass("level2") ){
            updateMenuLinkBG($menuTrigger, $lastMobileDisplayed, true);
        }
        //Otherwise change the background color and text color.
        else{
            updateMenuLinkBG($menuTrigger, $lastMobileDisplayed, false);
        }
        
    }
    //If there are no active items, set the color to the color of the $menuSections.
    else{
        updateMenuLinkBG($menuTrigger, $menuSections, true);
    }

    $mainbodyAndFooter.animate({
        left: mobileMenuWidth.toString() + "px"
    },{
        queue: true,
        duration: 500
    });    

    $menuSections.animate({
        left: "0px"
    },{
        queue: true,
        duration: 500,
        complete: function(){
            $menuTrigger.removeClass("disable");
            $menuTrigger.addClass("mobile-menu-active");
            $mainbodyAndFooter.click(function(){
                if ( $mainbodyAndFooter.hasClass("mobileMenuActive") && ($menuTrigger.hasClass("disable") === false) ){
                    closeMobileMenu($mobileMenuLink);
                }
            });
        }
    });

    jumpToShortCut($menuSections);
    $(".shortCutHere").removeClass("shortCutHere");
}

//Code for when the mobile search button is clicked.
$(".mobile-nav .mobile-search-link a").click(function(event){

    if ( $mobileSearchLink.hasClass("mobile-search-active") ){
        closeMobileSearch($mobileSearchLink);
    }
    else{
        showMobileSearch($mobileSearchLink);
    }
    event.preventDefault();
});

//When the mobile nav menu button is clicked, open it or close it, unless it is disabled.
function mobileExpandClick(){
    if ( $mobileMenuLink.hasClass("disable") === false ){
        if ( $mobileMenuLink.hasClass("mobile-menu-active") ){
            closeMobileMenu($mobileMenuLink);
        }
        else{
            showMobileMenu($mobileMenuLink);
        }
    }
}

//When the mobile expand menu button is clicked, show or hide the menu.
$mobileMenuLink.find("a").on("click", function(event){
    mobileExpandClick();
    event.preventDefault();
});

//Add the arrows next to all items that have child ULs.
$("nav#top .bottomHalf ul.sections li >a").each(function(){
    if ( $(this).next().is("ul") ){
        $(this).append("<span class='fa fa-chevron-right'></span>");
    }
});

//When a mobile menu item is clicked, go to the item if it has a sub item, or follow the link.
$("nav#top .bottomHalf ul.sections li >a").click(function(event){
    //Check if it has a sub item.
    if ( isDesktopView() === false && $(this).next().is("ul") ){
        //If it does, go to that item.
        mobileMenuRight($(this), true);
        event.preventDefault();
    }
    //If it does not, go to the link.
});

//When a back link is clicked, go back to previous menu item.
$("ul.sections").on("click touchend", "li.backLink a", function(event){
    mobileMenuLeft($(this));
    event.preventDefault();
});

//------------------------Top Nav in Desktop View---------------------------------------//

/*Set height of all the items to zero to start. Visibility is set to visible here to prevent the page
from showing the level2 items for a moment before the hight is set in script.*/
$allLevel2.css({"height":"0px", "visibility": "visible"});

/*
Function to expand the top nav menus on desktop.
$oneLink: jquery object of the link that was clicked or hovered.*/
function expandTopMenu($oneLink){
    //If the contentName is a corosponding div:
    if ( $oneLink.next().hasClass("level2") ) {

        var $l2Menu = $oneLink.next();
        $l2Menu.css("display","block");

        //Reset the z-index of all menu items.
        $allLevel2.css("z-index","");

        //Add one to the z-index of the current menu item.
        var currentz = $l2Menu.css("z-index");
        $l2Menu.css({"z-index":Number(currentz) + 1});

        //Get the current height of the bottom border, then convert it to a number.
        var borderHeight = $l2Menu.css("border-bottom-width");
        borderHeight = borderHeight.substring(0,borderHeight.length-2);
        borderHeight = parseInt(borderHeight);

        var curHeight = $l2Menu.height();
        var autoHeight = $l2Menu.css("height","auto").height();

        //Add the borderHeight if any to the autoHeight.
        autoHeight = autoHeight + borderHeight;

        $l2Menu.height(curHeight).animate({
            height: autoHeight
        },{
            queue: false,
            complete: function(){
                $l2Menu.css({"height":""});
            }
        });
    }
}

//Function to collapse some or all of the top nav menus.
function collapseTopMenu ($itemsToCollapse){
    //Reset the z-index of all menu items.
    $allLevel2.css("z-index","");

    $itemsToCollapse.animate({
        height: "0px"
    },{
        queue: false,
        complete: function(){
            $itemsToCollapse.css("display","");
        }
    });
}

//When the section links are hovered, desktop only.
$("nav#top .bottomHalf ul.sections > li > a").mouseenter(function(){
    if (isDesktopView()){
        if ($(this).hasClass("selected") === false ){
            //Expand the current itme.
            $(this).addClass("selected");
            expandTopMenu($(this));
        }
    }
});

//When the mouse leaves the menus or sections.
$("nav#top .bottomHalf ul.sections > li").mouseleave(function(){
    //Any time the mouse leaves the sections lis, colapse the menus.    
    if ( isDesktopView() ){
        if ( $(this).find("ul.level2").hasClass("locked") === false ){
            $(this).find(">a").removeClass("selected");
            collapseTopMenu($(this).find("ul.level2"));
        }
    }
});

//When a desktop menu level1 item is clicked.
$("nav#top .bottomHalf ul.sections > li > a").on("click touchstart", function(event){
    //This function only applies for desktop view and if the link clicked has sub items in its <li>.
    if (isDesktopView() && $(this).next().hasClass("level2") ){
        //If this item is already locked:
        if ($(this).next().hasClass("locked")){
            //Reset the menus and sections links for all selected or locked items.
            $allLevel2.removeClass("locked");
            $menuSections.find(">li >a").removeClass("selected");
            //To match the old menu, if you click an expanded item, it will close, even though it is 
            //technically still in the hover state. To open it you would click again or move off it and back on to
            //it.
            collapseTopMenu($allLevel2);
        }
        //if the item is not selected:
        else{
            //Collapse the old locked item.
            collapseTopMenu($allLevel2.filter(".locked"));
            //Reset the menus and sections links for all other selected or locked items.
            $allLevel2.removeClass("locked");
            $menuSections.find(">li >a").removeClass("selected");
            //Add the selected and locked classes to the current items.
            $(this).addClass("selected");
            $(this).next().addClass("locked");
            //Expand the current item.
            expandTopMenu($(this));

        }
        event.preventDefault();
    }
});


//Boolean variable to keep track of the state of the locked desktop search.
var lockedSearchActive = false;

function closeLockedSearch(){
    lockedSearchActive = false;
    $lockedSearchButtonLink.find(".fa-times-circle").animate({
        opacity: "0"
    },{
        queue: false,
        complete: function(){
            $lockedSearchButtonLink.find(".fa-times-circle").hide();
            $lockedSearchButtonLink.find(".fa-search").show();
        }
    });

    $lockedSearchButtonLink.find(".fa-search").animate({
        opacity: "1"
    },{
        queue: false
    });

    $desktopSearchContainer.find("form").css("display","");
    $desktopSearchContainer.animate({
        width: "225px"
    },{
        queue: false,
        complete: function(){
            $desktopSearchContainer.css({"z-index":"","opacity":""});
        }
    });
    $menuSections.show();
    $menuSections.animate({
        opacity: "1"
    },{
        queue: false
    });    
}

function showLockedSearch(){
    lockedSearchActive = true;
    $lockedSearchButtonLink.find(".fa-search").animate({
        opacity: "0"
    },{
        queue: false,
        complete: function(){
            $lockedSearchButtonLink.find(".fa-search").hide();
            $lockedSearchButtonLink.find(".fa-times-circle").show();
        }
    });

    $lockedSearchButtonLink.find(".fa-times-circle").animate({
        opacity: "1"
    },{
        queue: false
    });

    $desktopSearchContainer.find("form").css("display","block");
    $desktopSearchContainer.animate({
        width: "450px",
        opacity: "1"
    },{
        queue: false,
        complete: function(){
           $desktopSearchContainer.css({"z-index":"1400"});
        }
    });

    $menuSections.animate({
        opacity: "0"
    },{
        queue: false
    });
    $menuSections.hide();
}

//Show or hide the locked search area.

$lockedSearchButtonLink.click(function(event) {
    if (isDesktopView()){
        if (lockedSearchActive)
        {
            closeLockedSearch();
        }
        else{   
            showLockedSearch();
        }
        event.preventDefault();
    }
});

function lockTopNav(){
    $navWrap.addClass("scrollLocked");
    $navWrap.animate({
        backgroundColor: "#333333"
    },{
        queue: false,
        duration: 300,
        complete: function(){
            //Fallback if GSAP is not running.
            $navWrap.css("background-color","#333333");
        }
    });

    $menuSections.animate({
        marginLeft: "26%"
    },{
        queue: false,
        duration: 300
    });
}

function unlockTopNav(){
    $navWrap.removeClass("scrollLocked");
    
    closeLockedSearch();

    $navWrap.animate({
        backgroundColor: "#ffffff"
    },{
        queue: false,
        duration: 300,
        complete: function(){
            //Fallback if GSAP is not running.
            $navWrap.css("background-color","#ffffff");
        }
    });

    $menuSections.animate({
        marginLeft: "0px"
    },{
        queue: false,
        duration: 300
    });

}

var subClassNames;
function lockSubNav(){

    subClassNames = $subNav.attr("class");
    
    //Adjustments for special classes.
    if ( subClassNames && subClassNames.indexOf("institute") >= 0){
        $subNav.addClass("institute-lock");
    }

    var $parentMain = $subNav.closest(".mainbody");
    var parentHeight = $parentMain.outerHeight();
    var parentZ = $parentMain.css("z-index");
    //Make sure that the mainbody containing the sub nav has a higher z-index than the other mainbodys.
    //$subNav.closest(".mainbody").css({"z-index":Number(parentZ)+1,"height":parentHeight});
    $subNav.closest(".mainbody").css({"z-index":Number(parentZ)+1});
    $subNav.css("transition","none");
    $subNav.removeClass(subClassNames);
    $subNav.addClass("subLocked");
}

function unlockSubNav(){
    //Reset anything from special classes.
    $subNav.removeClass("institute-lock");

    $subNav.removeClass("subLocked");
    $subNav.addClass(subClassNames);
    //$subNav.closest(".mainbody").css({"z-index":"","height":""});
    $subNav.closest(".mainbody").css({"z-index":""});
}

function checkPosition(){
    if (isDesktopView()){
        if ($(window).scrollTop() > 50){
            if ($navWrap.hasClass("scrollLocked") === false){
                lockTopNav();
            }
        }
        //Need to call the value each time here, because of possible page resizes.
        if ($(window).scrollTop() > ($subNav.offset().top - (subNavHeight - 36) ) ){
            if ($subNav.hasClass("subLocked") === false){
                /*Set this each time the sub nav is locked, so the lock point is correct even when the
                page is resized.*/
                subNavOffsetTop = $subNav.offset().top;
                lockSubNav();
            }
        }
        if ($(window).scrollTop() <= (subNavOffsetTop - subNavHeight) ){
            if ($subNav.hasClass("subLocked")){
                unlockSubNav();
            }
        }
        if ($(window).scrollTop() <= 50){
            if ($navWrap.hasClass("scrollLocked")){
                unlockTopNav();
            }
        }
    }
}

var $pull = $('#pull');
var $toggleNav = $(".toggle-nav");
var $subUL = $('nav#sub ul');

function expandSubNav(){
    $subUL.slideDown();
    $toggleNav.addClass("open");
}

function colapseSubNav(){
    $subUL.slideUp();
    $toggleNav.removeClass("open");
}

$($pull).on("click", function(event){
    if ( $toggleNav.hasClass("open") ){
        colapseSubNav();
    }
    else{
        expandSubNav();
    }
    event.preventDefault();
});

//Function to switch between mobile and desktop navs.
function switchNavView(){
    //Check for IE8 or lower.
    if ( (($(".lt-ie9").length > 0 || $(".browser-ie-8").length > 0 ) === false) && ($navWrap.length > 0)) {
        //If it's not, determine which view to go to.
        if ( $(window).width() >= switchPoint ){
            //Go to DESKTOP
            $navWrap.addClass("desktop-nav");
            $navWrap.removeClass("mobile-nav");
            //Close the mobile search area if open when going to desktop view.
            closeMobileSearch($mobileSearchLink);
            //Close the mobile menu.
            closeMobileMenu($mobileMenuLink);
            $mainbodyAndFooter.removeClass("mobileView");
            checkPosition();
        }
        else{
            //Go to MOBILE
            $navWrap.addClass("mobile-nav");
            $navWrap.removeClass("desktop-nav");
            $allLevel2.removeClass("locked");
            $menuSections.find(">li >a").removeClass("selected");
            $mainbodyAndFooter.addClass("mobileView");
            collapseTopMenu($allLevel2);
            unlockTopNav();
            unlockSubNav();
            checkPosition();
        }
    }
    //If this is IE8 or lower, go to desktop.
    else{
        //Go to DESKTOP
        $navWrap.addClass("desktop-nav");
        $navWrap.removeClass("mobile-nav");
    }
}

//Check for IE8 or lower. If it is, don't need an event for responsiveness.
if ( ($(".lt-ie9").length > 0 || $(".browser-ie-8").length > 0 ) === false) {

    //Bind the resize function. This also crates a 'resizeEnd' event which is used later. 
    $(window).resize(function() {
        if(this.resizeTO) {
            clearTimeout(this.resizeTO);
        }
        this.resizeTO = setTimeout(function() {
            $(this).trigger('resizeEnd');
        }, 100); //The value here is the number of miliseconds to wait before the resize is considred to be 'ended'.
    });

}


//Adjust the page when it first loads if necessary. 
switchNavView();

//Whenever a resize completes:
$(window).bind('resizeEnd', function() {
    //Check if the nav view needs switching.
    switchNavView();
});

var didScroll;
var lastScrollTop = 0;
var delta = 5;

$(window).scroll(function(){
    checkPosition();
    didScroll = true;
});

function hasScrolled(){
    if (isDesktopView()===false){

        var st = $(this).scrollTop();

        // Make sure they scroll more than delta
        if(Math.abs(lastScrollTop - st) <= delta){
            return;
        }
        
        // If the user scrolled down and is past the navbar, add class .nav-up.
        // Don't hide the top if the mobile nav or mobile search is open.
        if (st > lastScrollTop && st > mobileHeaderHeight && 
            ($mobileMenuLink.hasClass("mobile-menu-active")===false) && 
            ($mobileSearchLink.hasClass("mobile-search-active")===false ) ){
            // Scroll Down
            $navWrap.removeClass('nav-down').addClass('nav-up');
        } else {
            // Scroll Up
            if(st + $(window).height() < $(document).height()) {
                $navWrap.removeClass('nav-up').addClass('nav-down');
            }
        }

        lastScrollTop = st;
    }
}

// run hasScrolled() and reset didScroll status
setInterval(function() {
  if (didScroll) {
    hasScrolled();
    didScroll = false;
  }
}, 180);

///----------------------End of top nav scripts------------------------///

var $resizedElements = $(".blog .full-copyarea").children().not(".authorInfo");
var $controlContainer = $(".text-size-box");

function removeFontSizeClasses( $elements ){
    $elements.removeClass("larger-text large-text");
    $controlContainer.find("a").removeClass("selectedBorder");
}

$controlContainer.find("a.small-text").addClass("selectedBorder");

$controlContainer.find($("a.small-text")).click( function() {
    removeFontSizeClasses($resizedElements);
    $(this).addClass("selectedBorder");
    return false;
});

$controlContainer.find($("a.large-text")).click( function() {
    removeFontSizeClasses($resizedElements);
    $resizedElements.addClass("large-text");
    $(this).addClass("selectedBorder");
    return false;
});

$controlContainer.find($(".text-size-box a.larger-text")).click( function() {
    removeFontSizeClasses($resizedElements);
    $resizedElements.addClass("larger-text");
    $(this).addClass("selectedBorder");
    return false;
});

$('.lt-ie9 *[class*="col2"], .browser-ie-8 *[class*="col2"]').find('.item:nth-of-type(2n), article:nth-of-type(2n)').addClass('last');
$('.lt-ie9 *[class*="col3"], .browser-ie-8 *[class*="col3"]').find('.item:nth-of-type(3n), article:nth-of-type(3n)').addClass('last');
$('.lt-ie9 *[class*="col4"], .browser-ie-8 *[class*="col4"]').find('.item:nth-of-type(4n), article:nth-of-type(4n)').addClass('last');
$('.lt-ie9 *[class*="col5"], .browser-ie-8 [class*="col5"]').find('.item:nth-of-type(5n), article:nth-of-type(5n)').addClass('last');
$('.lt-ie9 *[class*="related"], .browser-ie-8 *[class*="related"]').find('li:last-child').addClass('last');
$('.lt-ie9 *[class*="news"], .browser-ie-8 *[class*="news"]').find('li:last-child').addClass('last');
$('.lt-ie9 aside, .browser-ie-8 aside').find('li:last-child').addClass('last');

//Swaps out license key for flowplayer based on URL.
function millenniumFlowplayerKeySwap(externalKey){
    var wholeURL = window.location.href;
    if(wholeURL.indexOf('http://www.jpmorganchase.com/content/dam/jpmorganchase/en/legacy/corporate/includes/javascript/millenniumweb.com') > -1){
        $('.flowplayer').each(function(){
            $(this).attr('data-key','$526627217422934');
        });
    } else {
        $('.flowplayer').each(function(){
            $(this).attr('data-key', externalKey);
        });
    }
}
millenniumFlowplayerKeySwap('$520063017205765');


$(function() {
    // Set the Active Section in the Top Nav.
    // var keyValuePair = location.pathname.split('corporate/')[1].split('/');
    // $("*[href*=" + keyValuePair[0] + "]:first").parents(".jpui-menu-item").find(".jpui-menu-link").addClass("active");
    var theUrl = location.pathname;
    if((theUrl.indexOf("jpmorganchase.com/#") >= 0) || (theUrl.indexOf("Home") >= 0) || (theUrl === "/")) {
        $("#jpmc_home").addClass("active");
    } else if(theUrl.indexOf("/About-JPMC/") >= 0) {
        $("#jpmc_about").addClass("active");
    } else if(theUrl.indexOf("/Corporate-Responsibility/") >= 0) {
        $("#jpmc_cr").addClass("active");
    } else if(theUrl.indexOf("http://www.jpmorganchase.com/content/dam/jpmorganchase/en/legacy/corporate/includes/javascript/invester.shareholder.com") >= 0) {
        $("#jpmc_investor").addClass("active");
    } else if(theUrl.indexOf("/Careers/") >= 0) {
        $("#jpmc_careers").addClass("active");
    } else if(theUrl.indexOf("/institute/") >= 0) {
        $("#jpmc_institute").addClass("active");
    } 
    /*This checks to see if the contact-us section should activate, but will not activate on the 
    "contact-us" page of the Institute section.
    */
    else if( (theUrl.indexOf("Home/contact-us") >= 0) && (theUrl.indexOf("/institute")<0 ) ){
        $("#jpmc_contact").addClass("active");
    }

    // Initialize matchHeight ---------------

    //Set matchHeight on all .item class, except the items in the top nav.
    $('.item').not("nav#top ul.sections .level2 > *").matchHeight();
    $('.match-height-apply').matchHeight({byRow:false}); //DO NOT PUT THIS CLASS IN CSS!!!!!!!!!!

    $('.share-title a').on('click', function(e) {
        $(this).closest('.sharing').toggleClass('active');
        e.preventDefault();
    });

    // Initialize videos --------------
    var currPlayer;
    $('*[data-mid="video-modal"]').each(function(){
        $(this).click(function(){
            currPlayer = $(this).attr('data-vid-name');
            return false;
        });
    });

    $("#modal-content .transcript").click( function(){
        $("#modal-content .flowplayer").data("flowplayer").pause();
    });
    
    /*This function sets the sharing control styles based on window scrolltop.
    $shareControl: jQuery object handle on the sharing control.
    switchPoint: number at which point to switch the share control between static and fixed.
    */
    function sharingScrollCheck($shareControl, switchPoint){
        if ($(window).scrollTop() > switchPoint){
            $shareControl.removeClass("scrollWith");
        }
        else if ($(window).scrollTop() < switchPoint){
            $shareControl.addClass("scrollWith");
        }
    }

    /*This code block is used to determine what position the share control will lock in at $xxlarge and
     wider breakpoints. It looks at the body tag around the share control, and gets classes from there. 
     All classes to control this must have the prefix "share-". This code assumes there is only one
      "share-..." class on the body. If there is more than one, it will take the last classes with 
      the "share-" prefix.
    */
    var $shareControl = $(".sharing");
    
    //The default value, used on most pages.
    var shareTopValue = 517;
    
    //If there is a share control.
    if ($shareControl.length > 0){
        //Get the names of the classes on the body.
        var classNames = $shareControl.closest("body").attr("class");
        //If there was at least once class on the body:
        if (classNames){
            //Make an array of the classes.
            var classArray = classNames.split(" ");
            
            //Used to keep track of the class with the correct prefix.
            var shareClass = "";
            
            //Loop on the array of class names.
            $.each(classArray, function(index, value){
                //If any class name has the correct prefix, save it in shareClass.
                if (value.indexOf("share-") > -1){
                    shareClass = value;
                }
            });

            //Check the shareClass against all values with different presets.
            if (shareClass === "share-cr-microsite"){
                $shareControl.addClass("cr-microsite");
                shareTopValue = $shareControl.offset().top;
            }
            else if (shareClass === "share-annual-report"){
                $shareControl.addClass("annual-report");
                shareTopValue = $shareControl.offset().top;
            }
            //More share classes go here.
        }

        //switchPoint is the point at which the sharing goes from static to fixed.
        var switchPoint= shareTopValue - 133;

        //Run the function to set the share position on load.
        sharingScrollCheck($shareControl, switchPoint);
        //Make the share visible. By default is is visibility :hidden to avoid looking like it is moving.
        $shareControl.css("visibility","visible");

        //Keep the sharing element locked to the top of the page when scrolling down.
        $(window).scroll(function(){
            //Each time the window is scrolled, run the function.
            sharingScrollCheck($shareControl, switchPoint);
        });
    }

    if ( $("#image-modal").length > 0 ){
        $("#image-modal").heliumModal({
            vert: 50,
            speed: 500,
            easing: 'swing',
            onOpen: function(){  },  // callback for custom JS when modal opens
            afterOpen: function(){ $(".close").focus(); },
            onClose: function(){ $("figcaption .imageModalLink").focus(); }  // callback for custom JS when modal close
        });
    }

    if ( $("#image-modal-1").length > 0 ){
        $("#image-modal-1").heliumModal({
            vert: 50,
            speed: 500,
            easing: 'swing',
            onOpen: function(){  },  // callback for custom JS when modal opens
            afterOpen: function(){ $(".close").focus(); },
            onClose: function(){ $("figcaption .imageModalLink").focus(); }  // callback for custom JS when modal close
        });
    }

    if ( $("#image-modal-2").length > 0 ){
        $("#image-modal-2").heliumModal({
            vert: 50,
            speed: 500,
            easing: 'swing',
            onOpen: function(){  },  // callback for custom JS when modal opens
            afterOpen: function(){ $(".close").focus(); },
            onClose: function(){ $("figcaption .imageModalLink").focus(); }  // callback for custom JS when modal close
        });
    }

    if ( $("#text-modal").length > 0 ){
        $("#text-modal").heliumModal({
            vert: 50,
            speed: 500,
            easing: 'swing',
            onOpen: function(){  },  // callback for custom JS when modal opens
            afterOpen: function(){ $(".close").focus(); },
            onClose: function(){ $("figcaption .textModalLink").focus(); }  // callback for custom JS when modal close
        });
    }

    if ( $("#text-modal-1").length > 0 ){
        $("#text-modal-1").heliumModal({
            vert: 50,
            speed: 500,
            easing: 'swing',
            onOpen: function(){  },  // callback for custom JS when modal opens
            afterOpen: function(){ $(".close").focus(); },
            onClose: function(){ $("figcaption .textModalLink").focus(); }  // callback for custom JS when modal close
        });
    }

    if ( $("#text-modal-2").length > 0 ){
        $("#text-modal-2").heliumModal({
            vert: 50,
            speed: 500,
            easing: 'swing',
            onOpen: function(){  },  // callback for custom JS when modal opens
            afterOpen: function(){ $(".close").focus(); },
            onClose: function(){ $("figcaption .textModalLink").focus(); }  // callback for custom JS when modal close
        });
    }

    /*Function for creating sharing links on the pages. This is coded for Facebook, twitter, linkedIn and GooglePlus.
    This is done by setting the value for the links on the page on load. In general, values specified in the HTML overide the passed parameters.
        The parameters are:
    $target: The jQuery element to set links for.
    urlVar: [Optional] A string paramter for a variable to add to the URL. Can be used for sharing a specific video or subsection.
        Overwritten in the HTML by 'data-url' in the '.sharing' div. If not value is provided, the URL in the addressbar is used.
    title: [Optional] A string to use as a title in the shared text. Currently only used for Twitter.
        Overwritten in the HTML by a 'data-text' value in the HTML of the Twitter link. This can include a hashtag (#) or a via @ User
        for Twitter.
    */
    function createShareLinks($target, urlVar, title, via, summary){

        //linkURL is the url of the link to go to.
        var linkURL;
        //If there is a value in the HTML, use that.
        if ( $target.attr("data-url") ){
            linkURL = $target.attr("data-url");
        }
        else{
            //If there is not a value provided, use the page location. Remove any variables from the URL. If a variable is needed, use data-url or urlVar paramter.
            var windowLocaitonHref = window.location.href;
            if (windowLocaitonHref.indexOf("?mod=") > 0){
                linkURL = windowLocaitonHref.substring(0, windowLocaitonHref.indexOf("?mod="));
            }
            else if(windowLocaitonHref.indexOf("?ilv=") > 0){
                linkURL = windowLocaitonHref.substring(0, windowLocaitonHref.indexOf("?ilv="));
            }
            else{
                linkURL = window.location.href;
            }
        }

        //If a 'data-video-id' is provided, add it to the URL. This is for inline videos, to know which video to go to.
        if ($target.attr("data-video-id")){
            linkURL = linkURL + "?ilv=" + $target.attr("data-video-id");
        }
        //Add the urlVar if provided.
        else if (urlVar){
            linkURL = linkURL + urlVar;
        }

        //Encode the URL.
        var encodedURL = encodeURI(linkURL);

        //Get a handle on the twitter link. Get the descriptText from the 'data-text' value or title paramter.
        var $twitter = $($target.find(".twitter"));
        var descriptText="";
        if ($twitter.attr("data-text")){
            descriptText = $twitter.attr("data-text");
        }
        else if(summary){
            descriptText = summary;
        }

        //if summary is not set use og:title as summary for mail link
        if(!title){
            title = encodeURIComponent($("meta[property='og:title']").attr('content'));
        }

        /*Necessary string replacements for the Twitter description. This replaces certain special
        characters that cause errors in the Twitter description.*/
        descriptText = descriptText.replace(/#/g, "%23");
        descriptText = descriptText.replace(/ /g, "+");
        descriptText = descriptText.replace(/(\|| ,)/g, "%7C");
        descriptText = descriptText.replace(/&/g, "%26");

        /*Set the "via" property. If no "data-via" is set in the HTML it checks for a passed
        value. If that is not there it will not be included.
        The words "via" and the @ symbol are added automatically by Twitter.
        */
        var twitterVia="";
        if ($twitter.attr("data-via")){
            twitterVia = $twitter.attr("data-via");
            twitterVia = "&via=" + twitterVia;
        }
        else if (via){
            twitterVia = "&via=" + via;
        }

        var $linkedIn = $target.find(".linkedIn");
        /*Get the linkedIn Summary from the HTML*/
        var linkedInSummary = "";
        if ($linkedIn.attr("data-text")){
            linkedInSummary = $linkedIn.attr("data-text");
        }
        else if(summary){
            linkedInSummary = summary;
        }

        /*Replacements for some special characters in the linkedIn text. Special characters
        caused some text to be misinterpreted.*/
        linkedInSummary = linkedInSummary.replace(/#/g, "%23");
        linkedInSummary = linkedInSummary.replace(/&/g, "%26");

        //Get the OG description tag text to use in the mail share.
        var mailText;
        if ($("head meta[property='og:description']").attr("content")){
            mailText = $("head meta[property='og:description']").attr("content");
        }
        //If the OG desription tag is not available, use the linked in text.
        else if (linkedInSummary) {
            mailText = linkedInSummary;
        }
        //If the neither the LinkedIn summary nor the OG description is present, use the summary.
        else if (summary){
            mailText = summary;
        }
        //If none of the above are available, don't use anything. This is needed so the link still works.
        else{
            mailText = "";
        }

        //The text must be properly encoded.
        mailText = mailText.replace(/#/g, "%23");
        //mailText = mailText.replace(/ /g, "+");
        mailText = mailText.replace(/(\|| ,)/g, "%7C");
        mailText = mailText.replace(/&/g, "%26");

        //Get a handle on the links.
        var $facebook = $target.find(".facebook");
        var $googlePlus = $target.find(".googlePlus");
        var $mail = $target.find(".mail");

        //Set the links for each button.
        $facebook.attr("href", "https://www.facebook.com/sharer/sharer.php?u=" + encodedURL);

        //The Twitter Via will only display if one was provided, otherwise it is an empty string.
        $twitter.attr("href", "https://twitter.com/intent/tweet?text=" + descriptText + "+" + encodedURL + twitterVia);

        $linkedIn.attr("href", "http://www.linkedin.com/shareArticle?mini=true&url=" + encodedURL + "&summary=" + linkedInSummary);

        $googlePlus.attr("href", "https://plus.google.com/share?url=" + encodedURL);

        //Make the email link with subject and body. The %0A is an encoded line break.
        $mail.attr('href','mailto:?subject='+title+'&body='+ mailText + "%0A" + encodedURL);
    }

    //Set up share links for all contaiers of class 'sharing'.
    createShareLinks($(".sharing"));
    //If there is an 'inline-share' container, set up links for it. Checking with an if here, since most pages will not have it.
    if ($(".inline-share").length > 0){
        createShareLinks($(".inline-share"));
    }

    //On some pages the twitter link opened up a new window in addition to the modal warning message. This prevents that from happening.
    // Commented out to prevent the Twitter Share link from failing
    //$(".twitter").click(function(event){
        //event.stopPropagation();
    //});

    //Fix for the Careers footer page. Multiple links are in one box which needs to be hovered. This 
    //is a fix for mobile.
    $(".student-article .img-wrap, .student-article h3").one("click", function(event){
        event.preventDefault();
        $(".student-article .student").css("top", "-34px");
    });

    //Code for accordion on careers-faq page. this option has the answers hidden
    var allAnswersAbout = $(".accordion > .answer-about").hide();
    var allQuestionsAbout = $(".accordion > .question-about");
    $('.accordion > .question-about').click(function() {
        if ( $(this).next().css("display") === "none" ){
            allQuestionsAbout.removeClass("open");
            allQuestionsAbout.find("img").css("top","0px");
            allAnswersAbout.slideUp();
            $(this).next().slideDown();
            $(this).find("img").css("top","-13px");
            $(this).addClass("open");
        }
        return false;
    });

        //Code for accordion on careers-faq page.
    var allAnswers = $(".accordion > .answer");
    var allQuestions = $(".accordion > .question");
    $('.accordion > .question').click(function() {
        if ( $(this).next().css("display") === "none" ){
            allQuestions.removeClass("open");
            allQuestions.find("img").css("top","0px");
            allAnswers.slideUp();
            $(this).next().slideDown();
            $(this).find("img").css("top","-13px");
            $(this).addClass("open");
        }
        return false;
    });


    //Code for accordion on careers experiened professionals page.
    var allBenefitsList = $(".expandableBenefits ul").hide();
    var allBenefitsAnchor = $(".expandableBenefits a");
    $(".expandableBenefits a").click(function(){
        if ( $(this).next().css("display") === "none") {
            //allBenefitsList.removeClass("openUL").css("outline","1px dotted white");
            allBenefitsList.prev().find("img").css("top", "0px");
            allBenefitsList.slideUp();
            $(this).next().slideDown();
            $(this).find("img").css("top", "-13px");
            allBenefitsAnchor.removeClass("openItem");
            $(this).addClass("openItem");
        }
        return false;
    });

    //fix for focusing a video sidebar or similar animated element
    $('.video-sidebar a, .video-slide-mod a, .overlay-box a').not('h3 a, h2 a').focusin(function(){
        var element = $(this).parents('.video-sidebar, .video-slide-mod, .overlay-box');
        element.addClass('hover');
        setTimeout(function(){
            element.scrollTop(0);
        },0);
        element.focusout(function(){
            $(this).removeClass('hover');
            $(this).scrollTop(0);
        });
    });

    // No longer being used.
    // function setUpBreadCrumb(){
    //     //breadcrumb bar set up.
    //     var $bcb = $(".breadCrumbHeader ul li");
    //     var bcLength = $bcb.length;
    //     var i; //Counter.
    //     for (i=0; i<bcLength; i++){

    //         //Add the approprate class.
    //         if (i===0){
    //             $bcb.eq(0).addClass("zero");
    //         }
    //         else if (i===1){
    //             $bcb.eq(1).addClass("one");
    //         }
    //         else if (i===2){
    //             $bcb.eq(2).addClass("two");
    //         }
    //         else if (i===3){
    //             $bcb.eq(3).addClass("three");
    //         }
    //     }

    //     //Add the 'after arrow' span to each LI. This is what contains the Arrow on $small and larger sizes.
    //     $bcb.append("<span class='afterArrow'></span>");

    //     //Add an additional class for the last item.
    //     $bcb.eq(bcLength-1).addClass("lastCrumb");

    //     //Set the href value for the left arrow button on mobile. This gets the value of the link in the last crumb.
    //     $(".mobileBackArrow").parent().attr("href", $(".lastCrumb a").attr("href"));
        
    // }

    // setUpBreadCrumb();

    //For each element in the modal, preform the nessisary javascript modifications.
    function modalFlowplayer(){

        //Get handle on flowplayer API.
        var api = flowplayer($(".modal .flowplayer"));
        var $modalPlayer = $(".modal .flowplayer");

        createKeyInfo( $modalPlayer.find(".fp-mute"),"Mute or Unmute", 3);
        createKeyInfo( $modalPlayer.find(".fp-fullscreen"),"Fullscreen on or off",5);

        //Play Button. Changes accessible text depending on state of the player.
        createKeyInfo( $modalPlayer.find(".fp-play"), "Pause video", 1);
        api.bind("pause", function(){
            $modalPlayer.find(".fp-play .accessible-text").html("Play video");
        });
        api.bind("resume", function(){
            $modalPlayer.find(".fp-play .accessible-text").html("Pause video");
        });

        //Create accessible text on the time counters in the player.
        createKeyInfo( $modalPlayer.find(".fp-elapsed"),"Time Elasped:");
        createKeyInfo( $modalPlayer.find(".fp-remaining"), "Time Remaining:");
        createKeyInfo( $modalPlayer.find(".fp-duration"),"Video Duration:");

        //Video scruber
        var videoScrub = [
            {
                //Right arrow.
                button: 39,
                action: function() {
                    api.seek(api.video.time + 10);
                }
            },
            {
                //Left arrow.
                button: 37, action: function() {
                    if (api.video.time <= 10 && !api.playing)
                    {
                        api.stop();
                        api.seek(0);
                        api.video.time=0;
                    }
                    else
                    {
                       api.seek(api.video.time - 10);
                    }
                }
            }
        ];

        createKeyInfo( $modalPlayer.find(".fp-timeline"), "Left arrow rewinds, right arrow advances",2);
        createKeyEvents( $modalPlayer.find(".fp-timeline"),videoScrub);

        //Volume control
        var volumeKeys = [
            {
                //Up arrow. Increase volume.
                button: 38, action: function() {
                    api.volume(Number(api.volumeLevel) + 0.1);
                }
            },
            {
                /*Down arrow. Decrease volue. There was an issue where if the volume was set to zero by
                subtraction the api would not be able to add to it later on because it was getting back NaN
                for the value. This checks if the volume is .1, and then sets the volume to 0.0.
                */
                button: 40, action: function() {
                if (api.volumeLevel === 0.1){
                    api.volume(0.0);
                }
                //In all other cases, reduce the volume by .1.
                else{
                    api.volume(Number(api.volumeLevel) - 0.1);
                }
            }}
         ];
        createKeyInfo( $modalPlayer.find(".fp-volumeslider"),"Use up and down arrows for volume", 4);
        createKeyEvents( $modalPlayer.find(".fp-volumeslider"),volumeKeys);

        //Set the tabindexes for the video share buttons in the modal.
        $(".modal .facebook").attr("tabIndex", "8");
        $(".modal .twitter").attr("tabIndex", "9");
        $(".modal .linkedIn").attr("tabIndex", "10");
        $(".modal .googlePlus").attr("tabIndex", "11");
        $(".modal .mail").attr("tabIndex", "12");

        //Run the flowplayer Analytics when the player resumes, but only run it once.
        var logFlag = true;
        api.bind("resume", function(){
            if (logFlag)
            {
                logFlag = false;
                flowplayerAnalytics(api);
            }
        });
    }

    /*Function for adding keyboard controls to an element. The parameters are:
    $control: jquery object to add controls to.
    keyEvents: Object containing the key to listen for, and the function to do when it is pressed.
    The format for keyEvents should be
     {button: key number, action: function to execute}
     there can be multiple button and action pairs.
    */
    function createKeyEvents($control, keyEvents){
        //Check if they keyEvents are the right type.
        if (typeof(keyEvents)==="object")
        {
            //When the $control is focused.
            $control.focusin( function(){
                $control.keydown( function(event){
                    var i = 0;
                    for(i=0; i < keyEvents.length; i++){
                        if (event.which === keyEvents[i].button){
                            keyEvents[i].action();
                            return false;
                        }
                    }
                });
            });
        }

        //When the $control looses focus, unbind the keydown event.
        $control.focusout(function() {
            $control.off("keydown");
        });
    }

    /* Function for creating accessible text on a control.
    $control: jquery object to add controls to.
    accessibleText: String to set the accessible text to.
    tabKeyIndex: [Optional] Int to set the tab index to. If not included, no tabindex or href will be set.
    WARNING: This function will overwrite the href value if a tabindex is provided. This maybe be changed in the future.
    */
    function createKeyInfo($control, accessibleText, tabKeyIndex)
    {
        //Make sure that the tabKeyIndex is valid. Also ensures that if a herf is set a tabIndex is also set.
        if ((isNaN(tabKeyIndex) || tabKeyIndex === undefined || tabKeyIndex === null)===false)
        {
            //Set the tabIndex and an href value. An href value is nessissary for the tab key.
            $control.attr({tabindex: tabKeyIndex, href: '#'});
        }

        //Make sure the accessibleText is valid
        if (typeof(accessibleText)==="string"){
            //Check if the accessible text alredy exists.
            if ( $control.find(".accessible-text").length === 0){
                //If the control has text related to the time counters in flowplayer, create the text differently.
                if($control.hasClass('fp-remaining')||$control.hasClass('fp-elapsed')||$control.hasClass('fp-duration')){
                    //Check if the prevous node has the accessible text already.
                    if ($control.prev().hasClass('accessible-text') === false){
                        //Insert the accessible text before the $control.
                        $control.before('<span class="accessible-text">'+ accessibleText +'</span>');
                    }
                }
                else{
                    //Create the accessible text inside the $control element.
                    $control.prepend('<span class="accessible-text">'+ accessibleText +'</span>');
                }
            }
        }

        //Don't follow any links or scroll the page when the control is selected.
        $control.click(function(event){
            event.preventDefault();
        });
    }

    /*Function for setting custom focus actions for the tab key when an element is focused.
      $control: jQuery object to set a custom tab action for.
      $focusPrev: [Optional] The item to focus on when shift+tab is pressed. Pass null if no custom
                 action is needed.
      $focusNext: [Optional] The item to focus when tab is pressed. Pass null or leave out paramter if no
                custom action is needed.
    */
    function tabOverrides($control, $focusPrev, $focusNext)
    {
        //When the element is focused, bind a keydown event handler.
        $control.focusin( function(){
            $control.keydown(function(event){
                //If the keys are 9 (tab) and shift, focus the $focusPrev.
                if (event.which === 9 && event.shiftKey){
                    //If $focusPrev is null or undefined, do nothing.
                    if ($focusPrev){
                        $focusPrev.focus();
                        return false;
                    }
                }
                //If the key pressed is 9 (tab), focus $focusNext.
                else if (event.which === 9){
                    //If $focusNext is undefined or null, do nothing.
                    if ($focusNext){
                        $focusNext.focus();
                        return false;
                    }
                }
            });
        });
        //When the focus leaves the element, unbind all the keydown functions.
        $control.focusout(function() {
            $control.off("keydown");
        });
    }

    /*Function used determine if the video with close captions should be shown or if the clean video
    should be shown. This is based on what classes are on the captions div.
    cleanVids: Path to the video or videos without subtitles.
    subVids: Path to the video or videos with subtitles.
    Returns the correct video path or paths.
    */
    function getCorrectVideos(cleanVids, subVids)
    {
        if ($('#video-modal .captions').hasClass("off")){
            return cleanVids;
        }

        else if($('#video-modal .captions').hasClass("on")){
            return subVids;
        }

        /*Using else to make sure this function returns something
        in all instances.
        */
        else{
            return subVids;
        }
    }

    //Used to get a variable from the URL.
    function getUrlValue(varSearch){
        var searchString = window.location.search.substring(1);
        var variableArray = searchString.split('&');
        for(var i = 0; i < variableArray.length; i++){
            var keyValuePair = variableArray[i].split('=');
            if(keyValuePair[0] === varSearch){
                return keyValuePair[1];
            }
        }
    }

    function delaySetup() {
        window.setTimeout(inlineFlowplayer, 50);
    }

    function inlineFlowplayer() {
        var api = flowplayer($(".inline-video"));

        //The jQuery object that every other call will look inside of.
        var $inlineVideo = $(".inline-video");

        createKeyInfo($inlineVideo.find($(".fp-mute")),"Mute or Unmute", 0);

        //Move the fullscreen button to the end of the UI div, to correct the tab order.
        $inlineVideo.find($(".fp-fullscreen")).detach().appendTo($inlineVideo.find(".fp-ui"));
        createKeyInfo($inlineVideo.find($(".fp-fullscreen")),"Fullscreen on or off",0);

        //Play Button. Changes accessible text depending on state of the player.
        createKeyInfo($inlineVideo.find($(".fp-play")), "Pause video", 0);
        api.bind("pause", function(){
            $inlineVideo.find($(".fp-play .accessible-text")).html("Play video");
        });
        api.bind("resume", function(){
            $inlineVideo.find($(".fp-play .accessible-text")).html("Pause video");
        });

        /*This is a way to focus the play button on the inline video. Binding to play or load was not working. This
        binds the focus of the play button to the 'progress' event, which is when the video is progressing. It happens every
        few seconds. It will happen before the play button is ready. The check at the end stops the play button from being
        focused constantly, every time the player is progressing. Once the play button is focused, the flag is flipped, so
        the button is no longer focused. Unbinding the 'progress' will break other functions of the player.
        */
        var focusFlag = true;
        api.bind("progress", function(){
            if (focusFlag)
            {
                $inlineVideo.find($(".fp-play")).focus();
                if ($inlineVideo.find($(".fp-play")).is(":focus")){
                    focusFlag = false;
                }
            }
        });

        //When the turn captions off/on button is pressed, the play button must be focused as well.
        $inlineVideo.parent().find($(".captions")).click(function(){
            focusFlag = true;
        });

        //Create accessible text on the time counters in the player.
        createKeyInfo($inlineVideo.find($(".fp-elapsed")),"Time Elasped:");
        createKeyInfo($inlineVideo.find($(".fp-remaining")), "Time Remaining:");
        createKeyInfo($inlineVideo.find($(".fp-duration")),"Video Duration:");

        //Video scruber
        var videoScrub = [
            {
                //Right arrow.
                button: 39,
                action: function() {
                api.seek(api.video.time + 10);
                }},
            {
                //Left arrow.
                button: 37, action: function() {
                if (api.video.time <= 10 && !api.playing)
                {
                    //If the time is less than 10 seconds, go to the beginning.
                    api.stop();
                    api.seek(0);
                    api.video.time=0;
                }
                else
                {
                    api.seek(api.video.time - 10);
                }

            }}
        ];

        createKeyInfo($inlineVideo.find($(".fp-timeline")),"Left arrow rewinds, right arrow advances",0);
        createKeyEvents($inlineVideo.find($(".fp-timeline")),videoScrub);

        //Volume control
        var volumeKeys = [
            {
                //Up arrow. Increase volume.
                button: 38, action: function() {
                    api.volume(Number(api.volumeLevel) + 0.1);
                }
            },
            {
                /*Down arrow. Decrease volue. There was an issue where if the volume was set to zero by
                subtraction the api would not be able to add to it later on because it was getting back NaN
                for the value. This checks if the volume is .1, and then sets the volume to 0.0.
                */
                button: 40, action: function() {
                if (api.volumeLevel === 0.1){
                    api.volume(0.0);
                }
                //In all other cases, reduce the volume by .1.
                else{
                    api.volume(Number(api.volumeLevel) - 0.1);
                }
            }}
        ];
        createKeyInfo($inlineVideo.find($(".fp-volumeslider")),"Use up and down arrows for volume", 0);
        createKeyEvents($inlineVideo.find($(".fp-volumeslider")),volumeKeys);

        tabOverrides($inlineVideo.find(".fp-play"), $("#main-content-section") );

        //When the inline play button is clicked, play the video. Mainly used for keyboard controls.
        $(".inlineVidPlay").click( function(event){
            api.play();
            event.preventDefault();
            return false;
        });

        var startAnalytics = true;
        api.bind("resume", function(){
            if (navigator.userAgent.indexOf("iPad") > 0){
                $inlineVideo.css({"background-image": "none", "background-color":"white"});

            }
            //Run the flowplayer analytics, but only once.
            if (startAnalytics){
               flowplayerAnalytics(api);
               startAnalytics = false;
            }
        });
    }

    //If there is an inline-video, run the setup functions for the inline flowplayer.
    if ($(".inline-video").length > 0){
        delaySetup();
    }

    //Checks the URL on page load for the variable 'mod'. If it is found, set linkedVideo to true, and call the helium modal.
    var preload = getUrlValue("mod");
    var linkedVideo = false;
    if(preload){
        currPlayer = preload;
        linkedVideo = true;
        setTimeout(function() {
            $('#video-modal').heliumModal('openModal');
        }, 500);
    }

    //Checks the URL for the value 'ilv' and get its value.
    var linkToInline = getUrlValue("ilv");
    if (linkToInline){
        //Get the play button for the video and focus it.
        var $playBTN = $(".flowplayer[data-vid-name="+linkToInline+"]").next(".inlineVidPlay");
        $playBTN.focus();
        //Scroll the page to the player. Offset is aproxomate but works for existing inline video.
        $(window).scrollTop($playBTN.prev(".flowplayer").offset().top - 140);
    }

    /*Function for sending analytic data on a video. This will fire once for all videos. The analytics work
    by appending a gif to the page with parameters included in it.
    API: A handle on the current instance of the Flowplayer.
    */
    function flowplayerAnalytics(api){

        //Get the URL of the video.
        var videoURL = api.video.url;
        //Get the URL of the page.
        var pageLocation = window.location.href;
        //Get the title of the page, then trim off everything after  the | since it is always the same.
        var pageTitle = $("head title").html();
        pageTitle = pageTitle.substring(0, pageTitle.indexOf("|"));
        //Get the name of the video. This is a substring of the URL from the last / to the last .
        var vidCreativeName = videoURL.substring(videoURL.lastIndexOf("/")+1,videoURL.lastIndexOf("."));
        //The type of event that is being logged.
        var videoEvent;

        //The duration of the video.
        var duration = api.video.duration;
        //The log variables are used so that each position is logged only once.
        var logStart = true;
        var quarter = duration * 0.25;
        var log25 = true;
        var half = duration * 0.5;
        var log50 = true;
        var threeQuarter = duration * 0.75;
        var log75 = true;
        var playerFinished = false;
        //The current time, so each call will have a unique identifier.
        var curTime;
        //The image to append to the DOM to make the analytics call.
        var $logImg;
        var logSrc;

        //This happens every one second.
        var interval = setInterval( function() {

            //If the time is grather than zero and it has not been logged already...
            if (api.video.time > 0 && logStart === true){

                //The event is start.
                videoEvent = "Start";
                //Get the current time since Epoc in miliseconds.
                curTime = new Date().getTime();
                //Create a new IMG.
                $logImg = $("<img/>");
                //Set the source of the IMG with all the required variables.
                logSrc = "https://www.chase.com/online/Home/images/wa01.gif?log=1&wa_tp=21&wa_cb=" + curTime +
                    "&wa_uri=" + pageLocation +
                    "&wa_pt=" + pageTitle + "&wa_vi_n="+ vidCreativeName + "&wa_vi_s=" + videoEvent + "&wa_lob=JPMC";
                //Make sure the image is not visible.
                $logImg.attr("style","display:none;");
                //Append the image to the body.
                $logImg.attr("src", logSrc).appendTo("body");

                //console.log("Video Started: " + vidCreativeName);
                //Set this to false so the log does not repeat every second after this completes.
                logStart = false;
            }

            if (api.video.time > quarter && log25 === true){

                videoEvent = "25Percent";
                curTime = new Date().getTime();
                $logImg = $("<img/>");
                logSrc = "https://www.chase.com/online/Home/images/wa01.gif?log=1&wa_tp=21&wa_cb=" + curTime +
                    "&wa_uri=" + pageLocation +
                    "&wa_pt=" + pageTitle + "&wa_vi_n="+ vidCreativeName + "&wa_vi_s=" + videoEvent + "&wa_lob=JPMC";
                $logImg.attr("style","display:none;");
                $logImg.attr("src", logSrc).appendTo("body");

                //console.log("25% watched: " + vidCreativeName);
                log25 = false;
            }
            if(api.video.time > half && log50 === true){

                videoEvent = "50Percent";
                curTime = new Date().getTime();
                $logImg = $("<img/>");
                logSrc = "https://www.chase.com/online/Home/images/wa01.gif?log=1&wa_tp=21&wa_cb=" + curTime +
                    "&wa_uri=" + pageLocation +
                    "&wa_pt=" + pageTitle + "&wa_vi_n="+ vidCreativeName + "&wa_vi_s=" + videoEvent + "&wa_lob=JPMC";
                $logImg.attr("style","display:none;");
                $logImg.attr("src", logSrc).appendTo("body");

                //console.log("50% watched: " + vidCreativeName);
                log50 = false;
            }
            if (api.video.time > threeQuarter && log75 === true){

                videoEvent = "75Percent";
                curTime = new Date().getTime();
                $logImg = $("<img/>");
                 logSrc = "https://www.chase.com/online/Home/images/wa01.gif?log=1&wa_tp=21&wa_cb=" + curTime +
                    "&wa_uri=" + pageLocation +
                    "&wa_pt=" + pageTitle + "&wa_vi_n="+ vidCreativeName + "&wa_vi_s=" + videoEvent + "&wa_lob=JPMC";
                $logImg.attr("style","display:none;");
                $logImg.attr("src", logSrc).appendTo("body");

                //console.log("75% watched: "+ vidCreativeName);
                log75 = false;
            }

        }, 1000 );

        api.bind("unload", function(){
            //console.log("Unload: " + vidCreativeName);
            clearInterval(interval);
            api.unbind("finish");
        });

        api.bind("finish", function(){

            videoEvent = "End";
            curTime = new Date().getTime();
            var $startLogImg = $("<img/>");
            var startLogSrc = "https://www.chase.com/online/Home/images/wa01.gif?log=1&wa_tp=21&wa_cb=" + curTime +
                "&wa_uri=" + pageLocation +
                "&wa_pt=" + pageTitle + "&wa_vi_n="+ vidCreativeName + "&wa_vi_s=" + videoEvent + "&wa_lob=JPMC";
            $startLogImg.attr("style","display:none;");
            $startLogImg.attr("src", startLogSrc).appendTo("body");

            //console.log("Player finished: " + vidCreativeName);
            playerFinished = true;

            //Reset the video to the beginning.
            api.stop();
            api.video.time = 0;

            /*When the player is done, reset the progression variables, so things will be logged in an
            additional play through.
            */
            logStart = true;
            log25 = true;
            log50 = true;
            log75 = true;
        });
    }

// Initialize modal script --------------
    $("#video-modal").heliumModal({
        vert: 50,
        speed: 500,
        easing: 'swing',
        onOpen: function(){
            $('#video-modal .captions').hide();
            var cleanVideos = [
                { webm: "http://jpmorganchase.com/content/dam/jpmorganchase/en/article/multimedia/"+currPlayer+".webm" },
                { mp4:  "http://jpmorganchase.com/content/dam/jpmorganchase/en/article/multimedia/"+currPlayer+".mp4" },
                { ogg:  "http://jpmorganchase.com/content/dam/jpmorganchase/en/article/multimedia/"+currPlayer+".ogg" }
            ];

            var subtitledVideos = [
                { webm: "http://jpmorganchase.com/content/dam/jpmorganchase/en/article/multimedia/"+currPlayer+"-cc.webm" },
                { mp4:  "http://jpmorganchase.com/content/dam/jpmorganchase/en/article/multimedia/"+currPlayer+"-cc.mp4" },
                { ogg:  "http://jpmorganchase.com/content/dam/jpmorganchase/en/article/multimedia/"+currPlayer+"-cc.ogg" }
            ];

            //Get a handle on the flowplayer API.
            var $fp = $("#modal-content .flowplayer").data("flowplayer");

            // write share attributes
            var mod="";
            var title="";
            var via="";
            var summary="";
            if (currPlayer){
                mod = "?mod="+currPlayer;
                if ($('a[data-vid-name="'+currPlayer+'"]').attr('data-share-title')){
                    title= $('a[data-vid-name="'+currPlayer+'"]').attr('data-share-title');
                }
                if ($('a[data-vid-name="'+currPlayer+'"]').attr('data-share-summary')){
                    summary= $('a[data-vid-name="'+currPlayer+'"]').attr('data-share-summary');
                }
                if ($('a[data-vid-name="'+currPlayer+'"]').attr('data-via')){
                    via = $('a[data-vid-name="'+currPlayer+'"]').attr('data-via');
                }
            }
            createShareLinks($(".modal-share"), mod, title, via, summary);

            //Check if the user agent is iPhone.
            if (navigator.userAgent.indexOf("iPhone") !== -1)
            {
                $('#video-modal .captions').show();
                //Add the vid class for user on iPhone.
                $(".flowplayer").prepend($("<div></div>").addClass("vid"));

                //If there is a touch event, load the videos and start playing.
                $(".flowplayer").on("touchstart",function()
                {
                    $fp.load(getCorrectVideos(cleanVideos,subtitledVideos));
                });
            }
            /*if this is a video that is being linked to via a MOD= URL, usually from a share button. Using the linkedVideo to
            check for this.
            */
            else if (linkedVideo){
                //set linkedVideo to false so if another modal is opened later, this code to not execute.
                linkedVideo = false;
                //Disable the flowplayer. This was the only way I could figure out how to keep it from autoplaying.
                $fp.disable(true);
                //Show the captions button.
                $('#video-modal .captions').show();

                //Show play button, only needed when video is not playing as soon as modal is opened. Also focus it.
                var $modalPlayBtn = $(".modalPlayBtn");
                $modalPlayBtn.show();
                $modalPlayBtn.focus();

                if (navigator.userAgent.indexOf("iPhone") > 0 || navigator.userAgent.indexOf("iPad") > 0){
                    $modalPlayBtn.find("img").show();
                }

                //When the play button is clicked, start the video.
                $modalPlayBtn.on("click",function(event)
                {
                    event.preventDefault();
                    //Enable the video.
                    $fp.disable(false);
                    //Hide the play button.
                    $modalPlayBtn.hide();
                    //load in the video files.
                    $fp.load(getCorrectVideos(cleanVideos,subtitledVideos));
                });
                //If the captions button is clicked, do the same as above.
                $("#video-modal .captions").click(function(){
                    $fp.disable(false);
                    $modalPlayBtn.hide();
                    $fp.load(getCorrectVideos(cleanVideos,subtitledVideos));
                });
            }

            //For all other user agents, start playing automatically.
            else {
                $fp.load(getCorrectVideos(cleanVideos,subtitledVideos));

                $fp.one("resume", function(){
                    $('#video-modal .captions').show();
                });
            }

            $("#modal-content .flowplayer").css('background-image','url(images/poster_'+currPlayer+'.jpg)' );

            $("#video-modal .transcript").attr({
                href:'http://www.jpmorganchase.com/corporate/Home/transcript?transcript=/content/dam/jpmorganchase/en/article/multimedia/'+currPlayer+'.xml'
            });

        }, // callback for custom JS when modal opens
        afterOpen: function(){
            //When a modal window is closed, the video is no longer linked, so turn this off.
            linkedVideo = false;
            //Create a variable for the close button and transcript buttons, since they are used several times.
            var $closeButton = $("#video-modal .modal .x-button");
            var $modalPlayBtn = $(".modalPlayBtn");

            //If there is a play button (usually for linked videos), focus that.
            if ($modalPlayBtn.css("display") !== "none"){
                $modalPlayBtn.focus();
                $modalPlayBtn.attr("tabIndex", "2");
            }
            else{
                //Otherwise focus the close button after the modal is opened.
                $closeButton.focus();
            }

            //Call the function for setting up accessiability in the player.
            modalFlowplayer();

            /*Set the tab overide options for the close button and the mail button. This keeps the
             the focus from leaving the modal window when using the keyboard until the modal is closed.
            */
            // Updated  4/27/15 to change the last link int hr modal (mail) to the transcript
            // to accomodate hiding the sharign in the video modal.
            tabOverrides($closeButton, $(".modal .transcript"), null);
            tabOverrides( $(".modal .transcript"), null, $closeButton);
            $closeButton.focusin();


        },  // callback for custom JS when modal close,// callback for custom JS when modal opens
        onClose: function(){
            var $fp = $("#modal-content .flowplayer").data("flowplayer");
            $fp.disable(false);
            $(".modalPlayBtn").hide();

            $("#modal-content .flowplayer").data("flowplayer").unload();
            //Unbind the focusout event for the transcript button.
            $("#modal-content .transcript").off("focusin");


            $("a[data-vid-name='"+currPlayer+"']").focus().parents('.overlay-box, .video-sidebar, .video-slide-mod').find('h3 a, h2 a').focus();
            //If the vid class was added in for the video icon, (usually on mobile) then remove it.
            if ($(".flowplayer .vid"))
            {
                $(".flowplayer .vid").remove();
            }
        }  // callback for custom JS when modal close
    });

//For video captions.
$('#video-modal .captions').click(function(){

    $('#video-modal .captions').hide();
    if ($(this).hasClass("off"))
    {
        $(this).removeClass("off").addClass("on");

        $("#modal-content .flowplayer").data("flowplayer").load([
            { webm: "http://jpmorganchase.com/content/dam/jpmorganchase/en/article/multimedia/"+currPlayer+"-cc.webm" },
            { mp4:  "http://jpmorganchase.com/content/dam/jpmorganchase/en/article/multimedia/"+currPlayer+"-cc.mp4" },
            { ogg:  "http://jpmorganchase.com/content/dam/jpmorganchase/en/article/multimedia/"+currPlayer+"-cc.ogg" }
        ]);
        $("#video-modal .captions").html("Turn Captions Off");
    }
    else if ($(this).hasClass("on")){
        $(this).removeClass("on").addClass("off");

        $("#modal-content .flowplayer").data("flowplayer").load([
            { webm: "http://jpmorganchase.com/content/dam/jpmorganchase/en/article/multimedia/"+currPlayer+".webm" },
            { mp4:  "http://jpmorganchase.com/content/dam/jpmorganchase/en/article/multimedia/"+currPlayer+".mp4" },
            { ogg:  "http://jpmorganchase.com/content/dam/jpmorganchase/en/article/multimedia/"+currPlayer+".ogg" }
        ]);
        $("#video-modal .captions").html("Turn Captions On");
    }
    $('#video-modal .captions').show();
    return false;
});


//For video captions in an inline player.
$('.inlineVidContainer .captions').click(function(){
    $('.inlineVidContainer .captions').hide();
    var currInlineName = $(this).closest(".inlineVidContainer").find(".inline-video").attr("data-vid-name");
    if ($(this).hasClass("off"))
    {
        $(this).removeClass("off").addClass("on");

        $(".inlineVidContainer .flowplayer").data("flowplayer").load([
            { webm: "http://www.jpmorganchase.com/content/dam/jpmorganchase/en/article/multimedia/" + currInlineName + "-cc.webm" },
            { mp4:  "http://www.jpmorganchase.com/content/dam/jpmorganchase/en/article/multimedia/" + currInlineName + "-cc.mp4" },
            { ogg:  "http://www.jpmorganchase.com/content/dam/jpmorganchase/en/article/multimedia/" + currInlineName + "-cc.ogg" }
        ]);
        $(".inlineVidContainer .captions").html("Captions Off");
    }
    else if ($(this).hasClass("on")){
        $(this).removeClass("on").addClass("off");

        $(".inlineVidContainer .flowplayer").data("flowplayer").load([
            { webm: "http://www.jpmorganchase.com/content/dam/jpmorganchase/en/article/multimedia/" + currInlineName + ".webm" },
            { mp4:  "http://www.jpmorganchase.com/content/dam/jpmorganchase/en/article/multimedia/" + currInlineName + ".mp4" },
            { ogg:  "http://www.jpmorganchase.com/content/dam/jpmorganchase/en/article/multimedia/" + currInlineName + ".ogg" }
        ]);
        $(".inlineVidContainer .captions").html("Captions On");
    }
    $('.inlineVidContainer .captions').show();
    return false;
});

 var newsModal;
    $('*[data-mid="newsletter-modal"]').each(function(){
        $(this).click(function(){
            newsModal = $(this).attr('data-newsletter-name');
            return false;
        });
    });

     $("#newsletter-modal").heliumModal({
        vert: 50,
        speed: 500,
        easing: 'swing',
        onOpen: function(){
            $("#modal-content-news").load([
            ]);

        }, // callback for custom JS when modal opens
        afterOpen: function(){
            $("#newsletter-modal .modal .x-button").focus();
            modalFlowplayer();
        },  // callback for custom JS when modal close,// callback for custom JS when modal opens
        onClose: function(){
            $("#modal-content-news").unload();
        }  // callback for custom JS when modal close
    });

/*The pause on hover and/or pause on focus was causing a problem on iOS. Now changed to initalize 
differently on iOS devices to avoid this problem.
*/
if ( (navigator.userAgent.indexOf("iPad") !== -1) || (navigator.userAgent.indexOf("iPhone") !== -1) ) {
    // Initialize Slider ------------------- For Mobile and Tablet
    $("#homepage").heliumSlider({
        paneFade: false ,            // pane fade in: true or false
        paneXOffset: [15] ,         // pane animate X offset: pixel value
        paneYOffset: [0] ,          // pane animate Y offset: pixel value
        paneDelay: [1000] ,         // pane delay before animate: milliseconds
        paneSpeed: [500] ,          // pane speed of animate: milliseconds
        useNav: true ,              // use navigation: true or false
        navTemplate: "&bull;" ,     // template for contents of nav list item
        autoPlay: 6500 ,            // auto play slides. delay between transitions: milliseconds (negative values reverse direction.  any value between -20 and 20 disables autoplay)
        speed: 1000 ,               // speed of main slide animation (when using fader mode, speed becomes the delay between fade in and fade out animations.)
        easing: "linear" ,          // easing type: "swing" or "linear"
        mainFadeIn: false ,         // fade new slide in speed: milliseconds or false (mainFadeOut must be set)
        mainFadeOut: false ,        // fade old slide out speed: milliseconds or false (mainFadeIn must be set)
        autoStopSlide: "first" ,    // stop auto play on this slide: number or false
        autoStopLoop: 3 ,           // stop auto play after looping through slides this many times: number or false
        autoStopPause: true,        // When the auto play completes, pause the auto play and keep controls available. -MG 8/21/14
        afterSlide: function(){
        // this callback function fires after the main slide transition takes place
    } ,
        pauseOnFocus: false,         // pause autoplay on focus: true or false
        pauseOnHover: false,         // pause autoplay on hover: true or false
        pauseControls: true         // Show Play/Pause controls: true or false
    });
}
else{
    // Initialize Slider ------------------- For desktop
    $("#homepage").heliumSlider({
        paneFade: false ,            // pane fade in: true or false
        paneXOffset: [15] ,         // pane animate X offset: pixel value
        paneYOffset: [0] ,          // pane animate Y offset: pixel value
        paneDelay: [1000] ,         // pane delay before animate: milliseconds
        paneSpeed: [500] ,          // pane speed of animate: milliseconds
        useNav: true ,              // use navigation: true or false
        navTemplate: "&bull;" ,     // template for contents of nav list item
        autoPlay: 6500 ,            // auto play slides. delay between transitions: milliseconds (negative values reverse direction.  any value between -20 and 20 disables autoplay)
        speed: 1000 ,               // speed of main slide animation (when using fader mode, speed becomes the delay between fade in and fade out animations.)
        easing: "linear" ,          // easing type: "swing" or "linear"
        mainFadeIn: false ,         // fade new slide in speed: milliseconds or false (mainFadeOut must be set)
        mainFadeOut: false ,        // fade old slide out speed: milliseconds or false (mainFadeIn must be set)
        autoStopSlide: "first" ,    // stop auto play on this slide: number or false
        autoStopLoop: 3 ,           // stop auto play after looping through slides this many times: number or false
        autoStopPause: true,        // When the auto play completes, pause the auto play and keep controls available. -MG 8/21/14
        afterSlide: function(){
        // this callback function fires after the main slide transition takes place
    } ,
        pauseOnFocus: true,         // pause autoplay on focus: true or false
        pauseOnHover: true,         // pause autoplay on hover: true or false
        pauseControls: true         // Show Play/Pause controls: true or false
    });    
}
    $("#CR-home").heliumSlider({
        paneFade: true ,        // pane fade in: true or false
        paneXOffset: [15] ,    // pane animate X offset: pixel value
        paneYOffset: [0] ,      // pane animate Y offset: pixel value
        paneDelay: [1000] ,     // pane delay before animate: milliseconds
        paneSpeed: [500] ,     // pane speed of animate: milliseconds
        useNav: true ,          // use navigation: true or false
        navTemplate: "&bull;" , // template for contents of nav list item
        autoPlay: 6500 ,        // auto play slides. delay between transitions: milliseconds (negative values reverse direction.  any value between -20 and 20 disables autoplay)
        speed: 50 ,            // speed of main slide animation (when using fader mode, speed becomes the delay between fade in and fade out animations.)
        easing: "swing" ,       // easing type: "swing" or "linear"
        mainFadeIn: 800 ,     // fade new slide in speed: milliseconds or false (mainFadeOut must be set)
        mainFadeOut: 1000 ,     // fade old slide out speed: milliseconds or false (mainFadeIn must be set)
        autoStopSlide: "first" ,// stop auto play on this slide: number or false
        autoStopLoop: 1 ,       // stop auto play after looping through slides this many times: number or false
        autoStopPause: true,   // When the auto play completes, pause the auto play and keep controls available. -MG 8/21/14
        afterSlide: function(){
        // this callback function fires after the main slide transition takes place
        } ,
        pauseOnFocus: true,      // pause autoplay on hover: true or false
        pauseControls: true      // pause autoplay on hover: true or false
    });
$(".content-slider").each( function(){
    $(this).heliumSlider({
        paneFade: true ,        // pane fade in: true or false
        paneXOffset: [15] ,     // pane animate X offset: pixel value
        paneYOffset: [0] ,      // pane animate Y offset: pixel value
        paneDelay: [1000] ,     // pane delay before animate: milliseconds
        paneSpeed: [500] ,      // pane speed of animate: milliseconds
        useNav: true ,          // use navigation: true or false
        navTemplate: "&bull;" , // template for contents of nav list item
        autoPlay: 0 ,           // auto play slides. delay between transitions: milliseconds (negative values reverse direction.  any value between -20 and 20 disables autoplay)
        speed: 1000             // speed of main slide animation (when using fader mode, speed becomes the delay between fade in and fade out animations.)
    });
});

    $("#Detroit-home").heliumSlider({
        paneFade: true ,        // pane fade in: true or false
        paneXOffset: [15] ,    // pane animate X offset: pixel value
        paneYOffset: [0] ,      // pane animate Y offset: pixel value
        paneDelay: [1000] ,     // pane delay before animate: milliseconds
        paneSpeed: [500] ,     // pane speed of animate: milliseconds
        useNav: false ,          // use navigation: true or false
        navTemplate: "&bull;" ,
        autoPlay: 0 ,        // auto play slides. delay between transitions: milliseconds (negative values reverse direction.  any value between -20 and 20 disables autoplay)
        speed: 50 ,            // speed of main slide animation (when using fader mode, speed becomes the delay between fade in and fade out animations.)
        easing: "swing" ,       // easing type: "swing" or "linear"
        mainFadeIn: 800 ,     // fade new slide in speed: milliseconds or false (mainFadeOut must be set)
        mainFadeOut: 1000 ,    // fade old slide out speed: milliseconds or false (mainFadeIn must be set)
        autoStopSlide: "first" ,// stop auto play on this slide: number or false
        autoStopLoop: 1 ,       // stop auto play after looping through slides this many times: number or false
        afterSlide: function(){
        // this callback function fires after the main slide transition takes place
        } ,
        pauseOnHover: false      // pause autoplay on hover: true or false
    });
});

// Legal Disclaimer functionality
var $legalLink = $(".legal-link");
var $leagalDisclaimer = $(".legal-disclaimer");
var $legalWrapper = $(".legal-wrapper");
var legalHeight = "180px";

var legalHeight;

function openLegal(){
    $leagalDisclaimer.css("display","block");
    $leagalDisclaimer.animate({
        height: legalHeight + "px"
    },{
        queue: false,
        complete: function(){
            $legalWrapper.addClass("open");
            $leagalDisclaimer.css("display","");
        }
    });

    $legalWrapper.closest(".mainbody").animate({
        height: "+="+legalHeight + "px"
    },{
        queue: false,
        complete: function(){
            $legalWrapper.closest(".mainbody").css("height","");
        }
    });
}

function closeLegal(){
    $leagalDisclaimer.animate({
        height: "0px"
    },{
        queue: false,
        complete: function(){
            $legalWrapper.removeClass("open");
        }
    });

    $legalWrapper.closest(".mainbody").animate({
        height: "-="+legalHeight+"px"
    });
}

//Legal disclaimer toggle.
$legalLink.click(function(){
    if ($legalWrapper.hasClass("open")){
        closeLegal();
    }
    else{
        legalHeight = $leagalDisclaimer.css("height","auto").outerHeight();
        $leagalDisclaimer.css("height","0px");
        openLegal();
    }
    return false; //prevent loading href
});

//Smooth Scrolling 
$(function() {
  $('a.smooth[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') === this.pathname.replace(/^\//,'') && location.hostname === this.hostname) {
      var target = $(this.hash);
      var pageOffSet = 133;
      if ($(window).width() <= 768){
        pageOffSet = 80;
      }
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top - pageOffSet
        }, 1000);
        return false;
      }
    }
  });
});


    function hideAllArticles(){
        $(".ac-container article.active").removeClass('active').animate({ opacity: 0 }, 200, function(){
            $(this).hide();
        });
    }

    $(".ac-tabs .tab").click( function(){

        $('.ac-tabs .tab').removeClass('active');
        $(this).addClass('active');

        if ($(this).hasClass("ac-1")){
            hideAllArticles();
            $(".ac-container article.ac-1").addClass('active').show().animate({ opacity: 1 }, 600);
        }
        else if ($(this).hasClass("ac-2")){
            hideAllArticles();
            $(".ac-container article.ac-2").addClass('active').show().animate({ opacity: 1 }, 600);
        }
        else if ($(this).hasClass("ac-3")){
            hideAllArticles();
            $(".ac-container article.ac-3").addClass('active').show().animate({ opacity: 1 }, 600);
        }
        $.fn.matchHeight._update();
        return false;

    });
    $(".ac-tabs .tab.ac-1").trigger('click');

    $('.effect-dyk').on('touchstart',function(){
        $(this).toggleClass('touched');
    });

