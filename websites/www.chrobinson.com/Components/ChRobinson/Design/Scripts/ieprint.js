if (window.attachEvent && /MSIE (5\.5|6)/.test(navigator.userAgent)) {
    function printPNGFix(disable) {
        for (var i = 0; i < document.all.length; i++) {
            var e = document.all[i];
            if (e.filters['DXImageTransform.Microsoft.AlphaImageLoader'] || e._png_print) {
                if (disable) {
                    e._png_print = e.style.filter;
                    e.style.filter = '';
                }
                else {
                    e.style.filter = e._png_print;
                    e._png_print = '';
                }
            }
        }
    };
    window.attachEvent('onbeforeprint', function() { printPNGFix(1) });
    window.attachEvent('onafterprint', function() { printPNGFix(0) });
}