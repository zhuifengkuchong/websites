var HeroSliderSettings = {
    responsive: true,
    width: '100%',
    height: 'variable',
    items: {
        height: 'auto'
    },
    prev: '.prev',
    next: '.next',
    pagination: ".pag",
    scroll: {
        items: 1,
        easing: 'swing',
        duration: 1500,
        pauseOnHover: true,
        queue: true
    },
    play: false

};

var FeatureSliderSettings = {
    responsive: true,
    height: 212,
    circular: true,
    infinite: true,
    items: { visible: 3 },
    width: '100%',
    prev: { button: function () {
        var $wrapper = $(this).parent().parent();
        var $prev = $wrapper.find('.highlightFeature-slidePrev');
        return $prev;
    }
    },
    next: { button: function () {
        var $wrapper = $(this).parent().parent();
        var $next = $wrapper.find('.highlightFeature-slideNext');
        return $next;
    }
    },
    scroll: {
        items: 1,
        easing: 'swing',
        duration: 1000,
        pauseOnHover: true,
        queue: true
    },
    auto: { play: false },
    swipe: {
        onTouch: true,
        onMouse: true
    }
};
var FeatureSliderSettingsIe = {
    responsive: true,
    height: 212,
    circular: true,
    infinite: true,
    items: { visible: 3, width: '140px' },
    width: '100%',
    prev: { button: function () {
        var $wrapper = $(this).parent().parent();
        var $prev = $wrapper.find('.highlightFeature-slidePrev');
        return $prev;
    }
    },
    next: { button: function () {
        var $wrapper = $(this).parent().parent();
        var $next = $wrapper.find('.highlightFeature-slideNext');
        return $next;
    }
    },
    scroll: {
        items: 1,
        easing: 'swing',
        duration: 1000,
        pauseOnHover: true,
        queue: true
    },
    auto: { play: false },
    swipe: {
        onTouch: true,
        onMouse: true
    }
};
function initHeroSlider() {
    $('.carouselItem>img').height((Math.min($(window).width(), 1199) / 2.66));
    $('.carousel').carouFredSel(HeroSliderSettings);
    if (jQuery.support.leadingWhitespace) {
        //Write your code for IE7 and IE8 browsers
        $(window).resize(function () {
            $('.carousel').carouFredSel(HeroSliderSettings);
        });
    }
    $('.carouselItem>img').height('auto');
}

function initRAQ() {
    $('#slServicesQuote').change(function () {
        var qs = $(this).val();
        window.location = $('.js-raq-link').attr('href') + "?qs=" + qs;
    });
}
function initFeaturedSliders() {
    $('.highlightFeature-slides').each(function () {
        if ($(this).children('.highlightFeature-slide').length > 3) {
            if (jQuery.support.leadingWhitespace) {
                $(this).caroufredsel(FeatureSliderSettings);
            } else {
                $(this).caroufredsel(FeatureSliderSettingsIe);
            }
        } else {
            $(this).siblings('button').hide();
        }
    });
}

$(document).ready(function () {
    if ($('.highlightFeature-slides').length) {
        initFeaturedSliders();
    }
    $('#primary-nav .dropdown').not('.nav-search').hover(function () {
        if ($(window).width() > 990) {
            $(this).addClass('open');
        }
    }, function (e) {
        if ($(window).width() > 991) {
            if (e.relatedTarget != null) {
                $(this).removeClass('open');
            }
        }
    });
    if ($('.carousel').length) {
        initHeroSlider();
    }

    if ($('#slServicesQuote').length) {
        initRAQ();
    }
    $("#cse-search-box input[type='text']").focus(function () {
        $("#cse-search-box input[type='text']").css("background", "#fff")
    });

    var tn1 = $('.mygallery').tn3({
        skinDir: "/Presentation/chrobinson/tn3skins",
        responsive: "width",
        autoplay: true,
        delay: 5000,
        skin: "tn3e",
        mouseWheel: false,
        imageClick: "url",
        image: {
            crop: false,
            transitions: [{
                type: "blinds",
                duration: 300
            },
			    {
			        type: "grid",
			        duration: 160,
			        gridX: 9,
			        gridY: 7,
			        easing: "easeInCubic",
			        sort: "circle"
			    }, {
			        type: "slide",
			        duration: 430,
			        easing: "easeInOutExpo"
			    }]
        },
        touch: {
            fsMode: "http://www.chrobinson.com/tn3_touch_fullscreen.html"
        }
    });

});
