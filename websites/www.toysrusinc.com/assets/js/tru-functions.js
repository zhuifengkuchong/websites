/*
Site:           toysrusinc.com
Page:           tru-functions.js
Modified:       Apr 23 2010
----------------------------------------------------------- */
$(document).ready( function(){


// $("a[@rel~='lightbox']").addClass("link_pic");
// 
// $("a[@rel='external']").not(".noicon").addClass("external");
// $("a[@rel='external']").click(function() {
//  return !window.open($(this).attr("href"));
//  });

//==> search form at top of each page
	var srchtext = 'Search Toys"R"Us, Inc.';
	var srchtextPress = 'Search Press Releases';
	
	var srchField = $("#search-form #searchText");
	srchField.attr("value", srchtext); 
	srchField.focus( function(){if(srchField.val() == srchtext){srchField.attr("value", '');}});
	srchField.blur( function(){if(srchField.val() == ''){srchField.attr("value", srchtext); }});

	var srchFieldContentArea = $("#search-contentarea #searchText-contentarea");
	srchFieldContentArea.attr("value", srchtext); 
	srchFieldContentArea.focus( function(){if(srchFieldContentArea.val() == srchtext){srchFieldContentArea.attr("value", '');}});
	srchFieldContentArea.blur( function(){if(srchFieldContentArea.val() == ''){srchFieldContentArea.attr("value", srchtext); }});

	var srchFieldPress = $("#searchText-contentarea-pr");
	srchFieldPress.attr("value", srchtextPress); 
	srchFieldPress.focus( function(){if(srchFieldPress.val() == srchtextPress){srchFieldPress.attr("value", '');}});
	srchFieldPress.blur( function(){if(srchFieldPress.val() == ''){srchFieldPress.attr("value", srchtextPress); }});

});