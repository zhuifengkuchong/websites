<script id = "race1b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race1={};
	myVars.races.race1.varName="foo2_prev__onmouseover";
	myVars.races.race1.varType="@varType@";
	myVars.races.race1.repairType = "@RepairType";
	myVars.races.race1.event1={};
	myVars.races.race1.event2={};
	myVars.races.race1.event1.id = "foo2_prev";
	myVars.races.race1.event1.type = "onmouseover";
	myVars.races.race1.event1.loc = "foo2_prev_LOC";
	myVars.races.race1.event1.isRead = "True";
	myVars.races.race1.event1.eventType = "@event1EventType@";
	myVars.races.race1.event2.id = "Lu_window";
	myVars.races.race1.event2.type = "onload";
	myVars.races.race1.event2.loc = "Lu_window_LOC";
	myVars.races.race1.event2.isRead = "False";
	myVars.races.race1.event2.eventType = "@event2EventType@";
	myVars.races.race1.event1.executed= false;// true to disable, false to enable
	myVars.races.race1.event2.executed= false;// true to disable, false to enable
</script>

