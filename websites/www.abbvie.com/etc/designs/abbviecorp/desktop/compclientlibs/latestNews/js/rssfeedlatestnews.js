/**
 * Utility class for all functions related to LatestNews stuff
 */
LatestNewsUtils = {}
/*
*  method call for getting latest news
* @return String representing a brand. i.e 'ws'.
*/

// normal Ajax Call
function latestNewsJSONAjax(rssFeedUrlValue, fromDialog, callback) {
    var servletURL = '/bin/latestnewsNoChoice';		
    $.ajax({
        url :servletURL,
        async :true,
        data: {"url": rssFeedUrlValue },
        type: "GET",
        error: function(obj) {
            completeNewsResults = obj;         
           
            callback();
        },
        success : function(obj) {
        	completeNewsResults = obj;        	
           callback();
        }
    });
    // end of ajax call
}

function checkForSecondNewsAccomodation(){
	var contentCounter = 0;
	var modifiedresult ="";
	var counter = 0;
	$(function(){      
        $.each(completeNewsResults.items, function(){  
        	contentCounter = contentCounter + this.content.length;
        	if (contentCounter < 350 && counter <=1){
        		modifiedresult =modifiedresult.concat(counter.toString());
        		counter++;

        	}
        });
    });
	if (modifiedresult == ""){
		modifiedresult = "0";
	}
	if (modifiedresult != "0"){
		singleBigNews = false;
	}
	return modifiedresult;
}

function newsCount(newsCountAllNewsItems, contentsize){
	var contentCounter = 0;
	var result ="";
	var counter = 0;
  	$(function(){      
        $.each(newsCountAllNewsItems, function(){   
        	contentCounter = contentCounter + this.content.length;
        	if (contentCounter < contentsize ){
        		result =result.concat(counter.toString());
        		counter++;
        	}       	
        });
    });
    if (result == ""){
		result ="0";        			
	}
	if (result =="0"){
		result=	checkForSecondNewsAccomodation();
	}
   	return result;
}

function configureSingleNews(news){
	var tmpNews = news;
	var k=400;
	if (news.length >400){	
		while (k > 0 ){
		    if (news.charAt(k) == ' ' ||
		    		news.charAt(k+1) == ' '){
		    	break;
		    }
		    k--;
		}
		tmpNews = news.substring(0, k)+ " ...";
	}	
	return tmpNews;	
}
function displayLogic(){
	 allNewsItems = completeNewsResults.items
	 if(selectionCount != ""){
         $.each(allNewsItems, function(){       
             if(selectionCount.indexOf(this.value) > -1)
             {
            	 if (singleBigNews){
            		 testSingleBigNews = this.content;
            		 testSingleBigNews =configureSingleNews(testSingleBigNews);
            		 $('<p>'+this.date+'<br/><a href="'+this.url+'">'+testSingleBigNews
                             +'</a></p>').appendTo("div.newsContentLeft");
            	 }else{            		 
                 $('<p>'+this.date+'<br/><a href="'+this.url+'">'+this.content
                   +'</a></p>').appendTo("div.newsContentLeft");
            	 }
             } 
            	 
         });
     }else{         
         if (mode == 'EDIT' || mode == 'DESIGN') {
             if (completeNewsResults.StatusDescription == "") {
                 $('<p>Edit dialog to configure News.'
                   +'.</p>')
                 .appendTo("div.newsContentLeft");
             }else{
                 $('<p>'+completeNewsResults.StatusDescription
                   +'</p>')
                 .appendTo("div.newsContentLeft");
             }
         }else { 
             $('<p>News feed is down for maintenance. We apologize for the disruption.'
               +'<br/>Please try again after a few minutes.</p>')
             .appendTo("div.newsContentLeft");
         }
     }         
    $("div.newsContentLeft p:last").addClass("last");
    finalDisplay();
}
function finalDisplay(){
    $("div.newsContentLeft p").removeClass("last");
    $("div.newsContentLeft p:last").addClass("last");
    if($(".latestnews").height()>$(".newsFeed").height()){
        $("div.newsContentLeft p:last").remove();
        finalDisplay();
    }
    return;
}