/* Adobe Insight Page Tagging developed for The Hartford */
/* Author: Brian Baker */
/* Designed for: The Hartford */
/* Corporate site */

/* Page Level Data Collection */

//(function(){
var ct = "<img src=";
var cp = location.protocol === "https:" ? "https://" : "http://";
var cd = "http://ni.thehartford.com/corp/hig/prod/ni.thehartford.com";
var cu = "http://ni.thehartford.com/zag.gif?Log=1";
var ce = ">";

/* v2nd cookie for in case v1st is blocked */
var Cookies = document.cookie;
if(Cookies.indexOf("v2nd=") !== -1) {
	var CookieStart = Cookies.indexOf("v2nd=") + 5;
	var CookieEnd = Cookies.indexOf(";", CookieStart);
	if(CookieEnd === -1) {
		CookieEnd = Cookies.length;
	}

	var _v2nd = Cookies.substring(CookieStart, CookieEnd);
}

if(Cookies.indexOf("v2nd=") === -1) {
	var expiration = new Date();
	var _v2nd = expiration.getTime();
	expiration.setTime(expiration.getTime() + (365 * 86400000));
	document.cookie = "v2nd=" + _v2nd + "; expires=" + expiration.toGMTString() + ";path=/;";
}

//  ---------------------------------------

/* TLTSID cookie - Tea Leaf */
var Cookies = document.cookie;
if(Cookies.indexOf("TLTSID") !== -1) {
	var CookieStart = Cookies.indexOf("TLTSID=") + 7;
	var CookieEnd = Cookies.indexOf(";", CookieStart);
	if(CookieEnd === -1) {
		CookieEnd = Cookies.length;
	}

	var _TLTSID = Cookies.substring(CookieStart, CookieEnd);
}

//  ---------------------------------------

/* bounce_dur cookie as counter for time on page */
var Cookies = document.cookie;
if(Cookies.indexOf("bounce_dur=") !== -1) {
	var CookieStart = Cookies.indexOf("bounce_dur=") + 11;
	var CookieEnd = Cookies.indexOf(";", CookieStart);
	if(CookieEnd === -1) {
		CookieEnd = Cookies.length;
	}

var _bounce_dur = parseInt(Cookies.substring(CookieStart, CookieEnd))+1;
}

if(Cookies.indexOf("bounce_dur=") === -1) {
	var expiration = new Date();
	var _bounce_dur = 1;
	expiration.setTime(expiration.getTime() + 3600000);
	document.cookie = "bounce_dur=" + _bounce_dur + "; expires=" + expiration.toGMTString() + ";path=/;";
}

if (_bounce_dur == 1) {
(function (tos) {
var timesRun = 0;
var interval = setInterval(function(){
    timesRun += 1;
    if(timesRun === 12){
        clearInterval(interval);
    }
    tos = (function (t) {
      return t[0] == 50 ? (parseInt(t[1]) + 1) + ':00' : (t[1] || '0') + ':' + (parseInt(t[0]) + 10);
    })(tos.split(':').reverse());
eventCapture('Time on Page 10s Interval','Page Tag',tos,'');
}, 10000);
})('00');
}

//  ---------------------------------------

function getMeta(mtag) {
var metaElements = document.getElementsByTagName('META');
 metaWords = new Array();
 var i = 0;
 for (var m = 0; m < metaElements.length; m++)
   if (metaElements[m].name == mtag) {
     metaWords[i++] = metaElements[m].content;
 			return metaWords[0];
			}
}

//  ---------------------------------------

// Initialize string for ep params
getMeta('HIG.Template');
var higTemplate = getMeta('HIG.Template');
var eps = "";
var c = {};
c.sw = screen.width;
c.sh = screen.height;
c.cd = screen.colorDepth;
var d = {};
d.du = document.location.href;
d.dt = document.title;
d.dr = document.referrer;
d.v2nd = _v2nd;
d.TLTSID = _TLTSID;
d.template = metaWords[0];
v.zig = "corp/hig/prod";
var epv = "&et=" + encodeURIComponent('Page View') + '&ep=' + encodeURIComponent('Page Tag');
var cdt = "&cb=" + new Date().getTime();
var _vo = "";
//v.partner = splitParamsOnQ(document.location.href);

//Maxymiser integration
addEvent(window, 'load', function(e) {
if ((typeof ai_mm_cmp != 'undefined') && (ai_mm_cmp != '')) {
v.ai_mm_cmp = ai_mm_cmp;
v.ai_mm_el_id = ai_mm_el_id;
eventCapture('Maxymiser','Page Tag', 'Maxymiser','');
	}
});

//Add email code - eml
splitParamsOnQ(document.location.href);
if( typeof v !== "undefined") {
	for(vKey in v) {
		_vo = _vo + "&" + "v_" + vKey + "=" + encodeURIComponent(v[vKey]);
	}
}

var _do = "";
for(dKey in d) {
	_do = _do + "&" + "d_" + dKey + "=" + encodeURIComponent(d[dKey]);
}

var _co = "";
for(cKey in c) {
	_co = _co + "&" + "c_" + cKey + "=" + encodeURIComponent(c[cKey]);
}

/* var _vsearch = "";
if( typeof vsearch !== "undefined") {
	for(vKey in vsearch) {
		_vsearch = _vsearch + "&" + "v_" + vKey + "=" + encodeURIComponent(vsearch[vKey]);
	}
} */

// if v vars undefined on page level, set as 0|1 on site level
if( typeof vlc === 'undefined') {
	var vlc = "1";
	//automatic link tracking
}
if( typeof vfc === 'undefined') {
	var vfc = "0";
	//automatic form tracking
}
if( typeof vic === 'undefined') {
var vic = "0";
	//automatic input field tracking
}
if( typeof vbc === 'undefined') {
	var vbc = "0";
	//automatic button tracking
}
if( typeof vie === 'undefined') {
	var vie = "1";
	//automatic input element tracking
}
if( typeof vtac === 'undefined') {
	var vtac = "1";
	//automatic textarea tracking
}
if( typeof vsc === 'undefined') {
	var vsc = "1";
	//automatic select tracking
}
if( typeof vim === 'undefined') {
	var vim = "1";
	//automatic image map tracking
}
if( typeof vceic === 'undefined') {
	var vceic = "0";
	//automatic custom id event tracking
	var Id = "";
	var Evt = "click";
}
if( typeof vcenc === 'undefined') {
	var vcenc = "0";
	//automatic custom name event tracking
	var Name = "textarea";
	var Evt = "blur";
}
if( typeof vcecc === 'undefined') {
	var vcecc = "0";
	//automatic custom class event tracking
	var Class = "";
	var Evt = "";
}

//document.write(ct + cp + cd + cu + epv + _do + _vo + _co + _vsearch + cdt + ce);
//document.write(ct + cp + cd + cu + epv + _do + _vo + _co + cdt + ce);
var imageReq = ct + cp + cd + cu + epv + _do + _vo + _co + cdt + ce;
/* var pageView = document.createElement("span");
pageView.innerHTML = imageReq;
document.getElementsByTagName('body')[0].appendChild(pageView); */

var utag_data = utag_data || {};
utag_data.qs_epv = epv;
utag_data.qs_do = _do;
utag_data.qs_vo = _vo;
utag_data.qs_co = _co;


//  Remove extraneous chars, capitalize utility fctn
String.prototype.capitalize = function() {
	return this.replace(/(^|\s)([a-z])/g, function(m, p1, p2) {
		return p1 + p2.toUpperCase();
	});
};
//  ---------------------------------------

//  Get Overlay Class Name
function getByClass(className, tagName, parentElement) {
	parentElement = parentElement || document;
	tagName = tagName || "*";

	if(parentElement.getElementsByClassName) {
		return parentElement.getElementsByClassName(className);
	} else {
		var classNameTest = new RegExp("\\b" + className + "\\b");
		var elementsByTag = parentElement.getElementsByTagName(tagName);
		var results = [];
		for(var i = -1, el; el = elementsByTag[++i]; ) {
			if (classNameTest.test(el.className)) {
			results.push(el);
		}
	}
	return results;
	}
}

//  ---------------------------------------
// split ep parameters and build string
function epSplitParams(t) {
    t = unescape(t);
    var s_arr = [];
    var params = t.split(/&(?=[a-zA-Z0-9_]+=)/);
    params.shift();
    if (params) {
        var l = params.length;
        for (var i = 0; i < l; i++) {
            s_arr = params[i].split('=');

            eps = eps + "&" + escape(s_arr[0]) + "=" + escape(s_arr[1]);

        }
    }

    return eps;
}

//  ---------------------------------------

function splitParamsOnQ(t) {
	try{
 t = decodeURIComponent(t);
 	}
catch(err) {};
	var s_arr = [];
	var re = /\?(?=[a-zA-Z0-9_]+=)/;
	if (t.match(re)) {
	  var params = t.split('?')
	  re = /&(?=[a-zA-Z0-9_]+=)/;
	  if (params[1].match(re)) {
	    params = params[1].split('&')
	  }
	}
	if(params) {
		var l = params.length;
		for(var i = 0; i < l; i++) {
			s_arr = params[i].split('=');
			//eps = eps + "&" + encodeURIComponent(s_arr[0]) + "=" + encodeURIComponent(s_arr[1]);
			if (s_arr[0]==='eml') {
			v.ai_etaddress = s_arr[1];
			}

		}
	}

	return {params:params, eps:eps};
	
}

//  ---------------------------------------

function getLpos(t) {
	var lposFound = false;
	var h = t.href;
	h = decodeURIComponent(h);

	searchForLpos(h,'?');
	searchForLpos(t.name,'&');
	
	function searchForLpos(p,s) {
	var s_arr = [];
	if (s==='?') {
	  var re = /\?(?=[a-zA-Z0-9]+=)/;
	if (p.match(re)) {
	  var params = p.split('?');
	  re = /&(?=[a-zA-Z0-9]+=)/;
	  if (params[1].match(re)) {
	    params = params[1].split('&')
	  }
	}
	} else {
	  var params = p.split(/&(?=[a-zA-Z0-9]+=)/);
	if (params) {
	  params.shift();
	}
	}
	if ((params) && (lposFound != true)) {
		var l = params.length;
		for(var i = 0; i < l; i++) {
			s_arr = params[i].split('=');
			if (s_arr[0] === 'lpos') {
			  lposFound = true;
			  eps = eps + "&" + encodeURIComponent(s_arr[0]) + "=" + encodeURIComponent(s_arr[1]);
			  return eps;
		}
   }
	}
}

	if (lposFound != true)	{

	 if (t.parentNode.parentNode.parentNode.parentNode.id === "nav-utility") { linkType = "Header Nav Utility"; }
   else if (t.id === "branding-logo-link") { linkType = "Hartford Logo - Header"; }
   else if (t.parentNode.parentNode.id === "nav-breadcrumb") { linkType = "Top Nav - Breadcrumb"; }
   else if (t.parentNode.parentNode.id === "share-links") { linkType = "AddThis Sharing"; }
   else if (t.parentNode.parentNode.parentNode.id === "nav-secondary") { linkType = "Left Nav"; }
   else if (t.parentNode.parentNode.parentNode.id === "social-networking") { linkType = "Footer - Social Networking"; }
   else if (t.parentNode.parentNode.parentNode.parentNode.id.indexOf("info-block") !== -1) { linkType = "Top Nav Dropdown - Link"; }
   else if (t.parentNode.parentNode.id === "footer-primary-list") { linkType = "Primary Footer"; }
   else if (t.parentNode.parentNode.id.indexOf("footer-primary-list-") !== -1) { linkType = "Secondary Footer"; }
   else if (t.parentNode.parentNode.id === "popup-message-content") { linkType = "Lightbox Link"; }
   else { linkType = "Content";}
  
	 getLinkType(t);
	 //alert(linkType);

  /* if (linkTargetType == "External") { if(linkObj.target) { linkTarget = linkObj.target; } }
   else if (linkTargetType == "Unknown" && linkType == "Sub Tab") { linkTargetType = "Internal"; }
   if (linkTarget == "") { linkTarget = "_self"; } */

	/*eps = eps + "&" + encodeURIComponent('P1') + "=" + encodeURIComponent(t.parentNode.id);
	eps = eps + "&" + encodeURIComponent('P2') + "=" + encodeURIComponent(t.parentNode.parentNode.id);
	eps = eps + "&" + encodeURIComponent('P3') + "=" + encodeURIComponent(t.parentNode.parentNode.parentNode.id);
	eps = eps + "&" + encodeURIComponent('P4') + "=" + encodeURIComponent(t.parentNode.parentNode.parentNode.parentNode.id);
	eps = eps + "&" + encodeURIComponent('P5') + "=" + encodeURIComponent(t.parentNode.parentNode.parentNode.parentNode.parentNode.id);
	eps = eps + "&" + encodeURIComponent('P6') + "=" + encodeURIComponent(t.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.id);
  eps = eps + "&" + encodeURIComponent('P7') + "=" + encodeURIComponent(t.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.id);
	eps = eps + "&" + encodeURIComponent('P8') + "=" + encodeURIComponent(t.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.id);
	eps = eps + "&" + encodeURIComponent('P9') + "=" + encodeURIComponent(t.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.id);*/
	eps = eps + "&" + encodeURIComponent('lpos') + "=" + encodeURIComponent(linkType);

	return eps;
}
}

function getLinkType(t) {
	try {		
   var crossBrowser = {
    getAttr: function(ele, attr) {
        var result = (ele.getAttribute && ele.getAttribute(attr)) || null;
        if( !result ) {
            var attrs = ele.attributes;
            var length = attrs.length;
            for(var i = 0; i < length; i++)
                if(attrs[i].nodeName === attr)
                    result = attrs[i].nodeValue;
        }
        return result;
    }
};

		function getH2(ch) {
		var children = ch.childNodes;
	for (var i=0; i < children.length; i++) {
		if (children[i].tagName == 'H2') {
			return children[i].innerHTML;
			break;
		}
	}
}

   	function getUtilNavLink() {
   	var n = t.parentNode;
   	while (n.nodeName !== document.body) {			
   		if (n.id.indexOf("utility-links-list-item") !== -1) {
   		break;
   	  }
   	n = n.parentNode;
    }
  	return n.childNodes[0].getAttribute('title');
  	}
  
		function getNavLink() {
   	var n = t.parentNode;
   	while (n.nodeName !== document.body) {
   		if (n.className === "sub-nav") {
   		break;
   	  }
   	n = n.parentNode;
    }
    //var el = n.childNodes[1].childNodes[0];
    //var result = crossBrowser.getAttr(el, 'title');*/
  	if (n.childNodes[1].childNodes[0].getAttribute('title')) {
  	return n.childNodes[1].childNodes[0].getAttribute('title');
		} else {
		return n.childNodes[0].childNodes[0].getAttribute('title');
		}
	}
		function getNavTitle() {
   	var n = t.parentNode;
   	while (n.nodeName !== document.body) {
   		   			   			
   		if (n.className === "info-block block-type header-color") {
   		break;
   	}
   	n = n.parentNode;
  }
  return n.childNodes[0].childNodes[0].childNodes[0].innerHTML;
}
		var v = t.parentNode;
		
		while (v.nodeName !== document.body) {
			if (v.className === "utility-links-dropdown") 
				{ var getUtilLink = getUtilNavLink();
					linkType = "Header Nav Utility Dropdown - List Item for " + getUtilLink;
          break;
				}
        if (v.id === "account-access-dropdown") 
        { linkType = "Account Access";
          break;
				}
				if (v.id.indexOf("nav-primary-list-item") !== -1) 
        { linkType = "MegaNav Dropdown - Menu Option";
          break;
				}
				if (v.className === "sub-nav-list") 
        { var getLink = getNavLink();
        	linkType = "MegaNav Dropdown - List Item for " + getLink;
          break;
				}
				if (v.className === "toolbox") 
        { linkType = "Right Rail";
          break;
				}
				if (v.className === "nav-primary-dropdown") 
        { var getTitle = getNavTitle();
        	linkType = "MegaNav Dropdown - List Item for " + getTitle;
          break;
				}
				if (v.className === "sub-nav-title") 
        { linkType = "MegaNav Dropdown - List Header";
          break;
				}
				if (v.className === "carousel-content-overlay") 
        {  var hasH2 = getH2(v);
        	//linkType = 'Marquee - ' + (v.childNodes[1].innerHTML).replace(/<[^>]*>/g, "").replace(/&nbsp;/g," ");
        	linkType = 'Marquee - ' + hasH2.replace(/<[^>]*>/g, "").replace(/&nbsp;/g," ");
          break;
				}
				if (v.className === "share-links") 
        { linkType = "AddThis Sharing";
          break;
				}
				if (v.id === "footer-functional") 
        { linkType = "Functional Footer";
          break;
				}
				
		v = v.parentNode;
		}

return linkType;
}
catch(e)
{}
}

//  ---------------------------------------

function getLrank(t) {
	var lrankFound = false;
	searchForLrank(t.name);
	
	function searchForLrank(p) {
	var s_arr = [];
	var params = p.split(/&(?=[a-zA-Z0-9]+=)/);

	if ((params) && (lrankFound != true)) {
		params.shift();
		var l = params.length;
		for(var i = 0; i < l; i++) {
			s_arr = params[i].split('=');
			if (s_arr[0] === 'lrank') {
			lrankFound = true;
			eps = eps + "&" + encodeURIComponent(s_arr[0]) + "=" + encodeURIComponent(s_arr[1]);
			return eps;
		}
		}
   }
	}
}

//Load Tealium base code synchronously

document.write('<scr' + 'ipt type="text/javascript" src="../../../../tags.tiqcdn.com/utag/thehartford/main/prod/utag.js"/*tpa=http://tags.tiqcdn.com/utag/thehartford/main/prod/utag.js*/><\/sc' + 'ript>');

/* Event Capture */
function eventCapture(et, ep, eid, eurl) {

	_vo = '';	
			if ( typeof v !== "undefined") {
					for(vKey in v) {
							_vo = _vo + "&" + "v_" + vKey + "=" + encodeURIComponent(v[vKey]);
						}
					}
					
	// split ep parameters and build string
	epSplitParams(ep,'&');

	// remove all added name / value pairs on ep
	ep = ep.replace(/&.*/, "");

	var ea = '';
	eb = '';
	if(eid !== '') {
			eid = eid.replace(/(<([^>]+)>)/ig, "");   //Removes HTML Characters
			eid = eid.replace(/^\s+|\s+$/g, "");      //Removes global tabs
			ea = "&ei=" + encodeURIComponent(eid);
		}
	if(eurl !== '') {
		eb = "&eu=" + encodeURIComponent(eurl);
	}
	utag.link(
	{
		"call_type":"capture_event",
		//"call_protocol":cp, 
		//"call_domain":cd,
		//"call_directory":cu,
		"event_type":et,
		"event_parent":ep,
		"qs_ea":ea,
		"qs_eb":eb,
		"qs_do":_do,
		"qs_vo":_vo,
		"qs_eps":eps
	});
			
	eps = "";
}


//  ---------------------------------------
// Event Handler Object Detection
function addEvent(obj, type, fn) {
	if (obj) {
		if(obj.addEventListener) {
			obj.addEventListener(type, fn, false);
		} else if(obj.attachEvent) {
			obj.attachEvent('on' + type, function() {
				return fn.apply(obj, [window.event]);
			});
		}
	}
}

// Use this to add an onload event: addEvent(window, 'load', function(){fctn(arg1,arg2)});
//  ---------------------------------------
/*                iTag                   */
//  ---------------------------------------
function getiTag(evt) {
	var iArray = [];
	var evtName;
	var eValue;
	var evtParent;
	var eTrack;
	
	if (evt.attributes.itag) {
		iArray = evt.attributes.itag.nodeValue.split("|");
	}
	
	if (iArray[0] === 'No Track') {  //addme to revised
		eTrack = false;
	} else {
		eTrack = true;
	}
	
	if (iArray[1]) {
		evtParent = iArray[1];
	} else {
		evtParent = 'Page Tag';
	}

	if (iArray[2]) {
		evtName = iArray[2];
	} else if(evt.id) {//maybe check for target.title if necessary
		evtName = evt.id;
	} else {
		evtName = evt.name;
	}

	if(iArray[3]) {
		eValue = iArray[3];
	} else {
		eValue = evt.value;
	}

if(evt.type === 'checkbox') {  //use if mousedown
  	var evt_checked;
	if (evt.checked == true) {
				evt_checked = false;
			} else {
				evt_checked = true;
			}

	if (evt.type === 'checkbox') {
		if(iArray[3]) {
			eValue = iArray[3] + ' - (' + evt_checked + ')';
		} else {
			eValue = evt.value + ' - (' + evt_checked + ')';
		}
	}
	}

	return {
		evtParent : evtParent,
		evtName : evtName,
		eValue : eValue,
		eTrack : eTrack
	};
}
//  ---------------------------------------

//  ---------------------------------------
/*      Automatic Link Tracking          */
//  ---------------------------------------
// Add Event Handler; Parse, encode URI
function captureLink() {
	addEvent(window, 'load', function() {
		var anchor = document.body;
		addEvent(anchor, 'mousedown', function(e) {
		target = window.event ? window.event.srcElement : e ? e.target : null;
  
		function findParentNode(parentName, childObj) {
    while (childObj.nodeName.toLowerCase() !== parentName) {
        if (childObj.parentNode !== null && childObj.nodeName !== document.body) {
        childObj = childObj.parentNode;
      } else {
      return false;
    }
    }
    if (childObj !== null && childObj.nodeName.toLowerCase() === parentName) {
    	target = childObj;
    	return true;
    }
 }

var parent = findParentNode('a', target);

if (parent === true) {

				if (target.title) {
				var linkTitle = target.title;
				} else {
				var linkTitle = target.innerHTML.replace(/^[ \s\t]+/, "").replace(/\n/, "").replace(/\s{2}/g, " ").replace(/amp;/g, "");
				}

				if (linkTitle.indexOf("<img") !== -1 || linkTitle.indexOf("<IMG") !== -1) {

				linkTitle = linkTitle.match(/<(img|IMG)[^>]*>/g);
				var tempDiv = document.createElement('div');
				tempDiv.innerHTML = linkTitle;
				linkTitle = tempDiv.childNodes[0].attributes["alt"].nodeValue;
				
				}
				linkTitle = linkTitle.replace(/<[^>]*>/g, "").replace(/&nbsp;/g," ");
				
				var itag = getiTag(target);
				if(target.attributes.itag) {
					iArray = target.attributes.itag.nodeValue.split("|");
				}
				/*//Determine if link is a javascript form function call
				  //if(targetText.indexOf(".submit()") !== -1) {
					var getAction = target.parentNode.action;
					//iterate through hidden form values in future release
					eventCapture('JS Form Submit Link Click', 'Page Tag', linkTitle, getAction);
					} else {
					if ( this.className ) {  //use for className if exists
					hRef = this.className;
				} else { */
					var hRef = target.href;
				  getLpos(target);
				  //getLrank(target);
					//use for className if exists
					if(target.attributes.itag && iArray[2]) {
						linkTitle = iArray[2];
					}
					// Special situation for overlays
					var overlayActive = getByClass('ai_overlayActive', 'div');
					if(overlayActive.length) {
						var overlayActiveID = overlayActive[0].id;
						eventCapture('Link Click', 'Page Tag&flView=' + overlayActiveID, linkTitle, hRef); //take off capitalize due to lowercase flviews

					} else {
						eventCapture('Link Click', 'Page Tag', linkTitle, hRef);
					}

				if(target.attributes.itag) {
					if(iArray[0] === 'Page View') {
						eventCapture('Page View', iArray[1], '', '');
					}
					if(iArray[4]) {
						captureVideo(iArray[4]);
					}
				}
	/*	if (this.href) {  //must set as onclick event, not mousedown to use
		e.preventDefault ? e.preventDefault() : e.returnValue = false;
		setTimeout(function() { 
  	window.location = hRef}, 350);
 } **/
		}
			});
		//}
	});
}

if(vlc === "1") {
	captureLink();

}

//Custom eventCaptures for HIG

/* Search Box
if (document.getElementById("search-form")) {
        document.getElementById("search-submit").onmousedown = getText;
    }

function getText(){
	var searchText = document.getElementById("search-text").value;
	if ((searchText !== 'Search') && (searchText !== '')) {
    eventCapture('Internal Search','Search Box', searchText,'');
}
}

//add empty and Seach validation like qa and prod
if (document.getElementById("search-text")) {
        document.getElementById("search-text").onkeydown = getTextEnter;
    }

function getTextEnter(e){
try {
var evt = e || event;
if (evt.keyCode == 13) {
    var searchText = document.getElementById("search-text").value;
    if ((searchText !== 'Search') && (searchText !== '')) {
    eventCapture('Internal Search','Search Box', searchText,'');
   }
	}
    } catch (err) {}
}

*/

//Get A Quote

function isValidZipCode(value) {
   var re = /^\d{5}([\-]\d{4})?$/;
   return (re.test(value));
}

if (document.getElementById("header-hig-getaquote-main")) {
        document.getElementById("header-hig-getaquote-main").onmousedown = getOptionText;
    }

function getOptionText(){
    var optionText = document.getElementById("header-hig-getaquote-main").lob.options[document.getElementById("header-hig-getaquote-main").lob.selectedIndex].text;
    if (isValidZipCode(document.getElementById("header-hig-getaquote-main").zip.value)) {
    eventCapture('Get a Quote','Get a Quote&lpos=Header', optionText,'')
  }
}

if (document.getElementById('toolbox-hig-getaquote-main')){
        document.getElementById('toolbox-hig-getaquote-main').onmousedown  = getOptionText2;
    }

function getOptionText2(){
	var optionText = document.getElementById('toolbox-hig-getaquote-main').lob.options[document.getElementById('toolbox-hig-getaquote-main').lob.selectedIndex].text;
    if (isValidZipCode(document.getElementById("toolbox-hig-getaquote-main").zip.value)) {
    eventCapture('Get a Quote','Get a Quote&lpos=Right Rail', optionText,'')
  }
}

if (document.getElementById('marquee-hig-getaquote-main')){
        document.getElementById('marquee-hig-getaquote-main').onmousedown  = getOptionText3;
    }

function getOptionText3(){
	var optionText = document.getElementById('marquee-hig-getaquote-main').lob.options[document.getElementById('marquee-hig-getaquote-main').lob.selectedIndex].text;
    if (isValidZipCode(document.getElementById("marquee-hig-getaquote-main").zip.value)) {
    	getLinkType(document.getElementById('marquee-hig-getaquote-main'));
    eventCapture('Get a Quote','Get a Quote&lpos=Marquee', optionText,'')
  }
}
if (document.getElementById('body-hig-getaquote-main')){
        document.getElementById('body-hig-getaquote-main').onmousedown = getOptionText4;
    }

function getOptionText4(){
	var optionText = document.getElementById('body-hig-getaquote-main').lob.options[document.getElementById('body-hig-getaquote-main').lob.selectedIndex].text;
    if (isValidZipCode(document.getElementById("body-hig-getaquote-main").zip.value)) {
    eventCapture('Get a Quote','Get a Quote&lpos=Content', optionText,'')
  }
}

//Feedback
addEvent(window, 'load', function(e) {
		var input = document.body;
				addEvent(input, 'mousedown', function(e) {
					var target = window.event ? window.event.srcElement : e ? e.target : null;
					if(target.parentNode.id === 'oo_tab') {
				getOptionText5();}
				});
		});
		
//if (document.getElementById('oo_tab')){
//        document.getElementById('oo_tab').onmousedown = getOptionText5;
//    }

function getOptionText5(){
eventCapture('HIG Feedback','HIG Feedback&lpos=Right Edge', 'HIG Feedback','');
}

//Bazaar Voice Custom Tracking
addEvent(window, 'load', function() {
			var input = document.body;
     	    	
  addEvent(input, 'mousedown', function(e) {
			var target = window.event ? window.event.srcElement : e ? e.target : null;
			if ($(target).parent().children(":first").attr('type') == 'checkbox' ) {  //remember to add other input types

			captureInputFieldValBV($(target).parent().children(":first"));
				}
				});

});

function captureInputFieldValBV(evt) {
	var target = window.event ? window.event.srcElement : evt ? evt.target : null;
	var itag = getiTag(target);
	
	var is_checked = '';
	if (evt.attr('checked') == false) {
	is_checked = 'true' }
	else {
	is_checked = 'false'}
	;
	var overlayActive = getByClass('ai_overlayActive', 'div');
	if(overlayActive.length) {
		var overlayActiveID = overlayActive[0].id;
		eventCapture('Input Field Click - (' + is_checked + ')', itag.evtParent + '&flView=' + overlayActiveID, itag.evtName, itag.eValue);
	} else {
		eventCapture('Input Field Click - (' + is_checked + ')', itag.evtParent + '&lpos=BV', itag.evtName, itag.eValue);
	}
	evt.cancelBubble = true;
}
//----------------------------------------------------------------

/* BEGIN ADDTHIS.COM CAPTURE PAGE TAG
function shareEventHandler(evt) {
    if (evt.type == 'addthis.menu.share') {
    	if (evt.data.service !== 'print') {
    		alert('Thank you for sharing this content on ' + evt.data.service);		
      }
  	var linkTitle;
		switch (evt.type) {
        case "http://ni.thehartford.com/corp/hig/prod/addthis.menu.open":
            linkTitle = evt.data.pane;
            break;
        case "addthis.menu.close":
            linkTitle = evt.data.pane;
            break;
        case "addthis.menu.share":
            linkTitle = evt.data.service;
            break;
        default:
            linkTitle = 'Unexpected event: ' + evt;
          }

      var overlayActive = getByClass('ai_overlayActive', 'div');
					if(overlayActive.length) {
						var overlayActiveID = overlayActive[0].id;
						eventCapture('Link Click', 'Page Tag&flView=' + overlayActiveID, linkTitle, document.location.href);
					} else {
						eventCapture('Link Click', 'Page Tag', linkTitle, document.location.href);
					}
	}
}

// Listen to addthis events
addEvent(window, 'load', function() {
	if (typeof addthis != 'undefined')  {
		document.getElementById("atic_facebook").onclick = null;
/*addthis.addEventListener('http://ni.thehartford.com/corp/hig/prod/addthis.menu.open', shareEventHandler);
addthis.addEventListener('addthis.menu.close', shareEventHandler);
addthis.addEventListener('addthis.menu.share', shareEventHandler);
}
});
//  ---------------------------------------
// END ADDTHIS.COM CAPTURE PAGE TAG */

// BEGIN HTML 5 VIDEO CAPTURE PAGE TAG
function captureVideo(title) {// insert captureVideo("video name") for event  (otherwise, fctn must proceed fctn call)
	document.addEventListener("DOMContentLoaded", init, false);
	//use if wish to capture video on page load
	init();

	function init() {
		var video = document.getElementById(title);
		addEvent(video, "play", videoPlay);
		addEvent(video, "pause", videoPause);
		addEvent(video, "ended", videoEnd);
	}

	function videoPlay() {
		trackEvent('Play', title);
	}

	function videoPause() {
		trackEvent('Pause', title);
	}

	function videoEnd() {
		trackEvent('Ended', title);
	}

	function trackEvent(action, title) {
		var overlayActive = getByClass('ai_overlayActive', 'div');
		if(overlayActive.length) {
			var overlayActiveID = overlayActive[0].id;
			eventCapture(action, 'Video Player&flView=' + overlayActiveID, title, '');
		} else {
			eventCapture(action, 'Video Player', title, '');
		}
	}

}

//  ---------------------------------------
// END HTML 5 VIDEO CAPTURE PAGE TAG
// BEGIN LIMELIGHT VIDEO CAPTURE PAGE TAG
var title, position, duration, played, play_state, interval, prevPos, prevTitle, prevVidTitle, endState;
played = false;
endState = false;
interval = 0;

function delvePlayerCallback(playerId, eventName, data) {
    var id = "limelight_player_950030";
    if (eventName == 'onPlayerLoad' && (DelvePlayer.getPlayers() == null || DelvePlayer.getPlayers().length == 0)) {
        DelvePlayer.registerPlayer(id);
    }

    switch (eventName) {
    case 'onPlayerLoad':
        doOnPlayerLoad();
        break;

    case 'onMediaLoad':
        doOnMediaLoad(data);
        break;

    case 'onPlayStateChanged':
        doOnPlayStateChanged(data);
        break;

    case 'onPlayheadUpdate':
        doOnPlayheadUpdate(data);
        break;

    case 'onMediaComplete':
        onMediaComplete(data);
        break;
    }
}

function doOnPlayerLoad() {

}

function doOnMediaLoad(e) {
    title = e.title;
    duration = e.durationInMilliseconds;
}

function doOnPlayStateChanged(e) {

    var play_state;

    if (e.isBusy) {
        play_state = "buffering";
   				var overlayActive = getByClass('ai_overlayActive', 'div');
   				if(overlayActive.length) {
        	var overlayActiveID = overlayActive[0].id;
        	//eventCapture('Buffering', 'Limelight Video Player&flView=' + overlayActiveID, title, position + ' ms');
      		} else {
      		//eventCapture('Buffering', 'Limelight Video Player', title, position + ' ms');
      	}
   } else if (e.isPlaying) {
   	      play_state = "playing";
        	var overlayActive = getByClass('ai_overlayActive', 'div');
        	if(overlayActive.length) {
        			var overlayActiveID = overlayActive[0].id;
        		if (played === false) {
        			eventCapture('Play', 'Limelight Video Player&flView=' + overlayActiveID, title, position + ' ms');
        			played = true;
    	 		  } 	
    			} else {
    				if (prevVidTitle != title) {
    			  //if (played === false) {
    					eventCapture('Play', 'Limelight Video Player', title, position + ' ms');
    					//played = true;
    					prevVidTitle = title;
      			}
      		//}
    		  }
   } else {
        play_state = "paused";
        if (endState != true) {
            var overlayActive = getByClass('ai_overlayActive', 'div');
            if(overlayActive.length) {
            var overlayActiveID = overlayActive[0].id;
            eventCapture('Pause', 'Limelight Video Player&flView=' + overlayActiveID, title, position + ' ms');
        		} else {
        		eventCapture('Pause', 'Limelight Video Player', title, position + ' ms');
        	 }
        }
    }

    //eventCapture(play_state, 'Limelight Video Player&flView=' + overlayActiveID, e.title, e.positionInMilliseconds);
}

function doOnPlayheadUpdate(e) {
    position = e.positionInMilliseconds;
  	vidPos = Math.floor(position/1000);
  	posInterval = vidPos%15;

    if (prevTitle !== title) {
    	interval = 0;
    }
    
    if (position > 1450 && posInterval === 0 && vidPos != prevPos) {

    interval += 15;
    var overlayActive = getByClass('ai_overlayActive', 'div');
    if(overlayActive.length) {
    var overlayActiveID = overlayActive[0].id;
    eventCapture('Interval - '+ interval + ' sec', 'Limelight Video Player&flView=' + overlayActiveID, title, position + ' ms');
    } else {
    eventCapture('Interval - '+ interval + ' sec', 'Limelight Video Player', title, position + ' ms');
     }
    }
        prevPos = vidPos;
        prevTitle = title;
}

function onMediaComplete(e) {
    var overlayActive = getByClass('ai_overlayActive', 'div');
    if(overlayActive.length) {
    var overlayActiveID = overlayActive[0].id;
    eventCapture('Ended', 'Limelight Video Player&flView=' + overlayActiveID, title, position + ' ms');
		} else {
		eventCapture('Ended', 'Limelight Video Player', title, position + ' ms');	
		endState=true;
		}
}

//  ---------------------------------------
// BEGIN FORM CLICK CAPTURE PAGE TAG

function captureForm() {

	if(document.forms[0]) {
		addEvent(window, 'load', function() {
			var formsCollection = document.getElementsByTagName("form");
			for(var j = 0; j < formsCollection.length; j++) {
				for( i = 0; i < document.forms[j].elements.length; i++) {
					var forms = document.forms[j].elements[i];
					if(forms.type === 'text' || forms.type === 'textarea' || input.type === 'password' ) {
						addEvent(forms, 'blur', function(e) {
							captureInputFieldVal(e);

						});
					} else if(forms.type === 'select-one') {
						addEvent(forms, 'change', function(e) {
							captureInputFieldVal(e);

						});
					} else {
						addEvent(forms, 'click', function(e) {
							e.cancelBubble = true;
							captureInputFieldVal(e);

						});
					}
					// captureFormName( e ); use if want only form name, value and not et, ep, etc.
				}
			}
		});
	}
}

if(vfc === "1") {
	captureForm();
}

//  ---------------------------------------
function captureFormName(evt) {
	var target = window.event ? window.event.srcElement : evt ? evt.target : null;
	var itag = getiTag(target);
	var overlayActive = getByClass('ai_overlayActive', 'div');
	if(overlayActive.length) {
		var overlayActiveID = overlayActive[0].id;
		eventCapture('Form Click', itag.evtParent, itag.evtName, itag.eValue);
	} else {
		eventCapture('Form Click', itag.evtParent + '&flView=' + overlayActiveID, itag.evtName, itag.eValue);
	}

}

//  ---------------------------------------
// END FORM CLICK CAPTURE PAGE TAG

// BEGIN INPUT FIELD CLICK CAPTURE PAGE TAG

function captureInputField() {  //must use loop since the onchange event does not bubble up to the body for ie, will not work lateloading content, though
	//addEvent(window, 'load', function() {
		var inputCollection = document.getElementsByTagName("input");
		for(var j = 0; j < inputCollection.length; j++) {
			var input = inputCollection[j];
			if(input.type === 'text' || input.type === 'textarea') {
				addEvent(input, 'change', function(e) { //comment out if fields should not be captured
					captureInputFieldVal(e);			
				});
			}
			}
	//});
}

// BEGIN INPUT FIELD CLICK CAPTURE PAGE TAG

/*function captureInputField() { //works for lateloading content, but onchange won't bubble up for ie
	//addEvent(window, 'load', function() {	

	var input = document.body;
	addEvent(input, 'change', function(e) {  //does not bubble in ie
     var target = window.event ? window.event.srcElement : e ? e.target : null;
     //e.cancelBubble=true;
     if (target.type === 'text') {
     	captureInputFieldVal(e);
     	}
   // });
    
	/*addEvent(input, 'blur', function(e) { //does not bubble in ie
     var target = window.event ? window.event.srcElement : e ? e.target : null;
     if (target.type === 'text') {
     	//captureInputFieldVal(e);
     	}
    });
    
    addEvent(input, 'keypress', function(e) {
     var target = window.event ? window.event.srcElement : e ? e.target : null;
     if (e.keyCode == 13) {
     	captureInputFieldVal(e);
     	}
    });
    
});
}*/

function captureInputElements() {
	addEvent(window, 'load', function() {
			var input = document.body;
     	    	
  addEvent(input, 'mousedown', function(e) {
			var target = window.event ? window.event.srcElement : e ? e.target : null;
			if (target.type === 'button' || target.type === 'submit' || target.type === 'radio' || target.type === 'checkbox' || target.type === 'image') {  //remember to add other input types
			captureInputFieldVal(e);
				}
				});

});
}

addEvent(window, 'load', function() {
if(vic == "1") {
	captureInputField();
}});

if(vie == "1") {
	captureInputElements();
}

//  ---------------------------------------
// END INPUT FIELD CLICK CAPTURE PAGE TAG

// BEGIN BUTTON CLICK CAPTURE PAGE TAG

function captureButton() {
	addEvent(window, 'load', function(e) {
		var input = document.body;
		addEvent(input, 'mousedown', function(e) {
			var target = window.event ? window.event.srcElement : e ? e.target : null;
			if(target.type === 'button' || target.type === 'reset') {
				captureButtonFieldVal(e);}

				});		
		});
	}


if(vbc === "1") {
	captureButton();
}

//  ---------------------------------------
// END BUTTON CLICK CAPTURE PAGE TAG

// BEGIN TEXTAREA CLICK CAPTURE PAGE TAG

function captureTextarea() {
	addEvent(window, 'load', function(e) {
		var input = document.body;
				addEvent(input, 'blur', function(e) {
					var target = window.event ? window.event.srcElement : e ? e.target : null;
					if(target.type === 'textarea') {
				captureTextareaFieldVal(e);}

				});
		});
	}


if(vtac === "1") {
	captureTextarea();
}

//  ---------------------------------------
// END TEXTAREA CLICK CAPTURE PAGE TAG
// BEGIN SELECT CLICK CAPTURE PAGE TAG

function captureSelect() {
	/*addEvent(window, 'load', function(e) {  //can't use for ie - wont bubble onchange
		var input = document.body;
				addEvent(input, 'change', function(e) {
					var target = window.event ? window.event.srcElement : e ? e.target : null;
					if(target.type === 'select-one') {
						//alert(target.type);
				captureSelectFieldVal(e);}

				});
		});*/
		
addEvent(window, 'load', function() {
		var SelectCollection = document.getElementsByTagName("select");
		for(var j = 0; j < SelectCollection.length; j++) {
			if(SelectCollection[j].type === 'select-one') {
				addEvent(SelectCollection[j], 'change', function(e) {
					captureSelectFieldVal(e);

				});
			}
		}
	});
	}

if(vsc === "1") {
	captureSelect();
}

//  ---------------------------------------
// END SELECT CLICK CAPTURE PAGE TAG

// BEGIN INPUT FIELD CLICK CAPTURE VALUES
function captureInputFieldVal(evt) {
	var target = window.event ? window.event.srcElement : evt ? evt.target : null;
	var itag = getiTag(target);
	var overlayActive = getByClass('ai_overlayActive', 'div');
	if(overlayActive.length) {
		var overlayActiveID = overlayActive[0].id;
		eventCapture('Input Field Click - (' + target.type + ')', itag.evtParent + '&flView=' + overlayActiveID, itag.evtName, itag.eValue);
	} else {
		eventCapture('Input Field Click - (' + target.type + ')', itag.evtParent, itag.evtName, itag.eValue);
	}
	evt.cancelBubble = true;
}

//  ---------------------------------------
// END INPUT FIELD CLICK CAPTURE VALUES

function captureButtonFieldVal(evt) {
	var target = window.event ? window.event.srcElement : evt ? evt.target : null;
	var itag = getiTag(target);
	var overlayActive = getByClass('ai_overlayActive', 'div');
	if(overlayActive.length) {
		var overlayActiveID = overlayActive[0].id;
		eventCapture('Button Click - (' + target.type + ')', itag.evtParent + '&flView=' + overlayActiveID, itag.evtName, itag.eValue);
	} else {
		eventCapture('Button Click - (' + target.type + ')', itag.evtParent, itag.evtName, itag.eValue);
	}

}

//  ---------------------------------------

function captureTextareaFieldVal(evt) {
	var target = window.event ? window.event.srcElement : evt ? evt.target : null;
	var itag = getiTag(target);
	var overlayActive = getByClass('ai_overlayActive', 'div');
	if(overlayActive.length) {
		var overlayActiveID = overlayActive[0].id;

		eventCapture('Textarea Click - (' + target.type + ')', itag.evtParent + '&flView=' + overlayActiveID, itag.evtName, itag.eValue);
	} else {
		eventCapture('Textarea Click - (' + target.type + ')', itag.evtParent, itag.evtName, itag.eValue);
	}
}

//  ---------------------------------------
// BEGIN SELECT CLICK CAPTURE VALUES
function captureSelectFieldVal(evt) {
	var target = window.event ? window.event.srcElement : evt ? evt.target : null;
	var itag = getiTag(target);
	var overlayActive = getByClass('ai_overlayActive', 'div');
	if(overlayActive.length) {
		var overlayActiveID = overlayActive[0].id;
		eventCapture('Select Click - (' + target.type + ')', itag.evtParent + '&flView=' + overlayActiveID, itag.evtName, itag.eValue);
	} else {
		eventCapture('Select Click - (' + target.type + ')', itag.evtParent, itag.evtName, itag.eValue);
	}
}

//  ---------------------------------------
// BEGIN IMAGE MAP CAPTURE PAGE TAG
function captureImageMap() {
	addEvent(window, 'load', function(e) {
		var input = document.body;
				addEvent(input, 'mousedown', function(e) {
					var target = window.event ? window.event.srcElement : e ? e.target : null;
					if(target.nodeName === 'AREA') {
				captureAreaVal(e);}

				});
		});
	}

if(vim === "1") {
	captureImageMap();
}

//  ---------------------------------------
// END IMAGE MAP EVENT CAPTURE PAGE TAG

// BEGIN SELECT CLICK CAPTURE VALUES
function captureAreaVal(evt) {
	var target = window.event ? window.event.srcElement : evt ? evt.target : null;
	var itag = getiTag(target);
	var overlayActive = getByClass('ai_overlayActive', 'div');
	if(overlayActive.length) {
		var overlayActiveID = overlayActive[0].id;
		eventCapture('Image Map Click - (' + target.nodeName + ')', itag.evtParent + '&flView=' + overlayActiveID, target.alt, target.href);
	} else {
		eventCapture('Image Map Click - (' + target.nodeName + ')', itag.evtParent, target.alt, target.href);
	}
}

//  ---------------------------------------

function CaptureCustomNameEvtFieldVal(evt) {
	var target = window.event ? window.event.srcElement : evt ? evt.target : null;
	var itag = getiTag(target);
	var overlayActive = getByClass('ai_overlayActive', 'div');
	if(overlayActive.length) {
		var overlayActiveID = overlayActive[0].id;
		eventCapture('Custom Event by Name - (' + target.type + ')', itag.evtParent + '&flView=' + overlayActiveID, itag.evtName, itag.value);
	} else {
		eventCapture('Custom Event by Name - (' + target.type + ')', itag.evtParent, itag.evtName, itag.value);
	}
}

//  ---------------------------------------
// BEGIN CUSTOM ID EVENT CAPTURE PAGE TAG

function captureCustomIdEvt() {
	addEvent(window, 'load', function() {
		var CustomIdEvt = document.getElementById(Id);

		addEvent(CustomIdEvt, Evt, function(e) {
			CaptureCustomIdEvtFieldVal(e);

		});
	});
}

if(vceic === "1") {
	captureCustomIdEvt(Id, Evt);
}

//  ---------------------------------------
// END CUSTOM ID EVENT CAPTURE PAGE TAG
function CaptureCustomIdEvtFieldVal(evt) {
	var target = window.event ? window.event.srcElement : evt ? evt.target : null;
	var itag = getiTag(target);
	var overlayActive = getByClass('ai_overlayActive', 'div');
	if(overlayActive.length) {
		var overlayActiveID = overlayActive[0].id;
		eventCapture('Custom Event by Id - (' + target.type + ')', itag.evtParent + '&flView=' + overlayActiveID, itag.evtName, itag.value);
	} else {
		eventCapture('Custom Event by Id - (' + target.type + ')', itag.evtParent, itag.evtName, itag.value);
	}
}

//  ---------------------------------------
// BEGIN CUSTOM NAME EVENT CAPTURE PAGE TAG

function captureCustomNameEvt() {
	addEvent(window, 'load', function() {
		var CustomNameEvt = document.getElementsByName(Name)[0];

		addEvent(CustomNameEvt, Evt, function(e) {
			CaptureCustomNameEvtFieldVal(e);

		});
	});
}

if(vcenc === "1") {
	captureCustomNameEvt(Name, Evt);
}

//  ---------------------------------------
// END CUSTOM NAME EVENT CAPTURE PAGE TAG
function CaptureCustomNameEvtFieldVal(evt) {
	var target = window.event ? window.event.srcElement : evt ? evt.target : null;
	var itag = getiTag(target);
	var overlayActive = getByClass('ai_overlayActive', 'div');
	if(overlayActive.length) {
		var overlayActiveID = overlayActive[0].id;
		eventCapture('Custom Event by Name - (' + target.type + ')', itag.evtParent + '&flView=' + overlayActiveID, itag.evtName, itag.value);
	} else {
		eventCapture('Custom Event by Name - (' + target.type + ')', itag.evtParent, itag.evtName, itag.value);
	}
}

//  ---------------------------------------

// BEGIN CUSTOM CLASS EVENT CAPTURE PAGE TAG

function captureCustomClassEvt() {
	addEvent(window, 'load ', function() {
		var CustomClassEvt = document.getElementsByclassName(Class)[0];

		addEvent(CustomClassEvt, Evt, function(e) {
			CaptureCustomClassEvtFieldVal(e);

		});
	});
}

if(vcecc === "1") {
	captureCustomClassEvt(Class, Evt);
}

//  ---------------------------------------
// END CUSTOM CLASS EVENT CAPTURE PAGE TAG
function CaptureCustomClassEvtFieldVal(evt) {
	var target = window.event ? window.event.srcElement : evt ? evt.target : null;
	var itag = getiTag(target);
	var overlayActive = getByClass('ai_overlayActive', 'div');
	if(overlayActive.length) {
		var overlayActiveID = overlayActive[0].id;
		eventCapture('Custom Event by Class - (' + target.type + ')', itag.evtParent + '&flView=' + overlayActiveID, itag.evtName, itag.eValue);
	} else {
		eventCapture('Custom Event by Class - (' + target.type + ')', itag.evtParent, itag.evtName, itag.eValue);
	}
}

//  ---------------------------------------

addEvent(window, 'unload', function() {});