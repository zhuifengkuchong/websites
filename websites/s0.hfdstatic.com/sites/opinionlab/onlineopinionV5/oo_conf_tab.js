/*
OnlineOpinion v5.5.7
Released: 6/25/2012. Compiled 06/25/2012 11:51:23 AM -0500
Branch: master 443704ff871eb8951a618006ceef1f6a5d8715d8
Components: Full
The following code is Copyright 1998-2012 Opinionlab, Inc.  All rights reserved. Unauthorized use is prohibited. This product and other products of OpinionLab, Inc. are protected by U.S. Patent No. 6606581, 6421724, 6785717 B1 and other patents pending. http://www.opinionlab
*/

function getMetaContents(mn){
  var m = document.getElementsByTagName('meta');
  for (var i in m) {
    if (m[i].name == mn) {
      return m[i].content;
    }
  }
}

/* [+] Tab Icon configuration */
  var oo_tab = new OOo.Ocode({
	  tab: {}
	, onPageCard: {
		  closeWithOverlay: {}
	  }
	, customVariables: {
		  HIGtitle: getMetaContents('HIG.Title') || ''
		, HIGsite: getMetaContents('http://s0.hfdstatic.com/sites/opinionlab/onlineopinionV5/HIG.Site') || ''
	  }
  });
  
/*
Add the following style block after the inclusion of oo_style.css to adjust the tab border color:
<style type="text/css">
#oo_tab {
  border-color: #252525;
}
</style>
*/