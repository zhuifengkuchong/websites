$(function(){
    $('.link-section-2-last,.link-section-3-last').after('<div class="clear-both">&nbsp;</div>');
    $('.button-type-video').prepend('<span class="button-type-video-before"></span>');
    $('#nav-primary-list > li:last-child').addClass('last');
    $('.hig-quick-form input[type=text]').each(function(){
        $(this).addClass('input-type-text');
    });
    $('.content-wrapper-inner p:last-child ').addClass('last-child');
    
    $('.icon-arrow-right, .link-section-more, .utility-links-dropdown .list-links-2 > li > a').css({'paddingRight':'0','background-image':'none'}).after('<span class="span-arrow-right">&nbsp;</span>');

    $('#search-submit').attr('src','../imgs/search-submit.png'/*tpa=http://s0.hfdstatic.com/sites/build_4/imgs/search-submit.png*/).show();
});