/**
* hoverIntent r6 // 2011.02.26 // jQuery 1.5.1+
* <http://cherne.net/brian/resources/jquery.hoverIntent.html>
*
* @param  f  onMouseOver function || An object with configuration options
* @param  g  onMouseOut function  || Nothing (use configuration options object)
* @author    Brian Cherne brian(at)cherne(dot)net
*/
(function($){$.fn.hoverIntent=function(f,g){var cfg={sensitivity:7,interval:100,timeout:0};cfg=$.extend(cfg,g?{over:f,out:g}:f);var cX,cY,pX,pY;var track=function(ev){cX=ev.pageX;cY=ev.pageY};var compare=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);if((Math.abs(pX-cX)+Math.abs(pY-cY))<cfg.sensitivity){$(ob).unbind("mousemove",track);ob.hoverIntent_s=1;return cfg.over.apply(ob,[ev])}else{pX=cX;pY=cY;ob.hoverIntent_t=setTimeout(function(){compare(ev,ob)},cfg.interval)}};var delay=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);ob.hoverIntent_s=0;return cfg.out.apply(ob,[ev])};var handleHover=function(e){var ev=jQuery.extend({},e);var ob=this;if(ob.hoverIntent_t){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t)}if(e.type=="mouseenter"){pX=ev.pageX;pY=ev.pageY;$(ob).bind("mousemove",track);if(ob.hoverIntent_s!=1){ob.hoverIntent_t=setTimeout(function(){compare(ev,ob)},cfg.interval)}}else{$(ob).unbind("mousemove",track);if(ob.hoverIntent_s==1){ob.hoverIntent_t=setTimeout(function(){delay(ev,ob)},cfg.timeout)}}};return this.bind('mouseenter',handleHover).bind('mouseleave',handleHover)}})(jQuery);

/*
 * Lazy Load - jQuery plugin for lazy loading images
 *
 * Copyright (c) 2007-2012 Mika Tuupola
 *
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 *
 * Project home:
 *   http://www.appelsiini.net/projects/lazyload
 *
 * Version:  1.7.0
 *
 */
(function(a,b){$window=a(b),a.fn.lazyload=function(c){var d={threshold:0,failure_limit:0,event:"scroll",effect:"show",container:b,data_attribute:"original",skip_invisible:!0,appear:null,load:null};c&&(undefined!==c.failurelimit&&(c.failure_limit=c.failurelimit,delete c.failurelimit),undefined!==c.effectspeed&&(c.effect_speed=c.effectspeed,delete c.effectspeed),a.extend(d,c));var e=this;return 0==d.event.indexOf("scroll")&&a(d.container).bind(d.event,function(b){var c=0;e.each(function(){$this=a(this);if(d.skip_invisible&&!$this.is(":visible"))return;if(!a.abovethetop(this,d)&&!a.leftofbegin(this,d))if(!a.belowthefold(this,d)&&!a.rightoffold(this,d))$this.trigger("appear");else if(++c>d.failure_limit)return!1})}),this.each(function(){var b=this,c=a(b);b.loaded=!1,c.one("appear",function(){if(!this.loaded){if(d.appear){var f=e.length;d.appear.call(b,f,d)}a("<img />").bind("load",function(){c.hide().attr("src",c.data(d.data_attribute))[d.effect](d.effect_speed),b.loaded=!0;var f=a.grep(e,function(a){return!a.loaded});e=a(f);if(d.load){var g=e.length;d.load.call(b,g,d)}}).attr("src",c.data(d.data_attribute))}}),0!=d.event.indexOf("scroll")&&c.bind(d.event,function(a){b.loaded||c.trigger("appear")})}),$window.bind("resize",function(b){a(d.container).trigger(d.event)}),a(d.container).trigger(d.event),this},a.belowthefold=function(c,d){if(d.container===undefined||d.container===b)var e=$window.height()+$window.scrollTop();else var e=a(d.container).offset().top+a(d.container).height();return e<=a(c).offset().top-d.threshold},a.rightoffold=function(c,d){if(d.container===undefined||d.container===b)var e=$window.width()+$window.scrollLeft();else var e=a(d.container).offset().left+a(d.container).width();return e<=a(c).offset().left-d.threshold},a.abovethetop=function(c,d){if(d.container===undefined||d.container===b)var e=$window.scrollTop();else var e=a(d.container).offset().top;return e>=a(c).offset().top+d.threshold+a(c).height()},a.leftofbegin=function(c,d){if(d.container===undefined||d.container===b)var e=$window.scrollLeft();else var e=a(d.container).offset().left;return e>=a(c).offset().left+d.threshold+a(c).width()},a.inviewport=function(b,c){return!a.rightofscreen(b,c)&&!a.leftofscreen(b,c)&&!a.belowthefold(b,c)&&!a.abovethetop(b,c)},a.extend(a.expr[":"],{"below-the-fold":function(c){return a.belowthefold(c,{threshold:0,container:b})},"above-the-top":function(c){return!a.belowthefold(c,{threshold:0,container:b})},"right-of-screen":function(c){return a.rightoffold(c,{threshold:0,container:b})},"left-of-screen":function(c){return!a.rightoffold(c,{threshold:0,container:b})},"in-viewport":function(c){return!a.inviewport(c,{threshold:0,container:b})},"above-the-fold":function(c){return!a.belowthefold(c,{threshold:0,container:b})},"right-of-fold":function(c){return a.rightoffold(c,{threshold:0,container:b})},"left-of-fold":function(c){return!a.rightoffold(c,{threshold:0,container:b})}})})(jQuery,window);


$(function(){
    $("http://s0.hfdstatic.com/sites/build_4/js/img.lazy").show().lazyload({
            effect : "fadeIn",
            event : "delay1"
    });
});

addLoadEvent(function(){
    var timeout = setTimeout(function() {$("http://s0.hfdstatic.com/sites/build_4/js/img.lazy").trigger("delay1")}, 100);
});


/*!
* jQuery Cookie Plugin
* https://github.com/carhartl/jquery-cookie
*
* Copyright 2011, Klaus Hartl
* Dual licensed under the MIT or GPL Version 2 licenses.
* http://www.opensource.org/licenses/mit-license.php
* http://www.opensource.org/licenses/GPL-2.0
*/
(function($) {
    $.cookie = function(key, value, options) {

        // key and at least value given, set cookie...
        if (arguments.length > 1 && (!/Object/.test(Object.prototype.toString.call(value)) || value === null || value === undefined)) {
            options = $.extend({}, options);

            if (value === null || value === undefined) {
                options.expires = -1;
            }

            if (typeof options.expires === 'number') {
                var days = options.expires, t = options.expires = new Date();
                t.setDate(t.getDate() + days);
            }

            value = String(value);

            return (document.cookie = [
                encodeURIComponent(key), '=', options.raw ? value : encodeURIComponent(value),
                options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
                options.path ? '; path=' + options.path : '',
                options.domain ? '; domain=' + options.domain : '',
                options.secure ? '; secure' : ''
            ].join(''));
        }

        // key and possibly options given, get cookie...
        options = value || {};
        var decode = options.raw ? function(s) { return s; } : decodeURIComponent;

        var pairs = document.cookie.split('; ');
        for (var i = 0, pair; pair = pairs[i] && pairs[i].split('='); i++) {
            if (decode(pair[0]) === key) return decode(pair[1] || ''); // IE saves cookies with empty string as "c; ", e.g. without "=" as opposed to EOMB, thus pair[1] may be undefined
        }
        return null;
    };
})(jQuery);



//popup over grey screen -- begin code

        function ShowPopup(popupMessageContent) {
						try{eventCapture("Page View","Page Tag&flView=overlay_"+popupMessageContent,"","");}catch(err){}
            $('body').append('<div id="grey-screen">&nbsp;</div><div id="popup-container"><div id="overlay_'+popupMessageContent+'" class="ai_overlayActive"><div id="popup-message"><div id="close-popup"><a id="close-popup-link" class="close-popup-link" href="#" ><span>Close</span></a></div></div></div></div>');

            $('#grey-screen').css({ 'opacity': 0.7, 'width':getPageSize()[0],'height':getPageSize()[1]});
            if(ieVersion==6){
                $('embed, object, select').css({ 'visibility' : 'hidden' });
            }
            $('#popup-message').append($('#'+popupMessageContent+' > *'));

            $('#popup-container').css({'top':getPageScroll()[1] + (getPageSize()[3] / 10)-50,'left':getPageScroll()[0]}).show().animate({"top": "+=50"}, 400);

            $(window).resize(function() {
                $('#grey-screen').css({'width':getPageSize()[0],'height':getPageSize()[1]});
                $('#popup-container').css({'top':getPageScroll()[1] + (getPageSize()[3] / 10),'left':getPageScroll()[0]});
            });
            $('.close-popup-link').click( function() {ClearPopMsg(popupMessageContent);return false;});
            document.getElementById('close-popup-link').focus();
            return false;
        }

         function ClearPopMsg(whichContent){
                $('#'+whichContent).append($('#popup-message > *:not(#close-popup)'));
                $('#popup-container').remove();
                $('#grey-screen').fadeOut(function() { $('#grey-screen').remove(); });
                $('embed, object, select').css({ 'visibility' : 'visible' });
        }


         //getPageSize() by quirksmode.com
        function getPageSize() {
            var xScroll, yScroll;
            if (window.innerHeight && window.scrollMaxY) {
                xScroll = window.innerWidth + window.scrollMaxX;
                yScroll = window.innerHeight + window.scrollMaxY;
            } else if (document.body.scrollHeight > document.body.offsetHeight){ // all but Explorer Mac
                xScroll = document.body.scrollWidth;
                yScroll = document.body.scrollHeight;
            } else { // Explorer Mac...would also work in Explorer 6 Strict, Mozilla and Safari
                xScroll = document.body.offsetWidth;
                yScroll = document.body.offsetHeight;
            }
            var windowWidth, windowHeight;
            if (self.innerHeight) { // all except Explorer
                if(document.documentElement.clientWidth){
                    windowWidth = document.documentElement.clientWidth;
                } else {
                    windowWidth = self.innerWidth;
                }
                windowHeight = self.innerHeight;
            } else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
                windowWidth = document.documentElement.clientWidth;
                windowHeight = document.documentElement.clientHeight;
            } else if (document.body) { // other Explorers
                windowWidth = document.body.clientWidth;
                windowHeight = document.body.clientHeight;
            }
            // for small pages with total height less then height of the viewport
            if(yScroll < windowHeight){
                pageHeight = windowHeight;
            } else {
                pageHeight = yScroll;
            }
            // for small pages with total width less then width of the viewport
            if(xScroll < windowWidth){
                pageWidth = xScroll;
            } else {
                pageWidth = windowWidth;
            }
            arrayPageSize = new Array(pageWidth,pageHeight,windowWidth,windowHeight);
            return arrayPageSize;
        }

        //getPageScroll() by quirksmode.com
        function getPageScroll() {
            var xScroll, yScroll;
            if (self.pageYOffset) {
                yScroll = self.pageYOffset;
                xScroll = self.pageXOffset;
            } else if (document.documentElement && document.documentElement.scrollTop) {     // Explorer 6 Strict
                yScroll = document.documentElement.scrollTop;
                xScroll = document.documentElement.scrollLeft;
            } else if (document.body) {// all other Explorers
                yScroll = document.body.scrollTop;
                xScroll = document.body.scrollLeft;
            }
            arrayPageScroll = new Array(xScroll,yScroll);
            return arrayPageScroll;
        }
//popup over grey screen -- end code

//LightBox
var higanchor="";
function LightBoxDisplay(lbCID,lbURL) {
    try{eventCapture("Page View","Page Tag&flView=overlay_"+lbCID,"","");}catch(err){}
    if (ieVersion == '7'){
        $('body').append('<div id="grey-screen">&nbsp;</div><div id="popup-container"><div id="overlay_'+lbCID+'" class="ai_overlayActive"><table id="popup-message"><tbody><tr><td><div id="close-popup"><a id="close-popup-link" class="close-popup-link" href="#" ><span>Close</span></a></div><div id="popup-message-content"><img src="Unknown_83_filename"/*tpa=http://s0.hfdstatic.com/sites/hig_standard/build_3/imgs/ajax-loader.gif*/ style="margin:10px auto; display:block;" height="32" width="32" /></div></td></tr></tbody></table></div></div>');
    }
    else{
        $('body').append('<div id="grey-screen">&nbsp;</div><div id="popup-container"><div id="overlay_'+lbCID+'" class="ai_overlayActive"><div id="popup-message"><div id="close-popup"><a id="close-popup-link" class="close-popup-link" href="#" ><span>Close</span></a></div><div id="popup-message-content"><img src="Unknown_83_filename"/*tpa=http://s0.hfdstatic.com/sites/hig_standard/build_3/imgs/ajax-loader.gif*/ style="margin:10px auto; display:block;" height="32" width="32" /></div></div></div></div>');
    }

    $('#grey-screen').css({ 'opacity': 0.7, 'width':getPageSize()[0],'height':getPageSize()[1]});

    $('#popup-container').css({'top':getPageScroll()[1] + (getPageSize()[3] / 10)-50,'left':getPageScroll()[0]}).show().animate({"top": "+=50"}, 400);

    $.get(lbURL, function(data){

        //check if script is being linked to. If so, get src and do getScript on it and remove it from the HTML
        var scriptSrc;


        if(data.match(/<script src=.+<\/script>/)){
            scriptSrc = data.match(/<script src=.+<\/script>/);
            scriptSrc = scriptSrc.toString();
            var data1 = data.replace(/<script src=.+<\/script>/,"")
            scriptSrc = scriptSrc.replace("<script src=\"","");
            scriptSrc = scriptSrc.replace("\"><\/script>","");
            $.getScript(scriptSrc, function(data, textStatus, jqxhr) {
                $(data1).replaceAll('#popup-message-content img');
            });
        }
        else{
            $(data).replaceAll('#popup-message-content img');
        }

        document.getElementById('close-popup-link').focus();
        if(lbURL.indexOf("#")>=0){
            higanchor = lbURL.substring(lbURL.indexOf("#")+1)
            sethiganchorfocus();
        }
        if($("#popup-message-content h4.content-title").length > 0){
            $('#close-popup').after($("#popup-message-content h4.content-title"));
            $("#popup-message-content").css({'position':'relative'}); //ie7
        }
        if(ieVersion=='6'){
            var newWidth = $('#popup-message').width();
            $('#popup-message').css({ 'width':newWidth} );
        }

        if(ieVersion=='7'){
            var objWidth = $(this).find('object').width();
            if (objWidth > 0){
                $('#popup-message').css({ 'width':objWidth} );
            }
        }

    });

    $(window).resize(function() {
        $('#grey-screen').css({'width':getPageSize()[0],'height':getPageSize()[1]});
        $('#popup-container').css({'top':getPageScroll()[1] + (getPageSize()[3] / 10),'left':getPageScroll()[0]});
    });
    $('.close-popup-link').click( function() {ClearLightBox();return false;});
}

function sethiganchorfocus(){
        if(document.getElementById(higanchor)!=null){
            popupContainerTopOffset = $("#popup-container").offset().top;
            $('#'+higanchor).parents(".TabContentContainer .scroll-area").animate({scrollTop: $("#"+higanchor).offset().top - popupContainerTopOffset -100 },'slow');
        }
}

function ClearLightBox(){
        $('#popup-container').remove();
        $('#grey-screen').fadeOut(function() { $('#grey-screen').remove(); });
        $('embed, object, select').css({ 'visibility' : 'visible' });
}



/** code to execute on document ready **/
$(function(){

/* close lightbox/overlay on ESC key press*/
    $(document).keyup(function(e) {
        if (e.keyCode == 27) { // "Esc" Key
           if ( $('#popup-container').is(':visible') ) {
               $('.close-popup-link').click();
           }
        }
    });

/*for popups*/
    $('a[target^="jpopup"]').click(function(e){
    var href = $(this).attr('href');
    var arrTarget = $(this).attr('target').split("-");
    var theW="960";
    var theH="800";
    if(arrTarget[1]){
        theW=arrTarget[1];
    }
    if(arrTarget[2]){
        theH=arrTarget[2];
    }
    msgWindow=open(href,'New_Window','toolbar=1,scrollbars=1,location=1,statusbar=0,menubar=1,resizable, width=' + theW + ',height=' + theH + ', left=20, top=20');
    e.preventDefault();
    });


/* text sizer code */

    if($.cookie('TEXT_SIZE')) {
        var txtSize = $.cookie('TEXT_SIZE');
        $('body').addClass(txtSize);
        $('.text-size-option').removeClass('current-size');
        $('#'+txtSize+' > a').addClass('current-size');
    }
    $('.text-size-option').click(function() {
        $('.text-size-option').removeClass('current-size');
        $(this).addClass('current-size');
        var textSize = $(this).parent().attr('id');
        $('body').removeClass('text-size-default text-size-medium text-size-large').addClass(textSize);
        $.cookie('TEXT_SIZE',textSize, { path: '/', expires: 30 });
        return false;
    });

//generic (used for utility bar and acct access and mega-nav)
//click and hover open up dropdown (both desktop and touch). No JS then go to page in href

    $('.no-touch .has-dropdown > a').bind('click', function(event){
        if(!$(this).parent().hasClass('hoverTest')){
            $(this).parent().siblings().removeClass('hover').children().not('a').fadeOut(100);
            $(this).parent().toggleClass('hover').children().not('a').fadeToggle(100);
            event.preventDefault();
        }
        if(!$(this).hasClass('follow-link')){
            event.preventDefault();
        }
    });

    $('.touch .has-dropdown > a').bind('touchstart', function(event){
            //$('.has-dropdown.hover').removeClass('hover').children().not('a').fadeOut(100);
            $(this).parent().siblings().removeClass('hover').children().not('a').fadeOut(100);
            $(this).parent().toggleClass('hover').children().not('a').fadeToggle(100);
            event.preventDefault();

    });
    function hoverOver(){
            $(this).children().not('a').fadeIn(100);
            $(this).addClass('hover');
    }
    function hoverOut(){
            //close any open dropdowns
            $(this).find('.dd-open').removeClass('dd-open');
            $(this).find('.select-open').removeClass('select-open');
            $(this).children().not('a').hide();
            $(this).removeClass('hover');
    }
    $('.has-dropdown').hover(
        function(){
            $(this).addClass('hoverTest');
        },
        function(){
            $(this).removeClass('hoverTest');
        }
    );
    $('.has-dropdown').hoverIntent({
        over:hoverOver,
        timeout:200,
        out:hoverOut
    });

    //these two cause the dropdown/flyout to close if anything on the page is tapped other than something inside it
    $('html.touch').bind('touchstart', function(){
      $('.has-dropdown.hover').removeClass('hover').children().not('a').fadeOut(100);
    });

    $('.touch .has-dropdown').bind('touchstart', function(event){
        event.stopPropagation();
    });




    //manage default values for inputs
    $('input.check-placeholder').each(function(){
        $(this).attr('value',$(this).attr('placeholder'));
    });


    $('input.check-placeholder').blur(function(){
        if($(this).attr('value')==''){
            $(this).attr('value',$(this).attr('placeholder'));
        }
    });

    $('input.check-placeholder').focus(function(){
        if($(this).attr('value')==$(this).attr('placeholder')){
            $(this).attr('value','');
        }
    });



    /********** enhanced <select> form controls *************/
    $('.select-hig-style').each(function(){
        var thisSelect = $(this);
        var buttonClasses = thisSelect.attr('class');
        buttonClasses = buttonClasses.replace('select-hig-style','');
        thisSelect.removeClass(buttonClasses);
        thisSelect.children('select').hide();
        var selectOptions = $(this).find('option');
        var firstOption = "true";
        selectOptions.each(function(){
            if (firstOption == "true") {
                thisSelect.append('<a href="#" class="'+buttonClasses+' selected-option" data-value="'+$(this).attr('value')+'"><span class="icon-dropdown-2">'+$(this).text()+'</span></a>');
                thisSelect.append('<a href="#" class="select-hig-options" data-value="'+$(this).attr('value')+'">'+$(this).text()+'</a>')
                firstOption = "false";
            }
            else{
                thisSelect.append('<a href="#" class="select-hig-options" data-value="'+$(this).attr('value')+'">'+$(this).text()+'</a>');
            }
        });
        $(this).children('.select-hig-options').wrapAll('<div class="more-dropdown-options '+buttonClasses+'"/>');
    });

    $('.selected-option').click(function(event){
        var higSelect = $(this).next('.more-dropdown-options');
        var selectedValue = $(this).attr('data-value');
        var higSelectWrapper = $(this).parent('.select-hig-style');
                    
        if(higSelect.hasClass('select-open')){
            higSelect.removeClass('select-open');
            if ((ieVersion*1)<8) {higSelectWrapper.css('z-index','1')};
        }
        else{
            var height=$(this).outerHeight();
            var width = $(this).width();
            higSelect.children('a').removeClass('hover');
            higSelect.children('a[data-value="'+selectedValue+'"]').addClass('hover');
            higSelect.addClass('select-open').css({'top':height+'px','width':width+'px'});
            if ((ieVersion*1)<8) {higSelectWrapper.css('z-index','2')};
        }
        event.preventDefault();
    });

    $('.select-hig-style').delegate('.select-hig-options', 'click', function(event) {
        var selectedOptionVal = $(this).attr('data-value');
        var selectedOptionText = $(this).text();
        $(this).closest('.select-hig-style').children('select').val(selectedOptionVal);
        $(this).closest('.select-hig-style').children('.selected-option').attr('data-value',selectedOptionVal).children('span').text(selectedOptionText);

        //This section is for dropdowns that extend beyond their flyouts
        var selectInDropdown = $(this).closest('.has-dropdown');
        if (selectInDropdown.length >0){
            //if this <select> is inside a utility dropdown/flyout
            //add a new hidden div here, under the dropdown-options <div>
            //doesn't disappear until you hover off of it
            var height = $(this).closest('.select-hig-style').children('.selected-option').outerHeight();
            var position = $(this).closest('.select-hig-style').position();
            position = position.top;
            var helperWidth = $(this).parent().outerWidth();
            var heightFlyout = $(this).closest('.select-hig-style').parents('.utility-links-dropdown').outerHeight();
            var helperHeight = (height + position + $(this).closest('.more-dropdown-options').outerHeight())  - heightFlyout;
            if (helperHeight < 1) helperHeight = 0;
            var helperTop = heightFlyout - position;
            $(this).closest('.select-hig-style').append('<div id="hover-helper" />');
            $(this).closest('.select-hig-style').append('<div id="hover-helper-2" />');
            $('#hover-helper').css({'position':'absolute','top':height+'px','width':helperWidth+'px','height':$(this).closest('.more-dropdown-options').outerHeight()+'px'});
            $('#hover-helper-2').css({'position':'absolute','top':helperTop+'px','width':helperWidth+'px','height':helperHeight+'px'});
        }

        $(this).closest('.more-dropdown-options').removeClass('select-open');
        if ((ieVersion*1)<8) {$('.select-hig-style').css('z-index','1')};
        event.stopPropagation();
        event.preventDefault();
    });

    //this and the next are for dropdowns that extend beyond their flyout, in Utility bar
    $('.select-hig-style').delegate('#hover-helper', 'mousemove', function(event) {
        $(this).remove();
        $('#hover-helper-2').remove();
    });

    $('.select-hig-style').delegate('#hover-helper-2', 'mouseleave', function(event) {
        $(this).remove();
        $('#hover-helper').remove();
    });


    $('.select-hig-style').delegate('.select-hig-options', 'hover', function(event) {
        $(this).siblings().removeClass('hover');
        $(this).addClass('hover');
    });

    //these two cause the dropdowns to close if anything on the page is clicked other than something inside the select
    $('html').click(function() {
      $('.more-dropdown-options').removeClass('select-open');
      if ((ieVersion*1)<8) {$('.select-hig-style').css('z-index','1')};
    });

    $('.select-hig-style').click(function(event){
        event.stopPropagation();
    });


    /****** List of links dropdown, similar to hig enhanced <select> *******/
    $('.dropdown-list').each(function(){
        var thisD = $(this);
        var label = thisD.children('.dropdown-list-header').text();
        thisD.children('.dropdown-list-header').remove();
        if (label.length < 1){
            label = "Please Select:";
        }
        var buttonClasses = thisD.attr('class');
        buttonClasses = buttonClasses.replace('dropdown-list','');
        thisD.removeClass(buttonClasses);
        thisD.children('ul,h4').wrapAll('<div class="more-dropdown-options '+buttonClasses+'"/>');
        thisD.prepend('<a class="'+buttonClasses+'" href="#"><span class="icon-dropdown-2">'+label+'</span></a>');
        thisD.show();
    });

    $('.dropdown-list > a').click(function(event){
        var links = $(this).next('.more-dropdown-options');
        if(links.hasClass('dd-open')){
            links.removeClass('dd-open');
        }
        else{
            var height=$(this).outerHeight();
            var width = $(this).width();
            links.addClass('dd-open').css({'top':height+'px','width':width+'px'});
        }
        event.preventDefault();
    });

    //these two cause the dropdowns to close if anything on the page is clicked other than something inside the select
    $('html').click(function() {
      $('.dropdown-list > .more-dropdown-options').removeClass('dd-open');
    });

    $('.dropdown-list').click(function(event){
        event.stopPropagation();
    });

    /******* dropdown swapper code, similar to list of links dropdown *******/
    //alter the HTML
    $('.dropdown-swapper-header').each(function(){
        var linkText = $(this).text();
        var ddWrapper = $(this).siblings('.more-dropdown-options');
        if (ddWrapper.length < 1){
            $(this).before('<a href="#" class="button button-type-2"><span class="icon-dropdown-2">'+linkText+'</span></a>');
            $(this).before('<div class="more-dropdown-options button button-type-2" />');
            ddWrapper = $(this).siblings('.more-dropdown-options');
        }
        ddWrapper.append('<a href="#">'+linkText+'</a>');
    });

    $('.dropdown-swapper').each(function(){
        $(this).children('.dropdown-swapper-body:first').show();
    });

    //click event for the dropdown to open-close
    $('.dropdown-swapper').delegate('a:first','click', function(event){
        var links = $(this).next('.more-dropdown-options');
        if(links.hasClass('dd-open')){
            links.removeClass('dd-open');
        }
        else{
            var height=$(this).outerHeight();
            var width = $(this).width();
            links.addClass('dd-open').css({'top':height+'px','width':width+'px'});
        }
        event.preventDefault();
    });

    //click event for the dropdown options, to display correct content underneath
    $('.dropdown-swapper').delegate('.more-dropdown-options > a','click', function(event){
        var label = $(this).text();
        var wrapper = $(this).closest('.dropdown-swapper');
        var ddItems = wrapper.find('.more-dropdown-options > a');
        var bodyDivs = wrapper.children('.dropdown-swapper-body');
        var itemIndex = ddItems.index($(this));
        bodyDivs.hide();
        bodyDivs.eq(itemIndex).show();

        //This section is for dropdowns that extend beyond their flyouts
        var selectInDropdown = $(this).closest('.has-dropdown');
        if (selectInDropdown.length >0){
            //if this is inside a utility dropdown/flyout
            //add a new hidden div here, under the dropdown-options <div>
            //doesn't disappear until you hover off of it
            var height = $(this).closest('.dropdown-swapper').children('a.button').outerHeight();
            var position = $(this).closest('.dropdown-swapper').position();
            position = position.top;
            var helperWidth = $(this).parent().outerWidth();
            var heightFlyout = $(this).closest('.dropdown-swapper').parents('.utility-links-dropdown').outerHeight();
            var helperHeight = (height + position + $(this).closest('.more-dropdown-options').outerHeight())  - heightFlyout;
            if (helperHeight < 1) helperHeight = 0;
            var helperTop = heightFlyout - position;
            $(this).closest('.dropdown-swapper').append('<div id="hover-helper-3" />');
            $(this).closest('.dropdown-swapper').append('<div id="hover-helper-4" />');
            $('#hover-helper-3').css({'position':'absolute','top':height+'px','width':helperWidth+'px','height':$(this).closest('.more-dropdown-options').outerHeight()+'px'});
            $('#hover-helper-4').css({'position':'absolute','top':helperTop+'px','width':helperWidth+'px','height':helperHeight+'px'});
        }
        wrapper.find('.button > span').text(label);
        wrapper.children('.more-dropdown-options').removeClass('dd-open');
        event.preventDefault();
    });

    //these two cause the dropdowns to close if anything on the page is clicked other than something inside the select
    $('html').click(function() {
      $('.dropdown-swapper > .more-dropdown-options').removeClass('dd-open');
    });

    $('.dropdown-swapper').click(function(event){
        event.stopPropagation();
    });

    //this and the next are for dropdowns that extend beyond their flyout, in Utility bar
    $('.dropdown-swapper').delegate('#hover-helper-3', 'mousemove', function(event) {
        $(this).remove();
        $('#hover-helper-4').remove();
    });

    $('.dropdown-swapper').delegate('#hover-helper-4', 'mouseleave', function(event) {
        $(this).remove();
        $('#hover-helper-3').remove();
    });


    //table formating for .blue-white
    $('.blue-white').each(function(){
        $(this).find('tr').filter(':even').addClass('blue-row');
        $(this).find('tr').last().addClass('last-row');
    });

    $('form[data-analytics-var]').each(function(){
        var arrayOfVars = $(this).attr('data-analytics-var').split('|');
        var arrLen = arrayOfVars.length;
        var val="";
        for (var i=0; i < arrLen; i++){
            val = arrayOfVars[i];
            window[val.substr(0,val.indexOf(','))] = val.substr(val.indexOf(',')+1);
        }
    });
});


/*******Auto Quote JS********/

$(function(){
    $('.anchor-retrieve-quote').click(function(event){
        var el = $(this).closest('.hig-getaquote-main');
      el.hide();
      el.next('.hig-retrievequote-main').show();
      el.parent().prev().find('span').text('Retrieve a Quote');
      event.preventDefault();
    });

    $('.anchor-new-quote').click(function(event){
        var el = $(this).closest('.hig-retrievequote-main');
      el.hide();
      el.prev('.hig-getaquote-main').show();
      el.parent().prev().find('span').text('Request a Quote');
      event.preventDefault();
    });
    if($.cookie('hig_quote_flow')){
        $('#marquee-info-block .anchor-retrieve-quote, .toolbox .anchor-retrieve-quote').trigger('click');
    }
});

var origquotePLCode="";
var origquoteOtherPLCode="";
var hitcookiePLCode="false";
function submitNewQuote(currentForm){
 
  if(ValidateNewQuote(currentForm)){
   var lob = $(currentForm).find('select[name="lob"]').val();
   var homeplcodeval = $(currentForm).find('input[name="HomeCode"]').val();
   var currentplcode=$(currentForm).find('input[name="PLCode"]').val();
    
    if(currentplcode!=''  && currentplcode!='undefined'  && currentplcode!=null && origquotePLCode==""){
     origquotePLCode=currentplcode;

    }
   
   if (lob == "Renters" || lob == "Home" || lob == "Condo"){
   
    $(currentForm).find('input[name="KEY"]').val("AARP_HOME");
        if (typeof homequoteurl !=="undefined") {
            $(currentForm).attr('action',homequoteurl);
        }
    //homekey is global & overridden if KEY=AARP_HOME
        if(homeplcodeval!=''  && homeplcodeval!='undefined'  && homeplcodeval!=null && homekey=="false" && hitcookiePLCode=="false"){
            $(currentForm).find('input[name="PLCode"]').val(homeplcodeval);
     } 
   }
   else if(lob == "Flood"){
        if (typeof floodurl !=="undefined") {
        $(currentForm).attr('action',floodurl);
        }
   }else{
        //must be auto - set it back to original value if needed
     if($(currentForm).find('select[name="PLCode"]').val()!=origquotePLCode){
         $(currentForm).find('input[name="PLCode"]').val(origquotePLCode);   
         }
         if($.cookie('hig_quote_plcode_path')){
         if($.cookie('hig_quote_plcode_path').indexOf("/aarp/")>=0){
            $(currentForm).find('input[name="organic"]').remove();
            //console.log("REMOVED ORGANIC");
         }
         }
         
         
   }
        if(!$.cookie('hig_quote_flow')){
            $.cookie('hig_quote_flow','true', { path: '/', expires: 999999 });
        }   
     return true;
  }
  else {
   return false;
  }
}

function ValidateNewQuote(currentForm){
    var zip = $(currentForm).find('.quote-zip-input').val();
  if(zip.length != 5){
    alert('Please enter valid zip code.');
    return false;
  }
  else if(Number(zip)!=zip){
    alert('Please enter valid zip code.');
    return false;
  }
  else{
    return true;
  }
}


function submitRetrieveQuote(aForm){
  if(ValidateForm(aForm)){
    aForm.dob.value = aForm.dobMonth.value+aForm.dobDay.value+aForm.dobYear.value;
        return true;
  }
  else {
    return false;
  }
}

/*******END Auto Quote JS********/
/*******BEGIN generic form validation code for all forms, including auto quote********/

function ValidateEmail(fieldDiv){
    var regexEmail = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/g;
    var emailField=fieldDiv.find('input');
    if (typeof emailField == 'undefined') {
        alert("typeof failed");//no field on the form with the given ID
        return true;
    }
    else{
        if(emailField.val().match(regexEmail)){
            return true;
        }
        else{
            fieldDiv.addClass('error').append('<div class="field-error-msg">Email address format is not valid</div>');
            return false;
        }
    }
}

function Validate3PartDate(fieldDiv){
    var regexMonth = /^0[1-9]|1[0-2]/g;
    var regexYear = /^(19|20)[0-9]{2}/g;
    var regexDate = /^0[1-9]|1[0-9]|2[0-9]|3[0-1]/g;

    var month=fieldDiv.find('input[name="dobMonth"]');
    var day=fieldDiv.find('input[name="dobDay"]');
    var year=fieldDiv.find('input[name="dobYear"]');

    var passed = "true";

    if (typeof month == 'undefined') {
        alert("typeof failed");//no field on the form with the given ID
        return true;
    }
    else{
        if(!month.val().match(regexMonth)){
            fieldDiv.addClass('error').append('<div class="field-error-msg">Please enter a valid two-digit month</div>');
            passed = "false";
        }
    }

    if (typeof day == 'undefined') {
        alert("typeof failed");//no field on the form with the given ID
        return true;
    }
    else{
        if(!day.val().match(regexDate)){
            fieldDiv.addClass('error').append('<div class="field-error-msg">Please enter a valid two-digit date</div>');
            passed = "false";
        }
    }

    if (typeof year == 'undefined') {
        alert("typeof failed");//no field on the form with the given ID
        return true;
    }
    else{
        if(!year.val().match(regexYear)){
            fieldDiv.addClass('error').append('<div class="field-error-msg">Please enter a valid four-digit year</div>');
            passed = "false";
        }
    }

    if (passed=="false"){
        return false;
    }
    else {
        return true;
    }

}

function ValidateZip(fieldDiv){
    var regex5numbers = /^[0-9]{5}/g;
    var zipField=fieldDiv.find('input');
    if (typeof zipField == 'undefined') {
        alert("typeof failed");//no field on the form with the given ID
        return true;
    }
    else{
        if(zipField.val().match(regex5numbers)){
            return true;
        }
        else{
            fieldDiv.addClass('error').append('<div class="field-error-msg">Please enter a valid five-digit zip code</div>');
            return false;
        }
    }
}

function ValidateForm(objForm){
    var thisGetsFocus ="";
    var objFocus = "";
    var validationFailed = "false";
    $(objForm).find('.error .field-error-msg').remove();
    $(objForm).find('.form-field').removeClass('error');

    $(objForm).find('.form-field').each(function(){
        var fieldLabel = $(this).children('.field-name').text();
        var wrapper=$(this);
        if($(this).hasClass('required-field')){

            //text inputs
            var inputText = $(this).find('input[type="text"]');
            if(inputText.length>0){
                inputText.each(function(){
                    objFocus = $(this).first();
                    if( ($(this).val()=='' ) || ($(this).val()==$(this).attr('placeholder') ) ){
                        wrapper.addClass('error').append('<div class="field-error-msg">Please enter ' + fieldLabel+'</div>');
                        if (validationFailed=="false") thisGetsFocus = objFocus;
                        validationFailed = "true";
                        return false;
                    }
                });
            }

            //radio inputs
            var inputRadio = $(this).find('input[type="radio"]');
            if(inputRadio.length>0){
                objFocus = inputRadio.first();
                var radioPassed = "false";
                inputRadio.each(function(){
                    if($(this).attr('checked')) radioPassed = "true";
                });
                if(radioPassed=="false"){
                    if (validationFailed=="false") thisGetsFocus = objFocus;
                    wrapper.addClass('error').append('<div class="field-error-msg">Please select an option</div>');
                    validationFailed="true";
                }
            }

            //checkbox inputs
            var inputCheckbox = $(this).find('input[type="checkbox"]');
            if(inputCheckbox.length>0){
                objFocus = inputCheckbox.first();
                var cbPassed = "false";
                inputCheckbox.each(function(){
                    if($(this).attr('checked')) cbPassed = "true";
                });
                if(cbPassed=="false"){
                    if (validationFailed=="false") thisGetsFocus = objFocus;
                    wrapper.addClass('error').append('<div class="field-error-msg">Please select an option</div>');
                    validationFailed="true";
                }
            }

        }

        //if a field needs custom validation, need to put names of function in attrib "data-validation-function"
        //put this on form-field div, not at the input level, as validation could include a group of fields like phone number

        if($(this).attr('data-validation-function')!=undefined){
            if(!wrapper.hasClass('error')){
                var valFunction = $(this).attr('data-validation-function');
                if(!(window[valFunction]($(this)))){
                        if (validationFailed=="false") thisGetsFocus = objFocus;
                        validationFailed = "true";
                }
            }
        }

    });

    if(validationFailed == "true"){
        thisGetsFocus.focus();
        return false;
    }
    else{
        return true;
    }

}
/*******END generic form validation********/

function lpcookie(pageid){
    $.cookie("hig_lp", pageid, { expires: 365, path:"/" }); 
    hidealertbar();
}

function declinelpcookie(pageid){
var declineval="";
    if($.cookie("decline_hig_lp")!=null){
        declineval= $.cookie("decline_hig_lp");
    }
    declineval=declineval+pageid+",";
    $.cookie("decline_hig_lp", declineval, { expires: 365, path:"/"  });
    hidealertbar();
}

function hidealertbar(){
    $('#alert-bar').hide('slow');
}


if( typeof(higsearch) == 'undefined'  ) {
/* Modernizr 2.6.1 (Custom Build) | MIT & BSD
 * Build: http://modernizr.com/download/#-touch-cssclasses-teststyles-prefixes
 */
;window.Modernizr=function(a,b,c){function w(a){j.cssText=a}function x(a,b){return w(m.join(a+";")+(b||""))}function y(a,b){return typeof a===b}function z(a,b){return!!~(""+a).indexOf(b)}function A(a,b,d){for(var e in a){var f=b[a[e]];if(f!==c)return d===!1?a[e]:y(f,"function")?f.bind(d||b):f}return!1}var d="2.6.1",e={},f=!0,g=b.documentElement,h="modernizr",i=b.createElement(h),j=i.style,k,l={}.toString,m=" -webkit- -moz- -o- -ms- ".split(" "),n={},o={},p={},q=[],r=q.slice,s,t=function(a,c,d,e){var f,i,j,k=b.createElement("div"),l=b.body,m=l?l:b.createElement("body");if(parseInt(d,10))while(d--)j=b.createElement("div"),j.id=e?e[d]:h+(d+1),k.appendChild(j);return f=["&#173;",'<style id="s',h,'">',a,"</style>"].join(""),k.id=h,(l?k:m).innerHTML+=f,m.appendChild(k),l||(m.style.background="",g.appendChild(m)),i=c(k,a),l?k.parentNode.removeChild(k):m.parentNode.removeChild(m),!!i},u={}.hasOwnProperty,v;!y(u,"undefined")&&!y(u.call,"undefined")?v=function(a,b){return u.call(a,b)}:v=function(a,b){return b in a&&y(a.constructor.prototype[b],"undefined")},Function.prototype.bind||(Function.prototype.bind=function(b){var c=this;if(typeof c!="function")throw new TypeError;var d=r.call(arguments,1),e=function(){if(this instanceof e){var a=function(){};a.prototype=c.prototype;var f=new a,g=c.apply(f,d.concat(r.call(arguments)));return Object(g)===g?g:f}return c.apply(b,d.concat(r.call(arguments)))};return e}),n.touch=function(){var c;return"ontouchstart"in a||a.DocumentTouch&&b instanceof DocumentTouch?c=!0:t(["@media (",m.join("touch-enabled),("),h,")","{#modernizr{top:9px;position:absolute}}"].join(""),function(a){c=a.offsetTop===9}),c};for(var B in n)v(n,B)&&(s=B.toLowerCase(),e[s]=n[B](),q.push((e[s]?"":"no-")+s));return e.addTest=function(a,b){if(typeof a=="object")for(var d in a)v(a,d)&&e.addTest(d,a[d]);else{a=a.toLowerCase();if(e[a]!==c)return e;b=typeof b=="function"?b():b,f&&(g.className+=" "+(b?"":"no-")+a),e[a]=b}return e},w(""),i=k=null,e._version=d,e._prefixes=m,e.testStyles=t,g.className=g.className.replace(/(^|\s)no-js(\s|$)/,"$1$2")+(f?" js "+q.join(" "):""),e}(this,this.document);

}

function timer(length){
    var date = new Date();
    var curDate = null;
    do { curDate = new Date(); }
    while(curDate-date < length);
} 

function B4SubmitSearch(initialValue) {
    if ( (document.getElementById('search-text').value == initialValue) || (document.getElementById('search-text').value == '') ){
        alert("Cannot submit search. Please enter a search phrase.");
        return false;
    }
    var searchText = document.getElementById("search-text").value;
    try{eventCapture('Internal Search','Search Box', searchText,'');}catch(err){}
    timer(300);//delay so evantCapture can complete before page is navigated away from
}



//Legacy functions
function openBWindow(pageToLoad, winName, rs, w, h, thex, they){
        xposition=0; yposition=0;
        if ((parseInt(navigator.appVersion) >= 4 ) && (thex) && (they)){
                xposition = thex;
                yposition = they;
        }
    var resize = "";
    if (rs) {
        resize = "resizable,";
    }
       msgWindow=open(pageToLoad,winName,'toolbar=1,scrollbars=1,location=1,statusbar=0,menubar=1,' + resize + 'width=' + w + ',height=' + h);
        if (msgWindow.opener == null){
                msgWindow.opener = self;
        }
        if(! window.focus){
         }
       else{
         msgWindow.focus();
       }
}

function openCWindow(pageToLoad, winName, rs, w, h, thex, they){
        xposition=0; yposition=0;
        if ((parseInt(navigator.appVersion) >= 4 ) && (thex) && (they)){
                xposition = thex;
                yposition = they;
        }
    var resize = "";
    if (rs) {
        resize = "resizable,";
    }
       msgWindow=open(pageToLoad,winName,'toolbar=no,scrollbars=no,location=no,status=no,menubar=no,' + resize + 'width=' + w + ',height=' + h);
        if (msgWindow.opener == null){
                msgWindow.opener = self;
        }
        if(! window.focus){
         }
       else{
         msgWindow.focus();
       }
}

function openDWindow(pageToLoad, winName, rs, w, h, thex, they){
        xposition=0; yposition=0;
        if ((parseInt(navigator.appVersion) >= 4 ) && (thex) && (they)){
                xposition = thex;
                yposition = they;
        }
    var resize = "";
    if (rs) {
        resize = "resizable,";
    }

       msgWindow=open(pageToLoad,winName,'toolbar=no,scrollbars=1,location=no,status=no,menubar=no,' + resize + 'width=' + w + ',height=' + h);
        if (msgWindow.opener == null){
                msgWindow.opener = self;
        }
        if(! window.focus){
         }
       else{
         msgWindow.focus();
       }
}

function pop(url) {
    var left = (screen.width / 2) - (895 / 2);
    var top = (screen.height / 2) - (685 / 2);
    var newWin;
    newWin = window.open(url, "newWin", "status=no,scrollbars=no,resizable=no,toolbar=no,location=no,width=895,height=685,left=" + left + ",top=" + top + "");
    newWin.focus();   

}

/* Set some ids on Quote Forms for Adobe Insight */
$('.hig-getaquote-main, .hig-retrievequote-main').each(function(){
    var $form = $(this).children('form');
    var attr = $form.attr('id');
    var cName = $(this).attr('class');

    if (typeof attr == 'undefined' || attr == false) {
        if ( $form.closest('#header').length != 0 ){
            $form.attr('id','header-'+ cName);
            $form.find('input[type=submit]').attr('id','header-'+ cName+'-submit');
        }
        else if($form.closest('.toolbox').length != 0){
            $form.attr('id','toolbox-'+ $(this).attr('class'));
            $form.find('input[type=submit]').attr('id','toolbox-'+ cName+'-submit');
        }
        else if($form.closest('#content-head').length != 0){
            $form.attr('id','marquee-'+ $(this).attr('class'));
            $form.find('input[type=submit]').attr('id','marquee-'+ cName+'-submit');
        }
        else {
            $form.attr('id','body-'+ $(this).attr('class'));
            $form.find('input[type=submit]').attr('id','body-'+ cName+'-submit');
        }
    }
});


function overridequotevals(quotefield, quotevalue){
    var inputfieldname = 'input[name='+quotefield+']';
    $('#container').find(inputfieldname).each(function(){
        $(this).val(quotevalue);

    })


}


$(window).load(function() {
    //add pdf icon (via script because need new element bc some links already have background image
    $('.icon-pdf,.icon-word,.icon-xls').not(':has(img)').each(function() {
        $(this).css({'position':'relative','margin-left':'22px'});
        $(this).prepend('<span class="contains-image" />');
    });

    // video image play button
    $('.video-play').each(function(){
        $(this).wrap('<div class="play-button" />');
        $(this).after('<div class="play-button-image" />');
        var imgH = $(this).height();
        var imgW = $(this).width();
        $(this).next('.play-button-image').css({'left':(imgW * .5 - 22)+'px','top':(imgH *.5 - 22)+'px'});
        if(ieVersion==6 || ieVersion==7){
            $(this).closest('a').css('position','relative');
        }
        
    });
    
    //ie6-7
    if(ieVersion==6 || ieVersion==7){
        $('body').delegate('.play-button', 'hover', function() {
            $(this).find('.play-button-image').toggleClass('hover');
        });
    }
});

var homekey="false";//global var
addLoadEvent(

function(){
var winpathname = window.location.pathname;
var cookiepath="/";
/*11/28/2012 commented out by bv - no longer setting seperate cookie for /aarp
if (winpathname.substring(0, 6) == "/aarp/"){cookiepath="/aarp/";}*/

    $.urlParam = function(name){
        
        var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if(results!=null){
         return results[1] || 0;
        }
        
    }   
    if($.urlParam('PLCode')!=null){
        $.cookie("hig_quote_plcode", $.urlParam('PLCode'), { expires: 1, path:cookiepath });
        overridequotevals('PLCode',decodeURIComponent($.urlParam('PLCode')));
        hitcookiePLCode="true";
        
        //set orignal cookie path - will use this to remove organic if came in on aarp
        $.cookie("hig_quote_plcode_path", winpathname, { expires: 1, path:'/'});
                
    }else{
        if($.cookie('hig_quote_plcode')) {
            overridequotevals('PLCode',$.cookie('hig_quote_plcode'));
            hitcookiePLCode="true";
        }
    
    
    }

    //if cookie exists, plcode exists, so remove organic inputs on all quote forms
    if($.cookie('hig_quote_plcode')) {
        $('.hig-getaquote-main input[name=organic]').remove();
    }
    
    if($.urlParam('KEY')!=null){
        if($.urlParam('KEY')=="AARP_HOME"){
            homekey="true";
        }
    
    }

    if($.urlParam('overlay')!=null){
        var contentID = decodeURIComponent($.urlParam('overlay'));
        if(contentID*1 == contentID){
            LightBoxDisplay(contentID,'/cs/Satellite?pagename=HIG_Standard/LoadContent&SiteURLPrefix=TheHartford&contentid=' + contentID);
        }
    }
    
        
}
);


$(function(){
        var pathname = window.location.pathname;
        var changequotedefault="";
        if(pathname.indexOf("/recreational-vehicles/")>=0){
            changequotedefault=1;
        }
        if(pathname.indexOf("antique-car-insurance")>=0 || pathname.indexOf("antique-auto-insurance")>=0){
            changequotedefault=2;
        }
        if(pathname.indexOf("/aarp/home-insurance/")>=0){
            changequotedefault=3;
        }
        if(pathname.indexOf("/aarp/home-insurance/condo-insurance")>=0){
            changequotedefault=4;
        }
        if(pathname.indexOf("/aarp/home-insurance/renters-insurance")>=0){
            changequotedefault=5;
        }
        if(pathname.indexOf("/aarp/home-insurance/flood-insurance")>=0){
            changequotedefault=6;
        }
        var changeselectedvalue="";
        var changeselectedtext="";
        if(changequotedefault!=""){

            $('#container').find('select[name="lob"]').each(function(){
                $(this)[0].selectedIndex = changequotedefault;
                changeselectedvalue=$(this).val();
                changeselectedtext=$(this).find('option:selected').text();
                if(changeselectedvalue!="" && changeselectedtext!=""){
                    quoteparentobj =$(this).parent();                   
                    $(quoteparentobj).closest('.select-hig-style').children('.selected-option').attr('data-value',changeselectedvalue).children('span').text(changeselectedtext);       
                }
            });                 
        }
});


/*PREDICTIVE QUERY*/    
var callingpredictivequery="false";
var searchterm="";
var suggestedresultshtml="";
var searchtermlng;
var prvsearchterm="";
var oPredictiveCache = new Object();
var predictivequeryURL = predictivequeryDomain+'/esearchclient/suggest?app=hig2&start=1&results=10';
var submitviaenter=false;
$(document).ready(function () {
    $('#search-form').keypress(function(e){
        //by default don't allow submit on enter
        if ( e.which == 13 ) e.preventDefault();
    });



    $('#hig-suggestresults').mouseover(function(){
            $('#hig-suggestresults ul li a').removeClass("itemhover");//if user used arrow keys - now using mouse - need to clear hover classes
    });

    $('#search-text').attr('autocomplete','off');

    $('#search-text').keyup(function (e) {
            //set interval to turn put back styles on search-box
            setInterval(function(){
               if(!$('#hig-suggestresults').is(":visible")) {
                        //console.log("it not visible");
                            $('#search-text').removeClass('suggestedquery');
                 }
            }, 2000);


        var hitupdown=false;
        if(suggestedresultshtml!=""){
            switch(e.keyCode) {
                 case 38:
                    navigate('up');
                    hitupdown=true;
                    var tmpsearchtext=$('#search-text').val();
                    $('#search-text').val(tmpsearchtext);//this is done for chrome/safari - up arrow moves curser to first char - this puts back at end.


                  break;
                 case 40:
                    navigate('down');
                     hitupdown=true;
                 break;
                 case 13:
                     hitupdown=true;
                     submitviaenter=true;
                        //console.log("hit enter=|"+$("#hig-suggestresults ul li a.itemhover span").text()+"|");
                        if($('#hig-suggestresults').is(":visible") && $("#hig-suggestresults ul li a.itemhover span").text()!="") {
                            $('#search-text').val($("#hig-suggestresults ul li a.itemhover span").text());
                        }
                        $('#hig-suggestresults').hide();
                        $('#search-form').submit();


                break;
            }

        }
        if(!hitupdown){
            var RegExpressPredictiveQuery = /^[a-zA-Z0-9 ]*$/;   //letters & numbers only

            if (RegExpressPredictiveQuery.test(searchterm)) {
                //console.log("keyup  - " + callingpredictivequery);
                searchterm = $('#search-text').val().toLowerCase();
                searchtermlng = $('#search-text').val().length;
                //console.log("searchterm="+searchterm);
                //console.log("searchtermlng="+searchtermlng);


                if(checkPredictiveCache(searchterm)) {
                    //console.log("IS in CACHE");
                     populateResults(oPredictiveCache[searchterm]);
                }else{

                    if(callingpredictivequery=="false" && searchtermlng>2 && prvsearchterm!=searchterm && searchterm.toLowerCase()!="search"){
                        callingpredictivequery="true";
                        makesearchcall();
                        prvsearchterm = searchterm;
                    }
                }
            }else{
                //console.log('invalid char');
            }

        }else{
            //console.log('hit up down key');
        }
    });
});

function makesearchcall(){
//console.log('about to make call = '+searchterm);
    jQuery.getJSON(predictivequeryURL+'&q='+searchterm+'&callback=?', function(data) {

      populateResults(data);
      addToPredictiveCache(searchterm, data);
    });
}

//hide search results if click outside of element
$(document).click(function(event) {
    if($(event.target).parents().index($('#search-site')) == -1) {
         if($('#hig-suggestresults').is(":visible")) {
           clearPredictiveResults();
           $('#search-text').removeClass('suggestedquery');
        }
    }
})

$(document).click(function(event) {
    if($(event.target).parents().index($('#search-site')) == -1) {
         if($('#hig-suggestresults').is(":visible")) {
           clearPredictiveResults();
           $('#search-text').removeClass('suggestedquery');
        }
    }
})


function populateResults(data){

    //if no results - need to hide suggestedquery div

    suggestedresultshtml="<ul>";
    $.each(data.app_prop[0].terms ,function(i,term){
       // alert(data.app_prop[0].terms[i].term);
        //console.log("term="+ data.app_prop[0].terms[i].term);
        suggestedresultshtml = suggestedresultshtml + '<li><a href="#"><span>'+data.app_prop[0].terms[i].term+'</span></a></li>';
    });
     callingpredictivequery="false";
     suggestedresultshtml = suggestedresultshtml + "</ul>";
     $('#hig-suggestresults').html(suggestedresultshtml);
     $('#search-text').addClass('suggestedquery');
    // console.log("add sugg query class");
     $('#hig-suggestresults').show();

        $('#hig-suggestresults a').click(function() {
            $("#hig-suggestresults ul li a").removeClass("itemhover");
            $(this).addClass("itemhover");
            $('#search-text').val($("#hig-suggestresults ul li a.itemhover span").text());
            $('#hig-suggestresults').hide();
            $('#search-form').submit();
            return false;

        });
}

function navigate(direction) {
    //console.log("szie="+$("#hig-suggestresults ul li").size());
   if($("#hig-suggestresults ul li .itemhover").size() == 0) {
      currentSelection = -1;
   }

   if(direction == 'up' && currentSelection != -1) {
      if(currentSelection != 0) {
         currentSelection--;
      }
   } else if (direction == 'down') {
      if(currentSelection != $("#hig-suggestresults ul li").size() -1) {
         currentSelection++;
      }
   }
   setSelected(currentSelection);

}

function setSelected(menuitem) {
    //console.log("set section=" + menuitem);
   $("#hig-suggestresults ul li a").removeClass("itemhover");
   $("#hig-suggestresults ul li a").eq(menuitem).addClass("itemhover");

}



function clearPredictiveResults() {
    $('#hig-suggestresults').hide();
    $('#hig-suggestresults').css('height','auto' );
    $('#search-text').removeClass('suggestedquery');
}

function addToPredictiveCache(key, value) {
  oPredictiveCache[key] = value;
  //console.log('added to cache='+key);
}

function checkPredictiveCache(key) {
  if(oPredictiveCache[key]){
    return true;
  }else{
    return false;
    }
}   
/*END PREDICTIVE QUERY*/

//Tcode passing
function getQueryVariable2(variable,query){
       var vars = query.split("&");
       for (var i=0;i<vars.length;i++) {
               var pair = vars[i].split("=");
               if(pair[0] == variable){return pair[1];}
       }
       return(false);
}

function updateHrefs(param,valu){
	$('a:not([href^="#"]):not([href^="javascript"]):not([href^="Javascript"])').each(function(){
		var theQstring ='';
		var theHash ='';
		var theUrl = '';
		var defaultVal = '';
		var theHref=$(this).attr('href');
		if(theHref){
			theUrl = theHref;
			if(theHref.indexOf('?')>0){
				theUrl = theHref.split('?')[0];
				theQstring += theHref.split('?')[1];
				if(theHref.indexOf('#')>0){
					theHash = '#'+theHref.split('#')[1];
					theQstring = theQstring.split('#')[0];
				}
				theQstring += '&';
			}
			else if(theHref.indexOf('#')>0){
				theUrl = theHref.split('#')[0];
				theHash = '#'+theHref.split('#')[1];
			}
			defaultVal = getQueryVariable2(param,theQstring);
			if(!defaultVal){//no param value in the href, so continue with logic to add it
				$(this).attr('href', theUrl+'?'+theQstring+param+'='+valu+theHash);
			}else{//there is a param value so replace it
				theHref = theHref.replace(defaultVal,valu);
				$(this).attr('href', theHref);
			}
		}
	});
}

function updateTcode(){
	if(document.URL.indexOf('Tcode') > 0){
		var Tcode = getQueryVariable2("Tcode",window.location.search.substring(1));
		if(Tcode){
			updateHrefs('Tcode',Tcode);
		}
	}
}

updateTcode();