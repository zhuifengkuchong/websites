
var modal = (function(){
				var 
				method = {},
				$overlay,
				$modal,
				$content,
				$close;

				
				method.center = function () {
					var top, left;
					$modal.css({
						top:'50%',
                        width: '680px',
                        margin: '0 100 0 auto',
                        background:'rgba(0,0,0,0.2)',
                        borderRadius:'14px',
                        padding:'8px'
					});
				};

				
				method.open = function (settings) {
					$content.empty().append(settings.content);

					$modal.css({
						width: settings.width || 'auto', 
						height: settings.height || 'auto'
					});

					method.center();
					$(window).bind('resize.modal', method.center);
					$modal.show();
					$overlay.show();
				};

				
				method.close = function () {
					$modal.hide();
					$overlay.hide();
					$content.empty();
					$(window).unbind('resize.modal');
				};

				
				$overlay = $('<div id="overlay"></div>');
				$modal = $('<div id="modal"></div>');
				$content = $('<div id="content"></div>');
				$close = $('<a id="close" href="#">close</a>');

				$modal.hide();
				$overlay.hide();
				$modal.append($content, $close);

				$(document).ready(function(){
					$('body').append($overlay, $modal);						
				});

				$close.click(function(e){
					method.close();
					jwplayer('content').remove();
				});

				return method;
			}());

		
			function PlayVideo(vFile , vImage){
						modal.open({content: "Loading the player..."});
					jwplayer("content").setup({
						controlbar:"bottom",
						file:vFile ,
						image:vImage,
                        flashplayer: "jwplayer6/jwplayer.flash.swf"/*tpa=http://www.viacom.com/_catalogs/masterpage/js/jwplayer6/jwplayer.flash.swf*/,
                        width: "640",
   						height: "360",
        				logo:{file:'../img/viacom.png'/*tpa=http://www.viacom.com/_catalogs/masterpage/img/viacom.png*/,}
				});
								//alert($videoLink);
					e.preventDefault();

			}
