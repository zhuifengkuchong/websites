<script id = "race53b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race53={};
	myVars.races.race53.varName="Function[40664].elem";
	myVars.races.race53.varType="@varType@";
	myVars.races.race53.repairType = "@RepairType";
	myVars.races.race53.event1={};
	myVars.races.race53.event2={};
	myVars.races.race53.event1.id = "milgard";
	myVars.races.race53.event1.type = "onmouseout";
	myVars.races.race53.event1.loc = "milgard_LOC";
	myVars.races.race53.event1.isRead = "True";
	myVars.races.race53.event1.eventType = "@event1EventType@";
	myVars.races.race53.event2.id = "delta";
	myVars.races.race53.event2.type = "onmouseover";
	myVars.races.race53.event2.loc = "delta_LOC";
	myVars.races.race53.event2.isRead = "False";
	myVars.races.race53.event2.eventType = "@event2EventType@";
	myVars.races.race53.event1.executed= false;// true to disable, false to enable
	myVars.races.race53.event2.executed= false;// true to disable, false to enable
</script>

