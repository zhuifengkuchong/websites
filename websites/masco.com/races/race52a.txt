<script id = "race52a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race52={};
	myVars.races.race52.varName="Array[9513]$LEN";
	myVars.races.race52.varType="@varType@";
	myVars.races.race52.repairType = "@RepairType";
	myVars.races.race52.event1={};
	myVars.races.race52.event2={};
	myVars.races.race52.event1.id = "liberty";
	myVars.races.race52.event1.type = "onmouseover";
	myVars.races.race52.event1.loc = "liberty_LOC";
	myVars.races.race52.event1.isRead = "False";
	myVars.races.race52.event1.eventType = "@event1EventType@";
	myVars.races.race52.event2.id = "milgard";
	myVars.races.race52.event2.type = "onmouseout";
	myVars.races.race52.event2.loc = "milgard_LOC";
	myVars.races.race52.event2.isRead = "True";
	myVars.races.race52.event2.eventType = "@event2EventType@";
	myVars.races.race52.event1.executed= false;// true to disable, false to enable
	myVars.races.race52.event2.executed= false;// true to disable, false to enable
</script>

