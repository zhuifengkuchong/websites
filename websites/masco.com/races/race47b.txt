<script id = "race47b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race47={};
	myVars.races.race47.varName="Function[6416].uuid";
	myVars.races.race47.varType="@varType@";
	myVars.races.race47.repairType = "@RepairType";
	myVars.races.race47.event1={};
	myVars.races.race47.event2={};
	myVars.races.race47.event1.id = "liberty";
	myVars.races.race47.event1.type = "onmouseover";
	myVars.races.race47.event1.loc = "liberty_LOC";
	myVars.races.race47.event1.isRead = "True";
	myVars.races.race47.event1.eventType = "@event1EventType@";
	myVars.races.race47.event2.id = "delta";
	myVars.races.race47.event2.type = "onmouseover";
	myVars.races.race47.event2.loc = "delta_LOC";
	myVars.races.race47.event2.isRead = "False";
	myVars.races.race47.event2.eventType = "@event2EventType@";
	myVars.races.race47.event1.executed= false;// true to disable, false to enable
	myVars.races.race47.event2.executed= false;// true to disable, false to enable
</script>

