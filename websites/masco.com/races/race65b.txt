<script id = "race65b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race65={};
	myVars.races.race65.varName="Function[40664].elem";
	myVars.races.race65.varType="@varType@";
	myVars.races.race65.repairType = "@RepairType";
	myVars.races.race65.event1={};
	myVars.races.race65.event2={};
	myVars.races.race65.event1.id = "merillat";
	myVars.races.race65.event1.type = "onmouseout";
	myVars.races.race65.event1.loc = "merillat_LOC";
	myVars.races.race65.event1.isRead = "True";
	myVars.races.race65.event1.eventType = "@event1EventType@";
	myVars.races.race65.event2.id = "delta";
	myVars.races.race65.event2.type = "onmouseover";
	myVars.races.race65.event2.loc = "delta_LOC";
	myVars.races.race65.event2.isRead = "False";
	myVars.races.race65.event2.eventType = "@event2EventType@";
	myVars.races.race65.event1.executed= false;// true to disable, false to enable
	myVars.races.race65.event2.executed= false;// true to disable, false to enable
</script>

