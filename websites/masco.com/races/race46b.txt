<script id = "race46b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race46={};
	myVars.races.race46.varName="Function[38607].elem";
	myVars.races.race46.varType="@varType@";
	myVars.races.race46.repairType = "@RepairType";
	myVars.races.race46.event1={};
	myVars.races.race46.event2={};
	myVars.races.race46.event1.id = "delta";
	myVars.races.race46.event1.type = "onmouseover";
	myVars.races.race46.event1.loc = "delta_LOC";
	myVars.races.race46.event1.isRead = "True";
	myVars.races.race46.event1.eventType = "@event1EventType@";
	myVars.races.race46.event2.id = "milgard";
	myVars.races.race46.event2.type = "onmouseover";
	myVars.races.race46.event2.loc = "milgard_LOC";
	myVars.races.race46.event2.isRead = "False";
	myVars.races.race46.event2.eventType = "@event2EventType@";
	myVars.races.race46.event1.executed= false;// true to disable, false to enable
	myVars.races.race46.event2.executed= false;// true to disable, false to enable
</script>

