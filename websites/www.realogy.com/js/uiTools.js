// ----------------------------------------------
// REALOGY - Base User Interface Tools
//
// Date: Apr 25, 2006
// Devl: Derek Alldritt
// Revision: 1.0
// ----------------------------------------------

function quickSearch() {
	if (document.quickSearchForm.qt.value > "") {
		document.quickSearchForm.submit();
	} else {
		alert("Please input a search term to continue");
	}
}

function Search() {
	if (document.searchForm.qt.value > "") {
		document.searchForm.submit();
	} else {
		alert("Please input a search term to continue");
	}
}

function quickMediaSearch() {
	if (document.quickMediaSearchForm.qt.value > "") {
		document.quickMediaSearchForm.submit();
	} else {
		alert("Please input a search term to continue");
	}
}

function careerSearch() {
                document.JobSearch_NA.submit();
}

function openFeed(myURL) {
	prompt('Copy the URL below into your RSS reader to read headlines:',myURL);
}

// Function to verify the existance of a variable
function isDefined(item) {
	return (typeof item != 'undefined');
}


// ----------------------------------------------
// STYLE SWITCHING
// ===============
// - Humm....switches the font size only for now.
// ----------------------------------------------
var prefsLoaded = false;
var currentFontSize = 12;

function revertStyles(){
	currentFontSize = 12;
	changeFontSize(0);
}
function changeFontSize(sizeDifference){
	currentFontSize = parseInt(currentFontSize) + parseInt(sizeDifference);

	if(currentFontSize > 16){
		currentFontSize = 16;
	} 
	else if(currentFontSize < 8){
		currentFontSize = 8;
	}
	setFontSize(currentFontSize, 'CenterNav');
	setFontSize(currentFontSize, 'RightNav');
};
function setFontSize(fontSize, ElementID){
	var stObj = (document.getElementById) ? document.getElementById(ElementID) : document.all(ElementID);
	if( stObj ) {
		stObj.style.fontSize = fontSize + 'px';
	}
};

//window.onload = setUserOptions();

function setUserOptions(){
	if(!prefsLoaded){
		cookie = readCookie("fontSize");
		currentFontSize = cookie ? cookie : 12;
		setFontSize(currentFontSize, 'CenterNav');
		setFontSize(currentFontSize, 'RightNav');
		prefsLoaded = true;
	}
}

window.onunload = saveSettings;

function saveSettings()
{
  createCookie("fontSize", currentFontSize, 365);
}


// ----------------------------------------------
// Cookie functions
// ================
// - Pretty self explanitory, I hope.
// ----------------------------------------------

function createCookie(name,value,days) {
  if (days) {
    var date = new Date();
    date.setTime(date.getTime()+(days*24*60*60*1000));
    var expires = ";expires="+date.toGMTString();
  }
  else expires = "";
  document.cookie = name+"="+value+expires+";domain=.realogy.com;path=/;";
}
function readCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for(var i=0;i < ca.length;i++) {
    var c = ca[i];
    while (c.charAt(0)==' ') c = c.substring(1,c.length);
    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
  }
  return null;
}


// ----------------------------------------------
// HyperLink functions
// ===================
// - Rolls one graphic to another in say, a menu.
// ----------------------------------------------

function roll(file, tag) {
	document.images[tag].src = file;
}


// ----------------------------------------------
// FLASH LOAD FUNCTION - INDEX.CFM
// - This is an interesting work around to a 
//   patent infringement.  This function MUST
//   call the object as part of the patent
//   workaround.  Lovely.
// ----------------------------------------------

function writeIndexFlash() {
	var cookie = readCookie( "PLAYFULLFLASH" );
	if ( cookie == null ) {
		createCookie("PLAYFULLFLASH", "true", 365 );
		playfull = true;
	} else {
		playfull = false;
	}
		document.write('<div class="FlashHeader">');
			document.write('<OBJECT WIDTH="780" HEIGHT="184">');
			document.write('<PARAM NAME="MOVIE" VALUE="../flash/flashbanner.swf"/*tpa=https://www.realogy.com/flash/flashbanner.swf*/>');
			document.write('<PARAM NAME="PLAY" VALUE="true">');
			document.write('<PARAM NAME="FlashVars" VALUE="playfull=' + playfull + '">');
			document.write('<PARAM NAME="QUALITY" VALUE="best">');
			document.write('<EMBED SRC="../flash/flashbanner.swf"/*tpa=https://www.realogy.com/flash/flashbanner.swf*/ WIDTH="780" HEIGHT="184" PLAY="true" LOOP="true" QUALITY="best" FlashVars="playfull=' + playfull + '">'); 
			document.write('</EMBED>');
		document.write('</OBJECT>');
}

function writeDefinedFlash() {
                document.write('<div class="FlashHeader">');
                        document.write('<OBJECT WIDTH="780" HEIGHT="184">');
                        document.write('<PARAM NAME="MOVIE" VALUE="../flash/realogy_defined.swf"/*tpa=https://www.realogy.com/flash/realogy_defined.swf*/>');
                        document.write('<PARAM NAME="PLAY" VALUE="true">');
                        document.write('<PARAM NAME="QUALITY" VALUE="best">');
                        document.write('<EMBED SRC="../flash/realogy_defined.swf"/*tpa=https://www.realogy.com/flash/realogy_defined.swf*/ WIDTH="420" HEIGHT="244" PLAY="true" LOOP="true" QUALITY="best">');
                        document.write('</EMBED>');
                document.write('</OBJECT>');
}


// EOF
