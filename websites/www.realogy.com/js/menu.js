function isIE56() {
	var detect = navigator.userAgent.toLowerCase();
	var place = detect.indexOf('msie ');
	var version = 0;
	if ( place > 0 ) {
		version = detect.charAt(place + 5);
	}
	return ( version >= 7 || version == 0 ? false : true );
}

function showSearch() {
	show('searchbox');
	show('searchbox_invisibleLyr');
}

function hideSearch() {
	hide('searchbox');
	hide('searchbox_invisibleLyr');
}

var menuStack = new Array();
var menuItemStack = new Array();

function showMenu(lyrname, level) {
	
	if ( menuStack[level] != lyrname ) {
		hideMenus(level);
	} else {
		hideMenus(level+1);
	}
	
	show(lyrname);
	showInvis();
	menuStack[level] = lyrname;
	if ( isIE56() ) {
		toggleFormfields('hidden');
	}
}

function hideMenus(level) {
	if(level == 1) {
		hideInvis();
	}

	for ( var i = (menuStack.length -1); i >= level; i-- ) {
		hide(menuStack[i]);
	}
	if ( isIE56() ) {
		toggleFormfields('visible');
	}
}

function hideInvis(obj) {
	var theObj;
	theObj = getObject("invisibleLyr");
    
	if (theObj) {
		theObj.visibility = "hidden";
	} 
} 

function showInvis(obj) {
	var theObj;
	theObj = getObject("invisibleLyr");

	if (theObj) {
		theObj.visibility = "visible";
	} 
}

function toggleFormfields(state) {
	var selectElements = document.body.getElementsByTagName('select');
		
	for (var i=0; i < selectElements.length; i++) {
		selectElements[i].style.visibility = state;
	}
}

