
function EnableDisableBrowseFields(rdoFromLocalDrive, rdoFromSPSite, trAssetSelector, trFileSelector, MediaType, media) {

    var objrdoFromSPSite = document.getElementById(rdoFromSPSite);
    var objrdoFromLocalDrive = document.getElementById(rdoFromLocalDrive);

    if (objrdoFromSPSite.checked) {
        document.getElementById(trAssetSelector).style.display = 'block';
        document.getElementById(trFileSelector).style.display = 'none';
    }
    else if (objrdoFromLocalDrive.checked) {
        document.getElementById(trAssetSelector).style.display = 'none';
        document.getElementById(trFileSelector).style.display = 'block';
    }
}


function MediaChange(media, label, height, width, size, tblVideo1, tblImage, tblPreviewImage, lblPreviewImageSize, lblImageSize) {


    var Video1 = document.getElementById(tblVideo1);
    var Image = document.getElementById(tblImage);
    var PreviewImageTable = document.getElementById(tblPreviewImage);

    var IndexValue = document.getElementById(media).selectedIndex;
    var SelectedVal = document.getElementById(media).options[IndexValue].value;
    var PreviewImage = document.getElementById(lblPreviewImageSize);
    var ImageVideo = document.getElementById(label);

    if (SelectedVal == 'video') {
        document.getElementById(label).innerHTML = "Video Size:" + size + " MB";
        document.getElementById(lblPreviewImageSize).innerHTML = "Image Size:" + width + " x " + height;
        Video1.style.display = 'block';
        PreviewImageTable.style.display = 'block';
        PreviewImage.style.display = 'block';
        Image.style.display = 'none';
        ImageVideo.style.display = 'block';

    }
    else if (SelectedVal == 'image') {

        Video1.style.display = 'none';
        PreviewImage.style.display = 'none';
        Image.style.display = 'block';
        PreviewImageTable.style.display = 'none';
        document.getElementById(label).display = 'none';
        ImageVideo.style.display = 'none';
        document.getElementById(lblImageSize).innerHTML = "Image Size:" + width + " x " + height;

    }
}



function ShowHideRows(tableRow1, tableRow2, tableRow3) {
    document.getElementById(tableRow1).style.display = 'block';
    document.getElementById(tableRow2).style.display = 'none';
    document.getElementById(tableRow3).style.display = 'none';
}
