//Author:Nagarro
//Added On: 05/27/2011
//Purpose:Image\Video Light Box Overlay

//Modified On: 07/13/2011
//Purpose:lightbox faded Image's width should always be maximum screen size and container's position should set automatically according to window width
//Modified On: 07/18/2011
//Purpose:In Case of ribbon enables make the body scroll visible/hidden at lightbox creation/dismissal . 


//Author:PR
//Modified On: 07/26/2011
//Purpose:Image\Video Light Box Overlay Restructured 
//Modified On: 08/20/2011
//Purpose:webm video tag is added only when it is present in video library 

function OpenLightbox(dataString, obj) {
    //VideoJS.setupAllWhenReady();
    var pageNumber = $(obj).attr('number');
    var filename = $(obj).attr('download');
    var data = new Array();
    var temp = new Array();
    dataString = unescape(dataString);
    temp = dataString.split('#$&*');
    var count = Math.floor(temp.length / 6);
    for (var i = 0, j = 0; j < count; j++, i = i + 6) {
        data.push({
            'url': temp[i],
            //Replacing all occurrences of escaped single quote back to '
            'title': temp[i + 1],
            //Replacing all occurrences of escaped single quote back to '
            'caption': temp[i + 2],
            'video': temp[i + 3],
            'defaultVideo': temp[i + 4],
            'isVideo': temp[i + 5]
        });
    }
    var slideshow = new FD.SlideShow(data);
    var lightbox = new FD.Lightbox(slideshow);

    if (!(typeof (pageNumber) == "undefined")) {
        lightbox.pop(pageNumber - 1);
        if (!(typeof (filename) == "undefined")) {
            // Add download link in lightbox as mentioned in resource file
            if (typeof (lblMediaLibraryDownloadLink) != "undefined") {
                $('.slideshow').append("<a class='lightboxDownloadLink download' href='" + filename + "'>" + lblMediaLibraryDownloadLink + "</a>");
            }
            else {
                $('.slideshow').append("<a title='Download Slideshow' class='lightboxDownloadLink download' href='" + filename + "'>i</a>");
            }
        }
    } else {
        lightbox.pop();
    }

    //Author :PR
    //Modified On: 10/10/2011
    //fd_lightbox width should always be equal screen size ie maximum window width
    //In case of mobile device it should equal to document width
    if (mobileDevice) {
        $('.fd_lightbox').width(document.width);
        $('.fd_lightbox').height(document.height);
    }
    else {
        $('.fd_lightbox').width(screen.width);
    }

    //Check if the ribbon is visible
    if ($('#s4-ribbonrow').css('display') == 'block') {
        //Make the body scrollbar visible for IE only
        if ($.browser.msie) {
            $('#s4-workspace').attr('id', 's4-new');
            $('#s4-new').css('overflow', 'visible');
            $('body').css('overflow', 'visible');
            $('html').css('overflow', 'auto');
        }
    }
}


// use FD as namespace
// defined it in case it does not exist
(function (undefined) {

    this.FD = this.FD || {};

}).call(window);

// Class SlideShow
// Interfaces for working with Lightbox
// required: prepareToRender, getUI
// optional: getOuterWidth, getOuterHeight;

(function (undefined) {

    var fn;

    //  Constructor
    this.SlideShow = function (data, config) {
        this._$slideshow = null;
        this._$numbers = null;
        this._$images = null;
        this._$caption = null;
        this._$title = null;
        this._$video = null;
        this._$isVideo = null;
        this._data = data;
        this._curIdx = -1;
        if (config == undefined) {
            config = {};
        }
        this._config = config;
        this._fadeSpeed = config.fadeSpeed || 'slow';
        this._fadeTimeout = config.fadeTimeout || 500;
        this._numOfVisiblePagers = config.numOfVisiblePagers || 8;
        this._outerWidth = config.outerWidth || 658;
        this._outerHeight = config.outerHeight || 488;

        this._buildUI();
        this._registerEventListeners();
    };

    // shorthand for SlideShow prototype
    fn = this.SlideShow.prototype;

    // return UI component for LightBox to contain
    fn.getUI = function () {
        return this._$slideshow;
    };

    // prepare UI state for Lightbox to render 
    fn.prepareToRender = function (idx) {
        if( typeof(idx) == "undefined"){
            this.setCurrentIndex(0);
        } else {
            this.setCurrentIndex(idx);
        }
    };

    // return the outer width of SlideShow
    fn.getOuterWidth = function () {
        return this._outerWidth;
    };

    // return the outer height of SlideShow
    fn.getOuterHeight = function () {
        return this._outerHeight;
    }

    // setter of the current index of Slides
    // call event handler when the index is changed
    fn.setCurrentIndex = function (idx) {
        var ss = this;
        var data = ss._data;
        var oldIdx;

        // coerce the boundary
        if (idx < 0) {
            idx = 0;
        } else if (idx > data.length - 1) {
            idx = data.length - 1;
        }

        if (ss._curIdx == idx) return;

        oldIdx = ss._curIdx;
        ss._curIdx = idx;
        ss._onCurIdxChanged(oldIdx, idx);
    };

    // event handler when current index changes
    fn._onCurIdxChanged = function (oldIdx, newIdx) {
        var ss = this;

        ss._updateUI(oldIdx, newIdx);
    };

    // build SlideShow UI
    fn._buildUI = function () {
        var ss = this;
        var i;
        var imgHtml = "";
        var numHtml = "";

        for (i = 0; i < ss._data.length; i += 1) {

            if (ss._data[i].isVideo == 'true') {

                imgHtml += "<div class='video-js-box' style='display : none;'>";
                /* In case of no preview image (as preview image is non mandatory now)*/
                if (ss._data[i].url == '/image' || ss._data[i].url == ' ') {
                    imgHtml += PrepareVideoHtml(ss._data[i].video, '', ss._data[i].defaultVideo);
                }
                else {
                    imgHtml += PrepareVideoHtml(ss._data[i].video, ss._data[i].url, ss._data[i].defaultVideo);
                }
                imgHtml += "</div>";
            }
            else {
                imgHtml += '<img src="' + ss._data[i].url + '" style="z-index:1;"/>';
            }
            numHtml += '<li><a href="">' + (i + 1) + '</a></li>';
            if (ss._data.length == 1) {
                numHtml = "";
            }
        }

        var html = '<div class="slideshow"><p class="title"></p>'
             + '<div class="photos">'
             + imgHtml
             + '</div><p class="caption"><span class="caption_text"></span></p>'
             + '<ul class="pagers">'
             + '<li class="nav previous"><a id="previousNav" href="">«</a></li>'
             + '<li class="numbers"><ul>'
             + numHtml
             + '</ul></li><li class="nav next"><a id="nextNav" href="">»</a></li></ul>'
             + '<div class="overlay previous" style="z-index:1;"><div class="image"></div></div>'
             + '<div class="overlay next" style="z-index:1;"><div class="image"></div></div>';
+'</div>';
        var $content = $(html);

        this._$slideshow = $content;
        this._$numbers = $content.find('li.numbers');
        this._$caption = $content.find('span.caption_text');
        this._$title = $content.find('p.title');
        this._$images = $content.find('div.photos img');
        this._$video = $content.find('div.photos div');
    };

    // update SlideShow UI based on old slide index and new slide index
    fn._updateUI = function (oldIdx, newIdx) {

        var ss = this;
        //Author :PR
        //Modified On: 11/07/2011
        //on updating lightbox slide vides shuls be recereated
        //In case of mobile device behaviour is same as in case of IE browser
        if (mobileDevice) {
            try {
                var photos = $(".photos").first();
                var imgHtml = '';
                for (i = 0; i < ss._data.length; i += 1) {
                    if (ss._data[i].isVideo == 'true') {

                        if (i == newIdx) {
                            imgHtml += "<div class='video-js-box'>";
                        }
                        else {
                            imgHtml += "<div class='video-js-box' style='display : none;'>";
                        }

                        if (ss._data[i].url == '/image' || ss._data[i].url == ' ') {
                            imgHtml += PrepareVideoHtml(ss._data[i].video, '', ss._data[i].defaultVideo);
                        }
                        else {
                            imgHtml += PrepareVideoHtml(ss._data[i].video, ss._data[i].url, ss._data[i].defaultVideo);
                        }

                        imgHtml += "</div>";
                    }
                }

                $(".video-js-box").each(function () {
                    $(this).remove();
                });

                photos.append(imgHtml);

            }
            catch (e) {
                //do nothing
            }
        } else
        //for non ie browsers
        //description: for non-ie browsers source is rendered instead of flowplayer
        // so recreating the video tags.
            if (!$.browser.msie) {
                //fix: for video stop while navigation
                $(".video-js-box").each(function () {
                    try {
                        var div = $(this);
                        var video = div.children("video").first();

                        video.remove();
                        div.append(video);
                    }
                    catch (e) {
                        //do nothing
                    }
                });
            } //end if for non-ie
            //for ie
            //description: for ie flowplayer is rendered instead of source
            // so recreating the photos content.
            else {
                //photos class on div which contains all the slide shows
                var photos = $(".photos").first();
                var imgHtml = '';
                for (i = 0; i < ss._data.length; i += 1) {
                    if (ss._data[i].isVideo == 'true') {

                        if (i == newIdx) {
                            imgHtml += "<div class='video-js-box'>";
                        }
                        else {
                            imgHtml += "<div class='video-js-box' style='display : none;'>";
                        }

                        if (ss._data[i].url == '/image' || ss._data[i].url == ' ') {
                            imgHtml += PrepareVideoHtml(ss._data[i].video, '', ss._data[i].defaultVideo);
                        }
                        else {
                            imgHtml += PrepareVideoHtml(ss._data[i].video, ss._data[i].url, ss._data[i].defaultVideo);
                        }

                        imgHtml += "</div>";
                    }
                }

                $(".video-js-box").each(function () {
                    $(this).remove();
                });

                photos.append(imgHtml);
            }

        var $ns = ss._$numbers.find('li');
        var item = ss._data[newIdx];
        var oldItem = ss._data[oldIdx];
        var fadeDelay = 0;
        var fadeSpeed = 0;

        if (oldIdx > -1) {
            $ns.eq(oldIdx).removeClass('selected');

            var tempOldIndex = -1;

            for (i = 0; i <= oldIdx; i++) {
                if (ss._data[i].isVideo == oldItem.isVideo) {
                    tempOldIndex += 1;
                }
            }



            if (oldItem.isVideo == 'true') {

                ss._$video.eq(tempOldIndex).fadeOut(ss._fadeSpeed);
            }
            else {
                ss._$images.eq(tempOldIndex).fadeOut(ss._fadeSpeed);
            }

            fadeDelay = ss._fadeTimeout;
            fadeSpeed = ss._fadeSpeed;

            var tempNewIndex = -1;

            for (i = 0; i <= newIdx; i++) {
                if (ss._data[i].isVideo == item.isVideo) {
                    tempNewIndex += 1;
                }
            }

            if (item.isVideo == 'true') {
                ss._$video.eq(tempNewIndex).delay(fadeDelay).fadeIn(fadeSpeed);
            }
            else {
                ss._$images.eq(tempNewIndex).delay(fadeDelay).fadeIn(fadeSpeed);
            }
        }
        else {
            if (item.isVideo == 'true') {
                ss._$video.eq(newIdx).delay(fadeDelay).fadeIn(fadeSpeed);
            }
            else {
                ss._$images.eq(newIdx).delay(fadeDelay).fadeIn(fadeSpeed);
            }
        }
        $ns.eq(newIdx).addClass('selected');
        ss._$title.html(item.title);
        if (item.caption != undefined && item.caption != null && item.caption.length > 0) {
            ss._$caption.text(item.caption);
        }

        ss._updatePagers(newIdx);

        ss._$slideshow.find('div.overlay').css('visibility', 'visible');
        ss._$slideshow.find('http://www.elcompanies.com/Style Library/EsteeLauder/javascripts/LightBox/li.nav').removeClass('disabled');
        if (newIdx <= 0) {
            ss._$slideshow.find('div.overlay.previous').css('visibility', 'hidden');
            ss._$slideshow.find('li.nav.previous').addClass('disabled');
        }
        if (newIdx >= ss._data.length - 1) {
            ss._$slideshow.find('http://www.elcompanies.com/Style Library/EsteeLauder/javascripts/LightBox/div.overlay.next').css('visibility', 'hidden');
            ss._$slideshow.find('http://www.elcompanies.com/Style Library/EsteeLauder/javascripts/LightBox/li.nav.next').addClass('disabled');
        }
    };

    // get the visible range of the pager
    fn._getVisibleRange = function (curIdx) {
        var total_num = this._data.length;
        var visible_num = this._numOfVisiblePagers;
        var min_most_index;
        var range = { 'min': 0, 'max': 0 };
        var tmp_min = 0;

        if (total_num <= visible_num) {
            range.min = 0;
            range.max = total_num - 1;
        } else {
            min_most_index = total_num - visible_num;

            if (curIdx <= min_most_index) {
                range.min = curIdx;
                range.max = curIdx + visible_num - 1;
            } else {
                range.min = min_most_index;
                range.max = total_num - 1;
            }
        }

        return range;
    };

    // update pager UI based on current slide index
    fn._updatePagers = function (curIdx) {
        var ss = this;
        var $pagers = ss._$numbers.find('li');
        var i;
        var range = ss._getVisibleRange(curIdx);

        for (i = 0; i < ss._data.length; i++) {
            if (i >= range.min && i <= range.max) {
                $pagers.eq(i).addClass('visible');
            } else {
                $pagers.eq(i).removeClass('visible');
            }
        }
    };

    // register event listeners of the inner UI controls
    fn._registerEventListeners = function () {
        var ss = this;

        ss._$slideshow.find('http://www.elcompanies.com/Style Library/EsteeLauder/javascripts/LightBox/li.nav').each(function (idx, el) {
            var $nav = $(el);
            if ($nav.hasClass('previous')) {
                $nav.click(function (evt) {
                    evt.preventDefault();
                    ss._previous();
                });
            } else if ($nav.hasClass('next')) {
                $nav.click(function (evt) {
                    evt.preventDefault();
                    ss._next();
                });
            }
        });

        ss._$numbers.delegate('li', 'click', function (evt) {
            evt.preventDefault();
            var $number = $(this);
            var i = $number.index();
            ss.setCurrentIndex(i);
        });

        ss._$slideshow.find('div.overlay').each(function (idx, el) {
            var $overlay = $(el);
            var $img = $overlay.find('div.image');

            //Author :PR
            //Modified On: 11/07/2011
            //Navigation arrows should be visible always in case of mobile. No hover event is attached
            if (mobileDevice) {
				$img.css('display','block');
		    } else {
                $overlay.hover(function (evt) {
                    $img.fadeIn();
                }, function (evt) {
                    $img.fadeOut();
                });
            }
            if ($overlay.hasClass('previous')) {
                $img.click(function (evt) {
                    evt.preventDefault();
                    ss._previous();
                });
                //Author :PR
                //Modified On: 11/07/2011
                //Navigation arrows Binding Touch event
                $img.bind('touch' , function (evt) {
                    evt.preventDefault();
                    ss._previous();
                });

            } else if ($overlay.hasClass('next')) {
                $img.click(function (evt) {
                    evt.preventDefault();
                    ss._next();
                });
                //Author :PR
                //Modified On: 11/07/2011
                //Navigation arrows Binding Touch event
                $img.bind('touch' , function (evt) {
                    evt.preventDefault();
                    ss._next();
                });

            }
        });
    }

    // increase the current slide index by 1
    fn._next = function () {
        var ss = this;
        ss.setCurrentIndex(ss._curIdx + 1);
    };

    // decrease the current slide index by 1
    fn._previous = function () {
        var ss = this;
        ss.setCurrentIndex(ss._curIdx - 1);
    };

}).call(window.FD);


// Class Lightbox
// Interfaces of content object
// required: prepareToRender, getUI
// optional: getOuterWidth, getOuterHeight;

(function (undefined) {

    var fn;

    // Constructor
    this.Lightbox = function (content) {
        this._$win = $(window);
        this._$lightbox = null;
        this._$container = null;
        this._$closeButton = null;
        this._content = content;

        this._width = content.getOuterWidth != undefined ? content.getOuterWidth() : 658;
        this._height = content.getOuterHeight != undefined ? content.getOuterHeight() : 488;

        this._buildUI();

        this._registerEventListeners();
    };

    // shorthand for Lightbox prototype
    fn = this.Lightbox.prototype;

    // pop up Lightbox
    fn.pop = function (idx) {
        var lb = this;
        lb._$lightbox.fadeIn('fast', function () {
            lb._centerContent();
            lb._content.prepareToRender(idx);
            lb._showContent();
            /* style to center middle align the hellip in case of IE */
            if ($.browser.msie) {
                document.getElementById("nextNav").style.cssText = 'margin-top: -1px;';
                document.getElementById("previousNav").style.cssText = 'margin-top: -1px;';
            }
        });
    };

    // dismiss Lightbox
    fn.dismiss = function () {
        //this._$lightbox.remove();
        /*
        On Click of close button, fadeOut() is used instead of remove() to avoid the javascript error.
        error:: 'null' is null or not an object. This error was coming because of id of flowplayer is null.
        */
        var lb = this;
        lb._$container.html('');
        //Check if the ribbon is visible
        if ($('#s4-ribbonrow').css('display') == 'block') {
            //Make the body scrollbar disable
            if ($.browser.msie) {
                $('#s4-new').attr('id', 's4-workspace');
                $('#s4-workspace').css('overflow', '');
                $('body').css('overflow', '');
                $('html').css('overflow', '');
            }
        }
        this._$lightbox.fadeOut('fast');
    };

    // build the Lightbox UI
    fn._buildUI = function () {
        var lb = this;

        var $b = $('body');
        var $lightbox = $('<div class="fd_lightbox"><div><a class="close" href="">close</a></div></div>');
        $b.append($lightbox);

        lb._$container = $lightbox.find('>div').css({
            'width': lb._width + 'px',
            'height': lb._height + 'px'
        });
        lb._$container.append(lb._content.getUI());
        lb._$lightbox = $lightbox;
        lb._$closeButton = $lightbox.find('a.close');
    };

    // register event handlers of Lightbox inner UI controls
    fn._registerEventListeners = function () {
        var lb = this;

        lb._$lightbox.click(function (evt) {
            var target = evt.target;
            if ($(target).hasClass('fd_lightbox')) {
                lb.dismiss();
            }
        });
        //Author :PR
        //Modified On: 11/07/2011
        //Lightbox Background : Binding Touch event
        lb._$lightbox.bind('touchstart', function (evt) {
            var target = evt.target;
            if ($(target).hasClass('fd_lightbox')) {
                lb.dismiss();
            }
        });

        lb._$closeButton.click(function (evt) {

            evt.preventDefault();
            lb.dismiss();
        });

        //Author :PR
        //Modified On: 11/07/2011
        //Window resize event should call in case of non-mobile devices only
        if (!mobileDevice) {
            lb._$win.resize(function () {
                lb._centerContent();
            }).scroll(function () {
                lb._centerContent();
            });
        }
    }

    // center the lightbox on the screen
    fn._centerContent = function () {
        var $container = this._$container;
        var $win = this._$win;
        var top = ($win.height() - $container.outerHeight()) / 2 + $win.scrollTop();
        // calculating lightbox's position with respect to window size
        var left = ($win.width() - $container.outerWidth()) / 2;
        if (mobileDevice) {
            $container.css({
                'top': top + 'px',
                'left': left + 'px'
            });
        } else {
            $container.css({
                'top': 10 + '%',
                'left': left + 'px'
            });
        }
    };

    fn._showContent = function () {
        this._$container.show();
    };

}).call(window.FD);


//Author:PR
//Added On: 07/26/2011
//Purpose:Image\Video Light Box Overlay Restructured 
//Modified On: 08/20/2011
//Purpose:webm Video tag is added if default video is webm 


//Prepare all different formats for video
function PrepareVideoSource(source, defaultVideo) {

    var videoSourceHtml = "";
    if (defaultVideo == "webm") {
        videoSourceHtml += "<source src=\"" + source + ".webm\" ";
        videoSourceHtml += "type='video/webm; codecs=&quot;vp8, vorbis&quot;' />";
    }
    videoSourceHtml += "<source src=\"" + source + ".mp4\" ";
    videoSourceHtml += "type='video/mp4; codecs=&quot;avc1.42E01E, mp4a.40.2&quot;' />";
    videoSourceHtml += "<source src=\"" + source + ".mov\" ";
    videoSourceHtml += "type='video/quicktime;' />";

    return videoSourceHtml;
}


//Prepare all different formats for flash
function PrepareFlashSource(source, preview) {
    var flashSource = "";
    if (mobileDevice) {
        width = 618;
    }
    else {
        width = 658;
    }
    flashSource += "<object id='flowplayer' width='" + width + "' height='372' data='../../../../_Layouts/EsteeVideoPlayer/SWF/flowplayer.commercial-3.2.6.swf'/*tpa=http://www.elcompanies.com/_Layouts/EsteeVideoPlayer/SWF/flowplayer.commercial-3.2.6.swf*/ type='application/x-shockwave-flash'>";
    flashSource += "<param name='movie' value='../../../../_Layouts/EsteeVideoPlayer/SWF/flowplayer.commercial-3.2.6.swf'/*tpa=http://www.elcompanies.com/_Layouts/EsteeVideoPlayer/SWF/flowplayer.commercial-3.2.6.swf*/ />";
    flashSource += "<param name='allowfullscreen' value='true' /><param name='wmode' value='opaque' />";

    if (typeof (flowPlayerKey) != "undefined") {
        flashSource += "<param name='flashvars' value=\"config={'key':'" + flowPlayerKey + "',";    
    }
    else {
        flashSource += "<param name='flashvars' value=\"config={'key':'#$2becf3ffa032bb00774',";
    }
    
    var encodeSourceUrl = source.replace("'", "\\'");
    if (preview == '') {
        flashSource += "'clip':{";
        flashSource += "'url':'" + encodeSourceUrl + ".mp4'";
        flashSource += ",'autoPlay': false,'autoBuffering': true}}\" />";
    } else {
        flashSource += "'playlist':[{";
        flashSource += "'url':'" + preview + "'},{'url':'" + encodeSourceUrl + ".mp4'";
        flashSource += ",'autoPlay': false,'autoBuffering': true}]}\" />";
    }
    flashSource += "</object>";

    return flashSource;
}

//Prepare Html for lightbox
function PrepareVideoHtml(source, preview, defaultVideo) {

    //Prepare all different formats for video
    var videoSourceHtml = PrepareVideoSource(source, defaultVideo);
    //Prepare flash source
    var flashSource = PrepareFlashSource(source, preview);

    var videoeHtml = "";

    var userAgent = navigator.userAgent.toLowerCase();
    if ((userAgent.indexOf("android") != -1)) {
        videoeHtml += flashSource;
    }
    else
    // If Browser is Firefox with lesser version than 4.0 (Gecko version 2.0)
    if ($.browser.mozilla && $.browser.version.slice(0, 3) < 2.0) {
        videoeHtml += flashSource;
    } else {
    var width;
        if (mobileDevice) {
            width = 618;
        }
        else {
            width = 658;
        }
        videoeHtml += "<video id='example_video_1' class='video-js' width='"+ width +"' height='372' controls='controls'";
        if (preview == '') {
            videoeHtml += "preload='auto'>";
        } else {
            videoeHtml += "preload='none' ";
            videoeHtml += "poster='" + preview + "'>";
        }
        videoeHtml += videoSourceHtml;
        videoeHtml += flashSource;
        videoeHtml += "</video>";
    } return videoeHtml;
}
