//=================================================================================================

var request = 0;
var requestComplete = 0;
var enableBtn = true;
//Enable button for multiple item
function enableForAll() {
    request = 1;
    if (requestComplete == 1) 
    {
        request = 0;
        requestComplete = 0;
        if (enableBtn == false) {
            var items = SP.ListOperation.Selection.getSelectedItems();
            var itemCount = CountDictionary(items);
            return (itemCount > 0);
        }
        return enableBtn;
    }
    else {
        var context = SP.ClientContext.get_current();
        this.site = context.get_site();
        this.web = context.get_web();
        var list = web.get_lists().getByTitle('MediaKit Library');
        var camlQuery = new SP.CamlQuery();
        camlQuery.set_viewXml('<View Scope=\'RecursiveAll\'><Query><Where><Eq><FieldRef Name=\'IsActive\' /><Value Type=\'Boolean\'>1</Value></Eq></Where></Query></View>');
        this.listItems = list.getItems(camlQuery);
        context.load(listItems);
        context.executeQueryAsync(ReadListItemSucceeded, ReadListItemFailed);
    }
}

function ReadListItemSucceeded(sender, args) {
    requestComplete = 1;
    if (listItems.get_count() > 0) {
        enableBtn = true;
    }
    else enableBtn = false;

    RefreshCommandUI();
}

function ReadListItemFailed(sender, args) {
    request = 0;
    requestComplete = 0;
}

//Enable button for single item
function enableForSingle() {
    return (SP.ListOperation.Selection.getSelectedItems().length == 1);
}

function downloadZip() {
    var context = SP.ClientContext.get_current();
    this.site = context.get_site();
    this.web = context.get_web();
    context.load(this.site);
    context.load(this.web);
    context.executeQueryAsync(
        Function.createDelegate(this, this.onQuerySucceeded),
        Function.createDelegate(this, this.onQueryFailed)
    );
}

function onQuerySucceeded() {
    var listId = SP.ListOperation.Selection.getSelectedList();
    var items = SP.ListOperation.Selection.getSelectedItems();
    var itemCount = CountDictionary(items);
    
    var ids = "";
    for (var i = 0; i < itemCount; i++) {
        ids += items[i].id + ";";
    }

    //send a request to the zip aspx page.
    var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", this.site.get_url() + this.web.get_serverRelativeUrl() + "_layouts/Estee.Corporate.Web/UploadZipFile.aspx");

    var hfSourceUrl = document.createElement("input");
    hfSourceUrl.setAttribute("type", "hidden");
    hfSourceUrl.setAttribute("name", "sourceUrl");
    hfSourceUrl.setAttribute("value", location.href);
    form.appendChild(hfSourceUrl);

    var hfListId = document.createElement("input");
    hfListId.setAttribute("type", "hidden");
    hfListId.setAttribute("name", "listId");
    hfListId.setAttribute("value", listId);
    form.appendChild(hfListId);

    var hfItemIds = document.createElement("input")
    hfItemIds.setAttribute("type", "hidden");
    hfItemIds.setAttribute("name", "itemIDs");
    hfItemIds.setAttribute("value", ids);
    form.appendChild(hfItemIds);

    document.body.appendChild(form);
    form.submit();
    //SP.UI.Notify.addNotification('Operation Successful!');
}

function onQueryFailed(sender, args) {
    this.statusID = SP.UI.Status.addStatus("Download as Zip:",
        "Downloading Failed: " + args.get_message() + " <a href='#' onclick='javascript:closeStatus();return false;'>Close</a>.", true);
    SP.UI.Status.setStatusPriColor(this.statusID, "red");
}

function closeStatus() {
    SP.UI.Status.removeStatus(this.statusID);
}