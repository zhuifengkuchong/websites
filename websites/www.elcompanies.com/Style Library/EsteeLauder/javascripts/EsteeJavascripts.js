//Author: PR
//Added On: 11/07/2011
//Purpose: Global variable to detect mobile devices
var mobileDevice = false;

//Author:PR
//Added On: 06/04/2011
//Purpose: Footer Brands
//Setting the width of footer brands
function SetWidthFooterBrands(width) {

    //Set the Maximum width found to each column
    $('.footer .footer_extra  .footerBrands ul li').each(function () {
        var pos = $(this).index();
        $(this).width(width[pos]);
    });
}


//Author:PR
//Added On: 06/04/2011
//Purpose: Character Count
// Script Used to show character count 
//Author:PR
//Modified On: 08/17/2011
//Purpose: Character Update character count after text box value gets change

function CheckCount(elementVariable, words, classSelector)
//Detect keypress in the textarea
{
    var text_area_box = $(elementVariable).val(); //Get the values in the textarea
    var max_numb_of_words = words; //Set the Maximum Number of characters
    var main = text_area_box.length * 100; //Multiply the lenght on words x 100
    var value = (main / max_numb_of_words); //Divide it by the Max numb of words previously declared
    var count = max_numb_of_words - text_area_box.length; //Get Count of remaining characters
    //To make sure count is always positive
    if (count < 0) {
        count = 0;
    }
    if (text_area_box.length > max_numb_of_words) {
        //Remove the excess words using substring
        var remove_excess_characters = text_area_box.substr(0, max_numb_of_words);
        $(elementVariable).val(remove_excess_characters);
    }
    //Update the charecter count value
    var elem = $(elementVariable).parent().parent().parent().parent().find(classSelector);
    if (typeof (lblCharacterCount) != "undefined") {
        $(elem).html("<table style='float:right;'><tr><td>" + lblCharacterCount + " </td><td style='width:21px; text-align:right;'>" + count + '</td><td>/</td><td>' + max_numb_of_words + '</td></tr></table>'); //Output the count variable previously calculated into the div with id= count
    }
    else {
        $(elem).html("<table style='float:right;'><tr><td>" + "Character Remaining : " + " </td><td style='width:21px; text-align:right;'>" + count + '</td><td>/</td><td>' + max_numb_of_words + '</td></tr></table>'); //Output the count variable previously calculated into the div with id= count
    }

    return false;

}

//Modified On: 08/17/2011
//Purpose: Added the Paste event
function SetCountRemainingOnEdit() {

    $('.count').each(function () {
        var limit = $(this).attr('limit');
        var divParent = $(this).parent();
        var divInput = $(divParent).find('div div span input');
        var divTextarea = $(divParent).find('div div span textarea');
        var classSelector = '.count';

        if (divInput.length != 0) {
            CheckCount(divInput, limit, classSelector);
            $(divInput).keypress(function () {
                CheckCount(this, limit, classSelector);
            });
            $(divInput).keyup(function () {
                CheckCount(this, limit, classSelector);
            });
            $(divInput).change(function () {
                CheckCount(this, limit, classSelector);
            });
            $(divInput).bind('paste', function (e) {
                //Wait till the text gets pasted into the box
                setTimeout("ActionComplete('" + this.id + "')", 250);
            });
            $(divInput).bind('cut', function (e) {
                //Wait till the text gets cut from the box
                setTimeout("ActionComplete('" + this.id + "')", 250);
            });
        }
        if (divTextarea.length != 0) {
            CheckCount(divTextarea, limit, classSelector);
            $(divTextarea).keypress(function () {
                CheckCount(this, limit, classSelector);
            });
            $(divTextarea).keyup(function () {
                CheckCount(this, limit, classSelector);
            });
            $(divTextarea).change(function () {
                CheckCount(this, limit, classSelector);
            });
            $(divTextarea).bind('paste', function (e) {
                //Wait till the text gets pasted into the box
                setTimeout("ActionComplete('" + this.id + "')", 250);
            });
            $(divTextarea).bind('cut', function (e) {
                //Wait till the text gets cut from the box
                setTimeout("ActionComplete('" + this.id + "')", 250);
            });
        }
    });

    //Modified On: 08/17/2011
    //Purpose: Added the Paste event With URL Description field
    $('.countUrl').each(function () {
        var limit = $(this).attr('limit');
        var divParent = $(this).parent();
        var divInput = $(divParent).find('div div span input').get(1);
        var classSelector = '.countUrl';

        if (divInput.length != 0) {
            CheckCount(divInput, limit, classSelector);
            $(divInput).keypress(function () {
                CheckCount(this, limit, classSelector);
            });
            $(divInput).keyup(function () {
                CheckCount(this, limit, classSelector);
            });
            $(divInput).change(function () {
                CheckCount(this, limit, classSelector);
            });
            $(divInput).bind('paste', function (e) {
                //Wait till the text gets pasted into the box
                setTimeout("ActionComplete('" + this.id + "')", 250);
            });
            $(divInput).bind('cut', function (e) {
                //Wait till the text gets cut from the box
                setTimeout("ActionComplete('" + this.id + "')", 250);
            });
        }
    });
}

$(document).ready(function ()//When the dom is ready  
{
    SetBrandsOnMaster();
    SetCountRemainingOnEdit();
});


//Author:ND
//Added On: 04/15/2011
//Purpose:Left Navigation Topics Syncup 
//Left Nav Sync up start

//sychronizes the index
function SetIndex(index, returnable, callClick) {
    try {
        if (window["isSocietyPage"] != undefined && isSocietyPage == true) {
            var indexedObject = ".tab_nav > ul > li:nth-child(" + parseInt(index + 1) + ")";

            //remove the class from L2 navigation
            $(".content_menu.nav > ul > li > h3 > a.selected:first-child").removeClass("selected");

            //
            $(".content_menu.nav > ul > li > ul > li > a.selected:first-child").removeClass("selected");

            //add the selected style to left navigation topic
            $(".content_menu.nav > ul > li > ul > li:nth-child(" + parseInt(index + 1) + ")").find("a").addClass("selected");

            //if clicked through left navigation then call the click event of tabs
            if (callClick) {
                $(indexedObject).click();
            }
        }
    }
    catch (e) { }

    //check if it is called through left navigation then only return false else don't return
    if (returnable) {
        return false;
    }
}

//appends the topics in appropriate HTML
function AppendNavigation() {
    var submenu = '';
    //for each element set click event
    $(".tab_nav >ul>li").each(
                            function (index) {
                                submenu += "<li><a href='#' onclick='javascript:return SetIndex(" + index + ", true, true);'>" + $(this).find("a").html().replace(/<br>/ig, " ") + "</a></li>";
                            });

    $(".nav > ul >li > h3 > a.selected:first-child").each(
            function (index) {
                //if current page already contains children then append before other children
                if ($(this).parent().parent().find("ul").html() != null && submenu != '') {
                    $(this).parent().parent().find("ul>li").before(submenu);
                }
                //else create ul tag and append the children
                else if (submenu != '') {
                    submenu = "<ul>" + submenu + "</ul>";
                    $(this).parent().parent().append(submenu);
                }
                //initialize to first tab
                SetIndex(0, false, false);
            }
        );
}
//Left Nav Sync up end

//Author:ND
//Added On: 04/18/2011
//Purpose: Download Icon events sync up
//Download Icon events sync up start

$(document).ready(function () {
    $(".estee-Download-Image").hover(function (event) {
        $(this).css({
            'background-image': 'url("../images/icon_download_hover.png"/*tpa=http://www.elcompanies.com/Style Library/EsteeLauder/images/icon_download_hover.png*/)',
            'background-position': 'top left',
            'background-repeat': 'no-repeat',
            'width': '20px',
            'height': '16px',
            'cursor': 'pointer'
        });

        var downloadAnchor = $(this).parent().find(".estee-Download-Text:first a");
        downloadAnchor.css({ 'color': '#f8ec92' });
    }, function (event) {
        $(this).css({
            'background-image': 'url("../images/icon_download.png"/*tpa=http://www.elcompanies.com/Style Library/EsteeLauder/images/icon_download.png*/)',
            'background-position': 'top left',
            'background-repeat': 'no-repeat',
            'width': '20px',
            'height': '16px',
            'cursor': 'pointer'
        });
        var downloadAnchor = $(this).parent().find(".estee-Download-Text:first a");
        downloadAnchor.css({ 'color': '#f8efca' });
    });

    $(".estee-Download-Image").click(function (event) {
        var downloadAnchor = $(this).parent().find(".estee-Download-Text:first a");
        window.open(downloadAnchor.attr("href"));
    });

    $(".estee-Download-Text:first a").hover(function (event) {
        var downloadImage = $(this).parent().parent().find(".estee-Download-Image:first");
        downloadImage.mouseover();
    }, function (event) {
        var downloadImage = $(this).parent().parent().find(".estee-Download-Image:first");
        downloadImage.mouseout();
    });
    $(".estee-Download-Text:first a").attr("target", "_blank");
});

//Download Icon events sync up end


//Author:AT
//Added On: 04/26/2011
//Purpose: To open LightBox in Story Pages
//To open LightBox in Story Pages
function OpenLightboxold(dataString) {
    var data = new Array();
    var temp = new Array();
    temp = dataString.split('#$&*');
    var count = Math.floor(temp.length / 4);
    for (var i = 0, j = 0; j < count; j++, i = i + 4) {
        data.push({
            'url': temp[i],
            'title': temp[i + 1],
            'caption': temp[i + 2],
            'date': temp[i + 3]
        });
    }
    var slideshow = new FD.SlideShow(data);
    var lightbox = new FD.Lightbox(slideshow);
    lightbox.pop();
    $('.fd_lightbox').height($(document).height());
    $('.fd_lightbox').width($(document).width());
}

//Author:ND
//Added On: 04/26/2011
//Purpose: Asterix sign
//Add Asterix sign start
(function ($) {
    $.inArrayInsensitive = function (val, array) {
        var returnIndex = -1;
        $.each(array, function (index, indexValue) {
            if (indexValue != undefined && val.toLowerCase().trim() == indexValue.toLowerCase().trim()) {
                returnIndex = index;
                return true;
            };
        });
        return returnIndex;
    };
})(jQuery);

function AddAsterixToFields() {
    $(".ms-formfieldcontainer").each(function () {
        var fieldLabel = $(this).find(".ms-formfieldlabelcontainer > .ms-formfieldlabel:first");
        var fieldLabelText = "";

        if (fieldLabel.html() != null) {
            fieldLabelText = fieldLabel.html();
            if ($.inArrayInsensitive(fieldLabelText, requiredFields) >= 0) {
                var fieldValueControlContainer = $(this).find(".ms-formfieldlabelcontainer:first");
                $('<SPAN class="estee-Required">*</SPAN>').appendTo(fieldValueControlContainer);
            }
        }
    });
}
//Add Asterix sign end


//Author:AT
//Added On: 05/30/2011
//Purpose: Converts UTC time to Client Time Zone
//Converts UTC time start
function ConvertUTCToLocalTime(displayFormat, date) {
    var dd = new Date(date + " UTC");
    document.write(dd.format(displayFormat));
} //Converts UTC time end


//Author:ND
//Added On: 06/14/2011
//Purpose: attaches download class start
function SetDownloadLinkProperties() {
    var downloadAnchor = $("#downloadContainer a");

    if (downloadAnchor.html() != null) {
        downloadAnchor.addClass("download");
        if (downloadAnchor.attr("target") != null) {
            downloadAnchor.attr("target", "_blank");
        }
    }
}

function AdjustClickHereToTest() {

    if ($("#estee-download-container .ms-formfieldcontainer:first >  .ms-formfieldvaluecontainer:first > span > .ms-formdescription > a") != null) {
        $("#estee-download-container .ms-formfieldcontainer:first >  .ms-formfieldvaluecontainer:first > span > .ms-formdescription > a").attr("target", "_blank");
        $("#estee-download-container .ms-formfieldcontainer:first >  .ms-formfieldvaluecontainer:first > span > .ms-formdescription > a").attr("href", "#");

        $("#estee-download-container .ms-formfieldcontainer:first > .ms-formfieldvaluecontainer:first > span > .ms-formdescription > a").click(function (e) {
            $("#estee-download-container .ms-formfieldcontainer:first >  .ms-formfieldvaluecontainer:first > span > .ms-formdescription > a").attr("target", "_blank");
            $("#estee-download-container .ms-formfieldcontainer:first >  .ms-formfieldvaluecontainer:first > span > .ms-formdescription > a").attr("href", "#");

            var inputUrl = $("#estee-download-container .ms-formfieldcontainer:first >  .ms-formfieldvaluecontainer:first > span > input:first");

            if (inputUrl != null) {
                inputUrl = inputUrl.val().trim().toLowerCase();
            }
            //var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/

            //validated the url format
            var regexp = /^(http|https|ftp):\/[\/]+\w[:]{0,1}[0-9]*[\/]*\w*?/
            if (inputUrl != null && inputUrl != "" && regexp.test(inputUrl)) {
                window.open(inputUrl);
            }

            e.preventDefault();
        });
    }
}

//download link End

//Author:PR
//Added On: 05/07/2011
//Purpose: Header Brands
//Setting the width of Header brands
function SetWidthHeaderBrands(width) {
    var totalColumn = 4;

    //Calculate total width of brand section
    var totalWidth = 0;
    for (i = 0; i < totalColumn; i++) {
        //10 % additional width than footer brands since font is in upper case
        width[i] += parseInt(width[i] * 0.10);
        totalWidth += width[i];
    }

    //Set the Maximum width found to each column
    $('.headerBrands ul li').each(function () {
        var pos = $(this).index();
        $(this).width(width[pos]);
    });

    //Additional width for container include (padding for container and margins 20*4 + 10 + 10)
    totalWidth += 100;
    $('.brandNavWidth').width(totalWidth);
    $('.headerBrands').width(totalWidth);
}

//Author:PR
//Added On: 06/07/2011
//Purpose: Brands On Master Page in Header/Footer
//Setting the width of Header/Footer brands

function SetBrandsOnMaster() {
    var width = new Array();
    var totalColumn = 4;

    //Initialize Array with width 0
    for (i = 0; i < totalColumn; i++) {
        width[i] = 0;
    }

    //Find the Maximum width of each brand column
    $('.footer .footer_extra  .footerBrands ul li').each(function () {
        var pos = $(this).index();
        if (width[pos] < $(this).width()) {
            width[pos] = $(this).width() + 4; //add additional 4 pixel for font rendering in MAC
        }
    });

    SetWidthFooterBrands(width);
    SetWidthHeaderBrands(width);
}

//Author:MA
//Added On: 07/14/2011
//Purpose: Handles Buzz section Date and Time display
function ConvertUTCToLocalBuzzTime(year, month, date, hours, minutes, seconds, ms, format, feedName) {
    var d = Date.UTC(year, month, date, hours, minutes, seconds, ms); //returns the milliseconds elapsed
    var localDateTime = (new Date(d)).toLocaleDateString();
    var dd = new Date(localDateTime);
    if (feedName.toString() == "PressRelease") {
        dd.setMonth(dd.getMonth() - 1);
    }

    var localTime = (new Date(d)).toLocaleTimeString();

    var Time = localTime.split(':');
    var hours = Time[0];
    var minutes = Time[1];
    var clockNotation = Time[2].split(" ")[1];
    if (format.toString() == "Date") {

        document.write(dd.format("MM/dd"));
    }
    else {
        if (format.toString() == "Time") {
            //Working for IE-7/8, FF and MAC 
            if (clockNotation != undefined) {
                if ((hours == 12) && (clockNotation == "AM"))
                    hours = hours - 12;

                document.write(hours + ":" + minutes + " " + clockNotation);

            }
            //In case of 24 hour format
            else {
                //Working for Safari and Chrome
                if (hours > 11) {
                    if (hours != 12)
                    { hours = hours - 12; }

                    document.write(hours + ":" + minutes + " ")
                    document.write("PM")
                }
                else {
                    //To display hours less than 10 as single digit and not as double as per VD
                    if (hours < 10) {
                        hours = hours.toString().charAt(1);
                    }
                    document.write(hours + ":" + minutes + " ")
                    document.write("AM")
                }
            }

        }
    }
}

function DisplayDate(dateTime) {
    document.write(dateTime.format("MM/dd"));
}

function DisplayTime(minutes, hours) {
    if (minutes < 10) {
        minutes = "0" + minutes
    }
    //document.write(hours + ":" + minutes + " ")
    if (hours > 11) {
        if (hours != 12)
        { hours = hours - 12; }
        document.write(hours + ":" + minutes + " ")
        document.write("PM")
    }
    else {
        document.write(hours + ":" + minutes + " ")
        document.write("AM")
    }
}

function TwitterWrapper(created_at, format) {
    var dateTime = new Date(getDateFromFormat(created_at.substring(4).replace("+0000 ", ""), "MMM dd HH:mm:ss yyyy"));
    var d = Date.UTC(dateTime.getYear(), dateTime.getMonth(), dateTime.getDate(), dateTime.getHours(), dateTime.getMinutes(), dateTime.getSeconds(), dateTime.getMilliseconds());
    var localDateTime = new Date(d);

    var hours = localDateTime.getHours();
    var minutes = localDateTime.getMinutes();
    if (format.toString() == "Date") {
        DisplayDate(localDateTime);
    }
    else {
        if (format.toString() == "Time") {
            DisplayTime(minutes, hours);
        }
    }
}

function FacebookWrapper(created_time, format) {
    /*JS the date constructor is like this:
    new Date(milliseconds)  (milliseconds since 1970/01/01)
    and createdTime is a unix-style seconds-since-1970.
    So multiply by 1000.*/
    var fbMilliSeconds = parseInt(created_time);
    var dateTime = new Date(fbMilliSeconds * 1000);


    var hours = dateTime.getHours();
    var minutes = dateTime.getMinutes();
    if (format.toString() == "Date") {
        DisplayDate(dateTime);
    }
    else {
        if (format.toString() == "Time") {
            DisplayTime(minutes, hours);
        }
    }

}

//Author: PR
//Added On: 07/22/2011
//Purpose: Typekit call to each master page
function LoadTypekit() {
    try {
        Typekit.load({
            active: function () {
                SetContentHeight();
                // As soon as the fonts are active, set height for related link for CR Landing Page
                SetCRRelatedLinkHeight();
                SetStorySectionHeight();
                SetEmployeeQuoteStyle();
            }
        });

    } catch (e) {
    }
}

//Author: MA
//Added On: 07/27/2011
//Purpose: Set Related link padding top which is equal to height difference between Associated Information Header height 
//and related height
function SetCRRelatedLinkHeight() {

    if ($("div.content:first").hasClass('CR-content')) {

        var padding = $('#buzzDisplay .header h1').height() - $(".related_links h3").height();
        if (padding != null) {
            $(".related_links h3").css("padding-top", 0);
            $(".related_links h3").css("padding-top", padding);
        }

    }

}

//Author: DB
//Added On: 10/31/2011
//Purpose: Sets the height of Meet Our com[any Brand Page: Story section 
function SetStorySectionHeight() {
        if($('.meet_our_company').length > 0)
        {
        var maxHeight = 0;
            $('.tab_carousel .person').each(function()
                {
                    var inlineStyle = this.style.display;
                    this.style.display = "block";
                    if (this.clientHeight > maxHeight)
                    {
                        maxHeight = this.clientHeight;
                    }
                    this.style.display= inlineStyle;
                }
            );
        if (maxHeight < 140) {
            $('#defaultDivider').css('display', 'none');
            $('.tab_carousel').css('height', '588px');
            $('.hiddenDivider').css('display', 'block');
        }
        else if (maxHeight > 160) {
            $('.tab_carousel').css('height', '630px');
            $('#defaultDivider').css('height', '170px');
        }
    }
}



//Author: DB
//Added On: 08/31/2011
//Purpose: Used on set minimum height according to left nav
function SetContentHeight() {
    var height = $('.content_menu').height();
    //For Non Brand Pages
    if (height != null) {
        $(".content").css("min-height", height);
    }
    else      //Brand Pages  
    {
        var brandmenu_height = ($('.brand_menu').height());
        if (brandmenu_height != null) {
            $(".content").css("min-height", brandmenu_height);
        }
    }

}
//Author: AT
//Added On: 08/03/2011
//Modified On: 08/09/2011 --> Added bindHoverImage() function
//Purpose: Used on ShareThis control to provide the functionality of mousein and mouseout

function showLoadPopup(id1) {
    document.getElementById(id1).style.display = 'block';
    var more = document.getElementById('more');
    if (more != null) {
        more.className = 'more_Icon more_IconSel';
    }
}
function hideLoadPopup(id2) {
    document.getElementById(id2).style.display = 'none';
    var more = document.getElementById('more');
    if (more != null) {
        more.className = 'more_Icon';
    }
}
function iconChange(item, normalImage, hoverImage) {
    $(item).hover(function () {
        $(item).css('background-image', "url('" + hoverImage + "')");
    }, function () {
        $(item).css('background-image', "url('" + normalImage + "')");
    });
}
function bindHoverImage() {
    $('.shareItem a').each(function () {
        var hoverImage = $(this).attr('hoverimg');
        var normImage = $(this).attr('normimg');
        iconChange(this, normImage, hoverImage);
    });
}

//Author: MA
//Added On: 08/09/2011
//Purpose: Sets associated header UI when there is no at a glance section
function AssociatedHeaderSectionUI() {
    if (($.browser.msie) && ($.browser.version == 7.0)) {
        $('.separator').css('margin-top', '20px');
    }

    $('.features').addClass('ataglance-removed');

}

//Author: PR
//Added On: 08/17/2011
//Purpose: Trigger the change event when Paste/Cut event occure
function ActionComplete(Id) {
    //Trigger the change event assosiated
    $('#' + Id).trigger('change');
}

//Author:PR
//Added On: 08/17/2011
//Purpose: Sitemap page
//Setting the height of Sitemap columns
function SetSitemapColumnsHeight() {
    var totalColumn = 5;
    var pos = 0;
    var height = new Array();
    var currentHeight;

    //Find the Maximum height of each Sitemap column
    $('.sitemap_col1').each(function () {
        currentHeight = $(this).height();
        //Increase the position for each new row (Each row has fixed number of columns[totalColumn] )
        if (($(this).index() % totalColumn) == 0) {
            pos = pos + 1;
            height[pos] = 0;
        }
        if (height[pos] <= currentHeight) {
            height[pos] = currentHeight + 4; //add additional 4 pixel for font rendering in MAC
        }
    });

    //Set the Maximum height found to each column
    pos = 0;
    $('.sitemap_col1').each(function () {
        if (($(this).index() % totalColumn) == 0) {
            pos = pos + 1;
        }
        $(this).height(height[pos]);
    });
}


//Author:AT
//Added On: 08/19/2011
//Used On: Press Release Story Page
//Increasing the width of Story content and Headline when there are no Related Links present
function IncreaseWidth() {
    if ($('.aside').is(':hidden')) {
        $('.article2').width('720px');
        $('.article2 .header h1').width('658px');
    }
}

//Author:AT
//Added On: 08/23/2011
//Used On: SpotlightWithRTE pages
//Increasing the height of Content section title, in case, if Spotlight Items are present
function AddHeightToContentSection() {
    if ($('.main_content .features ul').children().size() > 0) {
        $('.main_content .sectionTitle').css('padding-top', '20px');
    }
}

//Author:PR
//Added On: 08/31/2011
//Used On: HomePage Carousal
function PrepareCarousal() {
    try {
        $('#carousalList').prepend($('#carousalList li:last-child'));

        //Find if the Items Width is less than carousal width
        var carousalWidth = 0;
        $('#carousalList').find('li').each(function () {
            carousalWidth += $(this).outerWidth(true);
        });
        if ($('#carousalList').width() < carousalWidth) {
            //Make the carousal scroll
            $('#carousalList').carouFredSel({
                height: 'auto',
                scroll: {
                    fx: "scroll",
                    items: 1
                },
                prev: {
                    fx: "scroll", /*
                    items: 1,
                    button: '#prev2',*/
                    pauseDuration: 0,
                    pauseOnHover: false,
                    duration: 2000
                },
                next: {
                    fx: "scroll",
                    pauseDuration: 0,
                    pauseOnHover: false,
                    duration: 2000
                },
                auto: {
                    play: true,
                    pauseDuration: 0,
                    duration: 2000,
                    easing: 'linear'
                },
                items: {
                    start: 1
                }
            });
            $('.list_carousel').hover(function () {
                $('#carousalList').stop().trigger('pause');
            }, function () {
                $('#carousalList').trigger('play', true);
            });
            $('#prev2').click(function () {
                $("#carousalList").trigger("prev", 3);
                $('#carousalList').trigger('pause');
            });

            $('#next2').click(function () {
                var items = $("#carousalList").triggerHandler("currentVisible");
                var itemsWidth = 0;
                var itemNumber = 0;
                $(items).each(function () {
                    itemsWidth += $(this).outerWidth(true);
                    if (itemsWidth > 660) {
                        $("#carousalList").trigger("slideTo", [$(items[itemNumber]), "next"]);
                        return false;
                    }
                    itemNumber++;
                });
                $('#carousalList').trigger('pause');
            });
            $('.pagenp').hover(function (e) {
                $('#carousalList').trigger('pause');
                event.stopPropagation()
            }, function () {
            });
        }


    } catch (e) {
    }
}

function SetHomePageViewMode() {
    try {
        $('.news div > a').addClass('see_all');
        $('.news span > a').addClass('see_all');
        $('.insight div > a').addClass('see_all_content');
        $('.insight span > a').addClass('see_all_content');

        var minHeightNews = 0;
        var minHeightNewsHeader = 0;

        //Find the maximum News Header Height
        $('.template_0 .news > ul li').each(
                function () {
                    var currentHeaderHeight = $(this).find('.article .header h1').height();
                    if (currentHeaderHeight > minHeightNewsHeader) {
                        minHeightNewsHeader = currentHeaderHeight;
                    }
                }
            );
        //Set the maximum News Header Height
        $('.template_0 .news > ul li .article .header h1').css('height', minHeightNewsHeader + 'px');

        //Find the maximum News Section Height
        $('.template_0 .news > ul li').each(
                function () {
                    var currentHeight = $(this).height();
                    if (currentHeight > minHeightNews) {
                        minHeightNews = currentHeight;
                    }
                }
            );
        //Set the maximum News Section Height
        $('.template_0 .news > ul li').css('height', minHeightNews + 'px');

    } catch (e) {
    }
}

//Author:PR
//Added On: 09/01/2011
//Used On: HomePage Fix click here to test on home page
function FixClickHereToTest() {
    if ($(".downloadContainer .ms-formfieldcontainer:first >  .ms-formfieldvaluecontainer:first > span > .ms-formdescription > a") != null) {
        $(".downloadContainer .ms-formfieldcontainer:first >  .ms-formfieldvaluecontainer:first > span > .ms-formdescription > a").attr("target", "_blank");
        $(".downloadContainer .ms-formfieldcontainer:first >  .ms-formfieldvaluecontainer:first > span > .ms-formdescription > a").attr("href", "#");

        $(".downloadContainer .ms-formfieldcontainer:first > .ms-formfieldvaluecontainer:first > span > .ms-formdescription > a").click(function (e) {
            $(".downloadContainer .ms-formfieldcontainer:first >  .ms-formfieldvaluecontainer:first > span > .ms-formdescription > a").attr("target", "_blank");
            $(".downloadContainer .ms-formfieldcontainer:first >  .ms-formfieldvaluecontainer:first > span > .ms-formdescription > a").attr("href", "#");

            var inputUrl = $(".downloadContainer .ms-formfieldcontainer:first >  .ms-formfieldvaluecontainer:first > span > input:first");

            if (inputUrl != null) {
                inputUrl = inputUrl.val().trim().toLowerCase();
            }
            //var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/

            //validated the url format
            var regexp = /^(http|https|ftp):\/[\/]+\w[:]{0,1}[0-9]*[\/]*\w*?/
            if (inputUrl != null && inputUrl != "" && regexp.test(inputUrl)) {
                window.open(inputUrl);
            }

            e.preventDefault();
        });
    }
}

function SetMediaLibraryUI() {
    $('.boiler_plate div > a').addClass('download');
    var featuredItemSlides = $('.featured_media > ul li').size();

    switch (featuredItemSlides) {
        case 0:
            $('.featured_media').css('display', 'none');
            break;
        case 1:
            $('.featured_media .nav').css('display', 'none');
            $('.featured_media .header .entry_meta').css('display', 'none');

            var marginFromTop = 270 - $('.featured_media .imgDisc').height();
            $('.imgDisc').css('margin-top', marginFromTop + 'px');
            break;
        case 2:
            var rightPosition = '60px';
            $('.featured_media .nav .next').css('right', rightPosition);
            break;
        default:
            break;
    }
}

//Author:AT
//Added On: 10/17/2011
//Used On : Search Page Scripts

//Used to remove the border of last search results when results are less than page size
function RemoveLastResultBorder() {
    try {
        $('.entries div ul  li:last-child').last().addClass('noBorder');
    }
    catch (e) {
        //do nothing
    }
}

//Used to make border of last search result solid when results are more than page size
function SolidLastResultBorder() {
    try {
        $('.entries div ul  li:last-child').last().addClass('lastResultBorder');
    }
    catch (e) {
        //do nothing
    }
}

//Used to trigger the click event of Global Search Box Go button when enter key is pressed
function OnGlobalSearchKeyDown(event, strUrl, clientId) {
    try {
        //if key is enter key
        if (event.keyCode == 13) {

            //Stop the default Enter key behaviour

            //for non-ie browsers
            if (event.preventDefault) {
                event.preventDefault();
            }
            //for ie
            else {
                event.returnValue = false;
            }

            //trigger the click of GO button
            $("#globalSearchAnchor").trigger('click');
        }
    }
    catch (e) {
        //do nothing
    }
}

//Used to trigger the click event of Search Box Go button when enter key is pressed
function OnSearchKeyDown(event, strUrl, clientId) {
    try {
        //if key is enter key
        if (event.keyCode == 13) {
            //Purpose:
            //Stop the default Enter key behaviour

            //for non-ie browsers
            if (event.preventDefault) {
                event.preventDefault();
            }
            //for ie
            else {
                event.returnValue = false;
            }

            //trigger the click of GO button
            $("#searchAnchor").trigger('click');
        }
    }
    catch (e) {
        //do nothing
    }
}

function rtrimnew(str) {
    return str.replace(/\s+$/, '');
}

function trim(str, chars) {
    return ltrim(rtrim(str, chars), chars);
}

function ltrim(str, chars) {
    chars = chars || "\\s";
    return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
}

function rtrim(str, chars) {
    chars = chars || "\\s";
    return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
}

//Search Page Scripts

//Author: PR
//Added On: 11/07/2011
//Purpose: Common javascript function to call with every page
function LoadStyles() {

    LoadTypekit();
    var userAgent = navigator.userAgent.toLowerCase();
    if ((userAgent.indexOf("iphone") != -1) || (userAgent.indexOf("ipad") != -1)) {
        document.write('<link rel="stylesheet" type="text/css" href="../styles/mobile.css"/*tpa=http://www.elcompanies.com/Style Library/EsteeLauder/styles/mobile.css*/ />');
        mobileDevice = true;
    }
}

//Author: AT
//Added On: 11/14/2011
//Purpose: Javascript function to change the image of Reader icon on hover
function bindReaderHoverImage() {
    $('.readersrow a').each(function () {
        var hoverImage = $(this).attr('hoverimg');
        var normImage = $(this).attr('normimg');
        iconChange(this, normImage, hoverImage);
    });
}
//RSS Feeds Page Scripts
//Author: DB
//Added On: 08/31/2011
//Purpose: Used on set minimum height according to left nav
function SetEmployeeQuoteStyle() {
    if ($('.template_3-4').length > 0)
        {
        $(document).ready(
                function () {
                    $('.panel p').each(function () {
                        this.style.display = "block";
                        var height = this.clientHeight;
                        if (height == 0) {
                            height = this.scrollHeight;
                        }
                        var margin = (190 - height) / 2;
                        this.style.display = "";
                        if (margin > 0 && margin < 90) {
                            this.style.marginTop = margin;
                        }
                    }
                    );
                });
    }
}

//Handle Post back on Media Library Page
//Author: PR
//Added On: 11/28/2011
function MediaScriptForPostBack(index, pageSeperator, _totalSlide) {
    var pos = 0;
    if (index != 0) {
        if (index > 2) {
            pos = index - 2;
            if (index == _totalSlide) {
                pos--;
            }
        }
        pos = -81 * pos;
    }
    $('.featuredSlide li').removeClass('selected');
    $('.featuredThumb li').removeClass('selected');
    $('.featuredSlide li:nth-child(' + index + ')').addClass('selected');
    $('.featuredThumb li:nth-child(' + index + ')').addClass('selected');
    $('.featuredThumb').css('left', pos + 'px');
    $('.imgDisc .header .entry_meta .order').html(index + "&nbsp;" + pageSeperator + "&nbsp;" + _totalSlide);
    $('#featuredCaption').html($('.featuredSlide .selected .downloadLink').html());
}