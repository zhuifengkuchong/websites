//Modified On: 07/20/2011
//Purpose: Cinematic carousal - make first dot highlighted
//Modified On: 07/18/2011
//To make the first dot highlighted as page loads

// config parameters for overlay component
var config = {
    sourceOffY: 70,
    sourceOnY: 90,
    speedIn: 'normal',
    speedOut: 'normal',
    yOffset: 125
};

$(function () {
    $.placeholder.shim();
});

jQuery(document).ready(function () {
    initSitePadding();
    $(window).resize(function () {
        initSitePadding();
    });
    // tab controls logic
    $('.tab_control').each(function (index) {
        var tab_control = this;
        var anchors = $(this).find('.nav a');
        var containers = $(this).find('.tab_content');
        anchors.each(function (index) {
            $(this).click(index, function (event) {
                event.preventDefault();
                $(tab_control).find('.selected').removeClass('selected');
                $(this).addClass('selected');
                $(containers[event.data]).addClass('selected');
            });
        });
    });
    $('.filters_list h3').each(function (index) {
        var accordion = new ELC.FilterListAccordion($(this));
    });
    // cinematic carousel
    var cinematic_carousel = new ELC.CinematicCarousel($('.cinematic .carousel').first());
    // media library features carousel
    featured_media_carousel = new FeaturedMediaCarousel($('.content .featured_media').first());
    // for the overlay component
    $('.overlay-slide-component').each(function () {
        var imgHolder = $('a', this);
        $(imgHolder).each(function () {
            $('img', this).first().each(function () {

                if ($(this).attr('src') !== "")
                    initSlideOverlay($(this).parent(), config);
            });
        });
    });
    // for the overlay component
    //DB : removed check for complete as image is not mandotory
    $('.overlay-fade-component').each(function () {
        var imgHolder = $('http://www.elcompanies.com/Style Library/EsteeLauder/javascripts/a.fade', this);
        $(imgHolder).each(function () {
            $('img', this).first().each(function () {
                initFadeOverlay($(this).parent());
            });
        });
    });
    // global directory
    $('.directory_entry').each(function () {
        var directory_entry = this;
        $('h1', this).click(function (event) {
            if ($('div', directory_entry).is(':visible')) {
                $(this).css({ backgroundPosition: "-700px -796px" });
                $('.lang', directory_entry).fadeOut();
                $('div', directory_entry).slideUp('slow', 'swing');
            } else {
                $(this).css({ backgroundPosition: "-700px -1048px" });
                $('.lang', directory_entry).fadeIn();
                $('div', directory_entry).slideDown('slow', 'swing');
            }
        });
    });

    //MA : Open A-Z tab when request comes from template 1.0
    $('.tab_control').each(function (index) {
        var queryString = getQuerystring('Tab');
        queryString.trim();
        queryString = decodeURI(queryString);
        var tab_control = this;
        var anchors = $(this).find('.nav a');
        var containers = $(this).find('.tab_content');
        anchors.each(function (index) {
            if (index == queryString) {
                $(tab_control).find('.selected').removeClass('selected');
                $(this).addClass('selected');
                $(containers[index]).addClass('selected');
            }
        });
    });

    //MA : redirection from individual brand page to contact us page
    $('#ConsumerInquiries .directory_entry').each(function () {
        var directory_entry = this;
        var queryString = getQuerystring('brandName');
        queryString.trim();
        queryString = decodeURIComponent(queryString);


        if ($('h1', this).text().toLowerCase().trim() == queryString.toLowerCase()) {

            if ($('div', directory_entry).is(':visible')) {
                $(this).children('h1').css({ backgroundPosition: "-700px -796px" });
                $('.lang', directory_entry).fadeOut();
                $('div', directory_entry).slideUp('slow', 'swing');
            } else {

                $(this).children('h1').css({ backgroundPosition: "-700px -1048px" });
                $('.lang', directory_entry).fadeIn();
                $('div', directory_entry).css('display', 'block');
                //Check if the ribbon is not visible
                if (!$('#s4-ribbonrow').is(':visible')) {
                    $(document).scrollTop($(this).position().top);
                }
            }
        }

    });

    function getQuerystring(key, default_) {
        if (default_ == null) default_ = "";
        key = key.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regex = new RegExp("[\\?&]" + key + "=([^&#]*)");
        var qs = regex.exec(window.location.href);
        if (qs == null)
            return default_;
        else
            return qs[1];
    }


    //  global nav memory
    NavigationMemory.init();

    $('http://www.elcompanies.com/Style Library/EsteeLauder/javascripts/div.main_content_footer div.menu').hover(function (evt) {
        $(this).addClass('hover')
    }, function (evt) {
        $(this).removeClass('hover');
    });

    $('li.menu span.networking').each(function () {
        $(this).parent().hover(function () {
            $(this).addClass('hover');
        }, function () {
            $(this).removeClass('hover');
        });
    });

    // create tab carousel instance
    var tab_carousel = new FD.TabCarousel($('div.tab_carousel').eq(0));
    tab_carousel.setCurrentIndex(0, 0);
});
// end of jQuery ready function

// define ELC as namespace
(function (undefined) {

    this.ELC = this.ELC || {};

}).call(window);

// Class ELC.FilterListAccordion
(function (undefined) {

    this.FilterListAccordion = function (filter_header) {
        var accordion = filter_header.parent();
        var filters = accordion.find('ul li');
        var radio_inputs = filters.has('input[type="radio"]');
        var other = accordion.children(':not(h3,ul)');
        filter_header.click(function (event) {
            event.preventDefault();
            var selected = !accordion.hasClass('selected');
            var k = 0;
            filters.each(function (index) {
                var item = $(this);
                if (!item.hasClass('selected')) {
                    if (k++ == 0) {
                        if (selected) {
                            item.slideDown('slow', function () {
                                accordion.addClass('selected');
                            });
                        } else {
                            item.slideUp('slow', function () {
                                accordion.removeClass('selected');
                            });
                        }
                    } else {
                        if (selected) {
                            item.slideDown('slow');
                        } else {
                            item.slideUp('slow');
                        }
                    }
                }
            });
            other.each(function () {
                if (selected) {
                    if (k == 0) {
                        $(this).slideDown('slow', function () {
                            accordion.addClass('selected');
                        });
                    } else {
                        $(this).slideDown('slow');
                    }
                } else {
                    if (k == 0) {
                        $(this).slideUp('slow', function () {
                            accordion.removeClass('selected');
                        });
                    } else {
                        $(this).slideUp('slow');
                    }
                }
            });
        });
    };

    this.CinematicCarousel = function (context) {
        var cinematics = context.children('ul').find('li');
        if (cinematics.length == 0) return;
        var nav = context.children('.nav');
        var controls = nav.find('a');
        var masthead = $('body > .masthead');
        var animating = false;
        var selected_index = 0;
        var depth = 0;
        var timer = null;

        //To make the first dot highlighted as page loads
        $(controls[selected_index]).parent().addClass('selected');
        $('.background_container', cinematics[selected_index]).fadeIn('slow');
        $('.proof_point', cinematics[selected_index]).delay(500).fadeIn('slow');

        controls.each(function (index) {
            $(this).click(function (event) {
                event.preventDefault();
                if (!animating) {
                    change(index);
                }
            });
        });

        var change = function (index) {
            if (index != selected_index) {
                animating = true;
                $('.proof_point', cinematics[selected_index]).delay(200).fadeOut('slow'); //.css('display', 'none');
                $('.background_container', cinematics[selected_index]).fadeOut('slow'); //.css('display', 'none');
                $(controls[index]).parent().addClass('selected');
                $(controls[selected_index]).parent().removeClass('selected');
                $('.background_container', cinematics[index]).delay(600).fadeIn('slow');
                $('.proof_point', cinematics[index]).delay(900).fadeIn('slow', function () {
                    $(cinematics[selected_index]).removeClass('selected');
                    $(this).parent().addClass('selected');
                    selected_index = index;
                    animating = false;
                });
            }
            clearTimeout(timer);
            timer = setTimeout(function () {
                var new_index = selected_index + 1;
                if (new_index >= controls.length) new_index = 0;
                change(new_index);
            }, 10000);
        };

        change(0);
    };

}).call(window.ELC);

NavigationMemory = {
    selected_index: 0,

    init: function () {
        var controls = $('.masthead .primary_nav > ul > li');
        controls.each(function (index) {
            if ($(this).hasClass('selected')) {
                selected_index = index;
            }
            $(this).hover(function (event) {
                controls.each(function () {
                    $(this).removeClass('selected');
                });
            },
                function (event) {
                    $(controls[selected_index]).addClass('selected');
                }
            );
        });
    }
}

var featured_media_carousel;

function FeaturedMediaCarousel(context) {
    var selected_index = -1; 
     $('.featuredThumb li').each(function(index){
     	if($(this).hasClass('selected')){
     		selected_index = index;
    		return false; 
     	}
    });
    if(selected_index < 0){
        selected_index = 0;
    }

    var depth = 0;
    var masthead = $('body > .masthead');
    var features = context.children('ul').find('li');
    var nav = context.children('.nav');
    var slideshow_button = context.children('.slideshow');
    var list = nav.find('ul').first();
    var controls = nav.find('li');
    var previous = nav.find('a.previous').first();
    var next = nav.find('http://www.elcompanies.com/Style Library/EsteeLauder/javascripts/a.next').first();
    var disabled = false;

    next.click(function (event) {
        event.preventDefault();
        var new_index = selected_index + 1;
        if (new_index != controls.length && !disabled) change(new_index);
    });

    previous.click(function (event) {
        event.preventDefault();
        var new_index = selected_index - 1;
        if (new_index >= 0 && !disabled) change(new_index)
    });

    controls.find('a').each(function (index) {
        $(this).click(function (event) {
            event.preventDefault();
            if (!disabled) change(index);
        });
    });

    var change = function (index) {
        if (index == selected_index) return;
        disabled = true;
        $(controls[index]).addClass('selected');
        $(controls[selected_index]).removeClass('selected');


        var totalSlide = $(controls).length;
        var currentSlide = $(features[index]).find('.preview a').attr('number');


        if (typeof (lblMediaLibraryPagingSeperator) != "undefined") {
            $('.header .entry_meta .order').html(currentSlide + "&nbsp;" + lblMediaLibraryPagingSeperator + "&nbsp;" + totalSlide);
        }
        else {
            $('.header .entry_meta .order').html(currentSlide + "&nbsp;" + of + "&nbsp;" + totalSlide);
        }




        $('#featuredCaption').html($(features[index]).find('.downloadLink').html());

        //Changed the time of fading effect from slow - > 1ms
        $(features[index]).css('z-index', ++depth).fadeIn("slow", function () {
            $(this).addClass('selected');
            $(features[selected_index]).removeClass('selected').css('display', 'none');

            next.removeClass('disabled');
            previous.removeClass('disabled');
            selected_index = index;
            disabled = false;
        });

        if (index != 0 && index != controls.length - 1) list.animate({ left: (81 * -(index - 1)) }, 'slow');
        nav.css('z-index', ++depth);
        slideshow_button.css('z-index', ++depth);
        masthead.css('z-index', ++depth);
        next.addClass('disabled');
        previous.addClass('disabled');
        var titleAnchor = $('.imgDisc h1').find('a');
        $(titleAnchor).attr('number', (index + 1));
        $('.new-index input[type=hidden]').val(index);
        $('.selected-index input[type=hidden]').val(selected_index);

        //To make the next previous button disable/enable
        next.removeClass('disablePagination');
        previous.removeClass('disablePagination');
        if (index == 0) {
            previous.addClass('disablePagination');
        }
        if (index == controls.length - 1) {
            next.addClass('disablePagination');
        }
    }
    if (selected_index == 0) {
        previous.addClass('disablePagination');
    }
    if (selected_index == controls.length - 1) {
        next.addClass('disablePagination');
    }
}

function initSlideOverlay(imgHolder, opts) {
    var imgWidth = 246;
    var imgHeight = 330;
    imgHolder.css({
        width: imgWidth,
        height: imgHeight
    });
    $('.panel', imgHolder).css({
        top: (imgHeight - opts.yOffset) + 'px',
        height: imgHeight
    });
    $('.source', imgHolder).css({
        top: (imgHeight - opts.sourceOffY) + 'px',
        height: imgHeight
    });
    imgHolder.hover(function () {
        $('.panel', this).delay(100).animate({ top: '0px' }, { duration: opts.speedIn, easing: 'swing' });
        $('.panel p', this).delay(100).fadeTo("slow", 1);
        $('.source', this).delay(100).animate({ top: imgHeight - opts.sourceOnY + 'px' }, { duration: opts.speedIn, easing: 'swing' });
        $('.source .title', this).delay(100).fadeTo("slow", 1);
    }, function () {
        $('.panel', this).stop(true, false).animate({ top: imgHeight - opts.yOffset + 'px' }, { duration: opts.speedOut });
        $('.panel p', this).stop(true, false).fadeTo("fast", 0);
        $('.source', this).stop(true, false).animate({ top: imgHeight - opts.sourceOffY + 'px' }, { duration: opts.speedOut });
        $('.source .title', this).stop(true, false).fadeTo("fast", 0);
    });
};

// DB : removed code which sets height,width as image is not mandotry
function initFadeOverlay(imgHolder) {
    var elem = $('.panel', imgHolder);
    imgHolder.hover(function () {
        $(elem).delay(200).fadeTo("slow", .8);
    }, function () {
        $(elem).stop(true, true).fadeOut();
    });
};

function initSitePadding() {
    if ($(window).width() > 1024) {
        $('.content').css({ paddingLeft: "30px", paddingRight: "30px" });
        $('.footer').css({ paddingLeft: "30px", paddingRight: "30px" });
    } else {
        $('.content').css({ paddingLeft: "20px", paddingRight: "20px" });
        $('.footer').css({ paddingLeft: "20px", paddingRight: "20px" });
    }
}
// define FD as namespace
(function (undefined) {

    this.FD = this.FD || {};

}).call(window);

// Class FD.TabCarousel
(function (undefined) {

    var fn;

    // Constructor
    this.TabCarousel = function ($el, config) {
        this._$el = $el;
        this._$tabs = $el.find('>ul.tabs>li');
        this._$pagers = null;
        this._$pagerSelectors = null;
        this._$tab_nav = $el.find('div.tab_nav').eq(0);
        this._$tab_navs = this._$tab_nav.find('li');
        this._slides = [];
        this._tabs = 0;
        this._curIdx = {
            'tab': -1,
            'slide': -1
        };
        this._timer = null;
        if (config === undefined || typeof config !== "object") {
            config = {};
        }
        this._fadeSpeed = config.fadeSpeed || 'slow';
        this._fadeTimeout = config.fadeTimeout || 500;
        this._slideShowTimeout = config.slideShowTimeout || 5000;

        this._init();
    };

    // shorthand for TabCarousel prototype
    fn = this.TabCarousel.prototype;

    // setter of current index, containing the tab index, and slide index
    // reset slide if resetSlide flag is true
    fn.setCurrentIndex = function (tab, slide, resetSlide) {

        if (mobileDevice) {
            try {
                RecreateVideoInSlides();
            }
            catch (e) {
                //do nothing
            }
        } else
        if (!$.browser.msie) {
            //fix: for video stop while navigation
            $(".video-js-box").each(function () {
                try {
                    var div = $(this);
                    var video = div.children("video").first();

                    video.remove();
                    div.append(video);
                }
                catch (e) {
                    //do nothing
                }
            });
        } //end if for non-ie
        //for ie
        //description: for ie flowplayer is rendered instead of source
        // so recreating the photos content.
        else {
            //#hdnVideoHtmlClient is a hidden variable having data source for videos.
            //need of this variable is to reassign the source, when user navigates from first slide to another slide.
            var source = $("#hdnVideoHtmlClient");
            if (source != undefined && source.val() != undefined) {
                var _data = source.val().split("@#$%").clean("");
                var count = 0;

                $(".slide_container").each(function () {
                    var videoDiv = $(this).find(".video-js-box").first();
                    if (videoDiv != null && _data.length > 0 && videoDiv.html() != null) {
                        videoDiv.remove();
                        $(this).append(_data[count]);
                        count++;
                    }

                });
            }
        }


        if (resetSlide === undefined || typeof resetSlide !== 'boolean') {
            resetSlide = true;
        }

        if (tab < 0) {
            tab = this._tabs - 1;
            //slide = 0;
        } else if (tab >= this._tabs) {
            tab = 0;
            //slide = 0;
        } else {
            var total_slides = this._slides[tab];
            if (slide < 0) {
                slide = total_slides - 1;
            } else if (slide >= total_slides) {
                slide = 0;
            }
        }
        if (this._curIdx.tab === tab && this._curIdx.slide === slide) return;

        var oldTab = this._curIdx.tab;
        var oldSlide = this._curIdx.slide;

        // if tab is changed, update tab and reset slide to 0 if asked
        // else, just update slide
        if (oldTab === tab) {
            this._curIdx.slide = slide;
        } else {
            this._curIdx.tab = tab;
            this._curIdx.slide = resetSlide ? 0 : slide;
        }

        this._onCurIdxChanged({
            'oldTab': oldTab,
            'newTab': this._curIdx.tab,
            'oldSlide': oldSlide,
            'newSlide': this._curIdx.slide
        });
    };

    // register event handlers of inner UI controls
    fn._init = function () {
        var carousel = this;
        this._$el.find('ul.tabs>li').each(function (idx, el) {
            var num = $(el).find('ul.slides>li').length;
            carousel._slides.push(num);
        });
        carousel._tabs = carousel._slides.length;

        // build pagers at real time
        // for progressive enhancement
        if (carousel._$pagers === null) {
            carousel._$pagers = $('<div class="pagers"></div>');
            carousel._$el.append(carousel._$pagers);
        }

        carousel._$pagers.delegate("li", "click", function (evt) {
            evt.preventDefault();

            var $selector = $(this);
            var i = $selector.index();

            carousel.setCurrentIndex(carousel._curIdx.tab, i);
        });

        carousel._$tab_nav.delegate("li", "click", function (evt) {
            evt.preventDefault();

            var $nav = $(this);
            var i = $nav.index();

            //Author:ND
            //Added On: 04/15/2011
            //Purpose:Left Navigation Topics Syncup 
            //Left Nav Sync up start
            SetIndex(parseInt(i), false);
            //Left Nav Sync up End
            carousel.setCurrentIndex(i, carousel._curIdx.slide);
        });

        this._buildNavArrows();
    };

    // build nav arrows at real time
    // for progressive enhancement
    fn._buildNavArrows = function () {
        var carousel = this;
        var html = '<div class="arrow left"><div class="image"></div></div><div class="arrow right"><div class="image"></div></div>';
        carousel._$el.append(html);

        carousel._$el.find('div.arrow').each(function (i, el) {
            var $div = $(el);
            var $img = $div.find('>div.image');
            if (mobileDevice) {
                //In case of mobile we are not attaching hover event
				if ($div.hasClass('left')) {
	                $img.click(function (evt) {
	                    carousel._backward();
	                });
		        } else if ($div.hasClass('right')) {
	                $img.click(function (evt) {
	                    carousel._forward();
				    });
	            }
		    } else {
                $div.hover(function (evt) {
                    var tab = carousel._curIdx.tab;
                    var slide = carousel._curIdx.slide;
                    if (($div.hasClass('left') && slide == 0 && tab == 0) || ($div.hasClass('right') && slide == carousel._slides[tab] - 1 && tab == carousel._tabs - 1)) {
                    } else {
                        $img.fadeIn();
                    }
                }, function (evt) {
                    $img.fadeOut();
                });
	            if ($div.hasClass('left')) {
	                $img.click(function (evt) {
	                    carousel._backward();
	                    var tab = carousel._curIdx.tab;
	                    var slide = carousel._curIdx.slide;
	                    if (slide == 0 && tab == 0) {
	                        $img.fadeOut();
	                    }
	                });
	            } else if ($div.hasClass('right')) {
	                $img.click(function (evt) {
	                    carousel._forward();
	                    var tab = carousel._curIdx.tab;
	                    var slide = carousel._curIdx.slide;
	                    if (slide == carousel._slides[tab] - 1 && tab == carousel._tabs - 1) {
	                        $img.fadeOut();
	                    }
	                });
	            }
            }
        })
    };

    // build pagers at real time
    // for progressive enhancement
    fn._buildPagers = function () {
        var carousel = this;
        var tab = carousel._curIdx.tab;
        var slide = carousel._curIdx.slide;

        if (tab < 0 || slide < 0) return;

        var numOfSlides = carousel._slides[tab];
        var i;
        var html = '<ul>';

        for (i = 0; i < numOfSlides; i += 1) {
            var klass = i === slide ? 'selected' : '';
            html += '<li class="' + klass + '"><a href="">' + (i + 1) + '</a></li>';
        }
        html += '</ul>';
        //Author:ND
        //Added On: 04/16/2011
        //Purpose:Hide Pager if number of slides is less than or equal to 1 
        //Left Nav Sync up Pager Count start
        if (numOfSlides <= 1) {
            html = '<style type="text/css">' +
                    '.template_3-5 .main_content UL.tabs P.info' +
                    '{' +
                        'top:auto;' +
                        'margin-top:22px;' +
                        '*margin-top:19px;/*for IE-7*/' +
                    '} ' +
                    '.template_3-3 .main_content UL.tabs P.info' +
                    '{' +
                        'top:auto;' +
                        'margin-top:16px;' +
                        '*margin-top:13px;/*for IE-7*/' +
                    '} '+
                '</style>';
        }
        //Left Nav Sync up Pager Count end
        carousel._$pagers.html(html);
        carousel._$pagerSelectors = carousel._$pagers.find('li');
    };

    // udpate pagers based on old slide index and new slide index
    fn._updatePagers = function (oldSlide, newSlide) {
        var carousel = this;

        var $lis = carousel._$pagerSelectors;
        if (oldSlide > -1) {
            $lis.eq(oldSlide).removeClass('selected');
        }
        $lis.eq(newSlide).addClass('selected');
    };

    // update nav tab based on old tab index and new tab index
    fn._updateTabNavs = function (oldTab, newTab) {
        var carousel = this;
        if (oldTab > -1) {
            carousel._$tab_navs.eq(oldTab).removeClass('selected');
            if (oldTab > 0) {
                carousel._$tab_navs.eq(oldTab - 1).removeClass('pre');
            }
        }
        carousel._$tab_navs.eq(newTab).addClass('selected');
        if (newTab > 0) {
            carousel._$tab_navs.eq(newTab - 1).addClass('pre');
        }
    };

    // update contents, and call back if any
    fn._updateContents = function (data, callback) {
        var ot = data.oldTab,
        nt = data.newTab,
        os = data.oldSlide,
        ns = data.newSlide;
        var carousel = this;
        var $tabs = carousel._$tabs;
        var fadeDelay = 0;
        var fadeSpeed = 0;

        var $os;
        var $ns;

        var $oldSlide = null;
        var $oldLeft = null;
        var $oldRight = null;
        var $newSlide = null;
        var $newLeft = null;
        var $newRight = null;

        if (ot > -1 && os > -1) {
            $os = $tabs.eq(ot).find('>ul.slides>li').eq(os);
            $oldSlide = $os.find('div.slide_container');
            $oldLeft = $os.find('div.info_container>div');
            $oldRight = $os.find('div.info_container>p');
        }

        $ns = $tabs.eq(nt).find('>ul.slides>li').eq(ns);
        $newSlide = $ns.find('div.slide_container');
        $newLeft = $ns.find('div.info_container>div');
        $newRight = $ns.find('div.info_container>p');

        if (ot <= -1 && os <= -1) {
            carousel._transit($oldSlide, $newSlide, fadeSpeed, fadeDelay, false, callback);
            carousel._transit($oldLeft, $newLeft, fadeSpeed, fadeDelay, false);
            carousel._transit($oldRight, $newRight, fadeSpeed, fadeDelay, false);
        } else if (ot !== nt) {
            fadeDelay = carousel._fadeTimeout;
            fadeSpeed = carousel._fadeSpeed;
            carousel._transit($oldSlide, $newSlide, fadeSpeed, fadeDelay, true, callback);
            carousel._transit($oldLeft, $newLeft, fadeSpeed, fadeDelay, true);
            carousel._transit($oldRight, $newRight, fadeSpeed, fadeDelay, true);
        } else if (ot === nt && os !== ns) {
            fadeDelay = carousel._fadeTimeout;
            fadeSpeed = carousel._fadeSpeed;
            carousel._transit($oldSlide, $newSlide, fadeSpeed, fadeDelay, false, callback);
            carousel._transit($oldLeft, $newLeft, fadeSpeed, fadeDelay, false);
            carousel._transit($oldRight, $newRight, fadeSpeed, fadeDelay, false);
        }
    };

    // transit from old slide to new slide, with config for fade effect
    // call back if any
    fn._transit = function ($old, $new, fadeSpeed, fadeDelay, mustTransit, callback) {
        if (!mustTransit && $new.hasClass('no_transition')) {
            if ($old !== null) {
                $old.hide();
            }
            $new.show();
        } else {
            if ($old !== null) {
                $old.fadeOut(fadeSpeed);
            }
            if (callback === undefined) {
                callback = function () { };
            }
            $new.delay(fadeDelay).fadeIn(fadeSpeed, callback);
        }
    };

    // event handler when current slide index is changed
    fn._onCurIdxChanged = function (data) {
        var carousel = this;
        carousel._stopTimer();

        if (mobileDevice) {
            try {

                RecreateVideoInSlides();
	            //In case of mobile Always display the navigation arrows on updating content
	        	var $img = carousel._$el.find('div.arrow .image') ;
	            $img.css('display','block');
	            
              	//In case of mobile hide the navigation arrows on updating content based on current section
	            carousel._$el.find('div.arrow').each(function (i, el) {
		            var $div = $(el);
		            var $imgarrow = $div.find('>div.image');
		            var tab = carousel._curIdx.tab;
	                var slide = carousel._curIdx.slide;
		            if ($div.hasClass('left')) {
		                if (slide == 0 && tab == 0) {
		                    $imgarrow .fadeOut();
		                }
		            }
		            if ($div.hasClass('right')) {
		                if (slide == carousel._slides[tab] - 1 && tab == carousel._tabs - 1) {
                    		$imgarrow .fadeOut();
                		}
    	            }            
	            });
            }
            catch (e) {
                //do nothing
            }
        } else
        if (!$.browser.msie) {
            //fix: for video stop while navigation
            $(".video-js-box").each(function () {
                try {
                    var div = $(this);
                    var video = div.children("video").first();

                    video.remove();
                    div.append(video);
                }
                catch (e) {
                    //do nothing
                }
            });
        } //end if for non-ie
        //for ie
        //description: for ie flowplayer is rendered instead of source
        // so recreating the photos content.
        else {
            //#hdnVideoHtmlClient is a hidden variable having data source for videos.
            //need of this variable is to reassign the source, when user navigates from first slide to another slide.
            var source = $("#hdnVideoHtmlClient");
            if (source != undefined && source.val() != undefined) {
                var _data = source.val().split("@#$%").clean("");
                var count = 0;

                $(".slide_container").each(function () {
                    var videoDiv = $(this).find(".video-js-box").first();
                    if (videoDiv != null && _data.length > 0 && videoDiv.html() != null) {
                        videoDiv.remove();
                        $(this).append(_data[count]);
                        count++;
                    }
                });
            }
        }
        if (data.oldTab !== data.newTab) {
            carousel._buildPagers();
            carousel._updateTabNavs(data.oldTab, data.newTab);
        } else {
            carousel._updatePagers(data.oldSlide, data.newSlide);
        }
        carousel._updateContents(data, function () {
            //carousel._startTimer();
        });
    };

    // increase current slide index by 1
    // advance to the next tab if needed
    fn._forward = function () {
        var carousel = this;
        var tab = carousel._curIdx.tab;
        var slide = carousel._curIdx.slide;
        if (slide == carousel._slides[tab] - 1 && tab == carousel._tabs - 1) {

        } else {
            slide++;
            if (slide > carousel._slides[tab] - 1) {
                slide = 0;
                tab++;

                //Author:ND
                //Added On: 04/15/2011
                //Purpose:Left Navigation Topics Syncup 
                //Left Nav Sync up start
                SetIndex(tab, false);
                //Left Nav Sync up End
            }
            carousel.setCurrentIndex(tab, slide, false);
        }
    };

    // decrese current slide index by 1
    // retreat to the previous tab if needed
    fn._backward = function () {
        var carousel = this;
        var tab = carousel._curIdx.tab;
        var slide = carousel._curIdx.slide;
        if (slide == 0 && tab == 0) {

        } else {
            slide--;
            if (slide < 0) {
                tab--;

                //Author:ND
                //Added On: 04/15/2011
                //Purpose:Left Navigation Topics Syncup 
                //Left Nav Sync up start
                SetIndex(tab, false);
                //Left Nav Sync up end
            }
            if (tab < 0) {
                tab = carousel._tabs - 1;

                //Author:ND
                //Added On: 04/15/2011
                //Purpose:Left Navigation Topics Syncup 
                //Left Nav Sync up start
                SetIndex(tab, false);
                //Left Nav Sync up end
            }
            carousel.setCurrentIndex(tab, slide, false);
        }
    };

    // not used
    // start the timer for auto play
    fn._startTimer = function () {
        var carousel = this;
        if (carousel._timer !== null) return;

        carousel._timer = setTimeout(function () {
            carousel._forward();
            carousel._timer = null;
            carousel._startTimer();
        }, carousel._slideShowTimeout);
    };

    // not used
    // stop the timer for auto play
    fn._stopTimer = function () {
        if (this._timer === null) return;

        clearTimeout(this._timer);
        this._timer = null;
    };
}).call(window.FD);

Array.prototype.clean = function (deleteValue) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == deleteValue) {
            this.splice(i, 1);
            i--;
        }
    }
    return this;
};

function RecreateVideoInSlides() {
    //#hdnVideoHtmlClient is a hidden variable having data source for videos.
    //need of this variable is to reassign the source, when user navigates from first slide to another slide.
    var source = $("#hdnVideoHtmlClient");
	if (source != undefined && source.val() != undefined) {
	    var _data = source.val().split("@#$%").clean("");
	    var count = 0;
	
	    $(".slide_container").each(function () {
	        var videoDiv = $(this).find(".video-js-box").first();
	        if (videoDiv != null && _data.length > 0 && videoDiv.html() != null) {
	            videoDiv.remove();
	            $(this).append(_data[count]);
	            count++;
	        }
	
	    });
	}
}
