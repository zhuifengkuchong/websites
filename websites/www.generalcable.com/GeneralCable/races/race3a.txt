<script id = "race3a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race3={};
	myVars.races.race3.varName="TopNav1_tSearchText__onkeydown";
	myVars.races.race3.varType="@varType@";
	myVars.races.race3.repairType = "@RepairType";
	myVars.races.race3.event1={};
	myVars.races.race3.event2={};
	myVars.races.race3.event1.id = "Lu_DOM";
	myVars.races.race3.event1.type = "onDOMContentLoaded";
	myVars.races.race3.event1.loc = "Lu_DOM_LOC";
	myVars.races.race3.event1.isRead = "False";
	myVars.races.race3.event1.eventType = "@event1EventType@";
	myVars.races.race3.event2.id = "TopNav1_tSearchText";
	myVars.races.race3.event2.type = "onkeydown";
	myVars.races.race3.event2.loc = "TopNav1_tSearchText_LOC";
	myVars.races.race3.event2.isRead = "True";
	myVars.races.race3.event2.eventType = "@event2EventType@";
	myVars.races.race3.event1.executed= false;// true to disable, false to enable
	myVars.races.race3.event2.executed= false;// true to disable, false to enable
</script>

