
// execute after the page loads

// search

$(document).ready(function(){
	
		if ( ($.browser.msie) ){
		$('.nav table div > ul > li').mouseenter(function(){ $(this).children('div').show();
		if( $(this).hasClass('company')) {
			$('.nav li.company a').css('background','transparent url(/_layouts/UNFIWebsite/Images/lefttopnavhover.png) repeat-x left top');
			$('div a', this).css('background','none');
		} else {
			if( $(this).hasClass('careers')) {
				$('.nav li.careers a').css('background','transparent url(/_layouts/UNFIWebsite/Images/righttopnavhover.png) repeat-x right top');
				$('div a', this).css('background','none'); 
			} else {
			$('a', this).css('background','transparent url(/_layouts/UNFIWebsite/Images/middletopnavhover.png) repeat-x left top');
			$('div a', this).css('background','none');
			}
		} 
	 })
		.mouseleave(function(){ $(this).children('div').hide();
		if( $(this).hasClass('company')) {
			$('.nav li.company a').css('background','transparent url(/_layouts/UNFIWebsite/Images/lefttopnav.png) repeat-x left top');
			$('div a', this).css('background','none'); 
		}
		else if( $(this).hasClass('careers')) {
			$('.nav li.careers a').css('background','transparent url(/_layouts/UNFIWebsite/Images/righttopnav.png) repeat-x right top');
			$('div a', this).css('background','none'); 
		} 
		else {
			$('a', this).css('background','transparent url(/_layouts/UNFIWebsite/Images/middletopnav.png) repeat-x left top');
			$('div a', this).css('background','none');
		} 		  
	   });
}else {
	$('.nav table div > ul > li').mouseenter(function(){ $(this).children('div').fadeIn();
		if( $(this).hasClass('company')) {
			$('.nav li.company a').css('background','transparent url(/_layouts/UNFIWebsite/Images/lefttopnavhover.png) repeat-x left top');
			$('div a', this).css('background','none');
		} else {
			if( $(this).hasClass('careers')) {
				$('.nav li.careers a').css('background','transparent url(/_layouts/UNFIWebsite/Images/righttopnavhover.png) repeat-x right top');
				$('div a', this).css('background','none'); 
			} else {
			$('a', this).css('background','transparent url(/_layouts/UNFIWebsite/Images/middletopnavhover.png) repeat-x left top');
			$('div a', this).css('background','none');
			}
		} 
	 })
		.mouseleave(function(){ $(this).children('div').fadeOut();
		if( $(this).hasClass('company')) {
			$('.nav li.company a').css('background','transparent url(/_layouts/UNFIWebsite/Images/lefttopnav.png) repeat-x left top');
			$('div a', this).css('background','none'); 
		}
		else if( $(this).hasClass('careers')) {
			$('.nav li.careers a').css('background','transparent url(/_layouts/UNFIWebsite/Images/righttopnav.png) repeat-x right top');
			$('div a', this).css('background','none'); 
		} 
		else {
			$('a', this).css('background','transparent url(/_layouts/UNFIWebsite/Images/middletopnav.png) repeat-x left top');
			$('div a', this).css('background','none');
		} 		  
	   });   
}
	
	
	if (screen.width>1024) { 
				$('.nav li.careers div').css('left','0px'); 
			}
			else {
				$('.nav li.careers div').css('left','-106px');
				$('.nav li.careers .wrapper').css('left','0px'); 
			}

// JQuery for Slideshow on homepage ////////////////////////////////////////////

	$(function() {
        $(".paging").show();
        $(".paging a:first").addClass("active");
                                
        var imageWidth = $(".window").width();
        var imageSum = $(".image_reel img").size();
        var imageReelWidth = imageWidth * imageSum;
                                
        $(".image_reel").css({'width' : imageReelWidth});

rotate = function() {
            var triggerID = $active.attr("rel") - 1; //Get number of times to slide
            var image_reelPosition = triggerID * imageWidth; //Determines the distance the image reel needs to slide

            $(".paging a").removeClass('active'); //Remove all active class
            $active.addClass('active'); //Add active class (the $active is declared in the rotateSwitch function)

            //Slider Animation
            $(".image_reel").animate({
                left: -image_reelPosition
            }, 500);
        };
rotateSwitch = function() {
            play = setInterval(function() { //Set timer - this will repeat itself every 7 seconds
                $active = $('.paging a.active').next(); //Move to the next paging
                if ( $active.length === 0) { //If paging reaches the end...
                    $active = $('.paging a:first'); //go back to first
                }
                rotate(); //Trigger the paging and slider function
            }, 7000); //Timer speed in milliseconds (7 seconds)
        };
                                
        rotateSwitch(); 

        $(".image_reel a").hover(function() {
            clearInterval(play); //Stop the rotation
        }, function() {
            rotateSwitch(); //Resume rotation timer
        });

        $(".paging a").click(function() {
            $active = $(this); //Activate the clicked paging
            //Reset Timer
            clearInterval(play); //Stop the rotation
            rotate(); //Trigger rotation immediately
            rotateSwitch(); // Resume rotation timer
            return false; //Prevent browser jump to link anchor
        });

    });


    $('.wrapper > div').mouseenter(function() {
        $(this).children('div').show();
    })
    .mouseleave(function() {
        $(this).children('div').hide();
    });

}); // end windowload

$(function() {
    var zIndexNumber = 1000;
    // Put your target element(s) in the selector below!
    $(".menusublinks").each(function() {
        $(this).css('zIndex', zIndexNumber);
        zIndexNumber -= 100;
    });
    $(".nav div.pop-nav .wrapper > div").each(function() {
        $(this).css('zIndex', zIndexNumber);
        zIndexNumber -= 10;
    });
});