/* Author: suhler@sangereby.com
Universal code
*/

var dropdownMenu;
var fixedTop;
var $fixedContentBlock;

var currentWidth = {};
currentWidth.actual = 0;
currentWidth.step = 0;
currentWidth.stepChange = false;
currentWidth.getActual = function () { return this.actual; };
currentWidth.getValue = function () { return this.step; };
currentWidth.setValue = function (val) {
    this.actual = val;
    //use larger images to prevent pixelation
    var prevStep = Number(this.step);
    if (val > 980) {
        this.step = 1280;
    } else if (val <= 980 && val > 768) {
        this.step = 980;
    } else if (val <= 768 && val > 480) {
        this.step = 768;
    } else {
        this.step = 480;
    }
    currentWidth.stepChange = (prevStep != this.step);
};

function resizeCheck() {
    currentWidth.setValue($(window).width());
    //check to see if there is a step change
    if (currentWidth.stepChange && $fixedContentBlock != null) {
        $fixedContentBlock.removeAttr("style");
    }
}

function buttonClick(id, forceHide) {
    var blockTop = fixedTop;
    $('.expansion-block[name!="' + id + '"]:visible').hide();
    $('.expansion-block[name="' + id + '"]').toggle();

    if ($('#search-expansion').is(":visible")) {

        $('input[name="main-search"]').focus();
        $('.expander[target="search"]').find('.nav-label').css('border-bottom-color', '#000000');
        $(document).scrollTop($('.expander[target="search"]').offset().top);
        blockTop -= $('#search-expansion').height();
    } else {
        $('.expander[target="search"]').find('.nav-label').css('border-bottom-color', '#808080');
    }

    if ($('#subnav-expansion').is(":visible")) {
        blockTop = 0;
        $('.expander[target="subnav"]').find('.nav-label').css('border-bottom-color', '#000000');
    } else {
        //blockTop = fixedTop;
        $('.expander[target="subnav"]').find('.nav-label').css('border-bottom-color', '#808080');
    }

    if ($fixedContentBlock != null) {
        $fixedContentBlock.css('top', blockTop);
    }
}

function styleTableRows() {
    $("table").each(function () {
        var rowCount = 0;
        var $rows = $(this).find("tr");
        $rows.each(function () {
            var row = $(this);
            if ($(row).children("td").length > 0) {
                rowCount++;
            }
            if (rowCount % 2 == 1) {
                if (!$(this).hasClass("total")) {
                    $(this).addClass("alt");
                }
            }

            //remove extra lines at the bottom of rows
            $(row).children("td").each(function () {

                if ($(this).css("border-bottom-style") != "none" && $(this).html() == "") {
                    $(this).css("border-bottom-style", "none");
                }

            });
        });
    });
}

$(document).ready(function () {
    dropdownMenu = $('#sectionNav').find('.dropdown-content');

    if ($('.fixed-block').length != 0) {
        $fixedContentBlock = $('.fixed-block')
    } else if ($('.fixed-block-community').length != 0) {
        $fixedContentBlock = $('.fixed-block-community')
    }
    fixedTop = parseInt($($fixedContentBlock).css('top'));


    $('.expander').click(function (event) {
        var id = $(this).attr("target");
        buttonClick(id);
    });
    $('#search-expansion').hide().removeClass('hidden');
    $('#subnav-expansion').hide().removeClass('hidden');

    $('.dropdown-label').click(function (event) {
        dropdownMenu.slideToggle("fast", function () {
            if ($(this).is(":visible")) {
                $('#sectionNav').find('.dropdown-label').html("Section Navigation &ndash;");
            } else {
                $('#sectionNav').find('.dropdown-label').html("Section Navigation +");
            }
        });
    });

    $('.logo').on('click', function () {
        window.location.href = "http://www.macysinc.com/default.aspx";
    });

    $(window).resize(function () {
        resizeCheck();
    });

    //style alternate td rows as gray.  more reliable than css:nth-child(odd)
    styleTableRows();

    $('a[href*="p=RssLanding"]').each(function (event) {
        var url = $(this).attr("href");
        var newUrl = url.replace("p=RssLanding", "p=irol-newsArticle");
        $(this).attr("href", newUrl);
    })

    //GA external link tracker
    /* $('a[href^="http"], a[href$=".pdf"], a[href$=".doc"], a[href$=".docx"], a[href$=".xls"], a[href$=".xlsx"]').click(function (event) {
        gaTrackCustom($(this).attr("href"))
    }) */
});

function resetOnResize() {
    currentWidth.setValue($(window).width());
    if (currentWidth.stepChange) {
        //reset the fixed block to the proper top value
        if ($fixedContentBlock != null) {
            $fixedContentBlock.removeAttr("style");
        }
        //reset the visibility of the side nav
        dropdownMenu.removeAttr("style");

        $('#search-expansion').hide();
        $('#subnav-expansion').hide();
        $('#sectionNav').find('.dropdown-label').html("Section Navigation +");

        $('.expander[target="search"]').find('.nav-label').removeAttr("style");
        $('.expander[target="subnav"]').find('.nav-label').removeAttr("style");
    }
}
/* 
function gaTrackCustom(link) {
    link = link.replace(/^http:\/\//, "")
    pageTracker._trackPageview("/external/" + String(link));
} */