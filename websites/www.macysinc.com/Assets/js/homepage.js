/* Author: suhler@sangereby.com
Homepage specific code
*/
var currentSlide = 0;
var slideshow_set = false;
var slideshow_setup = {
	effect: 'fold',
	slices: 14,
	startSlide: currentSlide + 1,
	animSpeed: 1000,
	pauseTime: 5000,
	controlNav: false,
	directionNav: false,
	afterChange: updateSlideList
};

var imgListNum = 0;
var imageList = [];
var imagePath = "/Assets/img/homepage/";
imageList[0] = ['Unknown_83_filename'/*tpa=http://www.macysinc.com/Assets/js/set01_01.jpg*/,
				'Unknown_83_filename'/*tpa=http://www.macysinc.com/Assets/js/set01_02.jpg*/,
                'Unknown_83_filename'/*tpa=http://www.macysinc.com/Assets/js/set01_03.jpg*/,
                'Unknown_83_filename'/*tpa=http://www.macysinc.com/Assets/js/set01_04.jpg*/,
                'Unknown_83_filename'/*tpa=http://www.macysinc.com/Assets/js/set01_05.jpg*/,
				'Unknown_83_filename'/*tpa=http://www.macysinc.com/Assets/js/set01_06.jpg*/];
imageList[1] = ['Unknown_83_filename'/*tpa=http://www.macysinc.com/Assets/js/set02_01.jpg*/,
                'Unknown_83_filename'/*tpa=http://www.macysinc.com/Assets/js/set02_02.jpg*/,
                'Unknown_83_filename'/*tpa=http://www.macysinc.com/Assets/js/set02_03.jpg*/,
                'Unknown_83_filename'/*tpa=http://www.macysinc.com/Assets/js/set02_04.jpg*/,
                'Unknown_83_filename'/*tpa=http://www.macysinc.com/Assets/js/set02_05.jpg*/,
				'Unknown_83_filename'/*tpa=http://www.macysinc.com/Assets/js/set02_06.jpg*/];
imageList[2] = ['Unknown_83_filename'/*tpa=http://www.macysinc.com/Assets/js/set03_01.jpg*/,
				'Unknown_83_filename'/*tpa=http://www.macysinc.com/Assets/js/set03_02.jpg*/,
                'Unknown_83_filename'/*tpa=http://www.macysinc.com/Assets/js/set03_03.jpg*/,
                'Unknown_83_filename'/*tpa=http://www.macysinc.com/Assets/js/set03_04.jpg*/,
                'Unknown_83_filename'/*tpa=http://www.macysinc.com/Assets/js/set03_05.jpg*/,
				'Unknown_83_filename'/*tpa=http://www.macysinc.com/Assets/js/set03_06.jpg*/];
imageList[3] = ['Unknown_83_filename'/*tpa=http://www.macysinc.com/Assets/js/set04_01.jpg*/,
                'Unknown_83_filename'/*tpa=http://www.macysinc.com/Assets/js/set04_02.jpg*/,
                'Unknown_83_filename'/*tpa=http://www.macysinc.com/Assets/js/set04_03.jpg*/,
                'Unknown_83_filename'/*tpa=http://www.macysinc.com/Assets/js/set04_04.jpg*/,
                'Unknown_83_filename'/*tpa=http://www.macysinc.com/Assets/js/set04_05.jpg*/,
				'Unknown_83_filename'/*tpa=http://www.macysinc.com/Assets/js/set04_06.jpg*/];

var hasSlideshow = true;

var currentWidth = {};
currentWidth.actual = 0;
currentWidth.step = 0;
currentWidth.stepChange = false;
currentWidth.getActual = function () { return this.actual; };
currentWidth.getValue = function () { return this.step; };
currentWidth.setValue = function (val) {
	this.actual = val;
	//use larger images to prevent pixelation
	var prevStep = Number(this.step);
	if (val > 980) {
		this.step = 1280;
		// } else if (val < 1280 && val >= 980) { 
		// this.step = 980;
	} else if (val <= 980 && val > 768) {
		this.step = 980;
	} else if (val <= 768 && val > 480) {
		this.step = 768;
	} else {
		this.step = 480;
	}
	currentWidth.stepChange = (prevStep != this.step);
};

$(document).ready(function () {
	imgListNum = Math.round(Math.random() * 3);
	$(window).resize(function () {
		updateDimensions();
	});
	updateDimensions(); //draw the slideshow once

	if (checkIfIE7or8()) {
		hasSlideshow = false;
	}

	$('#backstageSlides').nivoSlider({ pauseTime: 8000, effect: "slideInLeft", directionNav: false, slices: 1 });
	generateImageList();
	showRandomSlide();

});

function updateSlideList() {
	currentSlide = $('#slideshow').data('nivoslider').current();
}
function generateImageList() {
	var slideshowDiv = $('#slideshow');
	var height = Math.round(currentWidth.getValue() * 0.4296875);
	var prefix = currentWidth.getValue() < 1280 ? currentWidth.getValue() + "/" : "";
	for (var i = 0; i < imageList[imgListNum].length; i++) {
		slideshowDiv.append("<img src='" + imagePath + prefix + imageList[imgListNum][i] + "' width='" + currentWidth.getValue() + "' height='" + height + "' alt='slide'>");
	}
}

function updateDimensions() {
	if (hasSlideshow) {
		redrawSlideshow();
	} 
}
function checkIfIE7or8() {
	if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)) {
		var ieversion = new Number(RegExp.$1);
		if (ieversion <= 8) {
			return true;
		}
	}
	return false;
}

function redrawSlideshow() {
	currentWidth.setValue($(window).width());
	//check to see if there is a step change
	if (currentWidth.stepChange) {
		//clear out old slideshow
		if (slideshow_set) {
			delete $('#slideshow').data('nivoslider');
		}
		//generate a new set of properly sized images
		$('.slider-wrapper').empty();
		$('.slider-wrapper').html('<div id="slideshow" class="nivoSlider"></div>');
		generateImageList();

		//if user is on a tablet or desktop, show slideshow, otherwise show a static random image
		if (currentWidth.getActual() >= 768) {
			generateSlideshow();
		} else {
			showRandomSlide();
		}
	}

	if (currentWidth.getActual() >= 1280) {
		$(".slider-wrapper").css("left", "0");
		$("#slideshow").css("left", "0");
	} else {
		$(".slider-wrapper").css("left", "50%");
		$("#slideshow").css("left", -currentWidth.getValue() / 2);
	}
	$(".sizeBar").html("Dimensions: " + $(window).width() + " x " + $(window).height());
}
function generateSlideshow() {
	$('#slideshow').nivoSlider(slideshow_setup);
	slideshow_set = true;
}
function showRandomSlide() {
	var imgList = imageList[imgListNum];
	var rand = Math.round(Math.random() * (imgList.length - 1));
	var img = $("#slideshow img")[rand];
	$(img).addClass("shown");

}

























