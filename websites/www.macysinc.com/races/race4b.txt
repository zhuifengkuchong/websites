<script id = "race4b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race4={};
	myVars.races.race4.varName="Object[10706].paused";
	myVars.races.race4.varType="@varType@";
	myVars.races.race4.repairType = "@RepairType";
	myVars.races.race4.event1={};
	myVars.races.race4.event2={};
	myVars.races.race4.event1.id = "backstageSlides";
	myVars.races.race4.event1.type = "onmouseout";
	myVars.races.race4.event1.loc = "backstageSlides_LOC";
	myVars.races.race4.event1.isRead = "False";
	myVars.races.race4.event1.eventType = "@event1EventType@";
	myVars.races.race4.event2.id = "backstageSlides";
	myVars.races.race4.event2.type = "onmouseover";
	myVars.races.race4.event2.loc = "backstageSlides_LOC";
	myVars.races.race4.event2.isRead = "False";
	myVars.races.race4.event2.eventType = "@event2EventType@";
	myVars.races.race4.event1.executed= false;// true to disable, false to enable
	myVars.races.race4.event2.executed= false;// true to disable, false to enable
</script>

