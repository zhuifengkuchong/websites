var MSCOM = MSCOM || {};



MSCOM.utilities = function () {

	/**
	 * Ajax wrapper
	 * @param  {String}   url      URL for endpoint
	 * @param  {String}   type     type of call GET, POST
	 * @param  {String}   dataType Retrurn data type, JSON, HTML
	 * @param  {Function} callback What do we do with the data
	 * @return {none}
	 */
	function ajax(url,type,dataType,formData,callback) {
		$.ajax({
			url: url,
			type: type,
			dataType: dataType,
			data: formData,
			success: callback,
			error: function(xhr, textStatus, errorThrown) {
				console.log(xhr.status);
			}
		});
	}

	/**
	 * iOS6/7 bug with vw units and orientation change, hide and show tiles module, reset font-size
	 * @param {Obj} $tilesModule	tile module div
	 * @param {Obj} $tileRows		tile module rows
	 * @return {none}
	 */
	function repaintTiles($tilesModule,$tileRows){
		var height = $tilesModule.height();
		$tilesModule.css({'height':height});

		// iOS6/7 bug with vw units and orientation change, hide and show tiles module
		$tileRows.hide();

		setTimeout(function() {
			$tileRows.show();
			$tilesModule.removeAttr('style');
		}, 0);
	}

	/**
	 * Adds a timeout to methods that would otherwise
	 * be called multiple times when not needed
	 * @param  {function} func     callback
	 * @param  {number} wait       time to wait
	 * @param  {boolean} immediate trigger on leading edge
	 * @return {function}
	 */
	function debounce(func, wait, immediate) {
		var timeout;
		return function() {
			var context = this, args = arguments;
			var later = function() {
				timeout = null;
				if (!immediate) func.apply(context, args);
			};
			var callNow = immediate && !timeout;
			clearTimeout(timeout);
			timeout = setTimeout(later, wait);
			if (callNow) func.apply(context, args);
		};
	}

	/**
	 * Dynamically scales fonts
	 * @param  {object} $el jquery element
	 * @param  {number} min min font size
	 * @param  {number} max max font size
	 * @param  {number} mid how quickly font scales
	 * @return {none}
	 */
	function fontFlex($el, min, max, mid) {

		var mid_size = mid || 65,
			size = window.innerWidth / mid_size;

        if (size < min) size = min;
        if (size > max) size = max;

        $el.css('font-size', size + 'px');
	}

	/**
	 * adds font flex to tiles module
	 * @param  {object} $element jquery element
	 * @return {none}
	 */
	function tileFontFlex($element) {
		var wWidth = window.innerWidth;

		if ( wWidth >= 1280 ) {
			$element.css('font-size','');
		}
		else if ( wWidth < 1280 && wWidth >=1000 ) { // Small Desktop
			fontFlex($element, 12.8, 16, 80);

		} else if ( wWidth < 1000 && wWidth >=600 ) { // Tablet
			fontFlex($element, 12.8, 20, 50);

		} else { // Mobile
			fontFlex($element, 12.5, 60, 25);
		}
	}

	/**
	 * Adds background image to container
	 * @return {none}
	 */
	function responsiveBackground() {
		var $imgs = $('[data-url]');

		$imgs.each(function(){
			var URL = $(this).data('url'),
				fileName = URL.substr(0,URL.lastIndexOf(".")),
				fileSize = fileName.substr(0, fileName.lastIndexOf("-"))
				fileExt = URL.substr(URL.lastIndexOf(".") + 1,URL.length);
			
			$(this).css({
				"backgroundImage": "url("+fileSize+"-"+MSCOM.windowObj.viewport+'.'+fileExt+")"
			});
		});
	}



	/**
	 * Returns width of scrollbar if it exists
	 * @return {number} Width of scrollbar
	 */
	function getScrollBarWidth(){
		var width = 0;

		if (window.innerWidth) { //IE8 check
			width = window.innerWidth - $(window).width();
		}
		return width;
	}

	/**
	 * Applies "_last-three" and "_last-two" classes to dynamically set branch list items for
	 * correct bottom borders at three and two columns
	 * @return {none}
	 */
	function styleFluidGridList($fluidGridItems){
		var FG_ITEMS_LENGTH = $fluidGridItems.length,
			remainingThree = FG_ITEMS_LENGTH - (FG_ITEMS_LENGTH % 3),
			remainingTwo = FG_ITEMS_LENGTH - (FG_ITEMS_LENGTH % 2);

		if (FG_ITEMS_LENGTH % 3 === 0) {
			remainingThree -= 3;
		}
		if (FG_ITEMS_LENGTH % 2 === 0) {
			remainingTwo -= 2;
		}

		$fluidGridItems.each(function(index){
			if (index > remainingThree - 1 ) {
				$(this).addClass('_last-three');
			}
			if (index > remainingTwo - 1 ) {
				$(this).addClass('_last-two');
			}
		});
	}

	var api = {
		'ajax': ajax,
		'repaintTiles': repaintTiles,
		'debounce': debounce,
		'fontFlex': fontFlex,
		'tileFontFlex': tileFontFlex,
		'responsiveBackground': responsiveBackground,
		'getScrollBarWidth': getScrollBarWidth,
		'styleFluidGridList': styleFluidGridList
	};

	return api;
 }();



;var MSCOM = MSCOM || {};

MSCOM.windowObj = MSCOM.windowObj || {};

MSCOM = (function(MSCOM, $){

	var	$html = $('html'),
		$hamburger = $('.hamburger'),
		$trigger = $('.trigger'),
		$target = $('.target'),
		$mobileSearchTrigger = $('.mobile-nav').find('.search-button'),
		$searchTrigger = $('.sub-nav').find('.search-button'),
		$menuOverlay = $('.menu-overlay'),
		$mainNav = $('.main-nav'),
		$subNav = $('.sub-nav'),
		$smooth = $('._smooth'),
		$input = $('.search-bar').find('input'),
		$tileRow = $('.tiles-module .row').add('.talents-carousel'),
		$searchForm = $('.search-bar').find('form'),
		$searchInput = $searchForm.find('input[type="text"]'),
		$searchButton = $searchForm.find('button'),
		$threeUpCarousel = $('.employee-three-up').find('.row'),
		$faLocator = $('.falocator'),
		$svgImg = $('.svg-icon'),
		$hero = $('.hero');

	var loadingPartial = '<div class="loading"></div>';

	MSCOM.windowObj.windowWid = $(window).width();

	/**
	 * Initilaizes accordion for mobile menu
	 * @return {none}
	 */
	function initAccordion() {
		$subNav.on('click','.trigger', function(e){
			if(!$html.hasClass('large')){
				e.preventDefault();
				var ind = $trigger.index($(this));

				if(!$(this).hasClass('_active')){
					toggleItem();
				}

				$(this).parent('li').toggleClass('icon-up-nav icon-down-nav');
				$(this).toggleClass('_active');
				$($target[ind]).toggleClass('_active');
			}

			if($html.hasClass('touch') && MSCOM.windowObj.viewport === 'large') {
				e.preventDefault();
			}
		});
	}

	function checkNavigation() {
		if(MSCOM.windowObj.viewport === 'large') {
			toggleItem();
		}
	}

	function toggleItem() {
		$('.trigger._active').parent('li').toggleClass('icon-up-nav icon-down-nav');
		$trigger.removeClass('_active');
		$target.removeClass('_active');
	}

	function getPNG(el, original, isBG) {
		var newURL = original.replace("svg", "png");
		return newURL;
	}

	function svgIt(){
		if($svgImg.length > 0) {
			$svgImg.svgmagic({
				forceReplacements: false,
				replacementUriCreator: getPNG
			});
		}
	}


	/**
	 * Checks if placeholder is supported
	 * @return {none}
	 */
	function placeholderIsSupported() {
		var test = document.createElement('input');
		return ('placeholder' in test);
	}


	/**
	 * Registers all js for page and inits
	 * @return {none}
	 */
	function registerJS(){
		//we can require templates if needed

		var requires = ['Modules', 'Pages'];
		for( var i = 0; i < requires.length; i++) {
			var obj = MSCOM[requires[i]];
			for( var prop in obj){
				if(obj.hasOwnProperty(prop)){
					if(typeof obj[prop].init === 'function'){
						obj[prop].init();
					}
				}
			}
		}
	}

	/**
	 * Sets class per breakpoint
	 * @param {Number} val Window width
	 */

	function setBreakpointClass(val) {
		switch (true) {
			case (val >= 1000 && !$html.hasClass('large')):
				MSCOM.windowObj.viewport = 'large';
				$(MSCOM.windowObj).trigger('change');
				$html.addClass('large').removeClass('small medium');
				break;
			case (val <=999 && val >=600 && !$html.hasClass('medium')):
				MSCOM.windowObj.viewport = 'medium';
				$(MSCOM.windowObj).trigger('change');
				$html.addClass('medium').removeClass('small large');
				break;
			case (val > 0 && val <=599 && !$html.hasClass('small')):
				MSCOM.windowObj.viewport = 'small';
				$(MSCOM.windowObj).trigger('change');
				$html.addClass('small').removeClass('large medium');
				break;
			default:

			break;
		}
	}

	/**
	 * Returns viewport size
	 * @return {object}
	 */

	function viewport() {
		var e = window, a = 'inner';
		if (!('innerWidth' in window )) {
			a = 'client';
			e = document.documentElement || document.body;
		}
		return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
	}

	/**
	 * Sets Values for MSCOM.windowObj
	 * @return {none}
	 */

	function setValues() {
		MSCOM.windowObj.windowWid = viewport().width;
		MSCOM.windowObj.windowHT = viewport().height;
		setBreakpointClass(MSCOM.windowObj.windowWid);
	}


	/**
	 * Binds global elements
	 * @return {none}
	 */
	function binds() {

		$(window).on('resize', MSCOM.utilities.debounce(function() {
			setValues();
			fontFlexElements();
			reImg();
			initThreeUpEmployeeCarousel();
			checkNavigation();

		}, 250));

		$('.load-more-container').on('click', '.load-more-button a', loadMoreItems);

		$hamburger.on('click', openMenu);
		$mobileSearchTrigger.on('click', toggleSearch);
		$searchTrigger.on('click', toggleSearch);
		$menuOverlay.on('click', checkMenus);
		$mainNav.on('click', checkClick);
		$searchForm.on('submit', checkInput);
		$searchInput.on('keyup', toggleButton);
		$faLocator.on('submit', validateZip);
		$(MSCOM.windowObj).on('change', toggleMenu);
		$hero.on('click', clearTarget);
		
		if($smooth.length > 0 ){
			$smooth.smoothScroll();
		}
	}

	function clearTarget() {
		if($html.hasClass('touch') && MSCOM.windowObj.viewport === 'large') {
			return;
		}
	}

	function loadMoreItems(e) {
		e.preventDefault();
		var url = $(this).attr('href'),
			$parent = $(this).parent();

		$parent.before(loadingPartial);

		MSCOM.utilities.ajax(url,'GET','html', '', function(data){
			addMore(data, $parent);
		});
	}

	function addMore(data, $parent) {
		$('.loading').remove();
		$parent.before(data);
		$parent.remove();
	}

	function toggleButton() {
		if($(this).val().replace(/ /g,'') === '') {
			$searchButton.addClass('disabled').removeClass('blue');

		}else {
			$searchButton.addClass('blue').removeClass('disabled');
		}
	}

	function checkInput(e) {
		if($(this).find('button').hasClass('disabled')){
			return false;
		}
	}

	function validateZip() {
		var $input = $(this).find('input'),
				$errorMessage = $(this).find('.error-message'),
				inputVal = $input.val();


		if(inputVal.match(/^\d+$/) === null || inputVal.length < 5) {
			$(this).find('input').addClass('error');
			$errorMessage.css({'display': 'block'});
			return false;
		}
	}


	/**
	 * Opens mobile menu
	 * @param  {event} e mouse event
	 * @return {none}
	 */
	function openMenu(e) {
		$html.toggleClass('_mobile-open');
		if($html.hasClass('_search-open')) {
			toggleSearch(e);
		}
	}

	/**
	 * Checks if outside of search has been clicked
	 * @param  {event} e mouse event
	 * @return {none}
	 */
	function checkClick(e) {
		if($html.hasClass('large') && $html.hasClass('_search-open')){
			e.stopPropagation();
			toggleSearch(e);
		}
	}

	/**
	 * Toggles mobile menu if viewport changes
	 * @return {none}
	 */
	function toggleMenu() {
		if(MSCOM.windowObj.viewport === 'large' && $html.hasClass('_mobile-open')) {
			$html.toggleClass('_mobile-open');

		}
	}

	/**
	 * Toggles search open and close
	 * @param  {event} e mouse event
	 * @return {none}
	 */
	function toggleSearch(e) {
		e.stopPropagation();
		$html.toggleClass('_search-open');
		if($html.hasClass('_search-open')){
			$input.focus();
		}
	}

	/**
	 * Checks what menus are open
	 * @return {none}
	 */
	function checkMenus() {
		if($html.hasClass('_mobile-open')){
			$html.toggleClass('_mobile-open');
		}else if($html.hasClass('_search-open')){
			$html.toggleClass('_search-open');
			$searchTrigger.toggleClass('_active');
		}
	}

	/**
	 * Called in window resize bind
	 * @return {none}
	 */
    function fontFlexElements() {
    	MSCOM.utilities.tileFontFlex($tileRow);
    }

	/**
	 * Sets breakpoint on load
	 * @return {none}
	 */

	function setBreakpoint() {
		setValues();
	}

	/**
	 * adds filters for IE to responsive background images
	 * @return {none}
	 */
	function reImg() {
		MSCOM.utilities.responsiveBackground();
	}

	/**
	 * Sets the scrollbar width, 0 if no scrollbar
	 * @return {none}
	 */
	function calcScrollBarWidth(){
		MSCOM.windowObj.scrollBarWidth = MSCOM.utilities.getScrollBarWidth();
	}

	function initThreeUpEmployeeCarousel() {

		if(MSCOM.windowObj.viewport === "small" && !$threeUpCarousel.hasClass('slick-initialized')) {
			$threeUpCarousel.slick({
				draggable: false,
				centerMode: true,
				centerPadding: '6%',
				infinite: true,
				touchThreshold: 5
			});
		}else if($threeUpCarousel.hasClass('slick-initialized') && (MSCOM.windowObj.viewport === "medium") || MSCOM.windowObj.viewport === "large") {
			$threeUpCarousel.unslick();
		}
	}



	/**
	 * Inits the app
	 * @return {none}
	 */

	MSCOM.init = function(){
		calcScrollBarWidth();
		binds();
		setBreakpoint();
		initAccordion();
		registerJS();
		fontFlexElements(); // call once on page load
		reImg();
		initThreeUpEmployeeCarousel();
		svgIt();
	};

	return MSCOM;

}(MSCOM, jQuery));


/**
 * Check if MSCOM exists. If it does, init MSCOM
 * moved from bootstrap into app
 */


if (!window.console) {
	var console = {
		log : function() {}
	};
}

$(document).ready(function(){
	if( typeof MSCOM !== undefined) {
		MSCOM.init();
	}
});
;var MSCOM = MSCOM || {};

MSCOM.Modules = MSCOM.Modules || {};

MSCOM.Modules.branchLocator = function () {
	var $branchLocator = $('.branch-locator-module'),
		$selectState = $branchLocator.find('#selectState'),
		$selectCity = $branchLocator.find('#selectCity'),
		$loadData = $branchLocator.find('#loadData');

	// adapted from http:/$branch/www.morganstanley.com/about/careers/js/loadBranch.js on 10/21/14

	var xmlDoc,
		x; //xmlDoc

	/*
	 * Loads in XML according to appType
	 * @return {none}
	 */
	function initializeXML(callback) {
		var args = new Object();
		var query = location.search.substring(1); 
		var pairs = query.split("&");
		var header = "";
	
		for(var i = 0; i < pairs.length; i++) {
			var pos = pairs[i].indexOf('='); 
			if (pos == -1) continue; 
			var argname = pairs[i].substring(0,pos); 
			var value = pairs[i].substring(pos+1);
			args[argname] = unescape(value); 
		}

		if (args.type == 'RFA') {
			header = "Search for Resident Financial Advisor Openings";
			xmlDoc = AJAX_load('/msxml/RFA_branch_data.xml'+'?v='+Math.round(Math.random() * 100));
		} else if (args.type == 'PWAA') {
			header = "Search for Private Wealth Advisor Associate Openings";
			xmlDoc = AJAX_load('/msxml/PWAA_branch_data.xml'+'?v='+Math.round(Math.random() * 100));
		} else if (args.type == 'PBAA') {
			header = "Search for Private Banker Advisory Associate Openings";
			xmlDoc = AJAX_load('/msxml/PBAA_branch_data.xml'+'?v='+Math.round(Math.random() * 100));
		} else if (args.type == 'WAA') {
			header = "Search for Wealth Advisory Associate Openings";
			xmlDoc = AJAX_load('/msxml/FAA_branch_data.xml'+'?v='+Math.round(Math.random() * 100));
		} else {
			header = "Search for Financial Advisor Associate Openings";
			xmlDoc = AJAX_load('/msxml/FAA_branch_data.xml'+'?v='+Math.round(Math.random() * 100));
		}
		$("header > h2.icon-search").html(header);

		x = xmlDoc.getElementsByTagName("BRANCH");

		callback();

	}

	/*
	 * Returns XML file via AJAX
	 * @return {string}
	 */
	function AJAX_load(path){
		var result = null;

        $.ajax({
            url: path,
            type: 'get',
            dataType: 'xml',
            async: false,
            success: function(data) {
                result = data;
            }
        });

        return result;
	}

	/*
	 * Converts string to title case
	 * @return {string}
	 */
	function toTitleCase(str) {
		return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
	}

	/*
	 * Converts string to title case
	 * @return {string}
	 */
	function unique(arrayName) {
		var newArray = [];

		label:for(var i=0; i<arrayName.length;i++ )

		{
			for(var j=0; j<newArray.length;j++ )
			{
				if(newArray[j]==arrayName[i])
					continue label;
			}
			newArray[newArray.length] = arrayName[i];
		}

		return newArray.sort();
	}

	/*
	 * Clears load data div
	 * @return {none}
	 */
	function clearBranches(){
		$loadData.html('');
	}

	/*
	 * Add style classes to branch list items for tablet and mobile,
	 * three column to two column to one column due to variable amounts
	 * @return {none}
	 */
	function styleFluidGridList(){
		var $fluidGridItems = $loadData.find('.fg-list-item');

		MSCOM.utilities.styleFluidGridList($fluidGridItems);

	}

	/*
	 * Loads states into state select box
	 * @return {none}
	 */
	function loadStates() {
		// Legacy appSource code, unsure if still needed
			// document.getElementById("loadStatus").innerHTML = " ";
			// appSource = getApplicationSource();
			// if(appSource == "")
			// { appSource = urlRef(); }
			//

		var ary = [],
			aryFullStateName = {},
			opt,
			longName = "",
			optString = "",
			i;

		try
		{
			for(i=0; i<x.length; i++)
			{
				var stt = x[i].getElementsByTagName("STATE")[0].childNodes[0].nodeValue,
					sttFull = x[i].getElementsByTagName("STATEVAR")[0].childNodes[0].nodeValue;

				ary[i] = stt;
				aryFullStateName[stt] = sttFull;

			}
		}
		catch(e)
		{ document.getElementById("loadData").innerHTML = "Branch locator data could not be loaded. Please refresh the page."; }

		ary = unique(ary);

		for(i=0; i<ary.length; i++) {

			longName = ary[i] + ' - ' + aryFullStateName[ary[i]];
			opt = '<option value="'+ ary[i] +'">' + longName + '</option>';
			optString += opt;

		}

		$selectState.append(optString);

	}

	/*
	 * Loads cities into city select box after state has been selected
	 * @return {none}
	 */
	function loadCity(state,callback) {
		var ctyArray = [],
			ctyIndex = 0,
			opt,
			optString = "",
			callbackFunc = callback || function(){},
			i;

		$selectCity.html('<option value="Select">Select</option>');

		for(i=0; i<x.length; i++) {
			var stt = x[i].getElementsByTagName("STATE")[0].childNodes[0].nodeValue;
			if (stt == state)
			{
				var cty = x[i].getElementsByTagName("CITY")[0].childNodes[0].nodeValue;
				ctyArray[ctyIndex] = cty;
				ctyIndex++;
			}
		}
		ctyArray = unique(ctyArray);

		for(i=0; i<ctyArray.length; i++) {
			opt = '<option value="'+ ctyArray[i] +'">' + toTitleCase(ctyArray[i]) + '</option>';
			optString += opt;
		}
		$selectCity.append(optString);

		callbackFunc();

	}

	/*
	 * Displays branches of selected city
	 * @return {none}
	 */
	function displayBranches(city,state) {

		var data, divId, frmName, nme, ads, cty, stt, zip, phn, bIdVar, bVar, ctyId, i,
			sVar = " ",
			dataStr = "";

		for (i=0; i<x.length; i++) {

			cty = x[i].getElementsByTagName("CITY")[0].childNodes[0].nodeValue;
			stt = x[i].getElementsByTagName("STATE")[0].childNodes[0].nodeValue;

			if (cty == city && stt == state) {

				nme = x[i].getElementsByTagName("NAME")[0].childNodes[0].nodeValue;
				try
				{ ads = x[i].getElementsByTagName("ADDRESS")[0].childNodes[0].nodeValue; }
				catch(e)
				{ ads = " ";}
				cty = x[i].getElementsByTagName("CITY")[0].childNodes[0].nodeValue;
				stt = x[i].getElementsByTagName("STATE")[0].childNodes[0].nodeValue;
				try{
				phn = x[i].getElementsByTagName("PHONE")[0].childNodes[0].nodeValue;
				}
				catch(e)
				{phn = " ";}
				//EXCEPTION HANDLER FOR ZIP
				try
				{ zip = x[i].getElementsByTagName("ZIP")[0].childNodes[0].nodeValue; }
				catch(e)
				{ zip = " "; }
				bVar = x[i].getElementsByTagName("BRANCHVAR")[0].childNodes[0].nodeValue;
				sVar = x[i].getElementsByTagName("STATEVAR")[0].childNodes[0].nodeValue;
				bIdVar = x[i].getElementsByTagName("BRANCHID")[0].childNodes[0].nodeValue;

				//FORMAT DATA
				data =	'<li class="fg-list-item"><div class="fg-inner"><strong class="name">'+ toTitleCase(nme) +
						'</strong><div class="location"><span class="address-line-1">' +
						toTitleCase(ads) + '</span><span class="address-line-2">' + toTitleCase(cty) + ' ' + stt + ' '+ zip +
						'</span><span class="tel">Tel: <a class="tel " href="tel:' + phn + '">' + phn + '</a></span></div>' + '<form name="form'+i+'" id="form'+i+'">' +
						'<input type="hidden" value="'+ bVar + '"><input type="hidden" value="' + sVar +
						'"><input type="hidden" value="' + bIdVar+ '"><input type="button" value="Apply" id="btn' +
						i + '" data-analytics-button="locator apply button '+bVar+'"></form></div></li>';

				dataStr += data;

				// add anchor to url for back history
				document.location = '#' + window.escape(stt.toLowerCase())+ '-' + window.escape(cty.toLowerCase());
				// document.location = '#' + bIdVar;

			}


		}

		// append the data to fluid-grid-list
		$loadData.append(dataStr);

		styleFluidGridList();



	}

	//REDIRECT ON CLICK APPLY
	function redirectOnApply($form) {

		var salesforce = 'http://www.morganstanley.com/js/faa.html',
			formUrl,
			RETURL_BASE = "http://www.morganstanley.com/GWMG/RFE/webapp",
			UID = (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4()).toUpperCase(),
			$inputs = $form.find('input'),
			loc = $inputs[0].value,
			state = $inputs[1].value,
			branchID = $inputs[2].value;

		var args = new Object();
		var query = location.search.substring(1); 
		var pairs = query.split("&");
		var header = "";
	
		for(var i = 0; i < pairs.length; i++) {
			var pos = pairs[i].indexOf('='); 
			if (pos == -1) continue; 
			var argname = pairs[i].substring(0,pos); 
			var value = pairs[i].substring(pos+1);
			args[argname] = unescape(value); 
		}

		appSource = getApplicationSource();
		if (appSource == "") { appSource = urlRef(); }

		if (args.type == 'RFA') {
			var formURL = salesforce + "?jobId=a0R3000000AIiqa&jobName=Financial+Advisor+Associate+-+Resident+Financial+Advisor&jobLocation=Morgan+Stanley+Smith+Barney&docId=01530000000C2oU&orgId=00D300000001Spr&_cancelURL=http://www.morganstanley.com/careers&_sourcePage=%2Fjsp%2FJobDetail-IFRAME.jsp&__fp=NJ0hwtJusnkCyCrU0Re4uA%3D%3D&uid__c="+UID+"&_retURL="+RETURL_BASE+"/services/ThankYou%3fuid__c="+UID+"%26z=&applicationSource="+appSource+"&MSNR_Branch_State__c="+state+"&MSNR_Branch_Location__c="+loc+"&Training_Program__c=RFA (Resident Financial Advisor)"+"&branch_id="+branchID;
		} else if (args.type == 'PWAA') {
			var formURL = salesforce + "?jobId=a0R3000000BLrfA&jobName=Financial+Advisor+Associate+-+Resident+Financial+Advisor&jobLocation=Morgan+Stanley+Smith+Barney&docId=01530000000C2oU&orgId=00D300000001Spr&_cancelURL=http://www.morganstanley.com/careers&_sourcePage=%2Fjsp%2FJobDetail-IFRAME.jsp&__fp=NJ0hwtJusnkCyCrU0Re4uA%3D%3D&uid__c="+UID+"&_retURL="+RETURL_BASE+"/services/ThankYou%3fuid__c="+UID+"%26z=&applicationSource="+appSource+"&MSNR_Branch_State__c="+state+"&MSNR_Branch_Location__c="+loc+"&Training_Program__c=PWAA (Private Wealth Advisor Associate)"+"&branch_id="+branchID;
		} else if (args.type == 'PBAA') {
			var formURL = salesforce + "?jobId=a0R3000000B5JTJ&jobName=Financial+Advisor+Associate+-+Resident+Financial+Advisor&jobLocation=Morgan+Stanley+Smith+Barney&docId=01530000000C2oU&orgId=00D300000001Spr&_cancelURL=http://www.morganstanley.com/careers&_sourcePage=%2Fjsp%2FJobDetail-IFRAME.jsp&__fp=NJ0hwtJusnkCyCrU0Re4uA%3D%3D&uid__c="+UID+"&_retURL="+RETURL_BASE+"/services/ThankYou%3fuid__c="+UID+"%26z=&applicationSource="+appSource+"&MSNR_Branch_State__c="+state+"&MSNR_Branch_Location__c="+loc+"&Training_Program__c=PBAA (Private Banker Advisory Associate)"+"&branch_id="+branchID;
		} else if (args.type == 'WAA') {
			var formURL = salesforce + "?jobId=a0R30000007hYzQ&jobName=Financial+Advisor+Associate+-+Wealth+Advisory+Associate&jobLocation=Morgan+Stanley+Smith+Barney&docId=01530000000C2oU&orgId=00D300000001Spr&_cancelURL=http://www.morganstanley.com/careers&_sourcePage=%2Fjsp%2FJobDetail-IFRAME.jsp&__fp=NJ0hwtJusnkCyCrU0Re4uA%3D%3D&uid__c="+UID+"&_retURL="+RETURL_BASE+"/services/ThankYou%3fuid__c="+UID+"%26z=&applicationSource="+appSource+"&MSNR_Branch_State__c="+state+"&MSNR_Branch_Location__c="+loc+"&Training_Program__c=WAA (Wealth Advisor Associate)"+"&branch_id="+branchID;
		} else {
			formURL = salesforce + "?jobId=a0R30000000WVF2&jobName=Reach+for+Excellence&jobLocation=Morgan+Stanley&docId=01530000000C2oU&orgId=00D300000001Spr&_cancelURL=http://www.morganstanley.com/people&_sourcePage=%2Fjsp%2FJobDetail-IFRAME.jsp&__fp=NJ0hwtJusnkCyCrU0Re4uA%3D%3D&uid__c="+UID+"&_retURL="+RETURL_BASE+"/services/ThankYou%3fuid__c="+UID+"%26z=&applicationSource="+appSource+"&MSNR_Branch_State__c="+state+"&MSNR_Branch_Location__c="+loc+"&Training_Program__c=FAA (Financial Advisor)"+"&branch_id="+branchID;
		}


		formURL = '/people-opportunities/' + formURL;

		document.location = formURL;
	}

	/*
	 * Random number generator from MS legacy code
	 * @return {Number}
	 */
	function S4() {
		return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
	}

	function getApplicationSource() {
		var searchString = document.location.search;
		var Source = "";
		searchString = searchString.substring(1);
		var Pairs = searchString.split("&");

		for (i = 0; i < Pairs.length; i++) {
			var Pair = Pairs[i].split("=");
			var name = Pair[0];
			var value = Pair[1];
			if (name == "applicationSource") { Source = value;}
		}
		return Source;
	}

	function urlRef() {
		var str = "";
	
		if(document && document.referrer) { str = document.referrer; }
		
		if(str != "") {
			var len = str.length;
			var col = str.indexOf(':', 0);
			col = col + 3;
			var slash = str.indexOf('/', col);
			appSource = str.substring(col, slash);
		} else {
			appSource = document.domain;
		}
	
		return appSource;
	}
	
	/*
	 * Checks if value exits in xml with given tag
	 * @param {String} tag
	 * @param {String} value
	 * @return {Boolean}
	 */
	function valExists(tag,value) {
		var i;

		for (i=0; i<x.length; i++) {

			if (value === x[i].getElementsByTagName(tag)[0].childNodes[0].nodeValue) {
				return true;
			}
		}
		return false;
	}

	/*
	 * Sets State and City on form, displays branches
	 * @param {String} state
	 * @param {String} city
	 * @return {none}
	 */
	function setStateAndCity(state,city) {

		if ( valExists('STATE',state) && valExists('CITY',city)) {

			$selectState.val(state);
			loadCity(state, function(){
				$selectCity.val(city);
				displayBranches(city, state);
			});

			$('html, body').animate({scrollTop: $branchLocator.offset().top},0);
		}

	}

	/*
	 * Preloads form and results from Hash in url
	 * @return {none}
	 */
	function preloadFormAndResults() {

		var hash = $(location).attr('hash'),
			cty,
			stt;

		if (hash !== '') {

			stt = window.unescape(hash.split('-')[0]).toUpperCase().substr(1);
			cty = window.unescape(hash.split('-')[1]).toUpperCase();

			//set value of State select box and load cities, callback, load branches
			setStateAndCity(stt,cty);

		}

	}

	function init() {

		if ($branchLocator.length > 0) {

			if ( !$('body').hasClass('ie8') && !$('body').hasClass('ie9') ) {
				$('.custom-select').addClass('_active');
			}

			// load and initialize XML
			initializeXML(function(){

				// Callback: load in states from xml, check if hash exists in url, preload form and results
				loadStates();
				preloadFormAndResults();

			});

			// on user state select, update city select options
			$selectState.on('change',function(){
				var selectedState = $(this).val();

				loadCity(selectedState);

			});

			// on user city select, load in branches
			$selectCity.on('change',function(){
				var selectedState = $selectState.val(),
					selectedCity = $(this).val();

				clearBranches();
				displayBranches(selectedCity, selectedState);

			});

			// on apply button click, redirect to url with query string
			$loadData.on('click','input[type="button"]', function(e){
				e.preventDefault();

				var $locationForm = $(this).parent('form');

				redirectOnApply($locationForm);
			});
		}

	}

	var api = {
		'init': init
	};

	return api;
}();;var MSCOM = MSCOM || {};

MSCOM.Modules = MSCOM.Modules || {};

MSCOM.Modules.CarouselMobileOnly = function () {

	var $carouselMobileOnly = $('.carousel-mobile-only');

	function binds() {
		$(MSCOM.windowObj).on('change', function() { initCarousel(); });
	}

	function initCarousel() {

		if( MSCOM.windowObj.viewport !== "small" ) {
			$carouselMobileOnly.unslick();
			return;
		}

		if( $carouselMobileOnly.hasClass('slick-slider') ) {
			return;
		}

		$carouselMobileOnly.slick({
			dots: true,
			slide: 'li',
			slidesToShow: 1,
			arrows: true
		});

	}


	function init() {

		if ( $carouselMobileOnly.length > 0 ) {
			initCarousel();
			binds();
		}

	}

	var api = {
		'init': init
	};

	return api;
}();;var MSCOM = MSCOM || {};

MSCOM.Modules = MSCOM.Modules || {};

MSCOM.Modules.moduleName = function () {


	function init() {
		// console.log('do something');
	}

	var api = {
		'init': init
	};

	return api;
}();;var MSCOM = MSCOM || {};

MSCOM.Modules = MSCOM.Modules || {};

MSCOM.Modules.OneUpCarousel = function () {

	var $carouselSingle = $('.oneup-carousel');


	function init() {
		if ( $carouselSingle.hasClass('value-carousel')) {

			$carouselSingle.slick({
				slide: 'div',
				// adaptiveHeight: true,
				slidesToShow: 1,
				arrows: true,
				infinite: false,
				dots: true
			});



		} else {

			$carouselSingle.slick({
				slide: 'div',
				// adaptiveHeight: true,
				slidesToShow: 1,
				arrows: true,
				dots: true
			});

		}
	}

	var api = {
		'init': init
	};

	return api;
}();;var MSCOM = MSCOM || {};

MSCOM.Modules = MSCOM.Modules || {};

MSCOM.Modules.talentsCarousel = function () {

	var $talentsCarousel = $('.talents-carousel'),
		SLIDE_COUNT = $talentsCarousel.find('.tile').length,
		$window = $(window);

	// fix opacity flash and active slide bugs due to clone substitution/partial slides showing
	function activateClonedSlides(){
		if ( $talentsCarousel.slickCurrentSlide() === 0) {
			activateClonedSlideFirst();
		} else if ( $talentsCarousel.slickCurrentSlide() === SLIDE_COUNT-1 ) {
			activateClonedSlideLast();
		}
	}

	//helper function to quick fade slides
	function quickFadeSlides($slides){
		var $slidesContent = $slides.find('.content');

		$slides.css('opacity',1);
		$slidesContent.css('opacity',1);

		setTimeout(function(){
			$slides.css('opacity','');
			$slidesContent.css('opacity','');
		},500);
	}

	// fix opacity flash due to clone substitution
	function activateClonedSlideFirst(){

		if ( $window.width() < 600 ) {
			return false;
		} else if ($window.width() < 1000) {

			// change lastSlide context due to 2 slides showing
			var $slides = $talentsCarousel.find('[index="0"]').add('[index="-1"]');
			quickFadeSlides($slides);

		} else if ($window.width() < 1800) {
			var $slidesLarge = $talentsCarousel.find('[index="-2"]');
			quickFadeSlides($slidesLarge);

		} else {
			// change lastSlide context due to 5 slides showing vs 3
			var $slidesXLarge = $talentsCarousel.find('[index="-2"]').add('[index="-3"]');
			quickFadeSlides($slidesXLarge);

		}


	}

	// fix opacity flash due to clone substitution for viewport > 1800
	function activateClonedSlideLast(){

		if ( $window.width() < 600 ) {

			return false;

		} else if ( $window.width() < 1000) {

			var $slides = $talentsCarousel.find('[index="'+ (SLIDE_COUNT).toString() +'"]').add('[index="'+ (SLIDE_COUNT+1).toString() +'"]');
			quickFadeSlides($slides);

		} else if ($window.width() < 1800 ) {

			var $slidesLarge = $talentsCarousel.find('[index="'+ (SLIDE_COUNT-1).toString() +'"]');
			quickFadeSlides($slidesLarge);

		} else {

			// change lastSlide context due to 5 slides showing vs 3
			var $slidesXLarge = $talentsCarousel.find('[index="'+ (SLIDE_COUNT-1).toString() +'"]').add('[index="'+ (SLIDE_COUNT-2).toString() +'"]');
			quickFadeSlides($slidesXLarge);

		}
	}

	function bindLinks($tileLinks) {

		$tileLinks.on('click',function(e){
			e.preventDefault();

			var $link = $(this);

			if ( $link.hasClass('slick-active') ) {
				window.location.href = $link.attr('href');
			}

		});

	}

	function initCarousel() {
		$talentsCarousel.slick({
			centerMode: true,
			centerPadding: '2%',
			draggable: false,
			slide: 'a',
			slidesToShow: 5,
			responsive: [
			{
				breakpoint: 1800 - MSCOM.windowObj.scrollBarWidth, //subtract scrollbar so the breakpoints match with CSS
				settings: {
					centerMode: true,
					centerPadding: '10%',
					draggable: false,
					slidesToShow: 3,
				}
			},
			{
				breakpoint: 1280 - MSCOM.windowObj.scrollBarWidth,
				settings: {
					centerMode: true,
					centerPadding: '8.3%',
					slidesToShow: 3,
					draggable: false,
				}
			},
			{
				breakpoint: 1000 - MSCOM.windowObj.scrollBarWidth,
				settings: {
					centerMode: false,
					slidesToShow: 2.908,
					infinite: true,
					touchThreshold: 20
				}
			},
			{
				breakpoint: 600 - MSCOM.windowObj.scrollBarWidth,
				settings: {
					centerMode: true,
					centerPadding: '6%',
					slidesToShow: 1,
					infinite: true,
					touchThreshold: 5
				}
			}
			],
			onBeforeChange: function(){
				activateClonedSlides();
			},
			// fix active slide bug due to 2.9 slides showing on tablet, causes 3 slides to be active for the last 2 slides
			onAfterChange: function(){
				if ( window.innerWidth < 1001 && window.innerWidth > 600 ) {

					if ($talentsCarousel.slickCurrentSlide() === SLIDE_COUNT-1 || $talentsCarousel.slickCurrentSlide() === SLIDE_COUNT-2 ) {
						$talentsCarousel.find('.slick-active').first().removeClass('slick-active');
					}
				}
			},
			onInit: function(){
				// must be bound after init due to cloned slides
				bindLinks($talentsCarousel.find('.tile'));

				if ($talentsCarousel.hasClass('center-tile-5')) {

					setTimeout(function(){
					$talentsCarousel.slickGoTo(4);
					}, 250);
				}
			}
		});
	}

	function init() {

		if ($talentsCarousel.length > 0) {

			initCarousel();

		}
	}

	var api = {
		'init': init
	};

	return api;
}();;var MSCOM = MSCOM || {};

MSCOM.Modules = MSCOM.Modules || {};

MSCOM.Modules.video = function () {
	var $html = $('html'),
		$modalLink = $('.modal-open'),
		$videoStills = $('.image-wrapper a.modal-open');

	var token = 'r3aU2uz4jg9HQItatp1DLO3Hx4YKhBNe0_T_sPwezYF9WwGYEZqbrw..';

	function initModals() {
		if($modalLink.length > 0){
			$modalLink.each(function(i){
				var $modal = $(this),
					videoID = $(this).data('videoid'),
					uniqueClass = 'modal-'+videoID+'-'+i;

				$modal.addClass(uniqueClass); //add uniqueClass for each modal link

				$(this).modal({
					trigger: '.' + uniqueClass,
					olay:'div.video-overlay',
					modals:'div.modal',
					animationEffect: 'fadeIn',
					animationSpeed: 400,
					moveModalSpeed: 'fast',
					background: '000000',
					opacity: 0.8,
					openOnLoad: false,
					docClose: true,
					closeByEscape: true,
					moveOnScroll: true,
					resizeWindow: true,
					close:'.closeBtn',
					videoClass: 'video-modal',
					video: '//link.brightcove.com/services/player/bcpid3921437462001?bckey=AQ~~,AAAAlgiyhqE~,IVGrsNQ71r3IEJRIYAONVcd8vjHJ5-bq&bctid='+videoID+'&secureConnections=true&secureHTMLConnections=true'
			});
			});
		}
	}

	function binds() {
		$modalLink.on('click', function(e) {
			if($html.hasClass('touch')) {
				e.preventDefault();
				var videoID = $(this).data('videoid');
				$.ajax({
                    url: '//api.brightcove.com/services/library?command=find_video_by_id&video_id='+videoID+'&video_fields=name,length,FLVURL&media_delivery=http&secureHTMLConnections=true&secureHTMLConnections=true&token='+token+'',
                    dataType: 'jsonp',
                    success: function(data){
                       	window.location.replace(data.FLVURL);
                    }
                });
			}
		});
	}
	
	function init() {
		initModals();
		binds();
	}

	var api = {
	'init': init
	};

	return api;
}()