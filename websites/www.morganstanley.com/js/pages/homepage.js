var MSCOM = MSCOM || {};

MSCOM.Pages = MSCOM.Pages || {};

MSCOM.Pages.homepage = function () {
	var $tiles = $('.tiles-module .tile');

	var $twitter = $('.icon-twitter');
	// https://twitter.com/share?url=http://www.morganstanley.com/articles/coming-wave-of-wearable-device-technology/
	var tVars = args($twitter.attr("href").substring($twitter.attr("href").indexOf('?')+1));
	var tUrl = tVars['url'] || self.location;
	var $linkedin = $('.icon-linkedin');
	// http://www.linkedin.com/shareArticle?mini=true&url=http://www.morganstanley.com/articles/coming-wave-of-wearable-device-technology/&title=The%20Future%20of%20Wearables&summary=Wearables%20will%20disrupt%20and%20accelerate%20change%20in%20industries%20beyond%20technology,%20thereby%20turning%20%20%20%20%20%20%20%20the%20now%20remote%20and%20abstract%20Internet%20of%20Things%20into%20something%20personal%20and%20familiar.
	var lVars = args($linkedin.attr("href").substring($linkedin.attr("href").indexOf('?')+1));
	var lUrl = lVars['url'] || self.location;
	var $facebook = $('.icon-facebook');
	// https://www.facebook.com/sharer/sharer.php?m2w&s=100m&p[url]=http://www.morganstanley.com/articles/coming-wave-of-wearable-device-technology//&p[title]=The%20Future%20of%20Wearables
	var fVars = args($facebook.attr("href").substring($facebook.attr("href").indexOf('?')+1));
	var fUrl = fVars['p[url]'] || self.location;
	var bitlyURL = '';

	function args(str) {
		var args = new Object();
		var pairs = str.split("&");
	
		for(var i = 0; i < pairs.length; i++) {
			var pos = pairs[i].indexOf('='); 
			if (pos == -1) continue; 
			var argname = pairs[i].substring(0,pos); 
			var value = pairs[i].substring(pos+1);
			args[argname] = unescape(value); 
		}
		
		return args;
	}

	function shareOpen(type) {
		if (type == 'twitter') {
			var twitterUrl = 'https://twitter.com/share?url=' + bitlyURL;
			window.open(twitterUrl, "_blank", "");
		} else if (type == 'linkedin') {
			var linkedinUrl = 'http://www.linkedin.com/shareArticle?url=' + bitlyURL;
			
			linkedinUrl += '&mini=' + lVars['mini'];
			linkedinUrl += '&title=' + lVars['title'];
			linkedinUrl += '&summary=' + lVars['summary'];
			
			window.open(linkedinUrl, "_blank", "");
		} else if (type == 'facebook') {
			var facebookUrl = 'https://www.facebook.com/sharer/sharer.php?p[url]=' + bitlyURL;
			
			facebookUrl += '&p[title]=' + fVars['p[title]'];
			facebookUrl += '&s=' + fVars['s'];
			window.open(facebookUrl, "_blank", "");
		}
	}
	
	function initBitly(type, pageURL) {
		if (bitlyURL == '') {
			var url = pageURL;
			var token = "d6cc005b768aebeb1649727402ed08f079afabcd";
			$.ajax({
				url:'https://api-ssl.bitly.com/v3/shorten',
				data:{longUrl:url, access_token:token},
				dataType:"jsonp",
				success:function(v) {
					//alert(v.data.url);
					bitlyURL = v.data.url;
					shareOpen(type);
				},
				error: function (xhr, ajaxOptions, thrownError){
					//alert('status: '+ xhr.status+', error: '+thrownError);
			    }
			});
		} else {
			shareOpen(type);
		}
	}


	function addMobileHidden(){
		$tiles.each( function(index){
			var $tile = $(this);
				$col = $tile.parent();

			if (index > 4) {
				$tile.addClass('_mobile-hidden');
			}

			if ( $col.find('._mobile-hidden').length === $col.find('.tile').length ) { // if all tiles inside col are hidden
				$col.addClass('_mobile-hidden');
			}

		});
	}

	function init() {
		addMobileHidden();

		$twitter.on('click', function(){
			if (event.preventDefault) { event.preventDefault(); }
			else { event.returnValue = false; }
			
			initBitly('twitter', tUrl);
		});

		$linkedin.on('click', function(){
			if (event.preventDefault) { event.preventDefault(); }
			else { event.returnValue = false; }

			initBitly('linkedin', lUrl);
		});

		$facebook.on('click', function(){
			if (event.preventDefault) { event.preventDefault(); }
			else { event.returnValue = false; }

			initBitly('facebook', fUrl);
		});
	}

	var api = {
		'init': init
	};

	return api;
}();