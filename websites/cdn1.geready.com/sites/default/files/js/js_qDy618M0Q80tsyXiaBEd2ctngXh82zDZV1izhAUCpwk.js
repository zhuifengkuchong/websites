(function($) {
	Drupal.behaviors.ge_forms = {
		attach: function(context, settings) {

			$('#edit-product-services-consumer').bind('change', function() {
				if($('#edit-subject').val() === 'Products Services / Consumer' && $(this).val() === 'Home Appliances') {
					window.location.href = 'http://www.geappliances.com/service_and_support/contact/form_email.htm';
				}
			});
		}
	}
})(jQuery);
;
