/* Constants */
var rootDirectory = "";

// PINT_BrowserDetection("http://www.allergan.com/assets/scripts/global/browserupgrade.htm");

function init () {

	var fileName  = PINT_GetCurrentFileName();
	var directory = PINT_GetCurrentDirectory();

	PINT_global_locations();
	PINT_site_search();
	PINT_product_search();
	PINT_pipeline ();
	PINT_overlay ();

}

function cleanup () {}

window.onload = init;
window.onunload = cleanup;
