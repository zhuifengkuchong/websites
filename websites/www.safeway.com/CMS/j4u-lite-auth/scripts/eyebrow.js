/**
 * Created by lmath13 on 9/23/2014.
 */
var j4uLiteAuthModule = (function($) {
    var hasValidationError = false;
    this.validateCCorPhone = function() {
        var ccOrPhoneNumber = $('#j4ulite-mock-id-entry').val(),
            rexpPhoneNumber = /^([\d-. \(\)]+)$|^\d{10,19}$/,
            rexpCleanPhoneNumber = /^\d{10,19}/,
            validPhoneNumber = rexpPhoneNumber.exec(ccOrPhoneNumber), // validate the string
            regexCleanInput = /[^\d+]/g;

        if (validPhoneNumber !== null && rexpCleanPhoneNumber.exec(ccOrPhoneNumber.replace(regexCleanInput, '')) !== null) {
            window.location.href = 'http://www.safeway.com/ShopStores/Justforu-Lite.page#/J4u-Lite/'+ ccOrPhoneNumber;
        } else {
            j4uLiteAuthModule.hasValidationError = true;
            $('#j4ulite-mock-error-msg').addClass('shown');
            $('#j4ulite-mock-id-entry').addClass('errorBox');
        }

    };

    this.clearError = function() {
        j4uLiteAuthModule.hasValidationError = false;
        $('#j4ulite-mock-error-msg').removeClass('shown');
        $('#j4ulite-mock-id-entry').removeClass('errorBox')
    }

    this.checkLoggedIn = function() {
        var loggedIn = $('#globalui_header').hasClass('loggedin');
        if (loggedIn) {
           $('#ltLiteEyebrow').addClass('loggedIn'); //hide the eyebrow if user logged in.
        } else {
            $('#viewOffers').click(j4uLiteAuthModule.validateCCorPhone);
            $('#j4ulite-mock-id-entry').keyup(function (e) {
                if (e.which == 13) {
                    j4uLiteAuthModule.validateCCorPhone();
                } else if (j4uLiteAuthModule.hasValidationError) {
                    j4uLiteAuthModule.clearError();
                }
            });
            $('#j4ulite-mock-id-entry').focus(j4uLiteAuthModule.clearError);
        }
    }
    return this;
}(jQuery));
j4uLiteAuthModule.checkLoggedIn();

