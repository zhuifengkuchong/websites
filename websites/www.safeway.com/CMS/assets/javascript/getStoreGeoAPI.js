// This is inherited from legacy code
var domainNamews="safeway";
var usws=window.location.href;
var usaws=usws.split(".");
for (i=0;i<usaws.length;i++) {
    domainNamews=usaws[1];
    environmentws=usaws[0];
}

function weeklySpecialsLoad(){

    // The Weekly Specials fragment renders itself without GeoAPI data:
    document.getElementById("what_dinner_whole_generic").innerHTML="<a href='http://www.safeway.com/ShopStores/Weekly-Specials'><img src='../media/images/styleimages/ws_generic.gif'/*tpa=http://www.safeway.com/CMS/assets/media/images/styleimages/ws_generic.gif*/ width='220px' height='281px'></a>";
    document.getElementById("what_dinner_whole_generic").style.visibility="visible";
    document.getElementById("what_dinner_whole_generic").style.display="inline";
    document.getElementById("what_dinner_whole").style.visibility="hidden";
    document.getElementById("what_dinner_whole").style.display="none";

    // Weekly Specials provides a callback function for the GeoAPI
    // to execute if store data become available:
    GeoAPI.getStoreData(function(store) {
        //if (location.search.match(/^\?((\d+:\d+):..$)/)) { state = RegExp.$1; division = RegExp.$2; }
        //GeoAPI.state = state;
        // Return if storeData property is null
        //if(GeoAPI.storeData === null) return;
        if(store === null) return;

        var address1 = store.address;
        var city = store.city;
        var postalcode = store.postCode;
        var state = store.state;
        var clientkey = store.storeId;
        var banner = store.name;
        if (banner.toLowerCase() === "carrs") { banner = "carrsqc"; }
        banner = banner.replace(/[^a-zA-Z]+/gi, "");  // Remove any special characters or space in the store name. e.g. Dominick's
        var weekly_html = "http://weeklyspecials." + banner.toLowerCase() + ".com/customer_Frame.jsp?drpStoreID=" + clientkey + "&amp;showFlash=false";

        document.getElementById("what_dinner_whole_generic").style.visibility="hidden";
        document.getElementById("what_dinner_whole_generic").style.display="none";
        document.getElementById("what_dinner_whole").style.visibility="visible";
        document.getElementById("what_dinner_whole").style.display="inline";

        if ((address1) && (updateHTML("address1"))){document.getElementById("address1").innerHTML=address1;}
        //if ((address2) && (updateHTML("address2"))){document.getElementById("address2").innerHTML=address2  + " &nbsp;" ;}
        if ((city) && (updateHTML("city"))){document.getElementById("city").innerHTML=city + "&nbsp;" ;}
        if ((state) && (updateHTML("state"))){document.getElementById("state").innerHTML=state + "&nbsp;" ;}
        if ((postalcode) && (updateHTML("postalcode"))){document.getElementById("postalcode").innerHTML=postalcode;}
        if ((weekly_html) && (updateHTML("viewLink"))){document.getElementById("viewLink").value=weekly_html;}

        if ((banner) && (clientkey)){
            document.getElementById("clientkey").value=clientkey;
            document.getElementById("banner").value=banner;
            if (banner.toLowerCase()==="dominicks's"){document.getElementById("banner").value="dominicks";}
            document.getElementById("flyer_img").style.backgroundImage="url(http://weeklyspecials." + banner.toLowerCase() + ".com/info.jsp?drpStoreID=" + clientkey + ")";
            var strclientkey=clientkey;
            //writePersistentCookie ("mylocationX", strclientkey,  "4");
        }
        else {
            document.getElementById("what_dinner_whole_generic").innerHTML="<a href='http://www.safeway.com/ShopStores/Weekly-Specials'><img src='../media/images/styleimages/ws_generic.gif'/*tpa=http://www.safeway.com/CMS/assets/media/images/styleimages/ws_generic.gif*/ width='220px' height='281px'></a>";
            document.getElementById("what_dinner_whole_generic").style.visibility="visible";
            document.getElementById("what_dinner_whole_generic").style.display="inline";
            document.getElementById("what_dinner_whole").style.visibility="hidden";
            document.getElementById("what_dinner_whole").style.display="none";
        }

    });

    function updateHTML(elmId) {
        var elem = document.getElementById(elmId);
        var status = (typeof elem !== 'undefined' && elem !== null)? true : false;
        return status;
    }

}

//link for View button
function gotoWS(){

    var weeklyAdUrl = 'http://www.safeway.com/ShopStores/Weekly-Specials.page';
    //viewLink is used to determine if a store is available.
    if (document.getElementById("viewLink")){
        //the if(s_account) is all omniture logic - can be pulled when we move omniture to ensighten.
        if(s_account){
            var s=s_gi(s_account);
            s.linkTrackVars='eVar74';
            var ws_banner = 'safeway';
            parsedLocation=window.location.href;
            parsedLocationSplit=parsedLocation.split(".");
            ws_banner=parsedLocationSplit[1];
            ws_click=ws_banner+':home:widget:ws';
            s.eVar74=ws_click;
            s.tl(this,'o',ws_click);
        }
        weeklyAdUrl = document.getElementById("viewLink").value;
        if(swca && swca.weeklyad && swca.weeklyad.domain){
            weeklyAdUrl  = weeklyAdUrl.replace(/((?:\w+:)?\/\/)([^\/]*)(\/.*)?/,"$1"+swca.weeklyad.domain+"$3");
        }
    }
    window.location.replace(weeklyAdUrl);

}

function gotoWSStore(){
    window.location.replace("http://www.safeway.com/ShopStores/Weekly-Specials.page");
}

function addLoadEvent(func) {
    var oldonload = window.onload;
    if (typeof window.onload != 'function') {
        window.onload = func;
    } else {
        window.onload = function() {
            if (oldonload) {
                oldonload();
            }
            func();
        }
    }
}
addLoadEvent(weeklySpecialsLoad); 