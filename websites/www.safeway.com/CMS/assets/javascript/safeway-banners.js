/**
 *  safeway-banners.js
 *
 *  Requires the following scripts
 *  * common_utilities.js (goURL,readcookie,setCookie)
 *
 *  Requires the following variables set
 *  * varAuthLogin
 *  * varAuthLogout
 *  * varRssMyAccountUrl
 *  * varRssUserRegUrl
 *  * varRssUpdateContactInfoUrl
 *  * varDomainUrl
 *  * varDomainLabel
 *
 *  Sets the following variables
 *  * varMyCardUrl
 *  * varMyListUrl
 *  * logonId
 *  * loggedOn
 *
 *
 */


//Set variables
var varMyCardUrl,
    varMyListUrl,
    lowerDomainLabel = varDomainLabel.toLowerCase().replace(/\s/g,''),
    logonId;

(function() {
    //Logic to establish the namespace
    var swy = window.Safeway || (window.Safeway = {}),
        config = swy.config || (swy.config = {}),
        banner = swy.banner || (swy.banner = {});

    banner.init = function(){
        //Set Vars
        varMyCardUrl        = 'http://www.safeway.com/ShopStores/MyCard.page';
        varMyListUrl        = 'http://www.safeway.com/ShopStores/MyList.page';

        //set cookies
        setCookie('SWY_BANNER', lowerDomainLabel,0);
        setCookie('SWY_API_KEY ', 'emjou',0);
        setCookie('SWY_VERSION', '1.0',0);

        logonId = readcookie('SWY_LOGONID', ';');
        loggedOn = readcookie('swyConsumerDirectoryPro', ';');
        if(loggedOn == null || !loggedOn || loggedOn == 'null') {
            loggedOn = readcookie('SWY_REMEMBERME_UID', ';');
        }
        if(loggedOn == null || !loggedOn || loggedOn == 'null') {
            loggedOn = '';
        }
        if(!loggedOn || logonId == null || logonId == '' || logonId == 'null') {
            logonId = 'Guest';
        } else {
            if(logonId.length > 30) {
                logonId = logonId.substring(0, 100);
            }
        }
        if(loggedOn == '') {
            var remembered = readcookie('SWY_ISREMEMBERED',';');
            if(remembered && remembered == 'true') {
            } else {
                logonId = 'Guest';
            }
        }


    }

    config.bannerName = lowerDomainLabel;
})();


function setInnerHTML(inputname, value) {
    var obj = document.getElementById(inputname);
    if(obj != null) {
        obj.innerHTML = value;
    }
}
function updateClass(id, clsName) {
    var obj = document.getElementById(id);
    if(obj != null) {
        obj.className = clsName;
    }
}
function updateAttribute(id, attribute, value){
    var obj = document.getElementById(id);
    if(obj != null) {
        if(logonId!=='Guest') {
            value ="Welcome: "+value;
            obj.setAttribute(attribute, value);
        }
        else{
            obj.setAttribute(attribute, value);
        }
    }

}
// RSS/IASS TODO: change querystring format of redirect
function openssoLogin(currentLocation) {
    currentLocation = currentLocation ||  window.location.href;
    if(currentLocation.indexOf('http://www.safeway.com/CMS/assets/javascript/OSSO-Login.page')>-1){
        //don't want to do anything if we're already on the login page.
        return false;
    }
    var caller=checkCaller(currentLocation);
    // RSS/IASS - Temporary fix to avoid redirecting back to registration page after user signs in
    if ((currentLocation.indexOf('http://www.safeway.com/CMS/assets/javascript/RSS-Forms-UserRegistrationForm.page')>-1)|| (currentLocation.indexOf('http://www.safeway.com/CMS/assets/javascript/RSS-Forms-UserRegistrationSuccess.page')>-1)|| (currentLocation.indexOf('http://www.safeway.com/CMS/assets/javascript/RSS-Forms-UserChangePasswordForm.page')>-1)||(currentLocation.indexOf('http://www.safeway.com/CMS/assets/javascript/RSS-Forms-UserResetPasswordForm.page')>-1)||(currentLocation.indexOf('http://www.safeway.com/CMS/assets/javascript/RSS-Forms-ForgotUserIdForm.page')>-1) ||(currentLocation.indexOf('http://www.safeway.com/CMS/assets/javascript/RSS-Forms-AccountDisabled.page')>-1) || (currentLocation.indexOf('http://www.safeway.com/CMS/assets/javascript/RSS-Forms-AccountLocked.page')>-1) || (currentLocation.indexOf('http://www.safeway.com/CMS/assets/javascript/RSS-Forms-AccountSignedOut.page')>-1)) {
        caller='/';
    }

    // RSS/IASS: remove cookie defect 612
    setCookie('LtpaToken', 'remove');
    setCookie('LtpaToken2', 'remove');
    // RSS/IASS: remove cookie defect 467
    setCookie('swyAuthCookie', "remove");
    // RSS/IASS: remove cookie defect 612
    setCookie('swyConsumerDirectoryPro', 'remove');
    goURL(varAuthLogin + '?goto='+encodeURIComponent(caller));
}
function openssoLogoff() {
    var caller = window.location.href;
    if(caller.indexOf('StandaloneHeaderView') <= -1) {
        var remembered = readcookie('SWY_ISREMEMBERED',';');
        setCookie('LtpaToken', 'remove');
        setCookie('LtpaToken2', 'remove');
        // RSS/IASS: remove cookie defect 467
        setCookie('swyAuthCookie', "remove");
        goURL(varAuthLogout);
    }else{
        goURL(varAuthLogout);
    }
}
function rssReg() {
    var caller=checkCaller();
    goURL(varRssUserRegUrl + '?goto='+encodeURIComponent(caller));
}
function rssMyAccount() {
    var caller=checkCaller();
    goURL(varRssMyAccountUrl + '?goto='+encodeURIComponent(caller));
}
function rssUpdateContactInfo() {
    var caller=checkCaller();
    goURL(varRssUpdateContactInfoUrl + '?goto='+encodeURIComponent(caller));
}
function myList() {
    goURL(varMyListUrl);
}
function myCard() {
    goURL(varMyCardUrl);
}
function checkCaller(caller){
    caller = caller || window.location.href;
    if(caller.indexOf('StandaloneHeaderView') > -1){
        var gotoUrl=getURLParam("goto", "");
        if(gotoUrl!=null && gotoUrl!="") {
            caller = gotoUrl;
        }else{
            caller = document.referrer;
        }
    }
    return caller;
}


