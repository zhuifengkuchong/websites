var dataLayer = []; //Google Tag Manager dataLayer

var Stats = function(){
  s_code,
  _init = function(){

      s.pfxID = "lrn";

      setVarsFromCrumbs();
      setEvents();
      checkCampaign();
      if(s.simplepageName){
        writeSCode();
      }
  },

  checkCampaign = function(){
    var campaign = getURLParameter('cid');
    if(campaign != "null"){
      if(s.events){
        s.events= s.events + ',event20,event48';
      }else{
        s.events= 'event20,event48';
      }
    }
  },

  setVarsFromCrumbs =function(){
    var crumbs = jQuery("#crumbs");
    var crumbTitle = jQuery('.crumb-title').text();
    if (crumbs.size() > 0 && crumbTitle !== '') {
      s.simplepageName = jQuery('.crumb-title', crumbs).text().toLowerCase();
      s.detailpageName =  s.simplepageName;
      var prop3 = '';
      var prop22 = '';
      var allCrumbs = jQuery("a",crumbs);
      var l = allCrumbs.length;
      var separator = ": ";
      for(var i = 0; i<l; i++){
        // don't want first (home) link
        if(i != 0){
          var crumbText = jQuery(allCrumbs[i]).text().toLowerCase();
          prop22 = prop22 + crumbText + separator;
          if(i < 4){
            if((i > 1 && i === l-1) || (i === 1 && l === 2) || (i === 3)){
              separator = "";
            }
            prop3 = prop3 + crumbText + separator;
            separator = ": ";
          }
        } 
      }
      s.prop3 = prop3.toLowerCase();
      s.prop22 = prop22.toLowerCase() + s.simplepageName.toLowerCase();

      if(allCrumbs.size() === 1){
        s.channel = jQuery('#crumbs .crumb-title').text().toLowerCase();
      }else{
        s.channel = jQuery('#crumbs a:eq(1)').text().toLowerCase();
      }

    } else {
      customCrumbs();
    }
  },
  customCrumbs = function(){
    var fullURL = location.href;
    var homepageURL = "https://" + location.hostname + "/";
    var crumbtrail = [];

    if(fullURL == homepageURL){
      crumbtrail[0] = "homepage";

    }else if(fullURL.indexOf('/about') != -1){
      crumbtrail[0] = "about"; 

    }else if(fullURL.indexOf('/perspectives') != -1){
      crumbtrail[0] = "perspectives"; 
      
      if(fullURL.indexOf('/nearly-undetectable-submarines-threaten-u-s-ships-and-shipping') != -1){
        crumbtrail[1] = "nearly undetectable submarines threaten u.s. ships and shipping";
      } else if(fullURL.indexOf('/cancer-patients-urgently-need-effective-genetically-targeted-tre') != -1){
        crumbtrail[1] = "cancer patients urgently need effective, genetically-targeted treatments";
      } else if(fullURL.indexOf('/the-future-demands-smarter-power-grids-that-enable-the-two-way-f') != -1){
        crumbtrail[1] = "the future demands smarter power grids that enable the two-way flow of both energy & data";
      }
    }else if(fullURL.indexOf('/feature') != -1){
      crumbtrail[0] = "feature";

      if(fullURL.indexOf('/world-cup-fever') != -1){
        crumbtrail[1] = "world cup fever";
      } else if(fullURL.indexOf('/united-to-ascend') != -1){
        crumbtrail[1] = "united to ascend";
      } else if(fullURL.indexOf('/meet-roger-krone') != -1){
        crumbtrail[1] = "meet roger krone";
      } else if(fullURL.indexOf('/the-solutions-man') != -1){
        crumbtrail[1] = "the solutions man";
      } else if(fullURL.indexOf('/president-obama-visits') != -1){
        crumbtrail[1] = "president obama visits";
      }
    }else if(fullURL.indexOf('/careers') != -1){
      crumbtrail[0] = "careers";
  
    }else if((fullURL.indexOf('/engineering') != -1) || (fullURL.indexOf('/health') != -1) || (fullURL.indexOf('/natsec') != -1)){
      crumbtrail[0] = "industries"; 
      
      if(fullURL.indexOf('/newsroom') != -1){
        crumbtrail[1] = "newsroom"; 
        var pathArray = fullURL.split( '/' );
        if(pathArray[6]){
          s.prop4 = "newsroom search: " + jQuery.trim(jQuery(".dropdown option[value='"+pathArray[6]+"']").text().toLowerCase());
        }
        if(pathArray[7]){
          s.prop2 = pathArray[7];
        }

        if(s.prop2 || s.prop4){
          s.events="event17,event49";  // Industry Newsroom Search (17) w3
          s.pfxID = "srh";

          dataLayerPush('event17');
        }
      }   

      if(fullURL.indexOf('/engineering') != -1){
        crumbtrail[1] = "engineering"; 
      
        if(fullURL.indexOf('/projects') != -1){
        crumbtrail[2] = "projects"; 
          var pathArray = fullURL.split( '/' );
          if(pathArray[5]){
            s.prop2 = pathArray[5];
          }
          if(pathArray[6]){
            s.prop4 = "project search: " + jQuery.trim(jQuery(".dropdown option[value='"+pathArray[6]+"']").text().toLowerCase());
          }

          if(s.prop2){
            s.events="event16,event49";  // Industry Newsroom Search (17) w3
            s.pfxID = "srh";

            dataLayerPush('event16');
          }
        }  
      }else if(fullURL.indexOf('/health') != -1){
        crumbtrail[1] = "health"; 
      }else if(fullURL.indexOf('/natsec') != -1){
        crumbtrail[1] = "national security";
      }

      if(fullURL.indexOf('/who-we-serve') != -1){
        crumbtrail[2] = "who we serve"; 
      }else if(fullURL.indexOf('/capabilities') != -1){
        crumbtrail[2] = "capabilities"; 
      }else if(fullURL.indexOf('/projects') != -1){
        crumbtrail[2] = "projects";
      }else if(fullURL.indexOf('/solutions') != -1){
        crumbtrail[2] = "solutions";
      }else if(fullURL.indexOf('/newsroom') != -1){
        crumbtrail[2] = "newsroom";
      }
    }else if(fullURL.indexOf('/uk') != -1){
     crumbtrail[0] = "uk"; 
    }else{
        var full_location = window.location.pathname;
        if (full_location.charAt(0) == "/") {full_location = full_location.substr(1);}
        var sections = full_location.split("/"); 
        for (var i = 0; i < sections.length; i++) {
            var cleaned_section = sections[i].replace(/-/g, " "); 
            crumbtrail[i] = cleaned_section;
        }

    }

    s.channel = crumbtrail[0];
    var crumbtrailString = "";
    var prop3 = "";
    var prop22 = "";

    for(var i = 0; i<crumbtrail.length; i++){
      if(i === 0){
        crumbtrailString = crumbtrail[i];
        if(crumbtrail.length == 1){
          s.prop22 = crumbtrail[0];
        }
      }else if(i === crumbtrail.length-1 && crumbtrail.length !== 1){
        s.prop3 = crumbtrailString;
        s.prop22 = crumbtrailString + ': ' + crumbtrail[i];
      }else{
        crumbtrailString = crumbtrailString + ': ' + crumbtrail[i];
      }
    }
    
    /*s.prop3 = jQuery.grep(crumbtrailString, function(value, i) {
      return value != crumbtrail[crumbtrail.length-1];
    });

    s.prop22 = crumbtrailString;*/
    
    s.simplepageName = crumbtrail[crumbtrail.length-1];
    s.detailpageName =  s.simplepageName;

  },
  matchURLHash = function(hash){
    var match,
    hashURL = location.hash;
    if(hashURL.indexOf(hash) != -1){
      match = true;
    }else{
      match = false;
    }
    return match;
  },
  accordianExpand = function(accordianText){
    s.pfxID = "acc";
    s.events="event13,event49";  // Accordian Expand/Collapse (8) w1
    s.detailpageName = s.simplepageName + ": " + accordianText;
    writeSCode();

    dataLayerPush('event13');
  },

  getURLParameter = function(name) {
    return decodeURI(
        (RegExp(name + '=' + '(.+?)(&|jQuery)').exec(location.search)||[,null])[1]
    );
  },

  dataLayerPush = function(data){
    dataLayer.push({'event': data});
    //console.log(dataLayer);
    //console.log(s.events);
  },

  writeSCode = function() {
    var s_code=s.t();
    if(s_code)document.write(s_code);
  },

  linkedinCallback = function() {
    s.events="event10"; // Leads Initiated (1) w3
    s.prop19="LinkedIn";
    writeSCode();
    dataLayerPush(s.events);
  },
  setEvents = function() {
    //PAGE LOAD EVENTS

    jQuery("#select-content a").click(function(e) {
      s.events="event21,event50"; // Leads Initiated (1) w3
      writeSCode();

      dataLayerPush('event21');
    });

    // carousel clicks
    jQuery(".directional-controls").click(function() {
      s.events="event4,event48";  // Slideshow views (4) w1
      s.pfxID = "sls";
      writeSCode();

      dataLayerPush('event4');
    });

    // panorama style carousel clicks
    jQuery(".directional-controls-circle").click(function() {
      var carouselLink = this;
      var carouselWrap = jQuery(this).parent().parent();

      if ( jQuery("body").hasClass("page-perspectives") ) {
        if(jQuery(carouselWrap).hasClass('getting-ready-wrap')){
          if(jQuery(carouselLink).hasClass('next')){ 
            s.detailpageName = "the problem: " + jQuery(".getting-ready-wrap #getting-ready .ready-tip:eq(2) h2").text().toLowerCase();
          } else {
            s.detailpageName = "the problem: " + jQuery(".getting-ready-wrap #getting-ready .ready-tip:eq(0) h2").text().toLowerCase();
          }
        }

        if(jQuery(carouselWrap).hasClass('getting-ready-wrap2')){
          if(jQuery(carouselLink).hasClass('next')){ 
            s.detailpageName = "the solution: " + jQuery(".getting-ready-wrap2 #getting-ready .ready-tip:eq(2)").attr('id').toLowerCase();
          } else {
            s.detailpageName = "the solution: " + jQuery(".getting-ready-wrap2 #getting-ready .ready-tip:eq(0)").attr('id').toLowerCase();
          }
        }
      }

      s.events="event4,event48";  // Slideshow views (4) w1
      s.pfxID = "sls";
      writeSCode();

      dataLayerPush('event4');
    });

    //Lead Initiated
    jQuery("#edit-submitted-please-select-a-reason-for-contacting-us").change(function() {
      s.prop1=jQuery("#edit-submitted-please-select-a-reason-for-contacting-us").val();
      s.events="event1,event49"; // Leads Initiated (1) w3
      writeSCode();

      dataLayerPush('event1');

      jQuery.cookie('contact_form_type', s.prop1);
    });


    if(jQuery("body").hasClass("page-contact")){
      var hash = matchURLHash("#form/");
      if(hash == true){

        var formType = location.hash.split('/')[1];
        s.prop1=formType;
        s.events="event1,event49"; // Leads Initiated (1) w3

        dataLayerPush('event1');
        
        jQuery.cookie('contact_form_type', s.prop1);
      }
    }

    //Lead Completed
    //jQuery("[id^=edit-submit--]").click(function() {
    if( jQuery(".page-contact #messages").size() > 0 ){
      if(jQuery.cookie('contact_form_type')){
        s.prop1 = jQuery.cookie('contact_form_type');
      }
      s.events="event2,event50"; // Leads Completed (2) w5
      dataLayerPush('event2');
    }

    //Search Completed
    jQuery(".header-wrap .search-form").submit(function(e) {
      s.prop2= jQuery("#search", this).val();
      s.prop4="internal search:";
      //console.log('SUBMITTED!!');
      s.events="event3,event49"; // Internal Search (3) w3
      s.pfxID = "srh";
      //console.log(s.prop2);
      writeSCode();

      dataLayerPush('event3');
    });

    //Career Search
    jQuery("form[name='careers-keyword-search'] .search-submit").click(function(e) {
      s.prop2 = jQuery("form[name='careers-keyword-search'] input").val();
      s.prop4 = "career search:";
      s.events ="event5,event49,event21,event50";  // Career Search (5) w3
      s.pfxID = "srh";
      writeSCode();

      dataLayerPush('event5');
      dataLayerPush('event21');

    });

    // Career List Search Form: /careers/opportunities/
    jQuery(".page-careers-opportunities #career").submit(function() {
      s.events="event6,event48";  // Career List Search Form (6) w1
      s.pfxID = "srh";
      writeSCode();

      dataLayerPush('event6');
    });

    // TabView
    jQuery('.tabs a').click(function() {
      
      s.pfxID = "tabs";
      s.events="event12,event48";  // TabView (7) w1
      
      if(!jQuery("body").hasClass('node-type-static-imports')){
        
        if(jQuery(this).parent().parent().parent().prev().is("h2")){
          s.detailpageName = jQuery(this).parent().parent().parent().prev().text().toLowerCase() + ': ' + jQuery(this).text().toLowerCase();
        }else{
          s.detailpageName = s.simplepageName + ": " +  jQuery(this).text().toLowerCase();
        }
      }else{
        var pageHeader = jQuery("#block-system-main .node-content h1").text().toLowerCase();
        var tabTitle = jQuery("span", this).text().toLowerCase();
        s.pfxID = "tabs";
        s.events="event12,event48";  // TabView (7) w1
        s.detailPageName = pageHeader+": "+tabTitle;
        
      }

      writeSCode();

      dataLayerPush('event12');
    });

    // Download PDF
    jQuery(".pdf").click(function() {
      s.events="event11,event50";  // Download PDF (11) w5
      writeSCode();

      dataLayerPush('event11');
    });

    // Page pulldown
    jQuery(".next-section").click(function() {
      s.events="event15,event48";  // Page pulldown (15) w1
      writeSCode();

      dataLayerPush('event15');
    });

    // Social Media Exit Links
    jQuery(".socials .ir").click(function() {
      s.events="event22,event49";  // Social Media Exit Links (22) w3
      writeSCode();

      dataLayerPush('event22');
    });

    // Leadership Pages
    //jQuery(".bio-item-wrap a").click(function() {
    if( jQuery("body").hasClass("page-about-org")){
      s.events="event23,event49";  // Leadership Pages (23) w3
      dataLayerPush('event23');
    }
    if( jQuery("body").hasClass("node-type-person")){
      s.events="event23,event49";  // Leadership Pages (23) w3
      dataLayerPush('event23');
    }

    // Annual Report-Explore (Used HREF attribute since there is no unique id or class)
    jQuery('a[href="https://www.leidos.com/sites/default/files/u14/AnnualReport2011.pdf"]').click(function() {
      s.events="event25";  // Annual Report-Explore (25)
      writeSCode();

      dataLayerPush('event25');
    });

    // Location Pages
    // drop down for locations
    jQuery('#us-locations').change(function() {
      s.events="event30,event50";  // Location Pages (30)
      writeSCode();

      dataLayerPush('event30');
    });


    jQuery('#RestrictToCategoryState').change(function() {
      s.events="event30,event50";  // Location Pages (30)
      writeSCode();

      dataLayerPush('event30');
    });

    // drop down for locations
    jQuery('#ui-id-1').change(function() {
      s.events="event30,event50";  // Location Pages (30)
      writeSCode();

      dataLayerPush('event30');
    });

    // drop down for locations
    jQuery('#RestrictToCategoryCountry').change(function() {
      s.events="event30,event50";  // Location Pages (30)
      writeSCode();

      dataLayerPush('event30');
    });

    // Suppliers Portal
    jQuery('a[href="https://www.leidos.com/suppliers"]').click(function() {
      s.events="event27,event49";  // Supplier Portal (27) w3
      writeSCode();

      dataLayerPush('event27');
    });


    //Contract Center
    //jQuery('a[href="https://www.leidos.com/contractcenter"]').click(function() {
    if(jQuery("body").hasClass('page-contractcenter')){
      s.events="event29,event50";  // Contract Center (29) w5
      dataLayerPush('event29');
    }

    // Social Media Direct OptIn (LinkedIn)
    window.twttr = (function (d,s,id) {
      var t, js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return; js=d.createElement(s); js.id=id;
      js.src="../../../../../../platform.twitter.com/widgets.js"/*tpa=https://platform.twitter.com/widgets.js*/; fjs.parentNode.insertBefore(js, fjs);
      return window.twttr || (t = { _e: [], ready: function(f){ t._e.push(f) } });
    }(document, "script", "twitter-wjs"));

    function followAnalytics (intentEvent) {
      s.events="event14,event50";  // Social Media Direct OptIn (9) w5 (Twitter)
      s.prop19="Twitter";
      writeSCode();
      dataLayerPush(s.events);
    }
    if(typeof twttr !== 'undefined'){
        twttr.ready(function (twttr) {
          // Now bind our custom intent events
          twttr.events.bind('click', followAnalytics);
        });
    };
        

  };
  return {
    init : _init,
    linkedinCallback : linkedinCallback,
    accordianExpand : accordianExpand
  };
},
stats = new Stats();
jQuery(stats.init);
