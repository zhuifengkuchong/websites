var background,
    sliderPosition = 0;

var Background = function () {
    var $window,
        $sections,
        flip,

        isTouch,
        speed,
        sectionsLength;

    function updateBackgroundTopPosition() {
        var onPosition = $window.scrollTop() + ($window.height() / 1.3);

        for (var i = 0; i < sectionsLength; ++i) {
            var $thisSection = $sections.eq(i),
                newTopPosition = -((($window.scrollTop() - $thisSection.position().top) / speed) >> 0);

            if (isTouch === false) {
                $sections[i].style.backgroundPosition = 'center ' + newTopPosition + 'px';
            }

            if ($thisSection.offset().top < onPosition || isTouch === true) {
                $thisSection.addClass('inview');
            } else if ($thisSection.hasClass('inview')) {
                $thisSection.removeClass('inview');
            }
        }

        if ($window.width() > 800) {
            if (Modernizr.cssanimations) {
                $("#home-recipe-container").css("-webkit-transform", "translate3d(0,0,0)");
            } else {
                $("#home-recipe-container").css("left", "0");
            }
        }
    }

    function renderBackgroundSize() {

        if (isTouch === false) {
            var newHeight = $window.height() - 150;

            $sections.css('height', $window.height() + 'px');
            $('#frame-01').css('height', newHeight + 70);
            $('#frame-02').css('height', newHeight + 70);
            $('#frame-03').css('height', newHeight + 70);

            var heightAvailable = $('#frame-01').height() - ($('#frame-01 h2').height() + $('#frame-01 h2').position().top);

            $("#ytcontainer-container").css("max-width", heightAvailable);
            
        }
        
    }

    function animateArrow() {
        flip = flip ? 0 : 1

        $('#frame-01 .icon-arrow-down').animate({ bottom: 5 + flip * 20 },
            {
                duration: 1000,
                easting: "ease",
                queue: false,
                complete: animateArrow
            }
       );
    }

    function leftArrowClickHandler() {
        if(sliderPosition > 0) {
            sliderPosition--;
            $(".right-arrow").css("opacity", "1");
        }

        if (sliderPosition === 0) {
            $(".left-arrow").css("opacity", ".3");
        } else {
            $(".left-arrow").css("opacity", "1");
        }
        

        if (Modernizr.cssanimations) {
            $("#home-recipe-container").css("-webkit-transform", "translate3d(-" + sliderPosition + "%,0,0)");
        } else {
            $("#home-recipe-container").css("left", "-" + sliderPosition + "00%");
        }
    }

    function rightArrowClickHandler() {
        if(sliderPosition < 2) {
            sliderPosition++;
            $(".left-arrow").css("opacity", "1");
        }

        if (sliderPosition === 2) {
            $(".right-arrow").css("opacity", ".3");
        } else {
            $(".right-arrow").css("opacity", "1");
        }

        if (Modernizr.cssanimations) {
            $("#home-recipe-container").css("-webkit-transform", "translate3d(-" + sliderPosition + "%,0,0)");
        } else {
            $("#home-recipe-container").css("left", "-" + sliderPosition + "00%");
        }
    }

    function trackEvent(category, action, label) {
        _gaq.push(['_trackEvent', category, action, label]);
    }

    function trackingClickHandler(event) {
        var thisTarget = event.currentTarget,
        category = thisTarget.getAttribute('data-ga-category'),
        action = thisTarget.getAttribute('data-ga-action') || 'Click',
        label = thisTarget.getAttribute('data-ga-label') || thisTarget.innerText.trim();

        trackEvent(category, action, label);
    }

    function initializeTracking() {
        var click = document.querySelectorAll('[data-capture="click"]');

        var i;

        for (i = 0; i < click.length; ++i) {
            $(click[i]).on('click', trackingClickHandler);
        }
    }

    function initialize() {
        $window = $(window);
        isTouch = Modernizr.touch;
        speed = 10;
            
        $sections = $('.default-index #interactive-home section');
        sectionsLength = $sections.length;

        if (isTouch === false) {
            $sections.css("background-attachment", "fixed");
        }

        if (!Modernizr.cssanimations) {
            animateArrow();
        }

        /* Track users the reach the bottom of the page */
        $window.scroll(function () {
            if ($('body').height() <= ($window.height() + $window.scrollTop())) {
                ga.triggerTrackEvent("Homepage", "Scroll", "Bottom");
            }
        });

        $(".left-arrow").on("click", leftArrowClickHandler);
        $(".right-arrow").on("click", rightArrowClickHandler);

        $(".recipe img").swipe({ swipeLeft: rightArrowClickHandler, swipeRight: leftArrowClickHandler, allowPageScroll: "vertical" });

        initializeTracking();
    }

    initialize();

    return {
        renderBackgroundSize: renderBackgroundSize,
        updateBackgroundTopPosition: updateBackgroundTopPosition
    };

};

$(function () {
    background = new Background();
});

