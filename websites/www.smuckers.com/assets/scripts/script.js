var application,
    isTouch,
    $window,
    $document,
    supportsAnimations,
    supportsTransitions,
    settings = {
        isMobile: false,
        isTablet: false,
        isDesktop: false,
        maxSlides: 4
    };

var Application = function () {
    var $bodyContainer,
        $pageContainer,
        $navContainer,
        $headerContainer,
        $subNavContainer,
        $thisSubNavContainer,

        $navRecipeCat,
        $toggleElement,
        $addRecipeButton,
        $whatsNewCallouts,
        $whatsNewCalloutsContent,
        $calloutNum,
        $openWhatsNewCallout,
        $plusUp,
        $backToTop,
  
        disableNavHover,
        navigation,
        page,
        about,
        transition;

  var Page = function () {

      function checkResolution() {
          var oldSettings = $.extend({}, settings);

          settings.isMobile = $window.width() < 640;
          settings.isTablet = $window.width() < 1000 && $window.width() >= 640;
          settings.isDesktop = $window.width() >= 1000;

          if (settings.isMobile != oldSettings.isMobile || settings.isTablet != oldSettings.isTablet || settings.isDesktop != oldSettings.isDesktop) {
              $window.trigger('settingsChange');

              if (typeof navigation != 'undefined' && settings.isDesktop) {
                  disableNavHover = false;
                  navigation.toggleMenuHover();
              }
          }
      }

      function windowResizeHandler(event) {
          var wasDesktop = settings.isDesktop;

          checkResolution();

          if (typeof background != 'undefined') {
              background.renderBackgroundSize();
              background.updateBackgroundTopPosition();
          }
          
      }

      function windowScrollHandler(event) {

          if (typeof background != 'undefined') {
              background.updateBackgroundTopPosition();
          }
      }

      function initialize() {
          $window.on('resize', windowResizeHandler);
          $window.on('scroll', windowScrollHandler);

          windowResizeHandler();
          windowScrollHandler();
      }

      initialize();
  };

  var Navigation = function () {
      var $buttonNav,
          $overlayNav,
          $navList,
          $navLink,
          $subNavList,
          $navRecipeCat,
          $contentContainer,
          $contentLeft,
          $buttonSmuckersSites,
          $buttonViolator,
          $violator,
          $violatorContainer,
          $violatorContainerHeight,
          $closeViolator,
          $smuckersSitesContainer,
          $smuckersSites,
          $openMenu,
          $headerTotalHeight,
          $smuckersSitesHeight,
          $submenu,

          isInLeave,
          timer;

    function bodyContainerToggleOpen() {
      //$window.scrollTop(0);
      
      $bodyContainer.toggleClass('open');
      $buttonNav.children('span').toggleClass('icon-reorder');
      $buttonNav.children('span').toggleClass('icon-remove');
    }
    
    function toggleDropdownSubMenu($this) {
        $subNavContainer = $this.children('.subnav-container');

        $subNavContainer.toggleClass('block');

        $this.children('a').children('span').toggleClass('icon-plus');
        $this.children('a').children('span').toggleClass('icon-minus');
        $this.toggleClass('open');

        isInLeave = false;

        return false;
    }

    function toggleMobileSubMenu($this) {
        $thisParent = $this.parent();
        $thisSubNavContainer = $this.next('.subnav-container');
        $subNavContainer = $('.subnav-container');

        if ($thisParent.hasClass('navrecipes')) {
            $subNavList = $this.next(".subnav-container").children(".mega-container");
        }

        else {
            $subNavList = $this.next(".subnav-container").children("ul");
        }

        if ($thisParent.hasClass('open')) {
            $thisParent.removeClass('open');
            $thisSubNavContainer.css('height', '');
            $navLink.children('span').addClass('icon-plus').removeClass('icon-minus');
            $thisSubNavContainer.removeClass('block');
        }

        else {
            $navList.removeClass('open');
            $subNavContainer.css('height', '');
            $navLink.children('span').addClass('icon-plus').removeClass('icon-minus');

            $thisParent.addClass('open');
            $this.children('span').removeClass('icon-plus').addClass('icon-minus');
            $thisSubNavContainer[0].style.height = $subNavList.innerHeight() + 'px';
            $thisSubNavContainer.addClass('block');
        }
    }
    
    function buttonNavClickHandler(event) {
        disableNavHover = true;
        toggleMenuHover();
        
        bodyContainerToggleOpen();
      
        $overlayNav.show();
    }

    function toggleMenuHover() {
        if(disableNavHover === true){
            $submenu.unbind('mouseenter').unbind('mouseleave');
        }else{
            $submenu.hoverIntent(navHoverHandler);
        }
    }
    
    function overlayNavClickHandler(event) {
      bodyContainerToggleOpen();
      
      if (!supportsAnimations) {
        overlayNavAnimationEndHandler();
      }
    }
    
    function overlayNavAnimationEndHandler(event) {
      if (!$bodyContainer.hasClass('open')) {
        $overlayNav.hide();
      }
    }

    function buttonSmuckersSitesClickHandler(event) {

        //$violator.css(transition, "top 500ms cubic-bezier(0, 0, 0.2, 1)");
        //$smuckersSites.css(transition, "height 500ms cubic-bezier(0, 0, 0.2, 1)");

        $smuckersSitesContainer.toggleClass('open');
        $(this).children('span').toggleClass('icon-plus');
        $(this).children('span').toggleClass('icon-minus');

        if ($smuckersSitesContainer.hasClass('open')) {
            $buttonSmuckersSites.addClass('open');
            $bodyContainer.addClass('sites-open');
           // $smuckersSites[0].style.height = $smuckersSites.children("ul").innerHeight() + 'px';
            if (settings.isDesktop === true) {
                //$smuckersSitesHeight = $smuckersSites.children("ul").innerHeight();
                //$navContainer.css("top", $smuckersSitesHeight - 4 + "px");
                //$headerContainer.addClass('sitesopen');
                //$headerContainer.css("margin-top", $smuckersSitesHeight + "px");

            }
            if (settings.isMobile || settings.isTablet) {
                var sitesOffset = $buttonSmuckersSites.offset();

                $('body').animate({ scrollTop: sitesOffset.top }, 'slow');
            }
        }
        else {
            $bodyContainer.removeClass('sites-open');
            $buttonSmuckersSites.removeClass('open');
            //$smuckersSites[0].style.height = '';
            if (settings.isDesktop === true) {
                //$navContainer.css("top", "");
                //$headerContainer.css("margin-top", '');
                //$smuckersSitesHeight = 4;
            }
        }

        return false;
    }
    
    function smuckersSitesContainerTransitionEndHandler(event) {
        $violator.css(transition, "");
        $smuckersSites.css(transition, "");

        removeContentTransition();

      if (!$smuckersSitesContainer.hasClass('open') && !$buttonSmuckersSites.hasClass('open')) {
          $buttonSmuckersSites.removeClass('open');
          if (settings.isDesktop === true) {
              $headerContainer.removeClass('sitesopen');
          }
      }
      if ($navList.hasClass('open')) {
          $navList.removeClass('open');
      }
    }
    
    function navLinkClickHandler(event) {
        toggleMobileSubMenu($(this));
        return false;
    }

    function navHoverHandler(event) {
        $this = $(this);

        if (event.type == "mouseleave") {
            if($this.hasClass('submenu')){
                isInLeave = true;
                $openMenu = $this;
                timer = setTimeout(function () { toggleDropdownSubMenu($this); }, 750);
            }
            else{
                toggleDropdownSubMenu($this);
            }
                
        }
        else if (event.type == "mouseenter" && isInLeave === true) {
            if ($this.get(0) !== $openMenu.get(0)) {
                window.clearTimeout(timer);
                toggleDropdownSubMenu($openMenu);
                toggleDropdownSubMenu($this);
            } else {
                window.clearTimeout(timer);
            }
                
            isInLeave = false;
        }
        else {
            toggleDropdownSubMenu($this);
        }

        return false;
    }

    function buttonViolatorClickHandler(event) {
        $violatorContainerHeight = $violatorContainer.children('.message').outerHeight(true);

        //addContentTransition(event);

        if ($pageContainer.hasClass('violator-open')) {
            $pageContainer.removeClass('violator-open');
            $violatorContainerHeight = 4;
            $pageContainer.addClass('violator-closed');

            if(event.type==="click"){
                $.cookie('violator', 'closed', { path: '/' });
            }

            $buttonViolator.children('span').removeClass('icon-remove');
            $buttonViolator.children('span').addClass('icon-sort-down');
           
        }
        else {
            $pageContainer.addClass('violator-open');
            $pageContainer.removeClass('violator-closed');
            $buttonViolator.children('span').removeClass('icon-sort-down');
            $buttonViolator.children('span').addClass('icon-remove');
        }

        if (!$smuckersSitesContainer.hasClass('open') && !$buttonSmuckersSites.hasClass('open')) {
            $buttonSmuckersSites.removeClass('open');

            if (settings.isDesktop === true) {
                $headerContainer.removeClass('sitesopen');
            }
        }

        if ($navList.hasClass('open')) {
            $navList.removeClass('open');
        }

        if (settings.isMobile != true || settings.isTablet != true) {
            $headerTotalHeight = $violatorContainerHeight + $headerContainer.innerHeight();
        }
    }

    function violatorContainerTransitionEndHandler(event) {
        if (supportsTransitions) {
            $violatorContainer.css(transition, "");
        }

        removeContentTransition();        
    }

    function addContentTransition(event) {
        if (supportsTransitions) {
            //$contentContainer.css(transition, "margin 500ms cubic-bezier(0, 0, 0.2, 1)");
            //if (event === "load") {
                //$contentContainer.css("-webkit-transition-delay", "1s");
                //$contentLeft.css("-webkit-transition-delay", "1s");
            //}
        }
    }

    function removeContentTransition() {
        if (supportsTransitions) {
            $contentContainer.css(transition, "");
            $contentLeft.css(transition, "");
        }
    }

    function recipesHover() {

        if($('#cat-container').css("display") == "block"){
            $('#cat-container img').attr('src', 'http://www.smuckersrms.com/RSI/RecipeImage.ashx?recipeid=' + $(this).data('recipe-id') + '&w=100&h=100');
        }
    }

    function getViolatorHeight() {
        return $violator.innerHeight() + $headerContainer.innerHeight();
    }

    function initialize() {
      $buttonNav = $('#button-nav');
      $overlayNav = $('#overlay-nav');
      $navList = $('nav > ul > li');
      $navLink = $('nav > ul > li > a');
      $contentContainer = $('#content-container');
      $contentLeft = $('#content-left');
      $buttonSmuckersSites = $('#button-smuckers-sites');
      $smuckersSitesContainer = $('#smuckers-sites-container');
      $smuckersSites = $('#smuckers-sites');
      $submenu = $(".submenu");

      $buttonViolator = $('#button-violator-toggle');
      $closeViolator = $('#violator-close');
      $violator = $('#violator');
      $violatorContainer = $("#violator-container");

      $navRecipeCat = $('#cat-container div');

      $buttonNav.on('click', buttonNavClickHandler);

      $overlayNav.on('click', overlayNavClickHandler);
      $overlayNav.on(animationEnd, overlayNavAnimationEndHandler);

      if (!isTouch) {
          $submenu.hoverIntent(navHoverHandler);
      }

      if (isTouch && settings.isDesktop === false) {
          $navLink.on('click', navLinkClickHandler);
      }

      $openMenu = null;
      isInLeave = false;
      timer = null;

      $buttonSmuckersSites.on('click', buttonSmuckersSitesClickHandler);
      $smuckersSitesContainer.on(transEnd, smuckersSitesContainerTransitionEndHandler);

      $buttonViolator.on('click', buttonViolatorClickHandler);
      $closeViolator.on('click', buttonViolatorClickHandler);
      $violatorContainer.on(transEnd, violatorContainerTransitionEndHandler);

      $('#nav-recipe-dish a').on('mouseenter', recipesHover);
      $('#nav-recipe-dish a').on('mouseleave', recipesHover);

      if (!$.cookie('violator')) {
          //buttonViolatorClickHandler();
      }

    }

    initialize();

    return {
        toggleMenuHover: toggleMenuHover
    };
    
  };

  var About = function () {
      var $videoLinks,
          $videoOverlay,
          $videoPlayer,
          $ytplayer,
          $videoTitle,
          $videoSocial,
          $closeButton;

      function videoClickHandler(event) {
          if (!settings.isMobile) {
              if ($videoOverlay.hasClass('open')) { //close
                  $videoOverlay.removeClass('open');
                  $ytplayer.attr('src', '');
                  $videoTitle.html('');
                  $videoSocial.empty();
                  $document.unbind('click');
              }

              else { //open
                  $thisAnchor = $(this);

                  var ytId = $thisAnchor.data('ytId'),
                      videoTitle = $thisAnchor.data('videoTitle');

                  if (!videoTitle) {
                      videoTitle = $('span', $thisAnchor).html();
                  }

                  var pinItVideoTitle = videoTitle.replace(/\&amp;/g, 'and');

                  $videoTitle.html(videoTitle);

                  // add fb like button and parse the dom element
                  $videoSocial.append($('<div class="fb-like" data-send="false" data-layout="button_count" data-width="150" data-show-faces="false">')
                      .attr('data-href', $thisAnchor.attr('href')));
                  FB.XFBML.parse($videoSocial.get(0));

                  // add the pinit button and then remove and reload the pinterest js to parse the button
                  $('script[src*="Unknown_83_filename"/*tpa=http://www.smuckers.com/assets/scripts/assets.pinterest.com/js/pinit.js*/]').remove();
                  $videoSocial.append($('<a data-pin-do="buttonPin" data-pin-config="beside"><img class="pinit-button" src="../../../assets.pinterest.com/images/pidgets/pin_it_button.png"/*tpa=http://assets.pinterest.com/images/pidgets/pin_it_button.png*/ /></a>')
                      .attr('href', '//pinterest.com/pin/create/button/?url=http://youtu.be/' + ytId + '&media=http://img.youtube.com/vi/' + ytId + '/hqdefault.jpg&description=' + stripHTML(pinItVideoTitle)));
                  $.ajax({
                      url: '../../../assets.pinterest.com/js/pinit.js'/*tpa=http://assets.pinterest.com/js/pinit.js*/,
                      dataType: 'script',
                      cache: true
                  });

                  $videoOverlay.addClass('open');
                  $ytplayer.attr('src', 'http://www.youtube.com/embed/' + ytId + '?rel=0&autoplay=1&html5=1&origin=' + window.location);
                  //$videoPlayer.on('click', event.stopPropagation);
                  //$document.on('click', videoClickHandler);
              }

              event.stopPropagation();
              return false;
          }

          return true;
      }

      function initialize() {
          $videoLinks = $('a.video-embed');
          $videoOverlay = $('#video-overlay');
          $videoPlayer = $('#video-player');
          $ytplayer = $('#ytplayer');
          $videoTitle = $('#video-title');
          $videoSocial = $('#video-social');
          $closeButton = $('.close-button');

          $closeButton.on('click', videoClickHandler);
          $videoLinks.on('click', videoClickHandler);
      }

      initialize();

      return {
          videoClickHandler: videoClickHandler
      };
  };

  function toggleIconPlusMinus($toggleElement, $toggleType) {
      $toggleElement.toggleClass('icon-plus');
      $toggleElement.toggleClass('icon-minus');
  }

  function whatsNewClickHandler(event) {
      var $this = $(this);
      $whatsNewCalloutsContent = $this.children('.whats-new-content li');
      $calloutNum = $whatsNewCallouts.index(this);
      $openWhatsNewCallout = $('#whats-new-callouts li.open').index();
      var i;

      if ($whatsNewCallouts.hasClass('open')) {
          $whatsNewCallouts.removeClass('open');
          $whatsNewCallouts.children('span').removeClass('icon-remove');
          $whatsNewCallouts.children('span').addClass('icon-caret');

          $('#whats-new-module').css('margin-bottom', '');
      }

      if ($openWhatsNewCallout != $calloutNum) {
          $whatsNewCallouts.eq($calloutNum).addClass('open');
          $this.addClass('open');
          $this.children('span').addClass('icon-remove');
          $this.children('span').removeClass('icon-caret');
          $this.children('span').removeClass('oswald');
          $('#whats-new-module').css('margin-bottom', $this.children('ul').outerHeight(true) + "px");

          whatsNewTrack(event, $this);

      }

  }

  function whatsNewTrack(event, $thisPrev) {

      if($thisPrev != null){
          var $this = $thisPrev;
      }
      else {
        var $this = $(this);
      }

      var whatsNewTitle = $this.data('trackinglabel');

      if (event.type == "mouseenter" || event.type == "click") {
          var action = event.type;

          if (action == "mouseenter") {
              action = "rollover";
          }

          ga.triggerTrackEvent("Whats New", action, whatsNewTitle);
      }

      //Check to see if the current page url equals the cta url. If it does, then
      //hide it.
      var $whatsNewCta = $this.find('a');
      var currentUrl = window.location.pathname;

      if ($whatsNewCta.length > 0) {
          if (currentUrl.toLowerCase() === $whatsNewCta.attr('href').toLowerCase()) {
              $whatsNewCta.addClass('hide');
          }
      }

  }

  function plusUpTrack(event, $thisPrev) {

      if ($thisPrev != null) {
          var $this = $thisPrev;
      }
      else {
          var $this = $(this);
      }

      var pathname = window.location.pathname;

      if(pathname === "/"){
          pathname = "Home";
      }

      var label = pathname + " | " + $this.find('.info > span.green-box').data('trackinglabel');

      if (event.type == "mouseenter" || event.type == "click") {
          var action = event.type;

          if (action == "mouseenter") {
              action = "rollover";
          }

          ga.triggerTrackEvent("Plus Up", action, label);
      }
  }

  function plusUpClickHandler(event) {
      var $this = $(this);

      if ($this.hasClass('open')) {
          $this.removeClass('open');
      }
      else{
          $this.addClass('open');
          plusUpTrack(event, $this);
      }
  }

  function backToTopClickHandler() {
      $('body, html').stop().animate({ scrollTop: 0 }, 1000, "easeInOutQuart");
      return false;
  }

  function getUrlVars() {
      var queryStrings = [], hash;
      var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
      for (var i = 0; i < hashes.length; i++) {
          hash = hashes[i].split('=');
          queryStrings.push(hash[0]);
          queryStrings[hash[0]] = hash[1];
      }
      return queryStrings;
  }

  function stripHTML(html) {
      return html.replace(/<\/?([a-z][a-z0-9]*)\b[^>]*>?/gi, '');
  }
  
  function initialize() {

    $window = $(window),
    $document = $(document);
    $bodyContainer = $('#body-container');
    $pageContainer = $('#page-container');
    $navContainer = $('#nav-container');
    $headerContainer = $('#header-container');
    $whatsNewCallouts = $('#whats-new-callouts > li');
    $whatsNewArrowRight = $('.whatsnew-arrow');
    $plusUp = $('.plus-up');
    $backToTop = $('#footer-backtotop');

    isTouch = Modernizr.touch;
    supportsAnimations = Modernizr.cssanimations;
    supportsTransitions = Modernizr.csstransitions;
    disableNavHover = false;

    transition = Modernizr.prefixed('transition');

    if(isTouch){
        $whatsNewCallouts.on('click', whatsNewClickHandler);

        $plusUp.on('click', plusUpClickHandler);
    }
    else {
        $whatsNewCallouts.on('mouseenter', whatsNewTrack);
        $plusUp.on('mouseenter', plusUpTrack);
    }

    $backToTop.on('click', backToTopClickHandler);
    
    page = new Page();
    navigation = new Navigation();
    about = new About();
    
  }
  
  initialize();

  return {
      toggleIconPlusMinus: toggleIconPlusMinus,
      getUrlVars: getUrlVars,
      stripHTML: stripHTML
  };

};

$(function () {
    application = new Application();

    $('input[data-val-length-max]').each(function () {
        var $this = $(this);
        var data = $this.data();
        $this.attr('maxlength', data.valLengthMax);
    });
});
