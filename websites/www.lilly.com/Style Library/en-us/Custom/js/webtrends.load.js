// WebTrends SmartSource Data Collector Tag v10.4.1
// Copyright (c) 2014 Webtrends Inc.  All rights reserved.
// Tag Builder Version: 4.1.3.2
// Created: 2014.03.27
window.webtrendsAsyncInit=function(){
    var dcs=new Webtrends.dcs().init({
        dcsid:"dcskykqjqvz5bd05pjea7z15r_7k3s",
        domain:"http://www.lilly.com/Style%20Library/en-us/Custom/js/statse.webtrendslive.com",
        timezone:-5,
        i18n:false,
        offsite:true,
        download:true,
        downloadtypes:"xls,doc,ppt,xlsx,docx,pptx,pdf,txt,csv,zip,gzip,tar,swf,wmv,exe,rar,mp4,mp3,flv",
        anchor:true,
        javascript: true,
        onsitedoms:"www.Lilly.com,Lilly,com",
        fpcdom:".Lilly.com",
        plugins:{
            hm:{src:"../../../../../s.webtrends.com/js/webtrends.hm.js"/*tpa=http://s.webtrends.com/js/webtrends.hm.js*/}
        }
        }).track();
};
(function(){
    var s=document.createElement("script"); s.async=true; s.src="webtrends.min.js"/*tpa=http://www.lilly.com/Style%20Library/en-us/Custom/js/webtrends.min.js*/;    
    var s2=document.getElementsByTagName("script")[0]; s2.parentNode.insertBefore(s,s2);
}());
