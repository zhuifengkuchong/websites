<script id = "race66a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race66={};
	myVars.races.race66.varName="Object[13675].uuid";
	myVars.races.race66.varType="@varType@";
	myVars.races.race66.repairType = "@RepairType";
	myVars.races.race66.event1={};
	myVars.races.race66.event2={};
	myVars.races.race66.event1.id = "Lu_Id_input_3";
	myVars.races.race66.event1.type = "onkeypress";
	myVars.races.race66.event1.loc = "Lu_Id_input_3_LOC";
	myVars.races.race66.event1.isRead = "False";
	myVars.races.race66.event1.eventType = "@event1EventType@";
	myVars.races.race66.event2.id = "Lu_Id_input_2";
	myVars.races.race66.event2.type = "onkeypress";
	myVars.races.race66.event2.loc = "Lu_Id_input_2_LOC";
	myVars.races.race66.event2.isRead = "True";
	myVars.races.race66.event2.eventType = "@event2EventType@";
	myVars.races.race66.event1.executed= false;// true to disable, false to enable
	myVars.races.race66.event2.executed= false;// true to disable, false to enable
</script>

