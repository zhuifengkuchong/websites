<script id = "race54b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race54={};
	myVars.races.race54.varName="Lu_Id_input_2__onblur";
	myVars.races.race54.varType="@varType@";
	myVars.races.race54.repairType = "@RepairType";
	myVars.races.race54.event1={};
	myVars.races.race54.event2={};
	myVars.races.race54.event1.id = "Lu_Id_input_2";
	myVars.races.race54.event1.type = "onblur";
	myVars.races.race54.event1.loc = "Lu_Id_input_2_LOC";
	myVars.races.race54.event1.isRead = "True";
	myVars.races.race54.event1.eventType = "@event1EventType@";
	myVars.races.race54.event2.id = "Lu_DOM";
	myVars.races.race54.event2.type = "onDOMContentLoaded";
	myVars.races.race54.event2.loc = "Lu_DOM_LOC";
	myVars.races.race54.event2.isRead = "False";
	myVars.races.race54.event2.eventType = "@event2EventType@";
	myVars.races.race54.event1.executed= false;// true to disable, false to enable
	myVars.races.race54.event2.executed= false;// true to disable, false to enable
</script>

