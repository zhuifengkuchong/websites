<script id = "race68a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race68={};
	myVars.races.race68.varName="Object[13675].uuid";
	myVars.races.race68.varType="@varType@";
	myVars.races.race68.repairType = "@RepairType";
	myVars.races.race68.event1={};
	myVars.races.race68.event2={};
	myVars.races.race68.event1.id = "Lu_Id_input_1";
	myVars.races.race68.event1.type = "onkeypress";
	myVars.races.race68.event1.loc = "Lu_Id_input_1_LOC";
	myVars.races.race68.event1.isRead = "False";
	myVars.races.race68.event1.eventType = "@event1EventType@";
	myVars.races.race68.event2.id = "ss";
	myVars.races.race68.event2.type = "onkeypress";
	myVars.races.race68.event2.loc = "ss_LOC";
	myVars.races.race68.event2.isRead = "True";
	myVars.races.race68.event2.eventType = "@event2EventType@";
	myVars.races.race68.event1.executed= false;// true to disable, false to enable
	myVars.races.race68.event2.executed= false;// true to disable, false to enable
</script>

