<script id = "race105a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race105={};
	myVars.races.race105.varName="Object[13675].uuid";
	myVars.races.race105.varType="@varType@";
	myVars.races.race105.repairType = "@RepairType";
	myVars.races.race105.event1={};
	myVars.races.race105.event2={};
	myVars.races.race105.event1.id = "Lu_Id_input_6";
	myVars.races.race105.event1.type = "onblur";
	myVars.races.race105.event1.loc = "Lu_Id_input_6_LOC";
	myVars.races.race105.event1.isRead = "False";
	myVars.races.race105.event1.eventType = "@event1EventType@";
	myVars.races.race105.event2.id = "Lu_Id_button_3";
	myVars.races.race105.event2.type = "onblur";
	myVars.races.race105.event2.loc = "Lu_Id_button_3_LOC";
	myVars.races.race105.event2.isRead = "True";
	myVars.races.race105.event2.eventType = "@event2EventType@";
	myVars.races.race105.event1.executed= false;// true to disable, false to enable
	myVars.races.race105.event2.executed= false;// true to disable, false to enable
</script>

