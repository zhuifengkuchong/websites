<script id = "race32b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race32={};
	myVars.races.race32.varName="SiteSearch_SiteSearch2Button__onfocus";
	myVars.races.race32.varType="@varType@";
	myVars.races.race32.repairType = "@RepairType";
	myVars.races.race32.event1={};
	myVars.races.race32.event2={};
	myVars.races.race32.event1.id = "SiteSearch_SiteSearch2Button";
	myVars.races.race32.event1.type = "onfocus";
	myVars.races.race32.event1.loc = "SiteSearch_SiteSearch2Button_LOC";
	myVars.races.race32.event1.isRead = "True";
	myVars.races.race32.event1.eventType = "@event1EventType@";
	myVars.races.race32.event2.id = "Lu_DOM";
	myVars.races.race32.event2.type = "onDOMContentLoaded";
	myVars.races.race32.event2.loc = "Lu_DOM_LOC";
	myVars.races.race32.event2.isRead = "False";
	myVars.races.race32.event2.eventType = "@event2EventType@";
	myVars.races.race32.event1.executed= false;// true to disable, false to enable
	myVars.races.race32.event2.executed= false;// true to disable, false to enable
</script>

