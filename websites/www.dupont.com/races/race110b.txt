<script id = "race110b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race110={};
	myVars.races.race110.varName="Object[13675].uuid";
	myVars.races.race110.varType="@varType@";
	myVars.races.race110.repairType = "@RepairType";
	myVars.races.race110.event1={};
	myVars.races.race110.event2={};
	myVars.races.race110.event1.id = "Lu_Id_input_6";
	myVars.races.race110.event1.type = "onblur";
	myVars.races.race110.event1.loc = "Lu_Id_input_6_LOC";
	myVars.races.race110.event1.isRead = "True";
	myVars.races.race110.event1.eventType = "@event1EventType@";
	myVars.races.race110.event2.id = "SiteSearch_SiteSearch2Button";
	myVars.races.race110.event2.type = "onblur";
	myVars.races.race110.event2.loc = "SiteSearch_SiteSearch2Button_LOC";
	myVars.races.race110.event2.isRead = "False";
	myVars.races.race110.event2.eventType = "@event2EventType@";
	myVars.races.race110.event1.executed= false;// true to disable, false to enable
	myVars.races.race110.event2.executed= false;// true to disable, false to enable
</script>

