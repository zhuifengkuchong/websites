<script id = "race64b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race64={};
	myVars.races.race64.varName="Object[13675].uuid";
	myVars.races.race64.varType="@varType@";
	myVars.races.race64.repairType = "@RepairType";
	myVars.races.race64.event1={};
	myVars.races.race64.event2={};
	myVars.races.race64.event1.id = "SiteSearch_SiteSearch2Button";
	myVars.races.race64.event1.type = "onkeypress";
	myVars.races.race64.event1.loc = "SiteSearch_SiteSearch2Button_LOC";
	myVars.races.race64.event1.isRead = "True";
	myVars.races.race64.event1.eventType = "@event1EventType@";
	myVars.races.race64.event2.id = "ss";
	myVars.races.race64.event2.type = "onkeypress";
	myVars.races.race64.event2.loc = "ss_LOC";
	myVars.races.race64.event2.isRead = "False";
	myVars.races.race64.event2.eventType = "@event2EventType@";
	myVars.races.race64.event1.executed= false;// true to disable, false to enable
	myVars.races.race64.event2.executed= false;// true to disable, false to enable
</script>

