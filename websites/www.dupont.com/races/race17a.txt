<script id = "race17a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race17={};
	myVars.races.race17.varName="SiteSearch_SiteSearch2Button__onchange";
	myVars.races.race17.varType="@varType@";
	myVars.races.race17.repairType = "@RepairType";
	myVars.races.race17.event1={};
	myVars.races.race17.event2={};
	myVars.races.race17.event1.id = "Lu_DOM";
	myVars.races.race17.event1.type = "onDOMContentLoaded";
	myVars.races.race17.event1.loc = "Lu_DOM_LOC";
	myVars.races.race17.event1.isRead = "False";
	myVars.races.race17.event1.eventType = "@event1EventType@";
	myVars.races.race17.event2.id = "SiteSearch_SiteSearch2Button";
	myVars.races.race17.event2.type = "onchange";
	myVars.races.race17.event2.loc = "SiteSearch_SiteSearch2Button_LOC";
	myVars.races.race17.event2.isRead = "True";
	myVars.races.race17.event2.eventType = "@event2EventType@";
	myVars.races.race17.event1.executed= false;// true to disable, false to enable
	myVars.races.race17.event2.executed= false;// true to disable, false to enable
</script>

