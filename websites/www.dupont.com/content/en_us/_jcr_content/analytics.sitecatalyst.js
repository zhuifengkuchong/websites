
	        CQ_Analytics.registerAfterCallback(function(options) {
	            if(!options.compatibility && $CQ.inArray( options.componentPath, CQ_Analytics.Sitecatalyst.frameworkComponents) < 0 )
	                return false;    // component not in framework, skip SC callback
	            CQ_Analytics.Sitecatalyst.saveEvars();
	            CQ_Analytics.Sitecatalyst.updateEvars(options);
	            CQ_Analytics.Sitecatalyst.updateLinkTrackVars();
	            return false;
	        }, 10);
	
	        CQ_Analytics.registerAfterCallback(function(options) {
	            if(!options.compatibility && $CQ.inArray( options.componentPath, CQ_Analytics.Sitecatalyst.frameworkComponents) < 0 )
	                return false;    // component not in framework, skip SC callback
	            s = s_gi("dupontphoenixglobal");
	            if (s.linkTrackVars == "None") {
	                s.linkTrackVars = "events";
	            } else {
	                s.linkTrackVars = s.linkTrackVars + ",events";
	            }
	            CQ_Analytics.Sitecatalyst.trackLink(options);
	            return false;
	        }, 100);
	
	
	        CQ_Analytics.registerAfterCallback(function(options) {
	            if(!options.compatibility && $CQ.inArray( options.componentPath, CQ_Analytics.Sitecatalyst.frameworkComponents) < 0 )
	                return false;    // component not in framework, skip SC callback
	            CQ_Analytics.Sitecatalyst.restoreEvars();
	            return false;
	        }, 200);
	
	        CQ_Analytics.adhocLinkTracking = "false";
	        
	
/***************** ORIGINAL *********************/	
//	        var s_account = "dupontphoenixglobal";
//	        var s = s_gi(s_account);
/***************** END ORIGINAL *********************/

/***************** UPDATE START  *********************/
                var s = new AppMeasurement();
                s.account = "dupontphoenixglobal";
	        var s_account = s.account;
/***************** END UPDATE *********************/
	        s.fpCookieDomainPeriods = "2";
	        s.currencyCode= 'USD';
        s.trackInlineStats= true;
        s.linkTrackVars= 'None';
        s.charSet= 'UTF-8';
        s.linkLeaveQueryString= false;
        s.linkExternalFilters= '';
        s.linkTrackEvents= 'None';
        s.trackExternalLinks= true;
        s.linkDownloadFileTypes= 'exe,zip,wav,mp3,mov,mpg,avi,wmv,doc,pdf,xls';
        s.linkInternalFilters= 'javascript:,'+window.location.hostname;
        s.trackDownloadLinks= true;
        
        s.visitorNamespace = "dupont";
        s.trackingServer = "http://www.dupont.com/content/en_us/_jcr_content/dupont.d2.sc.omtrdc.net";
        s.trackingServerSecure = "http://www.dupont.com/content/en_us/_jcr_content/dupont.d2.sc.omtrdc.net";
        
        

/* Plugin Config */

s.usePlugins=true;
function s_doPlugins(s) {
    //add your custom plugin code here
	s.eVar34 = s.getPreviousValue(s.eVar67, "s_gpv_v34");
	s.prop34 = s.getPreviousValue(s.prop67, "s_gpv_c34");
}
s.doPlugins=s_doPlugins;




