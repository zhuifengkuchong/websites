var getPrev;
var dots = ['#productIdeationDot', "#productDesignDot", "#supplyChainDot", "#industrializationDot", "#manufacturingDot", "#sustainabilityDot"];

function connectDots( getVal ) {
	for( d = 0; d < dots.length; d++ ){
		$(dots[d - 1]).css('background', '#787878');
	}
	
	for( d = 0; d < dots.length; d++ ){
		if( dots[d] != getVal ){
			$(dots[d]).css('background', '#006599');
		} else {
			break;
		}
	}
}

function hideMain() {
	var getSliderWidth = -1 * $('#main').width();
	$('#main').animate({ right: getSliderWidth }, 1500, 'easeOutQuint');
}

function showMain() {
	getPrev = null;
	$('#main').animate({ right: 0 }, 1500, 'easeOutQuint');
}

function showSecondarySlider( getID ) {
	if( getPrev == null ){
		hideMain();
	} else {
		hideSecondarySlider( getPrev );
	}	
	
	var getDiv = "#" + getID;
	var getImg = "#" + getID + "Img";
	var getText = "#" + getID + "Text";
	var getOptions = "#" + getID + "Options";
	var getDot = "#" + getID + "Dot";
	
	for( d = 0; d < dots.length; d++ ){
		if( getDot == dots[d] ){
			connectDots( getDot );
		}
	}
	
	if( getPrev == null ){
		$('#dotsContainer').delay( 500 ).animate({ bottom: 25 }, 1500, 'easeOutQuint');
	}
		
	$(getDiv).animate({ marginLeft: 0 }, 500, 'easeOutQuad', function(){
		if( $('#main').width() > '768' ) {
			$(getImg).css('display', 'block');
			$(getImg).animate({ left: 0 }, 1500, 'easeOutQuint');
		}
		
		$(getText).animate({ right: 40 }, 1500, 'easeOutQuint');
		$(getOptions).delay( 100 ).animate({ top: 315 }, 1500, 'easeOutQuint');
	});
	
	getPrev = getID;
}

function hideSecondarySlider( getID ) {
	var getSliderWidth = $('#main').width();
	
	var getDiv = "#" + getID;
	var getImg = "#" + getID + "Img";
	var getText = "#" + getID + "Text";
	var getOptions = "#" + getID + "Options";
	
	if( $('#main').width() > '768' ) {
		$(getImg).animate({ left: -435 }, 500, 'linear', function(){ $(getImg).css('display', 'none'); } );
		$(getText).animate({ right: -500 }, 500, 'linear');
		//$('#dotsContainer').delay( 100 ).animate({ bottom: -25 }, 500, 'linear');
		
		$(getOptions).delay( 100 ).animate({ top: 500 }, 500, 'linear', function(){ 
			//showMain();
			$(getDiv).animate({ marginLeft: getSliderWidth }, 500, 'linear');
		});
	} else {
		$(getDiv).animate({ marginLeft: getSliderWidth }, 500, 'linear');
	}
}

function closeSecondarySlider( getID ) {
	var getSliderWidth = $('#main').width();
	
	var getDiv = "#" + getID;
	var getImg = "#" + getID + "Img";
	var getText = "#" + getID + "Text";
	var getOptions = "#" + getID + "Options";
	
		
	$(getImg).animate({ left: -435 }, 500, 'linear', function(){ $(getImg).css('display', 'none'); } );
	$(getText).animate({ right: -500 }, 500, 'linear');
	$('#dotsContainer').delay( 100 ).animate({ bottom: -25 }, 500, 'linear');
	
	$(getOptions).delay( 100 ).animate({ top: 500 }, 500, 'linear', function(){ 
		showMain();
		$(getDiv).animate({ marginLeft: getSliderWidth }, 500, 'linear');
	});
}

	
	
$(document).ready(function(){
	resetScreenElements();
	//startTimer();
	
	function resetScreenElements(){
		containerWidth = 1024; //$('.container').width();
		sliderCount = $("#sliderContent li").length;
		whichSlide = 0;
	}
	
	function startTimer(){
		timer = window.setInterval(function() {
			changeNxt();
		}, 15000);
	}

	function changeNxt(){
		if( (whichSlide + 1) < sliderCount ){
			whichSlide++;
		} else {
			whichSlide = 0;
		}

		$('#sliderContent').animate({left: -1 * (containerWidth * whichSlide)}, 1500, 'easeOutQuint');
		
		timer = window.clearInterval(timer);
		startTimer();
	}
	
	function changePrev(){
		if( whichSlide > 0 ){
			whichSlide--;
		} else {
			whichSlide = (sliderCount - 1);
		}
		
		$('#sliderContent').animate({left: -1 * (containerWidth * whichSlide)}, 1500, 'easeOutQuint');
		
		timer = window.clearInterval(timer);
		startTimer();
	}
	
	$("#SliderNav li").on("click", function(e){
		var getNewHeader = $(this).text();
		$('#sectionName').text( getNewHeader );
		$("#SliderNav").css('left', '-99999px');
		
       whichSlide = $(this).parent().children().index(this);
		$('#sliderContent').animate({left: -1 * (containerWidth * whichSlide)}, 1500, 'easeOutQuint');
		
		timer = window.clearInterval(timer);
		startTimer();
	});
	
	$("#changeSliderNext").on("click", function(e){
		changeNxt();
	});
	
	$("#changeSliderPrev").on("click", function(e){
		changePrev()
	});
});
