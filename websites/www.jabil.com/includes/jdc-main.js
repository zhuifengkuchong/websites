// Shows the search bar when clicking on the search icon in the header.
$('button#desktop-nav-search').on('click', function(e){
  $(this).parents('form').find("input.form-control").toggleClass("hidden");
});

// Custom dropdown menu toggle.
$('a.dropdown-toggle').each(function(idx, obj){
  $(obj).on('click', function(e){
    var target = $(obj).attr('dropdown-toggle'),
    dropdown = $("" + target);
    if ($(dropdown).css('display') == 'block') {
      $(dropdown).css('display', 'none');
    } else {
      // Hide all other drawers that are open before showing this one.
      $('.dropdown-menu').css('display', 'none');
      $(dropdown).css('display', 'block');
    }
    e.stopPropagation;
  });
});

function hideTray(tray) {
  var tile = tray.data('tile'),
  tileParent = tile.parent(),
  caret = tile.find(".caret-up");
  showKey = "previewShowing";

  $("#body-opacity").removeClass("preview-fade");
  // hides the 'Show Preview' link
  tile.removeClass("caption-preview");
  // Change the caret direction
  caret.removeClass('caret-up').addClass('caret-down');
  // Hide the preview pane
  tray.hide();
  // Set the tile's parent to transparent
  tileParent.removeClass("background-gray")
  // set previewShowing on this tile to false
  tile.data(showKey, false);
}

function hideTrays() {
  $('.preview-tray').each(function(idx, tray){
    hideTray($(tray));
  });
}

function togglePreviewTrayForTile(tile, tray) {
  var showKey = "previewShowing",
  showing = tile.data(showKey),
  tileParent = tile.parent();

  // If not the first click and not showing preview, hide it.
  if (typeof(showing) != 'undefined' && showing == true) {
    hideTray(tray);
  } else {
    hideTrays();

    $("#body-opacity").addClass("preview-fade");

    var tileTop = $(tileParent).offset().top,
    bpTop = $(tileParent).parent().offset().top,
    tpHeight = $(tileParent).height(),
    totalTop = (tileTop - bpTop) + tpHeight,
    caret = tile.find(".caret-down");

    caret.removeClass('caret-down').addClass('caret-up');
    // Extends the height to fill to the bottom of the tile.
    tile.addClass("caption-preview");
    // set its 'top' attr to be the offset of the item plus its height
    tray.css('top', totalTop);
    // set background on tile to gray
    tileParent.addClass("background-gray")
    // show the preview
    tray.show();

    // set previewShowing on this tile to true
    tile.data(showKey, true);
  }
}

// Popover for preview tray
$('.preview-tray').each(function(idx, obj){
  var tray = $(obj),
  tileID = tray.attr('data-preview-for'),
  tile = $(tileID);

  // set the tile to the preview tray's data attribute
  // for use in the hideTray() function.
  $(tray).data("tile", tile);

  // Toggle the tray on click of the tile.
  tile.on('click', function(e){
    togglePreviewTrayForTile(tile, tray);
    e.stopPropagation();
  });
});

// When a tray is out, hide it.
$('#body-opacity').on('click', function(e) {
  hideTrays();
});

/* Functionality for swapping videos out on the why-jabil page */
$('.video-container').each(function(_, ele){
  // If the main video container is not visible, then we are on a mobile
  // device and this event should be ignored. The main video container
  // has a class of .hidden-xs so that bootstrap takes care of hiding
  // it on mobile.

  // Listen for a click event on the video
  $(ele).on('click', function(e) {
    if ($('.main-video').css('display') == 'none') {
      console.log("main video is hidden - mobile");
      return false;
    }
    if ($(this).hasClass('video-active')) {
      console.log("Active video clicked - returning.");
      return false;
    }

    // Remove .video-active from the currently active video
    $('.video-active').removeClass('video-active');
    // Add .video-active to this video
    $(this).addClass('video-active');
    // Copy the title text into the main video container
    var title = $(this).find('.video-title').html();
    $('#main-video-container').find('h3 a').html(title);
    // Copy the description into the main video container
    var description = $(this).find('.video-description').html();
    $("#main-video-container").find('p').html(description);
    // Copy the video content into the main video container
    var content = $(this).find('.video-content').html();
    $('#main-video-container').find('.video-content').html(content);

    return true;
  });

});

/* Functionality for the slider on the home page */
$(".dot-slider-trigger").each(function(_, ele){
  // When a dot is clicked, replace the .dot-slider-container html
  // with the element attributed to data-attr-slider.
  $(ele).on('click', function(e){
    console.log("Clicked");
    var containerID = $(this).attr("data-attr-slider"),
    container = $(".dot-slider-container"+containerID),
    sliderBox = $(".dot-slider"),
    activeClass = "dot-slider-container-active",
    activeContainer = $("."+activeClass);

    if ($(container).hasClass(activeClass)) {
      console.log("Already active. Ignoring click");
      return false;
    }
    // Otherwise, swap the active class to container
    $(container).addClass(activeClass);
    $(activeContainer).removeClass(activeClass);

    e.stopPropagation();
    return false;
  });
});

/* Functionality to show and hide the tag filter on blog pages */
$('#tags-filter-toggle').on('click', function(e){

  var parent = $(this).closest('.tag-filter'),
  tags = $(parent).find('.tags'),
  caret = $(this).find('i'),
  tagsHidden = $(tags).hasClass("hidden");

  if (tagsHidden) {
    $(tags).removeClass("hidden");
    $(caret).removeClass("fa-caret-down");
    $(caret).addClass("fa-caret-up");
  } else {
    $(tags).addClass("hidden");
    $(caret).addClass("fa-caret-down");
    $(caret).removeClass("fa-caret-up");
  }
  e.stopPropagation();
  return false;
});

// culture of respect filter//
        $('li .filter').on('click', function(e) {
        	e.preventDefault();
               if($(this).attr('rel') == 'All'){
		      $('div[rel=\'div-Americas\']').show();
	              $('div[rel=\'div-Europe\']').show();
	              $('div[rel=\'div-North-Asia\']').show();
	              $('div[rel=\'div-South-Asia\']').show();
               }else{
	              $('div[rel=\'div-Americas\']').hide();
	              $('div[rel=\'div-Europe\']').hide();
	              $('div[rel=\'div-North-Asia\']').hide();
	              $('div[rel=\'div-South-Asia\']').hide();
	              $('div[rel=\'div-'+ $(this).attr('rel') +'\']').show();
              }
        });
        
//Why Jabil - scroll to top of main video when thumb is clicked
$('.vid-thumb').on('click', function(event) {
        $('html, body').animate({
	    scrollTop: $("#main-video-container").offset().top
	}, 1000);
});

//Start Toggle Search form
//toggle search input on icon click
$('.sb-icon-search').on('click', function(event) {
        $('.sb-search').toggleClass('sb-search-open');
});
//displays form submit when value is entered / hides when input has no value
$( "input.sb-search-input" ).keyup(function() {
  	if( $('input.sb-search-input').val() ) {
          $('.sb-search-submit').addClass('go-search');
    }
    else {
    	$('.sb-search-submit').removeClass('go-search');
    }
});
//close search input when click outside
$(document).mouseup(function (e) {
    var container = $(".sb-search");

    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    {
        container.removeClass('sb-search-open');
    }
});
//End Toggle Search form

// Helper function to get query string params.
var QueryString = function () {
  // This function is anonymous, is executed immediately and 
  // the return value is assigned to QueryString!
  var query_string = {};
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
        // If first entry with this name
    if (typeof query_string[pair[0]] === "undefined") {
      query_string[pair[0]] = pair[1];
        // If second entry with this name
    } else if (typeof query_string[pair[0]] === "string") {
      var arr = [ query_string[pair[0]], pair[1] ];
      query_string[pair[0]] = arr;
        // If third or later entry with this name
    } else {
      query_string[pair[0]].push(pair[1]);
    }
  } 
    return query_string;
} ();


// Start Expertise Hub 
// When filters are checked, add them to the list
var expertiseFilters = [];
$('.filter-group').find('input[type="checkbox"]').on('change', function(){
	var self = $(this),
	name = self.attr('name'),
	checked = self.prop('checked');
	if (checked) {
		expertiseFilters.push(name);
	} else {
		var idx = expertiseFilters.indexOf(name);
		expertiseFilters.splice(idx, 1);
	}
});

// listen to click event on submit button, generate a URL based on the global var,
// set window to that location
$('#expertise-filter').click(function(){
	var self = $(this),
	params = "";
	$.each(expertiseFilters, function(idx,ele){
		if (idx == 0) {
			params += "?f=" + encodeURIComponent(ele);
		} else {
			params += "&f=" + encodeURIComponent(ele);
		}
		
	});
	// TODO: Change this to the production URL
	window.location = window.location.pathname + params;
});

// Add URL params to filter array. used in function below.
var expertiseParams = [],
p = QueryString.f;
// Remove ASCII value for " " (%20) from the param
if (typeof(p) == "string") {
	var filter = decodeURIComponent(p);
	expertiseParams.push(filter);
} else if (typeof(p) != "undefined") {
	$.each(p, function(idx, ele){
		var filter = decodeURIComponent(ele);
		expertiseParams.push(decodeURIComponent(filter));
	});
}


// If page refreshes with params selected, check boxes automatically
$('.filter-group').find('input[type="checkbox"]').each(function(){
	var self = $(this),
	name = self.prop('name'),
	present = expertiseParams.indexOf(name);
	if (present >= 0) {
		self.prop('checked', true).change();
	}
});

$('ul.content-types').find('a').click(function(e){
	var ct = encodeURIComponent($.trim($(this).text())),
	path = window.location.pathname,
	params = "?ct=" + ct;
	$.each(expertiseFilters, function(idx,ele){
		params += "&f=" + encodeURIComponent(ele);
	});
	
	e.preventDefault();
	
	window.location = path + params;
});

var contentType = QueryString.ct;
if (typeof(contentType) != 'undefined') {
	$('ul.content-types').find('a').removeClass("active");
	$('ul.content-types').find('a').each(function(){
		if ($(this).text() == decodeURIComponent(contentType)) {
			$(this).addClass("active");
		}
	});
	
}

// initial page is 2, because results are already loaded one time
var expertisePage = 2;
// Functionality for the expertise hub 'show more' button
$('#expertise-more-results').click(function(){
	var self = $(this),
	parent = self.parents('.row'),
	params = window.location.search,
	path = "/templates/JDC Expertise Hub ajax",
	url = "";
	// check to see if params are empty
	if ($.trim(params)) {
		params += "&page=" + expertisePage;
	} else {
		params = "?page=" + expertisePage;
	}
	url = path + params;
	$.get(url, function(data){
		// get the .row this button is in, and insert data before it
		$(data).insertBefore(parent);
		expertisePage +=1;
	});
});

// Clear button - remove checked items
$('#clear-filter').click(function(){
$('.filter-group').find('input[type="checkbox"]').each(function(){
	$(this).prop('checked', false).change();
});
});
// End Expertise Hub
