<script id = "race74a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race74={};
	myVars.races.race74.varName="Object[13471].pause";
	myVars.races.race74.varType="@varType@";
	myVars.races.race74.repairType = "@RepairType";
	myVars.races.race74.event1={};
	myVars.races.race74.event2={};
	myVars.races.race74.event1.id = "cs-slider11";
	myVars.races.race74.event1.type = "onmouseout";
	myVars.races.race74.event1.loc = "cs-slider11_LOC";
	myVars.races.race74.event1.isRead = "False";
	myVars.races.race74.event1.eventType = "@event1EventType@";
	myVars.races.race74.event2.id = "cs-title-slider";
	myVars.races.race74.event2.type = "onmouseout";
	myVars.races.race74.event2.loc = "cs-title-slider_LOC";
	myVars.races.race74.event2.isRead = "False";
	myVars.races.race74.event2.eventType = "@event2EventType@";
	myVars.races.race74.event1.executed= false;// true to disable, false to enable
	myVars.races.race74.event2.executed= false;// true to disable, false to enable
</script>

