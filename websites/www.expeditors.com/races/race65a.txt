<script id = "race65a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race65={};
	myVars.races.race65.varName="Object[3837].UL";
	myVars.races.race65.varType="@varType@";
	myVars.races.race65.repairType = "@RepairType";
	myVars.races.race65.event1={};
	myVars.races.race65.event2={};
	myVars.races.race65.event1.id = "Lu_Id_li_11";
	myVars.races.race65.event1.type = "onmouseover";
	myVars.races.race65.event1.loc = "Lu_Id_li_11_LOC";
	myVars.races.race65.event1.isRead = "False";
	myVars.races.race65.event1.eventType = "@event1EventType@";
	myVars.races.race65.event2.id = "Lu_Id_li_34";
	myVars.races.race65.event2.type = "onmouseover";
	myVars.races.race65.event2.loc = "Lu_Id_li_34_LOC";
	myVars.races.race65.event2.isRead = "True";
	myVars.races.race65.event2.eventType = "@event2EventType@";
	myVars.races.race65.event1.executed= false;// true to disable, false to enable
	myVars.races.race65.event2.executed= false;// true to disable, false to enable
</script>

