$(function(){

    $("ul.dropdown li").click(function(){
    	$('ul:first',this).css('display', 'block');
    });
    
    $("ul.dropdown li").hover(function(){

    	$('ul:first',this).css('display', 'none');
    
    });
    
    $("ul.dropdown li ul li:has(ul)").find("a:first").append(" &raquo; ");
	
	
	$("div#language-dropdown").click(function(){
    	$('ul.sub_menu').show();
    });
	
    $("div#language-dropdown").mouseleave(function(){
    	$('ul.sub_menu').hide();
    });
	
	$("ul#primary-navigation li").click(function(){
		$(this).find('ul').toggle();
    });
	
	$("ul#primary-navigation li").hover(function(){
		$(this).find('ul').show();
    });
	
	$("ul#primary-navigation li").mouseleave(function(){
    	$(this).find('ul').hide();
    });
	
    /*$("ul.language-dropdown li ul li:has(ul)").find("a:first").append(" &raquo; ");*/

});