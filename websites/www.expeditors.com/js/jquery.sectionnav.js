/*
 * jQuery plugin for displaying the section navigation menu.
 */

(function($) {
	$.fn.sectionNav = function(options) {

		var defaults = {
			
		};

		var options = $.extend(defaults, options);
	
		// sets every menu is set to display: none in style.css
		
		return this.each(function(){
			list = $(this);
			
			// displays the first level menu
			list.find('.'+options.level1).toggle();
			
			// displays the second level menu
			list.find('.'+options.level1+' .level-2').toggle();
			
			// displays the third level menu(if there is any)
			list.find('.'+options.level2+' li').toggle();
			
			// sets the red banner behind current level 2 menu
			list.find('.'+options.level1+' .'+options.level2+' a').first().addClass('nav-level-2-current');
			
			// sets the color of the current page(only for level 3 menus)
			list.find('.'+options.level1+' .'+options.level2+' li.'+options.level3+" a").css('color', '#000');
		
			// highlights primary navigation menu item
			$('#primary-navigation li.'+options.level1).css('background-color','#555');
			
			// Page heading
			$('.page-heading').html(list.find('.'+options.level1+' span').html());
		});
	};
	
	
})(jQuery);