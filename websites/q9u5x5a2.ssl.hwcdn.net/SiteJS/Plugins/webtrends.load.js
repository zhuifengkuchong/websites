// WebTrends SmartSource Data Collector Tag v10.4.1
// Copyright (c) 2014 Webtrends Inc.  All rights reserved.
// Tag Builder Version: 4.1.3.2
// Created: 2014.03.27

var swDCSID;
var swDomain;
var wtodDCSID;

var docHost = window.location.hostname.toLowerCase();
// 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/Plugins/ameren.com' or 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/Plugins/www.ameren.com' or 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/Plugins/apps.ameren.com' or 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/Plugins/forms.ameren.com' or 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/Plugins/surveyforms.ameren.com' URLs to production Webtrends dcsid & domain
if (docHost.indexOf("https://q9u5x5a2.ssl.hwcdn.net/SiteJS/Plugins/ameren.com") == 0 ||
    docHost.indexOf("https://q9u5x5a2.ssl.hwcdn.net/SiteJS/Plugins/www.ameren.com") >= 0 ||
    docHost.indexOf("https://q9u5x5a2.ssl.hwcdn.net/SiteJS/Plugins/apps.ameren.com") >= 0 ||
    docHost.indexOf("https://q9u5x5a2.ssl.hwcdn.net/SiteJS/Plugins/forms.ameren.com") >= 0 ||
    docHost.indexOf("https://q9u5x5a2.ssl.hwcdn.net/SiteJS/Plugins/surveyforms.ameren.com") >= 0) {

        swDCSID = "dcsduc3pvx475k1x4a7tce5sz_1k9x";
        swDomain = "https://q9u5x5a2.ssl.hwcdn.net/SiteJS/Plugins/webstats.ameren.com";
		wtodDCSID = "dcs2226bh7o5uqtotv8bno3y4_7e3v";
}
// 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/Plugins/www2.ameren.com' URLs to production Webtrends dcsid & domain
else if (docHost.indexOf("https://q9u5x5a2.ssl.hwcdn.net/SiteJS/Plugins/www2.ameren.com") >= 0) {
	swDCSID = "dcs16ruaby475kdidkh0kc6sz_1h3i";
    swDomain = "https://q9u5x5a2.ssl.hwcdn.net/SiteJS/Plugins/webstats.ameren.com";
    wtodDCSID = "dcs22238ufi5gb88zz7oq4a9j_5j1e";
}
// 'localhost' or 'ecust'URLs to production Webtrends dcsid & domain
else if ((docHost.indexOf("localhost") >= 0) || (docHost.indexOf("ecust") >= 0)) {
    swDCSID = "dcsvnb6tax475kgc75cg5zhs8_3g3t";
    swDomain = "https://q9u5x5a2.ssl.hwcdn.net/SiteJS/Plugins/webtrends.ameren.com";
    wtodDCSID = "dcs222jowwewrdmlgwr0ra8hm_2s5q";
}
// All other URLs to internal Webtrends dcsid & domain
else {

    swDCSID = "dcsmv9mojx475kpn9i53andi2_6k7o";
    swDomain = "https://q9u5x5a2.ssl.hwcdn.net/SiteJS/Plugins/webtrends.ameren.com";
    wtodDCSID = "dcs222ki9ywrqczs8dypto6s2_6o5e";
}

window.webtrendsAsyncInit=function(){
    var dcs=new Webtrends.dcs().init({
        dcsid: wtodDCSID,
        domain: "https://q9u5x5a2.ssl.hwcdn.net/SiteJS/Plugins/statse.webtrendslive.com",
        timezone: -6,
        i18n: false,
        offsite: false,
        download: true,
        downloadtypes: "xls,doc,pdf,txt,csv,zip,docx,xlsx,rar,gzip",
        anchor: false,
        javascript: true,
        onsitedoms: new RegExp("https://q9u5x5a2.ssl.hwcdn.net/SiteJS/Plugins/ameren.com"),
        fpcdom: ".ameren.com",
        plugins:{
            //hm:{src:"../../../s.webtrends.com/js/webtrends.hm-1.js"/*tpa=https://s.webtrends.com/js/webtrends.hm.js*/},
            oss: {src: "../../../www.ameren.com/SiteJS/Plugins/webtrends.oss.js"/*tpa=https://www.ameren.com/SiteJS/Plugins/webtrends.oss.js*/ },
			link_t: {src:"../../../www.ameren.com/SiteJS/Plugins/webtrends.link_t.js"/*tpa=https://www.ameren.com/SiteJS/Plugins/webtrends.link_t.js*/},
			replicate: {
				src:"../../../www.ameren.com/SiteJS/Plugins/webtrends.replicate.js"/*tpa=https://www.ameren.com/SiteJS/Plugins/webtrends.replicate.js*/,
				servers: [{
					domain:swDomain,
			        dcsid:swDCSID
                }],
				callbackTimeout: 200
			}
        }
	}).track();
};
(function(){
    var s=document.createElement("script"); s.async=true; s.src="../../../www.ameren.com/SiteJS/Plugins/webtrends.js"/*tpa=https://www.ameren.com/SiteJS/Plugins/webtrends.js*/;
    var s2=document.getElementsByTagName("script")[0]; s2.parentNode.insertBefore(s,s2);
}());
