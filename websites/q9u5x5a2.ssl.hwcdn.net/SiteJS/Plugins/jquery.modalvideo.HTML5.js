var overlayTrigger;
(function($) {
    var modalDivID = "videoLinkModal";

    $(window).load(function () {
        var videoLinks = $('a[rel=video]'),
            playBtnSrc = '../../-/media/Common-Assets/Images/Button-Images/playbtn.png'/*tpa=https://q9u5x5a2.ssl.hwcdn.net/-/media/Common-Assets/Images/Button-Images/playbtn.png*/,
            playBtnHoverSrc = '../../-/media/Common-Assets/Images/Button-Images/playbtnHover.png'/*tpa=https://q9u5x5a2.ssl.hwcdn.net/-/media/Common-Assets/Images/Button-Images/playbtnHover.png*/,
            playBtnHeight = 24,
            playBtnPadding = 10;

        var isAdRotator = 0,
            adRotatorIsDone = 0,
            doLink = 0;

        videoLinks.live('loaded', function() {
            var link = $(this),
                img = link.find('img');

            //Do only one adRotator video link per page load but do all other video links
            isAdRotator = link.parents('[id="adRotatorImages"]').length;
            if (isAdRotator) {
                if (adRotatorIsDone) {
                    doLink = 0;
                }
                else {
                    doLink = 1;
                    adRotatorIsDone = 1;
                }
            }
            else {
                doLink = 1;
            }

            if (!link.parents('.bannerImageRotatorInfo').length && img.length && doLink) {

                var imgWidth = img.width(),
                    imgHeight = img.height();

                if (imgWidth > 0) {
                    var moveDown = (imgHeight - (playBtnHeight + playBtnPadding)) + "px", // The play button is 24 pixels tall + whatever padding is needed

                    divWrapper = $('<div class="modalPlayBtn" style="background-image: url(\'' + img.attr('src') + '\');' +
                                    ' background-repeat: no-repeat;' +
                                    ' width: ' + (imgWidth + 5) + 'px; height: ' + (imgHeight + 5) + 'px; ' +
                                    ' text-align: left; cursor: pointer; margin:0;"></div>'),

                    playBtn = $('<img src="' + playBtnSrc + '" ' +
                                'style="position: relative; top: ' + moveDown + '; left: ' + playBtnPadding + 'px;" />');

                    img.wrap(divWrapper);

                    img.parent().append(playBtn);

                    img.remove();
                }
                else {
                    img.bind('load', function() {
                        var $img = $(this),
                            imgWidth = $img.width(),
                            imgHeight = $img.height();


                        var moveDown = (imgHeight - (playBtnHeight + playBtnPadding)) + "px", // The play button is 24 pixels tall + whatever padding is needed

                        divWrapper = $('<div class="modalPlayBtn" style="background-image: url(\'' + $img.attr('src') + '\');' +
                                        ' background-repeat: no-repeat;' +
                                        ' width: ' + (imgWidth + 5) + 'px; height: ' + (imgHeight + 5) + 'px; ' +
                                        ' text-align: left; cursor: pointer; margin:0;"></div>'),

                        playBtn = $('<img src="' + playBtnSrc + '" ' +
                                    'style="position: relative; top: ' + moveDown + '; left: ' + playBtnPadding + 'px;" />');

                        $img.wrap(divWrapper);

                        $img.parent().append(playBtn);

                        $img.remove();
                    });
                }
            }

            var wrappedImages = $('.modalPlayBtn');

            if (wrappedImages.length) {
                $('.modalPlayBtn').each(function() {
                    var img = $(this);

                    img.hover(function() {
                        $(this).find('img').attr('src', playBtnHoverSrc);
                    },

                    function() {
                        $(this).find('img').attr('src', playBtnSrc);
                    });
                });
            }
            else {
                $(window).bind('load', function() {
                    $('.modalPlayBtn').each(function() {
                        var img = $(this);

                        img.hover(function() {
                            $(this).find('img').attr('src', playBtnHoverSrc);
                        },

                        function() {
                            $(this).find('img').attr('src', playBtnSrc);
                        });
                    });
                });
            }


        });

        videoLinks.trigger('loaded');


        videoLinks.live('click', function(e) {
            e.preventDefault();

            var videoLink = $(this),
                href = videoLink.attr('href'),
                width = (videoLink.attr('vidwidth')) ? videoLink.attr('vidwidth') : 560,
                height = (videoLink.attr('vidheight')) ? videoLink.attr('vidheight') : 340;

            callModalVideo(href, width, height);

            var X = "";

        });
    });

})(jQuery);

function callModalVideo(src, width, height)
{
    var modalDivID = "videoLinkModal",
        modalDiv = $('#' + modalDivID),
        flowplayerDestination = $("<a></a>").attr('id', 'flowplayerDestination').attr('href', src),
        isYouTube = false;

    //if existing player div, remove from DOM
	$(modalDiv).remove();

    //Toggle between YouTube / FlowPlayer dependent upon video being linked to.
	if (src.toLowerCase().indexOf("https://q9u5x5a2.ssl.hwcdn.net/SiteJS/Plugins/youtube.com") >= 0) {
	    $('body').append('<div id="' + modalDivID + '"><iframe width="560" height="315" src="' + src + '" frameborder="0" allowfullscreen></iframe></div>');
	    isYouTube = true;
	}
	else {
	    //add player div to DOM
	    $('body').append('<div id="' + modalDivID + '"><div id="player"></div></div>');
	}


    //build overlay
	var overlayTriggerOptions =
    {
        target: $('#' + modalDivID),

        api: true,

        top: 'center',

        expose:
        {
            color: '#666'
        },

        onBeforeLoad: function () {
            if (!isYouTube) {
                $(document).trigger('videoStarted');
                $('#' + modalDivID).css({ width: width + 'px', height: height + 'px' }).append(flowplayerDestination);
            }
            else {
                $('#' + modalDivID).css({ width: width + 'px', height: height + 'px' });
            }
        },

        onLoad: function () {
            if (!isYouTube) {
                //install Flowplayer
                $("#player").flowplayer({
                    key: '$297477298417371',
                    playlist: [
                       // a list of type-url mappings in picking order
                       [
                          { webm: src },
                          { mp4: src },
                          { ogg: src }
                       ]
                    ],
                    ratio: 9 / 16 // video with 16/9 aspect ratio
                });

                //add 'autoplay' attribute to the 'video' element
                $("video").attr("autoplay", "autoplay");
            }
        },

        onBeforeClose: function () {
            if (!isYouTube) {
                $(document).trigger('videoEnded');
                flowplayerDestination.remove();
                $("#player").flowplayer().unload();
            }
        }
    };

    //add overlay to DOM
	overlayTrigger = $("<div></div>").attr('id', 'overlayTrigger').hide().appendTo('body').overlay(overlayTriggerOptions);

	overlayTrigger.load();
}