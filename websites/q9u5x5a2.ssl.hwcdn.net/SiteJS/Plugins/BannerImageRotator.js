(function ($, window) {
    var control = null,

        showControl = null,

        autoScroll = null,

        imageRotatorInfoClass = '.bannerImageRotatorInfo',

        imageRotatorWrapperClass = '.bannerImageRotator',

        imageRotatorControlClass = '.bannerImageRotatorControl',

        imageRotatorControlBtnClass = '.bannerImageRotatorControlBtn',

        imageRotatorControlPrevBtnClass = 'bannerImageRotatorPrevBtn',

        imageRotatorControlNextBtnClass = 'bannerImageRotatorNextBtn',

        imageRotatorControlActiveBtnClass = 'bannerImageRotatorActiveBtn',

        imageRotatorControlInactiveBtnClass = 'bannerImageRotatorInactiveBtn',

        imageWrapperClass = '.bannerImageWrapper',

        imageRotatorControlTextClass = '.banerImageRotatorControlText',

        imageRotatorImages = [],

        maxImages = 5,

        imageRotatorControlWidth = null,

        imageRotatorButtonWidth = null,

        imageRotatorButtonsLeftMargin = null,

        currentImage = 0,

        height = 0,

        imageRotatorInterval = null;

    $(document).ready(function () {
        var imageRotatorWrapper = $(imageRotatorWrapperClass),

            hiddenPanel = $(imageRotatorInfoClass);

        showControl = hiddenPanel.find('input:hidden[id$=hiddenShowControl]').val();

        autoScroll = hiddenPanel.find('input:hidden[id$=hiddenAutoScroll]').val();

        imageRotatorImages = GetImages(hiddenPanel);

        $(document).bind('videoStarted', function () {

            stopRotate();

        });

        $(document).bind('videoStarted', function () {
            if (imageRotatorWrapper.length != 0) {
                BeginRotate(imageRotatorWrapper, autoScroll);
            }
        });

        if (imageRotatorImages.length) {
            //remove images exceeding maxImages allowed
            if (imageRotatorImages.length > maxImages) {
                imageRotatorImages.splice(maxImages - 1, imageRotatorImages.length - maxImages);
            }

            if (imageRotatorImages.length > 1) {
                InitializeControl(imageRotatorWrapper, showControl, autoScroll);

                BeginRotate(imageRotatorWrapper, autoScroll);
            }
            else if (imageRotatorImages.length == 1) {
                imageRotatorWrapper.append($(imageRotatorImages[0]).show());
            }
        }

    });

    function GetImages(container) {
        var images = [];

        container.find("img[id^='rotatorImage']").each(function () {
            var $this = $(this);

            if ($this.height() > height)
                height = $this.height();

            var anchor = $this.parent('a');

            if (anchor.length) {
                $this.css('cursor', 'pointer');

                if (anchor.attr('rel') == "video") {
                    var vidWidth = (anchor.attr('vidwidth')) ? anchor.attr('vidwidth') : '560';

                    var vidHeight = (anchor.attr('vidheight')) ? anchor.attr('vidheight') : '340';

                    $this.click(function () {
                        callModalVideo(anchor.attr('href'), vidWidth, vidHeight);
                    });
                }
                else {
                    $this.click(function () {
                        if (anchor.attr('target') == '_blank') {
                            var win = window.open(anchor.attr('href'), '_blank');
                            win.focus();
                        }
                        else {
                            window.location = anchor.attr('href');
                        }
                    });
                }
            }
        });

        return container.find("img[id^='rotatorImage']").hide();
    }

    function InitializeControl(container, show, autoScroll) {
        control = container.find(imageRotatorControlClass);
        containerWidth = container.width();
        containerHeight = container.parent().height();

        var containerPosition = container.position(),
            controlLeft = ((containerPosition.left + containerWidth) - (control.width() + 5)),
            controlTop = ((containerPosition.top + containerHeight) - (control.height() + 20));

        control.css({ top: controlTop + 'px', left: controlLeft + 'px' });

        if (show == "true") {
            // add buttons to Banner navigation control
            //Calculate margin-left necessary to center buttons in control
            imageRotatorControlWidth = control.width();
            imageRotatorButtonWidth = 12;
            imageRotatorButtonsLeftMargin = (imageRotatorControlWidth - (imageRotatorButtonWidth * (imageRotatorImages.length + 2))) / 2;

            $(imageRotatorControlBtnClass).append('<div class="' + imageRotatorControlPrevBtnClass + '" style="margin-left:' + imageRotatorButtonsLeftMargin + 'px;"></div>');
            for (var i = 1; i < imageRotatorImages.length + 1; i++) {
                if (i == 1) {
                    $(imageRotatorControlBtnClass).append('<div id="' + i.toString() + '" class="' + imageRotatorControlActiveBtnClass + '">' + i.toString() + '</div>');
                }
                else {
                    $(imageRotatorControlBtnClass).append('<div id="' + i.toString() + '" class="' + imageRotatorControlInactiveBtnClass + '">' + i.toString() + '</div>');
                }
            }
            $(imageRotatorControlBtnClass).append('<div class="' + imageRotatorControlNextBtnClass + '"></div>');

            control.show();
            bindControl(control, autoScroll, container);
        }
        else {
            bindControl(control, autoScroll, container);
        }
    }

    function BeginRotate(container, autoScroll) {
        var imageWrapper = container.find(imageWrapperClass);

        for (var i = 0; i < imageRotatorImages.length; i++) {
            imageWrapper.append(imageRotatorImages[i]);
        }

        imageWrapper.find('img').eq(currentImage).fadeIn("slow");

        setImageRotatorInterval(autoScroll, container);

        updateActiveImage(container);
    }

    function setImageRotatorInterval(autoScroll, container) {
        if (autoScroll == "true" || autoScroll == true) {
            imageRotatorInterval = window.setInterval(function () {
                rotate(container);

            }, 8000);
        }
    }

    function rotate(container, dir, normal) {
        var imageWrapper = container.find(imageWrapperClass);
        var nextImage = null;

        dir = dir || "next";

        normal = (normal == undefined) ? true : normal;

        if (dir == "prev" || dir == "next") {
            //go to previous or next image in rotation
            nextImage = dir == "prev" ? currentImage - 1 : currentImage + 1;
        }
        else {
            //jump to a specific image 
            nextImage = dir - 1;
        }

        if (nextImage >= imageRotatorImages.length)
            nextImage = 0;
        else if (nextImage < 0)
            nextImage = imageRotatorImages.length - 1;

        var curImage = imageWrapper.find('img').eq(currentImage);

        var nextImageObj = imageWrapper.find('img').eq(nextImage);

        if (normal) {
            curImage.fadeOut(2000, function () {
                updateActiveImage(container);

                nextImageObj.fadeIn(2000);
            });
        }
        else {
            curImage.fadeOut(500, function () {
                updateActiveImage(container);

                nextImageObj.fadeIn(500);
            });
        }

        currentImage = nextImage;
    }

    function stopRotate() {
        window.clearInterval(imageRotatorInterval);
    }

    function updateActiveImage(container) {
        var showControl = (container.find(imageRotatorControlClass).css('display') != "none");

        if (showControl == true) {
            //remove previous set of navigation buttons from Banner navigation control
            $(imageRotatorControlBtnClass).empty();

            //re-add the navigation buttons to reflect the current active image
            $(imageRotatorControlBtnClass).append('<div class="' + imageRotatorControlPrevBtnClass + '" style="margin-left:' + imageRotatorButtonsLeftMargin + 'px;"></div>');
            for (var i = 1; i < imageRotatorImages.length + 1; i++) {
                if (i == currentImage + 1) {
                    $(imageRotatorControlBtnClass).append('<div id="' + i.toString() + '" class="' + imageRotatorControlActiveBtnClass + '">' + i.toString() + '</div>');
                }
                else {
                    $(imageRotatorControlBtnClass).append('<div id="' + i.toString() + '" class="' + imageRotatorControlInactiveBtnClass + '">' + i.toString() + '</div>');
                }
            }

            $(imageRotatorControlBtnClass).append('<div class="' + imageRotatorControlNextBtnClass + '"></div>');

            bindControl(control, autoScroll, container);
        }
    }

    function bindControl(control, autoScroll, container) {
        //bind Prev and Next button click events
        control.find('.' + imageRotatorControlPrevBtnClass).bind('click', { container: container, autoScroll: autoScroll }, prevClick);
        control.find('.' + imageRotatorControlNextBtnClass).bind('click', { container: container, autoScroll: autoScroll }, nextClick);

        //bind a click event to each image button
        control.find('.' + imageRotatorControlActiveBtnClass).bind('click', { container: container, autoScroll: autoScroll }, imageClick);
        control.find('.' + imageRotatorControlInactiveBtnClass).bind('click', { container: container, autoScroll: autoScroll }, imageClick);
    }

    function prevClick(e) {
        //rotate to previous image
        var container = e.data.container;

        var autoScroll = e.data.autoScroll;

        if (autoScroll == "true" || autoScroll == true) {
            stopRotate();
        }

        rotate(container, "prev", false);

        if (autoScroll == "true" || autoScroll == true) {
            setImageRotatorInterval(autoScroll, container);
        }
    }

    function nextClick(e) {
        //rotate to next image
        var container = e.data.container;

        var autoScroll = e.data.autoScroll;

        if (autoScroll == "true" || autoScroll == true) {
            stopRotate();
        }

        rotate(container, "next", false);

        if (autoScroll == "true" || autoScroll == true) {
            setImageRotatorInterval(autoScroll, container);
        }
    }

    function imageClick(e) {
        //go to selected image
        var container = e.data.container;

        var autoScroll = e.data.autoScroll;

        if (autoScroll == "true" || autoScroll == true) {
            stopRotate();
        }

        rotate(container, $(this).attr("id"), false);

        if (autoScroll == "true" || autoScroll == true) {
            setImageRotatorInterval(autoScroll, container);
        }
    }

})(jQuery, window);