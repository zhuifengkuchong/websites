var defaultClass = "buttonImage",
    hoverClass = "buttonImageHover",
    selectedClass = "buttonImageSelected";

(function ($) {
    $(document).ready(function () {
        $('div.' + defaultClass).click(function () {
            SwitchSubNavigation($(this));
        });
        
        $('div.' + defaultClass).hover(function () {
            var $this = $(this);

            $this.addClass(hoverClass);
        },
        function () {
            var $this = $(this);

            $this.removeClass(hoverClass);
        });
                
    });
})(jQuery);

$(window).load(function () {
    //Set LeftNav Height for SubNavigation Pages
    if ($('.dynamicContent').length > 0) {
        SetLeftNavHeightforSubNavPage();
    }

    //This is done so that any Video Carousels within HTML Blobs load properly - note that the 1 millisecond sleep seems to be critical for Next button to work properly.
    setTimeout(function () {
        $('.dynamicContentDiv').hide();
        $('#DynamicContentDiv_0').show();
    }, 1);

    PositionAddRotator();
});

function SwitchSubNavigation(div) {
    var IDInt = $(div).attr('id').replace("buttonImage_", "");

    $('.' + selectedClass).removeClass(selectedClass);
    $(div).removeClass(hoverClass).addClass(selectedClass);

    $('.dynamicContentDiv').hide();
    $('#DynamicContentDiv_' + IDInt).show();
    
    PositionAddRotator();
}

function SetLeftNavHeightforSubNavPage() {
    var curInnerNavHeight = $('#leftNavWrapper').height();

    if (curInnerNavHeight < 346) {
        $('#leftNavigation').css('height', '346px');
    }
    else {
        $('#leftNavigation').css('height', curInnerNavHeight + 'px');
    }          
}

function PositionAddRotator() {
    if ($('#dynamicContent').length != 0) {
        setTimeout(function () {
            var contentHeight = $("#contentBody").height();

            if (contentHeight > 580) {
                contentHeight = contentHeight - 567;
                $('#adRotatorImages').css('bottom', '-' + contentHeight + 'px');
            } else {
                $('#adRotatorImages').css('bottom', '0px');
            }
        }, 1);
    }   
}
