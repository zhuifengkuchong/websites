if (!String.format) {
    String.prototype.format = function () {
        var str = String(this),

			replace;

        if (typeof arguments[0] == "string") {
            for (var i = 0; i < arguments.length; i++) {
                replace = new RegExp('\\{' + i + '\\}', 'g');

                str = str.replace(replace, arguments[i]);
            }

            return str;
        }
        else if (typeof arguments[0] == "object") {
            var obj = arguments[0];

            for (var prop in obj) {
                replace = new RegExp('\\{' + prop + '\\}', 'g');

                str = str.replace(replace, obj[prop]);
            }

            return str;
        }
        else {
            return;
        }
    };
}

// QA *********************************************************************************
jQuery(document).ready(function () {
    $('.qa').each(function (i) {
        var qa = $(this)

        var originalValues = new Array();

        qa.data('qa', i);

        var dialog = qa.siblings('.qaDialog').data('qa', i);

        dialog.find('input:text').each(function () {
            originalValues.push($(this).val());
        });

        dialog.find('textarea').each(function () {
            originalValues.push($(this).val());
        });

        dialog.data('originalValues', originalValues);
    });

    $('.qaEditLink').click(function (e) {
        e.preventDefault();

        var qa = $(this).parents('.ms-toolbarContainer').siblings('.qa:eq(0)');

        var qaNumber = qa.data('qa');

        hideFlashApps();

        var modalButtons =
        {
            "Cancel": function () {
                var dialogBox = $(this);

                var originalValues = dialogBox.data('originalValues');

                var textAreas = dialogBox.find('textarea');

                for (var i = 0; i < originalValues.length; i++) {
                    textAreas.eq(i).val(originalValues[i]);
                }

                showFlashApps();

                dialogBox.dialog("close");
            },

            "Ok": function () {
                var dialogBox = $(this);

                var originalValues = dialogBox.data('originalValues');

                var pos = 0;

                dialogBox.find('textarea').each(function () {
                    originalValues[pos++] = $(this).val();
                });

                dialogBox.data('originalValues', originalValues);

                var qa = $('.qa').eq(qaNumber);

                qa.find('span[id$=qaPreviewQuestion]').text(originalValues[0]);

                qa.find('span[id$=qaPreviewAnswer]').html(originalValues[1]);

                qa.show();

                showFlashApps();

                $(this).dialog("close");
            }
        };

        var dialogOptions =
        {
            width: 900,

            buttons: modalButtons,

            draggable: false,

            modal: true,

            resizable: false,

            open: onModalOpen,

            beforeclose: onModalClose
        };


        $('.qaDialog').each(function () {
            if ($(this).data('qa') == qaNumber)
                $(this).dialog(dialogOptions);
        });
    });

    $('.qaClear').click(function (e) {
        e.preventDefault();

        var qa = $(this).parents('.ms-toolbarContainer').siblings('.qa:eq(0)');

        var qaNumber = qa.data('qa');

        var originalValues;

        $('.qaDialog').each(function () {
            if ($(this).data('qa') == qaNumber) {
                var dialog = $(this);

                originalValues = new Array(dialog.data('originalValues').length);

                dialog.data('originalValues', originalValues);

                dialog.find('input[type^=text]').each(function () {
                    $(this).val("");
                });

                dialog.find('textarea').each(function () {
                    $(this).val("");
                });
            }
        });

        qa.find('span[id$=qaPreviewQuestion]').html("");

        qa.find('span[id$=qaPreviewAnswer]').html("");
    });

    $('.qaQuestionLink').live('click', function (e) { e.preventDefault(); });

    $('.qaImgClosed').live('click', function () {
        var qaAnswer = $(this).parent().next('.qaAnswer');

        if (qaAnswer.css('display') == 'none') {
            $(this).attr('class', 'qaImgOpen');

            qaAnswer.show();
        }
        else {
            $(this).attr('class', 'qaImgClosed');

            qaAnswer.hide();
        }

    });

    $('.qaImgOpen').live('click', function () {
        var qaAnswer = $(this).parent().next('.qaAnswer');

        if (qaAnswer.css('display') == 'none') {
            $(this).attr('class', 'qaImgOpen');

            qaAnswer.show();
        }
        else {
            $(this).attr('class', 'qaImgClosed');

            qaAnswer.hide();
        }

    });

    $('.qaAnswer').each(function () {
        if ($(this).text().search(/.*<.*>.*/) != -1) {
            $(this).html($(this).text());
        }
    });

});
// QA *********************************************************************************

// Flash App *********************************************************************************
jQuery(document).ready(function () {
    // {0} Width
    // {1} Height
    // {2} Src
    var FLASH_OBJECT = "<object style=\"z-index: 1\" width=\"{0}\" height=\"{1}\">" +
                        "<param name=\"movie\" value=\"{2}\" />" +
                        "<param name=\"wmode\" value=\"transparent\" />" +
                        "<param name=\"allowFullScreen\" value=\"true\" />" +
                        "<param name=\"allowscriptaccess\" value=\"true\" />" +
                        "<embed src=\"{2}\" wmode=\"transparent\" type=\"application/x-shockwave-flash\" " +
                        "allowscriptaccess=\"true\" allowfullscreen=\"true\" " +
                        "width=\"{0}\" height=\"{1}\">" +
                        "</embed></object>";

    $('.flashApp').each(function (i) {
        var flashApp = $(this)

        var originalValues = new Array();

        flashApp.data('flashApp', i);

        var dialog = flashApp.siblings('.flashAppDialog').data('flashApp', i);

        dialog.find('input:text').each(function () {
            originalValues.push($(this).val());
        });

        dialog.data('originalValues', originalValues);
    });

    $('.flashAppEditLink').click(function (e) {
        e.preventDefault();

        var flashApp = $(this).parents('.ms-toolbarContainer').siblings('.flashApp:eq(0)');

        //flashApp.hide();
        hideFlashApps();
        var flashAppNumber = flashApp.data('flashApp');

        var modalButtons =
        {
            "Cancel": function () {
                var dialogBox = $(this);

                var originalValues = dialogBox.data('originalValues');

                var textInputs = dialogBox.find('*:text');

                for (var i = 0; i < originalValues.length; i++) {
                    textInputs.eq(i).val(originalValues[i]);
                }

                showFlashApps();

                dialogBox.dialog("close");
            },

            "Ok": function () {
                var dialogBox = $(this);

                var originalValues = dialogBox.data('originalValues');

                var pos = 0;

                dialogBox.find('*:text').each(function () {
                    originalValues[pos++] = $(this).val();
                });

                dialogBox.data('originalValues', originalValues);

                var flashApp = $('.flashApp').eq(flashAppNumber);


                // {0} Width    [0] Src
                // {1} Height   [1] Width
                // {2} Src      [2] Height
                flashApp.html(FLASH_OBJECT.format({ 0: originalValues[1], 1: originalValues[2], 2: originalValues[0] }));

                showFlashApps();

                $(this).dialog("close");
            }
        };

        var dialogOptions =
        {
            width: 450,

            buttons: modalButtons,

            draggable: false,

            modal: true,

            resizable: false,

            open: onModalOpen,

            beforeclose: onModalClose
        };


        $('.flashAppDialog').each(function () {
            if ($(this).data('flashApp') == flashAppNumber)
                $(this).dialog(dialogOptions);
        });
    });

    $('.flashAppClear').click(function (e) {
        e.preventDefault();

        var flashApp = $(this).parents('.ms-toolbarContainer').siblings('.flashApp:eq(0)');

        var flashAppNumber = flashApp.data('flashApp');

        var originalValues;

        $('.flashAppDialog').each(function () {
            if ($(this).data('flashApp') == flashAppNumber) {
                var dialog = $(this);

                originalValues = new Array(dialog.data('originalValues').length);

                dialog.data('originalValues', originalValues);

                dialog.find('input[type^=text]').each(function () {
                    $(this).val("");
                });

                dialog.find('textarea').each(function () {
                    $(this).val("");
                });
            }
        });

        flashApp.html("");
    });

    $('.ameren-flashAppField').each(function (i) {
        var id = 'ameren-flashApp' + i;

        $(this).attr('id', id);

        var flashVars = {};

        var params = { wmode: 'transparent' };

        swfobject.embedSWF($(this).attr('rel'), id, $(this).attr('width'), $(this).attr('height'), "10", "Unknown_83_filename"/*tpa=https://q9u5x5a2.ssl.hwcdn.net/_layouts/AmerenPublishing/expressInstall.swf*/, flashVars, params, {}, swfLoaded);
    });
});

function swfLoaded(e) {

    if (!e.success) {
        var a = $('<a></a>').attr('href', 'http://www.adobe.com/go/getflashplayer')
                    .append($("<img />").attr('src', 'Unknown_83_filename'/*tpa=https://q9u5x5a2.ssl.hwcdn.net/_layouts/AmerenPublishing/AmerenGetFlash.jpg*/));

        $('#' + e.id).append(a);

    }
}
// Flash App *********************************************************************************

// Video Link *********************************************************************************
jQuery(document).ready(function () {
    $('.videoLink').each(function (i) {
        var videoLink = $(this);

        var originalValues = new Array();

        videoLink.data('videoLink', i);

        var dialog = videoLink.siblings('.videoDialog').data('videoLink', i);

        dialog.find('input[type^=text]').each(function () {
            originalValues.push($(this).val());
        });

        dialog.data('originalValues', originalValues);
    });

    $('.editVideoLink').click(function (e) {
        e.preventDefault();

        var videoLink = $(this).parents().siblings('.videoLink').eq(0);

        var videoLinkNumber = videoLink.data('videoLink');

        hideFlashApps();

        var modalButtons =
        {
            "Cancel": function () {
                var dialogBox = $(this);

                var originalValues = dialogBox.data('originalValues');

                var textInputs = dialogBox.find('input[type^=text]');

                for (var i = 0; i < originalValues.length; i++) {
                    textInputs.eq(i).val(originalValues[i]);
                }

                showFlashApps();

                dialogBox.dialog("close");
            },

            "Ok": function () {
                var dialogBox = $(this);

                var originalValues = dialogBox.data('originalValues');

                dialogBox.find('input[type^=text]').each(function (i) {
                    originalValues[i] = $(this).val();
                });

                dialogBox.data('originalValues', originalValues);

                var videoLink = $('.videoLink').eq(videoLinkNumber);

                videoLink.find('a[id$=Preview]')
                         .attr('href', originalValues[0])
                         .attr('vidwidth', originalValues[2])
                         .attr('vidheight', originalValues[3]);

                if (originalValues[1].length > 0)
                    videoLink.find('img[id$=Preview]').attr('src', originalValues[1]).show();
                else
                    videoLink.find('img[id$=Preview]').attr('src', '').hide();

                videoLink.show();

                showFlashApps();

                $(this).dialog("close");
            }
        };

        var dialogOptions =
        {
            width: 450,

            buttons: modalButtons,

            draggable: false,

            modal: true,

            resizable: false,

            open: onModalOpen,

            beforeclose: onModalClose
        };


        $('.videoDialog').each(function () {
            if ($(this).data('videoLink') == videoLinkNumber)
                $(this).dialog(dialogOptions);
        });
    });

    $('.lnkClear').click(function (e) {
        e.preventDefault();

        var videoLink = $(this).parents().siblings('.videoLink').eq(0);

        var videoLinkNumber = videoLink.data('videoLink');

        var originalValues;

        $('.videoDialog').each(function () {
            if ($(this).data('videoLink') == videoLinkNumber) {
                var dialog = $(this);

                originalValues = new Array(dialog.data('originalValues').length);

                dialog.data('originalValues', originalValues);

                dialog.find('input[type^=text]').each(function () {
                    $(this).val("");
                });
            }
        });

        videoLink.find('a[id$=Preview]').attr('href', '');

        videoLink.find('img[id$=Preview]').attr('src', '');

    });


});
// Video Link *********************************************************************************


// Chiclet *********************************************************************************
//window.resizeImages = true;

//jQuery(document).ready(function () {
//    $('.chiclet').each(function (i) {
//        var chiclet = $(this);

//        var originalValues = {};

//        chiclet.data('chiclet', i);

//        var dialog = chiclet.siblings('.chicletDialog').data('chiclet', i);

//        dialog.find("*:input:not(:button)").each(function () {
//            var input = $(this);

//            originalValues[this.id] = input.is(":checkbox") ? input.is(":checked") : input.val();
//        });

//        dialog.data('originalValues', originalValues);
//    });

//    if ($('.chicletImage').length > 0) {
//        $('.chicletImage').css('visibility', 'visible');
//    }

//    $('.chicletEditLink').click(function (e) {
//        e.preventDefault();

//        var chiclet = $(this).parents().siblings('.chiclet').eq(0);

//        var chicletNumber = chiclet.data('chiclet');

//        hideFlashApps();

//        var modalButtons =
//        {
//            "Cancel": function () {
//                var dialogBox = $(this);

//                var originalValues = dialogBox.data('originalValues');

//                dialogBox.find("*:input:not(:button)").each(function () {
//                    var input = $(this);

//                    if (input.is(":checkbox")) {
//                        if (originalValues[this.id])
//                            input.attr('checked', 'checked');
//                        else
//                            input.removeAttr('checked');
//                    }
//                    else
//                        input.val(originalValues[this.id]);
//                });

//                showFlashApps();

//                dialogBox.dialog("close");
//            },

//            "Ok": function () {
//                var dialogBox = $(this);

//                var originalValues = dialogBox.data('originalValues');

//                var chiclet = $('.chiclet').eq(chicletNumber);

//                dialogBox.find("*:input:not(:button)").each(function (i) {
//                    var input = $(this),
//                        id = this.id;

//                    originalValues[this.id] = input.is(":checkbox") ? input.is(":checked") : input.val();

//                    (function () {
//                        switch (i) {
//                            case 0:
//                                if (originalValues[id].length > 0)
//                                    chiclet.find('img[id$=chicletImagePreview]').attr('src', originalValues[id]).show();
//                                else
//                                    chiclet.find('img[id$=chicletImagePreview]').attr('src', '').hide();
//                                break;

//                            case 1:
//                                chiclet.find('a[id$=chicletPageUrl]').attr('href', originalValues[id]);
//                                break;

//                            case 2:
//                                if (originalValues[id]) {
//                                    chiclet.find('a[id$=chicletPageUrl]').attr('rel', 'video');
//                                }
//                                else {
//                                    chiclet.find('a[id$=chicletPageUrl]').removeAttr('rel');
//                                }
//                                break;

//                            case 3:
//                                chiclet.find('a[id$=chicletPageUrl]').attr('vidwidth', originalValues[id]);
//                                break;

//                            case 4:
//                                chiclet.find('a[id$=chicletPageUrl]').attr('vidheight', originalValues[id]);
//                                break;

//                            case 5:
//                                if (originalValues[id])
//                                    chiclet.find('a[id$=chicletPageUrl]').attr('target', '_blank');
//                                else
//                                    chiclet.find('a[id$=chicletPageUrl]').removeAttr('target');
//                                break;

//                            case 6:
//                                chiclet.find('span[id$=lblChicletTitlePreview]').html(originalValues[id]);
//                                break;

//                            case 7:
//                                chiclet.find('span[id$=lblChicletSnippetPreview]').html(originalValues[id]);
//                                break;
//                        }

//                    })();
//                });


//                dialogBox.data('originalValues', originalValues);

//                chiclet.show();

//                showFlashApps();

//                $(this).dialog("close");
//            }
//        };

//        var dialogOptions =
//        {
//            width: 530,

//            buttons: modalButtons,

//            draggable: false,

//            modal: true,

//            resizable: false,

//            open: onModalOpen,

//            beforeclose: onModalClose
//        };


//        $('.chicletDialog').each(function () {
//            if ($(this).data('chiclet') == chicletNumber)
//                $(this).dialog(dialogOptions);
//        });
//    });

//    $('.chicletClear').click(function (e) {
//        e.preventDefault();

//        var chiclet = $(this).parents().siblings('.chiclet').eq(0);

//        var chicletNumber = chiclet.data('chiclet');

//        var originalValues;

//        $('.chicletDialog').each(function () {
//            if ($(this).data('chiclet') == chicletNumber) {
//                var dialog = $(this);

//                originalValues = new Array(dialog.data('originalValues').length);

//                dialog.data('originalValues', originalValues);

//                dialog.find('input[type^=text]').each(function () {
//                    $(this).val("");
//                });

//                dialog.find('textarea').each(function () {
//                    $(this).val("");
//                });
//            }
//        });

//        chiclet.find('img[id$=Preview]').attr('src', '').hide();

//        chiclet.find('span[id$=Preview]').html("");
//    });

//    $('.chicletText').find('span').each(function () {
//        if ($(this).text().search(/.*<.*>.*/) != -1 || $(this).text().search(/.*\&.*\;.*/) != -1) {
//            $(this).html($(this).text());
//        }
//    });

//    $('.chicletImage').each(function () {
//        var anchor = $(this).find('a');

//        if (!anchor.attr('href') || anchor.attr('href') == '#') {
//            anchor.css('cursor', 'default');

//            anchor.click(function (e) { e.preventDefault(); });
//        }
//    });

//    $('.chiclet').show();

//});
// Chiclet *********************************************************************************


// Social Media Share *********************************************************************************
//This should no longer be necessary for Sitecore implementation.
//jQuery(document).ready(function () {

//    var FB_SHARE = "http://www.facebook.com/sharer.php?u=";

//    var TW_SHARE = "http://www.twitter.com/home?status=";

//    $('*[id$=chkViewSocialMediaShare]').click(function () {
//        var checkBox = $(this);

//        var preview = checkBox.parents('.ms-toolbarContainer').eq(0).siblings('.pnlSocialMediaShareLinks').eq(0);

//        var fbImage = preview.find('img[id$=imgShareFacebook]');

//        var twImage = preview.find('img[id$=imgShareTwitter]');

//        if (checkBox.attr('checked')) {
//            DisableImage(fbImage);

//            DisableImage(twImage);

//            DisableLink(fbImage);

//            DisableLink(twImage);
//        }
//        else {
//            EnableLink(FB_SHARE, fbImage);

//            EnableLink(TW_SHARE, twImage);

//            EnableImage(preview.find('img[id$=imgShareFacebook]'));

//            EnableImage(preview.find('img[id$=imgShareTwitter]'));
//        }

//    });

//    $('.chkViewSocialMediaShareLink').click(function (e) {
//        e.preventDefault();

//        var link = $(this);

//        var checkBox = $(this).siblings('*[id$=chkViewSocialMediaShare]');

//        var preview = checkBox.parents('.ms-toolbarContainer').eq(0).siblings('.pnlSocialMediaShareLinks').eq(0);

//        var fbImage = preview.find('img[id$=imgShareFacebook]');

//        var twImage = preview.find('img[id$=imgShareTwitter]');

//        if (checkBox.attr('checked')) {
//            checkBox.attr('checked', false);

//            EnableLink(FB_SHARE, fbImage);

//            EnableLink(TW_SHARE, twImage);

//            EnableImage(preview.find('img[id$=imgShareFacebook]'));

//            EnableImage(preview.find('img[id$=imgShareTwitter]'));
//        }
//        else {
//            checkBox.attr('checked', true);

//            DisableImage(fbImage);

//            DisableImage(twImage);

//            DisableLink(fbImage);

//            DisableLink(twImage);
//        }

//    });

//    $('div.share').find('a').each(function () {
//        var anchor = $(this),

//            hrefParts = anchor.attr('href').split('=');

//        hrefParts[1] = "http://" + window.location.host + hrefParts[1];

//        anchor.attr('href', hrefParts.join('='));
//    });
//});


function EnableLink(shareUrl, image) {
    var pageUrl = String(window.location);

    if (pageUrl[pageUrl.length - 1] == "/")
        pageUrl = pageUrl.substring(0, pageUrl.lenght - 2);


    image.wrap('<a target="_blank" href="' + shareUrl + pageUrl + '" ></a>');
}

function DisableLink(image) {
    var link = image.parent();

    link.before(link.find('img').clone());

    link.remove();
}

function EnableImage(image) {
    var baseUrl = image.attr('src').split('/');

    var lastIndex = baseUrl.length - 1;

    var imageName = baseUrl[lastIndex].split('.');

    var newUrl = "";

    imageName[0] = imageName[0].replace(/Disabled/, "");

    baseUrl[lastIndex] = "";

    newUrl += baseUrl.join('/');

    newUrl += imageName.join('.');

    image.attr('src', newUrl);
}

function DisableImage(image) {
    var baseUrl = image.attr('src').split('/');

    var lastIndex = baseUrl.length - 1;

    var imageName = baseUrl[lastIndex].split('.');

    var newUrl = "";

    imageName[0] = imageName[0] + "Disabled";

    baseUrl[lastIndex] = "";

    newUrl += baseUrl.join('/');

    newUrl += imageName.join('.');

    image.attr('src', newUrl);
}
// Social Media Share *********************************************************************************


// Modal Window *********************************************************************************
function onModalOpen() {
    $('.ui-dialog').css('padding', '0').find('.ui-dialog-titlebar').hide();
}

function onModalClose(e, ui) {
    $(this).dialog('destroy');

    $('form').eq(0).append($(this));
}

function hideFlashApps() {
    $('.flashApp').hide();
}

function showFlashApps() {
    $('.flashApp').show();
}

function alertVideo(e) {
    e.preventDefault();

    callModalVideo($(this).attr('href'), 540, 360);
}

// Modal Window *********************************************************************************