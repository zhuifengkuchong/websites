var int;

var _menus;
var _active // holds an ID while it's animating in.

$(function () {
    _menus = new Array();
    $("#main-nav ul a").each(function () {
        if ($(this).attr("rel") != undefined) {
            var s = $("#" + $(this).attr("rel")+".sub-menu");

            _menus.push({ root: $(this), sub: s });
            // add event listeners
            var p = $(this).offset();
            $(s).css("top", p.top - 35);
            
            $(this).mouseenter(function () {              
                clearInterval(int);
                showMenu(s);
            });

            $(this).mouseleave(function () {
                tryHide(s);
            });
            

        }
    });
});

/**
* shows the menu on rollover
*
* @param obj the sub-menu to show
* @param y the y position of the parent menu
*/
function showMenu(obj) {
    //output("SHOW " + $(obj).attr("id"));
    _active = $(obj).attr("id");
    hideMenu($(obj).attr("id"));
    
    clearInterval(int);

    $(obj).css("display", "block");
    $(obj).css("opacity", "0");
    $(obj).stop();
    $(obj).animate({
        opacity: 1
    }, 200, function () {
        _active = null;
    });
}

/**
* hides the open menu on rollover of another menu
*
*/
function hideMenu(id) {
    //output("KEEP "+ id);
    clearInterval(int);

    for (i in _menus) {
        
        if ($(_menus[i].sub).attr("id") != id) {
            var s = $(_menus[i].sub);
      
            animateOut(s);
        }
    }
}


function animateOut(obj) {
     
    //$(obj).css('filter', 'alpha(opacity=40)');
    //output("start hiding " + $(obj).attr("id"));
    $(obj).stop();
    $(obj).animate({
        opacity: 0
    }, 200, function () {
        //output("hiding complete " + $(obj).attr("id"));
        $(obj).css("display", "none");
    });    
}
/**
* hides all the menus
*
*/
function hideAllMenus() {
    //output("HIDE ALL " );
    clearInterval(int);

    for (i in _menus) {
       // output($(_menus[i].sub).attr("id"));
       
        $(_menus[i].sub).animate({
            opacity: 0
        }, 200, function () {
            $(_menus[i].sub).css("display", "none");
        });
       
        
    }
}



/**
* shows the menu on rollover
*
*/
function tryHide(obj) {
    clearInterval(int);
    //output("TRY HIDE " + $(obj).attr("id"));
    int = setInterval("hideAllMenus()", 500);
    for (i in _menus) {
        if (_menus[i].sub == obj) {

            $(_menus[i].sub).mouseenter(function () {
                output("sub over");
                stopHide(_menus[i].sub);
            })
           
            break;
        }
    }
    //$(this).children(".sub-menu").css("display", "none");
}

/**
* stops the hide function if you rollover the sub-menu.
*
* @param sub the .ub-menu object to clear
*/
function stopHide(sub) {
    //alert(sub);
    clearInterval(int);
    $(sub).unbind('mouseout');
    $(sub).unbind('mouseover');
    $(sub).mouseleave(function () {
        $(sub).unbind('mouseleave');
        tryHide(sub); 
    })
}


function output(s) {
    //$("#main").append(s +" + ");
}