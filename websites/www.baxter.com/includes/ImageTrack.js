/**
 * Created by clarkj on 6/3/14.
 * Baxter inage link tracking
 */
(function (_window) {
    Init = function (t, p) {

        // add in a selector for image clicks

        t.addSelector("A", {
            filter: function (dcs, o) {
                var e = o.element || {};

                var fileName = e.href || e.src;
                if (!fileName) return true;
                // make sure we have the root of the link - remove all the parameters
                fileName = fileName.split('?')[0];

                var fileExt = fileName.split('.')[fileName.split('.').length - 1];
                // extensions we want to track for go here
                var videoExt = ['JPG', 'GIF', 'PNG' ];

                // if we don't find a extension then drop out
                if (fileName.split('.').length < 2) return true;

                for (ext in videoExt) {
                    if (~fileExt.toUpperCase().indexOf(videoExt[ext])) return false;
                }
                return true;
            },
            transform: function (dcs, o) {
                var e = o.element || {};
                // preserve the pre-call data
                o.perserveArgsa = [];
                for (var a in o.argsa) {
                    o.perserveArgsa.push(o.argsa[a])
                }
                var desc = '';
                // IE7/8
                if (typeof e.childNodes[0] != 'undefined' && typeof e.childNodes[0].alt != 'undefined')
                    desc = e.childNodes[0].alt;
                // everybody else
                if (typeof e.childNodes[1] != 'undefined' && typeof e.childNodes[1].alt != 'undefined')
                    desc = e.childNodes[1].alt;

                var fileName = e.href.split('/').pop();

                // queue up all the parameters
                o.argsa.push('WT.z_imageURL', e.href);
                o.argsa.push('WT.z_imageText', desc);
                o.argsa.push('WT.z_fileName', fileName);
                o.argsa.push("http://www.baxter.com/includes/WT.dl", 55);
            },
            finish: function (dcs, o) {
                // restore the pre-call data
                while (o.argsa.length > 0) o.argsa.pop();
                while (o.perserveArgsa.length > 0) o.argsa.push(o.perserveArgsa.slice());
            }
        });
    };
    // END Init


    // Register the plugin with the Webtrends code
    Webtrends.registerPlugin('imageTracking', Init);
})(window);
