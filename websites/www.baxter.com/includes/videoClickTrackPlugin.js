/**
 * Created by clarkj on 11/6/13.
 *  11/19/2013 - changed filter function - simplified if statement for compatibility
 *
 * This is a simple Video tracking plug-in aligned to Baxter's site architecture
 *
 * Usage
 * plugins{videoTracking:{src="Unknown_83_filename"/*tpa=http://www.baxter.com/js/videoClickTrackPlugin.js*/}};
 *
 * WT.clip_n = the name of the clip parsed from the file name
 * WT.clip_ct = clip type or the extension of the file
 * WT.clip_t = media type x-player2 or Window Media Player depending on links or embeds
 * WT.dl = 40 this defines the hit as a video event
 * WT.clip = 'click' clip event was a click event
 *
 */

(function (WT) {
    //
    // video tracking selector
    addVideoSelector = function (dcs, options) {
        dcs.addSelector("*", {
            actionElems: {'A': 1, 'EMBED': 1},
            filter: function (dcs, o) {
                var e = o.element || {};
                // look to see if we have a video - filter out all hits that don't have a wmv file
                var fileName = e.href || e.src;
                if (!fileName) return true;
                var fileExt = fileName.split('.')[fileName.split('.').length - 1];
                // video extensions we want to track for go here
                videoExt = ['WMV','AVI','MP3','MP4','FLA' ];
                for ( ext in videoExt){
                    if (~fileExt.toUpperCase().indexOf(videoExt[ext])) return false;
                }
                return true;
            },
            transform: function (dcs, o) {
                var e = o.element || {};
                // preserve the pre-call data
                var perserveArgsa = [];
                for (var a in o.argsa) {
                    perserveArgsa.push(o.argsa[a])
                }
                // find the video name
                var videoUrl = e.href || e.src;
                // parse out the name
                var videoName = 'unknown';
                var videoExt = 'unknown';
                try {
                    videoEle = videoUrl.split('/');
                    videoName = videoEle[videoEle.length - 1].split('.')[0];
                    videoExt = videoEle[videoEle.length - 1].split('.')[1];
                } catch (e) {
                }
                // determine the type of the video
                var videoType = 'unknown';
                if (e.getAttribute('type')) {
                    videoType = e.getAttribute('type').split('/')[1];
                } else if (e.mimeType) {
                    videoType = e.mimeType;
                }

                // queue up all the parameters
                o.argsa.push('WT.clip_n', videoName);
                o.argsa.push('WT.clip_ev', 'click');
                o.argsa.push('WT.clip_ct', videoExt);
                o.argsa.push('WT.clip_t', videoType);
                // WT.dl of 40 defines a video event
                o.argsa.push("http://www.baxter.com/includes/WT.dl", 40);
            },
            finish: function (dcs, o) {
                // restore the pre-call data
                while (o.argsa.length > 0) o.argsa.pop();
                while (perserveArgsa.length > 0) o.argsa.push(perserveArgsa.slice());
            }

        });
    };
    Webtrends.registerPlugin("videoTracking", function (dsc, options) {
        addVideoSelector(dsc, options)
    });
})();