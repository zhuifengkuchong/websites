if (($("#teasers").length) && (!$("#flash_wrapper").length))
{
	if ($("#manual_start").length)
	{
		//<![CDATA[
		/*onload event per page*/
		$(function(){BAXTER.teaser.init({"autostart":false,"speed":5000,"label":""});});
		//]]>
	}
	else
	{
		//<![CDATA[
		/*onload event per page*/
		$(function(){BAXTER.teaser.init();});
		//]]>
	}
}

if (typeof(flash_array) != "undefined")
	{
		var flashvars={},params={},attributes={"wmode":"opaque"};

		for (var i=0, len=flash_array.length; i<len; ++i)
		{
			swfobject.embedSWF(flash_array[i].Url, flash_array[i].Name, flash_array[i].Width, flash_array[i].Height, "10",flashvars,params,attributes);
		}
	}



var links = document.getElementsByTagName("A");

function getCookie(c_name)
{
	if (document.cookie.length>0)
  	{
  		c_start=document.cookie.indexOf(c_name + "=");
  		if (c_start!=-1)
    		{
    			c_start=c_start + c_name.length+1;
    			c_end=document.cookie.indexOf(";",c_start);
    			if (c_end==-1) c_end=document.cookie.length;
    			return unescape(document.cookie.substring(c_start,c_end));
    		}
  	}
	return "";
}

function loopThroughLinks()
	{
   	//Loop thru all links

		var links = document.getElementsByTagName('A');
		for (i=0; i<links.length; i++) 
      	{   
			// FF fix
			if ((links[i].getAttribute("href") != null) && ((links[i].getAttribute("href").indexOf("http://") == 0) || (links[i].getAttribute("href").indexOf("https://") == 0)))
 				{checklink(links[i]);}
		}
	}

function chglink(objlnk)
   {
	objlnk.target = "";
	objlnk.href = "http://www.baxter.com/information/linking/external_link_disclaimer.html?u=" + objlnk.getAttribute("href");
   }

function checklink(objlnk)
	{


		if ((objlnk.getAttribute("href").indexOf("http://www.baxter.com/includes/script/WT.excl") == -1) && (objlnk.getAttribute("href").indexOf("http://www.baxter.com/includes/script/WT.svl") == -1) && (objlnk.getAttribute("href").indexOf("mailto:") == -1) && (objlnk.getAttribute("href").indexOf(".pdf") == -1) && (objlnk.getAttribute("href").indexOf("http://video.baxter/") == -1) && (objlnk.getAttribute("href").indexOf("http://teamsite/") == -1) && (objlnk.getAttribute("href").indexOf("../../index.htm"/*tpa=http://www.baxter.com/*/) == -1) && (objlnk.getAttribute("href").indexOf("http://staging.baxter.com/") == -1) && (objlnk.getAttribute("href").indexOf("https://www.baxter.com") == -1) && (objlnk.getAttribute("href").indexOf("http://10.77/") == -1) && (objlnk.getAttribute("href").indexOf("javascript:") == -1) && (objlnk.getAttribute("href").indexOf("http://phx.corporate/") == -1) && (objlnk.getAttribute("href").indexOf("http://investor.baxter.com/") == -1) && (objlnk.getAttribute("href").indexOf("http://www.colleaguepump.com/") == -1) && (objlnk.getAttribute("href").indexOf("sustainability.baxter") == -1))
		{
		
			if (objlnk.getAttribute("rel") != "image") {objlnk.rel = "external";}
			if(getCookie("bax_com_skip_disclaimer") != "baxter") {chglink(objlnk)};
		}
	}
	







if (document.location.href.indexOf("http://www.baxter.com/includes/script/external_link_disclaimer.html") == -1)
	{
		loopThroughLinks();
	}

