/*!
 * Change the xml file (config.xml) name and path in the script as per the requirement. 
*/

$(document).ready(function()
	{

if ($(".homepage").length > 0)
{


		try {var xmlhttp2=new XMLHttpRequest();}
		catch (e) {xmlhttp2=new ActiveXObject("Microsoft.XMLHTTP");}

		xmlhttp2.open("GET","http://www.baxter.com/includes/script/config.xml",false); //Please change the location of the XML files as per your web server config
		xmlhttp2.send(null);

		var browser = navigator.appName;

		if (browser=="Microsoft Internet Explorer") 
			{
    				xmlDoc = new ActiveXObject('Microsoft.XMLDOM');
    				xmlDoc.async = false;
    				xmlDoc.loadXML(xmlhttp2.responseText);
			} 
			else 
			{
    				xmlDoc = (new DOMParser()).parseFromString(xmlhttp2.responseText, 'text/xml');
			}

		$(xmlDoc).find("config").each(function()
			{ 
				fadeInTime=$(this).find('fadeInTime').text();
				transTime=$(this).find('trTime').text();
				var imageArray = new Array();
				var imgcontentArray=new Array();
				var fromArray=new Array();
				var toArray=new Array();
				var transArr=new Array();
				var imgcontenttitle=new Array();
				var imgcontentlink=new Array();
				var imgcontentbtn=new Array();
				var imgcontentsubtitle1=new Array();
				var imgcontentsubtitle2=new Array();
				var images = xmlDoc.getElementsByTagName("image");
				var transArr=xmlDoc.getElementsByTagName("transition");
				var imgcontent = xmlDoc.getElementsByTagName("text");
				var imgcontenttitle = xmlDoc.getElementsByTagName("title");
				var imgcontentlink = xmlDoc.getElementsByTagName("link");
				var imgcontentbtn = xmlDoc.getElementsByTagName("btnlabel");
				var imgcontentsubtitle1 = xmlDoc.getElementsByTagName("subtitle1");
				var imgcontentsubtitle2 = xmlDoc.getElementsByTagName("subtitle2");
	
				for ( var j = 0; j < transArr.length; ++j )
					{
						if(transArr[j].childNodes[0].nodeValue.toString()=="ANIM_BOTTOM")
							{
								fromArray[j]="top left";
								toArray[j]="bottom left";
							}
							else if(transArr[j].childNodes[0].nodeValue.toString()=="ANIM_ZOOMOUT")
								{
									fromArray[j]="bottom left";
									toArray[j]="http://www.baxter.com/includes/script/top left 1.3x";
								}
							else if(transArr[j].childNodes[0].nodeValue.toString()=="ANIM_LEFT")
								{
									fromArray[j]="top left";
									toArray[j]="top right";
								}
							else if(transArr[j].childNodes[0].nodeValue.toString()=="ANIM_TOP")
								{
									fromArray[j]="bottom left";
									toArray[j]="top left";
								}
							else if(transArr[j].childNodes[0].nodeValue.toString()=="ANIM_ZOOMIN")
								{
									fromArray[j]="top left";
									toArray[j]="http://www.baxter.com/includes/script/bottom left 1.3x";
								}
					}
		
				var slidesArr=new Array();
	
				for ( var i = 0; i < images.length; ++i )
					{
						imageArray[i] = images[i].childNodes[0].nodeValue.toString();
						imgcontentArray[i] = "<div class='bannercontenthead'>"+ imgcontenttitle[i].childNodes[0].nodeValue.toString()+"</div>"+"<div class='bannertext'>"+"<div class='bannersubtitle1'>"+imgcontentsubtitle1[i].childNodes[0].nodeValue.toString()+"</div>"+"<div class='bannersubtitle2'>"+imgcontentsubtitle2[i].childNodes[0].nodeValue.toString()+"</div>"+ "<div>"+imgcontent[i].childNodes[0].nodeValue.toString()+"</div>"+"<div class='bannerlink'>"+imgcontentlink[i].childNodes[0].nodeValue.toString()+"</div>"+"<div class='buttonplaceholder'><span class='bannerbtn'><span>"+imgcontentbtn[i].childNodes[0].nodeValue.toString()+"</span></span></div>"+"</div>";
						slidesArr[i]=new Object();	
						slidesArr[i].src=imageArray[i];
						slidesArr[i].alt=imgcontentArray[i];
						slidesArr[i].from=fromArray[i];
						slidesArr[i].to=toArray[i];
						slidesArr[i].time=transTime;
					}
	
	
				$('#slideshow').crossSlide({fade: fadeInTime}, slidesArr, function(idx, img, idxOut, imgOut)
					{
  						if (idxOut == undefined)
  							{
    								$('div.bannercontent').html(img.alt)
  							}  
					});


			});


$(".bannercontentholder").css({'display':'block'});
 }
     		else
     			{
          			$(".bannercontentholder").css({'display':'none'});
     			}


	});


