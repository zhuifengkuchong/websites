/**
 * BAXTER JS
 *
 * @author  	MGA/DMA
 * @date		2012-04-24
 * @version 	1.0
 * @requires 	jQuery 1.3.2/shadowbox 3.0b
 * @valid   	true www.jslint.com
 */


Shadowbox.init({skipSetup:true,players:["flv","html","iframe"]});

BAXTER=(typeof(BAXTER)==="undefined")?{}:BAXTER;
BAXTER.initNav=function(){
	/*
	@author: MGA
	highlights active nav elements as defined in the page
	see	BAXTER.activeNav={
			top:{pageId:"#job_seeker"},
			left:["#L1","#L1-1","#L1-1-4","#L1-1-1-3"]
		}
	*/
	if(typeof(BAXTER.activeNav)==="undefined"){return;}
	if(typeof(BAXTER.activeNav.top)!="undefined"){
		$(BAXTER.activeNav.top.pageId).addClass("current");
	}
	if(typeof(BAXTER.activeNav.left)!="undefined"){
		if (typeof(BAXTER.activeNav.left)==="object"){
			$(BAXTER.activeNav.left).each(function(){
				var that=this.toString();
				if (this.toString() != "#")
				{
				var that=this.toString();
				$(that).addClass("active");
				}
			});
		}
	}
};

BAXTER.tabs=(function(){
	/*@author: MGA*/
	var cfg={container:".tabs_wrapper",tab:".tabs li",lnk:"a",active:".active",wrapper:".tabs_widget",content:".tab_content"};
	if (!$(cfg.container.length)){return {init:function(){}};}
	var activeTab={
		tab:$(cfg.container+" "+cfg.tab+cfg.active),
		content:$($($(cfg.container+" "+cfg.tab+cfg.active+" "+cfg.lnk)).attr("href")),
		index:$(cfg.container+" "+cfg.tab+cfg.active).index($(cfg.container+" "+cfg.tab))
	};
	var init=function(){
		//hide all content not active
		$(cfg.wrapper+" "+cfg.content).each(function(i){
			if (i!=activeTab.index){
				$(this).hide();
			}
		})
		$(cfg.container+" "+cfg.lnk).each(function(){
			//clicks
			$(this).bind("click",function(){
				hideOldActive();
				activateTab(this);
				//console.log(this)
				return false;
			});
		});
	};
	var hideOldActive=function(){
		activeTab.tab.removeClass(cfg.active.substring(1,cfg.active.length));
		activeTab.content.hide();
	};
	var activateTab=function(elm){
		var t=$($(elm).parents("li")[0]);
		var id=$(elm).attr("href");
		$(t).addClass("active");
		$(id).show();
		activeTab={tab:t,content:$(id)};
	};
	return {init:init};
})();
BAXTER.product=(function(){
	/*@author: MGA*/
	var cfg={cName:".nav_internal",prev:".previous",next:".next"},previous="#",next="#";
	if(!BAXTER.productNav || !BAXTER.productNav.length){return {navInit:function(){}};}
	var getLinks=function(){
		var navItems=BAXTER.productNav.length;
		if (navItems===1){
			next=BAXTER.productNav[0];
			previous=next;
		} else {
			for (var i=0,j=navItems;i<j;i++){
				if (document.location.href.indexOf(BAXTER.productNav[i])!=-1){
					if (i===0){
						next=BAXTER.productNav[1];
						previous=BAXTER.productNav[navItems-1];
					} else {
						i=i*1;
						next=(i!=BAXTER.productNav.length-1)?BAXTER.productNav[i+1]:BAXTER.productNav[0];
						previous=(i!=BAXTER.productNav.length-1)?BAXTER.productNav[i-1]:BAXTER.productNav[i-1];
					}
					break;
				}
			}
		}
	};
	var getHref=function(url){
		return "http://"+document.domain+url;
	};
	var navInit=function(){
		getLinks();
		$(cfg.cName+" "+cfg.prev).each(function(){
			$(this).attr("href",BAXTER.product.getHref(previous));
		});
		$(cfg.cName+" "+cfg.next).each(function(){
			$(this).attr("href",BAXTER.product.getHref(next));
		});
	};
	return {
		getHref:getHref,
		navInit:navInit
	};
})();
BAXTER.initExternals=function(){
	$("a[rel='external']").each(function(){
		var disclaimer=(!$(this).attr("title"))?"":" title=\""+$(this).attr("title")+"\"";
		$(this).wrap("<span></span>").parent("span").append('<i class="external_link"'+disclaimer+'>&nbsp;</i>');
	});
};
BAXTER.fontSize=function(){
	// @author: DMA - remixed MGA
	$(".zoomin").click(function(){
		setFontSize(getFontSize(),1);
		return false;
	});

	$(".zoomout").click(function(){
		setFontSize(getFontSize(),-1);
		return false;
	});

	function getFontSize() {
		var currentSize = $("body").css("font-size");
		var currentSizeNumber = parseInt(currentSize,10);
		currentSizeNumber=(currentSizeNumber > 20)?20:currentSizeNumber;
		return currentSizeNumber;
	}

	function setFontSize(size,dir) {
		$("body").css("font-size", size+dir);
	}
};
BAXTER.socialBookmarking=function() {
	// @author: DMA
	// show the lit of social tools
	$(".content_tools #tools_social-bookmarking").toggle(function() {
		var pos = $(this).position();
		$("body").append("<div class='box rounded' id='wrapper_bookmark'>&nbsp;</div");
		$("#wrapper_bookmark").css( { "left": (pos.left) + "px", "top":pos.top + "px" } );
		$("#wrapper_bookmark").load('http://www.baxter.com/downloads/social_bookmark.htm',"", function(){
		    $(this).css("top:35px","left:792px" );
			//populate social tools
			var title = encodeURIComponent(document.title);
			var this_url = encodeURIComponent(document.URL);
			var ga_url = document.URL;
			var ga_url_indexof = ga_url.indexOf(".com/");	

			$(".digg").attr( "href", "http://digg.com/submit?phase=2&url="+this_url+"&title="+title);
			$(".delicious").attr( "href", "http://del.icio.us/post?url="+this_url+"&title="+title);
			$(".google").attr( "href", "http://www.google.com/bookmarks/mark?op=edit&bkmk="+this_url+"&title="+title);
			$(".stumble").attr("href", "http://www.stumbleupon.com/submit?url="+this_url+"&title="+title);
			$(".myspace").attr("href", "http://www.myspace.com/Modules/PostTo/Pages/?"+title+"&u="+this_url);
			$(".facebook").attr( "href", "http://www.facebook.com/sharer.php?u="+this_url+"&t="+title);
			$(".live").attr( "href", "https://favorites.live.com/quickadd.aspx?url="+this_url+"&title="+title);

			if (ga_url_indexof != -1)
			{
				ga_url = ga_url.substr(ga_url_indexof+4);	
				$(".digg").attr( "onClick", "_gaq.push(['_trackPageview','/social_bookmarking/digg" + ga_url + "']);");
				$(".delicious").attr( "onClick", "_gaq.push(['_trackPageview','/social_bookmarking/delicious" + ga_url + "']);");
				$(".google").attr( "onClick", "_gaq.push(['_trackPageview','/social_bookmarking/google" + ga_url + "']);");
				$(".stumble").attr( "onClick", "_gaq.push(['_trackPageview','/social_bookmarking/stumble" + ga_url + "']);");
				$(".myspace").attr( "onClick", "_gaq.push(['_trackPageview','/social_bookmarking/myspace" + ga_url + "']);");
				$(".facebook").attr( "onClick", "_gaq.push(['_trackPageview','/social_bookmarking/facebook" + ga_url + "']);");
				$(".live").attr( "onClick", "_gaq.push(['_trackPageview','/social_bookmarking/live" + ga_url + "']);");
			}

			$("#wrapper_bookmark").bind("click",function(){$("#wrapper_bookmark").remove();return true;})

		});
		return false;
	},function() {
		$("#wrapper_bookmark").remove();
	});
};
BAXTER.sendFriend=function() {
	// @author: DMA
	$(".content_tools #send_friend").click(function() {
		// create the place to receive the form
		$("#content").append("<div id='form_send_friend'>&nbsp;</div>");
		// load the page with the form in previous create place, the path may change if you put this page elsewhere 
		$("#content #form_send_friend").load("http://www.baxter.com/downloads/send_to_friend.html","",function(){
			var url = document.URL;
			var title = document.title;
			var welcome = "You should visit the page " + title + " at " + url;
			$("#content #form_send_friend #MESS").val(welcome);
			$("#content #form_send_friend #REF").val(url);
			$("#content #form_send_friend #TITLE").val(title);
		});
		$("#close").live('click', function() {
			$("#form_send_friend").remove();
			return false;
		});
		return false;
	});

	/*$("#send_friend").bind("click",function(){
		window.location.href='mailto:?subject='+$(this).attr("title")+"&body="+window.location.href;
		return false;
	})*/
}


/*ShadowBox players*/
Shadowbox.init({skipSetup:true,players:["flv","html","iframe"]});
/*onload event for all pages*/
$(function(){
	$("#nav_lev_1 ul:not(ul ul)").superfish({
		hoverClass:'active',
		delay:0,
		speed:0,
		onInit:function () {
			$(this).find("ul").children("li").each(function() {
				
				$(this).width($(this).parent().width())
			});
		},
		onHide:function(){$("#content").css({position:"static"});$("#controls").show();}
	}).bgIframe();
	$("#nav_lev_1 a:not(ul ul a)").hover(function(){$(this).parent("li").addClass("active");},function(){$(this).parent("li").removeClass("active");});
    Shadowbox.setup("a.player", {
        gallery:            "My Movies",
        autoplayMovies:     true
    });
	BAXTER.initNav();
	BAXTER.product.navInit();
	BAXTER.tabs.init();
	BAXTER.initExternals();
	BAXTER.fontSize();
	BAXTER.socialBookmarking();
	BAXTER.sendFriend();
});

function formHandler(obj)
{
document.location=obj.options[obj.selectedIndex].value;
}
