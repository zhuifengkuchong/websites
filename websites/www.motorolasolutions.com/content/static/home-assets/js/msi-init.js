'use strict';

msiObj.init = function () {
  // initialize here
  var $body = msiObj.utils.getEl('body');
  // determing direction and store
  msiObj.bindings.rtl = ($body.hasClass('rtl'));
  // set up to track window sizing
  msiObj.utils.trackWindowSize();
  // run first size check
  msiObj.utils.handleWindowResized();
  // set up bindings for tracking
  msiObj.bindings.unflowArr = [];
  // set up transition end event
  msiObj.utils.setTransitionEnd();

  // CONTROLLERS GATHER AND FIRE
  //==============================
  // gather and run the controllers
  msiObj.utils.getCtrls();
  //reset viewport on orientation changes
  msiObj.utils.resetViewportHeight();
  //
  $(window).on('resize', msiObj.utils.throttle(80, false, function () {
    msiObj.utils.resetViewportHeight();
  }));
};

// Initialize on Doc Ready
$(function() {
  msiObj.init();
});