'use strict';

if (typeof jQuery === "undefined") {
 throw "This widget requires jquery module to be loaded";
}

(function($){
  var myObj = {
    options: {
      minDist: 300,
      scrollSpeed: 1200,
      dockClass: 'docked'
    },

    /************************
    *    PRIVATE METHODS    *
    *************************/
    _init : function(){

    },
    _create : function(){
      var that = this;
      this.elements = {
        document: $(document)
      };
      this.scrolled = 0;
      this.showing = false;
      this.dir = (msiObj.bindings.rtl) ? 'right' : 'left';
      
      // bind scroll and run initial test
      this._bindScroll();
      this._trackScrolling();

      this.element.on('click', function (e) {
        e.preventDefault();
        that._toggleBtn(false);
        $(window).unbind('.backTopScroll');
        $('html, body').finish().animate({scrollTop: 0}, that.options.scrollSpeed, function () {
          document.location.hash = "#";
          that._bindScroll();
        });
      });
    },

    _trackScrolling: function () {
      if (msiObj.bindings.windowSize == 0) { return false; }
      this.scrolled = this.elements.document.scrollTop();
      //run the toggle
      this._toggleBtn((this.scrolled > this.options.minDist));
    },
    _toggleBtn: function (val) {
      if (val != this.showing) {
        var toDo = ( val ) ? 'addClass' : 'removeClass';
        if(!Modernizr.csstransforms3d) {
          var lval = (val) ? 0 : '-40px',
            tmpObj = {};
          tmpObj[this.dir] = lval;
          this.element.animate(tmpObj, 400);
        } else {
          this.element[toDo]('docked');
        }
        this.showing = val;
      }
    },
    _bindScroll: function () {
      var that = this;
      $(window).bind("scroll.backTopScroll", msiObj.utils.throttle(40, true, function () {
        that._trackScrolling();
      }));
    }
 };

  $.widget( 'ui.backToTop', myObj );
})(jQuery);

// initialize the controller
msiObj.controllers.back_to_top = {
  init: function () {
    $( '#back_to_top' ).backToTop();
  }
}

// allow anchor links within richtexteditor class to smooth scroll and support fixed header
$(document).ready(function() {
  var $root = $('html, body');
    $('.richtexteditor a[href^="#"]').click(function(){
      var a = $.attr(this, 'href');
        if(a.length > 1 && $('[name="' + a.substr(1) + '"]') && $('[name="' + a.substr(1) + '"]').offset()) {
            $('html, body').animate({ scrollTop: $('[name="' + a.substr(1) + '"]').offset().top - 60 }, 1000);
        return false;
        }
    });
});