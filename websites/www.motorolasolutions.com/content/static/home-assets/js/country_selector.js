//Need to trigger GoToCountry() as on click for all the links section

/* GoToCountry
 * parameters:	clickEvent - event object
 * 
 * Checks that the user clicked on a link within the SelectCountry DIV, and creates a cookie
 * storing the url that was clicked on.
*/
function GoToCountry(clickEvent)
{
	if(!clickEvent)
		clickEvent = window.event;
	var target;
	if((target = clickEvent.target) || (target = clickEvent.srcElement))
	{
		if(target.tagName == "A" || target.tagName == "a" || target.tagName == "area" || target.tagName == "AREA")
		{
			if($('#remember').prop('checked'))
			{
				createCookie(target);
			}
			window.opener.parent.location = target.href;
			self.close();
		}
	}
}

/* createCookie
 * parameters:	value - String containing the href
 * 
 * Creates a cookie with the storing the passed value parameter.  The cookie expires one year
 * from now.
*/
function createCookie(value)
{
	var expires = "";
	var expireDate = new Date();
	expireDate.setMonth(expireDate.getMonth() + 12);
	var expires = "; expires=" + expireDate.toUTCString();
	 
	var cookieValue = "BusinessLocaleURL=" + value + expires + "; path=/";
	document.cookie = cookieValue;
}