'use strict';

msiObj.utils = {
  getCtrls: function (scope) {
    // default scope if undefined
    scope = (typeof scope !== 'undefined') ? scope : $('body');
    // set up array to collect ctrls
    var ctrlArr = [];
    // parse for controllers
    var scopeCtrl = scope.attr('data-ctrl');
    if (scopeCtrl) {
      ctrlArr.push(scopeCtrl);
    }
    $('[data-ctrl]', scope).each(function () {
      var ctrlVal = $(this).attr('data-ctrl');
      // check if the controller is already in the array
      if ($.inArray(ctrlVal, ctrlArr) == -1) {
        // add the controller to the array for runs
        ctrlArr.push( ctrlVal );
      }
    })
    // run the controllers
    this.runCtrls(ctrlArr);
  },
  // If there are registered controllers, run them.
  runCtrls: function (ctrlArr) {
    var i = 0,
      len = ctrlArr.length;
    for (; i<len; i++) {
      if ( msiObj.controllers[ctrlArr[i]] && msiObj.controllers[ctrlArr[i]].init ) {
        msiObj.controllers[ctrlArr[i]].init();
      }
    };
  },
  getEl: function (el, sel, cache) {
    var type = {
      'class': '.',
      'element': '',
      'id': '#',
      '#': '#',
      '.': '.'
    };
    // prep el to remove - and put in _
    var prepEl = el.split("-").join("_");
    var x = (type[sel]) ? type[sel] : '';
    // get an element
    if (msiObj.bindings[prepEl] && cache) {
      // if it exist, return it
      return msiObj.bindings[prepEl];
    } else {
      // look it up
      var tmp = $(x+el);
      // store it
      msiObj.bindings[prepEl] = tmp;
      // return it
      return msiObj.bindings[prepEl];
    }
  },
  inArr: function (val, arr, toDo) {
    // return if we don't have a value or array
    if (!val || !arr) { return false; }
    // check if it's in the array
    var index = $.inArray(val, arr),
      inArr = ( index != -1);
    if (toDo == 'addUnique' && !inArr) {
      // add if asked to do so and it's not already there
      arr.push(val);
    } else if (toDo == 'remove' && inArr) {
      // remove from array if asked and it is in the array
      arr.splice(index, 1);
    }
    return inArr;
  },
  /* Throttle and Debounce come from Ben Alman and are used for performance gains */
  /* https://raw.githubusercontent.com/cowboy/jquery-throttle-debounce/v1.1/jquery.ba-throttle-debounce.js */
  throttle: function( delay, no_trailing, callback, debounce_mode ) {
    var timeout_id,
      last_exec = 0;

    if ( typeof no_trailing !== 'boolean' ) {
      debounce_mode = callback;
      callback = no_trailing;
      no_trailing = undefined;
    }

    function wrapper() {
      var that = this,
        elapsed = +new Date() - last_exec,
        args = arguments;

      function exec() {
        last_exec = +new Date();
        callback.apply( that, args );
      };

      function clear() {
        timeout_id = undefined;
      };

      if ( debounce_mode && !timeout_id ) {
        exec();
      }

      timeout_id && clearTimeout( timeout_id );

      if ( debounce_mode === undefined && elapsed > delay ) {
        exec();

      } else if ( no_trailing !== true ) {
        timeout_id = setTimeout( debounce_mode ? clear : exec, debounce_mode === undefined ? delay - elapsed : delay );
      }
    };

    if ( msiObj.guid ) {
      wrapper.guid = callback.guid = callback.guid || msiObj.guid++;
    }

    return wrapper;
  },
  debounce: function( delay, at_begin, callback ) {
    return callback === undefined
      ? this.throttle( delay, at_begin, false )
      : this.throttle( delay, callback, at_begin !== false );
  },
  trackWindowSize: function () {
    var that = this,
      $window = $(window);
    this.calcScrollWidth();
    $window.resize(that.throttle(80, false, function () {
        that.handleWindowResized();
    }));
  },
  handleWindowResized: function () {
    var size = $(window).width(),
      //store the current value of window size to check against
      tmp = msiObj.bindings.windowSize;

    if (size <= msiObj.bindings.windowSm) {
      msiObj.bindings.windowSize = 0;
    } else if (size > msiObj.bindings.windowSm && size <= msiObj.bindings.windowMd ) {
      msiObj.bindings.windowSize = 1;
    } else {
      msiObj.bindings.windowSize = 2;
    }
    msiObj.bindings.topHeight = (msiObj.bindings.windowSize < 2) ? 80 : 120;
    // only trigger the event if we changed a size value
    if (tmp != msiObj.bindings.windowSize) {
      $(window).trigger('resizeWin', [msiObj.bindings.windowSize, tmp]);
    }
  },
  unflowViewport: function (toDo, id) {
    var $viewport = msiObj.utils.getEl('ms_viewport', '#', true);
    if (toDo) {
      // track who wants the unflow
      this.inArr(id, msiObj.bindings.unflowArr, 'addUnique');
      // if not unflowed, unflow the vieport
      if (!msiObj.bindings.viewportUnflow) {
        msiObj.bindings.scrolled = $(document).scrollTop();
        // $viewport.addClass('unflow')
        $viewport.css({
          'height': 'auto',
          'position': 'fixed',
          'left':0,
          'right':'1px',
          'bottom': 0,
          'top': -msiObj.bindings.scrolled
        });
        // repaint viewport for ios6 ipad
        setTimeout(function () {
          $viewport.css('right', 0);
        }, 10);
        msiObj.bindings.viewportUnflow = true;
      }
    } else {
      this.inArr(id, msiObj.bindings.unflowArr, 'remove');
      // flow viewport if it's currently unflowed and the unflow array is empty
      if (msiObj.bindings.viewportUnflow && msiObj.bindings.unflowArr.length === 0) {
        // add the viewport back to the flow
        // $viewport.removeClass('unflow');
        $viewport.removeAttr('style');
        $(document).scrollTop(msiObj.bindings.scrolled);
        msiObj.bindings.viewportUnflow = false;
      }
    }
  },
  calcScrollWidth: function () {
    var $inner = jQuery('<div style="width: 100%; height:200px;">test</div>'),
        $outer = jQuery('<div style="width:200px;height:150px; position: absolute; top: 0; left: 0; visibility: hidden; overflow:hidden;"></div>').append($inner),
        inner = $inner[0],
        outer = $outer[0];

    jQuery('body').append(outer);
    var width1 = inner.offsetWidth;
    $outer.css('overflow', 'scroll');
    var width2 = outer.clientWidth;
    $outer.remove();

    var scrollbarW = (width1 - width2);
    msiObj.bindings.windowSm = 600 - scrollbarW;
    msiObj.bindings.windowMd = 984 - scrollbarW;
  },
  resetViewportHeight: function () {
    var bodyEl = msiObj.utils.getEl('ms-body-content', '.', true);

    //wrote this function because the ms-body-content is a different size before/after removing
    //the class of ms-body-grow. so there was a height-check race issue and it was flubbing up in iOS6
    function addClassViewSize() {
      setTimeout( function () {
        bodyEl.addClass('ms-body-grow');
        msiObj.data.viewSizeOn = true;
      }, 10);
    }
    if (bodyEl.height() < $(window).height()) {
      addClassViewSize();
    }
    if (msiObj.data.viewSizeOn) {
      bodyEl.removeClass('ms-body-grow');
      msiObj.data.viewSizeOn = false;

      if(bodyEl.height() < $(window).height()) {
        addClassViewSize();
      }
    }
  },
  /* experimenting with animation options */
  animate: function (el, prop, val, dur, ease, complete) {
    el.css('transition', prop+' '+dur+' '+ease);
    setTimeout( function () { el.css(prop, val); }, 0);
    setTimeout( function () {
      el.css('transition', 'none');
    }, dur);
  },
  setTransitionEnd: function () {
    var transEndEventNames = {
      'WebkitTransition' : 'webkitTransitionEnd',// Saf 6, Android Browser
      'MozTransition'    : 'transitionend',      // only for FF < 15
      'transition'       : 'transitionend'       // IE10, Opera, Chrome, FF 15+, Saf 7+
    };
    msiObj.events.transEndEventName = transEndEventNames[ Modernizr.prefixed('transition') ];
  }
};