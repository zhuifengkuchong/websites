'use strict';

var msiObj = {
  events: {},
  controllers: {},
  bindings: {},
  data: {},
  container: {
  	registry: {},
  	register: function (obj, handlers) {
  		for(var handler in handlers) {
  			if (this.registry[handler]) {
  				this.registry[handler].push({obj: obj, handler: handlers[handler]});
  			} else {
  				var tmp = handlers[handler];
  				this.registry[handler] = [{ obj: obj, handler: tmp }];
  			}
  		}
  	},
  	trigger: function (evt, args) {
      args = ( typeof(args) === 'undefined') ? [] : args;
  		var regevt = this.registry[evt];
  		if (regevt) {
	  		var i = 0,
	  			len = regevt.length,
          func;
	  		for (; i<len; i++) {
          func = regevt[i].handler;
          func.apply(regevt[i].obj, args)
	  		}
	  	}
  	}
  }
};

function rememberLocationStep1() {
	 if($('#remember').is(':checked')) {
		  $("#remember").attr('checked', false);
		  $(".remember-checkbox").fadeOut(250);
		  $(".remember-disclaimer").delay(250).fadeIn(250);
	  }
}
function rememberLocationStep2(a) {
	  if (a == 'n') {
		  $(".remember-disclaimer").fadeOut(250);
		  $(".remember-checkbox").delay(250).fadeIn(250);
		  $(".remember-checkbox").html('<input id="remember" type="checkbox" value="remember" name="remember" onClick="rememberLocationStep1();"><label for="remember"><span>Remember my selection</span></label>');
	  } else {
		  $(".remember-checkbox").html('<input id="remember" type="checkbox" value="remember" name="remember" onClick="rememberLocationStep1();" class="ms-checkbox checked" checked><label for="remember"><span>Remember my selection</span></label>');
		  $(".remember-disclaimer").fadeOut(250);
		  $(".remember-checkbox").delay(250).fadeIn(250);
		  
	  }
  }