'use strict';

if (typeof jQuery === "undefined") {
 throw "This widget requires jquery module to be loaded";
}

(function($){
  var myObj = {
    options: {
      topOffset: 120
    },

    /************************
    *    PRIVATE METHODS    *
    *************************/
    _init : function(){

    },
    _create : function(){
      var that = this;
      this.elements = {
        $input: this.element.find('input'),
        $label: this.element.find('label')
      };
      //init run
      that.toggleCheck();
      // bind event
      this.elements.$input.on('change', function () {
        that.toggleCheck();
      });
    },
    toggleCheck: function () {
      if (this.elements.$input.prop("checked")) {
        this.element.addClass('checked');
      } else {
        this.element.removeClass('checked');
      }
    }
 };

  $.widget( 'ui.checkbox', myObj );
})(jQuery);

// initialize the controller
msiObj.controllers.checkbox = {
	init: function () {
    // initi if ie8
		if ($('html').hasClass('lt-ie9')) {
			$( '.ms-checkbox' ).checkbox();
		}
	}
}