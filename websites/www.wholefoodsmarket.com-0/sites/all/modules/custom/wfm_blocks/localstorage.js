/**
 * Self-invoking function.
 */
(function wfm_localstoreinfo($){
  "use strict";
  var _gaq = _gaq || [];
  var storeLocalStorage = {

    /**
     * Make AJAX request to server for store data. Handle response in callbacks.
     */
    getJsonData: function() {
      var url = '/ajax/stores';
      $.getJSON(url)
        .done(function doneCallback(data) {
          if (storeLocalStorage.testLocalStorage()) {
            storeLocalStorage.setStoreData(data);
          }
          storeLocalStorage.storeDataSettingsObject(data);
        })
        .fail(function failCallback( jqxhr, textStatus, error ) {
          var err = textStatus + ", " + error;
          console.log('Request failed: ' . err);
          _gaq.push(['_trackEvent', 'clientError', 'localstoreage.js.getJsonData', err]);
      });
    },

    /**
     * Returns timestamp in seconds.
     */
    getTimeStamp: function() {
      return Math.round(+new Date()/1000);
    },

    /**
     * Start here.
     */
    init: function() {
      var data = '',
          message = 'Local storage is not present';

      if (storeLocalStorage.testLocalStorage()) {
        if (storeLocalStorage.needsUpdate()) {
          storeLocalStorage.getJsonData();
        }
        else {
          data = JSON.parse(localStorage.getItem('wfm_store_data'));
          storeLocalStorage.storeDataSettingsObject(data);
        }
      }
      else {
        storeLocalStorage.getJsonData();
        _gaq.push(['_trackEvent', 'clientError', 'http://www.wholefoodsmarket.com/sites//all//modules//custom//wfm_blocks//localstoreage.js.init', message]);
      }
    },

    /**
     * Returns true if timestamp in local storage is less than the timestamp
     * in Drupal settings object. This indicates that data in the server is
     * newer than that in local storage and refresh is needed.
     */
    needsUpdate: function() {
      var localtime = localStorage.getItem('wfm_store_last_update'),
          settingstime = Drupal.settings.WholeFoods.lastStoreUpdate;

      if (settingstime > localtime) {
        return true;
      }
    },

    /**
     * Save data into localstorage
     *
     *  @param data object
     */
    setStoreData: function(data) {
      var timestamp = storeLocalStorage.getTimeStamp(),
          json = JSON.stringify(data);

      localStorage.setItem('wfm_store_last_update', timestamp);
      localStorage.setItem('wfm_store_data', json);
    },

    /**
     * Save data to settings object
     *
     * @param data object
     */
    storeDataSettingsObject: function(data) {
      Drupal.settings.WholeFoods.stores = data;
    },

    /**
     * Return true if localstorage is present in browser, false if not.
     *
     * This is Modernizr's test for localstorage:
     * https://github.com/Modernizr/Modernizr/blob/master/feature-detects/storage/localstorage.js
     */
    testLocalStorage: function() {
      var mod = 'test';
      try {
        localStorage.setItem(mod, mod);
        localStorage.removeItem(mod);
        return true;
      } catch(e) {
        return false;
      }
    },

  };

  /**
   * Attach to Drupal behaviors
   */
  Drupal.behaviors.localstorestorage = {
    attach: function (context, settings) {
      $('body').once('localstorestorage', function () {
        storeLocalStorage.init();
      });
    }
  };

})(jQuery);
