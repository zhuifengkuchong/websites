/**
 * @file
 * Monster Navigation javascript handlers
 */

(function ($) {
  /**
   * Loads dynamic links per user settings.
   */
  Drupal.behaviors.wfm_monster_nav = {
    attach: function(context, settings) {
      $(window).load(function(){
        var links_container = $('div.main-links-container', context);
        //console.log('wfm_monster_nav.js - Drupal.behaviors.wfm_monster_nav: settings.wfm_monster_nav = ');
        //console.debug(settings.wfm_monster_nav);
        if (links_container.length) {
          //if (typeof settings.wfm_monster_nav == 'object') {
          if (typeof Drupal.settings.WholeFoods == 'object') {
          
            //var dynamic_links_url = settings.basePath + 'wfm-monster-nav/load-dynamic-links/' +
                //settings.wfm_monster_nav.uid + '/' + settings.wfm_monster_nav.store_nid;
          
            if (Drupal.settings.WholeFoods.user.drupalUID) {
              var userid = Drupal.settings.WholeFoods.user.drupalUID;
            } else {
              var userid = '0';
            }
            if (Drupal.settings.WholeFoods.localStore) {
            var dynamic_links_url = settings.basePath + 'wfm-monster-nav/load-dynamic-links/' + userid + '/' + 
              Drupal.settings.WholeFoods.localStore;
            } else {
              /* TODO this is default node id of US.  Needs to work internationally */
              var dynamic_links_url = settings.basePath + 'wfm-monster-nav/load-dynamic-links/' + userid + '/129556';
            }
            //console.log('wfm_monster_nav.js - Drupal.behaviors.wfm_monster_nav: dynamic_links_url = ' + dynamic_links_url);
            $.ajax({
              url: dynamic_links_url,
              dataType: 'json',
              error: function() {
             },
              success: function (data) {
                // Add loaded items
                for (var selector in data) {
                  var dynamic_link = $('div.main-links-container a.ajax-load[rel="' + selector + '"] + div.sub-menu-drop-container', context);
                  if (dynamic_link.length) {
                    if (data[selector].length) {
                      dynamic_link.html(data[selector]);
                    }
                    else {
                      // Remove links block if no items loaded
                      dynamic_link.parent('li').remove();
                    }
                  }
                }

                // Execute behaviors
                Drupal.attachBehaviors(links_container);
              }
            });
          }
        }
      });//document.ready
    }
  }
})(jQuery);
