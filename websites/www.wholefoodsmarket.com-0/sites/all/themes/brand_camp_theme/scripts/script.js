/**
 * @file
 * Theme specific JS file for brand_camp_theme
 */
(function($){
  $('.bc-menu-btn').live('click', function(e) {
    if ($('.block-wfm-monster-nav .block-content').toggleClass('displayed').hasClass('displayed')) {
      $('.block-wfm-monster-nav .block-content').animate({marginLeft: 0}, 400);
    }
    else {
      $('.block-wfm-monster-nav .block-content').animate({marginLeft: "-205px"}, 400);
    }
  });
  $('.nav-tab').live('click', function(e) {
    var $monsterNavBlockContent = $('.block-wfm-monster-nav .block-content');
    if ($monsterNavBlockContent.toggleClass('displayed').hasClass('displayed')) {
      $monsterNavBlockContent.animate({marginLeft: 0}, 400);
    }
    else {
      $monsterNavBlockContent.animate({marginLeft: "-205px"}, 400);
    }
  });
  $('.custom-load-more a').live('click', function(e) {
    $(this).hide();
    $('#block-views-brand-camp-list-block-1').show();
    $.jail();
    return false;
  });

  Drupal.behaviors.events = {
    attach: function(context, settings) {
      $(document).ajaxComplete(function(event, xhr, settings){
        //$(window).resize();
      //  $('.bc-progress').remove();
        if ($('#colorbox:visible').length) {
          var $cboxImages = $('#cboxContent img');
          if (!$cboxImages.length) {
            $.colorbox.resize();
          } else {
            var i = 0;
            $cboxImages.each(function(){
              $(this).load(function(){
                i++;
                if(i == $cboxImages.length) {
                  setTimeout($.colorbox.resize(), 100);
                }
              });
            });
          }

          $('#footer .view-display-id-wfm_footer_store_info').each(function() {
            $('#colorbox #footer-user-store-selector').replaceWith($(this).html());
          });
        }
      });
      //
      //$form.ajaxStart(function(event, xhr, settings){
      //  $(window).resize();
      //  if (!$('.bc-progress').length) {
      //    $(this).parent('.view-filters').after('<div class="bc-progress"></div>');
      //  }
      //});
    }
  };

  $(document).ready(function(){
    $('<div class="bc-menu-btn">' + Drupal.t('Menu') + '</div>').prependTo('#block-wfm-monster-nav-wfm-monster-navigation .block-inner');
    $(window).resize(function(){
      if ($('#colorbox:visible').length) {
        $.colorbox.resize({width: Math.min(1060, $(window).width())});
      }
      var $widget = $('.views-exposed-widget, .isoFilters-widget');
      var $formWrp = $('.view-filters, .isoFilters');
      if ($('#page').width() <= 1059) {
        $widget.addClass('tablet-widget');
        assignClick();
        // maybe we should replace it by media-query
        $formWrp.removeClass('desktop');
      }
      else {
        $formWrp.addClass('desktop');
        $widget.removeClass('tablet-widget').removeClass('expanded').attr('style', '');
        $widget.unbind('click');
      }
    });
    $(window).resize();
  });

  function assignClick() {
    // Removes previous event binding to prevent reactions duplicate
    $('.tablet-widget').unbind('click');
    $('.tablet-widget').click(function(e){
      var height = getLabelsHeight($(this));
      if (!$(this).hasClass('expanded')) {
        $(this).siblings().height(50).removeClass('expanded');
        $(this).height(height).addClass('expanded');
      }
      else {
        if (e.target.nodeName == 'LABEL') {
          $('#' + $(e.target).attr('for')).attr('checked', 'checked');
          $('#edit-submit-brand-camp-list').click();
          return;
        }
        $(this).height(50).removeClass('expanded');
      }
    });
  }

  function getLabelsHeight($el) {
    var $labels = $el.find('.views-widget label, .filter');
    var height = $labels.height();
    return height * ($labels.length + 1);
  }

  // animates couple of text slides
  function animateNext(outId, inId) {
    if ('flagIE' in window && flagIE == true) {
      jQuery(outId).animate({
        left: "-1000px",
      }, 1000, function(){
        jQuery(this).hide();
      });
      jQuery(inId).css('left', '1000px').show().animate({
        left: "0",
      }, 1000);

      return;
    }
    jQuery(outId).removeClass('slideInRight').addClass('slideOutLeft');
    jQuery(inId).removeClass('invisible').removeClass('slideOutLeft').addClass('slideInRight');
  }

  //animates array of text slides (id's)
  function animateSlides(slides) {
    var currentSlide = 0;
//    slides.forEach(function (slide) { jQuery(slide).removeClass('invisible') });
    setInterval(function() {
      if (currentSlide == slides.length-1) {
        animateNext(slides[currentSlide], slides[0]);
        currentSlide = 0;
      } else {
        animateNext(slides[currentSlide], slides[currentSlide+1]);
        currentSlide++;
      }
    }, 5000);
  }

  //animate header and footer text slides
  setTimeout(function() {
    animateSlides(['#header1', '#header2', '#header3', '#header4']);
  }, 1000);

  $(document).ready(function() {
    (function addScrollTopButton() {
      $('body').append(
        $('<div></div>')
          .attr({ class: 'scrollTop', id: 'scrollTop' })
          .click(function() {
            $.colorbox.close();
            window.scrollTo(0, 0);
          })
          .hide()
      );
    })();

    $(window).scroll(function() {
      if ($(this).scrollTop() > 1000 ) {
        $('#scrollTop').fadeIn(400);
      } else {
        $('#scrollTop').fadeOut(400);
      }
    });

    var _windowWidth = 0;
    $(window).resize(function() {
      if (_windowWidth != jQuery(window).width()) {
        _windowWidth = jQuery(window).width();
        jQuery('.page-values-matter .view-brand-camp-list.views-quicksand-container .view-content')
          .css({width: 'auto', height: 'auto'});
      }
    });
  });

})(jQuery);

jQuery.urlParam = function(name){
  var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
  if (results==null){
    return null;
  }
  else{
    return results[1] || 0;
  }
}