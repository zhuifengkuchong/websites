/*
 *  JAIL: jQuery Asynchronous Image Loader (brand camp version)
 */
(function ( name, definition ){
  // jquery plugin pattern - AMD + CommonJS - by Addy Osmani (https://github.com/addyosmani/jquery-plugin-patterns/blob/master/amd+commonjs/pluginCore.js)
  var theModule = definition(jQuery),
      hasDefine = typeof define === 'function' && define.amd;

  if ( hasDefine ){
    // AMD module
    define( 'jail', ['jquery'], theModule );

  }  else {
    ( this.jQuery || this.$ || this )[name] = theModule;
  }
}( 'jail', function ($) {
  /*
   * Public function defining 'jail'
   *
   * @param elements : images to load
   * @param options : configurations object
   */
  $.jail = function( elements, options) {
    $('body.page-node img[data-src]:visible').each(function(){
      $img = $(this);
      $img.attr("src", $img.attr("data-src"));
      $img.removeAttr('data-src');
    });
    if (!$isotopePager.hasOwnProperty('getVisible')) {
      return; //pager haven't loaded yet
    }

    $($isotopePager.getVisible()).find('img[data-src]').each(function(){
      $page = $('#page');
      $img = $(this);
      //if (_isInTheScreen($page, $img, 0)) {
        $img.attr("src", $img.attr("data-src"));
        $img.removeAttr('data-src');
      //}
    })
  };

  /*
   * Function that returns true if the image is visible inside the "window" (or specified container element)
   *
   * @param $ct : container - jQuery obj
   * @param $img : image selected - jQuery obj
   * @param optionOffset : offset
   */
  function _isInTheScreen ( $ct, $img, optionOffset ) {
    var is_ct_window  = $ct[0] === window,
        ct_offset  = (is_ct_window ? { top:0, left:0 } : $ct.offset()),
        ct_top     = ct_offset.top + ( is_ct_window ? $ct.scrollTop() : 0),
        ct_left    = ct_offset.left + ( is_ct_window ? $ct.scrollLeft() : 0),
        ct_right   = ct_left + $ct.width(),
        ct_bottom  = ct_top + $ct.height(),
        img_offset = $img.offset(),
        img_width = $img.width(),
        img_height = $img.height();

    return (ct_top - optionOffset) <= (img_offset.top + img_height) &&
        (ct_bottom + optionOffset) >= img_offset.top &&
        (ct_left - optionOffset)<= (img_offset.left + img_width) &&
        (ct_right + optionOffset) >= img_offset.left;
  }

  // Small wrapper
  $.fn.jail = function( options ) {

    new $.jail( this, options );

    // Empty current stack
    currentStack = [];

    return this;
  };

  return $.jail;
}));