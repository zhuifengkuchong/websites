/**
 * @file
 * Behavior for store select forms.
 */

//Self-invoking function
(function($){
  "use strict";
  $.fn.WFMstoreSelect = function() {
    var storeselect = {

      settings: {
        storeoptions: '',
      },

      /**
       * Add listeners to dom elements
       */
      addListeners: function() {
        $('.state-select').change(function() {
          storeselect.enableStoreSelect(this);
          storeselect.setStoreOptionsByState(this);
          storeselect.disableSubmit(this);
        });
        $('.store-select').change(function() {
          storeselect.enableSubmit(this);
        });
      },

      ajaxCallBack: function(storeObject) {
        var store = Drupal.WholeFoods.removeStoreNidKey(storeObject);
        storeselect.setUserDefaults(store);
      },

      /**
       * Disable store-select form store drop-down.
       * @param object state_select
       *   DOM element of state select. 
       */
      disableStoreSelect: function(state_select) {
        var form_id = '#' + $(state_select).parents('form').attr('id'), 
            selected_state = $(state_select).val();
        if (!selected_state || selected_state == '0') { //Zero as a string. Intentional.
          $(form_id + ' .store-select').addClass('disabled').prop('disabled', true);
        }
      },

      /**
       * Disable store-select form submit.
       * @param object state_select
       *   DOM element of state select. 
       */
      disableSubmit: function(state_select) {
        var form_id = '#' + $(state_select).parents('form').attr('id'),
            selected_store = $(form_id + ' .store-select').val();
        if (!selected_store) {
          $(form_id + ' .store-select-submit').addClass('disabled').prop('disabled', true);
        }
      },

      /**
       * Enable store-select form store drop-down.
       * @param object state_select
       *   DOM element of state select. 
       */
      enableStoreSelect: function(state_select){
        var form_id = '#' + $(state_select).parents('form').attr('id');
        if ($(state_select).val() && $(state_select).val() !== '0') { // Zero as a string. Intentional.
          $(form_id + ' .store-select').removeClass('disabled').prop('disabled', false);
        }
      },

      /**
       * Enable store-select form submit.
       * @param object store_select
       *   DOM element of store select.
       */
      enableSubmit: function(store_select) {
        var form_id = '#' + $(store_select).parents('form').attr('id');
        $(form_id + ' .store-select-submit').prop('disabled', false).removeClass('disabled');
      },

      /**
       * Build <option> element.
       * @param object values
       *   Attributes for the option element in key/value pairs.
       * @return object
       *   <option> DOM element
       */
      getOption: function(values) {
        var opt = document.createElement('OPTION'),
            option_values = {},
            defaults = {
              label: Drupal.t('Please Select a Store'),
              text: Drupal.t('Please Select a Store'),
              value: ''
            };
        if (typeof values == 'undefined') {
          values = {};
        } 
        option_values = $.extend(option_values, defaults, values);
        for (var attribute in option_values) {
          opt[attribute] = option_values[attribute];
        }
        return opt;
      },

      /**
       * Get store option values from the store select drop-down and save them
       * in storeselect.settings.storeoptions variable.
       */
      getStoreOptions: function() {
        var options = {};
        $('.store-select').first().children('optgroup').each(function(){
          var label = $(this).attr('label');
          options[label] = {};
          $(this).children('option').each( function(){
            var value = $(this).val();
            options[label][value] = {};
            options[label][value]['name'] = $(this).text();
            options[label][value]['selected'] = false;
            if($(this).prop('selected') === true) {
              options[label][value]['selected'] = true;
            }
          });
        });
        storeselect.settings.storeoptions = options;
      },

      /**
       * Initialization function.
       */
      init: function() {
        if ($('form#store-select-form').length > 1) {
          $('form#store-select-form').each(function(i){
            this.setAttribute("id", "store-select-form-" + i);
          });
        }
        storeselect.addListeners();
        storeselect.getStoreOptions();
        $('.state-select').each(function(){
          storeselect.disableStoreSelect(this);
          storeselect.setStoreOptionsByState(this);
          storeselect.disableSubmit(this);
        })
        storeselect.prepareUserDefaults();
      },

      /**
       * Determine if user has a store selected
       */
      prepareUserDefaults: function() {
        var storenid = '',
            state = '',
            storeinfo = '';
        if (Drupal.WholeFoods.getCookie('local_store')) {
          storenid = Drupal.WholeFoods.getCookie('local_store');
        }
        if (storenid) {
          Drupal.WholeFoods.getStoreInfo(storenid, storeselect.ajaxCallBack);
        }
      },

      /**
       * Filter store select options by selected state.
       * @param object state_select
       *   State select DOM object
       */
      setStoreOptionsByState: function(state_select){
        var options = storeselect.settings.storeoptions,
            stateabbr = $(state_select).val(),
            defaultopt = storeselect.getOption(),
            form_id = '#' + $(state_select).parents('form').attr('id'),
            store_select_element = $(form_id + ' .store-select');

        store_select_element.find('option').each(function(){
          $(this).detach();
        });
        store_select_element.find('optgroup').each(function(){
          $(this).detach();
        });
        store_select_element.append(defaultopt);
          
        //loop through options object, create optgroups and options
        $.each(options, function(index, value) {
          var abbr = index.substring(0, 2),
              optgroup = {};
          if (abbr == stateabbr) {
            optgroup = document.createElement('optgroup');
            optgroup.label = index.substring(2);
            store_select_element.append(optgroup);
            $.each(value, function(optindex, optvalue) {
              var option_values = {
                    label: optvalue.name,
                    text: optvalue.name,
                    value: optindex,
                    selected: optvalue.selected
                  },
                  opt = storeselect.getOption(option_values);
              store_select_element.find('optgroup').last().append(opt);
            });
          }
        });
      }, //setStateOptions

      /**
       * Set default state/store values from user's selected store.
       * @param object store
       *   Store info object from Drupal.setting.WholeFoods.stores
       */
      setUserDefaults: function(store) {
        if (Object.keys(store).length === 1) {
          store = Drupal.WholeFoods.removeStoreNidKey(store);
        }
        $('.state-select option[value="' + store.location.stateabbr + '"]').prop('selected', true);

        //This trigger is a FF fix.
        $('.state-select').trigger('change');

        $('.store-select').removeClass('disabled').prop('disabled', false);
        $('.store-select option[value="' + store.nid + '"]').prop('selected', true);
        $('.store-select').each(function(){
          storeselect.enableSubmit(this);
        });
      },

    }; //storeselect
    storeselect.init();
    Drupal.WholeFoods.setStoreSelectStore = storeselect.setUserDefaults;
  };

})(jQuery);


/**
 * Attach to Drupal behaviors.
 */
Drupal.behaviors.wfmstoreselect = {
  attach: function () {
    jQuery('body').once('ss-attach', function () {
      jQuery('.store-select-form').WFMstoreSelect();
    });
  }
};
;/**/
(function storeFooter($){
  'use strict';
  var sf = {
    wrapper: '#footer-user-store-selector',

    /**
     * @param string store
     *   store node id
     * @return string
     *   Display HTML for chosen store
     */
    buildInnerHtml: function(store) {
      var out = '',
          country = sf.getCountryName(store.location.country),
          link = '<a href="/stores/' + store.path + '">' + store.storename + '</a>',
          infolink = '<a href="/stores/' + store.path + '">' + Drupal.t('More info about this store') + '</a>',
          maplink = '<a href="/store-locations?store=' + store.nid + '">' + Drupal.t('Map & Directions') + '</a>',
          address = store.location.street + '<br />' + store.location.city + ', ' 
            + store.location.stateabbr + ' ' + store.location.zip + '<br />' 
            + country;
      
      out += '<h4>' + Drupal.t('Your Store Is') + ' ' + link + '</h4>';
      out += '<address>' + address + '</address>';
      out += '<span class="phone">' + Drupal.t('Phone:') + ' ' + store.phone + '</span><br />';
      out += '<span class="hours">' + Drupal.t('Hours:') + ' ' + store.hours + '</span><br />';
      out += '<span class="links">' + infolink + ' | ' + maplink + '</span>';
      return out;
    },

    /**
     * Get country name from abbreviation.
     * @param string country
     *   Country abbreviation.
     * @return string
     *   Country name
     */
    getCountryName: function(country) {
      if (country == 'US') {
        return Drupal.t('United States');
      }
      else if (country == 'CA') {
        return Drupal.t('Canada');
      }
      else if (country == 'UK' || country == 'GB') {
        return Drupal.t('United Kingdom');
      }
    },

    /**
     * Kick off script
     */
    init: function() {
      var user_store = $.cookie('local_store');
      if (user_store) {
        Drupal.WholeFoods.getStoreInfo(user_store, sf.placeStoreInfo);
      }
    },

    /**
     * Place store info in footer.
     * @param object || string || number store
     *   Param can either be a store node id or object of store info.
     *   Object should be formatted like this:
     *   {
     *     storenid: { 
     *        key: value,
     *        ...
     *     }
     *   } 
     */
    placeStoreInfo: function(store) {
      var storeinfo = {},
          storeHTML = '';

      if (typeof store == 'object') {
        storeinfo = Drupal.WholeFoods.removeStoreNidKey(store);
        storeHTML = sf.buildInnerHtml(storeinfo);

        $(sf.wrapper).empty();
        $(sf.wrapper).append(storeHTML);
      }
      else if (typeof store == 'string' || typeof store == 'number') {
        Drupal.WholeFoods.getStoreInfo(store, sf.placeStoreInfo);
      }
    },
  };

  // Attach to Drupal behaviors.
  Drupal.behaviors.store_footer_store_locator = {
    attach: function() {
      $(sf.wrapper).once('footer-user-store-selector', function() {
        // Run the script.
        sf.init();
        // Provide API to switch store in footer to external scripts.
        Drupal.WholeFoods.changeStoreInFooter = sf.placeStoreInfo;
      });
    }
  }
})(jQuery);;/**/
/**
 * @file
 * Javascript file for Healthy Eating newsletter signup.
 */
(function( $ ){
  $.fn.ajaxNewsletter = function(options) {
    var ajaxNewsletterForm = {
      options: $.extend({
        'formSelector': '.newsletter-ajax-form',
        'inputSelector': '.newsletter-ajax-form input.email',
        'submitSelector': '.newsletter-ajax-form input.form-submit',
        'submitElement': '<input type="submit" id="edit-submit" name="op" value="Subscribe" class="form-submit">',
      }, options),

      /**
       * Add event listeners.
       */
      addListeners: function() {
        $('body').delegate(ajaxNewsletterForm.options.formSelector, 'submit', function(){
          ajaxNewsletterForm.onFormSubmit();
          return false;
        });
        $('body').delegate(ajaxNewsletterForm.options.inputSelector, 'focus', function(){
          if($(this).hasClass('error')) {
            $(this).removeClass('error');
            $(this).val('');
          }
        });
      },

      /**
       * Before AJAX call replace submit button with loading gif.
       */
      beforeSendCallback: function(){
        $('.status-message').remove();
        var imagePath = '../../../all/themes/wholefoods/images/ajax-loader2.gif'/*tpa=http://www.wholefoodsmarket.com/sites/all/themes/wholefoods/images/ajax-loader2.gif*/;
        var imageTag = '<img src="'+imagePath+'" alt="' + Drupal.t('Loading....') + '" id="loadinggif">';
        $(ajaxNewsletterForm.options.submitSelector).replaceWith(imageTag);
      },

      /**
       * User feedback error message.
       *
       * @param string message
       *   Message to display.
       */
      errorMessage: function(message) {
        $(ajaxNewsletterForm.options.inputSelector).addClass('error');
        var display_message = '<p class="error  status-message">' + message + '</p>';
        $('#loadinggif').replaceWith(display_message);
        $('p.error').after(ajaxNewsletterForm.options.submitElement);
      },

      /**
       * User feedback error message.
       */
      getInputData: function() {
        var data = {};
        $(this.options.formSelector + ' input').each(function(index){
          var key = $(this).attr('name');
          var value = $(this).val();
          data[key] = value;
        });
        return data;
      },

      /**
       * Set everything up.
       */
      init: function() {
        ajaxNewsletterForm.addListeners();
        ajaxNewsletterForm.setDefaults();
      },

      /**
       * Form submit callback function.
       */
      onFormSubmit: function() {
        var formdata = ajaxNewsletterForm.getInputData();
        var url = '/ajax/newsletters/subscribe/healthy_eating';
        var settings = {
          beforeSend: ajaxNewsletterForm.beforeSendCallback(),
          data: formdata,
          success: function(data, textStatus, jqXHR) {
            var response = $.parseJSON(data);
            ajaxNewsletterForm.successCallback(response);
          },
          type: 'POST',
        };
        $.ajax(url, settings);
      },

      /**
       * Set email input from Drupal.settings.WholeFoods.user.email
       */
      setDefaults: function() {
        if(Drupal.settings.WholeFoods.user.email !== null) {
          var email = Drupal.settings.WholeFoods.user.email;
          $(ajaxNewsletterForm.options.inputSelector).val(email);
        }
      },

      /**
       * AJAX success callback function
       *
       * @param object response
       *   JSON encoded response object from drupal_http_request.
       */
      successCallback: function(response) {
        if(response.code === '200') {
          ajaxNewsletterForm.successMessage();
        }
        else {
          ajaxNewsletterForm.errorMessage(response.status_message);
        }
      },

      /**
       * Set user feedback success message.
       */
      successMessage: function() {
        $(ajaxNewsletterForm.options.inputSelector).addClass('success');
        $(ajaxNewsletterForm.options.formSelector).addClass('success');
        var success_message = Drupal.t("Thanks! You're subscribed!");
        var display_message = '<p class="success status-message">' + success_message + '</p>';
        $('#loadinggif').replaceWith(display_message);
      },
    }
    ajaxNewsletterForm.init();
  };
})( jQuery );

//Attach to Drupal behaviors
(function( $ ) {
  Drupal.behaviors.ajaxNewsletter = {
    attach: function(context, settings) {
      //Run on window.load so that settings are ready in Drupal.settings object
      $(window).load(function(){
        $('body', context).once('ajaxNewsletter', function(){
          $().ajaxNewsletter();
        });
      });
    }
  }
})( jQuery );
;/**/
