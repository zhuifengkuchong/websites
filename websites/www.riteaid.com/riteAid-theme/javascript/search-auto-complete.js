$(document).ready(function() {
	$("#SearchTextbox").autocomplete( {
		source: function( request, response) {
			$.ajax({
				url: "/services/autoSuggest/getAutoSuggestions?json.wrf=?",
				dataType: "jsonp",
				data: {
					q: request.term,
					rows: 10
				},
				success: function(data) {
					response($.map(data.autoSuggest,function(doc) {
					//response( $.map(data.facet_counts.facet_fields.autoComplete,function(doc) {
						return {
							label: doc,
							value: doc,
						}
					}));
				}
			});
		},
		minLength: 2,
		open: function() {
			$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
			$("ul.ui-autocomplete").css({"position":"527px"});
			$("ul.ui-autocomplete").css({"width":"527px"});
			$("li.ui-menu-item").css({"width":"527px"});
					
			//$("li.ui-menu-item:odd").addClass("ui-menu-item-alternate");
		},
		close: function() {
			$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
		},
		select: function( event, ui ) {
			$("#SearchTextbox").val(ui.item.label);
			var $form = $("#searchForm");
			$form.submit();
		}
	});
	$("#SearchTextboxBt").autocomplete( {
		source: function( request, response) {
			$.ajax({
				url: "/services/autoSuggest/getAutoSuggestions?json.wrf=?",
				dataType: "jsonp",
				data: {
					q: request.term,
					rows: 10
				},
				success: function(data) {
					response($.map(data.autoSuggest,function(doc) {
					//response( $.map(data.facet_counts.facet_fields.autoComplete,function(doc) {
						return {
							label: doc,
							value: doc,
						}
					}));
				}
			});
		},
		minLength: 2,
		open: function() {
			$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
			$("ul.ui-autocomplete").addClass("ui-autocomplete-header");
			//$("li.ui-menu-item:odd").addClass("ui-menu-item-alternate");
			$("li.ui-menu-item").addClass("ul-autocomplete-ellipsis");
			$("ul.ui-autocomplete").css({"width":"211px"});
			$("li.ui-menu-item").css({"width":"211px"});			
			$("li.ui-menu-item > a").bind('mouseenter', function () {
		    var $this = $(this);
		    if (this.offsetWidth < this.scrollWidth && !$this.attr('title'))
		        $this.attr('title', $this.text()); });
		},
		close: function() {
			$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
		},
		select: function( event, ui ) {
			$("#SearchTextboxBt").val(ui.item.label);
			var $form = $("#search-form");
			$form.submit();
		}
	});
});
