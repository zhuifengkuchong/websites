(function($) {

	$.fn.validateMessageOnRunTime = function(options) {

		var settings = $.extend({
			// These are the defaults.
			validateNoHTML: 													"had HTML",
			validateAlphaWithPeriodSingleQuoteSpaces: 							"can only contain a-z, quote, period and spaces",
			validateNumerics:													"can not be all numbers",
			validateUserName:													"Please enter a different username. Usernames must be between 6 and 15 characters and contain only letters, numbers or underscores ( _ ).",
			validatePassword:													"Please enter a different password. Passwords must be between 8 and 15 characters and contain only letters or numbers.",
			validatePasswordLength:												"Please enter a different password. Passwords must be between 8 and 15 characters and contain only letters or numbers.",
			validateNotBlank:													"Please",
			validateNotBlankForPhone:											"Please enter your phone number.",
			validateCardNumber:													"The card number you entered is invalid. Please check your entry and try again. Please enter your wellness+ or wellness+ with Plenti card number.",
			validatePlentiNumber:        										"The card number you entered is invalid. Please check your entry and try again. Please enter your wellness+ with Plenti card number.",
			validatePlentiCardNumber:											"The Plenti card number you entered is invalid. Please check your entry and try again.",
			validateWellnessCardNumber:											"The wellness+ card number you entered is invalid. Please check your entry and try again.",
			validateAssosiateCardNumber:										"Associate Wellness Card Number is not valid.",
			validateEqualToField:												"Your entries do not match. Please enter your {name} again.",
			validateEmailAddress:                           					"Please enter a valid email address.",
			validateLength:														"{name} name must be at least 2 characters long.",
			AlphaNumericWithPeriodSingleQuoteSpacesCommaSlashColonHyphenHash:   "Sorry, the {name} you entered contained an invalid character. Please enter your {name} again.",
			validateRequiredLength:												"{name} must be at least {requiredLength} characters long.",
			validateNoSpace:													"spaces are not allowed.",
			validateZipCode:													"Please enter a valid ZIP code.",
			validateNotAllSpace:												"can not be all spaces.",
			validateZipCode:													"Please enter a valid ZIP code.",
			validateDate:														"Please enter a valid date of birth.",
			validateBirthDate:                                                  "Sorry, you must be at least 13 years old to sign up for a wellness+ with Plenti account.",
			validateNotAllNumerics:												"Please enter a different username. Usernames must be between 6 and 15 characters and contain only letters, numbers or underscores ( _ ).",
			validateAlphabates: 												"Your {name} name can contain only letters. Please",
			validateSecurityAnswer:											    "Sorry, the answer to your security question can contain only letters, numbers, and spaces. Please try again.",
			validatePhoneNumberNumeric:											"The phone number you entered is invalid. Your phone number can contain only numbers. Please check your entry and try again.",
			validatePhoneNumber:												"Please enter a valid phone number.",
			validateDateBlank:													"Please enter your date of birth.",
			validateSixtyFiveAge:												"Sorry, you must be at least 65 years old to sign up for wellness65+.",
			enableButton:													    "false"
		}, options );

		var enableSubmit = false;
		var fieldObject = {};
		var fieldPhoneNumberObject = {};
		var fieldDateObject = {};
		var phoneCheckBlankFlag = false;
		var datecheckBlankFlag =  false;
		var removeMessageFlag = true;
		var blurCheckFlag = false;
		var keyPressFlag = false;
		if(settings.enableButton == 'false') {
			$("[validationForInlineSubmit='disable-submit-button']").css({"background":"none repeat scroll 0 0 #d9d9d9","color":"#999", "cursor":"default"});
			$("[validationForInlineSubmit='disable-submit-button']").attr("disableButton","true");
		}
		this.each(function(){
			if ($(this).attr('type') != 'radio') {
				var optionalField = $(this).attr('optionalField'); 
				if(typeof optionalField === typeof undefined || optionalField === false) {			

					if (settings.enableButton == 'false') {
						if(!$(this).val()) {
							fieldObject[$(this).attr('id')] = false;
						} else {
							fieldObject[$(this).attr('id')] = true;
						}

						if ($("#isWellnessSignup").val() != 'true') {
							if($(this).is(':hidden') == true) {
								fieldObject[$(this).attr('id')] = true;
							}
						}					
						if ($("#isWellnessSignup").val() != 'true') {
							if(fieldObject[$("[validatedate='MM']").attr("id")] == true && fieldObject[$("[validatedate='DD']").attr("id")] == true && fieldObject[$("[validatedate='YYYY']").attr("id")] == true) {
								fieldObject[$("[findErrorMessageForDate='validateBox']").attr("id")] = true;
							} else {
								fieldObject[$("[findErrorMessageForDate='validateBox']").attr("id")] = false;
							}
						}
						if ($("#isWellnessSignup").val() != 'true') {
							if(fieldObject[$("[validatePhoneNumber='areaCode']").attr("id")] == true && fieldObject[$("[validatePhoneNumber='firstThree']").attr("id")] == true && fieldObject[$("[validatePhoneNumber='lastFour']").attr("id")] == true) {
								fieldObject[$("[findErrorMessageForPhoneNumber='validateBox']").attr("id")] = true;
							} else {
								fieldObject[$("[findErrorMessageForPhoneNumber='validateBox']").attr("id")] = false;
							}
						}	
					} else if (settings.enableButton == 'true')	{
						fieldObject[$(this).attr('id')] = true;
						fieldObject[$("[findErrorMessageForDate='validateBox']").attr("id")] = true;
						fieldObject[$("[findErrorMessageForPhoneNumber='validateBox']").attr("id")] = true;
					}
					fieldObject[$("[validatedate='MM']").attr("id")]=true;
					fieldObject[$("[validatedate='DD']").attr("id")]=true;
					fieldObject[$("[validatedate='YYYY']").attr("id")]=true;
					fieldObject[$("[validatePhoneNumber='areaCode']").attr("id")]=true;
					fieldObject[$("[validatePhoneNumber='firstThree']").attr("id")]=true;
					fieldObject[$("[validatePhoneNumber='lastFour']").attr("id")]=true;

					if($("#"+$("[findErrorMessageForDate='validateBox']").attr("id")).is(':hidden') == true) {
						fieldObject[$("[findErrorMessageForDate='validateBox']").attr("id")] = true;
					} 
					if($("#"+$("[findErrorMessageForPhoneNumber='validateBox']").attr("id")).is(':hidden') == true) {
						fieldObject[$("[findErrorMessageForPhoneNumber='validateBox']").attr("id")] = true;
					}

					if (typeof $(this).attr("streetAddress2Optional") != typeof undefined) {
						fieldObject[$(this).attr('id')] = true;
					}
					var changeHandler = function() {
						if(!$(this).val()) {
							fieldObject[$(this).attr('id')] = false;
						} else {
							fieldObject[$(this).attr('id')] = true;
						}
					};	
					$(this).bind("blur dblclick", function(event) {

						if(event.type === 'dblclick' && $("#isWellnessSignup").val() == 'true') {
							$(this).bind("change", changeHandler);
							return false;
						} else {
							$(this).unbind("change", changeHandler);
						}

						var validationApplied = $(this).attr('ValidationFields');
						var hideErrorMessage =  $("#hideErrorMessage").val();
						var label = $("label[for='"+$(this).attr('id')+"']");
						if(typeof $(this).attr("title")!== typeof undefined) {
							label =	$(this).attr("title");
						} else {
							label =	label.html();
						}

						if($(this).val() == 'Please Enter Your Card Number') {
							$(this).val('');
						}

						$(this).parent().find("#hideValidationErrorMessage-"+$(this).attr('id')).remove();

						if ($(this).attr("id") != $("[validatePhoneNumber='areaCode']").attr("id") && $(this).attr("id") != $("[validatePhoneNumber='firstThree']").attr("id") &&  $(this).attr("id") != $("[validatePhoneNumber='lastFour']").attr("id")) {
							$(this).css({"background-color":"","box-shadow":"","border": ""});
						}



						if (settings.enableButton == 'false') {
							if($("#validateVisibleCheck").val()=='true') {
								$.each(fieldObject, function (index, value) {
									if($("#"+index).is(':hidden') == true) {
										fieldObject[index] = true;
									}
								});
								if($("#"+$("[findErrorMessageForDate='validateBox']").attr("id")).is(':hidden') == true) {
									fieldObject[$("[findErrorMessageForDate='validateBox']").attr("id")] = true;
								} 
								if($("#"+$("[findErrorMessageForPhoneNumber='validateBox']").attr("id")).is(':hidden') == true) {
									fieldObject[$("[findErrorMessageForPhoneNumber='validateBox']").attr("id")] = true;
								} 
								fieldObject[$("[validatedate='MM']").attr("id")]=true;
								fieldObject[$("[validatedate='DD']").attr("id")]=true;
								fieldObject[$("[validatedate='YYYY']").attr("id")]=true;
								fieldObject[$("[validatePhoneNumber='areaCode']").attr("id")]=true;
								fieldObject[$("[validatePhoneNumber='firstThree']").attr("id")]=true;
								fieldObject[$("[validatePhoneNumber='lastFour']").attr("id")]=true;
								$("#validateVisibleCheck").val('false');
							}
						} else if (settings.enableButton == 'true')	{
							fieldObject[$(this).attr('id')] = true;
						}
						try {			
							if(typeof validationApplied !== typeof undefined) {
								validationApplied = $(this).attr('ValidationFields').split(',');
								var errorFlag = false;
								for(var count=0; count < validationApplied.length; count++) {
									if((typeof $(this).attr('validatePhoneNumber') === typeof undefined || validationApplied[count] !== 'NotBlank') && (typeof $(this).attr('validatedate') === typeof undefined || validationApplied[count] !== 'NotBlank') ) {
										if(validationApplied[count] === 'NoHTML') {
											if($(this).val().indexOf('<') !== -1 || $(this).val().indexOf('>') !== -1 ) {
												if(typeof $(this).attr('validateFieldBox') !== typeof undefined) {
													$("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#validateBox").replaceWith('<p id ="validateBox-'+$(this).attr('validateFieldBox')+'"></p>');
													if($("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#hideValidationErrorMessage-"+$(this).attr('id')).length == 0) {

														$("#validateBox-"+$(this).attr('validateFieldBox')).replaceWith('<p id="hideValidationErrorMessage-'+$(this).attr('id')+'" class="errorMessage clear">'+label+': '+settings.validateNoHTML+'</p>');
														$(this).css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
													} else {
														$("#hideValidationErrorMessage-"+$(this).attr('id')).html(label+': '+settings.validateNoHTML);
														$(this).css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
													}


													$('<p id ="validateBox-'+$(this).attr('validateFieldBox')+'"></p>').insertAfter("#hideValidationErrorMessage-"+$(this).attr('id'));
												} else {
													$(this).parent().append( '<p id="hideValidationErrorMessage-'+$(this).attr('id')+'" class="errorMessage clear">'+label+': '+settings.validateNoHTML+'</p>');
													$(this).css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
												}
												fieldObject[$(this).attr('id')] = false;
												errorFlag = true;
												break;
											}
										} else if (validationApplied[count] === 'NoSpace'){
											if($(this).val().indexOf(' ') !== -1) {
												$(this).parent().append( '<p id="hideValidationErrorMessage-'+$(this).attr('id')+'" class="errorMessage clear">'+label+': '+settings.validateNoSpace+'</p>');
												$(this).css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
												fieldObject[$(this).attr('id')] = false;
												errorFlag = true;
												break;
											} 
										} else if (validationApplied[count] === 'NotAllSpace'){
											if($(this).val().trim() == "" || $(this).val().trim().length == 0) {
												$(this).parent().append( '<p id="hideValidationErrorMessage-'+$(this).attr('id')+'" class="errorMessage clear">'+settings.validateNotBlank+' '+label+'</p>');
												$(this).css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
												fieldObject[$(this).attr('id')] = false;
												errorFlag = true;
												break;
											} 

										} else if(validationApplied[count] === 'SecurityAnswer') { 
											if(/^[\\s a-zA-Z0-9]*$/.test($(this).val()) == false) {
												$(this).parent().append( '<p id="hideValidationErrorMessage-'+$(this).attr('id')+'" class="errorMessage clear">'+settings.validateSecurityAnswer+'</p>');
												$(this).css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
												fieldObject[$(this).attr('id')] = false;
												errorFlag = true;
												break;
											}
										} else if(validationApplied[count] === 'AlphabatesOnly'){
											if(/^[\\s a-zA-Z\\.'\\-]*$/.test($(this).val()) == false) {
												var validateMessage = '';
												if(typeof $(this).attr("NameCheck") !== typeof undefined && $(this).attr("NameCheck") == 'first') {
													validateMessage = settings.validateAlphabates.replace('{name}' ,$(this).attr("NameCheck"));
												} else if(typeof $(this).attr("NameCheck") !== typeof undefined && $(this).attr("NameCheck") == 'last') {
													validateMessage = settings.validateAlphabates.replace('{name}' ,$(this).attr("NameCheck"));
												}
												$(this).parent().append( '<p id="hideValidationErrorMessage-'+$(this).attr('id')+'" class="errorMessage clear">'+validateMessage+' '+label+' again.</p>');
												$(this).css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
												fieldObject[$(this).attr('id')] = false;
												errorFlag = true;
												break;
											}
										} else if (validationApplied[count] === 'NotAllNumerics') {
											if(/^[0-9]+$/.test($(this).val())) {
												$(this).parent().append( '<p id="hideValidationErrorMessage-'+$(this).attr('id')+'" class="errorMessage clear">'+settings.validateNotAllNumerics+'</p>');
												$(this).css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
												fieldObject[$(this).attr('id')] = false;
												errorFlag = true;
												break;
											} 
										} else if (validationApplied[count] === 'ZipCode') {
											if($(this).attr('id') === 'canadaPostalCode') {
												if (/^[a-zA-Z0-9]{6,6}$/.test($(this).val()) == false) {
													$(this).parent().append( '<p id="hideValidationErrorMessage-'+$(this).attr('id')+'" class="errorMessage clear">'+settings.validateZipCode+'</p>');
													$(this).css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
													fieldObject[$(this).attr('id')] = false;
													errorFlag = true;
													break;
												}
											} else {
												if(/^[0-9]{5,5}$/.test($(this).val()) == false) {
													if(typeof $(this).attr('validateFieldBox') !== typeof undefined) {
														$("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#validateBox").replaceWith('<p id ="validateBox-'+$(this).attr('validateFieldBox')+'"></p>');

														if($("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#hideValidationErrorMessage-"+$(this).attr('id')).length == 0) {

															$("#validateBox-"+$(this).attr('validateFieldBox')).replaceWith('<p id="hideValidationErrorMessage-'+$(this).attr('id')+'" class="errorMessage clear">'+label+': '+settings.validateZipCode+'</p>');
															$(this).css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
														} else {

															$("#hideValidationErrorMessage-"+$(this).attr('id')).html(label+': '+settings.validateZipCode);
															$(this).css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
														}
														$('<p id ="validateBox-'+$(this).attr('validateFieldBox')+'"></p>').insertAfter("#hideValidationErrorMessage-"+$(this).attr('id'));
													} else {
														$(this).parent().append( '<p id="hideValidationErrorMessage-'+$(this).attr('id')+'" class="errorMessage clear">'+settings.validateZipCode+'</p>');
														$(this).css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
													}
													fieldObject[$(this).attr('id')] = false;
													errorFlag = true;
													break;
												}
											}
										} else if (validationApplied[count] === 'ValidDate') {
											if ($("[validatedate='MM']").val() == '') {
												$("[validatedate='MM']").val('MM');
											}
											if($("[validatedate='DD']").val() == '') {
												$("[validatedate='DD']").val('DD');
											}
											if($("[validatedate='YYYY']").val() == '') {
												$("[validatedate='YYYY']").val('YYYY')
											}

											if($("[validatedate='MM']").val() != 'MM'  && $("[validatedate='DD']").val() !='DD' && $("[validatedate='YYYY']").val() != 'YYYY') {
												var date = $("[validatedate='MM']").val()+'/'+$("[validatedate='DD']").val()+'/'+$("[validatedate='YYYY']").val();
												var validateFlag = '';   
												var m = parseInt($("[validatedate='MM']").val(), 10);
												var d = parseInt($("[validatedate='DD']").val(), 10);
												var y = parseInt($("[validatedate='YYYY']").val(), 10);
												if(y%100 ==0) {
													if(y%400 == 0) {
														validateFlag = 'leap';
													} else {
														validateFlag = 'notleap';
													}
												} else if ( y%4 ==0) {
													validateFlag = 'leap';
												} else {
													validateFlag = 'notleap';
												}
												if(!(/^((0?[13578]|10|12)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[01]?))(-|\/)((19)([0-9])(\d{1})|([2-9])([0-9])([0-9])(\d{1})|([8901])(\d{1}))|(0?[2469]|11)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[0]?))(-|\/)((19)([0-9])(\d{1})|([2-9])([0-9])([0-9])(\d{1})|([8901])(\d{1})))$/.test(date))) {
													if(typeof $(this).attr('validateFieldBox') !== typeof undefined) {
														$("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#validateBox").replaceWith('<p id ="validateBox-'+$(this).attr('validateFieldBox')+'"></p>');
														if($("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#hideValidationErrorMessage-dateOfBirth").length == 0) {

															$("#validateBox-"+$(this).attr('validateFieldBox')).replaceWith('<p id="hideValidationErrorMessage-dateOfBirth" class="errorMessage clear">'+settings.validateDate+'</p>');
															$("["+$(this).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
														} else {

															$("#hideValidationErrorMessage-dateOfBirth").html(settings.validateDate);
															$("["+$(this).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
														}
													} else {
														$(this).parent().append( '<p id="hideValidationErrorMessage-dateOfBirth" class="errorMessage clear">'+settings.validateDate+'</p>');
														$("["+$(this).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
													}
													fieldObject[$("[findErrorMessageForDate='validateBox']").attr("id")] = false;
													errorFlag = true;
													datecheckBlankFlag = true;
													break;
												} else if(validateFlag == 'notleap' && m==2 && d>28 || validateFlag == 'leap' && m==2 && d>29 ) {
													if(typeof $(this).attr('validateFieldBox') !== typeof undefined) {
														$("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#validateBox").replaceWith('<p id ="validateBox-'+$(this).attr('validateFieldBox')+'"></p>');
														if($("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#hideValidationErrorMessage-dateOfBirth").length == 0) {

															$("#validateBox-"+$(this).attr('validateFieldBox')).replaceWith('<p id="hideValidationErrorMessage-dateOfBirth" class="errorMessage clear">'+settings.validateDate+'</p>');
															$("["+$(this).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
														} else {

															$("#hideValidationErrorMessage-dateOfBirth").html(settings.validateDate);
															$("["+$(this).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
														}
													} else {
														$(this).parent().append( '<p id="hideValidationErrorMessage-dateOfBirth" class="errorMessage clear">Date of Birth: '+settings.validateDate+'</p>');
														$("["+$(this).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
													}
													fieldObject[$("[findErrorMessageForDate='validateBox']").attr("id")] = false;
													errorFlag = true;
													datecheckBlankFlag = true;
													break;
												} else {
													var thirteenYearFromCurrent = new Date().getFullYear()-13,
													currentMonth = new Date().getMonth() +1,
													currentDate = new Date().getDate();

													if (y>new Date().getFullYear())	{
														$("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#validateBox").replaceWith('<p id ="validateBox-'+$(this).attr('validateFieldBox')+'"></p>');
														if($("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#hideValidationErrorMessage-dateOfBirth").length == 0) {

															$("#validateBox-"+$(this).attr('validateFieldBox')).replaceWith('<p id="hideValidationErrorMessage-dateOfBirth" class="errorMessage clear">'+settings.validateDate+'</p>');
															$("["+$(this).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
														} else {

															$("#hideValidationErrorMessage-dateOfBirth").html(settings.validateDate);
															$("["+$(this).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
														}
														fieldObject[$("[findErrorMessageForDate='validateBox']").attr("id")] = false;
														errorFlag = true;
														datecheckBlankFlag = true;
														break;
													} else if(y==new Date().getFullYear() && currentMonth<m ) {
														$("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#validateBox").replaceWith('<p id ="validateBox-'+$(this).attr('validateFieldBox')+'"></p>');
														if($("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#hideValidationErrorMessage-dateOfBirth").length == 0) {

															$("#validateBox-"+$(this).attr('validateFieldBox')).replaceWith('<p id="hideValidationErrorMessage-dateOfBirth" class="errorMessage clear">'+settings.validateDate+'</p>');
															$("["+$(this).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
														} else {
															$("#hideValidationErrorMessage-dateOfBirth").html(settings.validateDate);
															$("["+$(this).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
														}
														fieldObject[$("[findErrorMessageForDate='validateBox']").attr("id")] = false;
														errorFlag = true;
														datecheckBlankFlag = true;
														break;
													} else if(y==new Date().getFullYear() && currentMonth==m && currentDate<d ) {
														$("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#validateBox").replaceWith('<p id ="validateBox-'+$(this).attr('validateFieldBox')+'"></p>');
														if($("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#hideValidationErrorMessage-dateOfBirth").length == 0) {

															$("#validateBox-"+$(this).attr('validateFieldBox')).replaceWith('<p id="hideValidationErrorMessage-dateOfBirth" class="errorMessage clear">'+settings.validateDate+'</p>');
															$("["+$(this).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
														} else {

															$("#hideValidationErrorMessage-dateOfBirth").html(settings.validateDate);
															$("["+$(this).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
														}
														fieldObject[$("[findErrorMessageForDate='validateBox']").attr("id")] = false;
														errorFlag = true;
														datecheckBlankFlag = true;
														break;
													} else {
														if (typeof $(this).attr('validateSixtyFivePlusAge') != typeof undefined && $(this).attr('validateSixtyFivePlusAge') == 'true') {
															var sixtyFivePlusAge = new Date().getFullYear()-65;
															if (y>sixtyFivePlusAge ) {
																$("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#validateBox").replaceWith('<p id ="validateBox-'+$(this).attr('validateFieldBox')+'"></p>');
																if($("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#hideValidationErrorMessage-dateOfBirth").length == 0) {
																	$("#validateBox-"+$(this).attr('validateFieldBox')).replaceWith('<p id="hideValidationErrorMessage-dateOfBirth" class="errorMessage clear">'+settings.validateSixtyFiveAge+'</p>');
																	$("["+$(this).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
																} else {
																	$("#hideValidationErrorMessage-dateOfBirth").html(settings.validateSixtyFiveAge);
																	$("["+$(this).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
																}
																fieldObject[$("[findErrorMessageForDate='validateBox']").attr("id")] = false;
																errorFlag = true;
																datecheckBlankFlag = true;
																break;
															} else if(y==sixtyFivePlusAge && currentMonth<m ) {
																$("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#validateBox").replaceWith('<p id ="validateBox-'+$(this).attr('validateFieldBox')+'"></p>');
																if($("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#hideValidationErrorMessage-dateOfBirth").length == 0) {
																	$("#validateBox-"+$(this).attr('validateFieldBox')).replaceWith('<p id="hideValidationErrorMessage-dateOfBirth" class="errorMessage clear">'+settings.validateSixtyFiveAge+'</p>');
																	$("["+$(this).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
																} else {
																	$("#hideValidationErrorMessage-dateOfBirth").html(settings.validateSixtyFiveAge);
																	$("["+$(this).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
																}
																fieldObject[$("[findErrorMessageForDate='validateBox']").attr("id")] = false;
																errorFlag = true;
																datecheckBlankFlag = true;
																break;
															} else if (y==sixtyFivePlusAge && currentMonth==m && currentDate<d ) {
																$("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#validateBox").replaceWith('<p id ="validateBox-'+$(this).attr('validateFieldBox')+'"></p>');
																if($("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#hideValidationErrorMessage-dateOfBirth").length == 0) {
																	$("#validateBox-"+$(this).attr('validateFieldBox')).replaceWith('<p id="hideValidationErrorMessage-dateOfBirth" class="errorMessage clear">'+settings.validateSixtyFiveAge+'</p>');
																	$("["+$(this).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
																} else {
																	$("#hideValidationErrorMessage-dateOfBirth").html(settings.validateSixtyFiveAge);
																	$("["+$(this).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
																}
																fieldObject[$("[findErrorMessageForDate='validateBox']").attr("id")] = false;
																errorFlag = true;
																datecheckBlankFlag = true;
																break;
															} else {
																$("["+$(this).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"","box-shadow":"","border": ""});
																if($("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#validateBox").length == 0) {
																	$('<p id ="validateBox"></p>').insertAfter("[findErrorMessageForDate='validateBox'] #hideValidationErrorMessage-dateOfBirth");	
																}
																$("[findErrorMessageForDate='validateBox']").find("#hideValidationErrorMessage-dateOfBirth").remove();
																fieldObject[$("[findErrorMessageForDate='validateBox']").attr("id")] = true;
															}
														} else {
															if (y>thirteenYearFromCurrent )	{
																$("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#validateBox").replaceWith('<p id ="validateBox-'+$(this).attr('validateFieldBox')+'"></p>');
																if($("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#hideValidationErrorMessage-dateOfBirth").length == 0) {
																	$("#validateBox-"+$(this).attr('validateFieldBox')).replaceWith('<p id="hideValidationErrorMessage-dateOfBirth" class="errorMessage clear">'+settings.validateBirthDate+'</p>');
																	$("["+$(this).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
																} else {
																	$("#hideValidationErrorMessage-dateOfBirth").html(settings.validateBirthDate);
																	$("["+$(this).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
																}
																fieldObject[$("[findErrorMessageForDate='validateBox']").attr("id")] = false;
																errorFlag = true;
																datecheckBlankFlag = true;
																break;
															} else if(y==thirteenYearFromCurrent && currentMonth<m ) {
																$("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#validateBox").replaceWith('<p id ="validateBox-'+$(this).attr('validateFieldBox')+'"></p>');
																if($("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#hideValidationErrorMessage-dateOfBirth").length == 0) {
																	$("#validateBox-"+$(this).attr('validateFieldBox')).replaceWith('<p id="hideValidationErrorMessage-dateOfBirth" class="errorMessage clear">'+settings.validateBirthDate+'</p>');
																	$("["+$(this).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
																} else {
																	$("#hideValidationErrorMessage-dateOfBirth").html(settings.validateBirthDate);
																	$("["+$(this).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
																}
																fieldObject[$("[findErrorMessageForDate='validateBox']").attr("id")] = false;
																errorFlag = true;
																datecheckBlankFlag = true;
																break;
															} else if (y==thirteenYearFromCurrent && currentMonth==m && currentDate<d ) {
																$("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#validateBox").replaceWith('<p id ="validateBox-'+$(this).attr('validateFieldBox')+'"></p>');
																if($("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#hideValidationErrorMessage-dateOfBirth").length == 0) {
																	$("#validateBox-"+$(this).attr('validateFieldBox')).replaceWith('<p id="hideValidationErrorMessage-dateOfBirth" class="errorMessage clear">'+settings.validateBirthDate+'</p>');
																	$("["+$(this).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
																} else {
																	$("#hideValidationErrorMessage-dateOfBirth").html(settings.validateBirthDate);
																	$("["+$(this).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
																}
																fieldObject[$("[findErrorMessageForDate='validateBox']").attr("id")] = false;
																errorFlag = true;
																datecheckBlankFlag = true;
																break;
															} else {
																$("["+$(this).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"","box-shadow":"","border": ""});
																if($("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#validateBox").length == 0) {
																	$('<p id ="validateBox"></p>').insertAfter("[findErrorMessageForDate='validateBox'] #hideValidationErrorMessage-dateOfBirth");	
																}
																$("[findErrorMessageForDate='validateBox']").find("#hideValidationErrorMessage-dateOfBirth").remove();
																fieldObject[$("[findErrorMessageForDate='validateBox']").attr("id")] = true;
															}
														}
													}	
												}
											}  else if(hideErrorMessage == 'false' && ($(this).attr("id") == $("[validatedate='MM']").attr("id") || $(this).attr("id") == $("[validatedate='DD']").attr("id") ||  $(this).attr("id") == $("[validatedate='YYYY']").attr("id"))) {
												blurCheckFlag = true;
												if($("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#hideValidationErrorMessage-PhoneNumberCheck").length != 0){
													removeMessageFlag = false;
													blurCheckFlag = false;
												}


												if($("[validatedate='MM']").val() == 'MM' || $("[validatedate='DD']").val() == 'DD' || $("[validatedate='YYYY']").val() == 'YYYY') {
													if(typeof $(this).attr('validateFieldBox') !== typeof undefined) {
														$("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#validateBox").replaceWith('<p id ="validateBox-'+$(this).attr('validateFieldBox')+'"></p>');
														if($("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#hideValidationErrorMessage-dateOfBirth").length == 0) {
															$("#validateBox-"+$(this).attr('validateFieldBox')).replaceWith('<p id="hideValidationErrorMessage-dateOfBirth" class="errorMessage clear">'+settings.validateDateBlank+'</p>');
															$("["+$(this).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
														} else {
															$("#hideValidationErrorMessage-dateOfBirth").html(settings.validateDateBlank);
															$("["+$(this).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
														}
													}
													fieldObject[$("[findErrorMessageForDate='validateBox']").attr("id")] = false;
													errorFlag = true;
													datecheckBlankFlag = true;
													break;
												}
											}
										} else if (validationApplied[count] === 'AlphaWithPeriodSingleQuoteSpaces') {
											if (/^[\\s a-zA-Z\\.'\\-]*$/.test($(this).val()) == false) {
												$(this).parent().append( '<p id="hideValidationErrorMessage-'+$(this).attr('id')+'" class="errorMessage clear">'+label+': '+settings.validateAlphaWithPeriodSingleQuoteSpaces+'</p>');
												$(this).css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
												fieldObject[$(this).attr('id')] = false;
												errorFlag = true;
												break;
											}
										} else if (validationApplied[count] === 'Numerics') {
											if (/^[0-9]*$/.test($(this).val()) == false) {
												if(typeof $(this).attr('validateFieldBox') !== typeof undefined) {
													$("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#validateBox").replaceWith('<p id ="validateBox-'+$(this).attr('validateFieldBox')+'"></p>');
													if($("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#hideValidationErrorMessage-"+$(this).attr('id')).length == 0) {

														$("#validateBox-"+$(this).attr('validateFieldBox')).replaceWith('<p id="hideValidationErrorMessage-'+$(this).attr('id')+'" class="errorMessage clear">'+label+': '+settings.validateNumerics+'</p>');
														$(this).css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
													} else {

														$("#hideValidationErrorMessage-"+$(this).attr('id')).html(label+': '+settings.validateNumerics);
														$(this).css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
													}
													$('<p id ="validateBox-'+$(this).attr('validateFieldBox')+'"></p>').insertAfter("#hideValidationErrorMessage-"+$(this).attr('id'));
												} else {
													$(this).parent().append( '<p id="hideValidationErrorMessage-'+$(this).attr('id')+'" class="errorMessage clear">'+label+': '+settings.validateNumerics+'</p>');
													$(this).css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
												}
												fieldObject[$(this).attr('id')] = false;
												errorFlag = true;
												break;
											}
										} else if (validationApplied[count] === 'UserName') {
											var minLength = parseInt($(this).attr('LengthCheckMin'),10);
											var maxLength = parseInt($(this).attr('LengthCheckMax'),10);
											if (/^[a-zA-Z0-9_]*$/.test($(this).val()) == false || $(this).val().length < minLength || $(this).val().length > maxLength ) {
												$(this).parent().append( '<p id="hideValidationErrorMessage-'+$(this).attr('id')+'" class="errorMessage clear">'+settings.validateUserName+'</p>');
												$(this).css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
												fieldObject[$(this).attr('id')] = false;
												errorFlag = true;
												break;
											}
										} else if(validationApplied[count] === 'PhoneNumberCheck') {
											if($("[validatePhoneNumber='areaCode']").val() != '' && $("[validatePhoneNumber='firstThree']").val() != '' && $("[validatePhoneNumber='lastFour']").val() != '') { 
												var phoneNumber = $("[validatePhoneNumber='areaCode']").val()+$("[validatePhoneNumber='firstThree']").val()+$("[validatePhoneNumber='lastFour']").val();
												if (/^[0-9]*$/.test(phoneNumber) == false) {
													if(typeof $(this).attr('validateFieldBox') !== typeof undefined) {
														$("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#validateBox").replaceWith('<p id ="validateBox-'+$(this).attr('validateFieldBox')+'"></p>');
														if($("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#hideValidationErrorMessage-PhoneNumberCheck").length == 0) {
															$("#validateBox-"+$(this).attr('validateFieldBox')).replaceWith('<p id="hideValidationErrorMessage-PhoneNumberCheck" class="errorMessage clear">'+settings.validatePhoneNumberNumeric+'</p>');
															$("["+$(this).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
														} else {
															$("#hideValidationErrorMessage-PhoneNumberCheck").html(settings.validatePhoneNumber);
															$("["+$(this).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
														}
													}
													fieldObject[$("[findErrorMessageForPhoneNumber='validateBox']").attr("id")] = false;
													errorFlag = true;
													phoneCheckBlankFlag = true;
													break;
												} else {
													var phoneAreaCode = parseInt($("[validatePhoneNumber='areaCode']").val(), 10);
													var phoneNumberLastDigits = parseInt(($("[validatePhoneNumber='firstThree']").val()+$("[validatePhoneNumber='lastFour']").val()), 10);

													if(!validatePhoneNumber(phoneAreaCode, phoneNumberLastDigits) || phoneNumber.length != 10) {
														if(typeof $(this).attr('validateFieldBox') !== typeof undefined) {
															$("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#validateBox").replaceWith('<p id ="validateBox-'+$(this).attr('validateFieldBox')+'"></p>');
															if($("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#hideValidationErrorMessage-PhoneNumberCheck").length == 0) {
																$("#validateBox-"+$(this).attr('validateFieldBox')).replaceWith('<p id="hideValidationErrorMessage-PhoneNumberCheck" class="errorMessage clear">'+settings.validatePhoneNumber+'</p>');
																$("["+$(this).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
															} else {
																$("#hideValidationErrorMessage-PhoneNumberCheck").html(settings.validatePhoneNumber);
																$("["+$(this).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
															}
														}
														fieldObject[$("[findErrorMessageForPhoneNumber='validateBox']").attr("id")] = false;
														errorFlag = true;
														phoneCheckBlankFlag = true;
														break;
													} else {
														if($("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#validateBox").length == 0) {
															$('<p id ="validateBox"></p>').insertAfter("#hideValidationErrorMessage-PhoneNumberCheck");	
														}
														$("[findErrorMessageForPhoneNumber='validateBox']").find("#hideValidationErrorMessage-PhoneNumberCheck").remove();
														$("["+$(this).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"","box-shadow":"","border": ""});
														fieldObject[$("[findErrorMessageForPhoneNumber='validateBox']").attr("id")] = true;
														removeMessageFlag = true;
													}
												}
											} else if(hideErrorMessage == 'false' && ($(this).attr("id") == $("[validatePhoneNumber='areaCode']").attr("id") || $(this).attr("id") == $("[validatePhoneNumber='firstThree']").attr("id") ||  $(this).attr("id") == $("[validatePhoneNumber='lastFour']").attr("id"))) {
												if(typeof $(this).attr("phoneOptional") == typeof undefined ) {
													blurCheckFlag = true;
													if($("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#hideValidationErrorMessage-PhoneNumberCheck").length != 0){
														removeMessageFlag = false;
														blurCheckFlag = false;
													}
													if(  $("[validatePhoneNumber='areaCode']").val() == '' || $("[validatePhoneNumber='firstThree']").val() == '' || $("[validatePhoneNumber='lastFour']").val() == '') {
														if(typeof $(this).attr('validateFieldBox') !== typeof undefined) {
															$("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#validateBox").replaceWith('<p id ="validateBox-'+$(this).attr('validateFieldBox')+'"></p>');
															if($("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#hideValidationErrorMessage-PhoneNumberCheck").length == 0) {
																$("#validateBox-"+$(this).attr('validateFieldBox')).replaceWith('<p id="hideValidationErrorMessage-PhoneNumberCheck" class="errorMessage clear">'+settings.validateNotBlankForPhone+'</p>');
																$("["+$(this).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
															} else {
																$("#hideValidationErrorMessage-PhoneNumberCheck").html(settings.validateNotBlankForPhone);
																$("["+$(this).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
															}
														}
														fieldObject[$("[findErrorMessageForPhoneNumber='validateBox']").attr("id")] = false;
														errorFlag = true;
														phoneCheckBlankFlag = true;
														break;
													} 
												} else if (!($("[validatePhoneNumber='areaCode']").val() == '' && $("[validatePhoneNumber='firstThree']").val() == ''&&  $("[validatePhoneNumber='lastFour']").val() == '')) {
													blurCheckFlag = true;
													if($("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#hideValidationErrorMessage-PhoneNumberCheck").length != 0){
														removeMessageFlag = false;
														blurCheckFlag = false;
													}
													if(typeof $(this).attr('validateFieldBox') !== typeof undefined) {
														$("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#validateBox").replaceWith('<p id ="validateBox-'+$(this).attr('validateFieldBox')+'"></p>');
														if($("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#hideValidationErrorMessage-PhoneNumberCheck").length == 0) {
															$("#validateBox-"+$(this).attr('validateFieldBox')).replaceWith('<p id="hideValidationErrorMessage-PhoneNumberCheck" class="errorMessage clear">'+settings.validateNotBlankForPhone+'</p>');
															$("["+$(this).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
														} else {
															$("#hideValidationErrorMessage-PhoneNumberCheck").html(settings.validateNotBlankForPhone);
															$("["+$(this).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
														}
													}
													fieldObject[$("[findErrorMessageForPhoneNumber='validateBox']").attr("id")] = false;
													errorFlag = true;
													phoneCheckBlankFlag = true;
													break;
												}
											}
										} else if (validationApplied[count] === 'Password') {

											if ($(this).val().match('[a-z]') == null || $(this).val().match('[A-Z]') == null || $(this).val().match('[0-9]') == null ) {
												$(this).parent().append( '<p id="hideValidationErrorMessage-'+$(this).attr('id')+'" class="errorMessage clear">'+settings.validatePassword+'</p>');
												$(this).css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
												fieldObject[$(this).attr('id')] = false;
												errorFlag = true;
												break;
											}

										} else if(validationApplied[count] === 'Email') {
											if(/^[a-zA-Z0-9\!\#\$\%\&\'\*\+\-\/\=\?\^\_\`\{\|\}\~]+(\.[a-zA-Z0-9\!\#\$\%\&\'\*\+\-\/\=\?\^\_\`\{\|\}\~]+)*@[a-zA-Z0-9]([a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(\.[a-zA-Z0-9]([a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*\.[a-zA-Z]{2,6}$/.test($(this).val()) == false) {
												$(this).parent().append( '<p id="hideValidationErrorMessage-'+$(this).attr('id')+'" class="errorMessage clear">'+settings.validateEmailAddress+'</p>');
												$(this).css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
												fieldObject[$(this).attr('id')] = false;
												errorFlag = true;
												break;
											}

										} else if (validationApplied[count] === 'PasswordLength') {
											if ($(this).val().length < 8 || $(this).val().length > 15) {			
												$(this).parent().append( '<p id="hideValidationErrorMessage-'+$(this).attr('id')+'" class="errorMessage clear">'+settings.validatePasswordLength+'</p>');
												$(this).css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
												fieldObject[$(this).attr('id')] = false;
												errorFlag = true;
												break;
											}
										} else if(validationApplied[count] === 'NotBlank') {
											if (!$(this).val()) {
												if(typeof $(this).attr('validateFieldBox') !== typeof undefined) {
													$("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#validateBox").replaceWith('<p id ="validateBox-'+$(this).attr('validateFieldBox')+'"></p>');

													if($("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#hideValidationErrorMessage-"+$(this).attr('id')).length == 0) {

														$("#validateBox-"+$(this).attr('validateFieldBox')).replaceWith('<p id="hideValidationErrorMessage-'+$(this).attr('id')+'" class="errorMessage clear">'+settings.validateNotBlank+' '+label+'.</p>');
														$(this).css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
													} else {

														$("#hideValidationErrorMessage-"+$(this).attr('id')).html(settings.validateNotBlank+' '+label+'.');
														$(this).css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
													}
													$('<p id ="validateBox-'+$(this).attr('validateFieldBox')+'"></p>').insertAfter("#hideValidationErrorMessage-"+$(this).attr('id'));
												} else {
													$(this).parent().append( '<p id="hideValidationErrorMessage-'+$(this).attr('id')+'" class="errorMessage clear">'+settings.validateNotBlank+' '+label+'.</p>');
													$(this).css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
												}
												fieldObject[$(this).attr('id')] = false;
												errorFlag = true;
												break;
											}
										} else if (validationApplied[count] === 'AlphaNumericWithPeriodSingleQuoteSpacesCommaSlashColonHyphenHash'){
											if (/^[\\s a-zA-Z0-9\\.'\\-\\s\\/:,#]*$/.test($(this).val()) == false) {
												var validateMessage ='';
												if(typeof $(this).attr("NameCheck") !== typeof undefined) {
													validateMessage = settings.AlphaNumericWithPeriodSingleQuoteSpacesCommaSlashColonHyphenHash.replace('{name}' ,$(this).attr("NameCheck")).replace('{name}' ,$(this).attr("NameCheck"));
												}
												$(this).parent().append( '<p id="hideValidationErrorMessage-'+$(this).attr('id')+'" class="errorMessage clear">'+validateMessage+'</p>');
												$(this).css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
												fieldObject[$(this).attr('id')] = false;
												errorFlag = true;
												break;
											}

										} else if(validationApplied[count] === 'LengthCheck') {
											var minLength = parseInt($(this).attr('LengthCheckMin'));
											var maxLength = parseInt($(this).attr('LengthCheckMax'));
											var validateMessage = '';
											if(minLength == maxLength) {
												validateMessage = settings.validateRequiredLength.replace('{requiredLength}',maxLength);
												if(typeof $(this).attr("NameCheck") !== typeof undefined) {
													validateMessage = settings.validateLength.replace('{name}' ,$(this).attr("NameCheck"));
												}
											} else {
												validateMessage = settings.validateLength.replace('(min)', $(this).attr('LengthCheckMin')).replace('(max)',$(this).attr('LengthCheckMax'));
												if(typeof $(this).attr("LengthCheckName") !== typeof undefined) {
													validateMessage = settings.validateLength.replace('{name}' ,$(this).attr("LengthCheckName"));
												}													

											}
											if($(this).val().length < minLength || $(this).val().length > maxLength ) {
												if(typeof $(this).attr('validateFieldBox') !== typeof undefined) {
													$("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#validateBox").replaceWith('<p id ="validateBox-'+$(this).attr('validateFieldBox')+'"></p>');

													if($("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#hideValidationErrorMessage-"+$(this).attr('id')).length == 0) {

														$("#validateBox-"+$(this).attr('validateFieldBox')).replaceWith('<p id="hideValidationErrorMessage-'+$(this).attr('id')+'" class="errorMessage clear">'+label+': '+validateMessage+'</p>');
														$(this).css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
													} else {

														$("#hideValidationErrorMessage-"+$(this).attr('id')).html(label+': '+validateMessage);
														$(this).css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
													}
													$('<p id ="validateBox-'+$(this).attr('validateFieldBox')+'"></p>').insertAfter("#hideValidationErrorMessage-"+$(this).attr('id'));
												} else {
													$(this).parent().append( '<p id="hideValidationErrorMessage-'+$(this).attr('id')+'" class="errorMessage clear">'+validateMessage+'</p>');
													$(this).css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
												}
												fieldObject[$(this).attr('id')] = false;
												errorFlag = true;
												break;
											}
										} else if(validationApplied[count] === 'PlentiOrWellnessCardNumber') {
											if($(this).val().length == 16 || $(this).val().length == 11 ||  $(this).val().length == 14 || $(this).val().length == 13) {
												if($(this).val().length == 11 && /^95/.test($(this).val()) == false) {
													$(this).parent().append( '<p id="hideValidationErrorMessage-'+$(this).attr('id')+'" class="errorMessage clear">'+settings.validateWellnessCardNumber+'</p>');
													$(this).css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
													fieldObject[$(this).attr('id')] = false;
													errorFlag = true;
													break;
												} else if($(this).val().length == 16 && !isPlentiCardValid($(this).val())) {
													$(this).parent().append( '<p id="hideValidationErrorMessage-'+$(this).attr('id')+'" class="errorMessage clear">'+settings.validatePlentiCardNumber+'</p>');
													$(this).css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
													fieldObject[$(this).attr('id')] = false;
													errorFlag = true;
													break;
												} else if(($(this).val().length == 11 || $(this).val().length == 13 || $(this).val().length == 14) && !validateWellnessCardNumber($(this).val())) {
													$(this).parent().append( '<p id="hideValidationErrorMessage-'+$(this).attr('id')+'" class="errorMessage clear">'+settings.validateWellnessCardNumber+'</p>');
													$(this).css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
													fieldObject[$(this).attr('id')] = false;
													errorFlag = true;
													break;
												}
											} else {
												if($(this).val() != '') {
													$(this).parent().append( '<p id="hideValidationErrorMessage-'+$(this).attr('id')+'" class="errorMessage clear">'+settings.validateCardNumber+'</p>');
													$(this).css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
													fieldObject[$(this).attr('id')] = false;
													errorFlag = true;
													break;
												}

											}
										} else if(validationApplied[count] === 'PlentiCardNumber') { 
											if($(this).val().length == 16) {
												if (!isPlentiCardValid($(this).val())) {
													$(this).parent().append( '<p id="hideValidationErrorMessage-'+$(this).attr('id')+'" class="errorMessage clear">'+settings.validatePlentiCardNumber+'</p>');
													$(this).css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
													fieldObject[$(this).attr('id')] = false;
													errorFlag = true;
													break;
												}
											} else {
												if($(this).val() != '') {
													$(this).parent().append( '<p id="hideValidationErrorMessage-'+$(this).attr('id')+'" class="errorMessage clear">'+settings.validatePlentiNumber+'</p>');
													$(this).css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
													fieldObject[$(this).attr('id')] = false;
													errorFlag = true;
													break;
												}

											}

										} else if (validationApplied[count] === 'WellnessCardNumber') {
											if($(this).val().length == 11 ||  $(this).val().length == 14 || $(this).val().length == 13) {
												if($(this).val().length == 11 && /^95/.test($(this).val()) == false) {
													$(this).parent().append( '<p id="hideValidationErrorMessage-'+$(this).attr('id')+'" class="errorMessage clear">'+settings.validateWellnessCardNumber+'</p>');
													$(this).css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
													fieldObject[$(this).attr('id')] = false;
													errorFlag = true;
													break;
												}  else if(($(this).val().length == 11 || $(this).val().length == 13 || $(this).val().length == 14) && !validateWellnessCardNumber($(this).val())) {
													$(this).parent().append( '<p id="hideValidationErrorMessage-'+$(this).attr('id')+'" class="errorMessage clear">'+settings.validateWellnessCardNumber+'</p>');
													$(this).css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
													fieldObject[$(this).attr('id')] = false;
													errorFlag = true;
													break;
												}
											} else {
												if($(this).val() != '') {
													$(this).parent().append( '<p id="hideValidationErrorMessage-'+$(this).attr('id')+'" class="errorMessage clear">'+settings.validateWellnessCardNumber+'</p>');
													$(this).css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
													fieldObject[$(this).attr('id')] = false;
													errorFlag = true;
													break;
												}

											}
										} else if (validationApplied[count] === 'MexicoPhoneNumber') {
											if($(this).val().length != 10) {
												$(this).parent().append( '<p id="hideValidationErrorMessage-'+$(this).attr('id')+'" class="errorMessage clear">'+settings.validatePhoneNumber+'</p>');
												$(this).css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
												fieldObject[$(this).attr('id')] = false;
												errorFlag = true;
												break;
											}
										} else if(validationApplied[count] === 'EqualToField') {
											var validateMessage = '';
											if(typeof $(this).attr("NameCheck") !== typeof undefined && $(this).attr("NameCheck") == 'password') {
												validateMessage = settings.validateEqualToField.replace('{name}' ,$(this).attr("NameCheck"));
											}
											if (typeof $(this).attr("NewFieldToMatch") !== typeof undefined && $("#hideValidationErrorMessage-"+$(this).attr("NewFieldToMatch")).html() === validateMessage) {
												$("#hideValidationErrorMessage-"+$(this).attr("NewFieldToMatch")).remove();
												$(this).removeAttr('style');
												fieldObject[$(this).attr("NewFieldToMatch")] = true;
											}
											if (((typeof $(this).attr("NewFieldToMatch") !== typeof undefined && !!$("#"+$(this).attr("NewFieldToMatch")).val() && !!$(this).val()) || (typeof $(this).attr("ConfirmFieldToMatch") !== typeof undefined && !!$("#"+$(this).attr("ConfirmFieldToMatch")).val() && !!$(this).val())) && ((typeof ($("#"+$(this).attr("ConfirmFieldToMatch")).val()) !== typeof undefined && $("#"+$(this).attr("ConfirmFieldToMatch")).val() != $(this).val()) || (typeof ($("#"+$(this).attr("NewFieldToMatch")).val()) !== typeof undefined && $("#"+$(this).attr("NewFieldToMatch")).val() != $(this).val()))) {

												if (typeof $(this).attr("NewFieldToMatch") === typeof undefined) {
													$("#hideValidationErrorMessage-"+$(this).attr("id")).remove();
													$(this).parent().append( '<p id="hideValidationErrorMessage-'+$(this).attr('id')+'" class="errorMessage clear">'+validateMessage+'</p>');
													$(this).css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
													fieldObject[$(this).attr('id')] = false;
												} else {
													$("#hideValidationErrorMessage-"+$(this).attr("NewFieldToMatch")).remove();
													$("#"+$(this).attr("NewFieldToMatch")).parent().append( '<p id="hideValidationErrorMessage-'+$(this).attr("NewFieldToMatch")+'" class="errorMessage clear">'+validateMessage+'</p>');
													$("#"+$(this).attr("NewFieldToMatch")).css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
													fieldObject[$(this).attr("NewFieldToMatch")] = false;
												}
												errorFlag = true;
												break;								
											}	
										} 
									} else {

										if ($(this).attr("id") == $("[validatePhoneNumber='areaCode']").attr("id") || $(this).attr("id") == $("[validatePhoneNumber='firstThree']").attr("id") ||  $(this).attr("id") == $("[validatePhoneNumber='lastFour']").attr("id")) {
											fieldPhoneNumberObject["bluredId"] = $(this).attr("id");
											fieldPhoneNumberObject["bluredValidateField"] = $(this).attr('validateFieldBox');
										} else if(($(this).attr("id") == $("[validatedate='MM']").attr("id") || $(this).attr("id") == $("[validatedate='DD']").attr("id") ||  $(this).attr("id") == $("[validatedate='YYYY']").attr("id"))){
											fieldDateObject["bluredId"] = $(this).attr("id");
											fieldDateObject["bluredValidateField"] = $(this).attr('validateFieldBox');

										}


									}
								}
								if(!errorFlag) {
									$('#hideValidationErrorMessage-'+$(this).attr('id')).remove();
									fieldObject[$(this).attr('id')] = true;
									if ($(this).attr("id") == $("[validatePhoneNumber='areaCode']").attr("id") || $(this).attr("id") == $("[validatePhoneNumber='firstThree']").attr("id") ||  $(this).attr("id") == $("[validatePhoneNumber='lastFour']").attr("id")) {
										if($("[validatePhoneNumber='areaCode']").val() == '' && $("[validatePhoneNumber='firstThree']").val() == ''&&  $("[validatePhoneNumber='lastFour']").val() == '') {
											if(typeof $(this).attr("phoneOptional") != typeof undefined ) {
												if($("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#validateBox").length == 0) {
													$('<p id ="validateBox"></p>').insertAfter("#hideValidationErrorMessage-PhoneNumberCheck");	
												}
												$("[findErrorMessageForPhoneNumber='validateBox']").find("#hideValidationErrorMessage-PhoneNumberCheck").remove();
												$("["+$(this).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"","box-shadow":"","border": ""});
												fieldObject[$("[findErrorMessageForPhoneNumber='validateBox']").attr("id")] = true;
												removeMessageFlag = true;
											}
										}
									}

								}

							} else {
								fieldObject[$(this).attr('id')] = true;
								$("[findErrorMessageForZipCode='validateBox']").find("#hideValidationErrorMessage-"+$(this).attr('id')).remove();
								$("[findErrorMessageForPhoneNumber='validateBox']").find("#hideValidationErrorMessage-"+$(this).attr('id')).remove();
								$("[findErrorMessageForDate='validateBox']").find("#hideValidationErrorMessage-"+$(this).attr('id')).remove();

							}
						} catch(err) {		
						}
						$.each(fieldObject, function (index, value) {
							if(!value) {
								enableSubmit = false;
								return false;
							} else {
								enableSubmit = true;
							}
						});
						if(enableSubmit == false && settings.enableButton == 'false' ) {
							$("[validationForInlineSubmit='disable-submit-button']").css({"background":"none repeat scroll 0 0 #d9d9d9","color":"#999", "cursor":"default"});
							$("[validationForInlineSubmit='disable-submit-button']").attr("disableButton","true");
							//$("[validationForInlineSubmit='disable-submit-button']").attr("disabled","disabled");	
						} else {
							$("[validationForInlineSubmit='disable-submit-button']").removeAttr('style');
							$("[validationForInlineSubmit='disable-submit-button']").attr("disableButton","false"); 
						}
						$("#hideErrorMessage").val('false');
					});
					$(this).bind("focus", function() {
						if (($(this).attr("id") != $("[validatePhoneNumber='areaCode']").attr("id") && $(this).attr("id") != $("[validatePhoneNumber='firstThree']").attr("id") &&  $(this).attr("id") != $("[validatePhoneNumber='lastFour']").attr("id")) || (typeof $(this).attr("id") === typeof undefined && fieldPhoneNumberObject["bluredValidateField"] != '')) {
							if($("[validatePhoneNumber='areaCode']").val() == '' || $("[validatePhoneNumber='firstThree']").val() == '' || $("[validatePhoneNumber='lastFour']").val() == '') {
								if(typeof $("#"+fieldPhoneNumberObject['bluredId']).attr("phoneOptional") == typeof undefined ) {
									if(typeof fieldPhoneNumberObject["bluredValidateField"] !== typeof undefined && fieldPhoneNumberObject["bluredValidateField"] != '') {
										$("["+fieldPhoneNumberObject["bluredValidateField"]+"='validateBox']").find("#validateBox").replaceWith('<p id ="validateBox-'+fieldPhoneNumberObject["bluredValidateField"]+'"></p>');
										if($("["+fieldPhoneNumberObject["bluredValidateField"]+"='validateBox']").find("#hideValidationErrorMessage-PhoneNumberCheck").length == 0) {
											$("#validateBox-"+fieldPhoneNumberObject["bluredValidateField"]).replaceWith('<p id="hideValidationErrorMessage-PhoneNumberCheck" class="errorMessage clear">'+settings.validateNotBlankForPhone+'</p>');
											$("["+fieldPhoneNumberObject["bluredValidateField"]+"='validateBox'] input[id][name]").css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
										} else {
											$("#hideValidationErrorMessage-PhoneNumberCheck").html(settings.validateNotBlankForPhone);
											$("[findErrorMessageForPhoneNumber='validateBox'] input[id][name]").css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
										}
										fieldObject[$("[findErrorMessageForPhoneNumber='validateBox']").attr("id")] = false;
									}

									errorFlag = true;
								} else if (!($("[validatePhoneNumber='areaCode']").val() == '' && $("[validatePhoneNumber='firstThree']").val() == ''&&  $("[validatePhoneNumber='lastFour']").val() == '')) {
									if(typeof $("#"+fieldPhoneNumberObject['bluredId']).attr('validateFieldBox') !== typeof undefined) {
										$("["+$("#"+fieldPhoneNumberObject['bluredId']).attr('validateFieldBox')+"='validateBox']").find("#validateBox").replaceWith('<p id ="validateBox-'+$(this).attr('validateFieldBox')+'"></p>');
										if($("["+$("#"+fieldPhoneNumberObject['bluredId']).attr('validateFieldBox')+"='validateBox']").find("#hideValidationErrorMessage-PhoneNumberCheck").length == 0) {
											$("#validateBox-"+$("#"+fieldPhoneNumberObject['bluredId']).attr('validateFieldBox')).replaceWith('<p id="hideValidationErrorMessage-PhoneNumberCheck" class="errorMessage clear">'+settings.validateNotBlankForPhone+'</p>');
											$("["+$("#"+fieldPhoneNumberObject['bluredId']).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
										} else {
											$("#hideValidationErrorMessage-PhoneNumberCheck").html(settings.validateNotBlankForPhone);
											$("["+$("#"+fieldPhoneNumberObject['bluredId']).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
										}
									}
									fieldObject[$("[findErrorMessageForPhoneNumber='validateBox']").attr("id")] = false;
									errorFlag = true;
									phoneCheckBlankFlag = true;
								}
							} else {
								if(!phoneCheckBlankFlag) {
									if($("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#validateBox").length == 0) {
										$('<p id ="validateBox"></p>').insertAfter("#hideValidationErrorMessage-PhoneNumberCheck");	
									}
									$("[findErrorMessageForPhoneNumber='validateBox']").find("#hideValidationErrorMessage-PhoneNumberCheck").remove();
									$("["+$(this).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"","box-shadow":"","border": ""});
									fieldObject[$("[findErrorMessageForPhoneNumber='validateBox']").attr("id")] = true;
								}
							}
						} else if (fieldPhoneNumberObject["bluredId"] != '' && ($(this).attr("id") == $("[validatePhoneNumber='areaCode']").attr("id") || $(this).attr("id") == $("[validatePhoneNumber='firstThree']").attr("id") ||  $(this).attr("id") == $("[validatePhoneNumber='lastFour']").attr("id"))) {
							if (blurCheckFlag) {
								removeMessageFlag = true;
								blurCheckFlag = false;
							} else {
								removeMessageFlag = false;
							}
							if (removeMessageFlag) {
								if($("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#validateBox").length == 0) {
									$('<p id ="validateBox"></p>').insertAfter("#hideValidationErrorMessage-PhoneNumberCheck");	
								}
								$("[findErrorMessageForPhoneNumber='validateBox']").find("#hideValidationErrorMessage-PhoneNumberCheck").remove();
								$("["+$(this).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"","box-shadow":"","border": ""});
								fieldObject[$("[findErrorMessageForPhoneNumber='validateBox']").attr("id")] = true;
							}
							removeMessageFlag = false;
						}
						fieldPhoneNumberObject["bluredValidateField"] = '';
						fieldPhoneNumberObject["bluredId"] = '';

						if (($(this).attr("id") != $("[validatedate='MM']").attr("id") && $(this).attr("id") != $("[validatedate='DD']").attr("id") &&  $(this).attr("id") != $("[validatedate='YYYY']").attr("id"))) {
							if ($("[validatedate='MM']").val() == '') {
								$("[validatedate='MM']").val('MM');
							}
							if($("[validatedate='DD']").val() == '') {
								$("[validatedate='DD']").val('DD');
							}
							if($("[validatedate='YYYY']").val() == '') {
								$("[validatedate='YYYY']").val('YYYY')
							}
							if($("[validatedate='MM']").val() == 'MM' || $("[validatedate='DD']").val() == 'DD' || $("[validatedate='YYYY']").val() == 'YYYY') {
								if(typeof fieldDateObject["bluredValidateField"] !== typeof undefined && fieldDateObject["bluredValidateField"] != '') {
									$("["+fieldDateObject["bluredValidateField"]+"='validateBox']").find("#validateBox").replaceWith('<p id ="validateBox-'+fieldDateObject["bluredValidateField"]+'"></p>');
									if($("["+fieldDateObject["bluredValidateField"]+"='validateBox']").find("#hideValidationErrorMessage-dateOfBirth").length == 0) {
										$("#validateBox-"+fieldDateObject["bluredValidateField"]).replaceWith('<p id="hideValidationErrorMessage-dateOfBirth" class="errorMessage clear">'+settings.validateDateBlank+'</p>');
										$("["+fieldDateObject["bluredValidateField"]+"='validateBox'] input[id][name]").css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
									} else {
										$("#hideValidationErrorMessage-dateOfBirth").html(settings.validateDateBlank);
										$("[findErrorMessageForDate='validateBox'] input[id][name]").css({"background-color":"#ffcdd0","box-shadow":"none","border": "1px solid red"});
									}
								}
								errorFlag = true;

							} else {
								if(!datecheckBlankFlag) {
									$("[findErrorMessageForDate='validateBox'] input[id][name]").css({"background-color":"","box-shadow":"","border": ""});
									if($("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#validateBox").length == 0) {
										$('<p id ="validateBox"></p>').insertAfter("#hideValidationErrorMessage-dateOfBirth");	
									}
									$("[findErrorMessageForDate='validateBox']").find("#hideValidationErrorMessage-dateOfBirth").remove();
								}
							}
						} else if (fieldDateObject["bluredId"] != '' && ($(this).attr("id") == $("[validatedate='MM']").attr("id") || $(this).attr("id") == $("[validatedate='DD']").attr("id") ||  $(this).attr("id") == $("[validatedate='YYYY']").attr("id"))) {
							if (blurCheckFlag) {
								removeMessageFlag = true;
								blurCheckFlag = false;
							} else {
								removeMessageFlag = false;
							}
							if (removeMessageFlag) {
								if($("["+$(this).attr('validateFieldBox')+"='validateBox']").find("#validateBox").length == 0) {
									$('<p id ="validateBox"></p>').insertAfter("[findErrorMessageForDate='validateBox'] #hideValidationErrorMessage-dateOfBirth");	
								}
								$("[findErrorMessageForDate='validateBox']").find("#hideValidationErrorMessage-dateOfBirth").remove();
								$("["+$(this).attr('validateFieldBox')+"='validateBox'] input[id][name]").css({"background-color":"","box-shadow":"","border": ""});
								fieldObject[$("[findErrorMessageForDate='validateBox']").attr("id")] = true;
							}
							removeMessageFlag = false;
						}
						fieldDateObject["bluredValidateField"] = '';
						fieldDateObject["bluredId"] = '';
					});					
					$(this).bind("change", changeHandler);					
				}
			}
		});	

		function disableFunction() {
			return false;
		}
		function validatePhoneNumber(phoneAreaCode, phoneNumber) {
			var validAreaCode = true;
			var validPhoneNumber = true;

			if (phoneAreaCode % 111 == 0 && phoneAreaCode != 888)
				validAreaCode = false;

			if ((phoneNumber % 1111111) == 0 || phoneNumber == 5551212)
				validPhoneNumber = false;

			if (validAreaCode && validPhoneNumber) {
				return true;
			} else {
				return false;
			}
		}
	};	
})(jQuery);



