function validateWellnessCardNumber(cardNumber) {
	/*Following validations rules are to be applied to wellness card entered by the user
	 * 1) Remove spaces from the card number if it exists
	 * 2) Remove MK from the card number if it exists
	 * 3) For normal wellness card number
	 * 			1) Should be numeric
	 * 			2) Should start with 95 
	 * 			3) Should be 11 digit long
	 * 			4) Should satisfy checkDigit logic
	 * 3) For Associate card number
	 * 			1)Should be numeric  
	 * 			2) Should start with 99
	 * 			3) Should be 11 digit long after removing last 3 characters
	 */ 


	var cardNumberWithPrefix = cardNumber.toString();
	var cardNumber = "";
	//Remove spaces
	cardNumberWithPrefix = cardNumberWithPrefix.replace(" ", "");

	//Remove MK
	if (/^MK/.test(cardNumberWithPrefix) == true) {
		cardNumber = cardNumberWithPrefix.substring(2, cardNumberWithPrefix.length);
	} else {
		cardNumber = cardNumberWithPrefix;
	}

	return isValidAssociateCardNumber(cardNumber) || isValidWellnessCardNumber(cardNumber);
}

function isValidWellnessCardNumber(cardNumber) {
	var sum = 0;
	var added = 0;
	var validCheckDigit = false;
	var doubleFlag = false;

	if (cardNumber.length !== 11 || cardNumber.lastIndexOf("95", 0) !== 0) {
		return false;
	}
	
	for (var index = cardNumber.length - 1; index >= 0; index--) {
		if (doubleFlag) {
			added = (parseInt(cardNumber.substring(index, index + 1), 10)) << 1;
			if (added > 9)
				added -= 9;
		} else {
			added = parseInt(cardNumber.substring(index, index + 1), 10);
		}
		sum += added;
		doubleFlag = !doubleFlag;
	}

	if (sum % 10 == 0) {
		validCheckDigit = true;
	}
	if (validCheckDigit) {
		return validCheckDigit;
	} else if (isValidCheckDigitModUPC(cardNumber)== true) {
		//validate check digit with MOD UPC
		return  true;
	} else {
		return isValidCheckDigitForWellnessCardWithMK(cardNumber);
	}
}

function isValidCheckDigitModUPC(cardNumber) {
	var sum = 0;
	var added = 0;
	var validCheckDigit = false;
	var doubleFlag = false;

	for (var index = cardNumber.length - 1; index >= 0; index--) {
		if (doubleFlag) {
			added = (parseInt(cardNumber.substring(index, index + 1), 10)) * 3;

		} else {
			added = parseInt(cardNumber.substring(index, index + 1), 10);
		}
		sum += added;
		doubleFlag = !doubleFlag;
	}

	if (sum % 10 == 0) {
		validCheckDigit = true;
	}
	return validCheckDigit;
}

function isValidCheckDigitForWellnessCardWithMK(cardNumber) {

	var validCheckDigit = false;
	var doubleFlag = true;
	var temporaryString = "";
	var reversedCardNumber = "";
	var unitsDigit = 0;
	var tensDigit = 0;
	var oddSum = 0;
	var evenSum = 0;
	var added = 0;
	var totalSum = 0;
	var checkDigit = parseInt(cardNumber.substring(cardNumber.length - 1, cardNumber.length) ,10);
	var cardNumberArray = cardNumber.split("");
	cardNumberArray.reverse();
	reversedCardNumber = cardNumberArray.join("");
	reversedCardNumber = reversedCardNumber.slice(1 ,reversedCardNumber.length);
	for (var index = 0; index <= reversedCardNumber.length- 1; index++) {
		if (doubleFlag) {
			added = (parseInt(reversedCardNumber.substring(index, index + 1) ,10)) * 2;
			if (added > 9) {
				temporaryString  = added.toString();
				unitsDigit = parseInt(temporaryString.substring(0, 1) ,10);
				tensDigit = parseInt(temporaryString.substring(1, 2) ,10);
				added =  unitsDigit + tensDigit;
			}
			oddSum += added;
		} else {
			added = parseInt(reversedCardNumber.substring(index, index + 1) ,10);
			evenSum += added;
		}
		doubleFlag = !doubleFlag;
	}

	if (!((cardNumber.length % 2) == 0)) {
		oddSum += 4;
		evenSum += 22;
	} else {
		oddSum += 8;
		evenSum += 20;
	}
	totalSum = oddSum + evenSum + checkDigit;

	if (totalSum % 10 == 0) {
		validCheckDigit = true;
	}
	return validCheckDigit;
}

function isValidAssociateCardNumber(cardnumber) {
	if(cardnumber.length == 14 && /^99/.test(cardnumber)&& /^[0-9]*$/.test(cardnumber) == true) {
		return true;
	} else {
		return false;
	}
}
