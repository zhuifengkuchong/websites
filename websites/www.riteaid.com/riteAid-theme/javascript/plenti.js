/**
	This function is used to run Luhn Algorithm to check if card number valid
	Steps:
		 1)Double the alternate digits starting from the first digit.
		   If the value is bigger than 9 , substract by 9 so each value is less than 10.		 
		 2)Add all the non-doubled digits from the credit card number
		 3)Add the values calculated in step 1 and step 2 together
		 4)calculate the remainder when it is divided by 10.
		   If the remainder is zero, then it's a valid number, otherwise it's invalid.
**/
function isPlentiCardValid(cardNumber)
{
	
	if (cardNumber.lastIndexOf("310417", 0) !== 0) {
		return false;
	}

	//alert("calling isValidCard()");
	var ccard = new Array(cardNumber.length);
	var i     = 0;
    var sum   = 0;

	if (cardNumber.length != 16) {
		return false;
	} 
	//Init Array with Credit Card Number
	for(i = 0; i < cardNumber.length; i++){
		ccard[i] = parseInt(cardNumber.charAt(i));
	}	
	//add all the non-doubled digits from the credit card number	
	for(i = 0; i < cardNumber.length; i = i+2){
		ccard[i] = ccard[i] * 2;	
		//If the value is bigger than 9 , substract by 9		
		if(ccard[i] > 9){
			ccard[i] = ccard[i] - 9;
		}
	}
	
	for(i = 0; i < cardNumber.length; i++){
	   sum = sum + ccard[i];
	}	
	//calculate the remainder when it is divided by 10. 
	//If the remainder is zero, then it's a valid number, otherwise	it's invalid.
	return ((sum%10) == 0);
}