// version 1.0.0a

(function($) {

	var objId = "";

	function ShowTooltip(element, options) {
		this.$element = $(element);
		this.options = options;
		this.enabled = true;
	}
	ShowTooltip.prototype = {

		show : function(obj, options) {
			objId = obj.id;
			var toolTipWidth = $('#' + objId + '>.showTooltip').width();
			var objWidth = $(obj).innerWidth();
			var objContantWidth = $(obj).width();

			$('#' + objId).css('position', 'relative');
			var toolTipHeight = $('#' + objId + '>.showTooltip').outerHeight();
			$('#' + objId + '>.showTooltip').css('margin-top',
					-(toolTipHeight + 30));

			var toolTipheight = 0;
			if (options.border) {
				$('#' + objId + '>.showTooltip').css('border',
						'1px solid #cccccc');
				toolTipheight = $('#' + objId + '>.showTooltip').outerHeight();
				$('#' + objId + '>.showTooltip').css('display', 'block');
				$('#' + objId + '>.showTooltip' + '>.toolTipArrow').css(
						'margin-top', (toolTipheight - 7));
			} else {
				toolTipheight = $('#' + objId + '>.showTooltip').outerHeight();
				$('#' + objId + '>.showTooltip').css('display', 'block');
				$('#' + objId + '>.showTooltip' + '>.toolTipArrow').css(
						'margin-top', (toolTipheight - 5));
			}

			var actualWidth = $('#' + objId + '>.showTooltip').innerWidth(), actualHeight = $(
					'#' + objId + '>.showTooltip').innerHeight();

			$('#' + objId + '>.showTooltip').css('margin-left',
					-((actualWidth / 2) - (objWidth / 2)) + options.offsetX);
			$('#' + objId + ' > .showTooltip > .toolTipArrow').css(
					'margin-left', ((actualWidth / 2) - 10));
		},

		hide : function() {
			$('#' + objId + '>.showTooltip').css('display', 'none');
		}
	}

	$.fn.showTooltip = function(options) {

		if (options === true) {
			return this.data('showTooltip');
		} else if (typeof options == 'string') {
			return this.data('showTooltip')[options]();
		}

		options = $.extend( {}, $.fn.showTooltip.defaults, options);

		function get(ele) {
			var showTooltip = $.data(ele, 'showTooltip');
			if (!showTooltip) {
				showTooltip = new ShowTooltip(ele, $.fn.showTooltip
						.elementOptions(ele, options));
				$.data(ele, 'showTooltip', showTooltip);
			}
			return showTooltip;
		}

		function enter() {

			var showTooltip = get(this);
			if (options.delayIn == 0) {
				showTooltip.show(this, options);
			} else {
				setTimeout(function() {
					showTooltip.show(this, options);
				}, options.delayIn);
			}
		}
		;

		function leave() {
			var showTooltip = $('.toolTipBG');
			if (options.delayOut == 0) {
				showTooltip.hide();
			} else {
				setTimeout(function() {
					showTooltip.hide();
				}, options.delayOut);
			}
		}
		;

		if (!options.live)
			this.each(function() {
				get(this);
			});

		if (options.trigger != 'manual') {
			var binder = options.live ? 'live' : 'bind', eventIn = options.trigger == 'click' ? 'click'
					: 'focus', eventOut = options.trigger == 'click' ? 'blur'
					: 'blur', eventIn = options.trigger == 'hover' ? 'mouseenter'
					: 'focus'
			eventOut = options.trigger == 'hover' ? 'mouseleave' : 'blur';
			this[binder](eventIn, enter)[binder](eventOut, leave);
		}
	}

	$.fn.showTooltip.defaults = {
		delayIn : 0,
		delayOut : 0,
		offsetX : 0,
		border : true,
		trigger : 'click'
	};

	$.fn.showTooltip.elementOptions = function(ele, options) {
		return $.metadata ? $.extend( {}, options, $(ele).metadata()) : options;
	};

})(jQuery);