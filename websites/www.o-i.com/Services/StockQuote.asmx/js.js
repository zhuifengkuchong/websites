Type.registerNamespace('OI.Services');
OI.Services.StockQuote=function() {
OI.Services.StockQuote.initializeBase(this);
this._timeout = 0;
this._userContext = null;
this._succeeded = null;
this._failed = null;
}
OI.Services.StockQuote.prototype={
_get_path:function() {
 var p = this.get_path();
 if (p) return p;
 else return OI.Services.StockQuote._staticInstance.get_path();},
GetStockQuote:function(succeededCallback, failedCallback, userContext) {
return this._invoke(this._get_path(), 'GetStockQuote',false,{},succeededCallback,failedCallback,userContext); },
GetStockQuoteDetails:function(succeededCallback, failedCallback, userContext) {
return this._invoke(this._get_path(), 'GetStockQuoteDetails',false,{},succeededCallback,failedCallback,userContext); }}
OI.Services.StockQuote.registerClass('OI.Services.StockQuote',Sys.Net.WebServiceProxy);
OI.Services.StockQuote._staticInstance = new OI.Services.StockQuote();
OI.Services.StockQuote.set_path = function(value) { OI.Services.StockQuote._staticInstance.set_path(value); }
OI.Services.StockQuote.get_path = function() { return OI.Services.StockQuote._staticInstance.get_path(); }
OI.Services.StockQuote.set_timeout = function(value) { OI.Services.StockQuote._staticInstance.set_timeout(value); }
OI.Services.StockQuote.get_timeout = function() { return OI.Services.StockQuote._staticInstance.get_timeout(); }
OI.Services.StockQuote.set_defaultUserContext = function(value) { OI.Services.StockQuote._staticInstance.set_defaultUserContext(value); }
OI.Services.StockQuote.get_defaultUserContext = function() { return OI.Services.StockQuote._staticInstance.get_defaultUserContext(); }
OI.Services.StockQuote.set_defaultSucceededCallback = function(value) { OI.Services.StockQuote._staticInstance.set_defaultSucceededCallback(value); }
OI.Services.StockQuote.get_defaultSucceededCallback = function() { return OI.Services.StockQuote._staticInstance.get_defaultSucceededCallback(); }
OI.Services.StockQuote.set_defaultFailedCallback = function(value) { OI.Services.StockQuote._staticInstance.set_defaultFailedCallback(value); }
OI.Services.StockQuote.get_defaultFailedCallback = function() { return OI.Services.StockQuote._staticInstance.get_defaultFailedCallback(); }
OI.Services.StockQuote.set_enableJsonp = function(value) { OI.Services.StockQuote._staticInstance.set_enableJsonp(value); }
OI.Services.StockQuote.get_enableJsonp = function() { return OI.Services.StockQuote._staticInstance.get_enableJsonp(); }
OI.Services.StockQuote.set_jsonpCallbackParameter = function(value) { OI.Services.StockQuote._staticInstance.set_jsonpCallbackParameter(value); }
OI.Services.StockQuote.get_jsonpCallbackParameter = function() { return OI.Services.StockQuote._staticInstance.get_jsonpCallbackParameter(); }
OI.Services.StockQuote.set_path("http://www.o-i.com/Services/StockQuote.asmx");
OI.Services.StockQuote.GetStockQuote= function(onSuccess,onFailed,userContext) {OI.Services.StockQuote._staticInstance.GetStockQuote(onSuccess,onFailed,userContext); }
OI.Services.StockQuote.GetStockQuoteDetails= function(onSuccess,onFailed,userContext) {OI.Services.StockQuote._staticInstance.GetStockQuoteDetails(onSuccess,onFailed,userContext); }
var gtc = Sys.Net.WebServiceProxy._generateTypedConstructor;
if (typeof(OI.Services.StockQuoteStub) === 'undefined') {
OI.Services.StockQuoteStub=gtc("OI.Services.StockQuoteStub");
OI.Services.StockQuoteStub.registerClass('OI.Services.StockQuoteStub');
}
