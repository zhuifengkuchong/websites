

/***********************************************
 
	1) HELPER VARS
	2) OST DOM-LOAD CONSTANTS & FUNCTION CALLS
	3) GLOBAL FUNCTIONS
	4) PAGE & ELEMENT SPECIFIC FUNCTIONS
	5) USER AGENT SPECIFIC SCRIPTING

***********************************************/
		
			
	
	
	
	
	
	
	/* ===== HELPER VARS ===================================================================================== */
	
			var pgHash = location.hash;
			var pgHost = location.host;
			var pgHref = location.href;
			var pgPath = location.pathname;
			var pgTitle = document.title;
			var cache = [];
			var agent = navigator.userAgent.toLowerCase();
			var isIphone = ((agent.indexOf('iphone') != -1));
			var isIpad = ((agent.indexOf('ipad') != -1));
			var isAndroid = ((agent.indexOf('android') != -1));
			var origHeight;
			var isHome = ($('#home-promos').length > 0);
			var isWhyGlass = ($(".why-glass").length > 0);
			var theWindow = $(window);


	
	
	/* ===== POST DOM-LOAD ============================================================================ */


			$(document).ready(function () {


			    emptyAnchors();
			    inputLabelToggle();

			    var pageBkg = $('#page-bkg');
			    var isMapHover = $('#maphover-main');

			    /* -- LOGIC TO CHECK FOR PAGEBKG HIDDEN INPUT (THEN SET BKG) OR HOMEPAGE (THEN RUN HOMEPAGE FUNCTIONALITY) -- */

			    if (isHome) {


			        var homePromosCount = $('#home-promos ul li').length;
			        var homePromosWidth = homePromosCount * 95;
			        var promoId = 1;

			        $('#home-promos ul').css('width', homePromosWidth);
			        $('#page-content').append('<div id="home-heros"></div>');

			        $('#home-promos li').each(function () {

			            var $promo = $(this),
									promoClass = $promo.attr('rel');
			            promoHtml = $('div', $promo).html();
			            promoBkg = $('div', $promo).attr('rel');
			            preload(promoBkg);

			            $('#home-heros').append('<div class="home-hero-' + promoClass + '" rel="hero' + promoId + '">' + promoHtml + '</div>');

			            promoId++;

			        });


			        $('#home-promos ul li a').live('click', function () {
			            if ($(this).hasClass('current')) { }
			            else {
			                $('#home-promos .current span').hide();
			                $('#home-promos .current').removeClass('current');
			                $(this).addClass('current');
			                var getHeroBkg = $(this).next().attr('rel');
			                var getHeroId = $(this).attr('id');
			                $('#home-heros div').fadeOut(1000);
			                $('div[rel="' + getHeroId + '"]').fadeIn(1000);

			                if ($('#page-bkg').is(":visible")) {
			                    $('#page-bkg').fadeOut(500, function () {
			                        // Set page BG in fade function
			                        setPageBkg(getHeroBkg);
			                        $('#page-bkg').fadeIn(500);
			                    });
			                }
			                else {
			                    $('.bkgImg').fadeOut(500, function () {
			                        // Set page BG in fade function
			                        setPageBkg(getHeroBkg);
			                        $('.bkgImg').fadeIn(500);
			                    });
			                }
			            }
			            return false;
			        });

			        // Auto Slide Function
			        jQuery.fn.timer = function () {
			            if ($('.current').parent().next().children(":first").length == 0) {
			                $('#home-promos-content a:first').trigger('click');
			            } else {
			                $('.current').parent().next().children(":first").trigger('click');
			            }
			        }

			        // Init Auto Slide
			        var autoslide;
			        function startAutoSlide() {
			            autoslide = window.setInterval(function () {
			                $("#home-promos").timer();
			            }, 10000);
			        }

			        function stopAutoSlide() {
			            window.clearInterval(autoslide);
			        }

			        // Begin Auto Slide
			        startAutoSlide();

			        $('#home-promos').hover(
								function () {
								    $('#home-promos').stop().animate({ height: 110 }, 500);
								    // Stop Auto Slide on hover
								    stopAutoSlide();
								},
								function () {
								    $('#home-promos').stop().animate({ height: 50 }, 500);
								    // Restart Auto Slide 
								    startAutoSlide();
								});

			        $('#home-promos li a').hover(
								function () {
								    $('span', this).show();
								},
								function () {
								    $('span', this).hide();
								});

			        $('#home-promos li a:first').click();

			    }



			    /* -- MAP HOVER -- */

			    if (isMapHover.length > 0) {

			        $('area.[class^="maphover-"]').hover(
							function () {
							    var getClass = $(this).attr('class');
							    $('.maphover').attr('id', getClass);
							},
							function () {
							    $('.maphover').attr('id', 'maphover-main');
							}
						);

			    }


			    /* -- MAIN NAV SLIDE OUT -- */

			    $('#nav-main li.mainnav-off').hover(
						function () {
						    $('a:first', this).addClass('current').next().show().animate({ opacity: 1, width: 200 }, 150);
						    $('#footer-language, #footer-sitemap').slideUp();
						},
						function () {
						    $('a:first', this).removeClass('current').next().animate({ opacity: 0, width: 0 }, 150).hide();
						}
					);

			    $('#nav-main li.mainnav-off ul').hover(
						function () {
						    $(this).show();
						},
						function () {
						    $(this).hide();
						}
					);


			    /* -- FOOTER SLIDE UP -- */

			    $('#anchor-sitemap').live('click', function () {
			        $('#footer-language').hide();
			        $('#footer-sitemap').slideToggle(500);
			        return false;
			    });

			    $('#anchor-language').live('click', function () {
			        $('#footer-sitemap').hide();
			        $('#footer-language').slideToggle(500);
			        return false;
			    });


			    /* -- STATIC INCLUDES -- */

			    $('.static-include').each(function () {
			        var getLoad = $(this).attr('value')
			        $(this).parent().load(getLoad);
			    });




			    /* ====  REGION DETAILS TOGGLE  ====  */

			    $('.country ul li.city').live('click', function (e) {
			        $(this).toggleClass('on');
			        $(this).children('.details').toggle();

			        e.preventDefault();
			    });


			    /* -- METHOD TO RECEIVE IMG PATH AND PREPEND IMAGE TO PAGE FOR USE AS A FULL SIZED BACKGROUND -- */

			    function setPageBkg(pagebkg) {

			        $('#page-bkg').removeAttr("style");
			        $('#page-bkg').attr("style", "background-image: url(" + pagebkg + ");");
			        $('.bkgImg').attr("src", pagebkg);

			    }


			    //why glass

			    /* SLIDER CLICK EVENT */
			    $('#ui-vertaccordian .slider').live("click", function () {

			        // Assign current slider
			        $('.slider').removeClass('current-slider').css('display', '');
			        $(this).addClass('current-slider');
			        var accordianTextMargin = theWindow.height() / 2;
			        $('.slider-text').css('color', '#a6d8e5');
			        $(this).children('.slider-text').css('color', '#FFFFFF');

			        // Assign Vars
			        var newBG = $(this).find("img").attr("src");
			        var currentSlider = $(this).find("img").attr("src");
			        var currentCopy = $(this).attr("id");

			        /* BACKGROUND FADE */
			        if ($('#page-bkg').is(":visible")) {
			            $('#page-bkg').fadeOut(500, function () {
			                $('#page-bkg').attr("style", "background-image: url(" + newBG + ");");
			                $('#page-bkg').fadeIn(500);
			            });
			        }
			        else {
			            $('.bkgImg').fadeOut(500, function () {
			                $('.bkgImg').attr("src", newBG);
			                $('.bkgImg').fadeIn(500);
			            });
			        }

			        //Show appropriate copy
			        $('.slide-copy').css('display', 'none');
			        $('span[rel=' + currentCopy + ']').css('display', 'block');
			        $('span[rel=' + currentCopy + ']').hide();
			        $('span[rel=' + currentCopy + ']').fadeIn(1000);

			    });

			    // Next Slide Arrow
			    $("#next-slide").live("click", function () {
			        if ($('.current-slider').next().length == 0) {
			            $('.slider:first').trigger('click');
			        } else {
			            $('.current-slider').next().trigger('click');
			        }
			    });

			    // Next Slide Link
			    $('.next-link').live("click", function () {
			        if ($('.current-slider').next().length == 0) {
			            $('.slider:first').trigger('click');
			        } else {
			            $('.current-slider').next().trigger('click');
			        }
			    });

			    // Slide Hover
			    $('#ui-vertaccordian .slider').hover(
	                  function () {
	                      $(this).children('.slider-text').css('color', '#FFFFFF');
	                  },
	                  function () {
	                      if (!($(this).hasClass("current-slider"))) {
	                          $(this).children('.slider-text').css('color', '#a6d8e5');
	                      }
	                  }
	                );

			    //Slide Copy
			    $('.slide-copy:first').show();

			    //ensure content is at least as tall as page
			    origHeight = $("#subpage-content").height();
			    ensureContentHeight();

			    theWindow.resize(onResize).resize();

			    /* -- REGION SELECTOR -- */
			    $('#select-region-btn').click(function () {
			        $(this).siblings('#select-region-list').fadeIn();
			    })
			    $("#select-region-list").mouseout(function (evt) {
			        if (this == evt.relatedTarget || $(this).find(evt.relatedTarget).length > 0)
			            return;
			        $(this).siblings('#select-region-btn').fadeIn();
			        $(this).fadeOut();
			    });
			    $("#select-language-list").mouseout(function (evt) {
			        if (this == evt.relatedTarget || $(this).find(evt.relatedTarget).length > 0)
			            return;
			        $(this).siblings('#select-language-btn').fadeIn();
			        $(this).fadeOut();
			    });

			    $('#select-region-list li').click(function () {
			        $('#select-region-btn').empty()
			        $(this).clone().appendTo('#select-region-btn');
			        $('#select-region-list').fadeOut();
			        $('#select-region-btn').fadeIn();
			        $('#select-language-btn').removeClass("off")
                        .click(function () {
                            $(this).siblings('#select-language-list').fadeIn();
                        }); ;
			        $("#select-language-list ul").hide().filter("[id='" + $(this).text() + "']").show();
			    });
			    $('#select-language-list li').click(function () {
			        $('#select-language-btn').empty()
			        $(this).clone().appendTo('#select-language-btn');
			        $('#select-language-list').fadeOut();
			        $('#select-language-btn').fadeIn();
			        var langId = $(this).attr("langId");
			        $("#btn-go").removeClass("off").children("a").click(function () {
			            changeLanguage(langId);
			            return false;
			        });
			    });

			    var langName = $("#footer #select-language-list li[langid='" + getLanguage() + "']").text();
			    if (langName.length > 0)
			        $("#anchor-language").text(langName);
			});                         //end document.ready




			/* ===== FUNCTIONS ============================================================================= */

			function onResize() {
			    ensureContentHeight();

			    if (isWhyGlass)
                { 
                    if (theWindow.width() <= 989)
			            $("#ui-vertaccordian").css("left", "687px").css("right", "auto");
                    else
                        $("#ui-vertaccordian").css("right", "0px").css("left", "auto");
                }
			}

	function onEnterPressed(evt, func) {
	    var key = (evt.which) ? evt.which : evt.keyCode;
	    if (key == 13) {
	        var args = [];
	        for (var x = 2; x < arguments.length; x++)
	            args.push(arguments[x]);

	        func(args);
	        return false;
	    }
	}

	function ensureContentHeight() {
	    if (isHome) return;

	    var navMain = $("#nav-main");
	    if (navMain.length <= 0)
	        return;
	    if (navMain.height() + navMain.offset().top > theWindow.height())
	        navMain.addClass("scroll");
	    else
	        navMain.removeClass("scroll");

	    if ($("#subpage-content").length <= 0)
	        return;

	    var contentTop = $("#subpage-content").offset().top;
	    var contentBottom = contentTop + origHeight;
	    var footerTop = $("#footer").offset().top;
	    var cse = $("#cse");
	    if (cse.length > 0 && (cse.offset().top + cse.height()) > footerTop) {
	        $("#subpage-content").css("height", "auto");
	        return;
	    }
	    if (footerTop > contentBottom) {
	        var newHeight = Math.max(origHeight + (footerTop - contentBottom), navMain.height() - (navMain.hasClass("scroll") ? 130 : 0));
	        if (newHeight >= origHeight)
	            $("#subpage-content").height(newHeight);
	    }
	}
			
	
			/* -- METHOD TO RETURN FALSE ON EMPTY ANCHORS -- */
				function emptyAnchors(){
					$('a[href=""]').click(function(){ return false });	
				}
			
			
			
			/* -- METHOD TO PRELOAD IMAGE(S) -- */
				function preload(){ 
					var args_len = arguments.length; 
					for (var i = args_len; i--;) { 
						var cacheImage = document.createElement('img');
						cacheImage.src = arguments[i];
						cache.push(cacheImage);
					} 
				}
			
			
			
			
			
			/* -- METHOD TO TOGGLE AN INPUT LABEL ON/OFF DEPENDING ON VALUE OF INPUT -- */
				function inputLabelToggle(){
				
					if ($('.labeltoggle').length > 0) {										// IF .LABLETOGGLE EXISTS
					
						$('.labeltoggle').each(function(){									// FOR EACH .LABELTOGGLE
							if($(this).attr('value') != ""){									// IF VALUE OF INPUT IS NOT EMPTY ("")
								$(this).prev().hide();													// HIDE THE LABEL (PREVIOUS TO INPUT IN MARKUP)
							}
						}); 
							
						$('.labeltoggle').focus(function(){									// ON FOCUS/CLICK OF INPUT
							$(this).prev().hide();														// HIDE THE LABEL (PREVIOUS TO INPUT IN MARKUP)
						});
						
						$('.labeltoggle').blur(function(){									// ON BLUR/CLICK AWAY FROM INPUT
							if($(this).attr('value') != ""){									// IF INPUT VALUE IS NOT EMPTY ("")
								$(this).prev().hide();													// HIDE THE LABEL (PREVIOUS TO INPUT IN MARKUP)
							} else {																					// ELSE (INPUT HAS A VALUE)
									$(this).prev().show();												// SHOW THE LABEL (PREVIOUS TO INPUT IN MARKUP)
								}
						});
						
					}
				}
		

			/* -- NEWS FEED TABBED CONTENT -- */
				
				// Get ID of the feed list
				var currentFeed = "feed-1";

				/* -- TAB SELECTORS -- */

				$('ul.news-tab li a#company-tab').click(function (event) {										// First tab click actions
				    event.preventDefault();
				    currentFeed = "feed-1";
				    $('ul#feed-1').css('marginTop', 0);
				    $('#oi-panel').hide();
				    $('#company-panel').show();
				    $('ul.news-tab li a#company-tab').addClass('selected');
				    $('ul.news-tab li a#oi-tab').removeClass('selected');

				    scrollFinished();
				});	
				
				$('ul.news-tab li a#oi-tab').click(function(event) {												// Second tab click actions
					event.preventDefault();
					currentFeed = "feed-2";
					$('ul#feed-2').css('marginTop', 0);
					$('#company-panel').hide();
					$('#oi-panel').show();
					$('ul.news-tab li a#oi-tab').addClass('selected');
					$('ul.news-tab li a#company-tab').removeClass('selected');

					if ($('ul#feed-2 li.current-feed').length <= 0)
					    $('ul#feed-2 .feed-title:first').click();

					scrollFinished();
				});


	            /* -- FEED SCROLLER -- */
				var allowScroll = true;
								
				/* -- SCROLL DOWN -- */
				$('.scroll-down').click(function () {																				// Bottom scroll arrow actions
				    if (!allowScroll)
				        return;
				    var feed = $('ul#' + currentFeed);
				    if (feed.height() > Math.abs(parseInt(feed.css("margin-top")) - 300)) {
				        allowScroll = false;
				        $('ul#' + currentFeed).animate({ marginTop: '-=150' }, 500, function () { scrollFinished(); });
				    }
				});						
				
				
				/* -- SCROLL UP -- */
				$('.scroll-up').click(function () {																					// Top scroll arrow actions
				    if (!allowScroll)
				        return;
				    var feed = $('ul#' + currentFeed);
				    if (parseInt(feed.css("margin-top")) < 0) {
				        allowScroll = false;
				        $('.scroll-list ul').animate({ marginTop: '+=150' }, 500, function () { scrollFinished(); });
				    }
				});

				function scrollFinished() {
				    allowScroll = true;
				    var feed = $('ul#' + currentFeed);
				    $(".scroll-down").toggleClass("disabled", !(feed.height() > Math.abs(parseInt(feed.css("margin-top")) - 300)));
				    $(".scroll-up").toggleClass("disabled", !(parseInt(feed.css("margin-top")) < 0));
				}


				function changeLanguage(langTypeId) {
				    var ecm = $.cookie("ecm");
				    if (ecm.length <= 0)
				        return;

				    var langUpdates = ["SiteLanguage"];
				    for (var x = 0; x < langUpdates.length; x++) {
				        var match = new RegExp(langUpdates[x] + "=\\d*");
				        ecm = ecm.replace(match, langUpdates[x] + "=" + langTypeId);
				    }

				    $.cookie("ecm", ecm, { raw: true, path: "/" });
				    $.cookie("langSelected", "true", { path: "/", expires: new Date("1/1/9999") });
				    window.location = window.location;
				}

				function getLanguage() {
				    var ecm = $.cookie("ecm");
				    if (ecm.length <= 0)
				        return;

				    var match = new RegExp("SiteLanguage=(\\d*)");
				    return match.exec(ecm)[1];
				}
	
	/* ===== USER AGENT SPECIFIC SCRIPTING ================================================================ */
	
	
			if (isIphone){ /* alert('iphone'); */ }
			if (isIpad){ /* alert('iphone'); */ }	
			if (isAndroid){ /* alert('android'); */ }	
		  
		  
		  
		  
		  

		  