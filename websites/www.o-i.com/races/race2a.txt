<script id = "race2a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race2={};
	myVars.races.race2.varName="select-language-list__onmouseout";
	myVars.races.race2.varType="@varType@";
	myVars.races.race2.repairType = "@RepairType";
	myVars.races.race2.event1={};
	myVars.races.race2.event2={};
	myVars.races.race2.event1.id = "Lu_DOM";
	myVars.races.race2.event1.type = "onDOMContentLoaded";
	myVars.races.race2.event1.loc = "Lu_DOM_LOC";
	myVars.races.race2.event1.isRead = "False";
	myVars.races.race2.event1.eventType = "@event1EventType@";
	myVars.races.race2.event2.id = "select-language-list";
	myVars.races.race2.event2.type = "onmouseout";
	myVars.races.race2.event2.loc = "select-language-list_LOC";
	myVars.races.race2.event2.isRead = "True";
	myVars.races.race2.event2.eventType = "@event2EventType@";
	myVars.races.race2.event1.executed= false;// true to disable, false to enable
	myVars.races.race2.event2.executed= false;// true to disable, false to enable
</script>

