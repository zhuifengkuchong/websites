<script id = "race18a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race18={};
	myVars.races.race18.varName="Object[4179]..dropdown";
	myVars.races.race18.varType="@varType@";
	myVars.races.race18.repairType = "@RepairType";
	myVars.races.race18.event1={};
	myVars.races.race18.event2={};
	myVars.races.race18.event1.id = "Lu_Id_li_1";
	myVars.races.race18.event1.type = "onmouseover";
	myVars.races.race18.event1.loc = "Lu_Id_li_1_LOC";
	myVars.races.race18.event1.isRead = "False";
	myVars.races.race18.event1.eventType = "@event1EventType@";
	myVars.races.race18.event2.id = "Lu_Id_li_26";
	myVars.races.race18.event2.type = "onmouseover";
	myVars.races.race18.event2.loc = "Lu_Id_li_26_LOC";
	myVars.races.race18.event2.isRead = "True";
	myVars.races.race18.event2.eventType = "@event2EventType@";
	myVars.races.race18.event1.executed= false;// true to disable, false to enable
	myVars.races.race18.event2.executed= false;// true to disable, false to enable
</script>

