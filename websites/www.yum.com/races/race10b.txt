<script id = "race10b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race10={};
	myVars.races.race10.varName="logosp__onmouseout";
	myVars.races.race10.varType="@varType@";
	myVars.races.race10.repairType = "@RepairType";
	myVars.races.race10.event1={};
	myVars.races.race10.event2={};
	myVars.races.race10.event1.id = "logosp";
	myVars.races.race10.event1.type = "onmouseout";
	myVars.races.race10.event1.loc = "logosp_LOC";
	myVars.races.race10.event1.isRead = "True";
	myVars.races.race10.event1.eventType = "@event1EventType@";
	myVars.races.race10.event2.id = "Lu_window";
	myVars.races.race10.event2.type = "onload";
	myVars.races.race10.event2.loc = "Lu_window_LOC";
	myVars.races.race10.event2.isRead = "False";
	myVars.races.race10.event2.eventType = "@event2EventType@";
	myVars.races.race10.event1.executed= false;// true to disable, false to enable
	myVars.races.race10.event2.executed= false;// true to disable, false to enable
</script>

