<script id = "race28a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race28={};
	myVars.races.race28.varName="Function[13591].el";
	myVars.races.race28.varType="@varType@";
	myVars.races.race28.repairType = "@RepairType";
	myVars.races.race28.event1={};
	myVars.races.race28.event2={};
	myVars.races.race28.event1.id = "Lu_Id_li_16";
	myVars.races.race28.event1.type = "onmouseout";
	myVars.races.race28.event1.loc = "Lu_Id_li_16_LOC";
	myVars.races.race28.event1.isRead = "False";
	myVars.races.race28.event1.eventType = "@event1EventType@";
	myVars.races.race28.event2.id = "Lu_Id_li_20";
	myVars.races.race28.event2.type = "onmouseout";
	myVars.races.race28.event2.loc = "Lu_Id_li_20_LOC";
	myVars.races.race28.event2.isRead = "True";
	myVars.races.race28.event2.eventType = "@event2EventType@";
	myVars.races.race28.event1.executed= false;// true to disable, false to enable
	myVars.races.race28.event2.executed= false;// true to disable, false to enable
</script>

