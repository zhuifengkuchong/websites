/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {


// To understand behaviors, see https://drupal.org/node/756722#behaviors
Drupal.behaviors.my_custom_behavior = {
    attach: function (context, settings) {
        
        //$(document).ready(function () {
        //    console.log('javascript is working on this page');
        //    $("a.contextual-links-trigger").click(function () {
        //        console.log('click');
        //        event.preventDefault();
        //        console.log('maybe. shouldnt jump');
        //        $(this).next().toggle();
        //        console.log('working');
        //    });
        //});
        

      $(document).ready(function () {
            $("body").html(
            $("body").html().replace(/&reg;/gi, '<sup>&reg;</sup>').replace(/�/gi, '<sup>&reg;</sup>')); // this will break in script tags and a few other places.
            $("sup sup").each(function () {  // find sup with child sup
                $(this).parent("sup").html("&reg;");
            });
      });
    if($('body.division-food').length > 0 || $('body.division-diversey').length > 0 || $('body.division-product').length > 0 || $('body.division-medical').length > 0) {
    
    	$('#field-slideshow-1-wrapper .field-slideshow').css('margin', 'auto');
		
    }


   

    // blog mobile styles
	if($('body.page-blog').length > 0) {
		
		$('aside.sidebar-left').clone().addClass('mobile').appendTo('div#main');
		
	}

	
	//Move Services Link on Products&Services page
	if($('body.page-node-12').length > 0) {
		
			 $( ".views-row-even" ).each(function( index ) {
				var servicesLink = $(this).find('.views-field-name');
				var productsLink = $(this).prev().find('.views-field-name');
				
				productsLink.append(servicesLink.html());
				servicesLink.addClass('hide-Services');
			});
		
	}
	
	if($('.taxonomy-info.division-sub-category"').length > 0)
	{
		$('.fiter-header').show;
		$('.view-filters').show();
	}
	else{	
		$('.filter-header').hide();
		$('.view-filters').hide();
	}
	

	
	//Search bar
     $('#block-block-36').hide();
     $( "li.menu-590 a" ).click(function(e) {
       e.preventDefault();
       //alert('clicked search');
       $('#block-block-36').show();
     });
	  $( "#secondary-menu ul li:last-child a" ).click(function(e) {
       e.preventDefault();
       //alert('clicked search');
       $('#block-block-36').show();
     });
    
	 
	 $( "#formSearch" ).submit(function( event ) {
	  event.preventDefault();
	  var searchText = $('#txtSearch').val();
	  var langFRCA = $('body').hasClass('i18n-fr-ca');
	  //alert(langFRCA);
	  if(langFRCA){
       window.location.href = "http://www.sealedair.com/fr-ca/search/content/"+searchText;
	  }
	  else{
		  window.location.href = "http://www.sealedair.com/search/content/"+searchText;
	  }
	});


	$('.closeSearch').click(function(){
		$('#block-block-36').hide();
	});

	






	
	//Move T5 items on taxonomy pages up to sit with other view items
	if($('.page-taxonomy #block-views-category-t5s-block').length > 0 && $('#block-views-taxonomy-displays-block').length > 0){
		//alert('has t5');
			var t5row = $('.page-taxonomy #block-views-category-t5s-block .views-row').parent();
			var t5rowHtml = t5row.html();
			//alert(t5row);
			t5row.hide();
			$('#block-views-taxonomy-displays-block .view-content').append(t5rowHtml);
	}
	
	//Set homepage link switch back to video
	if($('.front .homepage-video-link').length > 0) {
		
		
  		if((navigator.platform.indexOf("iPhone") != -1) ||(navigator.platform.indexOf("iPod") != -1) ||(navigator.platform.indexOf("iPad") != -1)){
			$('.views-field-field-homepage-hero-image').hide();
		}
		else{
			$('.views-field-field-hero-video').hide();
		}
		
		$( ".homepage-video-link" ).click(function() {
  		
			$('.views-field-field-homepage-hero-image').fadeOut('slow');
			$('.views-field-field-hero-video').fadeIn('slow');
			
		});
			 
		
	}

    // filter out IP addresses for GA
	(function ($) {
	    //alert('hello');
	    function setCookie(cname, cvalue, exdays) {
	        var d = new Date();
	        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
	        var expires = "expires=" + d.toUTCString();
	        document.cookie = cname + "=" + cvalue + "; " + expires;
	    }

	    $(document).ready(function () {
	        $.ajax({
	            url: 'http://www.sealedair.com/sites//all//themes//sealedair//js//solutions-finder/seecheck.php',
	            success: function (data) {
	                console.log(data);
	                if (data === 'internal') {
	                    setCookie('seecheck', 'i', 30);
	                }
	                else {
	                    setCookie('seecheck', 'e', 30);
	                }
	                
	            }
	        });
	    });
	})(jQuery);



	
	//Push left content to full width if right content is empty
	if( $('.node-type-page-left-nav').length > 0 ){
		
		if($.trim($('#content .group-right').html()) == ''){
	  		$('#content .group-left').css('width','100%');
	  	}
		
	}
	
	//Push left content to full width if right content is empty
	if($('body.node-type-t5').length > 0){
	
		
	  	if($.trim($('.group-right').html()) == ''){
	  		$('.group-left').css('width','100%');
	  	}
			
	}
	
	//Push left content to full width if right content is empty
	if($('body.node-type-page-left-nav').length > 0){
		
		if($('#main .sidebar-right').length == 0){
			//console.log('heeeey!');
			$('#main #content').addClass('left-nav-stretch');
			
		}
		
	}
	
	//Push left content to full width if right content is empty
	if($('body.node-type-t8').length > 0){ /* node-type-solution-industry */
	
		
	  	if($.trim($('.group-right').html()) == ''){
	  		$('div.node-t8 .group-left').css('width','100%');
	  	}
			
	}

	
	/*
if($('body.node-type-solution-industry').length > 0){
		
		if($('#main group-right').length == 0){
			
			$('#main #content').addClass('left-nav-stretch');
			
		}
		
	}
*/

	//if($('.page-node-64196').length > 0){
	
		$('.page-node-64196 .milan-accordion ul li').each(function(){
		    $(this).click(function (){
			if ($(this).hasClass('on')) {
			    $(this).children('div').hide();
				$(this).removeClass('on');
				}
			else {
		        $('li').removeClass('on');
		        $('li').children('div').hide();
		        $(this).children('div').show;
		        $(this).children('div').slideToggle();
				$(this).toggleClass('on');
			}
			});
		});
	
	//}
	
	
	if($('.page-taxonomy-term').length > 0){
		var promoImg = $('.view-mode-small_promo a img');
		var promoTxt = $('.view-mode-small_promo .group-middle');
		
		
		/*
$(window).resize(function() {
		
			if($(window).width() > 520){
				promoTxt.height(promoImg.height());
			} else if($(window).width() <= 520){
				promoTxt.css('height', 'auto');
			}
		});
*/
		
		if($(window).width() <= 520){
			$('#content-bottom .taxonomy-term').append('<div class="arrow"></div>');
		}
		
	}

	
	
	//Solutions Finder
	if($('.view-solution-finder').length > 0) {
		/* $('.filter-hide').hide(); */
		
		$('.filter-show div.division').click(function(event){
			event.preventDefault();
			
			
			if(!$(this).hasClass('active')){
				$('.filter-show div.division').addClass('inactive');
			}
			
			if($(".filter-show div.food").hasClass("inactive") /* && $("div.diversey").hasClass("inactive") */){
				console.log('these are all inactive');
			}
			
		});
		
		
		$('.filter-show div.food').click(function(event) {
			event.preventDefault();
		   //$("#edit-field-division-tid").val("56").change();
		   if($(this).hasClass('active')){
			   $(this).removeClass('active');
			   $(this).addClass('inactive');
		   }
		   else{
		   		$(this).addClass('active');
		   		$(this).removeClass('inactive');
		   }
		   showAllActive();
		});	
		
		$('.filter-show div.diversey').click(function(event) {
			event.preventDefault();
		   //$("#edit-field-division-tid").val("57").change();
		     if($(this).hasClass('active')){
			   $(this).removeClass('active');
			   $(this).addClass('inactive');
		   }
		   else{
		   		$(this).addClass('active');
		   		$(this).removeClass('inactive');
		   }
		   showAllActive();
		});	
		
		$('.filter-show div.product').click(function(event) {
			event.preventDefault();
		   //$("#edit-field-division-tid").val("58").change();
		   if($(this).hasClass('active')){
			   $(this).removeClass('active');
			   $(this).addClass('inactive');
		   }
		   else{
		   		$(this).addClass('active');
		   		$(this).removeClass('inactive');
		   }
		   showAllActive();
		});	
		
		$('.filter-show div.medical').click(function(event) {
			event.preventDefault();
		   //$("#edit-field-division-tid").val("59").change();
		     if($(this).hasClass('active')){
			   $(this).removeClass('active');
			   $(this).addClass('inactive');
		   }
		   else{
		   		$(this).addClass('active');
		   		$(this).removeClass('inactive');
		   }
		   showAllActive();
		});
		
// to add and remove floating of industries
		/*
$('.filter-show div').click(function(event) {
			event.preventDefault();	
			
			console.log($('.filter-show').children('div.active').length);
			
			if( $('.filter-show').children('div.active').length == 4 ){
				console.log('woot!');
				$('.section-solutions-finder #page .view-solution-finder .views-row').css('float', 'left');
			} else {
				$('.section-solutions-finder #page .view-solution-finder .views-row').css('float', 'none');
			}		
		});
*/
		
	}
	
function turnOffVideo(){
	//if homepage
	if($('.front').length > 0){
		$('.views-field-field-homepage-hero-image').show();
		$('.views-field-field-hero-video').hide();
		
	}
}
	
	
function showAllActive(){
	//alert("in here");
	 //$('.solution .Care').fadeOut('slow');
	 $('.solution .Care')/* .parents('.views-row') */.css('opacity', '.45'); //.hide();
	 	 
	 var on = false;
	 
	 if($('.division.food.active').length > 0){
		 $('.solution .Food')/* .parents('.views-row') */.css({
		 	//'display' : 'inline-block',
		 	 //'float'  :  'none'
		 	 'opacity'  :  '1'
		 	});
		 on = true;
	 }
	 
	 if($('.division.diversey.active').length > 0){
		 $('.solution .Diversey')/* .parents('.views-row') */.css('opacity', '1');
		 on = true;
	 }
	 
	 if($('.division.product.active').length > 0){
		 $('.solution .Product')/* .parents('.views-row') */.css('opacity', '1');
		 on = true;
	 }
	 
	 if($('.division.medical.active').length > 0){
		 $('.solution .Medical')/* .parents('.views-row') */.css('opacity', '1');
		 on = true;
	 }
	 
	 if (!on){
		 $('.solution .Care')/* .parents('.views-row') */.css('opacity', '1'); //show();
	 }
	
}
	
	/*todo change to product type*/
	if($('.node-type-article').length > 0 || $('.node-type-product-detail').length > 0 ){
		
		//move breadcrumb
		var breadcrumb = $('#breadcrumb-wrapper');
		var productBreadcrumb = $('#pd-breadcrumb-wrapper');
		
		productBreadcrumb.append(breadcrumb.html());
		breadcrumb.hide();
		
		//initiate accordion js
		$( "#accordion" ).accordion({
			collapsible: true,
			
			beforeActivate: function(event, ui) {
				 // The accordion believes a panel is being opened
				if (ui.newHeader[0]) {
					var currHeader  = ui.newHeader;
					var currContent = currHeader.next('.ui-accordion-content');
				 // The accordion believes a panel is being closed
				} else {
					var currHeader  = ui.oldHeader;
					var currContent = currHeader.next('.ui-accordion-content');
				}
				 // Since we've changed the default behavior, this detects the actual status
				var isPanelSelected = currHeader.attr('aria-selected') == 'true';
		
				 // Toggle the panel's header
				currHeader.toggleClass('ui-corner-all',isPanelSelected).toggleClass('accordion-header-active ui-state-active ui-corner-top',!isPanelSelected).attr('aria-selected',((!isPanelSelected).toString()));
		
				// Toggle the panel's icon
				currHeader.children('.ui-icon').toggleClass('ui-icon-triangle-1-e',isPanelSelected).toggleClass('ui-icon-triangle-1-s',!isPanelSelected);
		
				 // Toggle the panel's content
				currContent.toggleClass('accordion-content-active',!isPanelSelected)    
				if (isPanelSelected) { currContent.slideUp(); }  else { currContent.slideDown(); }
		
				return false;
			}// Cancels the default action
			
			});
			
			
		$('#accordion .ui-accordion-content').css('height', 'auto');
		
	}
	
	if($(window).width() <= 768 || $(window).resize() <= 768){
			
			$('#tablet-menu .block-superfish .sf-menu li ul').remove();
			
	}
	
	if($(window).width() <= 520 || $(window).resize() <= 520){
			
	     $( "li.menu-590 a" ).click(function() {
	       $("html, body").animate({ scrollTop: 0 }, 300);
				 return false;
	     });
			
	}
	
	
	if( $('.node-type-page-left-nav').length > 0){

		
		$('.region-sidebar-first ul li').find('ul').hide();
		
		$('.region-sidebar-first ul li').has('ul').click(function(e){
			e.preventDefault();
			$(this).find('ul').toggle();
			$(this).toggleClass('is-minus');
		});
		
		$(".region-sidebar-first ul li ul li").click(function(e){  
        e.stopPropagation();  
    });

		
	}	
	
	
	// Toolbox Icons
	$('.toolbox .icon').each(function(){
		var $toolicon = $(this);
		var icon = $('.icon img');
	
			$($toolicon, icon).click(function(e){
				e.preventDefault();
				
				$tooldrop = $('http://www.sealedair.com/sites//all//themes//sealedair//js//div.info', $toolicon)
				
				$tooldrop.toggle();
				$('.info').not($tooldrop).hide();
				return false;

		});
		
		
	});
	
	$('div.info a').click(function(e){
		e.stopPropagation();
	});
	
// Mobile menu 
	$(".menu-icon").click(function(e) {
		e.stopPropagation();
	   $("#tablet-menu").toggleClass('show-menu');
	});
	
	$('#tablet-menu').click(function(e){
		e.stopPropagation();
	});
	
	$('html').click(function(){
		$('#tablet-menu').removeClass('show-menu');
		$('http://www.sealedair.com/sites//all//themes//sealedair//js//div.info').hide();
	});
	
	

  } 
  
};
})(jQuery, Drupal, this, this.document);


