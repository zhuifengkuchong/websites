$.fn.animateHover = function (settings) {
    settings = jQuery.extend({
        bgcolor: '#000000',
        fonthovercolor: '#ffffff',
        spanclass: 'hover',
        ignoreHeight: false,
        ignoreClass: null,
        preFn: null,
        hoverFn: null,
        duration: 500
    }, settings);
    return this.each(function (e, object) {
        $(this).css('position', 'relative');
        var original = $(this).css('background-color');
        var btnText = $(this).html();
        var width = $(this).css('width');
        var height = $(this).css('height');
        var paddingTop = $(this).css('padding-top');
        var paddingRight = $(this).css('padding-right');
        var paddingBottom = $(this).css('padding-bottom');
        var paddingLeft = $(this).css('padding-left');

        var styleStr = "display: none; padding-top: " + paddingTop + "; padding-right: " + paddingRight + "; padding-bottom: " + paddingBottom + "; padding-left: " + paddingLeft + ";width: " + width + "; color: " + settings.fonthovercolor + "; position: absolute; top: 0px; left: 0px; background-color: " + settings.bgcolor + ";";
        if (settings.ignoreHeight != true) {
            styleStr += "height: " + height + "";
        }
        $(this).append("<div class='" + settings.spanclass + "' style='" + styleStr + "'>" + btnText + "</div>");
        if (settings.preFn != null) {
            settings.preFn(e, this);
        }
        $(this).hover(function (e) {
            if (settings.hoverFn != null) {
                settings.hoverFn(e, this);
            }
            if (settings.ignoreClass != null && $(this).hasClass(settings.ignoreClass)) {
                return false;
            }
            $(this).children('.' + settings.spanclass).stop(true, true).fadeIn(settings.duration);
        }, function () {
            $(this).children('.' + settings.spanclass).stop(true, true).fadeOut(settings.duration);
        });
    });
};

$.fn.slider1903 = function (settings) {
    settings = jQuery.extend({
        toggleEvery: 4,
        useArrows: false,
        hasVideos: false,
        videoWraper: 'video',
        toggleSeperator: '...',
        maxTogglers: 6,
        currentToggleClass: 'current',
        toggleNavClass: 'ulSvn',
        preFn: null,
        postFn: null,
        speed: 1500,
        ease: 'easeOutQuint',
        type: 'slide'
    }, settings);

    //count and do math
    var $this = $(this),
        total = $(this).children().length,
        $itemEl = $(this).find('li:first-child'),
        singleWidth = $itemEl.outerWidth(true),

        baseWidth = total * singleWidth,
        baseMovement = singleWidth * settings.toggleEvery,
        totalPages = baseWidth / baseMovement,
        roundedPages = Math.floor(totalPages),
        hasOrphans, orphans, onOrphan = false;
    current = 0;
    //set the width for the view
    $(this).width(baseWidth);

    if (totalPages == roundedPages) {
        hasOrphans = false;
    } else {
        hasOrphans = true;
        substrt = totalPages.toString().lastIndexOf(".")
        orph = totalPages.toString().substring(substrt);
        orphans = orph * settings.toggleEvery;
    }

    if (settings.type == 'fade') {
        $this.find('li:not(:first)').hide();
        $this.find('li').each(function (index) {
            $(this).addClass("el-" + (index + 1));
        });
    }

    //add navigation elements if using numbered / paginated slider
    if (!settings.useArrows) {
        var i = 1;
        if (totalPages > 1) {
            var str = '<div class="pagination"><ul class="' + settings.toggleNavClass + '">';
            for (i; i <= roundedPages; i++) {
                if (i == current) {
                    str += '<li><a data-val="' + (i) + '" class="' + settings.currentToggleClass + '">' + (i) + '</a></li>';
                } else {
                    str += '<li><a data-val="' + (i) + '">' + (i) + '</a></li>';
                }
            }
            if (hasOrphans) {
                str += '<li><a data-id="orphan" data-count="' + orphans + '" data-val="' + (i) + '">' + (i) + '</a></li>';
            }
            str += '</ul></div>';
            $(this).after(str);
        }

        //control click functions   
        $('.' + settings.toggleNavClass + ' li a').click(function (e) {
            $('.' + settings.toggleNavClass + ' li a').each(function () {
                $(this).removeClass(settings.currentToggleClass)
            });
            $clicked = $(this).attr('data-val');
            $(this).addClass(settings.currentToggleClass);
            if (settings.type == 'slide') {
                if ($clicked > current) {
                    //if $clicked is not the last or the orphaned
                    if ($clicked < totalPages && current < roundedPages || !hasOrphans && $clicked != i) {
                        move = baseMovement * ($clicked - 1);
                        $this.stop(true, true).animate({
                            left: -move + 'px'
                        }, settings.speed, settings.ease)
                    }

                    //if $clicked is an orpan page
                    if (hasOrphans && $clicked == i) {
                        $this.stop(true, true).animate({
                            left: -((roundedPages - 1) * baseMovement) - (orphans * singleWidth) + 'px'
                        }, settings.speed, settings.ease)
                    }
                } else if ($clicked == 1) {
                    //if root
                    $this.stop(true, true).animate({
                        left: 0 + 'px'
                    }, settings.speed, settings.ease)
                }
            } else {
                $this.find('li').each(function (e) {
                    if ($(this).css('display') == 'block' || $(this).css('display') == 'list-item') {
                        $(this).fadeOut(settings.speed, function () {
                            $("li.el-" + $clicked).fadeIn(settings.speed);
                        });
                    }
                });
            }
            if (settings.hasVideos) {
                $(settings.videoWraper).each(function () {
                    $(this)[0].player.pause()
                });
            }
        });

    } else {
        //arrowed back / next slider
        var backAr = '<a href="#" class="slide1903 arr-back" attr-page="' + current + '">< Back</a>';
        var nextAr = '<a href="#" class="slide1903 arr-next" attr-page="' + current + '">Next ></a>';
        $(this).before(backAr);
        $(this).after(nextAr);
        $('.arr-back').addClass('disabled');
        $('.arr-back').click(function (e) {
            e.preventDefault();
            $('.arr-next').removeClass('disabled');
            if (current == 0) {
                //
            } else {
                if (!onOrphan) {
                    current = current - 1;
                } else {
                    onOrphan = false;
                }
                $this.stop(true, true).animate({
                    left: -(baseMovement * current) + 'px'
                }, settings.speed, settings.ease);
                $('.slide1903').each(function () {
                    $(this).attr('attr-page', current);
                })
                if (current == 0) {
                    $('.arr-back').addClass('disabled');
                };
            }
        })

        $('.arr-next').click(function (e) {
            e.preventDefault();
            $('.arr-back').removeClass('disabled');
            //Navigate To Orphans if Last
            //alert(roundedPages-1);
            if (current == (roundedPages - 1) && hasOrphans && !onOrphan) {
                $this.stop(true, true).animate({
                    left: -((roundedPages - 1) * baseMovement) - (orphans * singleWidth) + 'px'
                }, settings.speed, settings.ease);
                $(this).addClass('disabled');
                onOrphan = true;
            } else if (current == (roundedPages - 1) && !hasOrphans) {
                $this.stop(true, true).animate({
                    left: -((roundedPages - 1) * baseMovement) - (orphans * singleWidth) + 'px'
                }, settings.speed, settings.ease);
                $(this).addClass('disabled');
                //onOrphan = true;        
            }
            //if on orphans, than dont do anything
            else if (onOrphan == true) {
                //
            } else {

                current = current + 1;
                $this.stop(true, true).animate({
                    left: -(baseMovement * current) + 'px'
                }, settings.speed, settings.ease);

                $('.slide1903').each(function () {
                    $(this).attr('attr-page', current);
                })
            }
        })
    }

};

$.fn.randomize = function (childElem) {
    return this.each(function () {
        var $this = $(this);
        var elems = $this.children(childElem);

        elems.sort(function () {
            return (Math.round(Math.random()) - 0.5);
        });

        $this.remove(childElem);

        for (var i = 0; i < elems.length; i++)
        $this.append(elems[i]);
    });
}

jQuery.fn.idle = function (time) {
    var o = $(this);
    o.queue(function () {
        setTimeout(function () {
            o.dequeue();
        }, time);
    });
};

$.fn.heroSlider1903 = function (settings) {
    settings = jQuery.extend({
        type: 'slide',
        currentClass: 'current',
        autoScroll: true,
        delay: 4500,
        restartDelay: 15000,
        showTabs: true,
        tabsID: 'ulHeroSliderNavigation',
        showArrows: true,
        arrows: {
            nextclass: 'heroNext',
            backclass: 'heroBack',
            sharedclass: 'heroArr'
        },
        preFn: null,
        hoverFn: null,
        speed: 1000,
        ease: 'easeOutQuint'
    }, settings);
    return this.each(function (e, object) {
        $(this).css('position', 'relative');
        var totalElements = $(this).children().length,
            singleWidth = $(this).children(':first-child').outerWidth(true),
            ulWidth = parseInt(totalElements) * parseInt(singleWidth),
            $this = $(this),
            ulHeight = $(this).css('height'),
            totalElements = $(this).children().length,
            baseMovement = ulWidth / totalElements,
            restartInt, cycleInt, current = 1;


        if (settings.type == 'fade') {
            $(this).children().hide();
            $(this).find('li:first-child').fadeIn(settings.speed);
        } else {
            $(this).width(ulWidth);
        }

        if (settings.showArrows) {
            var backAr = '<a href="#" class="' + settings.arrows.sharedclass + ' ' + settings.arrows.backclass + '" attr-page="' + current + '">< Back</a>';
            var nextAr = '<a href="#" class="' + settings.arrows.sharedclass + ' ' + settings.arrows.nextclass + '" attr-page="' + current + '">Next ></a>';
            $(this).before(backAr);
            $(this).after(nextAr);
        }

        if (settings.showTabs) {
            var i = 1;
            if (totalElements > 1) {
                var str = '<div class="pagination"><ul class="' + settings.tabsID + '">';
                for (i; i <= totalElements; i++) {
                    if (i == current) {
                        str += '<li><a href="#" data-id="' + (i) + '" class="' + settings.currentClass + '">' + (i) + '</a></li>';
                    } else {
                        str += '<li><a href="#" data-id="' + (i) + '">' + (i) + '</a></li>';
                    }
                }
                str += '</ul></div>';
                $(this).after(str);
            }
        }

        if (settings.autoScroll) {
            cycleInt = setInterval(autoRotation, settings.delay);
        }

        function autoRotation() {
            if (settings.showTabs && !settings.showArrows) {
                var equatedtab;
                if (parseInt(current) + parseInt(1) > totalElements) {
                    equatedTab = 1;
                } else {
                    equatedTab = parseInt(current) + parseInt(1);
                }

                $('.' + settings.tabsID + ' li a').each(function () {
                    if ($(this).attr('data-id') == equatedTab) {
                        $(this).trigger('click', [false, equatedTab]);
                        return false;
                    }
                });

            } else {
                $('a.' + settings.arrows.nextclass).trigger('click', [false, null]);
            }
        };

        function restartRotation() {
            clearInterval(restartInt);
            cycleInt = setInterval(autoRotation, settings.delay);
        }

        if (settings.showTabs && !settings.showArrows) {
            $('.' + settings.tabsID + ' li a').click(function (e, kill, forcedVal) {
                handleToggle(e, kill, $(this).attr('data-id'));
            });
        } else {
            $('a.' + settings.arrows.sharedclass).click(function (e, kill, forcedVal) {
                handleToggle(e, kill, forcedVal);
            });

            $('.' + settings.tabsID + ' li a').click(function (e) {
                e.preventDefault();
                var forcedVal = $(this).attr('data-id');
                $('a.' + settings.arrows.nextclass).trigger('click', [true, forcedVal]);
            });
        }

        function handleToggle(e, kill, forcedVal) {
            if (settings.showTabs && current == forcedVal) {
                return false;
            }
            e.preventDefault();
            if (settings.autoScroll && kill != false) {
                clearInterval(cycleInt);
                clearInterval(restartInt);
                restartInt = setInterval(restartRotation, settings.restartDelay);
            }
            if ($(this).hasClass(settings.arrows.backclass) && forcedVal == null) {
                if (current == 1) {
                    current = totalElements;
                } else {
                    current = parseInt(current, 10) - parseInt(1, 10);
                }
            } else if (forcedVal == null) {
                if (current == totalElements) {
                    current = 1
                } else {
                    current = parseInt(current, 10) + parseInt(1, 10);
                }
            } else {
                current = forcedVal;
            }

            if (settings.showTabs) {
                $('.' + settings.tabsID + ' li a.' + settings.currentClass).removeClass(settings.currentClass);
                $('.' + settings.tabsID + ' li a').each(function () {
                    if ($(this).attr('data-id') == current) {
                        $(this).addClass(settings.currentClass);
                    }
                })
            }

            if (settings.type != 'fade') {
                if (current == 1) {
                    $this.stop(true, true).animate({
                        left: 0 + 'px'
                    }, settings.speed, settings.ease);
                } else {
                    move = baseMovement * (parseInt(current, 10) - parseInt(1, 10));
                    $this.stop(true, true).animate({
                        left: -move + 'px'
                    }, settings.speed, settings.ease);
                }
            } else {
                $this.children().filter(':visible').stop(true, true).fadeOut(settings.speed, function () {
                    $this.children().eq(parseInt(current, 10) - parseInt(1, 10)).stop(true, true).fadeIn(settings.speed);
                });
            }
        };
    });
};

$.fn.a1903Accordion = function(settings) {
    settings = jQuery.extend({
      speed: 300,
      currentClass: 'current',
    triggerClass: '.a1903Trigger',
      trigger: 'data-id',
      receiver: 'child-id',
      childElm: 'dd',
    nested: false,
    toggleHtml: false,
    openText: "Expand",
    closeText: "Close"
    }, settings);
  
  return this.each(function (e, object) {
    $this = $(this);  
    /*
    //OLD NESTED TRIGGER HANDLERS
    if($(this).parent().is("li") && !settings.nested || $(this).parent().not("li") && settings.nested) {
      $triggerParent = $(this).parent().parent();
    } else if($(this).parent().is("li") && settings.nested) {
      $triggerParent = $(this).parent().parent().parent();
    } else {
      $triggerParent = $(this).parent();
    }
    */
    //i = 0;
    $(this).find(settings.triggerClass).each(function() {
      if(!$(this).parent().hasClass(settings.currentClass)) {
        $(this).parent().next().hide();
      }
    });
    
    $(this).find(settings.triggerClass).click(function(e) {
      e.preventDefault();
      $this = $(this);
      favid = $(this).attr(settings.trigger);
      if(settings.nested) {
        $nested = $(this).parent().parent();
      } else {
        $nested = $(this).parent();
      }
      
      if(settings.childElm == 'dd') {
        if($nested.hasClass(settings.currentClass)) {
          $nested.removeClass(settings.currentClass).next().slideUp(settings.speed);
          $(this).removeClass('a1903Active');
          if(settings.toggleHtml) {$this.html(settings.openText);}
          return false;
        } else {
          $nested.parent().find('.'+settings.currentClass).each(function() {
            $(this).removeClass(settings.currentClass).next().slideUp(settings.speed);
            $(this).find('a.a1903Active').removeClass('a1903Active');
            if(settings.toggleHtml) {$(this).html(settings.openText);}
          });
          $nested.addClass(settings.currentClass).next().slideDown(settings.speed);
          $(this).addClass('a1903Active');
          if(settings.toggleHtml) {$this.html(settings.closeText);}
        }
      } else {
        if($nested.hasClass(settings.currentClass)) {
          $nested.removeClass(settings.currentClass).find(settings.childElm).slideUp(settings.speed);
          $(this).removeClass('a1903Active');
          if(settings.toggleHtml) {$this.html(settings.openText);}
          return false;
        } else {
          $nested.parent().find('.'+settings.currentClass).each(function() {
            $(this).removeClass(settings.currentClass).find(settings.childElm).slideUp(settings.speed);
            $(this).find('a.a1903Active').removeClass('a1903Active')
            if(settings.toggleHtml) {$(this).html(settings.openText);}
          });
          $nested.addClass(settings.currentClass).find(settings.childElm).slideDown(settings.speed);
          $(this).addClass('a1903Active');
          if(settings.toggleHtml) {$this.html(settings.closeText);}
        }        
      }
    })   
  })
}
