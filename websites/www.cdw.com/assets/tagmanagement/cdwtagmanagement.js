(function (window) {
//Generic Global Funtion for Ensighten TagManagement
window.ensCustomEvent = (function(){
					_public = {
						create: 
						function (_evtName,_eventData){ /* For IE */
						if(document.createEventObject)
						{
							window.evtData=_eventData;
							var evt=document.createEventObject();
							evt.detail=_eventData;
							return Bootstrapper.ensEvent.trigger(_evtName,evt)
						}
						else
						{ /* For FireFox and others */
							window.evtData=_eventData;
							var evt=document.createEvent("HTMLEvents");
							evt.initEvent(_evtName,true,true);
							evt.detail=_eventData;
							return!document.dispatchEvent(evt)
						}
						}
					}
					return _public;
				})();
	
	function tagger() {
	
		var _taggingService;
		
		function fireEvent(eventName, data) {
					
			var svc = getTaggingService();
			
			if (!svc || !svc.create) {
				//console.warn('No Tagging Service object available');
				return;
			}

			svc.create(eventName, data);
			//console.info('fired', eventName, 'with', data.page_type, data.page_name);
		}
		
		function getTaggingService() {
		
			if (!_taggingService) {
				// get tagging service from global object
				_taggingService = window.ensCustomEvent;
			}
			
			return _taggingService;
		}
	
		function createPageViewTag(categoryId, pageId) {
		
			fireEvent('CreatePageViewEvent', { page_type: categoryId, page_name: pageId });
		}		

		function createPageViewTag(categoryId, pageId, productId) {

		    fireEvent('CreatePageViewEvent', { page_type: categoryId, page_name: pageId, product_id: productId});
		}

		function createPageviewEventVideo(categoryId, pageId) {
		
			fireEvent('CreatePageviewEventVideo', { page_type: categoryId, page_name: pageId });
		}
		
		function createElementPageTag(categoryId, elementId) {

			fireEvent('CreateElementPageEvent', { page_type: categoryId, page_name: elementId });
		}
		
		// Public API
		return {
			createPageViewTag: createPageViewTag,
			createPageviewEventVideo: createPageviewEventVideo,
			createElementPageTag: createElementPageTag
		};
	}
	
	// Expose tagger as 'CdwTagMan' on the global object
	window.CdwTagMan = tagger();
	
})(window);







