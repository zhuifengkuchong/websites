$(document).ready(function(){

	if (window.location.pathname.indexOf("federal") > -1) {
		setupScrollTriggers([
			["ScrollingTag|now-trending-start|25%",".features-modules"],
			["ScrollingTag|solution spotlight-start|50%",".solutions-center-title"],
			["ScrollingTag|deal of the week-start|75%",".weekly-deals"],
			["ScrollingTag|footer|100%",".footerContainer"]
		],
			"HOMEPAGE-FEDERAL", 0
		);
	}	
	else if (window.location.pathname.indexOf("small-business") > -1) {
		setupScrollTriggers([
			["ScrollingTag|now-trending-start|25%",".features-modules"],
			["ScrollingTag|solution spotlight-start|50%",".solutions-center-title"],
			["ScrollingTag|deal of the week-start|75%",".weekly-deals"],
			["ScrollingTag|footer|100%",".footerContainer"]
		],
			"HOMEPAGE-SMALLBUSINESS", 0
		);
	}
	else if (window.location.pathname.indexOf("startups") > -1) {
		setupScrollTriggers([
			["ScrollingTag|start of sixth article-chicago startup|50%",".resources-list li:nth-of-type(6)"],
			["ScrollingTag|footer|100%",".footerContainer"]
		],
			"HOMEPAGE-STARTUPS", 0
		);
	}
	else if (window.location.pathname.indexOf("legal") > -1) {
		setupScrollTriggers([
			["ScrollingTag|Software-start|50%",".icon-software-48"],
			["ScrollingTag|footer|100%",".footerContainer"]
		],
			"HOMEPAGE-LEGAL", 0
		);
	}
	else if (window.location.pathname.indexOf("financial-services") > -1) {
		setupScrollTriggers([
			["ScrollingTag|about-cdw-financial-services|25%","#About"],
			["ScrollingTag|banks-start|50%","#Banks"],
			["ScrollingTag|capital-markets-start|75%","#CapitalMarkets"],
			["ScrollingTag|footer|100%",".footer-wrapper"]
		],
			"HOMEPAGE-FINANCIAL-SERVICES", 0
		);
	}
	else if (window.location.pathname.indexOf("energy-utilities") > -1) {
		setupScrollTriggers([
			["ScrollingTag|about-cdw-energy|25%","#About"],
			["ScrollingTag|oil and gas-start|50%","#OilGas"],
			["ScrollingTag|partners who get it-start|75%","#partners"],
			["ScrollingTag|footer|100%",".footer-wrapper"]
		],
			"HOMEPAGE-ENERGY-UTILITIES", 0
		);
	}
	else if (window.location.pathname.indexOf("state-local") > -1) {
		setupScrollTriggers([
			["ScrollingTag|now-trending-start|25%",".features-modules"],
			["ScrollingTag|solution spotlight-start|50%",".solutions-center-title"],
			["ScrollingTag|deal of the week-start|75%",".weekly-deals"],
			["ScrollingTag|footer|100%",".footerContainer"]
		],
			"HOMEPAGE-STATE-LOCAL", 0
		);
	}
	else if (window.location.pathname.indexOf("k-12-education") > -1) {
		setupScrollTriggers([
			["ScrollingTag|now-trending-start|25%",".features-modules"],
			["ScrollingTag|solution spotlight-start|50%",".solutions-center-title"],
			["ScrollingTag|deal of the week-start|75%",".weekly-deals"],
			["ScrollingTag|footer|100%",".footerContainer"]
		],
			"HOMEPAGE-K12-EDUCATION", 0
		);
	}
	else if (window.location.pathname.indexOf("higher-education") > -1) {
		setupScrollTriggers([
			["ScrollingTag|now-trending-start|25%",".features-modules"],
			["ScrollingTag|solution spotlight-start|50%",".solutions-center-title"],
			["ScrollingTag|deal of the week-start|75%",".weekly-deals"],
			["ScrollingTag|footer|100%",".footerContainer"]
		],
			"HOMEPAGE-HIGHER-EDUCATION", 0
		);
	}
	else if (window.location.pathname.indexOf("healthcare") > -1) {
		setupScrollTriggers([
			["ScrollingTag|now-trending-start|25%",".features-modules"],
			["ScrollingTag|solution spotlight-start|50%",".solutions-center-title"],
			["ScrollingTag|deal of the week-start|75%",".weekly-deals"],
			["ScrollingTag|footer|100%",".footerContainer"]
		],
			"HOMEPAGE-HEALTHCARE", 0
		);
	}
	else if (window.location.pathname.indexOf("nonprofit") > -1) {
		setupScrollTriggers([
			["ScrollingTag|nonprofit services-start|50%",".panes"],
			["ScrollingTag|footer|100%",".footerContainer"]
		],
			"HOMEPAGE-NONPROFIT", 0
		);
	}
	else {
		setupScrollTriggers([
			["ScrollingTag|slider-end|25%",".slide-pagination"],
			["ScrollingTag|solution spotlight-start|50%",".solutions-center-title"],
			["ScrollingTag|deal of the week-start|75%",".weekly-deals"],
			["ScrollingTag|footer|100%",".footerContainer"]
		],
			"Homepage-CDW-Welcome", 0
		);
	}


	
});  

function setupScrollTriggers(scrollTriggers, scrollPageTitle, autoAddNav) {
	var windowHeight = $(window).height();
	var windowScroll = $(window).scrollTop();
	
	var scrollTriggerTop = [];
	var scrollTriggerState = [];
	
	// Automatically add solutions template side nav items
	if (autoAddNav == 1) {
		$(".target_side_nav").each(function() { 
			scrollTriggers.push([$(this).text(),"#" + $(this).attr("id")]);
		});
	}
	
	// Set all triggered states to 0 & set all trigger heights
	for (i=0; i<scrollTriggers.length; i++) {
		scrollTriggerTop[i] = $(scrollTriggers[i][1]).offset().top - windowHeight;
		scrollTriggerState[i] = 0;
	}
	
	// Trigger all tagging on page load if page is already scrolled down
	for (i=0; i<scrollTriggers.length; i++) {
		if (windowScroll >= scrollTriggerTop[i] && scrollTriggerState[i] == 0) {
			fireTagging(scrollTriggers[i][0],i);
		}
	}

	window.onscroll = function (e) {  
		// Trigger tagging when document is scrolled to trigger height
		for (i=0; i<scrollTriggers.length; i++) {
			if ($(document).scrollTop() >= scrollTriggerTop[i] && scrollTriggerState[i] == 0) {
				fireTagging(scrollTriggers[i][0],i);
			}
		}
	}
	
	function fireTagging(tagName, tagNumber) {
		CdwTagMan.createElementPageTag(scrollPageTitle, scrollTriggers[tagNumber][0]);
		console.log("tagging: " + scrollTriggers[tagNumber][0]);
		scrollTriggerState[tagNumber] = 1;
	}
	
}