$(function () {
    /* Slideshow */
    $('.slideshow').cycle({
        "next": ".slide-next",
        "prev": ".slide-prev",
        "slides": ">div",
        "timeout": 5500,
        "pauseOnHover": true,
        "log": false,
    });

    $('.slide-pagination a').click(function (e) {
        e.preventDefault();

        var $btn = $(this),
            index = $('.slide-pagination a').index($btn);
            
        trackSlide('TabSelect');

        $('.slideshow').cycle('goto', index);

        //Hide Segment "Slide"
        if ($('#segment-dropdown').css('display') === 'block') {
            $('#segment-dropdown').stop(true, true).fadeOut(400);
        }
    });

    $('.slideshow').on('cycle-before', function (event, opts) {
        $('.slide-pagination a:eq(' + opts.nextSlide + ')').addClass('active').siblings().removeClass('active');

        //Get Previous Slide
        $('.slideshow').data('prevSlide', opts.currSlide);
    });

    /* Track Prev/Next */
    $('.slideshow').on('cycle-prev', function (event, opts) { trackSlide('Left'); });
    $('.slideshow').on('cycle-next', function (event, opts) { trackSlide('Right'); });

    //Format Slide Name for tracking
    function trackSlide(action) {
        var index = $('.slideshow').data('prevSlide'),
            name = $('.slide-pagination a:eq(' + index + ')').text().replace(/ /g, '').replace(/|/g, '');

        CdwTagMan.createElementPageTag($('.slideshow').data('segment'), 'Slide' + (index + 1) + '|' + name + '|' + action);
    }

    /* Scroll To */
    $('.js-scroll-to').on('click', function () {
        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top
        }, 500);
        return false;
    });

    /* Tabs */
    $('.tab-links li a').on('click', function (e) {
        e.preventDefault();

        //Get Index 
        var $link = $(this),
            $li = $link.parent('li'),
            index = $('.tab-links li').index($li),
            placement = $link.data('placement'),
            category = $link.data('category');

        //Show tab 
        $('.tab-content .tab:eq(' + index + ')').show().siblings().hide();

        //Add active state 
        $li.addClass('active').siblings().removeClass('active');

        if (placement != undefined && category != undefined) {
            R3_COMMON.placementTypes = placement;
            R3_COMMON.categoryHintIds = category;
            R3_COMMON.addClickthruParams(0, 'RecommendedForEDC=00000001&RecoType=RH&cm_sp=homepage-_-WaysToShop-_-DataStorage&ProgramIdentifier=3');
            r3();
        }
    });
    $('.tab-links li:eq(0) a').trigger('click');


    /* Segment Select */
    var $dd = $('#segment-dropdown');
    $('.segment-select a').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();

        $dd.stop(true, true).fadeToggle(400);
    });

    $dd.on('click', function (e) {
        e.stopPropagation();
    })

    $('body').on('click', function () {

        if ($dd.css('display') === 'block') {
            $dd.stop(true, true).hide();
        }
    })

    //$("#pnlRecntView").loadRecentlyViewedPrices(); 
    
    /* Solution Center */
    $("#modal-expert").dialog({
        autoOpen: false, width: 450, modal: true,
        open: function (ev, ui) {
            var $iframe = $("#modal-expert > iframe:eq(0)");
            if ($iframe.attr('src') === "") {
                $iframe.attr('src', $iframe.data('src'));
            }
        }
    });

    $(".btn-modal").click(function (e) {
        e.preventDefault();
        var $modal = $($(this).attr('href'));

        
        $modal.dialog('open');
        $(".ui-widget-overlay").bind("click", closeDialogWindowOnOverlayClick);
    });


     var closeDialogWindowOnOverlayClick = function(event){
         var closeButton = $(".ui-dialog:visible").find(".ui-dialog-titlebar-close");
         closeButton.trigger("click");
         $(".ui-widget-overlay").unbind("click", closeDialogWindowOnOverlayClick);
     }

});

/* Rich Relevance Popular Products Placements */
var R3_COMMON = new r3_common();
R3_COMMON.setClickthruServer(window.location.protocol + '//' + window.location.host);
R3_COMMON.setApiKey('c1b6edde3bea10a0');
R3_COMMON.setBaseUrl(window.location.protocol + '//recs.richrelevance.com/rrserver/');
R3_COMMON.setSessionId('72788A3301B54CE2A42DA420A3E36554');
R3_COMMON.setUserId('5428E218-6333-46E2-BE7F-0DEB216E6E56');
R3_COMMON.addPlacementType('http://www.cdw.com/content/homepage/assets/js/home_page.rr2');
R3_COMMON.addPlacementType('http://www.cdw.com/content/homepage/assets/js/home_page.rr3');
R3_COMMON.addPlacementType('http://www.cdw.com/content/homepage/assets/js/home_page.rr4');
R3_COMMON.addPlacementType('http://www.cdw.com/content/homepage/assets/js/home_page.rr5');
R3_COMMON.addPlacementType('http://www.cdw.com/content/homepage/assets/js/home_page.rr6');
R3_COMMON.addClickthruParams(0, 'RecommendedForEDC=00000001&RecoType=RH&cm_sp=homepage-_-WaysToShop&ProgramIdentifier=3');
var _obj_HomeBlock = new r3_home();
r3();
var RichRelevanceConfig = { suppressDynamicPricing: false, maxServiceDelay: 5000, noPriceValueString: 'Call', suppressDynamicPriceLabels: false, pageTypeValue: 'homepage', programValue: 'WaysToShop', scrollGroupSize: '4', showCompare: 'False' };