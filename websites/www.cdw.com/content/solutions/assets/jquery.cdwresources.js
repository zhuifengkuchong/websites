
var cdwresourcesIsIe8 = ($.browser.msie && $.browser.version <= 8);

jQuery.fn.cdwresources = function(options){

	var elem = $(this);
	var resourcesXML = false;
	var unsortedResources = false;
	var resourcesFetched = false; 
	
	//Create the spinner element and show it.
	var spinner = $('<div>').addClass('spinner').text('loading...').appendTo(elem).hide();
	spinner.show();

	var settings = $.extend(true, {
	  view : 'list', //table or list, default to list for simplicity
	   results_per_page : false,
	   resources_xml_url : 'http://www.cdw.com/content/solutions/assets/allresources.xml',
	   sortDir:'desc',
	   sortBy:'date',
	   site:false,
	   category:false, 
	   type:false,
	   featured:false,
	   industry:false,
	   location:false,
	   segment:false,
	   data:false,
	   brand:false,
	   exclude:{
	   	featured:false,
		site:false,
		category:false,
		type:false,
		industry:false,
		location:false,
		segment:false,
		data:false,
		brand:false
	   },
	   page:1
	}, options);
	
	var state = {};

	var defaultSortDir = settings.sortDir;
	var defaultSortBy = settings.sortBy;

	//If the element has the "stateChanged" event fired on it, redraw the table with the most current options (table view only)
	elem.bind('stateChanged',function(e,extraData){
		
		var oldState = state;
		var newState = {};
		
		//read select dropdowns state
		filter_elem.children('div').children('select').each(function(){
			var selectedOption = $(this).children('option:selected');
			var filterfield = $(this).data('filterfield');
			if(selectedOption.val() != 'default')
				newState[filterfield] = selectedOption.text();
		});		
		
		//read hidden inputs sortdir, sortby state	
		elem.find('.sortDir-state').each(function(){
			newState.sortDir = $(this).val();	
		});
		
		elem.find('.sortBy-state').each(function(){
			newState.sortBy = $(this).val();	
		});

		if(typeof extraData != 'undefined' && typeof extraData.resetPage != 'undefined' && extraData.resetPage){
			elem.find('.page-state').val("1");
		}
		
		elem.find('.page-state').each(function(){
			newState.page = $(this).val();	
		});		
		
	
		//if(newState != oldState)
		//	newState['page'] = 1;
			
		state = newState;
			
		spinner.show();

		//refresh table, giving enough time to show spinner
		setTimeout(function(){
			refresh_table(newState);
		},50);

		//adjust query string
		adjustQuerystring();
		
	});
	
	var pagination_elem;
	var table_elem;
	var list_elem;
	var filter_elem;	
	
	var current_page = -1;
	var current_sort_dir = false;
	var current_sort_by = false;
	
	// function on_xml_ready
	// parameters: none
	// callback after loading in the XML file,
	function on_xml_ready(){	
		if(settings.view == 'table'){
			initialize_table();

			//var refresh_options = {}
			var ho = queryHash();
			//if(ho.page != 'undefined'){
			//	refresh_options.page = ho.page;
			//}else{
			//	refresh_options.page = 1;
			//}


	
			if(typeof ho.sortby != 'undefined'){
				$('.sortBy-state').val(ho.sortby)
			}
			
			if(typeof ho.sortdir != 'undefined'){
				$('.sortDir-state').val(ho.sortdir);
			}	

			if(typeof ho.page != 'undefined'){
				$('.page-state').val(ho.page);
			}	

			//take a deep breath...
			setTimeout(function(){
				elem.trigger('stateChanged',{resetPage:false});
			},750);

		}else if(settings.view=='list'){
			initialize_list();
			refresh_list();
		}
	}

	// function get_current_options
	// parameters: overrides - options object
	// In table view, gets the current values of the drop downs
	// and combines them with settings in order to determine what
	// options to use for the current page view. returns those options.	
	function get_current_options(overrides){
		if(typeof overrides == 'undefined'){
			overrides = {}
		}
		
		//console.log('settings',settings);

		var options = $.extend(true,{},settings,overrides);
		
		return options;
		
	}

	function adjustQuerystring(){

		var params = [];

		if(state.page > 1){
			params = params.concat([['page',state.page]]);
		}

		if(state.sortDir && state.sortDir != defaultSortDir){
			params = params.concat([['sortdir',state.sortDir]]);
		}

		if(state.sortBy && state.sortBy != defaultSortBy){
			params = params.concat([['sortby',state.sortBy]]);
		}
		
		
		var filterFields = ['category','type','industry','brand','location'];

		//filter_elem.find('select').each(function(i,o){
		//	filterFields.push($(this).data('filterfield'));
		//})
		
		var param;

		var filterFieldsLength = filterFields.length;
		for(var i=0;i<filterFieldsLength;i++){
			param = [];
			if(typeof state[filterFields[i]] != 'undefined'){
				param[0] = filterFields[i];
				param[1] = state[filterFields[i]].replace('&','');
	
				if(param[1] != null && param[1] != 'default'){
					params = params.concat([param]);
				}
			}
		}
		
		var querystring = '';
		var queryparamct = 0;

		for (i=0;i<params.length;i++){


			if(queryparamct > 0 ){
				querystring += '&';
			}else{
				querystring += '#';
			}

			querystring += params[i][0]+'='+encodeURIComponent(params[i][1]);

			queryparamct++;
		}

		//throttle this function because IE8 is terrible at changing query strings
		setTimeout(function(){
			if(querystring != ''){
				window.location.hash = querystring;
			}else{
				window.location.hash = '#!';
			}
		},250);

	}
	

	/*======================================
	
		CONVENIENCE FUNCTIONS
		-arrayContains
		-eliminateDuplicates
		-isNumber
	
	========================================*/


	// function arrayContains
	// arguments:
	//   -a: an array
	//	 -obj: an object we are search for in the array
	// return: whether the array contains that object
	function arrayContains(a, obj) {
		var i = a.length;
		while (i--) {
		   if (a[i] === obj) {
			   return true;
		   }
		}
		return false;
	}


	// function eliminateDuplicates
	// arguments:
	//   -arr: an array which may or may not have duplicate values
	// return: same array with all duplicate values removed
	// This function is used to remove all duplicate values from an array.
	
	function eliminateDuplicates(arr) {
	  var i,
		  len=arr.length,
		  out=[],
		  obj={};
	
	  for (i=0;i<len;i++) {
		obj[arr[i]]=0;
	  }
	  for (i in obj) {
		out.push(i);
	  }
	  return out;
	}
	


	// function isNumber
	// arguments:
	//   -n: something that may or may not be a number
	// return: boolean
	// This is a convenience function to validate whether a value is a number
	
	function isNumber(n) {
	  return !isNaN(parseFloat(n)) && isFinite(n);
	}	



	/*======================================
	
		LIST SPECIFIC FUNCTIONS
		
		-refresh_list
		-initialize_list
	
	========================================*/


	// function initialize_list
	// arguments: none
	// return: none
	// create list UL and append to main element
	function initialize_list(){
		list_elem = $('<ul>').addClass('resources-list iconlist');
		elem.append(list_elem);	
	}


	// function refresh_list
	// arguments:
	//   overrides- override default settings
	// return: none
	// redraw list based on current settings
	function refresh_list(overrides){
		if(typeof overrides == 'undefined')
			overrides = {}

		var list_options = $.extend(true,{},settings,overrides);
		
		var filtered = filter_xml(list_options);
		
		list_elem.html('');
			
		//for each of the filtered results, build a row and insert into the container

		var resources,resourcesTotal,resourcesIterator,resource;

		resources = filtered.resultset;
		resourcesTotal = resources.length;


		var resourceObj,displayTitle,li,a;

		$(resources).each(function(resourcesIterator,o){
			
			var resourceObj = convert_resource_to_object($(resources[resourcesIterator]));
						
			var displayTitle = resourceObj.title;
			
			if(resourceObj.resourceType != false){
				displayTitle = resourceObj.resourceType + ': ' + displayTitle;	
			}
			
			if(resourceObj.isNew){
				displayTitle = displayTitle + ' <span class="new">- New</span>';
			}
						
			var li = $('<li>');
			
			if(resourcesIterator == (filtered.resultset.length - 1))
				li.addClass('last-child');
			
			var a = $('<a>').attr('href',resourceObj.url );
				if(resourceObj.resourceType == 'pdf'){
					a.attr({href:resourceObj.url,target:'_blank'});
				}
				a.appendTo(li);		 	


			if(true || resourceObj.mediaType =='page' || resourceObj.mediaType  == 'external' || resourceObj.mediaType  == 'pdf' || resourceObj.mediaType  == 'video'){
				a.attr({href:resourceObj.url,target:'_blank'});
			}

			if(resourceObj.resourceType != 'webinar'){
				a.click(function(){
					
					// Core Tag (CM Tag) for resources on click 
					var url = window.location.pathname; 						// get file path from url
					var filename = url.substring(url.lastIndexOf('/')+1); 		// get the file name from path
					filename = filename.substring(0, filename.length - 5) 		// trim out file extension 
					var directory = url.substring(0, url.lastIndexOf('/')); 	// get directory
					directory = directory.split("/"); 							// split directory into an array
					var lastDir = directory[directory.length-1];				// pick the last intex of the directory array

					cmCreatePageviewTag(lastDir + '|' + filename + '|' + resourceObj.title + '|' + resourceObj.resourceType.toLowerCase() + '|' + resourceObj.data , resourceObj.mediaType);
					return true;
				})
			}
			
			var icon = $('<span>').addClass('icon icon-32').appendTo(a);
			var text = $('<span>').addClass('text').html(displayTitle).appendTo(a);
			
			
			if (resourceObj.mediaType == 'pdf'){
				icon.addClass('icon-pdf-32');
			}else if (resourceObj.mediaType == 'external'){
				icon.addClass('icon-external_link-32');
			}else if (resourceObj.mediaType == 'page'){
				icon.addClass('icon-document-32');
			}else if (resourceObj.mediaType == 'video'){
				icon.addClass('icon-video-32');
			}else if (resourceObj.mediaType == 'link'){
				icon.addClass('icon-link-32');
			}
			
			list_elem.append(li);
			
		});
		
		spinner.hide();
	
	}

	/*======================================
	
		TABLE SPECIFIC FUNCTIONS
		
		-refresh_table
		-initialize_table
		-refresh_pagination
		-reset_filters
	
	========================================*/


	// function initialize_table
	// arguments: none
	// return: none
	// create elements for table vie and append them to the main element
	function initialize_table(){
		table_elem = $('<div>').addClass('resources-table');
		filter_elem = $('<div>').addClass('resources-filters');  
		pagination_elem = build_pagination();
		
		table_elem.html(build_table());
		filter_elem.html(build_filters());
		
		elem.append(filter_elem);
		elem.append(pagination_elem.clone());
		elem.append(table_elem);
		elem.append(pagination_elem.clone());
		
		elem.append(
			$('<input/>').attr({type:'hidden'}).addClass('sortBy-state').val(settings.sortBy)
		).append(
			$('<input/>').attr({type:'hidden'}).addClass('sortDir-state').val(settings.sortDir)
		).append(
			$('<input/>').attr({type:'hidden'}).addClass('page-state').val(settings.page)
		)
	}

	// function refresh_table
	// arguments:
	//   overrides- override default settings
	// return: none
	// redraw table based on current settings
	function refresh_table(overrides){
		
		spinner.show();
		
		var table_options = get_current_options(overrides),
			table = table_elem.children('table').eq(0),
			schedule_delete = table.children().children('tr.row-normal'),
			filtered = filter_xml(table_options);
		
		if(filtered.resultset.length == 0){
			var resetlink = $('<a>',{href:'#',text:'Click here to reset filters',click:function(e){e.preventDefault();reset_filters()}})
			table.append($('<tr>').addClass('row-normal row-reset').append($('<td>').attr('colspan','7').css({textAlign:'center'}).append('There were no results found for your search. ').append(resetlink)))	
		}


		$(filtered.resultset).each(function(){

			var thisTableRow = $('<tr>').addClass('row-normal'),
				tableCellTemplate = $('<td>').text(' '),
				resourceObj = convert_resource_to_object($(this)),
				displayTitle = resourceObj.title,
				titleHtml;
			
			if(resourceObj.isNew){
				displayTitle = displayTitle + ' - <span class="new">New</span>';
			}
				
			titleHtml = $('<a>').html(displayTitle).attr({href:resourceObj.url});
			
			
			if(true || resourceObj.mediaType=='page' || resourceObj.mediaType == 'external' || resourceObj.mediaType == 'pdf' || resourceObj.mediaType == 'video'){
				titleHtml.attr({href:resourceObj.url,target:'_blank'});
			}
			
			if(resourceObj.resourceType != 'webinar'){
				titleHtml.click(function(){
					//cmCreatePageviewTag(lastDir+'|'+filename+'|'+resourceObj.title+'|'+resourceObj.resourceType,resourceObj.mediaType);
					cmCreatePageviewTag('solutions|media-library|'+resourceObj.title+'|'+resourceObj.resourceType,resourceObj.mediaType);
					// alert('click');
				//	cmCreatePageviewTag(rawTitle+'|'+resourceType,mediaType);
					return true;
				})
			}
			

			
			thisTableRow.append(tableCellTemplate.clone().addClass('first-child').append(resourceObj.categories.join('<br/>')))
			thisTableRow.append(tableCellTemplate.clone().append(titleHtml));
			thisTableRow.append(tableCellTemplate.clone().append(resourceObj.resourceType))
			thisTableRow.append(tableCellTemplate.clone().append(resourceObj.industries.join('<br/>')))
			thisTableRow.append(tableCellTemplate.clone().append(resourceObj.brands.join('<br/>')))
			thisTableRow.append(tableCellTemplate.clone().append(resourceObj.locations.join('<br/>')))
			thisTableRow.append(tableCellTemplate.clone().append(resourceObj.date))
			table.append(thisTableRow);
		})
		
		schedule_delete.remove();


		//console.log('table_options',table_options)
		//adjust view		
		refresh_pagination(filtered.pagination);

		//adjust view
			
		
		var myTH = table_elem.find('.th-'+table_options.sortBy);
								
		myTH.siblings().andSelf().removeClass('selected selected-asc selected-desc');

		myTH.addClass('selected selected-'+table_options.sortDir);


		current_page = filtered.pagination.current_page;
		current_sort_dir = table_options.sortDir;
		current_sort_by = table_options.sortBy;
		
		spinner.hide();
	
	}
	
	// function refresh_pagination
	// arguments:
	//   pagination- the result.pagination object from the filter_xml function
	// return: none
	// redraw the media library pagination with current information
	function refresh_pagination(pagination){

		var pghtml = '';
		
		var paginationType = 'abbreviated'; //full or abbreviated
		
		if(paginationType == 'full'){
	
			if(pagination.current_page > 1){
				pghtml += '<a href="#" data-pagenum="'+ (parseInt(pagination.current_page) - 1) +'"  class="link-previous">Previous</a>';
				pghtml += '<span class="pipe">|</span>';
			}
	
			for(j = 1;j<=pagination.num_pages;j++){
				pghtml += '<a href="#page-'+j+'" data-pagenum="'+j+'" class="pagelink';
				if(j == pagination.current_page)
					pghtml += ' current ';
				
				pghtml += '">'+j+'</a>';
			}
	
			if(pagination.current_page < pagination.num_pages){
				pghtml += '<span class="pipe">|</span>';
				pghtml += '<a href="#" data-pagenum="'+ (parseInt(pagination.current_page) + 1) +'" class="link-next">Next</a>';
			}

		}else if(paginationType == 'abbreviated'){
////////////////

    var $num_pages = parseInt(pagination.num_pages),
    $cur_page = parseInt(pagination.current_page),
    $page_links = '',
    $search_function = 'gogogogo',
    $classical_guitarL = '<',
    $classical_guitarR = '>';

    // If this page is not the first page, generate the "previous" link
    if ($cur_page > 1)
    {
        $page_links += '<a href="#page-'+($cur_page-1)+'" data-pagenum="'+($cur_page-1)+'" class="pagelink">' + 'Previous' + "</a> ";
    }
    else
    {
        $page_links += '';
    }

    var $pager_num = 3; // How many page number you wish to display to the left and right sides of the current page
    var $index_start = ($cur_page - $pager_num) <= 0 ? 1 : $cur_page - $pager_num;
    var $index_finish = (($cur_page + $pager_num) >= $num_pages) ? $num_pages : $cur_page + $pager_num;


    
    if (($cur_page - $pager_num) > 1) { $page_links += '<a href="#page-'+('1')+'" data-pagenum="'+('1')+'" class="pagelink">' + '1' + "</a> " + '<span class="pipe">...</span>'; } // Display ... when there are more page items than $page_num

    // Loop through the pages generating the page number links
    // NOTE: I've modified the for index pointers here...
    for ($i = $index_start; $i <= $index_finish; $i++)
    {
        if ($cur_page == $i)
        {
            $page_links += ' <span class="pipe">' + $i + '</span>';
        }
        else
        {
        $page_links += '<a href="#page-'+($i)+'" data-pagenum="'+($i)+'" class="pagelink">' + $i + "</a> ";

        }
    }

    if (($cur_page + $pager_num) < $num_pages) { $page_links += '<span class="pipe">...</span>' +'<a href="#page-'+($num_pages)+'" data-pagenum="'+($num_pages)+'" class="pagelink">' + $num_pages + "</a> "; } // Display ... when there are more page items than $page_num

    // If this page is not the last page, generate the "next" link
    if ($cur_page < $num_pages)
    {
        $page_links += '<a href="#page-'+($cur_page+1)+'" data-pagenum="'+($cur_page+1)+'" class="pagelink">' + 'Next' + "</a> ";

    }
    else
    {
        $page_links += '';
    }

    pghtml = $page_links;




///////////////			
		}
		
		

		var pagination_elems = elem.find('.resources-pagination');
		
		if(pagination.totalresults > 0){
			pagination_elems.find('.pagination-info').html(pagination.first_result + ' - ' +pagination.last_result+' of '+pagination.totalresults+' results');
		}else{
			pagination_elems.find('.pagination-info').html('0 results');
		}
		pagination_elems.find('.pagination-links').html(pghtml);
		
		
		pagination_elems.append(
			$('<div>').css('clear','both')
		).find('a').click(function(e){	
			e.preventDefault();
			var pagenum = $(this).data('pagenum');
			$('.page-state').val(pagenum);
			elem.trigger('stateChanged',{resetPage:false});
		})
		
	
	}

	
	//function reset_filters
	// arguments: none
	// return: none
	// Reset all the filter drop downs. Change their values back to default,
	// non-filtering values, then refresh the table which will detect the new
	// drop down values.
	function reset_filters(){
		var filters = $('.resources-filters');
		filters.children('div').children('select').val('default');
		
		filters.children('div').children('.fancySelect').each(function(){
			var defaultText = $(this).find('.dropDown li.default').text()
			$(this).find('.selectBox span').text(defaultText).addClass('default');	
		})
		
		
		//
		$('.sortDir-state').val('desc');
		
		$('.sortBy-state').val('date');
		
		$('.page-state').val(1);


		elem.trigger('stateChanged',{resetPage:true});
	}

	/*======================================
	
		DATA FUNCTIONS
		
		-filter_xml
		-convert_resource_to_object
	
	========================================*/	




	// function filter_xml
	// arguments:
	//   -options: an options object containing filters and excludes
	// return: object with two properties, pagination and result set
	//   pagination contains information for the pagination elements to display
	//   result set is a query object containing all of the raw XML elements of
	//   matched resources
	// This function searches through the xml file based on the contents of
	// an option array and returns all matches

	function filter_xml(options){
		
		var resources,resourcesTotal,resourcesIterator,resource;

		var results = []; //empty jquery object for results
		
		resources = unsortedResources.clone();
		
		//console.log(resources.size());
		var filtersEngaged = false;

		//CACHE FILTER SPLITS AND EXISTS

		//site:false,
		if(options.site != false){
			options.siteIsMulti = (options.site.indexOf(",") != -1);
			if(options.siteIsMulti)
				options.siteMultifilter = options.site.split(',');
		}
		//category:false, 
		if(options.category != false){
			options.categoryIsMulti = (options.category.indexOf(",") != -1);
			if(options.categoryIsMulti)
				options.categoryMultifilter = options.category.split(',');
		}
		//type:false,
		if(options.type != false){
			options.typeIsMulti = (options.type.indexOf(",") != -1);
			if(options.typeIsMulti)
				options.typeMultifilter = options.type.split(',');
		}
		//featured:false,

		//industry:false,
		if(options.industry != false){
			options.industryIsMulti = (options.industry.indexOf(",") != -1);
			if(options.industryIsMulti)
				options.industryMultifilter = options.industry.split(',');
		}
		//location:false,
		if(options.location != false){
			options.locationIsMulti = (options.location.indexOf(",") != -1);
			if(options.locationIsMulti)
				options.locationMultifilter = options.location.split(',');
		}
		//segment:false,
		if(options.segment != false){
			options.segmentIsMulti = (options.segment.indexOf(",") != -1);
			if(options.segmentIsMulti)
				options.segmentMultifilter = options.segment.split(',');
		}

		//data:false,
		if(options.data != false){
			options.dataIsMulti = (options.data.indexOf(",") != -1);
			if(options.datatIsMulti)
				options.dataMultifilter = options.data.split(',');
		}


		//brand:false,
		if(options.brand != false){
			options.brandIsMulti = (options.brand.indexOf(",") != -1);
			if(options.brandIsMulti)
				options.brandMultifilter = options.brand.split(',');
		}

		//
		// EXCLUDE
		//

		//site:false,
		if(options.exclude.site != false){
			options.exclude.siteIsMulti = (options.exclude.site.indexOf(",") != -1);
			if(options.exclude.siteIsMulti)
				options.exclude.siteMultifilter = options.exclude.site.split(',');
		}
		//category:false, 
		if(options.exclude.category != false){

			options.exclude.categoryIsMulti = (options.exclude.category.indexOf(",") != -1);
			if(options.exclude.categoryIsMulti)
				options.exclude.categoryMultifilter = options.exclude.category.split(',');
		}
		//type:false,
		if(options.exclude.type != false){

			options.exclude.typeIsMulti = (options.exclude.type.indexOf(",") != -1);
			if(options.exclude.typeIsMulti)
				options.exclude.typeMultifilter = options.exclude.type.split(',');
		}
		//featured:false,

		//industry:false,
		if(options.exclude.industry != false){

			options.exclude.industryIsMulti = (options.exclude.industry.indexOf(",") != -1);
			if(options.exclude.industryIsMulti)
				options.exclude.industryMultifilter = options.exclude.industry.split(',');
		}
		//location:false,
		if(options.exclude.location != false){
			options.exclude.locationIsMulti = (options.exclude.location.indexOf(",") != -1);
			if(options.exclude.locationIsMulti)
				options.exclude.locationMultifilter = options.exclude.location.split(',');
		}
		//segment:false,
		if(options.exclude.segment != false){
			options.exclude.segmentIsMulti = (options.exclude.segment.indexOf(",") != -1);
			if(options.exclude.segmentIsMulti)
				options.exclude.segmentMultifilter = options.exclude.segment.split(',');
		}
		//brand:false,
		if(options.exclude.brand != false){
			options.exclude.brandIsMulti = (options.exclude.brand.indexOf(",") != -1);
			if(options.exclude.brandIsMulti)
				options.exclude.brandMultifilter = options.exclude.brand.split(',');
		}


		resourcesTotal = resources.length;

		for(resourcesIterator = 0;resourcesIterator<resourcesTotal;resourcesIterator++){

			var localMatch;				
			var thisResource = $(resources[resourcesIterator]);
			
			//Start assuming we have a match
			var isMatch = true;
			var doExclude = false;
			
			var nodes,total,i,txt;
						
			if(options.featured != false){
				localMatch = false;
				filtersEngaged = true;

				var featuredAttr = thisResource.attr('featured');
				
				// For some browsers, `attr` is undefined; for others,
				// `attr` is false.  Check for both.
				if (typeof featuredAttr !== 'undefined' && featuredAttr !== false && featuredAttr == 'featured') {
					localMatch = true;
				}

					
				if(localMatch){	
					//match is still true
				}else{
					isMatch = false;
				}

			}

			//INCLUDE CATEGORY
			if(options.category != false){
				
				localMatch = false;
				filtersEngaged = true;
				
				nodes = thisResource.children('categories').children('category');
				total = nodes.length;
				for(i=0;i<total;i++){

					txt = $(nodes[i]).text();

					if(options.categoryIsMulti){
												
						for(jj = 0;jj<options.categoryMultifilter.length;jj++){
							if(txt == options.categoryMultifilter[jj]){
								localMatch = true;	
							}
						}
						
					}else if(txt == options.category){
						localMatch = true;
						break; //break locations.each
					
					}
				}
				
				if(!localMatch){
					isMatch = false;
				}
			}	

			//EXLCUDE CATEGORY
			if(options.exclude.category != false){
				filtersEngaged = true;

				nodes = thisResource.children('categories').children('category');
				total = nodes.length;
				for(i=0;i<total;i++){

					txt = $(nodes[i]).text();

					if(options.exclude.categoryIsMulti){
												
						for(jj = 0;jj<options.exclude.categoryMultifilter.length;jj++){
							if(txt == options.exclude.categoryMultifilter[jj]){
								doExclude = true;	
							}
						}
						
					}else if(txt == options.exclude.category){
						doExclude = true;
						break; //break industries.each
					}
				}
				
				if(doExclude){
					continue;
					isMatch = false;
				}
			}


			
						
			//INCLUDE BY SITE
			if(options.site != false){
				localMatch = false;
				filtersEngaged = true;

				nodes = thisResource.children('sites').children('site');
				total = nodes.length;
				for(i=0;i<total;i++){
				  	txt = $(nodes[i]).text();

					if(options.siteIsMulti){
						
						
						for(jj = 0;jj<options.siteMultifilter.length;jj++){
							if(txt == options.siteMultifilter[jj]){
								localMatch = true;	
							}
						}
						
					}else if(txt == options.site){
						localMatch = true;
						//alert()
						break; //break locations.each
			  		}
			  	}
			  
				if(!localMatch){
					isMatch = false;
				}			  
			  			
			}

			//EXCLUDE BY SITE
			if(options.exclude.site != false){
				filtersEngaged = true;

				nodes = thisResource.children('sites').children('site');
				total = nodes.length;
				for(i=0;i<total;i++){
				  	txt = $(nodes[i]).text();
				
					if(options.exclude.siteIsMulti){
						
						
						for(jj = 0;jj<options.exclude.siteMultifilter.length;jj++){
							if(txt == options.exclude.siteMultifilter[jj]){
								doExclude = true;	
							}
						}
						
					}else if(txt == options.exclude.site){
						doExclude = true;
						break; //break sites.each
					}
				}
				
				if(doExclude){
					continue;
					isMatch = false;
				}
			}

			
			//INCLUDE BY TYPE
			if(options.type != false){
				localMatch = false;
				filtersEngaged = true;
				var txt = thisResource.children('resourceType').text();
	
				if(options.typeIsMulti){
										
					for(jj = 0;jj<options.typeMultifilter.length;jj++){
						if(txt == options.typeMultifilter[jj]){
							localMatch = true;	
						}
					}
					
				}else if(txt == options.type){
					localMatch = true;
				}
				
				
				
				if(!localMatch){
					isMatch = false;
				}
			}

			//EXCLUDE BY TYPE
			if(options.exclude.type != false){
				filtersEngaged = true;
				var resourceType = thisResource.children('resourceType');
				
				if(options.exclude.typeIsMulti){
					
					
					for(jj = 0;jj<options.exclude.typeMultifilter.length;jj++){
						if(resourceType.text() == options.exclude.typeMultifilter[jj]){
							doExclude = true;	
						}
					}
					
				}else if(resourceType.text() == options.exclude.type){
					doExclude = true;
				}
				
				
				
				if(doExclude){
					continue;
					isMatch = false;
				}				
			}


			//INCLUDE BY INDUSTRY
			if(options.industry != false){
				localMatch = false;
				filtersEngaged = true;
				
				nodes = thisResource.children('industries').children('industry');				
				total = nodes.length;
				for(i=0;i<total;i++){
				  	var txt = $(nodes[i]).text();

					if(options.industryIsMulti){
												
						for(jj = 0;jj<options.industryMultifilter.length;jj++){
							if(txt == options.industryMultifilter[jj]){
								localMatch = true;	
							}
						}
						
					}else if(txt == options.industry){
						localMatch = true;
						break; //break industries.each
					}
				}
				
				if(!localMatch){
					isMatch = false;
				}
			}

			//EXCLUDE BY INDUSTRY
			if(options.exclude.industry != false){
				filtersEngaged = true;
				
				nodes = thisResource.children('industries').children('industry');
				total = nodes.length;
				for(i=0;i<total;i++){
				  	txt = $(nodes[i]).text();
				
					if(options.exclude.industryIsMulti){
												
						for(jj = 0;jj<options.exclude.industryMultifilter.length;jj++){
							if(txt == options.exclude.industryMultifilter[jj]){
								doExclude = true;	
							}
						}
						
					}else if(txt == options.exclude.industry){
						doExclude = true;
						break; //break industries.each
					}
				}
				
				if(doExclude){
					continue;
					isMatch = false;
				}
			}
			
			//INCLUDE BY LOCATION
			if(options.location != false){
				localMatch = false;
				filtersEngaged = true;
				
				nodes = thisResource.children('locations').children('location');
				total = nodes.length;
				for(i=0;i<total;i++){
				  	txt = $(nodes[i]).text();

					
					if(options.locationIsMulti){
												
						for(jj = 0;jj<options.locationMultifilter.length;jj++){
							if(txt == options.locationMultifilter[jj]){
								localMatch = true;	
							}
						}
						
					}else if(txt == options.location){
						localMatch = true;
						break; //break locations.each
					}
				}
				
				if(!localMatch){
					isMatch = false;
				}
			}

			//EXCLUDE BY INDUSTRY
			if(options.exclude.location != false){
				filtersEngaged = true;
				
				nodes = thisResource.children('locations').children('location');
				total = nodes.length;
				for(i=0;i<total;i++){
				  	txt = $(nodes[i]).text();

				
					if(options.exclude.locationIsMulti){
												
						for(jj = 0;jj<options.exclude.locationMultifilter.length;jj++){
							if(txt == options.exclude.locationMultifilter[jj]){
								doExclude = true;	
							}
						}
						
					}else if(txt == options.exclude.location){
						doExclude = true;
						break; //break locations.each
					}
				}
				
				if(doExclude){
					continue;
					isMatch = false;
				}
			}

			
			//INCLUDE BY BRAND
			if(options.brand != false){
				localMatch = false;
				filtersEngaged = true;

				nodes = thisResource.children('brands').children('brand');
				total = nodes.length;
				for(i=0;i<total;i++){
				  	txt = $(nodes[i]).text();

					
					if(options.brandIsMulti){
												
						for(jj = 0;jj<options.brandMultifilter.length;jj++){
							if(txt == options.brandMultifilter[jj]){
								localMatch = true;	
							}
						}
						
					}else if(txt == options.brand){
						localMatch = true;
						break; //break brands.each
					}
				}
				
				if(localMatch){
					//match is still true
				}else{
					isMatch = false;
				}
			}

			//EXCLUDE BY BRAND
			if(options.exclude.brand != false){
				filtersEngaged = true;
				
				nodes = thisResource.children('brands').children('brand');
				total = nodes.length;
				for(i=0;i<total;i++){
				  	txt = $(nodes[i]).text();

				
					if(options.exclude.brandIsMulti){
						
						
						for(jj = 0;jj<options.exclude.brandMultifilter.length;jj++){
							if(txt == options.exclude.brandMultifilter[jj]){
								doExclude = true;	
							}
						}
						
					}else if(txt == options.exclude.brand){
						doExclude = true;
						break; //break brands.each
					}
				}
				
				if(doExclude){
		  			continue;
					isMatch = false;
				}
			}


			//INCLUDE BY SEGMENT
			if(options.segment != false){
				localMatch = false;
				filtersEngaged = true;
				
				nodes = thisResource.children('segments').children('segment');
				total = nodes.length;
				for(i=0;i<total;i++){
				  	txt = $(nodes[i]).text();

					if(options.segmentIsMulti){
												
						for(jj = 0;jj<options.segmentMultifilter.length;jj++){
							if(txt == options.segmentMultifilter[jj]){
								localMatch = true;	
							}
						}
						
					}else if(txt == options.segment){
						localMatch = true;
						break; //break segment.each
					}
				}
				
				if(localMatch){
					//match is still true
				}else{
					isMatch = false;
				}
			}

			//EXCLUDE BY SEGMENT
			if(options.exclude.segment != false){
				filtersEngaged = true;
				
				nodes= thisResource.children('segments').children('segment');
				
				total = nodes.length;
				for(i=0;i<total;i++){
				  	txt = $(nodes[i]).text();				
					if(options.exclude.segmentIsMulti){
												
						for(jj = 0;jj<options.exclude.segmentMultifilter.length;jj++){
							if(txt == options.exclude.segmentMultifilter[jj]){
								doExclude = true;	
							}
						}
						
					}else if(txt == options.exclude.segment){
						doExclude = true;
						break; //break brands.each
					}
				}
				
				if(doExclude){
		  			continue;
					isMatch = false;
				}
			}



////////////////////////////////



			//INCLUDE BY SEGMENT
			if(options.data != false){
				localMatch = false;
				filtersEngaged = true;
				
				nodes = thisResource.children('dataset').children('data');
				total = nodes.length;
				for(i=0;i<total;i++){
				  	txt = $(nodes[i]).text();

					if(options.dataIsMulti){
												
						for(jj = 0;jj<options.dataMultifilter.length;jj++){
							if(txt == options.dataMultifilter[jj]){
								localMatch = true;	
							}
						}
						
					}else if(txt == options.data){
						localMatch = true;
						break; //break segment.each
					}
				}
				
				if(localMatch){
					//match is still true
				}else{
					isMatch = false;
				}
			}

			//EXCLUDE BY SEGMENT
			if(options.exclude.data != false){
				filtersEngaged = true;
				
				nodes= thisResource.children('dataset').children('data');
				
				total = nodes.length;
				for(i=0;i<total;i++){
				  	txt = $(nodes[i]).text();				
					if(options.exclude.dataIsMulti){
												
						for(jj = 0;jj<options.exclude.dataMultifilter.length;jj++){
							if(txt == options.exclude.dataMultifilter[jj]){
								doExclude = true;	
							}
						}
						
					}else if(txt == options.exclude.data){
						doExclude = true;
						break; //break brands.each
					}
				}
				
				if(doExclude){
		  			continue;
					isMatch = false;
				}
			}





/////////////////////////////////



			
			
			//Once all filters have been applied, either push match into results array, or just continue with loop
			
			if(isMatch){
				results = results.concat([thisResource[0]]);
			}
		
		} //END RESOURCES.EACH LOOP;







	
		//IF no filters were engaged, take the whole set of resources as the results.
		if(!filtersEngaged){
			results = resources.toArray();
		}
		
		
		// BEGIN SORTING THE RESULTS BASED ON options.sortDir and options.sortBy
		// Sorts look for either the first xml element tagged primary="primary",
		// then the first xml element, in the order they're coded in the xml file.
	
		switch(options.sortDir.toLowerCase()){		
			case 'desc':
				sortDirModifier = -1;
			break;
			case 'asc':
			default:
				sortDirModifier = 1;		
		}
		
		switch(options.sortBy){
			case 'date':
				//sort by date
				results.sort(function(a,b){
					var A = jQuery(a).children('date').text();
	
					var B = jQuery(b).children('date').text();
					
					if(A > B){
						return 1*sortDirModifier;
					}else if(A < B){
						return -1*sortDirModifier;
					}else{
						return 0;
					}
				})				
			break;
			case 'title':
				//sort by title
				results.sort(function(a,b){
					var A = jQuery(a).children('title').text();
	
					var B = jQuery(b).children('title').text();
					
					if(A > B){
						return 1*sortDirModifier;
					}else if(A < B){
						return -1*sortDirModifier;
					}else{
						return 0;
					}
				})				
			break;
			case 'type':
			case 'resourceType':
				//sort by type
				results.sort(function(a,b){
					var A = jQuery(a).children('resourceType').text();
	
					var B = jQuery(b).children('resourceType').text();
					
					if(A > B){
						return 1*sortDirModifier;
					}else if(A < B){
						return -1*sortDirModifier;
					}else{
						return 0;
					}
				})				
			break;
			case 'location':
				//sort by location
				results.sort(function(a,b){
					var A = jQuery(a).children('locations').children('location[primary="primary"]').eq(0);
					if(A.size() == 0){
						A = jQuery(a).children('locations').children('location').eq(0);
					}
	
					var B = jQuery(b).children('locations').children('location[primary="primary"]').eq(0);
					if(B.size() == 0){
						B = jQuery(b).children('locations').children('location').eq(0);
					}
					
					
					if(A.text() > B.text()){
						return 1*sortDirModifier;
					}else if(A.text() < B.text()){
						return -1*sortDirModifier;
					}else{
						return 0;
					}
				})
			break;
			case 'industry':
				//sorty by industry
				results.sort(function(a,b){
					var A = jQuery(a).children('industries').children('industry[primary="primary"]').eq(0);
					if(A.size() == 0){
						A = jQuery(a).children('industries').children('industry').eq(0);
					}
	
					var B = jQuery(b).children('industries').children('industry[primary="primary"]').eq(0);
					if(B.size() == 0){
						B = jQuery(b).children('industries').children('industry').eq(0);
					}
					
					
					if(A.text() > B.text()){
						return 1*sortDirModifier;
					}else if(A.text() < B.text()){
						return -1*sortDirModifier;
					}else{
						return 0;
					}
				})
			break;
			case 'category':
				//sort by category
				results.sort(function(a,b){
					var A = jQuery(a).children('categories').children('category[primary="primary"]').eq(0);
					if(A.size() == 0){
						A = jQuery(a).children('categories').children('category').eq(0);
					}
	
					var B = jQuery(b).children('categories').children('category[primary="primary"]').eq(0);
					if(B.size() == 0){
						B = jQuery(b).children('categories').children('category').eq(0);
					}
					
					
					if(A.text() > B.text()){
						return 1*sortDirModifier;
					}else if(A.text() < B.text()){
						return -1*sortDirModifier;
					}else{
						return 0;
					}
				})
			break;
			case 'brand':
				//sort by brand
				results.sort(function(a,b){
					var A = jQuery(a).children('brands').children('brand[primary="primary"]').eq(0);
					if(A.size() == 0){
						A = jQuery(a).children('brands').children('brand').eq(0);
					}
	
					var B = jQuery(b).children('brands').children('brand[primary="primary"]').eq(0);
					if(B.size() == 0){
						B = jQuery(b).children('brands').children('brand').eq(0);
					}
					
					
					if(A.text() > B.text()){
						return 1*sortDirModifier;
					}else if(A.text() < B.text()){
						return -1*sortDirModifier;
					}else{
						return 0;
					}
				})
			break;
		}
		
		// PAGINATION
		// Record some metadata about the pagination in a sub-object of the return object.
		
		var results_size = results.length;
		var pagination_options = {};

		pagination_options.totalresults = results_size;
		
		if(options.results_per_page != false){
			{
				if(options.page == false) options.page = 1;
				
				var offset = (options.page-1) * options.results_per_page;
				var offset_end = offset + options.results_per_page;
				if(offset_end > results_size) offset_end = results_size;
				
				var num_pages = Math.ceil(results_size/options.results_per_page); 
				var current_page = options.page;
				results = results.slice(offset,offset_end);
				
				pagination_options.first_result = offset+1;
				pagination_options.last_result = offset_end;
				pagination_options.num_pages = num_pages;
				pagination_options.current_page = current_page;
			}
			
		}
		
		var result = {}; //empty return object
		
		
		result.pagination = pagination_options; //pagination data
		result.resultset = results; //array of JQUERY-wrapped XML <resource> nodes
		return result;
	}



	
	// function convert_resource_to_object
	// arguments:
	//   resourceXMLNode- a jquery-wrapped XML node of a resource
	// return: javascript object with data from the inputted XML node
	// This function takes a single jquery-processed <resource> node and 
	// returns a javascript data object for easier use in javascript
	function convert_resource_to_object(resourceXMLNode){

		// default objects
		var retObj = {
			url:false,
			title:false,
			data:false,
			resourceType:false,
			type:false,
			featured:false,
			isNew:false,
			date:false,
			mediaType:false,
			industries:[],
			brands:[],
			categories:[],
			locations:[],
			segments:[],
			dataset:[]
		}
				

		retObj.url = resourceXMLNode.children('resourceUrl').text();
		
		retObj.date = resourceXMLNode.children('date').text();
		
		retObj.title = resourceXMLNode.children('title').text();
		
		retObj.data = resourceXMLNode.children('dataset').children('data').text();

		retObj.type = retObj.resourceType = resourceXMLNode.children('resourceType').text();
		
		retObj.mediaType =   resourceXMLNode.children('mediaType').text();
		
		if(resourceXMLNode.attr('new') == 'new')
			retObj.isNew = true;



		var nodes,total,i;

		nodes = resourceXMLNode.children('categories').children('category');
		total = nodes.length;
		for(i = 0;i<total;i++){
			retObj.categories.push($(nodes[i]).text());
		}

		nodes = resourceXMLNode.children('brands').children('brand');
		total = nodes.length;
		for(i = 0;i<total;i++){
			retObj.brands.push($(nodes[i]).text());
		}
		
		nodes = resourceXMLNode.children('locations').children('location');
		total = nodes.length;
		for(i = 0;i<total;i++){
			retObj.locations.push($(nodes[i]).text());
		}
		
		nodes = resourceXMLNode.children('industries').children('industry');
		total = nodes.length;
		for(i = 0;i<total;i++){
			retObj.industries.push($(nodes[i]).text());
		}

		nodes = resourceXMLNode.children('dataset').children('data');
		total = nodes.length;
		for(i = 0;i<total;i++){
			retObj.dataset.push($(nodes[i]).text());
		}

		return retObj;
	
	}



	/*======================================
	
		INITIALIZATION FUNCTIONS
		
		-get_all_filter_values
		-build_filters
		-build_pagination
		-build_table
	
	========================================*/	

	// function get_all_filter_values
	// arguments: none
	// return: object containing arrays of terms
	// Search the xml file for all possible values and return an object
	// containing arrays of those values.
	function get_all_filter_values(){
	
		var retObj = {
			location:[],
			brand:[],
			category:[],
			industry:[],
			type:[]
		};
	
		//temporary filter settings to get all resources in XML file
//		var filtersettings = {results_per_page:9999,page:1}
//		filtersettings = $.extend(true,{},settings,filtersettings);
//		var allResourcesElems = filter_xml(filtersettings).resultset;
		
		var allResources = unsortedResources.clone();


		//NEW WORLD
		var nodes,total,i,txt;

		nodes = allResources.children('categories').children('category');
		total = nodes.length;
		for(i = 0;i<total;i++){
			txt = trimWhitespace($(nodes[i]).text());
			if(txt != '' && !arrayContains(retObj.category,txt))
				retObj.category = retObj.category.concat([txt]);
		}

		nodes = allResources.children('brands').children('brand');
		total = nodes.length;
		for(i = 0;i<total;i++){
			txt = trimWhitespace($(nodes[i]).text());
			if(txt != '' && !arrayContains(retObj.brand,txt))
				retObj.brand = retObj.brand.concat([txt]);
		}


		nodes = allResources.children('locations').children('location');
		total = nodes.length;
		for(i = 0;i<total;i++){
			txt = trimWhitespace($(nodes[i]).text());
			if(txt != '' && !arrayContains(retObj.location,txt))
				retObj.location = retObj.location.concat([txt]);
		}

		nodes = allResources.children('industries').children('industry');
		total = nodes.length;
		for(i = 0;i<total;i++){
			txt = trimWhitespace($(nodes[i]).text());
			if(txt != '' && !arrayContains(retObj.industry,txt))
				retObj.industry = retObj.industry.concat([txt]);
		}


		nodes = allResources.children('resourceType');
		total = nodes.length;
		for(i = 0;i<total;i++){
			txt = trimWhitespace($(nodes[i]).text());
			if(txt != '' && !arrayContains(retObj.type,txt))
				retObj.type = retObj.type.concat([txt]);
		}


		retObj.location = retObj.location.sort();
		retObj.category = retObj.category.sort();
		retObj.brand = retObj.brand.sort();
		retObj.industry = retObj.industry.sort();
		retObj.type = retObj.type.sort();

		return retObj;
		
	}

	// function build_filters
	// arguments: none
	// return: none
	// Build the interactive filter drop downs using data
	// from the resources xml file. Build reset button.
	function build_filters(){
		var allFilterValues = get_all_filter_values();
		
		var filters = $('<div>');
		var filterNames = ['category','type','industry','brand','location'];
		var filterTitles = ['Topic','Type','Industry','Brand Partners','Location'];
		
		var myQuery;
		for(i = 0;i<filterNames.length;i++){
		

			var selectElem = $('<select>').data('filterfield',filterNames[i]);
			selectElem.append($('<option>').val('default').text(filterTitles[i]));
			
			var ho = queryHash();

			myQuery = decodeURIComponent(ho[filterNames[i]]);

			for(j = 0; j < allFilterValues[filterNames[i]].length;j++){

				var optionElem = $('<option>').text(allFilterValues[filterNames[i]][j]);
				
				if(typeof ho[filterNames[i]] != 'undefined'){
					if(myQuery == allFilterValues[filterNames[i]][j].replace('&','')){
						optionElem.attr("selected","selected");
					}
				}

				selectElem.append(optionElem);
			}
			
				
			filters.append(selectElem);
			selectElem.bind('change input',function(){
			   elem.trigger('stateChanged',{resetPage:true});
			}).fancySelect();

		}

		myQuery = null;
		
		var resetbutton = $('<div>',{
			html:'<div><img src="../../../../img.cdw.com/content/solutions/images/reset-filders.png"/*tpa=https://img.cdw.com/content/solutions/images/reset-filders.png*/ /></div>',
			'class':'gobutton'
		}).click(reset_filters);
		
		filters.append(resetbutton);

		filters.append($('<div>').css('clear','both'));
		
		return filters;
	}
	

	// function build_pagination
	// arguments: none
	// return: the pagination scaffold as a jquery object
	// Builds the pagination scaffolding dynamically. Actual
	// data is filled in dynamically by the refresh_pagination function		
	function build_pagination(){
		return $('<div class="resources-pagination pagination"><div class="pagination-links"></div><div class="pagination-info"></div>');	
	}
	

	
	// function build_table
	// arguments: none
	// return: the table as a jquery object
	// Builds the table and its header row, and makes the row columns
	// sortable by clicking on the headers	
	function build_table(){

		var actual_table  = $('\
<table cellspacing="0" cellpadding="0" border="0"><tr class="row-header row">\
<th class="th-ss first-child" data-sortfield="category"><a href="#">Topic</a></th>\
<th class="th-title" data-sortfield="title"><a href="#">Title</a></th>\
<th class="th-type" data-sortfield="type"><a href="#">Type</a></th>\
<th class="th-industry" data-sortfield="industry"><a href="#">Industry</a></th>\
<th class="th-brand" data-sortfield="brand"><a href="#">Brand</a></th>\
<th class="th-location" data-sortfield="location"><a href="#">Location</a></th>\
<th class="th-date"  data-sortfield="date"><a href="#">Date</a></th>\
</tr></table>');
		


			
		actual_table.find('th').click(function(e){		
			e.preventDefault();
			
			var myTH = $(this);
			var newSortField = myTH.data('sortfield');
			
			var sortDirStateElem = elem.find('.sortDir-state');
			var sortByStateElem  = elem.find('.sortBy-state');
			var newSortDir;
			
			//if sortfield is equal to current sortdirstate, flip sortbystate
			if(newSortField == sortByStateElem.val())
			{
				newSortDir = (sortDirStateElem.val() == 'asc')?'desc':'asc';
				sortDirStateElem.val(newSortDir);
			}
			//otherwise, set sortdirstate to this sortfield, and set sortbystate to default for this field			
			else
			{
				
				
				if(newSortField == 'date'){
					newSortDir = 'desc';
				}else{
					newSortDir = 'asc';
				}
				sortByStateElem.val(newSortField);
				sortDirStateElem.val(newSortDir);
			}
			
			elem.trigger('stateChanged',{resetPage:true});
		});
		
		return actual_table;
	}

	
	/*======================================
	
		THE FOLLOWING FUNCTION INITIALIZES THE WHOLE PLUGIN AND RETURNS 
		A REFERENCE TO THE ORIGINAL ELEMENT FOR CHAINING.
	
	========================================*/	
	return this.each(function(){
		jQuery.ajax({
				type:'get',
				dataType:'xml',
				asynchronous:false,
				url:settings.resources_xml_url,
				success:function(data){
					resourcesXML = $(data);
					unsortedResources = resourcesXML.children('resources').children('resource');
					resourcesFetched = true;
					on_xml_ready();						
				}
		});
		
		if(settings.view == 'table'){
			if(!isNumber(settings.results_per_page)){
				settings.results_per_page = 9999999;
			}
		}	
	})


}

 function trimWhitespace(s) {
        var str = s.match(/\S+(?:\s+\S+)*/);
        return str ? str[0] : '';
  }

var queryHash = function (hash) {
	var hash = window.location.hash;
	/* create a "dictionary" object for the passed in hash value */
	var obj = {}, pair = null, strHash = hash.substring(0, hash.length);
	if (strHash.indexOf("#") === 0) {
		strHash = strHash.substring(1, strHash.length);
	}
	var queryIndex = strHash.indexOf("?");
	if (queryIndex > -1) {
		strHash = strHash.substring(queryIndex + 1, strHash.length);
	}
	var parts = strHash.split("&");
	for (var i=0; i<parts.length; i++) {
		pair = parts[i].split("=");
		obj[pair[0].toString().toLowerCase()] = pair[1];
	}
	return obj;
}