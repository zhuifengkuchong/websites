// -----------------------------------------
// Support functions
Object.size = function(obj) {
	var size = 0, key;
	for (key in obj) {
		if (obj.hasOwnProperty(key)) size++;
	}
	return size;
};

cdw = {};
cdw.header = {
	switchImageName: function(imageUrl, fileName) {		
		var lastSlash = imageUrl.lastIndexOf("/");	
		return imageUrl.substring(0,lastSlash+1) + fileName;
	},
	
	hookButtonToFields: function(button, fields) {
		var buttonElement = $(button.name);
		var t = {};
		for (var f in fields) t[fields[f].name] = false;
		buttonElement.data('validFields', t);
		for (var i in fields) {
			var each = fields[i];
			$(each.name).data('md', {button: button, element: buttonElement, field: each});
			$(each.name).focus( function() { if ($(this).val() == $(this).data('md').field.instructions) $(this).val(""); } );
			$(each.name).blur( function() { if ($(this).val() == "") $(this).val($(this).data('md').field.instructions); } );
			$(each.name).keyup(
				function(e) {
					var md = $(this).data('md');
					var validFields = md.element.data('validFields');
					var imageUrl = md.element.attr('src');
					var fileName = md.button.offFileName;
					if ($(this).val().length > 0) {
						validFields[md.field.name] = true;
						var validCount = 0;
						for (var eachField in validFields) if (validFields[eachField] == true) validCount++;
						if (validCount == Object.size(validFields)) { 
							fileName = md.button.onFileName;
							md.element.attr('disabled', false);
						}
					}
					else {
						validFields[md.field.name] = false;
						md.element.attr('disabled', true);
					}
					md.element.attr('src', cdw.header.switchImageName(imageUrl, fileName));				
				} );
			$(each.Name).keypress(
				function(e) {
					if (e.which == 13 && $(this).data('md').element.attr('disabled') == false) {
						$(this).blur();
						$(this).data('md').element.focus().click();
					}
				});
			$(each.Name).bind(
				'paste',
				function() {
					var el = $(this);
					setTimeout(function() {
						var md = $(this).data('md');
						var validFields = md.element.data('validFields');
						var imageUrl = md.element.attr('src');
						var fileName = md.button.offFileName;
						if ($(el).val().length > 0) {
							validFields[md.field.name] = true;
							var validCount = 0;
							for (var eachField in validFields) if (validFields[eachField] == true) validCount++;
							if (validCount == Object.size(validFields))  {
								fileName = md.button.onFileName;
								md.element.attr("disabled", false);
							}
						}
						else {
							md.element.attr("disabled", true);
							validFields[md.field.name] = false;
						}
						md.element.attr("src", cdw.header.switchImageName(imageUrl, fileName));
					},
					100);
				});
		}
		try {
			buttonElement.attr("disabled", true);
			buttonElement.attr("src", cdw.header.switchImageName(buttonElement.attr("src"), button.offFileName));					
		}
		catch(ex) {
		}
	}	
};	

  // ---------------------------------------- //
 //        Global Header functionality       //
// ---------------------------------------- //
$(document).ready(function(){  

	// // Fix presistant bar width for IE 
	// function resizebar(){ 
	// 	var width1 = $(document).width(); 
	// 	var width_diff = width1 - 21 + "px"; 
	// 	$('#presistantBar').css('width',width_diff); 
	// 	}

	// // Call function on page load 
	// var vMode = document.documentMode;		
	// if ($.browser.msie && vMode == 5) { resizebar(); }
	
	// // Call function on window resize
	// $(window).resize(function() {if ($.browser.msie && vMode == 5) { resizebar(); } });
	
	// Handler for catalog drop down height
	function resizeCatalogDropDown(){ 
				
		var catlogCount = $(".catalogOptions ").children().length
        // Supress catalog for Canada not logged in.		
		var hostName = window.location.host;
		if (catlogCount < 2 && hostName != '' && (hostName.toLowerCase().indexOf('http://www.cdw.com/content/header/global/scripts/cdw.ca') >= 0)) {
		    $('.catalogWrapper').css('display', 'none');
		    $('.keywordInput').addClass('keywordInputCA');
		    $('.searchBar').addClass('searchBarCA');
		    $('.advancedSearch').addClass('advancedSearchCA');
		}


		if (catlogCount > 23){
			$('.catalogOptions').css('height',250);
			$('.catalogOptions').css('overflow','hidden'); 			
			$('.catalogOptions').css('overflow-y','scroll'); 			
			}

		}
		resizeCatalogDropDown();

	// Products Menu Functions //
	// Show Level 1 menu
	showProductsMenu = {
        sensitivity: 3, 
        interval: 100, 
        timeout: 200, 
        over:	function() { 
			// Handler for IE Top Level hover state
			if ($.browser.msie) { 
				$('.productsMenu li').removeClass('activeTopLink');
				$('.productsMenu li .topLink').removeClass('activeTopLink');
				$(this).addClass('activeTopLink');
				$(this).find('.topLink').addClass('activeTopLink');
			}
		$('.rightCol').hide(); $(this).find('.levelOne').show(); $('.columnTwo').hide(); $('.columnOne li').find('a').removeClass('active'); 
		}, 
        out:	function() { $('.rightCol').hide(); $(this).find('.levelOne').hide(); } 
    }
	if ($(".headerLayoutTbl").length > 0 && typeof $.fn.hoverIntent === 'function') {
    	$('.productsMenu li').hoverIntent(showProductsMenu);
	}
	
	$('.productsMenuWrapper').hover(
	function(){},
	function(){
		if ($.browser.msie) { 
				$('.productsMenu li').removeClass('activeTopLink');
				$('.productsMenu li .topLink').removeClass('activeTopLink');
			}
		}
	);

	// Clear TopLink active state 
	$('.levelOne').hover(
	function(){},
	function(){
		if ($.browser.msie) { 
				$('.productsMenu li').removeClass('activeTopLink');
				$('.productsMenu li .topLink').removeClass('activeTopLink');
			}
		}
	);

	// Detect iPad and Android, optimize menu
	var isiPad = navigator.userAgent.match(/iPad/i);
	var isAndroid = navigator.userAgent.match(/android/i);
	
	if (isiPad != null || isAndroid != null){
	// Show Level 1 menu on tap
	$('.productsMenu li').click(
		function(){            
			$('.productsMenuWrapper').hide();
			$('.brandsMenuWrapper').hide();
			$('.offersMenuWrapper').hide();
			$('.favoritesMenuWrapper').hide();			
			$('.rightCol').hide();
			$(this).find('.levelOne').show();
			$('.columnTwo').hide();
			$('.columnOne li').find('a').removeClass('active');
			},
		function(){            
			$('.rightCol').hide();
    	    $(this).find('.levelOne').hide();
		});  
		
		$("#hardwareTopNav").attr("href", "#");
		$("#softwareTopNav").attr("href", "#")
		$("#brandsTopNav").attr("href", "#")
		$("#offersTopNav").attr("href", "#")
		$("#findersTopNav").attr("href", "#")		
		$("#favoritesTopNav").attr("href", "#")				
		}

	// Hardware menu toggle
	hardwareMenuHover = {
        sensitivity: 3, 
        interval: 15, 
        timeout: 200, 
        over:	function() { 
		
		$('.rightColCA').css('width','467px'); $('.hardwareColumnOne li').find('a').removeClass('active'); $('.columnTwo').hide(); $('.rightCol').show(); $('.rightCol div.' + $(this).attr('id')).show(); $(this).find('a').addClass('active'); 
		if( $('.' + $(this).attr('id')).find('.promoRepeater').is(':empty') ) { $('.rightColCA').css('width','264px') }
		}, 
		out:	function(){ }
    }
	
	if ($(".headerLayoutTbl").length > 0 && typeof $.fn.hoverIntent === 'function') {
    	$('.hardwareColumnOne li').hoverIntent(hardwareMenuHover);
		//$('.hardwareMenuWrapper .rightCol').append('<a href="http://www.cdw.com/shop/search/hubs/Products/Printers-Scanners-Print-Supplies/P.aspx?cm_sp=GlobalHeader-_-Products|Hardware-_-Printers,_Scanners___Print_Supplies" class="inkTonerFinderLink">Ink and Toner Finder</a>');
	}

	// Hide Right column for view all links
	$('.ViewAllMenuLink').hover(function(){ $('.hardwareColumnOne li').find('a').removeClass('active'); $('.softwareColumnOne li').find('a').removeClass('active'); $('.findersColumnOne li').find('a').removeClass('active'); $('.rightCol').hide(); })

	// Software menu toggle
	softwareMenuHover = {
        sensitivity: 3, 
        interval: 15, 
        timeout: 200, 
        over:	function(){ $('.softwareColumnOne li').find('a').removeClass('active'); $('.columnTwo').hide(); $('.rightCol').show(); $('.rightCol div.' + $(this).attr('id')).show(); $(this).find('a').addClass('active'); }, 
		out:	function(){}
    }
	if ($(".headerLayoutTbl").length > 0 && typeof $.fn.hoverIntent === 'function') {
    	$('.softwareColumnOne li').hoverIntent(softwareMenuHover);
	}
	// Finders menu toggle
	findersMenuHover = {
        sensitivity: 3, 
        interval: 15, 
        timeout: 600, 
        over:	function() {$('.findersColumnOne li').find('a').removeClass('active'); $('.columnTwo').hide(); $('.rightCol').show(); $('.rightCol div.' + $(this).attr('id')).show(); $(this).find('a').addClass('active'); },
		out:	function(){}
    }
	if ($(".headerLayoutTbl").length > 0 && typeof $.fn.hoverIntent === 'function') {
    	$('.findersColumnOne li').hoverIntent(findersMenuHover);
	}

		$(".findersColumnOne #listItem6 a").click(function(){ window.location.href = $(this).attr("href");});	
	if (isiPad != null || isAndroid != null){
		
		$('#divInkAndTonerTypeAhead').hide();
			$('.standard_menu').hide();
			$('.ipad_only_menu').show();		
		}
	
	// Solutions and Services menu functions //		
	//  Solutions Services Menu Levl 1 toggle
	solutionsServicesMenuHover = {
        sensitivity: 3, 
        interval: 30, 
        timeout: 200, 
        over:	function(){ 	
				// Handler for IE Top Level hover state
				if ($.browser.msie) { 
					$('.solutionsServicesMenu li').removeClass('activeTopLink');
					$('.solutionsServicesMenu li .topLink').removeClass('activeTopLink');
					$(this).addClass('activeTopLink');
					$(this).find('.topLink').addClass('activeTopLink');
				}	
				$('.columnOne li').find('a').removeClass('active'); 
				$(this).find('.levelOne').css('display', 'block'); 
				
				}, 
		out:	function(){		$(this).find('.levelOne').css('display', 'none'); $('.rightCol').hide();}
    }
	if ($(".headerLayoutTbl").length > 0 && typeof $.fn.hoverIntent === 'function') {
    	$('.solutionsServicesMenu li').hoverIntent(solutionsServicesMenuHover);
	}
	// Clear TopLink active state 
	$('.levelOne').hover(
	function(){},
	function(){
		if ($.browser.msie) { $('.solutionsServicesMenu li').removeClass('activeTopLink');$('.solutionsServicesMenu li .topLink').removeClass('activeTopLink');	$('.rightCol').hide(); }
		}
	);

	
	// iPad handler for solutions menu
	if (isiPad != null || isAndroid != null){
	// Show Level 1 menu on tap
	$('.solutionsServicesMenu li').click(
		function(){            
			$('.rightCol').hide(); $('.columnOne li').find('a').removeClass('active'); $('.columnOne li').find('a').removeClass('active2'); $(this).find('.levelOne').css('display', 'block');
			},
		function(){            
			$(this).find('.levelOne').css('display', 'none');
		});  
		
		$(".solutionsMenuItem").attr("href", "#");
		}
		
	// Solutions menu toggle
	solutionsMenuHover = {
        sensitivity: 3, 
        interval: 15, 
        timeout: 200, 
        over:	function(){	 
							$('.solutionsColumnOne li').find('a').removeClass('active'); 
							$('.columnTwo').hide(); $('.rightCol').show(); 
							$('.rightCol div.' + $(this).attr('id')).show(); 
							$(this).find('a').addClass('active'); 
							},
		out:	function(){}
    }
	if ($(".headerLayoutTbl").length > 0 && typeof $.fn.hoverIntent === 'function') {
   		$('.solutionsColumnOne li').hoverIntent(solutionsMenuHover);
	}
	
	// Services menu toggle
	servicesMenuHover = {
        sensitivity: 3,
        interval: 15,
        timeout: 200,
        over:	function(){ 
							$('.servicesColumnOne li').find('a').removeClass('active'); 
							$('.columnTwo').hide(); $('.rightCol').show(); 
							$('.rightCol div.' + $(this).attr('id')).show(); 
							$(this).find('a').addClass('active'); 
							},
		out:	function(){}
    }
	if ($(".headerLayoutTbl").length > 0 && typeof $.fn.hoverIntent === 'function') {
    	$('.servicesColumnOne li').hoverIntent(servicesMenuHover);
	}
	
	// Industry menu toggle
	industryMenuHover = {
        sensitivity: 3,
        interval: 15,
        timeout: 200,
        over:	function(){ 
							$('.IndustrycolumnOne li').find('a').removeClass('active'); 
							$('.columnTwo').hide(); $('.rightCol').show(); 
							$('.rightCol div.' + $(this).attr('id')).show(); 
							$(this).find('a').addClass('active'); 
							},
		out:	function(){}
    }
	if ($(".headerLayoutTbl").length > 0 && typeof $.fn.hoverIntent === 'function') {
    	$('.IndustrycolumnOne li').hoverIntent(industryMenuHover);
	}

	// Regional menu toggle
	regionalMenuHover = {
        sensitivity: 3,
        interval: 15,
        timeout: 200,
        over:	function(){ $('.regionalColumnOne li').find('a').removeClass('active'); $('.columnTwo').hide(); $('.rightCol').show(); $('.rightCol div.' + $(this).attr('id')).show(); $(this).find('a').addClass('active'); },
		out:	function(){}
    }
	if ($(".headerLayoutTbl").length > 0 && typeof $.fn.hoverIntent === 'function') {
	    $('.regionalColumnOne li').hoverIntent(regionalMenuHover);
	}
	
	// Hide Right Column
	hideRightCol = {
        sensitivity: 3, 
        interval: 5, 
        timeout: 200, 
        over: function () { $('.solutionsColumnOne li').find('a').removeClass('active'); $('.servicesColumnOne li').find('a').removeClass('active'); $('.IndustrycolumnOne li').find('a').removeClass('active'); $('.rightCol').hide(); },
		out:	function(){}
    }
   if ($(".headerLayoutTbl").length > 0 && typeof $.fn.hoverIntent === 'function') {
		$('.hideRightCol').hoverIntent(hideRightCol);
   }
	// Account Center menu functions	
	accountCenterMenuHover = {
        sensitivity: 3, 
        interval: 100, 
        timeout: 200, 
        over:	function(){ 	
				// Handler for IE Top Level hover state
				if ($.browser.msie) { 
					$('.accountCenterMenu li').removeClass('activeTopLink');
					$('.accountCenterMenu li .topLink').removeClass('activeTopLink');
					$(this).addClass('activeTopLink');
					$(this).find('.topLink').addClass('activeTopLink');
				}	
			$(this).find('.levelOne').css('display', 'block');
				
				}, 
		out:	function(){		$(this).find('.levelOne').css('display', 'none');}
    }
	if ($(".headerLayoutTbl").length > 0 && typeof $.fn.hoverIntent === 'function') {
	    $('.accountCenterMenu li').hoverIntent(accountCenterMenuHover);
	}
	
	// Presistant Bar Functions
	$('.jsDisabledSpacer').hide()
	
	// Hide all Presistatnt bar controls 
	function closePBControls() {$('#logonContainer').hide(); $('div.socialMediaWrapper').hide();$('.socialMedia').removeClass('socialMediaUp');  $('#helpContainer').hide(); $(".quickLInksList").hide();$(".downIconquickLinksLink").text("▼"); $('#cartContainer').hide(); $(".orderStatus").hide(); $(".quickOrder").hide(); $(".priorSearches").hide(); }	
	
	// Hide controls on click away
	$('.fullContainer').click(function(){closePBControls();})

	
	/* // Logon Module Show
	$('#log-on-link').click( function() {
		closePBControls(); 
		$('#logonContainer').slideDown("fast"); 
		return false;
		});
		
	// Logon Module Close
	$('#closeLogonModule').click(	function() {		
		$('#logonContainer').slideUp("fast");	
		$('.logonPopupUserName').val('Enter Username');	
		$('.logonPopupPassword').val('');	
		return false;
		}); */
	
	// Clear Username Field 
	 $('.txtbox-logon').focus(function() {		if(this.value == "Enter Username"){ this.value = '';        }      });		

	// Help Module Toggle
	 $('.needHelp').click(function () { $(".advancedSearch").hide(); $(".catalogOptions").hide(); $('#logonContainer').hide(); $('.socialMediaWrapper').hide(); $(".quickLInksList").hide(); $('#cartContainer').hide(); $('#helpContainer').slideToggle("fast"); return false;	});
    $('.needHelp a').click(		function(){	$(".advancedSearch").hide(); $(".catalogOptions").hide(); $('#logonContainer').hide(); $('.socialMediaWrapper').hide(); $(".quickLInksList").hide(); $('#cartContainer').hide(); $('#helpContainer').slideToggle("fast"); return false });
		
	// Social Media Toggle
	$('.socialMedia').show()
    $('.socialMedia').click(	function(){	$(".advancedSearch").hide(); $(".catalogOptions").hide(); $('#logonContainer').hide(); $('#helpContainer').hide(); $(".quickLInksList").hide(); $('#cartContainer').hide(); $('.socialMediaWrapper').slideToggle("fast");	});

	// Quick Links Toggle
	$(".quickLInksWrapper").show()
	$(".quickLinksLink").click(function () { $(".advancedSearch").hide(); $(".catalogOptions").hide(); $('#logonContainer').hide(); $('#helpContainer').hide(); $(".socialMediaWrapper").hide(); $('#cartContainer').hide(); $(".quickLInksList").slideToggle("fast"); return false; });

	// Order Status Toggle
	$(".orderStatusLink").click(function () { $(".quickLInksList").hide(); $(".orderStatus").show("fast"); return false;	});

	// Quick Order Toggle
	$(".quickOrderLink").click(function () { $(".quickLInksList").hide(); $(".quickOrder").show("fast"); return false; });

	// Previous Searches Toggle
	$(".priorSearchLink").click(function () { $(".quickLInksList").hide(); $(".priorSearches").show("fast"); return false; });

	// Close Quick Link modals 
	$(".close").click(				function() {$(".orderStatus").hide("fast"); $(".quickOrder").hide("fast"); $(".priorSearches").hide("fast");	});

	//  Cart Module Toggle
    //$('.cartItems').click(	function(){ $(".advancedSearch").hide(); $(".catalogOptions").hide(); $('#logonContainer').hide(); $('#helpContainer').hide(); $(".socialMediaWrapper").hide(); $('.quickLInksList').hide(); $('#cartContainer').slideToggle("fast"); return false})	

	// Search Bar Functions 
 	// Catalog Toggle
	$(".selectedCatalog").click(	function() { closePBControls(); $(".advancedSearch").hide(); $(".catalogOptions").slideToggle("fast"); });
		
	// Catalog Dropdown
	$(".catalogOptions a").click(	function() {closePBControls(); $(".selectedCatalogLabel").text($(this).text()); $("input[name=ctlgfilter]").val($(this).attr('alt')); $(".catalogOptions").hide();	});
	
	// Advanced Search Toggle
	$(".advancedSearchLink").click(	function() {closePBControls(); $(".catalogOptions").hide(); $(".advancedSearch").slideToggle("fast"); }	);

	// Close Advanced Serch
	$(".closeAdvancedSearch").click(function() {$(".catalogOptions").hide(); $(".advancedSearch").slideToggle("fast");	$(".moreSearchDownIcon").text("▼");});
	$("#searchbox2").click(function() {$(".catalogOptions").hide(); $(".advancedSearch").hide();	});

		
	// Global Modal Function 

	// Open Modal
	//$(".globalContentModal").click(function () {
//		var modalWindow = ('.') + $(this).attr("id");
//		$(".fullContainer").append(	'<div class="lightbox">' + $(modalWindow).html() + '<a href="#" class="closeLightbox"></a>' + '</div>' + '<div class="overlayX"></div>'	);
//		$(".overlayX, .lightbox").show(	);
//		$(".presistantBar").css('opacity','.5');
//
//	});
	
	// Open Modal
      $(".globalContentModal").click(function (e) {
           var modalWindow = ('.') + $(this).attr("id");
           var innerModal = $(modalWindow).html();
           //$(modalWindow).empty();
           $(".fullContainer").append( '<div class="lightbox">' + innerModal + '<a href="#" class="closeLightbox"></a>' + '</div>' + '<div class="overlayX"></div>'   );
           $(".overlayX, .lightbox").show(  );
           $(".presistantBar").css('opacity','.5');
           e.preventDefault();
      });
	
	// Close Modal
	/*$(".overlayX, .closeLightbox").live('click', function () {
		$(".overlayX, .lightbox").remove();
		$(".presistantBar").css('opacity','1');
		if ( $('.confirmationPopUp').is(':hidden'))  {
			//alert('hidden');
			$("#backgroundPopup").hide();
		}
	});	*/
	$('body').delegate(".overlayX, .closeLightbox", "click", function(e) {
		$(".overlayX, .lightbox").remove();
		$(".presistantBar").css('opacity','1');
		if ( $('.confirmationPopUp').is(':hidden'))  {
			//alert('hidden');
			$("#backgroundPopup").hide();
		}
		e.preventDefault();
	});	
	
	// Add Modal Resizing - Liveball related
	

	
	// Canada French Translation: This function is for a page-level content sensitive implementation 
	function OneLink(sHostname) { document.location.href = document.location.protocol + "//" + sHostname + document.location.pathname; }
	

	// Add class to Qualtrics Site Intercept feedback button div

/*	var dfd = $.Deferred();
	var checkSelector = setInterval(function () {
    if ($("img[alt='Feedback Link']").length) {
        dfd.resolve();
        clearInterval(checkSelector);
    }
	}, 1000);

	dfd.done(function () {
		$("img[alt='Feedback Link']").parent().css({'position':'fixed'});
	    //console.log('it has been added');
	});
	*/

   }); // Close main function