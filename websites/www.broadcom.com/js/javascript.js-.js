/* IE doesn't like console. */
if (typeof console == "undefined") {
	window.console = {
		log: function () {}
	};
}

$(document).ready(function() {

    /* Toggles up/down chevron for collapse/expand elements - uses bootstap collapse event - JASS */
	/* JD - Must have a target for this to work.*/
	// Any UI element that is modified in js, throw it in relive.
	$(this).relive();
	
	/* main menu - application dropdown carousel */
	$('#carousel_application').carousel({
		interval: 5000,
		wrap: true,
		pause: "hover",
	});
	/* main menu - expand / collapse dropdown - circumventing bootstrap collapse for custom ui */
	$('#nav_drop').collapse({toggle: false});
	
	$('div.nav_list').on('click', function(event){
		$('#nav_drop').collapse('show');
		$('div.nav_list ul li').removeClass('active');  // menu now split in two, bootstrap only clears active from the current menu
		$(this).parent().closest('li').addClass('active'); // put the class back
	});

	// We are the tab buttons, so if we are active and we are pressed, go ahead and collapse the menu.
	$('.nav_tab').click(function(event) {
		if ($(this).parent().closest('li').hasClass('active')) {
			$('#nav_drop').collapse('hide');
		}
	});
	
	
	
	// Offcanvas sidebar.
	$('.sidebar-button').click(function(event) {
		$('.sidebar-row-offcanvas').toggleClass('active');
	});
	
	$('#carousel_main').carousel({
		interval: 5000,
		wrap: true,
		pause: "hover",
	});
	
	var currentSlide = "carousel_0 active";
	
	$('#carousel_main').on('slid.bs.carousel', function () {
  var carouselData = $(this).data('bs.carousel');
  // EDIT: Doesn't work in Boostrap >= 3.2
  var currentIndex = carouselData.getActiveIndex();
  //var currentIndex = carouselData.getItemIndex(carouselData.$element.find('.item.active'));		
  setCarouselTabs(currentIndex);
		//get classname of active class
		$('.progress-bar').stop(true,true);
		$('.progress-bar').animate({
			width: '100%'
		    }, {
			duration: 4500,
			easing: 'linear',
			complete: function() {
			$('.progress-bar').innerWidth('0%');
			//setCarouselTabs(currentSlide);			
			}
		    }); 	

	});	
	$('#carousel_main').on('slide.bs.carousel', function () {
		//console.log("SLIDE");
		//get classname of active class
		//currentSlide = $('#carousel_main').find( "li.active" )[0].className;
		//setCarouselTabs(currentSlide);	
		
	});
	$('#carousel_main').click(function(){
		currentSlide = $(this)[0].className;
	});
	
	
	$('#carousel_main').ready(function () {
		$('.progress-bar').animate({
			width: '100%'
		    }, {
			duration: 5200,
			easing: 'linear',
			complete: function() {
			$('.progress-bar').innerWidth('0%');
			//setCarouselTabs(currentSlide);			
			}
		    }); 
		$('.carousel_0').css("background-color", "red");
		$('.carousel_1').css("background-color", "#c0c0c0");
		$('.carousel_2').css("background-color", "#d1d1d1");
	});		
	
	
	initBrowseHistory();
	
	initProducts();
	
	initConnectingEverything();
	
	initAutocompleteSearch();
	
	initScrollToTop();
	
	initGlobalSmoothScroll();
	
	// /contacts Distributors
	if($('#page-locations').length != 0) {
		// Create an array of styles.
		var styles = [
			{
				stylers: [
					{ hue: "#ffffff" },
					{ saturation: -20 }
				]
			},{
				featureType: "road",
				elementType: "geometry",
				stylers: [
					{ lightness: 100 },
					{ visibility: "simplified" }
				]
			},{
				featureType: "road",
				elementType: "labels",
				stylers: [
					{ visibility: "off" }
				]
			}
		];
		
		var location = {latitude: 33.638169, longitude: -117.851543};		// Irvine.
		var map = null;
		
		if(navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function(position) {
				initialLocation = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
				if (map) {
					map.setCenter(initialLocation);
					
				}
			});
		}

  
		google.maps.event.addDomListener(window, 'load', function() {
			map = new google.maps.Map(document.getElementById('distributor-map'), {
				center: new google.maps.LatLng(location.latitude, location.longitude),
				zoom: 10,
				scrollwheel: false,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			});
			
			// set map styles
/*			var styledMap = new google.maps.StyledMapType(styles, {name: "Styled Map"});
			map.mapTypes.set('map_style', styledMap);
		  map.setMapTypeId('map_style');
*/
			var panelDiv = document.getElementById('distributor-panel');

			var data = new LocationDataSource('http://www.broadcom.com/contact/locations_data.php');

			var view = new storeLocator.View(map, data, {
				geolocation: false,    
				features: data.getFeatures()				
			});

			new storeLocator.Panel(panelDiv, {
				view: view,
				directions: true
				});
		});		
	}

	// Blog
	if ($('.blog').length != 0) {
			
		if ($('.blog_navbar_content').length != 0) {
		
			//$('.gallery').bxSlider();
			 
			// Grab and drag this sucker.
			$('.blog_navbar_content').kinetic();
			
			
			// Show us the active nav.
			if ($('.blog_navbar .blog_navbar_nav > .active').length != 0) {
				$('.blog_navbar_content').scrollLeft($('.blog_navbar .blog_navbar_nav > .active').position().left);				
			}
			
			
			// Scroll the navigation.
			$('.blog_nav_right').click(function() {
				var scroll = $(".blog_navbar_content").scrollLeft();
				$(".blog_navbar_content").scrollLeft(scroll+200);
				$(".blog_navbar_content").perfectScrollbar('update');
			});
			$('.blog_nav_left').click(function() {
				var scroll = $(".blog_navbar_content").scrollLeft();
				$(".blog_navbar_content").scrollLeft(scroll-200);
				$(".blog_navbar_content").perfectScrollbar('update');
			});
			
			var interval;
			$(".blog_nav_right").on('mouseover', function() {
				interval = setInterval(function(){
					var scroll = $(".blog_navbar_content").scrollLeft();
					$(".blog_navbar_content").scrollLeft(scroll+20);
					$(".blog_navbar_content").perfectScrollbar('update');
				}, 100);
			}).on('mouseout', function() {			
				clearInterval(interval);
			});
			
			$(".blog_nav_left").on('mouseover', function() {
				interval = setInterval(function(){
					var scroll = $(".blog_navbar_content").scrollLeft();
					$(".blog_navbar_content").scrollLeft(scroll-20);
					$(".blog_navbar_content").perfectScrollbar('update');
				}, 100);
			}).on('mouseout', function() {			
				clearInterval(interval);
			});
		}
	}
	
	// Applicatin/IoT
	if ($('#application_iot').length != 0 || $('#application_car').length != 0|| $('#application_wirelesscharging').length != 0 ) {
		$('a[data-toggle="tab"]').on('http://www.broadcom.com/js/shown.bs.tab', function (e) {
			$('a[data-toggle="tab"]').each(function() {
				$(this).closest('div.col-sm-3').removeClass('active'); // newly activated tab
			});
			
			$(this).closest('div.col-sm-3').addClass('active'); // newly activated tab
		  
		});
	}
	// Photo Library
	$('.page-photos').find('[data-target="#image_modal"]').each(function () {			
		$(this).click(function() {
			var url = $(this).attr('data-img-url');
			$('.modal img').attr('src', url);
		});
	});
	
	$('#photo_library_images a[data-toggle="tab"]').on('http://www.broadcom.com/js/shown.bs.tab', function (e) {
		
		$('#photo_library_images').find('a[data-toggle="tab"]').each(function() {
			$(this).removeClass('active');
		});
		$(e.target).addClass('active');			
	});
	
	adjustRelatedProducts();
	$( window ).resize(function() {
	adjustRelatedProducts();
	});
});

//functions to call when ajax pages are loaded
jQuery.fn.relive = function() {
    var o = $(this[0]);
	initChevron(o);
	
	//$("a", o).tooltip({placement: 'bottom'});

	//$("a", o).tooltip('show');

	// The blog
	$(this).find('#blog_home_second').imagefill({fixImage:true}); 
	
	$(this).find('.blog_post_sm, .blog_post_md, .blog_post_lg').each(function () {
			$(this).imagefill({fixImage:true});
	});
	
	$(this).find('[data-image=crop]').each(function () {
		$(this).imagefill({fixImage:true});
	});
	
	
	
	// Perfect scrollbar
	$(this).find('[data-style=scrollbar]').each(function() {
		// TODO: Parse out attributes. Hacking for now.
		var args = $(this).attr('data-scrollbar');
		
		if (args) {
			
			// JD: Some reason I couldn't just pass in the parameter and had to do switch statements.
			switch (args) {
				case 'suppressScrollX':
					$(this).perfectScrollbar({'suppressScrollX':true});
				break;
				case 'suppressScrollY':
					$(this).perfectScrollbar({'suppressScrollY':true});
				break;
			}
			
		}
		else {
			$(this).perfectScrollbar();
		}
	});
};

/* Toggles up/down chevron for collapse/expand elements - uses bootstap collapse event - JASS */
/* JD - Must have a target for this to work.*/
function initChevron(object){
if(!object)
{
	object = $(document);
}
	$(object).find('[data-toggle="collapse"]').click(function(event) {
		var target = $(this).attr('data-target');
		
		if (!$(target).hasClass('in')) {
			$(this).addClass('in');
		}
		else {
			$(this).removeClass('in');
		}
	
	});
	
}

/**
 *  @brief Initialize browse history.
 *  
 *  @return Return_Description
 *  
 *  @details Function takes the document title and current URL and stores them in local storage as an array
 *            Array will be parsed by getBrowseHistory in the form of {"0":["TITLE","URL"]}
 */
function initBrowseHistory()
{
	var history =  '';
	if(supports_local_storage() != false)
	{
		history = localStorage.getItem( 'browse_history' );//getCookie('browse_history');//
	}
	var currentURL = window.location.href;
	var current = new Array(document.title, window.location.href);
	var historyList = {};
	//check if there is already history in there, otherwise create it in local storage
	if( typeof(history)!=="undefined" && history != null && history != "")
	{
		//if there is browsing history, parse it		
		historyList = JSON.parse( history );
		
		//convert to array for easier queuing
		var history_array = $.map(historyList, 
			function(value, index) {
				return [value];
			});
			
		//only store 6, otherwise remove the oldest(first) entry
		if(history_array.length > 6)
		{
			history_array.shift();
		}
		
		var historyString =  JSON.stringify(historyList);

		//no repeats
		if(historyString.indexOf(currentURL) == -1)
		{
			//add to history queue
			history_array.push(current);		
		}
		
		//convert to obj and store
		historyList = toObject(history_array);
		if(supports_local_storage() != false)
		{
			localStorage.setItem( 'browse_history',  JSON.stringify(historyList));//setCookie('browse_history', JSON.stringify(historyList))//			
		}
	}
	else
	{
		
		var currentPath = window.location.pathname;
		var newHistory = Array();
		
		if($('.browsing_history_content').length > 0){
/*
		}
		if(currentPath.indexOf("products") != -1)
		{
		*/
			newHistory.push(current);
			historyList = toObject(newHistory);
			if(supports_local_storage() != false)
			{
				localStorage.setItem( 'browse_history',  JSON.stringify(historyList));//setCookie('browse_history', JSON.stringify(historyList))//
			}
		}		
	}
	getBrowseHistory();
}

/**
 *  @brief Output a list of our browse history.
 *  
 *  @return Return_Description
 *  
 *  @details Details
 */
function getBrowseHistory()
{
	//get local storage history and convert it to array to parse
	var history = '';
	if(supports_local_storage() != false)
	{
		history = localStorage.getItem( 'browse_history' );// getCookie('browse_history');//
	}
	var currentURL = window.location.href;
	var historyList = {};	
	if( typeof(history)!=="undefined" && history != null && history != "")
	{
		historyList = JSON.parse( history );
		var history_array = $.map(historyList, 
			function(value, index) {
				return [value];
			});
		var index;
		var browseDiv = document.getElementsByClassName('browsing_history_content');
		if (typeof (browseDiv) != 'undefined' && browseDiv != null && browseDiv.length > 0) {
			
			var $ol = $('<ol class="breadcrumb"></ol>');
			$(browseDiv).append($ol);
			$initialIndex = 0;
			if(history_array.length > 4)
			{
				$initialIndex = history_array.length - 4;
			}
			for( index = $initialIndex; index < history_array.length; index++)
			{
				var url = "";
				var name = "";
				// Since the first array was 1 dimension, our new way has both the url and title of the page.
				if( Object.prototype.toString.call( history_array[index] ) === '[object Array]' ) {
					name = history_array[index][0];
					url = history_array[index][1];		

					// Trim out " | Broadcom"
					name = name.replace(" | Broadcom",""); 
					
					if(name.length > 50)
					{
						name = name.substring(0,40) + "...";
					}
				}
				else {	// Backwards compatibility.
					url = history_array[index];
					var linkText = history_array[index].split('/');
					var innerText = "";
					for(var i = linkText.length - 1; i > 0; i--)
					{
						if(linkText[i] != "")
						{
							name = linkText[i];
							break;
						}
					}
				}
				
				var browseHistoryLink = document.createElement('a');
				browseHistoryLink.id = 'browseHistoryLinkId' + index;
				browseHistoryLink.className = 'browseHistoryLinkClass' + index;
				browseHistoryLink.href = url;	
			
				
				browseHistoryLink.innerHTML = name;// + "</br>";
				//browseDiv[0].appendChild(browseHistoryLink);
				$ol.append($('<li></li>').append(browseHistoryLink));
				
			}
			
		}
	}
}



/* ProductSearchBox Widget js -------------------------------------
	** TODO: still deciding if we should leave it here or encapsulate it in the widget itself **
*/

function initAutocompleteSearch() {
	$('input[data-autocomplete="search"]').each(function() {
		var elem = $(this);
		var typingTimer;
		var doneTypingInterval = 700;
		
		// Save current value of element
		elem.data('oldVal', elem.val());
		
		// Add our drop down.
		var dropdown=$(elem.attr('data-target'));
		
		
		// Hide us.
		$(document).mouseup(function (e) {
			var container = dropdown;

			if (!container.is(e.target) // if the target of the click isn't the container...
				&& container.has(e.target).length === 0) // ... nor a descendant of the container
			{
				container.hide();
			}
		});

		//Detect keystroke and only execute after the user has finish typing
		function delayExecuteAutocomplete()
		{
			clearTimeout(typingTimer);
				typingTimer = setTimeout(
				function(){runAutocomplete()},
				doneTypingInterval
			);

			return true;
		}

		function runAutocomplete( theInputName)
		{
			// Style us
			dropdown.width(elem.width());

			

			// If value has changed and is longer than 2 chars
			if (elem.data('oldVal') != elem.val()){
			  // Updated stored value
				elem.data('oldVal', elem.val());
				if(elem.val().length <= 2)
				{
					dropdown.hide();		
					return;
				}			
				
				// Do some progress animation.
				elem.next('.form-control-feedback').show();
				
				//clear any previous results
				
				// grab data by ajax 
				$.get('/search/autocomplete', {widget:'productsearchbox', q:elem.val()}, function(result) {
					autoCompleteArray = jQuery.parseJSON(result);
					elem.next('.form-control-feedback').hide();
					
					if ((autoCompleteArray.applications.length <= 0) && (autoCompleteArray.products.length <= 0)) {
						return;				// Nothing found.
					}
				
					dropdown.show();
					
					
					$('.autocomplete-menu-results').remove();
					var resultDiv = $('<div class="autocomplete-menu-results"> </div>');
					autoCompleteArray.applications.forEach(function(item) {
						
						var contentLink = $('<a href="'+item.url+'"><img src="'+item.image+'" width="100" style="margin-right: 20px;"><span class="h4">' + item.title + '</span></a>');
						resultDiv.append($('<div class="autocomplete-menu-trending"></div>').append(contentLink));	
						
					});
					
					var border = (autoCompleteArray.applications.length > 0) && (autoCompleteArray.products.length > 0);
					if (border) {
						resultDiv.append('<hr>');
					}
					
					autoCompleteArray.products.forEach(function(item) {
						
						var contentLink = $('<a href="'+item.url+'">' + item.name + '</a>');
						resultDiv.append($('<div class="autocomplete-menu-link"></div>').append(contentLink));	
					});
					
					//add to the div
					dropdown.append(resultDiv);	
					
				});
				
			}
		}

		
		// Look for changes in the value
		elem.bind("paste", function(event){
			runAutocomplete();
		});
		elem.bind("propertychange keyup input paste", function(event){
			delayExecuteAutocomplete();
		});
	});
}




/* CSR content -----------------------------------------
 *
 */

function csrContentSelect(id) {
	//console.log(window.location);
	$.get(window.location.pathname, {csr_content:id}, function(result) {
		$('#csr_contents').hide().html(result).fadeIn();
		$('#csr_contents').relive();
	});

	return false;
}

// Stop closing if we are already open.
if ($('.accordion_panel_group').length != 0) {
	$('.panel-heading a').on('click',function(e){
		if ($(this).parents('.accordion_panel').children('.panel-collapse').hasClass('in')){
			e.stopPropagation();
		}
		 e.preventDefault();
	});
}

/* Downloads selector js --------------------------------------
 *
 */

$('.download-group-select').click(function(event) {
	/* TODO remove progress, causing Bluetooth and other download group info not to load - rh
	$('#download_results').fadeOut("fast", function() {
		$(this).show().html('<span class="fa fa-refresh fa-spin ajax-progress"></span>');
	});
	*/

	var group_id = $(this).data("id");
	var group_name = $(this).html();
	var group_url =  $(this).attr('href');
	
	$('#download_results_loading').show();
	
	// ajax download results 
	$.get(window.location.pathname, {widget:'downloadsselector_results', gid:group_id}, function(result) {
		$('#download_results_loading').hide();
		$('#download_results').hide().html(result).fadeIn();
		$('#download_results').relive();
		
		title = group_name + ' | Broadcom';
		document.title = title;
		if(window.history.pushState)
		{
			window.history.pushState({}, title, group_url);
		}
		
		$('#section_download_results').ScrollTo();
	});
	
	// ajax download blogs
	$.get(window.location.pathname, {widget:'downloadsselector_blogs', tag:group_name}, function(result) {	
		$('#download_blogs').hide().html(result).fadeIn();
		$('#download_blogs').relive();
	});
	
	event.preventDefault();
});


/**
 *  @brief /products
 *  
 *  @return Return_Description
 *  
 *  @details Details
 */
function initProducts()
{
	// /products/markets shows/hide ellipses for read more.
	$('.market-description').on('show.bs.collapse', function () {
		$('#market-description-ellipse').hide();
	});
	
	$('.market-description').on('hidden.bs.collapse', function () {
		$('#market-description-ellipse').show();
	});
	
	// Product filter system used in /products/category
	function filterAjax() {
		// Post form.
		var url = $('#filter-products').attr('data-url');
		$('#filter-products').fadeOut("fast", function() {
			$(this).show().html('<span class="fa fa-refresh fa-spin ajax-progress"></span>');
		});
		
		
		$.ajax({url: url, type: 'get', dataType: "json", data: $('#filter-ajax').serialize()}).done(function (data) {
			$('#filter-products').hide().html(data.content).fadeIn();
			$('#product_filter_container').html(data.sidebar);
			filterLive();
			$('#filter-products').relive();
			$('#product_filter_container').relive();
		});
	}
	
	// Product filter system.
	
	function filterLive() {
		$('#filter-pagination li select').change(function(event) {
			var page = $('option:selected', this).attr('data-page');
			event.preventDefault();
			$('#filter-ajax [name=start]').val(page);
			filterAjax();
		});
		
		$('#filter-pagination li a').click(function(event) {
			var page = $(this).attr('data-page');
			event.preventDefault();
			$('#filter-ajax [name=start]').val(page);
			filterAjax();
		});
		
		$('.filter-ajax-cb').click(function() {
			$('#filter-ajax [name=start]').val(0);			// Reset our start posision.
			
			// Add active 			
			$(this).parents('li').toggleClass('active', $(this).prop('checked'));
			
			// Post form.
			filterAjax();
		});
		
		$('.filter-reset').click(function() {
			$('#filter-ajax [name=start]').val(0);			// Reset our start posision.
			$('.filter-ajax-cb').removeAttr('checked');
			filterAjax();
		});
		
		// For form submission
		$('.brands-collapse,.categories-collapse,.technologies-collapse,.applications-collapse').on('show.bs.collapse', function () {
			var classList = $(this).attr('class').split(/\s+/);
			$.each( classList, function(index, item){
				if (item.indexOf('-collapse') > 0) {
					var className = item.replace('-collapse', '');
					$('input[name="collapse['+className+']"]').val('1');
					return false;
				}				
			});
			
		});
		$('.brands-collapse,.categories-collapse,.technologies-collapse,.applications-collapse').on('hide.bs.collapse', function () {
			var classList = $(this).attr('class').split(/\s+/);
			$.each( classList, function(index, item){
				if (item.indexOf('-collapse') > 0) {
					var className = item.replace('-collapse', '');
					$('input[name="collapse['+className+']"]').val('0');
					return false;
				}				
			});
		});
	}
	
	filterLive();
	
}

function initConnectingEverything()
{
	if ($('#body-connecting-everything').length == 0) {
		return;
	}
	
	// Affix sub nav and offset based on view port
	var offset = $('#banner').height();
	$('.sub-nav').affix({offset: offset});
	$(window).resize(function() {
		// Widths based on bootstrap
		var offset = $('#banner').height();
		$('.sub-nav').affix({offset: offset});
	});

	$('.note-type-facebook .note-image').each(function () {
			$(this).imagefill({fixImage:true});
	});
	
	$('.note-2.note-type-blog .note-image').each(function () {
			$(this).imagefill();
	});
	
	$('.note-2.note-type-youtube .note-image').each(function () {
			$(this).imagefill();
	});
	
	$('.note-blog-top .note-image').each(function () {
			$(this).imagefill();
	});
	
	$('.note-youtube-top .note-image').each(function () {
			$(this).imagefill();
	});
	
	$('.note-type-pinterest .note-image').each(function () {
			$(this).imagefill();
	});
	
	$('.note-type-see-more .note-image').each(function () {
			$(this).imagefill();
	});
	
	
	// Image loading wheel.	
	//$('.note-image img').hide();
	/*var load_handler = function() {
		$(this).fadeIn().css("display","inline-block");
	}
	$(this).find('.note-image img').filter(function() {
		return this.complete;
	}).each(load_handler).end().load(load_handler);*/

	
	
	
	// Pre CES
	$("#pre_ce_video").fitVids();
	
	// Dim down everything but blog.
	["blog", "facebook", "twitter", "pinterest", "youtube"].forEach(function (element, index, array) {
		$('#filter_'+element).click(function() {
			$(".filters").not(this).removeClass("active");
			$("[class*='note-type-']").removeClass("active");
			$(this).toggleClass('active');
			
			if ($(this).hasClass('active')) {
				var note_type = ".note-type-"+element;
				$("[class*='note-type-']:not("+note_type+")").fadeTo('slow', 'http://www.broadcom.com/js/0.33').addClass('filter');
				$(note_type).fadeTo('slow', '1.0').addClass('active');	// Turn the lights on.
			}
			else {
				$("[class*='note-type-']:not("+note_type+")").fadeTo('slow', '1.0').removeClass('filter');
			}
		});
	});
	
	
	
	
	
	// Turn the lights on.
	$('#filter_all').click(function() {
		$("[class*='note-type-']").fadeTo('slow', '1.0');
	});	
	
	
	// Scroll spy for date.
	$('body').scrollspy({ target: '#view-date-spy', offset: 120 });

	// JD - Turning off for now til ces.
	$('#view-date-spy').on('activate.bs.scrollspy', function (e) {		
		$('#view-date').fadeOut('fast', function() {
			$(this).text(e.target.textContent);
			$(this).fadeIn('fast');
		});
	});
	
	// Set our text of our current day.
	if (window.location.hash.substr(1)) {
		$('#view-date').text($('#'+window.location.hash.substr(1)).attr('title'));
	}
	
	
	// Overlay hover.
	/*$('.note').hover(function() {
			$(this).find('.note-overlay').show();
			// Hide our other text.
			if ($(this).find('.note-overlay').length) {
				$(this).find('.note-text').hide();
			}
		},
		function () {
			$(this).find('.note-overlay').hide();
			// Show our other text.
			if ($(this).find('.note-overlay').length) {
				$(this).find('.note-text').show();
			}
		}
	);*/
	
	// enable slideshow
	$('#page-connecting-everything .carousel').each(function() {
		// Randomize the time between 4500 and 5500.
		ran_interval = Math.floor(Math.random() * 1500) + 4500
		$(this).carousel({
			interval: ran_interval
		});
	});
	
}

/* 
 * Destributors
 */

if (typeof(google) != "undefined") {
/**
 * @extends storeLocator.StaticDataFeed
 * @constructor
 */
function LocationDataSource(file) {
  $.extend(this, new storeLocator.StaticDataFeed);

  var that = this;
  $.get(file, function(data) {
	var stores = [];
	var static_stores = [];
	for(var type in data){
		for (var j=0; j<data[type].length; j++) {
			
			var features = new storeLocator.FeatureSet;
			var position = new google.maps.LatLng(data[type][j].Latitude,data[type][j].Longitude);
			var guid = that.guid();

			features.add(that.FEATURES_.getById(type));
			
			var store = new storeLocator.Store(guid, position, data[type][j].Region, data[type][j].State, features, {
			  title: data[type][j].Name,
			  contact: data[type][j].Contact,
			  address: data[type][j].Address,
			  email: data[type][j].Email,
			  fax: data[type][j].Fax,
			  phone: data[type][j].Telephone,
			  website: data[type][j].Website,
			});
			
			if (data[type][j].Static == 'true') {
				//static_stores.push(store);		
stores.push(store);				
			}
			else {
				stores.push(store);
			}
			
		}
	}
    that.setStores(stores);
	that.setStaticStores(static_stores);
  });
}

LocationDataSource.prototype.guid = (function() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
               .toString(16)
               .substring(1);
  }
  return function() {
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
           s4() + '-' + s4() + s4() + s4();
  };
})();

/**
 * @private
 * @param {string} csv
 * @return {!Array.<!storeLocator.Store>}
 */
LocationDataSource.prototype.parse_ = function(csv) {
  var stores = [];
  
  
  
  var rows = $.csv.toArrays(csv);//csv.split('\n');

  var headings = rows[0];

  for (var i = 1, row; row = rows[i]; i++) {
  
    row = this.toObject_(headings, rows[i]);
	
	var features = new storeLocator.FeatureSet;
	var position = new google.maps.LatLng(row.Lat,row.Long);
    var locality = this.join_([row.Locality, row.Postcode], ', ');

	var guid = this.guid();

	// TEST
	if (i == 1) {
		features.add(this.FEATURES_.getById('Offices'));
	}
	else {
		features.add(this.FEATURES_.getById('Sales'));
	}
	
	
	var store = new storeLocator.Store(guid, position, features, {
      title: row.Distributor,
      address: this.join_([row.Address, locality], '<br>'),
	  telephone: row.Telephone,
    });
    stores.push(store);
  }
  return stores;
};

/**
 * Joins elements of an array that are non-empty and non-null.
 * @private
 * @param {!Array} arr array of elements to join.
 * @param {string} sep the separator.
 * @return {string}
 */
LocationDataSource.prototype.join_ = function(arr, sep) {
  var parts = [];
  for (var i = 0, ii = arr.length; i < ii; i++) {
    arr[i] && parts.push(arr[i]);
  }
  return parts.join(sep);
};


/**
 * Creates an object mapping headings to row elements.
 * @private
 * @param {Array.<string>} headings
 * @param {Array.<string>} row
 * @return {Object}
 */
LocationDataSource.prototype.toObject_ = function(headings, row) {
  var result = {};
  for (var i = 0, ii = row.length; i < ii; i++) {
    result[headings[i]] = row[i];
  }
  return result;
};

/**
 * @const
 * @type {!storeLocator.FeatureSet}
 * @private
 */
LocationDataSource.prototype.FEATURES_ = new storeLocator.FeatureSet(
	//new storeLocator.Feature('All', 'All', 'fa-building', '../images/icons/icn_blog.png'/*tpa=http://www.broadcom.com/images/icons/icn_blog.png*/, '../images/icons/text_large.gif'/*tpa=http://www.broadcom.com/images/icons/text_large.gif*/),
	new storeLocator.Feature('locations', 'Broadcom Offices', '../images/icons/keyicon_brcmoffices.png'/*tpa=http://www.broadcom.com/images/icons/keyicon_brcmoffices.png*/, '../images/icons/mapmarker_brcmoffices.png'/*tpa=http://www.broadcom.com/images/icons/mapmarker_brcmoffices.png*/, '../images/icons/keyicon_brcmoffices.png'/*tpa=http://www.broadcom.com/images/icons/keyicon_brcmoffices.png*/),
	new storeLocator.Feature('salesoffices', 'Sales Offices', '../images/icons/keyicon_sales.png'/*tpa=http://www.broadcom.com/images/icons/keyicon_sales.png*/, '../images/icons/mapmarker_sales.png'/*tpa=http://www.broadcom.com/images/icons/mapmarker_sales.png*/, '../images/icons/keyicon_sales.png'/*tpa=http://www.broadcom.com/images/icons/keyicon_sales.png*/),
	new storeLocator.Feature('manufacturers', 'Manufacturing Rep', '../images/icons/keyicon_mreps.png'/*tpa=http://www.broadcom.com/images/icons/keyicon_mreps.png*/, '../images/icons/mapmarker_mreps.png'/*tpa=http://www.broadcom.com/images/icons/mapmarker_mreps.png*/, '../images/icons/keyicon_mreps.png'/*tpa=http://www.broadcom.com/images/icons/keyicon_mreps.png*/),
	new storeLocator.Feature('distributors', 'Distributors', '../images/icons/keyicon_distrib.png'/*tpa=http://www.broadcom.com/images/icons/keyicon_distrib.png*/, '../images/icons/mapmarker_distrib.png'/*tpa=http://www.broadcom.com/images/icons/mapmarker_distrib.png*/, '../images/icons/keyicon_distrib.png'/*tpa=http://www.broadcom.com/images/icons/keyicon_distrib.png*/)
);

/**
 * @return {!storeLocator.FeatureSet}
 */
LocationDataSource.prototype.getFeatures = function() {
  return this.FEATURES_;
};
}

//Open window call from Ethernet NIC
function OpenBrWindow(url, winName, features, myWidth, myHeight, isCenter) {
	if (isCenter) {
		var myLeft = ($(window).width() - myWidth) / 2;
		var myTop = ($(window).height() - myHeight) / 2;
		features += (features != '') ? ',' : '';
		features += ',left=' + myLeft + ',top=' + myTop;
	}
	window.open(url, winName,
		features + ((features != '') ? ',' : '') + 'width=' + myWidth + ',height=' + myHeight);
}

//convert array to object
function toObject(arr) {
  var rv = {};
  for (var i = 0; i < arr.length; ++i)
    rv[i] = arr[i];
  return rv;
}




function productSearchPagination(DropDown) {
		var TargetIndex = DropDown.selectedIndex;
		
		window.location.href = DropDown.options[TargetIndex].value;
}


function playSplashVideo($img) {
	$width = $img.clientWidth;
	$height = $img.clientHeight;
	$video_url = $($img).attr('data-video');
	
	var video = '<iframe width="'+ $width +'" height="'+ $height +'" src="'+ $video_url +'"></iframe>';
	$($img).replaceWith(video);
}

function setCookie(cname, cvalue) {
    //var d = new Date();
    //d.setTime(d.getTime() + (exdays*24*60*60*1000));
    //var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";  path=/";
} 

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
} 

function setCarouselTabs(currentIndex)
{
	if (currentIndex == 1)
	{
		$('.carousel_1').css("background-color", "red");
		$('.carousel_2').css("background-color", "#c0c0c0");
		$('.carousel_0').css("background-color", "#c0c0c0");
	}
	else if(currentIndex == 2)
	{
		$('.carousel_2').css("background-color", "red");
		$('.carousel_0').css("background-color", "#d1d1d1");
		$('.carousel_1').css("background-color", "#c0c0c0");	
	}
	else if(currentIndex == 0)
	{
		$('.carousel_0').css("background-color", "red");
		$('.carousel_1').css("background-color", "#c0c0c0");
		$('.carousel_2').css("background-color", "#d1d1d1");		
	}
	
}

//KF:
function adjustRelatedProducts()
{
	var browser_width = $( window ).width();
	if(browser_width < 1200 && browser_width > 940)
	{
		$('#related_products .panel').find('p').each(function(){
			var text = $(this).text();
			var string_length = text.length;
			if(string_length > 50)
			{
				
				text = text.substring(0,50) + "...";
				
				$(this).text(text);
			}
		});
	}
}
function initScrollToTop(){
  
  if( jQuery(window).width() <= 420)
    {
		//return;
	}

  jQuery('#top_link').click(function(event){
		var duration = 400;
		event.preventDefault();
        jQuery('html, body').animate({scrollTop: 0}, duration);
        return false;    
	
	/*var lnk = jQuery(this);
    var id = lnk.attr('href');
    id = id.replace(/.*?#/g,"#");
    var el = jQuery(id);
    jQuery.scrollTo(el, 200);
    return false;*/
  });
  
  jQuery(window).scroll(function(e){
    var offsetY = jQuery(window).scrollTop();
    //console.log(offsetY) 
    if(offsetY >= 1000 ){
       //jQuery('#top_link').show();
        jQuery('#top_link').addClass('active');
    }
    else if(offsetY < 1000 ){
      //jQuery('#top_link').hide();
       jQuery('#top_link').removeClass('active');
    }      
  });
}
//KF: smooth scrolling for anchors
function initGlobalSmoothScroll(){

	$('a[href*=#]:not([href=#])').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			//flag to skip scroll to top
			var skipScroll = false;
			//check class name for certain phrases
			var className = $(this).attr('class');			
			if (className.indexOf("carousel") >= 0)
			{
				skipScroll = true;
			}
			//disable for accordians by checking for data-toggle collapse
			//disable for tabs by checking for data-toggle tab
			// TODO: There as to be a more generic way of doing this instead of checking for data type.
			if ($(this).data('toggle') != "collapse" && $(this).data('toggle') != "tab" &&  $(this).data('slide') != "next" && skipScroll == false) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
					$('html,body').animate({
					  scrollTop: target.offset().top
					}, 300);
					//return false;
				}
			}
		}
	});

}

function supports_local_storage() {
  try {
    return 'localStorage' in window && window['localStorage'] !== null;
  } catch(e){
    return false;
  }
}

// blog single post ----------------------------------------------------------------------

// blog testing --------------------------------------------------------------------------------------------
$(document).ready(function() {
	if($('#blog-single').length != 0) {
	$('iframe[src*="http://www.broadcom.com/js/youtube.com"]').each(function() {
		if(!$(this).hasClass("embed-responsive-item"))
		{		
			$(this).addClass("embed-responsive-item");
			$(this).wrap("<div class='embed-responsive embed-responsive-16by9'/>");
		}
	});	 
	}
	
	// Wordpress Ajax Load More Plugin -------------------------------------
	$.fn.almComplete = function(alm){
		
		// sidebar blog link click
		$('.blog-sidebar-link').click(function() {
			//$('.blog-sidebar-row').removeClass('active'); // remove the class from the currently selected
			$('#sidebar_' + $(this).data("id")).addClass('active'); // add the class to the newly clicked link
			if(window.history.pushState)
			{			
				window.history.pushState({},"", $(this).data("link"));
			}
		});
		
		$('#loading').fadeIn("fast", function() {
			$(this).show();
		});
		
		// Set the title and url of first item.
		title = $('.blog-sidebar-row.active').data("title");
		link = $('.blog-sidebar-row.active').data("link");
		if (title != null && link != null) {
			document.title = title;
			if(window.history.pushState)
			{			
				window.history.pushState({}, title, link);
			}
		}
		
		// Use the filter
		var filter_url = window.location.protocol+'//'+window.location.hostname+'/blog/filter/'+$('#blog-single').attr('data-filter')+'/';
		$.get(filter_url, {alm_complete:true, alm_page:alm.page}, function(result) {
			
			var $element_result = $(result);
			$('#loading').hide().fadeOut("fast", function() {$(this).hide();});
			$('#content').append($element_result).fadeIn();
			$element_result.relive();
			
			$('.blog-content').waypoint(function(direction) {
				$('.blog-sidebar-row').removeClass('active'); // remove the class from the currently selected
				$('#sidebar_' + this.element.id).addClass('active'); // add the class to the newly clicked link
				title = $('#sidebar_' + this.element.id).data("title");
				link = $('#sidebar_' + this.element.id).data("link");
				if(window.history.pushState)
				{				
					window.history.pushState({}, title, link);
				}
				document.title = title;
				
				var scrollContainer = $('#sidebar');
				var scrollTo = $('#sidebar_' + this.element.id);
				scrollContainer.animate({
					scrollTop: scrollTo.offset().top - scrollContainer.offset().top + scrollContainer.scrollTop()}, 'fast');
				
				// Google Analytics tracking for inifinite scroll
				ga('send', 'pageview', link);
				ga('send', 'event', 'blogs', 'scroll', {'page': link});
				
			}, {offset: '50px'});
			
			
		});
		
		
	}
	
	
});

$(document).ready(function() {
	
	
	$('.csn_toggles').click(function() {
		var selected_class = $(this)[0];//save the element that is currently clicked
		var has_toggle = false;//check if there are any items toggled
		var default_image = "Unknown_83_filename"/*tpa=http://www.broadcom.com/js/csn_1_overall_full.jpg*/; //default image
		
		//check each toggle element
		$(".csn_toggles").each(function() {
			//if this toggle has an "in" class, check if it is for the currently clicked line				
			if($(this).hasClass("in"))
			{
				if($(this)[0].textContent != selected_class.textContent)
				{
					//if this is not the current toggle element remove the "in"
					$(this).removeClass("in");
				}		
				has_toggle = true;
			}			
		});	
		
		$('.collapse.in').collapse('hide');
		
		if(has_toggle)
		{
			$('#main_image').attr('src', "/images/application/" + $(this).data('image'));
		}
		else
		{
			//there are not toggles active, so change back to default
			$('#main_image').attr('src', "/images/application/" + default_image);
		}
	});
	
	
	
	
	
});


