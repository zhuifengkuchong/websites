//	CELGENE STOCK WIDGET SCRIPT

var _stock_widget = {

	API: _celgene_stock_widget_api.api,
	HTML: _celgene_stock_widget_template.html,
	ID: _celgene_stock_widget_reference.id,
	RIGHT_RAIL: _celgene_stock_widget_right_rail.right_rail,

	document_init: function() {
		_stock_widget.HTML = $(_stock_widget.HTML);
		if( _stock_widget.RIGHT_RAIL ) {
			_stock_widget.get_right_rail_stock_info();
		} else {
			_stock_widget.get_front_page_stock_info();
		}
	},

	window_init: function() {

	},

	get_right_rail_stock_info: function() {
		$.getJSON(_stock_widget.API + "&callback=?", {}, function(response) {
			response = response[0];
			_stock_widget.HTML.find(".stock-symbol").html(response.t);
			_stock_widget.HTML.find(".stock-updated").html(response.lt);
			//_stock_widget.HTML.find(".stock-price").html("$" + response.l);
			_stock_widget.HTML.find(".stock-price").html(response.l);
			var price_change = response.c;
			if( price_change < 0 ) {
				price_change = response.c.toString();
				price_change = "-$" + price_change.substring(1);
			} else {
				price_change = response.c.toString();
				price_change.replace("+", "");
				price_change = "+$" + price_change.substring(1);
			}
			_stock_widget.HTML.find(".stock-price-change").html(price_change);
			// The percentage change only comes back with a +/- when the price has gone down so we only remove the first character
			// if the price has gone down
			// Lloyd McGarrigal
			// 12/09/2014
			var percentage_change = (response.c < 0) ? response.cp.toString().substring(1) : response.cp.toString();
			_stock_widget.HTML.find(".stock-price-change-percent").html(percentage_change + "%");
			$("#" + _stock_widget.ID).html(_stock_widget.HTML);
		});
	},

	get_front_page_stock_info: function() {
		$.getJSON(_stock_widget.API + "&callback=?", {}, function(response) {
			response = response[0];
			_stock_widget.HTML.find(".stock-price").html(response.l);
			_stock_widget.HTML.find(".stock-price-change").html(response.c.toString());
			// The percentage change only comes back with a +/- when the price has gone down so we only remove the first character
			// if the price has gone down
			// Lloyd McGarrigal
			// 12/09/2014
			var percentage_change = (response.c < 0) ? response.cp.toString().substring(1) : response.cp.toString();
			_stock_widget.HTML.find(".stock-price-change-percent").html(percentage_change + "%");
			if( response.c < 0 ) {
				_stock_widget.HTML.find(".stock-price-change-indicator").addClass("icon-carrot-down");
			} else {
				_stock_widget.HTML.find(".stock-price-change-indicator").addClass("icon-carrot-up");
			}
			$("#" + _stock_widget.ID).html(_stock_widget.HTML);
		});
	}

}

//waitForJquery(function() {
	$(document).ready(function() {
		_stock_widget.document_init();

		$(window).load(function() {
			_stock_widget.window_init();
		});
	});
//});

