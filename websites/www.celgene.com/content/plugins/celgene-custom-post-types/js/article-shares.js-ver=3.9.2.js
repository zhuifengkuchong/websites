//	CELGENE ARTICLE SHARES SCRIPT

_article_share = {

	defaults: _celgene_article_share_defaults,
	default_window_options: {
		url: "", // url of popup window
		title: "", // title of popup window
		center: false, // whether or not to center the pop up window on the screen
		fullscreen: false, // whether or not to display the pop up window the size of the screen
		// window arguments
		width: 0, // width of popup window, will be overriden if fullscreen
		height: 0, // height of popup window, will be overriden if fullscreen
		left: 0, // left position of popup window, will be overriden if center
		top: 0, // top position of popup window, will be overriden if center
		toolbar: "no", // whether or not to show the toolbar, can also accept 1 or true, will parse to "yes" and "no"
		status: "no", // whether or not to show the status bar, can also accept 1 or true, will parse to "yes" and "no"
		scrollbars: "no", // whether or not to show the scroll bars, can also accept 1 or true, will parse to "yes" and "no"
		menubar: "no", // whether or not to show the menu bar, can also accept 1 or true, will parse to "yes" and "no"
		resizable: "no" // whether or not to be resizable, can also accept 1 or true, will parse to "yes" and "no"
	},
	option_arguments: ["toolbar", "status", "scrollbars", "menubar", "resizable"],
	other_arguments: ["top", "left", "width", "height"],

	options: {
		// facebook
		facebook_pop_up_width: 548, // popup width, has been tested to work but feel free to override
		facebook_pop_up_height: 325, // popup height, has been tested to work but feel free to override

		// twitter
		twitter_pop_up_width: 548, // popup width, has been tested to work but feel free to override
		twitter_pop_up_height: 325, // popup height, has been tested to work but feel free to override

		// linkedin
		linked_in_pop_up_width: 790, // popup width, has been tested to work but feel free to override
		linked_in_pop_up_height: 640, // popup height, has been tested to work but feel free to override

		// googleplus
		googleplus_in_pop_up_width: 517,
		googleplus_in_pop_up_height: 511

	},

	document_init: function() {
		_article_share.bind_single_facebook_share();
		_article_share.bind_single_twitter_share();
		_article_share.bind_single_linkedin_share();
		_article_share.bind_single_googleplus_share();
		_article_share.bind_single_pinterest_share();
		_article_share.bind_single_send_email();
	},

	window_init: function() {

	},

	// open popup window function
	open_window: function(options) {
		// we can get passed an object of options, or we'll populate from the default options
		for( i in _article_share.default_window_options ) {
			if( typeof options[i] === "undefined"  ) {
				options[i] = _article_share.default_window_options[i];
			}
		}
		// if centered
		if( options.center ) {
			options.left = ( screen.width / 2 ) - ( options.width / 2 );
			options.top = ( screen.height / 2 ) - ( options.height / 2 );
		}
		// if fullscreen
		if( options.fullscreen ) {
			options.width = screen.width;
			options.height = screen.height;
		}
		// set argument values in case we got passed integers or boolean
		var arguments = [];
		var arguments_str = "";
		for( i = 0; i < _article_share.option_arguments.length; i++ ) {
			if( options[ _article_share.option_arguments[i] ] && options[ _article_share.option_arguments[i] ] != _article_share.default_window_options[ _article_share.option_arguments[i] ] ) {
				options[ _article_share.option_arguments[i] ] = "yes";
			} else {
				options[ _article_share.option_arguments[i] ] = "no";
			}
			arguments.push(_article_share.option_arguments[i] + "=" + options[ _article_share.option_arguments[i] ]);
		}
		for( i = 0; i < _article_share.other_arguments.length; i++ ) {
			arguments.push(_article_share.other_arguments[i] + "=" + options[ _article_share.other_arguments[i] ]);
		}
		arguments_str = arguments.join(",");
		var newwindow = window.open(options.url, options.title, arguments_str);
		if( newwindow ) newwindow.focus();
	},

	bind_single_facebook_share: function() {
		$(document).on("click", ".single-facebook-share", function(event) {
			event.preventDefault();
			var url = $(this).attr("data-fb-url");
			var title = $(this).attr("data-fb-title");
			var description = $(this).attr("data-fb-description");
			var image = $(this).attr("data-fb-image");
			var share_url = "http://www.facebook.com/sharer.php?m2w&s=100&p[title]=" + encodeURIComponent( title ) + "&p[summary]=" + encodeURIComponent( description ) + "&p[url]=" + encodeURIComponent( url ) + "&p[images][0]=" + encodeURIComponent( image );
			_article_share.open_window({
				center: true,
				width: _article_share.options.facebook_pop_up_width,
				height: _article_share.options.facebook_pop_up_height,
				url: share_url
			});
		})
	},

	bind_single_twitter_share: function() {
		$(document).on("click", ".single-twitter-share", function(event) {
			event.preventDefault();
			var url = $(this).attr("data-tw-url");
			var text = $(this).attr("data-tw-text");
			var via = $(this).attr("data-tw-via");
			if( typeof via === "undefined" ) {
				via = _article_share.defaults.twitter_via
			}
			var share_url = "https://twitter.com/" + ($("html").hasClass("lte8") ? "intent/tweet" : "share") + "?url=" + encodeURIComponent( url ) + "&text=" + encodeURIComponent( text ) + "&via=" + encodeURIComponent( via );
			_article_share.open_window({
				center: true,
				width: _article_share.options.twitter_pop_up_width,
				height: _article_share.options.twitter_pop_up_height,
				url: share_url
			});
		})
	},

	bind_single_linkedin_share: function() {
		$(document).on("click", ".single-linkedin-share", function(event) {
			event.preventDefault();
			var url = $(this).attr("data-li-url");
			var title = $(this).attr("data-li-title");
			var summary = $(this).attr("data-li-summary");
			var source = $(this).attr("data-li-source");
			if( typeof source === "undefined" ) {
				source = _article_share.defaults.linkedin_source;
			}
			var share_url = "http://www.linkedin.com/shareArticle?mini=true&url=" + encodeURIComponent( url ) + "&title=" + encodeURIComponent( title ) + "&summary=" + encodeURIComponent( summary ) + "&source=" + encodeURIComponent( source );
			_article_share.open_window({
				center: true,
				width: _article_share.options.linked_in_pop_up_width,
				height: _article_share.options.linked_in_pop_up_height,
				url: share_url
			});
		})
	},
		bind_single_googleplus_share: function() {
		$(document).on("click", ".single-google-plus-share", function(event) {
			event.preventDefault();
			var url = $(this).attr("data-li-url");
			var title = $(this).attr("data-li-title");
			var summary = $(this).attr("data-li-summary");
			var source = $(this).attr("data-li-source");
			if( typeof source === "undefined" ) {
				source = _article_share.defaults.linkedin_source;
			}
			var share_url = "https://plus.google.com/share?url=" + encodeURIComponent( url ) + "&title=" + encodeURIComponent( title ) + "&summary=" + encodeURIComponent( summary ) + "&source=" + encodeURIComponent( source );
			_article_share.open_window({
				center: true,
				width: _article_share.options.linked_in_pop_up_width,
				height: _article_share.options.linked_in_pop_up_height,
				url: share_url
			});
		})
	},

	bind_single_pinterest_share: function() {
		$(document).on("click", ".single-pinterest-share", function(event) {
			event.preventDefault();
			var url = $(this).attr("data-li-url");
			var title = $(this).attr("data-li-title");
			var summary = $(this).attr("data-li-summary");
			var image = $(this).attr("data-li-image");

			if( !image || typeof image === "undefined" ) {
				image = _article_share.defaults.pinterest_image;
			}
			var share_url = "//pinterest.com/pin/create/button/?url=" + encodeURIComponent( url ) + "&description=" + encodeURIComponent( title + (summary ? ": " + summary : '') ) + (image ? "&media=" + encodeURIComponent( image ) : '');
			_article_share.open_window({
				center: true,
				width: _article_share.options.linked_in_pop_up_width,
				height: _article_share.options.linked_in_pop_up_height,
				url: share_url
			});
		})
	},

	bind_single_send_email: function() {
		$(document).on("click", ".single-send-email", function(event) {
			event.preventDefault();
			var email_url = $(this).attr("data-email-url");
			var email_title = encodeURIComponent($(this).attr("data-email-title"));
			var email_description = encodeURIComponent($(this).attr("data-email-description"));
			var email_image = $(this).attr("data-email-image");
			var email_body = "Check out this article I just read on Celgene!";
			email_body += "%0D%0A%0D%0A";
			email_body += email_title
			email_body += "%0D%0A%0D%0A";
			email_body += email_description;
			email_body += "%0D%0A%0D%0A";
			email_body += email_image;
			email_body += "%0D%0A%0D%0A";
			email_body += email_url;
			window.location.href = "mailto:?body=" + email_body + "&subject=" + email_title;
		});
	}

}
//waitForJquery(function() {
	$(document).ready(function() {
		_article_share.document_init();

		$(window).load(function() {
			_article_share.window_init();
		});
	});
//});
