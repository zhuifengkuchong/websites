window.App = {};
// Scripts
// ---------

/**
 * WildEmitter
 * A super lightweight EventEmitter similar to what comes in Node.js, but with a support for wildcard events
 * https://github.com/HenrikJoreteg/wildemitter
 */
function WildEmitter() {
    this.callbacks = {};
}

// Listen on the given `event` with `fn`. Store a group name if present.
WildEmitter.prototype.on = function (event, groupName, fn) {
    var hasGroup = (arguments.length === 3),
        group = hasGroup ? arguments[1] : undefined,
        func = hasGroup ? arguments[2] : arguments[1];
    func._groupName = group;
    (this.callbacks[event] = this.callbacks[event] || []).push(func);
    return this;
};

// Adds an `event` listener that will be invoked a single
// time then automatically removed.
WildEmitter.prototype.once = function (event, groupName, fn) {
    var self = this,
        hasGroup = (arguments.length === 3),
        group = hasGroup ? arguments[1] : undefined,
        func = hasGroup ? arguments[2] : arguments[1];
    function on() {
        self.off(event, on);
        func.apply(this, arguments);
    }
    this.on(event, group, on);
    return this;
};

// Unbinds an entire group
WildEmitter.prototype.releaseGroup = function (groupName) {
    var item, i, len, handlers;
    for (item in this.callbacks) {
        handlers = this.callbacks[item];
        for (i = 0, len = handlers.length; i < len; i++) {
            if (handlers[i]._groupName === groupName) {
                //console.log('removing');
                // remove it and shorten the array we're looping through
                handlers.splice(i, 1);
                i--;
                len--;
            }
        }
    }
    return this;
};

// Remove the given callback for `event` or all
// registered callbacks.
WildEmitter.prototype.off = function (event, fn) {
    var callbacks = this.callbacks[event],
        i;

    if (!callbacks) return this;

    // remove all handlers
    if (arguments.length === 1) {
        delete this.callbacks[event];
        return this;
    }

    // remove specific handler
    i = callbacks.indexOf(fn);
    callbacks.splice(i, 1);
    return this;
};

/// Emit `event` with the given args.
// also calls any `*` handlers
WildEmitter.prototype.emit = function (event) {
    var args = [].slice.call(arguments, 1),
        callbacks = this.callbacks[event],
        specialCallbacks = this.getWildcardCallbacks(event),
        i,
        len,
        item,
        listeners;

    if (callbacks) {
        listeners = callbacks.slice();
        for (i = 0, len = listeners.length; i < len; ++i) {
            if (listeners[i]) {
                listeners[i].apply(this, args);
            } else {
                break;
            }
        }
    }

    if (specialCallbacks) {
        len = specialCallbacks.length;
        listeners = specialCallbacks.slice();
        for (i = 0, len = listeners.length; i < len; ++i) {
            if (listeners[i]) {
                listeners[i].apply(this, [event].concat(args));
            } else {
                break;
            }
        }
    }

    return this;
};

// Helper for for finding special wildcard event handlers that match the event
WildEmitter.prototype.getWildcardCallbacks = function (eventName) {
    var item,
        split,
        result = [];

    for (item in this.callbacks) {
        split = item.split('*');
        if (item === '*' || (split.length === 2 && eventName.slice(0, split[0].length) === split[0])) {
            result = result.concat(this.callbacks[item]);
        }
    }
    return result;
};
App.EventEmitter = new WildEmitter();
/* End WildEmitter */

// Polyfills:
// ----------
if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function(searchElement /*, fromIndex */ ) {
        'use strict';
        if (this == null) {
            throw new TypeError();
        }
        var n, k, t = Object(this),
            len = t.length >>> 0;

        if (len === 0) {
            return -1;
        }
        n = 0;
        if (arguments.length > 1) {
            n = Number(arguments[1]);
            if (n != n) { // shortcut for verifying if it's NaN
                n = 0;
            } else if (n != 0 && n != Infinity && n != -Infinity) {
                n = (n > 0 || -1) * Math.floor(Math.abs(n));
            }
        }
        if (n >= len) {
            return -1;
        }
        for (k = n >= 0 ? n : Math.max(len - Math.abs(n), 0); k < len; k++) {
            if (k in t && t[k] === searchElement) {
                return k;
            }
        }
        return -1;
    };
}

     // .___________..______     __  .___  ___.
     // |           ||   _  \   |  | |   \/   |
     // `---|  |----`|  |_)  |  |  | |  \  /  |
     //     |  |     |      /   |  | |  |\/|  |
     //     |  |     |  |\  \--.|  | |  |  |  |
     //     |__|     | _| `.___||__| |__|  |__|

if (!String.prototype.trim) {
   String.prototype.trim=function(){return this.replace(/^\s+|\s+$/g, '');};
}

// Log
window.log=function(){log.history=log.history||[];log.history.push(arguments);if(this.console){console.log(Array.prototype.slice.call(arguments))}};

// IE console polyfill
if(typeof(console) === 'undefined') {
    var console = {}
    console.log = console.error = console.info = console.debug = console.warn = console.trace = console.dir = console.dirxml = console.group = console.groupEnd = console.time = console.timeEnd = console.assert = console.profile = function() {};
}

// Global Variables
var mobileMaxWidth  = 767;
var tabletMaxWidth  = 959;
var desktopMaxWidth = 1280;




/* Text Resizer
/* -------------------------------------------------*/
var _TEXT_RESIZER_INDEX = 0;

// Cookies
function setCookie(CookieName,CookieValue) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + 30);
    var c_value = escape(CookieValue) + ";expires=" + exdate.toUTCString();
    c_value = c_value + ";path=/;domain=.celgene.com"
    document.cookie = CookieName + "=" + c_value;
}

function readCookie(CookieName) {
    var nameEQ = CookieName + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

// Text adjust
function ts( inc, index ) {
    inc = parseInt(inc);
    var emFont = new Array( '1',  '1.2', '1.4');
    if( typeof index !== "undefined" ) {
        _TEXT_RESIZER_INDEX = parseInt(index);
    } else {
        _TEXT_RESIZER_INDEX = parseInt(_TEXT_RESIZER_INDEX) + inc;
    }
    if( _TEXT_RESIZER_INDEX > (emFont.length - 1) ) {
        _TEXT_RESIZER_INDEX = parseInt(emFont.length - 1);
    } else if( _TEXT_RESIZER_INDEX < 0 ) {
        _TEXT_RESIZER_INDEX = 0;
    }
    setCookie('tsize',_TEXT_RESIZER_INDEX);
    $('#primary > article').not('.newsroom article .supplementary').not('#primary article.news.custom').css({'fontSize': emFont[_TEXT_RESIZER_INDEX] + 'em', 'line-height': (parseInt(emFont[_TEXT_RESIZER_INDEX]) + .5625) });
}

//set font size on page load
if (readCookie('tsize') != null) {
    ts(0,readCookie('tsize'));
}
/* -------------------------------------------------*/




// Primary Mobile Navigation
/* -------------------------------------------------*/
$(document).ready(function() {
    if ($(window).width() < mobileMaxWidth) {
        mobileFlyout();
    }
});

// Off-canvas navigation
function mobileFlyout() {
    var viewportHeight  = typeof window.innerHeight !== 'undefined' ? window.innerHeight : $(window).height();
    var header          = $('header#branding');
    var page            = $('#page');
    var nav             = $('nav.mobile');
    var trigger         = $('.mobile-trigger');

    // Set canvas height based on viewport
    nav.css('height', viewportHeight);

    window.addEventListener("orientationchange", function() {
        var navHeight = typeof window.innerHeight !== 'undefined' ? window.innerHeight : $(window).height();
        nav.height( navHeight );
    }, false);

    // How wide (also how far off the page), the new navs are
    var offCanvasWidth  = parseFloat(nav.width());

    // Menu toggle
    $(trigger).click(function() {
        // If menu is already pulled out, put it away
        if ($(trigger).hasClass('active')) {

            $(trigger).removeClass('active');
            $(header)
                .animate({ right: '0' }, 200, function() {
                    // This stuff to take pace after the animation duration
                    $(this).css({
                        right : 'auto',
                        position : 'fixed'
                    });
                });
            $(page)
                .animate({ right: '0' }, 200, function() {
                    // This stuff to take pace after the animation duration
                    $(this).css({
                        right: 'auto',
                        position: 'initial'
                    });
                });
        }
        // If not, pull it out
        else {

            $(trigger).addClass('active');
            //$(nav).animate({ right: 0 }, 200 );
            $(header)
                .css( 'position', 'fixed' )
                .css( 'left', 'auto' )
                .animate({ right: (offCanvasWidth + 'px') }, 200 );
            $(page)
                .css( 'position', 'relative' )
                .animate({ right: (offCanvasWidth + 'px') }, 200 );
        }
    });
}
/* -------------------------------------------------*/



// 'Sidebar' Mobile Navigation
function sidebarNavMobile() {

    // Run if there's a secondary sidebar nav
    if ( $('aside#secondary').length != 0 ) {

        /* Add classes to the subnavs
        /* ---------------------------*/

        // Secondary
        $('#sidebar-nav > ul > li.active li').addClass('secondary');
        $('#sidebar-nav > ul > li.current_page_ancestor li').addClass('secondary');

        // Tertiary
        $('#sidebar-nav > ul > li.active li li')
            .removeClass('secondary')
            .addClass('tertiary');
        $('#sidebar-nav > ul > li.current_page_ancestor li li')
            .removeClass('secondary')
            .addClass('tertiary');

        // Quaternary
        $('#sidebar-nav > ul > li.active li li li')
            .removeClass('secondary')
            .removeClass('tertiary')
            .addClass('quaternary');
        $('#sidebar-nav > ul > li.current_page_ancestor li li li')
            .removeClass('secondary')
            .removeClass('tertiary')
            .addClass('quaternary');
        /* ---------------------------*/


        // Create <select> nav
        $('#primary').before('<nav class="sidebar-nav-mobile"><select></select></nav>');

        /* Functions for creating the option tags
        /* --------------------------------------*/
        function createOptions(selector) {
            if( typeof selector !== "undefined" ) {
                $('.sidebar-nav-mobile select').append('<option value="' + selector.attr('href') + '" class="' + selector.parent().attr('class') + '">' + selector.text() + '</option>');
            }
        }
        function createChildOptions(selector) {
            $(selector).each(function(i) {
                var childItems = $(this);
                createOptions(childItems);
            });
        }
        /* --------------------------------------*/


        /* Grab the information needed to create option tags
        /* -------------------------------------------------*/

        /* SINGLE CONDITION */
        if( $("body").hasClass("single") ) {
            var topLevelItem = $("#menu-mobile-single-newsroom li:eq(0) a:eq(0)");
            var childItems = $("#menu-mobile-single-newsroom li:eq(0) ul li a");
        /* ---------------- */
        } else {
            // If the top level navigation is active
            if ( $('#sidebar-nav > ul > li').hasClass('active') ) {
                var topLevelItem = $('#sidebar-nav > ul > li.active > a');
                var childItems = $('#sidebar-nav > ul > li.active li a');
            }
            // Or if the top level is just the root for the current page
            else if ( $('#sidebar-nav > ul > li').hasClass('current_page_ancestor') ) {
                var topLevelItem = $('#sidebar-nav > ul > li.current_page_ancestor > a');
                var childItems = $('#sidebar-nav > ul > li.current_page_ancestor li a');
            }
        }

        // Either way, get the top level and the children elements and pull them together into the <select> field
        createOptions(topLevelItem);
        createChildOptions(childItems);


        /* -------------------------------------------------*/


        /* Make it function as a navigation instead of a simple select menu
        /* ----------------------------------------------------------------*/

        // Initiate the custom select menu plugin
        $('.sidebar-nav-mobile select').not('.no-generatedcontent select').customSelectMenu();

        // Once intitiated, target the <li> it generates and make the page go where the clicked data-option-value references
        $(".sidebar-nav-mobile li").click(function() {
            window.location = $(this).attr('data-option-value');
        });
        /* ----------------------------------------------------------------*/
    }
}

sidebarNavMobile();




// Homepage slider
// ----------------------------
function homepageInjectCallouts() {
    return;

    // Hide the top-level callouts
    $('#feature > .callout').hide();

    // Grab the callout content
    var callout1    = $('#callout-1.callout').html();
    var callout2    = $('#callout-2.callout').html();
    var callout3    = $('#callout-3.callout').html();

    // Create a place to put the stacked callout boxes
    $('#articles .slides > li:first-child').before('<li><div id="article-callout-stack"/></li>');

    // Put the stacked callout boxes into place
    $('#article-callout-stack').html('<div id="callout-1" class="callout">' + callout1 + '</div>' + '<div id="callout-2" class="callout">' + callout2 + '</div>');

    // Other callout boxes have different treatment
    $('#articles .slides > li:nth-child(3)').before('<li><div id="callout-3" class="callout">' + callout3 + '</div></li>');

}

function homepageDesktopFlexslider(selector) {
    var $window = $(window);
    var flexslider;

    function getWinWidth() {
        return (window.innerWidth || document.documentElement.clientWidth);
    }
    function getPageWidth() {
        return $('#page').innerWidth();
    }
    function dynItemWidth() {
        return (getWinWidth() < mobileMaxWidth) ? getPageWidth() :
               (getWinWidth() < tabletMaxWidth) ? getPageWidth() / 3 : getPageWidth() / 4;
    }

    function getGridSize() {
        return  (getWinWidth() < mobileMaxWidth) ? 1 : (getPageWidth() < tabletMaxWidth) ? 3 : 4;
    }

    $window.resize(function() {
        //if(typeof(flexslider.vars) !== 'undefined') {
        if (!$.isEmptyObject(flexslider)) {
            flexslider.vars.itemWidth = dynItemWidth();
            flexslider.vars.minItems = getGridSize();
            flexslider.vars.maxItems = getGridSize();
        }
    });

    // Initialize the Flexslider
    $window.load(function() {
        $(selector).flexslider({
            animation: "slide",
            animationLoop: true,
            slideshow: false,
            itemWidth: dynItemWidth(),
            minItems: getGridSize(),
            maxItems: getGridSize(),
            start: function(slider){
                flexslider = slider;
                setSlideHeights();
            }
        });
    });
}

function setSlideHeights() {
    var slideViewPort = $('.flex-viewport');
    var slideHeight = slideViewPort.height();
    slideViewPort.find('.slides>li').css('height', slideHeight);

    // var slides = $('ul.slides').find('> li');
    // var slideHeight = getMaxHeight(slides);


    // set height for stacking callouts
    $('#articles #callout-1').css('height', slideHeight / 2);
    $('#articles #callout-2').css('height', slideHeight / 2);

    //set slide heights
    // slides.each(function(x){
    //  $(this).css('height', slideHeight);
    // });
}

function getMaxHeight(slides) {
    var arr = [];
    slides.each(function(x){
        arr.push(parseInt($(this).css('height'), 10));
    });
    return Math.max.apply(null, arr);
}


// Homepage Hero Case C
// Center the thumbnail like the carousel articles
function homepageHeroRow(selector) {
    if ($(selector).length != 0) {
        $(selector).find('article img').css('marginLeft', $(this).width() / -6);
        $(selector).find('article:nth-child(even) img').css('marginLeft', '-55%');
    }
}


// Collect and specify actions for the homepage using the functions above
function homepageFlexslider(selector) {
    if ( $(window).width() < mobileMaxWidth ) {
        $(".mobile-callout").show();
        $(".mobile-callout-hide").hide();
        $(".mobile-callout-hide").remove();
        $(selector).flexslider();
        fixMobileFlexsliderHeight();
    } else {
        $(".mobile-callout").hide();
        $(".mobile-callout").remove();
        homepageInjectCallouts();
        homepageDesktopFlexslider(selector);
    }
}

function fixMobileFlexsliderHeight() {
	// Lloyd McGarrigal - lloyd@arteric.com
	// There was a height issue on the flexslider
	// this fixes that
	$('.slides').css('height', document.documentElement.clientWidth - 2);
}

// END Homepage slider


// Call it all and make it so.
$(document).ready(function() {
    homepageFlexslider('#articles.flexslider');

    // Jira: CELG:108
    // sizing issue with small images
    // an existing width: 100% style is being overridden
    // with an inline style of width: 320px
    // I'm going to start by overriding that EXACT inline style
    // ultimately it would be better to locate where that style is
    // being added but it may be in the flexslider plugin itself
    $('.flexslider-container figure img').each(function(){
        var $this = $(this);
        if( $this.css('width') === '320px' ) {
            $this.css('width', '100%');
        }
    });

    homepageHeroRow('#hero .row');

    // (almost) All Select Fields
    $('select').not('.unstyled-select').not('.sidebar-nav-mobile select').not('.no-generatedcontent select').customSelectMenu();
    $('.drop-nav').change( function() {
        if ($(this).val() != '') {
            document.location.href =  $(this).val();
        }
     });

    //  form conditional logic
    if( $("#input_1_12").length ) {
    	App.EventEmitter.on('window:load', function() {
            var _selected_country = $("#input_1_12 ul li.selected").attr("data-option-value");
            form_conditional(_selected_country);

	        $("#input_1_12 ul li").click(function() {
	            var _selected_country = $(this).attr("data-option-value");
	            form_conditional(_selected_country);
	        });
    	 });
    }

    //  implement limiter
    if( $("#field_1_19").length ) {
        $("#field_1_19 .ginput_container").css({
            position: "relative"
        });
        var _limiter_limit = 250;
        var _limiter_html = $('<div class="limiter-messaging">Characters remaining: <span class="limiter-span" id="limiter-span"></span></div>');
        _limiter_html.css({
            position: "absolute",
            bottom: "5px",
            left: "5px",
            "font-size": "10px"
        });
        $("#field_1_19 #input_1_19").after(_limiter_html);
        $("#field_1_19 #input_1_19").limiter(_limiter_limit, _limiter_html.find("#limiter-span"));
    }

});

//  form conditional
var _required_class = "gfield_contains_required";
function form_conditional(_selected_country) {
	// moved eventEmmiter to line 577 where this function assigned and called
//    App.EventEmitter.on('window:load', function() {
        if( _selected_country == "US" || _selected_country == "United States" ) {
            $("#field_1_10").show();
            $("#field_1_10").addClass(_required_class);
        } else {
            $("#field_1_10").hide();
            $("#field_1_10").removeClass(_required_class);
            $("#field_1_10 select.gfield_select").val("Alabama");
            $("#field_1_10 .custom-select-menu .selection-made").html('Alabama<i class="icon-arrow-light-down"></i>');
            $("#field_1_10 .custom-select-menu ul li").removeClass("selected");
            $("#field_1_10 .custom-select-menu ul li[data-option-value=Alabama]").addClass("selected");
        }
//    });
}

//  form limiter
(function($) {
    $.fn.extend( {
        limiter: function(limit, elem) {
            $(this).on("keyup focus blur", function() {
                setCount(this, elem);
            });
            function setCount(src, elem) {
                var chars = src.value.length;
                if( chars > limit ) {
                    src.value = src.value.substr(0, limit);
                    chars = limit;
                }
                elem.html( limit - chars );
            }
            setCount($(this)[0], elem);
        }
    });
})(jQuery);

// jira: CELG-9
$(function() {
    // The press releases on the therapies page are taller than the container
    // can hold.  The excess is hidden but it looks bad.  The problem is it loads
    // a group of press releases like the green press releases on the right side of
    // the newsroom page so there are 4 and the small grid can only hold three.
    // I am going to limit the grid on the therapies page to as many as will fit,
    // probably 3, and give them enough margin to fit well in there properly.
    // limiting this to the therapies page for now
    var context = $('.page-id-6');
    if( context.length > 0 ) {
        var gridPressReleases = context.find('nav#grid .press-releases');
        var gridLis = gridPressReleases.find('li');
        var holdingColumn = gridPressReleases.closest('.col');
        var columnSpace = holdingColumn.height();

        // set up li's
        gridLis.find('h4').css('margin', '0');

        // determine available space
        // the distance in position between the top of the column
        // and the top of the first li is how much space is already
        // lost
        var lostSpace = $( gridLis.get(0) ).position().top - holdingColumn.position().top;
        var availableSpace = columnSpace - lostSpace;

        // determine how many of the lis can fit
        var canHold = 0;
        var usedSpace = 0;
        gridLis.each( function() {
            var $element = $(this);
            if( (usedSpace + $element.height()) > availableSpace ) {
                $element.remove();
                return;
            } else {
                canHold += 1;
                usedSpace += $element.height();
            }
        } );
        var distributedSpace = availableSpace - usedSpace;
        gridLis.css( 'margin-bottom', distributedSpace / canHold );
    }

});

// Lloyd McGarrigal - lloyd@arteric.com
// Temporary fix for IE9 search bar problem
// 8/4/2014
$(function(){
    if( navigator.userAgent.indexOf( 'MSIE 9' ) > 0 ) {
        if( document.documentElement.clientWidth > 767 && document.documentElement.clientWidth < 800 ) {
            $('nav.secondary li a').css('font-size', '12px');
        }
    }
});

/**
 * Ensure callout-4 is not accidentally displayed
 * Lloyd McGarrigal
 */
$(function() {
    var callout4 = $('#callout-4');
    if( $('body').hasClass('home') && callout4.css('display') == 'block' ) {
        callout4.css('display', 'none');
    }
});


/**
 * Fire off a generic event when custom-select-menu is clicked
 * Lloyd McGarrigal
 * 12/22/2014
 */
$(function(){
    App.EventEmitter.on('window:load',function() {
       $('.custom-select-menu ul li').on('click', function() {
           App.EventEmitter.emit('custom-select-menu:click', $(this)[0]);
       });
    });

    // handle country dropdown clicks
    App.EventEmitter.on('custom-select-menu:click', function(element){
        var $el = $(element);
        // if the click is in the country dropdown handle the navigation
        if( $el.closest('.menu-global-sites-container, .menu-therapies-container').length > 0 ) {
            fakeAnchorClick(element);
        }
    });

    function fakeAnchorClick(element) {
        var $el = $(element);
        // gather up all the attributes of the element
        var attrs = {};
        for( var att, i=0, atts=element.attributes, l=atts.length; i<l; i+=1 ) {
            att = atts[i];
            attrs[att.nodeName] = att.nodeValue;
        }

        // console.log(attrs);

        // add all the attributes to the fake anchor
        var fakeAnchor = $('<a>', attrs);
        if( attrs['data-option-value'] !== '' && typeof attrs['data-option-value'] !== 'undefined' ) {
            fakeAnchor.attr('href', attrs['data-option-value']);
            // if it's going somewhere on the current site just go there
            // if( attrs['data-option-value'].indexOf('http') < 0 ) {
            if( checkIfInternal(attrs['data-option-value']) ) {
                window.location = attrs['data-option-value'];
            }
        }

        function checkIfInternal(target) {
            // console.log('got: ' + target);
            if( target.indexOf(location.host) >= 0 ||
                target.indexOf('http') < 0 ) {
                // exceptions for
                // AU - Australia
                // TR - Turkey
                if( target.indexOf(location.host+'.tr') > 0 ||
                    target.indexOf(location.host+'.au') > 0) {
                    return false;
                }
                return true;
            }
            return false;
        }

        // add some dimensions to the anchor so that it
        // can 'appear' on page
        fakeAnchor.css({
            'height': 1,
            'width': 1
        });

        $('body').append(fakeAnchor);
        // click the new anchor tag
        fakeAnchor.click();
        // remove the fake anchor
        setTimeout( function(){
            fakeAnchor.remove();
        }, 100 );
    }
});

/**
 * Anthony Outeiral
 * Listens for the EventEmitter to fire any event with :click in the name.
 * 12/22/2014
 */
$(function(){
    App.EventEmitter.on('*:click', function(){
        if( $(arguments[1]).parents('.countries').length > 0 ){
            dataLayer.push({
                'event': 'cust.click',
                'triggeringList': 'Countries',
                'triggeringObject': arguments[1].innerText
            });
        }
        if( $(arguments[1]).parents('.menu-therapies-container').length > 0 ){
            dataLayer.push({
                'event': 'cust.click',
                'triggeringList': 'Find a Therapy',
                'triggeringObject': arguments[1].innerText
            });
        }
    });
});

/**
 * Anthony Outeiral
 * Add datalayer pushes to the investors and careers in very specific locations
 * 12/23/2014
 */
$(function(){
    // top nav investor link
    var $top_investor = $('nav.secondary').find('[href*="investors"]');
    if( $top_investor.length > 0 ) {
        $top_investor.on('click', function() {
            dataLayer.push({
                'event' : 'http://www.celgene.com/content/themes/celgene/js/cust.nav',
                'triggeringNav' : 'Top Nav',
                'triggeringObject' : 'Investors Section'
            });
        });
    }

    // mobile nav investor link
    var $mobile_investor = $('nav.secondary-mobile').find('[href*="investors"]');
    if( $mobile_investor.length > 0 ) {
        $mobile_investor.on('click', function() {
            dataLayer.push({
                'event' : 'http://www.celgene.com/content/themes/celgene/js/cust.nav',
                'triggeringNav' : 'Mobile Nav',
                'triggeringObject' : 'Investors Section'
            });
        });
    }

    // footer investor link
    var $footer_investor = $('#menu-footer-primary-2').find('[href*="investors"]');
    if( $footer_investor.length > 0 ) {
        $footer_investor.on('click', function() {
            dataLayer.push({
                'event' : 'http://www.celgene.com/content/themes/celgene/js/cust.nav',
                'triggeringNav' : 'Footer',
                'triggeringObject' : 'Investors Section'
            });
        });
    }

    // top nav career link
    var $top_investor = $('nav.secondary').find('[href*="careers"]');
    if( $top_investor.length > 0 ) {
        $top_investor.on('click', function() {
            dataLayer.push({
                'event' : 'http://www.celgene.com/content/themes/celgene/js/cust.nav',
                'triggeringNav' : 'Top Nav',
                'triggeringObject' : 'Careers'
            });
        });
    }

    // mobile nav career link
    var $mobile_investor = $('nav.secondary-mobile').find('[href*="careers"]');
    if( $mobile_investor.length > 0 ) {
        $mobile_investor.on('click', function() {
            dataLayer.push({
                'event' : 'http://www.celgene.com/content/themes/celgene/js/cust.nav',
                'triggeringNav' : 'Mobile Nav',
                'triggeringObject' : 'Careers'
            });
        });
    }

    // footer career link
    var $footer_investor = $('#menu-footer-primary-2').find('[href*="careers"]');
    if( $footer_investor.length > 0 ) {
        $footer_investor.on('click', function() {
            dataLayer.push({
                'event' : 'http://www.celgene.com/content/themes/celgene/js/cust.nav',
                'triggeringNav' : 'Footer',
                'triggeringObject' : 'Careers'
            });
        });
    }
});

/**
 * Lloyd McGarrigal
 * Adding window onload event to EventEmitter
 * 2/12/2015
 */
if( typeof App.EventEmitter !== 'undefined' ) {
    $(window).on('load', function() {
        App.EventEmitter.emit('window:load');
    });
} else {
    // console.log('No event emitter found');
}

/**
 * Lloyd McGarrigal
 * Adding a function to hold other functions until jQuery is loaded
 * 2/12/2015
 */
// See header.php