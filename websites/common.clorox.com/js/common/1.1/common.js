/*
 * Clorox Common JS
 *
 * Library of commonly used functions on Clorox sites. Include jQuery, and then this file.
 * To use a given function, call it from a $(document).ready() in your own code; e.g.:
 *
 * $(document).ready( function() {
 *	$clxcommon.initExternal();
 *  $clxcommon.initWarning();
 * }
 * 
 * Details of each specific function are outlined below.
 *
 * @version 1.0
 * @requires jquery 1.3.2
 *
 */

// init the $clxcommon namespace. All functions should be part of this namespace
var $clxcommon = window.$clxcommon || {};  

/**
 * initExternal(): Open a link in a new window if it has rel='external'
 *
 */
$clxcommon.initExternal = function() {
 	$('a[rel="external"]').click( function() {
        window.open( $(this).attr('href') );
        return false;
    });
}

/*
 * addMouseoverClass(): this function attaches a class to an element on mouseover, and
 * removes it on mouseout. It is used to handle rollovers/warning links in IE6, which 
 * doesn't support the CSS :hover property. The idea is based on this:
 * http://www.htmldog.com/articles/suckerfish/dropdowns/
 * but modified for jQuery
 */
$clxcommon.addMouseoverClass = function(jQuerySelectorToAttach, className) {
	// fix warning popups in IE6
	$(jQuerySelectorToAttach).mouseover(function() {
		$(this).addClass(className);
	})
	
	$(jQuerySelectorToAttach).mouseout(function() {
		$(this).removeClass(className);
	})
}

/**
 * initWarning(): Add a warning about leaving a Clorox site. Add a class of 'warning' to any element
 * (except a link); warningLink should be applied to links. That keeps IE6 happy.
 *
 */ 
$clxcommon.initWarning = function() {
    // insert a tooltip span that tells you that you're leaving the site
    // class='warning' can be applied to anything other than a link
    // class='warningLink' should only be applied to <a> tags, as it is needed to keep IE6 happy
    $('.warning, a.warningLink').append("<span class='warning-text-eng'>You are now exiting this site.  We are not responsible for the content or data collection practices of the site you are about to visit.</span>");
    $('.warning span').addClass('externalTooltip');	// only add externalTooltip class to the 'warning' objects (not the 'warningLink' objects)
    												// because IE6 has a hissy fit if the span inside the warningLink object has a class

	$clxcommon.addMouseoverClass('.warning', 'whover');
}

/**
 * initWarningFrench(): Warning Link in French
 * 
 */ 
$clxcommon.initWarningFrench = function() {
    // class='warning-fr' can be applied to anything other than a link
    // class='warningLink-fr' should only be applied to <a> tags, as it is needed to keep IE6 happy
    $('.warning-fr, a.warningLink-fr').append("<span class='warning-text-fra'>Vous vous appr&ecirc;tez &agrave; quitter ce site. Nous ne sommes pas responsables des pratiques en mati&egrave;re de collecte de donn&eacute;es du prochain site que vous visiterez.</span>");
    $('.warning-fr span').addClass('externalTooltip');	// only add externalTooltip class to the 'warning' objects (not the 'warningLink' objects)
    												// because IE6 has a hissy fit if the span inside the warningLink object has a class

	$clxcommon.addMouseoverClass('.warning-fr', 'whover');
}

/**
 * initWarningSpanish(): Warning Link in Spanish
 * 
 */ 
$clxcommon.initWarningSpanish = function() {
    // class='warning-sp' can be applied to anything other than a link
    // class='warningLink-sp' should only be applied to <a> tags, as it is needed to keep IE6 happy
    $('.warning-sp, a.warningLink-sp').append("<span class='warning-text-esp'>Est&aacute; saliendo del sitio. No nos responsabilizamos por el contenido o las pr&aacute;cticas de recolecci&oacute;n de datos del sitio que est&aacute; a punto de visitar.</span>");
    $('.warning-sp span').addClass('externalTooltip');	// only add externalTooltip class to the 'warning' objects (not the 'warningLink' objects)
    												// because IE6 has a hissy fit if the span inside the warningLink object has a class

	$clxcommon.addMouseoverClass('.warning-sp', 'whover');
}



/**
 * createLink(): Given a jQuery selector and a URL, add an onclick event that 
 * lets you click on a div to go to a new page 
 *
 */
$clxcommon.createLink = function(cssSelector, url) {
	$(cssSelector).click(function() { 
		if ($(this).hasClass('external')) {
			window.open( url );
        	return false;
		} else {
			location.href=url;
		}
	});
}

/**
 * trackAnalyticsPageView(): tracks a pageview for Google Analytics. We track these as
 * '/pageType/pageName'
 *
 * Updated 5/5/2010: added classes 'clorox-promo', 'clorox-download', etc. to replace 'promo', 'download', etc. (but still backward compatible).
 */
$clxcommon.trackAnalyticsPageView = function(pageType, pageName) {

	// standardize our page types. We don't want to litter GA with a bunch of junk, so
	// we check to make sure the pageType in question is on the 'OK' list (and throw a 
	// really obvious alert if it isn't)
	if (!(pageType == 'promo' || pageType == 'download' || pageType == 'popup' || pageType == 'link' || pageType == 'video' || pageType == 'virtual' || pageType == 'clorox-promo' || pageType == 'clorox-download' || pageType == 'clorox-popup' || pageType == 'clorox-link' || pageType == 'clorox-video' || pageType=='clorox-virtual')) {
		alert("GA pageType '" + pageType + "' is not a known type.'");
		return;
	}

	if (pageName == '') {
		alert("pageName was passed blank.'");
		return;
	}

	try {
		_gaq.push(['_trackPageview', '/' + pageType + '/' + pageName]);		
	} catch(err) {}
}

/*
 * attachClickAnalytics(): if you want to automatically track outbound/download clicks on an entire
 * set of links, add a class to each link you want to track (e.g. 'clorox-promo'), and make sure
 * that each one has an id associated with it. Then call this function, e.g. 
 * $clxcommon.attachClickAnalytics('clorox-promo');
 * A click handler will be attached to each 'clorox-promo' link, and that will call the trackAnalyticsPageView()
 * function anytime somebody clicks on the given link, tracking the click in GA as 
 * /class/id (e.g. /clorox-promo/jas-clorox-promo).
 *
 * @param string classToAttach Name of the class to attach the click handlers to. Note that this value 
 * 		  must match one of the allowed page types in trackAnalyticssPageView(), e.g. 'clorox-promo', 'clorox-download', etc.
 * 
 */
$clxcommon.attachClickAnalytics = function(classToAttach) {
	$('.' + classToAttach).each(function() {
		$(this).mousedown(function() {	// use mousedown, not click, because it works with the embedded links
										// and gives us a bit more time to make the tracking call
			$clxcommon.trackAnalyticsPageView(classToAttach, this.id);			
		})
	});
}

// initialize the functions you need on document ready. You'll want to do this in YOUR
// code; this is just an example how in this common file.
//$(document).ready( function() {
//	$clxcommon.initExternal();
//	$clxcommon.attachClickAnalytics('clorox-download');
//});
