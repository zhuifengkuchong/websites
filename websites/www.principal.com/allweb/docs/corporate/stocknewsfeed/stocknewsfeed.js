<!-- CALLBACK FUNCTIONS -->
function widgetOnLoad(para){
	if(typeof para=='string'){
		para=eval('('+para+')');
	}
}

function widgetOnHeightChange(para){
	if(typeof para=='string'){
		para=eval('('+para+')');
	}
}

function widgetOnTickerChange(para){
	if(typeof para=='string'){
		para=eval('('+para+')');
	}
	window.location="http://www.principal.com/allweb/docs/corporate/stocknewsfeed/www.google.com";
}

<!-- FUNCTIONS TO BUILD WIDGETS -->
function retrieveStockFeed(h,w,s,t){
	QSAPI.init('PRIP',{
		env:'',
		proxyurl:'https://www.principal.com/allweb/docs/corporate/stocknewsfeed/qsapi_proxy.html',
		profileurl:'https://www.principal.com/allweb/docs/corporate/stocknewsfeed/Mstar_profile.xml',
		email:'http://www.principal.com/allweb/docs/corporate/stocknewsfeed/DEMO_00001@PRIP.com'
	},function(){
		var symbolType = t;
		var delimiter = ":";
		var symbolString = symbolType.concat(delimiter,s);

		QSAPI.createWidget(
			document.getElementById('ifream-container-widget0'),
			'http://quotespeed.morningstar.com/apis/api.jsp?instid=PRIP&module=quote',
			{
				"symbol":symbolString,
				"height":h,
				"fixHeight":false,
				"width":w,
				"showSetting":0,
				"autoComplete":1
			},
			{
				onLoad:'widgetOnLoad',
				onHeightChange:'widgetOnHeightChange'
			}
		);
	})
}

function retrieveStockTicker(h,w){
	QSAPI.init('PRIP',{
		env:'',
		proxyurl:'https://www.principal.com/allweb/docs/corporate/stocknewsfeed/qsapi_proxy.html',
		profileurl:'https://www.principal.com/allweb/docs/corporate/stocknewsfeed/Mstar_profile.xml',
		email:'http://www.principal.com/allweb/docs/corporate/stocknewsfeed/DEMO_00001@PRIP.com'
	},function(){
		QSAPI.createWidget(
			document.getElementById('ifream-container-widget4'),
			'http://quotespeed.morningstar.com/apis/api.jsp?instid=PRIP&module=quote&symbol=XNYS:PFG',
			{
				"dataPointList":'LastPrice',
				"showHead":0,
				"showTickerType":1,
				"width":w,
				"height":h
			},
			{
				onLoad:'widgetOnLoad',
				onHeightChange:'widgetOnHeightChange'
			}
		);
	})
}

function retrieveInsideStockTicker(h,w){
	QSAPI.init('PRIP',{
		env:'',
		proxyurl:'https://inside.theprincipal.net/allweb/docs/corporate/stocknewsfeed/qsapi_proxy.html',
		profileurl:'https://inside.theprincipal.net/allweb/docs/corporate/stocknewsfeed/Mstar_profile.xml',
		email:'http://www.principal.com/allweb/docs/corporate/stocknewsfeed/DEMO_00001@PRIP.com'
	},function(){
		QSAPI.createWidget(
			document.getElementById('ifream-container-widget4'),
			'http://quotespeed.morningstar.com/apis/api.jsp?instid=PRIP&module=quote&symbol=XNYS:PFG',
			{
				"dataPointList":'LastPrice',
				"showHead":0,
				"showTickerType":1,
				"width":w,
				"height":h
			},
			{
				onLoad:'widgetOnLoad',
				onHeightChange:'widgetOnHeightChange'
			}
		);
	})
}

function retrieveSecureStockTicker(h,w){
	QSAPI.init('PRIP',{
		env:'',
		proxyurl:'https://www.principal.com/allweb/docs/corporate/stocknewsfeed/qsapi_proxy.html',
		profileurl:'https://www.principal.com/allweb/docs/corporate/stocknewsfeed/Mstar_profile.xml',
		email:'http://www.principal.com/allweb/docs/corporate/stocknewsfeed/DEMO_00001@PRIP.com'
	},function(){
		QSAPI.createWidget(
			document.getElementById('ifream-container-widget4'),
			'http://quotespeed.morningstar.com/apis/api.jsp?instid=PRIP&module=quote&symbol=XNYS:PFG',
			{
				"dataPointList":'LastPrice',
				"showHead":0,
				"showTickerType":1,
				"width":w,
				"height":h
			},
			{
				onLoad:'widgetOnLoad',
				onHeightChange:'widgetOnHeightChange'
			}
		);
	})
}

/*
   Categories:
   0=all groups
   1=financial news
   2=market & economy
   3=world news
   4=national news
   5=corporate news
   6=small business news
*/
function retrieveNewsFeed(count, category, h, w){
	QSAPI.init('PRIP',{
		env:'',
		proxyurl:'https://www.principal.com/allweb/docs/corporate/stocknewsfeed/qsapi_proxy.html',
		profileurl:'https://www.principal.com/allweb/docs/corporate/stocknewsfeed/Mstar_profile.xml',
		email:'http://www.principal.com/allweb/docs/corporate/stocknewsfeed/DEMO_00001@PRIP.com'
	},function(){
		QSAPI.createWidget(
			document.getElementById('ifream-container-widget1'),
			'http://quotespeed.morningstar.com/apis/api.jsp?instid=PRIP&module=newsOverview',
			{
				"width":w,
				"height":h,
				"fixHeight":false,
				"maxCount":count,
				"category":category,
				"showTitle":false
			},
			{
				onLoad:'widgetOnLoad',
				onHeightChange:'widgetOnHeightChange'
			}
		);
	})
}

function retrieveMarketIndex(h,w,ch){
	QSAPI.init('PRIP',{
		env:'',
		proxyurl:'https://www.principal.com/allweb/docs/corporate/stocknewsfeed/qsapi_proxy.html',
		profileurl:'https://www.principal.com/allweb/docs/corporate/stocknewsfeed/Mstar_profile.xml',
		email:'http://www.principal.com/allweb/docs/corporate/stocknewsfeed/DEMO_00001@PRIP.com'
	},function(){
		QSAPI.createWidget(
			document.getElementById('ifream-container-widget2'),
			'http://quotespeed.morningstar.com/apis/api.jsp?instid=PRIP&module=market&submodule=marketindex',
			{
				"height":h,
				"width":w,
				"chartHeight":ch
			},
			{
				onLoad:'widgetOnLoad',
				onHeightChange:'widgetOnHeightChange'
			}
		);
	})
}

function retrieveQuickMarketIndex(h,w){
	QSAPI.init('PRIP',{
		env:'',
		proxyurl:'https://www.principal.com/allweb/docs/corporate/stocknewsfeed/qsapi_proxy.html',
		profileurl:'https://www.principal.com/allweb/docs/corporate/stocknewsfeed/Mstar_profile.xml',
		email:'http://www.principal.com/allweb/docs/corporate/stocknewsfeed/DEMO_00001@PRIP.com'
	},function(){
		QSAPI.createWidget(
			document.getElementById('ifream-container-widget3'),
			'http://quotespeed.morningstar.com/apis/api.jsp?instid=PRIP&module=market&submodule=marketindexquick',
			{
				"height":h,
				"width":w,
				"chartHeight":h
			},
			{
				onLoad:'widgetOnLoad',
				onHeightChange:'widgetOnHeightChange'
			}
		);
	})
}

function retrieveSecureQuickMarketIndex(h,w){
	QSAPI.init('PRIP',{
		env:'',
		proxyurl:'https://www.principal.com/allweb/docs/corporate/stocknewsfeed/qsapi_proxy.html',
		profileurl:'https://www.principal.com/allweb/docs/corporate/stocknewsfeed/Mstar_profile.xml',
		email:'http://www.principal.com/allweb/docs/corporate/stocknewsfeed/DEMO_00001@PRIP.com'
	},function(){
		QSAPI.createWidget(
			document.getElementById('ifream-container-widget3'),
			'http://quotespeed.morningstar.com/apis/api.jsp?instid=PRIP&module=market&submodule=marketindexquick',
			{
				"height":h,
				"width":w,
				"chartHeight":h
			},
			{
				onLoad:'widgetOnLoad',
				onHeightChange:'widgetOnHeightChange'
			}
		);
	})
}

function retrievePFundsQuickMarketIndex(h,w){
	QSAPI.init('PRIP',{
		env:'',
		proxyurl:'https://www.principalfunds.com/allweb/docs/corporate/stocknewsfeed/qsapi_proxy.html',
		profileurl:'https://www.principalfunds.com/allweb/docs/corporate/stocknewsfeed/Mstar_profile.xml',
		email:'http://www.principal.com/allweb/docs/corporate/stocknewsfeed/DEMO_00001@PRIP.com'
	},function(){
		QSAPI.createWidget(
			document.getElementById('ifream-container-widget3'),
			'http://quotespeed.morningstar.com/apis/api.jsp?instid=PRIP&module=market&submodule=marketindexquick',
			{
				"height":h,
				"width":w,
				"chartHeight":h
			},
			{
				onLoad:'widgetOnLoad',
				onHeightChange:'widgetOnHeightChange'
			}
		);
	})
}

function retrievePTrustQuickMarketIndex(h,w){
	QSAPI.init('PRIP',{
		env:'',
		proxyurl:'https://www.principaltrust.com/allweb/docs/corporate/stocknewsfeed/qsapi_proxy.html',
		profileurl:'https://www.principaltrust.com/allweb/docs/corporate/stocknewsfeed/Mstar_profile.xml',
		email:'http://www.principal.com/allweb/docs/corporate/stocknewsfeed/DEMO_00001@PRIP.com'
	},function(){
		QSAPI.createWidget(
			document.getElementById('ifream-container-widget3'),
			'http://quotespeed.morningstar.com/apis/api.jsp?instid=PRIP&module=market&submodule=marketindexquick',
			{
				"height":h,
				"width":w,
				"chartHeight":h
			},
			{
				onLoad:'widgetOnLoad',
				onHeightChange:'widgetOnHeightChange'
			}
		);
	})
}


function retrieveAutoComplete(w){
	QSAPI.init('PRIP',{
		env:'',
		proxyurl:'https://www.principal.com/allweb/docs/corporate/stocknewsfeed/qsapi_proxy.html',
		profileurl:'https://www.principal.com/allweb/docs/corporate/stocknewsfeed/Mstar_profile.xml',
		email:'http://www.principal.com/allweb/docs/corporate/stocknewsfeed/DEMO_00001@PRIP.com'
	},function(){
		QSAPI.createWidget(
			document.getElementById('ifream-container-widget5'),
			'//quotespeed.morningstar.com/apis/api.jsp?instid=PRIP&module=autocomplete&&width=50',
			{
			},
			{
				onTickerChange:'widgetOnTickerChange',
				onHeightChange:'widgetOnHeightChange'
			}
		);
	})
}

/*
   isGroup:
   0=all groups
   1=financial news
   2=market & economy
   3=world news
   4=national news
   5=corporate news
   6=small business news
*/
function retrieveNewsOverview(h,w){
	QSAPI.init('PRIP',{
		env:'',
		proxyurl:'https://www.principal.com/allweb/docs/corporate/stocknewsfeed/qsapi_proxy.html',
		profileurl:'https://www.principal.com/allweb/docs/corporate/stocknewsfeed/Mstar_profile.xml',
		email:'http://www.principal.com/allweb/docs/corporate/stocknewsfeed/DEMO_00001@PRIP.com'
	},function(){
		QSAPI.createWidget(
			document.getElementById('ifream-container-widget6'),
			'http://quotespeed.morningstar.com/apis/api.jsp?instid=PRIP&module=newsOverview',
			{
				"showTitle":false,
				"maxCount":3,
				"isGroup":0,
				"fixHeight":false,
				"width":w,
				"height":h
			},
			{
				onLoad:'widgetOnLoad',
				onHeightChange:'widgetOnHeightChange'
			}
		);
	})
}

function retrieveStockChart(h,w,s,t){
	QSAPI.init('PRIP',{
		env:'',
		proxyurl:'https://www.principal.com/allweb/docs/corporate/stocknewsfeed/qsapi_proxy.html',
		profileurl:'https://www.principal.com/allweb/docs/corporate/stocknewsfeed/Mstar_profile.xml',
		email:'http://www.principal.com/allweb/docs/corporate/stocknewsfeed/DEMO_00001@PRIP.com'
	},function(){
		var symbolType = t;
		var delimiter = ":";
		var symbolString = symbolType.concat(delimiter,s);
		QSAPI.createWidget(
			document.getElementById('ifream-container-widget7'),
			'http://quotespeed.morningstar.com/apis/api.jsp?instid=PRIP&module=chart',
			{
				"symbol":symbolString,
				"toPush":1,
				"autocp":1,
				"apiVersion":2,
				"width":w,
				"fixHeight":true,
				"height":h,
				"ty":1
			},
			{
				onLoad:'widgetOnLoad',
				onHeightChange:'widgetOnHeightChange'
			}
		);
	})
}