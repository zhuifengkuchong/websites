function removeSpaces(string) {
 return string.split(' ').join('');
}

function simplePopUp(url, size, nameOfWindow) {

	var finalsize = '';
	var popUpName = '';
	var myPopup;

	if (!nameOfWindow) {		
		popUpName = 'popupWindow';	
	} else {
		popUpName = nameOfWindow;
	}

	if ((!size) || ((size != 1) && (size != 2) && (size != 3))){
		finalsize = 'width=640,height=480';
	}
	if (size == 1){
		finalsize = 'width=350,height=150';
	}
	if (size == 2){
		finalsize = 'width=575,height=425';
	}

	if (size == 3){
		finalsize = 'width=800,height=600';
	}
	if(myPopup==null || myPopup.closed)//only launch window if it doesn't already exist
		myPopup=window.open(url,popUpName,finalsize+',scrollbars=yes,toolbar=yes,resizable=yes');				
		myPopup && myPopup.focus();
}

function customizedPopUp(url,winheight,winwidth,scrollbars,toolbar,resizable,menubar,status,location,nameOfWindow,wincenter,winmodal) {
	var myPopup;
	var scrollfinal = '';
	var toolfinal = '';
	var resizefinal = '';
	var menufinal = '';
	var statusfinal = '';
	var locationfinal = '';
	var popUpName = '';

	if (!nameOfWindow || (nameOfWindow == 'Y' || nameOfWindow == 'center' || nameOfWindow == 'modal')) {		
		popUpName = 'popupWindow';	
	} else {
		popUpName = nameOfWindow;
	}
	if (scrollbars == 'Y'){
		scrollfinal = ',scrollbars=yes'
	}
	if (toolbar == 'Y'){
		toolfinal = ',toolbar=yes'
	}
	if (resizable == 'Y'){
		resizefinal = ',resizable=yes'
	}
	if (menubar == 'Y'){
		menufinal = ',menubar=yes'
	}
	if (status == 'Y'){
		statusfinal = ',status=yes'
	}
	if (location == 'Y'){
		locationfinal = ',location=yes'
	}
	//looks for 'center' among optional arguments
	if(arguments[9] == 'center' || arguments[10] == 'center'){
		var winleft = (screen.width - winwidth) / 2;
		var wintop = (screen.height - winheight) / 2;
	}
	else{
		var winleft = 50;
		var wintop = 50;
	}
	winprops = 'height='+winheight+',width='+winwidth+',left='+winleft+',top='+wintop+scrollfinal+toolfinal+locationfinal+resizefinal+menufinal+statusfinal;

	if(myPopup==null || myPopup.closed) {//only launch window if it doesn't already exist		
		popUpName = removeSpaces(popUpName);
		myPopup = window.open(url, popUpName, winprops);
		myPopup && myPopup.focus();
	}
	if(arguments[9] == 'modal' || arguments[10] == 'modal' || arguments[11] == 'modal'){
		document.onclick=function(){
			if(!(myPopup==null || myPopup.closed))
				myPopup && myPopup.focus();
		}
	}
	else{
		document.onclick='';
	}
}