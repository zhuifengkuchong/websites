 /* 
  There are multiple ways of calling this script, the most common would be in the body onload.
  	onload="setDivHeights('div1','div2')"  < < pass the IDs of two or three divs.
	Another way:
	window.onload=function(){setDivHeights('div1','div2')}
 */
function setDivHeights(id1,id2,id3){ 
	if(document.getElementById){ 
		var div1 = document.getElementById(id1); 
		var div2 = document.getElementById(id2); 
		var div3 = document.getElementById(id3); 
		if((div1 && div1.style && typeof div1.offsetHeight == 'number') && (div2 && div2.style && typeof div2.offsetHeight == 'number')){ 
			var max = div1.offsetHeight; 
			if(div2.offsetHeight > max) 
					max = div2.offsetHeight; 
			if(div3 && (div3.offsetHeight > max)) 
					max = div3.offsetHeight; 
			div1.style.height = max + "px"; 
			div2.style.height = max + "px"; 
			if(div3 && div3.style) 
					div3.style.height = max + "px"; 
		} 
		if(window.onresize) 
				window.onresize=(div3)?function(){setDivHeights(div1,div2,div3)}:function(){setDivHeights(div1,div2)}; 
	}
} 