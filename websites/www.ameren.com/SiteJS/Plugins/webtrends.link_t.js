//	Javascript File: webtrends.link_t.js
//	Tracks all <a link clicks except the following, which are already enabled in the V10 tag config:
//      JavaScript
//      Downloads
//	Configuration example:
//	plugins:{
//		link_t:{src:"../../../www.domain-test.com/index.htm"/*tpa=http://www.domain-test.com/test/webtrends.link_t.js*/}
//	}
//	Customized by Jonathan Orleck on Nov 21, 2014

(function(document){	
    function linkt_selector(dcs) {
        // Onsite links
		dcs.addSelector("a",{
		    filter: function (dcsObject, options) {
		        var el = options['element'] || {};
		        var evt = options['event'] || {};
		        // Filter out offsite, right-click, anchor, download, mailto and javascript links
				if (dcsObject._isRightClick(evt) === true) return true;
		        if (el.href === "") return false;
				if (el.hostname === "") return false;
		        if (el.hostname === document.location.hostname) return false;
				if (dcsObject.dcsIsOnsite(el.hostname, dcsObject._onsitedoms)) return false; 
				if (el.hash !== "") return false;
		        if (dcsObject.dcsTypeMatch(el.pathname, dcsObject._downloadtypes) === true) return true;
		        if (el.protocol.toLowerCase() === "javascript:") return true;
                if (el.protocol.toLowerCase() === "mailto:") return true;
		        return true;
			},
		    transform: function (dcsObject, multiTrackObject) {
		        dcsObject._autoEvtSetup(multiTrackObject);
		        var el = multiTrackObject["element"] || {};
		        var evt = multiTrackObject['event'] || {};
		        var res = dcsObject.getURIArrFromEvent(el);
		        var ttl = "";
		        var dl = "";
                // Collect title from link
		        if (el.innerHTML) {
		            ttl = el.innerHTML;
		            if (ttl.indexOf("<img") >= 0 || ttl.indexOf("<IMG") >= 0) {
		                if (el.innerHTML.toString().indexOf("<img") != 0) ttl = "Image";
		                else if (ttl.indexOf("alt=") >= 0) ttl = el.getElementsByTagName("img")[0].getAttribute("alt");
		                if (ttl === "") ttl = "Image";
		            }
		            ttl = ttl.replace(/<\/?[a-z][a-z0-9]*[^<>]*>/gi, "").replace(/[^\w\s\/\&\;\.\+\-\_\(\)]/gi, "").replace(/\s+/g, " ");
		        }
		        ttl = "Link: " + ttl;
		        dl = "1";
				multiTrackObject.argsa.push(
					"DCS.dcssip", res.dcssip,
					"DCS.dcsuri", res.dcsuri,
					"DCS.dcsqry", res.dcsqry,
					"DCS.dcsref", res.dcsref,
					"https://www.ameren.com/SiteJS/Plugins/WT.ti", ttl,
					"https://www.ameren.com/SiteJS/Plugins/WT.dl", dl,
                    "https://www.ameren.com/SiteJS/Plugins/WT.nv", dcsObject.dcsNavigation(evt, dcsObject.navigationtag)
				);
				multiTrackObject.delayTime = 100;
			},
		    finish: function (dcsObject, multiTrackObject) {
				dcsObject._autoEvtCleanup();
			}
		});

        // Offsite Links
		dcs.addSelector("a", {
		    filter: function (dcsObject, options) {
		        var el = options['element'] || {};
		        var evt = options['event'] || {};
		        // Filter out offsite, right-click, anchor, download, and javascript links
				if (dcsObject._isRightClick(evt) === true) return true;
				if (el.href === "") return true;
				if (el.hostname === "") return true;
		        if (el.hostname === document.location.hostname) return true;
		        if (dcsObject.dcsIsOnsite(el.hostname, dcsObject._onsitedoms)) return true; 
		        if (dcsObject.dcsTypeMatch(el.pathname, dcsObject._downloadtypes) === true) return true;
		        if (el.protocol.toLowerCase() === "javascript:") return true;
		        if (el.protocol.toLowerCase() === "mailto:") return true;
                return false;
		    },
		    transform: function (dcsObject, multiTrackObject) {
		        dcsObject._autoEvtSetup(multiTrackObject);
		        var el = multiTrackObject["element"] || {};
		        var evt = multiTrackObject['event'] || {};
		        var res = dcsObject.getURIArrFromEvent(el);
		        var ttl = "";
		        var dl = "";
		        // Collect title from link
		        if (el.innerHTML) {
		            ttl = el.innerHTML;
		            if (ttl.indexOf("<img") >= 0 || ttl.indexOf("<IMG") >= 0) {
		                if (el.innerHTML.toString().indexOf("<img") != 0) ttl = "Image";
		                else if (ttl.indexOf("alt=") >= 0) ttl = el.getElementsByTagName("img")[0].getAttribute("alt");
		                if (ttl === "") ttl = "Image";
		            }
		            ttl = ttl.replace(/<\/?[a-z][a-z0-9]*[^<>]*>/gi, "").replace(/[^\w\s\/\&\;\.\+\-\_\(\)]/gi, "").replace(/\s+/g, " ");
		        }
		        multiTrackObject.argsa.push(
					"DCS.dcssip", res.dcssip,
					"DCS.dcsuri", res.dcsuri,
					"DCS.dcsqry", res.dcsqry,
					"DCS.dcsref", res.dcsref,
					"https://www.ameren.com/SiteJS/Plugins/WT.ti", "Offsite: " + ttl,
					"https://www.ameren.com/SiteJS/Plugins/WT.dl", "24",
                    "https://www.ameren.com/SiteJS/Plugins/WT.nv", dcsObject.dcsNavigation(evt, dcsObject.navigationtag)
				);
		        multiTrackObject.delayTime = 100;
		    },
		    finish: function (dcsObject, multiTrackObject) {
		        dcsObject._autoEvtCleanup();
		    }
		});

        // Mailto links
		dcs.addSelector("a",{
		    filter: function (dcsObject, options) {
		        var el = options['element'] || {};
		        var evt = options['event'] || {};
		        if (el.protocol.toLowerCase() !== "mailto:") return true;
		        return false;
		    },
		    transform: function (dcsObject, multiTrackObject) {
		        dcsObject._autoEvtSetup(multiTrackObject);
		        var el = multiTrackObject["element"] || {};
		        var evt = multiTrackObject['event'] || {};
		        var ttl = "";
		        var dl = "";
		        if (el.innerHTML) {
		            ttl = el.innerHTML;
		            if (ttl.indexOf("<img") >= 0 || ttl.indexOf("<IMG") >= 0) {
		                if (el.innerHTML.toString().indexOf("<img") != 0) ttl = "Image";
		                else if (ttl.indexOf("alt=") >= 0) ttl = el.getElementsByTagName("img")[0].getAttribute("alt");
		                if (ttl === "") ttl = "Image";
		            }
		            ttl = ttl.replace(/<\/?[a-z][a-z0-9]*[^<>]*>/gi, "").replace(/[^\w\s\/\&\;\.\+\-\_\(\)]/gi, "").replace(/\s+/g, " ");
		        }
		        var dcssip = location.hostname;
		        var dcsuri = location.pathname;
		        var dcsqry = location.search;
		        var dcsref = location.href;
		        multiTrackObject.argsa.push(
					"DCS.dcssip", dcssip,
					"DCS.dcsuri", dcsuri,
					"DCS.dcsqry", dcsqry,
					"DCS.dcsref", dcsref,
					"https://www.ameren.com/SiteJS/Plugins/WT.ti", "Mailto: " + ttl,
					"https://www.ameren.com/SiteJS/Plugins/WT.dl", "26",
                    "https://www.ameren.com/SiteJS/Plugins/WT.nv", dcsObject.dcsNavigation(evt, dcsObject.navigationtag)
				);
		        multiTrackObject.delayTime = 100;
		    },
		    finish: function (dcsObject, multiTrackObject) {
		        dcsObject._autoEvtCleanup();
		    }
		});
	}
	Webtrends.registerPlugin("link_t",linkt_selector);
})(document.Webtrends);