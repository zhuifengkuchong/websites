void function() {
    var oss;
    oss = function(dcs, options) {
        return dcs.addTransform(function(dcsObject, options) {
            if (options.domEvent == undefined) {
                if (options.argsa.indexOf("https://www.ameren.com/SiteJS/Plugins/WT.dl") != -1 && options.argsa[options.argsa.indexOf("https://www.ameren.com/SiteJS/Plugins/WT.dl") + 1] != "0") return;
                if (location.search.indexOf("&page=") >= 0) return;
                var q;
                var queryParams = Webtrends.getQryParams(window.location.search.substring(1));
                q = decodeURIComponent(queryParams["q"]);
                if (document.getElementsByClassName("searchresultscounts").length > 0){
                    var resultCount = document.getElementsByClassName("searchresultscounts")[0];
                    resultCount = resultCount.childNodes[1].innerHTML;
                    resultCount = resultCount.substring(resultCount.indexOf("of ") + 3, resultCount.indexOf("documents") - 1);
                }
                if(!options.argsa) {
                    options.argsa = [];
                }
                if(q != "undefined") {
                    options.argsa.push("https://www.ameren.com/SiteJS/Plugins/WT.oss", encodeURIComponent(q));
                }
                if(resultCount) {
                    options.argsa.push("WT.oss_r", encodeURIComponent(resultCount));
                }
            }
            else {
                options.argsa.push("https://www.ameren.com/SiteJS/Plugins/WT.oss", "");
                options.argsa.push("WT.oss_r", "");
            }
        }, 'all');
    };
    Webtrends.registerPlugin('oss', function(dcs, options) { 
    return oss(dcs, options);
    });
}.call(this);
