$(document).ready(function () {

    var laRegion = $('#laregion');
    var naRegion = $('#naregion');
    var africaRegion = $('#africaregion');
    var euroRegion = $('#euregion');
    var apRegion = $('#apregion');
    var regions = [laRegion, naRegion, africaRegion, euroRegion, apRegion];

    $.each(regions, function (index) {

        $(this).click(function (e) {
            switch (regions[index]) {
                case laRegion:
                    var url = "/global-sites/latin-america";
                    $(location).attr('href', url);
                    break;
                case naRegion:
                    var url = "/global-sites/us-and-canada";
                    $(location).attr('href', url);
                    break;
                case africaRegion:
                    var url = "/global-sites/africa-and-middle-east";
                    $(location).attr('href', url);
                    break;
                case euroRegion:
                    var url = "/global-sites/europe";
                    $(location).attr('href', url);
                    break;
                case apRegion:
                    var url = "/global-sites/asia-pacific";
                    $(location).attr('href', url);
                    break;
            }
        });
    });
});