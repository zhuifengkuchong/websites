//modified for IE8 compatibility
(function () {
    var i, c, y, v, n;
    v = document.querySelectorAll(".youtube");
    if (v.length > 0) {
        var head = document.head || document.getElementsByTagName('head')[0], style = document.createElement('style');
        var css = '.youtube{background-color:#000;max-width:100%;overflow:hidden;position:relative;cursor:hand;cursor:pointer}.youtube .thumb{bottom:0;display:block;left:0;margin:auto;max-width:100%;position:absolute;right:0;top:0;width:100%;height:auto}.youtube .play{-ms-filter:progid:DXImageTransform.Microsoft.Alpha(Opacity=60);filter:alpha(opacity=60);opacity:.6;height:53px;left:50%;margin-left:-24px;margin-top:-24px;position:absolute;top:50%;width:53px;background-image:url("../../img/video-play-button-overlay.png"/*tpa=http://www.ecolab.com/assets/img/video-play-button-overlay.png*/);background-repeat:no-repeat;}.youtube .play:hover,.youtube .thumb:hover~.play{background-image:url("../../img/video-play-button-rollover-overlay.png"/*tpa=http://www.ecolab.com/assets/img/video-play-button-rollover-overlay.png*/);}.youtube';
        style.type = 'text/css';
        if (style.styleSheet) {
            style.styleSheet.cssText = css;
        } else {
            style.appendChild(document.createTextNode(css));
        }
        head.appendChild(style);
    }
    for (n = 0; n < v.length; n++) {
        y = v[n];
        i = document.createElement("img");
        i.setAttribute("src", "//i.ytimg.com/vi/" + y.id + "/hqdefault.jpg");
        i.setAttribute("class", "thumb");
        c = document.createElement("div");
        c.setAttribute("class", "play");
        y.appendChild(i);
        y.appendChild(c);
        y.onclick = function () {
            var a = document.createElement("iframe");
            a.setAttribute("src", "//www.youtube.com/embed/" + this.id + "?modestbranding=1&rel=0&showinfo=0&autoplay=1&autohide=1&border=0&wmode=opaque&enablejsapi=1");
            a.style.width = this.style.width;
            a.style.height = this.style.height;
            a.setAttribute("allowFullScreen", '');
            a.setAttribute("frameborder", "0");
            this.parentNode.replaceChild(a, this);
        }
    };
})();