Bluewolf = {
	pushBreadcrumbs: function() {
		var firstTitle = jQuery.cookie('secondTitle') || '';
		var firstUrl = jQuery.cookie('secondUrl') || '';

		var secondTitle = document.title;
		var secondUrl = document.URL;

		if(firstUrl !== secondUrl) {
			jQuery.cookie('firstTitle', firstTitle);
			jQuery.cookie('firstUrl', firstUrl);

			jQuery.cookie('secondTitle', secondTitle);
			jQuery.cookie('secondUrl', secondUrl);
		}
	}
};

Bluewolf.pushBreadcrumbs();