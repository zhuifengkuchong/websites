var current_index = 0;
var total_features = 0;
var images = null;
var IntId = 0;

function nextFeature() {
  var lastIndex = current_index;
  if (current_index+1 < total_features) {
    current_index++;
    showCurrentFeature(lastIndex);
  }
  //IntId = setInterval(rotateFeature, 8000);
}

function previousFeature() {
  var lastIndex = current_index;
  if (current_index > 0) {
    current_index--;
    showCurrentFeature(lastIndex);
  }
  //IntId = setInterval(rotateFeature, 8000);
}

function rotateFeature() {
  var lastIndex = current_index;
  if (current_index+1 < total_features) {
    current_index++;
    showCurrentFeature(lastIndex);

    if(current_index+1 == total_features) {
      current_index = -1;
    }    
  } else {
    current_index = -1;
  }
}

function showCurrentFeature(lastIndex) {
  $("#feature-navigation-previous").removeClass("inactive");
  $("#feature-navigation-next").removeClass("inactive");

  if (current_index == 0) {
    $("#feature-navigation-previous").addClass("inactive");
  }

  if (current_index == (total_features-1)) {
    $("#feature-navigation-next").addClass("inactive");
  }

  // caption
  if (lastIndex >= -1) {
    if(lastIndex == -1) {
      lastIndex = $(".feature").length-1;
    }
    $($("#feature-captions em")[lastIndex]).animate({
       opacity: '0'
     }, 500, function() {
    $($("#feature-captions em")[lastIndex]).css("display", "none");
    });
  }
  
  $($("#feature-captions em")[current_index]).css("display", "inline");
  $($("#feature-captions em")[current_index]).animate({
      opacity: '1'
    }, 500, function() {
      // Animation complete.
  });

  var new_offset = "-" + (current_index * 562).toString();
  $("#feature-container").animate({
      marginLeft: new_offset
    }, 500, function() {
      // Animation complete.
  });

  //alert("LI: " + lastIndex + ", CI: " + current_index + ", FL: " + $(".feature").length);
  //alert("ARRAY L: " + $(images[current_index]).attr('data'));
  $("#feature-backgrounds").anystretch($(images[current_index]).attr('data'), {speed: 500});
}

function cleanUpMarkup() {
  // grab the features (generated from the CMS) and re-org in the mark-up we need
  var thumbs = $('.feature_thumb');
  var titles = $('.feature h1');
  var descriptions = $('.feature p');
  var captions = $('.feature em');
  images = $('.feature .use');

  $('.feature').remove();

  var feature_index = 0;
  var largest_feature_height = 0;

  titles.each(function(){

    //alert("T: " + $(thumbs[feature_index]).html() );
    //alert("T: " + thumbs);

    // build and insert the feature description
    if($(thumbs[feature_index]).html() != "&nbsp;") {
      var feature = $('<div class="feature" style="margin-left: 0px; margin-top: 40px; margin-left: -15px;"><table border="1" cellspacing="0" cellpadding="0" width="485"><tbody><tr><td valign="top" class="feature_thumb">' + $(thumbs[feature_index]).html() + '</td><td width="10" valign="top">&nbsp;</td><td valign="top"><div style="float: left;"><table border="0" cellspacing="0" cellpadding="0"><tbody><tr><td valign="top"><h1>' + $(titles[feature_index]).html() + '</h1></td></tr><tr><td valign="top">&nbsp;</td></tr><tr><td valign="top"><p style="top: 608px; margin-top: -20px;">' + $(descriptions[feature_index]).html() + '</p></td></tr></tbody></table></div></td></tr></tbody></table></div>');
    } else {
      var feature = $('<div class="feature"><h1>' + $(titles[feature_index]).html() + '</h1><p>' + $(descriptions[feature_index]).html() + '</p></div>');
    }

    $("#feature-container").append(feature);

    if (feature.height() > largest_feature_height) {
      largest_feature_height = feature.height();
    }

    // adjust the descriptions "top" to be flow after its title
    var title_height = $(feature.children('h1')[0]).height();
    
    if($(feature.children('h1')[0]).position()) {
      var new_position = $(feature.children('h1')[0]).position().top + title_height + 10; // add padding
    }

    $(feature.children('p')[0]).css("top", new_position);
   
    // build and insert the feature image caption
    var caption = $('<em>' + $(captions[feature_index]).html() + '</em>');
    $("#feature-captions").append(caption);

    feature_index++;
  });

  $("#feature-titles-descriptions").height(largest_feature_height + 20);

  $("#feature-container").css("width", titles.length * 562 +"px");

  // now that the parent container's height is set, update all of the features' top margins
  $(".feature").each(function(){
    var new_top = (largest_feature_height - $(this).height())/2;
    $(this).css("margin-top", new_top);
  });

  if($(window).height() > 505) {
    var freeheight = $(window).height() - 505; 
    $("#features").css("margin-top", ((freeheight/3) * 0.90)-10);
    $("#well1").css("margin-top", (freeheight/2)-120+73);
    $("#featuretablerow").css("height", (freeheight * 0.80));
  }
  
  if($(window).height() > 505) {  
    $('.bkimage').height($(window).height()+500);
    $('.bkimage').width($(window).width());
  } else {
    $('.bkimage').height($(window).height());
    $('.bkimage').width($(window).width());
  }
}

function startup() {
  // set the first feature as active
  showCurrentFeature(-1);
  
  //IntId = setInterval(rotateFeature, 8000);
  startSlider();
}

function startSlider() {
    IntId = setInterval(rotateFeature, 8000);
    $("#feature-navigation-pause").css("background-color","#E62628");
    $("#feature-navigation-pause").css("opacity","1");
}

function stopSlider() {
    clearInterval(IntId);
    $("#feature-navigation-pause").css("background-color","#777777");
    $("#feature-navigation-pause").css("opacity","0.5");
}

$(document).ready(function() {
  cleanUpMarkup();
  $( window ).resize(function() {
    if($(window).height() > 505) {
      var freeheight = $(window).height() - 505; 
      $("#features").css("margin-top", ((freeheight/3) * 0.90)-10);
      $("#well1").css("margin-top", (freeheight/2)-120+73);
      $("#featuretablerow").css("height", (freeheight * 0.80));
    }

    if($(window).height() > 505) {  
      $('.bkimage').height($(window).height()+500);
      $('.bkimage').width($(window).width());
    } else {
      $('.bkimage').height($(window).height());
      $('.bkimage').width($(window).width());
    }

    //$(".anystretch").filter("img").height( $(window).height() );
    //$(".anystretch").filter("img").width($(window).width());
   
    //$('.anystretch').height( $('.anystretch')[0].scrollHeight ).filter("img");
    //$('.anystretch').width( $('.anystretch')[0].scrollWidth ).filter("img");
    
  });
  

  total_features = $(".feature").length;

  setTimeout(startup,5);

  $("#feature-navigation-previous").live("click", previousFeature);

  $('#feature-navigation-pause').toggle(
    function(){
      stopSlider();
    },
    function(){
      startSlider();
    }
  )
  
  $("#feature-navigation-next").live("click", nextFeature);
  
});