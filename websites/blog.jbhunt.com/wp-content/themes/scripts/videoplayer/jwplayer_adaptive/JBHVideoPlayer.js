// File:		JBHVideoPlayer.js
// Version:		1.1
// Author:		Luke Godfrey
// Description:	An easy-to-use wrapper that loads videos, automatically detecting internal/external users.

/*

************
Instructions
************

In order for JBHVideoPlayer to work, the following files must be included in the HTML page head.

* jwplayer.js + jwplayer.key
* jquery.js
* JBHVideoPlayer.js
* JBHVideoPlayer.css

Include the required files in the head, then call jbhVideoPlayer.loadVideo() in the body.

Basic usage is to load the video right in the page.
In the body, wherever you want the video to appear, add a script tag with the following call.

	jbhVideoPlayer.loadVideo(videoName);

You can pass an object instead of a string for more customization. The file parameter is required.
The fallback_html, modal, and modal_html parameters are optional.

	jbhVideoPlayer.loadVideo({
		file:			videoName,
		modal:			true,
		modal_html:		"<img src='Unknown_83_filename'/*tpa=http://blog.jbhunt.com/wp-content/themes/scripts/videoplayer/jwplayer_adaptive/play_video.png*/ />"
	});

You can also pass any jwplayer parameter. Some examples are shown below.
For a complete list of jwplayer parameters, see http://www.longtailvideo.com/support/jw-player/28839/embedding-the-player/

	jbhVideoPlayer.loadVideo({
		file:			videoName,
		fallback_html:	"Click here to play on Android.",
		width:			640,
		height:			480,
		autostart:		true
	});

New in version 1.1: open modal view with image

	jbhVideoPlayer.openModalView(imageName, width, height);
*/

document.domain = "http://blog.jbhunt.com/wp-content/themes/scripts/videoplayer/jwplayer_adaptive/jbhunt.com";

function JBHVideoPlayer()
{
	var self = this;
	
	this.currentVideoId		= 0;
	this.isExternal			= true;
	this.checkingExternal	= false;
	this.checkedExternal	= false;
	
	this.modalIsOpen		= false;
	this.modalDivId			= "";
	this.modalParams		= {};
	
	this.isImage 			= false;
	this.imgDivId			= "";
	
	this.videoServer		= "http://blog.jbhunt.com/wp-content/themes/scripts/videoplayer/jwplayer_adaptive/videoserver.jbhunt.com";
	
	// Generate a div, append it to the parent of the current <script> tag, and return the div id
	this.createDiv = function(modal, modal_html)
	{
		var generateInline = true;
		
		if(arguments.length == 0)
		{
			modal = true;
			modal_html = "";
			generateInline = false;
		}
		
		// Generate div id
		var divId = "JBHVideoPlayer-" + self.currentVideoId;
		self.currentVideoId++;
		
		// Generate div
		var div = document.createElement("div");
		div.setAttribute("id", divId);
		
		// Generate wrapper
		var wrapper = document.createElement("div");
		wrapper.setAttribute("id", divId + "-wrapper")
		wrapper.appendChild(div);
		jQuery(wrapper).addClass("JBHVideoPlayer-VideoWrapper");
		
		if(modal)
		{
			jQuery(wrapper)
				.addClass("JBHVideoPlayer-ModalDialogue")
				.append("<a href='javascript:void(0)' onclick='jbhVideoPlayer.closeModalView(\"" + divId + "\")' style='position: absolute; text-decoration: none; font-size: 14px; font-weight: bold; top: -20px; right: 0; color: #fff;'>X</a>");
			
			if(generateInline)
				jQuery("script:last").after("<a href='javascript:void(0);' onclick='jbhVideoPlayer.openModalView(\"" + divId + "\");'>" + modal_html + "</a>");
			
			jQuery(document.body).append(wrapper);
		}
		else if(generateInline)
			jQuery("script:last").after(wrapper);
		
		return divId;
	}
	
	// Hit the web service to determine whether the user is internal or external
	this.checkExternal = function()
	{
		self.checkingExternal = true;
		
		jQuery.getJSON('http://videoredirect.jbhunt.com/videoredirect/isinternal?callback=?', function(json) { 
			if(typeof json.internal !== "undefined") {
				self.isExternal = !json.internal;
			}
				
			if(typeof json.server !== "undefined") {
				self.videoServer = json.server;
			}
					
			self.checkedExternal = true;
		});
	}
	
	this.loadVideo = function(params)
	{
		if(typeof params == "string")
			params = { file: params };
		
		if(typeof params.fallback_html === "undefined")
			params.fallback_html = "Play this on Android";
		
		if(typeof params.modal_html === "undefined")
			params.modal_html = "Click here";
		
		if(typeof params.modal === "undefined")
			params.modal = false;
		
		if(typeof params.divId === "undefined")
			params.divId = self.createDiv(params.modal, params.modal_html);
		
		if(typeof params.width !== undefined)
			document.getElementById(params.divId + "-wrapper").style.width = params.width;
		
		
		// Wait for the external check to come through before continuing
		if(!self.checkedExternal)
		{
			if(!self.checkingExternal)
				self.checkExternal();
			
			setTimeout(function() { self.loadVideo(params) }, 100);
			return;
		}
		
		var divId = params.divId;
		var primarySource, fallbackSource;
		
		if(self.isExternal)
		{
			primarySource = "http://jbhunt.ios.internapcdn.net/jbhunt_vitalstream_com/Adaptive%20Streaming/mktg/" + params.file + ".smil.m3u8";
			fallbackSource = "http://jbhunt.ios.internapcdn.net/jbhunt_vitalstream_com/Adaptive%20Streaming/mktg/" + params.file + ".smil.m3u8";
		}
		else
		{
			primarySource = "http://" + self.videoServer + ":1935/vod/smil:" + params.file + ".smil/playlist.m3u8";
			//fallbackSource = "rtmpt://" + self.videoServer + "/vod/mp4:" + params.file + "_adaptive2.mp4";
			fallbackSource = "http://jbhunt.ios.internapcdn.net/jbhunt_vitalstream_com/Adaptive%20Streaming/mktg/" + params.file + ".smil.m3u8";
		}
		jQuery("#" + divId).html("<a href='" + fallbackSource + "' class='video-fallback'>" + params.fallback_html + "</a>");
		
		params.file		= primarySource;
		params.primary	= "flash";
		params.fallback	= false;
		
		if(!params.modal)
			jwplayer(divId).setup(params);
		
		if(params.modal)
		{
			self.modalParams[divId] = params;
			self.modalParams[divId].autostart = true;
			
			jQuery("#" + divId + "-wrapper").hide();
			
			if(params.modalAutoStart) {
				self.openModalView(divId);
			}
		}
	}
	
	this.openModalImage = function(imageURL)
	{
		self.isImage = true;
		self.openModalView(imageURL);
	}
	
	this.openModalView = function(divId)
	{		
		// Image without dimensions
		if(self.isImage && arguments.length != 3)
		{
			// Preload the image to determine dimensions
			var image = document.createElement("img");
			image.onload = function()
			{
				self.openModalView(divId, image.width, image.height);
			}
			image.src = arguments[0];
			
			return;
		}
		
		// Three arguments => open an image view
		if(arguments.length == 3)
		{
			var imageName	= arguments[0];
			var width		= arguments[1];
			var height		= arguments[2];
			var imageHTML 	= "<img src='" + imageName + "' style='width: " + width + "px; height: " + height + "px;' />";
			
			if(self.imgDivId == "")
				self.imgDivId = self.createDiv();
			
			divId = self.imgDivId;
			self.isImage = true;
		}
		
		self.modalIsOpen = true;
		self.modalDivId = divId;
		
		// Place the iframe on top, if applicable
		var isIframe = window.frameElement != null;
		if(isIframe)
		{
			window.frameElement.style.position = "relative";
			window.frameElement.style.zIndex = 3000;
		}
		
		var background = document.createElement("div");
		
		jQuery(background)
			.addClass("JBHVideoPlayer-ModalBackground")
			.fadeTo(500, 0.8, function()
			{
				jQuery("#" + divId + "-wrapper").show();
				
				if(self.isImage)
					jQuery("#" + divId).html(imageHTML);
				else
					jwplayer(divId).setup(self.modalParams[divId]);
				
				var div = document.getElementById(divId + "-wrapper");
				var minVal = 20;
				
				if(isIframe)
				{
					var scrollTop = Math.max(jQuery("html, body", top.document).scrollTop(), jQuery("body", top.document).scrollTop());
					
					div.style.top		= Math.max(minVal, ((jQuery(top).height() - div.offsetHeight) / 2 + scrollTop - window.frameElement.offsetTop)) + "px";
					div.style.left		= ((jQuery(document).width() - div.offsetWidth) / 2) + "px";
					
					jQuery(top).scroll(function()
					{
						var scrollTop = Math.max(jQuery("html, body", top.document).scrollTop(), jQuery("body", top.document).scrollTop());
						div.style.top = Math.max(minVal, ((jQuery(top).height() - div.offsetHeight) / 2 + scrollTop - window.frameElement.offsetTop)) + "px";
					});
					
					jQuery(top).resize(function()
					{
						var scrollTop = Math.max(jQuery("html, body", top.document).scrollTop(), jQuery("body", top.document).scrollTop());
						div.style.top = Math.max(minVal, ((jQuery(top).height() - div.offsetHeight) / 2 + scrollTop - window.frameElement.offsetTop)) + "px";
					});
				}
				else
				{
					div.style.top		= Math.max( ((jQuery(window).height() - div.offsetHeight) / 2), 0 ) + "px";
					div.style.left		= Math.max( ((jQuery(window).width() - div.offsetWidth) / 2), 0 ) + "px";
				}
				
				jQuery(this).bind("click", function()
				{
					self.closeModalView(divId);
				});
			});
		
		jQuery(document.body).append(background);
		
		if(isIframe)
		{
			var top_background = top.document.createElement("div");
			
			jQuery(top_background)
				.addClass("JBHVideoPlayer-ModalBackground")
				.fadeTo(500, 0.8, function()
				{
					jQuery(this).bind("click", function()
					{
						self.closeModalView(divId);
					});
				})
				.css("background",	"#000")
				.css("position",	"fixed")
				.css("top",			"0")
				.css("bottom",		"0")
				.css("left",		"0")
				.css("width",		"100%")
				.css("z-index",		2000)
				.css("filter",		"alpha(opacity=0)")
				.css("opacity",		"0");
			
			jQuery(window.frameElement, top.document).after(top_background);
		}
	}
	
	this.closeModalView = function(divId)
	{
		jwplayer(divId).stop();
		
		jQuery("#" + divId + "-wrapper").hide();
		jQuery(".JBHVideoPlayer-ModalBackground").fadeTo(500, 0, function()
		{
			self.modalIsOpen = false;
			self.modalDivId = "";
			jQuery(this).remove();
		});
		
		if(window != top)
		{
			jQuery(".JBHVideoPlayer-ModalBackground", top.document).fadeTo(500, 0, function()
			{
				jQuery(this).remove();
			});
		}
	}
}

var jbhVideoPlayer = new JBHVideoPlayer();
jbhVideoPlayer.checkExternal();

jQuery(window).resize(function()
{
	if(jbhVideoPlayer.modalIsOpen)
	{
		var div = document.getElementById(jbhVideoPlayer.modalDivId + "-wrapper");
		div.style.top		= Math.max( ((jQuery(window).height() - div.offsetHeight) / 2), 0 ) + "px";
		div.style.left		= Math.max( ((jQuery(window).width() - div.offsetWidth) / 2), 0 ) + "px";
	}
});