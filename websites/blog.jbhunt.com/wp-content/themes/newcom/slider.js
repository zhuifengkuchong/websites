var sliderResources = [
        {id:"default_mn",
         slide:0,
         content:[
            {type:"video", alt:"J.B. Hunt 360",
                image:"http://blog.jbhunt.com/wp-content/themes/newcom/sliderImages/360_banner.jpg?v=2",
                link:"//www.youtube.com/embed/yXTVrK7xsW8?hd=1&rel=0&autohide=1&showinfo=0&autoplay=1&theme=light"},
            {type:"image", alt:"J.B. Hunt Hours of Service Whitepaper",
                image:"sliderImages/hos_banner.jpg"/*tpa=http://blog.jbhunt.com/wp-content/themes/newcom/sliderImages/hos_banner.jpg*/,
                //link:"http://blog.jbhunt.com/wp-content/themes/files/pdf/660_Minutes.pdf?src=hero", cssClass:""},
				//link:"/company/newsroom/white_papers/?src=slider", cssClass:""},
				link:"/company/newsroom/white_papers/hours_of_service_and_capacity/?src=660minutes", cssClass:""},
            {type:"image", alt:"Vote J.B. Hunt as Top Logistics Provider",
                image:"sliderImages/top_3pl_banner.jpg"/*tpa=http://blog.jbhunt.com/wp-content/themes/newcom/sliderImages/top_3pl_banner.jpg*/,
                link:"/vote?src=slider", cssClass:""},
            {type:"image", alt:"Sam's Club Carrier of the Year",
                image:"sliderImages/carrier_of_year_banner.jpg"/*tpa=http://blog.jbhunt.com/wp-content/themes/newcom/sliderImages/carrier_of_year_banner.jpg*/,
                link:"https://ww2.jbhunt.com/INET/webcontent.nsf/0/CD4123F37DFD616486257E19006CFEE3/$File/2014%20Sam%27s%20Club%20Carrier%20of%20the%20Year%20press%20release.pdf", cssClass:""},
            {type:"image", alt:"J.B. Hunt Commitment to Hiring Veterans",
                //image:"sliderImages/military_banner.jpg"/*tpa=http://blog.jbhunt.com/wp-content/themes/newcom/sliderImages/military_banner.jpg*/,
				image:"sliderImages/military_banner.jpg"/*tpa=http://blog.jbhunt.com/wp-content/themes/newcom/sliderImages/military_banner.jpg*/,
                //link:"/jobs/military?src=hero", cssClass:""}
				link:"/jobs/military/?src=slider", cssClass:""},

        ]} //hiding everything below because Marketing decided to change requirements after development already started.
        ,
        {id:"freight_solutions_mn",
         slide:0,
         content:[
            {type:"image", alt:"J.B. Hunt Industry Challenges Whitepaper",
             //image:"Unknown_83_filename"/*tpa=http://blog.jbhunt.com/newcom/img/freight_solutions3.jpg*/,
			 image:"sliderImages/hos_banner_v2.jpg"/*tpa=http://blog.jbhunt.com/wp-content/themes/newcom/sliderImages/hos_banner_v2.jpg*/,
             //link:"/company/newsroom/white_papers/?src=freightsolutions", cssClass:""}
			 link:"/company/newsroom/white_papers/hours_of_service_and_capacity/?src=660minutes", cssClass:""}
            //{type:"image", image:"Unknown_83_filename"/*tpa=http://blog.jbhunt.com/wp-content/themes/newcom/2.png*/, link:"/company/newsroom/white_papers/industry_challenges", cssClass:""}
        ]},
        {id:"transportation_management_mn",
        slide:0,
         content:[
            {type:"video", alt:"J.B. Hunt 360",
                image:"sliderImages/360_banner_v2.jpg"/*tpa=http://blog.jbhunt.com/wp-content/themes/newcom/sliderImages/360_banner_v2.jpg*/,
                link:"//www.youtube.com/embed/yXTVrK7xsW8?hd=1&rel=0&autohide=1&showinfo=0&autoplay=1&theme=light"}
            //{type:"image", image:"Unknown_83_filename"/*tpa=http://blog.jbhunt.com/wp-content/themes/newcom/2.png*/, link:"/company/newsroom/presentations/earnings_presentation/?src=hero", cssClass:""}
        ]},
        {id:"carriers_mn",
         slide:0,
         content:[
            {type:"image", 
			image:"sliderImages/carrier_of_the_year_banner_v2.jpg"/*tpa=http://blog.jbhunt.com/wp-content/themes/newcom/sliderImages/carrier_of_the_year_banner_v2.jpg*/,
            link:"https://ww2.jbhunt.com/INET/webcontent.nsf/0/CD4123F37DFD616486257E19006CFEE3/$File/2014%20Sam%27s%20Club%20Carrier%20of%20the%20Year%20press%20release.pdf", cssClass:""}
            //{type:"image", image:"Unknown_83_filename"/*tpa=http://blog.jbhunt.com/wp-content/themes/newcom/2.png*/, cssClass:""}
        ]},
        {id:"investors_mn",
         slide:0,
         content:[
            {type:"image", image:"http://blog.jbhunt.com/wp-content/themes/files/jbhunt.com/investor4.jpg?rev=nocache", link:"/company/newsroom/presentations/earnings_presentation/?src=subnav", cssClass:""}
            //{type:"image", image:"Unknown_83_filename"/*tpa=http://blog.jbhunt.com/wp-content/themes/newcom/2.png*/, link:"/company/newsroom/presentations/earnings_presentation/?src=hero", cssClass:""}
        ]},
        {id:"careers_mn",
         slide:0,
         content:[
            {type:"image", alt:"J.B. Hunt Commitment to Hiring Veterans",
                //image:"sliderImages/military_banner_v2.jpg"/*tpa=http://blog.jbhunt.com/wp-content/themes/newcom/sliderImages/military_banner_v2.jpg*/,
				image:"sliderImages/military_banner_v2.jpg"/*tpa=http://blog.jbhunt.com/wp-content/themes/newcom/sliderImages/military_banner_v2.jpg*/,
                //link:"/jobs/military?src=hero", cssClass:""},
				link:"/jobs/military/?src=subnav", cssClass:""},
            
        ]}
    ];
var changeSlides=setInterval(function () {advanceSlide();},5000); //interval for slide transitions
var stopAllSlides = false; //clicking to a specific slide will stop all transitions
var slideHover = false; //hovering over a slide will temporaryarly stop all transitions
function playVid(link, wrapper){
    //alert(link + " " + wrapper);
    stopAllSlides = true;
    //window.clearInterval(changeSlides);
    jQuery(wrapper).append('<iframe width="695" height="392" src="' + link + '" frameborder="0"></iframe>');
    jQuery("#newRotator").addClass("playVid");
}
function gotoslide(container, slide, play, link){
    jQuery("iframe",".s1, .s2, .s3, .s4").remove();
    if (jQuery(container).hasClass("active") == false){
        jQuery(".slideWrapper").removeClass("active");
        jQuery(container).addClass("active");
    }
    var currentclass = jQuery(container).attr("class");
    jQuery(container).attr("class", "slideWrapper active jump slides" + jQuery(".slide", container).length);
   // $activeTab.attr("class","slideWrapper active slides" + sliderResources[i].content.length + " slide" + sliderResources[i].slide);
    jQuery(container).addClass("slide" + slide);
    stopAllSlides = true;
    if (play == true){
        playVid(link, container + " .s" + (slide + 1));
    }
}
 function advanceSlide(){
    if (!stopAllSlides && !slideHover){
    var $activeTab = jQuery(".slideWrapper.active");
    var i = $activeTab.data("tab");
    var currentSlide = sliderResources[i].slide;
    if (sliderResources[i].content.length > 1){
        if (currentSlide + 1 == sliderResources[i].content.length){
            sliderResources[i].slide = 0;
        }
        else{
            sliderResources[i].slide = currentSlide + 1;
        }
    }
    
    $activeTab.attr("class","slideWrapper active slides" + sliderResources[i].content.length + " slide" + sliderResources[i].slide);
    }
}
    jQuery(document).ready(function(){
    
    for(var i = 0; i < sliderResources.length; i++) {
        var slider = sliderResources[i];
        jQueryslider = jQuery("<div></div>").addClass("slideWrapper").addClass("slides" + slider.content.length).addClass("sideStart").data("tab", i).attr("id", slider.id + "_slides");
        var slideControls = jQuery("<ul class='slideControls'></ul>");
        for(var c = 0; c < slider.content.length; c++){
            var content = slider.content[c];
            if (content.type == "video"){
                jQueryslider.append("<div class='slide s" + (c + 1) + "'><a class='videoLink" + (c + 1) + "' href='javascript:void(0)' onclick='playVid(\"" + content.link + "\", \"#" + slider.id + "_slides .s" + (c+1) + "\");'><img src='" + content.image + "' alt='" + content.alt + "'/></a></div>");
                //slideControls.append("<li class='c" + (c + 1) + "'><a href='javascript:void(0)' onclick='gotoslide(\"#" + slider.id + "_slides\", " + c + ", \"" + content.link + "\")'>" + (c + 1) + "</a></li>");
            } else {
                jQueryslider.append("<a class='slide s" + (c + 1) + "' href='" + content.link + "'><img src='" + content.image + "' alt='" + content.alt + "'/></a>");
               }
            slideControls.append("<li class='c" + (c + 1) + "'><a href='javascript:void(0)' onclick='gotoslide(\"#" + slider.id + "_slides\", " + c + ")'>&nbsp;</a></li>");
            
        }
        if (slider.content.length > 1){
            slideControls.appendTo(jQueryslider);
        }
        jQueryslider.appendTo("#newRotator");
    }
    //jQuery("#newRotator").html(jQueryrotator.wrap("<div></div>").html());
    jQuery(".slideWrapper:eq(0)").addClass("active");
    jQuery("#newRotator").mouseover(function(){
        slideHover = true;
    });
    jQuery("#newRotator").mouseout(function(){
        slideHover = false;
    });
    jQuery("#mainnav > li, #mainnav2 > li").click(function(e) {
        if(jQuery(this).hasClass('active') == false) {
            e.preventDefault();
            e.stopPropagation();
            //if (getParameterByName("newfeature")!=""){
                //to account for the scope changes via Marketing, need to update these values with installs if the slide order changes.
                if (jQuery(this).attr('data-gotoslide')){
                    gotoslide(jQuery(this).data("container"),  jQuery(this).data("slide"))
                }
                else{
                    try{
                        var sliderID = "#" + jQuery("a:first-child", this).attr("id") + "_slides";
                        jQuery(".slideWrapper").removeClass("active");
                        jQuery(sliderID).addClass("active");
                    }
                    catch(ex){}
                }
                
            //}
            jQuery("#mainnav .active, #mainnav2 .active").children(".subnav").hide(); // hide any active menu
            jQuery("#mainnav > li, #mainnav2 > li").removeClass('active'); //remove from any other active menu items
            jQuery("#mainnav, #mainnav2").animate({marginBottom: "55px"}, 500);
            jQuery(this).children(".subnav").show();
            jQuery(this).addClass('active');
        }
    });

   
    
});