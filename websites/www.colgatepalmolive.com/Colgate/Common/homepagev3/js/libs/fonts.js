WebFontConfig = {
   google: { families: [ 'Quicksand:700:latin', 'Open+Sans:400,800italic,700italic,600italic,400italic,300italic,800,700,600,300:cyrillic-ext,greek,latin,greek-ext,latin-ext,vietnamese,cyrillic' ] }
 };
 (function() {
   var wf = document.createElement('script');
   wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
     '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
   wf.type = 'text/javascript';
   wf.async = 'true';
   var s = document.getElementsByTagName('script')[0];
   s.parentNode.insertBefore(wf, s);
 })(); 