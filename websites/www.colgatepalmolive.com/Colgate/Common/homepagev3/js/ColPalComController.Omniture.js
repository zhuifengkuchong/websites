var COL_PAL_COM_CONTROLLER = ( function ( CPCC, _omni ){
	
	CPCC.OmniPageName = ''; // reserved for _omniture.pageName
	
	/**
	 *	CPCC.setOmniPageName()
	 *	- Set OmniPageName on load
	 */
	CPCC.setOmniPageName = function () {
		log( '---->CPCC.setOmniPageName()' );
		try {
			log( '$---->Initializing CPCC.OmniPageName' );
			CPCC.OmniPageName = _omniture.pageName.split( '.' )[0];
			log( CPCC.OmniPageName );
		} catch ( error ) {
			log( '<!>CPCC.setOmniPageName Method: ' + error );
		}
	}; //-- setOmniPageName()
	
	/**
	 *	CPCC.sendCustomOmnitureEvent()
	 *	- build Omniture sendPageView data
	 *	- determine pageRef by existance of content
	 *	- call omniture sendPageViewEvent() using CPCC.Tracking_Controller()
	 */
	CPCC.sendCustomOmnitureEvent = function ( omniPageLocation, omniPageContext ) {
		log( '---->CPCC.sendCustomOmnitureEvent()' );
		try {
			var	pageRef, pushPath, pageRefTest = $( '#imageMapProducts' ).size();			
			switch ( pageRefTest ) {
				case 0: pageRef = 'equity/'; break;
				case 1: pageRef = 'brand/'; break;
				default:
					log( '<!>CPCC.sendCustomOmnitureEvent() switch passed: ' + pageRefTest + ' instead of "0" or "1". Setting to empty string.' );
					pageRefTest = '';
					break;
			}
			pushPath = CPCC.OmniPageName + '/' + pageRef + omniPageLocation + '/' + omniPageContext;
			CPCC.Tracking_Controller( sendPageViewEvent, ['', pushPath] );
		} catch ( error ) {
			log( '<!>CPCC.sendCustomOmnitureEvent() Method: ' + error );
		}
	}; //-- sendCustomOmnitureEvent()
	
	/**
	 *	CPCC.initOmnitureModule()
	 *	- Simple initialization
	 *	- Call for namesake
	 */
	CPCC.initOmnitureModule = function () {
		log( '---->CPCC.initOmnitureModule()' );
		CPCC.setOmniPageName();
	};

    return CPCC;

}( COL_PAL_COM_CONTROLLER, _omniture ) );