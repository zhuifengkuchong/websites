var COL_PAL_COM_CONTROLLER = ( function () {
	
	var CPCC = {}, // Shortened Namespace
		isIE = false,
		isIE7 = false,
		isIE8 = false,
		isIE9 = false,
		isIE7or8 = false;
		
	/**
	 *	devUsageDisplay() - PRIVATE
	 *	- override generic usage display in development
	 */
	var devUsageDisplay = function () {
		log( '%---->devUsageDisplay()' );
		try {
			if ( $( '#usage' ).size() === 0 ) return;
			var usageBlock = $( '#usage' );
			$( '#usage' ).prev( 'a' ).toggle(
				function () { usageBlock.show(); },
				function () { usageBlock.hide(); }
			);
		} catch ( error ) {
			log( '<!>COL_PAL_COM_CONTROLLER ERROR - devUsageDisplay Function: ' + error );
		}
	}; //-- devUsageDisplay()
	
    /**
     *  imgExists( @imgSrc, @baseWidth ) - PRIVATE
     *  check image width againse @baseWidth
     *  if smaller then return false
     */
    var imgExists = function ( imgSrc, baseWidth ) {
        log( '%---->imgExists( ' + imgSrc + ', ' + baseWidth + ' )' );
        try {
            var img = document.createElement( 'img' );
            img.src = imgSrc;
            img.onload = function () {
                log( 'img.width: ' + img.width );
                if ( img.width < baseWidth ) {
                    log( 'img does not exist' );
                    return false;
                } else {
                    log( 'img exists' );
                    return true;
                }
            };
        } catch ( error ) {
            log( '<!>COL_PAL_COM_CONTROLLER ERROR - imgExists Function: ' + error );
        }
    }; //-- imgExists( @imgSrc, @baseWidth )
	
	/**
	 *	imageDisabledActions() - PRIVATE
	 *	- if images are disabled display h1 text correctly
	 */
	var imageDisabledActions = function () {
		log( '%---->imageDisabledActions()' );
		try {
			var testImage = '../images/icons/clear.png'/*tpa=http://www.colgatepalmolive.com/Colgate/Common/homepagev3/images/icons/clear.png*/;
			$( 'body' ).append( '<img src="' + testImage + '" id="testImage">' );
			var testForImage = function () {
				if ( $( '#testImage' ).size() !== 0 ) {
					log( 'testImage loaded' );
					log( $( '#testImage' ).width() );
					log( document.images[0].readyState );
					setTimeout( function () {
						log( $( '#testImage' ).width() );
						if ( ( $( '#testImage' ).width() === 0 ) || ( document.images[0].readyState === 'uninitialized' ) ) {
							$( '#innerContainer h1' ).css({
								left: '15px',
								top: '10px'
							});
						}
					}, 10000 );
				} else {
					testForImage();
				}
			}();
		} catch ( error ) {
			log( '<!>COL_PAL_COM_CONTROLLER ERROR - imageDisabledActions Function: ' + error );
		}
	}; //-- imageDisabledActions()
	
	/**
	 *	scrollIfiDevice() - PRIVATE
	 *	- If iPhone/iPod/iPad
	 *	- Use scrollTo trick to hide nav bar on device
	 *	- If loaded from home screen scroll down 40px -- TODO: find a better way to do this without hardcoding pixel values.
	 */
	var scrollIfiDevice = function () {
		log( '%---->scrollIfiDevice()' );
		try {
			if ( ( navigator.userAgent.match( /iPhone/i ) ) || ( navigator.userAgent.match( /iPod/i ) ) || ( navigator.userAgent.match( /iPad/i ) ) ) {
				$( window ).load( function () {
					setTimeout( function () {
						window.scrollTo( 0, 1 );
					}, 1 );
				});
			}
			if ( window.navigator.standalone === true ) {
				$( 'body' ).css( 'marginTop','40px' );
			}
		} catch ( error ) {
			log( '<!>.scrollIfiDevice Method: ' + error );
		}
	}; //-- scrollIfiDevice()
	
	/**
	 *	setIEVars() - PRIVATE
	 *	- set all isIE vars
	 */
	var setIEVars = function () {
		log( '%---->setIEVars()' );
		try {
			var htmlTag = $( 'html' );
			if ( htmlTag.hasClass( 'ie7' ) === true ) { isIE = true; isIE7 = true; isIE7or8 = true; }
			if ( htmlTag.hasClass( 'ie8' ) === true ) { isIE = true; isIE8 = true; isIE7or8 = true; }
			if ( htmlTag.hasClass( 'ie9' ) === true ) { isIE = true; isIE9 = true; }
		} catch ( error ) {
			log( '<!>COL_PAL_COM_CONTROLLER ERROR - setIEVars Function: ' + error );
		}
	}; //-- setIEVars()
	
	/**
	 *	minifyString() - PRIVATE
	 *	- Remove SingeQuotes, DoubleQuotes, Commas, Dashes, WhiteSpace, GreaterThan and LessThan
	 */
	var minifyString = function ( stringItem ) {
		log( '---->minifyString()' );
		try {
			return stringItem.replace( '<br>','' ).replace( '<br />','' ).replace( /[! ? . ' " , > < - \s]+/g,'' ).toLowerCase();
		} catch ( error ) {
			log( '<!>minifyString Method: ' + error );
		}
	}; //-- minifyString()
	
	
	/**
	 *	killBadAnchors() - PRIVATE
	 *	- preventDefault on all href's that point to # or are null
	 */
	var killBadAnchors = function () {
		log( '%---->killBadAnchors()' );
		try {
			$( 'a[href="#"], a[href=""]' ).each( function () {
				$( this ).click( function ( event ) {
					event.preventDefault();
				});
			});
		} catch ( error ) {
			log( '<!>COL_PAL_COM_CONTROLLER ERROR - killBadAnchors Function: ' + error );
		}
	}; //-- killBadAnchors()
	
	/**
	 *	searchSelect() - PRIVATE
	 *	- select search text on focus
	 */
	var searchSelect = function () {
		log( '%---->searchSelect()' );
		try {
			if ( $( '.searchTextBox' ).size() === 0 ) return;
			log( 'initialized' );
			$( '.searchTextBox' ).focus( function () {
				$( this ).select();
			});
		} catch ( error ) {
			log( '<!>COL_PAL_COM_CONTROLLER ERROR - searchSelect Function: ' + error );
		}
	}; //-- searchSelect()
	
	/**
	 *	imagePreload( imageList, writeToDOM ) - PRIVATE
	 *	- preload images pushed via @imageList
	 *	- write images to the dome if @writeToDOM = true
	 *	- setup css so dom element is not visible but not hidden
	 */
	var imagePreload = function ( imageList, writeToDOM ) {
		log( '%---->imagePreload()' );
		try {
			var imageObj = new Image();
			if ( writeToDOM ) {
				$( '<div id="imgPreload"></div>' ).appendTo( 'body' );
				$( '#imgPreload' ).css({
					'width': '0',
					'height': '0',
					'overflow': 'hidden'
				});
			}
			imageList.each( function () {
				imageObj.src = $( this ).attr( 'src' );
				log( imageObj.src );
				if ( writeToDOM ) {
					$( '<img src="' + $( this ).attr( 'src' ) + '">' ).appendTo( $( '#imgPreload' ) );
				}
			});
		} catch ( error ) {
			log( '<!>COL_PAL_COM_CONTROLLER ERROR - imagePreload Function: ' + error );
		}
	}; //-- imagePreload()
	
	/**
	 *	hideSubDisplay() - PRIVATE
	 *	- Hides Top Navigation SubDisplay Items
	 *	- Called from TopNavigation_Controller
	 */
	var hideSubDisplay = function ( subItem ) {
		log( '%---->hideSubDisplay()' );
		subItem.parent().removeClass( 'ON' );
		TNC.SubContentDefault.show();
		TNC.SubContentItem.hide(); // hide all displayed content
		TNC.SubNavLink.find( 'a' ).removeClass( 'ON' ); // remove all ON classes
	}; //-- hideSubDisplay()
	
	/**
	 *	loadCSS() - PRIVATE
	 *	- append @filePath@fileName to page
	 */
	var loadCSS = function ( filePath, fileName ) {
		log( '%---->loadCSS()' );
		var link = document.createElement("link");
		link.rel = "stylesheet";
		link.href = filePath + fileName + '.css';
		$( 'head' ).append( link );
	}; //-- loadCSS()
	
	/*-- ########################################################################################## --*/
	/*******************************/
	/** Alert_Controller - PUBLIC **/
	/* --------------------------- */
	CPCC.Alert_Controller = {
		
		AC: '',						// setup var for shortened namespace
		InitialPause: 2000,			// time for initial show ( original 2000 )
		InteractivePause: 60000,	// time to wait for interaction before hiding ( original 10000 )
		HoverPause: 60000,			// time to wait for interaction after hovering ( original 10000 )
		
		/**
		 *	Alert_Controller.init()
		 *	- check that #Alert exists, exit if not
		 *	- set shortened namespace to AC
		 *	- Show Alert @InitialPause seconds after load
		 *	- Hide Alert @InteractivePause seconds after it is shown
		 *	- If hover, Keep Alert visible
		 *	- Once hover off, hide alert after @HoverPause seconds
		*/
		init: function () {
			log( '---->Alert_Controller.init()' );
			try {
				AC = this; // shorten namespace
				if ( $( '#Alert' ).size() === 0 ) return;
				log( 'initialized' );
				var showAlert, hideAlert, clearTimer, clearAllTimers, initialTimer, interactiveTimer, hoverTimer, argItems, aI;
				// show #Alert
				showAlert = function () {
					log( '%---->showAlert()' );
					$( '#Alert' ).fadeIn( 1000 );
				};
				// hide #Alert
				hideAlert = function () {
					log( '%---->hideAlert()' );
					$( '#Alert' ).fadeOut( 500 );
				};
				// clears all Alert timers
				clearAllTimers = function () {
					log( '%---->clearAllTimers()' );
					clearTimeout( initialTimer );
					clearTimeout( interactiveTimer );
					clearTimeout( hoverTimer );
				};
				// initial timer - showAlert() after 2000ms
				initialTimer = setTimeout( function () {
					log( '@---->initialTimer()' );
					showAlert();
				}, AC.InitialPause );
				// interactive timer - hideAlert() after 10000ms
				interactiveTimer = setTimeout( function () {
					log( '@---->interactiveTimer()' );
					hideAlert();
				}, AC.InteractivePause );
				// clear all timers on mouseover - set new timer on mouseout
				$( '#Alert' ).hover(
					function () { clearAllTimers(); }, // over
					function () { // out
						// hover timer - hideAlert() after 10000ms
						hoverTimer = setTimeout( function () {
							log( '@---->hoverTimer()' );
							hideAlert();
						}, AC.HoverPause );
					}
				);
				// hide #Alert on click of .close
				$( '#Alert' ).find( '.close' ).on( 'click', function ( event ) {
					event.preventDefault();
					hideAlert();
					clearAllTimers();
				});
			} catch ( error ) {
				log( '<!>Alert_Controller.init Method: ' + error );
			}
		} //-- Alert_Controller.init()

	}; //-- Alert_Controller()
	
	/*-- ########################################################################################## --*/
	/**********************************/
	/** Location_Controller - PUBLIC **/
	/* ------------------------------ */
	CPCC.Location_Controller = {

		LC: '', // setup var for shortened namespace

		/**
		 *	Location_Controller.init()
		 *	- check that .location exists, exit if not
		 *	- set shortened namespace to LC
		 *	- setup toggle display of #countrySelect
		*/
		init: function () {
			log( '---->Location_Controller.init()' );
			try {
				LC = this; // shorten namespace
				if ( $( '.location' ).size() === 0 ) return;
				log( 'initialized' );
				$( '.location, #bigX' ).on( 'click', function ( event ) {
					event.preventDefault();
				    $( '#countrySelect' ).toggle();
				});
			} catch ( error ) {
				log( '<!>Location_Controller.init Method: ' + error );
			}
		} //-- Location_Controller.init()

	}; //-- Location_Controller()
	
	/*-- ########################################################################################## --*/
	/***************************************/
	/** TopNavigation_Controller - PUBLIC **/
	/* ----------------------------------- */
	CPCC.TopNavigation_Controller = {

		TNC: '', // setup var for shortened namespace
		TopNavLink: $( 'ul#menu li.firstTier' ), // returns first tier li's
		DropDowns: $( '.dropdown_1column, .dropdown_2columns' ), // returns container for dropdowns
		SubNavLink: $( 'li.hasSubContent' ), // returns second tier li's
		SubContentDefault: $( '.col_2 section.parentContent' ), // returns second tier section.parentContent
		SubContentItem: $( '.col_2 section.subContent' ), // returns second tier section section.subContent
		PreservedInstance: '', // setup var for sub content preservation during hover events
		
		/**
		 *	TopNavigation_Controller.init()
		 *	- set shortened namespace to TNC
		 *	- call Utils.setUniqueID for subNav and subContent items
		 *	- call TNC.headerSplitTest()
		 *	- call TNC.firstTierEvents()
		 *	- call TNC.secondTierEvents()
		 */
		init: function () {
			log( '---->TopNavigation_Controller.init()' );
			try {
				TNC = this; // shorten namespace
				Utils.setUniqueID( $( '#menu li .dropdown_2columns .col_1' ), $( 'li.hasSubContent' ), 'subNav_', '_g_', '_i_' );
				Utils.setUniqueID( $( '#menu li .dropdown_2columns .col_2' ), $( 'section' ).not( '.parentContent' ), 'subContent_', '_g_', '_i_' );
				TNC.headerSplitTest();
				TNC.firstTierEvents();
				TNC.subDisplay();
				TNC.secondTierEvents();
			} catch ( error ) {
				log( '<!>TopNavigation_Controller.init Method: ' + error );
			}
		}, //-- TopNavigation_Controller.init()
		
		/**
		 *	TNC.headerSplitTest()
		 *	- Test if .primaryNav is wider than header images
		 *	- Add .isSplit class if true
		 */
		headerSplitTest: function () {
			log( '---->TNC.headerSplitTest()' );
			try {
				var headerWidth = $( '#globieHeader' ).find( '#innerContainer' ).outerWidth( true ),
					primNavWidth = $( '#globieHeader' ).find( '#innerContainer' ).find( '.primaryNav' ).outerWidth( true ),
					navImgWidth = 0,
					widthTest = 0;
				$( '#globieHeader' ).find( '#innerContainer' ).children().not( 'nav' ).each( function () {
					navImgWidth = navImgWidth + $( this ).outerWidth( true );
				});
				widthTest = primNavWidth + navImgWidth;
				if ( widthTest >= headerWidth ) {
					$( 'nav.primaryNav' ).addClass( 'isSplit' );
				}
			} catch ( error ) {
				log( '<!>TNC.headerSplitTest Method: ' + error );
			}
		}, //-- TNC.headerSplitTest()
		
		/**
		 *	TNC.firstTierEvents()
		 *	- Pause/Resume CTA_Block on hover on/off
		 *	- Set/Remove ON classes
		 *	- CPCC.sendCustomOmnitureEvent( based off nav item
		 */
		firstTierEvents: function () {
			log( '---->TNC.firstTierEvents()' );
			try {
				TNC.TopNavLink.hover(
					function () {
						if ( $( this ).find( 'div' ).size() !== 0 ) {
							COL_PAL_COM_CONTROLLER.CTA_Block_Controller.pauseShow( 'pause' );
							$( this ).find( 'div' ).addClass( 'ON' );
						}
					},
					function () {
						if ( $( this ).find( 'div' ).size() !== 0 ) {
							COL_PAL_COM_CONTROLLER.CTA_Block_Controller.pauseShow( 'resume' );
							$( this ).find( 'div' ).removeClass( 'ON' );
						}
					}
				);
				// omniture
				$( '#menu' ).find( 'li.firstTier' ).children().not( 'div' ).click( function ( event ) {
					CPCC.sendCustomOmnitureEvent( 'nav', minifyString( $( this ).html() ) );
				});
			} catch ( error ) {
				log( '<!>TNC.firstTierEvents Method: ' + error );
			}
		}, //-- TNC.firstTierEvents()
		
		/**
		 *	TNC.subDisplay()
		 *	- TNC.DropDowns hover events
		 *	- use click to hide in mobile
		 */
		subDisplay: function () {
			log( '---->TNC.subDisplay()' );
			try {
				TNC.DropDowns.hoverIntent({
					over: function () {
						$( this ).parent().addClass( 'ON' );
					},
					out: function () {
						hideSubDisplay( $( this ) );
					},
					timeout: 100
				});
				$( 'body' ).children().click( function ( event ) {
					TNC.DropDowns.each( function () {
						if ( $( this ).hasClass( 'ON' ) === true ) {
							log( 'hideSubDisplay getting called' );
							hideSubDisplay( $( this ) );
						}
					});
				});
			} catch ( error ) {
				log( '<!>TNC.subDisplay Method: ' + error );
			}
		}, //-- TNC.subDisplay()
		
		/**
		 *	TNC.secondTierEvents()
		 *	- Control display of navigation depending on hover state
		 *	- CPCC.sendCustomOmnitureEvent( based off subnav items
		 */
		secondTierEvents: function () {
			log( '---->TNC.secondTierEvents()' );
			try {
				var currentSubContent, previousSubContent, subLinkVal = '';
				TNC.SubNavLink.each( function ( i ) {
					$( this ).hoverIntent({
						over: function () {
							TNC.SubContentDefault.hide(); // hide default
							TNC.SubContentItem.hide(); // hide all displayed content
							TNC.SubNavLink.find( 'a' ).removeClass( 'ON' ); // remove all ON classes
							currentSubContent = $( this ).attr( 'id' ).replace( 'subNav', 'subContent' );
							$( '#' + currentSubContent ).show();
							$( this ).find( 'a' ).addClass( 'ON' );
						},
						out: function () {
							// do nothing here - hover out events handled in TNC.subDisplay()
						},
						timeout: 500
					});
				});
				// omniture
				$( 'li.hasSubContent a' ).click( function ( event ) {
					CPCC.sendCustomOmnitureEvent( 'nav/sub', minifyString( $( this ).html() ) );
				});
				$( 'ul.icons' ).find( 'li a img' ).click( function ( event ) {
					CPCC.sendCustomOmnitureEvent( 'nav/sub/ourbrands', minifyString( this.alt ) );
				});
				$( 'section.parentContent a, section.subContent a' ).click( function ( event ) {
					$( this ).parentsUntil( '#menu' ).each( function ( i ) {
						if ( $( this ).find( 'http://www.colgatepalmolive.com/Colgate/Common/homepagev3/js/ul li a.ON' ).text() !== '' ) {
							subLinkVal = $( this ).find( 'http://www.colgatepalmolive.com/Colgate/Common/homepagev3/js/ul li a.ON' ).text();
						} else {
							subLinkVal = $( this ).find( 'http://www.colgatepalmolive.com/Colgate/Common/homepagev3/js/a.drop' ).text();
						}
					});
					CPCC.sendCustomOmnitureEvent( 'nav/sub/sublink', minifyString( subLinkVal ) );
				});
				
			} catch ( error ) {
				log( '<!>TNC.secondTierEvents Method: ' + error );
			}
		} //-- TNC.secondTierEvents()
		
	}; //-- TopNavigation_Controller()
	
	/*-- ########################################################################################## --*/
	/***********************************/
	/** CTA_Block_Controller - PUBLIC **/
	/* ------------------------------- */
	CPCC.CTA_Block_Controller = {
		
		CTABC: '', // setup var for shortened namespace
		CTAB: '',	// setup var for Raphael Block
		HasBeenBuilt: false, // check if ctaBlock has been built
		Rotator: '', // setup var for rotator
		RotatorSpeed: 500, // speed of Rotator Animation
		RotatorPause: 9000, // interval between Rotator Animation
		AnimationEndDelay: 1000, // add some delay to animation timer ( allows for animation to complete as next animation begins )
		MaxFontSize: 25, // Maximum Font Size set in CTABC.initRotator.onAfterSlide()
		RotatorPrev: 0, // keep track of previous RotatorBlock
		CurrBlock: $( '#rotatorPush_0' ), // set CurrBlock var to first item
		StopLogger: 1, // flag for stopping CTABC.rotatorAnimateControlBlocks Log || Set to 0 to turn back on
		
		/**
		 *	CTA_Block_Controller.init()
		 *	- check that #CTA_Block exists, exit if not
		 *	- make sure #CTA_Block_Images is positioned below #topNav
		 *	- set shortened namespace to CTABC
		 *	- set images size for ie7/8
		 *	- initializeRotator
		 *	- set default block if undefined
		 */
		init: function () {
			log( '---->CTA_Block_Controller.init()' );
			try {
				if ( $( '#CTA_Block' ).size() === 0 ) return;
				log( 'initialized' );
				$( '#CTA_Block_Images' ).css( 'top', $( '#topNav' ).height() );
				CTABC = this; // shorten namespace
				CTABC.setIEBgImageSize(); // set bg image size if ie7/8 and less than 1280
				$( window ).load( function () {
					if ( typeof CTA_Block === 'undefined' ) {
						CTABC.buildBlock( '#ff0000', 0.8, false, false, 1, 1, '#f4282b', '#ff0000', '#fff', '#555', '#efefef' );
						CTABC.initRotator(); // initialize image rotator
					} else {
						CTABC.buildBlock( CTA_Block.LocalBlockStyle.fillColor, CTA_Block.LocalBlockStyle.fillOpacity, CTA_Block.LocalBlockStyle.useImage, CTA_Block.LocalBlockStyle.useShadow, CTA_Block.LocalBlockStyle.scaleX, CTA_Block.LocalBlockStyle.scaleY, CTA_Block.LocalBlockStyle.blockAnimatorColor, CTA_Block.LocalBlockStyle.arrowColor, CTA_Block.LocalBlockStyle.statementColor, CTA_Block.LocalBlockStyle.statementLinkColor, CTA_Block.LocalBlockStyle.statementBackgroundColor );
						CTABC.MaxFontSize = CTA_Block.LocalBlockStyle.maxFontSize;
						CTABC.initRotator(); // initialize image rotator
					}
				});
			} catch ( error ) {
				log( '<!>CTA_Block_Controller.init Method: ' + error );
			}
		}, //-- CTA_Block_Controller.init()
		
		/**
		 *	CTA_Block_Controller.setIEBgImageSize()
		 *	- if IE7 or IE8 and browser width <= 1280
		 *	- loop through #CTA_Image_Container images and replace with small versions
		 *	- this is a workaround for css3 display zoom
		 */
		setIEBgImageSize: function () {
			log( '---->CTA_Block_Controller.setIEBgImageSize()' );
			try {
				if ( isIE7or8 ) {
					var bWidth = Utils.getBrowserDims( 'width' );
					if ( bWidth <= 1280 ) {
						$( '#CTA_Image_Container' ).find( 'li' ).each( function () {
						    var liImage = $( this ).css( 'backgroundImage' ).replace( '.jpg', 'Unknown_83_filename'/*tpa=http://www.colgatepalmolive.com/Colgate/Common/homepagev3/js/-small.jpg*/ ),
						        testImage = liImage.replace( 'url("', '' ).replace( '")', '');
						    if ( imgExists( testImage, 800 ) ) {
						        $( this ).css( 'backgroundImage', liImage );
						    }
						});
					}
				}
			} catch ( error ) {
				log( '<!>CTA_Block_Controller.setIEBgImageSize Method: ' + error );
			}
		}, //-- CTA_Block_Controller.setIEBgImageSize()
		
		/**
		 *	CTABC.buildBlock()
		 *	- Build CTA_Block using Raphael
		 *	- Add color to all other text attributes passed via @param
		 */
		buildBlock: function ( fillColor, fillOpacity, useImage, useShadow, scaleX, scaleY, blockAnimatorColor, arrowColor, statementColor, statementLinkColor, statementBackgroundColor ) {
			log( '---->CTABC.buildBlock()' );
			try {
				var rNav;
				if ( CTABC.HasBeenBuilt === false ) {
				    $( '#CTA_Block_Container' ).show();
					CTAB = Raphael( 'CTA_Block' );
					rNav = $( 'ul#rotatorNav' );
					log( 'Building ctaBlock using:\nfillColor: ' + fillColor + '\nfillOpacity: ' + fillOpacity + '\nuseImage: ' + useImage + '\nuseShadow: ' + useShadow + '\nscaleX: ' + scaleX + '\nscaleY: ' + scaleY );
					CTAB.ctaBlock( fillColor, fillOpacity, useImage, useShadow, scaleX, scaleY );
					rNav.find( 'li' ).hover(
						function() { $( this ).css( 'backgroundColor', blockAnimatorColor ); },
						function () { $( this ).css( 'backgroundColor', 'transparent' ); }
					);
					rNav.find( 'li .blockAnimator' ).css( 'backgroundColor', blockAnimatorColor );
					$( '.CTA_Blockquote' ).css( 'color', statementColor );
					$( '.statement' ).css( 'color', statementColor );
					$( '.statementLink, .statementLink a' ).css({
						'color': statementLinkColor,
						'backgroundColor': statementBackgroundColor
					});
					$( '.rotatorArrow' ).css( 'color', arrowColor );
					CTABC.HasBeenBuilt = true;
				} else {
					CTAB.clear(); // Clear initial CTAB if it exits
					CTAB = Raphael( 'CTA_Block' );
					log( 'Building ctaBlock using:\nfillColor: ' + fillColor + '\nfillOpacity: ' + fillOpacity + '\nuseImage: ' + useImage + '\nuseShadow: ' + useShadow + '\nscaleX: ' + scaleX + '\nscaleY: ' + scaleY );
					CTAB.ctaBlock( fillColor, fillOpacity, useImage, useShadow, scaleX, scaleY );
					CTABC.HasBeenBuilt = true;
				}
			} catch ( error ) {
				log( '<!>CTABC.buildBlock Method: ' + error );
			}
		}, //-- CTABC.buildBlock()
		
		/**
		 *	CTABC.initRotator()
		 *	- Set unique ID's to .statementBox and #rotatorNav li's
		 *	- Set CTABC.Rotator object to rotator, initialize bxSlider()
		 *	- bxSlider Method onBeforeSlide - remove ON class from RotatorPrev
				fadeOut RotatorPrev rotatorText
		 *	- bxSlider Method onAfterSlide - remove leading zeros from currentSlideNumber
				add ON class
				fadeIn current item rotatorText
				update RotatorPrev to current item
				send current item to CTABC.rotatorAnimateControlBlocks
		 *	- Call CTABC.rotatorManualControls()
		 */
		initRotator: function () {
			log( '---->CTABC.initRotator()' );
			try {
				var currSlideSet = 0, rxCurrSlide = 0, blockMid, quoteMidPre, useableFontSize, quoteMidPost, useablePoint;
				Utils.setUniqueID( $( '.statementBox' ), 'li', 'rotatorText_', '', '', true );
				Utils.setUniqueID( $( '#rotatorNav' ), 'li', 'rotatorPush_', '', '', true );
				$( 'li[id^="rotatorText_"]' ).not( ':first' ).hide();
				CTABC.Rotator = $( '#CTA_Image_Container' ).bxSlider({
					mode: 'fade',
					auto: true,
					controls: false,
					speed: CTABC.RotatorSpeed,
					easing: 'easeOutCubic',
					pause: CTABC.RotatorPause,
					onBeforeSlide: function ( currentSlideNumber, totalSlideQty, currentSlideHtmlObject ) {
						$( '#rotatorText_' + CTABC.RotatorPrev ).fadeOut( 'fast' );
						$( '#rotatorPush_' + CTABC.RotatorPrev ).find( '.blockAnimator' ).removeClass( 'ON' );
					},
					onAfterSlide: function( currentSlideNumber, totalSlideQty, currentSlideHtmlObject ) {
						currSlideSet = currentSlideNumber.toString();
						rxCurrSlide = currSlideSet.replace( /^0+/, '' ) * 1;
						$( '#rotatorText_' + rxCurrSlide ).find( 'blockquote' ).css( 'fontSize', CTABC.MaxFontSize );
						CTABC.RotatorPrev = rxCurrSlide;
						$( '#rotatorText_' + rxCurrSlide ).fadeIn( 'fast' );
						$( '#rotatorPush_' + rxCurrSlide ).find( '.blockAnimator' ).addClass( 'ON' );
						CTABC.rotatorAnimateControlBlocks( $( '#rotatorPush_' + rxCurrSlide ) );
					}
				});
				CTABC.rotatorManualControls();
			} catch ( error ) {
				log( '<!>CTABC.initRotator Method: ' + error );
			}
		}, //-- CTABC.initRotator()
		
		/**
		 *	CTABC.rotatorManualControls()
		 *	- Listen for click event on all items with class .rotator
		 *	- Grab id of item and apply necessary bxSlider method to control slide display
		 *	- CPCC.sendCustomOmnitureEvent( based off click item
		 */
		rotatorManualControls: function () {
			log( '---->CTABC.rotatorManualControls()' );
			try {
				$( '#rotatorNav .rotator, .rotatorArrow.rotator' ).dblclick( function ( event ) {
					return;
				});
				$( '#rotatorNav .rotator, .rotatorArrow.rotator' ).click( function ( event ) {
					event.preventDefault();
					var rotatorPushItem = $( this ).attr( 'id' ).replace( 'rotatorPush_', '' ).toString();
					switch( rotatorPushItem ) {
						case 'Left':
							CTABC.Rotator.goToPreviousSlide();
							CTABC.Rotator.stopShow();
							CTABC.Rotator.startShow();
							CPCC.sendCustomOmnitureEvent( 'CTA','Prev' );
							break;
						case 'Right':
							CTABC.Rotator.goToNextSlide();
							CTABC.Rotator.stopShow();
							CTABC.Rotator.startShow();
							CPCC.sendCustomOmnitureEvent( 'CTA','Next' );
							break;
						default: log( '<!>CTABC.rotatorManualControls() switch passed: ' + rotatorPushItem + ' instead of "Left" or "Right".' );
					}
					if ( ( rotatorPushItem !== 'Right' ) && ( rotatorPushItem !== 'Left' ) ) {
						CTABC.Rotator.goToSlide( rotatorPushItem );
						CTABC.Rotator.stopShow();
						CTABC.Rotator.startShow();
						CPCC.sendCustomOmnitureEvent( 'CTA','Box' + ( parseInt( rotatorPushItem, 10 ) + 1 ) );
					}
					$( '#rotatorPush_' + CTABC.RotatorPrev ).find( '.blockAnimator' ).stop( true, true );
				});
			} catch ( error ) {
				log( '<!>CTABC.rotatorManualControls Method: ' + error );
			}
		}, //-- CTABC.rotatorManualControls()
		
		/**
		 *	CTABC.rotatorAnimateControlBlocks()
		 *	- Ensure current item has top position of 0
		 *	- animate top position to -CTABC.CurrBlock.height using CTABC.RotatorPause + CTABC.AnimationEndDelay as timer
		 */
		rotatorAnimateControlBlocks: function ( item ) {
			if ( CTABC.StopLogger === 0 ) { log( '---->CTABC.rotatorAnimateControlBlocks()' ); }
			try {
				CTABC.CurrBlock = $( item ).find( '.blockAnimator' );
				$( item ).find( '.blockAnimator' ).css( 'top', 0 );
				CTABC.CurrBlock.animate({
					top: CTABC.CurrBlock.height() * -1
				}, CTABC.RotatorPause + CTABC.AnimationEndDelay );
			} catch ( error ) {
				log( '<!>CTABC.rotatorAnimateControlBlocks Method: ' + error );
			}
		}, //-- CTABC.rotatorAnimateControlBlocks()
		
		/**
		 *	CTABC.pauseShow()
		 *	- Setup Pause and Resume Calls
		 *	- Call Via:
		 *	COL_PAL_COM_CONTROLLER.CTA_Block_Controller.pauseShow( 'pause' );
		 *	COL_PAL_COM_CONTROLLER.CTA_Block_Controller.pauseShow( 'resume' );
		 */
		pauseShow: function ( playType ) {
			log( '---->CTABC.pauseShow()' );
			try {
				switch ( playType ) {
					case 'pause':
						log( 'PAUSE' );
						COL_PAL_COM_CONTROLLER.CTA_Block_Controller.Rotator.stopShow();
						COL_PAL_COM_CONTROLLER.CTA_Block_Controller.CurrBlock.pause();
						break;
					case 'resume':
						log( 'RESUME' );
						COL_PAL_COM_CONTROLLER.CTA_Block_Controller.Rotator.startShow();
						COL_PAL_COM_CONTROLLER.CTA_Block_Controller.CurrBlock.resume();
						break;
					default: log( '<!>CTABC.pauseShow() switch passed: ' + playType + ' instead of "pause" or "resume".' );
				}
			} catch ( error ) {
				log( '<!>CTABC.pauseShow Method: ' + error );
			}
		} //-- CTABC.pauseShow()
		
	}; //-- CTA_Block_Controller()
		
	/*************************/
	/** Raphael.fn.ctaBlock **/
	/* --------------------- */
	// Builds CTA Block Polygon
	Raphael.fn.ctaBlock = function ( fillColor, fillOpacity, useImage, useShadow, scaleX, scaleY ) {
		log( '%---->ctaBlock()' );
		try {
			fillColor		=	fillColor		||	'#fff';
			fillOpacity		=	fillOpacity		||	1;
			useImage		=	useImage		||	false;
			// var pathString = 'M22,15h421,q7,0 7,7l-60,177,q-3,6 -14,5h-354q-7,0 -7,-7v-185q0,-7 7,-7Z';
			var pathString = 'M22,15h421,q7,0 7,7l-60,162,q-3,6 -14,5h-354q-7,0 -7,-7v-160q0,-7 7,-7Z';
			if ( useShadow === true ) {
				this.set (
					this.path( pathString ).attr({
						fill: fillColor,
						'stroke-width': 0,
						opacity: fillOpacity
					})
					.scale( scaleX, scaleY, 0, 0 )
					// call glow three times for quality shadow effect
					.glow({ width: 4, fill: true, opacity: 0.1, color: '#333' })
					.glow({ width: 7, fill: true, opacity: 0.1, color: '#666' })
					.glow({ width: 10, fill: true, opacity: 0.1, color: '#999' })
				);
			} else {
				this.set (
					this.path( pathString ).attr({
						fill: fillColor,
						'stroke-width': 0,
						opacity: fillOpacity
					})
					.scale( scaleX, scaleY, 0, 0 )
				);
			}
			// use background image if supplied
			if ( useImage !== false ) {
				this.image( useImage, 15, 15, 435, 174 ).attr({opacity:0.4});
			}
			return this;
		} catch ( error ) {
			log( '<!>Raphael.fn.ctaBlock Method: ' + error );
		}
	}; //-- Raphael.fn.ctaBlock()
	
	/*-- ########################################################################################## --*/
	/*********************************/
	/** Product_Selector_Controller **/
	/* ----------------------------- */
	CPCC.Product_Selector_Controller = {
		
		PSC: '', // setup var for shortened namespace
		
		/**
		 *	Product_Selector_Controller.init()
		 *	- check that #productBackground exists, exit if not
		 *	- set shortened namespace to PSC
		 *	- set unique ID's on area within #imageMapProducts
		 *	- set unique ID's on li's within #productNav - NOT .productMain ( first item )
		 *	- preload images for .ie7 - fixes flicker
		 *	- Initialize PSC.pushProductImage() and PSC.pushProductLink()
		 */
		init: function () {
			log( '---->Product_Selector_Controller.init()' );
			try {
				if ( $( '#productBackground' ).size() === 0 ) return;
				log( 'initialized' );
				PSC = this; // shorten namespace
				Utils.setUniqueID( $( '#imageMapProducts' ), 'area', 'productMap_', '', '', true );
				Utils.setUniqueID( $( '#productNav' ), $( 'li' ).not( '.productMain' ), 'productLink_', '', '', true );
				if ( isIE7 ) {
					imagePreload( $( '.productDisplayContainer img ' ), true );
				}
				PSC.pushProductImage();
				PSC.pushProductLink();
				PSC.clickWatch();
			} catch ( error ) {
				log( '<!>Product_Selector_Controller.init Method: ' + error );
			}
		}, //-- Product_Selector_Controller.init()
		
		/**
		 *	PSC.pushProductImage()
		 *	- send hover item ID along with view type to PSC.productDisplay()
		 */
		pushProductImage: function () {
			log( '---->PSC.pushProductImage()' );
			try {
				$( 'area[id^="productMap_"]' ).hoverIntent({
					over: function () { PSC.productDisplay( $( this ).attr( 'id' ).replace( 'productMap_', '' ), 'show' ); },
					out: function () { PSC.productDisplay( $( this ).attr( 'id' ).replace( 'productMap_', '' ), 'hide' ); }
				});
			} catch ( error ) {
				log( '<!>PSC.pushProductImage Method: ' + error );
			}
		}, //-- PSC.pushProductImage()
		
		/**
		 *	PSC.pushProductLink()
		 *	- send hover item ID along with view type to PSC.productDisplay()
		 */
		pushProductLink: function () {
			log( '---->PSC.pushProductLink()' );
			try {
				$( 'li[id^="productLink_"]' ).hover(
					function () { PSC.productDisplay( $( this ).attr( 'id' ).replace( 'productLink_', '' ), 'show' ); },
					function () { PSC.productDisplay( $( this ).attr( 'id' ).replace( 'productLink_', '' ), 'hide' ); }
				);
			} catch ( error ) {
				log( '<!>PSC.pushProductLink Method: ' + error );
			}
		}, //-- PSC.pushProductImage()
		
		/**
		 *	PSC.productDisplay()
		 *	- show/hide product images
		 */
		productDisplay: function ( productID, view ) {
			log( '---->PSC.productDisplay()' );
			try {
				var productMain = $( '.productMain' ),
					productLink = $( '#productLink_' + productID ).find( 'a' ),
					productDisplay = $( '#productLink_' + productID ).find( '.productDisplayContainer' );
				switch ( view ) {
					case 'show':
						productMain.hide();
						productLink.show().addClass( 'ON' );
						productDisplay.addClass( 'ON' );
						break;
					case 'hide':
						productMain.show();
						productLink.removeClass( 'ON' );
						productDisplay.removeClass( 'ON' );
						break;
					default: log( '<!>PSC.productDisplay() switch expecting "show" or "hide", instead passed: ' + view );
				}
			} catch ( error ) {
				log( '<!>PSC.productDisplay Method: ' + error );
			}
		}, //-- PSC.productDisplay()
		
		/**
		 *	PSC.clickWatch()
		 *	- Watch for click event on <area> and <li> items
		 *	- Send <area>.Title and <li>.html data to CPCC.sendCustomOmnitureEvent()
		 */
		clickWatch: function ( productID, view ) {
			log( '---->PSC.clickWatch()' );
			try {
			    var omniPush;
			    $( 'area[id^="productMap_"]' ).not( 'li[id^="productLink_"] a' ).click( function ( e ) {
			        log( 'clickWatch area clicked' );
			        omniPush = $( this ).attr( 'title' );
			        log( 'omniPush: ' + omniPush );
			        CPCC.sendCustomOmnitureEvent( 'Category', minifyString( omniPush ) );
			    });
			    $( 'li[id^="productLink_"] a' ).click( function ( e ) {
			        log( 'clickWatch a clicked' );
			        omniPush = $( this ).html();
			        log( 'omniPush: ' + omniPush );
			        CPCC.sendCustomOmnitureEvent( 'Category', minifyString( omniPush ) );
			    });
			} catch ( error ) {
				log( '<!>PSC.clickWatch Method: ' + error );
			}
		} //-- PSC.clickWatch()
		
	}; //-- Product_Selector_Controller()
	
	/*-- ########################################################################################## --*/
	/****************************/
	/** Pod_Rotator_Controller **/
	/* ------------------------ */
	CPCC.Pod_Rotator_Controller = {
		
		PRC: '', // setup var for shortened namespace
		PodRotator: '', // setup var for product rotator
		
		/**
		 *	Pod_Rotator_Controller.init()
		 *	- check that #equityRotatorPod exists, exit if not
		 *	- set shortened namespace to PSC
		 *	- initialize rotation PRC.initPodRotation()
		 *	- CPCC.sendCustomOmnitureEvent( for clicked items
		 */
		init: function () {
			log( '---->Pod_Rotator_Controller.init()' );
			try {
				if ( $( '#equityRotatorPod' ).size() === 0 ) return;
				log( 'initialized' );
				PRC = this; // shorten namespace
				PRC.initPodRotation();
				$( '#equityRotatorPod' ).find( 'li' ).click( function () {
					CPCC.sendCustomOmnitureEvent( 'rotator', minifyString( $( this ).find( 'h5 a' ).text() ) );
				});
			} catch ( error ) {
				log( '<!>Pod_Rotator_Controller.init Method: ' + error );
			}
		}, //-- Product_Rotator_Controller.init()
		
		/**
		 *	PRC.initPodRotation()
		 *	- Check list size of ul#podRotator
		 *	- If size = 1 then hide arrows, otherwise
		 *	- Set PRC.PodRotator object to rotator, initialize bxSlider()
		 *	- Init manual controls - PRC.initPodControls()
		 */
		initPodRotation: function () {
			log( '---->PRC.initPodRotation()' );
			try {
				if ( $( 'ul#podRotator li' ).size() === 1 ) {
					$( '#equityRotatorPod .slideArrow' ).hide();
				} else {
					PRC.PodRotator = $( '#equityRotatorPod ul' ).bxSlider({
						auto: false,
						controls: false,
						speed: 1000,
						easing: 'easeInOutCirc'
					});
					PRC.initPodControls();
				}
			} catch ( error ) {
				log( '<!>PRC.initPodRotation Method: ' + error );
			}
		}, //-- PRC.initPodRotation()
		
		/**
		 *	PRC.initPodControls()
		 *	- setup Left / Right Controls for pod
		 *	- CPCC.sendCustomOmnitureEvent( for clicked arrows
		 */
		initPodControls: function () {
			log( '---->PRC.initPodControls()' );
			try {
				$( '#equityRotatorPod .slideArrow.rotator' ).click( function ( event ) {
					event.preventDefault();
					var rotatorPushItem = $( this ).attr( 'id' ).replace( 'slidePush_', '' );
					switch( rotatorPushItem ) {
						case 'Left':
							PRC.PodRotator.goToPreviousSlide();
							CPCC.sendCustomOmnitureEvent( 'rotator', 'Prev' );
							break;
						case 'Right':
							PRC.PodRotator.goToNextSlide();
							CPCC.sendCustomOmnitureEvent( 'rotator', 'Next' );
							break;
						default: log( '<!>PRC.initPodControls() switch passed: ' + rotatorPushItem + ' instead of "Left" or "Right".' );
					}
				});
			} catch ( error ) {
				log( '<!>PRC.initPodControls Method: ' + error );
			}
		} //-- PRC.initPodControls()
		
	}; //-- Pod_Rotator_Controller()
	
	/*-- ########################################################################################## --*/
	/***********************************************/
	/** CPCC.Independent_Item_Omniture_Tracking() **/
	/* ------------------------------------------- */
	// Use this area to identify any tracking of items not controlled elsewhere
	CPCC.Independent_Item_Tracking = function () {
		log( '---->Independent_Item_Tracking()' );
		try {
			// Equity Products Pod
			$( '#equityProductsPod' ).find( 'a' ).click( function () {
				CPCC.sendCustomOmnitureEvent( 'productPod', 'viewalloralcareproducts' );
			});
			// Equity Callout
			$( '#equityCallout' ).find( 'a' ).click( function () {
				CPCC.sendCustomOmnitureEvent( 'callout', 'resourcecenter' );
			});
		} catch ( error ) {
			log( '<!>CPCC.Independent_Item_Omniture_Tracking Method: ' + error );
		}
	}; //-- Independent_Item_Omniture_Tracking()
	
	/*-- ########################################################################################## --*/
	/*******************************/
	/** CPCC.TrackingController() **/
	/* --------------------------- */
	//	@callback being the method called for tracking
	//	@pushParams will be passed to @callback - Must be an Array
	//	Result: @callback( [@pushParams] ) = @callback( @pushParams[0], @pushParas[1], ... );
	CPCC.Tracking_Controller = function ( callback, pushParams ) {
		log( '---->Tracking_Controller()' );
		log( callback );
		log( pushParams );
		try {
			if ( arguments.length !== 2 ) {
				throw 'ERROR: 2 arguments required. @callback and @pushParams. Supplied ' + arguments.length;
			}
			callback.apply( this, pushParams );
		} catch ( error ) {
			log( '<!>Tracking_Controller Method: ' + error );
		}
	}; //-- Tracking_Controller()
	
	/*-- ########################################################################################## --*/
	/*-- ########################################################################################## --*/
	
	/**
	 *	CPCC.initialize()
	 *	- call self on load
	 *	- run initialization methods
	 *	- Called externally for proper augmentation
	 */
	CPCC.initialize = function () {
		log( '---->CPCC.initialize()' );
		try {
			devUsageDisplay();
			// imageDisabledActions();
			scrollIfiDevice();
			setIEVars();
			killBadAnchors();
			searchSelect();
			CPCC.Alert_Controller.init();
			CPCC.Location_Controller.init();
			CPCC.TopNavigation_Controller.init();
			CPCC.CTA_Block_Controller.init();
			CPCC.Product_Selector_Controller.init();
			CPCC.Pod_Rotator_Controller.init();
			CPCC.Independent_Item_Tracking();
		} catch ( error ) {
			log( '<!>CPCC.initialize Method: ' + error );
		}
	}; //-- CPCC.initialize();
	
	return CPCC;
	
}());