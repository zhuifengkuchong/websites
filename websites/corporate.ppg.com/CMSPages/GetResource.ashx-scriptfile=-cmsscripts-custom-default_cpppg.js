// **** WARNING **** //
// THIS FILE IS BEING SHARED BY SEVERAL SITES IF YOU NEED TO MODIFY IT PLEASE CREATE A SEPARATE FILE //

// BILLBOARD FUNCTIONS

function activateBillboard() {
  jQuery(".billboard .inactive").fadeOut(5000);
  return false;
}

// MENU FUNCTIONS

function toggleMenu() {
  if (jQuery("nav").css("display") == "none") {
    jQuery("nav").slideDown();
  } else {
    jQuery("nav").slideUp();
  }
  return false; 
}

// SEARCH FORM FUNCTIONS

function formrule_on() {
  if (this.defaultValue == this.value) this.value = "";
}

function formrule_off() {
  if (this.value == "") {
    this.value = this.defaultValue;
  }
}

// DOCUMENT READY

jQuery(document).ready(function(){  

  jQuery("#menu-button").on("click",toggleMenu);

  jQuery("header input[type='text']").on("focus",formrule_on);
  jQuery("header input[type='text']").on("blur",formrule_off);

  jQuery("#billboard.revolving div:nth-child(" + (Math.floor(Math.random() * jQuery("#billboard.revolving").children().length) + 1) + ")").show();
  billboardAnimate = setTimeout(activateBillboard,300);
  
  jQuery.getJSON("https://finance.google.com/finance/info?client=ig&q=NYSE:PPG&callback=?",function(response){
    var stockInfo = response[0];
    jQuery(".stockDate").html(stockInfo.lt + ":");
    jQuery(".stockPrice").html("$" + stockInfo.l);
    jQuery(".stockStatus").html(stockInfo.c);
  });


});