/* Create a name space for GLOBALP JavaScript so that we don't step on any third
 * party libraries. */

var GLOBALP = {
	terminals : {},
	form : {},
	event : {},
	utils : {}
};

// Generic functions for hiding and showing an element's hint.

GLOBALP.form.hideHint = function (element, hint)
{
	if (element.value == hint)
	{
		
		element.value = "";
		
		element.className = "";
		
	}
};

GLOBALP.form.showHint = function (element, hint)
{
	if (element.value == "")
	{
		
		element.value = hint;
		
		element.className = "hint";
		
	}
};

GLOBALP.form.validateRadiusSearch = function (form)
{
	
	if (form.zipCode.value.length == 0 || form.zipCode.value == "zip")
	{
		
		alert("Please provide a ZIP code.");
		
		return false;
		
	}
	
	return true;
	
};

GLOBALP.form.validateStateSearch = function (form)
{
	
	var option = form.state.options[form.state.selectedIndex];
	
	if (option.value.length == 0)
	{
		
		alert("Please select a state.");
		
		return false;
		
	}
	
	return true;
	
};

GLOBALP.event.addListener = function(element, eventName, listener)
{
	
	if (element.attachEvent)
	{
		
		// Use Internet Explorer method.
		
		element.attachEvent("on" + eventName, listener);
		
	}
	else if (element.addEventListener)
	{
		
		// Use newer DOM method supported by Firefox.
		
		element.addEventListener(eventName, listener, true);
		
	}
	else
	{
		
		// Use DOM 0 method.
		
		element["on" + eventName] = listener;
		
	}
	
};


// Returns the computed style for the element.

// NOTE: Safari contains a bug which causes it to return null if the element (or
// any parent element) is hidden. We do not attempt to normalize this because
// the caller may be checking for the display property.

GLOBALP.utils.getActiveStyle = function (obj)
{
  	
	// Ensure that we have an object and not just the ID of an HTML element.
	
	obj = GLOBALP.utils.getObject(obj);
	
	if (obj.currentStyle)
	{
		// Internet Explorer
		return obj.currentStyle;
	}
	else if (document.defaultView && document.defaultView.getComputedStyle)
	{
		// Mozilla/FireFox/Safari
		return document.defaultView.getComputedStyle(obj, null);	
	}
	else
	{
		// Fall back to non-computed style.	
		return obj.style;
	}
  	
};

GLOBALP.utils.getActiveStyleProperty = function (obj, property)
{
	
	// Ensure that we have an object and not just the ID of an HTML element.
	
	obj = GLOBALP.utils.getObject(obj);
	
	// Get the computed style for the object.
	
	var objStyle = GLOBALP.utils.getActiveStyle(obj);
	
	// Safari bug. See above.
	
	if (objStyle == null)
	{
		
		// If the user is looking for the display property, we'll assume that
		// the element is hidden, though technically it could be a parent
		// element that's hidden.
		
		if (property == "display")
		{
			return "none";
		}
		
		// Try to display the element. This won't work if the element is hidden
		// by a parent element.
		
		var oldDisplay = obj.style.display;
		
		obj.style.display = "block";
		
		objStyle = GLOBALP.utils.getActiveStyle(obj);
		
		obj.style.display = oldDisplay;
		
		// If it's still null, use the non-computed style.
		
		if (objStyle == null)
		{
			return obj.style[property];	
		}
		
		return objStyle[property];
		
	}
	else
	{
		
		return objStyle[property];
		
	}
		
};

// Takes 1 argument which is either the ID or object reference of elements whose
// style display mode will be toggled.

GLOBALP.utils.getObject = function (param)
{
	if (typeof(param) == "object")
	{
		return param;
	}
	else
	{
		return document.getElementById(param);
	}
};

GLOBALP.utils.toggleDisplay = function ()
{
	for ( var i = 0; i < arguments.length; i++ )
	{
		
		var obj = GLOBALP.utils.getObject(arguments[i]);
		
		if (obj == null)
		{
			GLOBALP.utils.debug("Could not find object with ID " + arguments[i] + " in the page.");
		}
		
		// Since all elements are hidden with a value of "none", whereas each
		// element may have its own display value, we'll check for "none".
		
		var display = GLOBALP.utils.getActiveStyleProperty(obj, "display");
		
		if (display == "none")
		{
			GLOBALP.utils.show(obj);
		}
		else
		{
			GLOBALP.utils.hide(obj);
		}
	
	}
};

GLOBALP.utils.show = function (obj)
{
	
	obj.style.display = GLOBALP.utils.getDefaultDisplay(obj);

	obj.style.visibility = "visible";
	
};


GLOBALP.utils.hide = function (obj)
{
	
	obj.style.display = "none";
	
	obj.style.visibility = "hidden";
	
};

// Determines the default display value for the object's HTML element type. For
// instance, a DIV would have a default display of "block". This method should
// ensure that we use the correct display value for each browser.

GLOBALP.utils.getDefaultDisplay = function (obj)
{
	
	var value = "";
	
	// Create a new element with a tag name that corresponds to the object.
	
	var objCopy = document.createElement(obj.tagName);
	
	// Unless we add the element to the document, we won't know the actual
	// display value. IE will return an empty string; Firefox, "block".
	
	// By setting the visibility to hidden, we ensure that the user will not
	// see any such changes.
	
	objCopy.visibility = "hidden";
	
	document.body.appendChild(objCopy);
	
	value = GLOBALP.utils.getActiveStyle(objCopy).display;
	
	document.body.removeChild(objCopy);
	
	return value;
	
};

GLOBALP.utils.debug = function (message)
{
	// alert(message);
};

/* Used to launch slideshow. */

function MM_openBrWindow(theURL,winName,features) { //v2.0
	window.open(theURL,winName,features);
}