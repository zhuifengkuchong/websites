jQuery(document).ready(function($) {

//http://www.jarloo.com/google-stock-api/

function moveHeadline(){
$('#headline').animate({right:'400px',opacity:0},2000);


};




//Stock API for header

$.ajax({
    url: 'http://finance.google.com/finance/info?client=ig&q=ABG', 
    success: function(data){ 

    	$('#st1').html(data[0].c);
    	if(data[0].c < 0){
    		$('#st1').css("color", "#B22725");
    	}
    	else{
    		$('#st1').css("color", "#427102");
    	}
    	$('#st2').html(data[0].cp+"%");
    	if(data[0].cp < 0){
    		$('#st2').css("color", "#B22725");
    	}
    	else{
    		$('#st2').css("color", "#427102");
    	}
    	$('#st3').text(data[0].l);
    },
//  error: function(){ alert('error');},
    dataType: 'jsonp'
});

});

//Home Page Headline Slider

messages = [
"<div class='headlineMessage'><h1 ><span style='font-size:0.8em;'>Welcome To Asbury Automotive Group,</span><br><i style='font-size:3em;font-weight:bold; line-height:1.21em'> Let's Drive.</i></h1></div>",
"<div class='headlineMessage'><h1 ><i style='font-size:3em;font-weight:bold; line-height:1.21em'>Fortune 500</i><br><i style='font-size:0.8em;'>Automotive Retailer.</i></h1></div>",
"<div class='headlineMessage'><h1 ><i style='font-size:3em;font-weight:bold; line-height:1.21em'>The 5<sup style='top: -0.6989em; font-size: .51em;'>th</sup> Largest</i><br><i style='font-size:0.8em;'>public automotive retailer in the country.</i></h1></div>",
"<div class='headlineMessage'><h1 ><i style='font-size:3em;font-weight:bold; line-height:1.21em'>Over 5,000</i><br><i style='font-size:0.8em;'>vehicles serviced per day.</i></h1></div>",
"<div class='headlineMessage'><h1 ><i style='font-size:3em;font-weight:bold; line-height:1.21em'>Nearly 8,000</i><br><i style='font-size:0.8em;'>employees across the country.</i></h1></div>",
"<div class='headlineMessage'><h1 ><i style='font-size:3em;font-weight:bold; line-height:1.21em'>Over <sup style='top: -0.6989em; font-size: .51em;'>$</sup>5 Billion</i><br><i style='font-size:0.8em;'>in revenue in 2013.</i></h1></div>",
"<div class='headlineMessage'><h1 ><i style='font-size:3em;font-weight:bold; line-height:1.21em'>81 Retail Stores</i><br><i style='font-size:0.8em;'>across America.</i></h1></div>",
"<div class='headlineMessage'><h1 ><i style='font-size:3em;font-weight:bold; line-height:1.21em'>29 Brands</i><br><i style='font-size:0.8em;'>of American, European and Asian vehicles.</i></h1></div>",
"<div class='headlineMessage'><h1 ><i style='font-size:3em;font-weight:bold; line-height:1.21em'>Over 85%</i><br><i style='font-size:0.8em;'>of sales from luxury and import brands.</i></h1></div>"];


//console.log(messages[1]);


var allMessages = $('#headlineContainer > .headlineMessage');
var countMessages = messages.length;

function cycleMessages(){
	currentIndex = $('#headlineContainer').children().index($('#current'));
	//console.log(currentIndex,countMessages);
	if(currentIndex == (countMessages - 1)){
		//fade in the first one,
		currentIndex = 0;
	} else{
		currentIndex++;
	}
	$('#current').removeAttr('id').fadeOut(500);
	$('#headlineContainer > .headlineMessage').eq(currentIndex).attr('id','current').fadeIn(500);	
	//console.log(currentIndex,countMessages);
}

//messages.map(addDiv);
for (i=0; i<messages.length; i++){
//	console.log(i);
	$('#headlineContainer').append(messages);
}
cycleMessages();
setInterval(cycleMessages,4000);



/* Dealerships Page */



        var csvData;

 
      
                csvData = $.ajax({
                    type: "GET",
                    url: "http://www.asburyauto.com/js/dealerlist/dealers.csv",
                    dataType: "text",
                    success: function (result) {
                       // console.log(result);
                      var appendDivs = [];
                       var responseArray = [];
                       var responseArray = result.split(/\r\n|\n/);

							for (var i = 0; i < responseArray.length; i++) {
								responseArray[i] = responseArray[i].split(';');
								responseArray[i].pop();

								responseArray[i].dealerGroup = responseArray[i][0];
								responseArray[i].state = responseArray[i][1];
								responseArray[i].dealerName = responseArray[i][2];
								responseArray[i].addressLine1 = responseArray[i][3];
								responseArray[i].addressLine2 = responseArray[i][4];
								responseArray[i].phoneNum = responseArray[i][5];
								responseArray[i].website = responseArray[i][6];
								responseArray[i].make = responseArray[i][7];
								responseArray[i].secmake = responseArray[i][8];
								responseArray[i].thirdmake = responseArray[i][9];

                appendDivs += '<div class="dealer" group="'+responseArray[i].dealerGroup+'" state="'+responseArray[i].state+'" make="'+responseArray[i].make+'">';
                appendDivs += '<strong>'+responseArray[i].dealerName+'</strong><br>';
                appendDivs += responseArray[i].addressLine1+'<br>';
                appendDivs += responseArray[i].addressLine2+'<br>';
                appendDivs += '<span class="tel phone_p">'+responseArray[i].phoneNum+'</span><br>';
				appendDivs += '<a class="website bodylink" href="'+responseArray[i].website+'" target="_blank">visit website</a><br />';
                appendDivs += '</div>';

							};

             $('#right_column_two_third').append(appendDivs);
                     }
                });


$(".make").click(function() {

  thisID = $(this).attr('make');
    thisText = $(this).attr('filter');
    
  $('#searchFilterTitle').fadeIn();
    $('#removeFilter').fadeIn();
    $('#filteringBy').text(thisText);

  $('.dealer').fadeOut(0);
  $('div[make="'+thisID+'"]').fadeIn(300);

});


$(".state").click(function() {

  thisID = $(this).attr('state');
    thisText = $(this).attr('filter');
    
  $('#searchFilterTitle').fadeIn();
    $('#removeFilter').fadeIn();
    $('#filteringBy').text(thisText);

  $('.dealer').fadeOut(0);
  $('div[state="'+thisID+'"]').fadeIn(300);

});


$(".group").click(function() {

  thisID = $(this).attr('id');


  $('#searchFilterTitle').fadeIn();
    $('#removeFilter').fadeIn();
  $('#filteringBy').text(thisID+' Group');

  $('.dealer').fadeOut(0);
  $('div[group="'+thisID+'"]').fadeIn(500);

});


$('#removeFilter').click(function(){


  $('#removeFilter').fadeOut();
  $('#searchFilterTitle').fadeOut();
  $('#filteringBy').text();
  $('.dealer').fadeIn(0);

});

$('.statemap').hover(
	function() {
		overImage = $(this).attr('state');
		$('#map').attr("src", "img/map_over-"+overImage+".png");
	},
	function() {
	$('#map').attr("src", "Unknown_83_filename"/*tpa=http://www.asburyauto.com/js/img/map.png*/);
});


// Scrolling sidebar for your website

// Downloaded from Marcofolio.net

// Read the full article: http://www.marcofolio.net/webdesign/create_a_sticky_sidebar_or_box.html



window.onscroll = function()


{

  if( window.XMLHttpRequest ) {

var mainHeight = parseInt($('#mainArea').height());
var sideHeight = parseInt($('#left_column_one_third').height());
var footerHeight = parseInt($('#footer').height());
var rightHeight = parseInt($('#right_column_two_third').height());




if(sideHeight < rightHeight){


var mainHeight = mainHeight - (sideHeight - footerHeight);


    if (document.documentElement.scrollTop < 281 || self.pageYOffset < 281) {

      $('#left_column_one_third').css('position','relative');

    } 
    if ((document.documentElement.scrollTop > 281 && mainHeight > document.documentElement.scrollTop) || (self.pageYOffset > 281 && mainHeight > self.pageYOffset)) {

      $('#left_column_one_third').css('position','fixed');
      $('#left_column_one_third').css('top','0');

    } 


   if (mainHeight < self.pageYOffset || mainHeight < document.documentElement.scrollTop){

      $('#left_column_one_third').css('position','absolute');
      $('#left_column_one_third').css('bottom','0');
      $('#left_column_one_third').css('top','');

  }

} else {

$('#left_column_one_third').css('position','relative');

}


}
}

/* This is for the Team Page */

$('.thumb').click(function(){
 var Ident = $(this).attr('name');

    Ident = Ident.split('_');
    //console.log(Ident[0]);
    $('.divs').not('div[make="'+Ident+'"]').hide();
    $('div[name="'+Ident+'"]').show();



});



/* This is for the Careers Page */

$('.click').click(function(){

	var ident = $(this).attr('id');
	ident = ident.split('_');
	$('#right_column_two_third_careers div').not('#'+ident[0]).hide();
	$('#'+ident[0]).show().children('div').show();
});

$('#right_column_two_third_careers div').not('#benefits').hide();
$('#benefits').show().children('div').show();

//$('.accordion > dd').hide();

$('.accordion > dt > a').click(function() {
    $(this).parent().next().slideToggle();
    return false;
  });

//$('.acc_paragraph').hide();

/*Acc for the Careers Page */


// <h1 ><i style='font-size:3.5em;font-weight:bold; line-height:.81em'>Over 4,000</i><br>
//     <i style='font-size:0.8em;'>vehicles serviced per day</i></h1>

// <h1 ><i style='font-size:3.5em;font-weight:bold; line-height:.81em'>Fortune 500</i><br>
//     <i style='font-size:0.8em;'>Automotive Retailer</i></h1>

// <h1 ><i style='font-size:3.5em;font-weight:bold; line-height:.81em'>Over 6,500</i><br>
//     <i style='font-size:0.8em;'>employees across the country</i></h1>

// <h1 ><i style='font-size:3.5em;font-weight:bold; line-height:.81em'>Over $4 Billion</i><br>
//     <i style='font-size:0.8em;'>in revenue in 2011</i></h1>

// <h1 ><i style='font-size:2.5em;font-weight:bold; line-height:2.9em'>81 Retail Stores</i></h1>

//  <h1 ><i style='font-size:3.5em;font-weight:bold; line-height:.81em'>28 Brands</i><br>
//     <i style='font-size:0.8em;'>of American,
// European and Asian vehicles</i></h1>


//  <h1 ><i style="font-size:3.5em;font-weight:bold; line-height:1.21em">Nearly 85%</i><br>
//     <i style="font-size:0.8em;">of sales are
// from import brands</i></h1>





