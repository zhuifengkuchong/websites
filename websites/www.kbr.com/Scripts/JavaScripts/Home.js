// images to randomly display on homepage are listed here
var random_images = [
  'Unknown_83_filename'/*tpa=http://www.kbr.com/Scripts/JavaScripts/We-Deliver.jpg*/,
  'Unknown_83_filename'/*tpa=http://www.kbr.com/Scripts/JavaScripts/Delivering-Excellence.jpg*/,
  'Unknown_83_filename'/*tpa=http://www.kbr.com/Scripts/JavaScripts/Global-Citizen-and-Neighbor.jpg*/,
  'Unknown_83_filename'/*tpa=http://www.kbr.com/Scripts/JavaScripts/Global-Reach-Local-Presence.jpg*/,
  'Unknown_83_filename'/*tpa=http://www.kbr.com/Scripts/JavaScripts/Any-Project-Any-Time-Any-Place.jpg*/,
  'Unknown_83_filename'/*tpa=http://www.kbr.com/Scripts/JavaScripts/Delivering-Responsibly.jpg*/
];

// how many different billboards do we want to appear
var howManyBillboards = 1,
    randNum = Math.abs( Math.floor( Math.random() * howManyBillboards ) ),
    ImageURL = 'url(/Images/Design/Screen/Billboards/Home/' + random_images[randNum] + ')',
    ImageName = random_images[randNum];

$(function () {

  //Set billboard image
  $( '#Billboard-Home' ).css( 'background-image', ImageURL );


	// Create rotating, tabbed highlight section
//	$.ajax({
//		dataType: 'script',
//		cache: true,
//		success: function () {
//			var i,
//          id,
//          obj = $.parseJSON( data ),
//          highlight,
//          highlightArr = [],
//          clickEvent,
//          tabber1;
//			for ( i = 0; i < data.length; i += 1 ) {
//			  obj = data[i];
//        id = 'Highlight-0' + (i + 1);
//        if ( obj.url.indexOf('http:') != -1 ) {
//          highlight = '<div class="Highlight" id="' + id + '"><h2><a href="' + obj.url + '" target="_blank">' + obj.title + '</a></h2><p>' + obj.intro + '</p></div>';
//        } else {
//				  highlight = '<div class="Highlight" id="' + id + '"><h2><a href="' + obj.url + '" >' + obj.title + '</a></h2><p>' + obj.intro + '</p></div>';
//        }
//				$( highlight ).appendTo('#Highlights');
//		  }
//		},
//		complete: function () {
//			tabber1 = new Yetii({
//				id: 'Feature',
//				active: 1,
//				interval: 10,
//				wait: 10,
//				tabclass: 'Highlight',
//				activeclass: 'Selected'
//			});
//		}
//	});

  // Add the four latest press releases from the XML file to the home page
  $.ajax({
    url: 'http://www.kbr.com/XML/press-releases.xml',
    dataType: 'xml',
    cache: true,
    success: function ( xml ) {
      var e,
          title,
          date,
          dateArray = [],
          pressRelease,
          articleLink;

      $( xml ).find( 'item' ).each( function ( e ) {

        title = $( this ).find( 'title' ).text();
        date = $( this ).find( 'pubDate' ).text();
        articleLink = $( this ).find( 'link' ).text();
        dateArray = date.split(" ");

        if ( e < 4 ) {

          title = '<h3><a href="' + articleLink + '">' + title + '</a></h3>';
          date = '<p>' + dateArray[1] + ' ' + dateArray[0] + ', ' + dateArray[2] + '</p>';
          pressRelease = '<div class="Headline">' + title + date + '</div>';

          $( pressRelease ).appendTo( '#LatestPressReleases' );

        }

      });
    }
  });

  //Events
  $.getJSON('http://www.kbr.com/Scripts/JavaScripts/data/events.json', function( data ) {
    var eventArr = [];
    $.each( data.events, function( key ) {
      var obj = data.events[key],
          items;
      if ( obj.expire >= cDate ) {
        items = '<div class="Headline"><h3>' + obj.eventname + '</h3><p>' + obj.month.substring(0,3) + ' ' + obj.dates +  ' | ' + obj.city + '</p></div>';
        eventArr.push(items);
      }
    });
    for ( var j = 0; j < 3; j++) {
      $( eventArr[j] ).appendTo( '#Events' );
    }
    $('<div class="Headline"><h3><a href="http://www.kbr.com/Newsroom/Events/">More Events</a></h3></div>').appendTo('#Events');
  });


  // Google Analytics event tracking, featured project
  (function () {
    var obj, title;
    obj = '#Extra01 a[href!="/Projects/"]';
    title = $(obj + ' h3').text();
    $(obj).mousedown(function(){
      _gaq.push(['_trackEvent', 'Featured Project, Homepage', 'Image & Title', title, 1]);
    });
  }());

  // Google Analytics event tracking, featured project
  (function () {
    var obj, title;
    obj = '#Extra01 a[href$="/Projects/"]';
    title = $(obj).text();
    $(obj).mousedown(function(){
      _gaq.push(['_trackEvent', 'Featured Project, Homepage', 'See All Projects', title, 2]);
    });
  }());

  // Google Analytics event tracking, advertisements
  $('#Extra02 div').each(function(){
    var id, obj, title;
    id = $(this).attr('id');
    obj = '#' + id + ' h2 a';
    title = $(obj).text();
    $(obj).mousedown(function () {
      _gaq.push(['_trackEvent', 'Advertisements, Homepage', id, title, 2]);
    });
  });

  // Google Analytics event tracking, publications
  $('#Publications h3 a').each(function(){
    var cat, title;
    cat = $(this).attr('rel');
		if (cat !== 'More Publications') {
		  cat = cat + ' Download';
		}
    title = $(this).text();
    $(this).mousedown(function () {
      _gaq.push(['_trackEvent', 'Publications', cat, title, 1]);
    });
  });

  // Fixes z-index positioning wonky-ness in IE 9 and below
  $('#NavigationHeader').hover(
    // mouseenter
    function () {
      $('#Billboard-Home div.showing').hide();
    },
    // mouseleave
    function () {
      $('#Billboard-Home div.showing').show();
    }
  )

});

$(window).load(function(){


  // Google Analytics event tracking, billboard clicks
  var currentBanner = $( '#Billboard-Home .banner0' + ( randNum + 1 ) + '' ),
			currentBannerLink = $('p a',currentBanner),
			clickEvent;

	$(currentBanner).addClass('showing');

	$(currentBannerLink).each(function(){

		clickEvent = '_gaq.push([\'_trackEvent\', \'Static Banner Click\', \'' + ImageName + '\', \'' + $(this).attr('href').toLowerCase() + '\']);';
		$(this).attr('onclick',clickEvent);

	})

  // Delay loading the stock quote until after the DOM finishes loading, the stock has been shown to block other things from loading in the past
  $('#Stock img').prop('src','../../../quotes.corporate-ir.net/media_files/irol/19/198137/qi/kbr_qi_1.gif'/*tpa=http://quotes.corporate-ir.net/media_files/irol/19/198137/qi/kbr_qi_1.gif*/);

})






























