//Google Analytics
function _pageTracker(a){this.type=a;this._trackEvent=function(a,b,c){_gaq.push(["_trackEvent",a,b,c])}}var _gaq=_gaq||[];_gaq.push(["_setAccount","UA-1013450-24"]);_gaq.push(["_trackPageview"]);(function(){var a,b;a=document.createElement("script");a.type="text/javascript";a.async=true;a.src=("https:"==document.location.protocol?"https://ssl":"http://www/")+".google-analytics.com/ga.js";b=document.getElementsByTagName("script")[0];b.parentNode.insertBefore(a,b)})();var pageTracker=new _pageTracker;

//Clears the Search Box Default Text
function clearDefault(el){if (el.defaultValue==el.value) el.value = ""}

//Yeti
function Yetii(){this.defaults={id:null,active:1,interval:null,wait:null,persist:null,tabclass:"tab",activeclass:"active",callback:null,leavecallback:null};this.activebackup=null;for(var a in arguments[0]){this.defaults[a]=arguments[0][a]}this.getTabs=function(){var a=[];var b=document.getElementById(this.defaults.id).getElementsByTagName("*");var c=new RegExp("(^|\\s)"+this.defaults.tabclass.replace(/\-/g,"\\-")+"(\\s|$)");for(var d=0;d<b.length;d++){if(c.test(b[d].className))a.push(b[d])}return a};this.links=document.getElementById("Highlight-Switch").getElementsByTagName("a");this.listitems=document.getElementById("Highlight-Switch").getElementsByTagName("li");this.show=function(a){for(var b=0;b<this.tabs.length;b++){this.tabs[b].style.display=b+1==a?"block":"none";if(b+1==a){this.addClass(this.links[b],this.defaults.activeclass);this.addClass(this.listitems[b],this.defaults.activeclass+"li")}else{this.removeClass(this.links[b],this.defaults.activeclass);this.removeClass(this.listitems[b],this.defaults.activeclass+"li")}}if(this.defaults.leavecallback&&a!=this.activebackup)this.defaults.leavecallback(this.defaults.active);this.activebackup=a;this.defaults.active=a;if(this.defaults.callback)this.defaults.callback(a)};this.rotate=function(a){this.show(this.defaults.active);this.defaults.active++;if(this.defaults.active>this.tabs.length)this.defaults.active=1;var b=this;if(this.defaults.wait)clearTimeout(this.timer2);this.timer1=setTimeout(function(){b.rotate(a)},a*1e3)};this.next=function(){this.defaults.active++;if(this.defaults.active>this.tabs.length)this.defaults.active=1;this.show(this.defaults.active)};this.previous=function(){this.defaults.active--;if(!this.defaults.active)this.defaults.active=this.tabs.length;this.show(this.defaults.active)};this.gup=function(a){a=a.replace(/[\[]/,"\\[").replace(/[\]]/,"\\]");var b="[\\?&]"+a+"=([^&#]*)";var c=new RegExp(b);var d=c.exec(window.location.href);if(d==null)return null;else return d[1]};this.parseurl=function(a){var b=this.gup(a);if(b==null)return null;if(parseInt(b))return parseInt(b);if(document.getElementById(b)){for(var c=0;c<this.tabs.length;c++){if(this.tabs[c].id==b)return c+1}}return null};this.createCookie=function(a,b,c){if(c){var d=new Date;d.setTime(d.getTime()+c*24*60*60*1e3);var e="; expires="+d.toGMTString()}else var e="";document.cookie=a+"="+b+e+"; path=/"};this.readCookie=function(a){var b=a+"=";var c=document.cookie.split(";");for(var d=0;d<c.length;d++){var e=c[d];while(e.charAt(0)==" ")e=e.substring(1,e.length);if(e.indexOf(b)==0)return e.substring(b.length,e.length)}return null};this.contains=function(a,b,c){return a.indexOf(b,c)!=-1};this.hasClass=function(a,b){return this.contains(a.className,b," ")};this.addClass=function(a,b){if(!this.hasClass(a,b))a.className=(a.className+" "+b).replace(/\s{2,}/g," ").replace(/^\s+|\s+$/g,"")};this.removeClass=function(a,b){a.className=a.className.replace(new RegExp("(^|\\s)"+b+"(?:\\s|$)"),"$1");a.className.replace(/\s{2,}/g," ").replace(/^\s+|\s+$/g,"")};this.tabs=this.getTabs();this.defaults.active=this.parseurl(this.defaults.id)?this.parseurl(this.defaults.id):this.defaults.active;if(this.defaults.persist&&this.readCookie(this.defaults.id))this.defaults.active=this.readCookie(this.defaults.id);this.activebackup=this.defaults.active;this.show(this.defaults.active);var b=this;for(var c=0;c<this.links.length;c++){this.links[c].customindex=c+1;this.links[c].onclick=function(){if(b.timer1)clearTimeout(b.timer1);if(b.timer2)clearTimeout(b.timer2);b.show(this.customindex);if(b.defaults.persist)b.createCookie(b.defaults.id,this.customindex,0);if(b.defaults.wait)b.timer2=setTimeout(function(){b.rotate(b.defaults.interval)},b.defaults.wait*1e3);return false}}if(this.defaults.interval)this.rotate(this.defaults.interval)}

// Thickbox 3
var tb_pathToImage = "../../Images/Design/loadingAnimation.gif"/*tpa=http://www.kbr.com/Images/Design/loadingAnimation.gif*/;
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('$(o).2S(9(){1u(\'http://www.kbr.com/Scripts/JavaScripts/a.18, 3n.18, 3i.18\');1w=1p 1t();1w.L=2H});9 1u(b){$(b).s(9(){6 t=X.Q||X.1v||M;6 a=X.u||X.23;6 g=X.1N||P;19(t,a,g);X.2E();H P})}9 19(d,f,g){3m{3(2t o.v.J.2i==="2g"){$("v","11").r({A:"28%",z:"28%"});$("11").r("22","2Z");3(o.1Y("1F")===M){$("v").q("<U 5=\'1F\'></U><4 5=\'B\'></4><4 5=\'8\'></4>");$("#B").s(G)}}n{3(o.1Y("B")===M){$("v").q("<4 5=\'B\'></4><4 5=\'8\'></4>");$("#B").s(G)}}3(1K()){$("#B").1J("2B")}n{$("#B").1J("2z")}3(d===M){d=""}$("v").q("<4 5=\'K\'><1I L=\'"+1w.L+"\' /></4>");$(\'#K\').2y();6 h;3(f.O("?")!==-1){h=f.3l(0,f.O("?"))}n{h=f}6 i=/\\.2s$|\\.2q$|\\.2m$|\\.2l$|\\.2k$/;6 j=h.1C().2h(i);3(j==\'.2s\'||j==\'.2q\'||j==\'.2m\'||j==\'.2l\'||j==\'.2k\'){1D="";1G="";14="";1z="";1x="";R="";1n="";1r=P;3(g){E=$("a[@1N="+g+"]").36();25(D=0;((D<E.1c)&&(R===""));D++){6 k=E[D].u.1C().2h(i);3(!(E[D].u==f)){3(1r){1z=E[D].Q;1x=E[D].u;R="<1e 5=\'1X\'>&1d;&1d;<a u=\'#\'>2T &2R;</a></1e>"}n{1D=E[D].Q;1G=E[D].u;14="<1e 5=\'1U\'>&1d;&1d;<a u=\'#\'>&2O; 2N</a></1e>"}}n{1r=1b;1n="1t "+(D+1)+" 2L "+(E.1c)}}}S=1p 1t();S.1g=9(){S.1g=M;6 a=2x();6 x=a[0]-1M;6 y=a[1]-1M;6 b=S.z;6 c=S.A;3(b>x){c=c*(x/b);b=x;3(c>y){b=b*(y/c);c=y}}n 3(c>y){b=b*(y/c);c=y;3(b>x){c=c*(x/b);b=x}}13=b+30;1a=c+2G;$("#8").q("<a u=\'\' 5=\'1L\' Q=\'1o\'><1I 5=\'2F\' L=\'"+f+"\' z=\'"+b+"\' A=\'"+c+"\' 23=\'"+d+"\'/></a>"+"<4 5=\'2D\'>"+d+"<4 5=\'2C\'>"+1n+14+R+"</4></4><4 5=\'2A\'><a u=\'#\' 5=\'Z\' Q=\'1o\'>1l</a> 1k 1j 1s</4>");$("#Z").s(G);3(!(14==="")){9 12(){3($(o).N("s",12)){$(o).N("s",12)}$("#8").C();$("v").q("<4 5=\'8\'></4>");19(1D,1G,g);H P}$("#1U").s(12)}3(!(R==="")){9 1i(){$("#8").C();$("v").q("<4 5=\'8\'></4>");19(1z,1x,g);H P}$("#1X").s(1i)}o.1h=9(e){3(e==M){I=2w.2v}n{I=e.2u}3(I==27){G()}n 3(I==3k){3(!(R=="")){o.1h="";1i()}}n 3(I==3j){3(!(14=="")){o.1h="";12()}}};16();$("#K").C();$("#1L").s(G);$("#8").r({Y:"T"})};S.L=f}n{6 l=f.2r(/^[^\\?]+\\??/,\'\');6 m=2p(l);13=(m[\'z\']*1)+30||3h;1a=(m[\'A\']*1)+3g||3f;W=13-30;V=1a-3e;3(f.O(\'2j\')!=-1){1E=f.1B(\'3d\');$("#15").C();3(m[\'1A\']!="1b"){$("#8").q("<4 5=\'2f\'><4 5=\'1H\'>"+d+"</4><4 5=\'2e\'><a u=\'#\' 5=\'Z\' Q=\'1o\'>1l</a> 1k 1j 1s</4></4><U 1W=\'0\' 2d=\'0\' L=\'"+1E[0]+"\' 5=\'15\' 1v=\'15"+1f.2c(1f.1y()*2b)+"\' 1g=\'1m()\' J=\'z:"+(W+29)+"p;A:"+(V+17)+"p;\' > </U>")}n{$("#B").N();$("#8").q("<U 1W=\'0\' 2d=\'0\' L=\'"+1E[0]+"\' 5=\'15\' 1v=\'15"+1f.2c(1f.1y()*2b)+"\' 1g=\'1m()\' J=\'z:"+(W+29)+"p;A:"+(V+17)+"p;\'> </U>")}}n{3($("#8").r("Y")!="T"){3(m[\'1A\']!="1b"){$("#8").q("<4 5=\'2f\'><4 5=\'1H\'>"+d+"</4><4 5=\'2e\'><a u=\'#\' 5=\'Z\'>1l</a> 1k 1j 1s</4></4><4 5=\'F\' J=\'z:"+W+"p;A:"+V+"p\'></4>")}n{$("#B").N();$("#8").q("<4 5=\'F\' 3c=\'3b\' J=\'z:"+W+"p;A:"+V+"p;\'></4>")}}n{$("#F")[0].J.z=W+"p";$("#F")[0].J.A=V+"p";$("#F")[0].3a=0;$("#1H").11(d)}}$("#Z").s(G);3(f.O(\'37\')!=-1){$("#F").q($(\'#\'+m[\'26\']).1T());$("#8").24(9(){$(\'#\'+m[\'26\']).q($("#F").1T())});16();$("#K").C();$("#8").r({Y:"T"})}n 3(f.O(\'2j\')!=-1){16();3($.1q.35){$("#K").C();$("#8").r({Y:"T"})}}n{$("#F").34(f+="&1y="+(1p 33().32()),9(){16();$("#K").C();1u("#F a.18");$("#8").r({Y:"T"})})}}3(!m[\'1A\']){o.21=9(e){3(e==M){I=2w.2v}n{I=e.2u}3(I==27){G()}}}}31(e){}}9 1m(){$("#K").C();$("#8").r({Y:"T"})}9 G(){$("#2Y").N("s");$("#Z").N("s");$("#8").2X("2W",9(){$(\'#8,#B,#1F\').2V("24").N().C()});$("#K").C();3(2t o.v.J.2i=="2g"){$("v","11").r({A:"1Z",z:"1Z"});$("11").r("22","")}o.1h="";o.21="";H P}9 16(){$("#8").r({2U:\'-\'+20((13/2),10)+\'p\',z:13+\'p\'});3(!(1V.1q.2Q&&1V.1q.2P<7)){$("#8").r({38:\'-\'+20((1a/2),10)+\'p\'})}}9 2p(a){6 b={};3(!a){H b}6 c=a.1B(/[;&]/);25(6 i=0;i<c.1c;i++){6 d=c[i].1B(\'=\');3(!d||d.1c!=2){39}6 e=2a(d[0]);6 f=2a(d[1]);f=f.2r(/\\+/g,\' \');b[e]=f}H b}9 2x(){6 a=o.2M;6 w=1S.2o||1R.2o||(a&&a.1Q)||o.v.1Q;6 h=1S.1P||1R.1P||(a&&a.2n)||o.v.2n;1O=[w,h];H 1O}9 1K(){6 a=2K.2J.1C();3(a.O(\'2I\')!=-1&&a.O(\'3o\')!=-1){H 1b}}',62,211,'|||if|div|id|var||TB_window|function||||||||||||||else|document|px|append|css|click||href|body||||width|height|TB_overlay|remove|TB_Counter|TB_TempArray|TB_ajaxContent|tb_remove|return|keycode|style|TB_load|src|null|unbind|indexOf|false|title|TB_NextHTML|imgPreloader|block|iframe|ajaxContentH|ajaxContentW|this|display|TB_closeWindowButton||html|goPrev|TB_WIDTH|TB_PrevHTML|TB_iframeContent|tb_position||thickbox|tb_show|TB_HEIGHT|true|length|nbsp|span|Math|onload|onkeydown|goNext|Esc|or|close|tb_showIframe|TB_imageCount|Close|new|browser|TB_FoundURL|Key|Image|tb_init|name|imgLoader|TB_NextURL|random|TB_NextCaption|modal|split|toLowerCase|TB_PrevCaption|urlNoQuery|TB_HideSelect|TB_PrevURL|TB_ajaxWindowTitle|img|addClass|tb_detectMacXFF|TB_ImageOff|150|rel|arrayPageSize|innerHeight|clientWidth|self|window|children|TB_prev|jQuery|frameborder|TB_next|getElementById|auto|parseInt|onkeyup|overflow|alt|unload|for|inlineId||100||unescape|1000|round|hspace|TB_closeAjaxWindow|TB_title|undefined|match|maxHeight|TB_iframe|bmp|gif|png|clientHeight|innerWidth|tb_parseQuery|jpeg|replace|jpg|typeof|which|keyCode|event|tb_getPageSize|show|TB_overlayBG|TB_closeWindow|TB_overlayMacFFBGHack|TB_secondLine|TB_caption|blur|TB_Image|60|tb_pathToImage|mac|userAgent|navigator|of|documentElement|Prev|lt|version|msie|gt|ready|Next|marginLeft|trigger|fast|fadeOut|TB_imageOff|hidden||catch|getTime|Date|load|safari|get|TB_inline|marginTop|continue|scrollTop|TB_modal|class|TB_|45|440|40|630|input|188|190|substr|try|area|firefox'.split('|'),0,{}))

var monthDigit, monthAbv, monthFull;

var monthConvert = function ( month ) {

  if (month === '01' || month === 'Jan' || month === 'January') {
   monthDigit = '01';
   monthAbv = 'Jan';
   monthFull = 'January';
 }

 if (month === '02' || month === 'Feb' || month === 'February') {
   monthDigit = '02';
   monthAbv = 'Feb';
   monthFull = 'February';
 }

 if (month === '03' || month === 'Mar' || month === 'March') {
   monthDigit = '03';
   monthAbv = 'Mar';
   monthFull = 'March';
 }

 if (month === '04' || month === 'Apr' || month === 'April') {
   monthDigit = '04';
   monthAbv = 'Apr';
   monthFull = 'April';
 }

 if (month === '05' || month === 'May') {
   monthDigit = '05';
   monthAbv = 'May';
   monthFull = 'May';
 }

 if (month === '06' || month === 'Jun' || month === 'June') {
   monthDigit = '06';
   monthAbv = 'Jun';
   monthFull = 'June';
 }

 if (month === '07' || month === 'Jul' || month === 'July') {
   monthDigit = '07';
   monthAbv = 'Jul';
   monthFull = 'July';
 }

 if (month === '08' || month === 'Aug' || month === 'August') {
   monthDigit = '08';
   monthAbv = 'Aug';
   monthFull = 'August';
 }

 if (month === '09' || month === 'Sep' || month === 'September') {
   monthDigit = '09';
   monthAbv = 'Sep';
   monthFull = 'September';
 }

 if (month === '10' || month === 'Oct' || month === 'October') {
   monthDigit = '10';
   monthAbv = 'Oct';
   monthFull = 'October';
 }

 if (month === '11' || month === 'Nov' || month === 'November') {
   monthDigit = '11';
   monthAbv = 'Nov';
   monthFull = 'November';
 }

 if (month === '12' || month === 'Dec' || month === 'December') {
   monthDigit = '12';
   monthAbv = 'Dec';
   monthFull = 'December';
 }

 return monthDigit, monthAbv, monthFull;

}

//get today's date
$(function() {
  var tDate, month, day, year;
  tDate = new Date();
  month = tDate.getMonth() + 1;
  month = month.toString();
  day   = tDate.getDate();
  year  = tDate.getFullYear();
  if ( month < 10 ) {	month = "0" + month; }
  if ( day < 10 ) {	day = "0" + day; }
  cDate = year + month + day;
});