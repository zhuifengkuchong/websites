/* RV2.js
 * This JS is mainly for RV2 products (show more)
 *
*/

(function(recentlyv2,window){
	var defaults = {
			"sliderPodsPerRow"		: 4,
			"showMorePodsPerRow"	: 6,
			"showATC"				: true,
			"isCart"				: false,
			"maxTitleLength"		: 60,
			"maxDescriptionLength"	: 70,
			"showMoreButton"		: false,
			"apiKey"				: "xsCGRApF2OyytV2Lf3PihBfFLXvz3tkX",
			"callback"				: function(){return true;},

			/*API INPUT DEFAULTS*/
		    "levels": "",
		    "storeId": "",
		    "trackingId": "",
		    "sessionId": "",
		    "customerId": "", //send 
		    "appId": "homedepot01", //send
		    "certonaSchema": "",
		    "exItems": "",
		    "maxProducts": 18,
		    "products": ""

			/*END INPUT DEFAULTS*/

		},

		savedData = {
			"pageId": "",
			"trackingId" : "",
			"parentItemName": "",
			"parentItemID": "",
			"parentShowMore": "",
			"rv2Schema": "",
			"resxLinks" : ""
		},

		baseTemplatePath = "/HFApp/static/global/scripts/desktop/thirdparty/certona/templates/", 
		rv2Listings = [], certonaSchemaData = '', schemeTemplate = '', rv2ContainerID = '', /*showMoreButtonPosition = '',*/ showMoreButton = '',

		getRV2SchemeData = function(certonaSchema){
			getRV2CookieData();

			var basePath = "/ProductServices/v2/products/recommendation",
				host = getRV2Host(),
				stringData = JSON.stringify({
								  "levels":defaults.levels,
								  "storeId":"",
								  "trackingId": defaults.trackingId,
								  "sessionId": defaults.sessionId,
								  "customerId":"",
								  "appId": defaults.appId,
								  "certonaSchema": defaults.certonaSchema,
								  "exItems":"",
								  "maxProducts": defaults.maxProducts,
								  "products":savedData.parentItemID
								}),
				requestUrl = THD.Utils.Url.set({
							setParms: {
								type : "json",
								key : getAPIKey()
							},
							url : host + basePath
						});

			//setCertonaHostProperty(host);

			$.ajax({
				url: requestUrl,
				type: "POST",
				contentType : "application/json",
				timeout : 2000,
				data: stringData
			}).done(function(data) {
				certonaSchemaData = data;
				processRV2Data(certonaSchemaData)
			}).fail(function(data) {
				THD.log("Load failed. Response: ", data);
			});

		},

		getRV2Host = function(){
			var hostName = getDomain(),
				protocol = window.location.protocol + "//",
				isBeta = readCookie("HD_DC") === "beta",
				isProdBeta = (hostName.indexOf("ps71") < 0),
				apiPrefix = protocol + "origin.api",
				betaHost = "";

				if(isBeta){
					betaHost = (isProdBeta) ? "-beta" : "beta.";
				}

			apiPrefix = (hostName.indexOf("homedepotdev") > -1) ? apiPrefix + "." : apiPrefix;


			hostName = hostName.replace("http://", apiPrefix + betaHost).replace("secure2","");
			hostName = hostName.replace("www.", ".");

			return hostName;
		},

		toggleNoProductsArrow = function(args){
			$("[data-itemid='"+ args.itemId +"']").parents(".content").toggleClass('noProductsParent', args.flag);
		},

		processRV2Data = function(certonaData){
		
			var productsData = certonaData.schemas,
				products = productsData[0].products,
				/*i = products.length,*/
				productsTitle = productsData[0].title,
				productsSchemaId = productsData[0].schemaId,
				podLimit = defaults.showMorePodsPerRow,
				imageDimension = "100", /*(defaults.isCart) ? "100" : "145",*/
				baseImageUrl = "/catalog/productImages/" + imageDimension + "/", productImage;


			if (products === undefined || products === null){

				rv2Listings.schemaItemId = defaults.certonaSchema + "_" + savedData.parentItemID;
				rv2Listings.noProductsFlag = true;
				rv2Listings.productsFlag = false;
				savedData.pageId = certonaData.pageId;
				savedData.resxLinks = '';

			} else {
				i = products.length;

				while(i--){

					isRowStart = (i % podLimit === 0);
					isRowEnd = ((i % podLimit) * podLimit === (podLimit - 1) * podLimit);
					dollarOff = products[i].dollarOff;
					discountFlag = (dollarOff !== 0) && (typeof dollarOff != "undefined");

					products[i].configurableFlag = (products[i].itemType === "CONFIGURABLE_BLINDS");
					products[i].merchFlag = (products[i].itemType === "MERCHANDISE");
					products[i].applianceFlag = (products[i].itemType === "MAJOR_APPLIANCE");

					products[i].isRowStart = isRowStart;

					if (products[i].guid !== 'No_Image'){
						products[i].productImage = baseImageUrl + products[i].guid.substring(0,2) + "/" + products[i].guid + "_" + imageDimension + ".jpg";
					} else {
						products[i].productImage = "../../../../../../../static/images/no-image-available.jpeg"/*tpa=http://www.homedepot.com/static/images/no-image-available.jpeg*/;
					}

					products[i].originalPrice = (typeof products[i].originalPrice != "undefined") ? products[i].originalPrice.toFixed(2) : "";
					products[i].price = (typeof products[i].price != "undefined") ? products[i].price.toFixed(2) : "";
					products[i].productNameShorten = rv2trimDescription(products[i].productName, "description");
					products[i].ratingPercentage = (products[i].rating / 5 * 100);
					products[i].imageDimension = imageDimension;
					products[i].recomendingOMSID = (defaults.products.indexOf(",") === -1 && savedData.parentItemID !== "")  ? savedData.parentItemID : "NA";
					products[i].discountFlag = (products[i].configurableFlag) ? false : discountFlag;
					products[i].schemaId = productsSchemaId;
					products[i].showATCFlag = defaults.showATC;

					if (savedData.resxLinks === "" || typeof savedData.resxLinks === null || typeof savedData.resxLink === undefined){
						savedData.resxLinks = products[i].productId + "|" + products[i].schemaId + ";";
					} else {
						savedData.resxLinks = savedData.resxLinks + ";" + products[i].productId + "|" + products[i].schemaId;
					}

					products[i].isRowEnd = isRowEnd;
				}

				rv2Listings.product = products;
				rv2Listings.productsFlag = true;
				rv2Listings.noProductsFlag = false;
				rv2Listings.title = "MORE LIKE " + rv2trimDescription(savedData.parentItemName, "title");
				rv2Listings.schemaItemId = defaults.certonaSchema + "_" + savedData.parentItemID;
				rv2Listings.pageId = savedData.pageid;
				rv2Listings.trackingId = certonaData.trackingId;
				rv2Listings.resxLinks = savedData.resxLinks;
				rv2Listings.origianlProduct = savedData.parentItemID;

				//savedData.pageId = certonaData.pageId;
			}

			savedData.resxLinks = "";

			renderScheme(rv2Listings);
		},

		renderScheme= function(){

			var rv2HTML = '';

			rv2HTML = Mustache.to_html(schemeTemplate, rv2Listings);

			$("[id^='rv_']").after(rv2HTML);

			$('#' + rv2Listings.schemaItemId).removeClass('hide');

			if (rv2Listings.noProductsFlag === true) {
				$('#' + rv2Listings.schemaItemId).addClass('noProductsListed');				
				
				toggleNoProductsArrow({
					flag: true,
					itemId: rv2Listings.schemaItemId.split("_")[3]
				});
			}

			$('#' + rv2Listings.schemaItemId + ' .showMoreV2:first').thdSliderV2();

			updateResx();

			$('.close-rv2').on("click", function(e){

				e.preventDefault();

				var closeClick = $(this),
					rv2ContainerID = closeClick.attr('data-showMoreID');

				$('.show-more-btn-container').removeClass('hide');
				$('#' + rv2ContainerID).addClass('hide');
				$('.show-more-btn-arrow').addClass('hide');
				savedData.resxLinks = '';				
				$("[id^='rv_']").removeClass('noProductsParent');
			});

			$("[id^='rv_'] .prev, [id^='rv_'] .next, .slide-left, .slide-right").on("click", function(e){

				$('.show-more-btn-container').removeClass('hide');
				$('.show-more-btn-arrow, .rv2ShowMore').addClass('hide');				
				$("[id^='rv_']").removeClass('noProductsParent');

			});

		},

		getTemplateData = function(templateName){
			loadTemplate(templateName);
		},

		loadTemplate = function(templateName){
			var path = baseTemplatePath + templateName;
			$.ajax({
				url: path,
			}).done(function(data) {
				schemeTemplate = data;
			}).fail(function() {
				THD.log("Could not load: "+ path);
			});
		},

		updateResx = function (){

			resx.event = "showmore";
			resx.itemid = $('#' + rv2ContainerID).data('itemid');
			resx.links = $('#' + rv2ContainerID).data('resxlinks');
			resx.customerid = defaults.customerId;
			resx.pageid = $('#' + rv2ContainerID).data('pageid');

			certonaResx.run()

		},

		getRV2CookieData = function(){
			defaults.sessionId = readCookie("RES_SESSIONID");
			defaults.trackingId = readCookie("RES_TRACKINGID");
			defaults.customerId = getWCUserIdFromCookies();

			if(defaults.storeId === ""){
				defaults.storeId = getTHDStoreNo();
			}

			if(defaults.storeId === "0"){
				defaults.storeId = "8119";
			}
		},

		rv2trimDescription = function(description, type){
			var maxDescriptionLength = defaults.maxDescriptionLength,
				maxTitleLength = defaults.maxTitleLength,
				maxLength = '';


			if (type === 'description'){
				maxLength = maxDescriptionLength;
			} else {
				maxLength = maxTitleLength;
			}

			if(description.length > maxLength){
				description = description.substring(0, maxLength);
				/* make sure ending at word */
				description = description.substr(0, Math.min(description.length, description.lastIndexOf(" ")));

				description = description + "...";
			}

			return description;
		},

		getHost = function(){
			var hostName = getDomain(),
				protocol = window.location.protocol + "//",
				isBeta = readCookie("usebeta"),
				isProdBeta = (hostName.indexOf("ps71") < 0),
				apiPrefix = protocol + "origin.api",
				betaHost = "";

				if(isBeta){
					betaHost = (isProdBeta) ? "-beta" : ".beta";
				}

			apiPrefix = (hostName.indexOf("homedepotdev") > -1) ? apiPrefix + "." : apiPrefix;


			hostName = hostName.replace("http://", apiPrefix + betaHost).replace("secure2","");
			hostName = hostName.replace("www.", ".");

			return hostName;
		},

		getAPIKey = function(){
			return defaults.apiKey;
		};

	$(document).on("click", '.show-more-btn', function(e) {
		e.preventDefault();
		var divIDParentExist, showMoreButton = $(this), showMoreButtonPosition, certonaData, processCertonaData, divClassParentExist, 
			dataDeferred = $.Deferred(), dataPromise = dataDeferred.promise();

		savedData.parentItemID = $(this).parent().attr('data-itemId');
		savedData.parentItemName = $(this).parent().attr('data-itemName');
		rv2ContainerID = defaults.certonaSchema + "_" + savedData.parentItemID;
		divIDParentExist = document.getElementById(rv2ContainerID);
		/*showMoreButtonPosition = {
			"x": showMoreButton[0].offsetLeft + 36 + "px",
			"y": showMoreButton[0].offsetTop + 24 + "px"
		};*/

		defaults.levels = $.trim($('#headerCrumb li:last-child a').html());

		if (divIDParentExist === null) {

			$('.rv2ShowMore, .show-more-btn-arrow').addClass('hide');
			$('.show-more-btn-container').removeClass('hide');

			dataDeferred.resolve(getRV2SchemeData(savedData.parentShowMore));
			
			showMoreButton.parent().addClass('hide');
			showMoreButton.parent().siblings('.show-more-btn-arrow').removeClass('hide');

		} else {

			$('.rv2ShowMore, .show-more-btn-arrow').addClass('hide');
			$('.show-more-btn-container').removeClass('hide');

				showMoreButton.parent().addClass('hide');
				showMoreButton.parent().siblings('.show-more-btn-arrow').removeClass('hide');
			$('#' + rv2ContainerID).removeClass('hide');
			
			if ( $('#' + rv2ContainerID).data('productsFalg') === true) {
				$('#' + rv2Listings.schemaItemId).addClass('noProductsListed');				

			}

			updateResx();
		}

	});

	/*shows the "show more" button if the "data-showmore" is true*/
	$(document).ready(function () {
		defaults.certonaSchema = $("[id^='rv_']").data('showmore');

		getTemplateData("http://www.homedepot.com/HFApp/static/global/scripts/desktop/thirdparty/certona/rv2.tmpl");

		THDModuleLoader.$includeJS("../../widgets/thdSliderV2.js"/*tpa=http://www.homedepot.com/HFApp/static/global/scripts/desktop/widgets/thdSliderV2.js*/);
		THDModuleLoader.$includeCSS("../../../../styles/desktop/widget/thdSliderV2.css"/*tpa=http://www.homedepot.com/HFApp/static/global/styles/desktop/widget/thdSliderV2.css*/);

		savedData.pageid = window.resx.pageid;

	});

}(
	THD.Utility.Namespace.createNamespace('THD.Thirdparty.RecentlyV2'),
	window
));
