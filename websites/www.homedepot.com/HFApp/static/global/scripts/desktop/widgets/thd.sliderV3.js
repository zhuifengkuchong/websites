/*

	Slider V3 to be used ONLY with content on responsive grid

	Sample HTML with autorun and runspeed set to 5 seconds:
	<div class="sliderV3" data-autorun="true" data-runspeed="5">
		<div class="col-1-4-lg col-1-3-sm pad">...</div>
		<div class="col-1-4-lg col-1-3-sm pad">...</div>
		<div class="col-1-4-lg col-1-3-sm pad">...</div>
		<div class="col-1-4-lg col-1-3-sm pad">...</div>
	</div>
*/
var Slider = (function(slider, $, window) {
	//private
	var sliderSelector = ".sliderV3",
		columnSelector = "div[class*='col-']",
		breakpoints = {
			"xs": 300,
			"sm": 568,
			"md": 769,
			"lg": 960
		},
		sliderWindowWidth,
		currentBreakpoint;

	//main init
	function build() {
		initSliders();
	}

	//get list of sliders from html and build new carousel object for each
	function initSliders() {
		var $sliderList = $(sliderSelector),
			i = $sliderList.length,
			$slider;

		while (i--) {
			$slider = $($sliderList[i]);

			if ($slider.data("loaded") !== "true") {
				var carousel = new Carousel($slider);
				carousel.init();
			}
		}
	}

	//parse through breakpoints object to get current breakpoint based on window width
	function getCurrentBreakPoint() {
		var windowWidth = $(window).width(),
			breakPoint,
			currentBreakPoint;

		for (var key in breakpoints) {
			if (breakpoints.hasOwnProperty(key)) {
				currentBreakPoint = breakpoints[key];
				if (windowWidth > currentBreakPoint) {
					breakPoint = key;
				}
			}
		}
		return breakPoint;
	}

	/**
	 * super simple carousel
	 * animation between panes happens with css transitions
	 */
	function Carousel(element) {
		var self = this,
			current_pane = 0,
			$sliderFrame,
			sliderWindowWidth,
			$navContainer,
			pane_count,
			$pagination = $("#paginationText"),
			slideTimer = parseInt(element.data("runspeed") * 1000) || 9000;

		self.autorun = (element.data("autorun"));
		self.playSlides = 0;

		//initialize carousel and set window behavior
		self.init = function() {
			buildCarousel();
			$(window).on("resize orientationchange", function() {
				buildCarousel();
			});
		};

		function buildCarousel() {
			//always stop autorun
			stopRotateSlides();
			currentBreakpoint = getCurrentBreakPoint();
			sliderWindowWidth = element.width();

			//need to calculate container frame width and set column widths based on viewport and expected number of columns
			var content = element.html(),
				$columns = $(columnSelector, element),
				frameWidth = adjustSliderWidth($columns);

			$sliderFrame = $('.slider-frame', element);
			content = adjustColumnWidths($columns, frameWidth);

			$sliderFrame = ($sliderFrame.length < 1) ? $("<div class=\"slider-frame\" />") : $sliderFrame;

			$sliderFrame.width(frameWidth);
			pane_count = getPaneCount();

			/*resized from smaller to larger, start over*/
			if ((pane_count * sliderWindowWidth) > frameWidth) {
				frameWidth = adjustSliderWidth($columns);
				$sliderFrame.width(frameWidth);
				pane_count = getPaneCount();
			}

			if (element.data("loaded") !== "true" && pane_count > 1) {
				$sliderFrame.html(content);
				$(element).addClass("done").html($sliderFrame).data("loaded", "true").before("<nav class=\"slider-arrows start\" id=\"sliderArrowNav\"><a class=\"slide-left\">left</a><a class=\"slide-right\">right</a></nav>");

				/* populate the pagination */
				$pagination.text("1 of " + pane_count);

				/* cache a reffrence to the nav once appneded before to be used later */
				$navContainer = $("#sliderArrowNav");
			}

			self.showPane(0, true);

			if (self.autorun) {
				setupAuto();
			}
		}

		//set up rotation interval
		function setupAuto() {
			self.playSlides = setInterval(function() {
				if (current_pane === (pane_count - 1)) {
					self.showPane(0, true);
				} else {
					self.next();
				}

			}, slideTimer);
		}

		//does what it says
		function stopRotateSlides() {
			clearInterval(self.playSlides);
		}

		//takes contents and sets each column to expected column width based on fullWidth param
		function adjustColumnWidths($contents, fullWidth) {
			var i = $contents.length,
				$column,
				adjustedColumnWidth,
				columnPlacement,
				numberOfColumns,
				columnClass,
				columnProps,
				adjustedColumnWidth,
				newColumnWidth;

			i = $contents.length;
			while (i--) {
				$column = $($contents[i]);
				columnClass = getColumnClass($column);
				columnProps = columnClass.split("-");
				numberOfColumns = parseFloat(columnProps[2]);
				columnPlacement = parseFloat(columnProps[1]);
				adjustedColumnWidth = sliderWindowWidth / numberOfColumns;
				newColumnWidth = adjustedColumnWidth * columnPlacement;
				$column.css({
					"width": newColumnWidth + "px"
				});
			}

			return $contents;
		}

		function adjustSliderWidth($contents) {
			var i = $contents.length,
				$column,
				fullWidth = 0;

			while (i--) {
				$column = $($contents[i]);
				fullWidth += $column.outerWidth();
			}
			return fullWidth + 5; /*adding 5 to account for box model padding issue*/
		}

		//find currently applied class based on breakpoint
		function getColumnClass($column) {
			var classNames = $column.attr("class").split(" "),
				selectedClass,
				defaultClass,
				classSuffix,
				i = classNames.length;
			while (i--) {
				if (classNames[i].indexOf(currentBreakpoint) > -1) {
					//found one with current break point, time to stop
					return classNames[i];
				} else if (classNames[i].indexOf("col-") > -1) {
					//doesn't have breakpoint, look for default class
					selectedClass = classNames[i];
					classSuffix = classNames[i].slice(-2);
					if (!isNaN(parseInt(classSuffix))) {
						defaultClass = selectedClass;
					}
				}
			}
			if (typeof defaultClass != "undefined") {

				selectedClass = defaultClass;
			}

			return selectedClass;
		}

		this.next = function() {
			return this.showPane(current_pane + 1, true);
		};
		this.prev = function() {
			return this.showPane(current_pane - 1, true);
		};

		function getPaneCount() {
			var sliderFrameWidth = $sliderFrame.width(),
				/*number of frames filled with content*/
				fullFrameCount = Math.floor(sliderFrameWidth / sliderWindowWidth),

				/*get count of frames period*/
				frameCount = sliderFrameWidth / sliderWindowWidth,
				frameCountRoundedUp = Math.ceil(frameCount),
				frameCountRoundedDown = Math.floor(frameCount),
				frameCountDiff = frameCount - frameCountRoundedDown,

				finalFrameCount = (frameCountDiff >= 0.015) ? frameCountRoundedUp : frameCountRoundedDown;

			/*set percentage for last frame*/
			self.lastFrameSize = fullFrameCount - frameCount;

			return finalFrameCount;
		}

		/**
		 * show pane by index
		 * @param   {Number}    index
		 */
		this.showPane = function(index) {
			var offset;

			$navContainer.removeClass("start").removeClass("end");

			// between the bounds
			index = Math.max(0, Math.min(index, pane_count - 1));

			current_pane = index;

			/* Update Pagination */
			$pagination.text( (current_pane + 1) + " of " + pane_count);

			offset = -((100 / pane_count) * current_pane);

			if (index === 0) {
				$navContainer.addClass("start");

			} else if (index + 1 === pane_count) {
				$navContainer.addClass("end");
			}

			setContainerOffset(offset, true);
		};



		function setContainerOffset(percent, animate) {
			var px;

			$sliderFrame.removeClass("animate");

			if (animate) { $sliderFrame.addClass("animate"); }

			px = ((sliderWindowWidth * pane_count) / 100) * percent;

			$sliderFrame.css("left", px + "px");

		}

		//hammer event handler
		function handleHammer(ev) {
			// disable browser scrolling
			ev.gesture.preventDefault();
			stopRotateSlides();

			switch (ev.type) {
				case 'dragright':
				case 'dragleft':
					// stick to the finger
					var pane_offset = -(100 / pane_count) * current_pane;
					var drag_offset = ((100 / sliderWindowWidth) * ev.gesture.deltaX) / pane_count;

					// slow down at the first and last pane
					if ((current_pane === 0 && ev.gesture.direction === Hammer.DIRECTION_RIGHT) ||
						(current_pane === pane_count - 1 && ev.gesture.direction === Hammer.DIRECTION_LEFT)) {
						drag_offset *= 0.4;
					}

					setContainerOffset(drag_offset + pane_offset);
					break;

				case 'swipeleft':
					self.next();
					ev.gesture.stopDetect();
					break;

				case 'swiperight':
					self.prev();
					ev.gesture.stopDetect();
					break;

				case 'release':
					// more then 50% moved, navigate
					if (Math.abs(ev.gesture.deltaX) > sliderWindowWidth / 2) {
						if (ev.gesture.direction === 'right') {
							self.prev();
						} else {
							self.next();
						}
					} else {
						self.showPane(current_pane, true);
					}
					break;
			}
		}

		if (THD.Utils.GestureHandler.touch) {
			element.hammer({
					drag_lock_to_axis: true
				})
				.on("release dragleft dragright swipeleft swiperight", handleHammer);
		}

		$(document).hammer().on('tap, click', '.slide-left', function() {
			stopRotateSlides();
			self.prev();
		});

		$(document).hammer().on('tap, click', '.slide-right', function() {
			stopRotateSlides();
			self.next();
		});
	}

	$(window).on("load", build);

	//public
	slider.init = function() {
		build();
	};

	slider.refresh = function(){
		initSliders();
	}

	return slider;
}(
	THD.Utility.Namespace.createNamespace('THD.Widget.Slider'),
	jQuery,
	window
));