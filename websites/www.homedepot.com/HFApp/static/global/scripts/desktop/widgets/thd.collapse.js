/* =============================================================
 * thd-collapse.js v1.0
 * thd collapse is a really simple accordian plugin
 *
 * Markup
 ********************************
 * Just add {data-toggle="collapse"} and a {data-target} to element to automatically assign control of a collapsible element. 
 * The data-target attribute accepts a css selector to apply the collapse to. 
 * Be sure to add the class {collapse} to the collapsible element. 
 * If you'd like it to default open, add the additional class "in".
 * =============================================================*/

THD.Widget.collapse = {};

!function ($, THD) {
	"use strict";

	var Collapse = function(element, options) {
		this.$element = $(element);
		this.options = $.extend({}, $.fn.collapse.defaults, options);

		if (this.options["parent"]) {
			this.$parent = $(this.options["parent"]);
		}

		this.options.toggle && this.toggle();
	};

	Collapse.prototype = {
		constructor : Collapse,

		dimension : function() {
			var hasWidth = this.$element.hasClass('width');
			return (hasWidth) ? 'width' : 'height';
		},

		show : function() {
			var dimension = this.dimension(),
				scroll = $.camelCase([ 'scroll', dimension ].join('-')),
				actives = this.$parent && this.$parent.find('.in'),
				hasData;

			if (actives && actives.length) {
				hasData = actives.data('collapse');
				actives.collapse('hide');
				hasData || actives.data('collapse', null);
			}

			this.$element[dimension](0);
			this.transition('addClass', 'show', 'shown');
			this.$element[dimension](this.$element[0][scroll]);

		},

		hide : function() {
			var dimension = this.dimension();

			this.reset(this.$element[dimension]());
			this.transition('removeClass', 'hide', 'hidden');
			this.$element[dimension](0);
		},

		reset : function(size) {
			var dimension = this.dimension();

			this.$element.removeClass('collapse')[dimension](size || 'auto')[0].offsetWidth;
			this.$element[size ? 'addClass' : 'removeClass']('collapse');

			return this;
		},

		transition : function(method, startEvent, completeEvent) {
			var that = this,
				complete = function() {
					if (startEvent === 'show'){
						that.reset();
					}
					that.$element.trigger(completeEvent);
				};

			this.$element.trigger(startEvent)[method]('in');

			return $.support.transition && this.$element.hasClass('collapse') ? this.$element.one($.support.transition.end, complete) : complete();
		},
		
		toggle : function() {
			return this[this.$element.hasClass('in') ? 'hide' : 'show']();
		}
	};

	/* -------------------------------------------------
	 * COLLAPSIBLE PLUGIN DEFINITION
	--------------------------------------------------*/

	$.fn.collapse = function(option) {
		return this.each(function() {
					var $this = $(this),
						data = $this.data('collapse'),
						options = (typeof option === 'object' && option);

					if (!data){ $this.data('collapse', (data = new Collapse(this, options))); }
					if (typeof option === 'string'){ data[option](); }
				});
	};

	$.fn.collapse.defaults = {
		toggle : true
	};

	$.fn.collapse.Constructor = Collapse;


	/* -----------------------------------------------------
	 * COLLAPSIBLE DATA-API - THD Style
	-------------------------------------------------------*/

	// Setting data-target for h4 and id for div at run time.
	THD.Widget.collapse.setId = function(){
		var $this, id, i,
			collapsible = $("#hfappContainer").find(".content > [data-toggle=collapse]");

		//specifically looks for DCTM placed collapse containers
		for (i = collapsible.length - 1; i >= 0; i -= 1) {

			$this =  $(collapsible).eq(i);
			id = $this.data("id");

			$this.siblings(".collapse").attr("id", id).end().data("target", "#"+id);

			//To accommodate the space between left & right panes not in the left rail
			if(!$this.parents().hasClass("rail")){ $this.parent("div.content").addClass("addSpace"); }
		}

	}();


	//click event on a data-toggle=collapse taged element
	function toggleContainerData(e) {
		e.preventDefault();
		var $this = $(this), href,
			target = $this.data('target') || e.preventDefault() || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, ''), // strip for ie7
			option = $(target).data('collapse') ? 'toggle' : $this.data();

		$(target).collapse(option);

		$this.toggleClass('on');
	}


	//checks for a hash value on page load to automatically open the coresponding content aera
	function checkForHash(){
		var hash = window.location.hash;
		if(Boolean(hash)){
			$(hash).collapse("show");
			$(hash).siblings("[data-toggle=collapse]").addClass("on");
		}
	}


	$(document)
		.on("click.collapse.data-api", "[data-toggle=collapse]", toggleContainerData)
		.ready(checkForHash);

}(window.jQuery, window.THD);