//exr4660 121313
$(document).ready(function(){
	var $sharewidget = $('.sharewidget');
	  if ( $sharewidget.length !== 0){
		//alert($sharewidget.length);
		 getSocialMediaWidget();
	  }
});

function getSocialMediaWidget(){
	var socialMedia = $(".sharewidget").attr("data-socialmedia");
	var n = socialMedia.split(",");

	for (var i = 0; i < n.length; i++){
		if (n[i] == "facebook"){
			$(".sharewidget").append("<span class='dwarf-facebookShareIcon'/>");
			
			/* Delegate event on Facebook click */
			$('body').delegate(".dwarf-facebookShareIcon", "click", function(event) {
				event.preventDefault();
				openNewWindow('https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(location.href), 'facebook_share_dialog', 'width=626,height=436');
			});
		}else if(n[i] == "twitter"){
			var twitterLink =  '<div class="twitter">'+'<a href="https://twitter.com/share" class="twitter-share-button" data-dnt="true" data-count="none" data-via="text here">Tweet</a>'+
			'<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="../../../../../../../platform.twitter.com/widgets.js"/*tpa=https://platform.twitter.com/widgets.js*/;fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>'+'</div>';
			$(".sharewidget").append(twitterLink);
		}else if(n[i] == "pinterest"){
			$(".sharewidget").append('<span class="pinit dwarf-pinterestShareIcon"/>');
				
			/* Delegate event on Pinterest click */
			$('body').delegate(".dwarf-pinterestShareIcon", "click", function(event){
				event.preventDefault();
				getPinitOverlay('pinterest');
			});
			
			/* Delegate event on Image click from Pinterest overlay */
			$('body').delegate(".pinthis-imageOuter_32x32", "click", function(event) {
				var pinterestUrl = 'http://pinterest.com/pin/create/button/?url=' + encodeURIComponent(document.location.href) + 
					'&amp;media='+$(this).find('img').attr('src')+'&amp;description='+encodeURIComponent(document.title);
				openNewWindow(pinterestUrl,'pinterest_share_dialog','width=626,height=436');
			});
			
			/* Delegate event on Image hover from Pinterest overlay */
			$('body').delegate(".pinthis-imageOuter_32x32", "mouseenter", function(event) {
				$(this).find('.pinterest_icon').show();
				$(this).find(".pinterestEqualSizeImage").addClass('pinterestEqualSizeImageHover');
			}).delegate(".pinthis-imageOuter_32x32", "mouseleave", function(event) {
				$(this).find('.pinterest_icon').hide();
				$(this).find(".pinterestEqualSizeImage").removeClass('pinterestEqualSizeImageHover');
			});
		}else if(n[i] == "google+"){
			$(".sharewidget").append('<div class="gplus">'+'<a href="#" class="thdGooglePlusIcon">'+'<img src="https://www.gstatic.com/images/icons/gplus-64.png" alt="Share on Google+" style="height:16px;margin-top:1px;"/>'+'</a>');
			
			/* Delegate event on GooglePlus click */
			$('body').delegate(".thdGooglePlusIcon", "click", function(event) {
				event.preventDefault();
				openNewWindow('https://plus.google.com/share?url={URL}','gplus_share_dialog','width=626,height=436');
			});
		}
	}
}

function getPinitOverlay( media ){
	
	if(media === "pinterest" ){
		pinImage("pinterest");
		$.fancybox({
			'content' : $('#pinterest_overlay').html(),
			'width'         : 640,
			'height'        : 510,
			'margin': 0,
			'padding': 0,
			'autoDimensions'    : false,
			'autoSize': false,
			'scrolling': 'no',
			'centerOnScroll': true,
			'enableEscapeButton': true,
			'modal': false,
			'overlayColor': '#666',
			'overlayOpacity': 0.7,
			"showCloseButton": true
		});
	}
}

function pinImage(media){
	if(media === "pinterest"){
		
		$('#pinterest_overlay').remove();
		$("body").append("<div id='pinterest_overlay'></div>");
		
		var header = '<div id="pinterest_header"><span class="dwarf-pinterestIcon small-icon"></span> <h3 class="pinterest_headermsg">Pin It to Pinterest</h3>' + 
				'<a class="pinterest_headerclose" href="#" title="Close"></a></div>';
		$('#pinterest_overlay').hide().append(header);
		$('#pinterest_overlay').append("<div id='pinterest_body'></div>");
		$('#pinterest_overlay').append("<div id='pinterest_footer'></div>");
		
		var pinImg = "";		
		$('img').each(function(){
			var imageWidth = $(this).width();
			var imageHeight = $(this).height();
			var pinterest_imageSizeInfo = '<span class="pinterest_imageSize">';
			if( imageWidth >= 145 && imageHeight >= 100 ) {
				var src = $(this).attr('src');
				//Check if the image source path is relative, if so form the absolute image path
				src = ( src.indexOf(".com") !== -1 ) ? src : ( document.location.host + src );
				
				if(imageHeight === 300){
					pinterest_imageSizeInfo = pinterest_imageSizeInfo + 'Preferred Image </span>';
				}else{
					pinterest_imageSizeInfo = pinterest_imageSizeInfo + imageHeight + ' x ' + imageWidth +' </span>';
				}
				pinImg = pinImg + '<span class="pinthis-imageOuter_32x32" >';
				pinImg = pinImg + '<span class="pinthis-imageInner_32x32" >' +
								'<img alt="' + (($(this).attr('alt') !== undefined ) ? $(this).attr('alt') : '') + '" class="pinterestEqualSizeImage" src="' + src + '" height="" width=""/> </a> </span><div class="pinterest_icon dwarf-pinterest"></div> '+ pinterest_imageSizeInfo +'</span>';
			}
		});
		$('#pinterest_body').append(pinImg);
	}

}

/* url param should the path to be opened in th window, 
** windowName param should be string cannot contain special characters (IE doesnot accespts hyphen & spaces),
** windowWidth param should be string, like 'width=626,height=436' */
function openNewWindow(url, windowName, windowWidth){
	window.open(url, windowName, windowWidth); 
}