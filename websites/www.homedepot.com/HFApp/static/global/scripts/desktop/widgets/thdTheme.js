
//  thdTheme - Funtion to set the page background image/s, container_30 background image and the pod background image
function thdTheme() {
    var $body = $("body"),
        pageID = $("#container").attr('data-thdtheme').split('-'),
        ext = pageID.pop(),
        lastNode = pageID.pop();
    
    // SlideBackground = false; // Defined in cartridge.js
    // Checks to see if there's a container background.
    if (lastNode === "containerBG") {
        var bgStyle = pageID.pop();
    }
    else {
        var bgStyle = lastNode;
    }

    var pageTheme = pageID.join("-"),
        pageThemeDir = pageTheme.replace(/-/g, "/");

    $body.addClass(pageTheme + ' ' + bgStyle);
    if (lastNode === "containerBG") {
        $('#container').css({'background-image': 'url(http://www.homedepot.com/hdus/en_US/DTCCOM/common/Themes/'+pageThemeDir+'/containerBG.jpg', 'background-repeat': 'no-repeat'});
    }
    $('.podBG').css('background-image', 'url(http://www.homedepot.com/hdus/en_US/DTCCOM/common/Themes/'+pageThemeDir+'/podBG.'+ext+')');

    switch (bgStyle) {
        case 'fullBGSlider':
        var heroSlider = Boolean($('.homepage .grid_30.slider').find('.hero_wrapper').length);
        if($(".tophat").children().hasClass("slider")){
            if (heroSlider){
                $(".homepage .grid_30.slider:not(':first')").addClass('bgSlider');
            }
        }
        else {
            if (heroSlider){
                $('.homepage .grid_30.slider:first').addClass('bgSlider');
            }
        }
        $(function () {
            var $Slider = $('.slider:first .slider_window'),
                $SlideCount = $Slider.find('.row').length; // Finds the amount of slides
            $body.css('background', 'none');
            // Loop to append background images to be used for the background slider
            for (var i = 1; i <= $SlideCount; i++){
                var $HeroBG = '<img class="iefullBG" src="http://www.homedepot.com/hdus/en_US/DTCCOM/common/Themes/'+pageThemeDir+'/bgSlide'+i+'.jpg" rel="'+i+'">';
                $body.append($HeroBG);
            }
            $('.iefullBG[rel="1"]').fadeIn(1000);
        });
        break;
        case 'fullBG': 
        $(function () {
            $body.css('background', 'none').append('<img class="iefullBG" src="http://www.homedepot.com/hdus/en_US/DTCCOM/common/Themes/'+pageThemeDir+'/bg.jpg">');
            $('.iefullBG').fadeIn(1000);   
        });
        break; 
        case 'tileBG': 
        $(function () {
            $body.css('background', 'url(http://www.homedepot.com/hdus/en_US/DTCCOM/common/Themes/'+pageThemeDir+'/bg.jpg) repeat fixed left top transparent');
        });
        break;
        case 'fullTC': 
        $(function () {
            $body.css('background', 'url(http://www.homedepot.com/hdus/en_US/DTCCOM/common/Themes/'+pageThemeDir+'/bg.jpg) no-repeat center top transparent');
        });
        break;
    }
}

thdTheme();

var $hasSlider = Boolean($('.slider').length);
if ($hasSlider) { 
    THDModuleLoader.$includeModule('thdSlider'); 
}