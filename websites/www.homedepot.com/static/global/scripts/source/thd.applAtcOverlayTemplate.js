var applAtcOverlayTemplate = {};

(function(applAtcOverlayTemplate){

	applAtcOverlayTemplate.templates = {
	'applAtcTemplate' : '<div id="applATCOverlay" class="grid_21" >'+
						  /* Added to Cart header starts*/ 
							'<div class="applmodelheader xlarge b btn-icon hide">'+
								'<i class="icon-small-green-check"></i>Added to Cart'+
							'</div>'+ /* Added to Cart header ends*/
							
							/*Header Section starts*/ 
							'<div id="applATCHeader">'+
								'{{#products.product.skus.sku.info}}'+
									'<div class="product-details "> <span class = "prod-name b georgia ">{{brandName}} </span>' + 
									'|'+
									'<span class=" model-number georgia  ">Model # {{modelNumber}}</span></div>'+
									'<div class="product-info xlarge b"> {{productLabel}} </div>'+
								'{{/products.product.skus.sku.info}}'+	
							'</div>'+/*Header Section ends*/
							
							/*Product Image - Column 1 starts*/
							'<div id="applATCOverlayCol1" class="grid_4">'+
								'{{#products.product.skus.sku.media}}'+
									'<img border="0" src="">'+
								'{{/products.product.skus.sku.media}}'+	
							'</div>'+ /*Product Image - Column 1 ends*/
							
							/* Product Info - Column 2 starts*/
							'<div id="applATCOverlayCol2" class="grid_8">'+
								'{{#products.product.skus.sku}}'+
									'<div class="prod-price">'+
										'{{#products.product.skus.sku.storeSkus.storeSku.pricing}}'+
											'<div> Was <span class="item_stike_price"> ${{originalPrice}} </span><span class="normal savings_grid">Save {{percentageOff}}%</span> </div>'+
											'<div class="xlarge item_price">${{specialPrice}} </div>'+
										'{{/products.product.skus.sku.storeSkus.storeSku.pricing}}'+
									'</div>'+
											
									/* Promotion and Savings section*/		 
									'<div class="appl-promo-details hide">'+
										'<div class="display-promo-sec ">'+
											'<div class="promo-sec-item">Cart (<span class=qty-total>1 item</span> )</div>'+
											'<div class="promo-sec-item">$ <span>0,000.00</span></div>'+
										'</div>'+
										'<div class="appl-promo-green">'+
											'{{#promotions.promotionEntry}}'+
												'<div class="grid_5 item-savePrice allCaps b">{{promoType}}</div>'+
												'<div class="grid_5 item-savePrice allCaps b">$<span>00.00</span></div>'+
											'{{/promotions.promotionEntry}}'+
										'</div>'+
									'</div>'+
																			
									'<div class="delivery-details b">'+
										'{{#shipping.freeShippingMessage}}'+
											'<div><span class="item-savePrice b"></span> <span class="item-shipLbl"></span></div>'+
										'{{/shipping.freeShippingMessage}}'+
									'</div>'+
								'{{/products.product.skus.sku}}'+								
							'</div>'+/* Product Info - Column 2 ends*/
							
							/* Delivery Details - Column 3 starts*/
							'<div id="applATCOverlayCol3" class="grid_8">'+
								'<div id="details-section">'+
									
									/*Check Delivery date*/
									'<div class="check-delivdate large b">'+
										'<div class="app-outofstock-msg btn-clear btn-long allCaps hide">'+
											'<i class="icon-outofstock"></i>OUT OF STOCK FOR <span id="oos_zip_code">{00000}</span>'+
										'</div>'+
										'<div class="appl-sameplace-ship large b hide">'+
											'<div class="appl-applsameorder-msg btn-clear btn-long">'+
												'<i class="icon-yellow-downarrow"></i><div class="applsameorder">All appliances must be delivered to the same ZIP code.</div>'+
											'</div>'+ 
										'</div>'+
										'<div class="appl-error normal btn-clear b btn-long hide">'+
											'<i class="icon-error"></i> <div class="errmessage">Please enter a valid ZIP code</div>'+
										'</div>'+
										'<div class="chk-earlierdate-msg normal btn-clear btn-long allCaps">'+
											'<i class="pns-applCal" style="height:33px;"></i>ENTER ZIP CODE TO CHECK AVAILABILITY'+
										'</div>'+
										'<div class="input-field btn-clear">'+
											'<input class="input-short-custom" type="text" maxlength="5" placeholder="Enter ZIP Code" name="new-zip-code">'+
											'<a class="new-zip-code-btn btn-orange btn handCursor"><span>CHECK AVAILABILITY </span></a>'+ 				
										'</div>'+				 
									'</div>'+ 
									
									/* Earliest Availability*/
									'<div class="early-availble hide">'+										
										'<div class="normal btn-clear allCaps b btn-long early-availble-msg">'+
											'<i class="icon-grey-cal"></i>Earliest Available: <span  class="normal availabledate early-date">00/00/0000</span>'+
										'</div>'+
										'<div class="normal btn-clear allCaps btn-long early-availble-msg hide">'+
											'<i class="icon-grey-cal"></i>Earliest Weekend: <span  class="normal availabledate early-weekend">00/00/0000</span>'+
										'</div>'+
									'</div>'+	
									
									/* Advisory Message*/
									'<div class="store-contact btn-clear btn-long hide">'+
										'<i class="icon-outofstock"></i> <div class="allCaps b strcontact">CONTACT YOUR STORE</div>'+
										'<div class="info-localmessage">Please contact your local store to order and arrange delivery</div>'+ 
									'</div>'+
									
									/*Backordered*/
									'<div class="app-backorder-msg btn-clear btn-long allCaps hide">'+
										'<i class="icon-outofstock"></i>ON BACKORDER until: <span id="appl-backOrd-date">00/00/0000</span>'+
									'</div>'+
				
									'<div class="appl-delivery-msg b hide">'+
										'My Delivery location: <span id="mydel-location"> 00000 </span> <a class="thdOrange normal" href="#">(change)</a>'+
									'</div>'+									

									/* DD down */
									'<div class="delivery-date large b hide">'+
										 '<div class="appl-backord-msg btn-clear btn-long">'+
											'<i class="icon-outofstock"></i> <div class="backorder">Delivery dates are currently unavailable for review. You can still continue shopping and check availability during checkout.</div>'+
										'</div>'+
									 '</div>'+
									 
										  
										  '<div id="appl-orderitem-error-msg" class="btn-clear btn-long hide">'+
											 '<i class="icon-error"></i> <div id="appl-orderitem-error" class="errmessage"></div>'+
											 
										  '</div>'+
									 
								'</div>'+
								
								
							/*Button Section*/
								'<div id="applATCBtnSec" class=" control-group">'+ 
									'<div class="zipNotExists-Btn controls">'+
										'<a id="applATCButton" class="btn btn-orange hide" href="#"><i class="icon-plus-white"></i> ADD TO CART </a>'+
										'<a  class="addtolist_act grey-btn btn handCursor hide"><span> <i class="icon-plus-grey"></i> ADD TO MY LIST</span></a>'+ 	
										'<a class="return-cart-btn btn-orange hide btn handCursor" href=""><span>RETURN TO CART </span></a>'+
									'</div>'+ 
									
									'<div id="addToListResponse"></div>'+
									'<input type="hidden" value="quickView" name="fromPage" id="fromPage">'+
									'<input type="hidden" value="" name="productId_1" id="productId_1">'+
									'<input type="hidden" value="" name="quantity_1" id="quantity_1">'+ 
									'<input type="hidden" value="" name="orderId" id="orderId">'+
									
									'<div class="controls zipExists-Btn hide">'+
										'<a class="btn btn-orange cont-btn" href="#">SELECT PARTS </a>'+
										'<button class="btn grey-btn cont-btn app-cont-gotocart"  type="submit" name="">EDIT CART </button>'+										
									'</div>'+ 
									
									'<div class="controls zipExists-Cartbtn hide">'+
										'<a class="btn btn-orange cont-btn" href="#">GO TO CART</a>'+
									'</div>'+ 
									
									'<div id="appl-ordritmadd-errbtn" class= "hide">'+
									'<a href="#" id="appl-ordritmadd-error-msg" class="btn btn-orange">EDIT CART </a>'+
									'</div>'+ 
								'</div>'+	
							
							'</div>'+/* Delivery Details - Column 3 ends*/
						
					'</div>'

	}
}(applAtcOverlayTemplate));