(function(applChangeZipCode){
	
  var changeZipcodeUrl ="",isAjaxInProgress = false,prodId='',productApiHostName ="",apiURL="",refreshCart=false,
  zipCodeValidator = function(zip) {
		var re5digit = /^\d{5}$/;
		if ((zip.length === 5) && (re5digit.test(zip))) {
		return true;
		}
		return false;
	},
	makeApiRequest = function(jsonURL,callbackFunction,dataType,errorCallbackFunction){
		$.ajax({
			url : jsonURL,

			dataType : dataType,
			timeout:dataType=='json' ? '': '15000',
			beforeSend :function(){
				if(!isAjaxInProgress){
					$.fancybox.showActivity();
					isAjaxInProgress = true;
				}else{
					return false;
				}
			},
			success : function(data){
				callbackFunction(data);
				$.fancybox.hideActivity();
				isAjaxInProgress = false;
			},
			error : function(xhRequest, ErrorText, thrownError){
				$.fancybox.hideActivity();	
				errorCallbackFunction(dataType,xhRequest,callbackFunction);
				isAjaxInProgress = false;
			}
		});
	},
	formatHostName = function(){
		url = window.location.host;
		var isPrBeta = readCookie('HD_DC') ==='beta' ? true : false;
		if (url.indexOf("www") === 0 || url.indexOf("secure2") === 0) {
			if(isPrBeta){
				productApiHostName = "http://www.homedepot.com/static/global/scripts/source/-beta.homedepot.com";
			}
			else{
				productApiHostName = ".homedepot.com";
			}
	 	}
		else if (url.indexOf("secure") === 0 || url.indexOf("hd") === 0) {
			productApiHostName = url.replace("secure.", "").replace("hd", "");
		}
	},
	formatDate = function (timestamp){
		if(timestamp.length>=10){
			return timestamp.substring(5,7)+"/"+timestamp.substring(8,10)+"/"+timestamp.substring(0,4);
		}

	},
	handleErrorResponseForAtc = function(dataType,xhRequest,callbackFunction){
			if(dataType==='json'){
				if(xhRequest.responseText !=undefined){
					var errorResponse = JSON.parse(xhRequest.responseText);
					if(errorResponse!= undefined && errorResponse !=""){
						callbackFunction(errorResponse);	
					}	
				}
			}
	},
	checkAvailability = function (){
		 var zipCode=$("#applATCOverlay input[name='new-zip-code']").val();
		 if(zipCodeValidator(zipCode)){
			 $('.check-delivdate').addClass('hide');
			 $(".appl-error").addClass('hide');
			 $("#applATCOverlay input[name='new-zip-code']").removeClass('error');
			 if (document.createElement("input").placeholder == undefined) {
				$("#details-section label.labelRemove").removeClass('labelerror error');
			 }
			 url = window.location.protocol+"//"+window.location.host+"/wcs/resources/api/v1/tools/deliveryAvailability?itemId="+prodId+"&zipCode="+zipCode+"&type=JSON&key=XZG1XWUGO90KKnt6Jb9Mc8Jce5Nb8Adj";
			 makeApiRequest(url,handleDDRespChangeZip,'json',handleErrorResponseForAtc);
			}
		 else{
			 invalidZipCode();
		 }
   },
   invalidZipCode = function (){
	   	$("#applATCOverlay input[name='new-zip-code']").val('');
	   	$(".appl-error").removeClass('hide unavailChangeZip');		 
		$(".appl-error .errmessage").text("Please enter a valid ZIP code");
		$("#applATCOverlay input[name='new-zip-code']").addClass('labelerror error');
		if (document.createElement("input").placeholder == undefined) {
		   var $placeHolder = $("#details-section label.labelRemove");
		   $placeHolder.addClass('labelerror error');
		   $placeHolder.css("text-indent","0px");
		}
   },   
   unavailableDDResponse = function(){
	   var errorMessage = "This item is unavailable for delivery to "+$("#applATCOverlay input[name='new-zip-code']").val()+". Please try another ZIP code.";
	   invalidZipCode();
	   $(".appl-error").addClass('unavailChangeZip');
	   $('.check-delivdate').removeClass('hide');
	   $(".appl-error .errmessage").text(errorMessage);
	   
   },   
   handleDDRespChangeZip = function(data){
		var statusCode, serverErrorCode = "",etaDate="",avlDates="";
		if(data.deliveryAvailabilityResponse.deliveryAvailability !== undefined && data.deliveryAvailabilityResponse.deliveryAvailability.availability.status){
			statusCode = data.deliveryAvailabilityResponse.deliveryAvailability.availability.status;
		}
		else if(data.deliveryAvailabilityResponse.errorData != undefined && data.deliveryAvailabilityResponse.errorData.errors.error) {
			serverErrorCode = data.deliveryAvailabilityResponse.errorData.errors.error.errorCode;
		}
		var updateZipAPIUrl = changeZipcodeUrl + '&zipCode='+ $("#applATCOverlay input[name='new-zip-code']").val();
		//var updateZipAPIUrl = changeZipcodeUrl ;
		if(statusCode === "AVAILABLE"){
			//invoke second call
			avlDates = data.deliveryAvailabilityResponse.deliveryAvailability.deliveryInfo;
			if($.isArray(avlDates)){
				etaDate =formatDate(avlDates[0].date);
			}
			else{
				etaDate =formatDate(avlDates.date);
			}	
			updateZipAPIUrl = updateZipAPIUrl+ "&applianceETA="+etaDate;
			invokeAOLChangeZipCode(updateZipAPIUrl);			
		}
		else if(statusCode === "BACK_ORDERED"){
			etaDate = formatDate(data.deliveryAvailabilityResponse.deliveryAvailability.availability.etaDate.substring(0,10));
			updateZipAPIUrl = updateZipAPIUrl+ "&applianceETA="+etaDate;
			invokeAOLChangeZipCode(updateZipAPIUrl);
		}	
		else if (serverErrorCode === "DLVRY_AVAIL_ERR_1003" || serverErrorCode === "DLVRY_AVAIL_ERR_1015" || serverErrorCode === "DLVRY_AVAIL_ERR_1016"){
			invokeAOLChangeZipCode(updateZipAPIUrl);
		}
		else if(serverErrorCode === "DLVRY_AVAIL_ERR_1000" ||  serverErrorCode === "DLVRY_AVAIL_ERR_1013" || serverErrorCode === "DLVRY_AVAIL_ERR_1014" ){
			updateZipAPIUrl =updateZipAPIUrl + '&ddDown=true';
			invokeAOLChangeZipCode(updateZipAPIUrl);
		}
		else{
			// error view
			unavailableDDResponse();
		}		
	},
	invokeAOLChangeZipCode= function(updateZipAPIUrl){
		 isAjaxInProgress = false;
		 makeApiRequest(updateZipAPIUrl,handleAOLChangeZipCodeResp,'html',handleAOLChangeZipCodeRespErr);
	},
	handleAOLChangeZipCodeResp = function(data){
		try {
			// error case
		    errorResp = JSON.parse(data);
		    var errCode = errorResp.configuration.responseData.response.responseCode;
		    var errMsg = errorResp.configuration.responseData.response.description;
		    if(errCode==="_ERR_AOL_DD_DOWN_CHANGE_ZIPCODE"){
		    	refreshCart=true;
			    $("#appl-orderitem-error").html(errMsg);
				$('#appl-orderitem-error').addClass('ddDownChangeZip');
	    		$('#appl-orderitem-error-msg').removeClass('hide');
	    		$('.return-cart-btn').removeClass('hide');
	    		var href = window.location.protocol+'//'+window.location.host+'/webapp/wcs/stores/servlet/OrderItemDisplay?storeId=10051&langId=-1&catalogId=10053';
	    		$('.return-cart-btn').attr('href' , href);
		    }
		    else if(errCode==="_ERR_AOL_CHANGE_ZIP_CODE"){	
		    	$('#appl-orderitem-error-msg').addClass('updateErrChangeZip');
		    	$("#appl-orderitem-error").html(errMsg);
	    		$('#appl-orderitem-error-msg').removeClass('hide');
		    }
		    else if(errCode==="_AOL_DROP_SHIP_CHANGE_ZIPCODE"){
		    	$.fancybox.close();
		    	window.location.href = window.location.protocol+'//'+window.location.host+'/webapp/wcs/stores/servlet/OrderItemDisplay?storeId=10051&langId=-1&catalogId=10053';
		    }
		    
		}
		catch(e){					
			// success where in we will show config overlay
			if(THD.Global.ApplATCOverlay!=undefined){
				THD.Global.ApplATCOverlay.config.$isCustomFBClose=true;
				THD.Global.ApplATCOverlay.responseApplPSConfig(data);
			}
			else{
				$.getScript("thd.applAtcOverlay.js"/*tpa=http://www.homedepot.com/static/global/scripts/source/thd.applAtcOverlay.js*/).done(function(){
					THD.Global.ApplATCOverlay.config.$isCustomFBClose=true;
					THD.Global.ApplATCOverlay.responseApplPSConfig(data);
				});
			}
						
		}
		
	},
	handleAOLChangeZipCodeRespErr = function(dataType,xhRequest,callbackFunction){
		$('body').append('<div id="applATCOverlay" class="grid_21"></div>');	
		$('#applATCOverlay').html(xhRequest.responseText);
		if(THD.Global.ApplATCOverlay!=undefined){
			$.fancybox(THD.Global.ApplATCOverlay.overlayPSApplCartConfig);
		}
		else{
			$.getScript("thd.applAtcOverlay.js"/*tpa=http://www.homedepot.com/static/global/scripts/source/thd.applAtcOverlay.js*/).done(function(){
				$.fancybox(THD.Global.ApplATCOverlay.overlayPSApplCartConfig);
			});
			
		}
		
		
	},
	calculateItemTotal = function(){

		//Actual and was price decimial format
		var wasPriceValue = $('.prod-price .item_stike_price').text().replace('$',''), wasPriceData='', 
				prodActualPrice = $('.prod-price .item_price').text().replace('$',''), actualPriceValue = '', miniCartItem = $('#miniCartNum').text();
		
		if(wasPriceValue.trim()===prodActualPrice.trim()){
		    $('.prod-price span').parent().addClass('hide');
		}
		
		if(wasPriceValue!='' && wasPriceValue > 1){
			$('.prod-price .item_stike_price').text('$'+parseFloat(wasPriceValue).toFixed(2));
		}
		if(prodActualPrice!='' && prodActualPrice >1 ){
			$('.prod-price .item_price').text('$'+parseFloat(prodActualPrice).toFixed(2));
		}		

		//Cart Qty Items
		if(miniCartItem!==''){
		  var cMiniItemVal = parseInt(miniCartItem), cMiniItemsDesc='item';
			  if(cMiniItemVal > 1){
				cMiniItemsDesc = 'items';
			 }else{
				$(".display-promo-sec").addClass('hide');
			}
				$('#applATCOverlayCol2 .qty-total').text(cMiniItemVal+' '+ cMiniItemsDesc);
		}
		$("#applATCOverlay input[name='productId_1']").val(prodId);
	},
	placeHolderNotSupportNativeBrowser = function(){
		if (document.createElement("input").placeholder == undefined) {
		// If Placeholder is not supported
			$('.input-short-custom').attr('id','zipcodesearch');
			$('.input-short-custom').parent().append('<label class="control-label labelRemove over-apply" style="display: block;" for="zipcodesearch">Enter ZIP Code</label>');
			}
			
		$('body').on('focus','.input-short-custom',function(e){
				$("#details-section label.labelRemove").labelOver('over-apply');
		});
	},
	overlayConfig = {
			'autoDimensions': true,
			'autoSize': false,
			'autoScale':false,
			'padding':0,
			'margin':0,
			'width': 680,
			'height':620,
			'scrolling':'no',
			'overlayOpacity': 0.7,
			'showCloseButton':true,
			'centerOnScroll':true,
			'transitionIn': 'none',
			'hideOnOverlayClick': false,
			'transitionOut': 'none',
			'overlayColor': '#000',
			'href' : '#applATCOverlay',
			'type': 'inline',  
			 "onComplete": function() {
					var $applATCOverlay = $('#applATCOverlay');
					$applATCOverlay.on('click','.new-zip-code-btn',function(e){
						 checkAvailability();
						});
					$applATCOverlay.on('keypress','input[name="new-zip-code"]',function(e){
						if (e.which == 13) {
						 checkAvailability();
						}
					}); 
				},
			'onClosed' : function(){	
				if(refreshCart){
					refreshCart=false;
					window.location.href = window.location.protocol+'//'+window.location.host+'/webapp/wcs/stores/servlet/OrderItemDisplay?storeId=10051&langId=-1&catalogId=10053';
				}
			}
	};
	buildZipCodeOverlay = function(data){
			if($('#applATCOverlayCntr').length === 0){
				$('<div id="applATCOverlayCntr"  style="display:none;"></div>').appendTo('body');
			}
			mustacheToHtml(applAtcOverlayTemplate.templates.applAtcTemplate,data,'#applATCOverlayCntr');
			$.fancybox(overlayConfig);
			placeHolderNotSupportNativeBrowser();
			$('#applATCOverlayCol3').addClass('applATCOverlayCol3vline');
			var reqMedia = data.products.product.skus.sku.media.mediaEntry;
			 for(var key =0;key<reqMedia.length;key++){
				if (reqMedia.hasOwnProperty(key) ){
					if(reqMedia[key].height=== 100 && reqMedia[key].mediaType==="IMAGE"){
						var srcUrl = reqMedia[key].location;
						$('#applATCOverlayCol1 img').attr('src' , srcUrl);
					}
				}
			}
			
			var shipLbl = data.products.product.skus.sku.shipping.freeShippingMessage;
			var shipLblArr = shipLbl.split(' ');
			$('.delivery-details .item-savePrice').html(shipLblArr.splice(0,1));
			$('.delivery-details .item-shipLbl').html(shipLblArr.splice(0,shipLblArr.length).join(' '));
			
			calculateItemTotal();
				//trigger analytics on overlay open
				_hddata["AJAX"]="formSubmit";
				_hddata["overlayType"]="appliance availability";
				_hddata["pageName"]="appliance>view product availability";
				_hddata["productID"]=prodId;
				if (_hddata['fulfillmentMethod1'] !== undefined) {
					delete _hddata['fulfillmentMethod1'];
				}
				if(_hddata["availability"]!== undefined){
					delete _hddata["availability"];
				}
				if(_hddata["errorMessage"]!== undefined){
					delete _hddata["errorMessage"];
				}
				if(_hddata["appDeliveryZip"]!== undefined){
					delete _hddata["appDeliveryZip"];
				}
				if(window.hddataReady){window.hddataReady("appliance availability");} ishddataReady = true;
		},
	mustacheToHtml = function(template,data,targetElem){
		var htmlCnt = Mustache.render(template,data);
		$(targetElem).html(htmlCnt);
	};
	
	applChangeZipCode.initApplChangeZipCodeOverlay = function (thisbtn){
    	if(typeof(applAtcOverlayTemplate)==='undefined'){
    		formatHostName();
    		if($("head").find("link[href*='Unknown_83_filename'/*tpa=http://www.homedepot.com/static/global/scripts/source/thd.applAtcOverlay.css*/]").length === 0){
    			THDModuleLoader.$includeCSS("../../styles/source/thd.applAtcOverlay.css"/*tpa=http://www.homedepot.com/static/global/styles/source/thd.applAtcOverlay.css*/);
    		}
    		$.getScript("thd.applAtcOverlayTemplate.js"/*tpa=http://www.homedepot.com/static/global/scripts/source/thd.applAtcOverlayTemplate.js*/).done(function(){applChangeZipCode.triggerChangeZipCodeOverlay(thisbtn);
			});
    	}
    	else{
    		applChangeZipCode.triggerChangeZipCodeOverlay(thisbtn);
    	}
    };
    applChangeZipCode.triggerChangeZipCodeOverlay = function (thisbtn){
    	prodId = thisbtn.attr("data-prodid");
		changeZipcodeUrl = thisbtn.attr("changezipcodeurl");	
        apiURL =   window.location.protocol+"//origin.api"+productApiHostName+"/ProductInfo/v2/products/sku?itemId="+prodId+"&type=JSON&show=info,media,shipping,promotions,pricing&key=XZG1XWUGO90KKnt6Jb9Mc8Jce5Nb8Adj";
    	makeApiRequest(apiURL,buildZipCodeOverlay,'jsonp',handleErrorResponseForAtc);

	};	
})(THD.Utility.Namespace.createNamespace('THD.Global.ApplChangeZipCode'));
