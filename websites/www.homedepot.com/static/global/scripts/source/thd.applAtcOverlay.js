/*
 * Functionalities related to zipCodeOverlay for appliance items
 *
 */
(function(applATCOverlay){
	
	var productIdListAdd =[],potentialRevenueListAdd=[],productIdListDelete=[],potentialRevenueListDelete=[],zipCodePreFillVal="",directATCFlow=false,directATCFlowErrMode="",checkAvailRes={},
   zipCodeValidator = function(zip) {
		var re5digit = /^\d{5}$/;
		if ((zip.length === 5) && (re5digit.test(zip))) {
		return true;
		}
		return false;
	},
	handleCheckAvailabilityResponse = function(data){
	
	 var isServerMessage =false;
		if(data.deliveryAvailabilityResponse.errorData != undefined && data.deliveryAvailabilityResponse.errorData.errors.error) {
			var isServerErrorCode = data.deliveryAvailabilityResponse.errorData.errors.error.errorCode;
	         isServerMessage = isServerErrorApplianceService(isServerErrorCode);
		}
        if(!isServerMessage){
			if(!$('.appl-sameplace-ship').hasClass('hide')){
				$('.appl-sameplace-ship').addClass('hide');
				$('.chk-earlierdate-msg').removeClass('hide');
				$('.input-short-custom').removeAttr('disabled');
			}
        	var availabilityStatus = data.deliveryAvailabilityResponse.deliveryAvailability.availability.status;
			if(availabilityStatus=== "BACK_ORDERED"){
				if(!directATCFlow){
					$("#applATCButton").removeClass('hide');
					$("#applATCOverlay .addtolist_act").removeClass('hide');
					$('.app-backorder-msg').removeClass('hide');
					$('.appl-delivery-msg').removeClass('hide');
					var backOrderDateTimestmp=formatDate(data.deliveryAvailabilityResponse.deliveryAvailability.availability.etaDate.substring(0,10));
					$('#appl-backOrd-date').text(backOrderDateTimestmp);
					$('#mydel-location').text(data.deliveryAvailabilityResponse.deliveryAvailability.zipCode);
					applATCOverlay.config.delvDate =backOrderDateTimestmp;
				    applATCOverlay.config.zipCode =data.deliveryAvailabilityResponse.deliveryAvailability.zipCode;
				    overlayAnalyticsDDErr($('.app-backorder-msg').text());
				}
				else{
					applATCOverlay.config.isAjaxInProgress = false;
					if(directATCFlow && $("#applATCOverlay input[name='new-zip-code']").length===0){
						applATCOverlay.config.zipCode=zipCodePreFillVal;
					}
					else{
						applATCOverlay.config.zipCode =$("#applATCOverlay input[name='new-zip-code']").val();	
						zipCodePreFillVal = $("#applATCOverlay input[name='new-zip-code']").val();						
					}
					applATCOverlay.config.delvDate =formatDate(data.deliveryAvailabilityResponse.deliveryAvailability.availability.etaDate.substring(0,10));
					var apiURL = generateOrderItemAddUrl("BO");					
			    	 makeApiRequest(apiURL,handleATCResponse,'html',handleErrorResponseForAtc); 			    	 
				}
				
			}
			else if(availabilityStatus=== "AVAILABLE") {
				var avlDates = data.deliveryAvailabilityResponse.deliveryAvailability.deliveryInfo;
				if($.isArray(avlDates)){
					applATCOverlay.config.delvDate =formatDate(avlDates[0].date);
				}
				else{
					applATCOverlay.config.delvDate =formatDate(avlDates.date);
				}
				if(directATCFlow && $("#applATCOverlay input[name='new-zip-code']").length===0){
					applATCOverlay.config.zipCode=zipCodePreFillVal;
				}
				else{
					applATCOverlay.config.zipCode =$("#applATCOverlay input[name='new-zip-code']").val();	
					zipCodePreFillVal = $("#applATCOverlay input[name='new-zip-code']").val();
				}	
		    	var apiURL = generateOrderItemAddUrl("AVL");
		    	applATCOverlay.config.isAjaxInProgress=false;
	    		makeApiRequest(apiURL,handleATCResponse,'html',handleErrorResponseForAtc); 
		    }
			else if(availabilityStatus=== "OOS_ALT_MODEL" || availabilityStatus==="OOS_ETA_UNAVAILABLE") {
				zipCodePreFillVal=data.deliveryAvailabilityResponse.deliveryAvailability.zipCode;
				if(!directATCFlow || applATCOverlay.config.currentState==="applZipCodeOverlay"){
					 $('.check-delivdate').removeClass('hide');
					 $('.app-outofstock-msg').removeClass('hide');
					 $('#oos_zip_code').text(data.deliveryAvailabilityResponse.deliveryAvailability.zipCode);
					 $('.zipExists-Btn').addClass('hide');
					 $("#applATCButton").addClass('hide');
					 $("#applATCOverlay .addtolist_act").addClass('hide');
					 overlayAnalyticsDDErr($(".app-outofstock-msg").text());	
				}
				else{
					 directATCFlowErrMode="OOS_ALT_MODEL";
					 applATCOverlay.config.isAjaxInProgress = false;
					 applATCOverlay.config.currentState ="applZipCodeOverlay";
					 var apiURL =   window.location.protocol+"//origin.api"+applATCOverlay.config.hostName+"/ProductInfo/v2/products/sku?itemId="+applATCOverlay.config.prodId+"&type=JSON&show=info,media,shipping,promotions,pricing&key=XZG1XWUGO90KKnt6Jb9Mc8Jce5Nb8Adj";
					 makeApiRequest(apiURL,buildZipCodeOverlay,'jsonp',handleErrorResponseForAtc);
				}
				
			}
			if(!directATCFlow){
				zipCodePreFillVal=$("#applATCOverlay input[name='new-zip-code']").val();
			}
        }

	},
	 isServerErrorApplianceService = function(isServerError){
        var isError = false;
        if(isServerError){
           switch(isServerError){
               case "DLVRY_AVAIL_ERR_1002":
               //serverErrorMsg="We're sorry, we could not find zipCode in your request.";
               errorView();
               isError=true;
               break;
               case "DLVRY_AVAIL_ERR_1001":
               //serverErrorMsg="We're sorry, we could not find any matches for the provided zipCode #123456.";
               errorView();
               isError=true;
               break;
               case "DLVRY_AVAIL_ERR_1012":
               //serverErrorMsg="The Home Depot does not currently deliver the item #100000541 to your zip code 12345. Please contact your local store to order and arrange delivery.";
               errorView();
               isError=true;
               break;
               case "DLVRY_AVAIL_ERR_1007":
               //serverErrorMsg="We're sorry, your zipcode #30339 does not have a valid associated primary store number.";
               errorView();
               isError=true;
               break;
               case "DLVRY_AVAIL_ERR_1000":
               //serverErrorMsg="We're sorry, we are not able to provide delivery availability details.";
               availableErrorView();
               isError=true;
               break;
               case "DLVRY_AVAIL_ERR_1011":
               //serverErrorMsg="We're sorry, provided product and zip code does not have any availability information.";
               errorView();
               isError=true;
               break;
               case "DLVRY_AVAIL_ERR_1003":
               invokeAtcForDlvryDtUnAvl();   
               isError=true;
               break;
               case "DLVRY_AVAIL_ERR_1015":
               invokeAtcForDlvryDtUnAvl();  
               isError=true;
               break;
               case "DLVRY_AVAIL_ERR_1013":
               availableErrorView();
               isError=true;
               break;
               case "DLVRY_AVAIL_ERR_1014":
               availableErrorView();
               isError=true;
               break;
               case "DLVRY_AVAIL_ERR_1016":
               invokeAtcForDlvryDtUnAvl();
               isError=true;
               break;			 
               default:
            }
       return  isError;
      }
	},
	invokeAtcForDlvryDtUnAvl = function (){
		applATCOverlay.config.isAjaxInProgress = false;
        if(directATCFlow && $("#applATCOverlay input[name='new-zip-code']").length===0){
			applATCOverlay.config.zipCode=zipCodePreFillVal;
		}
		else{
			applATCOverlay.config.zipCode =$("#applATCOverlay input[name='new-zip-code']").val();	
			zipCodePreFillVal=$("#applATCOverlay input[name='new-zip-code']").val();
		}       
        apiURL = generateOrderItemAddUrl("AVL");        
        makeApiRequest(apiURL,handleATCResponse,'html',handleErrorResponseForAtc); 
	},
	formatDate = function (timestamp){
		if(timestamp.length>=10){
			return timestamp.substring(5,7)+"/"+timestamp.substring(8,10)+"/"+timestamp.substring(0,4);
		}

	},
	orderItemAddErrorView = function(){		
		if(applATCOverlay.config.currentState ==="applZipCodeOverlay"){
			$("#appl-orderitem-error").html(applATCOverlayErrorMsg.orderItemAddErr);
			$('#appl-orderitem-error-msg').removeClass('hide');
			overlayAnalyticsATCErr(applATCOverlayErrorMsg.orderItemAddErr);
		}	
		else if (applATCOverlay.config.currentState ==="applATCOverlay"){
			applATCOverlay.config.orderItemAddErrorCode='500';
			applATCOverlay.config.orderItemAddError = true;
			applATCOverlay.config.currentState ="applZipCodeOverlay";
			var apiURL =   window.location.protocol+"//origin.api"+applATCOverlay.config.hostName+"/ProductInfo/v2/products/sku?itemId="+applATCOverlay.config.prodId+"&type=JSON&show=info,media,shipping,promotions,pricing&key=XZG1XWUGO90KKnt6Jb9Mc8Jce5Nb8Adj";
			makeApiRequest(apiURL,buildZipCodeOverlay,'jsonp',handleErrorResponseForAtc);	
		}
   },
   applATCOverlayErrorMsg = {
		   morethanTwoItemErr :"Sorry, but only two of the same appliance may be purchased per order.",
		   maxIApplItemErr    :"Sorry, but a maximum of 12 appliances may be purchased per order. To add this appliance to the cart, you must first remove another item.",
		   orderItemAddErr :"Sorry, but we were unable to add this item to the cart due to a system error.<br><br> Please try again.",
		   pnsDownErrMsg   :"Parts and services are currently unavailable. You can continue shopping and edit your options from the shopping cart."
   },
   overlayAnalyticsDDErr = function(errorMessage){
		_hddata["fulfillmentMethod1"]="shipped";
		_hddata["appDeliveryZip"]=$("#applATCOverlay input[name='new-zip-code']").val();
		_hddata["availability"]="unavailable";
		_hddata["errorMessage"]="appliance:"+errorMessage;
		if(window.hddataReady){window.hddataReady("error message");} ishddataReady = true;
	},
	overlayAnalyticsATCErr = function(errorMessage){
		_hddata["errorMessage"]="appliance:"+errorMessage;
		if(window.hddataReady){window.hddataReady("error message");} ishddataReady = true;
	},
	overlayAnalyticsPnsView = function (pageName,currentTab){		
		pageName = "checkout>appliance parts services>" + pageName;
		currentTab = "parts and services "+currentTab;		
		_hddata["channel"]="checkout";
		_hddata["pageName"]=pageName;
		_hddata["contentCategory"]="checkout>appliance parts services";
		_hddata["contentSubCategory"]="checkout>appliance parts services";
		_hddata["pageType"]="appliance parts and services overlay";			
		if(window.hddataReady){window.hddataReady(currentTab);} 		
	},
	overlayAnalyticsSaveParts= function (){	
		$.each(productIdListAdd, function( index, value ) {
			var cost = potentialRevenueListAdd[index],count=index+1,addtoCart = "addtocart"+count,productId ="productID"+count,
			fulfillmentMethod="fulfillmentMethod"+count ,productType= "productType"+count,
			 potentialUnits="potentialUnits"+count,potentialRevenue="potentialRevenue"+count;
			 _hddata[addtoCart]=addtoCart;
			_hddata[productId]=value;
			_hddata[fulfillmentMethod]="shipped";
			_hddata[productType]="DD_RELATED_PRODUCT";
			_hddata[potentialUnits]="1";
			_hddata[potentialRevenue]=cost;
		});		
		if(window.hddataReady){window.hddataReady("parts and service ATC")};
	},	
	overlayAnalyticsEditParts= function (){	
		_hddata = new Object();
		var countAdd =0;
		$.each(productIdListAdd, function( index, value ) {
			var cost = potentialRevenueListAdd[index],count= index+1,addtoCart = "addtocart"+count,productId ="productID"+count,
			potentialUnits="potentialUnits"+count,potentialRevenue="potentialRevenue"+count;
			_hddata[addtoCart]="cartAddition";
			_hddata[productId]=value;
			_hddata[potentialUnits]="1";	
			_hddata[potentialRevenue]=cost;	
			countAdd =count;
		});
		$.each(productIdListDelete, function( index, value ) {
			countAdd=countAdd+1;
			var cost = potentialRevenueListDelete[index],addtoCart = "addtocart"+countAdd,productId ="productID"+countAdd,
			removalUnits="removalUnits"+countAdd,removalRevenue="removalRevenue"+countAdd;
			_hddata[addtoCart]="cartRemoval";
			_hddata[productId]=value;
			_hddata[removalUnits]="1";
			_hddata[removalRevenue]=cost;
		});
		
		if(window.hddataReady){window.hddataReady("cart update")};
	},	
	makeApiRequest = function(jsonURL,callbackFunction,dataType,errorCallbackFunction){
		$.ajax({
			url : jsonURL,

			dataType : dataType,
			timeout:dataType=='json' ? '': '15000',
			beforeSend :function(){
				if(!applATCOverlay.config.isAjaxInProgress){
					$.fancybox.showActivity();
					applATCOverlay.config.isAjaxInProgress = true;
				}else{
					return false;
				}
			},
			success : function(data){
				callbackFunction(data);
				$.fancybox.hideActivity();
				applATCOverlay.config.isAjaxInProgress = false;
			},
			error : function(xhRequest, ErrorText, thrownError){
				$.fancybox.hideActivity();	
				errorCallbackFunction(dataType,xhRequest,callbackFunction);				
				applATCOverlay.config.isAjaxInProgress = false;
			}
		});
	},
	formatHostName = function(){
		url = window.location.host;
		var isPrBeta = readCookie('HD_DC') ==='beta' ? true : false;
		if (url.indexOf("www") === 0 || url.indexOf("secure2") === 0) {
			if(isPrBeta){
				url='http://www.homedepot.com/static/global/scripts/source/-beta.homedepot.com'
			}
			else{
	        url = ".homedepot.com";
			}
	 	}
		else if (url.indexOf("secure") === 0 || url.indexOf("hd") === 0) {
	        url = url.replace("secure.", "").replace("hd", "");
		}
		return url;
	},
	handleErrorResponseForAtc = function(dataType,xhRequest,callbackFunction){
		if(!applATCOverlay.config.isPnSflow){
			if(dataType==='json'){
				if(xhRequest.responseText !=undefined){
					var errorResponse = JSON.parse(xhRequest.responseText);
					if(errorResponse!= undefined && errorResponse !=""){
						callbackFunction(errorResponse);	
					}	
				}
				else{
					availableErrorView();
				}						
			}
			else if(dataType==='html' && xhRequest.status===500){
				orderItemAddErrorView();
			}
		}
		else{
			//handle parts and services error scenarios
			if(!applATCOverlay.config.$isCartPage){
				$('#details-section .delivery-date').addClass('hide');
				$('#details-section .app-backorder-msg').addClass('hide');
				var cntBtn = $("#atcSelectPartsBtn");
				cntBtn.text("CONTINUE SHOPPING");		
				cntBtn.addClass('pnsDwnErrBtn');
	    		$('.early-availble').addClass('hide');
	    		$('#pnsDwnAtc').removeClass('hide');
	    		overlayAnalyticsATCErr(applATCOverlayErrorMsg.pnsDownErrMsg);	
			}
			else{
				$('body').append('<div id="applATCOverlay" class="grid_21"></div>');	
				$('#applATCOverlay').html(xhRequest.responseText);
				$.fancybox(applATCOverlay.overlayPSApplCartConfig);
				overlayAnalyticsATCErr($('.pswarningmsg').text());	
			}
		}		
	},
	buildZipCodeOverlay = function(data){
			if($('#applATCOverlayCntr').length === 0){
				$('<div id="applATCOverlayCntr"  style="display:none;"></div>').appendTo('body');
			}
			mustacheToHtml(applAtcOverlayTemplate.templates.applAtcTemplate,data,'#applATCOverlayCntr');
			$.fancybox(overlayConfig);
			placeHolderNotSupportNativeBrowser();
			$('#applATCOverlayCol3').addClass('applATCOverlayCol3vline');
			var reqMedia = data.products.product.skus.sku.media.mediaEntry;
			 for(var key =0;key<reqMedia.length;key++){
				if (reqMedia.hasOwnProperty(key) ){
					if(reqMedia[key].height=== 100 && reqMedia[key].mediaType==="IMAGE"){
						var srcUrl = reqMedia[key].location;
						$('#applATCOverlayCol1 img').attr('src' , srcUrl);
					}
				}
			}
			
			var shipLbl = data.products.product.skus.sku.shipping.freeShippingMessage;
			var shipLblArr = shipLbl.split(' ');
			$('.delivery-details .item-savePrice').html(shipLblArr.splice(0,1));
			$('.delivery-details .item-shipLbl').html(shipLblArr.splice(0,shipLblArr.length).join(' '));
			
			calculateItemTotal();
			if(applATCOverlay.config.orderItemAddError){
				applATCOverlay.config.orderItemAddError = false;
				if(applATCOverlay.config.orderItemAddErrorCode==='_APPLIANCE_SC_MAX_ITEMS_ALLOWED'){
				   applATCOverlay.config.orderItemAddErrorCode="";
				   var href = window.location.protocol+'//'+window.location.host+'/webapp/wcs/stores/servlet/OrderItemDisplay?storeId=10051&langId=-1&catalogId=10053';
				   $('#appl-ordritmadd-error-msg').attr('href' , href);
				   $('#appl-ordritmadd-errbtn').removeClass('hide');
				   $('#appl-orderitem-error-msg').css('margin-left','-25px');
		    	   $('#appl-orderitem-error').css('width','223px');
		    	   $("#appl-orderitem-error").html(applATCOverlayErrorMsg.maxIApplItemErr);
		    	   $('#appl-orderitem-error-msg').removeClass('hide');
		    	   overlayAnalyticsATCErr(applATCOverlayErrorMsg.maxIApplItemErr);
				}
				else if(applATCOverlay.config.orderItemAddErrorCode==='500'){
					applATCOverlay.config.orderItemAddErrorCode="";
					$("#appl-orderitem-error").html(applATCOverlayErrorMsg.orderItemAddErr);
					$('#appl-orderitem-error-msg').removeClass('hide');
					overlayAnalyticsATCErr(applATCOverlayErrorMsg.orderItemAddErr);
				}
				else{
					$("#appl-orderitem-error").html(applATCOverlayErrorMsg.morethanTwoItemErr);
					$('#appl-orderitem-error-msg').css('margin-left','15px');
		    		$('#appl-orderitem-error-msg').removeClass('hide');
		    		overlayAnalyticsATCErr(applATCOverlayErrorMsg.morethanTwoItemErr);
				}
				$('.check-delivdate').addClass('hide');				
			}
			else if(directATCFlow){
				$('.check-delivdate').addClass('hide');
				 switch(directATCFlowErrMode){
	               case "OOS_ALT_MODEL":
	            	  	 $('.check-delivdate').removeClass('hide');
						 $('.app-outofstock-msg').removeClass('hide');
						 $('#oos_zip_code').text(zipCodePreFillVal);
						 $('.zipExists-Btn').addClass('hide');
						 $("#applATCButton").addClass('hide');
						 $("#applATCOverlay .addtolist_act").addClass('hide');
						 overlayAnalyticsDDErr($(".app-outofstock-msg").text());
	               break;
	               case "UN_AVAILABLE":
	            	   $('.store-contact').removeClass('hide');
	            	   $("#applATCButton").addClass('hide');
	            	   $('.appl-delivery-msg').removeClass('hide');
	            	   //applATCOverlay.config.zipCode =zipCodePreFillVal;
	            	   $('#mydel-location').text(applATCOverlay.config.zipCode);
	            	   $("#applATCOverlay .addtolist_act").removeClass('hide');
	            	   overlayAnalyticsDDErr($(".info-localmessage").text());
	               break;
	               case "ZIP_CODE_CHANGED":
						$('.input-short-custom').val(checkAvailRes.errZipCode);
						$('.input-short-custom').attr('disabled','disabled');
						$('.chk-earlierdate-msg').addClass('hide');
						$('.appl-sameplace-ship').removeClass('hide');
						$('.check-delivdate').removeClass('hide');
						$('.app-outofstock-msg').addClass('hide');
						if(!$('.app-backorder-msg').hasClass('hide') && !$('.appl-delivery-msg').hasClass('hide')){
							$('.app-backorder-msg').addClass('hide');
							$('.appl-delivery-msg').addClass('hide');
						}
	               break;	
	               default:
	            }				
			}
			else{
				//trigger analytics on overlay open
				_hddata["AJAX"]="formSubmit";
				_hddata["overlayType"]="appliance availability";
				_hddata["pageName"]="appliance>view product availability";
				//_hddata["pageType"]="appliance";
				_hddata["productID"]=applATCOverlay.config.prodId;
				if (_hddata['fulfillmentMethod1'] !== undefined) {
					delete _hddata['fulfillmentMethod1'];
				}
				if(_hddata["availability"]!== undefined){
					delete _hddata["availability"];
				}
				if(_hddata["errorMessage"]!== undefined){
					delete _hddata["errorMessage"];
				}
				if(_hddata["appDeliveryZip"]!== undefined){
					delete _hddata["appDeliveryZip"];
				}
				if(window.hddataReady){window.hddataReady("appliance availability");} ishddataReady = true;
			}
		},
	mustacheToHtml = function(template,data,targetElem){
		var htmlCnt = Mustache.render(template,data);
		$(targetElem).html(htmlCnt);
	},
	handleATCResponse = function(data)
	{
		try {
		    jsonData = JSON.parse(data);
		    var jsonResp = jsonData.cart.errorData;
		    if(jsonResp!=undefined && jsonResp.error!=undefined && jsonResp.error.errorCode==='_APPLIANCE_SC_MAX_SAME_ITEMS_ALLOWED'){
		    	if(applATCOverlay.config.currentState ==="applZipCodeOverlay"){
		    		$("#appl-orderitem-error").html(applATCOverlayErrorMsg.morethanTwoItemErr);
		    		$('#appl-orderitem-error-msg').css('margin-left','15px');
		    		$('#appl-orderitem-error-msg').removeClass('hide');
		    		overlayAnalyticsATCErr(applATCOverlayErrorMsg.morethanTwoItemErr);		    				    		
		    	}
		    	else if (applATCOverlay.config.currentState ==="applATCOverlay"){
		    		applATCOverlay.config.orderItemAddError = true;
		    		applATCOverlay.config.currentState ="applZipCodeOverlay";
		    		var apiURL =   window.location.protocol+"//origin.api"+applATCOverlay.config.hostName+"/ProductInfo/v2/products/sku?itemId="+applATCOverlay.config.prodId+"&type=JSON&show=info,media,shipping,promotions,pricing&key=XZG1XWUGO90KKnt6Jb9Mc8Jce5Nb8Adj";
		    		applATCOverlay.config.isAjaxInProgress=false;
		    		makeApiRequest(apiURL,buildZipCodeOverlay,'jsonp',handleErrorResponseForAtc);	
		    	}			
		    }
			else if(jsonResp!=undefined && jsonResp.error!=undefined && jsonResp.error.errorCode==='_APPLIANCE_SC_MAX_ITEMS_ALLOWED'){
				if(applATCOverlay.config.currentState ==="applZipCodeOverlay"){
					var href = window.location.protocol+'//'+window.location.host+'/webapp/wcs/stores/servlet/OrderItemDisplay?storeId=10051&langId=-1&catalogId=10053';
					$('#appl-ordritmadd-error-msg').attr('href' , href);
					$('#appl-ordritmadd-errbtn').removeClass('hide');
					$("#appl-orderitem-error").html(applATCOverlayErrorMsg.maxIApplItemErr);
		    		$('#appl-orderitem-error-msg').css('margin-left','-25px');
		    		$('#appl-orderitem-error').css('width','223px');
		    		$('#appl-orderitem-error-msg').removeClass('hide');
		    		overlayAnalyticsATCErr(applATCOverlayErrorMsg.maxIApplItemErr);
		    	}
		    	else if (applATCOverlay.config.currentState ==="applATCOverlay"){
		    		applATCOverlay.config.orderItemAddErrorCode=jsonResp.error.errorCode;
		    		applATCOverlay.config.orderItemAddError = true;
		    		applATCOverlay.config.currentState ="applZipCodeOverlay";
		    		var apiURL =   window.location.protocol+"//origin.api"+applATCOverlay.config.hostName+"/ProductInfo/v2/products/sku?itemId="+applATCOverlay.config.prodId+"&type=JSON&show=info,media,shipping,promotions,pricing&key=XZG1XWUGO90KKnt6Jb9Mc8Jce5Nb8Adj";
		    		applATCOverlay.config.isAjaxInProgress=false;
		    		makeApiRequest(apiURL,buildZipCodeOverlay,'jsonp',handleErrorResponseForAtc);	
		    	}					
			}
			else if(jsonResp!=undefined && jsonResp.error!=undefined && jsonResp.error.errorCode==='_APPLIANCE_SC_ZIPCODE_CHANGED'){
				if(!directATCFlow){
					var errZipCode = jsonResp.error.description;
					if(errZipCode!=''){
					  errZipCode = errZipCode.split("|");
					  $('.input-short-custom').val(errZipCode[0]);
					  $('.input-short-custom').attr('disabled','disabled');
					}
					$('.chk-earlierdate-msg').addClass('hide');
					$('.appl-sameplace-ship').removeClass('hide');
					$('.check-delivdate').removeClass('hide');
					$('.app-outofstock-msg').addClass('hide');
					if(!$('.app-backorder-msg').hasClass('hide') && !$('.appl-delivery-msg').hasClass('hide')){
						$('.app-backorder-msg').addClass('hide');
						$('.appl-delivery-msg').addClass('hide');
					}
				}
				else{
					 directATCFlowErrMode="ZIP_CODE_CHANGED";
					 var errZipCode = jsonResp.error.description;
						if(errZipCode!=''){
						  errZipCode = errZipCode.split("|");
						  checkAvailRes.errZipCode=errZipCode[0];
						}
					 applATCOverlay.config.isAjaxInProgress = false;
					 applATCOverlay.config.currentState ="applZipCodeOverlay";					 
					 var apiURL =   window.location.protocol+"//origin.api"+applATCOverlay.config.hostName+"/ProductInfo/v2/products/sku?itemId="+applATCOverlay.config.prodId+"&type=JSON&show=info,media,shipping,promotions,pricing&key=XZG1XWUGO90KKnt6Jb9Mc8Jce5Nb8Adj";
					 makeApiRequest(apiURL,buildZipCodeOverlay,'jsonp',handleErrorResponseForAtc);
					
				}
				
				overlayAnalyticsATCErr($('.appl-sameplace-ship').text());
			}

		    
		}catch(e) {
			if(applATCOverlay.config.currentState ==="applZipCodeOverlay"){
				$("#applATCOverlay").replaceWith(data);
				$minCart.refeshMiniCart();
			}
			else if (applATCOverlay.config.currentState ==="applATCOverlay"){
				$('<div id="applATCOverlayCntr"  style="display:none;"></div>').appendTo('body');
				$("#applATCOverlayCntr").html(data);
				$.fancybox(overlayConfig);
				$minCart.refeshMiniCart();
			}
			var pNsUrl = $('#atcSelectPartsBtn').attr('pnsurl');
			if(pNsUrl!=undefined && pNsUrl!=""){
				applATCOverlay.config.isPnSflow = true;	
				applATCOverlay.config.optOutConfiguration = true;
				$("#applATCOverlay").on('click','#atcSelectPartsBtn',function(e){						
							e.preventDefault();							
							makeApiRequest(pNsUrl,handlePNSResponse,'html',handleErrorResponseForAtc); 
				});
			}	

			var applServiceSwitch = $('#applATCCertonaAPIService').val();
			if(applServiceSwitch !== undefined && applServiceSwitch === "true"){
				loadApplCertona();
			}
			else{
				applATCOverlay.config.atcCertonaRePos = setInterval(waitForATCCertona, 50);
			}			
		}
		if(thd.buildProduct){
				thd.buildProduct.checkCartStatus(); 
		}
		applATCOverlay.detectBrowser();
	},
	
	waitForATCCertona = function(){
       	var certonaTimer = 0;

        if($('#ma_atcmodal_rr').is(':visible') && !$('#ma_atcmodal_rr').is(':empty')){           
			clearInterval(applATCOverlay.config.atcCertonaRePos);
            certonaTimer = 0;
            $.fancybox.resize();
			return false;
        }
		else{
			certonaTimer++;
			if(certonaTimer === 5){
                clearInterval(applATCOverlay.config.atcCertonaRePos)
				certonaTimer = 0;
				$.fancybox.resize();
				return false;
			}
		}
	},
		
	handlePNSResponse = function(data){
		/*$('.zipExists-Btn').addClass('hide');
		$('.applmodelheader').addClass('hide');
		$("#applATCOverlay").append(data);*/
		$("#applATCOverlay").html(data);
		var radioInps = $('#applATCOverlay input[type=radio]');
		thdForms.radioButtons.init(radioInps);
		thdForms.checkBoxes.init($('#applATCPnS input[type=checkbox]'));
		calaculateSubTotal();
		$("#applZipOverlay").addClass('atc-zip');
		if($('#ma_atcmodal_rr').length>0){
			$('#ma_atcmodal_rr').hide();
		}
		defaultLoadPSOverlay();
		applATCOverlay.config.selectedRqdPartsArray =[];
		applATCOverlay.config.selectedOptPartsArray =[];
		productIdListAdd =[];
		potentialRevenueListAdd=[];
		productIdListDelete=[];
		potentialRevenueListDelete=[];
		applATCOverlay.config.unSelectedItems =[];	
		applATCOverlay.config.$isPnsEditFlow = false;
		applATCOverlay.config.isHookupAvailable =$("#isHookupAvailable").length>0 ? true : false;
		if($('#radioYesBtn').find('.radio-custom').hasClass('radio-custom-active') &&!applATCOverlay.config.isHookupAvailable){
			applATCOverlay.config.selectedRqdPartsArray.push( $("#installYes").val());
			productIdListAdd.push($("#installYes").attr('data-productid'));
			potentialRevenueListAdd.push($("#installYes").attr('cost'));
		}
		$("#reqPnSSelection input[type='checkbox']").trigger("change");	
		$.fancybox.resize(); 
		//$("#applATCOverlay").replaceWith(data);
	},
    checkAvailability = function (){
		 var zipCode=$("#applATCOverlay input[name='new-zip-code']").val();
		 if(zipCodeValidator(zipCode)){
			 $('.check-delivdate').addClass('hide');
			 $(".appl-error").addClass('hide');
			 $("#applATCOverlay input[name='new-zip-code']").removeClass('error');
			 if (document.createElement("input").placeholder == undefined) {
				$("#details-section label.labelRemove").removeClass('labelerror error');
			 }
			 url = window.location.protocol+"//"+window.location.host+"/wcs/resources/api/v1/tools/deliveryAvailability?itemId="+applATCOverlay.config.prodId+"&zipCode="+zipCode+"&type=JSON&key=XZG1XWUGO90KKnt6Jb9Mc8Jce5Nb8Adj";
			 makeApiRequest(url,handleCheckAvailabilityResponse,'json',handleErrorResponseForAtc);
			}
		 else{
			 $(".appl-error").removeClass('hide');
			 if(!$('.appl-sameplace-ship').hasClass('hide')){
				$('.appl-sameplace-ship').addClass('hide');
				$('.input-short-custom').removeAttr('disabled');
				$('.chk-earlierdate-msg').removeClass('hide');
			 }
			 $(".app-outofstock-msg").addClass('hide');
			 $("#applATCOverlay input[name='new-zip-code']").addClass('labelerror error');
			 if (document.createElement("input").placeholder == undefined) {
				$("#details-section label.labelRemove").addClass('labelerror error');
			 }
			 overlayAnalyticsDDErr($(".appl-error").text());
		 }
    },
    generateOrderItemAddUrl = function(statusCode){
    	orderItemAddUrl = window.location.protocol+"//"+window.location.host+"/webapp/wcs/stores/servlet/OrderItemAdd?storeId=10051&langId=-1&catalogId=10053&"+"catEntryId="+applATCOverlay.config.prodId+"&quantity=1&contractId=2081191&addToCartConfirmation=true&nickName=APPLIANCE+DELIVERY+-+"+applATCOverlay.config.zipCode+"&zipCode="+
    	applATCOverlay.config.zipCode;
    	if(statusCode!=undefined && statusCode!=""){
    		orderItemAddUrl = orderItemAddUrl+"&statusCode="+statusCode;
    	}
    	if(applATCOverlay.config.delvDate!=undefined && applATCOverlay.config.delvDate!=""){
    		orderItemAddUrl = orderItemAddUrl+"&applianceETA="+applATCOverlay.config.delvDate;
    	}
    	return orderItemAddUrl;
    },
    defaultView = function()    {
    	$('.store-contact').addClass('hide');
    	$('.app-backorder-msg').addClass('hide');
    	$('.appl-delivery-msg ').addClass('hide');
    	$(".app-outofstock-msg").addClass('hide');
		$('.appl-sameplace-ship').addClass('hide');
    	$("#applATCButton").addClass('hide');
		$("#applATCOverlay .addtolist_act").addClass('hide');
    	$('.check-delivdate').removeClass('hide');
    },
    errorView = function() {
    	applATCOverlay.config.zipCode =$("#applATCOverlay input[name='new-zip-code']").val();
    	if(!directATCFlow || applATCOverlay.config.currentState==="applZipCodeOverlay"){
    		 $('.store-contact').removeClass('hide');
    		 $("#applATCButton").addClass('hide');
    		 $('.appl-delivery-msg').removeClass('hide');
    		//applATCOverlay.config.zipCode =$("#applATCOverlay input[name='new-zip-code']").val();
    		 $('#mydel-location').text(applATCOverlay.config.zipCode);
    		 $("#applATCOverlay .addtolist_act").removeClass('hide');
    		 overlayAnalyticsDDErr($(".info-localmessage").text());
    	}
    	else{
    		directATCFlowErrMode="UN_AVAILABLE";
			applATCOverlay.config.currentState ="applZipCodeOverlay";
			applATCOverlay.config.isAjaxInProgress = false;
			var apiURL =   window.location.protocol+"//origin.api"+applATCOverlay.config.hostName+"/ProductInfo/v2/products/sku?itemId="+applATCOverlay.config.prodId+"&type=JSON&show=info,media,shipping,promotions,pricing&key=XZG1XWUGO90KKnt6Jb9Mc8Jce5Nb8Adj";
			makeApiRequest(apiURL,buildZipCodeOverlay,'jsonp',handleErrorResponseForAtc);
    	}
    	
    },
    availableErrorView = function (){
    	applATCOverlay.config.isAjaxInProgress=false;
    	if(directATCFlow && $("#applATCOverlay input[name='new-zip-code']").length===0){
			applATCOverlay.config.zipCode=zipCodePreFillVal;
		}
		else{
			applATCOverlay.config.zipCode =$("#applATCOverlay input[name='new-zip-code']").val();	
			zipCodePreFillVal=$("#applATCOverlay input[name='new-zip-code']").val();
		}
    	var apiURL = generateOrderItemAddUrl();
		makeApiRequest(apiURL,handleATCResponse,'html',handleErrorResponseForAtc);  
	},
	calculateItemTotal = function(){

			//Actual and was price decimial format
			var wasPriceValue = $('.prod-price .item_stike_price').text().replace('$',''), wasPriceData='', 
					prodActualPrice = $('.prod-price .item_price').text().replace('$',''), actualPriceValue = '', miniCartItem = $('#miniCartNum').text();
			
			if(wasPriceValue.trim()===prodActualPrice.trim()){
			    $('.prod-price span').parent().addClass('hide');
			}
			
			if(wasPriceValue!='' && wasPriceValue > 1){
				$('.prod-price .item_stike_price').text('$'+parseFloat(wasPriceValue).toFixed(2));
			}
			if(prodActualPrice!='' && prodActualPrice >1 ){
				$('.prod-price .item_price').text('$'+parseFloat(prodActualPrice).toFixed(2));
			}		

		//Cart Qty Items
		if(miniCartItem!==''){
		  var cMiniItemVal = parseInt(miniCartItem), cMiniItemsDesc='item';
			  if(cMiniItemVal > 1){
				cMiniItemsDesc = 'items';
			 }else{
				$(".display-promo-sec").addClass('hide');
			}
				$('#applATCOverlayCol2 .qty-total').text(cMiniItemVal+' '+ cMiniItemsDesc);
	  }
		$("#applATCOverlay input[name='productId_1']").val(applATCOverlay.config.prodId);
	},
	placeHolderNotSupportNativeBrowser = function(){
		if (document.createElement("input").placeholder == undefined) {
		// If Placeholder is not supported
			$('.input-short-custom').attr('id','zipcodesearch');
			$('.input-short-custom').parent().append('<label class="control-label labelRemove over-apply" style="display: block;" for="zipcodesearch">Enter ZIP Code</label>');
			}
			
		$('body').on('focus','.input-short-custom',function(e){
				$("#details-section label.labelRemove").labelOver('over-apply');
		});
	},
	calaculateInstallCost = function(){			
		var install_price = 0.00; 
		if($('#radioYesBtn').find('.radio-custom').hasClass('radio-custom-active')){
			install_price = parseFloat($("#pns-install-price").val().replace("$",''));
		}
		return install_price;	
	},
	calaculateRqdPartsCost = function(){
		var rqd_parts_cost = 0.00;
		$('#reqPnSSelection input:checkbox:checked').map(function(i) {					
			rqd_parts_cost+= parseFloat($(this).attr('cost')); 
		});	
		return rqd_parts_cost;
	},	
	 calaculateOptnlPartsCost=function(){			
		var opt_parts_cost = 0.00
		$('#optPnSSelection input:checkbox:checked').map(function(i) {					
			opt_parts_cost+= parseFloat($(this).attr('cost')); 
		});	
		return opt_parts_cost;			
	},
	 calaculateWarrantyCost=function(){		
		var warrantyCost=0.00;
		$.each($('#protectionPlanSelection :input[type="radio"]'), function( key, value ) {
			if($(this).next().children('div.radio-custom').hasClass('radio-custom-active')){
				warrantyCost = parseFloat($(this).attr('cost'));
			}
		});
		return warrantyCost;
	},	
	 calaculateSubTotal=function(){
		var cost_for_selected_items=calaculateRqdPartsCost()+calaculateOptnlPartsCost()+calaculateWarrantyCost()+calaculateInstallCost();
		var $updatedSubtotal = parseFloat($("#itemSubtotal").val());
		$updatedSubtotal+=cost_for_selected_items;
		$updatedSubtotal=$updatedSubtotal.toFixed(2).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
		$("#applTotalValue").text('$'+ $updatedSubtotal);
	},
	 removeItemFromArray = function(value,arrayElement){
		var index= $.inArray(value,arrayElement);
		if(index>=0){
			arrayElement.splice(index,1);
		}
	},
	
	overlayConfig = {
			'autoDimensions': true,
			'autoSize': false,
			'autoScale':false,
			'padding':0,
			'margin':0,
			'width': 680,
			'height':620,
			'scrolling':'no',
			'overlayOpacity': 0.7,
			'showCloseButton':true,
			'centerOnScroll':true,
			'transitionIn': 'none',
			'hideOnOverlayClick': false,
			'transitionOut': 'none',
			'overlayColor': '#000',
			'href' : '#applATCOverlay',
			'type': 'inline',  
			 "onComplete": function() {if(applATCOverlay.config.currentState ==="applZipCodeOverlay"){
					var $applATCOverlay = $('#applATCOverlay');
					$applATCOverlay.on('click','.new-zip-code-btn',function(e){
						 checkAvailability();
						});
					$applATCOverlay.on('keypress','input[name="new-zip-code"]',function(e){
						if (e.which == 13) {
						 checkAvailability();
						}
					}); 
					$applATCOverlay.on('click','.appl-delivery-msg a',function(e){
						e.preventDefault();
						 defaultView();
						});
					$applATCOverlay.on('click','#applATCButton',function(e){
						e.preventDefault();
						$('.app-backorder-msg').addClass('hide');
				    	$('.appl-delivery-msg ').addClass('hide');
				    	$("#applATCButton").addClass('hide');
						$("#applATCOverlay .addtolist_act").addClass('hide');
						 var apiURL = generateOrderItemAddUrl("BO");
				    	 makeApiRequest(apiURL,handleATCResponse,'html',handleErrorResponseForAtc); 					 
						});					
				}},
			'onClosed' : function(){	
				if(applATCOverlay.config.optOutConfiguration){
					if(window.hddataReady){	window.hddataReady("parts and services optout");  } ;
				}
				applATCOverlay.config.isPnSflow = false;
				applATCOverlay.config.optOutConfiguration = false;
				$('#applATCOverlay').empty();
				//Cart page certona overlay page refresh on close event
				if(applATCOverlay.config.$isCartPage && applATCOverlay.config.currentState === "applATCOverlay"){
					var updateURL = $('#thdCart a').attr('href');
					THD.Checkout.Global.doubleClickUrl(updateURL);
				}

			}
	};
	 defaultLoadPSOverlay = function (){
	 
		 					if($('#applRequiredYes').hasClass('hide')){
								$('#applRequiredNo').addClass('showReqDesc');
							}else if($('#applRequiredNo').hasClass('hide')){
								$('#applRequiredYes').addClass('showReqDesc');
							}
							if($('.bc-req-parts').length === 0){
									$('#applOptionalInstr').removeClass('hide');
									$('#applRequiredNo').addClass('hide');
									$('#noInstall').addClass('hide');
									$('#optPnSSelection').removeClass('hide');
									$('#applPnSbrC ul').removeAttr('id');
									$('#applPnSbrC ul').attr('id','applbrc-2');
									var $firstBCrumb = $('.bc-opt-parts').prev(),isFirstCrumb = $firstBCrumb.hasClass('arrow-back');
									if(isFirstCrumb){$firstBCrumb.removeClass('arrow-back');}
									$('.bc-opt-parts i').removeClass('brc-brcrc arrow-back').addClass('icon-brcrc');
									$(".pns-next").addClass('optActive').removeClass('reqActive');
									overlayAnalyticsPnsView("optional parts","optional");
								}
								if($('#noInstall').length > 0){
									$('#noInstall').addClass('showReqDesc');
								}
								if($('.bc-req-parts').length === 0 && $('.bc-opt-parts').length === 0){
									$('#applPnSbrC').removeClass('reduced-2-brc').addClass('reduced-1-brc');
									$('#applOptionalInstr').addClass('hide');
									$('#protectionPlanInstr, #protectionPlanSelection').removeClass('hide');
									var $firstBCrumb = $('.bc-prot-plan').prev(),isFirstCrumb = $firstBCrumb.hasClass('arrow-back');
									if(isFirstCrumb){$firstBCrumb.removeClass('arrow-back');}
									$('#applPnSbrC ul').removeAttr('id');
									$('#applPnSbrC ul').attr('id','applbrc-3');
									$('.bc-prot-plan i').removeClass('brc-brcrc arrow-back').addClass('icon-brcrc');
									$('.pns-next').addClass('hide');
									if(applATCOverlay.config.$isCartPage){
										$('.pns-save').removeClass('hide');	
									}
									else{
										$("#applRightBtns").removeClass('hide');
										$('#subtotalStatictxt').removeClass('hide');
									}
									overlayAnalyticsPnsView("protection plan","protection");
								}
								if($('.bc-opt-parts').length === 0 && $('.bc-prot-plan').length === 0){
									$('#applPnSbrC').removeClass('reduced-2-brc').addClass('reduced-1-brc');
									var $firstBCrumb = $('.bc-req-parts').next(),isFirstCrumb = $firstBCrumb.hasClass('arrow-front');
									if(isFirstCrumb){$firstBCrumb.removeClass('arrow-front');}
									$('.pns-next').addClass('hide');
									if(applATCOverlay.config.$isCartPage){
										$('.pns-save').removeClass('hide');	
									}
									else{
										$("#applRightBtns").removeClass('hide');
										$('#subtotalStatictxt').removeClass('hide');
									}
									overlayAnalyticsPnsView("required parts and services","parts");
								}
								if($('.bc-req-parts').length === 0 && $('.bc-prot-plan').length === 0){
									$('#applPnSbrC').removeClass('reduced-2-brc').addClass('reduced-1-brc');
									$('.pns-next').addClass('hide');
									if(applATCOverlay.config.$isCartPage){
										$('.pns-save').removeClass('hide');	
									}
									else{
										$("#applRightBtns").removeClass('hide');
										$('#subtotalStatictxt').removeClass('hide');
									}
									overlayAnalyticsPnsView("optional parts","optional");
								}
								if(($('.bc-req-parts, .bc-opt-parts').length === 2 && $('.bc-prot-plan').length === 0 ) ||
								   ($('.bc-opt-parts, .bc-prot-plan').length === 2 && $('.bc-req-parts').length === 0) ||
								   ($('.bc-prot-plan, .bc-req-parts').length === 2 && $('.bc-opt-parts').length === 0))
								   {
										$('#applPnSbrC').addClass('reduced-2-brc');
										if($('.bc-prot-plan').length === 0 ){
											$('.applbrc-2 .arrow-front').removeClass('arrow-front');
										}
								}								
								if($('.bc-prot-plan, .bc-req-parts').length === 2 && $('.bc-opt-parts').length === 0){
									$('#applActionBtns .pns-next').removeClass('reqActive').addClass('optActive');
									overlayAnalyticsPnsView("required parts and services","parts");
								}
								$.each($('#protectionPlanSelection :input[type="radio"]'), function( key, value ) {
									$(this).removeAttr('name');
									$(this).attr('name','warranty_radio');									
								});
								
								if($('.bc-req-parts, .bc-opt-parts, .bc-prot-plan').length === 3){
									overlayAnalyticsPnsView("required parts and services","parts");
								}
								//binding radio button click event	
								$('body').on('click','#applATCPnS input[name=warranty_radio]',function(e){
									calaculateSubTotal();
								});
								var isErrorClose = $('#applATCPnS .err-btn-close').hasClass('excepClose');
								if(isErrorClose){
									applATCOverlay.config.$isCustomFBClose = true;
								}
								
								};
	applATCOverlay.overlayPSApplCartConfig = $.extend(true, {}, overlayConfig, {
						'type' : 'inline',
						'enableEscapeButton': true,						
						'onComplete' : function() {
							defaultLoadPSOverlay();							
							
						},
						'onClosed' : function() {
							if(applATCOverlay.config.$isCartPage && applATCOverlay.config.$isCustomFBClose){
								var updateURL = $('#thdCart a').attr('href');
								thdCheckoutGlobal.doubleClickUrl(updateURL);
							}
						}
		});
    applATCOverlay.config = {
    		prodId : "",
    		prodStatus:"",
        	delvDate:"",
        	zipCode:"",
        	hostName:"",
        	currentState:"",
        	orderItemAddError: false,
        	orderItemAddErrorCode:"",
        	isPnSflow : false,
        	$isCartPage:$('#cartContainer').length>0 ? true : false,
			$isCustomFBClose : false,
			isGoToCart :false,
			selectedRqdPartsArray:[],
			selectedOptPartsArray:[],
			unSelectedItems:[],
			$isPnsEditFlow:false,
			isAjaxInProgress:false,
			isHookupAvailable:false,
			certonaBtnClicked: false,
			optOutConfiguration:false,
			atcCertonaRePos : "",
     };
    applATCOverlay.initApplATCOverlay = function (thisbtn){

    	if(typeof(applAtcOverlayTemplate)==='undefined'){
    		var aolCookie = readCookie("THD_AOL");
        	if (aolCookie != "") {
        		var zipCode = aolCookie.split(",")[0];
        		if (zipCodeValidator(zipCode)) {    			
        			zipCodePreFillVal=zipCode;
        		}
        	}
    		applATCOverlay.config.hostName=formatHostName();
			THDModuleLoader.$includeCSS("../../styles/source/thd.applAtcOverlay.css"/*tpa=http://www.homedepot.com/static/global/styles/source/thd.applAtcOverlay.css*/);
    		$.getScript("thd.applAtcOverlayTemplate.js"/*tpa=http://www.homedepot.com/static/global/scripts/source/thd.applAtcOverlayTemplate.js*/).done(function(){applATCOverlay.triggerZipCodeOverlay(thisbtn);
			});
    	}
    	else{
    		applATCOverlay.triggerZipCodeOverlay(thisbtn);
    	}
    };
    applATCOverlay.triggerZipCodeOverlay = function (thisbtn){

    	applATCOverlay.config.prodId = thisbtn.attr("data-prodid");
    	applATCOverlay.config.prodStatus = thisbtn.attr("data-prodstatus");   
    	applATCOverlay.config.delvDate = thisbtn.attr("data-delvdate");
		applATCOverlay.config.zipCode = thisbtn.attr("data-zip");
		var changeZipcodeUrl = thisbtn.attr("changezipcodeurl");		
		//reset directATCFlow
		directATCFlow =false;
    	if(applATCOverlay.config.prodStatus !=undefined && (applATCOverlay.config.prodStatus ==="available" || applATCOverlay.config.prodStatus ==="available_service_exception" || applATCOverlay.config.prodStatus ==="UAVL"|| applATCOverlay.config.prodStatus ==="backordered"))
    		{
    		applATCOverlay.config.currentState ="applATCOverlay";
    		var apiURL = "";
    		if(applATCOverlay.config.prodStatus ==="available"){
    			 apiURL = generateOrderItemAddUrl("AVL");
    		}
    		else if(applATCOverlay.config.prodStatus ==="backordered"){
    			 apiURL = generateOrderItemAddUrl("BO");
    		}
    		else{
    			 apiURL = generateOrderItemAddUrl();
    		}
    		if(zipCodePreFillVal===undefined || zipCodePreFillVal===""){
    			zipCodePreFillVal = applATCOverlay.config.zipCode;
    		}
    		makeApiRequest(apiURL,handleATCResponse,'html',handleErrorResponseForAtc);    		
    	}
    	else if(changeZipcodeUrl!=undefined){
    		applATCOverlay.config.currentState ="applZipCodeOverlay";
        	var apiURL =   window.location.protocol+"//origin.api"+applATCOverlay.config.hostName+"/ProductInfo/v2/products/sku?itemId="+applATCOverlay.config.prodId+"&type=JSON&show=info,media,shipping,promotions,pricing&key=XZG1XWUGO90KKnt6Jb9Mc8Jce5Nb8Adj";
    		makeApiRequest(apiURL,buildZipCodeOverlay,'jsonp',handleErrorResponseForAtc);
		}
    	else if(zipCodePreFillVal!=undefined && zipCodePreFillVal!=""){
    		// calling direct ATC for future items 
    		 applATCOverlay.config.currentState ="applATCOverlay";
    		 directATCFlow = true;
    		 url = window.location.protocol+"//"+window.location.host+"/wcs/resources/api/v1/tools/deliveryAvailability?itemId="+applATCOverlay.config.prodId+"&zipCode="+zipCodePreFillVal+"&type=JSON&key=XZG1XWUGO90KKnt6Jb9Mc8Jce5Nb8Adj";
    		 makeApiRequest(url,handleCheckAvailabilityResponse,'json',handleErrorResponseForAtc);
		}
    	else{
    		applATCOverlay.config.currentState ="applZipCodeOverlay";
        	var apiURL =   window.location.protocol+"//origin.api"+applATCOverlay.config.hostName+"/ProductInfo/v2/products/sku?itemId="+applATCOverlay.config.prodId+"&type=JSON&show=info,media,shipping,promotions,pricing&key=XZG1XWUGO90KKnt6Jb9Mc8Jce5Nb8Adj";
    		makeApiRequest(apiURL,buildZipCodeOverlay,'jsonp',handleErrorResponseForAtc);
    	}

	};
	
	applATCOverlay.ajaxApplPSHTMLServiceCall = function(ajaxApplPSURL){	
		applATCOverlay.config.isPnSflow = true;
		makeApiRequest(ajaxApplPSURL, applATCOverlay.responseApplPSConfig, 'html',handleErrorResponseForAtc);
					
	};
	
	applATCOverlay.responseApplPSConfig = function($htmlresponsedata){
		if(applATCOverlay.config.$isCartPage){
			applATCOverlay.config.currentState = "";
			$('body').append('<div id="applATCOverlay" class="grid_21"></div>');			
			$('#applATCOverlay').html($htmlresponsedata);
			$.fancybox(applATCOverlay.overlayPSApplCartConfig);
			thdForms.radioButtons.init($('#applATCPnS input[type=radio]'));
			thdForms.checkBoxes.init($('#applATCPnS input[type=checkbox]'));
			applATCOverlay.config.selectedRqdPartsArray =[];
			applATCOverlay.config.selectedOptPartsArray =[];
			applATCOverlay.config.unSelectedItems =[];	
			productIdListAdd =[];
			potentialRevenueListAdd=[];
			productIdListDelete=[];
			potentialRevenueListDelete=[];
			applATCOverlay.config.$isPnsEditFlow = $('input[partorderitemid]').length>0 ? true : false;
			applATCOverlay.config.isHookupAvailable =$("#isHookupAvailable").length>0 ? true : false;
			if($('#radioYesBtn').find('.radio-custom').hasClass('radio-custom-active') && !applATCOverlay.config.$isPnsEditFlow && !applATCOverlay.config.isHookupAvailable){
				applATCOverlay.config.selectedRqdPartsArray.push( $("#installYes").val());		
				productIdListAdd.push($("#installYes").attr('data-productid'));
				potentialRevenueListAdd.push($("#installYes").attr('cost'));
			}
			$("#reqPnSSelection input[type='checkbox']").trigger("change");	
			calaculateSubTotal();		
			
		}						
	};
	
	applATCOverlay.ajaxApplPartsNServiceCall = function($ajaxServicePSJSONURL){
	'use strict';
	var $stackingURL = "", $responseCode = "",$formApplPS = $('#formPartsAndServices').serialize(), 
		$unCheckBoxPSValue = "relOid_", $unChkBoxParamsData = "",$unSelectedWarranty="",$selectedWarranty="",
		$isHookupSelected="" ;
	
	
		$.each($('#protectionPlanSelection :input[type="radio"]'), function( key, value ) {
			if($(this).attr('partorderitemid')===undefined && $(this).next().children('div.radio-custom').hasClass('radio-custom-active')){
				var $currentSelection =$(this).val();
				if($currentSelection!=undefined && $currentSelection !=""){
					$selectedWarranty = $currentSelection;
					productIdListAdd.push($(this).attr('data-productid'));
					potentialRevenueListAdd.push($(this).attr('cost'));
				}
			}	
			if($(this).attr('partorderitemid')!=undefined && $(this).attr('partorderitemid')!="" && !$(this).next().children('div.radio-custom').hasClass('radio-custom-active') )	{
				applATCOverlay.config.unSelectedItems.push($(this).attr('partorderitemid'));
				productIdListDelete.push($(this).attr('data-productid'));
				potentialRevenueListDelete.push($(this).attr('cost'));
				}
		});		
	
		$.each(applATCOverlay.config.unSelectedItems, function( key, value ) {
			$unChkBoxParamsData += $unCheckBoxPSValue + (key+1) + "=" + value + "&";
		});		
		if(applATCOverlay.config.isHookupAvailable){
			$isHookupSelected=$('#radioYesBtn').find('.radio-custom').hasClass('radio-custom-active');
		}
		$.ajax({
			url : $ajaxServicePSJSONURL,
			type: "POST",
			dataType : 'json',
			data:$formApplPS+'&required_selected='+applATCOverlay.config.selectedRqdPartsArray+'&opt_selected='+applATCOverlay.config.selectedOptPartsArray+'&'+$unChkBoxParamsData+'&esp_selected='+$selectedWarranty+'&isHookupSelected='+$isHookupSelected,
			beforeSend :function(){
				if(!applATCOverlay.config.isAjaxInProgress){
					$.fancybox.showActivity();
					applATCOverlay.config.isAjaxInProgress = true;
				}else{
					return false;
				}
			},
			success : function(jsonresponsedata){
				 if(jsonresponsedata.configuration.responseData.response !== ''){
					$stackingURL = jsonresponsedata.configuration.responseData.response.stackingURL;
					$responseCode = jsonresponsedata.configuration.responseData.response.responseCode;
				 }

					if($responseCode === "CONFIGURATION_STACKING" && $stackingURL !== undefined && $stackingURL !== "" ){
							overlayAnalyticsEditParts();
							applATCOverlay.config.$isCustomFBClose = true;
							$.ajax({
							url : window.location.protocol+"//"+window.location.host+"/webapp/wcs/stores/servlet/"+$stackingURL,
							type: "POST",
							dataType : 'html',						
							success : function(htmlresponsedata){	 
								$('#applATCOverlay').html(htmlresponsedata);
								$('#applATCPnS .err-btn-close').addClass('prevPSitemadded');
								$.fancybox.hideActivity();
								if(applATCOverlay.config.$isCartPage){
									applATCOverlay.config.currentState = "";
									$.fancybox(applATCOverlay.overlayPSApplCartConfig);
								}else{							
									$.fancybox(overlayConfig);
								}
								thdForms.radioButtons.init($('#applATCPnS input[type=radio]'));
								thdForms.checkBoxes.init($('#applATCPnS input[type=checkbox]'));
								applATCOverlay.config.selectedRqdPartsArray =[];
								applATCOverlay.config.selectedOptPartsArray =[];
								applATCOverlay.config.unSelectedItems =[];	
								productIdListAdd =[];
								potentialRevenueListAdd=[];
								productIdListDelete=[];
								potentialRevenueListDelete=[];
								applATCOverlay.config.$isPnsEditFlow = $('input[partorderitemid]').length>0 ? true : false;
								applATCOverlay.config.isHookupAvailable =$("#isHookupAvailable").length>0 ? true : false;
								if($('#radioYesBtn').find('.radio-custom').hasClass('radio-custom-active') && !applATCOverlay.config.$isPnsEditFlow && !applATCOverlay.config.isHookupAvailable){
									applATCOverlay.config.selectedRqdPartsArray.push( $("#installYes").val());		
									productIdListAdd.push($("#installYes").attr('data-productid'));
									potentialRevenueListAdd.push($("#installYes").attr('cost'));
								}
								$("#reqPnSSelection input[type='checkbox']").trigger("change");		
								calaculateSubTotal();
															
							},
							error : function(xhRequest, ErrorText, thrownError){
								$('body').append('<div id="applATCOverlay" class="grid_21"></div>');	
								$('#applATCOverlay').html(xhRequest.responseText);
								$.fancybox(applATCOverlay.overlayPSApplCartConfig);
								overlayAnalyticsATCErr($('.errormsgPS .pswarningmsg').text());
								return false;
							}
						});
					}
					else if( $responseCode === "_ERR_AOL_CONFIGURATION_UPDATE" || $responseCode === "CONFIGURATION_ERROR"){
						$('.errormsgsavePS').removeClass('hide');
						$('#applActionBtns').addClass('hide');
						$('#applPnSFooter .err-btn-close').addClass('prevPSitemadded ');
						applATCOverlay.config.$isCustomFBClose = true;
						$('#subtotalStatictxt').addClass('hide');
						overlayAnalyticsATCErr($('.errormsgsavePS  .pswarningmsg').text());  
						$.fancybox.hideActivity();
					}else{	
						if(applATCOverlay.config.$isCartPage){
							overlayAnalyticsEditParts();
							var updateURL = $('#thdCart a').attr('href');
							thdCheckoutGlobal.doubleClickUrl(updateURL);
						}
						else{
							overlayAnalyticsSaveParts();
							if(applATCOverlay.config.isGoToCart){
								console.log('Go to Cart button Click');
								window.location.href = window.location.protocol+'//'+window.location.host+'/webapp/wcs/stores/servlet/OrderItemDisplay?storeId=10051&langId=-1&catalogId=10053';
							}
							else{
								$.fancybox.close();
							}						
						}
						
					}
					applATCOverlay.config.isAjaxInProgress = false;
				
			},
			complete : function(jsonresponsedata){
				
			},
			error : function(xhRequest, ErrorText, thrownError){
				$('#subtotalStatictxt').addClass('hide');
				$('.errormsgsavePS').removeClass('hide');
				$('#applActionBtns').addClass('hide');
				$.fancybox.hideActivity();
				overlayAnalyticsATCErr($('.errormsgsavePS  .pswarningmsg').text()); 
				applATCOverlay.config.isAjaxInProgress = false;
			}
		});
		
			return false;
				
	};
	
	applATCOverlay.detectBrowser=function(){
		if ( $.browser.msie || window.ActiveXObject !=="") {
			$('#applATCOverlay').addClass('ie-class');
		}	
   };	
	
	applATCOverlay.toggleDisplay = function(divObjID) { 
		var divObj = $('#'+divObjID);
		if ( !divObj.hasClass('hide')) {
			divObj.addClass('hide');
		} 
		else {
			divObj.removeClass('hide');
		}
 	} 
	
	applATCOverlay.updateBreadCrumbs = function(ulId,targetSpan,btnClick){
		$('#applPnSbrC ul').attr('id',ulId);
		if(btnClick === 'pns-next')
			$('.'+targetSpan+' i').removeClass('brc-brcrc').addClass('icon-brcrc');
		else
			$('.'+targetSpan+' i').removeClass('icon-brcrc').addClass('brc-brcrc');
	};
	
	/* Certona on Apl ATC Modal - START */
	
	loadApplCertona = function(){
		var hostname = "";
		var apiKeyBasedOnHost = "";
		var currentStore = window.readCookie('THD-LOC-STORE');
		hostname = formatHostName();
		if(hostname === ".homedepot.com" || hostname === "http://www.homedepot.com/static/global/scripts/source/-beta.homedepot.com"){
			apiKeyBasedOnHost= "qgAdid5UhtbqdNy9wggswMZn1T0By0RC";
		}
		else{
			apiKeyBasedOnHost=  "1IjpJxh5T6cINCGOYHsTuKsm9IoGeZQN"; 
		}
		if (typeof resx !== "undefined"){
			resx ={
				appid: "HOMEDEPOT01",
				links: "",
				levels: "",
				itemid : _hddata["productID1"],
				event : "shopping cart"
			};
		}
	
		THD.Thirdparty.Certona.refresh({ 
			certonaSchema	:	"ma_atcmodal_rr",
			apiKey			:   apiKeyBasedOnHost,
			storeId			:	currentStore,
			maxProducts		:	"16",
			products		:   _hddata["productID1"],
			exItems			: 	"",
			keyword			: 	""
		}, function(){
			$.fancybox.resize(); 
		});
	},
	
	handleCertonaATCResponse = function(data){
		if(data !== null || data !== undefined){
			var itemsInCart = data["ItemsInCart"];
			var itemsSubTotal = data["SubTotalInCart"];
		}
		if(itemsInCart !== undefined && itemsSubTotal !== undefined){
			var $addedTocartLabel = $('.addedTocartLabel', applATCOverlay.config.clickedCertBtn.parent());
			applATCOverlay.config.clickedCertBtn.hide();
			$.fancybox.hideActivity();
			$addedTocartLabel.css({'display':'inline-block'});
			setTimeout(function() {
				$addedTocartLabel.css({'display':'none'});
				applATCOverlay.config.clickedCertBtn.show(); 
				applATCOverlay.config.certonaBtnClicked = false;
			}, 3000);
			
			$('.display-promo-sec .promo-sec-item').html('Cart ('+data["ItemsInCart"]+')');
			$('.appl-promo-green .promo-sec-item').html(data["SubTotalInCart"]);
			$('p#certonaErrSec').remove();
		}
		else{
			showErrResponse();
			var errCode = data.cart.errorData.error.errorCode;
			showErrMessages(errCode);
		}
	},
	
	showErrResponse = function(){
		applATCOverlay.config.clickedCertBtn.hide();
		$.fancybox.hideActivity();
		var $itemNotAddedLabel = $('.itemNotAddedLabel', applATCOverlay.config.clickedCertBtn.parent());
		$itemNotAddedLabel.css({'display':'inline-block'});
		applATCOverlay.config.certonaBtnClicked = false;
	},
	
	showErrResponseFromDDCall= function(dataType,xhRequest,callbackFunction){
		try{
			if(dataType==='json'){
				if(xhRequest.responseText !=undefined){ //500
					var errorResponse = JSON.parse(xhRequest.responseText);
					if(errorResponse!= undefined && errorResponse !=""){
						callbackFunction(errorResponse);	
					}	
					else{ // timeout
						showErrResponse();
						showErrMessages("00");
					}						
				}
				else if(dataType==='html' && xhRequest.status===500){
					showErrResponse();
					showErrMessages("00");
				}
			}
			else{
				showErrResponse();
				showErrMessages("00");
			}
		}
		catch(e){
				showErrResponse();
				showErrMessages("00");
		}
	},
	
	showErrMessages = function(errCode){
		$('p#certonaErrSec').remove(); // remove any previously displayed error message.
		$('#ma_atcmodal_rr').append('<p id="certonaErrSec" class="btn-clear"><i class="icon-error"></i><span></span></p>'); // add a new div for error message
		var $certonaErrMsg = $('#certonaErrSec span');
		if(errCode === "OOS_ALT_MODEL" || errCode === "OOS_ETA_UNAVAILABLE" ){
            $certonaErrMsg.html("Out Of Stock.");		
		}
		else if(errCode === "_APPLIANCE_SC_MAX_SAME_ITEMS_ALLOWED"){
			$certonaErrMsg.html(applATCOverlayErrorMsg.morethanTwoItemErr);
		}
		else if(errCode === "_APPLIANCE_SC_MAX_ITEMS_ALLOWED"){
			$certonaErrMsg.html(applATCOverlayErrorMsg.maxIApplItemErr);
		}
		else{
			$certonaErrMsg.html("Sorry, but we were unable to add this item to the cart due to a system error.");
		}
		
	},
	
	handleCertonaDDResponse = function(data){
		var statusCode, serverErrorCode = "";
		if(data.deliveryAvailabilityResponse.deliveryAvailability !== undefined && data.deliveryAvailabilityResponse.deliveryAvailability.availability.status){
			statusCode = data.deliveryAvailabilityResponse.deliveryAvailability.availability.status;
		}
		else if(data.deliveryAvailabilityResponse.errorData != undefined && data.deliveryAvailabilityResponse.errorData.errors.error) {
			serverErrorCode = data.deliveryAvailabilityResponse.errorData.errors.error.errorCode;
		}
		if(statusCode === "AVAILABLE" || statusCode === "AVL" || serverErrorCode === "DLVRY_AVAIL_ERR_1003" || serverErrorCode === "DLVRY_AVAIL_ERR_1015" || serverErrorCode === "DLVRY_AVAIL_ERR_1016"){
			statusCode = "AVL";
			var newbackendUrl = generateCertonaBackendUrl(statusCode);
			makeBackendApiReq(newbackendUrl,'json',handleCertonaATCResponse);
		}
		else if(statusCode === "BACK_ORDERED"){
			statusCode = "BO";
			var newbackendUrl = generateCertonaBackendUrl(statusCode);
			makeBackendApiReq(newbackendUrl,'json',handleCertonaATCResponse);
		}
		else if(serverErrorCode === "DLVRY_AVAIL_ERR_1000" || serverErrorCode === "DLVRY_AVAIL_ERR_1013"|| serverErrorCode === "DLVRY_AVAIL_ERR_1014"){
			var newbackendUrl = generateCertonaBackendUrl();
			makeBackendApiReq(newbackendUrl,'json',handleCertonaATCResponse);
		}		
		// status is OOS/other error codes
		else{
			showErrResponse();
			if(statusCode !== undefined && statusCode !== "" ){
				showErrMessages(statusCode);
			}
			else if(serverErrorCode !== undefined && serverErrorCode !== ""){
				showErrMessages(serverErrorCode);
			}
		}
		applATCOverlay.config.isAjaxInProgress = false;
	},
	
	generateCertonaBackendUrl = function(statusCode){
		var prodId = applATCOverlay.config.clickedCertBtn.attr("data-prodid");
    	var certonaOrderItemAddURL = window.location.protocol+"//"+window.location.host+"/webapp/wcs/stores/servlet/OrderItemAdd?storeId=10051&langId=-1&catalogId=10053&catEntryId="+prodId+"&quantity=1&contractId=2081191&addToCartConfirmation=true&fromAddToCartConfirmation=true&nickName=APPLIANCE+DELIVERY+-+"+applATCOverlay.config.zipCode+"&zipCode="+applATCOverlay.config.zipCode;
		if(statusCode!=undefined && statusCode!=""){
    		certonaOrderItemAddURL = certonaOrderItemAddURL+"&statusCode="+statusCode;
    	}
		if(applATCOverlay.config.delvDate!=undefined && applATCOverlay.config.delvDate!=""){
    		certonaOrderItemAddURL = certonaOrderItemAddURL+"&applianceETA="+applATCOverlay.config.delvDate;
    	}
    	return certonaOrderItemAddURL;
    },
	
	makeBackendApiReq = function(apiurl,dataType,callbackFunction){
		$.fancybox.showActivity();
		$.ajax({
			type:"POST",
			url: apiurl,
			dataType: dataType,
			success : function(data){
				callbackFunction(data);
			},
			complete: function () {
				$minCart.refeshMiniCart(); 
			},
			error : function(){
				showErrResponse();
				showErrMessages("00");
			}
		});
	},
	
	$('body').on('click','.triggerATCAppliOverlayRecom, #ma_atcmodal_rr .addCartConfirmBtn',function(e){ 
		e.preventDefault();
		if(!applATCOverlay.config.certonaBtnClicked){
			applATCOverlay.config.certonaBtnClicked = true;
			applATCOverlay.config.clickedCertBtn = $(this);
			if(applATCOverlay.config.clickedCertBtn.hasClass('triggerATCAppliOverlayRecom')){
				var prodId = $(this).attr("data-prodid");
				var newApiUrl = window.location.protocol+"//"+window.location.host+"/wcs/resources/api/v1/tools/deliveryAvailability?itemId="+prodId+"&zipCode="+applATCOverlay.config.zipCode+"&type=JSON&key=XZG1XWUGO90KKnt6Jb9Mc8Jce5Nb8Adj";
				makeApiRequest(newApiUrl,handleCertonaDDResponse,'json',showErrResponseFromDDCall);
			}
			else if(applATCOverlay.config.clickedCertBtn.hasClass('addCartConfirmBtn')){
				var setCCartUrl = $(this).attr('href') + '&addToCartConfirmation=true&fromAddToCartConfirmation=true';
				makeBackendApiReq(setCCartUrl,'json',handleCertonaATCResponse);
			}
		}
	});
	
	/* Certona on Apl ATC Modal - END */

	$('body').on('click','.pns-next',function(e){ 
		e.preventDefault();
		var $pnsbackBtn = $('.pns-back'), $pnsnextBtn = $(".pns-next"), $pnssaveBtn = $('.pns-save');
		
		if($(this).hasClass('reqActive')){
			$('#reqPnSRadiobtn').addClass('hide');
			applATCOverlay.toggleDisplay('reqPnSSelection');
			applATCOverlay.toggleDisplay('optPnSSelection');
			
			applATCOverlay.toggleDisplay($('.showReqDesc').attr('id'));
			applATCOverlay.toggleDisplay('applOptionalInstr');
			
			if($('.bc-prot-plan').length === 0 ){
				$pnsbackBtn.removeClass('hide prv-opt').addClass('prv-req');			
				$pnsnextBtn.removeClass('optActive').addClass('hide');  
				$('.applbrc-2 .arrow-front').removeClass('arrow-front');
				if(applATCOverlay.config.$isCartPage){
					$pnssaveBtn.removeClass('hide');	
				}
				else{
					$("#applRightBtns").removeClass('hide');
					$('#subtotalStatictxt').removeClass('hide');
				}
			}else{
				$pnsbackBtn.removeClass('hide').addClass('prv-req');
				$pnsnextBtn.removeClass('reqActive').addClass('optActive');
			}
			applATCOverlay.updateBreadCrumbs('applbrc-2','bc-opt-parts','pns-next');
			overlayAnalyticsPnsView("optional parts","optional");
			if($('.bc-prot-plan, .bc-req-parts, .bc-opt-parts').length === 3){
						$('.applbrc-2').addClass('prev-chk-mark');
			}
			
		 }
		 else if($(this).hasClass('optActive')){		 
			applATCOverlay.toggleDisplay('optPnSSelection');
			applATCOverlay.toggleDisplay('protectionPlanSelection');
			
			if($('.bc-opt-parts').length === 0 ){
				$('#reqPnSRadiobtn').addClass('hide');
				applATCOverlay.toggleDisplay($('.showReqDesc').attr('id'));
				$('#reqPnSSelection').addClass('hide');
				$pnsbackBtn.addClass('prv-req').removeClass('hide prv-opt');
			}else{				
				applATCOverlay.toggleDisplay('applOptionalInstr');
				$pnsbackBtn.removeClass('hide prv-req').addClass('prv-opt');
			}
			applATCOverlay.toggleDisplay('protectionPlanInstr');
						
						
			$pnsnextBtn.removeClass('optActive').addClass('hide');   
			if(applATCOverlay.config.$isCartPage){
				$pnssaveBtn.removeClass('hide');	
			}
			else{
				$("#applRightBtns").removeClass('hide');
				$('#subtotalStatictxt').removeClass('hide');
			}
			applATCOverlay.updateBreadCrumbs('applbrc-3','bc-prot-plan','pns-next');
			overlayAnalyticsPnsView("protection plan","protection");
			if($('.bc-prot-plan, .bc-req-parts, .bc-opt-parts').length === 3){
						$('.applbrc-2').addClass('prev-chk-mark');
			}
		 }
		  
    });
	$('body').on('click','.pns-back',function(e){
		e.preventDefault();
		var $pnsbackBtn = $('.pns-back'), $pnsnextBtn = $(".pns-next"), $pnssaveBtn = $('.pns-save');
		
		if($(this).hasClass('prv-req')){
			$('#reqPnSRadiobtn').removeClass('hide');
			applATCOverlay.toggleDisplay('reqPnSSelection');
			
			
			applATCOverlay.toggleDisplay($('.showReqDesc').attr('id'));
			if($('.bc-opt-parts').length === 0 ){				
				applATCOverlay.toggleDisplay('protectionPlanSelection');
				applATCOverlay.toggleDisplay('protectionPlanInstr');
			}else{
				applATCOverlay.toggleDisplay('optPnSSelection');
				applATCOverlay.toggleDisplay('applOptionalInstr');
			}
			$pnsbackBtn.removeClass('prv-req').addClass('hide');			
			
			if($('.bc-prot-plan').length === 0 ){
				$pnsnextBtn.removeClass('hide');
				if(applATCOverlay.config.$isCartPage){
					$pnssaveBtn.addClass('hide');
				}
				else{
					$("#applRightBtns").addClass('hide');	
					$('#subtotalStatictxt').addClass('hide');
				}
			}else{
				if($('.bc-opt-parts').length === 0 ){			
					$pnsnextBtn.addClass('optActive').removeClass('hide reqActive');
					if(applATCOverlay.config.$isCartPage){
						$pnssaveBtn.addClass('hide');
					}
					else{
						$("#applRightBtns").addClass('hide');	
						$('#subtotalStatictxt').addClass('hide');
					}					
				}else{
					$pnsnextBtn.removeClass('optActive').addClass('reqActive');	
				}				
			}
			
			applATCOverlay.updateBreadCrumbs('applbrc-1','bc-opt-parts','pns-back');
			overlayAnalyticsPnsView("required parts and services","parts");
			if($('.bc-prot-plan, .bc-req-parts, .bc-opt-parts').length === 3){
				if($('.applbrc-2').hasClass('prev-chk-mark')){
					$('.prev-chk-mark').removeClass('icon-brcrc').addClass('brc-brcrc');
				}
			}
		};
		if($(this).hasClass('prv-opt')){
			applATCOverlay.toggleDisplay('optPnSSelection');
			applATCOverlay.toggleDisplay('protectionPlanSelection');
			
			applATCOverlay.toggleDisplay('applOptionalInstr');
			applATCOverlay.toggleDisplay('protectionPlanInstr');
			
			$pnsnextBtn.removeClass('hide').addClass('optActive');
			if($('.bc-req-parts').length === 0){
				$(this).removeClass('prv-opt').addClass('hide');
			}else{
				$(this).removeClass('prv-opt').addClass('prv-req');
			}
			if(applATCOverlay.config.$isCartPage){
				$pnssaveBtn.addClass('hide');
			}
			else{
				$("#applRightBtns").addClass('hide');	
				$('#subtotalStatictxt').addClass('hide');
			}
			if($('#applPnSbrC ul').hasClass('direct-sel')){
				applATCOverlay.updateBreadCrumbs('applbrc-2','bc-opt-parts','pns-next');
				$('#applPnSbrC ul').removeClass('direct-sel');
			}else{
				applATCOverlay.updateBreadCrumbs('applbrc-2','bc-prot-plan','pns-back');	
			}
			overlayAnalyticsPnsView("optional parts","optional");
			if($('.bc-prot-plan, .bc-req-parts, .bc-opt-parts').length === 3){
						$('.applbrc-2').addClass('prev-chk-mark');
			}
		};
	 
	 });			
							 
	$('body').on('click','#applActionBtns .pns-save,.pns-cont-shop,.pns-goto-cart,.save-btn-psclose',function(e){
			e.preventDefault();
			applATCOverlay.config.optOutConfiguration = false;
			if($(this).hasClass('pns-goto-cart')){
				applATCOverlay.config.isGoToCart = true;
				console.log('Go to Cart button Click');
			}
			var ajaxServicePSJSONURL = window.location.protocol+"//"+window.location.host+"/webapp/wcs/stores/servlet/AOLConfigurationOrderItemUpdate";
			if(applATCOverlay.config.$isCartPage){
				ajaxServicePSJSONURL+="?&displayHeader=Y";
			}			
			applATCOverlay.ajaxApplPartsNServiceCall(ajaxServicePSJSONURL);
									
	});
	
	//binding radio click event for install/hookup No
	$('body').on('click','.reqPSRadioSel',function(e){
		e.preventDefault();
		applATCOverlay.config.selectedRqdPartsArray =[];
		applATCOverlay.config.unSelectedItems =[];		
		productIdListAdd =[];
		potentialRevenueListAdd=[];
		productIdListDelete=[];
		potentialRevenueListDelete=[];
		var $objCurState = $(this), $applYDesc = $("#applRequiredYes"), $applNDesc = $("#applRequiredNo"), $reqCHKBox = $("#reqPnSSelection input[type='checkbox']"), $pnsCustomSel = $("#reqPnSSelection .checkbox-custom ");
		
		if($objCurState.hasClass('radioYesBtn')){
			$applYDesc.removeClass('hide').addClass('showReqDesc');
			$applNDesc.addClass('hide').removeClass('showReqDesc');
			$reqCHKBox.attr("disabled","true");
			$reqCHKBox.attr("checked","checked");
			$pnsCustomSel.addClass('checkbox-custom-disabled-checked').addClass('checkbox-custom-active');
			if(!applATCOverlay.config.isHookupAvailable){
				applATCOverlay.config.selectedRqdPartsArray.push( $(this).val());
				productIdListAdd.push($(this).attr('data-productid'));
				potentialRevenueListAdd.push($(this).attr('cost'));
			}			
		}else{
			$applYDesc.addClass('hide').removeClass('showReqDesc');
			$applNDesc.removeClass('hide').addClass('showReqDesc');
			$reqCHKBox.removeAttr("disabled checked");
			$pnsCustomSel.removeClass('checkbox-custom-disabled-checked').removeClass('checkbox-custom-active');
			if(applATCOverlay.config.$isPnsEditFlow && !applATCOverlay.config.isHookupAvailable && $(this).attr('partorderitemid')!=undefined){
				  applATCOverlay.config.unSelectedItems.push( $(this).attr('partorderitemid'));
					productIdListDelete.push($(this).attr('data-productid'));
					potentialRevenueListDelete.push($(this).attr('cost'));
			}
		}
		calaculateSubTotal();
		$reqCHKBox.trigger("change");
	}); 
	
	//binding checkbox click event	
	$('body').on('click','#applATCPnS input:checkbox',function(e){
		calaculateSubTotal();
	});
	
	
	$('body').on('change','#applATCPnS input:checkbox',function(){
		
		var index =0,$currentSelection = $(this).attr('id'),$partOrdId = $(this).attr('partorderitemid'),
		$currentSkuVal=$(this).val(),$isChecked=$(this).is(':checked'),$productId=$(this).attr('data-productid'),$potentialRevenue=$(this).attr('cost');
		
		if($currentSelection.indexOf('reqPart')===0){
				if($isChecked){
					if($partOrdId===undefined){
						applATCOverlay.config.selectedRqdPartsArray.push($currentSkuVal);
						productIdListAdd.push($productId);
						potentialRevenueListAdd.push($potentialRevenue);
					}
					else{
						removeItemFromArray($partOrdId,applATCOverlay.config.unSelectedItems);
						removeItemFromArray($productId,productIdListDelete);
						removeItemFromArray($potentialRevenue,potentialRevenueListDelete);
					}
				} 
				else{
					if($partOrdId!=undefined && $partOrdId!=""){
						applATCOverlay.config.unSelectedItems.push($partOrdId);
						productIdListDelete.push($productId);
						potentialRevenueListDelete.push($potentialRevenue);
					}
					else{
						removeItemFromArray($currentSkuVal,applATCOverlay.config.selectedRqdPartsArray);
						removeItemFromArray($productId,productIdListAdd);
						removeItemFromArray($potentialRevenue,potentialRevenueListAdd);
					}
				}
		}
		else if($currentSelection.indexOf('optPart')===0){
			if($isChecked){
				if($partOrdId===undefined){
					applATCOverlay.config.selectedOptPartsArray.push($currentSkuVal);	
					productIdListAdd.push($productId);
					potentialRevenueListAdd.push($potentialRevenue);
				}
				else{
					removeItemFromArray($partOrdId,applATCOverlay.config.unSelectedItems);
					removeItemFromArray($productId,productIdListDelete);
					removeItemFromArray($potentialRevenue,potentialRevenueListDelete);
				}
			}
			else{
				if($partOrdId!=undefined && $partOrdId!=""){
					applATCOverlay.config.unSelectedItems.push($partOrdId);	
					productIdListDelete.push($productId);
					potentialRevenueListDelete.push($potentialRevenue);
				}
				else{
					removeItemFromArray($currentSkuVal,applATCOverlay.config.selectedOptPartsArray);
					removeItemFromArray($productId,productIdListAdd);
					removeItemFromArray($potentialRevenue,potentialRevenueListAdd);
				}
			}
		}
	});
		
	$('body').on('click','.app-cont-gotocart',function(e){
		if(window.hddataReady){	window.hddataReady("parts and services optout");  } 
	});
	$('body').on('click','.pnsDwnErrBtn',function(e){
		$.fancybox.close();
	});
	$('body').on('click','#applATCPnS .err-btn-close, .customfb-reload',function(e){
		$.fancybox.close();
		//refresh cart incase of cartpage flow
		if(applATCOverlay.config.$isCartPage && $('#applATCPnS .err-btn-close').hasClass('prevPSitemadded')){			
			var updateURL = $('#thdCart a').attr('href');
			thdCheckoutGlobal.doubleClickUrl(updateURL);
		}
	});
	//Required parts tab clicks
	$('body').on('click','li.applbrc-1',function(e){
			'use strict';
			e.preventDefault();
			if($('.bc-prot-plan, .bc-req-parts, .bc-opt-parts').length > 1){
				var $pnsbackBtn = $('.pns-back'), $pnsnextBtn = $(".pns-next"), $pnssaveBtn = $('.pns-save');
				applATCOverlay.overlayTabClicks("REQ");			
				$pnsbackBtn.removeClass('prv-opt prv-req').addClass('hide');	
				if($('.bc-opt-parts').length === 0 ){
					$pnsnextBtn.removeClass('hide').addClass('optActive');
				}else{
					$pnsnextBtn.removeClass('optActive hide').addClass('reqActive');
				}
				if(applATCOverlay.config.$isCartPage){
						$pnssaveBtn.addClass('hide');
				}
				else{
						$("#applRightBtns").addClass('hide');	
						$('#subtotalStatictxt').addClass('hide');
				}
				applATCOverlay.updateBreadCrumbs('applbrc-1','bc-req-parts','pns-next');
				if($('.bc-prot-plan, .bc-req-parts, .bc-opt-parts').length === 3){
					if($('.applbrc-2').hasClass('prev-chk-mark')){
						$('.prev-chk-mark').removeClass('icon-brcrc').addClass('brc-brcrc');
					}
				}
			}
	});
	//Optional parts tab clicks
	$('body').on('click','li.applbrc-2',function(e){
			'use strict';
			e.preventDefault();
			if($('.bc-prot-plan, .bc-req-parts, .bc-opt-parts').length > 1){
				var $pnsbackBtn = $('.pns-back'), $pnsnextBtn = $(".pns-next"), $pnssaveBtn = $('.pns-save');
				applATCOverlay.overlayTabClicks("OPT");		
				if($('.bc-req-parts').length === 0 ){
					$pnsbackBtn.removeClass('prv-opt prv-req').addClass('hide');
				}else{				
					$pnsbackBtn.removeClass('hide prv-opt').addClass('prv-req');
				}
				if($('.bc-prot-plan').length === 0 ){
					$pnsnextBtn.removeClass('optActive').addClass('hide reqActive');  
					if(applATCOverlay.config.$isCartPage){
						$pnssaveBtn.removeClass('hide');	
					}
				else{
					$("#applRightBtns").removeClass('hide');
					$('#subtotalStatictxt').removeClass('hide');
				}
			}else{
				if($('.bc-prot-plan').length > 0 ){			
					if(applATCOverlay.config.$isCartPage){
							$pnssaveBtn.addClass('hide');
					}else{
							$("#applRightBtns").addClass('hide');	
							$('#subtotalStatictxt').addClass('hide');
					}
				}
				$pnsnextBtn.removeClass('hide reqActive').addClass('optActive');
			} 
			applATCOverlay.updateBreadCrumbs('applbrc-2','bc-opt-parts','pns-next');
			if($('.bc-prot-plan, .bc-req-parts, .bc-opt-parts').length === 3){
				$('.applbrc-2').addClass('prev-chk-mark');
			}

			}
	});
	//Protection plan tab clicks
	$('body').on('click','li.applbrc-3',function(e){
		'use strict';
		e.preventDefault();
		if($('.bc-prot-plan, .bc-req-parts, .bc-opt-parts').length > 1){
			var $pnsbackBtn = $('.pns-back'), $pnsnextBtn = $(".pns-next"), $pnssaveBtn = $('.pns-save');
			applATCOverlay.overlayTabClicks("PROT");
				$pnsnextBtn.removeClass('optActive reqActive').addClass('hide'); 
				if($('.bc-opt-parts').length === 0 ){
					$pnsbackBtn.addClass('prv-req').removeClass('hide prv-opt');
				}else{				
					$pnsbackBtn.removeClass('hide prv-req').addClass('prv-opt');
				}			
				if(applATCOverlay.config.$isCartPage){
					$pnssaveBtn.removeClass('hide');	
				}
				else{
					$("#applRightBtns").removeClass('hide');
					$('#subtotalStatictxt').removeClass('hide');
				}
				applATCOverlay.updateBreadCrumbs('applbrc-3','bc-prot-plan','pns-next');
				$('#applPnSbrC ul').addClass('direct-sel');
				if($('.bc-prot-plan, .bc-req-parts, .bc-opt-parts').length === 3){
					if($('.applbrc-2').hasClass('prev-chk-mark')){
						$('.prev-chk-mark i').removeClass('brc-brcrc').addClass('icon-brcrc');
					}
				}
				
			}
	});
	
	applATCOverlay.overlayTabClicks = function(selectTab){
		var $reqInstallReqInst = $('.showReqDesc').attr('id');
		if(selectTab === "REQ"){
			$('#reqPnSRadiobtn, #reqPnSSelection').removeClass('hide');			
			$('#optPnSSelection, #applOptionalInstr, #protectionPlanSelection, #protectionPlanInstr').addClass('hide');
			$('#'+$reqInstallReqInst).removeClass('hide');
			overlayAnalyticsPnsView("required parts and services","parts");
		}else if(selectTab === "OPT"){
			$('#reqPnSRadiobtn, #reqPnSSelection, #protectionPlanSelection, #noInstall, #protectionPlanInstr').addClass('hide');			 			
			$('#optPnSSelection, #applOptionalInstr').removeClass('hide');
			$('#'+$reqInstallReqInst).addClass('hide');
			overlayAnalyticsPnsView("optional parts","optional");
		}else if(selectTab === "PROT"){
			$('#reqPnSRadiobtn, #reqPnSSelection, #optPnSSelection, #applOptionalInstr, #noInstall').addClass('hide');			 
			$('#protectionPlanSelection, #protectionPlanInstr').removeClass('hide');
			$('#'+$reqInstallReqInst).addClass('hide');	
			overlayAnalyticsPnsView("protection plan","protection");
		}
	};
					
	
})(THD.Utility.Namespace.createNamespace('THD.Global.ApplATCOverlay'));