//Creating ALLYMOBILE ns for device detection and mobile redirects (to begin with)
//These items live outside of the ALLY ns and have no dependecy on jQuery or other library
var ALLYMOBILE = ALLYMOBILE || {};

/**
* Device detection for device and device type
*
* Example usage: ALLYMOBILE.device.iphone (will return a boolean)
*
* Device list:
*   ALLYMOBILE.device.iphone
*   ALLYMOBILE.device.ipod
*   ALLYMOBILE.device.ipad
*   ALLYMOBILE.device.android
*   ALLYMOBILE.device.blackberry (will return boolean for webkit based Blackberries)
*   ALLYMOBILE.device.blackberryplaybook
*   ALLYMOBILE.device.windowsphone
*   ALLYMOBILE.device.kindlefire
*   ALLYMOBILE.device.othermobile (portrait resolution of 320 or less)
*   ALLYMOBILE.device.phone
*   ALLYMOBILE.device.tablet
*   ALLYMOBILE.device.desktop
*
* Methods for device detection inspired by Sencha Touch Ext.is
* http://docs.sencha.com/touch/1-1/#!/api/Ext.is
*
* Author: Vernon Kesner
* Version: 1.1.0
* Last update: March 21, 2012
*/

ALLYMOBILE.device = (function() {

  'use strict';

  var device = {};

  device.phone = false;
  device.tablet = false;
  //iPhone
  device.iphone = (testNavigator(/iPhone/i, 'platform')) ? true : false;
  if(device.iphone) { device.phone = true; }
  //iPad
  device.ipad = (testNavigator(/iPad/i, 'platform')) ? true : false;
  if(device.ipad) { device.tablet = true; }
  //iPod
  device.ipod = (testNavigator(/iPod/i, 'platform')) ? true : false;
  if(device.ipod) { device.phone = true; }
  //Android device
  device.android = testNavigator(/Android/i, 'userAgent');
  if(device.android) {
    //Android v3 built as tablet-only version of the OS
    //Can definitively say it's a tablet at this point
    if(testVersion(/Android\s(\d+\.\d+)/i, 3, 'match') ) {
      device.tablet = true;
    }
    //Checking for "mobile" in userAgent string for Mobile Safari.
    //Also checking resolution here (max portrait of 800), simply because so
    //many Android tablets that are popular use Android v2.x or now v4.x
    else if(testResolution(800) && testNavigator(/Mobile/i, 'userAgent')) {
      device.phone = true;
    }
    //Default phone vs. tablet value? Defaulting to phone for now until I can think
    //of a better alternative approach to narrow down better.
    else {
      device.phone = true;
    }
    // Array of edge cases that are hard to determine
    var tablets = ['Nexus 7'];
    for(var i=0; i < tablets.length; i++) {
      if (testNavigator(new RegExp(tablets[i]),'userAgent')) {
         device.tablet = true;
         device.phone = false;
      }
    }
  }
  //Blackberry Phone with WebKit
  device.blackberry = (testNavigator(/Blackberry/i, 'userAgent') && testNavigator(/Mobile/i, 'userAgent')) ? true : false;
  if(device.blackberry) { device.phone = true; }
  //Blackberry Playbook
  device.blackberryplaybook = testNavigator(/RIM\sTablet/i, 'userAgent');
  if(device.blackberryplaybook) { device.tablet = true; }
  //Windows Phone
  device.windowsphone = testNavigator(/Windows\sPhone/i, 'userAgent');
  if(device.windowsphone) { device.phone = true; }
  //Kindle Fire
  device.kindlefire = testNavigator(/Silk/i, 'userAgent');
  if(device.kindlefire) { device.tablet = true; }
  //other mobile
  //TODO: Remove below line and uncomment following line when Windows Phone is supported
  //device.othermobile = (device.phone || device.tablet || device.ipod || device.windowsphone) ? false : testResolution(320);
  //device.othermobile = (device.phone || device.tablet || device.ipod) ? false : testResolution(320);
  if(device.othermobile) { device.phone = true; }
  //desktop user?
  device.desktop = (device.phone || device.tablet || device.ipod) ? false : true;

  //Test window.navigator object for a match
  //return - Boolean
  function testNavigator(pattern, property) {
    return pattern.test(window.navigator[property]);
  }

  //Test if maximum portrait width set in platform is less than the current screen width
  //return - Boolean
  function testResolution(maxPortraitWidth) {
    var portraitWidth = Math.min(screen.width, screen.height) / ("devicePixelRatio" in window ? window.devicePixelRatio : 1);
    if(portraitWidth <= maxPortraitWidth) {
      return true;
    }
    else {
      return false;
    }
  }

  //Test OS Version
  //param - pattern - Regex pattern
  //param - version - Integer - Major version to compare against
  //param - versionComparison - String - How version matching is done "match", "greaterThan", "lessThan"
  //return - Boolean
  function testVersion(pattern, version, versionComparison) {
    var fullVersion = pattern.exec(window.navigator.userAgent),
        majorVersion = parseInt(fullVersion[1], 10);
        
    if(versionComparison === "match" && majorVersion === version ) {
      return true;
    }
    else if(versionComparison === "greaterThan" && majorVersion > version) {
      return true;
    }
    else if(versionComparison === "lessThan" && majorVersion < version) {
      return true;
    }
    else {
      return false;
    }
  }

  return device;

}());


/**
* Manage mobile redirects to m.ally.com with context
*/
ALLYMOBILE.redirects = (function() {

  'use strict';

  var redirectURL = "http://m.ally.com/",
      documentPathName = document.location.pathname,
      documentHost = document.location.host,
      fullSitePattern = /www\./,
      devPattern = /www\-/,
      localPattern = /localhost/,
      originPattern = /origin\-www/,
      stagingPattern = /staging\.int/,
      bdgPattern = /bdg..\.int/,
      supportPattern = /bdgsupport\.int/,
      mobilePattern = /^m-|^m\./,
      context = getContext(),
      preference = getPreference() || checkReferrer(),
      referrer = checkReferrer();

  var isTheHomePage = function () {
    return documentPathName === "/" || documentPathName === "/bank" || documentPathName === "/bank/" || documentPathName === "http://www.ally.com/bank/index.html" || documentPathName === "http://www.ally.com/index.html";
  };

  var isTheFullSite = function () {
    return fullSitePattern.test(documentHost);
  };

  var isTheAutoHomepage = function() {
    return documentPathName === "/auto/" || documentPathName === "/auto" || documentPathName === "http://www.ally.com/auto/index.html";
  };

  var isAHomepage = function() {
    return isTheHomePage() || isTheAutoHomepage();
  };

  var isDevEnvironment = function() {
    return stagingPattern.test(documentHost) || bdgPattern.test(documentHost) || supportPattern.test(documentHost);
  }

  var isOtherRedirect = function(){
    return redirectMaps.hasOwnProperty(location.pathname.match(/\/.*\//));
  };

  var isMobileSite = function(){
    return mobilePattern.test(documentHost);
  };

  var isLocal = function(){
    return localPattern.test(documentHost);
  };
  //include any query string params, such as CP tracking codes
  //return empty string if no query string exists, otherwise return the full QSP
  var getQueryStringFromReferrer = function(){
    return typeof(document.referrer.split('?')[1]) === 'undefined' ? '' : '?'+document.referrer.split('?')[1];
  }
  var redirectMaps = {
    '/bank/interest-checking-account/' : '/bank/interest-checking-account/',
    '/bank/online-savings-account/' : '/bank/online-savings-account/',
    '/bank/money-market-account/' : '/bank/money-market-account/',
    '/bank/high-yield-cd/' : '/bank/high-yield-cd/',
    '/bank/raise-your-rate-cd/' : '/bank/raise-your-rate-cd/',
    '/bank/no-penalty-cd/' : '/bank/no-penalty-cd/',
    '/bank/savings/' : '/bank/compare-saving-and-cd-rates/',
    '/bank/compare/' : '/bank/compare-saving-and-cd-rates/',
    '/bank/ira/high-yield-cd/' : '/bank/ira-high-yield-cd/',
    '/bank/ira/raise-your-rate-cd/' : '/bank/ira-raise-your-rate-cd/',
    '/bank/ira/online-savings-account/' : '/bank/ira-online-savings-account/',
    '/bank/online-banking/' : '/bank/online-banking/',
    'http://www.ally.com/bank/customer-reviews/reviews-modal.html' : '/bank/online-banking-reviews/',
    '/bank/fdic/' : '/bank/fdic/'
  };
  
  if((isAHomepage() || isOtherRedirect()) && (isTheFullSite() || isLocal() || isDevEnvironment()) && !isMobileSite()) {
    //This isn't a phone.  Abort! Abort!
    //BTW... This isn't included in the above if because that might change to allow other paths to be redirected
    //This will always ... most likely ... nah, let's risk it ... always ... stay the same.
    if(!ALLYMOBILE.device.phone) {
      return false;
    }

    //If we're in a dev/stage/origin environment, update the redirect url.
    if(devPattern.test(documentHost)) {
      redirectURL = '//' + documentHost.replace(devPattern, 'm-');
    }
    else if(originPattern.test(documentHost)) {
      redirectURL = 'http://origin-m.ally.com/';
    }
    else if(stagingPattern.test(documentHost)) {
      redirectURL = 'http://m-staging.int.ally.com/';
    }
    else if(supportPattern.test(documentHost)) {
      redirectURL = 'http://m-bdgsupport.int.ally.com/';
    }
    else if(bdgPattern.test(documentHost)) {
      redirectURL = '//m-' + documentHost.split(".")[0] + '.int.ally.com';
    }

    //Preferences won't matter now, but leaving code here in case we add them back in the future.
    if(preference === 'mobile') {
      //leave preferences alone, just redirect to mobile
      redirect(context);
    }
    else if(preference === 'full') {
      //abort detection and just return
      return;
    }
    else {
      //No preference found, do normal redirect setting preference of mobile
      setPreference('mobile');
      redirect(context);
    }
  }
  else if(isMobileSite() && window.location.search.match(/\?context=bank/)){
    redirectURL = '//' + document.location.host;
    redirect(context);
  }
  //Get the context of the current site section (Bank or Auto)
  //The homepage returns a context of "bank" right now
  function getContext() {
    var pathName = document.location.pathname,
        pathSearch = window.location.search;

    if(pathName !== '/' || pathSearch){
      if(pathName.match(/bank/) || pathSearch.match(/bank/) || pathSearch.match(/\?context=bank/)){
        return 'bank';
      }
      if(pathName.match(/auto/) || pathSearch.match('/auto/')){
        return 'auto';
      }
    }else{
      return false;
    }
    
        //bankPattern = /\/bank\//i,
        //autoPattern = /\/auto\//i;

        
    // //if(pathName === "/" || bankPattern.test(pathName)) {
    // if( /*pathName === "/" || pathName === "http://www.ally.com/index.html" ||*/ pathName === "/bank" || pathName === "/bank/" || pathName === "http://www.ally.com/bank/index.html") {
    //   return "bank";
    // }
    // else if(pathName === "/auto/" || pathName === "/auto" || pathName === "http://www.ally.com/auto/index.html") {
    // // else if(autoPattern.test(pathName)) {
    //   return "auto";
    // }
    // else {
    //   return false;
    // }
  }

  //Get the users stored preference if one exists
  //return - value or false
  function getPreference() {
    return false;
    // Commenting the below to disable the cookie setting.
    // Leaving in, just in case we want to go back. Then it's super easy to do.
    //return getCookie('sitePreference') || false;
  }

  //Set user site preference (stores for 30 days)
  //param - pref - String - mobile || full
  function setPreference(pref) {
    // Commenting the below to disable cookie setting.
    // Leaving in, just in case we want to go back. Then it's super easy to do.
    // setCookie('sitePreference', pref, 30);
  }

  //Check the URL for a full site preference tag
  //Coming from m.ally.com -> Query string: fullsite=true
  //Also, check referrer. If it's .mobi set preference to full
  //Coming from m.allybank.com -> redirect to m.ally.com/?context=bank
  //Coming from m.allyauto.com -> redirect to m.ally.com/?context=auto
  //return - "full" or "mobile"
  function checkReferrer() {
    var urlParams = window.location.href.slice(window.location.href.indexOf("?") + 1).split("&"),
        paramsLength = urlParams.length,
        params = [],
        thisParam,
        i,
        referrer = document.referrer,
        mobiPattern = /\.mobi/,
        mBankPattern = /allybank\.com/,
        mAutoPattern = /allyauto\.com/;

    for(i=0; i<paramsLength; i++) {
      thisParam = urlParams[i].split("=");
      params.push(thisParam[0]);
      params[thisParam[0]] = thisParam[1];
    }

    if(params["fullsite"] !== undefined && params["fullsite"] === "true") {
      //user prefers full site experience
      setPreference("full");
      return "full";
    }
    // Removing as full site from mobi needs query, otherwise we're redirecting.
    // else if(mobiPattern.test(referrer)) {
    //   //check for .mobi referrer, if so return "full" preference
    //   return "full";
    // }
    else if(mBankPattern.test(referrer)) {
      //Arrived from m.allybank.com, redirect to m.ally.com/?context=bank
      redirect("bank");
    }
    else if(mAutoPattern.test(referrer)) {
      //Arrived from m.allyauto.com, redirect to m.ally.com/?context=auto
      redirect("auto");
    }else {
      //no passed referrer message, return "mobile"
      return "mobile";
    }
  }

  //Perform the redirect, either by standard URL or context
  //param - context - String - Context to pass in a query string param
  //need to also include bankrate as ref property so we can conditionally change phone number displayed on mobile bank
  function redirect(context) {
    var url = redirectURL;
    //go to auto or bank homepage, otherwise go to defined redirect
    if(context == 'bank' && !isOtherRedirect()){
      url += '/bank/home/';
    }else if(context && !isOtherRedirect()) {
      url += '/?context=' + context;
    }else if(location.pathname == '/'){ //is bank homepage but not from allybank.com redirect?
      url += '/';
    }else if(isOtherRedirect()){
      url += redirectMaps[location.pathname.match(/\/.*\//)] + location.search;
    }
    document.location.href=url;
  }

  //Get a cookie
  //param - name - String - Name of cookie to retrieve
  //return - String || Null - Returns cookie value or null when not found
  function getCookie(name) {
    var nameMatcher = name + "=",
        cookieArray = document.cookie.split(';'),
        cookieArrayLength = cookieArray.length,
        thisCookie,
        i;

    for(i=0; i < cookieArrayLength; i++) {
      thisCookie = cookieArray[i];

      while(thisCookie.charAt(0) === ' ') {
        thisCookie = thisCookie.substring(1, thisCookie.length);
      }

      if(thisCookie.indexOf(nameMatcher) === 0) {
        return thisCookie.substring(nameMatcher.length, thisCookie.length);
      }
    }
    return null; //cookie not found, return null
  }

  //Set a cookie
  //param - name - String - Name of cookie
  //param - value - String - Content of cookie
  //param - days - Integer - 0 for session cookie, -1 to erase, Number of days cookie to persist
  function setCookie(name, value, days) {
    var date,
        expires;

    if(days) {
      date = new Date();
      date.setTime(date.getTime()+(days*24*60*60*1000));
      expires = "; expires=" + date.toGMTString();
    }
    else {
      expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
    return true;
  }

  //Erase the named cookie
  function eraseCookie(name) {
    setCookie(name, "", -1);
    return true;
  }

}());

		/*! Concatenated on 03-05-2015 at 12:54:22 AM by VERNON */
window.Modernizr=function(a,b,c){function d(a){t.cssText=a}function e(a,b){return d(x.join(a+";")+(b||""))}function f(a,b){return typeof a===b}function g(a,b){return!!~(""+a).indexOf(b)}function h(a,b){for(var d in a){var e=a[d];if(!g(e,"-")&&t[e]!==c)return"pfx"==b?e:!0}return!1}function i(a,b,d){for(var e in a){var g=b[a[e]];if(g!==c)return d===!1?a[e]:f(g,"function")?g.bind(d||b):g}return!1}function j(a,b,c){var d=a.charAt(0).toUpperCase()+a.slice(1),e=(a+" "+z.join(d+" ")+d).split(" ");return f(b,"string")||f(b,"undefined")?h(e,b):(e=(a+" "+A.join(d+" ")+d).split(" "),i(e,b,c))}function k(){o.input=function(c){for(var d=0,e=c.length;e>d;d++)E[c[d]]=!!(c[d]in u);return E.list&&(E.list=!(!b.createElement("datalist")||!a.HTMLDataListElement)),E}("autocomplete autofocus list placeholder max min multiple pattern required step".split(" ")),o.inputtypes=function(a){for(var d,e,f,g=0,h=a.length;h>g;g++)u.setAttribute("type",e=a[g]),d="text"!==u.type,d&&(u.value=v,u.style.cssText="position:absolute;visibility:hidden;",/^range$/.test(e)&&u.style.WebkitAppearance!==c?(q.appendChild(u),f=b.defaultView,d=f.getComputedStyle&&"textfield"!==f.getComputedStyle(u,null).WebkitAppearance&&0!==u.offsetHeight,q.removeChild(u)):/^(search|tel)$/.test(e)||(d=/^(url|email)$/.test(e)?u.checkValidity&&u.checkValidity()===!1:u.value!=v)),D[a[g]]=!!d;return D}("search tel url email datetime date month week time datetime-local number range color".split(" "))}var l,m,n="2.6.2",o={},p=!0,q=b.documentElement,r="modernizr",s=b.createElement(r),t=s.style,u=b.createElement("input"),v=":)",w={}.toString,x=" -webkit- -moz- -o- -ms- ".split(" "),y="Webkit Moz O ms",z=y.split(" "),A=y.toLowerCase().split(" "),B={svg:"http://www.w3.org/2000/svg"},C={},D={},E={},F=[],G=F.slice,H=function(a,c,d,e){var f,g,h,i,j=b.createElement("div"),k=b.body,l=k||b.createElement("body");if(parseInt(d,10))for(;d--;)h=b.createElement("div"),h.id=e?e[d]:r+(d+1),j.appendChild(h);return f=["&#173;",'<style id="s',r,'">',a,"</style>"].join(""),j.id=r,(k?j:l).innerHTML+=f,l.appendChild(j),k||(l.style.background="",l.style.overflow="hidden",i=q.style.overflow,q.style.overflow="hidden",q.appendChild(l)),g=c(j,a),k?j.parentNode.removeChild(j):(l.parentNode.removeChild(l),q.style.overflow=i),!!g},I=function(){function a(a,e){e=e||b.createElement(d[a]||"div"),a="on"+a;var g=a in e;return g||(e.setAttribute||(e=b.createElement("div")),e.setAttribute&&e.removeAttribute&&(e.setAttribute(a,""),g=f(e[a],"function"),f(e[a],"undefined")||(e[a]=c),e.removeAttribute(a))),e=null,g}var d={select:"input",change:"input",submit:"form",reset:"form",error:"img",load:"img",abort:"img"};return a}(),J={}.hasOwnProperty;m=f(J,"undefined")||f(J.call,"undefined")?function(a,b){return b in a&&f(a.constructor.prototype[b],"undefined")}:function(a,b){return J.call(a,b)},Function.prototype.bind||(Function.prototype.bind=function(a){var b=this;if("function"!=typeof b)throw new TypeError;var c=G.call(arguments,1),d=function(){if(this instanceof d){var e=function(){};e.prototype=b.prototype;var f=new e,g=b.apply(f,c.concat(G.call(arguments)));return Object(g)===g?g:f}return b.apply(a,c.concat(G.call(arguments)))};return d}),C.flexbox=function(){return j("flexWrap")},C.canvas=function(){var a=b.createElement("canvas");return!(!a.getContext||!a.getContext("2d"))},C.canvastext=function(){return!(!o.canvas||!f(b.createElement("canvas").getContext("2d").fillText,"function"))},C.webgl=function(){return!!a.WebGLRenderingContext},C.touch=function(){var c;return"ontouchstart"in a||a.DocumentTouch&&b instanceof DocumentTouch?c=!0:H(["@media (",x.join("touch-enabled),("),r,")","{#modernizr{top:9px;position:absolute}}"].join(""),function(a){c=9===a.offsetTop}),c},C.geolocation=function(){return"geolocation"in navigator},C.postmessage=function(){return!!a.postMessage},C.websqldatabase=function(){return!!a.openDatabase},C.indexedDB=function(){return!!j("indexedDB",a)},C.hashchange=function(){return I("hashchange",a)&&(b.documentMode===c||b.documentMode>7)},C.history=function(){return!(!a.history||!history.pushState)},C.draganddrop=function(){var a=b.createElement("div");return"draggable"in a||"ondragstart"in a&&"ondrop"in a},C.websockets=function(){return"WebSocket"in a||"MozWebSocket"in a},C.rgba=function(){return d("background-color:rgba(150,255,150,.5)"),g(t.backgroundColor,"rgba")},C.hsla=function(){return d("background-color:hsla(120,40%,100%,.5)"),g(t.backgroundColor,"rgba")||g(t.backgroundColor,"hsla")},C.multiplebgs=function(){return d("background:url(https://),url(https://),red url(https://)"),/(url\s*\(.*?){3}/.test(t.background)},C.backgroundsize=function(){return j("backgroundSize")},C.borderimage=function(){return j("borderImage")},C.borderradius=function(){return j("borderRadius")},C.boxshadow=function(){return j("boxShadow")},C.textshadow=function(){return""===b.createElement("div").style.textShadow},C.opacity=function(){return e("opacity:.55"),/^0.55$/.test(t.opacity)},C.cssanimations=function(){return j("animationName")},C.csscolumns=function(){return j("columnCount")},C.cssgradients=function(){var a="background-image:",b="gradient(linear,left top,right bottom,from(#9f9),to(white));",c="linear-gradient(left top,#9f9, white);";return d((a+"-webkit- ".split(" ").join(b+a)+x.join(c+a)).slice(0,-a.length)),g(t.backgroundImage,"gradient")},C.cssreflections=function(){return j("boxReflect")},C.csstransforms=function(){return!!j("transform")},C.csstransforms3d=function(){var a=!!j("perspective");return a&&"webkitPerspective"in q.style&&H("@media (transform-3d),(-webkit-transform-3d){#modernizr{left:9px;position:absolute;height:3px;}}",function(b){a=9===b.offsetLeft&&3===b.offsetHeight}),a},C.csstransitions=function(){return j("transition")},C.fontface=function(){var a;return H('@font-face {font-family:"font";src:url("https://")}',function(c,d){var e=b.getElementById("smodernizr"),f=e.sheet||e.styleSheet,g=f?f.cssRules&&f.cssRules[0]?f.cssRules[0].cssText:f.cssText||"":"";a=/src/i.test(g)&&0===g.indexOf(d.split(" ")[0])}),a},C.generatedcontent=function(){var a;return H(["#",r,"{font:0/0 a}#",r,':after{content:"',v,'";visibility:hidden;font:3px/1 a}'].join(""),function(b){a=b.offsetHeight>=3}),a},C.video=function(){var a=b.createElement("video"),c=!1;try{(c=!!a.canPlayType)&&(c=new Boolean(c),c.ogg=a.canPlayType('video/ogg; codecs="theora"').replace(/^no$/,""),c.h264=a.canPlayType('video/mp4; codecs="avc1.42E01E"').replace(/^no$/,""),c.webm=a.canPlayType('video/webm; codecs="vp8, vorbis"').replace(/^no$/,""))}catch(d){}return c},C.audio=function(){var a=b.createElement("audio"),c=!1;try{(c=!!a.canPlayType)&&(c=new Boolean(c),c.ogg=a.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/,""),c.mp3=a.canPlayType("audio/mpeg;").replace(/^no$/,""),c.wav=a.canPlayType('audio/wav; codecs="1"').replace(/^no$/,""),c.m4a=(a.canPlayType("audio/x-m4a;")||a.canPlayType("audio/aac;")).replace(/^no$/,""))}catch(d){}return c},C.localstorage=function(){try{return localStorage.setItem(r,r),localStorage.removeItem(r),!0}catch(a){return!1}},C.sessionstorage=function(){try{return sessionStorage.setItem(r,r),sessionStorage.removeItem(r),!0}catch(a){return!1}},C.webworkers=function(){return!!a.Worker},C.applicationcache=function(){return!!a.applicationCache},C.svg=function(){return!!b.createElementNS&&!!b.createElementNS(B.svg,"svg").createSVGRect},C.inlinesvg=function(){var a=b.createElement("div");return a.innerHTML="<svg/>",(a.firstChild&&a.firstChild.namespaceURI)==B.svg},C.smil=function(){return!!b.createElementNS&&/SVGAnimate/.test(w.call(b.createElementNS(B.svg,"animate")))},C.svgclippaths=function(){return!!b.createElementNS&&/SVGClipPath/.test(w.call(b.createElementNS(B.svg,"clipPath")))};for(var K in C)m(C,K)&&(l=K.toLowerCase(),o[l]=C[K](),F.push((o[l]?"":"no-")+l));return o.input||k(),o.addTest=function(a,b){if("object"==typeof a)for(var d in a)m(a,d)&&o.addTest(d,a[d]);else{if(a=a.toLowerCase(),o[a]!==c)return o;b="function"==typeof b?b():b,"undefined"!=typeof p&&p&&(q.className+=" "+(b?"":"no-")+a),o[a]=b}return o},d(""),s=u=null,function(a,b){function c(a,b){var c=a.createElement("p"),d=a.getElementsByTagName("head")[0]||a.documentElement;return c.innerHTML="x<style>"+b+"</style>",d.insertBefore(c.lastChild,d.firstChild)}function d(){var a=r.elements;return"string"==typeof a?a.split(" "):a}function e(a){var b=q[a[o]];return b||(b={},p++,a[o]=p,q[p]=b),b}function f(a,c,d){if(c||(c=b),k)return c.createElement(a);d||(d=e(c));var f;return f=d.cache[a]?d.cache[a].cloneNode():n.test(a)?(d.cache[a]=d.createElem(a)).cloneNode():d.createElem(a),f.canHaveChildren&&!m.test(a)?d.frag.appendChild(f):f}function g(a,c){if(a||(a=b),k)return a.createDocumentFragment();c=c||e(a);for(var f=c.frag.cloneNode(),g=0,h=d(),i=h.length;i>g;g++)f.createElement(h[g]);return f}function h(a,b){b.cache||(b.cache={},b.createElem=a.createElement,b.createFrag=a.createDocumentFragment,b.frag=b.createFrag()),a.createElement=function(c){return r.shivMethods?f(c,a,b):b.createElem(c)},a.createDocumentFragment=Function("h,f","return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&("+d().join().replace(/\w+/g,function(a){return b.createElem(a),b.frag.createElement(a),'c("'+a+'")'})+");return n}")(r,b.frag)}function i(a){a||(a=b);var d=e(a);return!r.shivCSS||j||d.hasCSS||(d.hasCSS=!!c(a,"article,aside,figcaption,figure,footer,header,hgroup,nav,section{display:block}mark{background:#FF0;color:#000}")),k||h(a,d),a}var j,k,l=a.html5||{},m=/^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i,n=/^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i,o="_html5shiv",p=0,q={};!function(){try{var a=b.createElement("a");a.innerHTML="<xyz></xyz>",j="hidden"in a,k=1==a.childNodes.length||function(){b.createElement("a");var a=b.createDocumentFragment();return"undefined"==typeof a.cloneNode||"undefined"==typeof a.createDocumentFragment||"undefined"==typeof a.createElement}()}catch(c){j=!0,k=!0}}();var r={elements:l.elements||"abbr article aside audio bdi canvas data datalist details figcaption figure footer header hgroup mark meter nav output progress section summary time video",shivCSS:l.shivCSS!==!1,supportsUnknownElements:k,shivMethods:l.shivMethods!==!1,type:"default",shivDocument:i,createElement:f,createDocumentFragment:g};a.html5=r,i(b)}(this,b),o._version=n,o._prefixes=x,o._domPrefixes=A,o._cssomPrefixes=z,o.hasEvent=I,o.testProp=function(a){return h([a])},o.testAllProps=j,o.testStyles=H,o.prefixed=function(a,b,c){return b?j(a,b,c):j(a,"pfx")},q.className=q.className.replace(/(^|\s)no-js(\s|$)/,"$1$2")+(p?" js "+F.join(" "):""),o}(this,this.document),function(a,b,c){function d(a){return"[object Function]"==q.call(a)}function e(a){return"string"==typeof a}function f(){}function g(a){return!a||"loaded"==a||"complete"==a||"uninitialized"==a}function h(){var a=r.shift();s=1,a?a.t?o(function(){("c"==a.t?m.injectCss:m.injectJs)(a.s,0,a.a,a.x,a.e,1)},0):(a(),h()):s=0}function i(a,c,d,e,f,i,j){function k(b){if(!n&&g(l.readyState)&&(t.r=n=1,!s&&h(),l.onload=l.onreadystatechange=null,b)){"img"!=a&&o(function(){v.removeChild(l)},50);for(var d in A[c])A[c].hasOwnProperty(d)&&A[c][d].onload()}}var j=j||m.errorTimeout,l=b.createElement(a),n=0,q=0,t={t:d,s:c,e:f,a:i,x:j};1===A[c]&&(q=1,A[c]=[]),"object"==a?l.data=c:(l.src=c,l.type=a),l.width=l.height="0",l.onerror=l.onload=l.onreadystatechange=function(){k.call(this,q)},r.splice(e,0,t),"img"!=a&&(q||2===A[c]?(v.insertBefore(l,u?null:p),o(k,j)):A[c].push(l))}function j(a,b,c,d,f){return s=0,b=b||"j",e(a)?i("c"==b?x:w,a,b,this.i++,c,d,f):(r.splice(this.i++,0,a),1==r.length&&h()),this}function k(){var a=m;return a.loader={load:j,i:0},a}var l,m,n=b.documentElement,o=a.setTimeout,p=b.getElementsByTagName("script")[0],q={}.toString,r=[],s=0,t="MozAppearance"in n.style,u=t&&!!b.createRange().compareNode,v=u?n:p.parentNode,n=a.opera&&"[object Opera]"==q.call(a.opera),n=!!b.attachEvent&&!n,w=t?"object":n?"script":"img",x=n?"script":w,y=Array.isArray||function(a){return"[object Array]"==q.call(a)},z=[],A={},B={timeout:function(a,b){return b.length&&(a.timeout=b[0]),a}};m=function(a){function b(a){var b,c,d,a=a.split("!"),e=z.length,f=a.pop(),g=a.length,f={url:f,origUrl:f,prefixes:a};for(c=0;g>c;c++)d=a[c].split("="),(b=B[d.shift()])&&(f=b(f,d));for(c=0;e>c;c++)f=z[c](f);return f}function g(a,e,f,g,h){var i=b(a),j=i.autoCallback;i.url.split(".").pop().split("?").shift(),i.bypass||(e&&(e=d(e)?e:e[a]||e[g]||e[a.split("/").pop().split("?")[0]]),i.instead?i.instead(a,e,f,g,h):(A[i.url]?i.noexec=!0:A[i.url]=1,f.load(i.url,i.forceCSS||!i.forceJS&&"css"==i.url.split(".").pop().split("?").shift()?"c":c,i.noexec,i.attrs,i.timeout),(d(e)||d(j))&&f.load(function(){k(),e&&e(i.origUrl,h,g),j&&j(i.origUrl,h,g),A[i.url]=2})))}function h(a,b){function c(a,c){if(a){if(e(a))c||(l=function(){var a=[].slice.call(arguments);m.apply(this,a),n()}),g(a,l,b,0,j);else if(Object(a)===a)for(i in h=function(){var b,c=0;for(b in a)a.hasOwnProperty(b)&&c++;return c}(),a)a.hasOwnProperty(i)&&(!c&&!--h&&(d(l)?l=function(){var a=[].slice.call(arguments);m.apply(this,a),n()}:l[i]=function(a){return function(){var b=[].slice.call(arguments);a&&a.apply(this,b),n()}}(m[i])),g(a[i],l,b,i,j))}else!c&&n()}var h,i,j=!!a.test,k=a.load||a.both,l=a.callback||f,m=l,n=a.complete||f;c(j?a.yep:a.nope,!!k),k&&c(k)}var i,j,l=this.yepnope.loader;if(e(a))g(a,0,l,0);else if(y(a))for(i=0;i<a.length;i++)j=a[i],e(j)?g(j,0,l,0):y(j)?m(j):Object(j)===j&&h(j,l);else Object(a)===a&&h(a,l)},m.addPrefix=function(a,b){B[a]=b},m.addFilter=function(a){z.push(a)},m.errorTimeout=1e4,null==b.readyState&&b.addEventListener&&(b.readyState="loading",b.addEventListener("DOMContentLoaded",l=function(){b.removeEventListener("DOMContentLoaded",l,0),b.readyState="complete"},0)),a.yepnope=k(),a.yepnope.executeStack=h,a.yepnope.injectJs=function(a,c,d,e,i,j){var k,l,n=b.createElement("script"),e=e||m.errorTimeout;n.src=a;for(l in d)n.setAttribute(l,d[l]);c=j?h:c||f,n.onreadystatechange=n.onload=function(){!k&&g(n.readyState)&&(k=1,c(),n.onload=n.onreadystatechange=null)},o(function(){k||(k=1,c(1))},e),i?n.onload():p.parentNode.insertBefore(n,p)},a.yepnope.injectCss=function(a,c,d,e,g,i){var j,e=b.createElement("link"),c=i?h:c||f;e.href=a,e.rel="stylesheet",e.type="text/css";for(j in d)e.setAttribute(j,d[j]);g||(p.parentNode.insertBefore(e,p),o(c,0))}}(this,document),Modernizr.load=function(){yepnope.apply(window,[].slice.call(arguments,0))};