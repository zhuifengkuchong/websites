<script id = "race12a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race12={};
	myVars.races.race12.varName="Lu_Id_button_4__onchange";
	myVars.races.race12.varType="@varType@";
	myVars.races.race12.repairType = "@RepairType";
	myVars.races.race12.event1={};
	myVars.races.race12.event2={};
	myVars.races.race12.event1.id = "Lu_DOM";
	myVars.races.race12.event1.type = "onDOMContentLoaded";
	myVars.races.race12.event1.loc = "Lu_DOM_LOC";
	myVars.races.race12.event1.isRead = "False";
	myVars.races.race12.event1.eventType = "@event1EventType@";
	myVars.races.race12.event2.id = "Lu_Id_button_4";
	myVars.races.race12.event2.type = "onchange";
	myVars.races.race12.event2.loc = "Lu_Id_button_4_LOC";
	myVars.races.race12.event2.isRead = "True";
	myVars.races.race12.event2.eventType = "@event2EventType@";
	myVars.races.race12.event1.executed= false;// true to disable, false to enable
	myVars.races.race12.event2.executed= false;// true to disable, false to enable
</script>

