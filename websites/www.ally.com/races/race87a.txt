<script id = "race87a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race87={};
	myVars.races.race87.varName="Object[3557].uuid";
	myVars.races.race87.varType="@varType@";
	myVars.races.race87.repairType = "@RepairType";
	myVars.races.race87.event1={};
	myVars.races.race87.event2={};
	myVars.races.race87.event1.id = "Lu_Id_input_1";
	myVars.races.race87.event1.type = "onblur";
	myVars.races.race87.event1.loc = "Lu_Id_input_1_LOC";
	myVars.races.race87.event1.isRead = "False";
	myVars.races.race87.event1.eventType = "@event1EventType@";
	myVars.races.race87.event2.id = "search-input";
	myVars.races.race87.event2.type = "onblur";
	myVars.races.race87.event2.loc = "search-input_LOC";
	myVars.races.race87.event2.isRead = "True";
	myVars.races.race87.event2.eventType = "@event2EventType@";
	myVars.races.race87.event1.executed= false;// true to disable, false to enable
	myVars.races.race87.event2.executed= false;// true to disable, false to enable
</script>

