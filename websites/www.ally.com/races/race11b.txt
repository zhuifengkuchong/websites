<script id = "race11b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race11={};
	myVars.races.race11.varName="search-input__onkeypress";
	myVars.races.race11.varType="@varType@";
	myVars.races.race11.repairType = "@RepairType";
	myVars.races.race11.event1={};
	myVars.races.race11.event2={};
	myVars.races.race11.event1.id = "search-input";
	myVars.races.race11.event1.type = "onkeypress";
	myVars.races.race11.event1.loc = "search-input_LOC";
	myVars.races.race11.event1.isRead = "True";
	myVars.races.race11.event1.eventType = "@event1EventType@";
	myVars.races.race11.event2.id = "Lu_DOM";
	myVars.races.race11.event2.type = "onDOMContentLoaded";
	myVars.races.race11.event2.loc = "Lu_DOM_LOC";
	myVars.races.race11.event2.isRead = "False";
	myVars.races.race11.event2.eventType = "@event2EventType@";
	myVars.races.race11.event1.executed= false;// true to disable, false to enable
	myVars.races.race11.event2.executed= false;// true to disable, false to enable
</script>

