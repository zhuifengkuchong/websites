<script id = "race75b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race75={};
	myVars.races.race75.varName="Object[3557].uuid";
	myVars.races.race75.varType="@varType@";
	myVars.races.race75.repairType = "@RepairType";
	myVars.races.race75.event1={};
	myVars.races.race75.event2={};
	myVars.races.race75.event1.id = "Lu_Id_input_1";
	myVars.races.race75.event1.type = "onfocus";
	myVars.races.race75.event1.loc = "Lu_Id_input_1_LOC";
	myVars.races.race75.event1.isRead = "True";
	myVars.races.race75.event1.eventType = "@event1EventType@";
	myVars.races.race75.event2.id = "Lu_Id_input_2";
	myVars.races.race75.event2.type = "onfocus";
	myVars.races.race75.event2.loc = "Lu_Id_input_2_LOC";
	myVars.races.race75.event2.isRead = "False";
	myVars.races.race75.event2.eventType = "@event2EventType@";
	myVars.races.race75.event1.executed= false;// true to disable, false to enable
	myVars.races.race75.event2.executed= false;// true to disable, false to enable
</script>

