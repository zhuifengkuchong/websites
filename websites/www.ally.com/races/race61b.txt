<script id = "race61b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race61={};
	myVars.races.race61.varName="Object[3557].uuid";
	myVars.races.race61.varType="@varType@";
	myVars.races.race61.repairType = "@RepairType";
	myVars.races.race61.event1={};
	myVars.races.race61.event2={};
	myVars.races.race61.event1.id = "Lu_Id_input_4";
	myVars.races.race61.event1.type = "onchange";
	myVars.races.race61.event1.loc = "Lu_Id_input_4_LOC";
	myVars.races.race61.event1.isRead = "True";
	myVars.races.race61.event1.eventType = "@event1EventType@";
	myVars.races.race61.event2.id = "bank-login-option";
	myVars.races.race61.event2.type = "onchange";
	myVars.races.race61.event2.loc = "bank-login-option_LOC";
	myVars.races.race61.event2.isRead = "False";
	myVars.races.race61.event2.eventType = "@event2EventType@";
	myVars.races.race61.event1.executed= false;// true to disable, false to enable
	myVars.races.race61.event2.executed= false;// true to disable, false to enable
</script>

