<script id = "race80b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race80={};
	myVars.races.race80.varName="Object[3557].uuid";
	myVars.races.race80.varType="@varType@";
	myVars.races.race80.repairType = "@RepairType";
	myVars.races.race80.event1={};
	myVars.races.race80.event2={};
	myVars.races.race80.event1.id = "Lu_Id_button_1";
	myVars.races.race80.event1.type = "onblur";
	myVars.races.race80.event1.loc = "Lu_Id_button_1_LOC";
	myVars.races.race80.event1.isRead = "True";
	myVars.races.race80.event1.eventType = "@event1EventType@";
	myVars.races.race80.event2.id = "Lu_Id_button_2";
	myVars.races.race80.event2.type = "onblur";
	myVars.races.race80.event2.loc = "Lu_Id_button_2_LOC";
	myVars.races.race80.event2.isRead = "False";
	myVars.races.race80.event2.eventType = "@event2EventType@";
	myVars.races.race80.event1.executed= false;// true to disable, false to enable
	myVars.races.race80.event2.executed= false;// true to disable, false to enable
</script>

