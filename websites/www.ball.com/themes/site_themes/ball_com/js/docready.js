$ && $(document).ready(function () {

		var Utility = {
			
			"parseACTID": 47,
			
			parseTemplate: function (template_str, return_type, callback) {
				$.post('/index.php/?ACT=' + Utility.parseACTID, {
					tag: template_str
				}, function (data) {
					if (typeof callback === 'function')
					callback(data);
				}, return_type);
			}
		}

// --- Language Switcher --- //
	
		// Switch page language
		$('#language_switcher .control').change(function() {
			window.location = $(this).val();
		});
		
		$('#bbr_language_switcher').change(function () {
			var url = $(this).val();
			window.location = url;
		});

// --- Region Selector Colorbox --- //
		
/*
	    // Colorbox for the region selector
        $("#region-selector-trigger").colorbox({
			width: "50%", 
			inline: true, 
			href: "#region-selector"
		});	

*/
// --- Tabbed Content --- //
		if($('#tabbed-content').length > 0){
			  // The "tab widgets" to handle.
			var tabs = $('#tabbed-content');
			tabs.tabs();
 		}


// --- Long Sustainability Titles --- //
		if($('http://www.ball.com/themes/site_themes/ball_com/js/h1.tab').length > 0){
			$('http://www.ball.com/themes/site_themes/ball_com/js/h1.tab').each(function(){
				var charLength = $(this).text().length;
				console.log(charLength);
				
				if( charLength <= 22 ){
				  	//alert('long tab on the page');
				  	$(this).addClass('tab-short');				  
				}

			});
 		}	
		
// --- Galleria --- //		
		if($('#galleria').length > 0){
		
			 // Load the classic galleria theme
			Utility.parseTemplate('{theme_url}', 'text', function (theme_url) {
				
				Galleria.loadTheme(theme_url + 'plugins/galleria_new/themes/classic/galleria.classic.js');
	
				Utility.parseTemplate('{exp:json:entries channel="slideshowimages" dynamic="no"}', 'json', function (data) {
	
					// Format returned data for galleria
					var galleria_data = [];
	
					for (var i in data) {
						galleria_data.push({
							image:			data[i].slideshow_image,
							title:			data[i].title,
							description:	data[i].slideshow_image_description,
							link:			data[i].slideshow_image_href_tag
						});
					}
					
					 // Initialize Galleria
			    	$('#galleria').galleria({
			    		dataSource:	galleria_data,
						autoplay: 5000, 
						transition: 'slide',
						transitionSpeed: 900,
						easing: 'swing',
				        imageCrop: true,
				        thumbnails: 'empty',
						height: '360',
						width: '980',
						carousel: false,
						thumbMargin: 10,
						imageMargin: 0,
						showInfo: false,
						showCounter: false,
						opacity: 0.55,
						initialTransition:'fade'
			    	});
	
				});			
			});
		}
		
// --- Accordion --- //
	
	$('#accordion .item').each(function (i, item) {
		//$(item).css('height', $(item).outerHeight());
	});

	$('#accordion a.heading').click(function(e) {
		e.preventDefault();
	
		$parent = $(this).parent();
		$this = $(this);
	
		function slideSiblings(context, callback) {
			context.siblings('.item').slideToggle(600, 'easeOutQuad', function() {
				context.parent().toggleClass('current');
				if (typeof callback === 'function') { callback() };
			});		
		}
	
		if($parent.hasClass('current')) {
			$.scrollTo($parent, 1000);
			slideSiblings($(this));
		} else {
			if ($('#accordion .current .item').length) {
				$('#accordion .current .item').slideUp('slow', 'linear', function() {
					$(this).parent().removeClass('current');
					$.scrollTo($parent,1000);	// must be in this callback, otherwise scrollto is off
				});
				slideSiblings($this);
			} else {
				$.scrollTo($parent,1000);
				slideSiblings($this);
			}
		}
	});

// --- FAQ search --- //	
		
	$('#faq-keywords').keyup(function() {
			var search = $(this).val();
			if(search.length >= 3) {
				search_faq(search);	
			}
			else{
				$('div.question').show();
				$('div.question').find('.answer').hide();
			}
		}
	)		
	
	function search_faq(search) {
		$('div.question').each(function(index) {
			var str= $(this).text()
			var regex = new RegExp(search, 'i');
			var found = str.search(regex);
			if (found == -1) {
				$(this).hide();
				$(this).find('.answer').hide();
			}
			else {
				$(this).show();
				$(this).find('.answer').show();
			}
		});				
	}
	
// --- Contacts region filter --- //	
	
	filter_contacts_by_region();
	
	function filter_contacts_by_region() {

		var division_filter;
		init();

		function init() {
			
			// assign the change handler to the region filter
			$('#contact-filter-form #region-division-filter').change(function() {
				//	filter_contacts_by_division($(this).val());
					filter_contacts_by_entry_id($(this).val());
			
			
			});

		} 

		// Filter the visible contacts by division
		function filter_contacts_by_division(selected_division) {
			$("#contact-list .contact").each(function() {
				if(selected_division == 0){
					$(this).show();
				} else {
					if($(this).attr('data-division-id') == selected_division) {
						$(this).show();
					} else {
						$(this).hide();
					}
				}
			});
		}

		// Filter the visible contacts by division
		function filter_contacts_by_entry_id(selected_division) {
			$("#contact-list .contact").each(function() {
				if(selected_division == 0){
					$(this).show();
				} else {
					if($(this).attr('data-entry-id') == selected_division) {
						$(this).show();
					} else {
						$(this).hide();
					}
				}
			});
		}



	}
	
// --- Locations filter --- //

	// Get the region id if it is set
	Utility.parseTemplate('{exp:session_variables:get name="region_id"}', 'text', function (region_id) {
		$('#locations-filter-form #region-filter').val(region_id);
		
		function filter_loc() {
			var el = '#locations-filter-form #region-filter';
			var select = $(el);
			var selected_region = $(el).val();

			if(selected_region == 0) {
				$('#accordion.location-list .location').show();
			} else {
				$("#accordion.location-list .location").each(function() {
					$(this).attr('data-region');
					if($(this).attr('data-region') == selected_region) {
						$(this).show();
					} else {
						$(this).hide();
					}
				});
			}
		}
		
		$('#locations-filter-form #region-filter').change(function() {
			filter_loc();
		});
	});
	
// --- Product Catalog --- //

	function product_catalog_category_init() {
		
		/*
		if (location.hash) {
			var hashval = location.hash.substr(1);
			$('#product-size-select').val(hashval);
			update_size_view(hashval);
		}
		*/
	
		$('#show-all-product-categories').click(function (e) {
			e.preventDefault();
			$('#product-categories').slideToggle();
		});

		$('#product-size-select').change(function () {
			var c = $(this).val();
			$('.product-size-div').hide().removeClass('hide');
			$('.' + c).show();
		});	
		
		$('.category-product-size-select').change(function () {
			if ($(this).val()) {
				window.location = $(this).val();
			}
		});
		
		$('#product-category-region-filter').change(function () {
			var v = $(this).val();
			if (!v) {
				$('.product-category-region').show();
			} else {
				$('.product-category-region').hide();
				$('div.' + v).show();				
			}
		});
		
		$('#product-view-filter').change(function () {
			window.location = $(this).val();
		});
		
		$('.product_wrap').click(function () {
			var opt = $(this).find('.product-link');
			window.location = opt.attr('href');
		});
	}	
	
	product_catalog_category_init();
});

// --- Retiree Request --- //

	$('form[name=retiree_request_form]').submit(function (e) {
		e.preventDefault();
		$('.form-message').remove();
		$that = $(this);
		var a = $(this).attr('action');
		$.post(a, $(this).serialize(), function (data) {
			if (data.success) {
				$that.before("<div class=\"green-copy form-message\"><p>Thank you! Your request has been received.</p></div>");
			} else {
				$.each(data, function(i , msg) {
					$('input[name='+msg.label+']').before("<div class=\"red-copy form-message\">" + msg.msg + "</div>");
				});
			}
		}, 'json');
	});