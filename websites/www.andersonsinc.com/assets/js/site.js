$('#home-page-slider').owlCarousel({
    items:1,
    loop:true,
    nav:true,
    dots:true,
    dotsEach:1,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        }
    },
    navText:['navigateleft','navigateright'],
    navContainerClass:'slide-navigation',
    navClass:['slide-previous ss-icon ss-gizmo','slide-next ss-icon ss-gizmo'],
    dotClass:'slide-page',
    dotsClass:'slide-pagination',
    responsiveRefreshRate:10,
    autoplay:true,
    autoplayHoverPause:true,
    autoplaySpeed:750,
    navSpeed:500,
    dotSpeed:500,
    autoplayTimeout:5000
});

$('#page-header-slider').owlCarousel({
    items:1,
    loop:true,
    nav:true,
    dots:true,
    dotsEach:1,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        }
    },
    navText:['navigateleft','navigateright'],
    navContainerClass:'slide-navigation',
    navClass:['slide-previous ss-icon ss-gizmo','slide-next ss-icon ss-gizmo'],
    dotClass:'slide-page',
    dotsClass:'slide-pagination',
    responsiveRefreshRate:10,
    autoplay:true,
    autoplayHoverPause:true,
    autoplaySpeed:750,
    navSpeed:500,
    dotSpeed:500,
    autoplayTimeout:5000,
    autoHeight:true
});

$('.testimonials').owlCarousel({
    items:1,
    loop:true,
    nav:false,
    dots:true,
    dotsEach:1,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        }
    },
    dotClass:'slide-page',
    dotsClass:'slide-pagination',
    responsiveRefreshRate:10
});

$('#andbeyond-mission-slider').owlCarousel({
    items:1,
    loop:true,
    nav:false,
    dots:true,
    dotsEach:1,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        }
    },
    dotClass:'slide-page',
    dotsClass:'slide-pagination',
    responsiveRefreshRate:10,
    autoplay:true,
    autoplayHoverPause:true,
    autoplaySpeed:750,
    navSpeed:500,
    dotSpeed:500,
    autoplayTimeout:5000
});

$('#andbeyond-testimonial-slider').owlCarousel({
    items:1,
    loop:true,
    nav:false,
    dots:true,
    dotsEach:1,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        }
    },
    dotClass:'slide-page',
    dotsClass:'slide-pagination',
    responsiveRefreshRate:10,
    autoplay:true,
    autoplayHoverPause:true,
    autoplaySpeed:750,
    navSpeed:500,
    dotSpeed:500,
    autoplayTimeout:5000
});

$(window).load(function(){
	loadFade();
	if($('body').hasClass('tabbed-layout')){
		var hash = window.location.hash;
		hash && $('ul.nav a[href="' + hash + '"]').tab('show');
	}
	else if($('body').hasClass('scrolling-layout')){
		if(window.location.hash){
			window.scrollTo(0, 0);  
	        setTimeout(function() {
	            window.scrollTo(0, 0);    
	        }, 1);
			$hash = window.location.hash.substring(1);
			setTimeout(function() {$('body').scrollTo($('#'+$hash), 500,  {offset:getOffset()} );}, 50);
	  	}
	}
	$(window).on("throttledresize", function(event){
		centerCaptions();
		resizeSidebar();
		facilityStyle();
		refreshSliders();
		refreshScroll();
		scrollCheck();
	});
	setTimeout(function (){
		$(window).trigger('resize');
	}, 250);
});

function centerCaptions(){
	$sliderHeight = $('.slider').outerHeight(false);
	$('.slide-overlay').each(function(){
		$captionHeight = $(this).outerHeight(false);
		$newTop = ($sliderHeight - $captionHeight)/2;
		$(this).css('top',$newTop);
	});
}

function refreshSliders(){
	var owl = $('.owl-carousel');
	owl.trigger('to.owl.carousel', [0,500]);
}

function forceListView(){
	$('a[href="#list"]').tab('show');
}

function facilityStyle(){
	if ($('body').hasClass('breakpoint-992')){
		$mapHeight = $('.location-map').first().actual('height');
		$('.facility-group-nav, .facility-content').css('min-height',$mapHeight);
	}
	else{
		$('.facility-group-nav, .facility-content').css('min-height',0);
	}
	if (!$('body').hasClass('breakpoint-768')){
		forceListView();
	}
}

//initialize settings for Breakpoint.js
$(document).ready(function() {
	$(window).setBreakpoints({
	    distinct: false, 
	    breakpoints: [768, 992] 
	});
	$(window).bind('exitBreakpoint992',function() {
		$('.vertically-centered').removeAttr('style');
	});
});


$(document).ready(function(){	
	$('.tabbed-layout #secondary-navigation a').click(function(e) {
	    $(this).tab('show');
	    var scrollmem = $('body').scrollTop();
	    window.location.hash = this.hash;
	    $('html,body').scrollTop(scrollmem);
	});
  
	$('.tabbed-layout #secondary-navigation a').click(function(e){
		setTimeout(function (){
			$(window).trigger('resize');
		}, 250);
	});
});

function getOffset(){
	var offset = 0;
	if ($('body').hasClass('breakpoint-992')){
		offset += -226;
	}
	else{
		offset += -72;
	}
	if ($(window).scrollTop() < 0){
		offset += -137;
	}
	return offset;
}

function resizeSidebar(){
	if ($('body').hasClass('breakpoint-768')){
		$contentHeight = $('#content').outerHeight(false);
		$('#sidebar').css('min-height',$contentHeight);
	}
	else{
		$('#sidebar').css('min-height',0);
	}
}

function refreshScroll(){
	$('body').scrollspy({target: '.secondary-navigation-container', offset: 226});
	$('body').scrollspy('refresh');
	$('body').scrollspy('process');
	$('.scrolling-layout #secondary-navigation').localScroll({target:'body', hash:true, offset: getOffset(), easing: 'easeInOutCubic', duration: 300, lock:true});
}

$('.dropdown').on('show.bs.dropdown', function(e){
	$(this).find('.dropdown-menu').first().stop(true, true).slideDown(300,'easeInOutCubic');
});

$('.dropdown').on('hide.bs.dropdown', function(e){
	$(this).find('.dropdown-menu').first().stop(true, true).slideUp(400,'easeInOutCubic');
});

$(document).ready(function() {
	$(".group-video, .andbeyond-video").fitVids();
});

$(document).ready(function() {
    $window = $(window);
   
    $window.scroll(function(){
    	$screenTop = $window.scrollTop() + $('#main-navigation').outerHeight(false);
		if ($('#secondary-navigation').length){
			$('.secondary-navigation-container').each(function(){
				$navContainer = $(this);
			});
			$navTop = $('.secondary-navigation-container').first().offset().top;
			$distance = $navTop - $screenTop;
			
			if ($distance <= 0){
		    	$('#secondary-navigation').addClass('sticky');
			}
			else{
				$('#secondary-navigation').removeClass('sticky');
			}	
		}
	});
	
	$window.scroll(function(){
		scrollCheck();
	});
});

function scrollCheck(){
	if ($window.scrollTop() > 106){
		$('body').addClass('scrolled');
	}
	else{
		$('body').removeClass('scrolled');
	}	
}

function loadFade(){
	$('.load-fade').addClass('load-faded');
}