<script id = "race27a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race27={};
	myVars.races.race27.varName="Object[3575].uuid";
	myVars.races.race27.varType="@varType@";
	myVars.races.race27.repairType = "@RepairType";
	myVars.races.race27.event1={};
	myVars.races.race27.event2={};
	myVars.races.race27.event1.id = "swui-departure-date-2";
	myVars.races.race27.event1.type = "onkeypress";
	myVars.races.race27.event1.loc = "swui-departure-date-2_LOC";
	myVars.races.race27.event1.isRead = "False";
	myVars.races.race27.event1.eventType = "@event1EventType@";
	myVars.races.race27.event2.id = "swui-arrival-date-2";
	myVars.races.race27.event2.type = "onkeypress";
	myVars.races.race27.event2.loc = "swui-arrival-date-2_LOC";
	myVars.races.race27.event2.isRead = "True";
	myVars.races.race27.event2.eventType = "@event2EventType@";
	myVars.races.race27.event1.executed= false;// true to disable, false to enable
	myVars.races.race27.event2.executed= false;// true to disable, false to enable
</script>

