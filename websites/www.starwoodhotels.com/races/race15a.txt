<script id = "race15a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race15={};
	myVars.races.race15.varName="swui-arrival-date-2__onfocus";
	myVars.races.race15.varType="@varType@";
	myVars.races.race15.repairType = "@RepairType";
	myVars.races.race15.event1={};
	myVars.races.race15.event2={};
	myVars.races.race15.event1.id = "Lu_DOM";
	myVars.races.race15.event1.type = "onDOMContentLoaded";
	myVars.races.race15.event1.loc = "Lu_DOM_LOC";
	myVars.races.race15.event1.isRead = "False";
	myVars.races.race15.event1.eventType = "@event1EventType@";
	myVars.races.race15.event2.id = "swui-arrival-date-2";
	myVars.races.race15.event2.type = "onfocus";
	myVars.races.race15.event2.loc = "swui-arrival-date-2_LOC";
	myVars.races.race15.event2.isRead = "True";
	myVars.races.race15.event2.eventType = "@event2EventType@";
	myVars.races.race15.event1.executed= false;// true to disable, false to enable
	myVars.races.race15.event2.executed= false;// true to disable, false to enable
</script>

