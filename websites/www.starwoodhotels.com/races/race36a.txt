<script id = "race36a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race36={};
	myVars.races.race36.varName="Object[3575].uuid";
	myVars.races.race36.varType="@varType@";
	myVars.races.race36.repairType = "@RepairType";
	myVars.races.race36.event1={};
	myVars.races.race36.event2={};
	myVars.races.race36.event1.id = "ysearchinput_searchType";
	myVars.races.race36.event1.type = "onchange";
	myVars.races.race36.event1.loc = "ysearchinput_searchType_LOC";
	myVars.races.race36.event1.isRead = "False";
	myVars.races.race36.event1.eventType = "@event1EventType@";
	myVars.races.race36.event2.id = "ysearchinput";
	myVars.races.race36.event2.type = "onchange";
	myVars.races.race36.event2.loc = "ysearchinput_LOC";
	myVars.races.race36.event2.isRead = "True";
	myVars.races.race36.event2.eventType = "@event2EventType@";
	myVars.races.race36.event1.executed= false;// true to disable, false to enable
	myVars.races.race36.event2.executed= false;// true to disable, false to enable
</script>

