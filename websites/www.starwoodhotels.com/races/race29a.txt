<script id = "race29a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race29={};
	myVars.races.race29.varName="Object[3575].uuid";
	myVars.races.race29.varType="@varType@";
	myVars.races.race29.repairType = "@RepairType";
	myVars.races.race29.event1={};
	myVars.races.race29.event2={};
	myVars.races.race29.event1.id = "ysearchinput_propertyIds";
	myVars.races.race29.event1.type = "onkeypress";
	myVars.races.race29.event1.loc = "ysearchinput_propertyIds_LOC";
	myVars.races.race29.event1.isRead = "False";
	myVars.races.race29.event1.eventType = "@event1EventType@";
	myVars.races.race29.event2.id = "ysearchinput_searchType";
	myVars.races.race29.event2.type = "onkeypress";
	myVars.races.race29.event2.loc = "ysearchinput_searchType_LOC";
	myVars.races.race29.event2.isRead = "True";
	myVars.races.race29.event2.eventType = "@event2EventType@";
	myVars.races.race29.event1.executed= false;// true to disable, false to enable
	myVars.races.race29.event2.executed= false;// true to disable, false to enable
</script>

