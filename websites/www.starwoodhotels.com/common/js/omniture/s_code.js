/*!
 * jQuery JavaScript Library v1.6.1
 * http://jquery.com/
 *
 * Copyright 2011, John Resig
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * Includes Sizzle.js
 * http://sizzlejs.com/
 * Copyright 2011, The Dojo Foundation
 * Released under the MIT, BSD, and GPL Licenses.
 *
 * Date: Thu May 12 15:04:36 2011 -0400
 */

/* SiteCatalyst code version: H.24.1.
Copyright 1996-2011 Adobe, Inc. All Rights Reserved
More info available at http://www.omniture.com */

var s=s_gi(s_omnitureAccount)
/************************** CONFIG SECTION **************************/
/* Conversion Config */
s.currencyCode="USD";
/* Link Tracking Config */
s.trackDownloadLinks=true;
s.trackExternalLinks=true;
s.trackInlineStats=true;
s.linkDownloadFileTypes="exe,zip,wav,mp3,mov,mpg,avi,wmv,pdf,doc,docx,xls,xlsx,ppt,pptx";
s.linkInternalFilters="javascript:,starwood.com,starwoodhotels,starwoodspecialoffers,spghoneymoon,starwooddestinations,hawaiiexclusive,starwoodchicago,fireflyhawaii,diplomatresort,starwoodhotels.com,fourpoints.com,luxurycollection.com,spg.com,sheraton.com,stregis.com,westin.com,preferredguest.com,whotel.com,whotels.com,atdmt.com,starwoodspecialoffers.com,starwooddestinations.com,stregisnewsletter.com,thelobby.com,starwoodrsvp.com,sheratonangebote.com,emusic.com,whotelstheworld.com,wthecard.rdai.com,starwoodspacollection.com,starwoodpromos.com,luxurycollectionpromotions.com,localhost:8080,wtaipei.com,starwood-ap.com,luxurycollectiondestinations.com,spghotescapes.com,wseoul.com,americas.starwoodoffers.com,starwoodpromos.com,westinmaui.com,thephoenician.com,w-barcelona.com,sheraton-waikiki.com,sheraton.pl,westinkaanapali.com,sheraton-maui.com,wsingaporesentosacove.com,royal-hawaiian.com,lemeridienmunich.com,sheratonclubroom.com,moana-surfrider.com,diplomatresort.com,wlondon.co.uk,westinny.com,sfpalace.com,wsouthbeach.com,sheratonmacao.com,wnewyork.com,sheratondenverdowntown.com,spg-asiapacific.com,stregisprinceville.com,sheraton-kauai.com,westinessence.com,grosvenorhouse-dubai.com,sheratonnewyork.com,stregisborabora.com,westinplayaconchal.com,danielihotelvenice.com,sheratontoronto.com,lemeridienetoile.com,westingrandmunich.com,sheratonkona.com,westinminaseyahi.com,upg.starwoodpromos.com,leroyalmeridien-dubai.com,north-america.starwoodoffers.com,w-hongkong.com,westinresortstjohn.com,starwoodmeetings.com,whollywoodhotel.com,westinharbourcastletoronto.com,wfortlauderdalehotel.com,sheratonbostonhotel.com,stregissaadiyatisland.com,lemeridienistanbuletiler.com,deals.sheraton.com,turnberryresort.co.uk,thenines.com,sheratondallashotel.com,westinsunsetkeycottages.com,westinprinceville.com,sheratonmaldives.com,lemeridien-mauritius.com,sheratonedinburgh.co.uk,westingrandberlin.com,westinpalacemadrid.com,westinnewyorkgrandcentral.com,sheratonneworleans.com,thewestinparis.com,westinbostonwaterfront.com,princess-kaiulani.com,stregisbalharbour.com,westinnusaduabali.com,westinseattle.com,sheratonparklane.com,westinpeachtreeplazaatlanta.com,westinlapalomaresort.com,al-maha.com,whotelaustin.com,lemeridien-dubai.com,sheratonmiragegoldcoast.com,wdallasvictory.com,westinsavannah.com,thelagunabali.com,theandaman.com,sheratonarabellapark.com,wsanfrancisco.com,aloftlondonexcel.com,grandebretagne.gr,sheratonportdouglas.com,national.ru,leroyalmeridienhamburg.com,lemeridienstuttgart.com,westinaruba.com,stregissingapore.com,wvieques.com,westincopleyplaceboston.com,sheratonatthewharf.com,hotelbristolwarsaw.pl,sheratongrandesukhumvit.com,sheratonontheparksydney.com,westincapetown.com,sheratonsalobre.com,westinhiltonheadisland.com,westinwhistler.com,lemeridien-borabora.com,sheratonlax.com,bookcadillacwestin.com,stregisbali.com,sheratonfrankfurtairport.com,wverbier.com,wseattle.com,sheratonathlonehotel.com,westin.pl,wchicago-lakeshore.com,lemeridienvienna.com,aloftbangkoksukhumvit11.com,sheratonsandiegohotel.com,imperialvienna.com,sheratonnashvilledowntown.com,hotel-marquesderiscal.com,wmontrealhotel.com,sheratonseattle.com,royalorchidsheraton.com,sheratonporto.com,wneworleans.com,stregismauritius.com,wwashingtondc.com,lemeridien-minaseyahi.com,westingrandfrankfurt.com,w-barcelona.es,stregisdoha.com,wnewyorktimessquare.com,sheratonvancouver.com,thewestinmichiganavenue.com,westinindianapolis.com,thewestindublin.com,sheratonsingapore.com,wparisopera.com,thegrittipalace.com,westinbayshore.com,westinchicago.com,westinkeywestresort.com,westinvegas.com,us.starwoodoffers.com,sheratonjumeirahbeach.com,plazaatheneebangkok.com,sheratonpalace.ru,stregisabudhabi.com,westincharlottehotel.com,elementtimessquare.com,stregissanfrancisco.com,deals.westin.com,lemeridienmalta.com,lemeridien-alaqah.com,westinbellevuedresden.com,wildhorsepassresort.com,westinnapa.com,sheratontelaviv.com,wlosangeles.com,lemeridienpiccadilly.co.uk,sheratoncentremontreal.com,latin-america.starwoodoffers.com,pulitzeramsterdam.com,sheratonparkanaheim.com,whoteldoha.com,wretreatmaldives.com,westinftlauderdalebeach.com,sheratonskyline.com,sheratonfiji.com,sheratonheathrowhotel.com,watlantamidtown.com,sheratonsuites.com,thewsandiegohotel.com,hotelprincedegalles.fr,sheratonnoosaresort.com,metropolpalace.com,westincapecoral.com,wminneapolishotel.com,sheratonnassau.com,stregisaspen.com,schlossfuschlsalzburg.com,watlantadowntown.com,translate.googleusercontent.com,sheratonkeywest.com,sheratonuniversal.com,sheratondoha.com,fourpointsbolzano.com,westinleipzig.com,wfrenchquarter.com,hotelfuerstenhofleipzig.com,wscottsdalehotel.com,lemeridiennuernberg.com,westinbluemountain.com,aloftabudhabi.com,westinsandiego.com,westinpoinsettgreenville.com,westinmissionhills.com,westinlakelasvegas.com,stregisbahiabeach.com,hotelelephantweimar.com,thewestinottawa.com,westincostanavarino.com,fourpointsbangkoksukhumvit.com,westinneworleanscanalplace.com,westinriverfrontbeavercreek.com,lemeridienkotakinabalu.com,bluepalace.gr,westincalgary.com,stregisdeervalley.com,sheratonlincolnharbor.com,sheratongolfroma.com,lemeridienkualalumpur.com,sheratontribeca.com,sheratonrome.com,sheratonlacaleta.com,sheratonphiladelphiadowntown.com,sheratoncasablanca.com,westinlosangelesairport.com,nakaislandphuket.com,westinjerseycitynewport.com,wchicagocitycenter.com,westinwashingtondccitycenter.com,sheratonnuernberg.com,westinorlandouniversal.com,wnewyorkunionsquare.com,hotelkamp.fi,sheratonabudhabihotel.com,westindenverdowntown.com,westingrandvancouver.com,sheratonlisboa.com,sheratonphoenixdowntown.com,sheratonalgarve.com,westincrowncenterkansascity.com,castillosonvidamallorca.com,lemeridienkhaolak.com,westinabudhabigolfresort.com,fourpointslax.com,westinpittsburgh.com,starwood-ap.cn,lemeridienlimassol.com,sheratonportsmouth.com,sheratonwestpark.com,hotel-mariacristina.com,lemeridienabudhabi.com,aloftdallasdowntown.com,princedegallesparis.com,sheratoncarlsbad.com,sheratonmiamiairport.com,aloftnewyorkbrooklyn.com,sheratonlosangelesdowntown.com,aloftchicagocitycenter.com,sheratonmadridmirasierra.com,goldenerhirsch.com,thehotelivy.com,watlantabuckhead.com,theparktowerknightsbridge.com,westinrichmond.com,whotelbangkok.com,sheratonbrooklyn.com,lemeridienmontecarlo.com,sheratonpuertoricohotelcasino.com,bristolvienna.com,westinsouthfielddetroit.com,login1.sheraton-surabaya.com,westingrandcayman.com,stregismardavall.com,westinohare.com,sheratonhuahin.com,westindallasfortworthairport.com,westinvalencia.com,thewestinkualalumpur.com,westintampaharbourisland.com,westinlongbeachhotel.com,sheraton-rio.com,sheratonoldsanjuan.com,thewestinedmonton.com,aloftmiamidoral.com,hotel-alfonsoxiii-sevilla.com,keratonattheplazajakarta.com,sheratonduesseldorfairport.com,fourpointssheikhzayedroad.com,lemeridiennice.com,wnewyorkdowntown.com,sheratonfortworth.com,fourpointssydney.com,lemeridienvilnius.com,westincincinnati.com,lemeridienbangkokpatpong.com,theluxurycollectionroadtrips.com,westinpalacemilan.com,sheratondubaimalloftheemirates.com,sheratonstationsquare.com,westincoralgables.com,stregishoustonhotel.com,wistanbul.com,lemeridienkohsamui.com,lemeridienparkhotelfrankfurt.com,westinsnowmass.com,westingaslamp.com,sheratonsyracuse.com,sheratonmyrtlebeach.com,westinprincetoronto.com,sheratonphiladelphiasocietyhill.com,thewestinnovascotian.com,sheratonparsippany.com,westinlangkawi.com,fourpointsorlandostudiocity.com,vanabellekohsamui.com,westineuropareginavenice.com,westinlakemary.com,sheratongunter.com,conventodoespinheiro.com,westinannapolis.com,sheratonsacramento.com,alofttallahassee.com,stregiswashingtondc.com,wstpetersburg.com,sheratonreadhouse.com,lemeridientahiti.com,sheratoncongressfrankfurt.com,sheratonkansascityhotel.com,fairfaxhoteldc.com,sheratonparkway.com,fourpointstimessquare.com,westinmammoth.com,lemeridienlavsplit.com,stregisfamilytraditions.com,westinvirginiabeach.com,leroyalmeridienabudhabi.com,thewestingalleriadallas.com,sheratontampariverwalk.com,lemeridienbudapest.com,westincolumbus.com,sheratoncavaliercalgary.com,westingovernormorris.com,deals.fourpoints.com,westingeorgetown.com,sheratonmallorcagolfhotel.com,westintampabay.com,sheratonfuschlseesalzburg.com,aloftbrussels.com,westinhuntsville.com,westinsohnaresort.com,sheraton.nl,westinatlantaairport.com,sheratonankara.com,sheratonprague.com,lemeridienchambers.com,sheratoninnerharbor.com,sheratoncavaliersaskatoon.com,aloftkualalumpursentral.com,westingrandesukhumvit.com,lemeridienphuketbeachresort.com,westinfinds.com,westinfortlauderdalehotel.com,sheratonsonoma.com,lemeridienbarcelona.com,sheratonmunichairport.com,stregisbangkok.com,lemeridienra.es,westinsiraybay.com,sheratonstockholm.com,fourpointsfrenchquarter.com,westinlombard.com,westindallasparkcentral.com,westinbellevuehotel.com,sheratonparisairport.com,sheratonatlanticcity.com,lemeridiendallasstoneleigh.com,sheratonsaopaulowtc.com,stregisaficionado.com,fourpointsbrentwood.com,sheratonsalzburg.com,fourpointssihlcity.com,aloftmilwaukeedowntown.com,westinalexandria.com,westingalleriahoustonhotel.com,aloftcharlotteuptown.com,hoteldesindes.nl,sheratonarlingtonhotel.com,westinstmaarten.com,sheratoncolumbia.com,westinflorence.com,westinnorthshore.com,kinggeorgeathens.com,sheratonhamilton.com,sheratonorlandonorth.com,westinarlingtongateway.com,sheratonpelikanhannover.com,westindragonaramalta.com,fourpointsdc.com,sheratontysonscorner.com,westinsouthcoastplaza.com,hotelkamp.com,fourpointshistoricsavannah.com,westinstonebriar.com,lemeridienatlantaperimeter.com,sheratonfairplex.com,westinmemphisbealestreet.com,westinatlantanorth.com,aloftchicagoohare.com,sheratonhanoi.com,sheratonakron.com,sheratonnorfolkwaterside.com,sheratonlajolla.com,sheratoncommander.com,sheratonsuiteshouston.com,westinriverwalksanantonio.com,romanoscostanavarino.com,aloftaustinatthedomain.com,sheratonburlington.com,alofttucsonuniversity.com,wstpetersburg.ru,sheratonbalikuta.com,sheratonokc.com,lemeridienbruxelles.fr,aloftharlem.com,sheratonstlouiscitycenter.com,canyonsuites.com,fourpointssohovillage.com,sheratonpattayaresort.com,sheratonpasadena.com,sheratonaddis.com,lemeridienbarcelona.es,westinnationalharbor.com,sheratonraleigh.com,lemeridienalkhobar.com,sheratonmahwah.com,westinbuckheadatlanta.com,sheratonstockholm.se,sheratonoffenbach.com,sheratongenova.com,sheratoncolumbuscapitolsquare.com,aloftnashvillewestend.com,sheratonsuitesfortlauderdale.com,aloftportlandairport.com,sheratonessen.com,sheratonbirmingham.com,sheratonkrabi.com,sheratondeiradubai.com,stregisflorence.com,sheratonbaltimorecitycenter.com,aloftchapelhill.com,wparisopera.fr,sheratonindianapoliscitycentre.com,sheratonelpaular.com,westinportland.com,fourpointsbarcelonadiagonal.com,westinchicagonorthwest.com,sheratonbaltimorenorth.com,sheratonmeadowlands.com,fourpointstorontoairport.com,lemeridienfishermanscove.com,sheratonhuahinpranburi.com,westinhoustondowntown.com,sheratongambiahotel.com,beatthetimer.com,sheratonmilanmalpensa.com,thewestinparis.fr,sheratonnicolausbari.com,hotel-alfonsoxiii-seville.com,fourpointschelsea.com,deals.alofthotels.com,whoboken.com,sheratontucson.com,sheraton-barra.com,alofttulsa.com,westintorontoairport.com,sheratonbursahotel.com,sheratonmadison.com,lemeridien-barbarons.com,aloftarundelmills.com,caladivolpe.com,sheratondavoshotelwaldhuus.com,sheratontorontoairport.com,sheratonsuitesplantation.com,sheratonannarbor.com,fourpointsmunichcentral.com,westinphoenixdowntown.com,westinpuntacana.com,sheratonoverlandpark.com,sheratoncountryclubplaza.com,stregisrome.com,aloftvaughanmills.com,fourpointsdestinfortwaltonbeach.com,sheratonindianapoliskeystonecrossing.com,aloftsanantonioairport.com,fourpointsdetroitairport.com,sheratonfitness.com,sheratonframingham.com,fourpointsburdubai.com,sheratonbrusselsairport.com,sheratonsuitesgalleriaatlanta.com,sheratonsharm.com,sheratonmissionvalley.com,sheratonmilanmalpensa.it,sheratonottawa.com,westinsavannahspa.com,sheratonsteamboatresort.com,sheratonclaytonhotel.com,lemeridienchiangrai.com,westinrome.com,fourpointsashevilledowntown.com,sheratonharrisburghershey.com,lemeridiennfis.com,sheratonbratislava.sk,fourpointsbiloxi.com,arionresortathens.com,sheratonclubdespins.com,sheratonvancouverairport.com,westinedinagalleria.com,aloftmountlaurel.com,aloftcharlotteballantyne.com,fourpointsstcatharines.com,sheraton-somabay.com,fourpointsolympiapark.com,sheratonbangalore.com,lemeridienbrussels.com,sheratongatewaytorontoairport.com,sheratonsuitesdallas.com,sheratonorlandodowntown.com,aloftlascolinas.com,sheratonsuitescolumbus.com,westinbwi.com,sheratonsuitesalexandria.com,aloftbirminghamsohosquare.com,sheratonagourahills.com,sheratonbrussels.com,westinhoustonmemorialcity.com,sheratonguildford.com,elementmiamiairport.com,sheratondianamajestic.it,sheratonanchorage.com,sheratonstamford.com,sheratontarrytown.com,aloftplano.com,westinshanghai.com,sheratoniowacity.com,sheratonpadova.com,sheratonbwiairport.com,sheratonanaheimsouth.com,sheratonmilwaukeebrookfield.com,sheratonsuiteswilmington.com,sheratoncrescent.com,sheratonnhatrang.com,sheratonlagos.com,westintysonscornerhotel.com,sheratonamsterdamairporthotel.com,aloftnashvillecoolsprings.com,sheratondabahia.com,aloftbroomfielddenver.com,lemeridienibom.com,westindulles.com,sheratondianamajestic.com,sheratoncerritos.com,sheratonalbuquerqueuptown.com,lemeridienangkor.com,westinprinceton.com,sheratonsilverspring.com,sheratonsaltlakecityhotel.com,aloftfrisco.com,aloftchesapeake.com,fourpointscalgarywest.com,lemeridienchiangmai.com,fourpointslondonontario.com,sheratonvitoria.com,fourpointscocoabeach.com,aloftmontrealairport.com,aloftcharlestonairport.com,sheratonannapolis.com,sheratonparisairport.fr,westinmumbaigardencity.com,fourpointslagos.com,fourpointsmilan.com,sheratonsuitestampaairport.com,westingurgaon.com,elementomahamidtowncrossing.com,sheratonimperialkualalumpur.com,vedema.gr,sheratoncolonial.com,sheratonedison.com,aloftjacksonvilletapestrypark.com,lemeridienversailleshotel.com,fourpointsdowntowndubai.com,sheratonneuesschloss.com,sheraton-poa.com,sheratonrtp.com,westinathens.com,sheratonmemphisdowntown.com,sheratonatlantaairport.com,sheratonlouisvilleriverside.com,hoteldesindesthehague.com,fourpointstempe.com,sheratonmiramarresort.com,westinoakshouston.com,sheratongranddfwairport.com,sheratonwilmingtonsouth.com,sheratonbratislava.com,sheratoneatontown.com,sheratonoran.com,sheraton-asuncion.com,lemeridiennoumea.com,westindetroitmetroairport.com,sheratonrhodesresort.com,westinpunekoregaonpark.com,sheratontampaeast.com,sheratonkuwait.com,sheratonneedham.com,sheratonvirginiabeach.com,fourpointscalgaryairport.ca,elementhoustonvintagepark.com,aloftphoenixairport.com,lemeridienrimini.com,fourpointszaporozhye.com,sheratonbrookhollow.com,fourpointsyork.com,lemeridienamman.com,aloftontario-rc.com,westinportlandharborview.com,fourpointssacramentoairport.com,westinsydney.com,sheratonammanalnabil.com,leroyalmansourmeridien.com,sheratonriyadh.com,sheratonportlandairport.com,westinvancouverairport.com,sheratonpretoria.com,lemeridien-pyramids.com,fourpointssanantonionorthwest.com,wretreatbali.com,sheratonjfk.com,sheratonbellevue.com,sheratonrichmondparksouth.com,lemeridienogeyiplace.com,sheratonamsterdamairport.com,lemeridiendahab.com,fourpointssandiegodowntown.com,hotelpulitzeramsterdam.nl,wboston.com,sheratonreston.com,sheratonsunnyvale.com,aloftbursa.com,sheratonjacksonville.com,fourpointsraleighdurhamairport.com,westinbirmingham.com,fourpointskingston.com,fourpointskelownaairport.com,sheratonhoustonwest.com,fourpointssanrafael.com,sheratonchicagoohare.com,sheratonphoenixairport.com,fourpointslosangeleswestside.com,reviews.starwoodpromos.com,westinshanghai.cn,sheratonbatumi.com,sheratondetroitmetroairport.com,sheratonsuiteselkgrove.com,fourpointsmilwaukeenorth.com,sheratonmetairieneworleans.com,pinecliffsresidencesuites.com,sheratonminneapolismidtown.com,sheratondammam.com,haciendauayamon.com,sheratonomaha.com,westinreston.com,sheratondenverwest.com,sheratonabuja.com,fourpointsvictoriagateway.com,lemeridiennice.fr,fourpointslouisvilleairport.com,westinpalacemilan.it,fourpointssanantoniodowntown.com,sheratonbrusselsairport.be,elementarundelmills.com,sheratonstpaulwoodbury.com,sheratonsanjose.com,lemeridienbalijimbaran.com,sheratonprovidenceairport.com,fourpointstallahasseedowntown.com,lemeridien-jeddah.com,aloftbwi.com,sheratonlaguardiaeast.com,mobile.royal-hawaiian.com,sheratonsaigon.com,alofttempe.com,sheratonwashingtonnorth.com,sheratonjeddah.com,fourpointscharlottepineville.com,sheratonalbuquerqueairport.com,fourpointsedmontonsouth.com,sheratonbruxelles.fr,westinhyderabadmindspace.com,fourpointswestlafayette.com,sheratonminneapoliswest.com,fourpointsrichmondairport.com,fourpointspleasanton.com,sheratonaugusta.com,fourpointsbangorairport.com,lemeridienbudapest.hu,sheratondubaicreek.com,sheratonsuitessandiego.com,fourpointscambridgekitchener.com,fourpointsbrussels.com,sheratonmidwestcity.com,sheratontiranahotel.com,sheratonbuckscounty.com,aloftbruxelles.com,haciendasanjosecholul.com,sheratondenvertech.com,sheratonkampala.com,fourpointsmiamibeach.com,sheratonmontrealairport.com,sheratoneriebayfront.com,stregissuites.com,residences.starwoodpromos.com,sheratonstonebriar.com,sheratonbrussels.be,sheratonnorthbrook.com,alofttulsadowntown.com,fourpointscaguas.com,sheratontunis.com,sheratonbahrain.com,aloftsanfranciscoairport.com,sheratonmontazah.com,fourpointswinnipegairport.com,sheratonbruxellesaeroport.fr,sheratonpleasanton.com,elementdfwnorth.com,aloftdullesairportnorth.com,pitrizzahotel.com,lemeridienheliopolis.com,lemeridienoran.com,thegrandmauritian.com,fourpointsmanchesterairport.com,sheratonherndondulles.com,sheratonphiladelphiaairport.com,mobile.stregisprinceville.com,fourpointsmississaugameadowvale.com,mobile.sheraton-waikiki.com,fourpointskalamazoo.com,fourpointsvancouverairport.com,deals.whotels.com,stregisgrandtour.com,aloftjacksonvilleairport.com,fourpointshalifax.com,haciendatemozon.com,aloftcupertino.com,westinplayabonita.com,fourpointswinnipegsouth.com,131.253.14.98,wretreatkohsamui.com,elementewing.com,hotelexcelsiornapoli.com,fourpointsedmontongateway.com,fourpointstorontomississauga.com,fourpointschattanooga.com,wildwoodsnowmass.com,lemeridienmakkah.com,fourpointssandakan.com,aloftclevelanddowntown.com,hotelcervocostasmeralda.com,ch.tbe.taleo.net,sheratonpraha.cz,buesingpalais.de,starwood-live.feedmagnet.com,aloftbrussels.be,w-barcelona.cat,aloftsiliconvalley.com,fourpointstucsonairport.com,romazzinohotel.com,aloftorlandodowntown.com,fourpointsriyadhkhaldia.com,lemeridienbrussels.be,fourpointshobbyairport.com,fourpointslongislandcity.com,global.starwoodoffers.com,aloftbolingbrook.com,draft.livecms.biz,aloftminneapolis.com,haciendasantarosa.com,lemeridienra.cat,lemeridien-douala.com,sheratonbijao.com,131.253.14.66,sheratondreamlandhotel.com,wtaipei.jp,fourpointssanantonioairport.com,fourpointsbakersfield.com,fourpointspittsburghairport.com,fourpointspenang.com,sheratondjibouti.com,fourpointssiliconvalley.com,mobile.sheraton-maui.com,sheratonluxor.com,fourpointsbostonloganairport.com,westinhamburg.com,lemeridienrendama.com,westinmelbourne.com,aloftgreenbay.com,webcache.googleusercontent.com,fourpointsbrussels.be,aloftmiamibrickell.com,aloftleawoodoverlandpark.com,hotelkamp.ru,fourpointskuwait.com,fourpointsminneapolisairport.com,fourpointsmontevideo.com,sheratonmetechipalace.com,starwoodpromos.dev.livecms.biz,china-spg.com,aloftrogersbentonville.com,meeting-in-frankfurt.com,sheratonhartfordsouth.com,wtaipei.co.kr,londonluxuryhoteldeals.com,fourpointsperth.com,canada.starwoodoffers.com,westinsacramento.com,sheratondetroitnovi.com,fourpoints-macae.com,aloftwinchester.com,sheratonlibertador.com,mobile.sheraton-kauai.com,westinpalacemadrid.ru,fourpointsedmontonairport.com,sheratonbakuairport.com,wsantiagohotel.com,spgpromos.com";
s.linkInternalFilters = s.linkInternalFilters + "," + getLinkInternalDomain(location.href);
s.linkLeaveQueryString=true;
s.linkTrackVars="None";
s.linkTrackEvents="None";

/* Form Analysis Config */
// s.formList="reservationForm"; // THESE WILL NEED TO BE UPDATED
s.trackFormList=true;
s.trackPageName=true;
s.useCommerce=true;
s.varUsed="eVar28";
s.eventList="event21,event22,event23"; //Abandon,Success,Error

/* Plugin: Media Tracking
Media tracking included to monitor video plays on SPG.tv and any other site
using video -> given by Omniture
*/
s.loadModule("Media")
s.Media.autoTrack=false;
s.Media.trackWhilePlaying=true;
/* TrackVars and TrackEvents are needed to properly track additional video data points.
*/
s.Media.trackVars="events,prop64,eVar61,eVar74,contextData.bc_tags,contextData.bc_refid,contextData.bc_player,contextData.bc_playertype,contextData.bc_playlist";
s.Media.trackEvents="event64,event65,event66,event67,event68,event82";
s.Media.trackMilestones="25,50,75,90";
s.Media.segmentByMilestones = true;
s.Media.trackUsingContextData = true;
s.Media.contextDataMapping = {
"http://www.starwoodhotels.com/common/js/omniture/a.media.name":"prop64,eVar61",
"a.media.segment":"",
"a.contentType":"",
"a.media.timePlayed":"",
"http://www.starwoodhotels.com/common/js/omniture/a.media.view":"event64",
"a.media.segmentView":"",
"a.media.complete":"event82",
"a.media.milestones":{
25:"event65",
50:"event66",
75:"event67",
90:"event68"
}
};

/* Function to parse the URL and return the domain for linkInternalFilter string*/
function getLinkInternalDomain (urlString) {
    var urlPattern = new RegExp("(http|https)://(.*?)/.*$");
    var parsedUrl = urlString.match(urlPattern);
    return parsedUrl[2];
}

function getBrandCode() {
    if (location.href.indexOf("/preferredguest/") > -1) return "SPG";
    else if(location.href.indexOf("/westin/") > -1) return "WI";
    else if(location.href.indexOf("/whotels/") > -1) return "WH";
    else if(location.href.indexOf("/lemeridien/") > -1) return "MD";
    else if(location.href.indexOf("/luxury/") > -1) return "LC";
    else if(location.href.indexOf("/sheraton/") > -1) return "SI";
    else if(location.href.indexOf("/stregis/") > -1) return "ST";
    else if(location.href.indexOf("/fourpoints/") > -1) return "4P";
    else if(location.href.indexOf("/element/") > -1) return "EL";
    else if(location.href.indexOf("/alofthotels/") > -1) return "AL";
    else if(location.href.indexOf("/corporate/") > -1) return "CORP";
    else if(location.href.indexOf("/pro/") > -1) return "PRO";
    else if(location.href.indexOf("/resorts/") > -1) return "RST";
    else if(location.href.indexOf("/bestrate/") > -1) return "BRG";
    else return "CORP";
}

function getDateByFormat(fieldString, dateFormatString){
	var date = new Date(); 

	var dateFormatString = dateFormatString.toLowerCase();
	var hasSlashOrDash = false;
	var fieldString;
	
	if (fieldString.search("/") > 0) {
			var dateValues = fieldString.split("/");
	        hasSlashOrDash = true;
	    } else if (fieldString.search("-") > 0) { 
			var dateValues = fieldString.split("-");
	        hasSlashOrDash = true;
	    } else if (fieldString.indexOf(".") >= 0) {
			var dateValues = fieldString.split(".");
		}else if (fieldString != "") { 
			var dateValues = new Array(fieldString);
		} else if (fieldString == "") {
			date.setTime(Date.parse(dateFormatString));
	    return date;
		}


	switch(dateFormatString) {
			case 'mm/dd/yyyy':
				if (dateValues[0]) month = dateValues[0];
				if (dateValues[1]) day = dateValues[1];
				if (dateValues[2]) year = dateValues[2];
				break;
			case 'dd/mm/aaaa':
	        case 'jj/mm/aaaa':
	        case 'gg/mm/aaaa':
	        case 'http://www.starwoodhotels.com/common/js/omniture/tt.mm.jjjj':
	        case 'dd/mm/jjjj':
	        case 'http://www.starwoodhotels.com/common/js/omniture/dd.mm.rrrr':
	        case 'gg/aa/yyyy':
				if (dateValues[0]) day = dateValues[0];
				if (dateValues[1]) month = dateValues[1];
				if (dateValues[2]) year = dateValues[2];
				break;
			case 'yyyy/mm/dd':
				if (dateValues[0]) year = dateValues[0];
				if (dateValues[1]) month = dateValues[1];
				if (dateValues[2]) day = dateValues[2];
				break;
			case 'yyyy/dd/mm':
				if (dateValues[0]) year = dateValues[0];
				if (dateValues[1]) day = dateValues[1];
				if (dateValues[2]) month = dateValues[2];
				break;
	        case 'http://www.starwoodhotels.com/common/js/omniture/dd.mm.yyyy':
	        case 'dd/mm/yyyy':
	            if (dateValues[0]) day = dateValues[0];
				if (dateValues[1]) month = dateValues[1];
				if (dateValues[2]) year = dateValues[2];
				break;
			case '\u0434\u0434.\u043c\u043c.\u0433\u0433\u0433\u0433':
	            if (dateValues[0]) day = dateValues[0];
				if (dateValues[1]) month = dateValues[1];
				if (dateValues[2]) year = dateValues[2];
				break;
	        case "yy\u5E74mm\u6708dd\u65E5":          
	            if(!hasSlashOrDash) {
	                year = fieldString.substr(0,2);
	                month = fieldString.substr(3,2);
	                day = fieldString.substr(6,2);
	            } else {
	                if (dateValues[0]) year = dateValues[0];
				    if (dateValues[1]) month = dateValues[1];
				    if (dateValues[2]) day = dateValues[2];
	            }
	            break;
	        case "yy\uB144mm\uC6D4dd\uC77C":          
	            if(!hasSlashOrDash) {
	                year = fieldString.substr(0,2);
	                month = fieldString.substr(3,2);
	                day = fieldString.substr(6,2);
	            } else {
	                if (dateValues[0]) year = dateValues[0];
				    if (dateValues[1]) month = dateValues[1];
				    if (dateValues[2]) day = dateValues[2];
	            }
	            break;
			default:
				if (dateValues[0]) month = dateValues[0];
				if (dateValues[1]) day = dateValues[1];
				if (dateValues[2]) year = dateValues[2];
				break;
		}
	date = year+"-"+month+"-"+day;
	return date;
}

/* Page Load Time Calculation*/
function s_getLoadTime(){
	if(!window.s_loadT){
		var current = new Date().getTime(),
			perf = window.performance?performance.timing:0,
			total = perf?perf.requestStart:window.inHeadTS||0;
		
		s_loadT = total?(((current-total)/1000).toFixed(2)):'';
	}	
	if(0<s_loadT<20){
		return s_loadT;
	}else if(s_loadT > 20){
		return "20+";
	}
}

/* Plugin Config */
s.usePlugins=true;
function s_doPlugins(s) {
    var confirmationNumber;

    /* Pseudo logic - Pass unique custom events on each booking page for fallout analysis */
    if (location.href.indexOf("http://www.starwoodhotels.com/reservations/booking/index.html") > -1) {
        s.events = s.apl(s.events, 'event16', ',', 1);
    }
    else if (location.href.indexOf("http://www.starwoodhotels.com/reservations/booking/verify.html") > -1 || location.href.indexOf("http://www.starwoodhotels.com/reservations/modify/verify.html") > -1 || location.href.indexOf("http://www.starwoodhotels.com/preferredguest/account/enroll/review.html") > -1) {
        s.events = s.apl(s.events, 'event17', ',', 1);
        var temp_fv = s.getValOnce(1,"s_fsubmit",0);
        if (temp_fv == 1) {
            s.events = s.apl(s.events, 'event22', ',', 1);
        }

        if (typeof s.prop48 != 'undefined' && s.prop48=='Unprofiled Guest') {
            s.eVar60 = s.eVar69 = "";
        }
    }
    else if (location.href.indexOf("http://www.starwoodhotels.com/reservations/booking/confirm.html") > -1) {
        s.events = s.apl(s.events, 'event18', ',', 1);

        // if YSYW personalize box is present on confirmation page, then fire event indicating this
        if (document.getElementById("personalize")) {
        	confirmationNumber = s.purchaseID.split('_')[1];
        	var temp_fv = s.getValOnce(s.purchaseID,"s_ysyw",0);
        	if (temp_fv == s.purchaseID) {
        		s.events = s.apl(s.events, 'event53', ',', 1);
        	}
            s.eVar51 = confirmationNumber + "|formEligible";
        }
    }
    else {
        // Nothing for now
    }

    /* Page View Event
    Page View event to flag page views and correlate with conversion variables -> s.apl provided by OMTR
    */
    s.events = s.apl(s.events, 'event9', ',', 1);

    /* Plugin: Get Visit Start */
    var temp_prop=s.getVisitStart("s_visit");
    if (temp_prop == "1") {
	    s.events = s.apl(s.events, 'event19', ',', 1);
    }

    /* Plugin: Get Time To Complete v.0.4 */
    if(s.events.indexOf('event19')>-1)
        s.prop38='start';

    if(s.events.indexOf('purchase')>-1)
	s.prop38='stop';

    s.prop38=s.getTimeToComplete(s.prop38,'ttc',0);

    /* Plugin: Get Days Since Last Visit v 1.0
    This plugin allows us to see when the last visit happened across all of our eVar and sProps captured -> provided by OMTR
    */
    s.eVar44=s.prop44=s.getDaysSinceLastVisit();

    /* Plugin: Form Analysis v.2.1 */
    // s.setupFormAnalysis();

    /* Plugin: Time Parting v.1.4
    This plugin allows us to see, by hour, day of week, weekday/wknd, what variables are getting the most traffic -> provided by OMTR
    */
    var curdate = new Date();
    var eVar13Day = (curdate.getDate() < 10) ? '0' + curdate.getDate() : curdate.getDate();
    var eVar13Month = ((curdate.getMonth() + 1) < 10) ? '0' + (curdate.getMonth() + 1) : (curdate.getMonth() + 1);
    var eVar13Year = (curdate.getYear() < 1000) ? curdate.getYear() + 1900 : curdate.getYear();
    var eVar13Temp = eVar13Month + ":" + eVar13Day + ":" + eVar13Year;

    var temphr = s.getTimeParting('h','-5',curdate.getFullYear());  // set hour of day
    var tempday = s.getTimeParting('d','-5',curdate.getFullYear());  // set day of week
    var tempweek = s.getTimeParting('w','-5',curdate.getFullYear()); // Set weekend vs. weekday
    if(temphr) {
        s.eVar42=s.prop49=tempweek+":"+tempday+":"+temphr;
    }

    /* Plugin: New vs Repeat User
    This plugin allows us to see whether a user is new or a repeat (without being logged in) -> provided by OMTR
    */

    // s.prop47=s.getNewRepeat();
    var brandCode = getBrandCode();
    s.prop47 = s.eVar72 = s.getNewRepeat();
    s.prop52 = s.eVar73 = brandCode + "_" + s.getNewRepeat(30,brandCode + "_nr");


    /* Plugin: Get Visit Number v.2.0
    This plugin allows us to see how often a user sees a particular page(s) in each visit as well as correlate/subrelate by other key data points.
    */
    s.prop36=s.eVar37=s.getVisitNum();
    /* Search Tracking
    This adds tracking to monitor sort Order (which is most popular, which leads to highest conversion, etc)
    as well as takes the dates searched and captures them ->
    */
    var temp_sortOrder = s.getQueryParam("sortOrder");

    if (temp_sortOrder) {
        sortName= "Sort-";
        if(temp_sortOrder == 0){
        	sortName=sortName+"Avail";
        }else if(temp_sortOrder == 7){
        	sortName=sortName+"HiLow";
        }else if(temp_sortOrder == 3){
        	sortName=sortName+"LowHi";
        }else if(temp_sortOrder == 2){
        	sortName=sortName+"Name";
        }else if(temp_sortOrder == 1){
        	sortName=sortName+"Distance";
        }else{
            sortName=temp_sortOrder;
        }
        if(s.pageName.indexOf(sortName) == -1){
        	s.pageName += ":"+sortName
        }
        // Lets find another variable to capture this as this overrides the MVT layout number
        s.eVar49 = temp_sortOrder;
    }

    var temp_arrivalDate = s.getQueryParam("arrivalDate");
    var temp_departureDate = s.getQueryParam("departureDate");
    
    if(temp_arrivalDate !== "" && location.href.indexOf("/rates/") == -1){
    	temp_arrivalDate = getDateByFormat(temp_arrivalDate, dateFormatString);
    }
    
    if(temp_departureDate !== "" && location.href.indexOf("/rates/") == -1){
    	temp_departureDate = getDateByFormat(temp_departureDate, dateFormatString);
    }
    
    if (temp_arrivalDate) {
        s.prop50 = s.eVar50=temp_arrivalDate+":"+temp_departureDate; //dates searched
    }
	
	if(!s.campaign){
		var em_c = s.getQueryParam("EM");
		var ps_c = s.getQueryParam("PS");
		s.campaign = (em_c.length > 0 && ps_c.length > 0) ? em_c + ":" + ps_c : em_c + ps_c;
	};

	if(!s.eVar30){
		s.eVar30 = s.getQueryParam("IM");
	}

	if(!s.eVar31){
		var es_c = s.getQueryParam("ES");
		var ep_c = s.getQueryParam("EP");
		s.eVar31 = (es_c.length > 0 && ep_c.length > 0) ? es_c + ":" + ep_c : es_c + ep_c;
	}

	s.eVar74 = s.pageName;

    /* Multi Marketing Touchpoint Tracking v.1.5
    This adds the ability to track how many marketing touchpoints were touched prior to a user booking -> provided by OMTR
    We added this because the below EM, ES...are plug-in related and there is more than one value - EM, ES, IM etc...
    Also added in sProp variables for pathing by campaign and most popular pages by campaign
    */
    var campaign_trak;
    var temp_s_campaign;
    temp_s_campaign = s.getQueryParam('EM,IM,ES,PS,EP',':');

    if(temp_s_campaign) {
        if (s.getQueryParam("EM"))  { campaign_trak = "EM:"+eVar13Temp; }
        if (s.getQueryParam("PS"))  { campaign_trak = "PS:"+eVar13Temp; }
	    if (s.getQueryParam("IM"))  { campaign_trak = "IM:"+eVar13Temp; }
	    if (s.getQueryParam("ES"))  { campaign_trak = "ES:"+eVar13Temp; }
	    if (s.getQueryParam("EP"))  { campaign_trak = "EP:"+eVar13Temp; }
	    s.eVar45 = s.crossVisitParticipation(campaign_trak,'s_ev45','30','5','>','purchase'); //30 day cookie
	}

    /*if (typeof s.prop48 != 'undefined' && s.prop48.length > 0) {
        s.prop48 = s.eVar43 = s.getAndPersistValue(s.prop48,'spg_number');
    }*/

    /*
     * If currency code change from the default, the the event need to set as event34
     */
    var temp_currencyCode = s.getQueryParam("currencyCode");
    if(temp_currencyCode){
    	s.events = "event34";
    }
    s.prop62 = s_getLoadTime();
	/*
	 7/7/2011 Keystone Changes: URL --> Prop63, prop13 --> eVar63
	*/
	s.prop63=(window.location.hostname + (window.location.pathname||"")).toLowerCase();
	s.eVar63=s.prop13;
	var tempPageName = s.pageName;

    s.prop23 = s.getPercentPageViewed();
    /* 7/14/2011 Keystone Recommendation Code Processing */
    _ksModule.recommendsProcessing(s);

    s.tnt=s.trackTNT();
    s.plugins="";
//
//    if (!(location.href.indexOf("http://www.starwoodhotels.com/reservations/booking/confirm.html") > -1 ||
//          location.href.indexOf("http://www.starwoodhotels.com/booking/cash_points/confirmation.html") > -1 ||
//          location.href.indexOf("http://www.starwoodhotels.com/booking/points/confirmation.html") > -1)) {
//            s.dynComp();
//    }
    	s.pageName = tempPageName;
    	if(s.pageType =='errorPage'){
    		s.prop20 = s.getPreviousValue(s.pageName_temp,'gpv_pn','');
    	}else{
    		s.prop20 = s.getPreviousValue(s.pageName,'gpv_pn','');
    	}
    }

s.doPlugins=s_doPlugins

var temp_ua = navigator.userAgent;
var temp_dt = "";
if (temp_ua.indexOf("iPod")>-1){temp_dt = "iPod";}
else if (temp_ua.indexOf("iPad")>-1){temp_dt = "iPad" + "|" + screen.width + "x" + screen.height;}
else if(temp_ua.indexOf("iPhone")>-1){temp_dt = "iPhone" + "|" + screen.width + "x" + screen.height;}
else if (temp_ua.indexOf("BlackBerry")>-1 || temp_ua.indexOf("BB10")>-1){temp_dt = "Blackberry Mobile";}
else if (temp_ua.indexOf("PlayBook")>-1){temp_dt = "Blackberry Tablet";}
else if (temp_ua.indexOf("Tablet")>-1 || temp_ua.indexOf("Galaxy Nexus")>-1){temp_dt = "Android Tablet";}
else if (temp_ua.indexOf("Mobile")>-1 || temp_ua.indexOf("Nexus")>-1){temp_dt = "Android Mobile";}
else {temp_dt = "Desktop";};
s.prop75 = temp_dt;

if (document.referrer != '' && document.referrer != 'undefined' && !(document.referrer.indexOf("http://www.starwoodhotels.com/common/js/omniture/starwoodhotels.com") > -1)) {
    var refStringArray = document.referrer.split('/');
    if (refStringArray != 'undefined' && refStringArray.length > 0) {
        s.prop24 = s.eVar29 = refStringArray[2];
    }
}


/* 09/07/2011 Keystone Solutions Plugins */
var _ksModule=function() {
	/* Global Data */
	var d=document,w=window,wh=w.location.href;
	/* var navDiv = d.getElementById("navigation"),
		aTags=(!navDiv?[]:navDiv.getElementsByTagName("A")); */
	// w.console=console||{warn:function(a){},error:function(a){}}
	/* Initialize on Virtual Tour pages */
	/* Globals Routines */
	var _domReady=function() { // DOMReady Wrapper Module
		var d=document,w=window,isjQ=typeof(jQuery)!="undefined",isMoo=typeof(w.MooTools)=="object",isYui=typeof(YAHOO)!="undefined",isReady=false;
		function setIsReady() { isReady=true;}
		/* function doLoad(f) {
			if(typeof(f)!="function") {return;}
			if(isMoo) { w.addEvent('domready',f); } // Mootools
			else if(isjQ) { jQuery(w).load(f); } // jQuery
		}; */
		function doWhenReady(f){
			if(typeof(f)!="function") {return;}
			if(isReady) { return f();}
			if(isYui){
				if(YAHOO.util.Event.DOMReady) { setIsReady(); return f(); } else { YAHOO.util.Event.onDOMReady(f); } // YUI 2.x
			}
			else
				if(isjQ) { jQuery(d).ready(f); } // jQuery
			else
				if(isMoo) { w.addEvent('domready',f); } // Mootools
			else {
					if(d.addEventListener) {
						d.addEventListener("DOMContentLoaded",setIsReady,false);
						d.addEventListener("DOMContentLoaded",f,false);
					} // Check for addEventListener
					else {
						if(typeof(d.readyState)!="undefined") {
							if(/in/.test(d.readyState)) {
								setTimeout('_domReady.onReady('+f+')',20);
							} else {
								setIsReady(); return f();
							}
						}
					}
			}
		};
		function prvInject(x) {
			function inject(path) {
				if(d.getElementsByTagName) {
					var h=d.getElementsByTagName("head")[0],s=d.createElement("script");
					s.type='text/javascript';
					s.async=true;
					s.src=path;
					h.appendChild(s);
				}
			}
			_domReady.onReady(function(){inject(x);});
		}
		return {
			// onLoad:doLoad,
			onReady:doWhenReady,
			inject:prvInject
		}
	}();
	var jQueryScriptLoading = false;
	var jQueryScriptReady = false;
	var waitjQuery=0;
	var fTarget=null;
	function initJQuery(callback) {
	    if(!fTarget) {fTarget=callback;}
	    if (typeof(jQuery) == 'undefined') {
	        if (! jQueryScriptLoading) {
	            //only output the script once..
	            jQueryScriptLoading = true;
	            //output the script (load it from google api)
	            _domReady.inject("../../../../ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"/*tpa=http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js*/);
	        }
	        waitjQuery++;
	        setTimeout(initJQuery, 500);
	    }  else {
	    	jQueryScriptReady=true;
	    	if(fTarget!==null) {fTarget();}
	    }
	}
	function virtualTourLinkLogic() {
		var href=$(this).attr('href')||wh,oc=String($(this).attr('onclick'))||wh;
		href=(href.indexOf("http")==-1&&oc.indexOf("http")!=-1?/\'(http[^\']+)\'/.exec(oc)[1]:href);
		var	did=s.getQueryParam("did","",href),
			bid=s.getQueryParam("brochureid","",href),
			mid=s.getQueryParam("mediaid","",href),
			txt=$(this).text().replace(/\W+/g,"").toLowerCase(),
			r=[];
		if(did.length>0) {r.push("did:"+did);}
		if(bid.length>0) {r.push("brochureid:"+bid);}
		if(mid.length>0) {r.push("mediaid:"+mid);}
		s.linkTrackVars="prop64,eVar64,events",s.linkTrackEvents="event51";
		s.events="event51";s.prop64='Iceportal|'+[s.prop21,s.prop1].join('|')+'|'+r.join("|"); s.eVar64='D=c64';
		s.tl(this,"o",s.pageName+":videotour:"+txt);
	}
	function processVirtualTour() {
		var link=$("a[class*='virtualTourLink']");/* ,clkf=link.click;
		link.click(function(){virtualTourLinkLogic();clkf();}); */
		link.click(virtualTourLinkLogic);
	}
	function processVirtualTourLinks() {
  		$("a[href*='iceportal']").click(virtualTourLinkLogic);
	}
	if(window.location.href.indexOf("http://www.starwoodhotels.com/common/js/omniture/virtual_tour.html")>-1) {//Activate Virtual Tour Code
		s.linkInternalFilters+="http://www.starwoodhotels.com/common/js/omniture/,iceportal.com";
		initJQuery(processVirtualTourLinks);
	}
	if(/property\/photos\/index.html\?propertyID\=/.test(wh)) {
		initJQuery(processVirtualTour);
	}
	function pubRecommendsProcessing(omni) {
		// Dependencies: None
		var results=(/starwoodhotels.com\/[^\/]+\/property\/([^\?]+)\/index.html\?.*propertyID=(\d+)/).exec(wh),
			approve="overview:photos:rooms:area:features:dining:meetings";
		if(results!==null) {
			if(approve.indexOf(results[1])!=-1) { omni.products=(!omni.products?"":",")+";"+results[2]; }
		}
	};
    // Global update to s object
    window.trackMetrics=function(action,data) {
        var l=location, prefix=s.pageName+":",me;
        if(!window.s) {return; }
        var actionRoutines= {
            "video": function() {
                s.linkTrackVars="prop64,eVar64,events"; s.linkTrackEvents="event51";
                prefix+="video:";
                s.prop64=prefix+(data.id||"No Video Title");
                s.eVar64="D=c64";
                s.events="event51";
                me.link();
            },
            "promo":function() {
                /* s.linkTrackVars="eVar30";s.linkTrackEvents="none";
                s.eVar30=data.id||"no Promo ID";
                me.link(); */
            },
            "link" : function() {
                s.tl(this,"o",prefix+(data.id||"LinkAction:" + l.hostname+l.pathname));
            }
        };
        me=actionRoutines;
        if(actionRoutines.common) {
            actionRoutines.common(s);
        }
        if(actionRoutines[action]) {
            if(typeof(actionRoutines[action])=="string") {
                action=actionRoutines[action];
            }
            actionRoutines[action](s);
        }
    };
	return {
		_domReady:_domReady,
		initJQuery:initJQuery,
		processVirtualTourLinks: processVirtualTourLinks,
		recommendsProcessing:pubRecommendsProcessing
	};
}();

/************************** PLUGINS SECTION *************************/
/* You may insert any plugins you wish to use here.     */

/************************ Test&Target Plugin Start *************************/
/*
* Plugin: getValOnce_v1.1
*/
s.getValOnce=new Function("v","c","e","t",""
+"var s=this,a=new Date,v=v?v:'',c=c?c:'s_gvo',e=e?e:0,i=t=='m'?6000"
+"0:86400000;k=s.c_r(c);if(v){a.setTime(a.getTime()+e*i);s.c_w(c,v,e"
+"==0?0:a);}return v==k?'':v");

/*
* TNT Integration Plugin v1.0
* v - Name of the javascript variable that is used. Defaults to s_tnt
(optional)
* p - Name of the url parameter. Defaults to s_tnt (optional)
* b - Blank Global variable after plugin runs. Defaults to true (Optional)
*/
s.trackTNT=new Function("v","p","b",""
+"var s=this,n='s_tnt',p=p?p:n,v=v?v:n,r='',pm=false,b=b?b:true;if(s."
+"getQueryParam){pm=s.getQueryParam(p);}if(pm){r+=(pm+',');}if(s.wd[v"
+"]!=undefined){r+=s.wd[v];}if(b){s.wd[v]='';}return r;");

/*
 * Plugin: Dynamic Variable Compression v1.0
 * Shortens the image request by setting dynamic
 * variables for repeated values.
*/
s.dcVars=new Function("c",""
+"vl='pageName,purchaseID,channel,server,pageType,campaign,state,zip,"
+"events,products,transactionID';d=s.split(vl,',');for(a=1;a<=75;a++)"
+"{f=c=='prop'?'c':'v';pa=c+a;if(typeof s[pa]!='undefined'&&s[pa]!='')"
+"{for(b=a;b<=75;b++){if(a==b){b++}pb=c+b;if(typeof s[pb]!='undefined"
+"'&&s[pb]!=''){if(s[pa]==s[pb]&&s[pb].indexOf('D=')==-1){s[pb]='D='+"
+"f+a;}}}for(e=0;e<d.length;e++){pnc=d[e];if(typeof s[pnc]!='undefine"
+"d'&&s[pnc]!='')if(s[pa]==s[d[e]]&&s[pnc].indexOf('D=')==-1){s[pnc]="
+"'D='+f+a;}}}}");
s.dynComp=new Function("",""
+"s.dcVars('prop');s.dcVars('eVar');");

/*
* Plugin: getQueryParam 2.4
*/
s.getQueryParam=new Function("p","d","u","h",""
+"var s=this,v='',i,j,t;d=d?d:'';u=u?u:(s.pageURL?s.pageURL:s.wd.loca"
+"tion);if(u=='f')u=s.gtfs().location;while(p){i=p.indexOf(',');i=i<0"
+"?p.length:i;t=s.p_gpv(p.substring(0,i),u+'',h);if(t){t=t.indexOf('#"
+"')>-1?t.substring(0,t.indexOf('#')):t;}if(t)v+=v?d+t:t;p=p.substrin"
+"g(i==p.length?i:i+1)}return v");
s.p_gpv=new Function("k","u","h",""
+"var s=this,v='',q;j=h==1?'#':'?';i=u.indexOf(j);if(k&&i>-1){q=u.sub"
+"string(i+1);v=s.pt(q,'&','p_gvf',k)}return v");
s.p_gvf=new Function("t","k",""
+"if(t){var s=this,i=t.indexOf('='),p=i<0?t:t.substring(0,i),v=i<0?'T"
+"rue':t.substring(i+1);if(p.toLowerCase()==k.toLowerCase())return s."
+"epa(v)}return''");

/*
 * Utility Functions: p_gh, split, apl, replace, read & write combined cookies
 */
s.p_gh=new Function(""
+"var s=this;if(!s.eo&&!s.lnk)return '';var o=s.eo?s.eo:s.lnk,y=s.ot("
+"o),n=s.oid(o),x=o.s_oidt;if(s.eo&&o==s.eo){while(o&&!n&&y!='BODY'){"
+"o=o.parentElement?o.parentElement:o.parentNode;if(!o)return '';y=s."
+"ot(o);n=s.oid(o);x=o.s_oidt}}return o.href?o.href:'';");
s.split=new Function("l","d",""
+"var i,x=0,a=new Array;while(l){i=l.indexOf(d);i=i>-1?i:l.length;a[x"
+"++]=l.substring(0,i);l=l.substring(i+d.length);}return a");

s.apl=new Function("L","v","d","u",""
+"var s=this,m=0;if(!L)L='';if(u){var i,n,a=s.split(L,d);for(i=0;i<a."
+"length;i++){n=a[i];m=m||(u==1?(n==v):(n.toLowerCase()==v.toLowerCas"
+"e()));}}if(!m)L=L?L+d+v:v;return L");
s.repl=new Function("x","o","n",""
+"var i=x.indexOf(o),l=n.length;while(x&&i>=0){x=x.substring(0,i)+n+x."
+"substring(i+o.length);i=x.indexOf(o,i+l)}return x");
s.c_rr=s.c_r;
s.c_r=new Function("k",""
+"var s=this,d=new Date,v=s.c_rr(k),c=s.c_rr('s_pers'),i,m,e;if(v)ret"
+"urn v;k=s.ape(k);i=c.indexOf(' '+k+'=');c=i<0?s.c_rr('s_sess'):c;i="
+"c.indexOf(' '+k+'=');m=i<0?i:c.indexOf('|',i);e=i<0?i:c.indexOf(';'"
+",i);m=m>0?m:e;v=i<0?'':s.epa(c.substring(i+2+k.length,m<0?c.length:"
+"m));if(m>0&&m!=e)if(parseInt(c.substring(m+1,e<0?c.length:e))<d.get"
+"Time()){d.setTime(d.getTime()-60000);s.c_w(s.epa(k),'',d);v='';}ret"
+"urn v;");
s.c_wr=s.c_w;
s.c_w=new Function("k","v","e",""
+"var s=this,d=new Date,ht=0,pn='s_pers',sn='s_sess',pc=0,sc=0,pv,sv,"
+"c,i,t;d.setTime(d.getTime()-60000);if(s.c_rr(k)) s.c_wr(k,'',d);k=s"
+".ape(k);pv=s.c_rr(pn);i=pv.indexOf(' '+k+'=');if(i>-1){pv=pv.substr"
+"ing(0,i)+pv.substring(pv.indexOf(';',i)+1);pc=1;}sv=s.c_rr(sn);i=sv"
+".indexOf(' '+k+'=');if(i>-1){sv=sv.substring(0,i)+sv.substring(sv.i"
+"ndexOf(';',i)+1);sc=1;}d=new Date;if(e){if(e.getTime()>d.getTime())"
+"{pv+=' '+k+'='+s.ape(v)+'|'+e.getTime()+';';pc=1;}}else{sv+=' '+k+'"
+"='+s.ape(v)+';';sc=1;}if(sc) s.c_wr(sn,sv,0);if(pc){t=pv;while(t&&t"
+".indexOf(';')!=-1){var t1=parseInt(t.substring(t.indexOf('|')+1,t.i"
+"ndexOf(';')));t=t.substring(t.indexOf(';')+1);ht=ht<t1?t1:ht;}d.set"
+"Time(ht);s.c_wr(pn,pv,d);}return v==s.c_r(s.epa(k));");

/*
 * Plugin: getQueryParam 2.4
 */
/*s.getQueryParam=new Function("p","d","u","h",""
+"var s=this,v='',i,j,t;d=d?d:'';u=u?u:(s.pageURL?s.pageURL:s.wd.loca"
+"tion);if(u=='f')u=s.gtfs().location;while(p){i=p.indexOf(',');i=i<0"
+"?p.length:i;t=s.p_gpv(p.substring(0,i),u+'',h);if(t){t=t.indexOf('#"
+"')>-1?t.substring(0,t.indexOf('#')):t;}if(t)v+=v?d+t:t;p=p.substrin"
+"g(i==p.length?i:i+1)}return v");
s.p_gpv=new Function("k","u","h",""
+"var s=this,v='',q;j=h==1?'#':'?';i=u.indexOf(j);if(k&&i>-1){q=u.sub"
+"string(i+1);v=s.pt(q,'&','p_gvf',k)}return v");
s.p_gvf=new Function("t","k",""
+"if(t){var s=this,i=t.indexOf('='),p=i<0?t:t.substring(0,i),v=i<0?'T"
+"rue':t.substring(i+1);if(p.toLowerCase()==k.toLowerCase())return s."
+"epa(v)}return''"); */
/*
 *	Plug-in: crossVisitParticipation v1.7 - stacks values from
 *	specified variable in cookie and returns value
 */

s.crossVisitParticipation=new Function("v","cn","ex","ct","dl","ev","dv",""
+"var s=this,ce;if(typeof(dv)==='undefined')dv=0;if(s.events&&ev){var"
+" ay=s.split(ev,',');var ea=s.split(s.events,',');for(var u=0;u<ay.l"
+"ength;u++){for(var x=0;x<ea.length;x++){if(ay[u]==ea[x]){ce=1;}}}}i"
+"f(!v||v==''){if(ce){s.c_w(cn,'');return'';}else return'';}v=escape("
+"v);var arry=new Array(),a=new Array(),c=s.c_r(cn),g=0,h=new Array()"
+";if(c&&c!=''){arry=s.split(c,'],[');for(q=0;q<arry.length;q++){z=ar"
+"ry[q];z=s.repl(z,'[','');z=s.repl(z,']','');z=s.repl(z,\"'\",'');arry"
+"[q]=s.split(z,',')}}var e=new Date();e.setFullYear(e.getFullYear()+"
+"5);if(dv==0&&arry.length>0&&arry[arry.length-1][0]==v)arry[arry.len"
+"gth-1]=[v,new Date().getTime()];else arry[arry.length]=[v,new Date("
+").getTime()];var start=arry.length-ct<0?0:arry.length-ct;var td=new"
+" Date();for(var x=start;x<arry.length;x++){var diff=Math.round((td."
+"getTime()-arry[x][1])/86400000);if(diff<ex){h[g]=unescape(arry[x][0"
+"]);a[g]=[arry[x][0],arry[x][1]];g++;}}var data=s.join(a,{delim:',',"
+"front:'[',back:']',wrap:\"'\"});s.c_w(cn,data,e);var r=s.join(h,{deli"
+"m:dl});if(ce)s.c_w(cn,'');return r;");

s.join = new Function("v","p",""
+"var s = this;var f,b,d,w;if(p){f=p.front?p.front:'';b=p.back?p.back"
+":'';d=p.delim?p.delim:'';w=p.wrap?p.wrap:'';}var str='';for(var x=0"
+";x<v.length;x++){if(typeof(v[x])=='object' )str+=s.join( v[x],p);el"
+"se str+=w+v[x]+w;if(x<v.length-1)str+=d;}return f+str+b;");
s.split=new Function("l","d",""
+"var i,x=0,a=new Array;while(l){i=l.indexOf(d);i=i>-1?i:l.length;a[x"
+"++]=l.substring(0,i);l=l.substring(i+d.length);}return a");

/*
 * Plugin: getTimeParting 2.0
 */
s.getTimeParting=new Function("t","z","y","l",""
+"var s=this,d,A,U,X,Z,W,B,C,D,Y;d=new Date();A=d.getFullYear();Y=U=S"
+"tring(A);if(s.dstStart&&s.dstEnd){B=s.dstStart;C=s.dstEnd}else{;U=U"
+".substring(2,4);X='090801|101407|111306|121104|131003|140902|150801"
+"|161306|171205|181104|191003';X=s.split(X,'|');for(W=0;W<=10;W++){Z"
+"=X[W].substring(0,2);if(U==Z){B=X[W].substring(2,4);C=X[W].substrin"
+"g(4,6)}}if(!B||!C){B='08';C='01'}B='03/'+B+'/'+A;C='11/'+C+'/'+A;}D"
+"=new Date('1/1/2000');if(D.getDay()!=6||D.getMonth()!=0){return'Dat"
+"a Not Available'}else{z=z?z:'0';z=parseFloat(z);B=new Date(B);C=new"
+" Date(C);W=new Date();if(W>B&&W<C&&l!='0'){z=z+1}W=W.getTime()+(W.g"
+"etTimezoneOffset()*60000);W=new Date(W+(3600000*z));X=['Sunday','Mo"
+"nday','Tuesday','Wednesday','Thursday','Friday','Saturday'];B=W.get"
+"Hours();C=W.getMinutes();D=W.getDay();Z=X[D];U='AM';A='Weekday';X='"
+"00';if(C>30){X='30'}if(B>=12){U='PM';B=B-12};if(B==0){B=12};if(D==6"
+"||D==0){A='Weekend'}W=B+':'+X+U;if(y&&y!=Y){return'Data Not Availab"
+"le'}else{if(t){if(t=='h'){return W}if(t=='d'){return Z}if(t=='w'){r"
+"eturn A}}else{return Z+', '+W}}}");

/*
 * Plugin: Form Analysis 2.1 (Success, Error, Abandonment)
 */
s.setupFormAnalysis=new Function(""
+"var s=this;if(!s.fa){s.fa=new Object;var f=s.fa;f.ol=s.wd.onload;s."
+"wd.onload=s.faol;f.uc=s.useCommerce;f.vu=s.varUsed;f.vl=f.uc?s.even"
+"tList:'';f.tfl=s.trackFormList;f.fl=s.formList;f.va=new Array('',''"
+",'','')}");
s.sendFormEvent=new Function("t","pn","fn","en",""
+"var s=this,f=s.fa;t=t=='s'?t:'e';f.va[0]=pn;f.va[1]=fn;f.va[3]=t=='"
+"s'?'Success':en;s.fasl(t);f.va[1]='';f.va[3]='';");
s.faol=new Function("e",""
+"var s=s_c_il["+s._in+"],f=s.fa,r=true,fo,fn,i,en,t,tf;if(!e)e=s.wd."
+"event;f.os=new Array;if(f.ol)r=f.ol(e);if(s.d.forms&&s.d.forms.leng"
+"th>0){for(i=s.d.forms.length-1;i>=0;i--){fo=s.d.forms[i];fn=fo.name"
+";tf=f.tfl&&s.pt(f.fl,',','ee',fn)||!f.tfl&&!s.pt(f.fl,',','ee',fn);"
+"if(tf){f.os[fn]=fo.onsubmit;fo.onsubmit=s.faos;f.va[1]=fn;f.va[3]='"
+"No Data Entered';for(en=0;en<fo.elements.length;en++){el=fo.element"
+"s[en];t=el.type;if(t&&t.toUpperCase){t=t.toUpperCase();var md=el.on"
+"mousedown,kd=el.onkeydown,omd=md?md.toString():'',okd=kd?kd.toStrin"
+"g():'';if(omd.indexOf('.fam(')<0&&okd.indexOf('.fam(')<0){el.s_famd"
+"=md;el.s_fakd=kd;el.onmousedown=s.fam;el.onkeydown=s.fam}}}}}f.ul=s"
+".wd.onunload;s.wd.onunload=s.fasl;}return r;");
s.faos=new Function("e",""
+"var s=s_c_il["+s._in+"],f=s.fa,su;if(!e)e=s.wd.event;if(f.vu){s[f.v"
+"u]='';f.va[1]='';f.va[3]='';}su=f.os[this.name];return su?su(e):tru"
+"e;");
s.fasl=new Function("e",""
+"var s=s_c_il["+s._in+"],f=s.fa,a=f.va,l=s.wd.location,ip=s.trackPag"
+"eName,p=s.pageName;if(a[1]!=''&&a[3]!=''){a[0]=!p&&ip?l.host+l.path"
+"name:a[0]?a[0]:p;if(!f.uc&&a[3]!='No Data Entered'){if(e=='e')a[2]="
+"'Error';else if(e=='s')a[2]='Success';else a[2]='Abandon'}else a[2]"
+"='';var tp=ip?a[0]+':':'',t3=e!='s'?':('+a[3]+')':'',ym=!f.uc&&a[3]"
+"!='No Data Entered'?tp+a[1]+':'+a[2]+t3:tp+a[1]+t3,ltv=s.linkTrackV"
+"ars,lte=s.linkTrackEvents,up=s.usePlugins;if(f.uc){s.linkTrackVars="
+"ltv=='None'?f.vu+',events':ltv+',events,'+f.vu;s.linkTrackEvents=lt"
+"e=='None'?f.vl:lte+','+f.vl;f.cnt=-1;if(e=='e')s.events=s.pt(f.vl,'"
+",','fage',2);else if(e=='s')s.events=s.pt(f.vl,',','fage',1);else s"
+".events=s.pt(f.vl,',','fage',0)}else{s.linkTrackVars=ltv=='None'?f."
+"vu:ltv+','+f.vu}s[f.vu]=ym;s.usePlugins=false;var faLink=new Object"
+"();faLink.href='#';s.tl(faLink,'o','Form Analysis');s[f.vu]='';s.us"
+"ePlugins=up}return f.ul&&e!='e'&&e!='s'?f.ul(e):true;");
s.fam=new Function("e",""
+"var s=s_c_il["+s._in+"],f=s.fa;if(!e) e=s.wd.event;var o=s.trackLas"
+"tChanged,et=e.type.toUpperCase(),t=this.type.toUpperCase(),fn=this."
+"form.name,en=this.name,sc=false;if(document.layers){kp=e.which;b=e."
+"which}else{kp=e.keyCode;b=e.button}et=et=='MOUSEDOWN'?1:et=='KEYDOW"
+"N'?2:et;if(f.ce!=en||f.cf!=fn){if(et==1&&b!=2&&'BUTTONSUBMITRESETIM"
+"AGERADIOCHECKBOXSELECT-ONEFILE'.indexOf(t)>-1){f.va[1]=fn;f.va[3]=e"
+"n;sc=true}else if(et==1&&b==2&&'TEXTAREAPASSWORDFILE'.indexOf(t)>-1"
+"){f.va[1]=fn;f.va[3]=en;sc=true}else if(et==2&&kp!=9&&kp!=13){f.va["
+"1]=fn;f.va[3]=en;sc=true}if(sc){nface=en;nfacf=fn}}if(et==1&&this.s"
+"_famd)return this.s_famd(e);if(et==2&&this.s_fakd)return this.s_fak"
+"d(e);");
s.ee=new Function("e","n",""
+"return n&&n.toLowerCase?e.toLowerCase()==n.toLowerCase():false;");
s.fage=new Function("e","a",""
+"var s=this,f=s.fa,x=f.cnt;x=x?x+1:1;f.cnt=x;return x==a?e:'';");
/*
 * Plugin: getNewRepeat 1.2 - Returns whether user is new or repeat
 */
s.getNewRepeat=new Function("d","cn",""
+"var s=this,e=new Date(),cval,sval,ct=e.getTime();d=d?d:30;cn=cn?cn:"
+"'s_nr';e.setTime(ct+d*24*60*60*1000);cval=s.c_r(cn);if(cval.length="
+"=0){s.c_w(cn,ct+'-New',e);return'New';}sval=s.split(cval,'-');if(ct"
+"-sval[0]<30*60*1000&&sval[1]=='New'){s.c_w(cn,ct+'-New',e);return'N"
+"ew';}else{s.c_w(cn,ct+'-Repeat',e);return'Repeat';}");

/*
 * Plugin: Days since last Visit 1.0.H - capture time from last visit
 */
s.getDaysSinceLastVisit=new Function(""
+"var s=this,e=new Date(),cval,ct=e.getTime(),c='s_lastvisit',day=24*"
+"60*60*1000;e.setTime(ct+3*365*day);cval=s.c_r(c);if(!cval){s.c_w(c,"
+"ct,e);return 'First page view or cookies not supported';}else{var d"
+"=ct-cval;if(d>30*60*1000){if(d>30*day){s.c_w(c,ct,e);return 'More t"
+"han 30 days';}if(d<30*day+1 && d>7*day){s.c_w(c,ct,e);return 'More "
+"than 7 days';}if(d<7*day+1 && d>day){s.c_w(c,ct,e);return 'Less tha"
+"n 7 days';}if(d<day+1){s.c_w(c,ct,e);return 'Less than 1 day';}}els"
+"e return '';}"
);
  /*
   * Plugin: getVisitNum - version 3.0
   */
   s.getVisitNum=new Function("tp","c","c2",""
  +"var s=this,e=new Date,cval,cvisit,ct=e.getTime(),d;if(!tp){tp='m';}"
  +"if(tp=='m'||tp=='w'||tp=='d'){eo=s.endof(tp),y=eo.getTime();e.setTi"
  +"me(y);}else {d=tp*86400000;e.setTime(ct+d);}if(!c){c='s_vnum';}if(!"
  +"c2){c2='s_invisit';}cval=s.c_r(c);if(cval){var i=cval.indexOf('&vn="
  +"'),str=cval.substring(i+4,cval.length),k;}cvisit=s.c_r(c2);if(cvisi"
  +"t){if(str){e.setTime(ct+1800000);s.c_w(c2,'true',e);return str;}els"
  +"e {return 'unknown visit number';}}else {if(str){str++;k=cval.substri"
  +"ng(0,i);e.setTime(k);s.c_w(c,k+'&vn='+str,e);e.setTime(ct+1800000);"
  +"s.c_w(c2,'true',e);return str;}else {s.c_w(c,e.getTime()+'&vn=1',e)"
  +";e.setTime(ct+1800000);s.c_w(c2,'true',e);return 1;}}");
  s.dimo=new Function("m","y",""
  +"var d=new Date(y,m+1,0);return d.getDate();");
  s.endof=new Function("x",""
  +"var t=new Date;t.setHours(0);t.setMinutes(0);t.setSeconds(0);if(x=="
  +"'m'){d=s.dimo(t.getMonth(),t.getFullYear())-t.getDate()+1;}else if("
  +"x=='w'){d=7-t.getDay();}else {d=1;}t.setDate(t.getDate()+d);return "
  +"t;");
/*
 * Plugin: getAndPersistValue 0.3 - get a value on every page
 */
s.getAndPersistValue=new Function("v","c","e",""
+"var s=this,a=new Date;e=e?e:0;a.setTime(a.getTime()+e*86400000);if("
+"v)s.c_w(c,v,e?a:0);return s.c_r(c);");
/*
 * Plugin: getTimeToComplete 0.4 - return the time from start to stop
 */
s.getTimeToComplete=new Function("v","cn","e",""
+"var s=this,d=new Date,x=d,k;if(!s.ttcr){e=e?e:0;if(v=='start'||v=='"
+"stop')s.ttcr=1;x.setTime(x.getTime()+e*86400000);if(v=='start'){s.c"
+"_w(cn,d.getTime(),e?x:0);return '';}if(v=='stop'){k=s.c_r(cn);if(!s"
+".c_w(cn,'',d)||!k)return '';v=(d.getTime()-k)/1000;var td=86400,th="
+"3600,tm=60,r=5,u,un;if(v>td){u=td;un='days';}else if(v>th){u=th;un="
+"'hours';}else if(v>tm){r=2;u=tm;un='minutes';}else{r=.2;u=1;un='sec"
+"onds';}v=v*r/u;return (Math.round(v)/r)+' '+un;}}return '';");
/*
 * Plugin: getVisitStart v2.0 - returns 1 on first page of visit
 * otherwise 0
 */
s.getVisitStart=new Function("c",""
+"var s=this,v=1,t=new Date;t.setTime(t.getTime()+1800000);if(s.c_r(c"
+")){v=0}if(!s.c_w(c,1,t)){s.c_w(c,1,0)}if(!s.c_r(c)){v=0}return v;");

/*
* Plugin: getPreviousValue_v1.0 - return previous value of designated
* variable (requires split utility)
*/
s.getPreviousValue=new Function("v","c","el",""
+"var s=this,t=new Date,i,j,r='';t.setTime(t.getTime()+1800000);if(el"
+"){if(s.events){i=s.split(el,',');j=s.split(s.events,',');for(x in i"
+"){for(y in j){if(i[x]==j[y]){if(s.c_r(c)) r=s.c_r(c);v?s.c_w(c,v,t)"
+":s.c_w(c,'no value',t);return r}}}}}else{if(s.c_r(c)) r=s.c_r(c);v?"
+"s.c_w(c,v,t):s.c_w(c,'no value',t);return r}");

/*
* Plugin: getPercentPageViewed v1.x
* This code has been modified from the original version distributed
* by Omniture and will not be supported by Omniture in any way
*/
s.getPercentPageViewed=new Function("",""
+"var s=this;if(typeof(s.linkType)=='undefined'||s.linkType=='e'){var"
+" v=s.c_r('s_ppv');s.c_w('s_ppv',0);return v;}");
s.getPPVCalc=new Function("",""
+"var dh=Math.max(Math.max(s.d.body.scrollHeight,s.d.documentElement."
+"scrollHeight),Math.max(s.d.body.offsetHeight,s.d.documentElement.of"
+"fsetHeight),Math.max(s.d.body.clientHeight,s.d.documentElement.clie"
+"ntHeight)),vph=s.d.clientHeight||Math.min(s.d.documentElement.clien"
+"tHeight,s.d.body.clientHeight),st=s.wd.pageYOffset||(s.wd.document."
+"documentElement.scrollTop||s.wd.document.body.scrollTop),vh=st+vph,"
+"pv=Math.round(vh/dh*100),cv=s.c_r('s_ppv'),cpi=cv.indexOf('|'),cpv="
+"'',ps='';if(cpi!=-1){cpv=cv.substring(0,cpi);ps=parseInt(cv.substri"
+"ng(cpi+1));}else{cpv=ps=0;}if(pv<=100){if(pv>parseInt(cpv)){ps=pv-M"
+"ath.round(vph/dh*100);s.c_w('s_ppv',pv+'|'+ps);}}else{s.c_w('s_ppv'"
+",'');}");
s.getPPVSetup=new Function("",""
+"var s=this;if(s.wd.addEventListener){s.wd.addEventListener('load',s"
+".getPPVCalc,false);s.wd.addEventListener('scroll',s.getPPVCalc,fals"
+"e);s.wd.addEventListener('resize',s.getPPVCalc,false);}else if(s.wd"
+".attachEvent){s.wd.attachEvent('onload',s.getPPVCalc);s.wd.attachEv"
+"ent('onscroll',s.getPPVCalc);s.wd.attachEvent('onresize',s.getPPVCa"
+"lc);}");
s.getPPVSetup();

/************************** ATLAS VARIABLES **************************/
/* @TODO: Fill in these variables with the settings mapped in the
 * ATLAS wizard and that match your desired preferences. 
 * @TODO: Comments should be removed in a production deployment. */
var adservConfig = {
    tEvar:              'eVar54', // Transfer variable, typically the "View Through" eVar.
    PartnerTag:         'swialo_AdobeGenesisSW_1', // Partner Tag 
    gID:                'ATL:', // Genesis ID
    requestURL:         "http://view.atdmt.com/partner/OMTR?ID=[PartnerTag]&var=[VAR]", // the Atlas request URL
    maxDelay:           "750", // The maximum time to wait for ATLAS servers to respond, in milliseconds.
    visitCookie:        "s_atlas", // The name of the visitor cookie to use to restrict ATLAS calls to once per visit.
    clickThroughParam:  "CID", // A query string paramter that will force the ATLAS call to occur.
    searchCenterParam:  undefined // SearchCenter identifier.
};
/************************ END ATLAS Variables ************************/

/*
 * Partner Plugin: Atlas Check 1.0 
 */

s.maxDelay = adservConfig.maxDelay;
s.loadModule("Integrate")
s.Integrate.onLoad=function(s,m) {
    var adservCheck = s.partnerADSERVCheck(adservConfig);
    if (adservCheck) {
        s.Integrate.add("Atlas_DMT");
        s.Integrate.Atlas_DMT.PartnerTag=adservConfig.PartnerTag;
        s.Integrate.Atlas_DMT.gID=adservConfig.gID;
        s.Integrate.Atlas_DMT.tVar=adservConfig.tEvar;
        s.Integrate.Atlas_DMT.get(adservConfig.requestURL);

        s.Integrate.Atlas_DMT.setVars=function(s,p){
            var
                at=p.lastImpTime,
                a1=p.lastImpSId,
                a2=p.lastImpPId,
                a3=p.lastImpAId,
                bt=p.lastClkTime,
                b1=p.lastClkSId,
                b2=p.lastClkPId,
                b3=p.lastClkAId;

            if(((at&&a1&&a2&&a3)||(bt&&b1&&b2&&b3))&&!p.errorCode)s[p.tVar]=adservConfig.gID+(at?at:0)+":"+(a1?a1:0)+":"+(a2?a2:0)+":"+(a3?a3:0)+":"+(bt?bt:0)+":"+(b1?b1:0)+":"+(b2?b2:0)+":"+(b3?b3:0)
        }
    }
}

/*
 * Partner Plugin: ADSERV Check 1.0 - Restrict ADSERV calls to once a visit, per report suite, per click
 * through. Used in conjunction with genesis_event_config table. Deduplicates SCM hits.
 */
s.partnerADSERVCheck=new Function("cfg",""
+"var s=this,c=cfg.visitCookie,src=cfg.clickThroughParam,scp=cfg.searchCenterParam,tv=cfg.tEvar,dl=',',cr,nc,q,g,gs,i,j,k,fnd,v=1,"
+"t=new Date,cn=0,ca=new Array,aa=new Array,cs=new Array;t.setTime(t.getTime()+1800000);cr=s.c_r(c);if(cr){v=0;}ca=s.split(cr,dl);"
+"aa=s.split(s.un,dl);for(i=0;i<aa.length;i++){fnd = 0;for(j=0;j<ca.length;j++){if(aa[i] == ca[j]){fnd=1;}}if(!fnd){cs[cn]=aa[i];c"
+"n++;}}if(cs.length){for(k=0;k<cs.length;k++){nc=(nc?nc+dl:'')+cs[k];}cr=(cr?cr+dl:'')+nc;v=1;}q=s.wd.location.search.toLowerCase("
+");q=s.repl(q,'?','&');g=q.indexOf('&'+src.toLowerCase()+'=');gs=(scp)?q.indexOf('&'+scp.toLowerCase()+'='):-1;if(g>-1){v=1;}else i"
+"f(gs>-1){v=0;s.vpr(tv,'SearchCenter Visitors');}if(!s.c_w(c,cr,t)){s.c_w(c,cr,0);}if(!s.c_r(c)){v=0;}return v>=1;");

/********************************************************************
 *
 * Supporting functions that may be shared between plug-ins
 *
 *******************************************************************/

/*
 * Utility Function: vpr - set the variable vs with value v
 */
s.vpr=new Function("vs","v",
"if(typeof(v)!='undefined'){var s=this; eval('s.'+vs+'=\"'+v+'\"')}");

/*
 * Utility Function: split v1.5 - split a string (JS 1.0 compatible)
 */
s.split=new Function("l","d",""
+"var i,x=0,a=new Array;while(l){i=l.indexOf(d);i=i>-1?i:l.length;a[x++]=l.substring(0,i);l=l.substring(i+d.length);}return a");

/*
 * Plugin Utility: Replace v1.0
 */
s.repl=new Function("x","o","n",""
+"var i=x.indexOf(o),l=n.length;while(x&&i>=0){x=x.substring(0,i)+n+x.substring(i+o.length);i=x.indexOf(o,i+l)}return x");


/* Module: Integrate */
s.m_Integrate_c="var m=s.m_i('Integrate');m.add=function(n,o){var m=this,p;if(!o)o='s_Integrate_'+n;if(!m.s.wd[o])m.s.wd[o]=new Object;m[n]=new Object;p=m[n];p._n=n;p._m=m;p._c=0;p._d=0;p.disable=0;p"
+".get=m.get;p.delay=m.delay;p.ready=m.ready;p.beacon=m.beacon;p.script=m.script;m.l[m.l.length]=n};m._g=function(t){var m=this,s=m.s,i,p,f=(t?'use':'set')+'Vars',tcf;for(i=0;i<m.l.length;i++){p=m[m."
+"l[i]];if(p&&!p.disable&&p[f]){if(s.apv>=5&&(!s.isopera||s.apv>=7)){tcf=new Function('s','p','f','var e;try{p[f](s,p)}catch(e){}');tcf(s,p,f)}else p[f](s,p)}}};m._t=function(){this._g(1)};m._fu=func"
+"tion(p,u){var m=this,s=m.s,v,x,y,z,tm=new Date;if(u.toLowerCase().substring(0,4) != 'http')u='http://'+u;if(s.ssl)u=s.rep(u,'http:','https:');p.RAND=Math&&Math.random?Math.floor(Math.random()*10000"
+"000000000):tm.getTime();p.RAND+=Math.floor(tm.getTime()/10800000)%10;x=0;while(x>=0){x=u.indexOf('[',x);if(x>=0){y=u.indexOf(']',x);if(y>x){z=u.substring(x+1,y);if(z.length>2&&z.substring(0,2)=='s."
+"'){v=s[z.substring(2)];if(!v)v=''}else{v=''+p[z];if(!(v==p[z]||parseFloat(v)==p[z]))z=0}if(z) {u=u.substring(0,x)+s.rep(escape(v),'+','%2B')+u.substring(y+1);x=y-(z.length-v.length+1)} else {x=y}}}"
+"}return u};m.get=function(u,v){var p=this,m=p._m;if(!p.disable){if(!v)v='s_'+m._in+'_Integrate_'+p._n+'_get_'+p._c;p._c++;p.VAR=v;p._d++;m.s.loadModule('Integrate:'+v,m._fu(p,u),0,1,p._n)}};m.delay"
+"=function(){var p=this;if(p._d<=0)p._d=1};m.ready=function(){var p=this,m=p._m;p._d=0;if(!p.disable)m.s.dlt()};m._d=function(){var m=this,i,p;for(i=0;i<m.l.length;i++){p=m[m.l[i]];if(p&&!p.disable&"
+"&p._d>0)return 1}return 0};m._x=function(d,n){var p=this[n],x;if(!p.disable){for(x in d)if(x&&(!Object||!Object.prototype||!Object.prototype[x]))p[x]=d[x];p._d--}};m.beacon=function(u){var p=this,m"
+"=p._m,s=m.s,imn='s_i_'+m._in+'_Integrate_'+p._n+'_'+p._c,im;if(!p.disable&&s.d.images&&s.apv>=3&&(!s.isopera||s.apv>=7)&&(s.ns6<0||s.apv>=6.1)){p._c++;im=s.wd[imn]=new Image;im.src=m._fu(p,u)}};m.s"
+"cript=function(u){var p=this,m=p._m;if(!p.disable)m.s.loadModule(0,m._fu(p,u),0,1)};m.l=new Array;if(m.onLoad)m.onLoad(s,m)";
s.m_i("Integrate");


/************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/
s.visitorNamespace="starwoodhotels"
s.trackingServer="http://www.starwoodhotels.com/common/js/omniture/metrics.starwoodhotels.com"
s.trackingServerSecure="http://www.starwoodhotels.com/common/js/omniture/smetrics.starwoodhotels.com"
s.dc=112
/****************************** MEDIA MODULE - PLEASE DO NOT ALTER! *****************************/
/* Module: Media */
s.m_Media_c="var m=s.m_i('Media');if(m.completeByCloseOffset==undefined)m.completeByCloseOffset=1;if(m.completeCloseOffsetThreshold==undefined)m.completeCloseOffsetThreshold=1;m.cn=function(n){var m="
	+"this;return m.s.rep(m.s.rep(m.s.rep(n,\"\\n\",''),\"\\r\",''),'--**--','')};m.open=function(n,l,p,b){var m=this,i=new Object,tm=new Date,a='',x;n=m.cn(n);if(!l)l=-1;if(n&&p){if(!m.l)m.l=new Object;"
	+"if(m.l[n])m.close(n);if(b&&b.id)a=b.id;if(a)for (x in m.l)if(m.l[x]&&m.l[x].a==a)m.close(m.l[x].n);i.n=n;i.l=l;i.o=0;i.x=0;i.p=m.cn(m.playerName?m.playerName:p);i.a=a;i.t=0;i.ts=0;i.s=Math.floor(tm"
	+".getTime()/1000);i.lx=0;i.lt=i.s;i.lo=0;i.e='';i.to=-1;i.tc=0;i.fel=new Object;i.vt=0;i.sn=0;i.sx=\"\";i.sl=0;i.sg=0;i.sc=0;i.us=0;i.ad=0;i.adpn;i.adpp;i.adppp;i.clk;i.CPM;i.co=0;i.cot=0;i.lm=0;i.l"
	+"om=0;m.l[n]=i}};m.openAd=function(n,l,p,pn,pp,ppp,CPM,b){var m=this,i=new Object;n=m.cn(n);m.open(n,l,p,b);i=m.l[n];if(i){i.ad=1;i.adpn=m.cn(pn);i.adpp=pp;i.adppp=ppp;i.CPM=CPM}};m._delete=function"
	+"(n){var m=this,i;n=m.cn(n);i=m.l[n];m.l[n]=0;if(i&&i.m)clearTimeout(i.m.i)};m.close=function(n){this.e(n,0,-1)};m.play=function(n,o,sn,sx,sl){var m=this,i;i=m.e(n,1,o,sn,sx,sl);if(i&&!i.m){i.m=new "
	+"Object;i.m.m=new Function('var m=s_c_il['+m._in+'],i;if(m.l){i=m.l[\"'+m.s.rep(i.n,'\"','\\\\\"')+'\"];if(i){if(i.lx==1)m.e(i.n,3,-1);i.m.i=setTimeout(i.m.m,1000)}}');i.m.m()}};m.click=function(n,o"
	+"){this.e(n,7,o)};m.complete=function(n,o){this.e(n,5,o)};m.stop=function(n,o){this.e(n,2,o)};m.track=function(n){this.e(n,4,-1)};m.bcd=function(vo,i){var m=this,ns='a.media.',v=vo.linkTrackVars,e=v"
	+"o.linkTrackEvents,pe='m_i',pev3,c=vo.contextData,x;if(i.ad){ns+='ad.';if(i.adpn){c['http://www.starwoodhotels.com/common/js/omniture/a.media.name']=i.adpn;c[ns+'pod']=i.adpp;c[ns+'podPosition']=i.adppp;}if(!i.vt)c[ns+'CPM']=i.CPM;}if (i.clk) {c[n"
	+"s+'clicked']=true;i.clk=0}c['a.contentType']='video'+(i.ad?'Ad':'');c['a.media.channel']=m.channel;c[ns+'name']=i.n;c[ns+'playerName']=i.p;if(i.l>0)c[ns+'length']=i.l;if(Math.floor(i.ts)>0)c[ns+'ti"
	+"mePlayed']=Math.floor(i.ts);if(!i.vt){c[ns+'view']=true;pe='m_s';i.vt=1}if(i.sx){c[ns+'segmentNum']=i.sn;c[ns+'segment']=i.sx;if(i.sl>0)c[ns+'segmentLength']=i.sl;if(i.sc&&i.ts>0)c[ns+'segmentView'"
	+"]=true}if(!i.cot&&i.co){c[ns+\"complete\"]=true;i.cot=1}if(i.lm>0)c[ns+'milestone']=i.lm;if(i.lom>0)c[ns+'offsetMilestone']=i.lom;if(v)for(x in c)v+=',contextData.'+x;pev3=c['a.contentType'];vo.pe="
	+"pe;vo.pev3=pev3;var d=m.contextDataMapping,y,a,l,n;if(d){vo.events2='';if(v)v+=',events';for(x in d){if(x.substring(0,ns.length)==ns)y=x.substring(ns.length);else y=\"\";a=d[x];if(typeof(a)=='strin"
	+"g'){l=m.s.sp(a,',');for(n=0;n<l.length;n++){a=l[n];if(x==\"a.contentType\"){if(v)v+=','+a;vo[a]=c[x]}else if(y=='view'||y=='segmentView'||y=='clicked'||y=='complete'||y=='timePlayed'||y=='CPM'){if("
	+"e)e+=','+a;if(y=='timePlayed'||y=='CPM'){if(c[x])vo.events2+=(vo.events2?',':'')+a+'='+c[x];}else if(c[x])vo.events2+=(vo.events2?',':'')+a}else if(y=='segment'&&c[x+'Num']){if(v)v+=','+a;vo[a]=c[x"
	+"+'Num']+':'+c[x]}else{if(v)v+=','+a;vo[a]=c[x]}}}else if(y=='milestones'||y=='offsetMilestones'){x=x.substring(0,x.length-1);if(c[x]&&d[x+'s'][c[x]]){if(e)e+=','+d[x+'s'][c[x]];vo.events2+=(vo.even"
	+"ts2?',':'')+d[x+'s'][c[x]]}}if(c[x])c[x]=undefined;if(y=='segment'&&c[x+'Num'])c[x+\"Num\"]=undefined}}vo.linkTrackVars=v;vo.linkTrackEvents=e};m.bpe=function(vo,i,x,o){var m=this,pe='m_o',pev3,d='"
	+"--**--';pe='m_o';if(!i.vt){pe='m_s';i.vt=1}else if(x==4)pe='m_i';pev3=m.s.ape(i.n)+d+Math.floor(i.l>0?i.l:1)+d+m.s.ape(i.p)+d+Math.floor(i.t)+d+i.s+d+(i.to>=0?'L'+Math.floor(i.to):'')+i.e+(x!=0&&x!"
	+"=2?'L'+Math.floor(o):'');vo.pe=pe;vo.pev3=pev3};m.e=function(n,x,o,sn,sx,sl,pd){var m=this,i,tm=new Date,ts=Math.floor(tm.getTime()/1000),c,l,v=m.trackVars,e=m.trackEvents,ti=m.trackSeconds,tp=m.tr"
	+"ackMilestones,to=m.trackOffsetMilestones,sm=m.segmentByMilestones,so=m.segmentByOffsetMilestones,z=new Array,j,t=1,w=new Object,x,ek,tc,vo=new Object;if(!m.channel)m.channel=m.s.wd.location.hostnam"
	+"e;n=m.cn(n);i=n&&m.l&&m.l[n]?m.l[n]:0;if(i){if(i.ad){ti=m.adTrackSeconds;tp=m.adTrackMilestones;to=m.adTrackOffsetMilestones;sm=m.adSegmentByMilestones;so=m.adSegmentByOffsetMilestones}if(o<0){if(i"
	+".lx==1&&i.lt>0)o=(ts-i.lt)+i.lo;else o=i.lo}if(i.l>0)o=o<i.l?o:i.l;if(o<0)o=0;i.o=o;if(i.l>0){i.x=(i.o/i.l)*100;i.x=i.x>100?100:i.x}if(i.lo<0)i.lo=o;tc=i.tc;w.name=n;w.ad=i.ad;w.length=i.l;w.openTi"
	+"me=new Date;w.openTime.setTime(i.s*1000);w.offset=i.o;w.percent=i.x;w.playerName=i.p;if(i.to<0)w.mediaEvent=w.event='OPEN';else w.mediaEvent=w.event=(x==1?'PLAY':(x==2?'STOP':(x==3?'MONITOR':(x==4?"
	+"'TRACK':(x==5?'COMPLETE':(x==7?'CLICK':('CLOSE')))))));if(!pd){if(i.pd)pd=i.pd}else i.pd=pd;w.player=pd;if(x>2||(x!=i.lx&&(x!=2||i.lx==1))) {if(!sx){sn=i.sn;sx=i.sx;sl=i.sl}if(x){if(x==1)i.lo=o;if("
	+"(x<=3||x>=5)&&i.to>=0){t=0;v=e=\"None\";if(i.to!=o){l=i.to;if(l>o){l=i.lo;if(l>o)l=o}z=tp?m.s.sp(tp,','):0;if(i.l>0&&z&&o>=l)for(j=0;j<z.length;j++){c=z[j]?parseFloat(''+z[j]):0;if(c&&(l/i.l)*100<c"
	+"&&i.x>=c){t=1;j=z.length;w.mediaEvent=w.event='MILESTONE';i.lm=w.milestone=c}}z=to?m.s.sp(to,','):0;if(z&&o>=l)for(j=0;j<z.length;j++){c=z[j]?parseFloat(''+z[j]):0;if(c&&l<c&&o>=c){t=1;j=z.length;w"
	+".mediaEvent=w.event='OFFSET_MILESTONE';i.lom=w.offsetMilestone=c}}}}if(i.sg||!sx){if(sm&&tp&&i.l>0){z=m.s.sp(tp,',');if(z){z[z.length]='100';l=0;for(j=0;j<z.length;j++){c=z[j]?parseFloat(''+z[j]):0"
	+";if(c){if(i.x<c){sn=j+1;sx='M:'+l+'-'+c;j=z.length}l=c}}}}else if(so&&to){z=m.s.sp(to,',');if(z){z[z.length]=''+(i.l>0?i.l:'E');l=0;for(j=0;j<z.length;j++){c=z[j]?parseFloat(''+z[j]):0;if(c||z[j]=="
	+"'E'){if(o<c||z[j]=='E'){sn=j+1;sx='O:'+l+'-'+c;j=z.length}l=c}}}}if(sx)i.sg=1}if((sx||i.sx)&&sx!=i.sx){i.us=1;if(!i.sx){i.sn=sn;i.sx=sx}if(i.to>=0)t=1}if((x>=2||i.x>=100)&&i.lo<o){i.t+=o-i.lo;i.ts+"
	+"=o-i.lo}if(x<=2||(x==3&&!i.lx)){i.e+=(x==1||x==3?'S':'E')+Math.floor(o);i.lx=(x==3?1:x)}if(!t&&i.to>=0&&x<=3){ti=ti?ti:0;if(ti&&i.ts>=ti){t=1;w.mediaEvent=w.event='SECONDS'}}i.lt=ts;i.lo=o}if(!x||("
	+"x<=3&&i.x>=100)){if(i.lx!=2)i.e+='E'+Math.floor(o);x=0;v=e=\"None\";w.mediaEvent=w.event=\"CLOSE\"}if(x==7){w.clicked=i.clk=1;t=1}if(x==5||(m.completeByCloseOffset&&(!x||i.x>=100)&&i.l>0&&o>=i.l-m."
	+"completeCloseOffsetThreshold)){w.complete=i.co=1;t=1}ek=w.mediaEvent;if(ek=='MILESTONE')ek+='_'+w.milestone;else if(ek=='OFFSET_MILESTONE')ek+='_'+w.offsetMilestone;if(!i.fel[ek]) {w.eventFirstTime"
	+"=true;i.fel[ek]=1}else w.eventFirstTime=false;w.timePlayed=i.t;w.segmentNum=i.sn;w.segment=i.sx;w.segmentLength=i.sl;if(m.monitor&&x!=4)m.monitor(m.s,w);if(x==0)m._delete(n);if(t&&i.tc==tc){vo=new "
	+"Object;vo.contextData=new Object;vo.linkTrackVars=v;vo.linkTrackEvents=e;if(!vo.linkTrackVars)vo.linkTrackVars='';if(!vo.linkTrackEvents)vo.linkTrackEvents='';if(m.trackUsingContextData)m.bcd(vo,i)"
	+";else m.bpe(vo,i,x,o);m.s.t(vo);if(i.us){i.sn=sn;i.sx=sx;i.sc=1;i.us=0}else if(i.ts>0)i.sc=0;i.e=\"\";i.lm=i.lom=0;i.ts-=Math.floor(i.ts);i.to=o;i.tc++}}}return i};m.ae=function(n,l,p,x,o,sn,sx,sl,"
	+"pd,b){var m=this,r=0;if(n&&(!m.autoTrackMediaLengthRequired||(length&&length>0)) &&p){if(!m.l||!m.l[n]){if(x==1||x==3){m.open(n,l,p,b);r=1}}else r=1;if(r)m.e(n,x,o,sn,sx,sl,pd)}};m.a=function(o,t){"
	+"var m=this,i=o.id?o.id:o.name,n=o.name,p=0,v,c,c1,c2,xc=m.s.h,x,e,f1,f2='s_media_'+m._in+'_oc',f3='s_media_'+m._in+'_t',f4='s_media_'+m._in+'_s',f5='s_media_'+m._in+'_l',f6='s_media_'+m._in+'_m',f7"
	+"='s_media_'+m._in+'_c',tcf,w;if(!i){if(!m.c)m.c=0;i='s_media_'+m._in+'_'+m.c;m.c++}if(!o.id)o.id=i;if(!o.name)o.name=n=i;if(!m.ol)m.ol=new Object;if(m.ol[i])return;m.ol[i]=o;if(!xc)xc=m.s.b;tcf=new"
	+" Function('o','var e,p=0;try{if(o.versionInfo&&o.currentMedia&&o.controls)p=1}catch(e){p=0}return p');p=tcf(o);if(!p){tcf=new Function('o','var e,p=0,t;try{t=o.GetQuickTimeVersion();if(t)p=2}catch("
	+"e){p=0}return p');p=tcf(o);if(!p){tcf=new Function('o','var e,p=0,t;try{t=o.GetVersionInfo();if(t)p=3}catch(e){p=0}return p');p=tcf(o)}}v=\"var m=s_c_il[\"+m._in+\"],o=m.ol['\"+i+\"']\";if(p==1){p="
	+"'Windows Media Player '+o.versionInfo;c1=v+',n,p,l,x=-1,cm,c,mn;if(o){cm=o.currentMedia;c=o.controls;if(cm&&c){mn=cm.name?cm.name:c.URL;l=cm.duration;p=c.currentPosition;n=o.playState;if(n){if(n==8"
	+")x=0;if(n==3)x=1;if(n==1||n==2||n==4||n==5||n==6)x=2;}';c2='if(x>=0)m.ae(mn,l,\"'+p+'\",x,x!=2?p:-1,0,\"\",0,0,o)}}';c=c1+c2;if(m.s.isie&&xc){x=m.s.d.createElement('script');x.language='jscript';x."
	+"type='text/javascript';x.htmlFor=i;x.event='PlayStateChange(NewState)';x.defer=true;x.text=c;xc.appendChild(x);o[f6]=new Function(c1+'if(n==3){x=3;'+c2+'}setTimeout(o.'+f6+',5000)');o[f6]()}}if(p=="
	+"2){p='QuickTime Player '+(o.GetIsQuickTimeRegistered()?'Pro ':'')+o.GetQuickTimeVersion();f1=f2;c=v+',n,x,t,l,p,p2,mn;if(o){mn=o.GetMovieName()?o.GetMovieName():o.GetURL();n=o.GetRate();t=o.GetTime"
	+"Scale();l=o.GetDuration()/t;p=o.GetTime()/t;p2=o.'+f5+';if(n!=o.'+f4+'||p<p2||p-p2>5){x=2;if(n!=0)x=1;else if(p>=l)x=0;if(p<p2||p-p2>5)m.ae(mn,l,\"'+p+'\",2,p2,0,\"\",0,0,o);m.ae(mn,l,\"'+p+'\",x,x"
	+"!=2?p:-1,0,\"\",0,0,o)}if(n>0&&o.'+f7+'>=10){m.ae(mn,l,\"'+p+'\",3,p,0,\"\",0,0,o);o.'+f7+'=0}o.'+f7+'++;o.'+f4+'=n;o.'+f5+'=p;setTimeout(\"'+v+';o.'+f2+'(0,0)\",500)}';o[f1]=new Function('a','b',c"
	+");o[f4]=-1;o[f7]=0;o[f1](0,0)}if(p==3){p='RealPlayer '+o.GetVersionInfo();f1=n+'_OnPlayStateChange';c1=v+',n,x=-1,l,p,mn;if(o){mn=o.GetTitle()?o.GetTitle():o.GetSource();n=o.GetPlayState();l=o.GetL"
	+"ength()/1000;p=o.GetPosition()/1000;if(n!=o.'+f4+'){if(n==3)x=1;if(n==0||n==2||n==4||n==5)x=2;if(n==0&&(p>=l||p==0))x=0;if(x>=0)m.ae(mn,l,\"'+p+'\",x,x!=2?p:-1,0,\"\",0,0,o)}if(n==3&&(o.'+f7+'>=10|"
	+"|!o.'+f3+')){m.ae(mn,l,\"'+p+'\",3,p,0,\"\",0,0,o);o.'+f7+'=0}o.'+f7+'++;o.'+f4+'=n;';c2='if(o.'+f2+')o.'+f2+'(o,n)}';if(m.s.wd[f1])o[f2]=m.s.wd[f1];m.s.wd[f1]=new Function('a','b',c1+c2);o[f1]=new"
	+" Function('a','b',c1+'setTimeout(\"'+v+';o.'+f1+'(0,0)\",o.'+f3+'?500:5000);'+c2);o[f4]=-1;if(m.s.isie)o[f3]=1;o[f7]=0;o[f1](0,0)}};m.as=new Function('e','var m=s_c_il['+m._in+'],l,n;if(m.autoTrack"
	+"&&m.s.d.getElementsByTagName){l=m.s.d.getElementsByTagName(m.s.isie?\"OBJECT\":\"EMBED\");if(l)for(n=0;n<l.length;n++)m.a(l[n]);}');if(s.wd.attachEvent)s.wd.attachEvent('onload',m.as);else if(s.wd."
	+"addEventListener)s.wd.addEventListener('load',m.as,false);if(m.onLoad)m.onLoad(s,m)";s.m_i("Media");
	
/************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/
	var s_code='',s_objectID;function s_gi(un,pg,ss){var c="s.version='H.27.2';s.an=s_an;s.logDebug=function(m){var s=this,tcf=new Function('var e;try{console.log(\"'+s.rep(s.rep(s.rep(m,\"\\\\\",\"\\\\"
		+"\\\\\"),\"\\n\",\"\\\\n\"),\"\\\"\",\"\\\\\\\"\")+'\");}catch(e){}');tcf()};s.cls=function(x,c){var i,y='';if(!c)c=this.an;for(i=0;i<x.length;i++){n=x.substring(i,i+1);if(c.indexOf(n)>=0)y+=n}retur"
		+"n y};s.fl=function(x,l){return x?(''+x).substring(0,l):x};s.co=function(o){return o};s.num=function(x){x=''+x;for(var p=0;p<x.length;p++)if(('0123456789').indexOf(x.substring(p,p+1))<0)return 0;ret"
		+"urn 1};s.rep=s_rep;s.sp=s_sp;s.jn=s_jn;s.ape=function(x){var s=this,h='0123456789ABCDEF',f=\"+~!*()'\",i,c=s.charSet,n,l,e,y='';c=c?c.toUpperCase():'';if(x){x=''+x;if(s.em==3){x=encodeURIComponent("
		+"x);for(i=0;i<f.length;i++) {n=f.substring(i,i+1);if(x.indexOf(n)>=0)x=s.rep(x,n,\"%\"+n.charCodeAt(0).toString(16).toUpperCase())}}else if(c=='AUTO'&&('').charCodeAt){for(i=0;i<x.length;i++){c=x.su"
		+"bstring(i,i+1);n=x.charCodeAt(i);if(n>127){l=0;e='';while(n||l<4){e=h.substring(n%16,n%16+1)+e;n=(n-n%16)/16;l++}y+='%u'+e}else if(c=='+')y+='%2B';else y+=escape(c)}x=y}else x=s.rep(escape(''+x),'+"
		+"','%2B');if(c&&c!='AUTO'&&s.em==1&&x.indexOf('%u')<0&&x.indexOf('%U')<0){i=x.indexOf('%');while(i>=0){i++;if(h.substring(8).indexOf(x.substring(i,i+1).toUpperCase())>=0)return x.substring(0,i)+'u00"
		+"'+x.substring(i);i=x.indexOf('%',i)}}}return x};s.epa=function(x){var s=this,y,tcf;if(x){x=s.rep(''+x,'+',' ');if(s.em==3){tcf=new Function('x','var y,e;try{y=decodeURIComponent(x)}catch(e){y=unesc"
		+"ape(x)}return y');return tcf(x)}else return unescape(x)}return y};s.pt=function(x,d,f,a){var s=this,t=x,z=0,y,r;while(t){y=t.indexOf(d);y=y<0?t.length:y;t=t.substring(0,y);r=s[f](t,a);if(r)return r"
		+";z+=y+d.length;t=x.substring(z,x.length);t=z<x.length?t:''}return ''};s.isf=function(t,a){var c=a.indexOf(':');if(c>=0)a=a.substring(0,c);c=a.indexOf('=');if(c>=0)a=a.substring(0,c);if(t.substring("
		+"0,2)=='s_')t=t.substring(2);return (t!=''&&t==a)};s.fsf=function(t,a){var s=this;if(s.pt(a,',','isf',t))s.fsg+=(s.fsg!=''?',':'')+t;return 0};s.fs=function(x,f){var s=this;s.fsg='';s.pt(x,',','fsf'"
		+",f);return s.fsg};s.mpc=function(m,a){var s=this,c,l,n,v;v=s.d.visibilityState;if(!v)v=s.d.webkitVisibilityState;if(v&&v=='prerender'){if(!s.mpq){s.mpq=new Array;l=s.sp('webkitvisibilitychange,visi"
		+"bilitychange',',');for(n=0;n<l.length;n++){s.d.addEventListener(l[n],new Function('var s=s_c_il['+s._in+'],c,v;v=s.d.visibilityState;if(!v)v=s.d.webkitVisibilityState;if(s.mpq&&v==\"visible\"){whil"
		+"e(s.mpq.length>0){c=s.mpq.shift();s[c.m].apply(s,c.a)}s.mpq=0}'),false)}}c=new Object;c.m=m;c.a=a;s.mpq.push(c);return 1}return 0};s.si=function(){var s=this,i,k,v,c=s_gi+'var s=s_gi(\"'+s.oun+'\")"
		+";s.sa(\"'+s.un+'\");';for(i=0;i<s.va_g.length;i++){k=s.va_g[i];v=s[k];if(v!=undefined){if(typeof(v)!='number')c+='s.'+k+'=\"'+s_fe(v)+'\";';else c+='s.'+k+'='+v+';'}}c+=\"s.lnk=s.eo=s.linkName=s.li"
		+"nkType=s.wd.s_objectID=s.ppu=s.pe=s.pev1=s.pev2=s.pev3='';\";return c};s.c_d='';s.c_gdf=function(t,a){var s=this;if(!s.num(t))return 1;return 0};s.c_gd=function(){var s=this,d=s.wd.location.hostnam"
		+"e,n=s.fpCookieDomainPeriods,p;if(!n)n=s.cookieDomainPeriods;if(d&&!s.c_d){n=n?parseInt(n):2;n=n>2?n:2;p=d.lastIndexOf('.');if(p>=0){while(p>=0&&n>1){p=d.lastIndexOf('.',p-1);n--}s.c_d=p>0&&s.pt(d,'"
		+".','c_gdf',0)?d.substring(p):d}}return s.c_d};s.c_r=function(k){var s=this;k=s.ape(k);var c=' '+s.d.cookie,i=c.indexOf(' '+k+'='),e=i<0?i:c.indexOf(';',i),v=i<0?'':s.epa(c.substring(i+2+k.length,e<"
		+"0?c.length:e));return v!='[[B]]'?v:''};s.c_w=function(k,v,e){var s=this,d=s.c_gd(),l=s.cookieLifetime,t;v=''+v;l=l?(''+l).toUpperCase():'';if(e&&l!='SESSION'&&l!='NONE'){t=(v!=''?parseInt(l?l:0):-6"
		+"0);if(t){e=new Date;e.setTime(e.getTime()+(t*1000))}}if(k&&l!='NONE'){s.d.cookie=k+'='+s.ape(v!=''?v:'[[B]]')+'; path=/;'+(e&&l!='SESSION'?' expires='+e.toGMTString()+';':'')+(d?' domain='+d+';':''"
		+");return s.c_r(k)==v}return 0};s.eh=function(o,e,r,f){};s.cet=function(f,a,t,o,b){var s=this,r,tcf;if(s.apv>=5&&(!s.isopera||s.apv>=7)){tc"
		+"f=new Function('s','f','a','t','var e,r;try{r=s[f](a)}catch(e){r=s[t](e)}return r');r=tcf(s,f,a,t)}else{if(s.ismac&&s.u.indexOf('MSIE 4')>=0)r=s[b](a);else{s.eh(s.wd,'onerror',0,o);r=s[f](a);s.eh(s"
		+".wd,'onerror',1)}}return r};s.gtfset=function(e){var s=this;return s.tfs};s.gtfsoe=new Function('e','var s=s_c_il['+s._in+'],c;s.eh(window,\"onerror\",1);s.etfs=1;c=s.t();if(c)s.d.write(c);s.etfs=0"
		+";return true');s.gtfsfb=function(a){return window};s.gtfsf=function(w){var s=this,p=w.parent,l=w.location;s.tfs=w;if(p&&p.location!=l&&p.location.host==l.host){s.tfs=p;return s.gtfsf(s.tfs)}return "
		+"s.tfs};s.gtfs=function(){var s=this;if(!s.tfs){s.tfs=s.wd;if(!s.etfs)s.tfs=s.cet('gtfsf',s.tfs,'gtfset',s.gtfsoe,'gtfsfb')}return s.tfs};s.mrq=function(u){var s=this,l=s.rl[u],n,r;s.rl[u]=0;if(l)fo"
		+"r(n=0;n<l.length;n++){r=l[n];s.mr(0,0,r.r,r.t,r.u)}};s.flushBufferedRequests=function(){};s.mr=function(sess,q,rs,ta,u){var s=this,dc=s.dc,t1=s.trackingServer,t2=s.trackingServerSecure,tb=s.trackin"
		+"gServerBase,p='.sc',ns=s.visitorNamespace,un=s.cls(u?u:(ns?ns:s.fun)),r=new Object,l,imn='s_i_'+s._in+'_'+un,im,b,e;if(!rs){if(t1){if(t2&&s.ssl)t1=t2}else{if(!tb)tb='http://www.starwoodhotels.com/common/js/omniture/2o7.net';if(dc)dc=(''+dc).toLow"
		+"erCase();else dc='d1';if(tb=='http://www.starwoodhotels.com/common/js/omniture/2o7.net'){if(dc=='d1')dc='112';else if(dc=='d2')dc='122';p=''}t1=un+'.'+dc+'.'+p+tb}rs='http'+(s.ssl?'s':'')+'://'+t1+'/b/ss/'+s.un+'/'+(s.mobile?'5.1':'1')+'/'+s.vers"
		+"ion+(s.tcn?'T':'')+'/'+sess+'?AQB=1&ndh=1'+(q?q:'')+'&AQE=1';if(s.isie&&!s.ismac)rs=s.fl(rs,2047)}if(s.d.images&&s.apv>=3&&(!s.isopera||s.apv>=7)&&(s.ns6<0||s.apv>=6.1)){if(!s.rc)s.rc=new Object;if"
		+"(!s.rc[un]){s.rc[un]=1;if(!s.rl)s.rl=new Object;s.rl[un]=new Array;setTimeout('if(window.s_c_il)window.s_c_il['+s._in+'].mrq(\"'+un+'\")',750)}else{l=s.rl[un];if(l){r.t=ta;r.u=un;r.r=rs;l[l.length]"
		+"=r;return ''}imn+='_'+s.rc[un];s.rc[un]++}if(s.debugTracking){var d='AppMeasurement Debug: '+rs,dl=s.sp(rs,'&'),dln;for(dln=0;dln<dl.length;dln++)d+=\"\\n\\t\"+s.epa(dl[dln]);s.logDebug(d)}im=s.wd["
		+"imn];if(!im)im=s.wd[imn]=new Image;im.alt=\"\";im.s_l=0;im.onload=im.onerror=new Function('e','this.s_l=1;var wd=window,s;if(wd.s_c_il){s=wd.s_c_il['+s._in+'];s.bcr();s.mrq(\"'+un+'\");s.nrs--;if(!"
		+"s.nrs)s.m_m(\"rr\")}');if(!s.nrs){s.nrs=1;s.m_m('rs')}else s.nrs++;im.src=rs;if(s.useForcedLinkTracking||s.bcf){if(!s.forcedLinkTrackingTimeout)s.forcedLinkTrackingTimeout=250;setTimeout('if(window"
		+".s_c_il)window.s_c_il['+s._in+'].bcr()',s.forcedLinkTrackingTimeout);}else if((s.lnk||s.eo)&&(!ta||ta=='_self'||ta=='_top'||ta=='_parent'||(s.wd.name&&ta==s.wd.name))){b=e=new Date;while(!im.s_l&&e"
		+".getTime()-b.getTime()<500)e=new Date}return ''}return '<im'+'g sr'+'c=\"'+rs+'\" width=1 height=1 border=0 alt=\"\">'};s.gg=function(v){var s=this;if(!s.wd['s_'+v])s.wd['s_'+v]='';return s.wd['s_'"
		+"+v]};s.glf=function(t,a){if(t.substring(0,2)=='s_')t=t.substring(2);var s=this,v=s.gg(t);if(v)s[t]=v};s.gl=function(v){var s=this;if(s.pg)s.pt(v,',','glf',0)};s.rf=function(x){var s=this,y,i,j,h,p,"
		+"l=0,q,a,b='',c='',t;if(x&&x.length>255){y=''+x;i=y.indexOf('?');if(i>0){q=y.substring(i+1);y=y.substring(0,i);h=y.toLowerCase();j=0;if(h.substring(0,7)=='http://')j+=7;else if(h.substring(0,8)=='ht"
		+"tps://')j+=8;i=h.indexOf(\"/\",j);if(i>0){h=h.substring(j,i);p=y.substring(i);y=y.substring(0,i);if(h.indexOf('google')>=0)l=',q,ie,start,search_key,word,kw,cd,';else if(h.indexOf('http://www.starwoodhotels.com/common/js/omniture/yahoo.co')>=0)l="
		+"',p,ei,';if(l&&q){a=s.sp(q,'&');if(a&&a.length>1){for(j=0;j<a.length;j++){t=a[j];i=t.indexOf('=');if(i>0&&l.indexOf(','+t.substring(0,i)+',')>=0)b+=(b?'&':'')+t;else c+=(c?'&':'')+t}if(b&&c)q=b+'&'"
		+"+c;else c=''}i=253-(q.length-c.length)-y.length;x=y+(i>0?p.substring(0,i):'')+'?'+q}}}}return x};s.s2q=function(k,v,vf,vfp,f){var s=this,qs='',sk,sv,sp,ss,nke,nk,nf,nfl=0,nfn,nfm;if(k==\"contextDat"
		+"a\")k=\"c\";if(v){for(sk in v)if((!f||sk.substring(0,f.length)==f)&&v[sk]&&(!vf||vf.indexOf(','+(vfp?vfp+'.':'')+sk+',')>=0)&&(!Object||!Object.prototype||!Object.prototype[sk])){nfm=0;if(nfl)for(n"
		+"fn=0;nfn<nfl.length;nfn++)if(sk.substring(0,nfl[nfn].length)==nfl[nfn])nfm=1;if(!nfm){if(qs=='')qs+='&'+k+'.';sv=v[sk];if(f)sk=sk.substring(f.length);if(sk.length>0){nke=sk.indexOf('.');if(nke>0){n"
		+"k=sk.substring(0,nke);nf=(f?f:'')+nk+'.';if(!nfl)nfl=new Array;nfl[nfl.length]=nf;qs+=s.s2q(nk,v,vf,vfp,nf)}else{if(typeof(sv)=='boolean'){if(sv)sv='true';else sv='false'}if(sv){if(vfp=='retrieveLi"
		+"ghtData'&&f.indexOf('.contextData.')<0){sp=sk.substring(0,4);ss=sk.substring(4);if(sk=='transactionID')sk='xact';else if(sk=='channel')sk='ch';else if(sk=='campaign')sk='v0';else if(s.num(ss)){if(s"
		+"p=='prop')sk='c'+ss;else if(sp=='eVar')sk='v'+ss;else if(sp=='list')sk='l'+ss;else if(sp=='hier'){sk='h'+ss;sv=sv.substring(0,255)}}}qs+='&'+s.ape(sk)+'='+s.ape(sv)}}}}}if(qs!='')qs+='&.'+k}return "
		+"qs};s.hav=function(){var s=this,qs='',l,fv='',fe='',mn,i,e;if(s.lightProfileID){l=s.va_m;fv=s.lightTrackVars;if(fv)fv=','+fv+','+s.vl_mr+','}else{l=s.va_t;if(s.pe||s.linkType){fv=s.linkTrackVars;fe"
		+"=s.linkTrackEvents;if(s.pe){mn=s.pe.substring(0,1).toUpperCase()+s.pe.substring(1);if(s[mn]){fv=s[mn].trackVars;fe=s[mn].trackEvents}}}if(fv)fv=','+fv+','+s.vl_l+','+s.vl_l2;if(fe){fe=','+fe+',';if"
		+"(fv)fv+=',events,'}if (s.events2)e=(e?',':'')+s.events2}for(i=0;i<l.length;i++){var k=l[i],v=s[k],b=k.substring(0,4),x=k.substring(4),n=parseInt(x),q=k;if(!v)if(k=='events'&&e){v=e;e=''}if(v&&(!fv|"
		+"|fv.indexOf(','+k+',')>=0)&&k!='linkName'&&k!='linkType'){if(k=='supplementalDataID')q='sdid';else if(k=='timestamp')q='ts';else if(k=='dynamicVariablePrefix')q='D';else if(k=='visitorID')q='vid';e"
		+"lse if(k=='marketingCloudVisitorID')q='mid';else if(k=='analyticsVisitorID')q='aid';else if(k=='audienceManagerLocationHint')q='aamlh';else if(k=='audienceManagerBlob')q='aamb';else if(k=='pageURL'"
		+"){q='g';if(v.length>255){s.pageURLRest=v.substring(255);v=v.substring(0,255);}}else if(k=='pageURLRest')q='-g';else if(k=='referrer'){q='r';v=s.fl(s.rf(v),255)}else if(k=='vmk'||k=='visitorMigratio"
		+"nKey')q='vmt';else if(k=='visitorMigrationServer'){q='vmf';if(s.ssl&&s.visitorMigrationServerSecure)v=''}else if(k=='visitorMigrationServerSecure'){q='vmf';if(!s.ssl&&s.visitorMigrationServer)v=''}"
		+"else if(k=='charSet'){q='ce';if(v.toUpperCase()=='AUTO')v='ISO8859-1';else if(s.em==2||s.em==3)v='UTF-8'}else if(k=='visitorNamespace')q='ns';else if(k=='cookieDomainPeriods')q='cdp';else if(k=='co"
		+"okieLifetime')q='cl';else if(k=='variableProvider')q='vvp';else if(k=='currencyCode')q='cc';else if(k=='channel')q='ch';else if(k=='transactionID')q='xact';else if(k=='campaign')q='v0';else if(k=='"
		+"resolution')q='s';else if(k=='colorDepth')q='c';else if(k=='javascriptVersion')q='j';else if(k=='javaEnabled')q='v';else if(k=='cookiesEnabled')q='k';else if(k=='browserWidth')q='bw';else if(k=='br"
		+"owserHeight')q='bh';else if(k=='connectionType')q='ct';else if(k=='homepage')q='hp';else if(k=='plugins')q='p';else if(k=='events'){if(e)v+=(v?',':'')+e;if(fe)v=s.fs(v,fe)}else if(k=='events2')v=''"
		+";else if(k=='contextData'){qs+=s.s2q('c',s[k],fv,k,0);v=''}else if(k=='lightProfileID')q='mtp';else if(k=='lightStoreForSeconds'){q='mtss';if(!s.lightProfileID)v=''}else if(k=='lightIncrementBy'){q"
		+"='mti';if(!s.lightProfileID)v=''}else if(k=='retrieveLightProfiles')q='mtsr';else if(k=='deleteLightProfiles')q='mtsd';else if(k=='retrieveLightData'){if(s.retrieveLightProfiles)qs+=s.s2q('mts',s[k"
		+"],fv,k,0);v=''}else if(s.num(x)){if(b=='prop')q='c'+n;else if(b=='eVar')q='v'+n;else if(b=='list')q='l'+n;else if(b=='hier'){q='h'+n;v=s.fl(v,255)}}if(v)qs+='&'+s.ape(q)+'='+(k.substring(0,3)!='pev"
		+"'?s.ape(v):v)}}return qs};s.ltdf=function(t,h){t=t?t.toLowerCase():'';h=h?h.toLowerCase():'';var qi=h.indexOf('?'),hi=h.indexOf('#');if(qi>=0){if(hi>=0&&hi<qi)qi=hi;}else qi=hi;h=qi>=0?h.substring("
		+"0,qi):h;if(t&&h.substring(h.length-(t.length+1))=='.'+t)return 1;return 0};s.ltef=function(t,h){t=t?t.toLowerCase():'';h=h?h.toLowerCase():'';if(t&&h.indexOf(t)>=0)return 1;return 0};s.lt=function("
		+"h){var s=this,lft=s.linkDownloadFileTypes,lef=s.linkExternalFilters,lif=s.linkInternalFilters;lif=lif?lif:s.wd.location.hostname;h=h.toLowerCase();if(s.trackDownloadLinks&&lft&&s.pt(lft,',','ltdf',"
		+"h))return 'd';if(s.trackExternalLinks&&h.indexOf('#')!=0&&h.indexOf('about:')!=0&&h.indexOf('javascript:')!=0&&(lef||lif)&&(!lef||s.pt(lef,',','ltef',h))&&(!lif||!s.pt(lif,',','ltef',h)))return 'e'"
		+";return ''};s.lc=new Function('e','var s=s_c_il['+s._in+'],b=s.eh(this,\"onclick\");s.lnk=this;s.t();s.lnk=0;if(b)return this[b](e);return true');s.bcr=function(){var s=this;if(s.bct&&s.bce)s.bct.d"
		+"ispatchEvent(s.bce);if(s.bcf){if(typeof(s.bcf)=='function')s.bcf();else if(s.bct&&s.bct.href)s.d.location=s.bct.href}s.bct=s.bce=s.bcf=0};s.bc=new Function('e','if(e&&e.s_fe)return;var s=s_c_il['+s"
		+"._in+'],f,tcf,t,n,nrs,a,h;if(s.d&&s.d.all&&s.d.all.cppXYctnr)return;if(!s.bbc)s.useForcedLinkTracking=0;else if(!s.useForcedLinkTracking){s.b.removeEventListener(\"click\",s.bc,true);s.bbc=s.useFor"
		+"cedLinkTracking=0;return}else s.b.removeEventListener(\"click\",s.bc,false);s.eo=e.srcElement?e.srcElement:e.target;nrs=s.nrs;s.t();s.eo=0;if(s.nrs>nrs&&s.useForcedLinkTracking&&e.target){a=e.targe"
		+"t;while(a&&a!=s.b&&a.tagName.toUpperCase()!=\"A\"&&a.tagName.toUpperCase()!=\"AREA\")a=a.parentNode;if(a){h=a.href;if(h.indexOf(\"#\")==0||h.indexOf(\"about:\")==0||h.indexOf(\"javascript:\")==0)h="
		+"0;t=a.target;if(e.target.dispatchEvent&&h&&(!t||t==\"_self\"||t==\"_top\"||t==\"_parent\"||(s.wd.name&&t==s.wd.name))){tcf=new Function(\"s\",\"var x;try{n=s.d.createEvent(\\\\\"MouseEvents\\\\\")}"
		+"catch(x){n=new MouseEvent}return n\");n=tcf(s);if(n){tcf=new Function(\"n\",\"e\",\"var x;try{n.initMouseEvent(\\\\\"click\\\\\",e.bubbles,e.cancelable,e.view,e.detail,e.screenX,e.screenY,e.clientX"
		+",e.clientY,e.ctrlKey,e.altKey,e.shiftKey,e.metaKey,e.button,e.relatedTarget)}catch(x){n=0}return n\");n=tcf(n,e);if(n){n.s_fe=1;e.stopPropagation();if (e.stopImmediatePropagation) {e.stopImmediateP"
		+"ropagation();}e.preventDefault();s.bct=e.target;s.bce=n}}}}}');s.oh=function(o){var s=this,l=s.wd.location,h=o.href?o.href:'',i,j,k,p;i=h.indexOf(':');j=h.indexOf('?');k=h.indexOf('/');if(h&&(i<0||"
		+"(j>=0&&i>j)||(k>=0&&i>k))){p=o.protocol&&o.protocol.length>1?o.protocol:(l.protocol?l.protocol:'');i=l.pathname.lastIndexOf('/');h=(p?p+'//':'')+(o.host?o.host:(l.host?l.host:''))+(h.substring(0,1)"
		+"!='/'?l.pathname.substring(0,i<0?0:i)+'/':'')+h}return h};s.ot=function(o){var t=o.tagName;if(o.tagUrn||(o.scopeName&&o.scopeName.toUpperCase()!='HTML'))return '';t=t&&t.toUpperCase?t.toUpperCase()"
		+":'';if(t=='SHAPE')t='';if(t){if((t=='INPUT'||t=='BUTTON')&&o.type&&o.type.toUpperCase)t=o.type.toUpperCase();else if(!t&&o.href)t='A';}return t};s.oid=function(o){var s=this,t=s.ot(o),p,c,n='',x=0;"
		+"if(t&&!o.s_oid){p=o.protocol;c=o.onclick;if(o.href&&(t=='A'||t=='AREA')&&(!c||!p||p.toLowerCase().indexOf('javascript')<0))n=s.oh(o);else if(c){n=s.rep(s.rep(s.rep(s.rep(''+c,\"\\r\",''),\"\\n\",''"
		+"),\"\\t\",''),' ','');x=2}else if(t=='INPUT'||t=='SUBMIT'){if(o.value)n=o.value;else if(o.innerText)n=o.innerText;else if(o.textContent)n=o.textContent;x=3}else if(o.src&&t=='IMAGE')n=o.src;if(n){o"
		+".s_oid=s.fl(n,100);o.s_oidt=x}}return o.s_oid};s.rqf=function(t,un){var s=this,e=t.indexOf('='),u=e>=0?t.substring(0,e):'',q=e>=0?s.epa(t.substring(e+1)):'';if(u&&q&&(','+u+',').indexOf(','+un+',')"
		+">=0){if(u!=s.un&&s.un.indexOf(',')>=0)q='&u='+u+q+'&u=0';return q}return ''};s.rq=function(un){if(!un)un=this.un;var s=this,c=un.indexOf(','),v=s.c_r('s_sq'),q='';if(c<0)return s.pt(v,'&','rqf',un)"
		+";return s.pt(un,',','rq',0)};s.sqp=function(t,a){var s=this,e=t.indexOf('='),q=e<0?'':s.epa(t.substring(e+1));s.sqq[q]='';if(e>=0)s.pt(t.substring(0,e),',','sqs',q);return 0};s.sqs=function(un,q){v"
		+"ar s=this;s.squ[un]=q;return 0};s.sq=function(q){var s=this,k='s_sq',v=s.c_r(k),x,c=0;s.sqq=new Object;s.squ=new Object;s.sqq[q]='';s.pt(v,'&','sqp',0);s.pt(s.un,',','sqs',q);v='';for(x in s.squ)if"
		+"(x&&(!Object||!Object.prototype||!Object.prototype[x]))s.sqq[s.squ[x]]+=(s.sqq[s.squ[x]]?',':'')+x;for(x in s.sqq)if(x&&(!Object||!Object.prototype||!Object.prototype[x])&&s.sqq[x]&&(x==q||c<2)){v+"
		+"=(v?'&':'')+s.sqq[x]+'='+s.ape(x);c++}return s.c_w(k,v,0)};s.wdl=new Function('e','var s=s_c_il['+s._in+'],r=true,b=s.eh(s.wd,\"onload\"),i,o,oc;if(b)r=this[b](e);for(i=0;i<s.d.links.length;i++){o="
		+"s.d.links[i];oc=o.onclick?\"\"+o.onclick:\"\";if((oc.indexOf(\"s_gs(\")<0||oc.indexOf(\".s_oc(\")>=0)&&oc.indexOf(\".tl(\")<0)s.eh(o,\"onclick\",0,s.lc);}return r');s.wds=function(){var s=this;if(s"
		+".apv>3&&(!s.isie||!s.ismac||s.apv>=5)){if(s.b&&s.b.attachEvent)s.b.attachEvent('onclick',s.bc);else if(s.b&&s.b.addEventListener){if(s.n&&((s.n.userAgent.indexOf('WebKit')>=0&&s.d.createEvent)||(s."
		+"n.userAgent.indexOf('Firefox/2')>=0&&s.wd.MouseEvent))){s.bbc=1;s.useForcedLinkTracking=1;s.b.addEventListener('click',s.bc,true)}s.b.addEventListener('click',s.bc,false)}else s.eh(s.wd,'onload',0,"
		+"s.wdl)}};s.vs=function(x){var s=this,v=s.visitorSampling,g=s.visitorSamplingGroup,k='s_vsn_'+s.un+(g?'_'+g:''),n=s.c_r(k),e=new Date,y=e.getYear();e.setYear(y+10+(y<1900?1900:0));if(v){v*=100;if(!n"
		+"){if(!s.c_w(k,x,e))return 0;n=x}if(n%10000>v)return 0}return 1};s.dyasmf=function(t,m){if(t&&m&&m.indexOf(t)>=0)return 1;return 0};s.dyasf=function(t,m){var s=this,i=t?t.indexOf('='):-1,n,x;if(i>=0"
		+"&&m){var n=t.substring(0,i),x=t.substring(i+1);if(s.pt(x,',','dyasmf',m))return n}return 0};s.uns=function(){var s=this,x=s.dynamicAccountSelection,l=s.dynamicAccountList,m=s.dynamicAccountMatch,n,"
		+"i;s.un=s.un.toLowerCase();if(x&&l){if(!m)m=s.wd.location.host;if(!m.toLowerCase)m=''+m;l=l.toLowerCase();m=m.toLowerCase();n=s.pt(l,';','dyasf',m);if(n)s.un=n}i=s.un.indexOf(',');s.fun=i<0?s.un:s.u"
		+"n.substring(0,i)};s.sa=function(un){var s=this;if(s.un&&s.mpc('sa',arguments))return;s.un=un;if(!s.oun)s.oun=un;else if((','+s.oun+',').indexOf(','+un+',')<0)s.oun+=','+un;s.uns()};s.m_i=function(n"
		+",a){var s=this,m,f=n.substring(0,1),r,l,i;if(!s.m_l)s.m_l=new Object;if(!s.m_nl)s.m_nl=new Array;m=s.m_l[n];if(!a&&m&&m._e&&!m._i)s.m_a(n);if(!m){m=new Object,m._c='s_m';m._in=s.wd.s_c_in;m._il=s._"
		+"il;m._il[m._in]=m;s.wd.s_c_in++;m.s=s;m._n=n;m._l=new Array('_c','_in','_il','_i','_e','_d','_dl','s','n','_r','_g','_g1','_t','_t1','_x','_x1','_rs','_rr','_l');s.m_l[n]=m;s.m_nl[s.m_nl.length]=n}"
		+"else if(m._r&&!m._m){r=m._r;r._m=m;l=m._l;for(i=0;i<l.length;i++)if(m[l[i]])r[l[i]]=m[l[i]];r._il[r._in]=r;m=s.m_l[n]=r}if(f==f.toUpperCase())s[n]=m;return m};s.m_a=new Function('n','g','e','if(!g)"
		+"g=\"m_\"+n;var s=s_c_il['+s._in+'],c=s[g+\"_c\"],m,x,f=0;if(s.mpc(\"m_a\",arguments))return;if(!c)c=s.wd[\"s_\"+g+\"_c\"];if(c&&s_d)s[g]=new Function(\"s\",s_ft(s_d(c)));x=s[g];if(!x)x=s.wd[\\'s_\\"
		+"'+g];if(!x)x=s.wd[g];m=s.m_i(n,1);if(x&&(!m._i||g!=\"m_\"+n)){m._i=f=1;if((\"\"+x).indexOf(\"function\")>=0)x(s);else s.m_m(\"x\",n,x,e)}m=s.m_i(n,1);if(m._dl)m._dl=m._d=0;s.dlt();return f');s.m_m="
		+"function(t,n,d,e){t='_'+t;var s=this,i,x,m,f='_'+t,r=0,u;if(s.m_l&&s.m_nl)for(i=0;i<s.m_nl.length;i++){x=s.m_nl[i];if(!n||x==n){m=s.m_i(x);u=m[t];if(u){if((''+u).indexOf('function')>=0){if(d&&e)u=m"
		+"[t](d,e);else if(d)u=m[t](d);else u=m[t]()}}if(u)r=1;u=m[t+1];if(u&&!m[f]){if((''+u).indexOf('function')>=0){if(d&&e)u=m[t+1](d,e);else if(d)u=m[t+1](d);else u=m[t+1]()}}m[f]=1;if(u)r=1}}return r};"
		+"s.m_ll=function(){var s=this,g=s.m_dl,i,o;if(g)for(i=0;i<g.length;i++){o=g[i];if(o)s.loadModule(o.n,o.u,o.d,o.l,o.e,1);g[i]=0}};s.loadModule=function(n,u,d,l,e,ln){var s=this,m=0,i,g,o=0,f1,f2,c=s."
		+"h?s.h:s.b,b,tcf;if(n){i=n.indexOf(':');if(i>=0){g=n.substring(i+1);n=n.substring(0,i)}else g=\"m_\"+n;m=s.m_i(n)}if((l||(n&&!s.m_a(n,g)))&&u&&s.d&&c&&s.d.createElement){if(d){m._d=1;m._dl=1}if(ln){"
		+"if(s.ssl)u=s.rep(u,'http:','https:');i='s_s:'+s._in+':'+n+':'+g;b='var s=s_c_il['+s._in+'],o=s.d.getElementById(\"'+i+'\");if(s&&o){if(!o.l&&s.wd.'+g+'){o.l=1;if(o.i)clearTimeout(o.i);o.i=0;s.m_a("
		+"\"'+n+'\",\"'+g+'\"'+(e?',\"'+e+'\"':'')+')}';f2=b+'o.c++;if(!s.maxDelay)s.maxDelay=250;if(!o.l&&o.c<(s.maxDelay*2)/100)o.i=setTimeout(o.f2,100)}';f1=new Function('e',b+'}');tcf=new Function('s','c"
		+"','i','u','f1','f2','var e,o=0;try{o=s.d.createElement(\"script\");if(o){o.type=\"text/javascript\";'+(n?'o.id=i;o.defer=true;o.onload=o.onreadystatechange=f1;o.f2=f2;o.l=0;':'')+'o.src=u;c.appendC"
		+"hild(o);'+(n?'o.c=0;o.i=setTimeout(f2,100)':'')+'}}catch(e){o=0}return o');o=tcf(s,c,i,u,f1,f2)}else{o=new Object;o.n=n+':'+g;o.u=u;o.d=d;o.l=l;o.e=e;g=s.m_dl;if(!g)g=s.m_dl=new Array;i=0;while(i<g"
		+".length&&g[i])i++;g[i]=o}}else if(n){m=s.m_i(n);m._e=1}return m};s.voa=function(vo,r){var s=this,l=s.va_g,i,k,v,x;for(i=0;i<l.length;i++){k=l[i];v=vo[k];if(v||vo['!'+k]){if(!r&&(k==\"contextData\"|"
		+"|k==\"retrieveLightData\")&&s[k])for(x in s[k])if(!v[x])v[x]=s[k][x];s[k]=v}}};s.vob=function(vo,onlySet){var s=this,l=s.va_g,i,k;for(i=0;i<l.length;i++){k=l[i];vo[k]=s[k];if(!onlySet&&!vo[k])vo['!"
		+"'+k]=1}};s.dlt=new Function('var s=s_c_il['+s._in+'],d=new Date,i,vo,f=0;if(s.dll)for(i=0;i<s.dll.length;i++){vo=s.dll[i];if(vo){if(!s.m_m(\"d\")||d.getTime()-vo._t>=s.maxDelay){s.dll[i]=0;s.t(vo)}"
		+"else f=1}}if(s.dli)clearTimeout(s.dli);s.dli=0;if(f){if(!s.dli)s.dli=setTimeout(s.dlt,s.maxDelay)}else s.dll=0');s.dl=function(vo){var s=this,d=new Date;if(!vo)vo=new Object;s.vob(vo);vo._t=d.getTi"
		+"me();if(!s.dll)s.dll=new Array;s.dll[s.dll.length]=vo;if(!s.maxDelay)s.maxDelay=250;s.dlt()};s._waitingForMarketingCloudVisitorID = false;s._doneWaitingForMarketingCloudVisitorID = false;s._marketi"
		+"ngCloudVisitorIDCallback=function(marketingCloudVisitorID) {var s=this;s.marketingCloudVisitorID = marketingCloudVisitorID;s._doneWaitingForMarketingCloudVisitorID = true;s._callbackWhenReadyToTrac"
		+"kCheck();};s._waitingForAnalyticsVisitorID = false;s._doneWaitingForAnalyticsVisitorID = false;s._analyticsVisitorIDCallback=function(analyticsVisitorID) {var s=this;s.analyticsVisitorID = analytic"
		+"sVisitorID;s._doneWaitingForAnalyticsVisitorID = true;s._callbackWhenReadyToTrackCheck();};s._waitingForAudienceManagerLocationHint = false;s._doneWaitingForAudienceManagerLocationHint = false;s._a"
		+"udienceManagerLocationHintCallback=function(audienceManagerLocationHint) {var s=this;s.audienceManagerLocationHint = audienceManagerLocationHint;s._doneWaitingForAudienceManagerLocationHint = true;"
		+"s._callbackWhenReadyToTrackCheck();};s._waitingForAudienceManagerBlob = false;s._doneWaitingForAudienceManagerBlob = false;s._audienceManagerBlobCallback=function(audienceManagerBlob) {var s=this;s"
		+".audienceManagerBlob = audienceManagerBlob;s._doneWaitingForAudienceManagerBlob = true;s._callbackWhenReadyToTrackCheck();};s.isReadyToTrack=function() {var s=this,readyToTrack = true,visitor = s.v"
		+"isitor;if ((visitor) && (visitor.isAllowed())) {if ((!s._waitingForMarketingCloudVisitorID) && (!s.marketingCloudVisitorID) && (visitor.getMarketingCloudVisitorID)) {s._waitingForMarketingCloudVisi"
		+"torID = true;s.marketingCloudVisitorID = visitor.getMarketingCloudVisitorID([s,s._marketingCloudVisitorIDCallback]);if (s.marketingCloudVisitorID) {s._doneWaitingForMarketingCloudVisitorID = true;}"
		+"}if ((!s._waitingForAnalyticsVisitorID) && (!s.analyticsVisitorID) && (visitor.getAnalyticsVisitorID)) {s._waitingForAnalyticsVisitorID = true;s.analyticsVisitorID = visitor.getAnalyticsVisitorID(["
		+"s,s._analyticsVisitorIDCallback]);if (s.analyticsVisitorID) {s._doneWaitingForAnalyticsVisitorID = true;}}if ((!s._waitingForAudienceManagerLocationHint) && (!s.audienceManagerLocationHint) && (vis"
		+"itor.getAudienceManagerLocationHint)) {s._waitingForAudienceManagerLocationHint = true;s.audienceManagerLocationHint = visitor.getAudienceManagerLocationHint([s,s._audienceManagerLocationHintCallba"
		+"ck]);if (s.audienceManagerLocationHint) {s._doneWaitingForAudienceManagerLocationHint = true;}}if ((!s._waitingForAudienceManagerBlob) && (!s.audienceManagerBlob) && (visitor.getAudienceManagerBlob"
		+")) {s._waitingForAudienceManagerBlob = true;s.audienceManagerBlob = visitor.getAudienceManagerBlob([s,s._audienceManagerBlobCallback]);if (s.audienceManagerBlob) {s._doneWaitingForAudienceManagerBl"
		+"ob = true;}}if (((s._waitingForMarketingCloudVisitorID)     && (!s._doneWaitingForMarketingCloudVisitorID)     && (!s.marketingCloudVisitorID)) ||((s._waitingForAnalyticsVisitorID)          && (!s."
		+"_doneWaitingForAnalyticsVisitorID)          && (!s.analyticsVisitorID)) ||((s._waitingForAudienceManagerLocationHint) && (!s._doneWaitingForAudienceManagerLocationHint) && (!s.audienceManagerLocati"
		+"onHint)) ||((s._waitingForAudienceManagerBlob)         && (!s._doneWaitingForAudienceManagerBlob)         && (!s.audienceManagerBlob))) {readyToTrack = false;}}return readyToTrack;};s._callbackWhen"
		+"ReadyToTrackQueue = null;s._callbackWhenReadyToTrackInterval = 0;s.callbackWhenReadyToTrack=function(callbackThis,callback,args) {var s=this,callbackInfo;callbackInfo = {};callbackInfo.callbackThis"
		+" = callbackThis;callbackInfo.callback     = callback;callbackInfo.args         = args;if (s._callbackWhenReadyToTrackQueue == null) {s._callbackWhenReadyToTrackQueue = [];}s._callbackWhenReadyToTra"
		+"ckQueue.push(callbackInfo);if (s._callbackWhenReadyToTrackInterval == 0) {s._callbackWhenReadyToTrackInterval = setInterval(s._callbackWhenReadyToTrackCheck,100);}};s._callbackWhenReadyToTrackCheck"
		+"=new Function('var s=s_c_il['+s._in+'],callbackNum,callbackInfo;if (s.isReadyToTrack()) {if (s._callbackWhenReadyToTrackInterval) {clearInterval(s._callbackWhenReadyToTrackInterval);s._callbackWhen"
		+"ReadyToTrackInterval = 0;}if (s._callbackWhenReadyToTrackQueue != null) {while (s._callbackWhenReadyToTrackQueue.length > 0) {callbackInfo = s._callbackWhenReadyToTrackQueue.shift();callbackInfo.ca"
		+"llback.apply(callbackInfo.callbackThis,callbackInfo.args);}}}');s._handleNotReadyToTrack=function(variableOverrides) {var s=this,args,varKey,variableOverridesCopy = null,setVariables = null;if (!s."
		+"isReadyToTrack()) {args = [];if (variableOverrides != null) {variableOverridesCopy = {};for (varKey in variableOverrides) {variableOverridesCopy[varKey] = variableOverrides[varKey];}}setVariables ="
		+" {};s.vob(setVariables,true);args.push(variableOverridesCopy);args.push(setVariables);s.callbackWhenReadyToTrack(s,s.track,args);return true;}return false;};s.gfid=function(){var s=this,d='01234567"
		+"89ABCDEF',k='s_fid',fid=s.c_r(k),h='',l='',i,j,m=8,n=4,e=new Date,y;if(!fid||fid.indexOf('-')<0){for(i=0;i<16;i++){j=Math.floor(Math.random()*m);h+=d.substring(j,j+1);j=Math.floor(Math.random()*n);"
		+"l+=d.substring(j,j+1);m=n=16}fid=h+'-'+l;}y=e.getYear();e.setYear(y+2+(y<1900?1900:0));if(!s.c_w(k,fid,e))fid=0;return fid};s.track=s.t=function(vo,setVariables){var s=this,notReadyToTrack,trk=1,tm"
		+"=new Date,sed=Math&&Math.random?Math.floor(Math.random()*10000000000000):tm.getTime(),sess='s'+Math.floor(tm.getTime()/10800000)%10+sed,y=tm.getYear(),vt=tm.getDate()+'/'+tm.getMonth()+'/'+(y<1900?"
		+"y+1900:y)+' '+tm.getHours()+':'+tm.getMinutes()+':'+tm.getSeconds()+' '+tm.getDay()+' '+tm.getTimezoneOffset(),tcf,tfs=s.gtfs(),ta=-1,q='',qs='',code='',vb=new Object;if ((!s.supplementalDataID) &&"
		+" (s.visitor) && (s.visitor.getSupplementalDataID)) {s.supplementalDataID = s.visitor.getSupplementalDataID(\"AppMeasurement:\" + s._in,(s.expectSupplementalData ? false : true));}if(s.mpc('t',argum"
		+"ents))return;s.gl(s.vl_g);s.uns();s.m_ll();notReadyToTrack = s._handleNotReadyToTrack(vo);if (!notReadyToTrack) {if (setVariables) {s.voa(setVariables);}if(!s.td){var tl=tfs.location,a,o,i,x='',c='"
		+"',v='',p='',bw='',bh='',j='1.0',k=s.c_w('s_cc','true',0)?'Y':'N',hp='',ct='',pn=0,ps;if(String&&String.prototype){j='1.1';if(j.match){j='1.2';if(tm.setUTCDate){j='1.3';if(s.isie&&s.ismac&&s.apv>=5)"
		+"j='1.4';if(pn.toPrecision){j='1.5';a=new Array;if(a.forEach){j='1.6';i=0;o=new Object;tcf=new Function('o','var e,i=0;try{i=new Iterator(o)}catch(e){}return i');i=tcf(o);if(i&&i.next){j='1.7';if(a."
		+"reduce){j='1.8';if(j.trim){j='1.8.1';if(Date.parse){j='1.8.2';if(Object.create)j='1.8.5'}}}}}}}}}if(s.apv>=4)x=screen.width+'x'+screen.height;if(s.isns||s.isopera){if(s.apv>=3){v=s.n.javaEnabled()?"
		+"'Y':'N';if(s.apv>=4){c=screen.pixelDepth;bw=s.wd.innerWidth;bh=s.wd.innerHeight}}s.pl=s.n.plugins}else if(s.isie){if(s.apv>=4){v=s.n.javaEnabled()?'Y':'N';c=screen.colorDepth;if(s.apv>=5){bw=s.d.do"
		+"cumentElement.offsetWidth;bh=s.d.documentElement.offsetHeight;if(!s.ismac&&s.b){tcf=new Function('s','tl','var e,hp=0;try{s.b.addBehavior(\"#default#homePage\");hp=s.b.isHomePage(tl)?\"Y\":\"N\"}ca"
		+"tch(e){}return hp');hp=tcf(s,tl);tcf=new Function('s','var e,ct=0;try{s.b.addBehavior(\"#default#clientCaps\");ct=s.b.connectionType}catch(e){}return ct');ct=tcf(s)}}}else r=''}if(s.pl)while(pn<s.p"
		+"l.length&&pn<30){ps=s.fl(s.pl[pn].name,100)+';';if(p.indexOf(ps)<0)p+=ps;pn++}s.resolution=x;s.colorDepth=c;s.javascriptVersion=j;s.javaEnabled=v;s.cookiesEnabled=k;s.browserWidth=bw;s.browserHeigh"
		+"t=bh;s.connectionType=ct;s.homepage=hp;s.plugins=p;s.td=1}if(vo){s.vob(vb);s.voa(vo)}if(!s.analyticsVisitorID&&!s.marketingCloudVisitorID)s.fid=s.gfid();if((vo&&vo._t)||!s.m_m('d')){if(s.usePlugins"
		+")s.doPlugins(s);if(!s.abort){var l=s.wd.location,r=tfs.document.referrer;if(!s.pageURL)s.pageURL=l.href?l.href:l;if(!s.referrer&&!s._1_referrer){s.referrer=r;s._1_referrer=1}s.m_m('g');if(s.lnk||s."
		+"eo){var o=s.eo?s.eo:s.lnk,p=s.pageName,w=1,t=s.ot(o),n=s.oid(o),x=o.s_oidt,h,l,i,oc;if(s.eo&&o==s.eo){while(o&&!n&&t!='BODY'){o=o.parentElement?o.parentElement:o.parentNode;if(o){t=s.ot(o);n=s.oid("
		+"o);x=o.s_oidt}}if(!n||t=='BODY')o='';if(o){oc=o.onclick?''+o.onclick:'';if((oc.indexOf('s_gs(')>=0&&oc.indexOf('.s_oc(')<0)||oc.indexOf('.tl(')>=0)o=0}}if(o){if(n)ta=o.target;h=s.oh(o);i=h.indexOf("
		+"'?');h=s.linkLeaveQueryString||i<0?h:h.substring(0,i);l=s.linkName;t=s.linkType?s.linkType.toLowerCase():s.lt(h);if(t&&(h||l)){s.pe='lnk_'+(t=='d'||t=='e'?t:'o');s.pev1=(h?s.ape(h):'');s.pev2=(l?s."
		+"ape(l):'')}else trk=0;if(s.trackInlineStats){if(!p){p=s.pageURL;w=0}t=s.ot(o);i=o.sourceIndex;if(o.dataset&&o.dataset.sObjectId){s.wd.s_objectID=o.dataset.sObjectId;}else if(o.getAttribute&&o.getAt"
		+"tribute('data-s-object-id')){s.wd.s_objectID=o.getAttribute('data-s-object-id');}else if(s.useForcedLinkTracking){s.wd.s_objectID='';oc=o.onclick?''+o.onclick:'';if(oc){var ocb=oc.indexOf('s_object"
		+"ID'),oce,ocq,ocx;if(ocb>=0){ocb+=10;while(ocb<oc.length&&(\"= \\t\\r\\n\").indexOf(oc.charAt(ocb))>=0)ocb++;if(ocb<oc.length){oce=ocb;ocq=ocx=0;while(oce<oc.length&&(oc.charAt(oce)!=';'||ocq)){if(o"
		+"cq){if(oc.charAt(oce)==ocq&&!ocx)ocq=0;else if(oc.charAt(oce)==\"\\\\\")ocx=!ocx;else ocx=0;}else{ocq=oc.charAt(oce);if(ocq!='\"'&&ocq!=\"'\")ocq=0}oce++;}oc=oc.substring(ocb,oce);if(oc){o.s_soid=n"
		+"ew Function('s','var e;try{s.wd.s_objectID='+oc+'}catch(e){}');o.s_soid(s)}}}}}if(s.gg('objectID')){n=s.gg('objectID');x=1;i=1}if(p&&n&&t)qs='&pid='+s.ape(s.fl(p,255))+(w?'&pidt='+w:'')+'&oid='+s.a"
		+"pe(s.fl(n,100))+(x?'&oidt='+x:'')+'&ot='+s.ape(t)+(i?'&oi='+i:'')}}else trk=0}if(trk||qs){s.sampled=s.vs(sed);if(trk){if(s.sampled)code=s.mr(sess,(vt?'&t='+s.ape(vt):'')+s.hav()+q+(qs?qs:s.rq()),0,"
		+"ta);qs='';s.m_m('t');if(s.p_r)s.p_r();s.referrer=s.lightProfileID=s.retrieveLightProfiles=s.deleteLightProfiles=''}s.sq(qs)}}}else s.dl(vo);if(vo)s.voa(vb,1);}s.abort=0;s.supplementalDataID=s.pageU"
		+"RLRest=s.lnk=s.eo=s.linkName=s.linkType=s.wd.s_objectID=s.ppu=s.pe=s.pev1=s.pev2=s.pev3='';if(s.pg)s.wd.s_lnk=s.wd.s_eo=s.wd.s_linkName=s.wd.s_linkType='';return code};s.trackLink=s.tl=function(o,t"
		+",n,vo,f){var s=this;s.lnk=o;s.linkType=t;s.linkName=n;if(f){s.bct=o;s.bcf=f}s.t(vo)};s.trackLight=function(p,ss,i,vo){var s=this;s.lightProfileID=p;s.lightStoreForSeconds=ss;s.lightIncrementBy=i;s."
		+"t(vo)};s.setTagContainer=function(n){var s=this,l=s.wd.s_c_il,i,t,x,y;s.tcn=n;if(l)for(i=0;i<l.length;i++){t=l[i];if(t&&t._c=='s_l'&&t.tagContainerName==n){s.voa(t);if(t.lmq)for(i=0;i<t.lmq.length;"
		+"i++){x=t.lmq[i];y='m_'+x.n;if(!s[y]&&!s[y+'_c']){s[y]=t[y];s[y+'_c']=t[y+'_c']}s.loadModule(x.n,x.u,x.d)}if(t.ml)for(x in t.ml)if(s[x]){y=s[x];x=t.ml[x];for(i in x)if(!Object.prototype[i]){if(typeo"
		+"f(x[i])!='function'||(''+x[i]).indexOf('s_c_il')<0)y[i]=x[i]}}if(t.mmq)for(i=0;i<t.mmq.length;i++){x=t.mmq[i];if(s[x.m]){y=s[x.m];if(y[x.f]&&typeof(y[x.f])=='function'){if(x.a)y[x.f].apply(y,x.a);e"
		+"lse y[x.f].apply(y)}}}if(t.tq)for(i=0;i<t.tq.length;i++)s.t(t.tq[i]);t.s=s;return}}};s.wd=window;s.ssl=(s.wd.location.protocol.toLowerCase().indexOf('https')>=0);s.d=document;s.b=s.d.body;if(s.d.ge"
		+"tElementsByTagName){s.h=s.d.getElementsByTagName('HEAD');if(s.h)s.h=s.h[0]}s.n=navigator;s.u=s.n.userAgent;s.ns6=s.u.indexOf('Netscape6/');var apn=s.n.appName,v=s.n.appVersion,ie=v.indexOf('MSIE ')"
		+",o=s.u.indexOf('Opera '),i;if(v.indexOf('Opera')>=0||o>0)apn='Opera';s.isie=(apn=='Microsoft Internet Explorer');s.isns=(apn=='Netscape');s.isopera=(apn=='Opera');s.ismac=(s.u.indexOf('Mac')>=0);if"
		+"(o>0)s.apv=parseFloat(s.u.substring(o+6));else if(ie>0){s.apv=parseInt(i=v.substring(ie+5));if(s.apv>3)s.apv=parseFloat(i)}else if(s.ns6>0)s.apv=parseFloat(s.u.substring(s.ns6+10));else s.apv=parse"
		+"Float(v);s.em=0;if(s.em.toPrecision)s.em=3;else if(String.fromCharCode){i=escape(String.fromCharCode(256)).toUpperCase();s.em=(i=='%C4%80'?2:(i=='%U0100'?1:0))}if(s.oun)s.sa(s.oun);s.sa(un);s.vl_l="
		+"'supplementalDataID,timestamp,dynamicVariablePrefix,visitorID,marketingCloudVisitorID,analyticsVisitorID,audienceManagerLocationHint,fid,vmk,visitorMigrationKey,visitorMigrationServer,visitorMigrat"
		+"ionServerSecure,ppu,charSet,visitorNamespace,cookieDomainPeriods,cookieLifetime,pageName,pageURL,referrer,contextData,currencyCode,lightProfileID,lightStoreForSeconds,lightIncrementBy,retrieveLight"
		+"Profiles,deleteLightProfiles,retrieveLightData';s.va_l=s.sp(s.vl_l,',');s.vl_mr=s.vl_m='timestamp,charSet,visitorNamespace,cookieDomainPeriods,cookieLifetime,contextData,lightProfileID,lightStoreFo"
		+"rSeconds,lightIncrementBy';s.vl_t=s.vl_l+',variableProvider,channel,server,pageType,transactionID,purchaseID,campaign,state,zip,events,events2,products,audienceManagerBlob,linkName,linkType';var n;"
		+"for(n=1;n<=75;n++){s.vl_t+=',prop'+n+',eVar'+n;s.vl_m+=',prop'+n+',eVar'+n}for(n=1;n<=5;n++)s.vl_t+=',hier'+n;for(n=1;n<=3;n++)s.vl_t+=',list'+n;s.va_m=s.sp(s.vl_m,',');s.vl_l2=',tnt,pe,pev1,pev2,p"
		+"ev3,resolution,colorDepth,javascriptVersion,javaEnabled,cookiesEnabled,browserWidth,browserHeight,connectionType,homepage,pageURLRest,plugins';s.vl_t+=s.vl_l2;s.va_t=s.sp(s.vl_t,',');s.vl_g=s.vl_t+"
		+"',trackingServer,trackingServerSecure,trackingServerBase,fpCookieDomainPeriods,disableBufferedRequests,mobile,visitorSampling,visitorSamplingGroup,dynamicAccountSelection,dynamicAccountList,dynamic"
		+"AccountMatch,trackDownloadLinks,trackExternalLinks,trackInlineStats,linkLeaveQueryString,linkDownloadFileTypes,linkExternalFilters,linkInternalFilters,linkTrackVars,linkTrackEvents,linkNames,lnk,eo"
		+",lightTrackVars,_1_referrer,un';s.va_g=s.sp(s.vl_g,',');s.pg=pg;s.gl(s.vl_g);s.contextData=new Object;s.retrieveLightData=new Object;if(!ss)s.wds();if(pg){s.wd.s_co=function(o){return o};s.wd.s_gs="
		+"function(un){s_gi(un,1,1).t()};s.wd.s_dc=function(un){s_gi(un,1).t()}}",
		w=window,l=w.s_c_il,n=navigator,u=n.userAgent,v=n.appVersion,e=v.indexOf('MSIE '),m=u.indexOf('Netscape6/'),a,i,j,x,s;if(un){un=un.toLowerCase();if(l)for(j=0;j<2;j++)for(i=0;i<l.length;i++){s=l[i];x=s._c;if((!x||x=='s_c'||(j>0&&x=='s_l'))&&(s.oun==un||(s.fs&&s.sa&&s.fs(s.oun,un)))){if(s.sa)s.sa(un);if(x=='s_c')return s}else s=0}}w.s_an='0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
		w.s_sp=new Function("x","d","var a=new Array,i=0,j;if(x){if(x.split)a=x.split(d);else if(!d)for(i=0;i<x.length;i++)a[a.length]=x.substring(i,i+1);else while(i>=0){j=x.indexOf(d,i);a[a.length]=x.subst"
		+"ring(i,j<0?x.length:j);i=j;if(i>=0)i+=d.length}}return a");
		w.s_jn=new Function("a","d","var x='',i,j=a.length;if(a&&j>0){x=a[0];if(j>1){if(a.join)x=a.join(d);else for(i=1;i<j;i++)x+=d+a[i]}}return x");
		w.s_rep=new Function("x","o","n","return s_jn(s_sp(x,o),n)");
		w.s_d=new Function("x","var t='`^@$#',l=s_an,l2=new Object,x2,d,b=0,k,i=x.lastIndexOf('~~'),j,v,w;if(i>0){d=x.substring(0,i);x=x.substring(i+2);l=s_sp(l,'');for(i=0;i<62;i++)l2[l[i]]=i;t=s_sp(t,'');d"
		+"=s_sp(d,'~');i=0;while(i<5){v=0;if(x.indexOf(t[i])>=0) {x2=s_sp(x,t[i]);for(j=1;j<x2.length;j++){k=x2[j].substring(0,1);w=t[i]+k;if(k!=' '){v=1;w=d[b+l2[k]]}x2[j]=w+x2[j].substring(1)}}if(v)x=s_jn("
		+"x2,'');else{w=t[i]+' ';if(x.indexOf(w)>=0)x=s_rep(x,w,t[i]);i++;b+=62}}}return x");
		w.s_fe=new Function("c","return s_rep(s_rep(s_rep(c,'\\\\','\\\\\\\\'),'\"','\\\\\"'),\"\\n\",\"\\\\n\")");
		w.s_fa=new Function("f","var s=f.indexOf('(')+1,e=f.indexOf(')'),a='',c;while(s>=0&&s<e){c=f.substring(s,s+1);if(c==',')a+='\",\"';else if((\"\\n\\r\\t \").indexOf(c)<0)a+=c;s++}return a?'\"'+a+'\"':"
		+"a");
		w.s_ft=new Function("c","c+='';var s,e,o,a,d,q,f,h,x;s=c.indexOf('=function(');while(s>=0){s++;d=1;q='';x=0;f=c.substring(s);a=s_fa(f);e=o=c.indexOf('{',s);e++;while(d>0){h=c.substring(e,e+1);if(q){i"
		+"f(h==q&&!x)q='';if(h=='\\\\')x=x?0:1;else x=0}else{if(h=='\"'||h==\"'\")q=h;if(h=='{')d++;if(h=='}')d--}if(d>0)e++}c=c.substring(0,s)+'new Function('+(a?a+',':'')+'\"'+s_fe(c.substring(o+1,e))+'\")"
		+"'+c.substring(e+1);s=c.indexOf('=function(')}return c;");
		c=s_d(c);if(e>0){a=parseInt(i=v.substring(e+5));if(a>3)a=parseFloat(i)}else if(m>0)a=parseFloat(u.substring(m+10));else a=parseFloat(v);if(a<5||v.indexOf('Opera')>=0||u.indexOf('Opera')>=0)c=s_ft(c);if(!s){s=new Object;if(!w.s_c_in){w.s_c_il=new Array;w.s_c_in=0}s._il=w.s_c_il;s._in=w.s_c_in;s._il[s._in]=s;w.s_c_in++;}s._c='s_c';(new Function("s","un","pg","ss",c))(s,un,pg,ss);return s}
		function s_giqf(){var w=window,q=w.s_giq,i,t,s;if(q)for(i=0;i<q.length;i++){t=q[i];s=s_gi(t.oun);s.sa(t.un);s.setTagContainer(t.tagContainerName)}w.s_giq=0}s_giqf()

