FSR.surveydefs = [{
    section: '4P_booking',
    name: 'browse',
    pin: 1,
    invite: {
        siteLogo: "Unknown_83_filename"/*tpa=http://www.starwoodhotels.com/common/js/foresee/v7/js/4P.gif*/,
        when: 'onentry'
    },
    pop: {
        when: 'later'
    },
    tracker: {
        url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_4P.htm',
        locales: [{
            locale: 'zh',
            url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_4P_zh.htm'
        }, {
            locale: 'jp',
            url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_4P_jp.htm'
        }]
    },
    criteria: {
        sp: 20,
        lf: 2
    },
    locales: [{
        locale: 'zh',
        sp: 10,
        lf: 2
    }, {
        locale: 'jp',
        sp: 10,
        lf: 2
    }],
    include: {
        urls: ['/fourpoints/reservation/booking/']
    }
},{
    section: '4P',
    name: 'browse',
    pin: 1,
    invite: {
        siteLogo: "Unknown_83_filename"/*tpa=http://www.starwoodhotels.com/common/js/foresee/v7/js/4P.gif*/,
        when: 'onentry'
    },
    pop: {
        when: 'later'
    },
    tracker: {
        url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_4P.htm',
        locales: [{
            locale: 'zh',
            url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_4P_zh.htm'
        }, {
            locale: 'jp',
            url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_4P_jp.htm'
        }]
    },
    criteria: {
        sp: 10,
        lf: 2
    },
    locales: [{
        locale: 'zh',
        sp: 5,
        lf: 2
    }, {
        locale: 'jp',
        sp: 5,
        lf: 2
    }],
    include: {
        urls: ['/fourpoints']
    }
}, {
    section: 'LC_booking',
    name: 'browse',
    pin: 1,
    invite: {
        siteLogo: "Unknown_83_filename"/*tpa=http://www.starwoodhotels.com/common/js/foresee/v7/js/LC.gif*/,
        when: 'onentry'
    },
    pop: {
        when: 'later'
    },
    tracker: {
        url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_LC.htm',
        locales: [{
            locale: 'zh',
            url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_LC_zh.htm'
        }, {
            locale: 'jp',
            url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_LC_jp.htm'
        }]
    },
    criteria: {
        sp: 20,
        lf: 2
    },
    locales: [{
        locale: 'zh',
        sp: 10,
        lf: 2
    }, {
        locale: 'jp',
        sp: 10,
        lf: 2
    }],
    include: {
        urls: ['/luxury/reservation/booking/']
    }
},{
    section: 'LC',
    name: 'browse',
    pin: 1,
    invite: {
        siteLogo: "Unknown_83_filename"/*tpa=http://www.starwoodhotels.com/common/js/foresee/v7/js/LC.gif*/,
        when: 'onentry'
    },
    pop: {
        when: 'later'
    },
    tracker: {
        url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_LC.htm',
        locales: [{
            locale: 'zh',
            url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_LC_zh.htm'
        }, {
            locale: 'jp',
            url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_LC_jp.htm'
        }]
    },
    criteria: {
        sp: 10,
        lf: 2
    },
    locales: [{
        locale: 'zh',
        sp: 5,
        lf: 2
    }, {
        locale: 'jp',
        sp: 5,
        lf: 2
    }],
    include: {
        urls: ['/luxury']
    }
}, {
    section: 'MD_booking',
    name: 'browse',
    pin: 1,
    invite: {
        siteLogo: "Unknown_83_filename"/*tpa=http://www.starwoodhotels.com/common/js/foresee/v7/js/MD.gif*/,
        when: 'onentry'
    },
    pop: {
        when: 'later'
    },
    tracker: {
        url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_MD.htm',
        locales: [{
            locale: 'zh',
            url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_MD_zh.htm'
        },{
            locale: 'jp',
            url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_MD_jp.htm'
        }]
    },
    criteria: {
        sp: 20,
        lf: 2
    },
    locales: [{
        locale: 'zh',
        sp: 10,
        lf: 2
    },{
        locale: 'jp',
        sp: 10,
        lf: 2
    }],
    include: {
        urls: ['/lemeridien/reservation/booking/']
    }
},{
    section: 'MD',
    name: 'browse',
    pin: 1,
    invite: {
        siteLogo: "Unknown_83_filename"/*tpa=http://www.starwoodhotels.com/common/js/foresee/v7/js/MD.gif*/,
        when: 'onentry'
    },
    pop: {
        when: 'later'
    },
    tracker: {
        url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_MD.htm',
        locales: [{
            locale: 'zh',
            url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_MD_zh.htm'
        },{
            locale: 'jp',
            url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_MD_jp.htm'
        }]
    },
    criteria: {
        sp: 10,
        lf: 2
    },
    locales: [{
        locale: 'zh',
        sp: 5,
        lf: 2
    },{
        locale: 'jp',
        sp: 5,
        lf: 2
    }],
    include: {
        urls: ['/lemeridien']
    }
}, {
    section: 'SI_booking',
    name: 'browse',
    pin: 1,
    invite: {
        siteLogo: "Unknown_83_filename"/*tpa=http://www.starwoodhotels.com/common/js/foresee/v7/js/SI.gif*/,
        when: 'onentry'
    },
    pop: {
        when: 'later'
    },
    tracker: {
        url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_SI.htm',
        locales: [{
            locale: 'zh',
            url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_SI_zh.htm'
        },{
            locale: 'jp',
            url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_SI_jp.htm'
        }]
    },
    criteria: {
        sp: 20,
        lf: 2
    },
    locales: [{
        locale: 'zh',
        sp: 10,
        lf: 2
    },{
        locale: 'jp',
        sp: 10,
        lf: 2
    }],
    include: {
        urls: ['/sheraton/reservation/booking/']
    }
},{
    section: 'SI',
    name: 'browse',
    pin: 1,
    invite: {
        siteLogo: "Unknown_83_filename"/*tpa=http://www.starwoodhotels.com/common/js/foresee/v7/js/SI.gif*/,
        when: 'onentry'
    },
    pop: {
        when: 'later'
    },
    tracker: {
        url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_SI.htm',
        locales: [{
            locale: 'zh',
            url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_SI_zh.htm'
        },{
            locale: 'jp',
            url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_SI_jp.htm'
        }]
    },
    criteria: {
        sp: 3,
        lf: 2
    },
    locales: [{
        locale: 'zh',
        sp: 5,
        lf: 2
    },{
        locale: 'jp',
        sp: 5,
        lf: 2
    }],
    include: {
        urls: ['/sheraton']
    }
}, {
    section: 'ST_booking',
    name: 'browse',
    pin: 1,
    invite: {
        siteLogo: "Unknown_83_filename"/*tpa=http://www.starwoodhotels.com/common/js/foresee/v7/js/ST.gif*/,
        when: 'onentry'
    },
    pop: {
        when: 'later'
    },
    tracker: {
        url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_ST.htm',
        locales: [{
            locale: 'zh',
            url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_ST_zh.htm'
        },{
            locale: 'jp',
            url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_ST_jp.htm'
        }]
    },
    criteria: {
        sp: 20,
        lf: 2
    },
    locales: [{
        locale: 'zh',
        sp: 10,
        lf: 2
    },{
        locale: 'jp',
        sp: 10,
        lf: 2
    }],
    include: {
        urls: ['/stregis/reservation/booking/']
    }
}, {
    section: 'ST',
    name: 'browse',
    pin: 1,
    invite: {
        siteLogo: "Unknown_83_filename"/*tpa=http://www.starwoodhotels.com/common/js/foresee/v7/js/ST.gif*/,
        when: 'onentry'
    },
    pop: {
        when: 'later'
    },
    tracker: {
        url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_ST.htm',
        locales: [{
            locale: 'zh',
            url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_ST_zh.htm'
        },{
            locale: 'jp',
            url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_ST_jp.htm'
        }]
    },
    criteria: {
        sp: 10,
        lf: 2
    },
    locales: [{
        locale: 'zh',
        sp: 5,
        lf: 2
    },{
        locale: 'jp',
        sp: 5,
        lf: 2
    }],
    include: {
        urls: ['/stregis']
    }
}, {
    section: 'WH_booking',
    name: 'browse',
    pin: 1,
    invite: {
        siteLogo: "Unknown_83_filename"/*tpa=http://www.starwoodhotels.com/common/js/foresee/v7/js/WH.gif*/,
        when: 'onentry'
    },
    pop: {
        when: 'later'
    },
    tracker: {
        url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_WH.htm',
        locales: [{
            locale: 'zh',
            url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_WH_zh.htm'
        },{
            locale: 'jp',
            url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_WH_jp.htm'
        }]
    },
    criteria: {
        sp: 14,
        lf: 2
    },
    locales: [{
        locale: 'zh',
        sp: 10,
        lf: 2
    },{
        locale: 'jp',
        sp: 10,
        lf: 2
    }],
    include: {
        urls: ['/whotels/reservation/booking/']
    }
}, {
    section: 'WH',
    name: 'browse',
    pin: 1,
    invite: {
        siteLogo: "Unknown_83_filename"/*tpa=http://www.starwoodhotels.com/common/js/foresee/v7/js/WH.gif*/,
        when: 'onentry'
    },
    pop: {
        when: 'later'
    },
    tracker: {
        url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_WH.htm',
        locales: [{
            locale: 'zh',
            url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_WH_zh.htm'
        },{
            locale: 'jp',
            url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_WH_jp.htm'
        }]
    },
    criteria: {
        sp: 7,
        lf: 2
    },
    locales: [{
        locale: 'zh',
        sp: 5,
        lf: 2
    },{
        locale: 'jp',
        sp: 5,
        lf: 2
    }],
    include: {
        urls: ['/whotels']
    }
}, {
    section: 'WI_booking',
    name: 'browse',
    pin: 1,
    invite: {
        siteLogo: "Unknown_83_filename"/*tpa=http://www.starwoodhotels.com/common/js/foresee/v7/js/WI.gif*/,
        when: 'onentry'
    },
    pop: {
        when: 'later'
    },
    tracker: {
        url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_WI.htm',
        locales: [{
            locale: 'zh',
            url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_WI_zh.htm'
        },{
            locale: 'jp',
            url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_WI_jp.htm'
        }]
    },
    criteria: {
        sp: 5,
        lf: 2
    },
    locales: [{
        locale: 'zh',
        sp: 10,
        lf: 2
    },{
        locale: 'jp',
        sp: 10,
        lf: 2
    }],
    include: {
        urls: ['/westin/reservation/booking/']
    }
}, {
    section: 'WI',
    name: 'browse',
    pin: 1,
    invite: {
        siteLogo: "Unknown_83_filename"/*tpa=http://www.starwoodhotels.com/common/js/foresee/v7/js/WI.gif*/,
        when: 'onentry'
    },
    pop: {
        when: 'later'
    },
    tracker: {
        url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_WI.htm',
        locales: [{
            locale: 'zh',
            url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_WI_zh.htm'
        },{
            locale: 'jp',
            url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_WI_jp.htm'
        }]
    },
    criteria: {
        sp: 2,
        lf: 2
    },
    locales: [{
        locale: 'zh',
        sp: 5,
        lf: 2
    },{
        locale: 'jp',
        sp: 5,
        lf: 2
    }],
    include: {
        urls: ['/westin']
    }
}, {
    section: 'AL_booking',
    name: 'browse',
    pin: 1,
    invite: {
        siteLogo: "Unknown_83_filename"/*tpa=http://www.starwoodhotels.com/common/js/foresee/v7/js/AL.gif*/,
        when: 'onentry'
    },
    pop: {
        when: 'later'
    },
    tracker: {
        url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_AL.htm',
        locales: [{
            locale: 'zh',
            url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_AL_zh.htm'
        },{
            locale: 'jp',
            url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_AL_jp.htm'
        }]
    },
    criteria: {
        sp: 20,
        lf: 2
    },
    locales: [{
        locale: 'zh',
        sp: 10,
        lf: 2
    },{
        locale: 'jp',
        sp: 10,
        lf: 2
    }],
    include: {
        urls: ['/alofthotels/reservation/booking/']
    }
}, {
    section: 'AL',
    name: 'browse',
    pin: 1,
    invite: {
        siteLogo: "Unknown_83_filename"/*tpa=http://www.starwoodhotels.com/common/js/foresee/v7/js/AL.gif*/,
        when: 'onentry'
    },
    pop: {
        when: 'later'
    },
    tracker: {
        url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_AL.htm',
        locales: [{
            locale: 'zh',
            url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_AL_zh.htm'
        },{
            locale: 'jp',
            url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_AL_jp.htm'
        }]
    },
    criteria: {
        sp: 10,
        lf: 2
    },
    locales: [{
        locale: 'zh',
        sp: 5,
        lf: 2
    },{
        locale: 'jp',
        sp: 5,
        lf: 2
    }],
    include: {
        urls: ['/alofthotels']
    }
}, {
    section: 'EL_booking',
    name: 'browse',
    pin: 1,
    invite: {
        siteLogo: "Unknown_83_filename"/*tpa=http://www.starwoodhotels.com/common/js/foresee/v7/js/EL.gif*/,
        when: 'onentry'
    },
    pop: {
        when: 'later'
    },
    tracker: {
        url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_EL.htm',
        locales: [{
            locale: 'zh',
            url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_EL_zh.htm'
        },{
            locale: 'jp',
            url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_EL_jp.htm'
        }]
    },
    criteria: {
        sp: 75,
        lf: 2
    },
    locales: [{
        locale: 'zh',
        sp: 10,
        lf: 2
    },{
        locale: 'jp',
        sp: 10,
        lf: 2
    }],
    include: {
        urls: ['/element/reservation/booking/']
    }
}, {
    section: 'EL',
    name: 'browse',
    pin: 1,
    invite: {
        siteLogo: "Unknown_83_filename"/*tpa=http://www.starwoodhotels.com/common/js/foresee/v7/js/EL.gif*/,
        when: 'onentry'
    },
    pop: {
        when: 'later'
    },
    tracker: {
        url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_EL.htm',
        locales: [{
            locale: 'zh',
            url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_EL_zh.htm'
        },{
            locale: 'jp',
            url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_EL_jp.htm'
        }]
    },
    criteria: {
        sp: 50,
        lf: 2
    },
    locales: [{
        locale: 'zh',
        sp: 5,
        lf: 2
    },{
        locale: 'jp',
        sp: 5,
        lf: 2
    }],
    include: {
        urls: ['/element']
    }
}, {
    section: 'SPG',
    name: 'browse',
    pin: 1,
    invite: {
        siteLogo: "Unknown_83_filename"/*tpa=http://www.starwoodhotels.com/common/js/foresee/v7/js/SPG.gif*/,
        when: 'onentry'
    },
    pop: {
        when: 'later'
    },
    tracker: {
        url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_SPG.htm',
        locales: [{
            locale: 'zh',
            url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_SPG_zh.htm'
        }, {
            locale: 'jp',
            url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_SPG_jp.htm'
        }]
    },
    criteria: {
        sp: 10,
        lf: 2
    },
    locales: [{
        locale: 'zh',
        sp: 5,
        lf: 2
    }, {
        locale: 'jp',
        sp: 5,
        lf: 2
    }],
    include: {
        urls: ['/preferredguest']
    }
}, {
    section: 'CORP',
    name: 'browse',
    pin: 1,
    invite: {
        siteLogo: "Unknown_83_filename"/*tpa=http://www.starwoodhotels.com/common/js/foresee/v7/js/CORP.gif*/,
        when: 'onentry'
    },
    pop: {
        when: 'later'
    },
    tracker: {
        url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_CORP.htm',
        locales: [{
            locale: 'zh',
            url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_CORP_zh.htm'
        },{
            locale: 'jp',
            url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker_CORP_jp.htm'
        }]
    },
    criteria: {
        sp: 0,
        lf: 2
    },
    locales: [{
        locale: 'zh',
        sp: 0,
        lf: 2
    },{
        locale: 'jp',
        sp: 0,
        lf: 2
    }],
    include: {
        urls: ['.']
    }
}];
FSR.properties = {
    repeatdays: 90,
    
    repeatoverride: false,
    
    altcookie: {},
    
    language: {
        locale: 'en',
        src: 'variable',
        type: 'client',
        name: 's.prop3',
        locales: [{
            match: 'zh_CN',
            locale: 'zh'
        }, {
            match: 'ja_JP',
            locale: 'jp'
        }]
    },
    
    exclude: {},
    
    zIndexPopup: 10000,
    
    ignoreWindowTopCheck: false,
    
    ipexclude: 'fsr$ip',
    
    mobileHeartbeat: {
        delay: 60, /*mobile on exit heartbeat delay seconds*/
        max: 3600 /*mobile on exit heartbeat max run time seconds*/
    },
    
    invite: {
    
        // For no site logo, comment this line:
        siteLogo: "Unknown_83_filename"/*tpa=http://www.starwoodhotels.com/common/js/foresee/v7/js/sitelogo.gif*/,
        
        //alt text fore site logo img
        siteLogoAlt: "",
        
        /* Desktop */
        dialogs: [[{
            reverseButtons: false,
            headline: "We'd welcome your feedback!",
            blurb: "Thank you for visiting our website. You have been selected to participate in a brief customer satisfaction survey to let us know how we can improve your experience.",
            noticeAboutSurvey: "The survey is designed to measure your entire experience, please look for it at the <u>conclusion</u> of your visit.",
            attribution: "This survey is conducted by an independent company ForeSee, on behalf of the site you are visiting.",
            closeInviteButtonText: "Click to close.",
            declineButton: "No, thanks",
            acceptButton: "Yes, I'll give feedback",
            error: "Error",
            warnLaunch: "this will launch a new window",
            
            locales: {
                "zh": {
                    headline: "我们欢迎您提出反馈意见！",
                    blurb: "谢谢您造访我们的网站。您已被选择参加一项简短的客户满意度调查，以便让我们了解如何改进您的体验。",
                    noticeAboutSurvey: "设计本项调查的目的是检测您的整个体验，请准备好在您访问结束时填写这份调查。",
                    attribution: "本项调查由独立公司 ForeSee 代表您造访的网站举办。",
                    closeInviteButtonText: "单击此处关闭。",
                    declineButton: "不参加，谢谢",
                    acceptButton: "是，我会提供反馈意见"
                },
                "jp": {
                    headline: "ご意見をお聞かせください！",
                    blurb: "弊社のウェブサイトをご覧いただき、ありがとうございます。このたび、お客様の体験を改善するプロジェクトの一環として、お客様の満足度に関するアンケートへのご案内を送信させていただきました。",
                    noticeAboutSurvey: "このアンケート調査はお客様の全体的な体験を測定することを目的としています。本サイトの閲覧終了時に<u>結果</u>をご覧ください。",
                    attribution: "このアンケート調査は、完全独立会社である ForeSee が、ご覧いただいているサイトを代表して実施します。",
                    closeInviteButtonText: "閉じる。",
                    declineButton: "お断りします",
                    acceptButton: "はい。調査に参加します"
                }
            }
        }]],
        
        exclude: {
            urls: ['/checkout'],
            referrers: [],
            userAgents: [],
            browsers: [],
            cookies: [],
            variables: []
        },
        include: {
            local: ['.']
        },
        
        delay: 0,
        timeout: 0,
        
        hideOnClick: false,
        
        hideCloseButton: false,
        
        css: 'Unknown_83_filename'/*tpa=http://www.starwoodhotels.com/common/js/foresee/v7/js/foresee-dhtml.css*/,
        
        hide: [],
        
        hideFlash: false,
        
        type: 'dhtml',
        /* desktop */
        // url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/invite.htm'
        /* mobile */
        url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/invite-mobile.htm',
        back: 'url'
    
        //SurveyMutex: 'SurveyMutex'
    },
    
    tracker: {
        width: '690',
        height: '450',
        timeout: 3,
        adjust: true,
        alert: {
            enabled: true,
            message: 'The survey is now available.',
            locales: [{
                locale: 'zh',
                message: '本项调查现在可供使用。'
            }, {
                locale: 'jp',
                message: 'アンケート調査をお受けになる準備ができました。'
            }]
        },
        url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/tracker.htm'
    },
    
    survey: {
        width: 690,
        height: 600
    },
    
    qualifier: {
        footer: '<div id=\"fsrcontainer\"><div style=\"float:left;width:80%;font-size:8pt;text-align:left;line-height:12px;\">This survey is conducted by an independent company ForeSee,<br>on behalf of the site you are visiting.</div><div style=\"float:right;font-size:8pt;\"><a target="_blank" title="Validate TRUSTe privacy certification" href="http://privacy-policy.truste.com/click-with-confidence/ctv/en/www.foreseeresults.com/seal_m"><img border=\"0\" src=\"{%baseHref%}truste.png\" alt=\"Validate TRUSTe Privacy Certification\"></a></div></div>',
        width: '690',
        height: '500',
        bgcolor: '#333',
        opacity: 0.7,
        x: 'center',
        y: 'center',
        delay: 0,
        buttons: {
            accept: 'Continue'
        },
        hideOnClick: false,
        css: 'Unknown_83_filename'/*tpa=http://www.starwoodhotels.com/common/js/foresee/v7/js/foresee-dhtml.css*/,
        url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/qualifying.htm'
    },
    
    cancel: {
        url: 'http://www.starwoodhotels.com/common/js/foresee/v7/js/cancel.htm',
        width: '690',
        height: '400'
    },
    
    pop: {
        what: 'survey',
        after: 'leaving-site',
        pu: false,
        tracker: true
    },
    
    meta: {
        referrer: true,
        terms: true,
        ref_url: true,
        url: true,
        url_params: false,
        user_agent: false,
        entry: false,
        entry_params: false
    },
    
    events: {
        enabled: true,
        id: true,
        codes: {
            purchase: 800,
            items: 801,
            dollars: 802,
            followup: 803,
            information: 804,
            content: 805
        },
        pd: 7,
        custom: {}
    },
    
    previous: false,
    
    analytics: {
        google_local: false,
        google_remote: false
    },
    
    cpps: {},
    
    mode: 'first-party'
};
