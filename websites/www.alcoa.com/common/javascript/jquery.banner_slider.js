var wait_please = 0;
var cycle_timer;
var nextObj;
var bannerWidth;
var baseArrowOpacity = ".5";
var hoverArrowOpacity = ".75";
var transitionSpeed = 500;
var bannerSpacer = 0;

$().ready(function () {
    $("#rotating_banner .rightArrow, #rotating_banner .leftArrow").css("opacity", baseArrowOpacity);

    $("#rotating_banner .leftArrow, #rotating_banner .rightArrow").mouseover(function () {
        $(this).css("opacity", hoverArrowOpacity);
    }).mouseout(function () {
        $(this).css("opacity", baseArrowOpacity);
    });

    $("#rotating_banner .leftArrow").click(function () {
        jump_banner(-1);
    });

    $("#rotating_banner .rightArrow").click(function () {
        jump_banner(1);
    });

    $("#rotating_nav li, #rotating_counter li").addClass("dummy").removeClass("dummy");

    $("#rotating_nav li").click(function () {
        rotate_banner($(this));
    });
    $("#rotating_counter li").click(function () {
        rotate_banner($(this));
    });
    bannerWidth = $("#rotating_images a img").eq(0).width();

    cycle_banners();
});

function jump_banner(dir_index) {
    var nextObjIndex = $("#rotating_counter li.active").index() + dir_index;
    if (nextObjIndex < 0) {
        nextObjIndex = $("#rotating_counter li").length - 1;
    } else if (nextObjIndex == $("#rotating_counter li").length) {
        nextObjIndex = 0;
    }
    rotate_banner($($("#rotating_counter li")[nextObjIndex]));
}

function rotate_banner(activeObj) {
    if (!activeObj.hasClass("active") && wait_please == 0) {
        window.clearTimeout(cycle_timer);
        wait_please = 1;
        var itemIndex = $("#rotating_counter li").index(activeObj);

        $("#rotating_counter li.active").removeClass("active", transitionSpeed);
        activeObj.addClass("active", transitionSpeed);

        $("#rotating_nav li.active").removeClass("active", transitionSpeed);
        $("#rotating_nav li").eq(itemIndex).addClass("active", transitionSpeed);

        $("#rotating_images").animate({ "left": (-(bannerWidth+bannerSpacer) * itemIndex) + "px" }, transitionSpeed, function () { wait_please = 0; cycle_banners(); });
    }
}

 function cycle_banners() {
     var nextObjIndex = $("#rotating_counter li.active").index() + 1;
     if (nextObjIndex == $("#rotating_counter li").length) {
        nextObjIndex = 0;
    }
    nextObj = $("#rotating_counter li")[nextObjIndex];
    cycle_timer = window.setTimeout("rotate_banner($(nextObj))", (rotating_duration[$("#rotating_counter li.active").index()] * 1000) - transitionSpeed);
}