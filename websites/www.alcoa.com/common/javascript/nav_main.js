var init_var, openWidget, jDisableFrame

init_var=true;

//global variables set elsewhere
var jLanguage = "";
var jCountry = "";

function colorCell(obj, textcolor, color, lbordercolor, rbordercolor, rbordersize) {
    if (!textcolor) {
        textcolor = '';
    }
    if (!color) {
        color = '';
    }
    if (!lbordercolor) {
        lbordercolor = color;
    }
    if (!rbordercolor) {
        rbordercolor = lbordercolor;
    }
    if (!rbordersize) {
        rbordersize = 0;
    }

    if (obj) {
        var x = "cell" + obj;
        x = document.getElementById(x);
        x.style.color = textcolor;
        x.style.backgroundColor = color;
        x.style.borderLeftColor = lbordercolor;
        x = "anchor" + obj;
        x = document.getElementById(x);
        if (x) x.style.color = textcolor;
    }
}

//ES - 12/11/2002 -- Added separate closeMenu() function
function closeMenu() {
	if (openWidget) {
		var x = "drop" + openWidget;
		x = document.getElementById(x);
		x.style.visibility = "hidden";
		
		openWidget = 0;
	}
}

function openMenuXML(obj) {
    if (!init_var) return false;

    if (openWidget) {
        closeMenu();
    }

    if (obj) {
        var cellObj = document.getElementById('cell' + obj);
        var openObj = document.getElementById('drop' + obj);
        var barObj = document.getElementById('nav_top_sections');
        var parentObj = openObj.parentNode;

        openObj.style.left = 0;
        var originalLeft = openObj.offsetLeft;
        var objLeft = cellObj.offsetLeft + barObj.offsetLeft - openObj.offsetLeft;

        if ((cellObj.style.borderLeftWidth == '0px') || (cellObj.style.borderLeftWidth == '0pt'))
            objLeft = objLeft - 1;

        if (objLeft + openObj.offsetWidth > barObj.offsetLeft + 730 - originalLeft)
            objLeft = barObj.offsetLeft + 730 - openObj.offsetWidth - originalLeft;

        openObj.style.left = objLeft + 'px';

        if (document.getElementById('parent_nav'))
            openObj.style.top = '49px';

        openObj.style.visibility = "visible";
        if (document.activeElement.id != '') {
            document.activeElement.blur();
        }
        openWidget = obj;
    }
}

function appendArrays() {
	if (location.href.indexOf('/greenland/')<=0)
	    breakFrames();
	//loadFrames();
	//init_var=true;
    addthis.init();
}

function popFlash(url, sizex, sizey, ver, bg) {
    var winX = sizex + 0;
    var winY = sizey + 35;
    win1 = window.open('', 'client', 'width=' + winX + ',height=' + winY + ',location=no,menubar=no,resizable=no,scrollbars=no,status=no,titlebar=no,toolbar=no,screenX=1,screenY=1,top=50,left=50');
    win1.location.href = 'http://www.alcoa.com/common/display/popframes.asp?swf=' + url + '&llang=' + jLanguage + '&fX=' + sizex + '&fY=' + sizey + '&ver=' + ver + '&bg=' + bg;
    win1.focus();
}

// TTT 5/21/02
// MB 3/20/03 - edited to use jLanguage
function popwindow(url, sizex, sizey) {
    win1 = window.open('', 'client', 'width=' + sizex + ',height=' + sizey + ',location=no,menubar=no,resizable=yes,scrollbars=yes,status=yes,titlebar=no,toolbar=no,screenX=1,screenY=1,top=50,left=50');
    win1.location.href = url + '&llang=' + jLanguage;
    win1.focus();
}

// MB 12/27/07 - created to email a link to the current page
function printPage() {
    window.print();
    dcsMultiTrack('DCS.dcsuri', 'http://www.alcoa.com/common/coding/print_friendly_page.asp', 'DCSext.print_url', location.href);
}

var newwin;
function glossary(glossterm) {
	var winvar = "/global/" + jLanguage + "/about_alcoa/glossary_frames.asp?" + glossterm;
	var xpos = 0;
	var ypos = 0;
	xpos += self.screenLeft+5;
	ypos += self.screenTop+5;

	if (xpos<0){
		xpos=0;
	}
	if (ypos<0){
		ypos=0;
	}


	if (newwin) {
		if (!newwin.closed) {
		newwin.focus();
		newwin.bodyframe.document.location.hash=glossterm;
//		newwin.document.title="Glossary"
		}
		else {
		newwin = window.open (winvar,'glosswin',config='height=200,width=340,toolbar=no,menubar=no,scrollbars=yes,resizable=no,location=no,directories=no,status=no');
		newwin.moveTo(xpos, ypos);
		newwin.focus();
		}
	}
	else {
		newwin = window.open (winvar,'glosswin',config='height=200,width=340,toolbar=no,menubar=no,scrollbars=yes,resizable=no,location=no,directories=no,status=no');
		newwin.moveTo(xpos, ypos);
		newwin.focus();
	}
}


function getVar(sVar) {
	var loc = top.location;
	loc = loc.toString()
	var c1 = 0;
	var c2 = 15;

	while (c2>c1+3) {
		c0 = c1
		c1 = loc.indexOf("/", c2);
		c2 = loc.indexOf("/", c1+1);
	}

	if (sVar == 'Lang') {
		return loc.substr(c1+1, c2-c1-1);
	}
	else if (sVar =='Country') {
		return loc.substr(c0+1, c1-c0-1);
	}
}


function imgChange() {
  if (document.images) {
    for (var i=0; i<imgChange.arguments.length; i+=2) {
      document[imgChange.arguments[i]].src = eval(imgChange.arguments[i+1] + ".src");
    }
  }
}

function breakFrames() {
	if (top.frames) {
		if ((top.location != location) && !(top.frameSaver)) {
			top.location.href = location.href;
		}
	}
}

function loadFrames() {
	var strHttp = document.location.toString();
	strHttp = strHttp.slice(0, strHttp.indexOf(":"));
	if (strHttp == "https") {
		breakFrames();
	} else
	{
		if (!jDisableFrame) {
			if (top.frames) {
				if (top.location != location) {
					var frameName = top.frames[0].name;
					var frameUrl = top.frames[0].location.toString();
					frameUrl = frameUrl.slice(frameUrl.indexOf("http://www.alcoa.com/global/en/survey_close.asp"), frameUrl.length);
					if ((frameName != "surveyFrame") || (frameUrl != "http://www.alcoa.com/global/en/survey_close.asp")) {
						var strUrl = top.frames[1].location.toString();
						strUrl = strUrl.slice(strUrl.indexOf("/", 10), strUrl.length);
						strUrl = strUrl.replace(/&/g, "***")
						strUrl = "/index.asp?page=" + strUrl;
						top.location.href = strUrl;
					}
				} else {
					var strUrl = document.location.toString();
					strUrl = strUrl.slice(strUrl.indexOf("/", 10), strUrl.length);
					strUrl = strUrl.replace(/&/g, "***")
					strUrl = "/index.asp?page=" + strUrl;
					top.location.href = strUrl;
				}
			}
		}
	}
}

function switchVideo(off, on) {
    document.getElementById(off).style.display = 'none';
    document.getElementById(on).style.display = 'block';
    document.getElementById(off).innerHTML = '';
}