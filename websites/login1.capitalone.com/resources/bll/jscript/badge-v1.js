// Added for eMFA RSA Device Information Capture - START
var remoteAddress1 = '192.168.0.1';
var resourceCount = 0;
var setDFP;
var setSignIn;
var getPlatform;
var DFPInterval = 4200;
var ConvArray = [];
var neiLoaded = 0;
// Added for eMFA RSA Device Information Capture - STOP

/**
 * Cookie plugin
 *
 * Copyright (c) 2006 Klaus Hartl (stilbuero.de)
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 */
jQuery.cookie=function(a,b,c){if(typeof b!='undefined'){c=c||{};if(b===null){b='';c.expires=-1}var d='';if(c.expires&&(typeof c.expires=='number'||c.expires.toUTCString)){var e;if(typeof c.expires=='number'){e=new Date();e.setTime(e.getTime()+(c.expires*24*60*60*1000))}else{e=c.expires}d='; expires='+e.toUTCString()}var f=c.path?'; path='+c.path:'';var g=c.domain?'; domain='+c.domain:'';var h=c.secure?'; secure':'';document.cookie=[a,'=',encodeURIComponent(b),d,f,g,h].join('');return true}else{var j=null;if(document.cookie&&document.cookie!=''){var k=document.cookie.split(';');for(var i=0;i<k.length;i++){var l=jQuery.trim(k[i]);if(l.substring(0,a.length+1)==(a+'=')){j=decodeURIComponent(l.substring(a.length+1));break}}}return j}};

/*
 * In-Field Label jQuery Plugin
 * http://fuelyourcoding.com/scripts/infield.html
 *
 * Copyright (c) 2009 Doug Neiner
 * Dual licensed under the MIT and GPL licenses.
 * Uses the same license as jQuery, see:
 * http://docs.jquery.com/License
 *
 * @version 0.1
 */
//(function(e){e.InFieldLabels=function(t,n,r){var i=this;i.$label=e(t);i.label=t;i.$field=e(n);i.field=n;i.$label.data("InFieldLabels",i);i.showing=true;i.init=function(){var t;i.options=e.extend({},e.InFieldLabels.defaultOptions,r);if(i.options.className){i.$label.addClass(i.options.className)}setTimeout(function(){if(i.$field.val()!==""){i.$label.hide();i.showing=false}else{i.$label.show();i.showing=true}},200);i.$field.focus(function(){i.fadeOnFocus()}).blur(function(){i.checkForEmpty(true)}).bind("keydown.infieldlabel",function(e){i.hideOnChange(e)}).bind("paste",function(){i.setOpacity(0)}).change(function(){i.checkForEmpty()}).bind("onPropertyChange",function(){i.checkForEmpty()}).bind("keyup.infieldlabel",function(){i.checkForEmpty()});if(i.options.pollDuration>0){t=setInterval(function(){if(i.$field.val()!==""){i.$label.hide();i.showing=false;clearInterval(t)}},i.options.pollDuration)}};i.fadeOnFocus=function(){if(i.showing){i.setOpacity(i.options.fadeOpacity)}};i.setOpacity=function(e){i.$label.stop().animate({opacity:e},i.options.fadeDuration,function(){if(e===0){i.$label.hide()}});i.showing=e>0};i.checkForEmpty=function(e){if(i.$field.val()===""){i.prepForShow();i.setOpacity(e?1:i.options.fadeOpacity)}else{i.setOpacity(0)}};i.prepForShow=function(){if(!i.showing){i.$label.css({opacity:0}).show();i.$field.bind("keydown.infieldlabel",function(e){i.hideOnChange(e)})}};i.hideOnChange=function(e){if(e.keyCode===16||e.keyCode===9){return}if(i.showing){i.$label.hide();i.showing=false}i.$field.unbind("keydown.infieldlabel")};i.init()};e.InFieldLabels.defaultOptions={fadeOpacity:.5,fadeDuration:300,pollDuration:0,enabledInputTypes:["text","search","tel","url","email","password","number","textarea"],className:false};e.fn.inFieldLabels=function(t){var n=t&&t.enabledInputTypes||e.InFieldLabels.defaultOptions.enabledInputTypes;return this.each(function(){var r=e(this).attr("for"),i,s;if(!r){return}i=document.getElementById(r);if(!i){return}s=e.inArray(i.type,n);if(s===-1&&i.nodeName!=="TEXTAREA"){return}new e.InFieldLabels(this,i,t)})}})(jQuery)


$('#badge-region, #sub-auth-coaf-ask, #sub-auth-auto-ask-footer input, #sub-auth-auto-ask-footer .auth-help').hide();

var badgeQuery = [], hash;
var q = document.URL.split('?')[1];
if(q != undefined)
{
    q = q.split('&');
    for(var i = 0; i < q.length; i++){
        hash = q[i].split('=');
        badgeQuery.push(hash[1]);
        badgeQuery[hash[0]] = hash[1];
    }
}

if (!Array.prototype.indexOf)
{
    Array.prototype.indexOf = function(obj, start)
    {
        for (var i = (start || 0), j = this.length; i < j; i++){if (this[i] === obj) { return i; }}
        return -1;
    }
}

if ($.browser.msie)
{
    $(function()
    {
        $('.log-in-badge').keydown(function(e)
        {
            if (e.keyCode == 13 && $(document.activeElement).hasClass('nosubmit') == false && $(document.activeElement).hasClass('active-account') == false && ($(document.activeElement).val() != 'auth-more-products' && $(document.activeElement).val() != 'auth-treasury-optimizer'))
            {
                $('#account-log-in').submit();
                return false;
            }
        });
    });
}

if (typeof pltfrmCode === 'undefined') var pltfrmCode = '';

switch (pltfrmCode)
{
    case "olbr":
        isBadgeSelectNoPersist = true;

        if ($.browser.msie && (document.documentMode <= 8 || document.documentMode === undefined))
        {
            $('#auth-banking').html('');
            $('#olbr-content').children().appendTo('#auth-banking');
            if (document.documentMode == 7)
            {
                $('#auth-banking .auth-welcome').css('margin-top','0px');
            }
        }
        else
        {
            $('#auth-banking').html($('#olbr-content').html());
        }

        $("label").inFieldLabels();
            $('#welcome-360, .auth-welcome img').unbind();
        $.cookie("PLTFRM_IND","OLB", badgeCookieSpec);
        break;
    default:
        $.cookie("PLTFRM_IND","WWW", badgeCookieSpec);
}

if (typeof isBadgeSelectNoPersist == "undefined") var isBadgeSelectNoPersist = false;

var Cof = Cof || {};
Cof.Login = function()
{
    var coafFlag;
    var currentAuth;
    var remAccounts = {};
    var submitForm = $('#login-go');
    var forceDefault = false;
    var remVal = '';
    var acType = '';
    var arrBadgeConf = new Array();
    var postAuthErrorSet = false;
    
    $('.rem-checkbox:visible').click(function()
    {
        if ($('.rem-checkbox:visible:checked').val() == undefined)
        {
            $('.user-id:visible').val('').blur();
            var dataId = $('.rem-checkbox:visible').attr('data-id');
            $.cookie('ISSO_RM_VAL_' + dataId, null, { expires: -1, domain: '.capitalone.com', path: '/' } );
            $.cookie('ISSO_RM', null, { expires: -1, domain: '.capitalone.com', path: '/' } );
        }
        else
        {
            $.cookie('ISSO_RM', 'true', badgeCookieSpec );
        }
    });

    
    $('#us-credit-cards-uid, #ca-credit-cards-uid').keydown(function(e)
    {
        var dataId = $('.rem-checkbox:visible').attr('data-id');
        var remUser = $.cookie('ISSO_RM_VAL_' + dataId);
        var spKeys = [192,54,189,190,8,46];
        if ($('.rem-checkbox:visible:checked').val() != undefined && remUser != undefined && (e.keyCode != 16 && e.keyCode != 9))
        {
            if (hasRemSet())
            {
                var char = String.fromCharCode(e.keyCode);
                if (/[a-zA-Z0-9]/.test(char) == true || spKeys.indexOf(e.keyCode) != -1)
                {
                    $('.user-id:visible').val('');
                    $('.rem-checkbox:visible').removeAttr('checked');
                    $.cookie('ISSO_RM_VAL_' + dataId, null, badgeCookieSpec );
                }
                else
                {
                    e.preventDefault();
                }
            }
        }
    });

    $('#default-btn, #default-user-forgot').click(function()
    {
        if (currentAuth == 'auth-default')
        {
            $('.account-types').next().show();
            $('.select-account').addClass('error-shadow');
            $('.active-account').focus();
            return false;
        }
    });

    $('#default-uid').keydown(function(e)
    {
        if (e.keyCode == 13) {
            $('.account-types').next().show();
            $('.select-account').addClass('error-shadow');
            $('active-account').focus();
            return false;
        }
    });

    $('#welcome-360, .auth-welcome img').click(function(e)
    {
        setAuth('auth-banking');
        setDropdown('auth-banking');
        $('.auth-error').hide();

    });

    $('html').click(function(){
       $('.forgot-collision-popup').hide();
    });

    $('#forgot-collision-id').click(function()
    {
        $('.forgot-collision-popup').show();
        return false;
    });

    $('ul.account-types').hide();
    $('.auth:not(:first)').hide();
    $('.sub-auth').hide();

    $('.log-in-focus a').click(function(e)
    {
        $('.log-in-overlay').show();
        $('.log-in-badge').addClass('hasFocus');
        $('.active-account').focus();
        e.preventDefault();
    });

    //dropdown behavior
    $('.active-account').click(function(e)
    {
        $('.account-types li').removeClass('sfhover');
        //chrome
        $('.account-types').css('height','auto');
        if (ieFix == true){$('.select-account').css('z-index','1000');}
        $('.account-types').next().hide();
        $('.select-account').removeClass('error-shadow');
        if ($('ul.account-types').css('display') === "none"){
            $('ul.account-types').slideDown("fast");
        } else {
            $('ul.account-types').slideUp("fast");
        }
        e.preventDefault();
    }).keydown(function(e){
        $('.account-types').next().hide();
        $('.select-account').removeClass('error-shadow');
        var keyCode = e.which || e.keyCode;
        if (String($(this)) == String($(document.activeElement)) && keyCode === 40) { //Down Arrow
            $('ul.account-types li').removeClass('sfhover');
            $('ul.account-types').slideDown("fast");
            $('ul.account-types input:first').focus().parent().addClass('sfhover');
            return false;
        }
    });

    //input label behavior
    $('.account-types label').click(function(e)
    {
        Cof.Login.newActiveAccount = $(this).html();
        Cof.Login.activeAuth = $(this).prev('input').val();
        if (Cof.Login.activeAuth == 'auth-more-products')
        {
            expandDropdown();
            $('.account-types').focus();
        }
        else
        {
            $('.active-account').html(Cof.Login.newActiveAccount);
            setAuth(Cof.Login.activeAuth);

            $('.account-types').slideUp("fast", function()
            {
                if (hasRemSet())
                {
                    $('.password:visible').focus();
                }
                else
                {
                    $('.user-id:visible').val('').focus();
                }

            });

        }
    });

    var hasRemSet = function()
    {
        var dataId = $('.rem-checkbox:visible').attr('data-id');
        var remUser = $.cookie('ISSO_RM_VAL_' + dataId);
        if ($('.rem-checkbox:visible:checked').val() != undefined && remUser != undefined ){return true;}else{return false;}
    };

    //dropdown hover behavior
    $('.account-types li').mouseover(function()
    {
        $('ul.account-types li').removeClass('sfhover');
        $(this).addClass('sfhover');
    }).mouseout(function()
    {
        $(this).removeClass('sfhover');
    });

    //dropdown keyboard behavior
    $('.account-types').keydown(function(e)
    {
        $('ul.account-types li').removeClass('sfhover');
        //chrome
        $('.account-types').css('height','auto');
        Cof.Login.keyCode = e.which || e.keyCode;
        if (Cof.Login.keyCode === 40 || Cof.Login.keyCode === 39)
        { //Down Arrow or Right Arrow
            Cof.Login.isFocused = $(document.activeElement);
            if (Cof.Login.isFocused.val() != 'auth-more-products')
            {
                Cof.Login.isFocused.parent().next().addClass('sfhover').children('a, input').focus();
            }
            e.preventDefault();
        }
        else if (Cof.Login.keyCode === 38 || Cof.Login.keyCode === 37)
        { //Up Arrow or Left Arrow
            Cof.Login.isFocused = $(document.activeElement);
            if (Cof.Login.isFocused.val() == 'auth-credit-cards')
            {
                $('.active-account').focus();
                $('.account-types').slideUp("fast");
            }
            else
            {
                Cof.Login.isFocused.parent().removeClass('sfhover').prev().addClass('sfhover').children('a, input').focus();
            }
            e.preventDefault();
        }
        else if (Cof.Login.keyCode === 13 || Cof.Login.keyCode === 9 || Cof.Login.keyCode === 32)
        { //Tab or Enter or Space
            if ($(document.activeElement).is('[href]'))
            {
                return true;
            }
            else
            {
                Cof.Login.newActiveAccount = $(document.activeElement).next('label').html();
                Cof.Login.activeAuth = $(document.activeElement).val();
                if (Cof.Login.activeAuth == 'auth-more-products')
                {
                    $('#rbMoreProducts').parent().remove();
                    $('.account-types li').removeClass('off');
                    $('#rbTreasury').parent().addClass('sfhover').children('a, input').focus();
                }
                else
                {
                    $('.active-account').html(Cof.Login.newActiveAccount);
                    setAuth(Cof.Login.activeAuth);
                    $('.account-types').slideUp("fast");
                    $(document.activeElement).click();
                    $('.active-account').focus();
                }
                e.preventDefault();
            }
        }
        else if (Cof.Login.keyCode === 27)
        { //Escape
            $('.active-account').focus();
            $('.account-types').slideUp("fast");
        }
    });

    //region flag behavior
    $('.region-select input').click(function(e)
    {
        if ($(this).attr('checked'))
        {
            $('.region-select label.active').removeClass('active');
            var currentAuth = $(this).attr('class');
            setAuth(currentAuth, true);
            $(this).parent().find('label').addClass('active');
        }
    }).focus(function() {
        $(this).parent().addClass('hasFocus');
    }).blur(function() {
        $(this).parent().removeClass('hasFocus');
    }).keyup(function(e) {
        Cof.Login.keyCode = e.which || e.keyCode;
        if (Cof.Login.keyCode === 9)
        { //Tab
            $('.region-select').addClass('hasFocus');
        }
    }).keydown(function(e) {
        Cof.Login.keyCode = e.which || e.keyCode;
        if (Cof.Login.keyCode === 9)
        { //Tab
            $('.region-select').removeClass('hasFocus');
        }
    });

    //input reset dropdown change
    $('input[name=rbAccountType],.region-select input').change(function()
    {
        if ($('#default-uid').val() != '')
        {
            $('.log-in-badge input').removeAttr('aria-describedby');
            $('.user-id:visible').val($('#default-uid').val()).blur();
            $('#default-uid').val('');
        }
    });

    $('.user-id,.password').blur(function()
    {
        $(this).next().not('.post-authentication').hide();
        $('.user-id,.password').removeClass('error-shadow');
    });

    $('.has-rem').click(function()
    {
        if (hasRemSet())
        {
            $(this).select();
        }
    }).mouseup(function(e){
        if (hasRemSet())
        {
            e.preventDefault();
        }
    });

    //homeloans radio behavior
    $('.hl-type input').change(function()
    {
        $('.log-in-badge div.auth  fieldset.hl-type legend').hide();
        var currentHL = $(document.activeElement).attr('id');
        Cof.Login.activeHlAuth = $(this).val();
        setAuth(Cof.Login.activeHlAuth, true);
        if ($.browser.msie) $('#' + currentHL).focus();
    });

    $('a, input').focus(function()
    {
        var focused = $(this);
        if(!focused.parents().hasClass('select-account')){ $('ul.account-types').slideUp("fast");}
        if(!focused.parents().hasClass('log-in-badge'))
        {
            $('.log-in-overlay').hide();
            $('.log-in-badge').removeClass('hasFocus');
        }
    });

    $(document).click(function(e)
    {
        var clicked = $(e.target);
        if(!clicked.parents().hasClass('select-account') && !clicked.hasClass('more-products') ){ $('ul.account-types').slideUp("fast"); }
        if (!clicked.parents().hasClass('log-in-focus'))
        {
            if($('.log-in-badge').hasClass('hasFocus') && !clicked.parents().hasClass('log-in-badge'))
            {
                $('.log-in-overlay').hide();
                $('.log-in-badge').removeClass('hasFocus');
            }
        }
    });

    $('.log-in-badge').delegate("#close-post-auth", 'click', function(){clearPostAuthError();});

    var getParentAuth = function(auth)
    {
        var authTarget = $('#' + auth);
        if ($(authTarget).hasClass('sub-auth') && $(authTarget).parent().attr('id') != 'account-log-in')
        {
                return $(authTarget).parent();
        }
        else
        {
            false;
        }
    }

    var configure = function()
    {
        var conf = $.cookie('ISSO_MODE');
        $.cookie('ISSO_MODE', null, { expires: -1, domain: '.capitalone.com', path: '/' } );
        if (conf == null)
        {
            conf = badgeQuery['badge'];
        }

        if (typeof conf  != 'undefined' && location.hash.replace("#","").indexOf('cof') == -1)
        {
               arrBadgeConf[0] = parseInt(conf.substr(0,4),16);
            arrBadgeConf[1] = parseInt(conf.substr(4,4),16);
              arrBadgeConf[2] = parseInt(conf.substr(8,4),16);
              if (badgeQuery['badge'])
              {
                  window.location = window.location + '#cof';
              }
        }

        //misc
        if (typeof arrBadgeConf[2] != 'undefined')
        {
            switch(arrBadgeConf[2])
            {
                case 1:
                    isBadgeSelectNoPersist = true;
                    break;
            }
        }
    }

    var setInitialAuth = function()
    {
        configure();
        var lob;
        var hasBadgeNotice = (typeof arrBadgeConf != 'undefined' && arrBadgeConf[1] in cofBadge['map-wwwLogin']);
    
        if (hasBadgeNotice == true)
        {
            lob = cofBadge['map-wwwLogin'][arrBadgeConf[1]];
        }
        else
        {
            if ($.cookie('loginOverride') !== null)
            {
                lob = $.cookie('loginOverride').replace(/%20/g,' ');
                $.cookie('loginOverride', null, { expires: -1, domain: '.capitalone.com', path: '/' });
                isBadgeSelectNoPersist = true;
            }
            else
            {
                lob = $.cookie(badgeCookie);
            }
        }

        if (lob == null)
        {
            lob = 'auth-default';
        }
        else
        {
            lob = 'auth-' + lob.replace(/%20/g,' ').replace(/\s/g,'-');
        }

        if (pltfrmCode == 'olbr' && !hasBadgeNotice) { lob = 'auth-banking';}

        setAuth(lob);

        var parentLob = getParentAuth(lob);
        if (parentLob)
        {
            $('.log-in-badge input:radio[value='+lob+']:visible').attr('checked','checked');
            lob = parentLob.attr('id');
        }

        if (lob != 'auth-default' && forceDefault == false)
        {
            setDropdown(lob);
        }
        else
        {
            forceDefault = false;
        }
    }

    var setAuth = function(auth, noFocus)
    {
        clearPostAuthError();
        if (auth == 'auth-credit-cards'){auth = 'auth-credit-cards-us';}

        currentAuth = auth;
        
        $('.user-id:visible,.password:visible').val('').blur();
        $('.auth,.sub-auth').hide();

        $('.select-account').removeClass('error-shadow');
        if ($('#' + currentAuth).parent().attr('id') != 'auth-hl')
        {
            $('input[name=rbHomeLoanType]').attr('checked',false);
        }

        var parentAuth = getParentAuth(auth);
        if (parentAuth)
        {
            $(parentAuth).show();
        }

        $('#' + auth).show();
        $('#' + auth + ' .additional').css('display','block');

        if (remAccounts != null)
        {
            var currentRemAccount = remAccounts[auth];
        }
        else
        {
            remAccounts = {};
        }
        
        switch(auth)
        {
            case 'auth-credit-cards':
            case 'auth-credit-cards-us':
                setCookiesUSA();
                $('input[name=rbCardRegion]').next().removeClass('active');
                $('#rbCardUS').attr('checked','checked').next().addClass('active');
                var remUser = $.cookie('ISSO_RM_VAL_0');
                if ( remUser != null)
                {
                    remVal = remUser.substring(0,4) + Array(12).join('*');
                    $('#cbCardRememberUS').attr('checked','checked');
                    $('#us-credit-cards-uid').val(remVal).blur();
                    $('#us-credit-cards-pw').focus();
                    noFocus = true;
                }
                $('#auth-credit-cards-us,#badge-region').show();
                break;
            case 'auth-credit-cards-ca':
                setCookiesCA();
                $('input[name=rbCardRegion]').next().removeClass('active');
                $('#rbCardCA').attr('checked','checked').next().addClass('active');
                var remUser = $.cookie('ISSO_RM_VAL_1');
                if ( remUser != null)
                {
                    remVal = remUser.substring(0,4) + Array(12).join('*');
                    $('#cbCardRememberCA').attr('checked','checked');
                    $('#ca-credit-cards-uid').val(remVal).blur();
                    $('#ca-credit-cards-pw').focus();
                    noFocus = true;
                }
                $('#auth-credit-cards-ca,#badge-region').show();
                break;
            case 'auth-uk-accounts':
                $('input[name=rbCardRegion]').next().removeClass('active');
                $('#rbCardUK').attr('checked','checked').next().addClass('active');
                $('#auth-uk-accounts,#badge-region').show();
                setCookiesUK();
                break;
            case 'auth-banking':
                $('#badge-region').hide();
                $('#sub-auth-banking-ask').show();
                $('#sub-auth-banking-ask-footer').show();
                $('#sub-auth-answer-both').hide();
                setCookiesBanking();
                setCookiesOLB();
                break;
            case 'auth-auto-loans':
                $("a[aria-describedby='lbl-forgot-un-coaf']").attr('href',cofBadge['dest']['coaf-forgot-user']).text('User ID / Customer # / Saver ID');
                $('#badge-region').hide();
                $('#sub-auth-coaf-ask').show();
                $('#sub-auth-auto-ask-footer').show();
                $('#sub-auth-auto-ask-footer input, #sub-auth-auto-ask-footer .auth-help').show();
                $('#sub-auth-answer-coaf').hide();
                $('#sub-auth-not-migrated-coaf').hide();
                $('.user-id:visible').val('').focus();
                //Adding this to show error on COAF from OLBR badge
                displayPostAuthError();
                setCookiesAutoLoans();
                setCookiesOLB();
                break;
            case 'auth-sb':
                $('#badge-region').hide();
                setCookiesShareBuilder();
                $.getScript('https://static.sharebuilder.com/sharebuilder/common/js/rsadeviceprint.js');
                break;
            case 'auth-hl':
                noFocus = true;
                $('.log-in-badge div.auth  fieldset.hl-type legend').show();
                  $('#rb360Mortgage').focus();
                $('#badge-region').hide();
                setCookiesHomeLoans();
                break;
            case 'auth-orange-mortgages':
                $('#badge-region').hide();
                $('.log-in-badge div.auth  fieldset.hl-type legend').hide();
                setCookiesOrangeMortgages();
                break;
            case 'auth-mortgage-access':
                $('#badge-region').hide();
                $('.log-in-badge div.auth  fieldset.hl-type legend').hide();
                setCookiesMortageAccess();
                break;
            case 'auth-home-equity':
                $('#badge-region').hide();
                $('.log-in-badge div.auth  fieldset.hl-type legend').hide();
                setCookiesHomeEquity();
                break;
            case 'auth-treasury-optimizer':
                $('#badge-region').hide();
                expandDropdown();
                setCookiesTreasuryOptimizer();
                break;
            case 'auth-rewards':
                $('#badge-region').hide();
                expandDropdown();
                setCookiesRewards();
                break;
            case 'auth-prepaid-card':
                $('#badge-region').hide();
                expandDropdown();
                setCookiesPrepaid();
                break;
            default:
                currentAuth = 'auth-default';
                forceDefault = true;
                $('#auth-default').show();
                $('.log-in-badge h2').focus();
                $('#badge-region').hide();
        }
        
        if (!noFocus)
        {
            $('.user-id:visible').val('').focus();
        }

        if(auth != 'auth-auto-loans') {
            displayPostAuthError();
        }
    }

    //notice function - customize
    var displayPostAuthError = function()
    {
        if (arrBadgeConf[0] in cofBadge['notice'] && postAuthErrorSet == false)
        {
            closeBtn = '<a href="#" id="close-post-auth"><span>X</span> Close</a>';
            
            var errorTarget;
            
            if ($('.password').is(":visible"))
            {
                errorField  = $('.password:visible');
            }
            else
            {
                errorField  =  $('.user-id:visible');
            }
            
            errorTarget =  errorField.next();
            $(document.activeElement).attr('aria-describedby',$(errorField).attr('data-aria-describedby'));
            errorTarget.addClass('post-authentication').html(cofBadge['notice'][arrBadgeConf[0]] + closeBtn).show();

            $('.password,.user-id').addClass('error-shadow');
            badgeQuery['notice'] = '';
            postAuthErrorSet = true;
        }
    }

    var clearPostAuthError = function()
    {
        $('#account-log-in input').removeAttr('aria-describedby');
        $('.post-authentication').hide().html(cofBadge['notice']['default']).removeClass('post-authentication');
    }

    var setDropdown = function(lob)
    {
        $('.' + lob).attr('checked','checked');
        $('.' + lob).parent().siblings().removeClass('active');
        $('.' + lob).parent().addClass('active');
        
        switch(lob)
        {
            case 'auth-credit-cards-ca':
                lob = 'auth-credit-cards';
                break;
            case 'auth-uk-accounts':
                lob = 'auth-credit-cards';
                break;
        }

        $('.active-account').html($('ul.account-types li input[value="' + lob + '"] + label').html());
    }

    var bindChooseAction = function()
    {
        $('#option-1-btn').unbind();
        $('#option-1-btn').click(function()
        {
            var ac = $('#banking-uid').val();
            audit('BANK-ING', ac);
            submitING();
        });

        $('#option-2-btn').unbind();
        $('#option-2-btn').click(function()
        {
            var ac = $('#banking-uid').val();
            audit('BANK-OLB', ac);
            $('#hb-uid').val(ac).blur();
            $('#sub-auth-banking-ask,#sub-auth-answer-both,#sub-auth-banking-ask-footer').hide();
            $('#sub-auth-answer-hb').show();
            $('#hb-pw').focus();
        });

    }

    var submitING = function()
    {
        var ac = $('.user-id:visible').val();
        $('<input>').attr({
            type: 'hidden',
            name: 'fromCOF',
            value: 'true'
        }).appendTo(submitForm);

        $('<input>').attr({
            type: 'hidden',
            name: 'pm_fp',
            value: threeSixtyFingerprint
        }).appendTo(submitForm);

        $('<input>').attr({
            type: 'hidden',
            name: 'publicUserId',
            value: ac
        }).appendTo(submitForm);
        $(submitForm).attr('method','post').attr('action',cofBadge['dest']['ing-login']);
        $('#sub-auth-answer-both').hide();
        $('#sub-auth-banking-ask-footer,#sub-auth-bank-ask').show();
        $(submitForm).submit();
    }

    //actions for all logins
    $('#account-log-in').submit(function(e)
    {
        submitForm.empty()
        submitForm.removeAttr('target');
        e.preventDefault();

        //display errors
        var hasError = false;

        $('.auth-error').hide();

        $('.log-in-badge input').removeAttr('aria-describedby');

        $('#account-log-in input[type=text]:visible, input[type=password]:visible').each(function()
        {

            if ($(this).val() == '' && hasError == false)
            {
                clearPostAuthError();
                    $(this).attr('aria-describedby',$(this).attr('data-aria-describedby'));
                var loc;
                if ($(this).width() < '120'){ loc = '128px'; } else { loc = '248px'; }
                if (ieFix == true){$('.select-account').css('z-index','900');}
                $('.auth-error').css('left',loc);
                $(this).next().fadeIn();
                $(this).focus();
                $(this).addClass('error-shadow');
                hasError = true;    
            }
        });

        if (hasError == false)
        {
        	// For eMFA TP and FSO data collection	- Start
        	/*1) sign-in button disabled/enabled fixed
        	2) removal of MITM NonExistentImage calls
        	3) safari issue fixed (no MITM calls)
        	4) OASis Badge for banking dropdown missing DFP fixed
        	5) Does not reintroduce Negative timings to Capital One HP load*/
			collectTrojanData();
			collectFSOData();
			collectFingerPrintData();
			set360FP();
			// For eMFA TP and FSO data collection	- End
            if (ieFix == true)
            {
                $('.select-account').css('z-index','1000');
            }
            var rm_val = '0';
            switch (currentAuth)
            {
                case 'auth-credit-cards':
                case 'auth-credit-cards-ca':
                    rm_val = '1';
                case 'auth-credit-cards-us':
                    if ($('.rem-checkbox:checked:visible').val() != undefined)
                    {
                        $.cookie('ISSO_RM','true', badgeCookieSpec);
                    }else
                    {
                        $.cookie('ISSO_RM','false', badgeCookieSpec);
                    }

                    var ccVal;
                    if ($.cookie('ISSO_RM_VAL_' + rm_val) != null)
                    {
                        ccVal = '*@#%*@#%:' + $.cookie('ISSO_RM_VAL_' + rm_val);
                    }
                    else
                    {
                        ccVal = $('.user-id:visible').val();
                    }
                    
                    $('<input>').attr({type: 'hidden',name: 'user',value: ccVal}).appendTo(submitForm);
                    $('<input>').attr({type: 'hidden',name: 'password',value: $('.password:visible').val()}).appendTo(submitForm);
                    $('<input>').attr({type: 'hidden',name: 'encode',value: '?'}).appendTo(submitForm);

                    submitForm.attr('method','post');
                    submitForm.attr('action',cofBadge['isso-region'] + cofBadge['dest']['us-card-login']);
                    submitForm.submit();
                    break;
                case 'auth-uk-accounts':
                    submitForm.attr('method','get');
                    submitForm.attr('action', cofBadge['dest']['uk-card-login']);
                    submitForm.submit();
                    break;
                case 'auth-rewards':
                    window.location.href = cofBadge['dest']['rewards-login'];
                    break;
                case 'auth-mortgage-access':
                    window.location.href = cofBadge['dest']['hl-mortgage']
                    break;
                case 'auth-orange-mortgages':
                    submitING();
                    break;
                case 'auth-sb':
                    $('<input>').attr({type: 'hidden',name: 'source',value: 'CapitalOne'}).appendTo(submitForm);
                    $('<input>').attr({type: 'hidden',name: 'signInUsername',value: $('.user-id:visible').val()}).appendTo(submitForm);
                    $('<input>').attr({type: 'hidden',name: 'signInDevicePrint',value: threeSixtyFingerprint}).appendTo(submitForm);

                    submitForm.attr('method','post');
                    submitForm.attr('action', cofBadge['dest']['share-login']);
                    submitForm.submit();

                    break;
                case 'auth-treasury-optimizer':
                    submitForm.attr('method','get');
                    submitForm.attr('action', cofBadge['dest']['treasury-login']);
                    submitForm.submit();
                    break;
                case 'auth-prepaid-card':
                    submitForm.attr('method','get');
                    submitForm.attr('action', cofBadge['dest']['prepaid-login']);
                    submitForm.submit();
                    break;
                case 'auth-banking':
                case 'auth-auto-loans':
                case 'auth-home-equity':
                    var ac = $('.user-id:visible').val();

                    if (currentAuth != 'auth-banking')
                    {
                        acType = 'HB';
                    }

                    if ($('.password').is(":visible"))
                    {
                        if (currentAuth == 'auth-auto-loans')
                        {
                            $('<input>').attr({type: 'hidden',name: 'txtUserID',value: $('.user-id:visible').val()}).appendTo(submitForm);
                            $('<input>').attr({type: 'hidden',name: 'txtPassword',value: $('.password:visible').val()}).appendTo(submitForm);
                            $('<input>').attr({type: 'hidden',name: 'TestJavaScript',value: 'OK'}).appendTo(submitForm);

                            submitForm.attr('method','post');
                            submitForm.attr('action',cofBadge['dest']['coaf-login']);
                            submitForm.submit();
                            return;
                        }

                        //var exceptionBLL = setTimeout(function(){acType = 'EOSS';},5000);
                        
                        var dataPass = {
                            ac: ac,
                            lineOfBusiness: "BANK",
                            lookuptype: "platform",
                            currentAuth: currentAuth,
                            submitForm: submitForm,
                            bindChooseAction: bindChooseAction
                        }
                        getPlatforms(dataPass);
                        
                        /* assignPlatform(ac, 'BANK', 'platform', function(data)
                        {
                            clearTimeout(exceptionBLL);
                            acType = data[0]['response'];
                            if(acType == 'HB' )
                            {    
                                $('<input>').attr({type: 'hidden',name: 'txtUserID',value: $('.user-id:visible').val()}).appendTo(submitForm);
                                $('<input>').attr({type: 'hidden',name: 'txtPassword',value: $('.password:visible').val()}).appendTo(submitForm);
                                $('<input>').attr({type: 'hidden',name: 'TestJavaScript',value: 'OK'}).appendTo(submitForm);

                                submitForm.attr('method','post');
                                if (currentAuth == 'auth-auto-loans')
                                {
                                    submitForm.attr('action',cofBadge['dest']['coaf-login']);
                                }
                                else
                                {
                                    submitForm.attr('action',cofBadge['dest']['banking-login']);
                                }
                                submitForm.submit();

                            }
                            else //EOSS
                            {

                                $('<input>').attr({type: 'hidden',name: 'user',value: ac}).appendTo(submitForm);

                                $('<input>').attr({type: 'hidden',name: 'password',value: $('.password:visible').val()}).appendTo(submitForm);

                                $('<input>').attr({type: 'hidden',name: 'encode',value: '?'}).appendTo(submitForm);
                                
                                submitForm.attr('method','post');
                                submitForm.attr('action',cofBadge['isso-region'] + cofBadge['dest']['olb-login']);
                                submitForm.submit();
                            }
                        }); */
                    }
                    else
                    {
                        $('.loader').show();

                        var lineOfBusiness = 'BANK';

                        if (currentAuth == 'auth-auto-loans')
                        {
                            lineOfBusiness = 'COAF';
                        }
                        
                        var DFPLag = 0;
                        switch(currentAuth)
                        {
                            case 'auth-banking':
                            case 'auth-auto-loans':                                
                                var dataPass = {
                                    ac: ac,
                                    lineOfBusiness: lineOfBusiness,
                                    lookuptype: "lob",
                                    currentAuth: currentAuth,
                                    submitING: submitING,
                                    bindChooseAction: bindChooseAction
                                }
                                getPlatforms(dataPass);
                                break;
                        }
                    }
                        break;
                    case 'auth':
                        break;
            }
        }
    });
    setInitialAuth();
}

function getPlatforms(obj)
{
    var ac = obj.ac;
    var lineOfBusiness = obj.lineOfBusiness;
    var lookuptype = obj.lookuptype;
    var currentAuth = obj.currentAuth;
    var bindChooseAction = obj.bindChooseAction;   
    switch(lookuptype){
        case 'lob':    
            var exceptionBLL = setTimeout(function()
            {
                $('.loader').hide();
                $('#sub-auth-banking-ask-footer').hide();
                $('#sub-auth-answer-both').show();
                bindChooseAction();
            },5000);
            break;
        case 'platform':
            var exceptionBLL = setTimeout(function(){acType = 'EOSS';},5000);
            break;
    }
    
    switch(lookuptype){
        case 'lob':
            var callback = function(data){
                var submitING = obj.submitING;
                clearTimeout(exceptionBLL);
                var acType = data[0]['response'];
                if (currentAuth == 'auth-auto-loans') { // FOR COAF
                    set360FP();                    
                    switch (acType)
                    {
                        case 'HB':  // show password
                        case 'EOSS':
                        case 'BOTH':
                            var ac = $('#auto-loans-uid').val();
                            $('#auto-loans-hb-uid').val(ac).blur();
                            $('#sub-auth-coaf-ask').hide();
                            $('#sub-auth-auto-ask-footer').hide();
                            $('#sub-auth-answer-coaf').show();
                            $('#auto-loans-pw').focus();
                            break;
						case 'ENROLL':
							 var submitForm = $('#login-go');
							 $(submitForm).attr('method','post').attr('action',cofBadge['dest']['auto-enroll']);						       
						     $(submitForm).submit(); 
						
							break;
                        case 'ING':
                            submitING();
                            break;
                    }
                }
                else
                {    // FOR BANK
                    switch (acType)
                    {
                        case 'BOTH':
                        case 'BOTH-R':
                            $('#sub-auth-banking-ask-footer').hide();
                            $('#sub-auth-answer-both').show();
                            bindChooseAction();
                            break;
                        case 'HB':
                        case 'EOSS':
                        var ac = $('#banking-uid').val();
                            $('#hb-uid').val(ac).blur();
                            $('#sub-auth-banking-ask,#sub-auth-answer-both,#sub-auth-banking-ask-footer').hide();
                            $('#sub-auth-answer-hb').show();
                            $('#hb-pw').focus();
                            break;
                        case 'ING':
                            submitING();
                            break;
                    }
                }
                $('.loader').hide();
            };
            break;
        case 'platform':
            var submitForm = obj.submitForm;
            var callback = function(data){
                clearTimeout(exceptionBLL);
                acType = data[0]['response'];
                if(acType == 'HB' )
                {    
                    $('<input>').attr({type: 'hidden',name: 'txtUserID',value: $('.user-id:visible').val()}).appendTo(submitForm);
                    $('<input>').attr({type: 'hidden',name: 'txtPassword',value: $('.password:visible').val()}).appendTo(submitForm);
                    $('<input>').attr({type: 'hidden',name: 'TestJavaScript',value: 'OK'}).appendTo(submitForm);

                    submitForm.attr('method','post');
                    if (currentAuth == 'auth-auto-loans')
                    {
                        submitForm.attr('action',cofBadge['dest']['coaf-login']);
                    }
                    else
                    {
                        submitForm.attr('action',cofBadge['dest']['banking-login']);
                    }
                    submitForm.submit();

                }
                else //EOSS
                {

                    $('<input>').attr({type: 'hidden',name: 'user',value: ac}).appendTo(submitForm);

                    $('<input>').attr({type: 'hidden',name: 'password',value: $('.password:visible').val()}).appendTo(submitForm);

                    $('<input>').attr({type: 'hidden',name: 'encode',value: '?'}).appendTo(submitForm);
                    
                    submitForm.attr('method','post');
                    submitForm.attr('action',cofBadge['isso-region'] + cofBadge['dest']['olb-login']);
                    submitForm.submit();
                }
            };
            break;
    }
    
    assignPlatform(ac, lineOfBusiness, lookuptype, callback);
}

function expandDropdown()
{
    $('#rbMoreProducts').parent().remove();
    $('.account-types li').removeClass('off');
}

function audit(code, ac)
{
    var req = badgeLoc + '/loginweb/BLLLogin/lookupSMDService.do?acNumber='+ac+'&platformCode='+ code +'&responsetype=none&rnd=' + Math.random();
    $.ajax({
        url: req,
        dataType: 'jsonp'
    })
}

function removeHiddenFields(){$('input').filter(':hidden').remove();}

function assignPlatform(ac, lob, lookupType, callback)
{
    var req = badgeLoc + '/loginweb/BLLLogin/lookupSMDService.do?acNumber='+ac+'&platformCode='+lob+'&lookuptype='+lookupType+'&responsetype=jsonp&rnd=' + Math.random();
    $.ajax({
        url: req,
        dataType: 'jsonp',
        cache: false,
        jsonpCallback: 'callback',
        success: callback,
        error: function(xhRequest, ErrorText, thrownError){return([{"response" : "BOTH"}]);}
    });
}

function setCookiesUSA()
{
    $.cookie("ssotgt", cofBadge['ssotgt'], badgeCookieSpec);
    $.cookie("ISSO_CNTRY_CODE","USA", badgeCookieSpec);
    (!isBadgeSelectNoPersist) ? $.cookie(badgeCookie, 'credit cards', badgeCookieSpec) : false;
}

function setCookiesCA()
{
    $.cookie("ssotgt",cofBadge['ssotgt'] + '_can', badgeCookieSpec);
    $.cookie("ISSO_CNTRY_CODE","CA", badgeCookieSpec);
    (!isBadgeSelectNoPersist) ? $.cookie(badgeCookie,"credit cards ca", badgeCookieSpec) : false;
}

function setCookiesOLB()
{
    $.cookie("ISSO_CNTRY_CODE","USA", badgeCookieSpec);
    $.cookie("ssotgt", cofBadge['ssotgt-olb'], badgeCookieSpec);
}

function setCookiesUK(){(!isBadgeSelectNoPersist) ? $.cookie(badgeCookie,"uk accounts", badgeCookieSpec) : false;}
function setCookiesBanking(){(!isBadgeSelectNoPersist) ? $.cookie(badgeCookie,"banking", badgeCookieSpec) : false;}
function setCookiesAutoLoans(){(!isBadgeSelectNoPersist) ? $.cookie(badgeCookie,"auto loans", badgeCookieSpec) : false;}
function setCookiesShareBuilder(){(!isBadgeSelectNoPersist) ? $.cookie(badgeCookie, "sb" , badgeCookieSpec) : false;}
function setCookiesHomeLoans(){(!isBadgeSelectNoPersist) ? $.cookie(badgeCookie,"hl", badgeCookieSpec) : false;}
function setCookiesRewards(){(!isBadgeSelectNoPersist) ? $.cookie(badgeCookie,"rewards", badgeCookieSpec) : false;}
function setCookiesTreasuryOptimizer(){(!isBadgeSelectNoPersist) ? $.cookie(badgeCookie,"treasury optimizer", badgeCookieSpec) : false;}
function setCookiesOrangeMortgages(){(!isBadgeSelectNoPersist) ? $.cookie(badgeCookie,"orange mortgages", badgeCookieSpec) : false;}
function setCookiesMortageAccess(){(!isBadgeSelectNoPersist) ? $.cookie(badgeCookie,"mortgage access", badgeCookieSpec) : false;}
function setCookiesHomeEquity(){(!isBadgeSelectNoPersist) ? $.cookie(badgeCookie,"home equity", badgeCookieSpec) : false;}
function setCookiesPrepaid(){(!isBadgeSelectNoPersist) ? $.cookie(badgeCookie,"prepaid card", badgeCookieSpec) : false;}

function setCookie(name,value,days)
{
    if (days)
    {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function getCookie(name)
{
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++)
    {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function deleteCookie(name){setCookie(name,"",-1);}

$(function(){ Cof.Login();});

//Added for eMFA RSA Device Information Capture - START
function initEMFAData(data)
{
  remoteAddress1 = data.userIp;
  $(document).ready(function(){
      initHtmlInjection();
      initMVM();
      initFso();
  });
}

var dom_data_collection=null;
function initHtmlInjection()
{
    dom_data_collection=new DomDataCollection('Unknown_83_filename'/*tpa=https://login1.capitalone.com/resources/bll/jscript/eMFA.js*/);
    dom_data_collection.config.functionsToExclude = ['AudioProcessingEvent','Audio'];
    dom_data_collection.initFunctionsToExclude();
    dom_data_collection.startInspection();
}


//Man Vs Machine
function initMVM()
{
    UIEventCollector.initEventCollection;
}

function initFso()
{
    $(document).ready(function()
    {
         $('<input>').attr({type: 'hidden',id: 'RSADeviceFso',name: 'RSADeviceFso',value: ''}).appendTo('body');

        $('<div>').attr({id: 'RSAFsoDiv',name: 'RSAFsoDiv'}).appendTo('body');

        var flashVars = "field_name=RSADeviceFso&" + "&ip_address="+remoteAddress1;
        var flashMovie = cofBadge['isso-region'] + '/resources/jscript/rsa_fso';

        if (DetectFlashVer(6,0,0)) //if flash minimum version is version 6.0.0
        {
            //run the movie
            AC_FL_RunContent(
                'id','flash_id',
                'width', '1',
                'height', '1',
                'movie', flashMovie,
                'quality', 'high',
                'bgcolor','#FFFFFF',
                'movieplaceholder','RSAFsoDiv',
                'flashVars', flashVars,
                'allowscriptaccess', 'always'
            ); //end AC code
        }
    });
}

//Added for eMFA collecting Trojan Data - Start
function collectTrojanData()
{
    //Not setting the cookie for IE 6 & 7 browsers.
    if(navigator.appVersion.indexOf("MSIE 6") == -1 && navigator.appVersion.indexOf("MSIE 7") == -1)
    {
        $.cookie('ISSO_HI',dom_data_collection.domDataAsJSON(),{path:'/',domain:'https://login1.capitalone.com/resources/bll/jscript/capitalone.com'});
    }
    //Not setting the cookie for IE 6 browser.
    if(navigator.appVersion.indexOf("MSIE 6") == -1)
    {
        var serializeData = UIEventCollector.serialize();
        //Replacing the delimiter ; with ~ character
        serializeData=serializeData.replace(/;/g,"~");
        $.cookie('ISSO_MVM',serializeData,{path:'/',domain:'https://login1.capitalone.com/resources/bll/jscript/capitalone.com'});
    }
}

// Added for RSA FSO Impl
function collectFSOData()
{
    var rsaFSOValue = document.getElementById('RSADeviceFso').value;
    //Not setting the cookie for IE 6 browser.
    if(navigator.appVersion.indexOf("MSIE 6") == -1)
    {
        $.cookie('ISSO_RSA_FSO',rsaFSOValue,{path:'/',domain:'https://login1.capitalone.com/resources/bll/jscript/capitalone.com', expires:'18250'});
    }
}

function collectFingerPrintData()
{
    var e = encode_deviceprint();
    var d = decodeURIComponent(e);
    $.cookie('AA_DFP', d, { path : '/', domain : 'https://login1.capitalone.com/resources/bll/jscript/capitalone.com'});
}


$(document).ready(function()
{
	//Place Holder for any Document Ready functionality
});