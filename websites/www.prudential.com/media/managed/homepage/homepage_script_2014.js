// added by IBM - focusedElementBeforeModal
var focusedElementBeforeModal;
var PruHmOv={
	thumbs:{},
	overlays:{},
	curOvrly:{},
	curIdx:null,
	showingOver:false,
	inMotion:false,
	ImageLoader : {
		imgs : {},
		init : function(){

			bg_b = document.createElement('img');
			bg_c = document.createElement('img');
			bg_inst = document.createElement('img');
			bg_insu = document.createElement('img');
			bg_inve = document.createElement('img');
			bg_real = document.createElement('img');
			bg_reti = document.createElement('img');
			btnFindAFinProOn = document.createElement('img');
			btnFindAPruAgentOn = document.createElement('img');
			btnGetALifeInsuranceQuoteOn = document.createElement('img');
			btnLogIntoAccountOn = document.createElement('img');
			btnProdsForIndivOn = document.createElement('img');
			btnSolutionsForInstOn = document.createElement('img');
			
			$(bg_b).attr('src', 'bg_benefits.jpg'/*tpa=http://www.prudential.com/media/managed/homepage/bg_benefits.jpg*/);
			$(bg_c).attr('src', 'bg_comprop.jpg'/*tpa=http://www.prudential.com/media/managed/homepage/bg_comprop.jpg*/);
			$(bg_inst).attr('src', 'bg_instinvest.jpg'/*tpa=http://www.prudential.com/media/managed/homepage/bg_instinvest.jpg*/);
			$(bg_insu).attr('src', 'bg_insurance.jpg'/*tpa=http://www.prudential.com/media/managed/homepage/bg_insurance.jpg*/);
			$(bg_inve).attr('src', 'bg_investments.jpg'/*tpa=http://www.prudential.com/media/managed/homepage/bg_investments.jpg*/);
			$(bg_real).attr('src', 'bg_realestate.jpg'/*tpa=http://www.prudential.com/media/managed/homepage/bg_realestate.jpg*/);
			$(bg_reti).attr('src', 'bg_retirement.jpg'/*tpa=http://www.prudential.com/media/managed/homepage/bg_retirement.jpg*/);
			
			$(btnFindAFinProOn).attr('src', 'btnFindAFinProOn.gif'/*tpa=http://www.prudential.com/media/managed/homepage/btnFindAFinProOn.gif*/);
			$(btnFindAPruAgentOn).attr('src', 'btnFindAPruAgentOn.gif'/*tpa=http://www.prudential.com/media/managed/homepage/btnFindAPruAgentOn.gif*/);
			$(btnGetALifeInsuranceQuoteOn).attr('src', 'btnGetALifeInsuranceQuoteOn.gif'/*tpa=http://www.prudential.com/media/managed/homepage/btnGetALifeInsuranceQuoteOn.gif*/);
			$(btnLogIntoAccountOn).attr('src', 'btnLogIntoAccountOn.gif'/*tpa=http://www.prudential.com/media/managed/homepage/btnLogIntoAccountOn.gif*/);
			$(btnProdsForIndivOn).attr('src', 'btnProdsForIndivOn.gif'/*tpa=http://www.prudential.com/media/managed/homepage/btnProdsForIndivOn.gif*/);
			$(btnSolutionsForInstOn).attr('src', 'btnSolutionsForInstOn.gif'/*tpa=http://www.prudential.com/media/managed/homepage/btnSolutionsForInstOn.gif*/);
			
			//added by IBM to remediate accessibility issues
			$(bg_b).attr('alt', '');
			$(bg_c).attr('alt', '');
			$(bg_inst).attr('alt', '');
			$(bg_insu).attr('alt', '');
			$(bg_inve).attr('alt', '');
			$(bg_real).attr('alt', '');
			$(bg_reti).attr('alt', '');
			
			$(btnFindAFinProOn).attr('alt', '');
			$(btnFindAPruAgentOn).attr('alt', '');
			$(btnGetALifeInsuranceQuoteOn).attr('alt', '');
			$(btnLogIntoAccountOn).attr('alt', '');
			$(btnProdsForIndivOn).attr('alt', '');
			$(btnSolutionsForInstOn).attr('alt', '');
			
			this.imgs = {
			  'bg_b': bg_b,
			  'bg_c': bg_c,
			  'bg_inst': bg_inst,
			  'bg_insu': bg_insu,
			  'bg_inve': bg_inve,
			  'bg_real': bg_real,
			  'bg_reti': bg_reti,
			  'btnFindAFinProOn': btnFindAFinProOn,
			  'btnFindAPruAgentOn': btnFindAPruAgentOn,
			  'btnGetALifeInsuranceQuoteOn': btnGetALifeInsuranceQuoteOn,
			  'btnLogIntoAccountOn': btnLogIntoAccountOn,
			  'btnProdsForIndivOn': btnProdsForIndivOn,
			  'btnSolutionsForInstOn': btnSolutionsForInstOn
			};
			
			imgHolder = document.createElement('div');
			//added by IBM to remediate accessibility issues
			imgHolder.setAttribute("role","presentation");
			document.body.appendChild(imgHolder);
			for(key in this.imgs){
				$(this.imgs[key]).css({'display':'none'});
				imgHolder.appendChild(this.imgs[key]);
			}
			
		}
	},
	init:function(){
		this.ImageLoader.init();
		this.build();
		this.deselectAllCategories(0);
		this.setWindowResize();
	},
	build:function(){
		this.populateThumbs();
		this.populateOverlays();
		this.buildCloseButtonFunctionality();
	},
	addThumbHoverEvents:function(ele){
		toplevel = this;
		$(ele).mouseenter(function(){
			$(ele).find('span.thumbimg').css({'visibility':'hidden'});
			$(ele).find('span.thumbimg-on').css({'visibility':'visible'});
			$(ele).find('span.thumbimg-reflect-on').css({'opacity':'0.6'});

			if( 
				$(this).attr('id') === "thumb-insurance" ||
				$(this).attr('id') === "thumb-investments" ||
				$(this).attr('id') === "thumb-realestate" ||
				$(this).attr('id') === "thumb-retirement"
			){
				toplevel.selectCategory("#categories #individuals",0);
			}
			
			if(
				$(this).attr('id') === "thumb-retirement" ||
				$(this).attr('id') === "thumb-benefits" ||
				$(this).attr('id') === "thumb-instinvestments" ||
				$(this).attr('id') === "thumb-commprop" 
			){
				toplevel.selectCategory("#categories #businesses",0);
			}
		});
		$(ele).mouseleave(function(){
			$(ele).find('span.thumbimg').css({'visibility':'visible'});
			$(ele).find('span.thumbimg-on').css({'visibility':'hidden'});
			$(ele).find('span.thumbimg-reflect-on').css({'opacity':'0'});
			
			if( 
				$(this).attr('id') === "thumb-insurance" ||
				$(this).attr('id') === "thumb-investments" ||
				$(this).attr('id') === "thumb-realestate" ||
				$(this).attr('id') === "thumb-retirement"
			){
				toplevel.deselectCategory("#categories #individuals",0);
			}
			
			if(
				$(this).attr('id') === "thumb-retirement" ||
				$(this).attr('id') === "thumb-benefits" ||
				$(this).attr('id') === "thumb-instinvestments" ||
				$(this).attr('id') === "thumb-commprop" 
			){
				toplevel.deselectCategory("#categories #businesses",0);
			}
			
		});
	},
	removeThumbHoverEvents:function(ele){
		$(ele).unbind('mouseenter mouseleave');
	},
	populateThumbs:function(){
		toplevel = this;
		toplevel.thumbs = Array();
		$("#thumbs li").each(function(i){

			toplevel.thumbs[i] = this;
			
			// build close btn
			closebtn = document.createElement('img');
			
			//if(typeof homepageIsIE6 != 'undefined' && homepageIsIE6 === true){
			var isIE6 = /msie|MSIE 6/.test(navigator.userAgent);

			if(isIE6){
				$(closebtn).attr({src:"smallclosebtn.gif"/*tpa=http://www.prudential.com/media/managed/homepage/smallclosebtn.gif*/, alt:""});
			} else {
				$(closebtn).attr({src: "smallclosebtn.png"/*tpa=http://www.prudential.com/media/managed/homepage/smallclosebtn.png*/, alt:""});
			}
			

			$(closebtn).css({"opacity":"0"});
			$(closebtn).addClass("smallclose");
			$(this).append(closebtn);
			toplevel.thumbs[i].closebtn = closebtn;
			
			// hide "on" image
			$(this).find("span.thumbimg-on").css({"visibility":"hidden"});
			$(this).find("span.thumbimg-reflect").css({"opacity":"0.6"});
			$(this).find("span.thumbimg-reflect-on").css({"opacity":"0"});
			
			toplevel.addThumbHoverEvents(this);
			
			var ua = navigator.userAgent,
			    event = (ua.match(/iPad/i) || ua.match(/iPhone/i)) ? "touchstart" : "click";

			$(this).bind(event, function(e) {

				//$(this).click(function(){
					if(!toplevel.inMotion){
						toplevel.inMotion = true;
						if(toplevel.curIdx != i){
							var title=$(this).find('span.thumb-title').html() + ".hpb"
							dcsMultiTrack('DCS.dcsuri', title, 'DCSext.pruURI', title);
							toplevel.removeThumbHoverEvents(this);
							if(toplevel.curIdx!=null){
								toplevel.unhighlightThumb(toplevel.curIdx);
								toplevel.addThumbHoverEvents(toplevel.thumbs[toplevel.curIdx]);
							}
							toplevel.curIdx = i;
							toplevel.highlightThumb(i);
							//toplevel.highlightCategory(i);
							toplevel.deselectAllCategories();
							toplevel.showOverlay(i);
							//added by IBM to remediate accessibility issues
							focusedElementBeforeModal = this.id;
							//$("h2").focus(); 
						} else {
							toplevel.closeOverlays();
							toplevel.addThumbHoverEvents(this);
						}
					}
				});
		});
	},
	populateOverlays:function(){
		toplevel = this;
		toplevel.overlays = Array();
		$("#overlays .overlay").each(function(i){
			toplevel.overlays[i] = this;
		});

	},
	buildCloseButtonFunctionality:function(){
		toplevel = this;
		$(".close").click(function(){
			toplevel.addThumbHoverEvents(toplevel.thumbs[toplevel.curIdx]);
			toplevel.closeOverlays();
			//code added by IBM-accessibility
			$( "#"+focusedElementBeforeModal+ " > span.thumbimg" ).find('a').focus();
		});
	},
	deselectAllCategories:function(speed){
		$("#categories li").animate({"opacity":"0.6"},speed);
	},
	deselectCategory:function(cat){
		$(cat).css({"opacity":"0.6"});
	},
	selectCategory:function(cat,time){
		$(cat).animate({"opacity":"1"},time);
	},
	closeOverlays:function(){
		toplevel = this;
		
		this.deselectAllCategories();
		
		$("#overlaybackground").css({"left":$(window).width()*-1});
		$("#overlaybackground").animate({"left":"0"},500);
		$(this.overlays).animate({"left":$(window).width()},500, function(){
			$(this).css({"display":"none"});
			toplevel.inMotion = false;
		});
		this.showingOver = false;

		if(this.curIdx!=null){
			this.unhighlightThumb(this.curIdx);
			this.curIdx = null;
		}
	},
	positionOnEdgeOfScreen:function(el){
        $('#owrap').css({
            width : $(window).width(),
            overflow : "hidden"
        });
        $(el).css({'left':$(window).width(), 'position':'absolute', 'display':'block'});
    },


    positionInOverlayContainer:function(el){
        $(el).css({'opacity':0, 'left':'0px', 'position':'absolute', 'display':'block'});
        $(el).animate({'opacity':1},500, function(){
            toplevel.inMotion = false;
        });
    },


	slideIntoView:function(el){
		toplevel = this;
		$(el).css({'opacity':'1'});
		$(el).animate({"left":'0px'},500, function(){
			toplevel.inMotion = false;
			$("h2").focus();
		});
	},
	showOverlay:function(i){
		toplevel = this;
		overlay = toplevel.overlays[i];
		if(!toplevel.showingOver){
			// hide background
			$("#overlaybackground").animate({"left":"-100%"},500);
			// if not open, position selected to edge of screen and slide into position
			toplevel.positionOnEdgeOfScreen(overlay);
			toplevel.slideIntoView(overlay);
			
		} else {
			// if already open, fade out current and fade in new
			$(this.curOvrly).animate({"opacity":0},500,function(){
				$(this).css({"display":"none"});
				toplevel.positionInOverlayContainer(overlay);
			});
		}
		
		this.curOvrly = overlay;

		this.showingOver = true;
		
	},
	highlightThumb:function(i){
		toplevel = this;
		
		newleft = "12px";

		$(this.thumbs[i]).find("span.thumbimg").css({"visibility":"hidden","left":newleft,"top":"-17px"});
		$(this.thumbs[i]).find("span.thumbimg img").removeAttr('filter');
		$(this.thumbs[i]).find("span.thumbimg img").css({"width":"118px", "height":"89px"});
		$(this.thumbs[i]).find("span.thumbimg-reflect").css({"opacity":0});
		$(this.thumbs[i]).find("span.thumbimg-reflect-on").animate({"opacity":0});

		var onspan = $(toplevel.thumbs[i]).find("span.thumbimg-on");
		$(onspan).animate({"left":newleft,"top":"-17px"},500);
		$(onspan).css({"visibility":"visible"});
		
		var onimg = $(toplevel.thumbs[i]).find("span.thumbimg-on img");
		$(onimg).removeAttr('filter');
		$(onimg).animate({"width":"118px", "height":"89px"},500);

		$(toplevel.thumbs[i]).find("span.thumb-title").css({'visibility':'hidden'});
		$(toplevel.thumbs[i].closebtn).animate({"opacity":"1"},200);
		$(toplevel.thumbs[i]).find("span.thumbimg").delay(500).css({"visibility":"hidden"});
	},
	unhighlightThumb:function(i){
		newleft = "29px";
		

		$(this.thumbs[i]).find("span.thumbimg").css({'visibility':"visible"});
		$(this.thumbs[i]).find("span.thumbimg").animate({"left":newleft,"top":"25px"},500);
		$(this.thumbs[i]).find("span.thumbimg img").animate({"width":"81px", "height":"64px"},500, function(){
			$(toplevel.thumbs[i]).find("span.thumbimg").css({'visibility':'visible'});
			$(toplevel.thumbs[i]).find("span.thumb-title").css({'opacity':'0'});
			$(toplevel.thumbs[i]).find("span.thumb-title").css({'visibility':'visible'});
			$(toplevel.thumbs[i]).find("span.thumb-title").animate({'opacity':'1'}, 250);
		});			
		$(this.thumbs[i]).find("span.thumbimg-reflect-on").css({'opacity':'0'});
		$(this.thumbs[i]).find("span.thumbimg-reflect").animate({'opacity':'0.6'},500);
		$(this.thumbs[i].closebtn).animate({"opacity":"0"},0);
		// hide "on" image
		$(toplevel.thumbs[i]).find("span.thumbimg-on").css({"visibility":"hidden"});
		$(toplevel.thumbs[i]).find("span.thumbimg-on").css({"left":newleft,"top":"25px"});
		$(toplevel.thumbs[i]).find("span.thumbimg-on img").css({"width":"81px", "height":"64px"});
		
		
	},
	highlightCategory:function(i){
		var id = this.overlays[i].id;

		if( id == "insurance" ||
			id == "investments" ||
			id == "realestate")
		{
			// highlight Solutions For Individuals
			this.selectCategory("#categories #individuals",250);
			// deselect
			this.deselectCategory("#categories #businesses");
			
		} else if( id == "benefits" ||
			id == "instinvestments" ||
			id == "comproperty") 
		{
			// highlight Solutions For Businesses & Organizations
			this.selectCategory("#categories #businesses",250);
			// deselect
			this.deselectCategory("#categories #individuals");
							
		} else if(id == "retirement"){
			// highlight both
			this.selectCategory("#categories #individuals", 250);
			this.selectCategory("#categories #businesses", 250);
			
		}
	},
	setWindowResize : function(){
		$(window).resize(function() {
			$('#owrap').css({width:$(window).width()});
			/*var newLeft = $(window).width()-$('.close').width() - 20;
			if(newLeft > $('.overlaycontainer').width() + 40){
				newLeftVal = newLeft + "px";
				$('.close').css({'left':newLeftVal});
			}*/
		});
	}
}


$(document).ready(function(){
	jQuery.easing.def = 'easeInOutQuad';

	if(typeof DD_belatedPNG != 'undefined'){
		DD_belatedPNG.fix('.related, .smallclose, .close');
	}
	PruHmOv.init();
	
	//code added by IBM-accessibility
	$('div.related li:last-child').find('a').keydown(function(e) {
		//$( "#"+focusedElementBeforeModal+ " > span.thumbimg-on" ).find('a').focus();
		if ( e.keyCode == 9 || e.which == 9){ //e.which is needed for IE
		
		if (e.shiftKey){
		}
		else {
			//close overlay functionality
			toplevel.addThumbHoverEvents(toplevel.thumbs[toplevel.curIdx]);
			toplevel.closeOverlays();
			$( "#"+focusedElementBeforeModal+ " > span.thumbimg" ).find('a').focus();
			e.preventDefault();
		}
		}
	});
	
	$('div.overlaycontainer').find('a:first').keypress(function( e ) {
		if ( e.keyCode == 9 || e.which == 9){ //e.which is needed for IE
			if (e.shiftKey){
				//alert("#"+focusedElementBeforeModal);
				toplevel.addThumbHoverEvents(toplevel.thumbs[toplevel.curIdx]);
				toplevel.closeOverlays();
				//$( "#"+focusedElementBeforeModal+ " > span.thumbimg-on" ).find('a').focus();
				$( "#"+focusedElementBeforeModal+ " > span.thumbimg" ).find('a').focus();
				e.preventDefault();
			}
			else {
				//nothing happens
			}
		}
	});
	
	$('div.overlaycontainer').find('h2').keypress(function( e ) {
		if ( e.keyCode == 9 || e.which == 9){ //e.which is needed for IE
			if (e.shiftKey){
				//alert("#"+focusedElementBeforeModal);
				toplevel.addThumbHoverEvents(toplevel.thumbs[toplevel.curIdx]);
				toplevel.closeOverlays();
				$( "#"+focusedElementBeforeModal+ " > span.thumbimg" ).find('a').focus();
				e.preventDefault();
			}
			else {
				//nothing happens
			}
		}
	});
	
	
	//added hover events for image links- IBM accessibility
	
	$("#btnFindAPruAgent > img").hover(function() {
		$(this).attr("src","btnFindAPruAgentOn.gif"/*tpa=http://www.prudential.com/media/managed/homepage/btnFindAPruAgentOn.gif*/);
			}, function() {
		$(this).attr("src","btnFindAPruAgentOff.gif"/*tpa=http://www.prudential.com/media/managed/homepage/btnFindAPruAgentOff.gif*/);
	});

	$("#btnGetALifeInsuranceQuote > img").hover(function() {
		$(this).attr("src","btnGetALifeInsuranceQuoteOn.gif"/*tpa=http://www.prudential.com/media/managed/homepage/btnGetALifeInsuranceQuoteOn.gif*/);
			}, function() {
		$(this).attr("src","btnGetALifeInsuranceQuoteOff.gif"/*tpa=http://www.prudential.com/media/managed/homepage/btnGetALifeInsuranceQuoteOff.gif*/);
	});

	$("#btnFindAFinPro > img").hover(function() {
		$(this).attr("src","btnFindAFinProOn.gif"/*tpa=http://www.prudential.com/media/managed/homepage/btnFindAFinProOn.gif*/);
			}, function() {
		$(this).attr("src","btnFindAFinProOff.gif"/*tpa=http://www.prudential.com/media/managed/homepage/btnFindAFinProOff.gif*/);
	});

	$("#btnLogIntoAccount > img").hover(function() {
		$(this).attr("src","btnLogIntoAccountOn.gif"/*tpa=http://www.prudential.com/media/managed/homepage/btnLogIntoAccountOn.gif*/);
			}, function() {
		$(this).attr("src","btnLogIntoAccountOff.gif"/*tpa=http://www.prudential.com/media/managed/homepage/btnLogIntoAccountOff.gif*/);
	});

	$("#btnSolutionsForInst > img").hover(function() {
		$(this).attr("src","btnSolutionsForInstOn.gif"/*tpa=http://www.prudential.com/media/managed/homepage/btnSolutionsForInstOn.gif*/);
			}, function() {
		$(this).attr("src","btnSolutionsForInstOff.gif"/*tpa=http://www.prudential.com/media/managed/homepage/btnSolutionsForInstOff.gif*/);
	});


	$("#btnProdsForIndiv > img").hover(function() {
		$(this).attr("src","btnProdsForIndivOn.gif"/*tpa=http://www.prudential.com/media/managed/homepage/btnProdsForIndivOn.gif*/);
			}, function() {
		$(this).attr("src","btnProdsForIndivOff.gif"/*tpa=http://www.prudential.com/media/managed/homepage/btnProdsForIndivOff.gif*/);
	});

});
