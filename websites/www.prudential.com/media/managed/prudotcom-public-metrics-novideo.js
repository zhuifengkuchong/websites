/* ----------------------------------- NON-VIDEO METRICS ---------------------------------- */
function clickMetrics(type,linkobj,wtstr,srcvar,typevar,catvar,goToPage) {
	var metricType = type;
	var wstr= (wtstr.length) ? wtstr : '';
	if(metricType.indexOf('dart') > -1) dartTrack(srcvar,typevar,catvar);
	if(metricType.indexOf('wt') > -1) webtrendsData(linkobj,wtstr);
	if (goToPage && typeof(goToPage)==='function') {
		goToPage();
		return false;
	}
}

function goToPage(linkobj) {
	var $linkobj = $(linkobj);
	var targetUrl = $linkobj.attr('href');
	var targetWin = $linkobj.attr('target');
	if (targetWin && targetWin == '_blank') window.open(targetUrl);
	else if (targetUrl && targetUrl.length > 1 && targetUrl!='#' && targetUrl.toLowerCase().indexOf('javascript') < 0) 
		document.location.href = targetUrl;
}

//WEBTRENDS 
function webtrendsData(lobj,wtstr) {
	var str = wtstr;
	dcsMultiTrack('DCS.dcsuri', str, 'DCSext.pruURI', str);
}

//DART
function dartTrack(srcvar,typevar,catvar,videoEvent) {
	var mEvent = (videoEvent && videoEvent.length) ? videoEvent : '';
	var dartiframe = $('#dartiframe');
	var iframeSrc = '';
	var dartSrc = srcvar;
	var dartType = typevar;
	var dartCat = catvar;
	var dartUrl = 'http://fls.doubleclick.net/activityi;src=' + dartSrc + ';type=' + dartType + ';cat=' + dartCat;
	var axel = Math.random() + ''; 
	var a = axel * 10000000000000;
	//if there's an event and 
	//we need to track all instances, not just the first, 
	//then ord=a, otherwise, ord=1 and num=a
	iframeSrc = (mEvent && mEvent == 'replay') ? dartUrl + ';ord=' + a + '?' : dartUrl + ';ord=1;num=' + a + '?';
	if (dartiframe.length) {
		dartiframe.attr('src',iframeSrc);
	} else {
		$('body').prepend('<iframe src="' + iframeSrc + '" width="1" height="1" frameborder="0" id="dartiframe"></iframe>');
	}
}
