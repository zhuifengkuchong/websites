
$(document).ready(function(){
	$('#mapoverlay').height($('#interiorcontentwrapper').height());
	$('#closepop a').click(function(){
		$('.mapitem').hide();
		$('.defaultmap').show();
		$('#popupmap').hide();
	});
	$('.viewoverlaymap').click(function(){
		$('#popupmap').show();
	});
	
	$(".showlocation").click(function(){
		var location = $(this).attr('title');
		$('.mapitem').hide();
		$('.'+location).show();
	});
	
	//ie7 fix
	$('#tabcontainer ul.tabnav li a').hover(function(){$(this).css('cursor','pointer');},function(){$(this).css('cursor','default');});
});


//08/14/2009 - Changed print.css references
function getUrlParam( name )
{
name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
var regexS = "[\\?&]"+name+"=([^&#]*)";
var regex = new RegExp( regexS );
var results = regex.exec( window.location.href );
if( results == null )
return "";
else
return results[1];
}
var format = getUrlParam('format');
if (format == 'print') {
document.write('<link rel="stylesheet" type="text/css" href="../pagenotfound.htm--view-View-PUBLIC-js-css-print.css"/*tpa=http://www.prudential.com/view/View/PUBLIC/js/css/print.css*/ media="all" />');
}

//For IE6 Flicker
try {
document.execCommand('BackgroundImageCache', false, true);
} catch(e) {}
