//Utility Functions

function getURLParameter(name) {
	return decodeURI((RegExp(name + '=' + '(.+?)(&|$)').exec(location.search) || [, null])[1]);
}

var format = getURLParameter('format');
if (format == 'print') {
		document.write('<link href="../theme/print.css"/*tpa=http://www.prudential.com/view/View/PUBLIC/theme/print.css*/ rel="stylesheet" type="text/css" media="all" />');
} else {
		document.write('<link href="../theme/print.css"/*tpa=http://www.prudential.com/view/View/PUBLIC/theme/print.css*/ rel="stylesheet" type="text/css" media="print" />');
		document.write('<link href="../theme/style.css"/*tpa=http://www.prudential.com/view/View/PUBLIC/theme/style.css*/ rel="stylesheet" type="text/css"  media="screen" />');
		document.write('<link href="../theme/eWebEditPro.css"/*tpa=http://www.prudential.com/view/View/PUBLIC/theme/eWebEditPro.css*/ rel="stylesheet" type="text/css"  media="screen" />');	
		//Separated for use in third-party direct content
		document.write('<link href="../theme/eWebEditPro-footer.css"/*tpa=http://www.prudential.com/view/View/PUBLIC/theme/eWebEditPro-footer.css*/ rel="stylesheet" type="text/css"  media="screen" />');	
}

function removeSpecialCharacters(str) {
	re = /\$|,|@|#|~|`|\%|\*|\^|\&|\(|\)|\+|\=|\[|\-|\_|\]|\[|\}|\{|\;|\:|\'|\"|\<|\>|\?|\||\\|\!|\$|\./g;
	return str.replace(re, "");
}

//Account Select Box
function gotoAccount() {

	var URL_AccountList = new Array(7);
	 URL_AccountList[0] = '';
	 URL_AccountList[1] = 'http://www.prudential.com/media/managed/htmlembed/pruonlinelogin_redirect.html?quadr=topright&name=insurancemfann';
	 URL_AccountList[2] = 'http://www.prudential.com/media/managed/pru_global_header_retirement_link.html';
	 URL_AccountList[3] = 'http://www.prudential.com/commandlogin';
	 URL_AccountList[4] = 'http://www.prudential.com/alliancelogin';
	 //URL_AccountList[5] = URL_AccountList[0];
	 URL_AccountList[5] = 'http://www.prudential.com/globallogin?quadr=topright&name=allloginoptions'
	 
	 var i = document.frmAccount.selAccount.selectedIndex;
	 document.frmAccount.selAccount.selectedIndex = 0;
	 
	 //if ((i!=0)&&(i!=5)) {
	if ((i!=0)) {	 
	  top.location.href = URL_AccountList[i];
	 }
}


//Open Calc Function, used in related link content (used on related link toolbox)
function open_calc_window(strLocation, strWinName) {
	rexp = /https:/
	if (rexp.test(strLocation)) {
		msgWindow = window.open(strLocation, null, 'height=500,width=500,location=no,scrollbars=yes,status=yes,menubars=no,toolbars=no,resizable=yes');
	} else {
		msgWindow = window.open(strLocation, null, 'height=500,width=500,location=no,scrollbars=yes,menubars=no,toolbars=no,resizable=yes');
	}
	msgWindow.opener = self;
}

//PHP Pages
function addbookmark() {
	var bookmarkurl = window.location;
	var bookmarktitle = "Prudential Web Site"
	if (document.all) window.external.AddFavorite(bookmarkurl, bookmarktitle)
}	

//GI PRODUCER SEARCH FUNCTION

function getOptionsId(objOpt, val) {
	var i;
	for (i = 0; i < objOpt.options.length; i++) if (objOpt.options[i].value == val) break;
	return i = (i == objOpt.options.length) ? objOpt.options.selectedIndex : i;
}

function TrimUsingRecursion(str) {
	if (str.charAt(0) == " ") {
		str = TrimUsingRecursion(str.substring(1));
	}
	if (str.charAt(str.length - 1) == " ") {
		str = TrimUsingRecursion(str.substring(0, str.length - 1));
	}
	return str;
}

function theSearch() {
	var stateKey = "";
	if (document.getElementById("state").value != "") {
		stateKey = "-keywords:" + document.getElementById("state").value;
	} else {
		alert("Please select a state.");
		if (document.getElementById("state")) document.getElementById('state').focus();
		return false;
	}
	var query = TrimUsingRecursion(document.getElementById('query').value);
	document.getElementById('query').value = query;
	var qt = document.getElementById("qt").value;
	var qtQ = query;
	if ((query == "") || (query == "Search")) {
		alert("Please enter a keyword or phrase in the text box.");
		document.getElementById('query').focus();
		return false;
	} else {
		document.getElementById("qt").value = qtQ;
		if (document.frmSearchForm2.qtype.value == 0) {
			document.getElementById('qt').value = stateKey + "  " + "\"" + query + "\"";
		} else {
			if (document.frmSearchForm2.qtype.value == 1) {
				qtQ = qtQ.replace(" ", "||");
				qtQ = stateKey + " " + qtQ;
				document.getElementById('qt').value = qtQ;
			} else {
				document.getElementById('qt').value = stateKey + " " + document.getElementById('qt').value;
			}
			document.frmSearchForm2.submit();
		}
	}
}

// function added by IBM to set focus on HTML Embed content as when user click on skip to main content link
function setFocusOn_HTMLEMBEDContent()
{
	if(document.getElementById('skipLinkJump'))
	{
		$("#skipLinkJump").focus();
	}
} 

// Global variable added to take advantage of iOS detection
var iOSdetected = 0;
var animationInProgress = 0;
$(document).ready(function () {
	


if 	((navigator.userAgent.toLowerCase().indexOf("ipad") > -1) || 
	(navigator.userAgent.toLowerCase().indexOf("ipod") > -1) ||
	(navigator.userAgent.toLowerCase().indexOf("iphone") > -1)){
		iOSdetected = 1;
	$('.navitem-new').removeClass('navitem-new-not-iOS');
}


/* APR Calculator Keyboard functionality - Code added by IBM Start */
$("#calapr").keypress(function(e){
			var code = e.keyCode || e.which;
			if(e.keyCode==13 || e.keyCode==32)//Enter/ space keycode 
			{  
				calculateAPR();
			}
	});
	
$("#resval").keypress(function(e){
			var code = e.keyCode || e.which;
			if(e.keyCode==13 || e.keyCode==32)//Enter/ space keycode 
			{  
				resetValues();
			}
	});	
	
//Added to fix accessibility issue
$("#selAccount").keypress(function(e){	
	if(e.keyCode==13 || e.keyCode==32)//Enter/ space keycode 
			{  
				gotoAccount();
			}
});
$("#selAccount").click(function(e){		
				gotoAccount();			
});

/* APR Calculator Keyboard functionality - Code added by IBM End */

/* code added by IBM for Skip to main content link. If Page has title then skip link will jump on page title and JAWS will start reading from there*/
if(document.getElementById("page-title"))
{
	var pgTitle_len = document.getElementById("page-title").innerHTML.length;
	var pgTitle_txt = document.getElementById("page-title").innerHTML;
	var flag = pgTitle_txt.indexOf("<!--");
	// change href value if there is no comment in page-title
	if(flag == -1)
	{
		$("a").each(function(){
			var val = $(this).attr('href');
			if(val=='#main')
			{
				$(this).attr('href', '#page-title');
			}
		});
	}
}

/* code added by IBM for Skip to main content link. If Page has HTML Embed file and don't have title(page-title) then skip link will jump on HTML Embed file content and JAWS will start reading from there. In this case, id='skipLinkJump' needs to be added in .html pages.*/
var templateType= $("body").attr("class");
if(templateType == 'HTMLEmbed' && document.getElementById("skipLinkJump"))
{
	$("a").each(function(){
		var val = $(this).attr('href');
		if(val=='#main')
		{
			$(this).attr('href', '#skipLinkJump');
			$(this).bind("click",setFocusOn_HTMLEMBEDContent);
		}
	});
}


	$('.navitem-new').keydown(function () {
		$('#solutionsIndividualsLink').removeClass('removeOutline');
		$('#solutionsBusinessesLink').removeClass('removeOutline');
		$('#indi-focus').removeClass('removeOutline');
		$('#busi-focus').removeClass('removeOutline');
		$('#productsServicesDiv').removeClass('removeOutline');
		$('#productsServicesLink').removeClass('removeOutline');
		$('#researchPerspecitvesLink').removeClass('removeOutline');
	});
	
	$('.products').keypress(function (e) {
		var code = e.keyCode || e.which;
		
		if(e.keyCode==13 || e.keyCode==32)//Enter/ space keycode 
		{  
			
			$('.products').click();
		}
	});

	$('.navitem-new a not:(.products)').click(function() {
		if ($(this).attr('href').length > 1 && page_id == 154) {
			var split_str = $(this).attr('href').split('name=');
			dcsMultiTrack("DCS.dcsuri","HP Top Nav: " + split_str[1], "DCSext.pruURI", "HP Top Nav: " + split_str[1]);
		}
	}); 
	// IBM: Event handler for keyboard navigation of sliding submenu by Vamsi and Bryan V.
	$('.closemenu').keypress(function (e) {
			var code = e.keyCode || e.which;
			if(e.keyCode==13 || e.keyCode==32)//Enter/ space keycode 
			{  	
				if (!e.shiftKey){
					e.preventDefault();
					$('.closemenu').click();
					$('.products').focus(); 
				}
			}
	});

	$('.navitem-hp').keydown(function () {
		$('#solutionsIndividualsLink').removeClass('removeOutline');
		$('#solutionsBusinessesLink').removeClass('removeOutline');
		$('#indi-focus').removeClass('removeOutline');
		$('#busi-focus').removeClass('removeOutline');
		$('#productsServicesDiv').removeClass('removeOutline');
		$('#productsServicesLink').removeClass('removeOutline');
		$('#researchPerspecitvesLink').removeClass('removeOutline');
	});
	
		$('.navitem-hp a not:(.products)').click(function() {
		if ($(this).attr('href').length > 1 && page_id == 154) {
			var split_str = $(this).attr('href').split('name=');
			dcsMultiTrack("DCS.dcsuri","HP Top Nav: " + split_str[1], "DCSext.pruURI", "HP Top Nav: " + split_str[1]);
		}
	}); 
	/* Code added for homepage by IBM -END*/
	
	//code to close slider menu with Esc key- Vinod
	if(!document.addEventListener) {
		document.attachEvent('onkeydown', function(event) {
		if (event.keyCode == 27) {
			menuclose();
			reinteriorborder();
			$('.products').focus();
			
		}
		}, true);
	}
	else 
	{
		document.addEventListener('keydown', function(event) {
			if (event.keyCode == 27) {
				menuclose();
				reinteriorborder();
				$('.products').focus();
			}
		}, true);
	}
	
	$('.primarycategories a.pcindividual').keypress(function (e) {
		if(e.keyCode==13 || e.keyCode==32)//Enter/ space keycode 
		{  

			$('.pcindividual').click();
		}
	});
	$('.primarycategories a.pcbusiness').keypress(function (e) {
		if(e.keyCode==13 || e.keyCode==32)//Enter/ space keycode 
		{  
			$('.pcbusiness').click();
		}
	});
	


	// HPQC: 2727
	if(document.getElementById("related-products-and-services"))
	{
		$("#related-products-and-services").attr('title', 'Related Products and Services');
	}
//------------------------------

//Move the banner from the floater if avail.
	$('#move-banner').appendTo($('#bannerholder'));
	
/*Added code to hide div if there is no banner inside it- IBM accessibility*/
$('#bannerholder').each(function(){
	var len = $(this).children().length;
	if(len<=0)
		{
			$(this).hide();
		}
	
});
	
	//Hide floater div if only holding temporary banner
	if($.trim($("#floater").html()).length==0)
  	$("#floater").remove(); 	
	
	//Search Box
	$('.gsa-term').focus(function () {
		if ($(this).val() == $(this)[0].title) {
			$(this).val("");
		}
	});
	$('.gsa-term').blur(function () {
		if ($(this).val() == "") {
			$(this).val($(this)[0].title);
		}
	});
	$('.gsa-term').blur();
	$(".gsa-go").click(function () {
		var str = document.gsa.term.value;
		if ((str == null || str.length == 0 || str == 'SEARCH')) {
			alert("Please enter a keyword or phrase in the text box.");
			document.gsa.term.focus();
			return false;
		} else {
			document.gsa.term.value = removeSpecialCharacters(document.gsa.term.value);
			document.gsa.as_q.value = document.gsa.term.value;
			document.gsa.submit();
		}
	});

	// Allow hitting return/enter to submit parent form.
	$("form#gsa input, form#gsa select").live('keypress', function (e) {
		if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
			var str = document.gsa.term.value;
			if ((str == null || str.length == 0 || str == 'SEARCH')) {
				alert("Please enter a keyword or phrase in the text box.");
				document.gsa.term.focus();
				return false;
			} else {
				document.gsa.term.value = removeSpecialCharacters(document.gsa.term.value);
				document.gsa.as_q.value = document.gsa.term.value;
				document.gsa.submit();
			}
			return false;
		} else {
			return true;
		}
	});

	// related-link-box
	$('.related-link-box').first().addClass('first');

	// related-link-box
	$('.product-info-group').first().addClass('first');

	//Content Border
	function reinteriorborder() {
		if ($('#interiorcontentwrapper').size() > 0) {
			$('#interiorcontentwrapper #contentholder').css('border-top', '1px solid #b7b7b7');
			$('#interiorcontentwrapper').css('backgroundPosition', '0 0');
		}
	}
	
	//Secondary Navigation Panel Functions
	function menuclose() {
		$("div.menupanel").slideUp('fast');



		$('.products').removeClass('productsActive a');

		$('.fullexposure .menusection').hide();
		$('.fullexposure .menusection:eq(0)').addClass('activems');
	}


    //Click event handler for any anchor that has an href that ends in top
	$("a[href='#top']").click(function() {
	  $("html, body").animate({ scrollTop: 0 }, "slow");
	  return false;
	});

	  
	$('.products').click(function () {
		if ($("div.menupanel").is(":hidden")) {
			$(this).addClass('productsActive a')

			$('.activems').show();
			$('.primarycategories').removeClass('pcbusactive').addClass('pcindactive');
			//IBM: Added script to show first tab as active every time '.products' is clicked
			if($('#solutionsBusinessesLink').hasClass('currenttab')){
				$('#solutionsBusinessesLink').removeClass('currenttab');
				$('#solutionsIndividualsLink').addClass('currenttab');
			}
			$("div.menupanel").slideDown();
			//IBM: pc-slider-focus added for Vamsi by Bryan V.
			if (iOSdetected == 0){
				$('.pc-slider-focus').focus();
			}else{
				$('.products').blur();
			}
			if ($('#interiorcontentwrapper').size() > 0) {
				$('#interiorcontentwrapper #contentholder').css('border-top', '0');
				$('#interiorcontentwrapper').css('backgroundPosition', '0 -1px');
			}
		} else {
			menuclose();
			reinteriorborder();
			if(iOSdetected == 1){
				$('.products').blur();
			}
		}
	});
	$('.closemenu').click(function () {
		if(iOSdetected == 0){
			$('.products').focus(); // IBM: Event handler for keyboard navigation of sliding submenu by Vamsi
		}
		menuclose();
		reinteriorborder();
	});
	// IBM: Event handlers for keyboard navigation of sliding submenu by Vamsi added by Bryan V.
	
	$('.closemenu').blur(function () {
		//$('.res-presp').focus();
		menuclose();
		reinteriorborder();
	});
	
	$('#solutionsIndividualsLink').keydown(function (e) {
		if ( e.keyCode == 9 || e.which == 9){
			if (e.shiftKey){
				e.preventDefault();
				$('.products').trigger('click');
				$('.products').focus();
			}
		}
	});

	$('#solutionsBusinessesLink').keydown(function (e) {
		if ( e.keyCode == 9 || e.which == 9){
			e.preventDefault();
			if (!e.shiftKey){
				$('.closemenu').click();
				$('.res-presp').focus();
			}else{
				$('#solutionsIndividualsLink').focus();
			}
		}
	});
	
	$('#indi-focus').keydown(function (e) {
		if(e.keyCode==9 && e.shiftKey) // Shift+Tab keycode
		{
			e.preventDefault();
			$('.pcindividual').focus();
		}
	});	
	$('.indi-lastlink').keydown(function (e) {
		if(e.keyCode==9 && !e.shiftKey) // Tab keycode
		{
			e.preventDefault();
			$('.pcbusiness').focus();
		}
	}); 
	$('.busi-lastlink').keydown(function (e) {
		if(e.keyCode==9 && !e.shiftKey) // Tab keycode
		{
			e.preventDefault();
			$('.closemenu').click();
			$('.res-presp').focus();
		}
	}); 
	// End IBM	
	
	
	$('.primarycategories a.pcindividual').click(function () {
		// This blocks clicks until animation has time to complete
		if (animationInProgress == 0){
			animationInProgress = 1;
			setTimeout(function(){
				animationInProgress = 0;
			}, 1000);
			if ($('.primarycategories').hasClass('pcbusactive')) {
				$('.primarycategories').removeClass('pcbusactive').addClass('pcindactive');
				
				// added for color contrast tabs
				$('#solutionsIndividualsLink').addClass('currenttab');
				$('#solutionsBusinessesLink').removeClass('currenttab');

				// added function to focus on sub-menu item when fade effect completes
				$('.fullexposure .menusection:eq(1)').hide().delay(800).siblings().fadeIn(800,function(){
					if (iOSdetected == 0){
						$('#indi-focus').focus();
					}
				});
			}else{
				if (iOSdetected == 0){
					$('#indi-focus').focus();
				}
			}
		}
	});
	$('.primarycategories a.pcbusiness').click(function () {
		// This blocks clicks until animation has time to complete
		if (animationInProgress == 0){
			animationInProgress = 1;
			setTimeout(function(){
				animationInProgress = 0;
			}, 1000);	
			if ($('.primarycategories').hasClass('pcindactive')) {
				$('.primarycategories').removeClass('pcindactive').addClass('pcbusactive');
				
				// added for color contrast tabs
				$('#solutionsIndividualsLink').removeClass('currenttab');
				$('#solutionsBusinessesLink').addClass('currenttab');

				// added function to focus on sub-menu item when fade effect completes
				$('.fullexposure .menusection:eq(0)').hide().delay(800).siblings().fadeIn(800,function(){
					if (iOSdetected == 0){
						$('#busi-focus').focus();
					}
				});
			}else{
				if (iOSdetected == 0){
					$('#busi-focus').focus();
				}
			}
		}
	});
	
	$('.navitem a').click(function() {
		if ($(this).attr('href').length > 1 && page_id == 154) {
			var split_str = $(this).attr('href').split('name=');
			dcsMultiTrack("DCS.dcsuri","HP Top Nav: " + split_str[1], "DCSext.pruURI", "HP Top Nav: " + split_str[1]);
		}
	}); 
	
	$('.fullexposure a').click(function() {
		if ($(this).attr('href').length > 1 && page_id == 154) {
			if($(this).parent().get(0).tagName== 'LI'){//lowest level link
				linkText = $(this).parent().text();
				navParentText = $(this).closest('div').children('p').text();
				primaryCategory = $('.pcindividual').parent().attr('class');
				primaryCategory = (primaryCategory.search('ind')>1 )? 'Solutions For Individuals':'Solutions For Businesses and Organizations';
				fullLinkDescription = 'Products & Services - ' + primaryCategory + ' - ' + navParentText + ' - ' + linkText;
				dcsMultiTrack("DCS.dcsuri","HP Top Nav: " + fullLinkDescription, "DCSext.pruURI", "HP Top Nav: " + fullLinkDescription);
			}
			if($(this).parent().get(0).tagName== 'P'){//lowest level HEADER link
				split_str = $(this).attr('href').split('name=');
				primaryCategory = $('.pcindividual').parent().attr('class');
				primaryCategory = (primaryCategory.search('ind')>1 )? 'Solutions For Individuals':'Solutions For Businesses and Organizations';
				fullLinkDescription = 'Products & Services - ' + primaryCategory + ' - ' + split_str[1];
				dcsMultiTrack("DCS.dcsuri","HP Top Nav: " + fullLinkDescription, "DCSext.pruURI", "HP Top Nav: " + fullLinkDescription);
			}
		}
	}); 
	
	$('.morespots p').hover(
	function () {
		$(this).addClass('phover');
		$('a', this).addClass('ahover');
		if ($(this).prev().size() > 0) {
			$(this).prev().addClass('phover');
		}
	}, function () {
		$(this).removeClass('phover');
		$('a', this).removeClass('ahover');
		if ($(this).prev().size() > 0) {
			$(this).prev().removeClass('phover');
		}
	});

	//PHP Associate List - Alternate Table Row Highlighting
	$(".phpAssociateList #associates tr:odd").addClass("odd"); //#C4D8E2 
	$(".phpAssociateList #associates tr:even").addClass("even"); //#FFF 	
	
	// Related Link hover arrow.
	$(".related-link-box ul li a").each(function () {
		$(this).append('<img class="h" src="../../../../media/system/cda/public/x.gif"/*tpa=http://www.prudential.com/media/system/cda/public/x.gif*/ height="7" width="6" alt="" />');
	});
	
	$('.related-link-box ul li a').mouseover(function () {
		$(this).find('img.h').remove();
		$(this).append('<img class="h" src="../../../../media/system/cda/public/bg-menuitemhover.png"/*tpa=http://www.prudential.com/media/system/cda/public/bg-menuitemhover.png*/ alt="" />');
	}).mouseout(function () {
		$(this).find('img.h').remove();
		$(this).append('<img class="h" src="../../../../media/system/cda/public/x.gif"/*tpa=http://www.prudential.com/media/system/cda/public/x.gif*/ height="7" width="6" alt="" />');
		});

	//Footer Metrics - WebTrends dcsMultiTrack
	if (document.domain.search('http://www.prudential.com/view/View/PUBLIC/js/prudential.com') != -1) {
		$('#linksectionwrapper a').click(function() {
			if ($(this).text().length > 1 && page_id == 154) dcsMultiTrack("DCS.dcsuri", "Ftr Link: " + encodeURIComponent($(this).text()) + " / " + page_id, "DCSext.pruURI", "Ftr Link: " + encodeURIComponent($(this).text()) + " / " + page_id)
		}); 
		$('#footerwrapper a').click(function() {
			if ($(this).text().length > 1) dcsMultiTrack("DCS.dcsuri", "Ftr Link: " + encodeURIComponent($(this).text()) + " / " + page_id, "DCSext.pruURI", "Ftr Link: " + encodeURIComponent($(this).text()) + " / " + page_id)
		});
		$('a.bringchallenges').click(function() {
			dcsMultiTrack("DCS.dcsuri", "Ftr Link: Prudential Bring Your Challenges Logo / " + page_id, "DCSext.pruURI", "Ftr Link: Prudential Bring Your Challenges Logo / " + page_id)
		}); 
		$('a.nav-metrics').click(function() {
			dcsMultiTrack("DCS.dcsuri", encodeURIComponent($(this).text()) + " / " + page_id + " - Header", "DCSext.pruURI", encodeURIComponent($(this).text()) + " / " + page_id + " - Header")
		}); 
						
		return true;
	}
	

	
	
});

