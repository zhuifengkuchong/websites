/* JavaScript
   Prudential eDG
   Public Site Specific JS
   Aug 2012 */
   
function createCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/; domain=.prudential.com";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}


function eraseCookie(name) {
	createCookie(name,"",-1);
}

function searchCookie(){
	var cookieName = 'PHP_LIC';
	var cookieVal = readCookie(cookieName);
	var ref = document.referrer;
	ref=ref.toLowerCase();
	if(cookieVal != null ){
 		dcsMultiTrack("DCS.dcsuri","PHP LI:" + document.URL, "DCSext.pruURI","PHP LI:" + document.URL);
	}else{
	if (ref.indexOf('http://www.prudential.com/view/View/PUBLIC/js/linkedin.com') != -1){
			var newDate = new Date();
			var hours = newDate.getHours();
			suffix = (hours >= 12)? 'PM' : 'AM'; 
			hours = (hours > 12)? hours -12 : hours; 
			var year = newDate.getFullYear();
			var minutes = newDate.getMinutes()<10? '0'+newDate.getMinutes() : newDate.getMinutes();
			var requiredDate = ((newDate.getMonth()+1)+'/'+(newDate.getDate())+'/'+year.toString().substring(2)+' '+hours+':'+minutes + ' '+suffix);
			this.eraseCookie(cookieName);
			this.createCookie(cookieName,requiredDate+"||NULL||NULL",100 * 365);
			dcsMultiTrack("DCS.dcsuri","PHP LI:" + document.URL, "DCSext.pruURI","PHP LI:" + document.URL)
	}
	}
}

/*
function searchCookieRFAAndQuote(){
	var cookieName = 'eRTSrc';
	var cookieVal = readCookie(cookieName);
	var urlVariable = buildURL();
	if(cookieVal != null ){
 		if(cookieVal == 'LI'){
			dcsMultiTrack("DCS.dcsuri","PHP LI:" + urlVariable, "DCSext.pruURI","PHP LI:" + urlVariable);
		}
	}
}


function buildURL(){
	var currentURL = document.URL;
	var CTRNValue = "";
	var OFFICEValue = "";
	queryString = currentURL.split('?')[1];
	if(queryString != null){
		queryStringVariables = queryString.split('&');
		for(index = 0;index<queryStringVariables.length;index++){
			if(queryStringVariables[index].split('=')[0] == 'CTRN'){
				CTRNValue = queryStringVariables[index].split('=')[1];
			}else if(queryStringVariables[index].split('=')[0] == 'OFFICE'){
				OFFICEValue = queryStringVariables[index].split('=')[1];
			}
		}	
	}
	
	return ((queryString!=null?currentURL.split('?')[0]:currentURL) + " - CTRN:" + CTRNValue + " OFFICE:" + OFFICEValue);
}
*/