/* ==========================================================================
   Project:     Stripes
   Date:        04/18/14
   Created by:  Third Wave Digital (www.thirdwavedigital.com)
   ========================================================================== */
  
 var stripesHomepage = {

     "init" : function(settings) {
        stripesHomepage.config = {

        }
        $.extend(stripesHomepage.config, settings);
        stripesHomepage.setup();
    },
    
    "setup" : function() {
        // Toggle tiles on diff. resolutions
        stripesHomepage.toggleTiles();   
    },
    
     "toggleTiles" : function() {
     	var $mobile_images = $("#mobile-view img");
     	var $desktop_images = $("#desktop-view img");
         if (utils.getWindowWidth() < 768) {
           $mobile_images.each(function(){
                $(this).attr("src", $(this).attr("data-src"));
            });
            //console.log("Mobile images enabled");
        } else {
            $desktop_images.each(function(){
                $(this).attr("src", $(this).attr("data-src"));
            });
            //console.log("Desktop images enabled");
        };
    }    
 };
 
$(document).ready(function() {
    
    stripesHomepage.init();
    
    $(window).resize(_.debounce(function(){
    	stripesHomepage.toggleTiles(); 
	}, 100));

});


