/* ==========================================================================
   Project:     Stripes
   Date:        04/18/14
   Created by:  Third Wave Digital (www.thirdwavedigital.com)
   ========================================================================== */
  
 var stripesMain = {

     "init" : function(settings) {
        stripesMain.config = {
            activeMenuIndex : $("header ul li.active").index()
        };
        $.extend(stripesMain.config, settings);
        stripesMain.setup();
    },
    
    "setup" : function() {
        // Manage the menus
        stripesMain.offcanvasMenu();
        stripesMain.mainMenu();
         // Open & close the submenu
        stripesMain.subMenu();
        // Resize the youtube embeds
        stripesMain.fixYoutubeEmbeds();
         // Load the main menu images if needed
        stripesMain.toggleMenuImages();
        // Close CMS ui on smaller screens
        stripesMain.toggleCMS();  
    },
    
    "offcanvasMenu" : function() {
        // Offcanvas menu
        if (utils.hasCSS3Transitions()) {
            // CSS3 Version
            $("header button").on("touchstart click", function(e){
              e.preventDefault();
              if (!$("html").hasClass("offcanvas-open")) {
                  $("html").removeClass("offcanvas-closed").addClass("offcanvas-open");
              } else {
                $("html").removeClass("offcanvas-open").addClass("offcanvas-closed");
                };
            });
        } else {
            // jQuery Version
            $("header button").on("touchstart click", function(e){
                e.preventDefault();
                  if (!$("html").hasClass("offcanvas-open-jquery")) {           
                      $(".wrapper").animate({right : "275px"},350);
                      $(".offcanvas").animate({right : "0px"},350);
                      $("html").addClass("offcanvas-open-jquery");
                  } else {        
                      $(".wrapper").animate({right : "0px"},350,function(){
                           $("html").removeClass("offcanvas-open-jquery"); 
                      });
                      $(".offcanvas").animate({right : "-280px"},350);
                  };
            });
        };  
        // Sub subs
        $(".offcanvas.navmenu-default .dropdown-menu a.dropdown-toggle").on("touchstart click", function(e){
        	e.preventDefault();
        	e.stopPropagation();
        	$(this).parent().toggleClass('open');
        });
    },
    
    "mainMenu" : function() {
        if (!utils.hasTouchSupport()) {  
  
       		// Enable hover with delay
	   		$("header ul li a[data-href]").hoverIntent({           
	            sensitivity: 7, 
	            interval: 50, 
	            over: function() {
	                var $this_dropdown = $($(this).attr("data-href"));
	                var $parent_li = $(this).closest("li");
	                 $("header ul li").not($parent_li).removeClass("active");
	                 $parent_li.addClass("active");
	                 $(".dropdown").removeClass("show");
	                 if (!$(".dropdown").hasClass("show")) {
	                     $this_dropdown.addClass("show");  
	                 };
	            },
	            out: function(){
	            
	            }
	        }); 
	        $("header ul>li:last-child").hover(function(){
	            closeDropdowns();
	        });         
	        $(".container-full.dropdown").mouseleave(function(){
	            closeDropdowns();
	        });
	         
	        // Quicklinks careers dropdown
	        $(".quicklinks ul li:has(ul)").hover(
			  function() {
			  	$(".quicklinks ul ul").removeClass("hidden");
			  	$(this).find("i").removeClass("fa-angle-down").addClass("fa-angle-up");
			  },function() {
			    $(".quicklinks ul ul").addClass("hidden");
			    $(this).find("i").removeClass("fa-angle-up").addClass("fa-angle-down");
			  }
			);
				
		} else {
	    	$("header ul li a[data-href]").on("click",function(e) {
		    	e.preventDefault();
       			e.stopPropagation();
       			var $this_dropdown = $($(this).attr("data-href"));
	            var $parent_li = $(this).closest("li");
	             $("header ul li").not($parent_li).removeClass("active");
	             $parent_li.addClass("active");
	             $(".dropdown").removeClass("show");
	             if (!$(".dropdown").hasClass("show")) {
	                 $this_dropdown.addClass("show");  
	             };
	    	});
	    	$(document).click(function(){
	            closeDropdowns();
	        });
	        
	      // Quicklinks careers dropdown
	        $(".quicklinks ul li:has(ul)").on("click",function(e) {
		       e.preventDefault(); 
		       $(".quicklinks ul ul").toggleClass("hidden");
		       if ($(".quicklinks").find("i").hasClass("fa-angle-down")) {
			       $(this).find("i").removeClass("fa-angle-down").addClass("fa-angle-up");
		       } else {
			       $(this).find("i").removeClass("fa-angle-up").addClass("fa-angle-down");
		       }
	        });  
	        
       	}
         
        function closeDropdowns() {           
            $("header ul li").removeClass("active");
            if (stripesMain.config.activeMenuIndex >=0) $("header ul li").eq(stripesMain.config.activeMenuIndex).addClass("active");
            $(".dropdown").removeClass("show");
            $(".quicklinks ul ul").addClass("hidden");
        }   
    },
    
     "subMenu" : function() {
        var $subMenu = $(".content .submenu");
        var $subMenuButton = $(".content .submenu-btn a");
        var original_text = $subMenuButton.html();
        $subMenuButton.on("click", function(e){
           e.preventDefault();
           e.stopPropagation();
           if (!$subMenu.hasClass("open")) {
               $subMenu.addClass("open");
               $(this).addClass("active");
               $(this).html("<i class='fa fa-times'></i> Close");
           } else {
               $subMenu.removeClass("open");
               $(this).removeClass("active");
               $(this).html(original_text);
           }
        });
        
        $(document).on("click",function(e) {
            $subMenu.removeClass("open");
            $subMenuButton.removeClass("active");
        });
    },
    
    "closeOffCanvasMenuOnResize" : function() {
    	var $html = $("html");
    	var $wrapper = $(".wrapper");
    	var $offcanvas = $(".offcanvas");
    	if (utils.getWindowWidth() > 767) {
            if (utils.hasCSS3Transitions() && $html.hasClass("offcanvas-open")) {
            	$html.removeClass("offcanvas-open").addClass("offcanvas-closed");
	        };
	        if ($html.hasClass("offcanvas-open-jquery")) {
	            $wrapper.animate({right : "0px"},300,function(){
	                $html.removeClass("offcanvas-open-jquery"); 
	            });
	            $offcanvas.animate({right : "-280px"},300);
	        }; 		        	
        };
    },    
    
    "toggleMenuImages" : function()  {
    	var $dropdown_img = $(".dropdown img");
        if (utils.getWindowWidth() < 768) {
           $dropdown_img.each(function(){
                $(this).attr("src","");
                $(this).addClass("hidden");
            });
            stripesMain.config.resizeEvent_toggleMenuImages = true;
            //console.log("Dropdown images disabled");   
        } else {
            $dropdown_img.each(function(){
                $(this).attr("src", $(this).attr("data-src"));
                $(this).removeClass("hidden");
            });
            stripesMain.config.resizeEvent_toggleMenuImages = false;
            //console.log("Dropdown images enabled");
        };  
    },   
       
    "fixYoutubeEmbeds" : function() {
        $("iframe[src*='http://www.youtube.com/embed/'],iframe[src*='//www.youtube.com/embed/']").wrap("<div class='video-container'/>");
    },
    
    "toggleCMS": function() {
    	var $da_toolbar = $("#da-toolbarWrapper");
    	var $da_table = $(".da-table");
    	var $body = $("body");
    	 
        if($da_toolbar.length) {
        	if(utils.getWindowWidth() < 820) {
        	    $body.css("padding-top","0px");
        	    $da_toolbar.addClass("hidden");
        	    $da_table.addClass("hidden");
        	} else {
        	    $body.css("padding-top","102px");
    			$da_toolbar.removeClass("hidden");
    			$da_table.removeClass("hidden");
			}
        }
    }
    
 };
 
$(document).ready(function() {
    
    stripesMain.init();
    
    $(window).resize(_.debounce(function(){
    	stripesMain.toggleCMS();
        stripesMain.closeOffCanvasMenuOnResize();
        stripesMain.toggleMenuImages();
	}, 100));
    
});


