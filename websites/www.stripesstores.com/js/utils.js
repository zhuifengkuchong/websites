/* ==========================================================================
   Project:     Stripes
   Date:        04/18/14
   Created by:  Third Wave Digital (www.thirdwavedigital.com)
   ========================================================================== */
  
var utils = {
    
    "init" : function(settings) {
        utils.config = {

        }
        $.extend(utils.config, settings);
        utils.setup();
    },
    
    "setup" : function() {
        utils.hasTouchSupport();
        utils.isMobile();
        utils.isAndroid();
        utils.isIOS();
        utils.hasCSS3Transitions();
    },
    
    "getWindowWidth" : function() {
        var width = Math.max($(window).innerWidth(),  window.innerWidth || document.documentElement.clientWidth);
        //console.log(width);
        return width;
    },
    
    "getWindowHeight" : function() {
        var height = Math.max($(window).innerHeight(),  window.innerHeight || document.documentElement.client);
        //console.log(height);
        return height
    },
    
    "isMobile" : function() {
        var check = false;
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
            $("html").addClass("mobile");
            check = true;
        };
        return check;
    },
    
    "isIOS" : function() {
        var check = false;
        if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
            $("html").addClass("ios");
            check = true; 
        };
        return check;
    },
    
    "isAndroid" : function() {
        var check = false;
        if (navigator.userAgent.match(/(Android)/)) {
            $("html").addClass("android");
            check = true;
        };
        return check;
    },
    
    "isOnScreen" : function(el) {
        var win = $(window),
        bounds = el.offset(),
        topoffset = el.height()/2,
        leftoffset = el.width(),
        viewport = {
            top : win.scrollTop()+topoffset,
            left : win.scrollLeft()+leftoffset
        };
        viewport.right = viewport.left + win.width();
        viewport.bottom = viewport.top + win.height();
        bounds.right = bounds.left + el.outerWidth();
        bounds.bottom = bounds.top + el.outerHeight();
        return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
    },
    
    "hasTouchSupport" : function() {
        var check = false;
        if ("ontouchstart" in document.documentElement || window.navigator.msMaxTouchPoints) {
            $("html").addClass("touch");
            check = true;
        };
        return check;
    },
    
    "hasCSS3Transitions" : function() {
        var check = false;
        var s = document.createElement('p').style;
        if ('transition' in s || 'WebkitTransition' in s || 'MozTransition' in s || 'msTransition' in s || 'OTransition' in s) {
            $("html").addClass("css3-transitions");
            check = true;
        }
        return check;
    },
    
    "equalHeights" : function(el) {
        var maxHeight = 0;
        el.each(function(){
            maxHeight = $(this).height() > maxHeight ? $(this).height() : maxHeight;
        }).height(maxHeight)
    },
    
    "truncateString" : function(str,n,useWordBoundary) {
        //http://stackoverflow.com/questions/1199352/smart-way-to-shorten-long-strings-with-javascript
         var toLong = str.length>n,
            s_ = toLong ? str.substr(0,n-1) : this;
            s_ = useWordBoundary && toLong ? s_.substr(0,s_.lastIndexOf(' ')) : s_;
            return  toLong ? s_ + ' ...' : s_;
     }
};

$(document).ready(function() {
    
    utils.init();
  
});


        



