<script id = "race16b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race16={};
	myVars.races.race16.varName="Lu_Id_li_23__onmouseover";
	myVars.races.race16.varType="@varType@";
	myVars.races.race16.repairType = "@RepairType";
	myVars.races.race16.event1={};
	myVars.races.race16.event2={};
	myVars.races.race16.event1.id = "Lu_Id_li_24";
	myVars.races.race16.event1.type = "onmouseover";
	myVars.races.race16.event1.loc = "Lu_Id_li_24_LOC";
	myVars.races.race16.event1.isRead = "True";
	myVars.races.race16.event1.eventType = "@event1EventType@";
	myVars.races.race16.event2.id = "Lu_Id_script_4";
	myVars.races.race16.event2.type = "Lu_Id_script_4__parsed";
	myVars.races.race16.event2.loc = "Lu_Id_script_4_LOC";
	myVars.races.race16.event2.isRead = "False";
	myVars.races.race16.event2.eventType = "@event2EventType@";
	myVars.races.race16.event1.executed= false;// true to disable, false to enable
	myVars.races.race16.event2.executed= false;// true to disable, false to enable
</script>

