<script id = "race58b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race58={};
	myVars.races.race58.varName="Lu_Id_li_23__onmouseout";
	myVars.races.race58.varType="@varType@";
	myVars.races.race58.repairType = "@RepairType";
	myVars.races.race58.event1={};
	myVars.races.race58.event2={};
	myVars.races.race58.event1.id = "Lu_Id_li_28";
	myVars.races.race58.event1.type = "onmouseout";
	myVars.races.race58.event1.loc = "Lu_Id_li_28_LOC";
	myVars.races.race58.event1.isRead = "True";
	myVars.races.race58.event1.eventType = "@event1EventType@";
	myVars.races.race58.event2.id = "Lu_Id_script_4";
	myVars.races.race58.event2.type = "Lu_Id_script_4__parsed";
	myVars.races.race58.event2.loc = "Lu_Id_script_4_LOC";
	myVars.races.race58.event2.isRead = "False";
	myVars.races.race58.event2.eventType = "@event2EventType@";
	myVars.races.race58.event1.executed= false;// true to disable, false to enable
	myVars.races.race58.event2.executed= false;// true to disable, false to enable
</script>

