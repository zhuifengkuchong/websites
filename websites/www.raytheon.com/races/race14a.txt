<script id = "race14a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race14={};
	myVars.races.race14.varName="Lu_Id_li_10__onmouseover";
	myVars.races.race14.varType="@varType@";
	myVars.races.race14.repairType = "@RepairType";
	myVars.races.race14.event1={};
	myVars.races.race14.event2={};
	myVars.races.race14.event1.id = "Lu_Id_script_4";
	myVars.races.race14.event1.type = "Lu_Id_script_4__parsed";
	myVars.races.race14.event1.loc = "Lu_Id_script_4_LOC";
	myVars.races.race14.event1.isRead = "False";
	myVars.races.race14.event1.eventType = "@event1EventType@";
	myVars.races.race14.event2.id = "Lu_Id_li_22";
	myVars.races.race14.event2.type = "onmouseover";
	myVars.races.race14.event2.loc = "Lu_Id_li_22_LOC";
	myVars.races.race14.event2.isRead = "True";
	myVars.races.race14.event2.eventType = "@event2EventType@";
	myVars.races.race14.event1.executed= false;// true to disable, false to enable
	myVars.races.race14.event2.executed= false;// true to disable, false to enable
</script>

