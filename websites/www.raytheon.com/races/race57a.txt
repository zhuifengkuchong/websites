<script id = "race57a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race57={};
	myVars.races.race57.varName="Lu_Id_li_23__onmouseout";
	myVars.races.race57.varType="@varType@";
	myVars.races.race57.repairType = "@RepairType";
	myVars.races.race57.event1={};
	myVars.races.race57.event2={};
	myVars.races.race57.event1.id = "Lu_Id_script_4";
	myVars.races.race57.event1.type = "Lu_Id_script_4__parsed";
	myVars.races.race57.event1.loc = "Lu_Id_script_4_LOC";
	myVars.races.race57.event1.isRead = "False";
	myVars.races.race57.event1.eventType = "@event1EventType@";
	myVars.races.race57.event2.id = "Lu_Id_li_27";
	myVars.races.race57.event2.type = "onmouseout";
	myVars.races.race57.event2.loc = "Lu_Id_li_27_LOC";
	myVars.races.race57.event2.isRead = "True";
	myVars.races.race57.event2.eventType = "@event2EventType@";
	myVars.races.race57.event1.executed= false;// true to disable, false to enable
	myVars.races.race57.event2.executed= false;// true to disable, false to enable
</script>

