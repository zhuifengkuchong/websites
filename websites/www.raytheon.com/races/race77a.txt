<script id = "race77a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race77={};
	myVars.races.race77.varName="hero__onmousedown";
	myVars.races.race77.varType="@varType@";
	myVars.races.race77.repairType = "@RepairType";
	myVars.races.race77.event1={};
	myVars.races.race77.event2={};
	myVars.races.race77.event1.id = "Lu_DOM";
	myVars.races.race77.event1.type = "onDOMContentLoaded";
	myVars.races.race77.event1.loc = "Lu_DOM_LOC";
	myVars.races.race77.event1.isRead = "False";
	myVars.races.race77.event1.eventType = "@event1EventType@";
	myVars.races.race77.event2.id = "hero";
	myVars.races.race77.event2.type = "onmousedown";
	myVars.races.race77.event2.loc = "hero_LOC";
	myVars.races.race77.event2.isRead = "True";
	myVars.races.race77.event2.eventType = "@event2EventType@";
	myVars.races.race77.event1.executed= false;// true to disable, false to enable
	myVars.races.race77.event2.executed= false;// true to disable, false to enable
</script>

