var $$FSR = {
  'enabled': true,
  'frames': false,
  'sessionreplay': true,
  'auto': true,
  'encode': true,
  'version': '18.1.8',
  'files': '/foresee/',
  // The 'swf_files' attribute needs to be set when foresee_transport.swf is not located at 'files'
  //'swf_files': '/some/other/location/'
  'id': 'qMDDR6ATsNA1qgTPjF1QFw==',
  'definition': 'foresee_surveydef.js'/*tpa=https://gateway.answerscloud.com/qualcomm/production/foresee/foresee_surveydef.js*/,
  'swf': {
    'fileName': 'foresee_transport.swf'/*tpa=https://gateway.answerscloud.com/qualcomm/production/foresee/foresee_transport.swf*/,
    'scriptAccess': 'always'
  },
  'worker': 'foresee_worker.js'/*tpa=https://gateway.answerscloud.com/qualcomm/production/foresee/foresee_worker.js*/,
  'embedded': false,
  'replay_id': 'https://gateway.answerscloud.com/qualcomm/production/foresee/qualcomm.com',
  'site_id': 'https://gateway.answerscloud.com/qualcomm/production/foresee/qualcomm.com',
  'attach': false,
  'renderer': 'W3C',
  // or "ASRECORDED"
  'layout': 'CENTERFIXED',
  // or "LEFTFIXED" or "LEFTSTRETCH" or "CENTERSTRETCH"
  'triggerDelay': 0,
  'heartbeat': true,
  'enableAMD': false,
  'pools': [{
    'path': '.',
    'sp': 100 // CHANGE ONLY WHEN INCLUDING SESSION REPLAY
  }],
  'sites': [{
    'path': /\w+-?\w+\.(com|org|edu|gov|net|co\.uk)/
  },
  {
    'path': '.',
    'domain': 'default'
  }],
  'storageOption': 'cookie',
  'nameBackup': window.name,
  'iframeHrefs': ["https://gateway.answerscloud.com/qualcomm/production/foresee/frameWorker.html"],
  'acceptableorigins': []
};

$$FSR.FSRCONFIG = {};

(function (config) {

  var FSR, supports_amd = !! config.enableAMD && typeof(_4c.global["define"]) === 'function' && !! _4c.global["define"]["amd"];

  if (!supports_amd) FSR = window.FSR;
  else FSR = {};
/*
 * ForeSee Survey Def(s)
 */
  FSR.surveydefs = [{
    name: 'mobile_web_eu',
    platform: 'phone',
    invite: {
      when: 'onentry',
      dialogs: [
        [{
          reverseButtons: false,
          headline: "We'd welcome your feedback!",
          blurb: "Can we email you later a brief customer satisfaction survey so we can improve your mobile experience?",
          attribution: "Conducted by ForeSee.",
          declineButton: "No, thanks",
          acceptButton: "Yes, I'll help",
          error: "Error"
        }],
        [{
          reverseButtons: false,
          headline: "Thank you for helping!",
          blurb: "Please provide your email address. After your visit we'll send you a link to the survey.",
          attribution: "ForeSee's <a class='fsrPrivacy' href='https://www.foresee.com/privacy-policy.shtml' target='_blank'>Privacy Policy</a>",
          declineButton: "Cancel",
          acceptButton: "email me",
          error: "Error",
          mobileExitDialog: {
            support: "e",
            //e for email only, s for sms only, b for both
            inputMessage: "email",
            emailMeButtonText: "email me",
            //textMeButtonText: "text me",
            fieldRequiredErrorText: "Enter an email address",
            invalidFormatErrorText: "Format should be: name@domain.com"
          }
        }]
      ]
    },
    pop: {
      when: 'later'
    },
    criteria: {
      sp: 0,
      //sp: 100,
      lf: 1
    },
    include: {
      urls: ['https://gateway.answerscloud.com/qualcomm/production/foresee/qualcomm.co.uk']
    }
  },
  {
    name: 'mobile_web_zh',
    platform: 'phone',
    invite: {
      when: 'onentry',
      dialogs: [
        [{
          reverseButtons: false,
          headline: "我们欢迎您提出反馈意见！",
          blurb: "以后我们可不可以用电子邮件向您发送一份简短的客户满意度调查问卷，以便我们改进您的网站浏览体验？",
          attribution: "由ForeSee主办。",
          declineButton: "不，谢谢",
          acceptButton: "是，我愿意参加",
          error: "错误"
        }],
        [{
          reverseButtons: false,
          headline: "谢谢您的帮助！",
          blurb: "请提供您的电子邮件地址。您的访问结束后，我们将向您发送这项调查问卷的链接。",
          attribution: "ForeSee's <a class='fsrPrivacy' href='https://www.foresee.com/privacy-policy.shtml' target='_blank'>的隐私政策</a>",
          declineButton: "取消",
          acceptButton: "发电子邮件给我",
          error: "错误",
          mobileExitDialog: {
            support: "e",
            //e for email only, s for sms only, b for both
            inputMessage: "电子邮件",
            emailMeButtonText: "发电子邮件给我",
            //textMeButtonText: "text me",
            fieldRequiredErrorText: "输入电子邮件地址",
            invalidFormatErrorText: "格式如下: name@domain.com"
          }
        }]
      ]
    },
    pop: {
      when: 'later'
    },
    criteria: {
      sp: 0,
      //sp: 100,
      lf: 1
    },
    include: {
      urls: ['https://gateway.answerscloud.com/qualcomm/production/foresee/qualcomm.cn']
    }
  },
  {
    name: 'mobile_web_en',
    platform: 'phone',
    invite: {
      when: 'onentry',
      dialogs: [
        [{
          reverseButtons: false,
          headline: "We'd welcome your feedback!",
          blurb: "Can we email or text you later a brief customer satisfaction survey so we can improve your mobile experience?",
          attribution: "Conducted by ForeSee.",
          declineButton: "No, thanks",
          acceptButton: "Yes, I'll help",
          error: "Error"
        }],
        [{
          reverseButtons: false,
          headline: "Thank you for helping!",
          blurb: "Please provide your email address or mobile number (US and CA only). After your visit we'll send you a link to the survey. Text Messaging rates apply.",
          attribution: "ForeSee's <a class='fsrPrivacy' href='https://www.foresee.com/privacy-policy.shtml' target='_blank'>Privacy Policy</a>",
          declineButton: "Cancel",
          acceptButton: "email/text me",
          error: "Error",
          mobileExitDialog: {
            support: "b",
            //e for email only, s for sms only, b for both
            inputMessage: "email or mobile number",
            emailMeButtonText: "email me",
            textMeButtonText: "text me",
            fieldRequiredErrorText: "Enter a mobile number or email address",
            invalidFormatErrorText: "Format should be: name@domain.com or 123-456-7890"
          }
        }]
      ]
    },
    pop: {
      when: 'later'
    },
    criteria: {
      sp: 100,
      lf: 1
    },
    include: {
      urls: ['.']
    }
  },
  {
    name: 'tablet_eu',
    platform: 'tablet',
    invite: {
      when: 'onentry',
      dialogs: [
        [{
          reverseButtons: false,
          headline: "We'd welcome your feedback!",
          blurb: "Can we email you later a brief customer satisfaction survey so we can improve your mobile experience?",
          attribution: "Conducted by ForeSee.",
          declineButton: "No, thanks",
          acceptButton: "Yes, I'll help",
          error: "Error"
        }],
        [{
          reverseButtons: false,
          headline: "Thank you for helping!",
          blurb: "Please provide your email address. After your visit we'll send you a link to the survey.",
          attribution: "ForeSee's <a class='fsrPrivacy' href='https://www.foresee.com/privacy-policy.shtml' target='_blank'>Privacy Policy</a>",
          declineButton: "Cancel",
          acceptButton: "email me",
          error: "Error",
          mobileExitDialog: {
            support: "e",
            //e for email only, s for sms only, b for both
            inputMessage: "email",
            emailMeButtonText: "email me",
            //textMeButtonText: "text me",
            fieldRequiredErrorText: "Enter an email address",
            invalidFormatErrorText: "Format should be: name@domain.com"
          }
        }]
      ]
    },
    pop: {
      when: 'later'
    },
    criteria: {
      sp: 0,
      //sp: 100,
      lf: 1
    },
    include: {
      urls: ['https://gateway.answerscloud.com/qualcomm/production/foresee/qualcomm.co.uk']
    }
  },
  {
    name: 'tablet_zh',
    platform: 'tablet',
    invite: {
      when: 'onentry',
      dialogs: [
        [{
          reverseButtons: false,
          headline: "我们欢迎您提出反馈意见！",
          blurb: "以后我们可不可以用电子邮件向您发送一份简短的客户满意度调查问卷，以便我们改进您的网站浏览体验？",
          attribution: "由ForeSee主办。",
          declineButton: "不，谢谢",
          acceptButton: "是，我愿意参加",
          error: "错误"
        }],
        [{
          reverseButtons: false,
          headline: "谢谢您的帮助！",
          blurb: "请提供您的电子邮件地址。您的访问结束后，我们将向您发送这项调查问卷的链接。",
          attribution: "ForeSee's <a class='fsrPrivacy' href='https://www.foresee.com/privacy-policy.shtml' target='_blank'>的隐私政策</a>",
          declineButton: "取消",
          acceptButton: "发电子邮件给我",
          error: "错误",
          mobileExitDialog: {
            support: "e",
            //e for email only, s for sms only, b for both
            inputMessage: "电子邮件",
            emailMeButtonText: "发电子邮件给我",
            //textMeButtonText: "text me",
            fieldRequiredErrorText: "输入电子邮件地址",
            invalidFormatErrorText: "格式如下: name@domain.com"
          }
        }]
      ]
    },
    pop: {
      when: 'later'
    },
    criteria: {
      sp: 0,
      //sp: 100,
      lf: 1
    },
    include: {
      urls: ['https://gateway.answerscloud.com/qualcomm/production/foresee/qualcomm.cn']
    }
  },
  {
    name: 'tablet_en',
    platform: 'tablet',
    invite: {
      when: 'onentry',
      dialogs: [
        [{
          reverseButtons: false,
          headline: "We'd welcome your feedback!",
          blurb: "Can we email or text you later a brief customer satisfaction survey so we can improve your mobile experience?",
          attribution: "Conducted by ForeSee.",
          declineButton: "No, thanks",
          acceptButton: "Yes, I'll help",
          error: "Error"
        }],
        [{
          reverseButtons: false,
          headline: "Thank you for helping!",
          blurb: "Please provide your email address or mobile number (US and CA only). After your visit we'll send you a link to the survey. Text Messaging rates apply.",
          attribution: "ForeSee's <a class='fsrPrivacy' href='https://www.foresee.com/privacy-policy.shtml' target='_blank'>Privacy Policy</a>",
          declineButton: "Cancel",
          acceptButton: "email/text me",
          error: "Error",
          mobileExitDialog: {
            support: "b",
            //e for email only, s for sms only, b for both
            inputMessage: "email or mobile number",
            emailMeButtonText: "email me",
            textMeButtonText: "text me",
            fieldRequiredErrorText: "Enter a mobile number or email address",
            invalidFormatErrorText: "Format should be: name@domain.com or 123-456-7890"
          }
        }]
      ]
    },
    pop: {
      when: 'later'
    },
    criteria: {
      sp: 100,
      lf: 1
    },
    include: {
      urls: ['.']
    }
  },
  {
    name: 'browse',
    platform: 'desktop',
    invite: {
      when: 'onentry'
    },
    pop: {
      when: 'later'
    },
    criteria: {
      sp: 100,
      lf: 2
    },
    locales: [{
      locale: 'eu',
      sp: 0,
      //sp: 100,
      lf: 2
    },
    {
      locale: 'cn',
      sp: 0,
      //sp: 100,
      lf: 2
    }],
    include: {
      urls: ['.']
    }
  }];


/*
 * ForeSee Properties
 */
  FSR.properties = {
    repeatdays: 90,

    repeatoverride: false,

    altcookie: {},

    language: {
      locale: 'en',
      src: 'location',
      locales: [{
        match: 'https://gateway.answerscloud.com/qualcomm/production/foresee/qualcomm.co.uk',
        locale: 'eu'
      },
      {
        match: 'https://gateway.answerscloud.com/qualcomm/production/foresee/qualcomm.cn',
        locale: 'cn'
      }]
    },

    exclude: {},

    ignoreWindowTopCheck: false,

    ipexclude: 'fsr$ip',

    mobileHeartbeat: {
      delay: 60,
      /*mobile on exit heartbeat delay seconds*/
      max: 3600 /*mobile on exit heartbeat max run time seconds*/
    },

    invite: {

      // For no site logo, comment this line:
      siteLogo: "sitelogo.gif"/*tpa=https://gateway.answerscloud.com/qualcomm/production/foresee/sitelogo.gif*/,

      //alt text fore site logo img
      siteLogoAlt: "",

      /* Desktop */
      dialogs: [
        [{
          reverseButtons: false,
          headline: "We'd welcome your feedback!",
          blurb: "Thank you for visiting Qualcomm.com. You have been selected to participate in a brief customer satisfaction survey to let us know how we can improve your experience.",
          noticeAboutSurvey: "The survey is designed to measure your entire experience, please look for it at the <u>conclusion</u> of your visit.",
          attribution: "This survey is conducted by an independent company ForeSee, on behalf of the site you are visiting.",
          closeInviteButtonText: "Click to close.",
          declineButton: "No, thanks",
          acceptButton: "Yes, I'll give feedback",
          error: "Error",
          warnLaunch: "this will launch a new window",

          locales: {
            "eu": {
              headline: "We'd welcome your feedback!",
              blurb: "Thank you for visiting qualcomm.co.uk. You have been selected to participate in a brief customer satisfaction survey to let us know how we can improve your experience.",
              noticeAboutSurvey: "The survey is designed to measure your entire experience, please look for it at the <u>conclusion</u> of your visit.",
              attribution: "This survey is conducted by an independent company ForeSee, on behalf of the site you are visiting.",
              closeInviteButtonText: "Click to close.",
              declineButton: "No, thanks",
              acceptButton: "Yes, I'll give feedback"
            },
            "cn": {
              headline: "我们欢迎您提出反馈意见！",
              blurb: "谢谢您造访我们的网站。您已被选择参加一项简短的客户满意度调查，以便让我们了解如何改进您的体验。",
              noticeAboutSurvey: "设计本项调查的目的是检测您的整个体验，请准备好在您访问结束时填写这份调查。",
              attribution: "本项调查由独立公司 ForeSee 代表您造访的网站举办。",
              closeInviteButtonText: "单击此处关闭。",
              declineButton: "不参加，谢谢",
              acceptButton: "是，我会提供反馈意见"
            }
          }
        }]
      ],

      exclude: {
        urls: ['/whywait', '/contact', '/signup', '/evaluation-kit', '/newsletter'],
        referrers: [],
        userAgents: [],
        browsers: [],
        cookies: [],
        variables: []
      },
      include: {
        local: ['.']
      },

      delay: 0,
      timeout: 0,

      hideOnClick: false,

      hideCloseButton: false,

      css: 'foresee_dhtml.css'/*tpa=https://gateway.answerscloud.com/qualcomm/production/foresee/foresee_dhtml.css*/,

      hide: [],

      hideFlash: false,

      type: 'dhtml',
      /* desktop */
      // url: 'https://gateway.answerscloud.com/qualcomm/production/foresee/invite.html'
      /* mobile */
      url: 'https://gateway.answerscloud.com/qualcomm/production/foresee/invite-mobile.html',
      back: 'url'

      //SurveyMutex: 'SurveyMutex'
    },

    tracker: {
      width: '690',
      height: '415',

      // Timeout is the normal between-page timeout
      timeout: 10,

      // Fast timeout is when we think there's a good chance we've closed the browser
      fasttimeout: 4,

      adjust: true,
      alert: {
        enabled: true,
        message: 'The survey is now available.',
        locales: [{
          locale: 'cn',
          message: '本项调查现在可供使用。 '
        }]
      },
      url: 'https://gateway.answerscloud.com/qualcomm/production/foresee/tracker.html',
      locales: [{
        locale: 'eu',
        url: 'https://gateway.answerscloud.com/qualcomm/production/foresee/tracker_eu.html',
        height: '435'
      },
      {
        locale: 'cn',
        url: 'https://gateway.answerscloud.com/qualcomm/production/foresee/tracker_cn.html',
        height: '435'
      }]
    },

    survey: {
      width: 690,
      height: 600
    },

    qualifier: {
      footer: '<div id=\"fsrcontainer\"><div style=\"float:left;width:80%;font-size:8pt;text-align:left;line-height:12px;\">This survey is conducted by an independent company ForeSee,<br>on behalf of the site you are visiting.</div><div style=\"float:right;font-size:8pt;\"><a target="_blank" title="Validate TRUSTe privacy certification" href="https://privacy-policy.truste.com/click-with-confidence/ctv/en/www.foreseeresults.com/seal_m"><img border=\"0\" src=\"{%baseHref%}truste.png\" alt=\"Validate TRUSTe Privacy Certification\"></a></div></div>',
      width: '690',
      height: '500',
      bgcolor: '#333',
      opacity: 0.7,
      x: 'center',
      y: 'center',
      delay: 0,
      buttons: {
        accept: 'Continue'
      },
      hideOnClick: false,
      css: 'foresee_dhtml.css'/*tpa=https://gateway.answerscloud.com/qualcomm/production/foresee/foresee_dhtml.css*/,
      url: 'https://gateway.answerscloud.com/qualcomm/production/foresee/qualifying.html'
    },

    cancel: {
      url: 'https://gateway.answerscloud.com/qualcomm/production/foresee/cancel.html',
      width: '690',
      height: '400'
    },

    pop: {
      what: 'survey',
      after: 'leaving-site',
      pu: false,
      tracker: true
    },

    meta: {
      referrer: true,
      terms: true,
      ref_url: true,
      url: true,
      url_params: false,
      user_agent: false,
      entry: false,
      entry_params: false
    },

    events: {
      enabled: true,
      id: true,
      codes: {
        purchase: 800,
        items: 801,
        dollars: 802,
        followup: 803,
        information: 804,
        content: 805
      },
      pd: 7,
      custom: {}
    },

    previous: false,

    analytics: {
      google_local: false,
      google_remote: false
    },

    cpps: {
      Viewed_Snapdragon_Content: { //this will be the name of the cpp
        source: 'url',
        init: 'No',
        patterns: [{
          regex: '/products/snapdragon/',
          value: 'Yes' //This will be the value sent 
        },
        {
          regex: '/news/snapdragon/',
          value: 'Yes'
        }]
      },
      Viewed_Products: {
        source: 'url',
        init: 'No',
        patterns: [{
          regex: '/products/',
          value: 'Yes'
        }]
      },
      Viewed_Invention: {
        source: 'url',
        init: 'No',
        patterns: [{
          regex: '/invention/',
          value: 'Yes'
        }]
      },
      Viewed_News: {
        source: 'url',
        init: 'No',
        patterns: [{
          regex: '/news/',
          value: 'Yes'
        }]
      },
      Viewed_Company: {
        source: 'url',
        init: 'No',
        patterns: [{
          regex: '/company/',
          value: 'Yes'
        }]
      },
      Viewed_Stories: {
        source: 'url',
        init: 'No',
        patterns: [{
          regex: '/invention/stories/',
          value: 'Yes'
        }]
      },
      Viewed_Research: {
        source: 'url',
        init: 'No',
        patterns: [{
          regex: '/invention/research/',
          value: 'Yes'
        }]
      },
      Viewed_ImpaQt: {
        source: 'url',
        init: 'No',
        patterns: [{
          regex: '/invention/impaqt/',
          value: 'Yes'
        }]
      },
      Viewed_Ventures: {
        source: 'url',
        init: 'No',
        patterns: [{
          regex: '/invention/ventures/',
          value: 'Yes'
        }]
      },
      Viewed_Licensing: {
        source: 'url',
        init: 'No',
        patterns: [{
          regex: '/invention/licensing/',
          value: 'Yes'
        }]
      },
      Viewed_Wireless_Technologies: {
        source: 'url',
        init: 'No',
        patterns: [{
          regex: '/invention/technologies/wireless/',
          value: 'Yes'
        }]
      },
      Viewed_Why_Wait_Campaign: {
        source: 'url',
        init: 'No',
        patterns: [{
          regex: '/whywait/',
          value: 'Yes'
        }]
      }
    },

    mode: 'first-party'
  };

  if (supports_amd) {
    define(function () {
      return FSR
    });
  }

})($$FSR);