<script id = "race6b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race6={};
	myVars.races.race6.varName="#document[0x7f9921bbc400].keydown";
	myVars.races.race6.varType="@varType@";
	myVars.races.race6.repairType = "@RepairType";
	myVars.races.race6.event1={};
	myVars.races.race6.event2={};
	myVars.races.race6.event1.id = "Lu_Id_input_1";
	myVars.races.race6.event1.type = "onkeydown";
	myVars.races.race6.event1.loc = "Lu_Id_input_1_LOC";
	myVars.races.race6.event1.isRead = "True";
	myVars.races.race6.event1.eventType = "@event1EventType@";
	myVars.races.race6.event2.id = "ie8message";
	myVars.races.race6.event2.type = "ie8message__parsed";
	myVars.races.race6.event2.loc = "ie8message_LOC";
	myVars.races.race6.event2.isRead = "False";
	myVars.races.race6.event2.eventType = "@event2EventType@";
	myVars.races.race6.event1.executed= false;// true to disable, false to enable
	myVars.races.race6.event2.executed= false;// true to disable, false to enable
</script>

