<script id = "race26a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race26={};
	myVars.races.race26.varName="Object[1432].triggered";
	myVars.races.race26.varType="@varType@";
	myVars.races.race26.repairType = "@RepairType";
	myVars.races.race26.event1={};
	myVars.races.race26.event2={};
	myVars.races.race26.event1.id = "Lu_Id_a_63";
	myVars.races.race26.event1.type = "onclick";
	myVars.races.race26.event1.loc = "Lu_Id_a_63_LOC";
	myVars.races.race26.event1.isRead = "False";
	myVars.races.race26.event1.eventType = "@event1EventType@";
	myVars.races.race26.event2.id = "fancybox-right";
	myVars.races.race26.event2.type = "onclick";
	myVars.races.race26.event2.loc = "fancybox-right_LOC";
	myVars.races.race26.event2.isRead = "True";
	myVars.races.race26.event2.eventType = "@event2EventType@";
	myVars.races.race26.event1.executed= false;// true to disable, false to enable
	myVars.races.race26.event2.executed= false;// true to disable, false to enable
</script>

