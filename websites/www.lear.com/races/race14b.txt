<script id = "race14b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race14={};
	myVars.races.race14.varName="Object[1432].triggered";
	myVars.races.race14.varType="@varType@";
	myVars.races.race14.repairType = "@RepairType";
	myVars.races.race14.event1={};
	myVars.races.race14.event2={};
	myVars.races.race14.event1.id = "Lu_Id_a_63";
	myVars.races.race14.event1.type = "onclick";
	myVars.races.race14.event1.loc = "Lu_Id_a_63_LOC";
	myVars.races.race14.event1.isRead = "False";
	myVars.races.race14.event1.eventType = "@event1EventType@";
	myVars.races.race14.event2.id = "carousel";
	myVars.races.race14.event2.type = "onmouseout";
	myVars.races.race14.event2.loc = "carousel_LOC";
	myVars.races.race14.event2.isRead = "True";
	myVars.races.race14.event2.eventType = "@event2EventType@";
	myVars.races.race14.event1.executed= false;// true to disable, false to enable
	myVars.races.race14.event2.executed= false;// true to disable, false to enable
</script>

