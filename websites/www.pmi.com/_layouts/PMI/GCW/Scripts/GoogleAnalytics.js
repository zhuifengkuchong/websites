
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-10855057-7']);
_gaq.push(['_setDomainName', 'http://www.pmi.com/_layouts/PMI/GCW/Scripts/www.pmi.com']);
_gaq.push(['_setAllowHash', false]);
_gaq.push(['_setAllowLinker', true]);
_gaq.push(['_trackPageview']);

function setGoogleAnalytics() {
    var ga = document.createElement('script');
    ga.type = 'text/javascript';
    ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www/') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(ga, s);
}

function callPageTracker() {
    try {
        if (googleAnalyticsAccount != null) {
            pageTracker = getTracker();
            pageTracker._setDomainName("none");
            pageTracker._setAllowHash(false);
            pageTracker._setAllowLinker(true);
            pageTracker._trackPageview();
        }
    }
    catch (err) {
        //alert("ex: " + err);
    }
}

function getTracker() {
    return _gat._createTracker(googleAnalyticsAccount);
}


function googleAnalyticsLink(link) {
    try {
        if (pageTracker != null) {
            pageTracker._trackPageview(link);
        }
    }
    catch (err) { }
}


//function setAnchorsOnClick() {
//    var anchors = document.getElementsByTagName("a");
//    for (var i = 0; i < anchors.length; i++) {
//        var anchor = anchors[i];
//        AddEvent(anchor, "click", testLink);
//        
//    }
//}

//function AddEvent(obj, functionName, functionCode) {
//    var currentUrl = location.href;
//    var siteUrl = location.protocol + "//" + location.hostname + ":" + location.port+"/";
//    var objHref = obj.href;
//    if ((objHref.length > 0 &&
//        objHref != siteUrl &&
//        objHref.indexOf("/_layouts/") == -1 &&
//        objHref.indexOf("javascript:") == -1 &&
//        objHref.indexOf(currentUrl) == -1)){
//        alert(objHref);
//        eval("var oldEvent = obj.on" + functionName + ";");
//        if (oldEvent != null) {

//            var additionalEvents = "obj.on" + functionName + " = function(e) { functionCode(obj); return oldEvent(e); };"
//            eval(additionalEvents);
//        } else {
//            eval("obj.on" + functionName + " = functionCode;");
//        }
//    }
//}

