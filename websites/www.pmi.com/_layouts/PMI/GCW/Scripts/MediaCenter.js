var HPMCImagetimer = '';
var HPMCImageParams;
var MCPRImageparams;
var MCSPLImageparams;

var HPMCCurrentImage = 1;
function HP_ImageSlider(params) {
    HPMCImageParams = params;
    HPMCImagetimer = window.setInterval(function () { HPMCMyTimer() }, 6000);
}

function MCPR_ImageLink(params) {
    MCPRImageparams = params;
}

function MCSPL_ImageLink(params) {
    MCSPLImageparams = params;
}


function HPMCImagePrevNext(opt) {
    clearInterval(HPMCImagetimer);
    if (opt <= 5) {
        HPMCCurrentImage = opt - 1;
        HPMCMyTimer();
        HPMCImagetimer = window.setInterval(function () { HPMCMyTimer() }, 6000);
    }
    else if (opt == 6) {
        clearInterval(HPMCImagetimer);
        (HPMCCurrentImage == 1) ? HPMCCurrentImage = HPMCImageParams.count - 1 : HPMCCurrentImage = HPMCCurrentImage - 2;
        HPMCMyTimer();
        HPMCImagetimer = window.setInterval(function () { HPMCMyTimer() }, 6000);
    }
    else {
        clearInterval(HPMCImagetimer);
        if (HPMCCurrentImage == HPMCImageParams.count) HPMCCurrentImage = 0;
        HPMCMyTimer();
        HPMCImagetimer = window.setInterval(function () { HPMCMyTimer() }, 6000);
    }
}

function HPMCMyTimer() {
    HPMCCurrentImage++;
    if (HPMCCurrentImage == HPMCImageParams.count + 1)
        HPMCCurrentImage = 1;
    var PagingUL = document.getElementById('ulSlider_nav');
    for (var i = 0; i < HPMCImageParams.count; i++) {
        document.getElementById('ulSlider_nav').childNodes[i].className = "";
    }
    document.getElementById('ulSlider_nav').childNodes[HPMCCurrentImage - 1].className = "active";

    if (HPMCCurrentImage == 1) {
        document.getElementById('imgHPMCImage').outerHTML = '<img style="cursor:pointer" id="imgHPMCImage" alt="' + HPMCImageParams.item1.altName + '" src="' + HPMCImageParams.item1.path + '" onclick="GoToImageLink(1)"/>';
        document.getElementById('divSlidetitle').innerHTML = HPMCImageParams.item1.ImageText;
    }
    else if (HPMCCurrentImage == 2) {
        document.getElementById('imgHPMCImage').outerHTML = '<img style="cursor:pointer" id="imgHPMCImage" alt="' + HPMCImageParams.item2.altName + '" src="' + HPMCImageParams.item2.path + '" onclick="GoToImageLink(2)"/>';
        document.getElementById('divSlidetitle').innerHTML = HPMCImageParams.item2.ImageText;
    }
    else if (HPMCCurrentImage == 3) {
        document.getElementById('imgHPMCImage').outerHTML = '<img style="cursor:pointer" id="imgHPMCImage" alt="' + HPMCImageParams.item3.altName + '" src="' + HPMCImageParams.item3.path + '" onclick="GoToImageLink(3)"/>';
        document.getElementById('divSlidetitle').innerHTML = HPMCImageParams.item3.ImageText;
    }
    else if (HPMCCurrentImage == 4) {
        document.getElementById('imgHPMCImage').outerHTML = '<img style="cursor:pointer" id="imgHPMCImage" alt="' + HPMCImageParams.item4.altName + '" src="' + HPMCImageParams.item4.path + '" onclick="GoToImageLink(4)"/>';
        document.getElementById('divSlidetitle').innerHTML = HPMCImageParams.item4.ImageText;
    }
    else {
        document.getElementById('imgHPMCImage').outerHTML = '<img style="cursor:pointer" id="imgHPMCImage" alt="' + HPMCImageParams.item5.altName + '" src="' + HPMCImageParams.item5.path + '" onclick="GoToImageLink(5)"/>';
        document.getElementById('divSlidetitle').innerHTML = HPMCImageParams.item5.ImageText;
    }


}

function GoToImageLink(ind) { 
    if (ind == 1)
        window.location.href = HPMCImageParams.item1.link == '' ? '#' : HPMCImageParams.item1.link;
    else if (ind == 2) {
        window.location.href= HPMCImageParams.item2.link == '' ? '#' : HPMCImageParams.item2.link;
    }
    else if (ind == 3) {
        window.location.href = HPMCImageParams.item3.link == '' ? '#' : HPMCImageParams.item3.link;
    }
    else if (ind == 4) {
        window.location.href= HPMCImageParams.item4.link == '' ? '#' : HPMCImageParams.item4.link;
    }
    else if (ind == 5) {
        window.location.href= HPMCImageParams.item5.link == '' ? '#' : HPMCImageParams.item5.link;
    }
}

function GoTopSPLImageLink(index) {
    var unit = index % 10
    var tens = parseInt(index / 10);
    var lnk = MCSPLImageparams[tens * 4 + unit - 1].link;
    if (lnk == '')
        lnk = '#';
    window.location.href(lnk);
}

function NavigateToImage(ind) {
    if (ind == 6)
        window.location.href(MCPRImageparams.item6.link == '' ? '#' : MCPRImageparams.item6.link);
    else if (ind == 7) {
        window.location.href(MCPRImageparams.item7.link == '' ? '#' : MCPRImageparams.item7.link);
    }
    else if (ind == 8) {
        window.location.href(MCPRImageparams.item8.link == '' ? '#' : MCPRImageparams.item8.link);
    }
}

jQuery(document).ready(function () {
    var rtregions = jQuery('.thumb_desc').length;
    var rtregions1 = jQuery('.thumb_desc');

    for (var i = 0; i < rtregions; i++) {
        var ss = jQuery('.thumb_desc')[i].children[0].innerHTML;
        if (ss.length == 0) {

            rtregions1[i].style.display = "none";
        }
    }

    var src = '';
    var index = 0;
    var filenameWithExtension = '';
    var filename = '';

    if (jQuery(".mediahighlight_box img").length == 3) {
        jQuery(".featured_sidebar").attr("style", "height:31%;width:100%;");
        jQuery(".featured_sidebar img").attr("style", "height:100%;width:100%;");
    }
    else if (jQuery(".mediahighlight_box img").length == 2) {
        jQuery(".featured_sidebar").attr("style", "height:50%;width:100%;");
        jQuery(".featured_sidebar img").attr("style", "height:100%;width:100%;");
    }

    src = document.URL;
    index = src.lastIndexOf("/") + 1;

    filenameWithExtension = src.substr(index);
    filename = filenameWithExtension.split(".")[0];

    if (filename == 'media_kit') {
        jQuery(".thumb_desc").each(function (index) {

            if (jQuery(this).children("h3").text().length > 18) {
                jQuery(this).children("a").attr('style', 'float:right; top:-17px')
            }
        });
    }
    else if (filename == 'media_center') {
        jQuery(".thumb_desc").each(function (index) {

            if (jQuery(this).children("h3").text().length > 16) {
                jQuery(this).children("a").attr('style', 'float:right; top:-24px')
            }
        });

        if (jQuery(".expand-collapse").length > 0) {
            jQuery(".mediahighlight_box").attr("style", "height:auto;overflow:none;");
        }
        else {
            jQuery(".mediahighlight_box").attr("style", "height:395px;overflow:hidden;");
        }

    }

}); 
