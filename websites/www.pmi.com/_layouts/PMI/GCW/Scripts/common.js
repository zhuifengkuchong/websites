// JavaScript Document
var TPIPage = false;
var EMAIL_PAGE_URL = "http://www.pmi.com/pages/send_to_friend.aspx"
var CURRENT_URL_PARAM = "url"

function resetTPIPage()
{
    if (TPIPage)
        resetcomponentsDDL();
}

// Modified by WrapTeam
function emailThisPage(_strCurrentPageUrl, secureUrl, pageToSendWeb,PageTitle) {
    var absoluteUrl = window.location.href.split('?')[0];
    var items = absoluteUrl.split('?')[0].split('/');
    var currPageName = items[items.length - 1];
    if (currPageName == "http://www.pmi.com/_layouts/PMI/GCW/Scripts/send_to_friend.aspx") 
    {
        window.location.href = secureUrl + strWebName + EMAIL_PAGE_URL + "?web=" + pageToSendWeb
            + "&pageTitle=" + PageTitle + "&" + CURRENT_URL_PARAM + "=" + encodeURIComponent(absoluteUrl);
    }
    else {
    // Modified by WrapTeam
    // Begin 
        if (strWebName.toLowerCase() == "/marketpages")
            strWebName = "/eng";
    // End
        _strCurrentPageUrl = encodeURIComponent(_strCurrentPageUrl);
        PageTitle = encodeURIComponent(PageTitle);
        window.location.href = secureUrl + strWebName + EMAIL_PAGE_URL + "?web=" + pageToSendWeb
            + "&pageTitle=" + PageTitle + "&" + CURRENT_URL_PARAM + "=" + _strCurrentPageUrl ;
    }
}

function insertCookie(cookieName, cookieValue) {

    var exdate = new Date();
    exdate.setDate(exdate.getDate() + 100);
    if (document.cookie) {
        document.cookie = cookieName + "=" + cookieValue + ";expires=" + exdate.toGMTString() + ";path=/";
    }
}
//VideoGallery Details and Single Image: verify if the inserted sequence number is included between 1 and 999
function CheckNumberValue(object) {
    var isText = new RegExp("[^0-9]");
    if (isText.exec(object.value) == null) {
        if (object.value == "") {
            return true;
        }
        if (object.value < 1 || object.value > 999) {
            alert("You must enter a value between 1 and 999");
            object.value = "";
            return false;
        }
    }
    else {
        alert("You must enter a value between 1 and 999");
        object.value = "";
    }
}

function setNavigationItem(containerID, itemID) {
    var container = document.getElementById(containerID);
    var ulMenus = container.getElementsByTagName("ul");

    if (ulMenus.length > 0) {
        var ulMenu = ulMenus[0];
        var menuItems = ulMenu.getElementsByTagName("li");
        for (var i = 0; i < menuItems.length; i++) {
            var menuItem = menuItems[i];
            var className = menuItem.className.toLowerCase();
            if (menuItem.id == itemID) {
                if (className.indexOf("active") == -1) {
                    menuItem.className = className + "active";
                }
            }
            else {
                if (className.indexOf("active") != -1) {
                    menuItem.className = className.replace("active", "");
                }
            }
        }
    }
}