// JavaScript Document

var More;
var Hide;

function buildFlash(divId, swfLoc, swfName, swfHeight, swfWidth, swfVar, swfVar2) {
    var flashVersion = 8;
    var so = new SWFObject(swfLoc, swfName, swfWidth, swfHeight, flashVersion, "#FFFFFF");
    so.addParam("wmode", "transparent");
    so.addParam("scale", "noscale");
    if (swfVar) {
        so.addVariable("flv", swfVar);
    }
    if (swfVar2) {
        so.addVariable("v2", swfVar2);
    }
    so.write(divId);
}

function buildBanner(divId, swfLoc, swfName, swfHeight, swfWidth, swfVar) {
    var flashVersion = 8;
    var so = new SWFObject(swfLoc, swfName, swfWidth, swfHeight, flashVersion, "#cccccc");
    so.addParam("wmode", "transparent");
    so.addParam("scale", "noscale");
    if (swfVar) {
        so.addVariable("xmlPath", swfVar);
    }
    so.write(divId);
}


function buildMap(divId, swfLoc, swfName, swfHeight, swfWidth, swfVar) {
    var flashVersion = 8;
    var so = new SWFObject(swfLoc, swfName, swfWidth, swfHeight, flashVersion, "#cccccc");
		so.addParam("wmode", "window");
    so.addParam("scale", "noscale");
    if (swfVar) {
        so.addVariable("xmlPath", swfVar);
    }
    so.write(divId);
}





function buildLargeFeature(divId, swfLoc, swfName, swfHeight, swfWidth, swfVar, swfVar2, swfVar3, swfVar4) {
    var flashVersion = 8;
    var so = new SWFObject(swfLoc, swfName, swfWidth, swfHeight, flashVersion, "#cccccc");
		so.addParam("wmode", "window");
    so.addParam("scale", "noscale");
    if (swfVar) {
        so.addVariable("lang", swfVar);
        so.addVariable("locale", swfVar2);
        so.addVariable("xmlPath", swfVar3);
        so.addVariable("swfDir", swfVar4);
    }
    so.write(divId);
}



var browser = navigator.appName;
var ver = navigator.appVersion;
var thestart = parseFloat(ver.indexOf("MSIE")) + 1;
var brow_ver = parseFloat(ver.substring(thestart + 4, thestart + 7));
var lightboxKiller;

if ((browser == "Microsoft Internet Explorer") && (brow_ver < 6)) {
    lightboxKiller = true;
} else {
    lightboxKiller = false;
}






function fixPNG(myImage) {

    if ((version >= 5.5) && (version < 7) && (document.body.filters)) {

        var imgID = (myImage.id) ? "id='" + myImage.id + "' " : ""
        var imgClass = (myImage.className) ? "class='" + myImage.className + "' " : ""
        var imgTitle = (myImage.title) ?
		             "title=\"" + myImage.title + "\" " : "title=\"" + myImage.alt + "\" "
        var imgStyle = "display:block; margin-top:0px;" + myImage.style.cssText
        var myParent = myImage.parentNode

        if (myParent.nodeName == "A") {
            var aHref = myParent.href;
            var aTarget = ""
            var aStyle = "style=\" display:block; width:" + myImage.width + "px; height:" + myImage.height + "px;\" "
            if (myParent.target !== "") {
                aTarget = "target=\"" + myParent.target + "\"";
            }
            var strNewHTML = "<span " + imgID + imgClass
				+ imgTitle + " style=\"" + "width:" + myImage.width
				+ "px; height:" + myImage.height
				+ "px;" + imgStyle + ";"
				+ "filter:progid:DXImageTransform.Microsoft.AlphaImageLoader"
				+ "(src=\'" + myImage.src + "\', sizingMethod='image');\">"
				+ "<a " + aStyle + "href=\"" + aHref + "\" target=\"" + aTarget + "\" ><!-- --></a> </span>"
            myParent.outerHTML = strNewHTML
        }
        else {
            var strNewHTML = "<span " + imgID + imgClass + imgTitle
				+ " style=\"" + "width:" + myImage.width
				+ "px; height:" + myImage.height
				+ "px;" + imgStyle + ";"
				+ "filter:progid:DXImageTransform.Microsoft.AlphaImageLoader"
				+ "(src=\'" + myImage.src + "\', sizingMethod='image');\"></span>"
            myImage.outerHTML = strNewHTML
        }

    }
}

var map = "";
var z = 100;
function fadein(article) {
    var k = document.getElementById(article).style;

    if (document.getElementById(article).className == "first" && k.zIndex < 98) {
        k.zIndex = 99;
    }

    var x = z - 1

    if (k.zIndex != x) {
        k.display = "none";

        k.zIndex = z;
        setTimeout("Effect.Appear('" + article + "', {duration: 1});", 150);

        var articlelink = document.getElementsByTagName('a');

        for (i = 0; i < articlelink.length; i++) {
            if (articlelink[i].id.match("select-")) {
                articlelink[i].parentNode.className = "";
            }
        }
        document.getElementById("select-" + article).parentNode.className = "active";
        z++;
    }
}

// Start Job Description Functions

function setJobs() {
    hideAllJobs('job-description', 'bottomLink', 'headerLink');
    var a = document.getElementById("job-search-results").getElementsByTagName("h3");
    for (i = 0; i < a.length; i++) {
        var str = a[i].firstChild.nodeValue;
        var lnk = "<a id=\"headerLink" + i + "\"  href='#headerLink" + i + "' onclick='showJob(" + i + ")'>" + str + "</a>";
        a[i].innerHTML = lnk;
    }
    var b = document.getElementById("job-search-results").getElementsByTagName("span");
    for (i = 0; i < b.length; i++) {
        var str2 = b[i].firstChild.nodeValue;
        var lnk2 = "<a id=\"bottomLink" + i + "\"  href='#headerLink" + i + "'  onclick='showJob(" + i + ")'>" + str2 + "</a>";
        b[i].innerHTML = lnk2;
    }
    jobPadding();
}
////Set faq p padding
function jobPadding(fid) {
    var a = document.getElementById("job-search-results").getElementsByTagName("div")
    for (i = 0; i < a.length; i++) {
        a[i].style.padding = "0 0 0 5px";
    }
}

var activeJob = ""
var activeJob2 = ""
//
function showJob(fid) {

    var link1 = "headerLink" + fid;
    var link2 = "bottomLink" + fid;
    if (activeJob == link1 || activeJob2 == link2) {
        hideAllJobs('job-description', 'bottomLink', 'headerLink');

        document.getElementById(link2).firstChild.nodeValue = More;

        activeJob = ""
        activeJob2 = ""

        var table = document.getElementById("job-search-results").getElementsByTagName("table")
        for (i = 0; i < table.length; i++) {
            table[i].style.margin = "0";
        }
    }
    else {
        hideAllJobs('job-description', 'bottomLink', 'headerLink');

        activeJob = link1;
        activeJob2 = link2;

        link1 = "job-description" + fid;
        link2 = "job-description" + fid;

        document.getElementById(activeJob).className = "open";
        document.getElementById(activeJob).style.margin = '0 0 30px 0';
        document.getElementById(activeJob2).firstChild.nodeValue = Hide;
        document.getElementById(activeJob2).className = "open";

        var table = document.getElementById("job-search-results").getElementsByTagName("table")
        for (i = 0; i < table.length; i++) {
            table[fid].style.margin = "0 0 15px 0";
        }

        show(link1);

    }
}
function hideAllJobs(divsId, BottomaId, HeaderaId) {

    var divs = document.getElementsByTagName('div');
    for (i = 0; i < divs.length; i++) {
        if (divs[i].id.match(divsId)) {
            divs[i].style.display = 'none';
        }
    }

    var Headeras = document.getElementsByTagName('a');
    for (i = 0; i < Headeras.length; i++) {
        if (Headeras[i].id.match(HeaderaId)) {
            Headeras[i].className = Headeras[i].className.replace(new RegExp("open\\b"), "");
        }
    }
    var Bottomas = document.getElementsByTagName('a');
    for (i = 0; i < Bottomas.length; i++) {
        if (Bottomas[i].id.match(BottomaId)) {
            Bottomas[i].className = Bottomas[i].className.replace(new RegExp("open\\b"), "");
            Bottomas[i].firstChild.nodeValue = More;
        }
    }

    var table = document.getElementById("job-search-results").getElementsByTagName("table")
    for (i = 0; i < table.length; i++) {
        table[i].style.margin = "0";
    }
}

// End Job Description Functions

// Start FAQ Description Functions

function setFaqs() {
    hideAllFAQ('faq-description', 'FAQbottomLink', 'FAQheaderLink');
    var a = document.getElementById("faq-list").getElementsByTagName("h3");
    for (i = 0; i < a.length; i++) {
        var str = a[i].firstChild.nodeValue;
        var lnk = "<a id=\"FAQheaderLink" + i + "\" href='javascript:showFaq(" + i + ")'>" + str + "</a>";
        a[i].innerHTML = lnk;
        //var aChild = a[i].firstChild.style.color = '#000000';
        //alert(aChild);
    }
    var b = document.getElementById("faq-list").getElementsByTagName("span");
    for (i = 0; i < b.length; i++) {
        var str2 = b[i].firstChild.nodeValue;
        var lnk2 = "<a id=\"FAQbottomLink" + i + "\"  href='javascript:showFaq(" + i + ")'>" + str2 + "</a>";
        b[i].innerHTML = lnk2;
    }
    faqPadding();
}
////Set faq p padding
function faqPadding(fid) {
    var a = document.getElementById("faq-list").getElementsByTagName("div")
    for (i = 0; i < a.length; i++) {
        //a[i].style.padding = "12px 0 0 0";
        //a[0].style.padding = "4px 0 0 0";
    }
}

var activeFaq = ""
var activeFaq2 = ""
//
function showFaq(fid) {
    var link1 = "FAQheaderLink" + fid;
    var link2 = "FAQbottomLink" + fid;
    if (activeFaq == link1 || activeFaq2 == link2) {
        hideAllFAQ('faq-description', 'FAQbottomLink', 'FAQheaderLink');

        activeFaq = ""
        activeFaq2 = ""

    }
    else {
        hideAllFAQ('faq-description', 'FAQbottomLink', 'FAQheaderLink');

        activeFaq = link1;
        activeFaq2 = link2;

        link1 = "faq-description" + fid;
        link2 = "faq-description" + fid;

        document.getElementById(activeFaq).className = "open";
        document.getElementById(activeFaq).style.margin = '0 0 30px 0';
        //document.getElementById(activeFaq2).firstChild.innerHTML = "";
        document.getElementById(activeFaq2).className = "open";

        show(link1);

    }
}
function hideAllFAQ(divsId, BottomaId, HeaderaId) {

    var divs = document.getElementsByTagName('div');
    for (i = 0; i < divs.length; i++) {
        if (divs[i].id.match(divsId)) {
            divs[i].style.display = 'none';
        }
    }

    var Headeras = document.getElementsByTagName('a');
    for (i = 0; i < Headeras.length; i++) {
        if (Headeras[i].id.match(HeaderaId)) {
            Headeras[i].className = Headeras[i].className.replace(new RegExp("open\\b"), "");
        }
    }
    var Bottomas = document.getElementsByTagName('a');
    for (i = 0; i < Bottomas.length; i++) {
        if (Bottomas[i].id.match(BottomaId)) {
            Bottomas[i].className = Bottomas[i].className.replace(new RegExp("open\\b"), "");
            //Bottomas[i].firstChild.innerHTML = "";
        }
    }
}

// End FAQ Description Functions

function setHover(string, string2) {
    document.getElementById(string).style.color = '#0065B1';
    document.getElementById(string2).style.color = '#0065B1';
}

function show(lyrId) {

    document.getElementById(lyrId).style.display = 'block';
    //document.getElementById(lyrId).style.padding='70px 0 0 0';
}



function newWindow(url, type) {
    if (url.length > 0) {
        var newwindow
        if (type == "popup") {
            var newwindow = window.open(url, 'pmipopup', 'height=650,width=1100,scrollbars=yes,location=yes,menubar=yes,resizable=yes,toolbar=yes');
        } else {
            newwindow = window.open(url);
        }
        if (window.focus) { newwindow.focus() }
    }
    return false;
}


function setCookie(c_name, value, expiredays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + expiredays);
    document.cookie = c_name + "=" + escape(value) + ";path=/" + ((expiredays == null) ? "" : ";expires=" + exdate.toGMTString());
}

function getCookie(c_name) {
    if (document.cookie.length > 0) {
        c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1) {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) c_end = document.cookie.length;
            return unescape(document.cookie.substring(c_start, c_end));
        }
    }
    return "";
}


// FONT SIZE CHANGER - COOKIE BASED
function changeFont(fontClass) {
    var HTMLBody = document.getElementsByTagName("body")[0];

    var footer = document.getElementById("main-footer-navigation");

    // Cookie popup's logic
    var cookiePanel = document.getElementById("lightbox-panel");
    var cookieSettings = document.getElementById("cookie-settings");
    var cookieConsent = document.getElementById("consentcookie");



    // modified by WRAP_DEV_TEAM
    var CurrentLang = getURLCurrentLanguage();
    if (CurrentLang == 'ara') {
        setCookie("PMIFontSizeArab", fontClass, 30);
    }
    else {
        setCookie("PMIFontSize", fontClass, 30);
    }
    HTMLBody.className = fontClass;
    cookiePanel.style.fontSize = "12px";
    cookieSettings.style.fontSize = "12px";
    cookieConsent.style.fontSize = "12px";
    if (footer != null)
    {
        if (fontClass == "") {
            footer.style.fontSize = "10px";
            footer.style.height = "188px";
        }
        else
            if (fontClass == "medium") {
                footer.style.fontSize = "12px";
                footer.style.height = "230px";
            }
            else {
                footer.style.fontSize = "15px";
                footer.style.height = "355px";
            }
    }
}



function setDefaultFontSize() {

    var HTMLBody = document.getElementsByTagName("body")[0];
    var CurrentLang = getURLCurrentLanguage();
    var footer = document.getElementById("main-footer-navigation");

    // Cookie popup's logic
    var cookiePanel = document.getElementById("lightbox-panel");
    var cookieSettings = document.getElementById("cookie-settings");
    var cookieConsent = document.getElementById("consentcookie");
    
    // modified by WRAP_DEV_TEAM
    // checks if the language is arabic
    if (CurrentLang == 'ara') {
        var fontSizeArab = getCookie("PMIFontSizeArab")
        if (!fontSizeArab) {
            setCookie("PMIFontSizeArab", 'medium', 30);
            HTMLBody.className = 'medium';
        }
        else {
            HTMLBody.className = fontSizeArab;
        }
    }
    else {
        var fontSize = getCookie("PMIFontSize")
        HTMLBody.className = fontSize;
        // cookiePanel.style.fontSize = "12px";
        // cookieSettings.style.fontSize = "12px";
        // cookieConsent.style.fontSize = "12px";
        if (footer != null)
        {
            if (fontSize == "") {
                footer.style.fontSize = "10px";
                footer.style.height = "188px";
            }
            else
                if (fontSize == "medium") {
                    footer.style.fontSize = "12px";
                    footer.style.height = "230px";
                }
                else {
                    footer.style.fontSize = "15px";
                    footer.style.height = "355px";
                }
        } 
    }
}

// Gets the current page language 
function getURLCurrentLanguage() {
    PageUrl = document.URL;
    UrlFields = PageUrl.split('/');
    CurrentLanguage = UrlFields[3];

    return CurrentLanguage;
}



// Function to remove buttons in header
//!!!!!The header.css is linked directly in the master page GCWMasterPage.master
//window.onload = function(){
//	
//	var HTMLhead = document.getElementsByTagName("head")[0];         
//	var headerCSS = document.createElement('link');
//	headerCSS.type = 'text/css';
//	headerCSS.rel = 'stylesheet';
//	headerCSS.href = 'Unknown_83_filename'/*tpa=http://www.pmi.com/_layouts/PMI/GCW/Scripts/_LAYOUTS/PMI/GCW/Style/header.css*/;
//	headerCSS.media = 'screen';
//	HTMLhead.appendChild(headerCSS);
//		
//	if ((browser=="Opera"))
//	{
//		var HTMLhead = document.getElementsByTagName("head")[0];         
//		var operaCSS = document.createElement('link');
//		operaCSS.type = 'text/css';
//		operaCSS.rel = 'stylesheet';
//		operaCSS.href = 'Unknown_83_filename'/*tpa=http://www.pmi.com/_layouts/PMI/GCW/Scripts/_LAYOUTS/PMI/GCW/Style/operaCSS.css*/;
//		operaCSS.media = 'screen';
//		HTMLhead.appendChild(operaCSS);
//	}
//	setDefaultFontSize();
//	autoClearInputs();
//}

function autoClearInputs() {
    var Inputs = document.getElementsByTagName('input');
    for (i = 0; i < Inputs.length; i++) {
        if (Inputs[i].value.length > 0 && Inputs[i].type == "text") {
            if (navigator.appName == "Microsoft Internet Explorer") {
                //IS doesnt like to set attributes in the normal way
                Inputs[i].setAttribute('rev', Inputs[i].value);
                Inputs[i].setAttribute('onfocus', function() { clearInput(this) });
                Inputs[i].setAttribute('onblur', function() { resetInput(this) });
            } else {
                Inputs[i].setAttribute("onfocus", "javascript:if(this.value=='" + Inputs[i].value + "')this.value='';")
                Inputs[i].setAttribute("onblur", "javascript:if(this.value=='')this.value='" + Inputs[i].value + "';")
            }
        }
    }
}


function clearInput(ele) {
    if (ele.value == ele.rev) {
        ele.value = "";
    }
}

function resetInput(ele) {
    if (ele.value == "") {
        ele.value = ele.rev;
    }
}


function setBrands() {
    var brandTextBox = document.getElementById("brand-architecture-text");
    var brandList = document.getElementById("brand-architecture").getElementsByTagName("li");
    for (i = 0; i < brandList.length; i++) {



        var brandTitle = brandList[i].childNodes[1].firstChild.nodeValue;
        var brandDetails = brandList[i].childNodes[2].firstChild.nodeValue;
        var brandImage = brandList[i].childNodes[0].innerHTML;
        var brandType = brandList[i].childNodes[0].firstChild.innerHTML;

        //alert(brandType);
        //alert(brandList[i].childNodes[1].firstChild.nodeValue);
        //alert(brandList[i].childNodes[2].firstChild.nodeValue);
        //alert(brandList[i].childNodes[0].innerHTML);

        brandList[i].childNodes[0].href = "javascript:doNothing();";
        //brandList[i].childNodes[0].removeAtrribute('href');
        //brandList[i].childNodes[0].onmouseover = "showText(this, \""+ brandTitle + "\" , \""+ brandDetails + "\")" ;
        brandList[i].childNodes[0].onmouseover = function() { showText(this) };
        if (i == 0) {

            brandTextBox.innerHTML = "<h3><em>" + brandTitle + "</em></h3> <p>" + brandDetails + "</p>";

        }
    }
}
function showText(param) {
    var brandTitle = param.parentNode.childNodes[1].firstChild.nodeValue;
    var brandDetails = param.parentNode.childNodes[2].firstChild.nodeValue;
    var brandImage = param.parentNode.childNodes[0].innerHTML;
    var brandType = param.parentNode.childNodes[0].firstChild.innerHTML;

    var brandTextBox = document.getElementById("brand-architecture-text");

    var brandList = document.getElementById("brand-architecture").getElementsByTagName("li");
    for (i = 0; i < brandList.length; i++) {
        brandList[i].firstChild.className = "";
        brandList[i].childNodes[0].firstChild.style.display = "none";
    }
    param.className = "active";
    param.parentNode.childNodes[0].firstChild.style.display = "block";

    brandTextBox.innerHTML = "<h3><em>" + brandTitle + "</em></h3> <p>" + brandDetails + "</p>";

}

function doNothing() {
}

function openHome(level, additional) {

    var urlPage = document.location.href;
    var splitted = urlPage.split("/");

    var urlBase = "";

    var count = 0;
    for (i = 0; i < splitted.length && i < level + 2; i++) {

        urlBase = urlBase + splitted[i] + "/";

    }

    window.open(urlBase + additional, "_self");


}

