// -----------------------------------------------------------------------------------
//
//	Navigationbox v1.03
//	by View Strategic PLC - http://www.view.uk.com
//	Last Modification: 11/03/2010
//
// -----------------------------------------------------------------------------------
/*

Table of Contents
-----------------
Configuration

Navigationbox Class Declaration
- initialize()
- updateNavigationList()
- end() 
- switchHover()
- killHover()
- resetActive()
    
Function Calls
- document.observe()
   
*/
// -----------------------------------------------------------------------------------

//
//  Configuration
//

var subNavOn = false; 	// bool to check if the second level nav is showing. that way we dont keep trying to switch it off  when it already off.

var tmpString = "";

NavigationboxOptions = Object.extend({

}, window.NavigationboxOptions || {});

// -----------------------------------------------------------------------------------

var Navigationbox = Class.create();

Navigationbox.prototype = {

    // initialize()
    // Constructor runs on completion of the DOM loading. Calls updateNavigationList and then
    // the function inserts html at the bottom of the page which is used to display the shadow 
    // overlay and the image container.
    //
    initialize: function() {
        try {
            this.updateNavigationList();
            $('container').observe('mouseleave', (function() { this.end(); }).bind(this))
            $('primary-navigation').observe('mouseleave', (function() { this.end(); }).bind(this))
            //mousenter 
            //mouseleave

        } catch (err) {
            // do nothing. we dont need to do this for anything other that IE anyway
        }
    },

    //
    // updateNavigationList()
    // Loops through anchor tags looking for 'navigationbox' references and applies onmouseover
    // events to appropriate links.
    //
    updateNavigationList: function() {
        this.updateNavigationList = Prototype.emptyFunction;
        document.observe('mouseover', (function(event) {
            var target = event.findElement('a[rel^=navigationbox]');
            var targethome = event.findElement('a[rel^=navigationhome]');

            if (target) {

                subNavOn = true;

                //(function() { Navigationbox.prototype.dome(target) }).delay(.25);
                this.switchHover(target);
                event.stop();
            }
            if (targethome) {
                this.switchHover(targethome);
                this.killHover(targethome);
                subNavOn = false;
            }
        }).bind(this));
    },

    //   dome: function(target){
    //		//alert(subNavOn);

    //		if (subNavOn)
    //		{
    //			this.switchHover(target);
    //		}
    //		
    //		//target.observe('mouseout', (function() { this.end(); }).bind(this))
    //		

    //   },

    //
    //  end()
    //	Closes navigationbox and sets everything back again
    //
    end: function() {

        if (subNavOn) {
            //alert('end');
            subNavOn = false;
            //alert(subNavOn);
            this.killHover();
            this.resetActive();
        }

    },




    showFlash: function() {

        //	try{
        //		//Mac safari doesnt like to chagne the visabliliy of the flahs on the homepage, so we do this
        //		document.getElementById('banner-home').innerHTML = tmpString;
        //	}catch(err){}

        //	var x = document.getElementsByTagName("script"); 
        //	for(var i=0;i<x.length;i++)
        //	{
        //		eval(x[i].text);
        //	}

        var flashObjects = document.getElementsByTagName("object");
        for (i = 0; i < flashObjects.length; i++) {
            flashObjects[i].style.visibility = "visible";
        }

        var flashEmbeds = document.getElementsByTagName("embed");
        for (i = 0; i < flashEmbeds.length; i++) {
            flashEmbeds[i].style.visibility = "visible";
        }
    },


    hideFlash: function() {

        //try{
        //	//Mac safari doesnt like to chagne the visabliliy of the flash on the homepage, so we do this
        //		tmpString = document.getElementById('banner-home').innerHTML;
        //		document.getElementById('banner-home').innerHTML = "";
        //	}catch(err){}



        var flashObjects = document.getElementsByTagName("object");

        for (i = 0; i < flashObjects.length; i++) {
            //flashObjects[i].style.visibility = "hidden";
            flashObjects[i].style.visibility = "hidden";
        }




        var flashEmbeds = document.getElementsByTagName("embed");
        for (i = 0; i < flashEmbeds.length; i++) {
            //flashEmbeds[i].style.visibility = "hidden";
            flashEmbeds[i].style.visibility = "hidden";
        }



    },




    //
    //  switchHover()
    //	de-activate all class="sethover" and class="active" on nav, and set this menu to class="sethover"
    //
    switchHover: function(anchor) {
        this.killHover();

        var HomeBanner = document.getElementById("banner-home");
        if (HomeBanner != null) {
            this.hideFlash();
        }

        if (anchor.parentNode.className.indexOf('sethover') < 0) {
            //alert('boo' + anchor.parentNode.className.indexOf('last'));			
            if (anchor.parentNode.className.indexOf('last') > -1) {
                anchor.parentNode.className += " lastsethover";

            }
            else {

                anchor.parentNode.className += " sethover";
                //alert('boo');
            }
            // Required for IE to grow the iframe when content is longer than standard.
            if (Prototype.Browser.IE) {
                if (navigator.appVersion.substr(22, 3) > "5.5") {
                    var ieULs = anchor.parentNode.getElementsByTagName('ul');
                    var ieIframes = anchor.parentNode.getElementsByTagName('iframe');
                    if (ieULs.length > 0 && ieIframes.length > 0) {
                        ieIframes[0].style.height = ieULs[0].offsetHeight + "px";
                    }
                } else {
                    var ieSelects = document.getElementById("main-content").getElementsByTagName('select');
                    if (ieSelects.length > 0) {
                        for (j = 0; j < ieSelects.length; j++) {
                            ieSelects[j].style.visibility = "hidden"
                        }
                    }
                }
            }
        }
    },

    //
    //  killHover()
    //	de-activate all class="sethover" and class="active" on nav
    //
    killHover: function() {

        var HomeBanner = document.getElementById("banner-home");
        if (HomeBanner != null) {
            this.showFlash();
        }


        var sfEls = document.getElementById("primary-navigation").getElementsByTagName("li");
        for (var i = 0; i < sfEls.length; i++) {
            //alert(sfEls[i].className);	
            if (sfEls[i].className != null) {
                sfEls[i].className = sfEls[i].className.replace(new RegExp("hide\\b"), "");
                sfEls[i].className = sfEls[i].className.replace(new RegExp("lastsethover\\b"), "");
                sfEls[i].className = sfEls[i].className.replace(new RegExp("sethover\\b"), "");
                sfEls[i].className = sfEls[i].className.replace(new RegExp("active\\b"), " hideactive");
            }
        }
    },





    //
    //  resetActive()
    //	de-activate all class="sethover"  on nav, and set this menu to class="sethover"
    //
    resetActive: function() {
        var sfEls = document.getElementById("primary-navigation").getElementsByTagName("li");
        for (var i = 0; i < sfEls.length; i++) {
            //alert(sfEls[i].className);	
            if (sfEls[i].className != null) {
                sfEls[i].className = sfEls[i].className.replace(new RegExp(" hideactive\\b"), " active");
            }
        }


        if (navigator.appVersion.substr(22, 3) < "6.0") {
            var ieSelects = document.getElementById("main-content").getElementsByTagName('select');
            if (ieSelects.length > 0) {
                for (j = 0; j < ieSelects.length; j++) {
                    ieSelects[j].style.visibility = "visible"
                }
            }
        }



    },

    setHover: function(nav) {
        try {
            var ieULs = nav.getElementsByTagName('ul');
            var ieIframes = nav.getElementsByTagName('iframe');
            if (navigator.appVersion.substr(22, 3) > "5.5" && navigator.appVersion.substr(22, 3) < "7.0" && Prototype.Browser.IE) {
                // IE script to cover <select> elements with <iframe>s
                if (ieIframes.length < 1) {
                    for (j = 0; j < ieULs.length; j++) {
                        var ieMat = document.createElement('iframe');
                        if (document.location.protocol == "https:")
                            ieMat.src = "Unknown_83_filename"/*tpa=http://0/*/;
                        else if (window.opera != "undefined")
                            ieMat.src = "";
                        else
                            ieMat.src = "javascript:false";
                        ieMat.scrolling = "no";
                        ieMat.frameBorder = "0";
                        ieMat.style.width = "940px";
                        ieMat.style.height = "285px";
                        ieMat.style.zIndex = "-1000000";
                        ieMat.style.position = "absolute";
                        ieMat.style.filter = "progid:DXImageTransform.Microsoft.Alpha(style=0,opacity=0)";
                        ieULs[j].insertBefore(ieMat, ieULs[j].childNodes[0]);
                        ieULs[j].style.zIndex = "10001";
                        ieULs[j].style.position = "absolute";
                    }
                }
            }
        }
        catch (err) {
            // do nothing. we dont need to do this for anything other that IE anyway
        }
    }
}


document.observe('dom:loaded', function() { new Navigationbox(); try { Navigationbox.prototype.setHover(document.getElementById('primary-navigation').childNodes[0]) } catch (err) { } });
//document.addEvent('domloaded', function ()) { new Navigationbox(); });


	 