function setCookies(name, value, path, expires, domain, secure) {
    var today = new Date();
    today.setTime(today.getTime());

    if (expires) {
        expires = expires * 1000 * 60 * 60 * 24;
    }
    var expires_date = new Date(today.getTime() + (expires));

    document.cookie = name + "=" + escape(value) +
		((expires) ? ";expires=" + expires_date.toGMTString() : "") +
		((path) ? ";path=" + path : "") +
		((domain) ? ";domain=" + domain : "") +
		((secure) ? ";secure" : "");
}

function getCookies(c_name) {
    var c_value = document.cookie;
    var c_start = c_value.indexOf(" " + c_name + "=");
    if (c_start == -1) {
        c_start = c_value.indexOf(c_name + "=");
    }
    if (c_start == -1) {
        c_value = null;
    }
    else {
        c_start = c_value.indexOf("=", c_start) + 1;
        var c_end = c_value.indexOf(";", c_start);
        if (c_end == -1) {
            c_end = c_value.length;
        }
        c_value = unescape(c_value.substring(c_start, c_end));
    }
    return c_value;
}

function checkCookie() {
    var panel = document.getElementById('lightbox-panel')

    if (getCookie("cookiePolicy") == "true") {
        panel.style.display = 'none';
    }
    else if (getCookie("session") == "true") {
        panel.style.display = 'none';
    }
    else {
        panel.style.display = 'inline';
    }
}

function displayDiv() {
    var lightbox = document.getElementById("lightbox-panel");
    var settings = document.getElementById("cookie-settings");
    if (settings.style.display == 'none' || settings.style.display == '') {
        settings.style.display = 'inline';
    }
    else {
        settings.style.display = 'none';
    }
}

function updatePolicyAnchor() {
    var url = window.location.href;
    var splitUrl = url.split("/", 3);
    var anchorToUpdate;
    if (splitUrl[0] != "http:") {
        anchorToUpdate = document.getElementById('policy');
        anchorToUpdate.setAttribute("href", "http://www.pmi.com/eng/pages/cookie_notice.aspx");
        anchorToUpdate = document.getElementById('cookiePolicySettings');
        anchorToUpdate.setAttribute("href", "http://www.pmi.com/eng/pages/cookie_notice.aspx");
    }
    var updatedUrl = splitUrl[0] + "//" + splitUrl[2] + "/eng/pages/cookie_notice.aspx";
    anchorToUpdate = document.getElementById('policy');
    anchorToUpdate.setAttribute("href", updatedUrl);
    anchorToUpdate = document.getElementById('cookiePolicySettings');
    anchorToUpdate.setAttribute("href", updatedUrl);
}

function displayDiv_RTL() {
    var lightbox = document.getElementById("lightbox-panel");
    var settings = document.getElementById("cookie-settings_RTL");
    if (settings.style.display == 'none' || settings.style.display == '') {
        settings.style.display = 'inline';
    }
    else {
        settings.style.display = 'none';
    }
}

function firefoxFix() {
    if (navigator.userAgent.indexOf("Firefox") !== -1 || navigator.userAgent.indexOf("Chrome") !== -1) {
        var panelP = document.getElementById("panelP");
        var consentButton = document.getElementById("consentbutton");
        consentButton.style.marginLeft = '-40px';
        panelP.style.marginTop = '-36px';
    }
}

function firefoxFix_RTL() {
    if (navigator.userAgent.indexOf("Firefox") !== -1 || navigator.userAgent.indexOf("Chrome") !== -1) {
        var consentButton = document.getElementById("consentbutton");
        consentButton.style.marginRight = '-40px';
    }
}