
window.$PmiWindow =
{
    _targetTextbox:null,

    setTargetTextbox: function(textbox) {
    
        this._targetTextbox = textbox;
    },

    getTargetTextbox: function() {
    
        return this._targetTextbox;
    },

    getRadWindow: function() {
        var oWindow;
        if (window.radWindow) oWindow = window.radWindow;
        else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
        return oWindow;
    }
};

// questa funzione e' stata spostata in FileExplorer.aspx
//Telerik.Web.UI.RadFileExplorer.prototype._onGridRowDataBound = function(grid, args) {
//    var dataItem = args.get_dataItem();
//    var itemName = dataItem["Name"];
//    //keeps the extension of item, that will identify the CSS class
//    var size = dataItem["Length"];

//    var cssClass;
//    if (itemName == "Pages" || itemName == "PublishingImages" || itemName == "Documents") {
//        cssClass = "rfeFileExtension folder", extension = null;
//    }
//    else {
//        cssClass = "rfeSite prova", extension = null;
//    }
//    //if the item has a length property, it's a file
//    if (size == null) {
//        size = "&nbsp;";
//    }
//    else {
//        //Use a faster approach
//        //var array = itemName.split(".");
//        //extension = array[array.length - 1];
//        extension = itemName.substr(itemName.lastIndexOf(".") + 1);

//        extension = extension.toLowerCase();
//        cssClass = "rfeFileExtension " + extension;
//    }

//    //create FileExplorerItem object
//    var itemProperties = { "type": (size == "&nbsp;" ? Telerik.Web.UI.FileExplorerItemType.Directory : Telerik.Web.UI.FileExplorerItemType.File),
//        "permissions": dataItem["Permissions"],
//        "name": itemName,
//        "path": this._getGridDataItemPath(dataItem),
//        "extension": extension,
//        "size": parseInt(size),
//        "url": dataItem["Url"]
//    };
//    Array.add(this._gridData, $create(Telerik.Web.UI.FileExplorerItem, itemProperties, null, null, null));

//    var tr = args.get_item("Name").get_element();

//    //HACK - IE has a problem with showing the background of the TR when the TD has a background image set
//    //This can be seen when a file explorer row is selected. Using a second element was the only possible workaround
//    var div = document.createElement("DIV");
//    div.className = cssClass;
//    div.innerHTML = itemName;
//    //make element unselectable
//    div.setAttribute("unselectable", "on");
//    //Add div to the cell when all is finished, not prior to this - it helps optimize a bit
//    var cell1 = tr.cells[0];
//    cell1.innerHTML = "";
//    cell1.setAttribute("unselectable", "on");
//    cell1.appendChild(div);

//    var cell2 = tr.cells[1];
//    cell2.setAttribute("unselectable", "on");
//    cell2.innerHTML = size;

//    //selection
//    if (this._selectedFile) {
//        if (itemName == this._selectedFile) {
//            this._selectedFile = null;
//            this._selectedRow = args.get_item();
//        }
//    }
//};

function OnFileSelected(wnd, fileSelected) {

    var textbox = $PmiWindow.getTargetTextbox();
    textbox.value = fileSelected;
}

function CreateTargetUrl(currentUrl, libraries, currentWeb) {
    
    if (currentUrl.toString().indexOf("?", 0) > -1) {
        currentUrl = currentUrl.toString().substring(0, currentUrl.toString().indexOf("?", 0));

    }

    currentUrl += "?libr=" + libraries+"&web="+currentWeb;

    return currentUrl;
}

jQuery.noConflict();