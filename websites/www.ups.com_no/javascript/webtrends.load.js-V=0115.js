// WebTrends SmartSource Data Collector Tag v10.4.1
// Copyright (c) 2014 Webtrends Inc.  All rights reserved.
// Tag Builder Version: 4.1.3.2
// Tag Created on: 2014.10.16
// Tag Update on 12/11/2014 by Virginia Carcavallo-Orr with Webtrends Professional Services. Changes made: Added Transform (lines 67-98) to enable multitrack events to collect only the parameters passed in the multitrack call without inheriting the meta tags in the page or the previous multitrack call parameters when the parameters in the multitrack are the same as the meta tags in the page.
// Tag Update on 12/12/2014 by Virginia Carcavallo-Orr with Webtrends Professional Services. Changes made: Added a sub function to the tag to store any of the multitrack events that get loaded before the tag is loaded (lines 9-20) and fire them after the tag loads (lines 118-121).
// Tag Update on 12/30/2014 by Virginia Carcavallo-Orr with Webtrends Professional Services. Changes made: Added the global variables for parameters WT, DCS and DCSext (lines 118-127).
// Tag Update on 12/31/2014 by Virginia Carcavallo-Orr with Webtrends Professional Services. Changes made: Set internationalization statement off (line 28).
// Tag Update on 04/01/2015 by Virginia Carcavallo-Orr with Webtrends Professional Services. Change made: adjusted the DCSext.ppid for the opinion lab tracking - Line 129


// pre queue any dcsMultitracks that come in before the tag fully loads
window.wtPreQueue = [];
function dcsMultiTrack() {
    try {
        var args = [];
        for (var c = 0; c < arguments.length; c++) {
            args.push(arguments[c]);
        }
        if (args.length > 0) window.wtPreQueue.push(args);
    } catch (e) {
    }
};

window.webtrendsAsyncInit = function () {
    var dcs = new Webtrends.dcs().init({
        dcsid: "dcslnrz6vne3g9s37bjvj8khc_2v8x",
		domain:"http://www.ups.com/javascript/ssdcwebtrends.ups.com",
        timezone: -5,
        i18n: false,
        offsite: true,
        download: true,
        downloadtypes: "xls,doc,pdf,txt,csv,zip,docx,xlsx,rar,gzip",
        anchor: true,
        javascript: true,
        onsitedoms: new RegExp("http://www.ups.com/javascript/ups.com"),
        fpcdom: ".ups.com",
        plugins: {
            //hm:{src:"../../s.webtrends.com/js/webtrends.hm.js"/*tpa=http://s.webtrends.com/js/webtrends.hm.js*/}
        }
    });
    dcs.addTransform(function (d, o) {
        try {
            var upsDocumentElements;
            if (document.all) {
                upsDocumentElements = document.all.tags("meta");
            }
            else if (document.documentElement) {
                upsDocumentElements = document.getElementsByTagName("meta");
            }
           if (typeof(upsDocumentElements) != "undefined") {
                for (var i = 1, j = 100; i <= upsDocumentElements.length && j > 0; i++, j--) {
                    upsMeta = upsDocumentElements.item(i - 1);
                    if (upsMeta.name) {
                        if (upsMeta.name.indexOf('APPVARS.') == 0) {
                            var n = upsMeta.name.split('.');
                            // put on the top of the stack so multitracks can over-write if neded
                            o.argsa.unshift('DCSext.' + n.pop(), upsMeta.content);

                        }
                    }
                }
            }

            o.argsa.unshift('DCSext.WBPM_ver',"http://www.ups.com/javascript/1.0.12");
            o.argsa.unshift('DCSext.WBPM_ac',"0");

        } catch (e) {
        }
    }, "all");
    dcs.addTransform(function (d, o) {
        try {
            o.argsa.push('DCSext.WBPM_ac', 1);

            // look for gApps.WBPM_ce
            for (var n = 0 ; n < o.argsa.length;n+=2) {
                if (!o.argsa[n].indexOf('gApps.')) {
                    o.argsa[n] = 'DCSext.' + o.argsa[n].split('.')[1]
                }
            }
        } catch (e) {
        }
    }, "multitrack");

	
	dcs.addTransform(function (d, o) {
        // normalize multitrack case data

        // first run through the meta that's in the DOM object
        try {
            for (var p = 0; p < o.argsa.length; p += 2) {
                if (o.argsa[p].split('.').length > 1) {
                    var root = o.argsa[p].split('.')[0];
                    var parm = o.argsa[p].split('.')[1];
                    for (r in d[root]) {
                        if (r.toUpperCase() == parm.toUpperCase()) {
                            o.argsa[p] = root + '.' + r;
                        }
                    }
                }
            }

            // run the stack just to be save
            for (var p = 0; p < o.argsa.length; p += 2) {
                if (o.argsa[p].split('.').length > 1) {
                    var parm = o.argsa[p].split('.')[1];
                    for (var r = 0; r < o.argsa.length; r += 2) {
                        if (o.argsa[r].split('.').length > 1 && o.argsa[r].split('.')[1].toUpperCase() == parm.toUpperCase()) {
                            o.argsa[p] = o.argsa[r];
                        }

                    }
                }
            }
        } catch (err) {
        }
    }, "multitrack");

    dcs.track();
	//Start --- Remove when these variables are hard coded using the latest code version
	var dCurrent = new Date();
	window.DCS = {};
	window.DCSext = {};
	window.WT = {};
	window.DCS.dcsdat=dCurrent.getTime();
	window.DCS.dcssip=window.location.hostname;
	window.DCS.dcsuri=window.location.pathname;
	window.DCS.dcsqry=window.location.search;
	if (typeof dcs.DCSext.ppid != 'undefined') window.DCSext.pPID = dcs.DCSext.ppid;
	
	//End --- Remove when these variables are hard coded using the latest code version
	
// if there were any dcsMultitracks before the tag loaded send them now
    while (window.wtPreQueue.length > 0) {
        Webtrends.multiTrack({argsa: window.wtPreQueue.shift()});
    }
	

};
(function () {
    var s = document.createElement("script");
    s.async = true;
    s.src = "webtrends.js"/*tpa=http://www.ups.com/javascript/webtrends.js*/;
    var s2 = document.getElementsByTagName("script")[0];
    s2.parentNode.insertBefore(s, s2);
}());
