// Copyright ? 2003-2005 United Parcel Service of America, Inc. All rights reserved.
var popWin,popWinType,ie,styles;

ie = (document.all) ? true : false;

styles="<link rel=\"stylesheet\" type=\"text/css\" href=\"../stylesheets/globalhomepage.css\"/*tpa=http://www.ups.com/stylesheets/globalhomepage.css*//>";

function writeStyles(){
        document.write(styles);
}

writeStyles()

/* -----------------------------------------------
FUNCTION: helpModLvl
DESC: Create a module level pop up window
ARGS:
        hrefTarget - url to be opened
-------------------------------------------------*/
function helpModLvl(hrefTarget) {
        //Set Variables
        var name, width, height;
        name = "helpModLvl";
        width="463";
        height="500";
        // Pop up the window
        popWindow(hrefTarget,name,width,height,resizable="yes");
}

/* -----------------------------------------------
FUNCTION: popWindow
DESC: Opens a window and positions it on the screen

ARGS:
        hrefTarget - url to be opened
        name - name of popup window (becomes popupType)
        width - width of the pop up window
        height - height of the pop up window
-------------------------------------------------*/
function popWindow(hrefTarget,name,width,height) {
// Create offset
        if (document.all) {
                xMax = screen.width, yMax = screen.height;
        } else {
                if (document.layers) {
                        xMax = window.outerWidth, yMax = window.outerHeight;
                } else {
                        xMax = 640, yMax=480;
                }
        }
        var xOffset = (xMax - 586)/2, yOffset = (yMax - 700)/2;
//Check if pop up exists
        if (!popWin||popWin.closed) {
                // no pop up exists set the popWinType to the current type
                popWinType = name;
                // open pop up window
                popWin = window.open(hrefTarget,name,'width='+width+',height='+height+',screenX='+xOffset+',screenY='+yOffset+
', top='+yOffset+',left='+xOffset+',scrollbars=yes,menubar=yes,resizable=yes');
        } else {
                // Check if the pop up is the same type
                if (popWinType != name) {
                // it is not the same type so close the current pop up
                popWin.close();
                // set the popWinType to the current type
                popWinType = name;
                // open pop up window
                popWin = window.open(hrefTarget,name,'width='+width+',height='+height+',screenX='+xOffset+',screenY='+yOffset+
', top='+yOffset+',left='+xOffset+',scrollbars=yes,menubar=yes,resizable=yes');
                return;
                }
                // Bring pop up to the foreground
                popWin.focus();
                // Change the location of the pop up to the location being requested
                popWin.location = hrefTarget;
        }
}

/* -----------------------------------------------
FUNCTION: popWrapper
DESC: Wrapper function takes in a url and a popup type
ARGS:
        hrefTarget - url to be opened
        popType - type of popup to use
-------------------------------------------------*/
function popWrapper(hrefTarget,popType) {
        if ((popType == "pop_up_2_col"))
        {
                helpFieldLvl(hrefTarget);
        }
        else if ((popType == "pop_up_3_col") || (popType == "pop_up_3_col") || (popType == "pr_fr_3_col"))
        {
                helpModLvl(hrefTarget);
        }
        else if ((popType == "pop_up_4_col") ||(popType == "pr_fr_4_col"))
        {
                printerFriend(hrefTarget);
        }
        else if ((popType == "pop_up_full_page"))
        {
                newWindow(hrefTarget);
        }
        else {
                return;
        }
}

/* -----------------------------------------------
FUNCTION: popLanguage
DESC: Create a module level pop up window
ARGS:
        formName - url to be opened
        sel - name of the select element from which to
                  get the value.
        check - checkbox to remember location
-------------------------------------------------*/
function popLanguage(formName,sel,check) {
        //Set Variables
        var  fieldValue, localeValue, hrefTarget,objTest,bolTestResult;
        objTest = /_none/;
        fieldValue = sel.options[sel.selectedIndex].value;
        bolTestResult = objTest.test(fieldValue);
        cookieValue = "";
        if ( check.checked )
        {
                cookieValue = check.value;
        }

        if ( fieldValue == "" )
        {
                alert('Please select a location')

        }
        else if ( bolTestResult || cookieValue == "yes" )
        {
                formName.submit();
        }
        else
        {
                location.href="http://www.ups.com/content/"+fieldValue.substring(0,2)+"/"+fieldValue.substring(3,5)+"/index.jsx";
        }
        return false;
}
