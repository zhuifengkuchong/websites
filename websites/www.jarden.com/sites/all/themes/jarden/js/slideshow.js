$(window).load(function(){
	
	var slideTime = 7000;
	
	var inTransition = false;
	
	var slides = $('#slideshow li, .middleSliderHolder .middleSlider'), current = 0;

    window.setTimeout(function(){
        gotoNextSlide();
    },slideTime);

    $(".innorow").each(function(){
        function cutString(s, n){
            var cut= s.indexOf('. ', n);
            //if(cut== -1) return s;
            return "<span class='shortString'>"+s.substring(0, n)+"... <a class='innoMore' href='#'>(Read More)</a></span><span class='fullString' style='display:none;'>"+s+" <a class='innoLess' href='#'>(Read Less)</a></span>";
        }
        
        var row = $(this);
        var rowText = $("p",row);
        var text = rowText.html();
        
        rowText.html(cutString(text,340));
        
        $('.innorow .innoLess').click(function(e){
            e.preventDefault();
            $(this).parent().hide();
            $(this).parent().prev().show();
        });
        $('.innorow .innoMore').click(function(e){
            e.preventDefault();
            $(this).parent().hide();
            $(this).parent().next().show();
        });
    });
    
    $(".bulletrow").each(function(){
        
        
        var row = $(this);
        var rowText = $("p",row);
        var text = rowText.html();
        
        
        $('.bulletrow .bulletLess').click(function(e){
            e.preventDefault();
            $(this).parents(".fullString").hide();
            $(".shortString",$(this).parents(".bulletrow")).show();
        });
        $('.bulletrow .bulletMore').click(function(e){
            e.preventDefault();
            $(".fullString",$(this).parents(".bulletrow")).show();
            $(this).parents(".shortString").hide();
        });
    });
    
    $(".faq").each(function(){
        
        
        var row = $(this);
        var rowText = $("p",row);
        var text = rowText.html();
        
        
        $('.faqrow .faqLess').click(function(e){
            e.preventDefault();
            $(this).parent().hide();
            $(this).parent().prev().show();
        });
        $('.faqrow .faqMore').click(function(e){
            e.preventDefault();
            $(this).parent().hide();
            $(this).parent().next().show();
        });
    });
    
    $(".susrow").each(function(){
        function cutString(s, n){
            var cut= s.indexOf('. ', n);
            if(cut== -1) return s;
            return "<span class='shortString'>"+s.substring(0, cut+2)+" <a class='innoMore' href='#'>(Read More)</a></span><span class='fullString' style='display:none;'>"+s+" <a class='innoLess' href='#'>(Read Less)</a></span>";
        }
        
        var row = $(this);
        var rowText = $("p",row);
        var text = rowText.html();
        
        rowText.html(cutString(text, 340)); 
        
        $('.susrow .innoLess').click(function(e){
            e.preventDefault();
            $(this).parent().hide();
            $(this).parent().prev().show();
        });
        $('.susrow .innoMore').click(function(e){
            e.preventDefault();
            $(this).parent().hide();
            $(this).parent().next().show();
        });
    });

	$(".letterrow").each(function(){
        function cutString(s, n){
            var cut= s.indexOf('. ', n);
            //if(cut== -1) return s;
            return "<span class='shortString'>"+s.substring(0, n)+" <a class='innoMore' href='#'>(Read More)</a></span><span class='fullString' style='display:none;'>"+s+" <a class='innoLess' href='#'>(Read Less)</a></span>";
        }
        
        var row = $(this);
        var rowText = $("p",row);
        var text = rowText.html();
        
        rowText.html(cutString(text,295));
        
        $('.letterrow .innoLess').click(function(e){
            e.preventDefault();
            $(this).parent().hide();
            $(this).parent().prev().show();
        });
        $('.letterrow .innoMore').click(function(e){
            e.preventDefault();
            $(this).parent().hide();
            $(this).parent().next().show();
        });
    });

	$('#slideshow .arrow').click(function(){
	    if (!inTransition) {
		    inTransition = true;
		        
            if($(this).hasClass('next')){
                gotoNextSlide();
            } else {
                gotoPrevSlide();
            }
    		
		}
	});
	
	
	function gotoNextSlide() {
        var li = slides.eq(current), nextIndex = 0;
        nextIndex = current >= slides.length-1 ? 0 : current+1;
        var next = slides.eq(nextIndex);
        
        next.show();
        current = nextIndex;
        
        li.fadeOut(function(){
            inTransition = false;
            li.removeClass('slideActive');
            next.addClass('slideActive');
        });
        window.setTimeout(function(){
            gotoNextSlide();
        },slideTime);
	}
	
	function gotoPrevSlide() {
        var li = slides.eq(current), nextIndex = 0;
	    nextIndex = current <= 0 ? slides.length-1 : current-1;
        var next = slides.eq(nextIndex);
        
	    next.show();
        current = nextIndex;
        
        li.fadeOut(function(){
            inTransition = false;
            li.removeClass('slideActive');
            next.addClass('slideActive');
        });
	}
	
});
