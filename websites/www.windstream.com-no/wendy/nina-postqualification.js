
/*

Nuance Nina Web - PostQualification script
==========================================

<script>
  NinaPostQualif = {
    url: "https://eu.agent.virtuoz.com/kaspersky-kaspersky_estore_va_de-german-WebBotRouter",
    cookieName: "VirtuOz-VzKasperskyEStore-postQualifSCI" // optional
  };
</script>
<script src='../404/-aspxerrorpath=-wendy-path-to-jQuery.js'/*tpa=http://www.windstream.com/wendy/path/to/jQuery.js*/></script>
<script src='../404/-aspxerrorpath=-wendy-path-to-Nina-postQualification.js'/*tpa=http://www.windstream.com/wendy/path/to/Nina-postQualification.js*/></script>

*/


(function(config){

    window.Nina = window.Nina || {};
    var http,
        lastchar,
        cookieName, cookieSCI,
        data,
        rand,
        service_url = config.service_url || "/jbotservice.asmx/Qualification";

    // if no service_url is set in VirtuOzVars, we exit and throw an error
    if( !config.hasOwnProperty('url' || typeof config.url !== "string") ){
        throw('NinaPostQualif.url NOT defined or is not a string');
    }

    Nina.postqualify = function(sci, name, value, callback){

        //Delete the possible / or \ at the end of the url.
        lastchar = config.url.charAt( config.url.length);
        if (lastchar === '/' || lastchar === '\\')
            config.url = config.url.substring(0, config.url.length - 1)

        // We will initialize the qualification sci if not set by the user.
        if (!sci && config.cookieName && typeof config.cookieName === 'string') {
            // Search in document.cookie for the SCI
            cookieSCI = getCookie(config.cookieName);
            //if (!cookieSCI)
            //  return -1;
            // Set the sci.
            sci = decodeURIComponent(cookieSCI);
        }else{
            throw('You must defined either the SCI or the cookieName');
        }

        // Create the Info object that will be pass to the Json service.
        data = { "Info": [{
            "SCI": sci,
            "Name": name.toLowerCase(),
            "DateTime": toISOString(new Date()),
            "Value": value.toLowerCase()
          }]
        };

        // Create the rand value to bypass proxy caching on Get command.
        rand = (new Date()).getTime() * Math.random() + (new Date()).getTime();

        // Make the ajax call
        if(window.$ && $.fn.jquery){
            $.ajax({
                url: config.url + service_url,
                dataType: 'script',
                data: { "Infos": JSON.stringify(data), "Rnd": rand }
            }).always(function(){
                if( typeof callback === 'function'){
                    callback();
                }
            });
        }else{
            sendQuery( config.url+service_url, data, callback);
        }
    };

    function _padzero(n){
        return n < 10 ? '0' + n : n;
    }

    function _pad2zeros(n){
        if (n > 9 && n < 100) { return '0' + n; }
        else if (n < 10)  { return '00' + n; }
        else return n;
    }

    function toISOString(d){
        return d.getUTCFullYear() + '-' + _padzero(d.getUTCMonth() + 1) +
            '-' + _padzero(d.getUTCDate()) + 'T' + _padzero(d.getUTCHours()) +
            ':' + _padzero(d.getUTCMinutes()) + ':' + _padzero(d.getUTCSeconds()) +
            '.' + _pad2zeros(d.getUTCMilliseconds()) + 'Z';
    }

    function getCookie( cookieName ) {
        var cookieJar = document.cookie.split( "; " );
        for( var x = 0; x < cookieJar.length; x++ ) {
            var cookie = cookieJar[x].split( '=' );
            if( cookie[0] === encodeURIComponent(cookieName) ) {
                return decodeURIComponent( cookie[1] );
            }
        }
        return null;
    }

    function sendQuery(url, data, callback){
        var script = document.createElement('script');
        script.async = true;
        script.src = url + "?" + "Infos=" + encodeURIComponent(JSON.stringify(data)) + "&Rnd=" + rand ;
        if(script.hasOwnProperty('onload') && typeof callback === 'function'){
            script.onload = function() {
                callback(true);
            };
        }
        if(script.hasOwnProperty('onerror') && typeof callback === 'function'){
            script.onerror = function() {
                callback(false);
            };
        }
        (document.head||document.documentElement).appendChild(script);
    }

})(NinaPostQualif || {});
