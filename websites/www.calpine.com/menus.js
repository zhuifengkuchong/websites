var label_list = new Array();

var link_list = new Array();

var menu_alive = new Array(); // will contain a hash of top-level-id = number



var tout = 100;

var avoidLikePlague = false;

var browser = navigator.userAgent.toLowerCase();

var isNav = browser.indexOf("mozilla") != -1 && browser.indexOf("compatible") == -1;

var appVer = navigator.appVersion.toLowerCase();

var numAppVer = parseFloat(appVer);

if (isNav && numAppVer < 5) { avoidLikePlague = true; }



// About Us

var labels1 = new Array();

labels1[0] = "Business Units";

labels1[1] = "Our Commitment";

labels1[2] = "Executive Management";

labels1[3] = "Board of Directors";

labels1[4] = "Government Relations";

labels1[5] = "Contact";

label_list["about"] = labels1;



var links1 = new Array();

links1[0] = "http://www.calpine.com/about/bus_units.asp";

links1[1] = "http://www.calpine.com/about/commitment.asp";

links1[2] = "http://www.calpine.com/about/exec_mgmt.asp";

links1[3] = "http://www.calpine.com/about/bdofdir.asp";

links1[4] = "http://www.calpine.com/about/gov_relns.asp";

links1[5] = "http://www.calpine.com/about/contact.asp";

link_list["about"] = links1;



// Investors

var labels2 = new Array();

labels2[0] = "Board & Executives";

labels2[1] = "News Releases";

labels2[2] = "Stock Information";

labels2[3] = "Equity Analysts";

labels2[4] = "Shareholder Information";

labels2[5] = "SEC Filings";

labels2[6] = "Financial Reports";

labels2[7] = "Calendar of Events";

labels2[8] = "Presentations";

labels2[9] = "Information Requests";

labels2[10] = "Email Alerts";

labels2[11] = "Investor FAQ";

label_list["investor"] = labels2;



var links2 = new Array();

links2[0] = "http://phx.corporate-ir.net/phoenix.zhtml?c=103361&p=irol-govBoard";

links2[1] = "http://phx.corporate-ir.net/phoenix.zhtml?c=103361&p=irol-news";

links2[2] = "http://phx.corporate-ir.net/phoenix.zhtml?c=103361&p=irol-stockquote";

links2[3] = "http://phx.corporate-ir.net/phoenix.zhtml?c=103361&p=irol-analysts";

links2[4] = "http://phx.corporate-ir.net/phoenix.zhtml?c=103361&p=proxy";

links2[5] = "http://phx.corporate-ir.net/phoenix.zhtml?c=103361&p=irol-sec";

links2[6] = "http://phx.corporate-ir.net/phoenix.zhtml?c=103361&p=irol-reportsAnnual";

links2[7] = "http://phx.corporate-ir.net/phoenix.zhtml?c=103361&p=irol-calendar";

links2[8] = "http://phx.corporate-ir.net/phoenix.zhtml?c=103361&p=irol-presentations";

links2[9] = "http://phx.corporate-ir.net/phoenix.zhtml?c=103361&p=irol-infoReq";

links2[10] = "http://phx.corporate-ir.net/phoenix.zhtml?c=103361&p=irol-alerts";

links2[11] = "http://phx.corporate-ir.net/phoenix.zhtml?c=103361&p=irol-faq";

link_list["investor"] = links2;



// Products

var labels3 = new Array();

labels3[0] = "Wholesale Energy";

labels3[1] = "Renewable Energy";

labels3[2] = "Energy Management Services";

labels3[3] = "Sales Office Contacts";

label_list["products"] = labels3;



var links3 = new Array();

links3[0] = "http://www.calpine.com/products/whlsl_energy.asp";

links3[1] = "http://www.calpine.com/products/renew_energy.asp";

links3[2] = "http://www.calpine.com/products/energy_mgmt.asp";

links3[3] = "http://www.calpine.com/products/reg_office.asp";

link_list["products"] = links3;



// Power

var labels4 = new Array();

labels4[0] = "Power Plants";

labels4[1] = "Our Technologies";

label_list["power"] = labels4;



var links4 = new Array();

links4[0] = "http://www.calpine.com/power/plants.asp";

links4[1] = "http://www.calpine.com/power/technologies.asp";

link_list["power"] = links4;



// Media

var labels5 = new Array();

labels5[0] = "News Releases";

labels5[1] = "Photos & Logos";

labels5[2] = "Media Contacts";

labels5[3] = "FAQs";

label_list["media"] = labels5;



var links5 = new Array();

links5[0] = "http://phx.corporate-ir.net/phoenix.zhtml?c=103361&p=irol-news";

links5[1] = "http://www.calpine.com/media/photos.asp";

links5[2] = "http://www.calpine.com/media/media_contact.asp";

links5[3] = "http://www.calpine.com/tools/faqs.asp";

link_list["media"] = links5;



// Careers

var labels6 = new Array();

labels6[0] = "Available Positions";

labels6[1] = "Employee Benefits";

labels6[2] = "Career Development";

labels6[3] = "EEO & Diversity";

//labels6[4] = "Internship Program";

labels6[4] = "Rotational Program";

label_list["careers"] = labels6;



var links6 = new Array();

links6[0] = "http://www.hrapply.com/calpine/Setup.app";

links6[1] = "http://www.calpine.com/careers/benefits.asp";

links6[2] = "http://www.calpine.com/careers/career_dev.asp";

links6[3] = "http://www.calpine.com/careers/diversity.asp";

//links6[4] = "http://www.calpine.com/careers/internship.asp";

links6[4] = "http://www.calpine.com/careers/rotational.asp";

link_list["careers"] = links6;





// load the menu divs

function loadDivs() {

  if (avoidLikePlague) { return false; }

  var counter = 0;

  for (topLevelId in label_list) {

    var thisLabels = label_list[topLevelId];

    var thisLinks = link_list[topLevelId];

    var numItems = thisLabels.length;

    document.write('<div onMouseover="incMenuAlive(\'' + topLevelId + '\')" onMouseout=" decMenuAlive(\'' + topLevelId + '\');hideOneDiv(\'' + topLevelId + '\');" id="menu_' + topLevelId + '" class="menudiv" style="visibility:hidden">');

    for (var i=0; i<numItems; i++) {

      if (i != 0) { document.write("<br>"); }

      document.write('<a class="menuitem" href="');

      document.write(thisLinks[i]);

      document.write('">');

      document.write(thisLabels[i] + '</a>');

    }

    document.write('</div>');

    menu_alive[topLevelId] = 1;

    counter += 1;

  }

}





function hideOneDiv(divid) {

  timerId = setTimeout("doHideOneDiv('" + divid + "')", tout);

}



function doHideOneDiv(divid) {

  if (menu_alive[divid] == 0) {

    menuOff(divid);

  }

}

 

function hideAllDivs() {

  timerId = setTimeout("doHideDivs()", tout);

}



function doHideDivs() {

  for (topLevelId in label_list) {

    if (menu_alive[topLevelId] == 0) {

      menuOff(topLevelId);

    }

  }

}



function incMenuAlive(menu_id) {

  menu_alive[menu_id] = 1;

}



function decMenuAlive(menu_id) {

  menu_alive[menu_id] = 0;

}





function imgOn(imgName) {

  if (document.images) {

    document.images[imgName].src = "http://www.calpine.com/img/" + imgName + "_on.gif";

  }

}

function imgOff(imgName) {

  if (document.images) {

    document.images[imgName].src = "http://www.calpine.com/img/" + imgName + "_off.gif";

  }

}



function menuOn(divid) {

  if (document.getElementById) {

    hideAllDivs();

    obj = document.getElementById("menu_" + divid);

    obj.style.visibility = "visible";

  }

}

function menuOff(divid) {

  if (document.getElementById) {

    obj = document.getElementById("menu_" + divid);

    obj.style.visibility = "hidden";

    incMenuAlive(divid);

  }

}







