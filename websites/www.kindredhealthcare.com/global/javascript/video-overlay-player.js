var VideoOverlay = function() {
    var overlay = $j('#video-modal').overlay({
        api: true,
        expose: {
            color: '#333',
            loadSpeed: 200,
            opacity: 0.9
        },
        onLoad: function() {
            $f('modal-player').load();
        },
        onClose: function() {
            $f('modal-player').pause(); /*ie8 win7 issue*/
            $f('modal-player').unload();
        }
    });


    $j('a[href$=.flv]').click(function(event) {
        var videoLink = this.href;
        flowplayer('modal-player', '../flash/flowplayer.leapfrog-3.1.5.swf'/*tpa=http://www.kindredhealthcare.com/global/flash/flowplayer.leapfrog-3.1.5.swf*/, {
            key: videoPlayerLicenseCode,
            clip: {
                scaling: 'fit',
                url: videoLink,
                autoPlay: true
            },
            canvas: {
                backgroundColor: '#ffffff'
            },
            plugins: {
                controls: {
                    autoHide: 'always',
                    hideDelay: 1000
                }
            }
        });

        overlay.load();

        return false;
    });
};



$j(document).ready(function() {
    var vidOverlay = new VideoOverlay();
});