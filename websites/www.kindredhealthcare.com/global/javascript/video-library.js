var homepageSlider;
var horizontalSlider;
var videoPlayer;

$j(document).ready(function() {
	//remove all click events from video links so the modal player doesnt interfere
	$j('.vertical-slider a.video-link').unbind('click');

	var miniLibrary = $j('#mini-video-library');
	var videoLibrary = $j('#video-library');
	var videoHeight = $j('#video-player').css('height');

	var $currentCat = $j('.current-category', videoLibrary);
	var initialCat = $j('.category-list a:last', videoLibrary).text().toUpperCase();
	$currentCat.text(initialCat);

	$j('#youtube-player').hide();

	if (miniLibrary.length > 0 || (videoLibrary.length > 0 && videoLibrary.find('.horizontal-slider .video').length > 0)) {
		var clipUrl;
		var imageUrl;
		var embedCode;

		if (miniLibrary.length > 0) {
			$j('.video-link', miniLibrary).unbind('click');
			clipUrl = $j('.vertical-slider a.video-link:first', miniLibrary).attr('href');
			imageUrl = $j('.thumb img:first', miniLibrary).attr('src');
			embedCode = getYoutubeEmbedCode($j('.thumb:first a.mobile-link').attr('href'));
		} else {
			var $videoWrapper = $j('.video-player-wrapper', videoLibrary);
			var $link = $j('.vertical-slider a.video-link:first', videoLibrary);
			var $mobileLink = $link.siblings('.mobile-link').hide();
			var $flvLink = $link.siblings('.flv-link').hide();
			var title = $link.siblings('.title').text();
			var description = $link.siblings('.description').html();

			embedCode = getYoutubeEmbedCode($mobileLink.attr('href'));
			clipUrl = $flvLink.attr('href') || '';
			imageUrl = $j('.vertical-slider .thumb:first', videoLibrary).attr('src');
			$videoWrapper.find('.title').text(title);
			$videoWrapper.find('.description').html(description);
		}

		imageUrl = imageUrl.replace(/\/image-full;max\$\d+,\d+.ImageHandler/i, '');

		/* --- Video Player --- */
		if ($j('#video-player').length == 1) {
			flowplayer('video-player', '../flash/flowplayer.leapfrog-3.1.5.swf'/*tpa=http://www.kindredhealthcare.com/global/flash/flowplayer.leapfrog-3.1.5.swf*/, {
				key: videoPlayerLicenseCode,
				clip: {
					scaling: 'fit'
				},
				playlist: [
					imageUrl,
					{ url: clipUrl, autoPlay: false }
				],
				canvas: {
					backgroundColor: '#ffffff'
				},
				plugins: {
					controls: {
						autoHide: 'always',
						hideDelay: 1000
					}
				},
				onLoad: function() {
					
				}
			});

			if (embedCode != undefined && embedCode != '') {
				$j('#video-library .video-player-wrapper .mobile-link').hide();
				$j('#video-player').css('height', '1px');
				$j('#youtube-player').show().html('<iframe width="100%" height="100%" src="http://www.youtube.com/embed/' + embedCode + '?rel=0" frameborder="0" allowfullscreen></iframe>');
			}
		}

		/* --- Homepage Slider --- */
		homepageSlider = function() {
			var $thumbsWrapper = $j('.thumbs-wrapper', miniLibrary);
			var $thumbs = $j('.thumb', $thumbsWrapper);
			var curIndex = 0;

			return {
				slide: function(slideCount) {
					var newIndex = curIndex + slideCount;
					if (slideCount > 0 && newIndex >= $thumbs.length) newIndex = $thumbs.length - 1;
					if (slideCount < 0 && newIndex < 0) newIndex = 0;
					if (newIndex != curIndex && newIndex != (curIndex + 1)) {
						var $elm = $thumbs.eq(newIndex);
						curIndex = newIndex;
						$thumbsWrapper.scrollTo($elm, 'normal', { offset: { left: -2} });
					}
				}
			};
		} ();

		$j('.left-button', miniLibrary).click(function() {
			homepageSlider.slide(-2);
			return false;
		});

		$j('.right-button', miniLibrary).click(function() {
			homepageSlider.slide(2);
			return false;
		});


		/* --- Horizontal Slider --- */
		horizontalSlider = function() {
			var $slideWrapper = $j('.horizontal-slider .slide-wrapper', videoLibrary);
			var $groups = $j('.slide-group', $slideWrapper);
			$groups.each(function(i) {
				this.$slides = $j('.slide', this);
				if (i < $groups.length - 1) $j(this).hide();
			});
			var curIndex = 0;
			var curGroup = $groups.length - 1;

			return {
				slide: function(slideCount, speed) {
					var $slides = $groups.eq(curGroup).get(0).$slides;
					var newIndex = curIndex + slideCount;
					if (slideCount > 0 && newIndex >= $slides.length) newIndex = $slides.length - 1;
					if (slideCount < 0 && newIndex < 0) newIndex = 0;
					if (newIndex != curIndex) {
						var $elm = $slides.eq(newIndex);
						curIndex = newIndex;
						$slideWrapper.scrollTo($elm, speed, { offset: { left: -20} });
					}
				},
				switchGroup: function(groupNumber) {
					var newGroup = (groupNumber < 0 || groupNumber >= $groups.length) ? 0 : groupNumber;
					if (newGroup != curGroup) {
						$groups.eq(curGroup).hide()
						$groups.eq(newGroup).show();
						curGroup = newGroup;
						this.slide(-$groups.eq(newGroup).get(0).$slides.length);
					}
				}
			};
		} ();

		$j('.horizontal-slider .prev-button', videoLibrary).click(function() {
			horizontalSlider.slide(-1, 'normal');
			return false;
		});

		$j('.horizontal-slider .next-button', videoLibrary).click(function() {
			horizontalSlider.slide(1, 'normal');
			return false;
		});

		$j('.horizontal-slider .category-list a', videoLibrary).click(function(event) {
			var category = $j(this).text().toUpperCase();
			var group = parseInt(this.hash.replace('#', ''));
			$currentCat.text(category);
			horizontalSlider.switchGroup(group);
			return false;
		});


		//set initial category if one was passed in on the hash
		var matches = window.location.hash.match(/^#([0-9]+)/);
		if (matches != null && matches.length > 0) {
			$j('.horizontal-slider .category-list a:eq(' + (matches[1] - 1) + ')').click();
		}


		/* --- Vertical Slider --- */
		if (typeof VerticalSlider !== "undefined") {
			var recentVideoSlider = new VerticalSlider(
				'#video-library .vertical-slider .video-wrapper',
				'#video-library .vertical-slider .slide-up-button',
				'#video-library .vertical-slider .slide-down-button',
				{
					scrollOptions: {
						offset: { top: -10 }
					}
				}
			);
		}

		/* --- Make Videos Play ---*/
		$j('.thumb', miniLibrary).click(function(event) {
			var videoPath = $j('a.video-link', this).attr('href');
			var embedCode = getYoutubeEmbedCode($j('a.mobile-link', this).attr('href'));
			if (embedCode != undefined && embedCode != '') {
				$f().stop();
				$j('#video-player').css('height', '1px');
				$j('#youtube-player').hide().html('<iframe width="100%" height="100%" src="http://www.youtube.com/embed/' + embedCode + '?autoplay=1&rel=0" frameborder="0" allowfullscreen></iframe>').show();
			} else {
				$j('#youtube-player').hide().html('');
				$j('#video-player').css('height', videoHeight);
				$f().play(videoPath);
			}
			return false;
		});

		$j('.horizontal-slider .video', videoLibrary).click(function(event) {
			var videoPath = $j('a.flv-link', this).attr('href');
			var mobilePath = $j('a.mobile-link', this).attr('href');
			var embedCode = getYoutubeEmbedCode(mobilePath);
			var title = $j('a.title', this).text();
			var description = $j('.description', this).html();
			var $videoWrapper = $j('#video-library .video-player-wrapper');
			$videoWrapper.find('.title').text(title);
			$videoWrapper.find('.description').html(description);
			if (embedCode != undefined && embedCode != '') {
				$videoWrapper.find('.mobile-link').hide();
				$f().stop();
				$j('#video-player').css('height', '1px');
				$j('#youtube-player').hide().html('<iframe width="100%" height="100%" src="http://www.youtube.com/embed/' + embedCode + '?autoplay=1&rel=0" frameborder="0" allowfullscreen></iframe>').show();
			} else {
				$j('#youtube-player').hide().html('');
				$j('#video-player').css('height', videoHeight);
				$f().play(videoPath);
			}
			return false;
		});

		$j('.vertical-slider .video .video-link', videoLibrary).click(function(event) {
			var $link = $j(this);
			var videoPath = $link.siblings('.flv-link').attr('href');
			var mobilePath = $link.siblings('.mobile-link').attr('href');
			var embedCode = getYoutubeEmbedCode(mobilePath);
			var title = $link.siblings('.title').text();
			var description = $link.siblings('.description').html();
			var $videoWrapper = $j('.video-player-wrapper', videoLibrary);
			$videoWrapper.find('.title').text(title);
			$videoWrapper.find('.description').html(description);
			if (embedCode != undefined && embedCode != '') {
				$videoWrapper.find('.mobile-link').hide();
				$f().stop();
				$j('#video-player').css('height', '1px');
				$j('#youtube-player').hide().html('<iframe width="100%" height="100%" src="http://www.youtube.com/embed/' + embedCode + '?autoplay=1&rel=0" frameborder="0" allowfullscreen></iframe>').show();
			} else {
				if (mobilePath != undefined) {
					$videoWrapper.find('.mobile-link').show();
				}
				$j('#youtube-player').hide().html('');
				$j('#video-player').css('height', videoHeight);
				$f().play(videoPath);
			}
			return false;
		});
	}

	function getYoutubeEmbedCode(url) {
		var pattern = /(?:http:\/\/www\.youtube\.com\/user\/kindredhealthcareinc#p\/(?:(?:a\/u\/.+\/(.+))|(?:u\/.+\/(.+))|search\/.+\/(.+)))|(?:http:\/\/youtu.be\/(.+))|(?:http:\/\/www\.youtube\.com\/watch\?v=(.+))/;
		var match = pattern.exec(url);

		if (match != null) {
			for (var i = 1; i < match.length; i++) {
				if (match[i] != undefined && match[i] != '') {
					return match[i];
				}
			}
		}

		return undefined;
	}
});