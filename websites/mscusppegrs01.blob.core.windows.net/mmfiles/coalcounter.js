/* Initializations */
var timerID;
var timerRunning = false;
var today = new Date();
var startday = new Date();
var secPerDay = 0;
var minPerDay = 0;
var hourPerDay = 0;
var secsLeft = 0;
var secsRound = 0;
var secsRemain = 0;
var minLeft = 0;
var minRound = 0;
var minRemain = 0;
var timeRemain = 0;
var tonsSold = 0;
var tonMillion = 0;
var roundtonMillion = 0;
var tonThousand = 0;
var roundtonThousand = 0;
var tonHundred = 0;
var tonDisplay = 0;
var baseTons = 255000000;
var dayTons = 0;
var hourTons = 0;
var minTons = 0;
var secTons = 0;
var daysInYear = 0;

/* This function will stop the clock */

function stopclock(){
if(timerRunning)
{
clearTimeout(timerID);
}
timerRunning = false;
}

/* This function will start the clock */

function startclock() {
stopclock();
showtime1();
}

/* This function will display the count-up */

function showtime1() {
startday = new Date("January 1, 2015 00:00 CST");
today = new Date();
secsPerDay = 1000 ;
minPerDay = 60 * 1000 ;
hoursPerDay = 60 * 60 * 1000;
PerDay = 24 * 60 * 60 * 1000;

/* Seconds */
secsLeft = (today.getTime() - startday.getTime()) / minPerDay;
secsRound = Math.round(secsLeft);
secsRemain = secsLeft - secsRound;
secsRemain = (secsRemain < 0) ? secsRemain = 60 - ((secsRound - secsLeft) * 60) : secsRemain = (secsLeft - secsRound) * 60;
secsRemain = Math.round(secsRemain);

/* Minutes */
minLeft = ((today.getTime() - startday.getTime()) / hoursPerDay);
minRound = Math.round(minLeft);
minRemain = minLeft - minRound;
minRemain = (minRemain < 0) ? minRemain = 60 - ((minRound - minLeft) * 60) : minRemain = ((minLeft - minRound) * 60);
minRemain = Math.round(minRemain - 0.495);

/* Hours */
hoursLeft = ((today.getTime() - startday.getTime()) / PerDay);
hoursRound = Math.round(hoursLeft);
hoursRemain = hoursLeft - hoursRound;
hoursRemain = (hoursRemain < 0) ? hoursRemain = 24 - ((hoursRound - hoursLeft) * 24)  : hoursRemain = ((hoursLeft - hoursRound) * 24);
hoursRemain = Math.round(hoursRemain - 0.5);

/* Days */
daysLeft = ((today.getTime() - startday.getTime()) / PerDay);
daysLeft = (daysLeft - 0.5);
daysRound = Math.round(daysLeft);
daysRemain = daysRound;

/* Time */
if (daysRemain == 1)
{
  day_rem = " day, "
}
else
{
  day_rem = " days, "
}

if (hoursRemain == 1)
{
  hour_rem = " hour, "
}
else
{
  hour_rem = " hours, "
}

if (minRemain == 1)
{
  min_rem = " minute, "
}
else
{
  min_rem = " minutes, "
}

if (secsRemain == 1)
{
  sec_rem = " second"
}
else
{
  sec_rem = " seconds"
}

/* calculate number of days in year - leap year or not */
currentyr= today.getYear();
//alert("this is currentyr " + currentyr);

if ((currentyr % 4 == 0)&& (currentyr % 100 !=0) || (currentyr % 400 == 0))  {
                daysInYear = 366;
}else{
                daysInYear = 365;
                }

//alert("this is daysInYear " + daysInYear);

/* calculate number of tons per day, hour, min, sec  */
dayTons = Math.round((baseTons / daysInYear) * 1000)/1000;
//alert ("this is dayTons " + dayTons);
hourTons = Math.round((dayTons / 24 )* 1000)/1000;
//alert ("this is hour tons " + hourTons);
minTons = Math.round((hourTons / 60)*1000)/1000;
//alert ("this is min tons" + minTons);
secTons = Math.round((minTons / 60)*1000)/1000;
//alert ("this is secTons " + secTons);

tonsSold = (daysRemain * dayTons) + (hoursRemain * hourTons) + (minRemain * minTons) + (secsRemain * secTons);
tonMillion = tonsSold / 1000000;
roundtonMillion = Math.floor(tonMillion);
tonThousand = (tonsSold - roundtonMillion * 1000000) / 1000;
roundtonThousand = Math.floor(tonThousand);
tonHundred = tonsSold - roundtonMillion * 1000000- roundtonThousand * 1000;
tonHundred = Math.floor(tonHundred);
//alert("this is ton hundred " + tonHundred);
if (tonHundred < 10) {
                tonHundred = ("00" + tonHundred);
                }else{
                                if (tonHundred < 100) {
                                tonHundred=("0" + tonHundred);
                                }
                }
                //alert("This is roundtonThousand " + roundtonThousand);
                if (roundtonThousand < 10) {
                roundtonThousand = ("00" + roundtonThousand);
                }else{
                                if (roundtonThousand < 100) {
                                roundtonThousand=("0" + roundtonThousand);
                                }
                }

tonDisplay = (roundtonMillion + ",") + (roundtonThousand + ",") + (tonHundred );

$(".SalesCounter").html(tonDisplay);
timerID = setTimeout("showtime1()",1000);
timerRunning = true;
}
startclock();

$('.jqueryRotator').cycle({
fx: 'fade',
timeout: 5000,
delay: 1000,
pager: '.coalCan',
activePagerClass: 'homeRotateActive',
pagerAnchorBuilder: function (index, el) {
return '<a href="#">&nbsp;</a>';
}
});

$('.coalCan a').click(function () {
$('.coalCan  a').removeClass('homeRotateActive');
var slide = $(this).index() + 1;
$(this).addClass('homeRotateActive');
$('.jqueryRotator').cycle('toggle');
});