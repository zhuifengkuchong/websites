// START OF SmartSource Data Collector TAG v10.2.10
// Copyright (c) 2012 Webtrends Inc.  All rights reserved.
window.webtrendsAsyncInit = function() {
    var dcs = new Webtrends.dcs().init({
        dcsid: "dcsst578p5dv0hoq1i7c5n5uh_8m1m" //Prod
    //    dcsid: "dcsmv53fs1y68jcr28h1dxaqm_9v9q" //Test
        , domain: "http://www.adm.com/_layouts/admworld/scripts/statse.webtrendslive.com"
        , timezone: -6
        //  , fpcdom:".{2},adm.com"
        ,fpcdom: ".adm.com"
        , i18n: true
        , offsite: true
        , download: true
        , downloadtypes: "xls,doc,pdf,txt,csv,zip,docx,xlsx,rar,gzip"
        , anchor: true
        , javascript: true
        , onsitedoms: ".adm.com"
        , plugins: {
          // hm: { src: "../../../../s.webtrends.com/js/webtrends.hm.js"/*tpa=http://s.webtrends.com/js/webtrends.hm.js*/ }
        }
    }).track();
};
(function() {
    var s = document.createElement("script"); s.async = true; s.src = "webtrends.min.js"/*tpa=http://www.adm.com/_layouts/admworld/scripts/webtrends.min.js*/;
    var s2 = document.getElementsByTagName("script")[0]; s2.parentNode.insertBefore(s, s2);
} ());
// END OF SmartSource Data Collector TAG v10.2.10