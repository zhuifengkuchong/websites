/* Build Time: 23:36:18 on 06-04-2015 */

/* ../head/mpulse.js */

/* Temporary Implementation of mPulse Tags Allowing For Better Backend System Testing. */
(function() {
	var dom,
		doc,
		where,
		apiKey,
		iframe = document.createElement('iframe'),
		subDomain = window.location.host.split('.')[0];

	if (subDomain === "www" || subDomain === "m") {
		apiKey = "W8H5V-D6KN9-TTLED-Y3VKD-B2SQA"; // production
	} else {
		apiKey = "6RXN8-RBUJN-58E9V-E7P3V-ZGGXP"; // development
	}

	iframe.src = "javascript:false";
	(iframe.frameElement || iframe).style.cssText = "width: 0; height: 0; border: 0";
	where = document.getElementsByTagName('script')[0];
	where.parentNode.insertBefore(iframe, where);
	try {
		doc = iframe.contentWindow.document;
	} catch(e) {
		dom = document.domain;
		iframe.src = "javascript:var d=document.open();d.domain='" + dom + "';void(0);";
		doc = iframe.contentWindow.document;
	}
	doc.open()._l = function() {
		var js = this.createElement("script");
		if (dom)
			this.domain = dom;
		js.id = "boomr-if-as";
		js.src = '//c.go-mpulse.net/boomerang/' + apiKey;
		BOOMR_lstart = new Date().getTime();
		this.body.appendChild(js);
	};
	doc.write('<body onload="document._l();">');
	doc.close();
})();
