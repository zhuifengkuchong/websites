/* Build Time: 23:36:18 on 06-04-2015 */

/* ../global/lib/eluminate.js */

/* DO NOT LINT */
/* Licensed Materials - Property of IBM, eluminate.js (Build # 4.14.33 Date/Time: 20130806-11:11), (C) Copyright IBM Corporation 2013. U.S. Government Users Restricted Rights:  Use, duplication or disclosure restricted by GSA ADP Schedule Contract with IBM Corp. */
(function(c){var i="",h="",b=false;function f(){var m=[cm_JSFCoreCookieName,"cmRS","cmTPSet","CoreAt","CMAVID","CoreM_State","CoreM_Ses"],n=cm_ClientID.split(";"),l;for(l=0;l<n.length;l++){m.push(cmJSFCreateCombinedSessionCookieName(n[l]))}for(l=0;l<m.length;l++){CC(m[l],cm_JSFPCookieDomain);CC(m[l])}}function e(l,m){var n=cI(l);return n&&n.toUpperCase()==m}function g(o){var l=0,m=0,p=o.length;for(;l<p;l++){m=~~(31*m+o.charCodeAt(l))}return Math.abs(m)}function a(m,l){return m?m:l?l:""}function d(){var s,u,q,l=screen,o=navigator,m=o.mimeTypes,r=o.plugins,t=""+l.width+l.height+l.availWidth+l.availHeight+l.colorDepth+l.pixelDepth+a(o.language,a(o.browserLanguage));if(m){for(q=0,u=m.length;q<u;q++){t+=a(m[q].type)}}if(r){for(q=0,u=r.length;q<u;q++){s=r[q];t+=a(s.name)+a(s.version)+a(s.description)+a(s.filename)}}return t}function k(){return h=="D"}c.cmSetCookieSetting=function(l){h=l;if(k()){f()}};c.cmCookiesDisabled=k;c.cmSessionCookiesOnly=function(){return h=="S"};c.cmSetOptOut=function(l){i=l};c.cmOptedOut=function(){return((i=="Y")||cI("CMDisabled")||e("CMOptout","OPT_OUT")||e("CMOptOut","OPT_OUT")||e("ID","OPT_OUT"))};c.cmAnonymous=function(){return((i=="A")||e("CMOptout","ANONYMOUS")||e("CMOptOut","ANONYMOUS"))};c.cmAutoAddTP=function(){return Math.random()<a(c.cm_TPThreshold,0.2)};c.cmSetIT=function(l){b=l};c.cmIT=function(){if(b){return"it"+g(d())}else{return null}}}(window));var cmUtils=(function(){return{console:{log:function(a){if(typeof console!=="undefined"){console.log(a)}},error:function(a){if(typeof console!=="undefined"){console.error(a)}}},string:{trim:function(a){if(typeof a==="string"){if(String.prototype.trim){return a.trim()}else{return a.replace(/^\s+|\s+$/g,"")}}return a}}}}());if(typeof CM_DDX==="undefined"){CM_DDX={domReadyFired:false,headScripts:true,dispatcherLoadRequested:false,firstPassFunctionBinding:false,BAD_PAGE_ID_ELAPSED_TIMEOUT:5000,version:-1,standalone:false,test:{syndicate:true,testCounter:"",doTest:false,newWin:false,process:function(){var d=CM_DDX.gup("http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/tms.test");CM_DDX.test.newWin=CM_DDX.gup("http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/tms.mWin")==="y";CM_DDX.test.doTest=CM_DDX.gup("tms.doTest")==="y";if(CM_DDX.test.doTest){var c=CM_DDX.gup("tms.syndicate");if(c===null){c="n"}if(d===null){d=""}c=c.toLowerCase();c=(c==="n"||c==="no"||c==="false")?"N":"Y";CM_DDX.test.testCounter=(d==="")?d:((d*1)+"");CM_DDX.test.syndicate=(c==="Y");CB("http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/ddx.test.info",d+"-"+c+"-"+CM_DDX.test.doTest+"-"+CM_DDX.test.newWin)}else{var b=cI("http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/ddx.test.info");if(b){var a=b.split("-");CM_DDX.test.testCounter=a[0];CM_DDX.test.syndicate=(a[1]==="Y");CM_DDX.test.doTest=(a[2]==="true");CM_DDX.test.newWin=(a.length===4&&a[3]==="true")}}}},partner:{},invokeFunctionWhenAvailable:function(a){if(CM_DDX.firstPassFunctionBinding===false){setTimeout(function(){CM_DDX.invokeFunctionWhenAvailable(a)},5)}else{if(CM_DDX.version!==0&&typeof(__$dispatcher)==="undefined"){setTimeout(function(){CM_DDX.invokeFunctionWhenAvailable(a)},CM_DDX.BAD_PAGE_ID_ELAPSED_TIMEOUT);return}if(CM_DDX.version!==0){a()}}},gup:function(d){d=d.replace(/[\[]/,"\\[").replace(/[\]]/,"\\]");var c="[\\?&]"+d+"=([^&#]*)";var b=new RegExp(c);var a=b.exec(window.location.href);return(a===null)?null:decodeURIComponent(a[1].replace(/\+/g," "))},privacy:{isDoNotTrackEnabled:function(b){var a=CM_DDX.privacy.getDoNotTrack(b);if(a==false){a=(cI("CM_DDX","pdnt0","false")=="true")?true:false}return a},setDoNotTrack:function(b,a){CM_DDX.setSubCookie("CM_DDX","pdnt"+b,a,365)},getDoNotTrack:function(a){return(cI("CM_DDX","pdnt"+a,"false")=="true")?true:false}},setSubCookie:function(b,a,e,c,d){cmSetSubCookie(b,a,e,new Date(new Date().getTime()+(c*86400000)).toGMTString(),d)}};if(!cm_ClientID){var cm_ClientID="99999999"}if(!cm_HOST){var cm_HOST="testdata.coremetrics.com/cm?"}if(!cmMarketing){var cmMarketing={}}cmMarketing.COOKIE_NAME="CoreMc";cmMarketing.INSTANCE=null;if(!cm_McClientID){var cm_McClientID=cm_ClientID}if(!cm_MC_LIB_HOST){var cm_MC_LIB_HOST="http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/libs.coremetrics.com"}if(!cm_MC_RULES_HOST){var cm_MC_RULES_HOST="http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/mktgcdn.coremetrics.com"}if(!cm_MC_USER_DETAILS_HOST){var cm_MC_USER_DETAILS_HOST="http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/mcdata.coremetrics.com"}if(!cm_MC_APP_SERVER_HOST){var cm_MC_APP_SERVER_HOST="http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/mc.coremetrics.com"}if(!cm_ClientTS){var cm_ClientTS=new Date().getTime()}if(!cm_TrackLink){var cm_TrackLink="A"}if(!cm_NewLinkTracker){var cm_NewLinkTracker=false}if(!cm_LinkClickDelay){var cm_LinkClickDelay=false}if(!cm_LinkClickDelayInterval){var cm_LinkClickDelayInterval=500}if(!cm_DelayHandlerReg){var cm_DelayHandlerReg=""}if(!cm_SkipHandlerReg){var cm_SkipHandlerReg=""}if(!cm_TrackImpressions){var cm_TrackImpressions="RSCM"}if(!cm_SecureTags||cm_SecureTags==null){var cm_SecureTags="|2|3|"}if(!cm_DownloadExtensions){var cm_DownloadExtensions=null}if(!cm_UseUTF8){var cm_UseUTF8=true}if(!cm_FormPageID){var cm_FormPageID=false}if(cm_UseCookie==null){var cm_UseCookie=false}if(!cm_TimeoutSecs){var cm_TimeoutSecs=15}if(!cm_UseDOMScriptLoad){var cm_UseDOMScriptLoad=true}if(!cm_OffsiteImpressionsEnabled){var cm_OffsiteImpressionsEnabled=false}if(!cm_AvidHost){var cm_AvidHost="http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/data.cmcore.com/cookie-id.js?fn=cmSetAvid"}var cm_AvidLoadTimedOut=false;if(!cm_JSFEnabled){var cm_JSFEnabled=false}if(!cm_JSFPCookieDomain){var cm_JSFPCookieDomain=null}if(!cm_JSFTrackClients){var cm_JSFTrackClients=true}if(!cm_JSFPCookieMigrate){var cm_JSFPCookieMigrate=false}if(!cm_JSFPForceMigrateCookies){var cm_JSFPForceMigrateCookies=false}if(!cm_JSFPCookieMigrateVisitorID){var cm_JSFPCookieMigrateVisitorID="cm_mc_uid"}if(!cm_JSFPCookieMigrateSessionID){var cm_JSFPCookieMigrateSessionID="cm_mc_sid"}if(!cm_JSFPMigrationDomainWhitelist){var cm_JSFPMigrationDomainWhitelist=null}if(!cm_JSFPMigrationDomainBlacklist){var cm_JSFPMigrationDomainBlacklist=null}if(!cm_JSFPMigrationPathWhitelist){var cm_JSFPMigrationPathWhitelist=null}if(!cm_JSFPMigrationOtherCookies){var cm_JSFPMigrationOtherCookies=null}if(!cm_JSFPMigrationOtherCookiesExpireTimes){var cm_JSFPMigrationOtherCookiesExpireTimes={}}if(!cm_JSFSessionType){var cm_JSFSessionType="I"}if(!cm_JSFSessionTimeout){var cm_JSFSessionTimeout=1800}if(!cm_JSFCoreCookieName){var cm_JSFCoreCookieName="CoreID6"}if(!cm_JSFCoreCookieExpireSeconds){var cm_JSFCoreCookieExpireSeconds=473040000}if(!cm_JSFEAMasterIDSessionCookie){var cm_JSFEAMasterIDSessionCookie=false}if(!cmUA){var cmUA={MSIE:2083}}if(!cmDefaultLimit){var cmDefaultLimit=8197}if(cGQ==null){var cGQ=true}if(!cGO){var cGO=1024}if(!cGR){var cGR=600000}if(!encodeURIComponent){var encodeURIComponent=null}var _$cV1=null;var cG8;var cG9;var cG6=document;var cGT;var cG7=new _cmt();cG6.cmTagCtl=cG7;var CI=cmStartTagSet;var CJ=cmSendTagSet;var cmIndex=0;var cG0=["vn1","vn2","st","pi","rs","ec","rf","ul"];var cGA=null;var cmValidFlag_SessionContinue=1;var cmValidFlag_NewSession=2;var cmValidFlag_NewVisitor=4;var cmValidFlag_SessionReset=32;var cmCore_JSFParamEnabled="cjen";var cmCore_JSFParamUserID="cjuid";var cmCore_JSFParamSessionID="cjsid";var cmCore_JSFParamValidFlag="cjvf";if(!cG4){var cG4=5000}if(!cG5){var cG5=200}var cG2={};var cG3={};var cGM=navigator.appVersion;var cGN=navigator.userAgent;var cGS=cGN.indexOf("Opera")>=0;var cGU=cGN.indexOf("Safari")>=0;var cmT3=-1;var cGC="";var cGD="";var cGE="";var cGF="";var cGG="";var cGH="";var cmSubmitFlag=false;var cmFormC1="submitbuttonreset";var cmFormC2="textpasswordtextarea";var cmFormC3="select-oneselect-multiple";var cGI="";var cGJ="";var cGK="";var chost=null;var cci=null;var _cm_CMRules={};var _cm_isNew=true,_cm_NRSet=false;if(!cm_PartnerDataClientIDs){var cm_PartnerDataClientIDs=""}var cm_Avid;var cmCookieExpDate;var cm_AvidLoadTimer;var cm_IOEnabled=false;var cm_ATEnabled=false;var cm_MCEnabled=false;(function(){CI();var e=new Date();var i=0;if(e.getFullYear){i=e.getFullYear()}else{i=e.getYear();if(i<1900){i+=1900}}e.setYear(i+15);cmCookieExpDate=e.toGMTString();if(cm_UseCookie){var k=cI("cmRS","pi","");chost=cm_HOST;cm_HOST=cI("cmRS","ho",chost);cci=cm_ClientID;cm_ClientID=cI("cmRS","ci",cci);var b=cI("cmRS","t3","");if(b!=""){cGA=b}var g=cI("cmRS","cjen","");if(g!=""){cm_JSFEnabled=true}var d=cI("cmRS","t1","");if(d!=""&&(!cGA||cm_ClientTS-cGA<cGR)){cmAddShared("st",d);var m=cI("cmRS","ul","");var o=cI("cmRS","rf","");var n=cI("cmRS","hr","");if(n!=""){var a=cI("cmRS","lti","");if(cm_ClientTS-a<cGR){var p=cI("cmRS","ln","");cM(d,a,p,n,true,k,m,o)}}var h=cI("cmRS","ac","");var f=cI("cmRS","fd","");if((h!="")||(f!="")){var a=cI("cmRS","fti","");if(cm_ClientTS-a<cGR){var c=cI("cmRS","fn","");var l=cI("cmRS","fu","");cL(d,a,c,h,l,f,true,k,m,o)}}}CC("cmRS")}if((cF(4)||CD(5))||cGS||cGU){cmAddNewEvent(document,"DOMContentLoaded",cmOnDomReady,"readystatechange",cmCheckIEReady);cmAddNewEvent(window,"load",cY);cmAddNewEvent(window,"unload",cZ);if(cm_DelayHandlerReg.indexOf("L")==-1){window.cX("main")}if(cm_DelayHandlerReg.indexOf("F")==-1){cU()}}CJ(1)}());var _cmPartnerUtils={AT_TagQueue:[],AT_PartnerCallQueue:[],AT_RulesSet:false};var _cmMc={readyToCall:{},mcTagQueue:[],callPending:{}};CM_DDX.test.process()}function cmRetrieveUserID(a){if(_$cV1!=null){a(_$cV1)}else{if(cm_JSFEnabled){_$cV1=cmJSFGetUserId();a(_$cV1)}else{var b="eluminate"+Math.floor((Math.random()*10000)+1);window[b]=function(c){_$cV1=c;a(_$cV1)};_cmPartnerUtils.loadScript(C8(null)+"//"+cm_Production_HOST+"/cookie-id.js?fn="+b)}}}function cmLoad(){if(cm_OffsiteImpressionsEnabled){cm_Avid=cI("CMAVID");if(cm_Avid==null){_cmPartnerUtils.loadScript(C8(null)+"//"+cm_AvidHost);cm_AvidLoadTimer=setTimeout(function(){cm_AvidLoadTimedOut=true},2000)}}var a=cm_Production_HOST;if(cm_ATEnabled){if(!cmOptedOut()&&!cmAnonymous()){if(typeof(_cm_CMRulesLoaded)=="undefined"){var c=cm_ClientID.split(";");for(var e=0;e<c.length;e++){c[e]=c[e].split("|")[0];if(cm_PartnerDataClientIDs.indexOf(c[e])!=-1){if(cI("CorePartnerMode")=="TEST"){_cmPartnerUtils.loadScript(C8(null)+"//"+a+"/at/rules_"+c[e]+"test.js")}else{_cmPartnerUtils.loadScript(C8(null)+"//"+a+"/at/rules_"+c[e]+".js")}}}cG6._cm_CMRulesLoaded=1}}}if(cm_MCEnabled){_cmPartnerUtils.loadScript(C8(null)+"//"+cm_MC_LIB_HOST+"/mc.js");try{if((_cmMc.getIframeMaxDepth("IMODGUIDIDENTIFIER",5)!=null)&&(window.name!=null)&&(window.name.length>0)){_cmPartnerUtils.loadScript(C8(null)+"//"+cm_MC_APP_SERVER_HOST+"/mcwebapp/js/easyXDM.js");_cmPartnerUtils.loadScript(C8(null)+"//"+cm_MC_APP_SERVER_HOST+"/mcwebapp/js/imodWebDesigner.js");_cmPartnerUtils.loadScript(C8(null)+"//"+cm_MC_APP_SERVER_HOST+"/mcwebapp/js/json2.js")}}catch(b){}}if(typeof($f126)==="undefined"&&!CM_DDX.dispatcherLoadRequested){CM_DDX.dispatcherLoadRequested=true;$cm_client_id=CM_DDX.cVA;var d=(CM_DDX.version>0)?"-v"+CM_DDX.version:"";if(CM_DDX.version>=2){_cmPartnerUtils.loadScript(C8(null)+"//tmscdn.coremetrics.com/tms/dispatcher"+d+".js")}if(CM_DDX.version>=3&&!cm_ATEnabled){_cmPartnerUtils.AT_RulesSet=true}}}_cmMc.getWebDesignerDependentScriptUrl=function(){var a=unica_imod.getWebDesignerScriptBaseUrl();if(a!=null){return a+"easyXDM.js"}else{return null}};_cmMc.getIframeMaxDepth=function(b,f){var e=parent;var a=null;var d=1;while(a==null&&e!=null&&(f==null||d<=f)){a=e.frames[b];var c=e;e=e.parent;if(e==c){e=null}d++}return a};var cI=cI;var cE=cE;function cmStartTagSet(){if(cG8){return false}cG8=[];cG8[0]=new _cm();cG9=1;return true}function cmAddShared(a,b){if(cG8){cG8[0][a]=b}}function cmSendTagSet(){var a;var c=cG8;var b=null,d=0;for(d=0;d<c.length;d++){if(typeof c[d]._$cmlch==="function"){b=c[d];break}}while((a=C7(arguments[0]))!=null){if(b){c9.call(b,a,c[0].ci)}else{c9(a,c[0].ci)}}cG8=null}function _cmCQ(b,c,a){this.pl=b;this.hosts=c.split(",");if(a){this.qs=a}this.cM5=CR}function CR(){var b=arguments;var c=b[0]?b[0]:this.hosts[0];return this.pl+"//"+c+(this.qs?this.qs:"")}function _cmt(){this.cM0={};this.uls={};this.rfs={};this.cTI=[];this.cPE=0;this.normalizeURL=c2;this.getPageID=c1;this.getPluginPageID=cmGetPluginPageID}function cmGetPluginPageID(f){var e="",c=cm_ClientID.split(";"),d=f.split("|")[0],b=f.split("|")[1];for(var g=0;g<c.length;g++){if(d==c[g].split("|")[0]){if(b){b=b.split(":");for(var a=0;a<b.length;a++){if(c[g].split("|")[1]&&(c[g].split("|")[1].toUpperCase().indexOf(b[a].toUpperCase())>-1)){e=cm_ClientID;break}}break}else{e=cm_ClientID;break}}}return this.getPageID(e)}function c1(a){var b=cG7.cM0[a];return b?b:""}function CS(b){var a=cG7.uls[b];if(!a){a=window.location.href}return a?a:""}function CT(b){var a=cG7.rfs[b];if(!a){a=cG6.referrer}return a?a:""}function CP(d){var e=cGT;if(!e){e=cGT=cG7.normalizeURL(window.location.href,false)}var c=d.indexOf("#");if(c>=0&&c<=e.length){var b=e.indexOf("#");if(b<0){b=e.length}if(d.substring(0,c)==e.substring(0,b)){return d.substring(c)}}return d}function c2(b,a){if(a){b=CP(b);var c=window.location.protocol+"//"+window.location.host;if(b.indexOf(c)==0){b=b.substring(c.length)}}return cD(b)}function c4(){for(var a in cmUA){if(cGM.indexOf(a)!=-1){return cmUA[a]}}return cmDefaultLimit}function C0(a){if(cG7){if(cG7.cTI&&cG7.cTI[a]){cG7.cTI[a].cmLD=true;if(cG7.cTI[a].ci){cmJSFSetValidFlagValue(cmValidFlag_SessionContinue,false,cG7.cTI[a].ci);cmJSFSetSessionCookies(false,cG7.cTI[a].ci)}}cG7.cPE--;if(cG7.onResponse){cG7.onResponse(a)}}window.dontExit=false}function CN(b){if(cG7){cG7.cPE--;var a=null;if(cG7.cTI&&cG7.cTI[b]){a=cG7.cTI[b];a.cmLD=true}if(cG7.onError&&(!a||!a.cmTO)){cG7.onError(3,a)}}}function c6(a,b){if(cG3){cG3[a]=true}C0(b)}function CO(b){if(cG7&&cG7.cTI&&cG7.cTI[b]&&!(cG7.cTI[b].cmLD)){var a=cG7.cTI[b];a.cmTO=a.src;if(cG7.onError){cG7.onError(4,a.cmTO)}}}function c8(b){if(!cG3||cG3[b]){return true}var a=new Date();if((a.getTime()-cG2[b])>cG4){return true}return false}function CV(h,e,b){if((CM_DDX.version>=3)&&CM_DDX.standalone){return}b=b||cm_ClientID;var d=function(i){var k=false;return function(){if(!k){if(typeof i==="function"){i()}k=true}}}(this._$cmlch);if((!cG2[h]||c8(h))&&(cm_OffsiteImpressionsEnabled==false||cm_Avid!=null||cm_AvidLoadTimedOut)){var c=new Image();var f=cmIndex;cG7.cTI[cmIndex++]=c;if(!cG2[h]){var g=new Date();cG2[h]=g.getTime();c.onload=function(){c6(h,f);d()}}else{c.onload=function(){C0(f);d()}}c.onerror=function(){CN(f);d()};if(cm_OffsiteImpressionsEnabled&&(cm_Avid!=null)&&(cm_Avid!="none")){e+="&avid="+cm_Avid}var a=c4();if(e.length>a){e=e.substring(0,a-6)+"&err=O"}if(cG7.onTagSent){cG7.onTagSent(e,f)}c.src=e;c.ci=b;setTimeout(function(){CO(f);d()},cm_TimeoutSecs*1000)}else{setTimeout(function(){CV(h,e,b)},cG5)}}function c9(a,c){if(cmOptedOut()){return}for(var d=0;d<a.hosts.length;d++){var b=a.cM5(a.hosts[d]);cG7.cPE++;CV.call(this,a.hosts[d],b,c)}}function cC(){var a=null;if(!this.ul){if(this.tid=="8"||(this.tid=="9"||this.tid=="10")){this.ul=window.location.protocol+"//"+window.location.hostname}else{this.ul=window.location.href}}if(cG8){cG8[cG9++]=this}else{var b=this.getImgSrc(arguments[0],1);c9.call(this,b,this.ci);a=b}return a}function cmLogError(a){}function C4(d,e,c){if(!c){if(!d.rf){if(!document.referrer){e.rf=""}else{e.rf=document.referrer}}else{if(d!=e){e.rf=d.rf}}if(!d.ul||d.ul==""||d.ul=="(none)"){e.ul=window.location.href}else{if(d!=e){e.ul=d.ul}}var b=cG7.normalizeURL(e.ul,false);var a=cG7.normalizeURL(e.rf,false);if(b!=""){e.ul=b}if(a!=""){e.rf=a}}}function C6(o,d,h){var i="";if(o.tid){i+="tid="+o.tid}var a=(o.tid==1||(o.pc&&o.pc.charAt(0)=="Y"));for(var k in o){if(k=="qs"||k=="tid"||k=="topline"){continue}if(o[k]!==0&&(!o[k]||o[k]==""||typeof(o[k])==="function")){continue}if(d&&d[k]&&d[k]==o[k]){continue}if(i!=""){i+="&"}i+=cD(k)+"="+cE(cD(o[k]))}if(!o.rs&&o.ci){if(o.pi&&a){cG7.cM0[o.ci]=o.pi}if(o.ul){cG7.uls[o.ci]=o.ul}if(o.rf){cG7.rfs[o.ci]=o.rf}}if(d&&cm_SecureTags.indexOf("|"+o.tid+"|")!=-1){d.protocol="https:"}if(cm_JSFEnabled&&!h){cmJSFSetSessionCookies(false,o.ci);i+=(i!=""?"&":"")+cmCore_JSFParamEnabled+"=1";var m=cmJSFGetUserId();i+="&"+cmCore_JSFParamUserID+"="+(m!=null?m:"");i+="&"+cmCore_JSFParamSessionID+"="+cmJSFGetSessionValue(o.ci);i+="&"+cmCore_JSFParamValidFlag+"="+cmJSFGetValidFlagValue(o.ci)}if(cm_PartnerDataClientIDs&&o.tid){try{var n={};for(var l in o){var b=o[l];if(typeof(b)!="function"&&typeof(b)!="undefined"){if(l=="ci"){b=b.split(";");for(var g=0;g<b.length;g++){b[g]=b[g].split("|")[0]}b=b.join(";")}}n[l]=b}if(d){for(var l in d){var b=d[l];if(typeof(b)!="function"&&typeof(b)!="undefined"){if(l=="ci"){b=b.split(";");for(var g=0;g<b.length;g++){b[g]=b[g].split("|")[0]}b=b.join(";")}}n[l]=b}}n.calculateTopLineAndReturnSegments=o.calculateTopLineAndReturnSegments;if((cm_ATEnabled&&_cmPartnerUtils.AT_RulesSet)||CM_DDX.version>=3){if(_cm_NRSet){_cmPartnerUtils.calculateAndSendATData(n)}else{_cmPartnerUtils.AT_TagQueue.push(n)}}}catch(f){}}var c=_cmPartnerUtils.copyTag(o,d);if(c.tid){_cmMc.mcTagQueue.push(c);if(cmMarketing.INSTANCE!==null){cmMarketing.INSTANCE.tagCallTriggered()}else{_cmMc.callPending.tagCallTriggered=true}}return i}_cmPartnerUtils.copyTag=function(a,e){var c={};for(var b in a){var d=a[b];if(typeof(d)!="function"&&typeof(d)!="undefined"){c[b]=d}}if(e){for(var b in e){var d=e[b];if(typeof(d)!="function"&&typeof(d)!="undefined"){c[b]=d}}}c.calculateTopLineAndReturnSegments=a.calculateTopLineAndReturnSegments;return c};function C8(b){var a=location.protocol;if(b&&b.protocol){a=b.protocol}if(a!="http:"&&a!="https:"){a="http:"}return a}function c0(){var c=arguments;C4(this,this,c[0]);var e={};var b=C6(this,e);var d=new _cmCQ(C8(e),cm_HOST,b);return c[1]?d:d.cM5()}function C7(){var f,n,b,r,e,c,o,d,k,q,g;if(!cG8||cG8.length<2){return null}f=cG8[0];n=cG8[1];f.ci=n.ci;for(k=1;k<cG8.length;k++){if(f.ci.indexOf(cG8[k].ci)==-1){f.ci+=";"+cG8[k].ci}if(cm_SecureTags.indexOf("|"+cG8[k].tid+"|")!=-1){f.protocol="https:"}}for(k=0;k<cG0.length;k++){b=cG0[k];if(!f[b]){f[b]=n[b]}}r=arguments;C4(n,f,r[0]);e=C8(f);g=new _cmCQ(e,cm_HOST);g.qs=C6(f);c=c4();o=0;for(var m=0;m<g.hosts.length;m++){d=e.length+g.hosts[m].length+g.qs.length;if(d>o){o=d}}for(k=1;k<cG8.length;k++){q=C6(cG8[k],f,true);if(k>1&&o+q.length+1>c){for(j=1;j<cG8.length-k+1;j++){cG8[j]=cG8[j+k-1]}cG8.length=cG8.length-k+1;break}o+=q.length+1;g.qs+="&"+q}if(k==cG8.length){cG8=null}return g}function _cm(){var d,b=arguments;this.ci=cm_ClientID;for(d=0;d<b.length;d++){this[b[d]]=b[++d]}this.write=cC;this.getImgSrc=c0;this.writeImg=cC;this.st=cm_ClientTS;this.vn1="http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/4.14.33";if(cF(5.5)||!cF(0)){var c=(cm_UseUTF8&&encodeURIComponent)||cGU?"utf-8":cG6.charset;if(!c){c=cG6.defaultCharset}if(!c){c=cG6.characterSet}this.ec=c}this.topline=[]}function cD(a){var b="";a=b+(!a&&a!==0?"":a);return a.split("'").join(b).split('"').join(b).split("\r").join(b).split("\n").join(b)}function cE(d){var c=0,b;while(d.charAt(c)==" "&&c!=d.length){c++}b=d.length-1;while(d.charAt(b)==" "&&b!=0){b--}d=d.substring(c,b+1);if(cm_UseUTF8&&encodeURIComponent){d=encodeURIComponent(d)}else{d=preEscape(d);d=escape(d);var a=new RegExp("%25u00","g");d=d.replace(a,"%u00")}d=d.split("+").join("%2B");return d}function preEscape(c){for(var b=160;b<256;b++){var a=new RegExp(String.fromCharCode(b),"g");c=c.replace(a,"%u00"+b.toString(16))}return c}function cF(a){var b=cGM.indexOf("MSIE");if(b!=-1){return(parseFloat(cGM.substring(b+5))>=a)}return false}function CD(a){return(cGN.indexOf("Gecko")!=-1&&parseInt(cGM)>=a)}function cI(b,a,d){var c=cG6.cookie;var e=cJ(b,c,";");if(!a||!e){if(!e&&d!=null){return d}return e}e=cJ(a,e,"&");if(!e&&d!=null){return d}return unescape(e)}function CL(){var e,a,b,d,f=0;a=cG6.cookie;if(a){e=a.split(";");f=e.length;for(d=0;d<e.length;d++){b=e[d].split("=");if(b.length<2||b[1]==null||b[1]==""){f--}}}return f}function CB(b,h,d,g){if(cmCookiesDisabled()){return true}var f,a,e,c=cG6.cookie;f=null;a=h.length+1;if(!cI(b)){a+=b.length}if(a>4096){f=1}else{if(c){if(CL()>=50){f=2}}}if(f){if(cG7.onError){cG7.onError(f,name)}return false}e=b+"="+h+"; path=/";if(g){e+="; domain="+g}if(d&&!cmSessionCookiesOnly()){e+="; expires="+d}cG6.cookie=e;return true}function cmSetSubCookie(m,k,i,b,f){var e=cI(m);var h;if(!e){h=k+"="+i}else{var l="&";var g=k+"=";var c=e.indexOf(g);if(c>=0){if(c>0&&e.charAt(c-1)!=l){c=e.indexOf(l+g);if(c>=0){c++}}}if(c>=0){var a=c+k.length+1;var d=e.indexOf(l,a);if(d<0){d=e.length}h=e.substring(0,a)+i+e.substring(d)}else{h=e+l+k+"="+i}}CB(m,h,b,f)}function CC(a,d){var b=cI(a);if(b!=null){var c=new Date();c.setYear(1973);var b=a+"=; path=/; expires="+c.toGMTString();if(d){b+="; domain="+d}cG6.cookie=b}return b}function cJ(a,h,c){var g,e,d,b,f=null;g=a+"=";e=c+" ";d=h.indexOf(e+g);if(d==-1){e=c;d=h.indexOf(e+g)}if(d==-1){d=h.indexOf(g);if(d!=0){return null}}else{d+=e.length}b=h.indexOf(e,d);if(b==-1){b=h.length}return h.substring(d+g.length,b)}function CG(c){if(!c){c=window.event}var a=[c.currentTarget,c.target,c.srcElement];for(var b=0;b<a.length;b++){if(a[b]){return a[b]}}}function CU(a,b,g,c,f){var e,d;a.pi=g?g:c1(b);if(cGQ){if(c||f){a.ul=c?c:"";a.rf=f?f:""}else{e=CS(b);d=CT(b);if(a.pi==""||e.indexOf("cm_")>0||(d!=""&&d.indexOf(window.location.protocol+"//"+window.location.host)!=0)){a.ul=e;a.rf=d}}}}function cL(g,e,c,h,a,i,m,f,l,b,d){var k=new _cm("tid","10");CU(k,k.ci,f,l,b);k.st=g;k.ti=e;k.fo=c;k.ac=h;k.hr=a;k.fi=i;k._$cmlch=d;if(m){k.rs="Y"}k.write(1)}function cM(h,a,b,c,n,g,m,e,f){var l=new _cm("tid","8");if(typeof(encodeURI)==="function"){c=encodeURI(c).replace(/%25/g,"%")}CU(l,l.ci,g,m,e);l.st=h;l.ti=a;l.nm=b;l.hr=c;l._$cmlch=f;var i=c.indexOf("cm_cr=");var d=c.indexOf("cm_me=");if(i>-1){var k=c.indexOf("&",i);if(k==-1){l.cm_cr=c.substring(i+6)}else{l.cm_cr=c.substring(i+6,k)}}if(d>-1){var k=c.indexOf("&",d);if(k==-1){l.cm_me=c.substring(d+6)}else{l.cm_me=c.substring(d+6,k)}}if(n){l.rs="Y"}l.write(1)}function CM(d){var f,b,c,e;if((f=d.indexOf("?"))==-1){f=d.lastIndexOf("/")}if(f!=-1){b=d.indexOf("#",f);if(b==-1){b=d.length}while(f!=-1&&f<b){f=d.indexOf("cm_",f);if(f!=-1){c=d.indexOf("&",f);if(c==-1){c=b}e=d.indexOf("=",f);if(e!=-1&&e<c){this[d.substring(f,e)]=d.substring(e+1,c)}f=c}}}}function CK(b,f,e,a,d){var k,i,c,l,g,h;if((f||e||a||d)&&b){k=new _cm("tid","9");i=new CM(CP(b));if(f){c=k.cm_sp_o=i.cm_sp_o;if(!c){c=k.cm_sp=i.cm_sp}}if(e){l=k.cm_re_o=i.cm_re_o;if(!l){l=k.cm_re=i.cm_re}}if(a){if(b.indexOf("#")==-1){g=k.cm_cr=i.cm_cr}}if(d){h=k.cm_me=i.cm_me}if(c||l||g||h){k.pi=c1(k.ci);k.st=cm_ClientTS;if(typeof cmCheckIgnoreImpression=="function"){if(cmCheckIgnoreImpression(c,l,g,h)){k.write(1)}}else{k.write(1)}}}}function cmFormBlurRecord(a){if(a.cmFormEleMemValue!=cmFormElementValue(a)&&a.cmFormEleMemValue!=null){cmFormReportInteraction(a)}a.form.cmEleValue=-1}function cmFormElementOnclickEvent(){try{var b;var a=cmFormElementValue(this);if((cmFormC1.indexOf(this.type)>=0)||(this.cmFormEleMemValue!=a)){if(this.type=="radio"){for(b=0;b<this.form.elements.length;b++){if(this.form.elements[b].cM2==this.cM2){this.form.elements[b].cmFormEleMemValue=null}}}this.cmFormEleMemValue=a;cmFormReportInteraction(this)}}catch(c){cmLogError(c)}}function cmFormElementOnfocusEvent(){try{this.form.cmEleValue=this.cM2;this.cmFormEleMemValue=cmFormElementValue(this)}catch(a){cmLogError(a)}}function cmFormElementOnblurEvent(){try{cmFormBlurRecord(this)}catch(a){cmLogError(a)}}function cmFormElementOnchangeEvent(){try{cmFormReportInteraction(this)}catch(a){cmLogError(a)}}function cmFormElementValue(c){var a;if(c.type=="checkbox"){return c.checked}else{if((cmFormC3.indexOf(c.type)>=0)&&c.options){var b="";for(a=0;a<c.options.length;a++){if(c.options[a].selected==true){b=b+c.options[a].index}}return b}else{if(cmFormC2.indexOf(c.type)>=0||c.type=="file"||c.type=="radio"){return c.value}else{return null}}}}function cO(g,h,d,c){var b,a,i,l="";var f=null;h=g+":"+h;if(g!=-1){if(cG6.forms[g]){f=cG6.forms[g];var l=f.attributes;a=f.action?f.action:l.action.nodeValue?l.action.nodeValue:l.getNamedItem("action").value?l.getNamedItem("action").value:""}}cGD=cG6.cmTagCtl.normalizeFORM(cGD);var k=c1(cm_ClientID);if(cm_FormPageID&&k!=""){var e=cGD.split(";");cGD="";for(i=0;i<e.length-1;i++){cGD+=k.split(":").join("").split(";").join("")+"_"+e[i]+";"}cm_FormPageID=false}if(cV(a)&&(g!="-1"||(g=="-1"&&cmSubmitFlag==false))){b=new Date();cGH=b.getTime();cGF=h;cGE=cG7.normalizeURL(a,true);cL(cm_ClientTS,cGH,cGD,cGF,cGE,cGC,false,c,c,c,d);cGG=cGC;cGC="";if((f)&&(typeof cmCustomFormSubmitHandler=="function")){cmCustomFormSubmitHandler(f,h)}}else{cGF=""}}function cmFormOnresetEvent(){var a;try{cO(this.cM1,"R")}catch(b){cmLogError(b)}try{for(a=0;a<cG6.forms[this.cM1].elements.length;a++){cG6.forms[this.cM1].elements[a].cmFormEleMemValue=false}}catch(b){cmLogError(b)}try{if(this.cQ){return this.cQ()}}catch(b){cmLogError(b)}}function cmFormOnsubmitEvent(c,a,d){try{if(this.cmEleValue>-1){cmFormBlurRecord(this.elements[this.cmEleValue])}}catch(b){cmLogError(b)}try{if(this.cM1>=0&&this.cmSubmitIndex==false){cmSubmitFlag=true;this.cmSubmitIndex=true;cO(this?this.cM1:-1,"S",a);CE()}else{if(typeof a==="function"){a()}}}catch(b){cmLogError(b)}cmJSFPMigrateLink(this,"action")}function cmFormReportInteraction(c){var b=cG6.cmTagCtl.normalizeFIELDS(c.name?c.name:c.id?c.id:"");var a=cGC+c.form.cM1+":"+c.cM2+":"+b.split(":").join("|").split(";").join("|")+";";if(a.length<1000){cGC=a}}function cmFormSubmit(){cmJSFPMigrateLink(this,"action");try{if(this.cmEleValue>-1){cmFormBlurRecord(this.elements[this.cmEleValue])}}catch(b){cmLogError(b)}try{if(this.cM1>=0&&this.cmSubmitIndex==false){cmSubmitFlag=true;this.cmSubmitIndex=true;cO(this?this.cM1:-1,"S");CE()}}catch(b){cmLogError(b)}try{if(cm_LinkClickDelay){setTimeout((function a(c){return function(){c.cmSubmit()}}(this)),cm_LinkClickDelayInterval);return false}else{this.cmSubmit()}}catch(b){cmLogError(b)}}cG6.cmTagCtl.normalizeFORM=function(a){return a};cG6.cmTagCtl.normalizeFIELDS=function(a){return a};function cU(){if(cm_SkipHandlerReg.indexOf("F")==-1){_$cF1();var c,f,b,a,k,g,h;for(c=0;c<cG6.forms.length;c++){f=cG6.forms[c];h=0;if(!f.cM1&&!f.cmEleValue&&!f.cmSubmitIndex){f.cM1=c;f.cmEleValue=-1;f.cmSubmitIndex=false;f.radiogroup={key:"value"};try{if(cF(5)&&!cF(8)){var d=f.attributes;b=d.name?d.name.nodeValue:d.id?d.id.nodeValue:d.action?d.action.nodeValue:"UNDEFINED"}else{if(f.attributes.getNamedItem){b=((f.attributes.getNamedItem("name"))&&(f.attributes.getNamedItem("name").value!==""))?f.attributes.getNamedItem("name").value:((f.attributes.getNamedItem("id"))&&(f.attributes.getNamedItem("id").value!==""))?f.attributes.getNamedItem("id").value:((f.attributes.getNamedItem("action"))&&(f.attributes.getNamedItem("action").value!==""))?f.attributes.getNamedItem("action").value:"UNDEFINED"}else{b=f.name?f.name:f.id?f.id:f.action?f.action:"UNDEFINED"}}}catch(k){b="ERROR";cmLogError(k)}cGD+=b+":"+c+";";try{if(f.submit!==cmFormSubmit){f.cmSubmit=f.submit;f.submit=cmFormSubmit}}catch(k){cmLogError(k)}if(typeof cm_NewFormTracker!=="undefined"){if(!f._$cV2){_$cF4(f)}}else{cmAddNewEvent(f,"submit",cmFormOnsubmitEvent)}cmAddNewEvent(f,"reset",cmFormOnresetEvent);for(a=0;a<f.elements.length;a++){k=f.elements[a];if(!k.cM1&&!k.cM2&&!k.cmFormEleMemValue){k.cM1=c;k.cM2=h;k.cmFormEleMemValue=null;h++;if(k.type=="radio"){g=k.name?k.name:k.id?k.id:"";if(g!=""){if(f.radiogroup[g]){k.cM2=f.radiogroup[g]}else{f.radiogroup[g]=k.cM2}}}if(cmFormC1.indexOf(k.type)>=0||k.type=="checkbox"||k.type=="radio"){try{cmAddNewEvent(k,"click",cmFormElementOnclickEvent)}catch(k){cmLogError(k)}}if(cmFormC2.indexOf(k.type)>=0||cmFormC3.indexOf(k.type)>=0){try{cmAddNewEvent(k,"focus",cmFormElementOnfocusEvent);cmAddNewEvent(k,"blur",cmFormElementOnblurEvent)}catch(k){cmLogError(k)}}if(k.type=="file"){try{cmAddNewEvent(k,"change",cmFormElementOnchangeEvent)}catch(k){cmLogError(k)}}}}}}}}function _$cF1(){if(typeof cm_NewFormTracker!=="undefined"&&cm_NewFormTracker.submitFunctions){var fnCounts={};var frm=null;var count=0;var fnToFrm={};for(frm in cm_NewFormTracker.submitFunctions){count=fnCounts[cm_NewFormTracker.submitFunctions[frm]];if(!count){count=0}count++;fnCounts[cm_NewFormTracker.submitFunctions[frm]]=count;fnToFrm[cm_NewFormTracker.submitFunctions[frm]]=frm}var funcName=null;for(funcName in fnToFrm){if(fnCounts[funcName]>1){cmUtils.console.error("Function "+funcName+" defined "+fnCounts[funcName]+" times. Hence ignoring.")}else{frm=_$cF2(fnToFrm[funcName]);if(frm&&!frm._$cV2){frm._$cV2=true;var newFuncName=funcName+Math.floor((Math.random()*10000)+1);window[newFuncName]=eval(funcName);window[funcName]=(function(f,fn){return function(){var args=arguments;var fnRealOnClick=function(){var retVal=fn.apply(this,args);if(retVal&&f){f.submit()}};if(f){cmFormOnsubmitEvent.call(f,undefined,fnRealOnClick)}return false}}(frm,window[newFuncName]))}}}}}function _$cF2(a){var c=a;if(typeof a==="string"){c=document.getElementById(a);if(!c){var b=document.getElementsByName(a);if(b.length>0){c=b[0]}b=null}}if(c&&typeof c==="object"&&c.tagName=="FORM"){return c}return null}function _$cF4(a){var b=function(d,c){return function(f){if(!f){f=window.event}var e=function(){var g=false;return function(){if(!g){g=true;var h=c&&c();d.onsubmit=function(){return h};d.submit()}}}();cmFormOnsubmitEvent.call(f.srcElement?f.srcElement:f.target,f,e);if(f.preventDefault){f.preventDefault()}else{f.returnValue=false}return false}}(a,a.onsubmit);a.onsubmit=b;a=null}function cV(d){if(cm_TrackLink==true||cm_TrackLink=="A"){return true}else{if(cm_TrackLink=="E"&&d.indexOf("/")!=0){return true}var f;if((f=cm_DownloadExtensions)!=null){var c=d.lastIndexOf(".");if(c!=-1){var a=d.substring(c);for(var b=0;b<f.length;b++){if(a==f[b]){return true}}}}return false}}function cW(b,a){CI();var b=CG(b);if(b){C9(b,a)}CJ(1);CE();if(a){setTimeout(a,cm_LinkClickDelayInterval)}}function C9(g,b,i){cGI="";cGJ="";cGK="";var c=g.tagName.toUpperCase();if(c=="AREA"){cGJ=g.href?g.href:"";var f=g.parentElement?g.parentElement:g.parentNode;if(f!=null){cGI=f.name?f.name:(f.title?f.title:(f.id?f.id:""))}}else{while(c!="A"&&c!="HTML"){if(!g.parentElement){if(g.parentNode){g=g.parentNode}else{break}}else{g=g.parentElement}if(g){c=g.tagName.toUpperCase()}}if(c=="A"){cGJ=g.href?g.href:"";cGI=g.name?g.name:(g.title?g.title:(g.id?g.id:""))}}if(g.getAttribute){var h=g.getAttribute("manual_cm_re");if(h){cGJ=cGJ.split("#");cGJ[0]=cGJ[0]+((cGJ[0].indexOf("?")>-1)?"&":"?")+"cm_re="+h;cGJ=cGJ.join("#")}var a=g.getAttribute("manual_cm_sp");if(a){cGJ=cGJ.split("#");cGJ[0]=cGJ[0]+((cGJ[0].indexOf("?")>-1)?"&":"?")+"cm_sp="+a;cGJ=cGJ.join("#")}}cGJ=cG7.normalizeURL(cGJ,true);if(cV(cGJ)==true){var d=new Date();cGK=d.getTime();if(typeof cmCustomLinkClickHandler=="function"){cmCustomLinkClickHandler(g)}cM(cm_ClientTS,cGK,cGI,cGJ,false,i,i,i,b)}else{cGJ=""}cmJSFPMigrateLink(g,"href")}function cmAddNewEvent(e,d,f,g,b){if(e.addEventListener){e.addEventListener(d,f,false)}else{if(e.attachEvent){g=g||d;b=b||f;var a=g+b,c="e"+a;if(typeof e[c]==="undefined"){e[c]=b;e[a]=function(){e[c](window.event)};e.attachEvent("on"+g,e[a])}}}}function cX(a){if(cm_ClientID!=="99999999"&&c1(cm_ClientID)!==""){cmAddClicksAndThrowImpressions(a)}else{cmAddClickHandlers();if(a==="onload"){setTimeout(cmThrowImpressionTags,10)}}}function cmAddClicksAndThrowImpressions(k){CI();var e,f,a,g,d,b,c;a=cm_TrackImpressions;g=(a.indexOf("S")!=-1);d=(a.indexOf("R")!=-1);b=(a.indexOf("C")!=-1);c=(a.indexOf("M")!=-1);for(e=0;e<cG6.links.length;e++){f=cG6.links[e];if(cm_SkipHandlerReg.indexOf("L")==-1){_$cF5(f)}if(k=="onload"){var h=f.href;if(f.getAttribute("manual_cm_re")){h=h.split("#");h[0]=h[0]+((h[0].indexOf("?")>-1)?"&":"?")+"cm_re="+f.getAttribute("manual_cm_re");h=h.join("#")}if(f.getAttribute("manual_cm_sp")){h=h.split("#");h[0]=h[0]+((h[0].indexOf("?")>-1)?"&":"?")+"cm_sp="+f.getAttribute("manual_cm_sp");h=h.join("#")}if(!f.cmImpressionSent){CK(h,g,d,b,c);f.cmImpressionSent=1}}}CJ(1)}function cmAddClickHandlers(){var b,a;for(a=0;a<cG6.links.length;a++){b=cG6.links[a];if(cm_SkipHandlerReg.indexOf("L")==-1){_$cF5(b)}}}function _$cF5(a){var b=cmUtils.string.trim(a.href).toLowerCase();if(!cm_NewLinkTracker||a.className.indexOf("cmUseOldLinkTracker")!=-1||b.indexOf("#")===0||b.indexOf("javascript:")===0||b.indexOf(location.href.toLowerCase()+"#")===0){cmAddNewEvent(a,"click",cW)}else{if(!a._$cF6){var c=a.onclick;a.onclick=null;a._$cF6=function(i){if(!i){i=window.event}var h,e;h=e=this.href;var g=i.srcElement?i.srcElement:i.target;var f=g.tagName.toUpperCase();if(f!=="AREA"&&f!=="A"){while(f!=="A"){g=g.parentElement?g.parentElement:g.parentNode;if(g){f=g.tagName.toUpperCase()}}}if(g){e=g.href}if(e===h){var k=false;if(g.target===window.name||g.target==="_self"){var d=(function(l,o,n){var m=false;return function(){if(!m){m=true;if(!o||o.call(n)!==false){location.href=l}}}})(e,c,g)}else{if(c){k=c.call(g)}}cW(i,d);if(g.target===window.name||g.target==="_self"||k){if(i.preventDefault){i.preventDefault()}else{i.returnValue=false}}}};cmAddNewEvent(a,"click",a._$cF6)}}}function cmThrowImpressionTags(){if(cm_ClientID==="99999999"||c1(cm_ClientID)===""){setTimeout(cmThrowImpressionTags,10);return}CI();var f,h,e,d,a,c,b;e=cm_TrackImpressions;d=(e.indexOf("S")!=-1);a=(e.indexOf("R")!=-1);c=(e.indexOf("C")!=-1);b=(e.indexOf("M")!=-1);for(f=0;f<cG6.links.length;f++){h=cG6.links[f];var g=h.href;if(h.getAttribute("manual_cm_re")){g=g.split("#");g[0]=g[0]+((g[0].indexOf("?")>-1)?"&":"?")+"cm_re="+h.getAttribute("manual_cm_re");g=g.join("#")}if(h.getAttribute("manual_cm_sp")){g=g.split("#");g[0]=g[0]+((g[0].indexOf("?")>-1)?"&":"?")+"cm_sp="+h.getAttribute("manual_cm_sp");g=g.join("#")}if(!h.cmImpressionSent){CK(g,d,a,c,b);h.cmImpressionSent=1}}CJ(1)}function cY(a){cmOnDomReady();window.setTimeout(function(){CM_DDX.firstPassFunctionBinding=true},CM_DDX.BAD_PAGE_ID_ELAPSED_TIMEOUT);if((cF(4)||CD(5))||cGS||cGU){window.cX("onload");cU()}}function cZ(h){cG3=null;CI();var b=false;for(var a=0;a<document.forms.length;a++){try{if(cG6.forms[a].cmEleValue>-1){cmFormBlurRecord(document.forms[a].elements[document.forms[a].cmEleValue])}}catch(h){cmLogError(h)}try{if(cGC!=""){b=true;cO(-1,"U")}}catch(h){cmLogError(h)}}CJ(1);if(b){window.dontExit=true;var f=new Date();var d=new Date();for(;window.dontExit&&(d-f<1000);){d=new Date()}}CE();if(cm_UseCookie&&cG7.cPE==0){var g=escape(c1(cm_ClientID));CB("cmRS","t3="+cmT3+"&pi="+g)}if(cG7.onUnload){cG7.onUnload()}if(cF(5)&&!cF(5.5)&&window.parent!=window){cG7.cTI=null}else{if(!cGU){for(var c=0;c<cG7.cTI.length;c++){cG7.cTI[c].onload=null;cG7.cTI[c].onerror=null}}}}function CE(){if(cm_UseCookie){cmT3=new Date().getTime();var b,a,f,d,c="";b=cGA?"&t4="+cGA:"";a=(cGJ!="")?"&lti="+cGK+"&ln="+escape(cGI)+"&hr="+escape(cGJ):"";f={};CU(f,cm_ClientID);var e="";if(cm_JSFEnabled){e="&cjen=1"}d="&t1="+cm_ClientTS+"&t3="+cmT3+b+a+"&fti="+cGH+"&fn="+escape(cGD)+"&ac="+cGF+"&fd="+escape(cGG)+"&fu="+escape(cGE)+"&pi="+escape(f.pi)+"&ho="+escape(cm_HOST)+"&ci="+escape(cm_ClientID);if(f.ul&&f.rf&&f.ul.length+f.rf.length<cGO){c="&ul="+escape(f.ul)+"&rf="+escape(f.rf)}if(!CB("cmRS",d+c+e)){if(!CB("cmRS",d+e)){CB("cmRS","t3="+cmT3+"&pi="+escape(f.pi)+e)}}}}function cmSetAvid(a){clearTimeout(cm_AvidLoadTimer);if(a){cm_Avid=a}else{cm_Avid="none"}CB("CMAVID",cm_Avid);cm_AvidLoadTimedOut=false}function cmJSFSetSessionCookies(b,c){if(!cm_JSFEnabled){return}var a=c.split(";");for(var d=0;d<a.length;d++){cmJSFSetSingleSessionCookie(b,a[d])}}function debugReadCookie(b){var e=b+"=";var a=document.cookie.split(";");for(var d=0;d<a.length;d++){var f=a[d];while(f.charAt(0)==" "){f=f.substring(1,f.length)}if(f.indexOf(e)==0){return f.substring(e.length,f.length)}}return null}function cmJSFGetCookieExpireDate(){var a=new Date();a.setTime(a.getTime()+(cm_JSFCoreCookieExpireSeconds*1000));return a.toGMTString()}function cmJSFGetUserId(){var a=cI(cm_JSFCoreCookieName);if(a){a=a.split("&",2)[0];if(a=="anonymous"||cmAnonymous()){a="1000000000000003"}}if(!a){a=cmIT()}return a}function cmJSFSetSingleSessionCookie(g,i,d){if(!cm_JSFEnabled){return}if(cmOptedOut()){return}var c=cI(cm_JSFCoreCookieName);if(c==null){c=cmJSFCreateUserId();if(cm_JSFTrackClients){c+="&ci="+i}CB(cm_JSFCoreCookieName,c,cmJSFGetCookieExpireDate(),cm_JSFPCookieDomain);if(!d){cmJSFSetSingleSessionCookie(true,i,true)}cmJSFSetValidFlagSingleValue(cmValidFlag_NewSession,false,i);cmJSFSetValidFlagSingleValue(cmValidFlag_NewVisitor,true,i);return}if(cm_JSFTrackClients){var k=cJ("ci",c,"&");k=k&&unescape(k);if(k){k=k.split(",").join("_")}if(k&&k.indexOf(i)<0){cmSetSubCookie(cm_JSFCoreCookieName,"ci",k+"_"+i,cmJSFGetCookieExpireDate(),cm_JSFPCookieDomain);k=cJ("ci",c,"&");k=k&&unescape(k);if(k.indexOf(i)>=0){if(!d){cmJSFSetSingleSessionCookie(true,i,true)}cmJSFSetValidFlagSingleValue(cmValidFlag_NewSession,false,i);cmJSFSetValidFlagSingleValue(cmValidFlag_NewVisitor,true,i);return}}}var l=(cmJSFGetSessionLoginCookieValue(i)!=null);if(!l){if(cmJSFCombineSessionCookies(i)){l=(cmJSFGetSessionLoginCookieValue(i)!=null)}}if(!l&&!g){if(!d){cmJSFSetSingleSessionCookie(true,i,true)}cmJSFSetValidFlagSingleValue(cmValidFlag_NewSession,true,i);return}var a=new Date();var b=a.getTime();var f=b+cm_JSFSessionTimeout*1000;var e=cmJSFIsSessionExpired(cmJSFGetSessionExpireCookieValue(i));if((g!=null&&g==true)||e){var h=b.toString();if(h.length<10){while(h.length<10){h="0"+h}}else{h=h.substring(0,10)}cmJSFSetSessionLoginCookieValue(i,h);if(e){cmJSFSetValidFlagSingleValue(cmValidFlag_SessionReset,true,i)}else{cmJSFSetValidFlagSingleValue(cmValidFlag_NewSession,true,i)}if(cm_JSFSessionType=="T"){cmJSFSetSessionExpiresCookieValue(i,f.toString())}}if(cm_JSFSessionType=="I"){cmJSFSetSessionExpiresCookieValue(i,f.toString())}}function cmJSFIsSessionExpired(b){if(b==null){return false}var a=new Date();if(a.getTime()>b){return true}else{return false}}function cmJSFCreateUserId(){var f=new Date();var d=Math.random();if(d==0){d=Math.random()}var b=Math.random();if(b==0){b=Math.random()}var c=d.toString().substring(2,4)+b.toString().substring(2,12)+f.getTime().toString();var a=c.length;var e=23;if(a<e){c=c+c.substring(a-(e-a),a)}if(a>e){c=c.substring(0,e)}return c}function cmJSFSetValidFlagValue(d,a,c){if(!cm_JSFEnabled){return}var b=c.split(";");for(var e=0;e<b.length;e++){cmJSFSetValidFlagSingleValue(d,a,b[e])}}function cmJSFSetValidFlagSingleValue(e,a,c){var b=null;var d=cmJSFGetSessionValidFlagCookieValue(c);if(d){var f=parseInt(d);if(!isNaN(f)){b=f}}if(b==null){b=cmValidFlag_SessionContinue}if(a){if(e==cmValidFlag_NewSession){b&=~cmValidFlag_SessionReset}if(e==cmValidFlag_SessionReset){b&=~cmValidFlag_NewSession}b|=e}else{b=e}b|=cmValidFlag_SessionContinue;cmJSFSetSessionValidFlagCookieValue(c,b)}function cmJSFGetClientIdForSession(a){if(cm_JSFEAMasterIDSessionCookie){a=a.split("|")[0]}return a}function cmJSFCreateSessionMigrationParamName(a){return cm_JSFPCookieMigrateSessionID+"_"+cmJSFGetClientIdForSession(a)}function cmJSFCreateCombinedSessionCookieName(a){return cmJSFGetClientIdForSession(a)+"_clogin"}function cmJSFCombineSessionCookies(b){var a=cI(b+"_login");var e=cI(b+"_expires");var d=cI(b+"_valid");if(a!=null&&e!=null&d!=null){var c="l="+a+"&e="+e+"&v="+d;CB(cmJSFCreateCombinedSessionCookieName(b),c,null,cm_JSFPCookieDomain);CC(b+"_login",cm_JSFPCookieDomain);CC(b+"_expires",cm_JSFPCookieDomain);CC(b+"_valid",cm_JSFPCookieDomain);return true}return false}function cmJSFSetSessionLoginCookieValue(a,b){cmSetSubCookie(cmJSFCreateCombinedSessionCookieName(a),"l",b,null,cm_JSFPCookieDomain)}function cmJSFSetSessionExpiresCookieValue(a,b){cmSetSubCookie(cmJSFCreateCombinedSessionCookieName(a),"e",b,null,cm_JSFPCookieDomain)}function cmJSFSetSessionValidFlagCookieValue(a,b){cmSetSubCookie(cmJSFCreateCombinedSessionCookieName(a),"v",b,null,cm_JSFPCookieDomain)}function cmJSFGetSessionLoginCookieValue(a){return cI(cmJSFCreateCombinedSessionCookieName(a),"l")}function cmJSFGetSessionExpireCookieValue(a){return cI(cmJSFCreateCombinedSessionCookieName(a),"e")}function cmJSFGetSessionValidFlagCookieValue(a){return cI(cmJSFCreateCombinedSessionCookieName(a),"v")}function cmJSFGetSessionValue(f){var e="";var d="";var b=f.split(";");for(var g=0;g<b.length;g++){var a=b[g];if(a==""){continue}var c=cmJSFGetSessionLoginCookieValue(a);e+=d+(c!=null?c:"");if(d==""){d="|"}}return e}function cmJSFGetValidFlagValue(f){var e="";var d="";var b=f.split(";");for(var g=0;g<b.length;g++){var a=b[g];if(a==""){continue}var c=cmJSFGetSessionValidFlagCookieValue(a);e+=d+(c!=null?c:"");if(d==""){d="|"}}return e}_cm.prototype.addTP=function(){coremetrics.getTechProps(this)};function cmJSFPMigrateCookies(b,m,n){if(b&&m&&cm_JSFEnabled&&cm_JSFPCookieMigrate){var e=cI(cm_JSFCoreCookieName),h,o,l,k;if(!e||cm_JSFPForceMigrateCookies){CB(cm_JSFCoreCookieName,b+(cm_JSFTrackClients?"&ci="+cm_ClientID.split(";").join(","):""),cmJSFGetCookieExpireDate(),cm_JSFPCookieDomain);h=(new Date().getTime()+cm_JSFSessionTimeout*1000).toString();o=cm_ClientID.split(";");for(k=0;k<o.length;++k){l=o[k];if(m[l]!==undefined){cmJSFSetSessionLoginCookieValue(l,m[l]);cmJSFSetSessionExpiresCookieValue(l,h);cmJSFSetSessionValidFlagCookieValue(l,"1")}}}}if(cm_JSFPCookieMigrate&&cm_JSFPMigrationOtherCookies!==null){var f=cm_JSFPMigrationOtherCookies.split(","),c,g,a,d;for(g=0;g<f.length;++g){c=f[g];if(n[c]!==undefined){a=cm_JSFPMigrationOtherCookiesExpireTimes[c];if(a){d=new Date();d.setTime(d.getTime()+parseInt(a));d=d.toGMTString()}else{d=null}CB(c,n[c],d,cm_JSFPCookieDomain)}}}}function cmJSFPMigrateLink(g,l){if(cm_JSFPCookieMigrate){var k=/:\/\/([a-z0-9_\-\.]+)/i.exec(g[l]),f,h,n,d,c,b,a,m;if(k){k=k[1]}if(k&&((k.indexOf(cm_JSFPCookieDomain)===-1)&&(g[l].toLowerCase().indexOf("javascript")!==0)&&((cm_JSFPMigrationDomainWhitelist!==null&&cmTextMatchList(k.toLowerCase(),cm_JSFPMigrationDomainWhitelist.split(",")))||(cm_JSFPMigrationDomainBlacklist!==null&&!(cmTextMatchList(k.toLowerCase(),cm_JSFPMigrationDomainBlacklist.split(","))))))||(cm_JSFPMigrationPathWhitelist!==null&&cmTextMatchList(g[l].toLowerCase(),cm_JSFPMigrationPathWhitelist.split(",")))){if(cm_JSFEnabled){f=cI(cm_JSFCoreCookieName);if(f){f=f.split("&",2)[0]}h=cm_ClientID.split(";");n="";for(d=0;d<h.length;++d){n+="&"+cmJSFCreateSessionMigrationParamName(h[d])+"="+cmJSFGetSessionLoginCookieValue(h[d])}g[l]+=(g[l].indexOf("?")>-1?"&":"?")+cm_JSFPCookieMigrateVisitorID+"="+f+n}if(cm_JSFPMigrationOtherCookies!==null){b=cm_JSFPMigrationOtherCookies.split(",");a="";for(c=0;c<b.length;++c){m=cI(b[c]);if(m){a+="&cm_mc_"+b[c]+"="+m}}a=(g[l].indexOf("?")>-1?"&":"?")+a.substring(1);g[l]+=a}}}}function cmTextMatchList(a,c){for(var b=0;b<c.length;++b){if(a.indexOf(c[b])>-1){return true}}return false}_cm.prototype.calculateTopLineAndReturnSegments=function cmCalculateTopLineAndReturnSegments(){var g=[];var h=_cmPartnerUtils.getContactCookieValues();var n=new Ctck();var F="";if(document.referrer){F=document.referrer}var o="";if(window.location.href){o=window.location.href}var y=false;for(var B in _cm_CMRules){if(_cm_CMRules.hasOwnProperty(B)){y=true;break}}var c=typeof(CM_DDX.notifySegmentProcessor)==="function";if(!y&&c){_cm_CMRules[CM_DDX.cVA]={cid:CM_DDX.cVA,segmentRules:[],tags:[],segments:[]};y=true}for(var x in _cm_CMRules){var E=_cm_CMRules[x];if(typeof(E)!="object"||typeof(E.cid)=="undefined"){continue}if(!this.topline[E.cid]){this.topline[E.cid]={}}this.topline[E.cid].pgct=h.getPgCt(E.cid);this.topline[E.cid].osshct=h.getOsshCt(E.cid);this.topline[E.cid].orders=h.getOrders(E.cid);this.topline[E.cid].sales=h.getSales(E.cid);this.topline[E.cid].itcartct=h.getItCartCt(E.cid);this.topline[E.cid].itpurct=h.getItPurCt(E.cid);this.topline[E.cid].pvct=h.getPvCt(E.cid);this.topline[E.cid].evpts=h.getEvPts(E.cid);this.topline[E.cid].evcomct=h.getEvComCt(E.cid);this.topline[E.cid].evinict=h.getEvIniCt(E.cid);this.topline[E.cid].elvct=h.getElvCt(E.cid);var v=true;if(h.getFpFlag(E.cid)){v=false}else{__cm_firstPageFlag=true}this.topline[E.cid].startTime=h.getStTime(E.cid);if(this.topline[E.cid].startTime==0){this.topline[E.cid].startTime=((new Date()).getTime()/1000)|0}this.topline[E.cid].slen=(((new Date()).getTime()/1000)|0)-this.topline[E.cid].startTime;this.topline[E.cid].n_r="";this.topline[E.cid].mkchnl="";this.topline[E.cid].mkpgm="";this.topline[E.cid].mkv="";this.topline[E.cid].mkc="";this.topline[E.cid].mkp="";this.topline[E.cid].mki="";this.topline[E.cid].cmguid="";this.topline[E.cid].natscheng="";this.topline[E.cid].natschtm="";this.topline[E.cid].refurl="";this.topline[E.cid].refsite="";this.topline[E.cid].enpg="";if(v){this.topline[E.cid].mkchnl=(new Crur()).DIRECT_LOAD_CHANNEL;if(this.pn){this.topline[E.cid].enpg=this.pn}this.topline[E.cid].n_r="NEW";if(!_cm_isNew){this.topline[E.cid].n_r="REPEAT"}var b=_cmPartnerUtils.parseVCPI(o);if(!b){b=_cmPartnerUtils.parseVCPI(F)}var w=_cmPartnerUtils.parseReferralURL(F);if(b&&b.length>0){this.topline[E.cid].mkchnl=w.MARKETING_PROGRAMS;this.topline[E.cid].mkpgm=b[0];this.topline[E.cid].mkv=b[1];this.topline[E.cid].mkc=b[2];this.topline[E.cid].mkp=b[3];this.topline[E.cid].mki=b[4];this.topline[E.cid].cmguid=b[5]}else{this.topline[E.cid].mkchnl=w.channel}this.topline[E.cid].refsite=w.refName;this.topline[E.cid].natscheng=w.natSearchEngine;this.topline[E.cid].natschtm=w.natSearchWord;this.topline[E.cid].refurl=F}if(typeof(__cm_firstPageFlag)!="undefined"&&__cm_firstPageFlag&&!this.topline[E.cid].enpg&&this.pn){this.topline[E.cid].enpg=this.pn}this.topline[E.cid].tzloc="";var d=new Date(2009,0,15);var m=Math.floor(d.getTimezoneOffset()/60);if(m==8){this.topline[E.cid].tzloc="LOS ANGELES"}else{if(m==7){this.topline[E.cid].tzloc="DENVER"}else{if(m==6){this.topline[E.cid].tzloc="CHICAGO"}else{if(m==5){this.topline[E.cid].tzloc="NEW YORK"}}}}if(this.tid!=1){if(this.tid==6||(this.pc&&(this.pc.indexOf("y")==0||this.pc.indexOf("Y")==0))){this.topline[E.cid].pgct++;if(this.se&&this.se.replace(/^\s*/,"").replace(/\s*$/,"")){this.topline[E.cid].osshct++}}}if(this.tid=="1"){this.topline[E.cid].pgct++;if(this.se&&this.se.replace(/^\s*/,"").replace(/\s*$/,"")){this.topline[E.cid].osshct++}}else{if(this.tid=="3"){this.topline[E.cid].orders++;if(this.tr&&parseFloat(this.tr)!=NaN){this.topline[E.cid].sales+=parseFloat(this.tr)}}else{if(this.tid=="4"){if(this.at&&this.at=="5"&&this.qt&&parseFloat(this.qt)!=NaN){this.topline[E.cid].itcartct+=parseFloat(this.qt)}if(this.at&&this.at=="9"&&this.qt&&parseFloat(this.qt)!=NaN){this.topline[E.cid].itpurct+=parseFloat(this.qt)}}else{if(this.tid=="5"){this.topline[E.cid].pvct++}else{if(this.tid=="14"){if(this.cpt&&parseFloat(this.cpt)!=NaN){this.topline[E.cid].evpts+=parseFloat(this.cpt)}if(this.cat&&this.cat=="2"){this.topline[E.cid].evcomct++}if(this.cat&&this.cat=="1"){this.topline[E.cid].evinict++}}else{if(this.tid=="15"){this.topline[E.cid].elvct++}}}}}}n.setPgCt(E.cid,this.topline[E.cid].pgct);n.setOsshCt(E.cid,this.topline[E.cid].osshct);n.setOrders(E.cid,this.topline[E.cid].orders);n.setSales(E.cid,this.topline[E.cid].sales);n.setItCartCt(E.cid,this.topline[E.cid].itcartct);n.setItPurCt(E.cid,this.topline[E.cid].itpurct);n.setPvCt(E.cid,this.topline[E.cid].pvct);n.setEvPts(E.cid,this.topline[E.cid].evpts);n.setEvComCt(E.cid,this.topline[E.cid].evcomct);n.setEvIniCt(E.cid,this.topline[E.cid].evinict);n.setElvCt(E.cid,this.topline[E.cid].elvct);n.setFpFlag(E.cid,"1");n.setStTime(E.cid,this.topline[E.cid].startTime)}for(var x in _cm_CMRules){var E=_cm_CMRules[x];if(typeof(E)!="object"||typeof(E.cid)=="undefined"){continue}if(c&&CM_DDX.cVA==E.cid){CM_DDX.notifySegmentProcessor(this,this.topline[E.cid])}var l=h.getSegRulesMet(E.cid);for(var z=0;z<E.segmentRules.length;z++){var t=E.segmentRules[z];if(l.indexOf(t.id+"_")==0||l.indexOf("_"+t.id+"_")!=-1){continue}var a=false;try{a=t.fn(this,this.topline[E.cid])}catch(D){}if(a){l+=t.id+"_"}}n.setSegRulesMet(E.cid,l);var C=h.getSegsMet(E.cid);for(var q=0;q<E.segments.length;q++){var f=E.segments[q];if(C.indexOf(f.id+"_")==0||C.indexOf("_"+f.id+"_")!=-1){continue}var p=true;for(var u=0;u<f.rules.length;u++){var A=f.rules[u];if(!(l.indexOf(A+"_")==0||l.indexOf("_"+A+"_")!=-1)){p=false;break}}if(p){if(!g[E.cid]){g[E.cid]=""}g[E.cid]+=f.id+"_";C+=f.id+"_"}}n.setSegsMet(E.cid,C)}if(y){_cmPartnerUtils.setContactCookieValues(n)}return g};_cmPartnerUtils.calculateAndSendATData=function(c){var a=c.calculateTopLineAndReturnSegments();var d=_cmPartnerUtils.cmGetPartnerRequestArray(c,a);for(var b=0;b<d.length;b++){c9(d[b])}};_cmPartnerUtils.loadScript=function(b){if(cm_UseDOMScriptLoad){try{var a=cG6.getElementsByTagName("head").item(0);var d=cG6.createElement("script");d.setAttribute("language","javascript");d.setAttribute("type","text/javascript");d.setAttribute("src",b);a.appendChild(d)}catch(c){}}else{cG6.write('<script language="javascript1.1" src="'+b+'"><\/script>')}};_cmPartnerUtils.cmGetPartnerRequestArray=function(g,h){var A=[];if(!g.ci){return A}var x="";if(g.rf){x=g.rf}else{if(document.referrer){x=document.referrer}}var l="";if(g.ul){l=g.ul}else{if(window.location.href){l=window.location.href}}for(var r in _cm_CMRules){var w=_cm_CMRules[r];if(typeof(w)!="object"){continue}if((g.ci+"").indexOf(w.cid+"")==-1){continue}if(w.version>1001){continue}var o=_cmPartnerUtils.getShuffledIndexArray(w.partners.length-1);for(var q=0;q<o.length;q++){var z=o[q];var c=w.partners[z];if(z<0||z>=w.tags.length){continue}var D=w.tags[z];var y=[];for(var m=0;m<D.length;m++){var p=D[m];if(p=="1"){if(g.tid=="1"||g.tid=="6"||(g.pc&&(g.pc.indexOf("y")==0||g.pc.indexOf("Y")==0))){var C=new Cptg(c.key,x,l);C.tid="1";_cmPartnerUtils.copyTagParms(g,C,["pi","pn","cg","pv_a1","pv_a2","pv_a3","pv_a4","pv_a5","pv_a6","pv_a7","pv_a8","pv_a9","pv_a10","pv_a11","pv_a12","pv_a13","pv_a14","pv_a15"]);y.push(C)}}else{if(p=="2"){if(g.tid=="5"){var C=new Cptg(c.key,x,l);C.tid="2";_cmPartnerUtils.copyTagParms(g,C,["pr","pm","cg","pr_a1","pr_a2","pr_a3","pr_a4","pr_a5","pr_a6","pr_a7","pr_a8","pr_a9","pr_a10","pr_a11","pr_a12","pr_a13","pr_a14","pr_a15"]);y.push(C)}}else{if(p=="3"){if(g.tid=="4"&&g.at&&g.at=="5"){var C=new Cptg(c.key,x,l);C.tid="3";_cmPartnerUtils.copyTagParms(g,C,["pr","pm","cg","qt","bp",["s_a1","pr_a1"],["s_a2","pr_a2"],["s_a3","pr_a3"],["s_a4","pr_a4"],["s_a5","pr_a5"],["s_a6","pr_a6"],["s_a7","pr_a7"],["s_a8","pr_a8"],["s_a9","pr_a9"],["s_a10","pr_a10"],["s_a11","pr_a11"],["s_a12","pr_a12"],["s_a13","pr_a13"],["s_a14","pr_a14"],["s_a15","pr_a15"]]);y.push(C)}}else{if(p=="4"){if(g.tid=="4"&&g.at&&g.at=="9"){var C=new Cptg(c.key,x,l);C.tid="4";_cmPartnerUtils.copyTagParms(g,C,["pr","pm","cg","qt","bp",["s_a1","pr_a1"],["s_a2","pr_a2"],["s_a3","pr_a3"],["s_a4","pr_a4"],["s_a5","pr_a5"],["s_a6","pr_a6"],["s_a7","pr_a7"],["s_a8","pr_a8"],["s_a9","pr_a9"],["s_a10","pr_a10"],["s_a11","pr_a11"],["s_a12","pr_a12"],["s_a13","pr_a13"],["s_a14","pr_a14"],["s_a15","pr_a15"]]);C.tr=g.tr;C.on=g.on;y.push(C)}}else{if(p=="5"){if(g.tid=="3"){var C=new Cptg(c.key,x,l);C.tid="5";_cmPartnerUtils.copyTagParms(g,C,["on",["tr","ov"],"ct","sa","zp","o_a1","o_a2","o_a3","o_a4","o_a5","o_a6","o_a7","o_a8","o_a9","o_a10","o_a11","o_a12","o_a13","o_a14","o_a15"]);y.push(C)}}else{if(p=="6"){if(g.topline[w.cid]&&g.topline[w.cid].natscheng){var C=new Cptg(c.key,x,l);C.tid="6";C.en=g.topline[w.cid].natscheng;C.se=g.topline[w.cid].natschtm;if(g.topline[w.cid].mkchnl==(new Crur()).MARKETING_PROGRAMS){C.st="PAID"}else{C.st="NATURAL"}y.push(C)}else{if(g.tid=="1"||g.tid=="6"||(g.pc&&(g.pc.indexOf("y")==0||g.pc.indexOf("Y")==0))){if(g.se&&g.se.replace(/^\s*/,"").replace(/\s*$/,"")){var C=new Cptg(c.key,x,l);C.tid="6";C.en="ONSITE";C.se=g.se;C.sr=g.sr;y.push(C)}}}}else{if(p=="7"){if(g.tid=="14"){var C=new Cptg(c.key,x,l);C.tid="7";_cmPartnerUtils.copyTagParms(g,C,[["cid","eid"],["ccid","cat"],["cat","at"],"cpt","c_a1","c_a2","c_a3","c_a4","c_a5","c_a6","c_a7","c_a8","c_a9","c_a10","c_a11","c_a12","c_a13","c_a14","c_a15"]);y.push(C)}}}}}}}}}if(c.type=="I"){for(var k=0;k<y.length;k++){var b=_cmPartnerUtils.cmGetImgSrc_Partner(y[k],c);A.push(b)}}else{if(c.type=="S"){for(var k=0;k<y.length;k++){if(c.callbackFunctionSet){try{c._cm_ConnectCallback(y[k])}catch(v){var u=new Cpse(w.cid+"",l,k);var d=_cmPartnerUtils.cmGetImgSrc_CMError(u);A.push(d)}}else{if(!_cmPartnerUtils.AT_PartnerCallQueue[c.pid]){_cmPartnerUtils.AT_PartnerCallQueue[c.pid]=[]}_cmPartnerUtils.AT_PartnerCallQueue[c.pid].push(y[k])}}}}}var a=h[w.cid];if(a){for(var n=0;n<w.segments.length;n++){var f=w.segments[n];if(a.indexOf(f.id)!=-1){var C=new Cptg("",x,l);C.tid="99";C.sid=f.id;var B=_cmPartnerUtils.getShuffledIndexArray(f.p.length-1);for(var q=0;q<B.length;q++){var z=B[q];if(f.p[z]<0||f.p[z]>=w.partners.length){continue}var c=w.partners[f.p[z]];C.ckey=c.key;if(c.type=="I"){var b=_cmPartnerUtils.cmGetImgSrc_Partner(C,c);A.push(b)}else{if(c.type=="S"){if(c.callbackFunctionSet){try{c._cm_ConnectCallback(C)}catch(v){var u=new Cpse(w.cid+"",l,z);var d=_cmPartnerUtils.cmGetImgSrc_CMError(u);A.push(d)}}else{if(!_cmPartnerUtils.AT_PartnerCallQueue[c.pid]){_cmPartnerUtils.AT_PartnerCallQueue[c.pid]=[]}_cmPartnerUtils.AT_PartnerCallQueue[c.pid].push(C)}}}}}}}}return A};_cmPartnerUtils.copyTagParms=function(e,a,d){for(var c=0;c<d.length;c++){var b=typeof(d[c]);if(b=="string"){a[d[c]]=e[d[c]]}else{if(b=="object"){a[d[c][1]]=e[d[c][0]]}}}};_cmPartnerUtils.cmGetImgSrc_Partner=function(d,c){var a=_cmPartnerUtils.cmGetQueryStringForTag_Partner(d);var b=null;if(C8(null)=="https:"){b=new _cmCQ("https:",c.surl.indexOf("://")==-1?c.surl:c.surl.substring(c.surl.indexOf("://")+3),a)}else{b=new _cmCQ("http:",c.url.indexOf("://")==-1?c.url:c.url.substring(c.url.indexOf("://")+3),a)}return b};_cmPartnerUtils.cmGetImgSrc_CMError=function(c){var a=_cmPartnerUtils.cmGetQueryStringForTag_Partner(c);var b=null;if(C8(null)=="https:"){b=new _cmCQ("https:",cm_HOST,a)}else{b=new _cmCQ("http:",cm_HOST,a)}return b};_cmPartnerUtils.cmGetQueryStringForTag_Partner=function(b){var a="";if(b.tid){a+="tid="+b.tid}for(var c in b){if(!b[c]||b[c]==""||b[c].constructor==Function||c=="tid"){continue}if(a!=""){a+="&"}a+=cD(c)+"="+cE(cD(b[c]))}return a};_cmPartnerUtils.setContactRule=function(c){var g=c.cid;_cm_CMRules[g]=c;for(var f=0;f<c.partners.length;f++){var d=c.partners[f];if(d.type=="S"){d._cm_ConnectCallback=function e(){};d.callbackFunctionSet=false;var a=d.url;if(C8(null)=="https:"){a=d.surl}a=a.indexOf("://")==-1?a:a.substring(a.indexOf("://")+3);_cmPartnerUtils.loadScript(C8(null)+"//"+a)}}_cmPartnerUtils.AT_RulesSet=true;if(_cm_NRSet){for(var b=0;b<_cmPartnerUtils.AT_TagQueue.length;b++){_cmPartnerUtils.calculateAndSendATData(_cmPartnerUtils.AT_TagQueue[b])}_cmPartnerUtils.AT_TagQueue=[]}};function _cm_registerCallback(h,l){if(!h){return}if(typeof(l)!="function"){return}for(var b in _cm_CMRules){var a=_cm_CMRules[b];if(typeof(a)!="object"||typeof(a.cid)=="undefined"){continue}for(var g=0;g<a.partners.length;g++){var d=a.partners[g];if(d.pid==h&&!d.callbackFunctionSet){d._cm_ConnectCallback=l;d.callbackFunctionSet=true;if(_cmPartnerUtils.AT_PartnerCallQueue[d.pid]){for(var c=0;c<_cmPartnerUtils.AT_PartnerCallQueue[d.pid].length;c++){try{d._cm_ConnectCallback(_cmPartnerUtils.AT_PartnerCallQueue[d.pid][c])}catch(f){}}_cmPartnerUtils.AT_PartnerCallQueue[d.pid]=[]}}}}}function cmSetNRFlag(b){if(_cm_NRSet){return}if(b){_cm_isNew=false}_cm_NRSet=true;if(_cmPartnerUtils.AT_RulesSet){for(var a=0;a<_cmPartnerUtils.AT_TagQueue.length;a++){_cmPartnerUtils.calculateAndSendATData(_cmPartnerUtils.AT_TagQueue[a])}_cmPartnerUtils.AT_TagQueue=[]}}_cmPartnerUtils.getContactCookieValues=function(){var g=1;var k=new Ctck();var d=cI("CoreAt");if(!d){return k}var f=d.split("&");var l,h,c,a;for(var e=0;e<f.length;e++){l=f[e];a=l.indexOf("=");if(a!=-1){var h=l.substring(0,a);var c=null;if(l.length>a+1){c=l.substring(a+1)}if(h&&c){var b=unescape(c).split(/\|/);if(b&&b.length>0){if(b[0]&&parseInt(b[0])<=g){if(b[1]){k.setPgCt(h,b[1])}if(b[2]){k.setOsshCt(h,b[2])}if(b[3]){k.setOrders(h,b[3])}if(b[4]){k.setSales(h,b[4])}if(b[5]){k.setItCartCt(h,b[5])}if(b[6]){k.setItPurCt(h,b[6])}if(b[7]){k.setPvCt(h,b[7])}if(b[8]){k.setEvPts(h,b[8])}if(b[9]){k.setEvComCt(h,b[9])}if(b[10]){k.setEvIniCt(h,b[10])}if(b[11]){k.setElvCt(h,b[11])}if(b[12]){k.setFpFlag(h,b[12])}if(b[13]){k.setStTime(h,b[13])}if(b[14]){k.setSegRulesMet(h,b[14])}if(b[15]){k.setSegsMet(h,b[15])}}}}}}return k};_cmPartnerUtils.setContactCookieValues=function(b){var a=1;var c="";for(var d in b.holder){if(d.length!=8||typeof(b.holder[d])=="function"){continue}c+=d+"="+a+"|"+b.getPgCt(d)+"|"+b.getOsshCt(d)+"|"+b.getOrders(d)+"|"+b.getSales(d)+"|"+b.getItCartCt(d)+"|"+b.getItPurCt(d)+"|"+b.getPvCt(d)+"|"+b.getEvPts(d)+"|"+b.getEvComCt(d)+"|"+b.getEvIniCt(d)+"|"+b.getElvCt(d)+"|"+b.getFpFlag(d)+"|"+b.getStTime(d)+"|"+b.getSegRulesMet(d)+"|"+b.getSegsMet(d)+"&"}CB("CoreAt",c,"",cm_JSFPCookieDomain)};_cmPartnerUtils.parseReferralURL=function(b){var s=new Crur();if(!b){return s}var c=this.extractDomainName(b);if(c.getPartsCount()==0){return s}if(c.url.search(/^[0-9]+(\.[0-9]+){3}$/)>=0){s.channel=s.REFERRAL_CHANNEL;s.refName=c.url;return s}var l=[["http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/GOOGLE.COM","q"],["http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/YAHOO.COM","http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/SEARCH.YAHOO.COM","p"],["http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/MSN.COM","http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/SEARCH.MSN.COM",["q","MT"]],["http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/AOL.COM","http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/SEARCH.AOL.COM",["aps_terms","query","encquery","q"]],["http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/AOL.COM",["http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/AOLSEARCH.AOL.COM","http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/AOLSEARCHT.AOL.COM"],"query"],["http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/ASK.COM",["q","ask"]],["http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/ASK.COM",["http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/ASKGEEVES.COM","http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/ASKJEEVES.COM","http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/ASKJEEVS.COM"],"ask"],["http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/BING.COM","q"],["http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/LYCOS.COM","http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/HOTBOT.LYCOS.COM","MT"],["http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/LYCOS.COM","query"],["http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/ALTAVISTA.COM","q"],["http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/ALTAVISTA.COM",["http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/PARTNERS.ALTAVISTA.COM","http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/ALTA-VISTA.COM"],"q"],["http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/NETSCAPE.COM","http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/SEARCH.NETSCAPE.COM",["search","query"]],["http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/WEBSEARCH.CNN.COM","query"],["http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/LOOKSMART.COM","key"],["http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/ABOUT.COM","terms"],["http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/MAMMA.COM","query="],["http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/ALLTHEWEB.COM",["query","q"]],["http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/VOILA.COM","kw"],["http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/VIRGILIO.IT","http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/SEARCH.VIRGILIO.IT","qs"],["http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/LIVE.COM","http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/SEARCH.LIVE.COM","q"],["http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/BAIDU.COM",["word","wd"]],["http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/SEARCH.ALICE.IT","qs"],["http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/YANDEX.RU","text"],["http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/CLUB-INTERNET.FR","q"],["http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/SEARCH.SEZNAM.CZ","q"],["http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/SEARCH.SEZNAM.CZ","w"],["http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/SEARCH.COM",["q","what","QUERY","OLDQUERY"]],["http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/SEARCH.YAM.COM","k"],["http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/GOOGLE.PCHOME.COM.TW","q"]];var g=[];for(var m=c.getPartsCount();g.length==0&&m>=2;m--){var o=c.getLast(m);for(var h=0;h<l.length;h++){var p=l[h];var n=(p.length>2)?p[1]:p[0];n=(typeof(n)=="string")?[n]:n;for(var f=0;f<n.length;f++){if(n[f]==o){g.push(p)}}}}if(g.length>0){s.channel=s.NATURAL_SEARCH_CHANNEL;s.natSearchEngine=g[0][0];s.refName=c.url;for(var m=0;m<g.length;m++){var p=g[m];var e=(p.length>2)?p[2]:p[1];var e=(typeof(e)=="string")?[e]:e;for(var h=0;h<e.length;h++){var r=new RegExp("[&?]"+e[h]+"=([^&]+)");var q=b.match(r);if(q){var d=_cmPartnerUtils.urlDecode(q[1]);if(d.search(/^[^a-zA-Z0-9]*$/)==-1){s.natSearchWord=d.replace(/\+/g," ");break}}}}}else{s.channel=s.REFERRAL_CHANNEL;s.refName=c.url}return s};_cmPartnerUtils.urlDecode=function(a){if(typeof(decodeURIComponent)=="function"){try{return decodeURIComponent(a)}catch(b){}}return unescape(a)};_cmPartnerUtils.extractDomainName=function(c){var b=c.match(/:\/*([^\/\?]+)/);var e=b?b[1]:"";e=e.toUpperCase();b=e.match(/^(?:WWW\d*\.)?([^:]+)/);if(b){e=b[1]}var d=e.length-1;var f=e.lastIndexOf(".");if(f==-1){return new Cspd()}else{if(f==d){e=e.substring(0,d)}}return new Cspd(e)};_cmPartnerUtils.parseVCPI=function(g){if(!g){return""}var d=g.match(/[&?]cm_mmc(_o)?=([^&]+)/);if(!d){return""}var i=d[1]?deObfuscate(d[2]):d[2];var h=i.split(/\-_\-|\*/);if(!h||h.length!=4){return""}var f=h[3].indexOf("|-|");if(f!=-1){h[3]=h[3].substring(0,f)}h[0]=_cmPartnerUtils.urlDecode(h[0]).replace(/\+/g," ");h[1]=_cmPartnerUtils.urlDecode(h[1]).replace(/\+/g," ");h[2]=_cmPartnerUtils.urlDecode(h[2]).replace(/\+/g," ");h[3]=_cmPartnerUtils.urlDecode(h[3]).replace(/\+/g," ");var c=g.match(/[&?]cm_guid=([^&]+)/);var e=(c&&c[1])?_cmPartnerUtils.urlDecode(c[1]):"";return[h[0]+"*"+h[1]+"*"+h[2]+"*"+h[3],h[0],h[1],h[2],h[3],e]};_cmPartnerUtils.deObfuscate=function(q){if(!q){return""}var o="-P2KHd7ZG3s14WRVhqmaJe8rQUz_gpwuTtbXLkFEB56ylfAMc0YOCjvnNSDxIo9i";var h="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-_1234567890";var a=45;var m=122;var d=[];for(var g=0;g<o.length;g++){var n=o.charCodeAt(g);d[n-45]=h.charAt(g)}var p="";try{for(var g=0;g<q.length;g++){var l=q.charAt(g);var b=q.charCodeAt(g);if(b<a||b>m){p+=l}else{var f=d[b-45];if(f==null){p+=l}else{p+=f}}}}catch(k){}return p};_cmPartnerUtils.getShuffledIndexArray=function(c){var e=[];for(var b=0;b<=c;b++){e.push(b)}for(var b=0;b<e.length;b++){var d=Math.floor(Math.random()*(e.length));var a=e[b];e[b]=e[d];e[d]=a}return e};_cmPartnerUtils.startsWith=function(a,b){return(a.toUpperCase().indexOf(b)==0)};_cmPartnerUtils.endsWith=function(a,b){return((a.toUpperCase().lastIndexOf(b)!=-1)&&(a.toUpperCase().lastIndexOf(b)+b.length==a.length))};_cmPartnerUtils.contains=function(a,b){return(a.toUpperCase().indexOf(b)!=-1)};function Ctck(){this.holder={};this.getIntValue=function(c,b){if(!this.holder[c]){return 0}var a=this.holder[c][b]?parseInt(this.holder[c][b]):0;a=(a==NaN)?0:a;return a};this.getFloatValue=function(c,b){if(!this.holder[c]){return 0}var a=this.holder[c][b]?parseFloat(this.holder[c][b]):0;a=(a==NaN)?0:a;return a};this.getStringValue=function(b,a){if(!this.holder[b]){return""}return this.holder[b][a]?this.holder[b][a]:""};this.setFloatValue=function(c,a,b){if(!this.holder[c]){this.holder[c]={}}if(a&&b&&parseFloat(b)!=NaN){if(typeof(b)=="number"){this.holder[c][a]=b.toFixed(2)+""}else{this.holder[c][a]=b}}};this.setIntValue=function(c,a,b){if(!this.holder[c]){this.holder[c]={}}if(a&&b&&parseInt(b)!=NaN){this.holder[c][a]=b+""}};this.setStringValue=function(c,a,b){if(!this.holder[c]){this.holder[c]=[]}if(a&&b){this.holder[c][a]=b}};this.getPgCt=function(a){return this.getIntValue(a,"pgct")};this.setPgCt=function(b,a){this.setIntValue(b,"pgct",a)};this.getOsshCt=function(a){return this.getIntValue(a,"osshct")};this.setOsshCt=function(b,a){this.setIntValue(b,"osshct",a)};this.getOrders=function(a){return this.getIntValue(a,"orders")};this.setOrders=function(b,a){this.setIntValue(b,"orders",a)};this.getSales=function(a){return this.getFloatValue(a,"sales")};this.setSales=function(b,a){this.setFloatValue(b,"sales",a)};this.getItCartCt=function(a){return this.getFloatValue(a,"itcartct")};this.setItCartCt=function(b,a){this.setFloatValue(b,"itcartct",a)};this.getItPurCt=function(a){return this.getFloatValue(a,"itpurct")};this.setItPurCt=function(b,a){this.setFloatValue(b,"itpurct",a)};this.getPvCt=function(a){return this.getIntValue(a,"pvct")};this.setPvCt=function(b,a){this.setIntValue(b,"pvct",a)};this.getEvPts=function(a){return this.getFloatValue(a,"evpts")};this.setEvPts=function(b,a){this.setFloatValue(b,"evpts",a)};this.getEvIniCt=function(a){return this.getIntValue(a,"evinict")};this.setEvIniCt=function(b,a){this.setIntValue(b,"evinict",a)};this.getEvComCt=function(a){return this.getIntValue(a,"evcomct")};this.setEvComCt=function(b,a){this.setIntValue(b,"evcomct",a)};this.getElvCt=function(a){return this.getIntValue(a,"elvct")};this.setElvCt=function(b,a){this.setIntValue(b,"elvct",a)};this.getFpFlag=function(a){return this.getIntValue(a,"fp")};this.setFpFlag=function(b,a){this.setIntValue(b,"fp",a)};this.getStTime=function(a){return this.getIntValue(a,"st")};this.setStTime=function(b,a){this.setIntValue(b,"st",a)};this.getSegRulesMet=function(a){return this.getStringValue(a,"segrules")};this.setSegRulesMet=function(b,a){this.setStringValue(b,"segrules",a)};this.getSegsMet=function(a){return this.getStringValue(a,"segs")};this.setSegsMet=function(b,a){this.setStringValue(b,"segs",a)}}function Cpse(c,a,b){this.ci=c;this.tid="21";this.ul=(a)?a:"";this.pindex=b}function Cptg(c,b,a){this.ckey=(c)?c:"";this.rf=(b)?b:"";this.ul=(a)?a:""}function Crur(){this.DIRECT_LOAD_CHANNEL="DIRECT LOAD";this.REFERRAL_CHANNEL="REFERRING SITES";this.NATURAL_SEARCH_CHANNEL="NATURAL SEARCH";this.MARKETING_PROGRAMS="MARKETING PROGRAMS";this.DIRECT_LOAD_REFERRAL_NAME="DL";this.channel=this.DIRECT_LOAD_CHANNEL;this.refName=this.DIRECT_LOAD_REFERRAL_NAME;this.natSearchEngine="";this.natSearchWord=""}function Cspd(a){this.url=(a)?a:"";this.splitUrl=this.url.split(".");this.getPartsCount=function(){return this.splitUrl.length};this.getLast=function(c){var b="";for(var d=c;d>=1;d--){if(this.splitUrl.length>=d){if(b){b+="."}b+=this.splitUrl[this.splitUrl.length-d]}}return b}}function cmCheckIEReady(){if(document.readyState=="complete"){cmOnDomReady()}}function cmOnDomReady(){if(!CM_DDX.domReadyFired){CM_DDX.domReadyFired=true;CM_DDX.invokeFunctionWhenAvailable(function(){__$dispatcher.domReady()})}}var coremetrics=(function(){var b="undefined",a="function";return{cmLoad:cmLoad,cmLastReferencedPageID:null,isDef:function(c){return typeof(c)!==b&&c},cmUpdateConfig:function(c){var e=coremetrics.isDef;if(e(c.io)){cm_IOEnabled=c.io}if(e(c.ia)){cm_OffsiteImpressionsEnabled=c.ia}if(e(c.at)){cm_ATEnabled=c.at}if(e(c.mc)){cm_MCEnabled=c.mc}if(e(c.ddx)&&e(c.ddx.version)){CM_DDX.version=c.ddx.version;if(e(c.ddx.standalone)){CM_DDX.standalone=c.ddx.standalone}}},getTechProps:function(f){var e,g=f||{},h=navigator,d=window.screen,c;g.jv=(typeof(Array.isArray)===a)?"1.8.5":(typeof([].reduce)===a)?"1.8":(typeof(Iterator)===a)?"1.7":(typeof(Array.forEach)===a)?"1.6":(typeof(decodeURI)===a)?"1.5":(typeof(NaN)==="number")?"1.3":(typeof(isFinite)===a)?"1.2":(typeof(isNaN)===a)?"1.1":"1.0";if(h.plugins){c=h.plugins;for(e=0;e<c.length;e++){g["np"+e]=c[e].name}}if(typeof(h.javaEnabled)===a){g.je=h.javaEnabled()?"y":"n"}g.sw=d.width;g.sh=d.height;g.pd=d.colorDepth;if(g.pd==0){g.pd=d.pixelDepth}g.tz=new Date().getTimezoneOffset()/60;return g}}})();var cm_exAttr=new Array();var cmCheckCMEMFlag=true;var cmAutoCopyAttributesToExtraFields=false;var cmPricePattern=/[^\-0-9\.]/gi;var cmSpacePattern=/^\s+|\s+$/gi;var cmMMCPattern=/cm_(?:mmc|ven|cat|pla|ite)/gi;function cmLoadIOConfig(){if(typeof(IORequest)=="function"){IORequest.client_id=cm_ClientID.split(";")[0].split("|")[0];IORequest.encrypt_cats=true;IORequest.encrypt_prds=true;IORequest.conflict_resolution=true;IORequest.max_prd_length=25;IORequest.max_cat_length=25;IORequest.timeout=[8000,4000];IORequest.use_site_category=false;if((IORequest.ie_version()!==null)&&(IORequest.ie_version()<7)){IORequest.a_max_elements=[3,3,5,3,3,3,3]}else{IORequest.a_max_elements=[3,3,5,3,3,7,7]}IORequest.required_attributes=[0,0,0,0,0];IORequest.access_method="json remote";IORequest.default_product_file=undefined}}function cmSetClientID(f,g,h,b,e){cm_PartnerDataClientIDs=cm_ClientID=f;if(typeof(IORequest)=="function"){IORequest.client_id=cm_ClientID.split(";")[0].split("|")[0]}cm_McClientID=cm_ClientID.split(";")[0].split("|")[0];if(g===true){cm_JSFEnabled=true}if(h){cm_HOST=cm_Production_HOST=h;if((h==="http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/test.coremetrics.com")||(h==="http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/testdata.coremetrics.com")){cm_Production_HOST="http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/data.coremetrics.com"}cm_HOST+="/cm?"}if(b){cm_JSFPCookieDomain=b}CM_DDX.cVA=cm_ClientID.split(";")[0].split("|")[0];if(!document.body&&CM_DDX.headScripts){document.write("<script src='",C8(null)+"//tmscdn.coremetrics.com/tms/"+CM_DDX.cVA+"/head"+(CM_DDX.test.doTest?"s-"+CM_DDX.test.testCounter:"")+".js?__t="+(CM_DDX.test.doTest?"0":(+new Date()))+"'><\/script>")}var d=cm_ClientID.split(";");var c;for(var a=0;a<d.length;++a){c=d[a].split("|")[0];_cmPartnerUtils.loadScript(C8(null)+"//libs.coremetrics.com/configs/"+c+".js")}cmRetrieveUserID(cmSetNRFlag)}function cmSetupCookieMigration(m,n,o,p,d,e,k){if(m){cm_JSFPCookieMigrate=m}if(n){cm_JSFPForceMigrateCookies=n}if(o){cm_JSFPMigrationDomainWhitelist=o}if(p){cm_JSFPMigrationDomainBlacklist=p}if(d){cm_JSFPMigrationPathWhitelist=d}if(e){cm_JSFPMigrationOtherCookies=e}if(k){cm_JSFPMigrationOtherCookiesExpireTimes=k}if(cm_JSFPCookieMigrate){var l=cm_ClientID.split(";"),b={},h,g,a,f,c={};for(h=0;h<l.length;++h){a=cmExtractParameter(cmJSFCreateSessionMigrationParamName(l[h]),window.location.href);if(a){b[l[h]]=a}}if(cm_JSFPMigrationOtherCookies){f=cm_JSFPMigrationOtherCookies.split(",");for(g=0;g<f.length;++g){a=cmExtractParameter("cm_mc_"+f[g],window.location.href);if(a){c[f[g]]=a}}}cmJSFPMigrateCookies(cmExtractParameter(cm_JSFPCookieMigrateVisitorID,window.location.href),b,c)}}var cmNormalizeBlackList,cmNormalizeWhiteList=null;function cmSetupNormalization(b,c,a){if(b){cmNormalizeBlackList=b}if(c){cmNormalizeWhiteList=c}if(a){if(document.cmTagCtl!=null){document.cmTagCtl.normalizeURL=a}}}function cmSetupOther(b){for(var a in b){window[a]=b[a]}}function cmSetCurrencyCode(a){cm_currencyCode=a}function cmSetFirstPartyIDs(b,e){cm_JSFPCookieMigrate=true;cm_JSFPForceMigrateCookies=true;var c=cm_ClientID.split(";");var d={};for(var a=0;a<c.length;++a){d[c[a]]=e}cmJSFPMigrateCookies(b,d,null)}function cmCreateManualImpressionTag(a,e,b,d,c){if(!a){a=c1(cm_ClientID)}cmMakeTag(["tid","9","pi",a,"cm_sp",e,"cm_re",b,"cm_cr",d,"cm_me",c,"st",cm_ClientTS])}function cmCreateManualLinkClickTag(b,c,a){if(cM!=null){var d=new Date();cGK=d.getTime();b=cG7.normalizeURL(b,true);cM(cm_ClientTS,cGK,c,b,false,a)}}function cmCreateManualPageviewTag(b,h,g,f,c,d,e,a){cmMakeTag(["tid","1","pi",b,"cg",h,"ul",g,"rf",f,"se",d,"sr",e,"cmAttributes",c,"cmExtraFields",a])}function cmCreateElementTag(b,a,c){cmMakeTag(["tid","15","eid",b,"ecat",a,"cmAttributes",c])}function cmCreatePageElementTag(c,b,a,f,e,d){cmCreateElementTag(c,b,d)}var cmCreateProductElementTag=cmCreatePageElementTag;function cmCreateConversionEventTag(d,c,f,e,b,a){cmMakeTag(["tid","14","cid",d,"cat",c,"ccid",f,"cpt",e,"cmAttributes",b,"cmExtraFields",a])}function cmCreateTechPropsTag(b,d,c,a){cmMakeTag(["tid","6","pi",b,"cg",d,"pc","Y","cmAttributes",c,"cmExtraFields",a])}function cmCreatePageviewTag(b,f,d,e,c,a){cmMakeTag(["tid","1","pi",b,"cg",f,"se",d,"sr",e,"cmAttributes",c,"cmExtraFields",a])}function cmCreateDefaultPageviewTag(a){cmCreatePageviewTag(cmGetDefaultPageID(),a)}function cmCreateProductviewTag(d,f,e,c,b){var a=c1(cm_ClientID);cmMakeTag(["tid","5","pi",a?a:"Product: "+f+" ("+d+")","pr",d,"pm",f,"cg",e,"pc","N","cm_vc",b?b:cmExtractParameter("cm_vc",document.location.href),"cmAttributes",c])}var __sArray=[];var __sRefArray=[];var __sSkuArray=[];var __sRefSkuArray=[];var __skuString="";function cmAddShop(d){var e=d.concat();var a=__sRefArray[d[1]+"|"+d[9]+"|"+d[11]+"|"+d[13]];if(typeof(a)!=="undefined"){var i=__sArray[a];if(i){var f=i[5];var h=i[7];var b=d[5];d[5]=parseInt(f)+parseInt(d[5]);d[7]=(((d[7]*b)+(h*f))/d[5]);__sArray[a]=d}}else{__sRefArray[d[1]+"|"+d[9]+"|"+d[11]+"|"+d[13]]=__sArray.length;__sArray[__sArray.length]=d}var g=__sRefSkuArray[e[1]];if(typeof(g)!=="undefined"){var c=__sSkuArray[g];if(c){var f=c[5];var h=c[7];var b=e[5];e[5]=parseInt(f)+parseInt(e[5]);e[7]=(((e[7]*b)+(h*f))/e[5]);__sSkuArray[g]=e}}else{__sRefSkuArray[e[1]]=__sSkuArray.length;__sSkuArray[__sSkuArray.length]=e}}function cmDisplayShops(){var a;for(a=0;a<__sArray.length;++a){cmMakeTag(__sArray[a])}__sArray=[];__sRefArray=[];__skuString=cmCalcSKUString()}var cmDisplayShop5s=cmDisplayShop9s=cmDisplayShops;function cmCalcSKUString(){var c="";for(var b=0;b<__sSkuArray.length;b++){var a=__sSkuArray[b];c+="|"+a[1]+"|"+a[7]+"|"+a[5]+"|"}__sSkuArray=[];__sRefSkuArray=[];return c}function cmCreateShopAction5Tag(d,f,c,g,e,b,a){if((typeof(cm_currencyCode)=="undefined")||(!cm_currencyCode)){cm_currencyCode=""}g=g.toString().replace(cmPricePattern,"");d=d.toString().replace(cmSpacePattern,"");var h=""+(b?b+"|||":"")+(a?"extra"+a:"");cmAddShop(["pr",d,"pm",f,"qt",c,"bp",g,"cg",e,"cmAttributes",b,"cmExtraFields",a,"ha1",cm_hex_sha1(h),"cc",cm_currencyCode,"at","5","tid","4","pc","N"])}function cmCreateShopAction9Tag(h,k,a,i,c,f,b,g,d,l){if((typeof(cm_currencyCode)=="undefined")||(!cm_currencyCode)){cm_currencyCode=""}i=i.toString().replace(cmPricePattern,"");b=b.toString().replace(cmPricePattern,"");h=h.toString().replace(cmSpacePattern,"");var e=""+(d?d+"|||":"")+(l?"extra"+l:"");cmAddShop(["pr",h,"pm",k,"qt",a,"bp",i,"cg",g,"cmAttributes",d,"cmExtraFields",l,"ha1",cm_hex_sha1(e),"cd",c,"on",f,"tr",b,"cc",cm_currencyCode,"at","9","tid","4","pc","N"])}function cmCreateOrderTag(f,a,e,b,d,h,i,c,g){if((typeof(cm_currencyCode)=="undefined")||(!cm_currencyCode)){cm_currencyCode=""}if(e){e=e.toString().replace(cmPricePattern,"")}a=a.toString().replace(cmPricePattern,"");cmMakeTag(["tid","3","on",f,"tr",a,"sg",e,"cd",b,"ct",d,"sa",h,"zp",i,"cc",cm_currencyCode,"cmAttributes",c,"cmExtraFields",g])}function cmCreateRegistrationTag(e,f,g,a,d,c,b){cmMakeTag(["tid","2","cd",e,"em",f,"ct",g,"sa",a,"zp",d,"cy",c,"cmAttributes",b])}function cmCreateErrorTag(a,b){cmMakeTag(["tid","404","pi",a,"cg",b,"pc","Y"])}function cmCreateCustomTag(a,b){cmMakeTag(["tid","7","li",a,"cmExtraFields",b])}function cmMakeTag(f){var l=new _cm("vn2","e4.0"),g,c={1:"pv_a",2:"rg",3:"o_a",4:"s_a",5:"pr_a",6:"pv_a",14:"c_a",15:"e_a"},b={1:"pv",2:"rg",3:"or",4:"sx",5:"pr",6:"pv",7:"ps",14:"cx"},d="cmAttributes",k="cmExtraFields";for(g=0;g<f.length;g+=2){l[f[g]]=f[g+1]}l.rnd=(Math.floor(Math.random()*11111111))+new Date().getTime();if(l.tid=="1"&&(cmCookiesDisabled()?cmAutoAddTP():(cI("cmTPSet")!="Y"))){l.tid="6";l.pc="Y"}if(l.tid=="6"){l.addTP();CB("cmTPSet","Y")}if(l.cm_exAttr){l[d]=l.cm_exAttr.join("-_-");l.cm_exAttr=null}function a(n,r,p){if(n[r]){var s=n[r].split("-_-"),o=p[n.tid],q;for(q=0;q<s.length;++q){n[o+(q+1)]=s[q]}n[r]=null}}a(l,d,c);a(l,k,b);if(cmAutoCopyAttributesToExtraFields){if((l.tid!="2")&&(l.tid!="15")){for(g=1;g<=15;++g){if(!(l[b[l.tid]+""+g])){l[b[l.tid]+""+g]=l[c[l.tid]+""+g]}}}}if((l.pi==null)&&((l.pc=="Y")||(l.tid=="1"))){l.pi=cmGetDefaultPageID()}if((l.pc=="Y")||(l.tid=="1")){coremetrics.cmLastReferencedPageID=l.pi}else{if(coremetrics.cmLastReferencedPageID==null){coremetrics.cmLastReferencedPageID="NO_PAGEID"}}try{if(parent.cm_ref!=null){l.rf=parent.cm_ref;if(l.pc=="Y"){parent.cm_ref=document.URL}}if(parent.cm_set_mmc){l.ul=document.location.href+((document.location.href.indexOf("?")<0)?"?":"&")+parent.cm_mmc_params;if(l.pc=="Y"){parent.cm_ref=l.ul;parent.cm_set_mmc=false}}}catch(e){}if(l.ul==null){l.ul=cG7.normalizeURL(window.location.href,false)}if(l.rf==null){l.rf=cG7.normalizeURL(document.referrer,false)}function m(i){return i.replace(cmMMCPattern,function(n){return n.toLowerCase()})}l.ul=m(l.ul);l.rf=m(l.rf);if((this.manual_cm_mmc)&&(l.ul.indexOf("cm_mmc")==-1)&&(l.ul.indexOf("cm_ven")==-1)){l.ul=l.ul+((l.ul.indexOf("&")==-1)?((l.ul.indexOf("?")==-1)?"?":"&"):"&")+"cm_mmc="+this.manual_cm_mmc}var h;if(cmCheckCMEMFlag){h=cmStartTagSet()}l.writeImg();if(cmCheckCMEMFlag){cmCheckCMEMFlag=false;cmCheckCMEM();if(h){cmSendTagSet()}}if(typeof cm_ted_io=="function"){if(cm_IOEnabled){cm_ted_io(l)}}}function cmGetDefaultPageID(){var b=window.location.pathname;var e=b.indexOf("?");if(e!=-1){b=b.substr(0,e)}var d=b.indexOf("#");if(d!=-1){b=b.substr(0,d)}var a=b.indexOf(";");if(a!=-1){b=b.substr(0,a)}var c=b.lastIndexOf("/");if(c==b.length-1){b=b+"default"}while(b.indexOf("/")==0){b=b.substr(1,b.length)}return(b)}function cmIndexOfParameter(b,a){return a.indexOf(b)}function cmExtractParameter(h,e){var c=null,d,b,g="&"+h+"=",a,f;d=e.indexOf("?");if(d>=0){b=e.indexOf("#");if(b<0){b=e.length}e="&"+e.substring(d+1,b);a=e.indexOf(g);if(a>=0){f=e.indexOf("&",a+1);if(f<0){f=e.length}c=e.substring(a+g.length,f)}}return c}function cmRemoveParameter(f,d){if(cmIndexOfParameter(f,d)==-1){return d}var c=d;var b=c.indexOf(f);var e=(b-1);var a=c.indexOf("&",b);if(a==-1){a=c.length}if(c.substring(e,b)=="?"){e=(e+1);a=(a+1)}return c.substring(0,e)+c.substring(a,c.length)}function cmGetMetaTag(c){var a=document.getElementsBytagName("meta");for(var b in a){if(a[b].name==c){return a[b].content}}return null}function cmCheckCMEM(){var b,a,c,d=["cm_em","cm_lm","cm_lm_o"];for(b=0;b<d.length;b++){c=cmExtractParameter(d[b],document.location.href);if(c){if(b==2){cmMakeTag(["tid","2","cd_o",c,"em_o",c])}else{a=c.indexOf(":");if(a>-1){c=c.substring(a+1)}cmCreateRegistrationTag(c,c)}}}}if(defaultNormalize==null){var defaultNormalize=null}function myNormalizeURL(a,h){var g=a;if(!g){g=""}var m=cmNormalizeBlackList;var l=cmNormalizeWhiteList;if(m){if(h){m=m.split("-_-")[0].split(",")}else{if(m.split("-_-")[1]){m=m.split("-_-")[1].split(",")}else{m=null}}}if(l){if(h){l=l.split("-_-")[0].split(",")}else{if(l.split("-_-")[1]){l=l.split("-_-")[1].split(",")}else{l=null}}}var b,c,n=g.indexOf("?"),k=[],f;if((n>0)&&(m||l)){b=g.substring(n+1);g=g.substring(0,n);c=b.split("&");if(m){for(var e=0;e<c.length;e++){f=true;for(var d=0;d<m.length;d++){if(c[e].toLowerCase().indexOf(m[d].toLowerCase()+"=")==0){f=false}}if(f){k[k.length]=c[e]}}}if(l){for(var e=0;e<c.length;e++){f=false;for(var d=0;d<l.length;d++){if(c[e].toLowerCase().indexOf(l[d].toLowerCase()+"=")==0){f=true}}if(f){k[k.length]=c[e]}}}g+="?"+k.join("&")}if(defaultNormalize!=null){g=defaultNormalize(g,h)}return g}if(document.cmTagCtl!=null){if((""+document.cmTagCtl.normalizeURL).indexOf("myNormalizeURL")==-1){defaultNormalize=document.cmTagCtl.normalizeURL;document.cmTagCtl.normalizeURL=myNormalizeURL}}var cm_hex_sha1=(function(){function b(l){return i(h(k(l),l.length*8))}function e(n){var p=0?"0123456789ABCDEF":"0123456789abcdef";var m="";var l;for(var o=0;o<n.length;o++){l=n.charCodeAt(o);m+=p.charAt((l>>>4)&15)+p.charAt(l&15)}return m}function d(n){var m="";var o=-1;var l,p;while(++o<n.length){l=n.charCodeAt(o);p=o+1<n.length?n.charCodeAt(o+1):0;if(55296<=l&&l<=56319&&56320<=p&&p<=57343){l=65536+((l&1023)<<10)+(p&1023);o++}if(l<=127){m+=String.fromCharCode(l)}else{if(l<=2047){m+=String.fromCharCode(192|((l>>>6)&31),128|(l&63))}else{if(l<=65535){m+=String.fromCharCode(224|((l>>>12)&15),128|((l>>>6)&63),128|(l&63))}else{if(l<=2097151){m+=String.fromCharCode(240|((l>>>18)&7),128|((l>>>12)&63),128|((l>>>6)&63),128|(l&63))}}}}}return m}function k(m){var l=new Array(m.length>>2);for(var n=0;n<l.length;n++){l[n]=0}for(var n=0;n<m.length*8;n+=8){l[n>>5]|=(m.charCodeAt(n/8)&255)<<(24-n%32)}return l}function i(m){var l="";for(var n=0;n<m.length*32;n+=8){l+=String.fromCharCode((m[n>>5]>>>(24-n%32))&255)}return l}function h(B,s){B[s>>5]|=128<<(24-s%32);B[((s+64>>9)<<4)+15]=s;var C=new Array(80);var A=1732584193;var z=-271733879;var y=-1732584194;var v=271733878;var u=-1009589776;for(var p=0;p<B.length;p+=16){var r=A;var q=z;var o=y;var n=v;var l=u;for(var m=0;m<80;m++){if(m<16){C[m]=B[p+m]}else{C[m]=g(C[m-3]^C[m-8]^C[m-14]^C[m-16],1)}var D=f(f(g(A,5),a(m,z,y,v)),f(f(u,C[m]),c(m)));u=v;v=y;y=g(z,30);z=A;A=D}A=f(A,r);z=f(z,q);y=f(y,o);v=f(v,n);u=f(u,l)}return[A,z,y,v,u]}function a(m,l,o,n){if(m<20){return(l&o)|((~l)&n)}if(m<40){return l^o^n}if(m<60){return(l&o)|(l&n)|(o&n)}return l^o^n}function c(l){return(l<20)?1518500249:(l<40)?1859775393:(l<60)?-1894007588:-899497514}function f(l,o){var n=(l&65535)+(o&65535);var m=(l>>16)+(o>>16)+(n>>16);return(m<<16)|(n&65535)}function g(l,m){return(l<<m)|(l>>>(32-m))}return function(l){if(l){return e(b(d(l)))}else{return null}}})();var _io_request=new IORequest();var _io_config=undefined;var _io_zone=undefined;var _io_state=new IOState();function cm_ted_io(a){IORequest.log(IORequest.log_trace,"Processing tag: tid="+a.tid+", pr="+a.pr+", cg="+a.cg+", at="+a.at+", pi="+a.pi);_io_state.cm_ted_io(a)}function _cm_io_rec(a){if(_io_request!==undefined){_io_request.cm_io_rec(a)}}function _cm_io_cfg(a){if(_io_request!==undefined){_io_request.cm_io_cfg(a,1)}}function _cm_io_ssp(a){if(_io_request!==undefined){_io_request.cm_io_ssp(a)}}function cmRecRequest(a,b,d,c,e){if(a===undefined){IORequest.log(IORequest.log_error,"cmRecRequest: Required zone id undefined.")}IORequest.rec_request(a,b,d,c,e)}function cmPageRecRequest(a,b,d,c){if(a===undefined){IORequest.log(IORequest.log_error,"cmPageRecRequest: Required zone id undefined.")}IORequest.page_rec_request(a,b,d,c)}function cmElementRecRequest(a,b,d,c){if(a===undefined){IORequest.log(IORequest.log_error,"cmElementRecRequest: Required zone id undefined.")}IORequest.element_rec_request(a,b,d,c)}function cmDisplayRecs(){IORequest.display_recs()}function cmGetTestGroup(a){return IORequest.ab_group_number}function cmSetRegId(b,a){if((b===undefined)||(b=="")){IORequest.log(IORequest.log_error,"cmSetRegId: Required registration id is blank or undefined.")}else{IORequest.setRegIdCalled=true;IORequest.ssp_reg_id=IORequest.encrypt16(b.toString());IORequest.log(IORequest.log_trace,"cmSetRegId",b+(a?","+a:"")+" - encryption of "+b+": "+IORequest.ssp_reg_id);IORequest.ssp_allow_flag=a}}function cmSetSegment(a){IORequest.setSegmentCalled=true;if((a===undefined)||(a=="")){IORequest.log(IORequest.log_trace,"cmSetSegment: Segment is blank or undefined, segment will be removed from cookie");IORequest.pf_segment=""}else{IORequest.pf_segment=IORequest.encrypt16(a.toString());IORequest.log(IORequest.log_trace,"cmSetSegment",a+" - encryption of "+a+": "+IORequest.pf_segment)}}function IORequest(p_default_json){var g_config_filename="Unknown_83_filename"/*tpa=http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/io_config.js*/;var g_version="V4";this.h_timer=undefined;this.h_script=undefined;this.xmlHttp=undefined;this.i_timeout=0;this.request_type="";this.action_callback=function(action){return};this.display_status=function(txt,color){return};this.cm_alert=function(p_text){if(!IORequest.production){alert(p_text)}};IOStopWatch=function(){this.start=function(){this.elapsed_time=0;this.t_start=new Date().getTime()};this.stop=function(){this.elapsed_time=new Date().getTime()-this.t_start;return(this.elapsed_time)}};this.stop_watch=new IOStopWatch("stop_watch");this.ajax_timeout=function(req_type){if(_io_request.xmlHttp!==undefined){try{if(_io_request.xmlHttp.abort!==undefined){if(typeof _io_request.xmlHttp.abort=="function"){_io_request.xmlHttp.abort()}}}catch(e){_io_request.display_status("IE - no abort property of the xmlHttp request object")}}IORequest.b_timeout=true;if(req_type==1){_io_request.action_callback("config_timeout");IORequest.i_zone=0;setTimeout('IORequest.config_download_failure("ajax timeout");',0)}else{if(req_type==2){_io_request.action_callback("ssp_timeout");IORequest.log(IORequest.log_warn,"Ajax timeout downloading ssp",_io_request.stop_watch.elapsed_time+" ms");IORequest.i_zone=0;setTimeout('IORequest.ssp_download_failure("ajax timeout");',0)}else{_io_request.display_status("Ajax timeout downloading product ("+_io_request.stop_watch.elapsed_time+"ms)","red");IORequest.log(IORequest.log_warn,"Ajax timeout downloading product",_io_request.stop_watch.elapsed_time+" ms");_io_request.download_product()}}};function getXmlHttpObject(){if(window.XMLHttpRequest){return new window.XMLHttpRequest}else{try{return new ActiveXObject("MSXML2.XMLHTTP.3.0")}catch(ex){return null}}}this.javascript_timeout=function(req_type){if(IORequest.h_script!==undefined){var h=document.getElementsByTagName("head").item(0);if(h){h.removeChild(IORequest.h_script);IORequest.h_script=undefined}}_io_request.stop_watch.stop();if(IORequest.request_crc!==undefined){IORequest.timeout_product[IORequest.offer_id+IORequest.request_crc]=1}if(req_type==1){_io_request.action_callback("config_timeout");IORequest.i_zone=0;setTimeout('IORequest.config_download_failure("javascript timeout");',0)}else{if(req_type==2){_io_request.action_callback("ssp_timeout");IORequest.log(IORequest.log_warn,"JavaScript timeout downloading ssp",_io_request.stop_watch.elapsed_time+" ms");IORequest.i_zone=0;setTimeout('IORequest.ssp_download_failure("javascript timeout");',0)}else{_io_request.display_status("JavaScript timeout downloading product ("+_io_request.stop_watch.elapsed_time+"ms)","blue");IORequest.log(IORequest.log_warn,"JavaScript timeout downloading product",_io_request.stop_watch.elapsed_time+" ms");if(IORequest.request_crc!==undefined){if((_io_config.file_not_found_pc!==undefined)&&(_io_config.file_not_found_pc>Math.floor(Math.random()*100))){var id=IORequest.offer_type+IORequest.offer_id+"|"+IORequest.request_crc+"|"+(IORequest.isCategoryOffer(IORequest.offer_type)?IORequest.plain_text_cat_id:(IORequest.isSearchOffer(IORequest.offer_type)?IORequest.plain_text_scrubbed_search_id:IORequest.plain_text_item_id));cmCreatePageElementTag(id,_io_config.file_not_found_id);IORequest.log(IORequest.log_trace,"page element tag for file not found",id)}}_io_request.download_product()}}};this.stateChanged=function(){if(_io_request.xmlHttp.readyState==4){clearTimeout(_io_request.h_timer);_io_request.h_timer=undefined;if(_io_request.xmlHttp.status==200){var txt=_io_request.xmlHttp.responseText;eval(txt)}else{if(_io_request.xmlHttp.status==404){_io_request.display_status("Ajax - Requested File not found on server - "+_io_request.xmlHttp.status+". Next step in recommendation plan attempted","blue");IORequest.log(IORequest.log_warn,"Ajax - Requested File not found on server - "+_io_request.xmlHttp.status,"next step in recommendation plan attempted");IORequest.b_404=true;if(_io_request.request_type=="config"){setTimeout('IORequest.config_download_failure("ajax 404");',0)}else{if(_io_request.request_type=="ssp"){IORequest.ssp_processed("Ajax 404 downloading ssp")}else{if(_io_request.request_type=="product"){_io_request.download_product()}}}}else{_io_request.display_status("Ajax - Unexpected status from stateChanged: "+_io_request.xmlHttp.status+".","red");IORequest.log(IORequest.log_error,"Ajax - Unexpected status from stateChanged",_io_request.xmlHttp.status);IORequest.b_404=true;if(_io_request.request_type=="config"){setTimeout('IORequest.config_download_failure("ajax 404");',0)}else{if(_io_request.request_type=="ssp"){IORequest.ssp_processed("Ajax 404 downloading ssp")}else{if(_io_request.request_type=="product"){_io_request.download_product()}}}}}}else{}};this.get_target_from_plan=function(p_rec_plan,p_b_category){if(IORequest.current_step>=p_rec_plan.rec_steps.length){return("_SX_")}var rec_step=p_rec_plan.rec_steps[IORequest.current_step];IORequest.log(IORequest.log_trace,"step: "+IORequest.current_step+" offer_id: "+rec_step.offer_id+" type: "+rec_step.offer_type+" target: "+rec_step.target_id+" algo_id: "+rec_step.algo_id+" algo_value",rec_step.algo_value);if(rec_step.target_id=="_NR_"){return("_NR_")}if(rec_step.target_id=="_DPF_"){return("_DPF_")}if(p_b_category&&!IORequest.isCategoryOffer(rec_step.offer_type)){IORequest.current_step++;this.display_status("Looking for Category - found Product: "+rec_step.target_id+".  Continuing to next step.","green");IORequest.log(IORequest.log_trace,"Looking for Category - found Product: "+rec_step.target_id+".  Continuing to next step.");return(this.get_target_from_plan(p_rec_plan,1))}if(rec_step.target_id=="_SP_"||rec_step.target_id=="_SG_"||rec_step.target_id=="_SE_"){if(IORequest.item_id==""){IORequest.current_step++;this.display_status("No item id specified. Continuing to next step.","blue");IORequest.log(IORequest.log_warn,"No item id specified.  Continuing to next step.");return(this.get_target_from_plan(p_rec_plan))}else{if(IORequest.isMultiTargetStep(rec_step)){return(IORequest.item_id)}else{return(IORequest.single_item_id)}}}if(rec_step.target_id=="_SC_"||rec_step.target_id=="_SGC_"||rec_step.target_id=="_SEC_"){if(IORequest.category_id==""){IORequest.current_step++;this.display_status("No category id specified. Continuing to next step.","blue");IORequest.log(IORequest.log_warn,"No category id specified.  Continuing to next step.");return(this.get_target_from_plan(p_rec_plan))}else{if(IORequest.isMultiTargetStep(rec_step)){return(IORequest.category_id)}else{return(IORequest.single_category_id)}}}if(rec_step.target_id=="_SS_"){if(IOConfig.crc_specified_search==""){IORequest.current_step++;this.display_status("No search term specified. Continuing to next step.","blue");IORequest.log(IORequest.log_warn,"No search term specified.  Continuing to next step.");return(this.get_target_from_plan(p_rec_plan))}else{return(IOConfig.crc_specified_search)}}if(rec_step.target_id=="_RVP_"||rec_step.target_id=="_RVL_"||rec_step.target_id=="_RVG_"||rec_step.target_id=="_RVLG_"||rec_step.target_id=="_LCP_"||rec_step.target_id=="_RPP_"||rec_step.target_id=="_RVC_"||rec_step.target_id=="_MPC_"||rec_step.target_id=="_MSP_"){var rc=_io_state.cm_get_item_from_cookie(rec_step.target_id,IORequest.isMultiTargetStep(rec_step));if(rc===0){IORequest.current_step++;this.display_status("No "+rec_step.target_id+" available. Continuing to next step.","green");IORequest.log(IORequest.log_trace,"No "+rec_step.target_id+" available.  Continuing to next step.");return(this.get_target_from_plan(p_rec_plan))}else{return(rc)}}this.display_status("unrecognized target id: "+rec_step.target_id+".","red");IORequest.log(IORequest.log_error,"unrecognized target id",rec_step.target_id);return("_NR_")};this.issue_page_element_tag=function(ab_test_array){if(IORequest.perm_cookie_not_supported===false){var session_cookie=IORequest.find_cookie(IORequest.ses_cookie);if(session_cookie===undefined){var random_number=new Date().getTime().toString();session_cookie=IORequest.set_and_check_cookie(IORequest.ses_cookie,"S"+random_number+"|",true);if(!session_cookie){return}}if(session_cookie.indexOf("|"+ab_test_array[0]+"|")==-1){IORequest.log(IORequest.log_trace,"issued page element tag "+ab_test_array[1],ab_test_array[0]);IORequest.log(IORequest.log_trace,"session cookie",session_cookie);IORequest.set_and_check_cookie(IORequest.ses_cookie,session_cookie+ab_test_array[0]+"|",true);cmCreatePageElementTag(ab_test_array[1],ab_test_array[0])}}};this.get_client_id=function(){var r_client_id;if(IORequest.client_id_override!==undefined){r_client_id=IORequest.client_id_override}else{if(IORequest.client_id!==undefined){r_client_id=IORequest.client_id}else{if(cm_ClientID!==undefined){r_client_id=cm_ClientID.split(";")[0].split("|")[0]}}if(IORequest.find_cookie(IORequest.test_cookie)===undefined){if(r_client_id.substr(0,1)=="6"){IORequest.log(IORequest.log_trace,"Retrieving data from client 9"+r_client_id.substr(1,r_client_id.length-1)+" instead of test client "+r_client_id);r_client_id="9"+r_client_id.substr(1,r_client_id.length-1)}}}return r_client_id};this.download_product=function(){IORequest.current_step++;this.io_zone=_io_config.zones[IORequest.zone_id];var zone_test_id="''";if(this.io_zone.ab_test_id!="no ab test"){this.issue_page_element_tag(this.io_zone.ab_test_id.split(":"));zone_test_id="'"+this.io_zone.ab_test_id+"'"}IORequest.log(IORequest.log_trace,"ab test id",this.io_zone.ab_test_id);if(!this.io_zone.rec_plan){this.cm_alert("rec_plan not defined - zone_id: "+IORequest.zone_id)}var rc=this.get_target_from_plan(this.io_zone.rec_plan,IORequest.b_timeout||IORequest.b_404);this.action_callback("recommendation_plan");if(rc=="_DPF_"&&(IORequest.default_product_file!==undefined)){_io_request.cm_io_rec(IORequest.default_product_file);return(0)}if(rc=="_SX_"||rc=="_NR_"||rc=="_DPF_"){var heading_txt="";if(rc=="_SX_"){this.display_status("steps exhausted. Calling zone population function "+this.io_zone.zpf+" without recommendations.","blue");IORequest.log(IORequest.log_warn,"steps exhausted - calling zone population function without recommendations",this.io_zone.zpf);heading_txt="Steps exhausted.  No recommendations found"}else{this.display_status("calling zone population function "+this.io_zone.zpf+" without recommendations (_NR_)","blue");IORequest.log(IORequest.log_warn,"calling zone population function without recommendations",this.io_zone.zpf);heading_txt="No recommendations found"}if(this.io_zone.zpf!==undefined){var guts="[],'"+this.io_zone.name+"','_NR_','','',[],[],'"+heading_txt+"',"+zone_test_id;if(_io_config.zpfcid!="N"){guts=guts+", []"}var zpf=this.io_zone.zpf+"("+guts+")";IORequest.log(IORequest.log_trace,"Calling zone population function",zpf);setTimeout(zpf,0)}else{this.display_status("Zone population function "+this.io_zone.name+"_zp is not defined.","red");IORequest.log(IORequest.log_error,"Zone population function ",this.io_zone.name+"_zp is not defined")}setTimeout('IORequest.stack_manager("rc: '+rc+'");',0);return(0)}var item=rc;this.offer_id=this.io_zone.rec_plan.rec_steps[IORequest.current_step].offer_id;this.cgi_version=this.io_zone.rec_plan.rec_steps[IORequest.current_step].offer_version;this.offer_type=this.io_zone.rec_plan.rec_steps[IORequest.current_step].offer_type;IORequest.offer_type=this.offer_type;IORequest.offer_id=this.offer_id;if(item.length>1){var image_url_prefix=((window.location.protocol=="https:"&&IORequest.access_method=="json remote")?IORequest.image_url_prefix["json remote https"]:IORequest.image_url_prefix[IORequest.access_method]);var pqa_cookie=IORequest.find_cookie(IORequest.pqa_cookie);if(pqa_cookie!==undefined&&(pqa_cookie.indexOf("E")>-1)){image_url_prefix=image_url_prefix.replace(IORequest.io_recs,"http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/recsprodqa.coremetrics.com")}var itemList="";for(var i_prd=0;i_prd<item.length;i_prd++){itemList=itemList+item[i_prd]+"|"}IORequest.request_crc=undefined;this.url=image_url_prefix+"?cm_cid="+this.get_client_id()+"&cm_offerid="+this.offer_id+"&cm_offertype="+this.offer_type;if(IORequest.isMultiTargetStep(this.io_zone.rec_plan.rec_steps[IORequest.current_step])){this.url+="&cm_algorithm="+this.io_zone.rec_plan.rec_steps[IORequest.current_step].algo_id;var current_algo_value=this.io_zone.rec_plan.rec_steps[IORequest.current_step].algo_value;if((current_algo_value!==undefined)&&(current_algo_value!=="")){this.url+="&cm_algorithmvalue="+current_algo_value}}this.url+="&cm_targetid="+itemList;this.display_status("retrieving recommendations for multiple targets: "+itemList+" url: "+this.url,"green");IORequest.log(IORequest.log_trace,"retrieving recommendations for multiple targets: "+itemList+" - url",this.url)}else{if((IORequest.isProductBasedOffer(this.offer_type))&&((IORequest.isCategoryOffer(this.offer_type)&&!IORequest.encrypt_cats)||(IORequest.isProductOffer(this.offer_type)&&!IORequest.encrypt_prds))){this.item_id_crc=IORequest.encrypt8(item[0])}else{this.item_id_crc=item[0]}IORequest.request_crc=this.item_id_crc;this.group=this.item_id_crc.substr(0,2);var url_prefix=((window.location.protocol=="https:"&&IORequest.access_method=="json remote")?IORequest.url_prefix["json remote https"]:IORequest.url_prefix[IORequest.access_method]);var url_cookie=IORequest.find_cookie(IORequest.url_cookie);if(url_cookie!==undefined&&(url_cookie.indexOf("old")>-1)){url_prefix=((window.location.protocol=="https:"&&IORequest.access_method=="json remote")?IORequest.url_prefix_old["json remote https"]:IORequest.url_prefix_old[IORequest.access_method])}else{var pqa_cookie=IORequest.find_cookie(IORequest.pqa_cookie);if(pqa_cookie!==undefined&&(pqa_cookie.indexOf("A")>-1)){url_prefix=url_prefix+"prodqa/"}}var version_postfix="?V="+this.cgi_version;if(_io_config.vcgi=="N"){version_postfix=""}this.url=url_prefix+this.get_client_id()+"/"+g_version+"/"+this.offer_type+this.offer_id+"/"+this.offer_type+this.group+"/"+this.item_id_crc+".js"+version_postfix;this.display_status("retrieving recommendations for target: "+item[0]+" url: "+this.url,"green");IORequest.log(IORequest.log_trace,"retrieving recommendations for target: "+item[0]+" - url",this.url)}this.action_callback("product_request");if((IORequest.access_method=="ajax local")||(IORequest.access_method=="ajax remote")){this.xmlHttp=getXmlHttpObject();if(this.xmlHttp===null){this.cm_alert("Your browser really does not support Ajax!");return}this.h_timer=setTimeout("_io_request.ajax_timeout(0)",IORequest.timeout[this.i_timeout]);this.i_timeout=1;this.request_type="product";this.xmlHttp.onreadystatechange=this.stateChanged;this.stop_watch.start();try{this.xmlHttp.open("GET",this.url,true)}catch(e){clearTimeout(this.h_timer);this.display_status("Ajax Error: Cross Domain request attempted.  Ajax not supported.  Try json x-domain.","red");IORequest.rec_request_abort()}try{this.xmlHttp.send(null)}catch(e1){clearTimeout(this.h_timer);this.display_status("Ajax Error: Host not found.  Ajax not supported.  Try json x-domain.","red");IORequest.rec_request_abort()}}else{var request_timeout=(this.io_zone.rec_plan.rec_steps[IORequest.current_step].target_id=="_SS_"?IOConfig.sfto:IORequest.timeout[this.i_timeout]);this.h_timer=setTimeout("_io_request.javascript_timeout(0)",request_timeout);this.i_timeout=1;this.stop_watch.start();try{var h=document.getElementsByTagName("head").item(0);IORequest.h_script=document.createElement("script");IORequest.h_script.setAttribute("language","javascript");IORequest.h_script.setAttribute("type","text/javascript");IORequest.h_script.setAttribute("charset","UTF-8");IORequest.h_script.setAttribute("src",this.url);h.appendChild(IORequest.h_script)}catch(e2){IORequest.rec_request_abort()}}};this.download_config=function(){var url_prefix=((window.location.protocol=="https:"&&IORequest.access_method=="json remote")?IORequest.url_prefix["json remote https"]:IORequest.url_prefix[IORequest.access_method]);var url_cookie=IORequest.find_cookie(IORequest.url_cookie);if(url_cookie!==undefined&&(url_cookie.indexOf("old")>-1)){url_prefix=((window.location.protocol=="https:"&&IORequest.access_method=="json remote")?IORequest.url_prefix_old["json remote https"]:IORequest.url_prefix_old[IORequest.access_method])}else{var pqa_cookie=IORequest.find_cookie(IORequest.pqa_cookie);if(pqa_cookie!==undefined&&(pqa_cookie.indexOf("A")>-1)){url_prefix=url_prefix+"prodqa/"}}this.url=url_prefix+this.get_client_id()+"/"+g_config_filename+"?ts="+(((new Date().getTime())/600000)|0);this.display_status("retrieving IO Config file: "+g_config_filename+" url: "+this.url,"green");IORequest.log(IORequest.log_trace,"retrieving IO config file "+g_config_filename,this.url);this.action_callback("config_request");if((IORequest.access_method=="ajax local")||(IORequest.access_method=="ajax remote")){this.xmlHttp=getXmlHttpObject();if(this.xmlHttp===null){this.cm_alert("Your browser really does not support Ajax!");return}this.h_timer=setTimeout("_io_request.ajax_timeout(1)",IORequest.timeout[this.i_timeout]);this.i_timeout=1;this.request_type="config";this.xmlHttp.onreadystatechange=this.stateChanged;this.stop_watch.start();try{this.xmlHttp.open("GET",this.url,true)}catch(e){clearTimeout(this.h_timer);this.display_status("Ajax Error: Cross Domain request attempted.  Ajax not supported.  Try json x-domain.","red");IORequest.rec_request_abort()}try{this.xmlHttp.send(null)}catch(e1){clearTimeout(this.h_timer);this.display_status("Ajax Error: Host not found.  Ajax not supported.  Try json x-domain.","red");IORequest.rec_request_abort()}}else{this.h_timer=setTimeout("_io_request.javascript_timeout(1)",IORequest.timeout[this.i_timeout]);this.i_timeout=1;this.stop_watch.start();try{var h=document.getElementsByTagName("head").item(0);var js=document.createElement("script");js.setAttribute("language","javascript");js.setAttribute("type","text/javascript");js.setAttribute("src",this.url);h.appendChild(js)}catch(e2){IORequest.rec_request_abort()}}};this.download_ssp=function(orig_cookie,reg_id){var ssp_url_prefix=((window.location.protocol=="https:"&&IORequest.access_method=="json remote")?IORequest.ssp_url_prefix["json remote https"]:IORequest.ssp_url_prefix[IORequest.access_method]);var pqa_cookie=IORequest.find_cookie(IORequest.pqa_cookie);if(pqa_cookie!==undefined&&(pqa_cookie.indexOf("E")>-1)){ssp_url_prefix=ssp_url_prefix.replace(IORequest.io_recs,"http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/recsprodqa.coremetrics.com")}var orig_cookie_encoded=encodeURIComponent(orig_cookie);this.url=ssp_url_prefix+"?cm_cid="+_io_request.get_client_id()+"&cm_regid="+reg_id+"&cm_ioc="+orig_cookie_encoded;IORequest.log(IORequest.log_trace,"retrieving SSP data for encrypted reg id: "+reg_id+" - url",this.url);this.action_callback("ssp_request");if((IORequest.access_method=="ajax local")||(IORequest.access_method=="ajax remote")){this.xmlHttp=getXmlHttpObject();if(this.xmlHttp===null){this.cm_alert("Your browser really does not support Ajax!");return}this.h_timer=setTimeout("_io_request.ajax_timeout(2)",IORequest.timeout[this.i_timeout]);this.i_timeout=1;this.request_type="ssp";this.xmlHttp.onreadystatechange=this.stateChanged;this.stop_watch.start();try{this.xmlHttp.open("GET",this.url,true)}catch(e){clearTimeout(this.h_timer);this.display_status("Ajax Error: Cross Domain request attempted.  Ajax not supported.  Try json x-domain.","red");IORequest.rec_request_abort()}try{this.xmlHttp.send(null)}catch(e1){clearTimeout(this.h_timer);this.display_status("Ajax Error: Host not found.  Ajax not supported.  Try json x-domain.","red");IORequest.rec_request_abort()}}else{this.h_timer=setTimeout("_io_request.javascript_timeout(2)",IORequest.timeout[this.i_timeout]);this.i_timeout=1;this.stop_watch.start();try{var h=document.getElementsByTagName("head").item(0);IORequest.h_script=document.createElement("script");IORequest.h_script.setAttribute("language","javascript");IORequest.h_script.setAttribute("type","text/javascript");IORequest.h_script.setAttribute("charset","UTF-8");IORequest.h_script.setAttribute("src",this.url);h.appendChild(IORequest.h_script)}catch(e2){IORequest.rec_request_abort()}}};this.cm_io_rec=function(_j){this.stop_watch.stop();if(this.h_timer!==undefined){clearTimeout(this.h_timer);this.h_timer=undefined}if(this.io_zone.zpf!==undefined){if(_j!==undefined){var target_product_id=_j.pd[0][0];var target_crc=_j.hd[6];var offer_type=_j.hd[2];var offer_id=_j.hd[3];var num_recs=_j.hd[5];var num_static_parms=3;if(IORequest.isPageBasedOffer(offer_type)){num_static_parms=5}if(num_recs==0){this.display_status("Downloaded product file contains no recommendations.  Continuing to next step.","blue");IORequest.log(IORequest.log_warn,"Downloaded product file contains no recommendations.  Continuing to next step.");this.download_product()}else{if(IORequest.isSearchOffer(offer_type)){target_product_id=IORequest.raw_search_term.replace(/"/g,'\\"')}if((IORequest.request_crc!==undefined)&&(target_crc!==undefined)&&(target_crc.length==8)&&(IORequest.timeout_product[offer_id+target_crc])){IORequest.log(IORequest.log_trace,"Product download attempt following timeout for same file.  Requested file CRC",IORequest.request_crc);IORequest.timeout_product[offer_id+target_crc]=0;return}this.display_status("Successful download of recommendations for item: "+target_product_id+' <font color="black">('+this.stop_watch.elapsed_time+" ms)</font>.","green");IORequest.log(IORequest.log_trace,"successful retrieval of recommendations for item "+target_product_id,this.stop_watch.elapsed_time+" ms");IORequest.log(IORequest.log_iuo,"requested version: "+this.cgi_version+" returned version",_j.hd[9]);IORequest.log(IORequest.log_product_file,"product file",_j);var product_ids=[];var cat_ids=[];var page_urls=[];var page_names=[];var rec_attributes=[];var tgt_attributes=[];if(IORequest.isProductOffer(offer_type)){if((IOConfig.category_structure=="E")&&(_j.pd[0][2])){_io_state.cm_ted_io({i_offer:"epr_category",cg:_j.pd[0][2].toString().toUpperCase()})}if((+IOConfig.brand_personalization[0])!=-1){var bp_index=(+IOConfig.brand_personalization[0])+num_static_parms;_io_state.cm_ted_io({i_offer:"brand",brn:_j.pd[0][bp_index]})}}var score=[];var resort=false;var mpc=_io_state.cm_get_item_from_cookie("_MPC_",false);var mpb=_io_state.cm_get_item_from_cookie("_MPB_",false);for(var i_prd=1;i_prd<_j.pd.length;i_prd++){score[i_prd-1]=[];score[i_prd-1][0]=i_prd;score[i_prd-1][1]=5000;j_pd_score=_j.pd[i_prd][1];if(_j.pd[i_prd][1].length==2){score[i_prd-1][1]=_j.pd[i_prd][1][1];j_pd_score=_j.pd[i_prd][1][2]}score[i_prd-1][2]=j_pd_score;var native_rec=((score[i_prd-1][1]>=5000)&&(score[i_prd-1][1]<6000));if(native_rec){if((IORequest.optional_parm=="R")&&(IORequest.isCategoryOffer(offer_type))){score[i_prd-1][2]=Math.floor(Math.random()*1000);resort=true}else{if((mpc!==0)&&(_io_config.cp!==1)){var cat_compare=(IORequest.encrypt_cats?IORequest.encrypt8(_j.pd[i_prd][2]):_j.pd[i_prd][2]);score[i_prd-1][2]=score[i_prd-1][2]*((cat_compare==mpc[0])?_io_config.cp:1);resort=true}if(mpb!==0){var brand=_j.pd[i_prd][(+IOConfig.brand_personalization[0])+num_static_parms];var brand_crc=IORequest.encrypt8(brand);score[i_prd-1][2]=score[i_prd-1][2]*((brand_crc==mpb[0])?(+IOConfig.brand_personalization[1]):1)}}}}if(resort){score.sort(function(a,b){return(b[1]==a[1]?b[2]-a[2]:b[1]-a[1])})}l_attribute_array=_j.pd[0].length;for(var i_att=num_static_parms;i_att<l_attribute_array;i_att++){var prefix=((_j.ap!==undefined&&_j.ap[i_att-num_static_parms]!==undefined)?_j.ap[i_att-num_static_parms]:"");tgt_attributes.push((_j.pd[0][i_att]===undefined)?undefined:prefix+_j.pd[0][i_att].replace(/"/g,'\\"'))}var product_filter_crc=[];var product_filter_raw=[];if(this.io_zone.filter_cp){var lcps=_io_state.cm_get_item_from_cookie("_LCP_",true);if(lcps!==0){for(var i_cp=0;i_cp<lcps.length;i_cp++){if(IORequest.encrypt_prds){product_filter_crc[lcps[i_cp]]=1}else{product_filter_raw[lcps[i_cp]]=1}}}}var rpps=_io_state.cm_get_item_from_cookie("_RPP_",true);if(rpps!==0){if(this.io_zone.filter_pp){for(var i_pp=0;i_pp<rpps.length;i_pp++){if(IORequest.encrypt_prds){product_filter_crc[rpps[i_pp]]=1}else{product_filter_raw[rpps[i_pp]]=1}}}if(IORequest.item_id!==""){for(var i_si=0;i_si<IORequest.item_id.length;i_si++){if(IORequest.encrypt_prds){product_filter_crc[rpps[i_si]]=1}else{product_filter_raw[rpps[i_si]]=1}}}}for(var i_bl=0;i_bl<_io_config.bad_list.length;i_bl++){product_filter_crc[_io_config.bad_list[i_bl]]=1}IORequest.reason=[];var len_required_attributes=(_io_config.required_attrs.length);var a_deemphasized_by_segment=[];var a_tmp_deemph=[];for(var ii=0;((product_ids.length<this.io_zone.n_recs)&&(ii<score.length));ii++){var i_pd=score[ii][0];var item_raw=_j.pd[i_pd][0];var zpf_item=item_raw.replace(/"/g,'\\"');var item_crc=(IORequest.isContentBasedOffer(offer_type)?IORequest.encrypt16(item_raw):IORequest.encrypt8(item_raw));IORequest.reason[item_raw]=0;var b_all_required_attributes=true;var b_excluded_by_segment_filter=false;var b_deemphasized_by_segment_filter=false;if((IORequest.filtered_out_products[item_raw]===undefined)&&(product_filter_raw[item_raw]===undefined)&&(product_filter_crc[item_crc]===undefined)&&(IOState.h_productview_product[item_raw]===undefined)&&(IOState.h_pageview_page[item_raw]===undefined)){var a_tmp=[];for(var i_at=num_static_parms;((i_at<_j.pd[i_pd].length)&&(b_all_required_attributes===true));i_at++){if((len_required_attributes>(i_at-num_static_parms))&&(_io_config.required_attrs[i_at-num_static_parms])&&!(_j.pd[i_pd][i_at])){b_all_required_attributes=false}else{var at_prefix=((!IORequest.is_undefined(_j.ap)&&_j.ap[i_at-num_static_parms]!==undefined)?_j.ap[i_at-num_static_parms]:"");a_tmp.push((_j.pd[i_pd][i_at]===undefined)?undefined:at_prefix+_j.pd[i_pd][i_at].replace(/"/g,'\\"'))}}if(b_all_required_attributes){var segment=_io_state.get_pf_segment_from_cookie();if(segment!=""){b_zone_found=false;for(var i_zones=0;((i_zones<_io_config.pf_zone_list.length)&&(b_zone_found===false));i_zones++){if(_io_config.pf_zone_list[i_zones]==this.io_zone.name){b_zone_found=true}}if(b_zone_found){if(_j.mids!==undefined){var i_attr_index=-1;for(var i_mids=0;((i_mids<_j.mids.length)&&(i_attr_index===-1));i_mids++){if(_j.mids[i_mids]==_io_config.pf_metric_id){i_attr_index=i_mids}}if(i_attr_index!=-1){var segment_attr_value=_j.pd[i_pd][num_static_parms+i_attr_index];var segment_array=segment_attr_value.split(_io_config.multi_target_delim);var in_segment=false;for(var i_segment=0;i_segment<segment_array.length;i_segment++){if(IORequest.encrypt16(segment_array[i_segment])==segment){in_segment=true}}if(!in_segment){if(_io_config.pf_filter_type=="EXCLUDE"){b_excluded_by_segment_filter=true}else{if(_io_config.pf_filter_type=="DEEMPHASIZE"){b_deemphasized_by_segment_filter=true;a_deemphasized_by_segment.push(_j.pd[i_pd]);a_tmp_deemph.push(a_tmp)}}}}}}}if(b_excluded_by_segment_filter){IORequest.log(IORequest.log_trace,zpf_item+" is not in segment","not sent to zpf");IORequest.reason[item_raw]=8}else{if(b_deemphasized_by_segment_filter){IORequest.log(IORequest.log_trace,zpf_item+" is not in segment","moved to end of recommendation list");IORequest.reason[item_raw]=9}else{product_ids.push(zpf_item);cat_ids.push(_j.pd[i_pd][2]);if(IORequest.isPageBasedOffer(offer_type)){page_urls.push(_j.pd[i_pd][3]);page_names.push(_j.pd[i_pd][4])}if(IORequest.conflict_resolution===true){IORequest.filtered_out_products[item_raw]=1}rec_attributes.push('["'+a_tmp.join('","')+'"]')}}}else{IORequest.log(IORequest.log_trace,zpf_item+" required attribute not present","not sent to zpf");IORequest.reason[item_raw]=1}}else{if((product_filter_raw[item_raw]!==undefined)||(product_filter_crc[item_crc]!==undefined)){IORequest.log(IORequest.log_trace,zpf_item+" is recently carted or purchased, is in bad item list, or is the specified item on the recommendation request","not sent to zpf");IORequest.reason[item_raw]=2}else{if(IORequest.filtered_out_products[item_raw]!==undefined){IORequest.log(IORequest.log_trace,zpf_item+" appears in previous zone","not sent to zpf");IORequest.reason[item_raw]=3}else{if(IOState.h_productview_product[item_raw]!==undefined){IORequest.log(IORequest.log_trace,zpf_item+" appears in the recommendation list but is also a product for which a product view tag was issued for this page","not sent to zpf");IORequest.reason[item_raw]=5}else{if(IOState.h_pageview_page[item_raw]!==undefined){IORequest.log(IORequest.log_trace,zpf_item+" appears in the recommendation list but is also a page for which a page view tag was issued for this page","not sent to zpf");IORequest.reason[item_raw]=7}}}}}}for(var jj=0;((product_ids.length<this.io_zone.n_recs)&&(jj<a_deemphasized_by_segment.length));jj++){var item_raw_deemph=a_deemphasized_by_segment[jj][0];var zpf_item_deemph=item_raw_deemph.replace(/"/g,'\\"');product_ids.push(zpf_item_deemph);cat_ids.push(a_deemphasized_by_segment[jj][2]);if(IORequest.isPageBasedOffer(offer_type)){page_urls.push(a_deemphasized_by_segment[jj][3]);page_names.push(a_deemphasized_by_segment[jj][4])}if(IORequest.conflict_resolution===true){IORequest.filtered_out_products[item_raw_deemph]=1}rec_attributes.push('["'+a_tmp_deemph[jj].join('","')+'"]')}var target_header_txt=[];target_header_txt._SP_="Recommendations";target_header_txt._SG_="Page Recommendations";target_header_txt._SE_="Element Recommendations";target_header_txt._SC_="Top Selling Items";target_header_txt._SGC_="Top Viewed Pages";target_header_txt._SEC_="Top Viewed Elements";target_header_txt._NR_="No Recommendations";target_header_txt._RVP_="Recently viewed item(s)";target_header_txt._RVG_="Recently viewed page(s)";target_header_txt._RVL_="Recently viewed items";target_header_txt._RVLG_="Recently viewed pages";target_header_txt._RPP_="Recently purchased item(s)";target_header_txt._LCP_="Recently carted item(s)";target_header_txt._RVC_="Recommendations from a category you've recently viewed";target_header_txt._MPC_="Top selling items from a category of your interest";target_header_txt._MSP_="Most Significant Purchase";target_header_txt._SS_="Recommendations based on search terms";target_header_txt._DPF_="Default Recommendations";var parms=[];var b_has_recs=product_ids.length?true:false;var target_id=b_has_recs?this.io_zone.rec_plan.rec_steps[IORequest.current_step].target_id:"_NR_";if(!b_has_recs){IORequest.log(IORequest.log_trace,"No recommendations made it through the filters","changing target symbolic from "+this.io_zone.rec_plan.rec_steps[IORequest.current_step].target_id+" to _NR_.")}var heading=this.io_zone.rec_plan.rec_steps[IORequest.current_step].heading||target_header_txt[target_id];parms.push(b_has_recs?'["'+product_ids.join('","')+'"]':"[]");parms.push('"'+this.io_zone.name+'"');parms.push('"'+target_id+'"');parms.push('"'+target_product_id+'"');parms.push('"'+_j.pd[0][2]+'"');parms.push("["+rec_attributes.join()+"]");parms.push('["'+tgt_attributes.join('","')+'"]');parms.push('"'+heading+'"');parms.push('"'+(this.io_zone.ab_test_id||"")+'"');parms.push(b_has_recs?'["'+cat_ids.join('","')+'"]':"[]");parms.push(IORequest.isPageBasedOffer(offer_type)?'"'+_j.pd[0][3]+'"':'""');parms.push(IORequest.isPageBasedOffer(offer_type)?'"'+_j.pd[0][4]+'"':'""');parms.push((b_has_recs&&IORequest.isPageBasedOffer(offer_type))?'["'+page_urls.join('","')+'"]':"[]");parms.push((b_has_recs&&IORequest.isPageBasedOffer(offer_type))?'["'+page_names.join('","')+'"]':"[]");var call=this.io_zone.zpf+"("+parms.join()+")";if(this.io_zone.zpf!==undefined){IORequest.log(IORequest.log_trace,"Calling zone population function",call);setTimeout(call,0)}setTimeout('IORequest.stack_manager("successful product retrieval");',0)}}else{setTimeout('IORequest.stack_manager("successful product retrieval");',0)}}else{this.display_status("Zone population function "+this.io_zone.name+"_zp is not defined.","red");IORequest.log(IORequest.log_error,"Zone population function ",this.io_zone.name+"_zp is not defined")}};this.cm_io_cfg=function(_json,b_download_from_cdn){this.stop_watch.stop();clearTimeout(_io_request.h_timer);_io_request.h_timer=undefined;if(_io_config===undefined){if(_json!==undefined){this.action_callback(b_download_from_cdn?"server_cfg":"default_cfg");IORequest.log(IORequest.log_trace,"successful retrieval of config file",this.stop_watch.elapsed_time+" ms");IORequest.log(IORequest.log_config_file,"config file",_json);_io_state.set_ab_test_group_from_cookie();if(_json.zp!==undefined){_io_config=new IOConfig(_json);this.action_callback("config_return")}else{setTimeout('IORequest.config_download_failure("corrupt config file");',0)}if(b_download_from_cdn){IORequest.i_zone=0;setTimeout('IORequest.config_downloaded("successful config download");',0)}}}else{IORequest.log(IORequest.log_warn,"config request where _io_config already defined","aborting request")}};this.cm_io_ssp=function(_json){this.stop_watch.stop();clearTimeout(_io_request.h_timer);_io_request.h_timer=undefined;if(this.h_timer!==undefined){clearTimeout(this.h_timer);this.h_timer=undefined}if(_json!==undefined){this.action_callback("ssp_retrieved");IORequest.log(IORequest.log_trace,"successful retrieval of ssp",this.stop_watch.elapsed_time+" ms");IORequest.log(IORequest.log_config_file,"ssp file",_json);if(_json.success){if(_json.value!==undefined){var pseudo_cookies=_json.value.split(IORequest.cookie_separator);if(pseudo_cookies.length>=10){pseudo_cookies[10]=new Date().getTime().toString()}var new_cookie_value=pseudo_cookies.join(IORequest.cookie_separator);IORequest.set_and_check_cookie(IORequest.state_cookie,new_cookie_value,false,IORequest.vanity_suffix);IORequest.recently_viewed_product=undefined;IORequest.recently_viewed_category=undefined;IORequest.recently_viewed_page=undefined;_io_state.cm_build_all_recent_arrays()}}else{IORequest.log(IORequest.log_trace,"SSP download failed: "+_json.message)}this.action_callback("ssp_complete");IORequest.i_zone=0;setTimeout('IORequest.ssp_processed("ssp processing complete");',0)}}}IORequest.crc32_tab=[0,1996959894,3993919788,2567524794,124634137,1886057615,3915621685,2657392035,249268274,2044508324,3772115230,2547177864,162941995,2125561021,3887607047,2428444049,498536548,1789927666,4089016648,2227061214,450548861,1843258603,4107580753,2211677639,325883990,1684777152,4251122042,2321926636,335633487,1661365465,4195302755,2366115317,997073096,1281953886,3579855332,2724688242,1006888145,1258607687,3524101629,2768942443,901097722,1119000684,3686517206,2898065728,853044451,1172266101,3705015759,2882616665,651767980,1373503546,3369554304,3218104598,565507253,1454621731,3485111705,3099436303,671266974,1594198024,3322730930,2970347812,795835527,1483230225,3244367275,3060149565,1994146192,31158534,2563907772,4023717930,1907459465,112637215,2680153253,3904427059,2013776290,251722036,2517215374,3775830040,2137656763,141376813,2439277719,3865271297,1802195444,476864866,2238001368,4066508878,1812370925,453092731,2181625025,4111451223,1706088902,314042704,2344532202,4240017532,1658658271,366619977,2362670323,4224994405,1303535960,984961486,2747007092,3569037538,1256170817,1037604311,2765210733,3554079995,1131014506,879679996,2909243462,3663771856,1141124467,855842277,2852801631,3708648649,1342533948,654459306,3188396048,3373015174,1466479909,544179635,3110523913,3462522015,1591671054,702138776,2966460450,3352799412,1504918807,783551873,3082640443,3233442989,3988292384,2596254646,62317068,1957810842,3939845945,2647816111,81470997,1943803523,3814918930,2489596804,225274430,2053790376,3826175755,2466906013,167816743,2097651377,4027552580,2265490386,503444072,1762050814,4150417245,2154129355,426522225,1852507879,4275313526,2312317920,282753626,1742555852,4189708143,2394877945,397917763,1622183637,3604390888,2714866558,953729732,1340076626,3518719985,2797360999,1068828381,1219638859,3624741850,2936675148,906185462,1090812512,3747672003,2825379669,829329135,1181335161,3412177804,3160834842,628085408,1382605366,3423369109,3138078467,570562233,1426400815,3317316542,2998733608,733239954,1555261956,3268935591,3050360625,752459403,1541320221,2607071920,3965973030,1969922972,40735498,2617837225,3943577151,1913087877,83908371,2512341634,3803740692,2075208622,213261112,2463272603,3855990285,2094854071,198958881,2262029012,4057260610,1759359992,534414190,2176718541,4139329115,1873836001,414664567,2282248934,4279200368,1711684554,285281116,2405801727,4167216745,1634467795,376229701,2685067896,3608007406,1308918612,956543938,2808555105,3495958263,1231636301,1047427035,2932959818,3654703836,1088359270,936918000,2847714899,3736837829,1202900863,817233897,3183342108,3401237130,1404277552,615818150,3134207493,3453421203,1423857449,601450431,3009837614,3294710456,1567103746,711928724,3020668471,3272380065,1510334235,755167117];IORequest.crc32_add=function(a,b){return IORequest.crc32_tab[(a^b)&255]^((a>>8)&16777215)};IORequest.crc32_str=function(c){var d;var a=c.length;var b;b=4294967295;for(d=0;d<a;d++){b=IORequest.crc32_add(b,c.charCodeAt(d))}return b^4294967295};IORequest.hex32=function(c){var d;var b;var a;d=c&65535;b=d.toString(16).toUpperCase();while(b.length<4){b="0"+b}d=(c>>>16)&65535;a=d.toString(16).toUpperCase();while(a.length<4){a="0"+a}return a+b};IORequest.isProductOffer=function(a){return(a=="P")};IORequest.isSearchOffer=function(a){return(a=="S")};IORequest.isEPRCategoryOffer=function(a){return(a=="E")};IORequest.isSiteCategoryOffer=function(a){return(a=="C")};IORequest.isCategoryOffer=function(a){return(IORequest.isEPRCategoryOffer(a)||IORequest.isSiteCategoryOffer(a)||IORequest.isPageCategoryOffer(a)||IORequest.isElementCategoryOffer(a))};IORequest.isPageOffer=function(a){return(a=="A")};IORequest.isPageCategoryOffer=function(a){return(a=="F")};IORequest.isPageBasedOffer=function(a){return(IORequest.isPageOffer(a)||IORequest.isPageCategoryOffer(a))};IORequest.isElementOffer=function(a){return(a=="B")};IORequest.isElementCategoryOffer=function(a){return(a=="G")};IORequest.isElementBasedOffer=function(a){return(IORequest.isElementOffer(a)||IORequest.isElementCategoryOffer(a))};IORequest.isContentBasedOffer=function(a){return(IORequest.isPageOffer(a)||IORequest.isElementOffer(a)||IORequest.isPageCategoryOffer(a)||IORequest.isElementCategoryOffer(a))};IORequest.isProductBasedOffer=function(a){return(!IORequest.isContentBasedOffer())};IORequest.isMultiTargetStep=function(a){return((a.algo_id!==undefined)&&(a.algo_id!==""))};IORequest.reverse=function(a){return a.split("").reverse().join("")};IORequest.encrypt16=function(a){return IORequest.hex32(IORequest.crc32_str(a))+IORequest.hex32(IORequest.crc32_str(IORequest.reverse(a)))};IORequest.encrypt8=function(a){return IORequest.hex32(IORequest.crc32_str(a))};IORequest.cookie_info=function(b,g){var k=document.cookie;var a=k.length;var h=k.split(";").length;IORequest.log(IORequest.log_trace,"cookie_length: "+a+" number of cookies",IORequest.cookie_count(b));IORequest.log(IORequest.log_trace,"cookie",k);alert("n: "+h+" l: "+a+" cookie: "+k);if(g){var f=g-a-3-b.length;var e="";for(var d=0;d<f;d++){e+=""+d%10}IORequest.set_and_check_cookie(b,e);IORequest.cookie_info(b)}};IORequest.cookie_count=function(a){var d=document.cookie;var b=0;if(d){b=d.split(";").length}return b};IORequest.find_cookie=function(c){var d=document.cookie.split("; ");var b=c.length;for(var a=0;a<d.length;a++){if((c+"=")==d[a].substring(0,b+1)){return(d[a].substring(b+1))}}return(undefined)};IORequest.rm_cookie=function(a){document.cookie=a+"=;path=/;expires="+new Date(1998,0).toGMTString()+";;"};IORequest.set_and_check_cookie=function(d,a,b,c){CB(d,a,b?null:new Date(2020,0).toGMTString(),c?c:null);a=IORequest.find_cookie(d);if(a===undefined){if(!b){IORequest.perm_cookie_not_supported=true}}IORequest.log(IORequest.log_cookie_write,"write "+d,IORequest.is_undefined(a)?"permanent cookies disabled":a);return(a)};IORequest.build_array_from_cookie=function(b,a){var c=IORequest.find_state_cookie(b);return((c===undefined)?undefined:(c.split(IORequest.cookie_separator))[a])};IORequest.find_state_cookie=function(c){if(IORequest.vanity_suffix===undefined){if(cm_JSFPCookieDomain===null||cm_JSFPCookieDomain===undefined){var f=document.domain;if(f){var d=/[^.]+\.[^.]+$/;IORequest.vanity_suffix="."+f.match(d)}}else{IORequest.vanity_suffix=cm_JSFPCookieDomain}}var g=IORequest.find_cookie(c);if(g===undefined){var a=((IORequest.ie_version()!==null)&&(IORequest.ie_version()<7))?20:30;if(IORequest.cookie_count()>=a){g=undefined}else{if(c==IORequest.state_cookie){var e=Math.floor(Math.random()*100);g=[e,IOConfig.version,IOConfig.brand_personalization[0],IOConfig.brand_personalization[1],IOConfig.category_structure,IORequest.a_max_elements[0],IORequest.a_max_elements[1],IORequest.a_max_elements[2],IORequest.a_max_elements[3],IORequest.a_max_elements[4],IORequest.a_max_elements[5],IORequest.a_max_elements[6]].join("~")+IORequest.cookie_separator+IORequest.cookie_separator+IORequest.cookie_separator+IORequest.cookie_separator+IORequest.cookie_array_separator+IORequest.cookie_array_separator+IORequest.cookie_array_separator+IORequest.cookie_array_separator+IORequest.cookie_array_separator+IORequest.cookie_array_separator+IORequest.cookie_separator+IORequest.cookie_separator+IORequest.cookie_separator+IORequest.cookie_separator+IORequest.cookie_separator+IORequest.cookie_separator+IORequest.cookie_separator+IORequest.cookie_separator}else{if(c==IORequest.state_cookie_content){g=[IORequest.a_max_page_elements[0]].join("~")+IORequest.cookie_separator+IORequest.cookie_separator+IORequest.cookie_array_separator}}var b=g;g=IORequest.set_and_check_cookie(c,b,false,IORequest.vanity_suffix)}}return(g)};IORequest.default_json={zp:[{id:"Default_Zone",rp:[["001",0,99,3]]}],rp:{"001":[["101","_DPF_","0","You might be interested in"]]},oa:{"101":["4","P"]}};IORequest.i_zone=1;IORequest.i_msg=0;IORequest.rec_stack=[];IORequest.filtered_out_products=[];IORequest.b_timeout=false;IORequest.b_404=false;IORequest.zone_id=0;IORequest.encrypt_16=0;IORequest.item_id=0;IORequest.single_item_id="";IORequest.category_id=0;IORequest.single_category_id="";IORequest.raw_search_term="";IORequest.current_step=-1;IORequest.timeout_product=[];IORequest.ssp_reg_id="";IORequest.ssp_use_reg_id=undefined;IORequest.ssp_allow_flag=undefined;IORequest.setRegIdCalled=undefined;IORequest.pf_segment="";IORequest.setSegmentCalled=undefined;IORequest.cookie_separator="~|~";IORequest.cookie_array_separator="|";IORequest.ses_cookie="CoreM_Ses";IORequest.state_cookie="CoreM_State";IORequest.state_cookie_content="CoreM_State_Content";IORequest.test_cookie="CoreM_State_Test";IORequest.pqa_cookie="CoreM_State_pqa";IORequest.url_cookie="CoreM_State_url";IORequest.no_log_cookie="CoreM_State_No_Log";IORequest.recently_viewed_product=undefined;IORequest.recently_viewed_page=undefined;IORequest.recently_viewed_category=undefined;IORequest.perm_cookie_not_supported=false;IORequest.a_max_page_elements=[6];IORequest.access_method="json local";IORequest.ab_group_number=undefined;IORequest.have_cookie=false;IORequest.log_cookie_write=2<<1;IORequest.log_config_file=2<<2;IORequest.log_product_file=2<<3;IORequest.log_trace=2<<4;IORequest.log_warn=2<<5;IORequest.log_error=2<<6;IORequest.log_iuo=2<<7;IORequest.production=false;IORequest.log_mask=IORequest.production?IORequest.log_error:(2<<16)-1;IORequest.log_mask=IORequest.log_mask&~IORequest.log_iuo;IORequest.breaklines=function(b){var a="";while(b.length>0){a+=b.substring(0,190)+"\n";b=b.substring(190)}return a};IORequest.log=function(c,b,a){if(!IORequest.disable_console_logging){if(typeof console!=="undefined"){if(IORequest.find_cookie(IORequest.no_log_cookie)===undefined){if(a!==undefined){b=b+": "+a}b=IORequest.breaklines(b);if(c==IORequest.log_product_file||c==IORequest.log_config_file){if(console.group){console.group()}if(console.dir){console.dir(a)}if(console.groupEnd){console.groupEnd()}}else{if(c==IORequest.log_warn){if(console.warn){console.warn(b)}}else{if(c==IORequest.log_error){if(console.error){console.error(b)}}else{if(IORequest.log_mask&c){if(console.log){console.log(b)}}}}}}}}};IORequest.ie_version=function(){return(/MSIE (\d+\.\d+);/.test(navigator.userAgent)?RegExp.$1:null)};IORequest.io_cdn="http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/iocdn.coremetrics.com";IORequest.io_recs="http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/recs.coremetrics.com";IORequest.url_prefix=[];IORequest.url_prefix["ajax local"]="";IORequest.url_prefix["ajax remote"]="/limelight/";IORequest.url_prefix["json local"]="";IORequest.url_prefix["json remote"]="http://"+IORequest.io_cdn+"/";IORequest.url_prefix["json remote https"]="https://"+IORequest.io_cdn+"/";IORequest.url_prefix_old=[];IORequest.url_prefix_old["json remote"]="http://coremetric.vo.llnwd.net/o33/";IORequest.url_prefix_old["json remote https"]="https://coremetric.hs.llnwd.net/o33/";IORequest.image_url_prefix=[];IORequest.image_url_prefix["json remote"]="http://"+IORequest.io_recs+"/iorequest/prodrecs";IORequest.image_url_prefix["json remote https"]="https://"+IORequest.io_recs+"/iorequest/prodrecs";IORequest.ssp_url_prefix=[];IORequest.ssp_url_prefix["json remote"]="http://"+IORequest.io_recs+"/iorequest/ssp";IORequest.ssp_url_prefix["json remote https"]="https://"+IORequest.io_recs+"/iorequest/ssp";IORequest.rec_request=function(a,d,b,c,e){IORequest.log(IORequest.log_trace,"cmRecRequest",a+(d?","+d:",")+(b?","+b:",")+(c?","+c:",")+(e?","+e:""));IORequest.rec_stack.push([a,false,d,b,c,e])};IORequest.page_rec_request=function(a,b,c,d){IORequest.log(IORequest.log_trace,"cmPageRecRequest",a+","+b+","+c);IORequest.rec_stack.push([a,true,b,c,d])};IORequest.element_rec_request=function(a,b,c,d){IORequest.log(IORequest.log_trace,"cmElementRecRequest",a+","+b+","+c);IORequest.rec_stack.push([a,true,b,c,d])};IORequest.rec_request_abort=function(){IORequest.rec_stack=[];IORequest.filtered_out_products=[];IORequest.log(IORequest.log_trace,"Aborted request","communication exception")};IORequest.display_recs=function(){IORequest.log(IORequest.log_trace,"cmDisplayRecs");IORequest.i_msg=0;IORequest.i_zone=1;IORequest.filtered_out_products=[];_io_config=undefined;if(IORequest.chris_dot_html_config){_io_config=new IOConfig(IORequest.chris_dot_html_config);IORequest.log(IORequest.log_config_file,"config file",IORequest.chris_dot_html_config);IORequest.i_zone=0;IORequest.stack_manager("http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/chris.html")}else{_io_request.download_config()}};IORequest.config_downloaded=function(a){var c=false;if(IORequest.ssp_use_reg_id){var e=_io_state.get_ssp_load_ts_from_cookie();var b=new Date().getTime();var d=new Date(e);d.setMinutes(d.getMinutes()+new Number(_io_config.ssp_retrieve_int));if(b>(d.getTime())){var f=IORequest.find_cookie(IORequest.state_cookie);if(f!==undefined){c=true;_io_request.download_ssp(f,IORequest.ssp_reg_id)}}}if(!c){IORequest.stack_manager(a)}};IORequest.config_download_failure=function(a){_io_config=new IOConfig(IORequest.default_json);for(var b=0;b<IORequest.rec_stack.length;b++){_io_config.add_zone(IORequest.rec_stack[b][0])}IORequest.stack_manager(a)};IORequest.ssp_processed=function(a){IORequest.stack_manager(a)};IORequest.ssp_download_failure=function(a){IORequest.stack_manager(a)};IORequest.encode_search_term=function(c){c=c.toString().toUpperCase();if(IOConfig.stpr){for(var a=0;a<IOConfig.stpr.length;a++){var b=IOConfig.stpr[a];b=b.toString().toUpperCase();if(c.substring(0,b.length)==b){c=c.substr(b.length)}}}c=c.replace(/[$'&`~@:\[\]\\!%^*()={}\| <>"]/g,"");return(c)};IORequest.stack_manager=function(a){if(IORequest.rec_stack.length){var c=IORequest.rec_stack.shift();IORequest.i_zone++;IORequest.i_msg=0;IORequest.zone_id=c[0];IORequest.encrypt_16=(c.length>1?c[1]:false);var b=(c.length>2?c[2]:"");b=(b==undefined?"":b.toString().toUpperCase());IORequest.plain_text_item_id=b;if(b!=""){if(IORequest.encrypt_16){IORequest.single_item_id=new Array(IORequest.encrypt16(b))}else{if(IORequest.encrypt_prds){IORequest.single_item_id=new Array(IORequest.encrypt8(b))}}b=b.split(_io_config.multi_target_delim);for(i_item_id=0;i_item_id<b.length;i_item_id++){if(IORequest.encrypt_16){b[i_item_id]=IORequest.encrypt16(b[i_item_id])}else{if(IORequest.encrypt_prds){b[i_item_id]=IORequest.encrypt8(b[i_item_id])}}}}IORequest.item_id=b;var e=(c.length>3?c[3]:"");e=(e==undefined?"":e.toString().toUpperCase());IORequest.plain_text_cat_id=e;if(e!=""){if(IORequest.encrypt_16){IORequest.single_category_id=new Array(IORequest.encrypt16(e))}else{if(IORequest.encrypt_cats){IORequest.single_category_id=new Array(IORequest.encrypt8(e))}}e=e.split(_io_config.multi_target_delim);for(i_cat_id=0;i_cat_id<e.length;i_cat_id++){if(IORequest.encrypt_16){e[i_cat_id]=IORequest.encrypt16(e[i_cat_id])}else{if(IORequest.encrypt_cats){e[i_cat_id]=IORequest.encrypt8(e[i_cat_id])}}}}IORequest.category_id=e;IORequest.optional_parm=(c.length>4?c[4]:"");IORequest.optional_parm=(IORequest.optional_parm==undefined?"":IORequest.optional_parm);IORequest.raw_search_term=(c.length>5?c[5]:"");IORequest.raw_search_term=(IORequest.raw_search_term==undefined?"":IORequest.raw_search_term);if(IORequest.raw_search_term!=""){var d=IORequest.encode_search_term(IORequest.raw_search_term);IORequest.plain_text_scrubbed_search_id=d;c[5]=d;IOConfig.crc_specified_search=new Array(IORequest.encrypt8(d))}else{IOConfig.crc_specified_search=""}IORequest.current_step=-1;IORequest.b_timeout=false;IORequest.b_404=false;if(_io_config.zones[IORequest.zone_id]===undefined){IORequest.log(IORequest.log_error,"Zone "+IORequest.zone_id+" is not defined in the configuration file","no action taken");IORequest.stack_manager("zone: "+IORequest.zone_id+" is not defined in the configuration file")}else{_io_request.display_status("stack_manager called - "+a+" - parms: "+c.join(", "),"green");IORequest.log(IORequest.log_trace,"stack_manager called - "+a+" - parms",c.join(", "));_io_request.download_product()}}else{if(IORequest.i_zone==3){IORequest.i_zone=2}_io_request.display_status("All recommendation requests completed","green");IORequest.log(IORequest.log_trace,"All recommendation requests completed for zone",IORequest.zone_id);IORequest.i_zone=1;IORequest.i_msg=0}};IORequest.is_undefined=function(a){var b;return(a===b)};IORequest.inspect_json=function(e,a,h){var g="",b,f;if(h===null||h===undefined){h=0}if(a===null||a===undefined){a=1}if(a<1){return'<font color="red">Error: Levels number must be > 0</font>'}if(e===null||e===undefined){return'<font color="red">Error: Object <b>NULL</b></font>'}g+="<ul>";var d;for(d in e){if(true){try{b=typeof(e[d]);g+="<li>("+b+") "+d+((e[d]===null)?(": <b>null</b>"):(':  <font color="red">'+e[d]+"</font>"))+"</li>";if((b=="object")&&(e[d]!==null)&&(h+1<a)){g+=IORequest.inspect_json(e[d],a,h+1)}}catch(c){if(typeof(c)=="string"){f=c}else{if(c.message){f=c.message}else{if(c.description){f=c.description}else{f="Unknown"}}}g+='<li><font color="red">(Error) '+d+": "+f+"</font></li>"}}}g+="</ul>";return g};IOConfig.version=-1;IOConfig.brand_personalization=[-1,-1];IOConfig.category_structure=-1;IOConfig.stpr=[];IOConfig.crc_specified_search="";function IOConfig(h){_io_state.cm_build_all_recent_arrays();var l=false;this.io=h;if(((IORequest.ie_version()!==null)&&(IORequest.ie_version()<7))){if(this.io.cie6b!==undefined){for(var i=0;i<IORequest.a_max_elements.length;i++){if(this.io.cie6b[i]!=IORequest.a_max_elements[i]){IORequest.a_max_elements[i]=this.io.cie6b[i];l=true}}}}else{if(this.io.cdfltb!==undefined){for(var c=0;c<IORequest.a_max_elements.length;c++){if(this.io.cdfltb[c]!=IORequest.a_max_elements[c]){IORequest.a_max_elements[c]=this.io.cdfltb[c];l=true}}}}if(this.io.cdfltpg!==undefined){for(var g=0;g<IORequest.a_max_page_elements.length;g++){if(this.io.cdfltpg[g]!=IORequest.a_max_page_elements[g]){IORequest.a_max_page_elements[g]=this.io.cdfltpg[g];l=true}}}if(this.io.cs===undefined){if(IOConfig.category_structure==-1){IOConfig.category_structure="S"}}else{var e=(this.io.cs!=="EPR");var b=(IOConfig.category_structure!=="E");if(e!==b){l=true;IOConfig.category_structure=(this.io.cs=="EPR"?"E":"S")}}if(this.io.cv!==undefined){if(IOConfig.version!==this.io.cv){l=true;IOConfig.version=this.io.cv}}if(this.io.bp!==undefined){if(IOConfig.brand_personalization[0]!=this.io.bp[0]){IOConfig.brand_personalization[0]=this.io.bp[0];l=true}if(IOConfig.brand_personalization[1]!=this.io.bp[1]){IOConfig.brand_personalization[1]=this.io.bp[1];l=true}}IORequest.ssp_use_reg_id=false;var k=_io_state.get_ssp_reg_id_from_cookie();if(this.io.sspe!==undefined){if(this.io.sspe=="Y"){if((this.io.sspl===undefined)||(this.io.sspl=="OPT_IN_MANDATORY")){if(IORequest.setRegIdCalled){if(IORequest.ssp_allow_flag){IORequest.ssp_use_reg_id=true;l=true}}}else{if(this.io.sspl=="OPT_IN_BY_DEFAULT"){if(IORequest.setRegIdCalled){if((IORequest.ssp_allow_flag===undefined)||(IORequest.ssp_allow_flag)){IORequest.ssp_use_reg_id=true;l=true}}else{IORequest.ssp_use_reg_id=true;IORequest.ssp_reg_id=k}}}}}if((IORequest.ssp_use_reg_id==false)&&(k!=="")){IORequest.ssp_reg_id="";l=true}if(l&&IORequest.have_cookie){var d=[IORequest.ab_group_number,IOConfig.version,IOConfig.brand_personalization[0],IOConfig.brand_personalization[1],IOConfig.category_structure,IORequest.a_max_elements[0],IORequest.a_max_elements[1],IORequest.a_max_elements[2],IORequest.a_max_elements[3],IORequest.a_max_elements[4],IORequest.a_max_elements[5],IORequest.a_max_elements[6]];var a=[IORequest.a_max_page_elements[0]];_io_state.cm_write_cookies(d,a);l=0}_io_state.cm_build_all_recent_arrays();IOConfig.stpr=this.io.stpr||[];IOConfig.sfto=this.io.sfto||1500;this.fcpl=this.io.fcpl===undefined?"N":this.io.fcpl.toString().toUpperCase();this.vcgi=this.io.vcgi===undefined?"Y":this.io.vcgi.toString().toUpperCase();this.zpfcid=this.io.zpfcid===undefined?"Y":this.io.zpfcid.toString().toUpperCase();this.required_attrs=this.io.ra||[];this.cp=this.io.cp||1;if(this.io.pfto!==undefined){IORequest.timeout[1]=this.io.pfto}if(this.io.fnf!==undefined){this.file_not_found_id=this.io.fnf[0];this.file_not_found_pc=this.io.fnf[1]}this.bad_list=this.io.bl||[];this.ps=this.io.ps===undefined?1:this.io.ps;this.ssp_access_method=this.io.sspa===undefined?"REGISTRATION_ID":this.io.sspa.toString().toUpperCase();this.ssp_retrieve_int=this.io.sspi===undefined?"30":this.io.sspi.toString().toUpperCase();this.pf_filter_type=this.io.pftype===undefined?"DEEMPHASIZE":this.io.pftype.toString().toUpperCase();this.pf_zone_list=this.io.pfzones||[];this.pf_metric_id=this.io.pfmetric===undefined?undefined:this.io.pfmetric.toString().toUpperCase();this.multi_target_delim=this.io.mtdelim===undefined?"|":this.io.mtdelim.toString().toUpperCase();this.zones=[];this.n_zones=this.io.zp.length;this.rec_plan=[];for(var f=0;f<this.n_zones;f++){this.zones[this.io.zp[f].id]=new IOZone(this.io.zp[f],this.rec_plan,this.io.rp,this.io.oa)}this.add_zone=function(m){var n={id:m,rp:[["001",0,99,3]]};this.zones[m]=new IOZone(n,this.rec_plan,this.io.rp,this.io.oa)}}function IOZone(h,g,c,e){var b=undefined;this.name=h.id;var a=this.name+"_zp";if((window[a]!==undefined)&&(typeof window[a]=="function")){this.zpf=a}else{if((window.io_rec_zp!==undefined)&&(typeof window.io_rec_zp=="function")){this.zpf="io_rec_zp"}else{this.zpf=undefined}}this.filter_pp=(((h.fp!==undefined)&&(h.fp===0))?0:1);this.filter_cp=(((h.fc!==undefined)&&(h.fc===0))?0:1);if(h.rp.length==1){if(g[h.rp[0][0]]===undefined){g[h.rp[0][0]]=new IORecPlan(h.rp[0][0],c,e)}this.rec_plan=g[h.rp[0][0]];this.n_recs=h.rp[0][3];this.ab_test_id="no ab test"}else{var f=IORequest.ab_group_number;this.rn=(f===undefined)?0:f;for(var d=0;((d<h.rp.length)&&(this.rec_plan===undefined));d++){if(this.rn>=h.rp[d][1]&&this.rn<=h.rp[d][2]){if(g[h.rp[d][0]]===undefined){g[h.rp[d][0]]=new IORecPlan(h.rp[d][0],c,e)}this.rec_plan=g[h.rp[d][0]];this.n_recs=h.rp[d][3];this.ab_test_id=((h.rp[d][4]!==undefined)?h.rp[d][4]:"no ab test")}}}}function IORecStep(a,b){this.offer_id=a[0];this.target_id=a[1];this.offer_type=this.offer_id?b[this.offer_id][1]:"N";this.offer_version=this.offer_id?b[this.offer_id][0]:0;this.heading=(a[3]!==undefined)?a[3]:"";this.algo_id=(a[4]!==undefined)?a[4]:"";this.algo_value=(a[5]!==undefined)?a[5]:"";this.to_string=function(){return("offer_id: "+this.offer_id+" target_id: "+this.target_id+" offer_type: "+this.offer_type+" offer_version: "+this.offer_version+" algo_id: "+this.algo_id+" algo_value: "+this.algo_value)}}function IORecPlan(b,a,c){this.rec_steps=[];this.id=b;for(var d=0;d<a[b].length;d++){this.rec_steps.push(new IORecStep(a[b][d],c))}}IOState.h_productview_product=[];IOState.h_pageview_page=[];function IOState(){var t=document;var e="undefined";var m=(IORequest.production?"~":"~");var k=":";var g=[];var y=[];var s=[];var B=[];var x=[];var o=[];var a=[];var z=[];var l=[];var p=[];var f=[];var v=[];var u=-1;var n=undefined;var r=undefined;var q=undefined;var A=undefined;var i=["p_viewed","p_carted","p_purchased","c_viewed","c_n_views","b_viewed","b_n_views"];var h=["pv","pc","pp","cv","cn","bv","bn"];var b=i;var w=false;var c=[];if(IORequest.basket_pages!==undefined){for(var d=0;d<IORequest.basket_pages.length;d++){c[IORequest.basket_pages[d]]=1}}this.cm_get_item_from_cookie=function(H,G){if(g.length!==0||(this.cm_build_all_recent_arrays()===true)){if((H=="_RVP_")||(H=="_RVL_")){if((G)||(H=="_RVL_")){return(a.length!=0?a:0)}else{return(IORequest.recently_viewed_product!=0?new Array(IORequest.recently_viewed_product):0)}}if(H=="_RVC_"){if(G){return(p.length!=0?p:0)}else{return(IORequest.recently_viewed_category!=0?new Array(IORequest.recently_viewed_category):0)}}if(H=="_LCP_"){if(G){return(z.length!=0?z:0)}else{return(z.length!=0?z.slice(0,1):0)}}if(H=="_RPP_"){if(G){return(l.length!=0?l:0)}else{return(l.length!=0?l.slice(0,1):0)}}if((H=="_RVG_")||(H=="_RVLG_")){if((G)||(H=="_RVLG_")){return(v.length!=0?v:0)}else{return(IORequest.recently_viewed_page!=0?new Array(IORequest.recently_viewed_page):0)}}if(H=="_MSP_"){var E=0;for(var F=1;F<l.length;F++){if(parseFloat(y[l[F]].pp_price)>parseFloat(y[l[E]].pp_price)){E=F}}return(l.length!=0?l.slice(E,E+1):0)}if(H=="_MPC_"){var E=0;for(var D=1;D<p.length;D++){if(parseInt(s[p[D]].n_viewed,10)>parseInt(s[p[E]].n_viewed,10)){E=D}}return(p.length!=0?p.slice(E,E+1):0)}if(H=="_MPB_"){var E=0;for(var C=1;C<f.length;C++){if(parseInt(B[f[C]].n_viewed,10)>parseInt(B[f[E]].n_viewed,10)){E=C}}return(f.length!=0?f.slice(E,E+1):0)}}return(0)};cm_initialize_id=function(C,D){C[D]=[];C[D].index=-1;C[D].n_bought=0;C[D].n_viewed=0;C[D].n_carted=0;C[D].pv_timestamp=0;C[D].pc_timestamp=0;C[D].pp_timestamp=0;C[D].pp_price=-1};cm_build_hash_from_array=function(E){var D=[];D.max_index=0;for(var C=0;C<E.length;C++){cm_initialize_id(D,E[C])}return D};cm_id_array_from_index_array=function(C,O,K,M,E,P){var D=[];D.max_length=O;if(C){var F=C.split("~");if(F.length==1){F=C.split(",")}for(var N=0;N<F.length;N++){var G=K[F[N]];D.push(G);if((E!==undefined)&&(P!==undefined)){for(var H=0;H<E.length;H++){var J=E[H];var L=P[H];if((J!==undefined)&&(L!==undefined)){var I=String(J).split("~");if(I.length==1){I=String(J).split(",")}if((!(M===undefined))&&(I.length>0)){M[G][L]=I[N]}}}}}if(D.length>D.max_length){D.length=D.max_length}}return D};cm_create_integer_array_from_id_array=function(G,D,C){var E=[];for(var F=0;F<G.length;F++){var H=G[F];if(D[H].index==-1){D[H].index=D.max_index++}E.push(D[H][C])}return E};cm_create_id_array_from_hash=function(C){var D=[];for(var E in C){if(typeof E!="function"){D[C[E].index]=E}}return D};cm_add_action=function(H,L,E,K,I,O,C){var D;var N=H;if(E){if(K){N=IORequest.encrypt8(H);IORequest.log(IORequest.log_trace,"encryption of "+H,N)}else{N=IORequest.encrypt16(H);IORequest.log(IORequest.log_trace,"encryption of "+H,N)}}if(N!==undefined){D=[N];D.max_length=I.max_length;if(L[N]===undefined){cm_initialize_id(L,N)}if(O!==undefined){for(var F=0;F<O.length;F++){var J=O[F];if(J!==undefined){if(C!==undefined){var G=C[F]}if(J.indexOf("n_viewed")>-1){L[N][J]++}else{if(J.indexOf("timestamp")>-1){L[N][J]=new Date().getTime()}else{if(G!==undefined){L[N][J]=G}}}}}}for(var M=0;M<I.length;M++){if(I[M]!=N){D.push(I[M])}}if(D.length>D.max_length){D.length=D.max_length}}else{D=I}return(D)};this.cm_write_cookies=function(N,T){var I=[cm_create_integer_array_from_id_array(a,y,"index").join("~"),cm_create_integer_array_from_id_array(z,y,"index").join("~"),cm_create_integer_array_from_id_array(l,y,"index").join("~"),cm_create_integer_array_from_id_array(p,s,"index").join("~"),cm_create_integer_array_from_id_array(p,s,"n_viewed").join("~"),cm_create_integer_array_from_id_array(f,B,"index").join("~"),cm_create_integer_array_from_id_array(f,B,"n_viewed").join("~")];if(w){for(var U=0;U<b.length;U++){I[U]=b[U]+k+I[U]}}var E=N.join("~");var M=cm_create_id_array_from_hash(y).join(m);var F=cm_create_id_array_from_hash(s).join(m);var O=cm_create_id_array_from_hash(B).join(m);var R=I.join(IORequest.cookie_array_separator);var D=cm_create_integer_array_from_id_array(a,y,"pv_timestamp").join("~");var K=cm_create_integer_array_from_id_array(z,y,"pc_timestamp").join("~");var J=cm_create_integer_array_from_id_array(l,y,"pp_timestamp").join("~");var L=cm_create_integer_array_from_id_array(l,y,"pp_price").join("~");var V="";if((IORequest.ssp_use_reg_id===undefined)){V=q}else{if(IORequest.ssp_use_reg_id==true){V=IORequest.ssp_reg_id}else{V=""}}var H=[E,M,F,O,R,D,K,J,L,n,r,V,A].join(IORequest.cookie_separator);var Q=IORequest.set_and_check_cookie(IORequest.state_cookie,H,false,IORequest.vanity_suffix);var S=T.join("~");var W=[cm_create_integer_array_from_id_array(v,x,"index").join("~")];var X=W.join(IORequest.cookie_array_separator);var C=cm_create_id_array_from_hash(x).join(m);var G=[S,C,X].join(IORequest.cookie_separator);var P=IORequest.set_and_check_cookie(IORequest.state_cookie_content,G,false,IORequest.vanity_suffix);return};this.set_ab_test_group_from_cookie=function(){var D=[];var C=IORequest.find_state_cookie(IORequest.state_cookie);if(C!==undefined){D=IORequest.build_array_from_cookie(IORequest.state_cookie,0).split(",");if(D.length>0){IORequest.ab_group_number=D[0];if(IORequest.ab_group_number.length>3){D=IORequest.build_array_from_cookie(IORequest.state_cookie,0).split("~");IORequest.ab_group_number=D[0]}}}return};this.get_ssp_load_ts_from_cookie=function(){var D="";var C=IORequest.find_state_cookie(IORequest.state_cookie);if(C!==undefined){var E=(C===undefined)?4:(C.split(IORequest.cookie_separator).length-1);if(E>4){D=IORequest.build_array_from_cookie(IORequest.state_cookie,10).split(IORequest.cookie_array_separator)}}return D};this.get_ssp_reg_id_from_cookie=function(){var D="";var C=IORequest.find_state_cookie(IORequest.state_cookie);if(C!==undefined){var E=(C===undefined)?4:(C.split(IORequest.cookie_separator).length-1);if(E>4){D=IORequest.build_array_from_cookie(IORequest.state_cookie,11).split(IORequest.cookie_array_separator)}}return D};this.get_pf_segment_from_cookie=function(){var D="";var C=IORequest.find_state_cookie(IORequest.state_cookie);if(C!==undefined){var E=(C===undefined)?4:(C.split(IORequest.cookie_separator).length-1);if(E>4){D=IORequest.build_array_from_cookie(IORequest.state_cookie,12).split(IORequest.cookie_array_separator)}}return D};this.cm_build_all_recent_arrays=function(){var D=[];var K=[];var M=[];var G=[];var C=false;var Q=IORequest.find_state_cookie(IORequest.state_cookie);if(IORequest.setSegmentCalled){A=IORequest.pf_segment}else{A=_io_state.get_pf_segment_from_cookie()}if(Q!==undefined){var O=(Q===undefined)?4:(Q.split(IORequest.cookie_separator).length-1);g=IORequest.build_array_from_cookie(IORequest.state_cookie,0).split(",");if(g.length>0){IORequest.have_cookie=true;IORequest.ab_group_number=g[0];if(IORequest.ab_group_number.length>3){g=IORequest.build_array_from_cookie(IORequest.state_cookie,0).split("~");IORequest.ab_group_number=g[0]}if(g.length>1){IOConfig.version=g[1];IOConfig.brand_personalization[0]=g[2];IOConfig.brand_personalization[1]=g[3];IOConfig.category_structure=g[4];IORequest.a_max_elements[0]=g[5];IORequest.a_max_elements[1]=g[6];IORequest.a_max_elements[2]=g[7];IORequest.a_max_elements[3]=g[8];IORequest.a_max_elements[4]=g[9];IORequest.a_max_elements[5]=g[10];IORequest.a_max_elements[6]=g[11]}}D=IORequest.build_array_from_cookie(IORequest.state_cookie,1).split(m);y=cm_build_hash_from_array(D);K=IORequest.build_array_from_cookie(IORequest.state_cookie,2).split(m);s=cm_build_hash_from_array(K);if(O>3){M=IORequest.build_array_from_cookie(IORequest.state_cookie,3).split(m);B=cm_build_hash_from_array(M)}var I=IORequest.build_array_from_cookie(IORequest.state_cookie,O<4?O:4).split(IORequest.cookie_array_separator);if(w&&(g_b_a_arrays[0].substring(0,2)==b[0].substring(0,2))){for(var P=0;P<I.length;P++){I[P]=I[P].substring(b[P].length+1)}}if(O>4){var H=IORequest.build_array_from_cookie(IORequest.state_cookie,5).split(IORequest.cookie_array_separator);var F=IORequest.build_array_from_cookie(IORequest.state_cookie,6).split(IORequest.cookie_array_separator);var N=IORequest.build_array_from_cookie(IORequest.state_cookie,7).split(IORequest.cookie_array_separator);var J=IORequest.build_array_from_cookie(IORequest.state_cookie,8).split(IORequest.cookie_array_separator);n=IORequest.build_array_from_cookie(IORequest.state_cookie,9).split(IORequest.cookie_array_separator);r=IORequest.build_array_from_cookie(IORequest.state_cookie,10).split(IORequest.cookie_array_separator);q=IORequest.build_array_from_cookie(IORequest.state_cookie,11).split(IORequest.cookie_array_separator);if(A===undefined){A=IORequest.build_array_from_cookie(IORequest.state_cookie,12).split(IORequest.cookie_array_separator)}}a=cm_id_array_from_index_array(I[0],IORequest.a_max_elements[0],D,y,new Array(H),new Array("pv_timestamp"));z=cm_id_array_from_index_array(I[1],IORequest.a_max_elements[1],D,y,new Array(F),new Array("pc_timestamp"));l=cm_id_array_from_index_array(I[2],IORequest.a_max_elements[2],D,y,new Array(N,J),new Array("pp_timestamp","pp_price"));p=cm_id_array_from_index_array(I[3],IORequest.a_max_elements[3],K,s,new Array(I[4]),new Array("n_viewed"));if(O>3){f=cm_id_array_from_index_array(I[5],IORequest.a_max_elements[5],M,B,new Array(I[6]),new Array("n_viewed"))}if(IORequest.recently_viewed_product===undefined){IORequest.recently_viewed_product=(a.length===0?0:a[0])}if(IORequest.recently_viewed_category===undefined){IORequest.recently_viewed_category=(p.length===0?0:p[0])}C=true}Q=IORequest.find_state_cookie(IORequest.state_cookie_content);if(Q!==undefined){o=IORequest.build_array_from_cookie(IORequest.state_cookie_content,0).split(",");if(o.length>0){IORequest.a_max_page_elements[0]=o[0]}G=IORequest.build_array_from_cookie(IORequest.state_cookie_content,1).split(m);x=cm_build_hash_from_array(G);var I=IORequest.build_array_from_cookie(IORequest.state_cookie_content,2).split(IORequest.cookie_array_separator);v=cm_id_array_from_index_array(I[0],IORequest.a_max_page_elements[0],G);if(IORequest.recently_viewed_page===undefined){IORequest.recently_viewed_page=(v.length===0?0:v[0])}C=true}if(g.length==1){IORequest.rm_cookie(IORequest.state_cookie);var L=[IORequest.ab_group_number,IOConfig.version,IOConfig.brand_personalization[0],IOConfig.brand_personalization[1],IOConfig.category_structure,IORequest.a_max_elements[0],IORequest.a_max_elements[1],IORequest.a_max_elements[2],IORequest.a_max_elements[3],IORequest.a_max_elements[4],IORequest.a_max_elements[5],IORequest.a_max_elements[6]];var E=[IORequest.a_max_page_elements[0]];this.cm_write_cookies(L,E)}return C};cm_build_html_table_from_array=function(H,G,J,C){var I=(C?2:1);var E=G.length;var F="";var D=(I==1?"<TD COLSPAN=2>":"<TD>");if(E>0&&(G[0]!==undefined)){F="<TR><TH ROWSPAN="+E+">"+H+"</TH>"+D+(I==2?J[G[0]][C]+"</TD><TD>":"")+G[0]+"</TD></TR>";for(var K=1;K<E;K++){F+="<TR>"+D+(I==2?J[G[K]][C]+"</TD><TD>":"")+G[K]+"</TD></TR>"}}else{F="<TR><TH ROWSPAN=1>"+H+"</TH><TD COLSPAN=2>No "+H+"</TD></TR>"}return(F)};cm_get_products_in_cart=function(){if(this.cm_build_all_recent_arrays()===true){return(z)}else{return([])}};this.cm_format_cookie_arrays=function(C){return("<H3>Obsolete</H3>")};this.cm_ted_io=function(G){var D=false;if(this.cm_build_all_recent_arrays()===true){if(G.i_offer!==undefined){if(G.i_offer=="epr_category"){if(_io_config.fcpl=="Y"){G.cg=G.cg.replace(/>.*$/,"");G.cg=G.cg.replace(/\s+$/,"")}if(G.cg!==undefined){if(!IORequest.encrypt_cats&&(G.cg.length>IORequest.max_cat_length)){IORequest.log(IORequest.log_warn,"EPR Category not added to cookie.  Category length is greater than the maximum of "+IORequest.max_cat_length+". Category",G.cg)}else{IORequest.log(IORequest.log_trace,"Adding EPR Category to cookie.  Category",G.cg);p=cm_add_action(G.cg,s,IORequest.encrypt_cats,true,p,new Array("n_viewed"));D=true}}}if(G.i_offer=="brand"){IORequest.log(IORequest.log_trace,"Adding brand to cookie. Brand",G.brn);f=cm_add_action(G.brn,B,1,true,f,new Array("n_viewed"));D=true}}else{if(G.tid==1||G.tid==4||G.tid==5){IORequest.log(IORequest.log_cookie_write,"initial "+IORequest.state_cookie,IORequest.find_state_cookie(IORequest.state_cookie));IORequest.log(IORequest.log_cookie_write,"initial "+IORequest.state_cookie_content,IORequest.find_state_cookie(IORequest.state_cookie_content))}if(1==G.tid){var E=""+G.pi.toString().toUpperCase();if(E!==undefined){IORequest.log(IORequest.log_trace,"Adding page ID from page view to cookie.  Page ID",E);v=cm_add_action(E,x,true,false,v);D=true}IOState.h_pageview_page[E]=1}if(5==G.tid){var C=""+G.pr.toString().toUpperCase();var F=""+G.cg.toString().toUpperCase();if(C!==undefined){if(!IORequest.encrypt_prds&&(C.length>IORequest.max_prd_length)){IORequest.log(IORequest.log_warn,"Product from product view not added to cookie.  Product length is greater than the maximum of "+IORequest.max_prd_length+". Product",C)}else{IORequest.log(IORequest.log_trace,"Adding product from product view to cookie.  Product",C);a=cm_add_action(C,y,IORequest.encrypt_prds,true,a,new Array("pv_timestamp"));D=true}}if((F!==undefined)&&(IOConfig.category_structure=="S")){if(!IORequest.encrypt_cats&&(F.length>IORequest.max_cat_length)){IORequest.log(IORequest.log_warn,"Site Category from product view not added to cookie.  Category length is greater than the maximum of "+IORequest.max_cat_length+". Category",F)}else{IORequest.log(IORequest.log_trace,"Adding Site Category from product view to cookie.  Category",F);p=cm_add_action(F,s,IORequest.encrypt_cats,true,p,new Array("n_viewed"));D=true}}IOState.h_productview_product[C]=1}if((G.pr!==undefined)&&(4==G.tid)&&(5==G.at)){if(!IORequest.encrypt_prds&&(G.pr.length>IORequest.max_prd_length)){IORequest.log(IORequest.log_warn,"Product from cart contents not added to cookie.  Product length is greater than the maximum of "+IORequest.max_prd_length+". Product",G.pr)}else{IORequest.log(IORequest.log_trace,"Adding product from cart contents to cookie.  Product",G.pr);z=cm_add_action(G.pr.toString().toUpperCase(),y,IORequest.encrypt_prds,true,z,new Array("pc_timestamp"));D=true}}if((G.pr!==undefined)&&(4==G.tid)&&(9==G.at)){if(!IORequest.encrypt_prds&&(G.pr.length>IORequest.max_prd_length)){IORequest.log(IORequest.log_warn,"Product from purchase not added to cookie.  Product length is greater than the maximum of "+IORequest.max_prd_length+". Product",G.pr)}else{IORequest.log(IORequest.log_trace,"Adding product from purchase to cookie.  Product",G.pr);l=cm_add_action(G.pr.toString().toUpperCase(),y,IORequest.encrypt_prds,true,l,new Array("pp_timestamp","pp_price"),new Array(null,G.bp));D=true}}}if(D){this.cm_write_cookies(g,o)}}}}cmLoadIOConfig();function cmExecuteTagQueue(){var b=window.cmTagQueue;if(b){var c=(b.constructor==Array);if(!c){return}for(var a=0;a<b.length;++a){window[b[a][0]].apply(window,b[a].slice(1))}}return true}cmExecuteTagQueue();

/* ../global/lib/cmdatatagutils.js */

/* DO NOT LINT */
/*
 * cmdatatagutils.js for IBM Websphere Commerce
 * $Id: cmdatatagutils-MASTER.txt 166638 2011-03-15 17:31:18Z whbird $
 * $Revision: 166638 $
 *
 * Version 4.2.0
 *
 * Coremetrics Tag v4.0, 8/7/2006
 * COPYRIGHT 1999-2002 COREMETRICS, INC.
 * ALL RIGHTS RESERVED. U.S.PATENT PENDING
 *
 * The following functions aid in the creation of Coremetrics data tags.
 * 12/11/08		WBIRD	-eluminate has cm_DelayHandlerReg="FL"
 * 12/30/09		WBIRD	-update to 90226018; add io_v4 initialization + new io_v4.js
 * 01/20/10		WBIRD	-normalization optimizations
 * 03/25/10		WBIRD	-removed rnd order# and rnd custID generation per 10015054
 * 06/04/10		WBIRD	-corrected "cmCheckCMEMFlag = false;" placement
 * 07/14/10		WBIRD	-unconditionally set __cg to "" as emergency fix for 10024478
 * 02/28/11		HWHITE	-remove impression tagging for e-spot campaigns
 * 03/15/11		WBIRD	-remove remaining impressions (RS) per 10067989
 */

// Set Cookie to disable calls to metrics server www7
document.cookie = "CMDisabled=Y; expires=Fri, 31 Dec 9999 23:59:59 GMT; path=/";

var cm_exAttr=new Array;

//var cm_ClientID = "90226018";
// Added line below to auto check and use correct Prod/Test ID's

// All Lowe's URL's
var low_ids = [
	['60226024', ['localhost:10040/wps/myportal','devinwww.myloweslife.com/wps/portal','webentportaldev1.lowes.com:12000/wps/portal','webentportaldev2.lowes.com:12000/wps/portal','webentportaldev1:16000/wps/myportal','webentportaldev1:16030/wps/myportal','webentportaldev2:16000/wps/myportal','webentportaldev2:16030/wps/myportal','webengres2:16000/wps/myportal','intinwww.myloweslife.com/wps/portal','wasportaltest1:12000/wps/myportal','wasportaltest2:12000/wps/myportal','wasportaltest1:16000/wps/myportal','wasportaltest2:16000/wps/myportal','http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/qawww.myloweslife.com','ihsentportalqa2a:12030/wps/portal','ihsentportalqa3a:12030/wps/portal','wasentportalqa1a:16000/wps/myportal','wasentportalqa1a:16030/wps/myportal','wasentportalqa2a:16000/wps/myportal','wasentportalqa2a:16030/wps/myportal','wasentportalqa3a:16000/wps/myportal','wasentportalqa3a:16030/wps/myportal','wasentportalqa4a:16000/wps/myportal','wasentportalqa4a:16030/wps/myportal','ihsentportalqa2b:12030/wps/myportal','ihsentportalqa3b:12030/wps/myportal','wasentportalqa1b:17000/wps/myportal','wasentportalqa1b:17030/wps/myportal','wasentportalqa2b:17000/wps/myportal','wasentportalqa2b:17030/wps/myportal','wasentportalqa3b:17000/wps/myportal','wasentportalqa3b:17030/wps/myportal','wasentportalqa4b:17000/wps/myportal','wasentportalqa4b:17030/wps/myportal']],
	['90226024', ['http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/www.myloweslife.com','http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/stage.myloweslife.com','http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/inwww.myloweslife.com','http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/exwww.myloweslife.com','ihsportalprod2:12030/wps/portal','ihsportalprod3:12030/wps/portal','wasentportalprod1a:16000/wps/portal','wasentportalprod1a:16030/wps/portal','wasentportalprod2a:16000/wps/portal','wasentportalprod2a:16030/wps/portal','wasentportalprod3a:16000/wps/portal','wasentportalprod3a:16030/wps/portal','wasentportalprod4a:16000/wps/portal','wasentportalprod4a:16030/wps/portal','wasentportalprod5a:16000/wps/portal','wasentportalprod5a:16030/wps/portal','wasentportalprod6a:16000/wps/portal','wasentportalprod6a:16030/wps/portal','ihsportalprod2b:12030/wps/portal','ihsportalprod3b:12030/wps/portal','wasentportalprod1b:17000/wps/portal','wasentportalprod1b:17030/wps/portal','wasentportalprod2b:17000/wps/portal','wasentportalprod2b:17030/wps/portal','wasentportalprod3b:17000/wps/portal','wasentportalprod3b:17030/wps/portal','wasentportalprod4b:17000/wps/portal','wasentportalprod4b:17030/wps/portal','wasentportalprod5b:17000/wps/portal','wasentportalprod5b:17030/wps/portal','wasentportalprod6b:17000/wps/portal','wasentportalprod6b:17030/wps/portal']],
	['90226018', ['http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/www.lowes.com','http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/es.lowes.com','http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/easy2.com']]
];

// Run through them and create the ID and the Array of URL's
for ( var i=0; i<low_ids.length; i++ ) {
	var cm_temp_id = low_ids[i][0],
		cm_url_arr = low_ids[i][1];
		cm_ClientID = '90226021';
	for ( var j=0; j<cm_url_arr.length; j++ ) {
		// If the current URL matches a URL in the list, set the ID to that list or use the default test URL.
		if ( window.location.href.indexOf(cm_url_arr[j]) > 0 ) {
			cm_ClientID = cm_temp_id;
		}
	}
}
/* So it is at least defined until we can clean up usage. */
cG7.cM0[cm_ClientID]='';
//console.debug(cm_ClientID);

var cm_TrackLink = "A";
var cm_TrackImpressions = ""; // Former Value: RSCM
var cm_JSFEnabled = false;

// lowes-requested rnd order# and custID - cI=read, CB=set, CC=clear

// code to determine javascript version

var cmJv = "1.0";

if (typeof(isNaN) == "function") cmJv = "1.1";
if (typeof(isFinite) == "function") cmJv = "1.2";
if (typeof(NaN) == "number") cmJv = "1.3";
if (typeof(decodeURI) == "function") cmJv = "1.5";
if (typeof(Array.forEach) == "function") cmJv = "1.6";
if (typeof(Iterator) == "object") cmJv = "1.7";

var cmCheckCMEMFlag = true;
var cmSendOSLinkClickTag = true;



/*
 * IO V4 Configuration
 *
 * The following variables are global to io_v4.js
 */

if (typeof(IORequest) == "function") {
	IORequest.client_id = cm_ClientID; 		// client id
	IORequest.encrypt_cats = true; 			// stores categories as 8 digit hex numbers (use if site has long category names)
	IORequest.encrypt_prds = true; 			// stores products as 8 digit hex numbers (use if site has long product names)
	IORequest.conflict_resolution = true; 	// for pages with multiple zones - a recommendation appearing in zone 1 will be filtered out from zone 2
	IORequest.max_prd_length = 25; 			// products with lengths longer than this number will be ignored
	IORequest.max_cat_length = 25; 			// categories with lengths longer than this number will be ignored
	IORequest.timeout = [8000, 4000]; 		// timeout for download of product file [first, subsequent] (ms)
	IORequest.use_site_category = false; 	// a site category is the one passed on cmCreate[Page|Product]Tag
											// a non site category is passed in the product files
	if ((IORequest.ie_version() !== null) && (IORequest.ie_version() < 7.0)) {
	  IORequest.a_max_elements = [3,3,5,3,3,3,3];
	} else {
	  IORequest.a_max_elements = [3,3,5,3,3,7,7];
	}
	IORequest.required_attributes = [0,0,0,0,0];
	IORequest.access_method = 'json remote';
	IORequest.default_product_file = undefined;
}

function cmSetProduction() {
	cm_HOST="www7.lowes.com/eluminate?";
//	cm_JSFPCookieDomain = "http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/somedomain.com";
}

function cmCreateConversionEventTag(eventID, actionType, categoryID, points,attributes) {
	if (attributes) {
		var cm_exAttr=new Array;
		cm_exAttr=attributes.split("-_-");
	}
	cmMakeTag(["tid","14","cid",eventID,"cat",actionType,"ccid",categoryID,"cpt",points,"cm_exAttr",cm_exAttr]);
}

function cmCreatePageviewTag(__pi,__cg,__se,__sr, store_id, attributes) {
	if (attributes) {
		var cm_exAttr=new Array;
		cm_exAttr=attributes.split("-_-");
	}
	cmMakeTag(["tid","1","pi",__pi,"cg",__cg,"se",__se,"sr",__sr,"pc","Y","pv11",store_id,"cm_exAttr",cm_exAttr]);
}

function cmCreateDefaultPageviewTag(__cg) {
	cmCreatePageviewTag(cmGetDefaultPageID(),__cg);
}

function cmCreateProductviewTag(__pi,__pr,__pm,__cg,store_id,pageCount,catIDoverride, attributes, __cmvc,__se,__sr) {
	if (catIDoverride) {
		if (catIDoverride != "0") { // allows client to override WSC auto-catID value if catIDoverride is not null or "0"
			__cg = catIDoverride;
		}
	}

	if ((pageCount == null) || pageCount == "") {
		pageCount = 'Y';
	}

	if ((__pi == null) || (__pi == "") || (pageCount == "N")) {
		__pi = cmPageviewData.pageName;
	}

	if (attributes){   // send attributes as 2nd JSP extraparms= value
		var cm_exAttr=new Array,
			prodUrl;
		cm_exAttr=attributes.split("-_-");
		/*
		 * Because the Product Detail page is cached, back-end was unable to push
		 * these values in cmCreateProductviewTag(). The N & NE value will be
		 * positioned in attribute 16 & 17.
		 */
		try {
			prodUrl = document.location.href.split('pd_')[1];
			prodUrl = prodUrl.split('?')[0];
			prodUrl = prodUrl.split('_');
			cm_exAttr.splice(16, 0, prodUrl[1], prodUrl[2])
		}
		catch(err) {

		}
	}

	if (__cmvc == null) {   // send __cmvc as 3rd JSP extraparms= value
		__cmvc = cmExtractParameter("cm_vc",document.location.href)
	}

	cmMakeTag(["tid","5","pi",__pi,"pr",__pr,"pm",__pm,"cg",__cg,"pc",pageCount,"pv11",store_id,"cm_vc",__cmvc,"se",__se,"sr",__sr,"cm_exAttr",cm_exAttr]);
}

var __sArray = new Array();
var __skuString = "";
var __ex=new Array();

function __cmGetPI(__id){
	var __pI;

	for (__pI = 0; __pI < __sArray.length; ++__pI) {
		if (__id == __sArray[__pI][1]) return __pI;
	}

	return -1;
}

function __cmGetPIPC(__pr,__cg) {
	var __pI, i;
	var cmAttr1=new Array();
	var cmAttr2=new Array();

	for (i=0;i<__ex.length;++i){
		cmAttr1=cmAttr1+__ex[i];
	}

	for (__pI = 0; __pI < __sArray.length; ++__pI) {
		if (__ex.length>0){
			cmAttr2=new Array();

			for (i=__sArray[__pI].length-__ex.length*2+1;i<__sArray[__pI].length;i=i+2){
				cmAttr2=cmAttr2+__sArray[__pI][i];
			}

			if (__pr == __sArray[__pI][1] && __cg == __sArray[__pI][9] && cmAttr1==cmAttr2){
				return __pI;
			}
		} else {
			if (__pr == __sArray[__pI][1] && __cg == __sArray[__pI][9]) return __pI;
		}
	}

	return -1;
}

function cmAddShop(__v) {

	var __i = __cmGetPIPC(__v[1],__v[9]);
	if (__i == -1) {
		if (__ex.length>0){
			for (var i=0; i<__ex.length; ++i){
				__v[__v.length]="s_a"+(i+1);
				__v[__v.length]=__ex[i];
			}
		}
		__sArray[__sArray.length] = __v;
	} else {
		var __oQ = __sArray[__i][5];
		var __oP = __sArray[__i][7];

		__sArray[__i][5] = parseInt(__sArray[__i][5]) + parseInt(__v[5]);
		__sArray[__i][7] = (((__v[7]*__v[5])+(__oP*__oQ))/__sArray[__i][5]);
	}
}

function cmCreateShopAction5Tag(__pr,__pm,__qt,__bp,__cg, store_id, currency, catIDoverride,attributes) {
	if (catIDoverride)	{
		if (catIDoverride != "0") {				// allows client to override WSC auto-catID value if catIDoverride is not null or "0"
			__cg = catIDoverride;
		}
	}

	if (attributes){
		__ex=attributes.split("-_-");
	} else {
		__ex=new Array();
	}

	cmAddShop(["pr",__pr,"pm",__pm,"qt",__qt,"bp",__bp,"cg",__cg,"ha1",attributes ? cm_hex_sha1(attributes) : null,"at","5","tid","4","pc","N","sx11",store_id,"cc",currency]);
}

function cmCreateShopAction9Tag(__pr,__pm,__qt,__bp,__cd,__on,__tr,__cg, store_id, currency, account_name, contract_name, catIDoverride,attributes) {
	if (catIDoverride)	{
		if (catIDoverride != "0") {				// allows client to override WSC auto-catID value if catIDoverride is not null or "0"
			__cg = catIDoverride;
		}
	}

	__cg = "";		// emergency fix for 10024478

	if (attributes) {
		__ex=attributes.split("-_-");
	} else {
		__ex=new Array();
	}

	cmAddShop(["pr",__pr,"pm",__pm,"qt",__qt,"bp",__bp,"cg",__cg,"ha1",attributes ? cm_hex_sha1(attributes) : null,"cd",__cd,"on",__on,"tr",__tr,"at","9","tid","4","pc","N","sx11",store_id,"cc",currency,"sx13",account_name,"sx14",contract_name]);
	cmCalcSKUString();
}

function cmDisplayShop5s() {
	cmDisplayShops();
}

function cmDisplayShop9s() {
	cmCalcSKUString();
	cmDisplayShops();
}

function cmCalcSKUString() {
	__skuString = "";

	for (i = 0; i < __sArray.length; ++i) {
		__skuString += "|"+__sArray[i][1]+"|"+__sArray[i][7]+"|"+__sArray[i][5]+"|";
	}
}

function cmDisplayShops() {
	var i;
	for (i = 0; i < __sArray.length; ++i) {
		cmMakeTag(__sArray[i]);
	}

	__sArray = new Array();
}

function cmCreateOrderTag(__on,__tr,__sg,__cd,__ct,__sa,__zp, store_id, currency, promotion_name, promotion_discount, promotion_code,attributes) {
	if (((promotion_code == null) || (promotion_code == "")) && promotion_name) { promotion_code = "No Code"; }

	if (attributes){
		var cm_exAttr=new Array;
		cm_exAttr=attributes.split("-_-");
	}

	cmMakeTag(["tid","3","osk",__skuString,"on",__on,"tr",__tr,"sg",__sg,"cd",__cd,"ct",__ct,"sa",__sa,"zp",__zp,"or11",store_id,"cc",currency,"or13",promotion_name,"or14",promotion_discount,"or15",promotion_code,"cm_exAttr",cm_exAttr]);
}

function cmCreateRegistrationTag(__cd,__em,__ct,__sa,__zp,__nl,__sd, store_id, customer_country, age, gender, marital_status, num_children, num_in_household, company_name, hobbies, income,
	promotionCode, myStoreOrg, myStoreregion, myStoreID, myStoreZIP, myStorelocality) {

	if (promotionCode) {
		marital_status = promotionCode;
	} else {
		marital_status = null;
	}

	if (myStoreOrg) {
		num_children = myStoreOrg;
	} else {
		num_children = null;
	}

	if (myStoreregion) {
		num_in_household = myStoreregion;
	} else {
		num_in_household = null;
	}

	if (myStoreID) {
		company_name = myStoreID;
	} else {
		company_name = null;
	}

	if (myStoreZIP) {
		hobbies = myStoreZIP;
	} else {
		hobbies = null;
	}

	cmMakeTag(["tid","2","cd",__cd,"em",__em,"ct",__ct,"sa",__sa,"zp",__zp,"nl",__nl,"sd",__sd,"rg1",store_id,"cy",customer_country,"ag",age,"gd",gender,
		"rg11",marital_status,"rg12",num_children,"rg13",num_in_household,"rg14",company_name,"rg15",hobbies,"ml",income,"rg1",myStorelocality]);
}

function cmAttrCreator(paramObj) {
	var arr = new Array();
	$.each(paramObj, function(i, value){
		arr[parseInt(i) - 1] = value.toString();
	});
	return arr.join('-_-');
}

function cmCreateErrorTag(__pi,__cg, store_id,attributes) {
	if (attributes){
		var cm_exAttr=new Array;
		cm_exAttr=attributes.split("-_-");
	}

	cmMakeTag(["tid","404","pi",__pi,"cg",__cg,"pc","Y","pv1",store_id,"cm_exAttr",cm_exAttr]);
}

function cmGetDefaultPageID () {
	var __p = window.location.pathname;
	var __t1 = __p.indexOf("?");

	if (__t1 != -1) __p = __p.substr(0, __t1);

	var __t2 = __p.indexOf("#");

	if (__t2 != -1) __p = __p.substr(0, __t2);

	var __t3 = __p.indexOf(";");

	if (__t3 != -1) __p = __p.substr(0, __t3);

	var __sp = __p.lastIndexOf("/");

	if (__sp == __p.length - 1) {
		__p = __p + "default.asp"; /* SET TO DEFAULT DOC NAME */
	}

	while (__p.indexOf("/") == 0) {
		__p = __p.substr(1,__p.length);
	}

	return(__p);
}

function cmMakeTag(__v) {
	// ensure this function doesn't run
	return false;

	var cm = new _cm("vn2", "e4.0");
	var i;
	for (i = 0; i < __v.length; i += 2) {
		var _n = __v[i];
		var _v = __v[i + 1];
		cm[_n] = _v;
	}

	var datestamp = new Date();
	var stamp = (Math.floor(Math.random() * 11111111)) + datestamp.valueOf();
	cm.rnd = stamp;

	if (cm.tid == "6") {
		cm.addTP();
		document.cookie = "cmTPSet=Y; path=/";
	}

	if (cm.tid == "1") {
		if (cI("cmTPSet") != 'Y') {
			cm.tid = "6";
			cm.pc = 'Y';
			cm.addTP();
			document.cookie = "cmTPSet=Y; path=/";
		}
	}

	if (cm.tid != "4" && typeof(cm.cm_exAttr)!="undefined"){
		switch(cm.tid){
			case "6":
				prefix="pv";
				break;

			case "1":
				prefix="pv";
				break;

			case "2":
				prefix="rg";
				break;

			case "5":
				prefix="pr";
				break;

			case "3":
				prefix="o";
				break;

			case "14":
				prefix="c";
				break;

			case "15":
				prefix="e";
				break;

			default:
				break;
		}

		var attrNum=cm.cm_exAttr.length;

		/* Removed so we could add more than 15 items to the cmAttr
		if (attrNum>15){ attrNum=15; }
		*/

		for (i=0;i<attrNum;i++){
			if (cm.tid == "2"){
				Attval=prefix+(i+1);
			} else {
				Attval=prefix+"_a"+(i+1);
			}

			cm[Attval]=cm.cm_exAttr[i];
		}

		cm.cm_exAttr=null;
	}

	if ((cm.pi == null) && (cm.pc == "Y")) {
		cm.pi = cmGetDefaultPageID();
	}

	try{
		if (parent.cm_ref != null) {
			cm.rf = parent.cm_ref;

			if (cm.pc == "Y") {
				parent.cm_ref = document.URL;
			}
		}

		// if parent had mmc variables and this is the first pageview, add mmc to this url
		if(parent.cm_set_mmc) {
			cm.ul = document.location.href + ((document.location.href.indexOf("?") < 0) ? "?" : "&") + parent.cm_mmc_params;

			if (cm.pc == "Y") {
				parent.cm_ref = cm.ul;
				parent.cm_set_mmc = false;
			}
		}
	}

	catch(err){}

	if (!(cm.ul)) {
		cm.ul = document.location.href;
	}

	if (!(cm.rf)) {
		cm.rf = document.referrer;
	}

	cm.ul = cmRemoveParameter("krypto",cm.ul);
	cm.rf = cmRemoveParameter("krypto",cm.rf);
	cm.ul = cmRemoveParameter("firstReferURL",cm.ul);
	cm.rf = cmRemoveParameter("firstReferURL",cm.rf);
	cm.ul = cmRemoveParameter("selectedLocalStoreBeanArray",cm.ul);
	cm.rf = cmRemoveParameter("selectedLocalStoreBeanArray",cm.rf);
	cm.ul = cmRemoveParameter("parentProductCacheKey",cm.ul);
	cm.rf = cmRemoveParameter("parentProductCacheKey",cm.rf);

	if (cmCheckCMEMFlag){cmStartTagSet();}
	cm.writeImg();
	if (cmCheckCMEMFlag) {
		cmCheckCMEMFlag = false;
		cmCheckCMEM();
		cmSendTagSet();
	}

	//check to see if we fire link click for email campaign (cm_cr)
	if (cmSendOSLinkClickTag) {
		if ((window.location.href.indexOf("cm_cr=OS:") > -1) || (window.location.href.indexOf("cm_cr=OS%3A") > -1) || (window.location.href.indexOf("cm_cr=OS%3a") > -1)) {
			var tempHref = window.location.href;
			tempHref = tempHref.split("cm_cr=OS:").join("cm_cr=");
			tempHref = tempHref.split("cm_cr=OS%3A").join("cm_cr=");
			tempHref = tempHref.split("cm_cr=OS%3a").join("cm_cr=");
			tempHref = tempHref.split("-_-E-mail%20Activity-_-").join("-_-1-_-");
			tempHref = tempHref.split("-_-E-mail%2BActivity-_-").join("-_-1-_-");
			tempHref = tempHref.split("-_-E-mail%2bActivity-_-").join("-_-1-_-");
			cmCreateManualLinkClickTag(tempHref,null,"Email");
			cmSendOSLinkClickTag = false;
		}
	}

	// Intelligent Offer call
	if (typeof cm_ted_io == 'function') {
		cm_ted_io(cm);
	}
}

if (defaultNormalize == null) { var defaultNormalize = null; }

function myNormalizeURL(url, isHref) {
	if (cmIndexOfParameter("#",url) != -1) {
		newURL = (cmRemoveParameter ("cm_cr",url));
	} else {
		var newURL = url;
	}

	var pageURL=document.URL;

	if (isHref) {
		var blackList = ["krypto=","storeref=","site=","Ntt=","newSearch=","Ns="];

		/* if (cG7.cM0[cm_ClientID].indexOf("SEARCH SUCCESSFUL:")>-1) {  // search results pages
			if (newURL.toLowerCase().indexOf("/pd")>0) {  // truncate collected product link HREF after '/pd'
				newURL = "/searchresult_productclick";
			}
		}*/
		var paramString;
		var paramIndex = newURL.indexOf("?");
		var params;
		var keepParams = new Array();
		var goodParam;

		if (paramIndex > 0) {
			paramString = newURL.substring(paramIndex+1);
			newURL = newURL.substring(0, paramIndex);
			params = paramString.split("&");

			for(var i=0; i<params.length; i++) {
				goodParam = true;
				for(var j=0; j<blackList.length; j++) {

				//This match is case insensitive.  Remove .toLowerCase() to add case sensitivity
					if (params[i].toLowerCase().indexOf(blackList[j].toLowerCase()) == 0) {
						goodParam = false;
					}
				}

				if(goodParam == true) {
					keepParams[keepParams.length] = params[i];
				}
			}

			newURL += "?" + keepParams.join("&");
		}

		if (defaultNormalize != null) {
			newURL = defaultNormalize(newURL, isHref);
		}
	}
	return newURL;
}

// install normalization
if (document.cmTagCtl != null) {
	var func = "" + document.cmTagCtl.normalizeURL;

	if (func.indexOf('myNormalizeURL') == -1) {
		defaultNormalize = document.cmTagCtl.normalizeURL;
		document.cmTagCtl.normalizeURL = myNormalizeURL;
	}
}

function cmIndexOfParameter (parameter, inString) {
	return inString.indexOf(parameter);
}

function cmExtractParameter (parameter, inString) {
	if (cmIndexOfParameter(parameter, inString) == -1) {
		return null;
	}

	var s = inString;
	var begin = s.indexOf(parameter);
	var end = s.indexOf("&", begin);

	if (end == -1) {
		end = s.length;
	}

	var middle = s.indexOf("=", begin);

	return s.substring(middle + 1, end);
}

function cmRemoveParameter (parameter, inString) {
	if (cmIndexOfParameter(parameter, inString) == -1) {
		return inString;
	}

	var s = inString;
	var begin = s.indexOf(parameter);
	var start = (begin - 1);
	var end = s.indexOf("&", begin);

	if (end == -1) {
		end = s.length;
	}

	if (s.substring(start, begin) == "?") {    // retain leading "?"
		start = (start + 1);
		end = (end + 1);
	}

	return s.substring(0, start) + s.substring(end, s.length);
}

function cmCheckCMEM() {
	if (cmIndexOfParameter("cm_em",document.location.href) != -1){
		var emailAddress = cmExtractParameter("cm_em",document.location.href);

		if (emailAddress.indexOf(":")>-1){
			emailAddress=emailAddress.substring(emailAddress.indexOf(":")+1);
		}

		cmCreateRegistrationTag(emailAddress,emailAddress);
	}

	if (cmIndexOfParameter("cm_lm",document.location.href) != -1){
		var emailAddress = cmExtractParameter("cm_lm",document.location.href);

		if (emailAddress.indexOf(":")>-1){
			emailAddress=emailAddress.substring(emailAddress.indexOf(":")+1);
		}

		cmCreateRegistrationTag(emailAddress,emailAddress);
	}
}

/* manual PageviewTag for off site page tagging.  Allows client to supply URL and Referring URL */
function cmCreateManualPageviewTag(pageID, categoryID,DestinationURL,ReferringURL) {
	cmMakeTag(["tid","1","pi",pageID,"cg",categoryID,"ul",DestinationURL,"rf",ReferringURL]);
}

function cmCreateManualImpressionTag(pageID, trackSP, trackRE) {
	// insert code to get pageID from cmTagControl if pageID is null
	cmMakeTag(["tid","9","pi",pageID,"cm_sp",trackSP,"cm_re",trackRE,"st",cm_ClientTS]);
}

function cmCreateManualLinkClickTag(href,name,pageID) {
	if (cmCreateLinkTag == null && cM != null) {
		var cmCreateLinkTag = cM;
	}

	if (cmCreateLinkTag != null) {
		var dt = new Date();
		cmLnkT3 = dt.getTime();
		href = cG7.normalizeURL(href, true);
		cmCreateLinkTag(cm_ClientTS, cmLnkT3, name, href, false, pageID);
	}
}

function cmCreatePageElementTag(elementID, elementCategory, attributes) {
	if (attributes){
		var cm_exAttr=new Array;
		cm_exAttr=attributes.split("-_-");
	}

	cmMakeTag(["tid","15","eid",elementID,"ecat",elementCategory,"pflg","0","cm_exAttr",cm_exAttr]);
}

/*  hash functions that support shop aggregation with attributes */
function cm_hex_sha1(s) { return cm_rstr2hex(cm_rstr_sha1(cm_str2rstr_utf8(s))); }

function cm_rstr_sha1(s) {
	return cm_binb2rstr(cm_binb_sha1(cm_rstr2binb(s), s.length * 8));
}

function cm_rstr2hex(input) {
	var hex_tab = 0 ? "0123456789ABCDEF" : "0123456789abcdef";
	var output = "";
	var x;

	for(var i = 0; i < input.length; i++) {
		x = input.charCodeAt(i);
		output += hex_tab.charAt((x >>> 4) & 0x0F) + hex_tab.charAt( x        & 0x0F);
  }

  return output;
}

function cm_str2rstr_utf8(input)
{
  var output = "";
  var i = -1;
  var x, y;

  while(++i < input.length)
  {
	/* Decode utf-16 surrogate pairs */
	x = input.charCodeAt(i);
	y = i + 1 < input.length ? input.charCodeAt(i + 1) : 0;
	if(0xD800 <= x && x <= 0xDBFF && 0xDC00 <= y && y <= 0xDFFF)
	{
	  x = 0x10000 + ((x & 0x03FF) << 10) + (y & 0x03FF);
	  i++;
	}

	/* Encode output as utf-8 */
	if(x <= 0x7F) {
	  output += String.fromCharCode(x);
	} else if(x <= 0x7FF) {
	  output += String.fromCharCode(0xC0 | ((x >>> 6 ) & 0x1F),
									0x80 | ( x         & 0x3F));
	} else if(x <= 0xFFFF) {
	  output += String.fromCharCode(0xE0 | ((x >>> 12) & 0x0F),
									0x80 | ((x >>> 6 ) & 0x3F),
									0x80 | ( x         & 0x3F));
	} else if(x <= 0x1FFFFF) {
	  output += String.fromCharCode(0xF0 | ((x >>> 18) & 0x07),
									0x80 | ((x >>> 12) & 0x3F),
									0x80 | ((x >>> 6 ) & 0x3F),
									0x80 | ( x         & 0x3F));
	}
  }
  return output;
}

function cm_rstr2binb(input) {
	var output = Array(input.length >> 2);

	for(var i = 0; i < output.length; i++) {
		output[i] = 0;
	}

	for(var i = 0; i < input.length * 8; i += 8) {
		output[i>>5] |= (input.charCodeAt(i / 8) & 0xFF) << (24 - i % 32);
	}

	return output;
}

function cm_binb2rstr(input) {
	var output = "";

	for(var i = 0; i < input.length * 32; i += 8) {
		output += String.fromCharCode((input[i>>5] >>> (24 - i % 32)) & 0xFF);
	}

	return output;
}

function cm_binb_sha1(x, len) {
	/* append padding */
	x[len >> 5] |= 0x80 << (24 - len % 32);
	x[((len + 64 >> 9) << 4) + 15] = len;

	var w = Array(80);
	var a =  1732584193;
	var b = -271733879;
	var c = -1732584194;
	var d =  271733878;
	var e = -1009589776;

	for(var i = 0; i < x.length; i += 16) {
		var olda = a;
		var oldb = b;
		var oldc = c;
		var oldd = d;
		var olde = e;

		for(var j = 0; j < 80; j++) {
			if(j < 16) {
				w[j] = x[i + j];
			} else {
				w[j] = cm_bit_rol(w[j-3] ^ w[j-8] ^ w[j-14] ^ w[j-16], 1);
			}

			var t = cm_safe_add(cm_safe_add(cm_bit_rol(a, 5), cm_sha1_ft(j, b, c, d)), cm_safe_add(cm_safe_add(e, w[j]), cm_sha1_kt(j)));
			e = d;
			d = c;
			c = cm_bit_rol(b, 30);
			b = a;
			a = t;
		}

		a = cm_safe_add(a, olda);
		b = cm_safe_add(b, oldb);
		c = cm_safe_add(c, oldc);
		d = cm_safe_add(d, oldd);
		e = cm_safe_add(e, olde);
	}
	return Array(a, b, c, d, e);
}

function cm_sha1_ft(t, b, c, d)
{
  if(t < 20) return (b & c) | ((~b) & d);
  if(t < 40) return b ^ c ^ d;
  if(t < 60) return (b & c) | (b & d) | (c & d);
  return b ^ c ^ d;
}

function cm_sha1_kt(t)
{
  return (t < 20) ?  1518500249 : (t < 40) ?  1859775393 :
		 (t < 60) ? -1894007588 : -899497514;
}

function cm_safe_add(x, y)
{
  var lsw = (x & 0xFFFF) + (y & 0xFFFF);
  var msw = (x >> 16) + (y >> 16) + (lsw >> 16);

  return (msw << 16) | (lsw & 0xFFFF);
}

function cm_bit_rol(num, cnt) {
  return (num << cnt) | (num >>> (32 - cnt));
}
/* This was moved from the masthead javascript file. */
function getCmExplorerAttributes(){
	var attributes = '',
		SEP = '-_-',
		selectedStore = Lowes.User.getCurrentStore(),
		chosenItems = [
			selectedStore.name,
			selectedStore.state,
			selectedStore.number,
			selectedStore.zip,
			selectedStore.city
		];
	for ( var i = 0; i < chosenItems.length; i++ ){
		attributes += chosenItems[i] || '';
		attributes += (i == chosenItems.length-1) ? '' : SEP;
	}
	return attributes;
}

cmSetProduction();
/* the following line disables the Intelligent offers console.logs */
IORequest.log = function(a, b, c) {};

/* cq.js */

// Temporary coremetrics object for setting pageview data to Home
// Will need to remove or update if mylowes is delivered prior to coremetrics removal
cmPageviewData = {'categoryId': 'HOME','pageName': 'HOME'};
cG7.cM0[cm_ClientID] = cmPageviewData.pageName;

/* ../global/lib/jquery-hoverintent.js */

/* DO NOT LINT */
/**
* hoverIntent is similar to jQuery's built-in "hover" function except that
* instead of firing the onMouseOver event immediately, hoverIntent checks
* to see if the user's mouse has slowed down (beneath the sensitivity
* threshold) before firing the onMouseOver event.
* 
* hoverIntent r5 // 2007.03.27 // jQuery 1.1.2
* <http://cherne.net/brian/resources/jquery.hoverIntent.html>
* 
* hoverIntent is currently available for use in all personal or commercial 
* projects under both MIT and GPL licenses. This means that you can choose 
* the license that best suits your project, and use it accordingly.
* 
* // basic usage (just like .hover) receives onMouseOver and onMouseOut functions
* $("ul li").hoverIntent( showNav , hideNav );
* 
* // advanced usage receives configuration object only
* $("ul li").hoverIntent({
*	sensitivity: 2, // number = sensitivity threshold (must be 1 or higher)
*	interval: 50,   // number = milliseconds of polling interval
*	over: showNav,  // function = onMouseOver callback (required)
*	timeout: 100,   // number = milliseconds delay before onMouseOut function call
*	out: hideNav    // function = onMouseOut callback (required)
* });
* 
* @param  f  onMouseOver function || An object with configuration options
* @param  g  onMouseOut function  || Nothing (use configuration options object)
* @return    The object (aka "this") that called hoverIntent, and the event object
* @author    Brian Cherne <brian@cherne.net>
*/
(function($) {
	$.fn.hoverIntent = function(f,g) {
		// default configuration options
		var cfg = {
			sensitivity: 7,
			interval: 100,
			timeout: 0
		};
		// override configuration options with user supplied object
		cfg = $.extend(cfg, g ? { over: f, out: g } : f );

		// instantiate variables
		// cX, cY = current X and Y position of mouse, updated by mousemove event
		// pX, pY = previous X and Y position of mouse, set by mouseover and polling interval
		var cX, cY, pX, pY;

		// A private function for getting mouse position
		var track = function(ev) {
			cX = ev.pageX;
			cY = ev.pageY;
		};

		// A private function for comparing current and previous mouse position
		var compare = function(ev,ob) {
			ob.hoverIntent_t = clearTimeout(ob.hoverIntent_t);
			// compare mouse positions to see if they've crossed the threshold
			if ( ( Math.abs(pX-cX) + Math.abs(pY-cY) ) < cfg.sensitivity ) {
				$(ob).unbind("mousemove",track);
				// set hoverIntent state to true (so mouseOut can be called)
				ob.hoverIntent_s = 1;
				return cfg.over.apply(ob,[ev]);
			} else {
				// set previous coordinates for next time
				pX = cX; pY = cY;
				// use self-calling timeout, guarantees intervals are spaced out properly (avoids JavaScript timer bugs)
				ob.hoverIntent_t = setTimeout( function(){compare(ev, ob);} , cfg.interval );
			}
		};

		// A private function for delaying the mouseOut function
		var delay = function(ev,ob) {
			ob.hoverIntent_t = clearTimeout(ob.hoverIntent_t);
			ob.hoverIntent_s = 0;
			return cfg.out.apply(ob,[ev]);
		};

		// A private function for handling mouse 'hovering'
		var handleHover = function(e) {
			// next three lines copied from jQuery.hover, ignore children onMouseOver/onMouseOut
			var p = (e.type == "mouseover" ? e.fromElement : e.toElement) || e.relatedTarget;
			while ( p && p != this ) { try { p = p.parentNode; } catch(e) { p = this; } }
			if ( p == this ) { return false; }

			// copy objects to be passed into t (required for event object to be passed in IE)
			var ev = jQuery.extend({},e);
			var ob = this;

			// cancel hoverIntent timer if it exists
			if (ob.hoverIntent_t) { ob.hoverIntent_t = clearTimeout(ob.hoverIntent_t); }

			// else e.type == "onmouseover"
			if (e.type == "mouseover") {
				// set "previous" X and Y position based on initial entry point
				pX = ev.pageX; pY = ev.pageY;
				// update "current" X and Y position based on mousemove
				$(ob).bind("mousemove",track);
				// start polling interval (self-calling timeout) to compare mouse coordinates over time
				if (ob.hoverIntent_s != 1) { ob.hoverIntent_t = setTimeout( function(){compare(ev,ob);} , cfg.interval );}

			// else e.type == "onmouseout"
			} else {
				// unbind expensive mousemove event
				$(ob).unbind("mousemove",track);
				// if hoverIntent state is true, then call the mouseOut function after the specified delay
				if (ob.hoverIntent_s == 1) { ob.hoverIntent_t = setTimeout( function(){delay(ev,ob);} , cfg.timeout );}
			}
		};

		// bind the function to the two event listeners
		return this.mouseover(handleHover).mouseout(handleHover);
	};
})(jQuery);

/* ../global/Lowes/function.js */

/** @fileOverview Extending Function prototype */

(function(){
	function update(array, args) {
		var aryLen = array.length,
			argLen = args.length;

		while(argLen--)
			array[aryLen + argLen] = args[argLen];

		return array;
	}

	function merge(array, args) {
		return update(Array.prototype.slice.call(array, 0), args);
	}
	/**
	 *	Expose context
	 *	@param {Object} context The context to expose to this Object
	 */
	Function.prototype.expose = function(context) {
		if(arguments.length < 2 && typeof arguments[0] === 'undefined') {
			return this;
		}
		var __function = this,
			__args = Array.prototype.slice(arguments, 1);

		return function() {
			var args = merge(__args, arguments);
			return __function.apply(context, args);
		}
	};

	/**
	 * Get the names of the `Function`'s arguments
	 *
	 * @returns Array Names of `Function` arguments
	 * @example
	 * var myFunction = function (input, output){ input='';output='';return output;};
	 * myFunction.argNames();
	 */
	Function.prototype.argNames = function() {
		var names = this.toString().match(/^[\s\(]*function[^(]*\(([^)]*)\)/)[1]
								   .replace(/\/\/.*?[\r\n]|\/\*(?:.|[\r\n])*?\*\//g, '')
								   .replace(/\s+/g, '').split(',');

		return names.length == 1 && !names[0] ? [] : names;
	}

	/**
	 * Delay a function execution
	 *
	 * @param {Number} timeout The time to delay in seconds
	 */
	Function.prototype.delay = function(timeout) {
	   var __function = this,
		  args = Array.prototype.slice(arguments, 1),
		  timeout = timeout * 1000;

		  return window.setTimeout(function (){
			 return __function.apply(__function, args);
		  }, timeout);
	}

	/** No idea really...seems to take function arguments and fire them after a millisecond delay */
	Function.prototype.defer = function() {
	  var args = [0.01].concat(arguments);

	  return this.delay.apply(this, args);
	}
}());

/* ../global/Lowes/string.js */

/** @fileOverview Library for native String extensions */

(function (){
	/**
	 * Extend String to add format ($$) function that will
	 * mimic C's sprintf function. Replace %@ character set
	 * with param/values passed to the method.
	 */
	String.prototype.format = String.prototype.$$ = function() {
		// Grab all our arguments
		var args = arguments;

		// Predefined an loop index.
		var idx = 0;

		// return the replaced values
		return this.replace(/%@([0-9]+)?/g, function(s, argIndex){
			argIndex = (argIndex) ? parseInt(argIndex,0)-1 : idx++;
			s = args[argIndex];

			return ((s===null) ? '(null)' : (s===undefined) ? '' : s).toString();
		});
	};

	/**
	 * Extend String to add words (w) method that will split
	 * a space separated string into an array
	 */
	String.prototype.words = String.prototype.w = function(){
		return this.split(' ');
	};

	/**
	 * Extend String to add method to uppercase just
	 * the first word of a sentence.
	 */
	String.prototype.ucFirstSentence = function (){
		/* Grab first letter, uppercase it, and put it back together with rest of the string */
		return this.substring(0,1).toUpperCase() + this.substring(1, this.length);
	};

	/**
	 * Extend String to add method to uppercase the first letter
	 * of each word in a sentence.
	 */
	String.prototype.ucFirstWord = function (){
		/* Grab an array of words from passed sentence */
		var stringArray = this.w();
		/* Setup complete string container var */
		var finalString = '';
		var item;
		/*
		 * Loop through all words in the string array, change the
		 * first letter of current word to a capital letter, and put
		 * back together with the rest of the word
		 */
		for(item in stringArray) {
			finalString += stringArray[item].substring(0,1).toUpperCase() + stringArray[item].substring(1, stringArray[item].length) + ' ';
		}
		/* Trim/Clean up the final string and return it */
		return finalString.substring(0, (finalString.length - 1));
	};

	/**
	 * Extend String to add method that will take camelCase
	 * syntax and convert it into a space seperated 'sentence'
	 * @deprecated
	 */
	String.prototype.deCamel = function (){
		/* Look for camelCase pattern, replace with character, set to lower case and return value */
		var returnString = this.replace(new RegExp("([A-Z])", "g"), (typeof arguments[0] !== 'undefined') ? arguments[0] : "_" + "$1");
		return returnString.toLowerCase();
	};

	/**
	 * Extend String to add method that will parse and convert links to
	 * valid html (a) links.
	 */
	String.prototype.linkify = function (){
		/* Extend String to add a parse routine to convert urls to links. Adding html into the string. */
		return (function(urlString){
			/* Setup matching pattern to patch http/s, ftp, file url types */
			var pattern = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;

			/***
			 * If there is www with no protocol:// pattern, we need
			 * to go a head and add it so the pattern replace will work.
			 *
			 * Note: this will not parse anything without www., unless
			 * it begins with protocol://
			 **/
			return (!urlString.match(pattern)) ?
				urlString.replace('www.', 'http://').replace(pattern,"<a href='$1'>$1</a>") :
				urlString.replace(pattern,"<a href='$1'>$1</a>");
		}(this));
	};

	/**
	 * Extend String to include a method that will add
	 * escape slashes to strings.
	 */
	String.prototype.addslashes = function (){
		var str = this;
		str=str.replace(/\'/g,'\\\'');
		str=str.replace(/\"/g,'\\"');
		str=str.replace(/\\/g,'\\\\');
		str=str.replace(/\0/g,'\\0');
		return str;
	};

	/**
	 * Extend String to include a method that will remove
	 * escape slashes from strings.
	 */
	String.prototype.stripslashes = function (){
		var str = this;
		str=str.replace(/\\'/g,'\'');
		str=str.replace(/\\"/g,'"');
		str=str.replace(/\\\\/g,'\\');
		str=str.replace(/\\0/g,'\0');
		return str;
	};

	/**
	 * Extend String to include a method that will remove
	 * dead space from the beginning and end of a string.
	 * @deprecated Use jQuery.trim()
	 */
	String.prototype.trim = function (str){
		return this.replace(/(?:^\s+|\s+$)/g, "");
	};

	/**
	 * Extend String to include a method that will
	 * format a string to capitalize important words
	 * Modified from code by Resig/Gruber
	 */
	String.prototype.toTitleCase = function() {
		var small_words = 'a an and as at but by en for if in of on or the to v[.]? via vs[.]?'.split(/\s/);
		var punct = '!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~';
		var small_re = small_words.join('|');

		function capitalize(s) {
			return s.charAt(0).toUpperCase() + s.substring(1);
		}

		var general_repl = [
			[/\b([A-Za-z][a-z.\']*)\b/g,
			 function(str, word) {
				 if (/[A-Za-z][.][A-Za-z]/.test(word)){
					 return word;
				 }
				 return capitalize(word);
			 }
			],
			[new RegExp('\\b(' + small_re + ')\\b', 'ig'),
			 function(str, small) { return small.toLowerCase(); }
			],
			[new RegExp('^([' + punct + ']*)(' + small_re + ')\\b', 'ig'),
			 function(str, punct, small) { return punct + capitalize(small); }
			],
			[new RegExp('\\b(' + small_re + ')([' + punct + ']*)$', 'ig'),
			 function(str, small, punct) { return capitalize(small) + punct; }
			]
		];

		var special_repl = [
			[/ V(s?)\. /g,
			 function(str, s) { return ' v' + s + '. '; }
			],
			[/([\'\u2019])S\b/g,
			 function(str, apos) { return apos + 's'; }
			],
			[/\b(AT&T|Q&A)\b/ig,
			 function(str, s) { return s.toUpperCase(); }
			],
			[/\b([NEWS]\.)/ig,
			 function(str, s) { return s.toUpperCase(); }
			]
		];

		var split_re = /([:.;?!][ ]|(?:[ ]|^)[\"\u201c])/g;
		var tokens_in = [];
		var tokens_out = [];
		var token, regex, repl, idx = 0, m;
		var check = this.toLowerCase();
		var i, j, k;
		while ((m = split_re.exec(check)) != null) {
			tokens_in.push(check.substring(idx, m.index), m[1]);
			idx = split_re.lastIndex;
		}
		tokens_in.push(check.substring(idx));

		for (i = 0; i < tokens_in.length; i++) {
			token = tokens_in[i];
			for (j = 0; j < general_repl.length; j++) {
				regex = general_repl[j][0];
				repl  = general_repl[j][1];
				token = token.replace(regex, repl);
			}
			tokens_out.push(token);
		}
		var title = tokens_out.join('');
		for (k = 0; k < special_repl.length; k++) {
			regex = special_repl[k][0];
			repl  = special_repl[k][1];
			title = title.replace(regex, repl);
		}

		return title;
	}
}());

/* ../global/Lowes/object.js */

/** @fileOverview Native Object utilities/methods */

(function (){
	/* private */
	function size(object) {
		var sz = 0, key;

		for(key in object) {
			if(object.hasOwnProperty(key)){ sz++; }
		}
		return sz;
	}
	/* private */
	function extend(destination, source) {
		var item;
		for(item in source){ destination[item] = source[item]; }
		return destination;
	}
	/* private */
	function clone(object) { return extend({}, object); }
	
	/*
	 * Extend native Object element to add functionality
	 * and make the methods globally accessible through the 
	 * native Object
	 */
	extend(Object, {
		/**
		 * Method for extending an Object's context
		 */
		extend: extend,
		/**
		 * Take one Object and return an exact copy of it
		 */
		clone: clone,
		/** Objects don't have length */
		size: size
	});
}());

/* ../global/Lowes/class.js */

/** John Resig's class implementation. */
(function(){
	var initializing = false, fnTest = /xyz/.test(function(){xyz;}) ? /\b_super\b/ : /.*/;
	// The base Class implementation (does nothing)
	this.Class = function(){};

	// Create a new Class that inherits from this class
	Class.extend = function(prop) {
	var _super = this.prototype;

	// Instantiate a base class (but only create the instance,
	// don't run the init constructor)
	initializing = true;
	var prototype = new this();
	initializing = false;

	// Copy the properties over onto the new prototype
	for (var name in prop) {
		// Check if we're overwriting an existing function
		prototype[name] = typeof prop[name] == "function" &&
		typeof _super[name] == "function" && fnTest.test(prop[name]) ?
		(function(name, fn){
			return function() {
			var tmp = this._super;

			// Add a new ._super() method that is the same method
			// but on the super-class
			this._super = _super[name];

			// The method only need to be bound temporarily, so we
			// remove it when we're done executing
			var ret = fn.apply(this, arguments);
			this._super = tmp;

			return ret;
			};
		})(name, prop[name]) :
		prop[name];
	}

	// The dummy class constructor
	function Class() {
		// All construction is actually done in the init method
		if ( !initializing && this.init )
		this.init.apply(this, arguments);
	}

	// Populate our constructed prototype object
	Class.prototype = prototype;

	// Enforce the constructor to be what we expect
	Class.constructor = Class;

	// And make this class extendable
	Class.extend = arguments.callee;

	return Class;
	};
}());

/* ../global/Lowes/json.js */

/*
	http://www.JSON.org/json2.js
	2010-08-25

	Public Domain.

	See http://www.JSON.org/js.html

	This is a reference implementation. You are free to copy, modify, or
	redistribute.
*/

// Create a JSON object only if one does not already exist. We create the
// methods in a closure to avoid creating global variables.

if (!this.JSON) {
	this.JSON = {};
}

(function () {

	function f(n) {
		// Format integers to have at least two digits.
		return n < 10 ? '0' + n : n;
	}

	if (typeof Date.prototype.toJSON !== 'function') {

		Date.prototype.toJSON = function (key) {

			return isFinite(this.valueOf()) ?
				   this.getUTCFullYear()   + '-' +
				 f(this.getUTCMonth() + 1) + '-' +
				 f(this.getUTCDate())	   + 'T' +
				 f(this.getUTCHours())	   + ':' +
				 f(this.getUTCMinutes())   + ':' +
				 f(this.getUTCSeconds())   + 'Z' : null;
		};

		String.prototype.toJSON =
		Number.prototype.toJSON =
		Boolean.prototype.toJSON = function (key) {
			return this.valueOf();
		};
	}

	var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
		escapable = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
		gap,
		indent,
		meta = {	// table of character substitutions
			'\b': '\\b',
			'\t': '\\t',
			'\n': '\\n',
			'\f': '\\f',
			'\r': '\\r',
			'"' : '\\"',
			'\\': '\\\\'
		},
		rep;


	function quote(string) {

// If the string contains no control characters, no quote characters, and no
// backslash characters, then we can safely slap some quotes around it.
// Otherwise we must also replace the offending characters with safe escape
// sequences.

		escapable.lastIndex = 0;
		return escapable.test(string) ?
			'"' + string.replace(escapable, function (a) {
				var c = meta[a];
				return typeof c === 'string' ? c :
					'\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
			}) + '"' :
			'"' + string + '"';
	}


	function str(key, holder) {

// Produce a string from holder[key].

		var i,			// The loop counter.
			k,			// The member key.
			v,			// The member value.
			length,
			mind = gap,
			partial,
			value = holder[key];

// If the value has a toJSON method, call it to obtain a replacement value.

		if (value && typeof value === 'object' &&
				typeof value.toJSON === 'function') {
			value = value.toJSON(key);
		}

// If we were called with a replacer function, then call the replacer to
// obtain a replacement value.

		if (typeof rep === 'function') {
			value = rep.call(holder, key, value);
		}

// What happens next depends on the value's type.

		switch (typeof value) {
		case 'string':
			return quote(value);

		case 'number':

// JSON numbers must be finite. Encode non-finite numbers as null.

			return isFinite(value) ? String(value) : 'null';

		case 'boolean':
		case 'null':

// If the value is a boolean or null, convert it to a string. Note:
// typeof null does not produce 'null'. The case is included here in
// the remote chance that this gets fixed someday.

			return String(value);

// If the type is 'object', we might be dealing with an object or an array or
// null.

		case 'object':

// Due to a specification blunder in ECMAScript, typeof null is 'object',
// so watch out for that case.

			if (!value) {
				return 'null';
			}

// Make an array to hold the partial results of stringifying this object value.

			gap += indent;
			partial = [];

// Is the value an array?

			if (Object.prototype.toString.apply(value) === '[object Array]') {

// The value is an array. Stringify every element. Use null as a placeholder
// for non-JSON values.

				length = value.length;
				for (i = 0; i < length; i += 1) {
					partial[i] = str(i, value) || 'null';
				}

// Join all of the elements together, separated with commas, and wrap them in
// brackets.

				v = partial.length === 0 ? '[]' :
					gap ? '[\n' + gap +
							partial.join(',\n' + gap) + '\n' +
								mind + ']' :
						  '[' + partial.join(',') + ']';
				gap = mind;
				return v;
			}

// If the replacer is an array, use it to select the members to be stringified.

			if (rep && typeof rep === 'object') {
				length = rep.length;
				for (i = 0; i < length; i += 1) {
					k = rep[i];
					if (typeof k === 'string') {
						v = str(k, value);
						if (v) {
							partial.push(quote(k) + (gap ? ': ' : ':') + v);
						}
					}
				}
			} else {

// Otherwise, iterate through all of the keys in the object.

				for (k in value) {
					if (Object.hasOwnProperty.call(value, k)) {
						v = str(k, value);
						if (v) {
							partial.push(quote(k) + (gap ? ': ' : ':') + v);
						}
					}
				}
			}

// Join all of the member texts together, separated with commas,
// and wrap them in braces.

			v = partial.length === 0 ? '{}' :
				gap ? '{\n' + gap + partial.join(',\n' + gap) + '\n' +
						mind + '}' : '{' + partial.join(',') + '}';
			gap = mind;
			return v;
		}
	}

// If the JSON object does not yet have a stringify method, give it one.

	if (typeof JSON.stringify !== 'function') {
		JSON.stringify = function (value, replacer, space) {

// The stringify method takes a value and an optional replacer, and an optional
// space parameter, and returns a JSON text. The replacer can be a function
// that can replace values, or an array of strings that will select the keys.
// A default replacer method can be provided. Use of the space parameter can
// produce text that is more easily readable.

			var i;
			gap = '';
			indent = '';

// If the space parameter is a number, make an indent string containing that
// many spaces.

			if (typeof space === 'number') {
				for (i = 0; i < space; i += 1) {
					indent += ' ';
				}

// If the space parameter is a string, it will be used as the indent string.

			} else if (typeof space === 'string') {
				indent = space;
			}

// If there is a replacer, it must be a function or an array.
// Otherwise, throw an error.

			rep = replacer;
			if (replacer && typeof replacer !== 'function' &&
					(typeof replacer !== 'object' ||
					 typeof replacer.length !== 'number')) {
				throw new Error('JSON.stringify');
			}

// Make a fake root object containing our value under the key of ''.
// Return the result of stringifying the value.

			return str('', {'': value});
		};
	}


// If the JSON object does not yet have a parse method, give it one.

	if (typeof JSON.parse !== 'function') {
		JSON.parse = function (text, reviver) {

// The parse method takes a text and an optional reviver function, and returns
// a JavaScript value if the text is a valid JSON text.

			var j;

			function walk(holder, key) {

// The walk method is used to recursively walk the resulting structure so
// that modifications can be made.

				var k, v, value = holder[key];
				if (value && typeof value === 'object') {
					for (k in value) {
						if (Object.hasOwnProperty.call(value, k)) {
							v = walk(value, k);
							if (v !== undefined) {
								value[k] = v;
							} else {
								delete value[k];
							}
						}
					}
				}t
				return reviver.call(holder, key, value);
			}


// Parsing happens in four stages. In the first stage, we replace certain
// Unicode characters with escape sequences. JavaScript handles many characters
// incorrectly, either silently deleting them, or treating them as line endings.

			text = String(text);
			cx.lastIndex = 0;
			if (cx.test(text)) {
				text = text.replace(cx, function (a) {
					return '\\u' +
						('0000' + a.charCodeAt(0).toString(16)).slice(-4);
				});
			}

// In the second stage, we run the text against regular expressions that look
// for non-JSON patterns. We are especially concerned with '()' and 'new'
// because they can cause invocation, and '=' because it can cause mutation.
// But just to be safe, we want to reject all unexpected forms.

// We split the second stage into 4 regexp operations in order to work around
// crippling inefficiencies in IE's and Safari's regexp engines. First we
// replace the JSON backslash pairs with '@' (a non-JSON character). Second, we
// replace all simple value tokens with ']' characters. Third, we delete all
// open brackets that follow a colon or comma or that begin the text. Finally,
// we look to see that the remaining characters are only whitespace or ']' or
// ',' or ':' or '{' or '}'. If that is so, then the text is safe for eval.

			if (/^[\],:{}\s]*$/
.test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@')
.replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']')
.replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {

// In the third stage we use the eval function to compile the text into a
// JavaScript structure. The '{' operator is subject to a syntactic ambiguity
// in JavaScript: it can begin a block or an object literal. We wrap the text
// in parens to eliminate the ambiguity.

				j = eval('(' + text + ')');

// In the optional fourth stage, we recursively walk the new structure, passing
// each name/value pair to a reviver function for possible transformation.

				return typeof reviver === 'function' ? walk({'': j}, '') : j;
			}

// If the text is not JSON parseable, then a SyntaxError is thrown.

			throw new SyntaxError('JSON.parse');
		};
	}
}());

/* ../global/Lowes/core.js */

/**
 * @fileOverview Lowes Core, sets up Lowes namespace and a few helper functions.
 */

(function ($){
	var i, methods, noop = function (){};
	if(!window.console){
		methods=[
			"log",
			"debug",
			"info",
			"warn",
			"error",
			"assert",
			"dir",
			"dirxml",
			"group",
			"groupEnd",
			"time",
			"timeEnd",
			"count",
			"trace",
			"profile",
			"profileEnd"
		];
		/**
		 * Spoof a console to catch errors from undefined console.*
		 * @name console
		 * @namespace
		 */
		window.console={};
		for(i=0;i<methods.length;++i){
			window.console[methods[i]]=noop;
		}
	}
}(jQuery));

/* Remove the no-js class since we have js */
$('body,html').removeClass('no-js');

/**
 * Lowes Namespace Object that will be the parent of all Lowes JS library classes.
 * @name Lowes
 * @namespace
 */
window.Lowes = {};

/** Totally incorrect, looking into this. */
Lowes.Version = "1.0.0";

/**
 * A browser object to check against.
 *
 * @memberOf Lowes
 */
Lowes.Browser = (function(){
	/* Grab the user agent. */
	var userAgent = navigator.userAgent;
	/* Go ahead and get the odd ball out the way. */
	var isOpera = Object.prototype.toString.call(window.opera) == '[object Opera]';
	var browser = {
			IE:				  !!window.attachEvent && !isOpera,
			Opera:		   isOpera,
			Safari:			  /(chrome).*(safari)/ig.test(userAgent)? false : true,
			Chrome:			  /(chrome).*(safari)/ig.test(userAgent)? true : false,
			Firefox:	   /(firefox)/ig.test(userAgent),
			MobileSafari:  /apple.*mobile/ig.test(userAgent)
		};
	return browser;
}());

/* Set params for Websphere storeId, langId, and CatalogId */
/** Websphere Store ID */
Lowes.storeId = 10151;
/** Websphere Language ID */
Lowes.langId = -1;
/** Websphere Catalog ID */
Lowes.catalogId = 10051;

// mPulse Ajax Tracking
var mPtimer = '';
$(document).ajaxSend(function(event, jqXHR, settings) {
	// gets the URL being used in the async call
	var $url = $('<a>').attr('href',settings.url),
		ajaxPath = $url[0].pathname;
	if ( !!window.BOOMR ) {
		// send the pathname without it's params to the requestStart method
		mPtimer = BOOMR.requestStart(ajaxPath.substr(ajaxPath.lastIndexOf('/') + 1));
	};
});
$(document).ajaxStart(function() {
	if ( !!window.mPtimer ){ mPtimer.loaded(); }
});

/* ../global/Lowes/cookie.js */

/**
 * @fileOverview Methods for dealing with cookies.
 */
(function (Lowes){
	/**
	 * Object for working with browser cookies. Simplifies setting, getting, deleting
	 * @namespace
	 */
	Lowes.Cookie = {
		/**
		 * Retrieve a cookie's value from the browser by name.
		 *
		 * @example Lowes.Cookie.get("selectedStore1");
		 * @param {String} name Name of the cookie.
		 */
		get: function(name) {
			var n = name + '=',
				ca = document.cookie.split(';'),
				i, c;
			for(i=0;i < ca.length;i++) {
				c = ca[i];
				while (c.charAt(0)==' ') { c = c.substring(1,c.length); }
				if (c.indexOf(n) == 0) {
					return c.substring(n.length,c.length);
				}
			}
			return null;
		},

		/**
		 * Create a cookie and set its value in the browser.
		 *
		 * @example Lowes.Cookie.set("selectedStore1", "store|details|list", 7);
		 * @param {String} name The name of the cookie to create
		 * @param {String} value The value to set the cookie with given name
		 * @param {String} days The date of expiration
		 */
		set: function(name, value, days) {
			var expires = '',
				date;
			if (days) {
				date = new Date();
				date.setTime(date.getTime()+(days*24*60*60*1000));
				expires = "; expires="+date.toGMTString();
			}
			document.cookie = name+"="+value+expires+"; path=/";
		},

		/**
		 * Remove a cookie from the browser by name.
		 *
		 * @example Lowes.Cookie.del("selectedStore1")
		 * @param {String} name Name of cookie to delete
		 */
		del: function(name) {
			// To delete, we just the value to nothing and
			// force it to expire.
			this.set(name, "", -1);
		},

		/**
		 * Cookie class method to see if cookies are enabled in browser
		 * @example if(Lowes.Cookie.enabled()){ alert("Yay, you like my cookies!"); }
		 * @returns {Boolean} Cookies enabled
		 */
		enabled: function(){
			var e = !!navigator.cookieEnabled;
			//if navigator,cookieEnabled is not supported
			if (typeof navigator.cookieEnabled=="undefined" && !e){
				document.cookie = "testcookie";
				e = document.cookie.indexOf("testcookie") != -1;
			}
			return e;
		}
	};
	/* Expose the updated library to global. */
	window.Lowes = Lowes;
}(window.Lowes || {}));

/* ../jquery/plugins/jquery.cookie.js */

/*jslint browser: true */ /*global jQuery: true */

/**
 * jQuery Cookie plugin
 *
 * Copyright (c) 2010 Klaus Hartl (stilbuero.de)
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 */

// TODO JsDoc

/**
 * Create a cookie with the given key and value and other optional parameters.
 *
 * @example $.cookie('the_cookie', 'the_value');
 * @desc Set the value of a cookie.
 * @example $.cookie('the_cookie', 'the_value', { expires: 7, path: '/', domain: 'http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/jquery.com', secure: true });
 * @desc Create a cookie with all available options.
 * @example $.cookie('the_cookie', 'the_value');
 * @desc Create a session cookie.
 * @example $.cookie('the_cookie', null);
 * @desc Delete a cookie by passing null as value. Keep in mind that you have to use the same path and domain
 *       used when the cookie was set.
 *
 * @param String key The key of the cookie.
 * @param String value The value of the cookie.
 * @param Object options An object literal containing key/value pairs to provide optional cookie attributes.
 * @option Number|Date expires Either an integer specifying the expiration date from now on in days or a Date object.
 *                             If a negative value is specified (e.g. a date in the past), the cookie will be deleted.
 *                             If set to null or omitted, the cookie will be a session cookie and will not be retained
 *                             when the the browser exits.
 * @option String path The value of the path atribute of the cookie (default: path of page that created the cookie).
 * @option String domain The value of the domain attribute of the cookie (default: domain of page that created the cookie).
 * @option Boolean secure If true, the secure attribute of the cookie will be set and the cookie transmission will
 *                        require a secure protocol (like HTTPS).
 * @type undefined
 *
 * @name $.cookie
 * @cat Plugins/Cookie
 * @author Klaus Hartl/klaus.hartl@stilbuero.de
 */

/**
 * Get the value of a cookie with the given key.
 *
 * @example $.cookie('the_cookie');
 * @desc Get the value of a cookie.
 *
 * @param String key The key of the cookie.
 * @return The value of the cookie.
 * @type String
 *
 * @name $.cookie
 * @cat Plugins/Cookie
 * @author Klaus Hartl/klaus.hartl@stilbuero.de
 */
jQuery.cookie = function (key, value, options) {
    
    // key and at least value given, set cookie...
    if (arguments.length > 1 && String(value) !== "[object Object]") {
        options = jQuery.extend({}, options);

        if (value === null || value === undefined) {
            options.expires = -1;
        }

        if (typeof options.expires === 'number') {
            var days = options.expires, t = options.expires = new Date();
            t.setDate(t.getDate() + days);
        }
        
        value = String(value);
        
        return (document.cookie = [
            encodeURIComponent(key), '=',
            options.raw ? value : encodeURIComponent(value),
            options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
            options.path ? '; path=' + options.path : '',
            options.domain ? '; domain=' + options.domain : '',
            options.secure ? '; secure' : ''
        ].join(''));
    }

    // key and possibly options given, get cookie...
    options = value || {};
    var result, decode = options.raw ? function (s) { return s; } : decodeURIComponent;
    return (result = new RegExp('(?:^|; )' + encodeURIComponent(key) + '=([^;]*)').exec(document.cookie)) ? decode(result[1]) : null;
};

/* ../global/Lowes/prefs.js */

/**
 * @fileOverview Preferences loading/setting via cookie
 */

(function (){
	// Grab Lowes namespace object or create a new one.
	var Lowes = window.Lowes || {};

	/**
	 * Simplified Cookie based preferences for users
	 * @namespace
	 * @memberOf Lowes
	 */
	Lowes.Prefs = {
		/**
		 *	Stores our preferences, key/value pair.
		 *	@type {object}
		 */
		data: { },

		/**
		 * Grabs our preferences cookie (json string) and 
		 * loads the result object into our local object.
		 */
		load: function() {
			var prefsCookie = Lowes.Cookie.get("lowes-prefs");

			if(prefsCookie !== null && typeof prefsCookie !== 'undefined') {
				try {
					this.data = JSON.parse(prefsCookie);
				}catch(e) {
					//error handling
				}
			}
		},

		/**
		 * Saves our preferences, as a JSON string, to our cookie path
		 *
		 * @param {date} expires When cookie should expire.
		 * @param {string} path Path to set the cookie too.
		 */
		save: function(expires, path) {
			// Store our cookie
			Lowes.Cookie.set('lowes-prefs', JSON.stringify(this.data), "");
		}
	};

	/* Expose the updated library to global. */
	window.Lowes = Lowes;
}());

/* ../global/Lowes/utils.js */

/**
 * @fileOverview Miscellaneous Utility functions for Lowe's
 */
(function() {
	// Grab Lowes namespace object or create a new one.
	var Lowes = window.Lowes || {};
	/**
	 * I think this was a good idea at one point, now its just confusing use getLowesPageType() to return the type...
	 * @namespace
	 */
	Lowes.PageTypes = {
		/** Anything but List, Details, or Category */
		Other : 0,
		/** Product Lists */
		List : 1,
		/** Product Detail Page */
		Details : 2,
		/** Product Categories/Sub-Cats */
		Category : 3,
		/** Product Comparison */
		Comparison : 4
	};
	/**
	 * Miscellaneous helper functions
	 * @namespace
	 */
	Lowes.Utils = {

		/**
		 * An alphabet array
		 * @type Array
		 */
		alphabet: ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"],

		/**
		 * Returns a state name from the shortcode.
		 * If you are certain you have an uppercase code, you could do Lowes.Utils.USStatesCodes[code]
		 *
		 * @param {String} code 2-letter state postal code
		 * @returns {String} The state name or false
		 */
		getStateFromCode: function (code){
			if (code && this.USStatesCodes[code.toUpperCase()]){
				return this.USStatesCodes[code.toUpperCase()];
			}
			return false;
		},

		/**
		 * Returns a state name from the shortcode
		 *
		 * @param {String} state 2-letter state postal code
		 * @returns {String|Boolean} The state name or false
		 */
		getCodeFromState: function (state){
			if (state){
				var stateCode;
				$.each( this.USStatesCodes, function(code, name) {
					if (state.toLowerCase() == name.toLowerCase()){ stateCode = code; return false; }
				});
				return stateCode || false;
			}else{
				return false;
			}
		},

		/**
		 * Returns a numeric value from Lowes.PagesTypes that you can check against Lowes.PageTypes
		 *
		 * @returns {Number} PageType id number
		 * @todo Define constants for page types in a more logical manner.
		 */
		getLowesPageType : function (){
			if (document.location.href.indexOf('pc_') !== -1) {
				return Lowes.PageTypes.Category;
			} else if (document.location.href.indexOf('pl_') !== -1) {
				return Lowes.PageTypes.List;
			} else if (document.location.href.indexOf('pd_') !== -1) {
				return Lowes.PageTypes.Details;
			} else if (document.location.href.indexOf('LowesProductComparisionCmd') !== -1) {
				return Lowes.PageTypes.Comparison;
			} else {
				return Lowes.PageTypes.Other;
			}
		},

		/**
		 * Simple random number generator
		 *
		 * @param {Number} from The starting number for range.
		 * @param {Number} to The ending number for the range.
		 */
		randomNumber : function (from, to){
			// Just make sure our from and to are integers.
			from = parseInt(from, 10);
			to = parseInt(to, 10);

			// return a random number between our params.
			return Math.floor(Math.random() * (to - from)) + from;
		},

		/**
		 * Convert URL query string into a native javascript object.
		 *
		 * @param {String} url Defaults to current URL, pass in an href to parse it instead.
		 * @returns {Object} The key/value pairs from the query string as a javascript object
		 */
		urlQueryObject : function (url){
			url = url || window.location.href;
			// Split off our url query string and setup our result object
			var query = (url.indexOf('?') !== -1) ? url.split('?')[1] : url;
			var qryObject = {};
			var i;
			var keyVal;

			if (typeof query == 'undefined') {
				return qryObject;
			}

			//remove the !#
			if (query.indexOf('#!') > -1) {
				query = query.split('#!')[0];
			}

			// Split the query string again to seperate query params.
			query = query.split('&');

			// Loop through our params
			for ( i = 0; i < query.length; i++ ) {
				// Split the query param string into a key/value based of '='
				keyVal = query[i].split('=');

				// Add our key/value to our result object.
				qryObject[keyVal[0]] = keyVal[1];
			}

			// Hand the result object back.
			return qryObject;
		},
		/**
		 * Pass in an Array and it will return you an object.
		 * @param  {array} array the array you want to convert to an object
		 * @return {object}       the returned object
		 */
		arrayToObject: function(array) {
			var rv = {};
			for (var i = 0; i < array.length; ++i)
			if (array[i] !== undefined) rv[i] = array[i];
			return rv;
		},
		/**
		 * Convert a javascript object into a url query string
		 *
		 * @param {Object} object Passed to be converted.
		 * @returns {String} query A formatted URL query string
		 */
		urlQueryString : function (object){
			// Setup our result string.
			var query = "",
				item;
			object = object || {};

			// Loop through object items and convert it to a key/value string.
			for ( item in object) {
				query += "%@=%@&".$$(item, object[item]);
			}

			// Strip off the trailing ampersand and return the value
			return query.substring(0, (query.length - 1));
		},

		/**
		 * An object storing State Code = State Name for the 50 states
		 *
		 * @type Object
		 * @example var NorthCarolina = Lowes.Utils.USStatesCodes['NC'];
		 */
		USStatesCodes : {
			'AL':'Alabama','AK':'Alaska','AZ':'Arizona','AR':'Arkansas','CA':'California','CO':'Colorado','CT':'Connecticut','DE':'Delaware','FL':'Florida','GA':'Georgia','HI':'Hawaii','ID':'Idaho','IL':'Illinois','IN':'Indiana','IA':'Iowa','KS':'Kansas','KY':'Kentucky','LA':'Louisiana','ME':'Maine','MD':'Maryland','MA':'Massachusetts','MI':'Michigan','MN':'Minnesota','MS':'Mississippi','MO':'Missouri','MT':'Montana','NE':'Nebraska','NV':'Nevada','NH':'New Hampshire','NJ':'New Jersey','NM':'New Mexico','NY':'New York','NC':'North Carolina','ND':'North Dakota','OH':'Ohio','OK':'Oklahoma','OR':'Oregon','PA':'Pennsylvania','RI':'Rhode Island','SC':'South Carolina','SD':'South Dakota','TN':'Tennessee','TX':'Texas','UT':'Utah','VT':'Vermont','VA':'Virginia ','WA':'Washington','WV':'West Virginia','WI':'Wisconsin','WY':'Wyoming'
		},

		/**
		 * Adds zeroes in front of a number.
		 *
		 * @param {Number|String} num The number to pad with zeroes
		 * @param {Number} count The desired length of the string - not the number of zeroes.
		 * @example var agentId = Lowes.Utils.zeroPad(7,3);
		 * @returns {String} The resulting number padded with zeroes
		 */
		zeroPad: function (num,count){
			var numZeropad = num.toString();
			while(numZeropad.length < count) {
				numZeropad = "0" + numZeropad;
			}
			return numZeropad;
		},

		/**
		 * Formats a number for US Currency
		 *
		 * @param {Number} n The float to be formatted
		 * @example var currency = Lowes.Utils.formatCurrency(1000);
		 * @returns {String} The number formatted {$xxx,xxx.xx} $1,000.00
		 */
		formatCurrency: function (n){
			if(isNaN(n) || n === null) { return 'http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/$0.00'; }
			var dl = ",",
				amt = parseFloat(n).toFixed(2),
				a = amt.split('.',2),
				d = a[1],
				aa =[], nn,
				sign = '';
			n = parseInt(a[0], 10);
			if(n < 0) { sign = '-'; }
			n = String(Math.abs(n));
			while(n.length > 3) {
				nn = n.substr(n.length-3);
				aa.unshift(nn);
				n = n.substr(0,n.length-3);
			}
			if(n.length) { aa.unshift(n); }
			n = aa.join(dl);
			amt = (d.length < 1) ? n : n + '.' + d;
			return "$"+sign+amt;
		},
		/**
		 * Removes US Currency formatting
		 *
		 * @param {Number} num The number to be formatted
		 * @example var total = Lowes.Utils.removeCurrency('http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/$1,000.00');
		 * @returns {String} The number unformatted {xxxxxx.xx} 1000.00
		 */
		removeCurrency: function (num){
			var n = num.replace(/\$|,/g,'');
			if(isNaN(n) || n === null) { return 0; }
			return n;
		},
		/**
		 * Returns click event for ipad if applicable
		 *
		 * @returns {String} touchstart or click
		 */
		getClickTypeForTouch: function() {
			var ua = navigator.userAgent,
				event = (ua.match(/iPad/i)) ? "touchstart" : "click";
			return event;
		},
		/**
		 * createMbox makes it easy to inject an mbox into a page
		 * @param mBoxName {string} the name of the mbox
		 * @param parent {function} the jQuery wrapper set you're trying to get
		 * @param method {string} the method you want to use to inject the mbox ("append", "prepend", etc)
		 * @param id {string} the id of the mbox div if it's different from the mbox name (deprecated)
		 * @example Lowes.Utils.createMbox('i070_product_feedback', $('#description-tab'), "prepend", 'i070_product_feedback');
		 */
		createMbox: function(mBoxName, parent, method, id) {
			id = id || mBoxName;
			parent[method]('<div id="' + id + '" />');
			mboxDefine(id, mBoxName);
			mboxUpdate(mBoxName);
		},

		/**
		* formKeyHandler handles phone field masking to create a better phone input user experience
		* @param e {string} is the keyup event being passed
		*/
		formKeyHandler: function(e) {
			var $input = $(this),
				digits = $input.val().replace(/[^\d]/g, '');

			if ($input.val() == "(" || e.which == 8) return;

			if (!digits.length) {
				return $input.val("");
			}

			if (digits.length < 3) {
			  return $input.val('(' + digits);
			}

			if (digits.length == 3) {
			  return $input.val('(' + digits + ') ');
			}

			if ((digits.length > 3 && digits.length < 6)) {
				var areaCode = digits.substring(0, 3);
				var rest = digits.substring(3, digits.length);
				return $input.val('(' + areaCode + ') ' + rest);
			}

			if (digits.length == 6) {
				var areaCode = digits.substring(0, 3);
				var rest = digits.substring(3, digits.length);
				return $input.val('(' + areaCode + ') ' + rest + '-');
			}

			if (digits.length > 6 && digits.length <= 10) {
				var areaCode = digits.substring(0, 3);
				var prefix = digits.substring(3, 6);
				var rest = digits.substring(6, digits.length)
				return $input.val('(' + areaCode + ') ' + prefix + '-' + rest);
			}
		},
		/**
		 * Check for HTML5 Storage
		 * @example Lowes.Utils.supports_html5_storage();
		 */
		supports_html5_storage: function () {
			try {
				return 'localStorage' in window && window.localStorage !== null;
			} catch (e) {
				return false;
			}
		}
	};

	/* Expose the updated library to global. */
	window.Lowes = Lowes;
}());

/* ../global/Lowes/user.js */

/**
 * @fileOverview Methods for dealing with the currently browsing user.
 */

(function(){
	// Grab Lowes namespace object or create a new one.
	var Lowes = window.Lowes || {};

	/**
	 *	Easily set/retrieve info about the current site user.
	 *	Users, this is where you learn what we track.
	 *
	 *	@namespace
	 *	@memberOf Lowes
	 */
	Lowes.User = {
		// We need to have the init here because bazzarvoice actually calls the function, so if it's removed,
		// it will break stuff.
		init: function (){
			/* Cache the properties via their public methods. */
			/** Cached User Details */
			Lowes.User.Details = $.parseJSON(unescape($.cookie('userDetails'))) || {}; // Cache Object for performance.
			/** Cached Current Store */
			Lowes.User.CurrentStore = Lowes.User.getCurrentStore(); // Cache Object for performance.
		},

		/**
		 * Signed in status check based on firstName cookie.
		 *
		 * @returns Boolean Whether or not the user is signed in.
		 */
		isSignedIn: function (){
			return !!$.cookie('firstName');
		},

		/**
		 * Zipped-in status check based on selectedStore1 cookie.
		 *
		 * @returns Boolean Whether or not the user is zipped in.
		 */
		isZippedIn: function (){
			return !!$.cookie('selectedStore1');
		},

		/**
		 * Read and parse the user's current store based on the selectedStore1 cookie.
		 *
		 * @returns Object The current store info.
		 */
		getCurrentStore: function (){
			var thisStore = {};
			if (this.isZippedIn()){
				// We can now be sure that there is a cookie.
				var currentStore = $.cookie('selectedStore1').split('|'),
					currentStoreData = currentStore[0].split('##');
				/* Array Values */
				thisStore.shortName = currentStoreData[0];
				thisStore.state = $.trim(currentStoreData[1]);
				thisStore.zip = currentStore[3];
				thisStore.number = currentStore[1];
				thisStore.address = currentStore[6];
				thisStore.city = currentStore[7];
				thisStore.hours = currentStore[8];
				thisStore.phone = currentStore[9];
				thisStore.fax = currentStore[10];
				thisStore.patchCode = currentStore[11];
				thisStore.cbcPhone = currentStore[12];

				/* Extra */
				thisStore.name = thisStore.shortName + ", " + thisStore.state;
				thisStore.postalAddress = thisStore.address + " " + thisStore.city + ", "+ thisStore.state;
			}
			return thisStore;
		},

		/**
		 * Check for segmented Home Profile user based on Cookie.
		 * @returns Boolean True if user has Home Profile Cookie segment, False if user has no Profile Cookie segment.
		 */
		isHomeProfileUser: function (){
			 return ( $.isArray(Lowes.User.Details.authorities) ? ($.inArray('ROLE_ML_HOME_PROFILE',Lowes.User.Details.authorities) > -1 ? true : false) : false );
		},
		/**
		 * Check for Employee Cookie.
		 * @returns Boolean True if user has Employee Cookie, False if user has no Employee Cookie.
		 */
		isEmployee: function (){
			 return ($.cookie('userSegment') == "Employee" ? true : false );
		}

	};


	/* Expose the updated library to global. */
	window.Lowes = Lowes;
}());
Lowes.User.init();

/* ../global/Lowes/UI/ui.js */

/**
 * @fileOverview UI namespace and UI.Core class
 */

(function($){
	// Grab Lowes namespace object or create a new one.
	var Lowes = window.Lowes || {};

	/**
	 * Lowes.UI Namespace Object that will be the parent of all Lowes UI components library classes.
	 * @memberOf Lowes
	 * @namespace
	 * @deprecated Use jQuery UI
	 */
	Lowes.UI = Lowes.UI || {};

	// Silly I know, just some consts for position.
	Lowes.UI.position = {
		TOP: 0,
		BOTTOM: 1,
		LEFT: 2,
		RIGHT: 3
	};

	Lowes.UI.Core = Class.extend(/** @lends Lowes.UI.Core# */{
		/**
		 * Base class that will contain common methods
		 * used across all UI components.
		 * @class
		 * @constructs
		 */
		init: function (){}
	});

	window.Lowes = Lowes;
}(jQuery));

/* ../global/Lowes/UI/carousel.js */

/** @fileOverview Lowes Carousel UI Component */
(function ($){
	// Grab Lowes namespace object or create a new one.
	var Lowes = window.Lowes || {};

	// Grab Lowes UI namespace object or create a new one.
	Lowes.UI = Lowes.UI || {};

	// Quick check to make sure our UI.Core class is there
	// to extend or create a new one.
	Lowes.UI.Core = Lowes.UI.Core || Class.extend({});

	var Carousel = Lowes.UI.Core.extend(/** @lends Lowes.UI.Carousel# */{
		/**
		 * Instantiate the Carousel on a container
		 * @memberOf Lowes.UI
		 * @extends Lowes.UI.Core
		 * @param {String} container
		 * @param {Object} options
		 * @constructs
		 */
		init: function (container, options) {
			if(!Lowes.DEBUG && !$(container).length) { return; }
			// Check to make sure we have a container param.
			if(typeof container !== 'string' || typeof container === 'undefined') {
				throw "No container element (selector) provided to create Carousel";
			}else {
				this.container = container;
			}
			// Check to make sure that if options were passed, they are objects.
			if(typeof options !== 'undefined' && typeof options !== 'object'){
				throw "Carousel options param must be an object";
			}
			options = options || {};
			this.container			 = container				 || '.horizontal_carousel';
			this.prev				 = options.prev				 || '%@ > .prev'.$$(this.container);
			this.next				 = options.next				 || '%@ > .next'.$$(this.container);
			this.carousel			 = options.carousel			 || '.carousel';
			this.visible			 = options.visible			 || 4;
			this.speed				 = options.speed			 || 1000;
			this.scroll				 = options.scroll			 || 1;
			this.controlBar			 = options.controlBar		 || false;
			this.controlBarMode		 = options.controlBarMode	 || 'status'; //only show status, or allow movement
			this.orientation		 = options.orientation		 || 'horizontal';
			this.callback			 = options.callback			 || function () {};
			this.easing				 = options.easing			 || 'swing';
			this.fireEvent			 = options.event			 || 'click';
			this.loop				 = (options.loop !== false) ? true : false;
			this.position			 = 0;
		},

		create:function (){
			var maskDiv = document.createElement('div'), //create mask for the viewport
				$mask,
				numberOfControls,
				control,
				$controlContainer,
				upperLimit,
				low,
				high,
				i,
				j,
				$el,
				$prepend,
				$append;

			//container will hold carousel and control bar, possibly the prev/next arrows, too
			this.$container = $(this.container);

			//get the selector that represents the carousel items (usually a <ul>)
			this.$carousel = $('%@ > %@'.$$(this.container, this.carousel));

			this.$carouselItems = this.$carousel.children();

			//wrap mask around carousel items
			this.$carousel.wrap(maskDiv);

			//if # of items < # visible, don't show controlbar
			if(this.$carouselItems.length <= this.visible) {
				this.controlBar = false;
			}

			$mask = this.$carousel.parent();

			if(this.controlBar && this.orientation == 'horizontal') {
				numberOfControls = Math.ceil((this.$carouselItems.length)/this.visible);

				$controlContainer = $(document.createElement('div')).addClass('control_bar');

				upperLimit = this.$carouselItems.length - this.visible;
				//generates elements and creates map
				for(i=0; i<numberOfControls; i++) {
					control = document.createElement('a');
					control.setAttribute('href', '#control'+i);
					$controlContainer.append(control);

					low = i * this.visible;

					//check to see if the low position is outside the upperLimit
					if(low > upperLimit) {
						low = low - (low - upperLimit);
					}

					high = low + this.visible - 1;
					for(j=low; j<=high; j++) {
						$el = $(this.$carouselItems.get(j));
						$el.addClass('control_'+i);
						if($el.hasClass('control_%@'.$$(i-1))) {
							//don't allow multiple control classes
							$el.removeClass('control_%@'.$$(i));
						}
					}
				}

				this.$controls = $controlContainer.find('a');
				this.$controls.filter(':first').addClass('active');
				if(this.controlBarMode != 'status') {
					this.$controls.live('click', this.__bindControlBarEvent.expose(this));
				}
				//else, mode is 'status', show just use controlbar to show status

				this.$container.append($controlContainer);
			}

			if(this.loop){
				$prepend = this.$carouselItems.slice($carouselItems.length-this.visible).clone();
				$append = this.$carouselItems.slice(0, this.visible).clone();
				this.$carousel.prepend($prepend).append($append);

				//make sure the selectorChildren variable reflects recent changes
				this.$carouselItems = this.$carousel.children();

				//position must compensate for prepended elements
				this.position += $prepend.length;
			}

			this.upperLimit = this.$carouselItems.length - this.visible;

			//setup the appropriate orientation and add width or height values to the Carousel object
			if(this.orientation == 'horizontal') {
				//horizontal
				this.childOuterWidth = this.$carouselItems.filter(':first').outerWidth(true);
				this.childWidth = this.$carouselItems.filter(':first').width();

				this.totalWidth = (this.$carouselItems.length * this.childOuterWidth);
				this.visibleWidth = (this.childOuterWidth * this.visible);

				//set carousel CSS rules first, order matters here
				this.$carousel.css({ 'position': 'absolute', 'left': -(this.position * this.childOuterWidth), 'width': this.totalWidth });

				$mask.css({ 'position': 'relative', 'width': this.visibleWidth, 'height': this.$carousel.height(), 'overflow': 'hidden', 'float': 'left', 'display': 'inline' });

			} else {
				//vertical
				this.childOuterHeight = this.$carouselItems.filter(':first').outerHeight(true);
				this.childHeight = this.$carouselItems.filter(':first').height();

				this.totalHeight = this.$carouselItems.length * this.childOuterHeight;
				this.visibleHeight = this.childOuterHeight * this.visible;

				//set carousel CSS rules first, order matters here
				this.$carousel.css({ 'position': 'absolute', 'top': -(this.position * this.childOuterHeight), 'height': this.totalHeight });

				$mask.css({ 'position': 'relative', 'height': this.visibleHeight, 'overflow': 'hidden' });
			}
			this.$prevButton = $(this.prev);
			this.$nextButton = $(this.next);

			if (this.$carouselItems.length > this.visible) {
				this.$prevButton.bind(this.fireEvent, this.__bindDirectionEvent.expose(this));
				this.$nextButton.bind(this.fireEvent, this.__bindDirectionEvent.expose(this));
			}
			this.updateArrows();
		},

		__bindDirectionEvent: function (event){
			event.preventDefault();
			var adjustment,
				divisor,
				newPosition;

			//prevent animation build up the lazy man's way
			if(this.$carousel.is(':animated')) { return; }

			//determine the direction it should scroll based on class of button that fired event
			newPosition = ($(event.currentTarget).hasClass('prev')) ? this.position - this.scroll : this.position + this.scroll;

			//if not looping and the next position to scroll to would put the carousel out of visible range, return
			if(!this.loop && (newPosition < 0 || newPosition > this.upperLimit)) {
				if (newPosition < 0){
					newPosition = 0;
				}else if (newPosition > this.upperLimit){
					newPosition = this.upperLimit;
				}
			}

			//if looping and the carousel would go out of visible range, adjust the position before scrolling
			if(this.loop && (newPosition < 0 || newPosition > this.upperLimit)) {
				divisor = (this.$carouselItems.length - (this.visible * 2));
				adjustment = (this.position < divisor) ? this.position + divisor : this.position - divisor;
				newPosition = (newPosition >= 0) ? (newPosition % divisor) : newPosition + divisor;

				if(this.orientation == 'horizontal') {
					this.$carousel.css('left', -(adjustment * this.childOuterWidth));
				}else{
					this.$carousel.css('top', -(adjustment * this.childOuterHeight));
				}
			}

			this.spinCarousel(newPosition);
		},

		__bindControlBarEvent: function (event){
			event.preventDefault();
			var index = $(event.currentTarget).attr('href').slice(8),
				newPosition = parseInt(index, 10) * this.visible;

			if(this.loop) {
				//compensate for prepended/appended items
				newPosition += this.visible;
			}

			//make sure the newPosition wouldn't be outside of the visible limit
			if(newPosition > this.upperLimit) {
				newPosition = newPosition - (newPosition - this.upperLimit);
			}

			this.spinCarousel(newPosition);
		},

		__onSlideEndCallback: function (event){
			var controlNumber,
				$currentControl;
			if(this.controlBar) {
				controlNumber = Math.ceil(this.position/this.visible);
				$currentControl = this.$controls.filter('[href=#control'+controlNumber+']');
				this.$controls.filter('.active').removeClass('active');
				$currentControl.addClass('active');
			}

			this.updateArrows();
		},

		/** Update the next/prev arrows */
		updateArrows: function (){
			//if at the beginning or end and "loop" is false, set arrow to disabled
			if(!this.loop) {
				/*if(this.$carouselItems.length <= this.visible)
				{
					this.$prevButton.css('visibility', 'hidden');
					this.$nextButton.css('visibility', 'hidden');
				}*/

				//disable prev arrow
				if(this.position == 0) {
					this.$prevButton.addClass('disabled prev_disabled');
				} else {
					this.$prevButton.removeClass('disabled prev_disabled');
				}

				if(this.position == this.upperLimit || this.$carouselItems.length <= this.visible) {
					this.$nextButton.addClass('disabled next_disabled');
				} else {
					this.$nextButton.removeClass('disabled next_disabled');
				}

			}
		},

		/**
		 * Spin the Carousel
		 *
		 * @param {Number} newPosition The offset position to move the slide to
		 */
		spinCarousel: function (newPosition) {
			this.position = newPosition;

			if(this.orientation == 'horizontal'){
				this.$carousel.animate({ 'left': -(this.childOuterWidth * newPosition) }, this.speed, this.easing, this.__onSlideEndCallback.expose(this));
			}else{
				this.$carousel.animate({ 'top': -(this.childOuterHeight * newPosition) }, this.speed, this.easing, this.__onSlideEndCallback.expose(this));
			}
		}
	});

	// Add Carousel to our UI Namesapce
	Lowes.UI.Carousel = Carousel;

	// Expose to the world.
	window.Lowes = Lowes;
}(jQuery));

/* ../global/Lowes/UI/modal.js */

/** @fileOverview Lowes Modal UI Component */

(function($){
	// Grab Lowes namespace object or create a new one.
	var Lowes = window.Lowes || {};

	// Grab Lowes UI namespace object or create a new one.
	Lowes.UI = Lowes.UI || {};

	// Quick check to make sure our UI.Core class is there
	// to extend or create a new one.
	Lowes.UI.Core = Lowes.UI.Core || Class.extend({});

	var Modal = Lowes.UI.Core.extend(/** @lends Lowes.UI.Modal# */{

		/**
		 * Initialize the Modal object and setup options
		 *
		 * @constructs
		 * @deprecated Use jQuery UI Dialog
		 * @param {Object} Options to use when setting up the component.
		 */
		init: function(options) {
			// Grab some options, or default to empty.
			var options		= options || {};

			// Setup our options, with defaults if needed.
			this.backdrop	= (typeof options.backdrop === 'undefined')? true : options.backdrop;
			this.effect		= options.effect	 || 'fade';
			this.url		= options.url		 || false;
			this.useHeight	= options.height	 || '300px';
			this.useWidth	= options.width		 || '300px';
			this.background = options.background || 'white';
			this.closeText	= options.closeText	 || 'X';
			this.open		= options.open		 || false;
			this.callback	= options.callback	 || function() {};
			this.speed		= options.speed		 || 'fast';
			this.content	= options.load		 || false;
			this.attach		= (typeof options.attach !== "undefined")? options.attach : false;
			this.classes	= options.classes || [];
			this.modalId   = options.id || "lowes-modal-id-element";

			// Regardes of classes the user wants, let's
			// push or own class on the end to make sure
			// we can get to it.
			this.classes.push('lowes-ui-modal');

			// Build modal Id
			//this.modalId = "lowes-modal-id-element";


			// Let's make a modal.
			this.__modal();

			// If they want a backdrop, GIVE IT TO THEM!
			if(this.backdrop)
				this.__backdrop();

			// Open on instatiation?!
			if(this.open)
				this.show();

			// If a content object is provided, let's load it up.
			if(this.content)
				this.load(this.content);
		},

		/**
		 * When called, show the modal that has been created.
		 * @param {Function} callback The function to execute when shown
		 */
		show: function (callback){
			var showCallback = callback || function (){};

			// If they want a backdrop
			if(this.backdrop) {
				// let's make the backdrop appear, then throw the modal on top of it.
				$(this.backdropElement)[this.__effectBuilder(true)](this.speed, function(){
					// Show the modal.
					$(this.modalBox)[this.__effectBuilder(true)](this.speed, function(){
						  showCallback();
					});
				}.expose(this));
			} else {
				// No backdrop, just show the modal.
				$(this.modalBox)[this.__effectBuilder(true)](this.speed);
			}
		},

		/**
		 * Load the desired content into the content area of the current modal
		 *
		 * @param {Object} loadObject Sets type and content to use.  Can only have a length of 1.
		 */
		load: function (loadObject){
			// Make sure our object is there and it's only 1 entity.
			/*
			if(typeof loadObject == undefined && Object.size(loadObject) > 1)
				throw "Object provided for load method, must have a count of 1";

			alert(typeof loadObject.html);

			// Hack? Grab the key and run the correct content process method.
			if(typeof loadObject.ajax != undefined)
				this.__loadAjax(loadObject.ajax);
			else if(typeof loadObject.text != undefined)
				this.__loadText(loadObject.text);
			else if(typeof loadObject.html != undefined)
				this.__loadHtml(loadObject.html);
			else
				throw "No valid load type provided. (choices: ajax, text, html)";
			*/
			// Hack: Quick Win/Fix
			this.__loadHtml(loadObject);
		},

		/**
		 * If attach option is given, build the proper position for our modal.
		 *
		 * @private
		*/
		__attach: function (){
			// Grab the element ot attach to and it's attributes.
			var attachTo = $(this.attach.element);
			var position = attachTo.offset();
			var width = attachTo.width();
			var height = attachTo.height();

			// Container object for return.
			var usePosition = {};

			// Where do you want it? Default to bottom.
			switch(position) {
				case Lowes.UI.position.TOP:
					// Set top of our modal to our attachment minus it's height (move us up).
					usePosition.top = position.top - height;
					// Line it up.
					usePosition.left = position.left;
				break;

				default:
				case Lowes.UI.position.BOTTOM:
					// Set our modal's position right below our attachement
					usePosition.top = position.top + height;
					// Line it up
					usePosition.left = position.left;
				break;
			}

			// return our position object.
			return usePosition;
		},

		/**
		 * Load the content area with the html passed.
		 *
		 * @private
		 * @param {String} html String containing HTML.
		 */
		__loadHtml: function (html){
			// Just set our html, old skewl.
			$(this.contentArea).html(html);
		},

		/**
		 * Add provided text to the content area of the modal.
		 *
		 * @private
		 * @param {String} text The text to use in the content area.
		 */
		__loadText: function (text){
			// Clear out our Content area.
			this.contentArea.innerHTML = "";

			// Create a nice text node and stick it in our content area.
			this.contentArea.appendChild(document.createTextNode(text));
		},

		__loadAjax: function (url){
			// Clear out our Content area.
			this.contentArea.innerHTML = "";

			// Because we want our content when our modal shows. We use async
			// to stop everything until we get what we want.
			// Then set our innerHTML to the response (hack?).
			this.contentArea.innerHTML = $.ajax({
						type: 'get',
						url: url,
						async: false
					}).responseText;
		},

		/**
		 * Just handles setting up which effect we want to use to show and hide our modal
		 *
		 * @private
		 * @param {Boolean} display Show it or not?
		 */
		__effectBuilder: function (display){
			// Make sure we are given accepted effects, otherwise let us know.
			if(this.effect != 'fade' && this.effect != 'slide')
				throw "Only Fade and Slide effect is supported";

			// return the proper effect with direction based off BOOL display param.
			return (this.effect == 'fade')? ((display)? 'fadeIn'	: 'fadeOut') : ((display)? 'slideDown'	: 'slideUp');
		},

		/**
		 * Build our model on the fly or use one if it's there.
		 *
		 * @private
		 */
		__modal: function (){
			// Predefine our modal variable.
			var modal = null;

			// If we already have a modal hiding in the dom, use that.
			if( $('.lowes-ui-modal').length){
				modal = $('.lowes-ui-modal')[0];
			} else {
				// If not, let's build one
				// Create our modal element and set it's id and classes.
				modal = document.createElement('div');
				modal.setAttribute('id', this.modalId);
				modal.setAttribute('class', this.classes.join(" "));

				// Style our modal to ensure positioning and visibility.
				$(modal).css({
					zIndex:999,
					position:'absolute',
					background:this.background,
					width:this.useWidth,
					height:this.useHeight,
					display: 'none'
				});

				var left;
				var top;

				// Are we creating attachment issues?
				if(this.attach != false) {
					// Call our private method to give us our position.
					var position = this.__attach();

					// Position our modal.
					$(modal).css({
						top:position.top,
						left:position.left
					});
				} else {
					// No attachment issues here....
					// grab the int value of the users desired width and height.
					left  = parseInt(this.useWidth.replace(/px$/, ''));
					top	  = parseInt(this.useHeight.replace(/px$/, ''));

					// Divde them up so we can position this thing, center screen.
					left  = "-%@px".$$(parseInt((left / 2)));
					top	  = "-%@px".$$(parseInt((top / 2)));

					// Set our centered position.
					$(modal).css({
						top:'50%',
						left:'50%',
						margin:"%@ 0 0 %@".$$(top, left)
					})
				}


				// Expose the modal to our current context.
				this.modalBox = modal;

				// Build our exit button/link.
				this.__exit();

				// Add our Content area.
				this.__contentArea();

				// Add the modal to our current DOM.
				document.body.appendChild(this.modalBox);

				// Bind our little exit deal with our close method.
				$('.lowes-ui-modal-exit').live('click', this.close.expose(this));
			}
		},

		/**
		 * Closes our Modal (and backdrop).
		 *
		 * @param {Object} evt The event object that fired, causing the close, or default to current context's close object.
		 */
		close: function (evt){
			// If there is no event object, just create an empty object.
			var evt = evt || {};

			// Grab our Exit link, either way.	This is our reference point for finding the modal.

			var aTarget = $(this.exitA);

			// If we have a backdrop
			if(this.backdrop) {
				// Get rid our modal, then fade out our backdrop. If there is a callback, fire it.
				aTarget.parent('div').parent('div')[this.__effectBuilder(false)](this.speed, function(){
					$(this.backdropElement)[this.__effectBuilder(false)](this.speed, this.callback);
				}.expose(this));
			} else {
			   /*
			   if(aTarget.length > 0)
				   // Get rid of our modal and fire the callback.
				   aTarget.parent('div').parent('div')[this.__effectBuilder(false)](this.speed, this.callback);
				else
				   $('.lowes-ui-modal')[this.__effectBuilder(false)](this.speed, this.callback);
				*/

				$("#%@".$$(this.modalId))[this.__effectBuilder(false)](this.speed, this.callback);
			}


		},

		/**
		 * Completely remove the backdrop and modal from the DOM
		 */
		destroy: function (){
			document.body.removeChild(this.modalBox);
			document.body.removeChild(this.backdropElement);
		},

		/**
		 * Create our content area for our Modal.
		 *
		 * @private
		 */
		__contentArea: function (){
			// Create our container element
			var content = document.createElement('div');

			// expose it to our current context.
			this.contentArea = content;

			// Add our default class.
			this.contentArea.setAttribute('class', 'lowes-ui-modal-content');

			// Append it to our modal.
			this.modalBox.appendChild(this.contentArea);
		},

		/**
		 * Create our way out of this place.  Exit element and container.
		 *
		 * @private
		 */
		__exit: function (){
		   /*
			// Create our container for the link.
			var exit = document.createElement('div');
			exit.setAttribute('class', 'lowes-ui-modal-exit-container');

			// Create our link
			var a = document.createElement('a');

			// Set our classes.
			a.setAttribute("class", "lowes-ui-modal-exit");

			// Do this for Chad.
			a.setAttribute("href", "javascript:;");

			//Set the content of our exit with provided copy and/or html.
			a.innerHTML = this.closeText;

			// Set our must have styles.
			$(exit).css({
				marginRight:'2px',
				marginTop:'2px',
				textAlign:'right'
			});

			// Expose to our context.
			this.exitA = a;

			// Add our link to our exit container
			exit.appendChild(a);

			// Add our exit container to our modal.
			this.modalBox.appendChild(exit);
			*/
		},

		/**
		 * Creates a backdrop/overlay for the modal to be display on top of.
		 *
		 * @private
		 */
		__backdrop: function (){
			// Create our element.
			var div = document.createElement('div');
			div.setAttribute('id', 'lowes-modal-id-element-backdrop');

			// Setup our must have styles.
			$(div).css({
			   zIndex:998,
				position:'absolute',
				top:0,
				left:0,
				right:0,
				bottom:0,
				height: $(document).height() /*"100%"*/,
				width:"100%",
				background:'black', // Could be an option later
				opacity:0.6, // Could be an option later.
				display:"none"
			});

			// Add our backdrop to our current context
			this.backdropElement = div;

			// And throw it in the DOM.
			document.body.appendChild(div);
		}
	});

	// Add Modal to our UI Namesapce
	Lowes.UI.Modal = Modal;

	// Expose to the world.
	window.Lowes = Lowes;

})(jQuery);

/* ../global/Lowes/UI/dynamic_details.js */

/*This section is for the HTMLBanner and Hero Templates */
/*
 * jScrollPane - v2.0.0beta9 - 2011-01-31
 * http://jscrollpane.kelvinluck.com/
 *
 * Copyright (c) 2010 Kelvin Luck
 * Dual licensed under the MIT and GPL licenses.
 */
(function ($,window,undefined){
	$.fn.jScrollPane = function (settings)
	{
		/* JScrollPane "class" - public methods are available through $('selector').data('jsp')*/
		function JScrollPane(elem, s)
		{
			var settings, jsp = this, pane, paneWidth, paneHeight, container, contentWidth, contentHeight,
				percentInViewH, percentInViewV, isScrollableV, isScrollableH, verticalDrag, dragMaxY,
				verticalDragPosition, horizontalDrag, dragMaxX, horizontalDragPosition,
				verticalBar, verticalTrack, scrollbarWidth, verticalTrackHeight, verticalDragHeight, arrowUp, arrowDown,
				horizontalBar, horizontalTrack, horizontalTrackWidth, horizontalDragWidth, arrowLeft, arrowRight,
				reinitialiseInterval, originalPadding, originalPaddingTotalWidth, previousContentWidth,
				wasAtTop = true, wasAtLeft = true, wasAtBottom = false, wasAtRight = false,
				originalElement = elem.clone().empty(),
				mwEvent = $.fn.mwheelIntent ? 'http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/mwheelIntent.jsp' : 'http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/mousewheel.jsp';
			originalPadding = elem.css('paddingTop') + ' ' +
								elem.css('paddingRight') + ' ' +
								elem.css('paddingBottom') + ' ' +
								elem.css('paddingLeft');
			originalPaddingTotalWidth = (parseInt(elem.css('paddingLeft'), 10) || 0) +
										(parseInt(elem.css('paddingRight'), 10) || 0);
			function initialise(s)
			{
				var clonedElem, tempWrapper, isMaintainingPositon, lastContentX, lastContentY,
						hasContainingSpaceChanged, originalScrollTop, originalScrollLeft;
				settings = s;
				if (pane === undefined) {
					originalScrollTop = elem.scrollTop();

					elem.css(
						{
							overflow: 'hidden',
							padding: 0
						}
					);
					/* TODO: Deal with where width/ height is 0 as it probably means the element is hidden and we should
					 come back to it later and check once it is unhidden...*/
					paneWidth = elem.innerWidth() + originalPaddingTotalWidth;
					paneHeight = elem.innerHeight();
					elem.width(paneWidth);

					pane = $('<div class="jspPane" />').css('padding', originalPadding).append(elem.children());
					container = $('<div class="jspContainer" />')
						.css({
							'width': paneWidth + 'px',
							'height': paneHeight + 'px'
						}
					).append(pane).appendTo(elem);
				} else {
					elem.css('width', '');
					hasContainingSpaceChanged = elem.innerWidth() + originalPaddingTotalWidth != paneWidth || elem.outerHeight() != paneHeight;
					if (hasContainingSpaceChanged) {
						paneWidth = elem.innerWidth() + originalPaddingTotalWidth;
						paneHeight = elem.innerHeight();
						container.css({
							width: paneWidth + 'px',
							height: paneHeight + 'px'
						});
					}
					/*If nothing changed since last check...*/
					if (!hasContainingSpaceChanged && previousContentWidth == contentWidth && pane.outerHeight() == contentHeight) {
						elem.width(paneWidth);
						return;
					}
					previousContentWidth = contentWidth;

					pane.css('width', '');
					elem.width(paneWidth);
					container.find('>.jspVerticalBar'/*,>.jspHorizontalBar'*/).remove().end();
				}
				/* Unfortunately it isn't that easy to find out the width of the element as it will always report the
				 width as allowed by its container, regardless of overflow settings.
				 A cunning workaround is to clone the element, set its position to absolute and place it in a narrow
				 container. Now it will push outwards to its maxium real width...*/
				clonedElem = pane.clone().css('position', 'absolute');
				tempWrapper = $('<div style="width:1px; position: relative;" />').append(clonedElem);
				$('body').append(tempWrapper);
				contentWidth = Math.max(pane.outerWidth(), clonedElem.outerWidth());
				tempWrapper.remove();

				contentHeight = pane.outerHeight();
				percentInViewH = contentWidth / paneWidth;
				percentInViewV = contentHeight / paneHeight;
				isScrollableV = percentInViewV > 1;

				if (!(isScrollableV)) {
					elem.removeClass('jspScrollable');
					pane.css({
						top: 0,
						width: container.width() - originalPaddingTotalWidth
					});
					removeMousewheel();
					removeFocusHandler();
					removeKeyboardNav();
					removeClickOnTrack();
				} else {
					elem.addClass('jspScrollable');
					isMaintainingPositon = settings.maintainPosition && (verticalDragPosition);
					if (isMaintainingPositon) {
						lastContentY = contentPositionY();
					}
					initialiseVerticalScroll();

					resizeScrollbars();
					/*if (isMaintainingPositon) {
						//scrollToX(lastContentX, false);
						scrollToY(lastContentY, false);
					}*/
					initFocusHandler();
					initMousewheel();
					initTouch();

					if (settings.enableKeyboardNavigation) {
						initKeyboardNav();
					}
					if (settings.clickOnTrack) {
						initClickOnTrack();
					}

				}
				/* Handy for populating pane via ajax */
				if (settings.autoReinitialise && !reinitialiseInterval) {
					reinitialiseInterval = setInterval(
						function ()
						{
							initialise(settings);
						},
						settings.autoReinitialiseDelay
					);
				} else if (!settings.autoReinitialise && reinitialiseInterval) {
					clearInterval(reinitialiseInterval);
				}
				originalScrollTop && elem.scrollTop(0) && scrollToY(originalScrollTop, false);
				elem.trigger('jsp-initialised', [isScrollableV]);
			}
			function initialiseVerticalScroll()
			{
				if (isScrollableV) {
					container.append(
						$('<div class="jspVerticalBar" />').append(
							$('<div class="jspCap jspCapTop" />'),
							$('<div class="jspTrack" />').append(
								$('<div class="jspDrag" />').append(
									$('<div class="jspDragTop" />'),
									$('<div class="jspDragBottom" />')
								)
							),
							$('<div class="jspCap jspCapBottom" />')
						)
					);
					verticalBar = container.find('>.jspVerticalBar');
					verticalTrack = verticalBar.find('>.jspTrack');
					verticalDrag = verticalTrack.find('>.jspDrag');
					verticalTrackHeight = paneHeight;
					container.find('>.jspVerticalBar>.jspCap:visible,>.jspVerticalBar>.jspArrow').each(
						function ()
						{
							verticalTrackHeight -= $(this).outerHeight();
						}
					);
					verticalDrag.hover(
						function ()
						{
							verticalDrag.addClass('jspHover');
						},
						function ()
						{
							verticalDrag.removeClass('jspHover');
						}
					).bind(
						'http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/mousedown.jsp',
						function (e)
						{
							/* Stop IE from allowing text selection */
							$('html').bind('http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/dragstart.jsp selectstart.jsp', nil);
							verticalDrag.addClass('jspActive'); /* This is leveragable */
							var startY = e.pageY - verticalDrag.position().top;
							$('html').bind(
								'http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/mousemove.jsp',
								function (e)
								{
									positionDragY(e.pageY - startY, false);
								}
							).bind('http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/mouseup.jsp mouseleave.jsp', cancelDrag);
							return false;
						}
					);
					sizeVerticalScrollbar();
				}
			}
			function sizeVerticalScrollbar()
			{
				verticalTrack.height(verticalTrackHeight + 'px');
				verticalDragPosition = 0;
				scrollbarWidth = settings.verticalGutter + verticalTrack.outerWidth();
				/* Make the pane thinner to allow for the vertical scrollbar */
				pane.width(paneWidth - scrollbarWidth - originalPaddingTotalWidth);
				/* Add margin to the left of the pane if scrollbars are on that side (to position
				 the scrollbar on the left or right set it's left or right property in CSS) */
				if (verticalBar.position().left === 0) {
					pane.css('margin-left', scrollbarWidth + 'px');
				}
			}
			function resizeScrollbars()
			{
				if (isScrollableV) {
					var verticalTrackWidth = verticalTrack.outerWidth();

					paneHeight -= verticalTrackWidth;

					sizeVerticalScrollbar();

				}
				/* reflow content */
				contentHeight = pane.outerHeight();
				percentInViewV = contentHeight / paneHeight;
				/* sets the size of the scrollbar pill */
				if (isScrollableV) {
					verticalDragHeight = Math.ceil(1 / percentInViewV * verticalTrackHeight);
					if (verticalDragHeight > settings.verticalDragMaxHeight) {
						verticalDragHeight = settings.verticalDragMaxHeight;
					} else if (verticalDragHeight < settings.verticalDragMinHeight) {
						verticalDragHeight = settings.verticalDragMinHeight;
					}
					verticalDrag.height(verticalDragHeight + 'px');
					dragMaxY = verticalTrackHeight - verticalDragHeight;
					_positionDragY(verticalDragPosition);  /* To update the state for the arrow buttons */
				}
			}
			function initClickOnTrack()
			{
				removeClickOnTrack();
				if (isScrollableV) {
					verticalTrack.bind(
						'http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/mousedown.jsp',
						function (e)
						{
							if (e.originalTarget === undefined || e.originalTarget == e.currentTarget) {
								var clickedTrack = $(this),
									offset = clickedTrack.offset(),
									direction = e.pageY - offset.top - verticalDragPosition,
									scrollTimeout,
									isFirst = true,
									doScroll = function ()
									{
										var offset = clickedTrack.offset(),
											pos = e.pageY - offset.top - verticalDragHeight / 2,
											contentDragY = paneHeight * settings.scrollPagePercent,
											dragY = dragMaxY * contentDragY / (contentHeight - paneHeight);
										if (direction < 0) {
											if (verticalDragPosition - dragY > pos) {
												jsp.scrollByY(-contentDragY);
											} else {
												positionDragY(pos);
											}
										} else if (direction > 0) {
											if (verticalDragPosition + dragY < pos) {
												jsp.scrollByY(contentDragY);
											} else {
												positionDragY(pos);
											}
										} else {
											cancelClick();
											return;
										}
										scrollTimeout = setTimeout(doScroll, isFirst ? settings.initialDelay : settings.trackClickRepeatFreq);
										isFirst = false;
									},
									cancelClick = function ()
									{
										scrollTimeout && clearTimeout(scrollTimeout);
										scrollTimeout = null;
										$(document).unbind('http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/mouseup.jsp', cancelClick);
										focusElem();
									};
								doScroll();
								$(document).bind('http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/mouseup.jsp', cancelClick);
								return false;
							}
						}
					);
				}
			}
			function removeClickOnTrack()
			{

				if (verticalTrack) {
					verticalTrack.unbind('http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/mousedown.jsp');
				}
			}
			function cancelDrag()
			{
				$('html').unbind('http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/dragstart.jsp selectstart.jsp mousemove.jsp mouseup.jsp mouseleave.jsp');
				if (verticalDrag) {
					verticalDrag.removeClass('jspActive');
				}

				focusElem();
			}
			function positionDragY(destY, animate)
			{
				if (!isScrollableV) {
					return;
				}
				if (destY < 0) {
					destY = 0;
				} else if (destY > dragMaxY) {
					destY = dragMaxY;
				}
				/* can't just check if(animate) because false is a valid value that could be passed in...*/
				if (animate === undefined) {
					animate = settings.animateScroll;
				}
				if (animate) {
					jsp.animate(verticalDrag, 'top', destY,	_positionDragY);
				} else {
					verticalDrag.css('top', destY);
					_positionDragY(destY);
				}
			}
			function _positionDragY(destY)
			{
				if (destY === undefined) {
					destY = verticalDrag.position().top;
				}
				container.scrollTop(0);
				verticalDragPosition = destY;
				var isAtTop = verticalDragPosition === 0,
					isAtBottom = verticalDragPosition == dragMaxY,
					percentScrolled = destY/ dragMaxY,
					destTop = -percentScrolled * (contentHeight - paneHeight);
				if (wasAtTop != isAtTop || wasAtBottom != isAtBottom) {
					wasAtTop = isAtTop;
					wasAtBottom = isAtBottom;
					elem.trigger('jsp-arrow-change', [wasAtTop, wasAtBottom, wasAtLeft, wasAtRight]);
				}
				pane.css('top', destTop);
				elem.trigger('jsp-scroll-y', [-destTop, isAtTop, isAtBottom]).trigger('scroll');
			}

			function contentPositionY()
			{
				return -pane.position().top;
			}
			/* have to remove every instance of deltaX to get mousewheel to work without horizontal scroll, including in the
			mousewheel plugin */
			function initMousewheel()
			{
				container.unbind(mwEvent).bind(
					mwEvent,
					function (event, delta, deltaY) {
						var dY = verticalDragPosition;
						jsp.scrollBy(-deltaY * settings.mouseWheelSpeed, false);
						/* return true if there was no movement so rest of screen can scroll */
						return dY == verticalDragPosition;
					}
				);
			}
			function removeMousewheel()
			{
				container.unbind(mwEvent);
			}
			function nil()
			{
				return false;
			}
			function initFocusHandler()
			{
				pane.find(':input,a').unbind('http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/focus.jsp').bind(
					'http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/focus.jsp',
					function (e)
					{
						scrollToElement(e.target, false);
					}
				);
			}
			function removeFocusHandler()
			{
				pane.find(':input,a').unbind('http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/focus.jsp');
			}

			function initKeyboardNav()
			{
				var keyDown, elementHasScrolled;
				/* IE also focuses elements that don't have tabindex set. */
				pane.focus(
					function ()
					{
						elem.focus();
					}
				);
				elem.attr('tabindex', 0)
					.unbind('http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/keydown.jsp keypress.jsp')
					.bind(
						'http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/keydown.jsp',
						function (e)
						{
							if (e.target !== this){
								return;
							}
							var dY = verticalDragPosition;
							switch(e.keyCode) {
								case 40: /* down */
								case 38: /* up */
								case 34: /* page down */
								case 32: /* space */
								case 33: /* page up */
								case 39: /* right */
								case 37: /* left */
									keyDown = e.keyCode;
									keyDownHandler();
									break;

							}
							elementHasScrolled = e.keyCode == keyDown && dY != verticalDragPosition;
							return !elementHasScrolled;
						}
					).bind(
						'http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/keypress.jsp', /* For FF/ OSX so that we can cancel the repeat key presses if the JSP scrolls... */
						function (e)
						{
							if (e.keyCode == keyDown) {
								keyDownHandler();
							}
							return !elementHasScrolled;
						}
					);

				if (settings.hideFocus) {
					elem.css('outline', 'none');
					if ('hideFocus' in container[0]){
						elem.attr('hideFocus', true);
					}
				} else {
					elem.css('outline', '');
					if ('hideFocus' in container[0]){
						elem.attr('hideFocus', false);
					}
				}

				function keyDownHandler()
				{
					var dY = verticalDragPosition;
					switch(keyDown) {
						case 40: /* down */
							jsp.scrollByY(settings.keyboardSpeed, false);
							break;
						case 38: /* up */
							jsp.scrollByY(-settings.keyboardSpeed, false);
							break;
						case 34: /* page down */
						case 32: /* space */
							jsp.scrollByY(paneHeight * settings.scrollPagePercent, false);
							break;
						case 33: /* page up */
							jsp.scrollByY(-paneHeight * settings.scrollPagePercent, false);
							break;
					}
					elementHasScrolled = dY != verticalDragPosition;
					return elementHasScrolled;
				}
			}
			function removeKeyboardNav()
			{
				elem.attr('tabindex', '-1')
					.removeAttr('tabindex')
					.unbind('http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/keydown.jsp keypress.jsp');
			}
			/* If no element has focus, focus elem to support keyboard navigation */
			function focusElem()
			{
				if (!$(':focus').length) {
					elem.focus();
				}
			}
			/* Init touch on iPad, iPhone, iPod, Android */
			function initTouch()
			{
				var startX,
					startY,
					touchStartX,
					touchStartY,
					moved,
					moving = false;

				container.unbind('touchstart.jsp touchmove.jsp touchend.jsp click.jsp-touchclick').bind(
					'http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/touchstart.jsp',
					function (e)
					{
						var touch = e.originalEvent.touches[0];
						startX = contentPositionX();
						startY = contentPositionY();
						touchStartX = touch.pageX;
						touchStartY = touch.pageY;
						moved = false;
						moving = true;
					}
				).bind(
					'http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/touchmove.jsp',
					function (ev)
					{
						if(!moving) {
							return;
						}

						var touchPos = ev.originalEvent.touches[0],
							dY = verticalDragPosition;

						jsp.scrollTo(startX + touchStartX - touchPos.pageX, startY + touchStartY - touchPos.pageY);

						moved = moved || Math.abs(touchStartX - touchPos.pageX) > 5 || Math.abs(touchStartY - touchPos.pageY) > 5;

						/* return true if there was no movement so rest of screen can scroll */
						return dY == verticalDragPosition;
					}
				).bind(
					'http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/touchend.jsp',
					function (e)
					{
						moving = false;
					}
				).bind(
					'click.jsp-touchclick',
					function (e)
					{
						if(moved) {
							moved = false;
							return false;
						}
					}
				);
			}
			function destroy(){
				var currentY = contentPositionY();
				elem.removeClass('jspScrollable').unbind('.jsp');
				elem.replaceWith(originalElement.append(pane.children()));
				originalElement.scrollTop(currentY);
			}
			/* Public API */
			$.extend(
				jsp,
				{
					/* Reinitialises the scroll pane (if it's internal dimensions have changed since the last time it
					 was initialised). The settings object which is passed in will override any settings from the
					 previous time it was initialised - if you don't pass any settings then the ones from the previous
					 initialisation will be used. */
					reinitialise: function (s)
					{
						s = $.extend({}, settings, s);
						initialise(s);
					},
					/* Scrolls the pane by the specified amount of pixels. animate is optional and if not passed then
					 the value of animateScroll from the settings object this jScrollPane was initialised with is used. */
					scrollBy: function (deltaY, animate)
					{
						jsp.scrollByY(deltaY, animate);
					},

					/* Scrolls the pane by the specified amount of pixels. animate is optional and if not passed then
					 the value of animateScroll from the settings object this jScrollPane was initialised with is used. */
					scrollByY: function (deltaY, animate)
					{
						var destY = contentPositionY() + deltaY,
							percentScrolled = destY / (contentHeight - paneHeight);
						positionDragY(percentScrolled * dragMaxY, animate);
					},

		/*Why does this reference positionDragX for vertical position??? */
					/* Positions the vertical drag at the specified y position (and updates the viewport to reflect
					 this). animate is optional and if not passed then the value of animateScroll from the settings
					 object this jScrollPane was initialised with is used. */
					positionDragY: function (y, animate)
					{
						positionDragX(y, animate);
					},

		/* Probably don't need this */
					/* This method is called when jScrollPane is trying to animate to a new position. You can override
					 it if you want to provide advanced animation functionality. It is passed the following arguments:
					  * ele          - the element whose position is being animated
					  * prop         - the property that is being animated
					  * value        - the value it's being animated to
					  * stepCallback - a function that you must execute each time you update the value of the property
					 You can use the default implementation (below) as a starting point for your own implementation. */
					animate: function (ele, prop, value, stepCallback)
					{
						var params = {};
						params[prop] = value;
						ele.animate(
							params,
							{
								'duration'	: settings.animateDuration,
								'ease'		: settings.animateEase,
								'queue'		: false,
								'step'		: stepCallback
							}
						);
					},
					/* Returns the current y position of the viewport with regards to the content pane. */
					getContentPositionY: function ()
					{
						return contentPositionY();
					},
					/* Returns the width of the content within the scroll pane. */
					getContentWidth: function ()
					{
						return contentWidth();
					},
					/* Returns the height of the content within the scroll pane. */
					getContentHeight: function ()
					{
						return contentHeight();
					},
					/* Returns the vertical position of the viewport within the pane content. */
					getPercentScrolledY: function ()
					{
						return contentPositionY() / (contentHeight - paneHeight);
					},
					/* Returns whether or not this scrollpane has a vertical scrollbar. */
					getIsScrollableV: function ()
					{
						return isScrollableV;
					},
					/* Gets a reference to the content pane. It is important that you use this method if you want to
					 edit the content of your jScrollPane as if you access the element directly then you may have some
					 problems (as your original element has had additional elements for the scrollbars etc added into
					 it). */
					getContentPane: function ()
					{
						return pane;
					},
					/* Removes the jScrollPane and returns the page to the state it was in before jScrollPane was
					 initialised. */
					destroy: function ()
					{
							destroy();
					}
				}
			);
			initialise(s);
		}
		/* Pluginifying code... */
		settings = $.extend({}, $.fn.jScrollPane.defaults, settings);
		/* Apply default speed */
		$.each(['mouseWheelSpeed', 'arrowButtonSpeed', 'trackClickSpeed', 'keyboardSpeed'], function () {
			settings[this] = settings[this] || settings.speed;
		});
		var ret;
		this.each(
			function ()
			{
				var elem = $(this), jspApi = elem.data('jsp');
				if (jspApi) {
					jspApi.reinitialise(settings);
				} else {
					jspApi = new JScrollPane(elem, settings);
					elem.data('jsp', jspApi);
				}
				ret = ret ? ret.add(elem) : elem;
			}
		);
		return ret;
	};
	$.fn.jScrollPane.defaults = {
		maintainPosition			: true,
		clickOnTrack				: true,
		autoReinitialise			: false,
		autoReinitialiseDelay		: 500,
		verticalDragMinHeight		: 0,
		verticalDragMaxHeight		: 99999,
		animateScroll				: false,
		animateDuration				: 300,
		animateEase					: 'linear',

		verticalGutter				: 4,
		mouseWheelSpeed				: 0,

		trackClickSpeed				: 0,
		trackClickRepeatFreq		: 70,

		enableKeyboardNavigation	: true,
		hideFocus					: false,
		keyboardSpeed				: 0,
		initialDelay                : 300,        /* Delay before starting repeating */
		speed						: 30,		/* Default speed when others falsey */
		scrollPagePercent			: .8		/* Percent of visible area scrolled when pageUp/Down or track area pressed */
	};
})(jQuery,this);
/*! Copyright (c) 2010 Brandon Aaron (http://brandonaaron.net)
 * Licensed under the MIT License (LICENSE.txt).
 *
 * Thanks to: http://adomas.org/javascript-mouse-wheel/ for some pointers.
 * Thanks to: Mathias Bank(http://www.mathias-bank.de) for a scope bug fix.
 * Thanks to: Seamus Leahy for adding deltaX and deltaY
 *
 * Version: 3.0.4
 *
 * Requires: 1.2.2+
 */
(function ($) {
var types = ['DOMMouseScroll', 'mousewheel'];
$.event.special.mousewheel = {
	setup: function () {
		if ( this.addEventListener ) {
			for ( var i=types.length; i; ) {
				this.addEventListener( types[--i], handler, false );
			}
		} else {
			this.onmousewheel = handler;
		}
	},
	teardown: function () {
		if ( this.removeEventListener ) {
			for ( var i=types.length; i; ) {
				this.removeEventListener( types[--i], handler, false );
			}
		} else {
			this.onmousewheel = null;
		}
	}
};
$.fn.extend({
	mousewheel: function (fn) {
		return fn ? this.bind("mousewheel", fn) : this.trigger("mousewheel");
	},

	unmousewheel: function (fn) {
		return this.unbind("mousewheel", fn);
	}
});
function handler(event) {
	var orgEvent = event || window.event, args = [].slice.call( arguments, 1 ), delta = 0, returnValue = true, /*deltaX = 0, */deltaY = 0;
	event = $.event.fix(orgEvent);
	event.type = "mousewheel";
	/* Old school scrollwheel delta */
	if ( event.wheelDelta ) { delta = event.wheelDelta/120; }
	if ( event.detail     ) { delta = -event.detail/3; }
	/* New school multidimensional scroll (touchpads) deltas */
	deltaY = delta;
	/* Gecko */
	if ( orgEvent.axis !== undefined && orgEvent.axis === orgEvent.HORIZONTAL_AXIS ) {
		deltaY = 0;
	}
	/* Webkit */
	if ( orgEvent.wheelDeltaY !== undefined ) { deltaY = orgEvent.wheelDeltaY/120; }
	/* Add event and delta to the front of the arguments */
	args.unshift(event, delta,/* deltaX, */deltaY);
	return $.event.handle.apply(this, args);
}})(jQuery);

/**
 * This function controls the "dynamic details" functionality of the hero templates.
 */
$(function (){
var detailsTimeout = [];
	function showDetails($details){
		if($details.height() == '128'){
			$details.find('.content').animate({
				'bottom' : '125px'
				},
				800 , 'easeInOutExpo'
			);
		}
		else if($details.height() == '698'){
			$details.find('.content').animate(
				{'bottom' : '210px'},
				800 , 'easeInOutExpo'
			);
		}
		else{
			$details.find('.content').animate(
				{'bottom' : '220px'},
				800 , 'easeInOutExpo'
			);
		}

	};
	function hideDetails($details){
		$details.find('.content').animate(
			{'bottom' : '0'},
			800 , 'easeInOutExpo'
		);
	};
	if($('.scroll-pane').length){
		$('.scroll-pane').jScrollPane({hideFocus : true});
	}

	$('.dyn_details').each(function (i){

		var $this = $(this);

		$this.find('.slider').bind('click',{details:$this},function (e){
			e.preventDefault();
			showDetails(e.data.details);
		});
		$this.bind("mouseenter",{i:i},function (e){
			clearTimeout(detailsTimeout[e.data.i]);
		});
		$this.bind("mouseleave",{i:i,details:$this},function (e){
			detailsTimeout[e.data.i]=setTimeout(function (){ hideDetails(e.data.details)}, 5000);
		});
		$this.find('.slideback').bind('click',{details:$this}, function (e){
			e.preventDefault();
			hideDetails(e.data.details);
		});
	});
});

/* ../global/Lowes/UI/form.js */

(function($) {
	// @todo Document this.
	$.extend($,{ placeholder: {
			browser_supported: function() {
				return this._supported !== undefined ?
					this._supported :
					( this._supported = !!('placeholder' in $('<input type="text">')[0]) );
			},
			shim: function(opts) {
				var config = {
					color: '#333',
					cls: 'placeholder',
					lr_padding:4,
					selector: 'input[placeholder], textarea[placeholder]'
				};
				$.extend(config,opts);
				!this.browser_supported() && $(config.selector)._placeholder_shim(config);
			}
	}});

	$.extend($.fn,{
		_placeholder_shim: function(config) {
			function calcPositionCss(target) {
				var op = $(target).offsetParent().offset();
				var ot = $(target).offset();

				return {
					top: ot.top - op.top + ($(target).outerHeight() - $(target).height()) /2,
					left: ot.left - op.left + config.lr_padding,
					width: $(target).width() - config.lr_padding
				};
			}
			return this.each(function() {
				var $this = $(this);

				if( !$.isEmptyObject($this.data('placeholder')) ) {
					var $ol = $this.data('placeholder');
					$ol.css(calcPositionCss($this));
					return true;
				}

				var possible_line_height = {};
				if( !$this.is('textarea') && $this.css('height') != 'auto') {
					possible_line_height = { lineHeight: $this.css('height'), whiteSpace: 'nowrap' };
				}
				var stylations = $.extend({
						position:'absolute',
						display: 'inline',
						'float':'none',
						overflow:'hidden',
						textAlign: 'left',
						color: config.color,
						cursor: 'text',
						paddingTop: $this.css('padding-top'),
						paddingLeft: $this.css('padding-left'),
						fontSize: $this.css('font-size'),
						fontFamily: 'Georgia',
						fontStyle: 'italic',
						fontWeight: $this.css('font-weight'),
						textTransform: $this.css('text-transform'),
						zIndex: 99,
						marginTop: 0
					}, possible_line_height);
				var ol = $('<label />')
					.text($this.attr('placeholder'))
					.addClass(config.cls)
					.css(stylations)
					.css(calcPositionCss(this))
					.attr('for', this.id)
					.data('target',$this)
					.click(function(){
						$(this).data('target').focus()
					})
					.insertBefore(this);
				$this.data('placeholder',ol)
					.focus(function(){
						ol.hide();
					}).blur(function() {
						ol[$this.val().length ? 'hide' : 'show']();
					}).triggerHandler('blur');
				$(window)
					.resize(function() {
						var $target = ol.data('target')
						ol.css(calcPositionCss($target))
					});
			});
		}
	});
})(jQuery);

jQuery(document).add(window).bind('ready load', function() {
	if (jQuery.placeholder) {
		jQuery.placeholder.shim({selector: '#header-block input[placeholder], #header-block textarea[placeholder]'});
	}
});

/* ../global/Lowes/UI/datalist.js */

/**
 * @fileOverview Lowes.UI.DataList() and Lowes.UI.DataListItem()
 * @author Core
 */

(function (){
	/* namespaces */
	var Lowes = window.Lowes || {};
	var UI = Lowes.UI || {};
	
	UI.DataList = Class.extend(/** @lends Lowes.UI.DataList# */{
		/**
		 * Takes all options for a jQuery list element plus
		 * options.listType to toggle between ol/ul
		 * 
		 * @memberOf Lowes
		 * @param {Object} options All the same options you can pass to jQuery('&lt;ul /&gt;', options)
		 * @param {String} options.listType An options extension for the type of list. Values: 'ordered', 'ol', anything else is 'ul'.
		 * @constructs
		 * @example	var dataList = new Lowes.UI.DataList();
		 * var ListItem = new Lowes.UI.DataListItem({id:'example-item'});
		 * dataList.load(ListItem.$listItem);
		 * dataList.$dataList.appendTo('body');
		 */
		init: function (options){
			options = options || {};
			options.listType = options.listType || 'ul';
			if (options.listType == 'ordered'){
				options.listType = 'ol';
			}
			if (options.listType != 'ol') {
				options.listType = 'ul';
			}
			this.options = options;
			/** a jQuery object allows easier DOM manipulation. */
			this.$dataList = $('<'+this.options.listType+' class="ui-datalist" />');
			return;
		},

		/**
		 * Add a Lowes.UI.DataListItem().$listItem to our $dataList
		 * @param {Lowes.UI.DatalistItem} $listItem DataList item to add.
		 */
		load: function ($listItem){
			this.$dataList.append($listItem);
			return true;
		},

		/**
		 * Simply output an HTML string
		 * @returns {String} An HTML string representation of the list.
		 */
		toHtml: function (){
			return $('<div />').append(this.$dataList).html();
		}
	});

	UI.DataListItem = Class.extend(/** @lends Lowes.UI.DataListItem# */{
		/** 
		 * Use with Lowes.UI.DataList to build dynamic lists.
		 * 
		 * @constructs
		 * @memberOf Lowes
		 * @param {Object} options All the same options you can pass to jQuery('&lt;li /&gt;', options)
		 * @example	var dataList = new Lowes.UI.DataList();
		 * var ListItem = new Lowes.UI.DataListItem({id:'example-item'});
		 * dataList.load(ListItem.$listItem);
		 * dataList.$dataList.appendTo('body');
		 */
		init: function (options){
			/** a jQuery object allows easier DOM manipulation. */
			this.$listItem = $('<li />', options);
			return;
		},

		/**
		 * Simply output an HTML string
		 * @returns {String} An HTML string representation of the list item.
		 */
		toHtml: function (){
			return $('<div />').append(this.$listItem).html();
		}
	});
	window.Lowes = Lowes;
}());

/* ../global/Lowes/UI/spin.js */

//fgnass.github.com/spin.js#v1.2.5
( function($, window, document, undefined) {
	/**
	 * Copyright (c) 2011 Felix Gnass [fgnass at neteye dot de]
	 * Licensed under the MIT license
	 */
	var prefixes = ['webkit', 'Moz', 'ms', 'O'],
		/* Vendor prefixes */
		animations = {},
		/* Animation rules keyed by their name */
		useCssAnimations,
		/**
		 * Insert a new stylesheet to hold the @keyframe or VML rules.
		 */
		sheet = (function() {
			var el = createEl('style');
			ins(document.getElementsByTagName('head')[0], el);
			return el.sheet || el.styleSheet;
		}()),
		/** The constructor */
		Spinner = function(o) {
			if(!this.spin) {
				return new Spinner(o);
			}
			this.opts = merge(o || {}, Spinner.defaults, defaults);
		},
		defaults = {
			lines : 12, // The number of lines to draw
			length : 7, // The length of each line
			width : 5, // The line thickness
			radius : 10, // The radius of the inner circle
			rotate : 0, // rotation offset
			//color : '#000', // #rgb or #rrggbb
			speed : 1, // Rounds per second
			trail : 100, // Afterglow percentage
			opacity : 1 / 4, // Opacity of the lines
			fps : 20, // Frames per second when using setTimeout()
			zIndex : 2e9, // Use a high z-index by default
			className : null, // "throbber" CSS class to assign to the element
			top : 'auto', // center vertically
			left : 'auto'          // center horizontally
		};
	/**
	 * Utility function to create elements. If no tag name is given,
	 * a DIV is created. Optionally properties can be passed.
	 */
	function createEl(tag, prop) {
		var el = document.createElement(tag || 'div'),
			n;

		for(n in prop) {
			el[n] = prop[n];
		}
		return el;
	}

	/**
	 * Appends children and returns the parent.
	 */
	function ins(parent /* child1, child2, ...*/) {
		var i;

		for(i = 1, n = arguments.length; i < n; i++) {
			parent.appendChild(arguments[i]);
		}
		return parent;
	}

	/**
	 * Creates an opacity keyframe animation rule and returns its name.
	 * Since most mobile Webkits have timing issues with animation-delay,
	 * we create separate rules for each line/segment.
	 */
	function addAnimation(alpha, trail, i, lines) {
		var name = ['opacity', trail, ~~(alpha * 100), i, lines].join('-');
		var start = 0.01 + i / lines * 100;
		var z = Math.max(1 - (1 - alpha) / trail * (100 - start), alpha);
		var prefix = useCssAnimations.substring(0, useCssAnimations.indexOf('Animation')).toLowerCase();
		var pre = prefix && '-' + prefix + '-' || '';

		if(!animations[name]) {
			sheet.insertRule('@' + pre + 'keyframes ' + name + '{' + '0%{opacity:' + z + '}' + start + '%{opacity:' + alpha + '}' + (start + 0.01) + '%{opacity:1}' + (start + trail) % 100 + '%{opacity:' + alpha + '}' + '100%{opacity:' + z + '}' + '}', 0);
			animations[name] = 1;
		}
		return name;
	}

	/**
	 * Tries various vendor prefixes and returns the first supported property.
	 **/
	function vendor(el, prop) {
		var s = el.style;
		var pp;
		var i;

		if(s[prop] !== undefined)
			return prop;
		prop = prop.charAt(0).toUpperCase() + prop.slice(1);
		for( i = 0; i < prefixes.length; i++) {
			pp = prefixes[i] + prop;
			if(s[pp] !== undefined)
				return pp;
		}
	}

	/**
	 * Sets multiple style properties at once.
	 */
	function css(el, prop) {
		for(var n in prop) {
			el.style[vendor(el, n) || n] = prop[n];
		}
		return el;
	}

	/**
	 * Fills in default values.
	 */
	function merge(obj) {
		for(var i = 1; i < arguments.length; i++) {
			var def = arguments[i];
			for(var n in def) {
				if(obj[n] === undefined)
					obj[n] = def[n];
			}
		}
		return obj;
	}

	/**
	 * Returns the absolute page-offset of the given element.
	 */
	function pos(el) {
		var o = {
			x : el.offsetLeft,
			y : el.offsetTop
		};
		while(( el = el.offsetParent)) {
			o.x += el.offsetLeft;
			o.y += el.offsetTop;
		}
		return o;
	}

	Spinner.defaults = {};
	merge(Spinner.prototype, {
		spin : function(target) {
			this.stop();
			var self = this,
				o = self.opts,
				el = self.el = css(createEl(0, {
					className : "throbber " + o.className
				}), {
					position : 'relative',
					zIndex : o.zIndex
				}),
				mid = o.radius + o.length + o.width,
				ep, // element position
				tp; // target position

			if(target) {
				target.insertBefore(el, target.firstChild || null);
				tp = pos(target);
				ep = pos(el);
				css(el, {
					left : (o.left == 'auto' ? tp.x - ep.x + (target.offsetWidth >> 1) : o.left + mid) + 'px',
					top : (o.top == 'auto' ? tp.y - ep.y + (target.offsetHeight >> 1) : o.top + mid) + 'px'
				});
			}

			el.setAttribute('aria-role', 'progressbar');
			self.lines(el, self.opts);

			if(!useCssAnimations) {
				// No CSS animation support, use setTimeout() instead
				var i = 0,
					fps = o.fps,
					f = fps / o.speed,
					ostep = (1 - o.opacity) / (f * o.trail / 100),
					astep = f / o.lines;

				(function anim() {
					var s, alpha;

					i++;
					for(s = o.lines; s; s--) {
						alpha = Math.max(1 - (i + s * astep) % f * ostep, o.opacity);
						self.opacity(el, o.lines - s, alpha, o);
					}
					self.timeout = self.el && setTimeout(anim, ~~(1000 / fps));
				}());
			}
			return self;
		},
		stop : function() {
			var el = this.el;
			if(el) {
				clearTimeout(this.timeout);
				if(el.parentNode)
					el.parentNode.removeChild(el);
				this.el = undefined;
			}
			return this;
		},
		lines : function(el, o) {
			var i = 0,
				seg;

			function fill(color, shadow) {
				return css(createEl(0, { className : "spindle" }), {
					position : 'absolute',
					width : (o.length + o.width) + 'px',
					height : o.width + 'px',
					//background : color,
					boxShadow : shadow,
					transformOrigin : 'left',
					transform : 'rotate(' + ~~(360 / o.lines * i + o.rotate) + 'deg) translate(' + o.radius + 'px' + ',0)',
					borderRadius : (o.width >> 1) + 'px'
				});
			}

			for(; i < o.lines; i++) {
				seg = css(createEl(), {
					position : 'absolute',
					top : 1 + ~(o.width / 2) + 'px',
					transform : o.hwaccel ? 'translate3d(0,0,0)' : '',
					opacity : o.opacity,
					animation : useCssAnimations && addAnimation(o.opacity, o.trail, i, o.lines) + ' ' + 1 / o.speed + 's linear infinite'
				});
				if(o.shadow)
					ins(seg, css(fill('#000', '0 0 4px ' + '#000'), {
						top : 2 + 'px'
					}));
				ins(el, ins(seg, fill(o.color, '0 0 1px rgba(0,0,0,.1)')));
			}
			return el;
		},
		opacity : function(el, i, val) {
			if(i < el.childNodes.length)
				el.childNodes[i].style.opacity = val;
		}
	});

	/////////////////////////////////////////////////////////////////////////
	// VML rendering for IE
	/////////////////////////////////////////////////////////////////////////

	/**
	 * Check and init VML support
	 */
	(function() {
		var s = css(createEl('group'), {
				behavior : 'url(#default#VML)'
			});

		function vml(tag, attr) {
			return createEl('<' + tag + ' xmlns="urn:schemas-microsoft.com:vml" class="spin-vml">', attr);
		}

		if(!vendor(s, 'transform') && s.adj) {

			// VML support detected. Insert CSS rule ...
			sheet.addRule('.spin-vml', 'behavior:url(#default#VML)');

			Spinner.prototype.lines = function(el, o) {
				var r = o.length + o.width,
					s = 2 * r,
					margin = -(o.width + o.length) * 2 + 'px',
					g = css(grp(), {
						position : 'absolute',
						top : margin,
						left : margin
					}),
					i;

				function grp() {
					return css(vml('group', {
						coordsize : s + ' ' + s,
						coordorigin : -r + ' ' + -r
					}), {
						width : s,
						height : s
					});
				}

				function seg(i, dx, filter) {
					ins(g, ins(css(grp(), {
						rotation : 360 / o.lines * i + 'deg',
						left : ~~dx
					}), ins(css(vml('roundrect', {
						arcsize : 1
					}), {
						width : r,
						height : o.width,
						left : o.radius,
						top : -o.width >> 1,
						filter : filter
					}), vml('fill', {
						color : o.color,
						opacity : o.opacity
					}), vml('stroke', {
						opacity : 0
					}) // transparent stroke to fix color bleeding upon opacity change
					)));
				}

				if(o.shadow) {
					for( i = 1; i <= o.lines; i++) {
						seg(i, -2, 'progid:DXImageTransform.Microsoft.Blur(pixelradius=2,makeshadow=1,shadowopacity=.3)');
					}
				}
				for( i = 1; i <= o.lines; i++){
					seg(i);
				}
				return ins(el, g);
			};
			Spinner.prototype.opacity = function(el, i, val, o) {
				var c = el.firstChild;

				o = o.shadow && o.lines || 0;
				if(c && i + o < c.childNodes.length) {
					c = c.childNodes[i + o];
					// short-circuit check
					c = c && c.firstChild;
					// without this duplicated line, IE throws an error
					c = c && c.firstChild;
					if(c){
						c.opacity = val;
					}
				}
			};
		} else {
			useCssAnimations = vendor(s, 'animation');
		}
	}());
	/* jQuery plugin for Spinner (Throbber) */
	$.fn.spin = function(opts) {
		this.each(function() {
			var $this = $(this), data = $this.data();

			if(data.spinner) {
				data.spinner.stop();
				delete data.spinner;
			}
			if(opts !== false) {
				data.spinner = new Spinner($.extend({
					color : $this.css('color')
				}, opts)).spin(this);
			}
		});
		return this;
	};

	window.Spinner = Spinner;
/*		$(function (){
		var opts = {
				lines : 13, // The number of lines to draw
				length : 7, // The length of each line
				width : 4, // The line thickness
				radius : 10, // The radius of the inner circle
				rotate : 0, // The rotation offset
				color : '#000', // #rgb or #rrggbb
				speed : 1, // Rounds per second
				trail : 60, // Afterglow percentage
				shadow : false, // Whether to render a shadow
				hwaccel : false, // Whether to use hardware acceleration
				className : 'throbber', // The CSS class to assign to the spinner
				zIndex : 2e9, // The z-index (defaults to 2000000000)
				top : 'auto', // Top position relative to parent in px
				left : 'auto' // Left position relative to parent in px
			},
			altOpts = $.extend({},opts,{color:"#FFF"});

		$('.ui-throbber').spin(opts);
		$('.ui-throbber-inverse').spin(altOpts);
	}); */

}(jQuery, window, document));


/* ../global/Lowes/module.js */

/** @fileOverview Lowes Module namespace */

(function (Lowes){
   /**
	* Just a container for specific modules built using 
	* LowesJS library. Items extending Module class will
	* Not be included in the main build of LowesJS.
	*/
   Lowes.Module = false;
   
   /* Expose the updated library to global. */
   window.Lowes = Lowes;
}(window.Lowes || {}));

/* ../global/Lowes/modules/metric-helpers.js */

/* global/Lowes/metrics/metrics.js */

// Poly Fill For Object.keys functionality
// From https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/keys
if (!Object.keys) {
	Object.keys = (function () {
		'use strict';
		var hasOwnProperty = Object.prototype.hasOwnProperty,
		hasDontEnumBug = !({toString: null}).propertyIsEnumerable('toString'),
		dontEnums = [
			'toString',
			'toLocaleString',
			'valueOf',
			'hasOwnProperty',
			'isPrototypeOf',
			'propertyIsEnumerable',
			'constructor'
		],
		dontEnumsLength = dontEnums.length;

		return function (obj) {
			if (typeof obj !== 'object' && (typeof obj !== 'function' || obj === null)) {
				throw new TypeError('Object.keys called on non-object');
			}

			var result = [], prop, i;

			for (prop in obj) {
				if (hasOwnProperty.call(obj, prop)) {
					result.push(prop);
				}
			}

			if (hasDontEnumBug) {
				for (i = 0; i < dontEnumsLength; i++) {
					if (hasOwnProperty.call(obj, dontEnums[i])) {
						result.push(dontEnums[i]);
					}
				}
			}
			return result;
		};
	}());
}

(function (window, document, $) {

	var Lowes = window.Lowes || {},

	/**
	 * Metrics Helpers - Functions for managing metrics
	 * @class
	 * @name Metrics
	 * @example Lowes.Metrics.ltag(dataObj {event_type: 'change-store'}, tagType 'tealium', tagFunctionType - only required for 'view');
	 */
	Metrics = {
		/** @lends Metrics */
		tagTypes : {
			'tealium': {
				'moduleID': 'utag'
			},
			'dtm': {
				'moduleID': 's'
			},
			'coremetrics': {
				'moduleID': 'cmUtils' //cmLoad
			}
		},

		/**
		 * Root function for a lowes metrics tag
		 * @function
		 * @param  {object} Data object formatted for your specified tag library (ex. {event_type: 'change-store'} for Tealium)
		 * @param  {string} Specified tag library (ex 'tealium')
		 * @param  {string} Alternative function from tag library (ex. 'view')
		 * @memberOf Metrics
		 */
		ltag: function(dataObj, tagType, tagFunctionType) {
			// Check for metric library type object/function availability
			if (!window[this.tagTypes[tagType].moduleID]) {
				// halt execution if it is not available
				return false;
			} else {
				// Route data + Continue with processing based on tagType
				//if (tagType === 'tealium') {
				//	this.processTealiumData(dataObj, tagFunctionType);
				//} else {
				//	return false;
				//}

				// Switch Statement For Future Tag Type Expansion
				switch (tagType) {
					case 'tealium':
			 			this.processTealiumData(dataObj, tagFunctionType);
			 		break;
					case 'dtm':
			 			console.log(JSON.stringify(dataObj) +' : '+  tagType);
			 		break;
				 	default:
						this.processTealiumData(dataObj, tagFunctionType);
			 		break;
				 }
			}
		},

		/**
		 * Cycle through data object nodes and run each through sanitize function
		 * @function
		 * @param  {object} Data object formatted for your specified tag library (ex. {event_type: 'change-store'} for Tealium)
		 * @param  {string} Alternative function from tag library (ex. 'view')
		 * @memberOf Metrics
		 */
		processTealiumData: function(dataObj, tagFunctionType) {
			var processedDataObj = dataObj,
				data_prop,
				key,
				i,
				prop_array = [];

			// Cycle through object and sanitize each key value pair
			for (key in dataObj) {
				// Operate on custom properties only
				if (dataObj.hasOwnProperty(key)) {
					// Set the intial data_prop value
					data_prop = dataObj[key];

					// operate on string values
					if (typeof data_prop === 'string' && key !== 'event_type') {
						processedDataObj[key] = this.cleanTealiumData(data_prop);
					}

					// // Malfunctioning when setting new dynamic values in data object
					// // Move cleanTealiumData to setting of all array wrapped values
					// // operate on array values
					// else if (typeof data_prop === 'object' && data_prop.length > 0) {
					// 	for (i = 0; i < data_prop.length; i++) {
					// 		prop_array[i] = this.cleanTealiumData(data_prop[i]);
					// 		processedDataObj[key] = prop_array;
					// 	}
					// }
				}
			}

			// When complete run the appropriate tealium function
			this.pushTealiumData(processedDataObj, tagFunctionType);
		},

		/**
		 * Sanitize Data before sending to Tealium
		 * - replace spaces with  _ | lowercase | alpha / numeric | cast currency to 2 decimal places (mirror backend)
		 * @function
		 * @param  {string or array} String or Array value of data object key (ex. 'change-store' or ['change-store','click-action'])
		 * @memberOf Metrics
		 */
		cleanTealiumData: function(dataString) {
			var cleanString = '';

			// convert all text to lowercase
			cleanString = dataString.toLowerCase();
			// replace spaces with underscores
			cleanString = cleanString.replace(/\s/g, '_');
			// replace dashes with underscores
			// cleanString = cleanString.replace(/-/g, '_');
			// remove all special characters
			cleanString = cleanString.replace(/[^A-Za-z0-9\_\,\:\.\;\-]/g, '');
			// replace double underscores resulting from whitespace replacement
			cleanString = cleanString.replace(/_+/g, '_');

			return cleanString;
		},

		/**
		 * Send data to Tealium
		 * @function
		 * @param  {object} Cleaned Data object formatted for your specified tag library (ex. {event_type: 'change-store'} for Tealium)
		 * @param  {string} Alternative function from tag library (ex. 'view'), If undefined 'http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/utag.link' is used.
		 * @memberOf Metrics
		 */
		pushTealiumData: function(dataObj, tagFunctionType) {
			// Check for Type, No Type Specified Defaults to Link type
			if (tagFunctionType === undefined || tagFunctionType === '') {
				utag.link(dataObj);
			} else if (tagFunctionType === 'view') {
				utag.view(dataObj);
			}
		},


		/**
		 * Create Basic utag_data object
		 * @description This object needs to be in place and populated before loading the Tealium
		 * @function
		 * @memberOf Metrics
		 */
		setupBasicUtagData: function() {
			// Ensure utag_data does not exist
			if (typeof utag_data !== 'object') {
				// Setup Data Object
				window.utag_data = {};
			}

			// Update Store info
			if (Lowes.User.isZippedIn()) {
				utag_data.user_store_name = Lowes.User.CurrentStore.shortName;
				utag_data.user_storeid = Lowes.User.CurrentStore.number;
				utag_data.zip = Lowes.User.CurrentStore.zip;
			}

			// Update User Status
			if(Lowes.User.isSignedIn()) {
				utag_data.visitor_status = 'registered';
				utag_data.userid = Lowes.User.Details.id;
			} else {
				utag_data.visitor_status = 'anonymous';
			}
		},

		/**
		 * Load Tealium Library
		 * @function
		 * @memberOf Metrics
		 */
		loadTealiumLib: function() {
			// Ensure only one load of tealium lib
			if (typeof utag !== 'object') {
				// Inject Code
				(function(a,b,c,d){
					var libEnv = 'dev',
						envSubDomain = location.hostname.slice(0, -10);
						
					if (envSubDomain === 'www') {
						libEnv = 'prod'
					}

					a='//tags.tiqcdn.com/utag/lowes/main/'+ libEnv +'/utag.js';
					b=document;c='script';d=b.createElement(c);d.src=a;d.type='text/java'+c;d.async=true;
					a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);
				})();
			}
		}

	};

	Lowes.Metrics = Metrics;
	window.Lowes = Lowes;

}(window, document, jQuery));

/* ../global/Lowes/UI/storelocator.js */

/* We need to ensure google has loaded before even attempting any of this. */
if (window.google) {
	/**
	 * @Package Lowes.StoreLocator
	 * @Author Lowes.com
	 * @version <%=VERSION=%>
	 * @todo	Create a better way of replacing the scrollable content and reinitializing the jScrollPane()
	 * 			Better separation of public/private variables.
	 */
	(function($){

			/* Private Helper Functions */
			function isDriveable(country){
				country = (country) ? country.toUpperCase() : '';
				return !!(country == 'US' || country == 'CA' || country == 'MX');
			}

			/**
			 * Formats store hours data for display on the checkout confirmation and
			 * store detail pages.  This is brittle and should really be coming from the facade.
			 */
			function formatStoreHours(storeHours) {
			  var week = {0:'Monday', 1:'Tuesday', 2:'Wednesday', 3:'Thursday', 4:'Friday', 5:'Saturday', 6:'Sunday'},
				  days = [], ret, hoursString = '';

			  $.each(week, function() {
				var openHour  = parseInt(storeHours[this + '_Open'], 10),
					closeHour = parseInt(storeHours[this + '_Close'], 10),
					openMin   = ':' + storeHours[this + '_Open'].substring(3,5),
					closeMin  = ':' + storeHours[this + '_Close'].substring(3,5);

				// don't display 00 minutes
				openMin  = (openMin  == ':00') ? '' : openMin;
				closeMin = (closeMin == ':00') ? '' : closeMin;

				if(openHour == closeHour) { // closed for the day
				  var hours = 'Store Closed';
				}
				else if(openHour == 0 && closeHour == 24) { // open 24 hrs
				  var hours = 'Open 24 hours';
				}
				else { // convert from 24 hr and add AM or PM
				  if(openHour == 24 || openHour == 00 || openHour == 12) {
						if(openHour==12){
				              amOrPm = ' PM';
				            }else{
				              amOrPm = ' AM';
				            }
		            	openHour = '12' + openMin + amOrPm;
					closeHour = closeHour < 12 ? closeHour + closeMin + ' AM' : (closeHour-12) + closeMin + ' PM';
				  }
				  else if(closeHour == 24 || closeHour == 00) {
					closeHour = '12' + closeMin + ' AM';
					openHour  = openHour  < 12 ? openHour  + openMin + ' AM' : (openHour-12) + openMin + ' PM';
				  }
				  else {
					openHour  = openHour  < 12 ? openHour  + openMin  + ' AM' : (openHour-12)  + openMin  + ' PM';
					closeHour = closeHour < 12 ? closeHour + closeMin + ' AM' : (closeHour-12) + closeMin + ' PM';
				  }

				  var hours = '' + openHour + ' - ' + closeHour;
				}

				// consolidate blocks
				if(days.length && hours == days[days.length-1].hours) { // same block
				  days[days.length-1].endDay = this.substring(0,3);
				}
				else { // new block
				  var obj = {};
				  obj.startDay = this.substring(0,3);
				  obj.endDay   = this.substring(0,3);
				  obj.hours    = hours;
				  days.push(obj);
				}
			  });

			  // remove duplicate start and end days and handle Thursday
			  ret = [];
			  $.each(days, function(i) {
				ret[i] = {};

				if(this.startDay == this.endDay) { // 1 day block
				  ret[i].days = this.startDay;
				}
				else { // multi-day block
				  ret[i].days = this.startDay + ' - ' + this.endDay;
				}
				ret[i].days = ret[i].days.replace('Thu', 'Thurs'); // special case for Thursday
				ret[i].hours = this.hours;
			  });

			  // convert obj to string
			  $.each(ret, function(index, item) {
				 hoursString += item.days + ' ' + item.hours;
				 if (index < (ret.length - 1)) {
					hoursString += ', ';
				 }
			  })

			  // return ret;
			  return hoursString;
			};

			/**
			 * Private Store Listing Data List Item
			 *
			 * @class StoreListing
			 * @extends Lowes.UI.DataListItem
			 * @param {object} store The store result from the query
			 * @param {string} store.Number
			 * @param {string} store.Name
			 * @param {string} store.Address
			 * @param {string} store.City
			 * @param {string} store.State
			 * @param {string} store.ZIP
			 * @param {string} store.Phone
			 * @param {string} store.Fax
			 * @param {string} store.Hours - discontinued value, no longer being updated use StoreHours
			 * @param {string} store.StoreHours - requires transform function to format this data
			 * @param {string} store.LAT
			 * @param {string} store.LNG
			 * @param {string} store.DIRE
			 * @param {string} store.CD CODE (H - "Headquarters", N - "Normal", NOS - "Opening Soon") - discontinued value
			 * @param {string} store.LOGO
			 * @param {string} store.CST_SEV
			 * @param {string} store.distVal
			 * @param {string} store.distTxt
			 * @param {string} store.distanceOrder Basically, the sort order.
			 * @param {class} eventScope The scope in which to execute event callbacks.
			 */
			var StoreListing = Lowes.UI.DataListItem.extend({
				init: function ( store, eventScope ){
					var $store = $('<div />'),
						toRemove = "Lowe's of ",
						isSelectedStore, $li, $storeLinks, $togglables, iconPrefix, icon,
						marker = eventScope.markersArray[store.distanceOrder],
						$directionsLink = $('<a />',{text: 'Get Directions'}),
						directionsAddress,
						storeHours,
						formattedStoreHours;

					store = store || {};
					eventScope = eventScope || this;
					if ( store.KEY ){
						/* Call the parent Class's init() to create this.$listItem */
						this._super({id: [eventScope.prefix, 'store-listing-', store.KEY].join('')});
						/*
						 * Determine if we are already zipped in to this store.
						 * note: not using Lowes.User.CurrentStore since I need a fresh check due to autozip.
						 */
						isSelectedStore = !!(Lowes.User.getCurrentStore().number == store.KEY);

						/* String parsing routines */
						store.NAME = [store.NAME.split(',')[0].toTitleCase(),store.STATE].join(', ');
						if ( store.NAME.substring(0,toRemove.length) == toRemove ){
							store.NAME = store.NAME.slice(toRemove.length);
						}
						store.ADDR = store.ADDR.toTitleCase();
						store.ADDR2 = store.CITY.toTitleCase()+', '+store.STATE+" "+store.ZIP+', Store #'+store.KEY;
						// Format Store Hours
						formattedStoreHours = formatStoreHours(store.StoreHours);

						/* Work with a private copy of the store listing */
						$li = this.$listItem;
						$li.append($('<span />', {text: store.distTxt, 'class': 'store-distance'}));
						$li.append( $('<img />', {src: marker.getIcon().url, alt: store.KEY, 'class':'store-marker'}) );
						if ( isSelectedStore ){
							$li.append($('<strong />', {
								text:"Your Store:",
								'class':'your-store'
							}));
						}
						$li.append($('<strong />', {text: store.NAME, 'class': 'store-name'}));
						$li.append($('<span />', {text: store.ADDR, 'class': 'store-location'}));
						$li.append($('<span />', {text: store.ADDR2, 'class': 'store-location togglable', style: 'display:block'}));
						$li.append(
							$('<span />', {text: 'Phone: '+store.PHONE, 'class': 'store-phone'}).append(
								$('<span />', {text: ' Fax: '+store.COMMFAX, 'class': 'store-fax togglable'})
							));
						$li.append($('<strong />', {text: formattedStoreHours, 'class': 'store-hours togglable'}));
						$li.append($('<ul />', {'class':'store-links'}));

						$storeLinks = $li.find('.store-links');
						/* View on map link */
						$storeLinks.append( $('<li />').append( $("<a />", {
								'class': 'view-on-map togglable',
								text: "View on Map",
								href: '#view-on-map'
							})));
						/* Check for Canadian Stores */
						if (store.COUNTRY == 'US' && !isSelectedStore){
							$storeLinks.append($('<li />').append( $("<a />", {
								'class': 'make-store',
								text: "Make This Your Store",
								href: '#select-store'
							})));
						} else if (store.COUNTRY == 'CA'){
							$storeLinks.append($('<li />').append( $("<a />", {
								'class': 'canadian-store',
								href: 'http://www.lowes.ca/',
								text: "http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/Visit Lowes.ca",
								target: "_blank"
							})));
						}

						/* Technical Debt for get directions functionality. */
						directionsAddress = [store.ADDR,store.CITY.toTitleCase(),store.STATE,store.ZIP].join();
						if (eventScope.prefix != 'SL-'){
							$directionsLink.attr('href',"/StoreLocatorDisplayView?getDirections="+escape(directionsAddress)).addClass("learn-more");
						}else{
							$directionsLink.addClass("get-directions");
						}
						$storeLinks.append($('<li />',{'class':'togglable'}).append($directionsLink));
						/* Uncomment the following for a link to Weekly Ads for this store */
						/*$storeLinks.append(
							$('<li />',{
								'class':'togglable',
								css:{display:'none'}
							}).append(
								$('<a />',{
									text:'View Weekly Ad',
									href:'http://lowes.shoplocal.com/lowes/?storeref='+store.Number,
									'class':'learn-more'
								})
							));*/
						/* Show/Hide Full Details */
						$li.append( $("<a />", {
							'class': 'more-store-info nav-link',
							html: "<span>Show Details</span>",
							href: '#show-details'
						}).append('<span class="ui-icon ui-icon-triangle-1-s" />'));
						$togglables = $li.find('.togglable');
						$togglables.hide();
						/* Events for this Store Listing */
						$li.delegate('.view-on-map','click', function (e){
							e.preventDefault();
							var $mapWrap = $('#'+eventScope.prefix+"map-wrap"),
								$storeDetails = $li.clone(true,true),
								$backButton = $('<a class="back-to-list btn" href="#Header-map-result">Back to List</a>').bind('click', function (e){
									e.preventDefault();
									$mapWrap.hide();
								});
							$storeDetails.find('.view-on-map').parent().remove();
							$mapWrap.find('.store-details').empty().append($storeDetails.html()).prepend($('<div class="ui-title-bar" />').append($backButton).append('<span class="close"><span>Close</span></span>'));
							/* This event didn't stay bound */

							$mapWrap.delegate('.make-store','click', function (e){
								e.preventDefault();
								eventScope.updateStore(store.KEY);
							});

							/* Remove all markers. */
							if ( eventScope.markersArray.length ){
								$.each( eventScope.markersArray, function (i, el){ el.setMap(null); });
							}
							/* Make sure this marker is included back in the array for other functions. */
							eventScope.markersArray.push(marker);
							$('#'+eventScope.prefix+"map-wrap").show();
							/* This is necessary in the pop-up or the map will be ugly. */
							google.maps.event.trigger(eventScope.map, 'resize');
							eventScope.map.setCenter(marker.getPosition());
							eventScope.map.setZoom(14);
							/* Add this marker back in. */
							google.maps.event.clearListeners(marker, 'click');
							marker.setMap(eventScope.map);
						});
						$li.delegate('.get-directions','click',function(e){
							eventScope.clear();
							document.getElementById(eventScope.prefix+'map-directions-to').value = directionsAddress;
							eventScope.setDisplayMode('directions');
						});
						$li.delegate('.make-store','click', function (e){ e.preventDefault(); eventScope.updateStore(store.KEY); });
						$li.delegate('.more-store-info', 'click',
							function (e){
								e.preventDefault();
								var $this = $(this),
									bookmarkY = eventScope.scrollList.data('jsp').getContentPositionY();

								if ($this.text().indexOf("Show") !== -1){
									$this.html("<span>Hide Details</span>").append('<span class="ui-icon ui-icon-triangle-1-n" />');
									$togglables.show();
								}else {
									$this.html("<span>Show Details</span>").append('<span class="ui-icon ui-icon-triangle-1-s" />');
									$togglables.hide();
								}
								eventScope.scrollList.data('jsp').reinitialise();
								eventScope.scrollList.data('jsp').scrollByY(bookmarkY);
							});
						$li.bind('mouseenter mouseleave', function (e){
							var size = (e.type == 'mouseenter') ? 40 : 32;

							marker.setIcon(new google.maps.MarkerImage(marker.getIcon().url,'','','', new google.maps.Size(size,size)));
						});
						this.$listItem = $li;
					}
				}
			});

			/**
			 * Private Data List Item for a disambiguation that has proper event binding.
			 */
			var Disambiguation = Lowes.UI.DataListItem.extend({
				init: function (address, eventScope){
					var $address,itemId;

					if (address !== 'undefined'){
						$address = $('<a />', {
								'class':'search-address',
								href:'#search-address',
								text:address.formatted
							}).bind('click', {searchInput:{value:[address.location.lat(),address.location.lng()].join(', ')}},function (e){
								e.preventDefault();
								eventScope.searchHandler(e);
							});
						itemId = [eventScope.prefix, 'ambiguation-', address.sortOrder].join('');
					}else{
						$address = $("<span>No address provided</span>");
						itemId = '';
					}
					/* Call the parent Class's init(). */
					this._super({
						id: itemId,
						html: $address,
						'class': 'disambiguation'
					});
				}
			});

			/**
			 * Class for locating stores / integration with Google Maps
			 *
			 * @class Lowes.StoreLocator
			 */
			var StoreLocator = Class.extend({

				/**
				 * Formally known as the Google Fusion Table query builder.
				 *
				 * @param {object} queryDef				An object containing definitions for the ajax call
				 * @param {string} queryDef.place		search term / lat - long / store ID
				 * @param {string} queryDef.count		override on default count. Number of stores to return
				 * @param {function} callback			The function to run after the query returns
				 */
				buildQuery: function (queryDef, callback){
					var service = '/IntegrationServices/resources/storeLocator/json/v2_0/stores?langId=-1&storeId=10702&catalogId=10051&',
						SL = this;
					queryDef.count = queryDef.count || 25;
					$.ajax({
						url: service + Lowes.Utils.urlQueryString(queryDef),
						type: 'GET',
						dataType: 'json',
						error: function(response){
							SL.failure();
						},
						success: function(response){
							// Adjust Current Location based on type of search
							if (typeof queryDef.place === "object" && response.Location.length > 0) {
								// working with store id search
								SL.currentLocation = response.Location[0].LLAT + ', ' + response.Location[0].LLON;
							} else {
								// working with location data search
								SL.currentLocation = queryDef.place;
							}
							SL.responseHandler(response);
							$('.ui-throbber').hide();
							
						}
					});
				},

				/** Wipe the slate clean */
				clear: function (){
					/* Unhook any directions */
					this.directionsDisplay.setMap(null);
					/* Clear markers and their infowindows */
					if ( this.currentInfoWindow ){ this.currentInfoWindow.close(); }
					if ( this.markersArray.length ){
						$.each( this.markersArray, function (i, marker){ marker.setMap(null); });
					}
					this.markersArray = [];
					this.locationsResult = [];
					/* Empty the scrollable result list and reinit */
					this.listContent.empty();
					this.scrollList.data('jsp').reinitialise();
				},
				CalcDistanceBetween: function (lat1, lon1, lat2, lon2) {
					//Radius of the earth in:  1.609344 miles,  6371 km  | var R = (6371 / 1.609344);
					var R = 3958.7558657440545; // Radius of earth in Miles
					var dLat = this.toRad(lat2-lat1);
					var dLon = this.toRad(lon2-lon1);
					var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(this.toRad(lat1)) * Math.cos(this.toRad(lat2)) * Math.sin(dLon/2) * Math.sin(dLon/2);
					var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
					var d = R * c;
					return d;
				},

				toRad: function (Value) {
					/** Converts numeric degrees to radians */
					return Value * Math.PI / 180;
				},
				/** Get the distances between points and update the map with sorted results. */
				distanceCallback: function (response, status){
					var i, results, sortedResults = [], locationsToRemove = [];

					if (status != google.maps.DistanceMatrixStatus.OK) {
						//console.log('Error was: ' + status);
					} else {
						results = response.rows[0].elements;

						for ( i = 0; i < results.length; i++) {
							if (results[i].status == 'ZERO_RESULTS'){
								//console.log("Error in the Distance Callback for "+response.originAddresses[0]);
								var dist = this.CalcDistanceBetween(this.locationsResult[i].LLAT, this.locationsResult[i].LLON, this.uLat,this.uLng );

								// Convert distance to miles with two decimal points precision
								if (isNaN(dist)) {
									this.locationsResult[i].distVal = 0;
									this.locationsResult[i].distTxt = "";
								}
								else {
									this.locationsResult[i].distVal = dist.toFixed(2)*1609;
									this.locationsResult[i].distTxt = dist.toFixed(2) + " mi";
								}
							}else{
								this.locationsResult[i].distVal = results[i].distance.value;
								this.locationsResult[i].distTxt = results[i].distance.text /*+ " in " + results[i].duration.text*/;
							}
						}
					}
					/* sort by distanceVal not distanceTxt */
					sortedResults = this.locationsResult.sort( function (pointA, pointB){
						return (pointA.distVal - pointB.distVal);
					});
					this.map.setCenter(new google.maps.LatLng(sortedResults[0].LLAT, sortedResults[0].LLON));
					/* Handle single results differently */
					if (sortedResults.length == 1){
						this.map.setZoom(14);
					}else{
						this.map.setZoom(10);
						/* Uncomment below and remove zoom to fit map to boundary of 5 closest store */
						/*var bounds = new google.maps.LatLngBounds();

						for (var i = 0; i < 5; i++) {
						  bounds.extend(new google.maps.LatLng(sortedResults[i].LAT, sortedResults[i].LNG));
						}
						this.map.fitBounds(bounds);*/
					}
					/* This kicks off a refresh of the map data */
					this.dropMarkers(sortedResults);
					this.populateSidebar(sortedResults);
				},

				/** Put markers on the map */
				dropMarkers: function (locationsResultSet){
					var locationJsonObj, i, marker, markerIcon, markerLatLng,
						SL = this,
						infowindow = new google.maps.InfoWindow();

					this.markersArray = [];
					this.currentInfoWindow = null;
					if (locationsResultSet.length) {
						for ( i=0; i < locationsResultSet.length; i++ ) {
							locationJsonObj = locationsResultSet[i];
							markerLatLng = new google.maps.LatLng(locationJsonObj.LLAT, locationJsonObj.LLON);
							marker = new google.maps.Marker({
								position: markerLatLng,
								map: this.map,
								title: locationJsonObj.NAME,
								animation: google.maps.Animation.DROP,
								shadow: this.originIconShadow
							});
							if (i < 26) {
								markerIcon = (parseFloat(Lowes.User.getCurrentStore()['number']) == locationJsonObj.KEY) ? this.yourStoreIconPrefix + this.alphabet[i] + this.originIconSuffix : this.originIconPrefix + this.alphabet[i] + this.originIconSuffix;
								marker.setIcon(new google.maps.MarkerImage(markerIcon));
							} else {
								marker.setIcon(new google.maps.MarkerImage(this.originIcon));
							}
							/** Fun times with scope */
							(function(marker, store){
								google.maps.event.addListener( marker, 'click', function(){
									var dL = new Lowes.UI.DataList(),
										storeDetails = new StoreListing(store, SL).$listItem;

									storeDetails.unbind("mouseenter mouseleave");
									storeDetails.find('.togglable').show();
									dL.load(storeDetails);
									infowindow.setContent(dL.$dataList[0]);
									infowindow.open(SL.map,marker);
								});
								google.maps.event.addListener( marker, 'mouseover', function(){
									marker.setIcon(new google.maps.MarkerImage(marker.getIcon().url,'','','', new google.maps.Size(40,40)));
								});
								google.maps.event.addListener( marker, 'mouseout', function(){
									marker.setIcon(new google.maps.MarkerImage(marker.getIcon().url,'','','', new google.maps.Size(32,32)));
								});
							}(marker, locationJsonObj));

							this.markersArray.push(marker);
						}
					}
				},

				/**
				 * Handle all public errors gracefully.
				 */
				failure: function (error){
					var error = error || this.genericError;
					this.listContent.html(error);
					this.scrollList.data('jsp').reinitialise();
				},

				/**
				 * Handle the directions lookup.
				 * @todo suggested routes, images for turn-by-turn
				 * @param {string} start	From Location
				 * @param {string} end		To Location
				 */
				getDirections: function (start,end){
					var SL = this,
						directionsService = new google.maps.DirectionsService(),
						request = {
							origin : start,
							destination : end,
							travelMode : google.maps.DirectionsTravelMode.DRIVING,
							provideRouteAlternatives: true
						};
					this.directionsDisplay.setMap(this.map);
					//this.directionsDisplay.setPanel(this.resultList);
					directionsService.route(request, function(response, status) {
						var i, myRoute, drivingFrom, drivingFromStreet, drivingFromAddress, drivingTo, drivingToStreet, drivingToAddress,
							$directions = $("<ol />");

						if (status == google.maps.DirectionsStatus.OK) {
							SL.directionsDisplay.setDirections(response);
							myRoute = response.routes[0].legs[0];

							drivingTo = myRoute.end_address.split(',');
							drivingToStreet = drivingTo[0];
							drivingTo.splice(0,1);
							drivingToAddress = drivingTo.join();

							drivingFrom = myRoute.start_address.split(',');
							drivingFromStreet = drivingFrom[0];
							drivingFrom.splice(0,1);
							drivingFromAddress = drivingFrom.join();

							for (i = 0; i < myRoute.steps.length; i++) {
								$directions.append($('<li class="driving-leg"><span class="instructions">'+myRoute.steps[i].instructions+'</span> <span class="distance">'+ myRoute.steps[i].distance.text + "</span>"));
							}
							/* parse first bold item in directions in order to achieve background image; right, left, merge */

							SL.listContent.html($('<h5 class="driving-summary">Driving Directions to '+ myRoute.end_address +'</h5><a href="javascript:window.print();" class="ui-print secondary button"><span><em class="ui-icon ui-icon-print"></em> Print</span></a>').add($('<div class="driving-point"><img src="../../../../images/map/markerSelectedA.png"/*tpa=http://www.lowes.com/images/map/markerSelectedA.png*/ /> <strong>'+drivingFromStreet+"</strong> <span>"+drivingFromAddress+"</span></div>")).add($directions).add($('<div class="driving-point"><img src="../../../../images/map/markerSelectedB.png"/*tpa=http://www.lowes.com/images/map/markerSelectedB.png*/ /> <strong>'+drivingToStreet+"</strong> <span>"+drivingToAddress+"</span></div>")));
							SL.scrollList.data('jsp').reinitialise();
						}
					});
				},

				/** @constructor */
				init: function (prefix, displayMode){
					/** Used to separate instances. */
					this.prefix = prefix || '';
					/* A generic error message */
					this.genericError = '<h2 class="error-title">We\'re Sorry</h2><p class="error">We weren\'t able to find a store for this entry</p><p class="error">Please make sure that:<ul class="error"><li>A city and store were entered</li><li>The city and street names were spelled correctly</li><li>a valid 5-digit ZIP code was entered</li></ul>';
					this.mapList = $(document.getElementById(this.prefix+"map-list"));
					this.scrollList = this.mapList.jScrollPane({hideFocus : true});
					this.listContent = this.scrollList.data('jsp').getContentPane();
					/* Create a new Google map that will be tied to this locator. */
					this.map = new google.maps.Map( document.getElementById( this.prefix+"map-canvas" ), {
						center: new google.maps.LatLng( 34.9983818, -99.99967040000001 ) /* Initially center of US */,
						zoom: 4 /* Fit the US in view */,
						mapTypeId: google.maps.MapTypeId.ROADMAP
					});
					this.geocoder = new google.maps.Geocoder();
					/* Contains a collection of marker objects. */
					this.markersArray = [];
					/* Contains a collection of store objects. */
					this.locationsResult = [];
					this.currentLocation;
					this.uLat;
					this.ULng;
					/* Map marker icons */
					this.originIcon = "../../../../images/map/marker.png"/*tpa=http://www.lowes.com/images/map/marker.png*/;
					this.originIconShadow = "../../../../images/map/markerShadow.png"/*tpa=http://www.lowes.com/images/map/markerShadow.png*/;
					this.originIconPrefix = "/images/map/marker";
					this.originIconSuffix = ".png";
					this.yourStoreIcon = "../../../../images/map/markerSelected.png"/*tpa=http://www.lowes.com/images/map/markerSelected.png*/;
					this.yourStoreIconPrefix = "/images/map/markerSelected";
					/* Used as a limit with the where clause search, we are currently using the orderby clause - 'sscarrol' */
					//this.miles = 75;
					//this.meters =  this.miles * 1.609344 * 1000;
					/* Local copy of the alphabet array */
					this.alphabet = Lowes.Utils.alphabet;
					/* Use the public function to call the initial display mode. */
					this.setDisplayMode( displayMode );
					/* This needs to be available globally in order to clear directions */
					this.directionsDisplay = new google.maps.DirectionsRenderer({draggable: true });
				},

				/** Update the results list with store locations */
				populateSidebar: function (){
					var dL, i, storeList = [];

					if (this.locationsResult.length) {
						dL = new Lowes.UI.DataList({listType:"ol"});
						for (i = 0; i < this.locationsResult.length; i++) {
							/* Load our StoreListing objects into our data list adding sort order. */
							this.locationsResult[i].distanceOrder = i;
							dL.load( new StoreListing(this.locationsResult[i], this).$listItem );
						}
						/* Write the DataList to the DOM */
						this.listContent.html(dL.$dataList);
						this.scrollList.data('jsp').reinitialise();
					}
				},

				/**
				 * Generic response handler for queries
				 */
				responseHandler: function (response){
					var service = new google.maps.DistanceMatrixService(),
						destinations =[],
						SL = this,
						locationObj = (response) ? response.Location : "",
						locationSize =  locationObj.length,
						i,
						$SLResults = $('#SL-map-results');

						//tests if locationObj is empty as a result of response being null upon success
						if(!locationObj) {
							SL.failure();
							$('.ui-throbber').hide();
							return;
						}

					this.locationsResult = locationObj;
					if (locationSize) {
						for (i = 0; i < locationSize; i++) {
							//console.log(locationObj);
							destinationLatLng = new google.maps.LatLng(locationObj[i].LLAT, locationObj[i].LLON);
							destinations[i] = destinationLatLng;
						}
						this.geocoder.geocode({ 'address' : SL.currentLocation },
							function(results, status) {
								service.getDistanceMatrix({
									origins : [ SL.currentLocation ],
									destinations : destinations,
									travelMode : google.maps.TravelMode.DRIVING,
									unitSystem : google.maps.UnitSystem.IMPERIAL,
									avoidHighways : false,
									avoidTolls : false
								}, function (response, status){
									// Calculate Distances and Show Datalist
									SL.distanceCallback(response, status);
									// Hide Loader
									if ($SLResults.length) {
										$SLResults.find('.ui-throbber').hide();
									}
									$('#HeaderLocator').find('.ui-throbber').hide();
								});
							});
					} else {
						this.failure();
					}
				},

				/**
				 *	Primary Search Event handler
				 *
				 *  @param {event} e Event object
				 *  @param {string} e.data.searchInput Use event data to pass the searchInput either from the input or the disambiguation
				 *  @todo Remove the need for the event data, prefer just a string as input.
				 */
				searchHandler: function (e){
					var searchInput = e.data.searchInput, SL = this, searchString, queryDef, dL,
						$SLResults = $('#SL-map-results');

					this.clear();
					this.setDisplayMode('locations');
					if (searchInput) { searchString = $.trim(searchInput.value.replace("'", "\\'")); }

					// Handle Creation, Initilization + Visibility of Loader
					// Determine if we're on the Store Locator Page and that masthead location search is not in use
					if ($SLResults.length && $SLResults.find('#SL-map-list').length && !$('#HeaderLocator').is(':visible')) {
						// add loader for store locator page if it doesn't exist
						if ($SLResults.find('.ui-throbber').length === 0) {
							$SLResults.find('#SL-map-list').before('<div class="ui-throbber""></div>');
						}
						// Show the loader then initialize the spin
						$SLResults.find('.ui-throbber').show().spin();
					} else {
						// If we're working with the Masthead Location Searchgit
						// Show the loader then initialize the spin
						$('#Header-map-results').find('.ui-throbber').show().spin();
					}

					if (searchString) {
						if (!isNaN(searchString) && searchString.length <= 4) {
							/* Assuming Store Search */
							searchString = Lowes.Utils.zeroPad(searchString, 4);
							queryDef = {
								'place' : [searchString]
							};
							SL.currentLocation = searchString;
							this.buildQuery(queryDef);
						}else{
							/* Create a new Datalist for the disambiguation */
							dL = new Lowes.UI.DataList();
							this.geocoder.geocode({ 'address' : searchString },
								function(results, status) {
									var i, geocodeambiguation, queryDef,
										$errorTitle = $('<h2 />', {'class':'error-title',text:'Did you mean:'}),
										j, addressComponent, everythingOk = false;

									if (status == google.maps.GeocoderStatus.OK) {
										if (results.length > 1) {
											/* Disambiguate */
											for (i = 0; i < results.length; i++) {
												for (j=0;j<results[i].address_components.length;j++){
													addressComponent = results[i].address_components[j].short_name;
													if (isDriveable(addressComponent)){
														dL.load(new Disambiguation({formatted:results[i].formatted_address, location:results[i].geometry.location, sortOrder:i}, SL).$listItem);
													}
												}
											}
											if (!dL.$dataList.children().length) {
												SL.listContent.html(SL.genericError);
											}else{
												SL.listContent.html(dL.$dataList).prepend($errorTitle);
											}
											SL.scrollList.data('jsp').reinitialise();

											// Hide Loader
											if ($SLResults.length) {
												$SLResults.find('.ui-throbber').hide();
											}
											$('#HeaderLocator').find('.ui-throbber').hide();

										} else if (results.length == 1) {
											var custZipCode = null;
											/* This is good, we only want one result */
											for (j=0;j<results[0].address_components.length;j++){
												addressComponent = results[0].address_components[j].short_name;
												if (results[0].address_components[j].types[0] === 'postal_code'){
													custZipCode = addressComponent;
												}
												if (isDriveable(addressComponent)){
													everythingOk = true;
												}
											}
											if (everythingOk){
												SL.uLat = results[0].geometry.location.lat();
												SL.uLng = results[0].geometry.location.lng();
												SL.currentLocation = searchString;

												var geoInt,
													geoAttempts = 0,
													geoDeferred = new $.Deferred(),
													geoData = function () {
												    	geoInt = setInterval(function () {
													    	if (geoAttempts < 2) {
													    		geoAttempts++;
													    		SL.geocoder.geocode({
																	'location' : {lat: SL.uLat, lng: SL.uLng}
																}, function(results, status) {
																	if (status == 'OK') {
																		for (var i = 0; i < results[0].address_components.length; i++) {
																			if (results[0].address_components[i].types == 'postal_code') {
																				custZipCode = results[0].address_components[i].short_name;
																			}
																		}
																		geoDeferred.resolve({
																			'place' : custZipCode
																		});
																	}
																});
													    	} else {
													    		geoDeferred.resolve(null);
													    	}	
													    }, 700);

												   return geoDeferred.promise();
												};

												geoData().done(function (place) {
													clearInterval(geoInt);
													geoAttempts = 0;
													if (place) {
												      	SL.buildQuery(place);
													} else {
														SL.failure();
														$('.ui-throbber').hide();
													} 
												});
											} else {
												SL.failure();
												$('.ui-throbber').hide();
											}
										}
									} else {
										/* Fail: unable to geocode, no results */
										//console.log("Geocode was not successful for the following reason: " + status);
										SL.failure();
										$('.ui-throbber').hide();
									}
								}); /* End Geocoding function scope */
						}
					} else {
						this.failure();
					}
				}, /* End searchHandler function scope */

				/**
				 * Set the Locator's display mode,
				 * Right now, just 'locations' and 'directions' but can be extended later
				 *
				 * @param {string} displayMode Either 'locations' or 'directions'
				 */
				setDisplayMode: function (displayMode){
					/* SL is a pointer back to this, to keep the reference clean in the handler */
					var SL = this,
						$focusField,
						$mapResults = $("#"+this.prefix+"map-results"),
						$mapList = $("#"+this.prefix+"map-list"),
						$mapInput = $("#"+this.prefix+"map-input"),
						$getDirections = $('#'+this.prefix+'map-get-directions'),
						$switchDirections = $('#'+this.prefix+'map-switch-directions'),
						$inputFrom = $('#'+this.prefix+'map-directions-from'),
						$inputTo = $('#'+this.prefix+'map-directions-to'),
						$searchInput = $('#'+this.prefix+'map-search'),
						$submitButton = $("#"+this.prefix+"map-search-submit"),
						searchData = {searchInput: $searchInput[0] };

					/* Hide all display modes. */
					$('#'+this.prefix+'map-input > div').css('display','none');
					/* Unbind events */
					$getDirections.unbind();
					$submitButton.unbind('click');
					$searchInput.unbind('keypress');
					$switchDirections.unbind('click');
					$inputFrom.unbind('keypress');
					$inputTo.unbind('keypress');
					switch(displayMode){
						case 'directions':
							/* The directions form elements */
							this.$inputPanel = $("#"+this.prefix+"map-directions");
							$inputFrom.val( $.trim($searchInput.val()).length ? $searchInput.val() : '');
							$focusField = $inputFrom;
							/** Events */
							/* Get Directions */
							$getDirections.bind('click', function (e){
								var from = $.trim($inputFrom.val()),
									to = $.trim($inputTo.val());

								if(from.length && to.length){
									SL.clear();
									SL.getDirections(from, to);
								}
								return false;
							});
							/* Pressing enter in the input fields */
							$inputFrom.add($inputTo).bind('keypress', function (e){
								if (e.which == 13) {
									$getDirections.click();
									return false;
								}
							});
							/* Switch Input Values */
							$switchDirections.bind('click', function (e){
								var from = $inputFrom.val(),
									to = $inputTo.val();

								$inputFrom.val(to);
								$inputTo.val(from);
								SL.clear();
								SL.getDirections($inputFrom.val(), $inputTo.val());
							});
							/* Back Button */
							this.$inputPanel.delegate('a[href="#'+this.prefix+'map-default"]', 'click', searchData, function (e){
								e.preventDefault();
								if ($.trim(e.data.searchInput.value) == ''){
									SL.clear();
									SL.setDisplayMode('locations');
								}else{
									SL.searchHandler(e);
								}
							});
							/* Drop a marker for the store as the "to" address. */
							this.geocoder.geocode({ 'address' : $inputTo.val() },
								function (results, status){
									if (status == google.maps.GeocoderStatus.OK) {
										SL.clear();
										var marker = new google.maps.Marker({
											map: SL.map,
											position: results[0].geometry.location
										});
										/* Should be the "to" address, so make it the "B" icon. */
										marker.setIcon(new google.maps.MarkerImage(SL.yourStoreIconPrefix + "B" + SL.originIconSuffix));
										/* Focus the map on this marker */
										SL.map.setCenter(results[0].geometry.location);
										SL.map.setZoom(10);
										/* Add this marker to the array or the clear routine won't work. */
										SL.markersArray = [marker];
									} else {
									  //  console.log("Geocode was not successful for the following reason: " + status);
									}
								});
							break;
						case 'locations':
						default:
							/* The default locations form elements */
							this.$inputPanel = $("#"+this.prefix+"map-default");
							$focusField = $searchInput;
							/** Events */
							/* Trigger click and enter to perform the main search function.
							 * @todo Use a submit function */
							$submitButton.bind( 'click', searchData, function (e){ SL.searchHandler(e); return false; });
							$searchInput.bind( 'keypress', searchData, function (e){
								if ( e.which == 13 ) {
									 e.preventDefault();
									 SL.searchHandler(e);
								}
							});
							break;
					}

					/* internal reference to display mode */
					this.displayMode = displayMode;
					/* show form elements */
					this.$inputPanel.css('display','block');
					$mapList.height($mapResults.height() - (($mapInput.height() || 80) + 1));
					this.scrollList.data('jsp').reinitialise();
					$focusField.focus();
				},

				/**
				 * This submits the form that sets the selected store cookie.
				 * @param {number} vStoreNumber The store number to set the cookie for.
				 */
				updateStore: function (vStoreNumber){
					var $input,
						url;
					if (typeof document.UpdateLocalStoreForm != 'undefined'){
						/* Submit from the Store Locator page */
						url = this.removeStoreQuery(document.UpdateLocalStoreForm.URL.value);
						document.UpdateLocalStoreForm.storeNumber.value = vStoreNumber;
						document.UpdateLocalStoreForm.URL.value = url;
						document.UpdateLocalStoreForm.submit();
					}else{
						/* Fake the form to submit a store number. */
						$input = $('<input type="hidden" name="storeNumber" value="'+vStoreNumber+'"/>').add($('<input type="hidden" name="URL" value="'+location.href+'"/>'));
						$('<form action="/LowesUpdateLocalStoreCmd" />').append($input).appendTo('body').submit();
					}

					// Trigger tealium Tag for Selecting new Store
					Lowes.Metrics.ltag({
						event_type: "change-store",
						link_name: "Make This Your Store",
						change_store_number: vStoreNumber
					}, "tealium");
				},
				/**
				 * Removes the store_code query from a URL
				 * @param {string} URL
				 */
				removeStoreQuery: function(url){
					var urlString = Lowes.Utils.urlQueryObject(url);
					if(urlString.store_code){
						url = url.replace(/store_code=\d*&?/g, '');
					}
					return url;
				}
			});

			// Add the Store Locator Class to Lowes namespace
			Lowes.StoreLocator = StoreLocator;
			// Expose the updated Lowes namespace to the world
			window.Lowes = Lowes;
	})(jQuery);
}

/* ../global/masthead.js */

/**
 * @fileOverview Navigation and site-wide template scripts.
 */

(function ($) {
	var Lowes = window.Lowes || {}, // Global Namespace for Lowe's objects
		$header = $("#header-block"), // Caching the jQuery selection for the header to reduce lookups.
		$page = $("#page-block"), // Caching the jQuery selection for the header to reduce lookups.
		$searchForm = $('#frmQuickSearch'), // Caching the jQuery selection for the quick search form to reduce lookups.
		$Ntt = $('#Ntt');

	/**
	 * The top portion of every page, navigation, user info, cart details, and search.
	 * @namespace
	 */
	Lowes.Masthead = {
		/**
		 * Pulls store information from the Facade based on store number.
		 * This has been done to synchronize this masthead information with
		 * the same service used on the "Find a Store" page.
		 */
		getStoreInfo: function(storeNumber) {

			var useQuery = {
				'place' : [storeNumber],
				'count' : 1
			},
			service = '/IntegrationServices/resources/storeLocator/json/v2_0/stores?langId=-1&storeId=10702&catalogId=10051&';

			$.ajax({
				url: service + Lowes.Utils.urlQueryString(useQuery),
				type: 'GET',
				dataType: 'json',
				success: function(response){
					Lowes.Masthead.setStoreInfo(response);
				}
			});
		},

		// Takes response from getStoreInfo and fills Lowes.Masthead.storeInfo
		setStoreInfo: function(response) {
			var myData = response.Location[0];

			Lowes.Masthead.storeInfo.shortName = myData.STORENAME;
			Lowes.Masthead.storeInfo.state = myData.STATE;
			Lowes.Masthead.storeInfo.zip = myData.ZIP;
			Lowes.Masthead.storeInfo.number = myData.KEY;
			Lowes.Masthead.storeInfo.address = myData.ADDRESS;
			Lowes.Masthead.storeInfo.city = myData.CITYFORMATTED;
			Lowes.Masthead.storeInfo.hours = myData.StoreHours; // object
			Lowes.Masthead.storeInfo.phone = myData.PHONE;
			Lowes.Masthead.storeInfo.fax = myData.COMMFAX;

			/* Extra */
			Lowes.Masthead.storeInfo.name = Lowes.Masthead.storeInfo.shortName + ", " + Lowes.Masthead.storeInfo.state;
			Lowes.Masthead.storeInfo.postalAddress = Lowes.Masthead.storeInfo.address + " " + Lowes.Masthead.storeInfo.city + ", "+ Lowes.Masthead.storeInfo.state;

			Lowes.Masthead.addStoreInfoBlock();
		},

		// Creates The Store Info Block with Default Information from the Cookie (Without the Hours)
		addStoreInfoBlockFromCookie: function() {
			
			var myInfo = Lowes.User.CurrentStore;

			storeDetailsBlock = [
				"<div id='store-info-content'>",
					"<a href='#NavigationClose' class='close'>",
						"<span>Close</span>",
					"</a>",
					"<p>",
						myInfo.name,
					"</p>",
					"<ul>",
						"<li class='store_info_address'>",
							myInfo.address.replace(/##/g,","),
						"</li>",
						"<li class='store_info_address'>",
							myInfo.city, ", ", myInfo.state, " ", myInfo.zip, ", Store #", myInfo.number, 
						'</li>',
						'<li class="StoreInfoPhone">&nbsp;</li>',
						'<li class="StoreInfoHours">&nbsp;</li>',
					'</ul>',
					'<ul id="store-info-links">',
						'<li>',
							'<a href="http://www.lowes.com/StoreLocatorDisplayView" class="change-store">',
								'Change Your Store',
							'</a>',
						'</li>',
						'<li>',
							'<a href="http://www.lowes.com/StoreLocatorDisplayView" class="get-directions learn-more">',
								'Get Directions',
							'</a>',
						'</li>',
					'</ul>',
				'</div>',
				'<div class="ui-north-arrow-bg"></div>',
				'<div class="ui-north-arrow"></div>'].join('');
			/* Inject the Store Details into it's wrapper' */
			$("#store-info-block").html(storeDetailsBlock);
		},
		
		// Adds Print Block and Store Hours & Phone Number to Store Information Tooltip
		addStoreInfoBlock: function() {
			var myInfo = Lowes.Masthead.storeInfo, 
			    storePhone = '',
			    myHours = Lowes.Masthead.formatStoreHours(Lowes.Masthead.storeInfo.hours);

			/* Code needed to append store info to print block until masthead goes dynamic.
			 * @todo Handlebars Template or add it to the DOM
			 */

			printText = [
				"<div class='print-container'>",
					"<h5>",
						myInfo.name,
					"</h5>",
					"<ul>",
						"<li class='address'>",
							myInfo.address,
						"</li>",
						"<li class='phone'>",
							myInfo.phone,
						"</li><li class='hours'>Hours of Operation:</li><li>",
							myHours,
						"</li>",
					"</ul>",
				"</div>"].join('');
			/* Inject the printText into it's wrapper */
			$('#print-masthead .print-store').html(printText);

			storePhone = [
				"Phone: ", myInfo.phone , " | Fax: " , myInfo.fax , myInfo.cbcPhone?"</li><li>Pro Services Desk: " + myInfo.cbcPhone:""].join('');
			
			myHours = myHours.replace(/##/g,",").replace("M-Sa","Mon. - Sat.").replace("Su","Sun").replace("Sunn","Sun");
			
			/* Inject the Store Phone Number into it's wrapper' */
			$(".StoreInfoPhone").html(storePhone);
			/* Inject the Store Hours into it's wrapper' */
			$(".StoreInfoHours").html(myHours);

		},

		// Format store hours from facade call
		formatStoreHours: function(storeHours) {
			var week = {0:'Monday', 1:'Tuesday', 2:'Wednesday', 3:'Thursday', 4:'Friday', 5:'Saturday', 6:'Sunday'},
			days = [], ret, amOrPm = '', hoursString = '';
			$.each(week, function() {
				var openHour  = parseInt(storeHours[this + '_Open'], 10),
				closeHour = parseInt(storeHours[this + '_Close'], 10),
				openMin   = ':' + storeHours[this + '_Open'].substring(3,5),
				closeMin  = ':' + storeHours[this + '_Close'].substring(3,5);

				// don't display 00 minutes
				openMin  = (openMin  == ':00') ? '' : openMin;
				closeMin = (closeMin == ':00') ? '' : closeMin;

				if(openHour == closeHour) { // closed for the day
					var hours = 'Store Closed';
				}
				else if(openHour == 0 && closeHour == 24) { // open 24 hrs
					var hours = 'Open 24 hours';
				}
				else { // convert from 24 hr and add AM or PM
					if(openHour == 24 || openHour == 00 || openHour == 12) {
						if(openHour==12){
				              amOrPm = ' PM';
				            }else{
				              amOrPm = ' AM';
				            }
		            	openHour = '12' + openMin + amOrPm;
						closeHour = closeHour < 12 ? closeHour + closeMin + ' AM' : (closeHour-12) + closeMin + ' PM';
					}
					else if(closeHour == 24 || closeHour == 00) {
						closeHour = '12' + closeMin + ' AM';
						openHour  = openHour  < 12 ? openHour  + openMin + ' AM' : (openHour-12) + openMin + ' PM';
					}
					else {
						openHour  = openHour  < 12 ? openHour  + openMin  + ' AM' : (openHour-12)  + openMin  + ' PM';
						closeHour = closeHour < 12 ? closeHour + closeMin + ' AM' : (closeHour-12) + closeMin + ' PM';
					}

					var hours = '' + openHour + ' - ' + closeHour;
				}

				// consolidate blocks
				if(days.length && hours == days[days.length-1].hours) { // same block
					days[days.length-1].endDay = this.substring(0,3);
				}
				else { // new block
					var obj = {};
					obj.startDay = this.substring(0,3);
					obj.endDay   = this.substring(0,3);
					obj.hours    = hours;
					days.push(obj);
				}
			});

			// remove duplicate start and end days and handle Thursday
			ret = [];
			$.each(days, function(i) {
				ret[i] = {};

				if(this.startDay == this.endDay) { // 1 day block
					ret[i].days = this.startDay;
				}
				else { // multi-day block
					ret[i].days = this.startDay + ' - ' + this.endDay;
				}
				ret[i].days = ret[i].days.replace('Thu', 'Thurs'); // special case for Thursday
				ret[i].hours = this.hours;
			});

			// convert obj to string
			$.each(ret, function(index, item) {
				hoursString += item.days + ' ' + item.hours;
				if (index < (ret.length - 1)) {
					hoursString += ', ';
				}
			})

			// return ret;
			return hoursString;
		},

		storeInfo: {} // populated by facade
	};
	Lowes.cqmylowes = location.protocol+'//'+location.host+'/en_us/mylowes.html';

	/* Check that there is a masthead ( are we in an iframe? ) */
	if ($header.length){
		/**
		 * Refreshes the dynamic portions of the masthead.
		 * Run this anytime the masthead changes drastically.
		 * @todo Lighten this up by moving some of the delegated events out.
		 */
		Lowes.Masthead.refresh = function (){
			var cartCookie,
				cartInfo,
				/* Salutation vars */
				$defaultSalutation = $('#lowes-salutation-default'),
				$activeSalutation = $('#lowes-salutation-active'),
				maxNameLength = 16,
				custName = Lowes.Cookie.get('firstName'),
				greeting,
				busName,
				userSegment,
				CBCCookie,
				/* Store Search */
				$storeSearchDefault = $('#lowes-store-default'),
				$storeSearchActive = $('#lowes-store-active'),
				$storeInfoBlock = $("#store-info-block"),
				$storeChangeBlock = $('#HeaderLocator'), // jQuery collection for the StoreLocator DOM
				toRemove = "Lowe's Of ", // The incoming store name needed this trimmed off
				storeDetails, // The current user's store'
				storeDetailsBlock,
				$weeklyAdLink = $("#nav-weekly-ads"),
				/* Set up a fake event to pass the current store search */
				fakedEvent,
				/* Search by Address in Masthead */
				storeSearchForm = document.getElementById('storeSearchForm'),
				storeSearchInput = document.getElementById('nav-search-input'),
				$storeInfoTrigger, // Initiates the Store Info Popover
				$storeChangeTrigger = $("#nav-store-change, #confirm-store .change-store"), // Initiates the Store Change Popover
				storeChangeClicked = false, // Stores the status of storeChange being clicked so we can delay it if need be
				HeaderLocator = null, // This will hold the instantiation of the Masthead StoLo
				storeDetailsDisabledPages = ["checkout-page", "checkout-login-page"],
				storeDetailsDisabled = $.inArray($('body').attr('id'), storeDetailsDisabledPages) > -1,
				$myLowesSignin = $defaultSalutation.find('.ui-event-signin'),
				$signInURL = $myLowesSignin.attr('href');

			/** Do our best to asynchronously load this store locator */
			function bindStolo(doChange){
				/* Call this when we feel the Google Maps API is fully loaded. */
				function doSearch(){
					if (!!doChange){
						storeChangeClicked = false;
						HeaderLocator.searchHandler(fakedEvent);
						HeaderLocator.scrollList.data('jsp').reinitialise();
					}
				}
				/* This should only be called from the Google load() method. */
				function mapsLoaded(){
					// Clear Google Maps Timeout
					window.clearTimeout(googleMapsTimeout);
					function vizLoaded(){
						// Clear Google Viz Timeout
						window.clearTimeout(googleMapsTimeout);
						/* Ensure the map is loaded then do the search */
						HeaderLocator = new Lowes.StoreLocator("Header-");
						/* if storechange is clicked during load, force dochange to true */
						doChange = doChange || storeChangeClicked;
						doSearch();
					}
					/* This should wait for the maps to load before calling mapsLoaded() */
					if (typeof google === 'object' && typeof google.maps === 'object') {
						google.load('visualization', '1', {"callback" : vizLoaded });
						// Activate Timeout for google viz
						var googleVizTimeout = window.setTimeout(function() {
							if (typeof google.visualization === undefined) redirectToLocatorPage();
						}, 3000);
					} else {
						// If google maps api is not loaded go to store locator page
						$storeChangeBlock.hide();
						redirectToLocatorPage();
					}
				}
				/* Load the map API before searching */
				if (HeaderLocator === null){
					// Check for the google api
					if (typeof google === 'object') {
						/* This should wait for the maps to load before calling mapsLoaded() */
						google.load('maps', '3', {"other_params":"client=gme-loweshomecenters&sensor=false","callback" : mapsLoaded});
						// Activate Timeout for google maps
						var googleMapsTimeout = window.setTimeout(function() {
							if (typeof google.maps === undefined) redirectToLocatorPage();
						}, 5000);
					} else {
						// If google jsapi is not loaded go to store locator page
						$storeChangeBlock.hide();
						redirectToLocatorPage();
					}
				}else{
					/* Map is loaded go ahead and do the search */
					doSearch();
				}
			}

			function redirectToLocatorPage(){
				window.location = "http://www.lowes.com/StoreLocatorDisplayView?storeId=10151&langId=-1&catalogId=10051";
			};

			/** The header popups for store location, ensure we close one when another opens */
			function storePopUpHandler(e){
				storeChangeClicked = true;
				e.preventDefault();
				var $anchor = $(this),
					$popUp = $('#' + $anchor.data('target')),
					offset = "",
					myPOS = 'left top';

				// Populate store info if not already set
				if(!Lowes.Masthead.storeInfo.hasOwnProperty('shortName')) {
					Lowes.Masthead.addStoreInfoBlockFromCookie();
					Lowes.Masthead.getStoreInfo(Lowes.User.CurrentStore.number);
				}

				// This is a temp fix until I can rehaul it a bit
				$popUp.closest('.main-level').css('zIndex', ($anchor.is('#page-block a')) ? 100 : 105);
				$storeChangeBlock.find('.ui-north-arrow')
					.show()
				.end()
				.find('.ui-north-arrow-bg')
					.show();

				if ($anchor.text().toLowerCase().indexOf("change") > -1) {
					if($anchor.hasClass('ui-popup-side')) {
						$storeChangeBlock.hide().find('.ui-north-arrow')
							.hide()
						.end()
						.find('.ui-north-arrow-bg')
							.hide();
						bindStolo(true);
						offset = "275 -20";
						myPOS = 'left top';
					} else {
						$storeInfoBlock.hide();
						bindStolo(true);
						offset = "-249 25";
					}
				}else {
					$storeChangeBlock.hide();
					bindStolo();
					offset = "-118 25";
				}
				$popUp.toggleClass('autozip', $anchor.closest('#confirm-store').length > 0)
					.fadeIn()
					.position({
						my: myPOS,
						at: 'left top',
						of: $anchor,
						offset: offset,
						collision: "none"
					});

				// Activate and/or Show Loader
				$("#Header-map-results .ui-throbber").show().spin();
			}

			/* Helper function, hides the first DOM element, shows the second one. Not robust enough for global consumption. */
			function toggleVisible($toHide, $toShow){
				$toHide.css("display","none");
				$toShow.css("display","block");
			}

			/* This is just to reduce redundant redundancy. */
			function signedInLinkToggler(invalue, outvalue){
				var link = [
					'/', (Lowes.User.isSignedIn() ? invalue : outvalue),
					'?langId=', Lowes.langId,
					'&catalogId=', Lowes.catalogId,
					'&storeId=', Lowes.storeId
				].join('');
				return link;
			}

			// function signedInLinkToggler(invalue, outvalue){
			// 	var link = "http://www.lowes.com/en_us/mylowes/purchases.html";
			// 	return link;
			// }

			/**
			 * Update MyLowes Nav Sign Up Link to prevent backend re-directs
			 * @function
			 */

			function updateMyLowesNavSignUp() {
				var $MyLowesNavSignUpLink = $('#lowes-salutation-default').find('.ui-event-signup');

				if ($MyLowesNavSignUpLink.length) {
					$MyLowesNavSignUpLink.attr('href', 'https://' + window.location.hostname + '/webapp/wcs/stores/servlet/UserRegistrationForm?langId=-1&storeId=10151&catalogId=10051&new=Y&cm_sp=NoDivision-_-MyLowesLP|A0-_-Corp|MyLowes_SignUp_Button');
				}
			};

			updateMyLowesNavSignUp();

			/**
			 * Update Mylowes Main navigation link to prevent backend re-directs
			 * - called when checking for customer name which signifies logged in status
			 * @function
			 */
			function updateMyLowesLinks (isLoggedIn) {
				var $myLowesLink = $('.main-nav').find('.main-level a.my-lowes');
				if (isLoggedIn) {
					$myLowesLink.attr('href', 'https://' + window.location.hostname + '/en_us/mylowes.html?cm_sp=NoDivision-_-MyLowesLP|A0-_-Corp|MyLowes_Masthead');
				} else {
					$myLowesLink.attr('href', 'https://' + window.location.hostname + '/webapp/wcs/stores/servlet/UserRegistrationForm?storeId=10151&langId=-1&catalogId=10051&krypto=w37ixU9mxd51c2GEFG7BpK77x6OcXtxy&cm_sp=NoDivision-_-MyLowesLP|A0-_-Corp|MyLowes_Masthead');
				}
			};

			/* Sign In / Sign Up / Sign Out / Greeting */
			if ( custName ){
				/* Check CBC account name */
				CBCCookie = Lowes.Cookie.get('memberGroup');
				userSegment = Lowes.Cookie.get('userSegment');
				if (CBCCookie) {
					busName = $.parseJSON(CBCCookie).bName;
					/* found business name so replace customer name with bus. name. */
					if (busName){ custName = busName; }
				}
				/* truncate long names */
				custName = ( custName.length >= maxNameLength ) ? custName.substring(0,maxNameLength) + '...' : custName;
				/* Define a greeting. */
				greeting = ['Welcome, ', custName];
				switch ( userSegment ){
					case 'Employee' :
					case 'Realtor' :
						if ( Lowes.Cookie.get('invalidUserSegment') ){
							// this is an invalid user segment cookie, remove the 2 cookies
							Lowes.Cookie.del("invalidUserSegment");
							Lowes.Cookie.del("userSegment");
							break;
						}
						greeting.push(' <span class="user-type">- (',userSegment,')</span>');
						break;
					default:
				}
				/* Append the message to the appropriate place. */
				$activeSalutation.find('.nav-welcome').html(greeting.join(''));
				/* Hide the default salutation and show the custom logged in one. */
				toggleVisible( $defaultSalutation, $activeSalutation );
				/* Update MyLowes link for logged in user */
				updateMyLowesLinks(true);
			} else {
				/* Hide the custom salutation and show the default. */
				toggleVisible( $activeSalutation, $defaultSalutation );
				$myLowesSignin.attr('href', $signInURL+'&URL='+Lowes.cqmylowes);
				/* Update MyLowes link for un-authenticated user */
				updateMyLowesLinks(false);
			}
			/* Sign out link */
			$('#header-block').on('click', '.ui-event-signout', function (e) {
				var windowLocation =  $(this).attr('href'),
					urlParams = {},
					queryString = null;
				e.preventDefault();
				Lowes.Cookie.del('firstName');
				if (typeof cmCreateConversionEventTag == 'function'){
					cmCreateConversionEventTag("MANAGE ACCOUNT SIGNOUT","2","MY LOWES","", cmAttrCreator({10:location.href}));
				}
				// Tealium
				Lowes.Metrics.ltag({
					event_type: "link-click",
					link_name: "sign-out",
					link_type: "masthead"
				}, "tealium");
				if (windowLocation.indexOf('url=TopCategoriesDisplayView') > -1 || windowLocation.indexOf('URL=TopCategoriesDisplayView') > -1) {
					urlParams = Lowes.Utils.urlQueryObject(windowLocation);
					delete urlParams['url'];
					delete urlParams['URL'];
					windowLocation = '/Logoff?' + Lowes.Utils.urlQueryString(urlParams);
				}
				window.location = windowLocation;
			});

			/* Toggle the Purchases link */
			$('#nav-my-order').attr('href',signedInLinkToggler('LowesAllPurchaseHistory','LowesFindOrders'));

			if (Lowes.User.isZippedIn()){
				/** Current store events; info, change */

				/* Store Change */
				// Open Store Change Popup
				$storeChangeTrigger.on('click.stolo', storePopUpHandler );
				// Close Store Change Popup
				$storeChangeBlock.on('click.stolo','.close', function (e) {
					e.preventDefault();
					$storeChangeBlock.hide();
				});

				/* Store Info */
				$storeInfoTrigger = $("#nav-store-info, #confirm-store .store-info, #noResults .change-store").on("click", storePopUpHandler );
				/* Events in the Store Info Popover */
				$storeInfoBlock.on('click.stolo', '.close', function (e) {
					e.preventDefault();
					$storeInfoBlock.hide();
				}).on('click.stolo','.change-store', function (e) {
					e.preventDefault();
					if ($storeInfoBlock.hasClass('autozip')) {
						$('#confirm-store .change-store').trigger('click');
					} else {
						$('#nav-store-change').trigger('click');
					}
					//toggleVisible($storeInfoBlock,$storeChangeBlock, $storeInfoBlock);
					bindStolo(true);
				}).on('click.stolo', '.get-directions', function (e) {
					e.preventDefault();
					location.href = $(this).attr('href')+'?getDirections='+escape([storeDetails.address,storeDetails.city,",",storeDetails.state,storeDetails.zip].join(" "));
					/*Lowes.Masthead.Locator.clear();
					document.getElementById(Lowes.Masthead.Locator.prefix+'map-directions-to').value = [storeDetails.address,storeDetails.zip].join(' ');
					Lowes.Masthead.Locator.setDisplayMode("directions");
					$storeInfoBlock.hide();
					$storeChangeBlock.show();*/
				});

				storeDetails = Lowes.User.getCurrentStore();

				/* Requested to suppress "Lowe's of " in store name. */
				if ( storeDetails.name.substring(0,toRemove.length) == toRemove ){
					storeDetails.name = storeDetails.name.slice(toRemove.length);
				}

				/* Set up a fake event to pass the current store search */
				fakedEvent = {data:{searchInput:{value:storeDetails.zip}}};

				/* Populate the DOM with store details. */
				$storeSearchActive.find(".store-name").text(storeDetails.name);

				/* Show the store details, hide search */
				if ( !storeDetailsDisabled ){
					toggleVisible( $storeSearchDefault, $storeSearchActive );
				}
				/* Add the store number to the weekly ad link. */
				if ($weeklyAdLink.length) { $weeklyAdLink.attr("href",$weeklyAdLink.attr("href")+"?storeref="+storeDetails.number); }
			}
			else {
				/* Show the store search, hide the details. */
				if ( !storeDetailsDisabled ){
					toggleVisible( $storeSearchActive, $storeSearchDefault );
				}
			}

			/* Find a store input. */
			$(document.storeSearchForm).submit(function (){
				var queryDef = {},
					searchStringInMasthead = $.trim(storeSearchInput.value.replace("'", "\\'"));

				// Trigger tealium Tag for Selecting new Store
				Lowes.Metrics.ltag({
					event_type: "change-store",
					link_name: "Find A Store"
				}, "tealium");

				/* Parse through the input */
				if (searchStringInMasthead == "") {
					/* Rather than handling form errors, we go straight through to the store locator. */
					return true;
				} else if (searchStringInMasthead.length < 5) {
					/* Assuming Store Search */
					searchStringInMasthead = Lowes.Utils.zeroPad(searchStringInMasthead, 4);
					location.href = "http://www.lowes.com/StoreLocatorDisplayView?storeId=10151&langId=-1&catalogId=10051&site="+searchStringInMasthead;
				} else if (searchStringInMasthead.length == 5 && !isNaN(searchStringInMasthead)) {
					/* Typical back-end search, returning true submits the form */
					storeSearchInput.value = searchStringInMasthead;
					return true;
				} else if (isNaN(searchStringInMasthead)) {
					location.href = "http://www.lowes.com/StoreLocatorDisplayView?storeId=10151&langId=-1&catalogId=10051&site="+searchStringInMasthead;
					return false;
				}
				return false;
			});

			/* Lowe's Navigation Mini-Cart */
			if ( $("#lowes-cart").length ) {
				cartCookie = Lowes.Cookie.get('SHOPPINGCART');
				if ( cartCookie != null ) {
					cartInfo = cartCookie.split('|');
					$("#nav-cart-count").text(cartInfo[0]);
					$("#nav-cart-total").text(cartInfo[1]);
				}
			}
		};

		/* Update dynamic portions of the masthead */
		Lowes.Masthead.refresh();

		/******************/
		/* Event Bindings */
		/******************/

		/* Lazy Loading the Category Panels */
		$("a.category-name:not(.fetched)").on("mouseenter", function (e){
			var $this = $(this),
				$menuContainer = $this.next('.category-panel'),
				promosHtml = '';

			/* check to see if flyout has content */
			if (!$this.hasClass('fetched')) {
				/* content does not exist, fetch it; add class after ajax load */
					$menuContainer.load( $this.attr("rel"), function (response, status, xhr){
						if(status != "error") {
							$this.addClass('fetched');
							//bind coremetrics for $menuContainer contents

							$menuContainer.on('click','a',function (e) {
								var $trigger = $(this),
									cmRegion = $trigger.parents('ul').attr('class'),
									cmProdCategory = $trigger.parents('.category-panel').find('h2').text(),
									cmCategory = '',
									cmTitle = ''
									cmPage = (typeof (cmProductviewData) != 'undefined')?cmProductviewData.pagename:((typeof (cmPageviewData) != 'undefined')?cmPageviewData.pageName:'');
								switch (cmRegion) {
									case "bucket-list" :
										cmCategory = "Buckets";
										cmTitle = $trigger.find('span').text();
									break;
								case "bucket-list-lci" :
									cmCategory = "Buckets";
									cmTitle = $trigger.attr('title');
								break;
									case "aux-nav" :
										cmCategory = "Learn More";
									break;
									case "link-list" :
									case "link-list-all" :
									if ($trigger.parents('div').attr('class') == "list-container primary-list") {
										cmCategory = "Shop More";
									} else if ($trigger.parents('div').attr('class') === 'list-container full-list'){
										cmCategory = "Savings By Category";
									} else {
										cmCategory = "Projects Inspiration";
									}
									break;
								}

								if (!cmTitle.length) {
									cmTitle = $trigger.text();
								}
								if (cmCategory.length) {
									cmCreatePageElementTag(cmProdCategory + " - " + cmCategory + " - " + cmTitle, "Global Masthead Departments", cmAttrCreator({1: cmPage}));
								}

								// Tealium Masthead Department Links Event
								Lowes.Metrics.ltag({
									event_type: "link-click",
									link_name: cmTitle,
									link_type: "masthead"
								}, "tealium");
							});
						}
					});
				}
		}).on('click', function (e) {
			var $trigger = $(this),
				cmSuperCategory = $trigger.text(),
				cmPage = (typeof cmProductviewData !== 'undefined') ? cmProductviewData.pagename : ((typeof cmPageviewData != 'undefined') ? cmPageviewData.pageName : '');
			cmCreatePageElementTag(cmSuperCategory + " - SuperCategory", "Global Masthead Departments", cmAttrCreator({1: cmPage}));
			// Tealium Super Cat Masthead Department Links Event
			cmSuperCategory = cmSuperCategory.toLowerCase();
			if (cmSuperCategory.indexOf(' ') !== -1 && cmSuperCategory.indexOf(' ') !== cmSuperCategory.length - 1) {
				cmSuperCategory = cmSuperCategory.replace(' ', '-');
				cmSuperCategory = cmSuperCategory.replace('&', '');
			}
			// Tealium
			Lowes.Metrics.ltag({
				event_type: "link-click",
				link_name: cmSuperCategory,
				link_type: "masthead"
			}, "tealium");
		});

		$('.shop_all_prod a.learn-more').on('click', function (e) {
			var $trigger = $(this),
				cmSuperCategory = $trigger.text(),
				cmPage = (typeof cmProductviewData !== 'undefined') ? cmProductviewData.pagename : ((typeof cmPageviewData != 'undefined') ? cmPageviewData.pageName : '');
			cmCreatePageElementTag(cmSuperCategory + " - SuperCategory", "Global Masthead Departments", cmAttrCreator({1: cmPage}));
			// Tealium Super Cat Masthead Department Links Event
			cmSuperCategory = cmSuperCategory.toLowerCase();
			if (cmSuperCategory.indexOf(' ') !== -1 && cmSuperCategory.indexOf(' ') !== cmSuperCategory.length - 1) {
				cmSuperCategory = cmSuperCategory.replace(' ', '-');
				cmSuperCategory = cmSuperCategory.replace('&', '');
			}
			// Tealium
			Lowes.Metrics.ltag({
				event_type: "link-click",
				link_name: cmSuperCategory,
				link_type: "masthead"
			}, "tealium");
		});

		/* Global Masthead Tracking - Blue Bar
		   - Excludes Mini Cart, User Name + Store Locator deep links
		*/
		$('#header-block').on('click', '#lowes-salutation-default a, #lowes-account a, #lowes-store-active .aux-nav a.nav-heading, .header-espot a, #header-brand a', function (e) {
			var $trigger = $(this),
				cmCategory = (typeof $trigger.attr('title') !== 'undefined') ? $trigger.attr('title') : $trigger.text(),
				cmPage = (typeof cmProductviewData !== 'undefined') ? cmProductviewData.pagename : ((typeof cmPageviewData !== 'undefined') ? cmPageviewData.pageName : '');
			cmCreatePageElementTag(cmCategory, "Global Masthead", cmAttrCreator({1: cmPage}));
			// Tealium General Masthead Links Event
			cmCategory = cmCategory.toLowerCase();
			if (cmCategory.indexOf(' ') !== -1 && cmCategory.indexOf(' ') !== cmCategory.length - 1) {
				cmCategory = cmCategory.replace(' ', '-');
				cmCategory = cmCategory.replace('&', '');
			}
			// Tealium
			Lowes.Metrics.ltag({
				event_type: "link-click",
				link_name: cmCategory,
				link_type: "masthead"
			}, "tealium");
		});

		/* Masthead Quick Links Tracking */
		$('#service-nav').on('click', 'a', function() {
			var linkName = $(this).text();
			// Tealium
			Lowes.Metrics.ltag({
				event_type: "link-click",
				link_name: linkName,
				link_type: "masthead"
			}, "tealium");
		});

		/* Apply hoverIntent to control hide/show delay. CSS handles the rest. */
		$('.main-level, .main-nav li.category').hoverIntent({
			sensitivity: 3, // number = sensitivity threshold (must be 1 or higher)
			interval: 50,   // number = milliseconds of polling interval
			over: function () {
				var $this = $(this),
					$loader;

				$this.siblings().removeClass("active").end().addClass('active');

				if ($this.hasClass("main-level")){
					/* Trigger the first category to load */
					$this.find("a.category-name:first").addClass('active').trigger("mouseenter");
				}
				/* Added to remove focus on search and the lingering typeahead container whilst navigating. */
				if ($this.find(".sub-level, .aux-nav").length >= 1 || $this.hasClass("aux-nav")) {

					$("#searchContainer").hide();
					$Ntt.blur();

				}
			},  // function = onMouseOver callback (required)
			timeout: 300,   // number = milliseconds delay before onMouseOut function call
			out: function () { $(this).removeClass('active'); }    // function = onMouseOut callback (required)
		});

		/* JS users get arrows, css users get angry at arrows. */
		$("ul.sub-level").append('<li class="ui-north-arrow-bg"></li><li class="ui-north-arrow"></li>');
		$("div.sub-level").append('<div class="ui-north-arrow-bg"></div><div class="ui-north-arrow"></div>');

		/* Footer modals */
		$('#footer-safe-shop').on('click', function (e){
			e.preventDefault();
			var $this = $(this),
				title = $this.attr('title'),
				container = ($('#container').length) ? '#container' : 'body',
				$standard_modal = ($('#standard_modal').length) ? $('#standard_modal') : $('<div/>', {
						'id': 'standard_modal'
					}).appendTo(container);

			$standard_modal.dialog({
				modal:true,
				dialogClass:'modal',
				autoOpen: false,
				resizable: false,
				draggable: false,
				width:500,
				height:230
			});

			$standard_modal.load($this.data('load'), function(){
				$standard_modal.dialog("option", "title", title).dialog('open');
			});
		});

		/* Email Dialog */
		$('#cm_email_trigger, .cm_email_trigger').on('click', function (e){
			e.preventDefault();
			var $emailBlock = $('#cm_email_modal'),
				$emailModal = $emailBlock.dialog({
					modal:true,
					dialogClass:'cm_email_dialog',
					title:'SIGN UP &amp; SAVE',
					autoOpen: false,
					resizable: false,
					draggable: false,
					width:480,
					height:400
				});
			/* This must be done on click to avoid too many requests to the exacttarget server. */
			$emailBlock.find('iframe').attr('src', 'http://pages.s4.exacttarget.com/lowes/quicksignup/');
			$emailModal.dialog('open');
		});

		$('#footer-block a').on('click', function(e){
			var linkName = $(this).text();
			Lowes.Metrics.ltag({
				event_type: "link-click",
				link_name: linkName,
				link_type: "footer"
			}, "tealium");
		});
	}
	/*
		External/Affliate/Third-Party Links/PDFs will open in a new window
		Easy2 Demos open into pop-up window
	*/
	if ( $page.length ) {
		// Open all PDFs in a new window
		$('a[href*=".pdf"]').attr('target','_blank');
		// External links will open in a new window
		$(".html_banner, #categories").find('[href^="http://"]').not('[href^="../../../../index.htm"/*tpa=http://www.lowes.com/*/],[class^="video"],[class^="link-new-window"],[target]').attr('target','_blank');
	}
	// Update global Lowes object
    window.Lowes = Lowes;
}(jQuery));
// D-01811 Rebate link in MyLowes incorrect
jQuery('#mylowes-rebate-center').find('a:contains("Start Online Submission")').attr('href', '/cd_Online+Submission_320108876_');

/* ../global/deprecated-globals.js */

/** 
 * @fileOverview The functions in this file are provided to gap legacy code with new development. 
 * Please do not use the functions declared here.
 * If you have code that uses these functions, please use the provided alternate method
 * and remove any calls to these functions.
 */

/* Plan to deprecate the Lowes Carousel */
/*
window.Lowes.UI.Carousel = function (){};

window.Lowes.UI.Carousel.prototype = function (container, options){
	options = $.extend({}, {
		something: ''
	}, options);
	return $('#'.container).carousel(options);
};
*/

/*
 * Check if the user is signed in or not.
 * @deprecated Please use Lowes.User.isSignedIn()
 * @returns {Boolean} whether the user is logged in or not.
 */
function isUserSignedIn(){
	console.log("Unknown_83_filename"/*tpa=http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/Deprecated Method %27isUserSignedIn%27, please check javascript/global/deprecated-globals.js*/);
    return Lowes.User.isSignedIn();
}

/*
 *	Place all old cookie functions here, these are the ones we are moving away from.
 *	They now forward on to the new functions for backwards compatibility. 
 *	Please remove any references to these functions in any code you come across.
 */


/**
 * Lowes.Cookies are passe
 * @todo fix unencode issues...
 * @namespace 
 */
//window.Lowes.Cookie = {};
/** @deprecated Use $.cookie(name, value, {'expires': expires}) */
//window.Lowes.Cookie.set = function (name, value, expires){
//	expires = expires || 7;
//	return $.cookie(name, value, {'expires': expires});
//};
/** @deprecated Use $.cookie(name) */
//window.Lowes.Cookie.get = function (name){ return $.cookie(name); };
/** @deprecated Use $.cookie(name, null) */
//window.Lowes.Cookie.del = function (name){ return $.cookie(name, null); };
/** Looking for an alternative to this */
//window.Lowes.Cookie.enabled = function (){
//	var e = !!navigator.cookieEnabled;
    //if navigator,cookieEnabled is not supported
//    if (typeof navigator.cookieEnabled=="undefined" && !e){
//        document.cookie = "testcookie";
//        e = document.cookie.indexOf("testcookie") != -1;
//    }
//    return e;
//};

/** @deprecated Use $.cookie(name) */
function chkCookie(name) {
	console.log("Unknown_83_filename"/*tpa=http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/Deprecated Method %27chkCookie%27, please check javascript/global/deprecated-globals.js*/);
	return Lowes.Cookie.get(name);
}
/** @deprecated Use $.cookie(name, value, {'expires': days}) */
function createCookie(name,value,days) {
	console.log("Unknown_83_filename"/*tpa=http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/Deprecated Method %27createCookie%27, please check javascript/global/deprecated-globals.js*/);
	Lowes.Cookie.set(name,value,days);
}
/** @deprecated Use $.cookie(name) */
function readCookie(name) {
	console.log("Unknown_83_filename"/*tpa=http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/Deprecated Method %27readCookie%27, please check javascript/global/deprecated-globals.js*/);
	return Lowes.Cookie.get(name);
}
/** @deprecated Use $.cookie(name, null) */
function eraseCookie(name) {
	console.log("Unknown_83_filename"/*tpa=http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/Deprecated Method %27eraseCookie%27, please check javascript/global/deprecated-globals.js*/);
    Lowes.Cookie.del(name);
}

/* ../global/Lowes/autozip.js */

/** @fileOverview Figure out a user's location and attempt to zip them in to it. */
(function ($, undefined) {
    /*
     * Beginning of variable declarations
     */
    var disableAutozip = Lowes.Cookie.get("disable-autozip"),
        selectedStore1 = Lowes.Cookie.get("selectedStore1"),
        $autoMsgParent = $('#confirm-store'),
        ajaxUrl = '/LowesStoreSearchCmd?ajaxReq=true&storeId=10151&langId=-1&usedAutozip=true&provider=de',
        storeValidateURL = '/IntegrationServices/resources/storeLocator/json/v2_0/stores?langId=-1&storeId=10702&catalogId=10051',
        overrideObj = {},
        /*
        updateCoremetrics = function (action, confirmStore) {
            var $confirmStore = $(confirmStore),
                msgState = $confirmStore.hasClass('confirmed') ? "MAX" : "MIN",
                cm_ChangeStore = ["AUTOZIP ", action, " STORE ", msgState].join(''),
                pageType = "";

            if ($confirmStore.hasClass('PL')) {
                pageType = "PRODLIST";
            } else if ($confirmStore.hasClass('PD')) {
                pageType = "PRODDETAIL";
            } else if ($confirmStore.hasClass('cart')) {
                pageType = "CART";
            }
            // Coremetrics
            //cmCreateConversionEventTag(cm_ChangeStore, 2, "TESTING", 0, "-_--_--_--_--_--_--_--_--_-" + pageType);
        },
        */
        updateAutozipFailCount = function(){
            var prefs,
                failCount;
            Lowes.Prefs.load();
            prefs = Lowes.Prefs.data;
            prefs.failCount=(prefs.failCount!==undefined)?prefs.failCount+=1:1;
            if (prefs.failCount >= 3){
                Lowes.Cookie.set("disable-autozip", "yes");
            }
            Lowes.Prefs.save();
        },

        /**
         * Compare provided store code with any currently active store code.
         * If the same, return true - if different, false.
         */
        isCurrentStore = function(newStoreCode) {
            if(typeof newStoreCode !== "undefined") {
                var curStore = Lowes.User.getCurrentStore().number;
                if(typeof curStore!=="undefined" && curStore===newStoreCode) {
                    return true;
                }
            }
            return false;
        },

        /**
         * Determines whether or not to perform a Google originated zip override
         **/
        doZipOverride = function() {
            var store_code = Lowes.Utils.urlQueryObject().store_code,
            	storeUpdate = Lowes.Utils.urlQueryObject().storeNumber,
                isValid = false; // false by default
            if(!storeUpdate && !isCurrentStore(store_code) && $.isNumeric(store_code)) {
                // Validate the provided store ID
                $.ajax({
                    url: storeValidateURL,
                    dataType: "JSON",
                    data: {
                        count:1,
                        place: store_code
                    },
                    async: false,
                    success: function(data) {
                        // Check for object property. Failed ID will still return an object.
                        if(data.Location.length>0) {
                            overrideObj = data;
                            isValid = true;
                        }
                    }
                });
            }
            return isValid;
        }
        /**
         * Perform AutoZip, based on various conditions
         **/
        doAutoZip = function(dataObj) {
            /*
             * Check if dataObj is an object. Then check if the user is zipped in
             * OR if there is a new store number being submitted. New store numbers
             * are meant to override any current store numbers.
             */
            if (typeof dataObj === "object") {
                if (!Lowes.User.isZippedIn() || !isCurrentStore(dataObj.Location[0].KEY)) {
                    console.log("Performing zip override.");
                    $input = $('<input type="hidden" name="storeNumber" value="' + dataObj.Location[0].KEY + '"/>').add($('<input type="hidden" name="URL" value="' + location.href + '"/>'));
                    $('<form action="/LowesUpdateLocalStoreCmd" />').append($input).appendTo('body').submit();
                    return false;
                }
            }
            /*
             * User is not being directed from Google and not zipped in anywhere.
             * Perform default autozip functionality.
             */
            if (typeof disableAutozip !== "undefined" && disableAutozip !== "yes" && !Lowes.User.isZippedIn()){
                console.log("Performing auto zip actions.");
                $.ajax({
                    url: ajaxUrl,
                    dataType: 'text',
                    async: false,
                    success: function (r) {
                        // Remove the \ that escapes the apostrophe in Lowe's, then turn that string into a JSON object
                        var JSON_str = r.replace(/\\/g, ""),
                            JSON;
                        try {
                            JSON = $.parseJSON(JSON_str);
                            if (JSON.selectedStore1) {
                                Lowes.Cookie.set('selectedStore1', JSON.selectedStore1, 30);
                                Lowes.Cookie.set('autozip', 'true', 30);

                                if (JSON.zipCode) {
                                    $('#storeSearchForm').find('input[name="zipCode"]').val(JSON.zipCode).end().submit();
                                }

                                Lowes.User.CurrentStore = Lowes.User.getCurrentStore(); // Re-Cache Lowes.User.CurrentStore Object

                                //cmCreatePageElementTag("AUTOZIP SUCCESS", "TESTING");
                                /*Checking for autozip on product list pages and refresh window if true*/
                                if ($('#productResults').length || $('#productCont').length) {
                                    window.location.reload();
                                } else {
                                    Lowes.Masthead.refresh();
                                }
                            } else {
                                updateAutozipFailCount();
                                //cmCreatePageElementTag("AUTOZIP FAIL", "NO STORE SELECTED");
                                console.log("Autozip failed - no store selected.");
                            }
                        } catch (e) {
                            //an international IP will have bad JSON value returned
                            //caught a problem trying to parse JSON; just return
                            updateAutozipFailCount();
                            //cmCreatePageElementTag("AUTOZIP FAIL", "BAD RESPONSE");
                            console.log("Autozip failed - bad response.");
                            return;
                        }
                    },
                    error: function () {
                        updateAutozipFailCount();
                        //cmCreatePageElementTag("AUTOZIP FAIL", "NO RESPONSE");
                        console.log("Autozip failed - no response.");
                    }
                });
            }
        };
    /*
     * End of variable declarations
     */
    console.log("***** START ZIPPING FUNCTIONS *****");
    if(!doZipOverride()) {
        doAutoZip();
    }else{
        doAutoZip(overrideObj);
    }
    console.log("***** END ZIPPING FUNCTIONS *****");

    if (Lowes.Cookie.get('autozip') === 'true' && $autoMsgParent.length) {
        $autoMsgParent.addClass('display');
        $('#detailCont').before($autoMsgParent.slideDown());
        if (!$autoMsgParent.hasClass('confirmed')) {
            cmCreatePageElementTag("AUTOZIP MAX MESSAGE", "TESTING");
        }
        $autoMsgParent.on('click', 'a', function (e) {
            var $this = $(this);

            if ($this.is('a[name="PL_changeStore"]')) { /* Change Store Link */
                //updateCoremetrics("CHANGE", $autoMsgParent);
            } else if ($this.is('#pl-confirm-store, #close-confirm-store')) { /* Confirm Store Button */
                e.preventDefault();
                $autoMsgParent.removeClass('display').addClass('confirmed');
                $this.next('a').html('Change Store <span class="ui-icon"></span>');
                Lowes.Cookie.set("ANZSC", "yes", 30);
                Lowes.Cookie.del("autozip");
                //updateCoremetrics("CONFIRM", $autoMsgParent);
                if ($this.attr('id') === "close-confirm-store") {
                    $autoMsgParent.hide();
                }
            }
        });
    }
    $("#disable-autozip").on("click", function(e) {
        e.preventDefault();
        Lowes.Cookie.set("disable-autozip", "yes");
        Lowes.Cookie.del("selectedStore1");
        window.location = window.location.href;
    });
}(jQuery));

/* ../global/Lowes/modules/zipin.js */

/** @fileOverview Sideshow bob */

(function ($){
	var doZipin;
	var Zipin = Class.extend(/** @lends Zipin# */{
		/**
		 * Prompt Users to enter their zip code
		 * @constructs
		 * @deprecated
		 * @private
		 * @param {Object} content Content, defines a key of content type and value of content source.
		 * @param {String} attach Element selector to attach to (optional). If not provided, defaults to modal.
		 */
		init: function (content, attach){
			// Grab our prefs
			Lowes.Prefs.load();
			Lowes.Prefs.data.zipin = (typeof Lowes.Prefs.data.zipin === 'undefined') ? {
				"show"		: true // Do we show the zip-in element?
			} : Lowes.Prefs.data.zipin; // We don't need defaults, we have some already.
			// Let's write our prefs just to be safe
			Lowes.Prefs.save();

			// Call our utility method that returns our page type.
			this.pageType = Lowes.Utils.getLowesPageType();

			// An ugly little html string that we use to inject our zip-in dialog.
			this.templateString  = [
					'  <div id="zip-code-avail-a" class="zip-code-dialog-box">',
					'	  <a href="javascript:;" class="btn_close" id="close-this-zip">close</a>',
					'	  <img src="../../../../images/zip_images/modal_woman_1.gif"/*tpa=http://www.lowes.com/images/zip_images/modal_woman_1.gif*/ class="featured_img" />',
					'	  <div class="zip-avail-npc">',
					'		 <h3 class="price_availability">Get Pricing &amp; Availability</h3>',
					'		 <p>We use your ZIP code to give you up-to-the-minute pricing and availability in your area.</p>',
					'	  </div>',
					'	  <div class="zip-avail-form">',
					'		 <form action="/LowesStoreSearchCmd" method="post" name="zipin" id="zipin">',
					'			<input value="true" name="masthead" type="hidden" />',
					'			<input value="TopCategoriesDisplayView" name="URL" type="hidden" />',
					'			<input value="true" name="masthead" type="hidden" />',
					'			<input value="StoreLocatorDisplayView" name="findStoreErrorURL" type="hidden" />',
					'			<input value="true" name="masthead" type="hidden" />',
					'			<input value="TopCategoriesDisplayView" name="mastheadURL" type="hidden" />',
					'			<input value="true" name="masthead" type="hidden" />',
					'			<input value="10151" name="storeId" type="hidden" />',
					'			<input value="true" name="masthead" type="hidden" />',
					'			<input value="10051" name="catalogId" type="hidden" />',
					'			<input value="true" name="masthead" type="hidden" />',
					'			<input value="-1" name="langId" type="hidden" />',
					'			<input value="true" name="masthead" type="hidden" />',
					'			<input value="" name="firstReferURL" type="hidden" />',
					'			<input value="true" name="masthead" type="hidden" />',
					'			<input value="" name="NttParam" type="hidden" />',
					'			<input value="true" name="masthead" type="hidden" />',
					'			<input value="" name="isQvSearch" type="hidden" />',
					'			<input value="true" name="masthead" type="hidden" />',
					'			<input value="" name="qvRedirect" type="hidden" />',
					'			<input value="true" name="masthead" type="hidden" />',
					'			<input type="hidden" name="zippedWithModal" value="1" />',
					'			<input value="true" name="masthead" type="hidden" />',
					'			<input id="zipcode_text_box" name="zipCode" class="text" type="text" />',
					'			<input value="true" name="masthead" type="hidden" />',
					'			<input src="../../../../images/zip_images/btn-zip-submit.gif"/*tpa=http://www.lowes.com/images/zip_images/btn-zip-submit.gif*/ class="btn" type="image" />',
					'			<input value="true" name="masthead" type="hidden" />',
					'		 </form>',
					'			<input value="true" name="masthead" type="hidden" />',
					'	  </div>',
					'			<input value="true" name="masthead" type="hidden" />',
					'	  <br style="clear: both;">',
					'			<input value="true" name="masthead" type="hidden" />',
					'  </div>'
				].join('');
			this.show();
		},

		/**
		 * Build a formal modal and display it to the user with content using the template string
		 *
		 * @private
		 */
		__showDefault: function(modalType) {
			var classes = [],
				width = '620px',
				height = '230px';

			// Setup our modal object.
			this.modal = new Lowes.UI.Modal({
				border: "" /*"1px solid #024d9e"*/,
				background:"none",
				height: height,
				width: width,
				load: this.templateString,
				/*load: {html: this.zipDiv},*/
				effect: 'slide',
				classes: classes
			});

			// Bind our close links to a click event to our close method.
			$('#close-this-zip').on('click', function (evt){
				evt.preventDefault();

				$('#lowes-modal-id-element-backdrop').fadeOut('fast', function (){
					$('#lowes-modal-id-element').fadeOut('fast', function (){
						//cmCreatePageElementTag(window.lowes_zipin_close_tag, "Testing", "close");
					});
				});
				Lowes.Prefs.load();
				prefs = Lowes.Prefs.data;
				prefs.closeCount=(prefs.closeCount!==undefined)?prefs.closeCount+=1:1;
				Lowes.Prefs.save();
			});

			// Show our modal.
			this.modal.show(function (){
				$('#zipcode_text_box').focus();
				// CoreMetrics
				cmCreatePageElementTag("ZIPIN MODAL","TESTING");
			});
		},

		__submitEvent: function (submit_tag){
			Lowes.Prefs.load();
			//Lowes.Prefs.data.zipin.show = false;
			Lowes.Prefs.save();
		},

		/**
		 * Test to see if the modal should be show, if so show it
		 * otherwise do nothing.
		 */
		show: function() {
			Lowes.Prefs.load();
			//$('body').prepend('<div style="position:absolute;left:0;top:0;">Data: '+Lowes.Prefs.data+' | Zipin: ' + Lowes.Prefs.data.zipin + '</div>')
			/**
			 * A collection of IF checks to handle whether we show the modal or not
			 */
			if(document.location.href.indexOf('http://www.lowes.com/cd_Giftlist+Builder_198984609_') > -1
					|| (typeof Lowes.Prefs.data.zipin.show != 'undefined' && !Lowes.Prefs.data.zipin.show)
					|| document.location.href.indexOf('LowesStoreSearchCmd') > -1) {
				return;
			}

			// Quick test to ensure the user isn't already zipped in
			if(Lowes.Cookie.enabled() && !Lowes.Cookie.get('selectedStore1')) {
				if (Lowes.Prefs.data.zipin.show && Lowes.Prefs.data.failCount >= 3 && (typeof Lowes.Prefs.data.closeCount === 'undefined' || Lowes.Prefs.data.closeCount < 3)) {
					this.__submitEvent("Woman_Zip_In_Modal_Submit");
					//window.lowes_zipin_close_tag = "Woman_Zip_In_Modal_Close";
					this.__showDefault();
				}
			}
		},

		/**
		 * Close method to handle when a user cancels the Zip In modal
		 * instead of actually zipping in.  This will tell our store/class
		 * not to show the modal anymore.
		 *
		 * @param {String} modal Selector of the element that will fire our close event.
		 */
		cancelClose: function(modal) {
			Lowes.Prefs.load();
			// If we didn't show it we don't need to close it.
			if(Lowes.Prefs.data.zipin.show) {
				// What type of dialog did we show?
				switch(this.pageType) {
					// For a list page, let's slide up the dialog and get rid of it.
					case Lowes.PageTypes.List:
						$('#zip-code-avail').slideUp('fast');
						break;

					// Otherwise just close the modal.
					default:
						$('#lowes-modal-id-element-backdrop').fadeOut('fast', function (){
							$('#lowes-modal-id-element').fadeOut('fast');
						});
						$('#zip-code-avail').slideUp('fast');
						//modal.close();
						break;
				}
				// Since you cancel you can't see it again!
				Lowes.Prefs.data.zipin.show = false;

				// Safe preferences.
				Lowes.Prefs.save();
			}
		}
	});
	//Array to hold the location of pages that may be affected by the zipin popup showing unexpectedly
	var blockZip = ["/webapp/wcs/stores/servlet/LogonFormNavIframeS"];
	if (Lowes.Cookie.enabled() && $.inArray(window.location.pathname, blockZip) == -1)
	{
	    doZipin = new Zipin();
	}
}(jQuery));

/* ../global/Lowes/modules/zipin-modal.js */

/**
 * @fileOverview ZipCodeModal | Utility for checking zipin status, showing a modal if zipin is not complete and accessing store data via the facade
 */
(function (window, document, $) {

	var Lowes = window.Lowes || {},

		/**
		 * ZipIn Modal
		 * @class
		 * @name ZipCodeModal
		 */
		ZipCodeModal = {
			/** @lends ZipCodeModal */

			zipModalWidget: {}, // holder object var for modal object
			$zipModalContent: {}, // holder object zip code modal content, jquery object
			currentStoreID: (typeof Lowes.User.CurrentStore.number === "undefined") ? '' : Lowes.User.CurrentStore.number, // initial state of storeID
			closestStoreData: {}, // holder object for returned data
			externalCallback: null, // holder for external call back function

			/**
			 * Setup zipcode modal and store params if present
			 * @memberOf ZipCodeModal
			 * @param  {object}   link     Accepts a jQuery object based on element selection
			 * @param  {Function} callback Optional External Function to call
			 * @function
			 */
			init: function (link, callback) {
				// set external callback function
				if (callback) {
					this.externalCallback = callback;
				}
				// setup our zipcode modal
				this.setupModal();
			},

			/**
			 * Build out html for modal
			 * @memberOf ZipCodeModal
			 * @function
			 */
			modalContent: function () {
				var zipModalHtml = [
					'<form action="#" id="zipCodeModal" class="ui-validateform">',
					'<p>Enter Your ZIP Code to View Real-Time Pricing and Availability For Your Local Store</p>',
					'<div class="modal-zip-entry ui-form">',
					'<label for="modal-zipCode">ZIP Code:</label>',
					'<input class="ui-input zipcode required" type="text" id="modal-zipCode" name="modal-zipCode" placeholder="Enter ZIP Code" maxLength="5">',
					'<p><small class="instructions"><strong>Example:</strong> 12345</small></p>',
					'<p><a class="btn" href="#" id=""><span>Find Store</span></a></p>',
					'<p><a href="http://www.lowes.com/StoreLocatorDisplayView?storeId=10151&langId=-1&catalogId=10051" class="find-zip">I Don\'t Know My ZIP Code</a></p>',
					'</div>',
					'</form>'
				];

				return zipModalHtml.join('');
			},

			/**
			 * Use Modal Content to create the modal using jQuery Dialog
			 * @memberOf ZipCodeModal
			 * @function
			 */
			setupModal: function () {
				this.$zipModalContent = $(this.modalContent()); // grab modal content initializing as jquery object

				// initialize jquery dialog into widget holder
				this.$zipModalContent.dialog({
					modal: true,
					autoOpen: false,
					draggable: false,
					resizable: false
				});

				// bind events to content inside of modal
				this.bindModalEvents();

				// initialize validation for zipcode modal form
				// global validation was not applied due to html being created via javascript
				this.$zipModalContent.validate({
					debug: true,
					focusCleanup: true,
					focusInvalid: false,
					errorClass: "error",
					onkeyup: false,
					ignore: ".ignore-validation",

					highlight: function (element, errorClass) {
						$(element).parent("li").addClass(errorClass);
					},
					unhighlight: function (element, errorClass) {
						$(element).parent("li").removeClass(errorClass);
					}
				});
			},

			/**
			 * Bind events for click on modal button and keyup(return) on modal input
			 * @memberOf ZipCodeModal
			 * @function
			 */
			bindModalEvents: function () {
				var zipModalRoot = this,
					$modal = zipModalRoot.$zipModalContent,
					$modalInput = $modal.find('input#modal-zipCode'),
					$modalButton = $modal.find('http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/a.btn');

				// setup click action for submit button
				$modalButton.on('click.zipModalSubmit', function (event) {
					event.preventDefault();
					// if valid submit zip, otherwise validation errors are shown
					if ($modal.valid()) {
						zipModalRoot.submitZip($modalInput.val());
					}
				});
				// capture return key if used while typing inside input field
				$modalInput.on('keyup.zipModalSubmit', function (event) {
					event.preventDefault();
					// if valid submit zip, otherwise validation errors are shown
					if (event.keyCode === 13 && $modal.valid()) {
						zipModalRoot.submitZip($modalInput.val());
					}
				});
			},

			/**
			 * Show the Modal
			 * @memberOf ZipCodeModal
			 * @function
			 */
			showModal: function () {
				this.$zipModalContent.dialog('open');
			},

			/**
			 * Hide the Modal
			 * @memberOf ZipCodeModal
			 * @function
			 */
			closeModal: function () {
				this.$zipModalContent.dialog('close');
			},

			/**
			 * Make Ajax call to facade to retrieve closest store based on zipcode
			 * @memberOf ZipCodeModal
			 * @param  {string}   zipCode     User entered zipcode
			 * @function
			 */
			submitZip : function (zipCode) {
				var zipModalRoot = this;

				// ajax call to facade
				$.ajax({
					url: "/IntegrationServices/resources/storeLocator/json/v2_0/stores?langId=-1&storeId=10702&catalogId=10051&",
					data: {
						place: zipCode,
						count: 1
					},
					dataType: "json"
				}).done(function (res) {
					// set our store data
					zipModalRoot.closestStoreData = res.Location[0];
					// if externalCallback exists send the data
					if (zipModalRoot.externalCallback !== '') {
						zipModalRoot.externalCallback(res.Location[0]);
					}
					// close modal if successful
					zipModalRoot.closeModal();
				}).fail(function (res) {
					console.log('failed', res);
				});
			}
		};

	Lowes.ZipCodeModal = ZipCodeModal;
	window.Lowes = Lowes;

}(window, document, jQuery));

/* ../global/Lowes/modules/predictive_search.js */

( function(window, document, $) {

		var Lowes = window.Lowes || {},

		/**
		 * @fileOverview PredictiveSearch | Typeahead/Autocomplete
		 */

		/**
		 * Predictive Search Object
		 * @class
		 * @name PredictiveSearch
		 */
		PredictiveSearch = {
			/** @lends  PredictiveSearch# */ 

			searchLength : 1, // Length of search term that triggers predictive search
			maxTerms : 7, // Maximum number of terms returned by search
			maxArticles : 3, // Maximum number of articles returned by search
			maxBrands : 0, // Maximum number of brands returned by search
			maxDepts : 0, // Maximum number of departments returned by search
			maxProducts : 3, // Maximum number of products returned by search
			setTimeout : 120000, // Set timeout amount for ajax calls
			isZippedIn : Lowes.Cookie.get('selectedStore1'), // Variable assigned to user's store information
			$searchForm : $('#frmQuickSearch'), // Variable assigned to search form
			$Ntt : $('#Ntt'), // The search input field
			isRemote : 0, // Check based from 'isZippedIn' that is set to 0 if user isn't remote, 1 if user is logged in from Alaska or Hawaii
			storeNum : 0, // User's store number that is set from data pulled from 'isZippedIn'
			$searchContainer : null, // Selector for entire search result container
			$terms : null, // Selector for terms ul in the search result container
			$products : null, // Selector for products ul in the search result container
			$articles : null, // Selector for articles ul in the search result container
			typedTerm : null, // String typed by user in search input
			termCount : null, // Number of search terms returned in ajax call (Minimum is 0, Maximum is 'maxTerms')
			currentTerm : null, // Highlighted search term when using up or down arrow key
			hoveredTerm : null, // Highlighted search term when hovering

			/**
			 * Setup Predictive Search zipped status, HTML and keyup actions
			 * @memberOf PredictiveSearch
			 * @param
			 * @function
			 */
			init : function() {
				this.checkZippedStatus(); // Check users zip in status for store number and if user is remote
				this.setSearchInputValue(); // If possible, sets the search input field with previously searched term
				this.setupSearchHtml();	// Embed HTML used for search container
				this.setKeyupActions();	// Setup keyup used for ajax calls and when using up or down arrow keys
				this.preventSearchActions(); // Prevent default for tab key in search input and disallow blank searches
				// Hide search result container when clicking outside of container
				$(document).mouseup(function(e) {
					if (!PredictiveSearch.$searchContainer.is(e.target) && PredictiveSearch.$searchContainer.has(e.target).length === 0) {
						PredictiveSearch.clearSearchContainer('hide');
					}
				});
			},

			/**
			 * Setup search container HTML
			 * @memberOf PredictiveSearch
			 * @param
			 * @function
			 */
			setupSearchHtml : function() {
				var searchBoxCoords = PredictiveSearch.$searchForm.find('.ui-frame').offset(), searchContainerTop = searchBoxCoords.top + 43, searchContainerLeft = searchBoxCoords.left;
				$('body').append('<div id="searchContainer" style="display: none;"><ul id="terms"></ul><ul id="products"></ul><ul id="articles"></ul></div>');
				this.$searchContainer = $('#searchContainer');
				PredictiveSearch.$searchContainer.css({'position' : 'absolute', 'top' : searchContainerTop, 'left' : searchContainerLeft});
				this.$terms = $("#terms");
				this.$products = $("#products");
				this.$articles = $("#articles");
			},

			/**
			 * Manages checks to obtain customer store number and remote status
			 * @memberOf PredictiveSearch
			 * @param
			 * @function
			 */
			checkZippedStatus : function() {
				// Check to see if customer is zipped in for autocomplete/typeahead ajax call
				if (this.isZippedIn !== null) {
					// Check to see if customer is remote
					if (this.isZippedIn.split("|")[4] === 'yes') {
						this.isRemote = 1;
					}
					// Assign store number based off user cookie
					this.storeNum = this.isZippedIn.split("|")[1];
				}
			},

			/**
			 * Manages checks for accepted use of tab key in search input, handles blank searches and trims certain characters
			 * @memberOf PredictiveSearch
			 * @param
			 * @function
			 */
			preventSearchActions : function() {
				// Prevent default for tab key in search input
				this.$Ntt.keydown(function(e) {
					if (e.which === 9 && PredictiveSearch.$searchContainer.is(':visible')) {
						return false;
					}
				});
				// Disallow blank searches, trim certain characters
				this.$searchForm.on('submit', PredictiveSearch.searchHandler);
				this.$Ntt.on('keypress', PredictiveSearch.searchHandler);
			},

			/**
			 * Routine for trimming the search field
			 * @memberOf PredictiveSearch
			 * @param
			 * @function
			 */
			searchTrim : function($el) {
				if ($el && $el.length) {
					return $el.val().replace(/[\*%]/g,"").trim();
				} else {
					return false;
				}
			},

			/**
			 * Handles the two search bindings
			 * @memberOf PredictiveSearch
			 * @param
			 * @function
			 */
			searchHandler : function(e) {
				var NttSearch = PredictiveSearch.searchTrim(PredictiveSearch.$Ntt);
				if (e.which === 13 && !PredictiveSearch.searchTrim($(this))) {
					// Run if this is a keypress and the key is ENTER
					PredictiveSearch.$searchForm.attr('action', 'javascript:void(0);');

                                        //Set up for Analytics
                                        PredictiveSearch.predictiveType = 'keyword';
                                        PredictiveSearch.selectedTerm = PredictiveSearch.searchTrim(PredictiveSearch.$Ntt);
                                        PredictiveSearch.setAnalytics();
                                        
				} else if (!e.which && !NttSearch) {
					// A submit action, but search term is empty
					return false;
				} else if (!e.which || e.which === 13) {

                                        // Everything checks out, do the search.
					PredictiveSearch.$Ntt.val(NttSearch);
					PredictiveSearch.$searchForm.attr('action', '/Search=' + PredictiveSearch.$Ntt.val().replace(/\s/g, "+"));
					sessionStorage.setItem("storedTerm", PredictiveSearch.$Ntt.val());
                                        
                                        //Set up for Analytics
                                        PredictiveSearch.predictiveType = 'keyword';
                                        PredictiveSearch.selectedTerm = PredictiveSearch.searchTrim(PredictiveSearch.$Ntt);
                                        PredictiveSearch.setAnalytics();
				}
			},
                        /**
                         * Makes the Analytics call
                         * @param {type} predictiveSearchTerm
                         * @returns {undefined}
                         */
                        setAnalytics: function(predictiveSearchTerm) {
                            if(typeof(window.digitalData.predictive)!=='object'){
                                window.digitalData = window.digitalData || {};
                                var analyticsObj = {
                                     event_type: 'predictive_search',
                                     predictive_search_term: PredictiveSearch.typedTerm,
                                     predictive_selected_term: PredictiveSearch.selectedTerm,
                                     predictive_position: PredictiveSearch.predictivePos,
                                     predictive_type: PredictiveSearch.predictiveType
                                 };

                                window.digitalData.predictive = analyticsObj;
                                //_satellite.track('predictive_search');
                                if(analyticsObj.predictive_search_term !== "" && analyticsObj.predictive_position !== null){
                                    if ( typeof CQ === 'undefined') {
                                        utag.link(analyticsObj);
                                    }else{
                                        s.events = "event10";
                                        s.eVar13 = PredictiveSearch.typedTerm+":"+PredictiveSearch.selectedTerm+":"+PredictiveSearch.predictivePos+":"+PredictiveSearch.predictiveType;
                                        s.tl();
                                    }
                                }
                            }
                        },

			/**
			 * If possible, sets the search input field with previously searched term
			 * @memberOf PredictiveSearch
			 * @param
			 * @function
			 */
			setSearchInputValue : function() { 
				if (sessionStorage.getItem("storedTerm") !== null) PredictiveSearch.$Ntt.val(sessionStorage.getItem("storedTerm"));
			},

			/**
			 * Setup the keyboard actions to control predictive search functionality
			 * @memberOf PredictiveSearch
			 * @param
			 * @function
			 */
			setKeyupActions : function() {
				// Run after keystroke in search box
				this.$Ntt.keyup(function(e) {
					var timer = null, searchVal = PredictiveSearch.$Ntt.val(), searchValLength = searchVal.length, current = PredictiveSearch.$terms.find('.current'), currentLength = current.length, getCurrentTerm, getFirstTerm = PredictiveSearch.$terms.find('li:first-child').text();
					// Hide and empty search container if no searchVal is present or it's length is not larger than searchLength
					if (searchVal === '' || searchValLength < PredictiveSearch.searchLength) {
						PredictiveSearch.clearSearchContainer('hide');
						PredictiveSearch.clearSearchContainer('terms');
						PredictiveSearch.clearSearchContainer('products');
						PredictiveSearch.clearSearchContainer('articles');
					} else {
						// Clear existing timer
						if (timer) {
							window.clearTimeout(timer);
						}
						// Create timer to assist with ajax call
						timer = window.setTimeout(function() {
							timer = null;
							/* Run except when arrow keys, tab, or search term of at least searchLength
							 * If so, reset typedTerm and currentTerm, hoveredTerm flags and make ajax calls for terms and products/articles
							 */
							if (searchValLength == (PredictiveSearch.$Ntt.val()).length && (e.which > 40 || e.which < 37) && e.which !== 9 && searchValLength >= PredictiveSearch.searchLength) {
								PredictiveSearch.clearSearchContainer('typedTerm');
								PredictiveSearch.clearSearchContainer('currentTerm');
								PredictiveSearch.clearSearchContainer('hoveredTerm');
								PredictiveSearch.getTerms(searchVal);
								PredictiveSearch.getProductsArticles(searchVal);
							}
						}, 300);
						// Run only if ajax call returns search terms
						if (PredictiveSearch.termCount > 0) {
							if (e.which === 40) {// Down arrow
								downKeyActions(current, currentLength);
								upDownKeyActions(searchVal);
							} else if (e.which === 38) {// Up arrow
								upKeyActions(current, currentLength);
								upDownKeyActions(searchVal);
							} else if (e.which === 39 && !currentLength) {// Right arrow
								rightKeyActions(getFirstTerm);
							} else if (e.which === 9 && !currentLength) {// Tab
								tabKeyActions(getFirstTerm);
							}
						}
					}
				});
				/**
				 * Manages up arrow key actions that moves the highlighted suggestion up the list
				 * @memberOf PredictiveSearch
				 * @param {string} list item containing current class
				 * @param {string} current list item length for comparison
				 * @function
				 */
				upKeyActions = function(current, currentLength) {
					/* Up arrow actions
					 * Checks to see if a term has the current class
					 * If so, remove the current class and then assign it to the previous term
					 * Else, assign the current class to the last term
					 */
					if (currentLength) {
						$(current).removeClass('current').prev().addClass('current');
					} else {
						PredictiveSearch.$terms.find('li:last-child').attr('class', 'searchDisplay current');
					}
					/* Checks to see if there is not a currently selected term,
					 * Else, decrement currentTerm
					 */
					if (PredictiveSearch.currentTerm === -1) {
						/* If user is hovering term, currentTerm is set to hoveredTerm, decrement hoveredTerm, reset hoveredTerm flag to negative
						 * Else, set current term to termCount - 1 (since indexes start at 0) or last term
						 */
						if (PredictiveSearch.hoveredTerm !== -1) {
							PredictiveSearch.currentTerm = PredictiveSearch.hoveredTerm;
							PredictiveSearch.currentTerm--;
							PredictiveSearch.clearSearchContainer('hoveredTerm');
						} else {
							PredictiveSearch.currentTerm = PredictiveSearch.termCount - 1;
						}
					} else {
						PredictiveSearch.currentTerm--;
					}
				};
				/**
				 * Manages down key actions that moves the highlighted suggestion down the list
				 * @memberOf PredictiveSearch
				 * @param {string} list item containing current class
				 * @param {string} current list item length for comparison
				 * @function
				 */
				downKeyActions = function(current, currentLength) {
					/* Down arrow actions
					 * Checks to see if a term has the current class
					 * If so, remove the current class and then assign it to the next term
					 * Else, assign the current class to the first term
					 */
					if (currentLength) {
						$(current).removeClass('current').next().addClass('current');
					} else {
						PredictiveSearch.$terms.find('li:first-child').attr('class', 'searchDisplay current');
					}
					/* Checks to see if there is not a currently selected term,
					 * Else, increment currentTerm
					 */
					if (PredictiveSearch.currentTerm === -1) {
						/* If user is hovering term, currentTerm is set to hoveredTerm, increment hoveredTerm, reset hoveredTerm flag to negative
						 * Else, set current term to 0 which is the first term
						 */
						if (PredictiveSearch.hoveredTerm !== -1) {
							PredictiveSearch.currentTerm = PredictiveSearch.hoveredTerm;
							PredictiveSearch.currentTerm++;
							PredictiveSearch.clearSearchContainer('hoveredTerm');
						} else {
							PredictiveSearch.currentTerm = 0;
						}
					} else {
						PredictiveSearch.currentTerm++;
					}
				};
				/**
				 * Manages the value used for the ajax call that is manipulated via the up or down keys
				 * @memberOf PredictiveSearch
				 * @param {string} value stored in search input
				 * @function
				 */
				upDownKeyActions = function(searchVal) {
					// Sets search value to be saved before up or down arrow keys are used
					if (PredictiveSearch.typedTerm === '') {
						PredictiveSearch.typedTerm = searchVal;
					}
					/* Checks to see that currentTerm is a number that is less than the total number of terms and greater than or equal to 0
					 *  If so, Cache current term text, Sets search input field to current term, make products/articles ajax call using currently selected term
					 */
					if (PredictiveSearch.currentTerm < PredictiveSearch.termCount && PredictiveSearch.currentTerm >= 0) {
						getCurrent = PredictiveSearch.$terms.find(".current");
                                                getCurrentTerm = getCurrent.text();
						PredictiveSearch.$Ntt.val(getCurrentTerm);
						PredictiveSearch.getProductsArticles(getCurrentTerm);
                                                
                                                //Set Predictive Position
                                                var predictiveList = $('#searchContainer #terms');
                                                var predictiveItem = $(this).parent();
                                                PredictiveSearch.predictivePos = getCurrent.index()+1;
					} else {
						/* Runs if currentTerm is a number outside the range of the total number of terms
						 * Sets search input field to typedTerm, make products/articles ajax call using typedTerm, set currentTerm flag to negative
						 */
						PredictiveSearch.$Ntt.val(PredictiveSearch.typedTerm);
						PredictiveSearch.getProductsArticles(PredictiveSearch.typedTerm);
						PredictiveSearch.clearSearchContainer('currentTerm');
                                                PredictiveSearch.predictivePos = null;
					}
				};
				/**
				 * Manages right arrow key actions that selects the first list item
				 * @memberOf PredictiveSearch
				 * @param {string} first list item
				 * @function
				 */
				rightKeyActions = function(getFirstTerm) {
					/* Right arrow actions
					 * Sets search input field to current term, then makes terms/products/articles ajax call using currently selected term
					 */
					PredictiveSearch.$Ntt.val(getFirstTerm);
					PredictiveSearch.getTerms(getFirstTerm);
					PredictiveSearch.getProductsArticles(getFirstTerm);
				};
				/**
				 * Manages tab key actions that selects the first list item
				 * @memberOf PredictiveSearch
				 * @param {string} first list item
				 * @function
				 */
				tabKeyActions = function(getFirstTerm) {
					/* Tab key actions
					 * Sets search input field to current term, then makes terms/products/articles ajax call using currently selected term
					 */
					if (getFirstTerm !== PredictiveSearch.$Ntt.val()) {
						PredictiveSearch.$Ntt.val(getFirstTerm);
						PredictiveSearch.getTerms(getFirstTerm);
						PredictiveSearch.getProductsArticles(getFirstTerm);
					} else {
						PredictiveSearch.clearSearchContainer('hide');
					}
				};
			},

			/**
			 * Manages tab key actions that selects the first list item
			 * @memberOf PredictiveSearch
			 * @param {string} first list item
			 * @function
			 */
			getHighlight: function(searchVal) {
				var searchTermSplit = searchVal.replace(/^\s+| +(?= )/g, "").replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1").split(" "), $searchText, termArray = [];     
				// Assign regex for matching search values to search result values           
				for(var i=0;i<searchTermSplit.length;i++) {
					termArray.push(new RegExp(searchTermSplit[i]+"(?!([^<]+)?>)", "gi"));
				}
				$('#terms .searchDisplay').contents().each(function() {
					var searchTextArray = [], searchTextDisplay = $(this).text().split(" ");
					if (searchTermSplit[0] !== "") {
						$searchText = $(this).text().replace(termArray[0], "<b>$&</b>");
					}
					for(var j=1;j<searchTermSplit.length;j++){
						if (searchTermSplit[j] !== "" && searchTermSplit[j] !== ">") {
							$searchText = $searchText.replace(termArray[j], "<b>$&</b>");
						}
					}
					$(this).html($searchText);
				});
			},

			/**
			 * Manages hover actions on search container
			 * @memberOf PredictiveSearch
			 * @param
			 * @function
			 */
			resultsHoverActions : function() {
				$('.searchDisplay').hoverIntent({
					sensitivity: 70,
					interval: 150,
					over: function(e) {
						PredictiveSearch.clearSearchContainer("currentTerm");
						PredictiveSearch.$searchContainer.find(".current").attr("class", "searchDisplay");
						$(this).attr("class", "searchDisplay current"); 
						if ($("#terms .searchDisplay:hover").length !== 0) {
							PredictiveSearch.hoveredTerm = $(this).index();
							PredictiveSearch.getProductsArticles($(e.target).text());
						}
					},
					timeout: 150,
					out: function() {
						$(this).attr("class", "searchDisplay");
						PredictiveSearch.clearSearchContainer("hoveredTerm"); 
					}
				});
			},

			/**
			 * Manages click actions on terms
			 * @memberOf PredictiveSearch
			 * @param
			 * @function
			 */
			resultsClickActions : function() {
                           
                           //Click Action for keywords
                           $(".termLink").click(function() {
                                var predictiveList = $("#searchContainer #terms");
                                var predictiveItem = $(this).parent();
                                PredictiveSearch.predictivePos = predictiveItem.index() + 1;
                                PredictiveSearch.typedTerm = PredictiveSearch.$Ntt.val();
                                PredictiveSearch.$Ntt.val($(this).text());
                                sessionStorage.setItem("storedTerm", PredictiveSearch.$Ntt.val());
                                PredictiveSearch.$searchForm.submit();

                            });
                            //Click Action for products (either image or product name)
                            $(".termProductLink").click(function(e) {
                                e.preventDefault();
                                sessionStorage.setItem("storedTerm", PredictiveSearch.$Ntt.val());
                                PredictiveSearch.predictiveType = "product";
                                var predictiveList = $("#searchContainer #products");
                                var predictiveItem = $(this).closest(".searchDisplay");
                                PredictiveSearch.predictivePos = predictiveItem.index() + 1;
                                PredictiveSearch.typedTerm = PredictiveSearch.$Ntt.val();
                                PredictiveSearch.selectedTerm = $(this).attr("data-product-name");
                                PredictiveSearch.setAnalytics();

                                window.location.href=$(this).attr('href');
                            });

			},

			/**
			 * Manages ajax success of terms
			 * @memberOf PredictiveSearch
			 * @param {object} json data object
			 * @function
			 */
			handleTermsAjaxSuccess : function(data) {
				var isArray = null, termsArray = [], termsHtml;
				// Empty terms list
				PredictiveSearch.clearSearchContainer('terms');
				// Return if empty data set
				if ($.isEmptyObject(data)) {
					return;
				}
				// Show search container upon successful ajax call
				PredictiveSearch.$searchContainer.show();
				// Iterate through terms object and append to terms list
				if ( typeof data.terms !== 'undefined') {
					isArray = false;
					$.each(data.terms, function(index, term) {
						if ( typeof term.name !== 'undefined') {
							isArray = true;
							termsArray.push('<li class="searchDisplay"><a class="termLink">' + term.name + '</a></li>');
						}
					});
					// Check for when only one result is present
					if (isArray === false) {
						termsArray.push('<li class="searchDisplay"><a class="termLink">' + data.terms.name + '</a></li>');
					}
					// join the array contents with no spaces
					termsHtml = termsArray.join('');
					// append the html all at once
					PredictiveSearch.$terms.append(termsHtml);
				}
				// Count for number of terms in list and call hover and click actions
				PredictiveSearch.termCount = PredictiveSearch.$terms.find('li').length;
				PredictiveSearch.resultsHoverActions();
				PredictiveSearch.resultsClickActions();
			},

			/**
			 * Manages ajax success of products/articles
			 * @memberOf PredictiveSearch
			 * @param {object} json data object
			 * @function
			 */
			handleProductsArticlesAjaxSuccess : function(data) {
				var isArray = null, productsArray = [], productsHtml, articlesArray = [], articlesHtml;
				// Empty products and articles lists
				PredictiveSearch.clearSearchContainer('products');
				PredictiveSearch.clearSearchContainer('articles');
				// Return if empty data set
				if ($.isEmptyObject(data)) {
					return;
				}
				// Show search container upon successful ajax call
				PredictiveSearch.$searchContainer.show();
				// Iterate through products object and append to products list
				if ( typeof data.products !== 'undefined') {
					isArray = false;
					$.each(data.products, function(index, product) {
						if ( typeof product.name !== 'undefined') {
							isArray = true;
                                                        //Clean name for embedding name into data-product-name
                                                        var cleanName = Lowes.Metrics.cleanTealiumData(product.name);
							productsArray.push('<li class="searchDisplay"><ul class="resultContainer"><li class="resultImg"><a href="' + product.url + '" data-product-name="' + cleanName + '" class="termProductLink"><img src="' + product.imgUrl + '" /></a></li><li class="resultDesc"><a href="' + product.url + '" data-product-name="' + cleanName + '" class="termProductLink">' + product.name + '</a></li></ul></li>');
						}
					});
					// Check for when only one result is present
					if (isArray === false && data.products.name !== 'undefined') {
                                            //Clean name for embedding name into data-product-name
                                            var cleanName = Lowes.Metrics.cleanTealiumData(data.products.name);
                                            productsArray.push('<li class="searchDisplay"><ul class="resultContainer"><li class="resultImg"><a href="' + data.product.url + '" data-product-name="' + cleanName + '" class="termProductLink"><img src="' + data.product.imgUrl + '" /></a></li><li class="resultDesc"><a href="' + data.products.url + '" data-product-name="' + cleanName + '" class="termProductLink">' + data.products.name + '</a></li></ul></li>');
					}
					// join the array contents with no spaces
					productsHtml = productsArray.join('');
					// append the html all at once
					PredictiveSearch.$products.append(productsHtml);
				}
				// Iterate through articles object and append to articles list
				if ( typeof data.articles !== 'undefined') {
					isArray = false;
					$.each(data.articles, function(index, article) {
						if ( typeof article.name !== 'undefined') {
							isArray = true;
							articlesArray.push('<li class="searchDisplay"><ul class="resultContainer"><li class="resultImg"><a href="' + article.url + '"><img src="' + article.imgUrl + '" /></a></li><li class="resultDesc"><a href="' + article.url + '">' + article.name + '</a></li></ul></li>');
						}
					});
					// Check for when only one result is present
					if (isArray === false) {
						articlesArray.push('<li class="searchDisplay"><ul class="resultContainer"><li class="resultImg"><a href="' + data.articles.url + '"><img src="' + data.articles.imgUrl + '" /></a></li><li class="resultDesc"><a href="' + data.articles.url + '">' + data.articles.name + '</a></li></ul></li>');
					}
					// join the array contents with no spaces
					articlesHtml = articlesArray.join('');
					// append the html all at once
					PredictiveSearch.$articles.append(articlesHtml);
				}
				// Call hover and click actions
				PredictiveSearch.resultsHoverActions();
				PredictiveSearch.resultsClickActions();
			},

			/**
			 * Manages resetting, emptying, or hiding search container elements
			 * @memberOf PredictiveSearch
			 * @param {string} element to be modified
			 * @function
			 */
			clearSearchContainer : function(elem) {
				switch (elem) {
					case 'terms':
						PredictiveSearch.$terms.empty();
						break;
					case 'products':
						PredictiveSearch.$products.empty();
						break;
					case 'articles':
						PredictiveSearch.$articles.empty();
						break;
					case 'typedTerm':
						PredictiveSearch.typedTerm = '';
						break;
					case 'currentTerm':
						PredictiveSearch.currentTerm = -1;
						break;
					case 'hoveredTerm':
						PredictiveSearch.hoveredTerm = -1;
						break;
					case 'termCount':
						PredictiveSearch.termCount = 0;
						break;
					default:
						PredictiveSearch.$searchContainer.hide();
				}
			},

			/**
			 * Manages ajax call for terms
			 * @memberOf PredictiveSearch
			 * @param {string} value from search input, arrow key manipulation, or hovering of terms
			 * @function
			 */
			getTerms : function(searchVal) {
				$.ajax({
					url : '/LowesSearchServices/resources/autocomplete/v1_0',
					timeout : PredictiveSearch.setTimeout,
					dataType : 'json',
					global: false,
					data : {
						searchTerm : searchVal,
						maxTerms : PredictiveSearch.maxTerms
					},
					beforeSend : function(xhr) {
						xhr.setRequestHeader("Authorization", "Basic VHlwZUFoZWFkRXh0ZXJuYWw6NG91aWk0MzJuOTA5MDQz");
					},
					statusCode : {
						204 : function() {
							PredictiveSearch.clearSearchContainer('termCount');
						}
					},
					success : function(data) {
						PredictiveSearch.handleTermsAjaxSuccess(data);
						PredictiveSearch.getHighlight(searchVal);
					},
					error : function() {
						PredictiveSearch.clearSearchContainer('hide');
					}
				});
			},

			/**
			 * Manages ajax call for products and articles
			 * @memberOf PredictiveSearch
			 * @param {string} value from search input, arrow key manipulation, or hovering of terms
			 * @function
			 */
			getProductsArticles : function(searchVal) {
				$.ajax({
					url : '/LowesSearchServices/resources/typeahead/v1_0',
					timeout : PredictiveSearch.setTimeout,
					dataType : 'json',
					global: false,
					data : {
						searchTerm : searchVal,
						maxArticles : PredictiveSearch.maxArticles,
						maxBrands : PredictiveSearch.maxBrands,
						maxDepts : PredictiveSearch.maxDepts,
						maxProducts : PredictiveSearch.maxProducts,
						store : PredictiveSearch.storeNum,
						isRemoteLocation : PredictiveSearch.isRemote
					},
					beforeSend : function(xhr) {
						xhr.setRequestHeader("Authorization", "Basic VHlwZUFoZWFkRXh0ZXJuYWw6NG91aWk0MzJuOTA5MDQz");
					},
					success : function(data) {
						PredictiveSearch.handleProductsArticlesAjaxSuccess(data);
					},
					error : function() {
						PredictiveSearch.clearSearchContainer('hide');
					}
				});
			}
		};

		Lowes.PredictiveSearch = PredictiveSearch;
		window.Lowes = Lowes;

	}(window, document, jQuery));
// Ensure we are not in the login modal before initializing
if ($('.lowes_account_login_modal').length === 0) {
	Lowes.PredictiveSearch.init();
}

/* ../global/lib/jquery.validate.js */

/* DO NOT LINT */
/**
 * jQuery Validation Plugin @VERSION
 *
 * http://bassistance.de/jquery-plugins/jquery-plugin-validation/
 * http://docs.jquery.com/Plugins/Validation
 *
 * Copyright (c) 2006 - 2011 Jörn Zaefferer
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 */

(function($) {

$.extend($.fn, {
	// http://docs.jquery.com/Plugins/Validation/validate
	validate: function( options ) {

		// if nothing is selected, return nothing; can't chain anyway
		if (!this.length) {
			//options && options.debug && window.console && console.warn( "nothing selected, can't validate, returning nothing" );
			return;
		}

		// check if a validator for this form was already created
		var validator = $.data(this[0], 'validator');
		if ( validator ) {
			return validator;
		}

		validator = new $.validator( options, this[0] );
		$.data(this[0], 'validator', validator);

		if ( validator.settings.onsubmit ) {

			// allow suppresing validation by adding a cancel class to the submit button
			this.find("input, button").filter(".cancel").click(function() {
				validator.cancelSubmit = true;
			});

			// when a submitHandler is used, capture the submitting button
			if (validator.settings.submitHandler) {
				this.find("input, button").filter(":submit").click(function() {
					validator.submitButton = this;
				});
			}

			// validate the form on submit
			this.submit( function( event ) {
				if ( validator.settings.debug )
					// prevent form submit to be able to see console output
					event.preventDefault();

				function handle() {
					if ( validator.settings.submitHandler ) {
						if (validator.submitButton) {
							// insert a hidden input as a replacement for the missing submit button
							var hidden = $("<input type='hidden'/>").attr("name", validator.submitButton.name).val(validator.submitButton.value).appendTo(validator.currentForm);
						}
						validator.settings.submitHandler.call( validator, validator.currentForm );
						if (validator.submitButton) {
							// and clean up afterwards; thanks to no-block-scope, hidden can be referenced
							hidden.remove();
						}
						return false;
					}
					return true;
				}

				// prevent submit for invalid forms or custom submit handlers
				if ( validator.cancelSubmit ) {
					validator.cancelSubmit = false;
					return handle();
				}
				if ( validator.form() ) {
					if ( validator.pendingRequest ) {
						validator.formSubmitted = true;
						return false;
					}
					return handle();
				} else {
					validator.focusInvalid();
					return false;
				}
			});
		}

		return validator;
	},
	// http://docs.jquery.com/Plugins/Validation/valid
	valid: function() {
        if ( $(this[0]).is('form')) {
            return this.validate().form();
        } else {
            var valid = true;
            var validator = $(this[0].form).validate();
            this.each(function() {
				valid &= validator.element(this);
            });
            return valid;
        }
    },
	// attributes: space seperated list of attributes to retrieve and remove
	removeAttrs: function(attributes) {
		var result = {},
			$element = this;
		$.each(attributes.split(/\s/), function(index, value) {
			result[value] = $element.attr(value);
			$element.removeAttr(value);
		});
		return result;
	},
	// http://docs.jquery.com/Plugins/Validation/rules
	rules: function(command, argument) {
		var element = this[0];

		if (command) {
			var settings = $.data(element.form, 'validator').settings;
			var staticRules = settings.rules;
			var existingRules = $.validator.staticRules(element);
			switch(command) {
			case "add":
				$.extend(existingRules, $.validator.normalizeRule(argument));
				staticRules[element.name] = existingRules;
				if (argument.messages)
					settings.messages[element.name] = $.extend( settings.messages[element.name], argument.messages );
				break;
			case "remove":
				if (!argument) {
					delete staticRules[element.name];
					return existingRules;
				}
				var filtered = {};
				$.each(argument.split(/\s/), function(index, method) {
					filtered[method] = existingRules[method];
					delete existingRules[method];
				});
				return filtered;
			}
		}

		var data = $.validator.normalizeRules(
		$.extend(
			{},
			$.validator.metadataRules(element),
			$.validator.classRules(element),
			$.validator.attributeRules(element),
			$.validator.staticRules(element)
		), element);

		// make sure required is at front
		if (data.required) {
			var param = data.required;
			delete data.required;
			data = $.extend({required: param}, data);
		}

		return data;
	}
});

// Custom selectors
$.extend($.expr[":"], {
	// http://docs.jquery.com/Plugins/Validation/blank
	blank: function(a) {return !$.trim("" + a.value);},
	// http://docs.jquery.com/Plugins/Validation/filled
	filled: function(a) {return !!$.trim("" + a.value);},
	// http://docs.jquery.com/Plugins/Validation/unchecked
	unchecked: function(a) {return !a.checked;}
});

// constructor for validator
$.validator = function( options, form ) {
	this.settings = $.extend( true, {}, $.validator.defaults, options );
	this.currentForm = form;
	this.init();
};

$.validator.format = function(source, params) {
	if ( arguments.length == 1 )
		return function() {
			var args = $.makeArray(arguments);
			args.unshift(source);
			return $.validator.format.apply( this, args );
		};
	if ( arguments.length > 2 && params.constructor != Array  ) {
		params = $.makeArray(arguments).slice(1);
	}
	if ( params.constructor != Array ) {
		params = [ params ];
	}
	$.each(params, function(i, n) {
		source = source.replace(new RegExp("\\{" + i + "\\}", "g"), n);
	});
	return source;
};

$.extend($.validator, {

	defaults: {
		messages: {},
		groups: {},
		rules: {},
		errorClass: "error",
		validClass: "valid",
		errorElement: "label",
		focusInvalid: true,
		errorContainer: $( [] ),
		errorLabelContainer: $( [] ),
		onsubmit: true,
		ignore: [],
		ignoreTitle: false,
		onfocusin: function(element) {
			this.lastActive = element;

			// hide error label and remove error class on focus if enabled
			if ( this.settings.focusCleanup && !this.blockFocusCleanup ) {
				this.settings.unhighlight && this.settings.unhighlight.call( this, element, this.settings.errorClass, this.settings.validClass );
				this.addWrapper(this.errorsFor(element)).hide();
			}
		},
		onfocusout: function(element) {
			if ( !this.checkable(element) && (element.name in this.submitted || !this.optional(element)) ) {
				this.element(element);
			}
		},
		onkeyup: function(element) {
			if ( element.name in this.submitted || element == this.lastElement ) {
				this.element(element);
			}
		},
		onclick: function(element) {
			// click on selects, radiobuttons and checkboxes
			if ( element.name in this.submitted )
				this.element(element);
			// or option elements, check parent select in that case
			else if (element.parentNode.name in this.submitted)
				this.element(element.parentNode);
		},
		highlight: function(element, errorClass, validClass) {
			if (element.type === 'radio') {
				this.findByName(element.name).addClass(errorClass).removeClass(validClass);
			} else {
				$(element).addClass(errorClass).removeClass(validClass);
			}
		},
		unhighlight: function(element, errorClass, validClass) {
			if (element.type === 'radio') {
				this.findByName(element.name).removeClass(errorClass).addClass(validClass);
			} else {
				$(element).removeClass(errorClass).addClass(validClass);
			}
		}
	},

	// http://docs.jquery.com/Plugins/Validation/Validator/setDefaults
	setDefaults: function(settings) {
		$.extend( $.validator.defaults, settings );
	},

	messages: {
		required: "This field is required.",
		remote: "Please fix this field.",
		email: "Please enter a valid email address.",
		url: "Please enter a valid URL.",
		date: "Please enter a valid date.",
		dateISO: "Please enter a valid date (ISO).",
		number: "Please enter a valid number.",
		digits: "Please enter only digits.",
		creditcard: "Please enter a valid credit card number.",
		equalTo: "Please enter the same value again.",
		accept: "Please enter a value with a valid extension.",
		maxlength: $.validator.format("Please enter no more than {0} characters."),
		minlength: $.validator.format("Please enter at least {0} characters."),
		rangelength: $.validator.format("Please enter a value between {0} and {1} characters long."),
		range: $.validator.format("Please enter a value between {0} and {1}."),
		max: $.validator.format("Please enter a value less than or equal to {0}."),
		min: $.validator.format("Please enter a value greater than or equal to {0}.")
	},

	autoCreateRanges: false,

	prototype: {

		init: function() {
			this.labelContainer = $(this.settings.errorLabelContainer);
			this.errorContext = this.labelContainer.length && this.labelContainer || $(this.currentForm);
			this.containers = $(this.settings.errorContainer).add( this.settings.errorLabelContainer );
			this.submitted = {};
			this.valueCache = {};
			this.pendingRequest = 0;
			this.pending = {};
			this.invalid = {};
			this.reset();

			var groups = (this.groups = {});
			$.each(this.settings.groups, function(key, value) {
				$.each(value.split(/\s/), function(index, name) {
					groups[name] = key;
				});
			});
			var rules = this.settings.rules;
			$.each(rules, function(key, value) {
				rules[key] = $.validator.normalizeRule(value);
			});

			function delegate(event) {
				var validator = $.data(this[0].form, "validator"),
					eventType = "on" + event.type.replace(/^validate/, "");
				validator.settings[eventType] && validator.settings[eventType].call(validator, this[0] );
			}
			$(this.currentForm)
				.validateDelegate("[type='tel'], [type='email'], :text, :password, :file, select, textarea", "focusin focusout keyup", delegate)
				.validateDelegate(":radio, :checkbox, select, option", "click", delegate);

			if (this.settings.invalidHandler)
				$(this.currentForm).bind("invalid-form.validate", this.settings.invalidHandler);
		},

		// http://docs.jquery.com/Plugins/Validation/Validator/form
		form: function() {
			this.checkForm();
			$.extend(this.submitted, this.errorMap);
			this.invalid = $.extend({}, this.errorMap);
			if (!this.valid())
				$(this.currentForm).triggerHandler("invalid-form", [this]);
			this.showErrors();
			return this.valid();
		},

		checkForm: function() {
			this.prepareForm();
			for ( var i = 0, elements = (this.currentElements = this.elements()); elements[i]; i++ ) {
				this.check( elements[i] );
			}
			return this.valid();
		},

		// http://docs.jquery.com/Plugins/Validation/Validator/element
		element: function( element ) {
			element = this.clean( element );
			this.lastElement = element;
			this.prepareElement( element );
			this.currentElements = $(element);
			var result = this.check( element );
			if ( result ) {
				delete this.invalid[element.name];
			} else {
				this.invalid[element.name] = true;
			}
			if ( !this.numberOfInvalids() ) {
				// Hide error containers on last error
				this.toHide = this.toHide.add( this.containers );
			}
			this.showErrors();
			return result;
		},

		// http://docs.jquery.com/Plugins/Validation/Validator/showErrors
		showErrors: function(errors) {
			if(errors) {
				// add items to error list and map
				$.extend( this.errorMap, errors );
				this.errorList = [];
				for ( var name in errors ) {
					this.errorList.push({
						message: errors[name],
						element: this.findByName(name)[0]
					});
				}
				// remove items from success list
				this.successList = $.grep( this.successList, function(element) {
					return !(element.name in errors);
				});
			}
			this.settings.showErrors
				? this.settings.showErrors.call( this, this.errorMap, this.errorList )
				: this.defaultShowErrors();
		},

		// http://docs.jquery.com/Plugins/Validation/Validator/resetForm
		resetForm: function() {
			if ( $.fn.resetForm )
				$( this.currentForm ).resetForm();
			this.submitted = {};
			this.prepareForm();
			this.hideErrors();
			var t = this.elements();
			t.removeClass( this.settings.errorClass );
			t.closest('li').removeClass( this.settings.errorClass );
		},

		numberOfInvalids: function() {
			return this.objectLength(this.invalid);
		},

		objectLength: function( obj ) {
			var count = 0;
			for ( var i in obj )
				count++;
			return count;
		},

		hideErrors: function() {
			this.addWrapper( this.toHide ).hide();
		},

		valid: function() {
			return this.size() == 0;
		},

		size: function() {
			return this.errorList.length;
		},

		focusInvalid: function() {
			if( this.settings.focusInvalid ) {
				try {
					$(this.findLastActive() || this.errorList.length && this.errorList[0].element || [])
					.filter(":visible")
					.focus()
					// manually trigger focusin event; without it, focusin handler isn't called, findLastActive won't have anything to find
					.trigger("focusin");
				} catch(e) {
					// ignore IE throwing errors when focusing hidden elements
				}
			}
		},

		findLastActive: function() {
			var lastActive = this.lastActive;
			return lastActive && $.grep(this.errorList, function(n) {
				return n.element.name == lastActive.name;
			}).length == 1 && lastActive;
		},

		elements: function() {
			var validator = this,
				rulesCache = {};

			// select all valid inputs inside the form (no submit or reset buttons)
			return $(this.currentForm)
			.find("input, select, textarea")
			.not(":submit, :reset, :image, [disabled]")
			.not( this.settings.ignore )
			.filter(function() {
				!this.name && validator.settings.debug && window.console && console.error( "%o has no name assigned", this);

				// select only the first element for each name, and only those with rules specified
				if ( this.name in rulesCache || !validator.objectLength($(this).rules()) )
					return false;

				rulesCache[this.name] = true;
				return true;
			});
		},

		clean: function( selector ) {
			return $( selector )[0];
		},

		errors: function() {
			return $( this.settings.errorElement + "." + this.settings.errorClass, this.errorContext );
		},

		reset: function() {
			this.successList = [];
			this.errorList = [];
			this.errorMap = {};
			this.toShow = $([]);
			this.toHide = $([]);
			this.currentElements = $([]);
		},

		prepareForm: function() {
			this.reset();
			this.toHide = this.errors().add( this.containers );
		},

		prepareElement: function( element ) {
			this.reset();
			this.toHide = this.errorsFor(element);
		},

		check: function( element ) {
			element = this.clean( element );

			// if radio/checkbox, validate first element in group instead
			if (this.checkable(element)) {
				element = this.findByName( element.name ).not(this.settings.ignore)[0];
			}

			var rules = $(element).rules();
			var dependencyMismatch = false;
			for (var method in rules ) {
				var rule = { method: method, parameters: rules[method] };
				try {
					var result = $.validator.methods[method].call( this, element.value.replace(/\r/g, ""), element, rule.parameters );

					// if a method indicates that the field is optional and therefore valid,
					// don't mark it as valid when there are no other rules
					if ( result == "dependency-mismatch" ) {
						dependencyMismatch = true;
						continue;
					}
					dependencyMismatch = false;

					if ( result == "pending" ) {
						this.toHide = this.toHide.not( this.errorsFor(element) );
						return;
					}

					if( !result ) {
						this.formatAndAdd( element, rule );
						return false;
					}
				} catch(e) {
					this.settings.debug && window.console && console.log("exception occured when checking element " + element.id
						 + ", check the '" + rule.method + "' method", e);
					throw e;
				}
			}
			if (dependencyMismatch)
				return;
			if ( this.objectLength(rules) )
				this.successList.push(element);
			return true;
		},

		// return the custom message for the given element and validation method
		// specified in the element's "messages" metadata
		customMetaMessage: function(element, method) {
			if (!$.metadata)
				return;

			var meta = this.settings.meta
				? $(element).metadata()[this.settings.meta]
				: $(element).metadata();

			return meta && meta.messages && meta.messages[method];
		},

		// return the custom message for the given element name and validation method
		customMessage: function( name, method ) {
			var m = this.settings.messages[name];
			return m && (m.constructor == String
				? m
				: m[method]);
		},

		// return the first defined argument, allowing empty strings
		findDefined: function() {
			for(var i = 0; i < arguments.length; i++) {
				if (arguments[i] !== undefined)
					return arguments[i];
			}
			return undefined;
		},

		defaultMessage: function( element, method) {
			return this.findDefined(
				this.customMessage( element.name, method ),
				this.customMetaMessage( element, method ),
				// title is never undefined, so handle empty string as undefined
				!this.settings.ignoreTitle && element.title || undefined,
				$.validator.messages[method],
				"<strong>Warning: No message defined for " + element.name + "</strong>"
			);
		},

		formatAndAdd: function( element, rule ) {
			var message = this.defaultMessage( element, rule.method ),
				theregex = /\$?\{(\d+)\}/g;
			if ( typeof message == "function" ) {
				message = message.call(this, rule.parameters, element);
			} else if (theregex.test(message)) {
				message = jQuery.format(message.replace(theregex, '{$1}'), rule.parameters);
			}
			this.errorList.push({
				message: message,
				element: element
			});

			this.errorMap[element.name] = message;
			this.submitted[element.name] = message;
		},

		addWrapper: function(toToggle) {
			if ( this.settings.wrapper )
				toToggle = toToggle.add( toToggle.parent( this.settings.wrapper ) );
			return toToggle;
		},

		defaultShowErrors: function() {
			for ( var i = 0; this.errorList[i]; i++ ) {
				var error = this.errorList[i];
				this.settings.highlight && this.settings.highlight.call( this, error.element, this.settings.errorClass, this.settings.validClass );
				this.showLabel( error.element, error.message );
			}
			if( this.errorList.length ) {
				this.toShow = this.toShow.add( this.containers );
			}
			if (this.settings.success) {
				for ( var i = 0; this.successList[i]; i++ ) {
					this.showLabel( this.successList[i] );
				}
			}
			if (this.settings.unhighlight) {
				for ( var i = 0, elements = this.validElements(); elements[i]; i++ ) {
					this.settings.unhighlight.call( this, elements[i], this.settings.errorClass, this.settings.validClass );
				}
			}
			this.toHide = this.toHide.not( this.toShow );
			this.hideErrors();
			this.addWrapper( this.toShow ).show();
		},

		validElements: function() {
			return this.currentElements.not(this.invalidElements());
		},

		invalidElements: function() {
			return $(this.errorList).map(function() {
				return this.element;
			});
		},

		showLabel: function(element, message) {
			var label = this.errorsFor( element );
			if ( label.length ) {
				// refresh error/success class
				label.removeClass().addClass( this.settings.errorClass );

				// check if we have a generated label, replace the message then
				label.attr("generated") && label.html(message);
			} else {
				// create label
				label = $("<" + this.settings.errorElement + "/>")
					.attr({"for":  this.idOrName(element), generated: true})
					.addClass(this.settings.errorClass)
					.html(message || "");
				if ( this.settings.wrapper ) {
					// make sure the element is visible, even in IE
					// actually showing the wrapped element is handled elsewhere
					label = label.hide().show().wrap("<" + this.settings.wrapper + "/>").parent();
				}
				if ( !this.labelContainer.append(label).length )
					this.settings.errorPlacement
						? this.settings.errorPlacement(label, $(element) )
						: label.insertAfter(element);
			}
			if ( !message && this.settings.success ) {
				label.text("");
				typeof this.settings.success == "string"
					? label.addClass( this.settings.success )
					: this.settings.success( label );
			}
			this.toShow = this.toShow.add(label);
		},

		errorsFor: function(element) {
			var name = this.idOrName(element);
    		return this.errors().filter(function() {
				return $(this).attr('for') == name;
			});
		},

		idOrName: function(element) {
			return this.groups[element.name] || (this.checkable(element) ? element.name : element.id || element.name);
		},

		checkable: function( element ) {
			return /radio|checkbox/i.test(element.type);
		},

		findByName: function( name ) {
			// select by name and filter by form for performance over form.find("[name=...]")
			var form = this.currentForm;
			return $(document.getElementsByName(name)).map(function(index, element) {
				return element.form == form && element.name == name && element  || null;
			});
		},

		getLength: function(value, element) {
			switch( element.nodeName.toLowerCase() ) {
			case 'select':
				return $("option:selected", element).length;
			case 'input':
				if( this.checkable( element) )
					return this.findByName(element.name).filter(':checked').length;
			}
			return value.length;
		},

		depend: function(param, element) {
			return this.dependTypes[typeof param]
				? this.dependTypes[typeof param](param, element)
				: true;
		},

		dependTypes: {
			"boolean": function(param, element) {
				return param;
			},
			"string": function(param, element) {
				return !!$(param, element.form).length;
			},
			"function": function(param, element) {
				return param(element);
			}
		},

		optional: function(element) {
			return !$.validator.methods.required.call(this, $.trim(element.value), element) && "dependency-mismatch";
		},

		startRequest: function(element) {
			if (!this.pending[element.name]) {
				this.pendingRequest++;
				this.pending[element.name] = true;
			}
		},

		stopRequest: function(element, valid) {
			this.pendingRequest--;
			// sometimes synchronization fails, make sure pendingRequest is never < 0
			if (this.pendingRequest < 0)
				this.pendingRequest = 0;
			delete this.pending[element.name];
			if ( valid && this.pendingRequest == 0 && this.formSubmitted && this.form() ) {
				$(this.currentForm).submit();
				this.formSubmitted = false;
			} else if (!valid && this.pendingRequest == 0 && this.formSubmitted) {
				$(this.currentForm).triggerHandler("invalid-form", [this]);
				this.formSubmitted = false;
			}
		},

		previousValue: function(element) {
			return $.data(element, "previousValue") || $.data(element, "previousValue", {
				old: null,
				valid: true,
				message: this.defaultMessage( element, "remote" )
			});
		}

	},

	classRuleSettings: {
		required: {required: true},
		email: {email: true},
		url: {url: true},
		date: {date: true},
		dateISO: {dateISO: true},
		dateDE: {dateDE: true},
		number: {number: true},
		numberDE: {numberDE: true},
		digits: {digits: true},
		creditcard: {creditcard: true}
	},

	addClassRules: function(className, rules) {
		className.constructor == String ?
			this.classRuleSettings[className] = rules :
			$.extend(this.classRuleSettings, className);
	},

	classRules: function(element) {
		var rules = {};
		var classes = $(element).attr('class');
		classes && $.each(classes.split(' '), function() {
			if (this in $.validator.classRuleSettings) {
				$.extend(rules, $.validator.classRuleSettings[this]);
			}
		});
		return rules;
	},

	attributeRules: function(element) {
		var rules = {};
		var $element = $(element);

		for (var method in $.validator.methods) {
			var value = $element.attr(method);
			if (value) {
				rules[method] = value;
			}
		}

		// maxlength may be returned as -1, 2147483647 (IE) and 524288 (safari) for text inputs
		if (rules.maxlength && /-1|2147483647|524288/.test(rules.maxlength)) {
			delete rules.maxlength;
		}

		return rules;
	},

	metadataRules: function(element) {
		if (!$.metadata) return {};

		var meta = $.data(element.form, 'validator').settings.meta;
		return meta ?
			$(element).metadata()[meta] :
			$(element).metadata();
	},

	staticRules: function(element) {
		var rules = {};
		var validator = $.data(element.form, 'validator');
		if (validator.settings.rules) {
			rules = $.validator.normalizeRule(validator.settings.rules[element.name]) || {};
		}
		return rules;
	},

	normalizeRules: function(rules, element) {
		// handle dependency check
		$.each(rules, function(prop, val) {
			// ignore rule when param is explicitly false, eg. required:false
			if (val === false) {
				delete rules[prop];
				return;
			}
			if (val.param || val.depends) {
				var keepRule = true;
				switch (typeof val.depends) {
					case "string":
						keepRule = !!$(val.depends, element.form).length;
						break;
					case "function":
						keepRule = val.depends.call(element, element);
						break;
				}
				if (keepRule) {
					rules[prop] = val.param !== undefined ? val.param : true;
				} else {
					delete rules[prop];
				}
			}
		});

		// evaluate parameters
		$.each(rules, function(rule, parameter) {
			rules[rule] = $.isFunction(parameter) ? parameter(element) : parameter;
		});

		// clean number parameters
		$.each(['minlength', 'maxlength', 'min', 'max'], function() {
			if (rules[this]) {
				rules[this] = Number(rules[this]);
			}
		});
		$.each(['rangelength', 'range'], function() {
			if (rules[this]) {
				rules[this] = [Number(rules[this][0]), Number(rules[this][1])];
			}
		});

		if ($.validator.autoCreateRanges) {
			// auto-create ranges
			if (rules.min && rules.max) {
				rules.range = [rules.min, rules.max];
				delete rules.min;
				delete rules.max;
			}
			if (rules.minlength && rules.maxlength) {
				rules.rangelength = [rules.minlength, rules.maxlength];
				delete rules.minlength;
				delete rules.maxlength;
			}
		}

		// To support custom messages in metadata ignore rule methods titled "messages"
		if (rules.messages) {
			delete rules.messages;
		}

		return rules;
	},

	// Converts a simple string to a {string: true} rule, e.g., "required" to {required:true}
	normalizeRule: function(data) {
		if( typeof data == "string" ) {
			var transformed = {};
			$.each(data.split(/\s/), function() {
				transformed[this] = true;
			});
			data = transformed;
		}
		return data;
	},

	// http://docs.jquery.com/Plugins/Validation/Validator/addMethod
	addMethod: function(name, method, message) {
		$.validator.methods[name] = method;
		$.validator.messages[name] = message != undefined ? message : $.validator.messages[name];
		if (method.length < 3) {
			$.validator.addClassRules(name, $.validator.normalizeRule(name));
		}
	},

	methods: {

		// http://docs.jquery.com/Plugins/Validation/Methods/required
		required: function(value, element, param) {
			// check if dependency is met
			if ( !this.depend(param, element) )
				return "dependency-mismatch";
			switch( element.nodeName.toLowerCase() ) {
			case 'select':
				// could be an array for select-multiple or a string, both are fine this way
				var val = $(element).val();
				return val && val.length > 0;
			case 'input':
				if ( this.checkable(element) )
					return this.getLength(value, element) > 0;
			default:
				return $.trim(value).length > 0;
			}
		},

		// http://docs.jquery.com/Plugins/Validation/Methods/remote
		remote: function(value, element, param) {
			if ( this.optional(element) )
				return "dependency-mismatch";

			var previous = this.previousValue(element);
			if (!this.settings.messages[element.name] )
				this.settings.messages[element.name] = {};
			previous.originalMessage = this.settings.messages[element.name].remote;
			this.settings.messages[element.name].remote = previous.message;

			param = typeof param == "string" && {url:param} || param;

			if ( this.pending[element.name] ) {
				return "pending";
			}
			if ( previous.old === value ) {
				return previous.valid;
			}

			previous.old = value;
			var validator = this;
			this.startRequest(element);
			var data = {};
			data[element.name] = value;
			$.ajax($.extend(true, {
				url: param,
				mode: "abort",
				port: "validate" + element.name,
				dataType: "json",
				data: data,
				success: function(response) {
					validator.settings.messages[element.name].remote = previous.originalMessage;
					var valid = response === true;
					if ( valid ) {
						var submitted = validator.formSubmitted;
						validator.prepareElement(element);
						validator.formSubmitted = submitted;
						validator.successList.push(element);
						validator.showErrors();
					} else {
						var errors = {};
						var message = response || validator.defaultMessage( element, "remote" );
						errors[element.name] = previous.message = $.isFunction(message) ? message(value) : message;
						validator.showErrors(errors);
					}
					previous.valid = valid;
					validator.stopRequest(element, valid);
				}
			}, param));
			return "pending";
		},

		// http://docs.jquery.com/Plugins/Validation/Methods/minlength
		minlength: function(value, element, param) {
			return this.optional(element) || this.getLength($.trim(value), element) >= param;
		},

		// http://docs.jquery.com/Plugins/Validation/Methods/maxlength
		maxlength: function(value, element, param) {
			return this.optional(element) || this.getLength($.trim(value), element) <= param;
		},

		// http://docs.jquery.com/Plugins/Validation/Methods/rangelength
		rangelength: function(value, element, param) {
			var length = this.getLength($.trim(value), element);
			return this.optional(element) || ( length >= param[0] && length <= param[1] );
		},

		// http://docs.jquery.com/Plugins/Validation/Methods/min
		min: function( value, element, param ) {
			return this.optional(element) || value >= param;
		},

		// http://docs.jquery.com/Plugins/Validation/Methods/max
		max: function( value, element, param ) {
			return this.optional(element) || value <= param;
		},

		// http://docs.jquery.com/Plugins/Validation/Methods/range
		range: function( value, element, param ) {
			return this.optional(element) || ( value >= param[0] && value <= param[1] );
		},

		// http://docs.jquery.com/Plugins/Validation/Methods/email
		email: function(value, element) {
			// contributed by Scott Gonzalez: http://projects.scottsplayground.com/email_address_validation/
			return this.optional(element) || /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i.test(value);
		},

		// http://docs.jquery.com/Plugins/Validation/Methods/url
		url: function(value, element) {
			// contributed by Scott Gonzalez: http://projects.scottsplayground.com/iri/
			return this.optional(element) || /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(value);
		},

		// http://docs.jquery.com/Plugins/Validation/Methods/date
		date: function(value, element) {
			return this.optional(element) || !/Invalid|NaN/.test(new Date(value));
		},

		// http://docs.jquery.com/Plugins/Validation/Methods/dateISO
		dateISO: function(value, element) {
			return this.optional(element) || /^\d{4}[\/-]\d{1,2}[\/-]\d{1,2}$/.test(value);
		},

		// http://docs.jquery.com/Plugins/Validation/Methods/number
		number: function(value, element) {
			return this.optional(element) || /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value);
		},

		// http://docs.jquery.com/Plugins/Validation/Methods/digits
		digits: function(value, element) {
			return this.optional(element) || /^\d+$/.test(value);
		},

		// http://docs.jquery.com/Plugins/Validation/Methods/creditcard
		// based on http://en.wikipedia.org/wiki/Luhn
		creditcard: function(value, element) {
			if ( this.optional(element) )
				return "dependency-mismatch";
			// accept only digits and dashes
			if (/[^0-9-]+/.test(value))
				return false;
			var nCheck = 0,
				nDigit = 0,
				bEven = false;

			value = value.replace(/\D/g, "");

			for (var n = value.length - 1; n >= 0; n--) {
				var cDigit = value.charAt(n);
				var nDigit = parseInt(cDigit, 10);
				if (bEven) {
					if ((nDigit *= 2) > 9)
						nDigit -= 9;
				}
				nCheck += nDigit;
				bEven = !bEven;
			}

			return (nCheck % 10) == 0;
		},

		// http://docs.jquery.com/Plugins/Validation/Methods/accept
		accept: function(value, element, param) {
			param = typeof param == "string" ? param.replace(/,/g, '|') : "png|jpe?g|gif";
			return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"));
		},

		// http://docs.jquery.com/Plugins/Validation/Methods/equalTo
		equalTo: function(value, element, param) {
			// bind to the blur event of the target in order to revalidate whenever the target field is updated
			// TODO find a way to bind the event just once, avoiding the unbind-rebind overhead
			var target = $(param).unbind(".validate-equalTo").bind("blur.validate-equalTo", function() {
				$(element).valid();
			});
			return value == target.val();
		}

	}

});

// deprecated, use $.validator.format instead
$.format = $.validator.format;

})(jQuery);

// ajax mode: abort
// usage: $.ajax({ mode: "abort"[, port: "uniqueport"]});
// if mode:"abort" is used, the previous request on that port (port can be undefined) is aborted via XMLHttpRequest.abort()
;(function($) {
	var pendingRequests = {};
	// Use a prefilter if available (1.5+)
	if ( $.ajaxPrefilter ) {
		$.ajaxPrefilter(function(settings, _, xhr) {
			var port = settings.port;
			if (settings.mode == "abort") {
				if ( pendingRequests[port] ) {
					pendingRequests[port].abort();
				}
				pendingRequests[port] = xhr;
			}
		});
	} else {
		// Proxy ajax
		var ajax = $.ajax;
		$.ajax = function(settings) {
			var mode = ( "mode" in settings ? settings : $.ajaxSettings ).mode,
				port = ( "port" in settings ? settings : $.ajaxSettings ).port;
			if (mode == "abort") {
				if ( pendingRequests[port] ) {
					pendingRequests[port].abort();
				}
				return (pendingRequests[port] = ajax.apply(this, arguments));
			}
			return ajax.apply(this, arguments);
		};
	}
})(jQuery);

// provides cross-browser focusin and focusout events
// IE has native support, in other browsers, use event caputuring (neither bubbles)

// provides delegate(type: String, delegate: Selector, handler: Callback) plugin for easier event delegation
// handler is only called when $(event.target).is(delegate), in the scope of the jquery-object for event.target
;(function($) {
	// only implement if not provided by jQuery core (since 1.4)
	// TODO verify if jQuery 1.4's implementation is compatible with older jQuery special-event APIs
	if (!jQuery.event.special.focusin && !jQuery.event.special.focusout && document.addEventListener) {
		$.each({
			focus: 'focusin',
			blur: 'focusout'
		}, function( original, fix ){
			$.event.special[fix] = {
				setup:function() {
					this.addEventListener( original, handler, true );
				},
				teardown:function() {
					this.removeEventListener( original, handler, true );
				},
				handler: function(e) {
					arguments[0] = $.event.fix(e);
					arguments[0].type = fix;
					return $.event.handle.apply(this, arguments);
				}
			};
			function handler(e) {
				e = $.event.fix(e);
				e.type = fix;
				return $.event.handle.call(this, e);
			}
		});
	};
	$.extend($.fn, {
		validateDelegate: function(delegate, type, handler) {
			return this.bind(type, function(event) {
				var target = $(event.target);
				if (target.is(delegate)) {
					return handler.apply(target, arguments);
				}
			});
		}
	});
})(jQuery);

/* ../global/lib/validation_rules.js */

/** @fileOverview  Validation Rules - depends on jquery.validate.js */

// Missing global variable definition for emailAjax
/* Scope in or declare window, susceptible to hoisting */
var lowes_simple_reg = window.lowes_simple_reg || {};

/**
	* Use with validation_rules.js messages object,
	Helps prevent same messages used more then once, and reduce file size and number of changes.
	If same validation message is used two or more times,
	please promote it into validation_rules_text Object below
	*/
var validation_rules_text = {
	msgInvalidCharEntered : 'Invalid characters entered. Please try again.',
	email_required: 'Please enter your email address.',
	checkout_email_required: 'Please enter email address.',
	email_invalid: 'Please enter a valid email address.',
	email_noMatch: 'That\'s an incorrect email address. Please try again.',

	email2_required: 'Please re-enter your email address.',
	email2_invalid: 'Email addresses must match; please re-enter.',
	
	phone_required: 'Please enter your phone number',

	password_required: 'Please enter your password.',
	password_invalid:'Your password doesn\'t appear to  be valid.',

	password2_required: 'Please re-enter your password.',
	password2_invalid: 'Passwords must match; please re-enter.',
	password2a_invalid: "Your password doesn't appear to be valid.",

	password3_required: 'Please re-enter your password. Please try again.',

	firstName_required: 'Please enter your first name.',
	firstName_invalid: "Invalid characters entered in first name. Please try again.",
	lastName_required: 'Please enter your last name.',
	lastName_invalid: "Invalid characters entered in last name. Please try again.",

	ccNickName_required: 'Please enter a credit card nickname.',
	ccNickName_invalid: "Invalid characters entered. Please try again.",

	zipCode_required: 'Please enter your ZIP code.',
	zipCode9_required:'Please enter a valid 5-digit or 9-digit ZIP code.',

	keyfob_required: "Please enter your MyLowe's card number.",
	keyfob_invalid_credit_card: "The number you entered appears to be a credit card number and is therefore invalid. Please enter a MyLowe's card number.",
	keyfob_invalid_mylowes_card: "The number you entered is invalid. Please check your MyLowe's card number and try again.",

	purchaseDate_invalid: "The date you entered is invalid. Please try again.",
	storeNumber_invalid: "The store number you entered is invalid. Please try again.",
	transNumber_invalid: "The transaction number you entered is invalid. Please try again.",
	confNumber_invalid: "The confirmation number you entered is invalid. Please try again.",
	poNumber_invalid: "The P.O. number you entered is invalid. Please try again.",
	moNumber_invalid: "The order number you entered is invalid. Please try again.",

	emailAjax_required: 'The email address you entered is already registered to a Lowes.com account. Please enter a new email address or sign in to access your account.',
	emailStatus_reserved: 'This is a reserved email address.',
	emailStatus_notfound: "We're Sorry; we don't have an account associated with this email address. Please try again.",

	ccNum_required:"Please enter a credit card number.",
	ccNum_invalid: "Please enter a valid card number.",

	// Address Book Validation
	address1_required: 'Please enter your address.',
	address1_invalid: 'Invalid characters entered in address 1. Please try again.',

	city1_required: 'Please enter a city.',
	city1_invalid: 'Invalid characters entered in city. Please try again.'
	// Address Book Validation End.
};

(function ($){
	var $formsToValidate = $('.ui-validateform');
	/*
		* automatically bold required fields so you don't have to
		* Please Use CSS for this.
		*/
	$formsToValidate.find('.required').prev("label").css("font-weight","bold");

	/* instantiate validation plugin */
	$formsToValidate.each(function (){
		var thisForm = $(this);
		thisForm.validate({
			debug: true,
			focusCleanup: true,
			focusInvalid: false,
			errorClass: "error",
			onkeyup: false,

			errorContainer: ".ui-formerror",

			submitHandler: function(form) {
				/* do additional code for a valid form */
				form.submit();
			},

			/* build aggregate error message on submission */
			invalidHandler: function(form, validator) {
						// ( validator.numberOfInvalids() ? $(form.target).find(".ui-formerror").show() : $(form.target).find(".ui-formerror").hide() );
			},

			/* group phone number as one validation relation */
			groups: { phone: "phone1 phone2 phone3", billingPhone: "billingPhone1" },

			messages: {

				exYear:{
					required: "Please enter an expiration date.",
					exYear: "Please enter a future date. The card expiration date can&apos;t occur in the past. "
				},

				exMonth:{
					required: "Please enter an expiration date.",
					exMonth: "Please enter a future date. The card expiration date can&apos;t occur in the past."
				},

				ccNickName:{
					required: validation_rules_text.ccNickName_required,
					ccNickName: validation_rules_text.ccNickName_invalid
				},

				ccType:{
					required: "Please choose a card type.",
					ccType: "Please choose a card type."
				},

				cc_cardNum:{
					required: validation_rules_text.ccNum_required ,
					cc_cardNum: validation_rules_text.ccNum_invalid
				},

				ccno:{
					required: validation_rules_text.ccNum_required,
					ccno: validation_rules_text.ccNum_invalid
				},

				firstName: {
					required: validation_rules_text.firstName_required,
					firstName: validation_rules_text.msgInvalidCharEntered
				},

				address1:{
					required: validation_rules_text.address1_required,
					address1: validation_rules_text.msgInvalidCharEntered
				},

				city1:{
					required: validation_rules_text.city1_required,
					address1: validation_rules_text.city1_invalid
				},

				lastName: {
					required: validation_rules_text.lastName_required,
					lastName: validation_rules_text.msgInvalidCharEntered
				},
				email1: {
					required: validation_rules_text.checkout_email_required,
					email1: validation_rules_text.email_invalid
				},
				Ecom_User_ID: {
					required: validation_rules_text.email_required,
					email1: validation_rules_text.email_invalid
				},
				resetLogonId: {
					required: validation_rules_text.email_required,
					email: validation_rules_text.email_invalid
				},
				email2: {
					required: validation_rules_text.email2_required,
					email2: validation_rules_text.email2_invalid
				},
				password1: {
					required: validation_rules_text.password_required,
					password1: validation_rules_text.password_invalid
				},
				logonPassword: {
					required: validation_rules_text.password_required,
					password1: validation_rules_text.password_invalid
				},
				password2: {
					required: validation_rules_text.password2_required,
					password2: validation_rules_text.password2a_invalid
				},
				password3: {
					required: validation_rules_text.password3_required,
					password3: validation_rules_text.password2_invalid
				},

				oldLogonPassword:{
					//required: validation_rules_text.password_required,
					// oldLogonPassword: validation_rules_text.password_invalid
					required: "Please enter your current password.",
					oldLogonPassword: "Please enter your current password."
				},
				challengeQuestion: "Please select a security question.",
				challengeAnswer: "Please enter a security answer.",
				addressName: "Please enter an address nickname.",
				addressLine1: "Please enter your address.",
				city: "Please enter your city.",
				state: "Please select your state.",
				addressField2: "Please enter an address nickname.",
				applyLCCPromo: "Your order qualifies for multiple offers. Please select one of the available options.",
				yourState: "Please select a state.",
				zipCode: {
				required: validation_rules_text.zipCode_required,
				digits: "Please enter a valid ZIP code."
				},
				zipCode9: validation_rules_text.zipCode9_required,
				billingPhone1: validation_rules_text.phone_required,
				"billing-address-phone1": validation_rules_text.phone_required,
				phone: "Please enter your entire 10-digit phone number including the area code.",
				keyfob: "The card number you entered is invalid. Please check your MyLowe's card number and try again.",

				Ecom_BillTo_Postal_Name_First: validation_rules_text.firstName_required,
				Ecom_BillTo_Postal_Name_Last: validation_rules_text.lastName_required,
				logonPasswordVerify: {
					required: validation_rules_text.password2_required,
					equalTo: validation_rules_text.password2_invalid
				},
				logonIdVerify: {
					required: validation_rules_text.email2_required,
					equalTo: validation_rules_text.email2_invalid,
					logonIdVerify: validation_rules_text.email_invalid
				},
				logonId:{
					required: validation_rules_text.email_required,
						logonId: validation_rules_text.email_invalid
				},
				Ecom_BillTo_Postal_PostalCode: {
					required: validation_rules_text.zipCode_required
				}

			},

			rules: {
				password2: { equalTo: "#password1" },
				email2: { equalTo: "#email1" },
				phoneUS: {required:true, phoneUS:true},
				billingPhone1: {required:true, minlength: 14, maxlength: 14},
				"billing-address-phone1": {required:true, minlength: 14, maxlength: 14},
				logonIdVerify: { equalTo: "#logonId" },
				logonPasswordVerify: { equalTo: "#logonPassword" }
				//,logonPassword: { password: true }
			},

			/* handle phone number special case */
			errorPlacement: function(error, element) {
				var nl = element.attr("name");
				if (nl == "phone1" || nl == "phone2" || nl == "phone3") {
					error.insertAfter("#phone3");
				}
				if (nl == "billingPhone1" || nl == "billingPhone2" || nl == "billingPhone3") {
					error.insertAfter(".billingPhoneParent .form-input-help:first");
				}
				if ($(element).hasClass("find-purchase-input")) {
					error.insertBefore($(element).parent().siblings(".context"))
				} else {
					error.insertAfter(element);
				}
			},

			highlight: function(element, errorClass) {
				$(element).parent("li").addClass(errorClass);
			},
			unhighlight: function(element, errorClass) {
				$(element).parent("li").removeClass(errorClass);
			}
		});
	});

	/**
		*	Extend custom methods not included in base plugin.
		*	Add custom validation rules here.
		*/

	$.validator.addMethod("poNumber", function(value, element) {
		return this.optional(element) || /^[0-9]{1,9}$/.test(value);
	}, validation_rules_text.poNumber_invalid);

	$.validator.addMethod("moNumber", function(value, element) {
		return this.optional(element) || /^[0-9]{7,15}$/.test(value);
	}, validation_rules_text.moNumber_invalid);

	$.validator.addMethod("confNumber", function(value, element) {
		return this.optional(element) || /^[0-9 \-]{7,14}$/.test(value)
	}, validation_rules_text.confNumber_invalid);

	$.validator.addMethod("transNumber", function(value, element) {
		return this.optional(element) || /^[0-9]{7,9}$/.test(value);
	}, validation_rules_text.transNumber_invalid);

	$.validator.addMethod("storeNumber", function(value, element) {
		return this.optional(element) || /^[0-9]{4}$/.test(value);
	}, validation_rules_text.storeNumber_invalid);

	$.validator.addMethod("purchaseDate", function(value, element) {
		var minDate = new Date(2011, 12 - 1, 19),
		maxDate = new Date(),
		thisDate = Date.parse(value);
		if((!/Invalid|NaN/.test(new Date(value))) && (thisDate >= minDate) && (thisDate <= maxDate) && /[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}/.test(value))
			return true;
		else
			return false;
	}, validation_rules_text.purchaseDate_invalid);

	$.validator.addMethod("ccNickName", function(value, element) {
		return /^\s*([0-9a-zA-Z #\-]*)\s*$/.test(value);

	}, validation_rules_text.ccNickName_required);

	$("#firstName").addClass("firstName");
	$.validator.addMethod("firstName", function(value, element) {
		return /^\s*([0-9a-zA-Z \-,.\']*)\s*$/.test(value);
	}, validation_rules_text.firstName_invalid);

	$.validator.addMethod("address1", function(value, element) {
		return /^\s*([0-9a-zA-Z- \'.\/,:;]*)\s*$/.test(value);
	}, validation_rules_text.address1_invalid);

	$.validator.addMethod("address2", function(value, element) {
		return /^\s*([0-9a-zA-Z- \'.\/,:;]*)\s*$/.test(value);
	}, validation_rules_text.address1_invalid);

	$("#lastName").addClass("lastName");
	$.validator.addMethod("lastName", function(value, element) {
		return /^\s*([0-9a-zA-Z \-,.\']*)\s*$/.test(value);
	}, validation_rules_text.lastName_invalid);

	$.validator.addMethod("email", function(value, element) {Array
		if(this.optional(element) || /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i.test(value)){
			$(element).val(value.toLowerCase());
		}
		return this.optional(element) || /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i.test(value);
	}, validation_rules_text.email_invalid);

	if ( !typeof utag === 'object' && utag_data.page_type !== 'checkout-login' ) {
		$.validator.addMethod("password", function(value, element) {
			// IE7 Defect, cant use:
			//return /(?=.*[a-zA-Z])(?=.*[0-9])(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9\W]{6,12})$/.test(value);
			return /(?=^[^\s]{6,12}$)(?=.*\d.*)(?=.*[a-zA-Z].*)^(?!.*(.)\1{3})/.test(value);
		}, "Your password doesn't appear to  be valid.");
	}

	$.validator.addMethod("password2", function(value, element) {
		//return /(?=.*[a-zA-Z])(?=.*[0-9])(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9\W]{6,12})$/.test(value);
		return /(?=^[^\s]{6,12}$)(?=.*\d.*)(?=.*[a-zA-Z].*)^(?!.*(.)\1{3})/.test(value);
	}, "Your password doesn't appear to be valid.");

	// Validate Zip code field
	$.validator.addMethod("zipcode", function(value, element) {
		var isValidZipCode = true;
		// Empty zip value.
		lowes_simple_reg.msg_sr_zipcode = '';
		if ($.trim(value) == ''){
			lowes_simple_reg.msg_sr_zipcode = validation_rules_text.zipCode_required;
		}

		// test length of value using regex
		if ( /^([0-9]{5}(?:-[0-9]{4})?)*$/.test(value) == true ) {
			$.ajax({
				method: 'get',
				async: false,
				url: '/webapp/wcs/stores/servlet/AjaxCheckZipCode',
				data : { zipCode : value, storeId : "10151"  },
				success: function(msg, data) {

					var $element;
					var objErrors = {};
					var protocol;
					if (msg.indexOf('ERROR_ZIP_CODE_NOT_VALID') != -1 ){
						// zip code is invalid

						$element = $(element);

						// Setup Error validation
						lowes_simple_reg.msg_sr_zipcode = 'Please enter a valid 5-digit ZIP code.';
						objErrors[ $element.attr('name') ] = lowes_simple_reg.msg_sr_zipcode;
						$element.closest('.ui-validateform').validate().showErrors(objErrors);

						// Register Modal, Auto height left,right column.
						if (typeof lowesRegisterModal == 'object'){
							lowesRegisterModal.autoHeightFieldsSignupModal();
							protocol = $(document).getUrlParam('protocol') || $('#tf_protocol').val(); // http or https
							$('body').append('<iframe src="' + protocol + '://' + window.location.host + '/MContent/Structured/Member/account_iframe_success.html?callback=acctModal.resizeOnloadRegisterError" style="width:0px;height:0px;display:none;"/>');
						}

						// Add Class to check if validation failed.
						$(element).addClass('validator_failed');

						isValidZipCode = false;
						return false;
					} else {
						// the zip code was valid
						$(element).removeClass('validator_failed');
						$(element).parent('li').removeClass('error');

						isValidZipCode = true;
						return true;
					}
				}
			});
		} else {
			// failed standard format validation
			isValidZipCode = false;
			lowes_simple_reg.msg_sr_zipcode = validation_rules_text.zipCode_required;
		}

		return isValidZipCode;

	}, function() { return lowes_simple_reg.msg_sr_zipcode; } );

	/* bind focus highlighting */
	$('.ui-input, .ui-textarea').on('focus',function() {
		$(this).closest('li').addClass('focus');
	}).on('blur', function(){
		var $this = $(this);
		$this.closest('li').removeClass('focus');
	});

	$.validator.addMethod("giftcard-quantity", function(value, element) {
		var num = parseInt(value);
		element.value = num.toString().replace(/^0+/, "");
		return (num > 0 && num <= 10);
	}, "Please enter a valid quantity.");

	$.validator.addMethod("gcAmount", function(value, element) {
		var num = parseFloat(value);
		if (value.length && value[0] == "$") {
			num = parseFloat(value.replace(/\$/gi,""));
		}
		num = num.toString().replace(/^0+/, "");
		if (!num || num == "NaN") {
			element.value = "";
		} else {
			element.value = num;
		}
		return (!$(element).hasClass('required') || (num >= 5 && num <= 500));
	}, "Please enter a valid amount.");

	$.validator.addMethod("gift-msg-txtarea", function(value, element) {
		var $element = $(element);
		$element.val(value.replace(/[\n\r]+/g,''));
		return true;
	});

	$.validator.addMethod("chat-question", function(value, element) {
		var $element = $(element),
			ccNum = value.match(/\d([0-9]|\s|-)*\d/g);
		if (ccNum && ccNum.length) {
			for (var i = 0, ii = ccNum.length; i < ii; i++) {
				var numbersOnly = ccNum[i].replace(/\s|-/gi,'');
				if (numbersOnly.length > 12 && numbersOnly.length < 17) {
					var lastFour = numbersOnly.substring(numbersOnly.length-4,numbersOnly.length);
					var maskedNum = ccNum[i].replace(/\d/gi,'X').toString();
					maskedNum = maskedNum.replace(/(X|-){4}$/gi,lastFour).toString();
					$element.val($element.val().replace(ccNum[i],maskedNum));
				}
			}
		}
		return true;
	});


	// Credit Card Max Number 19 , Actually this only checks if it is numbers.
	$.validator.addMethod("cc_cardNum", function(value, element) {
		//$.trim(value).replace(/ /g,'');
		return /^([0-9]{13,25}?)*$/.test(value);
	}, "The card number you entered is invalid. Please check your card number and try again.");


	$(".keyfob").addClass("keyfob_card_number");
	$.validator.addMethod("keyfob", function(value, element) {
		if(value.substring(0,3) == "819")
			return true;
		else
			return /^(48[0-9]{13}?)*$/.test(value);
	}, validation_rules_text.keyfob_invalid_mylowes_card);

	$.validator.addMethod("keyfob_card_number", function(value, element) {
		if(value.substring(0,3) == "819")
			return false;
		else
			return true;
	}, validation_rules_text.keyfob_invalid_credit_card);

	/**
		*	US Phone Number fields.
		*/

	$.validator.addMethod("phoneprefix", function(value, element) {
		var valid = /^([0-9]{3}?)*$/.test(value);
		/*Give element focus to make sure error evt is captured before
				focus goes to next item negating the error */
		if(!valid) { element.focus(); }
		return /^([0-9]{3}?)*$/.test(value);
	}, "Please enter your entire nine-digit phone number including the area code.");

	$.validator.addMethod("phonePrefix", function(value, element) {
		var valid = /^([0-9]{3}?)*$/.test(value);
		/*Give element focus to make sure error evt is captured before
				focus goes to next item negating the error */
		// if(!valid) { element.focus(); }
		return /^([0-9]{3}?)*$/.test(value);
	});

	$.validator.addMethod("phoneLineNumber", function(value, element) {
		var valid = /^([0-9]{4}?)*$/.test(value);
		/*Give element focus to make sure error evt is captured before
				focus goes to next item negating the error */
		// if(!valid) { element.focus(); }
		return /^([0-9]{4}?)*$/.test(value);
	});

	$.validator.addMethod("phone", function(value, element) {
		return /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/.test(value);
	}, "Please enter your entire 10-digit phone number including the area code.");

	/* handle auto tabbing for phone field */
	$("#phone1, #phone2").keyup(function(event) {
		var $this = $(this);
		if (event.keyCode !== 9 && $this.val().length === 3) { $this.next().focus(); }
	});

	$.validator.addMethod("phoneUS", function(phone_number, element) {
		phone_number = phone_number.replace(/\s+/g, "");

		if(this.optional(element) || phone_number.length > 9 &&
					phone_number.match(/^(1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/)){

			for(var i=0; i<=phone_number.length; i++){
				phone_number = phone_number.replace(RegExp("[^0-9]"), "");
			}

			if(phone_number.length > 10){
				phone_number = phone_number.replace(RegExp("^1"), "");
			}

			$(element).val(phone_number);
		}

		return this.optional(element) || phone_number.length > 9 &&
			phone_number.match(/^(1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);
	}, "Please enter your entire 10-digit phone number including the area code.");

	$.validator.addMethod("phonesuffix", function(value, element) {
		return /^([0-9]{4}?)*$/.test(value);
	}, "Please enter your entire nine-digit phone number including the area code.");
	/** End Phone Checks */

	/* Email Ajax Validation, check if email is in use. */
	$.validator.addMethod("emailAjax", function (value, element){
		lowes_simple_reg.msg_sr_email = '';
		// Empty email value.
		if ($.trim(value) == ''){
			lowes_simple_reg.msg_sr_email = validation_rules_text.email_required;
		}
		// validate email
		if (/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i.test(value) == true){
			// Ajax Check email is already in use:
			$.ajax({
				method: 'get',
				url: "/webapp/wcs/stores/servlet/AjaxCheckEmail",
				data: {
					Ecom_User_ID : value
				},
				success: function (msg){
					var $element;
					var objErrors = {};
					var protocol;
					if (msg.indexOf('_ERR_LOGONID_ALREDY_EXIST') != -1 ){
						$element = $(element);

						// Setup Error validation
						lowes_simple_reg.msg_sr_email = validation_rules_text.emailAjax_required;
						objErrors[ $element.attr('name') ] = validation_rules_text.emailAjax_required;
						$element.closest('.ui-validateform').validate().showErrors(objErrors);

						// Register Modal, Auto height left,right column.
						if (typeof lowesRegisterModal == 'object'){
							lowesRegisterModal.autoHeightFieldsSignupModal();
							protocol = $(document).getUrlParam('protocol') || $('#tf_protocol').val(); // http or https
							$('body').append('<iframe src="' + protocol + '://' + window.location.host + '/MContent/Structured/Member/account_iframe_success.html?callback=acctModal.resizeOnloadRegisterError" style="width:0px;height:0px;display:none;"/>');
						}

						// Add Class to check if validation failed.
						$(element).addClass('validator_failed');
						return false;
					}
				}
			});
		} else {
			// invalid email format
			lowes_simple_reg.msg_sr_email = validation_rules_text.email_invalid;
		}

		return !lowes_simple_reg.msg_sr_email.length;
	}, function() { return lowes_simple_reg.msg_sr_email; } );

	/* Account Status Validation, check if email is in use, reserved or invalid class=emailStatus */
	$.validator.addMethod("emailStatus", function (value, element){
		lowes_simple_reg.msg_sr_email = '';
		// Empty email value.
		if ($.trim(value) == ''){
			lowes_simple_reg.msg_sr_email = validation_rules_text.email_required;
		}

		// Check for Reserved Message + Remove if Present
		if ($('#Logon .ui-form .ui-informational').length) {
			$('#Logon .ui-form .ui-informational').hide();
		}

		// validate email
		if (/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i.test(value) == true){
			$.ajax({
				headers: {"Authorization": "Basic U0VNQTpvamlld2ZvaWZ1cmZ1NDMw"},
				method: 'get',
				url: "/wcs/resources/store/10151/member/userStatus/v1_0/" + value,
				success: function (msg){
					var $element = $(element),
						objErrors = {},
						protocol;

					// Check for Status of Account
					// Existing Account
					if (msg.x_accountStatus === "ACTIVE") {
						return false;
					}
					// Reserved Account
					else if (msg.x_accountOrigination === "ASSOCIATE" && msg.x_accountStatus === "INACTIVE") {
						// Setup Error validation
						lowes_simple_reg.msg_sr_email = validation_rules_text.emailAjax_reserved;
						objErrors[ $element.attr('name') ] = validation_rules_text.emailStatus_reserved;
						$element.closest('.ui-validateform').validate().showErrors(objErrors);

						// Show Reserved Message
						$('#Logon .ui-form .unclaimed-acct-message').show();

						// Add Class to check if validation failed.
						$(element).addClass('validator_failed');
						return false;
					}
				},
				error:function(msg) {
					return false;
				}
			});

		} else {
			// invalid email format
			lowes_simple_reg.msg_sr_email = validation_rules_text.email_invalid;
			return false;
		}

		return !lowes_simple_reg.msg_sr_email.length;
	}, function() { return lowes_simple_reg.msg_sr_email; } );

		/* Clear Form JS */
		/* Let's scope this class in a bit i.e. .ui-form-reset */
	$(".reset").click(function(e) {
		e.preventDefault();
		$('.ui-validateform').each(function (){
			$(this).validate().resetForm();
		});

	});
})(jQuery);

/* ../global/memberGroup.js */

(function (Lowes){
	/**
	 * Simplified cookie-based information for MemberGroups
	 * @namespace
	 */
	Lowes.MemberGroup = {
		/** Stores our preferences, key/value pair. */
		data: {},

		/**
		 * Grabs our preferences cookie (json string) and 
		 * loads the result object into our local object.
		 */
		load: function() {
			this.data = (Lowes.Cookie.get('memberGroup')) ? $.parseJSON(Lowes.Cookie.get('memberGroup')) : {};
		},

		/**
		 * Saves our preferences, as a JSON string, to our cookie path
		 *
		 * @param {Date} expires Date object for when cookie should expire.
		 * @param {String} path Path to set the cookie too.
		 */
		save: function(expires, path) {
			// Setup our date
			expires = expires || new Date(2020, 2, 2);
         
			// Setup our path
			path = path || '/';

			// Store our cookie
			Lowes.Cookie.set('memberGroup', "%@;path=%@;expires=%@".$$(escape(JSON.stringify(this.data)), path, expires.toUTCString()));
		}
	};
	// Expose Updated Lowes namespace to global.
	window.Lowes = Lowes;
}(window.Lowes || {}));

/* ../global/memberGroupInject.js */

(function ($){
	var CBCCookie = Lowes.Cookie.get('memberGroup'),template = [],catalogCount = 0,re = /c\w\wN/g,
		memberGroupParsed,userDetailsParsed,varBusinessName,cbcSimpleBannerOrders,cbcSimpleBannerProfile,
		cbcLastViewedLink,cbcLastViewedName,orderDate,catListTemplate='',i=0,j,id,catName,$multipleCatalogModal;

	if(CBCCookie) {
		//Last Viewed Catalog determination
			//extract catalog id from page URL
		$(document).on('click', '#multipleCatalogModalContent a, .cbc-signed-in-header-module a, .cbc-my-catalogs a', function (){
			var theUrl = $(this).attr('href')/*window.location.href*/, theUrlArray, theUrlArrayPartial, theId, idWithinCookie, idReference;
			var memberGroupParsed = $.parseJSON(CBCCookie);

			if(theUrl.indexOf("pl_") > -1){//use conditional indexOf() before split() to avoid errors
				theUrlArray = theUrl.split("pl_");
				if(theUrl.indexOf("_") > 0){
					theUrlArrayPartial = theUrlArray[1].split("_");
					theId = theUrlArrayPartial[1];
					idWithinCookie = CBCCookie.indexOf(theId);
					idReference = CBCCookie.replace(theId, "").substring((idWithinCookie - 8), (idWithinCookie - 3));

					memberGroupParsed.lastViewedCatalog = idReference;
					Lowes.Cookie.set('memberGroup', JSON.stringify(memberGroupParsed), 30);
				}
			}
			return true;
		});
		//END Last Viewed Catalog determination

		//count how many catalogs
		for( catalogCount = 0; re.exec(CBCCookie); ++catalogCount ){};

		//some quick variables for display
		memberGroupParsed = $.parseJSON(CBCCookie);
		userDetailsParsed = $.parseJSON(Lowes.Cookie.get('userDetails'));

		//BANNER
		//Content for banner
		varBusinessName = (memberGroupParsed.bName.length > 30)? (memberGroupParsed.bName.substring(0, 30) + "...") : memberGroupParsed.bName;
		cbcSimpleBannerOrders = $('#nav-my-order').attr('href');
		cbcSimpleBannerProfile = $('#nav-my-profile').attr('href');
		setMemberGroupEndeca = (!!memberGroupParsed.lastViewedCatalog) ? memberGroupParsed[memberGroupParsed.lastViewedCatalog] : memberGroupParsed.c01Id;
		memberGroupEndeca = parseInt(((setMemberGroupEndeca).split('_')[0])).toString(36);
		cbcLastViewedLink = '/' + Lowes.Cookie.get('firstName').replace(/'/g, '') + '/_/N-' + memberGroupEndeca + '/pl?rpp=16';

		template.push('<div id="cbc-global-banner-enhanced">',
			'<div class="cbc-portion-account-id">',
				'<h2 class="cbc-company-name">', varBusinessName, '</h2>',
				'<p class="cbc-welcome-back">Welcome back, <a href="', cbcSimpleBannerProfile, '">', Lowes.Cookie.get('firstName'), '</a> <span>(<a href="', $(".ui-event-signout").attr("href"), '">Sign Out</a>)</span></p>',
				'<p class="cbc-home-link"><a href="http://www.lowes.com/webapp/wcs/stores/servlet/LowesCBCLandingView?10151">Lowe\'s For Pros Home</a></p>',
			'</div>',
			'<div class="cbc-portion-my-resources">',
				'<div class="cbc-my-orders">',
					'<h3>My Orders</a></h3>',
					'<ul>');
				if(userDetailsParsed != null){
					if(typeof userDetailsParsed.lastOrder.uiOrderId != 'undefined'){
						orderDate = userDetailsParsed.lastOrder.date;
						orderDate = orderDate.split(" ");
						orderDate = orderDate[0];
						orderDate = orderDate.split("-");
						orderDate = [orderDate[1], orderDate[2], orderDate[0]].join('/');
						template.push('<li>',
								'<dl>',
									'<dt>Last Order: </dt>',
									'<dd><a href="http://www.lowes.com/LowesOrderDetail?orderId=%27, userDetailsParsed.lastOrder.wcOrderId, %27&storeId=10151&langId=-1&catalogId=10051">#', userDetailsParsed.lastOrder.uiOrderId, '</a> on ', orderDate, '</dd>',
								'</dl>',
							'</li>',
							'<li class="cbc-view-orders">',
								'<a href="', cbcSimpleBannerOrders, '">View My Orders</a>',
							'</li>');
					}else{
						template.push('<li>',
								'<dl>',
									'<dt>Order now.</dt>',
									'<dd></dd>',
								'</dl>',
							'</li>');
					}

				}

				template.push('</ul>',
				'</div>',
				'<div class="cbc-my-catalogs">',
					'<h3>My Catalogs</h3>',
					'<ul>');
			if ("lastViewedCatalog" in memberGroupParsed){
				cbcLastViewedName = memberGroupParsed[memberGroupParsed.lastViewedCatalog.replace(/Id/gi, "N")];
				template.push('<li>',
							'<dl>',
								'<dt>Last Viewed Catalog: </dt>',
								'<dd><a href="', cbcLastViewedLink, '">', cbcLastViewedName, '</a></dd>',
							'</dl>',
						'</li>');
				}
			if(!catalogCount){
				template.push('<li class="cbc-view-catalog">',
						'<dl>',
							'<dt>You do not have a custom catalog set up. Please contact your DCAS if you would like a catalog.</dt>',
							'<dd></dd>',
						'</dl>',
					'</li>');
			}
			if(catalogCount == 1){
				template.push('<li class="cbc-view-catalog">',
							'<a href="http://www.lowes.com/pl_%27, memberGroupParsed.bName.replace(/ /gi,"+"), '_', memberGroupParsed.c01Id, '">See Your Catalog</a>',
						'</li>');
			}
			if(catalogCount > 1){
				template.push('<li class="cbc-view-catalogs">',
							'<a href="#view-all-catalogs">See All ', catalogCount, ' Catalogs</a>',
						'</li>');

			}
			template.push('</ul>',
				'</div>',
			'</div>',
		'</div>');
		//add to DOM
		$('#page-block').prepend(template.join(''));

		//MULTIPLE CATALOG: we already established catalog count, now loop through the catalog cookie

		while (i<= (catalogCount-1)){
			j = ("0" + (i + 1)).slice (-2); //pad zeros to match cookie format
			id = ['c',j,'Id'].join('');
			catName = ['c', j, 'N'].join('');
			catListTemplate += '<li><a href="http://www.lowes.com/pl_%27 + memberGroupParsed.bName.replace(/ /gi,"+") + '_' + memberGroupParsed[id] + '">' + memberGroupParsed[catName] + '</a></li>';
			i++;
		}
		$("body").append(['<div id="multipleCatalogModal" class="multipleCatalogModal" style="display:none;">',
				'<div id="multipleCatalogModalContent">',
					'<h3>Your Catalogs <span>(', catalogCount, ')</span></h3>',
					'<ul>', catListTemplate, '</ul>',
				'</div>',
			'</div>'].join('')
		);
		//show MULTIPLECatalogs' lightbox
		$multipleCatalogModal = $('#multipleCatalogModal').dialog({
			modal:true,
			dialogClass:'multipleCatalogModal',
			autoOpen: false,
			resizable: false,
			draggable: false,
			width:455
		});

		$(document).on('click', '.cbc-view-catalogs a', function() {
			$multipleCatalogModal.dialog('open');
		});

		//non-banner but CBC relevant: "help" phone number replacement
		$('.help').html(function (index, oldhtml){ return oldhtml.replace('1-800-445-6937','1-866-284-3029'); });
	}
}(jQuery));

/* ../foresee/foresee-trigger.js */

var $$FSR = {
   'timestamp': 'February 26, 2015 @ 1:40 PM',
   'version': '15.3.5',
   'enabled': true,
   'frames' : false,
   'sessionreplay': true,
   'auto' : true,
   'encode' : false,
   'files': '/etc/clientlibs/lowes/foresee/foresee/',
    'js_files': '/etc/clientlibs/lowes/foresee/javascript/foresee/',
    'image_files': '/etc/clientlibs/lowes/foresee/foresee/',
    'html_files': '/etc/clientlibs/lowes/foresee/foresee/',
    'css_files': '/etc/clientlibs/lowes/foresee/css/foresee/',
   // needs to be set when foresee-transport.swf is not located at 'files'
   //'swf_files': '__swf_files_'
   'id': 'ooHPEq6j8lMgGxndybifdA==',
   'definition': 'Unknown_83_filename'/*tpa=http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/foresee-surveydef.js*/,
   'embedded': false,
   'replay_id': 'http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/lowes.com',
   'attach': false,
   'renderer':'W3C',  // or "ASRECORDED"
   'layout':'CENTERFIXED',  // or "LEFTFIXED" or "LEFTSTRETCH" or "CENTERSTRETCH"
   'pools' : [
      {
         path: '.',
         sp: 20  // CHANGE ONLY WHEN INCLUDING SESSION REPLAY
      }
   ],
   'sites': [
      {
         path: /\w+-?\w+\.(com|org|edu|gov|net|co\.uk)/
      },
      {
         path: '.',
         domain: 'default'
      }
   ],
   storageOption: 'cookie'
};

var FSRCONFIG = {};

// -------------------------------- DO NOT MODIFY ANYTHING BETWEEN THE DASHED LINES --------------------------------
if (typeof(FSR) == "undefined") {
(function(config){function S(){return function(){}}function da(ta){return function(){return ta}}
(function(ta,Ca){function oa(a,b){m.controller.execute(m.controller.qe,d._sd(),{sp:a,when:b,qualifier:void 0,invite:!1})}function ua(a,b,c){setTimeout(function(){a.Ni(b,c)},1)}function K(a,b){return(b?a.get(b):a)||""}function ka(a){return[a||g.n(),(a||g.n()).get("cp")||{}]}function la(a,b,c){d.q(a.length)||(a=[a]);for(var f=0;f<a.length;f++)O(a[f],b,c)}function va(a,b,c){var f=[];if(0<a.length){var e,k,p,g,r=/[\.:\[#]/g,h=[];if(r.test(a))for(var r=a.match(r),l=0;l<r.length;l++){var m=a.indexOf(r[l]);
h.push({ue:a.substr(0,m),Aj:r[l]});a=a.substr(m)}h.push({ue:a});a=h[0].ue.toUpperCase();for(r=h.length-1;1<=r;r--)l=h[r-1].Aj,m=h[r].ue,"["==l?k=m.substr(1,m.length-2).split("="):"."==l?p=m.substr(1):"#"==l?e=m.substr(1):":"==l&&(g=parseInt(m.replace(":nth-child(","").replace(")","")));0==a.length&&(a="*");if(c)for(r=b.childNodes.length-1;0<=r;r--)c=b.childNodes[r],1!=c.nodeType||"*"!=a&&c.tagName!=a||f.push(c);else f=wa(b.getElementsByTagName(a));if(e||k||p||g)for(r=f.length-1;0<=r;r--)(g&&d.vi(f[r])!=
g-1||p&&-1==f[r].className.indexOf(p)||e&&f[r].id!=e||k&&0>f[r].getAttribute(k[0]).indexOf(k[1]))&&f.splice(r,1)}return f}function wa(a){var b=[],c,d=0;for(c=b.length=a.length;d<c;d++)b[d]=a[d];return b}function F(a){var b=A.createElement("div");b.innerHTML=a;a=b.firstChild;a.parentNode.removeChild(a);var b=t.Le.qh,c;for(c in b)a[c]=b[c];return a}function ma(a,b){var c,d,e,k,p=L,g,r=b[a];r&&("object"===typeof r&&"function"===typeof r.toJSON)&&(r=r.toJSON(a));"function"===typeof Q&&(r=Q.call(b,a,r));
switch(typeof r){case "string":return pa(r);case "number":return isFinite(r)?String(r):"null";case "boolean":case "null":return String(r);case "object":if(!r)return"null";L+=ea;g=[];if("[object Array]"===Object.prototype.toString.apply(r)){k=r.length;for(c=0;c<k;c+=1)g[c]=ma(c,r)||"null";e=0===g.length?"[]":L?"[\n"+L+g.join(",\n"+L)+"\n"+p+"]":"["+g.join(",")+"]";L=p;return e}if(Q&&"object"===typeof Q)for(k=Q.length,c=0;c<k;c+=1)"string"===typeof Q[c]&&(d=Q[c],(e=ma(d,r))&&g.push(pa(d)+(L?": ":":")+
e));else for(d in r)Object.prototype.hasOwnProperty.call(r,d)&&(e=ma(d,r))&&g.push(pa(d)+(L?": ":":")+e);e=0===g.length?"{}":L?"{\n"+L+g.join(",\n"+L)+"\n"+p+"}":"{"+g.join(",")+"}";L=p;return e}}function pa(a){qa.lastIndex=0;return qa.test(a)?'"'+a.replace(qa,function(a){var c=Da[a];return"string"===typeof c?c:"\\u"+("0000"+a.charCodeAt(0).toString(16)).slice(-4)})+'"':'"'+a+'"'}function Ea(a,b){var c=[],d;for(d in a)a.hasOwnProperty(d)&&(c[d]=b(a[d]));return c}function Fa(a,b,c){X.isSupported(b.version)&&
(a.innerHTML=X.Bi(b,c),d.P&&(l[b.id]=A.getElementById(b.id)),d.K(this,{rk:function(){return a},qk:function(){return b},nk:function(){return c},mk:function(){return a.firstChild}}))}var d={},l=l=this,A=l.document;d.Gc=864E5;d.P=!!A.attachEvent;var fa=Object.prototype.hasOwnProperty,U=[],ga=!1,Y,U=[],ga=!1;d.q=function(a){return null!==a&&void 0!==a};d.mi=function(a){for(var b=a.length-1;0<=b;b--)for(var c=b-1;0<=c;c--)a[c]==a[b]&&a.splice(b,1);return a};d.vi=function(a){for(var b=a.parentNode.childNodes,
c,d=count=0;(c=b.item(d++))&&c!=a;)1==c.nodeType&&count++;return count};d.ia=function(a){return"[object Array]"==Object.prototype.toString.call(a)};d.Na=function(a){if(a){if(a.length)for(var b=a.length-1;0<=b;b--)a[b]=null;for(var c in a)if(b=typeof a[c],"function"==b||"object"==b)a[c]=null}};d.Da=function(a){return"function"==typeof a};d.Mi=function(a){return"object"==typeof a};d.trim=function(a){return a.toString().replace(/\s+/g," ").replace(/^\s+|\s+$/g,"")};d.ok=function(a){var b=a.getAttribute?
a.getAttribute("id"):a.id;b&&!d.vk(b)&&(b=a.attributes.id.value);return b};d.wi=function(a){return a.toString().replace(/([-.*+?^${}()|[\]\/\\])/g,"\\$1")};d.K=function(){var a=arguments,b=a[0]||{},c=1,f=a.length,e,k,p;"object"===typeof b||d.Da(b)||(b={});f===c&&(b=this,--c);for(;c<f;c++)if(null!=(e=a[c]))for(k in e)p=e[k],b!==p&&void 0!==p&&(b[k]=p);return b};d.ua=S();d.now=function(){return+new Date};d.shift=function(a){return a.splice(0,1)[0]};d.pf=function(a,b){for(var c in b)if(b[c]===a)return c;
return-1};d.Sc=function(){return A.location.protocol};d.$c=function(a,b){return-1!=d.pf(a,b)};d.Lb=function(a){return A.getElementById(a)};d.Td=function(a,b,c){var f=a.split(".");b=b[d.shift(f)];for(var e=c,k;null!=b&&0<f.length;)b=b[d.shift(f)];if(b){for(f=a.split(".");f.length&&(k=d.shift(f));)e=e[k]?e[k]:e[k]={};f=a.split(".");for(e=c;f.length&&(k=d.shift(f));)0<f.length?e=e[k]:e[k]=b}};d.ma=function(){return A.location.href};d.Mb=function(a){return encodeURIComponent(a)};d.Ba=function(a){return decodeURIComponent(a)};
d.Tc=function(){return A.referrer};d.le={};d.ed=function(a,b,c){c=c||d.ua;var f=A.createElement(b);(b="script"===b)||(f.rel="stylesheet");f.type=b?"text/javascript":"text/css";b&&(d.P?f.onreadystatechange=function(){"loaded"!=this.readyState&&"complete"!=this.readyState||c("ok")}:f.onload=function(){c("ok")},f.onerror=function(){c("error")});f[b?"src":"href"]=0==d.pf("//",a)?d.Sc()+a:a;b?d.Dd.appendChild(f):b||(d.le[f.href]?f=d.le[f.href]:(d.le[f.href]=f,d.Dd.appendChild(f)));if(!b){var e,k;"sheet"in
f?(e="sheet",k="cssRules"):(e="styleSheet",k="rules");var p=setInterval(function(){try{f[e]&&f[e][k].length&&(clearInterval(p),clearTimeout(g),c(!0,f))}catch(a){}finally{}},10),g=setTimeout(function(){clearInterval(p);clearTimeout(g);c(!1,f)},2500)}};d.Ua=function(a,b,c){c||(c=l);c=c.document;c=c.readyState;b=b||1;if(d.Da(a)&&(a=function(a,b){return function(){setTimeout(function(a){return function(){a.call(d.Sd);a=null}}(a),b);a=null}}(a,b),c&&("complete"==c||"loaded"==c))){ga=!0;for(U.push(a);a=
d.shift(U);)a&&a.call(d.Sd);return}if(!ga&&d.Da(a))U.push(a);else if(ga&&d.Da(a))a.call(d.Sd);else if(!d.Da(a))for(ga=!0;0<U.length;)(a=d.shift(U))&&a.call(d.Sd);a=c=c=c=null};A.addEventListener?Y=function(){-1<"complete,loaded".indexOf(A.readyState)&&(A.removeEventListener("readystatechange",Y,!1),d.Ua(null))}:d.P&&(Y=function(){-1<"complete,loaded".indexOf(A.readyState)&&(A.detachEvent("onreadystatechange",Y),d.Ua(null))});A.addEventListener?(A.addEventListener("readystatechange",Y,!1),A.addEventListener("DOMContentLoaded",
d.Ua,!1)):d.P&&A.attachEvent("onreadystatechange",Y);d.match=function(a){for(var b=[["urls",d.ma()],["referrers",d.Tc()],["userAgents",l.navigator.userAgent],["browsers",{name:x.W,version:x.Qd}]],c=0;c<b.length;c++)for(var f=b[c],e=a[f[0]]||[],k=0;k<e.length;k++){var p=e[k];if(!d.Mi(f[1])){if(d.Ba(f[1]).match(p))return!0}else if(d.Ba(f[1].name.toLowerCase()).match(p.name.toLowerCase())&&(!p.version||f[1].version==p.version))return!0}e=a.cookies||[];for(c=0;c<e.length;c++){var f=e[c],W;if((W=g.F.Ea(f.name))&&
W.match(f.value||"."))return!0}b=g.Jc("http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/fsr.ipo",g.Xc("http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/fsr.ipo"));if(a=a.variables)for(c=0,f=a.length;c<f;c++)if(e=a[c].name,W=a[c].value,e!=w.ipexclude||1!=b.get("value")){d.ia(e)||(e=[e],W=[W]);for(var r,k=!0,p=0,h=e.length;p<h;p++){try{if(r=(new Function("return "+e[p]))(),void 0===r||null===r)r=""}catch(m){r=""}var n;if(n=r||""===r){a:{n=r;var t=W[p];d.ia(t)||(t=[t]);for(var q=0,u=t.length;q<u;q++)if((n+"").match(t[q])){n=!0;break a}n=!1}n=!n}if(n){k=!1;break}}if(k)return!0}return!1};d.Dd=null;
d.Ua(function(){d.Dd=A.getElementsByTagName("head")[0]||A.documentElement});d.startTime=d.now();var w={},m=d.K({replay_id:"sitecom",site:{domain:"http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/site.com"},renderer:"W3C",layout:"",swf_files:"/"},Ca||{});d.Rb=function(){for(var a={},b=arguments,c=0,f=b.length;c<f;c++){var e=b[c];if(d.cd(e))for(var k in e){var p=e[k],g=a[k];a[k]=g&&d.cd(p)&&d.cd(g)?d.Rb(g,p):d.ze(p)}}return a};d.ze=function(a){var b;if(d.cd(a)){b={};for(var c in a)b[c]=d.ze(a[c])}else if(d.ia(a)){b=[];c=0;for(var f=a.length;c<f;c++)b[c]=
d.ze(a[c])}else b=a;return b};d.cd=function(a){if(!a||("[object Object]"!==Object.prototype.toString.call(a)||a.nodeType||a.setInterval)||a.constructor&&!fa.call(a,"constructor")&&!fa.call(a.constructor.prototype,"isPrototypeOf"))return!1;for(var b in a);return void 0===b||fa.call(a,b)||!fa.call(a,b)&&fa.call(Object.prototype,b)};d.N=function(){U=m=null;d=l=l.FSR=null};d.tk=function(a){var b=d.now(),c;do c=d.now();while(c-b<a)};if(d.q(l.FSRCONFIG)){var B=l.FSRCONFIG;B.surveydefs&&(d.surveydefs=B.surveydefs,
B.surveydefs=null);B.properties&&(d.properties=B.properties,B.properties=null)}l.FSR=d;l.FSR.opts=m;l.FSR.prop=w;d.Ga={};d.Ga.bh={};for(var v=d.Ga.bh,ra={},ha=["onload","onerror","onabort"],B=0;B<ha.length;B++)ra[ha[B]]=function(){this.gd(0==arguments.callee.If?1:0);this.nd=!1},ra[ha[B]].If=B;v.sa=function(a,b){this.options=d.K({},a);this.nd=!1;this.event=b;this.Be=0;return this};v.sa.prototype.gd=function(a,b){if(this.nd)switch(this.nd=!1,this.status=a,a){case 1:(this.options.onSuccess||d.ua)(b);
break;case 0:this.event?this.Uj():(this.options.onFailure||d.ua)(b);break;case -1:(this.options.onError||d.ua)(b)}};v.sa.prototype.Uj=function(){if(3>this.Be)this.cf();else this.onFailure()};v.sa.prototype.hf=function(a,b){this.nd=!0;var c=u.Fa(d.K(a,{uid:d.now()})),c=d.Sc()+"//"+this.options.host+this.options.path+this.options.url+"?"+c;b=d.K({},ra,b);for(var f=new Image,e=0;e<ha.length;e++){var k=ha[e];f[k]=function(){var a=arguments.callee;a.tc.onload=a.tc.onerror=a.tc.onabort=null;a.yi.call(a.self,
a.tc);a.tc=null};f[k].yi=b[k];f[k].tc=f;f[k].self=this}f.src=c};v.sa.prototype.send=function(a){this.Zj=a;this.cf()};v.sa.prototype.Sb=function(){var a=d.K(this.options.hd,{protocol:d.Sc()});this.hf(a,{onload:function(a){this.options.oa&&a.width!=this.options.oa?this.gd(0,a.width):this.gd(1,a.width)},onerror:function(){this.gd(-1)}})};v.sa.prototype.cf=function(){var a;this.Be++;a=d.K({event:this.event,ver:this.Be},this.Zj,a);this.hf(a)};d.Ga.Gd={};var u=d.Ga.Gd;u.qc=function(){for(var a=x.nf.replace(/[\s\\\/\.\(\);:]/gim,
""),b="",c=d.now()+"",f=0;f<a.length-1;f+=a.length/7)b+=Number(a.charCodeAt(Math.round(f))%16).toString(16);7<b.length&&(b=b.substr(b.length-7));return b+"-"+a.length+c.substr(c.length-6)+"-xxxx-xxxx-xxxxx".replace(/[xy]/g,function(a){var b=16*Math.random()|0;return("x"==a?b:b&3|8).toString(16)})};u.Tb=function(){return 0+100*Math.random()};u.Fa=function(a,b,c){var f="";if(a)for(var e in a)f+=(0!=f.length?"&":"")+(b?b+"["+e+"]":e)+"="+(c?a[e]:d.Mb(a[e]));return f};u.hash=function(a){a=a.split("_");
return 3*a[0]+1357+""+(9*a[1]+58)};u.Zf=function(a){a=a.replace(/[\[]/,"\\[").replace(/[\]]/,"\\]");a=RegExp("[\\?&+]"+a+"=([^&#]*)").exec(d.ma());return null==a?!1:a[1]};u.qb=function(a,b){return a[b]||a.files};u.mf=function(a){for(var b=/position *: *fixed/,c=0;c<document.styleSheets.length;c++)if(!document.styleSheets[c].href||-1==document.styleSheets[c].href.indexOf("Unknown_83_filename"/*tpa=http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/foresee-dhtml.css*/)){var d=document.styleSheets[c].cssRules?document.styleSheets[c].cssRules:document.styleSheets[c].rules;if(d)for(var e=
0;e<d.length;e++)d[e].cssText&&(d[e].cssText.match(b)&&d[e].selectorText)&&G(C(d[e].selectorText),a)}};d.Ga.$b={};var t=d.Ga.$b;t.za=function(a,b){var c,f,e;d.q(a.length)||(a=[a]);c=0;for(f=a.length;c<f;c++){e=a[c];var k=e.className||"";RegExp("\\b"+b+"\\b").test(k)||(e.className=(""==k?"":k+" ")+b)}};t.md=function(a,b){var c,f,e;d.q(a.length)||(a=[a]);c=0;for(f=a.length;c<f;c++)e=a[c],e.className&&(e.className=e.className.replace(RegExp("\\b"+b+"\\b"),""))};t.ji=function(a,b){if(a){d.q(a.length)||
(a=[a]);for(var c=0;c<a.length;c++)for(var f in b)f&&(-1=="zIndex".indexOf(f)&&("number"==typeof b[f]&&"opacity"!=f)&&(b[f]+="px"),a[c].style[f]=b[f])}return a};t.oc=function(a,b){if(a){d.q(a.length)||(a=[a]);for(var c=0;c<a.length;c++)for(var f in b)a[c].setAttribute(f,b[f])}return a};var G=t.ji;t.outerHTML=function(a){if(d.q(a.outerHTML))return a.outerHTML;var b={TEXTAREA:!0},c={HR:!0,BR:!0,IMG:!0,INPUT:!0},f=[],e="",k=a.nodeName;switch(a.nodeType){case 1:e=e+"<"+k.toLowerCase();if(b[k])switch(k){case "TEXTAREA":for(b=
0;b<a.attributes.length;b++)if("value"!=a.attributes[b].nodeName.toLowerCase())e+=" "+a.attributes[b].nodeName.toUpperCase()+'="'+a.attributes[b].nodeValue+'"';else var p=a.attributes[b].nodeValue;e+=">";e+=p;e+="</"+k+">"}else{for(b=a.attributes.length-1;0<=b;b--)p=a.attributes[b].nodeName.toLowerCase(),-1<"style,class,id".indexOf(p.toLowerCase())&&(e+=" "+p+'="'+a.attributes[b].nodeValue+'"');e+=">";c[k]||(e+=a.innerHTML,e+="</"+k.toLowerCase()+">")}break;case 3:e+=a.nodeValue;break;case 8:e+="\x3c!--"+
a.nodeValue+"--\x3e"}f.push(e);return f.join("")};d.Ga.k={};var h=d.Ga.k;h.Ef=function(a,b){for(var c=a.name,d=[a.site,a.section,b,g.n("q"),g.n("l")],e=0;e<d.length;e++)c+=d[e]?"-"+d[e]:"";return c};h.Oi=function(a,b){function c(b){"ok"===b&&d.surveydefs&&(d.K(w,d.properties),m.Xb=m.surveydefs=d.surveydefs,a())}var f=m.definition||"Unknown_83_filename"/*tpa=http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/foresee-surveydef.js*/;b?setTimeout(function(){c("ok")},100):d.ed(u.qb(m.site,"js_files")+f,"script",c)};h.log=function(a,b){if(w.events.enabled){var c=g.n(),f=c.get("sd");
d.q(f)||(f=c.get("cd"));var f=m.Xb[f],e=new Date;(new v.sa(h.ea.event,"logit")).send({cid:m.id,rid:c.get("rid")||"",cat:f.name,sec:f.section||"",type:c.get("q")||"",site:m.site.name||"",lang:c.get("l")||d.$S.locale||"",msg:a,param:b,tms:e.getTime(),tmz:6E4*e.getTimezoneOffset()})}};d.f={version:4.01};d.f.$a={Ne:"rpid",bc:"mid",ka:"rt",pa:"rc",Cd:"rcp",fc:"SESSION",ak:"DATA"};d.f.zb=function(){d.f.j?d.f.j.zb():(this.zd=!0,g.n(d.f.$a.ka,this.zd))};d.f.ha=function(a){d.f.j?d.f.j.ha(a):(this.cancel=!0,
g.n(d.f.$a.pa,this.cancel),a&&g.n(d.f.$a.Cd,this.cancel))};d.f.Vj=function(){g.n(d.f.$a.Cd)||(this.cancel=!1,g.n(d.f.$a.pa,this.cancel),this.zd=!0,g.n(d.f.$a.ka,this.zd))};d.f.Pf=function(){return!!g.n(d.f.$a.pa)};u.S={};u.S.Yb={};u.S.Bd=function(a,b,c,f){var e=u.S.Yb;if(a){e[b]||(e[b]=[]);e[b].push({Yd:a,Qc:c});if("unload"==b){if(d.q(d.Bc)){d.Bc.push(c);return}d.Bc=[]}"propertychange"!=b&&a.addEventListener?a.addEventListener(b,c,!f):a.attachEvent&&a.attachEvent("on"+b,c)}};u.S.Jg=function(a,b,c,
f,e){var k=u.S;if(e){if(a.getAttribute("_fsr"+b))return!1;a.setAttribute("_fsr"+b,"true")}else if(e=k.Yb[b])for(k=e.length-1;0<=k;k--){if(d.P)try{e[k].Yd.toString()}catch(p){e.splice(k,1);continue}if(e[k].Yd==a&&(f||e[k].Qc==c))return!1}u.S.Bd(a,b,c)};u.S.Kg=function(a,b,c){u.S.Bd(a,b,c,!0)};u.S.Pe=function(a,b,c){try{"propertychange"!=b&&a.removeEventListener?a.removeEventListener(b,c):a.detachEvent&&a.detachEvent("on"+b,c)}catch(d){}};var D=u.S.Bd,O=u.S.Kg,$=u.S.Pe,na=u.S.Jg;u.S.Ug=function(){for(var a=
d.Bc.length-1;0<=a;a--)try{d.Bc[a].call()}catch(b){}d.Na(d.Bc);u.S.fh();d.N()};D(l,"unload",u.S.Ug);u.S.fh=function(){if(d){var a=u.S,b;for(b in a.Yb){for(var c=a.Yb[b],f={};f=c.pop();)a.Pe(f.Yd,b,f.Qc),d.Na(f);delete a.Yb[b]}}};u.S.Fc=function(){this.Wb=[];this.ni=!1};u.S.Fc.prototype.Ma=function(a){this.Wb[this.Wb.length]={cj:!1,Qc:a}};u.S.Fc.prototype.Eg=function(){this.Wb=[]};u.S.Fc.prototype.U=function(){this.ni=!0;for(var a=0;a<this.Wb.length;a++){var b=this.Wb[a];b.Qc.apply(this,arguments);
b.cj&&(this.Wb.splice(a,1),a--)}};var E=u.S.Fc;d.Ga.ya={};var g=d.Ga.ya;g.sb=function(a){return a+(m.site.cookie?"."+m.site.cookie:"")};g.n=function(a,b){var c=g.sb("fsr.s"),c=g.Jc(c,g.Xc(c));return a?d.q(b)?c.set(a,b):c.get(a):c};g.Xc=function(a){var b;b="window"==m.storageOption?function(){var a=arguments.callee;return new g.fa(a.Wf,a.xf||{})}:function(){var a=arguments.callee;return new g.F(a.Wf,d.K({path:"/",domain:a.oe.site.domain,secure:a.oe.site.secure,encode:a.oe.encode},a.xf||{}))};b.Wf=
a;b.oe=m;b.xf=void 0;return b};var ya={};g.Jc=function(a,b){var c=ya[a];return null!=c?c:c=ya[a]=new b};t.ja={};t.ja.dc=function(a){var b=0,c=0,d=a.document,e=d.documentElement;"number"==typeof a.innerWidth?(b=a.innerWidth,c=a.innerHeight):e&&(e.clientWidth||e.clientHeight)?(b=e.clientWidth,c=e.clientHeight):d.body&&(d.body.clientWidth||d.body.clientHeight)&&(b=d.body.clientWidth,c=d.body.clientHeight);return{w:b,h:c}};t.ja.cc=function(a){var b=0,c=0,d=a.document,e=d.documentElement;"number"==typeof a.pageYOffset?
(c=a.pageYOffset,b=a.pageXOffset):d.body&&(d.body.scrollLeft||d.body.scrollTop)?(c=d.body.scrollTop,b=d.body.scrollLeft):e&&(e.scrollLeft||e.scrollTop)&&(c=e.scrollTop,b=e.scrollLeft);return{x:b,y:c}};t.ja.hh=function(a,b){a.scrollTo(0,b);window.document.body.scrollTop=b;window.document.body.scrollLeft=0};var za={Explorer:5.5,Safari:2,Firefox:1.4,Opera:1E3};u.De=function(a){function b(a){return-1<d.toLowerCase().indexOf(a.toLowerCase())}var c={L:"",W:"",version:0,Sf:!0,Qd:0},d=c.nf=a||l.navigator.userAgent;
/Opera[\/\s](\d+\.\d+)/.test(d)?c.W="Opera":/MSIE (\d+\.\d+)/.test(d)?c.W="IE":/Navigator[\/\s](\d+\.\d+)/.test(d)?c.W="Netscape":/Chrome[\/\s](\d+\.\d+)/.test(d)?c.W="Chrome":/Safari[\/\s](\d+\.\d+)/.test(d)?(c.W="Safari",/Version[\/\s](\d+\.\d+)/.test(d),c.version=new Number(RegExp.$1)):/Firefox[\/\s](\d+\.\d+)/.test(d)&&(c.W="Firefox");b("Windows")?c.L="Windows":b("OS X")?c.L="Mac":b("Linux")?c.L="Linux":b("Mac")&&(c.L="Mac");b("Android")?c.L="Android":b("iPod")?c.L="iPod":b("iPad")?c.L="iPad":
b("iPhone")?c.L="iPhone":(b("blackberry")||b("playbook")||b("BB10"))&&b("applewebkit")?c.L="Blackberry":b("Windows Phone")?c.L="Winphone":b("Kindle")?c.L="Kindle":b("Silk")?c.L="Kindle":b("BNTV250")?c.L="Nook":b("Nook")&&(c.L="Nook");""==c.L&&(c.L=void 0!=l.orientation?"Mobile":"Other");c.Sa=-1<"Android,iPod,iPad,iPhone,Blackberry,Winphone,Kindle,Mobile".indexOf(c.L);""==c.W?c.W="Unknown":c.bi&&0!=c.bi||(c.version=parseFloat(new Number(RegExp.$1)),c.Qd="IE"==c.W?6<c.version&&10>c.version?b("Trident")||
7!=c.version?b("Trident/5.0")&&9>=c.version?9:b("Trident/4.0")&&9>c.version?8:c.version:7:c.version:c.version);b("Android 2")&&(c.Sf=!1);return c};var x=u.De(),J={};d.stringify=function(a,b,c){var d;l.Prototype&&(d=Array.prototype.toJSON,delete Array.prototype.toJSON);if(l.JSON&&"function"===typeof l.JSON.stringify)a=l.JSON.stringify(a,b,c);else{var e;ea=L="";if("number"===typeof c)for(e=0;e<c;e+=1)ea+=" ";else"string"===typeof c&&(ea=c);if((Q=b)&&"function"!==typeof b&&("object"!==typeof b||"number"!==
typeof b.length))throw Error("_4c.stringify");a=ma("",{"":a})}l.Prototype&&(Array.prototype.toJSON=d);return a};var sa=/[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,qa=/[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,L,ea,Da={"\b":"\\b","\t":"\\t","\n":"\\n","\f":"\\f","\r":"\\r",'"':'\\"',"\\":"\\\\"},Q;l.JSON?J=l.JSON:function(){function a(a){return 10>a?"0"+a:a}
"function"!==typeof Date.prototype.toJSON&&(Date.prototype.toJSON=function(){return isFinite(this.valueOf())?this.getUTCFullYear()+"-"+a(this.getUTCMonth()+1)+"-"+a(this.getUTCDate())+"T"+a(this.getUTCHours())+":"+a(this.getUTCMinutes())+":"+a(this.getUTCSeconds())+"Z":null},Boolean.prototype.toJSON=function(){return this.valueOf()},Number.prototype.toJSON=function(){return this.valueOf()},String.prototype.toJSON=function(){return this.valueOf()});"function"!==typeof J.parse&&(J.parse=function(a,
c){function d(a,b){var e,g,h=a[b];if(h&&"object"===typeof h)for(e in h)Object.prototype.hasOwnProperty.call(h,e)&&(g=d(h,e),void 0!==g?h[e]=g:delete h[e]);return c.call(a,b,h)}var e;a=String(a);sa.lastIndex=0;sa.test(a)&&(a=a.replace(sa,function(a){return"\\u"+("0000"+a.charCodeAt(0).toString(16)).slice(-4)}));if(/^[\],:{}\s]*$/.test(a.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,"@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,"]").replace(/(?:^|:|,)(?:\s*\[)+/g,"")))return e=
(new Function("return "+a))(),"function"===typeof c?d({"":e},""):e;throw new SyntaxError("JSON.parse");})}();u.bk=J;t.Le={qh:{}};try{Array.prototype.slice.call(document.getElementsByTagName("html")),makeArray=function(a){return Array.prototype.slice.call(a)}}catch(Ha){}var C=t.Le.Ik=function(a,b,c){b=b||A;if(b.querySelectorAll&&!(d.P&&8>=x.version&&-1<a.indexOf("nth")))return wa(b.querySelectorAll(a));if(!c&&l.$&&!l.Prototype)return l.$(a,b);a=a.split(",");c=[];for(var f=a.length-1;0<=f;f--){var e=
a[f].replace(/^\s\s*/,"").replace(/\s\s*$/,"").replace(/\*=/g,"=").replace(/\>/g," > ").replace(/\s+/g," ");if(-1<e.indexOf(" ")){for(var e=e.split(" "),k=[b],p=!1,g=0;g<e.length;g++)if(">"==e[g])p=!0;else{for(var r=[],h=k.length-1;0<=h;h--)r=r.concat(va(e[g],k[h],p));k=r;p=!1}c=c.concat(d.mi(k))}else c=c.concat(va(e,b))}return c},Aa={width:"1",height:"1",id:"_"+(""+Math.random()).slice(9),allowfullscreen:!0,allowscriptaccess:"always",quality:"high",version:[3,0],bj:null,xi:null,Ce:!1,fi:!1};l.attachEvent&&
l.attachEvent("onunload",function(){__flash_unloadHandler=S();__flash_savedUnloadHandler=S()});var X=d.K(d.lk,{kk:Aa,Hi:function(){var a,b;try{b=navigator.plugins["Shockwave Flash"].description.slice(16)}catch(c){try{b=(a=new ActiveXObject("ShockwaveFlash.ShockwaveFlash.7"))&&a.GetVariable("$version")}catch(d){try{b=(a=new ActiveXObject("ShockwaveFlash.ShockwaveFlash.6"))&&a.GetVariable("$version")}catch(e){}}}return(b=/(\d+)[^\d]+(\d+)[^\d]*(\d*)/.exec(b))?[b[1],b[3]]:[0,0]},Rd:function(a){if(null===
a||void 0===a)return null;var b=typeof a;"object"==b&&a.push&&(b="array");switch(b){case "string":return a=a.replace(RegExp('(["\\\\])',"g"),"\\$1"),a=a.replace(/^\s?(\d+\.?\d*)%/,"$1pct"),'"'+a+'"';case "array":return"["+Ea(a,function(a){return X.Rd(a)}).join(",")+"]";case "function":return'"function()"';case "object":var b=[],c;for(c in a)a.hasOwnProperty(c)&&b.push('"'+c+'":'+X.Rd(a[c]));return"{"+b.join(",")+"}"}return String(a).replace(/\s/g," ").replace(/\'/g,'"')},Bi:function(a,b){a=d.K({},
a);var c='<object width="'+a.width+'" height="'+a.height+'" id="'+a.id+'" name="'+a.id+'"';a.fi&&(a.src+=(-1!=a.src.indexOf("?")?"&":"?")+Math.random());c=a.Ce||!d.P?c+(' data="'+a.src+'" type="application/x-shockwave-flash"'):c+' classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"';c+=">";if(a.Ce||d.P)c+='<param name="movie" value="'+a.src+'" />';a.width=a.height=a.id=a.Ce=a.src=null;a.bj=a.version=a.xi=null;for(var f in a)a[f]&&(c+='<param name="'+f+'" value="'+a[f]+'" />');f="";if(b){for(var e in b)if(b[e]){var k=
b[e];f+=e+"="+(/function|object/.test(typeof k)?X.Rd(k):k)+"&"}f=f.slice(0,-1);c+='<param name="flashvars" value=\''+f+"' />"}return c+"</object>"},isSupported:function(a){return T[0]>a[0]||T[0]==a[0]&&T[1]>=a[1]}}),T=d.Ge=X.Hi();d.Gf=null!=T&&0<T.length&&0<parseFloat(T[0]);d.Gf||(T=d.Ge=[0,0]);h.Ed={};h.Ed.gb=function(a,b){if(a){var c=g.n("m");if(c&&(c=(new Date).getTime()-c,c<1E3*b)){var d=function(){var a=h.ea.Wi;a.hd={rid:m.rid,cid:m.id};(new v.sa(a)).Sb()};d();var e=setInterval(d,1E3*a);setTimeout(function(){clearInterval(e)},
1E3*b-c)}}};h.ea={};h.ea.Qj={host:"http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/survey.foreseeresults.com",path:"/survey",url:"/display"};h.ea.Xi={host:"http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/i.4see.mobi",path:"/e",url:"/initialize"};h.ea.Wi={host:"http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/i.4see.mobi",path:"/e",url:"/recordHeartbeat"};h.ea.X={host:"http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/controller.4seeresults.com",path:"/fsrSurvey",url:"/OTCImg",oa:3};h.ea.event={host:"http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/events.foreseeresults.com",path:"/rec",url:"/process"};h.ea.domain={host:"http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/survey.foreseeresults.com",path:"/survey",url:"/FSRImg",oa:3};h.ea.Fj={host:"http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/replaycontroller.4seeresults.com",path:"/images",
enabled:!0};h.Za=function(a,b){this.options=a;this.Cb=b;this.jf=new E;this.Ud=new E;this.lg=new E;this.gg=new E;this.wc=this.bd=!1;-1<"iphone,ipad,ipod,android,winphone,blackberry,mobile".indexOf(x.L.toLowerCase())&&(this.bd=!0);-1<"winphone".indexOf(x.L.toLowerCase())&&(this.wc=!0);"IE"==x.W&&7>x.version&&(this.Ki=!0)};h.Za.prototype.show=function(a,b,c){this.Yf=b;this.Xh=c;if(!this.we){this.Vd=this.kf=!1;var f=this.Cb.invite,e=f.isMDOT,k=f.isZoomable||!1,p=u.qb(m.site,"image_files"),h=this.bd,r=
g.n("l"),R=this.Ri=F('<div class="fsrC"></div>');e&&t.za(R,"fsrM");var n=F('<div class="fsrFloatingContainer"></div>'),q=F('<div class="fsrFloatingMid"></div>'),v=F('<div class="fsrInvite"></div>'),Z=F('<div class="fsrLogos"></div>');f.siteLogo&&(f=f.siteLogo,"object"===typeof f&&(f=f.hasOwnProperty(r)?f[r]:f["default"]),f=F('<img src="'+p+f+'" class="fsrSiteLogo">'),Z.appendChild(f));f=F('<img src="'+p+'fsrlogo.gif" class="fsrCorpLogo">');Z.appendChild(f);for(var f=F('<div class="fsrDialogs"></div>'),
xa=[],z=0,B="",D=0;D<a.length;D++){var H=a[D],y=!1;c&&(H.locale&&c!=H.locale)&&(y=!0);if(!y){(y=H.locales)&&y[r]&&(H=d.K(H,y[r]),d.q(H.locale)||(H.locale=r));if(H.skipThisInvite){a.splice(D--,1);continue}var E=H.closeInviteButtonText;E&&(0<B.length&&(B+=" / "),B+=E);e&&17<H.acceptButton.length&&(H.acceptButton=H.acceptButton.substr(0,15)+"...");y=F('<div class="fsrDialog '+(1<a.length?" fsrMultiDialog":"")+'"><h1>'+H.headline+"</h1></div>");y.appendChild(F('<p class="fsrBlurb">'+H.blurb+"</p>"));
var L;H.noticeAboutSurvey&&(L=F('<p class="fsrSubBlurb">'+H.noticeAboutSurvey+"</p>"),y.appendChild(L));H.attribution&&y.appendChild(F('<p class="fsrAttribution">'+H.attribution+"</p>"));if(E=H.mobileExitDialog){var I=F('<div class="mobileExit"></div>');I.appendChild(F('<input type="hidden" id="mobileOnExitSupport" value="'+E.support+'"/>'));I.appendChild(F('<div class="mobileExitErrorFieldRequired mobileExitError hideField">'+E.fieldRequiredErrorText+"</div>"));I.appendChild(F('<div class="mobileExitErrorInvalidFormat mobileExitError hideField">'+
E.invalidFormatErrorText+"</div>"));var N=F('<input type="email" class="fsrEmailOrNumber" id="mobileOnExitInput" placeholder="'+E.inputMessage+'"/>');O(N,"keyup",function(a,b,c,d){return function(){a.Vi(this.value,b,c,d)}}(this,H.acceptButton,E.emailMeButtonText,E.textMeButtonText));if(this.bd){"android"==x.L.toLowerCase()&&u.mf({position:"static"});var P=function(a){return function(){var b=a.getBoundingClientRect(),c=t.ja.cc(l),d=t.ja.dc(l);b.top>c.y+d.h&&t.ja.hh(l,b.top+c.y-(d.h-b.height)/2)}}(N);
O(N,"focus",function(a,b){return function(){$(l,"scroll",a.Ub);$(l,"resize",a.Ta);setTimeout(P,500);G(b,{overflow:"visible"})}}(this,R));O(N,"blur",function(a,b){return function(){l.scrollTo(0,1);k&&"android"==x.L.toLowerCase()||a.wc||(a.Ub(),O(l,"scroll",a.Ub));a.Ta();O(l,"resize",a.Ta);G(b,{overflow:"hidden"})}}(this,R));this.wc||O(R,"touchmove",function(a){a.preventDefault()})}I.appendChild(N);y.appendChild(I);I=N=null}if(I=H.quizContent){N=F('<div class="fsrQuiz"></div>');N.appendChild(F('<p class="fsrQuizQuestion">'+
I.question+"</p>"));for(var M=0;M<I.answers.length;M++){var J=I.answers[M],K=F('<p class="fsrAnswer" id="fsrAns'+D+"_"+M+'"><input name="fsrQuiz'+D+'" type="radio" id="fsrA'+D+"_"+M+'"><label for="fsrA'+D+"_"+M+'">'+J.answer+"</label></p>");N.appendChild(K);J.proceedWithSurvey?O(K,"click",function(a){return function(){var b=this.parentNode.parentNode;G(C(".fsrQuiz",b),{display:"none"});G(C(".fsrSubBlurb",b),{display:"block"});G(C(".fsrB",b),{display:"block"});a.Ta.call(a)}}(this)):O(K,"click",function(a,
b,c){return function(){var d=this.parentNode.parentNode.parentNode;d.innerHTML='<div class="fsrDialog" style="margin-left: 0px;"><h1>'+b.cancelTitle+'</h1><p class="fsrBlurb">'+b.cancelText+'</p><div class="fsrB" style="display: block;"><a class="declineButton">'+c+"</a></div></div>";la(F(".declineButton"),"click",function(a){return function(){a.ob()}}(a));a.Lj.call(a);a.Ta.call(a);d=null}}(this,J,H.closeInviteButtonText))}y.appendChild(N);J=K=N=null}I=null;N=H.locale;M=F('<div class="fsrB"></div>');
++z;J=F('<div class="declineButtonContainer"><a href="javascript:void(0)" class="declineButton'+(d.P?" ie":"")+'" tabindex="'+z+'">'+H.declineButton+"</a></div>");++z;K=F('<div class="acceptButtonContainer"><a href="javascript:void(0)" class="acceptButton'+(d.P?" ie":"")+'"  tabindex="'+z+'">'+H.acceptButton+"</a></div>");H.reverseButtons?(M.appendChild(G(K,{"float":"left"})),M.appendChild(G(J,{"float":"right"}))):(M.appendChild(J),M.appendChild(K));la(C(".declineButton",M),"click",function(a,b){return function(){a.ob(b)}}(this,
N));"Chrome"==x.W&&la(C(".acceptButton",M),"mousedown",function(a,b){return function(){a.nj(b)}}(this,N));la(C(".acceptButton",M),"click",function(a,b){return function(){g.n("l",b);a.Kb(b)}}(this,N));I&&(G(L,{display:"none"}),G(M,{display:"none"}));y.appendChild(M);xa.push(y);N=K=J=M=null}H=null}a=F('<div class="fsrFooter"><a href="http://privacy-policy.truste.com/click-with-confidence/ctv/en/www.foreseeresults.com/seal_m" title="Validate TRUSTe privacy certification" target="_blank"><img src="'+
p+'truste.png" class="fsrTruste"></a></div>');v.appendChild(Z);v.appendChild(f);v.appendChild(F('<div class="fsrCTermination"></div>'));v.appendChild(a);v.appendChild(F('<div class="fsrCTermination"></div>'));q.appendChild(v);n.appendChild(q);R.appendChild(n);e||w.invite.hideCloseButton||(q=F('<a href="#" class="fsrCloseBtn" title="'+B+'"><div></div></a>'),v.appendChild(q),O(q,"click",function(a){return function(b){a.ob();b&&b.preventDefault?b.preventDefault():l.event&&l.event.returnValue?l.eventReturnValue=
!1:b.returnValue=!1}}(this)),q=null);v=l.document.body;0==v.children.length?v.appendChild(R):v.insertBefore(R,v.firstChild);if(this.bd||d.P&&(7>=x.version||"CSS1Compat"!=l.document.compatMode))q=e?"fsrM":"",this.Ki&&(q+=" fsrActualIE6"),R.className="fsrC ie6 "+q,this.Ub=function(a,b,c){return function(){var d=t.ja.cc(l);b.style.left=d.x+"px";b.style.top=d.y+"px";0>=d.y&&(c&&"blackberry"!=x.L.toLowerCase())&&l.scrollTo(0,1);Q.call(a)}}(this,R,h),k&&"android"==x.L.toLowerCase()||this.wc||O(l,"scroll",
this.Ub);var Q=this.Ta=function(a,b,c,d,f){return function(){var e=t.ja.dc(l);if(f){var k=document.body,p=document.documentElement,k=Math.max(k.scrollHeight,k.offsetHeight,p.clientHeight,p.scrollHeight,p.offsetHeight);G(a,{width:e.w+"px",height:k+"px"});G(b,{position:"relative",left:(b.parentNode.offsetWidth-b.offsetWidth)/2+"px",top:"10px"})}else d?(k=e.h,G(a,{width:e.w+"px",height:c.offsetHeight-t.ja.cc(l).y+"px"}),G(b,{position:"relative",left:(b.parentNode.offsetWidth-b.offsetWidth)/2+"px",top:(k-
b.offsetHeight)/2+"px"})):(G(a,{width:e.w+"px",height:e.h+"px"}),G(b,{position:"relative",left:(b.parentNode.offsetWidth-b.offsetWidth)/2+"px",top:(b.parentNode.offsetHeight-b.offsetHeight)/2+"px"}))}}(R,n,v,E,this.wc);this.Ta.call(this);O(l,"resize",this.Ta);var U=this.Lj=function(a,b,c){return function(){G(a,{width:b.offsetWidth+(a.offsetWidth-c.offsetWidth)+"px"})}}(n,f,Z),T=this.Ek=function(a,b){return function(){var c=a.offsetHeight,d=a.parentNode.offsetHeight;b&&(d=t.ja.dc(l).h);c=c>d?"rotateX(0deg) rotateZ(0deg) scale("+
d/c+")":"rotateX(0deg) rotateZ(0deg) scale(1.0)";d=a.style;d.WebkitTransform=c;d.MozTransform=c;d.msTransform=c;d.transform=c}}(n,E);G(n,{visibility:"hidden"});setTimeout(function(a,b,c,d,f,e,k){return function(){for(var p=0;p<b.length;p++)G(b[p],{marginLeft:(0<p?15:0)+"px"}),c.appendChild(b[p]);U.call(a);Q.call(a);f&&l.scrollTo(0,1);var p=d.offsetHeight,g=d.parentNode.offsetHeight;e&&(g=t.ja.dc(l).h);p>g?(t.za(d,"fsrBulgeInstant"),p="rotateX(0deg) rotateZ(0deg) scale("+g/p+")",g=d.style,g.WebkitTransform=
p,g.MozTransform=p,g.transform=p):0<k?t.za(d,"fsrBulgeInstant"):t.za(d,"fsrBulge");setTimeout(function(){Q.call(a);G(d,{visibility:"visible"});d.className+=" fsrCasueReflow";C(".fsrLogos")[0].focus()},1)}}(this,xa,f,n,h,E,b),1);this.Oc=function(a,b,c,f,e,k,p){return function(){k&&(p&&d.q(l.orientation))&&(0==l.orientation||180==l.orientation?t.md(b,"fsrLandscape"):t.za(b,"fsrLandscape"),l.scrollTo(0,1),setTimeout(function(){G(f,{width:c.offsetWidth+(f.offsetWidth-e.offsetWidth)+"px"});Q.call(a);T.call(a)},
1),setTimeout(function(){T.call(a)},500))}}(this,R,f,n,Z,e,h);this.Oc.call(this);O(l,"orientationchange",this.Oc);this.he=function(a){return function(b){27==(b.keyCode?b.keyCode:b.which)&&a.ob()}}(this);O(A,"keyup",this.he);this.we=!0;E=v=a=L=y=f=Z=v=q=n=R=f=null}};h.Za.prototype.Vi=function(a,b,c,d){var e=!1;a&&(a.match(/^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,6})+$/)?(e=!0,C(".acceptButton")[0].innerHTML=c):a.replace(/[^\d.]/g,"").match(/(^\d{10}$)/)&&(e=!0,C(".acceptButton")[0].innerHTML=
d));e||(C(".acceptButton")[0].innerHTML=b)};h.Za.prototype.sc=function(){this.we&&(this.Ta&&$(l,"resize",this.Ta),this.Ub&&$(l,"scroll",this.Ub),this.Oc&&$(l,"orientationchange",this.Oc),this.he&&$(A,"keyup",this.he),G(this.Ri,{display:"none"}),this.we=!1,"android"==x.L.toLowerCase()&&u.mf({position:"fixed"}))};h.Za.prototype.Kb=function(a){this.jf.U(a,this.Yf)};h.Za.prototype.nj=function(a){this.gg.U(a,this.Yf)};h.Za.prototype.ob=function(a){this.Ud.U(a)};h.Za.prototype.ld=function(a){this.lg.U(a)};
d.f.Fe={};var n=d.f.Fe;d.f.u=function(a){this.dd=a.tb()==a;this.j=a;this.ne=new d.f.u.ca(a);this.Nh();this.Kj=new ia(this.j,this.j.B,"resize",700,function(a,b,c){b=t.ja.dc(c.B);a={w:0,h:0};var d=0;x.Sa&&(b=A.documentElement,b={w:b.clientWidth,h:b.clientHeight},d=l.screen,d.availWidth&&(a={w:d.availWidth,h:d.availHeight}),d=l.orientation);c.D().log(c,n.A.G.Tg,{sz:b,avs:a,st:d},null,-350)});var b=this.j.B.onerror;this.j.B.onerror=function(a){return function(c,k,p){d&&(a.D().log(a,n.A.G.Xg,{v:c+", "+
k+", "+p}),b&&b.apply(this,arguments))}}(this.j);this.de=[];this.Gg(this.j.B.document.body,!0);this.ee=function(a,b,c){return function(){setTimeout(function(){for(var a=0;a<c.length;a++){var b=c[a];if(!d)break;c[a].Ff()&&(d.q(b.input)&&"SELECT"!=b.input.nodeName)&&(b.vb=b.input.value,b.yb())}},100);a.dd||a.j.tb().rb.ee.apply()}}(this,this.j,this.de);D(this.j.B.document,"click",this.ee);if(d.q(d.f.R.ng)){a=C(d.f.R.ng);for(var c=0;c<a.length;c++)a[c].vh||(a[c].vh=new ia(this.j,a[c],"scroll",400,function(a){return function(b,
c){c||(c=b.target||b.srcElement);c&&a.D().log(a,n.A.G.Me,{x:a.D().Y(a,t.J.T(c)),ps:{x:c.scrollLeft,y:c.scrollTop}})}}(this.j)))}a=null};d.f.u.prototype.Nh=function(){function a(a,b,c,f){var e=n.A.G;a.delayed||c.tb().ab||(c.D().log(c,e.Hd,{st:1}),c.tb().ab=!0);if(c.tb().ab){f=f||e.$g;var g={x:"unknown"!=typeof a.clientX?a.clientX:"unknown"!=typeof a.screenX?a.screenX:a.sX,y:"unknown"!=typeof a.clientY?a.clientY:"unknown"!=typeof a.screenY?a.screenY:a.sY};b=-1;(f==e.Je||f==e.Zg)&&(a=a.explicitOriginalTarget||
a.originalTarget||a.target||a.srcElement)&&(b=c.D().Y(c,t.J.T(a)));a=t.ja.cc(this.j.B);"undefined"==typeof g.x&&(g=c.je);d.q(g)&&(c.je=g,g={x:a.x+g.x,y:a.y+g.y},c.B!=c.B.top&&(e=c.B.frameElement,e=q.Gi(e,q.Ei(e)))&&(g.x+=e.x-a.x,g.y+=e.y-a.y),c.D().log(c,f,{ps:g,x:b}))}}var b=400;if(x.Sa){if(a=null,b=100,this.dd){this.Pb={alpha:0,beta:0,gamma:0};D(l,"deviceorientation",function(a){return function(b){if(d.q(b.alpha)){var c=Math.round(b.alpha),f=Math.round(b.beta);b=Math.round(b.gamma);if(a.Pb.alpha!=
c||a.Pb.beta!=f||a.Pb.gamma!=b){a.Pb.alpha=c;a.Pb.beta=f;a.Pb.gamma=b;var e=a.j;e.D().log(e,n.A.G.dh,{ot:{a:c,b:f,g:b}})}}}}(this));var c=function(a,b,c){b=[];if(d.q(a.touches))for(var f=0;f<a.touches.length;f++){var e=a.touches[f];b.push({x:e.pageX,y:e.pageY})}c.D().log(c,n.A.G.ih,{ts:b})};this.Tj=new ia(this.j,this.j.B,"touchstart",b,c);for(var f=["end","cancel","leave","move"],e=0;e<f.length;e++)this.Tj.Rb(this.j,this.j.B,"touch"+f[e],c);D(l,"touchstart",function(a){return function(){a.ac.U()}}(this.j))}}else c=
d.P?this.j.B.document:this.j.B,this.Yi=this.dd?new ia(this.j,c,"mousemove",250,a):this.j.tb().rb.Yi.Rb(this.j,c,"mousemove",a),D(this.j.B.document,"mousedown",function(a,b,c){return function(d){c.call(a,d,null,b,n.A.G.Je)}}(this,this.j,a));this.Fk=new ia(this.j,this.j.B,"scroll",b,function(b){return function(c,d,f){f.D().log(f,n.A.G.Sg,{ps:t.ja.cc(f.B)});f.B==f.B.top&&(x.Sa?(c=A.documentElement,d=l.screen,f.D().log(f,n.A.G.kh,{z:{cw:c.clientWidth,ch:c.clientHeight,w:l.innerWidth,h:l.innerHeight,sch:d.height,
scw:d.width,saw:d.availWidth,sah:d.availHeight}})):a&&b.call(this,c,d,f))}}(a));this.dd&&(x.Sa||(this.j.ab=!0,D(this.j.B.document,"mouseover",function(a,b){return function(a){b.ab||(a.relatedTarget||a.fromElement)||(b.D().log(b,n.A.G.Hd,{st:1}),b.ab=!0)}}(this,this.j)),D(this.j.B.document,"mouseout",function(a,b){return function(a){a=a?a:b.B.event;a=a.relatedTarget||a.toElement;var c;try{a&&(c=a.nodeName)}catch(f){}b.ab&&((!a||c&&"HTML"==c)&&d)&&(b.D().log(b,n.A.G.Hd,{st:0}),b.ab=!1,b.ac.U())}}(this,
this.j))),b=function(a){return function(){var b=q.Ai(a.oi);a.Wd&&a.Wd.width==b.width&&a.Wd.height==b.height||(a.Wd=b,a.D().log(a,n.A.G.Pg,{sz:{w:b.width,h:b.height}}))}}(this.j),b.apply(this,[]),this.j.hb.Ma(b),b=null);a=null};d.f.u.Yg=500;d.f.u.prototype.Gg=function(a,b){for(var c=+new Date,f=C("input, select, textarea",a,!0),e=this.de,k=f.length-1;0<=k;k--){if(+new Date-c>d.f.u.Yg){this.j.Qb.log(this.j,n.A.G.Vg,{});break}var g=f[k];if(g._fsrTracker)for(var h=e.length-1;0<=h;h--){var r=e[h];if(r.input==
g&&r.Ff()){r.yb();break}}else e[e.length]=new d.f.u.Oa(this.j,g),"reset"==g.getAttribute("type")&&D(g,"click",function(a){return function(){setTimeout(function(a){return function(){for(var b=0;b<a.length;b++)a[b].yb()}}(a),1)}}(e));g=null}!b&&d.P&&this.ne.ve(a);a=e=f=null};d.f.u.prototype.N=function(){this.ne.N();this.Kj=this.ee=this.de=this.j=this.ne=null;for(var a=d.f.u.ib.ef,b=0;b<a.length;b++)a[b].N(),a.splice(b--,1);a=d.f.u.We;for(b=0;b<a.length;b++)a[b].N(),a.splice(b--,1)};g.F=function(a,b){a||
(a="STORAGE");this.Ca=a.replace(/[- ]/g,"");g.F.M||g.F.Ra();this.wb=b||{};this.data={};this.gc=new E;this.xd=0;this.zc=4E3;this.la=!0};g.F.prototype.set=function(a,b){this.mb();this.M[a]=b;this.Aa()};g.F.prototype.reset=function(a){this.M=a;this.Aa()};g.F.prototype.get=function(a){this.mb();return a?this.M[a]:this.M};g.F.prototype.N=function(a){this.mb();delete this.M[a];this.Aa()};g.F.prototype.ub=function(){this.xd=0;this.M={};var a=this.wb.duration;this.wb.duration=-1;this.Aa();a?this.wb.duration=
a:delete this.wb.duration};g.F.prototype.mb=function(){this.M={};try{var a=g.F.Ea(this.Ca);a&&0<a.length&&(this.M=J.parse(a),d.q(this.M)?(this.xd=this.Ca.length+a.length+2,this.la=!1):this.M={})}catch(b){this.M={}}};g.F.prototype.Aa=function(){var a=d.stringify(this.M);this.Ca.length+d.Mb(a).length>this.zc&&this.gc.U(this);this.xd=g.F.write(this.Ca,a,this.wb)};g.F.Ea=function(a){return(a=l.document.cookie.match("(?:^|;)\\s*"+d.wi(a)+"=([^;]*)"))?d.Ba(a[1]):null};g.F.write=function(a,b,c){b=c&&d.q(c.encode)&&
!c.encode?b:d.Mb(b);a=d.Mb(a);for(var f in c)if(c[f]){var e=c[f];b+=";"+("duration"===f?"expires":f);switch(f){case "duration":b+="="+(new Date(d.now()+e*d.Gc)).toGMTString();default:b+="="+e}}l.document.cookie=a+"="+b;return a.length+b.length+2};g.F.ub=function(a,b){g.F.write(a,"",d.K(b,{duration:-1}))};g.F.Ra=function(a){a&&a.apply(g.F)};g.F.isSupported=da(!0);h.Wa={};d.rc=function(a,b){a||(a=d.now());A.cookie="fsr.a"+(m.site.cookie?"."+m.site.cookie:"")+"="+a+";path=/"+(m.site.domain?";domain="+
m.site.domain:"")+(b?";expires="+(new Date(d.now()+-1*d.Gc)).toGMTString()+";":";")+(m.site.secure?"secure":"")};d.gb=function(){h.Wa.timer||(d.rc(),h.Wa.timer=setInterval(d.rc,750))};d.wd=function(){h.Wa.timer&&(clearInterval(h.Wa.timer),delete h.Wa.timer,d.rc("stopped",!0))};d.fj=function(){h.Wa.timer&&(clearInterval(h.Wa.timer),delete h.Wa.timer,d.rc("paused"))};for(var aa=$$FSR.sites,B=0,Ga=aa.length;B<Ga;B++){var y;d.ia(aa[B].path)||(aa[B].path=[aa[B].path]);for(var ba=0,ca=aa[B].path.length;ba<
ca;ba++)if(y=d.ma().match(aa[B].path[ba])){m.siteid=B;m.site=$$FSR.sites[B];m.site.domain?"default"==m.site.domain&&(m.site.domain=null):m.site.domain=y[0];m.site.secure||(m.site.secure=null);m.site.name||(m.site.name=y[0]);ba="files js_files image_files html_files css_files swf_files".split(" ");for(B=0;B<ba.length;B++)ca=ba[B],m.site[ca]||$$FSR[ca]&&(m.site[ca]=$$FSR[ca]);break}if(y)break}d.gb();h.xa={};h.xa.set=function(a,b,c,d){c=ka(d);c[1][a]=b;c[0].set("cp",c[1])};h.xa.get=function(a,b){return ka(b)[0][a]};
h.xa.vf=function(a,b){var c=ka(b);delete c[1][a];c[0].set("cp",c[1])};h.xa.append=function(a,b,c,d){d=ka(d);d[1][a]=d[1][a]?d[1][a]+","+b:b;c&&(b=d[1][a].split(","),c=b.length>c?b.length-c:0,d[1][a]=b.splice(c,b.length-1-c+1).join());d[0].set("cp",d[1])};h.xa.Fa=function(a){a=a||g.n();var b=a.get("sd");d.q(b)||(b=a.get("cd"));b=m.Xb[b];a={browser:x.W+" "+x.version,os:x.L.match(/ipod|ipad|iphone/i)?"iOS":x.L,pv:a.get("pv"),url:K(a,"c"),entry:K(a,"ep"),ref_url:K(a,"ru"),locale:K(a,"l"),site:K(m.site.name),
section:K(b.section),referrer:K(a,"r"),terms:K(a,"st"),sessionid:K(a,"rid"),replay_id:K(a,"mid"),flash:d.Ge.join(".")};x.L.match(/android|ipod|ipad|iphone|blackberry|firefox/i)&&(a.screen=screen.width+"x"+screen.height);w.meta.user_agent&&(a.user_agent=x.nf);if(w.analytics.google_local||w.analytics.google){var c=g.F.Ea("__utma"),b=g.F.Ea("__utmz");c&&""!=c&&(c=c.split("."),a.first=c[2],a.last=c[3],a.current=c[4],a.visits=c[5]);if(b&&""!=b){var f,c=[];f=["utmgclid","utmcsr","utmccn","utmcmd","utmctr"];
for(var e=0;e<f.length;e++)c.push(RegExp(f[e]+"=([^\\|]*)"));if(b.match(c[0]))a.source="Google",a.campaign="Google Adwords",a.medium="cpc";else{if(f=b.match(c[1]))a.source=f[1];if(f=b.match(c[2]))a.campaign=f[1];if(f=b.match(c[3]))a.medium=f[1]}if(f=b.match(c[4]))a.keyword=f[1]}}b=g.n("cp");c=g.n("meta");a=d.K({},b||{},a||{},c||{});return u.Fa(a,"cpp")};y=h.xa;l.FSR.CPPS=y;y.set=y.set;y.get=y.get;y.erase=y.vf;y.append=y.append;B={};l.ForeSee=B;B.CPPS=y;y.fsr$set=y.set;y.fsr$get=y.get;y.fsr$erase=
y.vf;y.fsr$append=y.append;h.Z={};h.Z.vc=function(){var a,b=w.analytics.google_remote;if(b){var c=m.site.domain;b[c]&&(a=b[c])}return a};h.Z.Fa=function(a){var b={},c=h.Z.vc();c&&(b.domain="."+m.site.domain,b.id=c.id,b.name=c.name,b.event=a);return u.Fa(b,"ga")};h.Z.Cf=function(a){var b,c=h.Z.vc();c&&(b=c.events[a]);return b};h.Z.fireEvent=function(a){var b=h.Z.vc();b&&(l._gaq=l._gaq||[],(a=h.Z.Cf(a))&&l._gaq.push(["_trackEvent","foresee survey",a,b.name]))};h.Z.Di=function(a){var b=a;h.Z.vc()&&l._gat&&
(b=l._gat._getTrackerByName()._getLinkerUrl(a));return b};h.Z.uc=function(){var a=h.Z.vc();if(a){l._gaq=l._gaq||[];l._gaq.push(["_setAccount",a.id]);l._gaq.push(["_setDomainName","."+m.site.domain]);l._gaq.push(["_trackPageview"]);a=document.createElement("script");a.type="text/javascript";a.async=!0;a.src=("https:"==document.location.protocol?"https://ssl":"http://www/")+".google-analytics.com/ga.js";var b=document.getElementsByTagName("script")[0];b.parentNode.insertBefore(a,b)}};h.C={};h.C.V={dk:void 0,
jb:1,Ya:0,Ke:-1,Fd:-2};h.C.uc=function(){d.q(this.Kd)||(h.C.yh(),!d.q(this.Kd)&&(this.oh()&&this.Fh()&&this.lh()&&this.Eh()&&this.Kh()&&this.Ph()&&this.Lh())&&this.na(h.C.V.jb,"Continue: In the pool"))};h.C.yh=function(){var a=g.n("v");d.q(a)&&(this.va=a,this.Kd=0<this.va?!0:!1)};h.C.na=function(a,b){this.va=a;this.Bk=b;this.Kd=1>a?!1:!0;g.n("v",this.va);if(d.q(d.f)){var c=m.replay_id+"_pool",f=new g.fa(c);f.set("v",this.va);f.Aa();g.aa.isSupported()&&(c=new g.aa(c,!1),c.set("v",this.va),c.Aa())}};
h.C.Kh=function(){var a=m.site;return(a=(new g.F(g.sb("fsr.r"),{path:"/",domain:a.domain,secure:a.secure,encode:m.encode})).get("d"))?(this.na(h.C.V.Ke,"Exit: Persistent cookie found: "+a),!1):!0};h.C.Eh=function(){return g.F.Ea("fsr.o")?(this.na(h.C.V.Ya,"Exit: Opt-out cookie found"),!1):!0};h.C.oh=function(){return g.F.Ea(g.sb("fsr.a"))?!0:(this.na(h.C.V.Ya,"Exit: Cookies not supported"),!1)};h.C.lh=function(){return za[x.W]&&u.De.version<=za[x.W]?(this.na(h.C.V.Ya,"Exit: Browser not supported"),
!1):!0};h.C.Fh=function(){return d.k.R.pe[x.L.toLowerCase()]?!0:(this.na(h.C.V.Ya,"Exit: Platform not supported"),!1)};h.C.Lh=function(){var a=u.Tb();return 0<a&&a<=this.Gh()?!0:(this.na(h.C.V.Fd,"Exit: Not in global sample: "+a),!1)};h.C.Ph=function(){if(!d.q(d.f))return!0;var a=m.replay_id+"_pool",b=(new g.fa(a)).get("v");return d.q(b)||g.aa.isSupported()&&(b=(new g.aa(a,!1)).get("v"),d.q(b))?(this.na(b,"Exit: Not in pool based on storage"),!1):!0};h.C.Gh=function(){var a=(new Date).getHours(),
b=100;if(d.q(m.pools))for(var c=m.pools,f=0,e=c.length;f<e;f++){var k;"[object Array]"!==Object.prototype.toString.call(c[f].path)&&(c[f].path=[c[f].path]);for(var p=0,h=c[f].path.length;p<h;p++)if(k=d.ma().match(c[f].path[p])){b=c[f].sp;break}if(k)break}b=(p=g.Jc("http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/fsr.pool",g.Xc("http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/fsr.pool")))&&1==p.get("value")?100:b;d.ia(b)||(b=[{h:0,p:b}]);c=100;p=0;for(f=b.length;p<f;p++)a>=b[p].h&&(c=b[p].p);return c};var I;h.Fb=function(a,b){this.wb=a;this.Cb=b};h.Fb.prototype.Ji=function(){var a=this.zi();
I=new h.Za(this.wb,this.Cb);this.Cb.invite.timeout&&(this.Sj=setTimeout(function(a){return function(){a.Ud.U()}}(I),1E3*this.Cb.invite.timeout));I.jf.Ma(function(a,c,d){return function(e,k){c.kf=!0;a.vj(c)||c.sc();d[k+1]?(h.log("104",k+2),clearTimeout(a.Sj),setTimeout(function(){c.show(d[k+1],k+1,e)},500)):c.Vd||c.options.Ac.accepted(e)}}(this,I,a));I.gg.Ma(function(a,c){return function(a){c.Vd||c.options.Ac.preAccept(a)}}(this,I,a));I.Ud.Ma(function(a){return function(c){a.Vd=!0;a.sc();a.kf||a.options.Ac.declined(c)}}(I));
I.lg.Ma(function(a){return function(c){a.sc();a.options.Ac.ld(c)}}(I));I.show(a[0],0)};h.Fb.prototype.vj=function(a){if(d.Lb("mobileOnExitInput")){this.Hf(".mobileExitErrorFieldRequired");this.Hf(".mobileExitErrorInvalidFormat");var b=this.Cb,c=d.trim(d.Lb("mobileOnExitInput").value),f=d.trim(d.Lb("mobileOnExitSupport").value),e=function(a,b){return function(c){c&&(1==c?(b.sc(),g.n("m",(new Date).getTime()),h.Ed.gb(w.mobileHeartbeat.delay,w.mobileHeartbeat.max),b.options.Ac.accepted(b.Xh)):2==c?a.qg(".mobileExitErrorFieldRequired"):
3==c&&a.qg(".mobileExitErrorInvalidFormat"))}}(this,a);a=function(a){return function(){a.sc()}}(a);var k=h.ea.Xi,p=new Date-0+"_"+Math.round(1E13*Math.random()),l=u.hash(p);k.hd={rid:m.rid,cid:m.id,sid:h.Ef(b,b.pop.later),notify:c,a:p,b:l,c:d.Gc,support:f,cpps:h.xa.Fa()};(new v.sa(d.K({onSuccess:e,onError:a},k))).Sb();b=null;return!0}return!1};h.Fb.prototype.Hf=function(a){t.md(C(a),"showField");t.za(C(a),"hideField")};h.Fb.prototype.qg=function(a){t.md(C(a),"hideField");t.za(C(a),"showField")};h.Fb.prototype.zi=
function(){var a=this.Cb.invite.dialogs;d.ia(a[0])||(a=Array(a));return a};d._qualified=function(a){I.ld(a)};d._accepted=function(a){I.Kb(a)};d._declined=function(a){I.ob(a)};v.Eb=function(a){this.options=d.K({method:"POST",url:d.ma(),data:{},contentType:"application/x-www-form-urlencoded",oa:d.ua,Ka:d.ua},a)};v.Eb.prototype.send=function(a){a=d.K(this.options,a);var b=!1;if(l.XMLHttpRequest)b=new l.XMLHttpRequest;else if(window.ActiveXObject)try{b=new ActiveXObject("Msxml2.XMLHTTP")}catch(c){try{b=
new ActiveXObject("Microsoft.XMLHTTP")}catch(f){b=!1}}b.open(a.method,a.url,!0);b.setRequestHeader("Accept",a.type);b.setRequestHeader("Content-Type",a.contentType);b.onreadystatechange=function(a,b){return function(){4==b.readyState&&200==b.status?a.oa&&a.oa.apply(a,[b.responseText]):4==b.readyState&&200!=b.status&&a.Ka&&a.Ka.apply(a)}}(a,b);b.send(d.q(a.yc)&&!0==a.yc?a.data:u.Fa(a.data,null,!1));a=b=null};v.Eb.prototype.N=function(){d.Na(this.options)};v.Eb.isSupported=da(!0);v.Eb.Ra=function(a){a.call(v.Eb)};
v.Xa=function(a){this.options=d.K({method:"POST",url:d.ma(),data:{},contentType:"application/x-www-form-urlencoded",oa:d.ua,Ka:d.ua},a)};v.Xa.prototype.send=function(a){a=d.K(this.options,a);var b=d.q(a.yc)&&!0==a.yc?a.data:u.Fa(a.data,null,!1),c=!1;l.XMLHttpRequest&&(c=new l.XMLHttpRequest,c.open(a.method,a.url,!0),c.setRequestHeader("Accept",a.type),c.setRequestHeader("Content-Type",a.contentType),c.onreadystatechange=function(a,b,c){return function(){4==b.readyState&&(200==b.status?a.oa&&a.oa.apply(a,
[b.responseText]):a.Ka&&a.Ka.apply(a,[b.responseText]),c.N())}}(a,c,this),c.send(b));b=a=c=null};v.Xa.prototype.N=function(){d.Na(this.options)};v.Xa.isSupported=function(){return d.P&&10>x.Qd?!1:!0};v.Xa.Ra=function(a){a.call(v.Xa)};v.Ha=function(a){this.options=d.K({url:"",data:"",zf:"",od:"",domain:"",version:"",Ti:"",td:"",oa:d.ua,Ka:d.ua,fk:1E4},a)};var V={};v.Ha.prototype.send=function(a){a=d.K(this.options,a);var b=d.Lb("_fsr_swfContainerv2_");if(b&&b.ping){var c=u.qc();V[c]={df:this,Rh:a.oa,
rh:a.Ka};"data"==a.action?b.sendData(a.data,a.url,c,a.zf,a.od,a.domain,a.td,a.version,a.encoding,a.Ti):b.ping(a.url,c,a.od,a.td)}};d.FlashTransportResponse=function(a,b){var c=J.parse(b);c.status&&0<c.status?V[a].Rh.call(V[a].df,b):V[a].rh.call(V[a].df,b);delete V[a]};v.Ha.N=function(){var a=d.Lb("_fsr_swfContainerv2_");if(a)try{a.parentNode.removeChild(a),a.sendData=null,a.ping=null,a.isAlive=null}catch(b){}d.Na(V)};v.Ha.isSupported=function(){return d.Gf};v.Ha.Ra=function(a){v.Ha.Md=a;a=A.createElement("div");
a.id="_fsr_swfContainerv2";G(a,{position:"absolute",top:1,left:1,width:1,height:1,minWidth:1,minHeight:1,padding:0,margin:0,display:"block",visibility:"visible"});A.body.appendChild(a);var b=A.body;G(a,{top:b.scrollTop+1,left:b.scrollLeft+1});a={src:u.qb(m.site,"swf_files")+"foresee-transport.swf",id:"_fsr_swfContainerv2_"};var b="_fsr_swfContainerv2",c={quality:"high",wmode:"transparent",allowScriptAccess:"always"};"string"==typeof b&&(b=A.getElementById(b.replace("#","")));b&&("string"==typeof a&&
(a={src:a}),new Fa(b,d.K(d.K({},Aa),a),c));this.Bh()};v.Ha.Bh=function(){var a=v.Ha;a.nh=setInterval(function(){var a=v.Ha,c=d.Lb("_fsr_swfContainerv2_");c&&(c.isAlive&&a.Md)&&(clearInterval(a.nh),a.Md.call(v.Ha),a.Md=null)},500);a=null};d.f.$b={};var q=d.f.$b;q.Xf=function(a,b){if(b.contains)return b.contains(a);if(b.compareDocumentPosition)return!!(b.compareDocumentPosition(a)&16);for(;a.parentNode;){if(a==b||a==b.body)return!0;a=a.parentNode}return!1};q.Bf=function(){return d.P?l.document.charset:
l.document.characterSet};q.Ei=function(a){var b=a;if(a){if(a._pw)return a._pw;if(a.ownerDocument&&a.ownerDocument.defaultView)return a.ownerDocument.defaultView;for(;a.parentNode||a.document;){if(a.document)return b._pw=a;a=a.parentNode}}};q.be=function(a,b,c){var d="";(b=b.document.defaultView)&&b.getComputedStyle?d=b.getComputedStyle(a,"").getPropertyValue(c):a.currentStyle&&(c=c.replace(/\-(\w)/g,function(a,b){return b.toUpperCase()}),d=a.currentStyle[c]);return d};q.Fi=function(a,b){for(var c=
0,d=0,e=q.be;a;)c+=a.offsetTop+(parseFloat(e(a,b,"borderTopWidth"))||0),d+=a.offsetLeft+(parseFloat(e(a,b,"borderLeftWidth"))||0),a=a.offsetParent;return{x:d,y:c}};q.Gi=function(a,b){for(var c,d=!1;!d&&b;){var d=b.parent==b,e=q.Fi(a,b);c?(c.x+=e.x,c.y+=e.y):c=e;a=b.frameElement;b=b.parent}return c};q.Ai=function(a){var b=a||l.document;a=b.body;var b=b.documentElement,c=Math.max;return{width:c(c(a.scrollWidth,b.scrollWidth),c(a.offsetWidth,b.offsetWidth),c(a.clientWidth,b.clientWidth)),height:c(c(a.scrollHeight,
b.scrollHeight),c(a.offsetHeight,b.offsetHeight),c(a.clientHeight,b.clientHeight))}};var z={invite:void 0,qualifier:void 0,locale:void 0,canceled:!1};d.k=function(a){d.K(this,{options:d.K({},a),Qf:!1,Uf:!1,te:null,of:!1,Dg:!1,wf:[],re:null,Kk:null,nb:null,xc:null,tf:null,xb:null});m.controller=this;this.$j()};d.k.loaded=new E;d.k.Lf=new E;d.k.wg=new E;d.k.fe=new E;d.k.Nf=new E;d.k.Of=new E;d.k.Ag=new E;d.k.zg=new E;d.k.kg=new E;d.k.vg=new E;d.k.prototype.$j=function(){if(d.k.R.Zd)for(var a=[["loaded",
d.k.loaded],["initialized",d.k.Lf],["surveyDefChanged",d.k.wg],["inviteShown",d.k.fe],["inviteAccepted",d.k.Nf],["inviteDeclined",d.k.Of],["trackerShown",d.k.Ag],["trackerCanceled",d.k.zg],["qualifierShown",d.k.kg],["surveyShown",d.k.vg]],b=0;b<a.length;b++){var c=a[b];d.Da(d.k.R.Zd[c[0]])&&c[1].Ma(d.k.R.Zd[c[0]])}};d.k.prototype.X=function(a){switch(a){case 3:return a=g.n("t"),d.q(a)&&1===a;case 6:return a=g.n("t"),d.q(a)&&0===a;case 2:return d.q(g.n("i"));case 1:return 1===g.n("i");case 4:return d.q(g.n("s"));
case 5:return d.q(g.n("m"))}return!1};d.k.prototype.load=function(){this.xk=d.now();l.__$$FSRINIT$$__&&!0===l.__$$FSRINIT$$__||(l.__$$FSRINIT$$__=!0,m.auto&&(this.execute(this.mg,!0),this.wk=d.now()))};d.k.prototype.execute=function(){var a=arguments;if(m.enabled&&(m.frames||l==l.top)){for(var b=[],c=0;c<a.length;c++)b.push(a[c]);a=d.shift(b);this.Qf?a.apply(this,b):(this.wf.push({fn:a,args:b}),this.Uf||(this.Uf=!0,h.Oi(function(a){return function(){a.Ra()}}(this),m.embedded)))}};d.k.prototype.Ra=
function(){d.k.loaded.U();this.yf=!d.q(g.n("pv"));this.uc();if(this.yf&&d.q(d.f)){var a=h.ea.Fj;if(a.enabled&&h.C.va==h.C.V.jb){a.url="/"+m.replay_id+".gif";(new v.sa(d.K({onSuccess:function(a){return function(c){a.Yh(c);a.loaded()}}(this),onError:function(a){return function(){a.loaded()}}(this)},a))).Sb();return}}this.loaded()};d.k.prototype.loaded=function(){this.Qf=!0;setTimeout(function(a){return function(){var b=d.shift(a.wf);b&&(a.execute(b.fn,b.args),setTimeout(function(a){return function(){a.loaded()}}(a),
100))}}(this),100)};d.k.prototype.uc=function(){this.of=!0;this.X(3)||d.wd();if(this.yf){this.Ja()&&(h.C.na(h.C.V.Ya,"Exit: Met exclude criteria"),d.f&&d.f.ha());var a,b=m.site;w.altcookie&&w.altcookie.name&&(!(a=g.F.Ea(w.altcookie.name))||w.altcookie.value&&w.altcookie.value!=a||(h.C.na(h.C.V.Ke,"Exit: Alternate persistent cookie found: "+a),d.f&&d.f.ha()));a=new g.F(g.sb("fsr.r"),{path:"/",domain:b.domain,secure:b.secure,encode:m.encode});var c;(c=a.get("i"))&&d.now()<a.get("e")&&(m.rid=c);m.rid||
w.events.enabled&&w.events.id&&(m.rid=u.qc());m.rid&&g.n("rid",m.rid);if(c=a.get("s"))g.n("sd",c),g.n("lk",1);if((c=d.Tc())&&""!=c){w.meta.ref_url&&g.n("ru",c);if(w.meta.referrer){a=c.match(/^(\w+:\/\/)?((\w+-?\w+\.?)+)\/[!]?/);var f;a&&3<=a.length&&(f=a[2]);g.n("r",f)}w.meta.terms&&g.n("st",this.li(c)||"")}w.meta.entry&&(f=d.Ba(d.ma()),w.meta.entry_params||(f=f.replace(/(.*?)(\?.*)/g,"$1")),g.n("ep",f));h.C.va==h.C.V.jb&&w.invite.css&&d.ed(u.qb(m.site,"css_files")+w.invite.css,"link",d.ua);this.sj(g.n())}m.rid=
g.n("rid");f=w.tracker.timeout;w.tracker.adjust&&d.q(g.n("f"))&&(f=g.n("to"),c=(d.now()-g.n("f"))/1E3,f=Math.round(10*(0.9*f+0.1*2*c))/10,f=2>f?2:5<f?5:f);w.tracker.adjust&&g.n("to",f);d.k.Lf.U()};d.k.prototype.mg=function(){this.Hj();var a=!1;this.xc&&(a=this.ig(this.xc));this.nb&&(this.rj(this.nb,a),a||this.ig(this.nb),this.pj(this.nb),this.tj());this.uj()};d.k.prototype.Hj=function(){var a,b;m.sv=u.Tb();this.te=g.Jc("http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/fsr.sp",g.Xc("http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/fsr.sp"));d.q(g.n("cd"))&&(this.xb=g.n("cd"));m.cs=d.Ba(d.ma());
w.meta.url_params||(m.cs=m.cs.replace(/\n/g,"").replace(/(.*?)(\?.*)/g,"$1"));w.meta.url&&g.n("c",m.cs);this.language();var c=g.n("pv")?g.n("pv")+1:1;g.n("pv",c);c=g.n("lc")||{};a=this.Si();if(0!=a.length){for(b=a.length;0<b;){b=m.Xb[a[0]];b.idx=a[0];a="d"+b.idx;this.sf(b.criteria);c[a]||(c[a]={v:0,s:!1});b.lc=c[a].v+=1;b.ec=c[a].e||0;b.type="current";this.qf(b);var f=this.hi(this.Pi(b),b.lc,b.ec);-1<f?(b.ls=c[a].s=!0,d.ia(b.criteria.lf)&&(b.criteria.lf=b.criteria.lf[f],b.criteria.sp=b.criteria.sp[f],
b.pop.when=b.pop.when[f],d.ia(b.invite.dialogs)&&(b.invite.dialogs=b.invite.dialogs[f])),b.pin&&(a=g.n("pn"),(!d.q(a)||a>b.idx)&&g.n("pn",b.idx))):(b.ls=c[a].s=!1,d.ia(b.criteria.lf)&&(b.criteria.lf=b.criteria.lf[0],b.criteria.sp=b.criteria.sp[0],b.pop.when=b.pop.when[0],d.ia(b.invite.dialogs)&&(b.invite.dialogs=b.invite.dialogs[0])));this.rf(b);a=g.n("i");!d.q(a)&&(h.C.va==h.C.V.jb&&b.hj)&&(a=u.Tb(),0<a&&a<=b.hj||(h.C.na(h.C.V.Fd,"Exit: Not in local sample: "+a),d.f&&d.f.ha()));this.nb=b;this.tf=
b.idx;break}g.n("lc",c)}d.q(this.xb)&&(this.xb!=this.tf&&this.xb<m.Xb.length)&&(b=m.Xb[this.xb],b.idx=this.xb,a="d"+b.idx,this.sf(b),b.lc=c[a].v||0,b.ls=c[a].s||!1,b.type="previous",this.qf(b),this.rf(b),this.xc=b,this.xb=b.idx,d.k.wg.U(this.xc,this.nb))};d.k.prototype.ig=function(a){return h.C.va<h.C.V.Ya?!1:this.yj(a)?!0:this.jg(a)};d.k.prototype.rj=function(a,b){g.n("cd",a.idx);if(!b&&a.ls&&!g.n("lk")){var c=g.n("pn");d.q(c)&&c<a.idx||g.n("sd",a.idx)}};d.k.prototype.pj=function(a){h.C.va<h.C.V.Ya&&
!m.attach||(this.X(1)&&!this.X(4)&&(this.Ab(a,"pop",this.cg),this.Ab(a,"cancel",this.Pc)),this.X(2)||this.Ab(a,"attach",this.qe),this.X(3)&&this.Ab(a,"pause",this.pause),this.X(5)&&h.Ed.gb(w.mobileHeartbeat.delay,w.mobileHeartbeat.max))};d.k.prototype.yj=function(a){if(!this.Jj(a)||!this.X(3))return!1;ua(this,a,"tracker");return!0};d.k.prototype.Jj=function(a){if(!a.ls)return!1;if("previous"===a.type){if("later"!==a.pop.when||"leaving-section"!==a.pop.after)return!1}else if("current"===a.type&&"now"!==
a.pop.when)return!1;return!0};d.k.prototype.jg=function(a){var b=!0;this.Ij(a)||(b=!1);b&&(this.qj(a),ua(this,a,"invite"));return b};d.k.prototype.Ij=function(a){if(!a.invite)return!1;var b=this.X(2);if(a.invite.type&&"static"===a.invite.type)return!1;if(a.invite.type&&"dynamic"===a.invite.type&&b)return!0;if(b)return!1;b=d.Ba(d.ma());if(a.invite.include){var c=!0;a.invite.include.local&&(c=this.me(a.invite.include.local,b));if(!c)return this.Fg(a),!1}if(a.invite.exclude&&(c=!1,(c=this.me(a.invite.exclude.local||
[],b))||(c=this.me(a.invite.exclude.referrer||[],d.Ba(d.Tc()))),c||(c=d.k.R.Ja&&d.Da(d.k.R.Ja.Ob)?d.k.R.Ja.Ob():!1),c))return this.Fg(a),!1;b="previous"===a.type?"onexit":"onentry";return a.invite&&a.invite.when!=b||!a.ls?!1:0<a.sv&&a.sv<=a.criteria.sp};d.k.prototype.qj=function(a){var b=a.alt;if(b)for(var c=u.Tb(),d=0,e=0,k=b.length;e<k;e++)if(d+=b[e].sp,c<=d){b[e].url?(a.pop.what="url",a.pop.url=b[e].url):b[e].script&&(a.pop.what="script",a.pop.script=b[e].script);delete a.invite;break}};d.k.prototype.Ni=
function(a,b){switch(b){case "invite":this.$h(a);break;case "tracker":this.bg(a)}};d.k.prototype.me=function(a,b){for(var c=0,d=a.length;c<d;c++)if(b.match(a[c]))return!0;return!1};d.k.prototype.Fg=function(a){var b=g.n("lc");a.ec=b["d"+a.idx].e=(b["d"+a.idx].e||0)+1;g.n("lc",b)};d.k.prototype.$h=function(a){var b=this.Ob,c=this;"hybrid"===w.mode&&(b=this.gi);(new v.sa(d.K({onSuccess:function(){b.call(c,a)},onError:function(){b.call(c,a)}},h.ea.X))).Sb()};d.k.prototype.gi=function(a){var b=g.n("h");
if(!d.q(b)){var c=this.Ob,f=this;(new v.sa(d.K({hd:{"do":0},success:h.ea.X.oa,onSuccess:function(){c.call(f,a)},onFailure:function(){g.n("h",1)}},h.ea.domain))).Sb()}};d.k.prototype.Ab=function(a,b,c){if(a.links){var d=0;b=a.links[b]||[];for(var e=0,k=b.length;e<k;e++)d+=this.link(b[e].tag,b[e].attribute,b[e].patterns||[],b[e].qualifier,c,a,{sp:b[e].sp,when:b[e].when,invite:b[e].invite,pu:b[e].pu,check:b[e].check})}};d.k.prototype.link=function(a,b,c,f,e,k,g){for(var h=0,r=0;r<c.length;r++){for(var l=
c[r],l=C(a+"["+b+"*='"+l+"']"),m=0;m<l.length;m++)h++,D(l[m],"click",function(a,b,c,f,e){return function(){f&&d._qualify(f);e.call(a,b,c)}}(this,k,g,f,e));l=l=null}f=g=k=e=null;return h};d.k.prototype.qf=function(a){var b=a.criteria.lf;"number"===typeof b&&(a.criteria.lf={v:b,o:">="})};d.k.prototype.Pi=function(a){var b=a.criteria.lf;d.ia(b)||(b=[a.criteria.lf]);return b};d.k.prototype.hi=function(a,b,c){for(var d=-1,e=0,k=a.length;e<k;e++)">="==a[e].o&&b>=a[e].v?d=e:"="==a[e].o&&b-c==a[e].v?d=e:
">"==a[e].o&&b>a[e].v&&(d=e);return d};d.k.prototype.Ja=function(){var a=w.exclude,b=d.k.R.Ja&&d.Da(d.k.R.Ja.global)?d.k.R.Ja.global():!1;return a?d.match(a)||b:b};d.k.prototype.rf=function(a){a.sv=m.sv;d.ia(a.criteria.sp)&&(a.criteria.sp=a.criteria.sp[(new Date).getDay()]);var b=a.name+(a.section?"-"+a.section:""),c=b+(z.locale?"-"+z.locale:"");a.criteria.sp=this.te.get(b)||this.te.get(c)||a.criteria.sp;!1!==a.invite&&(a.invite=d.Rb(w.invite,a.invite||{}));b=["tracker","survey","qualifier","cancel",
"pop"];for(c=0;c<b.length;c++){var f=b[c];a[f]=d.Rb(w[f],a[f]||{})}a.repeatdays=w.repeatdays||a.repeatdays;d.ia(a.repeatdays)||(b=a.repeatdays,a.repeatdays=[b,b])};d.k.prototype.Yj=function(){m.enabled&&(!this.Dg&&this.of)&&(this.Dg=!0,this.Xj())};d.k.prototype.Xj=function(){0==z.invite&&(d.f&&d.f.ha(),h.log("103"));w.previous&&g.n("p",m.cs);w.tracker.adjust&&g.n("f",d.now())};d.k.prototype.Si=function(){for(var a=[],b=m.Xb,c=0,f=b.length,e=0;c<f;c++)if(!b[c].site||b[c].site==m.site.name){if(b[c].platform){var k=
"desktop",g=x.L.toLowerCase();"windows"!=g&&("mac"!=g&&"linux"!=g)&&(k="mobile");if(b[c].platform!=k)continue}if(d.match(b[c].include)){a[e++]=c;break}}return a};d.k.prototype.Yh=function(a){var b=u.Tb();0<b&&b<=a&&1!=a||(1!=a&&h.C.na(h.C.V.Fd,"Exit: Not in adjusted sample: "+b),d.f&&d.f.ha(1==a))};d.k.prototype.Ob=function(a){var b=this;z.locale&&g.n("l",z.locale);if(a.invite){if(!this.Li){this.Li=!0;if(a.invite.SurveyMutex){var c=a.invite.SurveyMutex;if(l[c])return;l[c]=!0}"random"==a.pop.when&&
(c=d.q(a.pop.now)?["now","later"]:["later","now"],u.Tb()<=a.pop[c[0]]?(a.invite.dialogs=a.invite.dialogs[c[0]],a.pop.when=c[0]):(a.invite.dialogs=a.invite.dialogs[c[1]],a.pop.when=c[1]));setTimeout(function(){g.n("i",0);var c;if(w.altcookie&&w.altcookie.name&&(c=g.F.Ea(w.altcookie.name))&&(!w.altcookie.value||w.altcookie.value==c)){d.f&&d.f.ha();return}d.k.fe.U(a,g.n());h.Z.fireEvent("invite_shown");z.repeatoverride||b.rd(a,1);h.log("100",m.cs);"page"==a.invite.type?b.jj(a):(d.K(z,{invite:0,repeatoverride:w.repeatoverride||
!1}),b.Dk=d.now(),b.hg(a,"invite"),b.Ck=d.now())},1E3*(a.invite.delay||0))}}else setTimeout(function(){d.K(z,{invite:0,repeatoverride:w.repeatoverride||!1});g.n("i",z.invite);z.repeatoverride||b.rd(a,1);b.Kb(a)},0)};d.k.prototype.hg=function(a,b){var c=this;a[b].css?d.ed(u.qb(m.site,"css_files")+a[b].css,"link",function(){c.pg(a)}):setTimeout(function(){c.pg(a)},100)};d.k.prototype.pg=function(a){function b(b){c.ob(a,b)}this.Hk=d.now();var c=this,f=0,f={Ac:{href:u.qb(m.site,"image_files"),accepted:function(b){c.Kb(a,
b)},preAccept:function(){c.wj(a)},declined:b,qualified:function(b){c.ld(a,b)},close:b}};z.type=0;for(var e=new h.Fb(f,a),k=a.invite?a.invite.hide:[],f=0;f<k.length;f++)G(C("#"+k[f]),{visibility:"hidden"});a.invite&&a.invite.hideFlash&&G(C("object, embed"),{visibility:"hidden"});e.Ji();this.Gk=d.now()};d.k.prototype.wj=function(a){"later"==a.pop.when&&(!a.invite.isMDOT&&a.pop.tracker)&&(def=a.tracker,opts="location=0,status=0,scrollbars=1,resizable=1,width="+def.width+",height="+def.height+",left="+
(l.screen.width-def.width)/2+",top="+(l.screen.height-def.height)/2+",toolbar=0,menubar=0",this.re=l.self.open("about:blank","mywindow",opts),this.re.blur())};d.k.prototype.Kb=function(a,b){d.k.Nf.U(a,g.n());h.Z.fireEvent("invite_accepted");b&&(z[b]=b,g.n("l",b));z.invite=1;h.log("101");g.n("i",1);a.lock&&g.n("lk",1);this.rd(a,0);h.C.na(h.C.V.jb);d.f&&(d.f.Pf()?d.f.Vj():d.f.zb());this.oj(a);this.closed(a)};d.k.prototype.ob=function(a,b){d.k.Of.U(a,g.n());h.Z.fireEvent("invite_declined");b&&(z[b]=
b,g.n("l",b));z.invite=-1;h.log("102");g.n("i",-1);this.rd(a,1);d.f&&d.f.ha();this.closed(a)};d.k.prototype.closed=function(a){for(var b=a.invite?a.invite.hide:[],c=0;c<b.length;c++)G(C("#"+b[c]),{visibility:"visible"});a.invite&&a.invite.hideFlash&&G(C("object, embed"),{visibility:"visible"})};d.k.prototype.ld=function(a,b){b&&(z[b]=b,g.n("l",b));z.qualifier=1;h.log("301");this.xj(a)};d.k.prototype.dj=function(a){z.repeatoverride=1==a};d.k.prototype.oj=function(a){"later"==a.pop.when?a.invite.isMDOT||
(a.pop.tracker&&this.fg(a),this.Ab(a,"pop",this.cg),this.Ab(a,"cancel",this.Pc),this.Ab(a,"pause",this.pause)):"now"==a.pop.when?this.eg(a):"both"==a.pop.when&&(this.fg(a),this.se(a))};d.k.prototype.eg=function(a){g.n("s",1);switch(a.pop.what){case "survey":this.se(a);break;case "qualifier":this.kj(a);break;case "url":this.mj(a);break;case "script":this.lj(a)}};d.k.prototype.xj=function(a){z.canceled?this.ag(a):this.se(a)};d.k.prototype.bg=function(a,b){this.X(3)?this.tg(a,b):this.eg(a)};d.k.prototype.se=
function(a){d.k.vg.U(a,g.n());var b=a.survey,c=a.pop;this.dg(h.Ef(a,c.now),b.width,b.height,c.pu,"400")};d.k.prototype.ij=function(a){var b=w.survey,c="feedback",d=z.locale;a&&(c+="-"+a);d&&(c+="-"+d);this.dg(c,b.width,b.height,!1,"600")};d.k.prototype.dg=function(a,b,c,f,e){var k=h.ea.Qj,g=new Date-0+"_"+Math.round(1E13*Math.random()),n=u.hash(g),g=u.Fa({sid:a,cid:m.id,pattern:m.cs,a:g,b:n,c:d.Gc,version:m.version}),n=h.xa.Fa();a=h.Z.Fa(h.Z.Cf("survey_shown"));k=d.Sc()+"//"+k.host+k.path+k.url+"?"+
g+"&"+n;a&&""!=a&&(k=k+"&"+a);k=h.Z.Di(k);this.pop(e,k,(l.screen.width-b)/2,(l.screen.height-c)/2,b,c,f);h.log(e,m.cs)};d.k.prototype.fg=function(a){this.X(3)||(d.k.Ag.U(a,g.n()),l.fsr$timer=setInterval(d.rc,1E3),this.jd(a.tracker,!0,"200"))};d.k.prototype.kj=function(a){d.k.kg.U(a,g.n());this.jd(a.qualifier,a.pop.pu,"300",a.pop.now)};d.k.prototype.jj=function(a){d.k.fe.U(a,g.n());g.F.write("fsr.p",d.ma(),{path:"/",domain:m.site.domain});this.jd(a.invite,!1,"_self")};d.k.prototype.ag=function(a){this.jd(a.cancel,
!1,"500")};d.k.prototype.cg=function(a,b){var c=!0;this.X(4)||(d.Da(b.X)&&(c=b.X()),c&&!this.X(6)&&this.bg(a,b))};d.k.prototype.Pc=function(a){if(!g.n("lk")&&this.X(3)){var b=l.open("","fsr200");b&&(d.k.zg.U(a,g.n()),b.close())}};d.k.prototype.tg=function(a,b){var c=this;"Firefox"==x.W&&a.qualifier.content?(this.Pc(a),setTimeout(function(){h.log("300",m.cs);c.hg(a,"qualifier")},1E3*(a.qualifier.delay||0))):g.n("fo",b&&b.pu?2:1)};d.k.prototype.jd=function(a,b,c,f){this.page(a);var e=(l.screen.width-
a.width)/2,k=(l.screen.height-a.height)/2,g=u.qb(m.site,"html_files")+(a.url.pop||a.url),n={siteid:m.siteid,name:m.site.name,domain:m.site.domain};f&&(n.when=f);f=u.Fa(n);g+="?"+f;f=c;"window"===m.storageOption&&(f=J.parse(l.name),f.popOther=c,f=d.stringify(f));this.pop(f,g,e,k,a.width,a.height,b);h.log(c,m.cs)};d.k.prototype.qe=function(a,b){if(!this.X(2)){var c=this;b.sp&&(a.criteria.sp=b.sp);if(b.when||b.qualifier)a.pop.when=b.when;0<a.sv&&a.sv<=a.criteria.sp&&(z.locale&&g.n("l",z.locale),b.invite?
this.jg(a):setTimeout(function(){c.Kb(a)},0))}};d.k.prototype.mj=function(a){var b=w.survey.width,c=w.survey.height;this.pop("Other",a.pop.url,(l.screen.width-b)/2,(l.screen.height-c)/2,b,c)};d.k.prototype.lj=function(a){d.ed(a.pop.script,"script")};d.k.prototype.pause=function(a){!d.q(a)||a?d.fj():d.gb()};d.k.prototype.pop=function(a,b,c,f,e,k,g){var h="",r=a;"_self"!=a&&(r="fsr"+a,h="location=0,status=0,scrollbars=1,resizable=1,width="+e+",height="+k+",left="+c+",top="+f+",toolbar=0,menubar=0");
"Winphone"==x.L?setTimeout(function(a){return function(){l.location=a}}(b),10):(a=this.re,d.q(a)?(a.document.location.href=b,a.name=r,g&&(l.open("about:blank").close(),l.self.focus())):(b=l.open(b,r,h,!1))&&g&&(b.blur(),"Firefox"==x.W?function(a){a.window.open("about:blank").close();a.opener.window.focus()}(b):l.focus()))};d.k.prototype.language=function(){var a=w.language;if(a&&(z.locale=a.locale,a.src)){var b=z.locale,c,f,e=a.type;switch(a.src){case "location":c=d.Ba(d.ma());break;case "cookie":c=
e&&"client"==e?g.F.Ea(a.name):g.n("lang");break;case "variable":d.ia(a.name)||(a.name=[a.name]);for(f=0;f<a.name.length;f++){var k=new Function("return "+a.name[f]);if(e&&"client"==e)try{c=k.call(l)}catch(p){c=void 0}else c=m[a.name];if(c)break}break;case "meta":0!=(f=A.getElementsByName(a.name)).length&&(c=f[0].content);break;case "navigator":c=navigator.browserLanguage||navigator.language;break;case "function":d.Da(a.value)&&(c=a.value.call(l,a,this))}c=c||"";a=a.locales||[];e=0;for(k=a.length;e<
k;e++){d.ia(a[e].match)||(a[e].match=[a[e].match]);var h;f=0;for(var r=a[e].match.length;f<r;f++)if(h=c.match(a[e].match[f])){b=a[e].locale;break}if(h)break}z.locale=b}};d.k.prototype.page=function(a){var b=g.n("l");if(b)for(var c=a.locales||[],f=0,e=c.length;f<e;f++)c[f].locale==b&&(d.Td("url",c[f],a),d.Td("width",c[f],a),d.Td("height",c[f],a))};d.k.prototype.sf=function(a){var b=z.locale;if(b)for(var c=a.locales||[],d=0,e=c.length;d<e;d++)if(c[d].locale==b){a.sp=c[d].sp;a.lf=c[d].lf;break}};d.k.prototype.li=
function(a){a=d.Ba(a||d.Tc());for(var b,c=null,f=["q","p","query"],e=0;e<f.length;e++)if(c=a.match(RegExp("[?&]"+f[e]+"=([^&]*)")))return!1;if(!c)return b;(b=decodeURI(c[1]))&&(b=b.replace(/\+/g," "));return b};d.k.prototype.rd=function(a,b){if(!z.repeatoverride&&a.repeatdays&&a.repeatdays[b]){var c=new g.F(g.sb("fsr.r"),{path:"/",domain:m.site.domain,secure:m.site.secure,duration:a.repeatdays[b],encode:m.encode}),f=c.get();f.d=a.repeatdays[b];var e=w.events;if(e.pd){f.i=m.rid;var k=new Date;k.setDate(k.getDate()+
e.pd);f.e=k.getTime();a.lock&&(f.s=a.idx)}c.reset(f);w.altcookie&&w.altcookie.name&&g.F.write(w.altcookie.name,w.altcookie.value,{path:w.altcookie.path||"/",domain:w.altcookie.domain||m.site.domain,secure:m.site.secure,duration:w.altcookie.persistent?w.altcookie.repeatdays||a.repeatdays[b]:null});"hybrid"==w.mode&&(new v.sa(d.K({hd:{"do":1,rw:1440*a.repeatdays[b]}},h.ea.domain))).Sb()}};d.k.prototype.tj=function(){var a=w.cpps;if(a)for(var b in a)if(a.hasOwnProperty(b)){var c=a[b],f="",e,k,p=c.mode,
n=c.arg,r=p&&"append"==p?h.xa.append:h.xa.set;if(!c.url||d.Ba(d.ma()).match(c.url)){if(c.pin&&(f=FSR.xa.get(b))){for(var p=!1,q=0,t=c.pin.length;q<t;q++)if(f===c.pin[q]){p=!0;break}if(p)continue}switch(c.source.toLowerCase()){case "url":k=function(){var a=b,f,e=c.patterns||[],k=r;return function(){for(var b=0,c=e.length;b<c;b++)if(d.Ba(d.ma()).match(e[b].regex)){f=e[b].value;break}f&&""!=f&&k(a,f,n)}};break;case "parameter":k=function(){var a=b,d=c.name,f=r,e;return function(){(e=u.Zf(d))&&""!=e&&
f(a,e,n)}};break;case "cookie":k=function(){var a=b,d=c.name,e=r;return function(){if((f=g.F.Ea(d))&&c.parameter){var b=c.parameter,b=b.replace(/[\[]/,"\\[").replace(/[\]]/,"\\]"),b=RegExp(b+"=([^&#]*)").exec(f);f="";b&&(f=b[1])}f&&""!=f&&e(a,f,n)}};break;case "variable":k=function(){var a=b,d=c.name,f=r,e;return function(){try{if(e=(new Function("return "+d)).call(l),void 0===e||null===e)e=!1}catch(b){e=!1}e&&""!=e&&f(a,e,n)}};break;case "meta":k=function(){var a=b,d=c.name,f=r,k;return function(){0!=
(e=A.getElementsByName(d)).length&&(k=e[0].content);k&&""!=k&&f(a,k,n)}};break;case "function":k=function(){var a=b,f=r,e,k=c;return function(){d.Da(k.value)&&(e=k.value.call(l,b,k,m.controller));e&&""!=e&&f(a,e,n)}};break;case "static":k=function(){var a=b,d=r,f=c.value;return function(){f&&""!=f&&d(a,f,n)}}}c.on&&"load"!=c.on&&c.query?D(c.query,c.on,k()):k()()}}};d.k.prototype.sj=function(a){var b=w.cpps;if(b)for(var c in b)if(b.hasOwnProperty(c)){var d=b[c];d.init&&h.xa.set(c,d.init,void 0,a)}};
d.k.Ib=function(a,b,c,d){var e=g.n("ev")||{};!d||""==d||e["e"+b]&&!a.repeat||(e["e"+b]=(e["e"+b]||0)+1,h.log(c,d),g.n("ev",e))};d.k.prototype.uj=function(){if(Math.abs(h.C.va)==h.C.V.jb){var a=w.events;if(a.custom){var b=0,c;for(c in a.custom)if(a.custom.hasOwnProperty(c)){var f=a.custom[c],e=a.codes[c];if(f.enabled){var k;switch(f.source.toLowerCase()){case "url":k=function(){var a=f,c=b,k=e,g=f.patterns||[],h;return function(){for(var b=0,f=g.length;b<f;b++)if(d.Ba(d.ma()).match(g[b])){h=g[b];break}d.k.Ib(a,
c,k,h)}};break;case "parameter":k=function(){var a=f,c=b,k=f.name,g=e,h;return function(){h=u.Zf(k);d.k.Ib(a,c,g,h)}};break;case "cookie":k=function(){var a=f,c=b,k=f.name,h=e,l;return function(){l=g.F.Ea(k);d.k.Ib(a,c,h,l)}};break;case "variable":k=function(){var a=f,c=b,k=f.name,g=e,h;return function(){try{if(h=(new Function("return "+k)).call(l),void 0===h||null===h)h=!1}catch(b){h=!1}d.k.Ib(a,c,g,h)}};break;case "function":k=function(){var a=f,c=b,k=f.value,g=e,h;return function(){d.Da(k)&&(h=
k.call(l,a,f,m.controller));d.k.Ib(a,c,g,h)}};break;case "static":k=function(){var a=f,c=b,k=f.value,g=e;return function(){d.k.Ib(a,c,g,k)}}}f.on&&"load"!=f.on&&f.query?D(f.query,f.on,k()):k()();b++}}}}};d.popNow=function(a){oa(a,"now")};d.popLater=function(a){oa(a,"later")};d.popImmediate=function(){oa(100,"now")};d.popFeedback=function(a){var b=m.controller;b.execute(b.ij,a)};d.clearTracker=function(){g.F.ub(g.sb("fsr.r"),{path:"/",domain:m.site.domain,secure:m.site.secure});g.F.ub(g.sb("fsr.s"),
{path:"/",domain:m.site.domain,secure:m.site.secure})};d.stopTracker=function(a){m.controller.tg(d._sd(),{pu:a})};d.run=function(){var a=m.controller;a.execute(a.mg)};d.invite=function(a,b,c){var f=m.controller;f.execute(f.qe,d._sd(),{sp:a,when:b,qualifier:c,invite:!0})};d.popCancel=function(){m.controller.ag(d._sd())};d.showInvite=function(){m.controller.Ob(d._sd())};d.close=function(){m.controller.Pc(d._sd())};d.pause=function(a){m.controller.pause(a)};d._sd=function(){return m.controller.nb};d._pd=
function(){return m.controller.xc};d._cancel=function(){z.canceled=!0};d._override=function(a){m.controller.dj(a)};d._language=function(a){a&&(z[a]=a,g.n("l",a))};d._qualify=function(a){z.canceled=!1;a&&(z.qid=a,g.n("q",a))};d.Cookie={};d.Cookie.read=function(a){return g.F.Ea(a)};d.Cookie.write=function(a,b,c){c||(c={});c.domain||(c.domain=m.site.domain);return g.F.write(a,b,c)};d.Storage={};d.Storage.read=function(a){return g.n(a)};d.$S=z;d.Ua(function(){h.C.uc();h.C.va==h.C.V.Ya?d.wd():((new d.k).load(),
D(l,"unload",function(){m.controller.Yj()}))});d.f.R={gj:{Oj:{"":"#lowes-salutation-active > a > span.nav-welcome",useraccountlanding:"#mylowes-account-info > div > div > div",addressmanagelistcmd:"#address_book > div > div",homeprofile:"#hp-header > h2 > div > span > a",userprofilecmd:"#dashboard > div > div > div > div > div.primary_address, #dashboard > div > div > div > div.infoCont",managepaymentmethodcmd:"#manage-cc > div > div.mylowes-manage-cc-details, #manage-cc > div > div > span > strong > span.cc_nick",
orderdisplaypendingview:"#display-ship-address-dropdown-PD > li > div, #billing-address-edit > fieldset > ol > li > div > p, #stored-cards > tbody > tr",useraccountview:"#ea_firstName, #ea_lastName, #ea_logonId"},ti:{"":"#DialogAddressConfirm > div > div, #two-column-b > div > table > tbody > tr > td.column-1.first"},Pj:{"":"#Ntt, #Header-map-search, #SL-map-search, input[id*='quantity_of'], #ShopCartForm > div > div > div > div > input, #main_content_rail > div > div > form > select.show_results, #main_content_rail > div > div > form > span > input, #sortBy, #filterBy, #purchase-method, #transaction-date, #no-results > div > form > fieldset > ol > li > input, #houzzSearch, #nav-search-input, #scGenericshipModeId, input[name*='zipcode'], #zipCode"},
ui:{},Zh:{},Ej:{},aj:{}},pe:{windows:!0,mac:!0,linux:!0,ipod:!1,ipad:!1,iphone:!1,android:!1,blackberry:!1,winphone:!1,kindle:!1,nook:!1,wince:!1,mobile:!1,other:!1},Ja:{jk:[{name:"ie",version:"7"}]},ng:null,Bg:"https://replay.foreseeresults.com/rec/",qi:500};n.Qg=function(a,b){for(var c=a.length,d=b.length,c=c>d?d:c,e=d=0,k=0;15<c-d;)chunkSize=Math.round((c-d)/2),k=c-chunkSize,a.substr(0,k)!=b.substr(0,k)?c=k:d=k;for(var g=b.substr(d),c=g.length;15<c-e;)chunkSize=Math.round((c-e)/2),k=c-chunkSize,
g.substr(g.length-k)!=a.substr(a.length-k)?c=k:e=k;return{o:d,c:e,r:b.substring(d,b.length-e)}};n.ra={};n.ra.ad=function(a){try{var b=g.n("meta")||{};b[a]=(b[a]||0)+1;g.n("meta",b)}catch(c){}};n.ra.set=function(a,b){try{var c=g.n("meta")||{};c[a]=b;g.n("meta",c)}catch(d){}};t.J={};t.J.Jb={};t.J.T=function(a,b,c){b=b||[];a.parentNode&&(b=t.J.T(a.parentNode,b));if(1==a.nodeType){var d,e;if(a.previousSibling){d=1;e=a.previousSibling;do 1==e.nodeType&&e.nodeName==a.nodeName&&d++,e=e.previousSibling;while(e);
1==d&&(d=null)}else if(a.nextSibling){e=a.nextSibling;do 1==e.nodeType&&e.nodeName==a.nodeName?(d=1,e=null):(d=null,e=e.previousSibling);while(e)}b.push(a.nodeName.toLowerCase()+(a.getAttribute("id")&&!c?"[@id='"+a.getAttribute("id")+"']["+(d||1)+"]":0<d?"["+d+"]":""))}return b};t.J.uk=function(a){for(var b in t.J.Jb)b.length>a.length&&b.substr(0,a.length)==a&&delete t.J.Jb[b]};t.J.pk=function(a,b){function c(a,b,c,d){if(a){for(var f=a.childNodes,e=0,k=0;k<f.length;k++){var g=f[k];if(1==g.nodeType&&
g.tagName&&g.tagName.toLowerCase()==b){var h=g.getAttribute?g.getAttribute("id"):g.id;if(h==c&&e+1>=d)return g;e++}}f=a.childNodes;for(k=0;k<f.length;k++)if(g=f[k],1==g.nodeType&&(g.tagName&&g.tagName.toLowerCase()==b)&&(h=g.getAttribute?g.getAttribute("id"):g.id,h==c))return g}}function d(a,b){b=b.toLowerCase();var c=[];if(a){for(var f=0;f<a.childNodes.length;f++){var e=a.childNodes[f];e.tagName&&e.tagName.toLowerCase()==b?c[c.length]=e:"#text"==b&&3==e.nodeType&&(c[c.length]=e)}return c}}if(t.J.Jb[a]&&
t.J.Jb[a].parentNode&&b==A)return t.J.Jb[a];for(var e=a.split(","),k=b,g=0;g<e.length;++g){var h=1;if(-1!=e[g].indexOf("[")&&-1!=e[g].indexOf("]"))if(h=e[g].split("[")[1].split("]")[0],-1<h.indexOf("@id="))var l=e[g].split("["),m=l[0],l=parseInt(l[2].replace("]","")),h=h.replace("@id='","").replace("'",""),k=c(k,m,h,l);else(m=d(k,e[g].split("[")[0]))&&(k=m[parseInt(h)-1]);else m=k,(l=d(k,e[g]))&&(k=l[h-1]),!k&&m&&(k=m.getElementsByTagName(e[g])[h-1])}b==A&&(t.J.Jb[a]=k);return k};v.hc={};d.Ua(function(){v.hc.Oe=
{gk:v.Eb,gh:v.Ha,Mg:v.Xa}});v.hc.Ci=function(a){for(var b=void 0,c=null,d=0;d<a.Cg.length;d++){c=a.Cg[d];try{if(c.isSupported()){b=c;break}}catch(e){}}void 0===b?a.Ka.call():b.Ra(a.Ua)};g.aa=function(a,b){a||(a="STORAGE");this.Ca="FSR_"+a.replace(/[- _.&]/g,"").toUpperCase();this.gc=new E;this.zc=3E6;this.ub();this.mb();d.q(b)&&!b||D(l,d.P||x.Sa?"unload":"beforeunload",function(a){return function(){a.Aa()}}(this))};g.aa.prototype.Db=function(){return this.Va+this.Bb>=this.zc?(this.gc.U(this),!0):
!1};g.aa.prototype.N=function(a){this.ba[a]&&(delete this.ba[a],this.Va=d.stringify(this.ba).length)};g.aa.prototype.ub=function(){this.Bb=this.Va=0;this.ba={};this.Ia="";this.la=!0};g.aa.prototype.get=function(a){return this.ba[a]};g.aa.prototype.getBlob=function(){return this.Ia};g.aa.prototype.set=function(a,b){b&&(this.ba[a]=b,this.Va=d.stringify(this.ba).length,this.la=!1,this.Db())};g.aa.prototype.qd=function(a){this.Ia=a;this.Bb=this.Ia.length;this.la=!1;this.Db()};g.aa.Ra=function(a){a.apply(g.aa)};
g.aa.isSupported=function(){return!!l.sessionStorage};g.aa.prototype.mb=function(){try{var a=l.sessionStorage[this.Ca+"_OBJ"];a&&0<a.length&&(this.ba=J.parse(a),this.Va=a.length,this.la=!1)}catch(b){}try{(a=l.sessionStorage[this.Ca+"_BLOB"])&&0<a.length&&(this.Ia=a,this.Bb=a.length,this.la=!1)}catch(c){}};g.aa.prototype.Aa=function(){try{l.sessionStorage[this.Ca+"_OBJ"]=d.stringify(this.ba),l.sessionStorage[this.Ca+"_BLOB"]=this.Ia}catch(a){}};g.fa=function(a,b){a||(a="STORAGE");this.Ca="FSR_"+a.replace(/[- _.&]/g,
"").toUpperCase();this.zc=3E6;this.gc=new E;this.ub();this.mb();d.q(b)&&!b||D(l,d.P||x.Sa?"unload":"beforeunload",function(a){return function(){a.Aa()}}(this))};g.fa.prototype.Db=function(){return this.Va+this.Bb>=this.zc?(this.gc.U(this),!0):!1};g.fa.prototype.N=function(a){this.ba[a]&&(delete this.ba[a],this.Va=d.stringify(this.ba).length)};g.fa.prototype.ub=function(){this.Bb=this.Va=0;this.ba={};this.Ia="";this.la=!0};g.fa.prototype.get=function(a){return this.ba[a]};g.fa.prototype.getBlob=function(){return this.Ia};
g.fa.prototype.set=function(a,b){b&&(this.ba[a]=b,this.Va=d.stringify(this.ba).length,this.la=!1,this.Db())};g.fa.prototype.qd=function(a){this.Ia=a;this.Bb=this.Ia.length;this.la=!1;this.Db()};g.fa.Ra=function(a){a.apply(g.fa)};g.fa.isSupported=da(!0);g.fa.prototype.mb=function(){var a=d.nameBackup,b=this.Ca+"_",c="",f=a.indexOf(b+"BEGIN_OBJ");-1<f&&(c=a.substr(f+(b+"BEGIN_OBJ").length,a.indexOf(b+"END_OBJ")-(f+(b+"BEGIN_OBJ").length)));try{0<c.length&&(this.ba=J.parse(c),this.Va=c.length,this.la=
!1)}catch(e){}c="";f=a.indexOf(b+"BEGIN_BLOB");-1<f&&(c=a.substr(f+(b+"BEGIN_BLOB").length,a.indexOf(b+"END_BLOB")-(f+(b+"BEGIN_BLOB").length)));try{0<c.length&&(this.Ia=c,this.Bb=c.length,this.la=!1)}catch(k){}};g.fa.prototype.Aa=function(){try{try{delete l.name}catch(a){}var b=l.name;d.q(b)||(b=window.name);if(d.q(b)){var c=this.Ca+"_",f=b.indexOf(c+"BEGIN_OBJ"),e=d.stringify(this.ba),e=c+"BEGIN_OBJ"+e+c+"END_OBJ",b=-1<f?b.substr(0,f)+e+b.substr(b.indexOf(c+"END_OBJ")+(c+"END_OBJ").length):b+e,
f=b.indexOf(c+"BEGIN_BLOB"),e=c+"BEGIN_BLOB"+this.Ia+c+"END_BLOB",b=-1<f?b.substr(0,f)+e+b.substr(b.indexOf(c+"END_BLOB")+(c+"END_BLOB").length):b+e;l.name=b;this.xd=l.name.length}}catch(k){}};try{delete l.name}catch(Ia){}d.nameBackup=l.name;d.k.R={Zd:{loaded:S(),initialized:S(),surveydefChanged:S(),inviteShown:function(){s.linkTrackVars="eVar61";s.linkTrackEvents="event76";s.eVar61="foresee:invite_true";s.events=s.apl(s.events,"event76",",",2);s.tl(!0,"o","link name")},inviteAccepted:function(){s.linkTrackVars=
"eVar61";s.linkTrackEvents="event75";s.eVar61="foresee:survey_accepted";s.events=s.apl(s.events,"event75",",",2);s.tl(!0,"o","link name")},inviteDeclined:function(){s.linkTrackVars="eVar61";s.linkTrackEvents="event75";s.eVar61="foresee:survey_declined";s.events=s.apl(s.events,"event75",",",2);s.tl(!0,"o","link name")},trackerShown:S(),trackerCanceled:S(),qualifierShown:S(),surveyShown:S()},Ja:{global:da(!1),Ob:da(!1)},pe:{windows:!0,mac:!0,linux:!0,ipod:!1,ipad:!1,iphone:!1,android:!1,blackberry:!1,
winphone:!1,kindle:!1,nook:!1,wince:!1,mobile:!1,other:!1}};d.f.u.ib={ef:[]};d.f.u.ib.Kc=function(a,b,c,f,e,k){this.j=a;this.yg=f;this.Bj=e;this.Jk=b;k||this.kb({},b);D(b,c,function(a,b){return function(c){a.kb&&a.kb(c,b);b=null}}(this,b));d.f.u.ib.ef.push(this);e=c=a=b=null};d.f.u.ib.Kc.prototype.Rb=function(a,b,c){D(b,c,function(a,b,c){return function(d){a.kb&&a.kb(d,b,c)}}(this,b,a));c=a=null};d.f.u.ib.Kc.prototype.kb=function(a,b,c){clearTimeout(this.ie);c=c||this.j;if(!this.Tf||d.now()-this.Tf>
this.yg)this.Bj.call(this,a,b,c),this.Tf=d.now();else{var f={},e;for(e in a)6==e.length&&("layerX"!=e&&"layerY"!=e)&&(f[e]=a[e]);this.ie=setTimeout(function(a,b,c,f){d.q(b.clientX)&&(b.sX=b.clientX,b.sY=b.clientY);return function(){b.delayed=!0;a.kb&&a.kb(b,c,f)}}(this,f,b,c),this.yg)}b=a=c=c=null};d.f.u.ib.Kc.prototype.N=function(){this.ie&&clearTimeout(this.ie);d.Na(this)};var ia=d.f.u.ib.Kc;d.f.u.qa={Jf:{}};d.f.u.qa.$f=function(a,b){for(var c=C("iframe",b||a.B.document),f=0;f<c.length;f++){var e=
c[f];if(!e._fsrB&&!e.getAttribute("_fsrB")||!d.f.u.qa.Jf[t.J.T(e)]){d.f.u.qa.Jf[t.J.T(e)]=!0;e.setAttribute("_fsrB",!0);if(this.Rj(e)&&this.xg(e)){var k=this.Df(e);if(!k)return;var g=k.document;this.Wj(g);var h=!1;try{var l=g.readyState;if("complete"==l||"loaded"==l)h=!0}catch(m){}h&&(d.q(k.j)?k.j.og():(k.j=new d.f.da(k,t.J.T(e),a),a.Rc.push(k.j),D(k,"unload",function(a){return function(){a.N()}}(k.j))));k=g=null}na(e,"load",function(a){return function(b){d.f.u.qa.ai(b,a)}}(a))}e=null}a=c=null};d.f.u.qa.ai=
function(a,b){var c=a.originalTarget||a.target||a.srcElement;if(c&&d.f.u.qa.xg(c)){var f=d.f.u.qa.Df(c);d.q(f.j)||(f.j=new d.f.da(f,t.J.T(c),b),b.Rc.push(f.j),D(f,"unload",function(a){return function(){a.N()}}(f.j)));f=null}a=b=c=null};d.f.u.qa.Rj=function(a){for(var b=["javascript:","Unknown_83_filename"/*tpa=http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/shim.gif*/,"about:blank"],c=0;c<b.length;c++)if(-1<a.src.indexOf(b[c]))return!1;return!0};d.f.u.qa.xg=function(a){a=a.src;if(-1>=a.indexOf("://"))return!0;var b=document.createElement("a");b.href=a;return b.hostname.toLowerCase()!=
l.location.hostname.toLowerCase()||b.protocol.toLowerCase()!=l.location.protocol.toLowerCase()?!1:!0};d.f.u.qa.Wj=function(a){a=C("iframe[_fsrB='true']",a);for(var b=0;b<a.length;b++)a[b]._fsrB=!1};d.f.u.qa.Df=function(a){var b;a&&a.contentWindow?b=a.contentWindow:a&&(a.contentDocument&&a.contentDocument.defaultView)&&(b=a.contentDocument.defaultView);return b!=b.top?b:null};d.f.u.We=[];d.f.u.Oa=function(a,b){if(!b._fsrTracker){b._fsrTracker=this;d.f.u.We.push(this);this.input=b;this.j=a;var c="",
c=b.getAttribute("type")?b.getAttribute("type").toLowerCase():"text";this.kc()&&(this.vb=b.getAttribute("value")||"");this.kc()&&!b.value||"checkbox"==c&&!b.checked||this.yb(!0);D(b,"scroll",function(a){return function(){a.input&&a.j.D().log(a.j,n.A.G.Me,{t:c,x:a.j.D().Y(a.j,t.J.T(a.input)),ps:{x:a.input.scrollLeft,y:a.input.scrollTop}})}}(this));D(b,"focus",function(a){return function(){a.input&&(a.kc()&&(a.Od(),a.Se=setInterval(function(a){return function(){a.input&&a.Od()}}(a),500)),a.j.D().log(a.j,
n.A.G.He,{t:c,st:1,x:a.j.D().Y(a.j,t.J.T(a.input))}))}}(this));D(b,"blur",function(a){return function(){a.input&&(a.kc()&&clearInterval(a.Se),a.j.D().log(a.j,n.A.G.He,{t:c,st:0,x:a.j.D().Y(a.j,t.J.T(a.input))}))}}(this));if("checkbox"==c||"radio"==c){var f=function(a){return function(){a.input&&a.Mc!=a.input.checked&&(a.Mc=a.input.checked,a.j.D().log(a.j,n.A.G.Qe,{t:c,b:a.input.checked,x:a.j.D().Y(a.j,t.J.T(a.input))}))}}(this);D(b,"click",f);D(a.B,"click",f)}"checkbox"!=c&&"radio"!=c&&D(b,"change",
function(a){return function(){a.input&&(a.Ye!=a.input.innerHTML&&a.yb(),a.vb=a.input.value,a.j.D().log(a.j,n.A.G.Qe,{t:c,si:a.input.selectedIndex,x:a.j.D().Y(a.j,t.J.T(a.input))}))}}(this));this.kc()&&(this.vb=this.input.value,D(b,"keydown",function(a){return function(b){a.input&&(d.P&&(b={keyCode:b.keyCode,charCode:b.charCode}),setTimeout(function(a,b){return function(){a.Od();var d=0;"number"==typeof b.keyCode?d=b.keyCode:"number"==typeof b.which?d=b.which:"number"==typeof b.charCode&&(d=b.charCode);
if(1<Math.abs(a.vb.length-a.input.value.length)||1<a.bb.e-a.bb.s)a.yb();else{0==d&&32==b.charCode&&(d=b.charCode);var f=a.j.D().Y(a.j,t.J.T(a.input));46>=d||91<=d&&96>d||112<=d&&145>=d?a.j.D().log(a.j,n.A.G.Ie,{t:c,xp:f,sk:d,ps:{x:a.input.scrollTop,y:a.input.scrollLeft}}):a.j.D().log(a.j,n.A.G.Ie,{t:c,xp:f,v:a.jc(a.input.value.substr(a.bb.c-1,1)),ps:{x:a.input.scrollTop,y:a.input.scrollLeft}})}a.vb=a.input.value}}(a,b),1))}}(this)));b=a=null}};d.f.u.Oa.prototype.Ff=function(){return this.Mc!=this.input.checked||
this.vb!=this.input.value?!0:!1};d.f.u.Oa.prototype.jc=function(a){return a&&this.Xe()?a.replace(/[^ \n\r\t]/g,"*"):a};d.f.u.Oa.prototype.Xe=function(){var a=this.input.getAttribute("class")||this.input.getAttribute("className");return a&&-1<a.indexOf("fsrVisible")?!1:!0};d.f.u.Oa.prototype.kc=function(){var a="text",b="text password textarea number email url search color ".split(" ");this.input.getAttribute("type")&&(a=this.input.getAttribute("type").toLowerCase());return"textarea"==this.input.tagName.toLowerCase()||
d.$c(a,b)};d.f.u.Oa.prototype.yb=function(a){var b=this.input.tagName.toLowerCase(),c=this.input.getAttribute("type");c||(c=this.input.tagName.toLowerCase(),"input"==c&&(c="text"));if("hidden"!=c&&"button"!=c){if(a){if(("radio"==c||"checkbox"==c)&&!this.input.checked){this.Mc=!1;return}if(("text"==c||"password"==c||"textarea"==b)&&""==this.input.value)return}var d=this.j.D(),e=n.A.G.Wg,k=d.Y(this.j,t.J.T(this.input));"textarea"!=b&&"textarea"!=c||d.log(this.j,e,{t:c,x:k,v:this.jc(this.input.value)||
"",wt:q.be(this.input,this.j.B,"width"),ht:q.be(this.input,this.j.B,"height")});"text"!=c&&"password"!=c&&"submit"!=c||d.log(this.j,e,{t:c,x:k,v:this.jc(this.input.value)||""});"radio"!=c&&"checkbox"!=c||!this.input.checked&&a||(d.log(this.j,e,{t:c,x:k,b:this.input.checked}),this.Mc=this.input.checked);if("select"==c||"select"==b){a=[];for(b=0;b<this.input.options.length;b++)a[a.length]={v:this.jc(this.input.options[b].value),t:this.jc(this.input.options[b].text)};!this.input.multiple&&(this.Xe()&&
1<a.length)&&(a.length=1);d.log(this.j,e,{t:c,x:k,sz:{w:this.input.offsetWidth,h:this.input.offsetHeight},o:a,si:this.input.options.selectedIndex});this.Ye=this.input.innerHTML}}};d.f.u.Oa.prototype.Od=function(){var a=this.wh();this.bb&&this.bb.s==a.s&&this.bb.e==a.e&&this.bb.c==a.c||setTimeout(function(a,c){return function(){a.j.D().log(a.j,n.A.G.Lg,c)}}(this,{x:this.j.D().Y(this.j,t.J.T(this.input)),ci:a}),1);this.bb=a};d.f.u.Oa.prototype.wh=function(){var a=this.input,b=a.value,c=a.ownerDocument,
f={s:0,e:0,c:0};if(d.P)if("textarea"==a.tagName.toLowerCase()){14>b.charCodeAt(b.length-1)&&(b=b.replace(/34/g,"")+String.fromCharCode(28));var c=c.selection.createRange(),e=c.duplicate();e.moveToElementText(a);e.setEndPoint("StartToEnd",c);f.e=b.length-e.text.length;e.setEndPoint("StartToStart",c);f.s=b.length-e.text.length;f.c=f.e;b.substr(b.length-1)==String.fromCharCode(28)&&(b=b.substr(0,b.length-1));a=b.substr(0,f.s).split("\n").length-1;c=b.substr(0,f.e).split("\n").length-1;f.c-=b.substr(0,
f.c).split("\n").length-1;f.s-=a;f.e-=c}else b=c.selection.createRange(),a=b.duplicate(),f.s=0-a.moveStart("character",-1E5),f.e=f.s+b.text.length,f.c=f.e;else try{f.s=a.selectionStart,f.e=a.selectionEnd,f.c=f.e}catch(k){}!f.s&&0>f.s&&(f={s:0,e:0,c:0});return f};d.f.u.Oa.prototype.N=function(){this.Ye=this.bb=this.input=this.vb=this.j=this.input._fsrTracker=null;clearTimeout(this.Se)};d.f.u.Hc=function(a){this.lb={};this.Lc=null;this.j=a;this.ei=0};d.f.u.Hc.prototype.N=function(){d.Na(this.lb);this.j=
this.lb=null;clearTimeout(this.Lc)};d.f.u.Hc.prototype.push=function(a,b,c,f){d&&(b={Lk:a,di:b,If:c,Ak:f,ki:d.now()},a&&1!=a.nodeType&&(a=a.parentNode),a&&this.lb&&(a._fsrKey||(a._fsrKey={id:"_"+this.ei++}),this.lb[a._fsrKey.id+(f||"").toLowerCase()]=b,this.Lc||(this.Lc=setTimeout(function(a){return function(){a.Ii()}}(this),1500)),b=a=null))};d.f.u.Hc.prototype.Ii=function(){this.Lc=null;var a=this.lb,b=d.now(),c;for(c in a){var f=a[c];f._fsrKey=null;"object"==typeof f&&(f.di(b-f.ki),d.Na(f))}d.Na(this.lb);
this.lb={}};d.f.u.ca=function(a){this.j=a;this.Uc=[];this.pc=[];this.pb=new d.f.u.Hc;this.xe=!d.P||(d.q(a.B.Element)||d.q(a.B.HTMLElement))&&!d.f.R.Mk;d.P&&(this.Gj(),this.xe&&this.ej(),this.ve(this.j.B.document),this.j.hb.Ma(function(a){return function(c){a.ve(c)}}(this)));0<="Chrome,Safari".indexOf(x.W)&&(this.Ig=new d.f.u.Gb(a));d.P||(na(a.B.document,"DOMSubtreeModified",this.ri(this)),na(a.B.document,"DOMAttrModified",this.pi(this.j,this)));a=null};d.f.u.ca.Vf={Kf:"input select img link meta title textarea br hr script".split(" "),
Hg:"html,head,header,article,aside,section,details,footer,figure,nav,body,div,span,ul,li,dd,dt,ol,dl,tr,td,span,form,img,a,area,iframe,fieldset,select,input,textarea,table",La:"appendChild removeChild removeNode insertAdjacentHTML replaceChild replaceNode swapNode insertBefore".split(" ")};var P=d.f.u.ca.Vf;d.f.u.ca.prototype.zj=function(a){function b(a,b){return!a||"_"==a.substr(0,1)||"on"==a.substr(0,2)||0<="script,meta,title".indexOf(b)||0==a.indexOf("siz")||0==a.indexOf("jQuery")?!1:!0}var c=
a.propertyName;a=a.srcElement;if(b(c,a.tagName)){if("innerHTML"==c||"innerText"==c||"outerHTML"==c){if("outerHTML"==c||a.tagName&&"select"==a.tagName.toLowerCase())a=a.parentNode;this.pb.push(a,function(a,b){return function(c){var d=q.H.nc(b,!1,a.j.B),f=t.J.T(b),e=a.j.D();e&&(e.log(a.j,n.A.G.Ec,{x:e.Y(a.j,f),h:q.O.Ic(b.innerHTML,a.j.B.document,f,d)},null,c),a.Jd(b));a=b=null}}(this,a),!1)}else if("on"!=c.substring(0,2)){for(var f=0;f<P.La.length;f++)if(c==P.La[f])return;if("_"==c.substr(0,1)||"on"==
c.substr(0,2).toLowerCase())return;var e=a[c];-1<c.indexOf("style.")&&(c="style",e=a.getAttribute(c));this.pb.push(a,function(a,b){return function(f){q.H.nc(b,!1,a.B);if("style"==c){var g=b.getAttribute("style")||b.style;g&&g.cssText&&(e=g.cssText)}var g=a.D(),h=t.J.T;if(g)if("style"==c){e&&"null"!=e||(e="");var h=g.Y(a,h(b,null,"id"==c)),l=b.getAttribute("style")||b.style;l&&g.log(a,n.A.G.Zb,{a:c,v:"undefined"!=typeof l.cssText?l.cssText:l+"",x:h},null,f)}else l=b.tagName.toLowerCase(),"object"==
typeof e||("value"==c&&"input"==l&&"hidden"==b.type||d.$c(l,["input","select","textarea"])&&!d.$c(c,"className cols rows class width height align".split(" ")))||g.log(a,n.A.G.Zb,{a:c,v:e+"",x:g.Y(a,h(b,null,"id"==c))},null,f);b=a=null}}(this.j,a),!0,c)}b=null}a=a=null};d.f.u.ca.prototype.ri=function(a){return function(b){b=b.originalTarget||b.target;var c=b.tagName?b.tagName.toLowerCase():"";-1==P.Kf.indexOf(c)?a.pb.push(b,function(a,b){return function(c){b.ph(a,c)}}(b,a)):"select"==c&&null!=b._fsrTracker&&
a.pb.push(b,function(a){return function(){a._fsrTracker.yb()}}(b))}};d.f.u.ca.prototype.pi=function(a,b){return function(c){var d=c.attrName,e=c.newValue;c=c.originalTarget;"on"==d.substr(0,2)||0<="script,meta,title".indexOf(c.tagName)||(b.pb.push(c,function(b){return function(c){var g=a.D(),h=t.J.T,l=n.A;if(g)if("style"==d){var h=g.Y(a,h(b,null,"id"==d)),m=b.getAttribute("style")||b.style;m&&g.log(a,l.G.Zb,{a:d,v:"undefined"!=typeof m.cssText?m.cssText:m+"",x:h},null,c)}else"value"==d&&"input"==
b.tagName.toLowerCase()&&"hidden"==b.type||"on"==d.substr(0,2).toLowerCase()||(h=g.Y(a,h(b,null,"id"==d)),g.log(a,l.G.Zb,{a:d,v:e,x:h},null,c))}}(c,d),!0,d),c=null)}};d.f.u.ca.prototype.ph=function(a,b){var c=this.j,f=d.f;3==a.nodeType&&(a=a.parentNode);var e=t.J.T(a);if(0==e[0].indexOf("html")){var k=f.$b.H.nc(a,!1,c.B),k=a.innerHTML?f.$b.O.Ic(a.innerHTML,c.B.document,e,k):{},g=c.D();g&&(g.log(c,f.Fe.A.G.Ec,{x:g.Y(c,e),h:k},null,b),this.Jd(a))}};d.f.u.ca.prototype.Jd=function(a){var b=a.tagName?
a.tagName.toLowerCase():"";1!=a.nodeType||d.$c(b,d.f.u.ca.Kf)||this.j.hb.U(a)};d.f.u.ca.prototype.ve=function(a){a=C(P.Hg,a,!0);for(var b=function(a){return function(b){a.zj(b)}}(this),c=a.length-1;0<=c;c--){var d=a[c];na(d,"propertychange",b,!0,!0);if(!this.xe){var e;if(!d.getAttribute("_fsrRewrite")){d.setAttribute("_fsrRewrite","true");for(var g=P.La.length-1;0<=g;g--)e=P.La[g],d[e]=this.pc[e]}}d=null}b=a=a=null};d.f.u.ca.prototype.Ui=function(a,b){return function(){var c=this.parentNode,d=b.Uc[a];
if(d){var e;if("BODY"==this.tagName)try{e=d(arguments[0],arguments[1])}catch(g){e=d.apply(this,arguments)}else if("IE"==x.W&&7==x.version)try{e=d.apply(this,arguments)}catch(h){try{e=d.call(this,arguments[0],arguments[1])}catch(l){e=d(arguments[0],arguments[1])}}else e=d.apply(this,arguments);c&&1==c.nodeType&&b.pb.push(c,function(a,b){return function(c){var d=q.H.nc(b,!1,a.j.B),f=t.J.T(b),e=a.j.D();e&&(e.log(a.j,n.A.G.Ec,{x:e.Y(a.j,f),h:q.O.Ic(b.innerHTML,a.j.B.document,f,d)},null,c),a.Jd(b));a=
b=null}}(b,c),!1);return e}}};d.f.u.ca.prototype.Gj=function(){for(var a,b=P.La.length-1;0<=b;b--)a=P.La[b],this.Uc[a]=this.j.B.document.body[a],this.pc[a]=this.Ui(a,this)};d.f.u.ca.prototype.ej=function(){for(var a=this.j.B,b=d.q(a.Element)?a.Element.prototype:void 0,c=d.q(a.HTMLElement)?a.HTMLElement.prototype:void 0,f=P.La.length-1;0<=f;f--)a=P.La[f],b&&-1<(b[a]+"").indexOf("native")&&(b[a]=this.pc[a]),c&&-1<(c[a]+"").indexOf("native")&&(c[a]=this.pc[a])};d.f.u.ca.prototype.Dj=function(){var a;
a=this.j.B;for(var b=d.q(a.Element)?a.Element.prototype:void 0,c=d.q(a.HTMLElement)?a.HTMLElement.prototype:void 0,f=P.La.length-1;0<=f;f--)a=P.La[f],b&&b[a]&&delete b[a],c&&c[a]&&delete c[a]};d.f.u.ca.prototype.N=function(){d.Na(this.Uc);this.xe&&d.P&&this.Dj();this.pc=this.Uc=null;this.pb.N();this.Ig&&this.Ig.N();this.j=null};d.f.u.Gb=function(a){this.j=a;this.eb=[];this.Cc=["src","style","id","class"];this.Zi=d.f.u.ca.Vf.Hg+", iframe, button, textarea";this.Re(a.B.document);a.hb.Ma(function(a){return function(c){0<
c.childNodes.length&&a.Re(c)}}(this));this.Te()};d.f.u.Gb.prototype.Te=function(){for(var a=new Date,b=0;b<this.eb.length;b++){var c=this.eb[b];if(c.cb){var f=this.Ve(c.cb),e=this.Oh(c.kd,f);if(e&&0<e.length){for(var g=0;g<e.length;g++){var h;"class"==e[g].oc.toLowerCase()&&-1==e[g].Ae.indexOf("fsr")&&q.H.nc(c.cb,!0,this.j.B);h="id"==e[g].oc.toLowerCase()?t.J.T(c.cb,[],!0):t.J.T(c.cb);if(0==h[0].indexOf("html")){if(!d)return;this.j.D().log(this.j,n.A.G.Zb,{a:e[g].oc,v:e[g].Ae,x:this.j.D().Y(this.j,
h)})}}c.kd=f}}else this.eb.splice(b--,1)}a=new Date-a;b=500;150<a&&(b=750);500<a&&(b=2E3);1E3<a&&(b=1E4);this.$i=setTimeout(function(a){return function(){a.Te()}}(this),b)};d.f.u.Gb.prototype.Ve=function(a){for(var b={},c=0;c<this.Cc.length;c++){var f=a.getAttribute(this.Cc[c]);if(!d)return;d.q(f)&&(b[this.Cc[c]]=f.toString())}return b};d.f.u.Gb.prototype.Oh=function(a,b){for(var c=[],f=0;f<this.Cc.length;f++){var e=this.Cc[f];if(!d)return;var g=d.q(a[e]),h=d.q(b[e]);if(g&&h)a[e].toString()!=b[e].toString()&&
(c[c.length]={oc:e,Ae:b[e]||""});else if(!g&&h||g&&!h)c[c.length]={oc:e,Ae:b[e]||""}}return c};d.f.u.Gb.prototype.Re=function(a){var b=C(this.Zi,a);if(a.documentElement){for(var c=[],d=0;d<b.length;d++)c.push(b[d]);c.push(a.documentElement);b=c}for(d=0;d<b.length;d++){a=b[d];for(var c={kd:this.Ve(a),cb:a},e={cb:null},g=0;g<this.eb.length;g++)if(this.eb[g].cb==a){e=this.eb[g];break}e.cb!=a?this.eb[this.eb.length]=c:e.kd=c.kd}};d.f.u.Gb.prototype.N=function(){clearTimeout(this.$i)};d.f.R.Og={};var Ba=
d.f.R.Og;n.Dc={mh:[],Vh:0};n.Dc.Af=function(a,b){b&&b.join&&(b=b.join(","));for(var c={},f=n.Dc,e=f.mh,g=e.length-1;0<=g;g--)if(e[g].str==a){c.uid=e[g].uid;break}if(!d.q(c.uid)){c.uid="_"+f.Vh++;if((c.kl=b)&&100<a.length)for(g=e.length-1;0<=g;g--)if(e[g].kl==b){f=n.Qg(e[g].str,a);c.diff={uid:e[g].uid,d:f};break}d.q(c.diff)?e[e.length]={str:a,uid:c.uid,kl:c.kl}:(c.str=a,e[e.length]=c)}return c};n.A={};n.A.Pd=[];n.A.G={Ee:0,jh:1,Tg:2,Sg:3,$g:4,Hd:5,Wg:6,He:7,Ie:8,Lg:9,Qe:10,Ec:11,Zb:12,Xg:13,Je:14,
Zg:15,ck:16,eh:17,Pg:18,Me:19,ah:20,Ng:21,dh:22,kh:23,ih:24,Vg:25};n.A.Q=function(a){this.I=d.f.$a;this.j=a;this.ye=this.$d=0;this.ke=d.now()-1E4;this.ta=this.wa=this.Zc=!1;this.ud=m.replay_id;this.domain=m.site.domain;this.ek=new E;this.Ld={};this.Nd();this.log(a,n.A.G.eh,{dtm:d.now(),ofs:(new Date).getTimezoneOffset(),v:x.W,dv:x.version,sid:m.replay_id,r:m.renderer,l:m.layout,m:x.Sa},-1);D(l,x.Sa?"unload":"beforeunload",function(a){return function(){a.ii()}}(this));if(this.ug=g.aa.isSupported())this.ic=
new g.aa(this.ud,!1);this.Qa=new g.fa(this.ud,!1);this.Uh();this.Th();this.Sh();this.Mj();this.Nj();a.ac.Ma(function(a){return function(){a.Hb(!0)}}(this));v.hc.Ci({Cg:[v.hc.Oe.gh,v.hc.Oe.Mg],Ua:function(a){return function(){a.Ad=this;a.wa&&a.gb();a.Hb()}}(this),Ka:function(a){return function(){n.ra.set("rtcr",3);n.ra.set("rtcp",g.n("pv"));a.ha()}}(this)});a=null};n.A.Q.prototype.ii=function(){this.Id();this.wa&&this.ff();this.Qa&&(this.Qa.set(this.I.fc,this.ga),this.Qa.qd(this.M),this.Qa.Aa());this.ic&&
(this.ic.set(this.I.fc,this.ga),this.ic.qd(this.M),this.ic.Aa())};n.A.Q.prototype.N=function(){this.Id();this.j.ac.Eg();this.j.ac=null;v.Ha.N();this.sg();this.rg()};n.A.Q.prototype.zb=function(){this.wa=!0;this.ga[this.I.ka]=!0;g.n(this.I.ka,!0);this.Hb();this.gb()};n.A.Q.prototype.sd=function(){this.wd();this.wa=!1;this.ga[this.I.ka]=!1;g.n(this.I.ka,!1)};n.A.Q.prototype.ha=function(a){this.sd();this.rg();this.sg();this.ta=!0;this.ga[this.I.pa]=!0;g.n(this.I.pa,!0);a&&g.n(_this.I.Cd,this.cancel);
this.Nd()};n.A.Q.prototype.Uh=function(){var a=this.Qa,b=this.ic;if(this.ug&&a.la&&!b.la){var c=b.get(this.I.fc);a.set(this.I.fc,c);b=b.getBlob();a.qd(b)}};n.A.Q.prototype.Th=function(){var a=d.Ga,b=this.Qa;this.Qa.la?(this.ga={},this.Vb=a.Gd.qc(),this.ga[this.I.Ne]=this.Vb,(b=a.ya.n(this.I.bc))||(b=a.Gd.qc()),this.Nb=b,this.ga[this.I.bc]=this.Nb,a.ya.n(this.I.bc,this.Nb),this.wa=!!a.ya.n(this.I.ka),this.ga[this.I.ka]=this.wa,a.ya.n(this.I.ka,this.wa),this.ta=!!a.ya.n(this.I.pa),this.ga[this.I.pa]=
this.ta,a.ya.n(this.I.pa,this.ta)):(this.ga=b.get(this.I.fc),this.Vb=this.ga[this.I.Ne],this.Nb=this.ga[this.I.bc],a.ya.n(this.I.bc,this.Nb),b=a.ya.n(this.I.ka),d.q(b)?(this.wa=b,this.ga[this.I.ka]=this.wa):(this.wa=this.ga[this.I.ka],a.ya.n(this.I.ka,this.wa)),b=a.ya.n(this.I.pa),d.q(b)?(this.ta=b,this.ga[this.I.pa]=this.ta):(this.ta=this.ga[this.I.pa],a.ya.n(this.I.pa,this.ta)))};n.A.Q.prototype.Sh=function(){this.M=this.Qa.getBlob()};n.A.Q.prototype.Hb=function(a){if(this.Qa&&!this.ta){var b=this.Db();
if(this.wa&&(5E3<d.now()-this.ke||a)||b)this.Id(),this.ff()}};n.A.Q.prototype.ff=function(){if(d.q(this.Ad)&&0!=this.M.length)if(this.Zc&&1E4>d.now()-this.ke)this.ce=!0;else{this.Zc=!0;this.ce=!1;this.ke=d.now();var a=new this.Ad,b=a instanceof v.Xa?"corsservice":"amfservice/amf";n.ra.set("rtp",b.substr(0,1));var c=this.ud.replace(/[- _.&]/g,"").toLowerCase(),f="datalen:"+this.M.length+",time:"+d.now(),e=d.Mb;n.ra.ad("rta");a.send({action:"data",contentType:"text/plain;charset=UTF-8",encoding:e(q.Bf()),
version:d.f.version,zf:this.Nb,od:this.Vb,domain:this.domain||l.location.hostname,td:c,url:d.f.R.Bg+b+"?action=data&metadata="+e(f)+"&encoding="+e(q.Bf())+"&session_id="+e(this.Vb)+"&global_session_id="+e(this.Nb)+"&domain="+e(this.domain)+"&site_id="+e(c)+"&version="+e(d.f.version)+"&cachebust="+Math.random(),yc:!0,data:'{"data":['+this.M+"]}",Ka:function(a){return function(b){n.ra.ad("rtf");a.$d++;a.Zc=!1;try{b=J.parse(b)}catch(c){}d.q(b.status)&&n.ra.ad("rtf"+Math.abs(parseInt(b.status)));10<a.$d?
(n.ra.set("rtcr",1),n.ra.set("rtcp",g.n("pv")),a.ha()):this.ce&&setTimeout(function(){a.Hb(!0)},100)}}(this),oa:function(a,b,c){return function(f){n.ra.ad("rts");a.$d=0;a.Zc=!1;try{f=J.parse(f)}catch(e){}if(d.q(f.status)){f=parseInt(f.status);if(1==f)b==a.ye?a.M="":a.M.substr(0,c.length)==c&&(a.M=a.M.substr(c.length));else if(2==f){n.ra.set("rtcr",2);n.ra.set("rtcp",g.n("pv"));a.ha();return}this.ce&&setTimeout(function(){a.Hb(!0)},100)}}}(this,this.ye,this.M+"")});e=a=null}};n.A.Q.prototype.bf=function(){if(d.q(this.Ad)&&
!this.ta){var a=this.ud.replace(/[- _.&]/g,"").toLowerCase(),b=new this.Ad,c=d.Mb;b.send({action:"ping",contentType:"text/plain;charset=UTF-8",od:this.Vb,td:a,url:d.f.R.Bg+(b instanceof v.Xa?"corsservice":"amfservice/amf")+"?action=ping&session_id="+c(this.Vb)+"&site_id="+c(a)+"&cachebust="+Math.random(),yc:!0,data:"",Ka:d.ua,oa:d.ua})}};n.A.Q.prototype.Nd=function(){this.mc={start:d.now(),log:[],guid:u.qc()};this.Pa=0};n.A.Q.prototype.Id=function(){if(0!=this.mc.log.length){this.ye++;var a=d.stringify(this.mc);
a&&0<a.length&&(this.M=this.M&&0<this.M.length?this.M+","+a:a);this.Nd()}};n.A.Q.prototype.Y=function(a,b){if(!this.ta){var c=n.A;b=b.join(",");if(!d.q(this.Ld[b])){c.Pd[c.Pd.length]=b;var f=c.Pd.length-1;this.Ld[b]=f;this.log(a,c.G.jh,{idx:f+"",xp:b},f)}return this.Ld[b]}};n.A.Q.prototype.log=function(a,b,c,f,e){if(!this.ta){var g=0;e||(e=0);a={x:null==f?this.Y(a,a.ae()):f,e:b,d:c,t:d.now()-this.mc.start-e};null!=a&&(f=n.A,this.mc.log[this.mc.log.length]=a,b==f.G.Ee?c.dom.str&&(g=c.dom.str.length,
this.Pa+=g):b==f.G.Ng?c.stylesheet?(g=c.stylesheet.length,this.Pa+=g):c.v&&(g=Math.round(1*c.v.length),this.Pa+=g):(b==f.G.Ec?c.h.str||c.h.diff?(g=Math.round(1*((c.h.str?c.h.str.length:0)+(c.h.diff?c.h.diff.d.r.length:0)+(c.h.kl?c.h.kl.length:0)+(c.h.uid?c.h.uid.length:0))),g+=50):g=84:g=60,this.Pa+=g))}};n.A.Q.prototype.Db=function(){return 3E6<this.M.length+this.Pa?!0:!1};n.A.Q.prototype.Ch=function(){var a=g.n(this.I.pa);d.q(a)&&this.ta!=a&&a?this.ha():(a=g.n(this.I.ka),d.q(a)&&this.wa!=a&&(a?
this.zb():this.sd()))};n.A.Q.prototype.gb=function(){this.Yc||(this.bf(),this.Yc=setInterval(function(a){return function(){a.bf()}}(this),6E4))};n.A.Q.prototype.wd=function(){this.Yc&&(clearInterval(this.Yc),delete this.Yc)};n.A.Q.prototype.Mj=function(){this.vd||(this.vd=setInterval(function(a){return function(){a.Ch()}}(this),7500))};n.A.Q.prototype.rg=function(){this.vd&&(clearInterval(this.vd),delete this.vd)};n.A.Q.prototype.Nj=function(){this.yd||(this.yd=setInterval(function(a){return function(){a.Hb()}}(this),
x.Sa?15E3:3E4))};n.A.Q.prototype.sg=function(){this.yd&&(clearInterval(this.yd),delete this.yd)};q.H={Xd:[],Wc:[],Vc:[],ge:!1,fd:[]};q.H.af=function(a,b){for(var c=C(a,b.document),d=A,e=0;e<c.length;e++){var g=c[e];if(-1<g.nodeName.toLowerCase().indexOf("body"))g.insertBefore(d.createComment("fsrHiddenBlockStart"),g.firstChild),g.appendChild(d.createComment("fsrHiddenBlockEnd"));else{var h=g.parentNode;h.insertBefore(d.createComment("fsrHiddenBlockStart"),g);h.insertBefore(d.createComment("fsrHiddenBlockEnd"),
g.nextSibling)}}};q.H.gf=function(a,b){t.za(C(a,b.document),"fsrVisible")};q.H.Dh=function(a,b){t.za(C(a,b.document),"fsrObscure")};q.H.$e=function(a,b,c){var d=q.H;c?d.gf(a,b):d.af(a,b);if(0<a.length)for("string"==typeof a&&(a=[a]),b=c?d.Wc:d.Xd,c=0;c<a.length;c++)b[b.length]=a[c]};q.H.Wh=function(a,b){return q.H.$e(a,b,!0)};q.H.zh=function(a,b){t.za(C(a,b.document),"fsrHidden");if(0<a.length){"string"==typeof a&&(a=[a]);for(var c=q.H.Vc,d=0;d<a.length;d++)c[c.length]=a[d]}};q.H.Jh=function(a,b){t.md(C(a,
b.document),"fsrVisible")};q.H.ik=function(a){q.H.fd[q.H.fd.length]=a};q.H.Rf=function(a){var b=!0;if(0<q.H.fd.length&&a.className)for(var c=q.H.fd,d=0;d<c.length;d++)-1<a.className.indexOf(c[d])&&(b=!1),d=null;return 1>a.childNodes.length||!b||a.childNodes[0].data&&-1<a.childNodes[0].data.indexOf("fsrHiddenBlockStart")?!0:!1};q.H.nc=function(a,b,c){var d=q,e=!1;if(!d.H.ge){d.H.ge=!0;if(0<d.H.Xd.length&&!b){b=C(d.H.Xd.join(","),c.document);for(var g=0,h=b.length;g<h;g++)if(d.Xf(a,b[g])||b[g]==a){if(!d.H.Rf(a)){e=
!0;break}}else d.Xf(b[g],a)&&!d.H.Rf(b[g])&&(b[g].insertBefore(A.createComment("fsrHiddenBlockStart"),b[g].firstChild),b[g].appendChild(A.createComment("fsrHiddenBlockEnd")))}var m;if(c!=l)try{m=c.document}catch(n){return}else m=c.document;if(0<d.H.Wc.length)for(g=d.H.Wc.length-1;0<=g;g--)t.za(C(d.H.Wc[g],m),"fsrVisible");if(0<d.H.Vc.length)for(g=d.H.Vc.length-1;0<=g;g--)t.za(C(d.H.Vc[g],m),"fsrHidden");d.H.ge=!1}return e};q.H.Qi=function(a,b){function c(a,b,c){for(var g in a)-1<d.indexOf(g)&&b(a[g],
c)}var d=(b.location.href||"about:blank").toLowerCase();c(a.Oj,this.af,b);c(a.ti,this.$e,b);c(a.Pj,this.gf,b);c(a.ui,this.Wh,b);c(a.Zh,this.zh,b);c(a.Ej,this.Jh,b);c(a.aj,this.Dh,b)};q.O=function(a,b){var c=q.O,d=a.document;return c.Ic(c.xh(d)+t.outerHTML(C("html",d)[0]),d,b,!1)};var ja=q.O;q.O.Ic=function(a,b,c,f){var e=q.O;if(d.P){b=C("object",b);for(var g=0;g<b.length;g++)a=e.sh(a,b[g])}return n.Dc.Af(e.Mh(a,f),c)};q.O.Mh=function(a,b){var c=q.O;a=c.th(a);a=c.uh(a);a=c.Qh(a);return a=c.Ah(a,b)};
q.O.Ah=function(a,b){return b?this.Nc(a,RegExp(".+","mig"),ja.Ze):0<=a.indexOf("fsrHiddenBlock")?q.O.Nc(a,RegExp("\x3c!--(\\W)*fsrHiddenBlockStart[\\w\\W]*?fsrHiddenBlockEnd(\\W)*--\x3e","mig"),ja.Ze):a};q.O.th=function(a){return-1<a.indexOf("/pre")||-1<a.indexOf("/PRE")?ja.Nc(a,RegExp("[\\s\\S]*?(?:\\/pre\\s*>)|[\\s\\S]+","mig"),ja.Ih):a};q.O.uh=function(a){return-1<a.indexOf("VIEWSTATE")?a.replace(/<input[^>]*name=["']?__VIEWSTATE[^>]*>/gim,'<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" />'):
a};q.O.Qh=function(a){a=a||"";return/select/mig.test(a)?ja.Nc(a,/(<select[^>]*>)[\s\S]*?(<\/[\w]*select>)/mig,function(a){var c=a[0].match(/<select[^>]*>/mig);return a[0].substr(0,a[0].lastIndexOf(c[c.length-1]))+c[c.length-1]+a[2]}):a};q.O.sh=function(a,b){return a.replace(RegExp("(<object[^>]*id=['\"]?"+b.id+"['\"]?.*?>)([\\s\\S]*?)(</object>)","im"),function(a,d,e,g){e=a="";for(var h=0;h<b.childNodes.length;h++){var l=b.childNodes[h];"PARAM"==l.tagName?a+=l.outerHTML:e+=l.outerHTML}return d+a+
e+g})};q.O.Nc=function(a,b,c){for(var d,e="",g=0;null!=(d=b.exec(a));)e+=a.substring(g,d.index),g=c(d),e+=g,g=d.index+d[0].length;return e+=a.substring(g,a.length)};q.O.Ze=function(a){a=a[0];if(0<a.length){res="";for(var b=-1,c=-1;b<a.length;)c=b,b=a.indexOf("<",b+1),-1<b?(res+=a.substring(c,b).replace("<","").replace(">","").replace(/[^\W]/g,"*"),res+=a.substring(b,a.indexOf(">",b)+1),b=a.indexOf(">",b)):b=a.length;res+=a.substring(c,a.length).replace("<","").replace(">","").replace(/[^\W]/g,"*")}return res};
q.O.Ih=function(a){var b=0,c=a[0];c&&(c="",b=q.O.Hh(a[0]),c+=a[0].substring(0,b).replace(/\t/g," ").replace(/\s+/g," "),c+=a[0].substring(b));return c};q.O.Hh=function(a){a=a.substring(0).search(RegExp("<\\s*?pre","mig"));return 0<=a?a+0:a};q.O.hk=function(a,b,c){for(var d=q.O.Cj(a,b,c);-1<d;)c=d,d=q.O.Cj(a,b,c+1);return c};q.O.xh=function(a){var b="";if(d.P&&("CSS1Compat"!=a.compatMode||5==a.documentMode))return b;var c=a.doctype;if(c)b="<!DOCTYPE HTML",c.publicId&&(b=b+' PUBLIC "'+c.publicId+'"'),
c.systemId&&(b=b+' "'+c.systemId+'"'),b+=">";else if(a=a.childNodes,c=0,a[c].text){for(;a[c].text&&(0==a[c].text.indexOf("\x3c!--")||0==a[c].text.indexOf("<?xml"));)c++;cdt=a[c].text.toLowerCase();0==cdt.indexOf("<!doctype")&&(b=a[c].text)}return b};d.f.da=function(a,b,c){this.B=a;this.Rc=[];this.uf=!1;this.hb=new E;this.hb.Ma(function(a){return function(){a.update.apply(a,arguments)}}(this));this.ac=new E;this.Mf=b||[];this.ab=!1;this.Qb=this.rb=this.fb=null;if(this.B.document){this.fb=c;c||(this.Qb=
new n.A.Q(this));a=Ba.zk;if(d.q(a)&&(a=a.apply(this))){this.B==this.B.top&&this.D().log(this,n.A.G.ah,{v:a},-1);return}q.H.Qi(d.f.R.gj,this.B);this.og();if(!c)var f=Ba.yk;d.q(f)&&f.apply(this);this.rb=new d.f.u(this);d.f.u.qa.$f(this)}b=a=c=null};d.f.da.prototype.update=function(a){a||(a=this.oi.body);0!=a.childNodes.length&&(this.rb.Gg(a),d.f.u.qa.$f(this,a))};d.f.da.prototype.D=function(){return this.fb?this.fb.D():this.Qb};d.f.da.prototype.ae=function(){this.Ue||(this.Ue=this.fb?this.Mf.concat(this.fb.ae()):
this.Mf);return this.Ue};d.f.da.prototype.tb=function(){return this.fb?this.fb.tb():this};d.f.da.prototype.zb=function(){this.D().zb()};d.f.da.prototype.sd=function(){this.D().sd()};d.f.da.prototype.ha=function(a){this.D().ha(a)};d.f.da.prototype.N=function(){var a=this;a.B||(a=a.j);if(!this.uf){this.uf=!0;for(var b=0;b<a.Rc.length;b++)a.Rc[b].N();a.rb&&a.rb.N();a.je&&(a.je=null);a.rb=null;a.B=null;a.fb=null;a.hb.Eg();a.hb=null;a.Qb&&(a.Qb.N(),a.Qb=null)}};d.f.da.prototype.og=function(){if(this.D()){var a;
this.B==this.B.top&&(a=this.B.document.body,a=n.Dc.Af(a.innerHTML,t.J.T(a)).uid);this.D().log(this,n.A.G.Ee,{dom:q.O(this.B,this.ae()),url:this.B.location.href.toString(),buid:a,start:d.startTime,domloadtime:d.now()-d.startTime})}};d.f.da.isSupported=function(){var a=!d.q(m.enabled)||m.enabled&&m.sessionreplay,b=d.f.R.pe[x.L.toLowerCase()],c=d.f.R.Ja?d.match(d.f.R.Ja):!1,f=!(h.C.va<h.C.V.jb),e=d.f.Pf();x.Sf||(b=!1);return a&&b&&!c&&f&&!e};d.f.da.Rg=function(){var a=d.f;a.j&&a.j.N();a.j=null};d.Ua(function(){if("http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/site.com"==
m.replay_id)alert("ForeSee: replay_id has not been updated.");else{h.C.uc();var a=l==l.top,b=d.f;!d.j&&a&&b.da.isSupported()&&"undefined"==typeof robotreplay?(b.recorder=b.j=new b.da(l),D(l,"unload",d.f.da.Rg),d.f.zd&&b.recorder.zb()):"undefined"!=typeof robotreplay&&alert("There is a previous version of ForeSee SessionReplay running on this site.")}},d.f.R.qi)})(self,$$FSR);
})({});
}
// -------------------------------- DO NOT MODIFY ANYTHING BETWEEN THE DASHED LINES --------------------------------

/* ../global/Lowes/My/floodlight.js */

function floodlightInit(){
	var urlPath = document.location.pathname.split('?')[0];
	
	if (urlPath.indexOf('/webapp/wcs/stores/servlet') !== -1){
		urlPath = urlPath.split('/webapp/wcs/stores/servlet')[1];
	}
	
	// Setup pages and values
	var floodlightPages = {
			LowesMyLowesCardTemporaryPrintView : [ 'https', 'fy11m873', '149741' ],
			UserRegistrationForm : ['https', 'fy11m214', '149816' ],
			MyLowesLanding : [ 'https', 'fy11m198', '149733' ],
			LowesMyLowesCardRegistrationView : [ 'https', 'fy11m042', '149734' ],
			LowesMyLowesCardRequest : [ 'https', 'fy11m012', '149736' ]
	};

	// Check for Page object
	var flood = floodlightPages[urlPath.substr(1)] || '';	
	
	if (typeof flood == 'object'){
		// Check for page object found,
		floodlightInsert(flood[0], flood[1], flood[2]);
		return true; // STOP HERE
	} 
	
	// Custom Page calls
/*
	switch (urlPath){
		case '/UserAccountLanding': 
			if ($('#ConfirmationMessageDisplay').length){
				if ($('.keyfobSuccess').length){
					floodlightInsert('https','fy11m042','149735');
				} else {
					var regSuccess = $('#ConfirmationMessageDisplay').children('p').text();
					if (regSuccess.indexOf('registration was successful') > -1){
						floodlightInsert('https','fy11m645','149820');
					}
				}
			}
		break;
	}
*/
	return '';	
}

function floodlightInsert(ssl,ddCat,mmActivity){
	var randomNumber = Math.random(),
		floodlightDC = randomNumber * 10000000000000,
		floodlightMM = randomNumber * 1000000;
		
	/*$('body').prepend('<iframe src="'+ssl+'://fls.doubleclick.net/activityi;src=1160694;type=fy11m164;cat='+ddCat+';ord='+floodlightDC+'?" width="1" height="1" frameborder="0" style="display:none"></iframe>');*/
		$.getScript(ssl+'://bs.serving-sys.com/BurstingPipe/ActivityServer.bs?cn=as&ActivityID='+mmActivity+'&rnd='+floodlightMM);
	
}


// on page load init floodlight
$(document).ready(function(){
	floodlightInit();
});

/* ../foresee/foresee-cookie.js */

/* DO NOT LINT */
;(function(){
	if (!Lowes.Cookie.get("lowes_TLTSID"))
	{
		var foreseeID = Lowes.Cookie.get("TLTSID");
		if(!!foreseeID){
			foreseeID = (foreseeID instanceof Array) ? foreseeID[0] : foreseeID;
			Lowes.Cookie.set('lowes_TLTSID',foreseeID);
		}
	}
})();

/* ../global/minicart/minicart.js */

/** @fileOverview MiniCart */

(function(){
	// Grab Lowes namespace object or create a new one.
	var Lowes = window.Lowes || {};

	/**
	*	Used to Display MiniCart modal
	*
	*	@object Lowes.MiniCart
	*
	* @public {Boolean} fetchInBkg if true, minicart fetches the cart contents immediately after click of atc; false, minicart fetches cart contents on hover of cart in masthead
	* @public {Boolean} isActive True if minicart is running, false if not initialized (Used to bind to add to cart buttons the old way if minicart is turned off for any reason)
	* @public {Boolean} isFetched True if cart data is cached, false if not or invalid
	* @public {Boolean} isNewContent True if the fetched/cached content has not been displayed
	* @public {Number} itemLimit Max number of items to display in minicart (most recent first)
	* @public {String} totalItemsInCart Default: 0; Total Line Items in cart
	* @public {Array} excludedPageIds Pages on which minicart should NOT load - html ID attribute
	* @public {Object} cartResponseCache JSON Object returned from Cart Display ajax call
	* @public {String} carouselTemplate,itemTemplate, addonTemplate, cartTemplate, errorTemplate HTML templates for ajax responses
	*/
	var MiniCart = {
		fetchInBkg : false,
		isActive : false,
		isFetched : false,
		isNewContent : false,
		isLoading : false,
		itemLimit : 11,
		totalItemsInCart : "0",
		checkoutURL : "/OrderItemDisplay?storeId=10151&langId=-1&catalogId=10051",
		addToCartURL : "/OrderItemAdd?",
		addToCartAjaxURL : "/webapp/wcs/stores/servlet/AjaxOrderItemAdd",
		removeFromCartAjaxURL : "/webapp/wcs/stores/servlet/AjaxOrderItemDelete",
		displayCartAjaxURL : "/webapp/wcs/stores/servlet/AjaxMiniCartDisplay",
		excludedPageIds: ['checkout-page','cart-page','checkout-login-page'],
		ignoreMiniCartIds: [],
		cartResponseCache : {
								"JSONStatus" : "success",
								"totalItemsInCart" : "0",
								"orderTotal" : null
							},
		carouselTemplate :	'<ul class="carousel">' +
							'</ul>',
		itemTemplate :	'<li>' +
						'	<div class="itemCount">' +
						'		<span class="currentItem"></span> of <span class="totalItems"></span>' +
						'	</div>' +
						'	<div class="item">' +
						'	<div class="productImgArea">' +
						'		<a class="productImgLink" title="View Details">' +
						'			<img class="productImg">' +
						'		</a>' +
						'	</div>' +
						'	<div class="titleArea">' +
						'	<h3 class="productTitle">' +
						'		<a></a>' +
						'	</h3>' +
						'	</div>' +
						' 	<div class="pricingArea">' +
						'		<p class="pricing"></p>' +
						'		<p class="quantity">' +
						'			Qty.: <span></span>' +
						'		</p>' +
						'	</div>' +
						'	<a class="removeItem" href="#">Remove</a>' +
						'</div></li>',
		addonTemplate : '<div class="addon">' +
						'	<h4></h4>' +
						'		<p class="pricing"></p>' +
						'	</div>',
		cartTemplate :	'<li class="more">' +
						'	<p><b><span class="remainingItems"></span> More Item(s)</b> in Your Cart</p>' +
						'		<a>View Cart</a>' +
						'</li>',
		errorTemplate : '<div class="ui-info-alert ui-messaging">' +
						'</div>',
		/**
		 * Initializes Minicart Dialogs & Notifications
		 */
		init : function () {
			Lowes.MiniCart.Dialog.init();
			if ($.inArray($('body').attr('id'),Lowes.MiniCart.excludedAddToCartPages) == -1 && !$('#error-page').length) {
				Lowes.MiniCart.Notifications.init();
			}
			if (!$('#mc-ui-block').size()) {
				$('body').append('<div id="mc-ui-block" class="ui-overlay" style="display:none;"><img src="../../images/ajax-ui-block-loader.gif"/*tpa=http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/images/ajax-ui-block-loader.gif*/ alt="Loading..." /></div>');
			}
			if (!$('#mc-block-overlay').size()) {
				$('body').append('<div id="mc-block-overlay" class="ui-widget-overlay" style="display:none;"></div>');
			}
			this.isActive = true;
		},
		/**
		 * Prompt Gets the contents of Minicart - cached or not
		 * @param {Boolean} forceUpdate force data call even if cached
		 */
		getCartContents : function (forceUpdate, sync) {
			var cartContents = {};
			if ((!MiniCart.isFetched && $('#nav-cart-count').text() > 0) || forceUpdate) {
				// get cart contents from back end
				cartContents = MiniCart.buildCartContents(MiniCart.fetchCartContents(sync));
			} else {
				cartContents = MiniCart.buildCartContents(MiniCart.cartResponseCache);
			}
			return cartContents;
		},
		/**
		 * Retrieves JSON object from back end
			-Sets cache
			-Updates Cart icon in masthead
		 * @return {Object} MiniCart Contents
		 */
		fetchCartContents : function(sync) {
			var ajaxResponse = {};
			MiniCart.isLoading = true;
			$.ajax({
				type : 'POST',
				dataType : 'json',
				url : MiniCart.displayCartAjaxURL,
				async : sync?false:true,
				success : function(data) {
					ajaxResponse = MiniCart.cartResponseCache = data.minicartJSON;
					MiniCart.isFetched = true;
					MiniCart.isNewContent = true;
					MiniCart.updateCartInfo(!ajaxResponse.totalQuantityInCart?0:ajaxResponse.totalQuantityInCart,Lowes.Utils.formatCurrency(parseFloat(ajaxResponse.orderTotal)));
					if(data.totalItemsInCart == 0) {
						MiniCart.isFetched = false;
					}
				},
				complete: function() {
					MiniCart.isLoading = false;
				},
				error : function() {
					MiniCart.isFetched = false;
					MiniCart.isNewContent = true;
					MiniCart.cartResponseCache = {"JSONStatus":"fail"};
				}
			});
			return ajaxResponse;
		},
		buildCartContents : function(cartObj) {
			if (typeof cartObj == 'object') {
				var $cartCarousel = $(MiniCart.carouselTemplate);
				if (cartObj.JSONStatus == "success") {
					var cartItems = cartObj.products,
						cartItemCount = $(cartItems).size(),
						cartItemLimit = MiniCart.itemLimit,
						itemHTML = '',
						currentItemIndex = 0,
						addCartLink = true;
					MiniCart.totalItemsInCart = cartObj.totalItemsInCart;
					if (MiniCart.totalItemsInCart > 0) {
						if (cartObj.checkoutURL != '') {
							MiniCart.checkoutURL = cartObj.checkoutURL;
						}
						$.each(cartItems, function(index,product) {
							currentItemIndex++;
							var $itemTemplate = $(MiniCart.itemTemplate),
								$installTemplate = $(MiniCart.addonTemplate),
								$eppTemplate = $(MiniCart.addonTemplate),
								addons = '',
								installation = {},
								epp = {};
							$itemTemplate
								.attr('id','cartItem_'+index)
								.attr('data-orderid',index)
								.find('.productTitle a').text(product.name).end()
								.find('.quantity span').text(parseFloat(product.quantity).toFixed(0)).end()
								.find('.productImg').attr('src',product.imageURL).end()
								.find('.pricing').text(Lowes.Utils.formatCurrency(parseFloat(product.price))).end()
								.find('.currentItem').text(currentItemIndex).end()
								.find('.totalItems').text(cartItemCount).end()
								.find('.productImgLink,.productTitle a').attr('href',product.detailPageURL);
							if (typeof product.epp != 'undefined' && product.epp != null) {
								epp = product.epp;
								$eppTemplate
									.find('h4')
									.text(epp.name).end()
									.find('.pricing').text(Lowes.Utils.formatCurrency(parseFloat(epp.price)));
								$itemTemplate.find('.item').append($eppTemplate);
							}
							if (typeof product.installation != 'undefined' && product.installation != null) {
								installation = product.installation;
								$installTemplate
									.find('h4')
									.text(installation.name).end()
									.find('.pricing')
									.text(installation.price);
								$itemTemplate.find('.item').append($installTemplate);
							}
							if (product.isBogoItem == "true") {
								$itemTemplate.find('.removeItem').replaceWith('<span class="removeMsg">Item automatically added as part of offer.</span>');
							}
							if (currentItemIndex > cartItemLimit) {
								if ((currentItemIndex) % 3 == 0 && currentItemIndex == MiniCart.cartResponseCache.totalItemsInCart) {
									$cartCarousel.append($itemTemplate);
									addCartLink = false;
								}
								return false;
							} else {
								$cartCarousel.append($itemTemplate);
							}
						});
						$cartCarousel.find('.totalItems').text(currentItemIndex).end().find('li').last().addClass('last');
						if (MiniCart.cartResponseCache.totalItemsInCart > cartItemLimit && addCartLink) {
							var $cartTemplate = $(MiniCart.cartTemplate),
								cartResponse = MiniCart.cartResponseCache,
								remainingItems = parseInt(cartResponse.totalItemsInCart) - cartItemLimit;
							$cartTemplate.find('.remainingItems').text(remainingItems).end()
								.find('a').attr('href',MiniCart.checkoutURL);
							$cartCarousel.append($cartTemplate);
						}
					} else {
						$cartCarousel.append('<li class="empty"><h3>Your cart is empty.</h3><p class="emptyMsg">Continue shopping to add items to your cart.</p></li>');
					}
					if (currentItemIndex < 4) {
						$cartCarousel.removeClass('carousel').addClass('noCarousel');
					}
				} else {
					var $errorTemplate = $(Lowes.MiniCart.errorTemplate);
					$errorTemplate.html('<p>Cart information not available at this time</p><p><a href="' + $('#lowes-cart .nav-heading').attr('href') + '">View Cart</a></p>');
					$cartCarousel.append($('<li class="empty"></li>').append($errorTemplate));
					$cartCarousel.removeClass('carousel').addClass('noCarousel');
				}
				return $cartCarousel;
			}
		},
		updateCartInfo : function(quantity,total) {
			var cartCookie = Lowes.Cookie.get('SHOPPINGCART'),
				cartCookieVal = cartCookie!=null?cartCookie.split('|'):[],
				cartSuggestedSell = cartCookieVal[2]?cartCookieVal[2]:null;
			quantity = parseInt(!quantity?0:quantity);
			Lowes.Cookie.set('SHOPPINGCART',quantity+'|'+total+'|'+cartSuggestedSell);
			$('#nav-cart-count').text(quantity);
			$('#nav-cart-total').text(total);
		},
		removeItemFromCart : function(e) {
			e.preventDefault();
			var $this = $(this).parent(),
				$errorTemplate = $(MiniCart.errorTemplate),
				itemToRemove = $this.closest('li').attr('id').split("_")[1],
				itemQuantity = $this.find('.quantity span').text(),
				itemPrice = $this.find('.pricingArea .pricing'),
				itemsRemaining = parseInt($('#miniCart .remainingItems').text());
			//fire coremetrics
			cmCreatePageElementTag('Remove-' + itemToRemove, 'MiniCart',cmAttrCreator({10: (typeof (cmProductviewData) != 'undefined')?cmProductviewData.pagename:((typeof (cmProductviewData) != 'undefined')?cmPageviewData.pageName:'')}));
			$this.fadeOut(function() {
				$(this).after('<p class="wait"><img src="../../../../images/ajax-loader.gif"/*tpa=http://www.lowes.com/images/ajax-loader.gif*/ alt="Please wait..." /></p>');
				$errorTemplate.text('Item removed from cart.');
				$.ajax({
					type: 'POST',
					url: MiniCart.removeFromCartAjaxURL,
					dataType: 'json',
					data: {
						"orderItemIds" : itemToRemove,
						"isAjaxItemDelete" : "true"
					},
					async: false,
					success: function(data){

						// Fire Tealium Event for Remove from Cart
						// if (typeof utag === "object") {
						// 	utag.link({event_type: "cart-remove", order_item_id: data.orderItemId[0], product_unit_price: [Lowes.Utils.removeCurrency(itemPrice.text())], product_quantity: [itemQuantity]});
						// }

						Lowes.Metrics.ltag({
							event_type: "cart-remove",
							order_item_id: data.orderItemId[0],
							product_unit_price: [Lowes.Metrics.cleanTealiumData(Lowes.Utils.removeCurrency(itemPrice.text()))],
							product_quantity: [Lowes.Metrics.cleanTealiumData(itemQuantity)]
						}, "tealium");

						$this.closest('li').find('.wait').remove();
						$this.html($errorTemplate).fadeIn();
						/* Remove any bogo items returned */
						if (typeof data.alsoRemoved != 'undefined') {
							$.each(data.alsoRemoved, function(lineItem,quantityRemaining) {
								var $thisItem = $('#cartItem_'+lineItem),
									$bogoTemplate = $(MiniCart.errorTemplate);
									thisQuantity = parseInt($thisItem.find('.quantity span').text());
								if (parseInt(quantityRemaining) > 0) {
									$thisItem.find('.quantity span').text(parseInt(quantityRemaining));
								} else {
									$thisItem.fadeOut().html($bogoTemplate.text('Additional promotional item removed from cart.')).fadeIn();
								}
							});
						}
						MiniCart.isFetched = false;
						MiniCart.isNewContent = true;
						MiniCart.fetchCartContents();
						if (itemsRemaining > 0) {
							$('#miniCart .remainingItems').text(itemsRemaining - 1);
						}
					}
				});
			});
		},
		coverRegion: function(show) {
			var yScroll = $(window).scrollTop(),
				yCover = 0,
				yOffset = 200;
			if ($('#mc-ui-block').size()) {
				if (show === true) {
					yOffset = $(window).height()/2 - 32;
					$('#mc-ui-block').add('#mc-block-overlay').width('100%').height($(document).height()).show();
					//Show cover and move loading graphic according to the view port
					yCover = yScroll + yOffset;
					$('#mc-ui-block').find('img').css('margin-top',yCover+'px').end().show();
				} else {
					//Hide cover if no other processes are in progress
					$('#mc-ui-block').add('#mc-block-overlay').hide().find('img').css('margin-top','20px');
				}
			}
			return false;
		},
		/**
		 * Builds an add to cart button to inject into the DOM. Will take you to cart if minicart isn't active
		 * Min/Mult functionality depends on Product.js and is ignored in other places.
		 * @param {string} catEntryId self-explanatory
		 * @param {number} initial quantity for quantity field. Default = 1
		 * @param {number} Optional - Minimum Quantity for purchase - if used with multiple, this must match the first multiple (min 2 with multiples of 3 won't work)
		 * @param {number} Optional - Multiple Quantities for purchase (Must be purchased in multiples of...)
		 * @param {number} Optional - Start Multiples at... Default: 0 (Multiples of 3 with a 3 start would = 9 as the first option)
		 * @param {number} Optional = End Multiples at... Default: 100
		 */

		buildAddToCartButton: function(catEntryId, quantity, minimum, multiple, multStart, multEnd) {
			if (!Lowes.User.isZippedIn()) {
				return false;
			}
			if ($('#atcform_' + catEntryId).length) {
				/*Can not create a duplicate add to cart button. For multiples, create a generic button that triggers a click on the atc button */
				return false;
			}
			var atcHTML = '<form name="atcform_' + catEntryId + '" id="atcform_' + catEntryId + '" class="atcForm" action="/OrderItemAdd" method="post">';
			atcHTML += '	<input type="hidden" name="catalogId" id="catalogId" value="10051">';
			atcHTML += '	<input type="hidden" name="langId" id="langId" value="-1">';
			atcHTML += '	<input type="hidden" name="storeId" id="storeId" value="10151">';
			atcHTML += '	<input type="hidden" name="catEntryId" id="catEntryId" value="' + catEntryId + '">';
			atcHTML += '	<input type="hidden" name="productId" id="productId" value="' + catEntryId + '">';
			atcHTML += '	<p class="quantity">';
			if (typeof Lowes.Product == 'object' && typeof Lowes.Product.BaseModel == 'object') {
				atcHTML += Lowes.Product.BaseModel.buildInput(catEntryId, minimum, multiple, multStart, multEnd, quantity);
			} else {
				atcHTML += '		<label for="quantity"><b>Qty.:</b></label>';
				atcHTML += '		<input type="text" maxlength="5" value="' + (quantity > 0 ? quantity : 1) + '" class="qty-input" name="quantity" id="quantity_' + catEntryId + '"/>';
			}
			atcHTML += '	</p>';
			atcHTML += '	<a class="button add atc-template" href="#addtocartinterstitial_page" id="atc_' + catEntryId + '" title="Add to Cart"><span>Add to Cart</span></a>';
			atcHTML += '</form>';
			return atcHTML;
		}
	};

	// Add the Object to Lowes namespace
	Lowes.MiniCart = MiniCart;

	// Expose the updated Lowes namespace to the world
	window.Lowes = Lowes;
}());

/* ../global/minicart/dialog.js */

/** @fileOverview MiniCart.Dialog */
(function(){
	// Grab Lowes namespace object or create a new one.
	var Lowes = window.Lowes || {};

	var Dialog = {
		dialogWidth: {
			"0":"330px",
			"1":"220px",
			"2":"450px",
			"3":"670px",
			"4":"760px"
		},
		timeoutFunction: null,
		globalTimeoutFunction: null,
		mcDiv: null,
		mcContent: null,
		mcLoader: null,
		adjustedWidth: false,
		activeCarousel: false,
		timeout: 800,
		delayHover: 2000, // delay for ipad hover
		globalTimeout: 600, // 0 for never timeout
		cartLink: "#lowes-cart",
		dialogHTML: '<div id="miniCartDialog" class="sub-level"><div class="ui-north-arrow-bg"></div><div class="ui-north-arrow"></div><div id="miniCart"></div></div>',
		dialogDefaultContent: '<p class="loading"><img src="../../images/ajax-loader.gif"/*tpa=http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/images/ajax-loader.gif*/ /></p>',
		init : function() {
			$('#lowes-cart').prepend(this.dialogHTML);
			$('#nav-cart-count').next().attr('id','nav-cart-label');
			Dialog.mcDiv = $("#miniCartDialog");
			Dialog.mcContent = $("#miniCart");
			if (!$('#lowes-cart .loader').length) {
				$(Dialog.cartLink).append('<img class="loader" style="display: none;" src="../../images/ajax-loader-minicart.gif"/*tpa=http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/images/ajax-loader-minicart.gif*/ />');
				Dialog.mcLoader = $('#lowes-cart .loader');
			}
			$(this.cartLink).hoverIntent({
				sensitivity: 3, // number = sensitivity threshold (must be 1 or higher)
				interval: 50,   // number = milliseconds of polling interval
				over: function (){
					var isFetched = Lowes.MiniCart.isFetched;
					if (!Lowes.MiniCart.isLoading) {
						if(isFetched == false && !Lowes.MiniCart.isLoading){
							Lowes.MiniCart.isLoading = true;
							$("#lowes-cart #nav-cart-label, #lowes-cart #nav-cart-total").fadeTo('fast',0.0, function() {
								Dialog.mcLoader.fadeIn('fast',function() {
									Dialog.showMC();
								});
							});
						} else {
							Dialog.showMC();
						}
					}
				},
				timeout: 300,   // number = milliseconds delay before onMouseOut function call
				out: function (){
					Dialog.hideMC();
				}
			});
		},
		showMC: function(){
			var isFetched = Lowes.MiniCart.isFetched;
			dialogContent = Lowes.MiniCart.getCartContents(isFetched?false:true, true);
			switch (Lowes.MiniCart.totalItemsInCart) {
				case null:
				case undefined:
					dialogWidth = "1";
				break;
				case "0" :
				case "1" :
				case "2" :
				case "3" :
					dialogWidth = Lowes.MiniCart.totalItemsInCart;
				break;
				default :
					dialogWidth = "4";
				break;
			}


			if(Lowes.MiniCart.isFetched == true && Dialog.adjustedWidth == false || Lowes.MiniCart.totalItemsInCart == 0 || Lowes.MiniCart.isNewContent == true){
				Dialog.updateContent(Dialog.mcDiv, Dialog.dialogWidth[dialogWidth], dialogContent);
				Lowes.MiniCart.isNewContent = false;
				Dialog.adjustedWidth = true;
				Dialog.activeCarousel = false;
			}
			Dialog.mcDiv.fadeIn(function(){
				Dialog.trackCursor();
				// height is set so that the window doesn't jump if a long item is removed & only shorter ones are left
				Dialog.mcContent.css('height',(($('#miniCart .noCarousel').height()?$('#miniCart .noCarousel').height():$('#miniCart .carousel').height())) + 'px');
				if(isFetched == false){
					Dialog.mcLoader.fadeOut('fast');
					$("#lowes-cart #nav-cart-label, #lowes-cart #nav-cart-total").fadeTo('slow',1);
					$('#miniCart > div:not(.arrow)').css('opacity',0);
				}
				$('#miniCart > div:not(.arrow)').css({'height':$('#miniCart').height() + 'px','width':'659px'}).animate({ opacity: 1 });
			});
			//$(Dialog.cartLink).off("mouseenter");
		},
		hideMC: function(){
			Dialog.mcDiv.fadeOut(function(){
				//Dialog.mcDiv.off("mouseleave").off("mouseenter");
				Dialog.globalTimeoutToggle('off');
			});
			//$(Dialog.cartLink).on("mouseenter", {item:Dialog}, Dialog.delayShowMC);
		},
		adjustWidth: function(target, width, content){
			if(target.length && width){
				//var offset = target.offset().left;
				target.animate({
					width: width
					//,left: offset - width
				},500, function() {
					if (typeof content != 'undefined') {
						Lowes.MiniCart.Dialog.updateContent(content);
					}
				});
			}
			return false;
		},
		trackCursor: function(){
			Dialog.mcDiv.on({
			  mouseenter: function(){
				window.clearTimeout(Dialog.timeoutFunction);
				//cleartimeout
				Dialog.globalTimeoutToggle('off');
			  },
			  mouseleave: function(){
				Dialog.timeoutFunction = window.setTimeout(function(){
					Dialog.hideMC();
				}, Dialog.mcTimeout);
			  }
			});
		},
		globalTimeoutToggle: function($state){
			switch($state){
				case 'on':
					if(Dialog.globalTimeout > 0 && Dialog.globalTimeoutFunction == null){
						Dialog.globalTimeoutFunction = window.setTimeout(
							function(){
								Dialog.hideMC();
							}, Dialog.globalTimeout
						);
					}
				break;
				case 'off':
					if(Dialog.globalTimeout > 0 && Dialog.globalTimeoutFunction != null){
						// clear the variable so it can turn global timeout back on
						Dialog.globalTimeoutFunction = window.clearTimeout(Dialog.globalTimeoutFunction);
					}
				break;
			}
		},
		updateContent: function(target, width, content){
			if(target.length && width){
				target.css('width',width);
			}
			//$(content).css({opacity: 0.0, visibility: "visible"});

			if (typeof content == 'object') {
				Dialog.mcContent.html('').append(content);
			} else {
				Dialog.mcContent.html(content);
			}
			$('#miniCart .removeItem').one('click', Lowes.MiniCart.removeItemFromCart);
			if (Lowes.MiniCart.totalItemsInCart > 3) {
				Dialog.mcContent.prepend('<div class="arrow prev"></div><div class="arrow next"></div>');
			} else {
				$('#miniCart .noCarousel').append('<div class="clear"></div>');
			}
			if (Lowes.MiniCart.totalItemsInCart > 0) {
				$('.buttonArea').remove();
				$('#miniCart').after('<div class="buttonArea' + (Lowes.MiniCart.totalItemsInCart > 3?" hasCarousel":"") + '"><a class="button-green" href="' + Lowes.MiniCart.checkoutURL + '"><span>View Cart &amp; Checkout</span></a></div>');
			} else if ($('#miniCartDialog .buttonArea').length) {
				$('#miniCartDialog .buttonArea').fadeOut().remove();
			}
			if (Lowes.MiniCart.totalItemsInCart > 3 && Dialog.activeCarousel == false) {
				new Lowes.UI.Carousel('#miniCart', { loop:false, controlBar:false, easing:'easeInOutExpo', scroll:3, speed:1000, visible:3 }).create();
				Dialog.activeCarousel = true;
			}
			return true;
		}
	} //End Dialog


	// Add the Object to Lowes namespace
	Lowes.MiniCart.Dialog = Dialog;

	// Expose the updated Lowes namespace to the world
	window.Lowes = Lowes;
}());

/* ../global/minicart/notifications.js */

/** @fileOverview MiniCart.Notifications */

Lowes.MiniCart.Notifications = (function() {

	var addedToCartJSON = {},
		addToCartURL = Lowes.MiniCart.addToCartURL,
		rtfItemAddedMsg = 'Item Added to Cart',
		modalErrorTitle = 'An error occured.',
		rtfCount, $addToCartButton, dialogWidth, modalErrorMsg;

	function setupContent() {
		//dialogWidthValues();
		Lowes.MiniCart.isFetched = false;
		Lowes.MiniCart.isNewContent = true;
		var product = addedToCartJSON.product,
			epp = addedToCartJSON.epp,
			rtf = addedToCartJSON.rtf,
			modalContent = '';
		if (Lowes.MiniCart.fetchInBkg) {
			Lowes.MiniCart.getCartContents(true);
		} else {
			Lowes.MiniCart.updateCartInfo(addedToCartJSON.orderTotalQty,Lowes.Utils.formatCurrency(parseFloat(addedToCartJSON.orderTotal)));
		}
		if (rtf.messageDetail) {
			modalContent += '<div class="mini-product-details-rtf">';
			modalContent += '<a href="'+product.detailPageURL+'" title="'+product.name+'"><img src="'+product.imageURL+'" alt="'+product.name+'" /></a>';
			modalContent += '<div class="product-info">';
			modalContent += '<h2><a href="'+product.detailPageURL+'" title="'+product.name+'">'+product.name+'</a></h2>';
			modalContent += '<div class="price">'+Lowes.Utils.formatCurrency(parseFloat(product.price))+'</div>';
			modalContent += '<div class="quantity">Qty.: '+ parseInt(product.quantity,10)+'</div>';
			if (epp.name) {
				modalContent += '<div class="epp-display clearfix">'+epp.name+' <span>'+Lowes.Utils.formatCurrency(parseFloat(epp.price))+'</span></div>';
			//	modalContent += '<div class="subtotal-display clearfix"><span>Subtotal: '+Lowes.Utils.formatCurrency(parseFloat(addedToCartJSON.subtotal))+'</span></div>';
			}
			modalContent += '</div>';
			modalContent += '</div>';
			modalContent += '<div class="rtf-container">';
			modalContent += '<h3>'+rtf.messageHeader+'</h3>';
			// modalContent += '<p>'+rtf.messageDetail+'</p>';
			modalContent += '<ul>';
			rtfCount = 0;
			$.each(addedToCartJSON.rtf.products, function(id,rtfProduct) {
				if (typeof rtfProduct.JSONStatus != 'undefined') {
					rtfCount++;
					modalContent += '<li>';
					modalContent += '<input type="hidden" value="'+id+'" name="catEntryId">';
					modalContent += '<a href="'+rtfProduct.detailPageUrl+'" title="'+rtfProduct.name+'"><img alt="'+rtfProduct.name+'" src="'+rtfProduct.imageURL+'" /></a>';
					modalContent += '<h2><a title="'+rtfProduct.name+'" href="'+rtfProduct.detailPageUrl+'">'+rtfProduct.name+'</a></h2>';
					modalContent += '<div class="price">'+Lowes.Utils.formatCurrency(parseFloat(rtfProduct.price))+'</div>';
					modalContent += '<div class="qty-area">' + Lowes.MiniCart.Dialog.dialogDefaultContent;
					modalContent += '<div class="quantity rtf-input">';
					if (!rtfProduct.isInCart){
						modalContent += '<label for="quantity_'+id+'">Qty.: </label>';
						modalContent += '<input type="text" name="quantity" id="quantity_'+id+'" value="1" />';
					} else {
						modalContent += 'Qty.: '+parseInt(rtfProduct.quantity,10);
					}
					modalContent += '</div>';
					if (!rtfProduct.isInCart){
						modalContent += '<div class="rtf-button-container rtf-input"><button type="submit" id="rtf_' + id + '" class="button add rtf-add"><span>Add to Cart</span></button></div>';
					} else {
						modalContent += '<div class="rtf-button-container added">'+rtfItemAddedMsg+'</div>';
					}
					modalContent += '</div>';
					modalContent += '</li>';
				}
			});
			modalContent += '</ul>';
			modalContent += '</div>';
		} else {
			rtfCount = 0;
			modalContent += '<div class="mini-product-details">';
			modalContent += '<a href="'+product.detailPageURL+'" title="'+product.name+'"><img src="'+product.imageURL+'" alt="'+product.name+'" /></a>';
			modalContent += '<h2><a href="'+product.detailPageURL+'" title="'+product.name+'">'+product.name+'</a></h2>';
			modalContent += '<div class="price">'+Lowes.Utils.formatCurrency(parseFloat(product.price))+'</div>';
			modalContent += '<div class="quantity">Qty.: '+parseInt(product.quantity,10)+'</div>';
			if(epp.name) {
				modalContent += '<div class="epp-display">'+epp.name+' <br />'+Lowes.Utils.formatCurrency(parseFloat(epp.price))+'</div>';
			}
			modalContent += '</div>';
		}
		setupDialog(modalContent);
	}

	function dialogWidthValues() {
		switch (rtfCount) {
			case 1:
			case 2:
				dialogWidth = '377px';
			break;
			case 3:
				dialogWidth = '520px';
			break;
			default:
				dialogWidth = '340px';
		}
		return dialogWidth;
	}
	function setupDialog(modalContent) {
		dialogWidthValues();

		if ($('#productAddToCart').length) {
			$('#productAddToCart').remove();
		}
		var modalDiv = $('<div id="productAddToCart">' + modalContent + '</div>').appendTo('body');

		modalDiv.dialog({
			dialogClass : 'cart-dialog modal-success',
			width : dialogWidth,
			resizable : false,
			show : 'fade',
			hide : 'fade',
			closeOnEscape : true,
			modal : true,
			draggable : false,
			title : addedToCartJSON.addedToCartMessage,
			open: function() {
				$('<a />', {
					text : 'Continue Shopping',
					href : '#'
				}).prependTo($(".ui-dialog-buttonset")).click(function() {
					modalDiv.dialog('close');
				});
			},
			buttons : {
				'Check Out' : {
					text : "Check Out",
					"class" : "button button-green",
					click : function() {
						document.location = Lowes.MiniCart.checkoutURL;
					}
				}
			}
		});

		$(document).on('click', '.ui-widget-overlay', function() {
			modalDiv.dialog('close');
		});
	}

	function updateRtfItem() {
		var product = addedToCartJSON.product,
			$qtyContainer = $addToCartButton.closest('.qty-area').find('.quantity'),
			$buttonContainer = $addToCartButton.closest('.rtf-button-container'),
			$cartTotal = $('#cartTotal');

		$qtyContainer.html('Qty.: '+ parseInt(addedToCartJSON.product.quantity,10));
		$buttonContainer.addClass('added').html('Item Added to Cart');
		Lowes.MiniCart.getCartContents(true);
	}

	return {
		init : function() {
			this.linkBinding();
		},
		linkBinding : function() {
			$('.ui-widget-overlay').on('click.minicart', function() {
				if ($('#productAddToCart').is(':ui-dialog')) {
					$('#productAddToCart').dialog('close');
				}
			});
			$('body').on('click.minicart','button.add:not(.ignoreMiniCart),a.add:not(.ignoreMiniCart)',Lowes.MiniCart.Notifications.addItemToMiniCart)
			.on('change','.atcForm .minQty', function(e){
				var $this = $(this),
					$inputRegion = $this.parents('form'),
					catEntryID = $inputRegion.attr('id').replace(/atcform_/gi,''),
					minQty = $this.data('minqty'),
					$msg = $inputRegion.find('.minMultMsg'),
					val = parseFloat($this.val());
				if (val < minQty || val == '') {
					$msg.addClass('error');
				} else {
					$msg.removeClass('error');
				}
			}).on('keydown','.qty-input', function(e) {
				if (e.keyCode == 13) {
					e.preventDefault();
					$(this).parents('.addToCart').find('.add').trigger('click');
				}
			});
		},
		addItemToMiniCart : function(e) {
			$addToCartButton = $(this);
			var $itemForm = $addToCartButton.closest('form'),
				$inputRegion = $addToCartButton.parents('.qty-area'),
				$quantityInput = $inputRegion.find('.quantity input'),
				itemData = {
					"catalogId" : "10051",
					"langId" : "-1",
					"storeId" : "10151",
					"URL" : "OrderItemDisplay",
					"isAjaxItemAdd" : "true"
				},
				catEntryId = '',
				quantity = '',
				deliveryMethod = '',
				eppId = '',
				price = '',
				isRTF = false,
				atcParams = {},
				clickType = Lowes.MiniCart.Notifications.getClickType(),
				cmCategoryId='',
				map = false,
				previousPricing = false;
			switch (clickType) {
				case "standard" :
					//atc template
					Lowes.MiniCart.coverRegion(true);
					catEntryId = $itemForm.find('[name=catEntryId]').val();
					$quantityInput = $itemForm.find('.qty-input');
					quantity = $quantityInput.val();
				break;
				case "rtf" :
					//Required to functions
					$inputRegion.find('.rtf-input').css('opacity',0);
					$inputRegion.find('.loading').show();
					isRTF = true;
					catEntryId = $addToCartButton.attr('id').split("_")[1];
					quantity = $quantityInput.val();
					price = Lowes.Utils.removeCurrency($addToCartButton.parents('li').find('.price').text());
				break;
				case "quickview" :
					//quickview
					Lowes.MiniCart.coverRegion(true);
					catEntryId = $addToCartButton.attr('id').split("_")[1];
					$quantityInput = $addToCartButton.parent().find('#quantity');
					if(!$quantityInput.is(":visible")){
						$quantityInput = $addToCartButton.parent().find('#quantity' +'_of_'+catEntryId);
					}
					quantity = $quantityInput.val();
					if ($addToCartButton.closest('#descCont').find('.pricing .price').text().trim()==="View Price In Cart"){
							map=true;
					}
					//price is not in quickview on map items
				break;
				case "detail" :
					//detail page
					if (Lowes.Product.addToCartDetail()) {
						Lowes.MiniCart.coverRegion(true);
						catEntryId = $addToCartButton.attr('id').split("_")[1];
						$quantityInput = $addToCartButton.parent().find('.qty-input');
						quantity = $quantityInput.val();
						price = $('#paOfferPrice').val();
						deliveryMethod = typeof $('.deliveryMethod:checked').val() == 'undefined'? '' : $('.deliveryMethod:checked').val();
						eppId = $('#warranty .warranty-checkbox:checked').val()?$('#warranty .warranty-checkbox:checked').val():'';
						if ($('.pricing strong').text().trim()==="View Price in Cart"){
							map=true;
						}
						if (cmProductviewData.isPreviousPrice === "true") {
							previousPricing=true;
						}
					} else {
						// can't continue b/c availability requirements haven't been met
						return false;
					}
				break;
				case "myLists" :
					//MyLowes Lists
					Lowes.MiniCart.coverRegion(true);
					atcParams = Lowes.Utils.urlQueryObject($addToCartButton.attr('href'));
					catEntryId = atcParams.catEntryId;
					quantity = atcParams.quantity;
					price = Lowes.Utils.removeCurrency($addToCartButton.parents('.drawer-right').find('.price').text());
					if ( $addToCartButton.closest('#pricing').find('.price').text().trim()==="View price in cart"){
						map=true;
					}
				break;
				case "compare" :
					//product compare
					Lowes.MiniCart.coverRegion(true);
					catEntryId = $addToCartButton.closest('.prod-compare').find('#catEntryId').val();
					quantity = $addToCartButton.closest('.prod-compare').find('#quantity').val();
					price = Lowes.Utils.removeCurrency($addToCartButton.closest('.prod-compare').find('.price').text());
					if ($addToCartButton.closest('.prod-compare').find('.price').text().trim()==="View Price in Cart"){
						map=true;
					}
				break;
				case "list" :
					//list page
					if ($addToCartButton.closest('.productWrapper').hasClass('gift-card-list')) {
						window.location = $addToCartButton.attr('href');
						return false;
					} else {
						Lowes.MiniCart.coverRegion(true);
						catEntryId = $itemForm.find('[name=catEntryId]').val();
						$quantityInput = $itemForm.find('.qty-input');
						quantity = $quantityInput.val();
						price = $itemForm.find('[name=paOfferPrice]').val();
						if ($addToCartButton.closest('.pricingArea').find('.pricing').text().trim()==="View Price in Cart"){
							map=true;
						}
						if ($addToCartButton.closest('.pricingArea').find('.wasPrice').attr('data-previouspriceflag') === "true"){
							previousPricing=true;
						}
					}
				break;
				case "cbc" :
					//cbc list pages
					Lowes.MiniCart.coverRegion(true);
					catEntryId = $itemForm.find('[name=catEntryId]').val();
					$quantityInput = $itemForm.find('.qty');
					quantity = $quantityInput.val();
					price = $itemForm.find('[name=paOfferPrice]').val();
				break;
				default :
					if ($itemForm.length) {
						//if not a minicart add button, check for a form & submit
						$itemForm.submit();
					} else if ($addToCartButton.attr('href').indexOf('/OrderItemAdd') != -1) {
						//if not a minicart add button, check for an add to cart href & go there
						window.location = $addToCartButton.attr('href');
					} else {
						Lowes.MiniCart.coverRegion();
						return false;
					}
				break;
			} // end switch
			e.preventDefault();
			quantity = parseInt(quantity,10);
			if (!catEntryId) {
				return false;
			} else if (!quantity || quantity < 0) {
				//if an input field for this add to cart button exists, if the quantity is valid,
				//set it to 1 & abort; otherwise, set quantity to 1 and continue
				if ($quantityInput.length == 1) {
					$quantityInput.val("1");
					Lowes.MiniCart.coverRegion();
					return false;
				} else {
					quantity = 1;
				}
			}
			if (!isRTF && $('.ui-dialog-content').is(':ui-dialog')) {
				$('.ui-dialog-content').dialog("close");
			}

			itemData.catEntryId_1 = catEntryId; //product
			itemData.quantity_1 = quantity;
			itemData.cmCategoryId=clickType;
			if (price.length) {
				itemData.price = price;
			}
			if (deliveryMethod.length) {
				itemData.shipModeId = deliveryMethod;
			}
			if (eppId.length) {
				itemData.catEntryId_2 = eppId;
				itemData.quantity_2 = quantity;
				itemData.eppSelected = true;
				itemData.fromItemDisplay = true;
			}
			setTimeout(function() {
				$.ajax({
					dataType : 'json',
					type : 'POST',
					url : Lowes.MiniCart.addToCartAjaxURL,
					data : itemData,
					async : false,
					success : function(data) {
						if (data.JSONStatus == 'success') {
							if (typeof data.addedToCartJSON != 'undefined') {
								addedToCartJSON = data.addedToCartJSON;
							} else {
								addedToCartJSON = data;
							}
							if (isRTF) {
								updateRtfItem();
							} else {
								setupContent();
							}
							if ($.inArray(clickType, ["detail","list","quickview"]) > -1) {
								Lowes.MiniCart.Notifications.fireCoremetrics(catEntryId,addedToCartJSON.product,clickType,map,previousPricing);
								Lowes.MiniCart.Notifications.fireCoremetricsShop5(addedToCartJSON,clickType,previousPricing);
								// Fire Tealium Event
								Lowes.MiniCart.Notifications.fireTealiumEvent(data);
							} else if($.inArray(clickType, ["compare","rtf", "myLists"]) > -1){
								Lowes.MiniCart.Notifications.fireCoremetrics(catEntryId,addedToCartJSON.product,clickType,map,previousPricing);
								// Fire Tealium Event
								Lowes.MiniCart.Notifications.fireTealiumEvent(data);
							}


						} else {
							Lowes.MiniCart.Notifications.redirectToCart(itemData);
						}
					},
					complete : function () {
						Lowes.MiniCart.coverRegion(false);
						if (isRTF) {
							$inputRegion.find('.loading').hide();
							$inputRegion.find('.rtf-input').css('opacity',1);
						}
					},
					error : function() {
						Lowes.MiniCart.Notifications.redirectToCart(itemData);

					}
				});
			},10); // delay b/c ui block doesn't happen in chrome (sync data call)
		},
		getClickType : function() {
			if (typeof $addToCartButton != 'undefined' && $addToCartButton.hasClass('rtf-add')) {
				//Required to functions
				return "rtf";
			} else if (typeof $addToCartButton != 'undefined' && $addToCartButton.hasClass('atc-template')) {
				//atc generated from minicart template
				return "standard";
			} else if (typeof $addToCartButton != 'undefined' && $addToCartButton.parents('#quickview').length) {
				//quickview
				return "quickview";
			} else if ($('#priceAvailability').length) {
				//detail page
				return "detail";
			} else if ($('#lists').length) {
				//MyLowes Lists
				return "myLists";
			} else if ($('.prod-compare').length) {
				//product compare
				return "compare";
			} else if ($('.product-listing').length) {
				//list page
				return "list";
			} else if ($('#cbc-anon-landing').length) {
				//cbc list pages
				return "cbc";
			} else {
				return false;
			}
		},
		fireTealiumEvent : function(data) {


				// Kill Event if in mylowes
				if (data.cmCategoryId[0] === "myLists") return;

				// Build Data Object for Tealium
				var newUtagData = {
					event_type: "cart-add",
					endeca_product_id: [Lowes.Metrics.cleanTealiumData(data.addedToCartJSON.product.id)],
					product_id: [Lowes.Metrics.cleanTealiumData(data.addedToCartJSON.product.partNumber)],
					product_unit_price: [Lowes.Metrics.cleanTealiumData(data.addedToCartJSON.product.price)],
					product_name: [Lowes.Metrics.cleanTealiumData(data.addedToCartJSON.product.name)],
					product_quantity: [Lowes.Metrics.cleanTealiumData(data.addedToCartJSON.product.quantity)]
				};
				// Check if the Extended Protection Plan has been Selected
				if (data.eppSelected && data.eppSelected[0] === "true") {
					newUtagData.event_type = "cart-add,prod-epp";
					newUtagData.epp_order_item_id = data.addedToCartJSON.epp.orderItemId;
					newUtagData.epp_price = data.addedToCartJSON.epp.price;
				}
				// Tealium
				Lowes.Metrics.ltag(newUtagData, "tealium");

		},
		fireCoremetrics : function(catEntryId,productObj,clickType,map,previousPricing) {
			var cmObject = {},
				cmCategory = "",
				price=productObj.price,
				name=productObj.name;
			//user must be zipped in to see an add to cart button, so don't do all this unless they are'
			if (!Lowes.User.isZippedIn()) {
				return false;
			}

			switch (clickType || this.getClickType()) {
				case "detail" :
					//detail page
					cmCategory = "product detail page";
				break;
				case "list" :
					//list page
					cmCategory = "product list page";
				break;
				case "quickview":
					//quickview
					cmCategory = "quickview";
				break;
				case "rtf":
					cmCategory = 'rtf';
				break;
				case "compare":
					cmCategory = 'compare page';
				break;
				case "myLists":
					cmCategory = 'my lowes list page';
				break;
				default :
					cmCategory = '';
				break;
			}

			if (map){
				cmCategory += " :MAP";
			}

			if (cmCategory.length) {
				cmObject[1] = name;
				cmObject[2] = catEntryId;
				cmObject[3] = price;
				cmObject[16] = previousPricing;
				cmCreatePageElementTag("Add to Cart", cmCategory, cmAttrCreator(cmObject));
			}
		},
		fireCoremetricsShop5: function(addedToCartJSON,clickType,previousPricing){
			var cmAttributes=(clickType=='detail')?cmProductviewData.explorerAttributes:cmPageviewData.explorerAttributes;
			cmAttributes+='-_--_-'+addedToCartJSON.product.promotionCode1;
			cmAttributes+='-_-' + addedToCartJSON.product.discount1;
			cmAttributes+='-_-' + addedToCartJSON.product.promotionCode2;
			cmAttributes+='-_-' + addedToCartJSON.product.discount2;
			if (clickType!='detail'){
				cmAttributes+='-_-' + '-_-';
			}
			cmAttributes+='-_-' +'null'+'-_-'+'null'+'-_-' + addedToCartJSON.product.price +'-_-'+ previousPricing +'-_-'+ clickType;
			cmCreateShopAction5Tag(addedToCartJSON.product.partnumber,
									addedToCartJSON.product.name,
									addedToCartJSON.product.quantity,
									addedToCartJSON.product.price,
									clickType,
									"10151",
									addedToCartJSON.product.currency,
									0,
									cmAttributes);
			cmDisplayShop5s();
		},
		redirectToCart : function (itemData) {
			var cartParams = '';
			$.each(itemData,function(key, val) {
				if (key != "isAjaxItemAdd") {
					cartParams += "&" + key + "=" + val;
				}
			});
			addToCartURL += cartParams;
			window.location = addToCartURL;
		}
	};
}());

/* ../global/minicart/init.js */

// Initialize MiniCart if not on an excluded page
if ($.inArray($('body').attr('id'),Lowes.MiniCart.excludedPageIds) == -1 && !$('#error-page').length) {
	Lowes.MiniCart.init();
}

/* ../global/Lowes/My/core.js */

/** @fileOverview My Lowe's Base Namespace */

(function (Lowes){
	/**
	 * MyLowe's base namespace
	 * @namespace
	 * @memberOf Lowes
	 */
	Lowes.My = {};
	window.Lowes = Lowes;
}(window.Lowes || {}));

/* ../global/Lowes/My/account.js */

/** @fileOverview Account management. */

 (function () {
	var Lowes = window.Lowes || {};
	var My = Lowes.My || {};
	/** @namespace */
	Lowes.My.Account = {};
	window.Lowes = Lowes;
}());

/* ************************************************************************************* */

/**
* Start Manage Account Login, Register Modal
* Account Modal for Register and Signin load in iframe
* @author NBP
* @example
* how to call Login Modal:  acctModal.loginOpen();
* how to call Register Modal: acctModal.registerOpen();
* @since 2.7
*/
var acctModal = {
	/** set to true after user is logged in or registered on modal, to help trigger reload of page. */
	reloadPageOnModalClose: false,
	/** set to true after user is logged in or registered on modal, to help trigger reload of page. */
	reloadCallback: null,
	appendUrl: '',
	viewState: null,
	/** Cache Account Modal DOM Object */
	$acctModal: null,

	/** Define Width, height of Modal, Register, Signin */
	WH_reg: {
		'width': 785,
		'height': 500
	},

	WH_regError: {
		'width': 785,
		'height': 625
	},
	WH_signin: {
		'width': 400,
		'height': 450
	},

	WH_signinError: {
		'width': 400,
		'height': 450
	},

	WH_forgotPassword: {
		'width': 400,
		'height': 450
	},

	/** Forgot password submitted, thank you window. */
	WH_forgotPasswordThankYou: {
		'width': 400,
		'height': 450
	},

	/**
	 * iframe modal load init, Create Empty Div tag used for modal.
	 */
	init: function () {
		if (!acctModal.$acctModal) {
			// Create modal for login
			$('<div id="modal_iframe_account" />').dialog({
				autoOpen: false,
				draggable: false,
				modal: true,
				position: 'center',
				resizable: false,
				closeText: 'Close'
			});
			acctModal.$acctModal = $('#modal_iframe_account');
			acctModal.$acctModal.css('overflow', 'hidden');
		}
	},

	/**
	* Common Resize Dialog, to resize Signin, Register modal
	* @example acctModal.resizeDialog({'width': 100, 'height':200});
	* @param {Object} WH Object containing width and height of dialog. {width: ##, height: ##}
	* @param {Number} WH.height Height of dialog.
	* @param {Number} WH.width Width of dialog.
	*/
	resizeDialog: function (WH) {
		// Resize Modal
		if (acctModal.$acctModal) {
			acctModal.$acctModal.dialog('option', {
				'width': WH.width,
				'height': WH.height
			});
		}

		// resize Iframe
		$('iframe#iframe_modal_account').prop({
			'width': (WH.width - 20),
			'height': (WH.height - 48)
		});

		if (acctModal.$acctModal) {
			// Center modal, after its been resized, Do not center at same time because center will be wrong.
			acctModal.$acctModal.dialog('option', {
				'position': 'center'
			});
		}
	},

	/**
	 * Resize register iframe on load of new page.
	 * @return none.
	 */
	resizeOnloadRegister: function () {
		acctModal.viewState = 'signup'; // helps other party code to fire based on what view user signin or signup from.
		acctModal.resizeDialog(acctModal.WH_reg);
	},

	/**
	 * Resize register iframe on load of new page.
	 **/
	resizeOnloadRegisterError: function () {
		acctModal.resizeDialog(acctModal.WH_regError);
	},

	/**
	 * Resize signin iframe on load of new page.
	 */
	resizeOnloadSignin: function () {
		acctModal.viewState = 'signin'; // helps other party code to fire based on what view user signin or signup from.
		acctModal.resizeDialog(acctModal.WH_signin);
	},

	/**
	 * Resize signin iframe on load of new page.
	 */
	resizeOnloadSigninError: function () {
		acctModal.resizeDialog(acctModal.WH_signinError);
	},

	/**
	 * Create iframe for signin modal
	 */
	signin: function () {
		var WH = acctModal.WH_signin;
		var locHref = (location.href.indexOf('firstReferURL') == -1 ? location.href : location.href.split('?')[0]);
		acctModal.$acctModal.dialog('option', {
			'width': WH.width,
			'height': WH.height
		}).html('<iframe src="https://' + document.location.host.split(':')[0] + '/webapp/wcs/stores/servlet/LogonFormNavIframeS?langId=' + Lowes.langId + '&storeId=' + Lowes.storeId + '&catalogId=' + Lowes.catalogId + '&URL=' + locHref + '&protocol=' + document.location.protocol.replace(':', '') + '" id="iframe_modal_account" class="acctModal_iframe" height="' + (WH.height - 48) + '" width="' + (WH.width - 20) + '" frameborder="0" border="0" cellspacing="0" scrolling="no" marginwidth="0" marginheight="0" style="border:none; margin-top: 12px; overflow:hidden;"></iframe>').dialog('open').dialog('option', "position", "center");
	},

	/**
	 * Resize forgot password (from signin modal)
	 */
	resizeOnloadForgotPass: function () {
		acctModal.resizeDialog(acctModal.WH_forgotPassword);
	},

	/**
	 * Resize forgot password sent, thank you screen (from signin modal)
	 */
	resizeOnloadForgotPassThankYou: function () {
		acctModal.resizeDialog(acctModal.WH_forgotPasswordThankYou);
	},

	/**
	 * close account modals, called from iframe
	 */
	closeAcctModal: function () {
		acctModal.$acctModal.dialog('close');
	},

	/**
	 * Reload current page,
	 * When current page being viewed is :logoff or Logon or Register, then redirect user to home page.
	 */
	reloadPage: function () {
		if (acctModal.reloadCallback) {
			acctModal.reloadCallback();
			return;
		}
		// List of pages that should not reload to same URL
		// Add any other pages to check in this array
		var noReloadToSamePage = ['/Logoff', '/LogonForm', '/UserRegistrationForm'];

		var onReloadToSamePageRedirect = '/';
		var redirectUrl = window.location.href.split('#')[0];

		// Check current page URL for any pages that should not load to same page:
		var i = 0;
		var i2 = noReloadToSamePage.length;
		for (i; i < i2; i++) {
			if (redirectUrl.indexOf(noReloadToSamePage[i]) !== -1) {
				redirectUrl = onReloadToSamePageRedirect;
				break;
			}
		}


		// Order Item Display page only Logic. Remove 'orderid' param and value.
		// Example: changes url from: http://url/?new=1&orderId=xxxx&new=2  into  http://url/?new=1
		if (redirectUrl.indexOf('OrderItemDisplay') !== -1) {
			redirectUrl = '/OrderItemDisplay?storeId=10151&langId=-1&catalogId=10051';
		}

		// Reload page to current page or homepage
		window.location = redirectUrl + this.appendUrl;
	},

	/**
	 * Register modal load iframe, set width and height of modal
	 */
	register: function () {
		var WH = acctModal.WH_reg;
		var locHref = (location.href.indexOf('firstReferURL') == -1 ? location.href : location.href.split('?')[0]);
		acctModal.$acctModal.dialog('option', {
			'width': WH.width,
			'height': WH.height
		}).html('<iframe src="https://' + document.location.host.split(':')[0] + '/RegistrationFormIframeS?langId=' + Lowes.langId + '&storeId=' + Lowes.storeId + '&catalogId=' + Lowes.catalogId + '&URL=' + locHref + '&protocol=' + document.location.protocol.replace(':', '') + '" id="iframe_modal_account" class="acctModal_iframe" height="' + (WH.height - 48) + '" width="' + (WH.width - 30) + '" frameborder="0" border="0" cellspacing="0" scrolling="no"' + ' marginwidth="0" marginheight="0" style="border:none; margin-top: 12px; overflow:hidden;"></iframe>').dialog('open').dialog('option', "position", "center");
		// Call position center after iframe is loaded, else will not center correctly.
	},

	/**
	 * Click events init onload for signin modal. can be used on any page by adding a class
	 * @example  <a href="#" class="signInModal">signin</a>
	 */
	initEvents: function () {
		// Signin Link in Header and anyplaced where class is signInModal click to load login modal
		$(document).on('click.signin', '.signInModal', function (e) {
			e.preventDefault();
			acctModal.signinOpen();
		});


		// delegate Register and Signin link inside a Modal.
		// acctModal.bindModalLinks(); // Commented out because not in use right now.
	},
	/**
	 * Open Signin Modal
	 */
	signinOpen: function () {
		acctModal.init(); // Create Dialog
		acctModal.signin(); // Load login iframe
	},
	/**
	 * Open Register Modal
	 */
	registerOpen: function () {
		acctModal.init(); // Create Dialog
		acctModal.register(); // Load register iframe
	},

	/**
	 * Bind Signin Modal and Register Modal to Favorites and Reminders Modals
	 * To load Login Modal or Register modal one or more elements being clicked
	 * should have class of "acctModalLogin" or "acctModalRegister"
	 * acctModal.bindModalLinks(); is called onload of page.
	 * @example: <a class="acctModalLogin" href="#">X</a>
	 */
	bindModalLinks: function () {
		var $body = $(document);
		// Bind Click events to open new modal
		// Find all links that says "Log In" or has class of .acctModalLogin
		$body.on('click.acctModal', '.modal_content a:contains(Log In),.acctModalLogin', function (e) {
			e.preventDefault();
			// Close active Modal
			$(this).closest('.mylowes_modal,.ui-dialog-titlebar').find('.close,.ui-dialog-titlebar-close').trigger('click');
			// Load Signin Modal
			acctModal.signinOpen();
		});
		// Bind Click events to open new modal
		// Find All links that says "Register" or has Class of .acctModalRegister
		$body.on('click.acctModal', '.modal_content a:contains(Register),.acctModalRegister', function (e) {
			e.preventDefault();
			// Close active Modal
			$(this).closest('.mylowes_modal,.ui-dialog-titlebar').find('.close,.ui-dialog-titlebar-close').trigger('click');
			// Load Register Modal
			acctModal.registerOpen();
		});
	}
};
/* End Manage Account */

/* ************************************************************************************* */

var myLowesLoginModal = {};

// Lowes Account Login Modal,
// Onload of iframe, Set Hidden values, Add Image
myLowesLoginModal.onload = function () {
	if ($('.lowes_account_login_modal').length) {
		var protocol = $(document).getUrlParam('protocol') || $('#tf_protocol').val() || 'http'; // http or https

		// Set on submit load login page if validation fails.
		// Please scope this selector in.
		$('.lowes_account_login_modal [name=reLogonURL]').val('LogonFormNavIframeS');

		var $Logon = $('#Logon');
		// Add protocol to Form Action, to help error status
		$Logon.prop('action', $Logon.prop('action') + '?protocol=' + protocol);

		// Helps Call back to parent window.
		var setLoginCallback = '';
		var checkParamURL = $(document).getUrlParam('URL') || '';

		// IE javascript error if URL is object grab last value of object.
		if (typeof checkParamURL == 'object') {
			checkParamURL = checkParamURL[checkParamURL.length - 1];
		}

		// Check if called from Favorites Modal.
		if (checkParamURL.indexOf('callback:') > -1) {
			setLoginCallback = '#callback=' + checkParamURL.replace('callback:', '');
		}

		// Set redirect URL on login
		var callbackURL = protocol + '://' + window.location.host + '/MContent/Structured/Member/account_iframe_success.html' + setLoginCallback;

		var URLvalue = $('.lowes_account_login_modal [name=URL]').val();
		var splitURLvalue = URLvalue.split('&URL=');
		URLvalue = URLvalue.replace('&URL=' + splitURLvalue[splitURLvalue.length - 1], '&URL=' + callbackURL);

		$('.lowes_account_login_modal [name=URL]').val(URLvalue);

/* New Sign In Modal */
		// Hide Sign-In Page Value Prop - Already in CSS
		//$('.lowes_account_login_modal #userLoginPage').hide();

		// Set redirect Url for Sign-In Button
		$('.lowes_account_login_modal .signUpLink').on('click', function() {
			var regUrl = $(this).prop('href');
			parent.location = regUrl;
		});
/* - - - - - - - - - - */

		// Find replace href URL's  for Register URL, append protocol
		$('#userLoginPage a[href^=UserRegistrationForm]').prop('href', function () {
			return $(this).prop('href').replace('UserRegistrationForm', 'RegistrationFormIframeS') + '&protocol=' + protocol;
		});

		var callBackMethod = 'resizeOnloadSignin';
		// Check if page has any error showing, if yes update Callback name.
		if ($('.ui-error:eq(0)').length) {
			callBackMethod += 'Error';
		}

		// iframe used to help call Parent window Javascript to resize Modal
		// IE7 will throw Error, if we dont wait until page is ready.
		// Change this it body Onload method, but its not fire correctly.
		// Trying to set timeout.
		// IE7 need this to be body onload event.
		setTimeout(function () {
			$('body').append('<iframe src="' + protocol + '://' + window.location.host + '/MContent/Structured/Member/account_iframe_success.html?callback=acctModal.' + callBackMethod + '" style="width:0px;height:0px;display:none;"/>');
		}, 2000);

		// Wait 1 sec before showing body.
		setTimeout(function () {
			// After DOM is updated, onload display page to avoid jumpie looks from above changes.
			$('body').show();
		}, 1000);
	}
};

/* MyLowes Signup */
// Lowes.My.Account.Signup = {
//     init: function () {
//         var lowes_simple_reg = window.lowes_simple_reg || {};
//         if ($('#Ecom_reg_zip_toggle').length) {
//             // Do not auto set ZIP value is zipcode input is already filled in HTML
//             if ($('#Ecom_BillTo_Postal_PostalCode').val() == '') {
//                 // Check if zip code cookie, if yes hide zip code area
//                 if (Lowes.User.isZippedIn()) {

//                     // Hide zip code field area if already zipped in.
//                     if (typeof lowesRegisterModal != 'object') {
//                         $('#Ecom_reg_zip_toggle').hide();
//                     }

//                     // Set Zip code input field value if already zipped in.
//                     $('#Ecom_BillTo_Postal_PostalCode').val(Lowes.User.getCurrentStore().zip);

//                 }
//             }
//         }
//     }
// };

/**
 * Check if user sessions has timed out form Ajax call respond results.
 * this method can be moved for Global Ajax use.
 * In cause user is on a page for long times and Sessions expires. check for "" if found, redirect user to Signin page.
 * @author NP (manage account)
 * @param {Object|String} data Ajax Respond data object or string passed in raw as is.
 * @param {String} ReDirectPage "SomePageToView" page used to redirect user after login, for example: "TopCategoriesDisplayView" or "AddressManageListCmd"
 * @param {Boolean} NotRedirectSignin If true, redirect user to signin page if sessions expired.
 * @return {Boolean} True if user login sessions expired. False if not expired. or Auto Redirect to signin page.
 * @example Lowes.My.Account.signinExpiredAjax(results, 'RedirectAfterLoginPageNameHere'); // After success login redirect to page defined 'RedirectAfterLoginPageNameHere'
 * @example Lowes.My.Account.signinExpiredAjax(results); // After sucess login Redirect to default CategoriesDisplayView
 */
Lowes.My.Account.signinExpiredAjax = function (data, ReDirectPage, NotRedirectSignin) {
	var findCurrentPageName = '';

	// Failed to find any Ajax Data to Analize.
	if (!data) {
		return false;
	}

	if (!ReDirectPage) {
		findCurrentPageName = window.location.href.split('?')[0].split('#')[0].split('/');
		if (findCurrentPageName.length > 3) {
			findCurrentPageName = findCurrentPageName[findCurrentPageName.length - 1];
			// Avoid redirect to signin page after sucess signin.
			if (findCurrentPageName == 'LogonForm') {
				findCurrentPageName = '';
			}
		}
	}

	findCurrentPageName = (findCurrentPageName != '' ? findCurrentPageName : '');

	var DefaultPageRedirect = ReDirectPage || findCurrentPageName; // What page to goto after signin.
	if ((typeof data == 'string' && data.indexOf('_ERR_DIDNT_LOGON') > -1) || (typeof data == 'object' && data.errorMessageKey == '_ERR_DIDNT_LOGON') || (typeof data.errorMessageKey == 'object' && data.errorMessageKey == '_ERR_DIDNT_LOGON')) {
		if (!NotRedirectSignin) {
			// Session Timeout, redirect to login screen
			window.location = 'http://www.lowes.com/LogonForm' + '?langId=' + Lowes.langId + '&storeId=' + Lowes.storeId + '&catalogId=' + Lowes.catalogId + '&URL=' + DefaultPageRedirect;
		}
		return true;
	}

	return false;
};

/**
 * Check for Login Forms and Auto-Focus
 */
Lowes.My.Account.Autofocus = function () {
	// Escape use if this is a login form already being handled by signin.js
	if (!$('#Logon').length && $('#Ecom_User_ID').length && !$(document.activeElement).is('input')) {
		// Focus on Email or else Password, if email is not empty focus on password. If both email+password are filled, do no focus.
		$('#Ecom_User_ID,#logonPassword').filter('[value=\'\']:eq(0)').focus();
	}
};

// Onload of page fire event:
$(document).ready(function () {
	acctModal.initEvents(); // Masthead Signin link
	myLowesLoginModal.onload(); // Modal login
	//Lowes.My.Account.Signup.init(); // Signup
});

/* ../global/Lowes/My/signin.js */

/**
 * Sign In Form Initialization.
 * @author NP
 */
(function () {
	var Lowes = window.Lowes || {},
		My = Lowes.My || {},
		Account = My.Account || {},

		Auth = {
			init: function () {
				var pageID = (location.pathname.split('/').reverse())[0];
				if (Lowes.Prefs.data.mylowes !== undefined && 'submit' in Lowes.Prefs.data.mylowes) {

					var eventType = (Lowes.Prefs.data.mylowes.submit === undefined) ? 'SignIn-SignUp' : Lowes.Prefs.data.mylowes.submit,
						regSource, regType, checkedOption, signUpInterval, loginInterval, loginFailureInterval, tagData;
					// gather and state cookie data set on signup/setup
					// check for value of registration type
					if (($('[data-trackingtype]').length)) {
						regType = $('[data-trackingtype]').data('trackingtype');
					} else if (Lowes.Prefs.data.mylowes.regType !== undefined) {
						regType = Lowes.Prefs.data.mylowes.regType;
					} else {
						regType = Lowes.Prefs.data.mylowes.submit;
					}
					// check for value of registration source
					if (Lowes.Prefs.data.mylowes.regSource !== undefined) {
						regSource = Lowes.Prefs.data.mylowes.regSource;
					} else {
						regSource = Lowes.Prefs.data.mylowes.submit;
					}
					// check for value of subscriptions check box
					if (Lowes.Prefs.data.mylowes.checkedOption !== undefined) {
						checkedOption = Lowes.Prefs.data.mylowes.checkedOption;
					} else {
						checkedOption = 'Subscription Checkbox Not in Use';
					}

					if (!$('.ui-error').size() && !$('.ui-formerror').size() || $('.ui-error').html() === '') {

						if (Lowes.Prefs.data.mylowes.submit === 'Sign Up' || Lowes.Prefs.data.mylowes.submit === 'Purchase Tracking') {
							cmCreateConversionEventTag("My Lowes", "2", regSource + " - " + regType, "",
								(Lowes.Prefs.data.mylowes.submit === 'Sign Up') ?
								cmAttrCreator({
									1: checkedOption
								})
								:
								cmAttrCreator({})
							);

							// Fire utag link for Successful Signup + for Checked Email Option Sign Up
							if (Lowes.Prefs.data.mylowes.checkedOption !== undefined && checkedOption === "Yes") {
								tagData = "registration-success,email-success";
							} else {
								tagData ="registration-success";
							}
							Lowes.Metrics.ltag({
								event_type: tagData,
								reg_type: regType,
								reg_source: regSource
							}, "tealium");

						} else if (Lowes.Prefs.data.mylowes.submit === 'Sign In') {
							cmCreateConversionEventTag("My Lowes", "2", regSource, "",
								cmAttrCreator({
									1: checkedOption,
									2: pageID
								})
							);

							// Fire utag link for Successful Sign In
							Lowes.Metrics.ltag({
								event_type: 'login_success'
							}, "tealium");
						}

						// Fire the CMX RegistrationTag when the user signs in or registers
						var userDetails = window.$.parseJSON(window.Lowes.Cookie.get('userDetails')),
							attributes = [];

						if (userDetails) {
							attributes[0] = userDetails.id;
							attributes[1] = userDetails.logon;

							if (Lowes.Prefs.data.mylowes.submit === 'Sign In') {
								attributes[17] = Lowes.Prefs.data.mylowes.submit; // extra attribute rg11
							} else {
								attributes[17] = Lowes.Prefs.data.mylowes.submit + ' - ' + regType; // extra attribute rg11
							}

							attributes[18] = Lowes.User.CurrentStore.name; // extra attribute rg12
							attributes[19] = Lowes.User.CurrentStore.number; // extra attribute rg13
							attributes[20] = Lowes.User.CurrentStore.state; // extra attribute rg14
							attributes[21] = Lowes.User.CurrentStore.zip; // extra attribute rg15
							attributes[22] = ((!$.cookie('logonId')) ? 'Not logged in : Not Authenticated' : 'Logged in'); // extra attribute rg1

							// Call into Coremetrics function for logging the registration event
							window.cmCreateRegistrationTag.apply(this, attributes);
						}

					} else {
						cmCreatePageElementTag("MY LOWES ERROR", $('.ui-error').find('p').text(),
							cmAttrCreator({
								1: 'Page Id: ' + pageID
							})
						);

						// If Sign In Fails
						if (Lowes.Prefs.data.mylowes.submit === 'Sign In') {
							// Fire utag link for Failed Sign In
							// check for utag before calling
							loginFailureInterval = window.setInterval(function(){
								if (typeof utag === 'object') {
									utag.link({event_type:"login-fail"});
									clearInterval(loginFailureInterval);
								}
							}, 1000);
						}

					}
					// remove temporary cookie data
					delete Lowes.Prefs.data.mylowes.submit;
					delete Lowes.Prefs.data.mylowes.regType;
					delete Lowes.Prefs.data.mylowes.regSource;
					delete Lowes.Prefs.data.mylowes.checkedOption;
					Lowes.Prefs.save();
				}
			}
		},

		SignIn = {
			form: $("#Logon"),

			/**
			 * Init on start of page for signin
			 * Bind Submit Form events, Keyup on Signin user name field for core metrics.
			 * Set Remember me check by from reading a cookie.
			 * @return none.
			 */
			init: function () {
				var $logonForm = this.form;
				// set up mbox for sign in
				$('#Logon .btn-container').attr('id', 'signInSubmit');
				mboxDefine("signInSubmit", "mylowesSignInSubmit");
				mboxUpdate("mylowesSignInSubmit");

				//SignIn.form.bind('submit.signin', this.__logonSubmit);
				$logonForm.on('click', 'button.primary', function (e) {
					e.preventDefault();
					SignIn.__logonSubmit();
				});
				//.find('[name=Ecom_User_ID], [name=logonPassword]').one('http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/keyup.CM', this.__logonSubmit);
				$logonForm.validate().settings.highlight = function(element, errorClass) { $(element).closest("li").addClass(errorClass); };
				$logonForm.validate().settings.unhighlight = function(element, errorClass) { $(element).closest("li").removeClass(errorClass); };

				// Operate on Login Form after starting checkout
				// Make sure we're on the Sign In Form after Start Checkout
				if (typeof utag === 'object' && utag_data.page_type === 'checkout-login') {
					// Scroll to first error in form on page load
					if ( $('.error').first().length ) {
						$(window).scrollTop($('.error').first().offset().top);
					}
					// Get the Login Button
					var $loginBtn = $("#Logon").find('.sign-in-links a.primary');
					// add proper click event to Submit button
					$loginBtn.on('click', function (e) {
						if (typeof utag === 'object') {
							utag.link({event_type:"login-checkout"});
						}
					});
					// Add Event for Enter Key
					$logonForm.on('keydown', function (e) {
						if(event.keyCode == 13) {
							document.Logon.submit();
							if (typeof utag === 'object') {
								utag.link({event_type:"login-checkout"});
							}
						}
					});
				}

				// Set Remember checkbox
				if (Lowes.Cookie.get('rememberMe')) {
					SignIn.form.find('#rememberme').prop('checked', 1).end().find('#Ecom_User_ID').val(Lowes.Cookie.get('logonId'));
				}

				// Onload focus, 1sec delay to help buffer javascript load, wait for page to render.
				// If user has not already clicked into a field, do auto focus.

				setTimeout(function () {
					var $formFields = Lowes.My.Account.SignIn.form.find('#Ecom_User_ID, #logonPassword');
					//Do not run focus if fields have error
					if(!$formFields.closest('.error').length){
						// Focus on Email or else Password, if email is not empty focus on password.
						// if both email+password are filled, do no focus.
						$formFields.filter('[value=\'\']:eq(0)').focus();
					}
					
				}, 1000);

			},
			__logonSubmit: function (e) {
				//e.preventDefault(); // Removed button changed logic to Submit button type (no longer <a> link)
				//var loginSources = ["Modal","Basic Page","Custom Page", "Checkout", "Reminder", "Favorites"];

				var rememberMeVal = ($("#rememberme").is(":checked")) ? "Yes" : "No",
					pageID = (location.pathname.split('/').reverse())[0],
					$logonForm = this.form;

				if ($logonForm.is('form')) {

					if ($logonForm.valid()) {

						cmCreateConversionEventTag("My Lowes", "1", "Sign In", "",
							cmAttrCreator({
								1: rememberMeVal
							})
						);
						Lowes.Prefs.data.mylowes = {'submit': 'Sign In', 'checkedOption': rememberMeVal};
						Lowes.Prefs.save();

						//return true;
						setTimeout(function () {SignIn.form.submit();}, 400);

					} else {
						// errored out login modal needs to be resized
						if ($('body.lowes_account_login_modal').length) {
							var protocol = $(document).getUrlParam('protocol') || $('#tf_protocol').val(); // http or https
							$('body').append('<iframe src="' + protocol + '://' + window.location.host + '/MContent/Structured/Member/account_iframe_success.html?callback=acctModal.resizeOnloadSigninError" style="width:0px;height:0px;display:none;"/>');
						}
						//acctModal.resizeOnloadSigninError();
						return false;
					}
				} else {
					return false;
				}
			}
	};

	Lowes.My.Account.SignIn = SignIn;
	Lowes.My.Account.Auth = Auth;
	window.Lowes = Lowes;
}());

//Self-executing
Lowes.Prefs.load();
if (Lowes.My.Account.SignIn.form.length) {
	Lowes.My.Account.SignIn.init(); // Signin Page Submit button, Set remember me
}
if (!!Lowes.Prefs.data.mylowes) {
	Lowes.My.Account.Auth.init();
}

/* ../global/Lowes/My/password.js */

/**
 *	Password related functionality
 *	A lot of this overlaps and can be cleaner.
 */ (function () {
	var Lowes = window.Lowes || {};
	var My = Lowes.My || {};
	var Account = My.Account || {};
	var Password = {
		init: function () {
			this.reset();
			this.forgot.init();
		},
		resetButton: $('#resetPasswordBtn'),
		forgotButton: $('#forgotPasswordSubmit'),
		resetForm: $('#ResetPasswordExpired'),
		forgotForm: $('#ResetPassword'),
		reset: function () {
			if (this.resetButton.length && this.resetForm.length) {
				/* Commented out because <a> link is now changed to <button submit>
				$resetPasswordBtn.click(function(){
					this.resetForm.submit();
				});*/

				// Validation bug Defect#19247
				this.resetForm.find('#resetLogonId').bind('blur', function () {
					Lowes.My.Account.Password.resetForm.validate().element("#resetLogonId");
				});

				this.resetForm.find('#resetLogonId').bind('focus', function () {
					Lowes.My.Account.Password.resetForm.validate().resetForm();
				});
				// end defect#19247
				this.resetForm.unbind().bind('submit', {
					self: this
				}, function (e) {
					var $this = $(this);
					// Validate for any errors:
					if (e.data.self.resetForm.valid()) {
						e.data.self.__disableResetPasswordBtn();

						// Submit Resetpassword request:
						$.ajax({
							type: 'POST',
							data: {
								URL: $this.find('input[name="URL"]').val(),
								errorURL: $this.find('input[name="errorURL"]').val(),
								storeId: Lowes.storeId,
								catalogId: Lowes.catalogId,
								langId: Lowes.langId,
								resetLogonId: $this.find('input[name="resetLogonId"]').val()
							},
							url: '/AjaxResetPasswordRequestS',
							success: function (response) {
								var responseObj = JSON.parse(response);

								e.data.self.__enableSendBtn();
								if (response.indexOf('ERROR_PSWORD_RESET_REQUEST_INVALID_ID') > -1) {
									e.data.self.__enableResetPasswordBtn();

									/*
									Commented out code, replaced with validation showErrors() method.
									var $parent = $('#resetLogonId').parent('li');
									var $error = $parent.find('.error');
									$parent.addClass('error');
									if ($error.length) {
										$error.html( Lowes.My.Account.Password.forgot.msg.emailNotFound ).show();
									} else {
										$parent.append('<label for="resetLogonId" generated="true" class="error">' + lowes_forgot_pass.msg.emailNotFound + '</label>');
									}*/

									var objErrors = {};
									objErrors['resetLogonId'] = Lowes.My.Account.Password.forgot.msg.emailNotFound;
									//$('#ResetPassword.ui-validateform').validate().showErrors(objErrors);
									Lowes.My.Account.Password.resetForm.validate().showErrors(objErrors);

									return false;
								}
								// Success, replace dialog content
								$('#ResetPasswordBody,#ResetPasswordExpired .ui-error').hide();
								cmCreateConversionEventTag("MANAGE ACCOUNT PASSWORD RESET", "2", "MY LOWES");
								// $('#forgotPasswordEmailSentDialog').show().css('padding', '10px');

								// Adjust Expiration Time in Confirmation
								var confirmationMsg = $("ul.successfulPwReset");
								if (responseObj.linkValidTime !== confirmationMsg.find("#expireTime").text()) {
									confirmationMsg.find("#expireTime").text(responseObj.linkValidTime);
								}
								// Show confirmation message
								confirmationMsg.fadeIn("fast");
							}
						});
					} else {
						e.data.self.__enableResetPasswordBtn();
					}
					return false;
				});
			}
		},
		__enableResetPasswordBtn: function () {
			// Enable submit button
			var self = this;
			this.resetButton.css('opacity', '1').unbind().click(function () {
				self.resetForm.submit();
			});
			return this;
		},
		__disableResetPasswordBtn: function () {
			// Disable submit button
			this.resetButton.css('opacity', '.5').click(function () {
				return false;
			});
			return this;
		},
		__enableSendBtn: function () {
			this.forgotButton.css('opacity', 1).unbind('click').bind('click', {
				self: this
			}, function (e) {
				e.data.self.forgot.send(e);
			});
			return this;
		},
		__disableSendBtn: function () {
			this.forgotButton.css('opacity', 0.5).unbind('click').bind('click', function (e) {
				return false;
			});
			return this;
		},
		forgot: {
			send: function (e) {
				e.preventDefault();
				// Disable Send button.
				Password.__disableSendBtn();
				// Validation Failed, re-enable submit button
				if (!Password.forgotForm.valid()) {
					Password.__enableSendBtn();
					return false;
				}
				// Ajax request submit email rquest.
				$.ajax({
					type: 'POST',
					data: {
						URL: Password.forgotForm.find('input[name="URL"]').val(),
						errorURL: Password.forgotForm.find('input[name="errorURL"]').val(),
						storeId: Lowes.storeId,
						catalogId: Lowes.catalogId,
						langId: Lowes.langId,
						resetLogonId: Password.forgotForm.find('input[name="resetLogonId"]').val()
					},
					url: '/AjaxResetPasswordRequestS',
					success: function (response) {
						var responseObj = JSON.parse(response);

						Password.__enableSendBtn();
						if (response.indexOf('ERROR_PSWORD_RESET_REQUEST_INVALID_ID') > -1) {
							/*var $parent = $('#resetLogonId').parent('li');
							var $error = $parent.find('.error');
							$parent.addClass('error');
							if ($error.length) {
								$error.html(e.data.self.msg.emailNotFound).show();
							} else {
								// $parent.append('<label for="resetLogonId" generated="true" class="error">' + + '</label>');
							}*/
							var objErrors = {};
							objErrors['resetLogonId'] = Lowes.My.Account.Password.forgot.msg.emailNotFound;
							$('#ResetPassword.ui-validateform').validate().showErrors(objErrors);
							return false;
						}
						cmCreateConversionEventTag("MANAGE ACCOUNT FORGOT PASSWORD", "2", "MY LOWES");
						// Success, replace dialog content
						Password.forgotForm.hide();

						// Adjust Expiration Time in Confirmation
						var confirmationMsg = $('#forgotPasswordEmailSentDialog');
						if (responseObj.linkValidTime !== confirmationMsg.find("#expireTime").text()) {
							confirmationMsg.find("#expireTime").text(responseObj.linkValidTime);
						}
						// Show Confirmation Message
						confirmationMsg.show();

						// forgot password executed from signin modal then resize thankyou screen.
						if (Lowes.My.Account.Password.forgot.isInsideModal()) {
							var protocol = $(document).getUrlParam('protocol') || $('#tf_protocol').val(); // http or https
							$('body').append('<iframe src="' + protocol + '://' + window.location.host + '/MContent/Structured/Member/account_iframe_success.html?callback=acctModal.resizeOnloadForgotPassThankYou" style="width:0px;height:0px;display:none;"/>');
						}
					}
				});
			},
			dialog: $('#forgotPasswordDialog'),
			// Check if forgot password is being loaded from inside a modal.
			isInsideModal: function () {
				return $('body.lowes_account_login_modal').length;
			},
			msg: {
				emailNotFound: "We're Sorry; we don't have an account associated with this email address. Please try again.",
				invalidEmail: this.emailNotFound,
				emptyEmail: 'Please enter your email address.',
				password: ''
			},
			init: function () {
				// Enter key inside form trigger submit.
				// This will prevent
				Password.forgotForm.unbind('submit').bind('submit', function () {
					$('#forgotPasswordSubmit').trigger('click'); // Call Lowes.My.Account.Forgot.password.send() method.
					return false; // prevent page from reloading.
				});

				if (!this.isInsideModal()) {
					this.dialog.dialog({
						autoOpen: false,
						draggable: false,
						modal: true,
						position: 'center',
						resizable: false,
						width: 382,
						closeText: 'Close'
					});
				}

				// Close Dialog window
				$('#forgotPasswordCancel').bind('mousedown click', {
					self: this
				}, function (e) {
					e.preventDefault();
					var forgot = e.data.self || Lowes.My.Account.Password.forgot;
					var protocol = $(document).getUrlParam('protocol') || $('#tf_protocol').val(); // http or https
					if (forgot.isInsideModal()) {
						forgot.dialog.hide(); // hide forgot password area
						Lowes.My.Account.SignIn.form.show(); // show signin area
						$('body').append('<iframe src="' + protocol + '://' + window.location.host + '/MContent/Structured/Member/account_iframe_success.html?callback=acctModal.resizeOnloadSigninError" style="width:0px;height:0px;display:none;"/>').find('#Ecom_User_ID').focus();
					} else {
						forgot.dialog.dialog('close');

					}
					// Reset submit button
					Lowes.My.Account.Password.__enableSendBtn();
				});

				// Bind New send action to submit button
				Password.forgotButton.unbind('click').bind('click', {
					self: this
				}, this.send);
				//var $forgotPasswordLink = $('#forgotPasswordLink');
				var $forgotPasswordLinks = $('#forgotPasswordLink, .unclaimed-acct-message a');

				if ($forgotPasswordLinks.length) {
					// Forgot password link loads dialog popup
					$forgotPasswordLinks.bind('click', {
						self: this
					}, function (e) {
						e.preventDefault();
						var forgot = e.data.self || Lowes.My.Account.Password.forgot;
						$('#resetLogonId').val($('#Ecom_User_ID').val()).addClass('email');
						if($("#resetLogonId").val().length == 0){
							$('#resetLogonId').val($('#email1').val());
						}

						//$('#resetLogonId').val($('#Ecom_User_ID').val()).addClass('email');


						$('.page-errors,.resetLogonId,#forgotPasswordEmailSentDialog').hide();
						$('#ResetPassword').show();

						$('#resetLogonIdLI').removeClass('error');

						if (forgot.isInsideModal()) {
							// Inside a modal
							var protocol = $(document).getUrlParam('protocol') || $('#tf_protocol').val(); // http or https
							forgot.dialog.show(); // show forgot password area
							// Style UL tag
							// Use CSS
							forgot.dialog.find('ul').css('border', 'none').end().find('h3').css('textAlign', 'left');

							$('#forgotPasswordEmailSentDialog,#Logon').hide(); // hide signin and thank you for register area
							// Add hidden iframe to talk to parent window to resize modal, focus for input
							$('body').append('<iframe src="' + protocol + '://' + window.location.host + '/MContent/Structured/Member/account_iframe_success.html?callback=acctModal.resizeOnloadForgotPass" style="width:0px;height:0px;display:none;"/>').find('#resetLogonId').focus();
						} else {
							// Not inside Modal, page by it self.
							forgot.dialog.dialog('open');
						}
					});
				}
			}
		}
	};
	Lowes.My.Account.Password = Password;
	window.Lowes = Lowes;
}());

/* Change Password Page for forgot password.  */
 (function () {
	var Lowes = window.Lowes || {};
	var My = Lowes.My || {};
	var Account = My.Account || {};
	/* Change Password Page for forgot password.  */
	var PasswordChange = {
		init: function () {
			var $form = $('#ResetPasswordForm');
			// Reset Password, Change password page.
			if ($form.length) {
				$('#logonPassword').val('').focus(); // Onload Start focus of input
				// Submit Change password
				$form.unbind('submit').bind('submit', function () {
					return ($form.valid() ? true : false);
				});
			}
		}

	};
	Lowes.My.Account.PasswordChange = PasswordChange;
	window.Lowes = Lowes;
}());
/* End Change Password Page for forgot password.  */

// Self-executing
Lowes.My.Account.Password.init();
Lowes.My.Account.PasswordChange.init();

/* ../global/Lowes/modules/diy-designer-redirect.js */

/**
 * @fileOverview DIYDesignerRedirect | Utility for adding store data to diy designer and patio config tool links
 */
(function (window, document, $) {

	var Lowes = window.Lowes || {},

		/**
		 * DIY Designer Redirect
		 * @class
		 * @name DIYDesignerRedirect
		 */
		DIYDesignerRedirect = {
			/** @lends DIYDesignerRedirect */

			designerType: 'deck', // default designer type, can be overridden when calling init
			$originalLink: null, // holder for jquery object passed through init
			originalUrl: '', // original url sent via link
			urlObject: {}, // holder object for result of Lowes.Utils.urlQueryObject()
			newStoreData: {}, // holder object for data returned from ZipCodeModal module
			currentStoreNumber: (typeof Lowes.User.CurrentStore.number === "undefined") ? '' : Lowes.User.CurrentStore.number, // initial state of storeNumber
			newStoreNumber: '', // holder for new store number
			currentStoreState: (typeof Lowes.User.CurrentStore.state === "undefined") ? '' : Lowes.User.CurrentStore.state, // initial state of storeState
			newStoreState: '', // holder for new store state
			currentStoreZipCode: (typeof Lowes.User.CurrentStore.zip === "undefined") ? '' : Lowes.User.CurrentStore.zip, // initial state of storeZipCode
			newStoreZipCode: '', // holder for new store state
			deckUrlBase: 'http://lowestools.diyonline.com/deck/entrypage.jsp', //?storeID=0595&isAssociate=0
			rampUrlBase: 'http://lowestools.diyonline.com/carsd/entrypage.jsp', //?storeid=0595&from_entrypage=1&cm_sp=BuildingSupplies-_-WheelchairRamps|M1-_-SellingTool|Gatehouse_Wheelchair_Ramps_Design';,
			patioUrlBase: 'http://patio.easy2.com/lowes/visualizer', //?storeNumber=1112&state=NC&zip=28277&cm_sp=OutdoorLiving-_-Patio|M1-_-SellingTool|Allen_Roth_Custom_Patio_Collection';,
			generatedUrl: '', // holder for new generated url

			/**
			 * Send a jquery object containing links and the designer type to start up link modification
			 * @memberOf DIYDesignerRedirect
			 * @param  {object} link         jQuery object via element selection, ie. $('.mylink')
			 * @param  {[string]} designerType [DIY Designer type 'deck', 'ramp', or 'patio']
			 * @function
			 */
			init: function ($link, designerType) {
				// cache link, this should be a jquery obj sent via init
				this.$originalLink = $link;
				this.originalUrl = $link.attr('href');
				// Parse URL Query Params into an Object
				//this.parseURL();
				this.urlObject = Lowes.Utils.urlQueryObject(this.originalUrl);
				// store designer type
				this.designerType = designerType;

				if (this.currentStoreNumber === '') {
					// setup our zipcode modal
					Lowes.ZipCodeModal.init($link, Lowes.DIYDesignerRedirect.processStoreData);
					// bind new event to deck designer button
					this.bindLinkEvents();
				} else {
					// parse url, then update with current store id
					this.newStoreNumber = Lowes.User.CurrentStore.number;
					this.newStoreState = Lowes.User.CurrentStore.state;
					this.newStoreZipCode = Lowes.User.CurrentStore.zip;
					this.updateLink();
				}
			},

			/**
			 * Bind events to existing diy designer link
			 * @memberOf DIYDesignerRedirect
			 * @param  {object} link         jQuery object via element selection, ie. $('.mylink')
			 * @function
			 */
			bindLinkEvents: function () {
				this.$originalLink.on('click.zipModalLink', function (event) {
					if (typeof Lowes.DIYDesignerRedirect.newStoreData.KEY !== "undefined") {
						return true;
					} else {
						//event.preventDefault();
						Lowes.ZipCodeModal.showModal();
						return false;
					}
				});
			},

			/**
			 * Callback function used to process data from external module
			 * @memberOf DIYDesignerRedirect
			 * @param  {object} data         Closest store data returned from Lowes.ZipCodeModal Module
			 * @function
			 */
			processStoreData: function (data) {
				// this may be called from an exteral module so it needs to be scoped via the full namespace
				Lowes.DIYDesignerRedirect.newStoreData = data;
				Lowes.DIYDesignerRedirect.newStoreNumber = data["KEY"];
				Lowes.DIYDesignerRedirect.newStoreState = data["STATE"];
				Lowes.DIYDesignerRedirect.newStoreZipCode = data["ZIP"];
				Lowes.DIYDesignerRedirect.triggerUpdatedLink();
			},

			/**
			 * Build URL Based on global type
			 * @memberOf DIYDesignerRedirect
			 * @function
			 */
			buildNewURL: function () {
				var newURL;
				// build new url with
				if (this.designerType === 'deck') {
					newURL = this.deckUrlBase + '?storeID=' + this.newStoreNumber + '&isAssociate=0';
				} else if (this.designerType === 'ramp') {
					newURL = this.rampUrlBase + '?storeID=' + this.newStoreNumber;
				} else if (this.designerType === 'patio') {
					newURL = this.patioUrlBase + '?storeNumber=' + this.newStoreNumber + '&state=' + this.newStoreState + '&zip=' + this.newStoreZipCode;
				}
				// check for promo code
				if (typeof this.urlObject["cm_sp"] !== 'undefined' && this.urlObject["cm_sp"] !== '') {
					newURL = newURL + '&cm_sp=' + this.urlObject["cm_sp"];
				}
				// check for wscommerce promo code
				if (typeof this.urlObject["cm_cr"] !== 'undefined' && this.urlObject["cm_cr"] !== '') {
					newURL = newURL + '&cm_cr=' + this.urlObject["cm_cr"];
				}
				return newURL;
			},

			/**
			 * Update HREF of passed in link
			 * @memberOf DIYDesignerRedirect
			 * @function
			 */
			updateLink: function () {
				this.generatedUrl = this.buildNewURL();
				$(this.$originalLink).attr('href', this.generatedUrl);
			},

			/**
			 * Trigger update of page with newly built link
			 * @memberOf DIYDesignerRedirect
			 * @function
			 */
			triggerUpdatedLink: function () {
				// update link url
				this.updateLink();

				// Reload page with designer link
				window.location = this.generatedUrl;
				// Open new page with designer link
				//window.open(this.generatedUrl, "_blank");
			}
		};

	Lowes.DIYDesignerRedirect = DIYDesignerRedirect;
	window.Lowes = Lowes;

}(window, document, jQuery));

// check for a tags before calling init
// - Deck Designer Link - $('a.button[title="Design Your Deck"]')
var deckDesignerLink = $('#content-block').find('a[title="Design Your Deck"]');
if (deckDesignerLink.length) {
	Lowes.DIYDesignerRedirect.init(deckDesignerLink, 'deck');
}

// - Ramp Designer Link - $('a.button[title="Design Your Deck"]')
// - http://www.lowes.com/Accessible-Home/Wheelchairs-Mobility-Aids/Wheelchair-Ramps-Components/_/N-1z0x3hz/pl
var rampDesignerLink = $('#content-block').find('a[title="Design Yours Now"]');
if (rampDesignerLink.length) {
	Lowes.DIYDesignerRedirect.init(rampDesignerLink, 'ramp');
}

// - Customize Your Patio Set Link - $('a[href*="patio-furniture-visualizer"]')
var CustomizeYourPatioSetLink = $('#page-block, .grid-container').find('a[href*="patio.easy2.com/lowes/visualizer"]');
if (CustomizeYourPatioSetLink.length) {
	Lowes.DIYDesignerRedirect.init(CustomizeYourPatioSetLink, 'patio');
}

// - 2013 Custom Patio Collections - $('a[href*="patio-furniture-visualizer"]')
var lastYearsPatioCollections = $('#page-block, .grid-container').find('a[href*="patio-furniture-visualizer"]');
if (lastYearsPatioCollections.length) {
	Lowes.DIYDesignerRedirect.init(lastYearsPatioCollections, 'patio');
}

/* ../global/lib/UI/jquery.lowes.lazyload.js */

(function($){
	// Grab digitalData namespace object or create a new one.
	var digitalData = window.digitalData || {};
	// Grab digitalData UI namespace object or create a new one.
	digitalData.UI = digitalData.UI || {};
	// Grab digitalData UI lazyLoad namespace object or create a new one.
	digitalData.UI.lazyLoad = digitalData.UI.lazyload || {};
	/**
	 * Lazy Load will load in a list of html fragments one path name at a time 
	 * when the bottom of the targeted container is reached. 
	 * @name jQuery.lowes.lazyload
	 * @extends jQuery.Widget
	 */	
	 $.widget( "lowes.lazyload", {
		/**
		 * Options that define the behavior of the lazyload.
		 * @memberOf jQuery.lowes.lazyload.prototype
		 */
		options : {
			/**
			 * List of URLs that point to HTML fragments (Banner sections)
			 * @type {Array.<string>} 
			 */
			fragments : [],
			/**
			 * Amount of buffer in pixels that is added to the trigger point
			 * for which lazyload adds content
			 * @type {number}
			 */
			buffer: window.innerHeight,
			/**
			 * The time in which we load content outside the scope of buffer. 
			 * @type {number} in milliseconds
			 */
			loadDelay: 0
		},
		/**
		 * Creates a new lazyload on targeted element
		 */
		_create : function(){
			var self = this,
				htmlLnth = self.options.fragments.length;
			self.fragCount = 0;
			self.$throbber = $('<div class="ui-throbber grid-100" style="width:30px; height:30px; text-align:center; margin:10px auto"></div>');			
			self.$body = $('body');
			self.$win = $(window);
			self.loading = false;
			self.count = 0;
			self.$element = $(this.element);
			self.$throbber.spin();
			self.$element.after(self.$throbber);
			if(htmlLnth) {
				self.fragCount = htmlLnth;
				self._bindings();
			}
			self._maxDelay();
		},
		/**
		 * Binding the lazy load event to the browser window
		 */
		_bindings : function(){
			var self = this;
			self.$win
				.on('scroll touchmove', function(){
					self.$win.trigger('http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/scroll.lazy')
					})
				.on('http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/scroll.lazy', function() {
					//load get new fragment based on scroll location
					if(self._compareLoc()) {
						self._validateContentLoad();
					}
				})
				.on('load', function(){
					//load content if the parent grid container is shorter than viewport
					if(self.$element.closest('.grid-container').height() <= self.$win.innerHeight()) {
						self._validateContentLoad();
					}
				});
			},
		/**
		 * Maximum time allowed to load content.
		 */
		_maxDelay : function() {
			var self = this;
			if(self.options.loadDelay) {
				setTimeout((function() {
					self._validateContentLoad()
				}), self.options.loadDelay);
			}
		},
		
		/**
		 * Window scroll function NEEDS MORE DOC
		 */
		_validateContentLoad : function() {
			var self = this;
			//Are there any more fragments to load 
			if(self.count < self.fragCount) {
				if(!self.loading) {
					self._loadContent();
				}
			}else {
				//destroy binding if no more fragments are left
				self._unbind();
				self.$throbber.remove();
			}

		},
		/**
		 * Loads new content in the DOM when scroll criteria is met
		 */
		_loadContent : function() {
			var self = this;
			self.loading = true;
			$.ajax({
				type: 'GET',
				dataType: 'html',
				url : self.options.fragments[self.count],
				beforeSend: function() {
					// Update digitalData With Lazy Loading Data
				   digitalData.UI.lazyLoad.parent = window.location.href;
				   digitalData.UI.lazyLoad.fragment = this.url;
				},
				success: function(response){
					self.$element.append(response);
				},
				complete: function () {
					self.count++;
					self._maxDelay();
					self.loading = false;
					// DTM Direct Call Rule For Tracking Lazy Loads
					_satellite.track('lazyLoad');
				}
			});

		},
		/**
		 * Compares the container location to the viewport (based on bottom of window)
		 * @returns {boolean} true if scroll meets or exceeds position of buffer 
		 */
		_compareLoc : function(){
			var self = this,
				portPos = this._getViewportBot(),
				elPos = this._getElementBot();
			if(portPos >= (elPos - self.options.buffer)){
				return true
			}else{
				return false
			}
		},
		/**
		 * Gets the current scroll position of the viewport (based on bottom of window)
		 * @returns {number} 
		 */
		_getViewportBot : function(){
			return Math.floor(this.$win.scrollTop() + this.$win.innerHeight());
		},
		/**
		 * Gets the current position of the targeted container (based on bottom of element)
		 * @returns {number}
		 */
		_getElementBot : function(){
			return Math.floor(this.$element.offset().top + $(this.element).height() );
		},
		/**
		 * Destroys the bind
		 */
		_unbind : function(){
			this.$win.off('http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/scroll.lazy');
		}
	});
}(jQuery));

/* ../global/tnt.js */

(function($){
	var $body = $('body'),
		supports_html5_storage = function () {
			try {
				localStorage.setItem('lowes', 'lowes');
				localStorage.removeItem('lowes');
				return true;
			} catch (e) {
				return false;
			}
		};

	// The following mbox is for personalization only. Please DO NOT use for other tests.
	// DSD-6862
	var pcid = mboxFactoryDefault.getPCId().getId();
	$body.append('<div id="personalizationMbox"></div>');
	mboxDefine('personalizationMbox', 'personalizationMbox');
	if (Lowes.User.isZippedIn()){
		var currentStore = Lowes.User.getCurrentStore();
		mboxUpdate('personalizationMbox', 'profile.pcid='+pcid, 'profile.storeId='+currentStore.number, 'profile.zip='+currentStore.zip, 'profile.storeState='+currentStore.state);
	}
	else{
		mboxUpdate('personalizationMbox', 'profile.pcid='+pcid);
	}

}( jQuery ));
