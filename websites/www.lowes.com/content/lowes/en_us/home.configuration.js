












var configuration = {"minifyEnabled":true,"catalogId":"10051","cqCssMinifiedUrl":"../../../wcsstore/B2BDirectStorefrontAssetStore/css/cq/cq-min.css"/*tpa=http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/css/cq/cq-min.css*/,"cqJavaScriptUrl":"../../../wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/cq-cat.js"/*tpa=http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/cq-cat.js*/,"langId":"-1","cqHeadJavaScriptMinifiedUrl":"../../../wcsstore/B2BDirectStorefrontAssetStore/javascript/cq-head/cq-head-min.js"/*tpa=http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq-head/cq-head-min.js*/,"akamaizedHostnames":["http://www.lowes.com/content/lowes/en_us/sam.lowes.com","http://www.lowes.com/content/lowes/en_us/lws7.lowes.com","http://www.lowes.com/content/lowes/en_us/wstlws.lowes.com","http://www.lowes.com/content/lowes/en_us/salws.lowes.com","http://www.lowes.com/content/lowes/en_us/www.lowes.com","http://www.lowes.com/content/lowes/en_us/lws7p.lowes.com","http://www.lowes.com/content/lowes/en_us/estlws.lowes.com","http://www.lowes.com/content/lowes/en_us/cslws.lowes.com"],"scene7Domain":"https://lda.lowes.com","cqCssUrl":"../../../wcsstore/B2BDirectStorefrontAssetStore/css/cq/cq-cat.css"/*tpa=http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/css/cq/cq-cat.css*/,"scene7UtilsUrl":"https://lda.lowes.com/s7sdk/2.6/js/s7sdk/utils/Utils.js","signInPageCqHref":"http://www.lowes.com/en_us/sign-in.html","signInPageLowesHref":"/webapp/wcs/stores/servlet/LogonForm?langId=-1&storeId=10151&catalogId=10051","cqHeadJavaScriptUrl":"../../../wcsstore/B2BDirectStorefrontAssetStore/javascript/cq-head/cq-head-cat.js"/*tpa=http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq-head/cq-head-cat.js*/,"cqJavaScriptMinifiedUrl":"../../../wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/cq-min.js"/*tpa=http://www.lowes.com/wcsstore/B2BDirectStorefrontAssetStore/javascript/cq/cq-min.js*/,"signInPagePostUrlEnabled":true,"signInPageLowesPostPath":"http://www.lowes.com/en_us/mylowes.html","storeId":"10151","tealiumUrl":"../../../../tags.tiqcdn.com/utag/lowes/lowescq/prod/utag.js"/*tpa=http://tags.tiqcdn.com/utag/lowes/lowescq/prod/utag.js*/,"urlPrefix":"../../../index.htm"/*tpa=http://www.lowes.com/*/,"previewEnabled":true,"runModes":["samplecontent","crx3tar","crx2","publish","prod"]};
var akamaizedHostnames = configuration.akamaizedHostnames;
var hostname = document.location.hostname;
var isAkamaiEnabled = false;

for (var i = 0; i < akamaizedHostnames.length; i++) {
    if (akamaizedHostnames[i] === hostname) {
        isAkamaiEnabled = true;
    }
}

configuration.akamaiEnabled = isAkamaiEnabled;

if (isAkamaiEnabled) {
    configuration.urlPrefix = '';
}