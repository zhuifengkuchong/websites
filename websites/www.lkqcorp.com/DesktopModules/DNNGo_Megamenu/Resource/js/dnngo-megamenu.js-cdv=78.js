(function($) {
	$.fn.dnngomegamenu = function(m) {
		m = $.extend({
			slide_speed: 200,
			delay_disappear: 500,
			megamenuwidth: "box"
		},
		m || {});
		return this.each(function(index) {
			var me = $(this),
			primary = me.find(".primary_structure > li"),
			slide = ".dnngo_menuslide",
			subs = ".dnngo_submenu",
			subbox = "dnngo_boxslide",
			hover = "menu_hover",
			slidedefault = "dnngo_slide_menu",
			interval;
			primary.mouseover(function() {
				var slides = $(this).find(slide);
				clearTimeout(interval);
				if (slides.css('display') == 'none') {
					$(this).addClass("menu_hover");
					slides.css("left", "0");
					var left = $(this).offset().left,
					winwidth = $(window).width(),
					width = slides.width();
					if (slides.find("ul").hasClass(slidedefault)) {
						if (winwidth - left < width) {
							slides.css("left", '-' + parseInt(width + left - winwidth + 5) + 'px');
						}
					}
					if (m.megamenuwidth == "full") {
						if (slides.find("div").hasClass(subbox)) {
							slides.css({
								"width": winwidth,
								"left": -left
							})
						}
					}
					if (m.megamenuwidth == "box") {
						if (slides.find("div").hasClass(subbox)) {
							slides.css({
								"width": $(".dnn_layout").width(),
								"left": -(left - (winwidth - $(".dnn_layout").width()) / 2)
							}
							)
						}
					}
					slides.fadeIn(m.slide_speed);
				}

				$(this).siblings().find(slide).fadeOut(m.slide_speed);
				$(this).siblings().find(subs).fadeOut(m.slide_speed);
				$(this).siblings().find(slide).find("li").removeClass(hover);
				$(this).siblings().find(subs).find("li").removeClass(hover);
				$(this).siblings().removeClass(hover);
			}).mouseout(function() {
				var me = $(this);
				interval = setInterval(function() {
					me.removeClass(hover);
					me.find("*").removeClass(hover);
					me.find(slide).fadeOut(m.slide_speed);
					me.find(subs).fadeOut(m.slide_speed);
					clearTimeout(interval)
				},
				m.slide_speed > m.delay_disappear ? m.slide_speed: m.delay_disappear);
			})

			primary.find("li").mouseover(function() {
				var subbox = $(this).find("> " + subs);

				if (subbox.css('display') == 'none') {
					$(this).addClass(hover);
					subbox.fadeIn(m.slide_speed);
					sub_left = $(this).offset().left + $(this).width(),
					winwidth = $(window).width(),
					sub_width = subbox.width();
					if (winwidth - sub_left < sub_width) {
						subbox.css({
							"left": "auto",
							"right": "100%"
						});
					} else {
						subbox.css({
							"left": "100%",
							"right": "auto"
						});
					}
				}
				$(this).siblings().removeClass(hover);
				$(this).siblings().find(subs).fadeOut(m.slide_speed);

			})
			
			
 
		});
	};
})(jQuery);