
//Google Analytics - Online Analytics and Operations Support
// add domains users visit in a session (to enable cross-domain tracking)

  var ga_domains = [
    'allstate',
    'https://www.allstate.com/resources/scripts/clicktoempower.org',
    'https://www.allstate.com/resources/scripts/defiendetuscolores.com',
    'https://www.allstate.com/resources/scripts/digitallocker.com',
    'https://www.allstate.com/resources/scripts/enviamalasuerte.com',
    'https://www.allstate.com/resources/scripts/goodhands360review.com',
    'https://www.allstate.com/resources/scripts/goodhandsinsurancereview.com',
    'https://www.allstate.com/resources/scripts/houseandhomesweeps.com',
    'https://www.allstate.com/resources/scripts/northlightspecialty.com',
    'https://www.allstate.com/resources/scripts/promotion.bassmaster.com',
    'https://www.allstate.com/resources/scripts/proteccioneslajugada.com',
    'https://www.allstate.com/resources/scripts/protectionisthegame.com',
    'https://www.allstate.com/resources/scripts/protectitorloseit.com',
    'https://www.allstate.com/resources/scripts/purplepurse.com',
    'https://www.allstate.com/resources/scripts/youralltimeteam.com'
  ]
