
	        CQ_Analytics.registerAfterCallback(function(options) {
	            if(!options.compatibility && $CQ.inArray( options.componentPath, CQ_Analytics.Sitecatalyst.frameworkComponents) < 0 )
	                return false;    // component not in framework, skip SC callback
	            CQ_Analytics.Sitecatalyst.saveEvars();
	            CQ_Analytics.Sitecatalyst.updateEvars(options);
	            CQ_Analytics.Sitecatalyst.updateLinkTrackVars();
	            return false;
	        }, 10);
	
	        CQ_Analytics.registerAfterCallback(function(options) {
	            if(!options.compatibility && $CQ.inArray( options.componentPath, CQ_Analytics.Sitecatalyst.frameworkComponents) < 0 )
	                return false;    // component not in framework, skip SC callback
	            s = s_gi("thermofishercorporateprod");
	            if (s.linkTrackVars == "None") {
	                s.linkTrackVars = "events";
	            } else {
	                s.linkTrackVars = s.linkTrackVars + ",events";
	            }
	            CQ_Analytics.Sitecatalyst.trackLink(options);
	            return false;
	        }, 100);
	
	
	        CQ_Analytics.registerAfterCallback(function(options) {
	            if(!options.compatibility && $CQ.inArray( options.componentPath, CQ_Analytics.Sitecatalyst.frameworkComponents) < 0 )
	                return false;    // component not in framework, skip SC callback
	            CQ_Analytics.Sitecatalyst.restoreEvars();
	            return false;
	        }, 200);
	
	        CQ_Analytics.adhocLinkTracking = "true";
	        
	
	
	        var s_account = "thermofishercorporateprod";
	        var s = s_gi(s_account);
	        s.fpCookieDomainPeriods = "2";
	        s.currencyCode= 'USD';
        s.trackInlineStats= true;
        s.linkTrackVars= 'None';
        s.charSet= 'UTF-8';
        s.linkLeaveQueryString= true;
        s.linkExternalFilters= '';
        s.linkTrackEvents= 'None';
        s.trackExternalLinks= true;
        s.linkDownloadFileTypes= 'exe,zip,wav,mp3,mov,mpg,avi,wmv,doc,pdf,xls';
        s.linkInternalFilters= 'javascript:,'+window.location.hostname;
        s.trackDownloadLinks= true;
        
        s.visitorNamespace = "thermofisher";
        s.trackingServer = "http://www.thermofisher.com/en/jcr:content/stats.thermofisher.com";
        s.trackingServerSecure = "http://www.thermofisher.com/en/jcr:content/sstats.thermofisher.com";
        
        s.usePlugins=true;
function s_doPlugins(s)
{ 

/* Set getNewRepeat v1.2   */    
	s.prop3=s.getNewRepeat();
	s.eVar3="D=c3";

/* Set Hierarchy variable
	 //s.hier1=s.prop6+"^"+s.prop5+"^"+s.prop11+"^"+s.prop12+"^"+s.prop13;
	 s.hier1=(s.prop6?s.prop6:'')+(s.prop5?'^'+s.prop5:'')+(s.prop11?'^'+s.prop11:'')+(s.prop12?'^'+s.prop12:'')+(s.prop13?'^'+s.prop13:'');
	 */
	 
/* Concatenate Parameters Examples
	s.eVar52=s.getQueryParam('wt.mc_id,channel,ca,internalcid',':',location); //Replace 52
	s.eVar33=s.getQueryParam('elq_cid,elq_mid',':',location); //Replace 33, 34
	 */

/* getQueryParam 2.3 */

	s.prop20=s.getQueryParam('routerPath',null,location); //DELETE-RECYCLE VARIABLE
	s.prop21=s.getQueryParam('keyword',null,location);
	s.prop22=s.getQueryParam('synonym',null,location);
	s.prop23=s.getQueryParam('browseKeyword',null,location);
	s.prop56=s.getQueryParam('lang',null,location);
	
	s.campaign=s.getQueryParam('wt.mc_id,ca,internalcid',':',location);
    s.prop52=s.getQueryParam('wt.mc_id,ca,internalcid,elq_cid,elq_mid,campaign,adgroup,adwordskeyword',':',location); //Replace eVar52
	
/* campaign s.props */ 	
	s.prop34=s.getQueryParam('wt.mc_id,channel',':',location); //DELETE-RECYCLE VARIABLE
	s.prop36=s.getQueryParam('channel',null,location); //DELETE-RECYCLE VARIABLE
	s.prop40=s.getQueryParam('paidterm',null,location);
	s.prop41=s.getQueryParam('http://www.thermofisher.com/en/jcr:content/wt.srch',null,location);	
	s.prop45=s.getQueryParam('utm_source,utm_medium,utm_campaign',':',location); //Replace 45,46,47
	s.prop46=s.getQueryParam('http://www.thermofisher.com/en/jcr:content/campaign,adgroup,adwordskeyword,wt.mc_id,wt.srch',':',location); //Paid Search Values new
	s.prop47=s.getQueryParam('http://www.thermofisher.com/en/jcr:content/wt.mc_id,paidterm,wt.srch',':',location); //Paid Search Values old
	s.prop48=s.getQueryParam('sitelink',null,location);
	s.prop49=s.getQueryParam('mobilebc',null,location); //DELETE-RECYCLE VARIABLE
	s.prop50=s.getQueryParam('flipbook',null,location); //DELETE-RECYCLE VARIABLE
 	s.prop51=s.getQueryParam('worldtourofinnovation',null,location); //DELETE-RECYCLE VARIABLE
	
/* Copy s.props to eVars */
  	if(s.prop34)s.eVar34="D=c34";
	if(s.prop36)s.eVar36="D=c36";
	if(s.prop40)s.eVar40="D=c40";
	if(s.prop41)s.eVar41="D=c41";
	if(s.prop45)s.eVar45="D=c45";
	if(s.prop46)s.eVar46="D=c46";
	if(s.prop47)s.eVar47="D=c47";
	if(s.prop48)s.eVar48="D=c48";
	if(s.prop49)s.eVar49="D=c49";
	if(s.prop50)s.eVar50="D=c50";
	if(s.prop51)s.eVar51="D=c51";
	
/* campaign eVars */ 	
	s.eVar30=s.getQueryParam('wt.mc_id',null,location);
	s.eVar31=s.getQueryParam('ca',null,location);
	s.eVar33=s.getQueryParam('elq_cid,elq_mid',':',location); //Replace 33, 34
	s.eVar35=s.getQueryParam('sc.em_id',null,location); //DELETE-RECYCLE VARIABLE
  	s.eVar37=s.getQueryParam('adgroup',null,location);
	s.eVar38=s.getQueryParam('adwordskeyword',null,location);
	s.eVar39=s.getQueryParam('campaign',null,location);
	s.eVar42=s.getQueryParam('internalcid',null,location);
	s.eVar43=s.getQueryParam('sc.promo_id',null,location); //DELETE-RECYCLE VARIABLE
	s.eVar53=s.getQueryParam('sc.osa_id',null,location); //DELETE-RECYCLE VARIABLE
	
/* Copy eVars to s.props */
	if(s.eVar30)s.prop30="D=v30"; //External Campaign
	if(s.eVar31)s.prop31="D=v31"; //TURL
	if(s.eVar33)s.prop33="D=v33"; //Eloqua cid mid
	if(s.eVar35)s.prop35="D=v35"; //Email
	if(s.eVar37)s.prop37="D=v37"; //Adgroup
	if(s.eVar38)s.prop38="D=v38"; //Adwords Keyword
	if(s.eVar39)s.prop39="D=v39"; //Adwords Campaign Name
	if(s.eVar42)s.prop42="D=v42"; //Internal Campaign
	if(s.eVar43)s.prop43="D=v43"; //On-site Promotion //DELETE-RECYCLE VARIABLE
	if(s.eVar53)s.prop53="D=v53"; //Off-site Promotion //DELETE-RECYCLE VARIABLE
	
/* Concatenate Campaign Parameter Values */
	
	s.eVar52 = '';
	
if (s.eVar30) {
  s.eVar52 += ":" + s.eVar30; 
}
if (s.eVar31) {
  s.eVar52 += ":" + s.eVar31; 
}
if (s.eVar42) {
  s.eVar52 += ":" + s.eVar42; 
}
if (s.eVar35) {
  s.eVar52 += ":" + s.eVar33; 
}
if (s.eVar39) {
  s.eVar52 += ":" + s.eVar39; 
}
if (s.eVar37) {
  s.eVar52 += ":" + s.eVar37; 
}
if (s.eVar38) {
  s.eVar52 += ":" + s.eVar38; 
}


/* Copy s.props to eVars Non-Campaign */
	if(s.prop9)s.eVar9="D=c9";
	if(s.prop10)s.eVar10="D=c10";
	if(s.prop11)s.eVar11="D=c11";
	if(s.prop14)s.eVar14="D=c14";
	if(s.prop15)s.eVar15="D=c15";
	if(s.prop16)s.eVar16="D=c16";
	if(s.prop17)s.eVar17="D=c17";
	if(s.prop19)s.eVar19="D=c19";
	if(s.prop20)s.eVar20="D=c20";
	if(s.prop21)s.eVar21="D=c21";
	if(s.prop22)s.eVar22="D=c22";
	if(s.prop23)s.eVar23="D=c23";
	if(s.prop56)s.eVar56="D=c56";
	
/* Copy eVars to s.props Non-Campaign */
	if(s.eVar4)s.prop4="D=v4"; //Username
	if(s.eVar5)s.prop5="D=v5"; //Language Section
	if(s.eVar6)s.prop6="D=v6"; //Language
	if(s.eVar7)s.prop7="D=v7"; //Country
	if(s.eVar8)s.prop8="D=v8"; //Logged-In
        if(s.eVar18)s.prop8="D=v18"; //Product CatNo

/* getVisitNum 3.0  */
	s.prop25=s.getVisitNum();
	s.eVar25="D=c25";

/* Days Since Last Visit */     
	s.prop26=s.getDaysSinceLastVisit();
	s.eVar26="D=c26";
 	
/* getTimeParting example with year check (EST) */
    var theDate=new Date();   
  	var currentYear=(theDate.getFullYear());
  	s.prop27=s.getTimeParting('h','-5',currentYear);
  	s.eVar27="D=c27";
  	s.prop28=s.getTimeParting('d','-5',currentYear);
  	s.eVar28="D=c28";

/* Track exit links */
	s.exitURL=s.exitLinkHandler();
	if(s.exitURL){
		s.prop29=s.exitURL;
		
	}
        
/* Get Percent of Page Viewed */
	s.prop24=s.getPercentPageViewed();
        
/* Get Keyword Pagerank */
	s.eVar44=s.searchEngineRank();
	
/* Set pageName = title if it's not already set */                
                if(typeof(s.pageName) == "undefined" && window['pagedata'])
                {
                                s.pageName = pagedata.navTitle+':'+pagedata.subsection+':'+pagedata.language+':'+pagedata.division+':'+pagedata.productLineID+':'+pagedata.code+':'+pagedata.slug;
                }


} 
s.doPlugins=s_doPlugins;



