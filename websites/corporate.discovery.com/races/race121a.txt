<script id = "race121a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race121={};
	myVars.races.race121.varName="Lu_Id_li_16__onmouseout";
	myVars.races.race121.varType="@varType@";
	myVars.races.race121.repairType = "@RepairType";
	myVars.races.race121.event1={};
	myVars.races.race121.event2={};
	myVars.races.race121.event1.id = "Lu_DOM";
	myVars.races.race121.event1.type = "onDOMContentLoaded";
	myVars.races.race121.event1.loc = "Lu_DOM_LOC";
	myVars.races.race121.event1.isRead = "False";
	myVars.races.race121.event1.eventType = "@event1EventType@";
	myVars.races.race121.event2.id = "Lu_Id_li_45";
	myVars.races.race121.event2.type = "onmouseout";
	myVars.races.race121.event2.loc = "Lu_Id_li_45_LOC";
	myVars.races.race121.event2.isRead = "True";
	myVars.races.race121.event2.eventType = "@event2EventType@";
	myVars.races.race121.event1.executed= false;// true to disable, false to enable
	myVars.races.race121.event2.executed= false;// true to disable, false to enable
</script>

