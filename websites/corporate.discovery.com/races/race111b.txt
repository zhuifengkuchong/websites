<script id = "race111b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race111={};
	myVars.races.race111.varName="Lu_Id_li_16__onmouseout";
	myVars.races.race111.varType="@varType@";
	myVars.races.race111.repairType = "@RepairType";
	myVars.races.race111.event1={};
	myVars.races.race111.event2={};
	myVars.races.race111.event1.id = "Lu_Id_li_34";
	myVars.races.race111.event1.type = "onmouseout";
	myVars.races.race111.event1.loc = "Lu_Id_li_34_LOC";
	myVars.races.race111.event1.isRead = "True";
	myVars.races.race111.event1.eventType = "@event1EventType@";
	myVars.races.race111.event2.id = "Lu_DOM";
	myVars.races.race111.event2.type = "onDOMContentLoaded";
	myVars.races.race111.event2.loc = "Lu_DOM_LOC";
	myVars.races.race111.event2.isRead = "False";
	myVars.races.race111.event2.eventType = "@event2EventType@";
	myVars.races.race111.event1.executed= false;// true to disable, false to enable
	myVars.races.race111.event2.executed= false;// true to disable, false to enable
</script>

