/*
Copyright (c) 2012, comScore Inc. All rights reserved.
version: 5.0.3
*/
COMSCORE.SiteRecruit.Builder.config = {
	version: "5.0.3",
	
	// invitations' settings
	invitation: [
							
											{ 	methodology: 2,
					projectId: 'p147490063',
					weight: 90,
					isRapidCookie: false,
					acceptUrl: 'http://ar.voicefive.com/bmx3/projects/srSplash.htm',
					secureUrl: '',
					acceptParams: {
						raw: 'survey_type=2&sUrl=http://survey2.surveysite.com/wix/p147490063.aspx&cookiePid=p147490063',
						siteCode: '1612',
												cookie: [
													],
												otherVariables: [
													]
					},
					viewUrl: 'http://web.survey-poll.com/tc/CreateLog.aspx',
					viewParams: 'http://www.microsoft.com/library/svy/log=comscore/view/p147490063-view.log',
					content: '<table style="width:360px; padding:0px; margin:0px; border-collapse:collapse; border:1px solid #999999; background-color: #FFFFFF;"><tr><td>  <table style="border:0px; border-collapse:collapse; width: 100%;"><tr><td>  	<table style="border:0px; border-collapse:collapse; width: 100%;"><tr valign="top"><td>  		<img src="logo-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/logo-stripe.gif*/ /></td><td><a href="Close" onclick="@declineHandler"><img border="0" src="close.gif"/*tpa=http://www.microsoft.com/library/svy/close.gif*/ /></a></td></tr> <tr><td colspan="2"><img src="top-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/top-stripe.gif*/ /> 		</td></tr> 	</table>  	<table style="width:100%;"><tr><td>   			<div style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 11px; color: #000000; margin: 6px 6px 15px 4px;line-height:14px;">Microsoft is conducting an online survey to understand use of the Windows Shop Web site. We\'d like to see which pages you visit today and then have you complete a survey when you leave the Windows Shop Web site. <br /><br />  Would you like to participate?  </div>  <div align="center" style="margin: 0; padding: 0"> <input style="margin: 0; padding: 0" type="button" value="  Yes  " onclick="@acceptHandler" />&nbsp;&nbsp; <input style="margin: 0; padding: 0"  type="button" value=" No " onclick="@declineHandler" /> </div>  <div style="font-family: Verdana,Arial,Helvetica,sans-serif; font-size:11px; color:#000000; margin:15px 0px 6px 6px;"><a href="http://privacy.microsoft.com/en-us/default.mspx" target="_blank">Privacy Statement</a></div>   <img src="bottom-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/bottom-stripe.gif*/ /></td></tr></table> </td></tr></table> </td></tr></table>   ',
					height: 210,
					width: 390,
					revealDelay: 1000,
					horizontalAlignment: 1,
					verticalAlignment: 1,
					horizontalMargin: 0,
					verticalMargin: 0,
					isHideBlockingElements: false,
					isAutoCentering: true,
					url: 'http://www.microsoft.com/library/svy/SiteRecruit_Tracker.htm',
					trackerGracePeriod: 5
					,usesSilverlight: false
					
					//silverlight config
										
											,trackerWindow: {
							width: 400,
							height: 270,
							orientation: 1,
							offsetX: 0,
							offsetY: 0,
							hideToolBars: true,
							trackerPageConfigUrl: 'trackerConfig_p147490063-EN-US.js'/*tpa=http://www.microsoft.com/library/svy/trackerConfig_p147490063-EN-US.js*/
							// future feature: 
							//features: "location=0,menubar=0,resizable=1,scrollbars=1,toolbar=0"
						}
																				,Events: {
						beforeRender: function() {
														
													}
						,beforeDestroy: function() {
														
													}
						,beforeAccept: function() {
														
													}
						,beforeDecline: function() {
														
													}
						,beforeRenderUpdate: function() {
														
													}
						,afterRender: function() {
														
													}
					}
				}
								
											,
								{ 	methodology: 2,
					projectId: 'p44121997',
					weight: 5,
					isRapidCookie: false,
					acceptUrl: 'http://survey2.surveysite.com/wix/p44121997.aspx',
					secureUrl: '',
					acceptParams: {
						raw: '',
						siteCode: '652',
												cookie: [
													],
												otherVariables: [
													]
					},
					viewUrl: 'http://web.survey-poll.com/tc/CreateLog.aspx',
					viewParams: 'http://www.microsoft.com/library/svy/log=comscore/view/p44121997-view.log',
					content: '<table style="width:360px; padding:0px; margin:0px; border-collapse:collapse; border:1px solid #999999; background-color: #FFFFFF;"><tr><td>  <table style="border:0px; border-collapse:collapse; width: 100%;"><tr><td>  	<table style="border:0px; border-collapse:collapse; width: 100%;"><tr valign="top"><td>  		<img src="logo-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/logo-stripe.gif*/ /></td><td><a href="Close" onclick="@declineHandler"><img border="0" src="close.gif"/*tpa=http://www.microsoft.com/library/svy/close.gif*/ /></a></td></tr> <tr><td colspan="2"><img src="top-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/top-stripe.gif*/ /> 		</td></tr> 	</table>  	<table style="width:100%;"><tr><td>   			<div style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 11px; color: #000000; margin: 6px 6px 15px 4px;line-height:14px;">Microsoft is conducting an online survey to understand use of the Windows Web site.  We\'d like to see which pages you visit today and then have you complete a survey when you leave the Web site.   <br /><br />Would you like to participate? </div>  <div align="center" style="margin: 0; padding: 0"> <input style="margin: 0; padding: 0" type="button" value="  Yes  " onclick="@acceptHandler" />&nbsp;&nbsp; <input style="margin: 0; padding: 0"  type="button" value=" No " onclick="@declineHandler" /> </div>  <div style="font-family: Verdana,Arial,Helvetica,sans-serif; font-size:11px; color:#000000; margin:15px 0px 6px 6px;"><a href="http://privacy.microsoft.com/en-us/default.mspx" target="_blank">Privacy Statement</a></div>   <img src="bottom-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/bottom-stripe.gif*/ /></td></tr></table> </td></tr></table> </td></tr></table>   ',
					height: 210,
					width: 390,
					revealDelay: 1000,
					horizontalAlignment: 1,
					verticalAlignment: 1,
					horizontalMargin: 0,
					verticalMargin: 0,
					isHideBlockingElements: false,
					isAutoCentering: true,
					url: 'http://www.microsoft.com/library/svy/SiteRecruit_Tracker.htm',
					trackerGracePeriod: 5
					,usesSilverlight: false
					
					//silverlight config
										
											,trackerWindow: {
							width: 400,
							height: 270,
							orientation: 1,
							offsetX: 0,
							offsetY: 0,
							hideToolBars: true,
							trackerPageConfigUrl: 'trackerConfig_p44121997-ALL.js'/*tpa=http://www.microsoft.com/library/svy/trackerConfig_p44121997-ALL.js*/
							// future feature: 
							//features: "location=0,menubar=0,resizable=1,scrollbars=1,toolbar=0"
						}
																				,Events: {
						beforeRender: function() {
														
													}
						,beforeDestroy: function() {
														
													}
						,beforeAccept: function() {
														
													}
						,beforeDecline: function() {
														
													}
						,beforeRenderUpdate: function() {
														
													}
						,afterRender: function() {
														
													}
					}
				}
								
											,
								{ 	methodology: 2,
					projectId: 'p117732561',
					weight: 5,
					isRapidCookie: false,
					acceptUrl: 'http://survey2.surveysite.com/wix/p117732561.aspx',
					secureUrl: '',
					acceptParams: {
						raw: 'l=9',
						siteCode: '652',
												cookie: [
													],
												otherVariables: [
													]
					},
					viewUrl: 'http://web.survey-poll.com/tc/CreateLog.aspx',
					viewParams: 'http://www.microsoft.com/library/svy/log=comscore/view/p117732561-view.log',
					content: '<table style="width:360px; padding:0px; margin:0px; border-collapse:collapse; border:1px solid #999999; background-color: #FFFFFF;"><tr><td>  <table style="border:0px; border-collapse:collapse; width: 100%;"><tr><td>  	<table style="border:0px; border-collapse:collapse; width: 100%;"><tr valign="top"><td>  		<img src="logo-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/logo-stripe.gif*/ /></td><td><a href="Close" onclick="@declineHandler"><img border="0" src="close.gif"/*tpa=http://www.microsoft.com/library/svy/close.gif*/ /></a></td></tr> <tr><td colspan="2"><img src="top-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/top-stripe.gif*/ /> 		</td></tr> 	</table>  	<table style="width:100%;"><tr><td>   			<div style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 11px; color: #000000; margin: 6px 6px 15px 4px;line-height:14px;">We\'re trying to understand how people use the Windows website so we can make it better. If you don\'t mind, we\'d like to see which pages you visit today and then ask you a few questions when you leave the site. <br /><br />Would you like to participate?</div>  <div align="center" style="margin: 0; padding: 0"> <input style="margin: 0; padding: 0" type="button" value="  Yes  " onclick="@acceptHandler" />&nbsp;&nbsp; <input style="margin: 0; padding: 0"  type="button" value=" No " onclick="@declineHandler" /> </div>  <div style="font-family: Verdana,Arial,Helvetica,sans-serif; font-size:11px; color:#000000; margin:15px 0px 6px 6px;"><a href="http://privacy.microsoft.com/en-us/default.aspx" target="_blank" style="font-size:12px; text-decoration:none;">Privacy Statement</a></div>   <img src="bottom-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/bottom-stripe.gif*/ /></td></tr></table> </td></tr></table> </td></tr></table>   ',
					height: 210,
					width: 390,
					revealDelay: 1000,
					horizontalAlignment: 1,
					verticalAlignment: 1,
					horizontalMargin: 0,
					verticalMargin: 0,
					isHideBlockingElements: false,
					isAutoCentering: true,
					url: 'http://www.microsoft.com/library/svy/SiteRecruit_Tracker.htm',
					trackerGracePeriod: 5
					,usesSilverlight: false
					
					//silverlight config
										
											,trackerWindow: {
							width: 400,
							height: 270,
							orientation: 1,
							offsetX: 0,
							offsetY: 0,
							hideToolBars: true,
							trackerPageConfigUrl: 'trackerConfig_p117732561.js'/*tpa=http://www.microsoft.com/library/svy/trackerConfig_p117732561.js*/
							// future feature: 
							//features: "location=0,menubar=0,resizable=1,scrollbars=1,toolbar=0"
						}
																				,Events: {
						beforeRender: function() {
														
													}
						,beforeDestroy: function() {
														
													}
						,beforeAccept: function() {
														
													}
						,beforeDecline: function() {
														
													}
						,beforeRenderUpdate: function() {
														
													}
						,afterRender: function() {
														
													}
					}
				}
						]
};
COMSCORE.SiteRecruit.Builder.run();
