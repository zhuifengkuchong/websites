/*
Copyright (c) 2014, comScore Inc. All rights reserved.
version: 5.0.3
*/
COMSCORE.SiteRecruit.Builder.config = {
	version: "5.0.3",
	
	// invitations' settings
	invitation: [
							
											{ 	methodology: 0,
					projectId: 'p271142646',
					weight: 100,
					isRapidCookie: false,
					acceptUrl: 'http://survey2.surveysite.com/wix/p271142646.aspx',
					secureUrl: '',
					acceptParams: {
						raw: 'l=9&SURVEY_TYPE=1',
						siteCode: '2788',
												cookie: [
													],
												otherVariables: [
													]
					},
					viewUrl: 'http://b.scorecardresearch.com/b',
					viewParams: 'c5=1',
					content: '<div id="srinvite" style="width:450px; border:1px solid #0073C6; border-top:8px solid #0073C6; margin:0; background-color:#FFFFFF; padding:0"> 	<div class="srtext" style="text-Align:left;margin:10px 60px 0px 50px; font-family:\'Segoe UI\'; font-size:18px; color:#666666; background-color:#FFFFFF;"> 		<img src="logo-stripe-short.gif"/*tpa=http://www.microsoft.com/library/svy/logo-stripe-short.gif*/ border="0"/> 	</div> 	<div class="srtext" style="position:relative; top:-42px; right:-30px;width:25px;"> </div> 	<div class="srtext" style="margin:10px 50px; font-family:\'Segoe UI\'; font-size:14px; color:#0073C6; background-color:#FFFFFF;width:300px;text-align:left;">Microsoft is conducting an online survey. <br /><br />Would you like to participate?</div> 	<div style="margin:14px 30px 10px 40px;"> 		<table style="line-height:12px;font-family:\'Segoe UI\';font-size:12px;color:#0073C6;border:0;"><tr><td style="vertical-Align:bottom;border:0;"><input type="button" style="border-style:none; background-color:#0073C6; color:#FFFFFF; font-size:14px; height:35px;width:100px;cursor:pointer;" value=" Yes " onclick="fSubmit();"/>	 &nbsp; <a href="javascript:void(0);" onClick="go_back();" style="font-family:\'Segoe UI\'; font-size:13px; color:#0073C6;"> No, go back to the Windows Web site </a></td></tr><tr><td style="vertical-Align:bottom;border:0;"><br /><a href="http://privacy.microsoft.com/en-us/default.mspx" target="_blank" style="font-family:\'Segoe UI\';font-size:12px;color:#0073C6;">Privacy Statement</a></td></tr> 		</table> <br /> 	</div> </div>',
					height: 300,
					width: 450,
					revealDelay: 500,
					horizontalAlignment: 1,
					verticalAlignment: 0,
					horizontalMargin: 0,
					verticalMargin: 4,
					startingHorizontalAlignment: 1,
					startingVerticalAlignment: 0,
					startingHorizontalMargin: 0,
					startingVerticalMargin: 0,					
					isHideBlockingElements: false,
					isAutoCentering: true,
					url: 'http://www.microsoft.com/library/svy/SiteRecruit_Tracker.htm',
					trackerGracePeriod: 5
					,usesSilverlight: false
					
					//silverlight config
																				,Events: {
						beforeRender: function() {
if(_site){ COMSCORE.SiteRecruit.Builder.invitation.config.acceptParams.siteCode = _site; }

if(_mobile){//MOBILE - append mobile=1; c6=1
	COMSCORE.SiteRecruit.Builder.invitation.config.acceptParams.raw += "&mobile=1";
	COMSCORE.SiteRecruit.Builder.invitation.config.viewParams += "&c6=1";
	var screenWidth = self.screen.availWidth, screenHeight = self.screen.availHeight;
	//var screenWidth = self.screen.width, screenHeight = self.screen.Height;
	if(screenWidth <=770){
		_w = 240; _h = 300;
		_contstr = '<div id="srinvite" style="width:'+_w+'px; border:1px solid #0073C6; border-top:8px solid #0073C6; margin:0; background-color:#FFFFFF; padding:0;"> 	 	<div class="srtop" style="background:#FFFFFF; margin:0; padding:0; vertical-align:top;"><img src="logo-stripe-short.gif"/*tpa=http://www.microsoft.com/library/svy/logo-stripe-short.gif*/ style="margin:10px" alt="Microsoft Logo"/><div style="position:absolute; top:26px; right:14px;"><img src="close.gif"/*tpa=http://www.microsoft.com/library/svy/close.gif*/ style="border:0;cursor:pointer;" onclick="go_back();" alt="Close button link" tabindex="1"/></div> 	</div> 	  	<div class="srtext" style="margin:10px 10px; font-family:segoe ui; font-size:14px; color:#666666; background-color:#FFFFFF;">Microsoft is conducting an online survey. <br />Would you be willing to participate?  </div> 	 	<div style="margin:4px"><input type="button" style="border-style:none; background-color:#0073C6; color:#FFFFFF; font-size:14px; height:35px;width:100px;cursor:pointer;" value=" Yes " onclick="fSubmit();"/>	 &nbsp; <a href="javascript:void(0);" onClick="go_back();" style="font-family:\'Segoe UI\'; font-size:13px; color:#0073C6;"> No, go back to the Microsoft website </a><br /><a href="http://privacy.microsoft.com/en-us/default.mspx" style="font-family:segoe ui; font-size:13px; color:#666666;" target="_blank">Privacy Statement</a></div>	</div>';
		COMSCORE.SiteRecruit.Builder.invitation.config.content = _contstr;
		COMSCORE.SiteRecruit.Builder.invitation.config.width = _w;
		COMSCORE.SiteRecruit.Builder.invitation.config.height = _h;					
	}
}
													}
						,beforeDestroy: function() {
														
													}
						,beforeAccept: function() {
												
													}
						,beforeDecline: function() {
														
													}
						,beforeRenderUpdate: function() {
														
													}
						,afterRender: function() {
														
													}
					}
				}
						]
};
COMSCORE.SiteRecruit.Builder.run();
