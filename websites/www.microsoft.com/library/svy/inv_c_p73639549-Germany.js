/*
Copyright (c) 2012, comScore Inc. All rights reserved.
version: 5.0.3
*/
COMSCORE.SiteRecruit.Builder.config = {
	version: "5.0.3",
	
	// invitations' settings
	invitation: [
							
											{ 	methodology: 1,
					projectId: 'p73639549',
					weight: 100,
					isRapidCookie: false,
					acceptUrl: 'http://web.survey-poll.com/tc/CreateAutomailMessage.aspx',
					secureUrl: '',
					acceptParams: {
						raw: 'http://www.microsoft.com/library/svy/survey=comscore/view&log=p73639549g-popup.log',
						siteCode: '1209',
												cookie: [
													],
												otherVariables: [
													]
					},
					viewUrl: 'http://web.survey-poll.com/tc/CreateLog.aspx',
					viewParams: 'http://www.microsoft.com/library/svy/log=comscore/view/p73639549g-view.log',
					content: '<table width="360" cellpadding="3" cellspacing="0" border="0" bgcolor="#FFFFFF" style="margin:0"><tr><td> <table width="100%" cellpadding="1" cellspacing="0" border="0" bgcolor="#999999" style="margin:0"><tr><td> <table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#FFFFFF" style="margin:0"><tr valign="top"><td> <img src="logo-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/logo-stripe.gif*/ /><a href="Close" onclick="@declineHandler"><img border="0" src="close.gif"/*tpa=http://www.microsoft.com/library/svy/close.gif*/ /></a><br /> <img src="top-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/top-stripe.gif*/ /> <table width="100%" cellpadding="5" style="margin:0"><tr><td style="background:#FFFFFF">  <div style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 11px; color: #000000; margin: 10px;">Microsoft f&#252;hrt derzeit eine Studie &#252;ber Technologie-Marken durch und ist an Ihrer Meinung interessiert. Wenn Sie teilnehmen m&#246;chten, geben Sie bitte Ihre E-Mail-Adresse unten ein, damit wir Ihnen den Fragebogen innerhalb der kommenden Woche zusenden k&#246;nnen.  </div>  <div align="center" style="margin: 0; padding: 0"> <form id="SiteRecruit_invitationForm" onsubmit="return false" style="margin: 0; padding: 0"> <input style="margin: 0; padding: 0" type="text" name="email"><br /><br /> <input style="margin: 0; padding: 0" type="submit" value="  Ja  " onclick="@acceptHandler" />&nbsp;&nbsp; <input style="margin: 0; padding: 0"  type="button" value=" Nein " onclick="@declineHandler" /> </form> </div>  <div style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 11px; color: #000000; margin: 10px;">Lesen Sie die <a href="http://survey2.surveysite.com/wi/p73639549/privacy_p73639549_Germany.html" target="_blank">Datenschutzerkl&#228;rung hier</a>.</div>  </td></tr></table> <img src="bottom-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/bottom-stripe.gif*/ /></td></tr></table> </td></tr></table> </td></tr></table>   ',
					height: 210,
					width: 390,
					revealDelay: 1000,
					horizontalAlignment: 1,
					verticalAlignment: 1,
					horizontalMargin: 0,
					verticalMargin: 0,
					isHideBlockingElements: false,
					isAutoCentering: true,
					url: 'http://www.microsoft.com/library/svy/SiteRecruit_Tracker.htm',
					trackerGracePeriod: 3
					,usesSilverlight: false
					
					//silverlight config
										
																				,Events: {
						beforeRender: function() {
														
													}
						,beforeDestroy: function() {
														
													}
						,beforeAccept: function() {
														
													}
						,beforeDecline: function() {
														
													}
						,beforeRenderUpdate: function() {
														
													}
						,afterRender: function() {
														
													}
					}
				}
						]
};
COMSCORE.SiteRecruit.Builder.run();
