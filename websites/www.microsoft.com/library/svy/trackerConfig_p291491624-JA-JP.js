/*
Copyright (c) 2014, comScore Inc. All rights reserved.
version: 5.0.3
*/
COMSCORE.SiteRecruit.Tracker.config = {
	isAutoFocus: true,
	title: 'マイクロソフト アンケート',
	content: '<table width="100%" cellpadding="2px" cellspacing="0px" border="0px" class="main" style="border:1px solid #0073C6; border-top:4px solid #0073C6">      	 	<tr><td class="content"> 			 			<div style="font-family:\'Segoe UI\'; font-size:18px; color:#0073C6; background-color:#FFFFFF;"><img src="logo-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/logo-stripe.gif*/ border="0"/></div> 			 			<p><div class="srtext" style="color:#0073C6;">このウィンドウを閉じないでください<br /><br /> アンケートへのご協力ありがとうございます。このサイトから退出する際にこちらのウィンドウにアンケートが表示されますので開いたままにしてください。</div></p>              			 			<p id="counterText" style="font-weight:bold; color:#0073C6;" class="srtext">このウィンドウは <span id="counter">10</span>秒後に最小化されます。</p>              			 			<p><div class="srtext"><a href="http://privacy.microsoft.com/ja-jp/" style="text-decoration:none; font-size:13px; color:#0073C6"  target="_blank">プライバシーに関する声明 </a></div></p>          		 	</td></tr>           </table>   <script id="customScript">  	 	COMSCORE.Custom = (function() { 	return { 		getCounter: function() 		{ 		    var el = document.getElementById(\'counter\'); 		    return el; 		}, 		getCounterValue: function() 		{ 		    var el = COMSCORE.Custom.getCounter(); 		    var v = Number(el.innerHTML); 		    return v; 		}, 		 	decrementCounter: function() 		{ 		    var el = COMSCORE.Custom.getCounter(); 		    var v = COMSCORE.Custom.getCounterValue(); 		    if (v > 0) el.innerHTML = --v; 		    if (v > 0) 		    { 		        window.setTimeout(function() { 					COMSCORE.Custom.decrementCounter(); 					 				}, \'1000\'); 		    } 		    else 		    { 		        self.blur(); 				document.getElementById(\'counterText\').innerHTML = \'&nbsp;\'; 		    } 		}, 		 		 	initCounter: function(){ 		     	window.setTimeout(function(){ COMSCORE.Custom.decrementCounter();}, \'1000\');}};  })();    if(/msie/i.test(navigator.userAgent)){  	COMSCORE.Custom.initCounter(); }else{  	try{ 		var element = document.getElementById("counterText");element.parentNode.removeChild(element); 	}catch(err){}  }  </script> ',	// content to inject in the new window
	match: '',	// browserable regex match
	isCaptureTrail: true,
	gracePeriod: 7
};
COMSCORE.SiteRecruit.Tracker.start();