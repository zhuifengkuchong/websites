/*
Copyright (c) 2012, comScore Inc. All rights reserved.
version: 5.0.3
*/
COMSCORE.SiteRecruit.Builder.config = {
	version: "5.0.3",
	
	// invitations' settings
	invitation: [
							
											{ 	methodology: 2,
					projectId: 'p176534785',
					weight: 100,
					isRapidCookie: false,
					acceptUrl: 'http://survey2.surveysite.com/wix/p176534785.aspx',
					secureUrl: 'https://survey2.securestudies.com/wix/p176534785.aspx',
					acceptParams: {
						raw: 'hsite=2342',
						siteCode: '2342',
												cookie: [
													],
												otherVariables: [
													]
					},
					viewUrl: 'https://b.scorecardresearch.com/b',
					viewParams: 'http://www.microsoft.com/library/svy/log=comscore/view/p176534785-view.log',
					content: '<div id="srinvite" style="width:450px; border:1px solid #0073C6; border-top:8px solid #0073C6; margin:0; background-color:#FFFFFF; padding:0"> 	<div class="srtext" style="text-Align:left;margin:10px 60px 0px 50px; font-family:\'Segoe UI\'; font-size:18px; color:#666666; background-color:#FFFFFF;"> 		<img src="logo-stripe-short.gif"/*tpa=http://www.microsoft.com/library/svy/logo-stripe-short.gif*/ border="0"/> 	</div> 	<div class="srtext" style="position:relative; top:-42px; right:-30px;width:25px;"> 			<a href="Close" onclick="@declineHandler" style="border:none;"><img src="close.gif"/*tpa=http://www.microsoft.com/library/svy/close.gif*/ style="border:0; margin-left:360px; padding:0;"/></a> 	</div> 	<div class="srtext" style="margin:0px 50px; font-family:\'Segoe UI\'; font-size:14px; color:#0073C6; background-color:#FFFFFF;width:300px;text-align:left;"> 		Microsoft is conducting an online survey to understand your opinions of the Microsoft Learning Web site.  If you choose to participate, the online survey will be presented to you when you leave the Microsoft Learning Web site.<br /><br />Would you like to participate?  	</div> 	<div style="margin:20px 30px 10px 40px;"> 		<table style="line-height:12px;font-family:\'Segoe UI\';font-size:12px;color:#0073C6;border:0;"> 			<tr><td style="width:100px;vertical-Align:bottom;border:0;"><a href="http://privacy.microsoft.com/en-us/default.mspx" style="text-decoration:none; font-size:12px; color:#0073C6;font-family:\'Segoe UI\';" target="_blank">Privacy Statement</a></td> 				<td width="225" align="center" style="border:0;"> 					<input type="button" style="border-style:none; background-color:#0073C6; color:#FFFFFF; font-size:14px; height:35px;width:100px;" value=" Yes " onclick="@acceptHandler;"/>	 &nbsp; 					<input type="button" style="border-style:none; background-color:#0073C6; color:#FFFFFF; font-size:14px; height:35px;width:100px;" value=" No " onclick="@declineHandler;"/>	<br /> 					</td></tr> 		</table> <br /> 	</div> </div>',
					height: 300,
					width: 450,
					revealDelay: 5000,
					horizontalAlignment: 1,
					verticalAlignment: 1,
					horizontalMargin: 0,
					verticalMargin: 0,
					isHideBlockingElements: false,
					isAutoCentering: true,
					url: 'http://www.microsoft.com/library/svy/SiteRecruit_Tracker.htm',
					trackerGracePeriod: 5
					,usesSilverlight: false
					
					//silverlight config
										
											,trackerWindow: {
							width: 400,
							height: 270,
							orientation: 1,
							offsetX: 0,
							offsetY: 0,
							hideToolBars: true,
							trackerPageConfigUrl: 'trackerConfig_p176534785-Learning.js'/*tpa=http://www.microsoft.com/library/svy/trackerConfig_p176534785-Learning.js*/
							// future feature: 
							//features: "location=0,menubar=0,resizable=1,scrollbars=1,toolbar=0"
						}
																				,Events: {
						beforeRender: function() {
														
													}
						,beforeDestroy: function() {
														
													}
						,beforeAccept: function() {
														
													}
						,beforeDecline: function() {
														
													}
						,beforeRenderUpdate: function() {
														
													}
						,afterRender: function() {
														
													}
					}
				}
						]
};
COMSCORE.SiteRecruit.Builder.run();