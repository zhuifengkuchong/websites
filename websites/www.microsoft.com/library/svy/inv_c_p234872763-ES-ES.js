/*
Copyright (c) 2014, comScore Inc. All rights reserved.
version: 5.0.3
*/
COMSCORE.SiteRecruit.Builder.config = {
	version: "5.0.3",
	
	// invitations' settings
	invitation: [
							
											{ 	methodology: 2,
					projectId: 'p234872763',
					weight: 100,
					isRapidCookie: false,
					acceptUrl: 'http://survey2.surveysite.com/wix/p234872763.aspx',
					secureUrl: 'https://survey2.securestudies.com/wix/p234872763.aspx',
					acceptParams: {
						raw: 'l=10',
						siteCode: '2647',
												cookie: [
													],
												otherVariables: [
													]
					},
					viewUrl: 'http://web.survey-poll.com/tc/CreateLog.aspx',
					viewParams: 'http://www.microsoft.com/library/svy/log=comscore/view/p234872763-view.log',
					content: '<div id="srinvite" style="width:450px; border:1px solid #0073C6; border-top:8px solid #0073C6; margin:0; background-color:#FFFFFF; padding:0"> 	<div class="srtext" style="text-Align:left;margin:10px 60px 0px 50px; font-family:\'Segoe UI\'; font-size:18px; color:#666666; background-color:#FFFFFF;"> 		<img src="logo-stripe-short.gif"/*tpa=http://www.microsoft.com/library/svy/logo-stripe-short.gif*/ border="0"/> 	</div> 	<div class="srtext" style="position:relative; top:-42px; right:-30px;width:25px;"> 		<img src="close.gif"/*tpa=http://www.microsoft.com/library/svy/close.gif*/ style="border:0; margin-left:360px; padding:0;cursor:pointer;" onclick="@declineHandler"/>  	</div> 	<div class="srtext" style="margin:0px 50px; font-family:\'Segoe UI\'; font-size:14px; color:#0073C6; background-color:#FFFFFF;width:300px;text-align:left;"> 		Nuestro objetivo es comprender cómo se usa el sitio web de Windows para poder mejorarlo.  Si no te importa, nos gustaría conocer las páginas que has visitado hoy y realizarte algunas preguntas antes de que salgas del sitio.<br /><br /> ¿ Te apetece ayudarnos? 	</div> 	<div style="margin:20px 30px 10px 40px;"> 		<table style="line-height:12px;font-family:\'Segoe UI\';font-size:12px;color:#0073C6;border:0;"> 			<tr><td style="width:100px;vertical-Align:bottom;border:0;"><a href="http://privacy.microsoft.com/es-ES/default.mspx" target="_blank">Declaraci&#243;n de privacidad</a></td> 				<td width="225" align="center" style="border:0;"> 					<input type="button" style="border-style:none; background-color:#0073C6; color:#FFFFFF; font-size:14px; height:35px;width:100px; cursor:pointer;" value=" S&#237; " onclick="@acceptHandler;"/>	 &nbsp; 					<input type="button" style="border-style:none; background-color:#0073C6; color:#FFFFFF; font-size:14px; height:35px;width:100px; cursor:pointer;" value=" No " onclick="@declineHandler;"/>	<br /> 					</td></tr> 		</table> <br /> 	</div> </div>',
					height: 300,
					width: 450,
					revealDelay: 1000,
					horizontalAlignment: 1,
					verticalAlignment: 1,
					horizontalMargin: 0,
					verticalMargin: 0,
					startingHorizontalAlignment: 1,
					startingVerticalAlignment: 0,
					startingHorizontalMargin: 0,
					startingVerticalMargin: 0,
					isHideBlockingElements: false,
					isAutoCentering: true,
					url: 'http://www.microsoft.com/library/svy/SiteRecruit_Tracker.htm',
					trackerGracePeriod: 5,
					usesSilverlight: false
					
					//silverlight config
										
											,trackerWindow: {
							width: 400,
							height: 270,
							orientation: 1,
							offsetX: 0,
							offsetY: 0,
							hideToolBars: true,
							trackerPageConfigUrl: 'trackerConfig_p234872763-ES-ES.js'/*tpa=http://www.microsoft.com/library/svy/trackerConfig_p234872763-ES-ES.js*/
							// future feature: 
							//features: "location=0,menubar=0,resizable=1,scrollbars=1,toolbar=0"
						}
																				,Events: {
						beforeRender: function() {
														
													}
						,beforeDestroy: function() {
														
													}
						,beforeAccept: function() {
														COMSCORE.SiteRecruit.Builder.invitation.config.url = "http://www.microsoft.com/library/svy/SiteRecruit_Tracker-MPE-BlueWhite.htm";
													}
						,beforeDecline: function() {
														
													}
						,beforeRenderUpdate: function() {
														
													}
						,afterRender: function() {
														
													}
					}
				}
						]
};
COMSCORE.SiteRecruit.Builder.run();
