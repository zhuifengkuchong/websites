/*
Copyright (c) 2014, comScore Inc. All rights reserved.
version: 5.0.3
*/
COMSCORE.SiteRecruit.Builder.config = {
	version: "5.0.3",
	
	// invitations' settings
	invitation: [
							
											{ 	methodology: 2,
					projectId: 'p292423925',
					weight: 100,
					isRapidCookie: false,
					acceptUrl: 'http://survey2.surveysite.com/wix/p292423925.aspx',
					secureUrl: '',
					acceptParams: {
						raw: 'l=9',
						siteCode: '2749',
												cookie: [
													],
												otherVariables: [
													]
					},
					viewUrl: '//b.scorecardresearch.com/b',
					viewParams: '',
					content: '<div id="srinvite" style="width:450px; border:1px solid #0073C6; border-top:8px solid #0073C6; margin:0; background-color:#FFFFFF; padding:0"> 	<div class="srtext" style="text-Align:left;margin:10px 60px 0px 50px; font-family:\'Segoe UI\'; font-size:18px; color:#666666; background-color:#FFFFFF;"> 		<img src="logo-stripe-short.gif"/*tpa=http://www.microsoft.com/library/svy/logo-stripe-short.gif*/ border="0"  alt="Microsoft Logo"/> 	</div> 	<div class="srtext" style="position:relative; top:-42px; right:-30px;width:25px;"> 		<img src="close.gif"/*tpa=http://www.microsoft.com/library/svy/close.gif*/ style="border:0; margin-left:360px; padding:0;cursor:pointer;" onclick="@declineHandler" alt="Close button link" tabindex="1"/>  	</div> 	<div class="srtext" style="margin:0px 50px; font-family:\'Segoe UI\'; font-size:14px; color:#0073C6; background-color:#FFFFFF;width:320px;text-align:left;"> 		Microsoft is conducting an online survey to understand your opinions of the Microsoft Band Support website. If you choose to participate, the online survey will be presented to you when you have completed your visit. <br /><br /> Would you like to participate?  	</div> 	<div style="margin:20px 30px 10px 40px;"> 		<table style="line-height:12px;font-family:\'Segoe UI\';font-size:12px;color:#0073C6;border:0;"> 			<tr><td style="width:100px;vertical-Align:bottom;border:0;"><a href="http://privacy.microsoft.com/en-us/" style="text-decoration:none; font-size:13px; color:#0073C6"  target="_blank">Privacy Statement</a></td> 				<td width="225" align="center" style="border:0;"> 					<input type="button" style="border-style:none; background-color:#0073C6; color:#FFFFFF; font-size:14px; height:35px;width:100px; cursor:pointer;" value=" Yes " onclick="@acceptHandler;" tabindex="2"/>	 &nbsp; 					<input type="button" style="border-style:none; background-color:#0073C6; color:#FFFFFF; font-size:14px; height:35px;width:100px; cursor:pointer;" value=" No " onclick="@declineHandler;" tabindex="3"/>	<br /> 					</td></tr> 		</table> <br /> 	</div> </div>',
					height: 310,
					width: 450,
					revealDelay: 1000,
					horizontalAlignment: 1,
					verticalAlignment: 1,
					horizontalMargin: 0,
					verticalMargin: 0,
					startingHorizontalAlignment: 1,
					startingVerticalAlignment: 0,
					startingHorizontalMargin: 0,
					startingVerticalMargin: 0,
					isHideBlockingElements: false,
					isAutoCentering: true,
					url: 'http://www.microsoft.com/library/svy/SiteRecruit_Tracker.htm',
					trackerGracePeriod: 7,
					usesSilverlight: false
					
					//silverlight config
										
											,trackerWindow: {
							width: 400,
							height: 270,
							orientation: 1,
							offsetX: 0,
							offsetY: 0,
							hideToolBars: true,
							trackerPageConfigUrl: 'trackerConfig_p292423925.js'/*tpa=http://www.microsoft.com/library/svy/trackerConfig_p292423925.js*/
							// future feature: 
							//features: "location=0,menubar=0,resizable=1,scrollbars=1,toolbar=0"
						}
																				,Events: {
						beforeRender: function() {
														document.onkeyup = function(evt) {
    evt = evt || window.event;
    if (evt.keyCode == 27) {
        COMSCORE.SiteRecruit.Builder.invitation.decline(this);
    }
};
													}
						,beforeDestroy: function() {
														
													}
						,beforeAccept: function() {
														
													}
						,beforeDecline: function() {
														
													}
						,beforeRenderUpdate: function() {
														
													}
						,afterRender: function() {
														
													}
					}
				}
						]
};
COMSCORE.SiteRecruit.Builder.run();
