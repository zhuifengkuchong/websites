/*
Copyright (c) 2013, comScore Inc. All rights reserved.
version: 5.0.3
*/
COMSCORE.SiteRecruit.Broker.config = {
	version: "5.0.3",
	//TODO:Karl extend cookie enhancements to ie userdata
		testMode: false,
	
	// cookie settings
	cookie:{
		name: 'msresearch',
		path: '/',
		domain:  '.microsoft.com',
		duration: 90,
		rapidDuration: 0,
		expireDate: ''
	},
	thirdPartyOptOutCookieEnabled : false,
	
	// optional prefix for pagemapping's pageconfig file
	prefixUrl: "",
	
	//events
	Events: {
		beforeRecruit: function() {
					}
	},
	
		mapping:[
	// m=regex match, c=page config file (prefixed with configUrl), f=frequency
	{m: '//[\\w\\.-]+/en-us/(default\\.aspx)?$', c: 'inv_c_p38796305-EN-US_M.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p38796305-EN-US_M.js*/, f: 0.05, p: 1 	}
	,{m: '//[\\w\\.-]+/en-ca/(default\\.aspx)?$', c: 'inv_c_p3069121557-EN-CA_M.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p3069121557-EN-CA_M.js*/, f: 0.05, p: 1 	}
	,{m: '//[\\w\\.-]+/fr-ca/(default\\.aspx)?$', c: 'inv_c_p3069121557-FR-CA_M.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p3069121557-FR-CA_M.js*/, f: 0.05, p: 1 	}
	,{m: '//[\\w\\.-]+/en-gb/(default\\.aspx)?$', c: 'inv_c_p3069121557-EN-GB_M.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p3069121557-EN-GB_M.js*/, f: 0.05, p: 1 	}
	,{m: '//[\\w\\.-]+/en-in/(default\\.aspx)?$', c: 'inv_c_p3069121557-EN-IN_M.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p3069121557-EN-IN_M.js*/, f: 0.05, p: 1 	}
	,{m: '//[\\w\\.-]+/fr-fr/(default\\.aspx)?$', c: 'inv_c_p3069121557-FR-FR_M.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p3069121557-FR-FR_M.js*/, f: 0.05, p: 1 	}
	,{m: '//[\\w\\.-]+/ja-jp/(default\\.aspx)?$', c: 'inv_c_p3069121557-JA-JP_M.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p3069121557-JA-JP_M.js*/, f: 0.05, p: 1 	}
	,{m: '//[\\w\\.-]+/zh-cn/(default\\.aspx)?$', c: 'inv_c_p3069121557-ZH-CN_M.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p3069121557-ZH-CN_M.js*/, f: 0.05, p: 1 	}
	,{m: '//[\\w\\.-]+/ru-ru/(default\\.aspx)?$', c: 'inv_c_p3069121557-RU-RU_M.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p3069121557-RU-RU_M.js*/, f: 0.05, p: 1 	}
	,{m: '//[\\w\\.-]+/pt-br/(default\\.aspx)?$', c: 'inv_c_p3069121557-PT-BR_M.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p3069121557-PT-BR_M.js*/, f: 0.05, p: 1 	}
	,{m: '//[\\w\\.-]+/de-de/(default\\.aspx)?$', c: 'inv_c_p3069121557-DE-DE_M.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p3069121557-DE-DE_M.js*/, f: 0.05, p: 1 	}
	//,{m: '//[\\w\\.-]+/en-us/windowsapps', c: 'inv_c_p271142646.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p271142646.js*/, f: 0.43, p: 1 	}
	,{m: '//[\\w\\.-]+/en-us/phone', c: 'inv_c_p291491624-US-Phone.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p291491624-US-Phone.js*/, f: 0.33, p: 1 	}
	,{m: '//[\\w\\.-]+/en-us/tablet', c: 'inv_c_p291491624-US-Tablet.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p291491624-US-Tablet.js*/, f: 0.33, p: 1 	}
	,{m: '//[\\w\\.-]+/en-us/premiumlaptop', c: 'inv_c_p291491624-US-Prem-Laptop.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p291491624-US-Prem-Laptop.js*/, f: 0.33, p: 1 	}
	,{m: '//[\\w\\.-]+/en-us/laptop', c: 'inv_c_p291491624-US-Laptop.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p291491624-US-Laptop.js*/, f: 0.33, p: 1 	}
	,{m: '//[\\w\\.-]+/en-gb/phone', c: 'inv_c_p291491624-EN-GB-Phone.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p291491624-EN-GB-Phone.js*/, f: 0.5, p: 2  	}
	,{m: '//[\\w\\.-]+/en-gb/tablet', c: 'inv_c_p291491624-EN-GB-Tablet.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p291491624-EN-GB-Tablet.js*/, f: 0.5, p: 2  	}
	,{m: '//[\\w\\.-]+/en-gb/laptop', c: 'inv_c_p291491624-EN-GB-Laptop.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p291491624-EN-GB-Laptop.js*/, f: 0.08, p: 1 	}
	,{m: '//[\\w\\.-]+/fr-fr/phone', c: 'inv_c_p291491624-FR-FR-Phone.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p291491624-FR-FR-Phone.js*/, f: 0.5, p: 2  	}
	,{m: '//[\\w\\.-]+/fr-fr/tablet', c: 'inv_c_p291491624-FR-FR-Tablet.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p291491624-FR-FR-Tablet.js*/, f: 0.5, p: 2  	}
	,{m: '//[\\w\\.-]+/de-de/phone', c: 'inv_c_p291491624-DE-DE-Phone.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p291491624-DE-DE-Phone.js*/, f: 0.5, p: 2  	}
	,{m: '//[\\w\\.-]+/ja-jp/tablet', c: 'inv_c_p291491624-JA-JP-Tablet.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p291491624-JA-JP-Tablet.js*/, f: 0.08, p: 2  	}
	,{m: '//[\\w\\.-]+/zh-cn/tablet', c: 'inv_c_p291491624-ZH-CN-Tablet.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p291491624-ZH-CN-Tablet.js*/, f: 0.08, p: 2  	}
	,{m: '//[\\w\\.-]+/it-it/phone', c: 'inv_c_p291491624-IT-IT-Phone.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p291491624-IT-IT-Phone.js*/, f: 0.43, p: 2  	}
]
};
//START ClickTaleData
var _clickTaleData = '';
if (typeof ClickTaleOnRecording === 'function')
{
	var prevClickTaleOnRecording = ClickTaleOnRecording;
	var ClickTaleOnRecording = function(){
		try{
			_clickTaleData = "&clickTalePID=" + ClickTaleGetPID() + "&clickTaleSID=" + ClickTaleGetSID() + "&clickTaleUID=" + ClickTaleGetUID();
		}catch(err){ }
		prevClickTaleOnRecording.apply(this, arguments);
	}
}
else
{
	var ClickTaleOnRecording = function(){
		try{
			_clickTaleData = "&clickTalePID=" + ClickTaleGetPID() + "&clickTaleSID=" + ClickTaleGetSID() + "&clickTaleUID=" + ClickTaleGetUID();
		}catch(err){ }
	}
}
//END ClickTaleData
COMSCORE.SiteRecruit.Broker.run();