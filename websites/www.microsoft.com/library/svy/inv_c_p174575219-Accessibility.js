/*
Copyright (c) 2014, comScore Inc. All rights reserved.
version: 5.0.3
*/
COMSCORE.SiteRecruit.Builder.config = {
	version: "5.0.3",
	
	// invitations' settings
	invitation: [
							
											{ 	methodology: 2,
					projectId: 'p174575219',
					weight: 100,
					isRapidCookie: false,
					acceptUrl: 'http://survey2.surveysite.com/wix/p174575219.aspx',
					secureUrl: '',
					acceptParams: {
						raw: 'l=9',
						siteCode: '1868',
												cookie: [
													],
												otherVariables: [
													]
					},
					viewUrl: 'http://b.scorecardresearch.com/b',
					viewParams: 'http://www.microsoft.com/library/svy/log=comscore/view/p174575219-view.log',
					content: '<table style="width:360px; padding:0px; margin:0px; border-collapse:collapse; border:1px solid #999999; background-color: #FFFFFF;"><tr><td>  <table style="border:0px; border-collapse:collapse; width: 100%;"><tr><td>  	<table style="border:0px; border-collapse:collapse; width: 100%;"><tr valign="top"><td>  		<img src="logo-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/logo-stripe.gif*/ alt="Microsoft Logo" /></td><td><img border="0" src="close.gif"/*tpa=http://www.microsoft.com/library/svy/close.gif*/ style="cursor:pointer;" alt="Close button link" onclick="@declineHandler"  tabindex="1" /></td></tr> <tr><td colspan="2"><img src="top-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/top-stripe.gif*/ /> 		</td></tr> 	</table>  	<table style="width:100%;"><tr><td>   			<div style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 11px; color: #000000; margin: 6px 6px 15px 4px;line-height:14px;">Microsoft is conducting an online survey to understand your opinions of the Microsoft Accessibility Web site. If you choose to participate, the online survey will be presented to you when you leave the Microsoft Accessibility Web site. <br /><br />Would you like to participate?</div>  <div align="center" style="margin: 0; padding: 0"> <input style="margin: 0; padding: 0" type="button" title="Yes" value="  Yes  " onclick="@acceptHandler" tabindex="2" />&nbsp;&nbsp; <input style="margin: 0; padding: 0"  type="button" title="No" value=" No " onclick="@declineHandler" tabindex="3" /> </div>  <div style="font-family: Verdana,Arial,Helvetica,sans-serif; font-size:11px; color:#000000; margin:15px 0px 6px 6px;"><a href="http://www.microsoft.com/privacystatement/en-us/core/default.aspx" target="_blank" style="font-size:12px; text-decoration:none;" title="Review the privacy statement" tabindex="4">Privacy Statement</a></div>   <img src="bottom-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/bottom-stripe.gif*/ /></td></tr></table> </td></tr></table> </td></tr></table>   ',
					height: 210,
					width: 390,
					revealDelay: 1000,
					horizontalAlignment: 1,
					verticalAlignment: 1,
					horizontalMargin: 0,
					verticalMargin: 0,
					startingHorizontalAlignment: 1,
					startingVerticalAlignment: 0,
					startingHorizontalMargin: 0,
					startingVerticalMargin: 0,
					isHideBlockingElements: false,
					isAutoCentering: true,
					url: 'http://www.microsoft.com/library/svy/SiteRecruit_Tracker.htm',
					trackerGracePeriod: 5,
					usesSilverlight: false
					
					//silverlight config
										
											,trackerWindow: {
							width: 400,
							height: 270,
							orientation: 1,
							offsetX: 0,
							offsetY: 0,
							hideToolBars: true,
							trackerPageConfigUrl: 'trackerConfig_p174575219-Accessibility.js'/*tpa=http://www.microsoft.com/library/svy/trackerConfig_p174575219-Accessibility.js*/
							// future feature: 
							//features: "location=0,menubar=0,resizable=1,scrollbars=1,toolbar=0"
						}
																				,Events: {
						beforeRender: function() {
														document.onkeyup = function(evt) {
    evt = evt || window.event;
    if (evt.keyCode == 27) {
        COMSCORE.SiteRecruit.Builder.invitation.decline(this);
    }
};
													}
						,beforeDestroy: function() {
														
													}
						,beforeAccept: function() {
														
													}
						,beforeDecline: function() {
														
													}
						,beforeRenderUpdate: function() {
														
													}
						,afterRender: function() {
														
													}
					}
				}
						]
};
COMSCORE.SiteRecruit.Builder.run();
