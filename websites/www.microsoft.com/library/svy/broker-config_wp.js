/*
Copyright (c) 2012, comScore Inc. All rights reserved.
version: 5.0.3
*/

/*
_cleHalt = false;
_refv = escape(document.referrer);	
try{
	if(_refv.search(/www\.microsoft\.com\/windows\/pc-selector|windows\.microsoft\.com\/en-us\/windows\/shop\/pcs/i) !=-1){ 
		_cleHalt=true; 	
	}else{ _cleHalt=false; }
}catch(e){		}

*/
COMSCORE.SiteRecruit.Broker.config = {
	version: "5.0.3",
	//TODO:Karl extend cookie enhancements to ie userdata
		testMode: false,
	
	// cookie settings
	cookie:{
		name: 'msresearch',
		path: '/',
		domain:  '.microsoft.com' ,
		duration: 90,
		rapidDuration: 0,
		expireDate: ''
	},
	
	// optional prefix for pagemapping's pageconfig file
	prefixUrl: "",
	
	//events
	Events: {
		beforeRecruit: function() {
					}
	},
	
		mapping:[
	// m=regex match, c=page config file (prefixed with configUrl), f=frequency
	{m: '^http://www\\.microsoft\\.com/windowsphone/en-us/', c: 'inv_c_p127227702-EN-US-P131919559.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p127227702-EN-US-P131919559.js*/, f: 0.0525, p: 0, halt: false   	}
	,{m: '^http://(www|js|i3)\\.microsoft\\.com/library/svy/int_cle-wp\\.htm', c: 'inv_c_p155754943-EN-US-INT.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p155754943-EN-US-INT.js*/, f: 1, p: 0  	}

]
};
COMSCORE.SiteRecruit.Broker.run();