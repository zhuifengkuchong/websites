/*
Copyright (c) 2014, comScore Inc. All rights reserved.
version: 5.0.3
*/
if(/windows\/en-us\/xp\/(top-questions|pcs-and-offers|end-of-xp-support)/i.test(document.referrer)) {
	COMSCORE.SiteRecruit._halt = true;
}

if(/[\w\.]+\/(surface)/i.test(SR_url)) {
	var allLinks = document.getElementsByTagName("a");
  function  checkLink(){
   for (var i = 0; i < allLinks.length; i++) {
  if(/(https:\/\/(login|accountservices|myservice)\.(live|passport|surface)\.(com|net)|youtube)/i.test(allLinks[i].href)){
    	if(allLinks[i].addEventListener){  		
    		hrefURL = allLinks[i].href;
      	allLinks[i].addEventListener('click',function(event){      
        	_set_SessionCookie("captlinks", this.href);
          _set_SessionCookie("graceIncr", 1); 
        },false);
      }else{ 
        allLinks[i].attachEvent('onclick',function(){ 
         	_set_SessionCookie("graceIncr", 1);
         	_set_SessionCookie("captlinks", this.href);
        });
      }
    }
   }
}
setTimeout("checkLink();", 3000);
}
_set_SessionCookie("graceIncr", 0);

if(/[\w\.]+\/(windowsembedded\/en-us\/)/i.test(SR_url)) {
	var hv = 0;
	var allLinks = document.getElementsByTagName("a");
	for (var i = 0; i < allLinks.length; i++) {
		
		if(/ /i.test(allLinks[i].href)){
			if(allLinks[i].addEventListener){          
         hrefURL = allLinks[i].href;
         allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", hv); },false);
    	}
    	else { 
      	allLinks[i].attachEvent('onclick',function(){     _set_SessionCookie("captEmbed", hv);     });
			}
		}
		if (/internet-of-things.aspx/i.test(window.location)) {
			if(/9hkSlsaHF08/i.test(allLinks[i].href)){ 																					if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '1'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '1'); }); } }
			if(/NYpdNGl1hco/i.test(allLinks[i].href)){ 																					if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '2'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '2'); }); } }
			if(/Microsoft_Internet_of_Things_Data_Sheet.pdf/i.test(allLinks[i].href)){ 					if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '3'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '3'); }); } }
			if(/Microsoft_Internet_of_Things_White_Paper.pdf/i.test(allLinks[i].href)){ 				if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '4'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '4'); }); } }
			if(/internet-of-things-infographic.aspx/i.test(allLinks[i].href)){ 									if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '5'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '5'); }); } }
			if(/Microsoft_Internet_of_Things_Top_Ten_Reasons.pdf/i.test(allLinks[i].href)){ 		if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '6'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '6'); }); } }
		}
		
		if (/internet-of-things-retail.aspx/i.test(window.location)) {
			if(/CCA_Microsoft_Internet_of_Things_News_Article.PDF/i.test(allLinks[i].href)){													if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '7'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '7'); }); } }
			if(/CCA_Microsoft_Internet_of_Things_Case_Study.PDF/i.test(allLinks[i].href)){														if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '8'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '8'); }); } }
			if(/CCA_Microsoft_Internet_of_Things_Customer_Success_Summary.PDF/i.test(allLinks[i].href)){							if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '9'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '9'); }); } }
			if(/xgnEaZ5pUO8/i.test(allLinks[i].href)){																																if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '10'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '10'); }); } }
			if(/Royal_Caribbean_Microsoft_Internet_of_Things_News_Article.pdf/i.test(allLinks[i].href)){							if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '11'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '11'); }); } }
			if(/Royal_Caribbean_Microsoft_Internet_of_Things_Case_Study.pdf/i.test(allLinks[i].href)){								if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '12'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '12'); }); } }
			if(/Royal_Caribbean_Microsoft_Internet_of_Things_Customer_Success_Summary.pdf/i.test(allLinks[i].href)){	if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '13'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '13'); }); } }
			if(/dtcbvNo_QA4/i.test(allLinks[i].href)){																																if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '14'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '14'); }); } }
			if(/Autolib_Microsoft_Internet_of_Things_News_Article.pdf/i.test(allLinks[i].href)){											if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '15'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '15'); }); } }
			if(/Autolib_Microsoft_Internet_of_Things_Case_Study.pdf/i.test(allLinks[i].href)){												if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '16'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '16'); }); } }
			if(/Autolib_Microsoft_Internet_of_Things_Customer_Success_Summary.pdf/i.test(allLinks[i].href)){					if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '17'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '17'); }); } }
			if(/Microsoft_Internet_of_Things_Top_Ten_Reasons_Retail.pdf/i.test(allLinks[i].href)){										if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '18'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '18'); }); } }
			if(/Microsoft_Internet_of_Things_Data_Sheet_Retail.pdf/i.test(allLinks[i].href)){												if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '19'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '19'); }); } }
		}
		
		if (/internet-of-things-manufacturing.aspx/i.test(window.location)) {
			if(/kpeNSamWgTU/i.test(allLinks[i].href)){																																if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '20'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '20'); }); } }
			if(/KUKA_Microsoft_Internet_of_Things_News_Article.pdf/i.test(allLinks[i].href)){												if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '21'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '21'); }); } }
			if(/KUKA_Microsoft_Internet_of_Things_Case_Study.pd/i.test(allLinks[i].href)){														if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '22'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '22'); }); } }
			if(/KUKA_Microsoft_Internet_of_Things_Customer_Success_Summary.pdf/i.test(allLinks[i].href)){						if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '23'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '23'); }); } }
			if(/UsUxI9stiYs/i.test(allLinks[i].href)){																																if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '24'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '24'); }); } }
			if(/Lido_Microsoft_Internet_of_Things_News_Article.pdf/i.test(allLinks[i].href)){												if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '25'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '25'); }); } }
			if(/Lido_Microsoft_Internet_of_Things_Case_Study.pdf/i.test(allLinks[i].href)){													if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '26'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '26'); }); } }
			if(/Lido_Microsoft_Internet_of_Things_Customer_Success_Summary.pdf/i.test(allLinks[i].href)){						if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '27'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '27'); }); } }
			if(/Microsoft_Internet_of_Things_Top_Ten_Reasons_Manufacturing.pdf/i.test(allLinks[i].href)){						if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '28'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '28'); }); } }
			if(/Microsoft_Internet_of_Things_Data_Sheet_Manufacturing.pdf/i.test(allLinks[i].href)){									if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '29'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '29'); }); } }
		}
		
		if (/internet-of-things-health.aspx/i.test(window.location)) {
			if(/buhhKdRnE6E/i.test(allLinks[i].href)){																																if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '30'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '30'); }); } }
			if(/Henry(%20| )Mayo_Microsoft_Internet_of_Things_News_Article.pdf/i.test(allLinks[i].href)){									if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '31'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '31'); }); } }
			if(/Henry(%20| )Mayo_Microsoft_Internet_of-Things_Case_Study.pdf/i.test(allLinks[i].href)){										if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '32'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '32'); }); } }
			if(/Henry_Mayo_Microsoft_Internet_of_Things_Customer_Success_Summary.pdf/i.test(allLinks[i].href)){			if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '33'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '33'); }); } }
				
			if(/_4dgMoycLkQ/i.test(allLinks[i].href)){																																if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '34'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '34'); }); } }
			if(/Great_River_Microsoft_Internet_of_Things_News_Article.pdf/i.test(allLinks[i].href)){									if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '35'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '35'); }); } }
			if(/Great_River_Microsoft_Internet_of_Things_Case_Study.pdf/i.test(allLinks[i].href)){										if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '36'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '36'); }); } }
			if(/Great_River_Microsoft_Internet_of_Things_Customer_Success_Summary.pdf/i.test(allLinks[i].href)){			if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '37'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '37'); }); } }
			if(/Microsoft_Internet_of_Things_Top_Ten_Reasons_Health.pdf/i.test(allLinks[i].href)){										if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '38'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '38'); }); } }
			if(/Microsoft_Internet_of_Things_Data_Sheet_Health.pdf/i.test(allLinks[i].href)){												if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '39'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '39'); }); } }
		}
		
		if (/internet-of-things-citynext.aspx/i.test(window.location)) {
			//40
			//41
			//42
			//43
			if(/dtcbvNo_QA4/i.test(allLinks[i].href)){																																if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '44'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '44'); }); } }
			if(/Autolib_Microsoft_Internet_of_Things_News_Article.pdf/i.test(allLinks[i].href)){											if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '45'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '45'); }); } }
			if(/Autolib_Microsoft_Internet_of_Things_Case_Study.pdf/i.test(allLinks[i].href)){												if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '46'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '46'); }); } }
			if(/Autolib_Microsoft_Internet_of_Things_Customer_Success_Summary.pdf/i.test(allLinks[i].href)){					if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '47'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '47'); }); } }
			//48                                     
			//49                                     
		}                                          
		                                           
		if (/internet-of-things-customer-stories.aspx\?id=1/i.test(window.location)) {
			if(/kpeNSamWgTU/i.test(allLinks[i].href)){																															if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '50'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '50'); }); } }
			if(/KUKA_Microsoft_Internet_of_Things_News_Article.pdf/i.test(allLinks[i].href)){												if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '51'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '51'); }); } }
			if(/KUKA_Microsoft_Internet_of_Things_Case_Study.pdf/i.test(allLinks[i].href)){													if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '52'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '52'); }); } }
			if(/KUKA_Microsoft_Internet_of_Things_Customer_Success_Summary.pdf/i.test(allLinks[i].href)){						if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '53'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '53'); }); } }
		}                                                                                                     			
		                                                                                                      			
		if (/internet-of-things-customer-stories.aspx\?id=2/i.test(window.location)) {                         			
			if(/xgnEaZ5pUO8/i.test(allLinks[i].href)){																																if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '54'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '54'); }); } }
			if(/Royal_Caribbean_Microsoft_Internet_of_Things_News_Article.pdf/i.test(allLinks[i].href)){							if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '55'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '55'); }); } }
			if(/Royal_Caribbean_Microsoft_Internet_of_Things_Case_Study.pdf/i.test(allLinks[i].href)){								if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '56'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '56'); }); } }
			if(/Royal_Caribbean_Microsoft_Internet_of_Things_Customer_Success_Summary.pdf/i.test(allLinks[i].href)){	if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '57'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '57'); }); } }

		}
		
		if (/internet-of-things-customer-stories.aspx\?id=3/i.test(window.location)) {
			if(/dtcbvNo_QA4/i.test(allLinks[i].href)){																																if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '58'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '58'); }); } }
			if(/Autolib_Microsoft_Internet_of_Things_News_Article.pdf/i.test(allLinks[i].href)){											if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '59'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '59'); }); } }
			if(/Autolib_Microsoft_Internet_of_Things_Case_Study.pdf/i.test(allLinks[i].href)){												if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '60'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '60'); }); } }
			if(/Autolib_Microsoft_Internet_of_Things_Customer_Success_Summary.pdf/i.test(allLinks[i].href)){					if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '61'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '61'); }); } }

		}
		
		if (/internet-of-things-customer-stories.aspx\?id=4/i.test(window.location)) {
			if(/_4dgMoycLkQ/i.test(allLinks[i].href)){																																if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '62'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '62'); }); } }
			if(/Great_River_Microsoft_Internet_of_Things_News_Article.pdf/i.test(allLinks[i].href)){									if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '63'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '63'); }); } }
			if(/Great_River_Microsoft_Internet_of_Things_Case_Study.pdf/i.test(allLinks[i].href)){										if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '64'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '64'); }); } }
			if(/Great_River_Microsoft_Internet_of_Things_Customer_Success_Summary.pdf/i.test(allLinks[i].href)){			if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '65'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '65'); }); } }

		}
		
		if (/internet-of-things-customer-stories.aspx\?id=5/i.test(window.location)) {
			if(/UsUxI9stiYs/i.test(allLinks[i].href)){																															if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '66'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '66'); }); } }
			if(/Lido_Microsoft_Internet_of_Things_News_Article.pdf/i.test(allLinks[i].href)){												if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '67'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '67'); }); } }
			if(/Lido_Microsoft_Internet_of_Things_Case_Study.pdf/i.test(allLinks[i].href)){													if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '68'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '68'); }); } }
			if(/Lido_Microsoft_Internet_of_Things_Customer_Success_Summary.pdf/i.test(allLinks[i].href)){						if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '69'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '69'); }); } }
		}
		
		
		if (/internet-of-things-customer-stories.aspx\?id=6/i.test(window.location)) {
			if(/buhhKdRnE6E/i.test(allLinks[i].href)){																																if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '70'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '70'); }); } }
			if(/Mayo_Microsoft_Internet_of_Things_News_Article.pdf/i.test(allLinks[i].href)){												if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '71'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '71'); }); } }
			if(/Mayo_Microsoft_Internet_of-Things_Case_Study.pdf/i.test(allLinks[i].href)){													if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '72'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '72'); }); } }
			if(/Henry_Mayo_Microsoft_Internet_of_Things_Customer_Success_Summary.pdf/i.test(allLinks[i].href)){			if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '73'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '73'); }); } }
		}		
		
		if (/internet-of-things-customer-stories.aspx\?id=7/i.test(window.location)) {
			if(/CCA_Microsoft_Internet_of_Things_News_Article.PDF/i.test(allLinks[i].href)){													if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '74'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '74'); }); } }
			if(/CCA_Microsoft_Internet_of_Things_Case_Study.PDF/i.test(allLinks[i].href)){														if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '75'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '75'); }); } }
			if(/CCA_Microsoft_Internet_of_Things_Customer_Success_Summary.PDF/i.test(allLinks[i].href)){							if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '76'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '76'); }); } }
		}
		
		//if (/internet-of-things-customer-stories.aspx\?id=8/i.test(window.location)) {
			//77
			//78
			//79
			//80
		//}
		
		if (/intelligent-systems.aspx/i.test(window.location)) {
			//81
			if(/1-1I5PT9W|1-1SIIEJW/i.test(allLinks[i].href)){																																																						if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '82' ); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '82' ); }); } }
			if(/1-1IC0TXD/i.test(allLinks[i].href)){																																																						if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '83' ); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '83' ); }); } }
			if(/What(%20| )Devices(%20| )Tell(%20| )You_Microsoft(%20| )Intelligent(%20| )Systems.pdf/i.test(allLinks[i].href)){																if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '84' ); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '84' ); }); } }
			if(/ZfGNq5UMSw0/i.test(allLinks[i].href)){																																																					if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '108'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '108'); }); } }
			if(/Gartner(%20| )and(%20| )Microsoft(%20| )on(%20| )Capitalizing(%20| )on(%20| )the(%20| )Internet(%20| )of(%20| )Things/i.test(allLinks[i].href)){if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '109'); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '109'); }); } }
			if(/o4PjFpa0wJc/i.test(allLinks[i].href)){																																																					if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '85' ); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '85' ); }); } }
		}
		
		if (/products-solutions.aspx/i.test(window.location)) {
			if(/Smarter(%20| )Business(%20| )Requires(%20| )Intelligent(%20| )Systems_IDG(%20| )TechNetwork.pdf/i.test(allLinks[i].href)){if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '86' ); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '86' ); }); } }
			if(/11-14RoadMap.mspx/i.test(allLinks[i].href)){																																							if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '87' ); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '87' ); }); } }
			if(/11-07WEAgile.aspx/i.test(allLinks[i].href)){																																							if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '88' ); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '88' ); }); } }
		}

		if (/products-solutions-overview.aspx/i.test(window.location)) {
			if(/11-07WEAgile.aspx/i.test(allLinks[i].href)){																																						if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '89' ); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '89' ); }); } }
		}
		
		if (/intelligent-systems-service.aspx/i.test(window.location)) {
			//if(/Microsoft(%20| )Azure(%20| )Intelligent(%20| )Systems(%20| )Service_Datasheet.pdf/i.test(allLinks[i].href)){					if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '90' ); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '90' ); }); } }
			
			if(/Microsoft(%20| )Azure(%20| )Intelligent(%20| )Systems(%20| )Service_Datasheet.pdf/i.test(allLinks[i].href)){						if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '104' ); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '104' ); }); } }
			if(/connect.microsoft.com\/site1132/i.test(allLinks[i].href)){																															if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '105' ); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '105' ); }); } }
			if(/www.windowsazure.com\/en-us/i.test(allLinks[i].href)){																																	if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '106' ); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '106' ); }); } }
			if(/NYpdNGl1hco/i.test(allLinks[i].href)){																																									if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '107' ); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '107' ); }); } }
		}
		
		if (/windows-embedded-8.aspx/i.test(window.location)) {
			if(/windows-embedded-8.aspx\#SeeIt/i.test(allLinks[i].href)){																																																			if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '91' ); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '91' ); }); } }
			if(/Windows(%20| )Embedded(%20| )8(%20| )Volume(%20| )Licensing(%20| )FAQ.pdf/i.test(allLinks[i].href)){																														if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '92' ); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '92' ); }); } }
			if(/Licensing_Windows_Embedded_8_VL_Brief.pdf/i.test(allLinks[i].href)){																																														if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '93' ); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '93' ); }); } }
			if(/Managing(%20| )Windows(%20| )Embedded(%20| )8(%20| )Devices(%20| )with(%20| )Configuration(%20| )Manager(%20| )2012(%20| )SP1.pdf/i.test(allLinks[i].href)){		if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '94' ); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '94' ); }); } }
			if(/E_SO78NWKB8/i.test(allLinks[i].href)){																																																													if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '95' ); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '95' ); }); } }
		}
		
		if (/windows-embedded-8-industry.aspx/i.test(window.location)) {
			if(/Windows(%20| )_Embedded_8.1_Industry(%20| )data(%20| )_sheet.pdf/i.test(allLinks[i].href)){														if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '96' ); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '96' ); }); } }
			if(/View(%20| )Hype(%20| )Cycle(%20| )for(%20| )Human-Computer(%20| )Interaction/i.test(allLinks[i].href)){								if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '97' ); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '97' ); }); } }
			if(/windowsembedded\/en-us\/developers.aspx/i.test(allLinks[i].href)){																											if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '98' ); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '98' ); }); } }
			if(/Windows(%20| )Embedded(%20| )8(%20| )Industry(%20| )Datasheet(%20| )for(%20| )Enterprise.pdf/i.test(allLinks[i].href)){if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '99' ); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '99' ); }); } }
		}
		
		if (/windows-embedded-8-standard.aspx/i.test(window.location)) {
			if(/WE(%20| )8(%20| )STANDARD(%20| )Datasheet_Final(%20| )098-117619.pdf/i.test(allLinks[i].href)){												if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '100' ); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '100' ); }); } }
			if(/Windows(%20| )Embedded(%20| )8(%20| )Standard(%20| )Datasheet(%20| )for(%20| )Enterprise.pdf/i.test(allLinks[i].href)){if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '101' ); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '101' ); }); } }
			if(/windowsembedded\/en-us\/developers.aspx/i.test(allLinks[i].href)){																											if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '102' ); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '102' ); }); } }
			if(/the_future_of_microsoft_net_new_options_new_choices_new_risks/i.test(allLinks[i].href)){																if(allLinks[i].addEventListener){ allLinks[i].addEventListener('click',function(event){ _set_SessionCookie("captEmbed", '103' ); },false); } else { allLinks[i].attachEvent('onclick',function(){ _set_SessionCookie("captEmbed", '103' ); }); } }
		}
	}
}

COMSCORE.SiteRecruit.Broker.config = {
	version: "5.0.3",
	//TODO:Karl extend cookie enhancements to ie userdata
		testMode: false,
	
	// cookie settings
	cookie:{
		name: 'msresearch',
		path: '/',
		domain:  '.microsoft.com' ,
		duration: 90,
		rapidDuration: 0,
		expireDate: ''
	},
	thirdPartyOptOutCookieEnabled : false,
	
	// optional prefix for pagemapping's pageconfig file
	prefixUrl: "",
	
	//events
	Events: {
		beforeRecruit: function() {
					}
	},
	
		mapping:[
	// m=regex match, c=page config file (prefixed with configUrl), f=frequency
	{m: 'http://[//w//.-]+/windowsembedded/en-us/internet-of-things.aspx', c: 'inv_c_p250749567-2403.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p250749567-2403.js*/, f: 0, p: 1, halt:true	}
	/*,{m: 'http://[//w//.-]+/windowsembedded/en-us/internet-of-things-retail.aspx', c: 'inv_c_p250749567-2403.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p250749567-2403.js*/, f: 0.8, p: 1 	}
	,{m: 'http://[//w//.-]+/windowsembedded/en-us/internet-of-things-manufacturing.aspx', c: 'inv_c_p250749567-2403.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p250749567-2403.js*/, f: 100, p: 1 	}
	,{m: 'http://[//w//.-]+/windowsembedded/en-us/internet-of-things-health.aspx', c: 'inv_c_p250749567-2403.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p250749567-2403.js*/, f: 0.8, p: 1 	}
	,{m: 'http://[//w//.-]+/windowsembedded/en-us/internet-of-things-citynext.aspx', c: 'inv_c_p250749567-2403.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p250749567-2403.js*/, f: 0.8, p: 1 	}
	,{m: '//[\\w\\.-]+/windowsembedded/en-us/internet-of-things-customer-stories.aspx\\?id=1', c: 'inv_c_p250749567-2403.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p250749567-2403.js*/, f: 0.8, p: 1 	}
	,{m: '//[\\w\\.-]+/windowsembedded/en-us/internet-of-things-customer-stories.aspx\\?id=2', c: 'inv_c_p250749567-2403.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p250749567-2403.js*/, f: 0.8, p: 1 	}
	,{m: '//[\\w\\.-]+/windowsembedded/en-us/internet-of-things-customer-stories.aspx\\?id=3', c: 'inv_c_p250749567-2403.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p250749567-2403.js*/, f: 0.8, p: 1 	}
	,{m: '//[\\w\\.-]+/windowsembedded/en-us/internet-of-things-customer-stories.aspx\\?id=4', c: 'inv_c_p250749567-2403.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p250749567-2403.js*/, f: 0.8, p: 1 	}
	,{m: '//[\\w\\.-]+/windowsembedded/en-us/internet-of-things-customer-stories.aspx\\?id=5', c: 'inv_c_p250749567-2403.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p250749567-2403.js*/, f: 0.8, p: 1 	}
	,{m: '//[\\w\\.-]+/windowsembedded/en-us/internet-of-things-customer-stories.aspx\\?id=6', c: 'inv_c_p250749567-2403.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p250749567-2403.js*/, f: 0.8, p: 1 	}
	,{m: '//[\\w\\.-]+/windowsembedded/en-us/internet-of-things-customer-stories.aspx\\?id=7', c: 'inv_c_p250749567-2403.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p250749567-2403.js*/, f: 0.8, p: 1 	}
	,{m: '//[\\w\\.-]+/windowsembedded/en-us/internet-of-things-customer-stories.aspx\\?id=8', c: 'inv_c_p250749567-2403.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p250749567-2403.js*/, f: 0.8, p: 1 	}
	,{m: 'http://[//w//.-]+/windowsembedded/en-us/windows-embedded-8.aspx', c: 'inv_c_p250749567-2403.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p250749567-2403.js*/, f: 0.8, p: 1 	}
	,{m: 'http://[//w//.-]+/windowsembedded/en-us/windows-embedded-8-industry.aspx', c: 'inv_c_p250749567-2403.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p250749567-2403.js*/, f: 0.8, p: 1 	}
	,{m: 'http://[//w//.-]+/windowsembedded/en-us/windows-embedded-8-standard.aspx', c: 'inv_c_p250749567-2403.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p250749567-2403.js*/, f: 0.8, p: 1 	}*/
]
};
COMSCORE.SiteRecruit.Broker.run();