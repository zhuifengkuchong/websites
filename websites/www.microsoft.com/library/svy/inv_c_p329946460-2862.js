/*
Copyright (c) 2015, comScore Inc. All rights reserved.
version: 5.0.3
*/
COMSCORE.SiteRecruit.Builder.config = {
	version: "5.0.3",
	
	// invitations' settings
	invitation: [
							
											{ 	methodology: 2,
					projectId: 'p329946460',
					weight: 100,
					isRapidCookie: false,
					acceptUrl: 'http://survey2.surveysite.com/wix/p329946460.aspx',
					secureUrl: '',
					acceptParams: {
						raw: '',
						siteCode: '2862',
												cookie: [
													],
												otherVariables: [
													]
					},
					viewUrl: 'http://b.scorecardresearch.com/b',
					viewParams: '',
					content: '<div id="srinvite" style="width:420px; border:1px solid #0073C6; border-top:8px solid #0073C6; margin:0; background-color:#FFFFFF; padding:0"> 	<div class="srtext" style="text-Align:left;margin:20px 60px 0px 50px; font-family:\'Segoe UI\'; font-size:18px; color:#666666; background-color:#FFFFFF;"> 		<img src="logo-stripe-short.gif"/*tpa=http://www.microsoft.com/library/svy/logo-stripe-short.gif*/ border="0"  alt="Microsoft Logo"/> 	</div> 	<div class="srtext" style="position:relative; top:-42px; right:-30px;width:25px;"> 		<img src="close.gif"/*tpa=http://www.microsoft.com/library/svy/close.gif*/ style="border:0; margin-left:360px; padding:0;cursor:pointer;" onclick="@declineHandler" alt="Close button link" tabindex="1"/>  	</div> 	<div class="srtext" style="margin:0px 50px; font-family:\'Segoe UI\'; font-size:14px; color:#0073C6; background-color:#FFFFFF;width:320px;text-align:left;"> 		Microsoft is conducting an online survey to understand your opinions of the Microsoft Dynamics Web site.  We’d like to see which pages you visit today and then have you complete a survey when you leave the Microsoft Dynamics Web site. <br /><br /> Would you like to participate?  	</div> 	<div style="margin:20px 20px 20px 50px;"> 		<table style="line-height:12px;font-family:\'Segoe UI\';font-size:12px;color:#0073C6;border:0;"><td width="225" style="border:0;"> 					<input type="button" style="border-style:none; background-color:#0073C6; color:#FFFFFF; font-size:14px; height:35px;width:100px; cursor:pointer;" value=" Yes " onclick="@acceptHandler;" tabindex="2"/>	 &nbsp; 					<input type="button" style="border-style:none; background-color:#0073C6; color:#FFFFFF; font-size:14px; height:35px;width:100px; cursor:pointer;" value=" No " onclick="@declineHandler;" tabindex="3"/>	<br /><br /></td></tr> 			<tr><td><a href="http://privacy.microsoft.com/en-us/default.mspx" target="_blank" style="font-family:\'Segoe UI\'; font-size:12px; color:#0073C6;">Privacy Statement</a></td></tr>		</table> <br /> 	</div> </div>',
					height: 360,
					width: 420,
					revealDelay: 1000,
					horizontalAlignment: 1,
					verticalAlignment: 1,
					horizontalMargin: 0,
					verticalMargin: 0,
					startingHorizontalAlignment: 1,
					startingVerticalAlignment: 0,
					startingHorizontalMargin: 0,
					startingVerticalMargin: 0,
					isHideBlockingElements: false,
					isAutoCentering: true,
					url: 'http://www.microsoft.com/library/svy/SiteRecruit_Tracker.htm',
					trackerGracePeriod: 8,
					usesSilverlight: false
					
					//silverlight config
										
											,trackerWindow: {
							width: 400,
							height: 270,
							orientation: 1,
							offsetX: 0,
							offsetY: 0,
							hideToolBars: true,
							trackerPageConfigUrl: 'trackerConfig_p329946460.js'/*tpa=http://www.microsoft.com/library/svy/trackerConfig_p329946460.js*/
							// future feature: 
							//features: "location=0,menubar=0,resizable=1,scrollbars=1,toolbar=0"
						}
																				,Events: {
						beforeRender: function() {
														document.onkeyup = function(evt) {
    evt = evt || window.event;
    if (evt.keyCode == 27) {
        COMSCORE.SiteRecruit.Builder.invitation.decline(this);
    }
};
													}
						,beforeDestroy: function() {
														
													}
						,beforeAccept: function() {
														
													}
						,beforeDecline: function() {
														
													}
						,beforeRenderUpdate: function() {
														
													}
						,afterRender: function() {
														
													}
					}
				}
						]
};
COMSCORE.SiteRecruit.Builder.run();
