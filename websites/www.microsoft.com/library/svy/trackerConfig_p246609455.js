/*
Copyright (c) 2012, comScore Inc. All rights reserved.
version: 5.0.3
*/
COMSCORE.SiteRecruit.Tracker.config = {
	isAutoFocus: true,
	title: 'Microsoft Survey',
	content: '<!-- TOOLBAR_EXEMPT --> <style type="text/css">body{margin:2px;}body,td{font-family:Verdana,Arial,Helvetica,sans-serif;font-size:11px;color:#000000;background-color:#FFFFFF;}a:link,a:visited,a:active{text-decoration:underline;}a:hover{text-decoration:none;}.main{border:1px solid #999999; } .main td { } .main .content { padding: 15px; } </style><table width="100%" cellpadding="0px" cellspacing="0px" border="0px" class="main"><tr><td><img src="logo-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/logo-stripe.gif*/ /></td></tr><tr><td background="top-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/top-stripe.gif*/ style="background-repeat: repeat-x;"><img src="top-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/top-stripe.gif*/ /></td></tr><tr><td class="content"><p><div style="font-family: Verdana, Arial, Helvetica, sans-serif;font-size:12px;color:#000000;">Please do not close this window.<br /><br /> Thank you! The survey will appear here when you\'ve completed your visit, so <b>please do not close this window</b>.</div></p><p id="counterText" style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 12px; color: #000000;">This window will minimize in <span id="counter">7</span> seconds.</p> <p><div style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 12px; color: #000000;"><a href="http://privacy.microsoft.com/en-us/default.mspx" target="_blank">Privacy Statement</a></div></p> </td> </tr> <tr> <td background="bottom-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/bottom-stripe.gif*/ style="background-repeat: repeat-x;"><img src="bottom-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/bottom-stripe.gif*/ /></td> </tr> </table><script id="customScript">if (/crossDomain=1/i.test(window.location)){document.getElementById(\'counterText\').style.visibility=\'hidden\';}else{COMSCORE.Custom = (function() { 	return { 		getCounter: function() 		{ 		var el = document.getElementById(\'counter\');return el;},getCounterValue: function(){var el = COMSCORE.Custom.getCounter();var v = Number(el.innerHTML);return v;},decrementCounter:function(){var el = COMSCORE.Custom.getCounter();var v = COMSCORE.Custom.getCounterValue();if (v > 0) el.innerHTML = --v;if (v > 0){window.setTimeout(function(){COMSCORE.Custom.decrementCounter();}, \'1000\');}else{ 		self.blur();document.getElementById(\'counterText\').innerHTML = \'&nbsp;\';}},initCounter:function(){window.setTimeout(function(){COMSCORE.Custom.decrementCounter();}, \'1000\');}};})();if(/msie/i.test(navigator.userAgent)){  	COMSCORE.Custom.initCounter(); }else{  	try{ 		var element = document.getElementById("counterText");element.parentNode.removeChild(element); 	}catch(err){} }  }</script>',
	match: '',	 // browserable regex match
	
	// START 5.1.3
	cookie: {
		name: 'cddsinprogress',
		path: '/',
		domain: '.microsoft.com'
	},
	
	trackerMappings: [
		{
			d: '.microsoftstore.com',
			c: 'Unknown_83_filename'/*tpa=http://www.microsoft.com/library/svy/www.microsoft.com/library/svy/msftstore/trackerConfig_p246609455.js*/,
			t: 'http://www.microsoftstore.com/SiteRecruit_Tracker.htm',
			k: 'msresearch'
		},
		{
			d: '.xbox.com',
			c: 'Unknown_83_filename'/*tpa=http://www.microsoft.com/library/svy/js.microsoft.com/library/svy/xbox/trackerConfig_p246609455.js*/,
			t: 'http://www.xbox.com/shell/siterecruit_tracker_v2.htm',
			k: 'xboxresearch'
		},
		{
			d: '.windowsphone.com',
			c: 'Unknown_83_filename'/*tpa=http://www.microsoft.com/library/svy/siterecruit.comscore.com/sr/windowsphone/trackerConfig_p246609455.js*/,
			t: 'http://www.windowsphone.com/SiteRecruit_Tracker.htm',
			k: 'msresearch'
		},
		{
			d: '.office.com',
			c: 'Unknown_83_filename'/*tpa=http://www.microsoft.com/library/svy/www.microsoft.com/library/svy/office/support/trackerConfig_p246609455.js*/,
			t: 'http://support.office.com/core/siterecruit_tracker.htm',
			k: 'msresearch'
		}
	],
	
	domainSwitch: 'tracking3p',
	domainMatch: '^(https?:\/\/)?([\\da-z\.-]+)\.([a-z\.]{2,6})',
	cddsEventMessage: 'cddsMessage',
	// END 5.1.3
	
	isCaptureTrail: true,
	delay: 0,
	gracePeriod: 8
};
//CROSS-DOMAIN DEPARTURE FUNCTIONALITY
if (/crossDomain=1/i.test(window.location)) {
	COMSCORE.SiteRecruit.Tracker.config.delay = 5000;
	
	// SEND SIGNAL TO BROKER TO STOP RECRUITMENT
	var d = COMSCORE.SiteRecruit.Tracker.config.cookie;
	
	var c = d.name + '=1'                                
  	+ '; path=' + d.path
  	+ '; domain=' + d.domain;
	document.cookie = c;
}
//END CROSS-DOMAIN DEPARTURE FUNCTIONALITY

var cs_tobj = COMSCORE.SiteRecruit.Tracker.surveyData;
// SET VARIABLE TO DETERMINE WHETHER OR NOT SESSIONSTORAGE IS SUPPORTED
cs_tobj.sstor = "";

// SET VARIABLE TO COUNT NUMBER OF EXPOSURES DURING TRACKING
cs_tobj.bcncnt;

// SET VARIABLE TO DETERMINE WHETHER OR NOT USER ALLOWS TRAFFIC TO SCORECARDRESEARCH
if (/bcn=(true|false)&/i.test(window.location)) {
	cs_tobj.bcn = RegExp.$1;
}
else if (window.sessionStorage.csProp != undefined) {
	if (/bcn=(true|false)&/i.test(window.sessionStorage.csProp)) {
		cs_tobj.bcn = RegExp.$1;
	}
	else {
		cs_tobj.bcn = false;
	}
}
else {
	cs_tobj.bcn = false;
}

cs_tobj.dnt = false;	// DO NOT TRACK - ADD PROP OR BEACON FOR ONE CYCLE

function propFromUrl() {
	if (/prop001/i.test(window.location)) {
		if (/propOrd=(.*)&/i.test(window.location)) {cs_tobj.propOrd = RegExp.$1;}
		if (/prop001\=(\d+)/i.test(window.location)){cs_tobj.prop001 = RegExp.$1;}
		if (/prop002\=(\d+)/i.test(window.location)){cs_tobj.prop002 = RegExp.$1;}
		if (/prop003\=(\d+)/i.test(window.location)){cs_tobj.prop003 = RegExp.$1;}
		if (/prop004\=(\d+)/i.test(window.location)){cs_tobj.prop004 = RegExp.$1;}
		if (/prop005\=(\d+)/i.test(window.location)){cs_tobj.prop005 = RegExp.$1;}
		if (/prop006\=(\d+)/i.test(window.location)){cs_tobj.prop006 = RegExp.$1;}
		if (/prop007\=(\d+)/i.test(window.location)){cs_tobj.prop007 = RegExp.$1;}
		if (/prop008\=(\d+)/i.test(window.location)){cs_tobj.prop008 = RegExp.$1;}
		if (/prop009\=(\d+)/i.test(window.location)){cs_tobj.prop009 = RegExp.$1;}
		if (/prop010\=(\d+)/i.test(window.location)){cs_tobj.prop010 = RegExp.$1;}
		if (/prop011\=(\d+)/i.test(window.location)){cs_tobj.prop011 = RegExp.$1;}
		if (/prop012\=(\d+)/i.test(window.location)){cs_tobj.prop012 = RegExp.$1;}
		if (/prop013\=(\d+)/i.test(window.location)){cs_tobj.prop013 = RegExp.$1;}
		if (/prop014\=(\d+)/i.test(window.location)){cs_tobj.prop014 = RegExp.$1;}
		if (/prop015\=(\d+)/i.test(window.location)){cs_tobj.prop015 = RegExp.$1;}
		if (/prop016\=(\d+)/i.test(window.location)){cs_tobj.prop016 = RegExp.$1;}
		if (/prop017\=(\d+)/i.test(window.location)){cs_tobj.prop017 = RegExp.$1;}
		if (/prop018\=(\d+)/i.test(window.location)){cs_tobj.prop018 = RegExp.$1;}
		if (/prop019\=(\d+)/i.test(window.location)){cs_tobj.prop019 = RegExp.$1;}
		if (/prop998\=(\d+)/i.test(window.location)){cs_tobj.prop998 = RegExp.$1;}
		if (/prop999\=(\d+)/i.test(window.location)){cs_tobj.prop999 = RegExp.$1;}
		if (/path001\=(\d+)/i.test(window.location)){cs_tobj.path001 = RegExp.$1;}
		if (/path002\=(\d+)/i.test(window.location)){cs_tobj.path002 = RegExp.$1;}
		if (/bcncnt\=(\d+)/i.test(window.location)) {cs_tobj.bcncnt  = RegExp.$1;}
		if (/ns__p\=(\d+)/i.test(window.location)){cs_tobj.ns__p = RegExp.$1;}
		if (/ns__ts\=(\d+)/i.test(window.location)){cs_tobj.ns__ts = RegExp.$1;}
		return true;
	}
	else {
		return false;
	}
}

function propInitialize() {
	cs_tobj.propOrd = "";
	cs_tobj.prop001 = "0";
	cs_tobj.prop002 = "0";
	cs_tobj.prop003 = "0";
	cs_tobj.prop004 = "0";
	cs_tobj.prop005 = "0";
	cs_tobj.prop006 = "0";
	cs_tobj.prop007 = "0";
	cs_tobj.prop008 = "0";
	cs_tobj.prop009 = "0";
	cs_tobj.prop010 = "0";
	cs_tobj.prop011 = "0";
	cs_tobj.prop012 = "0";
	cs_tobj.prop013 = "0";
	cs_tobj.prop014 = "0";
	cs_tobj.prop015 = "0";
	cs_tobj.prop016 = "0";
	cs_tobj.prop017 = "0";
	cs_tobj.prop018 = "0";
	cs_tobj.prop019 = "0";
	cs_tobj.prop998 = "0";
	cs_tobj.prop999 = "0";
	cs_tobj.bcncnt  = "0";
	cs_tobj.path001 = "0";
	cs_tobj.path002 = "0";
	cs_tobj.ns__p 	= "0";
	cs_tobj.ns__ts 	= "0";
}

function propFromStorage() {
	if (window.sessionStorage.csProp != undefined) {
		cs_tobj.dnt = true;
		if (/propOrd=(.*)&/i.test(window.sessionStorage.csProp)) {cs_tobj.propOrd = RegExp.$1;}
		if (/prop001\=(\d+)/i.test(window.sessionStorage.csProp)){cs_tobj.prop001 = RegExp.$1;}
		if (/prop002\=(\d+)/i.test(window.sessionStorage.csProp)){cs_tobj.prop002 = RegExp.$1;}
		if (/prop003\=(\d+)/i.test(window.sessionStorage.csProp)){cs_tobj.prop003 = RegExp.$1;}
		if (/prop004\=(\d+)/i.test(window.sessionStorage.csProp)){cs_tobj.prop004 = RegExp.$1;}
		if (/prop005\=(\d+)/i.test(window.sessionStorage.csProp)){cs_tobj.prop005 = RegExp.$1;}
		if (/prop006\=(\d+)/i.test(window.sessionStorage.csProp)){cs_tobj.prop006 = RegExp.$1;}
		if (/prop007\=(\d+)/i.test(window.sessionStorage.csProp)){cs_tobj.prop007 = RegExp.$1;}
		if (/prop008\=(\d+)/i.test(window.sessionStorage.csProp)){cs_tobj.prop008 = RegExp.$1;}
		if (/prop009\=(\d+)/i.test(window.sessionStorage.csProp)){cs_tobj.prop009 = RegExp.$1;}
		if (/prop010\=(\d+)/i.test(window.sessionStorage.csProp)){cs_tobj.prop010 = RegExp.$1;}
		if (/prop011\=(\d+)/i.test(window.sessionStorage.csProp)){cs_tobj.prop011 = RegExp.$1;}
		if (/prop012\=(\d+)/i.test(window.sessionStorage.csProp)){cs_tobj.prop012 = RegExp.$1;}
		if (/prop013\=(\d+)/i.test(window.sessionStorage.csProp)){cs_tobj.prop013 = RegExp.$1;}
		if (/prop014\=(\d+)/i.test(window.sessionStorage.csProp)){cs_tobj.prop014 = RegExp.$1;}
		if (/prop015\=(\d+)/i.test(window.sessionStorage.csProp)){cs_tobj.prop015 = RegExp.$1;}
		if (/prop016\=(\d+)/i.test(window.sessionStorage.csProp)){cs_tobj.prop016 = RegExp.$1;}
		if (/prop017\=(\d+)/i.test(window.sessionStorage.csProp)){cs_tobj.prop017 = RegExp.$1;}
		if (/prop018\=(\d+)/i.test(window.sessionStorage.csProp)){cs_tobj.prop018 = RegExp.$1;}
		if (/prop019\=(\d+)/i.test(window.sessionStorage.csProp)){cs_tobj.prop019 = RegExp.$1;}
		if (/prop998\=(\d+)/i.test(window.sessionStorage.csProp)){cs_tobj.prop998 = RegExp.$1;}
		if (/prop999\=(\d+)/i.test(window.sessionStorage.csProp)){cs_tobj.prop999 = RegExp.$1;}
		if (/path001\=(\d+)/i.test(window.sessionStorage.csProp)){cs_tobj.path001 = RegExp.$1;}
		if (/path002\=(\d+)/i.test(window.sessionStorage.csProp)){cs_tobj.path002 = RegExp.$1;}
		if (/bcncnt\=(\d+)/i.test(window.sessionStorage.csProp)) {cs_tobj.bcncnt  = RegExp.$1;}
		if (/ns__p\=(\d+)/i.test(window.sessionStorage.csProp)){cs_tobj.ns__p = RegExp.$1;}
		if (/ns__ts\=(\d+)/i.test(window.sessionStorage.csProp)){cs_tobj.ns__ts = RegExp.$1;}
		return true;
	}
	else {
		return false;
	}
}


if (typeof(Storage) == "undefined") {
	// NO SUPPORT
	cs_tobj.sstor = false;
	if (propFromUrl() == false) {
		propInitialize();
	}
}
else {
	// STORAGE SUPPORT
	cs_tobj.sstor = true;
	if (propFromUrl() == false) {
		if (propFromStorage() == false) {
			// FIRST LOAD
			propInitialize();
		}
	}
	else {
		var urlPropOrd = cs_tobj.propOrd;
		if (propFromStorage() == true) {
			if(cs_tobj.propOrd.length < urlPropOrd.length) {
				propFromUrl();
			}
		}
	}
}

setTimeout("COMSCORE.SiteRecruit.Tracker.start()", COMSCORE.SiteRecruit.Tracker.config.delay);
// END 5.1.3