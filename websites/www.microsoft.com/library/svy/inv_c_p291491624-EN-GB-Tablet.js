/*
Copyright (c) 2014, comScore Inc. All rights reserved.
version: 5.0.3
*/
COMSCORE.SiteRecruit.Builder.config = {
	version: "5.0.3",
	
	// invitations' settings
	invitation: [
							
											{ 	methodology: 2,
					projectId: 'p291491624',
					weight: 100,
					isRapidCookie: false,
					acceptUrl: 'http://survey2.securestudies.com/wix/p291491624.aspx',
					secureUrl: 'https://survey2.securestudies.com/wix/p291491624.aspx',
					acceptParams: {
						raw: 'SURVEY_TYPE=2&l=2057',
						siteCode: '2712',
												cookie: [
													],
												otherVariables: [
													]
					},
					viewUrl: '//b.scorecardresearch.com/b',
					viewParams: 'c6=2',
					content: '<div id="srinvite" style="width:450px; border:1px solid #0073C6; border-top:8px solid #0073C6; margin:0; background-color:#FFFFFF; padding:0"> 	<div class="srtext" style="text-Align:left;margin:10px 60px 0px 50px; font-family:\'Segoe UI\'; font-size:18px; color:#666666; background-color:#FFFFFF;"> 		<img src="logo-stripe-short.gif"/*tpa=http://www.microsoft.com/library/svy/logo-stripe-short.gif*/ border="0"  alt="Microsoft Logo"/> 	</div> 	<div class="srtext" style="position:relative; top:-42px; right:-30px;width:25px;"> 		<img src="close.gif"/*tpa=http://www.microsoft.com/library/svy/close.gif*/ style="border:0; margin-left:360px; padding:0;cursor:pointer;" onclick="@declineHandler" alt="Close button link" tabindex="1"/>  	</div> 	<div class="srtext" style="margin:0px 50px; font-family:\'Segoe UI\'; font-size:14px; color:#0073C6; background-color:#FFFFFF;width:320px;text-align:left;"> 		Microsoft is conducting an online survey to understand your opinions of the Microsoft website. We’d like to see which pages you visit today and then ask you to complete a survey when you leave the Microsoft website.  If you choose to participate, the online survey will be presented to you when you leave the Microsoft website.<br /><br /> Would you like to participate?  	</div> 	<div style="margin:20px 30px 10px 40px;"> 		<table style="line-height:12px;font-family:\'Segoe UI\';font-size:12px;color:#0073C6;border:0;"> 			<tr><td style="width:100px;vertical-Align:bottom;border:0;"><a href="http://privacy.microsoft.com/en-gb/" style="text-decoration:none; font-size:13px; color:#0073C6"  target="_blank">Privacy Statement</a></td> 				<td width="225" align="center" style="border:0;"> 					<input type="button" style="border-style:none; background-color:#0073C6; color:#FFFFFF; font-size:14px; height:35px;width:100px; cursor:pointer;" value=" Yes " onclick="@acceptHandler;" tabindex="2"/>	 &nbsp; 					<input type="button" style="border-style:none; background-color:#0073C6; color:#FFFFFF; font-size:14px; height:35px;width:100px; cursor:pointer;" value=" No " onclick="@declineHandler;" tabindex="3"/>	<br /> 					</td></tr> 		</table> <br /> 	</div> </div>',
					height: 310,
					width: 450,
					revealDelay: 1000,
					horizontalAlignment: 1,
					verticalAlignment: 1,
					horizontalMargin: 0,
					verticalMargin: 140,
					startingHorizontalAlignment: 1,
					startingVerticalAlignment: 0,
					startingHorizontalMargin: 0,
					startingVerticalMargin: 0,
					isHideBlockingElements: false,
					isAutoCentering: true,
					url: 'http://www.microsoft.com/library/svy/SiteRecruit_Tracker.htm',
					trackerGracePeriod: 7,
					usesSilverlight: false
					
					//silverlight config
										
											,trackerWindow: {
							width: 400,
							height: 270,
							orientation: 1,
							offsetX: 0,
							offsetY: 0,
							hideToolBars: true,
							trackerPageConfigUrl: 'trackerConfig_p291491624-EN-GB.js'/*tpa=http://www.microsoft.com/library/svy/trackerConfig_p291491624-EN-GB.js*/
							// future feature: 
							//features: "location=0,menubar=0,resizable=1,scrollbars=1,toolbar=0"
						}
																				,Events: {
						beforeRender: function() {
														document.onkeyup = function(evt) {
    evt = evt || window.event;
    if (evt.keyCode == 27) {
        COMSCORE.SiteRecruit.Builder.invitation.decline(this);
    }
};

						if(_mobile){//MOBILE - append mobile=1; c6=1
							COMSCORE.SiteRecruit.Builder.invitation.config.acceptParams.raw += "&mobile=1";
							COMSCORE.SiteRecruit.Builder.invitation.config.viewParams = "&c6=1";
							COMSCORE.SiteRecruit.Builder.invitation.config.horizontalAlignment= 0;
							COMSCORE.SiteRecruit.Builder.invitation.config.verticalAlignment= 0;
							COMSCORE.SiteRecruit.Builder.invitation.config.horizontalMargin= 4;
							COMSCORE.SiteRecruit.Builder.invitation.config.verticalMargin= 4;

							var screenWidth = self.screen.availWidth, screenHeight = self.screen.availHeight;
							//var screenWidth = self.screen.width, screenHeight = self.screen.Height;
							if(screenWidth <=770){
								_w = 240; _h = 300;
								_contstr = '<div id="srinvite" style="width:'+_w+'px; border:1px solid #0073C6; border-top:8px solid #0073C6; margin:0; background-color:#FFFFFF; padding:0;"> 	 	<div class="srtop" style="background:#FFFFFF; margin:0; padding:0; vertical-align:top;"><img src="logo-stripe-short.gif"/*tpa=http://www.microsoft.com/library/svy/logo-stripe-short.gif*/ style="margin:10px" alt="Microsoft Logo"/><div style="position:absolute; top:26px; right:14px;"><img src="close.gif"/*tpa=http://www.microsoft.com/library/svy/close.gif*/ style="border:0;cursor:pointer;" onclick="@declineHandler" alt="Close button link" tabindex="1"/></div> 	</div> 	  	<div class="srtext" style="margin:10px 10px; font-family:segoe ui; font-size:14px; color:#666666; background-color:#FFFFFF;">Microsoft is conducting an online survey to understand your opinions of the Microsoft website. We’d like to see which pages you visit today and then ask you to complete a survey when you leave the Microsoft website.  If you choose to participate, the online survey will be presented to you when you leave the Microsoft website.<br />Would you like to participate?  </div> 	 	<div style="margin:4px"><input type="button" style="border-style:none; margin:4px; background-color:#0073C6; color:#FFFFFF; font-size:14px; height:30px;width:60px; cursor:pointer;" value=" Yes " onclick="@acceptHandler;" tabindex="2"/><input type="button" style="border-style:none; background-color:#0073C6; color:#FFFFFF; margin:10px font-size:14px; height:30px;width:60px; cursor:pointer;" value=" No " onclick="@declineHandler;" tabindex="3"/><br /><a href="http://privacy.microsoft.com/en-gb/" style="font-family:segoe ui; font-size:13px; color:#0073C6;" target="_blank">Privacy Statement</a></div>	</div>';
								COMSCORE.SiteRecruit.Builder.invitation.config.content = _contstr;
								COMSCORE.SiteRecruit.Builder.invitation.config.width = _w;
								COMSCORE.SiteRecruit.Builder.invitation.config.height = _h;
								
							}
						}
													}
						,beforeDestroy: function() {
														
													}
						,beforeAccept: function() {
														
													}
						,beforeDecline: function() {
														
													}
						,beforeRenderUpdate: function() {
														
													}
						,afterRender: function() {
														
													}
					}
				}
						]
};
COMSCORE.SiteRecruit.Builder.run();
