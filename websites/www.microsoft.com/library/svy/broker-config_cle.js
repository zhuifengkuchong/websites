/*
Copyright (c) 2012, comScore Inc. All rights reserved.
version: 5.0.3
*/
_cleHalt = false;
_refv = escape(document.referrer);	
try{
	if(_refv.search(/www\.microsoft\.com\/windows\/pc-selector|windows\.microsoft\.com\/en-us\/windows\/shop\/pcs/i) !=-1){ 
		_cleHalt=true; 	
	}else{ _cleHalt=false; }
}catch(e){		}
COMSCORE.SiteRecruit.Broker.config = {
	version: "5.0.3",
	//TODO:Karl extend cookie enhancements to ie userdata
		testMode: false,
	
	// cookie settings
	cookie:{
		name: 'msresearch',
		path: '/',
		domain:  '.microsoft.com' ,
		duration: 90,
		rapidDuration: 0,
		expireDate: ''
	},
	
	// optional prefix for pagemapping's pageconfig file
	prefixUrl: "",
	
	//events
	Events: {
		beforeRecruit: function() {
					}
	},
	
		mapping:[
	// m=regex match, c=page config file (prefixed with configUrl), f=frequency
	{m: '^http://www\\.microsoft\\.com/windows/pc-selector/', c: 'inv_c_p147490063-EN-US.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p147490063-EN-US.js*/, f:100, p: 0, halt: _cleHalt   	}
	,{m: 'surface/en-us/products/surface-pro-3', c: 'inv_c_p296258930-US-Surface-CLE.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p296258930-US-Surface-CLE.js*/, f: 100, p: 1  	}
	,{m: 'tablet|laptop|premiumlaptop|phone', c: 'inv_c_p291491624-US-CLE.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p291491624-US-CLE.js*/, f: 100, p: 1 	}	
	,{m: 'en-gb/.*(tablet|phone|laptop)', c: 'inv_c_p291491624-EN-GB-CLE.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p291491624-EN-GB-CLE.js*/, f: 100, p: 2 	}
	,{m: 'fr-fr/.*(tablet|phone)', c: 'inv_c_p291491624-FR-FR-CLE.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p291491624-FR-FR-CLE.js*/, f: 100, p: 2 	}
	,{m: 'de-de/.*phone', c: 'inv_c_p291491624-DE-DE-CLE.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p291491624-DE-DE-CLE.js*/, f: 100, p: 2 	}
	,{m: 'ja-jp/.*tablet', c: 'inv_c_p291491624-JA-JP-CLE.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p291491624-JA-JP-CLE.js*/, f: 100, p: 2 	}
	,{m: 'zh-cn/.*tablet', c: 'inv_c_p291491624-ZH-CN-CLE.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p291491624-ZH-CN-CLE.js*/, f: 100, p: 2 	}
	,{m: 'it-it/.*phone', c: 'inv_c_p291491624-IT-IT-CLE.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p291491624-IT-IT-CLE.js*/, f: 100, p: 2 	}
	,{m: 'microsoft\\.com/en-us/windowsapps', c: 'inv_c_p271142646-CLE.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_p271142646-CLE.js*/, f: 100, p: 0  	}
]
};
COMSCORE.SiteRecruit.Broker.run();