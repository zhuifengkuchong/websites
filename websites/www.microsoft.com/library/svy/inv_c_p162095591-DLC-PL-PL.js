/*
Copyright (c) 2012, comScore Inc. All rights reserved.
version: 5.0.3
*/
COMSCORE.SiteRecruit.Builder.config = {
	version: "5.0.3",
	
	// invitations' settings
	invitation: [
							
											{ 	methodology: 2,
					projectId: 'p162095591',
					weight: 1,
					isRapidCookie: false,
					acceptUrl: 'http://survey2.surveysite.com/wix/p162095591.aspx',
					secureUrl: '',
					acceptParams: {
						raw: 'l=21&mth=1',
						siteCode: '1853',
												cookie: [
													],
												otherVariables: [
													]
					},
					viewUrl: 'http://web.survey-poll.com/tc/CreateLog.aspx',
					viewParams: 'http://www.microsoft.com/library/svy/log=comscore/view/p162095591-view.log',
					content: '<table width="360" cellpadding="3" cellspacing="0" border="0" style="background:#FFFFFF"><tr><td> <table width="100%" cellpadding="1" cellspacing="0" border="0" bgcolor="#999999"><tr><td> <table width="100%" cellpadding="0" cellspacing="0" border="0" style="background:#FFFFFF"><tr valign="top"><td> <img src="logo-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/logo-stripe.gif*/ /><a href="Close" onclick="@declineHandler"><img border="0" src="close.gif"/*tpa=http://www.microsoft.com/library/svy/close.gif*/ /></a><br /> <img src="top-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/top-stripe.gif*/ /> <table width="100%" cellpadding="5"><tr><td>  <div style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 11px; color: #000000; margin: 0 0 15px 0;">Celem tej ankiety jest poznanie Państwa opinii na temat Microsoft.com - Centrum pobierania. Jeśli zdecydują sie Państwo na udział w badaniu, ankieta pojawi się po opuszczeniu Microsoft.com - Centrum pobierania.<br /><br /> Czy chcą Państwo wziąć udział w badaniu?</div>  <div align="center" style="margin: 0; padding: 0"> <input style="margin: 0; padding: 0" type="button" value="  Tak  " onclick="@acceptHandler" />&nbsp;&nbsp; <input style="margin: 0; padding: 0"  type="button" value=" Nie " onclick="@declineHandler" /> </div>  <div style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 11px; color: #000000; margin: 15px 0 2px 0;"><a style="color:#0000EE; text-decoration: underline; " href="http://privacy.microsoft.com/pl-pl/default.mspx" target="_blank">O&#347;wiadczenie dotycz&#261;ce ochrony prywatno&#347;ci</a></div>  </td></tr></table> <img src="bottom-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/bottom-stripe.gif*/ /></td></tr></table> </td></tr></table> </td></tr></table>   ',
					height: 210,
					width: 390,
					revealDelay: 1000,
					horizontalAlignment: 1,
					verticalAlignment: 1,
					horizontalMargin: 0,
					verticalMargin: 0,
					isHideBlockingElements: false,
					isAutoCentering: true,
					url: 'http://www.microsoft.com/library/svy/SiteRecruit_Tracker.htm',
					trackerGracePeriod: 5
					,usesSilverlight: false
					
					//silverlight config
										
											,trackerWindow: {
							width: 400,
							height: 270,
							orientation: 1,
							offsetX: 0,
							offsetY: 0,
							hideToolBars: true,
							trackerPageConfigUrl: 'trackerConfig_p162095591-PL-PL.js'/*tpa=http://www.microsoft.com/library/svy/trackerConfig_p162095591-PL-PL.js*/
							// future feature: 
							//features: "location=0,menubar=0,resizable=1,scrollbars=1,toolbar=0"
						}
																				,Events: {
						beforeRender: function() {
														
													}
						,beforeDestroy: function() {
														
													}
						,beforeAccept: function() {
														
													}
						,beforeDecline: function() {
														
													}
						,beforeRenderUpdate: function() {
														
													}
						,afterRender: function() {
														
													}
					}
				}
						]
};
COMSCORE.SiteRecruit.Builder.run();
