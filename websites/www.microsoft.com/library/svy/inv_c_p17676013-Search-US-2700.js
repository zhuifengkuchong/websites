/*
Copyright (c) 2014, comScore Inc. All rights reserved.
version: 5.0.3
*/
COMSCORE.SiteRecruit.Builder.config = {
	version: "5.0.3",
	
	// invitations' settings
	invitation: [
							
											{ 	methodology: 2,
					projectId: 'p176726013',
					weight: 100,
					isRapidCookie: false,
					acceptUrl: 'https://survey2.securestudies.com/wix/p176726013.aspx',
					secureUrl: '',
					acceptParams: {
						raw: 'hsite=76&l=9&',
						siteCode: '2700',
												cookie: [
													],
												otherVariables: [
													]
					},
					viewUrl: 'http://b.scorecardresearch.com/b',
					viewParams: 'http://www.microsoft.com/library/svy/log=comscore/view/p176726013-view.log',
					content: '<table width="360" cellpadding="2" cellspacing="0" border="0" style="background:#FFFFFF; margin:0; display:table; border: 1px solid #CCCCCC;"><tr valign="top"> 	<td style="border:none; padding:2; margin:0" > 		<img src="logo-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/logo-stripe.gif*/ style="display:inline; margin-top:1px;" alt="Microsoft Logo"></td> 	<td style="border:none; padding:0; margin:0; text-align:right;" > 		<img border="0" onclick="@declineHandler" src="close.gif"/*tpa=http://www.microsoft.com/library/svy/close.gif*/ style="display:inline; cursor:pointer;" alt="Close button link" tabindex="1" /> 	</td></tr> 	<tr valign="top"> 	<td style="border:none; padding:0; margin:0" colspan="2"> 		<img src="top-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/top-stripe.gif*/ style="display:inline" /> 		<table width="360" cellpadding="5" style="margin:0; display:table"><tr><td style="width:340px;padding:6px;"> 			<div style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 11px; color: #000000; margin: 0 0 15px 0; line-height:14px;">Microsoft is conducting an online survey to understand your opinions of the Microsoft Search Web site. If you choose to participate, the online survey will be presented to you when you leave the Microsoft Search Web site.<br /> <br /> Would you like to participate?</div> 			<div align="center" style="margin: 0; padding: 0"> 				<input style="margin:0; padding:0; cursor:pointer;" type="button" value="  Yes  " onclick="@acceptHandler" tabindex="2" />&nbsp;&nbsp; 				<input style="margin:0; padding:0; cursor:pointer;"  type="button" value=" No " onclick="@declineHandler" tabindex="3" /> 			</div> 			<div style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 11px; color: #000000; margin: 15px 0 2px 0;"><a href="http://privacy.microsoft.com/en-us/default.mspx" target="_blank">Privacy Statement</a></div> 		</td></tr></table> 	</td></tr> 	<tr> <td style="border:none; padding:0; margin:0" colspan="2"> 		<img src="bottom-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/bottom-stripe.gif*/ style="display:inline" /> 	</td></tr> </table>',
					height: 210,
					width: 390,
					revealDelay: 1000,
					horizontalAlignment: 1,
					verticalAlignment: 1,
					horizontalMargin: 0,
					verticalMargin: 0,
					startingHorizontalAlignment: 1,
					startingVerticalAlignment: 0,
					startingHorizontalMargin: 0,
					startingVerticalMargin: 0,
					isHideBlockingElements: true,
					isAutoCentering: true,
					url: 'http://www.microsoft.com/library/svy/SiteRecruit_Tracker.htm',
					trackerGracePeriod: 7,
					usesSilverlight: false
					
					//silverlight config
										
											,trackerWindow: {
							width: 0,
							height: 0,
							orientation: 0,
							offsetX: 0,
							offsetY: 0,
							hideToolBars: false,
							trackerPageConfigUrl: 'trackerConfig_p17676013-Search-US.js'/*tpa=http://www.microsoft.com/library/svy/trackerConfig_p17676013-Search-US.js*/
							// future feature: 
							//features: "location=0,menubar=0,resizable=1,scrollbars=1,toolbar=0"
						}
																				,Events: {
						beforeRender: function() {
														var SR_url = window.location.toString(); //.toLowerCase();
var criteria_string = SR_url.substring(SR_url.indexOf("q=")+2,SR_url.length);
var _search = 'LocSearchExtract=' + criteria_string.replace(/\+/gi, " ");
var _l = readCookie("l");
var lck_search = "lock_search=" + _l;
COMSCORE.SiteRecruit.Builder.invitation.config.acceptParams.raw += _search + "&" + lck_search;

document.onkeyup = function(evt) {
    evt = evt || window.event;
    if (evt.keyCode == 27) {
        COMSCORE.SiteRecruit.Builder.invitation.decline(this);
    }
};
													}
						,beforeDestroy: function() {
														
													}
						,beforeAccept: function() {
														
													}
						,beforeDecline: function() {
														
													}
						,beforeRenderUpdate: function() {
														
													}
						,afterRender: function() {
														
													}
					}
				}
						]
};
COMSCORE.SiteRecruit.Builder.run();
