/*
Copyright (c) 2014, comScore Inc. All rights reserved.
version: 5.0.3
*/
COMSCORE.SiteRecruit.Builder.config = {
	version: "5.0.3",
	
	// invitations' settings
	invitation: [
							
											{ 	methodology: 2,
					projectId: 'p234872763',
					weight: 100,
					isRapidCookie: false,
					acceptUrl: 'http://survey2.surveysite.com/wix/p234872763.aspx',
					secureUrl: 'https://survey2.securestudies.com/wix/p234872763.aspx',
					acceptParams: {
						raw: 'l=9',
						siteCode: '1910',
												cookie: [
													],
												otherVariables: [
													]
					},
					viewUrl: 'http://web.survey-poll.com/tc/CreateLog.aspx',
					viewParams: 'http://www.microsoft.com/library/svy/log=comscore/view/p234872763-view.log',
					content: '<div id="srinvite" style="width:450px; border:1px solid #0073C6; border-top:8px solid #0073C6; margin:0; background-color:#FFFFFF; padding:0"> 	<div class="srtext" style="text-Align:left;margin:10px 60px 0px 50px; font-family:\'Segoe UI\'; font-size:18px; color:#666666; background-color:#FFFFFF;"> 		<img src="logo-stripe-short.gif"/*tpa=http://www.microsoft.com/library/svy/logo-stripe-short.gif*/ border="0"/> 	</div> 	<div class="srtext" style="position:relative; top:-42px; right:-30px;width:25px;"> 		<img src="close.gif"/*tpa=http://www.microsoft.com/library/svy/close.gif*/ style="border:0; margin-left:360px; padding:0;cursor:pointer;" onclick="@declineHandler"/>  	</div> 	<div class="srtext" style="margin:0px 50px; font-family:\'Segoe UI\'; font-size:14px; color:#0073C6; background-color:#FFFFFF;width:300px;text-align:left;"> 		We’re trying to understand how people use the Windows website so we can make it better.  If you don’t mind, we’d like to see which pages you visit today and then ask you a few questions when you leave the site. <br /><br /> Would you like to help? 	</div> 	<div style="margin:20px 30px 10px 40px;"> 		<table style="line-height:12px;font-family:\'Segoe UI\';font-size:12px;color:#0073C6;border:0;"> 			<tr><td style="width:100px;vertical-Align:bottom;border:0;"><a href="http://privacy.microsoft.com/en-us/default.aspx" target="_blank" style="font-size:12px; text-decoration:none;">Privacy Statement</a></td> 				<td width="225" align="center" style="border:0;"> 					<input type="button" style="border-style:none; background-color:#0073C6; color:#FFFFFF; font-size:14px; height:35px;width:100px; cursor:pointer;" value=" Yes " onclick="@acceptHandler;"/>	 &nbsp; 					<input type="button" style="border-style:none; background-color:#0073C6; color:#FFFFFF; font-size:14px; height:35px;width:100px; cursor:pointer;" value=" No " onclick="@declineHandler;"/>	<br /> 					</td></tr> 		</table> <br /> 	</div> </div>',
					height: 300,
					width: 450,
					revealDelay: 1000,
					horizontalAlignment: 1,
					verticalAlignment: 1,
					horizontalMargin: 0,
					verticalMargin: 0,
					startingHorizontalAlignment: 1,
					startingVerticalAlignment: 0,
					startingHorizontalMargin: 0,
					startingVerticalMargin: 0,
					isHideBlockingElements: false,
					isAutoCentering: true,
					url: 'http://www.microsoft.com/library/svy/SiteRecruit_Tracker.htm',
					trackerGracePeriod: 5,
					usesSilverlight: false
					
					//silverlight config
										
											,trackerWindow: {
							width: 400,
							height: 270,
							orientation: 1,
							offsetX: 0,
							offsetY: 0,
							hideToolBars: true,
							trackerPageConfigUrl: 'trackerConfig_p234872763.js'/*tpa=http://www.microsoft.com/library/svy/trackerConfig_p234872763.js*/
							// future feature: 
							//features: "location=0,menubar=0,resizable=1,scrollbars=1,toolbar=0"
						}
																				,Events: {
						beforeRender: function() {
														
													}
						,beforeDestroy: function() {
														
													}
						,beforeAccept: function() {
														
													}
						,beforeDecline: function() {
														
													}
						,beforeRenderUpdate: function() {
														
													}
						,afterRender: function() {
														
													}
					}
				}
						]
};
COMSCORE.SiteRecruit.Builder.run();
