/*
Copyright (c) 2013, comScore Inc. All rights reserved.
version: 5.0.3
*/
COMSCORE.SiteRecruit.Builder.config = {
	version: "5.0.3",
	
	// invitations' settings
	invitation: [
							
											{ 	methodology: 2,
					projectId: 'p225141998',
					weight: 100,
					isRapidCookie: false,
					acceptUrl: 'http://survey2.surveysite.com/wix/p225141998.aspx',
					secureUrl: '',
					acceptParams: {
						raw: 'l=17&mth=1',
						siteCode: '2824',
												cookie: [
													],
												otherVariables: [
													]
					},
					viewUrl: 'http://web.survey-poll.com/tc/CreateLog.aspx',
					viewParams: 'http://www.microsoft.com/library/svy/log=comscore/view/p225141998-view.log',					
					content: '<table width="360" cellpadding="3" cellspacing="0" border="0" style="background:#FFFFFF; margin:0; display:table"> <tr><td style="border:none; padding:0; margin:0" > 	<table width="360" cellpadding="1" cellspacing="0" border="0" bgcolor="#999999;" style="margin:0; padding:0; display:table"> 		<tr><td style="border:none; padding:0; margin:0" > 		<table width="360" cellpadding="0" cellspacing="0" border="0" style="background:#FFFFFF; margin:0; display:table"><tr valign="top"><td style="border:none; padding:0; margin:0" > 			<img src="logo-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/logo-stripe.gif*/ style="display:inline"><a href="javascript:void(0);" onclick="@declineHandler"><img border="0" src="close.gif"/*tpa=http://www.microsoft.com/library/svy/close.gif*/ style="display:inline" /></a><br /> 			<img src="top-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/top-stripe.gif*/ style="display:inline" /> 			<table width="360" cellpadding="5" style="margin:0; display:table"><tr><td style="width:340px;padding:6px;"> 				<div style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 11px; color: #000000; margin: 0 0 15px 0;">マイクロソフトは、Microsoft.com Web サイトが皆様のご期待に添うサイトであるために、ご意見を伺うオンラインアンケートを行っております。アンケートへのご協力を承諾いただけましたら、Microsoft.com Web サイトから退出する時点で質問が表示されます。<br /> アンケートにご協力いただけますか。</div>  <div align="center" style="margin: 0; padding: 0"> <input style="margin: 0; padding: 0" type="button" value="  はい  " onclick="@acceptHandler" />&nbsp;&nbsp; <input style="margin: 0; padding: 0"  type="button" value=" いいえ " onclick="@declineHandler" /> </div>  <div style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 11px; color: #000000; margin: 15px 0 2px 0;"><a href="http://privacy.microsoft.com/ja-jp/fullnotice.mspx" target="_blank">プライバシーに関する声明</a></div>  </td></tr></table> <img src="bottom-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/bottom-stripe.gif*/ style="display:inline" /></td></tr></table> </td></tr></table> </td></tr></table>   ',
					height: 210,
					width: 390,
					revealDelay: 1000,
					horizontalAlignment: 1,
					verticalAlignment: 1,
					horizontalMargin: 0,
					verticalMargin: 0,
					isHideBlockingElements: false,
					isAutoCentering: true,
					url: 'http://www.microsoft.com/library/svy/SiteRecruit_Tracker.htm',
					trackerGracePeriod: 5
					,usesSilverlight: false
					
					//silverlight config
										
											,trackerWindow: {
							width: 400,
							height: 270,
							orientation: 1,
							offsetX: 0,
							offsetY: 0,
							hideToolBars: true,
							trackerPageConfigUrl: 'trackerConfig_p225141998-JA-JP-server-cloud-WS.js'/*tpa=http://www.microsoft.com/library/svy/trackerConfig_p225141998-JA-JP-server-cloud-WS.js*/
							// future feature: 
							//features: "location=0,menubar=0,resizable=1,scrollbars=1,toolbar=0"
						}
																				,Events: {
						beforeRender: function() {
														
													}
						,beforeDestroy: function() {
														
													}
						,beforeAccept: function() {
														
													}
						,beforeDecline: function() {
														
													}
						,beforeRenderUpdate: function() {
														
													}
						,afterRender: function() {
														
													}
					}
				}
						]
};
COMSCORE.SiteRecruit.Builder.run();