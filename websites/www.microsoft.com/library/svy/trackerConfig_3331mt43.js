/*
Copyright (c) 2014, comScore Inc. All rights reserved.
version: 5.0.3
*/
COMSCORE.SiteRecruit.Tracker.config = {
	isAutoFocus: true,
	title: 'Microsoft Survey',
	content: '<!-- TOOLBAR_EXEMPT --> <style type="text/css">     body     {         margin: 2px;     }     body, td     {         font-family: Verdana, Arial, Helvetica, sans-serif;         font-size: 11px;         color: #000000;         background-color: #FFFFFF;     }     a:link, a:visited, a:active     {         text-decoration: underline;     }     a:hover     {         text-decoration: none;     }     .main     {         border: 1px solid #999999;     }     .main td     {     }     .main .content     {         padding: 15px;     }     </style>  <table width="100%" cellpadding="0px" cellspacing="0px" border="0px" class="main">     <tr>         <td><img src="logo-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/logo-stripe.gif*/ /></td>     </tr>     <tr>         <td background="top-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/top-stripe.gif*/ style="background-repeat: repeat-x;"><img src="top-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/top-stripe.gif*/ /></td>     </tr>     <tr>         <td class="content">             <p><div style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 12px; color: #000000;">Please do not close this window.<br /><br /> Thank you! The survey will appear here when you\'ve completed your visit, so <b>please do not close this window</b>.</div></p>             <p><div style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 12px; color: #000000;"><a href="http://survey.surveysite.com/wi/p61933108/privacy_p61933108.html" target="_blank">Privacy Statement</a></div></p>         </td>     </tr>     <tr>         <td background="bottom-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/bottom-stripe.gif*/ style="background-repeat: repeat-x;"><img src="bottom-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/bottom-stripe.gif*/ /></td>     </tr> </table>  ',	// content to inject in the new window
	match: '//[\\w\\.-]+/en-us/licensing',	// browserable regex match
	isCaptureTrail: false,
	gracePeriod: 5
};
COMSCORE.SiteRecruit.Tracker.start();