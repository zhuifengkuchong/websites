/*
Copyright (c) 2012, comScore Inc. All rights reserved.
version: 5.0.3
*/
var _halt=true;

function _set_SessionCookie(n, v){	
  var c = n+'=' + v  	    	
  				+ '; path=/'
   			 	+ '; domain=.microsoft.com';
	document.cookie = c;	
}

function readCookie(name){
  var ca = document.cookie.split(';');
  var nameEQ = name + "=";
   for(var i=0; i < ca.length; i++) {
    var c = ca[i];
     while (c.charAt(0)==' ') c = c.substring(1, c.length); //delete spaces
    if (c.indexOf(nameEQ) == 0) {
    	  	return c.substring(nameEQ.length, c.length);
    }
   }
  return null;   
}

function checkCookie(){
	var _count = readCookie("_nvisit");
	if(_count != null){ 
		_halt=false;
	}else{   
		_set_SessionCookie("_nvisit",1);
	}
}
checkCookie();	 	

COMSCORE.SiteRecruit.Broker.config = {
	version: "5.0.3",
	//TODO:Karl extend cookie enhancements to ie userdata
		testMode: false,
	
	// cookie settings
	cookie:{
		name: 'msresearch',
		path: '/',
		domain:  '.microsoft.com' ,
		duration: 90,
		rapidDuration: 0,
		expireDate: ''
	},
	
	// optional prefix for pagemapping's pageconfig file
	prefixUrl: "",
	
	//events
	Events: {
		beforeRecruit: function() {
					}
	},
	
		mapping:[
	// m=regex match, c=page config file (prefixed with configUrl), f=frequency
	{m: '//[\\w\\.-]+/learning/en-(?!((SyndicationPage)|(offers/virtualization-secampaign\\.aspx)))', c: 'inv_c_MSLearning42.js'/*tpa=http://www.microsoft.com/library/svy/inv_c_MSLearning42.js*/, f: 0.0934, p: 0, halt: false 	}
]
};
COMSCORE.SiteRecruit.Broker.run();