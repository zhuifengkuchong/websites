/*
Copyright (c) 2013, comScore Inc. All rights reserved.
version: 5.0.3
*/
COMSCORE.SiteRecruit.Builder.config = {
	version: "5.0.3",
	
	// invitations' settings
	invitation: [
							
											{ 	methodology: 2,
					projectId: 'p218292485',
					weight: 100,
					isRapidCookie: false,
					acceptUrl: 'http://survey2.surveysite.com/wix/p218292485.aspx',
					secureUrl: 'https://survey2.securestudies.com/wix/p218292485.aspx',
					acceptParams: {
						raw: 'hweight=4&',
						siteCode: '2198',
												cookie: [
													],
												otherVariables: [
													]
					},
					viewUrl: 'http://b.scorecardresearch.com/b',
					viewParams: 'http://www.microsoft.com/library/svy/log=comscore/view/p218292485-view.log',
					content: '<div style="width:366px; border: 1px solid #3b3b3b;background-color:#FFF;"> 	<div style="position:relative; top:12px; left:56px"> 		<div style="height:44px;font-family:\'Segoe UI Light\'; font-size:24px; color:#000;"> 			Please help us improve 			<div style="position:relative; top: -24pt; left:250px; width: 26; height: 26;"><a href="javascript:void(0);" onclick="@declineHandler"><img src="close_v3.gif"/*tpa=http://www.microsoft.com/library/svy/close_v3.gif*/ border="0"/></a></div> 		</div> 		<div style="font-family:\'Segoe UI\'; font-size:12px; color:#000; width:250px;line-height:14px;"> 			Microsoft is conducting an online survey to understand your opinions of the SQL Server Web site. If you choose to participate, the online survey will be presented to you when you leave the SQL Server Web site.<br /><br />Would you like to participate?  		</div> 		<div style="width:300px;height:38px;margin-top:28px;"> 		 			<div style="width:91px;" > 			       	<input name="button" type="button" value=" I want to " style="border-style:none; background-color:#000000; font-family:\'Segoe UI\'; font-size:12px; color:#FFF; height:34px;width:91px;" onclick="@acceptHandler;"/>     	</div> 	       	<div style="position:relative; top: -34px; left:147px; width:91px;height:34px;">     		<input name="button" type="button" value=" No thanks " style="border-style:none; background-color:#000000; font-family:\'Segoe UI\'; font-size:12px; color:#FFF; height:34px;width:91px;" onclick="@declineHandler;"/>	     	</div> 	  		   	</div>  		<div style="margin-top:10px;"><img src="microsoft-technet-logo.png"/*tpa=http://www.microsoft.com/library/svy/microsoft-technet-logo.png*/ style="display:inline">  			<div style="position:relative; top: -20px; left:167px;width:150px;"><a href="http://privacy.microsoft.com/en-us/default.mspx" style="font-family:\'Segoe UI\'; font-size:10px; color:#0072bc; text-decoration:none;" target="_blank">Privacy Statement</a></div> 		</div><br /> </div></div> ',
					height: 285,
					width: 390,
					revealDelay: 1000,
					horizontalAlignment: 1,
					verticalAlignment: 1,
					horizontalMargin: 80,
					verticalMargin: 200,
					startingHorizontalAlignment: 1,
					startingVerticalAlignment: 1,
					startingHorizontalMargin: 0,
					startingVerticalMargin: 0,
					isHideBlockingElements: false,
					isAutoCentering: true,
					url: 'http://www.microsoft.com/library/svy/SiteRecruit_Tracker.htm',
					trackerGracePeriod: 5,
					usesSilverlight: false
					
					//silverlight config
										
											,trackerWindow: {
							width: 400,
							height: 270,
							orientation: 1,
							offsetX: 0,
							offsetY: 0,
							hideToolBars: true,
							trackerPageConfigUrl: 'trackerConfig_p218292485-SQL.js'/*tpa=http://www.microsoft.com/library/svy/trackerConfig_p218292485-SQL.js*/
							// future feature: 
							//features: "location=0,menubar=0,resizable=1,scrollbars=1,toolbar=0"
						}
																				,Events: {
						beforeRender: function() {
														
													}
						,beforeDestroy: function() {
														
													}
						,beforeAccept: function() {
														
													}
						,beforeDecline: function() {
														
													}
						,beforeRenderUpdate: function() {
														
													}
						,afterRender: function() {
														
													}
					}
				}
						]
};
COMSCORE.SiteRecruit.Builder.run();