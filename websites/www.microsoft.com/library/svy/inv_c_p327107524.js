/*
Copyright (c) 2014, comScore Inc. All rights reserved.
version: 5.0.3
*/
COMSCORE.SiteRecruit.Builder.config = {
	version: "5.0.3",

	// invitations' settings
	invitation: [
		{
			methodology: 2,
			projectId: 'p327107524',
			weight: 100,
			isRapidCookie: false,
			acceptUrl: 'https://survey2.securestudies.com/wix/p327107524.aspx',
			secureUrl: 'https://survey2.securestudies.com/wix/p327107524.aspx',
			acceptParams: {
				raw: 'type=3&src=11',
				siteCode: '2853',
				cookie: [],
				otherVariables: []
			},
			viewUrl: 'http://b.scorecardresearch.com/b',
			viewParams: 'http://www.microsoft.com/library/svy/log=comscore/view/p327107524-view.log',
			content: '<table width="360" cellpadding="2" cellspacing="0" border="0" style="background:#FFFFFF; margin:0; display:table; border: 1px solid #CCCCCC;"><tr valign="top"> 	<td style="border:none; padding:2; margin:0" > 		<img src="logo-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/logo-stripe.gif*/ style="display:inline; margin-top:1px;"></td> 	<td style="border:none; padding:0; margin:0; text-align:right;" > 		<img border="0" onclick="@declineHandler" src="close.gif"/*tpa=http://www.microsoft.com/library/svy/close.gif*/ style="display:inline; cursor:pointer;" /> 	</td></tr> 	<tr valign="top"> 	<td style="border:none; padding:0; margin:0" colspan="2"> 		<img src="top-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/top-stripe.gif*/ style="display:inline" /> 		<table width="360" cellpadding="5" style="margin:0; display:table"><tr><td style="width:340px;padding:6px;"> 			<div style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 11px; color: #000000; margin: 0 0 15px 0; line-height:14px;">Microsoft is conducting an online survey to understand your opinions of the Windows for Business Web site. If you choose to participate, the online survey will be presented to you when you leave the Windows for Business Web site.  <br /><br /> Would you like to participate? </div> 			<div align="center" style="margin: 0; padding: 0"> 				<input style="margin:0; padding:0; cursor:pointer;" type="button" value="  Yes  " onclick="@acceptHandler" />&nbsp;&nbsp; 				<input style="margin:0; padding:0; cursor:pointer;"  type="button" value=" No " onclick="@declineHandler" /> 			</div> 			<div style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 11px; color: #000000; margin: 15px 0 2px 0;"><a href="https://survey2.securestudies.com/wi/p327107524/PRIVACY.htm" target="_blank">Privacy Statement</a></div> 		</td></tr></table> 	</td></tr> 	<tr> <td style="border:none; padding:0; margin:0" colspan="2"> 		<img src="bottom-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/bottom-stripe.gif*/ style="display:inline" /> 	</td></tr> </table>  ',
			height: 210,
			width: 390,
			revealDelay: 1000,
			horizontalAlignment: 1,
			verticalAlignment: 1,
			horizontalMargin: 80,
			verticalMargin: 200,
			isHideBlockingElements: false,
			isAutoCentering: true,
			url: 'http://www.microsoft.com/library/svy/SiteRecruit_Tracker.htm',
			trackerGracePeriod: 5
			,usesSilverlight: false
			//silverlight config
			,trackerWindow: {
				width: 400,
				height: 270,
				orientation: 1,
				offsetX: 0,
				offsetY: 0,
				hideToolBars: true,
				trackerPageConfigUrl: 'trackerConfig_p327107524.js'/*tpa=http://www.microsoft.com/library/svy/trackerConfig_p327107524.js*/
				// future feature: 
				//features: "location=0,menubar=0,resizable=1,scrollbars=1,toolbar=0"
			},
			
			Events: {
				beforeRender: function() {
														document.onkeyup = function(evt) {
    evt = evt || window.event;
    if (evt.keyCode == 27) {
        COMSCORE.SiteRecruit.Builder.invitation.decline(this);
    }
};					
					},
				beforeDestroy: function() {},
				beforeAccept: function() {},
				beforeDecline: function() {},
				beforeRenderUpdate: function() {},
				afterRender: function() {
					
					}
			}
		}
	]
};
COMSCORE.SiteRecruit.Builder.run();