/*
Copyright (c) 2014, comScore Inc. All rights reserved.
version: 5.0.3
*/
COMSCORE.SiteRecruit.Builder.config = {
	version: "5.0.3",
	
	// invitations' settings
	invitation: [
							
											{ 	methodology: 0,
					projectId: 'p296258930',
					weight: 100,
					isRapidCookie: false,
					acceptUrl: 'http://survey2.surveysite.com/wix/p296258930.aspx',
					secureUrl: '',
					acceptParams: {
						raw: 'SURVEY_TYPE=1&l=9',
						siteCode: '2761',
												cookie: [
													],
												otherVariables: [
													]
					},
					viewUrl: '//b.scorecardresearch.com/b',
					viewParams: 'c5=1',
					content: '<div id="srinvite" style="width:450px; border:1px solid #0073C6; border-top:8px solid #0073C6; margin:0; background-color:#FFFFFF; padding:0"> 	<div class="srtext" style="text-Align:left;margin:10px 60px 0px 50px; font-family:\'Segoe UI\'; font-size:18px; color:#666666; background-color:#FFFFFF;"> 		<img src="logo-stripe-short.gif"/*tpa=http://www.microsoft.com/library/svy/logo-stripe-short.gif*/ border="0"/> 	</div> 	<div class="srtext" style="position:relative; top:-42px; right:-30px;width:25px;"> </div> 	<div class="srtext" style="margin:10px 50px; font-family:\'Segoe UI\'; font-size:14px; color:#0073C6; background-color:#FFFFFF;width:300px;text-align:left;">Microsoft is conducting an online survey. <br /><br />Would you like to participate?</div> 	<div style="margin:14px 30px 10px 40px;"> 		<table style="line-height:12px;font-family:\'Segoe UI\';font-size:12px;color:#0073C6;border:0;"><tr><td style="vertical-Align:bottom;border:0;"><input type="button" style="border-style:none; background-color:#0073C6; color:#FFFFFF; font-size:14px; height:35px;width:100px;cursor:pointer;" value=" Yes " onclick="fSubmit();"/>	 &nbsp; <a href="javascript:void(0);" onClick="go_back();" style="font-family:\'Segoe UI\'; font-size:13px; color:#0073C6;"> No, go back to the Microsoft website </a></td></tr><tr><td style="vertical-Align:bottom;border:0;"><br /><a href="http://privacy.microsoft.com/en-us/default.mspx" target="_blank" style="font-family:\'Segoe UI\';font-size:12px;color:#0073C6;">Privacy Statement</a></td></tr> 		</table> <br /> 	</div> </div>',
					height: 310,
					width: 450,
					revealDelay: 1000,
					horizontalAlignment: 1,
					verticalAlignment: 0,
					horizontalMargin: 0,
					verticalMargin: 10,
					startingHorizontalAlignment: 1,
					startingVerticalAlignment: 0,
					startingHorizontalMargin: 0,
					startingVerticalMargin: 0,
					isHideBlockingElements: false,
					isAutoCentering: true,
					url: 'http://www.microsoft.com/library/svy/SiteRecruit_Tracker.htm',
					trackerGracePeriod: 7,
					usesSilverlight: false
					
					//silverlight config
										
											,trackerWindow: {
							width: 400,
							height: 270,
							orientation: 1,
							offsetX: 0,
							offsetY: 0,
							hideToolBars: true,
							trackerPageConfigUrl: 'trackerConfig_p296258930-US-Surface.js'/*tpa=http://www.microsoft.com/library/svy/trackerConfig_p296258930-US-Surface.js*/
							// future feature: 
							//features: "location=0,menubar=0,resizable=1,scrollbars=1,toolbar=0"
						}
																				,Events: {
						beforeRender: function() {
														document.onkeyup = function(evt) {
    evt = evt || window.event;
    if (evt.keyCode == 27) {
        COMSCORE.SiteRecruit.Builder.invitation.decline(this);
    }
};
													}
						,beforeDestroy: function() {
														
													}
						,beforeAccept: function() {
														
													}
						,beforeDecline: function() {
														
													}
						,beforeRenderUpdate: function() {
														
													}
						,afterRender: function() {
														
													}
					}
				}
						]
};
COMSCORE.SiteRecruit.Builder.run();
