/*
Copyright (c) 2013, comScore Inc. All rights reserved.
version: 5.0.3
*/
COMSCORE.SiteRecruit.Builder.config = {
	version: "5.0.3",
	
	// invitations' settings
	invitation: [
							
			{ 	methodology: 0,
					projectId: 'p3069121557',
					weight: 100,
					isRapidCookie: false,
					acceptUrl: 'http://survey.confirmit.com/wix/p3069121557.aspx',
					secureUrl: 'https://survey.confirmit.com/wix/p3069121557.aspx',
					acceptParams: {
						raw: 'type=3',
						siteCode: '1255',
												cookie: [
													],
												otherVariables: [
													]
					},
					viewUrl: 'http://b.scorecardresearch.com/b',
					viewParams: 'http://www.microsoft.com/library/svy/log=comscore/view/mshp7-view.log',
					content: '<div id="srinvite" style="width:450px; border:1px solid #4568C4; margin:0; background-color:#4568C4; padding:0;"> 	 	<div class="srtop" style="background:#4568C4; margin:0; padding:0; vertical-align:top;"> 		<img src="ms-logo_bL_r-100.png"/*tpa=http://www.microsoft.com/library/svy/ms-logo_bL_r-100.png*/ style="margin:10px" /> 	<img src="close-x.png"/*tpa=http://www.microsoft.com/library/svy/close-x.png*/ style="border:0; margin-left:250px; padding:0;cursor:pointer;" onclick="@declineHandler"/> 	</div> 	 	<h3 style="color:#FFF; font-family:segoe ui; font-size: 25px; font-weight:normal; margin:10px 30px"></h3> 	 	<div class="srtext" style="margin:10px 30px; font-family:segoe ui; font-size:14px; color:#FFFFFF; background-color:#4568C4;">Microsoft is conducting a short survey to better understand your needs of the Microsoft.com Home Page for mobile devices. <br />Would you be willing to participate?  </div> 	 	<div style="margin:20px 30px"><a href="http://privacy.microsoft.com/en-us/default.mspx" style="font-family:segoe ui; font-size:13px; color:#FFFFFF;" target="_blank">Privacy Statement</a><input type="button" style="border-style:none; margin:10px 0px 20px 20px; background-color:#FF8B00; color:#FFFFFF; font-size:14px; height:35px;width:100px;cursor:pointer;" value=" YES " onclick="@acceptHandler;"/><input name="button" type="button" style="border-style:none; margin:10px 0px 20px 20px; background-color:#FF8B00; color:#FFFFFF; font-size:14px; height:35px;width:100px;cursor:pointer;" onclick="@declineHandler;" value=" NO "/></div>		 	</div>',
					height: 450,
					width: 450,
					revealDelay: 1000,
					horizontalAlignment: 0,
					verticalAlignment: 0,
					horizontalMargin: 4,
					verticalMargin: 4,
					isHideBlockingElements: true,
					isAutoCentering: true,
					url: 'http://www.microsoft.com/library/svy/SiteRecruit_Tracker-MPE.htm',
					trackerGracePeriod: 5
					,usesSilverlight: false
					
					//silverlight config
																				,Events: {
						beforeRender: function() {		
		
							var screenWidth = self.screen.availWidth, screenHeight = self.screen.availHeight;
							//var screenWidth = self.screen.width, screenHeight = self.screen.Height;
							if(screenWidth <=770){
								_w = 240; _h = 300;
								_contstr = '<div id="srinvite" style="width:'+_w+'px; border:1px solid #4568C4; margin:0; background-color:#4568C4; padding:0;"> 	 	<div class="srtop" style="background:#4568C4; margin:0; padding:0; vertical-align:top;"><span class="srtop" style="background:#4568C4; margin:0; padding:0; vertical-align:top;"><img src="ms-logo_bL_r-100.png"/*tpa=http://www.microsoft.com/library/svy/ms-logo_bL_r-100.png*/ style="margin:10px" /></span><a href="Close" onclick="@declineHandler" style="border:none;"><img src="close-x.png"/*tpa=http://www.microsoft.com/library/svy/close-x.png*/ style="border:0; margin-left:40px; padding:0;"/></a> 	</div> 	  	<div class="srtext" style="margin:10px 10px; font-family:segoe ui; font-size:14px; color:#FFFFFF; background-color:#4568C4;">Microsoft is conducting a short survey to better understand your needs of the Microsoft.com Home Page for mobile devices. <br />Would you be willing to participate?  </div> 	 	<div style="margin:4px"><input type="button" style="border-style:none; margin:4px; background-color:#FF8B00; color:#FFFFFF; font-size:14px; height:30px;width:60px;" value=" YES " onclick="@acceptHandler;"/><input name="button" type="button" style="border-style:none; margin:10px; background-color:#FF8B00; color:#FFFFFF; font-size:14px; height:30px;width:60px;" onclick="@declineHandler;" value=" NO "/><br /><a href="http://www.microsoft.com/privacystatement/en-us/core/default.aspx" style="font-family:segoe ui; font-size:13px; color:#FFFFFF;" target="_blank">Privacy Statement</a></div>	</div>';
								COMSCORE.SiteRecruit.Builder.invitation.config.content = _contstr;
								COMSCORE.SiteRecruit.Builder.invitation.config.width = _w;
								COMSCORE.SiteRecruit.Builder.invitation.config.height = _h;
								
							}
	
													}
						,beforeDestroy: function() {
														
													}
						,beforeAccept: function() {
													}
						,beforeDecline: function() {
														
													}
						,beforeRenderUpdate: function() {
														
													}
						,afterRender: function() {
														
													}
					}
				}
						]
};
COMSCORE.SiteRecruit.Builder.run();
