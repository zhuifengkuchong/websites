/*
Copyright (c) 2012, comScore Inc. All rights reserved.
version: 5.0.3
*/
COMSCORE.SiteRecruit.Builder.config = {
	version: "5.0.3",
	
	// invitations' settings
	invitation: [
							
											{ 	methodology: 0,
					projectId: '3331mt2',
					weight: 100,
					isRapidCookie: false,
					acceptUrl: 'http://survey2.surveysite.com/wix/p15394611.aspx',
					secureUrl: '',
					acceptParams: {
						raw: 'hsite=2',
						siteCode: '2',
												cookie: [
													],
												otherVariables: [
													]
					},
					viewUrl: 'http://web.survey-poll.com/tc/CreateLog.aspx',
					viewParams: 'http://www.microsoft.com/library/svy/log=comscore/view/p15394611-view.log',
					content: '<table width="360" cellpadding="3" cellspacing="0" border="0" style="background:#FFFFFF"><tr><td> <table width="100%" cellpadding="1" cellspacing="0" border="0" bgcolor="#999999"><tr><td> <table width="100%" cellpadding="0" cellspacing="0" border="0" style="background:#FFFFFF"><tr valign="top"><td> <img src="logo-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/logo-stripe.gif*/ /><a href="Close" onclick="@declineHandler"><img border="0" src="close.gif"/*tpa=http://www.microsoft.com/library/svy/close.gif*/ /></a><br /> <img src="top-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/top-stripe.gif*/ /> <table width="100%" cellpadding="5"><tr><td>  <div style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 11px; color: #000000; margin: 0 0 15px 0;">Microsoft is conducting an online survey and is interested in your opinions. Would you like to participate? </div>  <div align="center" style="margin: 0; padding: 0"> <input style="margin: 0; padding: 0" type="button" value="  Yes  " onclick="@acceptHandler" />&nbsp;&nbsp; <input style="margin: 0; padding: 0"  type="button" value=" No " onclick="@declineHandler" /> </div>  <div style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 11px; color: #000000; margin: 15px 0 2px 0;">Review the <a href="http://survey2.surveysite.com/wi/p15394611/privacystatement_2.htm" target="_blank">Privacy Statement</a> for this survey.</div>  </td></tr></table> <img src="bottom-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/bottom-stripe.gif*/ /></td></tr></table> </td></tr></table> </td></tr></table>   ',
					height: 210,
					width: 390,
					revealDelay: 1000,
					horizontalAlignment: 1,
					verticalAlignment: 1,
					horizontalMargin: 0,
					verticalMargin: 0,
					isHideBlockingElements: true,
					isAutoCentering: true,
					url: 'http://www.microsoft.com/library/svy/SiteRecruit_Tracker.htm',
					trackerGracePeriod: 3
					,usesSilverlight: false
					
					//silverlight config
										
																				,Events: {
						beforeRender: function() {
														
													}
						,beforeDestroy: function() {
														
													}
						,beforeAccept: function() {
														
													}
						,beforeDecline: function() {
														
													}
						,beforeRenderUpdate: function() {
														
													}
						,afterRender: function() {
														
													}
					}
				}
						]
};
COMSCORE.SiteRecruit.Builder.run();
