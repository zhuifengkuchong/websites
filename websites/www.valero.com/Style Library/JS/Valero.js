/* Object Oriented Approach in Javascript */         

function InformationTiles(title, popUpHeader, popUpBody,redirectUrl,imageUrl) {        
	this.title = title;
    this.popUpHeader = popUpHeader;
    this.popUpBody = popUpBody;
    this.redirectUrl = redirectUrl;
    this.imageUrl = imageUrl;
}

InformationTiles.prototype = {
    getTitle: function() {
        return this.title;
    },        
    getPopUpBody: function() {
        return this.popUpBody;
    },
    getRedirectUrl: function(){
        return this.redirectUrl;
    },
    getImageUrl: function() {
        return this.imageUrl;
    },
    getPopUpHeader: function() {
        return this.popUpHeader;
    }
}

InformationTiles.prototype.renderContent = function() {
    var content = new Sys.StringBuilder();
    content.append(String.format("<a href='{0}'><img alt='{1}' src='{2}' /></a>", this.getRedirectUrl(),this.getTitle(), this.getImageUrl()));
    content.append(String.format("<span>{0}</span>", this.getTitle()));
    return content.toString();
    //return '<a href="'+ this.getRedirectUrl() + '"><img src="' + this.getImageUrl() + '" /></a><br /><span>' + this.getTitle() + '</span>';
}

function ConstituentTiles(title, highlightbuttontext, hideselectionbuttontext, redirectUrl, imageUrl, imagehoverUrl) 
{
    this.title = title;
    this.hideselectionbuttontext= hideselectionbuttontext;
    this.highlightbuttontext= highlightbuttontext;
    this.redirectUrl = redirectUrl;
    this.imageUrl = imageUrl;
    this.imagehoverUrl= imagehoverUrl;
}

ConstituentTiles.prototype = {

    getTitle: function() {
        return this.title;
    },
    getHideSelectionButtonText: function() {
        return this.hideselectionbuttontext;
    },
    getHighlightButtonText: function() {
        return this.highlightbuttontext;
    },
    getRedirectUrl: function() {
        return this.redirectUrl;
    },
    getImageUrl: function() {
        return this.imageUrl;
    },
    getImagehoverUrl: function() {
        return this.imagehoverUrl;
    }
}

ConstituentTiles.prototype.renderContent = function() {
    var content = new Sys.StringBuilder();
    content.append(String.format("<a href='{0}'><img alt='{1}' src='{2}' /></a>", this.getRedirectUrl(), this.getTitle(), this.getImageUrl()));
//    content.append(String.format("<span>{0}</span>", this.getTitle()));
    return content.toString();
    //return '<a href="' + this.getRedirectUrl() + '"><img src="' + this.getImageUrl() + '" /></a><br /><span>' + this.getTitle() + '</span>';
}

var imageToggledown = "url(../Style%20Library/Images/ToggleDown.jpg)";
var imageToggleUp = "url(../Style%20Library/Images/ToggleUp.jpg)";


function changeLink(obj,URL)
{
	//alert(URL);
	var imgURL = URL.split(",");
    document.getElementById(obj).href=imgURL[0];
    //document.getElementById(obj).target="_blank";
}

function SetTriggerPanelImage(panel, img)
{
	$get(panel).style.backgroundImage = img;
}

function ChangeObject(obj,value) 
{
	$get(obj).innerHTML = value;
}
               
function SetImage(panel, img) 
{
	var infoimage = img.split("\"");
 	$get(panel).src= ltrim(infoimage[5],"//");
}

function trim(str, chars) {
return ltrim(rtrim(str, chars), chars);
}

function ltrim(str, chars) {
chars = chars || "\\s";
return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
}

function rtrim(str, chars) {
chars = chars || "\\s";
return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
}

var ResultantDiv;
// This function calls the Web Service method.
function GetDropDownSelected(obj, group, dropdownName, divresult) {

	ResultantDiv = divresult;
    var selectedText = obj.options[obj.options.selectedIndex].text; 
    Valero.Service.GetContacts(selectedText, group,dropdownName, SucceededCallback);
}

// This is the callback function that
// processes the Web Service return value.
function SucceededCallback(e) {
    //var obj = Sys.Serialization.JavaScriptSerializer.deserialize(e, false);
    $get(ResultantDiv).innerHTML = e;
}


// This fucntion redirects the user when selecting from the site navigator
function SiteNavigatorRedirect(obj){
      if(obj.options.selectedIndex > 0)
      document.location.href=obj.options[obj.options.selectedIndex].value;
      }

// for email component
 function ShowEmailForm(emailTo,EmailControl,popupextender)
{		
	if(emailTo == 'emailthispage')
	{
		//$get(button.id).click();
		$get('divEmailThisPage').style.display = "block";	
		$find(popupextender).show();						
	}
	else
	{
		EmailControl.value = emailTo;
	 	//$get(button.id).click();
	 	$get('divWebparts').style.display = "block";
	 	$find(popupextender).show();											 	
 	}
}

// this function clears the value from text box on focus
function ClearTextBox(obj)											       
{
	if(obj.value=="Enter city & state or zip")
	{
	obj.value="";
	}
}
// this function swaps images on the constituent tiles
function newImage(arg) {
    if (document.images) {
        rslt = new Image();
        rslt.src = arg;
        return rslt;
    }
}

function changeImages() {
	var infoPanel = $get(changeImages.arguments[0].substr(0,changeImages.arguments[0].lastIndexOf('_')+1)+'Info');
    if (document.images && (preloadFlag == true)) {
        if (infoPanel  != null){
            $get(changeImages.arguments[0]).src = changeImages.arguments[1].src;                    }
    }
}
function changeImagesOnToggle() {
	var infoPanel = $get(changeImagesOnToggle.arguments[0].substr(0,changeImagesOnToggle.arguments[0].lastIndexOf('_')+1)+'Info');
	var	infoPanelGutter = $get(changeImagesOnToggle.arguments[0].substr(0,changeImagesOnToggle.arguments[0].lastIndexOf('_')+1)+'Gutter');
	infoPanelGutter.style.display = (infoPanel.style.display=='block')? 'none':'block' ;
	var image = $get(changeImagesOnToggle.arguments[0].substr(0,changeImagesOnToggle.arguments[0].lastIndexOf('_')+1)+'Image');
    if (document.images && (preloadFlag == true)) {
        if (infoPanel  != null && infoPanel.style.display == 'none')
            image.src = changeImagesOnToggle.arguments[2];            
        else
            image.src = changeImagesOnToggle.arguments[1];
    }
}

var preloadFlag = true;
function preloadImages() {
    if (document.images) {
        tile_01_over = newImage(tile01_ImageOn);
        tile_02_over = newImage(tile02_ImageOn);
        tile_03_over = newImage(tile03_ImageOn);
        tile_04_over = newImage(tile04_ImageOn);
        preloadFlag = true;
    }
}

function movedown() 
{ 
  window.location = 'default.aspx#slider';
}

function ProcessDefaultOnLoad(onLoadFunctionNames)
	{
		//** Uncomment this to see when this runs		
		ProcessPNGImages();
		UpdateAccessibilityUI();
		
		//** We comment out the offending ootb function
		//** and leave the rest of the functions as they were
		//ProcessImn();
		for (var i=0; i < onLoadFunctionNames.length; i++)
		{
			var expr="if(typeof("+onLoadFunctionNames[i]+")=='function'){"+onLoadFunctionNames[i]+"();}";
			eval(expr);
		}
		if (typeof(_spUseDefaultFocus)!="undefined")
			DefaultFocus();
	}	
