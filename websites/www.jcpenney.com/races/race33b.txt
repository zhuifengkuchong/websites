<script id = "race33b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race33={};
	myVars.races.race33.varName="first_department__onmouseout";
	myVars.races.race33.varType="@varType@";
	myVars.races.race33.repairType = "@RepairType";
	myVars.races.race33.event1={};
	myVars.races.race33.event2={};
	myVars.races.race33.event1.id = "first_department";
	myVars.races.race33.event1.type = "onmouseout";
	myVars.races.race33.event1.loc = "first_department_LOC";
	myVars.races.race33.event1.isRead = "True";
	myVars.races.race33.event1.eventType = "@event1EventType@";
	myVars.races.race33.event2.id = "Lu_DOM";
	myVars.races.race33.event2.type = "onDOMContentLoaded";
	myVars.races.race33.event2.loc = "Lu_DOM_LOC";
	myVars.races.race33.event2.isRead = "False";
	myVars.races.race33.event2.eventType = "@event2EventType@";
	myVars.races.race33.event1.executed= false;// true to disable, false to enable
	myVars.races.race33.event2.executed= false;// true to disable, false to enable
</script>

