<script id = "race63a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race63={};
	myVars.races.race63.varName="shopping_bag__onmousemove";
	myVars.races.race63.varType="@varType@";
	myVars.races.race63.repairType = "@RepairType";
	myVars.races.race63.event1={};
	myVars.races.race63.event2={};
	myVars.races.race63.event1.id = "Lu_DOM";
	myVars.races.race63.event1.type = "onDOMContentLoaded";
	myVars.races.race63.event1.loc = "Lu_DOM_LOC";
	myVars.races.race63.event1.isRead = "False";
	myVars.races.race63.event1.eventType = "@event1EventType@";
	myVars.races.race63.event2.id = "shopping_bag";
	myVars.races.race63.event2.type = "onmousemove";
	myVars.races.race63.event2.loc = "shopping_bag_LOC";
	myVars.races.race63.event2.isRead = "True";
	myVars.races.race63.event2.eventType = "@event2EventType@";
	myVars.races.race63.event1.executed= false;// true to disable, false to enable
	myVars.races.race63.event2.executed= false;// true to disable, false to enable
</script>

