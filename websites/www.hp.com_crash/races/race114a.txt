<script id = "race114a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race114={};
	myVars.races.race114.varName="Lu_Id_a_199__onkeypress";
	myVars.races.race114.varType="@varType@";
	myVars.races.race114.repairType = "@RepairType";
	myVars.races.race114.event1={};
	myVars.races.race114.event2={};
	myVars.races.race114.event1.id = "Lu_DOM";
	myVars.races.race114.event1.type = "onDOMContentLoaded";
	myVars.races.race114.event1.loc = "Lu_DOM_LOC";
	myVars.races.race114.event1.isRead = "False";
	myVars.races.race114.event1.eventType = "@event1EventType@";
	myVars.races.race114.event2.id = "Lu_Id_a_199";
	myVars.races.race114.event2.type = "onkeypress";
	myVars.races.race114.event2.loc = "Lu_Id_a_199_LOC";
	myVars.races.race114.event2.isRead = "True";
	myVars.races.race114.event2.eventType = "@event2EventType@";
	myVars.races.race114.event1.executed= false;// true to disable, false to enable
	myVars.races.race114.event2.executed= false;// true to disable, false to enable
</script>

