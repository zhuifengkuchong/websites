<script id = "race110a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race110={};
	myVars.races.race110.varName="Lu_Id_a_252__onkeydown";
	myVars.races.race110.varType="@varType@";
	myVars.races.race110.repairType = "@RepairType";
	myVars.races.race110.event1={};
	myVars.races.race110.event2={};
	myVars.races.race110.event1.id = "Lu_DOM";
	myVars.races.race110.event1.type = "onDOMContentLoaded";
	myVars.races.race110.event1.loc = "Lu_DOM_LOC";
	myVars.races.race110.event1.isRead = "False";
	myVars.races.race110.event1.eventType = "@event1EventType@";
	myVars.races.race110.event2.id = "Lu_Id_a_252";
	myVars.races.race110.event2.type = "onkeydown";
	myVars.races.race110.event2.loc = "Lu_Id_a_252_LOC";
	myVars.races.race110.event2.isRead = "True";
	myVars.races.race110.event2.eventType = "@event2EventType@";
	myVars.races.race110.event1.executed= false;// true to disable, false to enable
	myVars.races.race110.event2.executed= false;// true to disable, false to enable
</script>

