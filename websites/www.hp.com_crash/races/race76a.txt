<script id = "race76a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race76={};
	myVars.races.race76.varName="Lu_Id_ul_4__onkeydown";
	myVars.races.race76.varType="@varType@";
	myVars.races.race76.repairType = "@RepairType";
	myVars.races.race76.event1={};
	myVars.races.race76.event2={};
	myVars.races.race76.event1.id = "Lu_DOM";
	myVars.races.race76.event1.type = "onDOMContentLoaded";
	myVars.races.race76.event1.loc = "Lu_DOM_LOC";
	myVars.races.race76.event1.isRead = "False";
	myVars.races.race76.event1.eventType = "@event1EventType@";
	myVars.races.race76.event2.id = "Lu_Id_a_76";
	myVars.races.race76.event2.type = "onkeydown";
	myVars.races.race76.event2.loc = "Lu_Id_a_76_LOC";
	myVars.races.race76.event2.isRead = "True";
	myVars.races.race76.event2.eventType = "@event2EventType@";
	myVars.races.race76.event1.executed= false;// true to disable, false to enable
	myVars.races.race76.event2.executed= false;// true to disable, false to enable
</script>

