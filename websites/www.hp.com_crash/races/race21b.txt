<script id = "race21b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race21={};
	myVars.races.race21.varName="Lu_Id_ul_4__onkeydown";
	myVars.races.race21.varType="@varType@";
	myVars.races.race21.repairType = "@RepairType";
	myVars.races.race21.event1={};
	myVars.races.race21.event2={};
	myVars.races.race21.event1.id = "Lu_Id_a_21";
	myVars.races.race21.event1.type = "onkeydown";
	myVars.races.race21.event1.loc = "Lu_Id_a_21_LOC";
	myVars.races.race21.event1.isRead = "True";
	myVars.races.race21.event1.eventType = "@event1EventType@";
	myVars.races.race21.event2.id = "Lu_DOM";
	myVars.races.race21.event2.type = "onDOMContentLoaded";
	myVars.races.race21.event2.loc = "Lu_DOM_LOC";
	myVars.races.race21.event2.isRead = "False";
	myVars.races.race21.event2.eventType = "@event2EventType@";
	myVars.races.race21.event1.executed= false;// true to disable, false to enable
	myVars.races.race21.event2.executed= false;// true to disable, false to enable
</script>

