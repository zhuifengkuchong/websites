<script id = "race128b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race128={};
	myVars.races.race128.varName="Object[158219].caller";
	myVars.races.race128.varType="@varType@";
	myVars.races.race128.repairType = "@RepairType";
	myVars.races.race128.event1={};
	myVars.races.race128.event2={};
	myVars.races.race128.event1.id = "Lu_Id_a_252";
	myVars.races.race128.event1.type = "onkeydown";
	myVars.races.race128.event1.loc = "Lu_Id_a_252_LOC";
	myVars.races.race128.event1.isRead = "True";
	myVars.races.race128.event1.eventType = "@event1EventType@";
	myVars.races.race128.event2.id = "Lu_Id_div_81";
	myVars.races.race128.event2.type = "onkeydown";
	myVars.races.race128.event2.loc = "Lu_Id_div_81_LOC";
	myVars.races.race128.event2.isRead = "False";
	myVars.races.race128.event2.eventType = "@event2EventType@";
	myVars.races.race128.event1.executed= false;// true to disable, false to enable
	myVars.races.race128.event2.executed= false;// true to disable, false to enable
</script>

