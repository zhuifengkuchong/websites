<script id = "race129a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race129={};
	myVars.races.race129.varName="Object[7420].found";
	myVars.races.race129.varType="@varType@";
	myVars.races.race129.repairType = "@RepairType";
	myVars.races.race129.event1={};
	myVars.races.race129.event2={};
	myVars.races.race129.event1.id = "Lu_window";
	myVars.races.race129.event1.type = "onload";
	myVars.races.race129.event1.loc = "Lu_window_LOC";
	myVars.races.race129.event1.isRead = "False";
	myVars.races.race129.event1.eventType = "@event1EventType@";
	myVars.races.race129.event2.id = "Lu_Id_a_99";
	myVars.races.race129.event2.type = "onfocus";
	myVars.races.race129.event2.loc = "Lu_Id_a_99_LOC";
	myVars.races.race129.event2.isRead = "False";
	myVars.races.race129.event2.eventType = "@event2EventType@";
	myVars.races.race129.event1.executed= false;// true to disable, false to enable
	myVars.races.race129.event2.executed= false;// true to disable, false to enable
</script>

