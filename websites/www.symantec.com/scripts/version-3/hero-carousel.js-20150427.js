var heroCarousel = function(config){
	function createCarousel() {
    $('.heroCarousel').jcarousel({
        'wrap': 'circular',
				'animation': 'slow'
    }).jcarouselAutoscroll({
        'target': '+=1',
				'interval': config.autoRotationDelay
    });
		$(window).load(function(){
			createPagination();
		});
		stopRotation();
		hoverClickCarousel();
		threatconCarousel();
	}
	function createPagination() {
		highlightPagination();
		$('.heroCarousel-pagination').jcarouselPagination({
			'carousel': $('.heroCarousel'),
			'item': function(page, carouselItems) {
					return '<a href="#" class="heroCarouselPaginationItem"></a>';
			}
		});
		 $('[data-jcarousel]').each(function() {
	      var el = $(this);
      el.jcarousel(el.data());
	    });
	 
    $('[data-jcarousel-control]').each(function() {
	      var el = $(this);
	      el.jcarouselControl(el.data());
	    });
	}
	
	function hoverClickCarousel(){
		var clicked = false;
		$('.heroCarouselList').find("a").on("click", function(event){
			$('.heroCarousel').jcarouselAutoscroll('stop');
			clicked = true;
		});
		$('.heroCarousel').hover(function() {
				$(this).jcarouselAutoscroll('stop');
				$('.carousel-control-prev').show();
				$('.carousel-control-next').show();
			}, 
			function() {
				if(!clicked){
					$(this).jcarouselAutoscroll('start');
				}
				$('.carousel-control-prev').hide();
				$('.carousel-control-next').hide();
				
		});
	}
	
	function highlightPagination(){
		$('.heroCarousel-pagination').on('jcarouselpagination:active', 'a', function() {
                $(this).addClass('active');
            })
            .on('jcarouselpagination:inactive', 'a', function() {
                $(this).removeClass('active');
            });
	}
	function stopRotation(){
		var carouselCycles = 1,
		carouselItems = $('.heroCarousel').jcarousel('items').size();
		$('.heroCarousel').delegate('li', 'itemtargetin.jcarousel', function(event, carousel) {
			carouselCycles = carouselCycles + 1;
			if (carouselCycles == ((config.autoRotationCycles * carouselItems)+1)){
				$('.heroCarousel').jcarouselAutoscroll('stop');
				carouselCycles = 1;
			}
		});	
	}

	function threatconCarousel(){
		if($('.divLong[style*="block"]')) {	
				$('.heroCarousel').jcarouselAutoscroll('stop');
			}else{
					$('.heroCarousel').jcarouselAutoscroll('start');
				}
	}




	createCarousel(config);	
};
