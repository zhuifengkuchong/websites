$(document).ready(function() {
  $(".boxMax").click(function() {
    $(".divLong").hide();
    $(".divShort").show();
  });

  $(".boxMin").click(function() {
    $(".divShort").hide();
    $(".divLong").show();
  });

  setTimeout(function() {
      $(".divLong").hide();
      $(".divShort").show();
      $('.heroCarousel').jcarouselAutoscroll('start');
    }, 5000);
});