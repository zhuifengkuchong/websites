<script id = "race45a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race45={};
	myVars.races.race45.varName="Lu_Id_li_21__onmouseover";
	myVars.races.race45.varType="@varType@";
	myVars.races.race45.repairType = "@RepairType";
	myVars.races.race45.event1={};
	myVars.races.race45.event2={};
	myVars.races.race45.event1.id = "Lu_DOM";
	myVars.races.race45.event1.type = "onDOMContentLoaded";
	myVars.races.race45.event1.loc = "Lu_DOM_LOC";
	myVars.races.race45.event1.isRead = "False";
	myVars.races.race45.event1.eventType = "@event1EventType@";
	myVars.races.race45.event2.id = "Lu_Id_li_63";
	myVars.races.race45.event2.type = "onmouseover";
	myVars.races.race45.event2.loc = "Lu_Id_li_63_LOC";
	myVars.races.race45.event2.isRead = "True";
	myVars.races.race45.event2.eventType = "@event2EventType@";
	myVars.races.race45.event1.executed= false;// true to disable, false to enable
	myVars.races.race45.event2.executed= false;// true to disable, false to enable
</script>

