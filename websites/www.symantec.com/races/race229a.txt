<script id = "race229a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race229={};
	myVars.races.race229.varName="Function[17354].guid";
	myVars.races.race229.varType="@varType@";
	myVars.races.race229.repairType = "@RepairType";
	myVars.races.race229.event1={};
	myVars.races.race229.event2={};
	myVars.races.race229.event1.id = "Lu_Id_li_21";
	myVars.races.race229.event1.type = "onmouseover";
	myVars.races.race229.event1.loc = "Lu_Id_li_21_LOC";
	myVars.races.race229.event1.isRead = "False";
	myVars.races.race229.event1.eventType = "@event1EventType@";
	myVars.races.race229.event2.id = "Lu_Id_li_121";
	myVars.races.race229.event2.type = "onmouseover";
	myVars.races.race229.event2.loc = "Lu_Id_li_121_LOC";
	myVars.races.race229.event2.isRead = "True";
	myVars.races.race229.event2.eventType = "@event2EventType@";
	myVars.races.race229.event1.executed= false;// true to disable, false to enable
	myVars.races.race229.event2.executed= false;// true to disable, false to enable
</script>

