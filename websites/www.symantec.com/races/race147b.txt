<script id = "race147b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race147={};
	myVars.races.race147.varName="Function[17354].guid";
	myVars.races.race147.varType="@varType@";
	myVars.races.race147.repairType = "@RepairType";
	myVars.races.race147.event1={};
	myVars.races.race147.event2={};
	myVars.races.race147.event1.id = "Lu_Id_li_39";
	myVars.races.race147.event1.type = "onmouseover";
	myVars.races.race147.event1.loc = "Lu_Id_li_39_LOC";
	myVars.races.race147.event1.isRead = "True";
	myVars.races.race147.event1.eventType = "@event1EventType@";
	myVars.races.race147.event2.id = "Lu_Id_li_21";
	myVars.races.race147.event2.type = "onmouseover";
	myVars.races.race147.event2.loc = "Lu_Id_li_21_LOC";
	myVars.races.race147.event2.isRead = "False";
	myVars.races.race147.event2.eventType = "@event2EventType@";
	myVars.races.race147.event1.executed= false;// true to disable, false to enable
	myVars.races.race147.event2.executed= false;// true to disable, false to enable
</script>

