<script id = "race191a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race191={};
	myVars.races.race191.varName="Function[17354].guid";
	myVars.races.race191.varType="@varType@";
	myVars.races.race191.repairType = "@RepairType";
	myVars.races.race191.event1={};
	myVars.races.race191.event2={};
	myVars.races.race191.event1.id = "Lu_Id_li_21";
	myVars.races.race191.event1.type = "onmouseover";
	myVars.races.race191.event1.loc = "Lu_Id_li_21_LOC";
	myVars.races.race191.event1.isRead = "False";
	myVars.races.race191.event1.eventType = "@event1EventType@";
	myVars.races.race191.event2.id = "Lu_Id_li_83";
	myVars.races.race191.event2.type = "onmouseover";
	myVars.races.race191.event2.loc = "Lu_Id_li_83_LOC";
	myVars.races.race191.event2.isRead = "True";
	myVars.races.race191.event2.eventType = "@event2EventType@";
	myVars.races.race191.event1.executed= false;// true to disable, false to enable
	myVars.races.race191.event2.executed= false;// true to disable, false to enable
</script>

