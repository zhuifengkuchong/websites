<script id = "race185b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race185={};
	myVars.races.race185.varName="Function[17354].guid";
	myVars.races.race185.varType="@varType@";
	myVars.races.race185.repairType = "@RepairType";
	myVars.races.race185.event1={};
	myVars.races.race185.event2={};
	myVars.races.race185.event1.id = "Lu_Id_li_77";
	myVars.races.race185.event1.type = "onmouseover";
	myVars.races.race185.event1.loc = "Lu_Id_li_77_LOC";
	myVars.races.race185.event1.isRead = "True";
	myVars.races.race185.event1.eventType = "@event1EventType@";
	myVars.races.race185.event2.id = "Lu_Id_li_21";
	myVars.races.race185.event2.type = "onmouseover";
	myVars.races.race185.event2.loc = "Lu_Id_li_21_LOC";
	myVars.races.race185.event2.isRead = "False";
	myVars.races.race185.event2.eventType = "@event2EventType@";
	myVars.races.race185.event1.executed= false;// true to disable, false to enable
	myVars.races.race185.event2.executed= false;// true to disable, false to enable
</script>

