<script id = "race238a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race238={};
	myVars.races.race238.varName="Function[17354].guid";
	myVars.races.race238.varType="@varType@";
	myVars.races.race238.repairType = "@RepairType";
	myVars.races.race238.event1={};
	myVars.races.race238.event2={};
	myVars.races.race238.event1.id = "Lu_Id_li_21";
	myVars.races.race238.event1.type = "onmouseover";
	myVars.races.race238.event1.loc = "Lu_Id_li_21_LOC";
	myVars.races.race238.event1.isRead = "False";
	myVars.races.race238.event1.eventType = "@event1EventType@";
	myVars.races.race238.event2.id = "Lu_Id_li_130";
	myVars.races.race238.event2.type = "onmouseover";
	myVars.races.race238.event2.loc = "Lu_Id_li_130_LOC";
	myVars.races.race238.event2.isRead = "True";
	myVars.races.race238.event2.eventType = "@event2EventType@";
	myVars.races.race238.event1.executed= false;// true to disable, false to enable
	myVars.races.race238.event2.executed= false;// true to disable, false to enable
</script>

