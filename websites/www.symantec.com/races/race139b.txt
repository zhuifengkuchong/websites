<script id = "race139b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race139={};
	myVars.races.race139.varName="Function[17354].guid";
	myVars.races.race139.varType="@varType@";
	myVars.races.race139.repairType = "@RepairType";
	myVars.races.race139.event1={};
	myVars.races.race139.event2={};
	myVars.races.race139.event1.id = "Lu_Id_li_31";
	myVars.races.race139.event1.type = "onmouseover";
	myVars.races.race139.event1.loc = "Lu_Id_li_31_LOC";
	myVars.races.race139.event1.isRead = "True";
	myVars.races.race139.event1.eventType = "@event1EventType@";
	myVars.races.race139.event2.id = "Lu_Id_li_21";
	myVars.races.race139.event2.type = "onmouseover";
	myVars.races.race139.event2.loc = "Lu_Id_li_21_LOC";
	myVars.races.race139.event2.isRead = "False";
	myVars.races.race139.event2.eventType = "@event2EventType@";
	myVars.races.race139.event1.executed= false;// true to disable, false to enable
	myVars.races.race139.event2.executed= false;// true to disable, false to enable
</script>

