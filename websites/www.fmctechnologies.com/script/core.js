// FMC Technologies
// Common client-side script library
//

// -------------------------------------------------------------
// Show client alert
// -------------------------------------------------------------



function ShowClientAlert(txt)
{
	alert(txt);
}

// -------------------------------------------------------------
// Sets focus to a control 
// -------------------------------------------------------------        
function SetFocus(elmID){
  try{
    var obj=document.getElementById(elmID);
    if ( obj ){
      obj.focus();      
    }
    return true;
  }
  catch(e){
  }
}


// -------------------------------------------------------------
// Get An Object
// -------------------------------------------------------------        
function getObj(name)
{
  if (document.getElementById)
  {
  	return document.getElementById(name);
  }
  else if (document.all)
  {
	return document.all[name];
  }
  else if (document.layers)
  {
   	return document.layers[name];
  }
}


// -------------------------------------------------------------
// Get querystring parameters
// 
// Parse the current page's querystring
//     var qs = new Querystring()
// 
// Parse a given querystring
//     var qs2 = new Querystring("name1=value1&amp;name2=value2")
//
// Get parameters from the querystring
//     var v1 = qs2.get("name1")
//     var v3 = qs2.get("name3", "default value")
//
// http://adamv.com/dev/javascript/querystring
//
// -------------------------------------------------------------        

function Querystring(qs) { // optionally pass a querystring to parse
	this.params = new Object()
	this.get=Querystring_get
	
	if (qs == null)
		qs=location.search.substring(1,location.search.length)

	if (qs.length == 0) return

	// Turn <plus> back to <space>
	// See: http://www.w3.org/TR/REC-html40/interact/forms.html#h-17.13.4.1
	qs = qs.replace(/\+/g, ' ')
	var args = qs.split('&') // parse out name/value pairs separated via &
	
	// split out each name=value pair
	for (var i=0;i<args.length;i++) {
		var value
		var pair = args[i].split('=')
		var name = unescape(pair[0])

		if (pair.length == 2)
			value = unescape(pair[1])
		else
			value = name
		
		this.params[name] = value
	}
}


// -------------------------------------------------------------
// Show a popup div positioned relative to current mouse pointer
// -------------------------------------------------------------        
function showPop(id)
{
	var popObj = document.getElementById(id);
	var mp = findMousePosition();
	popObj.style.position = "absolute";
	popObj.style.top = mp.y;
	popObj.style.left = mp.x;
	showdiv(id);
}
    
// -------------------------------------------------------------
// Hide a popup div
// -------------------------------------------------------------        
function hidePop(pop)
{
	hidediv(pop);
}

// -------------------------------------------------------------
// Show a div
// -------------------------------------------------------------        
function showdiv(id) {
	//safe function to show an element with a specified id
		  
	if (document.getElementById) 
	{ // DOM3 = IE5, NS6
		document.getElementById(id).style.display = 'block';
	}
	else 
	{
		if (document.layers) 
		{ // Netscape 4
			document.id.display = 'block';
		}
		else 
		{ // IE 4
			document.all.id.style.display = 'block';
		}
	}
}

// -------------------------------------------------------------
// Hide a div
// -------------------------------------------------------------        
function hidediv(id)
{
	if (document.getElementById) { // DOM3 = IE5, NS6
		document.getElementById(id).style.display = 'none';
	}
	else 
	{
		if (document.layers) { // Netscape 4
			document.id.display = 'none';
		}
		else { // IE 4
			document.all.id.style.display = 'none';
		}
	}
}

// -------------------------------------------------------------
// Find the x-position of an object
// -------------------------------------------------------------        
function findPosX(obj)
  {
    var curleft = 0;
    if(obj.offsetParent)
        while(1) 
        {
          curleft += obj.offsetLeft;
          if(!obj.offsetParent)
            break;
          obj = obj.offsetParent;
        }
    else if(obj.x)
        curleft += obj.x;
    return curleft;
}

// -------------------------------------------------------------
// Find the y-position of an object
// -------------------------------------------------------------        
function findPosY(obj)
{
    var curtop = 0;
    if(obj.offsetParent)
        while(1)
        {
          curtop += obj.offsetTop;
          if(!obj.offsetParent)
            break;
          obj = obj.offsetParent;
        }
    else if(obj.y)
        curtop += obj.y;
    return curtop;
}
  
// -------------------------------------------------------------
// Find the location of the mouse pointer
// -------------------------------------------------------------        
function findMousePosition(e)
{
    e = e || window.event;
    var cursor = {x:0, y:0};
    if (e.pageX || e.pageY) {
        cursor.x = e.pageX;
        cursor.y = e.pageY;
    } 
    else {
        var de = document.documentElement;
        var b = document.body;
        cursor.x = e.clientX + 
            (de.scrollLeft || b.scrollLeft) - (de.clientLeft || 0);
        cursor.y = e.clientY + 
            (de.scrollTop || b.scrollTop) - (de.clientTop || 0);
    }
    return cursor;
}
