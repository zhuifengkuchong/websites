// FMC Technologies
// Search functions
//

// -------------------------------------------------------------
// Search Box
// -------------------------------------------------------------        

function SearchBoxGotFocus(box)
{
	if (box.value == 'SEARCH')
	{
		box.value = '';
	}
	box.style.color='black';
}

function SearchBoxLostFocus(box)
{
	if (box.value == '')
	{
		box.value = 'SEARCH';
	}
	box.style.color='#B8B8B8';
}

function EnableSearchButton(btn, box)
{
	btn.disabled = (box.value == '' || box.value == 'SEARCH');
}

function CheckSubmitSearch(box)
{
	if (box.value == '' || box.value == 'SEARCH')
	{
		alert("Please enter some text in the search box");
		return false;
	}
	else
	{
		return true;
	}
}
