

// -----------------------------------------------------------------------------------------
// The BrightcoveIntegration Namespace

var BCI = {}

BCI.VideoController = function (viewerID, name, width, height) {

	// The id of viewer control associated with this controller.
	this.viewerID = viewerID;

	// The name of this script object.
	this.Name = name;

	// The initial dimensions of the viewer.
	this.ViewerWidth = width;
	this.ViewerHeight = height;

	// The id passed on the templateLoadHandler event
	this.experienceID = null;

	// The Brightcove "experience" wrapper
	this.bcPlayer = null;

	// Modules
	this.bcExperience = null;
	this.bcMenu = null;
	this.bcAds = null;
	this.bcSocial = null;
	this.bcVideoPlayer = null;

	// Status registers
	this.playerLoaded = false;
	this.playerReady = false;

	// ------------------------------------------------------------------------------------------------------------
	// Player event handlers
	// Note: These are for embedded ("flash") video ... VideoScriptsHtml5Iframe.js contains the equivalent scripts for html5. 

	// Experience handlers ----------------------------

	this.onTemplateLoaded = function (id) {
		if (!bciUtils.isVideoInIframe()) {
			this.experienceID = id;
			this.playerReady = false;

			this.bcPlayer = brightcove.getExperience(id);
			this.bcVideoPlayer = this.bcPlayer.getModule(APIModules.VIDEO_PLAYER);
			this.bcExperience = this.bcPlayer.getModule(APIModules.EXPERIENCE);
			this.bcMenu = this.bcPlayer.getModule(APIModules.MENU);
			this.bcAds = this.bcPlayer.getModule(APIModules.ADVERTISING);
			this.bcSocial = this.bcPlayer.getModule(APIModules.SOCIAL);
			this.playerLoaded = true;
			bciUtils.log('Template loaded: ' + this.viewerID, this.Name + '.onTemplateLoaded');

			jQuery("#" + id).parents(".PlaylistPlayer").find(".VideoThumbnailLinkbutton").data({
				"viewer": { "id": id }
			});


			return this.bcExperience;

		}
	}

	this.onPlayerReady = function (event) {

		this.playerReady = true;
		var currentVid = this.bcVideoPlayer.getCurrentVideo();
		if (currentVid) {
			this.resetPlayerDimensions();
		}
		bciUtils.log('Media ready: ' + this.viewerID, this.Name + '.onPlayerReady');
	}

	// VidepPlayer handlers ----------------------------

	this.onPlayerError = function (event) {
		bciUtils.log('Media Error: ' + this.viewerID + '\n[Type: ' + event.type + '] [ErrorType: ' + event.errorType + '] [Error code: ' + event.code + '] [Error info: ' + event.info + ']', this.Name + '.onPlayerError');
	}

	this.onMediaBegin = function (event) {

		var currentVid = this.bcVideoPlayer.getCurrentVideo();
		var videoBcID = currentVid.id;

		var viewer = jQuery("#" + this.viewerID);
		if (!viewer) {
			return;
		}
		var playerPageID = viewer.data("viewer").playerPageID;

		bciUtils.reportVideoStart({ "playerPageID": playerPageID, "videoID": videoBcID });

		bciUtils.log('Media begin: ' + this.viewerID, this.Name + '.onMediaBegin');

	}


	this.onMediaChange = function (event) {
		// http://docs.brightcove.com/en/player/com/brightcove/api/dtos/MediaCollectionDTO.html

		var currentVid = this.bcVideoPlayer.getCurrentVideo();

		if (currentVid) {
			bciUtils.refreshVideoInfo({
				viewerID: this.viewerID,
				displayName: currentVid.displayName,
				shortDescription: currentVid.shortDescription,
				linkURL: currentVid.linkURL,
				linkText: currentVid.linkText
			});

			this.resetPlayerDimensions();
		}

		bciUtils.showElement(this.viewerID, 48);

		bciUtils.log('Media change: ' + this.viewerID, this.Name + '.onMediaChange');

	}


	this.onMediaProgress = function (event) {
	}

	this.registerVideoPlayerEventListener = function (name, delegate) {
		this.bcVideoPlayer.addEventListener(name, delegate);
	}

	this.registerExperienceEventListener = function (name, delegate) {
		this.bcExperience.addEventListener(name, delegate);
	}

	this.resetPlayerDimensions = function () {
		var currentVid = this.bcVideoPlayer.getCurrentVideo();
		var currentRendition = this.bcVideoPlayer.getCurrentRendition();

		var dimensions = bciUtils.calculateVideoDimensions(
			currentRendition.frameWidth,
			currentRendition.frameHeight,
			this.ViewerWidth,
			this.ViewerHeight
			);

		var player = jQuery("#" + this.experienceID)
		player.css("width", dimensions.width);
		player.css("height", dimensions.height);

	}


	// Methods for controlling the Brightcove video player

	this.loadVideo = function (videoId, viralUrl) {
		this.bcVideoPlayer.loadVideo(videoId);
		this.bcSocial.setLink(viralUrl);
	}

}



// -----------------------------------------------------------------------------------------
// Setup Script

jQuery(document).ready(function () {

	// ThumbnailLinks in playlists

	jQuery(".VideoThumbnailLinkbutton").css("cursor", "pointer");

	jQuery(".VideoThumbnailLinkbutton").click(function () {
		bciUtils.playVideo(this.id)
	});

	jQuery(".VideoThumbnailLinkbutton").mouseover(function () {
		var src = jQuery(this).data("images").overImage;
		jQuery(this).children("div.Thumb").children("img:first").attr("src", src);
		jQuery(this).children("div.Title").addClass("Over");
	});

	jQuery(".VideoThumbnailLinkbutton").mouseout(function () {
		var src = jQuery(this).data("images").normalImage;
		jQuery(this).children("div.Thumb").children("img:first").attr("src", src);
		jQuery(this).children("div.Title").removeClass("Over");
	});


	// Wrapped thumbnails and stills placed ad-hoc

	jQuery(".WrappedVideoImage").css("cursor", "pointer");

	jQuery(".WrappedVideoImage").click(function () {
		bciUtils.showVideoOverlay();
		bciUtils.playVideo(this.id)
	});


	jQuery(".WrappedVideoImage").mouseover(function () {
		var src = jQuery(this).data("images").overImage;
		jQuery(this).children("img:first").attr("src", src);
	});

	jQuery(".WrappedVideoImage").mouseout(function () {
		var src = jQuery(this).data("images").normalImage;
		jQuery(this).children("img:first").attr("src", src);
	});

	jQuery(".VideoOverlayPlayerContainer").children(".CloseDiv").children(".CloseLink:first").click(function () {

		var controllerName = jQuery(this).data("video").controller;
		var controller = window[controllerName]
		if (controller.bcVideoPlayer) {
			controller.bcVideoPlayer.pause(true);
		} else {
			var viewerID = controller.viewerID;
			var viewer = jQuery("#" + viewerID);
			var iframe = viewer.children("iframe.BrightcoveExperience:first")
			var frameContentWindow = iframe[0].contentWindow;
			var sendData = { "viewerID": viewerID };
			bciMessaging.sendMessage(frameContentWindow, "bciHtml5.pauseVideo", sendData, frameContentWindow.location);
		}
		bciUtils.hideVideoOverlay();
	});

	// Setup listener for Iframe-based players
	if (typeof window.addEventListener != 'undefined') {
		window.addEventListener('message', bciMessaging.handleMessage, false);
	} else if (typeof window.attachEvent != 'undefined') {
		window.attachEvent('onmessage', bciMessaging.handleMessage);
	}

});
