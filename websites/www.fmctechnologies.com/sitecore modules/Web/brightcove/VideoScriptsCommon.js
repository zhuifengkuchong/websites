var bciUtils = {


	reportVideoStart: function (data, origin) {
		try {
			//CallClientEventPipeline('BrightcovePlayVideo', data.playerPageID, "VideoBegin", "Video played", data.videoID);
			SitecoreClientEvent.Send(
				'BrightcovePlayVideo',
				{
					'AnalyticsItem': data.playerPageID,
					'AnalyticsGoal': 'Play Video',
					'AnalyticsKey': 'VideoBegin', 
					'AnalyticsText': 'Video started',
					'AnalyticsData': data.videoID, 
					'AnalyticsInteger': '',
					'VideoID': data.videoID
				}
			);
		}
		catch (exception) {
			bciUtils.log("Error reporting video start: " + exception.message, "bciUtils.reportVideoStart");
		}

	},

	registerShare: function(node)
	{
		var targetItem = node.data('AddThis').targetItemID;
		var shareType = node.data('AddThis').shareType;
		var shareData = node.data('AddThis').shareData;
		SitecoreClientEvent.Send(
			'BrightcoveShareVideo',
			{
				'TargetItemID': targetItemID,
				'ShareType': shareType,
				'ShareData': shareData
			}
		);
	},

	refreshVideoInfo: function (data, origin) {
		var viewer = jQuery("#" + data.viewerID);
		if (!viewer) {
			return;
		}

		var title = jQuery("#" + data.viewerID).children("div.VideoTitle:first");
		if (title) {
			title.html('');
			title.html(data.displayName);
		}

		var description = jQuery("#" + data.viewerID).children("div.VideoDescription:first");
		if (description) {
			var desc = data.shortDescription;
			if (data.linkText) {
				desc += '<br><a href="' + data.linkURL + '">' + data.linkText + "</a>";
			}
			description.html('');
			description.html(desc);
		}

	},

	playVideo: function (id) {
		var tn = jQuery("#" + id);
		if (tn.data("iframe")) {
			bciUtils.playIframeVideo(tn);
		} else {
			bciUtils.playFlashVideo(tn);
		}
	},

	playFlashVideo: function (tn) {
		var controller = window[tn.data("video").controller];
		var player = jQuery("#" + controller.viewerID);

		player.children("div.VideoTitle").html("Loading <span style='font-weight: normal; font-style: italic; '>" + tn.data("video").name + "</span>...");
		player.children("div.VideoDescription").html("Please wait.");

		var data = {};
		data.videoId = tn.data("video").id;
		data.viralUrl = tn.data("video").viralUrl;
		data.controllerName = tn.data("video").controller;
		bciUtils.waitPlayVideo(data);
	},

	playIframeVideo: function (tn) {
		var frameSelector = tn.data("iframe").selector;
		var iframe = jQuery(frameSelector);
		player = iframe.parents("div.VideoPlayerContainer");

		player.children("div.VideoTitle").html("Loading <span style='font-weight: normal; font-style: italic; '>" + tn.data("video").name + "</span>...");
		player.children("div.VideoDescription").html("Please wait.");

		var dimensions = bciUtils.calculateVideoDimensions(
			tn.data("video").frameWidth,
			tn.data("video").frameHeight,
			player.data("viewer").viewerWidth,
			player.data("viewer").viewerHeight
		);

		var u = iframe.attr("src");
		u = bciUtils.setQuerystringValue(u, "width", dimensions.width);
		u = bciUtils.setQuerystringValue(u, "height", dimensions.height);
		u = bciUtils.setQuerystringValue(u, "%40videoPlayer", tn.data("video").id);
		//u = bciUtils.setQuerystringValue(u, "thumbnailID", id);
		u = bciUtils.setQuerystringValue(u, "thumbnailID", tn.attr("id"));
		iframe[0].src = u;

		iframe.css("height", dimensions.height);
		iframe.css("width", dimensions.width);
	},


	waitPlayVideo: function (data, origin) {

		var controller = window[data.controllerName];

		if (!data.loops) {
			data.loops = 0;
		}

		if (controller.playerReady) {
			controller.loadVideo(data.videoId, data.viralUrl);
		} else {
			if (data.loops > 100) {
				bciUtils.log("Giving up, loop count exceeded.", 'waitPlayVideo');
				alert('Sorry, there was an error playing this video (player not ready).');
				return false;
			}
			if (data.loops % 10 == 0) {
				bciUtils.log(data.loops / 10, 'waitPlayVideo');
			}
			var d =
				"{"
				+ "'videoId':" + "'" + data.videoId + "', "
				+ "'viralUrl':" + "'" + data.viralUrl + "', "
				+ "'controllerName':" + "'" + data.controllerName + "', "
				+ "'loops':" + ++(data.loops)
				+ "}";
			s = "bciUtils.waitPlayVideo(" + d + ");";
			window.setTimeout(s, 100);
		}
	},

	setIframePlayerSize: function (viewerID, frameW, frameH, viewerW, viewerH) {
		if (bciUtils.isVideoInIframe()) {
			var dimensions = bciUtils.calculateVideoDimensions(frameW, frameH, viewerW, viewerH);
			var frameSelector = "#" + viewerID + " > iframe.BrightcoveExperience:first"
			var iframe = jQuery(frameSelector);
			iframe.css("height", dimensions.height);
			iframe.css("width", dimensions.width);

			//var player = iframe.parents("div.VideoPlayerContainer:first");
		}
	},

	showElement: function (viewerID, viewOffset) {
		var viewer = jQuery("#" + viewerID);

		var docViewTop = jQuery(window).scrollTop();
		var docViewBottom = docViewTop + jQuery(window).height();
		var elemTop = viewer.offset().top;
		var elemBottom = elemTop + viewer.height();

		if ((elemBottom <= docViewBottom) && (elemTop >= docViewTop)) {
			// viewer is visible
		} else {
			var offset = viewer.offset();
			destination = offset.top - viewOffset;
			jQuery(document).scrollTop(destination);
		}
	},


	showVideoOverlay: function () {
		var overlay = jQuery("#VideoOverlay");
		//overlay.addClass("OverlayVisible");
		overlay.css("visibility", "visible");
		overlay.css("display", "block");
		overlay.css("top", jQuery(window).scrollTop());

		var overlayContainer = jQuery("#VideoOverlayContainer");
		overlayContainer.css("visibility", "visible");
		overlayContainer.css("top", jQuery(window).scrollTop() + 100);
	},

	hideVideoOverlay: function () {
		var overlay = jQuery("#VideoOverlay");
		//overlay.removeClass("OverlayVisible");
		overlay.css("visibility", "none");
		overlay.css("display", "none");

		var overlayContainer = jQuery("#VideoOverlayContainer");
		overlayContainer.css("visibility", "hidden");
	},


	getQuerystringValue: function (url, key, default_) {
		if (default_ == null) {
			default_ = "";
		}
		var a = document.createElement('a');
		a.href = url;
		var search = unescape(a.search);
		if (search == "") {
			return default_;
		}
		search = search.substr(1);
		var params = search.split("&");
		for (var i = 0; i < params.length; i++) {
			var pairs = params[i].split("=");
			if (pairs[0] == key) {
				return pairs[1];
			}
		}
		return default_;
	},

	setQuerystringValue: function (url, name, value) {
		var patt = new RegExp("([?|&]" + name + "=)[^\&]+");
		if (patt.test(url)) {
			return url.replace(patt, '$1' + value);
		} else {
			return url + "&" + name + "=" + value;
		}
	},


	arrayContains: function (array, element) {
		for (var i = 0; i < array.length; i++) {
			if (array[i] == element) {
				return true;
			}
		}
		return false;
	},

	stringEndsWith: function (str, suffix) {
		return str.indexOf(suffix, str.length - suffix.length) !== -1;
	},

	stringStartsWith: function (str, suffix) {
		return str.slice(0, suffix.length) == suffix;
	},

	getAspectRatio: function (width, height) {

		// get the common denominator
		var w, x, y, cd;
		x = width;
		y = height;
		while (y != 0) {
			w = x % y;
			x = y;
			y = w;
		}
		commonDenuminator = x;
		return { w: width / x, h: height / x };
	},

	calculateVideoDimensions: function (frameW, frameH, viewerW, viewerH) {
		if (!(frameW && frameH && viewerW && viewerH)) { return; }
		var width, height;
		if ((frameW < viewerW) && (frameH < viewerH)) {
			width = frameW;
			height = frameH
		} else {
			var aspectRatio = bciUtils.getAspectRatio(frameW, frameH);
			var factor = Math.floor(viewerW / aspectRatio.w);
			width = factor * aspectRatio.w;
			height = factor * aspectRatio.h;
		}
		return { "width": width, "height": height };
	},

	//	resetObjectDimensions: function(data, origin){
	//		var obj = jQuery(data.selector);
	//		obj.css("height", data.height);
	//		obj.css("width", data.width);
	//	},



	raiseZIndex: function (objectToMove, objectToOvertake) {
		var moveObj = jQuery('#' + objectToMove);
		var overtakeObj = jQuery('#' + objectToOvertake);
		moveObj.css('z-index', parseInt(overtakeObj.css('z-index')) + 1);
	},

	isVideoInIframe: function () {
		if (jQuery('iframe.BrightcoveExperience')[0]) {
			return true;
		} else {
			return false;
		}
	},

	currentWindowIsVideoIframe: function () {
		return bciUtils.stringEndsWith(window.location.hostname, "http://www.fmctechnologies.com/sitecore modules/Web/Brightcove/brightcove.com");
	},

	log: function (message, obj) {
		try {
			var msg = '';
			if (obj) { msg = "[" + obj + "] "; }
			msg = msg + message;
			console.log(msg);
		}
		catch (err) { }
	},

	getIframePlayerFromExperienceID: function (experienceID) {
		try {
			//bciUtils.log("get iframe for " + experienceID);
			var iframeSelector = getExperienceIframeSelector(experienceID);
			return jQuery(iframeSelector).parents("div.VideoPlayerContainer");
		}
		catch (err) {
		}
	},

	getExperienceIframeSelector: function (experienceID) {
		return "iframe.BrightcoveExperience[src*='flashID=" + experienceID + "']"; ;
	}



}

//function BcViewer2Controller_onMediaChange(event) { BcViewer2Controller.onMediaChange(event); }

var bciHandlers = {

	onTemplateLoaded: function (experienceID) {
		var controller;
		if (bciUtils.isVideoInIframe()) {
			// Brightcove's iFrame has messaged over a ""callTemplateReady" event from the iFrame.
			// This is handled in the iframe script's constructor
		} else {

			// Call onTemplateLoaded for this controller instance, register the player events, and listen for PlayerReady.

			var controllerName = jQuery("#" + experienceID).parents(".VideoPlayerContainer").data("viewer").controller
			controller = eval(controllerName);
			if (!controller) { return; }
			var experience = controller.onTemplateLoaded(experienceID);

			bciHandlers[controller.Name + "_onMediaBegin"] = function (event) { window[controller.Name].onMediaBegin(event); };
			controller.registerVideoPlayerEventListener("mediaBegin", bciHandlers[controller.Name + "_onMediaBegin"]);

			bciHandlers[controller.Name + "_onMediaChange"] = function (event) { window[controller.Name].onMediaChange(event); };
			controller.registerVideoPlayerEventListener("mediaChange", bciHandlers[controller.Name + "_onMediaChange"]);

			bciHandlers[controller.Name + "_onMediaProgress"] = function (event) { window[controller.Name].onMediaProgress(event); };
			controller.registerVideoPlayerEventListener("mediaProgress", bciHandlers[controller.Name + "_onMediaProgress"]);

			if (experience.getReady()) {
				controller.onPlayerReady();
			} else {
				bciHandlers[controller.Name + "_onPlayerReady"] = function (event) { window[controller.Name].onPlayerReady(event); };
				controller.registerExperienceEventListener(brightcove.api.events.ExperienceEvent.TEMPLATE_READY, bciHandlers[controller.Name + "_onPlayerReady"]);
			}

			controller.onMediaChange(null);

		}

		bciUtils.log("Template loaded: " + experienceID, "bciHandlers.onTemplateLoaded");

	},

	onFrameReady: function (data, origin) {

		var frameLocation = data.location;
		var viewerID = data.viewer;
		bciUtils.log("Received onFrameReady from iFrame " + viewerID, "bciHandlers.onFrameReady");

		var frameSelector = "#" + viewerID + " > iframe.BrightcoveExperience:first"
		var iframe = jQuery(frameSelector);
		var frameContentWindow = iframe[0].contentWindow;
		var player = iframe.parents("div.VideoPlayerContainer:first");

		//var dimensions = bciUtils.calculateVideoDimensions(data.frameWidth, data.frameHeight, player.data("viewer").viewerWidth, player.data("viewer").viewerHeight);
		//iframe.css("height", dimensions.height);
		//iframe.css("width", dimensions.width);

		jQuery("#" + viewerID).parent().find(".VideoThumbnailLinkbutton").data({
			"iframe": { "location": frameLocation, "selector": frameSelector }
		});

		jQuery("#" + viewerID).data({
			"iframe": { "location": frameLocation, "selector": frameSelector }
		});

		if (jQuery("#" + viewerID).parents(".VideoOverlayPlayerContainer")){
			jQuery(".WrappedVideoImage").data({
				"iframe": { "location": frameLocation, "selector": frameSelector }
			});
		}

		var sendData = { "viewerID": viewerID };
		bciMessaging.sendMessage(frameContentWindow, "bciHtml5.doInit", sendData, frameContentWindow.location);

	},

	onIframePlayerReady: function(data, origin)
	{
		if (data.thumbnailID)
		{
			var frameLocation = data.location;
			var viewerID = data.viewer;
			var frameSelector = "#" + viewerID + " > iframe.BrightcoveExperience:first"
			var iframe = jQuery(frameSelector);

	//		var player = iframe.parents("div.VideoPlayerContainer");
	//		var dimensions = bciUtils.calculateVideoDimensions(data.frameWidth, data.frameHeight, player.data("viewer").viewerWidth, player.data("viewer").viewerHeight);
	//		iframe.css("width", dimensions.width);
	//		iframe.css("height", dimensions.height);

			iframe.css("width", data.width);
			iframe.css("height", data.height);
		}

	}


}

var bciMessaging = {

	validDomains: ["http://www.fmctechnologies.com/sitecore modules/Web/Brightcove/fmctechnologies.com", "hou2vmdev05", "http://www.fmctechnologies.com/sitecore modules/Web/Brightcove/fmcti.com", "http://www.fmctechnologies.com/sitecore modules/Web/Brightcove/brightcove.com"],

	sendMessage: function (target, method, data, targetDomain) {
		if (method === "bciHtml5.onTemplateLoaded") {
			var nosuch = "bciHtml5.onTemplateLoaded";
		}
		try {
			var args = { "method": method, "data": data };
			//target.postMessage(args, targetDomain);
			target.postMessage(args, "*");
		} catch (ex) {
			bciUtils.log("Error sending message to " + target.location + "; " + ex.message, "bciMessaging.sendMessage");
		}
	},

	handleMessage: function (e) {
		var data = e.data;
		var origin = e.origin;

		var domainIsValid = false;

		for (var i = 0; i < bciMessaging.validDomains.length; i++) {
			var d = bciMessaging.validDomains[i];
			if (bciUtils.stringEndsWith(e.origin, d)) {
				domainIsValid = true;
				break;
			}
		}
		if (!domainIsValid) {
			bciUtils.log("Rejected message from " + e.origin, "bciMessaging.handleMessage");
			return;
		}

		if (!data) {
			bciUtils.log("No data found on message from " + e.origin, "bciMessaging.handleMessage");
			return;
		}

		if ((typeof data) == "string") {
			//var brightcoveNativeMessageString = "brightcove.player";

			if (
				( bciUtils.stringStartsWith(data, "brightcove.player") )
				|| ( bciUtils.stringStartsWith(data, "complete::") )
				|| ( data == "complete" )
				){
				// Ignore Brightcove's native messaging.
				// bciUtils.log("Ignoring Brightcove native message from " + e.origin + ",\nData: " + data);
				return;

			} else {
				// Didn't understand the message.
				bciUtils.log("Invalid data payload on message from " + e.origin + ",\nData: " + data, "bciMessaging.handleMessage");
				return;
			}
		}

		if ((!data.method) || data.method == '') {
			try {
				bciUtils.log("No method on message from " + e.origin + ", Data: " + JSON.stringify(data), "bciMessaging.handleMessage");
			} catch (ex) {
				bciUtils.log("No method on message from " + e.origin + ", Data: " + data, "bciMessaging.handleMessage");
			}
			return;
		}

		if (data.method === "bciHtml5.onTemplateLoaded") {
			var nosuch = "bciHtml5.onTemplateLoaded";
		}

		var fn = eval(data.method);
		var passData = {};
		if (data.data) {
			passData = data.data;
		}
		if (typeof fn === 'function') {
			try {
				fn(passData, e.origin);
			}
			catch (ex) {
				bciUtils.log("Failed processing method '" + data.method + " , Error: " + ex.message + " \nData: " + JSON.stringify(data), "bciMessaging.handleMessage");
			}
		} else {
			bciUtils.log("Could not interpret method " + data.method + " on message from " + e.origin, "bciMessaging.handleMessage");
		}
	}
}

