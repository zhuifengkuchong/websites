var SitecoreClientEvent = {

	Send: function (pipelineName, eventData) {
		var ed = eventData;
		ed._pipeline = pipelineName;
		jQuery.post(
			"http://www.fmctechnologies.com/sitecore%20modules/Web/ClientEvent/ClientEvent.aspx",
			ed,	
			function (data) {
				try {
					if (data.length > 0) {
						console.log(pipelineName + ":" + data);
					}
				}
				catch (err) { }
			},
			"text"
			);
	},


	Analytics: function (item, goal, key, text, data, integer) {
		SitecoreClientEvent.Send(
			"ClientEvent.AddToAnalytics",
			{
				AnalyticsItem: item,
				AnalyticsGoal: goal,
				AnalyticsKey: key,
				AnalyticsText: text,
				AnalyticsData: data,
				AnalyticsInteger: integer
			}
		);
	},

	Log: function (text) {
		SitecoreClientEvent.Send("ClientEvent.LogEvent", { LogText: text });
	}

}