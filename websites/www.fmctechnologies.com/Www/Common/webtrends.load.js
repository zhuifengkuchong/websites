// WebTrends SmartSource Data Collector Tag v10.4.16
// Copyright (c) 2014 Webtrends Inc.  All rights reserved.
// Tag Builder Version: 4.1.2.12
// Created: 2014.02.06
window.webtrendsAsyncInit = function () {
    var dcs = new Webtrends.dcs().init({
        dcsid: "dcsf7gfdv00000cp3l2q1oof0_2u9m",
        domain: "http://www.fmctechnologies.com/Www/Common/statse.webtrendslive.com",
        timezone: -6,
        i18n: true,
        offsite: true,
        download: true,
        downloadtypes: "xls,doc,pdf,txt,csv,zip,docx,xlsx,rar,gzip",
        anchor: true,
        javascript: true,
        onsitedoms: "http://www.fmctechnologies.com/Www/Common/fmctechnologies.com",
        fpcdom: ".fmctechnologies.com",
        plugins: {
            //hm:{src:"../../../s.webtrends.com/js/webtrends.hm.js"/*tpa=http://s.webtrends.com/js/webtrends.hm.js*/}
        }
    }).track();
};
(function () {
    var s = document.createElement("script"); s.async = true; s.src = "webtrends.min.js"/*tpa=http://www.fmctechnologies.com/Www/Common/webtrends.min.js*/;
    var s2 = document.getElementsByTagName("script")[0]; s2.parentNode.insertBefore(s, s2);
}());