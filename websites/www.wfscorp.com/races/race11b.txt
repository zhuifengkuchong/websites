<script id = "race11b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race11={};
	myVars.races.race11.varName="Object[9402].loadedImages";
	myVars.races.race11.varType="@varType@";
	myVars.races.race11.repairType = "@RepairType";
	myVars.races.race11.event1={};
	myVars.races.race11.event2={};
	myVars.races.race11.event1.id = "Lu_Id_img_5";
	myVars.races.race11.event1.type = "onload";
	myVars.races.race11.event1.loc = "Lu_Id_img_5_LOC";
	myVars.races.race11.event1.isRead = "True";
	myVars.races.race11.event1.eventType = "@event1EventType@";
	myVars.races.race11.event2.id = "Lu_Id_img_4";
	myVars.races.race11.event2.type = "onload";
	myVars.races.race11.event2.loc = "Lu_Id_img_4_LOC";
	myVars.races.race11.event2.isRead = "False";
	myVars.races.race11.event2.eventType = "@event2EventType@";
	myVars.races.race11.event1.executed= false;// true to disable, false to enable
	myVars.races.race11.event2.executed= false;// true to disable, false to enable
</script>

