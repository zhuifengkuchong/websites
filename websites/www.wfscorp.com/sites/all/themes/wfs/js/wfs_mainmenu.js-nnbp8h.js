(function ($) {

// Explain link in query log
Drupal.behaviors.wfs_mainmenu = {
  attach: function() {
     function show() {
       var menu = $(this);
       menu.children(".menu").slideDown();
     }

     function hide() {
       var menu = $(this);
       menu.children(".menu").slideUp();
     }

     $("#main-menu li").hoverIntent({
       sensitivity: 1, // number = sensitivity threshold (must be 1 or higher)
       interval: 50,   // number = milliseconds for onMouseOver polling interval
       over: show,     // function = onMouseOver callback (required)
       timeout: 300,   // number = milliseconds delay before onMouseOut
       out: hide       // function = onMouseOut callback (required)
     });

     $('.slideshow').cycle({
      fx: 'fade', // choose your transition type, ex: fade, scrollUp, shuffle, etc...
      pager: '.slideshow_pager',
      pause: 1,
      speed: 500,
      timeout: 8000
      });

      $('#left-menu-block ul.menu a').mouseover(function () {
        $(this).prev('div').addClass('hover');
      });
      $('#left-menu-block ul.menu a').mouseout(function () {
        $(this).prev('div').removeClass('hover');
      });
    }
  }
})(jQuery);
