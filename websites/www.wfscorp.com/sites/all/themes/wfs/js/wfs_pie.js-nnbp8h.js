(function ($) {
  Drupal.behaviors.wfs_pie = {
    attach: function() {

      if ($("#left-menu-block").length > 0) {
        $("#block-domain-menu-block-main-links").treeview({
          persist: "location",
          collapsed: true,
          unique: true
        });

        $("#block-domain-menu-block-footer-links").treeview({
          persist: "location",
          collapsed: false,
          unique: false
        });
      }
      //execute only for IE7 and IE8
      if ($.browser.msie && (parseInt($.browser.version, 10) === 7 || parseInt($.browser.version, 10) === 8)) {
        elements = new Array(33);
        elements[0] = ".sidebar .section .block";
        elements[1] = ".background_Yes";
        elements[2] = ".border_Yes";
        elements[3] = ".border_No";
        elements[4] = ".background_No";
        elements[5] = "#main-menu li";
        elements[6] = "#main-menu a";
        elements[7] = "#main-menu li.hover";
        elements[8] = "#main-menu li.hover";
        elements[9] = ".page-access-denied #central-content-block";
        elements[10] = ".page-not-found #central-content-block";
        elements[11] = ".page-access-denied #page-title";
        elements[12] = ".page-not-found #page-title";
        elements[13] = "#central-content-block ul.horizontal-tabs-list li";
        elements[14] = "form#user-login";
        elements[15] = "form#user-pass";
        elements[16] = "div#default-location div.background_Yes";
        elements[17] = "fieldset.webform-component-fieldset legend";
        elements[18] = "div#webform-component-step-5--step-5-column-1--aircraft-1.form-item";
        elements[19] = "div#webform-component-step-5--step-5-column-2--aircraft-2.form-item";
        elements[20] = "div#webform-component-step-5--step-5-column-3--aircraft-3.form-item";
        elements[21] = "div#webform-component-step-3--step-3-column-primary.webform-component-column div#webform-component-step-3--step-3-column-primary--primary-label.form-item";
        elements[22] = "div#webform-component-step-3--step-3-column-accounting.webform-component-column div#webform-component-step-3--step-3-column-accounting--accounting-label.form-item";
        elements[23] = "div#webform-component-step-3--step-3-column-chief-pilot.webform-component-column div#webform-component-step-3--step-3-column-chief-pilot--chief-pilot-label.form-item";
        elements[24] = "div#webform-component-step-3--step-3-column-optional.webform-component-column div#webform-component-step-3--step-3-column-optional--optional-label.form-item";
        elements[25] = ".become-a-client-webform .form-step #webform-component-step-5--step-5-column-1";
        elements[26] = ".become-a-client-webform .form-step #webform-component-step-5--step-5-column-2";
        elements[27] = ".become-a-client-webform .form-step #webform-component-step-5--step-5-column-3";
        elements[28] = ".become-a-client-webform .form-step #webform-component-step-3--step-3-column-primary";
        elements[29] = ".become-a-client-webform .form-step #webform-component-step-3--step-3-column-accounting";
        elements[30] = ".become-a-client-webform .form-step #webform-component-step-3--step-3-column-chief-pilot";
        elements[31] = ".become-a-client-webform .form-step #webform-component-step-3--step-3-column-optional";
        elements[32] = ".become-a-client-webform .webform-client-form .step-indicator table tr.previous-step td";
        for (i = 0; i< elements.length; i++) {
          $(elements[i]).each(function() {
            if (window.PIE) {
              PIE.attach(this);
            }
          });
        }
      }
      
      $('#block-domain-menu-block-footer-links').css('visibility', 'visible');
      $('#block-domain-menu-block-main-links').css('visibility', 'visible');

    }
  }
})(jQuery);
