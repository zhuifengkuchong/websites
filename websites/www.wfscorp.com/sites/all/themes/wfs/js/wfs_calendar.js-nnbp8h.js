(function ($) {

Drupal.behaviors.wfs_calendar = {
  attach: function(context, settings) {

    var url = window.location.pathname.substr(1);

    var category = '<div class ="calendar-categories">' +
      '<div class="form-wrapper" id="calendar-category-wrapper">' +
      '<div class="form-item form-type-select">' +
      '  <label for="calendar-category">Category</label>' +
      '  <select id="calendar-category" class="form-select">' +
      '  </select>' +
      '</div>' +
    '</div>' +
    '<div class="clear"></div>';

    $("#calendar-category").live('change', function (index) {
      if ($(this).val()) {
        // Update the subcategory
        $("#calendar-subcategory-wrapper").load('/calendar_ajax/sub-category/' + $(this).val());
        // And refresh the calendar
        $(".region-content .content").load('/calendar_ajax/calendar/' + $("#calendar-category").val(),
          function () {
            for (var dom_id in settings.fullcalendar) {
              if (settings.fullcalendar.hasOwnProperty(dom_id)) {
                Drupal.fullcalendar.cache[dom_id] = new Drupal.fullcalendar.fullcalendar(dom_id);
              }
            }

            // Trigger a window resize so that calendar will redraw itself.
            $(window).resize();
          }
        );
      }
      else {
        // When all is selected as Parent Category, remove the subcategory select box.
        $("#calendar-subcategory-wrapper").html('');
      }
    });
    $("#calendar-subcategory").live('change', function (index){
      if ($(this).val()) {
        // When the subcategory changes, update only the calendar
        $(".region-content .content").load('/calendar_ajax/calendar/' + $("#calendar-category").val() + "/" + $(this).val(),
          function () {
            for (var dom_id in settings.fullcalendar) {
              if (settings.fullcalendar.hasOwnProperty(dom_id)) {
                Drupal.fullcalendar.cache[dom_id] = new Drupal.fullcalendar.fullcalendar(dom_id);
              }
            }

            // Trigger a window resize so that calendar will redraw itself.
            $(window).resize();
          }
          );
      }
      else {
        // When all is selected in the subcategory, only refresh the calendar based on the category selected value
        $(".region-content .content").load('/calendar_ajax/calendar/' + $("#calendar-category").val(),
          function () {
          for (var dom_id in settings.fullcalendar) {
            if (settings.fullcalendar.hasOwnProperty(dom_id)) {
              Drupal.fullcalendar.cache[dom_id] = new Drupal.fullcalendar.fullcalendar(dom_id);
            }
          }

          // Trigger a window resize so that calendar will redraw itself.
          $(window).resize();
        });
      }
    });

    $(".events-listing-view .view-header strike").replaceWith(category).show();

    var url_search = location.search.slice(1).split("&");
    parts = {};
    for (i in url_search) {
      parts[url_search[i].split("=")[0]] = url_search[i].split("=")[1];
    }

    if (parts['arg_3']) {
        $(".events-listing-view .view-header .calendar-categories").load("/calendar_categories/" +
         parts['arg_2'] + "/" +
         parts['arg_3'],
         function() { $(this).show(); });

    }
    else {
      if (parts['arg_2']) {
        $(".events-listing-view .view-header .calendar-categories").load("/calendar_categories/" + parts['arg_2'],
         function() { $(this).show(); });
      }
      else {
        $(".events-listing-view .view-header .calendar-categories").load("/calendar_categories", function() {
          $(this).show();
        });
      }
    }
  }
}
})(jQuery);
