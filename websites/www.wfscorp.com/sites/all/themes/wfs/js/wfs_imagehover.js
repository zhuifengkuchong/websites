(function ($) {

// Explain link in query log
Drupal.behaviors.image_hover = {
  attach: function() {
     function hide() {
       var menu = $(this);
       menu.children(".boxcaption").animate({top: 280}, 400);
     }

     function show() {
       var menu = $(this);
       menu.children(".boxcaption").show().animate({top: 0}, 400);
     }

     $(".image_hover").hoverIntent({
       sensitivity: 50, // number = sensitivity threshold (must be 1 or higher)
       interval: 50,   // number = milliseconds for onMouseOver polling interval
       over: show,     // function = onMouseOver callback (required)
       timeout: 300,   // number = milliseconds delay before onMouseOut
       out: hide       // function = onMouseOut callback (required)
     });

    }
  }

})(jQuery);
