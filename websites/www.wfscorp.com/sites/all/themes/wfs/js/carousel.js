(function ($) {
  Drupal.behaviors.carousel = {
    attach: function() {
      $("div.carousel").smoothDivScroll(
      {
        mousewheelScrolling: true,
        manualContinuousScrolling: true,
        visibleHotSpotBackgrounds: "always",
        autoScrollingMode: "always",
        autoScrollingDirection: "endlessloopright",
        autoScrollingInterval: 20,
        hotSpotScrollingInterval: 20
      });
      $("div.carousel").bind("mouseover", function() {
        $(this).smoothDivScroll("stopAutoScrolling");
      }).bind("mouseout", function() {
        $(this).smoothDivScroll("startAutoScrolling");
      });

      $('#left-menu-block shape').css('display', 'none');
    }
  }
})(jQuery);