/**
 * @file
 * Processes the FullCalendar options and passes them to the integration.
 */

(function ($) {


if (Drupal.fullcalendar) {

Drupal.fullcalendar.plugins.fullcalendar = {
  options: function (fullcalendar, settings) {
    var options = {
      eventClick: function (calEvent, jsEvent, view) {
        if (settings.sameWindow) {
          window.open(calEvent.url, '_self');
        }
        else {
          window.open(calEvent.url);
        }
        return false;
      },
      drop: function (date, allDay, jsEvent, ui) {
        for (var $plugin in Drupal.fullcalendar.plugins) {
          if (Drupal.fullcalendar.plugins.hasOwnProperty($plugin) && $.isFunction(Drupal.fullcalendar.plugins[$plugin].drop)) {
            try {
              Drupal.fullcalendar.plugins[$plugin].drop(date, allDay, jsEvent, ui, this, fullcalendar);
            }
            catch (exception) {
              alert(exception);
            }
          }
        }
      },
      events: function (start, end, callback) {
        $(".view-calendar-for-events .fc-view table tbody td").each(function(index) {
          $(this).removeClass("with-event");
          $(this).css("background", "#E5E5E5");
          $(this).css("color", "#6A697B");
        });


        // Fetch new items from Views if possible.
        if (fullcalendar.navigate && settings.ajax) {
          var prev_date, next_date, date_argument, argument, fetch_url;

          prev_date = $.fullCalendar.formatDate(start, 'yyyy-MM-dd');
          next_date = $.fullCalendar.formatDate(end, 'yyyy-MM-dd');
          date_argument = prev_date + '--' + next_date;
          argument = settings.args.replace('fullcalendar_browse', date_argument);
          fetch_url = Drupal.settings.basePath + 'fullcalendar/ajax/results/' + settings.view_name + '/' + settings.view_display + '/' + argument;

          $.ajax({
            type: 'GET',
            url: fetch_url,
            dataType: 'json',
            beforeSend: function () {
              // Add a throbber.
              this.progress = $('<div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div></div>');
              $(fullcalendar.dom_id + ' .fc-header-title').after(this.progress);
            },
            success: function (data) {
              if (data.status) {
                // Replace content.
                $(fullcalendar.dom_id + ' .fullcalendar-content').html(data.content);
                fullcalendar.parseEvents(callback);
              }
              // Remove the throbber.
              $(this.progress).remove();
            },
            error: function (xmlhttp) {
              alert(Drupal.t('An HTTP error @status occurred.', {'@status': xmlhttp.status}));
            }
          });
        }
        else {
          events = fullcalendar.parseEvents(callback);
        }

        if (!fullcalendar.navigate) {
          // Add events from Google Calendar feeds.
          for (var entry in settings.gcal) {
            if (settings.gcal.hasOwnProperty(entry)) {
              $('.fullcalendar', fullcalendar.$calendar).fullCalendar('addEventSource',
                $.fullCalendar.gcalFeed(settings.gcal[entry][0], settings.gcal[entry][1])
              );
            }
          }
        }
        // Set navigate to true which means we've starting clicking on
        // next and previous buttons if we re-enter here again.
        fullcalendar.navigate = true;
      },
      eventDrop: function (event, dayDelta, minuteDelta, allDay, revertFunc) {
        $.post(
          Drupal.settings.basePath + 'fullcalendar/ajax/update/drop/' + event.eid,
          'field=' + event.field + '&entity_type=' + event.entity_type + '&index=' + event.index + '&day_delta=' + dayDelta + '&minute_delta=' + minuteDelta + '&all_day=' + allDay + '&dom_id=' + event.dom_id,
          fullcalendar.update
        );
        return false;
      },
      eventResize: function (event, dayDelta, minuteDelta, revertFunc) {
        $.post(
          Drupal.settings.basePath + 'fullcalendar/ajax/update/resize/' + event.eid,
          'field=' + event.field + '&entity_type=' + event.entity_type + '&index=' + event.index + '&day_delta=' + dayDelta + '&minute_delta=' + minuteDelta + '&dom_id=' + event.dom_id,
          fullcalendar.update
        );
        return false;
      },
      eventAfterRender: function(event, element, view) {
        var url = window.location.pathname.substr(1);
        var current_month = 'fc-month-' + view.title.substr(0, view.title.indexOf(' '));
        var current_year = view.title.substr(view.title.indexOf(' ') + 1);
        var element_classes = element.attr('class').toString();

        if (element_classes.indexOf(current_month.toLowerCase()) > 0) {

          var corrector = null;

          // Find the position of the first day of the month
          // in the first week of the calendar.
          $(".view-calendar-for-events table tr.fc-first td").each(function(index) {
            var n = $(this).children().find(".fc-day-number").text();
            if (n == '1') {
              corrector = index - 1;
            }
          });

          // Days are identified using a fc-day-X where X is the position of the number
          // in the calendar, starting by 0
          var cell_class = element.attr('class').split(' ').slice(-1).toString();
          var cell_num = parseInt(cell_class.replace('fc-day', ''));

          var cell_bk = element.css('background-color');
          var calendar_day = corrector + cell_num;
          var event_cell = ".view-calendar-for-events table td.fc-day" + calendar_day;

          $(event_cell).css("color", "#FFF");
          // For days with multiple events,
          if ($(event_cell).hasClass("with-event")) {
            if ($(event_cell).css("background") != cell_bk) {
              // Use gray to indicate that there are at least two different events for this day.
              $(event_cell).css("background", "#6A697B");
            }
          }
          else  {

            months_map = {
              'fc-month-january': "01",
              'fc-month-february': "02",
              'fc-month-march': "03",
              'fc-month-april': "04",
              'fc-month-may': "05",
              'fc-month-june': "06",
              'fc-month-july': "07",
              'fc-month-august': "08",
              'fc-month-september': "09",
              'fc-month-october': "10",
              'fc-month-november': "11",
              'fc-month-december': "12"
            };

            $(event_cell).bind('click', function () {
              $(".view-events-for-day").load("/calendar_ajax/day/" + current_year + '-' + months_map[current_month.toLowerCase()] + '-' + cell_num);
            });

            $(event_cell).css("background", cell_bk);
          }
          $(event_cell).addClass("with-event");
        }
      }
    };

    // Merge in our settings.
    $.extend(options, settings.fullcalendar);

    // Pull in overrides from URL.
    if (settings.date) {
      $.extend(options, settings.date);
    }

    return options;
  }
};

}

}(jQuery));