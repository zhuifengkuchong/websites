(function (j$) {
    j$(document).ready(function () {
        // -- Cookie Code -- //
        var normalTextSize = 12;
        var oneSizeUpTextSize = 14;
        var twoSizesUpTextSize = 17;
        //units in which the text size is determined
        var textSizeUnits = "px";
        //in days, number of days to persist the textsize cookie
        var cookieLife = 365; //changed from 365 to 1 per client request 1/12/2010
        // name of the cookie
        var cookieName = "textsizestyle";
        var cookieNameClasses = "whichButtonPressed";


        //Searchbox default text
        j$("#header_search input[type*='text']").val("Enter a Keyword").attr("style", "color:#C2C2C2;");
        j$("#header_search input[type*='text']").focus(function () {
            if (j$("#header_search input[type*='text']").val() == "Enter a Keyword") {
                j$("#header_search input[type*='text']").val("").attr("style", "color:#000;");
            }
        });
        j$("#header_search input[type*='text']").blur(function () {
            if (j$("#header_search input[type*='text']").val() == "") {
                j$("#header_search input[type*='text']").val("Enter a Keyword").attr("style", "color:#C2C2C2;");
            }
        });


        // Global "Enter" keypress override of default <form> action to submit.
        j$("input").keypress(function (event) {
            if (event.which === 13) {
                event.preventDefault();
            }
        });

		/*Commercial Order Form Error     Page_IsValid only exists on certain pages*/
		if (typeof Page_IsValid != "undefined" && Page_IsValid === false) {
			jQuery(':checkbox').css({'outline-color':'#cc3333','outline-width':'2px','outline-style':'solid'}); 
		};
		
		/* Check if IE then IE version, outline style not available in IE7  */
		jQuery(".CommandButton").click(function(){
			var isIE=jQuery.browser.msie;
			var ieVersion=parseInt(jQuery.browser.version, 10);
  
			if (isIE) { 
				if (jQuery(':checkbox').not(':checked')) {
					var myStyle={};
					if (ieVersion === 7) {
						myStyle={'border-color':'#cc3333','border-width':'2px','border-style':'solid'};
					} else if (ieVersion === 8) {
						myStyle={'outline-color':'#cc3333','outline-width':'2px','outline-style':'solid'};  
					}
				jQuery(":checkbox").css(myStyle);
				}	
			}
		});


        //Fix for clicking "find" and having it search on "Enter a Keyword". Blanks out search field if that text is there.
        j$("#header #header_search a").click(function (event) {
            event.preventDefault();
            if (j$("#header_search input[type*='text']").val() == "Enter a Keyword") {
                j$("#header_search input[type*='text']").val("");
            }
            window.location = j$("#header #header_search a").attr("href");
        });

        // -- Text Resizer Code -- //
        checkCookie();
        j$("#text_resize a").click(function (event) {
            j$(".text_here").removeClass("text_here");
            j$(this).addClass("text_here");
        });
        j$("#smallFont").click(function (event) {
            j$("#text_resize a").attr("style", "text-decoration:none;");
            j$("#smallFont").attr("style", "text-decoration:underline;");
            setPageFontSize(normalTextSize);
            setCookie(normalTextSize, "smallFont");
        });
        j$("#mediumFont").click(function (event) {
            j$("#text_resize a").attr("style", "text-decoration:none;");
            j$("#mediumFont").attr("style", "text-decoration:underline;");
            setPageFontSize(oneSizeUpTextSize);
            setCookie(oneSizeUpTextSize, "mediumFont");
        });
        j$("#largeFont").click(function (event) {
            j$("#text_resize a").attr("style", "text-decoration:none;");
            j$("#largeFont").attr("style", "text-decoration:underline;");
            setPageFontSize(twoSizesUpTextSize);
            setCookie(twoSizesUpTextSize, "largeFont");
        });

        function createCookie(name, value, days) {
            if (days) {
                var date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                var expires = "; expires=" + date.toGMTString();
            }
            else var expires = "";
            document.cookie = name + "=" + value + expires + "; path=/";
        }

        function readCookie(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
            }
            return null;
        }

        function setCookie(theSize, theButton) {
            createCookie(cookieName, theSize, cookieLife);
            createCookie(cookieNameClasses, theButton, cookieLife);
        }

        function setPageFontSize(theSize) {
            document.body.style.fontSize = theSize + textSizeUnits;

            var h2s = document.getElementsByTagName("h2");
            for (var index = 0; index < h2s.length; index++) {
                h2s[index].style.fontSize = (parseInt(theSize) + 8).toString() + textSizeUnits;
            }
            var h3s = document.getElementsByTagName("h3");
            for (var index = 0; index < h3s.length; index++) {
                h3s[index].style.fontSize = (parseInt(theSize) + 2).toString() + textSizeUnits;
            }
            var h4s = document.getElementsByTagName("h4");
            for (var index = 0; index < h4s.length; index++) {
                h4s[index].style.fontSize = (parseInt(theSize) + 1).toString() + textSizeUnits;
            }
            var h5s = document.getElementsByTagName("h5");
            for (var index = 0; index < h5s.length; index++) {
                h5s[index].style.fontSize = (parseInt(theSize) + 0).toString() + textSizeUnits;
            }
        }
        function setButtonHereClass(theButton) {
            addClass(document.getElementById(theButton), "text_here");
        }
        function checkCookie() {
            if (readCookie(cookieName) != null) {
                setPageFontSize(readCookie(cookieName));
            }
            else {
                setPageFontSize(normalTextSize);
                j$("#smallFont").attr("style", "text-decoration:underline;")
            }
            if (readCookie(cookieNameClasses) != null) {
                setButtonHereClass(readCookie(cookieNameClasses));
            }
            else {
                setButtonHereClass("smallFont");
                j$("#text_resize").attr("style", "text-decoration:none;")
                j$("#smallFont").attr("style", "text-decoration:underline;")
            }
        }
        function addClass(element, value) {
            if (element) {
                if (!element.className) {
                    element.className = value;
                }
                else {
                    newClassName = element.className;
                    newClassName += " ";
                    newClassName += value;
                    element.className = newClassName;
                }
            }
        }


        //Initializes the rotator, generates the nav buttons, and adds listeners to the page left/right buttons.
        j$('#img_panels > div').cycle({
            fx: 'scrollHorz',
            easing: 'easeOutExpo',
            timeout: 10000,
            next: '#img_right',
            prev: '#img_left',
            pager: '#img_nav',
            pagerAnchorBuilder: function (idx, slide) {
                return '<li></li>';
            }
        });

        //Enables clicking on the video panel to pause the rotator and clicking on the navigation to resume it.
        /*j$('#img_panels > div').click(function () {
        j$('#img_panels > div').cycle('pause');
        });*/
        //Pause function moved below for youtube api -JB
        j$('#img_nav > li').click(function () {
            j$('#img_panels > div').cycle('resume');
        });
        j$('#img_left').click(function () {
            j$('#img_panels > div').cycle('resume');
        });
        j$('#img_right').click(function () {
            j$('#img_panels > div').cycle('resume');
        });

        //Unhides image rotator nav buttons
        j$('#img_nav').show();

        //Sets page left/right buttons as visible, fades buttons up on approach.
        j$('.pager_btn').fadeTo(0.00);
        j$('.pager_btn').approach({ opacity: 1 }, 150);

        //Toggles the play video image for the flood insurance page on hover, and starts the video on click.
        j$('#floodVideoImage').hover(function (event) {
            j$(this).attr('src', '../../../../0/images/Flood/ClickplayOver.jpg'/*tpa=http://www.auto-owners.com/Portals/0/images/Flood/ClickplayOver.jpg*/);
        }, function (event) {
            j$(this).attr('src', '../../../../0/images/Flood/Clickplay.jpg'/*tpa=http://www.auto-owners.com/Portals/0/images/Flood/Clickplay.jpg*/);
        });
        j$('#floodVideoImage').click(function () {
            j$(this).parent().html('<iframe style="float: right; height:350px; width: 220px;" src="http://www.fema.gov/help/widgets/countdown.html" frameborder="0" scrolling="no" marginwidth="0"></iframe>');
            j$('#mymovie').live('click', function (event) {
                alert(j$(this).attr('id'));
            });
        });
    });
})(jQuery.noConflict());

function new_window(url, winwidth, winheight, winleft, wintop) {
    parameters = "toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=1,width="+winwidth+",height="+winheight+",left="+winleft+",top="+wintop;
    link = window.open(url,"Link",parameters);
}



//youtube playing items
function onytplayerStateChange(newState) {
    switch (newState) {
        case 1:
        case 3:
            // Video has begun playing/buffering
            jQuery('#img_panels > div').cycle('pause');
			// Google Analytics for tracking playing the rotator video
            _gaq.push(['_trackEvent', 'RotatorPaperlessYouTubePlay', 'PlayVideo', 'Paperless YouTube Play']);
            break;
        case 2:
        case 0:
            // Video has been paused/ended
            jQuery('#img_panels > div').cycle('resume');
			break;
    }
}

function onW82TXTplayerStateChange1(newState) {
    switch (newState) {
        case 1:
        case 3:
            // Video has begun playing/buffering
            //jQuery('#videoTable').hide();
            jQuery('.youtube_label').hide();
            jQuery('.youtube_tr').height(0);
            jQuery('#youtube_video_W82TXT_2').hide();
            jQuery('#youtube_video_W82TXT_1').width(580);
            jQuery('#youtube_video_W82TXT_1').height(350);
            break;
        case 2:
        case 0:
            // Video has been paused/ended
            jQuery('.youtube_label').show();
            jQuery('#youtube_video_W82TXT_2').show();
            jQuery('.youtube_tr').height(210);
            jQuery('#youtube_video_W82TXT_1').width(300);
            jQuery('#youtube_video_W82TXT_1').height(197);
            break;
    }
}

//W82TXT player event handler
function onW82TXTplayerStateChange2(newState) {
    switch (newState) {
        case 1:
        case 3:
            // Video has begun playing/buffering
            //jQuery('#videoTable').hide();
            jQuery('.youtube_label').hide();
            jQuery('.youtube_tr').height(0);
            jQuery('#youtube_video_W82TXT_1').hide();
            jQuery('#youtube_video_W82TXT_2').width(580);
            jQuery('#youtube_video_W82TXT_2').height(350);
            break;
        case 2:
        case 0:
            // Video has been paused/ended
            jQuery('.youtube_label').show();
            jQuery('#youtube_video_W82TXT_1').show();
            jQuery('.youtube_tr').height(210);
            jQuery('#youtube_video_W82TXT_2').width(300);
            jQuery('#youtube_video_W82TXT_2').height(197);
            break;
    }
}

//W82txt Playlist event handler
function onW82TXTplaylistStateChange(newState) {
	switch (newState) {
		case 1:
			//Video has begun playing/buffering
			_gaq.push(['_trackEvent', 'W82txtYouTubePlaylistPlay', 'PlayVideo', 'Paperless YouTube Play']);
            break;
		case 2:
		case 0:
			//Video has been paused/ended
			break;
		case 3:
			//Video is buffering
	}
}

function onYouTubePlayerReady(playerId) {
    if (playerId == "player1") {
        ytplayer = document.getElementById("youtube_video");
        if (ytplayer.addEventListener) {
            ytplayer.addEventListener("onStateChange", "onytplayerStateChange", false);
        }
        else if (ytplayer.attachEvent) { //IE Fix
            ytplayer.attachEvent("onStateChange", "onytplayerStateChange");
        }
    }
    else if (playerId == "player2") {
        ytplayer = document.getElementById("youtube_video_W82TXT_1");
        if (ytplayer.addEventListener) {
            ytplayer.addEventListener("onStateChange", "onW82TXTplayerStateChange1", false);
        }
        else if (ytplayer.attachEvent) {
            ytplayer.attachEvent("onStateChange", "onW82TXTplayerStateChange1");
        }
    }
    else if (playerId == "player3") {
        ytplayer = document.getElementById("youtube_video_W82TXT_2");
        if (ytplayer.addEventListener) {
            ytplayer.addEventListener("onStateChange", "onW82TXTplayerStateChange2", false);
        }
        else if (ytplayer.attachEvent) {
            ytplayer.attachEvent("onStateChange", "onW82TXTplayerStateChange2");
        }
    }
	else if (playerId == "player7") {
		ytplayer = document.getElementById("youtube_video_W82TXT_Playlist");
		if (ytplayer.addEventListener) {
            ytplayer.addEventListener("onStateChange", "onW82TXTplaylistStateChange", false);
        }
        else if (ytplayer.attachEvent) {
            ytplayer.attachEvent("onStateChange", "onW82TXTplaylistStateChange");
        }
	}
}

/*Fixes weird IE error*/
var setRemoveCallback = function() {
	__flash__removeCallback = function(instance, name) {
		if(instance) {
			instance[name] = null;
		}
	};
	window.setTimeout(setRemoveCallback, 10);
};
setRemoveCallback();

(function(){
//Rotator Pane namespace
window.RP = window.RP || {};
})();

/**
* RP.Video Video object inside the image rotator.
* API: RP.Video.initialize.
**/
RP.Video = (function () {
	var initialize = function () {
		jQuery('.rotator_video_player').bind("playing", stopRotating);
		jQuery('.rotator_video_player').bind("ended", startRotating);
		jQuery('.rotator_video_player').bind("pause", startRotating);
		jQuery(document).bind('webkitfullscreenchange mozfullscreenchange fullscreenchange',fullScreenChanged);
	};
	
	var stopRotating = function () {
		jQuery('#img_panels > div').cycle('pause');
	};
	
	var startRotating = function () {
		if(document.mozFullScreenElement == null && document.webkitFullscreenElement == null && document.fullScreenElement == null)
		{
			jQuery('#img_panels > div').cycle('resume');
		}
	};
	
	var fullScreenChanged = function (e) {
		if(document.mozFullScreenElement != null || document.webkitFullscreenElement != null || document.fullScreenElement != null)
		{
			stopRotating();
		}
	
		if(document.mozFullScreenElement == null && document.webkitFullscreenElement == null && document.fullScreenElement == null)
		{
			if(jQuery('.rotator_video_player').get(0).paused || jQuery('.rotator_video_player').get(0).ended)
			{
				startRotating();
			}
		}
	};
	
	return {
		initialize: initialize
	};
})();
/**
* RP.Controls Controls object inside the image rotator.
* API: RP.Controls.initialize.
**/
RP.Controls = (function () {
	var initialize = function () {
		jQuery('#youtubeLink').click(allowYoutubeClick);
		jQuery('.rotator_video_player').click(blockYoutubeClick);
	};
	
	var blockYoutubeClick = function () {
		jQuery('#youtubeLink').removeAttr('href');
	};
	
	var allowYoutubeClick = function (e) {
		if(e.target.className !== 'rotator_video_player')
		{
			jQuery('#youtubeLink').attr('href','http://www.youtube.com/AutoOwnersInsurance');
		}
	};
	
	return {
		initialize: initialize
	};
})();

(function () {
	//Initializing RP.Video object on window load
	jQuery(document).ready(RP.Video.initialize);
	//Initializing RP.Controls object on window load
	jQuery(document).ready(RP.Controls.initialize);
})();
