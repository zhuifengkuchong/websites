var _gaq = _gaq || [];
	    _gaq.push(['_setAccount', 'UA-20251315-1']);
	    _gaq.push(['_setDomainName', '.auto-owners.com']);
	    _gaq.push(['_trackPageview']);
	    (function () {
	        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www/') + '.google-analytics.com/ga.js';
	        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	    })();
jQuery( document ).ready(function() {
	(function(){
		jQuery(".rotatorProxyLink").bind('click', function(event){
			event.preventDefault();
			_gaq.push(['_trackEvent', 'RotatorProxyLink', 'LinkClick', jQuery(event.target).html().trim()]);
			
			setTimeout(function(){
				window.location.href = jQuery(".rotatorProxyLink").attr('href');
			}, 500);
		});
		
		jQuery(".corpInfoProxyLink").bind('click', function(event){
			event.preventDefault();
			_gaq.push(['_trackEvent', 'CorpInfoProxyLink', 'LinkClick', jQuery(event.target).html().trim()]);
			
			setTimeout(function(){
				window.location.href = jQuery(".corpInfoProxyLink").attr('href');
			}, 500);
		});
		
		jQuery(".youtube_rotator #youtube-play").bind('click', function(event){
			event.preventDefault();
			_gaq.push(['_trackEvent', 'RotatorPaperlessYouTubeLink', 'LinkClick', jQuery(event.target).html().trim()]);
			
			setTimeout(function(){
				window.location.href = jQuery(".youtube_rotator #youtube-play").attr('href');
			}, 500);
		});
		
		jQuery(".rotatorLifeInsuranceLink").bind('click', function(event){
			event.preventDefault();
			_gaq.push(['_trackEvent', 'RotatorLifeInsuranceLink', 'LinkClick', jQuery(event.target).html().trim()]);
			
			setTimeout(function(){
				window.location.href = jQuery(".rotatorLifeInsuranceLink").attr('href');
			}, 500);
		});
		
		jQuery(".rotatorW8t2txtLink").bind('click', function(event){
			event.preventDefault();
			_gaq.push(['_trackEvent', 'RotatorW8t2txtLink', 'LinkClick', jQuery(event.target).html().trim()]);
			
			setTimeout(function(){
				window.location.href = jQuery(".rotatorW8t2txtLink").attr('href');
			}, 500);
		});
		
		jQuery(".rotatorOnlineBillsLink").bind('click', function(event){
			event.preventDefault();
			_gaq.push(['_trackEvent', 'RotatorOnlineBillsLink', 'LinkClick', jQuery(event.target).html().trim()]);
			
			setTimeout(function(){
				window.location.href = jQuery(".rotatorOnlineBillsLink").attr('href');
			}, 500);
		});
	})()
});