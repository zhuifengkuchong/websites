/*---------------------

PRINT CONTENT written to grab content from a page's content pane
Open it in a new, unstyled window, and print

----------------------*/
function printcontent(url) {
  url = url.replace(/ /g, "-");
  var disp_setting="toolbar=no,location=no,directories=yes,menubar=no,scrollbars=yes,width=800,height=600,left=10,top=10"; 
  var content_vlue = document.getElementById("print_content").innerHTML;

  var docprint=window.open("","",disp_setting); 
   docprint.document.open(); 
   docprint.document.write('<html><head><title>Auto-Owners Insurance Printable Page</title>'); 
   docprint.document.write('<link rel="Stylesheet" type="text/css" href="../includes/print.css"/*tpa=http://www.auto-owners.com/Portals/_default/Skins/AutoOwners_New/includes/print.css*/>');
   docprint.document.write('<link rel="stylesheet" type="text/css" href="../skin.css"/*tpa=http://www.auto-owners.com/Portals/_default/Skins/AutoOwners_New/skin.css*/ />');
   docprint.document.write('</head><body style="padding:10px;">');
   docprint.document.write('<a id="printlink" href="javascript:window.print();">Print&nbsp;&nbsp;&nbsp;<img src="../../../../../images/print.gif"/*tpa=http://www.auto-owners.com/images/print.gif*/ border="0" alt="Print" title="Print" /></a>');
   docprint.document.write('<div style=""><strong style="display:block; clear:both;">This Page\'s Address:</strong> http://www.auto-owners.com/' + url + '<br /><br /></div>');
   docprint.document.write('<div id="content">');
   docprint.document.write(content_vlue);
   docprint.document.write('</div></body></html>'); 
   docprint.document.close(); 
   docprint.focus();
}