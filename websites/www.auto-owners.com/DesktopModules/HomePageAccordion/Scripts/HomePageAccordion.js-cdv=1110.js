jQuery(function ($) {
    $(document).ready(function () {

        // Error Message Constants
        var INVALID_ZIP_CODE_MESSAGE = "A valid Zip Code is required.";
        var NON_AO_ZIP_CODE_MESSAGE = "Auto-Owners does not operate in the state of the Zip Code provided. Please try entering a different Zip Code.";
        var NON_PWQ_ZIP_CODE_MESSAGE = "Quoting is not available for the Zip Code provided. Please click <a href='http://agency.auto-owners.com/?adv=true'>here</a> to find an agent.";
        var ZIP_CODE_VALIDATION_ERROR = "An error has occurred validating the Zip Code provided. Please try again later.";
        var ADDRESS_VALIDATION_ERROR = "An error has occurred validating the Address provided. Please try again later.";
        var isMouseOverFindError = false;
        var isMouseOverQuoteError = false;

        // Accordion Setup
        $("#accordion_panel").accordion({ icons: { 'header': 'icon_closed', 'headerSelected': 'icon_open' }, fillspace: true });


        // ----- Public Quoting ----- //

        // Default Text
        $("#start_quote").val("Enter a Zip Code").attr("style", "color:#C2C2C2;");
        $("#start_quote").focus(function () {
            if ($("#start_quote").val() == "Enter a Zip Code") {
                $("#start_quote").val("").attr("style", "color:#000;");
            }
        });
        $("#start_quote").blur(function () {
            if ($("#start_quote").val() == "") {
                $("#start_quote").val("Enter a Zip Code").attr("style", "color:#C2C2C2;");
                $("#quote_zip_error").hide();
            }
        });

        // Keypress fix for the start a quote Module
        $("#start_quote").keypress(function (event) {
            if (event.which == 13) {
                event.preventDefault();
                event.stopPropagation();
                submitQuoteZipCode(event);
            }
        });

        $("#quote_panel .submit_quote").click(function (event) {
            event.preventDefault();
            event.stopPropagation();
            submitQuoteZipCode(event);
        });

        var submitQuoteZipCode = function (event) {
            hideQuoteZipCodeErrorMessage();
            var zipCode = $("#start_quote").val();

            if (isValidZipCode(zipCode)) {
                isUSZipCode(zipCode, function (result) {
                    if (result.d === true) {
                        isAutoOwnersZipCode(zipCode, function (result) {
                            if (result.d === true) {
                                isQuoteZipCodeOptedIn(zipCode, function (result) {
                                    if (result.d === true) {
                                        window.location.href = "http://agency.auto-owners.com/QuoteSearch.aspx?z=" + zipCode;
                                    } else {
                                        showQuoteZipCodeErrorMessage(NON_PWQ_ZIP_CODE_MESSAGE);
                                    }
                                }, function (response) {
                                    showQuoteZipCodeErrorMessage(ZIP_CODE_VALIDATION_ERROR);
                                });
                            } else {
                                showQuoteZipCodeErrorMessage(NON_AO_ZIP_CODE_MESSAGE);
                            }
                        }, function (response) {
                            showQuoteZipCodeErrorMessage(ZIP_CODE_VALIDATION_ERROR);
                        });
                    } else {
                        showQuoteZipCodeErrorMessage(INVALID_ZIP_CODE_MESSAGE);
                    }
                }, function (response) {
                    showQuoteZipCodeErrorMessage(ZIP_CODE_VALIDATION_ERROR);
                });
            } else {
                showQuoteZipCodeErrorMessage(INVALID_ZIP_CODE_MESSAGE);
            }
        };

        var showQuoteZipCodeErrorMessage = function (errorMessage) {
            $("#quote_zip_error_message").html(errorMessage);
            $("#quote_zip_error").show();
        };

        var hideQuoteZipCodeErrorMessage = function () {
            $("#quote_zip_error").hide();
            $("#quote_zip_error_message").html('');
        };


        // ----- Agency Locator ----- //

        // Default Text
        $("#find_agency").val("Enter a Zip Code").attr("style", "color:#C2C2C2;");
        $("#find_agency").focus(function () {
            if ($("#find_agency").val() == "Enter a Zip Code") {
                $("#find_agency").val("").attr("style", "color:#000;");
            }
        });
        $("#find_agency").blur(function () {
            if ($("#find_agency").val() == "") {
                $("#find_agency").val("Enter a Zip Code").attr("style", "color:#C2C2C2;");
                $("#find_zip_error").hide();
            }
        });

        // Keypress fix for the agency locator Module
        $("#find_agency").keypress(function (event) {
            if (event.which == 13) {
                event.preventDefault();
                event.stopPropagation();
                submitFindZipCode(event);
            }
        });

        $("#find_panel .submit_find").click(function (event) {
            event.preventDefault();
            event.stopPropagation();
            submitFindZipCode(event);
        });

        var submitFindZipCode = function (event) {
            hideFindZipCodeErrorMessage();
            var zipCode = $("#find_agency").val();

            if (isValidZipCode(zipCode)) {
                isUSZipCode(zipCode, function (result) {
                    if (result.d === true) {
                        isAutoOwnersZipCode(zipCode, function (result) {
                            if (result.d === true) {
                                window.location.href = "http://agency.auto-owners.com/?z=" + zipCode;
                            } else {
                                showFindZipCodeErrorMessage(NON_AO_ZIP_CODE_MESSAGE);
                            }
                        }, function (response) {
                            showFindZipCodeErrorMessage(ZIP_CODE_VALIDATION_ERROR);
                        });
                    } else {
                        showFindZipCodeErrorMessage(INVALID_ZIP_CODE_MESSAGE);
                    }
                }, function (response) {
                    showFindZipCodeErrorMessage(ZIP_CODE_VALIDATION_ERROR);
                });
            } else {
                showFindZipCodeErrorMessage(INVALID_ZIP_CODE_MESSAGE);
            }
        };

        // Shows the Find a Local Agency error message
        var showFindZipCodeErrorMessage = function (errorMessage) {
            $("#find_zip_error_message").html(errorMessage);
            $("#find_zip_error").show();
        };

        // Hides the Find a Local Agency error message
        var hideFindZipCodeErrorMessage = function () {
            $("#find_zip_error").hide();
            $("#find_zip_error_message").html('');
        };


        // US Zip Code Validation
        var isUSZipCode = function (zipCode, successHandler, failureHandler) {
            $.ajax({
                type: "GET",
                url: "/Portals/0/AgencyValidationClientService.svc/ValidateUSZipCode",
                data: 'zipCode=' + zipCode,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    if (successHandler) {
                        successHandler(result);
                    }
                },
                error: function (response) {
                    if (failureHandler) {
                        failureHandler(response);
                    }
                }
            });
        };

        // AO State Zip Code Validation
        var isAutoOwnersZipCode = function (zipCode, successHandler, failureHandler) {
            $.ajax({
                type: "GET",
                url: "/Portals/0/AgencyValidationClientService.svc/ValidateAutoOwnersZipCode",
                data: 'zipCode=' + zipCode,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    if (successHandler) {
                        successHandler(result);
                    }
                },
                error: function (response) {
                    if (failureHandler) {
                        failureHandler(response);
                    }
                }
            });
        };


        // PWQ State Zip Code Validation
        var isQuoteZipCodeOptedIn = function (zipCode, successHandler, failureHandler) {
            $.ajax({
                type: "GET",
                url: "/Portals/0/AgencyValidationClientService.svc/ValidatePublicQuotingZipCode",
                data: 'zipCode=' + zipCode,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    if (successHandler) {
                        successHandler(result);
                    }
                },
                error: function (response) {
                    if (failureHandler) {
                        failureHandler(response);
                    }
                }
            });
        };


        // Zip Code Regex Validation
        var isValidZipCode = function (zipCode) {
            var isValid = false;
            var regex = /^\d{5}|\D\d\D \d\D\d$/;

            if (regex.test(zipCode)) {
                isValid = true;
            } else if ('' === zipCode) {
                isValid = true;
            }

            return isValid;
        };

        // Hide error message on click or 'esc'
        $(document).keyup(function (event) {
            if (event.keyCode == 27) {
                hideQuoteZipCodeErrorMessage();
            }
        });
        $(document).mouseup(function (event) {
            if ($("#quote_zip_error").is(":visible")) {
                if (!isMouseOverQuoteError) {
                    hideQuoteZipCodeErrorMessage();
                }
            }
            if ($("#find_zip_error").is(":visible")) {
                if (!isMouseOverFindError) {
                    hideFindZipCodeErrorMessage();
                }
            }
        });
        $("#find_zip_error_message").hover(function (event) { isMouseOverFindError = true; }, function (event) { isMouseOverFindError = false; });
        $("#quote_zip_error_message").hover(function (event) { isMouseOverQuoteError = true; }, function (event) { isMouseOverQuoteError = false; });
    });
});