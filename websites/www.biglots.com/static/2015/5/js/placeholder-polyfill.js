$.fn.placeholder = function(options) {
	this.each(function() {
		var element = $(this);
	    
	    $.extend({
	        value: '',
	        cssClass: ''
	    }, options);

	    if (element.val() === '') {
	    	element.val(options.value);
	    	element.addClass(options.cssClass);
	    }
	    
	    element.focus(function() {
	        if (element.val() === options.value) {
	        	element.val('');
	        	element.removeClass(options.cssClass);
	        }
	    });
	    
	    element.blur(function() {
	        if (element.val() === '' || element.val() === options.value) {
	        	element.val(options.value);
	        	element.addClass(options.cssClass);
	        }
	    });
	})
}

$(function() {
	$('input[type=text], input[type=search]').each(function() {
		var placeholder = $(this).attr('placeholder');
		if (placeholder) {
			$(this).placeholder({value: placeholder, cssClass:'placeholder'});
		}
	});
});