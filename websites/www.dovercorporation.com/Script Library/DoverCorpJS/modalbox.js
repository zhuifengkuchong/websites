// Modal Dialog Box
// copyright 8th July 2006 by Stephen Chapman
// http://javascript.about.com/
// permission to use this Javascript on your web page is granted
// provided that all of the code in this script (including these
// comments) is used without any alteration
function mbox_pageWidth() 
{
    return window.innerWidth != null? window.innerWidth: document.documentElement && document.documentElement.clientWidth ? document.documentElement.clientWidth:document.body != null? document.body.clientWidth:null;
}

function mbox_pageHeight() 
{
    return window.innerHeight != null? window.innerHeight: document.documentElement && document.documentElement.clientHeight ? document.documentElement.clientHeight:document.body != null? document.body.clientHeight:null;
}

function mbox_posLeft() 
{
    return typeof window.pageXOffset != 'undefined' ? window.pageXOffset:document.documentElement && document.documentElement.scrollLeft? document.documentElement.scrollLeft:document.body.scrollLeft? document.body.scrollLeft:0;
}

function mbox_posTop() 
{
    return typeof window.pageYOffset != 'undefined' ? window.pageYOffset:document.documentElement && document.documentElement.scrollTop? document.documentElement.scrollTop: document.body.scrollTop?document.body.scrollTop:0;
}

function o(x){return document.getElementById(x);}

var height = 0, width = 0;
function scrollFix()
{
    var p='px';
    var obol=o('ol');
    obol.style.top=mbox_posTop()+'px';
    obol.style.left=mbox_posLeft()+'px';
    
    var tp=mbox_posTop()+((mbox_pageHeight()-height)/2)-12;
    var lt=mbox_posLeft()+((mbox_pageWidth()-width)/2)-12;
    var obbx=o('mbox');
    obbx.style.top=(tp<0?0:tp)+p;
    obbx.style.left=(lt<0?0:lt)+p;
}

function sizeFix()
{
    var p='px';
    var obol=o('ol');
    obol.style.height=mbox_pageHeight()+'px';
    obol.style.width=mbox_pageWidth()+'px';
    
    var tp=mbox_posTop()+((mbox_pageHeight()-height)/2)-12;
    var lt=mbox_posLeft()+((mbox_pageWidth()-width)/2)-12;
    var obbx=o('mbox');
    obbx.style.top=(tp<0?0:tp)+p;
    obbx.style.left=(lt<0?0:lt)+p;
}

function kp(e)
{
    ky=e?e.which:event.keyCode;
    if(ky==88||ky==120)hm();
    return false;
}

function inf(h)
{
    tag=document.getElementsByTagName('select');
    for(i=tag.length-1;i>=0;i--)
        tag[i].style.visibility=h;
    tag=document.getElementsByTagName('iframe');
    for(i=tag.length-1;i>=0;i--)
        tag[i].style.visibility=h;
    tag=document.getElementsByTagName('object');
    for(i=tag.length-1;i>=0;i--)
        tag[i].style.visibility=h;
}

function sm(obl, wd, ht)
{
    var h='hidden';
    var b='block';
    var p='px';
    var obol=o('ol'); 
    var obbxd = o('mbd');
    obbxd.innerHTML = o(obl).innerHTML;
    
    obol.style.height=mbox_pageHeight()+p;
    obol.style.width=mbox_pageWidth()+p;
    obol.style.top=mbox_posTop()+p;
    obol.style.left=mbox_posLeft()+p;
    obol.style.display=b;
    var tp=mbox_posTop()+((mbox_pageHeight()-ht)/2)-12;
    var lt=mbox_posLeft()+((mbox_pageWidth()-wd)/2)-12;
    var obbx=o('mbox');
    obbx.style.top=(tp<0?0:tp)+p;
    obbx.style.left=(lt<0?0:lt)+p;
    obbx.style.width=wd+p;
    obbx.style.height=ht+p;
    inf(h);
    obbx.style.display=b;
    
    height = ht;
    width = wd;
}

function hm()
{
    var v='visible';
    var n='none';
    o('ol').style.display=n;
    o('mbox').style.display=n;
    inf(v);
    document.onkeypress='';
}

function initmb()
{
    var ab='absolute';
    var n='none';
    var obody=document.getElementsByTagName('body')[0];
    var frag=document.createDocumentFragment();
    var obol=document.createElement('div');
    obol.setAttribute('id','ol');
    obol.style.display=n;
    obol.style.position=ab;
    obol.style.top=0;
    obol.style.left=0;
    obol.style.zIndex=998;
    obol.style.width='100%';
    frag.appendChild(obol);
    var obbx=document.createElement('div');
    obbx.setAttribute('id','mbox');
    obbx.style.display=n;
    obbx.style.position=ab;
    obbx.style.zIndex=999;
    
    var obl=document.createElement('span');
    obbx.appendChild(obl);
    var obbxd=document.createElement('div');
    obbxd.setAttribute('id','mbd');
    obl.appendChild(obbxd);
    frag.insertBefore(obbx,obol.nextSibling);
    obody.insertBefore(frag,obody.firstChild);
    window.onscroll = scrollFix; 
    window.onresize = sizeFix;
}
