 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../../www.google-analytics.com/analytics.js'/*tpa=http://www.google-analytics.com/analytics.js*/,'ga');

  ga('create', 'UA-27710058-1', 'http://www.dovercorporation.com/Script%20Library/DoverCorpJS/dovercorporation.com');
  ga('send', 'pageview');

$(function() {

	$.fn.swapSubmit = function() {
		var targ_nod = $(this);
		var targID_str = targ_nod.attr("id");
		$("<a href='#' id='"+targID_str+"-pic'><span></span>Submit</a>").insertAfter(this).click(function(evt) {
			evt.preventDefault();
			targ_nod.click();
		});
		targ_nod.css("display","none");
	}


	$("#primary-nav a").removeAttr("title");
	$("#snav-search-sbmit").swapSubmit();
	$("#snav-searchfield").focus(function() {
		$(this).siblings("label").css({display:"none"});
	});
	$("#snav-searchfield").blur(function() {
		if($(this).val()=="") {
			$(this).siblings("label").css({display:"block"});
		}
	});

//	var _gaq = _gaq || [];
//  _gaq.push(['_setAccount', 'UA-27212040-1']);
//  _gaq.push(['_trackPageview']);
//  (function() {
//		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
//		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www/') + '.google-analytics.com/ga.js';
//		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
//  })();

});

