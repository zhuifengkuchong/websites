//------------------------START FOR DOVERCORP----------------------------//

var youtubePopupStatus = 0;
$(document).ready(function () {
    $(".TabContent tr").each(function () {
        var indexOfVedio = $(".TabContent tr").index(this);
        if (indexOfVedio > 1 && indexOfVedio%2==0) {
            $(this).children("td").css("padding-top","10px");
        }
    });
    //LOADING POPUP
    //----------------------------START----------------------------//
    $(".Videoimg").each(function () {
        $(this).click(function () {
            centerPopupVideo($(".Videoimg").index(this));
            loadPopupVideo($(".Videoimg").index(this));
        });
    });
    $(".VideoTitle").each(function () {
        $(this).click(function () {
            centerPopupVideo($(".VideoTitle").index(this));
            loadPopupVideo($(".VideoTitle").index(this));
        });
    });

    $("#backgroundPopupVideo").click(function () {
        disablePopupVideo();
    });
    $("#YoutubePopupVideoClose").click(function () {
        disablePopupVideo();
    });

    //Press Escape event!
    $(document).keyup(function (e) {
        if (e.keyCode == 27 && youtubePopupStatus == 1) {
            disablePopupVideo();
        }
    });
});


function loadPopupVideo(indexOfPopup) {
    //loads popup only if it is disabled
    if (youtubePopupStatus == 0) {
        $("#backgroundPopupVideo").css({
            "opacity": "0.9"
        });
        $("#backgroundPopupVideo").fadeIn("slow");
        $("#YoutubePopupVideoPlayer").fadeIn("slow");
        youtubePopupStatus = 1;
    }
}

//disabling popup with jQuery magic!
function disablePopupVideo() {
    
    //disables popup only if it is enabled
    if (youtubePopupStatus == 1) {
        $("#backgroundPopupVideo").fadeOut("slow");
        $("#YoutubePopupVideoPlayer").fadeOut("slow");
        $("#YoutubePopupVideoContent").html("");
        youtubePopupStatus = 0;
    }
}

//centering popup
function centerPopupVideo(indexOfPopup) {
    //request data for centering
    var windowWidth = document.documentElement.clientWidth;
    var windowHeight = document.documentElement.clientHeight;
    var getYoutubeURL = $(".VideoLinkForYoutube").eq(indexOfPopup).html();
    
    var youtube = '<iframe width="853" height="480" src="' + getYoutubeURL + '?autoplay=1" frameborder="0"></iframe>';
    //$(".videoContainer").html(youtube);
    $("#YoutubePopupVideoContent").html(youtube);

    var popupHeight = $("#YoutubePopupVideoPlayer").height();
    var popupWidth = $("#YoutubePopupVideoPlayer").width();
    //centering
    $("#YoutubePopupVideoPlayer").css({
        "position": "fixed",
        "top": windowHeight / 2 - popupHeight / 2,
        "left": windowWidth / 2 - popupWidth / 2
        
    });
    //only need force for IE6

    $("#backgroundPopupVideo").css({
        "height": windowHeight
    });

}

//-----------------------END FOR DOVERCORP----------------------//