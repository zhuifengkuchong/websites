function loadjscssfile(filename, filetype){
	if (filetype=="js"){ //if filename is a external JavaScript file
		var fileref=document.createElement('script')
		fileref.setAttribute("type","text/javascript")
		fileref.setAttribute("src", filename)
	}
	else if (filetype=="css"){ //if filename is an external CSS file
		var fileref=document.createElement("link")
		fileref.setAttribute("rel", "stylesheet")
		fileref.setAttribute("type", "text/css")
		fileref.setAttribute("href", filename)
	}
	if (typeof fileref!="undefined")
		document.getElementsByTagName("head")[0].appendChild(fileref)
}
	
loadjscssfile("https://cdnjs.cloudflare.com/ajax/libs/jquery.SPServices/2014.02/jquery.SPServices-2014.02.min.js", "js")

function SetTitlesHeight(){
	"use strict";
	var irEventsContainer,UpconmingEventsPanel,pastEventsTitle,pastEventsTitle,upcomingEventsTitle,pastEventsTitleH,upcomingEventsTitleH;
	irEventsContainer = jQuery("[id*=ListIREvents].ir-event-container");
	if(irEventsContainer.length > 0){
		UpconmingEventsPanel = irEventsContainer.find("[id*=UpconmingEventsPanel].upconming-events-container");
		if(UpconmingEventsPanel.length > 0){			
			pastEventsTitle = jQuery(".past-events-container .ir-event-item-container.ir-event-header");			
			pastEventsTitle.css("min-height","10px");			
			upcomingEventsTitle = jQuery(".upconming-events-container .ir-event-item-container.ir-event-header");
			upcomingEventsTitle.css("min-height","10px");
			pastEventsTitleH = pastEventsTitle.height();
			upcomingEventsTitleH = upcomingEventsTitle.height();
			if(pastEventsTitleH < upcomingEventsTitleH - 10 || pastEventsTitleH >  upcomingEventsTitleH + 10){
				upcomingEventsTitle.height(pastEventsTitleH);
			}
		}
	}
}

function InsertDivLineEarnings(){
	var quarterCont = jQuery("[id*=EarningsContainer]  .quarter-container")
	if(quarterCont.length > 0){		
		for(i= 0; i< quarterCont.length -1 ; i++){
		  jQuery(quarterCont[i]).after("<div><hr></div>");
		}
	}
}


EventPresentation = (function () {
    function EventPresentation() {
		this.ID;
        this.Title;
		this.IsQuarter;
		this.QuarterTitle;
		this.QuarterYear;
		this.Quarter;
		this.ShowTime;
		this.EventDate;
		this.EventTimeZone;
		this.EventDescription;
		this.NewsRelease_icon;
		this.NewsRelease_Link;
		this.NewsRelease_LinkColor;
		this.NewsRelease_InNewWindow;
		this.Slides_icon;
		this.Slides_Link;
		this.Slides_LinkColor;
		this.Slides_InNewWindow;
		this.WebCast_icon;
		this.WebCast_Link;
		this.WebCast_LinkColor;
		this.WebCast_InNewWindow;
		this.Transcript_icon;
		this.Transcript_Link;
		this.Transcript_LinkColor;
		this.Transcript_InNewWindow;
		this.Supplemental_icon;
		this.Supplemental_Link;
		this.Supplemental_LinkColor;
		this.Supplemental_InNewWindow;
		this.Supplemental2_icon;
		this.Supplemental2_Link;
		this.Supplemental2_LinkColor;
		this.Supplemental2_InNewWindow;
		this.Supplemental3_icon;
		this.Supplemental3_Link;
		this.Supplemental3_LinkColor;
		this.Supplemental3_InNewWindow;
		this.Supplemental4_icon;
		this.Supplemental4_Link;
		this.Supplemental4_LinkColor;
		this.Supplemental4_InNewWindow;
		this.Supplemental5_icon;
		this.Supplemental5_Link;
		this.Supplemental5_LinkColor;
		this.Supplemental5_InNewWindow;
    };
	EventPresentation.prototype.SupplementalCount = function () {
		var total = 0;
		total = this.Supplemental_icon ? total+1 : total;
		total = this.Supplemental2_icon ? total+1 : total;
		total = this.Supplemental3_icon ? total+1 : total;
		total = this.Supplemental4_icon ? total+1 : total;
		total = this.Supplemental5_icon ? total+1 : total;
		
		return total;
	}

    return EventPresentation;
})();



 var eventsEntries = [];
function loadEventsPresentations() {
    "use strict";
	//console.log("loadEventsPresentations");
	var context, web, list, viewXml, query, items, itemsCount, item, nItem, i;
	context = new SP.ClientContext.get_current();
	web = context.get_web();
	list = web.get_lists().getByTitle("EventsPresentations");
	viewXml = '<View><RowLimit>1200</RowLimit></View>';
	query = new SP.CamlQuery();
	query.set_viewXml(viewXml);
	items = list.getItems(query);
	context.load(items,"Include(Title,COP_IsQuarter, COP_QuarterTitle, COP_QuarterYear, COP_Quarter, COP_ShowTime, COP_EventDate, COP_EventTimeZone, COP_EventDescription, COP_NewsRelease_icon, COP_NewsRelease_Link, COP_NewsRelease_LinkColor, COP_NewsRelease_InNewWindow, COP_Slides_icon, COP_Slides_Link, COP_Slides_LinkColor,COP_Slides_InNewWindow, COP_WebCast_icon,COP_WebCast_Link, COP_WebCast_LinkColor, COP_WebCast_InNewWindow, COP_Transcript_icon,  COP_Transcript_Link, COP_Transcript_LinkColor, COP_Transcript_InNewWindow, COP_Supplemental_icon, COP_Supplemental_Link, COP_Supplemental_LinkColor, COP_Supplemental_InNewWindow, COP_Supplemental2_icon, COP_Supplemental2_Link, COP_Supplemental2_LinkColor, COP_Supplemental2_InNewWindow, COP_Supplemental3_icon, COP_Supplemental3_Link, COP_Supplemental3_LinkColor, COP_Supplemental3_InNewWindow, COP_Supplemental4_icon, COP_Supplemental4_Link, COP_Supplemental4_LinkColor, COP_Supplemental4_InNewWindow, COP_Supplemental5_icon, COP_Supplemental5_Link, COP_Supplemental5_LinkColor, COP_Supplemental5_InNewWindow, ID)");
	context.add_requestSucceeded(onLoaded);
	context.add_requestFailed(onFailure);
	context.executeQueryAsync();
	function onLoaded() {
	   
		itemsCount = items.get_count();
		for (i = 0; i < itemsCount; i++) {
			item = items.itemAt(i);
			nItem = new EventPresentation();
			nItem.Title = item.get_fieldValues().Title;
			nItem.QuarterTitle = item.get_fieldValues().COP_QuarterTitle;
			nItem.QuarterTitle = nItem.QuarterTitle === null || nItem.QuarterTitle === undefined ? "" : nItem.QuarterTitle;
			nItem.QuarterYear = item.get_fieldValues().COP_QuarterYear;
			nItem.Quarter = item.get_fieldValues().COP_Quarter;
			nItem.IsQuarter = item.get_fieldValues().COP_IsQuarter;
			nItem.ShowTime = item.get_fieldValues().COP_ShowTime;
			nItem.EventDate = item.get_fieldValues().COP_EventDate;
			nItem.EventTimeZone = item.get_fieldValues().COP_EventTimeZone;
			nItem.EventDescription = item.get_fieldValues().COP_EventDescription;
			nItem.NewsRelease_icon = item.get_fieldValues().COP_NewsRelease_icon;
			nItem.NewsRelease_Link = item.get_fieldValues().COP_NewsRelease_Link;
			nItem.NewsRelease_LinkColor = item.get_fieldValues().COP_NewsRelease_LinkColor;
			nItem.NewsRelease_InNewWindow = item.get_fieldValues().COP_NewsRelease_InNewWindow;
			nItem.Slides_icon = item.get_fieldValues().COP_Slides_icon;
			nItem.Slides_Link = item.get_fieldValues().COP_Slides_Link;
			nItem.Slides_LinkColor = item.get_fieldValues().COP_Slides_LinkColor;
			nItem.Slides_InNewWindow = item.get_fieldValues().COP_Slides_InNewWindow;
			nItem.WebCast_icon = item.get_fieldValues().COP_WebCast_icon;
			nItem.WebCast_Link = item.get_fieldValues().COP_WebCast_Link;
			nItem.WebCast_LinkColor = item.get_fieldValues().COP_WebCast_LinkColor;
			nItem.WebCast_InNewWindow = item.get_fieldValues().COP_WebCast_InNewWindow;
			nItem.Transcript_icon = item.get_fieldValues().COP_Transcript_icon;
			nItem.Transcript_Link = item.get_fieldValues().COP_Transcript_Link;
			nItem.Transcript_LinkColor = item.get_fieldValues().COP_Transcript_LinkColor;
			nItem.Transcript_InNewWindow = item.get_fieldValues().COP_Transcript_InNewWindow;
			nItem.Supplemental_icon = item.get_fieldValues().COP_Supplemental_icon;
			nItem.Supplemental_Link = item.get_fieldValues().COP_Supplemental_Link;
			nItem.Supplemental_LinkColor = item.get_fieldValues().COP_Supplemental_LinkColor;
			nItem.Supplemental_InNewWindow = item.get_fieldValues().COP_Supplemental_InNewWindow;
			nItem.Supplemental2_icon = item.get_fieldValues().COP_Supplemental2_icon;
			nItem.Supplemental2_Link = item.get_fieldValues().COP_Supplemental2_Link;
			nItem.Supplemental2_LinkColor = item.get_fieldValues().COP_Supplemental2_LinkColor;
			nItem.Supplemental2_InNewWindow = item.get_fieldValues().COP_Supplemental2_InNewWindow;
			nItem.Supplemental3_icon = item.get_fieldValues().COP_Supplemental3_icon;
			nItem.Supplemental3_Link = item.get_fieldValues().COP_Supplemental3_Link;
			nItem.Supplemental3_LinkColor = item.get_fieldValues().COP_Supplemental3_LinkColor;
			nItem.Supplemental3_InNewWindow = item.get_fieldValues().COP_Supplemental3_InNewWindow;
			nItem.Supplemental4_icon = item.get_fieldValues().COP_Supplemental4_icon;
			nItem.Supplemental4_Link = item.get_fieldValues().COP_Supplemental4_Link;
			nItem.Supplemental4_LinkColor = item.get_fieldValues().COP_Supplemental4_LinkColor;
			nItem.Supplemental4_InNewWindow = item.get_fieldValues().COP_Supplemental4_InNewWindow;
			nItem.Supplemental5_icon = item.get_fieldValues().COP_Supplemental5_icon;
			nItem.Supplemental5_Link = item.get_fieldValues().COP_Supplemental5_Link;
			nItem.Supplemental5_LinkColor = item.get_fieldValues().COP_Supplemental5_LinkColor;
			nItem.Supplemental5_InNewWindow = item.get_fieldValues().COP_Supplemental5_InNewWindow;

			eventsEntries.push(nItem);
		}
		matchEventsPresentation();
	}
	function onFailure(ex) {
		//console.log("Fail! " + ex);	
	}
}

var eventsEntries = [];
function loadEventsPresentationsServices() {
    "use strict";
	//console.log("loadEventsPresentations");
	var   items, itemsCount, item, nItem;
 
	$().SPServices({
    operation: "GetListItems",
    async: true,
	webURL: window.location.pathname.split("/Pages")[0],
    listName: "EventsPresentations",
    CAMLViewFields: 	"<ViewFields><FieldRef Name='Title' /><FieldRef Name='COP_IsQuarter' /><FieldRef Name='COP_QuarterTitle' /><FieldRef Name='COP_EventDate' /> <FieldRef Name='COP_EventDescription' /><FieldRef Name='COP_Supplemental_InNewWindow' /><FieldRef Name='COP_Supplemental2_icon' /><FieldRef Name='COP_Supplemental2_Link' /><FieldRef Name='COP_Supplemental2_LinkColor' /><FieldRef Name='COP_Supplemental2_InNewWindow' /><FieldRef Name='COP_Supplemental3_icon' /><FieldRef Name='COP_Supplemental3_Link' /><FieldRef Name='COP_Supplemental3_LinkColor' /><FieldRef Name='COP_Supplemental3_InNewWindow' /></ViewFields>",
    completefunc: function (xData, Status) {		
		xData.responseXML = xData.responseXML === undefined ? jQuery.parseXML(xData.responseText) : xData.responseXML ;		
		//xDataw = xData;
		$(xData.responseXML).SPFilterNode("z:row").each(function() {    
			nItem = new EventPresentation();
			nItem.Title = $(this).attr("ows_Title");
			//nItem.QuarterYear = $(this).attr("ows_COP_QuarterYear");
			nItem.QuarterTitle = $(this).attr("ows_COP_QuarterTitle");
			//nItem.Quarter = $(this).attr("ows_COP_Quarter");
			nItem.IsQuarter = $(this).attr("ows_COP_IsQuarter");
			//nItem.ShowTime = $(this).attr("ows_COP_ShowTime");
			nItem.EventDate = $(this).attr("ows_COP_EventDate");
			//nItem.EventTimeZone = $(this).attr("ows_COP_EventTimeZone");
			// nItem.EventDescription = $(this).attr("ows_COP_EventDescription");
			// nItem.NewsRelease_icon = $(this).attr("ows_COP_NewsRelease_icon");
			// nItem.NewsRelease_Link = $(this).attr("ows_COP_NewsRelease_Link");
			// nItem.NewsRelease_LinkColor = $(this).attr("ows_COP_NewsRelease_LinkColor");
			// nItem.NewsRelease_InNewWindow = $(this).attr("ows_COP_NewsRelease_InNewWindow");
			// nItem.Slides_icon = $(this).attr("ows_COP_Slides_icon");
			// nItem.Slides_Link = $(this).attr("ows_COP_Slides_Link");
			// nItem.Slides_LinkColor = $(this).attr("ows_COP_Slides_LinkColor");
			// nItem.Slides_InNewWindow = $(this).attr("ows_COP_Slides_InNewWindow");
			// nItem.WebCast_icon = $(this).attr("ows_COP_WebCast_icon");
			// nItem.WebCast_Link = $(this).attr("ows_COP_WebCast_Link");
			// nItem.WebCast_LinkColor = $(this).attr("ows_COP_WebCast_LinkColor");
			// nItem.WebCast_InNewWindow = $(this).attr("ows_COP_WebCast_InNewWindow");
			// nItem.Transcript_icon = $(this).attr("ows_COP_Transcript_icon");
			// nItem.Transcript_Link = $(this).attr("ows_COP_Transcript_Link");
			// nItem.Transcript_LinkColor = $(this).attr("ows_COP_Transcript_LinkColor");
			// nItem.Transcript_InNewWindow = $(this).attr("ows_COP_Transcript_InNewWindow");
			// nItem.Supplemental_icon = $(this).attr("ows_COP_Supplemental_icon");
			// nItem.Supplemental_Link = $(this).attr("ows_COP_Supplemental_Link");
			// nItem.Supplemental_LinkColor = $(this).attr("ows_COP_Supplemental_LinkColor");
			// nItem.Supplemental_InNewWindow = $(this).attr("ows_COP_Supplemental_InNewWindow");
			nItem.Supplemental2_icon = $(this).attr("ows_COP_Supplemental2_icon");
			nItem.Supplemental2_Link = $(this).attr("ows_COP_Supplemental2_Link");
			nItem.Supplemental2_LinkColor = $(this).attr("ows_COP_Supplemental2_LinkColor");
			nItem.Supplemental2_InNewWindow = $(this).attr("ows_COP_Supplemental2_InNewWindow");
			nItem.Supplemental3_icon = $(this).attr("ows_COP_Supplemental3_icon");
			nItem.Supplemental3_Link = $(this).attr("ows_COP_Supplemental3_Link");
			nItem.Supplemental3_LinkColor = $(this).attr("ows_COP_Supplemental3_LinkColor");
			nItem.Supplemental3_InNewWindow = $(this).attr("ows_COP_Supplemental3_InNewWindow");
			// nItem.Supplemental4_icon = $(this).attr("ows_COP_Supplemental4_icon");
			// nItem.Supplemental4_Link = $(this).attr("ows_COP_Supplemental4_Link");
			// nItem.Supplemental4_LinkColor = $(this).attr("ows_COP_Supplemental4_LinkColor");
			// nItem.Supplemental4_InNewWindow = $(this).attr("ows_COP_Supplemental4_InNewWindow");
			// nItem.Supplemental5_icon = $(this).attr("ows_COP_Supplemental5_icon");
			// nItem.Supplemental5_Link = $(this).attr("ows_COP_Supplemental5_Link");
			// nItem.Supplemental5_LinkColor = $(this).attr("ows_COP_Supplemental5_LinkColor");
			// nItem.Supplemental5_InNewWindow = $(this).attr("ows_COP_Supplemental5_InNewWindow");
			eventsEntries.push(nItem);
      });
	  matchEventsPresentation();
    }
  });
  
}


function matchEventsPresentation() {
	"use strict";
	//console.log("matchEventsPresentation");
	var titleIndex, supplementalIndex, dateIndex, count, InvRevEPPanel, webpart, headerPast, eventRow, SupplementalHasTextLink, curTitle, curDateTitle, curSuppContainer, relatedEvent, currentTotalSupp, totalSupps;
	InvRevEPPanel =  jQuery("[id*=InvRevEPPanel]");
	InvRevEPPanel = InvRevEPPanel.length < 1 ?  jQuery("[id*=InvRevPanel]") : InvRevEPPanel ;
	webpart = InvRevEPPanel.closest("[webpartid*=-]");
	headerPast = InvRevEPPanel.find(".ir-event-header");
	eventRow = InvRevEPPanel.find(".ir-event-item-container:not(.ir-event-header)");
		
	count = 0;
	supplementalIndex = titleIndex = dateIndex = -1;
	headerPast.children().each(function(){
		dateIndex = this.className.indexOf("ir-event-date") > -1 ? count : dateIndex;
		titleIndex = this.className.indexOf("ir-event-title") > -1 ? count : titleIndex;
		supplementalIndex = this.className.indexOf("ir-event-supplemental") > -1 ? count : supplementalIndex;
		count++;
	});
	SupplementalHasTextLink = eventRow.find(".ir-event-item-text").length > 0;
	if( eventsEntries.length > 0){	
		eventRow.each(function(){			
			curTitle = jQuery(this).find(".ir-event-title:not([id*=DatePanel])").text().trim();			
			curDateTitle = jQuery(this).find(".ir-event-datetitle").text().trim();
			curTitle = curTitle === "" && curDateTitle === "" ? jQuery(this).closest(".quarter-container").find(".ir-earning-title-container").text().trim() : curTitle;
			curSuppContainer = $(this).find(".ir-event-supplemental");
			relatedEvent = GetEventObject(curTitle, curDateTitle);
			if(relatedEvent !== null && curSuppContainer.length > 0)
			{
				currentTotalSupp = curSuppContainer.find("ir-event-item").length;
				totalSupps = relatedEvent.SupplementalCount();
				
				if(currentTotalSupp < totalSupps)
				{
					curSuppContainer.find(".ir-event-item").addClass("ir-event-item-inline");
					AddSupplementals(relatedEvent, curSuppContainer,SupplementalHasTextLink);
				}
			}
			//console.log(curTitle);
		});	
	}
}

function AddSupplementals(relatedEvent, container, hasTextLink) {
    "use strict";
	var eventItem, eventLink, eventIcon, eventText;
	if(relatedEvent.Supplemental2_icon)
	{
		eventItem = jQuery("<div class='ir-event-item ir-event-item-inline'><div>");
		eventLink = jQuery("<a>");
		eventIcon = jQuery("<div class='ir-icon-display'><div>");
		eventText = jQuery("<div class='ir-event-item-text'><div>");		
		eventLink.attr("href", relatedEvent.Supplemental2_Link.split(", ")[0] );
		eventLink.attr("target", relatedEvent.Supplemental2_InNewWindow  ? "_blank" : "" );
		eventIcon.attr("style", relatedEvent.Supplemental2_icon );
		eventText.text( relatedEvent.Supplemental2_Link.split(", ")[1] );
		eventLink.css("color",relatedEvent.Supplemental2_LinkColor);
		eventLink.append(eventIcon);
		if(hasTextLink){
			eventLink.append(eventText);
		}
		eventItem.append(eventLink);
		container.append(eventItem);
	}
	if(relatedEvent.Supplementa3_icon)
	{
		eventItem = jQuery("<div class='ir-event-item ir-event-item-inline'><div>");
		eventLink = jQuery("<a>");
		eventIcon = jQuery("<div class='ir-icon-display'><div>");
		eventText = jQuery("<div class='ir-event-item-text'><div>");
		eventLink.attr("href", relatedEvent.Supplemental3_Link.split(", ")[0] );
		eventLink.attr("target", relatedEvent.Supplemental3_InNewWindow  ? "_blank" : "" );
		eventIcon.attr("style", relatedEvent.Supplemental3_icon );
		eventText.text( relatedEvent.Supplemental3_Link.split(", ")[1] );
		eventText.css("color",relatedEvent.Supplemental3_LinkColor);
		eventLink.append(eventIcon);
		if(hasTextLink){
			eventLink.append(eventText);
		}
		eventItem.append(eventLink);
		container.append(eventItem);
	}
	if(relatedEvent.Supplemental4_icon)
	{
		eventItem = jQuery("<div class='ir-event-item ir-event-item-inline'><div>");
		eventLink = jQuery("<a>");
		eventIcon = jQuery("<div class='ir-icon-display'><div>");
		eventText = jQuery("<div class='ir-event-item-text'><div>");
		eventLink.attr("href", relatedEvent.Supplemental4_Link.split(", ")[0] );
		eventLink.attr("target", relatedEvent.Supplemental4_InNewWindow  ? "_blank" : "" );
		eventIcon.attr("style", relatedEvent.Supplemental4_icon );
		eventText.text( relatedEvent.Supplemental4_Link.split(", ")[1] );
		eventText.css("color",relatedEvent.Supplemental4_LinkColor);
		eventLink.append(eventIcon);
		if(hasTextLink){
			eventLink.append(eventText);
		}
		eventItem.append(eventLink);
		container.append(eventItem);
	}
	if(relatedEvent.Supplemental5_icon)
	{
		eventItem = jQuery("<div class='ir-event-item ir-event-item-inline'><div>");
		eventLink = jQuery("<a>");
		eventIcon = jQuery("<div class='ir-icon-display'><div>");
		eventText = jQuery("<div class='ir-event-item-text'><div>");
		eventLink.attr("href", relatedEvent.Supplemental5_Link.split(", ")[0] );
		eventLink.attr("target", relatedEvent.Supplemental5_InNewWindow  ? "_blank" : "" );
		eventIcon.attr("style", relatedEvent.Supplemental5_icon );
		eventText.text(relatedEvent.Supplemental5_Link.split(", ")[1] );
		eventText.css("color",relatedEvent.Supplemental5_LinkColor );
		eventLink.append(eventIcon);
		if(hasTextLink){
			eventLink.append(eventText);
		}
		eventItem.append(eventLink);
		container.append(eventItem);
	}
}

function GetEventObject(title, dateTitle) {
    "use strict";
	var curEvent, event;
	for (event in eventsEntries)
	{
		curEvent = eventsEntries[event];
		if(curEvent.IsQuarter === true || curEvent.IsQuarter === "1" )
		{
			//console.log("looking: " + curEvent.QuarterTitle);
			if(curEvent.QuarterTitle !== undefined && curEvent.QuarterTitle.trim() === title || curEvent.Title.trim() === title){
				return curEvent;
			}
		}
		else if(curEvent.Title.trim() === title  )
		{
			return curEvent;
		}
	}
	for (event in eventsEntries)
	{
		curEvent = eventsEntries[event];
		if( dateTitle.trim().replace(/  /g, '').indexOf(curEvent.Title.trim()) > -1 )
		{
			return curEvent;
		}
	}
	return null;
}

function CompleteInvestorWebPart() {
	"use strict";
	var InvRevEPPanel, InvRevPanel;
	InvRevEPPanel =  jQuery("[id*=InvRevEPPanel]");		
	InvRevPanel =  jQuery("[id*=InvRevPanel]");		
	if(InvRevEPPanel.find(".ir-event-supplemental").length > 0 || InvRevPanel.find(".ir-event-supplemental").length > 0) {
		loadEventsPresentationsServices();	
	}
}

////////////////////////// Stock Quote Link

function SetStockQuoteLink() {
	var webpartStockPOD, stockTitle, clientContext, oList, camlQuery, listItemInfo, collListItem, listItemEnumerator, oListItem, val;
	webpartStockPOD = jQuery(".ticker.row-fluid").closest("[webpartid*=-]");
	if(webpartStockPOD.length > 0){
		stockTitle = webpartStockPOD.find("h4:contains('Stock Quote')");
		stockTitle = stockTitle.length > 0 ? stockTitle : webpartStockPOD.find("h4:contains('STOCK QUOTE')");
		if(stockTitle.length > 0){
			clientContext = new SP.ClientContext("/");
			oList = clientContext.get_web().get_lists().getByTitle('Configuration Settings');
			camlQuery = new SP.CamlQuery();
			camlQuery.set_viewXml('<View><Query><Where><Eq><FieldRef Name=\'Title\'/><Value Type=\'Text\'>' + "Stock Quote Link" + '</Value></Eq></Where></Query><RowLimit>1</RowLimit></View>');
			collListItem = oList.getItems(camlQuery);

			clientContext.load(collListItem, 'Include(Id,Title,COP_ConfigSettingsValue)');    
			clientContext.add_requestSucceeded(onStockConfigLoaded);
			clientContext.add_requestFailed(onStockConfigLoadedFailure);
			clientContext.executeQueryAsync();
			
		}
	}
	function onStockConfigLoaded() {
		"use strict";
		listItemInfo = '';
		listItemEnumerator = collListItem.getEnumerator();

		while (listItemEnumerator.moveNext()) {
			oListItem = listItemEnumerator.get_current();
			val = oListItem.get_item('COP_ConfigSettingsValue');			
		}
		if(val.length > 5){
			var linkStock = jQuery("<a>");
			linkStock.attr("href", val);
			stockTitle.wrap(linkStock);
		}
	}
 
	
	function onStockConfigLoadedFailure(sender, args) {
		//console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
	}
}


function SetStockQuoteLinkServices() {
	var webpartStockPOD, stockTitle, clientContext, oList, camlQuery, listItemInfo, collListItem, listItemEnumerator, oListItem, val;
	webpartStockPOD = jQuery(".ticker.row-fluid").closest("[webpartid*=-]");
	if(webpartStockPOD.length > 0){
		stockTitle = webpartStockPOD.find("h4:contains('Stock Quote')");
		stockTitle = stockTitle.length > 0 ? stockTitle : webpartStockPOD.find("h4:contains('STOCK QUOTE')");
		if(stockTitle.length > 0){		 
			var stockLink = "";
			$().SPServices({
				operation: "GetListItems",
				async: true,
				listName: "Configuration Settings",
				webURL: "/",
				CAMLQuery: "<Query><Where><Eq><FieldRef Name='Title' /><Value Type='Text'>Stock Quote Link</Value></Eq></Where></Query>",
				CAMLViewFields: "<ViewFields><FieldRef Name='Title' /><FieldRef Name='COP_ConfigSettingsValue' /></ViewFields>",
				completefunc: function (xData, Status) {		
					xData.responseXML = xData.responseXML === undefined ? jQuery.parseXML(xData.responseText) : xData.responseXML ;		
					
					$(xData.responseXML).SPFilterNode("z:row").each(function() {    				
						stockLink = $(this).attr("ows_COP_ConfigSettingsValue");
						if(stockLink !== ""){
							var linkStock = jQuery("<a>");
							linkStock.attr("href", stockLink);
							stockTitle.wrap(linkStock);
						}
					});
				}
			});
		}
	}	 
}


/////////////////// LINK TILE WebPart
function GetStyleElement(style, cssName){
	"use strict";
	var item, curEl, elements, curCSSName, curCSSNameVal;	
	elements = style.split(";");
	for(item in elements){
		curEl = elements[item];		
		curCSSName = curEl.substring(0,curEl.indexOf(":") );
		curCSSNameVal = curEl.substring(curEl.indexOf(":") +1);
		if(cssName.toLowerCase() ===  curCSSName.toLowerCase()){
			return curCSSNameVal;
		}
	}
}

function UseImgBkgLinkTileWP(){
	"use strict";
	var possibleLinkTileWP, urlValid, style, imgURL, webpart;
	possibleLinkTileWP = jQuery(".pod.links-tile");
	if(possibleLinkTileWP.length > 0){
		urlValid = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
		possibleLinkTileWP.each(function(){
			webpart = jQuery(this).closest("[webpartid*=-]");
			if(webpart.length > 0)
			{
				style = jQuery(this).attr("style");
				imgURL = GetStyleElement(style, "background-color");
				if(imgURL !== undefined && imgURL.match(urlValid) ){					
					jQuery(this).css("background-image","url("+imgURL+")");
					jQuery(this).css("background-size","cover");
				}			
			}
		});
	}
}

function SetImgBkgLinkTileWP() {
	var clientContext, oList, camlQuery, listItemInfo, listItemEnumerator,collListItem, oListItem, val;
	var possibleLinkTileWP, urlValid, style, imgURL, webpart;
	possibleLinkTileWP = jQuery(".pod.links-tile");
	
	
	if(possibleLinkTileWP.length > 0){
		urlValid = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
		var configLinkTiles = [];
		clientContext = new SP.ClientContext("/");
		oList = clientContext.get_web().get_lists().getByTitle('Configuration Settings');
		camlQuery = new SP.CamlQuery();		
		camlQuery.set_viewXml('<View><Query><Where><Contains><FieldRef Name=\'Title\'/><Value Type=\'Text\'>' + "LinkTileWP" + '</Value></Contains></Where></Query><RowLimit>100</RowLimit></View>');
		collListItem = oList.getItems(camlQuery);

		clientContext.load(collListItem, 'Include(Id,Title,COP_ConfigSettingsValue)');    
		clientContext.add_requestSucceeded(onImgConfigListLoaded);
		clientContext.add_requestFailed(onImgConfigListFailure);
		clientContext.executeQueryAsync();
	}
	
	function onImgConfigListLoaded() {
			
		listItemInfo = '';
		listItemEnumerator = collListItem.getEnumerator();

		while (listItemEnumerator.moveNext()) {
			oListItem = listItemEnumerator.get_current();
			title = oListItem.get_item("Title").replace("LinkTileWP=","");
			val = oListItem.get_item('COP_ConfigSettingsValue');
			configLinkTiles[title] = val.toUpperCase();
		}
		possibleLinkTileWP.each(function(){
			webpart = jQuery(this).closest("[webpartid*=-]");
			if(webpart.length > 0)
			{
				tmpTitle = jQuery(this).find("h4[id*=Header]").text().trim().toUpperCase();
				imgURL = configLinkTiles[tmpTitle];
				if(imgURL !== undefined){
					jQuery(this).css("background-image","url("+imgURL+")");
					jQuery(this).css("background-size","cover");
					jQuery(this).css("background-color","transparent");
					jQuery(this).find("[id*=Wrapper]").css("background-color","transparent");
					//console.log("wow!");
				}
			}
		});
	}
	function onImgConfigListFailure() {
		//console.log("Fail! " + ex);	
	}
	
}



function SetImgBkgLinkTileWPServices() {
	var clientContext, oList, camlQuery, listItemInfo, listItemEnumerator,collListItem, oListItem, val;
	var possibleLinkTileWP, urlValid, style, imgURL, webpart;
	possibleLinkTileWP = jQuery(".pod.links-tile");
	
	
	if(possibleLinkTileWP.length > 0){
		urlValid = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
		var configLinkTiles = [];
						
		$().SPServices({
			operation: "GetListItems",
			async: true,
			listName: "Configuration Settings",
			webURL: "/",
			CAMLQuery: "<Query><Where><Contains><FieldRef Name='Title' /><Value Type='Text'>LinkTileWP</Value></Contains></Where></Query>",
			CAMLViewFields: "<ViewFields><FieldRef Name='Title' /><FieldRef Name='COP_ConfigSettingsValue' /></ViewFields>",
			completefunc: function (xData, Status) {		
				xData.responseXML = xData.responseXML === undefined ? jQuery.parseXML(xData.responseText) : xData.responseXML ;		
				
				$(xData.responseXML).SPFilterNode("z:row").each(function() {    				
					//console.log($(this).attr("ows_COP_ConfigSettingsValue")); 
					//console.log($(this).attr("ows_Title")); 
					configLinkTiles[$(this).attr("ows_Title").replace("LinkTileWP=","")] = $(this).attr("ows_COP_ConfigSettingsValue").toUpperCase();
				});
				possibleLinkTileWP.each(function(){
					webpart = jQuery(this).closest("[webpartid*=-]");
					if(webpart.length > 0)
					{
						tmpTitle = jQuery(this).find("h4[id*=Header]").text().trim().toUpperCase();
						imgURL = configLinkTiles[tmpTitle];
						if(imgURL !== undefined){
							jQuery(this).css("background-image","url("+imgURL+")");
							jQuery(this).css("background-size","cover");
							jQuery(this).css("background-color","transparent");
							jQuery(this).find("[id*=Wrapper]").css("background-color","transparent");
							//console.log("wow!");
						}
					}
				}); 

			}
		});
	}
}


/////////////////////////
/////////////////////////
function RunSPServicesFunctions(){
	if($().SPServices === undefined){
		setTimeout(RunSPServicesFunctions,50);
	} else{ 
		SetStockQuoteLinkServices();
		SetImgBkgLinkTileWPServices(); 
		CompleteInvestorWebPart();
	}
}
////////////////////// PAGE READY

if (typeof _spBodyOnLoadFunctionNames === 'undefined') {
	jQuery(document).ready(function () {
		SetTitlesHeight();
		InsertDivLineEarnings();
		RunSPServicesFunctions(); 
	});
} else {
	_spBodyOnLoadFunctionNames.push("SetTitlesHeight"); 
	_spBodyOnLoadFunctionNames.push("InsertDivLineEarnings");
	_spBodyOnLoadFunctionNames.push("RunSPServicesFunctions"); 
}
		
		