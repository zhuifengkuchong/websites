(function ($) {


    /*


     Copyright 2012 Google Inc.

     Licensed under the Apache License, Version 2.0 (the "License");
     you may not use this file except in compliance with the License.
     You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

     Unless required by applicable law or agreed to in writing, software
     distributed under the License is distributed on an "AS IS" BASIS,
     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     See the License for the specific language governing permissions and
     limitations under the License.
    */

    /*

    Legend

    Search for these terms to find appropriate functions.

    Stores.Add_To_Left_Column
    - The function that adds stores to the list in the left column

    

    */


    var maxStoreCount = 50; // total number of stores to display in the list
    var maxStoreRadius = 50; // in miles
    var marker_array = new Array(); // used to store markers to reset from number to image

    // js prototype references
    // if you see something like a[j](), the array key likely references something in this list
    var d = !0,
        f = null,
        g = !1,
        h = Math,
        j = "getLocation",
        l = "trigger",
        m = "bindTo",
        n = "removeListener",
        o = "geometry",
        p = "attr",
        q = "getBounds",
        r = "find",
        s = "addListener",
        t = "maps",
        u = "getMap",
        v = "contains",
        w = "push",
        x = "addClass",
        z = "click",
        A = "highlight",
        B = "length",
        C = "prototype",
        D = "getId",
        E = "getMarker",
        F = "setMap",
        G = "append",
        H = "event",
        mapGlobal = undefined;

    function I(a) {
        return function () {
            return this[a]
        }
    }

    var J; // the main app functionality object

    function K() { }
    window.storeLocator = K;

    function L(a) {
        return a * h.PI / 180
    };

    function M(a, b) {
        this.b = a;
        this.a = b
    }
    K.Feature = M;
    M[C].getId = I("b");
    M[C].getDisplayName = I("a");
    M[C].toString = function () {
        return this.getDisplayName()
    };

    function N(a) {
        this.a = [];
        this.b = {};
        for (var b = 0, c; c = arguments[b]; b++) this.add(c)
    }
    K.FeatureSet = N;
    J = N[C];
    J.toggle = function (a) {
        this[v](a) ? this.remove(a) : this.add(a)
    };
    J.contains = function (a) {
        return a[D]() in this.b
    };
    J.getById = function (a) {
        return a in this.b ? this.a[this.b[a]] : f
    };
    J.add = function (a) {
        a && (this.a[w](a), this.b[a[D]()] = this.a[B] - 1)
    };
    J.remove = function (a) {
        this[v](a) && (this.a[this.b[a[D]()]] = f, delete this.b[a[D]()])
    };
    J.asList = function () {
        for (var a = [], b = 0, c = this.a[B]; b < c; b++) {
            var e = this.a[b];
            e !== f && a[w](e)
        }
        return a
    };
    var O = new N;

    function P(a, b) {
        // a = div#panel
        this.g = $(a);
        this.g[x]("storelocator-panel");
        this.c = $.extend({
            locationSearch: d,
            locationSearchLabel: "Enter Location:",
            featureFilter: d,
            directions: d,
            view: f
        }, b);
        this.l = new google[t].DirectionsRenderer({
            draggable: d
        });
        this.t = new google[t].DirectionsService;
        Q(this)
    }
    K.Panel = P;
    P.prototype = new google[t].MVCObject;

    function Q(a) {
        a.c.view && a.set("view", a.c.view);
        a.e = $('<form class="storelocator-filter"/>');

        // append form.storelocator-filter to panel
        a.g[G](a.e);

        // a.z = $('<div class="feature-switches"><h4>Show Stations With</h4><div class="switches active"><a href="#" class="ampm active">ampm</a><a href="#" class="chase-atm active">Chase ATM</a><a href="#" class="car-wash active">Car Wash</a></div></div>');
        // a.g[G](a.z);

        a.c.locationSearch && (a.i = $('<div class="location-search"><h4>' + a.c.locationSearchLabel + '</h4><input placeholder="Enter&nbsp;Location" class="location-field"><input type="submit" name="submit" id="zip-submit" /><div class="check-clear"><a href="#" class="submit">Go</a><input type="submit" class="submit" value="Go" /><!--a href="#" class="submit"></a><a href="#" class="clear"></a--></div><!--div class="results"><span>XX</span> Results Found</div--></div>'), a.e[G](a.i), "undefined" != typeof google[t].places ? R(a) : a.e.submit(function () {

            a.searchPosition($("input", a.i).val())
        }), a.e.submit(function () {
            a.searchPosition($("input", a.i).val())
            return g
        }), google[t][H][s](a, "geocode", function (b) {
            a.s && window.clearTimeout(a.s);
            if (b[o]) {

                this.k = b[o].location;
                a.h && S(a);
                var c = a.get("view");
                c[A](f);
                var e = c[u]();
                b[o].viewport ? e.fitBounds(b[o].viewport) : (e.setCenter(b[o].location), e.setZoom(13));
				if(jQuery.browser.mobile){e.setZoom(11)}
                c.refreshView();
                T(a);


            } else {
                a.searchPosition(b.name);
            }

        }));
        if (a.c.featureFilter) {
            a.d = $('<div class="feature-filter"><h4>SHOW STATIONS WITH</h4></div>');
            //a.kr = $("<div class='feature-wrapper' />");

            for (var b = a.get("view").getFeatures().asList(), c = 0, e = b[B]; c < e; c++) {
                var i = b[c],
                    k = $('<input type="checkbox" name="cb-' + i + '" id="cb-' + i + '" />');
                k.data("feature", i);
                a.d[G](k);
                $("<label for='cb-" + i + "' />")[G](i.getDisplayName()).appendTo(a.d);
            }
            //a.d[G](a.kr);
            a.e[G](a.d);
            a.d[r]("input").change(function () {
                var b = $(this).data("feature"),
                    c = a.get("featureFilter");
                c.toggle(b);
                a.set("featureFilter", c);
                a.get("view").refreshView()
            })
        }
        a.b = $('<ul class="store-list"/>');
        a.g[G](a.b);

        $(".feature-filter h4").bind('click', function (e) {
            e.preventDefault();
            //////////////console.log('clicked h4');
            $(this).parent().toggleClass('closed');
        });

        $(".location-search a.clear").bind('click', function (e) {
            e.preventDefault();
            $(".location-search input").val("");
        })
        $(".location-search a.submit").bind('click', function (e) {
            e.preventDefault();
            $(".storelocator-filter").submit();
        })

        a.c.directions && (a.a = $('<div class="directions-panel"><form><input class="directions-to"/><input type="submit" value="Find directions"/><a href="#" class="close-directions">Close</a></form><div class="rendered-directions"></div></div>'), a.a[r](".directions-to")[p]("readonly", "readonly"), a.a.hide(), a.h = g, a.a[r]("form").submit(function () {
            S(a);
            return g
        }), a.a[r](".close-directions")[z](function () {
            a.hideDirections()
        }),
            a.g[G](a.a))
    }
    var U = new google[t].Geocoder;

    function T(a) {
        var b = a.get("view");
        // remove stores changed listener
        a.r && google[t][H][n](a.r);
        // re-add stores changed listener
        a.r = google[t][H].addListenerOnce(b, "stores_changed", function () {
            a.set("stores", b.get("stores"))
        })
    }
    J = P[C];
    J.searchPosition = function (a) {
        var b = this,
            a = {
                address: a,
                bounds: this.get("view")[u]()[q]()
            };

        U.geocode(a, function (a, e) {
            e == google[t].GeocoderStatus.OK && google[t][H][l](b, "geocode", a[0])

            if (e == 'ZERO_RESULTS') {
                $(".location-search input").addClass("error");
            }
        })
    };
    J.setView = function (a) {
        this.set("view", a)
    };
    J.view_changed = function () {
        function a() {
            b.clearMarkers();
            T(c)
        }
        var b = this.get("view");
        this[m]("selectedStore", b);
        var c = this;

        // (see below for info on o, q, and p)
        // if this.o exists, remove the listener for it
        this.o && google[t][H][n](this.o);
        // if this.q exists, remove the listener for it
        this.q && google[t][H][n](this.q);
        // if this.p exists, remove the listener for it
        this.p && google[t][H][n](this.p);

        // view.getMap
        b[u]();

        // o = listener for load
        this.o = google[t][H][s](b, "load", a);
        // q = listener for zoom change
        this.q = google[t][H][s](b[u](), "zoom_changed", a);
        // p = listener for idle
        this.p = google[t][H][s](b[u](), "idle", function () {

            // on idle, a = map
            var a = b[u]();

            c.j ? a[q]()[v](c.j) || (c.j = a.getCenter(), T(c)) : c.j = a.getCenter();

        });

        // clear markers & reset stores changed listener
        a();

        // bind feature filter to view
        this[m]("featureFilter", b);
        this.f && this.f[m]("bounds", b[u]())
    };

    function R(a) {
        var b = $("input", a.i)[0];
        a.f = new google[t].places.Autocomplete(b);
        a.get("view") && a.f[m]("bounds", a.get("view")[u]());
        google[t][H][s](a.f, "place_changed", function () {
            google[t][H][l](a, "geocode", this.getPlace());
        })
    }
    J.stores_changed = function () {

        // Stores.Add_To_Left_Column

        var markers = this.c.view.c;

        // when stores change, reset the numbered pins to the generic gas icon
        for (pin in markers) {
            markers[pin].setIcon('Unknown_83_filename'/*tpa=http://arco.rpahosting.com/wp-content/themes/vip/tesoro/images/arco/intersect/tag.png*/)
        }


        if (this.get("stores")) {
            var view = this.get("view"),
                bounds = view && view[u]()[q](),
                stores = this.get("stores"),
                currentStore = this.get("selectedStore");



            // this.b is a node object for ul with class of store-list
            this.b.empty();

            stores[B] ? bounds && !bounds[v](stores[0][j]()) && this.b[G]('<li class="no-stores">There are no stores in this area. However, stores closest to you are listed below.</li>') : this.b[G]('<li class="no-stores">There are no stores in this area.</li>');

            // k = store count. either maxStoreCount or stores.length, whichever is less
            k = h.min(maxStoreCount, stores.length);

            var listItemClick = function () {
                // d = true, maybe
                view[A](this.store, d);
            }

            for (i = 0; i < k; i++) {

                // y = li element (store)
                var y = stores[i].getInfoPanelItem();

                if (y !== false) {

                    // get the individual store
                    y.store = stores[i];

                    // if the current store should be highlighted, then highlight it
                    currentStore && stores[i][D]() ==
                        currentStore[D]() && $(y)[x]("highlighted");


                    // the marker related to this store
                    var marker = markers[y.store[D]()];

                    // add click to list item to show pin detail
                    y.u || (y.u = google[t][H].addDomListener(y, "click", listItemClick));


                    // get and build the distance value
                    var dist = y.store.b.distance;
                    $(".distance", $(y)).text(numberWithCommas(Math.round(dist * 10) / 10) + " mi");

                    // adds store item to list if it's within the radius
                    if (dist < maxStoreRadius) {

                        // set the marker to a numbered marker
                        marker.setIcon('/wp-content/themes/vip/tesoro/images/arco/intersect/markers/' + (i + 1) + '.png');

                        // append the store to the list
                        this.b[G](y);
                    }
                }
            }

            var inc = 0;
            $("ul.store-list li.store").each(function (i) {
                // sets a class to alter font size if the store list number is double digits
                $(".index", $(this)).text(i + 1).removeClass("dd");
                if ((i) > 8) {
                    $(".index", $(this)).eq(0).addClass("dd");
                }
            });
            $("ul.store-list a.directions-link").bind('click', function (e) {
                // set up directions link to open directly in google maps
                e.preventDefault();
                var address = $(this).siblings('.address').html();

                window.open('https://google.com/maps/?daddr=' + escape(address.replace("<br>", " ")) + '&saddr=' + escape($(".location-field").val()) + '&directionsmode=driving', '_blank');
            })
        }
    };
    J.selectedStore_changed = function () {

        $(".highlighted", this.b).removeClass("highlighted");
        var a = this,
            b = this.get("selectedStore");
        if (b) {
            this.m = b;
            this.b[r]("#store-" + b[D]())[x]("highlighted");
            this.c.directions && this.a[r](".directions-to").val(b.getDetails().title);
            var c = a.get("view").getInfoWindow().getContent(),
                e = $("<a/>").text("Directions")[p]("href", "#")[x]("directions"),
                i = $("<a/>").text("Zoom here")[p]("href", "#")[x]("action")[x]("zoomhere"),
                k = $("<a/>").text("Street view")[p]("href", "#")[x]("action")[x]("streetview");
            e[z](function () {

                // get address and open google maps in new window with directions displayed
                var address = $(this).parent().children('.address').html();

                window.open('https://google.com/maps/?daddr=' + escape(address.replace("<br>", " ")) + '&saddr=' + escape($(".location-field").val()) + '&directionsmode=driving', '_blank');


                return g
            });
            // zoom to here listener
            i[z](function () {
                a.get("view")[u]().setOptions({
                    center: b[j](),
                    zoom: 16
                })
            });
            // street view listener
            k[z](function () {
                var c = a.get("view")[u]().getStreetView();
                c.setPosition(b[j]());
                c.setVisible(d)
            });

            // append everything to the info window
            $(c)[G](e)[G](i)[G](k);
        }
    };
    J.hideDirections = function () {
        this.h = g;
        this.a.fadeOut();
        this.d.fadeIn();
        this.b.fadeIn();
        this.l[F](f)
    };
    J.showDirections = function () {
        var a = this.get("selectedStore");
        this.d.fadeOut();
        this.b.fadeOut();
        this.a[r](".directions-to").val(a.getDetails().title);
        this.a.fadeIn();
        S(this);
        this.h = d
    };

    function S(a) {
        if (a.k && a.m) {
            var b = a.a[r](".rendered-directions").empty();
            a.t.route({
                origin: a.k,
                destination: a.m[j](),
                travelMode: google[t].DirectionsTravelMode.DRIVING
            }, function (c, e) {
                if (e == google[t].DirectionsStatus.OK) {
                    var i = a.l;
                    i.setPanel(b[0]);
                    i[F](a.get("view")[u]());
                    i.setDirections(c)
                }
            })
        }
    }
    J.featureFilter_changed = function () {
        T(this)
    };

    function V() {
        this.b = []
    }
    K.StaticDataFeed = V;
    V[C].setStores = function (a) {
        this.b = a;
        this.a ? this.a() : delete this.a
    };
    V[C].getStores = function (a, b, c) {

        if (this.b[B]) {
            for (var e = [], i = 0, k; k = this.b[i]; i++) k.hasAllFeatures(b) && e[w](k);

            // e = array of store locations

            // sort stores by distance to map center
            aa(a.getCenter(), e);

            // set stores
            c(e)

        } else {
            var y = this;
            this.a = function () {
                y.getStores(a, b, c)
            }
        }
    };

    function aa(a, b) {
        // a = center of map
        // b = array of locations

        mapGlobal = a;

        for (i = 0; i < b.length; i++) {
            // create distance property
            b[i].b.distance = b[i].distanceTo(a);
        }

        // sort locations by distance from map center in ascending order
        b.sort(function (x, e) {
            return x.b.distance - e.b.distance;
        })

    };
    /*
     Latitude/longitude spherical geodesy formulae & scripts
     (c) Chris Veness 2002-2010
     www.movable-type.co.uk/scripts/latlong.html
    */
    function W(a, b, c, e) {
        this.f = a;
        this.d = b;
        this.a = c || O;
        this.b = e || {}
    }
    K.Store = W;
    J = W[C];
    J.setMarker = function (a) {
        this.e = a;
        google[t][H][l](this, "marker_changed", a)
    };
    J.getMarker = I("e");
    J.getId = I("f");
    J.getLocation = I("d");
    J.getFeatures = I("a");
    J.hasFeature = function (a) {
        return this.a[v](a)
    };
    J.hasAllFeatures = function (a) {
        if (!a) return d;
        for (var a = a.asList(), b = 0, c = a[B]; b < c; b++)
            if (!this.hasFeature(a[b])) return g;
        return d
    };
    J.getDetails = I("b");

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    J.ba = function (a) {
        // create html for store list items

        var dist = Math.round(this.b.distance * 10) / 10;
        //if(dist < maxStoreRadius){
        for (var b = ["index", "title", "address", "phone", "distance"], c = [], e = 0, i = b[B]; e < i; e++) {

            var k = b[e];

            // store list builder

            // if item is a boolean, change the value to something usable
            //var content = ((k == "ampm" && a.b[k] == 1) || (k == "atm" && a.b[k] == 1) || (k == "carwash" && a.b[k] == 1)) ? k : a.b[k];
            var content = ((k == "ampm" && a.b[k] == 1)) ? k : a.b[k];

            // create the distance value
            if (k == "distance") content = numberWithCommas(dist) + " mi";

            // make the phone number click-to-call
            if (k == "phone") {
                content = '<a href="tel:' + a.b[k] + '">' + a.b[k] + '</a>';
            }


            // if(a.b[k] exists){ (do, all, this, stuff) }
            // a.b[k] is the content value related to title, address, phone, etc
            // if you wish to add a value here, you must add it to the content object way below
            /* example:
                
                <div class="phone"><a href="tel:5551212">555-1212</a></div>

                ^^ The following builds out divs like this guy ^^
                
            */
            a.b[k] && (c[w]('<div class="'), c[w](k), c[w]('">'), c[w](content), c[w]("</div>"));

        }
        return c.join("");
        // }else{
        //     $(".store-list").empty();
        // }
        //return false;
    }

    function ca(a) {
        var b = [];
        b[w]('<ul class="features">');
        for (var a = a.a.asList(), c = 0, e; e = a[c]; c++) b[w]("<li>"), b[w](e.getDisplayName()), b[w]("</li>");
        b[w]("</ul>");
        b[w]("<a href='#' class='directions-link'>Directions</a>");
        return b.join("")
    }
    J.getInfoWindowContent = function () {
        if (!this.c) {
            if (this.ba(this) !== false) {
                var a = ['<div class="store">'];
                a[w](this.ba(this));
                a[w](ca(this));
                a[w]("</div>");
                this.c = a.join("")
            } else {
                this.c = false;
            }
        }
        return this.c
    };
    J.getInfoPanelContent = function () {
        return this.getInfoWindowContent()
    };
    var X = {};
    W[C].getInfoPanelItem = function () {
        var a = this[D]();
        if (!X[a]) {
            var b = this.getInfoPanelContent();
            X[a] = false;
            if (b) {
                X[a] = $('<li class="store" data-store-id="' + this[D]() + '" id="store-' + this[D]() + '">' + b + "</li>")[0];
            }
        }
        return X[a]
    };
    W[C].distanceTo = function (a) {

        var b = this[j]();

        var unit = "N";

        var radlat1 = Math.PI * b.lat() / 180
        var radlat2 = Math.PI * a.lat() / 180
        var radlon1 = Math.PI * b.lng() / 180
        var radlon2 = Math.PI * a.lng() / 180
        var theta = b.lng() - a.lng()
        var radtheta = Math.PI * theta / 180
        var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
        dist = Math.acos(dist)
        dist = dist * 180 / Math.PI
        dist = dist * 60 * 1.1515
        if (unit == "K") { dist = dist * 1.609344 }
        if (unit == "N") { dist = dist * 0.8684 }

        return dist
    };

    function Y() { }
    K.DataFeed = Y;
    Y[C].getStores = function () { };

    function Z(a, b, c) {
        this.g = a;
        this.f = b;
        this.b = $.extend({
            updateOnPan: d,
            geolocation: d,
            features: new N
        }, c);
        da(this);
        google[t][H][l](this, "load");
        this.set("featureFilter", new N);
    }
    K.View = Z;
    Z.prototype = new google[t].MVCObject;

    function ea(a) {
        // this function is fired when geolocation is set to true in
        // the view constructor within initialize()
        window.navigator && navigator.geolocation && navigator.geolocation.getCurrentPosition(function (b) {
            b = new google[t].LatLng(b.coords.latitude, b.coords.longitude);
            a[u]().setCenter(b);
            a[u]().setZoom(jQuery.browser.mobile?13:11);//a[u]().setZoom(11);
            google[t][H][l](a, "load");
        }, void 0, {
            maximumAge: 6E4,
            timeout: 1E4
        })
    }

    function da(a) {
        // if geolocation is set to true, then call ea() to get the geolocation data
        a.b.geolocation && ea(a);
        a.c = {};
        a.a = new google[t].InfoWindow;
        var b = a[u]();
        a.set("updateOnPan", a.b.updateOnPan);
        google[t][H][s](a.a, "closeclick", function () {
            a[A](f)
        });
        google[t][H][s](b, "click", function () {
            a[A](f);
            a.a.close()
        })
    }
    J = Z[C];
    J.updateOnPan_changed = function () {
        this.e && google[t][H][n](this.e);
        if (this.get("updateOnPan") && this[u]()) {
            var a = this,
                b = this[u]();
            this.e = google[t][H][s](b, "idle", function () {
                a.refreshView()
            })
        }
    };
    J.addStoreToMap = function (a) {
        var b = this[E](a);
        a.setMarker(b);
        var c = this;
        b.n = google[t][H][s](b, "click", function () {
            ////////////////////console.log(c[A]);
            c[A](a, g)
        });
        b[u]() != this[u]() && b[F](this[u]())
    };
    J.createMarker = function (a) {
        var a = {
            position: a[j]()
        },
            b = this.b.markerIcon;
        b && (a.icon = b);
        var marker = new google[t].Marker(a);
        //console.log('J.createMarker');
        return marker;
    };
    J.getMarker = function (a) {
        var b = this.c,
            c = a[D]();
        b[c] || (b[c] = this.createMarker(a));
        //console.log('J.getMarker');
        return b[c]
    };
    J.getInfoWindow = function (a) {
        if (!a) return this.a;
        this.a.setContent($(a.getInfoWindowContent())[0]);
        return this.a
    };
    J.getFeatures = function () {
        return this.b.features
    };
    J.getFeatureById = function (a) {
        if (!this.d) {
            this.d = {};
            for (var b = 0, c; c = this.b.features[b]; b++) this.d[c[D]()] = c
        }
        return this.d[a]
    };
    J.featureFilter_changed = function () {
        google[t][H][l](this, "featureFilter_changed", this.get("featureFilter"));
        this.get("stores") && this.clearMarkers()
    };
    J.clearMarkers = function () {
        $(".location-search input").removeClass("error");
        for (var a in this.c) {
            this.c[a][F](f);
            var b = this.c[a].n;
            b && google[t][H][n](b)
        }
    };
    J.refreshView = function () {
        var a = this;
        var bounds = this[u]()[q]();

        if (bounds) {
            this.f.getStores(bounds, this.get("featureFilter"), function (b) {
                var c = a.get("stores");

                if (c)
                    for (var e = 0, i = c[B]; e < i; e++) google[t][H][n](c[e][E]().n);
                a.set("stores", b)
            })
        }
    };
    J.stores_changed = function () {
        for (var a = this.get("stores"), b = 0, c; c = a[b]; b++) this.addStoreToMap(c)
    };
    J.getMap = I("g");
    J.highlight = function (a, b) {

        var c = this.getInfoWindow(a);
        ////console.log("highlight", c);
        a ? (c = this.getInfoWindow(a), a[E]() ? c.open(this[u](), a[E]()) : (c.setPosition(a[j]()), c.open(this[u]())), this[u]().getStreetView().getVisible() && this[u]().getStreetView().setPosition(a[j]())) : c.close();
        //b && this[u]().panTo(a[j]()),
        this.set("selectedStore", a)
    };
    J.selectedStore_changed = function () {
        google[t][H][l](this, "selectedStore_changed", this.get("selectedStore"))
    };
})(jQuery);
/**
 * @extends storeLocator.StaticDataFeed
 * @constructor
 */
function DataSource() {
    jQuery.extend(this, new storeLocator.StaticDataFeed);

    var that = this;
    jQuery.get('http://arco.rpahosting.com/wp-content/themes/vip/tesoro/other_assets/SeattleStores.csv', function (data) {
        that.setStores(that.parse_(data));
    });
}

/**
 * @const
 * @type {!storeLocator.FeatureSet}
 * @private
 */
DataSource.prototype.FEATURES_ = new storeLocator.FeatureSet(
    new storeLocator.Feature('ampm-1', 'ampm')/*,
    new storeLocator.Feature('Atm-1', 'ATM'),
    new storeLocator.Feature('CarWash-1', 'Car Wash')*/
);

/**
 * @return {!storeLocator.FeatureSet}
 */
DataSource.prototype.getFeatures = function () {
    return this.FEATURES_;
};

/**
 * @private
 * @param {string} csv
 * @return {!Array.<!storeLocator.Store>}
 */
DataSource.prototype.parse_ = function (csv) {
    var stores = [];
    var rows = csv.split('\n');
    var headings = this.parseRow_(rows[0]);

    for (var i = 1, row; row = rows[i]; i++) {
        row = this.toObject_(headings, this.parseRow_(row));
        var features = new storeLocator.FeatureSet;
        // features.add(this.FEATURES_.getById('Brand_Tesoro-' + row.Brand_Tesoro));
        // features.add(this.FEATURES_.getById('Brand_Shell-' + row.Brand_Shell));
        // features.add(this.FEATURES_.getById('Brand_USA-' + row.Brand_USA));

        var storeNum = this.join_([row.ListNumber, row.StoreNumber], '');
        var stateZip = this.join_([row.State, row.Zip], ' ');
        var cityStateZip = this.join_([row.City, stateZip], ', ');
        var position = new google.maps.LatLng(row.Lat, row.Lng);
        var icon = 'Unknown_83_filename'/*tpa=http://arco.rpahosting.com/wp-content/themes/vip/tesoro/images/arco/intersect/tag.png*/;

        var store = new storeLocator.Store(row.ListNumber, position, features, {
            title: this.join_([row.StoreName, row.StoreNumber], ' #'),
            address: this.join_([row.Address, cityStateZip], '<br>'),
            phone: row.Phone,
            icon: icon
        });
        stores.push(store);
    }
    return stores;
};

/**
 * Joins elements of an array that are non-empty and non-null.
 * @private
 * @param {!Array} arr array of elements to join.
 * @param {string} sep the separator.
 * @return {string}
 */
DataSource.prototype.join_ = function (arr, sep) {
    var parts = [];
    for (var i = 0, ii = arr.length; i < ii; i++) {
        arr[i] && parts.push(arr[i]);
    }
    return parts.join(sep);
};

/**
 * Very rudimentary CSV parsing - we know how this particular CSV is formatted.
 * IMPORTANT: Don't use this for general CSV parsing!
 * @private
 * @param {string} row
 * @return {Array.<string>}
 */
DataSource.prototype.parseRow_ = function (row) {
    // Strip leading quote.
    if (row.charAt(0) == '"') {
        row = row.substring(1);
    }
    // Strip trailing quote. There seems to be a character between the last quote
    // and the line ending, hence 2 instead of 1.
    if (row.charAt(row.length - 2) == '"') {
        row = row.substring(0, row.length - 2);
    }

    row = row.split(',');

    return row;
};

/**
 * Creates an object mapping headings to row elements.
 * @private
 * @param {Array.<string>} headings
 * @param {Array.<string>} row
 * @return {Object}
 */
DataSource.prototype.toObject_ = function (headings, row) {
    var result = {};
    for (var i = 0, ii = row.length; i < ii; i++) {
        result[headings[i]] = row[i];
    }
    return result;
};;

function initialize() {
    var map = new google.maps.Map(document.getElementById('map-canvas2'), {
        center: new google.maps.LatLng(40, -100),
        zoom: 4,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var panelDiv = document.getElementById('panel');

    var data = new DataSource;

    var view = new storeLocator.View(map, data, {
        geolocation: true,
        features: data.getFeatures(),
        updateOnPan: false
    });

    view.createMarker = function (store) {
        return new google.maps.Marker({
            position: store.getLocation(),
            icon: store.getDetails().icon,
            title: store.getDetails().title,
            visible: true,
            labelContent: "$425K",
            labelAnchor: new google.maps.Point(22, 0),
            labelClass: "labels", // the CSS class for the label
            labelStyle: { opacity: 0.75 }
        });
    };

    if (console) //////////////console.log('geo');


        new storeLocator.Panel(panelDiv, {
            view: view
        });

    view.clearMarkers();

    google.maps.event.addListenerOnce(map, 'center_changed', function () {
        view.refreshView();
        view.createMarker = function (store) {
            return new google.maps.Marker({
                position: store.getLocation(),
                icon: store.getDetails().icon,
                title: store.getDetails().title,
                visible: true
            });
        };
        google.maps.event.addListener(map, 'idle', function () {
            view.refreshView();
        });
    });



    // $(".store-list").each(function(){
    //     ////////////////console.log("storelocator filter exists");
    // })
}

google.maps.event.addDomListener(window, 'load', initialize);;
/**
 * @extends storeLocator.StaticDataFeed
 * @constructor
 */
function DataSource() {
    jQuery.extend(this, new storeLocator.StaticDataFeed);

    var that = this;
    var csv_url = 'http://' + location.host + '/wp-content/themes/vip/tesoro/other_assets/MasterArcoStoreLocations-1.csv';
	//alert(csv_url);
    jQuery.get(csv_url, function (data) {
        that.setStores(that.parse_(data));
    });
}

/**
 * @const
 * @type {!storeLocator.FeatureSet}
 * @private
 */
DataSource.prototype.FEATURES_ = new storeLocator.FeatureSet(
    new storeLocator.Feature('ampm-1', 'ampm')/*,
    new storeLocator.Feature('Atm-1', 'ATM'),
    new storeLocator.Feature('CarWash-1', 'Car Wash')*/
);

/**
 * @return {!storeLocator.FeatureSet}
 */
DataSource.prototype.getFeatures = function () {
    return this.FEATURES_;
};

/**
 * @private
 * @param {string} csv
 * @return {!Array.<!storeLocator.Store>}
 */
DataSource.prototype.parse_ = function (csv) {
    var stores = [];
    var rows = csv.split('\n');
    var headings = this.parseRow_(rows[0]);

    for (var i = 1, row; row = rows[i]; i++) {
        row = this.toObject_(headings, this.parseRow_(row));
        var features = new storeLocator.FeatureSet;
        features.add(this.FEATURES_.getById('ampm-' + row.ampm));
        //features.add(this.FEATURES_.getById('Atm-' + row.Atm));
        //features.add(this.FEATURES_.getById('CarWash-' + row.CarWash));

        var storeNum = this.join_([row.ListNumber, row.StoreNumber], '');
        var stateZip = this.join_([row.State, row.Zip], ' ');
        var cityStateZip = this.join_([row.City, stateZip], ', ');
        var position = new google.maps.LatLng(row.Lat, row.Lng);

        var icon = location.protocol + "//" + location.hostname + '/wp-content/themes/vip/tesoro/images/arco/intersect/tag.png';

        //////////console.log(icon);

        var ampm = row.ampm;
        //var atm = row.Atm;
        //var carwash = row.CarWash;
        //////////////////////console.log(row);

        var store = new storeLocator.Store(row.ListNumber, position, features, {
            index: "0",
            title: this.join_([row.StoreName, row.StoreNumber], ' #'),
            address: this.join_([row.Address, cityStateZip], '<br>'),
            phone: row.Phone,
            icon: icon,
            ampm: ampm,/*
            atm: atm,
            carwash: carwash,*/
            distance: "0",
            directions: "Directions"
        });
        //////////////////////console.log(store);
        stores.push(store);
    }
    return stores;
};

/**
 * Joins elements of an array that are non-empty and non-null.
 * @private
 * @param {!Array} arr array of elements to join.
 * @param {string} sep the separator.
 * @return {string}
 */
DataSource.prototype.join_ = function (arr, sep) {
    var parts = [];
    for (var i = 0, ii = arr.length; i < ii; i++) {
        arr[i] && parts.push(arr[i]);
    }
    return parts.join(sep);
};

/**
 * Very rudimentary CSV parsing - we know how this particular CSV is formatted.
 * IMPORTANT: Don't use this for general CSV parsing!
 * @private
 * @param {string} row
 * @return {Array.<string>}
 */
DataSource.prototype.parseRow_ = function (row) {
    // Strip leading quote.
    if (row.charAt(0) == '"') {
        row = row.substring(1);
    }
    // Strip trailing quote. There seems to be a character between the last quote
    // and the line ending, hence 2 instead of 1.
    if (row.charAt(row.length - 2) == '"') {
        row = row.substring(0, row.length - 2);
    }

    row = row.split(',');

    return row;
};

/**
 * Creates an object mapping headings to row elements.
 * @private
 * @param {Array.<string>} headings
 * @param {Array.<string>} row
 * @return {Object}
 */
DataSource.prototype.toObject_ = function (headings, row) {
    var result = {};
    for (var i = 0, ii = row.length; i < ii; i++) {
        result[headings[i]] = row[i];
    }
    return result;
};;
