        function resizeOuterDiv() {
	        var containerHeight = jQuery(document).height();
	        var targetHeight = containerHeight - 140;

	        jQuery('#outerErrorDiv').css('height', targetHeight + 'px');
        }
	
        jQuery(document).ready(function() {
                jQuery("body").css("backgroundImage", "url('../../../www.arco.com/wp-content/themes/vip/tesoro/images/arco/404/404-error-background.jpg'/*tpa=http://www.arco.com/wp-content/themes/vip/tesoro/images/arco/404/404-error-background.jpg*/)");
	            // attach a resize event handler which will size the outer div
	            var containerHeight = jQuery(document).height()
	            var targetHeight = containerHeight - 140;
	            jQuery('#outerErrorDiv').css('height', targetHeight + 'px');
			
	            jQuery(window).resize(function() {resizeOuterDiv();});
        });
		
		resizeOuterDiv()