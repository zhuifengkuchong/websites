<script id = "race9b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race9={};
	myVars.races.race9.varName="Lu_Id_li_16__onmouseout";
	myVars.races.race9.varType="@varType@";
	myVars.races.race9.repairType = "@RepairType";
	myVars.races.race9.event1={};
	myVars.races.race9.event2={};
	myVars.races.race9.event1.id = "Lu_Id_li_16";
	myVars.races.race9.event1.type = "onmouseout";
	myVars.races.race9.event1.loc = "Lu_Id_li_16_LOC";
	myVars.races.race9.event1.isRead = "True";
	myVars.races.race9.event1.eventType = "@event1EventType@";
	myVars.races.race9.event2.id = "Lu_window";
	myVars.races.race9.event2.type = "onload";
	myVars.races.race9.event2.loc = "Lu_window_LOC";
	myVars.races.race9.event2.isRead = "False";
	myVars.races.race9.event2.eventType = "@event2EventType@";
	myVars.races.race9.event1.executed= false;// true to disable, false to enable
	myVars.races.race9.event2.executed= false;// true to disable, false to enable
</script>

