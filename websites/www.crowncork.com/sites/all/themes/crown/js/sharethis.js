/**
 * Asyncronously loads the sharethis library, preserving options
 * @see https://techblog.willshouse.com/2014/06/07/sharethis-asynchronous-javascript-loading/
 */
(function ($, Drupal) {
  Drupal.behaviors.crownShareThis = {
    attach: function(context, settings) {

      // Fire once, against the "sharethis" class on the page, if it exists
      $('.sharethis').once("crownShareThis", function() {
        // Switch to the old version
        $(document).switchTo5x=false;
        // Create the empty script tag
        var e = document.createElement("script");
        e.type = "text/javascript";
        e.async = true;
        // Create the onload callback where we will add the options if success or catch an error if it fails
        e.onload = function() {
          try {
            stLight.options({publisher: "6dc6eacc-0391-4848-9697-e21c602505f6", doNotHash: false, doNotCopy: false, hashAddressBar: false});
          } catch(e) {
            // optionally do something to handle errors here
          }
        }
        // Attach the script to the page with the sharethis url
        e.src = ('https:' == document.location.protocol ? 'https://ws' : 'http://w/') + '.sharethis.com/button/buttons.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(e, s);
      }); // end once

    }
  };
})(jQuery, Drupal);

