/**
 * Are you what your doing should go in the default script.js? REALLY SURE?
 */

(function ($, Drupal) {


  Drupal.behaviors.STARTER = {
    attach: function(context, settings) {

      // Tell Foundation to reload dropdowns when forms are reloaded with ajax
      // @see template.php crown_form_alter() where we are adding the custom class.
      if (typeof Foundation.libs.forms != 'undefined') {
        if ($('form.custom').length) {
          $(document).ajaxComplete(function() {
            Foundation.libs.forms.assemble();
          });
        }
      }
      // Disable zurb form custom class from mobile, tablet, and ie8
      // removing function encapsulation for now. 
      // function Disable_Zurb_Forms(){ }
      var html = jQuery('html');
      var ua = navigator.userAgent.toLowerCase();
      var isAndroid = ua.indexOf("android") > -1;
      var isIphone = (navigator.userAgent.match(/iPhone/i) != null) || (navigator.userAgent.match(/iPod/i) != null);
      var isIpad = navigator.userAgent.match(/iPad/i) != null;
        if ( html.hasClass('mobile') 
        || html.hasClass('tablet')
        || html.hasClass('lt-ie9')
        || isAndroid
        || isIphone
        || isIpad ) {
          jQuery('form').removeClass('custom');
          jQuery('form .custom').addClass('hidden');
        }

      // Implment iframe resize on all iframes (currently only used for careers)
      // @see https://github.com/davidjbradshaw/iframe-resizer
      var $frames = $('iframe');
      if ($frames.length && $.fn.iFrameResize.length) {
        var iframe_options = {
          'log': false,
          'checkOrigin': false,
          'autoResize': true,
          'bodyMargin': '10px 10px 20px 10px'
        };
        $frames.iFrameResize(iframe_options);
      }

      // Implement stackable tables on all but the ones with the 
      // scrollable-responsive-table-wrapper class, which will use 
      // the investor style instead.
      // Note that we are doing it for all tables except the investors ones
      // for now. Mostly we are targetting datatable (content) and views tables.
      $.each($("table"), function(index, elem) {
        // Try to select the wrapper element for this table.
        var $investor_wrapper = $(this).closest(".scrollable-responsive-table-wrapper");
        // If no wrapper exists, then we want to use stackable tables.
        if (!$investor_wrapper.length) {
          // Use once so that we don't get multiple calls on this table during ajax reloads.
          $(this).once('crownStackableTable', function() {
            // Double check that our library is in place and then call it.
            if ($.fn.stacktable.length) {
              $(this).stacktable();
            }
          });
        }        
      });

      /********* footer resize background img ***********/
      //inner container is position absolute leaving the l-footer-columns which displays background img with nothing to help expand the height and display img
      function footerHeight() {
        //(min-width: 920px)
        //console.log('(min-width: $topbar-breakpoint)');
        if (window.matchMedia('(min-width: 920px)').matches) {

          var footer_height = $('.footer-columns-wrapper').height();

          //hide transform is position absolute with a top value that bumps the green inner footer down -- the bottom offset is twice the value of the top value
          var footer_height_offset = $('.hide-transform').css("top");

          //bottom offset is twice the value of the top offset
          var footer_bg_height = parseInt(footer_height) + parseInt(footer_height_offset) * 2;
          

          $('.l-footer-columns').css('height', footer_bg_height);
          $('.l-footer-columns').css('padding', 0);
        }
        else {
          //remove styles applied by js when screen does not match min-width
          $('.l-footer-columns').css('height', '');
          $('.l-footer-columns').css('padding', '');
        }
      }

      footerHeight();

      // when window size changes, fire footerHeight function to adjust height
      $(window).resize(function(){  
        Drupal.poll(function() {
          footerHeight();
        });
      });

      // Add a fade in and out for exposed filters.
      // Note that block-views is the element that gets update by ajax.
      // @see http://drupal.stackexchange.com/questions/31238/how-do-i-change-the-ajax-loading-behaviour-in-views
      $('.views-exposed-widget', context).ajaxStart(function(){
        $(this).closest('.block-views').fadeTo(300, 0.5);
      });
      $('.views-exposed-widget', context).ajaxSuccess(function(){
         //$(this).closest('.block-views').fadeTo(300, 1.0);
         $(this).closest('.block-views').css('opacity', 0.5).fadeTo(300, 1.0);
      });

    }
  };

  

})(jQuery, Drupal);
