/**
 * Defines a global poll function in the Drupal namespace for use by all other scripts.
 * This should be placed above any scripts that call it in crown.info.
 * Polling functions are a way to keep frequently called event handlers from crashing a browser.
 * @see http://stackoverflow.com/questions/4372759/lower-the-frequency-of-javascript-event-polling
 */
(function ($, Drupal) {
  /**
   * Add our function directly on the drupal namespace
   */
  Drupal.poll = (function(){
    var timer = 0;
    return function(callback, ms){
      // Set a default of 25 miliseconds if that parameter was not given.
      if (!ms) {
        ms = 25;
      }
      clearTimeout(timer);
      timer = setTimeout(callback, ms);
    };
  })();
})(jQuery, Drupal);