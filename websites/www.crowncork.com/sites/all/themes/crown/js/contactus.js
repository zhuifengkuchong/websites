  /**
 * Handles the behaviors for the contact us form.
 */

// Always encapsulate
(function ($, Drupal) {

  // Call Drupal behaviors
  Drupal.behaviors.crownContactUs = {
    attach: function(context, settings) {
      $(".view-id-crown_contacts").once('crownContactUs', function() {
        //var contacts = new crownContactUs();
        //contacts.init($(this), settings);
      });
    }
  }

  /**
   * Create an object for the behaviors
   */
  var crownContactUs = function(){
    // Create a var to hold out public methods and object
    var pub = {};

    /**
     * Init
     */
    pub.init = function($elem, settings) {
      // Get each row of this view (note we are using the cusomized class from the custom text field)
      var $rows = $(".crownContact", $elem);
      // Go through each one
      $.each($rows, function(index, elem){
        var $this_elem = $(this);
        var id = $this_elem.attr("id");
        var checkbox_id = 'checkbox_'+id;
        var email = $(".crownContact-email", $this_elem).text();
        // Add the checkbox
        var $input = $('<input/>', {
          'type': 'checkbox',
          'class': 'form-checkbox crownContact-checkbox',
          'value': email,
          'name': checkbox_id,
          'id': checkbox_id
        }).prependTo($this_elem);
        // Add a label on the "territory" div
        $(".field-name-field-job-territory", $this_elem).wrapInner('<label for="'+ checkbox_id +'"></label>');;

      });
      // Get the hidden webform checkboxes wrapper
      var $hidden_checkboxes_wrapper = $(".webform-component--recipients");
      // Set up the listener on the checkboxes we just created
      $(".crownContact-checkbox", $elem).on("change", function() {
        var $checkbox = $("input[value='" + $(this).val() + "']", $hidden_checkboxes_wrapper);
        if ($checkbox.prop('checked')) {
          $checkbox.prop('checked', false);
          debug('UN-Checking. '+ $checkbox.val() +' is now', $checkbox.prop('checked'));
        }
        else {
          $checkbox.prop('checked', true);
          debug('Checking. '+ $checkbox.val() +' is now', $checkbox.prop('checked'));
        }
      });
      // Reload foundation elements (this makes it function given the 
      // dynamically added form above and also after ajax loads)
      if (typeof Foundation.libs.forms != 'undefined') {
        if ($('form.custom', $elem).length) {
          Foundation.libs.forms.assemble();
        }
      }
      // Clear out any previously checked items from previous ajax page loads
      $("input", $hidden_checkboxes_wrapper).prop('checked', false);
      // Finally hide the checkboxes
      $hidden_checkboxes_wrapper.hide();
    }

    /**
     * Debug wrapper
     */
    function debug(msg, obj){
      if (typeof console !== 'undefined' && 'log' in console) {
        //console.log(msg, obj);
      }
    };

    // Return out public methods and object
    return pub;
  }

})(jQuery, Drupal);
