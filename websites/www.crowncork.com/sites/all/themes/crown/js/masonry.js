/**
 * Masonry grid displayed on inspiration gallery and vertical teasers.
 * See assets -- vertical teasers, design inspiration gallery masonry-item for styles.
 */
(function ($, Drupal) {

  // Call Drupal behaviors
  Drupal.behaviors.masonry = {
    attach: function(context, settings) {
      // Call masonry once for each element
      // masonry class is the wrapping container for all grid items = ul
      $('.masonry', context).once("masonry", function() {
        var options = {};
        $.each($(this), function(index, elem){
          var grid = new crownGrid(elem, options);
        });
      });
      // Call masonry once for each .masonry-blocks element in the content-below-masonry region
      // masonry-blocks class is the wrapping container and block is the child class
      $('.masonry-blocks', context).once("masonry", function() {
        var options = {
          item_selector: '.block',
          column_width: '.block'
        };
        $.each($(this), function(index, elem){
          var grid = new crownGrid(elem, options);
        });
      });
    }
  }

  /**
   * The crownGrid holds our functionality
   */
  var crownGrid = function(elem, options) {
    var defaults = get_defaults();
    options = $.extend({}, defaults, options);
    // Load images before masonry js is declared 
    $(elem).imagesLoaded( function() {
      $(elem).masonry(options);       
    });

    // Get defaults
    function get_defaults() {
      return {
        //item selector = li,
        //gutterWidth: 2,
        item_selector: '.masonry-item',
        column_width: '.masonry-item',
        isAnimated: true,
        isResizable: true
      };
    }
  }

})(jQuery, Drupal);