/**
 * Javascript required to make Foundation 4 theme work in IE8
 */

(function ($, Drupal) {

  Drupal.behaviors.crownIE8Compatibility = {
    attach: function(context, settings) {

      // Main nav (top bar) secondary links don't work in ie8 if they have the .has-dropdown class.
      $('.lt-ie9 .main-nav > li > ul > .has-dropdown', context).once("crownIE8Compatibility", function() {
        $(this).removeClass('has-dropdown');
      });
    }
  };
})(jQuery, Drupal);