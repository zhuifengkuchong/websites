/**
 * Front JS
 * 
 * Assumes the drupal.poll.js has been included.
 */
// Encapsulate to make jquery happy.
(function ($, Drupal, detect) {
  // Add to the Drupal.behaviors namespace.
  Drupal.behaviors.front = {
    attach: function(context, settings) {
      $(".parallax").once("front", function(){
        var $parent_elem = $(this);
        var front = new frontController();
        front.init($parent_elem);
      });
    }
  }

  /**
   * Create an object to hold all our methods.
   */
  var frontController = function(){
    // Create the public namespace on the object.
    var pub = {};

    // The parent element
    pub.parent_elem = {};
    // The collection of first slides
    pub.elem_first_slides = {};
    // The collection of focus slides
    pub.elem_focus_slides = {};

    // Each block has a slide that is the focus element
    var selector_header = '.l-header';
    var selector_slide_first = '.slide-first';
    var selector_slide_focus = '.slide-focus';
    var selector_dots = '#dots';

    // Tracks the index of the current item. The first is 1.
    pub.count = 0;

    /**
     * Initialize a map with clickable pins and info windows
     */
    pub.init = function($parent_elem) {
      // Assign the parent
      pub.parent_elem = $parent_elem;
      // Assign the slide first elements
      pub.elem_first_slides = $(".slide-first");
      // Assign the slide focus elements
      pub.elem_focus_slides = $(".slide-focus");
      // Create an initial scroll event
      handleScroll();
      // Add our scroll event on the parent parallax div
      pub.parent_elem.on("scroll", function() {
        // Poll the window.resize event every 25ms for changes and fire specific code.
        Drupal.poll(function(){
          //debug('--parallax scroll event--');
          handleScroll();
        }, 25);
      });
      // Add one on the window also for non parallax fallbacks
      $(window).on("scroll", function() {
        // Poll the window.resize event every 25ms for changes and fire specific code.
        Drupal.poll(function(){
          //debug('--window scroll event--');
          handleScroll();
        }, 25);
      });
      // Create the dots
      createDots();
      // Create jump links
      setJumpLinkListeners();
      // TESTING
      //pub.elem_focus_slides.css('background', 'orange').css('opacity', '.5');
    };

    /**
     * Wrapper function that holds the things we do for scrolling
     */
    function handleScroll() {
      // Figure out which slide is in focus
      var $current = getCurrentSlide();
      //debug('current is '+ $current.attr('id'));
      if ($current.length) {
        // Adjust the animation classes
        addAnimationClasses($current);
        // Update the navigation dots
        updateDots();
      }
    }

    /**
     * Loop through the focus slides and find the one that has a positive offset
     */
    function getCurrentSlide() {
      // Set up the first items as the default to return
      var current = pub.elem_focus_slides.get(0);
      // Count the number of items
      var count = 0;
      // Get the header height
      var header_offset = getHeaderHeight();
      // Loop through all slide focus divs
      $.each(pub.elem_focus_slides, function(index, elem) {
        // Get the adjusted offset for this element
        var offset = getVerticalOffset(elem);
        // Create an offset
        var adjustment = 150;
        var final_offset = offset - adjustment;
        // Calculate the offset, taking the header into account
        //var offset = offset;// + header_offset;
        //debug('offset is '+ final_offset +'('+ offset +' - '+ adjustment +')');
        // If it is negative, mark it as current.
        if (final_offset <= 0) {
          count++;
          current = elem;
        }
        // Otherwise, exit the $.each statement so that the last one we processed is current
        else {
          return false;
        }
      });
      // Cache the counter
      pub.count = count;
      // Return the last "current" item standing as a jQuery object.
      return $(current);
    }

    /**
     * Adds the animation and active classes to the parent div
     */
    function addAnimationClasses($elem) {
      // Remove any previous active classes before we add this one
      removeAllActiveClasses();
      // Get the id of the current div, minus the -focus suffix
      // Note that this assumes the .slide-focus div has a convention of #slide-name-focus
      var first_id = $elem.attr('id');
      first_id = first_id.replace('-focus', '');
      // Construct the classes
      var animation_class = first_id + "_anim";
      var active_class = first_id + "_active";
      // Add the classes
      pub.parent_elem.addClass(animation_class);
      pub.parent_elem.addClass(active_class);
    }

    /**
     * Remove any existing actcive classes from the parent div in prep for adding the next one
     */
    function removeAllActiveClasses() {
      // Iterate through each .slide-first so we can build the classes
      $.each($(selector_slide_first), function(index, elem) {
        // And remove each one in the form of .slide-name_active
        var active_class = $(elem).attr('id') + "_active";
        pub.parent_elem.removeClass(active_class);
      });
    }

    /**
     * Return the header height.
     * Don't bother caching it on the object since it may change on resize
     */
    function getHeaderHeight() {
      return getOffsetHeight($(selector_header).get(0));
    }

    /**
     * 
     */
    function createDots() {
      var html_dots = '';
      $.each(pub.elem_first_slides, function(index, elem) {
        var id = $(elem).attr('id');
        var dot_class = id.replace('slide-', 'dot-');
        html_dots += '<div class="dot '+ dot_class +'"></div>';
      });
      // Add it to the page
      var $container = $('<section/>', {
        'class': 'dots',
        'id': 'dots',
        'html': html_dots
      }).insertBefore(pub.parent_elem);
      // Add onclick listeners
      $.each(pub.elem_first_slides, function(index, elem) {
        var id = $(elem).attr('id');
        var dot_class = id.replace('slide-', 'dot-');
        $(".dots ."+ dot_class).on("click touchstart", function(){
          Jump_Link('#'+ id +'-focus');
        });
      });
    }

    /**
     * Updates the active dot based on the dot counters number,
     * which reflects how many slides the user has scrolled past.
     */
    function updateDots(){
      // Get the dot count from the object, defaulting to 1
      dot_count = (pub.count > 1) ? pub.count - 1 : 0;
      var dots = $(selector_dots);
      if($(dots.children().get(dot_count)).hasClass('active')) return;
      dots.children('.active').removeClass('active');
      $(dots.children().get(dot_count)).addClass('active');
    } 

    /**
     * Quickly returns the distance an element is from the top of the screen
     * Remember that we use getBoundingClientRect because it works with css 3d transformations.
     */
    function getVerticalOffset(el){
      return el.getBoundingClientRect().top;
    }
    
    /**
     * Quickly returns the quarter height of an element,
     * which is used to determine when a user is viewing the slide
     * Remember that we use getBoundingClientRect because it works with css 3d transformations.
     */
    function getOffsetHeight(el){
      return el.getBoundingClientRect().height;
    } 

    /**
     * Jumps to the bottom of the focus div, which should be positioned at the next div
     */
    function Jump_Link(selector, multiplier){
      // Get where we are on the page.
      // Note we are using the document, which is good for both webkit and other browsers.
      var $elem_scrollable = $(document);
      var scroll_pos = $elem_scrollable.scrollTop();
      // Get the DOM element
      var target_dom = $(selector).get(0);
      // Get how far the top of the target div is from where we are
      var div_top_offset = getVerticalOffset(target_dom);
      // Get how tall the target div is
      var div_bottom_height = getOffsetHeight(target_dom);
      // Add an adjustment of 100px
      var adjustment = 0;
      // Add all of those together to get the final offset we need
      var final_target = scroll_pos + div_top_offset + div_bottom_height + adjustment;
      //debug(scroll_pos+" + "+div_top_offset+" + "+div_bottom_height+" + "+adjustment);
      //debug('jump is '+ final_target);
      // Animate a scroll to that position, using both body and html, which is good for all browsers.
      // Remember that the complete function will fire twice with this method.
      $("body, html").animate({ scrollTop: final_target }, 1500, "swing", function(){} );
      return false;
    }

    /**
     * Set listeners on the jump links
     */
    function setJumpLinkListeners() {
      // Add onclick listeners
      var ids = [];
      $.each(pub.elem_first_slides, function(index, elem) {
        var id = $(elem).attr('id');
        ids[index] = id;
      });
      $.each(ids, function(index, id){
        var next_index = index + 1;
        if (next_index >= ids.length) {
          next_index = 0;
        }
        var next_id = ids[next_index];
        $("."+ id +" .slide--object-jumplink").on("click touchstart", function(e){
          e.preventDefault();
          Jump_Link('#'+ next_id +'-focus');
        });
      });
    }

    /**
     * Debug wrapper
     */
    function debug(msg, obj) {
      if (typeof console !== 'undefined' && 'log' in console) {
        // UNCOMMENT ME FOR MATH!
        //console.log(msg, obj);
      }
    };

    // Return our public objects and methods
    return pub;
  }

})(jQuery, Drupal, detect);