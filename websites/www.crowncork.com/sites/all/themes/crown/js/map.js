/**
 * Google map implmentation and controls.
 * @see the locations view, which defines the geoJSON feed.
 * @see map_styles.js which holds the massive styles array.
 */

// Always encapsulate
(function ($, Drupal) {

  // Call Drupal behaviors
  Drupal.behaviors.crownGoogleMap = {
    attach: function(context, settings) {
      // Call init once to start things up.
      $('.view-crown-locations', context).once("crownGoogleMap", function() {
        $.each($(this), function(index, elem){
          var map = new crownMap();
          map.initWithPins(elem, settings, index);
        })
      });
      // And also for the block on the business unit page
      $('.sectionOverview--crownlocations', context).once("crownGoogleMap", function() {
        $.each($(this), function(index, elem){
          var map = new crownMap();
          map.initTeaser(elem, settings, index);
        })
      });
    }
  }

  /**
   * Create an object to hold all our methods.
   */
  var crownMap = function(){
    // Create the public namespace on the object.
    var pub = {};
    var styles = [];
    pub.elemObj = {};
    pub.settings = {};
    // These control the offsets for the info
    var window_offet_lat = 10;
    var window_offet_lng = 0;

    /**
     * Initialize a map with clickable pins and info windows
     */
    pub.initWithPins = function($elem, settings, nth) {
      // Save our params
      pub.elemObj = $elem;
      pub.settings = settings;
      // Prepend our map container to the page
      var this_id = 'crowngooglemap-' + nth;
      var $container = $('<div/>', {
        'class': 'crowngooglemap',
        'id': 'crowngooglemap-' + nth,
        'html': '<div class="crowngooglemap--loading">Loading...</div>'
      }).prependTo($elem);
      // Instantiate the map
      pub.map = new google.maps.Map(document.getElementById(this_id), get_map_options());
      // Set map styles
      set_map_data_styles();
      // Create the base info window object
      var infoWindow = new google.maps.InfoWindow({
          content: ""
      });
      // Add the listener for the pins to show the info window
      pub.map.data.addListener('click', function(event) {
        infoWindow.setContent('<div class="crowngooglemap--infocontent">'+ event.feature.getProperty("name") +'</div>');
        var lat = event.latLng.lat() + window_offet_lat;
        var lng = event.latLng.lng() + window_offet_lng;
        var anchorLatLng = new google.maps.LatLng(lat, lng);
        var anchor = new google.maps.MVCObject();
        anchor.set("position",anchorLatLng);
        infoWindow.open(pub.map,anchor);
      });
      // Load a GeoJSON from the same server as our demo.
      request_geojson(true);
    };

    /**
     * Initialize a map that takes the user to a different page
     */
    pub.initTeaser = function($elem, settings, nth) {
      // Save our params
      pub.elemObj = $elem;
      pub.settings = settings;
      // Append our map container to the page
      var this_id = 'crowngooglemap-' + nth;
      var $container = $('<div/>', {
        'class': 'crowngooglemap',
        'id': 'crowngooglemap-' + nth,
        'html': '<div class="crowngooglemap--loading">Loading...</div>'
      }).appendTo($elem);
      // Instantiate the map
      var options = get_map_options();
      options.panControl = false;
      options.streetViewControl = false;
      options.zoomControl = false;
      pub.map = new google.maps.Map(document.getElementById(this_id), options);
      // Set map styles
      set_map_data_styles();
      // Add a click event
      var click_location = get_teaser_href();
      google.maps.event.addListener(pub.map, 'click', function(event) {
        window.location.href = click_location;
      });
      // Add the listener for the pins to show the info window
      pub.map.data.addListener('click', function(event) {
        window.location.href = click_location;
      });
      google.maps.event.addListener(pub.map, 'mousemove', function(event) {
        pub.map.setOptions({ draggableCursor: 'pointer' });
      });
      // Load a GeoJSON from the same server as our demo.
      request_geojson(false);
    };

    /**
     * Return map options and styles
     */
    function get_map_options() {
      // Load our massive styles array from the other file to keep this one clean
      var styles = [];
      if (typeof Drupal.crownMapStyles == 'object') {
        if (typeof Drupal.crownMapStyles.get == 'function') {
          styles = Drupal.crownMapStyles.get();
        }
      }
      return {
        center: { lat: 41.1200325, lng: -87.8611531},
        zoom: 2,
        mapTypeControl: false,
        styles: styles,
        scrollwheel: false
      };
    }

    /**
     * Wrapper function for setting the styles
     */
    function set_map_data_styles() {
      pub.map.data.setStyle({
        // @todo: make this dynamic?
        icon: '../images/locations-marker.png'/*tpa=http://www.crowncork.com/sites/all/themes/crown/images/locations-marker.png*/
      });
    }

    /**
     * Request the geojson feed from the view
     */
    function request_geojson(use_bounds) {
      var bu_tid = get_contextual_argument();
      var geo_json_url = '/locations/feed/'+bu_tid;
      // Load the geojson from a URL - this is asynchronous, so process the data in a callback function
      var xhr = new XMLHttpRequest();
      xhr.open('GET', geo_json_url, true);
      // We use onreadstagechange because ie8 trips up over onload.
      xhr.onreadystatechange = function() {
          //ready?
          if (xhr.readyState != 4) {
              return false;
          }
          //get status:
          var status = xhr.status;
          //maybe not successful?
          if (status != 200) {
              //alert("AJAX: server status " + status);
              return false;
          }
          //Got result. All is good.
          attach_geojson(this.responseText, use_bounds);
          return true;
      } 
      xhr.send();
    }

    /**
     * Attach the geojson to the datalayer and zoom to bounds based on the coords in the set.
     */
    function attach_geojson(text, use_bounds) {
      // Parse the xml into json
      var json = JSON.parse(text);
      // Add the json to the data layer of the map
      var features = pub.map.data.addGeoJson(json);

      // Zoom to bounds to ensure we can see all pins
      if (use_bounds) {
        zoom_to_bounds(json);
      }
      else {
        pub.map.setCenter({ lat: 41.1200325, lng: -87.8611531});
      }
    }

    /**
     * Update a map's viewport to fit each geometry in a dataset
     * @param {google.maps.Map} map The map to adjust
     */
    function zoom_to_bounds(json) {
      // Only proceed if we have points to plot
      if (json.features.length) {
        // Set up a Google Maps Bounding object to work with
        var bounds = new google.maps.LatLngBounds();
        // Iterate through each point
        for(var i = 0; i < json.features.length; i++) {
          if(json.features[i].geometry.coordinates.length) {
            // Get the lat and lng. We are assuming the type is "Point".
            var lat = json.features[i].geometry.coordinates[1];
            var lng = json.features[i].geometry.coordinates[0];
            // Add this new point to our bounds object
            point = new google.maps.LatLng(lat, lng);
            bounds.extend(point);
          }
        }
        // Pan and zoom the map to fit all bounds
        pub.map.fitBounds(bounds);
        // Adjust the zoom if needed
        adjust_zoom();
      }
    }

    /**
     * Adjsut the starting zoom of the map.
     *
     * This is mean to account for being too far out or too far in.
     */
    function adjust_zoom() {
      // Get the zoom of the map
      var zoom = pub.map.getZoom();
      //debug('Map zoom after bounds is', zoom);
      // A zoom of 3 is the default to give enough visual information
      if (zoom >= 4) {
        zoom = 3;
      }
      // Back out to zoom 2 if needed
      else if (zoom >= 2 && zoom <= 3) {
        zoom = 2;
      }
      // If zoomed out to 1, that's OK
      else if (zoom == 1) {
        zoom = 1;
      }
      // All the way out is too far, so zoom back in a bit
      else if (zoom == 0) {
        zoom = 1;
      }
      // Zoom the map 
      //debug('Map final zoom after adjustment is '+ zoom);
      pub.map.setZoom(zoom);
    }

    /**
     * Wrapper function to figure out what argument to pass to the geoJSON feed
     * @see crown_preprocess_html() in template.php for where we set the variable.
     */
    function get_contextual_argument() {
      var tid = '';
      if (typeof pub.settings.crown != 'undefined') {
        if (typeof pub.settings.crown.field_product_category_bridge != 'undefined') {
          tid = pub.settings.crown.field_product_category_bridge;
        }
      }
      return tid;
    }

    /**
     * Get the teaser url
     */
    function get_teaser_href() {
      // Set the default link
      url = '/about-crown/global-locations';
      // Get the block title link in our container object
      var $title_link = $(".block-title a", pub.elemObj);
      if ($title_link.length) {
        // And use that as this link.
        url = $title_link.attr("href");
      }
      // Return the url
      return url;
    }

    /**
     * Debug wrapper
     */
    function debug(msg, obj){
      if (typeof console !== 'undefined' && 'log' in console) {
        //console.log(msg, obj);
      }
    };

    // Return all public object and methods.
    return pub;
  };

})(jQuery, Drupal);