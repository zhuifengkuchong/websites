/**
 * Slide out search bar and change icon on hover/touc.
 */

(function ($, Drupal) {

  Drupal.behaviors.crownSearchBar = {
    attach: function(context, settings) {

      // Works for touchscreens because this validates an empty search.
      $('.block-search', context).once("crownSearchBar", function() {
        $(this).hover(
          function() {
            $('.block-search .form-text').addClass('search-form-text--expanded');
            $('.block-search .form-text').animate({width:"140px"},200);
            $('.block-search .form-submit').addClass('search-form-submit--active');
          },
          function() {
            // Only hide if no text and it does not have focus (cursor in text box).
            if ($('.block-search .form-text').val().length == 0 && !$('.block-search .form-text').is(':focus')) {
              $('.block-search .form-text').removeClass('search-form-text--expanded');
              $('.block-search .form-text').animate({width:"0"},200);
              $('.block-search .form-submit').removeClass('search-form-submit--active');
            }
          }
        );
      });

      // Close if the body is clicked and there is no text in the form and it does not have focus (cursor in text box).
      // Reference; http://stackoverflow.com/questions/1403615/use-jquery-to-hide-a-div-when-the-user-clicks-outside-of-it
      $('body', context).once("crownSearchBar", function() {
        $(this).mouseup(function (e) {
          var container = $('.block-search');

          if (!container.is(e.target) // if the target of the click isn't the container...
            && container.has(e.target).length === 0) // ... nor a descendant of the container
          {
            if ($('.block-search .form-text').val().length == 0 && !$('.block-search .form-text').is(':focus')) {
              $('.block-search .form-text').removeClass('search-form-text--expanded');
              $('.block-search .form-text').animate({width:"0"},200);
              $('.block-search .form-submit').removeClass('search-form-submit--active');
            }
          }
        });
      });

      // Validate that the search input is not empty (also provides for toucscreen hover support).
      $('#search-block-form', context).once("crownSearchBar", function() {
        $(this).submit(function () {
          if ($.trim($(".block-search .form-text").val()) === "") {
            return false;
          }
        });
      });
    }
  };

})(jQuery, Drupal);
