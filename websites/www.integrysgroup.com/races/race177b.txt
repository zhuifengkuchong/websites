<script id = "race177b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race177={};
	myVars.races.race177.varName="Lu_Id_li_7__onmouseover";
	myVars.races.race177.varType="@varType@";
	myVars.races.race177.repairType = "@RepairType";
	myVars.races.race177.event1={};
	myVars.races.race177.event2={};
	myVars.races.race177.event1.id = "Lu_Id_li_7";
	myVars.races.race177.event1.type = "onmouseover";
	myVars.races.race177.event1.loc = "Lu_Id_li_7_LOC";
	myVars.races.race177.event1.isRead = "True";
	myVars.races.race177.event1.eventType = "@event1EventType@";
	myVars.races.race177.event2.id = "Lu_Id_a_9";
	myVars.races.race177.event2.type = "onmouseover";
	myVars.races.race177.event2.loc = "Lu_Id_a_9_LOC";
	myVars.races.race177.event2.isRead = "False";
	myVars.races.race177.event2.eventType = "@event2EventType@";
	myVars.races.race177.event1.executed= false;// true to disable, false to enable
	myVars.races.race177.event2.executed= false;// true to disable, false to enable
</script>

