<script id = "race211b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race211={};
	myVars.races.race211.varName="Lu_Id_li_18__onmouseover";
	myVars.races.race211.varType="@varType@";
	myVars.races.race211.repairType = "@RepairType";
	myVars.races.race211.event1={};
	myVars.races.race211.event2={};
	myVars.races.race211.event1.id = "Lu_Id_li_18";
	myVars.races.race211.event1.type = "onmouseover";
	myVars.races.race211.event1.loc = "Lu_Id_li_18_LOC";
	myVars.races.race211.event1.isRead = "True";
	myVars.races.race211.event1.eventType = "@event1EventType@";
	myVars.races.race211.event2.id = "Lu_Id_a_20";
	myVars.races.race211.event2.type = "onmouseover";
	myVars.races.race211.event2.loc = "Lu_Id_a_20_LOC";
	myVars.races.race211.event2.isRead = "False";
	myVars.races.race211.event2.eventType = "@event2EventType@";
	myVars.races.race211.event1.executed= false;// true to disable, false to enable
	myVars.races.race211.event2.executed= false;// true to disable, false to enable
</script>

