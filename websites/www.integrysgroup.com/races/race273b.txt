<script id = "race273b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race273={};
	myVars.races.race273.varName="Lu_Id_li_9__onmouseout";
	myVars.races.race273.varType="@varType@";
	myVars.races.race273.repairType = "@RepairType";
	myVars.races.race273.event1={};
	myVars.races.race273.event2={};
	myVars.races.race273.event1.id = "Lu_Id_a_11";
	myVars.races.race273.event1.type = "onmouseout";
	myVars.races.race273.event1.loc = "Lu_Id_a_11_LOC";
	myVars.races.race273.event1.isRead = "True";
	myVars.races.race273.event1.eventType = "@event1EventType@";
	myVars.races.race273.event2.id = "Lu_Id_a_11";
	myVars.races.race273.event2.type = "onmouseover";
	myVars.races.race273.event2.loc = "Lu_Id_a_11_LOC";
	myVars.races.race273.event2.isRead = "False";
	myVars.races.race273.event2.eventType = "@event2EventType@";
	myVars.races.race273.event1.executed= false;// true to disable, false to enable
	myVars.races.race273.event2.executed= false;// true to disable, false to enable
</script>

