<script id = "race150b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race150={};
	myVars.races.race150.varName="siteTopNavTab5__onmouseover";
	myVars.races.race150.varType="@varType@";
	myVars.races.race150.repairType = "@RepairType";
	myVars.races.race150.event1={};
	myVars.races.race150.event2={};
	myVars.races.race150.event1.id = "Lu_Id_li_24";
	myVars.races.race150.event1.type = "onmouseover";
	myVars.races.race150.event1.loc = "Lu_Id_li_24_LOC";
	myVars.races.race150.event1.isRead = "True";
	myVars.races.race150.event1.eventType = "@event1EventType@";
	myVars.races.race150.event2.id = "siteTopNavLink5";
	myVars.races.race150.event2.type = "onmouseover";
	myVars.races.race150.event2.loc = "siteTopNavLink5_LOC";
	myVars.races.race150.event2.isRead = "False";
	myVars.races.race150.event2.eventType = "@event2EventType@";
	myVars.races.race150.event1.executed= false;// true to disable, false to enable
	myVars.races.race150.event2.executed= false;// true to disable, false to enable
</script>

