<script id = "race81b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race81={};
	myVars.races.race81.varName="siteTopNavTab4__onmouseover";
	myVars.races.race81.varType="@varType@";
	myVars.races.race81.repairType = "@RepairType";
	myVars.races.race81.event1={};
	myVars.races.race81.event2={};
	myVars.races.race81.event1.id = "Lu_Id_a_24";
	myVars.races.race81.event1.type = "onmouseover";
	myVars.races.race81.event1.loc = "Lu_Id_a_24_LOC";
	myVars.races.race81.event1.isRead = "True";
	myVars.races.race81.event1.eventType = "@event1EventType@";
	myVars.races.race81.event2.id = "siteTopNavLink4";
	myVars.races.race81.event2.type = "onmouseover";
	myVars.races.race81.event2.loc = "siteTopNavLink4_LOC";
	myVars.races.race81.event2.isRead = "False";
	myVars.races.race81.event2.eventType = "@event2EventType@";
	myVars.races.race81.event1.executed= false;// true to disable, false to enable
	myVars.races.race81.event2.executed= false;// true to disable, false to enable
</script>

