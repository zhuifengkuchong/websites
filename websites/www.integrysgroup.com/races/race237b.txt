<script id = "race237b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race237={};
	myVars.races.race237.varName="Lu_Id_li_26__onmouseover";
	myVars.races.race237.varType="@varType@";
	myVars.races.race237.repairType = "@RepairType";
	myVars.races.race237.event1={};
	myVars.races.race237.event2={};
	myVars.races.race237.event1.id = "Lu_Id_li_26";
	myVars.races.race237.event1.type = "onmouseover";
	myVars.races.race237.event1.loc = "Lu_Id_li_26_LOC";
	myVars.races.race237.event1.isRead = "True";
	myVars.races.race237.event1.eventType = "@event1EventType@";
	myVars.races.race237.event2.id = "Lu_Id_a_28";
	myVars.races.race237.event2.type = "onmouseover";
	myVars.races.race237.event2.loc = "Lu_Id_a_28_LOC";
	myVars.races.race237.event2.isRead = "False";
	myVars.races.race237.event2.eventType = "@event2EventType@";
	myVars.races.race237.event1.executed= false;// true to disable, false to enable
	myVars.races.race237.event2.executed= false;// true to disable, false to enable
</script>

