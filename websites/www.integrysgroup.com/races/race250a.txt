<script id = "race250a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race250={};
	myVars.races.race250.varName="Lu_Id_li_2__onmouseout";
	myVars.races.race250.varType="@varType@";
	myVars.races.race250.repairType = "@RepairType";
	myVars.races.race250.event1={};
	myVars.races.race250.event2={};
	myVars.races.race250.event1.id = "Lu_Id_a_4";
	myVars.races.race250.event1.type = "onmouseover";
	myVars.races.race250.event1.loc = "Lu_Id_a_4_LOC";
	myVars.races.race250.event1.isRead = "False";
	myVars.races.race250.event1.eventType = "@event1EventType@";
	myVars.races.race250.event2.id = "Lu_Id_a_4";
	myVars.races.race250.event2.type = "onmouseout";
	myVars.races.race250.event2.loc = "Lu_Id_a_4_LOC";
	myVars.races.race250.event2.isRead = "True";
	myVars.races.race250.event2.eventType = "@event2EventType@";
	myVars.races.race250.event1.executed= false;// true to disable, false to enable
	myVars.races.race250.event2.executed= false;// true to disable, false to enable
</script>

