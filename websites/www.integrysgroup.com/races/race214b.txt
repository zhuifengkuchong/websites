<script id = "race214b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race214={};
	myVars.races.race214.varName="Lu_Id_li_19__onmouseover";
	myVars.races.race214.varType="@varType@";
	myVars.races.race214.repairType = "@RepairType";
	myVars.races.race214.event1={};
	myVars.races.race214.event2={};
	myVars.races.race214.event1.id = "Lu_Id_li_19";
	myVars.races.race214.event1.type = "onmouseover";
	myVars.races.race214.event1.loc = "Lu_Id_li_19_LOC";
	myVars.races.race214.event1.isRead = "True";
	myVars.races.race214.event1.eventType = "@event1EventType@";
	myVars.races.race214.event2.id = "Lu_Id_a_21";
	myVars.races.race214.event2.type = "onmouseover";
	myVars.races.race214.event2.loc = "Lu_Id_a_21_LOC";
	myVars.races.race214.event2.isRead = "False";
	myVars.races.race214.event2.eventType = "@event2EventType@";
	myVars.races.race214.event1.executed= false;// true to disable, false to enable
	myVars.races.race214.event2.executed= false;// true to disable, false to enable
</script>

