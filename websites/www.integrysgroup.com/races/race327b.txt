<script id = "race327b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race327={};
	myVars.races.race327.varName="Function[586].guid";
	myVars.races.race327.varType="@varType@";
	myVars.races.race327.repairType = "@RepairType";
	myVars.races.race327.event1={};
	myVars.races.race327.event2={};
	myVars.races.race327.event1.id = "Lu_Id_a_16";
	myVars.races.race327.event1.type = "onmouseover";
	myVars.races.race327.event1.loc = "Lu_Id_a_16_LOC";
	myVars.races.race327.event1.isRead = "True";
	myVars.races.race327.event1.eventType = "@event1EventType@";
	myVars.races.race327.event2.id = "Lu_Id_a_15";
	myVars.races.race327.event2.type = "onmouseover";
	myVars.races.race327.event2.loc = "Lu_Id_a_15_LOC";
	myVars.races.race327.event2.isRead = "False";
	myVars.races.race327.event2.eventType = "@event2EventType@";
	myVars.races.race327.event1.executed= false;// true to disable, false to enable
	myVars.races.race327.event2.executed= false;// true to disable, false to enable
</script>

