<script id = "race160b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race160={};
	myVars.races.race160.varName="siteTopNavTab1__onmouseover";
	myVars.races.race160.varType="@varType@";
	myVars.races.race160.repairType = "@RepairType";
	myVars.races.race160.event1={};
	myVars.races.race160.event2={};
	myVars.races.race160.event1.id = "Lu_Id_li_2";
	myVars.races.race160.event1.type = "onmouseover";
	myVars.races.race160.event1.loc = "Lu_Id_li_2_LOC";
	myVars.races.race160.event1.isRead = "True";
	myVars.races.race160.event1.eventType = "@event1EventType@";
	myVars.races.race160.event2.id = "siteTopNavLink1";
	myVars.races.race160.event2.type = "onmouseover";
	myVars.races.race160.event2.loc = "siteTopNavLink1_LOC";
	myVars.races.race160.event2.isRead = "False";
	myVars.races.race160.event2.eventType = "@event2EventType@";
	myVars.races.race160.event1.executed= false;// true to disable, false to enable
	myVars.races.race160.event2.executed= false;// true to disable, false to enable
</script>

