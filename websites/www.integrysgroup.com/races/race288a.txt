<script id = "race288a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race288={};
	myVars.races.race288.varName="Lu_Id_li_14__onmouseout";
	myVars.races.race288.varType="@varType@";
	myVars.races.race288.repairType = "@RepairType";
	myVars.races.race288.event1={};
	myVars.races.race288.event2={};
	myVars.races.race288.event1.id = "Lu_Id_a_16";
	myVars.races.race288.event1.type = "onmouseover";
	myVars.races.race288.event1.loc = "Lu_Id_a_16_LOC";
	myVars.races.race288.event1.isRead = "False";
	myVars.races.race288.event1.eventType = "@event1EventType@";
	myVars.races.race288.event2.id = "Lu_Id_a_16";
	myVars.races.race288.event2.type = "onmouseout";
	myVars.races.race288.event2.loc = "Lu_Id_a_16_LOC";
	myVars.races.race288.event2.isRead = "True";
	myVars.races.race288.event2.eventType = "@event2EventType@";
	myVars.races.race288.event1.executed= false;// true to disable, false to enable
	myVars.races.race288.event2.executed= false;// true to disable, false to enable
</script>

