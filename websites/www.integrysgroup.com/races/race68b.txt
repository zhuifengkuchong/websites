<script id = "race68b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race68={};
	myVars.races.race68.varName="Lu_Id_a_20__onmouseover";
	myVars.races.race68.varType="@varType@";
	myVars.races.race68.repairType = "@RepairType";
	myVars.races.race68.event1={};
	myVars.races.race68.event2={};
	myVars.races.race68.event1.id = "Lu_Id_a_20";
	myVars.races.race68.event1.type = "onmouseover";
	myVars.races.race68.event1.loc = "Lu_Id_a_20_LOC";
	myVars.races.race68.event1.isRead = "True";
	myVars.races.race68.event1.eventType = "@event1EventType@";
	myVars.races.race68.event2.id = "Lu_DOM";
	myVars.races.race68.event2.type = "onDOMContentLoaded";
	myVars.races.race68.event2.loc = "Lu_DOM_LOC";
	myVars.races.race68.event2.isRead = "False";
	myVars.races.race68.event2.eventType = "@event2EventType@";
	myVars.races.race68.event1.executed= false;// true to disable, false to enable
	myVars.races.race68.event2.executed= false;// true to disable, false to enable
</script>

