<script id = "race183b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race183={};
	myVars.races.race183.varName="Lu_Id_li_9__onmouseover";
	myVars.races.race183.varType="@varType@";
	myVars.races.race183.repairType = "@RepairType";
	myVars.races.race183.event1={};
	myVars.races.race183.event2={};
	myVars.races.race183.event1.id = "Lu_Id_li_9";
	myVars.races.race183.event1.type = "onmouseover";
	myVars.races.race183.event1.loc = "Lu_Id_li_9_LOC";
	myVars.races.race183.event1.isRead = "True";
	myVars.races.race183.event1.eventType = "@event1EventType@";
	myVars.races.race183.event2.id = "Lu_Id_a_11";
	myVars.races.race183.event2.type = "onmouseover";
	myVars.races.race183.event2.loc = "Lu_Id_a_11_LOC";
	myVars.races.race183.event2.isRead = "False";
	myVars.races.race183.event2.eventType = "@event2EventType@";
	myVars.races.race183.event1.executed= false;// true to disable, false to enable
	myVars.races.race183.event2.executed= false;// true to disable, false to enable
</script>

