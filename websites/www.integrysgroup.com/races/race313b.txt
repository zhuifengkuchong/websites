<script id = "race313b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race313={};
	myVars.races.race313.varName="Lu_Id_li_21__onmouseout";
	myVars.races.race313.varType="@varType@";
	myVars.races.race313.repairType = "@RepairType";
	myVars.races.race313.event1={};
	myVars.races.race313.event2={};
	myVars.races.race313.event1.id = "Lu_Id_a_23";
	myVars.races.race313.event1.type = "onmouseout";
	myVars.races.race313.event1.loc = "Lu_Id_a_23_LOC";
	myVars.races.race313.event1.isRead = "True";
	myVars.races.race313.event1.eventType = "@event1EventType@";
	myVars.races.race313.event2.id = "Lu_Id_a_23";
	myVars.races.race313.event2.type = "onmouseover";
	myVars.races.race313.event2.loc = "Lu_Id_a_23_LOC";
	myVars.races.race313.event2.isRead = "False";
	myVars.races.race313.event2.eventType = "@event2EventType@";
	myVars.races.race313.event1.executed= false;// true to disable, false to enable
	myVars.races.race313.event2.executed= false;// true to disable, false to enable
</script>

