<script id = "race30a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race30={};
	myVars.races.race30.varName="siteTopNavTab2__onmouseover";
	myVars.races.race30.varType="@varType@";
	myVars.races.race30.repairType = "@RepairType";
	myVars.races.race30.event1={};
	myVars.races.race30.event2={};
	myVars.races.race30.event1.id = "siteTopNavLink2";
	myVars.races.race30.event1.type = "onmouseover";
	myVars.races.race30.event1.loc = "siteTopNavLink2_LOC";
	myVars.races.race30.event1.isRead = "False";
	myVars.races.race30.event1.eventType = "@event1EventType@";
	myVars.races.race30.event2.id = "Lu_Id_a_9";
	myVars.races.race30.event2.type = "onmouseover";
	myVars.races.race30.event2.loc = "Lu_Id_a_9_LOC";
	myVars.races.race30.event2.isRead = "True";
	myVars.races.race30.event2.eventType = "@event2EventType@";
	myVars.races.race30.event1.executed= false;// true to disable, false to enable
	myVars.races.race30.event2.executed= false;// true to disable, false to enable
</script>

