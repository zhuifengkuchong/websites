<script id = "race336b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race336={};
	myVars.races.race336.varName="Function[586].guid";
	myVars.races.race336.varType="@varType@";
	myVars.races.race336.repairType = "@RepairType";
	myVars.races.race336.event1={};
	myVars.races.race336.event2={};
	myVars.races.race336.event1.id = "Lu_Id_a_23";
	myVars.races.race336.event1.type = "onmouseover";
	myVars.races.race336.event1.loc = "Lu_Id_a_23_LOC";
	myVars.races.race336.event1.isRead = "True";
	myVars.races.race336.event1.eventType = "@event1EventType@";
	myVars.races.race336.event2.id = "Lu_Id_a_22";
	myVars.races.race336.event2.type = "onmouseover";
	myVars.races.race336.event2.loc = "Lu_Id_a_22_LOC";
	myVars.races.race336.event2.isRead = "False";
	myVars.races.race336.event2.eventType = "@event2EventType@";
	myVars.races.race336.event1.executed= false;// true to disable, false to enable
	myVars.races.race336.event2.executed= false;// true to disable, false to enable
</script>

