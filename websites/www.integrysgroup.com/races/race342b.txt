<script id = "race342b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race342={};
	myVars.races.race342.varName="Function[586].guid";
	myVars.races.race342.varType="@varType@";
	myVars.races.race342.repairType = "@RepairType";
	myVars.races.race342.event1={};
	myVars.races.race342.event2={};
	myVars.races.race342.event1.id = "Lu_Id_a_28";
	myVars.races.race342.event1.type = "onmouseover";
	myVars.races.race342.event1.loc = "Lu_Id_a_28_LOC";
	myVars.races.race342.event1.isRead = "True";
	myVars.races.race342.event1.eventType = "@event1EventType@";
	myVars.races.race342.event2.id = "Lu_Id_a_27";
	myVars.races.race342.event2.type = "onmouseover";
	myVars.races.race342.event2.loc = "Lu_Id_a_27_LOC";
	myVars.races.race342.event2.isRead = "False";
	myVars.races.race342.event2.eventType = "@event2EventType@";
	myVars.races.race342.event1.executed= false;// true to disable, false to enable
	myVars.races.race342.event2.executed= false;// true to disable, false to enable
</script>

