<script id = "race325b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race325={};
	myVars.races.race325.varName="Function[586].guid";
	myVars.races.race325.varType="@varType@";
	myVars.races.race325.repairType = "@RepairType";
	myVars.races.race325.event1={};
	myVars.races.race325.event2={};
	myVars.races.race325.event1.id = "Lu_Id_a_14";
	myVars.races.race325.event1.type = "onmouseover";
	myVars.races.race325.event1.loc = "Lu_Id_a_14_LOC";
	myVars.races.race325.event1.isRead = "True";
	myVars.races.race325.event1.eventType = "@event1EventType@";
	myVars.races.race325.event2.id = "Lu_Id_a_13";
	myVars.races.race325.event2.type = "onmouseover";
	myVars.races.race325.event2.loc = "Lu_Id_a_13_LOC";
	myVars.races.race325.event2.isRead = "False";
	myVars.races.race325.event2.eventType = "@event2EventType@";
	myVars.races.race325.event1.executed= false;// true to disable, false to enable
	myVars.races.race325.event2.executed= false;// true to disable, false to enable
</script>

