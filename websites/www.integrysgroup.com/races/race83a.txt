<script id = "race83a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race83={};
	myVars.races.race83.varName="Lu_Id_a_24__onmouseover";
	myVars.races.race83.varType="@varType@";
	myVars.races.race83.repairType = "@RepairType";
	myVars.races.race83.event1={};
	myVars.races.race83.event2={};
	myVars.races.race83.event1.id = "Lu_DOM";
	myVars.races.race83.event1.type = "onDOMContentLoaded";
	myVars.races.race83.event1.loc = "Lu_DOM_LOC";
	myVars.races.race83.event1.isRead = "False";
	myVars.races.race83.event1.eventType = "@event1EventType@";
	myVars.races.race83.event2.id = "Lu_Id_a_24";
	myVars.races.race83.event2.type = "onmouseover";
	myVars.races.race83.event2.loc = "Lu_Id_a_24_LOC";
	myVars.races.race83.event2.isRead = "True";
	myVars.races.race83.event2.eventType = "@event2EventType@";
	myVars.races.race83.event1.executed= false;// true to disable, false to enable
	myVars.races.race83.event2.executed= false;// true to disable, false to enable
</script>

