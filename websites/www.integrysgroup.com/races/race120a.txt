<script id = "race120a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race120={};
	myVars.races.race120.varName="Lu_Id_li_10__onmouseover";
	myVars.races.race120.varType="@varType@";
	myVars.races.race120.repairType = "@RepairType";
	myVars.races.race120.event1={};
	myVars.races.race120.event2={};
	myVars.races.race120.event1.id = "Lu_Id_a_12";
	myVars.races.race120.event1.type = "onmouseover";
	myVars.races.race120.event1.loc = "Lu_Id_a_12_LOC";
	myVars.races.race120.event1.isRead = "False";
	myVars.races.race120.event1.eventType = "@event1EventType@";
	myVars.races.race120.event2.id = "Lu_Id_li_10";
	myVars.races.race120.event2.type = "onmouseover";
	myVars.races.race120.event2.loc = "Lu_Id_li_10_LOC";
	myVars.races.race120.event2.isRead = "True";
	myVars.races.race120.event2.eventType = "@event2EventType@";
	myVars.races.race120.event1.executed= false;// true to disable, false to enable
	myVars.races.race120.event2.executed= false;// true to disable, false to enable
</script>

