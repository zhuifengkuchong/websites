<script id = "race277b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race277={};
	myVars.races.race277.varName="Lu_Id_a_12__onmouseout";
	myVars.races.race277.varType="@varType@";
	myVars.races.race277.repairType = "@RepairType";
	myVars.races.race277.event1={};
	myVars.races.race277.event2={};
	myVars.races.race277.event1.id = "Lu_Id_a_12";
	myVars.races.race277.event1.type = "onmouseout";
	myVars.races.race277.event1.loc = "Lu_Id_a_12_LOC";
	myVars.races.race277.event1.isRead = "True";
	myVars.races.race277.event1.eventType = "@event1EventType@";
	myVars.races.race277.event2.id = "Lu_DOM";
	myVars.races.race277.event2.type = "onDOMContentLoaded";
	myVars.races.race277.event2.loc = "Lu_DOM_LOC";
	myVars.races.race277.event2.isRead = "False";
	myVars.races.race277.event2.eventType = "@event2EventType@";
	myVars.races.race277.event1.executed= false;// true to disable, false to enable
	myVars.races.race277.event2.executed= false;// true to disable, false to enable
</script>

