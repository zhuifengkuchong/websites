<script id = "race316a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race316={};
	myVars.races.race316.varName="Function[586].guid";
	myVars.races.race316.varType="@varType@";
	myVars.races.race316.repairType = "@RepairType";
	myVars.races.race316.event1={};
	myVars.races.race316.event2={};
	myVars.races.race316.event1.id = "Lu_Id_a_4";
	myVars.races.race316.event1.type = "onmouseover";
	myVars.races.race316.event1.loc = "Lu_Id_a_4_LOC";
	myVars.races.race316.event1.isRead = "False";
	myVars.races.race316.event1.eventType = "@event1EventType@";
	myVars.races.race316.event2.id = "Lu_Id_a_5";
	myVars.races.race316.event2.type = "onmouseover";
	myVars.races.race316.event2.loc = "Lu_Id_a_5_LOC";
	myVars.races.race316.event2.isRead = "True";
	myVars.races.race316.event2.eventType = "@event2EventType@";
	myVars.races.race316.event1.executed= false;// true to disable, false to enable
	myVars.races.race316.event2.executed= false;// true to disable, false to enable
</script>

