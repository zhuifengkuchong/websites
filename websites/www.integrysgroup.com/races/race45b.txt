<script id = "race45b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race45={};
	myVars.races.race45.varName="siteTopNavTab2__onmouseover";
	myVars.races.race45.varType="@varType@";
	myVars.races.race45.repairType = "@RepairType";
	myVars.races.race45.event1={};
	myVars.races.race45.event2={};
	myVars.races.race45.event1.id = "Lu_Id_a_14";
	myVars.races.race45.event1.type = "onmouseover";
	myVars.races.race45.event1.loc = "Lu_Id_a_14_LOC";
	myVars.races.race45.event1.isRead = "True";
	myVars.races.race45.event1.eventType = "@event1EventType@";
	myVars.races.race45.event2.id = "siteTopNavLink2";
	myVars.races.race45.event2.type = "onmouseover";
	myVars.races.race45.event2.loc = "siteTopNavLink2_LOC";
	myVars.races.race45.event2.isRead = "False";
	myVars.races.race45.event2.eventType = "@event2EventType@";
	myVars.races.race45.event1.executed= false;// true to disable, false to enable
	myVars.races.race45.event2.executed= false;// true to disable, false to enable
</script>

