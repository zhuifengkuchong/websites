<script id = "race43a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race43={};
	myVars.races.race43.varName="Lu_Id_li_11__onmouseover";
	myVars.races.race43.varType="@varType@";
	myVars.races.race43.repairType = "@RepairType";
	myVars.races.race43.event1={};
	myVars.races.race43.event2={};
	myVars.races.race43.event1.id = "Lu_window";
	myVars.races.race43.event1.type = "onload";
	myVars.races.race43.event1.loc = "Lu_window_LOC";
	myVars.races.race43.event1.isRead = "False";
	myVars.races.race43.event1.eventType = "@event1EventType@";
	myVars.races.race43.event2.id = "Lu_Id_a_13";
	myVars.races.race43.event2.type = "onmouseover";
	myVars.races.race43.event2.loc = "Lu_Id_a_13_LOC";
	myVars.races.race43.event2.isRead = "True";
	myVars.races.race43.event2.eventType = "@event2EventType@";
	myVars.races.race43.event1.executed= false;// true to disable, false to enable
	myVars.races.race43.event2.executed= false;// true to disable, false to enable
</script>

