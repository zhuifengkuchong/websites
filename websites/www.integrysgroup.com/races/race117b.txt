<script id = "race117b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race117={};
	myVars.races.race117.varName="siteTopNavTab2__onmouseover";
	myVars.races.race117.varType="@varType@";
	myVars.races.race117.repairType = "@RepairType";
	myVars.races.race117.event1={};
	myVars.races.race117.event2={};
	myVars.races.race117.event1.id = "Lu_Id_li_9";
	myVars.races.race117.event1.type = "onmouseover";
	myVars.races.race117.event1.loc = "Lu_Id_li_9_LOC";
	myVars.races.race117.event1.isRead = "True";
	myVars.races.race117.event1.eventType = "@event1EventType@";
	myVars.races.race117.event2.id = "siteTopNavLink2";
	myVars.races.race117.event2.type = "onmouseover";
	myVars.races.race117.event2.loc = "siteTopNavLink2_LOC";
	myVars.races.race117.event2.isRead = "False";
	myVars.races.race117.event2.eventType = "@event2EventType@";
	myVars.races.race117.event1.executed= false;// true to disable, false to enable
	myVars.races.race117.event2.executed= false;// true to disable, false to enable
</script>

