<script id = "race210b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race210={};
	myVars.races.race210.varName="siteTopNavTab3__onmouseover";
	myVars.races.race210.varType="@varType@";
	myVars.races.race210.repairType = "@RepairType";
	myVars.races.race210.event1={};
	myVars.races.race210.event2={};
	myVars.races.race210.event1.id = "Lu_Id_li_18";
	myVars.races.race210.event1.type = "onmouseover";
	myVars.races.race210.event1.loc = "Lu_Id_li_18_LOC";
	myVars.races.race210.event1.isRead = "True";
	myVars.races.race210.event1.eventType = "@event1EventType@";
	myVars.races.race210.event2.id = "siteTopNavLink3";
	myVars.races.race210.event2.type = "onmouseover";
	myVars.races.race210.event2.loc = "siteTopNavLink3_LOC";
	myVars.races.race210.event2.isRead = "False";
	myVars.races.race210.event2.eventType = "@event2EventType@";
	myVars.races.race210.event1.executed= false;// true to disable, false to enable
	myVars.races.race210.event2.executed= false;// true to disable, false to enable
</script>

