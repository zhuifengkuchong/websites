/*

jQuery textareaMaxlength by Chris Bartak
version 0.1

options: 
	message (string)
	showMessage (boolean) 

*/
(function( $ ) {
	
  $.fn.textareaMaxlength = function(options) {
  
	var maxlength;

	return this.each(function(){

		var $this = $(this);

		if (maxlength = $(this).attr('maxlength')){
			
			$this.wrap('<div class="jquery_maxlength"/>');
			var $parent = $this.parent();
			$parent.append('<div></div>');
			var showMessage = options.showMessage || true;
			var $countdown = $parent.find('div');
			var message = options.message || '{MAXLENGTH} character limit. {REMAINING} characters remaining.';
	
    		$(this).on('keyup', function(){
	
				var charsLeft = maxlength - $this.val().length;
		
				if (charsLeft < 1){
					$this.val($this.val().substring(0,maxlength));
					$countdown.addClass('lengthLimitReached');
				}else{
					$countdown.removeClass('lengthLimitReached');
				}
		
				if (showMessage){
					
					$countdown.html(message.replace('{MAXLENGTH}',maxlength).replace('{REMAINING}',(charsLeft >= 0)? charsLeft : 0));
				}
		
			});
	
		}	

	});

  };

})( jQuery );
