// Google Analytics account profile defined in global.js

// looks for anchors with data-ga-vpv attribute and adds trackPageView click event
function gaProcessVPVs($root) {
    $root.find("a[data-ga-vpv]").each(function () {
        var $this = $(this),
            url = $this.attr("data-ga-vpv");

        if (url) {
            $this.click(function () {
                pageTracker._trackPageview(url);
            });
        }
    });
}

function gaProcessFileURLs($root) {
    // document extensions to look for
    var fileTypes = ['ai','doc','docx','eps','jpg','mp3','pdf','ppt','pptx','psd','xls','xlsx','xml','zip'];
    var gaDocumentVpvPrefix = '/vpv/files';
    var gaOutgoingVpvPrefix = '/vpv/outgoing';
    var extRegExp = new RegExp('\\.(' + fileTypes.join('|') + ')(\\?.*)?$'); // RegExp incorporates the tracked filetypes above
    var localAbsoluteUrlRegExp = new RegExp('^https{0,1}:\/\/(((www|careers|investor)\.)?chevron.com|' + document.domain + ')(:\\d+){0,1}');

    $root.find('a[href]').each(function () {
        var a = this;
        var $a = $(a);

        // Get URL, set to lowercase and remove anchor
        var href = $a.attr('href').toLowerCase().replace(/#.*$/, '').replace(/\/$/, '');

        // Strip protocol, host and port from URL if it's actually the current domain (e.g. www.chevron.com)
        if (localAbsoluteUrlRegExp.test(href)) {
            href = href.replace(localAbsoluteUrlRegExp, '');
        }

        if (href != null && href != '' && !href.match(/^javascript:/)) { // Skip empty or JavaScript URLs

            if (href.match(/^https?:\/\//)) { // External link
                $a.click(function (e) {
                    href = href.replace(/^https?:\/\/(www\.)?/, '/').replace(/\/(default\.aspx)?$/, ''); //cleanup for nice GA reports
                    pageTracker._trackPageview(gaOutgoingVpvPrefix + href);

                    // Check for social network
                    var socialNetwork = null;
                    switch (href) {
                        case "/facebook.com/chevron":
                            socialNetwork = "FB";
                            break;
                        case "/linkedin.com/company/chevron":
                            socialNetwork = "LI";
                            break;
                        case "/twitter.com/chevron":
                            socialNetwork = "TW";
                            break;
                        case "/youtube.com/chevron":
                            socialNetwork = "YT";
                            break;
                    }
                    if (socialNetwork != null) {
                        var linkSource = "BODY";
                        if ($a.parents("#RightCol").length > 0) {
                            linkSource = "RR";
                        } else if ($a.parents(".socialIcons").length > 0) {
                            linkSource = "FT";
                        }

                        pageTracker._trackEvent("socialMedia", socialNetwork, linkSource);
                        pageTracker._setCustomVar(2, "SocialMedia", socialNetwork, 1);

                    }
                    if (!$a.hasClass("newWindow")) {
                        setTimeout('document.location = "' + a.href + '"', 100);
                        e.preventDefault();
                        e.stopPropagation();
                    }
                });
            } else if (extRegExp.test(href)) { // Check for extension
                $a.click(function (e) {

                    // Clear out prefix used in DownloadHyperLink
                    if (/^.*\?p=/.test(href)) {
                        href = href.replace(/^.*\?p=/, '');
                        setTimeout('document.location = "' + a.href + '"', 100);
                        e.preventDefault();
                        e.stopPropagation();
                    }

                    // If no leading slash, then merge with current URL
                    if (href.charAt(0) != '/') {
                        href = location.pathname.replace(/\/[^\/]*$/, '/') + href;
                    }

                    // Clean any ".." folders.  Replace the first instance of "/dirname/../" with "/". Repeat until no change occurred.
                    var tmpHref;
                    do {
                        tmpHref = href;
                        href = href.replace(/\/[^\/]*[^\/\.]+[^\/]*\/\.\.\//, '/');
                    } while (tmpHref != href);

                    // Remove redundant "/./"
                    href = href.replace(/\/\.\//, '/');
                    pageTracker._trackPageview(gaDocumentVpvPrefix + href);

                });
            }
        }
    });
}

// HTML5 video player events for Google Analytics tracking
function gaGetVideoType(url) {
    var ext = 'unknown_html5';
    if (url != null) {
        var extMatch = /\.([a-z\d]+)(\?.*)?$/.exec(url.toLowerCase());
        if (extMatch != null) {
            ext = extMatch[1];
        }
    }
    return ext;
}

function gaTrackVideoEvent(videoEvent, actionLabel) {
    pageTracker._trackEvent("VideoPlayer", $(videoEvent.target).attr('x-data-trackingid'), actionLabel + "_" + gaGetVideoType(videoEvent.target.currentSrc));
}

function gaProcessVideo($root) {
    $root.find('video').each(function () {
        // 30seconds_reached
        $(this).bind('timeupdate', function (evt) {
            if (Math.floor(evt.target.currentTime.toFixed(3)) >= 30) {
                if ($(this).attr('x-data-past30seconds') === 'false') {
                    gaTrackVideoEvent(evt, "30seconds_reached");
                    $(this).attr('x-data-past30seconds', 'true'); // Prevent duplicate events past the first 30 seconds
                }
            }
        }, false);

        // pausebutton_click
        $(this).bind('pause', function (evt) {
            // Log and track whether the 'Pause' button has been triggered.
            if (evt.target.currentTime != evt.target.duration) { // Prevent false pause event when video reaches end
                gaTrackVideoEvent(evt, "pausebutton_click");
            }
        }, false);

        // playbutton_click
        $(this).bind('play', function (evt) {
            pageTracker._trackPageview('/vpv/videoid/' + $(this).attr('x-data-trackingid'));
            gaTrackVideoEvent(evt, "playbutton_click");
        }, false);

        // video_complete
        $(this).bind('ended', function (evt) {
            gaTrackVideoEvent(evt, "video_complete");
        }, false);

    });
}

var init_ga_social_tracking = function (w) {
    // see http://code.google.com/p/analytics-api-samples/source/browse/trunk/src/tracking/javascript/v5/social/ga_social_tracking.js
    w._ga = w._ga || {};
    w._gaq = w._gaq || [];
    
    w._ga.getSocialActionTrackers_ = function (network, socialAction, opt_target, opt_pagePath) {
        return function () {
            // window._gat defined in ga.js
            var trackers = w._gat._getTrackers();
            for (var i = 0, tracker; tracker = trackers[i]; i++) {
                tracker._trackSocial(network, socialAction, opt_target, opt_pagePath);
            }
        };
    };

    // Note: This will not track facebook buttons using the iframe method.
    w._ga.trackFacebook = function (opt_pagePath) {
        try {
            // window.FB defined by fb:likebutton
            if (w.FB && w.FB.Event && w.FB.Event.subscribe) {
                w.FB.Event.subscribe('edge.create', function (opt_target) {
                    w._gaq.push(w._ga.getSocialActionTrackers_('facebook', 'like', opt_target, opt_pagePath));
                });
                w.FB.Event.subscribe('edge.remove', function (opt_target) {
                    w._gaq.push(w._ga.getSocialActionTrackers_('facebook', 'unlike', opt_target, opt_pagePath));
                });
                //? Are FB Send widgets used anywhere?
                w.FB.Event.subscribe('http://www.chevron.com/js/message.send', function (opt_target) {
                    w._gaq.push(w._ga.getSocialActionTrackers_('facebook', 'send', opt_target, opt_pagePath));
                });
            }
        } catch (e) {}
    };

    w._ga.trackTwitterHandler_ = function (intent_event, opt_pagePath) {
        var opt_target; //Default value is undefined
        if (intent_event && intent_event.type === 'tweet' || intent_event.type === 'click') {
            if (intent_event.target.nodeName === 'IFRAME') {
                opt_target = w._ga.extractParamFromUri_(intent_event.target.src, 'url');
            }
            var socialAction = intent_event.type + ((intent_event.type === 'click') ? '-' + intent_event.region : ''); //append the type of click to action
            w._gaq.push(w._ga.getSocialActionTrackers_('twitter', socialAction, opt_target, opt_pagePath));
        }
    };

    w._ga.trackTwitter = function (opt_pagePath) {
        var intent_handler = function (intent_event) {
            w._ga.trackTwitterHandler_(intent_event, opt_pagePath);
        };

        //? Are Twitter widgets used anywhere?
        if (w.twttr && w.twttr.events && w.twttr.events.bind) {
            //bind twitter Click and Tweet events to Twitter tracking handler
            w.twttr.events.bind('click', intent_handler);
            w.twttr.events.bind('tweet', intent_handler);
        }
    };

    w._ga.extractParamFromUri_ = function (uri, paramName) {
        if (!uri) {
            return;
        }
        var regex = new RegExp('[\\?&#]' + paramName + '=([^&#]*)'),
            params = regex.exec(uri);
        if (params != null) {
            return unescape(params[1]);
        }
        return;
    };

    // track Facebook:
    w._ga.trackFacebook();
    // track Twitter:
    w._ga.trackTwitter();
};

$(function () {
    if (typeof (pageTracker) != 'undefined') {
        gaProcessVPVs($(document));
        gaProcessFileURLs($(document));
        gaProcessVideo($(document));

    } // End if pageTracker doesn't exist query.
});   // End jQuery ready handler.

$(function () {
    if (typeof (pageTracker) != 'undefined') {
        gaProcessFileURLs($(document));

        //thickbox links
        $("a.thickbox, area.thickbox, input.thickbox").click(function () {
            pageTracker._trackPageview($(this).attr('href'));
        });

        //topnav links
        $("#TopNav a").click(function () {
            //if topnav link is also external, remove http// part
            var href = $(this).attr('href');
            if (href.match(/^http/)) {
                href = href.replace(/^https?\:\//, '');
            }
            pageTracker._trackPageview('/topnav' + href);
        });

        //homepage drawers links      
        $("a.homeDrawer").click(function () {
            pageTracker._trackPageview('/homedrawer/' + getValueFromClass(this, 'hd'));
        });

        //catchcode links      
        $("a.catchCode").click(function () {
            pageTracker._trackPageview('/catchcode/' + getValueFromClass(this, 'ga'));
        });
    }

    // initialize social tracking if _gat global is loaded
    if (window._gat) {
        init_ga_social_tracking(window);
    }
});


// Track SilverLight 2.0 
function onLoad() {
    var version = getSilverlightVersion();
    if (version) { __utmSetVar(version); }
}

function getSilverlightVersion() {
    var version = '';
    var container = null;
    try {
        var control = null;
        if (window.ActiveXObject) {
            control = new ActiveXObject('AgControl.AgControl');
        }
        else {
            if (navigator.plugins['Silverlight Plug-In']) {
                container = document.createElement('div');
                document.body.appendChild(container);
                container.innerHTML = '<embed type="application/x-silverlight" src="data:," />';
                control = container.childNodes[0];
            }
        }
        if (control) {
            if (control.isVersionSupported('2.0')) { version = 'Silverlight/2.0'; }
            else if (control.isVersionSupported('1.0')) { version = 'Silverlight/1.0'; }
        }
    }
    catch (e) { }
    if (container) {
        document.body.removeChild(container);
    }
    return version;
}