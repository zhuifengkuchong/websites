//**************************************************************************************************************//
// Homepage banner behavior.

function HomepageFeaturesHeader($contentDisplayItems, $navItems, defaultStoryClass) {

    if ($contentDisplayItems.length <= 1) {
        return;
    }

    this.contentItems = [];
    this.isCycling = false;
    this.currentContent = null;
    this.cycleTimeoutId;
    this.mouseTimeoutIntervalId;
    this.mouseActivityTimer = 0;
    this.defaultFeatureDuration = 6;
    this.cycleRestartTimeout = 60;

    for (var i = 0; i < $contentDisplayItems.length; i++) {
        this.contentItems.push(new HomepageFeature($($contentDisplayItems[i]), $($navItems[i]), i, this));
    }

    $contentDisplayItems.css("z-index", 1).addClass("contentItemDeselected").removeClass("contentItemSelected");
    $navItems.css("z-index", 7);

    var engine = this;
    this.scopedResetMouseActivityTimer = function () { engine.resetMouseActivityTimer() };
    this.scopedNextContentItem = function () { engine.nextContentItem() };
    this.scopedCheckMouseActivityTimer = function () { engine.checkMouseActivityTimer() };

    var defaultStoryIndex = 0;
    if (defaultStoryClass != undefined) {
        var defaultStoryDiv = $contentDisplayItems.filter(defaultStoryClass);
        if (defaultStoryDiv.length > 0) {
            defaultStoryIndex = $contentDisplayItems.index(defaultStoryDiv[0]);
        }
    }

    if (this.contentItems.length > 1) {
        this.startCycle(defaultStoryIndex);
    } else {
        this.showContentAt(defaultStoryIndex);
    }
}

HomepageFeaturesHeader.prototype = {

    nextContentItem: function () {
        this.showContentAt((this.currentContent.index + 1) % this.contentItems.length);
    },

    showContentAt: function (i) {
        this.showContent(this.contentItems[i]);
    },

    showContent: function (contentItem, instant) {
        var previousContent = this.currentContent;
        this.currentContent = contentItem;
        contentItem.select(previousContent, instant);
        if (this.isCycling) {
            this.cycleTimeoutId = setTimeout(this.scopedNextContentItem, contentItem.duration * 1000);
        }
    },

    resetMouseActivityTimer: function () {
        this.mouseActivityTimer = 0;
    },

    checkMouseActivityTimer: function () {
        this.mouseActivityTimer++;
        if (this.mouseActivityTimer == this.cycleRestartTimeout) {
            this.startCycle();
        }
    },

    TrackFeatureClick: function (feature) {
        this.TrackEvent('HomePageStories', feature.id + '{Rotating' + (this.isCycling ? 'ON' : 'OFF') + '}', (feature.index + 1) + 'of' + this.contentItems.length, feature.duration);
    },

    TrackEvent: function (cat, evt, lbl, data) {
        if (typeof (window.pageTracker) != 'undefined') {
            window.pageTracker._trackEvent(cat, evt, lbl, data);
        }
    },

    startCycle: function (i) {
        if (!this.isCycling) {
            this.isCycling = true;
            $(document).unbind("mousemove", this.scopedResetMouseActivityTimer);
            clearTimeout(this.cycleTimeoutId);
            clearInterval(this.mouseTimeoutIntervalId);
            if (i != undefined) {
                this.showContentAt(i);
            } else {
                this.nextContentItem();
            }
        }
    },

    stopCycle: function () {
        if (this.isCycling) {
            clearTimeout(this.cycleTimeoutId);
            clearInterval(this.mouseTimeoutIntervalId);
            this.isCycling = false;
            this.mouseActivityTimer = 0;
            $(document).bind("mousemove", this.scopedResetMouseActivityTimer);
            this.mouseTimeoutIntervalId = setInterval(this.scopedCheckMouseActivityTimer, 1000);
        }
    }
}

///////////////////////////////////////////////////////////////////////////
//

function HomepageFeature($contentItem, $navItem, index, engine) {
    var feature = this;
    this.index = index;
    this.engine = engine;

    // Content links
    this.$contentItem = $contentItem;
    this.$contentAnchors = $contentItem.find("a").andSelf().filter("a");
    this.$contentAnchors.click(function (e) {
        feature.onContentItemClick(e);
        e.stopPropagation();
        e.preventDefault();
    });

    // Nav links
    this.$navItem = $navItem;
    this.$navAnchors = $navItem.children("a").andSelf().filter("a");
    this.$navAnchors.attr("href", "#");
    this.$navAnchors.click(function (e) {
        feature.onNavItemClick(e);
        e.stopPropagation();
        e.preventDefault();
    });

    // ID
    this.id = $contentItem.attr("data-feature-id"); 

    // Duration
    this.duration = parseFloat($contentItem.attr("data-duration"));
    if (isNaN(this.duration) || this.duration == 0) {
        this.duration = engine.defaultFeatureDuration;
    }

}

HomepageFeature.prototype = {
    select: function (previousContent, instant) {
        if (previousContent) {
            previousContent.deselect();
        }

        var duration = instant ? 10 : 1000;
        this.$navItem.filter(".navItemUnselected").removeClass("navItemUnselected");
        this.$navItem.addClass("navItemSelected");
        this.$contentItem.stop().css("z-index", 9).addClass("contentItemSelected").removeClass("contentItemDeselected").fadeIn(duration);
        /*this.$contentItem.stop().css("z-index", 9).fadeIn(duration, function () {
            if (previousContent && previousContent.$contentItem != this) {
                previousContent.$contentItem.fadeOut(0);
            }
        });*/
    },

    deselect: function () {
        this.$navItem.addClass("navItemUnselected");
        this.$navItem.filter(".navItemSelected").removeClass("navItemSelected");
        this.$contentItem.stop().css("z-index", 1).addClass("contentItemDeselected").removeClass("contentItemSelected").fadeOut(10);
    },

    onContentItemClick: function (e) {
        this.engine.TrackFeatureClick(this);
        var href = null;
        if (e != undefined) {
            href = e.target.href;
        } else {
            href = this.$contentAnchors[0].href;
        }
        setTimeout(function () { document.location = href; }, 100);
    },

    onNavItemClick: function (e) {
        e.preventDefault();
        if (e.target.className != "navItemSelected") {
            this.engine.stopCycle();
            this.engine.showContent(this, true);
        }
    }
}

$().ready(function () {
    mainHeader = new HomepageFeaturesHeader(
	    $("#Features .feature"),
	    $("#Features a"),
        '.defaultFeature'
    );
});