/**************************************************************************************************************/
// Main drawer behavior, affects all 'accordion' drawers throughout site (but not 'promo' drawers on Home page).
function rAccordin() {
    this.initialize();
}
rAccordin.prototype = {
    initialize: function () {
        var engine = this;
        this.$accordion = $("div.accordion");
        if (this.$accordion.length > 0) {
            this.$allHeaders = this.$accordion.children("h3");
            this.$allDrawers = this.$accordion.children("div");
            if (this.$allHeaders.length > 0 && this.$allDrawers.length > 0) {

                // Wire up toggle link (if it exists)
                this.$toggleLink = $('#toggleDrwsLnk');
                this.$toggleLink.click(function (e) {
                    engine.toggleAllDrawers();
                });

                // Set first drawer to open
                var $defaultDrawer = $(this.$allDrawers[0]);

                // Check for "closeFirst" class to disable initial open
                if ($defaultDrawer.hasClass('closeFirst')) {
                    $defaultDrawer = undefined;
                }

                // Check query string for first drawer ID
                try {
                    var qs = new Querystring();
                    if (qs.get("d")) {
                        $defaultDrawer = this.$accordion.children('#' + qs.get("d"));
                    }
                } catch (e) { };

                // Check location hash for first drawer ID
                if (location.hash != undefined && location.hash != '') {
                    $defaultDrawer = this.$accordion.children(location.hash);
                }

                // Wire up mouse click events
                this.$allHeaders.children('a').click(function (e) {
                    var $anchor = $(this);
                    if (location.search != undefined && location.search != '') {
                        e.preventDefault();
                        e.stopPropagation();
                        location = location.href.replace(/\?.*/, $anchor.attr('href'));
                    } else {
                        var $header = $anchor.parent();
                        var $drawer = $header.next('div');
                        var wasVisible = $drawer.css("display") != "none";
                        if (wasVisible) {
                            e.preventDefault();
                            e.stopPropagation();
                        }
                        $drawer.slideToggle('fast', function () {
                            engine.setVisibility($drawer, !wasVisible);
                        });
                    }
                });

                // Hide all drawes and open default if set
                this.hideAllDrawers(this.$allDrawers);
                if ($defaultDrawer != undefined && $defaultDrawer.length > 0) {
                    this.showDrawers($defaultDrawer);
                    if (location.hash != undefined && location.hash != '') {
                        location.hash = $defaultDrawer[0].id;
                    }
                }
            }
        }
    },

    setVisibility: function ($objs, visibility) {
        if ($objs != undefined) {
            $objs.each(function (i) {
                //  alert(this.id + '.visible = ' + visibility);
                var $this = $(this);
                var $drawer;
                var $header;
                if ($this.is("h3")) {
                    $header = $this;
                    $drawer = $this.next('div');
                } else {
                    $header = $this.prev('h3');
                    $drawer = $this;
                }

                if (visibility) {
                    $drawer.show().removeClass("printOnly").css("display", "");
                    $header.removeClass("closed").addClass("open");
                } else {
                    $drawer.hide().addClass("printOnly").css("display", "");
                    $header.removeClass("open").addClass("closed");
                }
            });

        }
        this.allDrawersOpen = (this.$allDrawers.filter('.printOnly').length == 0);
        this.$toggleLink.text(this.allDrawersOpen ? 'Hide All' : 'Show All');
    },

    showDrawers: function ($objs) {
        this.setVisibility($objs, true);
    },

    hideDrawers: function ($objs) {
        this.setVisibility($objs, false);
    },

    showAllDrawers: function () {
        this.showDrawers(this.$allDrawers);
    },

    hideAllDrawers: function () {
        this.hideDrawers(this.$allDrawers);
    },

    toggleAllDrawers: function () {
        this.allDrawersOpen = !this.allDrawersOpen;
        this.setVisibility(this.$allDrawers, this.allDrawersOpen);
    }
}

// from http://adamv.com/dev/javascript/querystring
// For passing QS parameters to have a particular drawer open when page renders.
// Ex: http://www.chevron.com/news/publications/default.aspx?d=a2  <-renders page with drawer 2 open
function Querystring(qs) {
    this.params = new Object();
    this.get = Querystring_get;

    if (qs == null)
        qs = location.search.substring(1, location.search.length);

    if (qs.length == 0) return;

    qs = qs.replace(/\+/g, ' ');
    var args = qs.split('&');

    for (var i = 0; i < args.length; i++) {
        var value;
        var pair = args[i].split('=');
        var name = unescape(pair[0]);
        if (pair.length == 2)
            value = unescape(pair[1]);
        else
            value = name;
        this.params[name] = value;
    }
}

function Querystring_get(key, default_) {
    if (default_ == null) default_ = null;
    var value = this.params[key];
    if (value == null) value = default_;
    return value;
}

$().ready(function () {
    setTimeout(function () {
        window.drawers = new rAccordin();
    }, 100);
});