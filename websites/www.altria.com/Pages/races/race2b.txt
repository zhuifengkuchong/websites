<script id = "race2b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race2={};
	myVars.races.race2.varName="Tree[0x7fd64f339458]:ctl00_SearchBox_ctl03";
	myVars.races.race2.varType="@varType@";
	myVars.races.race2.repairType = "@RepairType";
	myVars.races.race2.event1={};
	myVars.races.race2.event2={};
	myVars.races.race2.event1.id = "ctl00_SearchBox_S6F789EBA_InputKeywords";
	myVars.races.race2.event1.type = "onfocus";
	myVars.races.race2.event1.loc = "ctl00_SearchBox_S6F789EBA_InputKeywords_LOC";
	myVars.races.race2.event1.isRead = "True";
	myVars.races.race2.event1.eventType = "@event1EventType@";
	myVars.races.race2.event2.id = "ctl00_SearchBox_ctl03";
	myVars.races.race2.event2.type = "ctl00_SearchBox_ctl03__parsed";
	myVars.races.race2.event2.loc = "ctl00_SearchBox_ctl03_LOC";
	myVars.races.race2.event2.isRead = "False";
	myVars.races.race2.event2.eventType = "@event2EventType@";
	myVars.races.race2.event1.executed= false;// true to disable, false to enable
	myVars.races.race2.event2.executed= false;// true to disable, false to enable
</script>

