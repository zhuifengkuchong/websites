//used to show approve/reject dialog
function showApproveAll() {
    var ctx = SP.ClientContext.get_current();
    var ItemIds = "";
    //get current list id
    var listId = SP.ListOperation.Selection.getSelectedList();
    //get all selected list items
    var selectedItems = SP.ListOperation.Selection.getSelectedItems(ctx);

    //collect selected item ids
    for (var i = 0; i < selectedItems.length; i++) {
        ItemIds += selectedItems[i].id + ",";
    }
    //prepare cutom approval page with listid 
    //and selected item ids passed in querystring
    var pageUrl = SP.Utilities.Utility.getLayoutsPageUrl(
        '/ALCS.AltriaCom.PublishingResources/ApproveRejectSelection.aspx?ids=' + ItemIds + '&listid=' + listId);
    var options = SP.UI.$create_DialogOptions();
    options.autosize = true;
    options.url = pageUrl;
    options.dialogReturnValueCallback = Function.createDelegate(null, OnDialogClose);
    SP.UI.ModalDialog.showModalDialog(options);
}

//used to determine whether the 'approve/reject selection' 
//ribbon will be enalbed or disabled
function enableApprovalAll() {
    var ctx = SP.ClientContext.get_current();
    return SP.ListOperation.Selection.getSelectedItems(ctx).length > 1;
}

//called on dialog closed
function OnDialogClose(result, target) {
    //if ok button is clicked in dialog, reload the grid.
    SP.UI.ModalDialog.RefreshPage(SP.UI.DialogResult.OK);
}