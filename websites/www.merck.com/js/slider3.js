//=====================================================
// slider3.js
// Version 1.8
//
// Version 1.8 Changes
// - Removed debug lines
// Version 1.7 Changes
//  + Added "firstTimeRunning" parameter to checkWidth() function
// Version 1.6 Changes
//  + Concole debug lines removed
// Version 1.5 Changes
//  + Added ability to use different images based on media query resolution changes
//  + Fixed IE Issues regarding mediaQuery issue. ( no support for matchMedia )
//  + Fixed IE issue by manually pulling in CSS style sheet depending on media query device/screen resolution
// Version 1.4 Changes
//  + Added even mode code to prevent dual text display issue
// Version 1.3 Changes
//  + Added mode code to prevent dual text display issue
// Version 1.2 Changes
//  + Added code to ensure circles DIV at original height ( they were scaling between slides when animated )
//  + Hide descriptionArea div when entering new slide to prevent dual text display issue
// Version 1.1 Changes
//  + Updated to resolve issue with new jQuery. 
//    Uses "!$.support.opacity " instead of "$.browser"
//=====================================================


//====================================================
//Page variables START
//====================================================
var delayBetweenSlides = 8000;
var allowUserToChangeSlidesWhileAnimationIsRunning = false;
var autoScroll = true;
var currentSlideIdx = 0;
var firstLoad = true;
var isAnimating = false;
var descriptionBlockAnimating = false;
var mql1 = ""; /* for handling MediaQueryList object (large)  */
var mql2 = ""; /* for handling MediaQueryList object (medium) */
var mql3 = ""; /* for handling MediaQueryList object (small)  */
var mql4 = ""; /* for handling MediaQueryList object (tiny)   */
var sliderElementCopy;

//====================================================
//Page variables END
//====================================================



//====================================================
// JQuery Document Ready Block START
//====================================================
$(document).ready(function() {

    //Disable caching of AJAX responses
    $.ajaxSetup ({
        cache: false
    });

    //Hook up "left arrow"
    $("#leftScrollArrow").click(function(){ autoScroll = false; scrollLeft(); });

    //Hook up "right arrow"
    $("#rightScrollArrow").click(function(){ autoScroll = false; scrollRight(); });
 
    //make a "clean" copy of the slider DIV so we can re-create the slider at different sizes later
    sliderElementCopy = $("#slider").html();
   
    //For modern browsers, use media queries and matchMedia to set and detect device/size changes
    try{   
        //set up matchMediaLists
        mql1 = window.matchMedia("(min-width: 1200px)");
        mql2 = window.matchMedia("(min-width: 768px) and (max-width: 1199px)");
        mql3 = window.matchMedia("(min-width: 479px) and (max-width: 767px)");
        mql4 = window.matchMedia("(max-width: 480px)");

        //init slider image size based on initial media
        initMediaChangeLarge(mql1);
        initMediaChangeMedium(mql2);
        initMediaChangeSmall(mql3);
        initMediaChangeTiny(mql4);

        //monitor media size changes
        mql1.addListener(handleMediaChangeLarge);
        mql2.addListener(handleMediaChangeMedium);
        mql3.addListener(handleMediaChangeSmall);
        mql4.addListener(handleMediaChangeTiny);
       
    }catch(err){
        //...Otherwise...
        //Prorbably IE (which does not support "matchMedia") so set up manual matching
        //and initial sizing
        checkWidth(true);

        //monitor width/resolution changes
        $(window).resize(checkWidth(false));
    }



});
//====================================================
// JQuery Document Ready Block END
//====================================================


//===============================================================================
//===============================================================================
//                            Functions below
//===============================================================================
//===============================================================================


//This function is only called with IE browsers to ensure proper device/size
//changes. It:
//1. Dynamically pulls in the proper CSS stylesheet to use
//2. Re-creates the slideshow for the propwer size
function checkWidth(firstTimeRunning) {
    var $window = $(window);
    var windowsize = $window.width();
    if (windowsize >= 1200) {
        $("#dynamicCSS").attr("href","Unknown_83_filename"/*tpa=http://www.merck.com/js/css/large_desktop.css*/);
        initSlider('ul.bjqs-large',firstTimeRunning);
    }else if(windowsize >= 768 && windowsize <= 1199){
        $("#dynamicCSS").attr("href","Unknown_83_filename"/*tpa=http://www.merck.com/js/css/portrait_table_2_land_desk.css*/);
        initSlider('ul.bjqs-medium',firstTimeRunning);
    }else if(windowsize >= 479 && windowsize <= 767){
        $("#dynamicCSS").attr("href","Unknown_83_filename"/*tpa=http://www.merck.com/js/css/landscape_phone_2_portrait_table.css*/);
        initSlider('ul.bjqs-small',firstTimeRunning);
    }else if(windowsize <= 480){
        $("#dynamicCSS").attr("href","Unknown_83_filename"/*tpa=http://www.merck.com/js/css/landscape_phone_down.css*/);
        initSlider('ul.bjqs-tiny',firstTimeRunning);
    }else{
        $("#dynamicCSS").attr("href","Unknown_83_filename"/*tpa=http://www.merck.com/js/css/large_desktop.css*/);
        initSlider('ul.bjqs-large',firstTimeRunning);
    }
}


//These functions are called for browsers other than IE.
//They determine the initial slider size to use depending on screen resolution/device
function initMediaChangeLarge(mediaQueryList) {
  if (mediaQueryList.matches) {
    initSlider('ul.bjqs-large',true);
  }
}
function initMediaChangeMedium(mediaQueryList) {
  if (mediaQueryList.matches) {
    initSlider('ul.bjqs-medium',true);
  }
}
function initMediaChangeSmall(mediaQueryList) {
  if (mediaQueryList.matches) {
    initSlider('ul.bjqs-small',true);
  }
}
function initMediaChangeTiny(mediaQueryList) {
  if (mediaQueryList.matches) {
   initSlider('ul.bjqs-tiny',true);
  }
}


//These functions are called for browsers other than IE.
//They are called when the the browser detects a media query change ( which is being monitored )
function handleMediaChangeLarge(mediaQueryList) {
  if (mediaQueryList.matches) {
   // $('#slider').changeImageListSource("large");
    initSlider('ul.bjqs-large',false);
  }
}
function handleMediaChangeMedium(mediaQueryList) {
  if (mediaQueryList.matches) {
   // $('#slider').changeImageListSource("medium");
    initSlider('ul.bjqs-medium',false);
  }
}
function handleMediaChangeSmall(mediaQueryList) {
  if (mediaQueryList.matches) {
   // $('#slider').changeImageListSource("small");
    initSlider('ul.bjqs-small',false);
  }
}
function handleMediaChangeTiny(mediaQueryList) {
  if (mediaQueryList.matches) {
   // $('#slider').changeImageListSource("tiny");
    initSlider('ul.bjqs-tiny',false);
  }
}


//Initialize the slider 
function initSlider(imageSize, setTimer)
{
    descriptionBlockAnimating = false;
    isAnimating = false;
    currentSlideIdx = 0;
 
    //restore a "clean" copy of the slider div
    $('#slider').html(sliderElementCopy);

    //Set up Slider
    $('#slider').bjqs({
        'height' : 396,
        'width' : 1336,
        'animtype' : 'slide',
        'animduration' : 450, 
        'animspeed' : 1000, 
        'automatic' : false, 
        'responsive' : false,
        'showcontrols' : false,
        'showmarkers' : true,
        'keyboardnav' : false, // enable keyboard navigation
        'hoverpause' : false,   // pause the slider on hover
        'slideImageSizeId': imageSize
    });

     //Initialize slide data
    updateSlideData();

    //We only want to set up the timer during the initial slider setup.
    //subsequent slider tare-downs and re-builds ( during media-query / size / device changes)
    //should not mess with the pre-establishes timer
    if( setTimer == true){
        //set up auto-scroll functionality
        setInterval( intervalFunction, delayBetweenSlides);
    }
}



function intervalFunction()
{

    if( autoScroll == true){
      scrollRight();
    }
}

//Scrolls left
function scrollLeft()
{
    //Don't accept a slide change if we are animating
    if( (isAnimating == true && allowUserToChangeSlidesWhileAnimationIsRunning == false) || ( descriptionBlockAnimating == true )){ 
        return;
    }

    //keep track of current index;
    if( currentSlideIdx > 0){
        currentSlideIdx--;
    }else{
        currentSlideIdx = $('#slider').numSlides() - 1;
    }

    //go to previous slide
    $('#slider').goTo('previous',null); 

    //Update title, description, etc for next slide
    updateSlideData();
    
}//end scrollLeft


//Scrolls right
function scrollRight()
{
   
    //Don't accept a slide change if we are animating
    if( (isAnimating == true && allowUserToChangeSlidesWhileAnimationIsRunning == false ) || ( descriptionBlockAnimating == true )){ 
        return;
    }

    //keep track of current index;
    if( currentSlideIdx < $('#slider').numSlides() -1){
        currentSlideIdx++;
    }else{
      currentSlideIdx=0;
    }
    
    //go to next slide
    $('#slider').goTo('forward',null); 

    //Update title, description, etc for next slide
    updateSlideData();
    
}//end scrollRight



function updateSlideData()
{    
    //Hide description area
    $('.descriptionArea').hide();

    //ensure circles DIV starts right-aligned in case we are not animating it for this slide
    $('#circles').css("right",50);

    //ensure height of circles DIV remains at its original size
    //(for some reason it was scaling)
    var origCirclesDivSize = $('#circles').css("height");
    $('#circles').css("height",origCirclesDivSize);

    //hide text before switching to new text
    $('.descriptionArea').hide();

    //See if we are animating this slide or not
    var circleAnimationElems = $('.circleAnimation');
    var circleAnimation = circleAnimationElems.eq(currentSlideIdx).text();

    if(circleAnimation == "true"){
        //We want the circles DIV to start on the left of the image area.
        //Since we dont know how big the area is ( due to browser resizing ) we need to cacculate
        //it based on current values.
        var imageAreaWidth = $("#imageArea").css("width");
        var rightImageBumnperWidth = $("#rightImageBumper").css("width");
        var totalWidth = parseInt(imageAreaWidth) -  parseInt(rightImageBumnperWidth);
        var totalWidthString = totalWidth + "px";

        //See if we are animating this slide or not
        var circleAnimationSpeedElems = $('.circleAnimationSpeed');
        var circleAnimationSpeed = circleAnimationSpeedElems.eq(currentSlideIdx).text();
      
        //Now we can animate it
        
        //reset the circles to be on the left         
        $('#circles').css("right",totalWidthString);
        //Now animate them
        isAnimating = true;
        $("#leftScrollArrow").css("cursor","wait");
        $("#rightScrollArrow").css("cursor","wait");
        $('#circles').animate({  right : 50 }, parseInt(circleAnimationSpeed), 
            function() { 
                isAnimating = false;
                $("#leftScrollArrow").css("cursor","pointer");
                $("#rightScrollArrow").css("cursor","pointer");
            });
    }
        
    

    //retrieve the various description elements
    var descriptions = $('.descriptionArea');
    var currDescription = descriptions.eq(currentSlideIdx);

    var circleOpacityElems = $('.circleOpacity');
    var currcircleOpacity = circleOpacityElems.eq(currentSlideIdx).text();

    //make sure all are initially hidden and set to a low z-index
    descriptions.hide();
    descriptions.css("z-index",0);
    //we hode a hidden dev to take advantage of the timing we can get by un-hiding it
    //via JQuery UI
    $("#dummyDivForTimingFunctions").hide();

    //display the relevant description block
    descriptionBlockAnimating = true;
    $("#leftScrollArrow").css("cursor","wait");
    $("#rightScrollArrow").css("cursor","wait");
    $(currDescription).css("z-index",1);
    $("#dummyDivForTimingFunctions").fadeIn(1000, function() {$(currDescription).show( "slide", { direction: "down" }, 500, 
        function(){ descriptionBlockAnimating = false;
                    $("#leftScrollArrow").css("cursor","pointer");
                    $("#rightScrollArrow").css("cursor","pointer");
        });});

    //if opacity not supported ( i.e. if IE 8 or under ), keep opacity = 1.0
    if ( !$.support.opacity ) { 
        currcircleOpacity='1.0';
    }
    //set circles opacity
    $("#circles").css("opacity",currcircleOpacity);
}//end updateSlideData
