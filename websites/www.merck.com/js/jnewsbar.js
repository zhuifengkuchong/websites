if (typeof Object.create !== "function") {
    Object.create = function (e) {
        function t() {}
        t.prototype = e;
        return new t
    }
}(function (e, t, n, r) {
    var i = {
        init: function (t, n) {
            var r = this;
            r.elem = n;
            r.$elem = e(n);
            r.options = e.extend({
                onHover: function () {}
            }, e.fn.jNewsbar.options, t);
            r.options.pauseTime = r.options.pauseTime + r.options.animSpeed;
            r.buildFrag();
            r.display()
        },
        buildFrag: function () {
            var e = this;
            e.$elem.addClass("jnewsbar jnews-" + e.options.theme + " jnews-" + e.options.position + " jnews-" + e.options.effect).css(e.options.position, 0).show();
            e.$elem.find("h2").wrap('<div class="jnewsbar-title"></div>').css({
                "font-size": e.options.height - 5 + "px",
                height: e.options.height + "px",
                "line-height": e.options.height + "px"
            });
            e.$elem.find("ul").wrap("<div></div>").addClass("jnewsbar-items");
            e.$elem.css({
                height: e.options.height + "px",
                "line-height": e.options.height + "px"
            });
            e.$elem.find("li").css({
                height: e.options.height + "px",
                "line-height": e.options.height + "px",
                overflow: "hidden"
            });
            e.$elem.append(e.buildControls())
        },
        buildControls: function () {
            var e = this;
            return '<div class="jnewsbar-navigate"><a href="#" class="prev" alt="Previous"><i class="icon-chevron-left icon-white"></i></a> <a href="#" class="next" alt="Next"><i class="icon-chevron-right icon-white"></i></a></div><div class="jnews-toggle"><a href="#" class="toggle"  style="height:' + e.options.height + "px; line-height:" + e.options.height + 'px;" >Toggle</a></div>'
        },
        display: function () {
            var t = this;
            t.reverseOrder();
            t.pauseOnhover();
            t.$elem.find(".jnewsbar-navigate .next").on("click", function (e) {
                switch (t.options.effect) {
                case "slideDown":
                    t.prev();
                    break;
                default:
                    t.next();
                    break
                }
                t.removeInterval();
                t.setInterval();
                e.preventDefault()
            });
            t.$elem.find(".jnewsbar-navigate .prev").on("click", function (e) {
                switch (t.options.effect) {
                case "slideDown":
                    t.next();
                    break;
                default:
                    t.prev();
                    break
                }
                t.removeInterval();
                t.setInterval();
                e.preventDefault()
            });
            t.$elem.find(".jnews-toggle .toggle").on("click", function (n) {
                var r = t.options.height * t.options.toggleItems;
                if (t.$elem.hasClass("opened")) {
                    t.$elem.removeClass("opened");
                    e(this).removeClass("open");
                    t.$elem.animate({
                        height: t.options.height + "px"
                    }, t.options.animSpeed).find(".jnewsbar-title h2, .jnewsbar-navigate").animate({
                        height: t.options.height + "px",
                        "line-height": t.options.height + "px"
                    }, t.options.animSpeed)
                } else {
                    t.$elem.addClass("opened");
                    e(this).addClass("open");
                    t.$elem.animate({
                        height: r + "px"
                    }, t.options.animSpeed).find(".jnewsbar-title h2, .jnewsbar-navigate").animate({
                        height: r + "px",
                        "line-height": r + "px"
                    }, t.options.animSpeed)
                }
                n.preventDefault()
            });
            t.setInterval()
        },
        tick: function () {
            var t = this;
            switch (t.options.effect) {
            case "slideDown":
                t.prev();
                break;
            case "fade":
                t.$elem.find("li:first").animate({
                    opacity: 0
                }, t.options.animSpeed, function () {
                    e(this).appendTo(t.$elem.find("ul.jnewsbar-items")).css("opacity", 1)
                });
                break;
            default:
                t.next();
                break
            }
        },
        setInterval: function () {
            var e = this;
            $setInterval = setInterval(function () {
                e.tick()
            }, e.options.pauseTime);
            e.$elem.attr("data-id", $setInterval)
        },
        removeInterval: function () {
            var e = this;
            t.clearInterval(e.$elem.attr("data-id"))
        },
        pauseOnhover: function () {
            var t = this;
            var n = 0;
            if (t.options.pauseOnHover) {
                t.$elem.find("li").on("hover mouseover mouseup", function () {
                    t.removeInterval();
                    if (n == 0) {
                        e(this).addClass("hovered");
                        t.options.onHover.call(t)
                    }
                    n++
                }).on("mouseout mousedown", function () {
                    t.setInterval();
                    t.$elem.find("li").removeClass("hovered");
                    n = 0
                })
            }
        },
        prev: function () {
            var e = this;
            e.$elem.find(".jnewsbar-items").css("margin-top", -e.options.height + "px");
            e.$elem.find(".jnewsbar-items li:last").prependTo(e.$elem.find(".jnewsbar-items"));
            e.$elem.find(".jnewsbar-items").animate({
                marginTop: 0 + "px"
            }, e.options.animSpeed, "linear", function () {})
        },
        next: function () {
            var t = this;
            t.$elem.find(".jnewsbar-items").animate({
                marginTop: -t.options.height + "px"
            }, t.options.animSpeed, "linear", function () {
                e(this).find("li:first").appendTo(this).hide().fadeIn(300);
                e(this).css("margin-top", 0)
            })
        },
        reverseOrder: function () {
            var t = this;
            if (t.options.effect == "slideDown") {
                var n = t.$elem.find(".jnewsbar-items");
                var r = n.children("li");
                n.append(r.get().reverse());
                e(t.$elem.find(".jnewsbar-items li:last")).prependTo(n)
            }
        }
    };
    e.fn.jNewsbar = function (t) {
        return this.each(function () {
            var n = Object.create(i);
            n.init(t, this);
            e.data(this, "jNewsbar", n)
        })
    };
    e.fn.jNewsbar.options = {
        position: "bottom",
        effect: "slideUp",
        height: 23,
        animSpeed: 600,
        pauseTime: 3e3,
        pauseOnHover: true,
        toggleItems: 5,
        theme: "dark"
    }
})(jQuery, window, document)