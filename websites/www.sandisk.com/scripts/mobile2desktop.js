/***********************************************************************/
// Function Name    : sMobToDsktopUrl
// Input Parameter  : sVal - This pointer of this function.
// Output Parameter : Nil
// Return Value     : Nil
// Description      : Redirect the Mobile Site to Desktop Site.
/***********************************************************************/
function sMobToDsktopUrl(sVal) {  
  var sBasUrl = location.href;
  var sApndDat, nApndStr;
  var bApdFlg, nApndVal;  

  bFlg = false;
  bApdFlg = false;
  var nStrtPos = sBasUrl.indexOf("?" + "UserViewMode" + "=");
    if (nStrtPos == -1)
    {
      var nApndVal = sBasUrl.indexOf("?");
      
      if (nApndVal != -1) {
        bApdFlg = true;
        var nApndStr = sBasUrl.indexOf("&" + "UserViewMode" + "=");
      }
      else {
        bFlg = true;
      }
    }
    if (nStrtPos == -1)
    {
      sBasUrl = null;
    }
    else
    {
      nStrtPos = sBasUrl.indexOf("=", nStrtPos) + 1;
     
      var nEndPos = sBasUrl.indexOf(";", nStrtPos);
      if (nEndPos == -1)
      {
        nEndPos = sBasUrl.length;
      }
     
      sBasUrl = unescape(sBasUrl.substring(nStrtPos, nEndPos));     
  }  
    sUrl = location.href;   
    var sMode = "UserViewMode=" + escape("Desktop");
    if (bFlg) {     
     sUrl = sUrl + "?" + sMode;
    }
    else if(bApdFlg) {
      if (nApndStr != -1) {        
        sUrl = sUrl.replace("&UserViewMode="+"Mobile", "&UserViewMode=" + "Desktop");
      }
      else {
       sUrl = sUrl + "&" + sMode;
      }
    }
    else {
      if ((sBasUrl == "Desktop") || (sBasUrl == "Mobile")) {        
        sUrl = sUrl.replace("?UserViewMode=" + "Mobile", "?UserViewMode=" + "Desktop");
      }
      else {
       sUrl = location.href;
      }
    }
   window.location = sUrl;
  }
/***********************************************************************/
  

/***********************************************************************/
// Function Name    : sDsktopToMobUrl
// Input Parameter  : sVal - This pointer of this function.
// Output Parameter : Nil
// Return Value     : Nil
// Description      : Redirect the Desktop Site to Mobile Site.
/***********************************************************************/
function sDsktopToMobUrl(sVal) {
  var sBasUrl = location.href;  
  var sApndDat, nApndStr;
  var bApdFlg, nApndVal;
  bFlg = false;
  bApdFlg = false;
  var nStrtPos = sBasUrl.indexOf("?" + "UserViewMode" + "=");

    if (nStrtPos == -1)
    {
      var nApndVal = sBasUrl.indexOf("?");
     
      if (nApndVal != -1) {
        bApdFlg = true;
        var nApndStr = sBasUrl.indexOf("&" + "UserViewMode" + "=");
      }
      else {
        bFlg = true;
      }
    }
    if (nStrtPos == -1)
    {
      sBasUrl = null;
    }    
    else
    {
      nStrtPos = sBasUrl.indexOf("=", nStrtPos) + 1;

      var nEndPos = sBasUrl.indexOf(";", nStrtPos);
      if (nEndPos == -1)
      {
        nEndPos = sBasUrl.length;
      }

      sBasUrl = unescape(sBasUrl.substring(nStrtPos, nEndPos));

  }
  var sMode = "UserViewMode=" + escape("Mobile");
    sUrl = location.href;
    if (bFlg) {     
     sUrl = sUrl + "?" + sMode;
    }
    else if(bApdFlg) {
      if (nApndStr != -1) {   
        sUrl = sUrl.replace("&UserViewMode=" + "Desktop", "&UserViewMode=" + "Mobile");
      }
      else {
       sUrl = sUrl + "&" + sMode;
      }
    }
    else {
     if ((sBasUrl == "Desktop") || (sBasUrl == "Mobile")) {        
        sUrl = sUrl.replace("?UserViewMode=" + "Desktop", "?UserViewMode=" + "Mobile");
     }
     else {
       sUrl = location.href;
     }
   }
   window.location = sUrl;    
 }
/***********************************************************************/

