/* Global Variables */
var sliderPosition=0; /* position of slider */
var sliderCount =4; /* 4 videos per slide */
var arrayVideos ; / * Get all the videos collection in array */
var videoTitles ;

function playVideo(videoUrl, title)
{
  
  try
  {
   var overlay = $('#videos');  
   $('#fade').show();
   $(overlay).show();
   $(overlay).css("position","absolute");
    if(videoTitles.length ==0)
    {
      videoTitles = $(".videoThumbTitle").get();  
    }
    $(overlay).css("top", (($(window).height() - $(overlay).outerHeight()) / 2) + $(window).scrollTop() + "px");
    $(overlay).css("left", (($(window).width() - $(overlay).outerWidth()) / 2) + $(window).scrollLeft() + "px");
     $("#title1").text(title);    
     $('.videoThumbImage').css("border",'2px solid #666666'); // De-select all the thumb images.  
      $('.videoThumbImage:hidden').css("border",'2px solid #666666');
     if($("#youtube-player-container").children().length >0)
     {
     
       $('#youtube-player-container').tubeplayer('play', videoUrl);        
     }
     else
     {
         
          $("#youtube-player-container").tubeplayer({
             width: 500, // the width of the player
             height: 305, // the height of the player
             autoPlay:1,
             allowFullScreen: "true", // true by default, allow user to go full screen
             initialVideo: videoUrl, // the video that is loaded into the player
             preferredQuality: "default",// preferred quality: default, small, medium, large, hd720
            modestbranding:0,  // For legal reasons, youtbue player to have youtube logo.
             onPlay: function(id){}, // after the play method is called
             onPause: function(){}, // after the pause method is called
             onStop: function(){}, // after the player is stopped
             onSeek: function(time){}, // after the video has been seeked to a defined point
             onMute: function(){}, // after the player is muted
             onUnMute: function(){}, // after the player is unmuted
             onPlayerEnded:function(){$("#youtube-player-container").tubeplayer("stop");}
             // when the player returns a state of ended
        });
      slideDivs(0); /* Reset the slider */
    }
      $('.videoThumbImage').css("border",'2px solid #666666'); // De-select all the thumb images.  
     $('#'+videoUrl).css("border",'2px solid #f00'); // select the current selection.
  }
  catch(err)
  {
    alert(err.msg);
  }
}

function closeOverlay()
{
   sliderPosition=0;
   if($("#youtube-player-container").children().length >0)
   {
     $("#youtube-player-container").tubeplayer("stop");
     $("#youtube-player-container").tubeplayer("destroy");  
     $('#youtube-player-container').empty();    
   }
   $('#fade').hide();
   $('#videos').hide();
}

function slideDivs(index)
{
       if(arrayVideos.length ==0)
       {          
          arrayVideos = $(".videoThumbImage").get(); / * Get all the videos collection in array */
        }
        if( videoTitles.length <=0)
        {
           videoTitles = $(".videoThumbTitle").get();
        }        
    if(index ==0)
    {
      $("#AncPrev").hide();
      if(arrayVideos.length >4)
      {
        $("#AncNext").show();
      }
    }       
     $("#video1").html(arrayVideos [index]);     
     $("#video1").append(videoTitles[index]);
     $("#video2").html(arrayVideos [index +1]);
      $("#video2").append(videoTitles[index+1]);
     $("#video3").html(arrayVideos [index  +2]);
       $("#video3").append(videoTitles[index+2]);
     $("#video4").html(arrayVideos [index +3]);
      $("#video4").append(videoTitles[index+3]);
}

function Prev()
{
   if(sliderPosition > 0)
   {
    sliderPosition -=1; // Reverse the position by 1.
     slideDivs(sliderPosition);
    $("#AncNext").show();
   }  
}

function Next()
{
  if($('.videoThumb').length >sliderCount)
  {
      sliderPosition+=1;
     $("#AncPrev").show();  
        slideDivs(sliderPosition);    
   }  
  if(arrayVideos.length <=sliderPosition+4)
  {
     $("#AncNext").hide();      
  }  
}
/*
 * Redirect to the search page when the enter key is pressed inside the search textbox
 */
function EnterKeyGoesToSearch(e, searchPageURL, inputElement)
{
  var keynum;
  if(window.event) // IE
  {
    keynum = e.keyCode;
  }
  else if(e.which) // Netscape/Firefox/Opera
  {
    keynum = e.which;
  }
  if (keynum == 13)
  {
   if (inputElement.value == '' || inputElement.value == 'Enter Search Text')
    {
      inputElement.value = 'Enter Search Text';
      return false;
    }
    else
    {
    window.location = 'http://srch.sandisk.com/search' + '?q=' + inputElement.value + '&client=wwwsandiskcom&output=xml_no_dtd&proxystylesheet=wwwsandiskcom&sort=date%3AD%3AL%3Ad1&oe=UTF-8&ie=UTF-8&ud=1&exclude_apps=1&site=wwwsandiskcom';
    return false;
    }
  }
  else
  {
    return true;
  }
}

function endsWith(testString, endingString){
      if(endingString.length > testString.length) return false;
      return testString.indexOf(endingString)==(testString.length-endingString.length);
}

/*
 * Click button when enter key is pressed inside the search textbox
 */
function EnterKeyClicksButton(e, buttonElementID)
{
  var keynum;
  if(window.event) // IE
  {
    keynum = e.keyCode;
  }
  else if(e.which) // Netscape/Firefox/Opera
  {
    keynum = e.which;
  }
  if (keynum == 13)
  {
    if (document.getElementById(buttonElementID).value == '' || inputElement.value == 'Enter Search Text')
    {
      document.getElementById(buttonElementID).value = 'Enter Search Text';
      return false;
    }
    else
    {
    var buttonElement = document.getElementById(buttonElementID);
    buttonElement.click();
    return false;
    }
  }
  else
  {
    return true;
  }
}
/*
  * END Redirect to the search page when the enter key is pressed inside the search textbox
  */
function getCookie(c_name)
{
var i,x,y,ARRcookies=document.cookie.split(";");
for (i=0;i<ARRcookies.length;i++)
  {
  x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
  y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
  x=x.replace(/^\s+|\s+$/g,"");
  if (x==c_name)  
  {
      return unescape(y.toLowerCase());
  }
  }
}
function setCookie(c_name,value,exdays)
{
  var exdate=new Date();
  exdate.setDate(exdate.getDate() + exdays);
 var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
  document.cookie=c_name + "=" + c_value;  
}
function languageSelection(languageCode) {
    ga('send', 'event', 'Country Selector','go',languageCode);
    document.location.href = 'http://www.sandisk.com/global-sites/sethomewebsite/?redirectURL=' + languageCode;
}
function ignoreLanguageconversion(languageCode)
{
    ga('send', 'event', 'Country Selector','dismiss',languageCode);
    setCookie("defaultPage", "ignore"+languageCode,365);
     $("#regionalLanguage").hide();        
}
function getbrowserLanguage() {
    if (navigator.appName == 'Netscape')
        return navigator.language;
    else
        return navigator.browserLanguage;
}
function tick(){
    $('#ticker li:first').animate({'opacity':0}, 200, function () { $(this).appendTo($('#ticker')).css('opacity', 1); });
  }setInterval(function(){ tick () }, 8000);

$(function() {
    

 // Handler for .ready() called.
  var capacitySelected;
  var colorSelected ;
  var activeDiv;
    /* code for browser language detection*/
    if(location.host =="http://www.sandisk.com/scripts/www.sandisk.com" || location.host =="http://www.sandisk.com/scripts/qa.sandisk.com" )
    {
      var conversionMessage ='';
      var language = getbrowserLanguage().toLowerCase();
      var cookieValue = getCookie("defaultPage");
      if(cookieValue =="" ||cookieValue == 'undefined' ||cookieValue == null  )
      {
        cookieValue ='';     
      }
      if (language.indexOf('de') > -1 && cookieValue.indexOf('de') == -1
      && location.hostname.toLowerCase().indexOf('de') == -1) { / * Germany Site */
          $("#regionalLanguage").show();        
          $("#languageSelection").attr("onclick","languageSelection('DE');");
          $("#ignoreLanguage").attr("onclick","ignoreLanguageconversion('DE');");
          conversionMessage ="Diese Website anzeigen in Deutsch";
          $("#txtConversionOption").text(conversionMessage);     
          $("#languageSelection").text("WEITER");
          $("#ignoreLanguage").text("Überspringen");                                                            
      }
      else if (language.indexOf('it') > -1 && cookieValue.indexOf('it') == -1
      && location.hostname.toLowerCase().indexOf('it') == -1){ /* Italian Site */
        $("#regionalLanguage").show();        
        $("#languageSelection").attr("onclick","languageSelection('IT');");
        $("#ignoreLanguage").attr("onclick","ignoreLanguageconversion('IT');");
        conversionMessage ="Visualizza questo sito web in italiano";
          $("#txtConversionOption").text(conversionMessage);     
          $("#languageSelection").text("VAI");
          $("#ignoreLanguage").text("Ignora");
      }
      else if (language.indexOf('fr') > -1 && cookieValue.indexOf('fr') == -1
      && location.hostname.toLowerCase().indexOf('fr') == -1){/* France Site */
          $("#regionalLanguage").show();        
          $("#languageSelection").attr("onclick","languageSelection('FR');");
          $("#ignoreLanguage").attr("onclick","ignoreLanguageconversion('FR');");
          conversionMessage ="Voir ce site en français";
          $("#txtConversionOption").text(conversionMessage);     
          $("#languageSelection").text("ENTRER");
          $("#ignoreLanguage").text("Rejeter");
      }
      else if(language.indexOf('ru') > -1 && cookieValue.indexOf('ru') == -1
      && location.hostname.toLowerCase().indexOf('ru') == -1) {/* Russia site */
          $("#regionalLanguage").show();                   
          $("#languageSelection").attr("onclick","languageSelection('RU');");        
          $("#ignoreLanguage").attr("onclick","ignoreLanguageconversion('RU');");
          conversionMessage ="Смотреть этот сайт на русском";
          $("#txtConversionOption").text(conversionMessage);     
          $("#languageSelection").text("Перейти");
          $("#ignoreLanguage").text("Пропустить");      
      }
     else if (language.indexOf('pt-br') > -1 && cookieValue.indexOf('br') == -1
      && location.hostname.toLowerCase().indexOf('br') == -1){/* Brazil Site */
          $("#regionalLanguage").show();        
          $("#languageSelection").attr("onclick","languageSelection('BR');");
          $("#ignoreLanguage").attr("onclick","ignoreLanguageconversion('BR');");
          conversionMessage ="Exibir este site em portugués";
          $("#txtConversionOption").text(conversionMessage);     
          $("#languageSelection").text("Ir");
          $("#ignoreLanguage").text("Dispensar");
      }
      else if(language.indexOf('es-ar') > -1 && cookieValue.indexOf('la') == -1
      && location.hostname.toLowerCase().indexOf('la') == -1) {/*Spanish aregentina Language */
          $("#regionalLanguage").show();        
          $("#languageSelection").attr("onclick","languageSelection('LA');");
          $("#ignoreLanguage").attr("onclick","ignoreLanguageconversion('LA');");
          conversionMessage ="Mira este sitio web en español";
          $("#txtConversionOption").text(conversionMessage);     
          $("#languageSelection").text("IR");
          $("#ignoreLanguage").text("Rechazar");
      }
      else if(language.indexOf('es-mx') > -1 && cookieValue.indexOf('mx') == -1
      && location.hostname.toLowerCase().indexOf('mx') == -1) {/*Spanish Mexican Language */
          $("#regionalLanguage").show();        
          $("#languageSelection").attr("onclick","languageSelection('MX');");
          $("#ignoreLanguage").attr("onclick","ignoreLanguageconversion('MX');");
          conversionMessage ="Mira este sitio web en español";
          $("#txtConversionOption").text(conversionMessage);     
          $("#languageSelection").text("IR");
          $("#ignoreLanguage").text("Rechazar");
      }
      else if(language.indexOf('es-cl') > -1 && cookieValue.indexOf('cl') == -1
      && location.hostname.toLowerCase().indexOf('cl') == -1){/* Spanish Chile Language*/
          $("#regionalLanguage").show();        
          $("#languageSelection").attr("onclick","languageSelection('CL');");
          $("#ignoreLanguage").attr("onclick","ignoreLanguageconversion('CL');");
          conversionMessage ="Mira este sitio web en español";
          $("#txtConversionOption").text(conversionMessage);     
          $("#languageSelection").text("IR");
          $("#ignoreLanguage").text("Rechazar");
      }
      else if (language.indexOf('es') > -1 && cookieValue.indexOf('es') == -1
      && location.hostname.toLowerCase().indexOf('es') == -1) { /* Spanish Site (ES) */
          $("#regionalLanguage").show();        
          $("#languageSelection").attr("onclick","languageSelection('ES');");
          $("#ignoreLanguage").attr("onclick","ignoreLanguageconversion('ES');");
          conversionMessage ="Mira este sitio web en español";
          $("#txtConversionOption").text(conversionMessage);     
          $("#languageSelection").text("IR");
          $("#ignoreLanguage").text("Rechazar");
      }
       else if(language.indexOf('ar') > -1 && cookieValue.indexOf('ar') == -1
      && location.hostname.toLowerCase().indexOf('ar') == -1) { /*  Arabic site */
          $("#regionalLanguage").show();        
          $("#languageSelection").attr("onclick","languageSelection('AR');");
          $("#ignoreLanguage").attr("onclick","ignoreLanguageconversion('AR');");
          conversionMessage ="عرض هذا الموقع باللغة العربية";
          $("#txtConversionOption").text(conversionMessage);     
          $("#languageSelection").text("ذهاب");
          $("#ignoreLanguage").text("استبعاد");
      }
      else if (language.indexOf('zh-tw') > -1 && cookieValue.indexOf('tw') == -1
      && location.hostname.toLowerCase().indexOf('tw') == -1) { /* Taiwan China Language */
          $("#regionalLanguage").show();        
          $("#languageSelection").attr("onclick","languageSelection('TW');");
          $("#ignoreLanguage").attr("onclick","ignoreLanguageconversion('TW');");
          conversionMessage ="View this website in Taiwan";
          $("#txtConversionOption").text(conversionMessage);     
          $("#languageSelection").text("GO");
          $("#ignoreLanguage").text("Dismiss");      
      }
      else if (language.indexOf('zh-hk') > -1 && cookieValue.indexOf('hk') == -1
      && location.hostname.toLowerCase().indexOf('hk') == -1) { /* HongKong China Language */
          $("#regionalLanguage").show();        
          $("#languageSelection").attr("onclick","languageSelection('HK');");
          $("#ignoreLanguage").attr("onclick","ignoreLanguageconversion('HK');");
          conversionMessage ="觀看這個網站在中國";
          $("#txtConversionOption").text(conversionMessage);     
          $("#languageSelection").text("前往");
          $("#ignoreLanguage").text("解除");
          
      }
     else if (language.indexOf('cn') > -1 && cookieValue.indexOf('cn') == -1
      && location.hostname.toLowerCase().indexOf('cn') == -1) { /* China Language */
          $("#regionalLanguage").show();        
          $("#languageSelection").attr("onclick","languageSelection('CN');");
          $("#ignoreLanguage").attr("onclick","ignoreLanguageconversion('CN');");
           conversionMessage ="观看这个网站在中国";
          $("#txtConversionOption").text(conversionMessage);     
          $("#languageSelection").text("前往");
          $("#ignoreLanguage").text("放弃");
      }
      else if (language.indexOf('ko') > -1 && cookieValue.indexOf('kr') == -1
      && location.hostname.toLowerCase().indexOf('kr') == -1) { /* Korea Language */
          $("#regionalLanguage").show();        
          $("#languageSelection").attr("onclick","languageSelection('KR');");
          $("#ignoreLanguage").attr("onclick","ignoreLanguageconversion('KR');");
          conversionMessage ="한글이 웹 사이트보기";
          $("#txtConversionOption").text(conversionMessage);     
          $("#languageSelection").text("이동");
          $("#ignoreLanguage").text("취소");      
      }
      else if (language.indexOf('ja') > -1 && cookieValue.indexOf('jp') == -1
      && location.hostname.toLowerCase().indexOf('jp') == -1) { /* Japan Language */
          $("#regionalLanguage").show();        
          $("#languageSelection").attr("onclick","languageSelection('JP');");
          $("#ignoreLanguage").attr("onclick","ignoreLanguageconversion('JP');");
          conversionMessage ="View this website in Japanese";
          $("#txtConversionOption").text(conversionMessage);     
          $("#languageSelection").text("GO");
          $("#ignoreLanguage").text("Dismiss");
      }         
      else
      {
               $("#regionalLanguage").hide();
               conversionMessage='';
      }
    }
    /* code for browser language detection*/
  arrayVideos = $(".videoThumbImage").get();
  videoTitles = $(".videoThumbTitle").get();   
  activeDiv = $("div.col2[capacity]:visible");
  capacitySelected =$(activeDiv).attr("capacity");
  colorSelected  = $(activeDiv).attr("color");
  $("li[capacities ='"+capacitySelected +"']").attr('id','capacity-active');   
  $("a[color='"+colorSelected+"']").parent().attr('id','capacity-color-act');
  changeProductImage(activeDiv,capacitySelected ,colorSelected);
  /* Product Details page Capacity selected click event */  
  $('div.capacity').find('a').click(function(){
    var isavailable= false;
    $('#capacity-active').attr('id','');
    $(this).parent().attr('id','capacity-active');
     capacitySelected =$(this).text();
    $('div.col2[capacity]').hide();
    if($('#capacity-color-act')!='')
    {
     colorSelected  =$('#capacity-color-act').find("a").attr("color");
      $('div.col2[capacity="'+capacitySelected+'"]').each(function(index) {
       if($(this).attr('color')==colorSelected  )
       {
         isavailable = true;
         $(this).show();
         activeDiv = $(this);
       }        
      });     
    }
    if(!isavailable)
    {
        activeDiv = $('div.col2[capacity="'+capacitySelected+'"]:first');        
        $(activeDiv).show();
        colorSelected =$(activeDiv).attr("color");    
        $('#capacity-color-act').attr('id','');
        $("a[color='"+colorSelected+"']").parent().attr('id','capacity-color-act');
    }
     changeProductImage(activeDiv,capacitySelected ,colorSelected )
  });
    /* Product Details page Capacity selected click event */
  /* Product Details page Color selected click event */
  $('ul#capacity-color').find('a').click(function(){
       var isavailable= false;
       colorSelected = $(this).attr("color");
       $('#capacity-color-act').attr('id','');
       $(this).parent().attr('id','capacity-color-act');
       $('div.col2[capacity]').hide();
       if($('#capacity-active')!='')  
       {
        capacitySelected  =$('#capacity-active').attr("capacities");
        $('div.col2[capacity='+capacitySelected+']').each(function(index) {
         if($(this).attr('color')==colorSelected )
         {
           isavailable = true;
           $(this).show();
           activeDiv = $(this);
          }
          });
        }
        if(!isavailable)
        {                                                      
           activeDiv = $('div.col2[color='+colorSelected +']:first');  
           $(activeDiv).show();
           capacitySelected  = $(activeDiv).attr("capacity");
           $('li[id=capacity-active]').attr('id','');
           $("li[capacities ='"+capacitySelected  +"']").attr('id','capacity-active');
        }
     changeProductImage(activeDiv,capacitySelected  ,colorSelected )
     });
 /* Product Details page Color selected click event */
$('#family-table tr:visible').removeClass('odd');
 /* Update the table for alternate row colors */   
  UpdateTable();
  $("input:checked").closest(".cat").css("background-color", "#eeeeee");
  $("input:checkbox").each(function(){
    if($(this).is(':checked'))
    {
      displayCategory($(this).closest(".cat").attr('id'));
    }
    else
    {
      hideCategory($(this).closest(".cat").attr('id'));
    }
  });
   /* Update the table for alternate row colors */
/* Family SD Cards page function */
$(".cat").click(function(){
if($(this).find("input:checked").length > 0)
{
  $(this).find("input:checkbox").attr('checked',false);
       $(this).css("background-color", "");
        hideCategory($(this).attr('id'));
}
else
 {
   $(this).find("input:checkbox").attr('checked',true);
   $(this).css("background-color", "#eeeeee");
    displayCategory($(this).attr('id'));  
  }
});
$(".cat").find("input:checkbox").click(function(){
  if($(this).attr('checked'))
   {
       $(this).attr('checked',false);         
   }
   else    
   {
       $(this).attr('checked',true);
   }
});
  
  /* Resources Filter Start */
  
  //To show div based on selected checkbox
  $(".filters :checkbox").click(function() {
    $("#Resources div.Doc_Types").hide();
  
    //Check for selected checkbox under DocType Filter
    var docTypeArray = [];
    $("#DocTypeList :checkbox:checked").each(function() {
        //Add filter selection to array
          docTypeArray.push('.'+$(this).val());
      });
    var docTypeStr = '';
    //Generate compatible String to be passed to is()
    if (docTypeArray.length > 0)
    {
      docTypeStr = docTypeArray.join();
      docTypeStr = docTypeStr.replace(',', ', ');
    }
    else
    {
      //If no checkbox is selected then give default class
      docTypeStr = '.Doc_Types';
    }
  
    //Check for selected checkbox under Product Filter
    var productArray = [];
    $("#ProductList :checkbox:checked").each(function() {
        //Add filter selection to array
          productArray.push('.'+$(this).val());
      });
    var productStr = '';
    //Generate compatible String to be passed to is()  
    if (productArray.length > 0)
    {
      productStr = productArray.join();
      productStr = productStr.replace(',', ', ');
    }
    else
    {
      //If no checkbox is selected then give default class
      productStr = '.Doc_Types';
    }
  
    //Check for selected checkbox under Industry Filter
    var industryArray = [];
    $("#IndustryList :checkbox:checked").each(function() {
        //Add filter selection to array
          industryArray.push('.'+$(this).val());
      });
    var industryStr = '';
    //Generate compatible String to be passed to is()  
    if (industryArray.length > 0)
    {
      industryStr = industryArray.join();
      industryStr = industryStr.replace(',', ', ');
    }
    else
    {
      //If no checkbox is selected then give default class
      industryStr = '.Doc_Types';
    }
  
    //Check for selected checkbox under Customer Filter
    var customerArray = [];
    $("#CustomerList :checkbox:checked").each(function() {
        //Add filter selection to array
          customerArray.push('.'+$(this).val());
      });
    var customerStr = '';
    //Generate compatible String to be passed to is()  
    if (customerArray.length > 0)
    {
      customerStr = customerArray.join();
      customerStr = customerStr.replace(',', ', ');
    }
    else
    {
      //If no checkbox is selected then give default class
      customerStr = '.Doc_Types';
    }
  
    //Check for selected checkbox under Application Filter
    var applicationArray = [];
    $("#ApplicationList :checkbox:checked").each(function() {
        //Add filter selection to array
          applicationArray.push('.'+$(this).val());
      });
    var applicationStr = '';
    //Generate compatible String to be passed to is()  
    if (applicationArray.length > 0)
    {
      applicationStr = applicationArray.join();
      applicationStr = applicationStr.replace(',', ', ');
    }
    else
    {
      //If no checkbox is selected then give default class
      applicationStr = '.Doc_Types';
    }
    
    $("#Resources div.Section div.Doc_Types").each(function() {
    //To check if current div has all the classes
    //matching the selected filters
    if (($(this).is(docTypeStr)) && ($(this).is(productStr)) && ($(this).is(industryStr)) && ($(this).is(customerStr)) && ($(this).is(applicationStr)))
    {
    //Display the div if all the user selected classes exist
      $(this).show();
    }
      });
    
    
    
    //To display No Matches Found
    if ($("#Resources div.Section").find('div').is(':visible') == false)
    {
      $("#Resources div.Section").find("div.NoMatches").show();
    }
    
    //To display all the divs if nothing is selected  
    if ($('input:checkbox:checked').length < 1)
    {
        //Display all divs if nothing is selected and # value is found
        $("#Resources div.Doc_Types").show();
        $("#Resources div.NoMatches").hide();
      
        //To hide repeating resources when nothing is selected
        var resourceArr = [];
        $('.Section .Doc_Types').each(function() {
          var heading = $(this).text();
          if ($.inArray(heading, resourceArr) === -1) {
              resourceArr.push(heading);
             
          } else {
              $(this).hide()
          }
        });
    }

   //To hide repeating resources when checkbox is toggled
    var resourceArray = [];
    $('.Section .Doc_Types').each(function() {
      var resHeading = $(this).text();
      if ($.inArray(resHeading , resourceArray) === -1 && $(this).is(":visible")) {
          resourceArray.push(resHeading);
         
      } else {
          $(this).hide()
      }
    });   
   });

  /* Resources Filter End */
  
});
/* Family SD Cards page function */

/* Product Details page change sku image based on selction */
function changeProductImage(activeDiv,capacity,color)
{  
   var imgSrc = $(activeDiv).attr("imagepath");
   $("#productimage").attr('src',imgSrc );
   $("#productimage").attr('title',capacity + ' '+color);
   $("#productimage").attr('alt',capacity + ' '+color);
}
/* Product Details page change sku image based on selction */
/* function to update the table odd/even rows */
function UpdateTable() {  
  $('#family-table tr:visible').removeClass('odd').filter(':odd').addClass('odd');
  $('#family-table tr:visible').removeClass('even').filter(':even').addClass('even');
}
/* function to update the table odd/even rows */
/* Display category for SD family page*/
function displayCategory(categoryName)
{
  $("#family-table tr[categoryName='"+categoryName+"']").show();
  UpdateTable();
}
function hideCategory(categoryName)
{
  $("#family-table tr[categoryName='"+categoryName+"']").hide();
  UpdateTable();
}
function showAllSDCards()
{
   $("input:checkbox").attr('checked',true);
  
  $("#family-table tr").show();
  UpdateTable();
  $(".cat").css("background-color", "#eeeeee");
}
/* Display category for SD family page*/
$(function () {
    var items = $('#Vertical-tabs>ul>li').each(function () {
    $(this).click(function () {
        //remove previous class and add it to clicked tab
        items.removeClass('current');
        $(this).addClass('current');

        $('#Vertical-tabs>div.tab-content').hide().eq(items.index($(this))).show();
    });
    });
        function showTab(tab) {
        $("#Vertical-tabs ul li:[tab*=" + tab + "]").click();
    }
});

$(document).ready(function()
{
//For right nav
  $(".Right-nav li.Right-nav-head").click(function()
    {
  $(this).css({background:"#8e8e8e"}).next("li.Right-nav-body").slideDown(300).siblings("li.Right-nav-body").slideUp(300);
    $(this).addClass("").siblings().css({background:""}).next("li.Right-nav-body").slideUp(300);
  });
  
  //For right nav current selection
  loc = location.href.substring(7);   
    loc = loc.substring(loc.indexOf("/"));    
  $("#corp-rightnav a[href='" + loc + "']").parent().addClass('selected');
if (loc == "/about-sandisk/corporate/advantage/business/") {
    $("#corp-rightnav a[href='" + loc + "']").parent().next("li.Right-nav-body").css("display","block");
  }
  $('.sub.selected').parent().parent().css("display","block");
  
  //For Media Kit
  $("#media-kit div.media-kit-head").click(function()
    {
    $(this).css({background:"#8e8e8e"}).next("div.media-kit-body").slideDown(300).siblings("div.media-kit-body").slideUp(300);
    $(this).addClass("").siblings().css({background:""}).next("div.media-kit-body").slideUp(300);
  });
  
  //For Resource Filters Page
  //Check if URL contains # value and select appropriate checkbox on page load
  var hash = window.location.hash;
  hash = hash.replace('#','');
  
  $(".filters :checkbox").each(function() {
  if ($(this).val() == hash)
  {
    $(this).attr('checked', true);
    //Trigger click event
    $(this).trigger('click');
    $(this).attr('checked', true);
    //Display divs that match the selected checkbox
    $(".filters :checkbox:checked").each(function() {
              $("." + $(this).val()).show();
    });
  }
  
});
  //To hide repeating resources when page load
    var resourceArr = [];
    $('.Section .Doc_Types').each(function() {
      var heading = $(this).text();
      if ($.inArray(heading, resourceArr) === -1) {
          resourceArr.push(heading);
         
      } else {
          $(this).hide()
      }
    });
});

function bios(index,title)
{
  var overlay = $('#management-bios');  
   $('#fade').show();
   $(overlay).show();
   $(overlay).css("position","absolute");
   $(overlay).css("top", (($(window).height() - $(overlay).outerHeight()) / 2) + $(window).scrollTop() + "px");
   $(overlay).css("left", (($(window).width() - $(overlay).outerWidth()) / 2) + $(window).scrollLeft() + "px");
   $("#title1").text(title);  
    visibleDivSelector = "#selector_" + index;
    $("div.biosContent:not(" + visibleDivSelector + ")").hide();
    $(visibleDivSelector).show();
}
function closebios()
{
   sliderPosition=0;
  $('#management-bios').hide();
   $('#fade').hide();
}
function swapimg_out(id,ids){
  document.getElementById(id).style.display="none";
  document.getElementById(ids).style.display="block";
}
function swapimg_in(id,ids){
  document.getElementById(ids).style.display="none";
  document.getElementById(id).style.display="block";
}

/* Start SD.js Functions */

/*function to validate Email Address */
/* Created by Bunty Parekh, 7/22/09 */
function ValidateEmailIDForNewsLetterSignUpForm(str,actionURL) {

    var at="@"
    var dot="."
    var lat=str.indexOf(at)
    var lstr=str.length
    var ldot=str.indexOf(dot)
    var errMsg = "Please enter a valid email"

    if (str.indexOf(at)==-1)
    {
       document.forms[0].email.value = errMsg;
       return false
    }
    if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr)
    {
      document.forms[0].email.value = errMsg;
              return false
    }
    if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr)
    {
               document.forms[0].email.value = errMsg;
        return false
    }
     if (str.indexOf(at,(lat+1))!=-1)
    {
     document.forms[0].email.value = errMsg;
        return false
     }
     if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot)
    {
      document.forms[0].email.value = errMsg;
        return false
     }
     if (str.indexOf(dot,(lat+2))==-1)
    {
    document.forms[0].email.value = errMsg;
        return false
     }
     if (str.indexOf(" ")!=-1)
    {
               document.forms[0].email.value = errMsg;
        return false
     }
     document.forms[0].action = actionURL;
            document.forms[0].target = '_blank';
      return true          
  }
function MM_swapImage()
{ //v3.0
var i,j=0,x,a=MM_swapImage.arguments;
document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_swapImgRestore()
{ //v3.0
var i,x,a = document.MM_sr; for(i=0;a && i<a.length && (x=a[i]) && x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d)
{ //v4.01
  var p,i,x;  
  if(!d)
     d=document;
  if((p=n.indexOf("?"))>0 && parent.frames.length)
  {
     d=parent.frames[n.substring(p+1)].document;
     n=n.substring(0,p);
  }
  if(!(x=d[n]) && d.all)
     x=d.all[n];
  for (i=0;!x && i<d.forms.length;i++)
    x=d.forms[i][n];
  for(i=0;!x && d.layers && i<d.layers.length;i++)
    x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById)
    x=d.getElementById(n);
  return x;
}

function MM_preloadImages() { //v3.0
var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
  var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
  if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

/*Start - Function: Toggle Function for SD WORM */
function change(current, number, id, newClass)
{  
    identity=document.getElementById(id);  
    identity.className=newClass;
    current.style.display = 'none';
    if(current.id== 'readmore' + number)
    {
     document.getElementById('readmore'+number).style.display = 'none';
     document.getElementById('collapse'+number).style.display = 'block';  
    }
    else if(current.id== 'collapse' + number)
    {
     document.getElementById('collapse'+number).style.display = 'none';
     document.getElementById('readmore'+number).style.display = 'block';
    }    
}
/*End- Function: Toggle Function for SD WORM */

/*Start - MediaManager Online Wizard */
function MM_showHideLayers() { //v9.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  document.getElementById("myIframe").setAttribute('src','http://www.sandisk.com/go/onlinewizard');
  for (i=0; i<(args.length-2); i+=3)
  with (document) if (getElementById && ((obj=getElementById(args[i]))!=null)) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}
/*End- MediaManager Online Wizard */

/* End SD.js Functions */

function onAfter(curr, next, opts) {
        var index = opts.currSlide;
        //$('#prev')[index == 0 ? 'hide' : 'show']();
        //$('#next')[index == opts.slideCount - 1 ? 'hide' : 'show']();
        $('#prev')[index == 0 ? 'show' : 'show']();
        $('#next')[index == opts.slideCount - 1 ? 'show' : 'show']();
    }

$('#SD_Ent_testimonial').cycle({
    fx:     'scrollHorz',
    prev:   '#prev',
    next:   '#next',
    after:   onAfter,
    nowrap: 1,
    timeout: 0
});

$('#slider-cs').cycle({
    fx:     'scrollHorz',
    prev:   '#prev-cs',
    next:   '#next-cs',
    after:   onAfter,
    nowrap: 1,
    timeout: 0
});

$('#slider-wp').cycle({
    fx:     'scrollHorz',
    prev:   '#prev-wp',
    next:   '#next-wp',
    after:   onAfter,
    nowrap: 1,
    timeout: 0
});



