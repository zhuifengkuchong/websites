//Blimp Animation

var tl = new TimelineLite({onComplete:function() {
    this.restart();
}});

var blimp = document.getElementById("otc-blimp");

tl.to(blimp, 440, {left:"-3%"}, {force3D:true});