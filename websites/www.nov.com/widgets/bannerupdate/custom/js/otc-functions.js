//main interface fader

function mainIntSwitchToRig() {
	var tl = new TimelineLite();
	var mainCont = document.getElementById("otc-main-cont");
	var otcRig = document.getElementById("otc-rig-cont");

	tl.to(mainCont, 1, {opacity:0}, "rig1").to(otcRig, 1, {opacity: 1}, "rig1").to(otcRig, 1, {display: "block"}, "rig1");
}

function mainIntSwitchFromRig() {
	var tl = new TimelineLite();
	var mainCont = document.getElementById("otc-main-cont");
	var otcRig = document.getElementById("otc-rig-cont");

	tl.to(mainCont, 1, {opacity:1}, "rig2").to(otcRig, 1, {opacity: 0}, "rig2").to(otcRig, 1, {display: "none"}, "rig2");
}

//main interface fader

function mainIntSwitchToWT() {
	var tl = new TimelineLite();
	var mainCont = document.getElementById("otc-main-cont");
	var otcWT = document.getElementById("otc-wt-cont");

	tl.to(mainCont, 1, {opacity:0}, "wt1").to(otcWT, 1, {opacity: 1}, "wt1").to(otcWT, 1, {display: "block"}, "wt1");;
}

function mainIntSwitchFromWT() {
	var tl = new TimelineLite();
	var mainCont = document.getElementById("otc-main-cont");
	var otcWT = document.getElementById("otc-wt-cont");

	tl.to(mainCont, 1, {opacity:1}, "wt2").to(otcWT, 1, {opacity: 0}, "wt2").to(otcWT, 1, {display: "none"}, "wt2");
}

//main interface fader

function mainIntSwitchToCAPS() {
	var tl = new TimelineLite();
	var mainCont = document.getElementById("otc-main-cont");
	var otcCAPS = document.getElementById("otc-caps-cont");

	tl.to(mainCont, 1, {opacity:0}, "caps1").to(otcCAPS, 1, {opacity: 1}, "caps1").to(otcCAPS, 1, {display: "block"}, "caps1");;
}

function mainIntSwitchFromCAPS() {
	var tl = new TimelineLite();
	var mainCont = document.getElementById("otc-main-cont");
	var otcCAPS = document.getElementById("otc-caps-cont");

	tl.to(mainCont, 1, {opacity:1}, "caps2").to(otcCAPS, 1, {opacity: 0}, "caps2").to(otcCAPS, 1, {display: "none"}, "caps2");
}

//Mobile Buttons

function mainIntSwitchToRigMobile() {
	var tl = new TimelineLite();
	var mainCont = document.getElementById("otc-main-cont");
	var otcRig = document.getElementById("otc-rig-cont-mob");

	tl.to(mainCont, 1, {opacity:0}, "rig1").to(otcRig, 1, {opacity: 1}, "rig1").to(otcRig, 1, {display: "block"}, "rig1");
}

function mainIntSwitchFromRigMobile() {
	var tl = new TimelineLite();
	var mainCont = document.getElementById("otc-main-cont");
	var otcRig = document.getElementById("otc-rig-cont-mob");

	tl.to(mainCont, 1, {opacity:1}, "rig2").to(otcRig, 1, {opacity: 0}, "rig2").to(otcRig, 0, {display: "none"}, "rig2");
}

function mainIntSwitchToWTMobile() {
	var tl = new TimelineLite();
	var mainCont = document.getElementById("otc-main-cont");
	var otcWT = document.getElementById("otc-wt-cont-mob");

	tl.to(mainCont, 1, {opacity:0}, "wt1").to(otcWT, 1, {opacity: 1}, "wt1").to(otcWT, 1, {display: "block"}, "wt1");;
}

function mainIntSwitchFromWTMobile() {
	var tl = new TimelineLite();
	var mainCont = document.getElementById("otc-main-cont");
	var otcWT = document.getElementById("otc-wt-cont-mob");

	tl.to(mainCont, 1, {opacity:1}, "wt2").to(otcWT, 1, {opacity: 0}, "wt2").to(otcWT, 0, {display: "none"}, "wt2");
}

function mainIntSwitchToCAPSMobile() {
	var tl = new TimelineLite();
	var mainCont = document.getElementById("otc-main-cont");
	var otcCAPS = document.getElementById("otc-caps-cont-mob");

	tl.to(mainCont, 1, {opacity:0}, "caps1").to(otcCAPS, 1, {opacity: 1}, "caps1").to(otcCAPS, 1, {display: "block"}, "caps1");
}

function mainIntSwitchFromCAPSMobile() {
	var tl = new TimelineLite();
	var mainCont = document.getElementById("otc-main-cont");
	var otcCAPS = document.getElementById("otc-caps-cont-mob");

	tl.to(mainCont, 1, {opacity:1}, "caps2").to(otcCAPS, 1, {opacity: 0}, "caps2").to(otcCAPS, 0, {display: "none"}, "caps2");

}