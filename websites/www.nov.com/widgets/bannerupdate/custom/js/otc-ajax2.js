//RIG CALLS

function myAjaxCallRig1(){
	$("#first-cont-rig").load("../OTC/Home/html_content/rig-cont1.html");
}

function myAjaxCallRig1DYN() {
	$("#first-cont-rig").load("../OTC/Home/html_content/rig-cont1DYN.html");
 }

 function myAjaxCallRig2() {
 	$("#first-cont-rig").load("../OTC/Home/html_content/rig-cont2.html");
}

function myAjaxCallRig2DYN() {
	$("#first-cont-rig").load("../OTC/Home/html_content/rig-cont2DYN.html");
}

function myAjaxCallRig3() {
	$("#first-cont-rig").load("../OTC/Home/html_content/rig-cont3.html");
}

function myAjaxCallRig3DYN() {
	$("#first-cont-rig").load("../OTC/Home/html_content/rig-cont3DYN.html");
}

function myAjaxCallRig4() {
	$("#first-cont-rig").load("../OTC/Home/html_content/rig-cont4.html");
}

function myAjaxCallRig4DYN() {
	$("#first-cont-rig").load("../OTC/Home/html_content/rig-cont4DYN.html");
}

function myAjaxCallRig5() {
	$("#first-cont-rig").load("../OTC/Home/html_content/rig-cont5.html");
}

function myAjaxCallRig5DYN() {
	$("#first-cont-rig").load("../OTC/Home/html_content/rig-cont5DYN.html");
}

//WELLBORE CALLS

function myAjaxCallWT1() {
	$("#first-cont-rig").load("../OTC/Home/html_content/WT-cont1.html");
}

function myAjaxCallWT1DYN() {
	$("#first-cont-rig").load("../OTC/Home/html_content/WT-cont1DYN.html");
}

function myAjaxCallWT2() {
	$("#first-cont-rig").load("../OTC/Home/html_content/WT-cont2.html");
}

function myAjaxCallWT2DYN() {
	$("#first-cont-rig").load("../OTC/Home/html_content/WT-cont2DYN.html");
}

function myAjaxCallWT3() {
	$("#first-cont-rig").load("../OTC/Home/html_content/WT-cont3.html");
}

function myAjaxCallWT3DYN() {
	$("#first-cont-rig").load("../OTC/Home/html_content/WT-cont3DYN.html");
}

function myAjaxCallWT4() {
	$("#first-cont-rig").load("../OTC/Home/html_content/WT-cont4.html");
}

function myAjaxCallWT4DYN() {
	$("#first-cont-rig").load("../OTC/Home/html_content/WT-cont4DYN.html");
}

function myAjaxCallWT5() {
	$("#first-cont-rig").load("../OTC/Home/html_content/WT-cont5.html");
}

function myAjaxCallWT5DYN() {
	$("#first-cont-rig").load("../OTC/Home/html_content/WT-cont5DYN.html");
}

//CAPS CALLS

function myAjaxCallCAPS1() {
	$("#first-cont-rig").load("../OTC/Home/html_content/CAPS-cont1.html");
}

function myAjaxCallCAPS1DYN() {
	$("#first-cont-rig").load("../OTC/Home/html_content/CAPS-cont1DYN.html");	
}

function myAjaxCallCAPS2() {
	$("#first-cont-rig").load("../OTC/Home/html_content/CAPS-cont2.html");
}

function myAjaxCallCAPS2DYN() {
	$("#first-cont-rig").load("../OTC/Home/html_content/CAPS-cont2DYN.html");	
}

function myAjaxCallCAPS3() {
	$("#first-cont-rig").load("../OTC/Home/html_content/CAPS-cont3.html");
}

function myAjaxCallCAPS3DYN() {
	$("#first-cont-rig").load("../OTC/Home/html_content/CAPS-cont3DYN.html");	
}

function myAjaxCallCAPS4() {
	$("#first-cont-rig").load("../OTC/Home/html_content/CAPS-cont4.html");
}

function myAjaxCallCAPS4DYN() {
	$("#first-cont-rig").load("../OTC/Home/html_content/CAPS-cont4DYN.html");	
}

function myAjaxCallCAPS5() {
	$("#first-cont-rig").load("../OTC/Home/html_content/CAPS-cont5.html");
}

function myAjaxCallCAPS5DYN() {
	$("#first-cont-rig").load("../OTC/Home/html_content/CAPS-cont5DYN.html");	
}

