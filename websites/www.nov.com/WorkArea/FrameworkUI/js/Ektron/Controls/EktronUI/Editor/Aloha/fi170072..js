    
var Aloha = window.Aloha || ( window.Aloha = {} );

Aloha.settings = {
    jQuery: window.$ektron,
    logLevels: { 'error': true, 'warn': false, 'info': false, 'debug': false, 'deprecated': false },
    errorhandling: false,
    baseUrl: "/WorkArea/FrameworkUI/js/ektron/Controls/EktronUI/Editor/Aloha/lib",
    basePath: "/WorkArea/FrameworkUI/js/ektron/Controls/EktronUI/Editor/Aloha/plugins/",
    ribbon: false,
    locale: 'en',
    sidebar: { disabled: true },
    plugins: {
        load: "dom-to-xhtml/dom-to-xhtml,ektron/general,common/ui,ektron/format,common/contenthandler,ektron/library,common/table,common/list,ektron/hyperlink,common/highlighteditables,common/undo,common/paste,common/horizontalruler,common/abbr,ektron/validator,ektron/template,ektron/embed,ektron/sourceview,ektron/advancedinspector,ektron/draganddrop,common/block,ektron/editInContext,common/characterpicker",
        block: { 
            defaults : {
                '.ektron-imageset-wrapper': {   
                    'aloha-block-type': 'DefaultBlock'
                } 
            }
        }
    },
    contentHandler: {
    insertHtml: [ 'word', 'generic', 'sanitize' ],
      initEditable: [ 'blockelement' ],
      getContents: ['basic'],
      sanitize: 'relaxed',
      handler: {
        generic: {
          transformFormattings: false // this will disable the transformFormattings method in the generic content handler
        },
        sanitize: {
          '.aloha-captioned-image-caption': { elements: [ 'em', 'strong' ] }
        },
      }
	},
    templatePlugin: {
        FolderID: 0
    },
    fontfamilies: [
                'Georgia, serif',
                '"Palatino Linotype", "Book Antiqua", Palatino, serif',
                '"Times New Roman", Times, serif',
                'Arial, Helvetica, sans-serif',
                '"Arial Black", Gadget, sans-serif',
                '"Comic Sans MS", cursive, sans-serif',
                '"Lucida Sans Unicode, Lucida Grande", sans-serif',
                'Tahoma, Geneva, sans-serif',
                '"Trebuchet MS", Helvetica, sans-serif',
                'Verdana, Geneva, sans-serif',
                '"Courier New", Courier, monospace',
                '"Lucida Console", Monaco, monospace'
            ],
    toolbar: {
        tabs: [
                {
					label: "tab.format.label"
				},
				{
					label: "tab.insert.label",
					showOn: { scope: 'Aloha.continuoustext' },
					components: [
						[
							'hyperlink', 'library', 'template', 'embed'
						]
					]
				}
                
                ,
				{
					label: "Review",
					showOn: { scope: 'Aloha.continuoustext' },
					components: [
						[
							'validator', 'translate', 'inspector', 'sourceview', 'inboundwriter'
						]
					]
                }
				
                ,
				{
					label: "File",
					showOn: { scope: 'Aloha.continuoustext' },
					components: [
						[
							'save', 'cancel'
						]
					]
				}
                
			]	
    }
};