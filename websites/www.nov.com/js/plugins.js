// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function() {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

// Init Mega-Menu
$(window).load(function() {
    var lastMenuPosition = 0;
    var countMenuPositions = 0;
    var setMenuPositions = setInterval(function() {
        var currMenuPosition = 0;
        $(".sub-menu>li>div").each(function(i) {
            currMenuPosition = (($(this).parent().index()) * $(this).parent().outerHeight(true));
            $(this).css({
                'position': 'absolute',
                'top': '-' + currMenuPosition + 'null'
            });
        });
        if (lastMenuPosition != currMenuPosition && countMenuPositions > 0) {
            clearInterval(setMenuPositions);
        }
        lastMenuPosition = currMenuPosition;
        countMenuPositions++;
    }, 1000);
});

// Place any jQuery/helper plugins in here.
/* MOBILE COLLAPSE MENU */
(function($) {
    $.fn.collapsable = function(options) {
        // iterate and reformat each matched element
        return this.each(function() {
            // cache this:
            var obj = $(this);
            var tree = obj.next('.navigation');
            obj.click(function() {
                if (obj.is(':visible')) {
                    tree.toggle();
                }
            });
            $(window).resize(function() {
                //if ( $(window).width() <= 570 ){tree.attr('style','');};
                if ($(window).width() <= 320) {
                    tree.attr('style', '');
                };
            });
        });
    };
})(jQuery);

/* MOBILE TOGGLE SEARCH */
$('.search-toggle').click(function(ev) {
    if ($('.mobile-search').hasClass('mobile-search-display')) {
        $('.mobile-search').removeClass('mobile-search-display');
    } else {
        $('.mobile-search').addClass('mobile-search-display');
    }
});

/*$(function() {
    $( "#tabs" ).tabs();
});*/


// Home Page Slider
(function($) {
    // Carousel Items Array - Add and hide
    var carousel_items = [];
    if ($(".carousel-item").length > 0) {
        $(".carousel-item").each(function(i) {
            carousel_items.push($(this));
            $(this).hide();
        });

        // Carousel Nav Items Array - Add
        var carousel_nav_items = [];
        $(".carousel-list>li").each(function(i) {
            carousel_nav_items.push($(this));
        });

        // Set Current Nav Item - Add activ calss
        var curr_carousel_nav_item = carousel_nav_items[0];
        curr_carousel_nav_item.addClass("active");

        // Carousel List Item Click Function
        var curr_carousel_item = carousel_items[0];
        $(".carousel-list>li>a").each(function(i) {
            $(this).click(function() {
                showcarousel(i);
                return false;
            });
        });

        $(".carousel-wrapper").mousemove(function(i) {
            autocarousel();
        });

        // Show Carousel Item
        function showcarousel(c) {
            if (carousel_items[c] != curr_carousel_item && !curr_carousel_item.is(':animated') && !carousel_items[c].is(':animated')) {
                if (curr_carousel_item.index() < carousel_items[c].index()) {
                    curr_carousel_item.animate({
                        'left': -curr_carousel_item.outerWidth()
                    }, 1000, function() {
                        $(this).hide();
                    });
                    carousel_items[c].css({
                        'left': carousel_items[c].outerWidth()
                    });
                    carousel_items[c].animate({
                        'left': 0
                    }, 1000);
                } else {
                    curr_carousel_item.animate({
                        'left': curr_carousel_item.outerWidth()
                    }, 1000, function() {
                        $(this).hide();
                    });
                    carousel_items[c].css({
                        'left': -carousel_items[c].outerWidth()
                    });
                    carousel_items[c].animate({
                        'left': 0
                    }, 1000);
                }

                curr_carousel_item = carousel_items[c];
                carousel_items[c].show();

                // Set Current Nav Item to Previous - Add/Remove Class - Swap
                prev_carousel_nav_item = curr_carousel_nav_item;
                if (carousel_nav_items[c] != prev_carousel_nav_item) {
                    curr_carousel_nav_item = carousel_nav_items[c];
                    curr_carousel_nav_item.addClass("active");
                    prev_carousel_nav_item.removeClass("active");
                }
                autocarousel();
            }
        }
        // Show current Carousel item
        curr_carousel_item.show();

        // Auto play Carousel
        var carousel_wait = 7; // Seconds to wait between change
        var carousel_timer = 0;

        function prevcarousel() {
            if (curr_carousel_item.index() - 1 >= 0) {
                showcarousel(curr_carousel_item.index() - 1);
            } else {
                showcarousel($(".carousel-list>li").length - 1);
            }
        }

        function nextcarousel() {
            if (curr_carousel_item.index() + 1 < $(".carousel-list>li").length) {
                showcarousel(curr_carousel_item.index() + 1);
            } else {
                showcarousel(0);
            }
        }

        function autocarousel() {
            clearInterval(carousel_timer);
            carousel_timer = setTimeout(nextcarousel, (carousel_wait * 1000));
        }
        autocarousel();

        // Detect touch swipe
        if ('ontouchstart' in document.documentElement) {
            /*$(".carousel-wrapper").touchwipe({
         wipeLeft: function() { prevcarousel(); },
         wipeRight: function() { nextcarousel(); },
         wipeUp: function() {  },
         wipeDown: function() {  },
         min_move_x: 20,
         min_move_y: 20,
         preventDefaultEvents: true
      });*/
            var carousel_touch_xstart = 0;
            var carousel_touch_xend = 0;
            var carousel_touch_minimum = 40; // Minimum touch movement
            $('.carousel-wrapper').bind({
                'touchstart': function(e) {
                    carousel_touch_xstart = e.originalEvent.touches[0].pageX;
                }
            });
            $('.carousel-wrapper').bind({
                'touchmove': function(e) {
                    carousel_touch_xend = e.originalEvent.touches[0].pageX;
                }
            });
            $('.carousel-wrapper').bind({
                'touchend': function(e) {
                    if (Math.abs(carousel_touch_xstart - carousel_touch_xend) > carousel_touch_minimum) {
                        if (carousel_touch_xstart > carousel_touch_xend) {
                            if (curr_carousel_item.index() + 1 < $(".carousel-list>li").length) {
                                showcarousel(curr_carousel_item.index() + 1);
                                e.preventDefault();
                            }
                        }
                        if (carousel_touch_xstart < carousel_touch_xend) {
                            if (curr_carousel_item.index() - 1 >= 0) {
                                showcarousel(curr_carousel_item.index() - 1);
                                e.preventDefault();
                            }
                        }
                    }
                    //e.preventDefault();
                }
            });
        }
    }
})(jQuery);

// Product Page Thumbnail Navigation
var product_thumb_scroll_speed = 200; // Speed of scrolling animation in milliseconds
var product_thumb_w = 0;

function setProductThumbnails() {
    $(".product-carousel>.product-carousel-scroll>.product-carousel-thumbs>a").data('loaded', false);
    var setThumbnailSizes = setInterval(function() {
        var thumbnails_set = true;
        var thumbnails_w = 0;
        var thumbnails_current = "";
        //Added for NOVUT-3544 MK
        var count = 0;
        $(".product-carousel>.product-carousel-scroll>.product-carousel-thumbs>a").each(function(i) {
            if (!$(this).data('loaded')) {
                if ($(this).children('div').children('img').width() > 10) {
                    $(this).data('loaded', true);
                }
                thumbnails_set = false;
            }
            if ($(this).data('loaded')) {
                thumbnails_w += $(this).outerWidth(true);
                product_thumb_w = $(this).outerWidth(true);
                if ($(this).hasClass('active-tn')) {
                    thumbnails_current = $(this);
                }
                //Addded for NOVUT-3544 MK
                if (count == 0) {
                    $(this).click();
                    count++;
                }
            }
        });
        if (thumbnails_set) {
            clearInterval(setThumbnailSizes);
        }
        if (thumbnails_w > 0) {
            $('.product-carousel>.product-carousel-scroll>.product-carousel-thumbs').width(thumbnails_w);
        }
        if (thumbnails_current.length > 0) {
            $('.product-carousel>.product-carousel-scroll>.product-carousel-thumbs').css({
                left: -thumbnails_current.position().left
            });
        }
    }, 100);
}

// Product carousel
$(function() {
    //setProductThumbnails();

    var VIDEO_HTML = "<video controls src='[VIDEO_PATH]' preload='auto' poster='[POSTER_PATH]' width='[WIDTH]' height='[HEIGHT]'><p><a href='[VIDEO_PATH]'>Download video</a></p></video>";
    var VIDEO_SWF = "../DefaultError.aspx-aspxerrorpath=-js-assets-videoplayer.swf"/*tpa=http://www.nov.com/js/assets/videoplayer.swf*/;
    var VIDEO_SWF_INSTALLER = "../DefaultError.aspx-aspxerrorpath=-js-assets-expressInstall.swf"/*tpa=http://www.nov.com/js/assets/expressInstall.swf*/;

    $('.product-carousel>a.next-arrow').click(function(ev) {
        if (!$(this).siblings('.product-carousel-scroll').children('.product-carousel-thumbs').is(':animated') && $(this).siblings('.product-carousel-scroll').children('.product-carousel-thumbs').outerWidth() > $(this).siblings('.product-carousel-scroll').width() && $(this).siblings('.product-carousel-scroll').children('.product-carousel-thumbs').position().left - product_thumb_w >= ($(this).siblings('.product-carousel-scroll').width() - $(this).siblings('.product-carousel-scroll').children('.product-carousel-thumbs').outerWidth())) {
            $(this).siblings('.product-carousel-scroll').children('.product-carousel-thumbs').animate({
                left: '-=' + product_thumb_w
            }, product_thumb_scroll_speed, function() {});
        }
        ev.preventDefault();
    });
    $('.product-carousel>a.prev-arrow').click(function(ev) {
        if (!$(this).siblings('.product-carousel-scroll').children('.product-carousel-thumbs').is(':animated') && $(this).siblings('.product-carousel-scroll').children('.product-carousel-thumbs').outerWidth() > $(this).siblings('.product-carousel-scroll').width() && $(this).siblings('.product-carousel-scroll').children('.product-carousel-thumbs').position().left + product_thumb_w <= 0) {
            $(this).siblings('.product-carousel-scroll').children('.product-carousel-thumbs').animate({
                left: '+=' + product_thumb_w
            }, product_thumb_scroll_speed, function() {});
        }
        ev.preventDefault();
    });
    $('.product-carousel>.product-carousel-scroll>.product-carousel-thumbs>a').click(function(ev) {
        if ($(this).attr('link-type') == undefined) {
            // Load image
            var href_low_src = $(this).attr('href');
            var href_high_src = $(this).attr('data-src-high');
            $('.zoom-icon').show();
            $(".product-image").show();
            $("#product-video").hide();
            $(".product-image>div>div").each(function(i) {
                if ($(this).attr('data-media') == undefined) {
                    $(this).attr('data-src', href_low_src);
                } else if ($(this).attr('data-media').indexOf("480px") > 0) {
                    $(this).attr('data-src', href_high_src); 
                } else {
                    $(this).attr('data-src', href_high_src); 
                }
            });
            $('.product-image>div>img').attr('src', href_high_src);
            $('.product-carousel>.product-carousel-scroll>.product-carousel-thumbs>a.active-tn').removeClass('active-tn');
            $(this).addClass('active-tn');
        } else if ($(this).attr('link-type') == 'video') {
            // Load video player
            $('.zoom-icon').hide();
            $(".product-image").hide();
            $("#product-video").show();
            var video_src = $(this).attr('href');
            var poster_src = $(this).attr('poster');
            var video_embed = VIDEO_HTML;
            var video_width = $('.product-image').width();
            var video_height = $('.product-image').height();
            video_embed.replace('[VIDEO_PATH]', video_src);
            video_embed.replace('[POSTER_PATH]', poster_src);
            video_embed.replace('[WIDTH]', video_width);
            video_embed.replace('[HEIGHT]', video_height);

            $("#product-video").html(video_embed);

            swfobject.embedSWF(VIDEO_SWF, "product-video", video_width, video_height, "10.0.0", VIDEO_SWF_INSTALLER, {
                'source': video_src,
                'poster': poster_src,
                'autoplay': 'true',
                'width': '320',
                'height': '175'
            }, {
                scale: "noScale"
            });
            $('.product-carousel>.product-carousel-scroll>.product-carousel-thumbs>a.active-tn').removeClass('active-tn');
            $(this).addClass('active-tn');
        }
        ev.preventDefault();
    });
});



// Media Page Thumbnail Navigation
var media_thumb_scroll_speed = 200; // Speed of scrolling animation in milliseconds
var media_thumb_w = 0;

function setMediaThumbnails() {
    $(".media-carousel>.media-carousel-scroll>.media-carousel-thumbs>a").data('loaded', false);
    var setThumbnailSizes = setInterval(function() {
        var thumbnails_set = true;
        var thumbnails_w = 0;
        var thumbnails_current = "";
        $(".media-carousel>.media-carousel-scroll>.media-carousel-thumbs>a").each(function(i) {
            if (!$(this).data('loaded')) {
                if ($(this).children('div').children('img').width() > 10) {
                    $(this).data('loaded', true);
                }
                thumbnails_set = false;
            }
            if ($(this).data('loaded')) {
                thumbnails_w += $(this).outerWidth(true);
                media_thumb_w = $(this).outerWidth(true);
                if ($(this).hasClass('active-tn')) {
                    thumbnails_current = $(this);
                }
            }
        });
        if (thumbnails_set) {
            clearInterval(setThumbnailSizes);
        }
        if (thumbnails_w > 0) {
            $('.media-carousel>.media-carousel-scroll>.media-carousel-thumbs').width(thumbnails_w);
        }
        if (thumbnails_current.length > 0) {
            $('.media-carousel>.media-carousel-scroll>.media-carousel-thumbs').css({
                left: -thumbnails_current.position().left
            });
        }
    }, 100);
}






// Media carousel
$(function() {

    var MEDIA_VIDEO_HTML = "<video controls src='[VIDEO_PATH]' preload='auto' poster='[POSTER_PATH]' width='[WIDTH]' height='[HEIGHT]'><p><a href='[VIDEO_PATH]'>Download video</a></p></video>";
    var MEDIA_VIDEO_SWF = "../DefaultError.aspx-aspxerrorpath=-js-assets-videoplayer.swf"/*tpa=http://www.nov.com/js/assets/videoplayer.swf*/;
    var MEDIA_VIDEO_SWF_INSTALLER = "../DefaultError.aspx-aspxerrorpath=-js-assets-expressInstall.swf"/*tpa=http://www.nov.com/js/assets/expressInstall.swf*/;

    $('.media-carousel>a.next-arrow').click(function(ev) {
        if (!$(this).siblings('.media-carousel-scroll').children('.media-carousel-thumbs').is(':animated') && $(this).siblings('.media-carousel-scroll').children('.media-carousel-thumbs').outerWidth() > $(this).siblings('.media-carousel-scroll').width() && $(this).siblings('.media-carousel-scroll').children('.media-carousel-thumbs').position().left - media_thumb_w >= ($(this).siblings('.media-carousel-scroll').width() - $(this).siblings('.media-carousel-scroll').children('.media-carousel-thumbs').outerWidth())) {
            $(this).siblings('.media-carousel-scroll').children('.media-carousel-thumbs').animate({
                left: '-=' + media_thumb_w
            }, media_thumb_scroll_speed, function() {});
        }
        ev.preventDefault();
    });
    $('.media-carousel>a.prev-arrow').click(function(ev) {
        if (!$(this).siblings('.media-carousel-scroll').children('.media-carousel-thumbs').is(':animated') && $(this).siblings('.media-carousel-scroll').children('.media-carousel-thumbs').outerWidth() > $(this).siblings('.media-carousel-scroll').width() && $(this).siblings('.media-carousel-scroll').children('.media-carousel-thumbs').position().left + media_thumb_w <= 0) {
            $(this).siblings('.media-carousel-scroll').children('.media-carousel-thumbs').animate({
                left: '+=' + media_thumb_w
            }, media_thumb_scroll_speed, function() {});
        }
        ev.preventDefault();
    });

    //   $('.media-carousel>.media-carousel-scroll>.media-carousel-thumbs>a').click( function (ev) {
    // if($(this).attr('link-type') == undefined) {
    //  // Load image
    //  var href_low_src = $(this).attr('href');
    //  var href_high_src = $(this).attr('data-src-high');
    //  $('.zoom-icon').show();
    //  $(".media-image").show();
    //  $("#media-video").hide();
    //  $(".media-image>div>div").each(function(i){
    //    if($(this).attr('data-media') == undefined) {
    //      $(this).attr('data-src', href_low_src);
    //    } else if($(this).attr('data-media').indexOf("480px") > 0) {
    //      $(this).attr('data-src', href_high_src);
    //    } else {
    //      $(this).attr('data-src', href_high_src);
    //    }
    //  });
    //  $('.media-image>div>img').attr('src', href_high_src);
    //  $('.media-carousel>.media-carousel-scroll>.media-carousel-thumbs>a.active-tn').removeClass('active-tn');
    //  $(this).addClass('active-tn');
    // } else if($(this).attr('link-type') == 'video') {
    //  // Load video player
    //  $('.zoom-icon').hide();
    //  $(".media-image").hide();
    //  $("#media-video").show();
    //  var video_src = $(this).attr('href');
    //  var poster_src = $(this).attr('poster');
    //  var video_embed = MEDIA_VIDEO_HTML;
    //  var video_width = $('.media-image').width();
    //  var video_height = $('.media-image').height();
    //  video_embed.replace('[VIDEO_PATH]', video_src);
    //  video_embed.replace('[POSTER_PATH]', poster_src);
    //  video_embed.replace('[WIDTH]', video_width);
    //  video_embed.replace('[HEIGHT]', video_height);
    //  $("#media-video").html(video_embed);
    //  swfobject.embedSWF(MEDIA_VIDEO_SWF, "media-video", video_width, video_height, "10.0.0", MEDIA_VIDEO_SWF_INSTALLER, { 'source':video_src, 'poster':poster_src, 'autoplay':'true', 'width':'320', 'height':'175' }, { scale: "noScale" });
    //  $('.media-carousel>.media-carousel-scroll>.media-carousel-thumbs>a.active-tn').removeClass('active-tn');
    //  $(this).addClass('active-tn');
    // }
    //       ev.preventDefault();
    //   });

});









// Product Zoom
var overlay = '<div id="overlay"> <div class="bg"></div><div class="zoom-image"><img src="../images/product/jetair-large.jpg"/*tpa=http://www.nov.com/images/product/jetair-large.jpg*/><div class="zoom-close"><a href="" class=""><img src="../images/product/close-icon.png"/*tpa=http://www.nov.com/images/product/close-icon.png*/></a></div></div> </div>';
$(window).load(function() {
    if ($('.zoom-icon').length > 0) {
        $('body').append(overlay);
        $('.zoom-icon').click(function(ev) {
            ev.preventDefault();
            $('#overlay .zoom-image').hide();
            var overlay_src = $(".product-image>div>img").attr("src");
            $(".product-image>div>div").each(function(i) {
                if ($(this).attr('data-media') != undefined && $(this).attr('data-media').indexOf("480px") > 0) {
                    overlay_src = $(this).attr('data-src');
                }
            });
            $('#overlay .zoom-image>img').attr("src", overlay_src);
            $('#overlay').show();
            var overloadLoad = setInterval(function() {
                if ($('#overlay .zoom-image').width() > 1) {
                    $('#overlay .zoom-image').show();
                    $('#overlay .zoom-image').css({
                        'width': '90%',
                        'height': 'auto'
                    });
                    $('#overlay .zoom-image>img').css({
                        'width': '100%',
                        'height': 'auto'
                    });
                    $('#overlay .zoom-image').css({
                        'top': (($(window).height() - $('#overlay .zoom-image').height()) / 2) + 'px',
                        'left': '5%'
                    });
                    if ($('#overlay .zoom-image>img').height() > $(window).height()) {
                        $('#overlay .zoom-image').css({
                            'height': '90%',
                            'width': 'auto'
                        });
                        $('#overlay .zoom-image>img').css({
                            'height': '100%',
                            'width': 'auto'
                        });
                        $('#overlay .zoom-image').css({
                            'left': (($(window).width() - $('#overlay .zoom-image').width()) / 2) + 'px',
                            'top': '5%'
                        });
                    }
                    clearInterval(overloadLoad);
                }
            }, 250);
        });
        $('#overlay .zoom-image>.zoom-close>a, #overlay>.bg').click(function(ev) {
            ev.preventDefault();
            $('#overlay').hide();
        });
    }
});


// News and Calendar Item Navigation
$(function() {
    $('div.showarea').fadeOut(0);
    $('div.showarea:first').fadeIn(0);
    $('a.leftarrow, a.rightarrow').click(function(ev) {
        ev.preventDefault();
        var $visibleItem = $('div.showarea:visible');
        var total = $('div.showarea').length;
        var index = $visibleItem.prevAll('div.showarea').length;
        $(this).attr('href') === '#carouselNext' ? index++ : index--;
        if (index === -1) {
            index = total - 1;
        }
        if (index === total) {
            index = 0
        }
        $visibleItem.hide();
        $('div.showarea:eq(' + index + ')').fadeIn(500);
    });
});
$(function() {
    $('div.eventarea').fadeOut(0);
    $('div.eventarea:first').fadeIn(0);
    $('a.prevarrow, a.nextarrow').click(function(ev) {
        ev.preventDefault();
        var $visibleItem = $('div.eventarea:visible');
        var total = $('div.eventarea').length;
        var index = $visibleItem.prevAll('div.eventarea').length;
        $(this).attr('href') === '#eventNext' ? index++ : index--;
        if (index === -1) {
            index = total - 1;
        }
        if (index === total) {
            index = 0
        }
        $visibleItem.hide();
        $('div.eventarea:eq(' + index + ')').fadeIn(500);
    });
});
/* Toggle q&a */
$(function() {
    (function($) {
        var allPanels = $('.accordion > dd').hide();
        allPanels.slideUp();
        $('.accordion > dt > a').click(function() {
            if ($(this).parent().hasClass('open')) {

                $(this).parent().next().slideUp('slow', function() {
                    $(this).prev().removeClass('open');
                    // alert ($(this).prev().html());
                });
            } else {
                $(this).parent().addClass('open');
                $(this).parent().next().slideDown();
            }
            return false;
        });
    })(jQuery);
});
/*! matchMedia() polyfill - Test a CSS media type/query in JS. Authors & copyright (c) 2012: Scott Jehl, Paul Irish, Nicholas Zakas. Dual MIT/BSD license */
window.matchMedia = window.matchMedia || (function(e, f) {
    var c, a = e.documentElement,
        b = a.firstElementChild || a.firstChild,
        d = e.createElement("body"),
        g = e.createElement("div");
    g.id = "mq-test-1";
    g.style.cssText = "position:absolute;top:-100em";
    d.appendChild(g);
    return function(h) {
        g.innerHTML = '&shy;<style media="' + h + '"> #mq-test-1 { width: 42px; }</style>';
        a.insertBefore(d, b);
        c = g.offsetWidth == 42;
        a.removeChild(d);
        return {
            matches: c,
            media: h
        }
    }
})(document);

/* show advanced search filters */
$(function() {
    var togglePanel = $('.adv-filters').hide();
    togglePanel.slideUp();
    $('a.toggle-show').click(function(ev) {
        ev.preventDefault();
        if ($(this).hasClass('open')) {
            togglePanel.slideUp('slow', function() {
                $('a.toggle-show').removeClass('open');
            });
        } else {
            $(this).addClass('open');
            togglePanel.slideDown('slow');
        }
    });
    return false;
});
