// JavaScript Document
$(document).ready(function(){
    $('.slide-trigger').collapsable();
	
	// testing compatibility mode or not
	if(typeof document.documentMode !== 'undefined') { // checks if this is IE 8 or >
         if(document.documentMode < 8) {  // check if in compat mode 
             // add code here to inform user that they need to turn off compatiblity view
             alert("This site is optimized for non-compatibility mode.  Please click F12 and change Browser Mode to IE8 (not compatibility mode)");  
         }
	} 
    
});

/*! Picturefill - Responsive Images that work today. (and mimic the proposed Picture element with divs). Author: Scott Jehl, Filament Group, 2012 | License: MIT/GPLv2 */

(function( w ){

    // Enable strict mode
    "use strict";

    w.picturefill = function() {
        var ps = w.document.getElementsByTagName( "div" );

        // Loop the pictures
        for( var i = 0, il = ps.length; i < il; i++ ){
            if( ps[ i ].getAttribute( "data-picture" ) !== null ){

                var sources = ps[ i ].getElementsByTagName( "div" ),
                    matches = [];

                // See if which sources match
                for( var j = 0, jl = sources.length; j < jl; j++ ){
                    var media = sources[ j ].getAttribute( "data-media" );
                    // if there's no media specified, OR w.matchMedia is supported 
                    if( !media || ( w.matchMedia && w.matchMedia( media ).matches ) ){
                        matches.push( sources[ j ] );
                    }
                }

                // Find any existing img element in the picture element
                var picImg = ps[ i ].getElementsByTagName( "img" )[ 0 ];

                if( matches.length ){           
                    if( !picImg ){
                        picImg = w.document.createElement( "img" );
                        picImg.alt = ps[ i ].getAttribute( "data-alt" );
                        ps[ i ].appendChild( picImg );
                    }

                    picImg.src =  matches.pop().getAttribute( "data-src" );
                }
                else if( picImg ){
                    ps[ i ].removeChild( picImg );
                }
            }
        }
        
        setProductThumbnails();
        setMediaThumbnails();       
    };

    // Run on resize and domready (w.load as a fallback)
    if( w.addEventListener ){
        w.addEventListener( "resize", w.picturefill, false );
        w.addEventListener( "DOMContentLoaded", function(){
            w.picturefill();
            // Run once only
            w.removeEventListener( "load", w.picturefill, false );
        }, false );
        w.addEventListener( "load", w.picturefill, false );
    }
    else if( w.attachEvent ){
        w.attachEvent( "onload", w.picturefill );
    }

}( this ));

// Auto Clear forms 
// ----------------------------------------------------------------------------------------------------
function init(){
    var inp = $('input.searchform');
    for(var i = 0; i < inp.length; i++) {
        if((inp[i].type == 'text') || (inp[i].type == 'search')) {
            inp[i].setAttribute('rel',inp[i].defaultValue)
            inp[i].onfocus = function() {
                if(this.value == this.getAttribute('rel')) {
                    this.value = '';
                } else {
                    return false;
                }
            }
            inp[i].onblur = function() {
                if(this.value == '') {
                    this.value = this.getAttribute('rel');
                } else {
                    return false;
                }
            }
            inp[i].ondblclick = function() {
                this.value = this.getAttribute('rel')
            }
        }
    }
}
if(document.childNodes) {
    window.onload = init
}

// Util Search Display

var $searchInit = $('a.searchInit');
var $searchInputCont = $('#header .utility-block');
var $searchInput = $('#header .utility-block input');
$searchInit.on('click', function(e) {
    e.preventDefault();
    $(this).hide();
    $searchInput.toggleClass('show');
    $searchInput.focus();
    $searchInputCont.width(340);
});
$searchInput.on('blur', function(e) {
    $(this).toggleClass('show');
    $searchInputCont.width(278);
    $searchInit.show();
});



if (Function('/*@cc_on return document.documentMode===10@*/')()){
    document.documentElement.className+=' ie10';
}


var doc = document.documentElement;
doc.setAttribute('data-useragent', navigator.userAgent);

