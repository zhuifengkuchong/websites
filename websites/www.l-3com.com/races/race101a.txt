<script id = "race101a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race101={};
	myVars.races.race101.varName="Lu_Id_a_45__onblur";
	myVars.races.race101.varType="@varType@";
	myVars.races.race101.repairType = "@RepairType";
	myVars.races.race101.event1={};
	myVars.races.race101.event2={};
	myVars.races.race101.event1.id = "Lu_DOM";
	myVars.races.race101.event1.type = "onDOMContentLoaded";
	myVars.races.race101.event1.loc = "Lu_DOM_LOC";
	myVars.races.race101.event1.isRead = "False";
	myVars.races.race101.event1.eventType = "@event1EventType@";
	myVars.races.race101.event2.id = "Lu_Id_a_45";
	myVars.races.race101.event2.type = "onblur";
	myVars.races.race101.event2.loc = "Lu_Id_a_45_LOC";
	myVars.races.race101.event2.isRead = "True";
	myVars.races.race101.event2.eventType = "@event2EventType@";
	myVars.races.race101.event1.executed= false;// true to disable, false to enable
	myVars.races.race101.event2.executed= false;// true to disable, false to enable
</script>

