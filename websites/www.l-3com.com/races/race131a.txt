<script id = "race131a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race131={};
	myVars.races.race131.varName="Function[30496].op";
	myVars.races.race131.varType="@varType@";
	myVars.races.race131.repairType = "@RepairType";
	myVars.races.race131.event1={};
	myVars.races.race131.event2={};
	myVars.races.race131.event1.id = "Lu_Id_a_16";
	myVars.races.race131.event1.type = "onfocus";
	myVars.races.race131.event1.loc = "Lu_Id_a_16_LOC";
	myVars.races.race131.event1.isRead = "False";
	myVars.races.race131.event1.eventType = "@event1EventType@";
	myVars.races.race131.event2.id = "Lu_Id_a_17";
	myVars.races.race131.event2.type = "onfocus";
	myVars.races.race131.event2.loc = "Lu_Id_a_17_LOC";
	myVars.races.race131.event2.isRead = "False";
	myVars.races.race131.event2.eventType = "@event2EventType@";
	myVars.races.race131.event1.executed= false;// true to disable, false to enable
	myVars.races.race131.event2.executed= false;// true to disable, false to enable
</script>

