<script id = "race231a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race231={};
	myVars.races.race231.varName="Function[30496].op";
	myVars.races.race231.varType="@varType@";
	myVars.races.race231.repairType = "@RepairType";
	myVars.races.race231.event1={};
	myVars.races.race231.event2={};
	myVars.races.race231.event1.id = "Lu_Id_a_17";
	myVars.races.race231.event1.type = "onblur";
	myVars.races.race231.event1.loc = "Lu_Id_a_17_LOC";
	myVars.races.race231.event1.isRead = "False";
	myVars.races.race231.event1.eventType = "@event1EventType@";
	myVars.races.race231.event2.id = "Lu_Id_a_18";
	myVars.races.race231.event2.type = "onblur";
	myVars.races.race231.event2.loc = "Lu_Id_a_18_LOC";
	myVars.races.race231.event2.isRead = "False";
	myVars.races.race231.event2.eventType = "@event2EventType@";
	myVars.races.race231.event1.executed= false;// true to disable, false to enable
	myVars.races.race231.event2.executed= false;// true to disable, false to enable
</script>

