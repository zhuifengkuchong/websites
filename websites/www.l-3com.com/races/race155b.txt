<script id = "race155b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race155={};
	myVars.races.race155.varName="Function[30496].op";
	myVars.races.race155.varType="@varType@";
	myVars.races.race155.repairType = "@RepairType";
	myVars.races.race155.event1={};
	myVars.races.race155.event2={};
	myVars.races.race155.event1.id = "Lu_Id_a_35";
	myVars.races.race155.event1.type = "onfocus";
	myVars.races.race155.event1.loc = "Lu_Id_a_35_LOC";
	myVars.races.race155.event1.isRead = "False";
	myVars.races.race155.event1.eventType = "@event1EventType@";
	myVars.races.race155.event2.id = "Lu_Id_a_34";
	myVars.races.race155.event2.type = "onfocus";
	myVars.races.race155.event2.loc = "Lu_Id_a_34_LOC";
	myVars.races.race155.event2.isRead = "False";
	myVars.races.race155.event2.eventType = "@event2EventType@";
	myVars.races.race155.event1.executed= false;// true to disable, false to enable
	myVars.races.race155.event2.executed= false;// true to disable, false to enable
</script>

