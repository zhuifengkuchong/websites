<script id = "race232a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race232={};
	myVars.races.race232.varName="Function[30496].op";
	myVars.races.race232.varType="@varType@";
	myVars.races.race232.repairType = "@RepairType";
	myVars.races.race232.event1={};
	myVars.races.race232.event2={};
	myVars.races.race232.event1.id = "Lu_Id_a_19";
	myVars.races.race232.event1.type = "onblur";
	myVars.races.race232.event1.loc = "Lu_Id_a_19_LOC";
	myVars.races.race232.event1.isRead = "False";
	myVars.races.race232.event1.eventType = "@event1EventType@";
	myVars.races.race232.event2.id = "Lu_Id_a_20";
	myVars.races.race232.event2.type = "onblur";
	myVars.races.race232.event2.loc = "Lu_Id_a_20_LOC";
	myVars.races.race232.event2.isRead = "False";
	myVars.races.race232.event2.eventType = "@event2EventType@";
	myVars.races.race232.event1.executed= false;// true to disable, false to enable
	myVars.races.race232.event2.executed= false;// true to disable, false to enable
</script>

