<script id = "race120b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race120={};
	myVars.races.race120.varName="Lu_Id_a_64__onblur";
	myVars.races.race120.varType="@varType@";
	myVars.races.race120.repairType = "@RepairType";
	myVars.races.race120.event1={};
	myVars.races.race120.event2={};
	myVars.races.race120.event1.id = "Lu_Id_a_64";
	myVars.races.race120.event1.type = "onblur";
	myVars.races.race120.event1.loc = "Lu_Id_a_64_LOC";
	myVars.races.race120.event1.isRead = "True";
	myVars.races.race120.event1.eventType = "@event1EventType@";
	myVars.races.race120.event2.id = "Lu_DOM";
	myVars.races.race120.event2.type = "onDOMContentLoaded";
	myVars.races.race120.event2.loc = "Lu_DOM_LOC";
	myVars.races.race120.event2.isRead = "False";
	myVars.races.race120.event2.eventType = "@event2EventType@";
	myVars.races.race120.event1.executed= false;// true to disable, false to enable
	myVars.races.race120.event2.executed= false;// true to disable, false to enable
</script>

