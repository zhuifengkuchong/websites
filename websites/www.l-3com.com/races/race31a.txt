<script id = "race31a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race31={};
	myVars.races.race31.varName="Lu_Id_a_37__onfocus";
	myVars.races.race31.varType="@varType@";
	myVars.races.race31.repairType = "@RepairType";
	myVars.races.race31.event1={};
	myVars.races.race31.event2={};
	myVars.races.race31.event1.id = "Lu_DOM";
	myVars.races.race31.event1.type = "onDOMContentLoaded";
	myVars.races.race31.event1.loc = "Lu_DOM_LOC";
	myVars.races.race31.event1.isRead = "False";
	myVars.races.race31.event1.eventType = "@event1EventType@";
	myVars.races.race31.event2.id = "Lu_Id_a_37";
	myVars.races.race31.event2.type = "onfocus";
	myVars.races.race31.event2.loc = "Lu_Id_a_37_LOC";
	myVars.races.race31.event2.isRead = "True";
	myVars.races.race31.event2.eventType = "@event2EventType@";
	myVars.races.race31.event1.executed= false;// true to disable, false to enable
	myVars.races.race31.event2.executed= false;// true to disable, false to enable
</script>

