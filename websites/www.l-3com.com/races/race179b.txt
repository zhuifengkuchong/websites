<script id = "race179b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race179={};
	myVars.races.race179.varName="Function[30496].op";
	myVars.races.race179.varType="@varType@";
	myVars.races.race179.repairType = "@RepairType";
	myVars.races.race179.event1={};
	myVars.races.race179.event2={};
	myVars.races.race179.event1.id = "Lu_Id_a_50";
	myVars.races.race179.event1.type = "onfocus";
	myVars.races.race179.event1.loc = "Lu_Id_a_50_LOC";
	myVars.races.race179.event1.isRead = "False";
	myVars.races.race179.event1.eventType = "@event1EventType@";
	myVars.races.race179.event2.id = "Lu_Id_a_49";
	myVars.races.race179.event2.type = "onfocus";
	myVars.races.race179.event2.loc = "Lu_Id_a_49_LOC";
	myVars.races.race179.event2.isRead = "False";
	myVars.races.race179.event2.eventType = "@event2EventType@";
	myVars.races.race179.event1.executed= false;// true to disable, false to enable
	myVars.races.race179.event2.executed= false;// true to disable, false to enable
</script>

