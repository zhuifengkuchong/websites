<script id = "race181b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race181={};
	myVars.races.race181.varName="Function[30496].op";
	myVars.races.race181.varType="@varType@";
	myVars.races.race181.repairType = "@RepairType";
	myVars.races.race181.event1={};
	myVars.races.race181.event2={};
	myVars.races.race181.event1.id = "Lu_Id_a_51";
	myVars.races.race181.event1.type = "onfocus";
	myVars.races.race181.event1.loc = "Lu_Id_a_51_LOC";
	myVars.races.race181.event1.isRead = "False";
	myVars.races.race181.event1.eventType = "@event1EventType@";
	myVars.races.race181.event2.id = "Lu_Id_a_50";
	myVars.races.race181.event2.type = "onfocus";
	myVars.races.race181.event2.loc = "Lu_Id_a_50_LOC";
	myVars.races.race181.event2.isRead = "False";
	myVars.races.race181.event2.eventType = "@event2EventType@";
	myVars.races.race181.event1.executed= false;// true to disable, false to enable
	myVars.races.race181.event2.executed= false;// true to disable, false to enable
</script>

