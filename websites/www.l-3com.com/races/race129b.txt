<script id = "race129b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race129={};
	myVars.races.race129.varName="Array[29964]$LEN";
	myVars.races.race129.varType="@varType@";
	myVars.races.race129.repairType = "@RepairType";
	myVars.races.race129.event1={};
	myVars.races.race129.event2={};
	myVars.races.race129.event1.id = "Lu_Id_a_12";
	myVars.races.race129.event1.type = "onfocus";
	myVars.races.race129.event1.loc = "Lu_Id_a_12_LOC";
	myVars.races.race129.event1.isRead = "False";
	myVars.races.race129.event1.eventType = "@event1EventType@";
	myVars.races.race129.event2.id = "Lu_Id_a_7";
	myVars.races.race129.event2.type = "onfocus";
	myVars.races.race129.event2.loc = "Lu_Id_a_7_LOC";
	myVars.races.race129.event2.isRead = "False";
	myVars.races.race129.event2.eventType = "@event2EventType@";
	myVars.races.race129.event1.executed= false;// true to disable, false to enable
	myVars.races.race129.event2.executed= false;// true to disable, false to enable
</script>

