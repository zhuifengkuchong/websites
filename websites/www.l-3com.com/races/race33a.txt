<script id = "race33a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race33={};
	myVars.races.race33.varName="Lu_Id_a_39__onfocus";
	myVars.races.race33.varType="@varType@";
	myVars.races.race33.repairType = "@RepairType";
	myVars.races.race33.event1={};
	myVars.races.race33.event2={};
	myVars.races.race33.event1.id = "Lu_DOM";
	myVars.races.race33.event1.type = "onDOMContentLoaded";
	myVars.races.race33.event1.loc = "Lu_DOM_LOC";
	myVars.races.race33.event1.isRead = "False";
	myVars.races.race33.event1.eventType = "@event1EventType@";
	myVars.races.race33.event2.id = "Lu_Id_a_39";
	myVars.races.race33.event2.type = "onfocus";
	myVars.races.race33.event2.loc = "Lu_Id_a_39_LOC";
	myVars.races.race33.event2.isRead = "True";
	myVars.races.race33.event2.eventType = "@event2EventType@";
	myVars.races.race33.event1.executed= false;// true to disable, false to enable
	myVars.races.race33.event2.executed= false;// true to disable, false to enable
</script>

