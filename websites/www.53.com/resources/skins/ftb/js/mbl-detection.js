// JavaScript Document

function GetURLParameter(sParam) {
  var sPageURL = window.location.search.substring(1);
  var sURLVariables = sPageURL.split('&');

  for (var i = 0; i < sURLVariables.length; i++) {
    var sParameterName = sURLVariables[i].split('=');
    if (sParameterName[0] == sParam) {
      return sParameterName[1];
    };
  };
};

var mblFlag = GetURLParameter('mblFwd'); 
//alert(mblFlag);

$(function(){
  if (Modernizr.mq('only screen and (max-width:700px)')) { 
    if (mblFlag !='1') {
      window.location = "https://www.53.com/personal-banking/account-management/mobile-banking/download.html";
    }
  }
});

