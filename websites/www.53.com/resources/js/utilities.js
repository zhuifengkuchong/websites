//Set the Default Status bar value
var statusbar_default = "Fifth Third Bank";

self.status = statusbar_default;

//--------------------
//Set Macromedia Functions

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.getElementsByName(n)) x=(d.getElementsByName(n))[0]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}


function MM_showHideLayers() { //v6.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}


function MM_callJS(jsStr) { //v2.0
  return eval(jsStr)
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

//-------------------
//Check browser version
//-------------------

function checkbrowser() {
	
	var appUserAgent = navigator.userAgent;
	
	//alert (appUserAgent);
	
	if ((appUserAgent.indexOf('6.1') >= 0) && (appUserAgent.indexOf('Netscape') >= 0)) {
		return true;	
	} else {
		return false;	
	}

}

//-------------------
// Netscape 6 check
//-------------------

function net6check()
{
	var userAgent = navigator.userAgent;
	var appVersion = navigator.appVersion;
	// document.write("User Agent is " + userAgent + " and AppVersion is " + appVersion);
	if ((userAgent.indexOf('Netscape6') >= 0)) {
	return true;
	}
	else {
	return false;
	}
}



//--------------------
//Set Navigation Array

var menus = new Array ();
menus[0] = new Array ("menuone",false);
menus[1] = new Array ("menutwo",false);
menus[2] = new Array ("menuthree",false);
menus[3] = new Array ("menufour",false);


function hideMenu(x) {
	showForm = true;
	menus[x][1] = true;
	layerName = menus[x][0];
	command = "hideMenuUtil('"+layerName+"',"+x+")";
	setTimeout(command, 100);
		
}

function hoverMenu(x,status_text) {
	layerName = menus[x][1] = false;
	showForm = false;
	
	//set the text displayed in the status bar
	if (status_text != null) {
	
		self.status = status_text;
	
	} else {
	
		self.status = statusbar_default;
	
	}
}

function showMenu(x) {
	
	MM_showHideLayers(menus[x][0],'','show');

	//Show the transparent layer that will cover the forms
	var layer = document.getElementById("spacerLayer");
	layer.style.visibility = 'visible';

	//Disable select boxes in the content area of the page

	if (document.getElementById('selectBox1') != null) {
		var selectMenu = document.getElementById('selectBox1');
		selectMenu.style.visibility = 'hidden';
	}
	
	if (document.getElementById('selectBox2') != null) {
		var selectMenu = document.getElementById('selectBox2');
		selectMenu.style.visibility = 'hidden';
	}
	
	if (document.getElementById('selectBox3') != null) {
		var selectMenu = document.getElementById('selectBox3');
		selectMenu.style.visibility = 'hidden';
	}
	
	if (document.getElementById('selectBox4') != null) {
		var selectMenu = document.getElementById('selectBox4');
		selectMenu.style.visibility = 'hidden';
	}
	
	if (document.getElementById('selectBox5') != null) {
		var selectMenu = document.getElementById('selectBox5');
		selectMenu.style.visibility = 'hidden';
	}
	
	if (document.getElementById('selectBox6') != null) {
		var selectMenu = document.getElementById('selectBox6');
		selectMenu.style.visibility = 'hidden';
	}

	showForm = false;
	MM_showHideLayers('menuHide','','hide');
	//hideNSForm();
				
}

function hideMenuUtil(x,y) {
	if (menus[y][1]) {
		MM_showHideLayers(x,'','hide');
		if(y == 0 || y == 1 || y == 2 || y == 3) {
			if (showForm) {
				MM_showHideLayers('menuHide','','show');
				//showNSForm();
				
				//Hide the transparent layer that will cover the forms
				var layer = document.getElementById("spacerLayer");
				layer.style.visibility = 'hidden';
					
				
				//Enable select boxes in the content area of the page
				//For more than six select boxes, add as needed following the same code format, and reference as appropriate in the HTML file
				if (document.getElementById('selectBox1') != null) {
					var selectMenu = document.getElementById('selectBox1');
					selectMenu.style.visibility = 'visible';
				}
				
				if (document.getElementById('selectBox2') != null) {
					var selectMenu = document.getElementById('selectBox2');
					selectMenu.style.visibility = 'visible';
				}
				
				if (document.getElementById('selectBox3') != null) {
					var selectMenu = document.getElementById('selectBox3');
					selectMenu.style.visibility = 'visible';
				}
				
				if (document.getElementById('selectBox4') != null) {
					var selectMenu = document.getElementById('selectBox4');
					selectMenu.style.visibility = 'visible';
				}
				
				if (document.getElementById('selectBox5') != null) {
					var selectMenu = document.getElementById('selectBox5');
					selectMenu.style.visibility = 'visible';
				}
				
				if (document.getElementById('selectBox6') != null) {
					var selectMenu = document.getElementById('selectBox6');
					selectMenu.style.visibility = 'visible';
				}
				if (document.getElementById('selectBox7') != null) {
					var selectMenu = document.getElementById('selectBox7');
					selectMenu.style.visibility = 'visible';
				}
				if (document.getElementById('selectBox8') != null) {
					var selectMenu = document.getElementById('selectBox8');
					selectMenu.style.visibility = 'visible';
				}
				if (document.getElementById('selectBox9') != null) {
					var selectMenu = document.getElementById('selectBox9');
					selectMenu.style.visibility = 'visible';
				}
				if (document.getElementById('selectBox10') != null) {
					var selectMenu = document.getElementById('selectBox10');
					selectMenu.style.visibility = 'visible';
				}
				if (document.getElementById('selectBox11') != null) {
					var selectMenu = document.getElementById('selectBox11');
					selectMenu.style.visibility = 'visible';
				}
				if (document.getElementById('selectBox12') != null) {
					var selectMenu = document.getElementById('selectBox12');
					selectMenu.style.visibility = 'visible';
				}
				if (document.getElementById('selectBox13') != null) {
					var selectMenu = document.getElementById('selectBox13');
					selectMenu.style.visibility = 'visible';
				}
				if (document.getElementById('selectBox14') != null) {
					var selectMenu = document.getElementById('selectBox14');
					selectMenu.style.visibility = 'visible';
				}
				if (document.getElementById('selectBox15') != null) {
					var selectMenu = document.getElementById('selectBox15');
					selectMenu.style.visibility = 'visible';
				}																				
				
			}
		}
		
		self.status = statusbar_default;
	}
			
}

function detectNavPosition(testString) {
	var bd = new BrowserDetector(navigator.userAgent);
	searchExpression = new RegExp("[/]" + testString + "[/]");
	myUrl = new String(document.location);
	if (myUrl.search(searchExpression) > -1)
	{
		return true;
	}
	else return false;
}

function load_URL(webpage) {

	//alert(webpage);
	location.href = webpage;
	
}


//The following functions handle adjusting the location of the menus
//if the online banking utility nav is activated

function cascadedstyle(el, cssproperty, csspropertyNS)
{
	if (el.currentStyle) //if IE5+
	return el.currentStyle[cssproperty]
	else if (window.getComputedStyle) {
		//if NS6+
		var elstyle=window.getComputedStyle(el, "")
		return elstyle.getPropertyValue(csspropertyNS)
	}
}


//--------------------------------------------------------------------

//following function sets the form parameter with cookie value
function post_cookie(form)
{       
	var cookie_value = getCookie('visitor_id');
	form.fp_cookie.value = cookie_value;      
}


//following function returns the value of cookie requested.
function getCookie(c_name)
{

if (document.cookie.length>0)
  {
  c_start=document.cookie.indexOf(c_name + "=")
  if (c_start!=-1)
    { 
    	
    c_start=c_start + c_name.length+1 
    c_end=document.cookie.indexOf(";",c_start)
    if (c_end==-1) c_end=document.cookie.length
    var cookie_value = unescape(document.cookie.substring(c_start,c_end));
       
    return cookie_value;
    } 
  }
return ""
}

