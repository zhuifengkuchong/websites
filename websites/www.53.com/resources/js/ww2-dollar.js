/* dollar.js for Fifth Third Bank
 *
 * The purpose of this JS file is to serve all www.53.com pages
 * with common jQuery functionality which is recyclable/reusable.
 *
 * Last modified on: 1/29/2012
 * Last modified by: Tracy Turner
 * Last Added New Calculator Modal Implementation
 *
 * Comment Anchors
 * =fn
 *    =doTimeout
 *    =hoverIntent
 *    =megaMenu
 *    =rng
 *    =videoPlayer
 * =onload
 *    =ibUpDown
 *    =mmObjects
 * =doc-ready
 *    =accordion
 *    =accordion-faq
 *    =bal
 *    =bbTabs
 *    =dialog
 *    =dialog-toolTip
 *    =dialog-basic
 *    =disclosures
 *    =Tabs
 *    =buttons
 *    =iblogin
 *    =navigation
 *    =printing
 *    =productFilter
 *    =rotators
 *    =sectionMenu
 *    =webTrends
 *      =checking
 *      =ratescroll
 */

// =fn
// =buttons
  $('.ui-button-default, .ui-button-primary, .ui-button-secondary, .ui-button-special, input:submit').button();  // initialize jQueryUI buttons

// =doTimeout
// jQuery doTimeout: Like setTimeout, but better! - v1.0 - 3/3/2010 * http://benalman.com/projects/jquery-dotimeout-plugin/ * Copyright (c) 2010 "Cowboy" Ben Alman * Dual licensed under the MIT and GPL licenses. * http://benalman.com/about/license/
(function($){var a={},c="doTimeout",d=Array.prototype.slice;$[c]=function(){return b.apply(window,[0].concat(d.call(arguments)))};$.fn[c]=function(){var f=d.call(arguments),e=b.apply(this,[c+f[0]].concat(f));return typeof f[0]==="number"||typeof f[1]==="number"?this:e};function b(l){var m=this,h,k={},g=l?$.fn:$,n=arguments,i=4,f=n[1],j=n[2],p=n[3];if(typeof f!=="string"){i--;f=l=0;j=n[1];p=n[2]}if(l){h=m.eq(0);h.data(l,k=h.data(l)||{})}else{if(f){k=a[f]||(a[f]={})}}k.id&&clearTimeout(k.id);delete k.id;function e(){if(l){h.removeData(l)}else{if(f){delete a[f]}}}function o(){k.id=setTimeout(function(){k.fn()},j)}if(p){k.fn=function(q){if(typeof p==="string"){p=g[p]}p.apply(m,d.call(n,i))===true&&!q?o():e()};o()}else{if(k.fn){j===undefined?e():k.fn(j===false);return true}else{e()}}}})(jQuery);


// =hoverIntent
// hoverIntent r5 // 2007.03.27 // jQuery 1.1.2+ * <http://cherne.net/brian/resources/jquery.hoverIntent.html> * @author Brian Cherne <brian@cherne.net>
(function($){$.fn.hoverIntent=function(f,g){var cfg={sensitivity:7,interval:100,timeout:0};cfg=$.extend(cfg,g?{over:f,out:g}:f);var cX,cY,pX,pY;var track=function(ev){cX=ev.pageX;cY=ev.pageY;};var compare=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);if((Math.abs(pX-cX)+Math.abs(pY-cY))<cfg.sensitivity){$(ob).unbind("mousemove",track);ob.hoverIntent_s=1;return cfg.over.apply(ob,[ev]);}else{pX=cX;pY=cY;ob.hoverIntent_t=setTimeout(function(){compare(ev,ob);},cfg.interval);}};var delay=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);ob.hoverIntent_s=0;return cfg.out.apply(ob,[ev]);};var handleHover=function(e){var p=(e.type=="mouseover"?e.fromElement:e.toElement)||e.relatedTarget;while(p&&p!=this){try{p=p.parentNode;}catch(e){p=this;}}if(p==this){return false;}var ev=jQuery.extend({},e);var ob=this;if(ob.hoverIntent_t){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);}if(e.type=="mouseover"){pX=ev.pageX;pY=ev.pageY;$(ob).bind("mousemove",track);if(ob.hoverIntent_s!=1){ob.hoverIntent_t=setTimeout(function(){compare(ev,ob);},cfg.interval);}}else{$(ob).unbind("mousemove",track);if(ob.hoverIntent_s==1){ob.hoverIntent_t=setTimeout(function(){delay(ev,ob);},cfg.timeout);}}};return this.mouseover(handleHover).mouseout(handleHover);};})(jQuery);

// =megaMenu
function mmDropDown(subnavID, ddID) {
  subnavID.hoverIntent(function() {  // delayed hover event
      ddID.slideDown(250, 'easeInCubic');  // show dropdown
    ddID.css('zIndex',99);
      subnavID.addClass('active');  // add active to subnav element
    },  // end delayed hover event
    function() {  // hover out event
      ddID.slideUp(250, 'easeInCubic');  // hide dropdown
    ddID.css('zIndex',50);
      subnavID.removeClass('active');
    }  // end hover out event
  );
}

function dropdown(subnavID, ddID, ddSections, smSections) {
  this.subnavID = subnavID;
  this.ddID = ddID;
  this.ddSections = ddSections;
  this.smSections = smSections;
  this.init = function() {
    mmDropDown(this.subnavID, this.ddID);
  };
}

// =mmObjects
var bbSolutions = new dropdown($('#bb-s'), $('#bb-s-dropdown'), $('.bb-solutions-dropdown-section'), '.bb-solutions-section' );
var bbProducts = new dropdown($('#bb-ps'), $('#bb-ps-dropdown'), $('.bb-ps-dropdown-section'), '.bb-ps-section');
var bbIndustry = new dropdown($('#bb-is'), $('#bb-is-dropdown'), $('.bb-is-dropdown-section'), '.bb-is-section');
var commProducts = new dropdown($('#cm-ps'), $('#cm-ps-dropdown'), $('.ps-dropdown-section'), '.ps-section');
var commIndustry = new dropdown($('#cm-is'), $('#cm-is-dropdown'), $('.is-dropdown-section'), '.is-section');
var arrDropdown = [bbSolutions, bbProducts, bbIndustry, commProducts, commIndustry];

for (var i=0, j=arrDropdown.length; i<j; i++) {
  if (arrDropdown[i].subnavID.length) {
    arrDropdown[i].init();
  }
}



// =rng
function rng(from, to) {
  return Math.floor((Math.random()*(to-from+1))+from);
}

// =videoPlayer
function videoPlayer(obj) {
    var flashvars = {
        APPLICATION_PATH: obj.app,
        CONFIG_PATH: obj.config,
        DEBUG: 'false'
    };
    var params = {
        bgcolor: '#FFFFFF'
    };
    swfobject.embedSWF('../media/swf/Shell.swf'/*tpa=https://www.53.com/resources/media/swf/Shell.swf*/, 'video-object', obj.width, obj.height, 'https://www.53.com/resources/js/9.0.28', 'Unknown_83_filename'/*tpa=https://www.53.com/resources/js/expressInstall.swf*/, flashvars, params);
}
  function trackInteractions(id, opt_id) {
    dcsMultiTrack('DCS.dcsuri','a','https://www.53.com/resources/js/WT.ti','b','WT.si_n','c','WT.si_p',id);
    elqFCS(id);
  }
  function trackInteractionsPopup(id, url, name, specs, opt_id) {
    trackInteractions(id);
  }
  function trackInteractionsClickThru(id, url, target, opt_id) {
    trackInteractions(id);
    location.href=url;
  }

// =videoDialog
function videoDialog(el) {
  var $link = $(el);
  var videoParams = {
    app: ($link.attr('data-app') !== undefined) ? $link.attr('data-app') : '../media/swf/Application.swf'/*tpa=https://www.53.com/resources/media/swf/Application.swf*/,
    config: $link.attr('data-config'),
    height: ($link.attr('data-height') !== undefined) ? $link.attr('data-height') : '455',
    width: ($link.attr('data-width') !== undefined) ? $link.attr('data-width') : '700'
  };

  videoPlayer(videoParams);


  $('#video-wrapper').dialog({
    close: function() { $('#video-wrapper').remove('#video-object').html('<div id="video-object"></div>'); },
    modal: true,
    title: $link.attr('title'),
    width: parseFloat(videoParams.width) + 25
  });
  return false;
}

// =onload
// =ibUpDown
(function() {
  var $ibLogin = $('#login, #login-dropdown, #login-full').find('#login-main');
  if ($ibLogin.length) {
    var upDown = function(data) {
      if (data.match(/down/g)) {
        //$ibLogin.html('<p class="half-top">Fifth Third Bank\'s Internet Banking is temporarily unavailable. We are working to restore the site and apologize for the inconvenience. Please try again later.</p>');
        $ibLogin.html('<p class="half-top">Currently, Fifth Third Bank is improving your Internet Banking site with scheduled maintenance to better serve you.  We apologize for any inconvenience. Please try again later.</p>');
      }
    };

    $.ajax({
      url: 'https://www.53.com/status/ib.txt',
      cache: false,
      success: function(data) { upDown(data); },
      error: function() { upDown('up'); }
    });
  }
})();

// document.ready
$(function() {
  var urlPathname = $(location).attr('pathname');

  if (urlPathname == '/product/rates/cd') {
  //if (urlPathname == 'https://www.53.com/vig/development/ftb-rates-demo.html') {
      $('h2:first').after('<div class="featuredCdRates"></div>');
      $('.featuredCdRates').load('https://www.53.com/apps/dda/static/featured-cd-rates.html');
      $("div.featuredCdRates:not(:first)").hide();
    //}
  }

// SET THE CALCULATOR MODAL LINKS TO WORK
    var mla = 'ul.modal-list li a';
  $(mla).attr('role','button');
  $(mla).attr('data-toggle','modal');
  $(mla).addClass('modal-trigger');
    
// JQUERY DIALOG FOR CALCULATORS
  $( '#modal-dialog' ).dialog({
    autoOpen: false,
    width: 950,
    modal: true
  });
     
// MODAL EVENT FOR CALCULATORS   
  $( '.modal-trigger' ).click(function() {
    var calcId = $(this).attr('data-cid');
    var jsonParam = json[calcId];
    $('.mm-item-content').hide();
    $('#modal-dialog h2').html(jsonParam.heading);
    $('#modal-dialog iframe').attr('src',jsonParam.url);        
    $('#modal-dialog').dialog('open');
      return false;
  });
  
  
  $('a[href=#post-form]').bind('click', function() {
    $(this).closest('form').submit();
    return false;
  });

  // =accordion
  $('.accordion').accordion({ 
    header: 'h3', 
  autoHeight: false,
    navigation: true  
  });

  $('#accordion').accordion({ // for BB homepage
    header: 'h2',
    collapsible: true,
    active: false,
    fillSpace: true,
  navigation: true
  });
  
  // =accordion-faq
  $('.accordion-faq').accordion({
//  header: 'h3',
    active: false,
    animated: false,
    collapsible: true,
  autoHeight: false,
  navigation: true
  
//    clearStyle: true
  });
  
  $('#mkg-accordion').accordion({ header: 'h3', autoHeight: false });

  // =bal
  $('#postal-code').keyup(function() {
    this.value = this.value.replace(/[^0-9\.]/g,'');
  });

  $('#bal-button').click(function() {
    $('#bal-form').submit();
    return false;
  });

  // =bbTabs
  var $bbTabs = $('#bb-tabs').find('a');
  $bbTabs.hover(function() {
    $this = $(this);
    attrID = '#' + $this.attr('data-copy');
    $('.content-clip').hide();
    $(attrID).show();
    return false;
  });

  // =Tabs
  $( "#tabs" ).tabs();

  // =dialog
  $('#dialog').dialog({
    autoOpen: false,
    width: 700,
    modal: true,
    buttons: {
      "close video": function() {
        $(this).dialog("close");
      }
    }
  });
  
  // =dialog-toolTip
  $('#dialog-tooltip').dialog({
    autoOpen: false,
    modal: false,
    buttons: {
      "close video": function() {
        $(this).dialog("close");
      }
    }
  });
  
  // =dialog-basic
  $('#dialog-basic').dialog({
    autoOpen: true,
    modal: true,
    width: 560,
    buttons: {
      "close": function() {
        $(this).dialog("close");
      }
    }
  });

  // =disclosures
  $('#expand-icon').click(function() {
    $('#toggle-copy').toggle('blind', 500);
    $('#icon-toggle').toggleClass('ui-icon-circle-minus');
    return false;
  });
  
    $('#expand-pe-icon').click(function() {
    $('#toggle-pe-copy').toggle('blind');
    $('#icon-pe-toggle').toggleClass('ui-53-icon-arrow-1-n');
    return false;
  });
  
  $('.toggle-display').click(function() {
    $(this).next('.toggle-copy').toggle('blind', 500);
    $(this).children('.icon-toggle').toggleClass('ui-icon-circle-minus');
    return false;
  });
    
    // used click() for jQ below 1.7 -- needs to be changed to on() when we migrate
    $( '[rel="external"]' ).click( function( e ) {
        e.preventDefault();

        // get data attributes or set defaults
        var $link = $( this ),
            $autoOpen = false,
            $width = $link.attr( 'data-width' ) ? $link.attr( 'data-width' ) + 'px' : 405; // width needs the 'px' where height doesnt
        var $title = $link.attr( 'data-title' ) ? $link.attr( 'data-title' ) : 'https://www.53.com/resources/js/Leaving 53.com';
        var $height = $link.attr( 'data-height' ) ? $link.attr( 'data-height' ) : 'auto';
        var $modal = $link.attr( 'data-modal' ) === 'true' || $link.attr( 'data-modal') === '' || $link.attr( 'data-modal' ) === undefined ? true : false;
        var $draggable = $link.attr( 'data-draggable' ) === 'true' ? true : false;
        var $href = $link.attr( 'href' );
        var $newWindow = $link.attr( 'data-new-window' ) === 'false' ? false : true;
        var $dialog;
        var $message = $link.attr( 'data-message' ) ? $link.attr( 'data-message' ) : "<p>You have clicked on an external link and are leaving the Fifth Third Bancorp website.  Linked web pages are not under the control of Fifth Third, its affiliates or subsidiaries.  Fifth Third provides such links as a convenience and is not responsible for the content or security of any linked web page.</p>";

        if ( $href === undefined || $href == "" ) {
            $href = $link.attr( 'data-href' ) ? $link.attr( 'data-href' ) : $href;
        }

        // check to see if the dialog already exists
        if ( !document.getElementById( 'external' ) ) {
            $('<div />', {
                'id'    : 'external'
            }).appendTo( 'body' );
        }
        $dialog = $( '#external' );

        $dialog.html( "<div>" + $message + "</div>" );
        $dialog.dialog({
            buttons: {
                'Ok': function() {
                    $newWindow ? window.open( $href ) : window.location.href = $href;
                    $(this).dialog("close");
                },
                'Cancel': function() {
                    $(this).dialog("close");
                }
            },
            autoOpen: $autoOpen,
            draggable: $draggable,
            modal: !!$modal,
            title: $title,
            width: $width,
            height: $height,
            position: 'center'
        });
        $dialog.dialog( 'open' );
    });

  var $videoLink = $('[href=#video]');
  if ($videoLink.length) {
    $videoLink.click(function() {
      videoDialog(this);
      return false;
    });
  }

  // =iblogin
  var fnLabel = function($objLabel) {
    var $input = $objLabel.next('input');

    $input.bind('click keydown', function() {
      $objLabel.addClass('hide');
    });
    $input.blur(function() {
      if ($input.val().length === 0) {
        $objLabel.removeClass('hide');
      }
    });
    $objLabel.click(function() {
      $objLabel.addClass('hide');
      $input.focus();
    });
  };

  var $uidLabel = $('#uid-label');
  if ($uidLabel.length) {
    fnLabel($uidLabel);
    $('#uid-input').focus();
  }

  var $pwLabel = $('#pw-label');
  if ($pwLabel.length) {
    fnLabel($pwLabel);
    $pwLabel.next('#pw').keydown(function(e){
      if (e.keyCode == 13) {
        $(this).closest('form').trigger('submit');
      return false;
      }
    });
  }

  $('#ib-login-button').click(function() {
    $(this).closest('form').submit();
    return false;
  });

  $('#login-dropdown').hoverIntent(
    function() {
      $(this).addClass('active');
      $('#login-dropdown-content').slideDown();
    },
    function() {}
  );

  $('#login-dropdown-close').click(function() {
    $('#login-dropdown').removeClass('active');
    $('#login-dropdown-content').slideUp();
  });

  // =navigation
  $('#navigation').find('li:first').addClass('first');

  // =printing
  $('[href="#print"]').click(function() {
    window.print();
    return false;
  });

  // =productFilter
  var $prodGroup = $('.product-group');
  var $prodCheckboxes = $('#sort-business-need :checkbox');
  var $products = $('.product');

  $('#number-products').text($products.length);

  $prodCheckboxes.click(function() {
    if ($prodCheckboxes.filter(':checked').length === 0) {
      $prodGroup.show().children().fadeIn();
    } else {
      $products.filter(':visible').stop(true,true).fadeOut();
      $prodGroup.hide();

      $.each($prodCheckboxes, function(i) {
        if ($prodCheckboxes[i].checked) {
          var $dataNeeds = $('div[data-need*=' + $prodCheckboxes[i].name + ']');

          $dataNeeds.parent().show();
          $dataNeeds.stop(true,true).slideDown();
        }
      });

    }
  });

  $('#alphabet').find('a').click(function() {
    var $this = $(this);
    $prodCheckboxes.attr('checked', false);
    $prodGroup.filter(':visible').fadeOut(200);
    $($this.attr('href')).delay(200).slideDown().children().show();
    return false;
  });

  $('#product-show-all').click(function() {
    $products.show();
    $prodCheckboxes.attr('checked', false);
    $prodGroup.slideDown();
    return false;
  });

  // =rotators
    // personal
    var $rotatorContent = $('.rotator-content');
    var $rotatorLinks = $('#ad-rotator ul').find('a');
    var $rotatorArrow = $('#rotator-arrow');
    var rotatorCount = 1;

    function animateRotator(el) {
      var $currActive = {
            link: $rotatorLinks.filter('.active'),
            banner: $rotatorContent.filter(':visible')
        };
      var $this = (isNaN(el)) ? $(el) : $rotatorLinks.eq(el);

      if (!$this.hasClass('active')) {
        if ($this.hasClass('personal-rotator-1')) {
          $rotatorArrow.animate({top: '23px'}, 400);
        } else if ($this.hasClass('personal-rotator-2')) {
          $rotatorArrow.animate({top: '101px'}, 400);
        } else {
          $rotatorArrow.animate({top: '177px'}, 400);
        }

        $currActive.banner.fadeOut();
        $('div').find('.' + $this.attr('class')).fadeIn();

        $currActive.link.removeClass('active');
        $this.addClass('active');
      }
    }

    $.doTimeout('rotator', 5000, function(){
      rotatorCount = (rotatorCount > 2) ? 0 : rotatorCount;
      animateRotator(rotatorCount++);

      return true;
    });

    if ($rotatorLinks.length > 0) {
      $rotatorLinks.click(function() {
        animateRotator(this);
        $.doTimeout('rotator');  // stop doTimeout
        return false;
      });
    }

    // commercial
    var $featuredLists = $('#featured-list').find('a');
    if ($featuredLists.length > 0) {
      $featuredLists.mouseenter(function() {
        var $this = $(this);
        var msgID = '#' + $this.attr('data-featured');

        $featuredLists.removeClass('start');
        if (!$this.hasClass('active')) {
          $('.featured-message').stop(true, true).fadeOut();
          $featuredLists.removeClass('active');

          $(msgID).delay(400).stop(true, true).fadeIn(200);
          $this.addClass('active');
        }
      });
    }

    // commercial industry-special
    var $industry = $('#featured').filter('.industry-specializations');

    if($industry.length) {
      var rotators = $industry.children();
      var num = rng(1,rotators.length) - 1;
      var rotator = rotators[num];

      $industry.addClass(rotator.id);
      $(rotator).removeClass('hide');
    }


  // =sectionMenu
  var $sectionMenu = $('#section-menu').find('a');
  if ($sectionMenu.length > 0) {
    $sectionMenu.click(function() {
      $this = $(this);
      attrID = '#' + $this.attr('data-menu');

      $sectionMenu.removeClass('active');
      $sectionMenu.find('.ui-icon-white').removeClass('ui-icon-white').addClass('ui-icon-gray');
      $this.addClass('active',200).find('.ui-icon-gray').removeClass('ui-icon-gray').addClass('ui-icon-white');

      $('.section-menu-content').fadeOut();
      $(attrID).fadeIn();

      return false;
    });
  }

  // =selectBox
  var $selectBox = $('.select-box');
  if ($selectBox.length > 0) {
    $.each($selectBox, function(i) {
      var $this = $($selectBox[i]);
      var selectList = '<ul class="ui-select-list">';
      var $selectOptions = $this.children();
      $.each($selectOptions, function(i) {
        if (i === 0) {
          selectList += ('<li class="ui-state-active">' + $($selectOptions[i]).text() + '<a href="#link"><span class="ui-icon-white ui-icon-triangle-1-s"></span></a></li>');
        }
        selectList += ('<li>' + $($selectOptions[i]).text() + '</li>');
      });
      $this.hide().after(selectList + '</ul>');
      $this.next().children().eq(1).addClass('ui-border');
    });

    $('.ui-state-active').click(function() {
      var $this = $(this);
      $this.closest('.ui-select-list').find('li').not('.ui-state-active').toggle();
      return false;
    });

    var $selectOptions = $('.ui-select-list').children();
    $selectOptions.not('.ui-state-active').hover(function() {
      $(this).toggleClass('ui-state-hover');
    }).click(function() {
      var $this = $(this);
      $this.prevAll('.ui-state-active').html($this.text() + '<a href="#link"><span class="ui-icon-white ui-icon-triangle-1-s"></span></a></li>');
      $this.parent().prev('.select-box').children().eq($selectOptions.index(this) - 1).attr('selected','selected');
    });


    $(document).click(function() {
      $selectOptions.not('.ui-state-active').hide();
    });

    var $selectGo = $('[href="#select-go"]');
    $selectGo.click(function() {
      var $optionSelected = $(this).parent().find(':selected');
      if ($optionSelected.attr('rel') === 'newWin') {
        window.open($optionSelected.val());
      } else if ($optionSelected.attr('value') !== '') {
        window.location = $optionSelected.val();
      }
      return false;
    });
  } // end if
  
// =checking
$( "#compare-dialog" ).dialog({
  resizable: false,
  height:200,
    width: 350,
    autoOpen: false,
    modal: true,
  buttons: {
    'Compare': function() {
      $( this ).dialog( 'close' );
    },
    Cancel: function() {
      $( this ).dialog( 'close' );
    }
  }
});

var $compareChecking = $('#compare-checking-container'),
     $compareBoxes = $('#compare-boxes-sprite');
 
// Start functionality
$compareChecking.delegate('input','click', function() {
    var $selectedToCompare = $compareChecking.find(':checked');
    
  $compareBoxes.attr('class', 'pe-icon-checkbox-' + $selectedToCompare.length);
                if ($selectedToCompare.length > 4) {
      $compareBoxes.attr('class', 'pe-icon-checkbox-4');
      $( "#compare-dialog" ).dialog( "open" );
      return false;
    }
});
 
$('#compare-boxes a, #compare-dialog.ui-dialog button:eq(0)').live('click',function() {
  var $selectedToCompare = $compareChecking.find(':checked'),
       compareURL = 'http://' + window.location.host + '/site/personal-banking/checking/checking-compare.html?prodID='
 
  $selectedToCompare.each(function() {
    compareURL += (this.id + ',');
  });
 
  window.location.href = compareURL;
  return false;
});
// =end checking
  
});
  // =webTrends
  /*var $wt = $('[data-wt]');
  if ($wt.length > 0) {
    $wt.click(function() {
      dcsMultiTrack($wt.attr('data-wt'));
    });
  } */

  $('#ignite-tool').one('click', function() {
  var $this = $(this),
      iframeSrc = this.href,
      iframe = document.createElement('iframe');

  iframe.src = iframeSrc;
  iframe.frameBorder = 0;
  iframe.height = $this.attr('data-height') || 530;
  iframe.scrolling = 'no';
  iframe.style.display = 'none';
  iframe.width = $this.attr('data-width') || 630;

  $('body').append('<div id="ignite-dialog"><p>You have clicked on an external link and are leaving the Fifth Third Bancorp website.  Linked web pages are not under the control of Fifth Third, its affiliates or subsidiaries.  Fifth Third provides such links as a convenience and is not responsible for the content or security of any linked web page.</p></div>');

$('#ignite-dialog')
    .dialog({
      buttons: {
        'Ok': function() {
          $(this)
            .append(iframe)
            .find('*:not(iframe)')
              .hide()
            .end()
            .find('iframe')
              .show()
            .end()
            .dialog('option', {
              height: 'auto',
              width: parseInt($this.attr('data-width'))+20 || 705,
              position: 'center'
            })
            .nextAll('.ui-dialog-buttonpane')
              .hide();
        },

        'Cancel': function() {
          $(this).dialog('close');
        }
      },
      modal: true,
      title: $this.attr('data-title') || "Business Banking Guide"
    });

    $(this).click(function() {
      $('#ignite-dialog').dialog('open');
      return false;
    });
  return false;
});

 $('#ignite-tool-1').one('click', function() {
  var $this = $(this),
      iframeSrc = this.href,
      iframe = document.createElement('iframe');

  iframe.src = iframeSrc;
  iframe.frameBorder = 0;
  iframe.height = $this.attr('data-height') || 530;
  iframe.scrolling = 'no';
  iframe.style.display = 'none';
  iframe.width = $this.attr('data-width') || 630;

  $('body').append('<div id="ignite-dialog-1"><p>You have clicked on an external link and are leaving the Fifth Third Bancorp website.  Linked web pages are not under the control of Fifth Third, its affiliates or subsidiaries.  Fifth Third provides such links as a convenience and is not responsible for the content or security of any linked web page.</p></div>');

$('#ignite-dialog-1')
    .dialog({
      buttons: {
        'Ok': function() {
          $(this)
            .append(iframe)
            .find('*:not(iframe)')
              .hide()
            .end()
            .find('iframe')
              .show()
            .end()
            .dialog('option', {
              height: 'auto',
              width: parseInt($this.attr('data-width'))+20 || 705,
              position: 'center'
            })
            .nextAll('.ui-dialog-buttonpane')
              .hide();
        },

        'Cancel': function() {
          $(this).dialog('close');
        }
      },
      modal: true,
      title: $this.attr('data-title') || "Business Banking Guide"
    });

    $(this).click(function() {
      $('#ignite-dialog-1').dialog('open');
      return false;
    });
  return false;
});

$('#ignite-tool-2').one('click', function() {
  var $this = $(this),
      iframeSrc = this.href,
      iframe = document.createElement('iframe');

  iframe.src = iframeSrc;
  iframe.frameBorder = 0;
  iframe.height = $this.attr('data-height') || 530;
  iframe.scrolling = 'no';
  iframe.style.display = 'none';
  iframe.width = $this.attr('data-width') || 630;

  $('body').append('<div id="ignite-dialog-2"><p>You have clicked on an external link and are leaving the Fifth Third Bancorp website.  Linked web pages are not under the control of Fifth Third, its affiliates or subsidiaries.  Fifth Third provides such links as a convenience and is not responsible for the content or security of any linked web page.</p></div>');

$('#ignite-dialog-2')
    .dialog({
      buttons: {
        'Ok': function() {
          $(this)
            .append(iframe)
            .find('*:not(iframe)')
              .hide()
            .end()
            .find('iframe')
              .show()
            .end()
            .dialog('option', {
              height: 'auto',
              width: parseInt($this.attr('data-width'))+20 || 705,
              position: 'center'
            })
            .nextAll('.ui-dialog-buttonpane')
              .hide();
        },

        'Cancel': function() {
          $(this).dialog('close');
        }
      },
      modal: true,
      title: $this.attr('data-title') || "Business Banking Guide"
    });

    $(this).click(function() {
      $('#ignite-dialog-2').dialog('open');
      return false;
    });
  return false;
});

// ratescroll
$('.ratescroll').click(function(e) {
  e.preventDefault();
  if (!$('.toggle-display').find('span').hasClass('ui-icon-circle-minus')) { // only trigger opening if it's currently closed
    $('.toggle-display').trigger('click');
  }
  var trgt = this.href.split("#")[1],
      target_offset = $("#"+trgt).offset(),
      target_top = target_offset.top;
  $('html, body').animate({scrollTop:target_top}, 500);
});

// Styling
  // Zebrastriping for table rows
  $(".zebrastriping tbody tr:even").addClass("odd");
  
// Copyright Date
$(function() {
  cDate = new Date();
  var cYear = cDate.getFullYear().toString();
  var cMonth = (cDate.getMonth()+1).toString(); // getMonth() is zero-based
  var cDay  = cDate.getDate().toString();
  $('#copyrightDate').append(cYear);
  $('#dateComplete').append(cMonth + '/' + cDay + '/' + cYear);// use this for mm/dd/yyyy format
});


