/* global.js for Fifth Third Bank
 *
 * The purpose of this JS file is to serve all www.53.com pages
 * with common functionality which is recyclable/reusable.
 *
 * Last modified on: 11/01/2010
 * Last modified by: Cory Dorning
 *
 * Comment Anchors
 *    =bgCache
 *    =addListener
 *    =GoogleAnalytics
 *    =GoogleSearch
 *    =inputValue
 *		=initialize
 */

// =bgCache
try {
  $.browser.msie && $.browser.version < 7 && document.execCommand( 'BackgroundImageCache', false, true );
} catch(e) { }

// =addListener
function addListener(element, type, fn) {
  if (window.addEventListener)	{ // Standard
    element.addEventListener(type, fn, false);
    return true;
  } else if (window.attachEvent) { // IE
    element.attachEvent('on' + type, fn);
    return true;
  } else { return false; }
}

// =GoogleAnalytics
//var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
//document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));

//try {
//	var pageTracker = _gat._getTracker("UA-5426510-2");
//	if (window.location.href.indexOf("https://www.53.com/resources/js/www.53.com") > -1) {  pageTracker._trackPageview(); }
//} catch(err) {}


// GOOGLE
// Analytics
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-5426510-2']);
  _gaq.push(['_setDomainName', 'https://www.53.com/resources/js/www.53.com']);
  _gaq.push(['_trackPageview']);
 
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www/') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

// =GoogleSearch
function googleSearchSubmit(e) {
	var searchString = document.getElementById('site-search-input').value;
	//var searchLocation = 'http://services.53.com/search/results.html?cx=015862182068035309598%3Ayl2z8hjqalm&cof=FORID%3A9;NB:1&ie=UTF-8&q=' + searchString.replace(/\s+/,'+');
	var searchLocation = 'https://www.53.com/search/results.html?cx=015862182068035309598%3Ayl2z8hjqalm&cof=FORID%3A9;NB:1&ie=UTF-8&q=' + searchString.replace(/\s+/,'+');

	if (e.preventDefault) {
		e.preventDefault();
	} else if (window.event) {
		window.event.returnValue = false;
	}

	window.location.href =  searchLocation;
}
// end application fix

// =inputValue
function inputValue(id, val) {
	var inputID = document.getElementById(id);  // get input ID

	if (inputID) {
		inputID.onfocus = function inputFocus() {  // focus event
			var el = this;  // get the input element
			if (el.value === val) {  // check the value
				el.className = '';  // remove class 'gray'
				el.value = '';  // set value to be blank
			}
		};

		inputID.onblur = function inputBlur() {  // blur event
			var el = this;  // get the input element
			if (el.value === '') {  // check the value
				el.className = 'gray';  // add class 'gray'
				el.value = val;  // set the value passed
				//if (id === 'pw') {
				//	el.type = 'text';
				//}
			}
		};
	}
}

// =initialize
(function init() {
	// application fix
  var googleSearch = document.googleSearch,
      googleButton = document.getElementById('g-search-button') || document.getElementById('g-search-results-button');

  if (googleSearch) {
    addListener(googleSearch, 'submit', googleSearchSubmit);
  }

  if (googleButton) {
    addListener(googleButton, 'click', googleSearchSubmit);
  }
	// end application fix

	inputValue('site-search-input', 'Search');
	inputValue('uid', 'User ID');
	inputValue('postal-code', 'ZIP Code');
})();

// Motionpoint
// Toggle
var MP = {
<!-- mp_trans_disable_start --> 
  Version: 'https://www.53.com/resources/js/1.0.23',
  Domains: {'es':'https://www.53.com/resources/js/espanol.53.com'},	
  SrcLang: 'en',
<!-- mp_trans_disable_end -->
  UrlLang: 'mp_js_current_lang',
  SrcUrl: decodeURIComponent('mp_js_orgin_url'),
<!-- mp_trans_disable_start --> 	
  init: function(){
    if (MP.UrlLang.indexOf('p_js_')==1) {
      MP.SrcUrl=window.top.document.location.href;
      MP.UrlLang=MP.SrcLang;
  }
},
getCookie: function(name){
  var start=document.cookie.indexOf(name+'=');
  if(start < 0) return null;
  start=start+name.length+1;
  var end=document.cookie.indexOf(';', start);
  if(end < 0) end=document.cookie.length;
  while (document.cookie.charAt(start)==' '){ start++; }
  return decodeURIComponent(document.cookie.substring(start,end));
},
setCookie: function(name,value,path,domain){
  var cookie=name+'='+encodeURIComponent(value);
  if(path)cookie+='; path='+path;
  if(domain)cookie+='; domain='+domain;
  var now=new Date();
  now.setTime(now.getTime()+1000*60*60*24*365);
  cookie+='; expires='+now.toUTCString();
  document.cookie=cookie;
},
switchLanguage: function(lang){
  if(lang!=MP.SrcLang){
    var script=document.createElement('SCRIPT');
    script.src=location.protocol+'//'+MP.Domains[lang]+'/'+MP.SrcLang+lang+'/?1023749632;'+encodeURIComponent(MP.SrcUrl);
	document.body.appendChild(script);
  } else if(lang==MP.SrcLang && MP.UrlLang!=MP.SrcLang){
    var script=document.createElement('SCRIPT');
    script.src=location.protocol+'//'+MP.Domains[MP.UrlLang]+'/'+MP.SrcLang+MP.UrlLang+'/?1023749634;'+encodeURIComponent(location.href);
	document.body.appendChild(script);
  }
  return false;
},
switchToLang: function(url) {
  window.top.location.href=url; 
}
<!-- mp_trans_disable_end -->   
};
