//add element methods
Element.addMethods({
	
	dropnav: function(element,para){
		element = $(element);
		Df.Namespace.create('df', element).dropnav = new Df.Dropnav( element ).set(para);
		return element;
	},
	
	tabset: function(element,para){
		element = $(element);
		Df.Namespace.create('df', element).tabset = new Df.Tabset( element ).set(para);
		return element;
	},
	
	accordion: function(element,para){
		element = $(element);
		Df.Namespace.create('df', element).accordion = new Df.Accordion( element ).set(para);
		return element;
	},
	
	cardset: function(element,para){
		element = $(element);
		Df.Namespace.create('df', element).cardset = new Df.Cardset( element ).set(para);
		return element;
	}
});

/*
 ref:		Df.NavCollection
 extends:	Df.UiCollection
 returns:	Df.NavCollection
 type:		Class
*/
Df.NavCollection = Class.create(Df.UiCollection, {
	initialize: function($super, element){
		$super(element)
		
		this.setPars({
			showClassName: 'activeList',
			activeControllerClassName: 'active',
			eventType: 'hover',
			onDisplay: false,
			onHide: false,
			scrollbars: false,
			forceClose: true
		});
	},
	
	/**
	 *pars hash is passed to each {@link Df.Dropnav.Item}
	 *@type Df.Dropnav
	 *
	 *@param pars  pars.animate: -o [{@link Df.Animate} pars hash | false] -d false  -r 
	 *@param pars  pars.pause: -o [ms] -d 200 -r only used on pars.eventType of 'hover'
	 *@param pars  pars.iframe: -o [iframe] -d true -r used to hide windowed elements in ie6 and earlier
	 *@param pars  pars.activeClassName: -o ['className' | false] -d 'active'  -r
	 *@param pars  pars.eventType: -o ['hover'|'click'] -d  'hover' -r
	 *@param pars  pars.onDisplay: -o [funtion|false] -d false -r first argument is class instance
	 *@param pars  pars.onHide: -o [funtion|false] -d false -r first argument is class instance
	 *@param pars  pars.childElement: -o ['HTML element'] -d  'UL' -r
	 *
	 *
	 *@returns dispatched events: ['set']
	 *
	 */
	
	/** @private */
	buildItems: function(){
		var elem = this.element.immediateDescendants();
		
		for(var i=0; i<elem.length; i++){
			this.items.push( new Df.NavItem( $(elem[i]) ).set( this.pars ) );
		}
		
	}
});

/*
 ref:		Df.NavItem
 extends:	Df.TogglePane
 returns:	Df.NavItem
 type:		Class
*/
Df.NavItem = Class.create(Df.TogglePane, {
	initialize: function($super, element){
		$super(element)
		
		this.scrollbars;
		
		this.iframe = false
		
		return this;
	},
	
	set: function($super, pars){
		$super(pars)
		
		if (this.pars.scrollbars) {
			this.scrollbars = new Df.Scrollbar(this.list);
			if(this.pars.scrollbars.constructor == Boolean){
				this.pars.scrollbars = {}
			}	
			
			this.scrollbars.getEvents().observe('set1','set', function(ins){
				if(ins.getElements().y.holder){
					ins.getElements().y.holder.style.display = 'none'
				}
			});
			
			this.scrollbars.set(pars.scrollbars);
		}
			
		return this;
	},
	
	showClickObserver: function(){
		if(this.pars.forceClose){
			this.pars.collection.showOnlyItem(this)
		}else{
			this.show()
		}
	},
	
	_finishShow: function($super){
		$super()
		
		if (this.scrollbars) {
			var xholder = this.scrollbars.getElements().y.holder;
			if (xholder) {
				xholder.style.display = "block";
			}
		}
		
		return this;
	},
	
	_finishHide: function($super){
		$super()

		if (this.scrollbars) {
			var xholder = this.scrollbars.getElements().y.holder;
			if(xholder){
				xholder.style.display = "none";
			}
		}
		
		return this;
	}
});

/*
 ref:		Df.Accordion
 extends:	Df.NavCollection
 returns:	Df.Accordion
 type:		Class
*/
Df.Accordion = Class.create(Df.NavCollection, {
	initialize: function($super, element){
		$super(element)
		
		this.setPars({
			eventType: 'click',
			forceClose: false
		});
		
		return this
	},
	
	buildItems: function(){
		var elem = this.element.immediateDescendants();
		
		for(var i=0; i<elem.length; i++){
			
			if(elem[i].tagName == "DT" && elem[i].next('dd')){
				Object.extend(this.pars, {
					controller: new Df.Ui(elem[i]).set()
				});
				this.items.push( new Df.NavItem( $(elem[i]).next('dd') ).set( this.pars ) );
			}
			
		}
	}
});

/*
 ref:		Df.Dropnav
 extends:	Df.NavCollection
 returns:	Df.Dropnav
 type:		Class
*/
Df.Dropnav = Class.create(Df.NavCollection, {
	initialize: function($super, element){
		$super(element)
		
		this.setPars({
			iframe: true,
			forceClose: true
		});
		
		return this
	},
	
	set: function($super, pars){
		$super(pars);
		if(this.pars.eventType === 'click' ){
			Event.observe(document.body,'click', function(e){
				this.bodyClickEvent(e);
			}.bind(this) );
		}
		
		return this;
	},

	bodyClickEvent: function(e){
		this.hideItems();
	},
	
	buildItems: function(){
		
		var elem = this.element.immediateDescendants();
		
		for(var i=0; i<elem.length; i++){
			if($(elem[i]).down('ul')){
				
				Object.extend(this.pars, {
					controller: new Df.Ui(elem[i]).set()
				});
				
				this.items.push( new Df.NavItem( $(elem[i]).down('ul') ).set( this.pars ) );	
			}
		}
		
	}
});

/*
 ref:		Df.Cardset
 extends:	Df.NavCollection
 returns:	Df.Cardset
 type:		Class
*/
Df.Cardset = Class.create(Df.NavCollection, {
	initialize: function($super, element){
		$super(element)
		
		this.setPars({
			treatAsMenu: false,
			showClassName: 'active',
			hideClassName: false,
			activeControllerClassName: false
		});
		
		return this
	},
	
	animationCompleteEvent: function(e){
		return
	},
	
	buildItems: function(){
		
		var elem = this.element.immediateDescendants();
		
		for(var i=0; i<elem.length; i++){
			
			Object.extend(this.pars, {
				controller: new Df.Ui(elem[i]).set()
			});
			
			this.items.push( new Df.CardsetItem( $(elem[i]) ).set( this.pars ) );	
		}
	}
});

/*
 ref:		Df.CardsetItem
 extends:	Df.NavItem
 returns:	Df.CardsetItem
 type:		Class
*/
Df.CardsetItem = Class.create(Df.NavItem, {
	initialize: function($super, element){
		$super(element)
		return this;	
	},
	
	animationCompleteEvent: function(e){
		return
	},
	
	controllerClickObserver: function(e){
		Event.stop(e);
		this.show()	
		return this
	},
	
	controllerHoverOutObserver: function(e){
		Event.stop(e);
		this.status = false
		return this
	},
	
	_show: function(){
		this.status = true
		this._showByStatus()
	},
	
	_showByStatus: Df.Ui.prototype.showByStatus,
	
	showByStatus: function(){
		if( this.status){
			var index = this.pars.collection.getInstanceItemIndex(this)
			
			for (var i=0; i<index; i++) {
				this.pars.collection.items[i].hide()
				
				if(this.pars.collection.pars.hideClassName){
					this.pars.collection.items[i].getElement().addClassName(this.pars.collection.pars.hideClassName)
				}
				
				if(this.pars.collection.pars.showClassName){
					this.pars.collection.items[i].getElement().removeClassName(this.pars.collection.pars.showClassName)
				}
			}
			
			for (var i=index; i<this.pars.collection.items.length; i++) {
				this.pars.collection.items[i]._show()
				
				if(this.pars.collection.pars.hideClassName){
					this.pars.collection.items[i].getElement().addClassName(this.pars.collection.pars.hideClassName)
				}
				
				if(this.pars.collection.pars.showClassName){
					this.pars.collection.items[i].getElement().removeClassName(this.pars.collection.pars.showClassName)
				}
				
			}
			
			if(this.pars.collection.pars.showClassName){
				this.getElement().addClassName(this.pars.collection.pars.showClassName)
			}
			
			if(this.pars.collection.pars.hideClassName){
				this.getElement().removeClassName(this.pars.collection.pars.hideClassName)
			}
		}
		
		return this
	}
});

/*
 ref:		Df.Tabset
 extends:	Df.NavCollection
 returns:	Df.Tabset
 type:		Class
*/
Df.Tabset = Class.create(Df.NavCollection, {
	initialize: function($super, element){
		$super(element)
		
		this.setPars({
			eventType: 'click',
			forceClose: true,
			treatAsMenu: false
		});
		
		return this
	},
	
	buildItems: function(){
		var elem = this.element.immediateDescendants();
		
		for(var i=0; i<elem.length; i++){
			
			if(elem[i].tagName == "DT" && elem[i].next('dd')){
				Object.extend(this.pars, {
					controller: new Df.Ui(elem[i]).set()
				});
				this.items.push( new Df.TabsetItem( $(elem[i]).next('dd') ).set( this.pars ) );
			}
			
		}
	}
});

/*
 ref:		Df.TabsetItem
 extends:	Df.NavItem
 returns:	Df.TabsetItem
 type:		Class
*/
Df.TabsetItem = Class.create(Df.NavItem, {
	initialize: function($super, element){
		$super(element)
		return this;	
	},
	
	controllerHoverOutObserver: function(e){
		Event.stop(e);
		this.status = false
		return this
	},
	
	showByStatus: function(){
		
		if( this.status && !this.displayStatus ){
			
			this.pars.collection.hideItems()
			
			this.showActions()
		}
		
		return this;
	},
	
	hideClickObserver: function(e){
		return
	}
	
});