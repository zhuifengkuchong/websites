(function(jQuery){ // using jQuery 1.10

if (typeof ess === "undefined") {
    var ess = {};
}

//if (Object.isUndefined(ess)) var ess = {};

ess.LTrim = function(value) {
	var re = /^\s*/;
	return value.replace(re, '');
}
ess.RTrim = function(value) {
	var re = /\s*$/;
	return value.replace(re, '');
}
ess.trim = function(value) {
	return ess.LTrim(ess.RTrim(value));
}
ess.objectSize = function(obj) {
	var size = 0, key;
	for (key in obj) {
		if (obj.hasOwnProperty(key)) size++;
	}
	return size;
};

var device = navigator.userAgent.toLowerCase();
var ios = device.match(/(iphone|ipod|ipad)/);

var TopNav = {

	init: function () {
		var self = TopNav;

		// ipad events

		if (ios) {
			jQuery('body').removeClass('no-touch').addClass('touch');
			
			//Hide expanded top menu on window scroll
			jQuery(document).on('touchstart',function (e){
				var container = jQuery("#global-nav .more-pop");
				if (!container.is(e.target) && container.has(e.target).length === 0){
					jQuery('.navitem').find('.more-pop').css({'display':'none'});
				}
			});


			// hover fix for iPad
			var preventDef = true;
			jQuery('#wrapper').click(function () { 
				jQuery('.navitem').removeClass('navactive').find('.more-pop').css({'display':'none'});
			});
			
			// division link
			jQuery('a.top-nav').on('touchstart', function () { 
					preventDef = jQuery(this).next().is(":visible") ? false : true;
				});

			jQuery('a.top-nav, .tab-panel ul li').click(function (e) {
				var pageBodyId =jQuery('body').attr('id');
				switch (pageBodyId) {
					case 'cart':
						preventDef = false;
						break;
					case 'addresses':
						preventDef = false;
						break;
					case 'shipping':
						preventDef = false;
						break;
					case 'payment':
						preventDef = false;
						break;
					case 'review':
						preventDef = false;
						break;
					case 'receipt':
						preventDef = false;
						break;
					case 'login':
						preventDef = false;
						break;
					case 'myaccount':
						preventDef = false;
						break;
					default:
						break;
				}
				if(this.title === 'World of RL'){
					preventDef = true;
				}
				if (window.location.protocol == "https:"){
					if(this.title != 'World of RL'){
						preventDef = false;
					}
				}
 				if (preventDef) {
 					return false;
 				}
			});
		
			// reposition country selector
			jQuery(parent.window).bind('orientationchange', function (e) {
				if (jQuery('#countrysel-more[rel="footer"]').is(':visible')) {
				
					//added for custom positioning of the lightbox when in landscape mode 
					var lsModeTop = 0;
					if (jQuery.inArray(parent.window.orientation, [90, -90]) >= 0) {
						if (jQuery('#tab-explore').hasClass('active')) {
						    lsModeTop = 40;
						}
					}
					var height = jQuery('#countrysel-more').innerHeight();
					var width = jQuery('#countrysel-more').width();
					jQuery('.blockMsg').css({
						'top': (jQuery(window).height() - height) / 2 + lsModeTop + 'px',
						'left': (jQuery(window).width() - width) / 2 + 'px'
					});
				}
			});
		} else {
			jQuery('body').removeClass('touch').addClass('no-touch');
		}

		var globalNav = jQuery('#global-nav');
		globalNav.hoverIntent({
			over: function () {
				 if (jQuery(this).is('.more')) {
				 	var morepop = jQuery(this).find('.more-pop');
				 	if (morepop){
				 		morepop.css('display','block');
				 	}
				 }
			},
			out: function () {
				if (jQuery(this).is('.more')) {
					var morepop = jQuery(this).find('.more-pop');
				 	if (morepop){
				 		morepop.css('display','none');
				 	}
				}
			},
			selector: '.navitem',
			interval: 100,
			timeout: 150
		});

		// set search default text
		jQuery('#search-box-head')
			.val('search')
			.focus(function () {
				if (jQuery(this).val() == 'search'){
					jQuery(this).val('');
				}
			});


		// utility navigation

		utilityNav = jQuery('#utility-container');

		jQuery('#utility-nav').find('li').hover(function (e) {
			e.preventDefault();
			if(jQuery(this).attr('id') == 'countrysel'){
				utilityNav.css('height','25px');
				return;
			}
			jQuery('#utility-nav li').removeClass('active');
			utilityNav.find('.more-pop').hide();
			jQuery('#' + jQuery(this).attr('id') + '-more').attr('rel', 'header').show();
			utilityNav.css('height', jQuery('#utility-nav li').height() + jQuery('#utility-container .more-pop:visible').height());
		});

		utilityNav.mouseleave(function () {
			jQuery(this).css('height', 'auto').find('.more-pop').hide();
		});

		// tabbing functionality for country selector
		jQuery('#countrysel-more').find('.tabs a').click(function (e) {
			e.preventDefault();
			var sibTab = jQuery(this).attr('rel');
			jQuery('#countrysel-more').find('.tabs li').removeClass('active');
			jQuery('.tab-panel').removeClass('active');
			jQuery(this).parents('li').addClass('active');
			jQuery('#' + sibTab).addClass('active');
			if (jQuery(this).parents('#countrysel-more').attr('rel') == 'header') {
				jQuery('#utility-container').addClass('active').css('height', jQuery('#utility-nav li').height() + jQuery('.more-pop:visible').height());
			}
		});

		utilityNav.find('.more-pop').addClass('ready');

		// omniture functionality for country selector

		jQuery('#tab-shop').find('a').click(function (e) {
			e.preventDefault();
			var context = (jQuery('#countrysel-more').attr('rel') == 'footer') ? 'footer' : 'global';
			if (jQuery(this).parents('.lang').length) { // linked locales
				var country = jQuery(this).parents('.lang').siblings('.country').html();
			} else { // no linked locales
				var country = jQuery(this).html();
			}
			country = country.replace(/\s/g, '').toLowerCase();
			var href = jQuery(this).attr('href');
			var ab = 'ab=' + context + '_cs_' + country + '_US';

			omniHeaderRedesignTracking(context + '_cs_shop_' + country, 'Country Selector');
			window.location = href + (href.split('?')[1] ? '&' : '?') + ab;
		});

		jQuery('#tab-explore').find('a').click(function (e) {
			e.preventDefault();
			var context = (jQuery('#countrysel-more').attr('rel') == 'footer') ? 'footer' : 'global';
			var country = jQuery(this).html().replace(/\s/g, '').toLowerCase();
			var href = jQuery(this).attr('href');
			var ab = 'ab=' + context + '_cs_' + country + '_US';

			omniHeaderRedesignTracking(context + '_cs_explore_' + country, 'Country Selector');
			window.location = href + (href.split('?')[1] ? '&' : '?') + ab;
		});

	}
};


//Sub Nav functionality

var SubNav ={
	
	init: function(){
		var self,maxlinks,result,subNavConfig,subNavConfigArr,arr,leftNavPageUrl,pageBodyId;
		pageBodyId =jQuery('body').attr('id');
		//pages to exclude cart,addresses,checkout,shipping,payment,review,receipt,login
		if (window.location.protocol == "https:"){
			return;
		}
		switch (pageBodyId) {
			case 'cart':
				return;
			case 'addresses':
				return;
			case 'shipping':
				return;
			case 'payment':
				return;
			case 'review':
				return;
			case 'receipt':
				return;
			case 'login':
				return;
			case 'myaccount':
				return;
			default:
				break;
		}
		
		subNavConfig = SubNav.getSubMenuConfig();

		self = SubNav;
		//create JSON object
		result = {};
		//Parse string to get data for JSON population
		subNavConfigArr = subNavConfig.split(',');
		jQuery.each(subNavConfigArr, function( index, value ) {
			arr = value.split('|');
			arr[1] && (result[arr[0]] = arr[1]);
		});
		//Loop through members of JSON object and kick submenu creation based on config in subNavConfig store attrib
		jQuery.each(result, function( k, v ) {
			if(k != 'links'){
				SubNav.setSubNavHoverTrigger(k);
				if (SubNav.supportsLocalStorage()) {
					if(SubNav.menuInLocalStorage(k)) {
						if(SubNav.menuFreshCheck(k)){
							SubNav.populateSubMenuFromLocalStorage(k)	
		            	}else{
		            		SubNav.generateSubMenu(k);		
		            	}

		        	} else {
		        		SubNav.generateSubMenu(k);	
		        	}
		         } else {
		         	SubNav.generateSubMenu(k);
		    	}
			}
		});
	},
	
	menuFreshCheck:function(submenu){
		var lastMod,one_hour,diff,d;

		lastMod = parseInt(localStorage["rlSubnav_lastMod_"+submenu]);
		if(isNaN(lastMod)){
			return false
		}
		one_hour=1000*60*60;
		d = new Date();
		d = parseInt(d.getTime());
		diff = (d - lastMod);
		if(diff > one_hour){
			localStorage.removeItem('rlSubnav_'+submenu);
			localStorage.removeItem("rlSubnav_lastMod_"+submenu);
			return false;
		} else{
			return true;
		}
	},

	menuInLocalStorage:function(submenu) {
		if(localStorage.getItem('rlSubnav_'+submenu) && localStorage.getItem('rlSubnav_lastMod_'+submenu) ) {
			return true;
		} else{
			return false;
		}
	},	

	supportsLocalStorage:function() {
		if(typeof(Storage) !== "undefined") {
			return true;
		} else{
			return false;
		}
	},

	populateSubMenuFromLocalStorage:function(submenu){
		var data,menuType;
		subMenuData = SubNav.getSubMenuInLocalStorage(submenu);
		menuType = SubNav.getSubMenuType(submenu);
		SubNav.populate(subMenuData,submenu,menuType);	
	},

	getSubMenuInLocalStorage:function(submenu){
		if (!SubNav.supportsLocalStorage()) {
			return false;
		}		
		var menuFromLocalStorage = localStorage["rlSubnav_"+submenu];
		return menuFromLocalStorage;

	},

	setSubMenuInLocalStorage:function(submenu,data){
		if (!SubNav.supportsLocalStorage()){
			return false;
		}
		var d = new Date();
		localStorage.removeItem('rlSubnav_'+submenu);
		localStorage.removeItem("rlSubnav_lastMod_"+submenu);
		localStorage.setItem('rlSubnav_'+submenu, data);
		localStorage.setItem('rlSubnav_lastMod_'+submenu , d.getTime());
		return true;
	},

	getSubMenuConfig:function(){
		var subMenuConfig = jQuery('#global-nav').attr('data-value');
		return subMenuConfig;
	},

	getSubMenuMaxLinks:function(){
		var maxlinks,results,subNavConfig,subNavConfigArr,arr;

		//Default value for maximun number of links in a sub column can be overidden in config
		maxlinks = 14;
		result = {};
		subNavConfig = SubNav.getSubMenuConfig();

		//Parse string to get data for JSON population
		subNavConfigArr = subNavConfig.split(',');
		jQuery.each(subNavConfigArr, function( index, value ) {
			arr = value.split('|');
			arr[1] && (result[arr[0]] = arr[1]);
		});
		//Loop through members of JSON object and for existence of maximum number of links value
		jQuery.each(result, function( k, v ) {
			if(k == 'links'){
				maxlinks = v - 1;	
			}
		});

		return maxlinks;
	},

	getSubMenuType:function(submenu){
		var subNavConfig,subNavConfigArr,arr,result,menuType;
		//Default value for menuType
		menuType = 2;
		subNavConfig = SubNav.getSubMenuConfig();

		//create JSON object
		result = {};
		//Parse string to get data for JSON population
		subNavConfigArr = subNavConfig.split(',');
		jQuery.each(subNavConfigArr, function( index, value ) {
			arr = value.split('|');
			arr[1] && (result[arr[0]] = arr[1]);
		});
		
		//Loop through members of JSON object and for existence of submenu value
		jQuery.each(result, function( k, v ) {
			if(k == submenu){
				menuType = v;	
			}
		});

		return menuType;
	},

	generateSubMenu:function(submenu){
		if(submenu == 'world'){
			return;
		}

		var leftNavPageUrl,maxLinks,menuType;

		leftNavPageUrl = SubNav.getMenuTopLinks(submenu);	
		maxLinks = SubNav.getSubMenuMaxLinks();
		menuType = SubNav.getSubMenuType(submenu);
		SubNav.fetch (leftNavPageUrl,submenu,menuType,maxLinks);	
	},


	setSubNavHoverTrigger: function(k){
		jQuery('#global-nav > li[rel='+k+']').addClass('more');
	},

	getMenuTopLinks: function(k){
		var menuTopLink;
		menuTopLink = jQuery('#global-nav > li[rel='+k+'] > a.top-nav').attr('href');
		menuTopLink = window.location.protocol +'//'+window.location.host + menuTopLink;
		return menuTopLink;
	},

	fetch: function(url,parentContainer,menuType,maxlinks){
		jQuery.ajax({
			type: "GET",
			url: url,
			dataType: "html",
			cache:'true',
			error: function() {
				
			},
			success: function(data) {
				SubNav.fixABTags(data,parentContainer,menuType,maxlinks);
			}
		});
	},

	fixABTags: function(data,parentContainer,menuType,maxlinks){
		var leftNavData, leftNavDataFixed; 
		leftNavData = jQuery(data).find('#left-nav').html();
		leftNavDataFixed = leftNavData;
		leftNavDataFixed = leftNavDataFixed.replace(/ab=ln_/g, 'ab=tn_');
		SubNav.formatSubMenu(leftNavDataFixed,parentContainer,menuType,maxlinks);
	},

	formatSubMenu:function(data,parentContainer,menuType,maxlinks){
		if(menuType == '1'){
			SubNav.formatSubMenuRule1(data,parentContainer,menuType,maxlinks);	
		} else if(menuType == '2'){
			SubNav.formatSubMenuRule2(data,parentContainer,menuType,maxlinks);
		} else if(menuType == '3'){
			SubNav.formatSubMenuRule3(data,parentContainer,menuType,maxlinks);
		} else {
			return;
		}
	},

	//Rule Used to hardcode 1st Column of Subnav links for Brands either Men or Women based on the Top Nav link
	formatSubMenuRule1:function(data,parentContainer,menuType,maxlinks){
		var tree,col1,col2,col3,col4,col5,col6,col7,isColVissible,linklist,col1_content,col2_content,col3_content,col4_content,maxlinks,headers,links,headerText,brandsLinks,headerMen,brandsMen,headerWomen,brandsWomen,headerShoes,brandsShoes,headerChildren,brandsChildren,headerBaby,brandsBaby,headerHome,brandsHome,headerGift,brandsGift,headerSale,brandsSale,newTree1,jQueryhtml2;

		tree = jQuery("<div>" + data + "</div>");
		col1_content = "";
		col2_content = "";
		col3_content = "";
		col4_content = "";
		maxlinks = maxlinks;
		headers = [];
		links = [];
		headerText ='';
		brandsLinks ='';
		headerMen = 'Brands';
		brandsMen = '<li rel=\"shop-all-men\"><a href=\"http://www.ralphlauren.com/shop/index.jsp?categoryId=1760781&ab=tn_ men_shopall\" title=\"Shop All Brands\" class=\"\">Shop All Brands</a></li>'+
					'<li rel=\"men-purple\"><a href=\"http://www.ralphlauren.com/shop/index.jsp?categoryId=54803326&ab=tn_men_purplelabel&cp=54803326\" title=\"Purple Label\" class=\"\">Purple Label</a></li>'+
					'<li rel=\"men-black\"><a href=\"http://www.ralphlauren.com/shop/index.jsp?categoryId=2871712&ab=tn_men_blacklabel&cp=2871712\" title=\"Black Label\" class=\"\">Black Label</a></li>'+
					'<li rel=\"men-rrl\"><a href=\"http://www.ralphlauren.com/shop/index.jsp?categoryId=11588650&ab=tn_men_rrl\" title=\"RRL\" class=\"\">RRL</a></li>'+
					'<li rel=\"men-prl\"><a href=\"http://www.ralphlauren.com/shop/index.jsp?categoryId=1766205&ab=tn_men_poloralphlauren&cp=1766205\" title=\"Polo Ralph Lauren\" class=\"\">Polo Ralph Lauren</a></li>'+
					'<li rel=\"men-denim\"><a href=\"http://www.ralphlauren.com/shop/index.jsp?categoryId=56905656&ab=tn_men_denimsupply\" title=\"Denim &amp; Supply\" class=\"\">Denim &amp; Supply</a></li>'+
					'<li rel=\"men-bigtall\"><a href=\"http://www.ralphlauren.com/shop/index.jsp?categoryId=1995989&ab=tn_men_bigandtall&tall&cp=1995989\" title=\"Big &amp; Tall\" class=\"\">Big &amp; Tall</a></li>'+
					'<li rel=\"men-golf\"><a href=\"http://www.ralphlauren.com/shop/index.jsp?categoryId=1795710&ab=tn_men_golf&cp=1795710\" title=\"Golf\" class=\"\">Golf</a></li>';
		headerWomen = 'Brands';
		brandsWomen = '<li rel=\"shop-all-women\"><a href=\"http://www.ralphlauren.com/shop/index.jsp?categoryId=1760782&ab=tn_women_shopall\" title=\"Shop All Women\" class=\"\">Shop All Brands</a></li>'+
					'<li rel=\"women-collection\"><a href=\"http://www.ralphlauren.com/shop/index.jsp?categoryId=12150841&ab=tn_women_collection\" title=\"Collection\" class=\"\">Collection</a></li>'+
					'<li rel=\"women-black\"><a href=\"http://www.ralphlauren.com/shop/index.jsp?categoryId=1766615&ab=tn_women_blacklabel&cp=1766615\" title=\"Black Label\" class=\"\">Black Label</a></li>'+
					'<li rel=\"women-rrl\"><a href=\"http://www.ralphlauren.com/shop/index.jsp?categoryId=18779106&ab=tn_women_rrl\" title=\"RRL\" class=\"\">RRL</a></li>'+
					'<li rel=\"women-prl\"><a href=\"http://www.ralphlauren.com/shop/index.jsp?categoryId=41783776&ab=tn_women_poloralphlauren&cp=41783776\" title=\"Polo Ralph Lauren\" class=\"\">Polo Ralph Lauren</a></li>'+
					'<li rel=\"women-lauren\"><a href=\"http://www.ralphlauren.com/shop/index.jsp?categoryId=1766613&ab=tn_women_lauren&cp=1766613\" title=\"Lauren\" class=\"\">Lauren</a></li>'+
					'<li rel=\"women-denim\"><a href=\"http://www.ralphlauren.com/shop/index.jsp?categoryId=56906126&ab=tn_women_denimsupply\" title=\"Denim &amp; Supply\" class=\"\">Denim &amp; Supply</a></li>'+
					'<li rel=\"women-golf\"><a href=\"http://www.ralphlauren.com/shop/index.jsp?categoryId=1766618&ab=tn_women_golfandTennis&cp=1766618\" title=\"Golf &amp; Tennis\" class=\"\">Golf &amp; Tennis</a></li>';
		headerShoes = 'Brands';
		brandsShoes = '<li rel=\"\"><a href=\"\" title=\"\" class=\"\">Place holder text</a></li>'+
					'<li rel=\"\"><a href=\"\" title=\"\" class=\"\">Place holder text</a></li>'+
					'<li rel=\"\"><a href=\"\" title=\"\" class=\"\">Place holder text</a></li>'+
					'<li rel=\"\"><a href=\"\" title=\"\" class=\"\">Place holder text</a></li>'+
					'<li rel=\"\"><a href=\"\" title=\"\" class=\"\">Place holder text</a></li>'+
					'<li rel=\"\"><a href=\"\" title=\"\" class=\"\">Place holder text</a></li>'+
					'<li rel=\"\"><a href=\"\" title=\"\" class=\"\">Place holder text</a></li>';
		headerChildren = 'Brands';
		brandsChildren = '<li rel=\"\"><a href=\"\" title=\"\" class=\"\">Place holder text</a></li>'+
					'<li rel=\"\"><a href=\"\" title=\"\" class=\"\">Place holder text</a></li>'+
					'<li rel=\"\"><a href=\"\" title=\"\" class=\"\">Place holder text</a></li>'+
					'<li rel=\"\"><a href=\"\" title=\"\" class=\"\">Place holder text</a></li>'+
					'<li rel=\"\"><a href=\"\" title=\"\" class=\"\">Place holder text</a></li>'+
					'<li rel=\"\"><a href=\"\" title=\"\" class=\"\">Place holder text</a></li>'+
					'<li rel=\"\"><a href=\"\" title=\"\" class=\"\">Place holder text</a></li>';
		headerBaby = 'Brands';
		brandsBaby = '<li rel=\"\"><a href=\"\" title=\"\" class=\"\">Place holder text</a></li>'+
					'<li rel=\"\"><a href=\"\" title=\"\" class=\"\">Place holder text</a></li>'+
					'<li rel=\"\"><a href=\"\" title=\"\" class=\"\">Place holder text</a></li>'+
					'<li rel=\"\"><a href=\"\" title=\"\" class=\"\">Place holder text</a></li>'+
					'<li rel=\"\"><a href=\"\" title=\"\" class=\"\">Place holder text</a></li>'+
					'<li rel=\"\"><a href=\"\" title=\"\" class=\"\">Place holder text</a></li>'+
					'<li rel=\"\"><a href=\"\" title=\"\" class=\"\">Place holder text</a></li>';
		headerHome = 'Brands';
		brandsHome = '<li rel=\"\"><a href=\"\" title=\"\" class=\"\">Place holder text</a></li>'+
					'<li rel=\"\"><a href=\"\" title=\"\" class=\"\">Place holder text</a></li>'+
					'<li rel=\"\"><a href=\"\" title=\"\" class=\"\">Place holder text</a></li>'+
					'<li rel=\"\"><a href=\"\" title=\"\" class=\"\">Place holder text</a></li>'+
					'<li rel=\"\"><a href=\"\" title=\"\" class=\"\">Place holder text</a></li>'+
					'<li rel=\"\"><a href=\"\" title=\"\" class=\"\">Place holder text</a></li>'+
					'<li rel=\"\"><a href=\"\" title=\"\" class=\"\">Place holder text</a></li>';
		headerGift = 'Brands';
		brandsGift = '<li rel=\"\"><a href=\"\" title=\"\" class=\"\">Place holder text</a></li>'+
					'<li rel=\"\"><a href=\"\" title=\"\" class=\"\">Place holder text</a></li>'+
					'<li rel=\"\"><a href=\"\" title=\"\" class=\"\">Place holder text</a></li>'+
					'<li rel=\"\"><a href=\"\" title=\"\" class=\"\">Place holder text</a></li>'+
					'<li rel=\"\"><a href=\"\" title=\"\" class=\"\">Place holder text</a></li>'+
					'<li rel=\"\"><a href=\"\" title=\"\" class=\"\">Place holder text</a></li>'+
					'<li rel=\"\"><a href=\"\" title=\"\" class=\"\">Place holder text</a></li>';
		headerSale = 'Brands';
		brandsSale = '<li rel=\"\"><a href=\"\" title=\"\" class=\"\">Place holder text</a></li>'+
					'<li rel=\"\"><a href=\"\" title=\"\" class=\"\">Place holder text</a></li>'+
					'<li rel=\"\"><a href=\"\" title=\"\" class=\"\">Place holder text</a></li>'+
					'<li rel=\"\"><a href=\"\" title=\"\" class=\"\">Place holder text</a></li>'+
					'<li rel=\"\"><a href=\"\" title=\"\" class=\"\">Place holder text</a></li>'+
					'<li rel=\"\"><a href=\"\" title=\"\" class=\"\">Place holder text</a></li>'+
					'<li rel=\"\"><a href=\"\" title=\"\" class=\"\">Place holder text</a></li>';

		jQuery('h3',tree).each(
			 function(index) {
			 	if(index > 2) return false;
			 	headers.push(this);
			}
		);
		
		jQuery('ul',tree).each(
			 function(index) {
			 	if(index > 2) return false;
			 	links.push(this);
			}
		);
		
		//Select Brands
		switch(parentContainer) {
			case 'men':
			    headerText = headerMen;
			    brandsLinks = brandsMen;
        		break;
        	case 'women':
        	    headerText = headerWomen;
			    brandsLinks = brandsWomen;
        	    break;
        	case 'shoes':
        	    headerText = headerShoes;
			    brandsLinks = brandsShoes;
        	    break;
        	case 'children':
        	    headerText = headerChildren;
			    brandsLinks = brandsChildren;
        	    break;
        	case 'baby':
        	    headerText = headerBaby;
			    brandsLinks =brandsBaby;
        	    break;
        	case 'home':
        	    headerText = headerHome;
			    brandsLinks = brandsHome;
        	    break;
        	case 'gift':
        	    headerText = headerGift;
			    brandsLinks = brandsGift;
        	    break;
        	case 'sale':
        	    headerText = headerSale;
			    brandsLinks = brandsSale;
        	    break;
        	default:
        		
        	} 
        //Populate the Columns
		col1_content = '<h3>'+headerText+'</h3>'+'<ul class="nav-items">'+brandsLinks+'</ul>';
		
		if(headers[0] != null && links[0] != null ){
			newTree1 =jQuery('<div>'+links[0].innerHTML+'</div>');
			newTree1 = jQuery('<ul class="nav-items">'+jQuery(newTree1).html().replace(/\n/g, "").replace(/[\t ]+\</g, "<").replace(/\>[\t ]+\</g, "><").replace(/\>[\t ]+$/g, ">")+'</ul>');
			jQueryhtml2 = jQuery('<div />',{html:newTree1});
			jQueryhtml2.find('li:gt('+maxlinks+')').remove();
			jQueryhtml2 = jQueryhtml2.html();
			col2_content = '<h3>'+headers[0].innerHTML+'</h3>'+jQueryhtml2;	
		}

		if(headers[1] != null && links[1] != null ){
			newTree1 =jQuery('<div>'+links[1].innerHTML+'</div>');
			newTree1 = jQuery('<ul class="nav-items">'+jQuery(newTree1).html().replace(/\n/g, "").replace(/[\t ]+\</g, "<").replace(/\>[\t ]+\</g, "><").replace(/\>[\t ]+$/g, ">")+'</ul>');
			jQueryhtml2 = jQuery('<div />',{html:newTree1});
			jQueryhtml2.find('li:gt('+maxlinks+')').remove();
			jQueryhtml2 = jQueryhtml2.html();
			col3_content = '<h3>'+headers[1].innerHTML+'</h3>'+jQueryhtml2;	
		}

		if(headers[2] != null && links[2] != null){
			newTree1 =jQuery('<div>'+links[2].innerHTML+'</div>');
			newTree1 = jQuery('<ul class="nav-items">'+jQuery(newTree1).html().replace(/\n/g, "").replace(/[\t ]+\</g, "<").replace(/\>[\t ]+\</g, "><").replace(/\>[\t ]+$/g, ">")+'</ul>');
			jQueryhtml2 = jQuery('<div />',{html:newTree1});
			jQueryhtml2.find('li:gt('+maxlinks+')').remove();
			jQueryhtml2 = jQueryhtml2.html();
			col4_content = '<h3>'+headers[2].innerHTML+'</h3>'+jQueryhtml2;	
		}

		col1 = ('<div id='+parentContainer+'_col1'+' class="col1">'+col1_content+'</div>'),
		col2 = ('<div id='+parentContainer+'_col2'+' class="col2"></div>'),
		col3 = ('<div id='+parentContainer+'_col3'+' class="col3">'+col2_content+'</div>'),
		col4 = ('<div id='+parentContainer+'_col4'+' class="col4"></div>'),
		col5 = ('<div id='+parentContainer+'_col5'+' class="col5">'+col3_content+'</div>'),
		col6 = ('<div id='+parentContainer+'_col6'+' class="col6"></div>'),
		col7 = ('<div id='+parentContainer+'_col7'+' class="col7">'+col4_content+'</div>');

		isColVissible = (col3.length > 41) ? "" : "hidden";
		col2 = ('<div id='+parentContainer+'_col2'+' class="col2 ' +isColVissible+'"></div>');
		isColVissible = (col3.length > 41) ? "" : "hidden";
		col3 = ('<div id='+parentContainer+'_col3'+' class="col3 ' +isColVissible+'">'+col2_content+'</div>');
		isColVissible = (col5.length > 41) ? "" : "hidden";
		col4 = ('<div id='+parentContainer+'_col4'+' class="col4 ' +isColVissible+'"></div>');
		isColVissible = (col5.length > 41) ? "" : "hidden";
		col5 = ('<div id='+parentContainer+'_col5'+' class="col5 ' +isColVissible+'">'+col3_content+'</div>');
		isColVissible = (col7.length > 41) ? "" : "hidden";
		col6 = ('<div id='+parentContainer+'_col6'+' class="col6 ' +isColVissible+'"></div>');
		isColVissible = (col7.length > 41) ? "" : "hidden";
		col7 = ('<div id='+parentContainer+'_col7'+' class="col7 ' +isColVissible+'">'+col4_content+'</div>');

		data = col1+col2+col3+col4+col5+col6+col7;
		SubNav.populate(data,parentContainer,menuType);	
	},

	//Used to create a submenu with multiple headers that flows from one column to the next
	formatSubMenuRule2:function(data,parentContainer,menuType,maxlinks){
		var tree, col1,col2,col3,col4,col5,col6,col7,isColVissible,linkslist,col1_content,col2_content,col3_content,col4_content,maxlinks,headers,links,newTree1,jQueryhtml2;

		tree = jQuery("<div>" + data + "</div>");
		col1_content = "";
		col2_content = "";
		col3_content = "";
		col4_content = "";
		maxlinks = maxlinks;
		headers = [];
		links = [];
		
		jQuery('h3',tree).each(
			 function(index) {
			 	headers.push(this);
			}
		);
		
		jQuery('ul',tree).each(
			 function(index) {
			 	links.push(this);
			}
		);

		if(headers[0] != null && links[0] != null){
			newTree1 =jQuery('<div>'+links[0].innerHTML+'</div>');
			newTree1 = jQuery('<ul class="nav-items">'+jQuery(newTree1).html().replace(/\n/g, "").replace(/[\t ]+\</g, "<").replace(/\>[\t ]+\</g, "><").replace(/\>[\t ]+$/g, ">")+'</ul>');
			jQueryhtml2 = jQuery('<div />',{html:newTree1});
			jQueryhtml2.find('li:gt('+maxlinks+')').remove();
			jQueryhtml2 = jQueryhtml2.html();
			col1_content = '<h3>'+headers[0].innerHTML+'</h3>'+jQueryhtml2;	
		}
		
		if(headers[1] != null && links[1] != null ){
			newTree1 =jQuery('<div>'+links[1].innerHTML+'</div>');
			newTree1 = jQuery('<ul class="nav-items">'+jQuery(newTree1).html().replace(/\n/g, "").replace(/[\t ]+\</g, "<").replace(/\>[\t ]+\</g, "><").replace(/\>[\t ]+$/g, ">")+'</ul>');
			jQueryhtml2 = jQuery('<div />',{html:newTree1});
			jQueryhtml2.find('li:gt('+maxlinks+')').remove();
			jQueryhtml2 = jQueryhtml2.html();
			col2_content = '<h3>'+headers[1].innerHTML+'</h3>'+jQueryhtml2;	
		}

		if(headers[2] != null && links[2] != null ){
			newTree1 =jQuery('<div>'+links[2].innerHTML+'</div>');
			newTree1 = jQuery('<ul class="nav-items">'+jQuery(newTree1).html().replace(/\n/g, "").replace(/[\t ]+\</g, "<").replace(/\>[\t ]+\</g, "><").replace(/\>[\t ]+$/g, ">")+'</ul>');
			jQueryhtml2 = jQuery('<div />',{html:newTree1});
			jQueryhtml2.find('li:gt('+maxlinks+')').remove();
			jQueryhtml2 = jQueryhtml2.html();
			col3_content = '<h3>'+headers[2].innerHTML+'</h3>'+jQueryhtml2;
		}

		if(headers[3] != null && links[3] != null){
			newTree1 =jQuery('<div>'+links[3].innerHTML+'</div>');
			newTree1 = jQuery('<ul class="nav-items">'+jQuery(newTree1).html().replace(/\n/g, "").replace(/[\t ]+\</g, "<").replace(/\>[\t ]+\</g, "><").replace(/\>[\t ]+$/g, ">")+'</ul>');
			jQueryhtml2 = jQuery('<div />',{html:newTree1});
			jQueryhtml2.find('li:gt('+maxlinks+')').remove();
			jQueryhtml2 = jQueryhtml2.html();
			col4_content = '<h3>'+headers[3].innerHTML+'</h3>'+jQueryhtml2;	
		}

		col1 = ('<div id='+parentContainer+'_col1'+' class="col1">'+col1_content+'</div>'),
		col2 = ('<div id='+parentContainer+'_col2'+' class="col2"></div>'),
		col3 = ('<div id='+parentContainer+'_col3'+' class="col3">'+col2_content+'</div>'),
		col4 = ('<div id='+parentContainer+'_col4'+' class="col4"></div>'),
		col5 = ('<div id='+parentContainer+'_col5'+' class="col5">'+col3_content+'</div>'),
		col6 = ('<div id='+parentContainer+'_col6'+' class="col6"></div>'),
		col7 = ('<div id='+parentContainer+'_col7'+' class="col7">'+col4_content+'</div>');

		isColVissible = (col3.length > 41) ? "" : "hidden";
		col2 = ('<div id='+parentContainer+'_col2'+' class="col2 ' +isColVissible+'"></div>');
		isColVissible = (col3.length > 41) ? "" : "hidden";
		col3 = ('<div id='+parentContainer+'_col3'+' class="col3 ' +isColVissible+'">'+col2_content+'</div>');
		isColVissible = (col5.length > 41) ? "" : "hidden";
		col4 = ('<div id='+parentContainer+'_col4'+' class="col4 ' +isColVissible+'"></div>');
		isColVissible = (col5.length > 41) ? "" : "hidden";
		col5 = ('<div id='+parentContainer+'_col5'+' class="col5 ' +isColVissible+'">'+col3_content+'</div>');
		isColVissible = (col7.length > 41) ? "" : "hidden";
		col6 = ('<div id='+parentContainer+'_col6'+' class="col6 ' +isColVissible+'"></div>');
		isColVissible = (col7.length > 41) ? "" : "hidden";
		col7 = ('<div id='+parentContainer+'_col7'+' class="col7 ' +isColVissible+'">'+col4_content+'</div>');

		data = col1+col2+col3+col4+col5+col6+col7;
		SubNav.populate(data,parentContainer,menuType);	
	},

	//Used to create a submenu with a potential for multiple headers in each column that can flow from one column to the next
	formatSubMenuRule3:function(data,parentContainer,menuType,maxlinks){
		var tree,col1,col2,col3,col4,col5,col6,col7,isColVissible,linkslist,col1_content,col2_content,col3_content,col4_content,headers,links,colContent,colLinks,newTree1,jQueryhtml2,sum,colContentX,limit,columnHolder,colLinksCount;
		
		tree = jQuery("<div>" + data + "</div>");
		col1_content = "";
		col2_content = "";
		col3_content = "";
		col4_content = "";
		maxlinks = maxlinks;
		headers = [];
		links = [];
		
		jQuery('h3',tree).each(
			 function(index) {
			 	headers.push(this);
			}
		);
		
		jQuery('ul',tree).each(
			 function(index) {
			 	links.push(this);
			}
		);
	
		colContent=[];
		colLinks =[];
		for (var i = 0, max = headers.length; i < max; i++) {
			if(headers[i] != null && links[i] != null){
				newTree1 =jQuery('<div>'+links[i].innerHTML+'</div>');
				newTree1 = jQuery('<ul class="nav-items">'+jQuery(newTree1).html().replace(/\n/g, "").replace(/[\t ]+\</g, "<").replace(/\>[\t ]+\</g, "><").replace(/\>[\t ]+$/g, ">")+'</ul>');
				jQueryhtml2 = jQuery('<div />',{html:newTree1});
				jQueryhtml2.find('li:gt('+maxlinks+')').remove();
				colLinksCount = jQuery(".nav-items li", jQueryhtml2).length;
				colLinks.push(colLinksCount);
				jQueryhtml2 = jQueryhtml2.html();
				colContent.push('<h3>'+headers[i].innerHTML+'</h3>'+jQueryhtml2);
			}
		}

		sum = 0;
		colContentX =[];
		limit = maxlinks;
		columnHolder = null;		
		for(var i = 0, max = colLinks.length; i < max; i++){
			if (columnHolder !== null && sum + colLinks[i] <= maxlinks) {
				sum += colLinks[i]+ 1;
				columnHolder.push(colContent[i]);
			} else{
				sum = colLinks[i]+1;
				columnHolder = [colContent[i]];
				colContentX.push(columnHolder);
			}
		}
		
		for (var i = 0, max = colContentX.length; i < max; i++) {
			if(i == 0){
				col1_content = colContentX[0].join(""); 
			}
			if(i == 1){
				col2_content = colContentX[1].join("");
			}
			if(i == 2){
				col3_content = colContentX[2].join("");
			}
			if(i == 3){
				col4_content = colContentX[3].join("");
			}
		}	

		col1 = ('<div id='+parentContainer+'_col1'+' class="col1">'+col1_content+'</div>'),
		col2 = ('<div id='+parentContainer+'_col2'+' class="col2"></div>'),
		col3 = ('<div id='+parentContainer+'_col3'+' class="col3">'+col2_content+'</div>'),
		col4 = ('<div id='+parentContainer+'_col4'+' class="col4"></div>'),
		col5 = ('<div id='+parentContainer+'_col5'+' class="col5">'+col3_content+'</div>'),
		col6 = ('<div id='+parentContainer+'_col6'+' class="col6"></div>'),
		col7 = ('<div id='+parentContainer+'_col7'+' class="col7">'+col4_content+'</div>');

		isColVissible = (col3.length > 41) ? "" : "hidden";
		col2 = ('<div id='+parentContainer+'_col2'+' class="col2 ' +isColVissible+'"></div>');
		isColVissible = (col3.length > 41) ? "" : "hidden";
		col3 = ('<div id='+parentContainer+'_col3'+' class="col3 ' +isColVissible+'">'+col2_content+'</div>');
		isColVissible = (col5.length > 41) ? "" : "hidden";
		col4 = ('<div id='+parentContainer+'_col4'+' class="col4 ' +isColVissible+'"></div>');
		isColVissible = (col5.length > 41) ? "" : "hidden";
		col5 = ('<div id='+parentContainer+'_col5'+' class="col5 ' +isColVissible+'">'+col3_content+'</div>');
		isColVissible = (col7.length > 41) ? "" : "hidden";
		col6 = ('<div id='+parentContainer+'_col6'+' class="col6 ' +isColVissible+'"></div>');
		isColVissible = (col7.length > 41) ? "" : "hidden";
		col7 = ('<div id='+parentContainer+'_col7'+' class="col7 ' +isColVissible+'">'+col4_content+'</div>');

		data = col1+col2+col3+col4+col5+col6+col7;
		SubNav.populate(data,parentContainer,menuType);	
	},
	
	populate: function(data,parentContainer,menuType){
		jQuery(".navitem[rel="+parentContainer+"]").append("<div class='more-pop'></div>");
		jQuery(".navitem[rel="+parentContainer+"] .more-pop").html(data);
		if(!SubNav.menuInLocalStorage(parentContainer)) {
			SubNav.setSubMenuInLocalStorage(parentContainer,data);
		}
		SubNav.checkFoROverFlow(parentContainer);
	},

	checkFoROverFlow:function(parentContainer){
		var columnsCount = 4
		for (var i = 0, max = columnsCount; i < max; i++) {
			var headerLength = jQuery('#'+parentContainer+'_col'+(i+1)+' h3').length;
			var linksLength = jQuery('#'+parentContainer+'_col'+(i+1)+' ul.nav-items li').length;
			var lastLi = jQuery('#'+parentContainer+'_col'+(i+1)+' ul.nav-items li:last');
			
			if((linksLength > 12) || (headerLength > 1 && linksLength > 11)){
				var element = jQuery('#'+parentContainer+'_col'+(i+1));
				var jsElement = document.querySelector('#'+parentContainer+'_col'+(i+1));
				element.parent().show();
				if( (jsElement.offsetHeight < jsElement.scrollHeight) || (jsElement.offsetWidth < jsElement.scrollWidth)){
					lastLi.remove();
				}
				element.parent().hide();
			}
		}
	},

	seoClickObserver: function(e){
		if(e.target.tagName.toLowerCase() == "a"){
			if(e.target.rel.match(/^redir:/)){
				e.preventDefault();
				var url = {q:[]}
				var parts = e.target.href.split(/\?|\#/)
				url.d = parts.shift()
				while(parts.length > 0){
					var p = parts.shift()
					if (p.indexOf('=') > -1)
						url.q = p.split('&')
					else
						url.h = p
				}
				//append redir
				parts = e.target.rel.replace('redir:','').split('|')
				while(parts.length > 0){
					var p = parts.shift()
					if (p.indexOf('p+') > -1)
						url.q.push(p.replace('p+',''))
					else if (p.indexOf('p-') > -1)
						url.q = $A(url.q).without(p.replace('p-',''))
					else if (p.indexOf('h+') > -1)
						url.h = p.replace('h+','')
					else if (p.indexOf('h-') > -1)
						url.h = false
				}
				//change href
				var newLocation = url.d
				if(url.q.length > 0) newLocation += '?' + url.q.join('&')
				if(url.h) newLocation += '#' + url.h
				window.location = newLocation
			}
		}else if(e.target.tagName.toLowerCase() == "span"){
			if(e.target.parentNode.rel.match(/^redir:/)){
				e.preventDefault();
				var url = {q:[]}
				var parts = e.target.parentNode.href.split(/\?|\#/)
				url.d = parts.shift()
				while(parts.length > 0){
					var p = parts.shift()
					if (p.indexOf('=') > -1)
						url.q = p.split('&')
					else
						url.h = p
				}
				//append redir
				parts = e.target.parentNode.rel.replace('redir:','').split('|')
				while(parts.length > 0){
					var p = parts.shift()
					if (p.indexOf('p+') > -1)
						url.q.push(p.replace('p+',''))
					else if (p.indexOf('p-') > -1)
						url.q = $A(url.q).without(p.replace('p-',''))
					else if (p.indexOf('h+') > -1)
						url.h = p.replace('h+','')
					else if (p.indexOf('h-') > -1)
						url.h = false
				}
				//change href
				var newLocation = url.d
				if(url.q.length > 0) newLocation += '?' + url.q.join('&')
				if(url.h) newLocation += '#' + url.h
				window.location = newLocation
			}
		}
	}
};



// DL Box functionality

var DLBox = {

	init: function () {
		var self = DLBox;
		var DLBTimer;

		jQuery('#dlbox').show();

		self.dlCloseDelay = 4000; // 4 sec
		self.dlBox = jQuery('#dlbox');
		self.dlHeight = self.dlBox.find('#dlbox-cms').height() + 2;

		if (self.doAutoDisplay()) {
			self.dlBox.addClass('open');
		}

		if(jQuery.trim(jQuery('#rl-dl-prev-content').text()).length > 0) {
			jQuery('#rl-dl-prev-content').addClass('rl-prev-active');
		}

		jQuery('#dlbox, #rl-dl-prev-content, .handle, .dlbox-cms').click(function (e) {
			e.stopPropagation();
			self.dlBox.removeClass('loading');
			window.clearTimeout(DLBTimer);
			if (e.target != this) return; // only continue if the target itself has been clicked
			self.toggle(true);
		});

		if (self.dlBox.is('.open')) {
			self.dlBox.css('margin-top', '0px');

			// check for hover
			self.timeOut = false;
			self.isFocus = false;

			jQuery('#dlbox')
				.mouseenter(function () {
					if (!ios) {
					self.isFocus = true;
					}
					
				})
				.mouseleave(function () {
					self.isFocus = false;
					if (self.timeOut) {
						self.timeOut = false;
						self.dlBox.addClass('ready').removeClass('loading');
						self.toggle();
					}
				});

			DLBTimer = window.setTimeout(function() {
				self.timeOut = true;
				if (!self.isFocus && self.dlBox.is('.loading')) {
					self.timeOut = false;
					self.dlBox.addClass('ready').removeClass('loading');
					self.toggle();
				}
			}, self.dlCloseDelay);

		} else {
			self.dlBox.removeClass('loading').css('margin-top', '-' + self.dlHeight + 'px').removeClass('open');
		}

		return;
	},

	toggle: function (clicked) {
		var self = DLBox;
		var clicked = clicked || false;
		self.dlBox.addClass('ready');
		if (self.dlBox.is('.open')) {
			if (device.indexOf("msie") > -1) {
				self.dlBox.animate({'margin-top': '-' + self.dlHeight + 'px'}, 300).removeClass('open');
			}else{		
			self.dlBox.css('margin-top', '-' + self.dlHeight + 'px').removeClass('open');
			}
			if (clicked) {
				omniHeaderRedesignTracking('global_promo_close', 'Promo Dropdown');
			}
		} else {
		
			if (device.indexOf("msie") > -1) {
				self.dlBox.animate({'margin-top': '0px'}, 300).addClass('open');
			}else{
			self.dlBox.css('margin-top', '0px').addClass('open');
			}
			omniHeaderRedesignTracking('global_promo_open', 'Promo Dropdown');
		}
		return;
	},

	doAutoDisplay: function () {
		var self = DLBox;

		var cookieName = 'DL-Box-Promo';
		var campaignId = 'Untitled';

		if (jQuery('input[name="campaignId"]').length) {
			campaignId = jQuery('input[name="campaignId"]').val();
		}

		if (Cookie.read(cookieName) == null) { // cookie doesn't exist
			Cookie.create(cookieName, campaignId);
			return true;
		} else { // cookie does exist
			if (Cookie.read(cookieName) == campaignId) {
				return false;
			} else {
				Cookie.create(cookieName, campaignId); // set the new campaign
				return true;
			}
		}
	}
};

// Cookie Util

var Cookie = {

	create: function (name, value, days) {
		if (days) {
			var date = new Date();
			date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
			var expires = "; expires=" + date.toGMTString();
		}
		else var expires = "";
		document.cookie = name + "=" + value + expires + "; path=/";
	},

	read: function (name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for (var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') c = c.substring(1, c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
		}
		return null;
	},

	erase: function (name) {
		Cookie.create(name, "", -1);
	}
};

// Onload Functionality

jQuery(window).load(function() {
	if (jQuery('#dlbox').length) {
		DLBox.init();
	}
});
jQuery(function () {

	TopNav.init();
	var subNavConfig = jQuery('#global-nav').attr('data-value');
	// For some browsers, `attr` is undefined; for others, `attr` is false.  Check for both.
	if (typeof subNavConfig !== typeof undefined && subNavConfig !== false) {
		SubNav.init(subNavConfig);
		jQuery('#global-nav').on('click','.leftnav', function(e) {
            SubNav.seoClickObserver(e); 
        });
	}
	//fix TopNav-108
	jQuery('#global-nav ul.nav-items').removeAttr('style');

	// in site.js
	if (typeof attachStickyView == 'function') {
		attachStickyView();
	}

	// prevent RL World from linking out
	jQuery("#global-nav").find("li.world a:first").click(function(e){
		e.preventDefault();
	});

	jQuery("#sign-up-box").click(function(){
		if (this.value == 'YOUR EMAIL ADDRESS' || this.value == 'INCORRECT EMAIL ADDRESS'
			|| this.value == 'THANK YOU' || this.value == 'YOUR E-MAIL') {
			this.value = '';
		}
	});
	
	//Denim and Supply header functionality
	jQuery(".dNsSubHeadUl li").hover(
		function(){ 
				dNsLiWidth = jQuery(this).outerWidth();
				dNsDD = jQuery(this).find("ul").outerWidth();
				dNsCal = (Math.floor((dNsDD - dNsLiWidth)/2)*-1)+3;
				jQuery(this).find("ul").css("left",dNsCal+"px");
				jQuery(this).addClass("dNsHover");
				jQuery(this).find("ul").stop(true, true).delay(200).slideDown(200);
			},
		function(){	jQuery(this).removeClass("dNsHover");
			jQuery(this).find("ul").stop(true, true).slideUp(200);}
	);
	jQuery(".dNsHide li").hover(
		function(){jQuery(this).parent().show();},
		function(){jQuery(this).parent().show();}
	);

	
	(function () {

		// Free shipping message toggle
		var shipMessage = jQuery('#free-shipping'),
			shipMessageText = shipMessage.html(),
			shipMessageArray = jQuery.map(shipMessageText.split('|'), jQuery.trim),
			shipMessageLength = shipMessageArray.length;
			intervalCounter = 1,
			cycleDelay = 4000;

		if (shipMessageLength > 1) {
			shipMessage.html(shipMessageArray[0]).show(); // instantly show first message
			setInterval(function () {
				shipMessage.fadeOut(1000, function () {
					shipMessage.html(shipMessageArray[intervalCounter]);
					intervalCounter = (intervalCounter === shipMessageLength - 1) ? 0 : intervalCounter + 1;
					shipMessage.fadeIn(1000);
				});
			}, cycleDelay);
		} else {
			shipMessage.html(shipMessageText);
			shipMessage.show();
		}
		
	})();

});

})($j); // using jQuery 1.10

// added for header redesign custom link tracking events
function omniHeaderRedesignTracking (propEvarVariable, pev2Variable) {
	try {
		s.customLinkTrackEnabled = true;
		s.linkTrackVars = 'pageName,prop2,prop5,prop12,prop13,prop14,prop15,prop25,prop26,eVar1,eVar22,eVar24,eVar25,eVar27,eVar45,eVar48';
		s.linkType = 'o';
		s.prop25 = propEvarVariable;
		s.eVar1 = propEvarVariable;
		s.prop12 = s.pageName + ': ' + propEvarVariable;
		s.tl(this, s.linkType, pev2Variable);
	} catch(err) { } // do nothing
}

// used for external header
function topnav_search (e, magSearch) {
	var kw;
	if (e) {
		var keyunicode=e.charCode || e.keyCode;
		if (keyunicode == 13 || magSearch) {
			kw = $('#search-box').val();
		}
	} else {
		kw = $('#search-box').val();
	}
	if (kw) {
		// SF - BEGIN SEARCH HACK TO STOP FORM SUBMITTING BEFORE DOCUMENT LOCATION NAVIGATES
		jQuery("form").unbind("submit")
		jQuery("form").submit(function(e) {
			return false;
		});
		// SF - END SEARCH HACK
		document.location = 'http://www.ralphlauren.com/search/controller.jsp?kw=' + kw;
	}
}

// Onload Functionality

jQuery(function () { // using jQuery 1.4.2
	var device = navigator.userAgent.toLowerCase();
	var ios = device.match(/(iphone|ipod|ipad)/);

	// Change Country link in footer

	jQuery('#change-country , #countrysel').click(function () {
	
		//added for custom positioning of the lightbox when in landscape mode 
		var lsModeTop = 0;
		if (ios) {
			if (jQuery('#tab-explore').hasClass('active')) {
			    lsModeTop = 40;
			}
		}
		jQuery('#countrysel-more').attr('rel', 'footer'); // set context
		var height = jQuery('#countrysel-more').innerHeight();
		var width = 406;

		jQuery.blockUI({ 
			message: jQuery('#countrysel-more'),
			css: {
				top: (jQuery(window).height() - height) / 2 + lsModeTop + 'px',
				left: (jQuery(window).width() - width) / 2 + 'px',
				width: width + 'px',
				height: 'auto',
				border: 'none'
			},
			overlayCSS: {
				backgroundColor: '#000',
				opacity: 0.1,
				cursor: 'default'
			}, 
			centerX: true,
			centerY: true,
			baseZ: 2500
		});

		

		// reposition lightbox when switching tabs
		jQuery('#countrysel-more[rel="footer"] .tabs a').click(function (e) {
			var height = jQuery('#countrysel-more').innerHeight();
			jQuery('.blockMsg').css('top', (jQuery(window).height() - height) / 2 + lsModeTop + 'px');
		});

		jQuery('#countrysel-more a.close, .blockOverlay').click(function() {
			jQuery.unblockUI();
		}); 

		// omniture
		omniHeaderRedesignTracking('footer_cs', 'Country Selector');
	});

function centerCountrySelector(){
	var height,width,winWidth,winHeight,newWidth,newHeight;
	height = jQuery('#countrysel-more').innerHeight();
	width = jQuery('#countrysel-more').innerWidth();
	winWidth = window.innerWidth;
	winHeight = window.innerHeight;
	newHeight = (winHeight - height) / 2;
	newWidth = (winWidth - width) / 2;
	jQuery('.blockUI.blockMsg.blockPage').css({'top' : newHeight,'left' : newWidth});
}

var currentHeight,currentWidth;
 
jQuery(window).resize(function () {
   var windowHeight,windowWidth;
   windowHeight = window.innerHeight;
   windowWidth = window.innerWidth;
    if (currentHeight == undefined || currentHeight != windowHeight
      || currentWidth == undefined || currentWidth != windowWidth) {
 
    	centerCountrySelector();
    	currentHeight = windowHeight;
    	currentWidth = windowWidth;
   }
});

//Added for PMO 4858-18 "(continue shopping smP)"

function setCookieTopNav(c_name,value,exdays) {
	var exdate = new Date();
	exdate.setDate(exdate.getDate() + exdays);
	var c_value = escape(value) + ((exdays==null) ? "" : "; expires=" + exdate.toUTCString());
	document.cookie = c_name + "=" + c_value + '; path=/';
}

function getCookieTopNav(c_name) {
	var i,x,y,ARRcookies = document.cookie.split(";");
	for (i = 0; i < ARRcookies.length; i++) {
	  x = ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
	  y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
	  x = x.replace(/^\s+|\s+$/g,"");
	  if (x == c_name) {
	    return unescape(y);
      }
	}
}

	if (window.location.href.indexOf('/product/') > -1 || window.location.href.indexOf('/shop/') > -1 || window.location.href.indexOf('/family/') > -1 || window.location.href.indexOf('/search/') > -1) {
		setCookieTopNav('cartReferrerCookie', window.location, 365);
	}
	if(jQuery('#continueshop').length > 0){
		jQuery('#continueshop').click(function (event) {
			event.preventDefault();
			cartReferrerURL = getCookieTopNav('cartReferrerCookie');
			if (cartReferrerURL !== null && cartReferrerURL !== '') {
				window.location = cartReferrerURL;
			} else {
				window.location = jQuery(this).attr("href");
			}
		});
	}
});
