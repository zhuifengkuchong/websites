var device;
var ios;
$j( document ).ready(function() {

	var backToTop = $j('#backtotop');

	device = navigator.userAgent.toLowerCase();
	ios = device.match(/(ipad)/);

	function backTopDisplay() {
	    var y = $j(this).scrollTop();
	    if (y > 300) {// TODO Add flag that is set when btt is displayed, so fadeIn is not called with each scroll.
	    	utils.isIe8() ? backToTop.fadeIn(40) : backToTop.fadeIn();
	    } else {
	    	utils.isIe8() ? backToTop.fadeOut(40) : backToTop.fadeOut();
			backToTop.removeClass('clicked');
	    }	
	}

	function backTopPos(){
		var sw = $j(window).width();
		var sh = $j(window).height();
		var dw = 960;

		backToTop.removeClass('fixed small').css({'right':'','top':''});
		
		var bw = backToTop.outerWidth(true);
		if (sw >= (dw + (bw*2))) {
			backToTop
				.addClass('fixed')
				.css('right',(((sw-dw)/2)-bw)+'px');

			if (sh < (400 + bw)){
				backToTop.css('top',(sh-bw-15)+'px');	
			}
		} else {
			backToTop.removeClass('fixed').addClass('small');
			if (!ios) {
				backToTop.addClass('desktop');	
			}
		}
		backTopDisplay();
	}

	backTopPos();
	
	$j(window).resize(function() {
		backTopPos();
	});
	
	$j(window).scroll(function () {  
		backTopDisplay();  
	});
		
	backToTop.click(function(){
		backToTop.addClass('clicked');
		$j('html, body').animate({
		    scrollTop: 0
		 }, 600);
		createSiteOptGenericEngImpr('MPP: Back to Top','MPP_Back to Top');
	});
});
