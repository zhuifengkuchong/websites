(function( utils, $, undefined ) {

	var isIos = null;
	var isIE7 = null;
	var isIe8 = null;
	var isIE9 = null;
	var isIe10 = null;
	var isIe11 = null;
	var isIe = null;
	var isFF = null;
	var isTouchDevice = null;
	var isPhone = null;
	var isTablet = null;
	var touchEndTimer = null;

    utils.isTouchDevice = function() {
		if (isTouchDevice === null) {
			// look for &touch=true or &touch=false in the url to override device detection
			var touchOverride = utils.getQueryParameters()["touch"];
			if (touchOverride) {
				isTouchDevice = (touchOverride == "true");
			} else {
				//return navigator.userAgent.match(/iPad/i) != null;
				isTouchDevice = this.isTablet() || this.isPhone();
			}
		}
		return isTouchDevice;
	};

	utils.getQueryParameters = function(queryString) {

		var queryParameters = {},
		    re = /([^&=]+)=([^&]*)/g,
		    m;

		if (queryString == null) {
			queryString = location.search.substring(1);
		}

		// Remove all '%' from the query as they will cause an error in decodeURIComponent.
		queryString = queryString.replace(/%/g, "");

		// Creates a map with the query string parameters
		while ((m = re.exec(queryString))) {
			queryParameters[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
		}

		return queryParameters;
	};	

	utils.isTablet = function() {
		if (isTablet === null) {
			if (MobileEsp != null) {
				isTablet = MobileEsp.DetectTierTablet();
			} else {
				isTablet = false;
			}
		}
		return isTablet;
	};

	utils.isPhone = function() {
		if (isPhone === null) {
			if (MobileEsp != null) {
				isPhone = MobileEsp.DetectTierIphone() || MobileEsp.DetectTierOtherPhones();
			} else {
				isPhone = false;
			}
		}
		return isPhone;
	};

	utils.isIe7 = function() {
		if (isIE7 === null) {
			isIE7 = navigator.userAgent.match(/MSIE 7.0/i) !== null;
		}
		return isIE7;
	};

	utils.isIe8 = function() {
		if (isIe8 === null) {
			isIe8 = navigator.userAgent.match(/MSIE 8.0/i) !== null;
		}
		return isIe8;
	};

	utils.isIe9 = function() {
		if (isIE9 === null) {
			isIE9 = navigator.userAgent.match(/MSIE 9.0/i) !== null;
		}
		return isIE9;
	};

	utils.isIe10 = function() {
		if (isIe10 === null) {
			isIe10 = navigator.userAgent.match(/MSIE 10.0/i) !== null;
		}
		return isIe10;
	};

	utils.isIe11 = function() {
		if (isIe11 === null) {
			isIe11 = navigator.userAgent.match(/Trident\/7\./) !== null;
		}
		return isIe11;
	};

	utils.isIe = function() {
		if (isIe === null) {
			isIe = navigator.userAgent.match(/MSIE/i) !== null;
		}
		return isIe;
	};

	utils.isFF = function() {
		if (isFF === null) {
			isFF = navigator.userAgent.match(/Firefox/i) !== null;
		}
		return isFF;
	};

	utils.isIos = function() {
		if (isIos === null) {
			isIos = navigator.userAgent.match(/iphone/i) !== null || navigator.userAgent.match(/ipad/i) !== null;
		}
		return isIos;
	};

}( window.utils = window.utils || {}, $j ));