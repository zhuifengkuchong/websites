
function setBrowserClass() {

	var body = $j('body');

	/*
	if (!body.hasClass('p2p')) {
		body.addClass('p2p');
	}
	*/
	if (utils.isTouchDevice()) {
		body.removeClass('no-touch').addClass('touch');
		//$j('head').append('<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">');
	} else {
		body.addClass('no-touch');
		if(utils.isIe()) {
			if (body.hasClass('standardVersion')) {
				body.removeClass('standardVersion');
			}
			body.addClass('ieVersion');	
			if (utils.isIe7()) {
				body.addClass('ie7Version');
			}
			if (utils.isIe8()) {
				body.addClass('ie8Version');
			}
			if (utils.isIe9()) {
				body.addClass('ie9Version');
			}
			if (utils.isIe10()) {
				body.addClass('ie10Version');
			}

		}else {
			if (!body.hasClass('standardVersion')) {
				body.addClass('standardVersion');
			}
			if (utils.isFF()) {
				body.addClass('ffVersion');
			}
			if (utils.isIe11()) {
				body.removeClass('standardVersion');
				body.addClass('ieVersion');
				body.addClass('ie11Version');
			}
		}
	}
};

setBrowserClass();