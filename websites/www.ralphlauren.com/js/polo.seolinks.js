if(polo)
{}else
{	var polo = {};
}
polo.toggle = function(obj)
{
	var ary1 = $('dynamicLeftNav').getElementsByClassName('seeMoreLabel');
	for (var i = 0;i < ary1.length; i++)
	{
		ary1[i].style.display = 'none';
		ary1[i].style.height = '0px';
	}
	ary1 = $('dynamicLeftNav').getElementsByClassName('seeMoreLabelover');
	for (var i = 0;i < ary1.length; i++)
	{
		ary1[i].style.display = 'none';
		ary1[i].style.height = '0px';
	}
	var ary2 = $('dynamicLeftNav').getElementsByClassName('seeMoreItems');
	for (var i = 0;i < ary2.length; i++)
	{
		ary2[i].style.display = 'block';
	}
}

// Rewrite all anchor tags with rel tags set as indicated below
//syntax
//rel="redir:p+n=v|p-n=v|h+v|h-
//p+ adds a name value pair
//p- removes a name value pair
//h+ changes the hash
//h- removes the hash

//disect current href
polo.seoClickObserver = function(e){
	if(e.target.tagName.toLowerCase() == "a"){
		if(e.target.rel.match(/^redir:/)){
			e.stop();
			var url = {q:[]}
			var parts = e.target.href.split(/\?|\#/)
			url.d = parts.shift()
			while(parts.length > 0){
				var p = parts.shift()
				if (p.indexOf('=') > -1)
					url.q = p.split('&')
				else
					url.h = p
			}
			//append redir
			parts = e.target.rel.replace('redir:','').split('|')
			while(parts.length > 0){
				var p = parts.shift()
				if (p.indexOf('p+') > -1)
					url.q.push(p.replace('p+',''))
				else if (p.indexOf('p-') > -1)
					url.q = $A(url.q).without(p.replace('p-',''))
				else if (p.indexOf('h+') > -1)
					url.h = p.replace('h+','')
				else if (p.indexOf('h-') > -1)
					url.h = false
			}
			//change href
			var newLocation = url.d
			if(url.q.length > 0) newLocation += '?' + url.q.join('&')
			if(url.h) newLocation += '#' + url.h
			window.location = newLocation
		}
	}else if(e.target.tagName.toLowerCase() == "span"){
		if(e.target.parentNode.rel.match(/^redir:/)){
			e.stop();
			var url = {q:[]}
			var parts = e.target.parentNode.href.split(/\?|\#/)
			url.d = parts.shift()
			while(parts.length > 0){
				var p = parts.shift()
				if (p.indexOf('=') > -1)
					url.q = p.split('&')
				else
					url.h = p
			}
			//append redir
			parts = e.target.parentNode.rel.replace('redir:','').split('|')
			while(parts.length > 0){
				var p = parts.shift()
				if (p.indexOf('p+') > -1)
					url.q.push(p.replace('p+',''))
				else if (p.indexOf('p-') > -1)
					url.q = $A(url.q).without(p.replace('p-',''))
				else if (p.indexOf('h+') > -1)
					url.h = p.replace('h+','')
				else if (p.indexOf('h-') > -1)
					url.h = false
			}
			//change href
			var newLocation = url.d
			if(url.q.length > 0) newLocation += '?' + url.q.join('&')
			if(url.h) newLocation += '#' + url.h
			window.location = newLocation
		}
	}
}
