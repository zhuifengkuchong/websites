

try { 
    var locale = decodeURIComponent(sendXtraFlashParam()).split('lang=')[1];
} catch(err) { 
    var locale = "null";    
}

var device = navigator.userAgent.toLowerCase();
var ios = device.match(/(iphone|ipod|ipad)/);

var navColor = "black";
var slideColors = [];

var mW = 1400;
var mH = 754;
var heroSlider;
var heroSliderDiv = $j("#hero_carousel");
var promoSlider
var promoSliderDiv = $j("#promo_carousel");
var sendPixel = true;
var isAutoSlide = true;
var totalSlides;

$j(document).ready(function(){

    

    $j(document).on("onVideoLoopReady", function(e, videoID, videoObject) {
    // console.log("onVideoLoopReady", videoID, videoObject)
    })
    $j(document).on("onVideoLoopDone", function(e, videoID, videoObject) {
       // console.log("onVideoLoopDone", videoID, videoObject)
    })
    $j(document).on("onVideoLoopComplete", function(e, videoID, videoObject) {
        // console.log("onVideoLoopComplete", videoID, videoObject);
        // if(videoID == "blockLoopVideo_heroVideo"){
        //     videoObject.destroy()
        // }
    })

    totalSlides = $j('.heroslide').length;

    buildSliders = function() {
        // -- Create top carousel --
        heroSliderDiv.append($j(".heroslide"))
        heroSlider = heroSliderDiv.royalSlider({
                    imageScaleMode: "none",
                    autoScaleSlider: true,
                    keyboardNavEnabled: true,
                    transitionSpeed: 1000,
                    loop: true,
                    slidesSpacing: 0,
                    numImagesToPreload: totalSlides,
                    slidesDiff: 1,
                    imgWidth: mW,
                    imgHeight: mH,
                    addActiveClass: true,
                    allowCSS3: true,
                    // controlNavigation: 'bullets',
                     autoPlay: {
                        enabled: true,
                        pauseOnHover: true,
                        delay: 3500
                    }, 
                    visibleNearby: {
                        enabled: true,
                        centerArea: 1,
                        center: true,
                        //breakpoint: mW,
                        navigateByCenterClick: false
                    },
                    arrowsNavAutoHide: false
        }).data('royalSlider');

        promoSliderDiv.append($j(".promoslide"))
        promoSlider = promoSliderDiv.royalSlider({
            transitionType: 'fade',
            imgWidth: mW,
            imgHeight: 371,
            transitionSpeed: 1000,
            controlNavigation: 'none',
            controlsInside: false,
            sliderDrag: false,
            sliderTouch: false,
            navigateByClick: false,
            arrowsNav: false
        }).data('royalSlider');

        
        function fireSlidePixel(nextIdx, e) {
            objOmnitureTracking.trackEvent(totalSlides, nextIdx, e);
        }

        var eventTrigger;
        var nextIdx;

        heroSlider.ev.on('rsDragStart', function(event) {
            isDragged = true;
        });
        heroSlider.ev.on('rsDragRelease', function() {
            isDragged = false;
            sendPixel == false;
        });


        heroSlider.ev.on('rsAfterSlideChange', function (event) {
            isDragged = false;

            if (sendPixel == true) {
                // if(isAutoSlide){
                //     eventTrigger = "Autoplay";
                // }
                if(!isAutoSlide){
                     fireSlidePixel(nextIdx, eventTrigger);
                }
               
            }
        });

        heroSlider.ev.on('rsBeforeMove', function (event, type, userAction) {
            var nextSlide;
            var curr = heroSlider.currSlideId;
            var pager = $j.isNumeric(type);


            if (pager == true) {
                // isAutoSlide =false;
                eventTrigger = 'Radio';
                nextSlide = type;
                nextIdx = type + 1;
                if (nextIdx > heroSlider.numSlides) {
                    nextIdx = 1;
                    nextSlide = 0;
                }
                if (nextIdx == 0) {
                    nextIdx = heroSlider.numSlides;
                    nextSlide = heroSlider.numSlides - 1;
                }
            } else {
                sendPixel = true;
                eventTrigger = 'PrevNext';
                if (type == 'prev') {
                    if (curr == 0) {
                        nextSlide = heroSlider.numSlides - 1;
                        nextIdx = heroSlider.numSlides;
                    } else {
                        nextSlide = curr - 1;
                        nextIdx = curr;
                    }
                }
                if (type == 'next') {
                    if (curr == heroSlider.numSlides - 1) {
                        nextSlide = 0;
                        nextIdx = 1;
                    } else {
                        nextSlide = curr + 1;
                        nextIdx = curr + 2;
                    }
                }
            }
            promoSlider.goTo(nextSlide);

            isDragged = true;
        });

        hp.init();

    };

    
    function showHideBullets(_opacity){
        $j('.rsNav').css({ opacity: _opacity });
    }

}($j));



var hp = {

    init: function () {

        var self = hp;

        // GSI Selectors
        if (locale == null) { // If local
            siteHeader = $j('#sectionHeader');
            siteLogo = siteHeader.find('#topnavLogo');
            $j('body').addClass('local');
            navClassSelector = $j('body');
            navClassPrefix = 'rl-';
        } else if (locale == 'en_US') { // If EU
            siteHeader = $j('#site-header');
            headerEls = $j('#site-header > div');
            siteLogo = siteHeader.find('#logo');
            navClassSelector = $j('body');
            navClassPrefix = 'rl-';
        } else { // If EU
            siteHeader = $j('#rl-topnav');
            headerEls = $j('#rl-topnav > div');
            siteLogo = siteHeader.find('#rl-logo');
            navClassSelector = $j('body');
            navClassPrefix = 'rl-';
        }

        navClassSelector.removeClass('rl-white').addClass('rl-black');


        $j(document).scrollTop(0);

        self.introAnim();

    },

    introAnim: function () {
        $j('.lp-container').css('opacity', '1');
        
        var self = hp;
        var cssAnim = true;
        if(!ios){
            $j('.rsArrow').addClass('wide');
            $j('.rsArrow').animate({ width: '82px' }, 200);
        }
        // $j('#site-header > div,#site-header #logo,#rl-topnav > div,#rl-topnav #rl-logo').animate({ 'opacity': 1 }, 1000);

        setTimeout(function () {
            $j('#hero_carousel').css('display', 'block');
            $j('#hero_carousel').animate({
                        opacity: 1
                    },1000,function(){
                        $j('.rsArrow').animate({ opacity: 1 }, 1000);
                        // $j('.rsArrow,.rsNav').animate({ opacity: 1 }, 1000);
                        if(!ios){$j('.rsArrow').animate({ width: '52px' }, 1000);}
                    });
            $j('#promo_carousel').animate({ 'opacity': 1 }, 1000);

            if(!ios){
                $j('.rsArrow').hover(function () {
                    if (cssAnim) { $j(this).addClass('expand'); } else { $j(this).animate({ width: 82 }, 200); }
                }, function () {
                    if (cssAnim) { $j(this).removeClass('expand'); } else { $j(this).animate({ width: 52 }, 200); }
                });
            }
        }, 500);

        readjust();
        return false;

    }
}

function windowWidth() {
    if (ios) {
        winW = 980;
    } else {
        winW = $j(window).width();
    }
    return winW;
}

function readjust() {
    var newW = windowWidth();
    if (newW > 960 && !ios) { 
        $j('.rsArrowLeft').css('left', -(newW-mW)/2);
        $j('.rsArrowRight').css('right', -(newW-mW)/2);
    }else{
        $j('.rsArrowLeft').css('left', 0);
        $j('.rsArrowRight').css('right', 44); //IPAD HACK OFFSET RIGHT
    }
    updateMask();
}
function updateMask(h) {
    var sliderW = windowWidth();
    var heroW = mW;
    var sideW = Math.ceil((sliderW - heroW));
    if(!ios){
        $j('.sidepane').css({ 'width': sideW }).show();
        $j('#sideR').css({ 'right': -(sideW) });
        $j('#sideL').css({ 'left': -(sideW) });
    }else{
        $j('.sidepane').remove();
    }
}

$j(window).scroll(function(){
    readjust();
});
$j(window).resize(function () {
    readjust();
});
$j(window).mousedown(function() {
    isAutoSlide=false;
    heroSlider.stopAutoPlay();
});




/* CUSTOM PIXEL FIRING */

function firePixel(_tag) {
    objOmnitureTracking.trackevar(_tag)
}
/* ********************* ------------------------- OMNITURE TRACKING OBJECT ------------------------- ******************** */
var objOmnitureTracking = {
    trackevar: function (_tag) {
        try {
            if ((s != null) && (s != undefined)) {
                if (s.un == undefined) { s.un == ''; }
                s.linkTrackVars="eVar40";
                s.eVar40 = _tag;
                //s.t();
                s.tl(true, 'e', 'HP Carousel');

            }

        }
        catch (e) {

        }
    },

    trackEvent: function (page, slide, trigger) {
        evarFire = 'int' + '_HP_Hero_P'+page+'_S'+slide+'_'+trigger;
        // console.log('evar: ' + evarFire+" "+isAutoSlide);
        try {
            if ((s != null) && (s != undefined)) {
                if (s.un == undefined) { s.un == ''; }
                s.linkTrackVars="eVar40";
                s.eVar40 = evarFire;
                //s.t();
                s.tl(true, 'o', 'HP Carousel');
            }
        }
        catch (e) {
        }

    }

};
