var gsAnalytics = { 
	escape : function() { 
		var regex = /(\||'|\$|%|\^|\*|:|~|,)/gi; 
		return function(input) {
			return input && input.replace(regex, '')
		}; 
	}(),
	
	/* hasSmoothing is Derivative of TypeHelpers version 1.0.
	* TypeHelpers is released under the following MIT License:
	* Copyright (c) 2009 Zoltan Hawryluk
	* 
	* Permission is hereby granted, free of charge, to any person obtaining a copy
	* of this software and associated documentation files (the "Software"), to deal
	* in the Software without restriction, including without limitation the rights
	* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	* copies of the Software, and to permit persons to whom the Software is
	* furnished to do so, subject to the following conditions:
	* 
	* The above copyright notice and this permission notice shall be included in
	* all copies or substantial portions of the Software.
	* 
	* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	* THE SOFTWARE.
	*/
	hasSmoothing : function() {
		if (typeof(screen.fontSmoothingEnabled) != "undefined") {
			    return screen.fontSmoothingEnabled;
		} 
		else {
			try {
				// Create a 35x35 Canvas block.
				var canvasNode = document.createElement('canvas');
				canvasNode.width = "35";
				canvasNode.height = "35";
	
				// We must put this node into the body, otherwise
				// Safari Windows does not report correctly.
				canvasNode.style.display = 'none';
				document.body.appendChild(canvasNode);
				var ctx = canvasNode.getContext('2d');
	
				// draw a black letter 'O', 32px Arial.
				ctx.textBaseline = "top";
				ctx.font = "32px Arial";
				ctx.fillStyle = "black";
				ctx.strokeStyle = "black";
	
				ctx.fillText("O", 0, 0);
	
				// start at (8,1) and search the canvas from left to right,
				// top to bottom to see if we can find a non-black pixel.  If
				// so we return true.
				for (var j = 8; j <= 32; j++) {
					for (var i = 1; i <= 32; i++) {
						var imageData = ctx.getImageData(i, j, 1, 1).data;
						var alpha = imageData[3];
						if (alpha != 255 && alpha != 0) {
							return true; // font-smoothing must be on.
						}
					}
				}
				// didn't find any non-black pixels - return false.
				return false;
			}
			catch (ex) {
				// Something went wrong (for example, Opera cannot use the
				// canvas fillText() method.  Return null (unknown).
				return null;
			}
		}
	},
	
	readCookie : function(key) {
		var cookieValues = document.cookie.match('(?:^|;)\\s*' + key + '=([^;]*)');
		return (cookieValues) ? decodeURIComponent(cookieValues[1]) : null;
	},
            
	isArray : function(possibleArray) {
		return Object.prototype.toString.apply(possibleArray) === '[object Array]';
	},
	
	inArray : function(value, array) {
		for (var i = 0, len = array.length; i < len; i++) {
			if (array[i] === value) {
				return true;
			}
		}
		return false;
	},
	
	setCookie : function(key, value, millis) {
		var date = new Date();
		date.setTime(date.getTime() + millis);
		document.cookie = key + '=' + value + ';path=/;domain=goldmansachs.com;expires=' + date.toGMTString();
	},
	
	deleteCookie : function(key) {
		document.cookie = key + '=;expires=Thu, 01-Jan-70 00:00:01 GMT;';
	},
	
	createCommaList : function(inputList) {
		if (inputList)
			return (gsAnalytics.isArray(inputList)) ? inputList.join(',') : inputList;
		else
			return "";
	},
	
	createArray : function(inputList) {
		if (inputList)
			return (gsAnalytics.isArray(inputList)) ? inputList : inputList.split(',');
		else
			return [];
	},
	
	keysPresentIterator : function(object1, object2) {
		return function(k) {
			if (typeof object1[k] != 'undefined') {
				if (typeof object2[k] == 'undefined') {
					return false;
				}
			}
			else {
				if (typeof object2[k] != 'undefined') {
					return false;
				}
			}
		};
	},
	
	deepCompare : function(object1, object2, keys) {
		var iterate;
		if (keys) {
			iterate = function(obj, callback) {
				for (var i=0, len=keys.length; i<len; i++) {
					if (callback(keys[i]) == false) {
						return false;
					}
				}
			};
		}
		else {
			iterate = function(obj, callback) {
				var k;
				for (k in obj) {
					if (callback(k) == false) {
						return false;
					}
				}
			};
		}
		
		if (iterate(object1, gsAnalytics.keysPresentIterator(object1, object2)) === false || iterate(object2, gsAnalytics.keysPresentIterator(object2, object1)) === false) {
			return false;
		}
		
		var valueEqualsObject2 = function(k) {
			if (object1[k]) {
				if (typeof object1[k] == 'object') {
					if (gsAnalytics.deepCompare(object1[k], object2[k]) === false) {
						return false;
					}
				}
				else {
					if (object1[k] !== object2[k]) {
						return false;
					}
				}
			}
			else {
				if (object2[k]) {
					return false;
				}
			}
		};
		
		if (iterate(object1, valueEqualsObject2) === false) {
			return false;
		}
		return true;
	},
	
	isOpenInSameWindow : function(linkElement) {
		if (typeof linkElement.target == 'undefined') {
			return true;
		}
		else {
			return ((linkElement.target == '_blank') || (linkElement.target.indexOf('_') != 0));
		}
	},

	isLinkWithHref : function(linkElement) {
		return (typeof linkElement == 'object' && 
				linkElement.tagName == 'A' && 
				linkElement.href && 
				linkElement.href != '');
	},

	isInternalLink : function(linkHref) {
		var internalFiltersArray = s.linkInternalFilters.split(','); 
		for (var i = 0; i < internalFiltersArray.length; i++) {
			if (linkHref.toLowerCase().indexOf(internalFiltersArray[0].toLowerCase() != 1)) {
				return true;
			}
		}
		return false;
	},

	isDownloadLink : function(linkHref) {
		var downloadFileTypesArray = s.linkDownloadFileTypes.split(',');
		for (var i = 0; i < downloadFileTypesArray.length; i++) {
			if (linkHref.toLowerCase().indexOf('.' + downloadFileTypesArray[i].toLowerCase()) != -1) {
				return true;
			}
		}
		return false;
	},
	
	isUnmodifiedLeftClick : function(jqEvent) { // todo - make this work
		return jqEvent.which === 1 && !jqEvent.ctrlKey && !jqEvent.shiftKey && !jqEvent.altKey;
	},
	
	loadJS: function(jsPath) {
		var script = document.createElement("script");
		script.type = "text/javascript";
		script.src = jsPath;
		document.getElementsByTagName("head")[0].appendChild(script);
	},
	
	loadImage: function(imgPath) {
		var img = document.createElement('img');
		img.height = 1;
		img.width = 1;
		img.src = imgPath;
		document.getElementsByTagName("body")[0].appendChild(img);
	},
	
	loadMMPixel: function(activityID) {
		var ebRand = Math.random()+'';
		ebRand = ebRand * 1000000;
		gsAnalytics.loadJS(['http://bs.serving-sys.com/BurstingPipe/ActivityServer.bs?cn=as&ActivityID=', activityID, '&rnd=', ebRand].join(''));	
	}
};
