var GS = window['GS'] || {};

GS.Carousel = (function ($){
	GS.Carousel = function () {
		var autoRotation,
			pauseRotation = false,
			rotateInterval = 7000;
		
	    function scrollBy(offset, $c, e) {
	        var new_item_num,
	            $c_wrap = $c.find(".content-carousel-scroll-container"),
	            $c_items = $c.find(".content-carousel-item"),
	            el_on_page = $c.data("elements"),
	            $prev = $c.find(".content-carousel-arrow-left"),
	            $next = $c.find(".content-carousel-arrow-right"),
	            $p_controls = $c.find('.pagination');

	        if (!$c.data("itemNum")) {
	            $c.data("itemNum", 0);
	        }
	        
	        new_item_num = $c.data("itemNum") + offset;
	        if (new_item_num<0){
	        	new_item_num += $c_items.length;
	        }
	        if (new_item_num>$c_items.length-1) {
	        	new_item_num -= $c_items.length;
	        }
	        
        	$c_wrap.find('li.content-carousel-item').each(function(index, elm){
        		var item = $(elm);
        		if (index-new_item_num<0) {
        			item.stop().animate({
            			'left': (index-new_item_num+$c_items.length)*100 + '%'
            		});
        		} else {
        			item.stop().animate({
            			'left': (index-new_item_num)*100 + '%'
            		});	        			
        		}
        		
        	});
	        
	        $c.data("itemNum", new_item_num);

	        if($p_controls.length) {
	            $p_controls.find('.active').removeClass('active')
	            $p_controls.children().each(function(){
	                var $this = $(this);

	                if($this.data('index') == new_item_num){
	                    $this.addClass('active')
	                }
	            })
	        }

	        // Update prev/next states if circle is not set as true.
	        if ($c.data('circle')!==true) {
		        if(el_on_page + new_item_num === $c_items.length) {
		            $next.addClass('disabled');
		        } else {
		            $next.removeClass('disabled');
		        }

		        if(new_item_num === 0) {
		            $prev.addClass('disabled');
		        } else {
		            $prev.removeClass('disabled');
		        }
	        }	        
	    }

	    function buildPagination($c, total_slides) {
	        var $c_page_wrap = $c.find('.pagination');
	        
	        for(var i = 0; i < total_slides; i++) {
	            var $li = $('<a href="#"></a>');

	            if(i === 0 ) {
	                $li.addClass('active')
	            }
	            $li.data('index',i);

	            $c_page_wrap.append($li);
	        }
	    }
	    
	    function setAutoRotation ($c, el_on_page) {
            if ($c.data('auto')===true && !pauseRotation) {
            	//clear existing auto rotation timer.
            	if (autoRotation) {
            		window.clearInterval(autoRotation);
            	}
            	//set up another auto rotation timer with fresh start at 0 sec.
	            autoRotation = window.setInterval(function(){
	            	scrollBy(el_on_page, $c);
	            }, rotateInterval);
            }
	    }
	    
	    //pause auto rotation if arrow left/right and pagination elements are focused.
	    function setPauseRotation ($c, el_on_page) {
            if ($c.data('auto')===true) {     	
	            $c.find(".content-carousel-arrow, .pagination a").on('focus', function(){
                	//clear existing auto rotation timer while on focus
                	if (autoRotation) {
                		window.clearInterval(autoRotation);
                	}
                	pauseRotation = true;
	            });
	            
	            $c.find(".content-carousel-arrow, .pagination a").on('blur', function(e){
	            	//restart auto rotation timer while on blur.
    	            autoRotation = window.setInterval(function(){
    	            	scrollBy(el_on_page, $c);
    	            }, rotateInterval);
    	            pauseRotation = false;
	            });
            }	    	
	    }

	    this.init = function() {
	        var $content_carousel = $(".content-carousel");
	        	
	        $content_carousel.each(function(index, c){
	            var $c = $(c),
	                $c_items = $c.find(".content-carousel-item"),
	                $c_scroll_container = $c.find(".content-carousel-scroll-container"),
	                el_on_page, total_slides, $p_controls;
	            
	            el_on_page = $c.data("elements");
	            if (!el_on_page) {
	                if (!$c_items.length) {
	                    throw new Error("No .content-carousel-item elements were found");
	                }

	                //determine number of elements on a page
	                var width = $($c_items[0]).width();
	                var $wrap = $c.find(".content-carousel-wrap")
	                el_on_page = Math.ceil($wrap.width()/width);
	                $c.data("elements", el_on_page);
	            }

	            total_slides = Math.ceil($c_items.length / el_on_page);
	            if(total_slides > 1 && $c.data('paginate') === true) {
	               buildPagination($c, total_slides);

	               $p_controls = $c.find('.pagination');

	               $p_controls.find('a').on('click', function(e){
	            	   	e.preventDefault();
	                    var $target = $(e.target);
	                    if(!$target.hasClass('active')) {
	                        offset = $target.index() - $p_controls.find('.active').index();
	                        scrollBy(offset, $c, e);
	                        //always use el_on_page to set up auto rotation
	                        //because offset could be different every time
	                        setAutoRotation($c, el_on_page);
	                    }
	               })
	            } else if(total_slides === 1) {
	                $c.find('.content-carousel-arrow').remove();
	            }
	           
	            
	            if ($c.data('circle')!==true) {
		            $c.find(".content-carousel-arrow-left").addClass('disabled');
	            }

	            $c.find(".content-carousel-arrow-left").on('click', function(e){
	                e.preventDefault();
	                if (!$(this).hasClass('disabled')) {
	                	scrollBy(-el_on_page, $c);
	                	setAutoRotation($c, el_on_page);
	                }
	            });
	            
	            $c.find(".content-carousel-arrow-right").on('click', function(e){
	                e.preventDefault();
	                if (!$(this).hasClass('disabled')) {
	                	scrollBy(el_on_page, $c);
	                	setAutoRotation($c, el_on_page);
	                }
	            })
	            
	            if(Modernizr.touch) {
	                $c.addClass('touch');
	                $c.find('.content-carousel-wrap').swipe({
	                    allowPageScroll: 'none',

	                    swipeLeft: function(event, direction, distance, duration, fingerCount) {
	                    	if (!$(this).hasClass('disabled')) {
	                    		scrollBy(el_on_page, $c);
	                    		setAutoRotation($c, el_on_page);
	                    	}
	                    },
	                    swipeRight: function(event, direction, distance, duration, fingerCount) {
	                    	if (!$(this).hasClass('disabled')) {
	                    		scrollBy(-el_on_page, $c);
	                    		setAutoRotation($c, el_on_page);
	                    	}
	                    },
	                    allowPageScroll:'vertical' 
	                });

	            }

	            if ($c_items.length > el_on_page) {
	                $c.addClass("content-carousel-more-items");
	            }

	            $c_items.each(function(index, c_item) {
	                var $c_item = $(c_item);
	                $c_item.css({
	                    "left": (index * 100 / el_on_page)+"%"
	                });
	            });
	            
	            setAutoRotation($c, el_on_page);
	            setPauseRotation($c, el_on_page);
	        });
	        
	        var pageWidth = $(".pagination").width(),
        		divWidth = $content_carousel.width();
	        $(".pagination").css({
	        	left: 100*(divWidth-pageWidth)/(divWidth*2) + "%"
	        });
	        

	    };
	}
	return new GS.Carousel();
})(jQuery);

GS.Page.register('*', GS.Carousel.init);