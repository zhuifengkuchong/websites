(function($){
	$.fn.extend({
		CompanySearch: function(options) {
			var defaults = {
				url: ''
			};
			var opts =  $.extend(defaults, options);

			return this.each(function() {
				var o = opts,
				$this = $(this);

				var SearchObj = {
					modifySearch:function(){
						var $search = $this.find('.SearchInput'),
						$button = $this.find('.SearchButton');

						var search = function(here){
							var term = encodeURI(here.val());
							var url = o.url+term;
							if (term.length)
								window.location.href = url;
						};

						$search.removeAttr("onkeypress").keypress(function(e) {
							if(e.which == 13) {
								e.preventDefault();
								search($(this));
							}
						});

						$button.click(function(e){
							e.preventDefault();
							search($search);
						});
					}
				};
				SearchObj.modifySearch();
			});
		}
	});
})(jQuery);