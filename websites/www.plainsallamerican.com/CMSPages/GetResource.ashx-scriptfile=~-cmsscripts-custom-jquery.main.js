// page init
jQuery(function () {
    jcf.customForms.replaceAll();
    //initDropDown();
    //initOpenClose();
    //initLightbox();
    //initAccordion();
    initSameHeight();
    //initPopups();
    //initCarousel();
    //initFitVids();
    initDropDownClasses();
    //initSlideShow();
    //jQuery('input, textarea').placeholder();
    //initTooltip();
    //initMobileNav();
    //initStickyHeader();
    //initDottedText();
    initMainNavigation();
    initTimeline();
	initAccordionChildFloating();
    initBoxGrid();
    initPlaceholders();
    initMatchHeight();
	initAffixes();
});

function initAffixes() {
    $('#search-wrapper').affix(
        {
             offset: {
                  top: 200
             }
        }
    );
}

function initMatchHeight(){
  $('.panel.panel-stock-quote .panel-heading .panel-title').matchHeight();
  $(window).resize(function () {
    $('.panel.panel-stock-quote .panel-heading .panel-title').matchHeight();
  });
}

function initPlaceholders(){
  $('[placeholder]').focus(function() {
    var input = $(this);
    if (input.val() == input.attr('placeholder')) {
      input.val('');
      input.removeClass('placeholder');
    }
  }).blur(function() {
    var input = $(this);
    if (input.val() == '' || input.val() == input.attr('placeholder')) {
      input.addClass('placeholder');
      input.val(input.attr('placeholder'));
    }
  }).blur();
  $('[placeholder]').parents('form').submit(function() {
    $(this).find('[placeholder]').each(function() {
      var input = $(this);
      if (input.val() == input.attr('placeholder')) {
        input.val('');
      }
    })
  });
}

jQuery(window).load(function () {
    initCustomOpenClose();
})
function initBoxGrid(){
    var layoutType = $('#hfLayoutValue').val();
    var typeClass = 'hfLayoutValue-' + $('#hfLayoutValue').val();
	$(window).load(function () {
		$.fn.matchHeight._beforeUpdate = function(event, groups){ 
			$('.glance-box').find('.item').css('height','');
			$(groups[0].elements[0]).closest('.glance-box').removeClass('match-height');
		}
		$.fn.matchHeight._afterUpdate = function(event, groups){ 
			$('.glance-box').addClass('match-height');
		}
        $('.glance-box').addClass('match-height ' + typeClass);
		if (layoutType == "2" || layoutType == "4") {
			var height = 0;
			height += parseInt($('.item:eq(2)').css("height").replace("px", ""));
			height += parseInt($('.item:eq(3)').css("height").replace("px", ""));
			$('.glance-box').find('.item:first').attr("style", "height:" + ((height) + 10) + "px");
		} else if(layoutType == "3"){
          $('.glance-box').find('.item').css('height', parseInt($('.glance-box .item:eq(0)').css('height').replace('px',''))+35 + 'px');
        }
	});
	if ($("#hfLayoutValue").val() != "2" && $("#hfLayoutValue").val() != "4") {
		$('.glance-box').find('.item').matchHeight(false);
	}
	else
	{
		$('.glance-box').find('.item').matchHeight(false);
	}  
}
  
function initAccordionChildFloating(){
	$('.collapse').each(function(){
		var container = $(this);
		container.find('.contact').each(function(index, item){
			var contact = $(item);
			if (((index + 1) % 3) == 0) {
				$('<div class="clearfix visible-md-block visible-lg-block"></div>').insertAfter(contact);
			}
			if (((index + 1) % 2) == 0) {
				$('<div class="clearfix visible-sm-block"></div>').insertAfter(contact);
			}				
		});
	});
}

function initMainNavigation() {
    $(".nav-main > li").hoverIntent({
        over: function () {
            var target = $(this).find('.drop');
            $(target).fadeIn("fast");
        },
        out: function () {
            var target = $(this).find('.drop');
            $(target).fadeOut("fast");
        },
        timeout: 200
	});
}

function initTimeline() {
    var $timeline_block = $('.cd-timeline-block');

    //hide timeline blocks which are outside the viewport
    $timeline_block.each(function () {
        if ($(this).offset().top > $(window).scrollTop() + $(window).height() * 0.75) {
            $(this).find('.cd-timeline-img, .cd-timeline-content').addClass('is-hidden');
        }
    });

    //on scolling, show/animate timeline blocks when enter the viewport
    $(window).on('scroll', function () {
        $timeline_block.each(function () {
            if ($(this).offset().top <= $(window).scrollTop() + $(window).height() * 0.75 && $(this).find('.cd-timeline-img').hasClass('is-hidden')) {
                $(this).find('.cd-timeline-img, .cd-timeline-content').removeClass('is-hidden').addClass('bounce-in');
            }
        });
    });
}

function initDottedText() {
    var activeClass = 'active-text';
    var isTouchDevice = ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch;
    var isWinPhoneDevice = navigator.msPointerEnabled && /MSIE 10.*Touch/.test(navigator.userAgent);
    var event = isTouchDevice ? (isWinPhoneDevice ? 'MSPointerDown' : 'click') : 'click';
    var animSpeed = 400;
    jQuery('.tab-pane').each(function () {
        var wrap = jQuery(this);
        var opener = wrap.find('.more');
        var slide = wrap.find('.text');
        var slideWrap = slide.find('.text-holder');
        var fixedHeight = wrap.attr('data-fixedheight');
        var dottedText = slideWrap.find('p:first-child');
        var slideHeight;
        fixedHeight = +fixedHeight;

        function refreshState() {
            slideHeight = slideWrap.height();
            if (slideHeight < fixedHeight) {
                opener.hide();
            } else if (slideHeight > fixedHeight) {
                opener.show();
            }

            if (!wrap.hasClass(activeClass)) {
                slide.css({ height: fixedHeight });
                initDotted();
            }
        }
        function refreshHeight() {
            slideHeight = slideWrap.height();
            slide.css({
                height: slideHeight
            })
        }
        opener.on(event, function (e) {
            e.preventDefault();
            if (!wrap.hasClass(activeClass)) {
                wrap.addClass(activeClass);
                destroyDotted();
                slide.stop().animate({
                    height: slideWrap.height()
                }, {
                    duration: animSpeed, complete: function () {
                        jQuery(window).on('load resize orientationchange', refreshHeight)
                    }
                });
            }
            else {
                wrap.removeClass(activeClass);
                slide.stop().animate({ height: fixedHeight }, {
                    duration: animSpeed, complete: function () {
                        initDotted();
                        jQuery(window).on('load resize orientationchange', function () {
                            slide.css({
                                height: fixedHeight
                            })
                        })
                    }
                });
            }
            if (wrap.hasClass(activeClass)) {
                jQuery(window).off('load resize orientationchange', refreshHeight)
            }
        });
        jQuery('a[data-toggle="tab"]').on('https://www.plainsallamerican.com/CMSPages/shown.bs.tab', updateDotted)
        function initDotted() {
            dottedText.dotdotdot({
                height: fixedHeight,
                watch: "window"
            });
        }

        function updateDotted() {
            dottedText.trigger('update');
        }
        function destroyDotted() {
            dottedText.trigger('destroy');
        }
        ResponsiveHelper.addRange({
            '..767': {
                on: function () {
                    if (!wrap.hasClass(activeClass)) {
                        slide.css({ height: fixedHeight });
                        initDotted();
                    }
                    jQuery(window).on('load resize orientationchange', refreshState)
                },
                off: function () {
                    jQuery(window).on('load resize orientationchange', destroyDotted)
                    wrap.removeClass(activeClass);
                    slide.css({ height: '' });
                    destroyDotted();
                    jQuery('a[data-toggle="tab"]').off('https://www.plainsallamerican.com/CMSPages/shown.bs.tab', updateDotted)
                    jQuery(window).off('load resize orientationchange', refreshState);
                }
            }
        });
    });
}

function initStickyHeader() {
    var header = jQuery('#header');
    var win = jQuery(window);

    setState();
    win.on('resize scroll orientationchange load', setState);

    function setState() {
        if (win.scrollTop() > 30 && !header.hasClass('alt')) {
            header.addClass('alt');
        }
        else if (win.scrollTop() <= 30 && header.hasClass('alt')) {
            header.removeClass('alt');
        }
    }
}

// fade gallery init
function initSlideShow() {
    jQuery('div.promo-block').fadeGallery({
        slides: 'div.slide',
        btnPrev: 'a.btn-prev',
        btnNext: 'a.btn-next',
        pagerLinks: '.icons li',
        event: 'mouseover',
        useSwipe: true,
        autoRotation: false,
        autoHeight: true,
        switchTime: 3000,
        animSpeed: 500,
        onInit: function (self) {
            var isTouchDevice = /MSIE 10.*Touch/.test(navigator.userAgent) || ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch;
            if (!isTouchDevice) self.gallery.addClass('desktop');
        }
    });
}

function initMobileNav() {
    jQuery('.menu-opener').mobileMenu({
        pageWrapper: '#wrapper',
        initResolution: 1023,
        animSpeed: 500
    });
}

//open-close with custom start height
function initCustomOpenClose() {
    var win = jQuery(window);
    jQuery('.text-section').each(function () {
        var holder = jQuery(this);
        var opener = holder.find('.opener');
        var slide = holder.find('.slide').wrapInner('<div></div>');
        var startHeight = parseInt(slide.css('maxHeight'));
        slide.height(startHeight).css({ maxHeight: 'none' });
        var slideHeight = slide.children().outerHeight(true);
        var activeClass = 'active';
        var timer;

        function setHeight() {
            slideHeight = slide.children().outerHeight(true);
            holder.hasClass(activeClass) ? slide.css({ height: slideHeight }) : slide.css({ height: startHeight });
        }

        //set state on page load
        setHeight();
        win.on('resize load orientationchange', function () {
            clearTimeout(timer);
            timer = setTimeout(setHeight, 50);
        });

        //click handler
        opener.bind('click', function (e) {
            e.preventDefault();
            if (holder.hasClass(activeClass)) {
                holder.removeClass(activeClass);
                slide.stop().animate({ height: startHeight });
            }
            else {
                holder.addClass(activeClass);
                slide.stop().animate({ height: slide.children().outerHeight(true) });
            }
        });
    });
}

function initTooltip() {
    jQuery('[data-toggle="tooltip"]').tooltip();
}

// animated navigation init
function initDropDown() {
    var nav = jQuery('#nav');

    function initMobileAccordion() {
        nav.slideAccordion({
            opener: 'span.arrow',
            slider: 'https://www.plainsallamerican.com/CMSPages/div.drop',
            collapsible: true,
            animSpeed: 300
        });
        nav.find('.drop-list').slideAccordion({
            opener: '>a',
            slider: '>ul',
            collapsible: false,
            allowClickWhenExpanded: true,
            animSpeed: 300
        });
    }

    ResponsiveHelper.addRange({
        '..1023': {
            on: function () {
                initMobileAccordion();
            },
            off: function () {
                nav.trigger('destroy.accordion');
                nav.find('.drop-list').trigger('destroy.accordion');
            }
        },
        '1024..': {
            on: function () {
                initTouchNav();
                nav.animDropdown({
                    items: 'li',
                    drop: '>.drop',
                    animSpeed: 400,
                    effect: 'fade'
                });
            },
            off: function () {
                nav.trigger('destroyAnimDrop');
                if (window.mainTNav) {
                    window.mainTNav.destroy();
                }
            }
        }
    });
}

// scroll gallery init
function initCarousel() {
    var win = jQuery(window);
    jQuery('div.gallery').scrollGallery({
        mask: 'https://www.plainsallamerican.com/CMSPages/div.mask',
        slider: 'div.slideset',
        slides: 'div.slide',
        btnPrev: 'a.btn-prev',
        btnNext: 'a.btn-next',
        pagerLinks: '.pagination li',
        stretchSlideToMask: ((win.width() < 768) ? true : false),
        maskAutoSize: true,
        autoRotation: false,
        switchTime: 3000,
        animSpeed: 500,
        step: 1,
        onInit: function (self) {
            win.on('resize orientatinchange', function () {
                self.options.stretchSlideToMask = (win.width() < 768) ? true : false
            });
        }
    });
    jQuery('div.carousel').each(function () {
        var gallery = jQuery(this);
        var captions = gallery.find('.carousel-caption').css({ opacity: 0 });
        var slides = gallery.find('.slide');
        var animDuration = 500;
        var prevBtn = gallery.find('.btn-prev');
        var nextBtn = gallery.find('.btn-next');
        var timer;

        slides.each(function () {
            var slide = jQuery(this);
            var caption = slide.find('.carousel-caption').css({ opacity: 0 });
            slide.data('caption', caption);
        });

        gallery.scrollGallery({
            mask: 'https://www.plainsallamerican.com/CMSPages/div.mask',
            slider: 'div.slideset',
            slides: 'div.slide',
            btnPrev: 'https://www.plainsallamerican.com/CMSPages/a.prev',
            btnNext: 'https://www.plainsallamerican.com/CMSPages/a.next',
            pagerLinks: '.pagination li',
            stretchSlideToMask: true,
            maskAutoSize: true,
            autoRotation: false,
            switchTime: 3000,
            animSpeed: 500,
            step: 1,
            onInit: function (self) {
                var activeSlide = self.slides.filter('.active')
                activeSlide.find('.carousel-caption').css({ opacity: 1 });

                if (self.options.autoRotation) {
                    timer = setTimeout(function () {
                        activeSlide.find('.carousel-caption').stop().animate({ opacity: 0 }, animDuration);
                    }, self.options.switchTime - animDuration);
                }

                prevBtn.on('click', function (e) {
                    e.preventDefault();
                    clearTimeout(timer);
                    animOut(function () {
                        self.prevSlide();
                    });
                });
                nextBtn.on('click', function (e) {
                    e.preventDefault();
                    clearTimeout(timer);
                    animOut(function () {
                        self.nextSlide();
                    });
                });
            },
            onChange: function (self) {
                var activeSlide = slides.filter('.active');

                activeSlide.find('.carousel-caption').stop().animate({ opacity: 1 }, animDuration);
                if (self.options.autoRotation) {
                    clearTimeout(timer);
                    timer = setTimeout(function () {
                        activeSlide.find('.carousel-caption').stop().animate({ opacity: 0 }, animDuration);
                    }, self.options.switchTime - animDuration);
                }
            }
        });

        function animOut(callback) {
            slides.filter('.active').find('.carousel-caption').stop().animate({ opacity: 0 }, {
                duration: animDuration,
                complete: function () {
                    return callback();
                }
            });
        }
    });
}

// accordion menu init
function initAccordion() {
    jQuery('ul.accordion').slideAccordion({
        opener: 'a.opener',
        slider: 'div.slide',
        animSpeed: 300
    });
    jQuery('ul.add-nav').slideAccordion({
        opener: 'a.opener',
        slider: 'ul',
        animSpeed: 300
    });
}

// align blocks height
function initSameHeight() {
    jQuery('.article-list').sameHeight({
        elements: '.text',
        flexible: true,
        multiLine: true,
        biggestHeight: true
    });

    jQuery('#sidebar').sameHeight({
        elements: '.box',
        flexible: true,
        multiLine: true,
        biggestHeight: false,
        useMinHeight: true
    });

    jQuery('.subsidiaries-list').sameHeight({
        elements: 'li',
        flexible: true,
        multiLine: true,
        biggestHeight: true
    });
}

// open-close init
function initOpenClose() {
    var isTouchDevice = /MSIE 10.*Touch/.test(navigator.userAgent) || ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch;
    jQuery('.menu').openClose({
        activeClass: 'active',
        opener: '.opener',
        slider: '.slide',
        animSpeed: 400,
        effect: 'slide'
    });
    jQuery('div.open-close').openClose({
        activeClass: 'active',
        opener: '.opener',
        slider: '.slide',
        animSpeed: 400,
        effect: 'fade',
        animEnd: function (self, isOpened) {
            if (isOpened) jQuery('html, body').animate({ scrollTop: self.holder.offset().top }, self.options.animSpeed);
        }
    });
    jQuery('.search-open').openClose({
        hideOnClickOutside: true,
        activeClass: 'active',
        opener: '.opener',
        slider: '.search-form',
        animSpeed: 400,
        effect: 'none',
        event: 'click'
    });
    jQuery('.hubspot-block').each(function () {
        var holder = jQuery(this);
        var slide = holder.find('.slider');
        var opener = holder.find('.opener');
        var animSpeed = 500;

        opener.on('click', function (e) {
            e.preventDefault();
            holder.hasClass('active') ? hideSlide() : showSlide();
        });

        if (holder.hasClass('active')) showSlide(true);
        else hideSlide(true);

        function hideSlide(initial) {
            holder.removeClass('active');
            slide.stop().animate({ marginLeft: -slide.outerWidth() }, initial ? 0 : animSpeed);
        }
        function showSlide(initial) {
            holder.addClass('active');
            slide.stop().animate({ marginLeft: 0 }, initial ? 0 : animSpeed);
        }
    });
    jQuery('.works-list').each(function () {
        var items = jQuery(this).children();

        items.each(function () {
            var item = jQuery(this);
            var mask = item.find('.mask');
            var slide = item.find('.popup');

            function showSlide() {
                items.not(item).trigger('hideSlide');
                item.addClass('hover');
                mask.css({ height: slide.outerHeight() });
            }
            function hideSlide() {
                item.removeClass('hover');
                mask.css({ height: 0 });
            }

            if (!isTouchDevice) {
                item.on({
                    'mouseenter': showSlide,
                    'mouseleave hideSlide': hideSlide
                });
            }
            else {
                item.on({
                    'touchstart MSPointerDown': function (e) {
                        if (!item.hasClass('hover')) {
                            e.preventDefault();
                            showSlide();
                        }
                    },
                    'hideSlide': hideSlide
                });
                jQuery('body').on('click', function (e) {
                    if (item.hasClass('hover') && !jQuery(e.target).closest(item).length) {
                        item.trigger('hideSlide');
                    }
                });
            }
        });
    });
}

// fancybox modal popup init
function initLightbox() {
    jQuery('a.lightbox, a[data-rel*="lightbox"]').each(function () {
        var link = jQuery(this);
        link.attr('rel', link.attr('data-rel')).fancybox({
            padding: 0,
            margin: 0,
            cyclic: false,
            autoScale: true,
            overlayShow: true,
            overlayOpacity: 0.9,
            overlayColor: '#000000',
            titlePosition: 'inside',
            onComplete: function (box) {
                if (link.attr('href').indexOf('#') === 0) {
                    jQuery('#fancybox-content').find('a.close').unbind('https://www.plainsallamerican.com/CMSPages/click.fb').bind('https://www.plainsallamerican.com/CMSPages/click.fb', function (e) {
                        jQuery.fancybox.close();
                        e.preventDefault();
                    });
                }
            }
        });
    });
}

/* Fancybox overlay fix */
jQuery(function () {
    // detect device type
    var isTouchDevice = ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch;
    var isWinPhoneDevice = navigator.msPointerEnabled && /MSIE 10.*Touch/.test(navigator.userAgent);

    // create stylesheet
    var styleTag = jQuery('<style>').appendTo('head');
    styleSheet = styleTag.prop('sheet') || styleTag.prop('styleSheet');

    // crossbrowser style handling
    var addCSSRule = function (selector, rules, index) {
        if (styleSheet.insertRule) {
            styleSheet.insertRule(selector + '{' + rules + '}', index);
        } else {
            styleSheet.addRule(selector, rules, index);
        }
    };

    if (isTouchDevice || isWinPhoneDevice) {
        // custom positioning for mobile devices
        setTimeout(function () {
            var page = jQuery('body'),
				fancySideShadows = jQuery('#fancybox-bg-ne, #fancybox-bg-e, #fancybox-bg-se'),
				fancyOverlay = jQuery('#fancybox-overlay'),
				fancyWrap = jQuery('#fancybox-wrap');

            // override position handler
            jQuery.fancybox.center = function () {
                if (fancyWrap.innerWidth() > fancyOverlay.innerWidth()) {
                    fancyWrap.css({ padding: 0 });
                    fancySideShadows.css({ display: 'none' });
                } else {
                    fancyWrap.css({ padding: '' });
                    fancySideShadows.css({ display: '' });
                }
                fancyWrap.css({
                    left: (page.innerWidth() - fancyWrap.innerWidth()) / 2
                });
            };
            jQuery(window).on('resize orientationchange', function () {
                jQuery.fancybox.center();
            });
        }, 1);
    } else {
        // use position fixed for desktop
        addCSSRule('#fancybox-overlay', 'position:fixed;top:0;left:0');
    }
});

// popups init
function initPopups() {
    jQuery('.popup-holder').contentPopup({
        mode: 'click'
    });
}

// handle flexible video size
function initFitVids() {
    jQuery('#content').fitVids();
}

// handle dropdowns on mobile devices
function initTouchNav() {
    jQuery('#nav').each(function () {
        window.mainTNav = new TouchNav({
            navBlock: this,
            menuDrop: '.drop'
        });
    });
}

// add classes if item has dropdown
function initDropDownClasses() {
    jQuery('.nav-main li').each(function () {
        var item = jQuery(this);
        var drop = item.find('.drop');
        var link = item.find('a').eq(0);
        if (drop.length) {
            item.addClass('has-drop-down');
            if (link.length) link.addClass('has-drop-down-a');
        }
    });
}

/*
 * jQuery Dropdown plugin
 */
; (function ($) {
    $.fn.animDropdown = function (o) {
        // default options
        var options = $.extend({
            hoverClass: 'hover',
            dropClass: 'drop-active',
            items: 'li',
            drop: '>ul',
            delay: 100,
            animSpeed: 300,
            effect: 'fade'
        }, o);

        return this.each(function () {
            // options
            var nav = $(this),
				items = nav.find(options.items);

            items.addClass(options.hoverClass).each(function () {
                var item = $(this), delayTimer;
                var drop = item.find(options.drop);
                item.data('drop', drop);
                if (drop.length) {
                    dropdownEffects[options.effect].prepare({ item: item, drop: drop });
                }

                item.bind(window.TouchNav && TouchNav.isActiveOn(this) ? 'https://www.plainsallamerican.com/CMSPages/itemhover.drop' : 'https://www.plainsallamerican.com/CMSPages/mouseenter.drop', function (e) {
                    if (e.type === 'https://www.plainsallamerican.com/CMSPages/itemhover.drop') {
                        e.stopPropagation();
                    }
                    hideAllDropdowns(item);
                    item.addClass(options.hoverClass);
                    clearTimeout(delayTimer);
                    delayTimer = setTimeout(function () {
                        if (drop.length && item.hasClass(options.hoverClass)) {
                            item.addClass(options.dropClass);
                            dropdownEffects[options.effect].animate({
                                drop: drop, state: true, speed: options.animSpeed, complete: function () {
                                    // callback
                                }
                            });
                        }
                    }, options.delay);
                    item.data('timer', delayTimer);
                }).bind(window.TouchNav && TouchNav.isActiveOn(this) ? 'https://www.plainsallamerican.com/CMSPages/itemleave.drop' : 'https://www.plainsallamerican.com/CMSPages/mouseleave.drop', function (e) {
                    if (e.type === 'https://www.plainsallamerican.com/CMSPages/itemleave.drop') {
                        e.stopPropagation();
                    }
                    if (!item.hasClass(options.dropClass)) {
                        item.removeClass(options.hoverClass);
                    }
                    clearTimeout(delayTimer);
                    delayTimer = setTimeout(function () {
                        if (drop.length && item.hasClass(options.dropClass)) {
                            dropdownEffects[options.effect].animate({
                                drop: drop, state: false, speed: options.animSpeed, complete: function () {
                                    // callback
                                    item.removeClass(options.hoverClass);
                                    item.removeClass(options.dropClass);
                                }
                            });
                        }
                    }, options.delay);
                    item.data('timer', delayTimer);
                });
            });

            // hide dropdowns
            items.removeClass(options.hoverClass);
            if (dropdownEffects[options.effect].postProcess) {
                items.each(function () {
                    dropdownEffects[options.effect].postProcess({ item: $(this) });
                });
            }

            // hide current level dropdowns
            function hideAllDropdowns(except) {
                var siblings = except.siblings();
                siblings.removeClass(options.hoverClass).each(function () {
                    var item = $(this);
                    clearTimeout(item.data('timer'));
                });
                siblings.filter('.' + options.dropClass).each(function () {
                    var item = jQuery(this).removeClass(options.dropClass);
                    if (item.data('drop').length) {
                        dropdownEffects[options.effect].animate({ drop: item.data('drop'), state: false, speed: options.animSpeed });
                    }
                });
            }

            function destroy() {
                nav.find('.drop-slide-wrapper > *').removeAttr('style').unwrap('div');
                items.each(function () {
                    var item = jQuery(this);
                    if (item.data('drop')) {
                        item.data('drop').removeAttr('style');
                    }
                });
                items.unbind('https://www.plainsallamerican.com/CMSPages/itemhover.drop itemleave.drop mouseenter.drop mouseleave.drop');
                items.removeData();
                nav.off('destroyAnimDrop');
            }
            nav.on('destroyAnimDrop', destroy);
        });
    };

    // dropdown effects
    var dropdownEffects = {
        fade: {
            prepare: function (o) {
                o.drop.css({ opacity: 0, display: 'none' });
            },
            animate: function (o) {
                o.drop.stop().css('display', 'block').animate({ opacity: o.state ? 1 : 0 }, {
                    duration: o.speed || 0, complete: function () {
                        if (o.state) {
                            o.drop.css({ opacity: '' });
                        } else {
                            o.drop.css({ opacity: 0, display: 'none' });
                        }
                        if (typeof o.complete === 'function') {
                            o.complete.call(o.drop);
                        }
                    }
                });
            }
        },
        slide: {
            prepare: function (o) {
                var elementWrap = o.drop.wrap('<div class="drop-slide-wrapper">').show().parent();
                var elementHeight = o.drop.outerHeight(true);
                var elementWidth = o.drop.outerWidth(true);
                elementWrap.css({
                    height: elementHeight,
                    width: elementWidth,
                    position: 'absolute',
                    overflow: 'hidden',
                    top: o.drop.css('top'),
                    left: o.drop.css('left')
                });
                o.drop.css({
                    position: 'static',
                    display: 'block',
                    top: 'auto',
                    left: 'auto'
                });
                o.drop.data('height', elementHeight).data('wrap', elementWrap).css({ marginTop: -elementHeight });
            },
            animate: function (o) {
                o.drop.data('wrap').show().css({ overflow: 'hidden' });
                o.drop.stop().animate({ marginTop: o.state ? 0 : -o.drop.data('height') }, {
                    duration: o.speed || 0, complete: function () {
                        if (o.state) {
                            o.drop.css({ marginTop: '' });
                            o.drop.data('wrap').css({ overflow: '' });
                        } else {
                            o.drop.data('wrap').css({ display: 'none' });
                        }
                        if (typeof o.complete === 'function') {
                            o.complete.call(o.drop);
                        }
                    }
                });
            },
            postProcess: function (o) {
                if (o.item.data('drop').length) {
                    o.item.data('drop').data('wrap').css({ display: 'none' });
                }
            }
        }
    };
}(jQuery));

/*
 * jQuery Open/Close plugin
 */
; (function ($) {
    function OpenClose(options) {
        this.options = $.extend({
            addClassBeforeAnimation: true,
            hideOnClickOutside: false,
            activeClass: 'active',
            opener: '.opener',
            slider: '.slide',
            animSpeed: 400,
            effect: 'fade',
            event: 'click'
        }, options);
        this.init();
    }
    OpenClose.prototype = {
        init: function () {
            if (this.options.holder) {
                this.findElements();
                this.attachEvents();
                this.makeCallback('onInit', this);
            }
        },
        findElements: function () {
            this.holder = $(this.options.holder);
            this.opener = this.holder.find(this.options.opener);
            this.slider = this.holder.find(this.options.slider);
        },
        attachEvents: function () {
            // add handler
            var self = this;
            this.eventHandler = function (e) {
                e.preventDefault();
                if (self.slider.hasClass(slideHiddenClass)) {
                    self.showSlide();
                } else {
                    self.hideSlide();
                }
            };
            self.opener.bind(self.options.event, this.eventHandler);

            // hover mode handler
            if (self.options.event === 'over') {
                self.opener.bind('mouseenter', function () {
                    self.showSlide();
                });
                self.holder.bind('mouseleave', function () {
                    self.hideSlide();
                });
            }

            // outside click handler
            self.outsideClickHandler = function (e) {
                if (self.options.hideOnClickOutside) {
                    var target = $(e.target);
                    if (!target.is(self.holder) && !target.closest(self.holder).length) {
                        self.hideSlide();
                    }
                }
            };

            // set initial styles
            if (this.holder.hasClass(this.options.activeClass)) {
                $(document).bind('click touchstart', self.outsideClickHandler);
            } else {
                this.slider.addClass(slideHiddenClass);
            }
        },
        showSlide: function () {
            var self = this;
            if (self.options.addClassBeforeAnimation) {
                self.holder.addClass(self.options.activeClass);
            }
            self.slider.removeClass(slideHiddenClass);
            $(document).bind('click touchstart', self.outsideClickHandler);

            self.makeCallback('animStart', self, true);
            toggleEffects[self.options.effect].show({
                box: self.slider,
                speed: self.options.animSpeed,
                complete: function () {
                    if (!self.options.addClassBeforeAnimation) {
                        self.holder.addClass(self.options.activeClass);
                    }
                    self.makeCallback('animEnd', self, true);
                }
            });
        },
        hideSlide: function () {
            var self = this;
            if (self.options.addClassBeforeAnimation) {
                self.holder.removeClass(self.options.activeClass);
            }
            $(document).unbind('click touchstart', self.outsideClickHandler);

            self.makeCallback('animStart', false);
            toggleEffects[self.options.effect].hide({
                box: self.slider,
                speed: self.options.animSpeed,
                complete: function () {
                    if (!self.options.addClassBeforeAnimation) {
                        self.holder.removeClass(self.options.activeClass);
                    }
                    self.slider.addClass(slideHiddenClass);
                    self.makeCallback('animEnd', false);
                }
            });
        },
        destroy: function () {
            this.slider.removeClass(slideHiddenClass).css({ display: '' });
            this.opener.unbind(this.options.event, this.eventHandler);
            this.holder.removeClass(this.options.activeClass).removeData('OpenClose');
            $(document).unbind('click touchstart', this.outsideClickHandler);
        },
        makeCallback: function (name) {
            if (typeof this.options[name] === 'function') {
                var args = Array.prototype.slice.call(arguments);
                args.shift();
                this.options[name].apply(this, args);
            }
        }
    };

    // add stylesheet for slide on DOMReady
    var slideHiddenClass = 'js-slide-hidden';
    $(function () {
        var tabStyleSheet = $('<style type="text/css">')[0];
        var tabStyleRule = '.' + slideHiddenClass;
        tabStyleRule += '{position:absolute !important;left:-9999px !important;top:-9999px !important;display:block !important}';
        if (tabStyleSheet.styleSheet) {
            tabStyleSheet.styleSheet.cssText = tabStyleRule;
        } else {
            tabStyleSheet.appendChild(document.createTextNode(tabStyleRule));
        }
        $('head').append(tabStyleSheet);
    });

    // animation effects
    var toggleEffects = {
        slide: {
            show: function (o) {
                o.box.stop(true).hide().slideDown(o.speed, o.complete);
            },
            hide: function (o) {
                o.box.stop(true).slideUp(o.speed, o.complete);
            }
        },
        fade: {
            show: function (o) {
                o.box.stop(true).hide().fadeIn(o.speed, o.complete);
            },
            hide: function (o) {
                o.box.stop(true).fadeOut(o.speed, o.complete);
            }
        },
        none: {
            show: function (o) {
                o.box.hide().show(0, o.complete);
            },
            hide: function (o) {
                o.box.hide(0, o.complete);
            }
        }
    };

    // jQuery plugin interface
    $.fn.openClose = function (opt) {
        return this.each(function () {
            jQuery(this).data('OpenClose', new OpenClose($.extend(opt, { holder: this })));
        });
    };
}(jQuery));

/*
 * Popups plugin
 */
; (function ($) {
    function ContentPopup(opt) {
        this.options = $.extend({
            holder: null,
            popup: '.popup',
            btnOpen: '.open',
            btnClose: '.close',
            openClass: 'popup-active',
            clickEvent: 'click',
            mode: 'click',
            hideOnClickLink: true,
            hideOnClickOutside: true,
            delay: 50
        }, opt);
        if (this.options.holder) {
            this.holder = $(this.options.holder);
            this.init();
        }
    }
    ContentPopup.prototype = {
        init: function () {
            this.findElements();
            this.attachEvents();
        },
        findElements: function () {
            this.popup = this.holder.find(this.options.popup);
            this.btnOpen = this.holder.find(this.options.btnOpen);
            this.btnClose = this.holder.find(this.options.btnClose);
        },
        attachEvents: function () {
            // handle popup openers
            var self = this;
            this.clickMode = isTouchDevice || (self.options.mode === self.options.clickEvent);

            if (this.clickMode) {
                // handle click mode
                this.btnOpen.bind(self.options.clickEvent, function (e) {
                    if (self.holder.hasClass(self.options.openClass)) {
                        if (self.options.hideOnClickLink) {
                            self.hidePopup();
                        }
                    } else {
                        self.showPopup();
                    }
                    e.preventDefault();
                });

                // prepare outside click handler
                this.outsideClickHandler = this.bind(this.outsideClickHandler, this);
            } else {
                // handle hover mode
                var timer, delayedFunc = function (func) {
                    clearTimeout(timer);
                    timer = setTimeout(function () {
                        func.call(self);
                    }, self.options.delay);
                };
                this.btnOpen.bind('mouseover', function () {
                    delayedFunc(self.showPopup);
                }).bind('mouseout', function () {
                    delayedFunc(self.hidePopup);
                });
                this.popup.bind('mouseover', function () {
                    delayedFunc(self.showPopup);
                }).bind('mouseout', function () {
                    delayedFunc(self.hidePopup);
                });
            }

            // handle close buttons
            this.btnClose.bind(self.options.clickEvent, function (e) {
                self.hidePopup();
                e.preventDefault();
            });
        },
        outsideClickHandler: function (e) {
            // hide popup if clicked outside
            var targetNode = $((e.changedTouches ? e.changedTouches[0] : e).target);
            if (!targetNode.closest(this.popup).length && !targetNode.closest(this.btnOpen).length) {
                this.hidePopup();
            }
        },
        showPopup: function () {
            // reveal popup
            this.holder.addClass(this.options.openClass);
            this.popup.css({ display: 'block' });

            // outside click handler
            if (this.clickMode && this.options.hideOnClickOutside && !this.outsideHandlerActive) {
                this.outsideHandlerActive = true;
                $(document).bind('click touchstart', this.outsideClickHandler);
            }
        },
        hidePopup: function () {
            // hide popup
            this.holder.removeClass(this.options.openClass);
            this.popup.css({ display: 'none' });

            // outside click handler
            if (this.clickMode && this.options.hideOnClickOutside && this.outsideHandlerActive) {
                this.outsideHandlerActive = false;
                $(document).unbind('click touchstart', this.outsideClickHandler);
            }
        },
        bind: function (f, scope, forceArgs) {
            return function () { return f.apply(scope, forceArgs ? [forceArgs] : arguments); };
        }
    };

    // detect touch devices
    var isTouchDevice = /MSIE 10.*Touch/.test(navigator.userAgent) || ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch;

    // jQuery plugin interface
    $.fn.contentPopup = function (opt) {
        return this.each(function () {
            new ContentPopup($.extend(opt, { holder: this }));
        });
    };
}(jQuery));

/*! http://mths.be/placeholder v2.0.7 by @mathias */
; (function (window, document, $) {

    // Opera Mini v7 doesn’t support placeholder although its DOM seems to indicate so
    var isOperaMini = Object.prototype.toString.call(window.operamini) == '[object OperaMini]';
    var isInputSupported = 'placeholder' in document.createElement('input') && !isOperaMini;
    var isTextareaSupported = 'placeholder' in document.createElement('textarea') && !isOperaMini;
    var prototype = $.fn;
    var valHooks = $.valHooks;
    var propHooks = $.propHooks;
    var hooks;
    var placeholder;

    if (isInputSupported && isTextareaSupported) {

        placeholder = prototype.placeholder = function () {
            return this;
        };

        placeholder.input = placeholder.textarea = true;

    } else {

        placeholder = prototype.placeholder = function () {
            var $this = this;
            $this
				.filter((isInputSupported ? 'textarea' : ':input') + '[placeholder]')
				.not('.placeholder')
				.bind({
				    'focus.placeholder': clearPlaceholder,
				    'blur.placeholder': setPlaceholder
				})
				.data('placeholder-enabled', true)
				.trigger('blur.placeholder');
            return $this;
        };

        placeholder.input = isInputSupported;
        placeholder.textarea = isTextareaSupported;

        hooks = {
            'get': function (element) {
                var $element = $(element);

                var $passwordInput = $element.data('placeholder-password');
                if ($passwordInput) {
                    return $passwordInput[0].value;
                }

                return $element.data('placeholder-enabled') && $element.hasClass('placeholder') ? '' : element.value;
            },
            'set': function (element, value) {
                var $element = $(element);

                var $passwordInput = $element.data('placeholder-password');
                if ($passwordInput) {
                    return $passwordInput[0].value = value;
                }

                if (!$element.data('placeholder-enabled')) {
                    return element.value = value;
                }
                if (value == '') {
                    element.value = value;
                    // Issue #56: Setting the placeholder causes problems if the element continues to have focus.
                    if (element != safeActiveElement()) {
                        // We can't use `triggerHandler` here because of dummy text/password inputs :(
                        setPlaceholder.call(element);
                    }
                } else if ($element.hasClass('placeholder')) {
                    clearPlaceholder.call(element, true, value) || (element.value = value);
                } else {
                    element.value = value;
                }
                // `set` can not return `undefined`; see http://jsapi.info/jquery/1.7.1/val#L2363
                return $element;
            }
        };

        if (!isInputSupported) {
            valHooks.input = hooks;
            propHooks.value = hooks;
        }
        if (!isTextareaSupported) {
            valHooks.textarea = hooks;
            propHooks.value = hooks;
        }

        $(function () {
            // Look for forms
            $(document).delegate('form', 'submit.placeholder', function () {
                // Clear the placeholder values so they don't get submitted
                var $inputs = $('.placeholder', this).each(clearPlaceholder);
                setTimeout(function () {
                    $inputs.each(setPlaceholder);
                }, 10);
            });
        });

        // Clear placeholder values upon page reload
        $(window).bind('beforeunload.placeholder', function () {
            $('.placeholder').each(function () {
                this.value = '';
            });
        });

    }

    function args(elem) {
        // Return an object of element attributes
        var newAttrs = {};
        var rinlinejQuery = /^jQuery\d+$/;
        $.each(elem.attributes, function (i, attr) {
            if (attr.specified && !rinlinejQuery.test(attr.name)) {
                newAttrs[attr.name] = attr.value;
            }
        });
        return newAttrs;
    }

    function clearPlaceholder(event, value) {
        var input = this;
        var $input = $(input);
        if (input.value == $input.attr('placeholder') && $input.hasClass('placeholder')) {
            if ($input.data('placeholder-password')) {
                $input = $input.hide().next().show().attr('id', $input.removeAttr('id').data('placeholder-id'));
                // If `clearPlaceholder` was called from `$.valHooks.input.set`
                if (event === true) {
                    return $input[0].value = value;
                }
                $input.focus();
            } else {
                input.value = '';
                $input.removeClass('placeholder');
                input == safeActiveElement() && input.select();
            }
        }
    }

    function setPlaceholder() {
        var $replacement;
        var input = this;
        var $input = $(input);
        var id = this.id;
        if (input.value == '') {
            if (input.type == 'password') {
                if (!$input.data('placeholder-textinput')) {
                    try {
                        $replacement = $input.clone().attr({ 'type': 'text' });
                    } catch (e) {
                        $replacement = $('<input>').attr($.extend(args(this), { 'type': 'text' }));
                    }
                    $replacement
						.removeAttr('name')
						.data({
						    'placeholder-password': $input,
						    'placeholder-id': id
						})
						.bind('focus.placeholder', clearPlaceholder);
                    $input
						.data({
						    'placeholder-textinput': $replacement,
						    'placeholder-id': id
						})
						.before($replacement);
                }
                $input = $input.removeAttr('id').hide().prev().attr('id', id).show();
                // Note: `$input[0] != input` now!
            }
            $input.addClass('placeholder');
            $input[0].value = $input.attr('placeholder');
        } else {
            $input.removeClass('placeholder');
        }
    }

    function safeActiveElement() {
        // Avoid IE9 `document.activeElement` of death
        // https://github.com/mathiasbynens/jquery-placeholder/pull/99
        try {
            return document.activeElement;
        } catch (err) { }
    }

}(this, document, jQuery));

/*
 * JavaScript Custom Forms Module
 */
jcf = {
    // global options
    modules: {},
    plugins: {},
    baseOptions: {
        unselectableClass: 'jcf-unselectable',
        labelActiveClass: 'jcf-label-active',
        labelDisabledClass: 'jcf-label-disabled',
        classPrefix: 'jcf-class-',
        hiddenClass: 'jcf-hidden',
        focusClass: 'jcf-focus',
        wrapperTag: 'div'
    },
    // replacer function
    customForms: {
        setOptions: function (obj) {
            for (var p in obj) {
                if (obj.hasOwnProperty(p) && typeof obj[p] === 'object') {
                    jcf.lib.extend(jcf.modules[p].prototype.defaultOptions, obj[p]);
                }
            }
        },
        replaceAll: function (context) {
            for (var k in jcf.modules) {
                var els = jcf.lib.queryBySelector(jcf.modules[k].prototype.selector, context);
                for (var i = 0; i < els.length; i++) {
                    if (els[i].jcf) {
                        // refresh form element state
                        els[i].jcf.refreshState();
                    } else {
                        // replace form element
                        if (!jcf.lib.hasClass(els[i], 'default') && jcf.modules[k].prototype.checkElement(els[i])) {
                            new jcf.modules[k]({
                                replaces: els[i]
                            });
                        }
                    }
                }
            }
        },
        refreshAll: function (context) {
            for (var k in jcf.modules) {
                var els = jcf.lib.queryBySelector(jcf.modules[k].prototype.selector, context);
                for (var i = 0; i < els.length; i++) {
                    if (els[i].jcf) {
                        // refresh form element state
                        els[i].jcf.refreshState();
                    }
                }
            }
        },
        refreshElement: function (obj) {
            if (obj && obj.jcf) {
                obj.jcf.refreshState();
            }
        },
        destroyAll: function () {
            for (var k in jcf.modules) {
                var els = jcf.lib.queryBySelector(jcf.modules[k].prototype.selector);
                for (var i = 0; i < els.length; i++) {
                    if (els[i].jcf) {
                        els[i].jcf.destroy();
                    }
                }
            }
        }
    },
    // detect device type
    isTouchDevice: ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch,
    isWinPhoneDevice: navigator.msPointerEnabled && /MSIE 10.*Touch/.test(navigator.userAgent),
    // define base module
    setBaseModule: function (obj) {
        jcf.customControl = function (opt) {
            this.options = jcf.lib.extend({}, jcf.baseOptions, this.defaultOptions, opt);
            this.init();
        };
        for (var p in obj) {
            jcf.customControl.prototype[p] = obj[p];
        }
    },
    // add module to jcf.modules
    addModule: function (obj) {
        if (obj.name) {
            // create new module proto class
            jcf.modules[obj.name] = function () {
                jcf.modules[obj.name].superclass.constructor.apply(this, arguments);
            }
            jcf.lib.inherit(jcf.modules[obj.name], jcf.customControl);
            for (var p in obj) {
                jcf.modules[obj.name].prototype[p] = obj[p]
            }
            // on create module
            jcf.modules[obj.name].prototype.onCreateModule();
            // make callback for exciting modules
            for (var mod in jcf.modules) {
                if (jcf.modules[mod] != jcf.modules[obj.name]) {
                    jcf.modules[mod].prototype.onModuleAdded(jcf.modules[obj.name]);
                }
            }
        }
    },
    // add plugin to jcf.plugins
    addPlugin: function (obj) {
        if (obj && obj.name) {
            jcf.plugins[obj.name] = function () {
                this.init.apply(this, arguments);
            }
            for (var p in obj) {
                jcf.plugins[obj.name].prototype[p] = obj[p];
            }
        }
    },
    // miscellaneous init
    init: function () {
        if (navigator.pointerEnabled || navigator.msPointerEnabled) {
            // use pointer events instead of mouse events
            this.eventPress = navigator.pointerEnabled ? 'pointerdown' : 'MSPointerDown';
            this.eventMove = navigator.pointerEnabled ? 'pointermove' : 'MSPointerMove';
            this.eventRelease = navigator.pointerEnabled ? 'pointerup' : 'MSPointerUp';
        } else {
            // handle default desktop mouse events
            this.eventPress = 'mousedown';
            this.eventMove = 'mousemove';
            this.eventRelease = 'mouseup';
        }
        if (this.isTouchDevice) {
            // handle touch events also
            this.eventPress += ' touchstart';
            this.eventMove += ' touchmove';
            this.eventRelease += ' touchend';
        }

        setTimeout(function () {
            jcf.lib.domReady(function () {
                jcf.initStyles();
            });
        }, 1);
        return this;
    },
    initStyles: function () {
        // create <style> element and rules
        var head = document.getElementsByTagName('head')[0],
			style = document.createElement('style'),
			rules = document.createTextNode('.' + jcf.baseOptions.unselectableClass + '{' +
				'-moz-user-select:none;' +
				'-webkit-tap-highlight-color:rgba(255,255,255,0);' +
				'-webkit-user-select:none;' +
				'user-select:none;' +
			'}');

        // append style element
        style.type = 'text/css';
        if (style.styleSheet) {
            style.styleSheet.cssText = rules.nodeValue;
        } else {
            style.appendChild(rules);
        }
        head.appendChild(style);
    }
}.init();

/*
 * Custom Form Control prototype
 */
jcf.setBaseModule({
    init: function () {
        if (this.options.replaces) {
            this.realElement = this.options.replaces;
            this.realElement.jcf = this;
            this.replaceObject();
        }
    },
    defaultOptions: {
        // default module options (will be merged with base options)
    },
    checkElement: function (el) {
        return true; // additional check for correct form element
    },
    replaceObject: function () {
        this.createWrapper();
        this.attachEvents();
        this.fixStyles();
        this.setupWrapper();
    },
    createWrapper: function () {
        this.fakeElement = jcf.lib.createElement(this.options.wrapperTag);
        this.labelFor = jcf.lib.getLabelFor(this.realElement);
        jcf.lib.disableTextSelection(this.fakeElement);
        jcf.lib.addClass(this.fakeElement, jcf.lib.getAllClasses(this.realElement.className, this.options.classPrefix));
        jcf.lib.addClass(this.realElement, jcf.baseOptions.hiddenClass);
    },
    attachEvents: function () {
        jcf.lib.event.add(this.realElement, 'focus', this.onFocusHandler, this);
        jcf.lib.event.add(this.realElement, 'blur', this.onBlurHandler, this);
        jcf.lib.event.add(this.fakeElement, 'click', this.onFakeClick, this);
        jcf.lib.event.add(this.fakeElement, jcf.eventPress, this.onFakePressed, this);
        jcf.lib.event.add(this.fakeElement, jcf.eventRelease, this.onFakeReleased, this);

        if (this.labelFor) {
            this.labelFor.jcf = this;
            jcf.lib.event.add(this.labelFor, 'click', this.onFakeClick, this);
            jcf.lib.event.add(this.labelFor, jcf.eventPress, this.onFakePressed, this);
            jcf.lib.event.add(this.labelFor, jcf.eventRelease, this.onFakeReleased, this);
        }
    },
    fixStyles: function () {
        // hide mobile webkit tap effect
        if (jcf.isTouchDevice) {
            var tapStyle = 'rgba(255,255,255,0)';
            this.realElement.style.webkitTapHighlightColor = tapStyle;
            this.fakeElement.style.webkitTapHighlightColor = tapStyle;
            if (this.labelFor) {
                this.labelFor.style.webkitTapHighlightColor = tapStyle;
            }
        }
    },
    setupWrapper: function () {
        // implement in subclass
    },
    refreshState: function () {
        // implement in subclass
    },
    destroy: function () {
        if (this.fakeElement && this.fakeElement.parentNode) {
            this.fakeElement.parentNode.insertBefore(this.realElement, this.fakeElement);
            this.fakeElement.parentNode.removeChild(this.fakeElement);
        }
        jcf.lib.removeClass(this.realElement, jcf.baseOptions.hiddenClass);
        this.realElement.jcf = null;
    },
    onFocus: function () {
        // emulated focus event
        jcf.lib.addClass(this.fakeElement, this.options.focusClass);
    },
    onBlur: function (cb) {
        // emulated blur event
        jcf.lib.removeClass(this.fakeElement, this.options.focusClass);
    },
    onFocusHandler: function () {
        // handle focus loses
        if (this.focused) return;
        this.focused = true;

        // handle touch devices also
        if (jcf.isTouchDevice) {
            if (jcf.focusedInstance && jcf.focusedInstance.realElement != this.realElement) {
                jcf.focusedInstance.onBlur();
                jcf.focusedInstance.realElement.blur();
            }
            jcf.focusedInstance = this;
        }
        this.onFocus.apply(this, arguments);
    },
    onBlurHandler: function () {
        // handle focus loses
        if (!this.pressedFlag) {
            this.focused = false;
            this.onBlur.apply(this, arguments);
        }
    },
    onFakeClick: function () {
        if (jcf.isTouchDevice) {
            this.onFocus();
        } else if (!this.realElement.disabled) {
            this.realElement.focus();
        }
    },
    onFakePressed: function (e) {
        this.pressedFlag = true;
    },
    onFakeReleased: function () {
        this.pressedFlag = false;
    },
    onCreateModule: function () {
        // implement in subclass
    },
    onModuleAdded: function (module) {
        // implement in subclass
    },
    onControlReady: function () {
        // implement in subclass
    }
});

/*
 * JCF Utility Library
 */
jcf.lib = {
    bind: function (func, scope) {
        return function () {
            return func.apply(scope, arguments);
        };
    },
    browser: (function () {
        var ua = navigator.userAgent.toLowerCase(), res = {},
		match = /(webkit)[ \/]([\w.]+)/.exec(ua) || /(opera)(?:.*version)?[ \/]([\w.]+)/.exec(ua) ||
				/(msie) ([\w.]+)/.exec(ua) || ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+))?/.exec(ua) || [];
        res[match[1]] = true;
        res.version = match[2] || "0";
        res.safariMac = ua.indexOf('mac') != -1 && ua.indexOf('safari') != -1;
        return res;
    })(),
    getOffset: function (obj) {
        if (obj.getBoundingClientRect && !jcf.isWinPhoneDevice) {
            var scrollLeft = window.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft;
            var scrollTop = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop;
            var clientLeft = document.documentElement.clientLeft || document.body.clientLeft || 0;
            var clientTop = document.documentElement.clientTop || document.body.clientTop || 0;
            return {
                top: Math.round(obj.getBoundingClientRect().top + scrollTop - clientTop),
                left: Math.round(obj.getBoundingClientRect().left + scrollLeft - clientLeft)
            };
        } else {
            var posLeft = 0, posTop = 0;
            while (obj.offsetParent) { posLeft += obj.offsetLeft; posTop += obj.offsetTop; obj = obj.offsetParent; }
            return { top: posTop, left: posLeft };
        }
    },
    getScrollTop: function () {
        return window.pageYOffset || document.documentElement.scrollTop;
    },
    getScrollLeft: function () {
        return window.pageXOffset || document.documentElement.scrollLeft;
    },
    getWindowWidth: function () {
        return document.compatMode == 'CSS1Compat' ? document.documentElement.clientWidth : document.body.clientWidth;
    },
    getWindowHeight: function () {
        return document.compatMode == 'CSS1Compat' ? document.documentElement.clientHeight : document.body.clientHeight;
    },
    getStyle: function (el, prop) {
        if (document.defaultView && document.defaultView.getComputedStyle) {
            return document.defaultView.getComputedStyle(el, null)[prop];
        } else if (el.currentStyle) {
            return el.currentStyle[prop];
        } else {
            return el.style[prop];
        }
    },
    getParent: function (obj, selector) {
        while (obj.parentNode && obj.parentNode != document.body) {
            if (obj.parentNode.tagName.toLowerCase() == selector.toLowerCase()) {
                return obj.parentNode;
            }
            obj = obj.parentNode;
        }
        return false;
    },
    isParent: function (parent, child) {
        while (child.parentNode) {
            if (child.parentNode === parent) {
                return true;
            }
            child = child.parentNode;
        }
        return false;
    },
    getLabelFor: function (object) {
        var parentLabel = jcf.lib.getParent(object, 'label');
        if (parentLabel) {
            return parentLabel;
        } else if (object.id) {
            return jcf.lib.queryBySelector('label[for="' + object.id + '"]')[0];
        }
    },
    disableTextSelection: function (el) {
        if (typeof el.onselectstart !== 'undefined') {
            el.onselectstart = function () { return false; };
        } else if (window.opera) {
            el.setAttribute('unselectable', 'on');
        } else {
            jcf.lib.addClass(el, jcf.baseOptions.unselectableClass);
        }
    },
    enableTextSelection: function (el) {
        if (typeof el.onselectstart !== 'undefined') {
            el.onselectstart = null;
        } else if (window.opera) {
            el.removeAttribute('unselectable');
        } else {
            jcf.lib.removeClass(el, jcf.baseOptions.unselectableClass);
        }
    },
    queryBySelector: function (selector, scope) {
        if (typeof scope === 'string') {
            var result = [];
            var holders = this.getElementsBySelector(scope);
            for (var i = 0, contextNodes; i < holders.length; i++) {
                contextNodes = Array.prototype.slice.call(this.getElementsBySelector(selector, holders[i]));
                result = result.concat(contextNodes);
            }
            return result;
        } else {
            return this.getElementsBySelector(selector, scope);
        }
    },
    prevSibling: function (node) {
        while (node = node.previousSibling) if (node.nodeType == 1) break;
        return node;
    },
    nextSibling: function (node) {
        while (node = node.nextSibling) if (node.nodeType == 1) break;
        return node;
    },
    fireEvent: function (element, event) {
        if (element.dispatchEvent) {
            var evt = document.createEvent('HTMLEvents');
            evt.initEvent(event, true, true);
            return !element.dispatchEvent(evt);
        } else if (document.createEventObject) {
            var evt = document.createEventObject();
            return element.fireEvent('on' + event, evt);
        }
    },
    inherit: function (Child, Parent) {
        var F = function () { }
        F.prototype = Parent.prototype
        Child.prototype = new F()
        Child.prototype.constructor = Child
        Child.superclass = Parent.prototype
    },
    extend: function (obj) {
        for (var i = 1; i < arguments.length; i++) {
            for (var p in arguments[i]) {
                if (arguments[i].hasOwnProperty(p)) {
                    obj[p] = arguments[i][p];
                }
            }
        }
        return obj;
    },
    hasClass: function (obj, cname) {
        return (obj.className ? obj.className.match(new RegExp('(\\s|^)' + cname + '(\\s|$)')) : false);
    },
    addClass: function (obj, cname) {
        if (!this.hasClass(obj, cname)) obj.className += (!obj.className.length || obj.className.charAt(obj.className.length - 1) === ' ' ? '' : ' ') + cname;
    },
    removeClass: function (obj, cname) {
        if (this.hasClass(obj, cname)) obj.className = obj.className.replace(new RegExp('(\\s|^)' + cname + '(\\s|$)'), ' ').replace(/\s+$/, '');
    },
    toggleClass: function (obj, cname, condition) {
        if (condition) this.addClass(obj, cname); else this.removeClass(obj, cname);
    },
    createElement: function (tagName, options) {
        var el = document.createElement(tagName);
        for (var p in options) {
            if (options.hasOwnProperty(p)) {
                switch (p) {
                    case 'class': el.className = options[p]; break;
                    case 'html': el.innerHTML = options[p]; break;
                    case 'style': this.setStyles(el, options[p]); break;
                    default: el.setAttribute(p, options[p]);
                }
            }
        }
        return el;
    },
    setStyles: function (el, styles) {
        for (var p in styles) {
            if (styles.hasOwnProperty(p)) {
                switch (p) {
                    case 'float': el.style.cssFloat = styles[p]; break;
                    case 'opacity': el.style.filter = 'progid:DXImageTransform.Microsoft.Alpha(opacity=' + styles[p] * 100 + ')'; el.style.opacity = styles[p]; break;
                    default: el.style[p] = (typeof styles[p] === 'undefined' ? 0 : styles[p]) + (typeof styles[p] === 'number' ? 'px' : '');
                }
            }
        }
        return el;
    },
    getInnerWidth: function (el) {
        return el.offsetWidth - (parseInt(this.getStyle(el, 'paddingLeft')) || 0) - (parseInt(this.getStyle(el, 'paddingRight')) || 0);
    },
    getInnerHeight: function (el) {
        return el.offsetHeight - (parseInt(this.getStyle(el, 'paddingTop')) || 0) - (parseInt(this.getStyle(el, 'paddingBottom')) || 0);
    },
    getAllClasses: function (cname, prefix, skip) {
        if (!skip) skip = '';
        if (!prefix) prefix = '';
        return cname ? cname.replace(new RegExp('(\\s|^)' + skip + '(\\s|$)'), ' ').replace(/[\s]*([\S]+)+[\s]*/gi, prefix + "$1 ") : '';
    },
    getElementsBySelector: function (selector, scope) {
        if (typeof document.querySelectorAll === 'function') {
            return (scope || document).querySelectorAll(selector);
        }
        var selectors = selector.split(',');
        var resultList = [];
        for (var s = 0; s < selectors.length; s++) {
            var currentContext = [scope || document];
            var tokens = selectors[s].replace(/^\s+/, '').replace(/\s+$/, '').split(' ');
            for (var i = 0; i < tokens.length; i++) {
                token = tokens[i].replace(/^\s+/, '').replace(/\s+$/, '');
                if (token.indexOf('#') > -1) {
                    var bits = token.split('#'), tagName = bits[0], id = bits[1];
                    var element = document.getElementById(id);
                    if (tagName && element.nodeName.toLowerCase() != tagName) {
                        return [];
                    }
                    currentContext = [element];
                    continue;
                }
                if (token.indexOf('.') > -1) {
                    var bits = token.split('.'), tagName = bits[0] || '*', className = bits[1], found = [], foundCount = 0;
                    for (var h = 0; h < currentContext.length; h++) {
                        var elements;
                        if (tagName == '*') {
                            elements = currentContext[h].getElementsByTagName('*');
                        } else {
                            elements = currentContext[h].getElementsByTagName(tagName);
                        }
                        for (var j = 0; j < elements.length; j++) {
                            found[foundCount++] = elements[j];
                        }
                    }
                    currentContext = [];
                    var currentContextIndex = 0;
                    for (var k = 0; k < found.length; k++) {
                        if (found[k].className && found[k].className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'))) {
                            currentContext[currentContextIndex++] = found[k];
                        }
                    }
                    continue;
                }
                if (token.match(/^(\w*)\[(\w+)([=~\|\^\$\*]?)=?"?([^"]*)"?\]$/)) {
                    var tagName = RegExp.$1 || '*', attrName = RegExp.$2, attrOperator = RegExp.$3, attrValue = RegExp.$4;
                    if (attrName.toLowerCase() == 'for' && this.browser.msie && this.browser.version < 8) {
                        attrName = 'htmlFor';
                    }
                    var found = [], foundCount = 0;
                    for (var h = 0; h < currentContext.length; h++) {
                        var elements;
                        if (tagName == '*') {
                            elements = currentContext[h].getElementsByTagName('*');
                        } else {
                            elements = currentContext[h].getElementsByTagName(tagName);
                        }
                        for (var j = 0; elements[j]; j++) {
                            found[foundCount++] = elements[j];
                        }
                    }
                    currentContext = [];
                    var currentContextIndex = 0, checkFunction;
                    switch (attrOperator) {
                        case '=': checkFunction = function (e) { return (e.getAttribute(attrName) == attrValue) }; break;
                        case '~': checkFunction = function (e) { return (e.getAttribute(attrName).match(new RegExp('(\\s|^)' + attrValue + '(\\s|$)'))) }; break;
                        case '|': checkFunction = function (e) { return (e.getAttribute(attrName).match(new RegExp('^' + attrValue + '-?'))) }; break;
                        case '^': checkFunction = function (e) { return (e.getAttribute(attrName).indexOf(attrValue) == 0) }; break;
                        case '$': checkFunction = function (e) { return (e.getAttribute(attrName).lastIndexOf(attrValue) == e.getAttribute(attrName).length - attrValue.length) }; break;
                        case '*': checkFunction = function (e) { return (e.getAttribute(attrName).indexOf(attrValue) > -1) }; break;
                        default: checkFunction = function (e) { return e.getAttribute(attrName) };
                    }
                    currentContext = [];
                    var currentContextIndex = 0;
                    for (var k = 0; k < found.length; k++) {
                        if (checkFunction(found[k])) {
                            currentContext[currentContextIndex++] = found[k];
                        }
                    }
                    continue;
                }
                tagName = token;
                var found = [], foundCount = 0;
                for (var h = 0; h < currentContext.length; h++) {
                    var elements = currentContext[h].getElementsByTagName(tagName);
                    for (var j = 0; j < elements.length; j++) {
                        found[foundCount++] = elements[j];
                    }
                }
                currentContext = found;
            }
            resultList = [].concat(resultList, currentContext);
        }
        return resultList;
    },
    scrollSize: (function () {
        var content, hold, sizeBefore, sizeAfter;
        function buildSizer() {
            if (hold) removeSizer();
            content = document.createElement('div');
            hold = document.createElement('div');
            hold.style.cssText = 'position:absolute;overflow:hidden;width:100px;height:100px';
            hold.appendChild(content);
            document.body.appendChild(hold);
        }
        function removeSizer() {
            document.body.removeChild(hold);
            hold = null;
        }
        function calcSize(vertical) {
            buildSizer();
            content.style.cssText = 'height:' + (vertical ? '100%' : '200px');
            sizeBefore = (vertical ? content.offsetHeight : content.offsetWidth);
            hold.style.overflow = 'scroll'; content.innerHTML = 1;
            sizeAfter = (vertical ? content.offsetHeight : content.offsetWidth);
            if (vertical && hold.clientHeight) sizeAfter = hold.clientHeight;
            removeSizer();
            return sizeBefore - sizeAfter;
        }
        return {
            getWidth: function () {
                return calcSize(false);
            },
            getHeight: function () {
                return calcSize(true)
            }
        }
    }()),
    domReady: function (handler) {
        var called = false
        function ready() {
            if (called) return;
            called = true;
            handler();
        }
        if (document.addEventListener) {
            document.addEventListener("DOMContentLoaded", ready, false);
        } else if (document.attachEvent) {
            if (document.documentElement.doScroll && window == window.top) {
                function tryScroll() {
                    if (called) return
                    if (!document.body) return
                    try {
                        document.documentElement.doScroll("left")
                        ready()
                    } catch (e) {
                        setTimeout(tryScroll, 0)
                    }
                }
                tryScroll()
            }
            document.attachEvent("onreadystatechange", function () {
                if (document.readyState === "complete") {
                    ready()
                }
            })
        }
        if (window.addEventListener) window.addEventListener('load', ready, false)
        else if (window.attachEvent) window.attachEvent('onload', ready)
    },
    event: (function () {
        var guid = 0;
        function fixEvent(e) {
            e = e || window.event;
            if (e.isFixed) {
                return e;
            }
            e.isFixed = true;
            e.preventDefault = e.preventDefault || function () { this.returnValue = false }
            e.stopPropagation = e.stopPropagation || function () { this.cancelBubble = true }
            if (!e.target) {
                e.target = e.srcElement
            }
            if (!e.relatedTarget && e.fromElement) {
                e.relatedTarget = e.fromElement == e.target ? e.toElement : e.fromElement;
            }
            if (e.pageX == null && e.clientX != null) {
                var html = document.documentElement, body = document.body;
                e.pageX = e.clientX + (html && html.scrollLeft || body && body.scrollLeft || 0) - (html.clientLeft || 0);
                e.pageY = e.clientY + (html && html.scrollTop || body && body.scrollTop || 0) - (html.clientTop || 0);
            }
            if (!e.which && e.button) {
                e.which = e.button & 1 ? 1 : (e.button & 2 ? 3 : (e.button & 4 ? 2 : 0));
            }
            if (e.type === "DOMMouseScroll" || e.type === 'mousewheel') {
                e.mWheelDelta = 0;
                if (e.wheelDelta) {
                    e.mWheelDelta = e.wheelDelta / 120;
                } else if (e.detail) {
                    e.mWheelDelta = -e.detail / 3;
                }
            }
            return e;
        }
        function commonHandle(event, customScope) {
            event = fixEvent(event);
            var handlers = this.events[event.type];
            for (var g in handlers) {
                var handler = handlers[g];
                var ret = handler.call(customScope || this, event);
                if (ret === false) {
                    event.preventDefault()
                    event.stopPropagation()
                }
            }
        }
        var publicAPI = {
            add: function (elem, type, handler, forcedScope) {
                // handle multiple events
                if (type.indexOf(' ') > -1) {
                    var eventList = type.split(' ');
                    for (var i = 0; i < eventList.length; i++) {
                        publicAPI.add(elem, eventList[i], handler, forcedScope);
                    }
                    return;
                }

                if (elem.setInterval && (elem != window && !elem.frameElement)) {
                    elem = window;
                }
                if (!handler.guid) {
                    handler.guid = ++guid;
                }
                if (!elem.events) {
                    elem.events = {};
                    elem.handle = function (event) {
                        return commonHandle.call(elem, event);
                    }
                }
                if (!elem.events[type]) {
                    elem.events[type] = {};
                    if (elem.addEventListener) elem.addEventListener(type, elem.handle, false);
                    else if (elem.attachEvent) elem.attachEvent("on" + type, elem.handle);
                    if (type === 'mousewheel') {
                        publicAPI.add(elem, 'DOMMouseScroll', handler, forcedScope);
                    }
                }
                var fakeHandler = jcf.lib.bind(handler, forcedScope);
                fakeHandler.guid = handler.guid;
                elem.events[type][handler.guid] = forcedScope ? fakeHandler : handler;
            },
            remove: function (elem, type, handler) {
                // handle multiple events
                if (type.indexOf(' ') > -1) {
                    var eventList = type.split(' ');
                    for (var i = 0; i < eventList.length; i++) {
                        publicAPI.remove(elem, eventList[i], handler);
                    }
                    return;
                }

                var handlers = elem.events && elem.events[type];
                if (!handlers) return;
                delete handlers[handler.guid];
                for (var any in handlers) return;
                if (elem.removeEventListener) elem.removeEventListener(type, elem.handle, false);
                else if (elem.detachEvent) elem.detachEvent("on" + type, elem.handle);
                delete elem.events[type];
                for (var any in elem.events) return;
                try {
                    delete elem.handle;
                    delete elem.events;
                } catch (e) {
                    if (elem.removeAttribute) {
                        elem.removeAttribute("handle");
                        elem.removeAttribute("events");
                    }
                }
                if (type === 'mousewheel') {
                    publicAPI.remove(elem, 'DOMMouseScroll', handler);
                }
            }
        }
        return publicAPI;
    }())
}

// custom select module
jcf.addModule({
    name: 'select',
    selector: 'select',
    defaultOptions: {
        useNativeDropOnMobileDevices: true,
        hideDropOnScroll: true,
        showNativeDrop: false,
        handleDropPosition: false,
        selectDropPosition: 'bottom', // or 'top'
        wrapperClass: 'select-area',
        focusClass: 'select-focus',
        dropActiveClass: 'select-active',
        selectedClass: 'item-selected',
        currentSelectedClass: 'current-selected',
        disabledClass: 'select-disabled',
        valueSelector: 'span.center',
        optGroupClass: 'optgroup',
        openerSelector: 'a.select-opener',
        selectStructure: '<span class="left"></span><span class="center"></span><a class="select-opener"></a>',
        wrapperTag: 'span',
        classPrefix: 'select-',
        dropMaxHeight: 200,
        dropFlippedClass: 'select-options-flipped',
        dropHiddenClass: 'options-hidden',
        dropScrollableClass: 'options-overflow',
        dropClass: 'select-options',
        dropClassPrefix: 'drop-',
        dropStructure: '<div class="drop-holder"><div class="drop-list"></div></div>',
        dropSelector: 'div.drop-list'
    },
    checkElement: function (el) {
        return (!el.size && !el.multiple);
    },
    setupWrapper: function () {
        jcf.lib.addClass(this.fakeElement, this.options.wrapperClass);
        this.realElement.parentNode.insertBefore(this.fakeElement, this.realElement.nextSibling);
        this.fakeElement.innerHTML = this.options.selectStructure;
        this.fakeElement.style.width = (this.realElement.offsetWidth > 0 ? this.realElement.offsetWidth + 'px' : 'auto');

        // show native drop if specified in options
        if (this.options.useNativeDropOnMobileDevices && (jcf.isTouchDevice || jcf.isWinPhoneDevice)) {
            this.options.showNativeDrop = true;
        }
        if (this.options.showNativeDrop) {
            this.fakeElement.appendChild(this.realElement);
            jcf.lib.removeClass(this.realElement, this.options.hiddenClass);
            jcf.lib.setStyles(this.realElement, {
                top: 0,
                left: 0,
                margin: 0,
                padding: 0,
                opacity: 0,
                border: 'none',
                position: 'absolute',
                width: jcf.lib.getInnerWidth(this.fakeElement) - 1,
                height: jcf.lib.getInnerHeight(this.fakeElement) - 1
            });
            jcf.lib.event.add(this.realElement, jcf.eventPress, function () {
                this.realElement.title = '';
            }, this)
        }

        // create select body
        this.opener = jcf.lib.queryBySelector(this.options.openerSelector, this.fakeElement)[0];
        this.valueText = jcf.lib.queryBySelector(this.options.valueSelector, this.fakeElement)[0];
        jcf.lib.disableTextSelection(this.valueText);
        this.opener.jcf = this;

        if (!this.options.showNativeDrop) {
            this.createDropdown();
            this.refreshState();
            this.onControlReady(this);
            this.hideDropdown();
        } else {
            this.refreshState();
        }
        this.addEvents();
    },
    addEvents: function () {
        if (this.options.showNativeDrop) {
            jcf.lib.event.add(this.realElement, 'click', this.onChange, this);
        } else {
            jcf.lib.event.add(this.fakeElement, 'click', this.toggleDropdown, this);
        }
        jcf.lib.event.add(this.realElement, 'change', this.onChange, this);
    },
    onFakeClick: function () {
        // do nothing (drop toggles by toggleDropdown method)
    },
    onFocus: function () {
        jcf.modules[this.name].superclass.onFocus.apply(this, arguments);
        if (!this.options.showNativeDrop) {
            // Mac Safari Fix
            if (jcf.lib.browser.safariMac) {
                this.realElement.setAttribute('size', '2');
            }
            jcf.lib.event.add(this.realElement, 'keydown', this.onKeyDown, this);
            if (jcf.activeControl && jcf.activeControl != this) {
                jcf.activeControl.hideDropdown();
                jcf.activeControl = this;
            }
        }
    },
    onBlur: function () {
        if (!this.options.showNativeDrop) {
            // Mac Safari Fix
            if (jcf.lib.browser.safariMac) {
                this.realElement.removeAttribute('size');
            }
            if (!this.isActiveDrop() || !this.isOverDrop()) {
                jcf.modules[this.name].superclass.onBlur.apply(this);
                if (jcf.activeControl === this) jcf.activeControl = null;
                if (!jcf.isTouchDevice) {
                    this.hideDropdown();
                }
            }
            jcf.lib.event.remove(this.realElement, 'keydown', this.onKeyDown);
        } else {
            jcf.modules[this.name].superclass.onBlur.apply(this);
        }
    },
    onChange: function () {
        this.refreshState();
    },
    onKeyDown: function (e) {
        this.dropOpened = true;
        jcf.tmpFlag = true;
        setTimeout(function () { jcf.tmpFlag = false }, 100);
        var context = this;
        context.keyboardFix = true;
        setTimeout(function () {
            context.refreshState();
        }, 10);
        if (e.keyCode == 13) {
            context.toggleDropdown.apply(context);
            return false;
        }
    },
    onResizeWindow: function (e) {
        if (this.isActiveDrop()) {
            this.hideDropdown();
        }
    },
    onScrollWindow: function (e) {
        if (this.options.hideDropOnScroll) {
            this.hideDropdown();
        } else if (this.isActiveDrop()) {
            this.positionDropdown();
        }
    },
    onOptionClick: function (e) {
        var opener = e.target && e.target.tagName && e.target.tagName.toLowerCase() == 'li' ? e.target : jcf.lib.getParent(e.target, 'li');
        if (opener) {
            this.dropOpened = true;
            this.realElement.selectedIndex = parseInt(opener.getAttribute('rel'));
            if (jcf.isTouchDevice) {
                this.onFocus();
            } else {
                this.realElement.focus();
            }
            this.refreshState();
            this.hideDropdown();
            jcf.lib.fireEvent(this.realElement, 'change');
        }
        return false;
    },
    onClickOutside: function (e) {
        if (jcf.tmpFlag) {
            jcf.tmpFlag = false;
            return;
        }
        if (!jcf.lib.isParent(this.fakeElement, e.target) && !jcf.lib.isParent(this.selectDrop, e.target)) {
            this.hideDropdown();
        }
    },
    onDropHover: function (e) {
        if (!this.keyboardFix) {
            this.hoverFlag = true;
            var opener = e.target && e.target.tagName && e.target.tagName.toLowerCase() == 'li' ? e.target : jcf.lib.getParent(e.target, 'li');
            if (opener) {
                this.realElement.selectedIndex = parseInt(opener.getAttribute('rel'));
                this.refreshSelectedClass(parseInt(opener.getAttribute('rel')));
            }
        } else {
            this.keyboardFix = false;
        }
    },
    onDropLeave: function () {
        this.hoverFlag = false;
    },
    isActiveDrop: function () {
        return !jcf.lib.hasClass(this.selectDrop, this.options.dropHiddenClass);
    },
    isOverDrop: function () {
        return this.hoverFlag;
    },
    createDropdown: function () {
        // remove old dropdown if exists
        if (this.selectDrop && this.selectDrop.parentNode) {
            this.selectDrop.parentNode.removeChild(this.selectDrop);
        }

        // create dropdown holder
        this.selectDrop = document.createElement('div');
        this.selectDrop.className = this.options.dropClass;
        this.selectDrop.innerHTML = this.options.dropStructure;
        jcf.lib.setStyles(this.selectDrop, { position: 'absolute' });
        this.selectList = jcf.lib.queryBySelector(this.options.dropSelector, this.selectDrop)[0];
        jcf.lib.addClass(this.selectDrop, this.options.dropHiddenClass);
        document.body.appendChild(this.selectDrop);
        this.selectDrop.jcf = this;
        jcf.lib.event.add(this.selectDrop, 'click', this.onOptionClick, this);
        jcf.lib.event.add(this.selectDrop, 'mouseover', this.onDropHover, this);
        jcf.lib.event.add(this.selectDrop, 'mouseout', this.onDropLeave, this);
        this.buildDropdown();
    },
    buildDropdown: function () {
        // build select options / optgroups
        this.buildDropdownOptions();

        // position and resize dropdown
        this.positionDropdown();

        // cut dropdown if height exceedes
        this.buildDropdownScroll();
    },
    buildDropdownOptions: function () {
        this.resStructure = '';
        this.optNum = 0;
        for (var i = 0; i < this.realElement.children.length; i++) {
            this.resStructure += this.buildElement(this.realElement.children[i], i) + '\n';
        }
        this.selectList.innerHTML = this.resStructure;
    },
    buildDropdownScroll: function () {
        jcf.lib.addClass(this.selectDrop, jcf.lib.getAllClasses(this.realElement.className, this.options.dropClassPrefix, jcf.baseOptions.hiddenClass));
        if (this.options.dropMaxHeight) {
            if (this.selectDrop.offsetHeight > this.options.dropMaxHeight) {
                this.selectList.style.height = this.options.dropMaxHeight + 'px';
                this.selectList.style.overflow = 'auto';
                this.selectList.style.overflowX = 'hidden';
                jcf.lib.addClass(this.selectDrop, this.options.dropScrollableClass);
            }
        }
    },
    parseOptionTitle: function (optTitle) {
        return (typeof optTitle === 'string' && /\.(jpg|gif|png|bmp|jpeg)(.*)?$/i.test(optTitle)) ? optTitle : '';
    },
    buildElement: function (obj, index) {
        // build option
        var res = '', optImage;
        if (obj.tagName.toLowerCase() == 'option') {
            if (!jcf.lib.prevSibling(obj) || jcf.lib.prevSibling(obj).tagName.toLowerCase() != 'option') {
                res += '<ul>';
            }

            optImage = this.parseOptionTitle(obj.title);
            res += '<li rel="' + (this.optNum++) + '" class="' + (obj.className ? obj.className + ' ' : '') + (index % 2 ? 'option-even ' : '') + 'jcfcalc"><a href="#">' + (optImage ? '<img src="' + optImage + '" alt="" />' : '') + '<span>' + obj.innerHTML + '</span></a></li>';
            if (!jcf.lib.nextSibling(obj) || jcf.lib.nextSibling(obj).tagName.toLowerCase() != 'option') {
                res += '</ul>';
            }
            return res;
        }
            // build option group with options
        else if (obj.tagName.toLowerCase() == 'optgroup' && obj.label) {
            res += '<div class="' + this.options.optGroupClass + '">';
            res += '<strong class="jcfcalc"><em>' + (obj.label) + '</em></strong>';
            for (var i = 0; i < obj.children.length; i++) {
                res += this.buildElement(obj.children[i], i);
            }
            res += '</div>';
            return res;
        }
    },
    positionDropdown: function () {
        var ofs = jcf.lib.getOffset(this.fakeElement), selectAreaHeight = this.fakeElement.offsetHeight, selectDropHeight = this.selectDrop.offsetHeight;
        var fitInTop = ofs.top - selectDropHeight >= jcf.lib.getScrollTop() && jcf.lib.getScrollTop() + jcf.lib.getWindowHeight() < ofs.top + selectAreaHeight + selectDropHeight;


        if ((this.options.handleDropPosition && fitInTop) || this.options.selectDropPosition === 'top') {
            this.selectDrop.style.top = (ofs.top - selectDropHeight) + 'px';
            jcf.lib.addClass(this.selectDrop, this.options.dropFlippedClass);
            jcf.lib.addClass(this.fakeElement, this.options.dropFlippedClass);
        } else {
            this.selectDrop.style.top = (ofs.top + selectAreaHeight) + 'px';
            jcf.lib.removeClass(this.selectDrop, this.options.dropFlippedClass);
            jcf.lib.removeClass(this.fakeElement, this.options.dropFlippedClass);
        }
        this.selectDrop.style.left = ofs.left + 'px';
        this.selectDrop.style.width = this.fakeElement.offsetWidth + 'px';
    },
    showDropdown: function () {
        document.body.appendChild(this.selectDrop);
        jcf.lib.removeClass(this.selectDrop, this.options.dropHiddenClass);
        jcf.lib.addClass(this.fakeElement, this.options.dropActiveClass);
        this.positionDropdown();

        // highlight current active item
        var activeItem = this.getFakeActiveOption();
        this.removeClassFromItems(this.options.currentSelectedClass);
        jcf.lib.addClass(activeItem, this.options.currentSelectedClass);

        // show current dropdown
        jcf.lib.event.add(window, 'resize', this.onResizeWindow, this);
        jcf.lib.event.add(window, 'scroll', this.onScrollWindow, this);
        jcf.lib.event.add(document, jcf.eventPress, this.onClickOutside, this);
        this.positionDropdown();
    },
    hideDropdown: function () {
        if (this.selectDrop.parentNode) {
            this.selectDrop.parentNode.removeChild(this.selectDrop);
        }
        if (typeof this.origSelectedIndex === 'number') {
            this.realElement.selectedIndex = this.origSelectedIndex;
        }
        jcf.lib.removeClass(this.fakeElement, this.options.dropActiveClass);
        jcf.lib.addClass(this.selectDrop, this.options.dropHiddenClass);
        jcf.lib.event.remove(window, 'resize', this.onResizeWindow);
        jcf.lib.event.remove(window, 'scroll', this.onScrollWindow);
        jcf.lib.event.remove(document.documentElement, jcf.eventPress, this.onClickOutside);
        if (jcf.isTouchDevice) {
            this.onBlur();
        }
    },
    toggleDropdown: function () {
        if (!this.realElement.disabled && this.realElement.options.length) {
            if (jcf.isTouchDevice) {
                this.onFocus();
            } else {
                this.realElement.focus();
            }
            if (this.isActiveDrop()) {
                this.hideDropdown();
            } else {
                this.showDropdown();
            }
            this.refreshState();
        }
    },
    scrollToItem: function () {
        if (this.isActiveDrop()) {
            var dropHeight = this.selectList.offsetHeight;
            var offsetTop = this.calcOptionOffset(this.getFakeActiveOption());
            var sTop = this.selectList.scrollTop;
            var oHeight = this.getFakeActiveOption().offsetHeight;
            //offsetTop+=sTop;

            if (offsetTop >= sTop + dropHeight) {
                this.selectList.scrollTop = offsetTop - dropHeight + oHeight;
            } else if (offsetTop < sTop) {
                this.selectList.scrollTop = offsetTop;
            }
        }
    },
    getFakeActiveOption: function (c) {
        return jcf.lib.queryBySelector('li[rel="' + (typeof c === 'number' ? c : this.realElement.selectedIndex) + '"]', this.selectList)[0];
    },
    calcOptionOffset: function (fake) {
        var h = 0;
        var els = jcf.lib.queryBySelector('.jcfcalc', this.selectList);
        for (var i = 0; i < els.length; i++) {
            if (els[i] == fake) break;
            h += els[i].offsetHeight;
        }
        return h;
    },
    childrenHasItem: function (hold, item) {
        var items = hold.getElementsByTagName('*');
        for (i = 0; i < items.length; i++) {
            if (items[i] == item) return true;
        }
        return false;
    },
    removeClassFromItems: function (className) {
        var children = jcf.lib.queryBySelector('li', this.selectList);
        for (var i = children.length - 1; i >= 0; i--) {
            jcf.lib.removeClass(children[i], className);
        }
    },
    setSelectedClass: function (c) {
        var activeOption = this.getFakeActiveOption(c);
        if (activeOption) {
            jcf.lib.addClass(activeOption, this.options.selectedClass);
        }
    },
    refreshSelectedClass: function (c) {
        if (!this.options.showNativeDrop) {
            this.removeClassFromItems(this.options.selectedClass);
            this.setSelectedClass(c);
        }
        if (this.realElement.disabled) {
            jcf.lib.addClass(this.fakeElement, this.options.disabledClass);
            if (this.labelFor) {
                jcf.lib.addClass(this.labelFor, this.options.labelDisabledClass);
            }
        } else {
            jcf.lib.removeClass(this.fakeElement, this.options.disabledClass);
            if (this.labelFor) {
                jcf.lib.removeClass(this.labelFor, this.options.labelDisabledClass);
            }
        }
    },
    refreshSelectedText: function () {
        if (!this.dropOpened && this.realElement.title) {
            this.valueText.innerHTML = this.realElement.title;
        } else {
            var activeOption = this.realElement.options[this.realElement.selectedIndex];
            if (activeOption) {
                if (activeOption.title) {
                    var optImage = this.parseOptionTitle(this.realElement.options[this.realElement.selectedIndex].title);
                    this.valueText.innerHTML = (optImage ? '<img src="' + optImage + '" alt="" />' : '') + this.realElement.options[this.realElement.selectedIndex].innerHTML;
                } else {
                    this.valueText.innerHTML = this.realElement.options[this.realElement.selectedIndex].innerHTML;
                }
            }
        }

        var selectedOption = this.realElement.options[this.realElement.selectedIndex];
        if (selectedOption && selectedOption.className) {
            this.fakeElement.setAttribute('data-option-class', jcf.lib.getAllClasses(selectedOption.className, 'option-').replace(/^\s+|\s+$/g, ''));
        } else {
            this.fakeElement.removeAttribute('data-option-class');
        }
    },
    refreshState: function () {
        this.origSelectedIndex = this.realElement.selectedIndex;
        this.refreshSelectedClass();
        this.refreshSelectedText();
        if (!this.options.showNativeDrop) {
            this.positionDropdown();
            if (this.selectDrop.offsetWidth) {
                this.scrollToItem();
            }
        }
    }
});


// navigation accesibility module
function TouchNav(opt) {
    this.options = {
        hoverClass: 'hover',
        menuItems: 'li',
        menuOpener: 'a',
        menuDrop: 'ul',
        navBlock: null
    };
    for (var p in opt) {
        if (opt.hasOwnProperty(p)) {
            this.options[p] = opt[p];
        }
    }
    this.init();
}
TouchNav.isActiveOn = function (elem) {
    return elem && elem.touchNavActive;
};
TouchNav.prototype = {
    init: function () {
        if (typeof this.options.navBlock === 'string') {
            this.menu = document.getElementById(this.options.navBlock);
        } else if (typeof this.options.navBlock === 'object') {
            this.menu = this.options.navBlock;
        }
        if (this.menu) {
            this.addEvents();
        }
    },
    addEvents: function () {
        // attach event handlers
        var self = this;
        this.menuItems = lib.queryElementsBySelector(this.options.menuItems, this.menu);

        for (var i = 0; i < this.menuItems.length; i++) {
            (function (i) {
                var item = self.menuItems[i],
					currentDrop = lib.queryElementsBySelector(self.options.menuDrop, item)[0],
					currentOpener = lib.queryElementsBySelector(self.options.menuOpener, item)[0];

                // only for touch input devices
                if ((self.isTouchDevice || navigator.msPointerEnabled) && currentDrop && currentOpener) {
                    jQuery(currentOpener).bind('click.TouchNav', lib.bind(self.clickHandler, self));
                    jQuery(currentOpener).bind(navigator.msPointerEnabled ? 'MSPointerDown.TouchNav' : 'touchstart.TouchNav', function (e) {
                        if (navigator.msPointerEnabled && e.pointerType !== e.MSPOINTER_TYPE_TOUCH) return;
                        self.touchFlag = true;
                        self.currentItem = item;
                        self.currentLink = currentOpener;
                        self.pressHandler.apply(self, arguments);
                    });
                }
                // for desktop computers and touch devices
                jQuery(item).bind('mouseover.TouchNav', function () {
                    if (!self.touchFlag) {
                        self.currentItem = item;
                        self.mouseoverHandler();
                    }
                });
                jQuery(item).bind('mouseout.TouchNav', function () {
                    if (!self.touchFlag) {
                        self.currentItem = item;
                        self.mouseoutHandler();
                    }
                });

                item.touchNavActive = true;
            })(i);
        }

        // hide dropdowns when clicking outside navigation
        if (this.isTouchDevice || navigator.msPointerEnabled) {
            jQuery(document).bind(navigator.msPointerEnabled ? 'MSPointerDown.TouchNav' : 'touchstart.TouchNav', lib.bind(this.clickOutsideHandler, this));
        }
    },
    mouseoverHandler: function () {
        lib.addClass(this.currentItem, this.options.hoverClass);
        jQuery(this.currentItem).trigger('itemhover');
    },
    mouseoutHandler: function () {
        lib.removeClass(this.currentItem, this.options.hoverClass);
        jQuery(this.currentItem).trigger('itemleave');
    },
    hideActiveDropdown: function () {
        for (var i = 0; i < this.menuItems.length; i++) {
            if (lib.hasClass(this.menuItems[i], this.options.hoverClass)) {
                lib.removeClass(this.menuItems[i], this.options.hoverClass);
                jQuery(this.menuItems[i]).trigger('itemleave');
            }
        }
        this.activeParent = null;
    },
    pressHandler: function (e) {
        // hide previous drop (if active)
        if (this.currentItem !== this.activeParent) {
            if (this.activeParent && this.currentItem.parentNode === this.activeParent.parentNode) {
                lib.removeClass(this.activeParent, this.options.hoverClass);
            } else if (!this.isParent(this.activeParent, this.currentLink)) {
                this.hideActiveDropdown();
            }
        }
        // handle current drop
        this.activeParent = this.currentItem;
        if (lib.hasClass(this.currentItem, this.options.hoverClass)) {
            this.preventCurrentClick = false;
        } else {
            e.preventDefault();
            this.preventCurrentClick = true;
            lib.addClass(this.currentItem, this.options.hoverClass);
            jQuery(this.currentItem).trigger('itemhover');
        }
    },
    clickHandler: function (e) {
        // prevent first click on link
        if (this.preventCurrentClick || typeof this.preventCurrentClick === 'undefined') {
            e.preventDefault();
        }
    },
    clickOutsideHandler: function (event) {
        var e = event.changedTouches ? event.changedTouches[0] : event;
        if (this.activeParent && !this.isParent(this.menu, e.target)) {
            this.hideActiveDropdown();
            this.touchFlag = false;
        }
    },
    isParent: function (parent, child) {
        while (child.parentNode) {
            if (child.parentNode == parent) {
                return true;
            }
            child = child.parentNode;
        }
        return false;
    },
    isTouchPointerEvent: function (e) {
        return (e.type.indexOf('touch') > -1) ||
				(navigator.pointerEnabled && e.pointerType === 'touch') ||
				(navigator.msPointerEnabled && e.pointerType == e.MSPOINTER_TYPE_TOUCH);
    },
    isPointerDevice: (function () {
        return !!(navigator.pointerEnabled || navigator.msPointerEnabled);
    }()),
    isTouchDevice: (function () {
        return !!(('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch);
    }()),
    destroy: function () {
        var self = this;
        for (var i = 0; i < this.menuItems.length; i++) {
            (function (i) {
                var item = self.menuItems[i];
                var currentDrop = lib.queryElementsBySelector(self.options.menuDrop, item)[0];
                var currentOpener = lib.queryElementsBySelector(self.options.menuOpener, item)[0];
                if ((self.isTouchDevice || navigator.msPointerEnabled) && currentDrop && currentOpener) {
                    jQuery(currentOpener).unbind('click.TouchNav');
                    jQuery(currentOpener).unbind(navigator.msPointerEnabled ? 'MSPointerDown.TouchNav' : 'touchstart.TouchNav');
                }
                jQuery(item).unbind('mouseover.TouchNav');
                jQuery(item).unbind('mouseout.TouchNav');
            })(i);
        }
        if (this.isTouchDevice || navigator.msPointerEnabled) {
            jQuery(document).unbind(navigator.msPointerEnabled ? 'MSPointerDown' : 'touchstart');
        }
    }
};

/*
 * Utility module
 */
lib = {
    hasClass: function (el, cls) {
        return el && el.className ? el.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)')) : false;
    },
    addClass: function (el, cls) {
        if (el && !this.hasClass(el, cls)) el.className += " " + cls;
    },
    removeClass: function (el, cls) {
        if (el && this.hasClass(el, cls)) { el.className = el.className.replace(new RegExp('(\\s|^)' + cls + '(\\s|$)'), ' '); }
    },
    extend: function (obj) {
        for (var i = 1; i < arguments.length; i++) {
            for (var p in arguments[i]) {
                if (arguments[i].hasOwnProperty(p)) {
                    obj[p] = arguments[i][p];
                }
            }
        }
        return obj;
    },
    each: function (obj, callback) {
        var property, len;
        if (typeof obj.length === 'number') {
            for (property = 0, len = obj.length; property < len; property++) {
                if (callback.call(obj[property], property, obj[property]) === false) {
                    break;
                }
            }
        } else {
            for (property in obj) {
                if (obj.hasOwnProperty(property)) {
                    if (callback.call(obj[property], property, obj[property]) === false) {
                        break;
                    }
                }
            }
        }
    },
    event: (function () {
        var fixEvent = function (e) {
            e = e || window.event;
            if (e.isFixed) return e; else e.isFixed = true;
            if (!e.target) e.target = e.srcElement;
            e.preventDefault = e.preventDefault || function () { this.returnValue = false; };
            e.stopPropagation = e.stopPropagation || function () { this.cancelBubble = true; };
            return e;
        };
        return {
            add: function (elem, event, handler) {
                if (!elem.events) {
                    elem.events = {};
                    elem.handle = function (e) {
                        var ret, handlers = elem.events[e.type];
                        e = fixEvent(e);
                        for (var i = 0, len = handlers.length; i < len; i++) {
                            if (handlers[i]) {
                                ret = handlers[i].call(elem, e);
                                if (ret === false) {
                                    e.preventDefault();
                                    e.stopPropagation();
                                }
                            }
                        }
                    };
                }
                if (!elem.events[event]) {
                    elem.events[event] = [];
                    if (elem.addEventListener) elem.addEventListener(event, elem.handle, false);
                    else if (elem.attachEvent) elem.attachEvent('on' + event, elem.handle);
                }
                elem.events[event].push(handler);
            },
            remove: function (elem, event, handler) {
                var handlers = elem.events[event];
                for (var i = handlers.length - 1; i >= 0; i--) {
                    if (handlers[i] === handler) {
                        handlers.splice(i, 1);
                    }
                }
                if (!handlers.length) {
                    delete elem.events[event];
                    if (elem.removeEventListener) elem.removeEventListener(event, elem.handle, false);
                    else if (elem.detachEvent) elem.detachEvent('on' + event, elem.handle);
                }
            }
        };
    }()),
    queryElementsBySelector: function (selector, scope) {
        scope = scope || document;
        if (!selector) return [];
        if (selector === '>*') return scope.children;
        if (typeof document.querySelectorAll === 'function') {
            return scope.querySelectorAll(selector);
        }
        var selectors = selector.split(',');
        var resultList = [];
        for (var s = 0; s < selectors.length; s++) {
            var currentContext = [scope || document];
            var tokens = selectors[s].replace(/^\s+/, '').replace(/\s+$/, '').split(' ');
            for (var i = 0; i < tokens.length; i++) {
                token = tokens[i].replace(/^\s+/, '').replace(/\s+$/, '');
                if (token.indexOf('#') > -1) {
                    var bits = token.split('#'), tagName = bits[0], id = bits[1];
                    var element = document.getElementById(id);
                    if (element && tagName && element.nodeName.toLowerCase() != tagName) {
                        return [];
                    }
                    currentContext = element ? [element] : [];
                    continue;
                }
                if (token.indexOf('.') > -1) {
                    var bits = token.split('.'), tagName = bits[0] || '*', className = bits[1], found = [], foundCount = 0;
                    for (var h = 0; h < currentContext.length; h++) {
                        var elements;
                        if (tagName == '*') {
                            elements = currentContext[h].getElementsByTagName('*');
                        } else {
                            elements = currentContext[h].getElementsByTagName(tagName);
                        }
                        for (var j = 0; j < elements.length; j++) {
                            found[foundCount++] = elements[j];
                        }
                    }
                    currentContext = [];
                    var currentContextIndex = 0;
                    for (var k = 0; k < found.length; k++) {
                        if (found[k].className && found[k].className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'))) {
                            currentContext[currentContextIndex++] = found[k];
                        }
                    }
                    continue;
                }
                if (token.match(/^(\w*)\[(\w+)([=~\|\^\$\*]?)=?"?([^\]"]*)"?\]$/)) {
                    var tagName = RegExp.$1 || '*', attrName = RegExp.$2, attrOperator = RegExp.$3, attrValue = RegExp.$4;
                    if (attrName.toLowerCase() == 'for' && this.browser.msie && this.browser.version < 8) {
                        attrName = 'htmlFor';
                    }
                    var found = [], foundCount = 0;
                    for (var h = 0; h < currentContext.length; h++) {
                        var elements;
                        if (tagName == '*') {
                            elements = currentContext[h].getElementsByTagName('*');
                        } else {
                            elements = currentContext[h].getElementsByTagName(tagName);
                        }
                        for (var j = 0; elements[j]; j++) {
                            found[foundCount++] = elements[j];
                        }
                    }
                    currentContext = [];
                    var currentContextIndex = 0, checkFunction;
                    switch (attrOperator) {
                        case '=': checkFunction = function (e) { return (e.getAttribute(attrName) == attrValue) }; break;
                        case '~': checkFunction = function (e) { return (e.getAttribute(attrName).match(new RegExp('(\\s|^)' + attrValue + '(\\s|$)'))) }; break;
                        case '|': checkFunction = function (e) { return (e.getAttribute(attrName).match(new RegExp('^' + attrValue + '-?'))) }; break;
                        case '^': checkFunction = function (e) { return (e.getAttribute(attrName).indexOf(attrValue) == 0) }; break;
                        case '$': checkFunction = function (e) { return (e.getAttribute(attrName).lastIndexOf(attrValue) == e.getAttribute(attrName).length - attrValue.length) }; break;
                        case '*': checkFunction = function (e) { return (e.getAttribute(attrName).indexOf(attrValue) > -1) }; break;
                        default: checkFunction = function (e) { return e.getAttribute(attrName) };
                    }
                    currentContext = [];
                    var currentContextIndex = 0;
                    for (var k = 0; k < found.length; k++) {
                        if (checkFunction(found[k])) {
                            currentContext[currentContextIndex++] = found[k];
                        }
                    }
                    continue;
                }
                tagName = token;
                var found = [], foundCount = 0;
                for (var h = 0; h < currentContext.length; h++) {
                    var elements = currentContext[h].getElementsByTagName(tagName);
                    for (var j = 0; j < elements.length; j++) {
                        found[foundCount++] = elements[j];
                    }
                }
                currentContext = found;
            }
            resultList = [].concat(resultList, currentContext);
        }
        return resultList;
    },
    trim: function (str) {
        return str.replace(/^\s+/, '').replace(/\s+$/, '');
    },
    bind: function (f, scope, forceArgs) {
        return function () { return f.apply(scope, typeof forceArgs !== 'undefined' ? [forceArgs] : arguments); };
    }
};

/*
 * jQuery Accordion plugin
 */
; (function ($) {
    $.fn.slideAccordion = function (opt) {
        // default options
        var options = $.extend({
            addClassBeforeAnimation: false,
            allowClickWhenExpanded: false,
            activeClass: 'active',
            opener: '.opener',
            slider: '.slide',
            animSpeed: 300,
            collapsible: true,
            event: 'click'
        }, opt);

        return this.each(function () {
            // options
            var accordion = $(this);
            var items = accordion.find(':has(' + options.slider + ')');

            items.each(function () {
                var item = $(this);
                var opener = item.find(options.opener);
                var slider = item.find(options.slider);
                opener.bind(options.event, function (e) {
                    if (!slider.is(':animated')) {
                        if (item.hasClass(options.activeClass)) {
                            if (options.allowClickWhenExpanded) {
                                return;
                            } else if (options.collapsible) {
                                slider.slideUp(options.animSpeed, function () {
                                    hideSlide(slider);
                                    item.removeClass(options.activeClass);
                                });
                            }
                        } else {
                            // show active
                            var levelItems = item.siblings('.' + options.activeClass);
                            var sliderElements = levelItems.find(options.slider);
                            item.addClass(options.activeClass);
                            showSlide(slider).hide().slideDown(options.animSpeed);

                            // collapse others
                            sliderElements.slideUp(options.animSpeed, function () {
                                levelItems.removeClass(options.activeClass);
                                hideSlide(sliderElements);
                            });
                        }
                    }
                    e.preventDefault();
                });
                if (item.hasClass(options.activeClass)) showSlide(slider); else hideSlide(slider);
            });


            accordion.bind('destroy.accordion', function () {
                items.find(options.opener).unbind(options.event);
                items.find(options.slider).removeAttr('style');
            });
        });
    };

    // accordion slide visibility
    var showSlide = function (slide) {
        return slide.css({ position: '', top: '', left: '', width: '' });
    };
    var hideSlide = function (slide) {
        return slide.show().css({ position: 'absolute', top: -9999, left: -9999, width: slide.width() });
    };
}(jQuery));

/*
 * jQuery SameHeight plugin
 */
; (function ($) {
    $.fn.sameHeight = function (opt) {
        var options = $.extend({
            skipClass: 'same-height-ignore',
            leftEdgeClass: 'same-height-left',
            rightEdgeClass: 'same-height-right',
            elements: '>*',
            flexible: false,
            multiLine: false,
            useMinHeight: false,
            biggestHeight: false
        }, opt);
        return this.each(function () {
            var holder = $(this), postResizeTimer, ignoreResize;
            var elements = holder.find(options.elements).not('.' + options.skipClass);
            if (!elements.length) return;

            // resize handler
            function doResize() {
                elements.css(options.useMinHeight && supportMinHeight ? 'minHeight' : 'height', '');
                if (options.multiLine) {
                    // resize elements row by row
                    resizeElementsByRows(elements, options);
                } else {
                    // resize elements by holder
                    resizeElements(elements, holder, options);
                }
            }
            doResize();

            // handle flexible layout / font resize
            var delayedResizeHandler = function () {
                if (!ignoreResize) {
                    ignoreResize = true;
                    doResize();
                    clearTimeout(postResizeTimer);
                    postResizeTimer = setTimeout(function () {
                        doResize();
                        setTimeout(function () {
                            ignoreResize = false;
                        }, 10);
                    }, 100);
                }
            };

            // handle flexible/responsive layout
            if (options.flexible) {
                $(window).bind('resize orientationchange fontresize', delayedResizeHandler);
            }

            // handle complete page load including images and fonts
            $(window).bind('load', delayedResizeHandler);
        });
    };

    // detect css min-height support
    var supportMinHeight = typeof document.documentElement.style.maxHeight !== 'undefined';

    // get elements by rows
    function resizeElementsByRows(boxes, options) {
        var currentRow = $(), maxHeight, maxCalcHeight = 0, firstOffset = boxes.eq(0).offset().top;
        boxes.each(function (ind) {
            var curItem = $(this);
            if (curItem.offset().top === firstOffset) {
                currentRow = currentRow.add(this);
            } else {
                maxHeight = getMaxHeight(currentRow);
                maxCalcHeight = Math.max(maxCalcHeight, resizeElements(currentRow, maxHeight, options));
                currentRow = curItem;
                firstOffset = curItem.offset().top;
            }
        });
        if (currentRow.length) {
            maxHeight = getMaxHeight(currentRow);
            maxCalcHeight = Math.max(maxCalcHeight, resizeElements(currentRow, maxHeight, options));
        }
        if (options.biggestHeight) {
            boxes.css(options.useMinHeight && supportMinHeight ? 'minHeight' : 'height', maxCalcHeight);
        }
    }

    // calculate max element height
    function getMaxHeight(boxes) {
        var maxHeight = 0;
        boxes.each(function () {
            maxHeight = Math.max(maxHeight, $(this).outerHeight());
        });
        return maxHeight;
    }

    // resize helper function
    function resizeElements(boxes, parent, options) {
        var calcHeight;
        var parentHeight = typeof parent === 'number' ? parent : parent.height();
        boxes.removeClass(options.leftEdgeClass).removeClass(options.rightEdgeClass).each(function (i) {
            var element = $(this);
            var depthDiffHeight = 0;
            var isBorderBox = element.css('boxSizing') === 'border-box' || element.css('-moz-box-sizing') === 'border-box' || '-webkit-box-sizing' === 'border-box';

            if (typeof parent !== 'number') {
                element.parents().each(function () {
                    var tmpParent = $(this);
                    if (parent.is(this)) {
                        return false;
                    } else {
                        depthDiffHeight += tmpParent.outerHeight() - tmpParent.height();
                    }
                });
            }
            calcHeight = parentHeight - depthDiffHeight;
            calcHeight -= isBorderBox ? 0 : element.outerHeight() - element.height();

            if (calcHeight > 0) {
                element.css(options.useMinHeight && supportMinHeight ? 'minHeight' : 'height', calcHeight);
            }
        });
        boxes.filter(':first').addClass(options.leftEdgeClass);
        boxes.filter(':last').addClass(options.rightEdgeClass);
        return calcHeight;
    }
}(jQuery));

/*
 * jQuery FontResize Event
 */
jQuery.onFontResize = (function ($) {
    $(function () {
        var randomID = 'font-resize-frame-' + Math.floor(Math.random() * 1000);
        var resizeFrame = $('<iframe>').attr('id', randomID).addClass('font-resize-helper');

        // required styles
        resizeFrame.css({
            width: '100em',
            height: '10px',
            position: 'absolute',
            borderWidth: 0,
            top: '-9999px',
            left: '-9999px'
        }).appendTo('body');

        // use native IE resize event if possible
        if (window.attachEvent && !window.addEventListener) {
            resizeFrame.bind('resize', function () {
                $.onFontResize.trigger(resizeFrame[0].offsetWidth / 100);
            });
        }
            // use script inside the iframe to detect resize for other browsers
        else {
            var doc = resizeFrame[0].contentWindow.document;
            doc.open();
            doc.write('<scri' + 'pt>window.onload = function(){var em = parent.jQuery("#' + randomID + '")[0];window.onresize = function(){if(parent.jQuery.onFontResize){parent.jQuery.onFontResize.trigger(em.offsetWidth / 100);}}};</scri' + 'pt>');
            doc.close();
        }
        jQuery.onFontResize.initialSize = resizeFrame[0].offsetWidth / 100;
    });
    return {
        // public method, so it can be called from within the iframe
        trigger: function (em) {
            $(window).trigger("fontresize", [em]);
        }
    };
}(jQuery));


/*
 * jQuery Carousel plugin
 */
; (function ($) {
    function ScrollGallery(options) {
        this.options = $.extend({
            mask: 'https://www.plainsallamerican.com/CMSPages/div.mask',
            slider: '>*',
            slides: '>*',
            activeClass: 'active',
            disabledClass: 'disabled',
            btnPrev: 'a.btn-prev',
            btnNext: 'a.btn-next',
            generatePagination: false,
            pagerList: '<ul>',
            pagerListItem: '<li><a href="#"></a></li>',
            pagerListItemText: 'a',
            pagerLinks: '.pagination li',
            currentNumber: 'span.current-num',
            totalNumber: 'span.total-num',
            btnPlay: '.btn-play',
            btnPause: '.btn-pause',
            btnPlayPause: '.btn-play-pause',
            galleryReadyClass: 'gallery-js-ready',
            autorotationActiveClass: 'autorotation-active',
            autorotationDisabledClass: 'autorotation-disabled',
            stretchSlideToMask: false,
            circularRotation: true,
            disableWhileAnimating: false,
            autoRotation: false,
            pauseOnHover: isTouchDevice ? false : true,
            maskAutoSize: false,
            switchTime: 4000,
            animSpeed: 600,
            event: 'click',
            swipeThreshold: 15,
            handleTouch: true,
            vertical: false,
            useTranslate3D: false,
            step: false
        }, options);
        this.init();
    }
    ScrollGallery.prototype = {
        init: function () {
            if (this.options.holder) {
                this.findElements();
                this.attachEvents();
                this.refreshPosition();
                this.refreshState(true);
                this.resumeRotation();
                this.makeCallback('onInit', this);
            }
        },
        findElements: function () {
            // define dimensions proporties
            this.fullSizeFunction = this.options.vertical ? 'outerHeight' : 'outerWidth';
            this.innerSizeFunction = this.options.vertical ? 'height' : 'width';
            this.slideSizeFunction = 'outerHeight';
            this.maskSizeProperty = 'height';
            this.animProperty = this.options.vertical ? 'marginTop' : 'marginLeft';

            // control elements
            this.gallery = $(this.options.holder).addClass(this.options.galleryReadyClass);
            this.mask = this.gallery.find(this.options.mask);
            this.slider = this.mask.find(this.options.slider);
            this.slides = this.slider.find(this.options.slides);
            this.btnPrev = this.gallery.find(this.options.btnPrev);
            this.btnNext = this.gallery.find(this.options.btnNext);
            this.currentStep = 0; this.stepsCount = 0;

            // get start index
            if (this.options.step === false) {
                var activeSlide = this.slides.filter('.' + this.options.activeClass);
                if (activeSlide.length) {
                    this.currentStep = this.slides.index(activeSlide);
                }
            }

            // calculate offsets
            this.calculateOffsets();

            // create gallery pagination
            if (typeof this.options.generatePagination === 'string') {
                this.pagerLinks = $();
                this.buildPagination();
            } else {
                this.pagerLinks = this.gallery.find(this.options.pagerLinks);
                this.attachPaginationEvents();
            }

            // autorotation control buttons
            this.btnPlay = this.gallery.find(this.options.btnPlay);
            this.btnPause = this.gallery.find(this.options.btnPause);
            this.btnPlayPause = this.gallery.find(this.options.btnPlayPause);

            // misc elements
            this.curNum = this.gallery.find(this.options.currentNumber);
            this.allNum = this.gallery.find(this.options.totalNumber);
        },
        attachEvents: function () {
            // bind handlers scope
            var self = this;
            this.bindHandlers(['onWindowResize']);
            $(window).bind('load resize orientationchange', this.onWindowResize);

            // previous and next button handlers
            if (this.btnPrev.length) {
                this.prevSlideHandler = function (e) {
                    e.preventDefault();
                    self.prevSlide();
                };
                this.btnPrev.bind(this.options.event, this.prevSlideHandler);
            }
            if (this.btnNext.length) {
                this.nextSlideHandler = function (e) {
                    e.preventDefault();
                    self.nextSlide();
                };
                this.btnNext.bind(this.options.event, this.nextSlideHandler);
            }

            // pause on hover handling
            if (this.options.pauseOnHover && !isTouchDevice) {
                this.hoverHandler = function () {
                    if (self.options.autoRotation) {
                        self.galleryHover = true;
                        self.pauseRotation();
                    }
                };
                this.leaveHandler = function () {
                    if (self.options.autoRotation) {
                        self.galleryHover = false;
                        self.resumeRotation();
                    }
                };
                this.gallery.bind({ mouseenter: this.hoverHandler, mouseleave: this.leaveHandler });
            }

            // autorotation buttons handler
            if (this.btnPlay.length) {
                this.btnPlayHandler = function (e) {
                    e.preventDefault();
                    self.startRotation();
                };
                this.btnPlay.bind(this.options.event, this.btnPlayHandler);
            }
            if (this.btnPause.length) {
                this.btnPauseHandler = function (e) {
                    e.preventDefault();
                    self.stopRotation();
                };
                this.btnPause.bind(this.options.event, this.btnPauseHandler);
            }
            if (this.btnPlayPause.length) {
                this.btnPlayPauseHandler = function (e) {
                    e.preventDefault();
                    if (!self.gallery.hasClass(self.options.autorotationActiveClass)) {
                        self.startRotation();
                    } else {
                        self.stopRotation();
                    }
                };
                this.btnPlayPause.bind(this.options.event, this.btnPlayPauseHandler);
            }

            // enable hardware acceleration
            if (isTouchDevice && this.options.useTranslate3D) {
                this.slider.css({ '-webkit-transform': 'translate3d(0px, 0px, 0px)' });
            }

            // swipe event handling
            if (isTouchDevice && this.options.handleTouch && window.Hammer) {
                this.swipeHandler = Hammer(this.mask[0], {
                    dragBlockHorizontal: !this.options.vertical,
                    dragBlockVertical: this.options.vertical,
                    dragMinDistance: 1,
                    behavior: {
                        touchAction: this.options.vertical ? 'pan-x' : 'pan-y'
                    }
                }).on('touch release ' + (self.options.vertical ? 'dragup dragdown' : 'dragleft dragright'), function (e) {
                    switch (e.type) {
                        case 'touch':
                            if (self.galleryAnimating) {
                                e.gesture.stopDetect();
                            } else {
                                self.pauseRotation();
                                self.originalOffset = parseFloat(self.slider.css(self.animProperty));
                            }
                            break;
                        case 'dragup':
                        case 'dragdown':
                        case 'dragleft':
                        case 'dragright':
                            e.gesture.preventDefault();
                            var tmpOffset = self.originalOffset + e.gesture[self.options.vertical ? 'deltaY' : 'deltaX'];
                            tmpOffset = Math.max(Math.min(0, tmpOffset), self.maxOffset);
                            self.slider.css(self.animProperty, tmpOffset);
                            break;
                        case 'release':
                            self.resumeRotation();
                            if (Math.abs(e.gesture[self.options.vertical ? 'deltaY' : 'deltaX']) > self.options.swipeThreshold) {
                                if (e.gesture.direction == 'left' || e.gesture.direction == 'up') {
                                    self.nextSlide();
                                } else {
                                    self.prevSlide();
                                }
                            } else {
                                self.switchSlide();
                            }
                    }
                });
            }
        },
        onWindowResize: function () {
            if (!this.galleryAnimating) {
                this.calculateOffsets();
                this.refreshPosition();
                this.buildPagination();
                this.refreshState();
                this.resizeQueue = false;
            } else {
                this.resizeQueue = true;
            }
        },
        refreshPosition: function () {
            this.currentStep = Math.min(this.currentStep, this.stepsCount - 1);
            this.tmpProps = {};
            this.tmpProps[this.animProperty] = this.getStepOffset();
            this.slider.stop().css(this.tmpProps);
        },
        calculateOffsets: function () {
            var self = this, tmpOffset, tmpStep;
            if (this.options.stretchSlideToMask) {
                var tmpObj = {};
                tmpObj[this.innerSizeFunction] = this.mask[this.innerSizeFunction]();
                this.slides.css(tmpObj);
            }

            this.maskSize = this.mask[this.innerSizeFunction]();
            this.sumSize = this.getSumSize();
            this.maxOffset = this.maskSize - this.sumSize;

            // vertical gallery with single size step custom behavior
            if (this.options.vertical && this.options.maskAutoSize) {
                this.options.step = 1;
                this.stepsCount = this.slides.length;
                this.stepOffsets = [0];
                tmpOffset = 0;
                for (var i = 0; i < this.slides.length; i++) {
                    tmpOffset -= $(this.slides[i])[this.fullSizeFunction](true);
                    this.stepOffsets.push(tmpOffset);
                }
                this.maxOffset = tmpOffset;
                return;
            }

            // scroll by slide size
            if (typeof this.options.step === 'number' && this.options.step > 0) {
                this.slideDimensions = [];
                this.slides.each($.proxy(function (ind, obj) {
                    self.slideDimensions.push($(obj)[self.fullSizeFunction](true));
                }, this));

                // calculate steps count
                this.stepOffsets = [0];
                this.stepsCount = 1;
                tmpOffset = tmpStep = 0;
                while (tmpOffset > this.maxOffset) {
                    tmpOffset -= this.getSlideSize(tmpStep, tmpStep + this.options.step);
                    tmpStep += this.options.step;
                    this.stepOffsets.push(Math.max(tmpOffset, this.maxOffset));
                    this.stepsCount++;
                }
            }
                // scroll by mask size
            else {
                // define step size
                this.stepSize = this.maskSize;

                // calculate steps count
                this.stepsCount = 1;
                tmpOffset = 0;
                while (tmpOffset > this.maxOffset) {
                    tmpOffset -= this.stepSize;
                    this.stepsCount++;
                }
            }
        },
        getSumSize: function () {
            var sum = 0;
            this.slides.each($.proxy(function (ind, obj) {
                sum += $(obj)[this.fullSizeFunction](true);
            }, this));
            this.slider.css(this.innerSizeFunction, sum);
            return sum;
        },
        getStepOffset: function (step) {
            step = step || this.currentStep;
            if (typeof this.options.step === 'number') {
                return this.stepOffsets[this.currentStep];
            } else {
                return Math.min(0, Math.max(-this.currentStep * this.stepSize, this.maxOffset));
            }
        },
        getSlideSize: function (i1, i2) {
            var sum = 0;
            for (var i = i1; i < Math.min(i2, this.slideDimensions.length) ; i++) {
                sum += this.slideDimensions[i];
            }
            return sum;
        },
        buildPagination: function () {
            if (typeof this.options.generatePagination === 'string') {
                if (!this.pagerHolder) {
                    this.pagerHolder = this.gallery.find(this.options.generatePagination);
                }
                if (this.pagerHolder.length && this.oldStepsCount != this.stepsCount) {
                    this.oldStepsCount = this.stepsCount;
                    this.pagerHolder.empty();
                    this.pagerList = $(this.options.pagerList).appendTo(this.pagerHolder);
                    for (var i = 0; i < this.stepsCount; i++) {
                        $(this.options.pagerListItem).appendTo(this.pagerList).find(this.options.pagerListItemText).text(i + 1);
                    }
                    this.pagerLinks = this.pagerList.children();
                    this.attachPaginationEvents();
                }
            }
        },
        attachPaginationEvents: function () {
            var self = this;
            this.pagerLinksHandler = function (e) {
                e.preventDefault();
                self.numSlide(self.pagerLinks.index(e.currentTarget));
            };
            this.pagerLinks.bind(this.options.event, this.pagerLinksHandler);
        },
        prevSlide: function () {
            if (!(this.options.disableWhileAnimating && this.galleryAnimating)) {
                if (this.currentStep > 0) {
                    this.currentStep--;
                    this.switchSlide();
                } else if (this.options.circularRotation) {
                    this.currentStep = this.stepsCount - 1;
                    this.switchSlide();
                }
            }
        },
        nextSlide: function (fromAutoRotation) {
            if (!(this.options.disableWhileAnimating && this.galleryAnimating)) {
                if (this.currentStep < this.stepsCount - 1) {
                    this.currentStep++;
                    this.switchSlide();
                } else if (this.options.circularRotation || fromAutoRotation === true) {
                    this.currentStep = 0;
                    this.switchSlide();
                }
            }
        },
        numSlide: function (c) {
            if (this.currentStep != c) {
                this.currentStep = c;
                this.switchSlide();
            }
        },
        switchSlide: function () {
            var self = this;
            this.galleryAnimating = true;
            this.tmpProps = {};
            this.tmpProps[this.animProperty] = this.getStepOffset();
            this.slider.stop().animate(this.tmpProps, {
                duration: this.options.animSpeed, complete: function () {
                    // animation complete
                    self.galleryAnimating = false;
                    if (self.resizeQueue) {
                        self.onWindowResize();
                    }

                    // onchange callback
                    self.makeCallback('onChange', self);
                    self.autoRotate();
                }
            });
            this.refreshState();

            // onchange callback
            this.makeCallback('onBeforeChange', this);
        },
        refreshState: function (initial) {
            if (this.options.step === 1 || this.stepsCount === this.slides.length) {
                this.slides.removeClass(this.options.activeClass).eq(this.currentStep).addClass(this.options.activeClass);
            }
            this.pagerLinks.removeClass(this.options.activeClass).eq(this.currentStep).addClass(this.options.activeClass);
            this.curNum.html(this.currentStep + 1);
            this.allNum.html(this.stepsCount);

            // initial refresh
            if (this.options.maskAutoSize && typeof this.options.step === 'number') {
                this.tmpProps = {};
                this.tmpProps[this.maskSizeProperty] = this.slides.eq(Math.min(this.currentStep, this.slides.length - 1))[this.slideSizeFunction](true);
                this.mask.stop()[initial ? 'css' : 'animate'](this.tmpProps);
            }

            // disabled state
            if (!this.options.circularRotation) {
                this.btnPrev.add(this.btnNext).removeClass(this.options.disabledClass);
                if (this.currentStep === 0) this.btnPrev.addClass(this.options.disabledClass);
                if (this.currentStep === this.stepsCount - 1) this.btnNext.addClass(this.options.disabledClass);
            }

            // add class if not enough slides
            this.gallery.toggleClass('not-enough-slides', this.sumSize <= this.maskSize);
        },
        startRotation: function () {
            this.options.autoRotation = true;
            this.galleryHover = false;
            this.autoRotationStopped = false;
            this.resumeRotation();
        },
        stopRotation: function () {
            this.galleryHover = true;
            this.autoRotationStopped = true;
            this.pauseRotation();
        },
        pauseRotation: function () {
            this.gallery.addClass(this.options.autorotationDisabledClass);
            this.gallery.removeClass(this.options.autorotationActiveClass);
            clearTimeout(this.timer);
        },
        resumeRotation: function () {
            if (!this.autoRotationStopped) {
                this.gallery.addClass(this.options.autorotationActiveClass);
                this.gallery.removeClass(this.options.autorotationDisabledClass);
                this.autoRotate();
            }
        },
        autoRotate: function () {
            var self = this;
            clearTimeout(this.timer);
            if (this.options.autoRotation && !this.galleryHover && !this.autoRotationStopped) {
                this.timer = setTimeout(function () {
                    self.nextSlide(true);
                }, this.options.switchTime);
            } else {
                this.pauseRotation();
            }
        },
        bindHandlers: function (handlersList) {
            var self = this;
            $.each(handlersList, function (index, handler) {
                var origHandler = self[handler];
                self[handler] = function () {
                    return origHandler.apply(self, arguments);
                };
            });
        },
        makeCallback: function (name) {
            if (typeof this.options[name] === 'function') {
                var args = Array.prototype.slice.call(arguments);
                args.shift();
                this.options[name].apply(this, args);
            }
        },
        destroy: function () {
            // destroy handler
            $(window).unbind('load resize orientationchange', this.onWindowResize);
            this.btnPrev.unbind(this.options.event, this.prevSlideHandler);
            this.btnNext.unbind(this.options.event, this.nextSlideHandler);
            this.pagerLinks.unbind(this.options.event, this.pagerLinksHandler);
            this.gallery.unbind({ mouseenter: this.hoverHandler, mouseleave: this.leaveHandler });

            // autorotation buttons handlers
            this.stopRotation();
            this.btnPlay.unbind(this.options.event, this.btnPlayHandler);
            this.btnPause.unbind(this.options.event, this.btnPauseHandler);
            this.btnPlayPause.unbind(this.options.event, this.btnPlayPauseHandler);

            // destroy swipe handler
            if (this.swipeHandler) {
                this.swipeHandler.dispose();
            }

            // remove inline styles, classes and pagination
            var unneededClasses = [this.options.galleryReadyClass, this.options.autorotationActiveClass, this.options.autorotationDisabledClass];
            this.gallery.removeClass(unneededClasses.join(' '));
            this.slider.add(this.slides).removeAttr('style');
            if (typeof this.options.generatePagination === 'string') {
                this.pagerHolder.empty();
            }
        }
    };

    // detect device type
    var isTouchDevice = /MSIE 10.*Touch/.test(navigator.userAgent) || ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch;

    // jquery plugin
    $.fn.scrollGallery = function (opt) {
        return this.each(function () {
            $(this).data('ScrollGallery', new ScrollGallery($.extend(opt, { holder: this })));
        });
    };
}(jQuery));

/*! matchMedia() polyfill - Test a CSS media type/query in JS. Authors & copyright (c) 2012: Scott Jehl, Paul Irish, Nicholas Zakas. Dual MIT/BSD license */
; window.matchMedia = window.matchMedia || (function (e, f) { var c, a = e.documentElement, b = a.firstElementChild || a.firstChild, d = e.createElement("body"), g = e.createElement("div"); g.id = "mq-test-1"; g.style.cssText = "position:absolute;top:-100em"; d.appendChild(g); return function (h) { g.innerHTML = '&shy;<style media="' + h + '"> #mq-test-1 { width: 42px; }</style>'; a.insertBefore(d, b); c = g.offsetWidth == 42; a.removeChild(d); return { matches: c, media: h } } })(document);

/*! Picturefill - Responsive Images that work today. (and mimic the proposed Picture element with span elements). Author: Scott Jehl, Filament Group, 2012 | License: MIT/GPLv2 */
; (function (a) { a.picturefill = function () { var b = a.document.getElementsByTagName("span"); for (var f = 0, l = b.length; f < l; f++) { if (b[f].getAttribute("data-picture") !== null) { var c = b[f].getElementsByTagName("span"), h = []; for (var e = 0, g = c.length; e < g; e++) { var d = c[e].getAttribute("data-media"); if (!d || (a.matchMedia && a.matchMedia(d).matches)) { h.push(c[e]) } } var m = b[f].getElementsByTagName("img")[0]; if (h.length) { var k = h.pop(); if (!m || m.parentNode.nodeName === "NOSCRIPT") { m = a.document.createElement("img"); m.alt = b[f].getAttribute("data-alt") } if (k.getAttribute("data-width")) { m.setAttribute("width", k.getAttribute("data-width")) } else { m.removeAttribute("width") } if (k.getAttribute("data-height")) { m.setAttribute("height", k.getAttribute("data-height")) } else { m.removeAttribute("height") } m.src = k.getAttribute("data-src"); k.appendChild(m) } else { if (m) { m.parentNode.removeChild(m) } } } } }; if (a.addEventListener) { a.addEventListener("resize", a.picturefill, false); a.addEventListener("DOMContentLoaded", function () { a.picturefill(); a.removeEventListener("load", a.picturefill, false) }, false); a.addEventListener("load", a.picturefill, false) } else { if (a.attachEvent) { a.attachEvent("onload", a.picturefill) } } }(this));

/*
 * FancyBox - jQuery Plugin
 * Simple and fancy lightbox alternative
 *
 * Examples and documentation at: http://fancybox.net
 * 
 * Copyright (c) 2008 - 2010 Janis Skarnelis
 * That said, it is hardly a one-person project. Many people have submitted bugs, code, and offered their advice freely. Their support is greatly appreciated.
 *
 * Version: 1.3.4 (11/11/2010)
 * Requires: jQuery v1.3+
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 */
; (function (B) { var L, T, Q, M, d, m, J, A, O, z, C = 0, H = {}, j = [], e = 0, G = {}, y = [], f = null, o = new Image(), i = /\.(jpg|gif|png|bmp|jpeg)(.*)?$/i, k = /[^\.]\.(swf)\s*$/i, p, N = 1, h = 0, t = "", b, c, P = false, s = B.extend(B("<div/>")[0], { prop: 0 }), S = /MSIE 6/.test(navigator.userAgent) && B.browser.version < 7 && !window.XMLHttpRequest, r = function () { T.hide(); o.onerror = o.onload = null; if (f) { f.abort() } L.empty() }, x = function () { if (false === H.onError(j, C, H)) { T.hide(); P = false; return } H.titleShow = false; H.width = "auto"; H.height = "auto"; L.html('<p id="fancybox-error">The requested content cannot be loaded.<br />Please try again later.</p>'); n() }, w = function () { var Z = j[C], W, Y, ab, aa, V, X; r(); H = B.extend({}, B.fn.fancybox.defaults, (typeof B(Z).data("fancybox") == "undefined" ? H : B(Z).data("fancybox"))); X = H.onStart(j, C, H); if (X === false) { P = false; return } else { if (typeof X == "object") { H = B.extend(H, X) } } ab = H.title || (Z.nodeName ? B(Z).attr("title") : Z.title) || ""; if (Z.nodeName && !H.orig) { H.orig = B(Z).children("img:first").length ? B(Z).children("img:first") : B(Z) } if (ab === "" && H.orig && H.titleFromAlt) { ab = H.orig.attr("alt") } W = H.href || (Z.nodeName ? B(Z).attr("href") : Z.href) || null; if ((/^(?:javascript)/i).test(W) || W == "#") { W = null } if (H.type) { Y = H.type; if (!W) { W = H.content } } else { if (H.content) { Y = "html" } else { if (W) { if (W.match(i)) { Y = "image" } else { if (W.match(k)) { Y = "swf" } else { if (B(Z).hasClass("iframe")) { Y = "iframe" } else { if (W.indexOf("#") === 0) { Y = "inline" } else { Y = "ajax" } } } } } } } if (!Y) { x(); return } if (Y == "inline") { Z = W.substr(W.indexOf("#")); Y = B(Z).length > 0 ? "inline" : "ajax" } H.type = Y; H.href = W; H.title = ab; if (H.autoDimensions) { if (H.type == "html" || H.type == "inline" || H.type == "ajax") { H.width = "auto"; H.height = "auto" } else { H.autoDimensions = false } } if (H.modal) { H.overlayShow = true; H.hideOnOverlayClick = false; H.hideOnContentClick = false; H.enableEscapeButton = false; H.showCloseButton = false } H.padding = parseInt(H.padding, 10); H.margin = parseInt(H.margin, 10); L.css("padding", (H.padding + H.margin)); B(".fancybox-inline-tmp").unbind("fancybox-cancel").bind("fancybox-change", function () { B(this).replaceWith(m.children()) }); switch (Y) { case "html": L.html(H.content); n(); break; case "inline": if (B(Z).parent().is("#fancybox-content") === true) { P = false; return } B('<div class="fancybox-inline-tmp" />').hide().insertBefore(B(Z)).bind("fancybox-cleanup", function () { B(this).replaceWith(m.children()) }).bind("fancybox-cancel", function () { B(this).replaceWith(L.children()) }); B(Z).appendTo(L); n(); break; case "image": P = false; B.fancybox.showActivity(); o = new Image(); o.onerror = function () { x() }; o.onload = function () { P = true; o.onerror = o.onload = null; F() }; o.src = W; break; case "swf": H.scrolling = "no"; aa = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="' + H.width + '" height="' + H.height + '"><param name="movie" value="' + W + '"></param>'; V = ""; B.each(H.swf, function (ac, ad) { aa += '<param name="' + ac + '" value="' + ad + '"></param>'; V += " " + ac + '="' + ad + '"' }); aa += '<embed src="' + W + '" type="application/x-shockwave-flash" width="' + H.width + '" height="' + H.height + '"' + V + "></embed></object>"; L.html(aa); n(); break; case "ajax": P = false; B.fancybox.showActivity(); H.ajax.win = H.ajax.success; f = B.ajax(B.extend({}, H.ajax, { url: W, data: H.ajax.data || {}, dataType: "text", error: function (ac, ae, ad) { if (ac.status > 0) { x() } }, success: function (ad, af, ac) { var ae = typeof ac == "object" ? ac : f; if (ae.status == 200 || ae.status === 0) { if (typeof H.ajax.win == "function") { X = H.ajax.win(W, ad, af, ac); if (X === false) { T.hide(); return } else { if (typeof X == "string" || typeof X == "object") { ad = X } } } L.html(ad); n() } } })); break; case "iframe": E(); break } }, n = function () { var V = H.width, W = H.height; if (V.toString().indexOf("%") > -1) { V = parseInt((B(window).width() - (H.margin * 2)) * parseFloat(V) / 100, 10) + "px" } else { V = V == "auto" ? "auto" : V + "px" } if (W.toString().indexOf("%") > -1) { W = parseInt((B(window).height() - (H.margin * 2)) * parseFloat(W) / 100, 10) + "px" } else { W = W == "auto" ? "auto" : W + "px" } L.wrapInner('<div style="width:' + V + ";height:" + W + ";overflow: " + (H.scrolling == "auto" ? "auto" : (H.scrolling == "yes" ? "scroll" : "hidden")) + ';position:relative;"></div>'); H.width = L.width(); H.height = L.height(); E() }, F = function () { H.width = o.width; H.height = o.height; B("<img />").attr({ id: "fancybox-img", src: o.src, alt: H.title }).appendTo(L); E() }, E = function () { var W, V; T.hide(); if (M.is(":visible") && false === G.onCleanup(y, e, G)) { B('.fancybox-inline-tmp').trigger('fancybox-cancel'); P = false; return } P = true; B(m.add(Q)).unbind(); B(window).unbind("https://www.plainsallamerican.com/CMSPages/resize.fb scroll.fb"); B(document).unbind("https://www.plainsallamerican.com/CMSPages/keydown.fb"); if (M.is(":visible") && G.titlePosition !== "outside") { M.css("height", M.height()) } y = j; e = C; G = H; if (G.overlayShow) { Q.css({ "background-color": G.overlayColor, opacity: G.overlayOpacity, cursor: G.hideOnOverlayClick ? "pointer" : "auto", height: B(document).height() }); if (!Q.is(":visible")) { if (S) { B("select:not(#fancybox-tmp select)").filter(function () { return this.style.visibility !== "hidden" }).css({ visibility: "hidden" }).one("fancybox-cleanup", function () { this.style.visibility = "inherit" }) } Q.show() } } else { Q.hide() } c = R(); l(); if (M.is(":visible")) { B(J.add(O).add(z)).hide(); W = M.position(), b = { top: W.top, left: W.left, width: M.width(), height: M.height() }; V = (b.width == c.width && b.height == c.height); m.fadeTo(G.changeFade, 0.3, function () { var X = function () { m.html(L.contents()).fadeTo(G.changeFade, 1, v) }; B('.fancybox-inline-tmp').trigger('fancybox-change'); m.empty().removeAttr("filter").css({ "border-width": G.padding, width: c.width - G.padding * 2, height: H.autoDimensions ? "auto" : c.height - h - G.padding * 2 }); if (V) { X() } else { s.prop = 0; B(s).animate({ prop: 1 }, { duration: G.changeSpeed, easing: G.easingChange, step: U, complete: X }) } }); return } M.removeAttr("style"); m.css("border-width", G.padding); if (G.transitionIn == "elastic") { b = I(); m.html(L.contents()); M.show(); if (G.opacity) { c.opacity = 0 } s.prop = 0; B(s).animate({ prop: 1 }, { duration: G.speedIn, easing: G.easingIn, step: U, complete: v }); return } if (G.titlePosition == "inside" && h > 0) { A.show() } m.css({ width: c.width - G.padding * 2, height: H.autoDimensions ? "auto" : c.height - h - G.padding * 2 }).html(L.contents()); M.css(c).fadeIn(G.transitionIn == "none" ? 0 : G.speedIn, v) }, D = function (V) { if (V && V.length) { if (G.titlePosition == "float") { return '<table id="fancybox-title-float-wrap" cellpadding="0" cellspacing="0"><tr><td id="fancybox-title-float-left"></td><td id="fancybox-title-float-main">' + V + '</td><td id="fancybox-title-float-right"></td></tr></table>' } return '<div id="fancybox-title-' + G.titlePosition + '">' + V + "</div>" } return false }, l = function () { t = G.title || ""; h = 0; A.empty().removeAttr("style").removeClass(); if (G.titleShow === false) { A.hide(); return } t = B.isFunction(G.titleFormat) ? G.titleFormat(t, y, e, G) : D(t); if (!t || t === "") { A.hide(); return } A.addClass("fancybox-title-" + G.titlePosition).html(t).appendTo("body").show(); switch (G.titlePosition) { case "inside": A.css({ width: c.width - (G.padding * 2), marginLeft: G.padding, marginRight: G.padding }); h = A.outerHeight(true); A.appendTo(d); c.height += h; break; case "over": A.css({ marginLeft: G.padding, width: c.width - (G.padding * 2), bottom: G.padding }).appendTo(d); break; case "float": A.css("left", parseInt((A.width() - c.width - 40) / 2, 10) * -1).appendTo(M); break; default: A.css({ width: c.width - (G.padding * 2), paddingLeft: G.padding, paddingRight: G.padding }).appendTo(M); break } A.hide() }, g = function () { if (G.enableEscapeButton || G.enableKeyboardNav) { B(document).bind("https://www.plainsallamerican.com/CMSPages/keydown.fb", function (V) { if (V.keyCode == 27 && G.enableEscapeButton) { V.preventDefault(); B.fancybox.close() } else { if ((V.keyCode == 37 || V.keyCode == 39) && G.enableKeyboardNav && V.target.tagName !== "INPUT" && V.target.tagName !== "TEXTAREA" && V.target.tagName !== "SELECT") { V.preventDefault(); B.fancybox[V.keyCode == 37 ? "prev" : "next"]() } } }) } if (!G.showNavArrows) { O.hide(); z.hide(); return } if ((G.cyclic && y.length > 1) || e !== 0) { O.show() } if ((G.cyclic && y.length > 1) || e != (y.length - 1)) { z.show() } }, v = function () { if (B.support.opacity === false) { m.get(0).style.removeAttribute("filter"); M.get(0).style.removeAttribute("filter") } if (H.autoDimensions) { m.css("height", "auto") } M.css("height", "auto"); if (t && t.length) { A.show() } if (G.showCloseButton) { J.show() } g(); if (G.hideOnContentClick) { m.bind("click", B.fancybox.close) } if (G.hideOnOverlayClick) { Q.bind("click", B.fancybox.close) } B(window).bind("https://www.plainsallamerican.com/CMSPages/resize.fb", B.fancybox.resize); if (G.centerOnScroll) { B(window).bind("https://www.plainsallamerican.com/CMSPages/scroll.fb", B.fancybox.center) } if (G.type == "iframe") { B('<iframe id="fancybox-frame" name="fancybox-frame' + new Date().getTime() + '" frameborder="0" hspace="0" ' + (window.attachEvent ? 'allowtransparency="true""' : "") + ' scrolling="' + H.scrolling + '" src="' + G.href + '"></iframe>').appendTo(m) } M.show(); P = false; B.fancybox.center(); G.onComplete(y, e, G); K() }, K = function () { var V, W; if ((y.length - 1) > e) { V = y[e + 1].href; if (typeof V !== "undefined" && V.match(i)) { W = new Image(); W.src = V } } if (e > 0) { V = y[e - 1].href; if (typeof V !== "undefined" && V.match(i)) { W = new Image(); W.src = V } } }, U = function (W) { var V = { width: parseInt(b.width + (c.width - b.width) * W, 10), height: parseInt(b.height + (c.height - b.height) * W, 10), top: parseInt(b.top + (c.top - b.top) * W, 10), left: parseInt(b.left + (c.left - b.left) * W, 10) }; if (typeof c.opacity !== "undefined") { V.opacity = W < 0.5 ? 0.5 : W } M.css(V); m.css({ width: V.width - G.padding * 2, height: V.height - (h * W) - G.padding * 2 }) }, u = function () { return [B(window).width() - (G.margin * 2), B(window).height() - (G.margin * 2), B(document).scrollLeft() + G.margin, B(document).scrollTop() + G.margin] }, R = function () { var V = u(), Z = {}, W = G.autoScale, X = G.padding * 2, Y; if (G.width.toString().indexOf("%") > -1) { Z.width = parseInt((V[0] * parseFloat(G.width)) / 100, 10) } else { Z.width = G.width + X } if (G.height.toString().indexOf("%") > -1) { Z.height = parseInt((V[1] * parseFloat(G.height)) / 100, 10) } else { Z.height = G.height + X } if (W && (Z.width > V[0] || Z.height > V[1])) { if (H.type == "image" || H.type == "swf") { Y = (G.width) / (G.height); if ((Z.width) > V[0]) { Z.width = V[0]; Z.height = parseInt(((Z.width - X) / Y) + X, 10) } if ((Z.height) > V[1]) { Z.height = V[1]; Z.width = parseInt(((Z.height - X) * Y) + X, 10) } } else { Z.width = Math.min(Z.width, V[0]); Z.height = Math.min(Z.height, V[1]) } } Z.top = parseInt(Math.max(V[3] - 20, V[3] + ((V[1] - Z.height - 40) * 0.5)), 10); Z.left = parseInt(Math.max(V[2] - 20, V[2] + ((V[0] - Z.width - 40) * 0.5)), 10); return Z }, q = function (V) { var W = V.offset(); W.top += parseInt(V.css("paddingTop"), 10) || 0; W.left += parseInt(V.css("paddingLeft"), 10) || 0; W.top += parseInt(V.css("border-top-width"), 10) || 0; W.left += parseInt(V.css("border-left-width"), 10) || 0; W.width = V.width(); W.height = V.height(); return W }, I = function () { var Y = H.orig ? B(H.orig) : false, X = {}, W, V; if (Y && Y.length) { W = q(Y); X = { width: W.width + (G.padding * 2), height: W.height + (G.padding * 2), top: W.top - G.padding - 20, left: W.left - G.padding - 20 } } else { V = u(); X = { width: G.padding * 2, height: G.padding * 2, top: parseInt(V[3] + V[1] * 0.5, 10), left: parseInt(V[2] + V[0] * 0.5, 10) } } return X }, a = function () { if (!T.is(":visible")) { clearInterval(p); return } B("div", T).css("top", (N * -40) + "px"); N = (N + 1) % 12 }; B.fn.fancybox = function (V) { if (!B(this).length) { return this } B(this).data("fancybox", B.extend({}, V, (B.metadata ? B(this).metadata() : {}))).unbind("https://www.plainsallamerican.com/CMSPages/click.fb").bind("https://www.plainsallamerican.com/CMSPages/click.fb", function (X) { X.preventDefault(); if (P) { return } P = true; B(this).blur(); j = []; C = 0; var W = B(this).attr("rel") || ""; if (!W || W == "" || W === "nofollow") { j.push(this) } else { j = B('a[rel="' + W + '"], area[rel="' + W + '"]'); C = j.index(this) } w(); return }); return this }; B.fancybox = function (Y) { var X; if (P) { return } P = true; X = typeof arguments[1] !== "undefined" ? arguments[1] : {}; j = []; C = parseInt(X.index, 10) || 0; if (B.isArray(Y)) { for (var W = 0, V = Y.length; W < V; W++) { if (typeof Y[W] == "object") { B(Y[W]).data("fancybox", B.extend({}, X, Y[W])) } else { Y[W] = B({}).data("fancybox", B.extend({ content: Y[W] }, X)) } } j = jQuery.merge(j, Y) } else { if (typeof Y == "object") { B(Y).data("fancybox", B.extend({}, X, Y)) } else { Y = B({}).data("fancybox", B.extend({ content: Y }, X)) } j.push(Y) } if (C > j.length || C < 0) { C = 0 } w() }; B.fancybox.showActivity = function () { clearInterval(p); T.show(); p = setInterval(a, 66) }; B.fancybox.hideActivity = function () { T.hide() }; B.fancybox.next = function () { return B.fancybox.pos(e + 1) }; B.fancybox.prev = function () { return B.fancybox.pos(e - 1) }; B.fancybox.pos = function (V) { if (P) { return } V = parseInt(V); j = y; if (V > -1 && V < y.length) { C = V; w() } else { if (G.cyclic && y.length > 1) { C = V >= y.length ? 0 : y.length - 1; w() } } return }; B.fancybox.cancel = function () { if (P) { return } P = true; B('.fancybox-inline-tmp').trigger('fancybox-cancel'); r(); H.onCancel(j, C, H); P = false }; B.fancybox.close = function () { if (P || M.is(":hidden")) { return } P = true; if (G && false === G.onCleanup(y, e, G)) { P = false; return } r(); B(J.add(O).add(z)).hide(); B(m.add(Q)).unbind(); B(window).unbind("https://www.plainsallamerican.com/CMSPages/resize.fb scroll.fb"); B(document).unbind("https://www.plainsallamerican.com/CMSPages/keydown.fb"); if (G.type === "iframe") { m.find("iframe").attr("src", S && /^https/i.test(window.location.href || "") ? "javascript:void(false)" : "about:blank") } if (G.titlePosition !== "inside") { A.empty() } M.stop(); function V() { Q.fadeOut("fast"); A.empty().hide(); M.hide(); B('.fancybox-inline-tmp').trigger('fancybox-cleanup'); m.empty(); G.onClosed(y, e, G); y = H = []; e = C = 0; G = H = {}; P = false } if (G.transitionOut == "elastic") { b = I(); var W = M.position(); c = { top: W.top, left: W.left, width: M.width(), height: M.height() }; if (G.opacity) { c.opacity = 1 } A.empty().hide(); s.prop = 1; B(s).animate({ prop: 0 }, { duration: G.speedOut, easing: G.easingOut, step: U, complete: V }) } else { M.fadeOut(G.transitionOut == "none" ? 0 : G.speedOut, V) } }; B.fancybox.resize = function () { if (Q.is(":visible")) { Q.css("height", B(document).height()) } B.fancybox.center(true) }; B.fancybox.center = function () { var V, W; if (P) { return } W = arguments[0] === true ? 1 : 0; V = u(); if (!W && (M.width() > V[0] || M.height() > V[1])) { return } M.stop().animate({ top: parseInt(Math.max(V[3] - 20, V[3] + ((V[1] - m.height() - 40) * 0.5) - G.padding)), left: parseInt(Math.max(V[2] - 20, V[2] + ((V[0] - m.width() - 40) * 0.5) - G.padding)) }, typeof arguments[0] == "number" ? arguments[0] : 200) }; B.fancybox.init = function () { if (B("#fancybox-wrap").length) { return } B("body").append(L = B('<div id="fancybox-tmp"></div>'), T = B('<div id="fancybox-loading"><div></div></div>'), Q = B('<div id="fancybox-overlay"></div>'), M = B('<div id="fancybox-wrap"></div>')); d = B('<div id="fancybox-outer"></div>').append('<div class="fancybox-bg" id="fancybox-bg-n"></div><div class="fancybox-bg" id="fancybox-bg-ne"></div><div class="fancybox-bg" id="fancybox-bg-e"></div><div class="fancybox-bg" id="fancybox-bg-se"></div><div class="fancybox-bg" id="fancybox-bg-s"></div><div class="fancybox-bg" id="fancybox-bg-sw"></div><div class="fancybox-bg" id="fancybox-bg-w"></div><div class="fancybox-bg" id="fancybox-bg-nw"></div>').appendTo(M); d.append(m = B('<div id="fancybox-content"></div>'), J = B('<a id="fancybox-close"></a>'), A = B('<div id="fancybox-title"></div>'), O = B('<a href="javascript:;" id="fancybox-left"><span class="fancy-ico" id="fancybox-left-ico"></span></a>'), z = B('<a href="javascript:;" id="fancybox-right"><span class="fancy-ico" id="fancybox-right-ico"></span></a>')); J.click(B.fancybox.close); T.click(B.fancybox.cancel); O.click(function (V) { V.preventDefault(); B.fancybox.prev() }); z.click(function (V) { V.preventDefault(); B.fancybox.next() }); if (B.fn.mousewheel) { M.bind("https://www.plainsallamerican.com/CMSPages/mousewheel.fb", function (V, W) { if (P) { V.preventDefault() } else { if (B(V.target).get(0).clientHeight == 0 || B(V.target).get(0).scrollHeight === B(V.target).get(0).clientHeight) { V.preventDefault(); B.fancybox[W > 0 ? "prev" : "next"]() } } }) } if (B.support.opacity === false) { M.addClass("fancybox-ie") } if (S) { T.addClass("fancybox-ie6"); M.addClass("fancybox-ie6"); B('<iframe id="fancybox-hide-sel-frame" src="' + (/^https/i.test(window.location.href || "") ? "javascript:void(false)" : "about:blank") + '" scrolling="no" border="0" frameborder="0" tabindex="-1"></iframe>').prependTo(d) } }; B.fn.fancybox.defaults = { padding: 10, margin: 40, opacity: false, modal: false, cyclic: false, scrolling: "auto", width: 560, height: 340, autoScale: true, autoDimensions: true, centerOnScroll: false, ajax: {}, swf: { wmode: "transparent" }, hideOnOverlayClick: true, hideOnContentClick: false, overlayShow: true, overlayOpacity: 0.7, overlayColor: "#777", titleShow: true, titlePosition: "float", titleFormat: null, titleFromAlt: false, transitionIn: "fade", transitionOut: "fade", speedIn: 300, speedOut: 300, changeSpeed: 300, changeFade: "fast", easingIn: "swing", easingOut: "swing", showCloseButton: true, showNavArrows: true, enableEscapeButton: true, enableKeyboardNav: true, onStart: function () { }, onCancel: function () { }, onComplete: function () { }, onCleanup: function () { }, onClosed: function () { }, onError: function () { } }; B(document).ready(function () { B.fancybox.init() }) })(jQuery);

// side sliding mobile menu
jQuery.fn.mobileMenu = function (opt) {
    var options = jQuery.extend({
        pageWrapper: null,
        animSpeed: 500,
        initResolution: 767,
        useSwipe: false,
        fixedPanels: null,
        fixedHeight: false,
        fixedPosition: false
    }, opt);

    return this.each(function () {
        var opener = jQuery(this),
			win = jQuery(window),
			body = jQuery('body'),
			wrapper = body.find(options.pageWrapper),
			initResolution = "'.." + options.initResolution + "'",
			animSpeed = options.animSpeed,
			isTouchDevice = /MSIE 10.*Touch/.test(navigator.userAgent) || ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch,
			navBlock,
			fixedPanels,
			obj = {};

        obj[initResolution] = {
            on: function () {
                init();
            },
            off: function () {
                destroy();
            }
        }

        //init layouts
        ResponsiveHelper.addRange(obj);

        //init mobile menu function
        function init() {
            //preparation of html structure
            if (!wrapper.length) {
                body.wrapInner('<div id="wrapper" class="temp-wrap" />');
                wrapper = jQuery('#wrapper');
            }
            wrapper.wrap('<div id="page" class="temp-wrap" />');
            if (options.fixedPosition) opener.parent().addClass('fixed');
            if (options.fixedPanels) fixedPanels = wrapper.find(options.fixedPanels);

            //set variables
            var page = jQuery('#page');
            navBlock = jQuery('#' + opener.attr('data-rel'))
            navBlock.data('parent', navBlock.parent());
            navBlock.appendTo(page).show();
            var defWidth = navBlock.outerWidth();

            //addition the necessary styles
            wrapper.add(page).css({
                overflow: 'hidden',
                position: 'relative'
            });
            navBlock.css({
                position: options.fixedPosition ? 'fixed' : 'absolute',
                top: 0,
                left: '100%'
            });

            //click handler
            opener.bind('click', function (e) {
                e.preventDefault();
                page.hasClass('nav-active') ? hideMenu() : showMenu();
            });
            wrapper.on('click', function (e) {
                if (!jQuery(e.target).closest(opener).length && page.hasClass('nav-active')) hideMenu();
            });

            //recalc sizes on window width change
            win.bind('resize.Mobile orientationchange.Mobile', function () {
                if (opener.hasClass('active')) {
                    defWidth = navBlock.outerWidth();
                    navBlock.css({ marginLeft: -defWidth });
                    wrapper.css({ left: -defWidth });
                    if (options.fixedHeight) page.css({ height: Math.max(win.height(), navBlock.outerHeight()) });
                }
            });

            //show menu function
            function showMenu() {
                page.addClass('nav-active');
                //crop extra space if option fixedHeight is active
                if (options.fixedHeight) {
                    page.css({ height: Math.max(win.height(), navBlock.outerHeight()) });
                }
                defWidth = navBlock.outerWidth();

                //animate menu
                navBlock.stop().animate({
                    marginLeft: -defWidth
                }, animSpeed);
                wrapper.stop().animate({
                    left: -defWidth
                }, animSpeed);
                if (fixedPanels) {
                    fixedPanels.stop().animate({
                        marginLeft: -defWidth,
                        marginRight: defWidth
                    }, animSpeed);
                }
                if (options.fixedPosition) {
                    opener.parent().stop().animate({
                        marginLeft: -defWidth
                    }, animSpeed);
                    navBlock.css({
                        top: navBlock.offset().top,
                        position: 'absolute'
                    });
                    win.bind('scroll.Mobile', function () {
                        if (win.scrollTop() < navBlock.offset().top) {
                            navBlock.css('top', win.scrollTop());
                        }
                    });
                }
            }

            //hide menu function
            function hideMenu() {
                //animate menu
                navBlock.stop().animate({
                    marginLeft: 0
                }, animSpeed);
                wrapper.stop().animate({
                    left: 0
                }, {
                    duration: animSpeed,
                    complete: function () {
                        page.removeClass('nav-active');
                        //remove fixed height of page
                        if (options.fixedHeight) page.css('height', '');
                    }
                });
                if (fixedPanels) {
                    fixedPanels.stop().animate({
                        marginLeft: 0,
                        marginRight: 0
                    }, animSpeed);
                }
                if (options.fixedPosition) {
                    opener.parent().stop().animate({
                        marginLeft: 0
                    }, animSpeed);
                    navBlock.css({
                        top: '',
                        position: 'fixed'
                    });
                    win.unbind('scroll.Mobile');
                }
            }

            //enable swipe for menu opening on touch devices 
            if (options.useSwipe && isTouchDevice) {
                Hammer(page).on("swipeleft", hideMenu).on("swiperight", showMenu);
            }
        }
        //destroy mobile menu function
        function destroy() {
            win.unbind('resize.Mobile orientationchange.Mobile scroll.Mobile');
            wrapper.stop().removeAttr('style');
            navBlock.stop().removeAttr('style').appendTo(navBlock.data('parent'));
            jQuery('.temp-wrap > *:first-child').unwrap();
            opener.unbind('click');
        }
    });
}

/*
 * jQuery SlideShow plugin
 */
; (function ($) {
    function FadeGallery(options) {
        this.options = $.extend({
            slides: 'ul.slideset > li',
            activeClass: 'active',
            disabledClass: 'disabled',
            btnPrev: 'a.btn-prev',
            btnNext: 'a.btn-next',
            generatePagination: false,
            pagerList: '<ul>',
            pagerListItem: '<li><a href="#"></a></li>',
            pagerListItemText: 'a',
            pagerLinks: '.pagination li',
            currentNumber: 'span.current-num',
            totalNumber: 'span.total-num',
            btnPlay: '.btn-play',
            btnPause: '.btn-pause',
            btnPlayPause: '.btn-play-pause',
            galleryReadyClass: 'gallery-js-ready',
            autorotationActiveClass: 'autorotation-active',
            autorotationDisabledClass: 'autorotation-disabled',
            autorotationStopAfterClick: false,
            circularRotation: true,
            switchSimultaneously: true,
            disableWhileAnimating: false,
            disableFadeIE: false,
            autoRotation: false,
            pauseOnHover: true,
            autoHeight: false,
            useSwipe: false,
            swipeThreshold: 15,
            switchTime: 4000,
            animSpeed: 600,
            event: 'click'
        }, options);
        this.init();
    }
    FadeGallery.prototype = {
        init: function () {
            if (this.options.holder) {
                this.findElements();
                this.attachEvents();
                this.refreshState(true);
                this.autoRotate();
                this.makeCallback('onInit', this);
            }
        },
        findElements: function () {
            // control elements
            this.gallery = $(this.options.holder).addClass(this.options.galleryReadyClass);
            this.slides = this.gallery.find(this.options.slides);
            this.slidesHolder = this.slides.eq(0).parent();
            this.stepsCount = this.slides.length;
            this.btnPrev = this.gallery.find(this.options.btnPrev);
            this.btnNext = this.gallery.find(this.options.btnNext);
            this.currentIndex = 0;

            // disable fade effect in old IE
            if (this.options.disableFadeIE && !$.support.opacity) {
                this.options.animSpeed = 0;
            }

            // create gallery pagination
            if (typeof this.options.generatePagination === 'string') {
                this.pagerHolder = this.gallery.find(this.options.generatePagination).empty();
                this.pagerList = $(this.options.pagerList).appendTo(this.pagerHolder);
                for (var i = 0; i < this.stepsCount; i++) {
                    $(this.options.pagerListItem).appendTo(this.pagerList).find(this.options.pagerListItemText).text(i + 1);
                }
                this.pagerLinks = this.pagerList.children();
            } else {
                this.pagerLinks = this.gallery.find(this.options.pagerLinks);
            }

            // get start index
            var activeSlide = this.slides.filter('.' + this.options.activeClass);
            if (activeSlide.length) {
                this.currentIndex = this.slides.index(activeSlide);
            }
            this.prevIndex = this.currentIndex;

            // autorotation control buttons
            this.btnPlay = this.gallery.find(this.options.btnPlay);
            this.btnPause = this.gallery.find(this.options.btnPause);
            this.btnPlayPause = this.gallery.find(this.options.btnPlayPause);

            // misc elements
            this.curNum = this.gallery.find(this.options.currentNumber);
            this.allNum = this.gallery.find(this.options.totalNumber);

            // handle flexible layout
            this.slides.css({ display: 'block', opacity: 0 }).eq(this.currentIndex).css({
                opacity: ''
            });
        },
        attachEvents: function () {
            var self = this;

            // flexible layout handler
            this.resizeHandler = function () {
                self.onWindowResize();
            };
            $(window).bind('load resize orientationchange', this.resizeHandler);

            if (this.btnPrev.length) {
                this.btnPrevHandler = function (e) {
                    e.preventDefault();
                    self.prevSlide();
                    if (self.options.autorotationStopAfterClick) {
                        self.stopRotation();
                    }
                };
                this.btnPrev.bind(this.options.event, this.btnPrevHandler);
            }
            if (this.btnNext.length) {
                this.btnNextHandler = function (e) {
                    e.preventDefault();
                    self.nextSlide();
                    if (self.options.autorotationStopAfterClick) {
                        self.stopRotation();
                    }
                };
                this.btnNext.bind(this.options.event, this.btnNextHandler);
            }
            if (this.pagerLinks.length) {
                this.pagerLinksHandler = function (e) {
                    e.preventDefault();
                    self.numSlide(self.pagerLinks.index(e.currentTarget));
                    if (self.options.autorotationStopAfterClick) {
                        self.stopRotation();
                    }
                    console.log('xxx');
                };
                this.pagerLinks.bind(self.options.event, this.pagerLinksHandler);
            }

            if (this.pagerLinks.length) {
                this.pagerLinksHandlerX = function (e) {
                    e.preventDefault();
                    self.numSlide(0);
                    console.log(this);
                };
                this.pagerLinks.bind("mouseout", this.pagerLinksHandlerX);
            }

            // autorotation buttons handler
            if (this.btnPlay.length) {
                this.btnPlayHandler = function (e) {
                    e.preventDefault();
                    self.startRotation();
                };
                this.btnPlay.bind(this.options.event, this.btnPlayHandler);
            }
            if (this.btnPause.length) {
                this.btnPauseHandler = function (e) {
                    e.preventDefault();
                    self.stopRotation();
                };
                this.btnPause.bind(this.options.event, this.btnPauseHandler);
            }
            if (this.btnPlayPause.length) {
                this.btnPlayPauseHandler = function (e) {
                    e.preventDefault();
                    if (!self.gallery.hasClass(self.options.autorotationActiveClass)) {
                        self.startRotation();
                    } else {
                        self.stopRotation();
                    }
                };
                this.btnPlayPause.bind(this.options.event, this.btnPlayPauseHandler);
            }

            // swipe gestures handler
            if (this.options.useSwipe && window.Hammer && isTouchDevice) {
                this.swipeHandler = Hammer(this.gallery[0], {
                    dragBlockHorizontal: true,
                    dragMinDistance: 1
                }).on('release dragleft dragright', function (e) {
                    switch (e.type) {
                        case 'dragleft':
                        case 'dragright':
                            e.gesture.preventDefault();
                            break;
                        case 'release':
                            if (e.gesture.distance > self.options.swipeThreshold) {
                                if (e.gesture.direction === 'left') {
                                    self.nextSlide();
                                } else if (e.gesture.direction === 'right') {
                                    self.prevSlide();
                                }
                            }
                    }
                });
            }

            // pause on hover handling
            if (this.options.pauseOnHover) {
                this.hoverHandler = function () {
                    if (self.options.autoRotation) {
                        self.galleryHover = true;
                        self.pauseRotation();
                    }
                };
                this.leaveHandler = function () {
                    if (self.options.autoRotation) {
                        self.galleryHover = false;
                        self.resumeRotation();
                    }
                };
                this.gallery.bind({ mouseenter: this.hoverHandler, mouseleave: this.leaveHandler });
            }
        },
        onWindowResize: function () {
            if (this.options.autoHeight) {
                this.slidesHolder.css({ height: this.slides.eq(this.currentIndex).outerHeight(true) });
            }
        },
        prevSlide: function () {
            if (!(this.options.disableWhileAnimating && this.galleryAnimating)) {
                this.prevIndex = this.currentIndex;
                if (this.currentIndex > 0) {
                    this.currentIndex--;
                    this.switchSlide();
                } else if (this.options.circularRotation) {
                    this.currentIndex = this.stepsCount - 1;
                    this.switchSlide();
                }
            }
        },
        nextSlide: function (fromAutoRotation) {
            if (!(this.options.disableWhileAnimating && this.galleryAnimating)) {
                this.prevIndex = this.currentIndex;
                if (this.currentIndex < this.stepsCount - 1) {
                    this.currentIndex++;
                    this.switchSlide();
                } else if (this.options.circularRotation || fromAutoRotation === true) {
                    this.currentIndex = 0;
                    this.switchSlide();
                }
            }
        },
        numSlide: function (c) {
            if (this.currentIndex != c) {
                this.prevIndex = this.currentIndex;
                this.currentIndex = c;
                this.switchSlide();
            }
        },
        switchSlide: function () {
            var self = this;
            if (this.slides.length > 1) {
                this.galleryAnimating = true;
                if (!this.options.animSpeed) {
                    this.slides.eq(this.prevIndex).css({ opacity: 0 });
                } else {
                    this.slides.eq(this.prevIndex).stop().animate({ opacity: 0 }, { duration: this.options.animSpeed });
                }

                this.switchNext = function () {
                    if (!self.options.animSpeed) {
                        self.slides.eq(self.currentIndex).css({ opacity: '' });
                    } else {
                        self.slides.eq(self.currentIndex).stop().animate({ opacity: 1 }, { duration: self.options.animSpeed });
                    }
                    clearTimeout(this.nextTimer);
                    this.nextTimer = setTimeout(function () {
                        self.slides.eq(self.currentIndex).css({ opacity: '' });
                        self.galleryAnimating = false;
                        self.autoRotate();

                        // onchange callback
                        self.makeCallback('onChange', self);
                    }, self.options.animSpeed);
                };

                if (this.options.switchSimultaneously) {
                    self.switchNext();
                } else {
                    clearTimeout(this.switchTimer);
                    this.switchTimer = setTimeout(function () {
                        self.switchNext();
                    }, this.options.animSpeed);
                }
                this.refreshState();

                // onchange callback
                this.makeCallback('onBeforeChange', this);
            }
        },
        refreshState: function (initial) {
            this.slides.removeClass(this.options.activeClass).eq(this.currentIndex).addClass(this.options.activeClass);
            this.pagerLinks.removeClass(this.options.activeClass).eq(this.currentIndex).addClass(this.options.activeClass);
            this.curNum.html(this.currentIndex + 1);
            this.allNum.html(this.stepsCount);

            // initial refresh
            if (this.options.autoHeight) {
                if (initial) {
                    this.slidesHolder.css({ height: this.slides.eq(this.currentIndex).outerHeight(true) });
                } else {
                    this.slidesHolder.stop().animate({ height: this.slides.eq(this.currentIndex).outerHeight(true) }, { duration: this.options.animSpeed });
                }
            }

            // disabled state
            if (!this.options.circularRotation) {
                this.btnPrev.add(this.btnNext).removeClass(this.options.disabledClass);
                if (this.currentIndex === 0) this.btnPrev.addClass(this.options.disabledClass);
                if (this.currentIndex === this.stepsCount - 1) this.btnNext.addClass(this.options.disabledClass);
            }

            // add class if not enough slides
            this.gallery.toggleClass('not-enough-slides', this.stepsCount === 1);
        },
        startRotation: function () {
            this.options.autoRotation = true;
            this.galleryHover = false;
            this.autoRotationStopped = false;
            this.resumeRotation();
        },
        stopRotation: function () {
            this.galleryHover = true;
            this.autoRotationStopped = true;
            this.pauseRotation();
        },
        pauseRotation: function () {
            this.gallery.addClass(this.options.autorotationDisabledClass);
            this.gallery.removeClass(this.options.autorotationActiveClass);
            clearTimeout(this.timer);
        },
        resumeRotation: function () {
            if (!this.autoRotationStopped) {
                this.gallery.addClass(this.options.autorotationActiveClass);
                this.gallery.removeClass(this.options.autorotationDisabledClass);
                this.autoRotate();
            }
        },
        autoRotate: function () {
            var self = this;
            clearTimeout(this.timer);
            if (this.options.autoRotation && !this.galleryHover && !this.autoRotationStopped) {
                this.gallery.addClass(this.options.autorotationActiveClass);
                this.timer = setTimeout(function () {
                    self.nextSlide(true);
                }, this.options.switchTime);
            } else {
                this.pauseRotation();
            }
        },
        makeCallback: function (name) {
            if (typeof this.options[name] === 'function') {
                var args = Array.prototype.slice.call(arguments);
                args.shift();
                this.options[name].apply(this, args);
            }
        },
        destroy: function () {
            // navigation buttons handler
            this.btnPrev.unbind(this.options.event, this.btnPrevHandler);
            this.btnNext.unbind(this.options.event, this.btnNextHandler);
            this.pagerLinks.unbind(this.options.event, this.pagerLinksHandler);
            $(window).unbind('load resize orientationchange', this.resizeHandler);

            // remove autorotation handlers
            this.stopRotation();
            this.btnPlay.unbind(this.options.event, this.btnPlayHandler);
            this.btnPause.unbind(this.options.event, this.btnPauseHandler);
            this.btnPlayPause.unbind(this.options.event, this.btnPlayPauseHandler);
            this.gallery.bind({ mouseenter: this.hoverHandler, mouseleave: this.leaveHandler });

            // remove swipe handler if used
            if (this.swipeHandler) {
                this.swipeHandler.dispose();
            }
            if (typeof this.options.generatePagination === 'string') {
                this.pagerHolder.empty();
            }

            // remove unneeded classes and styles
            var unneededClasses = [this.options.galleryReadyClass, this.options.autorotationActiveClass, this.options.autorotationDisabledClass];
            this.gallery.removeClass(unneededClasses.join(' '));
            this.slidesHolder.add(this.slides).removeAttr('style');
        }
    };

    // detect device type
    var isTouchDevice = /MSIE 10.*Touch/.test(navigator.userAgent) || ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch;

    // jquery plugin
    $.fn.fadeGallery = function (opt) {
        return this.each(function () {
            $(this).data('FadeGallery', new FadeGallery($.extend(opt, { holder: this })));
        });
    };
}(jQuery));

/*! Hammer.JS - v1.1.3 - 2014-05-20
 * http://eightmedia.github.io/hammer.js
 *
 * Copyright (c) 2014 Jorik Tangelder <j.tangelder@gmail.com>;
 * Licensed under the MIT license */
!function (a, b) { "use strict"; function c() { d.READY || (s.determineEventTypes(), r.each(d.gestures, function (a) { u.register(a) }), s.onTouch(d.DOCUMENT, n, u.detect), s.onTouch(d.DOCUMENT, o, u.detect), d.READY = !0) } var d = function v(a, b) { return new v.Instance(a, b || {}) }; d.VERSION = "1.1.3", d.defaults = { behavior: { userSelect: "none", touchAction: "pan-y", touchCallout: "none", contentZooming: "none", userDrag: "none", tapHighlightColor: "rgba(0,0,0,0)" } }, d.DOCUMENT = document, d.HAS_POINTEREVENTS = navigator.pointerEnabled || navigator.msPointerEnabled, d.HAS_TOUCHEVENTS = "ontouchstart" in a, d.IS_MOBILE = /mobile|tablet|ip(ad|hone|od)|android|silk/i.test(navigator.userAgent), d.NO_MOUSEEVENTS = d.HAS_TOUCHEVENTS && d.IS_MOBILE || d.HAS_POINTEREVENTS, d.CALCULATE_INTERVAL = 25; var e = {}, f = d.DIRECTION_DOWN = "down", g = d.DIRECTION_LEFT = "left", h = d.DIRECTION_UP = "up", i = d.DIRECTION_RIGHT = "right", j = d.POINTER_MOUSE = "mouse", k = d.POINTER_TOUCH = "touch", l = d.POINTER_PEN = "pen", m = d.EVENT_START = "start", n = d.EVENT_MOVE = "move", o = d.EVENT_END = "end", p = d.EVENT_RELEASE = "release", q = d.EVENT_TOUCH = "touch"; d.READY = !1, d.plugins = d.plugins || {}, d.gestures = d.gestures || {}; var r = d.utils = { extend: function (a, c, d) { for (var e in c) !c.hasOwnProperty(e) || a[e] !== b && d || (a[e] = c[e]); return a }, on: function (a, b, c) { a.addEventListener(b, c, !1) }, off: function (a, b, c) { a.removeEventListener(b, c, !1) }, each: function (a, c, d) { var e, f; if ("forEach" in a) a.forEach(c, d); else if (a.length !== b) { for (e = 0, f = a.length; f > e; e++) if (c.call(d, a[e], e, a) === !1) return } else for (e in a) if (a.hasOwnProperty(e) && c.call(d, a[e], e, a) === !1) return }, inStr: function (a, b) { return a.indexOf(b) > -1 }, inArray: function (a, b) { if (a.indexOf) { var c = a.indexOf(b); return -1 === c ? !1 : c } for (var d = 0, e = a.length; e > d; d++) if (a[d] === b) return d; return !1 }, toArray: function (a) { return Array.prototype.slice.call(a, 0) }, hasParent: function (a, b) { for (; a;) { if (a == b) return !0; a = a.parentNode } return !1 }, getCenter: function (a) { var b = [], c = [], d = [], e = [], f = Math.min, g = Math.max; return 1 === a.length ? { pageX: a[0].pageX, pageY: a[0].pageY, clientX: a[0].clientX, clientY: a[0].clientY } : (r.each(a, function (a) { b.push(a.pageX), c.push(a.pageY), d.push(a.clientX), e.push(a.clientY) }), { pageX: (f.apply(Math, b) + g.apply(Math, b)) / 2, pageY: (f.apply(Math, c) + g.apply(Math, c)) / 2, clientX: (f.apply(Math, d) + g.apply(Math, d)) / 2, clientY: (f.apply(Math, e) + g.apply(Math, e)) / 2 }) }, getVelocity: function (a, b, c) { return { x: Math.abs(b / a) || 0, y: Math.abs(c / a) || 0 } }, getAngle: function (a, b) { var c = b.clientX - a.clientX, d = b.clientY - a.clientY; return 180 * Math.atan2(d, c) / Math.PI }, getDirection: function (a, b) { var c = Math.abs(a.clientX - b.clientX), d = Math.abs(a.clientY - b.clientY); return c >= d ? a.clientX - b.clientX > 0 ? g : i : a.clientY - b.clientY > 0 ? h : f }, getDistance: function (a, b) { var c = b.clientX - a.clientX, d = b.clientY - a.clientY; return Math.sqrt(c * c + d * d) }, getScale: function (a, b) { return a.length >= 2 && b.length >= 2 ? this.getDistance(b[0], b[1]) / this.getDistance(a[0], a[1]) : 1 }, getRotation: function (a, b) { return a.length >= 2 && b.length >= 2 ? this.getAngle(b[1], b[0]) - this.getAngle(a[1], a[0]) : 0 }, isVertical: function (a) { return a == h || a == f }, setPrefixedCss: function (a, b, c, d) { var e = ["", "Webkit", "Moz", "O", "ms"]; b = r.toCamelCase(b); for (var f = 0; f < e.length; f++) { var g = b; if (e[f] && (g = e[f] + g.slice(0, 1).toUpperCase() + g.slice(1)), g in a.style) { a.style[g] = (null == d || d) && c || ""; break } } }, toggleBehavior: function (a, b, c) { if (b && a && a.style) { r.each(b, function (b, d) { r.setPrefixedCss(a, d, b, c) }); var d = c && function () { return !1 }; "none" == b.userSelect && (a.onselectstart = d), "none" == b.userDrag && (a.ondragstart = d) } }, toCamelCase: function (a) { return a.replace(/[_-]([a-z])/g, function (a) { return a[1].toUpperCase() }) } }, s = d.event = { preventMouseEvents: !1, started: !1, shouldDetect: !1, on: function (a, b, c, d) { var e = b.split(" "); r.each(e, function (b) { r.on(a, b, c), d && d(b) }) }, off: function (a, b, c, d) { var e = b.split(" "); r.each(e, function (b) { r.off(a, b, c), d && d(b) }) }, onTouch: function (a, b, c) { var f = this, g = function (e) { var g, h = e.type.toLowerCase(), i = d.HAS_POINTEREVENTS, j = r.inStr(h, "mouse"); j && f.preventMouseEvents || (j && b == m && 0 === e.button ? (f.preventMouseEvents = !1, f.shouldDetect = !0) : i && b == m ? f.shouldDetect = 1 === e.buttons || t.matchType(k, e) : j || b != m || (f.preventMouseEvents = !0, f.shouldDetect = !0), i && b != o && t.updatePointer(b, e), f.shouldDetect && (g = f.doDetect.call(f, e, b, a, c)), g == o && (f.preventMouseEvents = !1, f.shouldDetect = !1, t.reset()), i && b == o && t.updatePointer(b, e)) }; return this.on(a, e[b], g), g }, doDetect: function (a, b, c, d) { var e = this.getTouchList(a, b), f = e.length, g = b, h = e.trigger, i = f; b == m ? h = q : b == o && (h = p, i = e.length - (a.changedTouches ? a.changedTouches.length : 1)), i > 0 && this.started && (g = n), this.started = !0; var j = this.collectEventData(c, g, e, a); return b != o && d.call(u, j), h && (j.changedLength = i, j.eventType = h, d.call(u, j), j.eventType = g, delete j.changedLength), g == o && (d.call(u, j), this.started = !1), g }, determineEventTypes: function () { var b; return b = d.HAS_POINTEREVENTS ? a.PointerEvent ? ["pointerdown", "pointermove", "pointerup pointercancel lostpointercapture"] : ["MSPointerDown", "MSPointerMove", "MSPointerUp MSPointerCancel MSLostPointerCapture"] : d.NO_MOUSEEVENTS ? ["touchstart", "touchmove", "touchend touchcancel"] : ["touchstart mousedown", "touchmove mousemove", "touchend touchcancel mouseup"], e[m] = b[0], e[n] = b[1], e[o] = b[2], e }, getTouchList: function (a, b) { if (d.HAS_POINTEREVENTS && !(navigator.msPointerEnabled && !navigator.pointerEnabled)) return t.getTouchList(); if (a.touches) { if (b == n) return a.touches; var c = [], e = [].concat(r.toArray(a.touches), r.toArray(a.changedTouches)), f = []; return r.each(e, function (a) { r.inArray(c, a.identifier) === !1 && f.push(a), c.push(a.identifier) }), f } return a.identifier = 1, [a] }, collectEventData: function (a, b, c, d) { var e = k; return r.inStr(d.type, "mouse") || t.matchType(j, d) ? e = j : t.matchType(l, d) && (e = l), { center: r.getCenter(c), timeStamp: Date.now(), target: d.target, touches: c, eventType: b, pointerType: e, srcEvent: d, preventDefault: function () { var a = this.srcEvent; a.preventManipulation && a.preventManipulation(), a.preventDefault && a.preventDefault() }, stopPropagation: function () { this.srcEvent.stopPropagation() }, stopDetect: function () { return u.stopDetect() } } } }, t = d.PointerEvent = { pointers: {}, getTouchList: function () { var a = []; return r.each(this.pointers, function (b) { a.push(b) }), a }, updatePointer: function (a, b) { a == o || a != o && 1 !== b.buttons ? delete this.pointers[b.pointerId] : (b.identifier = b.pointerId, this.pointers[b.pointerId] = b) }, matchType: function (a, b) { if (!b.pointerType) return !1; var c = b.pointerType, d = {}; return d[j] = c === (b.MSPOINTER_TYPE_MOUSE || j), d[k] = c === (b.MSPOINTER_TYPE_TOUCH || k), d[l] = c === (b.MSPOINTER_TYPE_PEN || l), d[a] }, reset: function () { this.pointers = {} } }, u = d.detection = { gestures: [], current: null, previous: null, stopped: !1, startDetect: function (a, b) { this.current || (this.stopped = !1, this.current = { inst: a, startEvent: r.extend({}, b), lastEvent: !1, lastCalcEvent: !1, futureCalcEvent: !1, lastCalcData: {}, name: "" }, this.detect(b)) }, detect: function (a) { if (this.current && !this.stopped) { a = this.extendEventData(a); var b = this.current.inst, c = b.options; return r.each(this.gestures, function (d) { !this.stopped && b.enabled && c[d.name] && d.handler.call(d, a, b) }, this), this.current && (this.current.lastEvent = a), a.eventType == o && this.stopDetect(), a } }, stopDetect: function () { this.previous = r.extend({}, this.current), this.current = null, this.stopped = !0 }, getCalculatedData: function (a, b, c, e, f) { var g = this.current, h = !1, i = g.lastCalcEvent, j = g.lastCalcData; i && a.timeStamp - i.timeStamp > d.CALCULATE_INTERVAL && (b = i.center, c = a.timeStamp - i.timeStamp, e = a.center.clientX - i.center.clientX, f = a.center.clientY - i.center.clientY, h = !0), (a.eventType == q || a.eventType == p) && (g.futureCalcEvent = a), (!g.lastCalcEvent || h) && (j.velocity = r.getVelocity(c, e, f), j.angle = r.getAngle(b, a.center), j.direction = r.getDirection(b, a.center), g.lastCalcEvent = g.futureCalcEvent || a, g.futureCalcEvent = a), a.velocityX = j.velocity.x, a.velocityY = j.velocity.y, a.interimAngle = j.angle, a.interimDirection = j.direction }, extendEventData: function (a) { var b = this.current, c = b.startEvent, d = b.lastEvent || c; (a.eventType == q || a.eventType == p) && (c.touches = [], r.each(a.touches, function (a) { c.touches.push({ clientX: a.clientX, clientY: a.clientY }) })); var e = a.timeStamp - c.timeStamp, f = a.center.clientX - c.center.clientX, g = a.center.clientY - c.center.clientY; return this.getCalculatedData(a, d.center, e, f, g), r.extend(a, { startEvent: c, deltaTime: e, deltaX: f, deltaY: g, distance: r.getDistance(c.center, a.center), angle: r.getAngle(c.center, a.center), direction: r.getDirection(c.center, a.center), scale: r.getScale(c.touches, a.touches), rotation: r.getRotation(c.touches, a.touches) }), a }, register: function (a) { var c = a.defaults || {}; return c[a.name] === b && (c[a.name] = !0), r.extend(d.defaults, c, !0), a.index = a.index || 1e3, this.gestures.push(a), this.gestures.sort(function (a, b) { return a.index < b.index ? -1 : a.index > b.index ? 1 : 0 }), this.gestures } }; d.Instance = function (a, b) { var e = this; c(), this.element = a, this.enabled = !0, r.each(b, function (a, c) { delete b[c], b[r.toCamelCase(c)] = a }), this.options = r.extend(r.extend({}, d.defaults), b || {}), this.options.behavior && r.toggleBehavior(this.element, this.options.behavior, !0), this.eventStartHandler = s.onTouch(a, m, function (a) { e.enabled && a.eventType == m ? u.startDetect(e, a) : a.eventType == q && u.detect(a) }), this.eventHandlers = [] }, d.Instance.prototype = { on: function (a, b) { var c = this; return s.on(c.element, a, b, function (a) { c.eventHandlers.push({ gesture: a, handler: b }) }), c }, off: function (a, b) { var c = this; return s.off(c.element, a, b, function (a) { var d = r.inArray({ gesture: a, handler: b }); d !== !1 && c.eventHandlers.splice(d, 1) }), c }, trigger: function (a, b) { b || (b = {}); var c = d.DOCUMENT.createEvent("Event"); c.initEvent(a, !0, !0), c.gesture = b; var e = this.element; return r.hasParent(b.target, e) && (e = b.target), e.dispatchEvent(c), this }, enable: function (a) { return this.enabled = a, this }, dispose: function () { var a, b; for (r.toggleBehavior(this.element, this.options.behavior, !1), a = -1; b = this.eventHandlers[++a];) r.off(this.element, b.gesture, b.handler); return this.eventHandlers = [], s.off(this.element, e[m], this.eventStartHandler), null } }, function (a) { function b(b, d) { var e = u.current; if (!(d.options.dragMaxTouches > 0 && b.touches.length > d.options.dragMaxTouches)) switch (b.eventType) { case m: c = !1; break; case n: if (b.distance < d.options.dragMinDistance && e.name != a) return; var j = e.startEvent.center; if (e.name != a && (e.name = a, d.options.dragDistanceCorrection && b.distance > 0)) { var k = Math.abs(d.options.dragMinDistance / b.distance); j.pageX += b.deltaX * k, j.pageY += b.deltaY * k, j.clientX += b.deltaX * k, j.clientY += b.deltaY * k, b = u.extendEventData(b) } (e.lastEvent.dragLockToAxis || d.options.dragLockToAxis && d.options.dragLockMinDistance <= b.distance) && (b.dragLockToAxis = !0); var l = e.lastEvent.direction; b.dragLockToAxis && l !== b.direction && (b.direction = r.isVertical(l) ? b.deltaY < 0 ? h : f : b.deltaX < 0 ? g : i), c || (d.trigger(a + "start", b), c = !0), d.trigger(a, b), d.trigger(a + b.direction, b); var q = r.isVertical(b.direction); (d.options.dragBlockVertical && q || d.options.dragBlockHorizontal && !q) && b.preventDefault(); break; case p: c && b.changedLength <= d.options.dragMaxTouches && (d.trigger(a + "end", b), c = !1); break; case o: c = !1 } } var c = !1; d.gestures.Drag = { name: a, index: 50, handler: b, defaults: { dragMinDistance: 10, dragDistanceCorrection: !0, dragMaxTouches: 1, dragBlockHorizontal: !1, dragBlockVertical: !1, dragLockToAxis: !1, dragLockMinDistance: 25 } } }("drag"), d.gestures.Gesture = { name: "gesture", index: 1337, handler: function (a, b) { b.trigger(this.name, a) } }, function (a) { function b(b, d) { var e = d.options, f = u.current; switch (b.eventType) { case m: clearTimeout(c), f.name = a, c = setTimeout(function () { f && f.name == a && d.trigger(a, b) }, e.holdTimeout); break; case n: b.distance > e.holdThreshold && clearTimeout(c); break; case p: clearTimeout(c) } } var c; d.gestures.Hold = { name: a, index: 10, defaults: { holdTimeout: 500, holdThreshold: 2 }, handler: b } }("hold"), d.gestures.Release = { name: "release", index: 1 / 0, handler: function (a, b) { a.eventType == p && b.trigger(this.name, a) } }, d.gestures.Swipe = { name: "swipe", index: 40, defaults: { swipeMinTouches: 1, swipeMaxTouches: 1, swipeVelocityX: .6, swipeVelocityY: .6 }, handler: function (a, b) { if (a.eventType == p) { var c = a.touches.length, d = b.options; if (c < d.swipeMinTouches || c > d.swipeMaxTouches) return; (a.velocityX > d.swipeVelocityX || a.velocityY > d.swipeVelocityY) && (b.trigger(this.name, a), b.trigger(this.name + a.direction, a)) } } }, function (a) { function b(b, d) { var e, f, g = d.options, h = u.current, i = u.previous; switch (b.eventType) { case m: c = !1; break; case n: c = c || b.distance > g.tapMaxDistance; break; case o: !r.inStr(b.srcEvent.type, "cancel") && b.deltaTime < g.tapMaxTime && !c && (e = i && i.lastEvent && b.timeStamp - i.lastEvent.timeStamp, f = !1, i && i.name == a && e && e < g.doubleTapInterval && b.distance < g.doubleTapDistance && (d.trigger("doubletap", b), f = !0), (!f || g.tapAlways) && (h.name = a, d.trigger(h.name, b))) } } var c = !1; d.gestures.Tap = { name: a, index: 100, handler: b, defaults: { tapMaxTime: 250, tapMaxDistance: 10, tapAlways: !0, doubleTapDistance: 20, doubleTapInterval: 300 } } }("tap"), d.gestures.Touch = { name: "touch", index: -1 / 0, defaults: { preventDefault: !1, preventMouse: !1 }, handler: function (a, b) { return b.options.preventMouse && a.pointerType == j ? void a.stopDetect() : (b.options.preventDefault && a.preventDefault(), void (a.eventType == q && b.trigger("touch", a))) } }, function (a) { function b(b, d) { switch (b.eventType) { case m: c = !1; break; case n: if (b.touches.length < 2) return; var e = Math.abs(1 - b.scale), f = Math.abs(b.rotation); if (e < d.options.transformMinScale && f < d.options.transformMinRotation) return; u.current.name = a, c || (d.trigger(a + "start", b), c = !0), d.trigger(a, b), f > d.options.transformMinRotation && d.trigger("rotate", b), e > d.options.transformMinScale && (d.trigger("pinch", b), d.trigger("pinch" + (b.scale < 1 ? "in" : "out"), b)); break; case p: c && b.changedLength < 2 && (d.trigger(a + "end", b), c = !1) } } var c = !1; d.gestures.Transform = { name: a, index: 45, defaults: { transformMinScale: .01, transformMinRotation: 1 }, handler: b } }("transform"), "function" == typeof define && define.amd ? define(function () { return d }) : "undefined" != typeof module && module.exports ? module.exports = d : a.Hammer = d }(window);

/*
 * Responsive Layout helper
 */
ResponsiveHelper = (function (d) { var c = [], h, e = d(window), f = false; if (window.matchMedia) { if (window.Window && window.matchMedia === Window.prototype.matchMedia) { f = true } else { if (window.matchMedia.toString().indexOf("native") > -1) { f = true } } } function b() { var j = e.width(); if (j !== h) { h = j; d.each(c, function (k, l) { d.each(l.data, function (n, m) { if (m.currentActive && !g(m.range[0], m.range[1])) { m.currentActive = false; if (typeof m.disableCallback === "function") { m.disableCallback() } } }); d.each(l.data, function (n, m) { if (!m.currentActive && g(m.range[0], m.range[1])) { m.currentActive = true; if (typeof m.enableCallback === "function") { m.enableCallback() } } }) }) } } e.bind("load resize orientationchange", b); function g(k, j) { var l = ""; if (k > 0) { l += "(min-width: " + k + "px)" } if (j < Infinity) { l += (l ? " and " : "") + "(max-width: " + j + "px)" } return i(l, k, j) } function i(l, k, j) { if (window.matchMedia && f) { return matchMedia(l).matches } else { if (window.styleMedia) { return styleMedia.matchMedium(l) } else { if (window.media) { return media.matchMedium(l) } else { return h >= k && h <= j } } } } function a(m) { var j = m.split(".."); var l = parseInt(j[0], 10) || -Infinity; var k = parseInt(j[1], 10) || Infinity; return [l, k].sort(function (o, n) { return o - n }) } return { addRange: function (k) { var j = { data: {} }; d.each(k, function (m, l) { j.data[m] = { range: a(m), enableCallback: l.on, disableCallback: l.off } }); c.push(j); h = null; b() } } }(jQuery));

/*!
* FitVids 1.0.3
*
* Copyright 2013, Chris Coyier - http://css-tricks.com + Dave Rupert - http://daverupert.com
* Credit to Thierry Koblentz - http://www.alistapart.com/articles/creating-intrinsic-ratios-for-video/
* Released under the WTFPL license - http://sam.zoy.org/wtfpl/
*
* Date: Thu Sept 01 18:00:00 2011 -0500
*/
; (function (a) { a.fn.fitVids = function (b) { var c = { customSelector: null }; if (!document.getElementById("fit-vids-style")) { var f = document.createElement("div"), d = document.getElementsByTagName("base")[0] || document.getElementsByTagName("script")[0], e = "&shy;<style>.fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}</style>"; f.className = "fit-vids-style"; f.id = "fit-vids-style"; f.style.display = "none"; f.innerHTML = e; d.parentNode.insertBefore(f, d) } if (b) { a.extend(c, b) } return this.each(function () { var g = ["iframe[src*='https://www.plainsallamerican.com/CMSPages/player.vimeo.com']", "iframe[src*='https://www.plainsallamerican.com/CMSPages/youtube.com']", "iframe[src*='https://www.plainsallamerican.com/CMSPages/youtube-nocookie.com']", "iframe[src*='https://www.plainsallamerican.com/CMSPages/kickstarter.com'][src*='https://www.plainsallamerican.com/CMSPages/video.html']", "object", "embed"]; if (c.customSelector) { g.push(c.customSelector) } var h = a(this).find(g.join(",")); h = h.not("object object"); h.each(function () { var m = a(this); if (this.tagName.toLowerCase() === "embed" && m.parent("object").length || m.parent(".fluid-width-video-wrapper").length) { return } var i = (this.tagName.toLowerCase() === "object" || (m.attr("height") && !isNaN(parseInt(m.attr("height"), 10)))) ? parseInt(m.attr("height"), 10) : m.height(), j = !isNaN(parseInt(m.attr("width"), 10)) ? parseInt(m.attr("width"), 10) : m.width(), k = i / j; if (!m.attr("id")) { var l = "fitvid" + Math.floor(Math.random() * 999999); m.attr("id", l) } m.wrap('<div class="fluid-width-video-wrapper"></div>').parent(".fluid-width-video-wrapper").css("padding-top", (k * 100) + "%"); m.removeAttr("height").removeAttr("width") }) }) } })(window.jQuery || window.Zepto);

/*
 *	jQuery dotdotdot 1.6.16
 *
 *	Copyright (c) Fred Heusschen
 *	www.frebsite.nl
 *
 *	Plugin website:
 *	dotdotdot.frebsite.nl
 *
 *	Dual licensed under the MIT and GPL licenses.
 *	http://en.wikipedia.org/wiki/MIT_License
 *	http://en.wikipedia.org/wiki/GNU_General_Public_License
 */
!function (t, e) { function n(t, e, n) { var r = t.children(), o = !1; t.empty(); for (var i = 0, d = r.length; d > i; i++) { var l = r.eq(i); if (t.append(l), n && t.append(n), a(t, e)) { l.remove(), o = !0; break } n && n.detach() } return o } function r(e, n, i, d, l) { var s = !1, c = "table, thead, tbody, tfoot, tr, col, colgroup, object, embed, param, ol, ul, dl, blockquote, select, optgroup, option, textarea, script, style", u = "script, .dotdotdot-keep"; return e.contents().detach().each(function () { var f = this, h = t(f); if ("undefined" == typeof f || 3 == f.nodeType && 0 == t.trim(f.data).length) return !0; if (h.is(u)) e.append(h); else { if (s) return !0; e.append(h), l && e[e.is(c) ? "after" : "append"](l), a(i, d) && (s = 3 == f.nodeType ? o(h, n, i, d, l) : r(h, n, i, d, l), s || (h.detach(), s = !0)), s || l && l.detach() } }), s } function o(e, n, r, o, d) { var c = e[0]; if (!c) return !1; var f = s(c), h = -1 !== f.indexOf(" ") ? " " : "　", p = "letter" == o.wrap ? "" : h, g = f.split(p), v = -1, w = -1, b = 0, y = g.length - 1; for (o.fallbackToLetter && 0 == b && 0 == y && (p = "", g = f.split(p), y = g.length - 1) ; y >= b && (0 != b || 0 != y) ;) { var m = Math.floor((b + y) / 2); if (m == w) break; w = m, l(c, g.slice(0, w + 1).join(p) + o.ellipsis), a(r, o) ? (y = w, o.fallbackToLetter && 0 == b && 0 == y && (p = "", g = g[0].split(p), v = -1, w = -1, b = 0, y = g.length - 1)) : (v = w, b = w) } if (-1 == v || 1 == g.length && 0 == g[0].length) { var x = e.parent(); e.detach(); var T = d && d.closest(x).length ? d.length : 0; x.contents().length > T ? c = u(x.contents().eq(-1 - T), n) : (c = u(x, n, !0), T || x.detach()), c && (f = i(s(c), o), l(c, f), T && d && t(c).parent().append(d)) } else f = i(g.slice(0, v + 1).join(p), o), l(c, f); return !0 } function a(t, e) { return t.innerHeight() > e.maxHeight } function i(e, n) { for (; t.inArray(e.slice(-1), n.lastCharacter.remove) > -1;) e = e.slice(0, -1); return t.inArray(e.slice(-1), n.lastCharacter.noEllipsis) < 0 && (e += n.ellipsis), e } function d(t) { return { width: t.innerWidth(), height: t.innerHeight() } } function l(t, e) { t.innerText ? t.innerText = e : t.nodeValue ? t.nodeValue = e : t.textContent && (t.textContent = e) } function s(t) { return t.innerText ? t.innerText : t.nodeValue ? t.nodeValue : t.textContent ? t.textContent : "" } function c(t) { do t = t.previousSibling; while (t && 1 !== t.nodeType && 3 !== t.nodeType); return t } function u(e, n, r) { var o, a = e && e[0]; if (a) { if (!r) { if (3 === a.nodeType) return a; if (t.trim(e.text())) return u(e.contents().last(), n) } for (o = c(a) ; !o;) { if (e = e.parent(), e.is(n) || !e.length) return !1; o = c(e[0]) } if (o) return u(t(o), n) } return !1 } function f(e, n) { return e ? "string" == typeof e ? (e = t(e, n), e.length ? e : !1) : e.jquery ? e : !1 : !1 } function h(t) { for (var e = t.innerHeight(), n = ["paddingTop", "paddingBottom"], r = 0, o = n.length; o > r; r++) { var a = parseInt(t.css(n[r]), 10); isNaN(a) && (a = 0), e -= a } return e } if (!t.fn.dotdotdot) { t.fn.dotdotdot = function (e) { if (0 == this.length) return t.fn.dotdotdot.debug('No element found for "' + this.selector + '".'), this; if (this.length > 1) return this.each(function () { t(this).dotdotdot(e) }); var o = this; o.data("dotdotdot") && o.trigger("https://www.plainsallamerican.com/CMSPages/destroy.dot"), o.data("dotdotdot-style", o.attr("style") || ""), o.css("word-wrap", "break-word"), "nowrap" === o.css("white-space") && o.css("white-space", "normal"), o.bind_events = function () { return o.bind("https://www.plainsallamerican.com/CMSPages/update.dot", function (e, d) { e.preventDefault(), e.stopPropagation(), l.maxHeight = "number" == typeof l.height ? l.height : h(o), l.maxHeight += l.tolerance, "undefined" != typeof d && (("string" == typeof d || d instanceof HTMLElement) && (d = t("<div />").append(d).contents()), d instanceof t && (i = d)), g = o.wrapInner('<div class="dotdotdot" />').children(), g.contents().detach().end().append(i.clone(!0)).find("br").replaceWith("  <br />  ").end().css({ height: "auto", width: "auto", border: "none", padding: 0, margin: 0 }); var c = !1, u = !1; return s.afterElement && (c = s.afterElement.clone(!0), c.show(), s.afterElement.detach()), a(g, l) && (u = "children" == l.wrap ? n(g, l, c) : r(g, o, g, l, c)), g.replaceWith(g.contents()), g = null, t.isFunction(l.callback) && l.callback.call(o[0], u, i), s.isTruncated = u, u }).bind("https://www.plainsallamerican.com/CMSPages/isTruncated.dot", function (t, e) { return t.preventDefault(), t.stopPropagation(), "function" == typeof e && e.call(o[0], s.isTruncated), s.isTruncated }).bind("https://www.plainsallamerican.com/CMSPages/originalContent.dot", function (t, e) { return t.preventDefault(), t.stopPropagation(), "function" == typeof e && e.call(o[0], i), i }).bind("https://www.plainsallamerican.com/CMSPages/destroy.dot", function (t) { t.preventDefault(), t.stopPropagation(), o.unwatch().unbind_events().contents().detach().end().append(i).attr("style", o.data("dotdotdot-style") || "").data("dotdotdot", !1) }), o }, o.unbind_events = function () { return o.unbind(".dot"), o }, o.watch = function () { if (o.unwatch(), "window" == l.watch) { var e = t(window), n = e.width(), r = e.height(); e.bind("resize.dot" + s.dotId, function () { n == e.width() && r == e.height() && l.windowResizeFix || (n = e.width(), r = e.height(), u && clearInterval(u), u = setTimeout(function () { o.trigger("https://www.plainsallamerican.com/CMSPages/update.dot") }, 100)) }) } else c = d(o), u = setInterval(function () { if (o.is(":visible")) { var t = d(o); (c.width != t.width || c.height != t.height) && (o.trigger("https://www.plainsallamerican.com/CMSPages/update.dot"), c = t) } }, 500); return o }, o.unwatch = function () { return t(window).unbind("resize.dot" + s.dotId), u && clearInterval(u), o }; var i = o.contents(), l = t.extend(!0, {}, t.fn.dotdotdot.defaults, e), s = {}, c = {}, u = null, g = null; return l.lastCharacter.remove instanceof Array || (l.lastCharacter.remove = t.fn.dotdotdot.defaultArrays.lastCharacter.remove), l.lastCharacter.noEllipsis instanceof Array || (l.lastCharacter.noEllipsis = t.fn.dotdotdot.defaultArrays.lastCharacter.noEllipsis), s.afterElement = f(l.after, o), s.isTruncated = !1, s.dotId = p++, o.data("dotdotdot", !0).bind_events().trigger("https://www.plainsallamerican.com/CMSPages/update.dot"), l.watch && o.watch(), o }, t.fn.dotdotdot.defaults = { ellipsis: "... ", wrap: "word", fallbackToLetter: !0, lastCharacter: {}, tolerance: 0, callback: null, after: null, height: null, watch: !1, windowResizeFix: !0 }, t.fn.dotdotdot.defaultArrays = { lastCharacter: { remove: [" ", "　", ",", ";", ".", "!", "?"], noEllipsis: [] } }, t.fn.dotdotdot.debug = function () { }; var p = 1, g = t.fn.html; t.fn.html = function (n) { return n != e && !t.isFunction(n) && this.data("dotdotdot") ? this.trigger("update", [n]) : g.apply(this, arguments) }; var v = t.fn.text; t.fn.text = function (n) { return n != e && !t.isFunction(n) && this.data("dotdotdot") ? (n = t("<div />").text(n).html(), this.trigger("update", [n])) : v.apply(this, arguments) } } }(jQuery);
  
 /**
* jquery.matchHeight-min.js v0.5.2
* http://brm.io/jquery-match-height/
* License: MIT
*/
(function(c){var n=-1,f=-1,r=function(a){var b=null,d=[];c(a).each(function(){var a=c(this),k=a.offset().top-h(a.css("margin-top")),l=0<d.length?d[d.length-1]:null;null===l?d.push(a):1>=Math.floor(Math.abs(b-k))?d[d.length-1]=l.add(a):d.push(a);b=k});return d},h=function(a){return parseFloat(a)||0},p=function(a){var b={byRow:!0,remove:!1,property:"height"};if("object"===typeof a)return c.extend(b,a);"boolean"===typeof a?b.byRow=a:"remove"===a&&(b.remove=!0);return b},b=c.fn.matchHeight=function(a){a=
p(a);if(a.remove){var e=this;this.css(a.property,"");c.each(b._groups,function(a,b){b.elements=b.elements.not(e)});return this}if(1>=this.length)return this;b._groups.push({elements:this,options:a});b._apply(this,a);return this};b._groups=[];b._throttle=80;b._maintainScroll=!1;b._beforeUpdate=null;b._afterUpdate=null;b._apply=function(a,e){var d=p(e),g=c(a),k=[g],l=c(window).scrollTop(),f=c("html").outerHeight(!0),m=g.parents().filter(":hidden");m.each(function(){var a=c(this);a.data("style-cache",
a.attr("style"))});m.css("display","block");d.byRow&&(g.each(function(){var a=c(this),b="inline-block"===a.css("display")?"inline-block":"block";a.data("style-cache",a.attr("style"));a.css({display:b,"padding-top":"0","padding-bottom":"0","margin-top":"0","margin-bottom":"0","border-top-width":"0","border-bottom-width":"0",height:"100px"})}),k=r(g),g.each(function(){var a=c(this);a.attr("style",a.data("style-cache")||"")}));c.each(k,function(a,b){var e=c(b),f=0;d.byRow&&1>=e.length?e.css(d.property,
""):(e.each(function(){var a=c(this),b={display:"inline-block"===a.css("display")?"inline-block":"block"};b[d.property]="";a.css(b);a.outerHeight(!1)>f&&(f=a.outerHeight(!1));a.css("display","")}),e.each(function(){var a=c(this),b=0;"border-box"!==a.css("box-sizing")&&(b+=h(a.css("border-top-width"))+h(a.css("border-bottom-width")),b+=h(a.css("padding-top"))+h(a.css("padding-bottom")));a.css(d.property,f-b)}))});m.each(function(){var a=c(this);a.attr("style",a.data("style-cache")||null)});b._maintainScroll&&
c(window).scrollTop(l/f*c("html").outerHeight(!0));return this};b._applyDataApi=function(){var a={};c("[data-match-height], [data-mh]").each(function(){var b=c(this),d=b.attr("data-match-height")||b.attr("data-mh");a[d]=d in a?a[d].add(b):b});c.each(a,function(){this.matchHeight(!0)})};var q=function(a){b._beforeUpdate&&b._beforeUpdate(a,b._groups);c.each(b._groups,function(){b._apply(this.elements,this.options)});b._afterUpdate&&b._afterUpdate(a,b._groups)};b._update=function(a,e){if(e&&"resize"===
e.type){var d=c(window).width();if(d===n)return;n=d}a?-1===f&&(f=setTimeout(function(){q(e);f=-1},b._throttle)):q(e)};c(b._applyDataApi);c(window).bind("load",function(a){b._update(!1,a)});c(window).bind("resize orientationchange",function(a){b._update(!0,a)})})(jQuery);
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
/*!
 * hoverIntent v1.8.1 // 2014.08.11 // jQuery v1.9.1+
 * http://cherne.net/brian/resources/jquery.hoverIntent.html
 *
 * You may use hoverIntent under the terms of the MIT license. Basically that
 * means you are free to use hoverIntent as long as this header is left intact.
 * Copyright 2007, 2014 Brian Cherne
 */

/* hoverIntent is similar to jQuery's built-in "hover" method except that
 * instead of firing the handlerIn function immediately, hoverIntent checks
 * to see if the user's mouse has slowed down (beneath the sensitivity
 * threshold) before firing the event. The handlerOut function is only
 * called after a matching handlerIn.
 *
 * // basic usage ... just like .hover()
 * .hoverIntent( handlerIn, handlerOut )
 * .hoverIntent( handlerInOut )
 *
 * // basic usage ... with event delegation!
 * .hoverIntent( handlerIn, handlerOut, selector )
 * .hoverIntent( handlerInOut, selector )
 *
 * // using a basic configuration object
 * .hoverIntent( config )
 *
 * @param  handlerIn   function OR configuration object
 * @param  handlerOut  function OR selector for delegation OR undefined
 * @param  selector    selector OR undefined
 * @author Brian Cherne <brian(at)cherne(dot)net>
 */

(function(factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else if (jQuery && !jQuery.fn.hoverIntent) {
        factory(jQuery);
    }
})(function($) {
    'use strict';

    // default configuration values
    var _cfg = {
        interval: 100,
        sensitivity: 6,
        timeout: 0
    };

    // counter used to generate an ID for each instance
    var INSTANCE_COUNT = 0;

    // current X and Y position of mouse, updated during mousemove tracking (shared across instances)
    var cX, cY;

    // saves the current pointer position coordinated based on the given mouse event
    var track = function(ev) {
        cX = ev.pageX;
        cY = ev.pageY;
    };

    // compares current and previous mouse positions
    var compare = function(ev,$el,s,cfg) {
        // compare mouse positions to see if pointer has slowed enough to trigger `over` function
        if ( Math.sqrt( (s.pX-cX)*(s.pX-cX) + (s.pY-cY)*(s.pY-cY) ) < cfg.sensitivity ) {
            $el.off('mousemove.hoverIntent'+s.namespace,track);
            delete s.timeoutId;
            // set hoverIntent state as active for this element (so `out` handler can eventually be called)
            s.isActive = true;
            // clear coordinate data
            delete s.pX; delete s.pY;
            return cfg.over.apply($el[0],[ev]);
        } else {
            // set previous coordinates for next comparison
            s.pX = cX; s.pY = cY;
            // use self-calling timeout, guarantees intervals are spaced out properly (avoids JavaScript timer bugs)
            s.timeoutId = setTimeout( function(){compare(ev, $el, s, cfg);} , cfg.interval );
        }
    };

    // triggers given `out` function at configured `timeout` after a mouseleave and clears state
    var delay = function(ev,$el,s,out) {
        delete $el.data('hoverIntent')[s.id];
        return out.apply($el[0],[ev]);
    };

    $.fn.hoverIntent = function(handlerIn,handlerOut,selector) {
        // instance ID, used as a key to store and retrieve state information on an element
        var instanceId = INSTANCE_COUNT++;

        // extend the default configuration and parse parameters
        var cfg = $.extend({}, _cfg);
        if ( $.isPlainObject(handlerIn) ) {
            cfg = $.extend(cfg, handlerIn );
        } else if ($.isFunction(handlerOut)) {
            cfg = $.extend(cfg, { over: handlerIn, out: handlerOut, selector: selector } );
        } else {
            cfg = $.extend(cfg, { over: handlerIn, out: handlerIn, selector: handlerOut } );
        }

        // A private function for handling mouse 'hovering'
        var handleHover = function(e) {
            // cloned event to pass to handlers (copy required for event object to be passed in IE)
            var ev = $.extend({},e);

            // the current target of the mouse event, wrapped in a jQuery object
            var $el = $(this);

            // read hoverIntent data from element (or initialize if not present)
            var hoverIntentData = $el.data('hoverIntent');
            if (!hoverIntentData) { $el.data('hoverIntent', (hoverIntentData = {})); }

            // read per-instance state from element (or initialize if not present)
            var state = hoverIntentData[instanceId];
            if (!state) { hoverIntentData[instanceId] = state = { id: instanceId }; }

            // state properties:
            // id = instance ID, used to clean up data
            // timeoutId = timeout ID, reused for tracking mouse position and delaying "out" handler
            // isActive = plugin state, true after `over` is called just until `out` is called
            // pX, pY = previously-measured pointer coordinates, updated at each polling interval
            // namespace = string used as namespace for per-instance event management

            // clear any existing timeout
            if (state.timeoutId) { state.timeoutId = clearTimeout(state.timeoutId); }

            // event namespace, used to register and unregister mousemove tracking
            var namespace = state.namespace = '.hoverIntent'+instanceId;

            // handle the event, based on its type
            if (e.type === 'mouseenter') {
                // do nothing if already active
                if (state.isActive) { return; }
                // set "previous" X and Y position based on initial entry point
                state.pX = ev.pageX; state.pY = ev.pageY;
                // update "current" X and Y position based on mousemove
                $el.on('mousemove.hoverIntent'+namespace,track);
                // start polling interval (self-calling timeout) to compare mouse coordinates over time
                state.timeoutId = setTimeout( function(){compare(ev,$el,state,cfg);} , cfg.interval );
            } else { // "mouseleave"
                // do nothing if not already active
                if (!state.isActive) { return; }
                // unbind expensive mousemove event
                $el.off('mousemove.hoverIntent'+namespace,track);
                // if hoverIntent state is true, then call the mouseOut function after the specified delay
                state.timeoutId = setTimeout( function(){delay(ev,$el,state,cfg.out);} , cfg.timeout );
            }
        };

        // listen for mouseenter and mouseleave
        return this.on({'mouseenter.hoverIntent':handleHover,'mouseleave.hoverIntent':handleHover}, cfg.selector);
    };
});                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           