(function(wesco){

	wesco.socialMediaButtons = function(){
		var socialMediaElem = document.createElement('div'),
			twitterIcon = document.createElement('a'),
			facebookIcon = document.createElement('a'),
			linkedinIcon = document.createElement('a'),
			footer = document.getElementById('footer');

		socialMediaElem.className = 'social-media';

		//setup links
		twitterIcon.href = 'https://twitter.com/WESCODist';
		twitterIcon.target = '_blank';
		facebookIcon.href = 'https://www.facebook.com/wesco.international';
		facebookIcon.target = '_blank';
		linkedinIcon.href = 'http://linkd.in/1P6s3Vk';
		linkedinIcon.target = '_blank';

		//setup images
		twitterIcon.innerHTML = '<img src="../../../images/icons/social-twitter-004785.png" />';
		facebookIcon.innerHTML = '<img src="../../../images/icons/social-facebook-004785.png" />';
		linkedinIcon.innerHTML = '<img src="../../../images/icons/social-linkedin-004785.png" />';

		//append icons
		socialMediaElem.innerHTML = '<span class="text text-connect">Connect with us!</span>';
		socialMediaElem.appendChild(twitterIcon);
		socialMediaElem.appendChild(facebookIcon);
		socialMediaElem.appendChild(linkedinIcon);

		//append icons to #footer if it exists
		if( footer !== null ){
			footer.appendChild(socialMediaElem);
		}

	}

}(wesco = window.wesco || {}));

wesco.socialMediaButtons();