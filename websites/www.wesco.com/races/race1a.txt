<script id = "race1a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race1={};
	myVars.races.race1.varName="Lu_Id_input_4__onkeyup";
	myVars.races.race1.varType="@varType@";
	myVars.races.race1.repairType = "@RepairType";
	myVars.races.race1.event1={};
	myVars.races.race1.event2={};
	myVars.races.race1.event1.id = "Lu_Id_script_6";
	myVars.races.race1.event1.type = "Lu_Id_script_6__parsed";
	myVars.races.race1.event1.loc = "Lu_Id_script_6_LOC";
	myVars.races.race1.event1.isRead = "False";
	myVars.races.race1.event1.eventType = "@event1EventType@";
	myVars.races.race1.event2.id = "Lu_Id_input_4";
	myVars.races.race1.event2.type = "onkeyup";
	myVars.races.race1.event2.loc = "Lu_Id_input_4_LOC";
	myVars.races.race1.event2.isRead = "True";
	myVars.races.race1.event2.eventType = "@event2EventType@";
	myVars.races.race1.event1.executed= false;// true to disable, false to enable
	myVars.races.race1.event2.executed= false;// true to disable, false to enable
</script>

