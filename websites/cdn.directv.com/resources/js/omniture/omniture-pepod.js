
reportPepodPageData = function (s,addItemToArray,json) {
	
	if((json.requestURI).indexOf('http://cdn.directv.com/receivers.jsp') > 0 || (json.requestURI).indexOf('http://cdn.directv.com/configure.jsp') > 0) {
		s.prop1 = "Packages";
		var path = window.location + "";
		if(path != null && path.indexOf('http://cdn.directv.com/resources/js/omniture/configure.jsp') > 0){	
			if (document.cookie.contains("firstLoad")){
				s.prop42 = "View All Packages";
				s.prop44 = "CTA";
			}
			s.eVar66 = s.prop1;
			s.eVar25 = "Logout";
		}
		
		//check for laungage
		if(json.currentLanguage === 'Spanish') {
			s.prop2 = s.prop1+':Spanish';
		}else {
			s.prop2 = s.prop1+':English';
		}
		
		//hmcMsg check
		if(json.hmcMsg != ""  && json.hmcMsg != null){
			s.prop37 = s.eVar42= json.hmcMsg;
		}
		
		s.prop3 = s.prop2+':Prospect';
		
		if((json.pepHdPreference && json.packageEligibleForRecommendation)  && ((json.queryString).indexOf('from') < 0) && (json.creditBandRiskFactor==0)&& (json.order.numberOfReceivers==0)) {
			s.prop4 = s.prop3+':Promo_PickTV';
		}
		else {
			if(path != null && path.indexOf('http://cdn.directv.com/resources/js/omniture/configure.jsp') > 0){
				s.prop4 = s.prop3+':Zipcode';
				if (document.cookie.contains("firstLoad")){
					s.prop41 = s.prop4;
					s.prop43 = s.prop4 + " | " + s.prop42;
				}
				else {
					document.cookie = "firstLoad= true"
				}
			}else{
				s.prop4 = s.prop3+':Select Receivers';
			}
		}
		
		Reporting.pageName = s.prop4;
		s.prop15 = Reporting.PageData.hardwareSegment;
		s.eVar7 = Reporting.PageData.hardwareSegment;
		s.prop20= json.omnitureTargetMarket;
		s.eVar23= json.omnitureTargetMarket;
		if(json.isHmcClientRemovalMsg != ""){
			s.prop41 = s.prop4 +"_"+"Genie Removal";
			s.prop42 = "Remove";
			s.prop43 = s.prop41+" "+ "|"+" "+s.prop42;
			s.prop44 = "CTA";
			s.prop3 = "PEPOD:Cart:RemoveGenie";
			s.prop4 = "PEPOD:Cart:RemoveGenie";
			s.pageName = "PEPOD:Cart:RemoveGenie";
			s.eVar49 = "PEPOD:Cart:RemoveGenie";
			s.eVar42 = json.hmcRemovalAlertTxt;
			s.prop49 = Reporting.accountId;
			addItemToArray(events, 'event30');
		}
		
	}
	
}