/**
* formValidation.js holds different javascript validations to be done at client side.
* The flows that are using this component currently are as follows:-
* - Registration.
* - Reset Password.
* - Retrieve Email.
**/

!function ($) {
	/**
	* @class Wrapper for all validations used.
	* @name DtvApp.Controls.FormValidate
	* @extends DtvApp.Modules.BaseModule
	*/
	DtvApp.Modules.BaseModule('DtvApp.Controls.FormValidate', {
	},
    {
		/* To avoid initializing multiple times */
		init: function () {
			DtvApp.Controls.FormValidate = this;
		}, 
		/* Initializing validator for each form specifically with form id */
		load: function (formId, modal) {
			formEl = $(formId);
			formModal = modal;
			var _self = this;
			var validator = $(formId).validate({
				onkeyup: _self._validateForm,
				onfocusout: _self._validateForm,
				onfocusin: _self._splValidateForm
			});
							
			$('input',formId).on('keypress', function(e){
				if (e.keyCode === 13) {
					e.preventDefault();
					if($(formId).find('.cep-btn-action').hasClass('js-submit')){
						if($(this).parents('.js-registration-tup').length>0)
							$(this).parents('.js-registration-tup').find('.cep-btn-action.js-submit').click();
						else 
							$(formId).find('.cep-btn-action.js-submit').click();
					}
				}
			});
		},
		/* Clears validation messages and values for specific 
		*  sibling elements on focus and validates the 
		*  particular element on prepopulation.
		*/
		_splValidateForm: function (element, event) {
			el = $(element);
			var _self = DtvApp.Controls.FormValidate;
			/* To clear messages and values of sibling elements*/
    		if(el.attr('groupName')){
				var group = el.attr('groupName');
				$("div."+group).hide();
				$("div."+group).siblings('.form-error-message').hide();
			}
			
		},
		/* Shows success icon and message on validation */
		_showValid: function (el) {
			$(el + "Error").hide().siblings('.form-error-message').hide();
			$(el + "Success").show();
			$(el).removeClass('invalid');
			this._continueState(el, formEl);
			
			  formEl.find('.submit-note-btn').addClass('js-submit');
            
			
		},
		/* Shows error icon on validation */
		_showInvalid: function (el,eType) {
			$(el + "Success").hide();
			$(el + "Error").hide();
			if( !($(el).is('input') || $(el).is('select')) 
				|| ( ($(el).is('input') ||  $(el).is('select')) && (eType == 'focusout'))){
				$(el).addClass('invalid');
				$(el + "Error").show();
				
				
			}
			
			
			 if(formEl.find('.error.btn').is(":visible")){
			     
			     formEl.find('.submit-note-btn').removeClass('js-submit');
             }
			 else{
			     
			     formEl.find('.submit-note-btn').addClass('js-submit'); 
			 }
             
             
			   /* if(  formEl.find('.error btn').parents().find('b.faux-checkbox').hasClass('selected') ){
			        formEl.find('.submit-note-btn').addClass('js-submit');
			    }
			    if( !(  formEl.find('.error btn').parents().find('b.faux-checkbox').hasClass('selected')) ){
			        formEl.find('.submit-note-btn').removeClass('js-submit');
			    }*/
			
			/*if( formEl.find('b.textPreference').hasClass('selected') || 
			        formEl.find('b.emailPreference').hasClass('selected') ||
			        formEl.find('b.oncallPreference').hasClass('selected') ){
			    formEl.find('.submit-note-btn').removeClass('js-submit');
			}
			if( !( formEl.find('b.textPreference').hasClass('selected') ||
			        formEl.find('b.emailPreference').hasClass('selected') ||
                    formEl.find('b.oncallPreference').hasClass('selected') ) ){
			    formEl.find('.submit-note-btn').addClass('js-submit');
			}*/
			
			formEl.find('.cep-btn-action').addClass('disabled-button').removeClass('js-submit');
		},
		/* Shows different error messages on validation */
		_showErrorMsg : function(el,errMsgClass,eType){
			this._showInvalid(el,eType);
			var errorDivId = $(el + "Error");
			errorDivId.siblings('.form-error-message').hide();
			if( !($(el).is('input') || $(el).is('select')) 
				|| ( ($(el).is('input') ||  $(el).is('select')) && (eType == 'focusout')))
				errorDivId.siblings('.'+errMsgClass).show();
			if(formModal){
				formModal.resizeModal();
			}
			 
		},
		/* Validates different elements based on their business
		*  and shows success/error icon and messages accordingly.
		*/
		_validateForm: function (element, event) {
			formEl = $(element).parents('form:eq(0)');
			el = $(element);
			var _self = DtvApp.Controls.FormValidate;
			var isValid = true;
			var regex_email = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			var regex_numeric = /^\d+$/;
			var regex_aplhaNumeric = /^[a-zA-Z0-9]+$/;
			//var regex_password = /^.*(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).*$/;
			var value = el.val();
			if(el.is('input') || el.is('select')){
				/* Validation logic for email using regular expression defined above*/
				if (el.hasClass("js-email")) {
					isValid = true;
					var errMsgClass;
					if(value.length == 0){
						isValid = false;
						errMsgClass = "js-email-req";
					}
					else if(!regex_email.test(value)){
						isValid = false;
						errMsgClass = "js-email-invalid";
					}
				}
				/* Validation logic for security answer. Validations covered are
				*  Non-blank,
				*  Minimum length - 4,
				*  Alphanumeric.
				*/
				else if (el.hasClass("js-securityAnswer")){
					isValid = true;
					var errMsgClass;
					if(value.length == 0){
						isValid = false;
						errMsgClass = "js-secAns-req";
					}
					else if((!regex_aplhaNumeric.test(value)) || value.length < 4 ){
						isValid = false;
						errMsgClass = "js-secAns-invalid";
					} 
					/* To avoid having same value as email and password during registration*/
					formEl.find("input.js-securityAnswerCheck").each(function () {
					    if($(this).val().length > 0 && $(this).val() == value){
						    isValid = false;
							errMsgClass = "js-secAns-invalid";
						}
					});
				}
				/* Validation logic for security Question. Validations covered are
				*  Non-blank.
				*/
				else if (el.hasClass("js-securityQuestion")){
					isValid = true;
					var errMsgClass;
					if(value.length == 0){
						isValid = false;
						errMsgClass = "js-secQues-req";
					}
				}
				/* Validation logic for password using regular expression defined above.
				*  Validations covered are
				*  Should have atleast 1 special char, 1 digit, 1 upper case alpha
				*  and 1 lower case alpha.
				*  Minimum length - 8
				*/
				else if (el.hasClass("js-password")) {
					isValid = true;
					var errMsgClass;
					if(value.length == 0){
						isValid = false;
						errMsgClass = "js-pwd-req";
					}
					//else if(!regex_password.test(value)){
					else if(!regex_aplhaNumeric.test(value)){
						isValid = false;
						errMsgClass = "js-pwd-invalid";
					}
					else if(value.length < 8){
						isValid = false;
						errMsgClass = "js-pwd-length";
					}
					if($("#confirmPassword", formEl).val().length > 0){
						if(value === $("#confirmPassword", formEl).val()) {
							_self._showValid("#confirmPassword");
						} else {
							_self._showErrorMsg("#confirmPassword",'js-confPwd-mismatch',event.type);
						}
					}
				}
				/* Validation logic for confirm password. It should match with password value*/
				else if (el.hasClass("js-confirmPassword")) {
					isValid = false;
					errMsgClass = 'js-confPwd-mismatch';
					if($("#password", formEl).val().length > 0 && value === $("#password", formEl).val()){
						isValid = true;
					}
				}
				/* Validation logic for account number. Validations covered are
				*  Non-blank,
				*  Numeric.
				*/
				else if (el.hasClass("js-accountNumber")) {
					isValid = true;
					var errMsgClass;
					if(value.length == 0){
						isValid = false;
						errMsgClass = "js-acc-req";
					}
					else if(!regex_numeric.test(value)){
						isValid = false;
						errMsgClass = "js-acc-invalid";
					}
				}
				/* Validation logic for last name. Validations covered are
				*  Non-blank,
				*  Should not contain space as first char.
				*/
				else if (el.hasClass("js-lastName")) {
					isValid = false;
					errMsgClass = 'js-lname-req' ;
					if(value.length > 0 && $.trim(value) != ''){
						isValid = true;
						if(value.charAt(0)==' ') {
                            el.val($.trim(value));
                        }
					}
				}
				/* Validation logic for phone number. Validations covered are
				*  Non-blank,
				*  Minimum length - 10,
				*  Numeric.
				*/
				else if (el.hasClass("js-phoneNumber")) {
				    _self._formatPhoneNumber(el, event);
					var phNum = el.val().replace(/\-/g, '');
					isValid = false;
					errMsgClass = 'js-phone-invalid';
					if(regex_numeric.test(phNum) && phNum.indexOf(" ") == -1 && phNum.length == 10 && phNum.substring(3,6) !== '555' ){
						isValid = true;
					}
				}
				/* Validation logic for zip code. Validations covered are
				*  Non-blank,
				*  Minimum length - 5,
				*  Should not be 00000,
				*  Numeric.
				*/
				else if (el.hasClass("js-zipCode")) {
					isValid = false;
					errMsgClass = 'js-zipCode-invalid';
					if(regex_numeric.test(value) && value.indexOf(" ") == -1 && value.length == 5 && value != '00000'){
						isValid = true;
					}
				}
				/* Validation logic for SSN. Validations covered are
				*  Non-blank,
				*  Minimum length - 4,
				*  Numeric.
				*/
				else if (el.hasClass("js-ssn")) {
					isValid = false;
					errMsgClass = 'js-ssn-invalid';
					if(regex_numeric.test(value) && value.indexOf(" ") == -1 && value.length == 4){
						isValid = true;
					}
				}
				/* Validation logic for Credit card no. last four digit. Validations covered are
				*  Non-blank,
				*  Minimum length - 4,
				*  Numeric.
				*/
				else if (el.hasClass("js-creditCardNumber")) {
					isValid = false;
					errMsgClass = 'js-creditCardNumber-invalid';
					if(regex_numeric.test(value) && value.indexOf(" ") == -1 && value.length == 4){
						isValid = true;
					}
				}
				/* Shows success/error icon on validation */
				if (isValid){
					_self._showValid("#"+el.attr("id"));
				} else {
					_self._showErrorMsg("#"+el.attr("id"),errMsgClass,(event=='autofocus') ? '' : event.type);
				}
			} else {
				if(value.length==0)
					_self._showInvalid("#"+el.attr("id"),(event=='autofocus') ? '' : event.type);
				else 
					_self._showValid('#'+el.attr("id"));
			}
		},
		/* Decides whether the form can be submittted and 
		*  enable/disable the submit button accordingly. 
		*/
		_continueState: function(el, formEl){
			var btnEl = formEl.find('.cep-btn-action');
			var disable = false;
			formEl.find("input.js-required,select.js-required,.showerror").each(function () {
				if ($(this).is(":visible") || $(this).hasClass("js-mandate")){
					if($(this).hasClass('showerror')){
						disable = true;
					} else if($(this).val()=="") {
						disable = true;
					} else if($(this).is('input') && !$('#'+$(this).attr("id") + "Success").is(":visible")){
						disable = true;
					}
				}
			});
			if(disable)
				btnEl.addClass('disabled-button').removeClass('js-submit');
			else
				btnEl.removeClass('disabled-button').addClass('js-submit');
		},
		/* Includes hyphen automatically in between phone number
		*  in the format NPA-NXX-XXXX dynamically when
		*  user enters value.
		*/
		_formatPhoneNumber: function(el, ev) {
			var ph = el.val();
			if (ph === $(el).attr("placeholder")) return;
			var phVal = ph.split('-');
			if(phVal.length > 2 && phVal[0].length < 4 && phVal[1].length < 4 && phVal[2].length < 5)
				return;
			if(ph.length == 3 || ph.length == 7){
				if(ev.keyCode!=8){
					el.val(ph+'-');
				} else {
					return;
				}
			}
			phVal = el.val().split('-');
			if(phVal.length > 2 && phVal[0].length < 4 && phVal[1].length < 4 && phVal[2].length < 5)
				return;
			var phNum = ph.replace(/\-/g, '');
			if(phNum.length > 4) {
				var output = [phNum.slice(0, 3), '-', phNum.slice(3)].join('');
				if(phNum.length >=6) {
					output = [output.slice(0, 7), '-', output.slice(7)].join('');
				}
				el.val(output.substring(0,12));
			}
		}
    });
    DtvApp.createControl(document.body, 'FormValidate');
}(jQuery);