/**
* retrieveEmail.js holds javascript for the retrieve email module.
* The functionality handled are as follows:-
* - Navigation between different screens.
* - Reset of different forms.
* - Individual form initializations.
**/
var forgotEmailPasswordModal, forgotEmailModal, forgotPasswordModal;
!function ($) {
	/**
	* @class Wrapper for all behaviour used in retrieve email.
	* @name DtvApp.Controls.RetEmail
	* @extends DtvApp.Modules.BasePage
	*/
	$(function () {
		retEmail = new DtvApp.Controls.RetEmail('#dtv_body_tup');
	});

	DtvApp.Controls.RetEmail = DtvApp.Controls.BasePage({
		_self : '',
		/* Initializes retrieve email modal */
		init: function () {
			_self = this;		
		},
		/* Initializes first form in retrieve email */
		_initRetEmailForm1: function(panel){
			var formId = "#forgotEmailForm1";
			var fc = new DtvApp.Controls.Form(formId, {
				disableEnter: true,
				beforeAjaxCall: function () {
					$('.wait-container').show();
				},
				onSuccessCallback: function (data,selector) {
					$('.wait-container').hide();
					if(data.success){
						$('#forgotEmail-confirm-update-email .email-confirm-section span.ret-email-label').text(data.email.toLowerCase());						
						$('#forgotEmail-input-accnt').hide();
						$('#forgotEmail-confirm-update-email').show();						
						if (typeof(Reporting) !== "undefined" && typeof(Reporting.UserActionManager) !== "undefined") {
							Reporting.UserActionManager.reportForgotEmail();
						}
					}else{
						if(typeof(selector)==='undefined')selector=$(formId).find('.cep-btn-action.js-submit');
						$(selector).parents('div.js-forgot-email-tup').find('div.message-container').html(data.errors[0]).show();
						_self._resetForm(formId);
					}
				}
			});
			/* To Handle Radio Switch*/
			$(".register-field-option input[type=text]").focus(function(){
    			_self._changeVerifyOption(el.attr('groupName'));
            });
    		
    		$('span.reg-radio').bind("click", function(){				
            	if($(this).hasClass('js-verificationGroup2')){
            		_self._changeVerifyOption('js-verificationGroup1');
            	}
            	else {
            		_self._changeVerifyOption('js-verificationGroup2');
            	}
            });
			
			/* Calling validation and placeholder components */
			DtvApp.Controls.FormValidate.load(formId);
			DtvApp.Controls.dtvPlaceHolder.setPlaceHolder(formId);
			if (typeof(Reporting) !== "undefined" && typeof(Reporting.UserActionManager) !== "undefined") {
				Reporting.UserActionManager.reportViewForgotEmail();
			}
		},			
		/* Initializes second form in retrieve email */
		_initRetEmailForm2: function(panel){			
			var formId = "#forgotEmailForm4";
			var fc = new DtvApp.Controls.Form(formId, {
				disableEnter: true,
				beforeAjaxCall: function () {
					$('.wait-container').show();
				},
				onSuccessCallback: function (data,selector) {
					$('.wait-container').hide();
					if(data.success){
						$('#forgotEmail-confirm-update-email').hide();											
						panel.lightbox.hide();
						$('#loginField').val(data.email.toLowerCase());
						$('#loginField').focus();
						$('#message_container .dtv-message-tup').hide();
						$('#message_container #loginMessageDiv .dtv-message-tup.info-message').html("<ul><li>" +data.messages[0]+ "</li></ul>").show();
					}else{
						if(typeof(selector)==='undefined')selector=$(formId).find('.cep-btn-action.js-submit');
						$(selector).parents('div.js-forgot-email-tup').find('div.message-container').html(data.errors[0]).show();
						_self._resetForm(formId);					
					}
				}
			});
			/* Calling validation and placeholder components */
			DtvApp.Controls.FormValidate.load(formId); 
			DtvApp.Controls.dtvPlaceHolder.setPlaceHolder(formId);
			/* Event Handling of 4th Form */
			$("#forgotEmail-confirm-update-email .continue-btn").bind("click",function(){
				panel.lightbox.hide();
				/* SDR2910722: Forgot my email scripting
				var retEmail = $('#forgotEmail-confirm-update-email .email-confirm-section span.ret-email-label').first().text();
				$('#loginField').val(retEmail.toLowerCase());	*/
				$('#loginField').focus();
			});						
			$("#forgotEmail-confirm-update-email .change-email").bind("click",function(){
				$("#forgotEmail-confirm-update-email .email-confirm-section").hide();
				$("#forgotEmail-confirm-update-email .email-update-section").show();
				if (typeof(Reporting) !== "undefined" && typeof(Reporting.UserActionManager) !== "undefined") {
					Reporting.UserActionManager.reportChangeEmail();
				}
			});			
		
		},
		_changeVerifyOption: function(otherGroup){
			var selectedGroup = 'js-verificationGroup1';
			if(otherGroup=='js-verificationGroup1')selectedGroup='js-verificationGroup2';
			
			$(".forgotEmailOption input[type=text]").removeClass('js-required');
			$(".forgotEmailOption div").removeClass('selected');
			
			$('input[type=text].'+selectedGroup).addClass('js-required');   
			$('input[type=text].'+selectedGroup).parents('div.register-field-option').addClass('selected');
			$('input[type=radio].'+selectedGroup).attr('checked',true);
			$('#accountNumber').attr("tabindex","10");
			$('#phoneNumber').attr("tabindex","10");
			$('#Ssn4').attr("tabindex","11");
			
			$('input[type=text].'+otherGroup).each(function() {
				$(this).val("");
				$(this).triggerHandler("blur.placeholder");
				$(this).removeAttr("tabindex");
				$(this).removeClass('invalid');
			});
			
			$("div."+otherGroup).hide();
			$("div."+otherGroup).siblings('.form-error-message').hide();
			if(selectedGroup == 'js-verificationGroup1')
				$("div#forgotEmail-input-accnt #mode").val("locateByAccount");
			else
				$("div#forgotEmail-input-accnt #mode").val("locateByPhone");
			DtvApp.Controls.FormValidate._continueState("", $('div#forgotEmail-input-accnt'));
		},
		/* Launches retrieve email modal */
		launchForgotEmailModal: function(){			
	          var self = this;
	            Dtv.app.ajaxLightbox.show('http://cdn.directv.com/DTVAPP/global/lightbox/component/lightboxForgotEmailComponentNew.jsp', {
	                contentBehavior: {
	                    options: {
	                        onInit: function(panel) {
	                        	_self._initRetEmailForm1(panel);
	    						_self._initRetEmailForm2(panel);	    						
	    						 panel.onDom(panel.el, 'click', {
	    							 'a.close-modal': function() {
	    								 panel.lightbox.hide(); 
	    							 }									 
	    						});
	    						 $("#accountNumber-fakeElm").hide();
	                        },
	                        autoFocusField: false
	                    }
	                }
	            });
			
		},		
		/* Resets different elements in retrieve email forms */
		_resetForm:function(formId){
			$(formId).find('.cep-btn-action').addClass('disabled-button').removeClass('js-submit');
		},
		/* Clears values, validation icons and messages between
		*  sibiling elements on focus of one another.
		*/
		_changeSiblingFields : function(formId, fromClass, toClass){
			$(formId).find(fromClass).each(function() {
				$(this).addClass('js-required');
				$('#ssn').attr("tabindex","3");
				$('#phoneNumber').attr("tabindex","3");
				$('#zipCode').attr("tabindex","4");
			});
			$(formId).find(toClass).each(function() {
				$(this).removeClass('js-required');
				$(this).val("");
				$(this).triggerHandler("blur.placeholder");
				$(this).removeAttr("tabindex");
				$(this).removeClass('invalid');
			});
			$(toClass + 'msg').hide().siblings('.form-error-message').hide();
			$(formId).find('.cep-btn-action').addClass('disabled-button').removeClass('js-submit');
		}
		
	});
}(jQuery);