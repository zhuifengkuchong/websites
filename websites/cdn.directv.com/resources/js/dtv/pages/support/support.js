var dtvModel; // this jsonobj will be holding all the data which are need from BackEnd

!function ($) {
    $(function () {
       
    	new DtvApp.Controls.Support('#dtv_body_tup');

    });
    DtvApp.Controls.Support = DtvApp.Controls.BasePage({
        init: function () {
           var self = this;
           var referrer = null;
           this.checkReferrer();
           $("#backAnswerCenter").click(function() {
               window.location = self.referrer;
           });
           $("#ansCntrMinMaxBtn").click(function() {
        	   $("#support-modal").parent('.ui-dialog').css({ "top":"auto", "left":"auto", "right":10,"bottom":10 });
               $("#support-modal").toggleClass('collapsed');
           });
           $("#ansCntrCloseBtn").click(function() {
               $("#support-modal").addClass("close-modal");
           });
           $("#support-modal").parent('.ui-dialog').bind("mousewheel" , function(ev) {
               ev.preventDefault();
           });
        },

        checkReferrer: function () {
            var self = this;
            var ACMkey = "ACM";
            var referrerKey = "referrer";
            var referrerAttKey = "referrerAtt";
            var referringDomain;
            var ACMregex = new RegExp("[\\?&]" + ACMkey + "=([^&#]*)");
            var referrerRegex = new RegExp("[\\?&]" + referrerKey + "=([^&#]*)");
            var referrerAttRegex = new RegExp("[\\?&]" + referrerAttKey + "=([^&#]*)");
            var referrerResults = referrerRegex.exec(location.search);
            var results = ACMregex.exec(location.search);
            if (referrerResults != null) {
                self.referrer = decodeURIComponent(referrerResults[1]);
                referringDomain = self.referrer.split('/')[2];
            }
            var referrerAttResults = referrerAttRegex.exec(location.search);
            if ( referrerAttResults!= null) {
                referrerAttResults = referrerAttResults[1];
                self.referrer = self.referrer + '?' + referrerAttResults;
            }
            if (results != null ){
                results = results[1];
            }
            //var results = regex.exec(document.referrer);
            //decodeURIComponent(results[1].replace(/\+/g, " ")).toLowerCase() == "false"
            if ( (results == null || results == "true" ) && (referringDomain == "http://cdn.directv.com/resources/js/dtv/pages/support/support.directv.com" || referringDomain == "http://cdn.directv.com/resources/js/dtv/pages/support/supporttest.directv.com" )) {
                var modal = new DtvApp.Controls.Modal('#support-modal', {
                    isModal: false,
                    autoOpen: true,
                    width: 441,
                    height: 'auto',
                    dialogClass: 'support-modal'
                });

                $("#support-modal").show();
                modal.load({
                    onInitContent: function () {
                        $("#support-iframe").attr("src", self.referrer.replace("detail", "detail_lightbox"));
                    },
                    onAfterShow: function () {
                        $("#support-modal").parent('.ui-dialog').draggable();
                        $("#support-modal").parent('.ui-dialog').css({ "height":"auto" , "top":"auto", "left":"auto", "right":10,"bottom":10 , "position":"fixed"});
                    }
                });
            } 
        }
        /*toggleSupportModal: function () {
            if ($("#support-modal").hasClass("collapsed")) {
                $("#support-modal").removeClass("collapsed")
                $(this).html("Hide Support Panel")
                $(".ui-dialog").css({ "width": "512px", "left": $(window).width() - 512 + "px" });
            } else {
                $("#support-modal").addClass("collapsed")
                $(this).html("Show Support Panel")
                $(".ui-dialog").css({ "width": "27px", "left": $(window).width() - 27 + "px", "overflow": "hidden" });
            }
        },*/
    });
} (jQuery);