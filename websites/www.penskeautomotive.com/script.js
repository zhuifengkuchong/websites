function IsZipValid(value) {
	return /^\d{5}$/.test(value);
}

function IsPhoneValid(value) {
	return /^\(\d{3}\)\s\d{3}-\d{4}|\(\d{3}\)-\d{3}-\d{4}|\d{3}\-\d{3}-\d{4}|\d{3}\d{3}\d{4}$/.test(value);
}

function IsEmailValid(value) {
	return /^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.(([0-9]{1,3})|([a-zA-Z]{2,3})|(aero|coop|info|museum|name))$/.test(value);
}

function BaseValidate(formId) {
	var inputs = $('#' + formId + ' input,' + '#' + formId + ' select,' + '#' + formId + ' textarea,' + '#' + formId + ' div.RadioType');
	
	var input = null;
	var hidenMsgObj = null;
	var errMsg = '';
	
	inputs.each(function() {
		input = $(this);
		hidenMsgObj = $('#' + (input.attr('name') ? input.attr('name') : input.attr('id')) + 'Msg');
		
		if (!input.attr('disabled')) {
			if (input.hasClass('TxtType') && input.hasClass('Required') && input.val() == '') {
				if (hidenMsgObj.length > 0) {
					errMsg += hidenMsgObj.text() + '\n';
				} else if (input.attr('title') != '') {
					errMsg += '- ' + input.attr('title') + '\n';
				} else {
					errMsg += '- Field could not be empty \n';
				}
			}
		
			if (input.hasClass('SelectType') && input.hasClass('Required') && input.val() == 0) {
				if (hidenMsgObj.length > 0) {
					errMsg += hidenMsgObj.text() + '\n';
				} else if (input.attr('title') != '') {
					errMsg += '- ' + input.attr('title') + '\n';
				} else {
					errMsg += '- Dropdown should be selected. \n';
				}
			}
			
			if (input.hasClass('ZipCodeType') && ((input.hasClass('Required') || input.val() != '') && !IsZipValid(input.val()))) {
				if (hidenMsgObj.length > 0) {
					errMsg += hidenMsgObj.text() + '\n';
				} else if (input.attr('title') != '') {
					errMsg += '- ' + input.attr('title') + '\n';
				} else {
					errMsg += '- Please provide correct zip code.\n';
				}
			}
			
			if (input.hasClass('PhoneType') && ((input.hasClass('Required') || input.val() != '') && !IsPhoneValid(input.val()))) {
				if (hidenMsgObj.length > 0) {
					errMsg += hidenMsgObj.text() + '\n';
				} else if (input.attr('title') != '') {
					errMsg += '- ' + input.attr('title') + '\n';
				} else {
					errMsg += '- Please provide correct phone number.\n';
				}
			}
			
			if (input.hasClass('EmailType') && ((input.hasClass('Required') || input.val() != '') && !IsEmailValid(input.val()))) {
				if (hidenMsgObj.length > 0) {
					errMsg += hidenMsgObj.text() + '\n';
				} else if (input.attr('title') != '') {
					errMsg += '- ' + input.attr('title') + '\n';
				} else {
					errMsg += '- Please provide correct email.\n';
				}	
			}
			
			if (input.hasClass('ChbxType') && input.hasClass('Required') && input.attr('checked') == false) {
				if (hidenMsgObj.length > 0) {
					errMsg += hidenMsgObj.text() + '\n';
				} else if (input.attr('title') != '') {
					errMsg += '- ' + input.attr('title') + '\n';
				} else {
					errMsg += '- Checkbox should be selected. \n';
				}
			}
			
			if (input.hasClass('RadioType') && input.hasClass('Required') && $('#' + input.attr('id') + ' input:radio:checked').length == 0) {
				if (hidenMsgObj.length > 0) {
					errMsg += hidenMsgObj.text() + '\n';
				} else if (input.attr('title') != '') {
					errMsg += '- ' + input.attr('title') + '\n';
				} else {
					errMsg += '- Radiobox should be selected. \n';
				}
			}
		}
	});
	
	return errMsg;
}

function toggleArrow (el) {
	var homeLeftMenuDiv = document.getElementById ("homeLeftMenu");
	var aDivs = homeLeftMenuDiv.getElementsByTagName("div");
	var aSpans = homeLeftMenuDiv.getElementsByTagName("span");
	var arrowRight = el.getElementsByTagName("span")[0];
	var arrowDown = el.getElementsByTagName("span")[1];

	subitemsDiv = el.parentNode.nextSibling;
	//fix for FF where nextSibling of el is not div with subitems but "\n "
	if (!subitemsDiv.className)
		subitemsDiv = subitemsDiv.nextSibling;

	for (i=0; i < aDivs.length; i++) {
		if (aDivs[i].className.indexOf("subitemsLevel2") > -1 && aDivs[i] != subitemsDiv && aDivs[i].className.indexOf("hidden") < 0) {
			aDivs[i].className = aDivs[i].className + " hidden";
		}
	}
	for (i=0; i < aSpans.length; i++) {
		if (aSpans[i].className.indexOf("arrowDown") > -1 && aSpans[i] != arrowDown && aSpans[i].className.indexOf("hidden") <0) {
			aSpans[i].className = aSpans[i].className + " hidden";
			aSpans[i].previousSibling.className = aSpans[i].previousSibling.className.replace(" hidden", "");
		}
	}
	if (arrowRight.className.indexOf("hidden")<0) {
		arrowRight.className = arrowRight.className + " hidden";
		arrowDown.className = arrowDown.className.replace(" hidden", "");
		subitemsDiv.className = subitemsDiv.className.replace(" hidden", "");
	} else {
		arrowDown.className = arrowDown.className + " hidden";
		arrowRight.className = arrowRight.className.replace(" hidden", "");
		subitemsDiv.className = subitemsDiv.className + " hidden";
	}
}

function HomeDealersLocatorSearch() {
	var location = document.getElementById("_homeLocation");
	var state = document.getElementById("_homeState");
	var brand = document.getElementById("_homeBrand");
	CommonDealersLocatorSearch(location, state, brand);
}
function ControlDealersLocatorSearch() {
	var location = document.getElementById("_location");
	var state = document.getElementById("_state");
	var brand = document.getElementById("_brand");
	CommonDealersLocatorSearch(location, state, brand);
}
function CommonDealersLocatorSearch(location, state, brand) {
	var newURL = window.location.protocol + "//" + window.location.host + window.location.pathname;
	var aURL = newURL.split('/');
	var pageName = aURL[aURL.length - 1];
	newURL = newURL.replace(pageName, "");
	newURL = newURL + "dealer-locator.aspx";
	if (location.value != '' || brand.value != '') {
		var isFirst = true;
		newURL = newURL + "?";
		if (location.value != '') {
			isFirst = false;
			newURL = newURL + '_location=' + location.value;
			if (location.value.toLowerCase() == 'us' && state != null && state.value != '') {
				newURL = newURL + '&_state=' + state.value;
			}
		}
		if (brand.value != '') {
			if (!isFirst) newURL = newURL + '&';
			isFirst = false;
			newURL = newURL + '_brand=' + brand.value;
		}
	}
	window.location = newURL;
}

function OnChangeLocation() {
	var location = document.getElementById("_location");
	var state = document.getElementById("_state");
	if (location.value.toLowerCase() == 'us') {
		$("#_state").show();
	} else {
		$("#_state").val('').prop('selected', true);
		$("#_state").hide();
	}
	GetBrandsByLocation('_location', '_state', '_brand');
}

function OnChangeHomeLocation() {
	var location = $("#_homeLocation").val();
	if (location.toLowerCase() == 'us') {
		$('#_homeState').parent().show();
	} else {
		$("#_homeState").val('').prop('selected', true);
		$('#_homeState').parent().hide();
	}
	GetBrandsByLocation('_homeLocation', '_homeState', '_homeBrand');
}

function clearEmploymetForm() {
	document.getElementById('_dealershipName').value = '';
	document.getElementById('_firstName').value = '';
	document.getElementById('_middleName').value = '';
	document.getElementById('_lastName').value = '';
	document.getElementById('_streetAddress').value = '';
	document.getElementById('_city').value = '';
	document.getElementById('_state').value = '';
	document.getElementById('_zipCode').value = '';
	document.getElementById('_phone').value = '';
	document.getElementById('_mobilePhone').value = '';
	document.getElementById('_emailAddress').value = '';
	document.getElementById('_multiLine').value = '';
	document.getElementById('_position').value = '';
}

function checkEmailStr(email) {
	if(!(/^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.(([0-9]{1,3})|([a-zA-Z]{2,3})|(aero|coop|info|museum|name))$/.test(email))) {
		return false;
	} else {
		return true;
	}	
}

function ValidateEmploymentForm(positionError) {
	var errorList = '';
	if (document.getElementById("_dealershipName").value.length == 0) {
		errorList += '- Please select dealership.\n';
	}

	if (document.getElementById("_firstName").value.length == 0) {
		errorList += '- Please provide your First Name.\n';
	}

	if (document.getElementById("_lastName").value.length == 0) {
		errorList += '- Please provide your Last Name.\n';
	}

	if (document.getElementById("_streetAddress").value.length == 0) {
		errorList += '- Please provide your Street Address.\n';
	}

	if (document.getElementById("_city").value.length == 0) {
		errorList += '- Please provide your City.\n';
	}

	if (document.getElementById("_state").value.length == 0) {
		errorList += '- Please provide your State.\n';
	}

	if (document.getElementById("_state").value.length == 0) {
		errorList += '- Please provide your State.\n';
	}

	if (document.getElementById("_zipCode").value.length == 0) {
		errorList += '- Please provide your Zip Code.\n';
	}

	if (checkEmailStr(document.getElementById("_emailAddress").value) == false) {
		errorList += '- Please provide valid Email.\n';
	}

	if (document.getElementById("_position").value.length == 0) {
		errorList += '- Please provide ' + positionError + '.\n';
	}

	if (errorList != '') {
		alert('You must enter a value in the following fields: \n' + errorList);
		return false;
	} else
		return true;
}

function OpenWindowCenter(url, name, width, height, scroll){
	var params = '';
	if (scroll){
		params += 'scrollbars=yes, ';
		width = width + 16;
	} 
	var scrX = (document.all)?window.screenLeft:window.screen.left;
	var left   = scrX + (window.screen.width  - width)/2;
	var top    = (window.screen.height - height)/2;
	
	params += 'width=' + width + ', height=' + height;
	params += ', top=' + top+', left=' + left;
	popupWin = window.open(url, name, params);
	popupWin.focus();

}

function closeSelf() {
	window.open('','_parent','');
	window.opener = top;        
	window.close();
}

function flashLoader(flashMovie, flashVars, flashHeight, flashWidth, flashAlign, flashQuality, flashBGColor, flashID, flashScale) {
	var flashPluginSpace = 'http://www.macromedia.com/go/getflashplayer';
	document.write('<OBJECT type="application/x-shockwave-flash"  classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0" WIDTH="' + flashWidth + '" HEIGHT="' + flashHeight + '" id="' + flashID + '" ALIGN="' + flashAlign + '">');
	document.write('<PARAM NAME="FlashVars" VALUE="' + flashVars + '">');
	document.write('<PARAM NAME="movie" VALUE="' + flashMovie + '">');
	document.write('<PARAM NAME="quality" VALUE="' + flashQuality + '">');
	if (flashScale == null) {
		var flashScale = '';
		var flashScaleParam = '';
	} else {
		document.write('<PARAM NAME="scale" VALUE="' + flashScale + '">');
		var flashScaleParam = ' scale="' + flashScale + '"';
	}
	document.write('<PARAM NAME="bgcolor" VALUE="' + flashBGColor + '">');
	document.write('<param name="wmode" value="transparent">');
	document.write('<param name="allowScriptAccess" value="always">');
	document.write('<EMBED src="' + flashMovie + '" quality="' + flashQuality + '"' + flashScaleParam + ' bgcolor="' + flashBGColor + '"  WIDTH="' + flashWidth + '" HEIGHT="' + flashHeight + '" NAME="' + flashID + '" ALIGN="' + flashAlign + '" TYPE="application/x-shockwave-flash" PLUGINSPAGE="' + flashPluginSpace + '" FlashVars="' + flashVars + '" wmode="transparent" allowScriptAccess="always"></EMBED>');
	document.write('</OBJECT>');
}

function clearForm() {
	document.getElementById("_txtName").value = '';
	document.getElementById("_txtSendersEmail").value = '';
	document.getElementById("_txtEmail").value = '';
	document.getElementById("_txtCustomMessage").value = '';
	document.getElementById("_chkSendCopy").value = '';
	document.getElementById("ThankYouBox").style.display = "none";
	document.getElementById("RejectedMessageBox").style.display = "none";
}

function validateOnSubmit() {
	var errorList = '';
	if (document.getElementById("_txtName").value.length == 0) {
		errorList += '- Please provide your Name.\n';
	}

	if (checkEmailStr(document.getElementById("_txtSendersEmail").value) == false) {
		errorList += '- Please provide your Email address.\n';
	}

	if (checkEmailStr(document.getElementById("_txtEmail").value) == false) {
		errorList += '- Please provide target Email address .\n';
	}

	if (errorList != '') {
		alert('You must enter a value in the following fields: \n' + errorList);
		return false;
	} else
		return true;
}

function ShowHideDepartmentDetail(id) {
	var div = document.getElementById('DepartmentDetails'+id);
	var linkTxt = document.getElementById('DepartmentDetailsLink'+id);
	if (div.style.display == 'none') {
		div.style.display = 'block';
		linkTxt.innerHTML = 'Close';
	} else {
		div.style.display = 'none';
		linkTxt.innerHTML = 'Department Details';
	}
}

function SelectAllDealership() {
	$('.DealerShipTablTdHead input').attr('checked', $('#alldilership').attr('checked'));
}
function submitStep1(asmxUrl, form) {
	if ($('#txtZip').val() != '') {
		ValidateZipCodeAsync(asmxUrl, form);
	} else if ($('#txtState').val() != '') {
		return true;
	} else {
		alert('Please provide the following information:\n ZIP Code or State');
	}
	return false;
}

function submitStep2() {
	var chboxList = $('input:chekbox');
	var isSelected = false;

	for (i=0; i < chboxList.length; i++) {
		if (chboxList[i].checked && (chboxList[i].name == 'dealer_' || chboxList[i].name == 'hqdealer_')) {
			isSelected = true;
			break;
		}
	}
	if(isSelected)
		return true;
	else {
		alert('Select A Penske Location');
		return false;
	}
}

function submitStep3() {
	var chboxList = $('input:chekbox');
	var isSelected = false;

	for (i=0; i < chboxList.length; i++) {
			if(chboxList[i].checked) {
				isSelected = true;
				break;
			}
	}
	if(isSelected)
		return true;
	else {
		alert('Please Select A Department(s)');
		return false;
	}
}

function submitStep4(formId) {
	var errMsg = BaseValidate(formId);
	
	if (errMsg == '')
		//formObj.submit();
		return true;
	else
		alert('Please provide the following information:\n' + errMsg);
		return false;
}

function validateAttachment(idAttachVisible, idAttach) {
	var attachValue = $('#' + idAttach).val();
	var attach = $('#' + idAttach);
	var attachVisible = $('#' + idAttachVisible);
	var htmlStr = $('#'+idAttach+'Div').html();
	if (attachValue.lastIndexOf('.rtf') > 0 || attachValue.lastIndexOf('.txt') > 0) {
		attachVisible.attr('value',attachValue);
	} else {
		$('#'+idAttach+'Div').html(htmlStr);
		attachVisible.attr('value','');
		alert('Only the following file types may be uploaded: .rtf, .txt');
	}
}

function submitSiteFeedback() {
	var isSelected = false;
	var isDidSubmitNo = false;
	var errMsg = '';
	var searchEng = $('input[name="SearchEngineRadio"]');
	var socialMedia = $('input[name="SocialMediaRadio"]');
	var handheldDevice = $('input[name="HandheldDeviceRadio"]');
	var navigate = $('input[name="HowNavigateRadio"]');
	var didSubmit = $('input[name="DidSubmitRadio"]');
	var didSubmitNo = $('input[id="SubmitNo"]');
	var submitDifficulty = $('input[name="SubmitDifficultyRadio"]');
	var comparisonBetter = $('input[name="ComparisonRadio"]');
	var chkbox = $('#SiteFeedbackHowFind input:checkbox');

	for (i = 0; i < chkbox.length; i++) {
		if (chkbox[i].checked) {
			isSelected = true;
			break;
		}
	}
	if (!isSelected) {
		errMsg += 'You must make a selection in Question 1.\n';
	} else {
		if ($('#SearchEngineChk').attr('checked')) {
			isSelected = false;
			for (i = 0; i < searchEng.length; i++) {
				if (searchEng[i].checked) {
					isSelected = true;
					break;
				}
			}
			if (!isSelected)
				errMsg += 'You must make a selection in Question 1 "Search Engine".\n';
		}

		if ($('#DealershipChk').attr('checked') && ($('#dealerName').val() == "" || $('#dealerName').val() == "Dealership Name"))
			errMsg += 'You must enter in Question 1 "Dealership Recommendation".\n';

		if ($('#SocialMediaChk').attr('checked')) {
			isSelected = false;
			for (i = 0; i < socialMedia.length; i++) {
				if (socialMedia[i].checked) {
					isSelected = true;
					break;
				}
			}
			if (!isSelected)
				errMsg += 'You must make a selection in Question 1 "Social Media Network".\n';
		}
	}
	isSelected = false;
	for (i = 0; i < handheldDevice.length; i++) {
		if (handheldDevice[i].checked) {
			isSelected = true;
			break;
		}
	}
	if (!isSelected)
		errMsg += 'You must make a selection in Question 2.\n';
	if (($('#OtherHandheldDevice').is(':checked')) && ($('#deviceName').val() == ""))
		errMsg += 'You must fill other device name in Question 2.\n';

	isSelected = false;
	for (i = 0; i < navigate.length; i++) {
		if (navigate[i].checked) {
			isSelected = true;
			break;
		}
	}
	if (!isSelected)
		errMsg += 'You must make a selection in Question 3.\n';

	isSelected = false;
	for (i = 0; i < didSubmit.length; i++) {
		if (didSubmit[i].checked) {
			isSelected = true;
			break;
		}
	}
	if (!isSelected)
		errMsg += 'You must make a selection in Question 4.\n';

	for (i = 0; i < didSubmitNo.length; i++) {
		if (didSubmitNo[i].checked) {
			isDidSubmitNo = true;
			break;
		}
	}

	isSelected = false;
	for (i = 0; i < submitDifficulty.length; i++) {
		if (submitDifficulty[i].checked) {
			isSelected = true;
			break;
		}
	}
	if (!isSelected && !isDidSubmitNo)
		errMsg += 'You must make a selection in Question 4."easy yes/no".\n';

	if (!checkComments('FavoriteComment')) {
		errMsg += 'In Question 5 HTML and XML prohibited. Only plain text allowed.\n';
	}

	if (!checkComments('ImproveComment')) {
		errMsg += 'In Question 6 HTML and XML prohibited. Only plain text allowed.\n';
	}

	isSelected = false;
	for (i = 0; i < comparisonBetter.length; i++) {
		if (comparisonBetter[i].checked) {
			isSelected = true;
			break;
		}
	}
	if (!isSelected)
		errMsg += 'You must make a selection in Question 7.\n';

	if (errMsg != '') {
		alert(errMsg);
		return false;
	} else {
		return true;
	}
}

function selectChk(Name, id) {
	var chkbox = $('input[name='+ Name +']');
	var chk = $('#'+id);

	 if (!chk.attr('checked')) {
		for (i=0; i < chkbox.length; i++) {
			chkbox[i].checked = false;
		}
	}
}

function selectChk2(idChk, idInput) {
	var chkbox = $('#'+idChk);
	var input = $('#'+idInput);

	 if (!chkbox.attr('checked')) {
		input.attr('value','Dealership Name');
		input.css('color','#999999');
	}
}

function selectRadio(id, idChk) {
	var radio = $('#' + id);
	var chk = $('#' + idChk);
	
	if (radio.attr('checked')) {
		if (!chk.attr('checked')) {
			chk.attr('checked', true);
		}
	}
}

function checkComments(id) {
	var obj = document.getElementById(id);
	if (obj.value.length > 0) {
		if (!(/>|<|"/.test(obj.value))) 
			return true;
		else 
			return false;
	}
	return true;
}

function ValidateZipCodeAsync(asmxUrl, form) {
	var zipCode = $('#txtZip');

	zipCodeRegex = new RegExp(/(^\d{5}$)|(^\d{5}-\d{4}$)/);
	if (zipCode == null || !zipCodeRegex.test(zipCode.val())) {
		alert('Please enter a valid ZIP code');
		return false;
	} else {
		$.ajax({
			type: "POST",
			url: asmxUrl,
			data:	"{zip: '"+ zipCode.val() + "' }",
			contentType: "application/json",
			dataType: "text",
			success: function(response) {
				var sendinfo = (typeof response) == 'string' ? eval('(' + response + ')') : response;
				if (sendinfo != "Ok"){
					alert(sendinfo);
				} else {
					form.submit();
				}
			},
			failure: function(msg) {
				alert(msg);
			}
		});
	}
}

function IsTouchDevice() {
	return window.Touch || ('ontouchstart' in document.documentElement);
}

// jQuery dropdown control
try {
	(function ($) {
		$.fn.dropDown = function (_options) {
			var _self = this;

			var _defaults = {
				/*defaultLabel: "Select Value",*/
				hasScroll: true,
				slideTime: 100,
				isClassic: false,
				onClick: function (selectedId, selectedValue) { }
			}
			if (IsTouchDevice()) {
				_defaults.isClassic = true;
			}
			var _options = $.extend(_defaults, _options);
			var _data = [];
			var _listHeight = 0;
			var _isEnabled = true;

			if (!(_self._isInitialized)) {
				if (_options.isClassic) {
					var _select = $("<select class='selectBackground'><option value='" + _options.defaultLabel + "'>" + _options.defaultLabel + "</option></select>");
					_self.css("background-image", "none");
					_self.append(_select);
					_select.css("width", _self.width());
					_select.css("height", _self.height());
					_self.Open = function () { }
					_self.Close = function () { }
					_self.Click = function (selectedId, selectedValue) {
						_self.SetSelectedIdAndValue(selectedId, selectedValue);
						if (jQuery.isFunction(_options.onClick)) {
							_options.onClick(selectedId, selectedValue);
						}
					}

					var _selectedId = "";
					var _selectedValue = "";
					var _selectedDataIndex = -1;

					_self.GetSelectedId = function () {
						return _selectedId;
					}
					_self.GetSelectedValue = function () {
						return _selectedValue;
					}
					_self.SetSelectedId = function (newId, noReselect) {
						newId = newId.toString().toLowerCase().replace(/ /g, "_");
						_selectedDataIndex = -1;
						_selectedId = "";
						_selectedValue = "";
						for (x = 0; x < _data.length; x++) {
							if (_data[x].Key == newId) {
								_selectedDataIndex = x;
								_selectedId = _data[x].Key;
								_selectedValue = _data[x].Value;
								break;
							}
						}
						if (!noReselect) {
							ReselectValue();
						}
					}
					_self.SetSelectedValue = function (newValue, noReselect) {
						_selectedDataIndex = -1;
						_selectedId = "";
						_selectedValue = "";
						for (x = 0; x < _data.length; x++) {
							if (_data[x].Value == newValue) {
								_selectedDataIndex = x;
								_selectedId = _data[x].Key;
								_selectedValue = _data[x].Value;
								break;
							}
						}
						if (!noReselect) {
							ReselectValue();
						}
					}
					_self.SetSelectedIdAndValue = function (newId, newValue) {
						_self.SetSelectedId(newId, true);
						if (_selectedDataIndex == -1) {
							_self.SetSelectedValue(newValue, true);
						}
						ReselectValue();
					}

					_self.Refresh = function () {
						$(_select).change(function () {
							var selectedOption = $("option:selected", _select);
							var classes = selectedOption.attr("class").split(" ");
							for (i = 0; i < classes.length; i++) {
								if (classes[i].indexOf("selectorId") > -1) {
									var selectedId = classes[i].split("-")[1];
									_self.Click(selectedId, selectedOption.html());
									return;
								}
							}
							_self.Click("", "");
						});
					}

					_self.LoadData = function (arr, IdFieldName, ValueFieldName) {
						IdFieldName = IdFieldName || 'Key';
						ValueFieldName = ValueFieldName || 'Value';
						_data = [];
						for (i = 0; i < arr.length; i++) {
							_data[i] = {
								Key: arr[i][IdFieldName].toString().toLowerCase().replace(/ /g, "_"),
								Value: arr[i][ValueFieldName].toString()
							};
						}
						var resultingHTML = "";
						if (_options.defaultLabel) {
							resultingHTML = '<option class="">' + _options.defaultLabel + '</option>\n';
						}
						for (i = 0; i < _data.length; i++) {
							var tmpEl = _data[i];
							resultingHTML += '<option class="selectorId-' + tmpEl.Key + '">' + tmpEl.Value + '</option>\n';
						}
						_select.html(resultingHTML);

						_self.Refresh();
					}
					_self.Enable = function () {
						_isEnabled = true;
						_select.attr('disabled', false);
					}
					_self.Disable = function () {
						_isEnabled = false;
						_select.attr('disabled', true);
					}
				} else {
					_self.mouseenter(function () {
						$(this).toggleClass("selectorHovered");
					});
					_self.mouseleave(function () {
						$(this).toggleClass("selectorHovered");
					});

					var _selectedEl = $("<div class='initialLabel'>" + _options.defaultLabel + "</div>");
					_self.append(_selectedEl);
					_selectedEl.click(function () {
						if (_isEnabled)
							_self.Open();
					});

					var _dropdownEl = $("<div class='dropdown hidden'></div>");
					_self.append(_dropdownEl);

					if (_options.hasScroll) {
						var _dropdownButtonUp = $("<div class='dropdownButtonUp'></div>");
						var _dropdownScrollerArea = $("<div class='dropdownScrollerArea'></div>");
						var _dropdownButtonDown = $("<div class='dropdownButtonDown'></div>");

						_dropdownEl.append(_dropdownButtonUp);
						_dropdownEl.append(_dropdownScrollerArea);
						_dropdownEl.append(_dropdownButtonDown);
					}
					var _dropdownList = $("<div class='dropdownList'></div>");
					_dropdownEl.append(_dropdownList);

					_self.Open = function () {
						_dropdownEl.removeClass("hidden");
						RefreshScroller();
					}
					_self.Close = function () {
						_isSliding = false;
						_dropdownEl.addClass("hidden");
						_dropdownEl.addClass("hidden");
					}

					//hide if mouse out of list for more than 500ms
					var _popupTimer;
					_dropdownEl.mouseleave(function () {
						_isSliding = false;
						_popupTimer = setTimeout(function () { _self.Close(); }, 500);
					})
					_dropdownEl.mouseenter(function () {
						clearTimeout(_popupTimer);
					});

					_self.Click = function (selectedId, selectedValue) {
						_self.SetSelectedIdAndValue(selectedId, selectedValue);
						if (jQuery.isFunction(_options.onClick)) {
							_options.onClick(selectedId, selectedValue);
						}
					}

					var _isSliding = false;
					var _selectedId = "";
					var _selectedValue = "";
					var _selectedDataIndex = -1;

					_self.GetSelectedId = function () {
						return _selectedId;
					}
					_self.GetSelectedValue = function () {
						return _selectedValue;
					}
					_self.SetSelectedId = function (newId, noReselect) {
						newId = newId.toString().toLowerCase().replace(/ /g, "_");
						_selectedDataIndex = -1;
						_selectedId = "";
						_selectedValue = "";
						for (x = 0; x < _data.length; x++) {
							if (_data[x].Key == newId) {
								_selectedDataIndex = x;
								_selectedId = _data[x].Key;
								_selectedValue = _data[x].Value;
								break;
							}
						}
						if (!noReselect) {
							ReselectValue();
						}
					}
					_self.SetSelectedValue = function (newValue, noReselect) {
						_selectedDataIndex = -1;
						_selectedId = "";
						_selectedValue = "";
						for (x = 0; x < _data.length; x++) {
							if (_data[x].Value == newValue) {
								_selectedDataIndex = x;
								_selectedId = _data[x].Key;
								_selectedValue = _data[x].Value;
								break;
							}
						}
						if (!noReselect) {
							ReselectValue();
						}
					}
					_self.SetSelectedIdAndValue = function (newId, newValue) {
						_self.SetSelectedId(newId, true);
						if (_selectedDataIndex == -1) {
							_self.SetSelectedValue(newValue, true);
						}
						ReselectValue();
					}

					_self.Refresh = function () {
						//remove item listeners;
						$(".dropdownItem", _dropdownEl).unbind('mouseenter');
						$(".dropdownItem", _dropdownEl).unbind('mouseleave');
						$(".dropdownItem", _dropdownEl).unbind('click');
						//hover style handler;
						$(".dropdownItem", _dropdownEl).mouseenter(function () {
							$(this).toggleClass("itemsHovered");
						});
						$(".dropdownItem", _dropdownEl).mouseleave(function () {
							$(this).toggleClass("itemsHovered");
						});
						//on item click handler
						$(".dropdownItem", _dropdownEl).click(function () {
							_self.Close();
							var classes = $(this).attr("class").split(" ");
							for (i = 0; i < classes.length; i++) {
								if (classes[i].indexOf("selectorId") > -1) {
									var selectedId = classes[i].split("-")[1];
									_self.Click(selectedId, $(this).html());
									return;
								}
							}
							_self.Click("", "");
						});
						RefreshScroller();
					}

					_self.LoadData = function (arr, IdFieldName, ValueFieldName) {
						IdFieldName = IdFieldName || 'Key';
						ValueFieldName = ValueFieldName || 'Value';
						_data = [];
						for (i = 0; i < arr.length; i++) {
							_data[i] = {
								Key: arr[i][IdFieldName].toString().toLowerCase().replace(/ /g, "_"),
								Value: arr[i][ValueFieldName].toString()
							};
						}
						var resultingHTML = "";
						if (_options.defaultLabel) {
							resultingHTML = '<div class="dropdownItem">' + _options.defaultLabel + '</div>\n';
						}
						for (i = 0; i < _data.length; i++) {
							var tmpEl = _data[i];
							resultingHTML += '<div class="dropdownItem selectorId-' + tmpEl.Key + '">' + tmpEl.Value + '</div>\n';
						}
						_dropdownList.html(resultingHTML);

						_self.Refresh();
					}
					_self.Enable = function () {
						_isEnabled = true;
						_self.css("opacity", "1");
						_self.css("color", "#000000");
					}
					_self.Disable = function () {
						_isEnabled = false;
						_self.css("opacity", "1");
						_self.css("color", "#BBBBBB");
					}
				}
				_self._isInitialized = true;
			}

			// helper functions
			function ScrollToTop() {
				var timer;
				if (_isSliding) {
					var currentValue = _dropdownScrollerArea.slider("value");
					currentValue += 15;
					_dropdownScrollerArea.slider("value", currentValue);
					if (currentValue < -_listHeight)
						currentValue = -_listHeight;
					if (currentValue > 0)
						currentValue = 0;
					_dropdownList.css("top", currentValue + "px");

					timer = setTimeout(function () {
						ScrollToTop();
					},
										_options.slideTime);
				} else {
					clearTimeout(timer);
				}
			}

			function ScrollToBottom() {
				var timer;
				if (_isSliding) {
					var currentValue = _dropdownScrollerArea.slider("value");
					currentValue -= 15;
					_dropdownScrollerArea.slider("value", currentValue);
					if (currentValue < -_listHeight)
						currentValue = -_listHeight;
					if (currentValue > 0)
						currentValue = 0;
					_dropdownList.css("top", currentValue + "px");

					timer = setTimeout(function () {
						ScrollToBottom();
					},
										_options.slideTime);
				} else {
					clearTimeout(timer);
				}
			}

			function RefreshScroller() {
				if (_options.hasScroll && _dropdownScrollerArea.slider) {
					//clear slider;
					_dropdownScrollerArea.slider("destroy");
					//remove button listeners;
					_dropdownButtonUp.unbind('click');
					_dropdownButtonUp.unbind('click');

					//slider initialization
					_listHeight = _dropdownList.height() - _dropdownEl.height();
					if (_listHeight > 0) {
						_dropdownScrollerArea.slider({
							animate: true,
							orientation: "vertical",
							min: -_listHeight,
							max: 0,
							value: 0,
							slide: function (event, ui) {
								_dropdownList.css("top", ui.value + "px");
							}
						});
						_dropdownButtonUp.mouseup(function () {
							_isSliding = false;
						});
						_dropdownButtonUp.mousedown(function () {
							_isSliding = true;
							ScrollToTop();
						});

						_dropdownButtonDown.mouseup(function () {
							_isSliding = false;
						});
						_dropdownButtonDown.mousedown(function () {
							_isSliding = true;
							ScrollToBottom();
						});
					}
				}
			}

			function ReselectValue() {
				//console.log("Control:" + _self.selector + "; Selected Id=<" + _selectedId + ">, Value=<" + _selectedValue + ">, Index=<" + _selectedDataIndex + ">");
				if (_options.isClassic) {
					if (_selectedId) {
						$(".selectorId-" + _selectedId, _select).attr("selected", "selected");
					} else {
						$("option:first", _select).attr("selected", "selected");
					}
				} else {
					$(".itemsSelected", _dropdownEl).removeClass("itemsSelected");
					if (_selectedId) {
						_selectedEl.html(_selectedValue);
						$(".selectorId-" + _selectedId, _dropdownEl).addClass("itemsSelected");
					} else {
						_selectedEl.html(_options.defaultLabel);
					}
				}
			}

			return this;
		}
	})(jQuery);
}
catch (err) { }

function ValidateNarrowYourSearch(redirectURL) {
	var refreshURL = 'window.location.href = \'' + redirectURL;
	if ($('#findCarsConditionId').val() == "3") {
		$('#SearchCarsSubmitWarningId').css('display', 'block');
		return false;
	}
	if ($('#findCarsMakeId').val() == "" &&
		$('#findCarsModelId').val() == "" &&
		$('#findCarsBodyStyleId').val() == "" &&
		$('#findCarsPrice1Id').val() == "0" &&
		$('#findCarsPrice2Id').val() == "") {
		$('#SearchCarsSubmitWarningId').css('display', 'block');
		return false;
	}
	return true;
}

function VehicleSearchRadioClick(id) {
	if (!$('#' + id).hasClass('RadioInactive') && !$('#' + id).hasClass('RadioChecked')) {
		$('.Radio').removeClass('RadioChecked');
		$('#' + id).addClass('RadioChecked');
		$('#findCarsConditionId').val(id.toLowerCase().indexOf('new') != -1 ? 'new' : 'used');
		VehicleSearchUpdateData();
	}
}

var constMinus = 10000000;
function VehicleSearchUpdateData() {
	var newUsed = $('#findCarsConditionId').val() == "new";
	var year = $('#findCarsYear').val();
	var makeId = $('#findCarsMakeId').val();
	var modelId = $('#findCarsModelId').val();
	var price = $('#findCarsPrice1Id').val();

	$.ajax({
		type: "POST",
		url: AppRoot + "templates/webservices/common.asmx/GetSearchVehiclesData",
		data: "{newUsed: " + newUsed + "," +
				"year: " + year + "," +
				"makeId: " + makeId + "," +
				"modelId: " + modelId + "," +
				"priceLow: " + (price > constMinus ? price - constMinus : 0) + "," + //(price < 0 ? -price : 0)
				"priceHigh: " + (price < constMinus ? price : 0) + ", " + //(price > 0 ? price : 0)
				"constMinus: " + constMinus + " }",
		contentType: "application/json; charset=utf-8",
		dataType: "text",
		success: function (response) {
			eval(eval(response));
			if (newVehiclesCount > 0) {
				$('#vsRadioNew').removeClass('RadioInactive');
			} else {
				$('#vsRadioNew').addClass('RadioInactive');
			}
			if (usedVehiclesCount > 0) {
				$('#vsRadioUsed').removeClass('RadioInactive');
			} else {
				$('#vsRadioUsed').addClass('RadioInactive');
			}
		},
		failure: function (msg) {
			alert(msg);
		}
	});
}

function VehicleSearchFindClick() {
	var newUsed = $('#findCarsConditionId').val() == "new";
	var year = $('#findCarsYear').val();
	var makeId = $('#findCarsMakeId').val();
	var modelId = $('#findCarsModelId').val();
	var price = $('#findCarsPrice1Id').val();

	$.ajax({
		type: "POST",
		url: AppRoot + "templates/webservices/common.asmx/GetFullUrl",
		data: "{newUsed: " + newUsed + "," +
				"year: " + year + "," +
				"makeId: " + makeId + "," +
				"modelId: " + modelId + "," +
				"priceLow: " + (price > constMinus ? price - constMinus : 0) + "," + //(price < 0 ? -price : 0)
				"priceHigh: " + (price < constMinus ? price : 0) + "} ", //(price > 0 ? price : 0)
		contentType: "application/json; charset=utf-8",
		dataType: "text",
		success: function (response) {
			ntptEventTag('leadview=SearchCarsFromPenskeAutomotiveToPenskeCars');
			window.location = eval(response);
		},
		failure: function (msg) {
			alert(msg);
		}
	});
}

function GetBrandsByLocation(locationFieldId, stateFieldId, targetBrandFieldId) {

	var location = $('#' + locationFieldId).val();
	var state = $('#' + stateFieldId).val();

	$.ajax({
		type: "POST",
		url: AppRoot + "templates/webservices/common.asmx/GetBrandsByLocation",
		data: JSON.stringify({ location: location, state: state }),
		contentType: "application/json",
		dataType: "json",
		success: function (response) {
			var result = eval(response);

			var options = '<option value="">Select Brand</option>';

			for (var i = 0; i < result.length; i++)
				options += '<option value="' + result[i].toLowerCase() + '">' + result[i] + '</option>';

			$('#' + targetBrandFieldId).html(options);

		},
		failure: function (msg) {
			alert(msg);
		}
	});
}