<script id = "race2a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race2={};
	myVars.races.race2.varName="DOMNode[0x7f943efe9560].selected";
	myVars.races.race2.varType="@varType@";
	myVars.races.race2.repairType = "@RepairType";
	myVars.races.race2.event1={};
	myVars.races.race2.event2={};
	myVars.races.race2.event1.id = "_homeLocation";
	myVars.races.race2.event1.type = "onchange";
	myVars.races.race2.event1.loc = "_homeLocation_LOC";
	myVars.races.race2.event1.isRead = "False";
	myVars.races.race2.event1.eventType = "@event1EventType@";
	myVars.races.race2.event2.id = "_homeState";
	myVars.races.race2.event2.type = "onchange";
	myVars.races.race2.event2.loc = "_homeState_LOC";
	myVars.races.race2.event2.isRead = "True";
	myVars.races.race2.event2.eventType = "@event2EventType@";
	myVars.races.race2.event1.executed= false;// true to disable, false to enable
	myVars.races.race2.event2.executed= false;// true to disable, false to enable
</script>

