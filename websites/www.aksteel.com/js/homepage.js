var $BannerCount, $Banners, BannerLength;
var CurrentBanner = 0;
$(document).ready(function () {
    $BannerCount = $("#banner-count");
    $Banners = $(".gallery li");
    BannerLength = $Banners.length;    

    $(".next a").click(function (e) {
        e.preventDefault();
        this.blur();
        clearInterval(Rotating);
        NextBanner();
    });

    $(".prev a").click(function (e) {
        e.preventDefault();
        this.blur();
        clearInterval(Rotating);
        PreviousBanner();
    });

    var Rotating = setInterval("NextBanner()", 4000);
});

function NextBanner() {
    //$($Banners[CurrentBanner]).animate({ left: "-900px" }, 300);
    $($Banners[CurrentBanner]).fadeOut(300, function () {
        $($Banners[CurrentBanner]).addClass("first");
        $(this).removeClass("first");
    });
    if (CurrentBanner == BannerLength - 1) {
        CurrentBanner = 0;
    }
    else {
        CurrentBanner++;
    }
    //$($Banners[CurrentBanner]).css("left", "900px").animate({ left: "0" }, 300);
    $($Banners[CurrentBanner]).show();
    $BannerCount.html((CurrentBanner + 1) + " of " + (BannerLength));
}
function PreviousBanner() {
    //$($Banners[CurrentBanner]).animate({ left: "900px" }, 300);
    $($Banners[CurrentBanner]).fadeOut(300);
    if (CurrentBanner == 0) {
        CurrentBanner = BannerLength - 1;
    }
    else {
        CurrentBanner--;
    }
    $($Banners[CurrentBanner]).fadeIn(300);
    //$($Banners[CurrentBanner]).css("left", "-900px").animate({ left: "0" }, 300);
    $BannerCount.html((CurrentBanner + 1) + " of " + (BannerLength));
}