$(document).ready(function () {
    var CurrentUrl = window.location.pathname.toLowerCase();
    var CurrentUrlSplit = CurrentUrl.split("/");
    if (CurrentUrlSplit[1] != "") {
        $("." + CurrentUrlSplit[1]).addClass("active");
    }
    //$("#main-nav a[href=/" + CurrentUrlSplit[1] + "/]").parent().addClass("active");

    $("#left-side-nav a[href=" + CurrentUrl + "]").parent().addClass("active");

    var qsYear = GetQueryStringValue("year");
    if (qsYear != "") {
        $(".subnav .year" + qsYear).addClass("active");
    }
    else {
        var YearNav = $(".yearnav");
        if (YearNav.length == 1) {
            var tempLi = YearNav.find("li:first");
            if (tempLi.hasClass("home")) {
                tempLi.addClass("active");
            }
            else {
                YearNav.find("li")[1].className = "active";
            }
        }
    }

    $("#btnSearch").click(function () {
        var value = $(".txtSearch").val();
        if (value != "") {
            window.location.href = "http://www.aksteel.com/Search/default.aspx?q=" + value + "&cx=014470239484978518068:scp0i1stwum&cof=FORID:11#1073";
        }
    });
    $(".txtSearch").keypress(function (e) {
        if (e.which == 13) {
            if (this.value != "") {
                window.location.href = "http://www.aksteel.com/Search/default.aspx?q=" + this.value + "&cx=014470239484978518068:scp0i1stwum&cof=FORID:11#1073";
                return false;
            }
        }
    });

});

function GetQueryStringValue(Key) {
    var QStrArray = document.location.search.substring(1).split("&");
    for (var i = 0; i < QStrArray.length; i++) {
        var tempArray = QStrArray[i].split("=");
        if (tempArray[0] == Key) {
            return tempArray[1];
        }
    }

    return "";
}
