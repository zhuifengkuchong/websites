//jQuery.noConflict();
jQuery(document).ready(function()
{
	// This code follows Yahoo's Module Pattern
	accessibilityToolboxFunc = function()
	{
		var fontSizes = { normal : '87.5%', larger : '100%', largest : '112.5%' };
		var selectedFontSize;

		var contrasts = ['normal', 'flipped'];
		var selectedContrast;
		
		var accessibilitySettingsInCookie = readCookie('ACCESSIBILITY_FONT_CONTRAST_SETTINGS');
		
		if(accessibilitySettingsInCookie != null) 
		{
			accessibilitySettings = accessibilitySettingsInCookie.split(' ');
			
			changeFontSize(accessibilitySettings[0]);
			changeContrast(accessibilitySettings[1]);
		}
		else
		{
			jQuery('#font-normal').addClass('current');
			jQuery('#contrast-normal').addClass('current');
		}

		function createCookie(name, value, days)
		{
			if( days )
			{
				var expireDate = new Date();
				expireDate.setTime(expireDate.getTime() + (days*24*60*60*1000));
				var expiresValue = "; expires=" + expireDate.toGMTString();
			}
			else
			{
				var expiresValue = "";
			}
			
			document.cookie = name + "=" + value + expiresValue + "; path=/";
		}

		function readCookie(name)
		{
			var nameEQ = name + "=";
			var ca = document.cookie.split(';');
		
			for(var i=0; i < ca.length; i++)
			{
				var c = ca[i];

				while (c.charAt(0)==' ') c = c.substring(1,c.length);

				if (c.indexOf(nameEQ) == 0)
					return c.substring(nameEQ.length, c.length);
			}
			
			return null;
		}

		function changeFontSize(newFontSize)
		{
			selectedFontSize = 'normal';
			
			for(key in fontSizes)
			{
				if(key == newFontSize)
				{
					selectedFontSize = newFontSize;
				}
			}

			// set the selected font-size
			jQuery('body').css('font-size', fontSizes[selectedFontSize]);
			
			jQuery('a.fontChanger').each(function(){ jQuery(this).removeClass('current'); });
			jQuery('#font-' + selectedFontSize).addClass('current');

			createCookie('ACCESSIBILITY_FONT_CONTRAST_SETTINGS', (selectedFontSize + ' ' + selectedContrast), 1);
		}

		function changeContrast(newContrast)
		{
			if( newContrast == 'flipped' )
			{
	  			selectedContrast = newContrast;

				jQuery('body').addClass('flipped');
	  		}
	  		else
	  		{
				selectedContrast = 'normal';

				jQuery('body').removeClass('flipped');
			}
			
			jQuery('a.contrastChanger').each(function(){ jQuery(this).removeClass('current'); });
			
			jQuery('#contrast-' + selectedContrast).addClass('current');

			createCookie('ACCESSIBILITY_FONT_CONTRAST_SETTINGS', (selectedFontSize + ' ' + selectedContrast), 1);
		}

		//the returned object here will become accessibilityToolboxFunc
		return {
			
			changeFontSizeOnClick : function(newFontSize)
			{
				changeFontSize(newFontSize);
			},
			
			changeContrastOnClick : function(newContrast)
			{
				changeContrast(newContrast);
			}
		}

	}(); // the parens here cause the anonymous function to execute and return
	
 	jQuery('div#accessibilityToolbox a.fontChanger').click(function(event)
 	{
		accessibilityToolboxFunc.changeFontSizeOnClick(jQuery(this).attr('id').split('-')[1]);

		event.preventDefault();
	});
	
	jQuery('div#accessibilityToolbox a.contrastChanger').click(function(event)
	{
		accessibilityToolboxFunc.changeContrastOnClick(jQuery(this).attr('id').split('-')[1]);

		event.preventDefault();
	});

});

