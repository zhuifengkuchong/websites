/* JQuery Enhancements for Gap Inc */

$(document).ready(function () {
    if (document.getElementById("careerIFrame")) {
        var regexS = "[\\?&]redir=([^&#]*)";
        var regex = new RegExp( regexS );
        var results = regex.exec( window.location.href );
        if( results != null )
            document.getElementById('careerIFrame').src=unescape(results[1]);
    }

    /* ui tweaks */
        /* filter checkboxes */
            $("#search-filter").addClass("js-enabled");
            $("#search-filter input:checked").parent('label').attr('class', 'true');
            $("#search-filter input").change(function() { $(this).parent('label').attr('class', this.checked); });  
        
        /* secondary navigation column height */           
            a = $("#secondary-navigation");
            b = $("#primary-content");    
            
            if (a.height() <  b.height()-220){
            	a.height( b.height()-220 );
            } else if (a.height() >= b.height()-220)  {
            	/* for search pages, where side nav filter is
            	 * longer than center content, add to side nav
            	 * so that go button is not covered by gradient 
            	 * image
            	 */            	        
            	a.height(a.height()+200);
           	}
           
        /* Search Text */
            var c = $("#search .text");
            c.attr("value",d=c.attr("title"));
            c.watermark(d);
        

        /* Select Box */
            $('select').uniform();

        /* tagline height */
            $.fn.setAllToMaxHeight = function(){ return this.height( Math.max.apply(this, $.map( this , function(e){ return $(e).height() }) ) ); }
            $('.hero .tagline').setAllToMaxHeight();

        /* tagline position */
            $('.hero .tagline').each(function(index) { $(this).css("top",(323-$(this).outerHeight())/2); });

        /* right-tagline height */
            $.fn.setAllToMaxHeight = function(){ return this.height( Math.max.apply(this, $.map( this , function(e){ return $(e).height() }) ) ); }
            $('.hero .tagline-right').setAllToMaxHeight();

        /* right- tagline position */
            $('.hero .tagline-right').each(function(index) { $(this).css("top",(323-$(this).outerHeight())/2); });

    /* functionality */

        /* calendar popup */
            $('#calendar-popup-container').calendarpopup();

            if(typeof calculateTimeout == 'function') {
            /* hero transition */
                $(".hero-transitional ul").cycle({
                     fx:     'fade',
                     speed:   500,
                     timeout: 5000,
                     pause: true,
                     pager:  '.hero-navigation',
                     timeoutFn: calculateTimeout
                }); 
            } else {
        /* hero transition */
            $(".hero-transitional ul").cycle({
                 fx:     'fade',
                 speed:   500,
                 timeout: 5000,
                 pause: true,
                 pager:  '.hero-navigation'
            }); 
            }

        /* filter check all */
            $('#search-filter input.all').click(function () { 
                $(this).parents('fieldset:eq(0)').find(':checkbox').attr('checked', this.checked); 
                $(this).parents('fieldset:eq(0)').find('label').attr('class', this.checked); 
            });

        /* gallery modal */
            $(".imageGallery ul a.modal").colorbox({transition:'none', opacity:'0.8', html: function(){
                return "<img src="+$(this).attr('href')+" alt="+$(this).attr('title')+" />" +$(this).siblings('.downloads').html();
            }});

        /* job search */
            $("#type").chained("#region"); 
            $("#country").chained("#type, #region");
            /* $("#country").change(function(){ if ($(this).val()!='') { window.open(unescape($(this).val()), '', 'height=800,width=1000,scrollbars=yes,resizable=yes' ); }}); */
            
        /* print button */  
            $('.page-actions .print').click(function() { window.print(); return false; });

        var path = "";
        if(window.designpath) path = designpath;

        /* video */
            /* hero */
                $('#video-hero .video-hero').click(function() { 
                    jwplayer("video-hero").setup({
                        autostart: true,
                        file: $(this).attr("href"),
                        height: 323,
                        width: 726,
                        controlbar: 'over',
                        skin: path+'js/slim.zip',
                        players: [
                          { type: "flash", src: path+"js/player.swf" },
                          { type: "html5" }
                        ]
                    });
                    $(".hero-transitional ul").cycle('stop');
                    return false;
                });         
        
            /* modal */
                $('.youtube-modal').colorbox({transition:'none', opacity:'0.8', html:'<div id="video-container">Loading the player ...</div>', onComplete: function(){
                    jwplayer("video-container").setup({
                        autostart: true,
                        file: $(this).attr("href"),
                        height: 300,
                        width: 535,
                        controlbar: 'over',
                        skin: path+'js/slim.zip',
                        players: [
                          { type: "flash", src: path+"js/player.swf" },
                          { type: "html5" }
                        ]
                    })}, onCleanup: function(){ jwplayer("video-container").remove();}}
                );

		    // fix for IE7 issue
                var height = '675px';
                if($.browser.msie && $.browser.version == 7) height = '725px';
                
                $('.map-modal').colorbox({transition:'none', opacity:'0.8', width: '840px', height: height, html:'<div id="flashContainer">Loading map...<!-- Place alt flash content here --></div>', onComplete: function(){
                         swfobject.embedSWF($(this).attr("href"), "flashContainer", "800", "600", "9", "",{worldXML : $(this).attr("worldXml")},{menu:'false', quality:'best', scale:'noscale'});
                    }, onCleanup: function(){swfobject.removeSWF("flashContainer");}}
                );

        /* social media tabs */  
            $('#social-tabs').dcSocialTabs({
            	widgets: "facebook,twitter,youtube",
            	facebookId: '143767352335568',
            	twitterId: '!372421328218763264',
            	youtubeId: 'GapInc',
				youtube: {
					limit: 5
				},
            	location: 'left',
            	offset: 141,
            	height: 550,
            	imagePath: '/etc/designs/gapinc/images/icons/'
            });
});


/* plugins */

/* calendar popup */
(function(a){a.fn.calendarpopup=function(){return this.each(function(){var d=null;var e=false;var c=a(".page-actions li a.calendar");var b=a("#calendar-popup");a([c.get(0),b.get(0)]).mouseover(function(){if(d){clearTimeout(d)}if(e){return}else{b.show();e=true}}).mouseout(function(){if(d){clearTimeout(d)}d=setTimeout(function(){e=false;b.hide()},500)});a("#calendar-popup .header a").click(function(){b.hide()})})}})(jQuery);

/*
 * Chained - jQuery chained selects plugin
 *
 * Copyright (c) 2010 Mika Tuupola
 *
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 *
 */

(function(a){a.fn.chained=function(c,b){return this.each(function(){var d=this;var e=a(d).clone();a(c).each(function(){a(this).bind("change",function(){a(d).html(a(e).html());var f="";a(c).each(function(){f+="\\"+a(":selected",this).val()});f=f.substr(1);var h=a(c).first();var g=a(":selected",h).val();a("option",d).each(function(){if(!a(this).hasClass(f)&&!a(this).hasClass(g)&&a(this).val()!==""){a(this).remove()}});if(1==a("option",d).size()&&a(d).val()==""){a(d).attr("disabled","disabled")}else{a(d).removeAttr("disabled")}a.uniform.update(".job-search select")});a(this).trigger("change")})})};a.fn.chainedTo=a.fn.chained})(jQuery);


/*

Uniform v1.7.5
Copyright � 2009 Josh Pyles / Pixelmatrix Design LLC
http://pixelmatrixdesign.com

Requires jQuery 1.4 or newer

Much thanks to Thomas Reynolds and Buck Wilson for their help and advice on this

Disabling text selection is made possible by Mathias Bynens <http://mathiasbynens.be/>
and his noSelect plugin. <http://github.com/mathiasbynens/noSelect-jQuery-Plugin>

Also, thanks to David Kaneda and Eugene Bond for their contributions to the plugin

License:
MIT License - http://www.opensource.org/licenses/mit-license.php

Enjoy!

*/

(function(a){a.uniform={options:{selectClass:"selector",radioClass:"radio",checkboxClass:"checker",fileClass:"uploader",filenameClass:"filename",fileBtnClass:"action",fileDefaultText:"No file selected",fileBtnText:"Choose File",checkedClass:"checked",focusClass:"focus",disabledClass:"disabled",buttonClass:"button",activeClass:"active",hoverClass:"hover",useID:true,idPrefix:"uniform",resetSelector:false,autoHide:true},elements:[]};if(a.browser.msie&&a.browser.version<7){a.support.selectOpacity=false}else{a.support.selectOpacity=true}a.fn.uniform=function(k){k=a.extend(a.uniform.options,k);var d=this;if(k.resetSelector!=false){a(k.resetSelector).mouseup(function(){function l(){a.uniform.update(d)}setTimeout(l,10)})}function j(l){$el=a(l);$el.addClass($el.attr("type"));b(l)}function g(l){a(l).addClass("uniform");b(l)}function i(o){var m=a(o);var p=a("<div>"),l=a("<span>");p.addClass(k.buttonClass);if(k.useID&&m.attr("id")!=""){p.attr("id",k.idPrefix+"-"+m.attr("id"))}var n;if(m.is("a")||m.is("button")){n=m.text()}else{if(m.is(":submit")||m.is(":reset")||m.is("input[type=button]")){n=m.attr("value")}}n=n==""?m.is(":reset")?"Reset":"Submit":n;l.html(n);m.css("opacity",0);m.wrap(p);m.wrap(l);p=m.closest("div");l=m.closest("span");if(m.is(":disabled")){p.addClass(k.disabledClass)}p.bind({"mouseenter.uniform":function(){p.addClass(k.hoverClass)},"mouseleave.uniform":function(){p.removeClass(k.hoverClass);p.removeClass(k.activeClass)},"mousedown.uniform touchbegin.uniform":function(){p.addClass(k.activeClass)},"mouseup.uniform touchend.uniform":function(){p.removeClass(k.activeClass)},"click.uniform touchend.uniform":function(r){if(a(r.target).is("span")||a(r.target).is("div")){if(o[0].dispatchEvent){var q=document.createEvent("MouseEvents");q.initEvent("click",true,true);o[0].dispatchEvent(q)}else{o[0].click()}}}});o.bind({"focus.uniform":function(){p.addClass(k.focusClass)},"blur.uniform":function(){p.removeClass(k.focusClass)}});a.uniform.noSelect(p);b(o)}function e(o){var m=a(o);var p=a("<div />"),l=a("<span />");if(!m.css("display")=="none"&&k.autoHide){p.hide()}p.addClass(k.selectClass);if(k.useID&&o.attr("id")!=""){p.attr("id",k.idPrefix+"-"+o.attr("id"))}var n=o.find(":selected:first");if(n.length==0){n=o.find("option:first")}l.html(n.html());o.css("opacity",0);o.wrap(p);o.before(l);p=o.parent("div");l=o.siblings("span");o.bind({"change.uniform":function(){l.text(o.find(":selected").html());p.removeClass(k.activeClass)},"focus.uniform":function(){p.addClass(k.focusClass)},"blur.uniform":function(){p.removeClass(k.focusClass);p.removeClass(k.activeClass)},"mousedown.uniform touchbegin.uniform":function(){p.addClass(k.activeClass)},"mouseup.uniform touchend.uniform":function(){p.removeClass(k.activeClass)},"click.uniform touchend.uniform":function(){p.removeClass(k.activeClass)},"mouseenter.uniform":function(){p.addClass(k.hoverClass)},"mouseleave.uniform":function(){p.removeClass(k.hoverClass);p.removeClass(k.activeClass)},"keyup.uniform":function(){l.text(o.find(":selected").html())}});if(a(o).attr("disabled")){p.addClass(k.disabledClass)}a.uniform.noSelect(l);b(o)}function f(n){var m=a(n);var o=a("<div />"),l=a("<span />");if(!m.css("display")=="none"&&k.autoHide){o.hide()}o.addClass(k.checkboxClass);if(k.useID&&n.attr("id")!=""){o.attr("id",k.idPrefix+"-"+n.attr("id"))}a(n).wrap(o);a(n).wrap(l);l=n.parent();o=l.parent();a(n).css("opacity",0).bind({"focus.uniform":function(){o.addClass(k.focusClass)},"blur.uniform":function(){o.removeClass(k.focusClass)},"click.uniform touchend.uniform":function(){if(!a(n).attr("checked")){l.removeClass(k.checkedClass)}else{l.addClass(k.checkedClass)}},"mousedown.uniform touchbegin.uniform":function(){o.addClass(k.activeClass)},"mouseup.uniform touchend.uniform":function(){o.removeClass(k.activeClass)},"mouseenter.uniform":function(){o.addClass(k.hoverClass)},"mouseleave.uniform":function(){o.removeClass(k.hoverClass);o.removeClass(k.activeClass)}});if(a(n).attr("checked")){l.addClass(k.checkedClass)}if(a(n).attr("disabled")){o.addClass(k.disabledClass)}b(n)}function c(n){var m=a(n);var o=a("<div />"),l=a("<span />");if(!m.css("display")=="none"&&k.autoHide){o.hide()}o.addClass(k.radioClass);if(k.useID&&n.attr("id")!=""){o.attr("id",k.idPrefix+"-"+n.attr("id"))}a(n).wrap(o);a(n).wrap(l);l=n.parent();o=l.parent();a(n).css("opacity",0).bind({"focus.uniform":function(){o.addClass(k.focusClass)},"blur.uniform":function(){o.removeClass(k.focusClass)},"click.uniform touchend.uniform":function(){if(!a(n).attr("checked")){l.removeClass(k.checkedClass)}else{var p=k.radioClass.split(" ")[0];a("."+p+" span."+k.checkedClass+":has([name='"+a(n).attr("name")+"'])").removeClass(k.checkedClass);l.addClass(k.checkedClass)}},"mousedown.uniform touchend.uniform":function(){if(!a(n).is(":disabled")){o.addClass(k.activeClass)}},"mouseup.uniform touchbegin.uniform":function(){o.removeClass(k.activeClass)},"mouseenter.uniform touchend.uniform":function(){o.addClass(k.hoverClass)},"mouseleave.uniform":function(){o.removeClass(k.hoverClass);o.removeClass(k.activeClass)}});if(a(n).attr("checked")){l.addClass(k.checkedClass)}if(a(n).attr("disabled")){o.addClass(k.disabledClass)}b(n)}function h(q){var o=a(q);var r=a("<div />"),p=a("<span>"+k.fileDefaultText+"</span>"),m=a("<span>"+k.fileBtnText+"</span>");if(!o.css("display")=="none"&&k.autoHide){r.hide()}r.addClass(k.fileClass);p.addClass(k.filenameClass);m.addClass(k.fileBtnClass);if(k.useID&&o.attr("id")!=""){r.attr("id",k.idPrefix+"-"+o.attr("id"))}o.wrap(r);o.after(m);o.after(p);r=o.closest("div");p=o.siblings("."+k.filenameClass);m=o.siblings("."+k.fileBtnClass);if(!o.attr("size")){var l=r.width();o.attr("size",l/10)}var n=function(){var s=o.val();if(s===""){s=k.fileDefaultText}else{s=s.split(/[\/\\]+/);s=s[(s.length-1)]}p.text(s)};n();o.css("opacity",0).bind({"focus.uniform":function(){r.addClass(k.focusClass)},"blur.uniform":function(){r.removeClass(k.focusClass)},"mousedown.uniform":function(){if(!a(q).is(":disabled")){r.addClass(k.activeClass)}},"mouseup.uniform":function(){r.removeClass(k.activeClass)},"mouseenter.uniform":function(){r.addClass(k.hoverClass)},"mouseleave.uniform":function(){r.removeClass(k.hoverClass);r.removeClass(k.activeClass)}});if(a.browser.msie){o.bind("http://www.gapinc.com/etc/designs/gapinc/js/click.uniform.ie7",function(){setTimeout(n,0)})}else{o.bind("change.uniform",n)}if(o.attr("disabled")){r.addClass(k.disabledClass)}a.uniform.noSelect(p);a.uniform.noSelect(m);b(q)}a.uniform.restore=function(l){if(l==undefined){l=a(a.uniform.elements)}a(l).each(function(){if(a(this).is(":checkbox")){a(this).unwrap().unwrap()}else{if(a(this).is("select")){a(this).siblings("span").remove();a(this).unwrap()}else{if(a(this).is(":radio")){a(this).unwrap().unwrap()}else{if(a(this).is(":file")){a(this).siblings("span").remove();a(this).unwrap()}else{if(a(this).is("button, :submit, :reset, a, input[type='button']")){a(this).unwrap().unwrap()}}}}}a(this).unbind(".uniform");a(this).css("opacity","1");var m=a.inArray(a(l),a.uniform.elements);a.uniform.elements.splice(m,1)})};function b(l){l=a(l).get();if(l.length>1){a.each(l,function(m,n){a.uniform.elements.push(n)})}else{a.uniform.elements.push(l)}}a.uniform.noSelect=function(l){function m(){return false}a(l).each(function(){this.onselectstart=this.ondragstart=m;a(this).mousedown(m).css({MozUserSelect:"none"})})};a.uniform.update=function(l){if(l==undefined){l=a(a.uniform.elements)}l=a(l);l.each(function(){var n=a(this);if(n.is("select")){var m=n.siblings("span");var p=n.parent("div");p.removeClass(k.hoverClass+" "+k.focusClass+" "+k.activeClass);m.html(n.find(":selected").html());if(n.is(":disabled")){p.addClass(k.disabledClass)}else{p.removeClass(k.disabledClass)}}else{if(n.is(":checkbox")){var m=n.closest("span");var p=n.closest("div");p.removeClass(k.hoverClass+" "+k.focusClass+" "+k.activeClass);m.removeClass(k.checkedClass);if(n.is(":checked")){m.addClass(k.checkedClass)}if(n.is(":disabled")){p.addClass(k.disabledClass)}else{p.removeClass(k.disabledClass)}}else{if(n.is(":radio")){var m=n.closest("span");var p=n.closest("div");p.removeClass(k.hoverClass+" "+k.focusClass+" "+k.activeClass);m.removeClass(k.checkedClass);if(n.is(":checked")){m.addClass(k.checkedClass)}if(n.is(":disabled")){p.addClass(k.disabledClass)}else{p.removeClass(k.disabledClass)}}else{if(n.is(":file")){var p=n.parent("div");var o=n.siblings(k.filenameClass);btnTag=n.siblings(k.fileBtnClass);p.removeClass(k.hoverClass+" "+k.focusClass+" "+k.activeClass);o.text(n.val());if(n.is(":disabled")){p.addClass(k.disabledClass)}else{p.removeClass(k.disabledClass)}}else{if(n.is(":submit")||n.is(":reset")||n.is("button")||n.is("a")||l.is("input[type=button]")){var p=n.closest("div");p.removeClass(k.hoverClass+" "+k.focusClass+" "+k.activeClass);if(n.is(":disabled")){p.addClass(k.disabledClass)}else{p.removeClass(k.disabledClass)}}}}}}})};return this.each(function(){if(a.support.selectOpacity){var l=a(this);if(l.is("select")){if(l.attr("multiple")!=true){if(l.attr("size")==undefined||l.attr("size")<=1){e(l)}}}else{if(l.is(":checkbox")){f(l)}else{if(l.is(":radio")){c(l)}else{if(l.is(":file")){h(l)}else{if(l.is(":text, :password, input[type='email']")){j(l)}else{if(l.is("textarea")){g(l)}else{if(l.is("a")||l.is(":submit")||l.is(":reset")||l.is("button")||l.is("input[type=button]")){i(l)}}}}}}}}})}})(jQuery);