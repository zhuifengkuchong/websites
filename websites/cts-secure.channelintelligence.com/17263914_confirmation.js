// Copyright Channel Intelligence, Inc. 2002-2008
var ci_vid = 17263914;
var ci_cookieDomain=".advanceauto.com";
var ci_loggingurl="https://ttwbs.channelintelligence.com?eid=1&";
var ci_imgs=[];
function CI_ReadCookie(ci_cookieName) {
	var ci_cookieParts = document.cookie.split(';');
	ci_cookieName += '=';
	for (var ci_cookiePartIndex=0;ci_cookiePartIndex<ci_cookieParts.length;ci_cookiePartIndex++)
	{
		var ci_cookiePart=ci_cookieParts[ci_cookiePartIndex];
		while (ci_cookiePart.charAt(0)===' '){ci_cookiePart=ci_cookiePart.substring(1,ci_cookiePart.length);}
		if (ci_cookiePart.indexOf(ci_cookieName)===0){return ci_cookiePart.substring(ci_cookieName.length,ci_cookiePart.length);}
	}
	return null;
}
function CI_LogError(err, customIdentifier) {
	try {
		var oI=new Image();oI.src='https://secure.channelintelligence.com/links/support/js.error.asp?nVID='+ci_vid+'&sCustomerIdentifier='+customIdentifier+'&sMessage='+encodeURIComponent(err.message)+'&sName='+encodeURIComponent(err.name)+'&nNumber='+(err.number&0xFFFF).toString();
	}catch (err1) {}
}
function CI_GetValue(ci_vName,ci_dValue) {
	if (typeof(window[ci_vName])!=="undefined"){return window[ci_vName];}else{return ci_dValue===undefined?null:ci_dValue;}
}
function ci_RQV2(name,dValue,search){
    var qArg=new RegExp('[\\?&]'+name+'=?([^&#]*)','i').exec(search);
    if(qArg===null){return dValue===undefined?null:dValue;}else if(qArg.length<2){return '';}else{return qArg[1];}
}
function CI_ExternalJS(link){
  var script  = document.createElement('script');
  script.src  = link;
  script.type = 'text/javascript';
  script.defer = true;
  document.getElementsByTagName('head').item(0).appendChild(script);
}
function CI_ExternalJS_CB(link,callback){
	var newScript = document.createElement('script');
	newScript.type = 'text/javascript';
	newScript.defer = true;
	newScript.src = link;
	
	if(newScript.readyState) {
		newScript.onreadystatechange = function() {
			if (newScript.readyState == 'complete' || newScript.readyState == 'loaded') {
				callback();
				newScript.onreadystatechange = null;
			}
		}              
	} else {
		newScript.onload = callback;
	}
			
	document.getElementsByTagName('head').item(0).appendChild(newScript); 
}
function CI_ExternalCSS(url){
  	var link  = document.createElement('link');
  	link.rel  = 'stylesheet';
  	link.type = 'text/css';
  	link.href = url;
  	document.getElementsByTagName('head').item(0).appendChild(link);
}
function ci_FP(ci_pix_url){var ci_pic=new Image(1,1);ci_pic.src=ci_pix_url;ci_imgs[ci_imgs.length]=ci_pic;}
function ci_FP_SCRIPT(ci_pix_url, protocol){document.write('<script type="text/javascript" src="' + (protocol !== undefined ? protocol + '://' : (window.location.protocol.toLowerCase() == 'http:' ? 'http://' : 'https://')) + ci_pix_url + '"></script>');}
function ci_FP_FRAME(ci_pix_url){document.write('<iframe width="0" scrolling="no" height="0" frameborder="0" src="' + ci_pix_url + '"></iframe>');}
function ci_PIX(loc,eid,tid,src,sku,tag,cat){
	var url='';
	if (loc===1){url='https://ttwbs.channelintelligence.com?';}
	if (loc===2){url='https://cts-log.channelintelligence.com?';}
	url+='vid='+ci_vid+'&eid='+eid+'&tid='+tid;
	if(src!==null){url+='&src='+src;}
	if(sku!==null){url+='&sku='+sku;}
	if(tag!==null){url+='&tag='+tag;}
	if(cat!==null){url+='&cat='+cat;}
	url += "&ref="+escape(document.referrer);
	return ci_FP(url);
}
Array.prototype.sum = function() {
  return (! this.length) ? 0 : this.slice(1).sum() +
      ((typeof this[0] == 'number') ? this[0] : 0);
};

try {
	var ci_orderid=CI_GetValue('CI_OrderID',null);
	if(ci_orderid!==null) {
		var ci_baseurl=ci_loggingurl;
		var ci_url="cts=v87&"+"v="+ci_vid+"&o="+ci_orderid;
		if (CI_ReadCookie('ci_cpncode')!==null){ci_url+="&cpncode="+CI_ReadCookie('ci_cpncode');}
		if (CI_ReadCookie('ci_tid')!==null){ci_url+="&tid="+CI_ReadCookie('ci_tid');}
		var ci_aIDs=CI_GetValue('CI_ItemIDs',null);
		var ci_aPrices=CI_GetValue('CI_ItemPrices',null);
		var ci_aQtys=CI_GetValue('CI_ItemQtys',null);
		var ci_aMfrNumbers=CI_GetValue('CI_ItemMfrNums',null);
		var ci_aProdNames=CI_GetValue('CI_ItemNames',null);
		var ci_aCatIDs=CI_GetValue('CI_ItemCatIDs',null);
		var ci_aItemPricesDiscount=CI_GetValue('CI_ItemPricesDiscount',null);
		var ci_pixmgr=CI_ReadCookie('ci_pixmgr');
		var ci_afforder=CI_ReadCookie('ci_afforder');
		var ci_ETvars=CI_ReadCookie('ci_ETvars');
		var ci_ggid=CI_ReadCookie('ci_ggid');
		var ci_gglb=CI_ReadCookie('ci_gglb');
		var ci_customerid=CI_GetValue('CI_CustomerID',null);	
		var ci_currency=CI_GetValue('CI_Currency',null);
		var ci_tax=CI_GetValue('CI_Tax',null);		
		var ci_shipping=CI_GetValue('CI_Shipping',null);
		//remove dollar sign from shipping and tax
		if(ci_shipping!=null && ci_shipping!= undefined) {ci_shipping=ci_shipping.replace("$", "");}
		if(ci_tax!=null) {ci_tax=ci_tax.replace("$", "");}
		var ci_shiptype=CI_GetValue('CI_ShipType',null);
		var ci_shipstate=CI_GetValue('CI_ShipState',null);
		var ci_shipzip=CI_GetValue('CI_ShipZIP',null);
		var ci_shipcity=CI_GetValue('CI_ShipCity',null);
		var ci_shipcountry=CI_GetValue('CI_ShipCountry',null);
		var ci_aItemCoreCharges=CI_GetValue('CI_ItemCoreCharges',null);
		var ci_src=CI_ReadCookie('ci_src');
		var ci_itemCount=0;
		var ci_transactionValue=0;
		var ci_fullTransactionValue=0;
		var ci_productids='';
		var ci_qtystring='';
		var ci_pricestring='';
		var ci_cjitems='';
		var ci_cjprice='';
		
		for (ci_skuIndex = 0;ci_skuIndex<ci_aIDs.length;ci_skuIndex++) {
			ci_url += "&s=" + ci_aIDs[ci_skuIndex] + "|" + ci_aQtys[ci_skuIndex] + "|" + ci_aPrices[ci_skuIndex] + "|";
			if (ci_aMfrNumbers != undefined) {ci_url += ci_aMfrNumbers[ci_skuIndex];}
			if (ci_aItemPricesDiscount != undefined) {
				try {
					var tempPromo = parseFloat(ci_aItemPricesDiscount[ci_skuIndex]);
					if(tempPromo > 0) {
						ci_url += "&s="+ci_aIDs[ci_skuIndex]+"_Promo|1|-"+tempPromo+"|";						
						ci_fullTransactionValue = ci_fullTransactionValue - tempPromo;
					}
				}catch(err){CI_LogError(err, 'confirmation_CI_Promo');}
			}
			if (ci_aItemCoreCharges != undefined) {
				try {
					var tempCore = ci_aItemCoreCharges[ci_skuIndex].replace("$", "");;
					if(tempCore > 0) {
						ci_url += "&s=Core|"+ci_aQtys[ci_skuIndex]+"|"+tempCore+"|";						
					}
				}catch(err){CI_LogError(err, 'confirmation_CI_Core');}
			}
			
			ci_transactionValue = ci_transactionValue + parseFloat(ci_aQtys[ci_skuIndex] * ci_aPrices[ci_skuIndex]);
			ci_fullTransactionValue = ci_fullTransactionValue + parseFloat(ci_aQtys[ci_skuIndex] * ci_aPrices[ci_skuIndex]);
			
			ci_itemCount = ci_itemCount + parseInt(ci_aQtys[ci_skuIndex]);
			if(ci_productids===''){ci_productids = ci_aIDs[ci_skuIndex];}else{ci_productids = ci_productids + ',' + ci_aIDs[ci_skuIndex];}
			if(ci_qtystring===''){ci_qtystring = ci_aQtys[ci_skuIndex];}else{ci_qtystring = ci_qtystring + ',' + ci_aQtys[ci_skuIndex];}
			if(ci_pricestring===''){ci_pricestring = ci_aPrices[ci_skuIndex];}else{ci_pricestring = ci_pricestring + ',' + ci_aPrices[ci_skuIndex];}
			ci_cjitems += "&ITEM" + (ci_skuIndex + 1) + "=" + ci_aIDs[ci_skuIndex] + "&QTY" + (ci_skuIndex + 1) + "=" + parseInt(ci_aQtys[ci_skuIndex]) + "&AMT" + (ci_skuIndex + 1) + "=" + ci_aPrices[ci_skuIndex]+ "&DCNT" + (ci_skuIndex + 1) + "=" + ci_aItemPricesDiscount[ci_skuIndex];      
		}
		
		//Get Order Total after Discounts
		var ci_esmoamount=0;
 		var ci_esmconamount=0;
        for(var ci_con=0; ci_con<CI_ItemPricesDiscount.length; ci_con++){
            ci_esmconamount += parseFloat(CI_ItemPricesDiscount[ci_con]);
        }
        for(var ci_esmi=0; ci_esmi < CI_ItemPrices.length; ci_esmi++){
        	ci_esmoamount+= parseFloat(CI_ItemPrices[ci_esmi])*parseInt(CI_ItemQtys[ci_esmi]);
        }
        ci_esmoamount=ci_esmoamount-ci_esmconamount;
        ci_esmoamount=ci_esmoamount.toFixed(2);
		
		
		try{
			if (ci_shipping != undefined) {
				ci_fullTransactionValue  = ci_fullTransactionValue + parseFloat(ci_shipping);
				ci_url += "&s=Shipping|1|"+ci_shipping+"|";
			}
			}catch(err){CI_LogError(err, 'confirmation_CI_Shipping');}
		try{
			if (ci_tax != undefined) {
				ci_fullTransactionValue  = ci_fullTransactionValue + parseFloat(ci_tax);
			}
		}catch(err){CI_LogError(err, 'confirmation_CI_Tax');}
		
		try{
			ci_FP(ci_baseurl + ci_url);
		}catch(err){CI_LogError(err, 'confirmation_CI');}
		
		//EchoSearchMedia
		try{
			ci_FP_SCRIPT('Unknown_83_filename'/*tpa=http://cts-secure.channelintelligence.com/d9lq0o81skkdj.cloudfront.net/k/ap/1kc.js*/, 'https');
		}catch(err){CI_LogError(err, 'confirmation_EchoSearchMedia');}
		
		//Yahoo
		if(ci_pixmgr==='yahoo'){
			try{
				window.ysm_customData = new Object();
				window.ysm_customData.conversion = "transId="+ci_orderid+",currency=USD,amount="+ci_transactionValue;
				var ysm_accountid  = "1A3UNTUL5AJUCCU31S3B6JB952G";
				function ysm0(locUrl,refUrl,customData){var hashValue=0;var hashString="";var maxValue=16000000;if(customData!=null&&typeof(hashString.charCodeAt)=='function'){hashString=customData;if(locUrl!=null)hashString+=locUrl;if(refUrl!=null)hashString+=refUrl;for(var i=0;i<hashString.length;i++){hashValue=11*hashValue+hashString.charCodeAt(i);if(hashValue>=maxValue){hashValue=hashValue % maxValue;}}}return hashValue;}
				function ysm1(n){var c=document.cookie;var i=-1;var j=0;while((i=c.indexOf(n,j))!=-1){if(i==0){break;}else if(i>1&&i<c.length){if(c.charAt(i-1)==' '&&c.charAt(i-2)==';')break;}j=i+1;}if(i==-1)return '';var s=i+n.length;var e=c.indexOf(';',s);if(e==-1)e=c.length;return c.substring(s,e);}
				function ysm2(o,f,i){if(i > 20)return '';var e='';for(p in o){if(typeof(p)=='string'&&p!=null){var x=(f!=''?f+'.'+p:'.'+p );if(typeof(o[p])=='object'&&o[p]!=null){e+=ysm2(o[p],x,i+1 );}else if(typeof(o[p])=='string'&&o[p]!=null){e+=x+':'+escape(o[p])+';';}}}return e;}
				function ysm3(a,h,i,n,v){var w=window;var d=document;var k="ysm_bbk"+a;var ohc=ysm1(k+'=');var c=ysm2(w.ysm_customData,'',0);if(c.length > 0){var cd=escape(c.toString());var hc=ysm0(document.location.toString(),document.referrer.toString(),cd);if(hc==0||hc!=ohc){var t=new Date();d.ysmBeaconPingImage=new Image();d.ysmBeaconPingImage.src='//'+h+i+'?pv=1&cv=1&sn='+n+'&sv='+v+'&st='+t.getTime().toString()+'&ad='+a+'&cs='+cd;d.cookie= k+"="+ hc+"; path=/";}}}if(typeof(pm_hasScriptRun)=="undefined"){var pm_hasScriptRun=true;ysm3(ysm_accountid,'http://cts-secure.channelintelligence.com/data.wa.perf.overture.com','Unknown_83_filename'/*tpa=http://cts-secure.channelintelligence.com/image/ysmcc.gif*/,'Unknown_83_filename'/*tpa=http://cts-secure.channelintelligence.com/ysmcc.js*/,'1.9');}
				ci_FP("https://ttwbs.channelintelligence.com?eid=33&ci_url="+ci_url+"&d=25017771&res="+escape("http://cts-secure.channelintelligence.com/srv3.wa.marketingsolutions.yahoo.com"));
			}catch(err){CI_LogError(err, 'confirmation_Yahoo');}
		}
	
		//YourAmigo
		try{
			CI_ExternalJS_CB("https://support.youramigo.com/91137873/trace.js",function() {
				ya_dv = ci_esmoamount;
				ya_tid = ci_orderid;
				ya_pid = ci_productids;
				for (ci_skuIndex = 0;ci_skuIndex<ci_aIDs.length;ci_skuIndex++) {
					yaAddItemDetail(ci_aIDs[ci_skuIndex], ci_aQtys[ci_skuIndex], ci_aPrices[ci_skuIndex]);
				}
				 
				ya_cust = "91137873";
				yaConvert();
			});
		}catch(err){CI_LogError(err, 'confirmation_YourAmigo');} 
		
		//Google
		if(ci_pixmgr==='google'){
			try{
				ci_FP("https://ttwbs.channelintelligence.com?eid=33&ci_url="+ci_url+"&d=25356082&res="+escape("google"));
			}catch(err){CI_LogError(err, 'confirmation_Google');}
		}
		
		//MSN
		try{
			ci_FP('https://491913.r.msn.com/?type=1&cp=1');
		}catch(err){CI_LogError(err, 'confirmation_MSN');}
		
		//CJ Pixel
		/*
		if(ci_afforder==='cj'||ci_pixmgr==='cj'){
			try{
				var ci_cjurl='';
				ci_cjurl="https://www.emjcd.com/u?CID=1514537&OID="+ci_orderid+"&TYPE=330401"+ci_cjitems+"&CURRENCY=USD&METHOD=IMG";
				ci_FP(ci_cjurl);
				ci_FP('https://ttwbs.channelintelligence.com?eid=33&ci_url='+ci_url+'&d=11138&res='+escape(ci_cjurl));
			}catch(err){CI_LogError(err, 'confirmation_CJ');}
		}
		*/
		
	}  
}catch(err1){CI_LogError(err1, 'confirmation');}