(function($) {

	if (!$) return; 

	var defaults = {
		autocomplete : true,
		sayt : false,
		account: "",
		searchDomain: "",
		inputElement: "",
		inputFormElement: "",
		delay: 150,
		minLength: 3,
		maxResults: 10,
		browserAutocomplete: false,
		submitOnSelect: true,
		queryCaseSensitive: false,
		startsWith: false,
		highlightWords : true,		
		highlightWordsBegin : false,
		header : "",
		footer : ""
	}


	$.fn.AdobeAutocomplete = function(s)
	{

		s = $.extend(defaults,s);
		
		// Extend Autocomplete to allow for highlighting words
		$.extend($.ui.autocomplete.prototype, {
		
			highlightMatches: function(q,t)
			{
				if (s.highlightWords || s.highlightWordsBegin)
				{
					var st = (s.highlightWordsBegin) ? "^" : "";
					var re = new RegExp("(" + st + q + ")","i");
					t = t.replace(re,"<b>" + "$1" + "</b>");
				}		
				return t;
			
			}
		
		});
		
		// Assign Autocomplete Callback functions
		var _ops = {
		
			getAutocompleteRequest : function(s,query)
			{
				var g_staged = (document.getElementById("sp_staged") ? document.getElementById("sp_staged").value : 0);
				var protocol = (document.location.protocol == "https:" ? "https:" : "http:");
				var postfix = (g_staged ? "-stage/" : "/");
				var acct = s.account.split("");
				var spid = "";
				var j = 0;
				for (var i = 0; i < acct.length ; i++)
				{
					if (i >= 2)
					{
						j++;
						if (j == 2) 
						{
							j = 0;
							spid += (i != (acct.length - 1)) ? acct[i] + "/" : acct[i];
						}
						else
						{
							spid += acct[i];
						}
					}
					else
					{
							spid += acct[i];
					}
		
				}
				return protocol + "//content.atomz.com/autocomplete/" + spid + postfix + "?query=" + query + "&callback=?";
			
			},
	
			source: function(request,response)
			{
				if (!request.term) request.term = "";
				var req = this.options.getAutocompleteRequest(s,request.term);
				if (!s.browserAutocomplete)
				{
					$(s.inputFormElement).attr("autocomplete","off");
				}
	

				if (request.term)
				{
					var o = this;
	
					$.getJSON(req,function(data,txt,req) {  
		
						var matcher = null;
						var matchcase = (o.options.queryCaseSensitive) ? "" : "i";
						var res = 0;
						var max = (s.maxResults) ? s.maxResults : 10000;
			
			
						if (s.startsWith)
						{
							matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex(request.term), matchcase );
						}
						else
						{
							matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), matchcase );
						
						}
		
						response($.map(data, function(item) {
							if (matcher.test(item) && res < max)
							{
								res++;
								return {
									label: o.highlightMatches(request.term,item),
									value: item
								}
							}
							
							
						}));							  
		
						if (s.maxResults)
						{
							data.length = s.maxResults;
						}
		
		
					  });
				}		
		
			},
	
	
			open : function(event,ui)
			{
			
				if (event.keyCode != 40 && event.keyCode != 38) 
				{			
					var acdiv = ".ui-autocomplete.ui-menu.ui-widget.ui-widget-content.ui-corner-all";
					$(acdiv + " > li:first").prepend(s.header);
					$(acdiv + " > li:last").append(s.footer);

					// Autocomplete uses text() method, fix any escaped > <
					$(acdiv + " li").each( function() {
						t = $(this).find("a").html();
						t = t.replace(/\&lt;b\&gt;/g,"<b>");	
						t = t.replace(/\&lt;\/b\&gt;/g,"</b>");	
						$(this).find("a").html(t);
					});
				}
			
			},

			
			select: function(event, ui) 
			{
				$(s.inputElement).val(ui.item.value);

				if (typeof s.onselect === "function") { s.onselect(event,ui); } 

				if (s.submitOnSelect)
				{
					$(s.inputFormElement).submit();
				}
			},
			
			search: function(event,ui)
			{

				var str = "";
				if (typeof s.onsearch === "function") { 
					s.onsearch("","",event,ui); 
				} 
				if (s.sayt) { return false; }
				return s.autocomplete;

			}
			
				
		};
		
			
		_ops = $.extend(_ops, s);
		$.extend($.ui.autocomplete.prototype.options, _ops);
		return this.autocomplete(s);
	
	};

})(jQuery);
