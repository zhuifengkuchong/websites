/* global Modernizr */
//  ============================================================
//  VARIABLES
//  ============================================================


var cache = {}, type, videoWidth, videoHeight, currType, id,
    shaleView = false,
    $secPopup = $('.operations-popup'),
    $secPopupPrimary = $('.operations-popup-content'),
    $secPopupSecondary = $('.operations-secondary-content'),
    $secPopupFact = $('.operations-popup-fact'),
    $secPopupVideo = $('.operations-video'),
    $investorNews = $('.investor-news'),
    $opsNav = $('.ops-nav'),
    width = $('body').innerWidth(), //$(window).width(),
    popUpCloseBtn = '<a href="#" class="btn-close"><i class="icon-angle-down"></i></a>',
    mobileLoadBtn = '<a href="#" class="btn-box" id="mobileLoad">Learn More <i class="icon-angle-right"></i></a>',
    mobileCloseBtn = '<a href="#" class="btn-box" id="mobileHide">Hide <i class="icon-angle-right"></i></a>';


//  ============================================================
//  MOBILE - Primary Pop Up Content - SHOW/HIDE BUTTONS
//  ============================================================


$(document).on('click', '#mobileLoad', function(e){
    e.preventDefault();
    $('.operations-popup-content').css('padding-bottom', 0);
    $('.operations-secondary-content').slideDown(400);
    $(this).hide(0, function(){
        $('.operations-secondary-content a.btn-box').show(0);
    });
});

$(document).on('click', '#mobileHide', function(e){
    e.preventDefault();
    $('.operations-popup-content').css('padding-bottom', '2em');
    $('.operations-secondary-content').slideUp(400);
    $(this).hide(0, function(){
        $('.operations-popup-content a.btn-box').show(0);
    });

});

//  ============================================================
//  MOBILE - Secondary Pop Up Content - MARKER TRIGGER
//  ============================================================


function secPopupSecondaryTrigger(size) {
    size = size.toLowerCase();
    if (size === "desk"){
        $secPopupPrimary.animate({'bottom': '100%'}, 500, 'easeInQuart', function(){
            $secPopupSecondary.show().animate({'top': '200px'}, 500, "easeOutQuart");
            $secPopupFact.show("slide", { direction: "down" }, 700, "easeOutQuart");
            // $secPopupFact.show().animate({'top': 0}, 500, "easeInQuart");
            // $secPopupPrimary.show("slide", { direction: "up" }, 500, "easeInQuart");
        });
    } else {
        $('.operations-secondary-content').slideDown(400);
        $('.operations-popup-content').css('padding-bottom', 0);
        $('.operations-popup-content #mobileLoad').hide(0, function(){
            $('.operations-secondary-content a.btn-box').show(0);
        });
    }
}

//  ============================================================
//  MOBILE - Nav State Toggle
//  ============================================================


$(document).on('click', '.operations-menu h2', function(e){
    if (width <= 1024){
        e.preventDefault();
        $opsNav.slideToggle(400);
        $(this).toggleClass('selected');
    }
});

//  ============================================================
//  DESKTOP - Pop Up Content Close Buttons
//  ============================================================


$(document).on('click', '.operations-popup-content .btn-close', function(e){
    e.preventDefault();
    $secPopup.hide("slide", { direction: "down" }, 500, "easeInQuart");
});

$(document).on('click', '.operations-popup-fact .btn-close', function(e){
    e.preventDefault();
    $secPopupFact.hide("slide", { direction: "down" }, 500, "easeInQuart");
    $secPopupSecondary.show().animate({'top': '100%'}, 500, "easeInQuart", function(){
        $secPopupPrimary.animate({'bottom': '30px'}, 500, 'easeOutQuart');
    });
    // $secPopup.hide("slide", { direction: "down" }, 500, "easeInQuart");
});

//  ============================================================
//  VIDEO OVERLAY FUNCTIONS
//  ============================================================

function setVideoSize(){

    // Height
    videoHeight = $('.operations-map-container').height();
    if (width >= 1024){
        videoHeight = videoHeight - 114;
    } else {
        videoHeight = videoHeight - 138;
    }

    // Width
    videoWidth = Math.round(videoHeight * 1.78);
    if (videoWidth > width){
        videoWidth = "100%";
    } else {
        videoWidth = videoWidth + 'px';
    }

    // Convert Height to Str
    videoHeight = videoHeight + 'px';
    if ($secPopupVideo.html()){
        $secPopupVideo.find('iframe').attr({
            'height': videoHeight,
            'width': videoWidth
        });
    }

    if (width < 768){
        $secPopupVideo.remove();
    }

}

var videoId, videoPlayer, videoParams, videoUrl, videoUrlMobile, youtubeUrl,
    videoCloseBtn = '<a href="#" id="popupVideoCloseBtn"></a>',
    youtubeUrl = 'http://www.youtube.com/embed/',
    videoIframe = '<iframe class="youtube-player" width="100%" height="100%" allowfullscreen frameborder="0" />';
    videoParams = [
        'theme=light',
        'color=white',
        'wmode=transparent',
        'modestbranding=1',
        'rel=0',
        'autoplay=1'
    ];
    setVideoSize();

$(document).on('click', '.btn-video', function(e){
    e.preventDefault();
    videoId = $(this).attr('data-video-id');
    videoUrlMobile = youtubeUrl + videoId;
    videoUrl = youtubeUrl + videoId + '?' + videoParams.join('&'),
    videoPlayer = $(videoIframe).attr({
        src: videoUrl,
        height: videoHeight,
        width: videoWidth
    });
    if (width >= 768){
        $secPopupVideo.html(videoPlayer).show();
        $secPopupVideo.append(videoCloseBtn);
    } else {
        window.open(videoUrlMobile);
    }
});

$(document).on('click', '#popupVideoCloseBtn', function(e){
    e.preventDefault();
    $secPopupVideo.hide(0, function(){
        $(this).remove();
    });
});


//  ============================================================
//  JSON INIT FUNCTIONS
//  ============================================================

if(typeof String.prototype.supplant !== 'function') {
    String.prototype.supplant = function(o) {
        return this.replace(/{([^{}]*)}/g,
            function (a,b) {
                var r = o[b];
                return typeof r === 'string' ?
                    r : a;
            });
    };
}


var breadcrumbMarkupTemplate = {
	breadcrumbs: '<ul><li><a href="../../../index.htm"/*tpa=http://www.chk.com/*/>Home</a></li><li><a href="http://www.chk.com/operations">Operations</a></li><li>{breadcrumb}</li></ul>',
};


//  ============================================================
//  JSON Data Functions
//  ============================================================


//  News Block � Date Format
//  ~~~~~~~~~~~~~
/*
function formatDate(date){
    var today = new Date(date.replace(/-/g, '/')),
		weekday= ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" ],
		todayPretty,
		todayDay = weekday[today.getDay()],
		month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
		todayMonth = month[today.getMonth()],
		todayDate = today.getDate(),
		todayYear = today.getFullYear();

	todayPretty = todayDay + ', ' + todayMonth + ' ' + todayDate + ', ' + todayYear;
    return todayPretty; // .toString();
}

*/
function newsItemsUpdate(data){
  var targets = $("*[data-ajax='buildNewsSummary']");
  if ( targets.length < 1 ) { return; }
  
  targets.each( function(i) {
    //set filter
    var filter = ( data.Name === "Overview" ) ? "" : data.Name,
      target = $(this);
    target.attr("data-news-category", filter);
    loadNewsSummary( target );
  });

/*
    var newsItems = [], newsItem;
    for (var i = data.newsItems.length - 1; i >= 0; i--) {
        newsItem = data.newsItems[i];
        newsItem.dateFormatted = formatDate(newsItem.date);
        newsItems.push(breadcrumbMarkupTemplate.news.supplant(newsItem));
    }
    $investorNews.html(newsItems.join(""));
    $investorNews.append(breadcrumbMarkupTemplate.newsLinks);
*/
}





function sanitizeName ( name ) {
  return name.toLowerCase().replace(/\s+/g, "_");
}

function desanitizeId ( id ) {
  return toTitleCase( id.replace( /_/g, " " ) );
}

function sanitizeFileName ( name ) {
  return name.toLowerCase().replace(/_/g, "-");
}

function sanitizeMethodName ( val ) {
  return val.replace( /_/g, " " );
}

function toTitleCase(str)
{
  if ( str.indexOf("-") > -1 ) return toTitleCase(str.replace( /-/g, " " )).replace( /\s+/g, "-" ); 
  return str.replace( /\w\S*/g, function( txt ){ 
    if ( txt === 'and' ) return txt;
    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); 
  });
}







//  ============================================================
//  JSON Call - State/Nav Select + Map State Trigger
//  ============================================================

function changeState() {
  if ( $.address.value() != "" && $.address.value() != "/overview" ) {
    var stateId = $.address.value().slice(1).toLowerCase();
    var localNavigationElement = $("ul.ops-nav a[data-type='" + stateId + "']");
    onNavigationSelect ( localNavigationElement );
  }
}

function changeWildcard() {
  var baseJobsUrl = "/careers/search-our-jobs",
  jobWildcard = $(".wildcard[href^='"+baseJobsUrl+"']");

  if ( $.address.value() != "" && $.address.value() != "/overview" ) {
    jobWildcard.attr("href", baseJobsUrl + $.address.value());
  } else {
    jobWildcard.attr("href", baseJobsUrl);
  }
}


$('.ops-nav li a').on('click', function(evnt){
    onNavigationSelect( $(this), evnt );
});

function onNavigationSelect ( selectedElement, selectEvent ) {

  // type is really just the state name or the overview
  var stateId = selectedElement.attr('data-type');
  
  // stop the anchor from trying to navigate
  if ( typeof selectEvent != "undefined" ) selectEvent.preventDefault();
  
  // close the fact box if it is open
  if ($secPopupFact.html() && $secPopupFact.css('display') === "block" ){ $('.operations-popup-fact .btn-close').trigger('click'); }
  
  // deselect any other siblings of this element
  selectedElement.parent().parent().find( "li > a" ).removeClass( "selected" );
  
  // make this element selected
  selectedElement.addClass('selected');
  
  // update the browser url
  $.address.value( stateId );
  
  // zoom to the selected item's state
  $(document).trigger( 'zoom-to-state', [stateId] );

  // update the page with this state's data
  getStateData( stateId, selectedElement );
  
}


//*** STATE DATA FUNCTIONS ***//
function getStateData( stateId, selectedElement ) {

  var url = "http://services.chk.com/Operations/getoperationsbystate/" + sanitizeMethodName( stateId );
  
  $.ajax({
    url: url,
    dataType: 'JSONP',
    type: 'GET',
    success: function ( data ) {
        populateStateData( data, selectedElement );
    }, 
    error: function() { 
        if ( console.log ) console.log( 'Error retrieving state data.' ); 
    }
  });

}

function populateStateData ( data, selectedElement ) {

  // update the news list
  // newsItemsUpdate(data);
  
  // update the breadcrumb
 	$('nav.breadcrumb').html( breadcrumbMarkupTemplate.breadcrumbs.supplant({breadcrumb: data.Breadcrumb}) );

  // update the headline and summary area
  $('.operations-title-grid article').html( $(data.DescriptionContent) );

  // update the sidebar
  $(".main-content > .two-col .two-col-secondary").html( $(data.SecondaryContent) );

  // if this is a state, update the primary map overlay
  if (type !== "overview"){
    $secPopupPrimary.html( $(data.PrimaryContent) );
    $secPopupPrimary.prepend( popUpCloseBtn );
  }
  
  // lastly, do this
  opsLinkWidthInit( selectedElement );
  
}

function opsLinkWidthInit(target){

  var activeTitle, navToggle;
  var type = target.attr("data-type");
  
  if (matchMedia('screen and (min-width: 1024px)').matches) {
    if (type !== "overview"){
      $secPopup.show("slide", { direction: "down" }, 500, "easeOutQuart");
    } else {
      $('.operations-map-container .operations-popup-fact').hide("slide", { direction: "down" }, 500, "easeInQuart");
      $('.operations-map-container .operations-popup').children('div').attr('style', '');
      $secPopup.hide("slide", { direction: "down" }, 500, "easeInQuart");
    }
  }
  else if (matchMedia('screen and (min-width: 768px)').matches) {
    
    activeTitle = target.html();
    navToggle = '<a href="#" class="ops-nav-toggle"><i class="icon-angle-down"></i></a>';
    activeTitle = '<span class="heading">' + activeTitle + '</span>' + navToggle;
    $('.operations-menu h2').html(activeTitle);
    $opsNav.slideUp(400);
    if (type !== "overview"){
        $secPopup.show("slide", { direction: "down" }, 500, "easeOutQuart");
    } else {
        $secPopup.hide("slide", { direction: "down" }, 500, "easeInQuart");
        $('.operations-map-container .operations-popup-fact').hide("slide", { direction: "down" }, 500, "easeInQuart");
        $('.operations-map-container .operations-popup').children('div').attr('style', '');
    }
    
  }
  else {

    activeTitle = target.html();
    navToggle = '<a href="#" class="ops-nav-toggle"><i class="icon-angle-down"></i></a>';
    activeTitle = '<span class="heading">' + activeTitle + '</span>' + navToggle;
    $('.operations-menu h2').html(activeTitle);
    
    if ( $('.operations-title-grid .operations-secondary-content').html() ) {
      $secPopupSecondary.empty();
      $secPopupSecondary.hide();
      $secPopupPrimary.attr('style', '');
    }

    if (type !== "overview"){
      $('.operations-title-grid').children().not('article').remove();
      var $secPopupMobile = $secPopup.clone().html();
      $opsNav.slideUp(400, function(){
        $('.operations-title-grid').prepend($secPopupMobile);
        $('.operations-title-grid').children().not('article').wrapAll( "<div class='operations-popup'></div>" );
        $('.operations-title-grid .operations-popup').slideToggle(400);
        if (shaleView) {
          secPopupSecondaryTrigger('mobile');
        } else {
          $('.operations-popup-content #mobileLoad').hide(0, function(){
            $('.operations-popup-content a.btn-box').show(0);
          });
        }
      });
    } else {
      $('.operations-title-grid').children().not('article').slideDown(400, function(){
        $(this).remove();
        //$opsNav.slideUp(function(){ $('.operations-title-grid .operations-popup').empty(); }); 
      });
    }
    
  }
}


function shaleClick(type, id){
    
    getShaleData( id );
    
    if ($('.operations-title-grid .operations-popup').html()){
        if (width >= 768){
            secPopupSecondaryTrigger('desk');
        } else {
            secPopupSecondaryTrigger('mobile');
        }
    } else {
        mapZoom(true, type, function(){
            if (width >= 768){
                secPopupSecondaryTrigger('desk');
            }
        });
    }
}






//*** SHALE DATA FUNCTIONS ***//
function getShaleData( shaleId ) {

  var url = "http://services.chk.com/Operations/getoperationsshales";
  
  $.ajax({
    url: url,
    dataType: 'JSONP',
    type: 'GET',
    success: function ( data ) {
        populateShaleData( data, shaleId );
    }, 
    error: function() { 
        if ( console.log ) console.log( 'Error retrieving shale data.' ); 
    }
  });

}

function populateShaleData( data, shaleId ) {
  
  //filter down to the current play data object
  var shale = jQuery.grep(data, function( element, index ) { 
    return element.Title === desanitizeId( shaleId ) 
  });

  try {
    // update the primary content
    popupContentShale( shale[0].PrimaryContent );
  } 
  catch( ex ) {
    if ( console.log ) console.log( "Unable to find primary shale content." );
  }
  
  try {
    // update the secondary content
    $secPopupFact.html( $(shale[0].SecondaryContent) );
  } 
  catch( ex ) {
    if ( console.log ) console.log( "Unable to find secondary shale content." );
  }

}

function popupContentShale( html ){
  $secPopupSecondary.html( $(html) );
  $secPopupSecondary.append( mobileCloseBtn );
  if ( width <= 1024 ){
    var $secPopupMobileSec = $secPopupSecondary.clone().html();
    $('.operations-title-grid .operations-secondary-content').html( $secPopupMobileSec );
  }
}




//  ============================================================
//  MOBILE/DESK INIT FUNCTIONS (Switching between states)
//  These are called by interaction.js - bp
//  ============================================================


function mobileOpsInit() {
    if ($secPopupPrimary.html()) {
        $('.operations-map-container .operations-popup').hide(function(){
            $('.ops-nav li a[data-type="' + currType +  '"]').trigger('click');
            $('.operations-map-container .operations-popup-fact').hide(0);
            $('.operations-map-container .operations-popup').children('div').attr('style', '');
            $('.operations-title-container .operations-popup').children('div').attr('style', '');
        });
    }
}

function deskOpsInit() {
    if ($secPopupPrimary.html()){
        $('.operations-title-grid .operations-popup').hide(function(){
            $('.ops-nav li a[data-type="' + currType +  '"]').trigger('click');
            $('.operations-map-container .operations-popup').children('div').attr('style', '');
            $('.operations-title-container .operations-popup').children('div').attr('style', '');
        });
    }
}


//  ============================================================
//  MAP FUNCTIONS
//  ============================================================

function mapZoom(sv, type, callback) {
    shaleView = Boolean(sv);
    $('.ops-nav li a[data-type="'+ type +'"]').trigger('click');
    $('.operations-title-grid .operations-secondary-content').slideUp(400, function(){
        $('.operations-popup-content').css('padding-bottom', '2em');
    });
    callback && callback();
}


// ~ Google Map Init ~
//======================================================

Modernizr.load([
  {
    test: $('#map').length,
    yep : 'http://www.chk.com/Style Library/js/partials/operations-map.js?v=1'
  },
  {
    test: $.address,
    nope: '../../../../static.chk.com/jquery.address/1.5/jquery.address-1.5.min.js'/*tpa=http://static.chk.com/jquery.address/1.5/jquery.address-1.5.min.js*/,
    complete: function() {
		  setTimeout(addressInit, 1000);
		}
  }
]);

//  ============================================================
//  ADDRESS FUNCTIONs
//  ============================================================



function addressInit(){
  var initPath = $.address.path();

  if (initPath !== "/"){
    initPath = initPath.slice(1);
    $('.ops-nav li a[data-type="' + initPath + '"]').trigger('click');
  } else {
    $('.ops-nav li a[data-type="overview"]').trigger('click');
  }

  changeWildcard();
  
  $.address.change( function(event){
    $.address.path( event.value );
    changeState( event.value );
    changeWildcard();
  });
}



//  ============================================================
//  RESIZE FUNCTION
//  ============================================================


$(window).resize(function(){
    width = $(window).width();
    setVideoSize();
}).trigger('resize');


//  ============================================================
//  INTIALIZE OVERVIEW STATE
//  ============================================================
