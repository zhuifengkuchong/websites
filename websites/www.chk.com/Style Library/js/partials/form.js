$('.search-bar form input').watermark('SEARCH', {useNative: false});
$('.login-pw').watermark('PASSWORD', {useNative: false});
$('.login-user').watermark('USERNAME', {useNative: false});

$("input[type='text'].form-control, .chk-form input[type='text'], .chk-form input[type='email'], .chk-form input[type='tel'], .chk-form textarea").each( function() {
  var input = $(this);
  input.watermark( input.attr("placeholder") );
});