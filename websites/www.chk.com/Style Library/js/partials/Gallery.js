var _VIDEO_GALLERY_SERVICE_URL = "http://services.chk.com/Lists2/Video Galleries",
	_IMAGE_GALLERY_SERVICE_URL = "http://services.chk.com/Lists2/Photo Galleries",
	_GALLERY_SORT_ORDER = 'chkRollupSortOrder asc, Title asc',
	_VIDEO_YOUTUBE_URL = "http://www.youtube.com/";

$(function() {
  getImagesGallery();
  getVideoGallery();
});

function getVideoGallery(){
	//Get the container for the video library
	var VideoContainer = $('*[data-ajax="videoGallery"]'); 
	
	var VideoGalleryFolder = VideoContainer.data("gallery-folder");
	//Set the fitler to get the video test gallery
	var SelectVideoGallery = 'FileDirRef eq \'/Video Galleries/'+VideoGalleryFolder+'\'';
	var  TopFilter;
	 if(VideoContainer.data("gallery-max")){
		TopFilter = VideoContainer.data("gallery-max");
	 }
	//Set the filter param for retrieving video gallery items
	var param = {
		recursive : 'true',
		fields: 'chkExternalSourceContentId,FileDirRef,FileLeafRef,FileRef,chkRollupSortOrder,Title,Description,VideoLinkUrl,VideoText',
		$orderby : _GALLERY_SORT_ORDER,
		$top: TopFilter,
		$filter : SelectVideoGallery
	};
	//Check if the video container is present on the page
	if(VideoContainer.length > 0)
	{
		//Make an ajax call to retrieve all the items for video gallery
		$.ajax({
				url: _VIDEO_GALLERY_SERVICE_URL,
				dataType: 'JSONP',
				data: param,
				type: 'GET',
				success: function (data) {
					DisplayVideoGallery(data);
				},
				error: function() {
					if(console.log) console.log('Error retrieving Videos for Video Gallery');
				}
		});
	}
}


function DisplayVideoGallery(data){

var strVideoGalleryGrid = '<h2>'+$('*[data-ajax="videoGallery"]').data("gallery-folder")+'</h2>' + 
						  '<div class="people-grid">'+
						  '<div class="pg-item-overlay" data-slide-dir="left" style="top: 0px; height: 296px; display: none;">'+
						  '<a href="#" id="pg-close"></a>'+
						  '</div>';
var videoEmbedURL;
var videoExpandURL;
var videoDescription;				  
		$.each(data, function(i, item){
		
					videoEmbedURL = _VIDEO_YOUTUBE_URL + '/embed/' + item.chkExternalSourceContentId ;
					videoExpandURL = item.VideoLinkUrl;
					videoDescription = (item.Description.length > 0) ? item.Description : '';

  				var strTitle = '';
  				//Check the length of the title and set the limit of char to display if the title is too long
  				if(item.Title != 'undefined' && item.Title.length > 0){
  				  strTitle = (item.Title.length < 60) ? item.Title : item.Title.substring(0,59) + '...' ;
  				}

					strVideoGalleryGrid = strVideoGalleryGrid	+ '<article class="pg-item-video">'+
						  '<div class="pg-item-box caption">'+
							'<div class="vert-block">'+
							'<h4>'+ strTitle +'</h4>'+ 
							'<div class="caption"><i class="icon-plus-circle"></i>Expand</div>'+
						  '</div>'+
						  '</div>'+
						  '<a href="#"><img alt="" src="'+item.FileRef+'?RenditionId=8"></a>'+
						  '<div class="hover-content">'+
							'<div class="video-box vb-padding">'+
								'<div class="video-nav vb-small">'+
									'<a href="'+item.VideoLinkUrl+'" data-video-id="'+ item.chkExternalSourceContentId +'" target="_blank" style="width:100%" class="video-expand"><i class="icon-plus-circle"></i> '+item.VideoText+'</a>'+
								'</div>'+
								'<div class="article--video-wrapper"><iframe class="article--video-player" width="100%" height="296" allowfullscreen="" frameborder="0" src="'+videoEmbedURL+'?theme=light&amp;color=white&amp;wmode=transparent&amp;modestbranding=1&amp;iv_load_policy=3&amp;rel=0&amp;autohide=1&amp;vq=hd720&amp;fs=1&amp;controls=1"></iframe></div>'+
							'</div>'+
						  '</div>'+
						  '</article>';
				});
						  
			strVideoGalleryGrid = strVideoGalleryGrid + '</div>';
			var VideoContainer =  $('*[data-ajax="videoGallery"]'); 
			VideoContainer.html(strVideoGalleryGrid);
			SetClassforImages();		  
	}


//GetImagesGallery function gets the images for the gallery
function getImagesGallery(){
	var ImagesContainer = $('*[data-ajax="buildGallery"]'); 
	//Get the folder which contains all the images. 
	var ImageGalleryFolder = ImagesContainer.data("gallery-folder");
	var SelectImageGallery = 'FileDirRef eq \'/Photo Galleries/'+ImageGalleryFolder+'\'';
	var  TopFilter;
	 if(ImagesContainer.data("gallery-max")){
		TopFilter = ImagesContainer.data("gallery-max");
	 }
	var param = {
		 recursive : 'true',
		 fields : 'Title,FileRef,chkRollupSortOrder,FileDirRef,ImageLinkUrl,ImageLinkText',
		 $orderby : _GALLERY_SORT_ORDER,
		 $top: TopFilter,
		 $filter : SelectImageGallery
		};
		
		if(ImagesContainer.length != 0)
		{
		
			$.ajax({
							url: _IMAGE_GALLERY_SERVICE_URL,
							dataType: 'JSONP',
							data: param,
							type: 'GET',
							success: function (data) {
								DisplayImageGallery(data);
							},
							error: function () {
								if (console.log) console.log('Error retrieving Images for Image Gallery"');
							}
						}); 
		}
					
}

function DisplayImageGallery(data){
	
	var strImageGalleryGrid = '<div class="people-container article--video-grid multimedia">' +
							  '<h2>'+$('*[data-ajax="buildGallery"]').data("gallery-folder")+'</h2>' + 
							  '<div class="people-grid">'+
							  '<div class="pg-item-overlay" data-slide-dir="left" style="top: 0px;height: 296px; display: none;">'+
							  '<a href="#" id="pg-close"></a>'+
							  '</div>';
		$.each(data, function(i, item){
				var strTitle = '';
				//Check the length of the title and set the limit of char to display if the title is too long
				if(item.Title != 'undefined' && item.Title.length > 0){
						  strTitle = (item.Title.length < 60) ? item.Title : item.Title.substring(0,59) + '...' ;
					}
					strImageGalleryGrid = strImageGalleryGrid + '<article class="pg-item-photo">'+
																'<div class="pg-item-box caption">'+
																'<div class="vert-block">'+
																'<h4>'+ strTitle +'</h4>'+ 
																'<div class="caption"><i class="icon-plus-circle"></i>Expand</div>'+
																'</div>'+
																'</div>'+
																'<a href="#"><img alt="" src="'+ item.FileRef +'?RenditionId=8"></a>'+
																'<div class="hover-content photo" style="background-image:url('+window.location.protocol+'//'+window.location.hostname + sanitizeUrl(item.FileRef) + '?RenditionID=8'+');">'+
																'<img alt="" class="photoSrc" src="'+item.FileRef+'?RenditionID=8'+'">'+
																'<a href="'+item.ImageLinkUrl+'"><i class="icon-plus-circle"></i>'+item.ImageLinkText+'</a>'+
																'</div></article>';
		});
	
		 strImageGalleryGrid = strImageGalleryGrid + '</div></div>';
		 var ImagesContainer = $('*[data-ajax="buildGallery"]');
		 ImagesContainer.html(strImageGalleryGrid);
		 SetClassforImages();
}


function SetClassforImages(){


//Initialize the variables to set the classes the Image Gallery
var $peopleGrid = $('.people-grid'),
	$peopleArticle = $peopleGrid.find('article'),
	$itemOverlay = $('.pg-item-overlay'),
	$itemOverlayContent,
	$activeItem;

	var itemHeightPG, calcHeightPG,
	itemHeightCG, calcHeightCG, width,
	positionOffset, positionOffsetBottom;

	
	var popupOverlay = {
	open: function(dir, position){
		var promise = popupOverlay.closeAll();
		//moved to click event so it would get the sizes correct prior to the call to popupOverlay.open
		//OverlayitemHeightResize();
		promise.done(function() {
			// Determine Hover Overlay Position
			$itemOverlay.css({
				'top': positionOffset + 'px',
				'height': calcHeightPG + 'px'
			});

			if ($activeItem.hasClass('last-row') && width >= 768 && $peopleArticle.size() > 3) {
				$itemOverlay.addClass('last-row').css({
					'top': positionOffsetBottom + 'px'
				});
			}
			
			if ($peopleArticle.size() <= 3) {
				var singleRowCalcHeightPG = calcHeightPG + 14;
				$peopleGrid.css({'height': singleRowCalcHeightPG + 'px'});
			}

			popupOverlay.content.add();
			if ($itemOverlay.find('iframe').length > 0){
				$itemOverlay.addClass('video');
				$itemOverlay.children().not('#pg-close').hide(0, function(){
					$(this).delay(500).fadeIn(500);
				});
			}
			position && $itemOverlay.removeClass('side-desk side-port').addClass(position);
			$itemOverlay.attr('data-slide-dir', dir).show("slide", { direction: dir }, 500, 'easeOutQuart');
		});
	},
	close: function(dir, callback){
		$itemOverlay.hide({
			effect: 'slide',
			direction: dir,
			speed: 500,
			easing: 'easeInQuart',
			complete: function() {
				callback && callback(dir);
			}
		});
		$itemOverlay.attr('class', 'pg-item-overlay');
	},
	closeAll: function(){
		var dfd = new $.Deferred(),
			dir = $itemOverlay.attr('data-slide-dir');

		this.close(dir, function(){
			popupOverlay.content.remove();
			dfd.resolve();
		});
		return dfd.promise();
	},

	content: {
		add: function(){
			$itemOverlay.append( $itemOverlayContent );
		},
		remove: function(){
			$itemOverlay.children().not('#pg-close').remove();
		}
	}
};


$('#pg-close').on('click', function(e){
	e.preventDefault();
	var dir = $itemOverlay.attr('data-slide-dir');
	popupOverlay.close(dir, function(){
		popupOverlay.content.remove();
		$peopleGrid.removeAttr("style");
	})
});


// ~ Calculate the overlay height based off the height of the article ~
//======================================================
var OverlayitemHeightResize = function(){
	itemHeightPG = $peopleArticle.innerHeight();
	//mobile view
	if (width < 768) {
		calcHeightPG = itemHeightPG + 14;
	}
	else {
		calcHeightPG = (itemHeightPG * 2) + 14;
	}
	$peopleArticle.find('.hover-content iframe').attr('height', calcHeightPG);

};



// ~ Add Class to Every Third Item (Right Column) ~
//======================================================
$peopleArticle.filter(':nth-child(3n+1)').addClass('side-desk');
$peopleArticle.filter(':odd').addClass('side-port');

// ~ Detect Bottom Row of items ~
//======================================================

var articleCount = $peopleArticle.size(),
	remArticle = articleCount % 3;

	if (remArticle > 1) {
		$peopleArticle.slice(-2).addClass('last-row');
	} else if (remArticle > 0) {
		$peopleArticle.slice(-1).addClass('last-row');
	} else {
		$peopleArticle.slice(-3).addClass('last-row');
	}
	

$peopleGrid.on('click', 'article', function(){

	popupOverlay.closeAll();
	OverlayitemHeightResize();
	$activeItem = $(this);

	//if(width >= 768){

		// ~ cache variables
		var $hoverContent = $activeItem.find('.hover-content');

		positionOffset = $activeItem.position().top,
		positionOffsetBottom = (positionOffset - itemHeightPG) - 14;

		// Add Class Based On Content Type
		var type = $hoverContent.hasClass('video')? 'video' : 'text';
		$itemOverlay.addClass(type);

		if ($activeItem.hasClass('side-desk')) {
			$itemOverlay.addClass('side-desk');
		}

		// Clone Content from Selected Article to Overlay
		if ('video' === type) {
			var videoId = $hoverContent.data('videoId'),
				videoUrl = youtubeUrl + videoId + '?' + videoParams.join('&'),
				videoPlayer = $(videoIframe).attr('src', videoUrl);
			$itemOverlayContent = videoPlayer;
		}
		else {
			$itemOverlayContent = $hoverContent.clone();
		}

	// Desktop View
	if(width >= 768){
		// ~ open ~
		if (width > 1023 && $activeItem.hasClass('side-desk')){
			popupOverlay.open('right', 'side-desk');
		}
		else if ( width < 1024 && $activeItem.hasClass('side-port') ) {
			popupOverlay.open('right', 'side-port');
		}
		else {
			popupOverlay.open('left');
		}
	}
	// Mobile View
	else {
		if('video' === type){
			popupOverlay.close();
			window.open(videoUrl);
		}
		if('text' === type){
			popupOverlay.open('left');
		}
	}

	//}
});
$(window).resize(function(){
	width = $(window).width();
}).trigger('resize'); 


}


function sanitizeUrl(data) {
	return data.replace(/ /g,"%20");
}


