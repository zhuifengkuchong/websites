var _LIST_SERVICE_PATH_STOCK_TICKER = "http://services.chk.com/Lists2/StockQuoteServiceData",
  _DEFAULT_STOCK_ORDER = "chkStockQuoteDate desc",
  _DEFAULT_STOCK_FILTER = "chkStockTicker eq 'CHK'",
  _DEFAULT_STOCK_FIELDS = "chkStockExchange,chkStockTicker,chkStockQuoteDate,chkStockPriceCurrent,chkStockPercentChange,chkStockAmountChange",
  _STOCK_HEADER_TEMPLATE = '<ul id="stock-box"><li class="symbol">{{Exchange}} <span>{{Symbol}}</span></li><li>\
                            <ul class="stock-info"><li class="change">{{ChangeFixed}}</li><li class="arrow">{{{ArrowMarkup}}}</li>\
                            <li class="current">{{Price}}</li></ul></li></ul>',
  _STOCK_HERO_TEMPLATE = '<h3>{{Exchange}} <strong>{{Symbol}}</strong></h3><span class="sb-current"><sup>$</sup>{{PriceFixed}}</span>\
                          <div class="sb-change-box">{{{ArrowMarkup}}}<span class="sb-change">{{ChangeFixed}} ({{PercentFixed}}%)</span>\
                          </div><span class="sb-date">As of {{Date}}</span>';

yepnope({
	test: typeof Mustache === 'undefined',
	yep: ['../../../../static.chk.com/mustache/latest/mustache.min.js'/*tpa=http://static.chk.com/mustache/latest/mustache.min.js*/],
	callback: function( url, result, key ) { 
		//console.log( url + " loaded!"); 
	},
	complete: function() {
		//console.log("stock-ticker loaded!"); 
		$(function () {
		  var targets = $("*[data-ajax='buildStockTicker'], *[data-ajax='buildHeroStockTicker']");
		  targets.each( function(i) {
		    requestStockTicker( $(this) );
		  });
		});
	}
});

function requestStockTicker( target ) {
  var parameters = {
                    $top: 1,
                    fields: _DEFAULT_STOCK_FIELDS,
                    $filter: _DEFAULT_STOCK_FILTER,
                    $orderby: _DEFAULT_STOCK_ORDER
                   };
  $.ajax({
    url: _LIST_SERVICE_PATH_STOCK_TICKER,
    dataType: 'JSONP',
    data: parameters,
    type: 'GET',
    success: function (data) { buildStockTicker(target, data); }, 
    error: function() { if ( console.log ) { console.log('Error retrieving stock quote.'); } }
  });

}

function buildStockTicker( target, data ){
  if ( data === undefined ) return;
  if ( data.length === 0  ) return;
  var isHero = ( target.data("ajax").toLowerCase() === "buildherostockticker" ),
    item = data[0];
  target.html( buildStockMarkup( target, item, isHero ) );
}

function buildStockMarkup( target, item, isHero ) {
  var template = ( isHero ) ? _STOCK_HERO_TEMPLATE : _STOCK_HEADER_TEMPLATE,
      stock = new CurrentStock( item );
  return Mustache.to_html( template, stock );
}

function CurrentStock( item ) {
    return { 
            Exchange: item.chkStockExchange, 
            Symbol: item.chkStockTicker,
            Date: item.chkStockQuoteDate,
            Price: item.chkStockPriceCurrent,
            PriceFixed: fixAmount( item.chkStockPriceCurrent ),
            Percent: item.chkStockPercentChange,
            PercentFixed: fixAmount( item.chkStockPercentChange, 1, 100, true ),
            Change: item.chkStockAmountChange,
            ChangeFixed: fixAmount( item.chkStockAmountChange ),
            ArrowMarkup: setArrow( fixAmount( item.chkStockAmountChange ) )
           };
}

function fixAmount( string, decimals, multiplier, flipSign ){
  multiplier = ( multiplier ) ? multiplier : 1;
  decimals = ( decimals !== undefined ) ? decimals : 2;
  flipSign = ( flipSign !== undefined ) ? flipSign : false;
  var isNegative = !string.indexOf( "(" );
  string = string.replace( /[^0-9\.-]+/g, "" );
  string = ( isNegative ) ? "-" + string : string;
  number = ( Number(string)*multiplier ).toFixed( decimals );
  number = ( !flipSign ) ? number : number*-1;
  return number.toString();
}

function setArrow( string ){
  var isNegative = ( Number(string) < 0 )
  return ( isNegative ) ? '<span class="sb-negative"><i class="icon-angle-down"></i></span>' : '<span class="sb-positive"><i class="icon-angle-up"></i></span>';
}
