$(function () {
  var targets = $("*[data-youtube-id]");
  targets.each( function(i) {
    renderVideo( $(this) );
  });
});

function renderVideo( container ) {
  var youtubeUrl = 'http://www.youtube.com/embed/',
  	youtubeExtUrl = 'http://www.youtube.com/watch?v=',
  	videoIframe = '<iframe class="article--video-player" width="100%" height="100%" allowfullscreen frameborder="0" />',
  	videoParams = [
  		'theme=light',
  		'color=white',
  		'wmode=transparent',
  		'modestbranding=1',
  		'iv_load_policy=3',
  		'rel=0',
  		'autohide=1',
  		'vq=hd720',
  		'fs=1',
  		'controls=1'
  	],
    videoId = container.data('youtube-id'),
		youtubeExtUrlBuild = youtubeExtUrl + videoId,
		videoEmbedUrl = youtubeUrl + videoId + '?' + videoParams.join('&'),
		videoPlayer = $(videoIframe).attr('src', videoEmbedUrl);

  	//$articleVideoLink.attr('href', youtubeExtUrlBuild);

	container.html(videoPlayer);
}