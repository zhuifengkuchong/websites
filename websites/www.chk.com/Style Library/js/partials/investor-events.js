var _INVESTOR_EVENTS_SERVICE_URL = "http://services.chk.com/Lists2/ThompsonEvents",
  _THOMPSON_PORTAL_URL = "http://phx.corporate-ir.net/phoenix.zhtml?c=104617";

yepnope([
  {
    test: typeof moment === 'undefined',
    yep: ['../../../../static.chk.com/moment/2.9.0/moment.min.js'/*tpa=http://static.chk.com/moment/2.9.0/moment.min.js*/],
    callback: function() { }
  },
  {
    test: typeof moment().tz === 'undefined',
    yep: ['../../../../static.chk.com/moment-timezone/0.3.0/moment-timezone-2010-2020.min.js'/*tpa=http://static.chk.com/moment-timezone/0.3.0/moment-timezone-2010-2020.min.js*/],
    callback: function() { }
  },
  {
    test: window.ko === undefined,
    yep: '../../../../static.chk.com/knockout/3.1.0/knockout.min.js'/*tpa=http://static.chk.com/knockout/3.1.0/knockout.min.js*/,
    callback: function() { }
  },
  {
    test: window.underscore,
    nope: ['../../../../static.chk.com/underscore/1.6.0/underscore.min.js'/*tpa=http://static.chk.com/underscore/1.6.0/underscore.min.js*/, 
           '../../../../static.chk.com/clndr/1.2.0/clndr.min.js'/*tpa=http://static.chk.com/clndr/1.2.0/clndr.min.js*/],
    callback: function() { },
    complete: function() {
      $(function () {
        var targets = $("*[data-ajax='bindInvestorEvents']");
        targets.each( function() {
          bindInvestorEvents( $(this) );
        });
      });
    }
  }
]);

function bindInvestorEvents( target ) {
    
    var externalUrl = _THOMPSON_PORTAL_URL;
    
    var viewModel = function(data) {
        var self = this;
        self.externalUrl = externalUrl;
        self.postUrl = ko.observable(externalUrl + "&p=irol-eventReminderSuccess&t=EventReminder");
        self.allEvents = ko.observableArray(data);
        self.selectedDay = ko.observableArray();
        self.submitterEmailAddress = ko.observable();
        self.allFutureEventsOrdered = ko.computed(function () {
            return ko.utils.arrayFilter(self.allEvents(), function(item) {
                return moment(item.date()) > moment();
            });
        });
        self.allPastEventsOrdered = ko.computed(function () {
            var pastEvents = ko.utils.arrayFilter(self.allEvents(), function(item) {
                return moment(item.date()) < moment();
            });
            return pastEvents.sort(function(left, right) { return moment(right.date()).diff(moment(left.date())); }).slice(0,5);
        });
        self.togglePastEvents = function(showPastEvents) {
            if (showPastEvents) {
                $(".events-future").slideUp();
                $(".events-past").slideDown();
            }
            else {
                $(".events-future").slideDown();
                $(".events-past").slideUp();
            }
            return false;
        };
    };

    getDataAjax();
    
    function EventItem(startDate, endDate, title, url, eventId, hasDetails, timeZone) {
        this.start = startDate + " " + timeZone;
        this.end = endDate + " " + timeZone;
        this.date = ko.observable( startDate + " " + timeZone ); //clndr
        this.title = ko.observable(title); //clndr
        this.url = ko.observable(url); //clndr
        this.eventId = ko.observable(eventId);
        this.remindMe = ko.observable();
        this.daysPriorToEvent = ko.observable();
        this.hasDetails = ( hasDetails === "Yes" ) ? true : false;
        this.localLongDateText = moment.tz(startDate, "America/Chicago").format('dddd, MMMM Do, YYYY h:mm A z');
        this.localShortDateText = moment.tz(startDate, "America/Chicago").format('MM/DD/YYYY h:mm A z');
        
    }
    
    function getDataAjax() {
        //filter by date
        var tomorrow = moment().add(1,"day").format("YYYY-MM-DD");
        var dataUrl = _INVESTOR_EVENTS_SERVICE_URL,
            param = {
                $filter: "chkThompsonEventDisplayToDate gt datetime'" + tomorrow + "' and chkThompsonEventDisplayFromDate lt datetime'" + tomorrow + "'",
                $orderby: "chkThompsonEventStartDate asc",
                fields: "chkThompsonEventDisplayToDate,chkThompsonEventDisplayFromDate,chkThompsonEventEndDate,chkThompsonEventStartDate,chkThompsonEventTitle,chkThompsonEventID,chkThompsonEventLinkStatus,chkThompsonEventTimeZone"
            };

        $.ajax({
            url: dataUrl,
            dataType: 'JSONP',
            data: param,
            type: 'GET',
            success: function (data) {
                processEvents(data);
            },
            error: function () {
                if (console.log) console.log('Error retrieving investor events.');
            }
        });
    }
    
    function processEvents(data) {
        var mappedData = ko.utils.arrayMap(data, function(item) {
            return new EventItem(item.chkThompsonEventStartDate, item.chkThompsonEventEndDate, item.chkThompsonEventTitle, externalUrl + '&p=irol-EventDetails&EventId=' + item.chkThompsonEventID, item.chkThompsonEventID, item.chkThompsonEventLinkStatus, item.chkThompsonEventTimeZone);
        });
        var vm = new viewModel(mappedData);
        ko.applyBindings(vm);

        //filter data to only show previous 6 months.
        var dateSixMonthsAgo = moment().add(-6,"M");
        var filteredCalData = ko.utils.arrayFilter(mappedData, function(item) {
            return moment(item.date()).diff(moment(dateSixMonthsAgo)) > 0;
        });
        
        var eventsCalendar = $('.events-clndr').clndr({
            template: $('#template-calendar').html(),
            daysOfTheWeek: ['Su','Mo','Tu','We','Th','Fr','Sa'],
            events: filteredCalData,
            multiDayEvents: {
                startDate: 'start',
                endDate: 'end'
            },
            startWithMonth: moment(),
            clickEvents: {
                click: function(target) {
                    if( target.events.length > 0 ) {
                        //create html for array of events
                        //var mappedEventData = ko.utils.arrayMap(target.events, function(item) {
                        //    return new EventItem(item.date(), item.endDate, item.title(), externalUrl + '&p=irol-EventDetails&EventId=' + item.eventId(), item.eventId(), item.hasDetails, item.timeZone() );
                        //});
                        //vm.selectedDay(mappedEventData);
                        vm.selectedDay( target.events );
                    }
                }
            },
            doneRendering: function () {
              $(".clndr-grid .today").click();
            }
        });
    }
        
}