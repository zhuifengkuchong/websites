// USER BOX MOUSE OVER FUNCTION

var currId,
	isIE = document.body.style.msTouchAction !== undefined,
	$html = $("html");

if ($html.hasClass('touch') && $html.hasClass('safari5') && $html.hasClass('mobile') && $html.hasClass('ipad')) {
	yepnope({
        load: ['../../../../static.chk.com/blackandwhite/0.2.8/jquery.BlackAndWhite.min.js'/*tpa=http://static.chk.com/blackandwhite/0.2.8/jquery.BlackAndWhite.min.js*/, 'ie-grayscale.js'/*tpa=http://www.chk.com/style library/js/partials/ie-grayscale.js*/]
    });
}

if (isIE) {
    yepnope({
        load: ['../../../../static.chk.com/blackandwhite/0.2.8/jquery.BlackAndWhite.min.js'/*tpa=http://static.chk.com/blackandwhite/0.2.8/jquery.BlackAndWhite.min.js*/, 'ie-grayscale.js'/*tpa=http://www.chk.com/style library/js/partials/ie-grayscale.js*/]
    });
}

var userBoxDestroy = function(){
	// $('.login-box li').unbind('mouseenter mouseleave');
	$(".login-box li").unbind("mouseenter").unbind("mouseleave");
	$(".login-box li").removeProp('hoverIntent_t');
	$(".login-box li").removeProp('hoverIntent_s');
};

var userBoxInit = function(){
	$('.login-box li').mouseenter(function(){
		currId = $(this).attr('id');
		$('.login-box li').removeClass('active');
		$(this).addClass('active');
		$('.login-paragraph').removeClass('active');
		$('#p-' + currId).addClass('active');
	});

	$('.login-box').mouseleave(function(){
		$('.login-box li').removeClass('active');
		$('.login-paragraph').removeClass('active');
		$('#p-default').addClass('active');

	});

};

var userBoxIn = function(){
	currId = $(this).attr('id');
	$('.login-box li').removeClass('active');
	$(this).addClass('active');
	$('.login-paragraph').removeClass('active');
	$('#p-' + currId).addClass('active');
};

var userBoxOut = function(){
	$('.login-box li').removeClass('active');
	$('.login-paragraph').removeClass('active');
	$('#p-default').addClass('active');
};

var blankFunction = function(){
	$.noop();
};

var userBoxInitIntent = function(){
	$('.login-box li').hoverIntent(userBoxIn, blankFunction);
	$('.login-box').hoverIntent(blankFunction, userBoxOut);
};

// $('.login-box li').mouseenter(function(){
// 	currId = $(this).attr('id');
// 	$('.login-box li').removeClass('active');
// 	$(this).addClass('active');
// 	$('.login-paragraph').removeClass('active');
// 	$('#p-' + currId).addClass('active');
// });

// $('.login-box').mouseleave(function(){
// 	$('.login-box li').removeClass('active');
// 	$('.login-paragraph').removeClass('active');
// 	$('#p-default').addClass('active');

// });


// // bind the hoverIntent
// $("#demo1 li").hoverIntent(makeTall, makeShort)
// // unbind the hoverIntent
// $("#demo1 li").unbind("mouseenter").unbind("mouseleave");
// $("#demo1 li").removeProp('hoverIntent_t');
// $("#demo1 li").removeProp('hoverIntent_s');
