var _INVESTOR_SEC_SERVICE_URL = "http://services.chk.com/Lists2/ThompsonSECFilings",
    _INVESTOR_SEC_ORDERBY = "chkThompsonDateFiled desc",
    _INVESTOR_SEC_FIELDS = "chkThompsonDateFiled,chkThompsonYear,chkThompsonDocLink,chkThompsonXSLLink,chkThompsonPDFLink,chkThompsonHtmlLink,chkThompsonDescription,chkThompsonForm,chkThompsonFilingGroup,chkThompsonFilingID,chkHidden";
    
$.holdReady(true);
yepnope([{
		test: window.ko,
		nope: '../../../../static.chk.com/knockout/3.1.0/knockout.min.js'/*tpa=http://static.chk.com/knockout/3.1.0/knockout.min.js*/,
		callback: function( url, result, key ) { 
			//console.log( url + " loaded!");
		},
		complete: function() {
			//console.log("investorSEC load completed");
			$.holdReady(false);
		}

	}
]);

$(function () {
  var targets = $("*[data-ajax='buildSecFilings']");
  targets.each( function(i) { buildSecFilings( $(this) ); });
});
    
function buildSecFilings( target ) {
    var viewModel = function (data) {
        var self = this;
        self.keyWordSearch = ko.observable();
        self.selectedYear = ko.observable();
        self.selectedGroup = ko.observable();
        self.allData = ko.observableArray(data);
        self.allYears = ko.computed(function() {
            var years = ko.utils.arrayMap(self.allData(), function(item) {
                return sanitizeNumber(item.chkThompsonYear, 0);
            });
            return ko.utils.arrayGetDistinctValues(years).sort(function(left,right) {return left > right ? -1 : 1;});
        });
        self.allGroups = ko.computed(function() {
            var groups = ko.utils.arrayMap(self.allData(), function(item) {
                return item.chkThompsonFilingGroup;
            });
            return ko.utils.arrayGetDistinctValues(groups).sort();
        });
        self.itemCount = ko.observable(20);
        self.filteredItems = ko.computed(function () {
            var test =  ko.utils.arrayFilter(self.allData(), function (item) {
                var filterSearchText = isNullOrEmpty(self.keyWordSearch()) || item.chkThompsonDescription.toLowerCase().indexOf(self.keyWordSearch().toLowerCase()) !== -1
                var filterYear = isNullOrEmpty(self.selectedYear()) || sanitizeNumber(item.chkThompsonYear, 0) == self.selectedYear()
                var filterGroup = isNullOrEmpty(self.selectedGroup())  || item.chkThompsonFilingGroup == self.selectedGroup()
                return filterSearchText && filterYear && filterGroup;
            });
            return test.slice(0,self.itemCount());
        });
        self.showMore = function(count) {
            var more = self.itemCount() + count;
            if (isNaN(more))
                more = count;
            
            self.itemCount(more);
        };
    };
    getDataAjax();

    function isNullOrEmpty(value) {
        //if (!value) { return true; }
        //else { return false; }
        return !value
    };
    
    function getDataAjax() {
        $.ajax({
            url: _INVESTOR_SEC_SERVICE_URL,
            dataType: 'JSONP',
            data: { $orderby: _INVESTOR_SEC_ORDERBY, fields: _INVESTOR_SEC_FIELDS, $filter: "chkHidden ne 'Yes'" },
            type: 'GET',
            success: function (data) { processSecFilings(data); },
            error: function () { if (console.log) { console.log('Error retrieving SEC filing information.'); } }
        });
    };

    function processSecFilings(data) {
        ko.applyBindings(new viewModel(data));
        /*$("td[data-title='Form']>a").each( function(i){ 
          var target = $(this),
            url = target.attr("href");
          target.after( '<a href="#" onclick="javascript:void window.open(\'' + url + '\',\'\', \'status=0,scrollbars=1,resizable=1\');">test</a>' );
        });*/
    };
    
    function sanitizeNumber( string, decimals, multiplier ){
      multiplier = ( multiplier ) ? multiplier : 1;
      decimals = ( decimals !== undefined ) ? decimals : 2;
      var isParentheticalNegative = !string.indexOf( "(" );
      string = string.replace( /[^0-9\.-]+/g, "" );
      string = (isParentheticalNegative) ? "-" + string : string;
      return ( Number(string)*multiplier ).toFixed( decimals ).toString();
    }

}


/*
<div data-ajax="buildSecFilings">
    <div class="sec-filters">
        <div class="filter-set keyword-set">
            <label>Keyword Search</label>
            <input data-bind="value: keyWordSearch" />
            <button>SUBMIT</button>
        </div>
    </div>
    <div class="sec-filters">
        <div class="filter-set year-set">
            <label>Year Filter</label>
            <select data-bind="options:allYears, value:selectedYear, optionsCaption: 'All years'"></select>        
        </div>
        <div class="filter-set group-set">
            <label>Group Filter</label>
            <select data-bind="options:allGroups, value:selectedGroup, optionsCaption: 'All groups'"></select>
        </div>
    </div>
    <table class="investors sec-filing">
        <thead>
            <tr><th>Filing Date</th><th>Form</th><th>Description</th><th>Filing Group</th><th>Download</th></tr>
        </thead>
        <tbody data-bind="foreach: filteredItems">
            <tr>
                <td data-bind="text: moment(chkThompsonDateFiled).format('L');" data-title="FilingDate"></td>
                <td data-bind="html: '<a href=http://services.corporate-ir.net/SEC.Enhanced/SecCapsule.aspx?c=104617&fid=' + chkThompsonFilingID + '>' + chkThompsonForm + '</a>'" data-title="Form"></td>
                <td data-bind="text: chkThompsonDescription" data-title="Description"></td>
                <td data-bind="text: chkThompsonFilingGroup" data-title="FilingGroup"></td>
                <td data-title="Download">
                    <a data-bind="visible: chkThompsonPDFLink, html: '<span class=btn-download><i class=icon-download></i></span>', attr: {href: chkThompsonPDFLink}"></a>
                </td>
            </tr>
        </tbody>
    </table>
    <input type="button" data-bind="click: showMore(10)" value="Show More"/> 
</div>
*/
