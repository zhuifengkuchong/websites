$(function() {

  
  var $stockChart = $('#container');
  var chkChartSeriesId = 1;

  $.holdReady(true);

  yepnope([{
		test: typeof moment === 'undefined',
		yep: ['../../../../static.chk.com/moment/2.9.0/moment.min.js'/*tpa=http://static.chk.com/moment/2.9.0/moment.min.js*/],
		callback: function() { }
	},{
		test: typeof Highcharts === 'undefined',
		yep: ['../../../../static.chk.com/highstock/2.0.1/highstock.min.js'/*tpa=http://static.chk.com/highstock/2.0.1/highstock.min.js*/],
		callback: function() {}
	},{
    test: window.ko,
    nope: '../../../../static.chk.com/knockout/3.1.0/knockout.min.js'/*tpa=http://static.chk.com/knockout/3.1.0/knockout.min.js*/,
    callback: function() {},
    complete: function() {
      $.holdReady(false);
      readySetGo();
  }}]);
	
	function readySetGo() {
		var compareSymbols = ["SP500","XLE","EPX"].sort();
		var viewModel = function(stockSymbols, selectedSymbol) {
        var self = this;
        self.stockSymbols = ko.observableArray(stockSymbols);
        self.selectedSymbol = ko.observable(selectedSymbol);
        self.filteredStockSymbols = ko.computed(function () {
          var allSymbols = ko.utils.arrayMap(self.stockSymbols(), function(item) {
            return item.stockSymbol;
          }).sort();
          var differences = ko.utils.compareArrays(allSymbols,compareSymbols);
          var results = [];
          ko.utils.arrayForEach(differences, function(difference) {
            if (difference.status === "deleted") {
              results.push(difference.value);
            }
          });
          results = results.sort();
          
          return ko.utils.arrayFilter(self.stockSymbols(), function(item) {
            return $.inArray(item.stockSymbol, results) === -1;
          });
        });
      };
    
      var emptyObject = {stockSymbol: 'CHK', priceHistory: null, volumeHistory: null }
    
      createChart(emptyObject);
    
      $stockChart.highcharts().showLoading('<div>Loading stock price data...</div><br><div><img src="../../img/chart-loader.gif"/*tpa=http://www.chk.com/Style Library/img/chart-loader.gif*//></div>');
		  
      var startDateRange = moment().add(-1,"y").format("YYYY-MM-DD");
      var endDateRange = moment().add(1,"d").format("YYYY-MM-DD");
      getAjaxData(startDateRange, endDateRange);


    
      function getAjaxData(startDate, endDate) {    
        
        var dataSet = [],
          dataUrl = "http://services.chk.com/Lists2/StockQuoteServiceData",
          param = {
            fields: "chkStockTicker,chkStockQuoteDate,chkStockPricePreviousClose,chkStockVolume",
            $filter: "chkStockQuoteDate gt datetime'" +startDate+"' and chkStockQuoteDate lt datetime'"+endDate+"'",
            $orderby: "chkStockQuoteDate"
          };
        $.ajax({
          dataType: "jsonp",
          url: dataUrl,
          data: param,
          success: function(data) {
    
            var seriesData = [],
              uniqueStockSymbols = 
                ko.utils.arrayGetDistinctValues(ko.utils.arrayMap(data,function(item) { return item.chkStockTicker; }));
            
            uniqueStockSymbols.sort( function( left, right ) { return left === 'CHK' ? 1 : -1; } );
            
            $.each(uniqueStockSymbols, function( index, value ) {
              var stockSymbol = value, 
                stockHistory = ko.utils.arrayFilter(data, function(item) {
                  var currentDay = moment().format("yyyy-mm-dd");
                  if (moment(item.chkStockQuoteDate, "MM/DD/YYYY h:mm A").format("YYYY-MM-DD") === currentDay)
                    return false;
                  else
                    return item.chkStockTicker === stockSymbol;
                }),
                priceHistory = ko.utils.arrayMap(stockHistory, function(point) {
                  return [moment(point.chkStockQuoteDate, "MM/DD/YYYY h:mm A").unix() * 1000, 
                    parseFloat(point.chkStockPricePreviousClose.replace('$','').replace(/,/g,''))];
                }).sort(function(left,right){
                  return moment(new Date(left[0])).diff(moment(new Date(right[0])));
                }),
                volumeHistory = ko.utils.arrayMap(stockHistory, function(point) {
                  return [moment(point.chkStockQuoteDate, "MM/DD/YYYY h:mm A").unix() * 1000, 
                    parseFloat(point.chkStockVolume.replace(/,/g,''))];
                }).sort(function(left,right){
                  return moment(new Date(left[0])).diff(moment(new Date(right[0])));
                });
              
              seriesData.push({stockSymbol: stockSymbol, priceHistory: priceHistory, volumeHistory: volumeHistory});
            });
            
            var chkData = ko.utils.arrayFirst(seriesData, function(item) {
                return item.stockSymbol === "CHK";
            });
    
            createChart(chkData);
            
            //populate viewModel
            viewModel = new viewModel(seriesData,"CHK");
            ko.applyBindings(viewModel);
            viewModel.selectedSymbol.subscribe( function( newValue ) {
                dropdownChanged = true;
                stockSymbolChanged(newValue);
            });
            
          }
        });
        
      }
  
      function stockSymbolChanged(data) {
        var comparePriceId = -999,
          compareVolumeId = -998,
          chart = $stockChart.highcharts(),
          comparePriceSeries = chart.get(comparePriceId),
          compareVolumeSeries = chart.get(compareVolumeId),
          chkPriceSeries = chart.get(chkChartSeriesId);
        
        if (comparePriceSeries !== null) {
            comparePriceSeries.remove();
        };
        if (compareVolumeSeries !== null) {
            compareVolumeSeries.remove();
        };
        
        if (data) {
          if (chkPriceSeries !== null) {
            chkPriceSeries.update({ compare: 'percent' });
          };
          chart.addSeries({
            id: comparePriceId,
            compare: 'percent',
            name: data.stockSymbol,
            data: data.priceHistory,
            color: '#0084b6'
          });
    			chkPriceSeries.update({ type: 'line' });
        }
        else {
          if (chkPriceSeries !== null) {
            chkPriceSeries.update({ compare: undefined, type: 'area' });
          };
        };
      }
        
      function updateChartData(data) {
        if($stockChart.highcharts().series) {
          $stockChart.highcharts().series[0].setData(data.priceHistory);
          $stockChart.highcharts().series[1].setData(data.volumeHistory);
        };
        $stockChart.highcharts().hideLoading;
      }

      function createChart(data) {
    		
        $stockChart.highcharts('StockChart', {
    			
    			exporting: { enabled: false },
    			
    			credits: { enabled: false },
          
          navigator: { maskFill: 'rgba(0, 185, 227, 0.25)', outlineColor: '#00b9e3', outlineWidth: 1, series: { lineColor: '#999', color: '#eee' } },
    			
    			rangeSelector: {
    				inputEnabled: false,
    				selected: 3,
    				buttons: [
    					{ type: 'day', count: 10, text: '10d' }, 
    					{ type: 'month', count: 1, text: '1m' }, 
    					{ type: 'month', count: 3, text: '3m' }, 
    					{ type: 'ytd', text: 'YTD' }, 
    					/*{ type: 'year', count: 1, text: '1y' },*/
    					{ type: 'all', text: '1y' }
    				],
    				buttonTheme: { 
    					fill: '#fff',
    					stroke: '#00b9e3',
    					'stroke-width': 1,
    					r: 0,
    					style: { color: '#00b9e3', fontWeight: 'normal' },
    					states: {
    						hover: {
    							fill: '#fff',
    							stroke: '#014c7b',
    							style: { color: '#014c7b', fontWeight: 'normal' }
    						},
    						select: {
    							fill: '#00b9e3',
    							stroke: '#00b9e3',
    							style: { color: 'white', fontWeight: 'normal' }
    						}
    					}
    				},
    				inputBoxBorderColor: '#00b9e3',
    				inputBoxHeight: 18,
    				inputBoxWidth: 120
    			},
          
          yAxis: [
            {
              labels: { align: 'left', x: 10, formatter: function() { return '$'+this.value; } },
              height: '60%',
              offset: 2
            }, 
            {
              labels: { align: 'left', x: 10 },
              top: '65%',
              height: '35%',
              offset: 2
            }
          ],

          xAxis: {
		        type: 'datetime',
              dateTimeLabelFormats: { day: '%b %e \'%y', week: '%b %e \'%y' },
              labels: { staggerLines: 2 }
          },
        
          series: [
            {
              id: chkChartSeriesId,
              name: 'CHK',
              shadow: false,
      				threshold : null,
              color: '#42a145',
              data: data.priceHistory,
      				type: 'area',
      				index: 7,
      				area: { fillColor: '#42a145', fillOpacity: .65 }, 
            },{
              type: 'area',
              name: 'CHK Volume',
              step: true,
              yAxis: 1,
              data: data.volumeHistory,
              color: '#9BBF1E',
      				index: 8,
      				area: { fillColor: '#9BBF1E', fillOpacity: 1.00, showInLegend: false }
            }
          ],
          
    			legend: { enabled: true, align: 'center' },
    
          tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <strong>${point.y}</strong><br/>',
        	  valueDecimals: 2,
    			  shared: true
          }
          
        });
      }
	};
});
