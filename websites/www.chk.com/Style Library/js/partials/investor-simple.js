var _INVESTOR_SERVICE_URL = "http://services.chk.com/Lists",
    _INVESTOR_SERVICE2_URL = "http://services.chk.com/Lists2",
    _INVESTOR_SIMPLE_TARGET_IDS = ["buildAnalystCoverage", 
                                   "buildDividendSplitHistory", 
                                   "buildAnnualQuarterlyReports",
                                   "upcomingEvents"];
$(function () {

  yepnope([
    {
      test: typeof Mustache === 'undefined',
      yep: ['../../../../static.chk.com/mustache/latest/mustache.min.js'/*tpa=http://static.chk.com/mustache/latest/mustache.min.js*/]
    },
    {
      test: typeof moment === 'undefined',
      yep: ['../../../../static.chk.com/moment/2.9.0/moment.min.js'/*tpa=http://static.chk.com/moment/2.9.0/moment.min.js*/],
      callback: function() { }
    },
    {
      test: typeof moment().tz === 'undefined',
      yep: ['../../../../static.chk.com/moment-timezone/0.3.0/moment-timezone-2010-2020.min.js'/*tpa=http://static.chk.com/moment-timezone/0.3.0/moment-timezone-2010-2020.min.js*/],
      callback: function() { },
      complete: function() {
        $(function () {
          // loop through the target ids, see if any elements on the page are using them
          for (var i = 0; i < _INVESTOR_SIMPLE_TARGET_IDS.length; i++) {
              var selector = "*[data-ajax='" + _INVESTOR_SIMPLE_TARGET_IDS[i] + "']",
                  targets = $(selector);
              // see if there are any target elements on the page for this target id
              // if so, get the data and build the table or list
              if ( targets.length > 0 ) { 
                targets.each( function() { 
                  RequestData( $(this) ); 
                }); 
              }
          }
          var formTargets = $("*[data-ajax='buildEmailAlertForm']");
          formTargets.each( function() {
            buildEmailAlertForm( $(this) );
          });
        });
      }
    }
  ]);
  
  
  function RequestData( element ) {
      var errorText = "Error retrieving investor data [DataAjax - " + element.data("ajax") + "]",
        settings = BuildInvestorSettings( element );
      
      $.ajax({
          url: settings.url, 
          dataType: 'JSONP',
          data: settings.parameters,
          type: 'GET',
          success: function (data) { BuildInvestorTable( element, data ); },
          error: function () { if (console.log) { console.log( errorText ); }
          }
      });
  }
    
  function BuildInvestorTable( element, data ) {
    var errorText = "Investor ajax id not found [BuildTable - " + element.data("ajax") + "]";
    switch ( element.data("ajax") ) {
      case "buildAnalystCoverage" : 
        RenderAnalystCoverageTable( element, data );
        break;
      case "buildAnnualQuarterlyReports" : 
        RenderAnnualQuarterlyReports( element, data );
        break;
      case "buildDividendSplitHistory" : 
        RenderDividendSplitHistoryTable( element, data );
        break;
      case "upcomingEvents" : 
        RenderUpcomingEventsList( element, data );
        break;
      default : if ( console.log ) { console.log( errorText ); }
    } 
  }
  
  function RenderAnalystCoverageTable(target, data) {
      var table = $("<table/>").addClass("investors").addClass("analyst-coverage"),
          header = $("<thead/>"),
          body = $("<tbody/>"),
          tabs = $("<div/>").attr("id", "divTab");
  
      header.append("<th>Firm</th><th>Analyst</th><th>Phone</th>");
  
      for (i = 0; i < data.length; i++) {
        // if modified since yesterday
        if ( moment( data[i].Modified ).isAfter( moment().subtract(1, 'days') ) ) {
          var row = $("<tr/>"),
              firmCol = $("<td/>").attr("data-title","Firm"),
              AnalystCol = $("<td/>").attr("data-title","Analyst"),
              PhoneCol = $("<td/>").attr("data-title","Phone");
  
          firmCol.append(data[i].Company);
          var email = data[i].EMail;
          AnalystCol.append("<a href=mailto:" + email + ">" + data[i].FirstName + " " + data[i].chkPersonLastName + "</a>");
          PhoneCol.append(data[i].PrimaryNumber);
  
          firmCol.appendTo(row);
          AnalystCol.appendTo(row);
          PhoneCol.appendTo(row);
          row.appendTo(body);
        }
      }
      tabs.appendTo(target);
      header.appendTo(table);
      body.appendTo(table);
      table.appendTo(target);
  }
  
  function RenderDividendSplitHistoryTable(target, data) {
      var table = $("<table/>").addClass("investors").addClass("dividend-split-history"),
          header = $("<thead/>"),
          body = $("<tbody/>"),
          tabs = $("<div/>").attr("id", "divTab");
  
      header.append("<th>Ex-Dividend</th><th>Record</th><th>Payable</th><th>Amount</th><th>Type</th>");
  
      for (i = 0; i < data.length; i++) {
          var row = $("<tr/>"),
              executeDateCol = $("<td/>").attr("data-title","Ex-Dividend"),
              recordDateCol = $("<td/>").attr("data-title","Record"),
              payableDateCol = $("<td/>").attr("data-title","Payable"),
              amountCol = $("<td/>").attr("data-title","Amount"),
              typeCol = $("<td/>").attr("data-title","Type");
  
          executeDateCol.append(moment(data[i].chkThompsonExecuteDate).format("L")).appendTo(row);
          recordDateCol.append(moment(data[i].chkThompsonRecordDate).format("L")).appendTo(row);
          payableDateCol.append(moment(data[i].chkThompsonPayableDate).format("L")).appendTo(row);
          amountCol.append(data[i].chkThompsonAmount).appendTo(row);
          typeCol.append(data[i].chkThompsonClass).appendTo(row);
  
          row.appendTo(body);
      }
      tabs.appendTo(target);
      header.appendTo(table);
      body.appendTo(table);
      table.appendTo(target);
  }
  
  function RenderAnnualQuarterlyReports(target, data) {
      var table = $("<table/>").addClass("investors").addClass("annual-quarterly-reports"),
          header = $("<thead/>"),
          body = $("<tbody/>"),
          tabsList = $("<ul/>").addClass("ui-tabs-nav"),
          annualAnchor = $("<a/>").attr("href", "#AR").text("Annual Reports"),
          otherAnchor = $("<a/>").attr("href", "#OR").text("Quarterly & Other Reports"),
          annualTabItem = $("<li/>").addClass("ui-state-default").append( annualAnchor ),
          otherTabItem = $("<li/>").addClass("ui-state-default").append( otherAnchor );
      
      target.empty();
      
      annualAnchor.click( function( ev ) { onTabClick("AR", $(this) ); } );
      otherAnchor.click(function( ev ) { onTabClick("OR", $(this) ); } );
      
      if ( target.data("reportType") === "OR" ) { otherTabItem.addClass("ui-state-active"); }
      else { annualTabItem.addClass("ui-state-active"); }
      tabsList.append(annualTabItem).append(otherTabItem);
      
  
      header.append("<th>Date</th><th>Report</th>");
  
      for (i = 0; i < data.length; i++) {
          var row = $("<tr/>"),
              documentMoment = moment( data[i].chkThompsonDocumentDate, "MM/DD/YYYY h:mm A" );
              dateCell = $("<td/>").attr( "data-title", "Date" ).text( documentMoment.format("L") ),
              titleCell = $("<td/>").attr( "data-title", "Report" ),
              anchor = $("<a/>").addClass("pdf-icon").addClass("downloadable").attr("href", data[i].chkThompsonDocumentURL.split(", ")[0]).attr("target", "_blank").text( data[i].chkThompsonDocumentTitle ),
              icon = $("<span/>").addClass("btn-download").append( $("<i/>").addClass("icon-download") );
          
          anchor.prepend( icon );
          titleCell.append( anchor );
          dateCell.appendTo( row );
          titleCell.appendTo( row );
          row.appendTo( body );
      }
      tabsList.appendTo( target );
      header.appendTo( table );
      body.appendTo( table );
      table.appendTo( target );
      
    function onTabClick(type, element) {
        var target = $("*[data-ajax='buildAnnualQuarterlyReports']");
        element.parent().parent().find(">li").removeClass("ui-state-active");
        element.parent().addClass("ui-state-active");
        if (type === "AR") { target.data("reportType", "AR");  }
        else if (type === "OR") { target.data("reportType", "OR"); }
  
        RequestData(target);
  
        return false;
    }
  }

  function RenderUpcomingEventsList( target, data ) {
    var _THOMPSON_URL_PREFIX = "http://phx.corporate-ir.net/phoenix.zhtml?c=104617&p=irol-EventDetails&EventId=",
      _TEMPLATE_LINKED = '<li><a href="{{Url}}"><p>{{Title}}</p><time>{{Weekday}}, {{LongMonth}} {{Day}}, {{Year}} <br>\
                          {{Time}}</time><aside>More Information<i class="icon-angle-right"></i></aside></a></li>',
      _TEMPLATE_UNLINKED = '<li><p>{{Title}}</p><time>{{Weekday}}, {{LongMonth}} {{Day}}, {{Year}} <br>{{Time}}</time></li>',
      _TEMPLATE_EMPTY = '<li>No events are currently scheduled</li>';

    target.addClass("upcoming-events");
    var list = $("<ul/>");
  
    $.each(data, function(i, item){
      var template = ( item.chkThompsonEventLinkStatus === "Yes" ) ? _TEMPLATE_LINKED : _TEMPLATE_UNLINKED;
      list.append( Mustache.to_html( template, UpcomingEvent( item ) ) );
    });
    if ( data.length === 0 ) list.append( _TEMPLATE_EMPTY );
    target.html( list );

    function UpcomingEvent( item ) {
      var startMoment = moment.tz(item.chkThompsonEventStartDate, "America/Chicago");
      return { 
          Date: startMoment,
          ShortWeekday: startMoment.format('ddd'),
          Weekday: startMoment.format('dddd'),
          ShortMonth: startMoment.format('MMM'),
          LongMonth: startMoment.format('MMMM'),
          Day: startMoment.format('DD'),
          Month: startMoment.format('MM'),
          Year: startMoment.format('YYYY'),
          Time: startMoment.format('h:mm A z'),
          StartDate: startMoment,
          EndDate: moment.tz(item.chkThompsonEventEndDate, "America/Chicago"),
          Title: item.chkThompsonEventTitle,
          Url: _THOMPSON_URL_PREFIX + item.chkThompsonEventID,
          Id: item.chkThompsonEventID
      };
    }
  }

  function buildEmailAlertForm( target ) {
    var url = "http://phx.corporate-ir.net/phoenix.zhtml?c=104617&p=irol-alertsLong&t=AlertPost",
      formElement = $("<form/>").attr("name", "EmailAlert").attr("method", "post").attr("action", url),
      addressInput = $('<input type="email"/>').attr("required", "").attr("name", "control_AlertEmail"),
      submitButton = $('<button type="submit">').text("Sign up").addClass("btn-box").append( $('<i/>').addClass("icon-angle-right") ),
      errorInput = $('<input type="hidden"/>').attr("name", "ErrorLanguage").attr("value", ""),
      captchaInput = $('<input type="hidden"/>').attr("name", "control_CaptchaModuleType").attr("value", "Alerts"),
      basepageInput = $('<input type="hidden"/>').attr("name", "basepage").attr("value", "irol-alerts");
      
    formElement.append(addressInput).append(submitButton).append(errorInput).append(captchaInput).append(basepageInput);
    target.html(formElement);
  }
  
  
  function BuildInvestorSettings( element ) {
    var errorText = "Investor ajax id not found [BuildParams - " + element.data("ajax") + "]";
    switch ( element.data("ajax") ) {
      case "buildAnalystCoverage" : return { 
        url: _INVESTOR_SERVICE_URL, 
        parameters: {
          list: "ThompsonAnalyst", 
          $orderby: "Company" 
        }
      };
      case "buildAnnualQuarterlyReports" : return { 
        url: _INVESTOR_SERVICE2_URL + "/ThompsonReportsPresentations",
        parameters: {
          fields: "chkThompsonDocumentTitle,chkThompsonDocumentDate,chkThompsonDocumentCode,chkThompsonDocumentID,chkThompsonDocumentType,chkThompsonDocumentURL,chkThompsonCodec,chkThompsonIsActive",
          $filter: "chkThompsonDocumentCode eq '" + element.data("reportType") + "'",
          $orderby: "chkThompsonDocumentDate desc,chkThompsonDocumentTitle desc",
        }
      };
      case "buildDividendSplitHistory" : return { 
        url: _INVESTOR_SERVICE_URL,
        parameters: {
          list: "ThompsonDividendSplitHistory", 
          $orderby: "chkThompsonExecuteDate desc" 
        }
      };
      case "upcomingEvents" : return {
        url: _INVESTOR_SERVICE2_URL + "/ThompsonEvents",
        parameters: {
          $filter: "chkThompsonEventDisplayToDate gt datetime'" + moment().add(0,'d').format("YYYY-MM-DD") + "' and chkThompsonEventDisplayFromDate lt datetime'" + moment().add(60,'d').format("YYYY-MM-DD") + "' and chkThompsonEventStartDate gt datetime'" + moment().add(0,'d').format("YYYY-MM-DD") + "' and chkThompsonEventStartDate lt datetime'" + moment().add(60,'d').format("YYYY-MM-DD") + "'",
          $orderby: "chkThompsonEventEndDate",
          fields: "chkThompsonEventDisplayToDate,chkThompsonEventDisplayFromDate,chkThompsonEventEndDate,chkThompsonEventStartDate,chkThompsonEventTitle,chkThompsonEventID,chkThompsonEventLinkStatus",
          $top: ( element.attr("data-event-count")  === undefined ) ? 3 : element.attr("data-event-count")
        }
      };
      default : if ( console.log ) { console.log( errorText ); }
    }
  }
  
});