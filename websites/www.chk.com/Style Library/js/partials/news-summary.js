// create a settings obect to preserve state, and a filter object
// should proably add a namespace to this one day
var Settings,
    Filters = { 
      YEAR: { value: 0, name: "year", displayname: "years", column: "chkCalculatedYear" }, 
      LOCATION: { value: 1, name: "location", displayname: "locations", column: "chkNewsCategory" },
      TOPIC: { value: 2, name: "topic", displayname: "topics", column: "chkNewsTopics" },
      TAG: { value: 3, name: "tag", displayname: "tags", column: "chkTags" }
    };

// create some "constants"
var _LIST_SERVICE_PATH_NEWS_SUMMARY = "http://services.chk.com/Lists2",
  _DEFAULT_NEWS_LIST_NAME = "Pages",
  _DEFAULT_NEWS_SUBSITE = "News",
  _DEFAULT_NEWS_PATH = "/media/news",
  _DEFAULT_NEWS_ORDER = "chkRollupPinToTop desc,chkContentDate desc",
  _DEFAULT_NEWS_FILTER = "not chkNewsCategory eq ''",
  _DEFAULT_NEWS_SELECT = "Title,chkCalculatedYear,chkNewsCategory,chkNewsTopics,chkTags,chkRollupHideOnHomePage,chkRollupPinToTop,chkRollupSortOrder,chkContentDate,chkContentHeadline,chkNewsCategory,chkContentSummary,chkExternalSourceContentId,chkTagsTaxHTField0",

  // default settings
  _DEFAULT_ARTICLE_COUNT = 10000,
  _DEFAULT_NEWS_YEAR = "All years",
  _DEFAULT_NEWS_LOCATION = "All locations",
  _DEFAULT_NEWS_TOPIC = "All topics",
  _DEFAULT_NEWS_TAG = "All tags",
  _DEFAULT_MORE_LINK = "/media/news",
  _DEFAULT_RSS_LINK = "/media/news/rss",
  _DEFAULT_TEMPLATE_NAME = "summary", //homepage, archive, summary
  _DEFAULT_SIDEBAR_FILTER = ""
  _DEFAULT_ALL_ITEMS_OPTION = true,
  _DEFAULT_SHOW_YEARS = false,
  _DEFAULT_SHOW_LOCATIONS = false,
  _DEFAULT_SHOW_TOPICS = false,
  _DEFAULT_SHOW_TAGS = false,
  _DEFAULT_SHOW_TAGS_AS_DROPDOWN = false,
  _DEFAULT_ONLY_SHOW_PRESS_RELEASES = false,
  _DEFAULT_HIDE_PRESS_RELEASES = false,

  // news templates
  _NEWS_SUMMARY_TEMPLATE = '<li><a href="{{Url}}"><span><time datetime="{{Timestamp}}">{{Weekday}}, {{LongMonth}} {{Day}}, {{Year}}</time><p>{{Headline}}</p><aside>Read More <i class="icon-angle-right"></i></aside></span></a></li>',
  _HOMEPAGE_NEWS_SUMMARY_TEMPLATE = '<article><a href="{{Url}}"><span><time datetime="{{Timestamp}}">{{Weekday}} <strong>{{ShortMonth}} {{Day}}</strong> {{Year}}</time><p>{{Headline}}</p><aside>Read More <i class="icon-angle-right"></i></aside></span></a></article>',
  // footer templates
  _NEWS_FOOTER_TEMPLATE = '<li class="no-hover news-footer"><a class="full-feed-link" href="{{MoreUrl}}">Full News Feed</a><a href="{{RssUrl}}"> <i class="icon-rss"></i> RSS Feed</a></li>',
  _HOMEPAGE_NEWS_FOOTER_TEMPLATE = '<article class="no-hover news-footer"><span class="no-hover"><a class="full-feed-link" href="{{MoreUrl}}">Full News Feed</a><a href="{{RssUrl}}"> <i class="icon-rss"></i> RSS Feed</a></span></article>',
  // empty templates
  _NEWS_EMPTY_TEMPLATE = '<li class="none-found no-hover">No articles found. <a class="reset-filters" href="#/All years/All locations/All topics/All tags">Reset filters</a></li>',
  _HOMEPAGE_NEWS_EMPTY_TEMPLATE = '<article class="none-found no-hover">No articles found. <a class="reset-filters" href="#/All years/All locations/All topics/All tags">Reset filters</a></article>';
  // more templates
  _NEWS_MORE_TEMPLATE = '<li class="load-more"><a href="#">Load <span>{{count}}</span> more articles <i class="icon-angle-down"></i></a></li>'
  _HOMEPAGE_NEWS_MORE_TEMPLATE = '<article class="load-more"><a href="#">Load <span>{{count}}</span> more articles <i class="icon-angle-down"></i></a></article>';



// ensure required libraries are available
yepnope([
  {
    test: typeof Mustache === 'undefined',
    yep: ['../../../../static.chk.com/mustache/latest/mustache.min.js'/*tpa=http://static.chk.com/mustache/latest/mustache.min.js*/],
    callback: function () {}
  }, 
  {
    test: typeof moment === 'undefined',
    yep: ['../../../../static.chk.com/moment/2.9.0/moment.min.js'/*tpa=http://static.chk.com/moment/2.9.0/moment.min.js*/],
    callback: function () {}
  },
  {
    test: $.address,
    nope: ['../../../../static.chk.com/jquery.address/1.5/jquery.address-1.5.min.js'/*tpa=http://static.chk.com/jquery.address/1.5/jquery.address-1.5.min.js*/],
    callback: function () {},
    
    complete: function () {
      var targets = $("*[data-ajax='buildNewsSummary']");
      targets.each( function () {
        initializeNews( $(this) );
      });
    }
  }
]);

function initializeNews( target ) {
  var isFirstLoad = true;
  
  $.address.init(function( event ) {
    // create the settings state object
    initializeSettings( target, event );
    // create and store the markup containers for the section, sidebar and filters
    buildNewsScaffolding();
    // build the filter controls
    buildNewsFilters();

  });
  
  $.address.change(function(event) {
    // if there are filters in the $.address path, set the filters
    setAddressFilters( event );
    // build the articles list
    getNewsDataAndBuildArticles();
    
    if (isFirstLoad) {
      isFirstLoad = false;
    } else {
      setFilterUrls( event );
    }
  });
}

function initializeSettings(target, event) {
  Settings = {
    target: target,
    section: $(),
    articles: $(),
    filters: $(),
    sidebar: $(),
    pagesize: 0,
    filterable: false,
    filterdisplaynames: [ Filters.YEAR.displayname, Filters.LOCATION.displayname, Filters.TOPIC.displayname, Filters.TAG.displayname ],
    count: (target.attr("data-article-count") === undefined) ? _DEFAULT_ARTICLE_COUNT : parseInt( target.attr( "data-article-count" ) ),
    year: (target.attr("data-news-year") === undefined) ? _DEFAULT_NEWS_YEAR : target.attr("data-news-year"),
    location: (target.attr("data-news-location") === undefined) ? _DEFAULT_NEWS_LOCATION : target.attr("data-news-location"),
    topic: (target.attr("data-news-topic") === undefined) ? _DEFAULT_NEWS_TOPIC : target.attr("data-news-topic"),
    tag: (target.attr("data-news-tag") === undefined) ? _DEFAULT_NEWS_TAG : target.attr("data-news-tag"),
    showyears: (target.attr("data-news-show-years") === undefined) ? _DEFAULT_SHOW_YEARS : target.attr("data-news-show-years").toLowerCase() === "true",
    showlocations: (target.attr("data-news-show-locations") === undefined) ? _DEFAULT_SHOW_LOCATIONS : target.attr("data-news-show-locations").toLowerCase() === "true",
    showtopics: (target.attr("data-news-show-topics") === undefined) ? _DEFAULT_SHOW_TOPICS : target.attr("data-news-show-topics").toLowerCase() === "true",
    showtags: (target.attr("data-news-show-tags") === undefined) ? _DEFAULT_SHOW_TAGS : target.attr("data-news-show-tags").toLowerCase() === "true",
    sidebarfilter: (target.attr("data-sidebar-filter") === undefined) ? _DEFAULT_SIDEBAR_FILTER : target.attr("data-sidebar-filter").toLowerCase(),
    onlypressreleases: (target.attr("data-news-only-show-press-releases") === undefined) ? _DEFAULT_ONLY_SHOW_PRESS_RELEASES : target.attr("data-news-only-show-press-releases").toLowerCase() === "true",
    hidepressreleases: (target.attr("data-news-hide-press-releases") === undefined) ? _DEFAULT_HIDE_PRESS_RELEASES : target.attr("data-news-hide-press-releases").toLowerCase() === "true",
    more: (target.attr("data-more-link") === undefined) ? _DEFAULT_MORE_LINK : target.attr("data-more-link"),
    rss: (target.attr("data-rss-link") === undefined) ? _DEFAULT_RSS_LINK : target.attr("data-rss-link"),
    template: (target.attr("data-template") === undefined) ? _DEFAULT_TEMPLATE_NAME.toLowerCase() : target.attr("data-template").toLowerCase(),
    articleMarkup: _NEWS_SUMMARY_TEMPLATE,
    footerMarkup: _NEWS_FOOTER_TEMPLATE,
    emptyMarkup: _NEWS_EMPTY_TEMPLATE,
    moreMarkup: _NEWS_MORE_TEMPLATE
  };

  // if filters are displayed, set filterable to true
  if ( Settings.showyears || Settings.showlocations || Settings.showtopics || Settings.showtags ) Settings.filterable = true;
  // enable the sidebar when appropriate
  if ( $.inArray( Settings.sidebarfilter, Settings.filterdisplaynames ) !== -1 ) Settings["show" + Settings.sidebarfilter ] = true;
  else Settings.sidebarfilter = _DEFAULT_SIDEBAR_FILTER;
  // Set .count to default if it is set to a non-number (could be mistyped in the markup)
  if ( isNaN( Settings.count ) ) Settings.count = _DEFAULT_ARTICLE_COUNT;
  // Set .year to current year when the year filter is shown
  if ( Settings.year === "All years" && Settings.showyears ) Settings.year = new Date().getFullYear();
  // If the template is homepage, use the hompage markup templates
  if (Settings.template === 'home page' || Settings.template === 'homepage') {
    Settings.template = 'homepage';
    Settings.articleMarkup = _HOMEPAGE_NEWS_SUMMARY_TEMPLATE;
    Settings.footerMarkup = _HOMEPAGE_NEWS_FOOTER_TEMPLATE;
    Settings.emptyMarkup = _HOMEPAGE_NEWS_EMPTY_TEMPLATE;
    Settings.moreMarkup = _HOMEPAGE_NEWS_MORE_TEMPLATE;
  }
  
  //Settings.count = 5;
  
}



function buildNewsScaffolding() {
  // empty the news target
  Settings.target.html("");

  // if needed, create the sidebar section and store a reference to it in the settings
  if ( Settings.sidebarfilter !== "" ) Settings.target.append( Settings.sidebar = $("<div/>").addClass("two-col-secondary").addClass("news-sidebar") );

  // create the articles section and store a reference to it in the settings
  Settings.section = $("<div/>").addClass("news-section");
  // add class for when a sidebar is used
  if ( Settings.sidebarfilter !== "" ) Settings.section.addClass("two-col-primary");
  // put the section in the target, before the sidebar
  Settings.target.prepend(Settings.section);

  // create an articles list and store a reference to it in the settings; use a different article list for homepage news
  Settings.articles = (Settings.template === "homepage") ? $("<div/>").addClass("news-grid") : $("<ul/>").addClass("investor-news");
  Settings.articles.addClass("article-list");
  // append the article container to the section
  Settings.section.append(Settings.articles);
  
  // create an filter container and store a reference to it in the settings. 
  Settings.filters = $('<div/>').addClass("filter-container").addClass("inline-container");
  // add a class to the section that holds the filters and articles, if filters are shown
  if ( Settings.filterable ) Settings.section.addClass("filterable");
  // add a class that denotes the template used
  Settings.section.addClass(Settings.template);
}

function getNewsDataAndBuildArticles() {
  // get the news data and use it to build a list of articles
  $.ajax({
    url: _LIST_SERVICE_PATH_NEWS_SUMMARY + "/" + _DEFAULT_NEWS_LIST_NAME,
    dataType: 'JSONP',
    data: buildNewsQueryParameters(),
    type: 'GET',
    success: function (response) {
      buildNewsSummary(response);
    },
    error: function () {
      if (console.log) console.log("Error retrieving news articles"); 
    }
  });
}


function buildNewsQueryParameters() {
  var queryFilter = _DEFAULT_NEWS_FILTER;
  // Don't show articles set to hide on home page when the homepage template is used
  queryFilter = ( Settings.template === "homepage" ) ? queryFilter + " and chkRollupHideOnHomePage eq 'No'" : queryFilter;
  // Filter down to a specific year if one is provided
  queryFilter = ( Settings.year !== "All years" ) ? queryFilter + " and " + Filters.YEAR.column + " eq '" + Settings.year + "'" : queryFilter;
  // deal with locations (chkNewsCategories)
  if ( Settings.onlypressreleases ) {
    queryFilter = queryFilter + " and " + Filters.LOCATION.column + " eq 'Press Releases'";
  } else {
    // Filter out press releases if specified
    queryFilter = ( Settings.hidepressreleases ) ? queryFilter + " and " + Filters.LOCATION.column + " ne 'Press Releases'" : queryFilter;
    // Filter down to a specific location if one is provided
    queryFilter = ( Settings.location !== "All locations" ) ? queryFilter + " and (" + Filters.LOCATION.column + " eq '" + Settings.location + "' or " + Filters.LOCATION.column + " eq 'All locations')" : queryFilter;
  }
  // Filter down to a specific topic if one is provided
  queryFilter = ( Settings.topic !== "All topics" ) ? queryFilter = queryFilter + " and (" + Filters.TOPIC.column + " eq '" + Settings.topic + "' or " + Filters.TOPIC.column + " eq 'All topics')" : queryFilter;
  // Filter down to a specific topic if one is provided
  queryFilter = ( Settings.tag !== "All tags" && Settings.tag !== "All+tags" ) ? queryFilter + " and substringof('" + Settings.tag + "'," + Filters.TAG.column + ")" : queryFilter;

  // return the query parameters object
  return {
    list: _DEFAULT_NEWS_LIST_NAME,
    subsite: _DEFAULT_NEWS_SUBSITE,
    $top: Settings.count,
    $skip: Settings.pagesize,
    $orderby: _DEFAULT_NEWS_ORDER,
    $filter: queryFilter,
    fields: _DEFAULT_NEWS_SELECT
  };
}


function buildNewsSummary(data) {
    // fade the article list back in
    Settings.articles.fadeTo( "fast" , 1 )
    // if the page size is zero, clear the article list
    if (Settings.pagesize === 0) Settings.articles.html("");
    // if no articles are returned, use the empty markup template
    if ( data.length < 1 ) {
        // grab the empty template markup and hide it so we can fade it in later
        var empty = $(Settings.emptyMarkup).hide();
        // if this is the first page or paging is not used
        if (Settings.pagesize === 0) {
            // show the empty markup
            Settings.articles.append(empty.fadeIn());
        // if paging is used, and a footer exists
        } else if (Settings.articles.find(".no-hover").length === 1) {
            // reuse the empty markup for a "no more" message and insert it before the footer
            Settings.articles.find(".no-hover").before(empty.text("No additional articles found.").fadeIn());
            // hide the load more news link
            $(".load-more").fadeOut();
        }
    // otherwise, data is returned - build the article items
    } else {
        // loop through the articles
        $.each(data, function (i, item) {
            // build the markup, then hide it so we cn fade it in later
            var article = $(buildArticleMarkup(item)).hide();
            // if this is the first page or paging is not used, append the article to the article list
            if (Settings.pagesize === 0) Settings.articles.append(article.fadeIn());
            // otherwise, find the footer and insert the article before it
            else Settings.articles.find(".load-more").before(article.fadeIn());
        });
        // if this is the first page or paging is not used
        if (Settings.pagesize === 0) {
          // Do not display paging on summaries and homepage templates
          if ( Settings.template !== "summary" && Settings.template !== "homepage" ) {
            // append the load more news item
            Settings.articles.append( $( Mustache.to_html( Settings.moreMarkup, Settings ) ) );
          }
          // build the footer since it does not exist yet
          Settings.articles.append(buildFooterMarkup());
          // if there are less articles in the result set than the page size
          if ( data.length < Settings.count ) {
            // hide the load more news link
            $(".load-more").hide();
          } else {
            // otherwise, setup the click event for the load more news link
            $(".load-more").bind("click", function () {
              Settings.pagesize = Settings.pagesize + Settings.count;
              getNewsDataAndBuildArticles();
              return false;
            });
          }
        }
    }
}

function buildNewsFilters() {
  // do nothing if not filterable
  if ( !Settings.filterable ) return;

  // create the filter placeholders
  var yearsPlaceholder = $('<div/>').addClass("years-placeholder"),
    locationsPlaceholder = $('<div/>').addClass("locations-placeholder"),
    topicsPlaceholder = $('<div/>').addClass("topics-placeholder");
    tagsPlaceholder = $('<div/>').addClass("tags-placeholder");

  // append placeholders to the filter container
  Settings.filters.append(yearsPlaceholder).append(locationsPlaceholder).append(topicsPlaceholder).append(tagsPlaceholder);
  // append the filter container to the section
  Settings.section.prepend(Settings.filters);
  
  // build the appropriate filters
  if ( Settings.showyears ) getFilterAndBuildUi( Filters.YEAR.column, Filters.YEAR.displayname );
  if ( Settings.showlocations ) getFilterAndBuildUi( Filters.LOCATION.column, Filters.LOCATION.displayname );
  if ( Settings.showtopics ) getFilterAndBuildUi( Filters.TOPIC.column, Filters.TOPIC.displayname );
  if ( Settings.showtags ) getFilterAndBuildUi( Filters.TAG.column, Filters.TAG.displayname );

  function getFilterAndBuildUi( column, name ) {
    var result;
    $.ajax({
      //url: 'http://services.chk.com/Lists2/Pages?list=Pages&subsite=News&%24group=[column]&fields=[column]',
      url: _LIST_SERVICE_PATH_NEWS_SUMMARY + "/" + _DEFAULT_NEWS_LIST_NAME,
      dataType: 'JSONP',
      jsonpCallback: name,
      type: 'GET',
      data: {
        list: _DEFAULT_NEWS_LIST_NAME,
        subsite: _DEFAULT_NEWS_SUBSITE,
        $group: column,
        fields: column
      },
      success: function (data) {
        // build the filter control
        result = buildFilterUi( distinctFilters(data, name, column), name );
      },
      error: function () {
        if (console.log) console.log('Error retrieving ' + name + ' for the news articles"');
      },
      complete: function() {
        // replace the placeholder with the filter control
        $(".filter-container > ." + name + "-placeholder").replaceWith( result );
        // set all the urls for the anchors
        setFilterUrls();
        // reformat the sidebar filter as a list
        if ( Settings.sidebarfilter !== "" ) listifySidebarFilter();
      }
    });
  }

  function distinctFilters( data, name, column ){
    // build an array of distinct filter values
    var filtersArray = [];
    $.each(data, function (i, item) {
      var filterlist = eval( "item." + column );
      if (filterlist !== "") {
        var filters = filterlist.split(";");
        $.each(filters, function(j, filter) {
          if ($.inArray(filter, filtersArray) === -1) {
            if (filter !== "") {
              filtersArray.push(filter);
            }
          }
        });
      }
    });
    
    // sort the filter values
    filtersArray.sort();
    // for the year filter, reverse the order of the values values
    if ( name === Filters.YEAR.displayname ) filtersArray.reverse();
    // if an all items value doesn't exist, prepend one to the beginning of the array
    if ( filtersArray.indexOf( "All " + name ) === -1) filtersArray.unshift( "All " + name );
    // return the array
    return filtersArray;
  }

  function buildFilterUi( filterValues, name ){
    // create a container for the filter, add some classes
    var filter = $("<div/>").addClass(name + "-filter").addClass("news-filter").attr("data-filter-group", name),
    // create a variable to hold the initial filter value    
      initialDefault = "";
    
    // set the initial value - could be written to loop through the Filters object instead...
    if ( name === Filters.YEAR.displayname ) initialDefault = Settings.year;
    if ( name === Filters.LOCATION.displayname ) initialDefault = Settings.location;
    if ( name === Filters.TOPIC.displayname ) initialDefault = Settings.topic;
    if ( name === Filters.TAG.displayname ) initialDefault = Settings.tag;
    
    // create the filter header
    var filterHeader = $('<h2><span>' + initialDefault + '</span> <i class="icon-angle-down"></i></h2>');
    // create the click event for the header
    filterHeader.click( function (e) {
    	e.preventDefault();
    	// when the header is clicked, toogle the visibility of the list of values
    	$(this).parent().find('ul').slideToggle(500);
    });
    // append the header to the filter container
    filter.append(filterHeader);
    // create a container for the values list
    var filterList = $("<ul>");
    // for every filter value...
    $.each(filterValues, function (i, item) {
        // create a variable to hold the current value
        var filterValue = filterValues[i],
        // create an item for the filter value        
          listItem = $('<li>').attr("data-filter-value", filterValue),
        // create an anchor for the filter value
          anchor = $('<a/>').attr("data-" + name, filterValue).text(filterValue + " ").append( $("<i/>").addClass("icon-cancel") );
        // add the anchor to the item
        listItem.append(anchor);
        // if this item's value equals the current filter's value, set it to active
        if ( filterValue === initialDefault.toString() ) anchor.addClass("active");
        // add the item to the filter list
        $(filterList).append(listItem);
    });
    // add the list to the filter container and return it
    return filter.append(filterList);
  }
  
  function listifySidebarFilter() {
    // get the name of the filter that is to be displayed in the sidebar
    var name = Settings.sidebarfilter, 
    // grab a reference to the filter and detatch it from the DOM
      list = Settings.filters.find("." + name + "-filter").detach();
    // add the default class to the filter's header and hide the arrow icon
    list.find("h2").addClass("default").text(name).find("i").hide();
    // append the filter to the sidebar
    Settings.sidebar.append( list );
  }
}

function setFilterUrls( obj ) {
  obj = ( typeof obj === "undefined" ) ? Settings : obj;
  for( var thisname in Filters ) {
    var year = ( obj === Settings ) ? obj.year : obj.pathNames[Filters.YEAR.value],
      location = ( obj === Settings ) ? obj.location : obj.pathNames[Filters.LOCATION.value],
      topic = ( obj === Settings ) ? obj.topic : obj.pathNames[Filters.TOPIC.value],
      tag = ( obj === Settings ) ? obj.tag : obj.pathNames[Filters.TAG.value],
      values = [ year, location, topic, tag ],
      filterKey = Filters[thisname].name.toUpperCase(),
      filterDisplayName = Filters[thisname].displayname,
      anchorsSelector = "." + filterDisplayName + "-filter li a";
      
    $(anchorsSelector).each( function(){
      var anchor = $(this),
        isActive = anchor.hasClass("active"),
        text = ( isActive ) ? "All " + filterDisplayName : anchor.attr("data-" + filterDisplayName);
      values[Filters[filterKey].value] = text;
      anchor.attr("href", "#/" + values[0] + "/" + values[1] + "/" + values[2] + "/" + values[3] );
    });
  }
}

function setAddressFilters ( e ) {
  // for every type of filter enumerated in the Filters object
  for ( var thisname in Filters ) {
    // create a variable that holds the filter value in the address
    var addressFilterText = e.pathNames[Filters[thisname].value],
    // create a variable that holds the filter name
      filterName = Filters[thisname].name;
    // set the filter value in the Settings object
    if ( typeof addressFilterText !== "undefined" ) Settings[filterName] = addressFilterText;
    // do the set of actions required when changing a filter value
    setFilter( Filters[thisname].displayname, Settings[filterName] );
  }
}

function setFilter( name, value ) {
  // fade the articles list
  Settings.articles.fadeTo( "fast" , 0.4 );
  // show the first page worth of results
  Settings.pagesize = 0;

  var filter = $(".news-filter[data-filter-group='" + name + "']"),
    link = filter.find("a[data-" + name + "='" + value + "']"),
    isSidebar = filter.parent().hasClass("news-sidebar"),
    isActive = link.hasClass("active");
  // if not a sidebar filter, slide the filter list up
  if ( !isSidebar ) filter.find('ul').slideUp(500);
  // if not a sidebar filter, set this filter's header to the filter value
  if ( !isSidebar ) filter.find("h2 > span").text( value );
  // if this is a sidebar filter, set this filter's header to the filter name
  if ( isSidebar ) filter.find("h2 > span").text( filter.attr("data-filter-group") );

  // remove active from all items
  filter.find('a').each(function(){ 
    $(this).removeClass("active"); 
  });
  // set the anchor to active
  link.addClass("active");
}

function buildArticleMarkup(item) {
  // create the article markup based on the article data and a mustache template
  return Mustache.to_html(Settings.articleMarkup, ArticleSummary(item));
}

function buildFooterMarkup() {
  // create the footer markup based on the Settings data and a mustache template
  var footerUrls = {
    MoreUrl: Settings.more,
    RssUrl: Settings.rss
  };
  return Mustache.to_html(Settings.footerMarkup, footerUrls);
}


function ArticleSummary(item) {
  // create an article item
  var contentDate = moment(item.chkContentDate, "MM/DD/YYYY h:mm A");
  var headline = (item.chkContentHeadline === "") ? item.Title : item.chkContentHeadline;
  return {
    Headline: headline,
    Timestamp: contentDate.format('YYYY-MM-DD'),
    Weekday: contentDate.format('dddd'),
    ShortMonth: contentDate.format('MMM'),
    LongMonth: contentDate.format('MMMM'),
    Day: contentDate.format('DD'),
    Month: contentDate.format('MM'),
    Year: contentDate.format('YYYY'),
    NewsLocation: item.chkNewsCategory,
    Title: item.Title,
    Url: _DEFAULT_NEWS_PATH.toLowerCase() + "/" + toLowerReplacingSpacesWithDashes(item.chkNewsCategory) + "/" + sanitizeTitleForArticleUrl(item.Title),
    Address: "#/" + contentDate.format('YYYY') + "/" + item.chkExternalSourceContentId
  };
}

function toLowerReplacingSpacesWithDashes(name) {
  return name.toLowerCase().replace(/\s+/g, "-");
}

function sanitizeTitleForArticleUrl(name) {
  return name.replace(/\s+/g, "+").replace(/;/, "");
}