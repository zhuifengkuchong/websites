$(function () {
  var _ZOOM_SCRIPT_PATH = "../../../../static.chk.com/jquery.smart.zoom/2014.07.24/e-smart-zoom-jquery.min.js"/*tpa=http://static.chk.com/jquery.smart.zoom/2014.07.24/e-smart-zoom-jquery.min.js*/,
      _DEFAULT_BACKGROUND_COLOR = "#f2f2f2",
      _DEFAULT_CLICK_ZOOM = 0.8,
      _DEFAULT_IMAGE_PATH = "Unknown_83_filename"/*tpa=http://www2013.chk.com/sitecollectionimages/default-image.jpg*/,
      _ZOOM_MARKUP = '<div class="zoom-container"><img class="zoom-image" /></div>',
      _ZOOM_CONTROL_MARKUP = '<div class="zoom-controls"><span class="zoom-in" title="Zoom in"><i class="icon-plus"></i></span><span class="zoom-out" title="Zoom out"><i class="icon-plus"></i></span></div>';

  yepnope({
  	test: typeof smartZoom === 'undefined',
    yep: [_ZOOM_SCRIPT_PATH],
  	callback: function( url, result, key ) { 
      //if ( typeof smartZoom === 'object' ) console.log( url + " loaded!"); 
      //else console.log( "Error loading zoom script" );
  	},
  	complete: function() {
      //console.log("Image zoom loaded!"); 

      var targets = $("*[data-ajax='imageZoom']");
      if ( targets.length < 1 ) { return; }
      
      targets.each( function(i) {
        var target = $(this), targetId = "imageZoom"+i;
        target.attr("id", targetId);
        buildZoomWidget(target);
      });
  	}
  });
    
  function buildZoomWidget( target ) {
    // set default values
    var backgroundColor = ( target.data("background-color")  === undefined ) ? _DEFAULT_BACKGROUND_COLOR : target.data("background-color"),
        clickZoom = ( target.data("click-zoom") === undefined ) ? _DEFAULT_CLICK_ZOOM : target.data("click-zoom"),
        imagePath = ( target.data("image-path") === undefined ) ? _DEFAULT_IMAGE_PATH : target.data("image-path");
    
    target.html( $( _ZOOM_CONTROL_MARKUP ) );
    target.append( $( _ZOOM_MARKUP ) );
    var zoomImage = target.find("img.zoom-image"),
        zoomIn = target.find(".zoom-controls>.zoom-in"),
        zoomOut = target.find(".zoom-controls>.zoom-out");
    zoomImage.attr("src", imagePath);
    $(zoomImage).smartZoom( { "maxScale" : 4, "containerBackground" : backgroundColor } );
    $(zoomIn).bind("click", zoomClick);
    $(zoomOut).bind("click", zoomClick);
  }

  function zoomClick(e){
    var scaleToAdd = ( $(e.target).parent().hasClass("zoom-out") || $(e.target).hasClass("zoom-out") ) ? -0.8 : 0.8;
    $(".zoom-image").smartZoom("zoom", scaleToAdd);
  }    

});
