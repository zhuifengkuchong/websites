var _INVESTOR_SERVICE_URL = "http://services.chk.com/Lists",
  _INVESTOR_NONGAAP_SERVICE_URL = "http://services.chk.com/Lists2/Documents";
  
yepnope({
	test: typeof moment === 'undefined',
	yep: ['../../../../static.chk.com/moment/2.9.0/moment.min.js'/*tpa=http://static.chk.com/moment/2.9.0/moment.min.js*/],
	callback: function( url, result, key ) { 
		//console.log( url + " loaded!"); 
	},
	complete: function() {
		//console.log("Investor loaded!"); 
		$(document).ready(function () {
			LoadGlobalVariables();
		    GetStockInformation();
			GetAnalystRatings();
		    DisplayAvailableMeasures();
			GetPeRatios();
			GetEPSGrowthRates(); 
			GetEarningForeCasts();
			GetEarningActuals();
			buildEarningsArchive();
			loadNonGaap();
		});
	}
});

function GetStockInformation(){
var StockInfoContainer = $('*[data-ajax="StockInformation"]'); 

//Set the list and the parameters which needs to be returned back for the Analyst Estimates Table
var param = {
		list : 'ThompsonStockInformation',
		$orderby : 'chkThompsonQuoteDate desc',
		$top : 1,
		$select: 'chkThompsonDiffFromPrevClose,chkThompsonFiftyTwoWeekHigh,chkThompsonFiftyTwoWeekLow,chkThompsonMedianPriceTarget,chkThompsonPercentDifference,chkThompsonPrevClose,chkThompsonQuoteDate'
		};
		if(StockInfoContainer.length != 0){
				$.ajax({
						url: _INVESTOR_SERVICE_URL,
						dataType: 'JSONP',
						jsonpCallback: 'AnalystEstimates',
						data: param,
						type: 'GET',
						success: function (data) {
							DisplayStockInformation(data);
						},
						error: function () {
							if (console.log) console.log('Error retrieving Analyst Estimates"');
						}
					}); 
			}

}


function DisplayStockInformation(data){
		var EstimateTable = '<table class="investors"><tr><th>Stock Information</th><th></th></tr>' ;
		$.each(data, function(i, item){
			   EstimateTable = EstimateTable + '<tr><td>Price (Prev. Close)</td><td>$'+item.chkThompsonPrevClose.toFixed(2) +'</td></tr>' +
											   '<tr><td>52 Week High</td><td>$'+item.chkThompsonFiftyTwoWeekHigh.toFixed(2)+'</td></tr>' +
											   '<tr><td>52 Week Low</td><td>$'+item.chkThompsonFiftyTwoWeekLow.toFixed(2)+'</td></tr>' +
											    '<tr><td>Price Date(Latest)</td><td>'+moment(item.chkThompsonQuoteDate).format('L')+'</td></tr>' +
											   '<tr><td>Median Price Target</td><td>$'+item.chkThompsonMedianPriceTarget.toFixed(2)+'</td></tr>'; //+ 
											   //'<tr><td>Diff from Prev. Close</td><td>$'+item.chkThompsonDiffFromPrevClose+'</td></tr>' +
											   //'<tr><td>% Difference</td><td>'+(item.chkThompsonPercentDifference).toFixed(2)+'%</td></tr>' ;
		});
		
		EstimateTable = EstimateTable + '</table>';
		var StockInfoContainer = $("*[data-ajax='StockInformation']"); 
		//StockInfoContainer.append(EstimateTable);
		StockInfoContainer.html(EstimateTable);
		
}


function GetAnalystRatings(){
var AnalystRatingsContainer = $('*[data-ajax="AnalystRatings"]'); 

	var param = {
		list : 'thompsonAnalystRatings',
		$orderby : 'chkThompsonMeasureDate desc',
		$top : 1,
		$select: 'chkThompsonBuy,chkThompsonHold,chkThompsonMeanEstimate,chkThompsonMeasureDate,chkThompsonSell,chkThompsonStrongBuy,chkThompsonUnderPerform'
		};
		if(AnalystRatingsContainer.length != 0){
				$.ajax({
						url: _INVESTOR_SERVICE_URL,
						dataType: 'JSONP',
						data: param,
						type: 'GET',
						success: function (data) {
							DisplayAnaylstRatings(data);
						},
						error: function () {
							if (console.log) console.log('Error retrieving Analyst Ratings"');
						}
					}); 
			}

}


function DisplayAnaylstRatings(data){
			var RatingsTable = '<table class="investors"><tr><th>Analyst Ratings</th><th></th></tr>' ;
		$.each(data, function(i, item){
			   RatingsTable = RatingsTable + '<tr><td>Strong Buy</td><td>'+item.chkThompsonStrongBuy +'</td></tr>' +
											   '<tr><td>Buy</td><td>'+item.chkThompsonBuy+'</td></tr>' +
											   '<tr><td>Hold</td><td>'+item.chkThompsonHold+'</td></tr>' +
											   '<tr><td>Underperform</td><td>'+item.chkThompsonUnderPerform+'</td></tr>' +
											   '<tr><td>Sell</td><td>'+item.chkThompsonSell+'</td></tr>' ;
											  
		});
		
		RatingsTable = RatingsTable + '</table>';
		var AnalystRatingsContainer = $("*[data-ajax='AnalystRatings']"); 
		//AnalystRatingsContainer.append(RatingsTable);
		AnalystRatingsContainer.html(RatingsTable);
		var MeanInformation =  '<br/><div style="font-weight:normal; font-size:small;text-align:center;">Mean Recommendation: <strong>'+data[0].chkThompsonMeanEstimate.toFixed(1)+'</strong><br/>' + 
								'<img src="../../../../phx.corporate-ir.net/img/fc3.jpg.gif"/*tpa=http://phx.corporate-ir.net/img/fc3.jpg*/></img></div>';
		AnalystRatingsContainer.append(MeanInformation);
}


function GetPeRatios(){
var PeRatioContainer = $("*[data-ajax='PeRatio']"); 

	var param = {
		list : 'ThompsonPERatioGrowthRate',
		$orderby : 'chkThompsonFiscalPeriod asc',
		$top : 3,
		$filter : 'chkThompsonEstimateType eq \'PERatio\'',
		$select: 'chkThompsonCompany,chkThompsonFiscalPeriod,chkThompsonIndustry,chkThompsonEstimateType,chkThompsonRelative'
		};
		if(PeRatioContainer.length != 0){
				$.ajax({
						url: _INVESTOR_SERVICE_URL,
						dataType: 'JSONP',
						data: param,
						type: 'GET',
						success: function (data) {
							DisplayPERatios(data);
						},
						error: function () {
							if (console.log) console.log('Error retrieving Analyst Ratings"');
						}
					}); 
			}
}


function DisplayPERatios(data){
	var PERatioTable = '<table class="investors"><tr><th>P/E Ratios</th><th>Company</th><th>Industry</th><th>Ind. Relative</th></tr>' ;
		$.each(data, function(i, item){
			   PERatioTable = PERatioTable + '<tr><td>'+item.chkThompsonFiscalPeriod +'</td>' +
											   '<td>'+(item.chkThompsonCompany).toFixed(2)+'</td>' +
											   '<td>'+(item.chkThompsonIndustry).toFixed(2)+'</td>' +
											    '<td>'+(item.chkThompsonRelative).toFixed(2)+'</td></tr>';						  
		});
		
		PERatioTable = PERatioTable + '</table>';
		var PeRatioContainer = $("*[data-ajax='PeRatio']"); 
		//PeRatioContainer.append(PERatioTable);
		PeRatioContainer.html(PERatioTable);
		DisplayPERatioChart();
}

function DisplayPERatioChart(){
	var PeGrowthURL = '../../../../phx.corporate-ir.net/CfxTemp/104617PERatio.jpg'/*tpa=http://phx.corporate-ir.net/CfxTemp/104617PERatio.jpg*/;
var PeCompanyIndustry = '../../../../media.corporate-ir.net/media_files/IROL/global_images/spacer.gif'/*tpa=http://media.corporate-ir.net/media_files/IROL/global_images/spacer.gif*/;
var strPeGrowthChart = '<table><tr><td valign="top" colspan="7">' +
							'<img border="0" src='+PeGrowthURL+'></td></tr>'+
							'<tr style="font-size:10px;">'+
								'<td width="60"><img src="../../../../media.corporate-ir.net/media_files/IROL/global_images/spacer.gif"/*tpa=http://media.corporate-ir.net/media_files/IROL/global_images/spacer.gif*/ width="1" height="10"></td>'+
								'<td align="center"><span>FY0</span></td>'+
								'<td width="15"><img src="../../../../media.corporate-ir.net/media_files/IROL/global_images/spacer.gif"/*tpa=http://media.corporate-ir.net/media_files/IROL/global_images/spacer.gif*/ width="1" height="10"></td>'+
								'<td align="center"><span class="ccbnDisclaimer">FY1</span></td>'+
								'<td width="15"><img src="../../../../media.corporate-ir.net/media_files/IROL/global_images/spacer.gif"/*tpa=http://media.corporate-ir.net/media_files/IROL/global_images/spacer.gif*/ width="1" height="10"></td>'+
								'<td align="center"><span class="ccbnDisclaimer">FY2</span></td>'+
								'<td width="38"><img src="../../../../media.corporate-ir.net/media_files/IROL/global_images/spacer.gif"/*tpa=http://media.corporate-ir.net/media_files/IROL/global_images/spacer.gif*/ width="1" height="10"></td>'+
							'</tr>'+
							'<tr>'+
							'<td width="60"><img src="../../../../media.corporate-ir.net/media_files/IROL/global_images/spacer.gif"/*tpa=http://media.corporate-ir.net/media_files/IROL/global_images/spacer.gif*/ width="1" height="10"></td>'+
							' <td colspan="6" align="center">'+
								'<table cellpadding="0" cellspacing="0" border="0" width="100%">'+
								'<tbody>'+
								'<tr style="font-size:small">'+
								'<td width="50%" valign="top" align="left"><img src="../../../../media.corporate-ir.net/media_files/irol/global_images/EstimatesBlue1.gif"/*tpa=http://media.corporate-ir.net/media_files/irol/global_images/EstimatesBlue1.gif*/ border="0"><span class="ccbnDisclaimer">&nbsp;Company</span></td>'+
								'<td width="50%" valign="top" align="right"><img src="../../../../media.corporate-ir.net/media_files/irol/global_images/EstimatesBlue2.gif"/*tpa=http://media.corporate-ir.net/media_files/irol/global_images/EstimatesBlue2.gif*/ border="0"><span class="ccbnDisclaimer">&nbsp;Industry</span></td>'+
								'</tr>'+
								'</tbody>'+
								'</table>'+
							'</td>'+
							'</tr>'+
							'</table>';
							
	 //$('#PeRatioContainer').append(strPeGrowthChart);		
	 $('#PeRatioContainer').html(strPeGrowthChart);
}

function GetEPSGrowthRates(){
var GrowthRateContainer = $("*[data-ajax='GrowthRate']"); 

	var param = {
		list : 'ThompsonPERatioGrowthRate',
		$orderby : 'chkThompsonFiscalPeriod asc',
		$top : 3,
		$filter : 'chkThompsonEstimateType eq \'GrowthRate\'',
		$select: 'chkThompsonCompany,chkThompsonFiscalPeriod,chkThompsonIndustry,chkThompsonEstimateType,chkThompsonRelative'
		};
		if(GrowthRateContainer.length !=0){
				$.ajax({
						url: _INVESTOR_SERVICE_URL,
						dataType: 'JSONP',
						data: param,
						type: 'GET',
						success: function (data) {
							DisplayEPSGrowhRates(data);
						},
						error: function () {
							if (console.log) console.log('Error retrieving Analyst Ratings"');
						}
					}); 
			}
}

function DisplayEPSGrowhRates(data){

   
	var PERatioTable = '<table class="investors"><tr><th>P/E Ratios</th><th>Company</th><th>Industry</th><th>Ind. Relative</th></tr>' ;
		$.each(data, function(i, item){
			   PERatioTable = PERatioTable + '<tr><td>'+item.chkThompsonFiscalPeriod +'</td>' +
											   '<td>'+(item.chkThompsonCompany).toFixed(2)+'</td>' +
											   '<td>'+item.chkThompsonIndustry.toFixed(2)+'</td>' +
											    '<td>'+item.chkThompsonRelative.toFixed(2)+'</td></tr>';						  
		});
		
		PERatioTable = PERatioTable + '</table>';
		var GrowthRateContainer = $('*[data-ajax="GrowthRate"]'); 
		//GrowthRateContainer.append(PERatioTable);
		GrowthRateContainer.html(PERatioTable);
		//GrowthRateContainer.append('<img style="width:230px;" src="'+PeGrowthURL+'"></img>');
		DisplayPeGrowthChart();
}

//Display comparison chart for the PE Growth of the company versus industry
function DisplayPeGrowthChart() {
var PeGrowthURL = '../../../../phx.corporate-ir.net/CfxTemp/104617Growth.jpg'/*tpa=http://phx.corporate-ir.net/CfxTemp/104617Growth.jpg*/;
var PeCompanyIndustry = '../../../../media.corporate-ir.net/media_files/IROL/global_images/spacer.gif'/*tpa=http://media.corporate-ir.net/media_files/IROL/global_images/spacer.gif*/;
var strPeGrowthChart = '<table><tr><td valign="top" colspan="7">' +
							'<img border="0" src='+PeGrowthURL+'></td></tr>'+
							'<tr style="font-size:10px;">'+
								'<td width="60"><img src="../../../../media.corporate-ir.net/media_files/IROL/global_images/spacer.gif"/*tpa=http://media.corporate-ir.net/media_files/IROL/global_images/spacer.gif*/ width="1" height="10"></td>'+
								'<td align="center"><span>FY1/FY0</span></td>'+
								'<td width="15"><img src="../../../../media.corporate-ir.net/media_files/IROL/global_images/spacer.gif"/*tpa=http://media.corporate-ir.net/media_files/IROL/global_images/spacer.gif*/ width="1" height="10"></td>'+
								'<td align="center"><span class="ccbnDisclaimer">FY2/FY1</span></td>'+
								'<td width="15"><img src="../../../../media.corporate-ir.net/media_files/IROL/global_images/spacer.gif"/*tpa=http://media.corporate-ir.net/media_files/IROL/global_images/spacer.gif*/ width="1" height="10"></td>'+
								'<td align="center"><span class="ccbnDisclaimer">FY3/FY2</span></td>'+
								'<td width="38"><img src="../../../../media.corporate-ir.net/media_files/IROL/global_images/spacer.gif"/*tpa=http://media.corporate-ir.net/media_files/IROL/global_images/spacer.gif*/ width="1" height="10"></td>'+
							'</tr>'+
							'<tr>'+
							'<td width="60"><img src="../../../../media.corporate-ir.net/media_files/IROL/global_images/spacer.gif"/*tpa=http://media.corporate-ir.net/media_files/IROL/global_images/spacer.gif*/ width="1" height="10"></td>'+
							' <td colspan="6" align="center">'+
								'<table cellpadding="0" cellspacing="0" border="0" width="100%">'+
								'<tbody>'+
								'<tr style="font-size:small">'+
								'<td width="50%" valign="top" align="left"><img src="../../../../media.corporate-ir.net/media_files/irol/global_images/EstimatesBlue1.gif"/*tpa=http://media.corporate-ir.net/media_files/irol/global_images/EstimatesBlue1.gif*/ border="0"><span class="ccbnDisclaimer">&nbsp;Company</span></td>'+
								'<td width="50%" valign="top" align="right"><img src="../../../../media.corporate-ir.net/media_files/irol/global_images/EstimatesBlue2.gif"/*tpa=http://media.corporate-ir.net/media_files/irol/global_images/EstimatesBlue2.gif*/ border="0"><span class="ccbnDisclaimer">&nbsp;Industry</span></td>'+
								'</tr>'+
								'</tbody>'+
								'</table>'+
							'</td>'+
							'</tr>'+
							'</table>';
							
	//$('#PeGrowthRateContainer').append(strPeGrowthChart);						
	$('#PeGrowthRateContainer').html(strPeGrowthChart);
							
}

function GetEarningForeCasts(selectedMeasure){
	var EstimateContainer = $('*[data-ajax="AnalystForeCastInfo"]'); 

	if(selectedMeasure){
		filterParam = selectedMeasure;
		EstimateContainer.html('');
	}
	else{
		filterParam = 'eps';
	}
	var param = {
		list : 'ThompsonEarningsForcasts',
		$orderby : 'chkThompsonMeasureDate asc',
		$filter : 'chkThompsonMeasureType eq \''+ filterParam +'\'',
		$select: 'chkThompsonHigh,chkThompsonLow,chkThompsonMean,chkThompsonMedian,chkThompsonMeasureType,chkThompsonMeasureDate,chkThompsonPeriod,chkThompsonPeriodType'
		};
		if(EstimateContainer.length !=0){
				$.ajax({
						url: _INVESTOR_SERVICE_URL,
						dataType: 'JSONP',
						data: param,
						type: 'GET',
						success: function (data) {
							DisplayEarningForecasts(data);
							//DisplayAvailableMeasures(data);
						},
						error: function () {
							if (console.log) console.log('Error retrieving Analyst Ratings"');
						}
					}); 
			}	
}

function DisplayAvailableMeasures() {
   var measurelist = '<br/><table class="investors"><tr><th>Avaliable Measures</th></tr><tr><td>';
	 measurelist =  measurelist + '<select onchange="GetEarningForeCasts(value);GetEarningActuals(value);">'+
					 '<option value="eps">Earnings Per Share (EPS)</option>'+
					 '<option value="bps">Book Value Per Share (BPS)</option>'+
					  '<option value="cps">Cash Flow Per Share (CPS)</option>'+
					  '<option value="dps">Dividend Per Share (DPS)</option>'+
					  '<option value="ebi">EBIT (EBI)</option>'+
					  '<option value="ebt">EBITDA (EBT)</option>'+
					  '<option value="gps">GAAP Earnings Per Share (GPS)</option>'+
					  '<option value="nav">Net Asset Value (NAV)</option>'+
					  '<option value="ndt">Net Debt (NDT)</option>'+
					  '<option value="net">Net Income (NET)</option>'+
					  '<option value="opr">Operating Profit (OPR)</option>'+
					  '<option value="roe">Return On Equity (ROE)</option>'+
					  '<option value="sal">Sales (SAL)</option>'+
					  '<option value="seppar">Earnings Per Share (Parent) (SEPPAR)</option>'+
					  '<option value="snipar">Net Income (Parent) (SNIPAR)</option>'+
					  '<option value="soppar">Operating Profit (Parent) (SOPPAR)</option>'+
					  '<option value="ssapar">Sales (Parent) (SSAPAR)</option>'+
					 '</select></td></tr></table>';
					 
	var AvailMeasuresContainer = $('*[data-ajax="AvailableMeasures"]');
	//AvailMeasuresContainer.append(measurelist);
	AvailMeasuresContainer.html(measurelist);

}


//This function will display analyst forecasts
function DisplayEarningForecasts(data){

	var AnalystForeCastTable = '<table class="investors"><tr><th>Result Type</th><th>Fiscal Period</th><th>Mean</th><th>High</th><th>Low</th><th>Median</th></tr>' ;
		$.each(data, function(i, item){
			   AnalystForeCastTable = AnalystForeCastTable + '<tr><td>'+item.chkThompsonPeriodType +'</td>' +
											  '<td>'+item.chkThompsonPeriod +'</td>' +
											  '<td>'+ RoundValues(item.chkThompsonMean) +'</td>' +
											  '<td>'+ RoundValues(item.chkThompsonHigh)+'</td>' +
											  '<td>'+ RoundValues(item.chkThompsonLow) +'</td>' +
											  '<td>'+ RoundValues(item.chkThompsonMedian) +'</td></tr>';						  
		});
		
		AnalystForeCastTable = AnalystForeCastTable + '</table>';
		var AnalystForeCastContainer = $('*[data-ajax="AnalystForeCastInfo"]'); 
		//AnalystForeCastContainer.append(AnalystForeCastTable);
    AnalystForeCastContainer.html(AnalystForeCastTable);
}


function GetEarningActuals(selectedMeasure){
	var ReportedDataContainer = $('*[data-ajax="ReportedData"]'); 
	
	if(selectedMeasure){
		filterParam = selectedMeasure;
		ReportedDataContainer.html('');
	}
	else{
		filterParam = 'eps';
	}
	
	var param = {
		list : 'ThompsonEarningsActuals',
		 $orderby : 'chkThompsonMeasureDate asc',
		$filter : 'chkThompsonMeasureType eq \''+ filterParam +'\'',
		 $select: 'chkThompsonMeanEstimate,chkThompsonMeasureType,chkThompsonPeriod,chkThompsonPeriodType,chkThompsonReported,chkThompsonMeasureDate,chkThompsonSuprisePercent'
		};
		if(ReportedDataContainer.length != 0){
				$.ajax({
						url: _INVESTOR_SERVICE_URL,
						dataType: 'JSONP',
						data: param,
						type: 'GET',
						success: function (data) {
							DisplayEarningActuals(data);
						},
						error: function () {
							if (console.log) console.log('Error retrieving Analyst Ratings"');
						}
					}); 
			}	
}


function DisplayEarningActuals(data){
	
	var ActualDataTable = '<table class="investors"><tr><th>Result Type</th><th>Fiscal Period</th><th>Reported EPS</th><th>Mean Estimate</th><th>Surprise % Change</th></tr>' ;
		$.each(data, function(i, item){
			   ActualDataTable = ActualDataTable + '<tr><td>'+item.chkThompsonPeriodType +'</td>' +
											  '<td>'+item.chkThompsonPeriod +'</td>' +
											  '<td>'+ RoundValues(item.chkThompsonReported) +'</td>' +
											  '<td>'+ RoundValues(item.chkThompsonMeanEstimate)+'</td>' + 
											  '<td>'+ RoundValues(item.chkThompsonSuprisePercent)+'</td></tr>';						  
		});
		
		ActualDataTable = ActualDataTable + '</table>';
		var ReportedDataContainer = $("*[data-ajax='ReportedData']"); 
		//ReportedDataContainer.append(ActualDataTable);
		ReportedDataContainer.html(ActualDataTable);
}   




function LoadGlobalVariables(){
		window.ReportsPage = 1;
		window.topVariable = 20;
		window.skipRecords = 0;
}

function buildEarningsArchive(LoadMore){	
	var EstimateContainer = $("*[data-ajax='buildEarningsArchive']"); 

	var skip = skipRecords;
	var toprecords = window.topVariable;
	var param = {
		list : 'ThompsonReportsPresentations',
		$orderby : 'chkThompsonDocumentDate desc,chkThompsonDocumentTitle desc',
		$top : toprecords,
		$skip : skip,
		$select: 'chkThompsonDocumentTitle,chkThompsonDocumentDate,chkThompsonDocumentURL,chkThompsonCodec,chkThompsonDocumentCode',
		$filter: "chkThompsonDocumentCode eq 'SS' or chkThompsonDocumentCode eq 'AU'"
		};
		if(EstimateContainer.length != 0){
				$.ajax({
						url: _INVESTOR_SERVICE_URL,
						dataType: 'JSONP',
						data: param,
						type: 'GET',
						success: function (data) {
							renderEarningsArchive(data,LoadMore);
						},
						error: function () {
							if (console.log) console.log('Error retrieving Analyst Ratings"');
						}
					}); 
			}	
}

function renderEarningsArchive(data,LoadMore){
  var documentTypeIcon; 

  if(!LoadMore) {

    var ReportsTable = '<table id="EarningsArchive" class="investors earnings-archive"><tr><th>Date</th><th style="text-align:left;">Title</th><th>Download</th></tr>';
		$.each(data, function(i, item){
      //console.log( moment(item.chkThompsonDocumentDate).format('L'), item.chkThompsonDocumentTitle.slice(0, 1) );
      if ( item.chkThompsonDocumentTitle.slice(0, 1) == "Q" ) {
        if(item.chkThompsonCodec.toLowerCase() == "pdf") {
          documentTypeIcon = "../../img/pdf-icon.png"/*tpa=http://www.chk.com/Style Library/img/pdf-icon.png*/;
        }
        else if(item.chkThompsonCodec.toLowerCase() == "flash") {
          documentTypeIcon = "../../../sitecollectionimages/wildcards/webcast.png"/*tpa=http://www.chk.com/sitecollectionimages/wildcards/webcast.png*/
        }
        ReportsTable = ReportsTable + '<tr><td>'+ moment(item.chkThompsonDocumentDate).format('L') +'</td>' +
          '<td style="text-align:left;">'+item.chkThompsonDocumentTitle +'</td>' +
          '<td><a href="'+item.chkThompsonDocumentURL.Url+'" target="_new"><img style="height:31px;" src="'+documentTypeIcon+'"></img></a></td>';
      }
    });
		
    //ReportsTable = ReportsTable + '<tr><td><a id="LoadMoreReports" href="#">Load More</a></td></tr>';
    ReportsTable = ReportsTable + '</table>';
    var ReportedDataContainer = $("*[data-ajax='buildEarningsArchive']"); 
    //ReportedDataContainer.append(ReportsTable);
    ReportedDataContainer.html(ReportsTable);
    ReportedDataContainer.append('<span><a name="End" id="LoadMoreReports" href="#End">Load More</a></span>'); 
    $('#LoadMoreReports').click(loadMoreEarningsArchive);

  }
	else {

		var ReportsTable;
		$.each(data, function(i, item){
      if ( item.chkThompsonDocumentTitle.slice(0, 1) == "Q" ) {
        if(item.chkThompsonCodec.toLowerCase() == "pdf") {
          documentTypeIcon = "../../img/pdf-icon.png"/*tpa=http://www.chk.com/Style Library/img/pdf-icon.png*/;
        }
        else if(item.chkThompsonCodec.toLowerCase() == "flash") {
          documentTypeIcon = "../../../sitecollectionimages/wildcards/webcast.png"/*tpa=http://www.chk.com/sitecollectionimages/wildcards/webcast.png*/
        }
  			ReportsTable = ReportsTable + '<tr><td>'+ moment(item.chkThompsonDocumentDate).format('L') +'</td>' +
  			  '<td style="text-align:left;">'+item.chkThompsonDocumentTitle +'</td>' +
  			  '<td><a href="'+item.chkThompsonDocumentURL.Url+'" target="_new"><img style="height:31px;" src="'+documentTypeIcon+'"></img></a></td>';
  		}
		});
		$("#EarningsArchive tr:last").after(ReportsTable);
	}
}


function loadMoreEarningsArchive(){
	window.skipRecords = window.skipRecords + 20;
	buildEarningsArchive(true);
}

/*
function loadNonGaapMeasure() {
	var EstimateContainer = $('*[data-ajax="nonGaapMeasure"]'); 

	var filterParam = 'FileDirRef eq \'/Documents/Investors/non-gaap\'';
	var param = {
		 recursive : 'true',
		 fields : 'Title,FileRef,FileLeafRef',
		 $orderby : 'FileLeafRef desc',
		 $filter : filterParam
		};
		 if(EstimateContainer.length != 0){
				$.ajax({
						url: _INVESTOR_NONGAAP_SERVICE_URL,
						dataType: 'JSONP',
						data: param,
						type: 'GET',
						success: function (data) {
							DisplayNonGaap(data);
						},
						error: function () {
							if (console.log) console.log('Error retrieving Analyst Ratings"');
						}
					}); 
			}	
}

function DisplayNonGaap(data){
		strNonGaapTable = '<ul class="documentList non-gaap">';
		$.each(data, function(i,item){
			strNonGaapTable = strNonGaapTable + '<li><a class="pdf-icon downloadable" href="'+item.FileRef+'" target="_blank"><span class="btn-download"><i class="icon-download"></i></span> '+item.Title+'</a></li>' ;							
		});
		strNonGaapTable = strNonGaapTable + '</ul>';
		var NonGaapContainer = $('*[data-ajax="nonGaapMeasure"]'); 
		//NonGaapContainer.append(strNonGaapTable);
		NonGaapContainer.html(strNonGaapTable);
}
*/

function loadNonGaap() {
  var target = $('*[data-ajax="nonGaapMeasure"]'),
    template = ( target.attr("data-template")  === undefined ) ? "table" : target.attr("data-template"),
    filterParam = 'FileDirRef eq \'/Documents/Investors/non-gaap\'';

  var param = {
    recursive : 'true',
    fields : 'Title,FileRef,FileLeafRef',
    $orderby : 'FileLeafRef desc',
    $filter : filterParam
  };
	
  if(target.length !== 0){
    $.ajax({
      url: _INVESTOR_NONGAAP_SERVICE_URL,
      dataType: 'JSONP',
      data: param,
      type: 'GET',
      success: function (data) {
        buildNonGaap(data, target, template);
      },
      error: function () {
        if (console.log) console.log('Error retrieving Analyst Ratings"');
      }
    }); 
  }	
}

function buildNonGaap(data, target, template){
  var element;
  if ( template === "table") {
    element = $('<table/>').addClass("investors").addClass("annual-quarterly-reports");
    element.append( $('<thead><tr><th>Document Title</th></tr></thead>') );
    var tablebody = $('<tbody/>');
    $.each(data, function(i,item){
      var tablerow = $('<tr/>'),
        tablecell = $('<td/>').attr("data-title", "Document Title"),
        anchor = $('<a/>').addClass("pdf-icon").addClass("downloadable").attr("href", item.FileRef).attr("target", "_blank"),
        icon = $('<span class="btn-download"><i class="icon-download"></i></span>');        
      anchor.text(item.Title).prepend(icon);
      tablecell.append(anchor);
      tablerow.append(tablecell);
      tablebody.append(tablerow);
    });
    element.append(tablebody);
  } else {    
    element = $('<ul/>').addClass("documentList").addClass("non-gaap");
    $.each(data, function(i,item){
      var listitem = $('<li/>'),
        anchor = $('<a/>').addClass("pdf-icon").addClass("downloadable").attr("href", item.FileRef).attr("target", "_blank"),
        icon = $('<span class="btn-download"><i class="icon-download"></i></span>');
      anchor.text(item.Title).prepend(icon);
      listitem.append(anchor);
      element.append(listitem);
    });
  }
  target.html(element);
}


function RoundValues(param){
	var defaultParam = param != null ? param.toFixed(2) : 'N/A' ;
	return defaultParam;
}
