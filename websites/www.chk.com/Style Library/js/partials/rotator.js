
(function($) {

	var rotatorSetWidth = function(){
		var rotatorControlWidth = $(window).width(),
			rotatorControlPos = rotatorControlWidth - 1200,
			rotatorControlRight = Math.round(rotatorControlPos / 2  + 14);

		$('.item .background').css('width', rotatorControlWidth + 'px');
		// $('.item').css('width', rotatorControlWidth + 'px');

		if (rotatorControlWidth > 1200){
			$('.indicator').css('right', rotatorControlRight + 'px');
		} else if (rotatorControlWidth < 767) {
			// NO CALL ~ MOBILE
		} else {
			$('.indicator').css('right', '14px');
		}

	};

	rotatorSetWidth();

	var destaque = $(".rotator").destaque({
		slideMovement: 150,
		slideSpeed: 2000,
		elementSpeed: 2100,
		autoSlideDelay: 5000,
		easingType: "easeInOutQuart",
		itemSelector: ".item",
		itemBackgroundSelector: ".background",
		itemForegroundElementSelector: ".foreground .element",
		stopOnMouseOver: true,
		itemDefaultZIndex: 2,
		baseSize: $(window).width(),
		// controlsSelector: ".indicator .bullet",
		onInit: function(destaque) {
      var slideCount = destaque.element.find(destaque.params.itemSelector).length;
      if ( slideCount > 1 ) {
        var indicator  = $("<div/>").addClass("indicator");
        for( var i = 0; i < slideCount; i++ ){
          indicator.append( $("<a/>").addClass("bullet").attr("href", "#").attr("data-slide", i) );
        }
        indicator.insertAfter( destaque.element );
      }
		},
		onPageUpdate: function(destaque, pageData) {
			$(".indicator .bullet").removeClass("active");
			$(".indicator .bullet[data-slide='"+ pageData.currentSlide +"']").addClass("active");
			rotatorSetWidth();
		},
		onResize: function(destaque) {
			rotatorSetWidth();
		}
	});

	$(".indicator .bullet").click(function(e) {
	  e.preventDefault();
	  destaque.goTo(parseInt($(this).attr("data-slide"), 10));
	});

	$(window).resize(function(){
		var winWidth = $(window).width();
		destaque.refresh({baseSize: winWidth});
	})
	
})(jQuery);