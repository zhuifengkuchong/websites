var isIE8 = !+'\v1';
var isHome = ( $("#HomePageLayout").length === 1 ) ? true : false,
    isStandard = ( $("#StandardPageLayout").length === 1 ) ? true : false,
    isOperations = ( $("#OperationsPageLayout").length === 1 ) ? true : false;
    
var resizeWatcher = function() {

  var mql = window.matchMedia('screen and (min-width: 768px)'),
    mediaChanged = function(mql) {
  
    var isDesktop = ( mql.matches );
    
    if ( isDesktop ) { 

      primaryGlobalNavDestroy();
      primaryNavShow();         
      
      if ( isHome ) {
        userBoxInitIntent();
      } 
      
      if ( isOperations ) deskOpsInit();
      
      if ( isOperations || isStandard ) {
          /* if careers */ cgItemInitDesktop();
      }
     
    } else {

      primaryGlobalNavBuild();
      
      if ( isHome ) {
        userBoxDestroy();
      }
      
      if ( isOperations ) mobileOpsInit();
      
      if ( isOperations || isStandard ) {
        /* if careers */ cgItemInitMobile();
      }

    }
  };
  window.matchMedia('screen and (min-width: 768px)').addListener(mediaChanged);
  mediaChanged(mql);

};

// ## Partials ########
//========================================================================================================================
var isInvestorSEC = $("*[data-ajax='buildSecFilings']").length > 0;
var isInvestorEvents = $("*[data-ajax='bindInvestorEvents']").length > 0;
var isInvestorChart = $("*[data-ajax='buildHistoricalStockChart']").length > 0;
var isInvestor = $('*[data-ajax="StockInformation"]').length > 0 || 
                 $('*[data-ajax="AnalystRatings"]').length > 0 || 
                 $("*[data-ajax='PeRatio']").length > 0 || 
                 $("*[data-ajax='GrowthRate']").length > 0 || 
                 $('*[data-ajax="AnalystForeCastInfo"]').length > 0 || 
                 $('*[data-ajax="ReportedData"]').length > 0 || 
                 $("*[data-ajax='buildEarningsArchive']").length > 0 || 
                 $('*[data-ajax="nonGaapMeasure"]').length > 0;
var isInvestorSimple = $("*[data-ajax='buildEmailAlertForm']").length > 0 || 
                       $("*[data-ajax='buildAnalystCoverage']").length > 0 || 
                       $("*[data-ajax='buildDividendSplitHistory']").length > 0 || 
                       $("*[data-ajax='buildAnnualQuarterlyReports']").length > 0 ||
                       $("*[data-ajax='upcomingEvents']").length > 0;
var isStockTicker =  $("*[data-ajax='buildStockTicker'], *[data-ajax='buildHeroStockTicker']").length > 0;
var isNewsSummary = $("*[data-ajax='buildNewsSummary']").length > 0;
var isYouTube = $("*[data-youtube-id]").length > 0;
var isGallery = $("*[data-ajax='videoGallery']").length > 0 || 
                $("*[data-ajax='buildGallery']").length > 0;
var isZoom = $("*[data-ajax='imageZoom']").length > 0;
var hasUpcomingEvents = $("*[data-ajax='upcomingEvents']").length > 0;

yepnope([
  {
    test: isOperations,
    yep: 'partials/operations.js'/*tpa=http://www.chk.com/Style Library/js/partials/operations.js*/
  },
  {
    test: isInvestorSEC,
    yep: 'partials/investor-sec.js'/*tpa=http://www.chk.com/Style Library/js/partials/investor-sec.js*/
  },
  {
    test: isInvestorEvents,
    yep: 'partials/investor-events.js'/*tpa=http://www.chk.com/Style Library/js/partials/investor-events.js*/
  },
  {
    test: isInvestorChart,
    yep: 'partials/investor-chart.js'/*tpa=http://www.chk.com/Style Library/js/partials/investor-chart.js*/
  },
  {
    test: isInvestor,
    yep: 'partials/Investor.js'/*tpa=http://www.chk.com/Style Library/js/partials/Investor.js*/
  },
  {
    test: isInvestorSimple,
    yep: 'partials/investor-simple.js'/*tpa=http://www.chk.com/Style Library/js/partials/investor-simple.js*/
  },
  {
    test: isStockTicker,
    yep: 'partials/stock-ticker.js'/*tpa=http://www.chk.com/Style Library/js/partials/stock-ticker.js*/
  },
  {
    test: isNewsSummary,
    yep: 'partials/news-summary.js'/*tpa=http://www.chk.com/Style Library/js/partials/news-summary.js*/
  },
  {
    test: isYouTube,
    yep: 'partials/youtube.js'/*tpa=http://www.chk.com/Style Library/js/partials/youtube.js*/
  },
  {
    test: isGallery,
    yep: 'partials/Gallery.js'/*tpa=http://www.chk.com/Style Library/js/partials/Gallery.js*/,
  },
  {
    test: isZoom,
    yep: 'partials/zoom.js'/*tpa=http://www.chk.com/Style Library/js/partials/zoom.js*/,
  },
  {
    test: isHome,
    yep: [
      '../../../static.chk.com/destaque/0.3.1/jquery.destaque.min.js'/*tpa=http://static.chk.com/destaque/0.3.1/jquery.destaque.min.js*/,
      '../../../static.chk.com/jquery-mobile/custom/jquery.mobile.events.min.js'/*tpa=http://static.chk.com/jquery-mobile/custom/jquery.mobile.events.min.js*/,
      '../../../static.chk.com/hoverintent/r5/jquery.hoverintent.min.js'/*tpa=http://static.chk.com/hoverintent/r5/jquery.hoverintent.min.js*/,
      'partials/rotator.js'/*tpa=http://www.chk.com/Style Library/js/partials/rotator.js*/,
      'partials/user-box.js'/*tpa=http://www.chk.com/Style Library/js/partials/user-box.js*/,
      'partials/form.js'/*tpa=http://www.chk.com/Style Library/js/partials/form.js*/,
      'partials/navigation.js'/*tpa=http://www.chk.com/Style Library/js/partials/navigation.js*/,
      ],
    nope: [
      'http://www.chk.com/Style Library/js/partials/careers-box.js?v=1',
      'partials/form.js'/*tpa=http://www.chk.com/Style Library/js/partials/form.js*/,
      'partials/navigation.js'/*tpa=http://www.chk.com/Style Library/js/partials/navigation.js*/
      ],
    complete: function() {
      resizeWatcher();
    }
  }
]);

