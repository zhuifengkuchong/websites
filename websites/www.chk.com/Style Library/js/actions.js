tabify();
hideLayoutAreas();
addIdsToTopLevelPrimaryNavigation();
setDoubleWideNavigationMenus([1,2,3]);
hookUpSearch();
fixHeroBackgrounds();
fixHeroWebparts();
fixParallax();
fixHomeRotator();
hookupShare();
fixRichContentWebparts();
fixAudience();
fixPdsForm( $("#login-owners"), "https://secure.pds-austin.com/chesapeake/main.asp", "owners" );
fixPdsForm( $("#login-vendors"), "https://secure.pds-austin.com/chesapeakeap/vrlogin.asp", "vendors" );
fixContactForm();
fixShareholderMeetingForm();
fixCompletionForm();
fixTrademarks();
fixDataVizEmbeds();
renderContentAccordion();
fixTweetText();
renderVideos();
fixCollageBox();

function tabify() {
  if ( $("*[data-action='tabify']").length < 1 ) { return; }
  $("*[data-action='tabify']").tabs().find(">ul").addClass('inline-container');
}

function hideLayoutAreas(){
  var hideHero = $("meta[name='hide-hero'][content='Yes']").length,
    hideLocal = $("meta[name='hide-local'][content='Yes']").length,
    hideShare = $("meta[name='hide-share'][content='Yes']").length,
    hero = $("http://www.chk.com/style library/js/div.hero"),
    local = $("div.local"),
    share = $("div.social-sharing-container");
    
  if ( hideHero  > 0 ) hero.hide();
  if ( hideLocal > 0 ) local.hide();
  if ( hideShare > 0 ) share.hide();
}

function addIdsToTopLevelPrimaryNavigation() {

  var topPrimaries = $("div.primary > ul > li");

  topPrimaries.each( function(i) {

    var item = $(this),
        text = item.find(">a").text().replace(/\ \b/gi,'-').toLowerCase();

    item.attr("id", text);
    
    if ( i === ( topPrimaries.length - 1 ) ) {
      item.addClass("nav-global-start");
    }

  });

}

function setDoubleWideNavigationMenus(arr) {
  var topPrimaries = $("div.primary > ul > li");
  topPrimaries.each( function(i) {
    if ( jQuery.inArray( i, arr ) != -1 ) {
      $(this).find(">ul.secondary").addClass("width-double");
    }
  });
}

function hookUpSearch() {

    //SEARCH
    $(".search-bar input").keypress(function (event) { if (event.keyCode == 13) { searchThis($(this).val()); return (false); } });
    $(".search-bar button").click(function () { searchThis($(this).siblings("input").eq(0).val()); return (false); });

}

function searchThis(keywordString) { window.location = "http://www.chk.com/search#k=" + keywordString; }

function fixHeroBackgrounds() {
  var imagebox = $("#rotator-bg"), imagepath = imagebox.find(">img").attr("src");
  if ( imagebox.css("background-image") === 'none' ) imagebox.css("background-image", "url("+imagepath+")" );
}

function fixHeroWebparts() {
  var heroWebpartBox = $(".ms-rte-wpbox"), heroWebpart = $(".hero-wildcard");
  if ( heroWebpartBox.length > 0 && heroWebpartBox.length > 0 ) heroWebpart.appendTo( heroWebpartBox );
}

function fixParallax() {
  $(".parallax-box #parallax-bg").each( function() {
    $(this).css( "background-image", "url(" + $(this).find("> img").attr("src") + ")" );
  });
  
  $(".parallax-container .spotlight-title").each( function() {
    $(this).css( "background-image", "url(" + $(this).find("> img").hide().attr("src") + ")" );
  });
}

function fixHomeRotator() {
  var bkgs = $(".rotator-container > .rotator > .item .background");
  bkgs.each( function() {
    if ( $("html.oldie").length > 0 ) return;
    $(this).css( "background-image", "url(" + $(this).find("> img").hide().attr("src") + ")" );
  });
  $(".rotator-container > .rotator > .item").each( function(i) { $(this).addClass("item-" + i); });
}

function hookupShare(){
  var button = $(".social-sharing > a");
  button.click( function(e) {
    e.stopPropagation();
    var button = $(this);
    var list = button.parent().find(".social-sharing-list");
    list.slideToggle('fast');
  });
  $('body').click(function(e) {
    var list = $(".social-sharing-list");
    if ($(e.target).closest('.social-sharing-list').length === 0) list.slideUp('fast');
  });
}

function fixRichContentWebparts() {
  var richParts = $("div.ms-rte-wpbox > div[id^='vid_']");
  
  richParts.each( function() {
    var part = $(this),
        guidDashes = $(this).attr("id").replace("vid_", ""),
        guidUnderscores = guidDashes.replace(/-/g, '_'),
        misplacedPart = $("div.ms-webpart-zone div[id*='" + guidUnderscores + "']"),
        misplacedContent = misplacedPart.closest(".ms-webpartzone-cell");
    part.after( $("<div/>").addClass("replaced-content").append(misplacedContent) );
  });
}

function fixAudience(){
  if ( $("ul.audiences").length === 0 ) return;
  $("ul.audiences").contents().filter(function() { return this.nodeType == 3; }).remove();
  $('div.login-box').html( $('div.login-box').html().replace(/\u200B/g,'') );
}

function fixPdsForm( element, target, type ) {
  if ( element.length === 0 ) return;
  element.find( "input.login-user" ).attr( "name", "Username" );
  element.find( "input.login-pw" ).attr( "name", "Password" );
  if ( type === "owners" ) element.append( '<input type="hidden" name="login" value="login">' );
  if ( type === "vendors" ) {
    element.append( '<input type="hidden" name="action" value="login">' );
    element.append( '<input type="hidden" name="LoginType" value="Vendor">' );
    element.append( '<input type="hidden" name="LoginPage" value="Vendor">' );
  }
  element.find( "button" ).attr( "name", "cslogin" ).val("login");
  element.wrap( $("<form/>").addClass("login-form").attr("action", target).attr("method", "POST") );
}

function fixContactForm() {
  var targetForms = $("div.ltif_wp");
  if ( targetForms.length < 1 ) return;
  if ( window.location.href.toLowerCase().indexOf("contact") === -1 ) { return; }
  targetForms.each( function() { 
    var thisForm = $(this).addClass("chk-form"),
      talkToDropdown = thisForm.find("select[id$='_Q_Group']"),
      aboutDropdown = thisForm.find("select[id$='_Q_Group2']"),
      temporaryDropdown = $("<select/>"),
      name = thisForm.find("input[id$='_Q_Name']").addClass("contact--name").attr("placeholder", "*NAME").attr("required","required"),
      email = thisForm.find("input[id$='_Q_Email']").addClass("contact--email").addClass("h5-email").attr("placeholder", "*EMAIL").attr("required","required"),//.attr("type","email"),
      phone = thisForm.find("input[id$='_Q_Phone']").addClass("contact--phone").attr("placeholder", "*PHONE").attr("required","required"),//.attr("type","tel"),.addClass("validate-ignore")
      subject = thisForm.find("input[id$='_Q_Subject']").addClass("contact--subject").attr("placeholder", "*SUBJECT").attr("required","required"),
      body = thisForm.find("textarea[id$='_Q_Body']").addClass("contact--message").attr("placeholder", "*YOUR MESSAGE").attr("required","required"),
      button = thisForm.find("input[id$='_btnSubmit']").addClass("btn-box").val("Send"),
      container = $("<div/>").addClass("form--contact");

    try{ email.prop("type","email"); } 
    catch(e){}
    try{ phone.prop("type","tel"); } 
    catch(e){}
    container.append(talkToDropdown).append("<br/>").append(aboutDropdown).append(name).append(email).append(phone).append(subject).append(body);
    thisForm.find(" div.ltip.pnlList").html( container );

    initializeSubcategoryDropdowns( talkToDropdown, aboutDropdown, temporaryDropdown );
    addCategoryDropdownChangeEventHandler( talkToDropdown, aboutDropdown, temporaryDropdown );
    addSubmitButtonClickEventHandler( button, talkToDropdown, aboutDropdown );

    //clone the about dropdown options to the temporary dropdown and hide the about dropdown
    function initializeSubcategoryDropdowns( talkToDropdown, aboutDropdown, temporaryDropdown ) {
      var subcategoryOptions = aboutDropdown.find("option");

      subcategoryOptions.each( function() {
        var currentOption = $(this);
        var pair = currentOption.text().split( ": " );
          currentOption.text( pair[1] ).attr( "data-category", pair[0] );
        });
    
        subcategoryOptions.clone().appendTo( temporaryDropdown );
        toggleSubcategory( talkToDropdown.val(), true, temporaryDropdown, aboutDropdown );
        if ( aboutDropdown.val() !== null ) { 
          aboutDropdown.show();
        } else {
          aboutDropdown.hide();
        }
    }
    
    function addCategoryDropdownChangeEventHandler( talkToDropdown, aboutDropdown, temporaryDropdown  ) {
        talkToDropdown.change( function() {
          toggleSubcategory( $(this).val(), true, temporaryDropdown, aboutDropdown );
        });
    }
    
    function toggleSubcategory( value, doSlide, temporaryDropdown, aboutDropdown ) {
        var matchingOptions = $([]),
          currentAboutValue = aboutDropdown.val();
        if ( value !== "Talk to..." ) { 
          matchingOptions = temporaryDropdown.find( "[data-category='" + value + "']" );
        }

        aboutDropdown.empty();
        if ( matchingOptions.length > 0 ) {
          temporaryDropdown.children().eq(0).clone().appendTo( aboutDropdown );
          matchingOptions.clone().appendTo( aboutDropdown );
          if (doSlide) { aboutDropdown.slideDown(); } 
          else { aboutDropdown.show(); }
          if ( currentAboutValue !== null ) {
            aboutDropdown.val( currentAboutValue );
          } else {
            aboutDropdown.focus();
          }
        } else {
          aboutDropdown.empty();
          if (doSlide) { aboutDropdown.slideUp(); } 
          else { aboutDropdown.hide(); }
        }
    }

    function addSubmitButtonClickEventHandler( button, talkToDropdown, aboutDropdown ) {
      var message = "";
      button.click( function () {
        if ( talkToDropdown.val() === "Talk to..." ) { 
          message = "You haven't selected a recipient for your message. Please select one to continue."; 
          alert(message);
          return false;
        }
        if ( aboutDropdown.val() === "About..." && aboutDropdown.find("option").length > 0 ) { 
          message = "You haven't selected a topic for your message. Please select one to continue."; 
          alert(message);
          return false;
        }
        return true;
      });
    }
  });
}

function fixShareholderMeetingForm(){
  var targetForms = $("div.ltif_wp");
  if ( targetForms.length < 1 ) return;
  if ( window.location.href.toLowerCase().indexOf("annual-shareholder-meeting") === -1 ) return;
  targetForms.each( function() { 
    var thisForm = $(this).addClass("asm-container").addClass("chk-form"),
      typeDefaultChoiceText = "*CHOOSE ONE",
      stateDefaultChoiceText = "*STATE",
      typeLabel = $('<p class="asm-label">Are you a registered shareholder or beneficial shareholder?</p>'),
      typeTipSummary = $('<div class="asm-tip"><i class="icon-info-circled"></i> Most registrants are beneficial shareholders. <a href="#typedetail">What is the difference?</a></div>'),
      typeTipDetail = $('<div id="tip-detail"><p><strong>Registered shareholders</strong> hold Chesapeake shares directly and appear on the records of Chesapeake’s transfer agent, Computershare.</p><p><strong>Beneficial shareholders</strong> hold Chesapeake shares through a bank, broker or other intermediary. Note that you will be required to provide proof of share ownership, such as a copy of the portion of your voting instruction form showing your name and address, a bank or brokerage firm account statement or a letter from the bank, broker or other intermediary holding your shares, confirming ownership.</p></div>').hide().css("opacity", 0),
      typeDropdown = thisForm.find("select[id$='_Q_AttendingType']").attr("required","required"),
      firstname = thisForm.find("input[id$='_Q_FirstName']").addClass("asm-first-name").attr("placeholder", "*FIRST NAME").attr("required","required"),
      middleInitial = thisForm.find("input[id$='_Q_Middle Initial']").addClass("asm-middle-initial").attr("placeholder", "INITIAL").addClass("validate-ignore"),
      lastname = thisForm.find("input[id$='_Q_LastName']").addClass("asm-last-name").attr("placeholder", "*LAST NAME").attr("required","required"),
      email = thisForm.find("input[id$='_Q_Email']").addClass("asm-email").addClass("h5-email").attr("placeholder", "*EMAIL").attr("required","required"),
      street = thisForm.find("input[id$='_Q_Address']").addClass("asm-address-1").attr("placeholder", "*STREET").attr("required","required"),
      city = thisForm.find("input[id$='_Q_City']").addClass("asm-city").attr("placeholder", "*CITY").attr("required","required"),
      stateDropdown = thisForm.find("select[id$='_Q_State']"),
      zip = thisForm.find("input[id$='_Q_Zip']").addClass("asm-zip").attr("placeholder", "*POSTAL CODE").attr("required","required"),
      phone = thisForm.find("input[id$='_Q_Phone']").addClass("asm-phone").attr("placeholder", "*PHONE").attr("required","required"),
      company = thisForm.find("input[id$='_Q_Company']").addClass("asm-company").attr("placeholder", "COMPANY").addClass("validate-ignore"),
      guestLabel = $('<p class="asm-label">Will you be bringing a guest or designating a proxy?</p>'),
      guestDropdown = thisForm.find("select[id$='_Q_BringGuest']").attr("required","required"),
      proxyFirstname = thisForm.find("input[id$='_Q_ProxyFirstName']").addClass("asm-proxy-firstname").attr("placeholder", "*PROXY FIRST NAME").addClass("validate-ignore"),
      proxyLastname = thisForm.find("input[id$='_Q_ProxyLastName']").addClass("asm-proxy-lastname").attr("placeholder", "*PROXY LAST NAME").addClass("validate-ignore"),
      proxyEmail = thisForm.find("input[id$='_Q_ProxyEmail']").addClass("asm-proxy-email").attr("placeholder", "*PROXY EMAIL").addClass("validate-ignore"),
      guestFirstname = thisForm.find("input[id$='_Q_GuestFirstName']").addClass("asm-guest-firstname").attr("placeholder", "*GUEST FIRST NAME").addClass("validate-ignore"),
      guestLastname = thisForm.find("input[id$='_Q_GuestLastName']").addClass("asm-guest-lastname").attr("placeholder", "*GUEST LAST NAME").addClass("validate-ignore"),
      guestRelationship = thisForm.find("input[id$='_Q_GuestRelationship']").addClass("asm-guest-relationship").attr("placeholder", "*GUEST RELATIONSHIP TO SHAREHOLDER").addClass("validate-ignore"),

      button = thisForm.find("input[id$='_btnSubmit']").addClass("btn-box").val("Register").css("margin-top", "1em"),
      container = $("<div/>").addClass("form-asm").addClass("form--contact"),
      typeContainer = $("<div/>").addClass("asm-type-container"),
      guestContainer = $("<div/>").addClass("asm-guest-container"),
      guestDetails = $("<div/>").addClass("asm-guest-details").hide().css("opacity", 0),
      proxyDetails = $("<div/>").addClass("asm-proxy-details").hide().css("opacity", 0),
      formRow = $("<div/>").addClass("chk-form-row"),
      fiftyBox = $("<div/>").addClass("fifty-percent"),
      fortyBox = $("<div/>").addClass("forty-percent"),
      thirtyBox = $("<div/>").addClass("thirty-percent"),
      twentyBox = $("<div/>").addClass("twenty-percent"),
      
      nameRow = formRow.clone().append( fortyBox.clone().append(firstname) ).append( twentyBox.clone().append(middleInitial) ).append( fortyBox.clone().append(lastname) ),
      locationRow = formRow.clone().append( fortyBox.clone().append(city) ).append( twentyBox.clone().append(stateDropdown) ).append( fortyBox.clone().append(zip) ),
      proxyNameRow = formRow.clone().append( fiftyBox.clone().append(proxyFirstname) ).append( fiftyBox.clone().append(proxyLastname) );

      
    typeDropdown.find("option").eq(0).val("").html(typeDefaultChoiceText);
    stateDropdown.find("option").eq(0).val("").html(stateDefaultChoiceText);
    
    addTipClickEventHandler( typeTipSummary.find(">a"), typeTipSummary, typeTipDetail );
    addGuestDropdownChangeEventHandler( guestDropdown, proxyDetails, guestDetails );
    
    try{ email.prop("type","email"); } 
    catch(e){}
    try{ phone.prop("type","tel"); } 
    catch(e){}
    try{ proxyEmail.prop("type","email"); } 
    catch(e){}
    
    
    typeContainer.append(typeLabel).append(typeDropdown).append(typeTipSummary).append(typeTipDetail);
    proxyDetails.append(proxyNameRow).append(proxyEmail);
    guestDetails.append(guestFirstname).append(guestLastname).append(guestRelationship);
    guestContainer.append(guestLabel).append(guestDropdown).append(guestDetails).append(proxyDetails);
    
    container.append(typeContainer).append(nameRow);
    container.append(street).append(locationRow);
    container.append(email).append(phone).append(company).append(guestContainer);
    thisForm.find(" div.ltip.pnlList").html( container );
    
    function addTipClickEventHandler( link, summary, detail ){
      link.on("click", function(e) {
        e.preventDefault();
        summary.animate({ "height": "hide", "opacity": 0 }, 300);
        detail.animate({ "height": "show", "opacity": 1 }, 300);
      });
    }
    
    function addGuestDropdownChangeEventHandler( dropdown, proxy, guest  ) {
      dropdown.change( function() {
        var choice = dropdown.val().toLowerCase();
        var choices = {
          'neither': function () {
            proxy.find("input").removeAttr("required").addClass("validate-ignore");
            guest.find("input").removeAttr("required").addClass("validate-ignore");
            proxy.animate({ "height": "hide", "opacity": 0 }, 300);
            guest.animate({ "height": "hide", "opacity": 0 }, 300);
          },
          'bringing a guest': function () {
            proxy.find("input").removeAttr("required").addClass("validate-ignore");
            guest.find("input").attr("required", "required").removeClass("validate-ignore");
            proxy.animate({ "height": "hide", "opacity": 0 }, 300);
            guest.animate({ "height": "show", "opacity": 1 }, 300);
          },
          'designating a proxy': function () {
            proxy.find("input").attr("required", "required").removeClass("validate-ignore");
            guest.find("input").removeAttr("required").addClass("validate-ignore");
            proxy.animate({ "height": "show", "opacity": 1 }, 300);
            guest.animate({ "height": "hide", "opacity": 0 }, 300);
          }
        };
        if (typeof choices[choice] !== 'function') { throw new Error('Invalid choice.'); }
        choices[choice]();
      });
    }
  });
}


function fixCompletionForm() {
  var targets = $("div.ltif_wp");
  if ( targets.length < 1 ) return;
  if ( window.location.href.toLowerCase().indexOf("completion") === -1 ) { return; }
  targets.each( function() { 
    var target = $(this),
      firstname = target.find("input[id$='_Q_FirstName']").addClass("completion-firstname").attr("placeholder", "*FIRST NAME").attr("required","required"),
      lastname = target.find("input[id$='_Q_LastName']").addClass("completion-lastname").attr("placeholder", "*LAST NAME").attr("required","required"),
      email = target.find("input[id$='_Q_Email']").addClass("completion-email").addClass("h5-email").attr("placeholder", "*EMAIL").attr("required","required"),
      company = target.find("input[id$='_Q_Company']").addClass("completion-company").attr("placeholder", "*COMPANY").attr("required","required"),
      location = target.find("input[id$='_Q_Location']").addClass("completion-location").attr("placeholder", "*LOCATION").attr("required","required"),
      signature = target.find("input[id$='_Q_Signature']").addClass("completion-signature").attr("placeholder", "*YOUR SIGNATURE").attr("required","required"),
      text = $('<div/>').addClass("completion-text").html( target.find("tr:last-child .singleLineQuestText").html() ),
      container = $("<div/>").addClass("form-completion").addClass("standard-form");

    target.find("input[id$='_btnSubmit']").addClass("btn-box").val("Sign");
    try{ email.prop("type","email"); } 
    catch(e){}
    container.append(firstname).append(lastname).append(email).append(company).append(location).append(text).append(signature);
    target.find(" div.ltip.pnlList").html( container );
  });
}


function fixTrademarks() {
  $('body :not(script,sup)').contents().filter(function() {
    return this.nodeType === 3;
  }).replaceWith(function() {
    return this.nodeValue.replace(/[™®©]/g, '<sup>$&</sup>');
  });
}

function fixDataVizEmbeds() {
  if (Modernizr.csstransforms) {
    $(".data-visualization").addClass("scalable");
  }
  else {
    $(".data-visualization").addClass("unscalable");
  }
}

function renderContentAccordion() {

  var targets = $("dl.accordion");
  if ( targets.length < 1 ) return;
  targets.each( function() { 
    var target = $(this);
    // display the initial open details
    showDetails( target.find("i.icon-angle-down").parent() );

    // Get all the links.
    var summaryLinks = target.find(" > dt > a");
    summaryLinks.addClass("btn-box");

    // click event
    summaryLinks.on('click', function(e) {
      e.preventDefault();
      var clickedLink = $(this),
        detailContainers = target.find(" > dd");
        
      if ( clickedLink.find(" > i").hasClass("icon-angle-down") ) {
        hideDetails( detailContainers, target );
      } else {
        hideDetails( detailContainers.not( clickedLink.attr("href") ), target );
        showDetails( clickedLink );
      }
    });
  });
        
  function showDetails( summaryAnchors ) {
    summaryAnchors.each( function() {
      var thisLink = $(this),
        thisSummaryArrow = thisLink.find(">i"),
        thisDetailContainer = $( thisLink.attr("href") );
      
      thisSummaryArrow.removeClass( "icon-angle-right" );
      thisSummaryArrow.addClass( "icon-angle-down" );
      thisDetailContainer.animate({ "height": "show", "opacity": 1 }, 300);
    });
  }

  function hideDetails( detailContainers, accordionContainer ) {
    detailContainers.each( function() {
      var thisDetailContainer = $(this),
        thisSummaryArrow = accordionContainer.find(" > dt > a[href='#" + thisDetailContainer.attr("id") + "'] > i");
        
      thisSummaryArrow.removeClass( "icon-angle-down" );
      thisSummaryArrow.addClass( "icon-angle-right" );
      thisDetailContainer.animate({ "height": "hide", "opacity": 0 }, 300);
    });
  }

}

function fixTweetText() {
  var targets = $("ul.social-sharing-list");
  if ( targets.length < 1 ) return;
  targets.each( function() { 
    var target = $(this);
    target.find("li>a.addthis_button_twitter").attr("addthis:title", $("#TweetText").text());
  });
}

function renderVideos(){
  var youtubeUrl = 'http://www.youtube.com/embed/',
    youtubeExtUrl = 'http://www.youtube.com/watch?v=',
    videoIframe = '<iframe class="article--video-player" width="100%" height="100%" allowfullscreen frameborder="0" />',
    videoParams = [
      'theme=light',
      'color=white',
      'wmode=transparent',
      'modestbranding=1',
      'iv_load_policy=3',
      'rel=0',
      'autohide=1',
      'vq=hd720',
      'fs=1',
      'controls=1'
    ],
    articleVideoLink = $('.video-expand'),
    articleVideoWrapper = $('.article--video-wrapper');

  var videoId = articleVideoLink.data('videoId'),
    youtubeExtUrlBuild = youtubeExtUrl + videoId,
    videoEmbedUrl = youtubeUrl + videoId + '?' + videoParams.join('&'),
    videoPlayer = $(videoIframe).attr('src', videoEmbedUrl);

  articleVideoLink.attr('href', youtubeExtUrlBuild);
  articleVideoWrapper.html(videoPlayer);
}

function fixCollageBox(){
  // haven't dug into what this is doing, could probabaly be named better.
  var $gridPhoto = $('.article--video-grid').find('.hover-content.photo');
  
  $(document).on('click', '.pg-item-photo', function(){
    return false;
  });

  $(document).on('click', '.pg-item-video', function(){
    return false;
  });
  
  $gridPhoto.each(function(){
    var gridPhotoImgSrc = $(this).find('img').attr('src'),
      currWidth = $(this).width();
    currWidth = currWidth - 14;
    $(this).css({
      'background-image': 'url(' + gridPhotoImgSrc + ')'
    });
  });
}