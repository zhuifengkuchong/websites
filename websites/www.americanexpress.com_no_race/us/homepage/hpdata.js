

//

'use strict';

var AHP = AHP || {};

AHP.isContentLoaded = false;
AHP.softLaunch = false;
AHP.currentTimeDate = null;
try {
	var timestamp = Date.parse('06/08/2015 16:41:35 GMT');
	if ( isNaN( timestamp ) === true ) {
		timestamp = Date.parse('06/08/2015 09:05:21 MST');
	}
	AHP.currentTimeDate = new Date(timestamp);
} catch (e) {
	AHP.currentTimeDate = new Date();
}

AHP.data = AHP.data || {};
AHP.data.heroTakeover = AHP.data.heroTakeover || {};
AHP.data.hero = AHP.data.hero || {};
try{
	var UN_M = "";
	AHP.data.urgentNotice = {
		m: UN_M
	}
} catch(e) {}

try {
	AHP.data.heroDefault = 40065;AHP.data.heroTakeover.id = "40067";AHP.data.heroTakeover.dts = "02/19/2015 00:00:00 MST";AHP.data.heroTakeover.dte = "02/19/2015 23:59:59 MST";AHP.data.heroes = {
	 "list": [
		, {
			"id":"40078"
			,"filter":"pr"
			,"dts":"06/02/2015 00:00:00 MST"
			,"dte":"07/06/2015 23:59:00 MST"
		}
		, {
			"id":"40076"
			,"filter":"cm,pr"
			,"dts":"05/21/2015 00:00:00 MST"
			,"dte":"06/17/2015 23:59:00 MST"
		}
		, {
			"id":"40075"
			,"filter":"cm,pr"
			,"dts":"05/07/2015 00:00:00 MST"
			,"dte":"06/30/2015 23:59:00 MST"
		}
		, {
			"id":"40074"
			,"filter":"cm,pr"
			,"dts":"05/04/2015 00:00:00 MST"
			,"dte":"07/04/2015 23:59:00 MST"
		}
	]
};


} catch(e) {}

